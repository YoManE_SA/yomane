﻿namespace Netpay.Crypt.UI
{
	partial class frmEncDec
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnStart = new System.Windows.Forms.Button();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblTarget = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnSelectTarget = new System.Windows.Forms.Button();
            this.btnSelectSource = new System.Windows.Forms.Button();
            this.cbKeyIndex = new System.Windows.Forms.ComboBox();
            this.chkUseBase64TextFile = new System.Windows.Forms.CheckBox();
            this.txtSource = new System.Windows.Forms.TextBox();
            this.txtTarget = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rbFolder = new System.Windows.Forms.RadioButton();
            this.rbFile = new System.Windows.Forms.RadioButton();
            this.btnManage = new System.Windows.Forms.Button();
            this.tbContainer = new System.Windows.Forms.TabControl();
            this.tbFiles = new System.Windows.Forms.TabPage();
            this.scText = new System.Windows.Forms.SplitContainer();
            this.txtInput = new Netpay.Crypt.UI.ctlEncodedText();
            this.txtOutput = new Netpay.Crypt.UI.ctlEncodedText();
            this.tbText = new System.Windows.Forms.TabPage();
            this.pnlFiles = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSignature = new System.Windows.Forms.TabPage();
            this.scSign = new System.Windows.Forms.SplitContainer();
            this.txtSignInput = new Netpay.Crypt.UI.ctlEncodedText();
            this.txtSignKey = new Netpay.Crypt.UI.ctlEncodedText();
            this.cbSignType = new System.Windows.Forms.ComboBox();
            this.txtSignOutput = new Netpay.Crypt.UI.ctlEncodedText();
            this.rbEncrypt = new System.Windows.Forms.RadioButton();
            this.rbDeccrypt = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.tbContainer.SuspendLayout();
            this.tbFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scText)).BeginInit();
            this.scText.Panel1.SuspendLayout();
            this.scText.Panel2.SuspendLayout();
            this.scText.SuspendLayout();
            this.tbText.SuspendLayout();
            this.pnlFiles.SuspendLayout();
            this.tbSignature.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scSign)).BeginInit();
            this.scSign.Panel1.SuspendLayout();
            this.scSign.Panel2.SuspendLayout();
            this.scSign.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlBody.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(314, 150);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(118, 29);
            this.btnStart.TabIndex = 10;
            this.btnStart.Text = "&Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(11, 51);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(41, 13);
            this.lblSource.TabIndex = 3;
            this.lblSource.Text = "Source";
            // 
            // lblTarget
            // 
            this.lblTarget.AutoSize = true;
            this.lblTarget.Location = new System.Drawing.Point(11, 98);
            this.lblTarget.Name = "lblTarget";
            this.lblTarget.Size = new System.Drawing.Size(38, 13);
            this.lblTarget.TabIndex = 6;
            this.lblTarget.Text = "Target";
            // 
            // btnSelectTarget
            // 
            this.btnSelectTarget.Location = new System.Drawing.Point(358, 114);
            this.btnSelectTarget.Name = "btnSelectTarget";
            this.btnSelectTarget.Size = new System.Drawing.Size(74, 20);
            this.btnSelectTarget.TabIndex = 8;
            this.btnSelectTarget.Text = "Select...";
            this.btnSelectTarget.UseVisualStyleBackColor = true;
            this.btnSelectTarget.Click += new System.EventHandler(this.btnSelectTarget_Click);
            // 
            // btnSelectSource
            // 
            this.btnSelectSource.Location = new System.Drawing.Point(358, 69);
            this.btnSelectSource.Name = "btnSelectSource";
            this.btnSelectSource.Size = new System.Drawing.Size(74, 20);
            this.btnSelectSource.TabIndex = 5;
            this.btnSelectSource.Text = "Select...";
            this.btnSelectSource.UseVisualStyleBackColor = true;
            this.btnSelectSource.Click += new System.EventHandler(this.btnSelectSource_Click);
            // 
            // cbKeyIndex
            // 
            this.cbKeyIndex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbKeyIndex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKeyIndex.FormattingEnabled = true;
            this.cbKeyIndex.Location = new System.Drawing.Point(329, 29);
            this.cbKeyIndex.Name = "cbKeyIndex";
            this.cbKeyIndex.Size = new System.Drawing.Size(109, 21);
            this.cbKeyIndex.TabIndex = 4;
            this.cbKeyIndex.SelectedIndexChanged += new System.EventHandler(this.cbKeyIndex_SelectedIndexChanged);
            // 
            // chkUseBase64TextFile
            // 
            this.chkUseBase64TextFile.AutoSize = true;
            this.chkUseBase64TextFile.Location = new System.Drawing.Point(20, 157);
            this.chkUseBase64TextFile.Name = "chkUseBase64TextFile";
            this.chkUseBase64TextFile.Size = new System.Drawing.Size(208, 17);
            this.chkUseBase64TextFile.TabIndex = 9;
            this.chkUseBase64TextFile.Text = "Use Base64 Text file method (very old)";
            this.chkUseBase64TextFile.UseVisualStyleBackColor = true;
            // 
            // txtSource
            // 
            this.txtSource.Location = new System.Drawing.Point(14, 69);
            this.txtSource.Name = "txtSource";
            this.txtSource.Size = new System.Drawing.Size(319, 20);
            this.txtSource.TabIndex = 4;
            // 
            // txtTarget
            // 
            this.txtTarget.Location = new System.Drawing.Point(14, 114);
            this.txtTarget.Name = "txtTarget";
            this.txtTarget.Size = new System.Drawing.Size(319, 20);
            this.txtTarget.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(327, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select Key";
            // 
            // rbFolder
            // 
            this.rbFolder.AutoSize = true;
            this.rbFolder.Location = new System.Drawing.Point(198, 15);
            this.rbFolder.Name = "rbFolder";
            this.rbFolder.Size = new System.Drawing.Size(54, 17);
            this.rbFolder.TabIndex = 2;
            this.rbFolder.Text = "Folder";
            this.rbFolder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbFolder.UseVisualStyleBackColor = true;
            // 
            // rbFile
            // 
            this.rbFile.AutoSize = true;
            this.rbFile.Checked = true;
            this.rbFile.Location = new System.Drawing.Point(121, 15);
            this.rbFile.Name = "rbFile";
            this.rbFile.Size = new System.Drawing.Size(41, 17);
            this.rbFile.TabIndex = 1;
            this.rbFile.TabStop = true;
            this.rbFile.Text = "File";
            this.rbFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbFile.UseVisualStyleBackColor = true;
            // 
            // btnManage
            // 
            this.btnManage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnManage.Location = new System.Drawing.Point(444, 28);
            this.btnManage.Name = "btnManage";
            this.btnManage.Size = new System.Drawing.Size(74, 23);
            this.btnManage.TabIndex = 5;
            this.btnManage.Text = "&Manage...";
            this.btnManage.UseVisualStyleBackColor = true;
            this.btnManage.Click += new System.EventHandler(this.btnManage_Click);
            // 
            // tbContainer
            // 
            this.tbContainer.Controls.Add(this.tbFiles);
            this.tbContainer.Controls.Add(this.tbText);
            this.tbContainer.Controls.Add(this.tbSignature);
            this.tbContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbContainer.Location = new System.Drawing.Point(8, 8);
            this.tbContainer.Margin = new System.Windows.Forms.Padding(0);
            this.tbContainer.Name = "tbContainer";
            this.tbContainer.SelectedIndex = 0;
            this.tbContainer.Size = new System.Drawing.Size(512, 288);
            this.tbContainer.TabIndex = 0;
            // 
            // tbFiles
            // 
            this.tbFiles.BackColor = System.Drawing.SystemColors.Control;
            this.tbFiles.Controls.Add(this.scText);
            this.tbFiles.Location = new System.Drawing.Point(4, 22);
            this.tbFiles.Name = "tbFiles";
            this.tbFiles.Padding = new System.Windows.Forms.Padding(3);
            this.tbFiles.Size = new System.Drawing.Size(504, 262);
            this.tbFiles.TabIndex = 1;
            this.tbFiles.Text = "Transform Text";
            // 
            // scText
            // 
            this.scText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scText.Location = new System.Drawing.Point(3, 3);
            this.scText.Name = "scText";
            this.scText.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scText.Panel1
            // 
            this.scText.Panel1.Controls.Add(this.txtInput);
            // 
            // scText.Panel2
            // 
            this.scText.Panel2.Controls.Add(this.txtOutput);
            this.scText.Size = new System.Drawing.Size(498, 256);
            this.scText.SplitterDistance = 122;
            this.scText.TabIndex = 2;
            // 
            // txtInput
            // 
            this.txtInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtInput.Location = new System.Drawing.Point(0, 0);
            this.txtInput.Name = "txtInput";
            this.txtInput.ReadOnly = false;
            this.txtInput.Size = new System.Drawing.Size(498, 122);
            this.txtInput.TabIndex = 0;
            this.txtInput.TextBackColor = System.Drawing.SystemColors.Window;
            this.txtInput.Title = "Input";
            this.txtInput.ViewEncodingType = Netpay.Crypt.UI.ctlEncodedText.EncodingType.PlainText;
            this.txtInput.Changed += new System.EventHandler(this.txtInput_Changed);
            // 
            // txtOutput
            // 
            this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOutput.Location = new System.Drawing.Point(0, 0);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.Size = new System.Drawing.Size(498, 130);
            this.txtOutput.TabIndex = 0;
            this.txtOutput.TextBackColor = System.Drawing.SystemColors.Window;
            this.txtOutput.Title = "Output";
            this.txtOutput.ViewEncodingType = Netpay.Crypt.UI.ctlEncodedText.EncodingType.PlainText;
            // 
            // tbText
            // 
            this.tbText.BackColor = System.Drawing.SystemColors.Control;
            this.tbText.Controls.Add(this.pnlFiles);
            this.tbText.Location = new System.Drawing.Point(4, 22);
            this.tbText.Name = "tbText";
            this.tbText.Padding = new System.Windows.Forms.Padding(3);
            this.tbText.Size = new System.Drawing.Size(504, 262);
            this.tbText.TabIndex = 0;
            this.tbText.Text = "Transform Files";
            // 
            // pnlFiles
            // 
            this.pnlFiles.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlFiles.Controls.Add(this.label3);
            this.pnlFiles.Controls.Add(this.rbFolder);
            this.pnlFiles.Controls.Add(this.btnStart);
            this.pnlFiles.Controls.Add(this.rbFile);
            this.pnlFiles.Controls.Add(this.txtTarget);
            this.pnlFiles.Controls.Add(this.txtSource);
            this.pnlFiles.Controls.Add(this.lblSource);
            this.pnlFiles.Controls.Add(this.lblTarget);
            this.pnlFiles.Controls.Add(this.btnSelectSource);
            this.pnlFiles.Controls.Add(this.chkUseBase64TextFile);
            this.pnlFiles.Controls.Add(this.btnSelectTarget);
            this.pnlFiles.Location = new System.Drawing.Point(29, 25);
            this.pnlFiles.Name = "pnlFiles";
            this.pnlFiles.Size = new System.Drawing.Size(446, 196);
            this.pnlFiles.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Source Type:";
            // 
            // tbSignature
            // 
            this.tbSignature.BackColor = System.Drawing.SystemColors.Control;
            this.tbSignature.Controls.Add(this.scSign);
            this.tbSignature.Location = new System.Drawing.Point(4, 22);
            this.tbSignature.Name = "tbSignature";
            this.tbSignature.Padding = new System.Windows.Forms.Padding(3);
            this.tbSignature.Size = new System.Drawing.Size(504, 262);
            this.tbSignature.TabIndex = 2;
            this.tbSignature.Text = "Signature calculator";
            // 
            // scSign
            // 
            this.scSign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scSign.Location = new System.Drawing.Point(3, 3);
            this.scSign.Name = "scSign";
            this.scSign.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scSign.Panel1
            // 
            this.scSign.Panel1.Controls.Add(this.txtSignInput);
            this.scSign.Panel1.Controls.Add(this.txtSignKey);
            // 
            // scSign.Panel2
            // 
            this.scSign.Panel2.Controls.Add(this.cbSignType);
            this.scSign.Panel2.Controls.Add(this.txtSignOutput);
            this.scSign.Size = new System.Drawing.Size(498, 256);
            this.scSign.SplitterDistance = 171;
            this.scSign.TabIndex = 3;
            // 
            // txtSignInput
            // 
            this.txtSignInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSignInput.Location = new System.Drawing.Point(0, 0);
            this.txtSignInput.Name = "txtSignInput";
            this.txtSignInput.ReadOnly = false;
            this.txtSignInput.Size = new System.Drawing.Size(498, 84);
            this.txtSignInput.TabIndex = 0;
            this.txtSignInput.TextBackColor = System.Drawing.SystemColors.Window;
            this.txtSignInput.Title = "Input";
            this.txtSignInput.ViewEncodingType = Netpay.Crypt.UI.ctlEncodedText.EncodingType.PlainText;
            this.txtSignInput.Changed += new System.EventHandler(this.txtSign_Changed);
            // 
            // txtSignKey
            // 
            this.txtSignKey.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtSignKey.Location = new System.Drawing.Point(0, 84);
            this.txtSignKey.Name = "txtSignKey";
            this.txtSignKey.ReadOnly = false;
            this.txtSignKey.Size = new System.Drawing.Size(498, 87);
            this.txtSignKey.TabIndex = 1;
            this.txtSignKey.TextBackColor = System.Drawing.SystemColors.Window;
            this.txtSignKey.Title = "HMAC Key";
            this.txtSignKey.ViewEncodingType = Netpay.Crypt.UI.ctlEncodedText.EncodingType.PlainText;
            this.txtSignKey.Changed += new System.EventHandler(this.txtSign_Changed);
            // 
            // cbSignType
            // 
            this.cbSignType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSignType.FormattingEnabled = true;
            this.cbSignType.Items.AddRange(new object[] {
            "SHA1",
            "MD5",
            "SHA256",
            "SHA384",
            "SHA512",
            "HMACMD5",
            "HMACRIPEMD160",
            "HMACSHA1",
            "HMACSHA256",
            "HMACSHA384",
            "HMACSHA512",
            "MACTripleDES"});
            this.cbSignType.Location = new System.Drawing.Point(96, 3);
            this.cbSignType.Name = "cbSignType";
            this.cbSignType.Size = new System.Drawing.Size(109, 21);
            this.cbSignType.TabIndex = 0;
            this.cbSignType.SelectedIndexChanged += new System.EventHandler(this.cbSignType_SelectedIndexChanged);
            // 
            // txtSignOutput
            // 
            this.txtSignOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSignOutput.Location = new System.Drawing.Point(0, 0);
            this.txtSignOutput.Name = "txtSignOutput";
            this.txtSignOutput.ReadOnly = true;
            this.txtSignOutput.Size = new System.Drawing.Size(498, 81);
            this.txtSignOutput.TabIndex = 1;
            this.txtSignOutput.TextBackColor = System.Drawing.SystemColors.Window;
            this.txtSignOutput.Title = "Output Signature";
            this.txtSignOutput.ViewEncodingType = Netpay.Crypt.UI.ctlEncodedText.EncodingType.Base64;
            // 
            // rbEncrypt
            // 
            this.rbEncrypt.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbEncrypt.Checked = true;
            this.rbEncrypt.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rbEncrypt.Location = new System.Drawing.Point(8, 28);
            this.rbEncrypt.Name = "rbEncrypt";
            this.rbEncrypt.Size = new System.Drawing.Size(112, 23);
            this.rbEncrypt.TabIndex = 1;
            this.rbEncrypt.TabStop = true;
            this.rbEncrypt.Text = "&Encrypt";
            this.rbEncrypt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbEncrypt.UseVisualStyleBackColor = true;
            // 
            // rbDeccrypt
            // 
            this.rbDeccrypt.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbDeccrypt.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rbDeccrypt.Location = new System.Drawing.Point(126, 28);
            this.rbDeccrypt.Name = "rbDeccrypt";
            this.rbDeccrypt.Size = new System.Drawing.Size(112, 23);
            this.rbDeccrypt.TabIndex = 2;
            this.rbDeccrypt.Text = "&Decrypt";
            this.rbDeccrypt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbDeccrypt.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Select Mode";
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.label2);
            this.pnlTop.Controls.Add(this.rbDeccrypt);
            this.pnlTop.Controls.Add(this.btnManage);
            this.pnlTop.Controls.Add(this.label1);
            this.pnlTop.Controls.Add(this.rbEncrypt);
            this.pnlTop.Controls.Add(this.cbKeyIndex);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(528, 57);
            this.pnlTop.TabIndex = 0;
            // 
            // pnlBody
            // 
            this.pnlBody.Controls.Add(this.tbContainer);
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.Location = new System.Drawing.Point(0, 57);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Padding = new System.Windows.Forms.Padding(8);
            this.pnlBody.Size = new System.Drawing.Size(528, 304);
            this.pnlBody.TabIndex = 1;
            // 
            // frmEncDec
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 361);
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.pnlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(480, 360);
            this.Name = "frmEncDec";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Netpay.Crypt UI";
            this.tbContainer.ResumeLayout(false);
            this.tbFiles.ResumeLayout(false);
            this.scText.Panel1.ResumeLayout(false);
            this.scText.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scText)).EndInit();
            this.scText.ResumeLayout(false);
            this.tbText.ResumeLayout(false);
            this.pnlFiles.ResumeLayout(false);
            this.pnlFiles.PerformLayout();
            this.tbSignature.ResumeLayout(false);
            this.scSign.Panel1.ResumeLayout(false);
            this.scSign.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scSign)).EndInit();
            this.scSign.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlBody.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Label lblSource;
		private System.Windows.Forms.Label lblTarget;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button btnSelectTarget;
		private System.Windows.Forms.Button btnSelectSource;
		private System.Windows.Forms.ComboBox cbKeyIndex;
		private System.Windows.Forms.CheckBox chkUseBase64TextFile;
		private System.Windows.Forms.TextBox txtSource;
		private System.Windows.Forms.TextBox txtTarget;
        private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RadioButton rbFolder;
		private System.Windows.Forms.RadioButton rbFile;
        private System.Windows.Forms.Button btnManage;
        private System.Windows.Forms.TabControl tbContainer;
        private System.Windows.Forms.TabPage tbText;
        private System.Windows.Forms.TabPage tbFiles;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbEncrypt;
        private System.Windows.Forms.RadioButton rbDeccrypt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.SplitContainer scText;
        private ctlEncodedText txtInput;
        private ctlEncodedText txtOutput;
        private System.Windows.Forms.Panel pnlFiles;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.TabPage tbSignature;
        private System.Windows.Forms.SplitContainer scSign;
        private ctlEncodedText txtSignInput;
        private System.Windows.Forms.ComboBox cbSignType;
        private ctlEncodedText txtSignOutput;
        private ctlEncodedText txtSignKey;
	}
}