﻿namespace Netpay.Crypt.UI
{
	partial class frmKeyManage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnSaveFiles = new System.Windows.Forms.Button();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPath = new System.Windows.Forms.ComboBox();
            this.cbKeyIndex = new System.Windows.Forms.ComboBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbKeyType = new System.Windows.Forms.ComboBox();
            this.cbKeySize = new System.Windows.Forms.ComboBox();
            this.chSplitKey = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkImportEncrypted = new System.Windows.Forms.CheckBox();
            this.btnDefaultProd = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbDPAPI = new System.Windows.Forms.ComboBox();
            this.btnDefaultDev = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cbKeyMode = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbKeyPadding = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbBlockSize = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnDeriveKey = new System.Windows.Forms.Button();
            this.txtIV = new Netpay.Crypt.UI.ctlEncodedText();
            this.txtKey = new Netpay.Crypt.UI.ctlEncodedText();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSaveFiles
            // 
            this.btnSaveFiles.Location = new System.Drawing.Point(312, 21);
            this.btnSaveFiles.Name = "btnSaveFiles";
            this.btnSaveFiles.Size = new System.Drawing.Size(146, 24);
            this.btnSaveFiles.TabIndex = 2;
            this.btnSaveFiles.Text = "&Load and save key files...";
            this.btnSaveFiles.UseVisualStyleBackColor = true;
            this.btnSaveFiles.Click += new System.EventHandler(this.btnSaveFiles_Click);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(20, 21);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(107, 24);
            this.btnGenerate.TabIndex = 0;
            this.btnGenerate.Text = "&Generate New Key";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbPath);
            this.groupBox1.Controls.Add(this.cbKeyIndex);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.btnView);
            this.groupBox1.Controls.Add(this.btnImport);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(17, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(480, 89);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Default Key Store";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Key Index";
            // 
            // cbPath
            // 
            this.cbPath.FormattingEnabled = true;
            this.cbPath.Location = new System.Drawing.Point(75, 26);
            this.cbPath.Name = "cbPath";
            this.cbPath.Size = new System.Drawing.Size(383, 21);
            this.cbPath.TabIndex = 1;
            this.cbPath.TextChanged += new System.EventHandler(this.cbPath_TextChanged);
            // 
            // cbKeyIndex
            // 
            this.cbKeyIndex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKeyIndex.FormattingEnabled = true;
            this.cbKeyIndex.Location = new System.Drawing.Point(75, 54);
            this.cbKeyIndex.Name = "cbKeyIndex";
            this.cbKeyIndex.Size = new System.Drawing.Size(62, 21);
            this.cbKeyIndex.TabIndex = 3;
            this.cbKeyIndex.SelectedIndexChanged += new System.EventHandler(this.cbKeyIndex_SelectedIndexChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(251, 53);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 23);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Delete Key";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(144, 53);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(100, 23);
            this.btnView.TabIndex = 4;
            this.btnView.Text = "&Show key";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(358, 53);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(100, 23);
            this.btnImport.TabIndex = 6;
            this.btnImport.Text = "&Import key";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "&Path";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Key &Type";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Key &Size";
            // 
            // cbKeyType
            // 
            this.cbKeyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKeyType.FormattingEnabled = true;
            this.cbKeyType.Location = new System.Drawing.Point(81, 27);
            this.cbKeyType.Name = "cbKeyType";
            this.cbKeyType.Size = new System.Drawing.Size(101, 21);
            this.cbKeyType.TabIndex = 1;
            this.cbKeyType.SelectedIndexChanged += new System.EventHandler(this.cbKeyType_SelectedIndexChanged);
            // 
            // cbKeySize
            // 
            this.cbKeySize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKeySize.FormattingEnabled = true;
            this.cbKeySize.Location = new System.Drawing.Point(81, 56);
            this.cbKeySize.Name = "cbKeySize";
            this.cbKeySize.Size = new System.Drawing.Size(101, 21);
            this.cbKeySize.TabIndex = 3;
            // 
            // chSplitKey
            // 
            this.chSplitKey.AutoSize = true;
            this.chSplitKey.Location = new System.Drawing.Point(204, 58);
            this.chSplitKey.Name = "chSplitKey";
            this.chSplitKey.Size = new System.Drawing.Size(272, 17);
            this.chSplitKey.TabIndex = 13;
            this.chSplitKey.Text = "Store in two splited places (registry and disk/registry)";
            this.chSplitKey.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkImportEncrypted);
            this.groupBox2.Controls.Add(this.btnDefaultProd);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cbDPAPI);
            this.groupBox2.Controls.Add(this.btnDefaultDev);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cbKeyMode);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cbKeyPadding);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cbBlockSize);
            this.groupBox2.Controls.Add(this.chSplitKey);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cbKeyType);
            this.groupBox2.Controls.Add(this.cbKeySize);
            this.groupBox2.Location = new System.Drawing.Point(17, 170);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(480, 169);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Key Properties";
            // 
            // chkImportEncrypted
            // 
            this.chkImportEncrypted.AutoSize = true;
            this.chkImportEncrypted.Location = new System.Drawing.Point(204, 85);
            this.chkImportEncrypted.Name = "chkImportEncrypted";
            this.chkImportEncrypted.Size = new System.Drawing.Size(262, 17);
            this.chkImportEncrypted.TabIndex = 14;
            this.chkImportEncrypted.Text = "Import as encrypted data (using internal password)";
            this.chkImportEncrypted.UseVisualStyleBackColor = true;
            // 
            // btnDefaultProd
            // 
            this.btnDefaultProd.Location = new System.Drawing.Point(204, 109);
            this.btnDefaultProd.Name = "btnDefaultProd";
            this.btnDefaultProd.Size = new System.Drawing.Size(254, 24);
            this.btnDefaultProd.TabIndex = 15;
            this.btnDefaultProd.Text = "Set default &Production settings";
            this.btnDefaultProd.UseVisualStyleBackColor = true;
            this.btnDefaultProd.Click += new System.EventHandler(this.btnDefaultProd_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(362, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "(DPAPI Key and IV)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(201, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Protect";
            // 
            // cbDPAPI
            // 
            this.cbDPAPI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDPAPI.FormattingEnabled = true;
            this.cbDPAPI.Location = new System.Drawing.Point(255, 27);
            this.cbDPAPI.Name = "cbDPAPI";
            this.cbDPAPI.Size = new System.Drawing.Size(101, 21);
            this.cbDPAPI.TabIndex = 11;
            // 
            // btnDefaultDev
            // 
            this.btnDefaultDev.Location = new System.Drawing.Point(204, 137);
            this.btnDefaultDev.Name = "btnDefaultDev";
            this.btnDefaultDev.Size = new System.Drawing.Size(254, 24);
            this.btnDefaultDev.TabIndex = 16;
            this.btnDefaultDev.Text = "Set default Development settings";
            this.btnDefaultDev.UseVisualStyleBackColor = true;
            this.btnDefaultDev.Click += new System.EventHandler(this.btnDefaultDev_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Mode";
            // 
            // cbKeyMode
            // 
            this.cbKeyMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKeyMode.FormattingEnabled = true;
            this.cbKeyMode.Location = new System.Drawing.Point(81, 140);
            this.cbKeyMode.Name = "cbKeyMode";
            this.cbKeyMode.Size = new System.Drawing.Size(101, 21);
            this.cbKeyMode.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Padding";
            // 
            // cbKeyPadding
            // 
            this.cbKeyPadding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKeyPadding.FormattingEnabled = true;
            this.cbKeyPadding.Location = new System.Drawing.Point(81, 112);
            this.cbKeyPadding.Name = "cbKeyPadding";
            this.cbKeyPadding.Size = new System.Drawing.Size(101, 21);
            this.cbKeyPadding.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Block S&ize";
            // 
            // cbBlockSize
            // 
            this.cbBlockSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBlockSize.FormattingEnabled = true;
            this.cbBlockSize.Location = new System.Drawing.Point(81, 84);
            this.cbBlockSize.Name = "cbBlockSize";
            this.cbBlockSize.Size = new System.Drawing.Size(101, 21);
            this.cbBlockSize.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnDeriveKey);
            this.groupBox3.Controls.Add(this.btnSaveFiles);
            this.groupBox3.Controls.Add(this.btnGenerate);
            this.groupBox3.Location = new System.Drawing.Point(17, 107);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(480, 58);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Key Generate / Load";
            // 
            // btnDeriveKey
            // 
            this.btnDeriveKey.Location = new System.Drawing.Point(133, 21);
            this.btnDeriveKey.Name = "btnDeriveKey";
            this.btnDeriveKey.Size = new System.Drawing.Size(173, 24);
            this.btnDeriveKey.TabIndex = 1;
            this.btnDeriveKey.Text = "&Generate Key with password...";
            this.btnDeriveKey.UseVisualStyleBackColor = true;
            this.btnDeriveKey.Click += new System.EventHandler(this.btnDeriveKey_Click);
            // 
            // txtIV
            // 
            this.txtIV.Location = new System.Drawing.Point(17, 457);
            this.txtIV.Name = "txtIV";
            this.txtIV.ReadOnly = false;
            this.txtIV.Size = new System.Drawing.Size(480, 107);
            this.txtIV.TabIndex = 4;
            this.txtIV.TextBackColor = System.Drawing.SystemColors.Window;
            this.txtIV.Title = "Initialization Vector (IV)";
            this.txtIV.ViewEncodingType = Netpay.Crypt.UI.ctlEncodedText.EncodingType.Hex;
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(17, 345);
            this.txtKey.Name = "txtKey";
            this.txtKey.ReadOnly = false;
            this.txtKey.Size = new System.Drawing.Size(480, 107);
            this.txtKey.TabIndex = 3;
            this.txtKey.TextBackColor = System.Drawing.SystemColors.Window;
            this.txtKey.Title = "Key";
            this.txtKey.ViewEncodingType = Netpay.Crypt.UI.ctlEncodedText.EncodingType.Hex;
            // 
            // frmKeyManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 582);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.txtIV);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmKeyManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Key Manager";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmKeyManage_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btnView;
		private System.Windows.Forms.Button btnGenerate;
		private System.Windows.Forms.Button btnImport;
		private System.Windows.Forms.ComboBox cbKeyIndex;
		private System.Windows.Forms.Button btnSaveFiles;
        private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbKeyType;
        private System.Windows.Forms.ComboBox cbKeySize;
        private ctlEncodedText txtKey;
        private ctlEncodedText txtIV;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cbPath;
        private System.Windows.Forms.CheckBox chSplitKey;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbKeyMode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbKeyPadding;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbBlockSize;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnDeriveKey;
        private System.Windows.Forms.Button btnDefaultDev;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbDPAPI;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkImportEncrypted;
        private System.Windows.Forms.Button btnDefaultProd;
	}
}