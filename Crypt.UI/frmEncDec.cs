﻿/* UI interface to Netpay.Crypt Cryptography By Udi Azulay 2015-03-09 */
using System;
using System.Windows.Forms;

namespace Netpay.Crypt.UI
{
    public partial class frmEncDec : Form
	{
		public bool IsFolder { get { return rbFolder.Checked; } }
		public bool IsEncrypt { get { return rbEncrypt.Checked; } }

		public string Source { get { return txtSource.Text; } set { txtSource.Text = value; } }
		public string Target { get { return txtTarget.Text; } set { txtTarget.Text = value; } }

		public frmEncDec() 
		{ 
			InitializeComponent();
            UpdateAvailableKeys();
            cbSignType.SelectedIndex = 0;
        }

        public int KeyIndex { get { return cbKeyIndex.SelectedIndex; } }

		private void btnSelectSource_Click(object sender, EventArgs e)
		{
			if (IsFolder) {
				if (folderBrowserDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.OK) return;
				txtSource.Text = folderBrowserDialog1.SelectedPath;
			} else {
				openFileDialog1.CheckFileExists = true;
				openFileDialog1.DefaultExt = IsEncrypt ? null : "dpa";
				if (openFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.OK) return;
				txtSource.Text = openFileDialog1.FileName;
			}
		}

		private void btnSelectTarget_Click(object sender, EventArgs e)
		{
			if (IsFolder) {
				if (folderBrowserDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.OK) return;
				txtTarget.Text = folderBrowserDialog1.SelectedPath;
			} else {
				saveFileDialog1.DefaultExt = IsEncrypt ? "dpa" : null;
				if (saveFileDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.OK) return;
				txtTarget.Text = saveFileDialog1.FileName;
			}
		}

        private void UpdateAvailableKeys() 
        {
            cbKeyIndex.Items.Clear();
            cbKeyIndex.Items.Add("None");
            cbKeyIndex.SelectedIndex = 0;
            for (int i = 1; i <= 255; i++) cbKeyIndex.Items.Add(i.ToString());
            var items = Netpay.Crypt.SymEncryption.EnumAvaiableKeyIndexes();
            foreach (var v in items) cbKeyIndex.Items[v] = v + " (Exist)";
        }

		private void btnManage_Click(object sender, EventArgs e)
		{
			var keyView = new frmKeyManage();
			keyView.ShowDialog(this);
            UpdateAvailableKeys();
		}

		private void btnStart_Click(object sender, EventArgs e)
		{
			string[] files;
            if (cbKeyIndex.SelectedIndex == 0) { MessageBox.Show(this, "KeyIndex not selected"); return; }
			if (IsFolder) files = System.IO.Directory.GetFiles(Source);
			else files = new string[] { Source };
            using (var enc = Netpay.Crypt.SymEncryption.FromKeyIndex(KeyIndex)) { 
			    foreach (var file in files) 
			    {
				    string outputFile = IsFolder ? System.IO.Path.Combine(Target, System.IO.Path.GetFileName(file)) : Target;
                    try {
#pragma warning disable 0612
                        if (IsEncrypt) {
                            if (chkUseBase64TextFile.Checked) enc.EncryptBase64File(file, outputFile);
                            else enc.EncryptFile(file, outputFile);
                        } else {
                            if (chkUseBase64TextFile.Checked) enc.DecryptBase64File(file, outputFile);
                            else enc.DecryptFile(file, outputFile);
                        }
#pragma warning disable 0612
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, "Error on file:" + file + "\r\n" + ex.Message);
                    }
			    }
            }
            MessageBox.Show(this, "Completed");
		}

        private void txtInput_Changed(object sender, EventArgs e)
        {
            try { 
                if (cbKeyIndex.SelectedIndex > 0) {
                    using (var crypt = Netpay.Crypt.SymEncryption.FromKeyIndex(KeyIndex)) { 
                        if (IsEncrypt) txtOutput.Blob = crypt.Encrypt(txtInput.Blob);
                        else if (txtInput.Text.Length > 0) txtOutput.Blob = crypt.Decrypt(txtInput.Blob);
                    }
                } else txtOutput.Blob = txtInput.Blob;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void cbKeyIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtInput_Changed(sender, e);
        }

        private void txtSign_Changed(object sender, EventArgs e)
        {
            try
            {
                if (cbSignType.Text.IndexOf("MAC", StringComparison.InvariantCultureIgnoreCase) > -1)
                {
                    using (var crypt = Netpay.Crypt.Hmac.Create(cbSignType.Text))
                    {
                        crypt.Key = txtSignKey.Blob;
                        txtSignOutput.Blob = crypt.Calculate(txtSignInput.Blob);
                    }
                }
                else txtSignOutput.Blob = txtSignInput.Blob.Hash(cbSignType.Text);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void cbSignType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSignKey.ReadOnly = !(cbSignType.Text.IndexOf("MAC", StringComparison.InvariantCultureIgnoreCase) > -1);
            txtSignKey.TextBackColor =  txtSignKey.ReadOnly ? System.Drawing.SystemColors.Control : System.Drawing.SystemColors.Window;
            txtSign_Changed(sender, e);
        }

	}
}
