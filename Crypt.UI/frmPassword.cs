﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

namespace Netpay.Crypt.UI
{
	public partial class frmPassword : Form
	{
		public frmPassword() { InitializeComponent(); }

		public static DialogResult GetPassword(IWin32Window owner, out string password)
		{
			password = null;
            var frm = new frmPassword();
			frm.txtPassword.Text = "";
            if (frm.ShowDialog(owner) != System.Windows.Forms.DialogResult.OK) return DialogResult.Cancel;
            password = frm.txtPassword.Text;
			return System.Windows.Forms.DialogResult.OK;
		}

	}
}
