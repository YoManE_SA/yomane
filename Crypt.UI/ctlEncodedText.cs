﻿/* UI interface to Netpay.Crypt Cryptography By Udi Azulay 2015-03-09 */
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Netpay.Crypt.UI
{
    public partial class ctlEncodedText : UserControl
    {
        private Netpay.Crypt.Blob _blob;
        private bool onChange = false;
        public enum EncodingType { PlainText, Hex, Base64, Ascii, UTF8, Unicode, Windows1255 }
        public event EventHandler Changed;
        public ctlEncodedText() { InitializeComponent(); cmbEncoding.SelectedIndex = 0; _blob = new Netpay.Crypt.Blob(); }

        public string Title { get { return lblTitle.Text; } set { lblTitle.Text = value; } }
        public EncodingType ViewEncodingType { get { return (EncodingType)cmbEncoding.SelectedIndex; } set { cmbEncoding.SelectedIndex = (int)value; } }
        public bool IsEmpty { get { return string.IsNullOrEmpty(txtText.Text); } }
        public bool ReadOnly { get { return txtText.ReadOnly; } set { txtText.ReadOnly = value; } }
        public System.Drawing.Color TextBackColor { get { return txtText.BackColor; } set { txtText.BackColor = value; } }

        [Browsable(false), BindableAttribute(false), DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public Netpay.Crypt.Blob Blob
        {
            get {
                return _blob;
            }
            set {
                onChange = true;
                _blob = value;
                if (_blob == null) _blob = new Crypt.Blob();
                switch (ViewEncodingType)
                {
                    case EncodingType.PlainText: txtText.Text = _blob.Text; break;
                    case EncodingType.Base64: txtText.Text = _blob.Base64; break;
                    case EncodingType.Hex: txtText.Text = _blob.Hex; break;
                    case EncodingType.Ascii: txtText.Text = _blob.Ascii; break;
                    case EncodingType.UTF8: txtText.Text = _blob.UTF8; break;
                    case EncodingType.Unicode: txtText.Text = _blob.Unicode; break;
                    case EncodingType.Windows1255: txtText.Text = _blob.Windows1255; break;
                }
                onChange = false;
            }
        }

        public override string Text { get { return _blob.Text; } set { _blob.Text = value; } }

        private void Encoding_Changed(object sender, EventArgs e)
        {
            Blob = Blob;
        }

        private void Text_Changed(object sender, EventArgs e)
        {
            txtSize.Text = txtText.Text.Length.ToString();
            if (!onChange) { 
                try
                {
                    switch (ViewEncodingType)
                    {
                        case EncodingType.PlainText: _blob.Text = txtText.Text; break;
                        case EncodingType.Base64: _blob.Base64 = txtText.Text; break;
                        case EncodingType.Hex: _blob.Hex = txtText.Text; break;
                        case EncodingType.Ascii: _blob.Ascii = txtText.Text; break;
                        case EncodingType.UTF8: _blob.UTF8 = txtText.Text; break;
                        case EncodingType.Unicode: _blob.Unicode = txtText.Text; break;
                        case EncodingType.Windows1255: _blob.Windows1255 = txtText.Text; break;
                    }
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }
            }
            if (Changed != null) Changed(this, e);
        }
    }
}
