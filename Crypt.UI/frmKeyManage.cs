﻿/* UI interface to Netpay.Crypt Cryptography By Udi Azulay 2015-03-09 */
using System;
using System.ComponentModel;
using System.Windows.Forms;
using Netpay.Crypt;

namespace Netpay.Crypt.UI
{
	public partial class frmKeyManage : Form
	{
        private bool ignoreKeyOnSet  = false;
		public frmKeyManage() 
		{
			InitializeComponent();
            AddKeyStoreHistory(Netpay.Crypt.SymEncryption.DefaultKeyStore);
            AddKeyStoreHistory(@"c:\keys\");
            AddKeyStoreHistory(@"Registry://HKEY_CLASSES_ROOT\Netpay\Crypt\Keys");
            cbPath.SelectedIndex = 0;
            foreach (var v in Netpay.Crypt.SymEncryption.GetProviders) cbKeyType.Items.Add(v);
            cbKeyPadding.DataSource = Enum.GetValues(typeof(System.Security.Cryptography.PaddingMode));
            cbKeyMode.DataSource = Enum.GetValues(typeof(System.Security.Cryptography.CipherMode));
            cbDPAPI.DataSource = Enum.GetValues(typeof(Netpay.Crypt.ProtectScope));
            if (cbKeyType.Items.Count > 0) cbKeyType.SelectedIndex = 0;
            cbDPAPI.Text = Netpay.Crypt.ProtectScope.None.ToString();
            //btnDefaultProd_Click(this, EventArgs.Empty);
        }

        private void AddKeyStoreHistory(string path, bool selected = false)
        {
            if (!cbPath.Items.Contains(path)) cbPath.Items.Add(path);
            if (selected) cbPath.Text = path;
        }

        public Netpay.Crypt.SymEncryption SymEncryption {
            get {
                var ret = new Netpay.Crypt.SymEncryption(cbKeyType.Text);
                ret.Mode = (System.Security.Cryptography.CipherMode) Enum.Parse(typeof(System.Security.Cryptography.CipherMode), cbKeyMode.Text);
                ret.Padding = (System.Security.Cryptography.PaddingMode) Enum.Parse(typeof(System.Security.Cryptography.PaddingMode), cbKeyPadding.Text);
                ret.ProtectScope = (Netpay.Crypt.ProtectScope)Enum.Parse(typeof(Netpay.Crypt.ProtectScope), cbDPAPI.Text);
                ret.Compose2Parts = chSplitKey.Checked;
                if (!string.IsNullOrEmpty(cbKeySize.Text)) ret.KeySize = int.Parse(cbKeySize.Text);
                if (!string.IsNullOrEmpty(cbBlockSize.Text)) ret.BlockSize = int.Parse(cbBlockSize.Text);
                if (!txtKey.IsEmpty) ret.Key = txtKey.Blob;
                if (!txtIV.IsEmpty) ret.IV = txtIV.Blob;
                return ret;
            }
            set
            {
                if (cbKeyType.FindStringExact(value.ProviderName, 0) < 0)
                    cbKeyType.Items.Add(value.ProviderName);
                cbKeyType.Text = value.ProviderName;
                cbKeySize.Text = value.KeySize.ToString();
                cbBlockSize.Text = value.BlockSize.ToString();
                cbKeyPadding.Text = value.Padding.ToString();
                cbKeyMode.Text = value.Mode.ToString();
                cbDPAPI.Text = value.ProtectScope.ToString();
                chSplitKey.Checked = value.Compose2Parts;
                if (!ignoreKeyOnSet) {
                    txtKey.Blob = value.Key;
                    txtIV.Blob = value.IV;
                }
            }
        }

        private void cbKeyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var symEncryption = new Netpay.Crypt.SymEncryption(cbKeyType.Text);
            cbKeySize.DataSource = symEncryption.AvailableKeySize;
            cbBlockSize.DataSource = symEncryption.AvailableBlockSize;
        }

        public int KeyIndex { get { return cbKeyIndex.SelectedIndex + 1; } set { cbKeyIndex.SelectedIndex = value - 1; } }

		private void btnGenerate_Click(object sender, EventArgs e)
		{
            txtIV.Blob = txtKey.Blob = new Blob();
			cbKeyIndex.SelectedIndex = -1;
            try {
                var obj = SymEncryption;
                obj.GenerateIV();
                obj.GenerateKey();
                SymEncryption = obj;
            } catch(Exception ex) {
                MessageBox.Show("Could not create key from properties, check key type, size - " + ex.Message);
            }
		}

        private void btnDeriveKey_Click(object sender, EventArgs e)
        {
            string password;
            if(frmPassword.GetPassword(this, out password) != System.Windows.Forms.DialogResult.OK) return;
            txtIV.Blob = txtKey.Blob = new Blob();
            cbKeyIndex.SelectedIndex = -1;
            try {
                var obj = SymEncryption;
                obj.GenerateWithPassword(password);
                SymEncryption = obj;
            } catch(Exception ex) {
                MessageBox.Show("Could not create key from properties, check key type, size - " + ex.Message);
            }
        }

		private void btnImport_Click(object sender, EventArgs e)
		{
            if (cbKeyIndex.SelectedIndex < 0) { MessageBox.Show(this, "KeyIndex not selected"); return; }
			if (System.IO.File.Exists(Netpay.Crypt.SymEncryption.MapFileName(KeyIndex)))
				if(MessageBox.Show(this, "File already exist, would you like to overwrite?", Application.ProductName, MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK) return;
            try {
                var obj = SymEncryption;
                obj.SaveToFile(Netpay.Crypt.SymEncryption.MapFileName(KeyIndex), chkImportEncrypted.Checked ? KeyFileMode.EncryptedFile : KeyFileMode.TextFile);
                MessageBox.Show(this, "Key Import Completed");
                var keyIndex = KeyIndex;
                cbPath_TextChanged(sender, e);
                KeyIndex = keyIndex;

            } catch(Exception ex) {
                MessageBox.Show(this, "Key not saved - " + ex.Message); 
            }
		}

		private void btnShow_Click(object sender, EventArgs e)
		{
			if (cbKeyIndex.SelectedIndex < 0) { MessageBox.Show(this, "KeyIndex not selected"); return; }
            try { SymEncryption = Netpay.Crypt.SymEncryption.FromKeyIndex(KeyIndex); }
            catch (Exception ex) { MessageBox.Show("Error on load key:\r\n" + ex.Message); }
		}

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (cbKeyIndex.SelectedIndex < 0) { MessageBox.Show(this, "KeyIndex not selected"); return; }
            if (MessageBox.Show("Delete Key " + KeyIndex + " From store?", "Key Store Managment", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK) return;
            try { Netpay.Crypt.SymEncryption.DeleteFile(Netpay.Crypt.SymEncryption.MapFileName(KeyIndex)); } 
            catch(Exception ex) { MessageBox.Show(ex.Message); }
            cbPath_TextChanged(sender, e);
        }

        private void btnSaveFiles_Click(object sender, EventArgs e)
        {
            (new frmSplitKey()).Show(this);
        }

        private void cbPath_TextChanged(object sender, EventArgs e)
        {
            cbKeyIndex.SelectedIndex = -1;
            cbKeyIndex.Items.Clear();
            Netpay.Crypt.SymEncryption.DefaultKeyStore = cbPath.Text;
            AddKeyStoreHistory(cbPath.Text, true);
            for (int i = 1; i <= 255; i++) cbKeyIndex.Items.Add(i.ToString());
            var items = Netpay.Crypt.SymEncryption.EnumAvaiableKeyIndexes(cbPath.Text);
            foreach (var v in items) cbKeyIndex.Items[v - 1] = v + " (Exist)";
            cbKeyIndex_SelectedIndexChanged(sender, e);
        }

        private void frmKeyManage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) this.Close();
        }

        private void btnDefaultDev_Click(object sender, EventArgs e)
        {
            ignoreKeyOnSet = true;
            SymEncryption = new Crypt.SymEncryption("Rijndael") { KeySize = 128, BlockSize = 128, Padding = System.Security.Cryptography.PaddingMode.PKCS7, Mode = System.Security.Cryptography.CipherMode.CBC, Compose2Parts = false, ProtectScope = ProtectScope.None };
            chkImportEncrypted.Checked = false;
            ignoreKeyOnSet = false;
        }
        private void btnDefaultProd_Click(object sender, EventArgs e)
        {
            ignoreKeyOnSet = true;
            if (cbPath.Text.StartsWith("Registry://"))
                if (MessageBox.Show(this, "For production mode it's better to use a disk keystore and the \"Store splitted\" option.\r\nthis will ensure the key is splited between disk and registry.\r\nContinue without change?", "Warning", MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes) return;
            SymEncryption = new Crypt.SymEncryption("Rijndael") { KeySize = 256, BlockSize = 128, Padding = System.Security.Cryptography.PaddingMode.PKCS7, Mode = System.Security.Cryptography.CipherMode.CBC, Compose2Parts = true, ProtectScope = ProtectScope.LocalMachine };
            chkImportEncrypted.Checked = true;
            ignoreKeyOnSet = false;
        }

        private void cbKeyIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnView.Enabled = btnDelete.Enabled = cbKeyIndex.Text.Contains("Exist");
            btnImport.Enabled = cbKeyIndex.SelectedIndex != -1;
        }


	}
}
