﻿/* UI interface to Netpay.Crypt Cryptography By Udi Azulay 2015-03-09 */
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Netpay.Crypt.UI
{
	public partial class frmSplitKey : Form
	{
		public frmSplitKey() 
        { 
            InitializeComponent();
            txtPath.Text = @"c:\temp\Split\KeyName.keypart";
            for (int i = 1; i <= 10; i++) cbPartCount.Items.Add(i.ToString());
            cbPartCount.SelectedIndex = 0;
        }
        private frmKeyManage _partFrom;
        public int PartCount { get { return int.Parse(cbPartCount.Text); } }
        public void Show(frmKeyManage partFrom) 
        {
            _partFrom = partFrom;
            this.ShowDialog(partFrom);
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            string path = txtPath.Text;
            string password = null;
            var fileMode = chkEncrypt.Checked ? Netpay.Crypt.KeyFileMode.EncryptedFile : Netpay.Crypt.KeyFileMode.TextFile;
            try
            {
                if (rbSplit.Checked) { 
                    if (chkPassword.Checked && frmPassword.GetPassword(this, out password) != System.Windows.Forms.DialogResult.OK) return;
                    if (cbPartCount.SelectedIndex == 0) _partFrom.SymEncryption.SaveToFile(path, fileMode, password);
                    else _partFrom.SymEncryption.Split(path, PartCount, fileMode, password);
                } else {
                    bool needpassword = false;
                    do {
                        try {
                            needpassword = false; 
                            if (cbPartCount.SelectedIndex == 0) _partFrom.SymEncryption = Netpay.Crypt.SymEncryption.FromFile(path, password);
                            else {
                                var tmp = new Netpay.Crypt.SymEncryption();
                                tmp.Join(path, PartCount, password);
                                _partFrom.SymEncryption = tmp;
                            }
                        } catch (IncorrectPassword) { 
                            if (frmPassword.GetPassword(this, out password) != System.Windows.Forms.DialogResult.OK) return;
                            needpassword = true; 
                        }
                    } while (needpassword);
                }
                base.DialogResult = System.Windows.Forms.DialogResult.OK;
                base.Close();
            } catch(Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Key files (*.key;*.keypart)|*.key;*.keypart|All files (*.*)|*.*";
            openFileDialog1.DefaultExt = cbPartCount.SelectedIndex == 0 ? "key" : "keypart";
            if (openFileDialog1.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            if (cbPartCount.SelectedIndex > 0)
            {
                var filename = System.IO.Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
                filename = System.IO.Path.ChangeExtension(filename, openFileDialog1.DefaultExt);
                txtPath.Text = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(openFileDialog1.FileName), filename);
            } else txtPath.Text = openFileDialog1.FileName;

        }

        private void cbPartCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try { txtPath.Text = System.IO.Path.ChangeExtension(txtPath.Text, cbPartCount.SelectedIndex == 0 ? "key" : "keypart"); } catch { }
        }

        private void rbSplit_CheckedChanged(object sender, EventArgs e)
        {
            chkEncrypt.Enabled = chkPassword.Enabled = rbSplit.Checked;
        }

	}
}
