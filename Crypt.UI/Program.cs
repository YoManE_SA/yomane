﻿using System;
using System.Windows.Forms;
using System.Linq;

namespace Netpay.Crypt.UI
{
	static class Program
	{
        //[System.Runtime.InteropServices.DllImport("kernel32.dll")] private static extern bool FreeConsole();
        [System.Runtime.InteropServices.DllImport("kernel32.dll")] private static extern bool AttachConsole(int dwProcessId);
        private const int ATTACH_PARENT_PROCESS = -1;
        private static void ShowConsole() { AttachConsole(ATTACH_PARENT_PROCESS); }

		[STAThread]
        static void Main(string[] args)
		{
            if (args.Contains("/?") || args.Contains("?") || args.Contains("/help") || args.Contains("-help")) {
                ShowConsole();
                Console.WriteLine("\r\nNetpay.Crypt UI command mode actions:");
                Console.WriteLine("\t/?, /help - Prints this help.");
                Console.WriteLine("\t/manage - Open key managment screen.");
                Console.WriteLine("\t/enc [key] [src] [dest]: - Encrypt src (file or folder) to dest (file or folder) using key (index of fileName).");
                Console.WriteLine("\t/dec [key] [src] [dest]: - Decrypt src (file or folder) to dest (file or folder) using key (index of fileName).");
            } else if (args.Contains("/enc")) {
                ShowConsole();
                try { Netpay.Crypt.SymEncryption.GetKey(int.Parse(args[1])).EncryptFile(args[2], args[3]); } 
                catch(Exception ex) { Console.WriteLine("\r\nError: " + ex.Message); }
            } else if (args.Contains("/dec")) {
                ShowConsole();
                try { Netpay.Crypt.SymEncryption.GetKey(int.Parse(args[1])).DecryptFile(args[2], args[3]); }
                catch (Exception ex) { Console.WriteLine("\r\nError: " + ex.Message); }
            } else { 
			    Application.EnableVisualStyles();
			    Application.SetCompatibleTextRenderingDefault(false);
                if (args.Contains("/manage") || args.Contains("-manage")) Application.Run(new frmKeyManage() { StartPosition = FormStartPosition.CenterScreen });
                else Application.Run(new frmEncDec());
            }
		}
	}
}
