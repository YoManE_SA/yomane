﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Netpay.Dal.DataAccess
{
	public class ErrorNetDataContext : ErrorNetDataContextBase
	{
		public ErrorNetDataContext(string connection)
			: base(connection)
		{

		}

		public bool LogToDebugOutput { get; set; }
		public void SetupLogger(bool LogToDebugOutput = false)
		{
			if (LogToDebugOutput) this.Log = new DebugTextWriter();
			else this.Log = new System.IO.StringWriter(new StringBuilder());
		}
	}
}
