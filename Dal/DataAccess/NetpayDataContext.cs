﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.Linq.Mapping;
using System.Reflection;
using System.IO;
using Netpay.Dal.Netpay;

namespace Netpay.Dal.DataAccess
{
	public class DebugTextWriter : System.IO.TextWriter
	{
		public override void Write(char[] buffer, int index, int count) { Write(new String(buffer, index, count)); }
		public override void Write(string value) { System.Diagnostics.Debug.WriteLine("SQL:" + value); }
		public override Encoding Encoding { get { return System.Text.Encoding.Default; } }
	}

	public class NetpayDataContext : NetpayDataContextBase
	{

		public NetpayDataContext(string connection)
			: base(connection)
		{
			this.CommandTimeout = 120;
			//this.ExecuteCommand("set transaction isolation level read uncommitted"); 
		}
		public bool LogToDebugOutput { get; set;}
		public void SetupLogger(bool LogToDebugOutput = false)
		{
			if (LogToDebugOutput) this.Log = new DebugTextWriter();
			else this.Log = new StringWriter(new StringBuilder());	
		}

		public string Logged
		{
			get 
			{
                if (!(Log is StringWriter)) return string.Empty;
				return ((StringWriter)this.Log).GetStringBuilder().ToString();
			}
		}

		#region Function mappings
		[Function(Name = "dbo.IsCHB", IsComposable = true)]
		[return: Parameter(DbType = "bit")]
		public bool IsChb([Parameter(Name = "deniedStatus",
			DbType = "TinyInt")] byte @deniedStatus, [Parameter(Name = "isTestOnly",
			DbType = "bit")] bool @isTestOnly)
		{
			return ((bool)(this.ExecuteMethodCall(this,
				((MethodInfo)(MethodInfo.GetCurrentMethod())),
				@deniedStatus, @isTestOnly).ReturnValue));
		}	
		#endregion	
	}
}
