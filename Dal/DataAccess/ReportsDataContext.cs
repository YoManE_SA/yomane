﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Netpay.Dal.Reports;

namespace Netpay.Dal.DataAccess
{
	public class ReportsDataContext : ReportsDataContextBase
	{
		public ReportsDataContext(string connection)
			: base(connection)
		{
			//this.Log = new LogWriter();
			//this.Log = new DebuggerWriter();
			this.CommandTimeout = 120;
		}	
	}
}
