﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;

namespace Netpay.Dal.DataAccess
{
	public static class Extensions
	{
		public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string property)
		{
			return ApplyOrder<T>(source, property, "OrderBy");
		}

		public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string property)
		{
			return ApplyOrder<T>(source, property, "OrderByDescending");
		}

		public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string property)
		{
			return ApplyOrder<T>(source, property, "ThenBy");
		}

		public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source, string property)
		{
			return ApplyOrder<T>(source, property, "ThenByDescending");
		}

		private static IOrderedQueryable<T> ApplyOrder<T>(IQueryable<T> source, string property, string methodName)
		{
			string[] props = property.Split('.');
			Type type = typeof(T);
			ParameterExpression arg = Expression.Parameter(type, "entity");
			Expression expr = arg;

			foreach (string prop in props)
			{
				PropertyInfo pi = type.GetProperty(prop);
				expr = Expression.Property(expr, pi);
				type = pi.PropertyType;
			}

			Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
			LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);
			object result = typeof(Queryable).GetMethods().Single(method => method.Name == methodName && method.IsGenericMethodDefinition && method.GetGenericArguments().Length == 2 && method.GetParameters().Length == 2).MakeGenericMethod(typeof(T), type).Invoke(null, new object[] { source, lambda });

			return (IOrderedQueryable<T>)result;
		}

		public static IEnumerable<IGrouping<TKey, TSource>> GroupBy<TKey, TSource>(this IQueryable<TSource> source, string property)
		{
			return ApplyGrouping<TKey, TSource>(source, property, "GroupBy");
		}

		private static IEnumerable<IGrouping<TKey, TSource>> ApplyGrouping<TKey, TSource>(IQueryable<TSource> source, string property, string methodName)
		{
			string[] props = property.Split('.');
			Type type = typeof(TSource);
			ParameterExpression arg = Expression.Parameter(type, "entity");
			Expression expr = arg;

			foreach (string prop in props)
			{
				PropertyInfo pi = type.GetProperty(prop);
				expr = Expression.Property(expr, pi);
				type = pi.PropertyType;
			}

			Type delegateType = typeof(Func<,>).MakeGenericType(typeof(TSource), type);
			LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);
			object result = typeof(Queryable).GetMethods().Single(method => method.Name == methodName && method.IsGenericMethodDefinition && method.GetGenericArguments().Length == 2 && method.GetParameters().Length == 2).MakeGenericMethod(typeof(TSource), type).Invoke(null, new object[] { source, lambda });

			return (IEnumerable<IGrouping<TKey, TSource>>)result;
		}
	}
}
