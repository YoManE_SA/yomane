﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Dal.DataAccess
{
	public class ADO
	{
		/// <summary>
		/// Generates ADO.NET data reader for the specified query in the specified database
		/// </summary>
		/// <param name="sQuery">SQL query</param>
		/// <param name="sConn">Connection string</param>
		/// <param name="nTimeout">Command timeout (seconds), with default value of 30 seconds</param>
		/// <returns>Data reader containing the results of the query execution</returns>
		public static System.Data.SqlClient.SqlDataReader ExecReader(string sQuery, string sConn, int nTimeout=30)
		{
			System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(sConn);
			System.Data.SqlClient.SqlCommand cm = new System.Data.SqlClient.SqlCommand(sQuery, cn);
			cm.CommandTimeout = nTimeout;
			try
			{
				cn.Open();
				return cm.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
			}
			catch (Exception ex)
			{
				if (cn.State == System.Data.ConnectionState.Open) cn.Close();
				throw new Exception("ExecReader failure: " + sQuery, ex);
			}
			finally
			{
				cm.Dispose();
			}
		}

        public static object ExecScalar(string sQuery, string sConn, int nTimeout = 30)
        {
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(sConn);
            System.Data.SqlClient.SqlCommand cm = new System.Data.SqlClient.SqlCommand(sQuery, cn);
            cm.CommandTimeout = nTimeout;
            try
            {
                cn.Open();
                return cm.ExecuteScalar();
            }
            catch (Exception ex)
            {
                if (cn.State == System.Data.ConnectionState.Open) cn.Close();
                throw new Exception("ExecReader failure: " + sQuery, ex);
            }
            finally
            {
                cm.Dispose();
            }
        }



        /// <summary>
        /// Generates ADO.NET data reader for the specified query in the specified database
        /// </summary>
        /// <param name="sQuery">SQL query</param>
        /// <param name="sConn">Connection string</param>
        /// <param name="nTimeout">Command timeout (seconds), with default value of 30 seconds</param>
        /// <returns>Data reader containing the results of the query execution</returns>
        public static int ExecQuery(string sQuery, string sConn, int nTimeout=30)
		{
			System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(sConn);
			System.Data.SqlClient.SqlCommand cm = new System.Data.SqlClient.SqlCommand(sQuery, cn);
			cm.CommandTimeout = nTimeout;
			try
			{
				cn.Open();
				cm.CommandText = sQuery;
				var ret = cm.ExecuteNonQuery();
				cn.Close();
				return ret;
			}
			catch (Exception ex)
			{
				if (cn.State == System.Data.ConnectionState.Open) cn.Close();
				throw new Exception("ExecQuery failure: " + sQuery, ex);
			}
			finally
			{
				if (cm != null) cm.Dispose();
				if (cn != null) cn.Dispose();
			}
		}
		/// <summary>
		/// Replaces DBNull values by empty strings
		/// </summary>
		/// <param name="oValue">The data reader field value</param>
		/// <returns>If the value is DbNull, returns the empty string, otherwise returns the value.</returns>
		public static string EmptyIfDbNull(object oValue)
		{
			return Convert.IsDBNull(oValue) ? string.Empty : oValue.ToString();
		}
	}
}
