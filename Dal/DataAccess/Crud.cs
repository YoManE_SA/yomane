﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
//using Microsoft.AnalysisServices.AdomdClient;

namespace Netpay.Dal.DataAccess
{
	public static class Crud
	{
		//public static CellSet ExecMdx(string connectionString, string mdx) 
		//{
		//		AdomdConnection connection = new AdomdConnection(connectionString);
		//		AdomdCommand command = connection.CreateCommand();
		//		command.CommandText = mdx;
		//		CellSet set = null;
		//		try
		//		{
		//			connection.Open();
		//			set = (CellSet)command.Execute();
		//		}
		//		finally
		//		{
		//			if (connection != null && connection.State != ConnectionState.Closed)
		//				connection.Close();
		//		}

		//		return set;
		//}
		
		public static DataTable ExecQuery(string connectionString, string query)
		{
			SqlConnection connection = new SqlConnection(connectionString);
			SqlCommand command = new SqlCommand(query, connection);

			DataTable table = null;
			SqlDataReader reader = null; 
			try
			{
				connection.Open();
				reader = command.ExecuteReader();
				table = new DataTable();
				table.Load(reader);
			}
			finally
			{
				if (reader != null && !reader.IsClosed)
					reader.Close();
				if (connection != null && connection.State != ConnectionState.Closed)
					connection.Close();
			}

			return table;
		}
	}
}
