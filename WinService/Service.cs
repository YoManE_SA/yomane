﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.WinService
{
    partial class Service : ServiceBase
    {
        public Service()
        {
            InitializeComponent();
        }

        public void Start(string[] args) { OnStart(args); }

        protected override void OnStart(string[] args)
        {
            Netpay.Infrastructure.Application.Init("Win Service");
            Netpay.Bll.Application.Init();
            Netpay.Process.Application.Init();
            Netpay.Monitoring.Application.Init();
            SetEnable(true);
        }

        protected override void OnStop()
        {
            SetEnable(false);
        }

        private void SetEnable(bool bEnable)
        {
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;

            Netpay.Bll.BankHandler.AuthHandlerManager.Current.EnableFilesMonitor = bEnable;
            //Start monitors
            Netpay.Bll.BankHandler.AuthHandlerManager.Current.EnableGenerateTimer = bEnable;
            Netpay.Bll.BankHandler.ChbHandlerManager.Current.EnableFilesMonitor = bEnable;
            Netpay.Bll.BankHandler.EpaHandlerManager.Current.EnableFilesMonitor = bEnable;
            Netpay.Bll.BankHandler.FraudHandlerManager.Current.EnableFilesMonitor = bEnable;

            Netpay.Bll.Wires.WiresBatch.EnableRealTimeSend = bEnable;
            Netpay.Infrastructure.Domain.EnableClearTempDirectory = bEnable;


            Netpay.Bll.BankHandler.FNBAuthHandler.EnableRealTimeFetch = bEnable;

            Netpay.Bll.ThirdParty.eCentric.EnableServers(bEnable);
            Netpay.Bll.ThirdParty.ACS.EnableServers(bEnable);


        }
    }
}
