﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace Netpay.WinService
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main(string[] args)
		{
			if (args != null && args.Length > 0 && args[0].ToLower() == "-debug") 
				(new Service()).Start(args);
			else 
				ServiceBase.Run(new ServiceBase[] { new Service() });
		}
	}
}
