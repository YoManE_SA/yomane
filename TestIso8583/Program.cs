﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using System.Threading;
using System.ServiceProcess;
using System.IO;
using System.Xml;
using System.Windows.Forms;

namespace TestIso8583
{
    class Program
    {
        static int nSuccess = 0;
        static int nFail = 0;
        static string strIP = "localhost";
        [STAThread]
        static void Main(string[] args)
        {
            TestWebServiceV1();
            //TestWebServiceV2();
            Console.WriteLine("Done... Press something to exit.");
            Console.ReadLine();
            return;
            Netpay.Crypt.SymEncryption.GetKey(5).DecryptFile(@"C:\Lendl\Work\YoMaNe\MarkoffFiles\OneSP20180212.sw2", @"C:\Lendl\Work\YoMaNe\MarkoffFiles\OneSP20180212.sw2d");
            return;

            //TwoHundredForReversalPost();
            ReversalPost();
            Environment.Exit(0);
            return;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmStressTest());

            Environment.Exit(0);

            int nNoOfItterations = 10;
            DateTime dtStart = DateTime.Now;
            int nCount = 0;

            Console.WriteLine("Post to (leave blank for localhost):");
            string strResult = Console.ReadLine();

            if (!string.IsNullOrEmpty(strResult))
                strIP = strResult;

            if (strIP == "localhost")
            {
                nNoOfItterations = 1;
            }
            else if (strIP == "dev")
            {
                strIP = "51.140.78.162";
            }

            /*
            var client = new System.Net.Sockets.TcpClient();

            //Acs
            client.Connect("localhost", 52015);
            */

            var iso8583xml = new Netpay.Bll.ThirdParty.Iso8583Base.Iso8583PostXml((Netpay.Bll.ThirdParty.Iso8583Base)null);

            //ACS
            iso8583xml.MessageType = "0200"; //message type
            //iso8583xml.SetField("Field_002", "9710400000017529"); //PAN
            //iso8583xml.SetField("Field_004", "000000006598");     //Amount
            //iso8583xml.SetField("Field_049", "710");              //CurrencyIsoNumber
            //iso8583xml.SetField("Field_041", "1604198600000");    //Terminal number
            //iso8583xml.SetField("Field_042", "206001001483255");  //Merchant number - appears in terminal accessname1
            //iso8583xml.SetField("Field_037", "SC0211180459");     //Reference code
            //iso8583xml.SetField("Field_011", "565099");           //Reference number
            //iso8583xml.SetField("Field_014", "9999");             //Exp date
            //iso8583xml.SetField("Field_052", "2FC225CBFEE048C8"); //Encrypted pin


            //Ecentric
            iso8583xml.SetField("Field_002", "9710400000017529"); //PAN
            iso8583xml.SetField("Field_004", "000000000001");     //Amount
            iso8583xml.SetField("Field_049", "710");              //CurrencyIsoNumber
            iso8583xml.SetField("Field_041", "ST001");    //Terminal number
            iso8583xml.SetField("Field_042", "ST001");  //Merchant number - appears in terminal accessname1
            iso8583xml.SetField("Field_037", "SC0313125653");     //Reference code
            iso8583xml.SetField("Field_011", "146613");           //Reference number
            iso8583xml.SetField("Field_014", "9999");             //Exp date
            iso8583xml.SetField("Field_052", "1234"); //Encrypted pin


            string strXML = iso8583xml.InnerXml.ToString();
            byte[] xmlAsBytes = Netpay.Bll.ThirdParty.Iso8583Base.XmlDocToByteArray(iso8583xml);

            List<Thread> lstThreads = new List<Thread>();
            List<Task> lstTasks = new List<Task>();

            while (nCount < nNoOfItterations)
            {
                nCount++;
                #region Thread

                Thread pMyThread = new Thread(() =>
                {
                    try
                    {
                        Post(nCount, dtStart);
                    }
                    catch (Exception ex)
                    {
                        while (ex != null)
                        {
                            try
                            {
                                Post(nCount, dtStart);
                                ex = null;
                            }
                            catch (Exception ex1) { ex = ex1; }

                            if ((DateTime.Now - dtStart).TotalSeconds > 10)
                                break;
                        }
                    }
                });
                lstThreads.Add(pMyThread);

                #endregion

                #region Task

                Task pMyTask = new Task(() =>
                {
                    try
                    {
                        Post(nCount, dtStart);
                    }
                    catch (Exception ex)
                    {
                        while (ex != null)
                        {
                            try
                            {
                                Post(nCount, dtStart);
                                ex = null;
                            }
                            catch (Exception ex1) { ex = ex1; }

                            if ((DateTime.Now - dtStart).TotalSeconds > 10)
                                break;
                        }
                    }
                });
                lstTasks.Add(pMyTask);
                #endregion
            }

            nCount = 0;
            foreach (var pTask in lstTasks)
            {
                nCount++;
                pTask.Start();
                pTask.Wait();
                //Console.WriteLine(nCount.ToString());// + " - StartTime:" + dtStart.ToString() + " (AVG: " + ((decimal)nCount / (decimal)(DateTime.Now - dtStart).TotalSeconds).ToString("0.00") + " per second)");
            }

            Task.WaitAll(lstTasks.ToArray());

            Console.WriteLine("Done - " + nCount + " in " + (DateTime.Now - dtStart).TotalSeconds + " seconds (AVG: " + ((decimal)nCount / (decimal)(DateTime.Now - dtStart).TotalSeconds) + " per second)");
            Console.WriteLine("Success: " + nSuccess);
            Console.WriteLine("Fail: " + nFail);
            Console.ReadLine();
        }

        static void ReversalPost()
        {
            var xml = new Netpay.Bll.ThirdParty.Iso8583Base.Iso8583PostXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Iso8583PostXml><MsgType>0420</MsgType><Fields><Field_002>9710400005000298</Field_002><Field_003>001000</Field_003><Field_004>000000020000</Field_004><Field_007>0206135404</Field_007><Field_011>000259</Field_011><Field_012>153449</Field_012><Field_013>0206</Field_013><Field_014>9999</Field_014><Field_015>0219</Field_015><Field_018>6010</Field_018><Field_022>901</Field_022><Field_025>00</Field_025><Field_028>C00000000</Field_028><Field_030>C00000000</Field_030><Field_035>9710400005000298=9999547</Field_035><Field_037>P5Pfef022219</Field_037><Field_038>P00001</Field_038><Field_039>00</Field_039><Field_040>547</Field_040><Field_041>80000005</Field_041><Field_042>360000000000020</Field_042><Field_043>SureSwipe/Mercantile.  Jhb          GPZA</Field_043><Field_049>710</Field_049><Field_054>1053710D000000020000</Field_054><Field_056>1400</Field_056><Field_059>0000885441</Field_059><Field_090>020000025102061334490000000000000000000000</Field_090><Field_095>000000000000000000000000C00000000C00000000</Field_095><Field_123>51010121234C101</Field_123><Field_127_002>0000885441</Field_127_002><Field_127_003>TermAppISO  YomaneSnk   022221000259GearmaxTG   </Field_127_003><Field_127_004>4       000221000    0</Field_127_004><Field_127_006>11</Field_127_006><Field_127_011>0000885432</Field_127_011><Field_127_013>007102000     710</Field_127_013><Field_127_014></Field_127_014><Field_127_020>20171206</Field_127_020><Field_127_022>235TermApp.ISO:ReconciliationIndicator13022218Postilion:MetaData3118235TermApp.ISO:ReconciliationIndicator11117ACSData111216TenderDetailGUID11117SERVICE111213SerialNumbers11117MANAGER11117ACSData272Reversal created in tranSave() - bCancel is true and CompletionCode is 3216TenderDetailGUID236188fc6dd-e082-4bfb-bd86-39872e158a8c17SERVICE14RETL213SerialNumbers22519MagIC-M-4211316-937-24317MANAGER2100000000000</Field_127_022><Field_127_026>YomaneSnk</Field_127_026></Fields></Iso8583PostXml>");

            string strXML = xml.InnerXml.ToString();
            byte[] xmlAsBytes = Netpay.Bll.ThirdParty.Iso8583Base.XmlDocToByteArray(xml);


            var client = new System.Net.Sockets.TcpClient();

            //Acs
            //client.Connect("localhost", 52015);
            client.Connect(strIP, 52015);

            //Ecentric
            //client.Connect("localhost", 9393);

            client.Client.ReceiveTimeout = 10000000;
            client.Client.Send(xmlAsBytes);
            //client.Client.BeginSend(xmlAsBytes, 0, xmlAsBytes.Length , System.Net.Sockets.SocketFlags.None, SendCallback, null);

            byte[] bytes = new byte[2048];

            var result = client.Client.Receive(bytes);

        }

        static void TwoHundredForReversalPost()
        {
            var xml = new Netpay.Bll.ThirdParty.Iso8583Base.Iso8583PostXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Iso8583PostXml><MsgType>0200</MsgType><Fields><Field_002>9710400005000298</Field_002><Field_003>001000</Field_003><Field_004>000000020000</Field_004><Field_007>0206133449</Field_007><Field_011>000251</Field_011><Field_012>153449</Field_012><Field_013>0206</Field_013><Field_014>9999</Field_014><Field_015>0219</Field_015><Field_018>6010</Field_018><Field_022>901</Field_022><Field_025>00</Field_025><Field_026>12</Field_026><Field_028>C00000000</Field_028><Field_030>C00000000</Field_030><Field_035>9710400005000298=9999547</Field_035><Field_037>P5Pfef022219</Field_037><Field_040>547</Field_040><Field_041>80000005</Field_041><Field_042>360000000000020</Field_042><Field_043>SureSwipe/Mercantile.  Jhb          GPZA</Field_043><Field_049>710</Field_049><Field_052>673F68D385962D9D</Field_052><Field_056>1510</Field_056><Field_059>0000885432</Field_059><Field_123>51010121234C101</Field_123><Field_127_002>0000885432</Field_127_002><Field_127_003>TermAppISO  YomaneSnk   022219000251GearmaxTG   </Field_127_003><Field_127_004>4       000219000    0</Field_127_004><Field_127_013>007102000     710</Field_127_013><Field_127_014></Field_127_014><Field_127_020>20171206</Field_127_020><Field_127_022>235TermApp.ISO:ReconciliationIndicator13022218Postilion:MetaData3106235TermApp.ISO:ReconciliationIndicator111216TenderDetailGUID11117SERVICE11117MANAGER111213SerialNumbers111216TenderDetailGUID23694784237-215a-4835-afc7-11b598e238c117SERVICE14RETL17MANAGER2100000000000213SerialNumbers22519MagIC-M-4211316-937-243</Field_127_022></Fields></Iso8583PostXml>");

            string strXML = xml.InnerXml.ToString();
            byte[] xmlAsBytes = Netpay.Bll.ThirdParty.Iso8583Base.XmlDocToByteArray(xml);


            var client = new System.Net.Sockets.TcpClient();

            //Acs
            //client.Connect("localhost", 52015);
            client.Connect(strIP, 52015);

            //Ecentric
            //client.Connect("localhost", 9393);

            client.Client.ReceiveTimeout = 10000000;
            client.Client.Send(xmlAsBytes);
            //client.Client.BeginSend(xmlAsBytes, 0, xmlAsBytes.Length , System.Net.Sockets.SocketFlags.None, SendCallback, null);

            byte[] bytes = new byte[2048];

            var result = client.Client.Receive(bytes);

        }

        #region Windows Service (ISO 8583)

        static void Post(int nCount, DateTime dtStart)
        {
            if (strIP == "localhost")
                if (!IsWindowsServiceRunning())
                    Environment.Exit(0);


            /*string xmldata = "&lt; ?xml version=\"1.0\" encoding=\"UTF-8\" ?&gt;" +
                             "&lt; Iso8583PostXml &gt;" +
                             "&lt; MsgType &gt; 0200 &lt;/MsgType&gt;" +
                             "&lt; Fields &gt;" +
                             "&lt; Field_002 &gt; 9710400000017529 &lt;/Field_002 &gt;" +
                             "&lt; Field_003 &gt; 000000           &lt;/Field_003 &gt;" +
                             "&lt; Field_004 &gt; 000000006598     &lt;/Field_004 &gt;" + 
                             "&lt; Field_007 &gt; 0211180459       &lt;/Field_007 &gt;" +
                             "&lt; Field_011 &gt; 565099           &lt;/Field_011 &gt;"+
                             "&lt; Field_012 &gt; 180503           &lt;/Field_012 &gt;" +
                             "&lt; Field_013 &gt; 0211             &lt;/Field_013 &gt;" +
                             "&lt; Field_014 &gt; 9999             &lt;/Field_014 &gt;" +
                             "&lt; Field_015 &gt; 0211             &lt;/Field_015 &gt;" +
                             "&lt; Field_018 &gt; 5411             &lt;/Field_018 &gt;" +
                             "&lt; Field_022 &gt; 901              &lt;/Field_022 &gt;" + 
                             "&lt; Field_025 &gt; 00               &lt;/Field_025 &gt;" +
                             "&lt; Field_026 &gt; 12               &lt;/Field_026 &gt;" +
                             "&lt; Field_028 &gt; C00000000        &lt;/Field_028 &gt;" +
                             "&lt; Field_030 &gt; C00000000        &lt;/Field_030 &gt;" +
                             "&lt; Field_032 &gt; 88877788877      &lt;/Field_032 &gt;" + 
                             "&lt; Field_035 &gt; 9710400000017529 = 9999547 &lt;/Field_035 &gt;" + 
                             "&lt; Field_037 &gt; SC0211180459     &lt;/Field_037 &gt;" + 
                             "&lt; Field_040 &gt; 547              &lt;/Field_040 &gt;" + 
                             "&lt; Field_041 &gt; 97483219         &lt;/Field_041 &gt;"+ 
                             "&lt; Field_042 &gt; 206001001483209  &lt;/Field_042 &gt;" +
                             "&lt; Field_043 &gt; ZA_GP_CH_Raslouw SB048327     GPZA &lt;/Field_043 &gt;" + 
                             "&lt; Field_049 &gt; 710              &lt;/Field_049 &gt;" + 
                             "&lt; Field_052 &gt; 2FC225CBFEE048C8 &lt;/Field_052 &gt;" + 
                             "&lt; Field_056 &gt; 1510             &lt;/Field_056 &gt;" +
                             "&lt; Field_059 &gt; 0711804964       &lt;/Field_059 &gt;" +
                             "&lt; Field_123 &gt; 51010121300C101  &lt;/Field_123 &gt;" +
                             "&lt; Field_127_002 &gt; 0711804964   &lt;/Field_127_002 &gt;"+
                             "&lt; Field_127_003 &gt; PosConSrc PBOneSP     565099565099OneSP &lt;/Field_127_003 &gt;"+ 
                             "&lt; Field_127_004 &gt; 00000250     &lt;/Field_127_004 &gt;"+ 
                             "&lt; Field_127_012 &gt; Shoprite     &lt;/Field_127_012 &gt;" + 
                             "&lt; Field_127_013 &gt; GPZA          710 &lt;/Field_127_013 &gt;" + 
                             "&lt; Field_127_014 &gt; FNB              &lt;/Field_127_014 &gt;" +
                             "&lt; Field_127_020 &gt; 20160211         &lt;/Field_127_020 &gt;" +
                             "&lt; Field_127_022 &gt; 218Postilion: MetaData232214PosPrivateData11117Country111214PosPrivateData237 &lt;" +
                             "?xml version = \"1.0\" ?&gt; &lt; PrivateData /&gt; 17Country1201 &lt;/Field_127_022 &gt; &lt;/Fields &gt; &lt;/Iso8583PostXml &gt;";
                             */

            var iso8583xml = new Netpay.Bll.ThirdParty.Iso8583Base.Iso8583PostXml((Netpay.Bll.ThirdParty.Iso8583Base)null);

            //ACS
            iso8583xml.MessageType = "0200"; //message type
            //iso8583xml.SetField("Field_002", "9710400000017529"); //PAN
            //iso8583xml.SetField("Field_004", "000000006598");     //Amount
            //iso8583xml.SetField("Field_049", "710");              //CurrencyIsoNumber
            //iso8583xml.SetField("Field_041", "1604198600000");    //Terminal number
            //iso8583xml.SetField("Field_042", "206001001483255");  //Merchant number - appears in terminal accessname1
            //iso8583xml.SetField("Field_037", "SC0211180459");     //Reference code
            //iso8583xml.SetField("Field_011", "565099");           //Reference number
            //iso8583xml.SetField("Field_014", "9999");             //Exp date
            //iso8583xml.SetField("Field_052", "2FC225CBFEE048C8"); //Encrypted pin


            //Ecentric
            iso8583xml.SetField("Field_002", "9710400000017529"); //PAN
            iso8583xml.SetField("Field_004", "000000000001");     //Amount
            iso8583xml.SetField("Field_049", "710");              //CurrencyIsoNumber
            iso8583xml.SetField("Field_041", "ST001");    //Terminal number
            iso8583xml.SetField("Field_042", "ST001");  //Merchant number - appears in terminal accessname1
            iso8583xml.SetField("Field_037", "SC0313125653");     //Reference code
            iso8583xml.SetField("Field_011", "146613");           //Reference number
            iso8583xml.SetField("Field_014", "9999");             //Exp date
            iso8583xml.SetField("Field_052", "6185"); //Encrypted pin



            string strXML = iso8583xml.InnerXml.ToString();
            byte[] xmlAsBytes = Netpay.Bll.ThirdParty.Iso8583Base.XmlDocToByteArray(iso8583xml);

            string strConsole = "";

            try
            {
                var client = new System.Net.Sockets.TcpClient();

                //Acs
                //client.Connect("localhost", 52015);
                client.Connect(strIP, 52015);

                //Ecentric
                //client.Connect("localhost", 9393);

                client.Client.ReceiveTimeout = 10000000;
                client.Client.Send(xmlAsBytes);
                //client.Client.BeginSend(xmlAsBytes, 0, xmlAsBytes.Length , System.Net.Sockets.SocketFlags.None, SendCallback, null);

                byte[] bytes = new byte[2048];

                var result = client.Client.Receive(bytes);
                strConsole = ("Success:" + nSuccess);

                nSuccess++;
            }
            catch (Exception ex)
            {
                nFail++;
                strConsole = ("Fail:" + nFail + " - " + ex.Message);
            }

            Console.WriteLine(strConsole + " - " + (nFail + nSuccess) + " in " + (DateTime.Now - dtStart).TotalSeconds.ToString("0.00") + " seconds (AVG: " + ((decimal)(nFail + nSuccess) / (decimal)(DateTime.Now - dtStart).TotalSeconds).ToString("0.00") + " per second)");
        }

        static bool IsWindowsServiceRunning()
        {
            ServiceController sc = new ServiceController("Netpay Win Service");

            switch (sc.Status)
            {
                case ServiceControllerStatus.Running:
                    return true;
                case ServiceControllerStatus.Stopped:
                case ServiceControllerStatus.Paused:
                case ServiceControllerStatus.StopPending:
                case ServiceControllerStatus.StartPending:
                    return false;
                default:
                    return false;
            }


        }

        #endregion

        #region WebService

        static void TestWebServiceV1()
        {
            #region Login

            string strCredentialsToken = "";
            string strEncodedCookie = "";

            var LoginRequest = (HttpWebRequest)WebRequest.Create("http://webservices.yomane.net/wallet.asmx/Login");

            LoginRequest.Method = "POST";
            //LoginRequest.Accept = "text/xml";
            LoginRequest.ContentType = "application/json; charset=UTF-8";
            var encoding = new UTF8Encoding();

            StringBuilder sbEntirePostData = new StringBuilder();

            sbEntirePostData.AppendLine("{\"email\":\"warren@yomane.com\",\"options\":{\"appName\":\"Android Wallet\",\"applicationToken\":\"e3448aa9-2826-4839-9943-7f2d9fb92d80\"},\"password\":\"Password123\"}");

            var bytes = encoding.GetBytes(sbEntirePostData.ToString());
            LoginRequest.ContentLength = bytes.Length;

            using (var writeStream = LoginRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }


            using (var LoginResponse = (HttpWebResponse)LoginRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (LoginResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", LoginResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = LoginResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);

                responseValue = (responseValue.Substring(responseValue.IndexOf("CredentialsToken"))).Replace("CredentialsToken", string.Empty);
                responseValue = responseValue.Remove(responseValue.IndexOf("VersionUpdateRequired"));
                strCredentialsToken = responseValue.Replace("\"", string.Empty).Replace(":", string.Empty).Replace(",", string.Empty);

            }

            #endregion


            #region GetSupportedPaymentMethods

            var GetSupportedPaymentMethodsRequest = (HttpWebRequest)WebRequest.Create("http://webservices.yomane.net/AppIdentity.asmx/GetSupportedPaymentMethods");

            GetSupportedPaymentMethodsRequest.Method = "POST";
            //GetSupportedPaymentMethodsRequest.Accept = "text/xml";
            GetSupportedPaymentMethodsRequest.ContentType = "application/json; charset=UTF-8";
            encoding = new UTF8Encoding();

            sbEntirePostData = new StringBuilder();

            //sbEntirePostData.AppendLine("{\"destWalletId\":\"4840271\"}");

            bytes = encoding.GetBytes(sbEntirePostData.ToString());
            GetSupportedPaymentMethodsRequest.ContentLength = bytes.Length;
            GetSupportedPaymentMethodsRequest.Headers.Add("Cookie", "credentialsToken=" + strCredentialsToken);
            GetSupportedPaymentMethodsRequest.Headers.Add("applicationToken", "e3448aa9-2826-4839-9943-7f2d9fb92d80");

            using (var writeStream = GetSupportedPaymentMethodsRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (var GetSupportedPaymentMethodsResponse = (HttpWebResponse)GetSupportedPaymentMethodsRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (GetSupportedPaymentMethodsResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", GetSupportedPaymentMethodsResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = GetSupportedPaymentMethodsResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);
            }

            #endregion

            #region FindFriend

            var FindFriendRequest = (HttpWebRequest)WebRequest.Create("http://webservices.yomane.net/wallet.asmx/FindFriend");

            FindFriendRequest.Method = "POST";
            //FindFriendRequest.Accept = "text/xml";
            FindFriendRequest.ContentType = "application/json; charset=UTF-8";
            encoding = new UTF8Encoding();

            sbEntirePostData = new StringBuilder();

            sbEntirePostData.AppendLine("{\"page\":1,\"pageSize\":10,\"searchTerm\":\"fris\"}");

            bytes = encoding.GetBytes(sbEntirePostData.ToString());
            FindFriendRequest.ContentLength = bytes.Length;
            FindFriendRequest.Headers.Add("Cookie", "credentialsToken=" + strCredentialsToken);

            using (var writeStream = FindFriendRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (var FindFriendResponse = (HttpWebResponse)FindFriendRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (FindFriendResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", FindFriendResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = FindFriendResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);
            }

            #endregion

            #region GetRequest

            var GetRequestRequest = (HttpWebRequest)WebRequest.Create("http://webservices.yomane.net/balance.asmx/GetRequests");

            GetRequestRequest.Method = "POST";
            //GetRequestRequest.Accept = "text/xml";
            GetRequestRequest.ContentType = "application/json; charset=UTF-8";
            encoding = new UTF8Encoding();

            sbEntirePostData = new StringBuilder();

            sbEntirePostData.AppendLine("{\"filters\":{\"page\":1,\"pageSize\":10,\"CurrencyIso\":\"ZAR\"}}");

            bytes = encoding.GetBytes(sbEntirePostData.ToString());
            GetRequestRequest.ContentLength = bytes.Length;
            GetRequestRequest.Headers.Add("Cookie", "credentialsToken=" + strCredentialsToken);

            using (var writeStream = GetRequestRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (var GetRequestResponse = (HttpWebResponse)GetRequestRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (GetRequestResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", GetRequestResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = GetRequestResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);
            }

            #endregion

            //return;

            #region GetFriends

            var GetFriendsRequest = (HttpWebRequest)WebRequest.Create("http://webservices.yomane.net/wallet.asmx/GetFriends");

            GetFriendsRequest.Method = "POST";
            //GetFriendsRequest.Accept = "text/xml";
            GetFriendsRequest.ContentType = "application/json; charset=UTF-8";
            encoding = new UTF8Encoding();

            sbEntirePostData = new StringBuilder();

            sbEntirePostData.AppendLine("{\"destWalletId\":\"4840271\"}");

            bytes = encoding.GetBytes(sbEntirePostData.ToString());
            GetFriendsRequest.ContentLength = bytes.Length;
            GetFriendsRequest.Headers.Add("Cookie", "credentialsToken=" + strCredentialsToken);

            using (var writeStream = GetFriendsRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (var GetFriendsResponse = (HttpWebResponse)GetFriendsRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (GetFriendsResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", GetFriendsResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = GetFriendsResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);
            }

            #endregion

            #region GetStoredPaymentMethods

            var GetStoredPaymentMethodsRequest = (HttpWebRequest)WebRequest.Create("http://webservices.yomane.net/wallet.asmx/GetStoredPaymentMethods");

            GetStoredPaymentMethodsRequest.Method = "POST";
            GetStoredPaymentMethodsRequest.Accept = "*";
            GetStoredPaymentMethodsRequest.ContentType = "text/xml;charset=UTF-8 SOAPAction: \"WebServices/GetStoredPaymentMethods\"";
            encoding = new UTF8Encoding();

            sbEntirePostData = new StringBuilder();

            sbEntirePostData.AppendLine("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"WebServices\">");
            sbEntirePostData.AppendLine("   <soapenv:Header/>");
            sbEntirePostData.AppendLine("   <soapenv:Body>");
            sbEntirePostData.AppendLine("      <web:GetStoredPaymentMethods/>");
            sbEntirePostData.AppendLine("   </soapenv:Body>");
            sbEntirePostData.AppendLine("</soapenv:Envelope>");

            bytes = encoding.GetBytes(sbEntirePostData.ToString());
            GetStoredPaymentMethodsRequest.ContentLength = bytes.Length;
            GetStoredPaymentMethodsRequest.Headers.Add("Cookie", "credentialsToken=" + strCredentialsToken);

            using (var writeStream = GetStoredPaymentMethodsRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (var GetStoredPaymentMethodsResponse = (HttpWebResponse)GetStoredPaymentMethodsRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (GetStoredPaymentMethodsResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", GetStoredPaymentMethodsResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = GetStoredPaymentMethodsResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);
            }

            #endregion

            #region ProcessTransaction

            var ProcessTransactionRequest = (HttpWebRequest)WebRequest.Create("http://webservices.yomane.net/wallet.asmx");

            ProcessTransactionRequest.Method = "POST";
            ProcessTransactionRequest.Accept = "*";
            ProcessTransactionRequest.ContentType = "text/xml;charset=UTF-8";// SOAPAction: \"WebServices/ProcessTransaction\"";
            encoding = new UTF8Encoding();

            sbEntirePostData = new StringBuilder();

            sbEntirePostData.AppendLine("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"WebServices\">");
            sbEntirePostData.AppendLine("   <soapenv:Header/>");
            sbEntirePostData.AppendLine("   <soapenv:Body>");
            sbEntirePostData.AppendLine("      <web:ProcessTransaction>");
            sbEntirePostData.AppendLine("         <web:data>");
            sbEntirePostData.AppendLine("            <web:PinCode>1234</web:PinCode>");
            sbEntirePostData.AppendLine("            <web:StoredPaymentMethodId>215237</web:StoredPaymentMethodId>");
            //sbEntirePostData.AppendLine("            <web:ShippingAddressId>?</web:ShippingAddressId>");
            sbEntirePostData.AppendLine("            <web:MerchantNumber>1094815</web:MerchantNumber>");
            //sbEntirePostData.AppendLine("            <web:OrderId>?</web:OrderId>");
            sbEntirePostData.AppendLine("            <web:Amount>1.23</web:Amount>");
            sbEntirePostData.AppendLine("            <web:CurrencyIso>ZAR</web:CurrencyIso>");
            //sbEntirePostData.AppendLine("            <web:Installments>?</web:Installments>");
            //sbEntirePostData.AppendLine("            <web:ShopCartCookie>?</web:ShopCartCookie>");
            //sbEntirePostData.AppendLine("            <web:ProductId>?</web:ProductId>");
            //sbEntirePostData.AppendLine("            <web:Quantity>?</web:Quantity>");
            sbEntirePostData.AppendLine("            <web:Text>Test Purchase</web:Text>");
            //sbEntirePostData.AppendLine("            <web:Latitude>?</web:Latitude>");
            //sbEntirePostData.AppendLine("            <web:Longitude>?</web:Longitude>");
            sbEntirePostData.AppendLine("         </web:data>");
            sbEntirePostData.AppendLine("      </web:ProcessTransaction>");
            sbEntirePostData.AppendLine("   </soapenv:Body>");
            sbEntirePostData.AppendLine("</soapenv:Envelope>");

            bytes = encoding.GetBytes(sbEntirePostData.ToString());
            ProcessTransactionRequest.ContentLength = bytes.Length;
            ProcessTransactionRequest.Headers.Add("Cookie", "credentialsToken=" + strCredentialsToken);

            using (var writeStream = ProcessTransactionRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (var ProcessTransactionResponse = (HttpWebResponse)ProcessTransactionRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (ProcessTransactionResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", ProcessTransactionResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = ProcessTransactionResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);
            }

            #endregion

        }

        static void TestWebServiceV2()
        {
            #region Login

            string strCredentialsToken = "";
            string strEncodedCookie = "";

            var LoginRequest = (HttpWebRequest)WebRequest.Create("http://webservices2.yomane.net/v2/Account.svc/Login");

            LoginRequest.Method = "POST";
            LoginRequest.Accept = "*";
            LoginRequest.ContentType = "application/json; charset=UTF-8";
            var encoding = new UTF8Encoding();
            LoginRequest.Headers.Add("applicationToken", "eb602b20-c55a-4eb7-9b91-398aed379f2d");

            StringBuilder sbEntirePostData = new StringBuilder();
            sbEntirePostData.AppendLine("{\"email\":\"lendl@yomane.com\",\"userName\":null,\"password\":\"p4s5w0rD$\",\"options\":{\"appName\":\"Android Wallet\",\"applicationToken\":\"071d9538-9eec-42e5-83d7-916faa403799\",\"deviceId\":\"6bacb22a976bebc6\",\"pushToken\":\"c1olg1kOkdE:APA91bF-TYYX5z0CfLPXnYF3oY27-Kg84iYA5s0Mc3BmXD_VCcu-BzZHZlnUa0GUgxCrW2LhkTbzZ6lj31ToaCTOoLpEqCbSBOqi0_vjtHscKpI5Bnsn1B4XznNemXDMt4JwDyrXKVu9\",\"setCookie\":false,\"excludeDataImages\":true,\"userRole\":\"15\"}}");
            //sbEntirePostData.AppendLine("{\"email\":\"testing@yomane.net\",\"options\":{\"appName\":\"Android Wallet\",\"applicationToken\":\"e3448aa9-2826-4839-9943-7f2d9fb92d80\"},\"password\":\"News12345!@\"}");

            var bytes = encoding.GetBytes(sbEntirePostData.ToString());
            LoginRequest.ContentLength = bytes.Length;

            using (var writeStream = LoginRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }


            using (var LoginResponse = (HttpWebResponse)LoginRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (LoginResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", LoginResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = LoginResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);

                responseValue = (responseValue.Substring(responseValue.IndexOf("CredentialsToken"))).Replace("CredentialsToken", string.Empty);
                responseValue = responseValue.Remove(responseValue.IndexOf(","));
                strCredentialsToken = responseValue.Replace("\"", string.Empty).Replace(":", string.Empty).Replace(",", string.Empty);

            }

            #endregion

            //return;
            #region GetTotal

            var GetTotalRequest = (HttpWebRequest)WebRequest.Create("http://webservices2.yomane.net/v2/Balance.svc/GetTotal");

            GetTotalRequest.Method = "POST";
            GetTotalRequest.Accept = "*";
            GetTotalRequest.ContentType = "application/json; charset=UTF-8";
            encoding = new UTF8Encoding();

            sbEntirePostData = new StringBuilder();

            sbEntirePostData.AppendLine("{\"currencyIsoCode\":\"ZAR\"}");

            bytes = encoding.GetBytes(sbEntirePostData.ToString());
            GetTotalRequest.ContentLength = bytes.Length;
            GetTotalRequest.Headers.Add("Cookie", "default_Token=" + strCredentialsToken);
            GetTotalRequest.Headers.Add("applicationToken", "071d9538-9eec-42e5-83d7-916faa403799");

            using (var writeStream = GetTotalRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (var GetTotalResponse = (HttpWebResponse)GetTotalRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (GetTotalResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", GetTotalResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = GetTotalResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);
            }

            #endregion

            #region GetFriends

            var GetFriendsRequest = (HttpWebRequest)WebRequest.Create("http://webservices2.yomane.net/v2/Customer.svc/GetFriends");

            GetFriendsRequest.Method = "POST";
            GetFriendsRequest.Accept = "*";
            GetFriendsRequest.ContentType = "application/json; charset=UTF-8";
            encoding = new UTF8Encoding();

            sbEntirePostData = new StringBuilder();

            sbEntirePostData.AppendLine("{\"destWalletId\":\"4840271\"}");

            bytes = encoding.GetBytes(sbEntirePostData.ToString());
            GetFriendsRequest.ContentLength = bytes.Length;
            GetFriendsRequest.Headers.Add("Cookie", "default_Token=" + strCredentialsToken);

            using (var writeStream = GetFriendsRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (var GetFriendsResponse = (HttpWebResponse)GetFriendsRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (GetFriendsResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", GetFriendsResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = GetFriendsResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);
            }

            #endregion

            #region FindFriend

            var FindFriendRequest = (HttpWebRequest)WebRequest.Create("http://webservices.yomane.net/wallet.asmx/FindFriend");

            FindFriendRequest.Method = "POST";
            //FindFriendRequest.Accept = "text/xml";
            FindFriendRequest.ContentType = "application/json; charset=UTF-8";
            encoding = new UTF8Encoding();

            sbEntirePostData = new StringBuilder();

            sbEntirePostData.AppendLine("{\"page\":1,\"pageSize\":10,\"searchTerm\":\"\"}");

            bytes = encoding.GetBytes(sbEntirePostData.ToString());
            FindFriendRequest.ContentLength = bytes.Length;
            FindFriendRequest.Headers.Add("Cookie", "credentialsToken=" + strCredentialsToken);

            using (var writeStream = FindFriendRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (var FindFriendResponse = (HttpWebResponse)FindFriendRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (FindFriendResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", FindFriendResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = FindFriendResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);
            }

            #endregion

            #region ProcessTransaction

            var ProcessTransactionRequest = (HttpWebRequest)WebRequest.Create("http://webservices.yomane.net/wallet.asmx/ProcessTransaction");

            ProcessTransactionRequest.Method = "POST";
            //ProcessTransactionRequest.Accept = "text/xml";
            ProcessTransactionRequest.ContentType = "application/json; charset=UTF-8";
            encoding = new UTF8Encoding();

            sbEntirePostData = new StringBuilder();

            sbEntirePostData.AppendLine("{\"data\":{\"amount\":1.23,\"customerNumber\":\"1177039\",\"merchantNumber\":1094815,\"pinCode\":\"6986\",\"storedPaymentMethodId\":215226}}");

            bytes = encoding.GetBytes(sbEntirePostData.ToString());
            ProcessTransactionRequest.ContentLength = bytes.Length;
            ProcessTransactionRequest.Headers.Add("Cookie", "default_Token=" + strCredentialsToken);

            using (var writeStream = ProcessTransactionRequest.GetRequestStream())
            {
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (var ProcessTransactionResponse = (HttpWebResponse)ProcessTransactionRequest.GetResponse())
            {
                var responseValue = string.Empty;

                if (ProcessTransactionResponse.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", ProcessTransactionResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response
                using (var responseStream = ProcessTransactionResponse.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                Console.WriteLine(responseValue);
            }

            #endregion

        }

        #endregion
    }
}
