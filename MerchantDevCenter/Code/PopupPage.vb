﻿Namespace Code
    Public Class PopupPage
        Inherits NetpayPage

        Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
            If Not IsAuthorized() Then
                Response.Write(String.Format("<script>top.opener.top.location.href = '{0}';top.window.close();</script>", Request.ApplicationPath))
                Response.End()
            End If
            MyBase.OnInitComplete(e)
        End Sub
    End Class
End Namespace


