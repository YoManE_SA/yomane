Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Namespace Code
    Public Class htmlInputs
        Inherits System.Web.UI.Page

        Public Shared Function PutRecordsetComboDefault(ByVal sSqlString As String, ByVal selectName As String, ByVal selectExtras As String, _
         ByVal optionValue As String, ByVal optionText As String, ByVal optionValueDefault As String, ByVal optionTextDefault As String) As String

            Dim sSelected As String
            Dim bHasRows As Boolean
            Dim iReaderTmp As SqlDataReader = dbPages.ExecReader(sSqlString)
            bHasRows = iReaderTmp.HasRows
            If bHasRows Then
                HttpContext.Current.Response.Write("<select name=""" & selectName & """ id=""" & selectName & """ " & selectExtras & ">")
                If optionValueDefault <> "false" And optionTextDefault <> "false" Then
                    HttpContext.Current.Response.Write("<option value=""" & optionValueDefault & """>" & optionTextDefault & "</option>")
                End If
                While iReaderTmp.Read()
                    sSelected = ""
                    If Trim(iReaderTmp(optionValue)) = Trim(HttpContext.Current.Request(selectName)) Then sSelected = " selected"
                    HttpContext.Current.Response.Write("<option value=""" & iReaderTmp(optionValue) & """" & sSelected & ">" & iReaderTmp(optionText) & "</option>")
                End While
                HttpContext.Current.Response.Write("</select>")
            End If
            iReaderTmp.Close()
            Return bHasRows
        End Function
    End Class
End Namespace


