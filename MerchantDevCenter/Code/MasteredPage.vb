﻿Namespace Code
    Public Class MasteredPage
        Inherits NetpayPage

        Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
            Page.MasterPageFile = "~/Templates/" & WebUtils.CurrentDomain.ThemeFolder & "/MasterPage.master"
            MyBase.OnPreInit(e)
        End Sub

        Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
            If Not IsAuthorized() Then Response.Redirect("~/", True)
            MyBase.OnInitComplete(e)
        End Sub
    End Class
End Namespace


