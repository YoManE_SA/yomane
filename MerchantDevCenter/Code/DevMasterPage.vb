﻿Imports Microsoft.VisualBasic

Namespace Code
    Public Class DevMasterPage
        Inherits MasterPage
        Private Shared _freeAccessPages As System.Collections.Generic.List(Of String) = Nothing
        Private _loadFreePages As Boolean

        Protected ReadOnly Property IsDev() As Boolean
            Get
                If Request("DebugTest") IsNot Nothing Then Return Request("DebugTest") = "1"
                Return (WebUtils.DomainHost = "localhost") Or (WebUtils.DomainHost = "127.0.0.1") 'Or Request.UserHostAddress.StartsWith("192.168.5")
            End Get
        End Property

        Protected ReadOnly Property IsGuest() As Boolean
            Get
                Return Not WebUtils.IsLoggedin
            End Get
        End Property

        Private Function xmlAttribValue(ByVal pNode As System.Xml.XmlNode, ByVal attribName As String) As String
            Dim aatr As System.Xml.XmlAttribute = pNode.Attributes(attribName)
            If aatr IsNot Nothing Then Return aatr.Value.EmptyIfNull()
            Return String.Empty
        End Function

        Protected Sub LoadXmlTree(ByVal t As TreeView, ByVal fileName As String)
            Dim xmlDoc As New System.Xml.XmlDocument
            xmlDoc.Load(fileName)
            LoadXmlTree(t.Nodes, xmlDoc.DocumentElement)
            setSelectedItem(t.Nodes(0))
        End Sub

        Private Sub LoadXmlTree(ByVal treeNodeCol As TreeNodeCollection, ByVal xmlNode As System.Xml.XmlNode)
            Dim bAddNode As Boolean = True
            Dim bDevRequest As Boolean = IsDev
            Dim pageUrl As String = xmlAttribValue(xmlNode, "URL")
            Dim userOnlyPage As Boolean = dbPages.TestVar(xmlAttribValue(xmlNode, "UserOnly"), False)
            Dim newNode As TreeNode = New TreeNode(xmlAttribValue(xmlNode, "Title"), Nothing, Nothing, pageUrl, Nothing)
            Select Case xmlNode.LocalName
                Case "SiteMenu" : newNode.SelectAction = TreeNodeSelectAction.None
                Case "HeadItem" : newNode.SelectAction = TreeNodeSelectAction.Expand
                Case "PageItem" : newNode.SelectAction = TreeNodeSelectAction.Select
            End Select
            If userOnlyPage Then
                If IsGuest Then
                    newNode.NavigateUrl = "~/Website/PermissionDenied.aspx?Title=" & Server.UrlEncode(newNode.Text)
                    newNode.Text += " *"
                End If
            ElseIf Not String.IsNullOrEmpty(pageUrl) Then
                Dim qIndex = pageUrl.IndexOf("?")
                If qIndex > 0 Then pageUrl = pageUrl.Substring(0, qIndex)
                If _loadFreePages Then _freeAccessPages.Add(System.IO.Path.GetFileName(pageUrl).ToLower())
            End If
            If dbPages.TestVar(xmlAttribValue(xmlNode, "DevOnly"), False) Then
                If bDevRequest Then newNode.Text += " **" Else bAddNode = False
            End If
            If bAddNode Then
                treeNodeCol.Add(newNode)
                For Each n As System.Xml.XmlNode In xmlNode.ChildNodes
                    If n.NodeType = System.Xml.XmlNodeType.Element Then LoadXmlTree(newNode.ChildNodes, n)
                Next
            End If
        End Sub

        Public Function setSelectedItem(ByVal node As WebControls.TreeNode) As Boolean
            Dim sFile As String = System.IO.Path.GetFileName(Request.Path).ToLower()
            If (Request.QueryString.Count > 0) Then sFile &= "?" & Request.QueryString.ToString().ToLower()
            For i As Integer = 0 To node.ChildNodes.Count - 1
                Dim sNode As WebControls.TreeNode = node.ChildNodes(i)
                If sNode.NavigateUrl.ToLower().EndsWith(sFile) Then
                    sNode.Select()
                    sNode = sNode.Parent
                    While Not sNode Is Nothing
                        sNode.Expand()
                        sNode = sNode.Parent
                    End While
                    Return True
                Else
                    If setSelectedItem(sNode) Then Return True
                End If
            Next
            Return False
        End Function

        Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
            MyBase.OnInit(e)
            If Not String.IsNullOrEmpty(Request.QueryString("Action")) Then
                If LCase(Trim(Request.QueryString("Action"))) = "logout" Then
                    WebUtils.Logout()
                End If
            End If
            'If Not WebUtils.IsLoggedin Then
            'If Not Request.FilePath.ToLower().EndsWith("login.aspx") Then
            'Response.Redirect("Login.aspx")
            'End If
            'End If
            If IsPostBack Then Exit Sub
            _loadFreePages = (_freeAccessPages Is Nothing)
            If _loadFreePages Then
                _freeAccessPages = New System.Collections.Generic.List(Of String)
                _freeAccessPages.Add("default.aspx")
                _freeAccessPages.Add("login.aspx")
                _freeAccessPages.Add("permissiondenied.aspx")
            End If
            LoadXmlTree(FindControl("TreeView2"), MapPath("~/App_Data/MenuSide_BasicInfo.xml"))
            LoadXmlTree(FindControl("TreeView1"), MapPath("~/App_Data/MenuSide_PaySolutions.xml"))
            LoadXmlTree(FindControl("TreeView4"), MapPath("~/App_Data/MenuSide_Services.xml"))
            LoadXmlTree(FindControl("TreeView3"), MapPath("~/App_Data/MenuSide_Management.xml"))
        End Sub

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
            MyBase.OnLoad(e)
            CType(FindControl("lblHeaderTitle"), Label).Text = WebUtils.CurrentDomain.BrandName & " - Developer Center"
            If Request("ToPDF") = "1" Then dbPages.SendControlPdf(FindControl("ContentPlaceHolder1"), WebUtils.CurrentDomain.DevCentertUrl & "website/")
        End Sub

        Public Shared ReadOnly Property FreeAccessPages() As System.Collections.Generic.List(Of String)
            Get
                Return _freeAccessPages
            End Get
        End Property
    End Class
End Namespace


