﻿Namespace Code
    Public Class IFramePage
        Inherits NetpayPage

        Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
            If Not IsAuthorized() Then
                Response.Write(String.Format("<script>parent.location.href = '{0}'</script>", Request.ApplicationPath))
                Response.End()
            End If
            MyBase.OnInitComplete(e)
        End Sub
    End Class
End Namespace


