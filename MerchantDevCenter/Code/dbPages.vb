Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Web
Imports System.Configuration
Imports WebSupergoo.ABCpdf6
Imports System.Net
Imports System.IO

Namespace Code
    Public Class dbPages
        Public Shared Function getConfig(ByVal sKey As String) As String
            Return ConfigurationManager.AppSettings.Item(sKey)
        End Function

        Public Shared ReadOnly Property DSN() As String
            Get
                Return WebUtils.CurrentDomain.Sql1ConnectionString
            End Get
        End Property

        Public Shared Function ExecSql(ByVal sqlStr As String) As Long
            Dim DBCon As New System.Data.SqlClient.SqlConnection(DSN)
            Dim DBCom As New System.Data.SqlClient.SqlCommand(sqlStr, DBCon)
            Try
                DBCon.Open()
                ExecSql = DBCom.ExecuteNonQuery()
            Catch
                With System.Web.HttpContext.Current
                    .Response.Write(sqlStr)
                    .Response.Flush()
                End With
                Throw
            Finally
                DBCon.Close()
                DBCom.Dispose()
                DBCon.Dispose()
            End Try
        End Function

        Public Shared Function ExecScalar(ByVal sqlStr As String) As Object
            Dim DBCon As New SqlConnection(DSN)
            Dim DBCom As New SqlCommand(sqlStr, DBCon)
            Try
                DBCon.Open()
                ExecScalar = DBCom.ExecuteScalar()
            Catch
                With System.Web.HttpContext.Current
                    .Response.Write(sqlStr)
                    .Response.Flush()
                End With
                Throw
            Finally
                DBCon.Close()
                DBCom.Dispose()
                DBCon.Dispose()
            End Try
        End Function

        Public Shared Function ExecReader(ByVal sqlStr As String) As SqlDataReader
            Dim DBCon As New SqlConnection(DSN)
            Dim DBCom As New SqlCommand(sqlStr, DBCon)
            Try
                DBCon.Open()
                ExecReader = DBCom.ExecuteReader(System.Data.CommandBehavior.CloseConnection)
            Catch
                With System.Web.HttpContext.Current
                    .Response.Write(sqlStr)
                    .Response.Flush()
                End With
                Throw
            Finally
                DBCom.Dispose()
            End Try
        End Function

        Public Shared Function dbtextShowForm(ByVal sText As String) As String
            If sText Is Nothing Then Return ""
            Return sText.Trim.Replace("``", """").Replace("`", "'")
        End Function

        Public Shared Function dbtextShow(ByVal sText As String) As String
            If sText Is Nothing Then Return ""
            Return sText.Trim.Replace("``", """").Replace("`", "'").Replace(VbCr, "<br />")
        End Function

        Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Integer, ByVal lMax As Integer, ByVal lDef As Integer) As Integer
            If (Not xval Is Nothing) And IsNumeric(xval) Then
                Try
                    TestVar = Integer.Parse(xval)
                Catch
                    TestVar = lDef
                End Try
                If (lMin <= lMax) And ((TestVar < lMin) Or (TestVar > lMax)) Then TestVar = lDef
            Else
                TestVar = lDef
            End If
        End Function

        Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Decimal, ByVal lMax As Decimal, ByVal lDef As Decimal) As Decimal
            Dim rVal As Decimal = lDef
            Try
                rVal = Decimal.Parse(xval.ToString())
                If ((lMin <= lMax) And ((rVal < lMin) Or (rVal > lMax))) Then rVal = lDef
            Catch
            End Try
            Return rVal
        End Function

        Public Shared Function TestVar(ByVal xval As Object, ByVal lMax As Integer, ByVal lDef As String) As String
            Dim rVal As String = lDef
            Try
                rVal = xval.ToString()
                If (rVal.Length > lMax And lMax > 0) Then rVal = rVal.Substring(0, lMax)
            Catch
            End Try
            Return rVal
        End Function

        Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Date, ByVal lMax As Date, ByVal lDef As Date) As Date
            Dim rVal As Date = lDef
            Try
                rVal = Date.Parse(xval.ToString())
                If ((lMin <= lMax) And ((rVal < lMin) Or (rVal > lMax))) Then rVal = lDef
            Catch
            End Try
            Return rVal
        End Function

        Public Shared Function TestVar(ByVal xval As Object, ByVal lDef As Boolean) As Boolean
            Dim rVal As Boolean = lDef
            Try
                Boolean.TryParse(xval.ToString(), rVal)
            Catch
            End Try
            Return rVal
        End Function

        Public Shared Function GetFileText(ByVal sFileName As String) As String
            Dim output As String = ""
            If System.IO.File.Exists(sFileName) Then
                Dim sr As New System.IO.StreamReader(sFileName)
                output = sr.ReadToEnd()
                sr.Close()
            End If
            Return output
        End Function

        Public Shared Sub ShowXml(ByVal Node As String, ByVal value As String, Optional ByVal indent As Integer = 0, Optional ByVal bCloseTag As Boolean = True, Optional ByVal sComment As String = Nothing)
            Dim Response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            Response.Write(Space(indent * 4).Replace(" ", "&nbsp;"))
            Response.Write("&lt;" & Node & "&gt;" & value)
            If bCloseTag Then Response.Write("&lt;/" & Node & "&gt;")
            If Not String.IsNullOrEmpty(sComment) Then Response.Write(sComment)
            Response.Write("<br>")
        End Sub

        Public Shared Sub SendControlPdf(ByVal ctl As Control, Optional ByVal sBaseURL As String = "")
            Dim Response As HttpResponse = ctl.Page.Response
            Dim oldStream As Stream = Response.Filter
            Dim nOutputStram As New MemoryStream()
            Response.Filter = nOutputStram
            Response.ContentType = "application/pdf"
            Response.Write("<html><style>")
            Response.Write(dbPages.GetFileText(ctl.Page.Request.MapPath("StyleSheet.css")))
            Response.Write("</style><body class=""pdf"">")
            ctl.RenderControl(New HtmlTextWriter(Response.Output))
            Response.Write("</body></html>")
            Response.Flush()
            Response.Filter = oldStream
            SendHtmlPdf(ctl.Page.Response, System.Text.Encoding.UTF8.GetString(nOutputStram.GetBuffer()))
            nOutputStram.Dispose()
        End Sub

        Public Shared Sub SendHtmlPdf(Response As HttpResponse, strHtml As String)
            Dim nPgfObg As New Doc()
            If (Response.ContentType <> "application/pdf") Then Response.ContentType = "application/pdf"
            nPgfObg.HtmlOptions.Paged = True
            nPgfObg.HtmlOptions.AddLinks = False
            nPgfObg.Rect.Top = nPgfObg.Rect.Top - 20 : nPgfObg.Rect.Bottom = nPgfObg.Rect.Bottom + 20
            Dim nPageID As Integer = nPgfObg.AddImageHtml(strHtml)
            While True
                If Not nPgfObg.Chainable(nPageID) Then Exit While
                nPgfObg.Page = nPgfObg.AddPage()
                nPageID = nPgfObg.AddImageToChain(nPageID)
            End While
            nPgfObg.Save(Response.OutputStream)
            nPgfObg.Clear()
            Response.End()
        End Sub

        Public Shared Function SendRequestHTTP(ByVal sMethod As String, ByVal sURL As String, ByRef sParams As String) As Integer
            Dim hwrsResult As HttpWebResponse
            Dim hwrqCharge As HttpWebRequest
            Dim stReader As StreamReader = Nothing
            Try
                hwrqCharge = CType(WebRequest.Create(sURL), HttpWebRequest)
                hwrqCharge.Method = sMethod
                If Not String.IsNullOrEmpty(sParams) Then
                    hwrqCharge.ContentType = "application/x-www-form-urlencoded"
                    Using sw As StreamWriter = New StreamWriter(hwrqCharge.GetRequestStream())
						sw.Write(sParams)
						sw.Close()
					End Using
                End If
                hwrsResult = CType(hwrqCharge.GetResponse(), HttpWebResponse)
                stReader = New StreamReader(hwrsResult.GetResponseStream())
                sParams = stReader.ReadToEnd()
                Return hwrsResult.StatusCode
            Catch e As Exception
                Throw e
            Finally
                If stReader IsNot Nothing Then stReader.Dispose()
            End Try
        End Function

		Public Shared ReadOnly Property Currencies As List(Of Netpay.Bll.Currency)
			Get
                Dim ret = Netpay.Bll.Currency.Cache.OrderBy(Function(x) x.ID).ToList()
                If Not Netpay.Infrastructure.Domain.Current.IsHebrewVisible Then ret = ret.Skip(1).ToList()
                Return ret
            End Get
		End Property
    End Class
End Namespace

