﻿Imports Microsoft.VisualBasic
Imports Netpay.Bll
Imports System.Collections.Generic
Imports System.Linq
Imports Netpay.Infrastructure

Namespace Code
	Public Class NetpayPage
        Inherits BasePage
        Private _authorization As Authorization = Nothing

        'Private Shared _freeAccessPages As List(Of String) = Nothing

        'Private Shared ReadOnly Property FreeAccessPages() As List(Of String)
        '	Get
        '		If _freeAccessPages Is Nothing Then
        '			_freeAccessPages = New List(Of String)
        '			_freeAccessPages.Add("default.aspx")
        '			_freeAccessPages.Add("login.aspx")
        '			_freeAccessPages.Add("permissiondenied.aspx")
        '			_freeAccessPages.Add("testenvironment.aspx")
        '			_freeAccessPages.Add("List_ReplyCodes.aspx")
        '			_freeAccessPages.Add("List_IsoCodes.aspx")
        '			_freeAccessPages.Add("states.aspx")
        '			_freeAccessPages.Add("codinghelp.aspx")
        '			_freeAccessPages.Add("recurringinfo.aspx")
        '			_freeAccessPages.Add("silentpost_cc.aspx")
        '			_freeAccessPages.Add("hostedpage_about.aspx")
        '			_freeAccessPages.Add("hostedpage_example.aspx")
        '			_freeAccessPages.Add("hostedpage_msgfields.aspx")
        '			_freeAccessPages.Add("hostedpagev2_about.aspx")
        '			_freeAccessPages.Add("hostedpagev2_example.aspx")
        '			_freeAccessPages.Add("hostedpagev2_msgfields.aspx")
        '			_freeAccessPages.Add("publicpage_about.aspx")
        '			_freeAccessPages.Add("silentpost_echeck.aspx")
        '			_freeAccessPages.Add("silentpost_ddebit.aspx")
        '			_freeAccessPages.Add("verifytrans_about.aspx")
        '			_freeAccessPages.Add("verifytrans_msgfields.aspx")
        '			_freeAccessPages.Add("clientreg_msgfields.aspx")
        '			_freeAccessPages.Add("ccstorage_msgfields.aspx")
        '			_freeAccessPages.Add("recurring_sending.aspx")
        '			_freeAccessPages.Add("recurring_reply.aspx")
        '			_freeAccessPages.Add("refund_reply.aspx")
        '			_freeAccessPages.Add("refund_sending.aspx")
        '			_freeAccessPages.Add("wallet.aspx")
        '			_freeAccessPages.Add("Cart_MsgFields.aspx")
        '		End If
        '		Return _freeAccessPages
        '	End Get
        'End Property

        Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
			If Not IsAuthorized() Then Response.End()
			MyBase.OnInitComplete(e)
		End Sub

		Public Function IsAuthorized() As Boolean
			Dim pageFilePath As String = System.IO.Path.GetFileName(Request.Path)
			Return IsAuthorized(pageFilePath)
		End Function

		Public Function IsAuthorized(pageFileName As String) As Boolean
			pageFileName = pageFileName.Trim().ToLower()
			If DevMasterPage.FreeAccessPages.Contains(pageFileName) Then Return True
			If Not IsLoggedin Then Return False
			If LoggedUser.Role = Infrastructure.Security.UserRole.Merchant Then Return True
			Return False
			'Return LoggedUser.AccessList.Where(Function(po) po.Name.ToLower() = pageFileName And po.Type = PermissionObjectType.WebPage).SingleOrDefault() IsNot Nothing
		End Function

		Public ReadOnly Property Domain As Netpay.Infrastructure.Domain
			Get
				Return Netpay.Infrastructure.Domain.Current
			End Get
		End Property

		Public ReadOnly Property Account As Bll.Accounts.Account
			Get
				Return Bll.Accounts.Account.Current
			End Get
		End Property

		Public ReadOnly Property LimitedLogin As Bll.Accounts.LimitedLogin
			Get
				Return Bll.Accounts.LimitedLogin.Current
			End Get
		End Property

        Public ReadOnly Property Merchant As Bll.Merchants.Merchant
            Get
                Return Bll.Merchants.Merchant.Current
            End Get
        End Property

        Public ReadOnly Property MerchantNumber As String
            Get
                If (Merchant Is Nothing) Then Return "#######"
                Return Merchant.Number
            End Get
        End Property

        Public Property Authorization As Authorization
            Get
                If _authorization Is Nothing Then
                    If (Merchant Is Nothing) Then _authorization = New Authorization() _
                    Else _authorization = New Authorization(Merchant.ID)
                End If
                Return _authorization
            End Get
            Set
                _authorization = Nothing
            End Set
        End Property


    End Class
End Namespace


