Imports Microsoft.VisualBasic
Imports system.Data.SqlClient

Namespace Code
    Public Enum AuthorizationProperty
        None
        AllowRecurring
        AllowRemotePull
        ApprovalOnly
        AskRefund
        DataStoring
        ElectronicWallet
        HostedPayment
        HostedPaymentEcheck
        HostedPaymentV2
        HostedPaymentV2CreditCard
        HostedPaymentV2Echeck
        HostedPaymentV2Instant
        HostedPaymentV2Wallet
        HostedPaymentV2SignatureOptional
        PublicPayment
        RemoteCharge
        RemoteChargeEcheck
        RemoteSignup
        StoreCreditCards
        VirtualTerminal
        VirtualTerminalEcheck
        InvoiceEnabled
        AllowRecurringModify
    End Enum

    Public Class Authorization
        Private _values As Dictionary(Of String, Boolean) = New Dictionary(Of String, Boolean)()
        Public Sub New()

        End Sub

        Public Sub New(id As Integer)
            Dim sql As String = "SELECT ID, CompanyName, CustomerNumber, IsNull(i.IsEnable, 0) InvoiceEnabled, " &
             " IsNull(h.IsEnabled, 0) IsHostedV2, IsNull(h.IsCreditCard, 0) IsHostedV2CreditCard, IsNull(IsECheck, 0) IsHostedV2ECheck, IsNull(IsDirectDebit, 0) IsHostedV2Instant," &
             " IsNull(IsCustomer, 0) IsHostedV2Wallet, IsNull(IsSignatureOptional, 0) IsHostedV2SignatureOptional," &
             " IsApprovalOnly, IsAskRefund, IsAskRefundRemote, IsCustomerPurchase, IsCustomerPurchaseEcheck, IsPublicPay," &
             " IsRemoteCharge, IsRemoteChargeEcheck, IsSystemPay, IsSystemPayEcheck, IsCustomerPurchasePayerID," &
             " IsWindowChargeEwallet, IsCcStorage, IsAutoPersonalSignup, IsAllowRemotePull, RemotePullIPs, GD_Text," &
             " IsNull(r.IsEnabled, 0) IsAllowRecurring, IsNull(r.IsEnabledModify, 0) IsAllowRecurringModify" &
             " FROM tblCompany m LEFT JOIN tblGlobalData s ON s.GD_Group=" & GlobalDataGroup.MERCHANT_STATUS & " AND s.GD_ID=ActiveStatus" &
             " LEFT JOIN tblCompanySettingsHosted h ON m.ID=h.CompanyID" &
             " LEFT JOIN Setting.SetMerchantInvoice i ON m.ID=i.Merchant_id" &
             " LEFT JOIN tblMerchantRecurringSettings r ON m.ID=r.MerchantID" &
             " WHERE m.ID = " & id
            Dim reader As SqlDataReader = dbPages.ExecReader(sql)
            If reader.Read() Then Load(reader)
            reader.Close()
        End Sub

        Public Property IsAllowed(ByVal index As AuthorizationProperty) As Boolean
            Get
                Dim ret As Boolean
                If Not _values.TryGetValue("AuthorizationProperty" & index, ret) Then Return False
                Return ret
            End Get
            Set(ByVal value As Boolean)
                _values("AuthorizationProperty" & index) = value
            End Set
        End Property

        Public ReadOnly Property IsAnyHostedV2Enabled() As Boolean
            Get
                Return IsAllowed(AuthorizationProperty.HostedPaymentV2CreditCard) Or IsAllowed(AuthorizationProperty.HostedPaymentV2Echeck) Or IsAllowed(AuthorizationProperty.HostedPaymentV2Instant)
            End Get
        End Property

        Private Sub Load(ByVal drData As SqlDataReader)
            If Not WebUtils.IsLoggedin And Not HttpContext.Current.Request.RawUrl.ToLower.Contains("/login.aspx") Then
                HttpContext.Current.Server.Transfer("/website/login.aspx", False)
            End If
            IsAllowed(AuthorizationProperty.AllowRecurring) = drData("IsAllowRecurring")
            IsAllowed(AuthorizationProperty.ApprovalOnly) = drData("IsApprovalOnly")
            IsAllowed(AuthorizationProperty.RemoteCharge) = drData("IsRemoteCharge")
            IsAllowed(AuthorizationProperty.RemoteChargeEcheck) = drData("IsRemoteChargeEcheck")
            IsAllowed(AuthorizationProperty.VirtualTerminal) = drData("IsSystemPay")
            IsAllowed(AuthorizationProperty.VirtualTerminalEcheck) = drData("IsSystemPayEcheck")
            IsAllowed(AuthorizationProperty.HostedPayment) = drData("IsCustomerPurchase")
            IsAllowed(AuthorizationProperty.HostedPaymentEcheck) = drData("IsCustomerPurchaseEcheck")
            IsAllowed(AuthorizationProperty.HostedPaymentV2) = drData("IsHostedV2")
            IsAllowed(AuthorizationProperty.HostedPaymentV2CreditCard) = drData("IsHostedV2CreditCard")
            IsAllowed(AuthorizationProperty.HostedPaymentV2Echeck) = drData("IsHostedV2ECheck")
            IsAllowed(AuthorizationProperty.HostedPaymentV2Instant) = drData("IsHostedV2Instant")
            IsAllowed(AuthorizationProperty.HostedPaymentV2Wallet) = drData("IsHostedV2Wallet")
            IsAllowed(AuthorizationProperty.HostedPaymentV2SignatureOptional) = drData("IsHostedV2SignatureOptional")
            IsAllowed(AuthorizationProperty.PublicPayment) = drData("IsPublicPay")
            IsAllowed(AuthorizationProperty.DataStoring) = drData("IsCustomerPurchasePayerID")
            IsAllowed(AuthorizationProperty.ElectronicWallet) = drData("IsWindowChargeEwallet")
            IsAllowed(AuthorizationProperty.StoreCreditCards) = drData("IsCCStorage")
            IsAllowed(AuthorizationProperty.RemoteSignup) = drData("IsAutoPersonalSignup")
            IsAllowed(AuthorizationProperty.AllowRemotePull) = drData("IsAllowRemotePull") And Not String.IsNullOrEmpty(drData("RemotePullIPs").ToString.Trim)
            IsAllowed(AuthorizationProperty.AskRefund) = drData("IsAskRefund") And drData("IsAskRefundRemote")
            IsAllowed(AuthorizationProperty.InvoiceEnabled) = drData("InvoiceEnabled")
            IsAllowed(AuthorizationProperty.AllowRecurringModify) = drData("IsAllowRecurring") And drData("IsAllowRecurringModify")
        End Sub

    End Class
End Namespace

