	// text validation object
	function textValidation(instance, friendlyName, type, minLength, maxLength, isRequired)
	{
		this.errMsg = "";
		this.lang = "english";

		// check if the instance exists
		if(instance == null)
		{
			alert(friendlyName + " doesn't seem to exist on this page");
			return;
		}
		
		this.setLang = function(lang)
		{
			this.lang = lang; 
		}
		
		this.getInstance = function()
		{
			return instance;
		}

		this.eventHandler = function()
		{	
			var keyCode = event.keyCode;
			if(keyCode >= 96 && keyCode <= 105)
			{
				keyCode = keyCode - 48 ;
			}			
			
			var key = String.fromCharCode(keyCode);

			// enable allowed keys 
			if(isAllowedKey(keyCode, event.ctrlKey))
			{
				return true;
			}

			// disable input that exceeds max length
			if(instance.value.length == maxLength)
			{		
				return false;
			}	
					
			// enable only numeric input
			if(type == "numeric")
			{
				// enable dot
				if(keyCode == 46 || keyCode == 110 || keyCode == 190)
				{
					return true;
				}
				
				// enable numbers
				var reg = new RegExp("[0-9]");
				return (reg.test(key));
			}
			
			return true;		
		}
		
		instance.onkeydown = this.eventHandler;
	
		this.validate = function()
		{
			if(instance.value == "" && isRequired)
			{
				if(this.lang == "hebrew")
				{
					this.errMsg = " ��� ���� " + friendlyName
				}
				else
				{
					this.errMsg = friendlyName + " is required";
				}
				
				return false;			
			}
	
			if(instance.value != "" && instance.value.length < minLength)
			{
				if(this.lang == "hebrew")
				{
					this.errMsg = friendlyName + " ���� ����� ����� " + minLength + " ����� "  
				}
				else
				{
					this.errMsg = friendlyName + " must contain at least " + minLength + " characters";
				}

				return false;	
			}
			
			if(instance.value.length > maxLength)
			{
				if(this.lang == "hebrew")
				{
					this.errMsg = friendlyName + " ���� ���� ����� ���� � " + maxLength + " ����� "  
				}
				else
				{
					this.errMsg = friendlyName + " can not exceed " + maxLength + " characters";
				}

				return false;	
			}	

			if(instance.value.length > 0)
			{
				switch(type)
				{
					case "email":
						var reg = new RegExp("^[\\w-_\.]*[\\w-_\.]\@[\\w]\.+[\\w]+[\\w]$" );
						if(! reg.test(instance.value))
						{
							if(this.lang == "hebrew")
							{
								this.errMsg = friendlyName + " ���� ���� "  
							}
							else
							{
								this.errMsg = friendlyName + " is invalid";
							}
							
							return false
						}
						
						break;
					default:
						break;
				}
			}
		
			return true;
		}
	}
	
	// validation object (deprecated)
	function validationObj(instance, friendlyName, type, minLength, maxLength)
	{
		this.errMsg = "";
		this.lang = "english";
		
		// check if the instance exists
		if(instance == null)
		{
			alert(friendlyName + " doesn't seem to exist on this page");
			return;
		}
		
		this.setLang = function(lang)
		{
			this.lang = lang; 
		}
		
		this.getInstance = function()
		{
			return instance;
		}

		this.eventHandler = function()
		{
			var keyCode = event.keyCode;
			if(keyCode >= 96 && keyCode <= 105)
			{
				keyCode = keyCode - 48 ;
			}			
			
			var key = String.fromCharCode(keyCode);

			// enable allowed keys  
			if(isAllowedKey(keyCode, event.ctrlKey))
			{
				return true;
			}

			// disable input that exceeds max length
			if(instance.value.length == maxLength)
			{		
				return false;
			}	
					
			// enable only numeric input
			if(type == "numeric")
			{
				// enable dot
				if(keyCode == 46 || keyCode == 110 || keyCode == 190)
				{
					return true;
				}
				
				// enable numbers
				var reg = new RegExp("[0-9]");
				return (reg.test(key));
			}
			
			return true;	
		}
		
		instance.onkeydown = this.eventHandler;
	
		this.validate = function()
		{
			if(instance.value == "" && minLength > 0)
			{
				if(this.lang == "hebrew")
				{
					this.errMsg = " ��� ���� " + friendlyName
				}
				else
				{
					this.errMsg = friendlyName + " is required";
				}
				
				return false;			
			}
			
			if(instance.value.length > maxLength)
			{
				if(this.lang == "hebrew")
				{
					this.errMsg = friendlyName + " ���� ���� ����� ���� � " + maxLength + " ����� "  
				}
				else
				{
					this.errMsg = friendlyName + " can not exceed " + maxLength + " characters";
				}

				return false;	
			}	
					
			if(instance.value.length < minLength)
			{
				if(this.lang == "hebrew")
				{
					this.errMsg = friendlyName + " ���� ����� ����� " + minLength + " ����� "  
				}
				else
				{
					this.errMsg = friendlyName + " must contain at least " + minLength + " characters";
				}

				return false;	
			}

			switch(type)
			{
				case "email":
					var reg = new RegExp("^[\\w-_\.]*[\\w-_\.]\@[\\w]\.+[\\w]+[\\w]$" );
					if(! reg.test(instance.value))
					{
						if(this.lang == "hebrew")
						{
							this.errMsg = friendlyName + " ���� ���� "  
						}
						else
						{
							this.errMsg = friendlyName + " is invalid";
						}
						
						return false
					}
					
					break;
				default:
					break;
			}
		
			return true;
		}
	}

	// check box validation object
	function checkBoxValidation(instance, errMsg)
	{
		this.errMsg = errMsg;
		this.lang = "english";	
		
		// check if the instance exists
		if(instance == null)
		{
			alert(errMsg + " doesn't seem to exist on this page");
			return;
		}
		
		this.setLang = function(lang)
		{
			this.lang = lang; 
		}
		
		this.getInstance = function()
		{
			return instance;
		}
		
		this.getValue = function()
		{			
			return instance.checked;
		}		

		this.validate = function()
		{
			return this.getValue();
		}
	}	
	
	// select validation object
	function selectValidation(instance, friendlyName)
	{
		this.errMsg = "";
		this.lang = "english";	
		
		// check if the instance exists
		if(instance == null)
		{
			alert(friendlyName + " doesn't seem to exist on this page");
			return;
		}
		
		this.setLang = function(lang)
		{
			this.lang = lang; 
		}
		
		this.getInstance = function()
		{
			return instance;
		}
		
		this.getValue = function()
		{			
			return instance.value;
		}		

		this.validate = function()
		{
			if (this.getValue() == "" || this.getValue() == "0")
			{
				if(this.lang == "hebrew")
				{
					this.errMsg = " ��� ��� " + friendlyName
				}
				else
				{
					this.errMsg = friendlyName + " is required";
				}
				
				return false;				
			}	
			
			return true;
		}
	}
	
	// cc date validation object
	function ccDateValidation(mmInstance, yyInstance)
	{
		this.errMsg = "";
		this.lang = "english";	
		
		// check if the instance exists
		if(mmInstance == null || yyInstance == null)
		{
			alert("cc date instance doesn't seem to exist on this page");
			return;
		}
		
		this.setLang = function(lang)
		{
			this.lang = lang; 
		}
		
		this.getInstance = function()
		{
			return mmInstance;
		}
		
		this.getValue = function()
		{			
			// month set is one month back, so this month will still be valid
			return new Date(yyInstance.value, (mmInstance.value), 1);
		}		

		this.validate = function()
		{
			if (this.getValue() < new Date())
			{
				if(this.lang == "hebrew")
				{
					this.errMsg = "����� ������ ���� �����"
				}
				else
				{
					this.errMsg = "Card has expired";
				}
				
				return false;				
			}		
			
			return true;
		}
	}
	
	// cc validation object
	function ccValidation(instanceBase, friendlyName)
	{
		this.errMsg = "";
		this.lang = "english";
		this.instance = new Array(4);
		for(var i = 0; i <= 3; i++)
		{
			this.instance[i] = eval(instanceBase + (i + 1))
			
			// check if the instance exists
			if(this.instance[i] == null)
			{
				alert(friendlyName + " doesn't seem to exist on this page");
				return;
			}
		}

		var ISFIREFOX = false
		this.eventHandler = function()
		{
			evnt = (ISFIREFOX) ? Event : event;
			var keyCode = (ISFIREFOX) ? evnt.charCode : evnt.keyCode;
			//alert(Event.charCode);
			if(keyCode >= 96 && keyCode <= 105)
			{
				keyCode = keyCode - 48 ;
			}			
			var key = String.fromCharCode(keyCode);

			// enable allowed keys 
			if(isAllowedKey(keyCode, evnt.ctrlKey))
			{
				return true;
			}
					
			// enable only numeric input
			var reg = new RegExp("[0-9]");
			return (reg.test(key));
			
			return true;
		}
		
		// bind event handler
		for(var i = 0; i <= 3; i++)
		{
			this.instance[i].onkeydown = this.eventHandler;
		}
		
		this.setLang = function(lang)
		{
			this.lang = lang; 
		}
		
		this.getInstance = function()
		{
			return this.instance[0];
		}
		
		this.getValue = function()
		{
			val = "";
			for(var i = 0; i <= 3; i++)
			{
				val += this.instance[i].value; 
			}
			
			return val;
		}		
	
		this.validate = function()
		{
			if (this.getValue() == "")
			{
				if(this.lang == "hebrew")
				{
					this.errMsg = " ��� ���� " + friendlyName
				}
				else
				{
					this.errMsg = friendlyName + " is required";
				}
				
				return false;				
			}		
			
			return true;
		}
	}
	
	// validation manager object
	function validationManager(lang)
	{
		this.container = new Array();

		this.add = function(validationObject)
		{
			if(validationObject.getInstance != null)
			{
				validationObject.setLang(lang);
				this.container.push(validationObject);
			}
		}
		
		this.validate = function()
		{
			isValid = true;
			errMsg = "";
			
			for(var i = 0; i < this.container.length; i++)
			{
				currentObj = this.container[i];

				//var d = document.getElementById('myDiv');
				//var olddiv = document.getElementById(divNum);
				//d.removeChild(olddiv);
				

				if(! currentObj.validate())
				{
					/*
					var newDivName = (currentObj.id + "ErrDiv");
					var newDiv = document.createElement('div');
					newDiv.setAttribute("id", newDivName);
					newDiv.innerHTML = currentObj.errMsg;
					//newDiv.style.position = "static";
					//newDiv.style.display = "inline";
					newDiv.style.backgroundColor = "lightyellow";
					newDiv.style.border = "1px solid black";
					newDiv.style.fontFamily = "arial";
					newDiv.style.fontSize = "10pt";
					newDiv.style.padding = "1px 1px 1px 1px";
					newDiv.style.width = "50%";
					//newDiv.style.left = findPosX(currentObj.getInstance());
					//newDiv.style.top = (findPosY(currentObj.getInstance()) - newDiv.offsetHeight);
					currentObj.getInstance().parentNode.appendChild(newDiv);
					//document.body.appendChild(newDiv);
					*/

					isValid = false;
					errMsg += currentObj.errMsg + "\n";
					
					// start workaround to return only the first err and focus the instance
					// remove this to return a list of errs
					currentObj.getInstance().focus();
					alert(errMsg);
					
					return false;
					// end workaround
				}
			}
			
			if(isValid)
			{
				return true;
			}
			else
			{
				alert(errMsg);
				return false;
			}
		}
	}
	
	// returns if the key is allowed
	function isAllowedKey(keyCode, isCtrl)
	{
		if (isCtrl)
		{
			// check for ctrl keys, such as control paste
			allowedKeyArr = new Array();
			allowedKeyArr.push(86);	// ctrl	v	
			allowedKeyArr.push(88);	// ctrl	x
			allowedKeyArr.push(67);	// ctrl	c	
			allowedKeyArr.push(65);	// ctrl	a
			
			for (i = 0; i < allowedKeyArr.length; i++)
			{
				if (keyCode == allowedKeyArr[i])
				{
					return true; 
				}
			}			
		}
		else
		{
			// check for single keys
			allowedKeyArr = new Array();
			allowedKeyArr.push(8); // backspace
			allowedKeyArr.push(9); // tab
			allowedKeyArr.push(46); // delete
			allowedKeyArr.push(27); // escape
			allowedKeyArr.push(37); // left arrow
			allowedKeyArr.push(39);	// right arrow			

			for (i = 0; i < allowedKeyArr.length; i++)
			{
				if (keyCode == allowedKeyArr[i])
				{
					return true; 
				}
			}		
		}

		return false;
	}
	
	
	function findPosX(obj)
	{
		var curleft = 0;
		if (obj.offsetParent)
		{
			while (obj.offsetParent)
			{
				curleft += obj.offsetLeft
				obj = obj.offsetParent;
			}
		}
		else if (obj.x)
		{
			curleft += obj.x;
		}
			
		return curleft;
	}

	function findPosY(obj)
	{
		var curtop = 0;
		if (obj.offsetParent)
		{
			while (obj.offsetParent)
			{
				curtop += obj.offsetTop
				obj = obj.offsetParent;
			}
		}
		else if (obj.y)
		{
			curtop += obj.y;
		}
		
		return curtop;
	}
	
