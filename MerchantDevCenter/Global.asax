<%@ Application Language="VB" %>
<%@ Import Namespace="Netpay.Web" %>
<%@ Import Namespace="Netpay.Infrastructure" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="System.Threading" %>
<script runat="server">
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        Netpay.Web.ContextModule.AppName = "Dev Center"
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Dim ex As Exception = Server.GetLastError()
        If (TypeOf ex Is System.Web.HttpException) Then
            If (ex.Message.StartsWith("Validation of viewstate MAC failed.")) Then
                Response.Redirect("~/Website/Login.aspx")
                Exit Sub
            End If
        End If
        Netpay.Infrastructure.Logger.Log(LogTag.DevCenter, ex)
    End Sub

    Sub Application_PreRequestHandlerExecute(ByVal sender As Object, ByVal e As EventArgs)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-us")
    End Sub

    Public Sub Application_AcquireRequestState(ByVal sender As Object, ByVal e As EventArgs)
        If Request.IsSecureConnection Then Exit Sub
        If Request.ServerVariables("HTTP_HOST").StartsWith("192.168") Or Request.ServerVariables("HTTP_HOST") = "127.0.0.1" _
         Or Request.ServerVariables("HTTP_HOST") = "localhost" Or Request.ServerVariables("HTTP_HOST").StartsWith("80.179.180.") _
         Or Request.ServerVariables("HTTP_HOST") = "merchantstest.netpay-intl.com" Then Exit Sub
        If WebUtils.CurrentDomain.ForceSSL Then Response.Redirect("https://" & Request.Url.ToString().Substring(7))
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        Dim app = TryCast(sender, System.Web.HttpApplication)
        If (app IsNot Nothing And app.Context IsNot Nothing) Then
            app.Context.Response.Headers.Remove("Server")
        End If
    End Sub
</script>