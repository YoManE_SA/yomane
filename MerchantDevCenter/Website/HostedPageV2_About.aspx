<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
	Sub Page_Load()
		If Not Page.IsPostBack Then
            If (Authorization.IsAnyHostedV2Enabled) Then
                Select Case dbPages.TestVar(Request("T"), 0, -1, 1)
                    Case "1"
                        aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2CreditCard
                        aa.formatFeatureMessage("Credit Card Hosted Page")
                    Case "2"
                        aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Echeck
                        aa.formatFeatureMessage("Echeck Hosted Page")
                    Case "3"
                        aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Instant
                        aa.formatFeatureMessage("Alternative payment methods Hosted Page")
                End Select
            Else
                aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2CreditCard
            End If
        End If
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">HOSTED PAYMENT PAGE VER.2</span>
			<span class="PageSubHeading">- ABOUT</span><br />
			<custom:AuthAlert ID="aa" PropertyToCheck="HostedPaymentV2" runat="server" />
		</td>
		<td valign="top" align="right">
		</td>
	</tr>		
	<tr>
		<td colspan="2" valign="top">
			<br />
			<table cellspacing="0" cellpadding="1" border="0" width="100%">
			<tr>
				<td>
					Hosted Payments Page is suitable for all web sites that need to process credit card payments in real-time.<br />
					We host and manage the payment page, which is secured by it's own SSL certificate and customisable via an online wizard.<br />
					Suited to all sizes of merchants, it is highly cost effective and operates on an extremely robust infrastructure.<br />
					Supports 3D verification with authentication of "Verified by Visa" and "MasterCard SecureCode".<br />
					<ul class="aboutList">
						<li>Supports all transaction types</li>
						<li>Multilanguage - speak to your customers in their own language</li>
						<li>3D Secure capability</li>
						<li>Cost savings - does not require an SSL certificate</li>
						<li>Ensures merchants are compliant with scheme regulations (PCI DSS compliant solution)</li>
					</ul>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
</asp:Content>