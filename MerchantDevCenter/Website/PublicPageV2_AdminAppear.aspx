<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  EnableEventValidation="false" validateRequest="False" Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" %>

<%@ Import Namespace="Netpay.CommonTypes" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<script runat="server">
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not IsPostBack Then DataLoad()
        LoadLanguages()
    End Sub

    Private Sub DataLoad()
        Dim nValues = Netpay.Bll.Merchants.Hosted.Translation.GetTranslations(Merchant.ID, 0, TranslationGroup.PublicPaymentPage)
        Dim bImageExist As Boolean = False
        Dim ppv As PublicPayOptionsVO = PublicPayment.GetOptions(WebUtils.CredentialsToken)
        If ppv Is Nothing Then ppv = New PublicPayOptionsVO()
        If ppv.IsAsk4PersonalInfo Then isAsk4PersonalInfo1.Checked = True Else isAsk4PersonalInfo2.Checked = True
        Dim rgl As RadioButton() = {isChargeOptions1, isChargeOptions2, isChargeOptions3}
        rgl(ppv.ChargeOptions).Checked = True
    End Sub

    Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ppv As PublicPayOptionsVO = PublicPayment.GetOptions(WebUtils.CredentialsToken)
        If ppv Is Nothing Then ppv = New PublicPayOptionsVO()
        ppv.IsAsk4PersonalInfo = isAsk4PersonalInfo1.Checked
        ppv.ChargeOptions = dbPages.TestVar(Request("ctl00$ContentPlaceHolder1$isChargeOptions"), 0, -1, 0)
        PublicPayment.SaveOptions(WebUtils.CredentialsToken, ppv)
        For Each tab As TabView In tbLanguageText.Tabs
            Merchants.SetTranslation(WebUtils.CredentialsToken, tab.ItemData, TranslationGroup.PublicPaymentPage, "FrontTop", CType(tab.FindControl("txtLanguageText1"), TextBox).Text)
            Merchants.SetTranslation(WebUtils.CredentialsToken, tab.ItemData, TranslationGroup.PublicPaymentPage, "FrontBottom", CType(tab.FindControl("txtLanguageText2"), TextBox).Text)
            'mshl.IsDefault = CType(tab.FindControl("chkLanguageIsDefault"), CheckBox).Checked
        Next
    End Sub

    Private Function InstantiateLanguageTab(lng As Netpay.CommonTypes.Language) As TabView
        gtLanguageTab.Template.InstantiateIn(tbLanguageText)
        Dim tab As TabView = tbLanguageText.Tabs(tbLanguageText.Tabs.Count - 1)
        tab.ID = "Lng" & lng
        tab.ItemData = lng
        tab.Text = WebUtils.DomainCache.Languages(lng).EnglishName
        Return tab
    End Function

    Private Sub LoadLanguages()
        tbLanguageText.Tabs.Clear()
        Dim hppLanguages = System.Configuration.ConfigurationManager.AppSettings("HppV2Languages").Split(",")
        Dim allLanguages = WebUtils.DomainCache.Languages.Values.Where(function(l) hppLanguages.Contains(l.Culture)).ToDictionary(function(l) l.Language)
        Dim availLanguages as New List(Of String)
        For Each l In allLanguages
            Dim texts = Merchants.GetTranslations(WebUtils.CredentialsToken, l.Key, TranslationGroup.PublicPaymentPage)
            If texts IsNot Nothing Then
                Dim tab As TabView = InstantiateLanguageTab(l.Key)
                If texts.ContainsKey("FrontTop") Then CType(tab.FindControl("txtLanguageText1"), TextBox).Text = texts("FrontTop")
                If texts.ContainsKey("FrontBottom") Then CType(tab.FindControl("txtLanguageText2"), TextBox).Text = texts("FrontBottom")
                CType(tab.FindControl("ddlRemoveLanguage"), Button).Text = "Remove " + l.Key.ToString()
                availLanguages.Add(l.Key)
            End If
        Next
        If tbLanguageText.Tabs.Count > 0 And Not IsPostBack Then tbLanguageText.ActiveViewIndex = 0
        For each l in availLanguages
            allLanguages.Remove(l)
        Next
        Dim oldValue = ddlLanguage.SelectedValue
        ddlLanguage.DataSource = allLanguages.Values
        ddlLanguage.DataTextField = "EnglishName"
        ddlLanguage.DataValueField = "LanguageID"
        ddlLanguage.DataBind()
        ddlLanguage.SelectedValue = oldValue
    End Sub

    Private Sub AddLanguage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlLanguage.SelectedIndex < 0 Then Return
        Merchants.SetTranslation(WebUtils.CredentialsToken, ddlLanguage.SelectedValue, TranslationGroup.PublicPaymentPage, "FrontTop", "")
        Merchants.SetTranslation(WebUtils.CredentialsToken, ddlLanguage.SelectedValue, TranslationGroup.PublicPaymentPage, "FrontBottom", "")
        Dim tab As TabView = InstantiateLanguageTab(ddlLanguage.SelectedValue)
        tbLanguageText.ActiveViewIndex = tbLanguageText.Tabs.Count - 1
        ddlLanguage.Items.RemoveAt(ddlLanguage.SelectedIndex)
    End Sub

    Private Sub RemoveLanguage_Command(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim tab As TabView = tbLanguageText.Tabs(tbLanguageText.ActiveViewIndex)
        ddlLanguage.Items.Add(New ListItem(tab.Text, tab.ItemData))
        tbLanguageText.Tabs.Remove(tab)
        Merchants.DeleteTranslation(WebUtils.CredentialsToken, CType(tab.ItemData, Netpay.CommonTypes.Language), TranslationGroup.PublicPaymentPage)
    End Sub

    Private Sub iuLogo_OnDataUpdate(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ppv As PublicPayOptionsVO = PublicPayment.GetOptions(WebUtils.CredentialsToken)
        If ppv Is Nothing Then ppv = New PublicPayOptionsVO()
        PublicPayment.SaveOptions(WebUtils.CredentialsToken, ppv)
    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">PUBLIC PAYMENT PAGE</span>
			<span class="PageSubHeading">- MANAGE APPEARANCE</span><br />
		</td>
	</tr>
	<tr>
		<td><custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" /></td>
	</tr>
	<tr>
		<td>
			<br/><br/>
			This page enables you to define the appearance of the public payment page.<br/><br/>
		</td>
	</tr>
	<tr><td height="8"></td></tr>
	<tr>
		<td colspan="2">
            <netpay:TabControl runat="server" ID="tbLanguageText" Width="100%" CssStyle="TabControl" UseJQuery="true">
                <TabSpace>
                    <asp:DropDownList runat="server" id="ddlLanguage" />
                    &nbsp;
                    <asp:Button runat="server" id="ddlAddLanguage" Text="Add" OnClick="AddLanguage_Click" UseSubmitBehavior="true" />
                </TabSpace>
            </netpay:TabControl>
            <netpay:GenericTemplate runat="server" id="gtLanguageTab">
                <netpay:TabView runat="server" Text="English">
                    <table width="93%">
			            <tr>
				            <td valign="top">Text to be shown<br /> above items list<br /></td>
				            <td valign="top" width="80%">
					            <asp:TextBox runat="server" Rows="3" Style="width:100%" ID="txtLanguageText1" MaxLength="1500" TextMode="MultiLine" /><br />
				            </td>
			            </tr>
			            <tr><td height="20"></td></tr>
			            <tr>
				            <td valign="top">Text to be shown<br /> below items list<br /></td>
				            <td valign="top">
					            <asp:TextBox runat="server" Rows="3" Style="width:100%" ID="txtLanguageText2" MaxLength="1500" TextMode="MultiLine" /><br />
				            </td>
			            </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button runat="server" id="ddlRemoveLanguage" Text="Remove" style="float:right;" OnCommand="RemoveLanguage_Command" OnClientClick="if(!confirm('Are you sure you wish to remove this language data?')) return false;" />
                            </td>
                        </tr>
                    </table>
                </netpay:TabView>
            </netpay:GenericTemplate>
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr><td><br /></td></tr>
			<tr><td align="left" style="font-weight:bold;">Personal Details</td></tr>
			<tr>
				<td valign="top">Choose if you want to present the client with personal details fields.<br /></td>
			</tr><tr>
				<td>
					<asp:RadioButton runat="server" GroupName="Ask4PersonalInfo" id="isAsk4PersonalInfo1" /> Yes, show personal details fields.<br/>
					<input type="checkbox" style="visibility:hidden;"/> [<a class="faq" style="font-size: 11px;" href="PublicPageV2_AdminAppearInfo.aspx" >Define Fields</a>]<br/>
					<asp:RadioButton runat="server" GroupName="Ask4PersonalInfo" id="isAsk4PersonalInfo2" /> No, do not show personal details fields.<br/>
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr><td align="left" style="font-weight:bold;">Payment Options</td></tr>
			<tr>
				<td valign="top">Define payment options, or let the client decide for himself.<br /></td>
			</tr><tr>
				<td valign="top">
					<asp:RadioButton runat="server" ID="isChargeOptions2" GroupName="isChargeOptions" value="1" /> Yes, let me define the payment options.<br/>
					<input type="checkbox" style="visibility:hidden;"/> [<a class="faq" style="font-size: 11px;"  href="PublicPageV2_AdminAppearChargeList.aspx" >Define Options</a>]<br/>
					<asp:RadioButton runat="server" ID="isChargeOptions1" GroupName="isChargeOptions" value="0" /> No, let the client decide the<br/>
					<input type="checkbox" style="visibility:hidden;"/> payment options. [<a class="faq" style="font-size: 11px;"  href="PublicPageV2_AdminAppearChargeFree.aspx" >Define Options</a>]<br/>
					<asp:RadioButton runat="server" ID="isChargeOptions3" GroupName="isChargeOptions" value="2" /> Show my options, and let the client<br/>
					<input type="checkbox" style="visibility:hidden;" /> decide the payment options.<br/>
				</td>
			</tr>
			<tr>
				<td colspan="3"></td>
				<td>
					<br/>
					<asp:Button runat="server" UseSubmitBehavior="true" id="btnUpdate" Text=" Update " class="submit" OnClick="btnUpdate_Click" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
</asp:Content>