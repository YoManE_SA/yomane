<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - PAYMENT METHOD STORAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
    Dim sBgColor as String = ""
    Dim IsCustomerPurchasePayerID, IsApprovalOnly As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If WebUtils.IsLoggedin Then
            Dim sSQL As String = "SELECT IsCustomerPurchasePayerID, IsApprovalOnly " &
                " FROM tblCompany WHERE ID = " & Merchant.ID
            Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
            If iReader.Read() Then
                IsCustomerPurchasePayerID = iReader("IsCustomerPurchasePayerID")
                If Not IsCustomerPurchasePayerID Then
                    lblIsCustomerPurchasePayerID.Text = "<span style=""color:maroon;"">This option is not active - Contact support to activate.</span><br />"
                Else
                    lblIsCustomerPurchasePayerID.Visible = False
                End If

                IsApprovalOnly = iReader("IsApprovalOnly")
                If Not IsApprovalOnly Then
                    'lblIsApprovalOnly.Text = "<span style=""color:maroon;"">Authorization only is not available, contact " & WebUtils.CurrentDomain.BrandName & " to activate</span><br />"
                Else
                    'lblIsApprovalOnly.Text = ""
                    'lblIsApprovalOnly.Visible = False
                End If
            End If
            iReader.Close()
        Else
            'lblIsApprovalOnly.Text = "<span style=""color:maroon;"">Not Applicable to Guest Login</span><br/>"
            lblIsCustomerPurchasePayerID.Text = "<span style=""color:maroon;"">Not Applicable to Guest Login</span><br/>"
        End If
        If Not Page.IsPostBack Then
            Select Case Request("T")
                Case "2" : aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Echeck
                Case "3" : aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Instant
            End Select
            litSignatureRequired.Text = IIf(Authorization.IsAllowed(AuthorizationProperty.HostedPaymentV2SignatureOptional), "No", "Yes")
        End If
    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">PAYMENT METHOD STORAGE</span>
			<span class="PageSubHeading">- MESSAGE FIELDS</span><br />
			<custom:AuthAlert ID="aa" PropertyToCheck="HostedPaymentV2" runat="server" />
		</td>
	</tr>	
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">PAGE URL<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<%=CurrentDomain.ProcessV2URL%>PaymentMethodStorage<br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Request Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
				<th width="46">Max<br />Length<br /></th>
				<th width="80" valign="bottom">Required <sup>1</sup><br /></th>
			</tr>
			<tr>
				<td>merchantID<br /></td>
				<td>Your 7 digits merchant number - <%=MerchantNumber%><br /></td>
				<td>7</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>signature<br /></td>
				<td>
					Signature for verifying the authenticity of the request parameters.<br />
					Field values to use: <i>merchantID + PersonalHashKey</i><br /><br />
					Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
				</td>
				<td>30</td>
				<td><asp:Literal ID="litSignatureRequired" Text="Yes" runat="server" /></td>
			</tr>
			<tr>
				<td>trans_comment<br /></td>
				<td>Optional text used mainly to describe the transaction</td>
				<td>500</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>disp_payFor<br /></td>
				<td>Text shown to buyer in payment window, Usually description of purchase (Cart description, Product name)</td>
				<td>40</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>disp_paymentType<br /></td>
				<td>
					List of payment types that are available to the client.<br />
					If more than one, use comma to seperate the values.<br />
					(example: CC,ED)<br />
					<br />
					Available values are:<br />
					CC = Credit Card<br />
					<%If (Request("DebugTest") = "1") Then %>
						EC = Electronic Check (available soon)<br />
						ID = Instant Debit (available soon)<br />
						WT = Customer Login (available soon)<br />
					<%End If%>
				</td>
				<td>12</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>disp_lng<br /></td>
				<td>
					Language Code for the UI text in the window.<br />
					If omitted, language is taken from user's browser settings.<br />
					<br />
					Available values are:<br />
					en-us = English USA <br />
					it-it = Italian Italy<br />
					he-il = Hebrew Israel<br />
				</td>
				<td>5</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_id<br /></td>
				<td>available soon</td>
				<td>10</td>
				<td>Optional</td>
			</tr>
			<!--<tr>
				<td>client_id<br /></td>
				<td>
					<asp:Label ID="lblIsCustomerPurchasePayerID" runat="server" />
					Used for second time buyers where client exist in sellers database and seller wishes to automatically fill the page with payer's credit card information.<br />
				</td>
				<td>10</td>
				<td>Optional</td>
			</tr>-->
			<tr>
				<td>url_notify<br /></td>
				<td>
					The URL for notifying your system with the transaction result, Must include http:// or https://.<br />
					<p>(this parameter can be configured in <a href="HostedPageV2_Admin.aspx">Hosted Page V.2</a> under the managment section, if your send this parameter with the request it will overrides the configured value).</p>
				</td>
				<td>255</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>url_redirect<br /></td>
				<td>
					The URL to which the buyer�s browser is redirected to after completing the payment, Must include http:// or https://.<br />
					<p>(this parameter can be configured in <a href="HostedPageV2_Admin.aspx">Hosted Page V.2</a> under the managment section, if your send this parameter with the request it will overrides the configured value).</p>
				</td>
				<td>255</td>
				<td>Optional</td>
			</tr>
			</table>
		</td>	
	</tr>
	<tr><td><br /></td></tr>
	<tr><td class="FieldSecHeading">Automatic filling of form data:<br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<td width="110">client_fullName<br /></td>
				<td>Cardholder full name</td>
				<td width="46">50</td>
				<td width="80">Optional</td>
			</tr>
			<tr>
				<td>client_email<br /></td>
				<td>Cardholder Email address</td>
				<td>50</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_phoneNum<br /></td>
				<td>Cardholder phone number</td>
				<td>15</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_idNum<br /></td>
				<td>Government issued ID number</td>
				<td>9</td>
				<td>Optional</td>
			</tr>
			<tr><td colspan="4"><br /></td></tr>
			<tr>
				<td>client_billAddress1<br /></td>
				<td>Client billing address line 1</td>
				<td>50</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_billAddress2<br /></td>
				<td>Client billing address line 2</td>
				<td>50</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_billCity<br /></td>
				<td>Client billing city</td>
				<td>20</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_billZipcode<br /></td>
				<td>Client billing zip code</td>
				<td>20</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_billState<br /></td>
				<td>Client billing state in 2 char ISO code</td>
				<td>2</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_billCountry<br /></td>
				<td>Client billing country in 2 char ISO code</td>
				<td>2</td>
				<td>Optional</td>
			</tr>
			</table>
		</td>	
	</tr>
	<tr><td><br /></td></tr>
	<tr><td><span class="FieldSecHeading">User defined parameters:<br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
				<tr>
					<td>
						You can append your own fields to the request.<br />
						These fields with be stored during the request processing and returned with the reply.<br />
						Use this to transfer more information to your url_redirect and url_notify pages.
					</td>
					<td width="46">2000 <sup>2</sup></td>
					<td width="80">Optional</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Response Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td>replyCode<br /></td>
				<td>Transaction authorization (see list below)<br /></td>
			</tr>
			<tr>
				<td>replyDesc<br /></td>
				<td>Reply description<br /></td>
			</tr>
			<tr>
				<td>storage_id<br /></td>
				<td>storage number<br /></td>
			</tr>
			<tr>
				<td>store_date<br /></td>
				<td>storage time and date<br /></td>
			</tr>
			<tr>
				<td>client_id<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>paymentMethod<br /></td>
				<td>
					Description of the payment method used in the transaction.<br />
					For credit cards, the description consists of the card type and the last four digits of the card number.<br />
					For bank transfers, the description consists of the platform name and the last four digits of the account.<br />
				</td>
			</tr>
			<tr>
				<td>signature<br /></td>
				<td>
					Signature for verifying the authenticity of the response parameters.<br />
					Field values used: <i>replyCode + storage_id + PersonalHashKey</i><br /><br />
					Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					replyCode=00&replyDesc=SUCCESS&storage_id=123456&store_date=01/01/2000 00:00:01&client_id=1234<br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Reply Codes<br /></td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="1">
			<tr>
				<td><span>00</span> = Store successeded<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><span>xx</span> = store failed, see replyDesc for explanation<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td>
			- For a more detail list of reply codes <a href="CcStorage_MsgFields.aspx">click here</a> 
			<a href="CcStorage_MsgFields.aspx"><img src="/NPCommon/Images/iconNewWinRight.GIF" border="0" align="middle" /></a><br />
		</td>
	</tr>
	<tr>
		<td>
			<br /><hr size="1" width="100%" noshade="noshade" />
			(1) Some field requirements are dependent on merchant configuration and can be change on your request<br />
			(2) The overall length of the request is subject to the 2000 byte length restriction<br />
		</td>
	</tr>
	</table>
</asp:Content>