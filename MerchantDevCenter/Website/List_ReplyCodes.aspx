<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - REPLY CODES" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<script runat="server">

		protected sub Page_Load
			lblCompany1.Text=CurrentDomain.BrandName
		End Sub
	
	</script>

	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">REPLY CODES</span><br />
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="1">
			<tr>
				<td>
					000 = The transaction has completed successfully<br />
					001 = Transaction is pending authorization <br />
				</td>
			</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr><td class="SecHeading"><asp:Label ID="lblCompany1" runat="server" /> Reject Codes<br /></td></tr>
	<tr><td><br /></td></tr>	
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="left">
				<tr>
					<th valign="bottom">Reply<br /></th>
					<th valign="bottom">Description<br /></th>
				</tr>
				<%
					Dim sSQL As String = "SELECT Code, DescriptionMerchantEng FROM tblDebitCompanyCode WHERE DebitCompanyID=1 AND Code<>'000' AND Code<>'001' ORDER BY Code"
					Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
					While iReader.Read()
						%>
							<tr>
								<td><%= iReader("Code") %><br /></td>
								<td><%= iReader("DescriptionMerchantEng") %><br /></td>
							</tr>
						<%
					End while
					iReader.Close()
				%>
			</table>
			<br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Bank Reject Codes<br /></td></tr>
	<tr><td><br /></td></tr>	
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="left">
			<tr>
				<th valign="bottom">Reply<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td>XXX (Any other code)<br /></td>
				<td>
					Transaction was not authorized<br />
					See explanation in the reply string returned or in transaction list under "rejected transactions"<br />	
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
		
</asp:Content>

