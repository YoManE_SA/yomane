<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - RECURRING / PENDING" %>
<script language="vbscript" type="text/vbscript" runat="server">
	Protected Sub Page_Load()
		rptCurrency.DataBind()
		lblLinkToMerchant.Text = "<a href=""" & CurrentDomain.MerchantUrl & """ target=""_blank"">" & lblLinkToMerchant.Text & "</a>"
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">WALLET</span>
			<span class="PageSubHeading">- INTEGRATION AND NOTIFICATION</span><br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td colspan="2" class="SecHeading">To open the wallet site:<br /><br /></td>
	</tr>
	<tr>
		<td>
			<asp:Label ID="lblProcessURL2" runat="server" />[wallet_site]/website/signup.aspx?CN=<span style="color:#003366;">[Company number]</span>&CR=<span style="color:#003366;">[Customer reference code]</span></nobr>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td colspan="2">
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th valign="bottom">Field<br /></th>
				<th>Max<br />Length<br /></th>
				<th valign="bottom" colspan="2">Detail<br /></th>
			</tr>
			<tr>
				<td width="113">CN<br /></td>
				<td width="50">7</td>
				<td width="120"><span style="color: #003366;">Company number:</span><br /></td>
				<td>
					Your company number<!-- as appears on your
					<asp:label ID="lblLinkToMerchant" Text="merchant panel" runat="server" />-->
				</td>
			</tr>
			<tr>
				<td>CR<br /></td>
				<td>50</td>
				<td><span style="color: #003366;">Customer reference code:</span><br /></td>
				<td>This customer's reference code at you system<br /></td>
			</tr>
			</table>
			<br />
			Additional fields can be chained to the query string.
			The overall length of the string containing the additional fields is limited to 100 characters
			(not including mandatory "CN" and "CR" fields).
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Notification from wallet to your site:<br /><br /></td>
	</tr>
	<tr>
		<td>
			First you will have to configure the page handler in you site under: Management --&gt; Notifications.
			<br /><br />
			Every time your client is trying to deposit in to the balance, you recieve a notification
			indicating the customer, the amount, the currency and the success status.
			<br /><br />
			The additional fields passed to the signup page (above) are chained to the post string.
			<br /><br />
			You can also receive a balance transfer verification string.<br />
			The string is hashed with MD5 algorythm and consists of the transfer ID, followed by the amount transfered,
			the transfer currency code and your Personal Hash Key.<br />
			In order to enable that feature you have to create your Personal Hash Key
			(visit Management --&gt; Security Settings).
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td colspan="2">
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th valign="bottom">Field<br /></th>
				<th>Max<br />Length<br /></th>
				<th valign="bottom" colspan="2">Detail<br /></th>
			</tr>
			<tr>
				<td width="113">CR<br /></td>
				<td width="50">50</td>
				<td width="120"><span style="color: #003366;">Customer reference code:</span><br /></td>
				<td>
					The customer's id in your system (the one you sent when you first open the wallet (see above))
				</td>
			</tr>
			<tr>
				<td width="113">Amount<br /></td>
				<td width="50">20</td>
				<td width="120"><span style="color: #003366;">Amount:</span><br /></td>
				<td>
					The amount the customer tried to deposit
				</td>
			</tr>
			<tr>
				<td width="113">Currency<br /></td>
				<td width="50">3</td>
				<td width="120"><span style="color: #003366;">Currency ISO Code:</span><br /></td>
				<td>
					<asp:Repeater ID="rptCurrency" DataSource="<%# dbPages.Currencies  %>" runat="server">
						<ItemTemplate>
							<asp:Literal runat="server" Text='<%#Eval("IsoCode")%>' /> = <asp:Literal runat="server" Text='<%#Eval("Name")%>' />
						</ItemTemplate>
						<SeparatorTemplate><br /></SeparatorTemplate>
					</asp:Repeater>
				</td>
			</tr>
			<tr>
				<td width="113">Status<br /></td>
				<td width="50">2</td>
				<td width="120"><span style="color: #003366;">Status code:</span><br /></td>
				<td>
					Success status for the operation, 00 = Success, all other codes means fail.
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<br />
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr>
				<td class="SecExpandLink">
					<u>Notification Example:</u><br /><br />
					[your_site_handler]?CR=123456&Amount=20.3&Currency=USD&Status=00
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	
</asp:Content>
