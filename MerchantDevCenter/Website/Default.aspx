<%@ Page Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/MasterPage.master" Title="DEVELOPER CENTER" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage" %>
<script runat="server">
    Sub Page_Load()
        Dim status = Netpay.Infrastructure.MerchantStatus.Integration
        If Merchant IsNot Nothing Then
            litWelcome.Text = Merchant.Name & ", merchant " & MerchantNumber
            status = Merchant.ActiveStatus
        Else
            litWelcome.Text = "Guest"
        End If
        litStatus.Text = status.ToString()
        If litStatus.Text = "Integration" Then litTest.Visible = True
    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0"  cellpadding="0" border="0" width="100%">
		<tr>
			<td>
				<span class="PageHeading">DEVELOPER CENTER</span><br />
			</td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr>
			<td>
				<span style="font-size:13px;">Welcome, <asp:Literal ID="litWelcome" runat="server" />.</span><br />
				Your current status is: <b><asp:Literal ID="litStatus" runat="server" /></b>.
				<asp:Literal ID="litTest" Visible="false" runat="server">
					<br /><br />Visit <a href="TestEnvironment.aspx">Basic Info -&gt; Test Environment</a> on the left menu
					to learn about the limitations and rules when in integration.
				</asp:Literal>
				
			</td>
		</tr>
		<tr><td><br /><hr style="border:1px dotted silver;" /><br /></td></tr>
		<tr>
			<td class="FieldSecHeading">CHANGE LOG</td>
		</tr>
		<tr>
			<td>

				<br /><p style="padding-bottom:4px;">22/07/2012</p>
				Added: New Service "Blacklist", allow to block values from processing under your account.<br />
				<br /><p style="padding-bottom:4px;">15/05/2012</p>
				Added: New page "Basic Info - Signature" showing instruction and code samples for our signature parameter.<br />
				<br /><p style="padding-bottom:4px;">07/03/2012</p>
				Added: New feature under "Hosted Page V.2 - Management", option to set display text in multiple languages.<br />
				<br /><p style="padding-bottom:4px;">28/02/2012</p>
				Added: New page "Basic Info - Account Info" showing merchant's limits and settings.<br />
				Modified: Field <i>"Signature"</i> under Request Fields in Hosted Page V.2 has been changed to strengthen security.<br />
				<br /><p style="padding-bottom:4px;">07/02/2012</p>
				Added: New <i>"disp_recurring"</i> field under Request Fields in Hosted Page V.2.<br />
				Modified: Recurring explanation under Request Fields in Hosted Page V.2 has been changed.
			</td>
		</tr>
	</table>
</asp:Content>