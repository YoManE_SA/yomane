<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" EnableEventValidation="false" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<script runat="server">
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		trHebrew.Visible = CurrentDomain.IsHebrewVisible
		iuLogo.TargetFileName = "/GlobalData/HostedPageLogos/HP_" + Session("MerchantNumber")
		If Not IsPostBack Then LoadData()
	End Sub

	Private Sub LoadData()
		Dim bImageExist As Boolean = False
		Dim sSQL As String = "SELECT companyName_Heb, companyName_Eng, companyName_Spa, companyName_Fre, logoPic FROM tblShoppingPurchaseAdmin WHERE CompanyID=" & WebUtils.LoggedUser.ID
		Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
		If iReader.Read() Then
			companyName_Heb.Text = dbPages.dbtextShow(iReader("companyName_Heb"))
			companyName_Eng.Text = dbPages.dbtextShow(iReader("companyName_Eng"))
			companyName_Spa.Text = dbPages.dbtextShow(iReader("companyName_Spa"))
			companyName_Fre.Text = dbPages.dbtextShow(iReader("companyName_Fre"))
			iuLogo.FileName = "/GlobalData/HostedPageLogos/" & Trim(iReader("logoPic"))
		End If
		iReader.Close()
	End Sub
	
	Private Sub iuLogo_OnDataUpdate(ByVal sender As Object, ByVal e As System.EventArgs)
		dbPages.ExecSql("UPDATE tblShoppingPurchaseAdmin SET logoPic='" & iuLogo.SaveFileName.ToSql(True) & "' WHERE CompanyID=" & WebUtils.LoggedUser.ID)
	End Sub

	Private Sub btnUpdate_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim sSQL As String = "UPDATE tblShoppingPurchaseAdmin SET companyName_Heb=Left(N'" & companyName_Heb.Text.ToSql(True) & "', 30), companyName_Eng=Left(N'" & companyName_Eng.Text.ToSql(True) & "', 30), companyName_Spa=Left(N'" & companyName_Spa.Text.ToSql(True) & "', 30), companyName_Fre=Left(N'" & companyName_Fre.Text.ToSql(True) & "', 30) WHERE CompanyID=" & WebUtils.LoggedUser.ID
		If dbPages.ExecSql(sSQL) = 0 Then
			sSQL = "INSERT INTO tblShoppingPurchaseAdmin(companyName_Heb, companyName_Eng, companyName_Spa, companyName_Fre, CompanyID)" & _
			" VALUES(Left(N'" & companyName_Heb.Text.ToSql(True) & "', 30), Left(N'" & companyName_Eng.Text.ToSql(True) & "', 30), Left(N'" & companyName_Spa.Text.ToSql(True) & "', 30), Left(N'" & companyName_Fre.Text.ToSql(True) & "', 30)," & WebUtils.LoggedUser.ID & ")"
			dbPages.ExecSql(sSQL)
		End If
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<script src="/include/formValidator2.js" type="text/javascript"></script>
	<script type="text/javascript">
	/*
		var vm = new validationManager("english");

		function registerValidation() {
			// register validation
			formInstance = document.frmDisplay;
			vm.add(new textValidation(formInstance.companyName_Heb, "Hebrew merchant descriptor", "string", 0, 30, true));
			vm.add(new textValidation(formInstance.companyName_Eng, "English merchant descriptor", "string", 0, 30, true));
		}
		
		function validate() {
			return vm.validate()
		}
	*/
	</script>
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">HOSTED PAYMENT PAGE</span>
			<span class="PageSubHeading">- MANAGE APPEARANCE</span><br />
			<custom:AuthAlert PropertyToCheck="HostedPayment" runat="server" />
		</td>
	</tr>
	<tr>
		<td colspan="2" class="SecHeading">
			<br /><br />Manage how hosted page will look<br/><br />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr>
				<td width="21%" valign="top" style="color:#003366;">Merchant Logo<br /></td>
				<td width="30%" valign="top">An Image file to exhibit in the payment window 90X50 Pixel.<br /></td>
				<td width="2%"></td>
				<td valign="top">
					<netpay:ImageUpload ID="iuLogo" runat="server" ImageWidth="90" ImageHeight="50" OnDataUpdate="iuLogo_OnDataUpdate" />
				</td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td valign="top" style="color:#003366;">Merchant Descriptor<br /></td>
				<td>To be shown in English window.<br /></td>
				<td></td>
				<td valign="top">
					<asp:TextBox runat="server" size="28" maxlength="35" id="companyName_Eng" class="inputdata" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td>To be shown in Spanish window.<br /></td>
				<td></td>
				<td valign="top">
					<asp:TextBox runat="server" size="28" maxlength="35" id="companyName_Spa" class="inputdata" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td>To be shown in French window.<br /></td>
				<td></td>
				<td valign="top">
					<asp:TextBox runat="server" size="28" maxlength="35" id="companyName_Fre" class="inputdata" />
				</td>
			</tr>
			<tr runat="server" id="trHebrew">
				<td></td>
				<td valign="top">To be shown in Hebrew window.<br /></td>
				<td></td>
				<td valign="top">
					<asp:TextBox runat="server" size="28" maxlength="35" id="companyName_Heb" class="inputdata" />
				</td>
			</tr>
			<tr>
				<td colspan="4"><span style="color:Maroon;">*</span> Required fields</td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td></td>
				<td>
					<br/>
					<asp:Button runat="server" Text=" Update " ID="btnUpdate" style="background-color:white;font-size:11px;width:98px;cursor:pointer;" OnClick="btnUpdate_OnClick" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
		
</asp:Content>

