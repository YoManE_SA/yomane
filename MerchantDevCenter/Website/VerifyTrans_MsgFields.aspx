<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - VERIFY TRANSACTIONS" %>

<script runat="server">
	Dim sBrandName as String = CurrentDomain.BrandName
	Dim sProcessURL as String = CurrentDomain.ProcessURL
	
	public sub Page_Load
		rptCurrency.DataBind()
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">VERIFY TRANSACTIONS</span>
			<span class="PageSubHeading">- MESSAGE FIELDS</span><br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">PAGE URL<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td><%=sProcessURL %>verify_trans.asp<br /></td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Request Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
				<th width="46">Max<br />Length<br /></th>
				<th width="80" valign="bottom">Required<br /></th>
			</tr>
			<tr>
				<td>CompanyNum<br /></td>
				<td>Your company number - <%=MerchantNumber%><br /></td>
				<td>7</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>TransID<br /></td>
				<td>ID number of transaction to be verified<br /></td>
				<td>7</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>TransDate<br /></td>
				<td>The transaction date in our system in format <span style="font-family:Monospace;">dd/MM/yyyy HH:mm:ss</span><br /></td>
				<td>17</td>
				<td>Yes</td>
			</tr>				
			<tr>
				<td>TransType<br /></td>
				<td>
					0 = Debit Transaction<br />
					1 = Authorization only<br />
					2 = Pending Transaction<br />
				</td>
				<td>1</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>TransAmount<br /></td>
				<td>Amount to be charged<br />(example: 199.95)<br /></td>
				<td>8</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>TransCurrency<br /></td>
				<td>
					<asp:Repeater ID="rptCurrency" DataSource="<%# dbPages.Currencies  %>" runat="server">
						<ItemTemplate>
							<asp:Literal runat="server" Text='<%#Eval("ID")%>' /> = <asp:Literal runat="server" Text='<%#Eval("IsoCode")%>' />
							(<asp:Literal runat="server" Text='<%#Eval("Name")%>' />)
						</ItemTemplate>
						<SeparatorTemplate><br /></SeparatorTemplate>
					</asp:Repeater>
				</td>
				<td>1</td>
				<td>Yes</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Reply Codes<br /></td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="1">
			<tr><td>00 = Transaction exists<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>10 = Not enough data or invalid data<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>11 = Merchant does not exist<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>12 = Transaction number does not exist or is not associated with merchant<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>13 = Transaction date does not match<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>14 = Transaction amount does not match<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>15 = Transaction currency type does not match<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>16 = Pending transaction approved<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>17 = Pending transaction declined<br /></td></tr>
			</table>
		</td>
	</tr>
	</table>
</asp:Content>