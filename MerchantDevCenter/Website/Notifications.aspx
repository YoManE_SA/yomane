<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  EnableEventValidation="false" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    Protected Sub SaveForm(ByVal o As Object, ByVal e As EventArgs)
        Dim sIsRecurringReply As Integer = IIf(chkIsRecurringReply.Checked, 1, 0)
        Dim sRecurringReplyURL As String = txtRecurringReplyUrl.Text.ToSql(True)
        Dim sIsRecurringModifyReply As Integer = IIf(chkIsRecurringModifyReply.Checked, 1, 0)
        Dim sRecurringModifyReplyURL As String = txtRecurringModifyReplyUrl.Text.ToSql(True)
        Dim sIsPendingReply As Integer = IIf(chkIsPendingReply.Checked, 1, 0)
        Dim sPendingReplyURL As String = txtPendingReplyUrl.Text.ToSql(True)
        Dim sIsWalletReply As Integer = IIf(chkIsWalletReply.Checked, 1, 0)
        Dim sWalletReplyURL As String = txtWalletReplyUrl.Text.ToSql(True)
        Dim sUseHasKeyInWalletReply As Integer = IIf(chkUseHasKeyInWalletReply.Checked, 1, 0)
        Dim sIsNotifyRefundRequestApproved As Integer = IIf(chkIsNotifyRefundRequestApproved.Checked, 1, 0)
        Dim sNotifyRefundRequestApprovedUrl As String = txtNotifyRefundRequestApprovedUrl.Text.ToSql(True)
        Dim sbSQL As New StringBuilder("IF EXISTS")
        sbSQL.Append(" (SELECT company_id FROM tblCompanyChargeAdmin WHERE company_id=" & Merchant.ID & ")")
        sbSQL.Append(" UPDATE tblCompanyChargeAdmin SET")
        sbSQL.Append(" IsRecurringReply=" & sIsRecurringReply & ",")
        sbSQL.Append(" RecurringReplyURL='" & sRecurringReplyURL & "',")
        sbSQL.Append(" IsRecurringModifyReply=" & sIsRecurringModifyReply & ",")
        sbSQL.Append(" RecurringModifyReplyURL='" & sRecurringModifyReplyURL & "',")
        sbSQL.Append(" IsPendingReply=" & sIsPendingReply & ",")
        sbSQL.Append(" PendingReplyURL='" & sPendingReplyURL & "',")
        sbSQL.Append(" IsWalletReply=" & sIsWalletReply & ",")
        sbSQL.Append(" WalletReplyURL='" & sWalletReplyURL & "',")
        sbSQL.Append(" UseHasKeyInWalletReply=" & sUseHasKeyInWalletReply & ",")
        sbSQL.Append(" isNotifyRefundRequestApproved=" & sIsNotifyRefundRequestApproved & ",")
        sbSQL.Append(" NotifyRefundRequestApprovedUrl='" & sNotifyRefundRequestApprovedUrl & "' ")
        sbSQL.Append(" WHERE company_id=" & Merchant.ID)
        sbSQL.Append(" ELSE INSERT INTO tblCompanyChargeAdmin (IsRecurringReply, RecurringReplyURL, IsRecurringModifyReply, RecurringModifyReplyURL,")
        sbSQL.Append(" IsPendingReply, PendingReplyURL, IsWalletReply, WalletReplyURL, UseHasKeyInWalletReply, isNotifyRefundRequestApproved, NotifyRefundRequestApprovedUrl, company_id) VALUES (")
        sbSQL.Append(" " & sIsRecurringReply & ", '" & sRecurringReplyURL & "', ")
        sbSQL.Append(" " & sIsRecurringModifyReply & ", '" & sRecurringModifyReplyURL & "', ")
        sbSQL.Append(" " & sIsPendingReply & ", '" & sPendingReplyURL & "', ")
        sbSQL.Append(" " & sIsWalletReply & ", '" & sWalletReplyURL & "', ")
        sbSQL.Append(" " & sUseHasKeyInWalletReply & " , ")
        sbSQL.Append(" " & sIsNotifyRefundRequestApproved & ", '" & sNotifyRefundRequestApprovedUrl & "', ")
        sbSQL.Append(" " & Merchant.ID & ")")
        dbPages.ExecSql(sbSQL.ToString)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Page.IsPostBack Then
            Page.Title = "DEVELOPER CENTER - NOTIFICATIONS"
            Dim sSQL As String = "SELECT * FROM tblCompanyChargeAdmin WHERE company_id=" & Merchant.ID
            Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
            If drData.Read() Then
                chkIsRecurringReply.Checked = drData("IsRecurringReply")
                txtRecurringReplyUrl.Text = drData("RecurringReplyURL")
                chkIsRecurringModifyReply.Checked = drData("IsRecurringModifyReply")
                txtRecurringModifyReplyUrl.Text = drData("RecurringModifyReplyURL")
                chkIsPendingReply.Checked = drData("IsPendingReply")
                txtPendingReplyUrl.Text = drData("PendingReplyURL")
                chkIsWalletReply.Checked = drData("IsWalletReply")
                txtWalletReplyUrl.Text = drData("WalletReplyURL")
                chkUseHasKeyInWalletReply.Checked = drData("UseHasKeyInWalletReply")
                txtNotifyRefundRequestApprovedUrl.Text = IIf(IsDBNull(drData("NotifyRefundRequestApprovedUrl")), "", drData("NotifyRefundRequestApprovedUrl"))
                chkIsNotifyRefundRequestApproved.Checked = drData("IsNotifyRefundRequestApproved")
            End If
            drData.Close()
            dsRecurStatus.ConnectionString = dbPages.DSN
        End If
        Dim sHashKey As String = dbPages.ExecScalar("SELECT HashKey FROM tblCompany WHERE ID=" & Merchant.ID)
        If String.IsNullOrEmpty(sHashKey) Then
            chkUseHasKeyInWalletReply.Checked = False
            chkUseHasKeyInWalletReply.Enabled = False
            lblHash.Text = "<span style=""color:Maroon;"">Hashing feature is currently disabled,"
            lblHash.Text &= " because you have no Personal Hash Key.<br />"
            lblHash.Text &= "Click ""Security Settings"" on the left to generate one.</span>"
        Else
            lblHash.Text = "Your Personal Hash Key can be obtained in ""Security Settings"" page."
        End If
    End Sub
    
    Protected ReadOnly Property PendingTestSendData As String
        Get
            Return "Reply=000&transID=12345&Date=01/01/2000&TypeCredit=0" & vbCrLf & "&Payments=1&Amount=10.00&Currency=1&Order=54321&reference=12345"
        End Get
    End Property
    
    Protected Sub btnTestPendingNotification(ByVal sender As Object, ByVal e As System.EventArgs)
		ltPendingTextResult.Visible = True
        If txtPendingReplyUrl.Text.Trim() = "" Then
            ltPendingTextResult.Text = "<br/><span style=""color:maroon"">Please set url</span><br />"
            Return
        End If
        Try
            Dim sendData = PendingTestSendData.Replace(vbCrLf, "")
            Dim nRes As Integer = dbPages.SendRequestHTTP("POST", txtPendingReplyUrl.Text, sendData)
            ltPendingTextResult.Text = "<br/><span style=""color:green"">String was sent succesfully, return code: " & nRes & "</span><br />"
        Catch ex As Exception
            ltPendingTextResult.Text = "<br/><span style=""color:red"">String cannot be send due to the following error:<div>" & ex.Message & "</div></span>"
        End Try
    End Sub
    
    Protected ReadOnly Property RefundReqTestSendData As String
        Get
            Return "RequestId=432&OriginalTransId=123212&RefundTransId=14323" & vbCrLf & "&Amount=100.00&Currency=1&RefundStatus=ProcessedAccepted&Date=01/01/2012"
        End Get
    End Property
    
    Protected Sub btnTestRefundRequestNotification(ByVal sender As Object, ByVal e As System.EventArgs)
		ltRefundRequestTextResult.Visible = True
        If txtPendingReplyUrl.Text.Trim() = "" Then
            ltPendingTextResult.Text = "<br/><span style=""color:maroon"">Please set url</span><br />"
            Return
        End If
        Try
            Dim sendData = RefundReqTestSendData.Replace(vbCrLf, "")
            Dim nRes As Integer = dbPages.SendRequestHTTP("POST", txtNotifyRefundRequestApprovedUrl.Text, sendData)
            ltRefundRequestTextResult.Text = "<br/><span style=""color:green"">String was sent succesfully, return code: " & nRes & "</span><br />"
        Catch ex As Exception
            ltRefundRequestTextResult.Text = "<br/><span style=""color:red"">String cannot be send due to the following error:<div>" & ex.Message & "</div></span>"
        End Try
    End Sub
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<span class="PageHeading">NOTIFICATIONS</span><br /><br />
			Configuring Url address for receiveing automatic hidden reply<br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Recurring<br /></td>
	</tr>
	<tr>
		<td>
			<custom:AuthAlert PropertyToCheck="AllowRecurring" runat="server" />
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr>
				<td colspan="2">
					<asp:CheckBox runat="server" ID="chkIsRecurringReply" CssClass="option" />
					Send an hidden reply with the result of a charge attempt (POST)<br />
				</td>
			</tr>
			<tr>
				<td width="22"></td>
				<td>
					Enter a valid Url adrress including http://<br />
					<asp:TextBox runat="server" ID="txtRecurringReplyUrl" CssClass="long" maxlength="100" /><br />
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="SecExpandLink">
					<img onclick="showSection('0');" style="cursor:pointer;" id="oListToggle0" src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle">
					<a href="javascript:showSection('0');">Show response explanation</a><br/>
				</td>
			</tr>
			<tr id="trSec0" style="display:none;">
				<td></td>
				<td>
					The response sent back is in the same format and with the same fields as in <a href="SilentPost_Cc.aspx">silent post --> credit card</a>.<br />
					The Only exceptions are the following additional fields, They are attached to the end of the response string.<br /><br />
					<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
					</tr>
					<tr>
						<td>recur_initialID<br /></td>
						<td>Identification number of the initial transaction<br /></td>
					</tr>
					<tr>
						<td>recur_seriesID<br /></td>
						<td>Identification number of the series<br /></td>
					</tr>
					<tr>
						<td>recur_chargeCount<br /></td>
						<td>Number of charges within the series<br /></td>
					</tr>
					<tr>
						<td>recur_chargeNum<br /></td>
						<td>The current charge number within the charge count<br /></td>
					</tr>
					<tr>
						<td>recur_status<br /></td>
						<td>
							<asp:SqlDataSource ID="dsRecurStatus" runat="server" SelectCommand="SELECT * FROM tblGlobalData WHERE GD_Group=62 AND GD_Lng=1 ORDER BY GD_ID" />
							<asp:Repeater DataSourceID="dsRecurStatus" runat="server">
								<ItemTemplate><asp:Literal Text='<%# Eval("GD_ID") %>' runat="server" /> - <asp:Literal Text='<%# Eval("GD_Text") %>' runat="server" /></ItemTemplate>
								<SeparatorTemplate>, </SeparatorTemplate>
							</asp:Repeater>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<asp:CheckBox runat="server" ID="chkIsRecurringModifyReply" CssClass="option" />
					Send an hidden reply with the result of a recurring series modification attempt (POST)<br />
				</td>
			</tr>
			<tr>
				<td width="22"></td>
				<td>
					Enter a valid Url adrress including http://<br />
					<asp:TextBox runat="server" ID="txtRecurringModifyReplyUrl" CssClass="long" maxlength="100" /><br />
				</td>
			</tr>
			<tr><td><br /></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="SecHeading"><br />Pending transactions<br /></td>
	</tr>
	<tr>
		<td>
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr>
				<td valign="top" colspan="2">
					<asp:CheckBox runat="server" ID="chkIsPendingReply" CssClass="option" />
					Allow receiving hidden reply when charge status changed (POST)<br />
                </td>
            </tr><tr>
                <td width="20" style="padding-left:25px;">
					URL:
                </td><td>
					<asp:TextBox runat="server" ID="txtPendingReplyUrl" CssClass="long" maxlength="100" />
                </td>
            </tr><tr>
				<td></td>
				<td class="SecExpandLink">
					<img onclick="showSection('1');" style="cursor:pointer;" id="oListToggle1" src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle">
					<a href="javascript:showSection('1');">Show response explanation and testing</a><br/>
				</td>
			</tr>
			<tr id="trSec1" style="display:none;">
                <td></td>
                <td>
					<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" width="500">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
					</tr>
					<tr>
						<td>Reply<br /></td>
						<td>transaction's new reply code<br /></td>
					</tr>
					<tr>
						<td>transID<br /></td>
						<td>new transaction id<br /></td>
					</tr>
					<tr>
						<td>Date<br /></td>
						<td>new transaction date<br /></td>
					</tr>
					<tr>
						<td>TypeCredit<br /></td>
						<td>typeCredit of the original transaction<br /></td>
					</tr>
					<tr>
						<td>Payments<br /></td>
						<td>installments of the original transaction<br /></td>
					</tr>
					<tr>
						<td>Amount<br /></td>
						<td>amount of the new transaction<br /></td>
					</tr>
					<tr>
						<td>Currency<br /></td>
						<td>currency of the new transaction<br /></td>
					</tr>
					<tr>
						<td>Order<br /></td>
						<td>order that you passed with the original transaction<br /></td>
					</tr>
					<tr>
						<td>reference<br /></td>
						<td>original transaction id</td>
					</tr>
					</table>
                    <br />
					<table border="0" cellspacing="0" cellpadding="4" width="500" style="border:1px solid #c5c5c5;">
					<tr>
						<td>
							<asp:Button runat="server" Text="SEND" ID="btnSendTest" style="font-size:11px; float:right;" onclick="btnTestPendingNotification" />
							<span style="text-decoration:underline;">Test your page with this reply</span><br />
							<%=PendingTestSendData.Replace(vbCrLf, "<br/>") %><br />
							<asp:Literal Visible="false" runat="server" ID="ltPendingTextResult" Mode="PassThrough" />
						</td>
					</tr>
					</table>
                </td>
			</tr>
			<tr><td><br /></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="SecHeading"><br />Refund Request<br /></td>
	</tr>
	<tr>
		<td>
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr>
				<td valign="top" colspan="2">
					<asp:CheckBox runat="server" ID="chkIsNotifyRefundRequestApproved" CssClass="option" />
					Allow receiving hidden reply when processing successfully a refund request (POST)<br />
                </td>
            </tr><tr>
                <td width="20" style="padding-left:25px;">
					URL:
                </td><td>
					<asp:TextBox runat="server" ID="txtNotifyRefundRequestApprovedUrl" CssClass="long" maxlength="100" />
                </td>
            </tr><tr>
				<td></td>
				<td class="SecExpandLink">
					<img onclick="showSection('2');" style="cursor:pointer;" id="oListToggle2" src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle">
					<a href="javascript:showSection('2');">Show response explanation and testing</a><br/>
				</td>
			</tr>
			<tr id="trSec2" style="display:none;">
                <td></td>
                <td>
					<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" width="500">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
					</tr>
					<tr>
						<td>RequestId<br /></td>
						<td>Identification number of the refund request<br /></td>
					</tr>
					<tr>
						<td>OriginalTransId<br /></td>
						<td>Identification number of the original transaction<br /></td>
					</tr>
					<tr>
						<td>RefundTransId<br /></td>
						<td>Identification number of the new refund transaction<br /></td>
					</tr>
					<tr>
						<td>Amount<br /></td>
						<td>the amount of the refund<br /></td>
					</tr>
					<tr>
						<td>Currency<br /></td>
						<td>the currency of the refund<br /></td>
					</tr>
					<tr>
						<td>Status<br /></td>
						<td>
                            can be: CreatedAccepted, ProcessedAccepted - both means the refund completed
						</td>
					</tr>
					</table>
                    <br />
					<table border="0" cellspacing="0" cellpadding="4" width="500" style="border:1px solid #c5c5c5;">
					<tr>
						<td>
							<asp:Button runat="server" Text="SEND" style="font-size:11px; float:right;" onclick="btnTestRefundRequestNotification" />
							<span style="text-decoration:underline;">Test your page with this reply</span><br />
							<%=RefundReqTestSendData.Replace(vbCrLf, "<br/>") %> <br />
							<asp:Literal Visible="false" runat="server" ID="ltRefundRequestTextResult" Mode="PassThrough" />
						</td>
					</tr>
					</table>
                </td>
			</tr>
			<tr><td><br /></td></tr>
			</table>
		</td>
	</tr>
    <tr>
		<td class="SecHeading"><br />Wallet balance transfers<br /></td>
	</tr>
	<tr>
		<td>
			<custom:AuthAlert PropertyToCheck="ElectronicWallet" runat="server" />
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr>
				<td valign="top">
					<asp:CheckBox runat="server" ID="chkIsWalletReply" CssClass="option" />
					Allow receiving hidden reply automatically when wallet balance is transferred (POST)<br />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					URL: &nbsp;
					<asp:TextBox runat="server" ID="txtWalletReplyUrl" CssClass="long" maxlength="100" /><br />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:CheckBox runat="server" ID="chkUseHasKeyInWalletReply" CssClass="option" />
					Append hashed (MD5) verification string<br />
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td><asp:Label runat="server" ID="lblHash" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td></td>
				<td>
					<br/>
					<asp:Button runat="server" UseSubmitBehavior="false" OnClick="SaveForm" Text="Update" CssClass="submit" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
</asp:Content>
