<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<script runat="server">
	Dim sSQL, sExtraTxt, sLinkTxt As String
	Dim langTxt As String = ""
	Dim sangCode As Integer = 1

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Select Case Trim(Request("lang"))
			Case "eng" : langTxt = "English" : sangCode = 1
			Case "spa" : langTxt = "Spanish" : sangCode = 2
			Case "heb" : langTxt = "Hebrew" : sangCode = 0
		End Select
		If Not IsPostBack Then LoadData()
	End Sub
	
	Private Sub btnUpdate_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
		sSQL = "UPDATE tblPublicPayAdmin SET " & _
		 "isAllowPublicPayUse=" & IIf(isAllowPublicPayUse.Checked, 1, 0) & "," & _
		 "isSEndAnswer=" & IIf(isSendAnswer.Checked, 1, 0) & ", " & _
		 "SEndAnswerURL='" & SendAnswerURL.Text.ToSql(True) & "' " & _
		 "WHERE CompanyID=" & WebUtils.LoggedUser.ID & " AND lang=" & sangCode
        If dbPages.ExecSql(sSQL) = 0 Then
            sSQL = "INSERT INTO tblPublicPayAdmin(isAllowPublicPayUse,isSEndAnswer,SEndAnswerURL,CompanyID,lang)" & _
             " VALUES(" & IIf(isAllowPublicPayUse.Checked, 1, 0) & ", " & IIf(isSendAnswer.Checked, 1, 0) & ", '" & SendAnswerURL.Text.ToSql(True) & "'," & WebUtils.LoggedUser.ID & "," & sangCode & ")"
            dbPages.ExecSql(sSQL)
        End If
	End Sub
	
	Private Sub LoadData()
		Dim iReader As SqlDataReader = dbPages.ExecReader("SELECT * FROM tblPublicPayAdmin WHERE CompanyID=" & WebUtils.LoggedUser.ID & " AND lang=" & sangCode)
		If iReader.Read() Then
			isSendAnswer.Checked = iReader("isSendAnswer")
			SendAnswerURL.Text = iReader("SendAnswerURL")
			IsAllowPublicPayUse.Checked = iReader("isAllowPublicPayUse")
		End If
		iReader.Close()
		
		Dim ArrayLang As String() = {"English", "Spanish"}
		If CurrentDomain.IsHebrewVisible Then
			ReDim Preserve ArrayLang(ArrayLang.Length)
			ArrayLang(UBound(ArrayLang)) = "Hebrew"
		End If
		For i As Integer = 0 To ArrayLang.Length - 1
			sExtraTxt = "Off"
			sLinkTxt = ""
			If Trim(Left(ArrayLang(i), 3).ToLower) <> Trim(Request("lang").ToLower) Then
				sExtraTxt = "On"
				sLinkTxt = " onclick=""location.href='?lang=" & Trim(Left(ArrayLang(i), 3).ToLower) & "';"""
			End If
			lstLanguages.Text &= "<span title=""" & ArrayLang(i) & """ class=""pageMenu" & sExtraTxt & """" & sLinkTxt & ">" & Left(ArrayLang(i), 3).ToUpper & "</span>"
		Next
	End Sub

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">PUBLIC PAYMENT PAGE</span>
			<span class="PageSubHeading">- MANAGE CODE (<%=langTxt%>)</span><br />
		</td>
		<td align="right">
			<asp:Literal runat="server" ID="lstLanguages" Mode="PassThrough" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<br/><br/>
			This page enables you to configure the public payment page.<br/><br/>
		</td>
	</tr>
	<tr><td height="8"></td></tr>
	<tr>
		<td colspan="2">
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr>
				<td bgcolor="#f5f5f5" valign="top">
					<asp:CheckBox id="isAllowPublicPayUse" runat="server" />
					Enable <span style="text-transform:capitalize;"><%= Lcase(langTxt) %></span> public payment page
					(check to allow customers to access the page)<br />
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td class="SecHeading">Notification<br /></td>
			</tr>
			<tr>
				<td valign="top">Would you like to receive the client & transaction details, after each complete transaction? <br /></td>
			</tr>
			<tr>
				<td valign="top">
					<asp:CheckBox runat="server" id="isSendAnswer" /> Yes, to the following internet address:<br/>
					<asp:TextBox runat="server" id="SendAnswerURL" size="60" MaxLength="500" />
					(valid URL only)<br/>
				</td>
			</tr>
			<tr>
				<td>
					<br/>
					<asp:Button runat="server" ID="btnUpdate" UseSubmitBehavior="true" style="background-color:white;font-size:11px;width:98px;cursor:pointer;" Text=" Update " OnClick="btnUpdate_OnClick" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
</asp:Content>