<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
    function GetDirectLinkHTML(sLanguage as String, sPrefix as String) as String
        Dim sURL As String : sURL = WebUtils.CurrentDomain.ProcessV2Url & "public/?merchantID=" & MerchantNumber
        Return "<li><b>" & sLanguage & "</b>:<br><a target=""_blank"" href=""" & sURL & """>" & sURL & "</a><br /><br /></li>"
    End Function

    function GetDirectLinkHTML(sLanguage as String) as String
        return GetDirectLinkHTML(sLanguage, sLanguage.Substring(0, 3).ToLower())
    End Function

    Function GetDirectLinkHTMLGuest(ByVal sLanguage As String, ByVal sPrefix As String) As String
        Dim sURL As String : sURL = WebUtils.CurrentDomain.ProcessV2Url & "public/?merchantID=1415119"
        Return "<li><b>" & sLanguage & "</b>:<br><a target=""_blank"" href=""" & sURL & """>" & sURL & "</a><br /><br /></li>"
    End Function

    Function GetDirectLinkHTMLGuest(ByVal sLanguage As String) As String
        Return GetDirectLinkHTMLGuest(sLanguage, sLanguage.Substring(0, 3).ToLower())
    End Function

    sub Page_Load
        If Not Session.Item("Guest") Then
            lblDirectLink.Text = GetDirectLinkHTML("English")
        Else
            lblDirectLink.Text = GetDirectLinkHTMLGuest("English")
        End If

    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">CREDIT CARD</span>
			<span class="PageSubHeading">- PUBLIC PAYMENT PAGE</span><br />
			<custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" />
		</td>
	</tr>
	<tr>
		<td valign="top">
			<br />
			<table cellspacing="0" cellpadding="1" border="0" width="100%">
			<tr>
				<td valign="top">
				
					<img src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder%>/images/screenshot_publicpage.jpg" style="float:right; margin:0 0 20px 20px;" alt="" border="0">
					<span class="SecHeading">What is Public Payment Page</span><br />
					Public payment page is a web page which contains your own personal payment form.<br />
					Using this web page, clients surfing the net, can make payments to your account.<br />
					Public payment page is accessible to any one who knows its address.<br />
					<br />
					You can manage Public Payment Page, to add text in various places, pre define fixed fields, or let the client have more control.<br />
					For example, payment amount and number of payments fields, can be pre defined by you, or you can let the client decide for himself, how much and with how many payments he wants to pay.<br />
					<br /><br />
					<span class="SecHeading">When to use</span><br />
					Public payment page can be useful for the merchant that has no payment web page and does not want to build one.<br />
					<br /><br />
					<span class="SecHeading">How to use</span><br />
					To use Public payment page, follow these 3 simple steps:<br />
					<br />
					<ol>
						<li>
							Make sure Public Pay is Active, by checking the indicator on the top right side of this page.
							If Inactive is indicated, please contact <%= WebUtils.CurrentDomain.BrandName %> support.
						</li>
						<li>
							Use MANAGEMENT > PUBLIC PAYMENT PAGE >
							<a href="publicPageV2_AdminAppear.aspx"<%if Session.Item("Guest")%> onclick="return false"<%End If%>>Manage Appearance</a>
							to allow access to the page, define it's appearance and to choose the payment options
							to include in the form.
						</li>
						<li>
							Use the following address to refer directly to the payment page from your web site using a hyper link:<br />
							<br />
							<ul style="list-style-type:square;">
								<asp:Label ID="lblDirectLink" runat="server" />
							</ul>
						</li>
					</ol>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
		
</asp:Content>

