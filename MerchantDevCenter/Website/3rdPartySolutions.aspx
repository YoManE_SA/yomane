<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - SILENT POST - Credit Card" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Dim sBgColor as String, strStatus As String = ""
	
	Dim IsRemoteChargePersonalNumber,IsRemoteChargeCVV2,IsRemoteChargePhoneNumber, _
	IsRemoteChargeEmail,IsApprovalOnly,IsBillingAddressMust,IsAllowRecurring,IsUseFraudDetectionMaxMind As Boolean
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

	End Sub
	
	protected sub lbtnMerchant_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Response.Redirect(CurrentDomain.MerchantUrl,True)
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td><span class="PageHeading">3RD PARTY SOLUTIONS</span></td>
	</tr>
	<tr>
		<td>
			<br />
				Our payment solution is available for use within many of the top leading "off the shelf" shopping cart solutions<br />
			<br />
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td><br />
			<a href="http://addons.oscommerce.com/" target="_blank" title="Netpay at osCommerce"><img class="partnerLogo" src="/NPCommon/images/logo_osCommerce.gif" alt="Netpay at osCommerce" title="Netpay at osCommerce" /></a>
			Search for the word "Netpay" in the osCommerce Community Add-Ons:
			<a href="http://addons.oscommerce.com/" target="_blank" title="Netpay at osCommerce">http://addons.oscommerce.com/</a>
		</td>
	</tr>
	<tr>
		<td><br />
			<a href="http://marketplace.x-cart.com/" target="_blank" title="Netpay at X-Cart"><img class="partnerLogo" src="/NPCommon/images/logo_xcart.gif" alt="Netpay at X-Cart" title="Netpay at X-Cart" height="27" /></a>
			Search for the word "Netpay" in the X-Cart Marketplace:
			<a href="http://marketplace.x-cart.com/" target="_blank" title="Netpay at X-Cart">http://marketplace.x-cart.com/</a>
		</td>
	</tr>
	<tr>
		<td><br />
			<a href="http://www.magentocommerce.com/magento-connect/netpay-intl-your-complete-online-payment-solution-6410.html" target="_blank" title="Netpay at Magento"><img class="partnerLogo" src="/NPCommon/images/logo_magento.gif" alt="Netpay at Magento" title="Netpay at Magento" height="40" /></a>
			Link to Magento-connect for our plugin:
			<a href="http://www.magentocommerce.com/magento-connect/netpay-intl-your-complete-online-payment-solution-6410.html" target="_blank" title="Netpay at Magento-connect">Netpay at Magento-connect</a>
		</td>
	</tr>


	
	</table>
</asp:Content>