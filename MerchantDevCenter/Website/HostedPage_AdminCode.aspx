<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<script runat="server">
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		If Not IsPostBack Then LoadData()
	End Sub
		
	Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
		Dim nReplyAtEndTarget As Integer = 0
		Dim rb() As RadioButton = {ReplyAtEndTarget1, ReplyAtEndTarget2, ReplyAtEndTarget3, ReplyAtEndTarget4}
		For i As Integer = 0 To rb.Length - 1
			If rb(i).Checked Then nReplyAtEndTarget = i + 1
		Next
		Dim sSQL As String = "UPDATE tblShoppingPurchaseAdmin SET companyName_Heb='" & Request("companyName_Heb").ToSql(True) & "', companyName_Eng='" & Request("companyName_Eng").ToSql(True) & "', " & _
		"isReplyAtEnd=" & isReplyAtEnd.Checked.ToSql() & ", isReplySilent=" & isReplySilent.Checked.ToSql() & ", ReplyAtEndURL='" & ReplyAtEndURL.Text.ToSql(True) & "', " & _
		"ReplySilentURL='" & ReplySilentURL.Text.ToSql(True) & "', ReplyAtEndTarget=" & nReplyAtEndTarget & " WHERE CompanyID=" & WebUtils.LoggedUser.ID
		If dbPages.ExecSql(sSQL) = 0 Then
			sSQL = "INSERT INTO tblShoppingPurchaseAdmin(companyName_Heb, companyName_Eng, isReplyAtEnd, isReplySilent, ReplyAtEndURL, ReplyAtEndTarget, ReplySilentURL, CompanyID)" & _
			" VALUES('" & Request("companyName_Heb").ToSql(True) & "','" & Request("companyName_Eng").ToSql(True) & "'," & isReplyAtEnd.Checked.ToSql() & _
			", " & isReplySilent.Checked.ToSql() & ",'" & ReplyAtEndURL.Text.ToSql(True) & "'," & nReplyAtEndTarget & ",'" & ReplySilentURL.Text.ToSql(True) & "'," & WebUtils.LoggedUser.ID & ")"
			dbPages.ExecSql(sSQL)
		End If
	End Sub

	Private Sub LoadData()
		Dim rb() As RadioButton = {ReplyAtEndTarget1, ReplyAtEndTarget2, ReplyAtEndTarget3, ReplyAtEndTarget4}
		Dim iReader As SqlDataReader = dbPages.ExecReader("SELECT * FROM tblShoppingPurchaseAdmin WHERE CompanyID=" & WebUtils.LoggedUser.ID)
		If iReader.Read() Then
			isReplyAtEnd.Checked = iReader("isReplyAtEnd")
			isReplySilent.Checked = iReader("isReplySilent")
			ReplyAtEndURL.Text = dbPages.dbtextShow(iReader("ReplyAtEndURL"))
			Dim nRadioButtonChecked As Integer = dbPages.TestVar(iReader("ReplyAtEndTarget"), 1, 4, 0)
			If nRadioButtonChecked > 0 Then rb(nRadioButtonChecked - 1).Checked = True
			'ReplyAtEndTarget.Checked = iReader("ReplyAtEndTarget")
			ReplySilentURL.Text = dbPages.dbtextShow(iReader("ReplySilentURL"))
		End If
		iReader.Close()
	End Sub

	Protected Sub btnSendTest_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		If ReplySilentURL.Text.Trim() = "" Then
			ltTextResult.Text = "<span style=""color:maroon"">Please set url for hidden reply</span><br />"
			Return
		End If
		Try
			Dim nRes As Integer = dbPages.SendRequestHTTP("POST", ReplySilentURL.Text, "Reply=000&transID=1000&Date=01/01/1900+00:00:01")
			ltTextResult.Text = "<span style=""color:green"">String was sent succesfully, return code: " & nRes & "</span><br />"
		Catch ex As Exception
			ltTextResult.Text = "<span style=""color:red"">String cannot be send due to the following error:<div>" & ex.Message & "</div></span>"
		End Try
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">HOSTED PAYMENT PAGE</span>
			<span class="PageSubHeading">- MANAGE CODE</span><br />
			<custom:AuthAlert PropertyToCheck="HostedPayment" runat="server" />
		</td>
	</tr>
	<tr>
		<td colspan="2" class="SecHeading"><br /><br />How to get the answer<br /></td>
	</tr>
	<tr><td height="8"></td></tr>
	<tr>
		<td colspan="2">
			<span style="color:Maroon;font-weight:bold;">Important:</span>
			in order to enable the following configuration, remove "ReplyURL"
			and "ReplyType" fields from the string you send during the transaction attempt.<br />
			<br /><br />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr>
				<td width="21%" valign="top">Redirecting and/or<br />receiving reply </td>
				<td valign="top">
					<asp:CheckBox runat="server" id="isReplyAtEnd" /> Allow redirecting user at the End of the charge proccess (GET)<br />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:RadioButton runat="server" ID="ReplyAtEndTarget1" GroupName="ReplyAtEndTarget" /> In the opener page - Redirect + receive reply<br />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:RadioButton runat="server" ID="ReplyAtEndTarget2" GroupName="ReplyAtEndTarget" /> In the opener page - Redirect only<br />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:RadioButton runat="server" ID="ReplyAtEndTarget3" GroupName="ReplyAtEndTarget" /> In the charge page itself - Redirect + receive reply<br />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:RadioButton runat="server" ID="ReplyAtEndTarget4" GroupName="ReplyAtEndTarget" /> In the charge page itself - Redirect only<br />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					URL: &nbsp;<asp:TextBox runat="server" id="ReplyAtEndURL" size="70" MaxLength="200" class="inputdata" /><br />
				</td>
			</tr>
			<tr><td height="6"><br /></td></tr>
			<tr>
				<td valign="top">Receiving hidden<br />reply<br /></td>
				<td valign="top">
					<asp:CheckBox runat="server" id="isReplySilent" /> Allow receiving hidden reply automatically when charge is made (POST)<br />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					URL: &nbsp;<asp:TextBox runat="server" id="ReplySilentURL" size="70" MaxLength="200" class="inputdata" /><br />
				</td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td></td>
				<td style="padding-left:22px;">
					<table border="0" cellspacing="0" cellpadding="4" width="400" style="border:1px solid #c5c5c5;">
					<tr>
						<td>
							<span style="text-decoration:underline;">Test your page with this reply</span><br />
							(Reply=000&TransID=1000&Date=01/01/1900 00:00:01) &nbsp;
							<asp:Button runat="server" Text="SEND" ID="btnSendTest" style="font-size:11px;" onclick="btnSendTest_Click" /><br />
							<asp:Literal runat="server" ID="ltTextResult" Mode="PassThrough" />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td></td>
				<td>
					<br/>
					<asp:Button runat="server" UseSubmitBehavior="true" ID="btnSave" Text=" Update " class="submit" OnCommand="btnSave_Click" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
</asp:Content>

