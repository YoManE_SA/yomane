<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="CART - MESSAGE FIELDS" %>

<script runat="server">
	public sub Page_Load
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
		<tr>
			<td>
				<span class="PageHeading">SHOPPING CART</span>
				<span class="PageSubHeading"> - MESSAGE FIELDS</span><br />
			</td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">PAGE URL<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td><%=CurrentDomain.CartUrl %><br /></td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">Add Item Fields<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
						<th width="46">Max<br />Length<br /></th>
						<th width="80" valign="bottom">Required<br /></th>
					</tr>
					<tr>
						<td>merchantID</td>
						<td>Your company number <%	If WebUtils.LoggedUser IsNot Nothing Then Response.Write(" - " & Merchant.AccountNumber)%><br /></td>
						<td>7</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>itemName</td>
						<td>Cart item name</td>
						<td>100</td>
						<td>Yes</td>
					</tr>			
					<tr>
						<td>currency</td>
						<td>Item price and shipping currency</td>
						<td>2</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>price</td>
						<td>Item price</td>
						<td>&nbsp;</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>taxRate</td>
						<td>Item tax percentage</td>
						<td>&nbsp;</td>
						<td>Optional</td>
					</tr>
					<tr>
						<td>shipping</td>
						<td>Item shipping cost</td>
						<td>&nbsp;</td>
						<td>Optional</td>
					</tr>
					<tr>
						<td>infoUrl</td>
						<td>Item information page URL</td>
						<td>&nbsp;</td>
						<td>Optional</td>
					</tr>
					<tr>
						<td>imageUrl</td>
						<td>Item image page URL, must be jpg or gif image format</td>
						<td>&nbsp;</td>
						<td>Optional</td>
					</tr>
					<tr>
						<td>itemSignature</td>
						<td>
							Signature for verifying the authenticity of the request parameters.<br />
							Field values to use: <i>itemName + price + shipping + taxRate + PersonalHashKey</i><br /><br />
							Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
						</td>
						<td>&nbsp;</td>
						<td>Optional</td>
					</tr>	
				</table>
			</td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">Open Cart, Checkout<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
						<th width="46">Max<br />Length<br /></th>
						<th width="80" valign="bottom">Required<br /></th>
					</tr>
					<tr>
						<td>merchantID</td>
						<td>Your company number <%	If WebUtils.LoggedUser IsNot Nothing Then Response.Write(" - " & Merchant.AccountNumber)%><br /></td>
						<td>7</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>checkout</td>
						<td>to go directly to checkout page send true<br /></td>
						<td>4</td>
						<td>no</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><br /></td></tr>
	</table>
</asp:Content>