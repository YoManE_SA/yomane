﻿<%@ Page Language="VB" Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE V2 WIZARD" MasterPageFile="~/Templates/Tmp_netpayintl/MasterPage.master" AutoEventWireup="true" Inherits="Netpay.MerchantDevCenter.Website_HostedPageV2_Wizard" Codebehind="HostedPageV2_Wizard.aspx.vb" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentHeader" runat="Server">
	<script type="text/javascript">
		function showSection(secName)
		{
			with (document.getElementById(secName))
			{
				if (style.display != '') 
					style.display = ''; 
				else 
					style.display = 'none';
				document.getElementById(secName + 'Img').src = (style.display != '' ? '/NPCommon/Images/tree_expand.gif' : '/NPCommon/Images/tree_collapse.gif');
			}
		}
    </script>
	<style type="text/css">
		input { background-color: white !important; }
		input.wide { width: 100%; }
		input.option { vertical-align: middle; }
		input.bkgwhite { background-color: #ffffff !important; }
	</style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td>
				<span class="PageHeading">HOSTED PAYMENT PAGE VER.2</span>
				<span class="PageSubHeading"> - WIZARD</span><br />
				<custom:AuthAlert ID="aa" PropertyToCheck="HostedPaymentV2" runat="server" />
			</td>
		</tr>
		<tr>
			<td>
				<br />
				The following form will help you create a link to place in your website.<br />
				For a complete list of all possible parameters see <i>Hosted Page V.2 - <a href="HostedPageV2_MsgFields.aspx?T=<%=Request("T")%>">
					Message Fields</a></i>
				<img src="/NPCommon/images/iconNewWinRight.GIF" align="middle" /><br />
			</td>
		</tr>
	</table>
	<br />
	<br />
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="top">
				<table border="0" style="width: 360px;">
					<tr>
						<td class="SecHeading">
							Required data<br />
							<br />
						</td>
					</tr>
					<!--
					<tr>
						<td>merchantID<br /></td>
						<td><asp:TextBox runat="server" class="wide" id="merchantID" Enabled="false" /><br /></td>
					</tr>
					-->
					<tr>
						<td valign="top">
							<table border="0" cellspacing="2" cellpadding="1">
								<tr>
									<td valign="top" style="width: 140px;">
										Payment Type
										<netpay:PageItemHelp ID="PageItemHelp4" Text="If none are selected all available payment options will be shown, otherwise the first method will be selected by default with a button to change"
											Width="300" runat="server" />
									</td>
									<td style="width: 235px;">
										<table>
											<tr>
												<td>
													<asp:CheckBox runat="server" ID="PaymentMedhodCC" value="CC" Checked="true" />Credit Card
												</td>
												<td>
													<asp:CheckBox runat="server" ID="PaymentMedhodEC" value="EC" />ECheck
												</td>
												<td>
													<asp:CheckBox runat="server" ID="PaymentMedhodWT" value="WT" />Wallet
												</td>
											</tr>
											<tr>
												<td colspan="3">
													<asp:CheckBox runat="server" id="PaymentMedhodID" value="ID" />Instant Online Bank Transfer &nbsp;
												</td>
											</tr>
											<tr>
												<td colspan="3">
													<asp:CheckBox runat="server" id="PaymentMedhodDD" value="DD" />European Direct Debit &nbsp;
												</td>
											</tr>
											<tr>
												<td colspan="3">
													<asp:CheckBox runat="server" ID="PaymentMedhodOB" value="OB" />Online Bank Transfer
												</td>
											</tr>
											<tr>
												<td colspan="3">
													<asp:CheckBox runat="server" ID="PaymentMedhodWM" value="WM" />Web Money
												</td>
											</tr>
											<tr>
												<td colspan="3">
													<asp:CheckBox runat="server" ID="PaymentMedhodCUP" value="CUP" />China union pay
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										UI Default Language
									</td>
									<td>
										<asp:DropDownList runat="server" ID="disp_lng">
											<asp:ListItem />
										</asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td valign="top">
										UI Language List
									</td>
									<td>
										<asp:RadioButtonList runat="server" ID="disp_lngType" RepeatDirection="Vertical" OnSelectedIndexChanged="disp_lngType_OnSelectedIndexChanged" AutoPostBack="true" CssClass="option">
											<asp:ListItem Selected="True">Show all languages</asp:ListItem>
											<asp:ListItem>Hide language selector</asp:ListItem>
											<asp:ListItem>Choose languages:</asp:ListItem>
										</asp:RadioButtonList>
										<asp:CheckBoxList runat="server" ID="disp_lngList" RepeatDirection="Horizontal" RepeatColumns="3" Enabled="false" />
									</td>
								</tr>
								<tr>
									<td></td>
									<td>
									</td>
								</tr>
								<tr>
									<td>
										Trans Amount<br />
									</td>
									<td>
										<input runat="server" onkeydown="return IsKeyDigit();" id="trans_amount" value="1.23" class="xxshort" />
										<netpay:CurrencyDropDown ID="trans_currency" runat="server" EnableBlankSelection="false" />
									</td>
								</tr>
								<tr>
									<td>
										Installments<br />
									</td>
									<td>
										<asp:DropDownList ID="ddlInstallments" runat="server" />
									</td>
								</tr>
							</table>
							<br />
							<br />
						</td>
					</tr>
					<tr>
						<td class="SecHeading">
							Optional Data<br />
							<br />
						</td>
					</tr>
					<tr>
						<td>
							<table border="0" cellspacing="2" cellpadding="1">
								<tr>
									<td style="width: 120px;">
										Text Pay For
										<netpay:PageItemHelp ID="PageItemHelp1" Text="Text displayed on the page to describe what the payment is for, for example: basket checkout, flight tickets, etc..."
											Width="300" runat="server" />
									</td>
									<td style="width: 235px;">
										<asp:TextBox runat="server" class="wide" ID="disp_payFor" Text="Purchase" /><br />
									</td>
								</tr>
								<tr>
									<td>
										Trans Comment<br />
									</td>
									<td>
										<asp:TextBox runat="server" class="wide" ID="trans_comment" Text="Testing hosted payment page" /><br />
									</td>
								</tr>
								<tr>
									<td>
										Trans RefNum<br />
									</td>
									<td>
										<asp:TextBox runat="server" class="wide" ID="trans_refNum" Text="" /><br />
									</td>
								</tr>
								<tr>
									<td>
										Redirect URL
										<netpay:PageItemHelp ID="PageItemHelp2" Text="URL within your domain to resirect the customer when payment process is complete"
											Width="300" runat="server" />
									</td>
									<td>
										<asp:TextBox runat="server" class="wide" ID="url_redirect" /><br />
									</td>
								</tr>
								<tr>
									<td>
										Notify URL
										<netpay:PageItemHelp ID="PageItemHelp3" Text="URL within your domain to send hidden server notification with the status of the transaction"
											Width="300" runat="server" />
									</td>
									<td>
										<asp:TextBox runat="server" class="wide" ID="url_notify" /><br />
									</td>
								</tr>
								<tr>
									<td>
										Skin Number
									</td>
									<td>
										<asp:DropDownList ID="ddlSkin" runat="server" />
									</td>
								</tr>
								<tr>
									<td>mobile version</td>
									<td>
										<asp:DropDownList runat="server" ID="ddldisp_mobile">
											<asp:ListItem Text="auto" />
											<asp:ListItem Text="false" Value="false" />
											<asp:ListItem Text="true" Value="true" />
										</asp:DropDownList>
									</td>
								</tr>
								<asp:PlaceHolder ID="phSignature" runat="server">
									<tr>
										<td>signature</td>
										<td>
											<asp:DropDownList runat="server" ID="ddlSignature">
												<asp:ListItem Text="" />
												<asp:ListItem Text="MD5" />
												<asp:ListItem Text="SHA256" Selected="True" />
											</asp:DropDownList>
										</td>
									</tr>			
								</asp:PlaceHolder>
							</table>
							<br />
						</td>
					</tr>
					<tr>
						<td onclick="showSection('tdRecurring')" style="cursor: pointer;">
							<img id="tdRecurringImg" src="/NPCommon/Images/tree_expand.gif" align="top" /><span class="SecHeading">Optional Recurring Data</span>
							<netpay:PageItemHelp ID="PageItemHelp6" Text="Make a transaction process again at a later time"
								Width="300" runat="server" />
							<br />
						</td>
					</tr>
					<tr>
						<td style="display: none;" id="tdRecurring">
							<table class="FormTable" border="0" cellspacing="2" cellpadding="1">
								<tr>
									<td colspan="2" style="padding-bottom: 8px;">
										For examples see <a href="RecurringInfo.aspx">Recurring information</a>
										<img src="/NPCommon/images/iconNewWinRight.GIF" align="middle" /><br />
									</td>
								</tr>
								<tr>
									<td style="width: 120px;">
										Trans Recurring 1<br />
									</td>
									<td style="width: 235px;">
										<asp:TextBox runat="server" class="wide" ID="trans_recurring1" /><br />
									</td>
								</tr>
								<tr>
									<td>
										Trans Recurring 2<br />
									</td>
									<td>
										<asp:TextBox runat="server" class="wide" ID="trans_recurring2" /><br />
									</td>
								</tr>
								<tr>
									<td>Recurring Display<br /></td>
									<td>
										<asp:DropDownList runat="server" id="disp_recurring" >
											<asp:ListItem Text="standard display" Value="0" />
											<asp:ListItem Text="hide last charge length" Value="1" />
										</asp:DropDownList>
									</td>
								</tr>
							</table>
							<br />
							<br />
						</td>
					</tr>
					<tr>
						<td onclick="showSection('tdClientInfo')" style="cursor: pointer;">
							<img id="tdClientInfoImg" src="/NPCommon/Images/tree_expand.gif" alt="expand" align="top" /><span
								class="SecHeading">Optional Filling Client Data</span>
								<netpay:PageItemHelp ID="PageItemHelp5" Text="Depending on payment type used, if fields are used on page then automaticly fill value with data from below"
								Width="300" runat="server" />
							<br />
						</td>
					</tr>
					<tr>
						<td style="display: none;" id="tdClientInfo">
							<table class="FormTable" border="0" cellspacing="2" cellpadding="1">
								<tr>
									<td style="width: 120px;">
										Full Name<br />
									</td>
									<td style="width: 235px;">
										<asp:TextBox ID="client_fullName" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Email Address<br />
									</td>
									<td>
										<asp:TextBox ID="client_email" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Phone Number<br />
									</td>
									<td>
										<asp:TextBox ID="client_phoneNum" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										ID Number<br />
									</td>
									<td>
										<asp:TextBox ID="client_idNum" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										<br />
									</td>
								</tr>
								<tr>
									<td>
										Billing Address 1<br />
									</td>
									<td>
										<asp:TextBox ID="client_billaddress1" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Billing Address 2<br />
									</td>
									<td>
										<asp:TextBox ID="client_billaddress2" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Billing City<br />
									</td>
									<td>
										<asp:TextBox ID="client_billcity" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Billing Zipcode<br />
									</td>
									<td>
										<asp:TextBox ID="client_billzipcode" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Billing State<br />
									</td>
									<td>
										<asp:TextBox ID="client_billstate" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Billing Country<br />
									</td>
									<td>
										<asp:TextBox ID="client_billcountry" class="wide" runat="server" />
									</td>
								</tr>
							</table>
							<br />
							<br />
						</td>
					</tr>
					<tr>
						<td>
							<br />
						</td>
					</tr>
				</table>
			</td>
			<td valign="top" align="right">
				<table border="0" align="right" style="background-color: #f5f5f5; width: 314px;">
					<tr>
						<td class="SecHeading">
							Generated Code<br />
						</td>
					</tr>
					<tr>
						<td>
							<br />
						</td>
					</tr>
					<tr>
						<td colspan="2" valign="top">
							<div runat="server" ID="txQuery"
								style="border: 1px dashed #c0c0c0; background-color: #f7f7f7; overflow: auto; word-wrap:break-word;
								width: 306px; height: 250px; font: normal 12px Courier New, Monospace;" />
							<br />
							<asp:Button runat="server" ID="btnGenerate" OnClick="btnGenerate_Click" Text="Refresh Code"
								Style="background-color: white; font-size: 10px; width: 100px; cursor: pointer;" />
							&nbsp;&nbsp;
							<asp:Button runat="server" ID="btnOpenPage" OnClick="btnOpenPage_Click" UseSubmitBehavior="true"
								Text="Open Window" Style="background-color: white; font-size: 10px; width: 100px;
								cursor: pointer;" /><br />
							<br />
                            <asp:Checkbox runat="server" id="chkGenerateForm" />Generate code as form inputs fields</br></br>
							<span style="font-size: 10px;">Refresh and copy the gernerated link into your website.
								To see resulted code click the "Open Window" button</span><br /><br />
							<asp:PlaceHolder runat="server" ID="phQrCode">
								<b>QR Code</b><br />
								The below QR Code represents the above link to the HPP<br />
								<div style="text-align:center;"><asp:Image runat="server" ID="imgQrCode" /><br />(ISO 18004:2006)</div>
							</asp:PlaceHolder>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<asp:Literal ID="OpenScript" runat="server" Mode="PassThrough" ViewStateMode="Disabled" />
</asp:Content>
