<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - VERIFY TRANSACTIONS" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    Protected Sub Page_Load()
        If WebUtils.IsLoggedin Then
            lblCompany1.Text = CurrentDomain.BrandName
            lblCompany2.Text = CurrentDomain.BrandName
        Else
            lblCompany1.Text = "&lt;Company Name&gt;"
            lblCompany2.Text = "&lt;Company Name&gt;"
        End If
    End Sub

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">VERIFY TRANSACTIONS</span>
			<span class="PageSubHeading">- ABOUT</span><br />
		</td>
	</tr>		
	<tr>
		<td>
			<br />
			<table cellspacing="0" cellpadding="1" border="0" width="100%">
			<tr>
				<td>
					<span class="SecHeading">What is Transaction Verification</span><br />
					Transaction Verification is intended for merchants who are using
					either silent post or hosted payment page to make transactions,
					if they wish to verify one more time (at the end of a successful transaction)
					that the transaction has been received at
					<asp:Label ID="lblCompany1" runat="server" />
					with the correct data.
					<br />
					With Transaction Verification you can verify,
					that an integrity of a transaction has been maintained during transmission
					to and from <asp:Label ID="lblCompany2" runat="server" /> system.
					<br /><br />
					<span class="SecHeading">When to use</span><br />
					If you are using hosted payment page or silent post,
					it is recommended to use Transaction Verification
					just after a transaction has been completed successfully.
					<br />
					<br /><br />
				</td>
				
			</tr>
			<tr>
				<td colspan="2">
					<span class="SecHeading">How to use</span><br />
					To use Transaction Verification, send HTTP request from your server just after successfully completed transaction.<br />
					For details about request and response fields, visit <a href="VerifyTrans_MsgFields.aspx">VERIFY TRANSACTIONS - Message Fields</a> page.
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
	
</asp:Content>
