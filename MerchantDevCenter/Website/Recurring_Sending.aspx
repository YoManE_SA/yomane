<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="MANAGE RECURRING SERIES - SUBMITTING DATA" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
    Public Sub Page_Load()
        '
        ' 20080423 Tamir
        '
        ' Support for multiple devcenter sites
        '
        lblProcessURL.Text = CurrentDomain.ProcessUrl & "recurring_modify.aspx"
        litMerchant.Text = MerchantNumber
        litSample1.Text = CurrentDomain.ProcessUrl & "recurring_modify.aspx?MerchantNumber=" & MerchantNumber & "&amp;SeriesID=123&amp;Action=SUSPEND"
        litSample2.Text = CurrentDomain.ProcessUrl & "recurring_modify.aspx?MerchantNumber=" & MerchantNumber & "&amp;TransID=987654&amp;Action=RESUME"
        litSample3.Text = CurrentDomain.ProcessUrl & "recurring_modify.aspx?MerchantNumber=" & MerchantNumber & "&amp;SeriesID=123&amp;Action=MODIFY&amp;StartDD=15&amp;StartMM=07&amp;StartYYYY=2009&amp;Recurring1=1W2A5.99&ampRecurring2=11M1A9.99"

        If Not Page.IsPostBack Then
            If WebUtils.IsLoggedin Then
                litSignatureMandatory.Text = IIf(Netpay.Bll.Transactions.Recurring.MerchantSettings.Current.ForceMD5OnModify , "Yes", "No")
            End If
        End If
    End Sub
</script>
<asp:content id="Content1" contentplaceholderid="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">MANAGE RECURRING SERIES</span>
			<span class="PageSubHeading">- SUBMITTING DATA</span><br />
			<custom:AuthAlert PropertyToCheck="AllowRecurringModify" runat="server" />
		</td>
	</tr>
	<tr>
		<td>
			<br />
			This page describes how to remotly edit/cancel an existing recurring series.<br />
			<br />
			Using this interface you can:
			<ul style="list-style-type:square;">
				<li>suspend active series</li>
				<li>resume suspended series</li>
				<li>modify dates and/or amounts of future charges</li>
				<li>delete series</li>
			</ul>
			The request must be sent from your server to the following URL:
			<ul style="list-style-type:square;">
				<li><asp:Label ID="lblProcessURL" runat="server" /></li>
			</ul>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td style="padding-bottom:4px;">
			Sample requests:<br />
		</td>
	</tr>
	<tr>
		<td style="padding-bottom:8px;">
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					<textarea cols="1" rows="1" onclick="select();" class="sampleCode"><asp:Literal ID="litSample1" runat="server" /></textarea>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-bottom:8px;">
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					<textarea cols="1" rows="1" onclick="select();" class="sampleCode"><asp:literal ID="litSample2" runat="server" /></textarea>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-bottom:8px;">
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					<textarea cols="1" rows="1" onclick="select();" class="sampleCode"><asp:literal ID="litSample3" runat="server" /></textarea>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td><br /><b>REQUEST FIELDS</b><br /></td></tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th>Field<br /></th>
				<th>Required<br /></th>
				<th>Description<br /></th>
			</tr>
			<tr>
				<td>MerchantNumber<br /></td>
				<td>Yes</td>
				<td>Your merchant number <asp:Literal ID="litMerchant" runat="server" /></td>
			</tr>
			<tr>
				<td>SeriesID<br /></td>
				<td>Yes<sup>1</sup></td>
				<td>ID number of the recurring series.<br />This field is required only if TransID is omitted.<br /></td>
			</tr>
			<tr>
				<td>TransID<br /></td>
				<td>Yes<sup>1</sup></td>
				<td>ID number of the initial transaction that started the recurring series.<br />This field is required only if SeriesID is omitted.<br /></td>
			</tr>
			<tr>
				<td>Action<br /></td>
				<td>Yes</td>
				<td>
					Action to take on the series.<br /><br />
					Accepts one of the following values:
					<ul style="margin-top:0;padding-top:10px;margin-bottom:0;padding-bottom:0;">
						<li>SUSPEND - Suspend the specified series, if active</li>
						<li>RESUME - Resume the specified series, if suspended</li>
						<li>DELETE - Delete the specified series (this cannot be undone!)</li>
						<li>MODIFY - Drop all the future charges and create new charges based on the Recurring fields</li>
					</ul>
				</td>
			</tr>
			<tr>
				<td>Uncharged<br /></td>
				<td>No</td>
				<td>
					When resuming the series (Action=RESUME), action to take on the past charges which are still unprocessed.<br /><br />
					Accepts one of the following values:
					<ul style="margin-top:0;padding-top:10px;margin-bottom:0;padding-bottom:0;">
						<li>CANCEL - Cancel all past charges which are still unprocessed.</li>
						<li>REINSTATE - Reinstate all past charges which are still unprocessed (also charges that were manually cancelled in the past).</li>
						<li>Empty (or omitted) � No changes are made to charges' state.</li>
					</ul>
				</td>
			</tr>
			<tr>
				<td>Signature<br /></td>
				<td><asp:Literal ID="litSignatureMandatory" text="No" runat="server" /></td>
				<td>
					Signature for verifying the authenticity of the request parameters.<br />
					Field values to use: <i>MerchantNumber + SeriesID + TransID + Action + PersonalHashKey</i><br /><br />
					Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td><br /><b>OPTIONAL FIELDS - START DATE</b><br /></td></tr>
	<tr>
		<td>
			<div style="padding:7px 15px 15px;">
				When the action is MODIFY, all the future charges in the specified recurring series are deleted.<br />
				You can specify a date to start new charges' schedule.<br />
				If omitted, the first of the new charges will be scheduled for today.<br />
				<br />
				If the first of the new charges is scheduled for today, it will be processed immediately!<br />
				<br />
			</div>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th>Field<br /></th>
				<th>Length<br /></th>
				<th>Description<br /></th>
				<th>Details<br /></th>
			</tr>
			<tr>
				<td>StartDD<br /></td>
				<td>2<br /></td>
				<td>Day<br /></td>
				<td>Day to start the future charges. Between 1 and 31, depending on month specified in StartMM.</td>
			</tr>
			<tr>
				<td>StartMM<br /></td>
				<td>2<br /></td>
				<td>Month<br /></td>
				<td>Month to start the future charges. Between 1 and 12.<br /></td>
			</tr>
			<tr>
				<td>StartYYYY<br /></td>
				<td>4<br /></td>
				<td>Year<br /></td>
				<td>Year to start the future charges. Cannot be less than the current year.<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br/></td></tr>
	<tr><td><br /><b>OPTIONAL FIELDS - RECURRING STRINGS</b></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<div style="padding:7px 15px 15px;">
				When the action is MODIFY, new payment schedule must be set.<br />
				<br />
				See <a href="RecurringInfo.aspx">Recurring information</a> for the recurring string structure and examples.<br />
				<br />
				Pay attention: recurring series is limited in time. The maximum duration is 10 years.<br />
			</div>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
				<th width="46">Max<br />Length<br /></th>
				<th width="80" valign="bottom">Required <sup>2</sup><br /></th>
			</tr>
			<tr>
				<td>Recurring1<br /></td>
				<td>Parameter for first stage in the recurring series<br /></td>
				<td>12</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Recurring2<br /></td>
				<td>Parameter for second stage in the recurring series<br /></td>
				<td>12</td>
				<td>No</td>
			</tr>
			<tr>
				<td>Recurring3<br /></td>
				<td>Parameter for third stage in the recurring series<br /></td>
				<td>12</td>
				<td>No</td>
			</tr>
			<tr>
				<td>Recurring4<br /></td>
				<td>Parameter for fourth stage in the recurring series<br /></td>
				<td>12</td>
				<td>No</td>
			</tr>
			<tr>
				<td>Recurring5<br /></td>
				<td>Parameter for fifth stage in the recurring series<br /></td>
				<td>12</td>
				<td>No</td>
			</tr>
			<tr>
				<td>Recurring6<br /></td>
				<td>Parameter for sixth stage in the recurring series<br /></td>
				<td>12</td>
				<td>No</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br /><hr size="1" width="100%" noshade="noshade" />
	(1) Either SeriesID or TransID must be provided<br />
	(2) When the action is MODIFY<br />
</asp:content>