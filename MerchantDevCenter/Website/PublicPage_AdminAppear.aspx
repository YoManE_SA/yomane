<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  EnableEventValidation="false" validateRequest="False" Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<script runat="server">
	Dim sExtraTxt, sLinkTxt As String
	
	Dim langTxt As String = ""
	Dim langCode As Integer = 0
	Dim styleDir As String = ""
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Select Case Trim(Request("lang"))
			Case "eng" : langTxt = "English" : langCode = 1 : styleDir = "ltr"
			Case "spa" : langTxt = "Spanish" : langCode = 2 : styleDir = "ltr"
			Case "heb" : langTxt = "Hebrew" : langCode = 0 : styleDir = "rtl"
		End Select
        iuLogo.TargetFileName = Account.MapPublicPath("/UIServices/PP_")
        If Not IsPostBack Then DataLoad()
	End Sub

	Private Sub DataLoad()
		Dim bImageExist As Boolean = False
        Dim iReader As SqlDataReader = dbPages.ExecReader("SELECT * FROM tblPublicPayAdmin WHERE CompanyID=" & Account.MerchantID & " AND lang=" & langCode)
		If iReader.Read() Then
			TextTop.Text = dbPages.dbtextShowForm(iReader("TextTop"))
			TextBottom.Text = dbPages.dbtextShowForm(iReader("TextBottom"))
			EndTextPass.Text = dbPages.dbtextShowForm(iReader("EndTextPass"))
			EndTextFail.Text = dbPages.dbtextShowForm(iReader("EndTextFail"))
			If iReader("isAsk4PersonalInfo") Then isAsk4PersonalInfo1.Checked = True Else isAsk4PersonalInfo2.Checked = True
			Dim rgl As RadioButton() = {isChargeOptions1, isChargeOptions2, isChargeOptions3}
			rgl(dbPages.TestVar(iReader("isChargeOptions"), 0, -1, 0)).Checked = True
            iuLogo.FileName = Account.MapPublicPath("/UIServices/" & Trim(iReader("logoPic")))
		End If
		iReader.Close()
	End Sub
	
	Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim sChargeOptionsValue As String = Request("ctl00$ContentPlaceHolder1$isChargeOptions")
		If String.IsNullOrEmpty(sChargeOptionsValue) Then sChargeOptionsValue = "0"
		Dim isChargeOptions As Integer = sChargeOptionsValue.ToInt32(0, 2, 0)
        Dim sSQL As String = "UPDATE tblPublicPayAdmin SET" & _
          " TextTop=Left('" & TextTop.Text.ToSql(True) & "', 500)," & _
          " TextBottom=Left('" & TextBottom.Text.ToSql(True) & "', 500)," & _
          " EndTextPass=Left('" & EndTextPass.Text.ToSql(True) & "', 500)," & _
          " EndTextFail=Left('" & EndTextFail.Text.ToSql(True) & "', 500)," & _
          " isAsk4PersonalInfo=" & IIf(isAsk4PersonalInfo1.Checked, 1, 0) & "," & _
          " isChargeOptions=" & isChargeOptions & _
          " WHERE CompanyID=" & Account.MerchantID & " AND lang=" & langCode
		If (dbPages.ExecSql(sSQL) = 0) Then
			sSQL = "INSERT INTO tblPublicPayAdmin" & _
			 " (TextTop, TextBottom, EndTextPass, EndTextFail, isAsk4PersonalInfo, isChargeOptions, CompanyID, lang)" & _
			 " VALUES(Left('" & TextTop.Text.ToSql(True) & "', 500)," & _
			 " Left('" & TextBottom.Text.ToSql(True) & "', 500)," & _
			 " Left('" & EndTextPass.Text.ToSql(True) & "', 500)," & _
			 " Left('" & EndTextFail.Text.ToSql(True) & "', 500)," & _
			 " " & IIf(isAsk4PersonalInfo1.Checked, 1, 0) & "," & _
			 " " & isChargeOptions & ", " & Account.MerchantID & "," & langCode & ")"
			dbPages.ExecSql(sSQL)
		End If
	End Sub

	Sub iuLogo_OnDataUpdate(ByVal sender As Object, ByVal e As System.EventArgs)
		dbPages.ExecSql("UPDATE tblPublicPayAdmin SET logoPic='" & iuLogo.SaveFileName.ToSql(True) & "' WHERE CompanyID=" & Account.MerchantID)
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<script src="../include/formValidator2.js" type="text/javascript"></script>
	<script type="text/javascript">
		var vm = new validationManager("english");
		function registerValidation() 
		{
			formInstance = document.frmPublicPay;
			vm.add(new textValidation(document.getElementById('<%=TextTop.ClientID%>'), "Upper Text field", "string", 0, 500, false));
			vm.add(new textValidation(document.getElementById('<%=TextBottom.ClientID%>'), "Lower Text field", "string", 0, 500, false));
			vm.add(new textValidation(document.getElementById('<%=EndTextPass.ClientID%>'), "Completion Text - Trans. Done field", "string", 0, 500, false));
			vm.add(new textValidation(document.getElementById('<%=EndTextFail.ClientID%>'), "Completion Text - Trans. Rejected field", "string", 0, 500, false));
		}
		function validate() 
		{
			return vm.validate()
		}
	</script>

	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">PUBLIC PAYMENT PAGE</span>
			<span class="PageSubHeading">- MANAGE APPEARANCE (<%=langTxt%>)</span><br />
		</td>
		<td align="right">
			<%
				Dim ArrayLang As String() = {"English", "Spanish"}
				If CurrentDomain.IsHebrewVisible Then
					ReDim Preserve ArrayLang(ArrayLang.Length)
					ArrayLang(UBound(ArrayLang)) = "Hebrew"
				End If
				For i As Integer = 0 To ArrayLang.Length - 1
					sExtraTxt = "Off"
					sLinkTxt = ""
					If Trim(Left(ArrayLang(i), 3).ToLower) <> Trim(Request("lang").ToLower) Then
						sExtraTxt = "On"
						sLinkTxt = " onclick=""location.href='?lang=" & Trim(Left(ArrayLang(i), 3).ToLower) & "';"""
					End If
					Response.Write("<span title=""" & ArrayLang(i) & """ class=""pageMenu" & sExtraTxt & """" & sLinkTxt & ">" & Left(ArrayLang(i), 3).ToUpper & "</span>")
				Next
			%>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<br/><br/>
			This page enables you to define the appearance of the public payment page.<br/><br/>
		</td>
	</tr>
	<tr><td height="8"></td></tr>
	<tr>
		<td colspan="2">
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr>
				<td width="19%" valign="top" style="color:#003366;">Company Logo<br /></td>
				<td width="30%" valign="top">Image file, presented in the upper part of the page, above the payment form.<br/>Size in pixels: 150x35<br/></td>
				<td width="2%"></td>
				<td valign="top">
					<netpay:ImageUpload ID="iuLogo" runat="server" ImageWidth="150" ImageHeight="35" OnDataUpdate="iuLogo_OnDataUpdate" />
				</td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td valign="top" style="color:#003366;">Upper Text<br /></td>
				<td valign="top">This text is presented in the upper part of the page, above the payment form. <br />(500 characters max.)<br /></td>
				<td></td>
				<td><asp:TextBox runat="server" Rows="4" TextMode="MultiLine" style="font-size:11px;" id="TextTop" dir="<%= sStyleDir %>" /></td>
			</tr>
			<tr>
				<td valign="top" style="color:#003366;">Lower Text<br /></td>
				<td valign="top">This text is presented in the lower part of the page, under the payment form.<br />(500 characters max.)<br /></td>
				<td></td>
				<td><asp:TextBox runat="server" Rows="4" TextMode="MultiLine" style="font-size:11px;" ID="TextBottom" dir="<%= sStyleDir %>" /></td>
			</tr>
			<tr>
				<td valign="top" style="color:#003366;">Completion Text - Successful Charge<br /></td>
				<td valign="top">This text is presented in the following page, once the transactions is done.<br />(500 characters max.)<br /></td>
				<td></td>
				<td><asp:TextBox runat="server" Rows="4" TextMode="MultiLine" style="font-size:11px;" ID="EndTextPass" dir="<%= sStyleDir %>" /><br/></td>
			</tr>
			<tr>
				<td valign="top" style="color:#003366;">Completion Text - Charge Rejected<br /></td>
				<td valign="top">This text is presented in the following page, when the transactions has been rejected.<br />(500 characters max.)<br /></td>
				<td></td>
				<td><asp:TextBox runat="server" Rows="4" TextMode="MultiLine" style="font-size: 11px;" ID="EndTextFail" dir="<%= sStyleDir %>" /><br /></td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td valign="top" style="color:#003366;">Personal Details<br /></td>
				<td valign="top">Choose if you want to present the client with personal details fields.<br /></td>
				<td></td>
				<td valign="top">
					<asp:RadioButton runat="server" GroupName="Ask4PersonalInfo" id="isAsk4PersonalInfo1" /> Yes, show personal details fields.<br/>
					<input type="checkbox" style="visibility:hidden;"/> [<a class="faq" style="font-size: 11px;" href="PublicPage_AdminAppearInfo.aspx?lang=<%= trim(request("lang")) %>" >Define Fields</a>]<br/>
					<asp:RadioButton runat="server" GroupName="Ask4PersonalInfo" id="isAsk4PersonalInfo2" /> No, do not show personal details fields.<br/>
				</td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td valign="top" style="color:#003366;">Payment Options<br /></td>
				<td valign="top">Define payment options, or let the client decide for himself.<br /></td>
				<td></td>
				<td valign="top">
					<asp:RadioButton runat="server" ID="isChargeOptions2" GroupName="isChargeOptions" value="1" /> Yes, let me define the payment options.<br/>
					<input type="checkbox" style="visibility:hidden;"/> [<a class="faq" style="font-size: 11px;"  href="PublicPage_AdminAppearChargeList.aspx?lang=<%= trim(request("lang")) %>" >Define Options</a>]<br/>
					<asp:RadioButton runat="server" ID="isChargeOptions1" GroupName="isChargeOptions" value="0" /> No, let the client decide the<br/>
					<input type="checkbox" style="visibility:hidden;"/> payment options. [<a class="faq" style="font-size: 11px;"  href="PublicPage_AdminAppearChargeFree.aspx?lang=<%= trim(request("lang")) %>" >Define Options</a>]<br/>
					<asp:RadioButton runat="server" ID="isChargeOptions3" GroupName="isChargeOptions" value="2" /> Show my options, and let the client<br/>
					<input type="checkbox" style="visibility:hidden;" /> decide the payment options.<br/>
				</td>
			</tr>
			<tr>
				<td colspan="3"></td>
				<td>
					<br/>
					<asp:Button runat="server" UseSubmitBehavior="true" id="btnUpdate" Text=" Update " class="submit" OnClick="btnUpdate_Click" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />

</asp:Content>