﻿<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="RECURRING INFORMATION" %>
<script runat="server">

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td><span class="PageHeading">RECURRING INFORMATION</span></td>
	</tr>
	<tr>
		<td>
			<br />
			The following examples are an overview on how to send recurring data.<br />
			The code shown is intended to give you a start point on how to implement recurring information in our services.<br />
			<br />
			Pay attention: recurring series is limited in time. The maximum duration is 10 years.<br />
		</td>
	</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="3" align="center" width="100%">
	<tr>
		<td>
			<br />Each recurring field has the following format:<br />
			<span style="color:#003366;">{number of charges}{gap unit}{gap length}A{amount}</span><br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<table class="FieldsTable" border="0" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th style="white-space:nowrap;">Recurring Field Part</th>
				<th>Description</th>
				<th colspan="2">Possible Values</th>
			</tr>
			<tr>
				<td style="white-space:nowrap;">{number of charges}</td>
				<td>A number of charges in this stage of the recurring series</td>
				<td>A positive integer number between 1 and 99</td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">{gap unit}</td>
				<td>A unit to specify a gap between the dates of subsequent charges</td>
				<td>D (day), W (week), M (month), Q (quarter), Y (year)</td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">{gap length}</td>
				<td>A number of gap units</td>
				<td>1 or more</td>
			</tr>
			<tr>
				<td style="white-space:nowrap;">A{amount}</td>
				<td>Amount to charge every time (optional) with decimal point (if needed)</td>
				<td>
					Use only if the amount to charge at this stage is different from the initial transaction amount.
					If the amount is omitted, letter A must be omitted, too.</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br/></td></tr>
	<tr>
		<td>
			Here are some useful samples of recurring series (having a transaction amount $10):<br />
			<br />
			<table style="padding-left:30px;" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding-bottom:5px;">
						<span style="color:#003366;">Recurring1=12M1</span><br />
						Annual subscription with monthly charges, $10 each charge<br />
						Total amount is $120 for a year
					</td>
				</tr>
				<tr>
					<td style="padding-bottom:5px;">
						<span style="color:#003366;">Recurring1=4Q1</span><br />
						Annual subscription with quarterly charges, $10 each charge<br />
						Total amount is $40 for a year
					</td>
				</tr>
				<tr>
					<td style="padding-bottom:5px;">
						<span style="color:#003366;">Recurring1=1D5&amp;Recurring2=12M1A30</span><br />
						5 days trial, then annual subscription with monthly charges, $30 each charge<br />
						Total amount is $370 for a year and five days
					</td>
				</tr>
				<tr>
					<td style="padding-bottom:5px;">
						<span style="color:#003366;">Recurring1=1D5&amp;Recurring2=1D25A20&amp;Recurring3=11M1A30</span><br />
						5 days trial, then a "remainder" charge ($20 for 25 days), then 11 months subscription with monthly charges, $30 each payment<br />
						Total amount is $360 for a year
					</td>
				</tr>
				<tr>
					<td style="padding-bottom:5px;">
						<span style="color:#003366;">Recurring1=3M1&amp;Recurring2=3M1A20&amp;Recurring3=6M1A30</span><br />
						Annual subscription: first three months for $10 a month, then three months for $20 a month, then 6 months for $30 a month<br />
						Total amount is $270 for a year
					</td>
				</tr>
				<tr>
					<td style="padding-bottom:5px;">
						<span style="color:#003366;">Recurring1=1M1&amp;Recurring2=2M1A10&amp;Recurring3=3M1A20&amp;Recurring4=6M1A30&RecurringTransType=0</span><br />
						Annual subscription: first month for free, then two months for $10 a month, then three months for $20 a month, then 6 months for $30 a month<br />
						Total amount is $260 for a year.<br />
					</td>
				</tr>
				<tr>
					<td style="padding-bottom:5px;">
						Important for this sample<br />
						- TransType must be 1, this ensures first month for free.<br />
						- Amount should be 1, in order to affect the payment ability of the credit card as less as possible.<br />
					</td>
				<tr>
			</table>
		</td>
	</tr>
	</table>
</asp:Content>
