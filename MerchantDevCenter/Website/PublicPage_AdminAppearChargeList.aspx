<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<%@ Import namespace="Netpay.Infrastructure" %>
<script runat="server">
	Dim sSQL, sCheckText, sPayments As String
	Dim i, nCount As Integer
	Dim iReader As SqlDataReader
		
	Dim langTxt As String = ""
	Dim langCode As Integer = 1
	Dim styleDir As String = ""
	
	public Function NullIfDBNull(source As object ) As Object 
		if (source is DBNull.Value) Then return Nothing
		return source
	End Function
		
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		'
		' 20080424 Tamir
		'
		' Support for multiple devcenter sites
		'
		lblCurrency.Text = "<select name=""Currency"" class=""input2"">"
		For Each v In dbPages.Currencies
			lblCurrency.Text &= "<option value=""" & v.ID & """>" & v.IsoCode & "</option>"
		Next
		lblCurrency.Text &= "</select>"
		Select Case Trim(Request("lang"))
			Case "spa" : langTxt = "Spanish" : langCode = 2 : styleDir = "ltr"
			Case "heb" : langTxt = "Hebrew" : langCode = 0 : styleDir = "rtl"
			Case Else : langTxt = "English" : langCode = 1 : styleDir = "ltr"
		End Select

		Dim idVal As Integer = 0
		If Not Request("ID") = Nothing Then idVal = Integer.Parse(Request("ID"))
			
		If Trim(Request("Action")) = "Add" Then
			Dim nQtyStart As Integer = Request("QtyStart")
			Dim nQtyEnd As Integer = Request("QtyEnd")
			Dim nQtyStep As Integer = Request("QtyStep")
			If (nQtyEnd < nQtyStart) Or (nQtyStep < 1) Or (nQtyStep > nQtyEnd) Or (((nQtyEnd - nQtyStart) / nQtyStep) > 50) Then
				nQtyStart = 1
				nQtyEnd = 1
				nQtyStep = 1
			End If
			
			Dim payments As Integer = 1
			If Not String.IsNullOrEmpty(Request("payments")) Then payments = Request("payments").ToInt32(1)
			Dim creditType As Integer = Integer.Parse(Request("CreditType"))
			Dim currency As Integer = Integer.Parse(Request("Currency"))
			Dim amount As Integer = Request("Amount").ToInt32(1)
			Dim priority As Integer = Integer.Parse(Request("Priority"))
			sSQL = "INSERT INTO tblPublicPayChargeOptions(Text, Payments, CreditType, Currency, Amount, CompanyID, lang, QtyStart, QtyEnd, QtyStep, Priority) " & _
				"VALUES('" & Request("Text").Trim().ToSql(True) & "'," & payments & "," & creditType & "," & currency & "," & amount & "," & WebUtils.LoggedUser.ID & "," & langCode & ", " & nQtyStart & ", " & nQtyEnd & ", " & nQtyStep & ", " & priority & ")"
			dbPages.ExecSql(sSQL)
		ElseIf Trim(Request("Action")) = "x" Then
			sSQL = "SELECT TOP 1 ImageFileName FROM tblPublicPayChargeOptions WHERE id = " & idVal
			Dim imageName As String = dbPages.TestVar(dbPages.ExecScalar(sSQL), -1, "")
			dbPages.ExecSql("DELETE FROM tblPublicPayChargeOptions WHERE ID=" & idVal)
		ElseIf Trim(Request("Action")) = "Update" Then
			Dim nDefault As Integer? = Request("IsDefault").ToNullableInt32()
			If nDefault Is Nothing Then Exit Sub
			dbPages.ExecSql("UPDATE tblPublicPayChargeOptions SET isDefaultCheck=0 WHERE CompanyID = " & WebUtils.LoggedUser.ID & " AND lang = " & langCode)
			If nDefault > 0 Then dbPages.ExecSql("UPDATE tblPublicPayChargeOptions SET isDefaultCheck=1 WHERE ID=" & nDefault)
		End If
		sSQL = "SELECT * FROM tblPublicPayChargeOptions WHERE CompanyID=" & WebUtils.LoggedUser.ID & " AND lang=" & langCode & " ORDER BY Priority"
		lstPrd.DataSource = dbPages.ExecReader(sSQL)
		lstPrd.DataBind()
		CType(lstPrd.DataSource, System.Data.IDataReader).Close()

	End Sub
	
	Function drawQuantityCombo(ByVal nQtyStart As Integer, ByVal nQtyEnd As Integer, ByVal nQtyStep As Integer) As String
		Dim pRet As String = ""
		If nQtyStart = nQtyEnd Then
			'response.write "����-<span class='txt11'>" & nQtyStart  & "</span>&nbsp;&nbsp;"
		Else
			pRet &= "<span class=""txt11"">Qty:</span> <select name='qty'>"
			For n As Integer = nQtyStart To nQtyEnd Step nQtyStep
				pRet &= "<option value='" & n & "'>" & n & "</option>"
			Next
			pRet &= "</select>"
		End If
		Return pRet
	End Function
	
	Function drawPaymentsCombo(ByVal controlName As String, ByVal nPayments As Integer) As String
		Dim pRet As String = ""
		If nPayments > 1 Then
			pRet &= "<select name='" & controlName & "'>"
			For n As Integer = 1 To nPayments
				pRet &= "<option value='" & n & "'>" & n & "</option>"
			Next
			pRet &= "</select>"
		Else
			pRet = "<span>1 payments</span>"
		End If
		Return pRet
	End Function
	
	Protected Sub Product_OnItemDataBound(Sender As Object, e As RepeaterItemEventArgs)
		Dim pppParams = "merchantID=" & Session("MerchantNumber") & "&item=" & e.Item.DataItem("ID")
		CType(e.Item.FindControl("imgQrCode"), Image).ImageUrl = String.Format("{0}QrCodes.asmx/RenderGenerateCode?target=PublicPaymentPageV2&urlParams={1}", WebUtils.CurrentDomain.WebServicesUrl, HttpUtility.UrlEncode(pppParams))
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<script language="JavaScript" type="text/javascript">
		var offset = <%=IIF(langCode = 1, "250", "0")%>;
		function showImg(optionId) {
			var obj = document.all('imgDiv' + optionId);
			obj.style.left = window.event.clientX - offset; //(window.event.clientX - document.body.scrollLeft);
			obj.style.top = (window.event.clientY + document.body.scrollTop);
			obj.style.display = 'block';	
		}
		function hideImg(optionId) {
			var obj = document.all('imgDiv' + optionId);
			obj.style.display = 'none';	
		}	
		function openUploadWin(optionId) {
			placeLeft = ((screen.width / 2) - 200);
			placeTop = ((screen.height / 2) - 60);
			window.open("LoadImage.aspx?ID=" + optionId + "&lang=<%=trim(request("lang"))%>&langCode=<%=langCode%>", "uploadWin" ,  "top=" + placeTop + ", left=" + placeLeft + ", height=120,width=400,status=no,toolbar=no,menubar=no,location=no");
		}	
		function IsKeyDigit() {
			var nKey=event.keyCode;
			if ((nKey==8)|(nKey==9)|(nKey==13)|(nKey==35)|(nKey==36)|(nKey==37)|(nKey==39)|(nKey==46)|(nKey==48)|(nKey==49)|(nKey==50)|(nKey==51)|(nKey==52)|(nKey==53)|(nKey==54)|(nKey==55)|(nKey==56)|(nKey==57)|(nKey==96)|(nKey==97)|(nKey==98)|(nKey==99)|(nKey==100)|(nKey==101)|(nKey==102)|(nKey==103)|(nKey==104)|(nKey==105)|(nKey==110)|(nKey==190)) { return true; };
			return false;
		}
		function ShowPayments()	{
			var frmThis=event.srcElement.form;
			if (frmThis.CreditType.value==8 || frmThis.CreditType.value==6) {
				frmThis.Payments.disabled=false;
			}
			else {
				frmThis.Payments.disabled=true;
			}
		}
		function CheckForm()
		{
			var frmThis=event.srcElement.form;
			if (frmThis.Text.value=='') {
				alert('Please enter Payment description');
				frmThis.Text.focus();
				return false;
			}
			var nQtyStart = new Number (frmThis.QtyStart.value);
			var nQtyEnd = new Number (frmThis.QtyEnd.value);
			var nQtyStep = new Number (frmThis.QtyStep.value);
			if (nQtyEnd < nQtyStart) {
				alert('The Quantity End field can not be greater than the Start field');
				frmThis.QtyEnd.focus();
				return false;
			}		
			if (nQtyStep < 1) {
				alert('Quantity Step field must be greater than 1');
				frmThis.QtyStep.focus();
				return false;
			}			
			if (nQtyStep > nQtyEnd) {
				alert('Quantity Step field must be greater than the End field');
				frmThis.QtyStep.focus();
				return false;
			}		
			if (((nQtyEnd - nQtyStart) / nQtyStep) > 50) {
				alert('The total of generated quantity options can not exceed 50');
				frmThis.QtyStep.focus();
				return false;
			}	
			if (frmThis.Amount.value=='') {
				alert('Please enter debit amount');
				frmThis.Amount.focus();
				return false;
			}
			return true;
		}
	</script>
	
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">PUBLIC PAYMENT PAGE</span>
			<span class="PageSubHeading">- PAYMENT OPTIONS (<%= langTxt%>)</span><br />
		</td>
		<td align="right">[<a class="faq" href="PublicPage_AdminAppear.aspx?lang=<%= trim(request("lang")) %>">back to page management</a>]<br /></td>
	</tr>
	<tr>
		<td colspan="2">
			<custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading" colspan="2"><br />Adding payment options:<br /></td></tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td colspan="2" style="border:1px dashed #c0c0c0; padding-top:3px; padding-bottom:3px;">
			<input type="hidden" name="Lang" value="<%= request("lang").ToSql(True) %>">
			<table width="98%" align="center" border="0" cellspacing="0" cellpadding="1">
			<tr>
				<td>
					<table border="0" cellpadding="3">
					<tr>
						<td>
							Pos.<br />
							<input class="input2" type="Text" size="2" style="font-size:12px;" onkeydown="return IsKeyDigit();" name="Priority" value="1">
						</td>	
						<td nowrap>
							Description (In english)<br />
							<input class="input2" type="Text" size="30" dir="<%= styleDir %>" style="font-size:12px;" name="Text" value=""><br />
						</td>	
						<td align="right">&nbsp;</td>	
						<td align="right">Quantity *<br />Start <input class="input2" type="Text" size="2" onkeydown="return IsKeyDigit();" name="QtyStart" value="1"></td>	
						<td align="right"><br />End <input class="input2" type="Text" size="2"  onkeydown="return IsKeyDigit();" name="QtyEnd" value="1"> </td>			
						<td align="right"><br />Step <input class="input2" type="Text" size="2" onkeydown="return IsKeyDigit();" name="QtyStep" value="1"></td>
					</tr>
					</table>
				</td>								
			</tr>
			<tr>
				<td>
					<table border="0" cellpadding="3">
					<tr>						
						<td>
							Payment type<br />
							<%
							    htmlInputs.PutRecordsetComboDefault("SELECT * FROM List.TransCreditType WHERE " & IIf(langCode = 0, "IsShow=1 AND TransCreditType_id<>0", "TransCreditType_id=1") & " ORDER BY ShowOrder", "CreditType", " onchange=""ShowPayments();"" class=""input2"" dir=""ltr""", "TransCreditType_id", "Name", "", "")
							%>
							<script type="text/javascript" language="javascript" defer="defer">
								document.getElementById("CreditType").selectedIndex=1;
							</script>
						</td>
						<td>
							Payments<br /> <%=drawPaymentsCombo("payments", 12)%>
						</td>
						<td>
							Amount<br />
							<input class="input2" type="Text" size="8" style="font-size:12px;" onkeydown="return IsKeyDigit();" name="Amount" value="">
						</td>
						<td>
							Currency<br />
							<asp:Label ID="lblCurrency" runat="server" />
						</td>
						<td align="right" width="20%">
							<br /><input type="submit" name="Action" value=" Add " class="button1" style="font-size:11px;" onclick="return CheckForm();"><br />
						</td>
					</tr>
					</table>
				</td>		
			</tr>
			<tr>
				<td style="padding-top:8px;">
					* Quantity options list is defined by 3 fields: &nbsp; The Start field defines the list starting number,
					The End field defines the list ending number, The Step field defines the step between each number in the list.<br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td colspan="2" class="SecHeading"><br />Payment option added:<br /></td></tr>
	<tr><td height="10"></td></tr>
		<tr>
			<td colspan="2">
				<table width="100%" border="0" cellspacing="2" cellpadding="1">
					<asp:Repeater runat="server" ID="lstPrd" OnItemDataBound="Product_OnItemDataBound" >
						<ItemTemplate>
							<tr height="40">
								<td bgcolor="#f5f5f5"><%# Container.DataItem("Priority")%></td>
								<td bgcolor="#f5f5f5"><input type="Radio" <%# IIF(Container.DataItem("isDefaultCheck"), "checked", "")%> name="IsDefault" value="<%# Container.DataItem("ID") %>" /></td>
								<td bgcolor="#f5f5f5">
									<span><%# Container.DataItem("Text")%>&nbsp;-&nbsp;</span>
									<%# CType(Container.DataItem("Amount"), Decimal).ToAmountFormat(WebUtils.DomainHost, CType(Container.DataItem("Currency"), Integer))%>
									&nbsp;&nbsp;<%# drawQuantityCombo(Container.DataItem("QtyStart"), Container.DataItem("QtyEnd"), Container.DataItem("QtyStep"))%>
									in <%# drawPaymentsCombo("itemPayments", IIF(Container.DataItem("Payments") Is DBNull.Value, 1, Container.DataItem("Payments"))) %>&nbsp;&nbsp;
									<%# IIf(Container.DataItem("CreditType") = 6, "(Special credit)&nbsp;", IIf(Container.DataItem("CreditType") = 2, "(Credit)&nbsp;", ""))%>
								</td>
								<!--
								<td bgcolor="#f5f5f5" valign="middle" align="center">
									<asp:MultiView runat="server" ID="ImageView" ActiveViewIndex='<%# IIF(System.IO.File.Exists(Server.MapPath("/GlobalData/PublicPageLogos/" & Container.DataItem("ImageFileName"))), 0, 1) %>' >
										<asp:View ID="View1" runat="server">
											<img src="/GlobalData/PublicPageLogos/<%# Trim(NullIfDBNull(Container.DataItem("ImageFileName"))) %>" width="30" height="30" onmouseover="showImg(<%# Container.DataItem("ID") %>)" onmouseout="hideImg(<%# Container.DataItem("ID") %>)" />
											<div id="imgDiv<%# Container.DataItem("ID") %>" name="imgDiv<%# Container.DataItem("ID") %>" style="display:none; position:absolute;">
												<img src="/GlobalData/PublicPageLogos/<%# Trim(NullIfDBNull(Container.DataItem("ImageFileName")))%>" border="1" />
											</div>
											<br />
											<a href="?Action=RemovePic&ID=<%#Container.DataItem("ID")%>&lang=<%=Request("lang")%>" class="faq" style="font-size:10px;" onclick="if(!confirm('Are you sure ?')) return false;"> Remove </a>
										</asp:View>
										<asp:View ID="View2" runat="server">
											<a href="javascript:openUploadWin(<%#Container.DataItem("ID")%>);" class="faq" style="font-size:10px;">Add image</a>
										</asp:View>
									</asp:MultiView>
								</td>				
								-->				
								<td bgcolor="#f5f5f5" valign="middle" align="center">
									<asp:Image runat="server" ID="imgQrCode" />
								</td>
								<td bgcolor="#f5f5f5" width="7%" align="center">
									<a href="?Action=x&ID=<%#Container.DataItem("ID")%>&lang=<%=Request("lang")%>" class="button1" style="font-size:11px;" onclick="if(!confirm('Are you sure ?')) return false;">Delete</a><br />
								</td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</table>
				<input type="submit" name="Action" value="Update" />
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td>
						In this page you can define payment options for clients that enter Public Pay page.<br />
						The client will be presented with payment options, from which he will have to choose one, without the possibility of specifying amount and payment type.<br /><br />
						Here is an example:<br />
					</td>
				</tr>
				<tr>
					<td>
						<input type="radio" value="1" name="IDRSelectd"> <span>Service & insurance fees for 6 month - </span> <span>$ 150.00 in 1</span> payment<br />
						<input type="radio" value="1" name="IDRSelectd" checked> <span>Service & insurance fees for 12 month - </span> <span>$ 250.00 in 1</span> payment<br />
					</td>
				</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:Content>