<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" EnableEventValidation="false" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Protected Sub Page_Load()
		If Not Page.IsPostBack Then
			cblCurrencies.DataSource = WebUtils.DomainCache.Currencies.Values
			cblCurrencies.DataTextField = "IsoCode"
			cblCurrencies.DataValueField = "ID"
			cblCurrencies.DataBind()
			
			Dim ppv As PublicPayOptionsVO = Netpay.Bll.PublicPayment.GetOptions(WebUtils.CredentialsToken)
			If ppv Is Nothing Then ppv = New PublicPayOptionsVO()
			Dim currencyOptions As String = ppv.CurrencyOptions
			If String.IsNullOrEmpty(currencyOptions) Then currencyOptions = "-1"
			For i As Integer = 0 To cblCurrencies.Items.Count - 1
				cblCurrencies.Items(i).Selected = (currencyOptions.IndexOf(cblCurrencies.Items(i).Value) > -1)
			Next
		End If
	End Sub

	Protected Sub btnUpdateCurrencies_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim sChecked As String = String.Empty
		For Each liItem As ListItem In cblCurrencies.Items
			If liItem.Selected Then sChecked &= "," & liItem.Value
		Next
		If sChecked = String.Empty Then
			lblUpdateCurrencies.Text = "Select at least one currency!"
		Else
			lblUpdateCurrencies.Text = ""
			Dim ppv As PublicPayOptionsVO = Netpay.Bll.PublicPayment.GetOptions(WebUtils.CredentialsToken)
			If ppv Is Nothing Then ppv = New PublicPayOptionsVO()
			ppv.CurrencyOptions = sChecked.Substring(1)
			Netpay.Bll.PublicPayment.SaveOptions(WebUtils.CredentialsToken, ppv)
		End If
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<script type="text/javascript">
		function checkForm(frmData)
		{
			var bChecked=false;
			for (var i=0;i<frmData.elements.length;i++) if (frmData.elements[i].type=="checkbox") bChecked|=frmData.elements[i].checked;
			if (bChecked) return true;
			alert("Please select currency!");
			return false;
		}
	</script>
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
		<tr>
			<td>
				<span class="PageHeading">PUBLIC PAYMENT PAGE</span>
				<span class="PageSubHeading">- PAYMENT OPTIONS</span><br />
			</td>
			<td align="right">[<a class="faq" href="PublicPageV2_AdminAppear.aspx">back to page management</a>]<br /></td>
		</tr>
		<tr>
			<td colspan="2">
				<custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" />
			</td>
		</tr>
		<tr>
			<td>
				<br />
				<br />
			</td>
		</tr>
		<tr><td colspan="2" class="SecHeading">Debit currency selection<br /></td></tr>
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<asp:CheckBoxList ID="cblCurrencies" runat="server" />
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<asp:Button ID="btnUpdateCurrencies" runat="server" Text="Update" CssClass="button1" OnClick="btnUpdateCurrencies_Click" OnClientClick="return checkForm(this.form);" />
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<asp:Label ID="lblUpdateCurrencies" runat="server" ForeColor="maroon" />
			</td>
		</tr>
	</table>
</asp:Content>