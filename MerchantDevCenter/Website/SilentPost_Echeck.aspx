<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - ACH / ECHECK - SILENT POST" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
    Dim sBrandName As String = CurrentDomain.BrandName
    Dim sProcessURL As String = CurrentDomain.ProcessURL
    Dim sBgColor As String = ""
    Dim IsRemoteChargeEcheck, IsRemoteChargePersonalNumber, IsRemoteChargeCVV2, IsRemoteChargePhoneNumber, IsRemoteChargeEmail As Boolean
    Dim IsApprovalOnly, IsBillingAddressMust, IsAllowRecurring, IsUseFraudDetectionMaxMind As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If CurrentDomain.IsHebrewVisible Then
            lblStates1.Text = "|&nbsp; <a href=""List_IsoCodes.aspx?lang=he"" style=""font-size:10px;"">Hebrew List</a>"
            lblCountries1.Text = "|&nbsp; <a href=""List_IsoCodes.aspx?lang=he"" style=""font-size:10px;"">Hebrew List</a>"
        Else
            lblStates1.Text = ""
            lblCountries1.Text = ""
        End If
        rptCurrency.DataBind()
        If WebUtils.IsLoggedin Then
            lblBillingAddress1.Text = IIf(IsBillingAddressMust, "Yes", "Optional")
            lblBillingCity.Text = IIf(IsBillingAddressMust, "Yes", "Optional")
            lblBillingZipCode.Text = IIf(IsBillingAddressMust, "Yes", "Optional")
            lblBillingState.Text = IIf(IsBillingAddressMust, "Yes", "Optional")
            lblBillingCountry.Text = IIf(IsBillingAddressMust, "Yes", "Optional")
            lblIsRemoteChargeEmail.Text = IIf(IsRemoteChargeEmail, "Yes", "Optional")
            lblIsRemoteChargePersonalNumber.Text = IIf(IsRemoteChargePersonalNumber, "Yes", "Optional")
            lblIsRemoteChargePhoneNumber.Text = IIf(IsRemoteChargePhoneNumber, "Yes", "Optional")
        Else
            lblBillingAddress1.Text = "Not Applicable"
            lblBillingCity.Text = "Not Applicable"
            lblBillingZipCode.Text = "Not Applicable"
            lblBillingState.Text = "Not Applicable"
            lblBillingCountry.Text = "Not Applicable"
            lblIsRemoteChargeEmail.Text = "Not Applicable"
            lblIsRemoteChargePersonalNumber.Text = "Not Applicable"
            lblIsRemoteChargePhoneNumber.Text = "Not Applicable"
        End If
    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">ACH / ECHECK</span>
			<span class="PageSubHeading">- SILENT POST</span><br />
			<custom:AuthAlert PropertyToCheck="RemoteChargeEcheck" runat="server" />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">PAGE URL<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td><%=sProcessURL %>remoteCharge_echeck.asp<br /></td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Request Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
				<th width="46">Max<br />Length<br /></th>
				<th width="50">Type<br/></th>
				<th width="80" valign="bottom">Required <sup>1</sup><br /></th>
			</tr>
			<tr>
				<td>CompanyNum<br/></td>
				<td>Your company number you received from us<br/></td>
				<td>7</td>
				<td>String</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>AccountName<br/></td>
				<td>Full bank account name<br/></td>
				<td>50</td>
				<td>String</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>RoutingNumber<br/></td>
				<td>ABA / Routing number / Bank code<br/></td>
				<td>20</td>
				<td>Integer</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>AccountNumber<br/></td>
				<td>Account number<br/></td>
				<td>20</td>
				<td>String</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>BankAccountType<br/></td>
				<td>Account Type: 1 = Checking, 2 = Savings<br/></td>
				<td>1</td>
				<td>Integer</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>BankName<br/></td>
				<td>Name of the bank<br/></td>
				<td>50</td>
				<td>String</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>BankCity<br/></td>
				<td>City of the bank<br/></td>
				<td>15</td>
				<td>String</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>BankPhone<br/></td>
				<td>Phone number of the bank<br/></td>
				<td>15</td>
				<td>String</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>BankState<br/></td>
				<td>2 Char Iso Code</td>
				<td>2</td>
				<td>String</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>BankCountry<br/></td>
				<td>2 Char Iso Code</td>
				<td>2</td>
				<td>String</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Amount<br/></td>
				<td>Transaction amount<br/></td>
				<td>8</td>
				<td>Double</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Currency<br/></td>
				<td>
					<asp:Repeater ID="rptCurrency" DataSource="<%# dbPages.Currencies  %>" runat="server">
						<ItemTemplate>
							<asp:Literal runat="server" Text='<%#Eval("ID")%>' /> = <asp:Literal runat="server" Text='<%#Eval("IsoCode")%>' />
							(<asp:Literal runat="server" Text='<%#Eval("Name")%>' />)
						</ItemTemplate>
						<SeparatorTemplate><br /></SeparatorTemplate>
					</asp:Repeater>
				</td>
				<td>1</td>
				<td>Integer</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>CreditType <sup>2</sup><br/></td>
				<td>1 = Debit<br/>0 = Refund<br/></td>
				<td>1</td>
				<td>Integer</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>BirthDate<br/></td>
				<td>Birth date of the customer formated as yyyy/mm/dd<br/></td>
				<td>10</td>
				<td>Date</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>BillingAddress1<br /></td>
				<td>1st Address line<br /></td>
				<td>100</td>
				<td>String</td>
				<td><asp:Label ID="lblBillingAddress1" runat="server" /></td>
			</tr>
			<tr>
				<td>BillingAddress2<br /></td>
				<td>2nd Address line, Ok to leave out<br /></td>
				<td>100</td>
				<td>String</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>BillingCity<br /></td>
				<td>City name<br /></td>
				<td>60</td>
				<td>String</td>
				<td><asp:Label ID="lblBillingCity" runat="server" /></td>
			</tr>
			<tr>
				<td>BillingZipCode<br /></td>
				<td>Zip Code number<br /></td>
				<td>15</td>
				<td>String</td>
				<td><asp:Label ID="lblBillingZipCode" runat="server" /></td>
			</tr>
			<tr>
				<td>BillingState<br /></td>
				<td>
					State - 2 character iso format<br />
					Required only when BillingCountry is US/CA.<br />
					<a href="List_IsoCodes.aspx" class="faq" style="font-size:10px;">English list</a>
					&nbsp;<asp:Label ID="lblStates1" runat="server" />
				</td>
				<td>5</td>
				<td>String</td>
				<td><asp:Label ID="lblBillingState" runat="server" /></td>
			</tr>
			<tr>
				<td>BillingCountry<br /></td>
				<td>
					Country - 2 character iso format<br />
					<a href="List_IsoCodes.aspx" class="faq" style="font-size:10px;">English list</a>
					&nbsp;<asp:Label ID="lblCountries1" runat="server" />
				</td>
				<td>5</td>
				<td>String</td>
				<td><asp:label ID="lblBillingCountry" runat="server" /></td>
			</tr>
			<tr>
				<td>Email<br/></td>
				<td>Account owner email<br/></td>
				<td>50</td>
				<td>String</td>
				<td><asp:Label ID="lblIsRemoteChargeEmail" runat="server" /></td>
			</tr>
			<tr>
				<td>PersonalNum<br /></td>
				<td>Social security number<br /></td>
				<td>20</td>
				<td>String</td>
				<td><asp:Label ID="lblIsRemoteChargePersonalNumber" runat="server" /></td>
			</tr>
			<tr>
				<td>PhoneNumber<br/></td>
				<td>Account owner phone number<br/></td>
				<td>20</td>
				<td>String</td>
				<td><asp:Label ID="lblIsRemoteChargePhoneNumber" runat="server" /></td>
			</tr>
			<tr>
				<td>Order<br/></td>
				<td>Transaction unique id, which is sent back with the reply<br/></td>
				<td>100</td>
				<td>String</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>Comment<br/></td>
				<td>Client memo<br/></td>
				<td>500</td>
				<td>String</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>RefTransID<br /></td>
				<td>
					Reference transaction ID. Used to specify the original transaction id 
					when TypeCredit = 0 or TypeCredit = 4 or when needed to complete missing transaction data.<br />
				</td>
				<td>10</td>
				<td>Integer</td>
				<td>Optional</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Response Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td>replyCode<br /></td>
				<td>Transaction authorization (see list below)<br /></td>
			</tr>
			<tr>
				<td>replyMessage<br /></td>
				<td>Reply message in English<br /></td>
			</tr>
			<tr>
				<td>transId<br /></td>
				<td>Transaction number<br /></td>
			</tr>
			<tr>
				<td>transDate<br /></td>
				<td>Transaction time and date<br /></td>
			</tr>
			<tr>
				<td>transAmount<br /></td>
				<td>Transaction amount<br /></td>
			</tr>
			<tr>
				<td>transCurrency<br /></td>
				<td>Transaction currency<br /></td>
			</tr>
			<tr>
				<td>transOrder<br /></td>
				<td>Transaction id or order number that was supplied by merchant<br /></td>
			</tr>
			<tr>
				<td>transComment<br /></td>
				<td>Comment as it was sent by the merchant<br /></td>
			</tr>
			<tr>
				<td>confirmationNumber<br /></td>
				<td>Confirmation number<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					replyCode=000&replyMessage=SUCCESS&transId=12345&transDate=01/01/2000 00:00:01
					&transAmount=10.99&transCurrency=1&transOrder=T1234&transComment=White box&confirmationNumber=0123456
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Reply Codes<br /></td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="1">
			<tr>
				<td><span>000</span> = Authorized transaction<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><span>001</span> = Transaction accepted, waiting authorization<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><span>xxx</span> = Transaction was not Authorized, see unauthorized comment explanation<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td>
			- For a more detail list of reply codes <a href="List_ReplyCodes.aspx">click here</a> 
			<a href="List_ReplyCodes.aspx"><img src="/NPCommon/Images/iconNewWinRight.GIF" border="0" align="middle" /></a><br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Consumer Consent<br/></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			Merchant must add this following consumer consent for the transaction and rules and regulations
			at the page where the consumer is entering his debit information
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td class="SecExpandTxt">
					&lt;input type="Checkbox" name="IsEcheckAgree" value="1"&gt; I here by authorize <asp:Label ID="lblCompany1" runat="server" /> to charge the above amount using my bank account details provided above.
					I also give my consent to the &lt;a href="javascript:var winShow=window.open('<asp:Label ID="lblContentURL" runat="server" />rulesPop.asp', 'fraRules', 'TOP=0, LEFT=0, WIDTH=400, HEIGHT=300, scrollbars=yes');"&gt;rules &amp; regulations&lt;/a&gt; presented.
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<br /><hr size="1" width="100%" noshade="noshade" />
			(1) Some field requirements are dependent on merchant configuration and can be change on your request<br />
			(2) First option is default when field is empty<br />
		</td>
	</tr>
	</table>
	<br />
		
</asp:Content>

