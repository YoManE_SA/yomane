﻿<%@ Page Language="VB" Title="DEVELOPER CENTER - INVOICES" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Register Namespace="Netpay.Web.Controls" Assembly="Netpay.Web" TagPrefix="netpay" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Protected Sub SaveForm(ByVal o As Object, ByVal e As EventArgs)
        Dim sSQL As String = "UPDATE [Setting].[SetMerchantInvoice] SET" & _
        " IsCreateInvoice=" & IIf(chkCreateInvoice.Checked, 1, 0) & _
        ",IsCreateReceipt=" & IIf(chkCreateReceipt.Checked, 1, 0) & _
        ",IsAutoGenerateILS=" & IIf(chkAutoGenerateILS.Checked, 1, 0) & _
        ",IsAutoGenerateOther=" & IIf(chkAutoGenerateOther.Checked, 1, 0) & _
        ",IsAutoGenerateRefund=" & IIf(chkAutoGenerateRefund.Checked, 1, 0) & _
        ",IsIncludeTax=" & IIf(chkIncludeTax.Checked, 1, 0) & _
        ",ItemText='" & txtItemText.Text.ToSql() & "'" & _
        " WHERE Merchant_id=" & Merchant.ID
		dbPages.ExecSql(sSQL)
	End Sub

    Private Sub LoadData()
        If Not WebUtils.IsLoggedin then Exit Sub
        
        Dim bImageExist As Boolean = False
        Dim iReader As SqlDataReader = dbPages.ExecReader("SELECT * FROM [Setting].[SetMerchantInvoice] WHERE Merchant_id=" & Merchant.ID)
        If iReader.Read() Then
            chkCreateInvoice.Checked = iReader("IsCreateInvoice")
            chkCreateReceipt.Checked = iReader("IsCreateReceipt")
            chkAutoGenerateILS.Checked = iReader("IsAutoGenerateILS")
            chkAutoGenerateOther.Checked = iReader("IsAutoGenerateOther")
            chkAutoGenerateRefund.Checked = iReader("IsAutoGenerateRefund")
            chkIncludeTax.Checked = iReader("IsIncludeTax")
            If iReader("ItemText") IsNot DBNull.Value Then txtItemText.Text = iReader("ItemText")
        End If
        iReader.Close()
    End Sub

	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		MyBase.OnLoad(e)
		If Not IsPostBack Then
			If Not Authorization.IsAllowed(AuthorizationProperty.InvoiceEnabled) Then
                chkCreateInvoice.Enabled = False
                chkCreateReceipt.Enabled = False
                chkAutoGenerateILS.Enabled = False
                chkAutoGenerateOther.Enabled = False
                chkIncludeTax.Enabled = False
				txtItemText.Enabled = False
				btnSave.Enabled = False
			End If
			LoadData()
		End If	
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	 <tr>
		 <td>
			<span class="PageHeading">EXTERNAL INVOICES</span><br />
			<custom:AuthAlert PropertyToCheck="InvoiceEnabled" runat="server" />
		 </td>
	</tr>
	<tr>
		<td>
			<br /><br />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<tr><td class="SecHeading" colspan="2">CREATION<br /><br /></td></tr>	
			<tr><td valign="top"><asp:CheckBox runat="server" ID="chkCreateInvoice" /> Create Invoice<br /></td></tr>
			<tr><td valign="top"><asp:CheckBox runat="server" ID="chkCreateReceipt" /> Create Receipt<br /></td></tr>
			<tr><td><br /><br /></td></tr>
			<tr><td class="SecHeading" colspan="2">MANAGEMENT<br /><br /></td></tr>	
			<tr><td valign="top"><asp:CheckBox runat="server" ID="chkIncludeTax" />Include tax<br /><br /></td></tr>
			<tr><td valign="top">Invoice Item Text<br /><asp:TextBox runat="server" ID="txtItemText" MaxLength="80" /></td></tr>
			<tr><td><br /><br /></td></tr>
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr><td class="SecHeading" colspan="2">AUTO GENERATE INVOICES<br /><br /></td></tr>	
			<tr><td valign="top"><asp:CheckBox runat="server" ID="chkAutoGenerateILS" /> On ILS transactions<br /></td></tr>
			<tr><td valign="top"><asp:CheckBox runat="server" ID="chkAutoGenerateOther" /> On other Currency transactions (the system first convert the amount to ILS)<br /></td></tr>
			<tr><td valign="top"><asp:CheckBox runat="server" ID="chkAutoGenerateRefund" /> On refunds<br /></td></tr>
            <tr><td><br /></td></tr>
			<tr>
				<td>
					<br/>
					<asp:Button ID="btnSave" runat="server" OnClick="SaveForm" Text="Update" CssClass="submit" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
</asp:Content>
