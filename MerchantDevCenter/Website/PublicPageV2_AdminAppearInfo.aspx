<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" %>

<%@ Import Namespace="Netpay.CommonTypes" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<script runat="server">
		Dim lang As Language = Language.English
		Dim styleDir As String = ""
		
		Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
			lang = dbPages.TestVar(Request("lang"), 0, -1, 0)
			styleDir = IIf(lang = Language.Hebrew, "rtl", "ltr")
			If Not IsPostBack Then LoadData()
		End Sub
		
		Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
			Merchants.SetTranslation(WebUtils.CredentialsToken, lang, TranslationGroup.PublicPaymentPage, "Var1Name", Var1Name.Text)
			Merchants.SetTranslation(WebUtils.CredentialsToken, lang, TranslationGroup.PublicPaymentPage, "Var2Name", Var2Name.Text)
			Merchants.SetTranslation(WebUtils.CredentialsToken, lang, TranslationGroup.PublicPaymentPage, "Var3Name", Var3Name.Text)
			Merchants.SetTranslation(WebUtils.CredentialsToken, lang, TranslationGroup.PublicPaymentPage, "Var4Name", Var4Name.Text)
			Dim ppv As PublicPayOptionsVO = PublicPayment.GetOptions(WebUtils.CredentialsToken)
			If ppv Is Nothing Then ppv = New PublicPayOptionsVO()

			ppv.IsVar1 = Var1Show.Checked
			ppv.IsRequiredVar1 = Var1R.Checked

			ppv.IsVar2 = Var2Show.Checked
			ppv.IsRequiredVar2 = Var2R.Checked

			ppv.IsVar3 = Var3Show.Checked
			ppv.IsRequiredVar3 = Var3R.Checked

			ppv.IsVar4 = Var4Show.Checked
			ppv.IsRequiredVar4 = Var4R.Checked

			ppv.IsFullName = FullNameShow.Checked
			ppv.IsRequiredFullName = FullNameRequiredR.Checked
			ppv.IsAddress = AddressShow.Checked
			ppv.IsRequiredAddress = AddressRequiredR.Checked
			ppv.IsPhone = PhoneShow.Checked
			ppv.IsRequiredPhone = PhoneRequiredR.Checked
			ppv.IsCellular = CellularShow.Checked
			ppv.IsRequiredCellular = CellularRequiredR.Checked
			ppv.IsEmail = EmailShow.Checked
			ppv.IsRequiredEmail = EmailRequiredR.Checked
			ppv.IsHowGetHere = HowGetHereShow.Checked
			ppv.IsRequiredHowGetHere = HowGetHereRequiredR.Checked
			PublicPayment.SaveOptions(WebUtils.CredentialsToken, ppv)
		End Sub

		Private Sub LoadData()
			Dim nTrans = Merchants.GetTranslations(WebUtils.CredentialsToken, lang, TranslationGroup.PublicPaymentPage)
			Dim ppv As PublicPayOptionsVO = PublicPayment.GetOptions(WebUtils.CredentialsToken)
			If ppv Is Nothing Then ppv = New PublicPayOptionsVO()
			FullNameShow.Checked = ppv.IsFullName
			IIf(ppv.IsRequiredFullName, FullNameRequiredR, FullNameRequiredO).Checked = True
			AddressShow.Checked = ppv.IsAddress
			IIf(ppv.IsRequiredAddress, AddressRequiredR, AddressRequiredO).Checked = True
			PhoneShow.Checked = ppv.IsPhone
			IIf(ppv.IsRequiredPhone, PhoneRequiredR, PhoneRequiredO).Checked = True
			CellularShow.Checked = ppv.IsCellular
			IIf(ppv.IsRequiredCellular, CellularRequiredR, CellularRequiredO).Checked = True
			EmailShow.Checked = ppv.IsEmail
			IIf(ppv.IsRequiredEmail, EmailRequiredR, EmailRequiredO).Checked = True
			HowGetHereShow.Checked = ppv.IsHowGetHere
			IIf(ppv.IsRequiredHowGetHere, HowGetHereRequiredR, HowGetHereRequiredO).Checked = True

			Var1Show.Checked = ppv.IsVar1
			Var1Name.Text = nTrans.EmptyIfNull("Var1Name")
			IIf(ppv.IsRequiredVar1, Var1R, Var1O).Checked = True

			Var2Show.Checked = ppv.IsVar2
			Var2Name.Text = nTrans.EmptyIfNull("Var2Name")
			IIf(ppv.IsRequiredVar2, Var2R, Var2O).Checked = True

			Var3Show.Checked = ppv.IsVar3
			Var3Name.Text = nTrans.EmptyIfNull("Var3Name")
			IIf(ppv.IsRequiredVar3, Var3R, Var3O).Checked = True

			Var4Show.Checked = ppv.IsVar4
			Var4Name.Text = nTrans.EmptyIfNull("Var4Name")
			IIf(ppv.IsRequiredVar4, Var4R, Var4O).Checked = True
		End Sub
	</script>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td>
			<span class="PageHeading">PUBLIC PAYMENT PAGE</span>
			<span class="PageSubHeading">- MANAGE APPEARANCE (<%=lang.ToString()%>)</span><br />
		</td>
		<td align="right">[<a class="faq" href="PublicPageV2_AdminAppear.aspx?lang=<%= lang %>">back to page management</a>]<br /></td>
	</tr>
	<tr>
		<td colspan="2">
			<custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td colspan="2">Mark the fields you wish to include in the payment form<br /></td></tr>
	<tr><td height="12"></td></tr>
	<tr>
		<td colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="1">
			<tr>
				<td><asp:CheckBox runat="server" ID="FullNameShow" /> Full name<br /></td>
				<td width="26"><br /></td>
				<td><asp:RadioButton ID="FullNameRequiredO" runat="server" GroupName="FullNameRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="FullNameRequiredR" runat="server" GroupName="FullNameRequired" class="input2" /> Required filed</td>
			</tr>
			<tr>
				<td><asp:CheckBox runat="server" ID="AddressShow" /> Address<br /></td>
				<td><br /></td>
				<td><asp:RadioButton ID="AddressRequiredO" runat="server" GroupName="AddressRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="AddressRequiredR" runat="server" GroupName="AddressRequired" class="input2" /> Required filed</td>
			</tr>
			<tr>
				<td><asp:CheckBox runat="server" ID="PhoneShow" /> Phone<br /></td>
				<td><br /></td>
				<td><asp:RadioButton ID="PhoneRequiredO" runat="server" GroupName="PhoneRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="PhoneRequiredR" runat="server" GroupName="PhoneRequired" class="input2" /> Required filed</td>
			</tr>
			<tr>
				<td><asp:CheckBox runat="server" ID="CellularShow" /> Cell phone<br /></td>
				<td><br /></td>
				<td><asp:RadioButton ID="CellularRequiredO" runat="server" GroupName="CellularRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="CellularRequiredR" runat="server" GroupName="CellularRequired" class="input2" /> Required filed</td>
			</tr>
			<tr>
				<td><asp:CheckBox runat="server" ID="EmailShow" /> Email<br /></td>
				<td><br /></td>
				<td><asp:RadioButton ID="EmailRequiredO" runat="server" GroupName="EmailRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="EmailRequiredR" runat="server" GroupName="EmailRequired" class="input2" /> Required filed</td>
			</tr>
			<tr>
				<td><asp:CheckBox runat="server" ID="HowGetHereShow" /> How did you arrive here<br /></td>
				<td><br /></td>
				<td><asp:RadioButton ID="HowGetHereRequiredO" runat="server" GroupName="HowGetHereRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="HowGetHereRequiredR" runat="server" GroupName="HowGetHereRequired" class="input2" /> Required filed</td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td>
					<asp:CheckBox runat="server" ID="Var1Show" />
					&nbsp;<asp:TextBox runat="server" MaxLength="100" ID="Var1Name" size="20" dir="<%= sStyleDir %>" class="inputData" /><br />
				</td>
				<td><br /></td>
				<td><asp:RadioButton ID="Var1O" runat="server" GroupName="Var1" class="input2" /> Optional field </td>
				<td><asp:RadioButton ID="Var1R" runat="server" GroupName="Var1" class="input2" /> Required field </td>
			</tr>
			<tr>
				<td>
					<asp:CheckBox runat="server" ID="Var2Show" />
					&nbsp;<asp:TextBox runat="server" MaxLength="100" ID="Var2Name" size="20" dir="<%= sStyleDir %>" class="inputData" /><br />
				</td>
				<td><br /></td>
				<td><asp:RadioButton ID="Var2O" runat="server" GroupName="Var2" class="input2" /> Optional field </td>
				<td><asp:RadioButton ID="Var2R" runat="server" GroupName="Var2" class="input2" /> Required field </td>
			</tr>
			<tr>
				<td>
					<asp:CheckBox runat="server" ID="Var3Show" />
					&nbsp;<asp:TextBox runat="server" MaxLength="100" ID="Var3Name" size="20" dir="<%= sStyleDir %>" class="inputData" /><br />
				</td>
				<td><br /></td>
				<td><asp:RadioButton ID="Var3O" runat="server" GroupName="Var3" class="input2" /> Optional field </td>
				<td><asp:RadioButton ID="Var3R" runat="server" GroupName="Var3" class="input2" /> Required field </td>
			</tr>
			<tr>
				<td>
					<asp:CheckBox runat="server" ID="Var4Show" />
					&nbsp;<asp:TextBox runat="server" MaxLength="100" ID="Var4Name" size="20" dir="<%= sStyleDir %>" class="inputData" /><br />
				</td>
				<td><br /></td>
				<td><asp:RadioButton ID="Var4O" runat="server" GroupName="Var4" class="input2" /> Optional field </td>
				<td><asp:RadioButton ID="Var4R" runat="server" GroupName="Var4" class="input2" /> Required field </td>
			</tr>
			<tr>
				<td colspan="4" align="right">
					<br /><asp:Button runat="server" UseSubmitBehavior="true" Text=" Update " class="submit" OnCommand="btnUpdate_Click" /><br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>	
</asp:Content>