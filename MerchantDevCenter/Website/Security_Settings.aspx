<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - SECURITY SETTINGS" EnableEventValidation="false" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Protected Sub SetHashKeyVisibilty(ByVal sHashKey As String)
		If String.IsNullOrEmpty(sHashKey) Then
			lblHashKey.Text = "Your have no Personal Hash Key generated."
			btnHashKeyChange.Visible = False
			btnHashKeyDestroy.Visible = False
			btnHashKeyCreate.Visible = True
		Else
			lblHashKey.Text = "Your Personal Hash Key is <b>" & sHashKey & "</b>"
			btnHashKeyChange.Visible = True
			btnHashKeyDestroy.Visible = True
			btnHashKeyCreate.Visible = False
		End If
	End Sub

	Protected Sub DestroyHashKey(ByVal o As Object, ByVal e As EventArgs)
		dbPages.ExecSql("UPDATE tblCompany SET HashKey='' WHERE ID=" & Merchant.ID)
		dbPages.ExecSql("UPDATE tblCompanyChargeAdmin SET UseHasKeyInWalletReply=0 WHERE company_id=" & Merchant.ID)
		SetHashKeyVisibilty(Nothing)
	End Sub

	Protected Sub CreateHashKey(ByVal o As Object, ByVal e As EventArgs)
		'Dim hashKey As String = dbPages.ExecScalar("EXEC GetNewHashKey")
		Dim hashKey As String = Netpay.Infrastructure.Security.Encryption.GetHashKey()
		dbPages.ExecSql("UPDATE tblCompany SET HashKey='" & hashKey & "' WHERE ID=" & Merchant.ID)
		SetHashKeyVisibilty(hashKey)
	End Sub

	Protected Sub btnSave_Save(ByVal o As Object, ByVal e As EventArgs)
		dbPages.ExecSql("UPDATE tblCompany SET ForceCCStorageMD5=" & IIf(chkForceCCStorageMD5.Checked, 1, 0) & " WHERE ID=" & Merchant.ID)
		dbPages.ExecSql("UPDATE tblMerchantRecurringSettings SET ForceMD5OnModify=" & IIf(chkRecurringForceMD5OnModify.Checked, 1, 0) & " WHERE MerchantID=" &Merchant.ID)
	End Sub
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		If Not Page.IsPostBack Then
			Page.Title = "DEVELOPER CENTER - SECURTY SETTINGS"
			Dim sSQL As String = "SELECT m.HashKey, m.ForceCCStorageMD5, s.ForceMD5OnModify FROM tblCompany m LEFT JOIN tblMerchantRecurringSettings s ON m.ID=s.MerchantID WHERE m.ID=" & Merchant.ID
			Dim iReader As System.Data.IDataReader = dbPages.ExecReader(sSQL)
			If iReader.Read() Then
				chkForceCCStorageMD5.Checked = IIf(IsDBNull(iReader("ForceCCStorageMD5")), False, iReader("ForceCCStorageMD5"))
				SetHashKeyVisibilty(iReader("HashKey"))
			End If
			pnlRecurring.Visible = Not IsDBNull(iReader("ForceMD5OnModify"))
			chkRecurringForceMD5OnModify.Checked = IIf(IsDBNull(iReader("ForceMD5OnModify")), False, iReader("ForceMD5OnModify"))
			iReader.Close()
		End If
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<span class="PageHeading">SECURITY SETTINGS (INTEGRATION)</span>
			<span class="PageSubHeading"></span><br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Personal Hash Key<br /><br /></td>
	</tr>
	<tr>
		<td>
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr>
				<!-- <td valign="top" style="color:#003366;">Receiving reply<br /></td> -->
				<td valign="top">
					<asp:Label runat="server" ID="lblHashKey" />
					<br /><br />
					<script language="javascript" type="text/javascript">
						function ConfirmHashKeyAction(sAction)
						{
							var sText="Personal Hash Key modifications cannot be undone."
							sText+="\n\nDo you really want to "+sAction.toUpperCase()+" your Personal Hash Key ?!"
							return confirm(sText);
						}
					</script>
					<asp:Button ID="btnHashKeyDestroy" OnClick="DestroyHashKey" Text="Destroy Personal Hash Key"
						OnClientClick="if (!ConfirmHashKeyAction('destroy')) return false;" 
						CssClass="submit" UseSubmitBehavior="false" runat="server"/>
					<asp:Button ID="btnHashKeyChange" OnClick="CreateHashKey" Text="Change Personal Hash Key"
						OnClientClick="if (!ConfirmHashKeyAction('change')) return false;" 
						CssClass="submit" UseSubmitBehavior="false" runat="server" />
					<asp:Button ID="btnHashKeyCreate" OnClick="CreateHashKey" Text="Create Personal Hash Key"
						CssClass="submit" UseSubmitBehavior="false" runat="server" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Options<br /><br /></td>
	</tr>
	<tr>
		<td>
			<asp:checkbox id="chkForceCCStorageMD5" runat="server" /> Force MD5 signature when sending credit card storage data
		</td>
	</tr>
	<tr>
		<td>
			<asp:Panel ID="pnlRecurring" runat="server">
				<asp:checkbox id="chkRecurringForceMD5OnModify" runat="server" /> Force MD5 signature when sending recurring modify service
			</asp:Panel>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td>
			<asp:Button runat="server" id="btnSave" OnClick="btnSave_Save" Text=" Save " /> 
		</td>
	</tr>
	</table>
	<br />
</asp:Content>
