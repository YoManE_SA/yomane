<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
    Dim sBgColor as String = ""
    Dim IsCustomerPurchasePayerID, IsApprovalOnly, IsAllowRecurring As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        'dsMethodSource.ConnectionString = dbPages.DSN
        'If CurrentDomain.IsHebrewVisible Then
        rptCurrency.DataBind()

        Dim languages = System.Configuration.ConfigurationManager.AppSettings("HppV2Languages").Split(",")
        Dim cultures As New System.Collections.Generic.List(of System.Globalization.CultureInfo)
        For Each Language In languages
            If Language.ToLower() <> "he-il" Or CurrentDomain.IsHebrewVisible Then _
                cultures.Add(System.Globalization.CultureInfo.GetCultureInfo(Language))
        Next
        rptLanguages.DataSource = cultures
        rptLanguages.DataBind()

        If WebUtils.IsLoggedin Then
            Dim sSQL As String = "SELECT IsCustomerPurchasePayerID, IsApprovalOnly, tblMerchantRecurringSettings.* " &
                " FROM tblCompany Left Join tblMerchantRecurringSettings ON(tblMerchantRecurringSettings.MerchantID = tblCompany.ID)" &
                " WHERE ID = " & Merchant.ID
            Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
            If iReader.Read() Then
                IsCustomerPurchasePayerID = iReader("IsCustomerPurchasePayerID")
                If Not IsCustomerPurchasePayerID Then
                    lblIsCustomerPurchasePayerID.Text = "<span style=""color:maroon;"">This option is not active - Contact support to activate.</span><br />"
                Else
                    lblIsCustomerPurchasePayerID.Visible = False
                End If

                IsApprovalOnly = iReader("IsApprovalOnly")
                If Not IsApprovalOnly Then
                    'lblIsApprovalOnly.Text = "<span style=""color:maroon;"">Authorization only is not available, contact " & WebUtils.CurrentDomain.BrandName & " to activate</span><br />"
                Else
                    'lblIsApprovalOnly.Text = ""
                    'lblIsApprovalOnly.Visible = False
                End If
                IsAllowRecurring = dbPages.TestVar(iReader("IsEnabled"), False)
                If IsAllowRecurring Then
                    litRecurringYears.Text = iReader("MaxYears")
                    litRecurringStages.Text = iReader("MaxStages")
                    litRecurringCharges.Text = iReader("MaxCharges")
                End If
            End If
            iReader.Close()
        Else
            'lblIsApprovalOnly.Text = "<span style=""color:maroon;"">Not Applicable to Guest Login</span><br/>"
            lblIsCustomerPurchasePayerID.Text = "<span style=""color:maroon;"">Not Applicable to Guest Login</span><br/>"
        End If
        If Not Page.IsPostBack Then
            If (Authorization.IsAnyHostedV2Enabled) Then
                Select Case dbPages.TestVar(Request("T"), 0, -1, 1)
                    Case "1"
                        aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2CreditCard
                        aa.formatFeatureMessage("Credit Card Hosted Page")
                    Case "2"
                        aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Echeck
                        aa.formatFeatureMessage("Echeck Hosted Page")
                    Case "3"
                        aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Instant
                        aa.formatFeatureMessage("Alternative payment methods Hosted Page")
                End Select
            Else
                aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2CreditCard
            End If
            litSignatureRequired.Text = IIf(Authorization.IsAllowed(AuthorizationProperty.HostedPaymentV2SignatureOptional), "No", "Yes")
        End If
    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">HOSTED PAYMENT PAGE VER.2</span>
			<span class="PageSubHeading">- MESSAGE FIELDS</span><br />
			<custom:AuthAlert ID="aa" PropertyToCheck="HostedPaymentV2" runat="server" />
		</td>
	</tr>	
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">PAGE URL<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<%=CurrentDomain.ProcessV2URL%>hosted<br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Request Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
				<th width="46">Max<br />Length<br /></th>
				<th width="80" valign="bottom">Required <sup>1</sup><br /></th>
			</tr>
			<tr>
				<td>merchantID<br /></td>
				<td>Your 7 digits merchant number - <%=MerchantNumber%><br /></td>
				<td>7</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>signature<br /></td>
				<td>
					Signature for verifying the authenticity of the request parameters.<br />
					Field values to use: All parameters in the order they appear in the request, first GET then POST + PersonalHashKey.<br /><br />
					more information can be found at the <a style="font-size:90%;" href="#signature">Signature</a> section in this page
				</td>
				<td>30</td>
				<td><asp:Literal ID="litSignatureRequired" Text="Yes" runat="server" /></td>
			</tr>
			<tr>
				<td>trans_amount<br /></td>
				<td>Amount to be charge<br />(example: 199.95)<br /></td>
				<td>20</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>trans_currency<br /></td>
				<td>
					<asp:Repeater ID="rptCurrency" DataSource="<%# dbPages.Currencies  %>" runat="server">
						<ItemTemplate>
							<asp:Literal runat="server" Text='<%#Eval("IsoCode")%>' /> = <asp:Literal runat="server" Text='<%#Eval("Name")%>' />
						</ItemTemplate>
						<SeparatorTemplate><br /></SeparatorTemplate>
					</asp:Repeater>
				</td>
				<td>3</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>trans_type<br /></td>
				<td>
					0 = Debit Transaction (Default when field is empty)<br />
					1 = Authorization only<br />
					<asp:Label ID="lblIsApprovalOnly" runat="server" />
				</td>
				<td>1</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>trans_installments<br /></td>
				<td>Number of installments, 1 for regular transaction<br /></td>
				<td>2</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>trans_refNum<br /></td>
				<td>Unique text used to defer one transaction from another<br /></td>
				<td>100</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>trans_comment<br /></td>
				<td>Optional text used mainly to describe the transaction</td>
				<td>255</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>trans_storePm<br /></td>
				<td>Send value of 1 to save the payment method for future use if the transaction success</td>
				<td>1</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>disp_payFor<br /></td>
				<td>Text shown to buyer in payment window, Usually description of purchase (Cart description, Product name)</td>
				<td>40</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>disp_paymentType<br /></td>
				<td>
					List of payment types that are available to the client.<br />
					Available values are can be found here: <a href="List_PaymentMethods.aspx">Payment Methods</a>.<br />
                    this list is use the abbreviation field from the list.
					If more than one, use comma to separate the values.<br />
					(example: CC,ED)<br />
				</td>
				<td>80</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>disp_lng<br /></td>
				<td>
					The default language for the UI text in the window.<br />
					If omitted, language is taken from user's browser settings.<br />
					<br />
					Available values are:<br />
					<asp:Repeater runat="server" id="rptLanguages">
						<ItemTemplate>
							<span style="font-family:Monospace;"><%# Eval("Name") %></span> = <%# Eval("NativeName") %><br />
						</ItemTemplate>
					</asp:Repeater>
				</td>
				<td>5</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>disp_lngList<br /></td>
				<td>
					Specifies the language(s) available to user in the Language Selector in the window.<br />
					<br />
					Available values are:<br />
					<span style="font-family:Monospace;">all</span> = all languages are available (default if the field is empty or omitted)<br />
					<span style="font-family:Monospace;">hide</span> = the language selector is hidden<br />
					comma-separated list of codes for enabling specific language(s).<br />
					<br />
					For example, <span style="font-family:Monospace;">disp_lngList=en-us,it-it</span>
					will allow user to switch between English and Italian, and <span style="font-family:Monospace;">disp_lngList=en-us</span> will show English only.<br />
					The available languages are listed in the <span style="font-family:Monospace;">disp_lng</span> field description.<br />
 				</td>
				<td>5</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>disp_recurring<br /></td>
				<td>
					Specifies the deviations from the standard display of the recurring charges.<br />
					<br />
					Available values are:<br />
					<span style="font-family:Monospace;">0</span> = standard display (default if the field is empty or omitted)<br />
					<span style="font-family:Monospace;">1</span> = hide the number of charges in the last stage of the recurring series<br />
 				</td>
				<td>3</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>disp_mobile<br /></td>
				<td>
					Specifies that the ui should be sized to small screen.<br />
					<br />
					Available values are:<br />
					<span style="font-family:Monospace;">auto</span> = auto detect if the client device has small screen<br />
					<span style="font-family:Monospace;">true</span> = show small size UI for small screens<br />
					<span style="font-family:Monospace;">false</span> = shows normal UI<br />
 				</td>
				<td>3</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_id<br /></td>
				<td>available soon</td>
				<td>10</td>
				<td>Optional</td>
			</tr>

			<tr>
				<td>client_AutoRegistration<br /></td>
				<td>Send value of 1 to create wallet for the customer if not already exist (match by email) and return the id of the wallet</td>
				<td>1</td>
				<td>Optional</td>
			</tr>

			<!--<tr>
				<td>client_id<br /></td>
				<td>
					<asp:Label ID="lblIsCustomerPurchasePayerID" runat="server" />
					Used for second time buyers where client exist in sellers database and seller wishes to automatically fill the page with payer's credit card information.<br />
				</td>
				<td>10</td>
				<td>Optional</td>
			</tr>-->
			<tr>
				<td>url_notify<br /></td>
				<td>
					The URL for notifying your system with the transaction result.<br />
					Must include <span style="font-family:Monospace;">http://</span> or <span style="font-family:Monospace;">https://</span>.<br />
					This parameter can be configured in <a href="HostedPageV2_Admin.aspx?T=<%= Request("T") %>">Hosted Page V.2</a> under the Management section.
					If specified in the request, overrides the configured value.<br />
				</td>
				<td>255</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>url_redirect<br /></td>
				<td>
					The URL to which the buyer�s browser is redirected to after completing the payment<br />
					Must include <span style="font-family:Monospace;">http://</span> or <span style="font-family:Monospace;">https://</span>.<br />
					This parameter can be configured in <a href="HostedPageV2_Admin.aspx?T=<%= Request("T") %>">Hosted Page V.2</a> under the Management section.
					If specified in the request, overrides the configured value.<br />
				</td>
				<td>255</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>skin_no<br /></td>
				<td>
					The skin number to apply to the window opened by the request.<br />
					The skins can be configured in the <a href="Customize.aspx?T=<%= Request("T") %>">Customization</a> page.<br />
				</td>
				<td>2</td>
				<td>Optional</td>
			</tr>
			</table>
		</td>	
	</tr>
	<tr><td><br /></td></tr>
	<tr><td class="FieldSecHeading">Automatic filling of form data:<br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<td width="110">client_fullName<br /></td>
				<td>Cardholder full name</td>
				<td width="46">50</td>
				<td width="80">Optional</td>
			</tr>
			<tr>
				<td>client_email<br /></td>
				<td>Cardholder Email address</td>
				<td>50</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_phoneNum<br /></td>
				<td>Cardholder phone number</td>
				<td>15</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_idNum<br /></td>
				<td>Government issued ID number</td>
				<td>9</td>
				<td>Optional</td>
			</tr>
			<tr><td colspan="4"><br /></td></tr>
			<tr>
				<td>client_billAddress1<br /></td>
				<td>Client billing address line 1</td>
				<td>50</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_billAddress2<br /></td>
				<td>Client billing address line 2</td>
				<td>50</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_billCity<br /></td>
				<td>Client billing city</td>
				<td>20</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_billZipcode<br /></td>
				<td>Client billing zip code</td>
				<td>20</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_billState<br /></td>
				<td>Client billing state in 2 char ISO code</td>
				<td>2</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>client_billCountry<br /></td>
				<td>Client billing country in 2 char ISO code</td>
				<td>2</td>
				<td>Optional</td>
			</tr>
			</table>
		</td>	
	</tr>
	<tr><td><br /></td></tr>
	<tr><td class="FieldSecHeading">Recurring: &nbsp;&nbsp; <span style="font-weight:normal;"></span><br /></td></tr>
	<tr>
		<td>
			<br />
			See <a href="RecurringInfo.aspx">Recurring information</a> for detailed description and for some useful examples.<br />
			<br />
			Notice: The customer can see the recurring series description containing the charge amount and the number of charges for each stage.
			If the number of charges in the last stage is 99, that number will not appear in the description.<br />
			<br />
			The recurring series is subject to the following configurable limitations:
			<ul class="aboutList">
				<li>Overall duration: <asp:Literal ID="litRecurringYears" Text="3" runat="server" /> years</li>
				<li>Number of stages: <asp:Literal ID="litRecurringStages" Text="4" runat="server" /></li>
				<li>Overall number of charges: <asp:Literal ID="litRecurringCharges" Text="400" runat="server" /></li>
			</ul>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
				<tr>
					<td width="110">trans_recurringType<br /></td>
					<td>
						Specifies transaction type for subsequent transactions.<br />
						Does not affect the initial transaction.<br />
						Accepts the same values as trans_type.<br />
						If omitted, all the subsequent transactions will be of the same type as the initial transaction.
					</td>
					<td width="46">1</td>
					<td width="80">Optional</td>
				</tr>
				<tr>
					<td width="110">trans_recurring1<br /></td>
					<td>Parameter for first stage in the recurring series<br /></td>
					<td width="46">12</td>
					<td width="80">Optional</td>
				</tr>
				<tr>
					<td>trans_recurring2<br /></td>
					<td>Parameter for second stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>trans_recurring3<br /></td>
					<td>Parameter for third stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>trans_recurring4<br /></td>
					<td>Parameter for fourth stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>trans_recurring5<br /></td>
					<td>Parameter for fifth stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>trans_recurring6<br /></td>
					<td>Parameter for sixth stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td><span class="FieldSecHeading">User defined parameters:<br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
				<tr>
					<td>
						You can append your own fields to the request.<br />
						These fields will be stored during the request processing and returned with the reply.<br />
						Use this to transfer more information to your url_redirect and url_notify pages.
					</td>
					<td width="46">2000 <sup>2</sup></td>
					<td width="80">Optional</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Response Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td>replyCode<br /></td>
				<td>Transaction authorization (see list below)<br /></td>
			</tr>
			<tr>
				<td>replyDesc<br /></td>
				<td>Reply description<br /></td>
			</tr>
			<tr>
				<td>trans_id<br /></td>
				<td>Transaction number<br /></td>
			</tr>
			<tr>
				<td>trans_date<br /></td>
				<td>Transaction time and date<br /></td>
			</tr>
			<tr>
				<td>trans_amount<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>trans_currency<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>trans_installments<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>trans_refNum<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>client_id<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>client_wallet_id<br /></td>
				<td >The id of customer's wallet if request includes client_AutoRegistration=1<br /></td>
			</tr>
			<tr>
				<td>storage_id<br /></td>
				<td >A returned storage_id if request includes trans_storePm=1<br /></td>
			</tr>
			<tr>
				<td>paymentDisplay<br /></td>
				<td>
					Description of the payment method used in the transaction.<br />
					For credit cards, the description consists of the card type and the last four digits of the card number.<br />
					For bank transfers, the description consists of the platform name and the last four digits of the account.<br />
				</td>
			</tr>
			<tr>
				<td>signature<br /></td>
				<td >
					Signature for verifying the authenticity of the Response parameters.<br />
					Field value used: <i>replyCode + trans_id + PersonalHashKey</i><br /><br />
					Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
				</td>
			</tr>
			<tr>
				<td>client_fullName</td>
				<td>Cardholder full name <sup>3</sup></td>
			</tr>
			<tr>
				<td>client_phoneNum</td>
				<td>Cardholder phone number <sup>3</sup></td>
			</tr>
			<tr>
				<td>client_email<br /></td>
				<td>Cardholder Email address</td>
			</tr>
			<tr>
				<td>recurringSeries_id</td>
				<td>Recurring series ID number (if recurring series has been created)</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					replyCode=000&replyDesc=SUCCESS&trans_id=123456&trans_date=01/01/2000 00:00:01&trans_amount=10.99<br />
					&trans_currency=USD&trans_installments=1&trans_refNum=A1234567&client_id=1234<br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Reply Codes<br /></td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="1">
			<tr>
				<td><span>000</span> = Authorized transaction<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><span>001</span> = Transaction accepted, waiting authorization<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><span>xxx</span> = Transaction was not Authorized, see unauthorized comment explanation<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td>
			- For a more detail list of reply codes <a href="List_ReplyCodes.aspx">click here</a> 
			<a href="List_ReplyCodes.aspx"><img src="/NPCommon/Images/iconNewWinRight.GIF" border="0" align="middle" /></a><br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading"><a name="signature" style="text-decoration:none;cursor:none;">Signature</a><br /></td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			The goal of the signature is to verify the request comes form the merchant and not from unauthorized party.<br />
			The idea here is to create a hash string that contains the values from the request plus a security value (personal hash key) that is known only to the merchant and us.<br />
			In order to compute this value you should concatenate all the values in the request in the same order they appear in the request (first GET parameters and then POST parameters),
			to this values string you should concatenate the personal hash key (which can be found <a href="Security_Settings.aspx">here</a>).<br />
			Then hash the result using SHA256, encode it with base64 and add a signature parameter with the result to the request 
			(note that if you add the signature parameter to the URL you should also encode the result with URL encoding).<br />
			<br />
			for example:<br />
			assuming merchantID = 1234567, trans_amount = 5.3, trans_currency = ILS, personal hash key = abcd<br />
			then the request string will be: merchantID=1234567&trans_amount=5.3&trans_currency=ILS<br />
			and the values string will be: 12345675.3ILS<br />
			after adding the personal hash key the value string should be: 12345675.3ILSabcd<br />
			base64(SHA256) of it is kXLFnMcwx2e9pVwtPTIAY0X+JkOJlfkbH+sg1CivF2E=<br />
			then you can add to the request a url encoded signature paramter which will make the request looks like:<br />
			<p class="merchantIDlink">merchantID=1234567&trans_amount=5.3&trans_currency=ILS&signature=kXLFnMcwx2e9pVwtPTIAY0X%2BJkOJlfkbH%2Bsg1CivF2E%3D</p>
			For more general information regarding signatures, please refer to: <a href="Signature.aspx">BASIC INFO --> SIGNATURE</a>
			
		</td>
	</tr>
	<tr>
		<td>
			<br /><hr size="1" width="100%" noshade="noshade" />
			(1) Some field requirements are dependent on merchant configuration and can be change on your request<br />
			(2) The overall length of the request is subject to the 2000 byte length restriction<br />
			(3) That field is passed only to Notification URL.<br />
		</td>
	</tr>
	</table>
</asp:Content>