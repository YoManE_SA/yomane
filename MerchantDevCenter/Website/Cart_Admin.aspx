﻿<%@ Page Title="DEVELOPER CENTER - CART ADMIN" Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"   %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Protected Sub SaveForm(ByVal o As Object, ByVal e As EventArgs)
		Dim sSQL As String = "UPDATE tblCompanySettingsCart SET " & _
		 "IsShippingEnabled=" & IIf(chkShippingEnabled.Checked, 1, 0) & "," & _
		 "IsShippingRequired=" & IIf(chkShippingRequired.Checked, 1, 0) & "," & _
		 "IsCartStored=" & IIf(chkStoreCart.Checked, 1, 0) & "," & _
		 "IsAuthOnly=" & IIf(chkIsAuthOnly.Checked, 1, 0) & " WHERE CompanyID=" & Merchant.ID
		If (dbPages.ExecSql(sSQL) = 0) Then
			sSQL = "INSERT INTO tblCompanySettingsCart(IsShippingEnabled, IsShippingRequired, IsCartStored, IsAuthOnly, CompanyID) VALUES(" & _
			 IIf(chkShippingEnabled.Checked, 1, 0) & ", " & IIf(chkShippingRequired.Checked, 1, 0) & ", " & IIf(chkStoreCart.Checked, 1, 0) & ", " & IIf(chkIsAuthOnly.Checked, 1, 0) & ", " & Merchant.ID & ")"
			dbPages.ExecSql(sSQL)
		End If
	End Sub

	Private Sub LoadData()
		Dim bImageExist As Boolean = False
		Dim iReader As SqlDataReader = dbPages.ExecReader("SELECT * FROM tblCompanySettingsCart WHERE CompanyID=" & Merchant.ID)
		If iReader.Read() Then
			chkShippingEnabled.Checked = iReader("IsShippingEnabled")
			chkShippingRequired.Checked = iReader("IsShippingRequired")
			chkStoreCart.Checked = iReader("IsCartStored")
			chkIsAuthOnly.Checked = iReader("IsAuthOnly")
		End If
		iReader.Close()
	End Sub

	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		MyBase.OnLoad(e)
		If Not IsPostBack Then LoadData()
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	 <tr>
		 <td>
			<span class="PageHeading">SHOPPING CART</span>
			<span class="PageSubHeading"> - MANAGEMENT</span><br />
		 </td>
	</tr>
	<tr>
		<td>
			<br /><br />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr><td class="SecHeading" colspan="2">Configuration<br /></td></tr>
			<tr><td><br /></td></tr>
			<tr><td valign="top"><asp:CheckBox runat="server" ID="chkShippingEnabled" />Ask for shipping address<br /></td></tr>
			<tr><td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" ID="chkShippingRequired" />Require shipping address<br /></td></tr>
			<tr><td valign="top"><asp:CheckBox runat="server" ID="chkStoreCart" />Store cart between visits<br /></td></tr>
			<tr><td valign="top"><asp:CheckBox runat="server" ID="chkIsAuthOnly" />Authorize transaction instead of capture<br /></td></tr>
			<tr><td><br /></td></tr>
			<tr>
				<td>
					<br/>
					<asp:Button ID="Button1" runat="server" OnClick="SaveForm" Text="Update" CssClass="submit" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
</asp:Content>

