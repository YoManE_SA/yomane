﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OuterLogin.aspx.vb" Inherits="Netpay.MerchantDevCenter.OuterLogin" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
    <title><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:PageTitle %>" /></title>
		<style type="text/css">
			body { background:transparent url('/NPCommon/images/outerLoginBkg.gif')!important; font-family:Arial, Helvetica, sans-serif; margin:0; padding:0; overflow:hidden; }
			form { margin:0; padding:0; }
			table.webControlsLogin td { font-size: 10px!important; color:#fff; line-height:10px!important;}
			input, textarea { background:#fff; border:thin silver solid!important; height:18px!important; width:155px!important;}
			.loginbtn { font-size:12px; font-weight:bold; color:#fff; background:transparent url('/NPCommon/images/green_btn.png')!important; width:133px!important; height:25px!important; text-align:center!important; border:none!important;  }
			.divLink { padding-top:6px; margin-left:5px; }
			.divLink a { color:#ffffff; font-size:12px; }
		</style>
	</head>
	<body>
		<form id="form1" runat="server">
			<div>
			<netpay:Login ID="Login1" UserType="MerchantPrimary" OnLoginSuccess="LoginSuccess" Layout="Vertical" ShowMessageBox="true" runat="server" />
			</div>
			<div class="divLink"><a href="https://devcenter.netpay-intl.com" target="_blank"><asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Developer_Center%>" /></a></div>
		</form>
	</body>
</html>
