<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

        ' 20080416 Tamir
        '
        ' Support for multiple devcenter sites
        '
        lblCompany1.Text = CurrentDomain.BrandName
        lblCompany2.Text = CurrentDomain.BrandName
        lblCompany3.Text = CurrentDomain.BrandName
        lblCompany4.Text = CurrentDomain.BrandName
        lblCompany5.Text = CurrentDomain.BrandName
    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">HOSTED PAYMENT PAGE</span>
			<span class="PageSubHeading">- ABOUT</span><br />
			<custom:AuthAlert PropertyToCheck="HostedPayment" runat="server" />
		</td>
		<td valign="top" align="right">

		</td>
	</tr>		
	<tr>
		<td colspan="2" valign="top">
			Hosted payment page is a secured form that launches above the sellers website from
			<asp:Label ID="lblCompany1" runat="server" /> URL to accept credit-card information from the buyer.
			The window will explain the buyer that <asp:Label ID="lblCompany2" runat="server" /> is authorized
			to accept payment for the seller and show the seller information registered with
			<asp:Label ID="lblCompany3" runat="server" /> system and enables the buyer to make payment
			towards the sellers account.<br />
			<br />
			This form of accepting payment is the most common way of accepting payment via
			<asp:Label ID="lblCompany4" runat="server" /> due to the fact that the seller does not see
			the full credit card information of the buyer and their for the seller does not require to secure
			his website or deal with any data security to his server which saves the seller a lot of money
			or time consuming application filing for private merchant account.<br />
			<br />
			The buyer has the option of creating a Personal Account at
			<asp:Label ID="lblCompany5" runat="server" />.
			That enables him to view a full real-time report of his expanses, make payment
			to other sellers and friends using his account by wire transfer,
			real cash deposits and credit card payments.
			<br />
			<br />
			It's strongly recommended to verify each successful tranaction using our <a href="VerifyTrans_About.aspx">transaction verification</a> service.
		</td>
	</tr>
	</table>
	<br />
</asp:Content>