﻿<%@ Page Title="Payment Methods" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/MasterPage.master" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        rptPaymentType.DataSource = Netpay.Bll.PaymentMethods.PaymentMethod.Cache.Where(Function(p) p.Type <> Netpay.CommonTypes.PaymentMethodType.System)
        rptPaymentType.DataBind()
        rptPaymentGroup.DataSource = Netpay.Bll.PaymentMethods.Group.Search(Nothing) : rptPaymentGroup.DataBind()
    End Sub
</script>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">PAYMENT METHODS LIST</span>
		</td>
	</tr>
    <tr>
        <td>
            <asp:repeater runat="server" id="rptPaymentGroup">
                <HeaderTemplate>
                    PAYMENT GROUPS:
                    <table class="FieldsTable" width="80%" >
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Abbreviation</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><asp:Literal ID="Literal1" runat="server" Text='<%#Eval("ID")%>' /></td>
                        <td><asp:Literal ID="Literal2" runat="server" Text='<%#Eval("Name")%>' /></td>
                        <td><asp:Literal ID="Literal3" runat="server" Text='<%#Eval("ShortName")%>' /></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:repeater>
        </td>
    </tr>
    <tr><td><br /></td></tr>
    <tr>
        <td>
            PAYMENT METHODS:
            <asp:repeater runat="server" id="rptPaymentType">
                <HeaderTemplate>
                    <table class="FieldsTable" width="80%" >
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Abbreviation</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><asp:Literal runat="server" Text='<%#Eval("ID")%>' /></td>
                        <td><asp:Literal runat="server" Text='<%#Eval("Name")%>' /></td>
                        <td><asp:Literal runat="server" Text='<%#Eval("Abbreviation")%>' /></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:repeater>
        </td>
    </tr>
    </table>
</asp:Content>