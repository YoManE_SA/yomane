<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"   Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" EnableViewState="true" %>
<%@ Register TagPrefix="netpay" Namespace="Netpay.Web.Controls" Assembly="Netpay.Web" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<%@ Import namespace="System.Linq" %>
<script runat="server">
	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not IsPostBack Then LoadData()
	End Sub
    
    Private Sub btnSave_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        
        Dim settings = Netpay.Bll.Merchants.Hosted.PaymentPage.Load(Merchant.ID)
        If settings Is Nothing Then settings = New Netpay.Bll.Merchants.Hosted.PaymentPage(Merchant.ID)
        settings.RedirectionUrl = ReplyURL.Text
        settings.NotificationUrl = ReplySilentURL.Text
        settings.Save()
    End Sub
		
    Private Sub LoadData()
        Dim reqString As String = " *"
        Dim bImageExist As Boolean = False
        Dim settings = Netpay.Bll.Merchants.Hosted.PaymentPage.Load(Merchant.ID)
        If settings Is Nothing Then Exit Sub
        
        ReplySilentURL.Text = dbPages.dbtextShow(settings.NotificationUrl)
        ReplyURL.Text = dbPages.dbtextShow(settings.RedirectionUrl)
    End Sub

    Sub btnSendRequest_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim nStatus As Integer = 0
        If Trim(ReplySilentURL.Text) <> "" Then
            Try
                nStatus = dbPages.SendRequestHTTP("POST", ReplySilentURL.Text, "replyCode=000&trans_id=1000&trans_date=01/01/1900+00:00:01")
                ltRequestRet.Text = "<span style=""color:green"">String was sent succesfully, return code: " & nStatus & "</span><br />"
            Catch ex As Exception
                ltRequestRet.Text = "<span style=""color:red"">String cannot be send due to the following error:<div>" & ex.Message & "</div></span>"
            End Try
        Else
            ltRequestRet.Text = "<span style=""color:maroon"">Please set url for hidden reply</span><br />"
        End If
    End Sub
		
    Sub Page_Load()
        If (Authorization.IsAnyHostedV2Enabled) Then
            Select Case dbPages.TestVar(Request("T"), 0, -1, 1)
                Case "1"
                    aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2CreditCard
                    aa.formatFeatureMessage("Credit Card Hosted Page")
                Case "2"
                    aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Echeck
                    aa.formatFeatureMessage("Echeck Hosted Page")
                Case "3"
                    aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Instant
                    aa.formatFeatureMessage("Alternative payment methods Hosted Page")
            End Select
        Else
            aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2CreditCard
        End If
        
    End Sub
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	 <tr>
		 <td>
			<span class="PageHeading">HOSTED PAYMENT PAGE VER.2</span>
			<span class="PageSubHeading">- MANAGEMENT</span><br />
			<custom:AuthAlert ID="aa" PropertyToCheck="None" runat="server" />
		 </td>
	</tr>
	<tr>
		<td>
			<br /><br />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr><td class="SecHeading" colspan="2">Display Settings<br /></td></tr>
			<tr><td><br /></td></tr>
            <tr><td>Settings for Logo, Text and Input filelds can be found at Merchant Control Panel.</td></tr>
            </table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
            <br /><br />
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="2">
			<tr><td class="SecHeading" colspan="2">Global Request Fields<br /></td></tr>
			<tr><td><br /></td></tr>
			<tr>
				<td valign="top" nowrap="nowrap">Redirection URL<br /></td>
				<td valign="top">
					<asp:TextBox runat="server" ID="ReplyURL" size="70" MaxLength="200" Cssclass="inputdata" /><br />
					URL Address: Be sure to include http:// or https://<br />
				</td>
			</tr>
			<tr><td height="15"></td></tr>
			<tr>
				<td valign="top" nowrap="nowrap">Notification URL<br /></td>
				<td>
					<asp:TextBox runat="server" ID="ReplySilentURL" size="70" MaxLength="200" Cssclass="inputdata" /><br />
					URL Address: Be sure to include http:// or https://<br />
				</td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td></td>
				<td>
					<table border="0" cellspacing="0" cellpadding="4" width="413" style="border:1px solid #c5c5c5;">
					<tr>
						<td>
							<span style="text-decoration:underline;">Test your page with this reply</span><br />
							(replyCode=000&trans_id=1000&trans_date=01/01/1900+00:00:01) &nbsp;
							<asp:Button runat="server" ID="btnSendRequest" Text="SEND" style="font-size:11px;" OnClick="btnSendRequest_OnClick" /><br />
							<asp:Literal runat="server" ID="ltRequestRet" Mode="PassThrough" />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td></td>
				<td>
					<br/>
					<asp:Button runat="server" UseSubmitBehavior="true" ID="btnSave" Text=" Update " CssClass="submit" OnClick="btnSave_OnClick" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
</asp:Content>