﻿<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  MasterPageFile="~/Templates/Tmp_netpayintl/MasterPage.master" Title="ACCOUNT CONFIGURATION" %>
<script runat="server">
	Sub Page_Load()
		mcsData.MerchantID = Merchant.ID
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td>
				<span class="PageHeading">ACCOUNT INFO</span>
			</td>
		</tr>
		<tr>
			<td>
				<br />
				<netpay:MerchantConfigSnapshot TableCssClass="FieldsTable" ID="mcsData" runat="server" />
			</td>
		</tr>
	</table>
</asp:Content>
