﻿<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		lbPageHeading.Text = Request("Title")
		MyBase.OnLoad(e)
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<asp:Label runat="server" id="lbPageHeading" class="PageHeading" /><br />
		</td>
	</tr>
	<tr>
		<td style="text-align:center;">
			<br /><br /><br />
			<h5>Permission denied</h5>
			Guest login is not allowed to view this page
		</td>
	</tr>
	</table>
</asp:Content>
