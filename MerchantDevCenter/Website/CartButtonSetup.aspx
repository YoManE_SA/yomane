﻿<%@ Page Title="" Language="VB"  AutoEventWireup="false" Inherits="Netpay.MerchantDevCenter.Website_CartButtonSetup" Codebehind="CartButtonSetup.aspx.vb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<script type="text/javascript">
		function changeCurrency()
		{
			var selectedCurrency = $("#ctl00_ContentPlaceHolder1_ddCurrency option:selected").text();
			$("#ctl00_ContentPlaceHolder1_lblCurrency1").html(selectedCurrency);
			$("#ctl00_ContentPlaceHolder1_lblCurrency2").html(selectedCurrency);
		}

		function clearResult()
		{
			$("#ctl00_ContentPlaceHolder1_txtResult").text("");
		}

		function selectResult()
		{
			$("#ctl00_ContentPlaceHolder1_txtResult").select();
		}
	</script>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<span class="PageHeading">SHOPPING CART</span>
			<span class="PageSubHeading"> - BUTTON WIZARD</span><br />
		</td>
	</tr>
	</table>
	<br /><br />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<div id="txtError" class="error" visible="false" runat="server"></div>
		</td>
	</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="top">
				<table border="0" width="290">
					<tr>
						<td colspan="2" class="SecHeading">ADD ITEM BUTTON<br /><br /></td>
					</tr>
					<tr>
						<td>Item ID</td>
						<td>
							<asp:TextBox ID="txtItemID" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>Item Name</td>
						<td>
							<asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>Currency</td>
						<td><netpay:CurrencyDropDown ID="ddCurrency" onchange="changeCurrency()" runat="server"></netpay:CurrencyDropDown></td>
					</tr>
					<tr>
						<td>Price</td>
						<td>
							<asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
							<span id="lblCurrency1" runat="server"></span>
						</td>
					</tr>
					<tr>
						<td>Tax Rate</td>
						<td><asp:TextBox ID="txtTaxRate" runat="server"></asp:TextBox> %</td>
					</tr>
					<tr>
						<td>Shipping</td>
						<td>
							<asp:TextBox ID="txtShipping" runat="server"></asp:TextBox>
							<span id="lblCurrency2" runat="server"></span>
						</td>
					</tr>
					<tr>
						<td>Item Info. URL</td>
						<td><asp:TextBox ID="txtItemInfoUrl" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>Item Image URL</td>
						<td><asp:TextBox ID="txtItemImageUrl" runat="server"></asp:TextBox></td>
					</tr>
					<tr>
						<td>Open method</td>
						<td>
							<asp:DropDownList ID="ddActivationModeItem" runat="server">
								<asp:ListItem Text="Popup" Value="popup"></asp:ListItem>
								<asp:ListItem Text="Redirect" Value="redirect"></asp:ListItem>
							</asp:DropDownList>
						</td>
					</tr>
					<tr><td><br /></td></tr>
					<tr>
						<td>Minimum Quantity</td>
						<td><asp:TextBox ID="txtQuantityMin" runat="server" /></td>
					</tr>
					<tr>
						<td>Maximum Quantity</td>
						<td><asp:TextBox ID="txtQuantityMax" runat="server" /></td>
					</tr>
					<tr>
						<td>Quantity Step</td>
						<td><asp:TextBox ID="txtQuantityStep" runat="server" /></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<br /><asp:Button ID="btnCreateItemButton" Text="Create" OnClientClick="clearResult()" runat="server" />
						</td>
					</tr>
					<tr><td><br /><br /></td></tr>
					<tr>
						<td colspan="2" class="SecHeading">OPEN CART BUTTON<br /><br /></td>
					</tr>
					<tr>
						<td>Open method</td>
						<td>
							<asp:DropDownList ID="ddActivationModeCart" runat="server">
								<asp:ListItem Text="Popup" Value="popup"></asp:ListItem>
								<asp:ListItem Text="Redirect" Value="redirect"></asp:ListItem>
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<br /><asp:Button ID="btnCreateCartButton" Text="Create" OnClientClick="clearResult()" runat="server" />
						</td>
					</tr>
				</table>			
			</td>
			<td align="right" valign="top">
				<table align="right" style="background-color:#f5f5f5;">
					<tr><td class="SecHeading">Resulted Code<br /><br /></td></tr>
					<tr>
						<td align="right" style="padding:0 15px;">
							<asp:TextBox ID="txtResult" TextMode="MultiLine" BorderStyle="Solid" Width="330px" Height="350px" runat="server" BorderWidth="1px" BorderColor="Silver" BackColor="White"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td style="padding:10px 15px;">
							<input type="button" id="btnSelect" value="Select Text" onclick="selectResult()" runat="server" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:Content>

