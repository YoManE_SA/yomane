﻿<%@ Page Language="VB"  Title="DEVELOPER CENTER - PAYMENT METHOD STORAGE" MasterPageFile="~/Templates/Tmp_netpayintl/MasterPage.master" AutoEventWireup="true" Inherits="Netpay.MerchantDevCenter.Website_StoreMethoodPage_Wizard" Codebehind="Storage_Wizard.aspx.vb" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentHeader" runat="Server">
	<script type="text/javascript">
		function showSection(secName)
		{
			with (document.getElementById(secName))
			{
				if (style.display != '') 
					style.display = ''; 
				else 
					style.display = 'none';

				document.getElementById(secName + 'Img').src = (style.display != '' ? '/NPCommon/Images/tree_expand.gif' : '/NPCommon/Images/tree_collapse.gif');
			}
		}
	</script>
	<style type="text/css">
		input { background-color: white !important; }
		input.wide { width: 100%; }
		input.option { vertical-align: middle; }
		input.bkgwhite { background-color: #ffffff !important; }
	</style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td>
				<span class="PageHeading">PAYMENT METHOD STORAGE</span>
				<span class="PageSubHeading"> - WIZARD</span><br />
				<custom:AuthAlert ID="aa" PropertyToCheck="HostedPaymentV2" runat="server" />
			</td>
		</tr>
		<tr>
			<td>
				<br />
				The following form will help you create a link to place in your website.<br />
				For a complete list of all possible parameters see <i>Payment Store Page - <a href="Storage_MsgFields.aspx?T=<%=Request("T")%>">
					Message Fields</a></i>
				<img src="/NPCommon/images/iconNewWinRight.GIF" align="middle" /><br />
			</td>
		</tr>
	</table>
	<br />
	<br />
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="top">
				<table border="0" style="width: 360px;">
					<tr>
						<td class="SecHeading">
							Required data<br />
							<br />
						</td>
					</tr>
					<!--
					<tr>
						<td>merchantID<br /></td>
						<td><asp:TextBox runat="server" class="wide" id="merchantID" Enabled="false" /><br /></td>
					</tr>
					-->
					<tr>
						<td valign="top">
							<table border="0" cellspacing="2" cellpadding="1">
								<tr>
									<td>
										UI Language
									</td>
									<td>
										<asp:RadioButtonList runat="server" ID="disp_lng" RepeatDirection="Horizontal">
											<asp:ListItem Text="English" Value="en-us" Selected="True" />
											<asp:ListItem Text="Hebrew" Value="he-il" />
											<asp:ListItem Text="Italian" Value="it-it" />
										</asp:RadioButtonList>
									</td>
								</tr>
								<asp:PlaceHolder ID="phMD5Required" Visible="false" runat="server">
									<tr>
										<td>
											MD5 signature
										</td>
										<td>
											<asp:CheckBox runat="server" ID="cbMD5Required" Checked="true" Enabled="false" />Include signature in string
										</td>
									</tr>								
								</asp:PlaceHolder>
							</table>
							<br />
							<br />
						</td>
					</tr>
					<tr>
						<td class="SecHeading">
							Optional Data<br />
							<br />
						</td>
					</tr>
					<tr>
						<td>
							<table border="0" cellspacing="2" cellpadding="1">
								<tr>
									<td style="width: 120px;">
										Text Pay For
										<netpay:PageItemHelp ID="PageItemHelp1" Text="Text displayed on the page to describe what the payment is for, for example: basket checkout, flight tickets, etc..."
											Width="300" runat="server" />
									</td>
									<td style="width: 235px;">
										<asp:TextBox runat="server" class="wide" ID="disp_payFor" Text="Purchase" /><br />
									</td>
								</tr>
								<tr>
									<td>
										Trans Comment<br />
									</td>
									<td>
										<asp:TextBox runat="server" class="wide" ID="trans_comment" MaxLength="500" Text="Testing hosted payment page" /><br />
									</td>
								</tr>
								<tr>
									<td>
										Redirect URL
										<netpay:PageItemHelp ID="PageItemHelp2" Text="URL within your domain to resirect the customer when payment process is complete"
											Width="300" runat="server" />
									</td>
									<td>
										<asp:TextBox runat="server" class="wide" ID="url_redirect" /><br />
									</td>
								</tr>
								<tr>
									<td>
										Notify URL
										<netpay:PageItemHelp ID="PageItemHelp3" Text="URL within your domain to send hidden server notification with the status of the transaction"
											Width="300" runat="server" />
									</td>
									<td>
										<asp:TextBox runat="server" class="wide" ID="url_notify" /><br />
									</td>
								</tr>
								<asp:PlaceHolder ID="phOptionalMD5" Visible="false" runat="server">
									<tr>
										<td>
											MD5 signature
										</td>
										<td>
											<asp:CheckBox runat="server" ID="cbMD5Optional" />Include signature in string
										</td>
									</tr>								
								</asp:PlaceHolder>
							</table>
							<br />
							<br />
						</td>
					</tr>
					<tr>
						<td onclick="showSection('tdClientInfo')" style="cursor: pointer;">
							<img id="tdClientInfoImg" src="/NPCommon/Images/tree_expand.gif" alt="expand" align="top" /><span
								class="SecHeading">Optional Filling Client Data</span>
								<netpay:PageItemHelp ID="PageItemHelp5" Text="Depending on payment type used, if fields are used on page then automaticly fill value with data from below"
								Width="300" runat="server" />
							<br />
						</td>
					</tr>
					<tr>
						<td style="display: none;" id="tdClientInfo">
							<table class="FormTable" border="0" cellspacing="2" cellpadding="1">
								<tr>
									<td style="width: 120px;">
										Full Name<br />
									</td>
									<td style="width: 235px;">
										<asp:TextBox ID="client_fullName" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Email Address<br />
									</td>
									<td>
										<asp:TextBox ID="client_email" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Phone Number<br />
									</td>
									<td>
										<asp:TextBox ID="client_phoneNum" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										ID Number<br />
									</td>
									<td>
										<asp:TextBox ID="client_idNum" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										<br />
									</td>
								</tr>
								<tr>
									<td>
										Billing Address 1<br />
									</td>
									<td>
										<asp:TextBox ID="client_billaddress1" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Billing Address 2<br />
									</td>
									<td>
										<asp:TextBox ID="client_billaddress2" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Billing City<br />
									</td>
									<td>
										<asp:TextBox ID="client_billcity" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Billing Zipcode<br />
									</td>
									<td>
										<asp:TextBox ID="client_billzipcode" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Billing State<br />
									</td>
									<td>
										<asp:TextBox ID="client_billstate" class="wide" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										Billing Country<br />
									</td>
									<td>
										<asp:TextBox ID="client_billcountry" class="wide" runat="server" />
									</td>
								</tr>
							</table>
							<br />
							<br />
						</td>
					</tr>
					<tr>
						<td>
							<br />
						</td>
					</tr>
				</table>
			</td>
			<td valign="top" align="right">
				<table border="0" align="right" style="background-color: #f5f5f5; width: 314px;">
					<tr>
						<td class="SecHeading">
							Generated Code<br />
						</td>
					</tr>
					<tr>
						<td>
							<br />
						</td>
					</tr>
					<tr>
						<td colspan="2" valign="top">
							<asp:TextBox runat="server" ReadOnly="true" ID="txQuery" TextMode="MultiLine" Rows="5"
								Style="border: 1px dashed #c0c0c0; background-color: #f7f7f7; overflow: auto;
								width: 306px; height: 250px; font: normal 12px Courier New, Monospace;" />
							<br />
							<asp:Button runat="server" ID="btnGenerate" OnClick="btnGenerate_Click" Text="Refresh Code"
								Style="background-color: white; font-size: 10px; width: 100px; cursor: pointer;" />
							&nbsp;&nbsp;
							<asp:Button runat="server" ID="btnOpenPage" OnClick="btnOpenPage_Click" UseSubmitBehavior="true"
								Text="Open Window" Style="background-color: white; font-size: 10px; width: 100px;
								cursor: pointer;" /><br />
							<br />
							<span style="font-size: 10px;">Refresh and copy the gernerated link into your website.
								To see resulted code click the "Open Window" button</span><br />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<asp:Literal ID="OpenScript" runat="server" Mode="PassThrough" />
</asp:Content>
