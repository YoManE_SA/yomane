<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<script runat="server">
		Dim langTxt As String = ""
		Dim langCode As String = ""
		Dim styleDir As String = ""
		
		Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
			Select Case Trim(Request("lang"))
				Case "eng" : langTxt = "English" : langCode = 1 : styleDir = "ltr"
				Case "spa" : langTxt = "Spanish" : langCode = 2 : styleDir = "ltr"
				Case "heb" : langTxt = "Hebrew" : langCode = 0 : styleDir = "rtl"
			End Select
			If Not IsPostBack Then LoadData()
		End Sub
		
		Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
			Dim sSQL As String = "UPDATE tblPublicPayPersonalDataOptions SET " & _
			"FullName=" & IIf(FullNameShow.Checked, 1, 0) & ", isRequiredFullName=" & IIf(FullNameRequiredR.Checked, 1, 0) & _
			", Address=" & IIf(AddressShow.Checked, 1, 0) & ", isRequiredAddress=" & IIf(AddressRequiredR.Checked, 1, 0) & _
			", Phone=" & IIf(PhoneShow.Checked, 1, 0) & ", isRequiredPhone=" & IIf(PhoneRequiredR.Checked, 1, 0) & _
			", Cellular=" & IIf(CellularShow.Checked, 1, 0) & ", isRequiredCellular=" & IIf(CellularRequiredR.Checked, 1, 0) & _
			", Email=" & IIf(EmailShow.Checked, 1, 0) & ", isRequiredEmail=" & IIf(EmailRequiredR.Checked, 1, 0) & _
			", HowGetHere=" & IIf(HowGetHereShow.Checked, 1, 0) & ", isRequiredHowGetHere=" & IIf(HowGetHereRequiredR.Checked, 1, 0) & _
			", Var1=" & IIf(Var1Show.Checked, 1, 0) & ", Var1_name='" & Var1Name.Text.ToSql(True) & "', Var1_isRequired=" & IIf(Var1R.Checked, 1, 0) & _
			", Var2=" & IIf(Var2Show.Checked, 1, 0) & ", Var2_name='" & Var2Name.Text.ToSql(True) & "', Var2_isRequired=" & IIf(Var2R.Checked, 1, 0) & _
			", Var3=" & IIf(Var3Show.Checked, 1, 0) & ", Var3_name='" & Var3Name.Text.ToSql(True) & "', Var3_isRequired=" & IIf(Var3R.Checked, 1, 0) & _
			", Var4=" & IIf(Var4Show.Checked, 1, 0) & ", Var4_name='" & Var4Name.Text.ToSql(True) & "', Var4_isRequired=" & IIf(Var4R.Checked, 1, 0) & _
			" WHERE CompanyID=" & WebUtils.LoggedUser.ID & " AND lang=" & langCode
			If dbPages.ExecSql(sSQL) = 0 Then
				sSQL = "INSERT INTO tblPublicPayPersonalDataOptions(FullName, isRequiredFullName, Address, isRequiredAddress, Phone, isRequiredPhone, Cellular, isRequiredCellular, Email, isRequiredEmail, HowGetHere, isRequiredHowGetHere," & _
				"Var1, Var1_name, Var1_isRequired, Var2, Var2_name, Var2_isRequired, Var3, Var3_name, Var3_isRequired, Var4, Var4_name, Var4_isRequired,CompanyID, lang)" & _
				"VALUES(" & IIf(FullNameShow.Checked, 1, 0) & "," & IIf(FullNameRequiredR.Checked, 1, 0) & _
				"," & IIf(AddressShow.Checked, 1, 0) & "," & IIf(AddressRequiredR.Checked, 1, 0) & _
				"," & IIf(PhoneShow.Checked, 1, 0) & "," & IIf(PhoneRequiredR.Checked, 1, 0) & _
				"," & IIf(CellularShow.Checked, 1, 0) & "," & IIf(CellularRequiredR.Checked, 1, 0) & _
				"," & IIf(EmailShow.Checked, 1, 0) & "," & IIf(EmailRequiredR.Checked, 1, 0) & _
				"," & IIf(HowGetHereShow.Checked, 1, 0) & "," & IIf(HowGetHereRequiredR.Checked, 1, 0) & _
				"," & IIf(Var1Show.Checked, 1, 0) & ",'" & Var1Name.Text.ToSql(True) & "'," & IIf(Var1R.Checked, 1, 0) & _
				"," & IIf(Var2Show.Checked, 1, 0) & ",'" & Var2Name.Text.ToSql(True) & "'," & IIf(Var2R.Checked, 1, 0) & _
				"," & IIf(Var3Show.Checked, 1, 0) & ",'" & Var3Name.Text.ToSql(True) & "'," & IIf(Var3R.Checked, 1, 0) & _
				"," & IIf(Var4Show.Checked, 1, 0) & ",'" & Var4Name.Text.ToSql(True) & "'," & IIf(Var4R.Checked, 1, 0) & _
				"," & WebUtils.LoggedUser.ID & "," & langCode & ")"
				dbPages.ExecSql(sSQL)
			End If
		End Sub

		Private Sub LoadData()
			Dim iReader As SqlDataReader = dbPages.ExecReader("SELECT * FROM tblPublicPayPersonalDataOptions WHERE companyID = " & WebUtils.LoggedUser.ID & " AND lang = " & langCode)
			If iReader.Read() Then
				FullNameShow.Checked = iReader("FullName")
				IIf(iReader("isRequiredFullName"), FullNameRequiredR, FullNameRequiredO).Checked = True
				AddressShow.Checked = iReader("Address")
				IIf(iReader("isRequiredAddress"), AddressRequiredR, AddressRequiredO).Checked = True
				PhoneShow.Checked = iReader("Phone")
				IIf(iReader("isRequiredPhone"), PhoneRequiredR, PhoneRequiredO).Checked = True
				CellularShow.Checked = iReader("Cellular")
				IIf(iReader("isRequiredCellular"), CellularRequiredR, CellularRequiredO).Checked = True
				EmailShow.Checked = iReader("Email")
				IIf(iReader("isRequiredEmail"), EmailRequiredR, EmailRequiredO).Checked = True
				HowGetHereShow.Checked = iReader("HowGetHere")
				IIf(iReader("isRequiredHowGetHere"), HowGetHereRequiredR, HowGetHereRequiredO).Checked = True

				Var1Show.Checked = iReader("Var1")
				Var1Name.Text = iReader("Var1_name")
				IIf(iReader("Var1_isRequired"), Var1R, Var1O).Checked = True

				Var2Show.Checked = iReader("Var2")
				Var2Name.Text = iReader("Var2_name")
				IIf(iReader("Var2_isRequired"), Var2R, Var2O).Checked = True

				Var3Show.Checked = iReader("Var3")
				Var3Name.Text = iReader("Var3_name")
				IIf(iReader("Var3_isRequired"), Var3R, Var3O).Checked = True

				Var4Show.Checked = iReader("Var4")
				Var4Name.Text = iReader("Var4_name")
				IIf(iReader("Var4_isRequired"), Var4R, Var4O).Checked = True
			End If
			iReader.Close()
		End Sub
	</script>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td>
			<span class="PageHeading">PUBLIC PAYMENT PAGE</span>
			<span class="PageSubHeading">- MANAGE APPEARANCE (<%=langTxt%>)</span><br />
		</td>
		<td align="right">[<a class="faq" href="PublicPage_AdminAppear.aspx?lang=<%= trim(request("lang")) %>">back to page management</a>]<br /></td>
	</tr>
	<tr>
		<td colspan="2">
			<custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td colspan="2">Mark the fields you wish to include in the payment form<br /></td></tr>
	<tr><td height="12"></td></tr>
	<tr>
		<td colspan="2">
			<input type="hidden" name="lang" value="<%= request("lang").ToSql(True) %>" />
			<table width="100%" border="0" cellspacing="0" cellpadding="1">
			<tr>
				<td><asp:CheckBox runat="server" ID="FullNameShow" /> Full name<br /></td>
				<td width="26"><br /></td>
				<td><asp:RadioButton ID="FullNameRequiredO" runat="server" GroupName="FullNameRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="FullNameRequiredR" runat="server" GroupName="FullNameRequired" class="input2" /> Required filed</td>
			</tr>
			<tr>
				<td><asp:CheckBox runat="server" ID="AddressShow" /> Address<br /></td>
				<td><br /></td>
				<td><asp:RadioButton ID="AddressRequiredO" runat="server" GroupName="AddressRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="AddressRequiredR" runat="server" GroupName="AddressRequired" class="input2" /> Required filed</td>
			</tr>
			<tr>
				<td><asp:CheckBox runat="server" ID="PhoneShow" /> Phone<br /></td>
				<td><br /></td>
				<td><asp:RadioButton ID="PhoneRequiredO" runat="server" GroupName="PhoneRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="PhoneRequiredR" runat="server" GroupName="PhoneRequired" class="input2" /> Required filed</td>
			</tr>
			<tr>
				<td><asp:CheckBox runat="server" ID="CellularShow" /> Cell phone<br /></td>
				<td><br /></td>
				<td><asp:RadioButton ID="CellularRequiredO" runat="server" GroupName="CellularRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="CellularRequiredR" runat="server" GroupName="CellularRequired" class="input2" /> Required filed</td>
			</tr>
			<tr>
				<td><asp:CheckBox runat="server" ID="EmailShow" /> Email<br /></td>
				<td><br /></td>
				<td><asp:RadioButton ID="EmailRequiredO" runat="server" GroupName="EmailRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="EmailRequiredR" runat="server" GroupName="EmailRequired" class="input2" /> Required filed</td>
			</tr>
			<tr>
				<td><asp:CheckBox runat="server" ID="HowGetHereShow" /> How did you arrive here<br /></td>
				<td><br /></td>
				<td><asp:RadioButton ID="HowGetHereRequiredO" runat="server" GroupName="HowGetHereRequired" class="input2" /> Optional filed</td>
				<td><asp:RadioButton ID="HowGetHereRequiredR" runat="server" GroupName="HowGetHereRequired" class="input2" /> Required filed</td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td>
					<asp:CheckBox runat="server" ID="Var1Show" />
					&nbsp;<asp:TextBox runat="server" MaxLength="100" ID="Var1Name" size="20" dir="<%= sStyleDir %>" class="inputData" /><br />
				</td>
				<td><br /></td>
				<td><asp:RadioButton ID="Var1O" runat="server" GroupName="Var1" class="input2" /> Optional field </td>
				<td><asp:RadioButton ID="Var1R" runat="server" GroupName="Var1" class="input2" /> Required field </td>
			</tr>
			<tr>
				<td>
					<asp:CheckBox runat="server" ID="Var2Show" />
					&nbsp;<asp:TextBox runat="server" MaxLength="100" ID="Var2Name" size="20" dir="<%= sStyleDir %>" class="inputData" /><br />
				</td>
				<td><br /></td>
				<td><asp:RadioButton ID="Var2O" runat="server" GroupName="Var2" class="input2" /> Optional field </td>
				<td><asp:RadioButton ID="Var2R" runat="server" GroupName="Var2" class="input2" /> Required field </td>
			</tr>
			<tr>
				<td>
					<asp:CheckBox runat="server" ID="Var3Show" />
					&nbsp;<asp:TextBox runat="server" MaxLength="100" ID="Var3Name" size="20" dir="<%= sStyleDir %>" class="inputData" /><br />
				</td>
				<td><br /></td>
				<td><asp:RadioButton ID="Var3O" runat="server" GroupName="Var3" class="input2" /> Optional field </td>
				<td><asp:RadioButton ID="Var3R" runat="server" GroupName="Var3" class="input2" /> Required field </td>
			</tr>
			<tr>
				<td>
					<asp:CheckBox runat="server" ID="Var4Show" />
					&nbsp;<asp:TextBox runat="server" MaxLength="100" ID="Var4Name" size="20" dir="<%= sStyleDir %>" class="inputData" /><br />
				</td>
				<td><br /></td>
				<td><asp:RadioButton ID="Var4O" runat="server" GroupName="Var4" class="input2" /> Optional field </td>
				<td><asp:RadioButton ID="Var4R" runat="server" GroupName="Var4" class="input2" /> Required field </td>
			</tr>
			<tr>
				<td colspan="4" align="right">
					<br /><asp:Button runat="server" UseSubmitBehavior="true" Text=" Update " class="submit" OnCommand="btnUpdate_Click" /><br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>	
</asp:Content>