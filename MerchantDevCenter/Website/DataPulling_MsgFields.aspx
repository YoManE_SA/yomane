<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - DATA PULLING" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
	Dim sBgColor As String = String.Empty
    Dim IsCustomerPurchasePayerID, IsApprovalOnly As Boolean
	Dim sProcessURL As String = CurrentDomain.ProcessUrl

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If WebUtils.IsLoggedin Then
            Dim sSQL As String = "SELECT IsCustomerPurchasePayerID, IsApprovalOnly FROM tblCompany WHERE ID = " & Merchant.ID
            Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
            If iReader.Read() Then
                IsCustomerPurchasePayerID = iReader("IsCustomerPurchasePayerID")
                IsApprovalOnly = iReader("IsApprovalOnly")
            End If
            iReader.Close()
        End If
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">DATA PULLING</span>
			<span class="PageSubHeading">- MESSAGE FIELDS</span><br />
			<custom:AuthAlert ID="aa" PropertyToCheck="AllowRemotePull" runat="server" />
		</td>
	</tr>	
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">PAGE URL<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td><%=sProcessURL %>Remote_DataPulling.aspx<br /></td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Request Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
				<th width="46">Max<br />Length<br /></th>
				<th width="80" valign="bottom">Required<br /></th>
			</tr>
			<tr>
				<td>CompanyNum<br /></td>
				<td>Your company number - <%=MerchantNumber%><br /></td>
				<td>7</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>DateStart<br /></td>
				<td>Beginning of transaction date range (yyyy-mm-dd HH:mm:ss) <sup>1</sup><br /></td>
				<td>20</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>DateEnd<br /></td>
				<td>End of transaction date range (yyyy-mm-dd HH:mm:ss) <sup>2</sup><br /></td>
				<td>20</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>ReqType<br /></td>
				<td>
					Request type:<br />
					<span style="font-family:Monospace;">0</span> = Captured transactions<br />
					<span style="font-family:Monospace;">1</span> = Rejected transactions<br />
					<span style="font-family:Monospace;">5</span> = Pre-auth. transactions<br />
					<span style="font-family:Monospace;">10</span> = Recurring modify log<br />
				</td>
				<td>2</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>TransType<br /></td>
				<td>
					Transaction type:<br />
					<span style="font-family:Monospace;">1</span> = Debit transactions<br />
					<span style="font-family:Monospace;">2</span> = Refund transactions<br />
					<span style="font-family:Monospace;">4</span> = Chargebacks<br />
					<span style="font-family:Monospace;">8</span> = Retrieval requests<br />
					<br />
					<div>The values can be combined; e.g. having TransType=6, both refunds and chargebacks will be returned.</div>
					<br />
					<div>If TransType is empty or not specified, transactions of all types will be returned.</div>
				</td>
				<td>1</td>
				<td>No</td>
			</tr>
			<tr>
				<td>TransID<br /></td>
				<td>
					Transaction ID to pull. This field is relevant only when pulling captured transactions (ReqType=0), otherwise it is ignored.
				</td>
				<td>8</td>
				<td>No</td>
			</tr>
			<tr>
				<td>Signature<br /></td>
				<td>
					Signature for verifying the authenticity of the request parameters.<br />
					Field values to use: <i>CompanyNum + PersonalHashKey</i><br /><br />
					Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
				</td>
				<td>30</td>
				<td>Yes</td>
			</tr>
			</table>
		</td>	
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Response Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr><td>When ReqType=0 and request is valid <sup>4</sup></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					<%
                        dbPages.ShowXml("Transactions Count=""3210"" ReturnCount=""1000""","",0,False)
                        dbPages.ShowXml("Trans","",1,False)
                        dbPages.ShowXml("Id","12345",2)
                        dbPages.ShowXml("Date","25/07/2009 12:35:46",2)
                        dbPages.ShowXml("Currency","1",2)
                        dbPages.ShowXml("Amount","10.99",2)
                        dbPages.ShowXml("Approval","1234567",2)
                        dbPages.ShowXml("Email","john@domain.com",2)
                        dbPages.ShowXml("Phone","1-123-1234567",2)
                        dbPages.ShowXml("PersonalNum","123456789",2)
                        dbPages.ShowXml("Type","CHB",2)
                        dbPages.ShowXml("ChargebackDate", "24/08/2009 22:33:44", 2, , "<sup>7</sup>")
                        dbPages.ShowXml("ChargebackReason", "33", 2, , "<sup>7</sup>")
                        dbPages.ShowXml("Order", "A123", 2)
                        dbPages.ShowXml("RecurringCharge", "2", 2, , "<sup>6</sup>")
                        dbPages.ShowXml("RecurringSeries", "231", 2, , "<sup>6</sup>")
                        dbPages.ShowXml("Comment","White box",2)
                        dbPages.ShowXml("Reply", "000", 2)
                        dbPages.ShowXml("OriginalTransID", "1234567", 2, , "<sup>6</sup>")
                        dbPages.ShowXml("RefTrans", "R1234567", 2, , "<sup>8</sup>")
                        dbPages.ShowXml("Method", "CC", 2, , "<sup>5</sup>")
                        dbPages.ShowXml("CreditCard", "", 2, False)
                        dbPages.ShowXml("Type","Visa",3)
                        dbPages.ShowXml("BIN","880005",3)
                        dbPages.ShowXml("BINCountry","US",3)
                        dbPages.ShowXml("ExpMM","01",3)
                        dbPages.ShowXml("ExpYY","10",3)
                        dbPages.ShowXml("Last4","1234",3)
                        dbPages.ShowXml("Holder","John Smith",3)
                        dbPages.ShowXml("/CreditCard","",2,False)
                        dbPages.ShowXml("Address","",2,False)
                        dbPages.ShowXml("Line1","99 Broadway Ave.",3)
                        dbPages.ShowXml("Line2","",3)
                        dbPages.ShowXml("City","Massapequa",3)
                        dbPages.ShowXml("Postal","11758",3)
                        dbPages.ShowXml("State","NY",3)
                        dbPages.ShowXml("Country","US",3)
                        dbPages.ShowXml("/Address","",2,False)
                        dbPages.ShowXml("/Trans","",1,False)
                        dbPages.ShowXml("/Transactions","",0,False)
					%>		
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td>When ReqType=10 and request is valid <sup>4</sup></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					<%
					dbPages.ShowXml("RecurringModifyLog Count=""3210"" ReturnCount=""1000""", "", 0, False)
						dbPages.ShowXml("RecurringModify","", 1, False)
							dbPages.ShowXml("LogID", "12345", 2)
							dbPages.ShowXml("DateTime", "01/02/2003 04:05:06", 2)
							dbPages.ShowXml("Series", "123", 2)
							dbPages.ShowXml("Action", "RESUME", 2)
							dbPages.ShowXml("RequestString", "MerchantNumber=1234567&SeriesID=123&Action=RESUME", 2)
							dbPages.ShowXml("RemoteIP", "123.123.123.123", 2)
						dbPages.ShowXml("/RecurringModify", "", 1, False)
					dbPages.ShowXml("/RecurringModifyLog", "", 0, False)
					%>
				</td>
			</tr>
			</table>	
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td>When request is invalid</td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td>Code<br /></td>
				<td>Reply code (see list below)<br /></td>
			</tr>
			<tr>
				<td>Message<br /></td>
				<td>Reply description<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					<%
						dbPages.ShowXml("Error", "", 0, False)
						dbPages.ShowXml("Code","12",1)
						dbPages.ShowXml("Message","Merchant company number is invalid",1)
						dbPages.ShowXml("/Error", "", 0, False)
					%>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Reply Codes<br /></td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="1">
			<tr><td><span>11</span> = Missing or invalid input<br /></td></tr>
			<tr><td><span>12</span> = Merchant company number is invalid<br /></td></tr>
			<tr><td><span>13</span> = Merchant is unauthorized to use this service<br /></td></tr>
			<tr><td><span>14</span> = Service not allowed from your IP Address<br /></td></tr>
			<tr><td><span>15</span> = MD5 Signature not correct<br /></td></tr>
			<tr><td><span>16</span> = The service is temporarily unavailable<br /></td></tr>
			<tr><td><span>17</span> = Merchant is closed or blocked<br /></td></tr>
			</table>
		</td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td>
			<br /><hr size="1" width="100%" noshade="noshade" />
			(1) Default time is 00:00:00 when hh:MM:ss is empty<br />
			(2) Default time is 23:59:59 when hh:MM:ss is empty<br />
			(4) Maximum records sent back is 1000<br />
			(5) In transaction with method other than credit card, CreditCard and Address sections are not displayed<br />
			(6) Contains positive number when relevant, otherwise zero<br />
			(7) Appears only when Type is CHB<br />
			(8) Appears only if RefTransID and RefTransType were passed in Silent Post<br />
		</td>
	</tr>
	</table>
</asp:Content>