<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage" MasterPageFile="~/Tmp_netpayintl/MasterPage.master" Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<script runat="server">
	Dim sSQL As String
	Dim langTxt As String = ""
	Dim langCode As Integer = 1
	Dim styleDir As String = ""
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		'Select Case Trim(Request("lang"))
		'	Case "spa" : langTxt = "Spanish" : langCode = 2 : styleDir = "ltr"
		'	Case "heb" : langTxt = "Hebrew" : langCode = 0 : styleDir = "rtl"
		'	Case Else : langTxt = "English" : langCode = 1 : styleDir = "ltr"
		'End Select
		If Not IsPostBack Then
			lstPrd.DataSource = Netpay.Bll.PublicPayment.GetChargeOptions(WebUtils.DomainHost, WebUtils.LoggedUser.Number, Netpay.CommonTypes.Language.English)
			lstPrd.DataBind()
		End If
	End Sub

	Protected Sub btnChange_Command(Sender As Object, e As CommandEventArgs)
		Dim chargeOption = Netpay.Bll.PublicPayment.GetChargeOption(WebUtils.CredentialsToken, e.CommandArgument)
		hdChargeOptionsID.Value = chargeOption.ID
		txtText.Text = chargeOption.Text
		txtDescription.Text = chargeOption.Description
		txtQtyStart.Text = chargeOption.QuantityMin
		txtQtyEnd.Text = chargeOption.QuantityMax
		txtQtyStep.Text = chargeOption.QuantityInterval
		txtQtyAvailable.Text = IIf(chargeOption.QuantityAvailable Is Nothing, "", chargeOption.QuantityAvailable)
		'dllInstallments.SelectedValue = chargeOption.PaymentsMax
		txtAmount.Text = chargeOption.Amount
		ddlCreditType.SelectedValue = chargeOption.CreditType
		ddlCurrency.SelectedValue = chargeOption.CurrencyID
	End Sub
	
	Protected Sub btnAdd_Command(Sender As Object, e As CommandEventArgs)
		Dim chargeOption As New Netpay.Infrastructure.VO.PublicPayChargeOptionVO
		chargeOption.ID = hdChargeOptionsID.Value.ToInt32(0)
		chargeOption.Text = txtText.Text
		chargeOption.Description = txtDescription.Text
		chargeOption.QuantityMin = txtQtyStart.Text.ToInt32(1)
		chargeOption.QuantityMax = txtQtyEnd.Text.ToInt32(1)
		chargeOption.QuantityInterval = txtQtyStep.Text.ToInt32(1)
		chargeOption.QuantityAvailable = txtQtyAvailable.Text.ToNullableInt32()
		If (chargeOption.QuantityMax < chargeOption.QuantityMin) Or _
			(chargeOption.QuantityInterval < 1) Or (chargeOption.QuantityInterval > chargeOption.QuantityMax) Or _
			(((chargeOption.QuantityMax - chargeOption.QuantityMin) / chargeOption.QuantityInterval) > 50) Then
			chargeOption.QuantityMin = 1
			chargeOption.QuantityMax = 1
			chargeOption.QuantityInterval = 1
		End If
		chargeOption.PaymentsMax = dllInstallments.SelectedValue.ToInt32(1)
		chargeOption.Amount = txtAmount.Text.ToDecimal(0)
		chargeOption.CreditType = ddlCreditType.SelectedValue
		chargeOption.CurrencyID = ddlCurrency.SelectedValue
		'Dim priority As Integer = txtPriority.Text.ToInt32(0)
		Netpay.Bll.PublicPayment.SaveChargeOption(WebUtils.CredentialsToken, chargeOption)
	End Sub
	
	Protected Sub btnDelete_Command(Sender As Object, e As CommandEventArgs)
		Dim chargeOption = Netpay.Bll.PublicPayment.GetChargeOption(WebUtils.CredentialsToken, e.CommandArgument)
		Dim imageName As string = MapPath("/GlobalData/PublicPageLogos/" & chargeOption.ImageFileName)
		If imageName <> "" Then If File.Exists(sSQL) Then System.IO.File.Delete(sSQL)
		Netpay.Bll.PublicPayment.DeleteChargeOption(WebUtils.CredentialsToken, e.CommandArgument)
	End Sub

	Protected Sub btnDeleteImage_Command(Sender As Object, e As CommandEventArgs)
		Dim chargeOption = Netpay.Bll.PublicPayment.GetChargeOption(WebUtils.CredentialsToken, e.CommandArgument)
		Dim imageName As string = MapPath("/GlobalData/PublicPageLogos/" & chargeOption.ImageFileName)
		If imageName <> "" Then If File.Exists(sSQL) Then System.IO.File.Delete(sSQL)
		chargeOption.ImageFileName = Nothing
		Netpay.Bll.PublicPayment.SaveChargeOption(WebUtils.CredentialsToken, chargeOption)
	End Sub

	Protected Sub btnUpdateDefault_Command(Sender As Object, e As CommandEventArgs)
		'Dim nDefault As Integer? = Request("IsDefault").ToNullableInt32()
		'If nDefault Is Nothing Then Exit Sub
		'Netpay.Bll.PublicPayment.DeleteChargeOption(WebUtils.CredentialsToken, e.CommandArgument)
	End Sub
	
	Protected Sub Product_OnItemDataBound(Sender As Object, e As RepeaterItemEventArgs)
		If e.Item.DataItem Is Nothing Then Exit Sub
		Dim pppParams As String = "merchantID=" & WebUtils.LoggedUser.Number & "&item=" & CType(e.Item.DataItem, Netpay.Infrastructure.VO.PublicPayChargeOptionVO).ID
		CType(e.Item.FindControl("imgQrCode"), Image).ImageUrl = String.Format("{0}QrCodes.asmx/RenderGenerateCode?target=PublicPaymentPageV2&urlParams={1}", WebUtils.CurrentDomain.WebServicesUrl, HttpUtility.UrlEncode(pppParams))
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<script language="JavaScript" type="text/javascript">
		var offset = <%=IIF(langCode = 1, "250", "0")%>;
		function showImg(optionId) {
			var obj = document.all('imgDiv' + optionId);
			obj.style.left = window.event.clientX - offset; //(window.event.clientX - document.body.scrollLeft);
			obj.style.top = (window.event.clientY + document.body.scrollTop);
			obj.style.display = 'block';	
		}
		function hideImg(optionId) {
			var obj = document.all('imgDiv' + optionId);
			obj.style.display = 'none';	
		}	
		function openUploadWin(optionId) {
			placeLeft = ((screen.width / 2) - 200);
			placeTop = ((screen.height / 2) - 60);
			window.open("LoadImage.aspx?ID=" + optionId + "&lang=<%=trim(request("lang"))%>&langCode=<%=langCode%>", "uploadWin" ,  "top=" + placeTop + ", left=" + placeLeft + ", height=120,width=400,status=no,toolbar=no,menubar=no,location=no");
		}	
		function IsKeyDigit() {
			var nKey=event.keyCode;
			if ((nKey==8)|(nKey==9)|(nKey==13)|(nKey==35)|(nKey==36)|(nKey==37)|(nKey==39)|(nKey==46)|(nKey==48)|(nKey==49)|(nKey==50)|(nKey==51)|(nKey==52)|(nKey==53)|(nKey==54)|(nKey==55)|(nKey==56)|(nKey==57)|(nKey==96)|(nKey==97)|(nKey==98)|(nKey==99)|(nKey==100)|(nKey==101)|(nKey==102)|(nKey==103)|(nKey==104)|(nKey==105)|(nKey==110)|(nKey==190)) { return true; };
			return false;
		}
		function ShowPayments()	{
			var frmThis=event.srcElement.form;
			if (frmThis.CreditType.value==8 || frmThis.CreditType.value==6) {
				frmThis.Payments.disabled=false;
			}
			else {
				frmThis.Payments.disabled=true;
			}
		}
		function CheckForm()
		{
			var frmThis=event.srcElement.form;
			if (frmThis.Text.value=='') {
				alert('Please enter Payment description');
				frmThis.Text.focus();
				return false;
			}
			var nQtyStart = new Number (frmThis.QtyStart.value);
			var nQtyEnd = new Number (frmThis.QtyEnd.value);
			var nQtyStep = new Number (frmThis.QtyStep.value);
			if (nQtyEnd < nQtyStart) {
				alert('The Quantity End field can not be greater than the Start field');
				frmThis.QtyEnd.focus();
				return false;
			}		
			if (nQtyStep < 1) {
				alert('Quantity Step field must be greater than 1');
				frmThis.QtyStep.focus();
				return false;
			}			
			if (nQtyStep > nQtyEnd) {
				alert('Quantity Step field must be greater than the End field');
				frmThis.QtyStep.focus();
				return false;
			}		
			if (((nQtyEnd - nQtyStart) / nQtyStep) > 50) {
				alert('The total of generated quantity options can not exceed 50');
				frmThis.QtyStep.focus();
				return false;
			}	
			if (frmThis.Amount.value=='') {
				alert('Please enter debit amount');
				frmThis.Amount.focus();
				return false;
			}
			return true;
		}
	</script>
	
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">PUBLIC PAYMENT PAGE</span>
			<span class="PageSubHeading">- PAYMENT OPTIONS (<%= langTxt%>)</span><br />
		</td>
		<td align="right">[<a class="faq" href="PublicPage_AdminAppear.aspx?lang=<%= trim(request("lang")) %>">back to page management</a>]<br /></td>
	</tr>
	<tr>
		<td colspan="2">
			<custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading" colspan="2"><br />Adding payment options:<br /></td></tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td colspan="2" style="border:1px dashed #c0c0c0; padding-top:3px; padding-bottom:3px;">
			<asp:HiddenField runat="server" ID="hdChargeOptionsID" Value="" />
			<input type="hidden" name="Lang" value="<%= request("lang").ToSql(True) %>" />
			<table width="98%" align="center" border="0" cellspacing="0" cellpadding="1">
			<tr>
				<td>
					<table border="0" cellpadding="3">
					<tr>
						<td>
							Pos.<br />
							<asp:TextBox runat="server" CssClass="input2" size="2" style="font-size:12px;" onkeydown="return IsKeyDigit();" id="txtPriority" Text="1" />
						</td>	
						<td nowrap>
							Description (In english)<br />
							<asp:TextBox runat="server" CssClass="input2" size="30" dir="<%= styleDir %>" style="font-size:12px;" id="txtText" /><br />
						</td>	
						<td>&nbsp;</td>	
						<td>Quantity *<br />Start <asp:TextBox runat="server" CssClass="input2" size="2" onkeydown="return IsKeyDigit();" id="txtQtyStart" Text="1" /></td>	
						<td><br />End <asp:TextBox runat="server" CssClass="input2" size="2"  onkeydown="return IsKeyDigit();" id="txtQtyEnd" Text="1" /></td>			
						<td><br />Step <asp:TextBox runat="server" CssClass="input2" size="2" onkeydown="return IsKeyDigit();" id="txtQtyStep" Text="1" /></td>
						<td><br />Available <asp:TextBox runat="server" CssClass="input2" size="2" onkeydown="return IsKeyDigit();" id="txtQtyAvailable" /></td>
					</tr>
					</table>
				</td>								
			</tr>
			<tr>
				<td>
					<table border="0" cellpadding="3">
					<tr>						
						<td>
							Payment type<br />
							<netpay:CreditTypeDropDown runat="server" ID="ddlCreditType" />
						</td>
						<td>
							Payments<br /> <netpay:NumericDropDown runat="server" id="dllInstallments" MinValue="1" MaxValue="12" />
						</td>
						<td>
							Amount<br />
							<asp:TextBox runat="server" id="txtAmount" onkeydown="return IsKeyDigit();" class="input2" />
						</td>
						<td>
							Currency<br />
							<netpay:CurrencyDropDown ID="ddlCurrency" runat="server" EnableBlankSelection="false" />
						</td>
						<td align="right" width="20%">
							<br /><asp:Button runat="server" Text=" Add " OnCommand="btnAdd_Command" CssClass="button1" style="font-size:11px;" OnClientClick="return CheckForm();" /><br />
						</td>
					</tr>
					</table>
				</td>		
			</tr>
			<tr>
				<td>
					<table border="0" cellpadding="3" width="100%">
						<tr>
							<td>
								Description<br />
								<asp:TextBox runat="server" id="txtDescription" Width="100%" class="input2" TextMode="MultiLine" Rows="2" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="padding-top:8px;">
					* Quantity options list is defined by 3 fields: &nbsp; The Start field defines the list starting number,
					The End field defines the list ending number, The Step field defines the step between each number in the list.<br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td colspan="2" class="SecHeading"><br />Payment option added:<br /></td></tr>
	<tr><td height="10"></td></tr>
		<tr>
			<td colspan="2">
				<asp:Repeater runat="server" ID="lstPrd" OnItemDataBound="Product_OnItemDataBound" >
					<HeaderTemplate>
						<table width="100%" border="0" cellspacing="2" cellpadding="1">
					</HeaderTemplate>
					<ItemTemplate>
						<tr height="40">
							<td bgcolor="#f5f5f5"><%'# Eval("Priority")%></td>
							<td bgcolor="#f5f5f5"><input type="Radio" <%# IIF(Eval("IsDefault"), "checked", "")%> name="IsDefault" value="<%# Eval("ID") %>" /></td>
							<td bgcolor="#f5f5f5">
								<span><%# Eval("Text")%>&nbsp;-&nbsp;</span>
								<%# CType(Eval("Amount"), Decimal).ToAmountFormat(WebUtils.DomainHost, CType(Eval("CurrencyID"), Integer))%>
								&nbsp;&nbsp;<netpay:NumericDropDown runat="server" MinValue='<%#Eval("QuantityMin")%>' MaxValue='<%#Eval("QuantityMax")%>' ValueStep='<%#Eval("QuantityInterval")%>' />
								in <netpay:NumericDropDown runat="server" MinValue="1" MaxValue='<%#Eval("PaymentsMax")%>' />&nbsp;&nbsp;
								<%# IIf(Eval("CreditType") = 6, "(Special credit)&nbsp;", IIf(Eval("CreditType") = 2, "(Credit)&nbsp;", ""))%>
							</td>
							<td bgcolor="#f5f5f5" valign="middle" align="center">
								<asp:MultiView runat="server" ID="ImageView" ActiveViewIndex='<%# IIF(System.IO.File.Exists(Server.MapPath("/GlobalData/PublicPageLogos/" & Eval("ImageFileName"))), 0, 1) %>' >
									<asp:View ID="View1" runat="server">
										<img src="/GlobalData/PublicPageLogos/<%# Trim(Eval("ImageFileName"))%>" alt="Item Image" width="30" height="30" onmouseover="showImg(<%# Eval("ID") %>)" onmouseout="hideImg(<%# Eval("ID") %>)" />
										<div id="imgDiv<%# Eval("ID") %>" style="display:none; position:absolute;">
											<img src="/GlobalData/PublicPageLogos/<%# Trim(Eval("ImageFileName"))%>" alt="Item Image" border="1" />
										</div>
										<br />
										<asp:LinkButton runat="server" OnCommand="btnDeleteImage_Command" CommandArgument='<%#Eval("ID")%>' CssClass="faq" OnClientClick="if(!confirm('Are you sure ?')) return false;" Text="Remove" />
									</asp:View>
									<asp:View ID="View2" runat="server">
										<a href="javascript:openUploadWin(<%#Eval("ID")%>);" class="faq" style="font-size:10px;">Add image</a>
									</asp:View>
								</asp:MultiView>
							</td>								
							<td bgcolor="#f5f5f5" valign="middle" align="center">
								<asp:Image runat="server" ID="imgQrCode" />
							</td>
							<td bgcolor="#f5f5f5" width="7%" align="center">
								<asp:LinkButton runat="server" OnCommand="btnChange_Command" CommandArgument='<%#Eval("ID")%>' CssClass="button1" Text="Change" />
								<br />
								<asp:LinkButton runat="server" OnCommand="btnDelete_Command" CommandArgument='<%#Eval("ID")%>' CssClass="button1" OnClientClick="if(!confirm('Are you sure ?')) return false;" Text="Delete" />
							</td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						</table>
					</FooterTemplate>
				</asp:Repeater>
				<asp:Button runat="server" OnCommand="btnUpdateDefault_Command" Text="Update" />
			</td>
		</tr>
	</table>
</asp:Content>