<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="TRANSACTION HISTORY - MESSAGE FIELDS" %>

<script runat="server">
    Dim xmlData As String
    Public Sub Page_Load()
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
		<tr><td class="SecHeading">Service URL<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td><%=CurrentDomain.WebServicesUrl %>Merchants.asmx/TransactionHistory<br /></td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">Request Formats<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr><td>
            The service accept calls in the following protocols:<br />
            SOAP, XML, HTTP POST and JSON<br /><br />
            for SOAP calls you may use the wsdl declaration in the following address:<br />
            <a href="<%=CurrentDomain.WebServicesUrl %>Merchants.asmx?WSDL"><%=CurrentDomain.WebServicesUrl %>Merchants.asmx?WSDL</a>
            <br /><br />
        </td></tr>
        <tr><td style="border:1px dashed #c0c0c0; padding:5px; background-color:#f7f7f7;">
<pre>
&lt;TransactionHistory xmlns=&quot;http://netpay-intl.com/&quot;&gt;
      &lt;credentialsToken&gt;string&lt;/credentialsToken&gt;
&lt;/TransactionHistory&gt;
</pre>
        </td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field</th>
						<th valign="bottom">Description</th>
						<th width="46">Max<br />Length</th>
						<th width="80" valign="bottom">Required</th>
					</tr>
					<tr>
						<td>credentialsToken</td>
						<td>The credentials token recieved from the login service</td>
						<td>36</td>
						<td>Yes</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr><td><br /></td></tr>
		<tr><td class="SecHeading">Response Format<br /></td></tr>
		<tr><td><br /></td></tr>
        <tr><td style="border:1px dashed #c0c0c0; padding:5px; background-color:#f7f7f7;">
<pre>
&lt;TransactionHistoryResponse xmlns=&quot;http://netpay-intl.com/&quot;&gt;
    &lt;TransactionHistoryResult&gt;
		  &lt;TransactionInfo&gt;
			&lt;ID&gt;1234567&lt;/ID&gt;
			&lt;Amount&gt;1.5000&lt;/Amount&gt;
			&lt;Card&gt;Visa .... 0000&lt;/Card&gt;
			&lt;InsertDate&gt;2013-10-02T11:08:49&lt;/InsertDate&gt;
			&lt;Currency&gt;EUR&lt;/Currency&gt;
			&lt;Comment&gt;Transaction info&lt;/Comment&gt;
		  &lt;/TransactionInfo&gt;
		  &lt;TransactionInfo&gt;
			&lt;ID&gt;1234568&lt;/ID&gt;
			&lt;Amount&gt;1.5000&lt;/Amount&gt;
			&lt;Card&gt;Visa .... 0000&lt;/Card&gt;
			&lt;InsertDate&gt;2013-10-02T11:11:49&lt;/InsertDate&gt;
			&lt;Currency&gt;EUR&lt;/Currency&gt;
			&lt;Comment&gt;Transaction info&lt;/Comment&gt;
		  &lt;/TransactionInfo&gt;
    &lt;/TransactionHistoryResult&gt;
&lt;/TransactionHistoryResponse&gt;
</pre>
        </td></tr>
		<tr><td><br /></td></tr>
	</table>
</asp:Content>