<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
    Dim sBgColor as String = ""
    Dim IsCustomerPurchasePayerID,IsApprovalOnly,IsAllowRecurring As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If CurrentDomain.IsHebrewVisible Then
            lblHebrewFile1.Text = "Hebrew page - " & CurrentDomain.ProcessURL & "window_charge_heb.asp"
        Else
            lblHebrewFile1.Text = ""
        End If
        rptCurrency.DataBind()
        If WebUtils.IsLoggedin Then
            Dim sSQL As String = "SELECT IsCustomerPurchasePayerID, IsApprovalOnly, tblMerchantRecurringSettings.IsEnabled as IsAllowRecurring " &
                " FROM tblCompany Left Join tblMerchantRecurringSettings ON(tblMerchantRecurringSettings.MerchantID = tblCompany.ID)" &
                " WHERE ID = " & Merchant.ID
            Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
            If iReader.Read() Then
                IsCustomerPurchasePayerID = iReader("IsCustomerPurchasePayerID")
                If Not IsCustomerPurchasePayerID Then
                    lblIsCustomerPurchasePayerID.Text = "<span style=""color:maroon;"">This option is not active - Contact support to activate.</span><br />"
                Else
                    lblIsCustomerPurchasePayerID.Visible = False
                End If

                IsApprovalOnly = iReader("IsApprovalOnly")
                If Not IsApprovalOnly Then
                    lblIsApprovalOnly.Text = "<span style=""color:maroon;"">Authorization only is not available, contact " & WebUtils.CurrentDomain.BrandName & " to activate</span><br />"
                Else
                    lblIsApprovalOnly.Text = ""
                    lblIsApprovalOnly.Visible = False
                End If
                IsAllowRecurring = dbPages.TestVar(iReader("IsAllowRecurring"), False)
            End If
            iReader.Close()
        Else
            lblIsApprovalOnly.Text = "<span style=""color:maroon;"">Not Applicable to Guest Login</span><br/>"
            lblIsCustomerPurchasePayerID.Text = "<span style=""color:maroon;"">Not Applicable to Guest Login</span><br/>"
        End If
    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">HOSTED PAYMENT PAGE</span>
			<span class="PageSubHeading">- MESSAGE FIELDS</span><br />
			<custom:AuthAlert PropertyToCheck="HostedPayment" runat="server" />
		</td>
	</tr>	
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">PAGE URL<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			English page - <%=CurrentDomain.ProcessURL%>window_charge_eng.asp<br />
			French page - <%=CurrentDomain.ProcessURL%>window_charge_fre.asp<br />
			Spanish page - <%=CurrentDomain.ProcessURL%>window_charge_spa.asp<br />
			Italian page - <%=CurrentDomain.ProcessURL%>window_charge_ita.asp<br />
			<asp:Label ID="lblHebrewFile1" runat="server" />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Request Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
				<th width="46">Max<br />Length<br /></th>
				<th width="80" valign="bottom">Required <sup>1</sup><br /></th>
			</tr>
			<tr>
				<td>CompanyNum<br /></td>
				<td>Your company number - <%=Merchant.Number%><br /></td>
				<td>7</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>TransType<br /></td>
				<td>
					0 = Debit Transaction (Default when field is empty)<br />
					1 = Authorization only<br />
					<!--<%' If not IsApprovalOnly Then %><span style="color:maroon;">Authorization only is not available, contact <%'= WebUtils.CurrentDomain.BrandName %> to activate</span><br /><%' End If %>-->
					<asp:Label ID="lblIsApprovalOnly" runat="server" />
				</td> 
				<td>1</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>ReplyURL<br /></td>
				<td>URL address for replying with approval code, usually at your domain name.<br /></td>
				<td>100</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>ReplyType<br /></td>
				<td>
					Determines where the reply page will will launch after transaction process.<br />
					0 = At the same page from which the window was launched (Automatically after process)<br />
					1 = At the small window where transaction was processed (after the buyer clicks submit)
				</td>
				<td>1</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>TypeCredit <sup>2</sup><br /></td>
				<td>1 = Debit<br />8 = Installments<br /></td>
				<td>1</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Payments<br /></td>
				<td>Number of installments, 1 for regular transaction<br /></td>
				<td>2</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Amount<br /></td>
				<td>Amount to be charge<br />(example: 199.95)<br /></td>
				<td>20</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Currency<br /></td>
				<td>
					<asp:Repeater ID="rptCurrency" DataSource="<%# dbPages.Currencies  %>" runat="server">
						<ItemTemplate>
							<asp:Literal runat="server" Text='<%#Eval("ID")%>' /> = <asp:Literal runat="server" Text='<%#Eval("IsoCode")%>' />
							(<asp:Literal runat="server" Text='<%#Eval("Name")%>' />)
						</ItemTemplate>
						<SeparatorTemplate><br /></SeparatorTemplate>
					</asp:Repeater>
				</td>
				<td>1</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Signature<br /></td>
				<td>
					Signature for verifying the authenticity of the request parameters.<br />
					Field values to use: <i>CompanyNum + PersonalHashKey</i><br /><br />
					Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
				</td>
				<td>30</td>
				<td>Optional</td>
			</tr>			
			<tr>
				<td>PayFor<br /></td>
				<td>Text shown to buyer in payment window, Usually description of purchase (Cart description, Product name)</td>
				<td>40</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>WalletOnly<br /></td>
				<td>Set only registered members can make a charge<br />1 = Yes, 0 = No</td>
				<td>1</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>Order<br /></td>
				<td>Unique text used to defer one transaction from another<br /></td>
				<td>10</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>Comment<br /></td>
				<td>Optional text used mainly to describe the transaction</td>
				<td>255</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>PayerID<br /></td>
				<td>
					<asp:Label ID="lblIsCustomerPurchasePayerID" runat="server" />
					<!--<%' If NOT IsCustomerPurchasePayerID Then %><span style="color:maroon;">This option is not active - Contact support to activate.</span><br /><%' End If %>-->
					Used for second time buyers where client exist in sellers database and seller wishes to automatically fill the page with payer's credit card information.<br />
				</td>
				<td>10</td>
				<td>Optional</td>
			</tr>
			</table>
		</td>	
	</tr>
	<tr><td><br /></td></tr>
	<tr><td class="FieldSecHeading">Automatic filling of form data:<br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<td width="110">ccHolderName<br /></td>
				<td>Cardholder full name</td>
				<td width="46">50</td>
				<td width="80">Optional</td>
			</tr>
			<tr>
				<td>ccHolderIdNum<br /></td>
				<td>Government issued ID number</td>
				<td>9</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>ccHolderPhoneNum<br /></td>
				<td>Cardholder phone number</td>
				<td>15</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>ccHolderEmail<br /></td>
				<td>Cardholder Email address</td>
				<td>50</td>
				<td>Optional</td>
			</tr>
			</table>
		</td>	
	</tr>
	<%
        If IsAllowRecurring Or Not WebUtils.IsLoggedin Then
		%>
		<tr><td><br /></td></tr>
		<tr><td><span class="FieldSecHeading">Recurring:</span> &nbsp;&nbsp; [ <a href="" onclick="var winRecurring=window.open('Recurring_HelpWin.htm','fraRecurring','width=400, height=380, resizable=1, scrollbars=1');return false;">SAMPLES</a> ]<br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
				<tr>
					<td width="110">RecurringMode<br /></td>
					<td>Mode: 1 = Day, 2 = Week, 3 = Month, 4 = Year<br /></td>
					<td width="46">1</td>
					<td width="80">Optional</td>
				</tr>
				<tr>
					<td>RecurringCycle<br /></td>
					<td>Cycle: Time gap according to the mode<br /></td>
					<td>3</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>RecurringCount<br /></td>
					<td>Count: Number of charges<br /></td>
					<td>3</td>
					<td>Optional</td>
				</tr>
				</table>
			</td>
		</tr>
		<%
	End if
	%>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Response Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td>Reply<br /></td>
				<td>Transaction authorization (see list below)<br /></td>
			</tr>
			<tr>
				<td>TransID<br /></td>
				<td>Transaction number<br /></td>
			</tr>
			<tr>
				<td>Date<br /></td>
				<td>Transaction time and date<br /></td>
			</tr>
			<tr>
				<td>Amount<br /></td>
				<td >Transaction amount<br /></td>
			</tr>
			<tr>
				<td>TypeCredit<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>Payments<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>Currency<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>PayFor<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>PayerID<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>Order<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>Comment<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					Reply=000&TransID=12345&Date=01/01/2000 00:00:01&TypeCredit=1
					&Payments=1&Amount=10.99&Currency=0&PayFor=Cart 1234&PayerID=1234
					&Order=A1234567&Comment=White box
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Reply Codes<br /></td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="1">
			<tr>
				<td><span>000</span> = Authorised transaction<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><span>001</span> = Transaction accepted, waiting authorization<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><span>xxx</span> = Transaction was not Authorized, see unauthorized comment explanation<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td>
			- For a more detail list of reply codes <a href="List_ReplyCodes.aspx">click here</a> 
			<a href="List_ReplyCodes.aspx"><img src="/NPCommon/Images/iconNewWinRight.GIF" border="0" align="middle" /></a><br />
		</td>
	</tr>
	<tr>
		<td>
			<br /><hr size="1" width="100%" noshade="noshade" />
			(1) Some field requirements are dependent on merchant configuration and can be change on your request<br />
			(2) First option is default when field is empty<br />
		</td>
	</tr>
	</table>
		
</asp:Content>

