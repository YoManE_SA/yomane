<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - STORING CREDIT CARDS" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
	Dim sBrandName as String = CurrentDomain.BrandName
	Dim sProcessURL as String = CurrentDomain.ProcessURL
	Dim ForceSignature As Boolean = False

	Protected Sub Page_Load()
		If WebUtils.IsLoggedin Then ForceSignature = dbPages.TestVar(dbPages.ExecScalar("SELECT ForceCCStorageMD5 FROM tblCompany WHERE ID = " & Merchant.ID), False)
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">STORING CREDIT CARDS</span>
			<span class="PageSubHeading">	- MESSAGE FIELDS</span><br />
			<custom:AuthAlert PropertyToCheck="StoreCreditCards" runat="server" />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">PAGE URL<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td><%=sProcessURL %>store_card.asp<br /></td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Request Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
				<tr>
					<th width="110" valign="bottom">Field<br /></th>
					<th valign="bottom">Description<br /></th>
					<th width="46">Max<br />Length<br /></th>
					<th width="80" valign="bottom">Required<br /></th>
				</tr>
				<tr>
					<td>CompanyNum<br /></td>
					<td>Your company number - <%=MerchantNumber%><br /></td>
					<td>7</td>
					<td>Yes</td>
				</tr>
				<tr>
					<td>CardNum</td>
					<td>Credit card number to be charged </td>
					<td>20</td>
					<td>Yes</td>
				</tr>
				<tr>
					<td>ExpMonth</td>
					<td>Expiration month (MM) </td>
					<td>2</td>
					<td>Yes</td>
				</tr>
				<tr>
					<td>ExpYear</td>
					<td>Expiration year (YYYY) </td>
					<td>4</td>
					<td>Yes</td>
				</tr>
				<tr>
					<td>CHFullName</td>
					<td>Cardholder name as it appears on the card </td>
					<td>50</td>
					<td>Yes</td>
				</tr>
				<tr>
					<td>CVV2</td>
					<td>3 digit number from back of the credit card</td>
					<td>5</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>CHEmail</td>
					<td>Cardholder email address</td>
					<td>50</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>CHPersonalNum</td>
					<td>Cardholder Driver id or social security number</td>
					<td>20</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>CHPhoneNumber</td>
					<td>Cardholder phone number</td>
					<td>20</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>Comment</td>
					<td>Usually used to display product name in <asp:Label ID="lblCompany1" runat="server" /> reporting system </td>
					<td>255</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>billingStreet1</td>
					<td>Client billing address line 1</td>
					<td>50</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>billingStreet2</td>
					<td>Client billing address line 2</td>
					<td>50</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>billingCity</td>
					<td>Client billing city</td>
					<td>20</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>billingZipcode</td>
					<td>Client billing zip code</td>
					<td>20</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>billingState</td>
					<td>Client billing state in 2 char ISO code</td>
					<td>2</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>billingCountry</td>
					<td>Client billing country in 2 char ISO code</td>
					<td>2</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>Signature<br /></td>
					<td>
						Signature for verifying the authenticity of the request parameters.<br />
						Field values to use: <i>CompanyNum + PersonalHashKey</i><br /><br />
						Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
					</td>
					<td>30</td>
					<td><%= IIf(ForceSignature,"Yes","Optional") %></td>
				</tr>			
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Response Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td>ReplyCode<br /></td>
				<td>Reply code (see list below)<br /></td>
			</tr>
			<tr>
				<td>ReplyDesc<br /></td>
				<td>Reply description<br /></td>
			</tr>
			<tr>
				<td>CardId<br /></td>
				<td>Stored credit card ID number, to be passed as ccStorageID in <a href="SilentPost_Cc.aspx">Silent Post</a> request<br /></td>
			</tr>
			<tr>
				<td>InsertDate<br /></td>
				<td>Exact date and time at the server<br /></td>
			</tr>
			<tr>
				<td>Comment<br /></td>
				<td>A returned field from the request<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					ReplyCode=00&ReplyDesc=Card has been stored successfully<br />
					&CardId=12345&InsertDate=01/01/2000 00:00:01&Comment=test 123
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Reply Codes<br /></td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<netpay:GlobalDataGroupValueTable GlobalDataGroup="CcStorageReturnCode" runat="server" />
		</td>
	</tr>
	</table>

</asp:Content>
