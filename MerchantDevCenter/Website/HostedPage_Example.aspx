<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
	Function GetLanguageSelectHTML(ByVal sLanguage As String, ByVal sPrefix As String, Optional ByVal bChecked As Boolean = False) As String
		Return "<input class=""option"" type=""radio"" name=""WinLang"" style=""background-color:#ffffff;"" onclick=""frmCodePay.action='" & CurrentDomain.ProcessURL & "window_charge_" & sPrefix & ".asp';"" value=""_" & sPrefix & """ " & IIf(bChecked, "checked=""checked"" ", String.Empty) & "/> " & sLanguage & "<br>"
	End Function

	Function GetLanguageSelectHTML(ByVal sLanguage As String, Optional ByVal bChecked As Boolean = False) As String
		Return GetLanguageSelectHTML(sLanguage, sLanguage.Substring(0, 3).ToLower(), bChecked)
	End Function

	sub Page_Load
		lblReplyURL.Text = "<input class=""wide"" name=""ReplyURL"" value=""" & CurrentDomain.ProcessURL & "CheckReply.asp"" /><br />"
		lblEnglishOption.Text = GetLanguageSelectHTML("English", True)
		lblFrenchOption.Text = GetLanguageSelectHTML("French")
		lblSpanishOption.Text = GetLanguageSelectHTML("Spanish")
		lblItalianOption.Text = GetLanguageSelectHTML("Italian")
		lblQueryProcessURL.Text = "<input type=""hidden"" id=""hidQueryProcessURL"" value=""" & CurrentDomain.ProcessURL & """ />"
		lblQueryCompany.Text = "<input type=""hidden"" id=""hidQueryCompany"" value=""" & CurrentDomain.BrandName & """ />"
		if CurrentDomain.IsHebrewVisible then
			lblCurrencySelect.Text = "<select name=""Currency""><option value=""0"">NIS</option><option value=""1"" selected>Dollar</option></select>"
			lblHebrewOption.Text = GetLanguageSelectHTML("Hebrew")
		else
			lblCurrencySelect.Text = "<select name=""Currency""><option value=""1"" selected>Dollar</option></select>"
			lblHebrewOption.Text = ""
		End If
	end sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	</form>
	<asp:Label ID="lblQueryProcessURL" runat="server" />
	<asp:Label ID="lblQueryCompany" runat="server" />
	<script type="text/javascript" language="JavaScript">
		function ShowQuery()
		{
			var formIndex;
			for (var i = 0; i < frmCodePay.WinLang.length; i++) if (frmCodePay.WinLang[i].checked) sWinLang = frmCodePay.WinLang[i].value.substr(1, 3);

			var sQuery = '<a href="' + hidQueryProcessURL.value + 'window_charge_' + sWinLang + '.asp' + (sWinLang.toLowerCase() == "heb" ? "x" : "") + '?';
			for (formIndex = 0; formIndex < 10; formIndex++)
			{
				if (document.frmCodePay[formIndex].type != 'button')
				{
					if (formIndex > 0)
					{
						sQuery = sQuery + '&';
					}
					sQuery = sQuery + document.frmCodePay[formIndex].name + '=' + document.frmCodePay[formIndex].value;
				}
			}

			sQuery = sQuery + '" target="fraShoppingPurchase"'
			sQuery = sQuery + ' onclick="window.open(\'\', this.target, \'scrollbars=1, width=490, height=500, resizable=0, Status=1, top=200, left=250\');"'
			sQuery = sQuery + '>Pay by <b>' + hidQueryCompany.value + '</b></a>'
			document.getElementById("txaQuery").value = sQuery;
			document.getElementById("txaQuery").style.display = "";
			document.getElementById("btnHideQuery").style.display = "";

			//window.prompt('Sample code for openning hosted payment page',sQuery);
			return false;
		}

		function HideQuery()
		{
			document.getElementById("txaQuery").value = "";
			document.getElementById("txaQuery").style.display = "none";
			document.getElementById("btnHideQuery").style.display = "none";
		}

		function IsKeyDigit()
		{
			var nKey = event.keyCode;
			if ((nKey == 8) | (nKey == 9) | (nKey == 13) | (nKey == 35) | (nKey == 36) | (nKey == 37) | (nKey == 39) | (nKey == 46) | (nKey == 48) | (nKey == 49) | (nKey == 50) | (nKey == 51) | (nKey == 52) | (nKey == 53) | (nKey == 54) | (nKey == 55) | (nKey == 56) | (nKey == 57) | (nKey == 96) | (nKey == 97) | (nKey == 98) | (nKey == 99) | (nKey == 100) | (nKey == 101) | (nKey == 102) | (nKey == 103) | (nKey == 104) | (nKey == 105) | (nKey == 110) | (nKey == 190)) { return true; };
			return false;
		}

		function ShowPayments()
		{
			if (document.frmCodePay.CreditType.value == 8 || document.frmCodePay.CreditType.value == 6)
			{
				document.frmCodePay.Payments.disabled = false;
			}
			else
			{
				document.frmCodePay.Payments.disabled = true;
				document.frmCodePay.Payments.selectedIndex = 0;
			}
		}

		function DirectForm(frm)
		{
			frm.action = "<%=CurrentDomain.ProcessURL%>" + "window_charge";
			for (var i = 0; i < frm.WinLang.length; i++) if (frm.WinLang[i].checked) frm.action += frmCodePay.WinLang[i].value + ".asp" + (frmCodePay.WinLang[i].value.toLowerCase() == "_heb" ? "x" : "");
		}
	</script>
	<style type="text/css">
		input.wide
		{
			width:400px;
		}
		input.option
		{
			style="vertical-align:middle;"
		}
	</style>
	<form id="frmCodePay" name="frmCodePay" method="post" action="<%= WebUtils.CurrentDomain.ProcessUrl %>shopping_purchase_RegularPay_eng.asp" target="fraShoppingPurchase" onsubmit="DirectForm(this);window.open('', this.target, 'width=490, height=510, resizable=1, scrollbars=1, status=1');">
	<input type="Hidden" name="PayerID" value="" />
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">HOSTED PAYMENT PAGE</span>
			<span class="PageSubHeading">- EXAMPLE</span><br />
			<custom:AuthAlert ID="AuthAlert1" PropertyToCheck="HostedPayment" runat="server" />
		</td>
	</tr>		
	<tr>
		<td>
			<br />
			The following form launches a window with the hosted payment page helping you to see and experience this method of payment.<br />
			<br /><br />
		</td>
	</tr>
	<tr><td class="SecHeading">Request data<br /><br /></td></tr>
	<tr>
		<td valign="top">
			<table class="FormTable" border="0" cellspacing="0" cellpadding="1">
			<tr>
			    <td>CompanyNum<br /></td>
			    <td><input class="wide" name="CompanyNum" value="<%= MerchantNumber %>" /><br /></td>
			</tr>
			<tr>
				<td>ReplyURL<br /></td>
				<td><asp:Label ID="lblReplyURL" runat="server" /></td>
			</tr>
			<tr>
				<td>ReplyType<br /></td>
				<td><input class="wide" name="ReplyType" value="1" /><br /></td>
			</tr>
			<tr>
				<td>PayFor<br /></td>
				<td><input class="wide" name="PayFor" value="Purchase" /><br /></td>
			</tr>
			<tr>
				<td>Comment<br /></td>
				<td><input class="wide" name="Comment" value="Testing hosted payment page" /><br /></td>
			</tr>
			<tr>
				<td>TypeCredit<br /></td>
				<td>	
					<select name="CreditType" onchange="ShowPayments();">
						<option value="1" selected="selected">Regular</option>
						<option value="8">Installments</option>
						<option value="0">Refund</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Payments<br /></td>
				<td>
					<select name="Payments" disabled="disabled">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Amount<br /></td>
				<td><input onkeydown="return IsKeyDigit();" name="Amount" value="1.50"><br /></td>
			</tr>
			<tr>
				<td>Currency<br /></td>
				<td><asp:Label ID="lblCurrencySelect" runat="server" /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Page language:<br /><br /></td></tr>	
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<asp:Label ID="lblEnglishOption" runat="server" />
					<asp:Label ID="lblFrenchOption" runat="server" />
					<asp:Label ID="lblSpanishOption" runat="server" />
					<asp:Label ID="lblItalianOption" runat="server" />
					<asp:Label ID="lblHebrewOption" runat="server" />
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td>
					<input type="submit" value="Open Window" style="background-color:white;font-size:10px;width:100px;cursor:pointer;" /> &nbsp;&nbsp;
					See and understand what is "Hosted Payment Page".
				</td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td>
					<input type="Button" onclick="ShowQuery();" value="Generate Link" style="background-color:white;font-size:10px;width:100px;cursor:pointer;" /> &nbsp;&nbsp;
					Generate an Html snippet for pasting into a web page.
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td>
					<textarea readonly="readonly" id="txaQuery" style="border:1px dashed #c0c0c0;background-color:#f7f7f7;display:none;overflow:auto;width:550px;height:115px;font:normal 12px Courier New, Monospace;"></textarea>
					<br />
					<input id="btnHideQuery" type="Button" onclick="HideQuery();" value="Hide Link" style="display:none;background-color:white;font-size:10px;width:115px;cursor:pointer;" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
	</form>
		
</asp:Content>