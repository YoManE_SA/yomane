<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - CREDIT CARD - SILENT POST" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>

<script runat="server">
    Dim sBrandName As String = CurrentDomain.BrandName
    Dim sProcessURL As String = CurrentDomain.ProcessURL

    Dim IsRemoteCharge, IsRemoteChargePersonalNumber, IsRemoteChargeCVV2, IsRemoteChargePhoneNumber, _
    IsRemoteChargeEmail, IsApprovalOnly, IsBillingAddressMust, IsAllowRecurring, IsUseFraudDetectionMaxMind, IsRefund As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        litRefTransType.Visible = Authorization.IsAllowed(AuthorizationProperty.AllowRemotePull)
        If CurrentDomain.IsHebrewVisible Then
            lblStates1.Text = "|&nbsp; <a href=""List_IsoCodes.aspx?lang=he"" style=""font-size:10px;"">Hebrew List</a>"
            lblCountries1.Text = "|&nbsp; <a href=""List_IsoCodes.aspx?lang=he"" style=""font-size:10px;"">Hebrew List</a>"
        Else
            lblStates1.Text = ""
            lblCountries1.Text = ""
        End If
        rptCurrency.DataBind()
        If WebUtils.IsLoggedin Then
            Dim sSQL As String = "SELECT IsRemoteCharge,IsRemoteChargePersonalNumber,IsRemoteChargeCVV2,IsRemoteChargePhoneNumber," &
            " IsRemoteChargeEmail, IsApprovalOnly, IsBillingAddressMust, tblMerchantRecurringSettings.IsEnabled as IsAllowRecurring, IsUseFraudDetection_MaxMind, IsRefund, AutoCaptureHours" &
            " FROM tblCompany Left Join tblMerchantRecurringSettings ON(tblMerchantRecurringSettings.MerchantID = tblCompany.ID)" &
            " WHERE ID = " & Merchant.ID
            Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
            If iReader.Read() Then
                IsRemoteCharge = iReader("IsRemoteCharge")
                IsRemoteChargePersonalNumber = iReader("IsRemoteChargePersonalNumber")
                lblIsRemoteChargePersonalNumber.Text = IIf(IsRemoteChargePersonalNumber, "Yes <sup>3</sup>", "Optional")

                IsRemoteChargeCVV2 = iReader("IsRemoteChargeCVV2")
                lblIsRemoteChargeCVV2.Text = IIf(IsRemoteChargeCVV2, "Yes <sup>3</sup>", "Optional")

                IsRemoteChargePhoneNumber = iReader("IsRemoteChargePhoneNumber")
                lblIsRemoteChargePhoneNumber.Text = IIf(IsRemoteChargePhoneNumber, "Yes <sup>3</sup>", "Optional")

                IsRemoteChargeEmail = iReader("IsRemoteChargeEmail")
                lblIsRemoteChargeEmail.Text = IIf(IsRemoteChargeEmail, "Yes <sup>3</sup>", "Optional")

                IsApprovalOnly = iReader("IsApprovalOnly")
                If Not IsApprovalOnly Then
                    lblIsApprovalOnly.Text = "<span style=""color:maroon;"">Authorization only is not available, contact " & sBrandName & " to activate</span><br />"
                    lblIsApprovalOnly2.Text = "<span style=""color:maroon;"">Authorization only is not available, contact " & sBrandName & " to activate</span><br />"
                Else
                    lblIsApprovalOnly.Text = ""
                    lblIsApprovalOnly2.Text = ""
                End If

                IsBillingAddressMust = iReader("IsBillingAddressMust")
                lblIsBillingAddressMust.Text = IIf(IsBillingAddressMust, "Yes", "Optional")

                IsAllowRecurring = dbPages.TestVar(iReader("IsAllowRecurring"), False)

                IsUseFraudDetectionMaxMind = iReader("IsUseFraudDetection_MaxMind")
                lblIsUseFraudDetectionMaxMind.Text = IIf(IsUseFraudDetectionMaxMind, "Yes", "Optional")

                IsRefund = iReader("IsRefund")
                If Not IsRefund Then
                    lblIsRefund.Text = "<span style=""color:maroon;"">Refund is not available, contact " & sBrandName & " to activate</span><br />"
                Else
                    lblIsRefund.Text = ""
                    lblIsRefund.Visible = False
                End If

                If iReader("AutoCaptureHours") > 0 Then lblAutoCapture.Text = "will be captured automatically after " & iReader("AutoCaptureHours") & " hours"
            End If
            iReader.Close()
        Else
            lblIsRemoteChargePersonalNumber.Text = "Not Applicable"
            lblIsRemoteChargeCVV2.Text = "Not Applicable"
            lblIsRemoteChargePhoneNumber.Text = "Not Applicable"
            lblIsRemoteChargeEmail.Text = "Not Applicable"
            lblIsApprovalOnly.Text = "<span style=""color:maroon;"">Not Applicable to Guest Login</span><br/>"
            lblIsBillingAddressMust.Text = "Not Applicable"
            lblIsUseFraudDetectionMaxMind.Text = "Not Applicable"
            lblIsRefund.Text = "<span style=""color:maroon;"">Not Applicable to Guest Login</span><br/>"
        End If
        litStoreCc.Visible=Not Authorization.IsAllowed(AuthorizationProperty.StoreCreditCards)
    End Sub

    Protected Sub lbtnMerchant_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(CurrentDomain.MerchantUrl, True)
    End Sub
</script>
<asp:Content ID="Content1" Visible="true" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">CREDIT CARD</span>
			<span class="PageSubHeading">- SILENT POST</span>
			<br />
			<custom:AuthAlert PropertyToCheck="RemoteCharge" runat="server" />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">PAGE URL<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td><%=sProcessURL %>remote_charge.asp<br /></td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Request Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
				<th width="46">Max<br />Length<br /></th>
				<th width="80" valign="bottom" style="white-space:nowrap;">Required <sup>1</sup><br /></th>
			</tr>
			<tr>
				<td>CompanyNum<br /></td>
				<td>Your company number - <%=MerchantNumber%><br /></td>
				<td>7</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>TransType <sup>2</sup><br /></td>
				<td>
					Transaction type:<br />
					0 = Debit Transaction<br />
					1 = Authorization only<br />
					2 = Capture<br />
					3 = Charge <a href="CcStorage_MsgFields.aspx">stored credit card</a>
						(pass the stored card ID in <b>CcStorageID</b> field)
					<br />
					<asp:label id="lblIsApprovalOnly" runat="server" />
				</td>
				<td>2</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>CcStorageID<br /></td>
				<td>When charging <a href="CcStorage_MsgFields.aspx">stored credit card</a>, the ID of the stored card.</td>
				<td>9</td>
				<td>No <sup>4</sup></td>
			</tr>
			<tr>
				<td>CardNum<br /></td>
				<td>Credit card number to be charged<br /></td>
				<td>20</td>
				<td>Yes <sup>3</sup></td>
			</tr>
			<tr>
				<td>ExpMonth<br /></td>
				<td>Expiration month (MM)<br /></td>
				<td>2</td>
				<td>Yes <sup>3</sup></td>
			</tr>
			<tr>
				<td>ExpYear<br /></td>
				<td>Expiration year (YYYY)<br /></td>
				<td>4</td>
				<td>Yes <sup>3</sup></td>
			</tr>
			<tr>
				<td>Track2<br /></td>
				<td>Track 2 of credit card number<br /></td>
				<td>20</td>
				<td>No <sup>4</sup></td>
			</tr>
			<tr>
				<td>Member<br /></td>
				<td>Cardholder name as it appears on the card<br /></td>
				<td>50</td>
				<td>Yes <sup>3</sup></td>
			</tr>
			<tr>
				<td>TypeCredit <sup>2</sup><br /></td>
				<td>
					1 = Debit<br />
					8 = Installments<br />
					0 = Refund<br />
					<asp:Label ID="lblIsRefund" runat="server" />
				</td>
				<td>1</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Payments<br /></td>
				<td>Number of installments, 1 for regular transaction<br /></td>
				<td>2</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Amount<br /></td>
				<td>Amount to be charged, e.g. 199.95<br /></td>
				<td>10</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Currency<br /></td>
				<td>
					<asp:Repeater ID="rptCurrency" DataSource="<%# dbPages.Currencies  %>" runat="server">
						<ItemTemplate>
							<asp:Literal runat="server" Text='<%#Eval("ID")%>' /> = <asp:Literal runat="server" Text='<%#Eval("IsoCode")%>' />
							(<asp:Literal runat="server" Text='<%#Eval("Name")%>' />)
						</ItemTemplate>
						<SeparatorTemplate><br /></SeparatorTemplate>
					</asp:Repeater>
				</td>
				<td>1</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>CVV2<br /></td>
				<td>3-5 digits from back of the credit card<br /></td>
				<td>5</td>
				<td><asp:label ID="lblIsRemoteChargeCVV2" runat="server" /></td>
			</tr>
			<tr>
				<td>Email<br /></td>
				<td>Cardholder email address<br /></td>
				<td>50</td>
				<td><asp:Label ID="lblIsRemoteChargeEmail" runat="server" /></td>
			</tr>
			<tr>
				<td>PersonalNum<br /></td>
				<td>Cardholder Driver ID or social security number<br /></td>
				<td>20</td>
				<td><asp:label ID="lblIsRemoteChargePersonalNumber" runat="server" /></td>
			</tr>
			<tr>
				<td>DateOfBirth<br /></td>
				<td>
					Date of birth of card holder<br />
					format should be yyyyMMdd
				</td>
				<td>8</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>PhoneNumber<br /></td>
				<td>Cardholder phone number<br /></td>
				<td>20</td>
				<td><asp:Label ID="lblIsRemoteChargePhoneNumber" runat="server" /></td>
			</tr>
			<tr>
				<td>ClientIP<br /></td>
				<td>IP address of the client making the purchase<br /></td>
				<td>15</td>
				<td><asp:Label ID="lblIsUseFraudDetectionMaxMind" runat="server" /></td>
			</tr>
			<tr>
				<td>BillingAddress1<br /></td>
				<td>1st Address line<br /></td>
				<td>100</td>
				<td><asp:Label ID="lblIsBillingAddressMust" runat="server"/></td>
			</tr>
			<tr>
				<td>BillingAddress2<br /></td>
				<td>2nd Address line<br /></td>
				<td>100</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>BillingCity<br /></td>
				<td>City name<br /></td>
				<td>60</td>
				<td><%= IIf(IsBillingAddressMust, "Yes", "Optional") %></td>
			</tr>
			<tr>
				<td>BillingZipCode<br /></td>
				<td>Zip Code number<br /></td>
				<td>15</td>
				<td><%= IIf(IsBillingAddressMust,"Yes","Optional") %></td>
			</tr>
			<tr>
				<td>BillingState<br /></td>
				<td>
					State - 2 character iso format<br />
					Required only when BillingCountry is US/CA.<br />
					<a href="List_IsoCodes.aspx" style="font-size:10px;">English list</a>
					&nbsp;<asp:Label ID="lblStates1" runat="server" />
				</td>
				<td>5</td>
				<td><%= IIf(IsBillingAddressMust,"Yes","Optional") %></td>
			</tr>
			<tr>
				<td>BillingCountry<br /></td>
				<td>
					Country - 2 character iso format<br />
					<a href="List_IsoCodes.aspx" style="font-size:10px;">English list</a>
					&nbsp;<asp:Label ID="lblCountries1" runat="server" />
				</td>
				<td>5</td>
				<td><%= IIf(IsBillingAddressMust,"Yes","Optional") %></td>
			</tr>
            <!--
			<tr>
				<td>UserName<br /></td>
				<td>Customer name<br /></td>
				<td>50</td>
				<td>Optional</td>
			</tr>
            -->
			<tr>
				<td>ConfirmationNum<br /></td>
				<td>Confirmation number<br /></td>
				<td>7</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>Order<br /></td>
				<td>Unique Text used to defer one transaction from another<br /></td>
				<td>100</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>Comment<br /></td>
				<td>Optional text used mainly to describe the transaction</td>
				<td>500</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>TransApprovalID<br /></td>
				<td>
					Authorized transaction ID. Used to specify the initial pre-auth transaction ID<br />
					when capturing pre-auth transaction (TransType = 2).<br />
					<asp:label id="lblIsApprovalOnly2" runat="server" />
				</td>
				<td>10</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>RefTransID<br /></td>
				<td>
					Reference transaction ID. Used to specify the original transaction id<br />
					when TypeCredit = 0 or when needed to complete missing transaction data.<br />
				</td>
				<td>10</td>
				<td>Optional</td>
			</tr>
			<asp:Literal ID="litRefTransType" runat="server">
				<tr>
					<td>RefTransType<br /></td>
					<td>
						Type of reference to the transaction specified by RefTransID.<br />
						RefTransType can be any single alphanumeric character.<br />
						Here are some common examples:<br />
						A = additional charge,<br />
						C = comission,<br />
						D = deferred charge,<br />
						E = extra charge,<br />
						F = fee,<br />
						R = recurring charge,<br />
						S = subsequent charge.<br />
						If both RefTransType and RefTransID are specified,<br />
						they will appear in the XML returned by Data Pulling.<br />
						Example: having RefTransID=1234567&amp;RefTransType=R,<br />
						&lt;RefTrans&gt;R1234567&lt;/RefTrans&gt; will appear in the XML.
					</td>
					<td>1</td>
					<td>Optional</td>
				</tr>
			</asp:Literal>
			<tr>
				<td>TrmCode<br /></td>
				<td>When activated and applied in merchant config, this parameter selects the exact terminal to use<br /></td>
				<td>4</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>AutoCaptureHours</td>
				<td>
					Authorization only (TransType=1), the number of hours to wait before automatic capture is carried out.<br />
					If omitted, the authorized transaction <asp:label id="lblAutoCapture" runat="server" Text="will not be captured automatically" />.
				</td>
				<td>2</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>RetURL<br /></td>
				<td>
					3D secure only, In cases where 553 is returned.
					the client should be redirected to bank's site.
					when the interaction with bank is done, the client is redirected to this URL.
				</td>
				<td>255</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>StoreCc<br /></td>
				<td>
					Specifies, if the credit card should be stored in case of success:<br />
					0 = Do not store the card (default)<br />
					1 = Store the card<br />
					<asp:Literal runat="server" ID="litStoreCc">
						In order to store the credit card,
						<a href="CcStorage_MsgFields.aspx">Credit Card Storage</a> service should be enabled in your account.</td>
					</asp:Literal>
				<td>1</td>
				<td>No <sup>4</sup></td>
			</tr>
			<tr>
				<td>AutoWalletRegistration<br /></td>
				<td>
					Send 1 to create new wallet for the customer using customer data and CC details.
					Wallet is created only in case of success.
				</td>
				<td>1</td>
				<td>No</td>
			</tr>
			<tr>
				<td>Signature<br /></td>
				<td>
					Signature for verifying the authenticity of the request parameters.<br />
					Field values to use: <i>CompanyNum + TransType + TypeCredit + Amount + Currency + CardNum + RefTransID + PersonalHashKey</i><br /><br />
					Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
				</td>
				<td>50</td>
				<td><%= "Optional" %></td>
			</tr>			

			</table>
		</td>
	</tr>
	<%
        If IsAllowRecurring Or Not WebUtils.IsLoggedin Then
		%>
		<tr><td><br /></td></tr>
		<tr><td class="FieldSecHeading">Recurring: &nbsp;&nbsp; <span style="font-weight:normal;"></span><br /></td></tr>
		<tr>
			<td>
				<br />
				See <a href="RecurringInfo.aspx">Recurring information</a> for detailed description and for some useful examples.<br />
				<br />
				Pay attention: recurring series is limited in time. The maximum duration is 10 years.<br />
				<br />
				<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
				<tr>
					<td width="110">RecurringTransType<br /></td>
					<td>
						Specifies transaction type for subsequent transactions.<br />
						Does not affect the initial transaction.<br />
						Accepts the same values as TransType.<br />
						If omitted, all the subsequent transactions will be of the same type as the initial transaction.
					</td>
					<td width="46">1</td>
					<td width="80">Optional</td>
				</tr>
				<tr>
					<td>Recurring1<br /></td>
					<td>Parameter for first stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>Recurring2<br /></td>
					<td>Parameter for second stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>Recurring3<br /></td>
					<td>Parameter for third stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>Recurring4<br /></td>
					<td>Parameter for fourth stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>Recurring5<br /></td>
					<td>Parameter for fifth stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
				<tr>
					<td>Recurring6<br /></td>
					<td>Parameter for sixth stage in the recurring series<br /></td>
					<td>12</td>
					<td>Optional</td>
				</tr>
				</table>
			</td>
		</tr>
		<%
	End if
	%>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Response Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td>TransType<br /></td>
				<td>Transaction type<br /></td>
			</tr>
			<tr>
				<td>Reply<br /></td>
				<td>Reply code (see list below)<br /></td>
			</tr>
			<tr>
				<td>TransID<br /></td>
				<td>Transaction number<br /></td>
			</tr>
			<tr>
				<td>Date<br /></td>
				<td>Transaction time and date<br /></td>
			</tr>
			<tr>
				<td>Order<br /></td>
				<td>Transaction id or order number that was supplied by merchant<br /></td>
			</tr>
			<tr>
				<td>Amount<br /></td>
				<td>Transaction amount<br /></td>
			</tr>
			<tr>
				<td>Payments<br /></td>
				<td>Number of payments<br /></td>
			</tr>
			<tr>
				<td>Currency<br /></td>
				<td>Transaction currency<br /></td>
			</tr>
			<tr>
				<td>ConfirmationNum<br /></td>
				<td>Confirmation number<br /></td>
			</tr>
			<tr>
				<td>Comment<br /></td>
				<td>Comment as it was sent by the merchant<br /></td>
			</tr>
			<tr>
				<td>ReplyDesc<br /></td>
				<td>Reply description<br /></td>
			</tr>
			<tr>
				<td>CCType<br /></td>
				<td>Credit card brand name<br /></td>
			</tr>
			<tr>
				<td>Descriptor<br /></td>
				<td>Bank statement descriptor for this transaction<br /></td>
			</tr>
			<tr>
				<td>RecurringSeries<br /></td>
				<td>Recurring series number (only if recurring series has been created)<br /></td>
			</tr>
			<tr>
				<td>Last4<br /></td>
				<td>Last four digits of the credit card number<br /></td>
			</tr>
			<tr>
				<td>CcStorageID <sup>4</sup><br /></td>
				<td>The ID of the credit card in the <a href="CcStorage_MsgFields.aspx">Credit Card Storage</a></td>
			</tr>
			<tr>
				<td>WalletID<br /></td>
				<td>The ID of the newly created wallet for the customer</td>
			</tr>
			<tr>
				<td>D3Redirect<br /></td>
				<td>in case Reply=553 the client has to be redirected to this address to complete the transaction.</td>
			</tr>
			<tr>
				<td>signType<br /></td>
				<td>at the moment, this value always equal to SHA256
				</td>
			</tr>
			<tr>
				<td>Signature<br /></td>
				<td>
					a base64, urlEncoded, SHA256 of response values:<br/>
					to regenerate the value: <br/>
					1. long_values_string = Reply + TransID + Order + Amount + Currency<br/>
					2. urlEncode(base64(sha356( (long_values_string) + (merchant HashKey) ))) 
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					TransType=0&Reply=000&TransID=12345&Date=01/01/2000 00:00:01
					&Order=A1234&Amount=10.99&Payments=1&Currency=0&ConfirmationNum=0123456
					&Comment=White box&ReplyDesc=SUCCESS&CCType=VISA&Descriptor=abc ltd.
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Reply Codes<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="1">
			<tr>
				<td><span>000</span> = Authorized transaction<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><span>001</span> = Transaction accepted, waiting authorization<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><span>xxx</span> = Transaction was not Authorized, see unauthorized comment explanation<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td>
			- For a more detail list of reply codes <a href="List_ReplyCodes.aspx">click here</a> 
			<a href="List_ReplyCodes.aspx"><img src="/NPCommon/Images/iconNewWinRight.GIF" border="0" align="middle" /></a><br />
		</td>
	</tr>
	<tr>
		<td>
			<br /><hr size="1" width="100%" noshade="noshade" />
			(1) Some field requirements are dependent on merchant configuration and can be change on your request<br />
			(2) First option is default when field is empty<br />
			(3) This field must be omitted when charging <a href="CcStorage_MsgFields.aspx">stored credit card</a> (TransType=3)<br />
			(4) This field is relevant only when <a href="CcStorage_MsgFields.aspx">Credit Card Storage</a> service is enabled in the merchant account<br />
		</td>
	</tr>
	</table>
</asp:Content>