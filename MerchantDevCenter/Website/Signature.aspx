<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - SILENT POST - Credit Card" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    Public Shared Function GenerateSHA256(ByVal value As String) As String
        Dim sh As System.Security.Cryptography.SHA256 = System.Security.Cryptography.SHA256.Create()
        Dim hashValue As Byte() = sh.ComputeHash(System.Text.Encoding.UTF8.GetBytes(value))
        Return System.Convert.ToBase64String(hashValue)
    End Function
    
    Protected Sub Generate_Click(sender As Object, e As EventArgs)
        txQuery.InnerText = GenerateSHA256(txtString.Text)
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<style type="text/css">
		textarea
		{
			width:100%;
			height:170px;
			border:1px dashed #c0c0c0;
			font-weight:normal;
			background-color:rgb(247, 247, 247);
			color:black;
			overflow:auto;
			font:normal 97% Verdana,arial,helvetica,sans-Serif;
		}
		.code
		{
			font-size:12px;
			font-weight:normal;
			font-family:Courier New;
			font-style:italic;
			/*background-color:rgb(247, 247, 247);*/
		}
	</style>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td><span class="PageHeading">SIGNATURE</span></td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			Integration with some of <%= WebUtils.CurrentDomain.BrandName %> payment services requires generating a hashed signature.<br />
			<br />
			SHA256 signature is used to validate the integrity of the data, for more information on SHA256 see:
			<a target="_blank" href="http://en.wikipedia.org/wiki/SHA256">http://en.wikipedia.org/wiki/SHA256</a><br />
			<br />
			Your Personal Hash Key is managed in the <a href="Security_Settings.aspx">Security Settings</a> page under Global Settings.<br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td colspan="2" class="SecHeading">Instruction<br /><br /></td></tr>
	<tr>
		<td>
			<ul style="list-style-type: decimal; margin-left:22px; padding-left:0px;">
                <li>Concatenate the required parameters values, the list of required parameters is explained in the signature field on the API of the service you wish to use.</li>
				<li>Add your <a href="Security_Settings.aspx">personal hash Key</a> to the end of the concatenated string.</li>
				<li>Apply SHA256 hash to the string.</li>
                <li>Convert the hash result to Base64.</li>
                <li>URL encode the Base64 result if using GET mode.</li>
				<li>Now that you have a signature, Include the result as a value of the signature field in the request.</li>
			</ul>
			<br />
            Example with the following values:<br />
			CompanyNum = 1234567<br />
			TransType = 1<br />
			TypeCredit = 1<br />
			Amount = 5.4<br />
			Currency = 1<br />
			CardNum = 4580000000000000<br />
			RefTransID = 1234<br />
			PersonalHashKey = AU7E468HNF<br />
			<br />
			CompanyNum + TransType + TypeCredit + Amount + Currency + CardNum + RefTransID + PersonalHashKey<br />
			"1234567" + "1" + "1" + "5.4" + "1" + "4580000000000000" + "1234" + "AU7E468HNF"<br /><br />
            Base64(SHA256("1234567115.4145800000000000001234AU7E468HNF"))<br />
			Result: "PTpzX9OACBC+V3Fd9+TNCehnwIfqMaXmnUtsZMSRyVo="<br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td colspan="2" class="SecHeading">Code Example<br /><br /></td></tr>
	<tr>
		<td>
			The following examples show how the signature should be built in various languages.<sup>3</sup><br />
			<br />
			All examples are using the string "<span class="code">1234567ABCDEFGHIJ</span>" as input.<br />
			The correct output with URLEncoded is "<span class="code">yZo3as0hwZMOO%2bVO0sTXSg%3d%3d</span>".<br />
			The correct output without URLEncoded is "<span class="code">yZo3as0hwZMOO+VO0sTXSg==</span>".<br />
			<br />
		</td>
	</tr>
	<tr>
		<td class="SecExpandLink">
			<img onclick="showSection('0');" style="cursor:pointer;" id="oListToggle0" src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="top">
			<a href="javascript:showSection('0');">1 - ASP.Net</a> (VB)<br />
		</td>
	</tr>
	<tr>
		<td id="trSec0" style="display:none;">
			<br />
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
                    <pre style="width:690px;height:200px;overflow-x:scroll;">
Public Class Signature
    Public Shared Function GenerateSHA256(ByVal value As String) As String
        Dim sh As System.Security.Cryptography.SHA256 = System.Security.Cryptography.SHA256.Create()
        Dim hashValue As Byte() = sh.ComputeHash(System.Text.Encoding.UTF8.GetBytes(value))
        Return System.Convert.ToBase64String(hashValue)
	End Function
End Class

'Usage Example
Dim sSignature As String = Signature.GenerateSHA256("1234567ABCDEFGHIJ")
                    </pre>
				</td>
			</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr>
		<td class="SecExpandLink">
			<img onclick="showSection('1');" style="cursor:pointer;" id="oListToggle1" src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="top">
			<a href="javascript:showSection('1');">2 - ASP.Net</a> (C#)<br />
		</td>
	</tr>
	<tr>
		<td id="trSec1" style="display:none;">
			<br />
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
                    <pre style="width:690px;height:200px;overflow-x:scroll;">
public class Signature
{
    public static string GenerateSHA256(string value)
    {
        System.Security.Cryptography.SHA256 sh = System.Security.Cryptography.SHA256.Create();
        byte[] hashValue = sh.ComputeHash(System.Text.Encoding.UTF8.GetBytes(value));
        return System.Convert.ToBase64String(hashValue);
    }
}

//Usage Example
string sSignature = Signature.GenerateSHA256("1234567ABCDEFGHIJ");
                    </pre>
				</td>
			</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr>
		<td class="SecExpandLink">
			<img onclick="showSection('2');" style="cursor:pointer;" id="oListToggle2" src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="top">
			<a href="javascript:showSection('2');">3 - PHP 5</a><br />
		</td>
	</tr>
	<tr>
		<td id="trSec2" style="display:none;">
			<br />
			<table border="0" style="border:1px dashed #c0c0c0;" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
                    <pre style="width:690px;">
$val=base64_encode(hash("sha256", "1234567ABCDEFGHIJ", true));
                    </pre>
				</td>
			</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr>
		<td class="SecExpandLink">
			<img onclick="showSection('3');" style="cursor:pointer;" id="oListToggle3" src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="top">
			<a href="javascript:showSection('3');">4 - Classic ASP</a> (VBScript)<br />
		</td>
	</tr>
	<tr>
		<td id="trSec3" style="display:none;">
			<br />
			<table border="0" style="border:1px dashed #c0c0c0;" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
                    <pre style="width:690px;height:300px;overflow:scroll;">
<!--#include file="Asp-SHA256-Impl.asp"-->
                    </pre>
				</td>
			</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td colspan="2" class="SecHeading">Signature Generating  Form<br /><br /></td></tr>
	<tr><td>Use the following form to compare the output signature from your code with ours.<br /><br /></td></tr>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0">
			<tr>
				<td>
					String to hash<br />
					<asp:TextBox runat="server" ID="txtString" CssClass="long" maxlength="100" /> &nbsp;<br />
				</td>
				<td>
					<br />
					<asp:Button runat="server" id="btnGenerate" Text="Generate" CssClass="submit" onclick="Generate_Click" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div runat="server" ID="txQuery"
						style="border: 1px dashed #c0c0c0; background-color:#f7f7f7; overflow:auto; word-wrap:break-word;
						width:100%; height:50px; font:normal 12px Courier New, Monospace; margin-top:15px;">Result ...</div>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
</asp:Content>