﻿Public Class OuterLogin
    Inherits System.Web.UI.Page

    Protected Overrides Sub InitializeCulture()
        Dim sendCulture As String = dbPages.TestVar(Request("Language"), -1, "en-us")
        Culture = sendCulture
        UICulture = sendCulture
        MyBase.InitializeCulture()
    End Sub

    Protected Sub LoginSuccess(ByVal sender As Object, ByVal e As EventArgs)
        Response.Write("<script>top.location.href='Default.aspx'</script>")
    End Sub

End Class