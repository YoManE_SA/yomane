﻿<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="BLACKLIST - MESSAGE FIELDS" %>

<script runat="server">
    Dim xmlData As String
    Public Sub Page_Load()
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
		<tr>
			<td>
				<span class="PageHeading">BLACKLIST</span><br />
			</td>
		</tr>
		<tr>
			<td>
				Service to remotly manage a blacklist for values to be blocked from processing under your account.
			</td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">Service URL<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td><%=CurrentDomain.WebServicesUrl %>Risk.asmx/ManageBlocks<br /></td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">Request Formats<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr><td>
            The service accept calls in the following protocols:<br />
            SOAP 1.1, SOAP 1.2, HTTP POST and JSON<br /><br />
            for SOAP calls you may use the wsdl declaration in the following address:<br />
            <a href="<%=CurrentDomain.WebServicesUrl %>Risk.asmx?WSDL"><%=CurrentDomain.WebServicesUrl %>Risk.asmx?WSDL</a>
            <br /><br />
            also you can find more help on web services requests in the following link:<br />
            <a href="Examples_ServiceUsage.aspx">Service Usage Examples</a>
            <br /><br />
        </td></tr>
        <tr><td style="border:1px dashed #c0c0c0; padding:5px; background-color:#f7f7f7;">
        <%
            xmlData = _
            "<ManageBlocks xmlns=""http://netpay-intl.com/"">" & vbCrLf & _
            "   <credentialsToken>guid</credentialsToken>" & vbCrLf & _
            "   <signature>string</signature>" & vbCrLf & _
            "   <items>" & vbCrLf & _
            "       <BlockItem Type=""OneOf_ValueTypeEnum"" Action=""OneOf_ActionTypeEnum"">" & vbCrLf & _
            "           <Comment>string</Comment>" & vbCrLf & _
            "           <Values>" & vbCrLf & _
            "               <BlockItemValue Key=""string"" Value=""string"" />" & vbCrLf & _
            "           </Values>" & vbCrLf & _
            "       </BlockItem>" & vbCrLf & _
            "   </items>" & vbCrLf & _
            "</ManageBlocks>"
            Response.Write("<pre>" & Server.HtmlEncode(xmlData) & "</pre>")
        %>
        </td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
						<th width="46">Max<br />Length<br /></th>
						<th width="80" valign="bottom">Required<br /></th>
					</tr>
					<tr>
						<td>credentialsToken</td>
						<td>
                            A guid credential token that you have got from a previus call to Login.<br />
                            See <a style="font-size:90%;" href="Login_MsgFields.aspx">SERVICES --> LOGIN TOKEN</a> for more information and API.
                        </td>
						<td>32</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>signature</td>
						<td>
							Signature for verifying the authenticity of the request parameters.<br />
							Field values to use: <i>credentialsToken + CountOf(items) + hashKey</i><br /><br />
							Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
                        </td>
						<td>64</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>items</td>
						<td>Contains a list of BlockItem</td>
						<td></td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>BlockItem</td>
						<td>
                            List of block items groups, each have the following attributes:<br />
                            <ul>
                             <li style="margin-top:5px;">Type = can have one the following values: CreditCard, Email, FullName, PhoneNumber, PersonalID, Bin, BinCountry</li>
                             <li style="margin-top:5px;">Action = can have one the following values: Add, Remove, Check</li>
                            </ul> 
                        </td>
						<td>12</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Comment</td>
						<td>Comment that will be attached to the block<br /></td>
						<td>255</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Values</td>
						<td>List of BlockItemValue elements<br /></td>
						<td></td>
						<td>Yes</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap">BlockItemValue</td>
						<td>
                            List of values each with the following attributes:<br />
                            <ul>
                             <li style="margin-top:5px;">Key = the key for the value as sent in the request</li>
                             <li style="margin-top:5px;">Value = the blocked value</li>
                            </ul>
                            <br />
                         </td>
						<td>200</td>
						<td>Yes</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr><td><br /></td></tr>
		<tr><td class="SecHeading">Response Formats<br /></td></tr>
		<tr><td><br /></td></tr>
        <tr><td style="border:1px dashed #c0c0c0; padding:5px; background-color:#f7f7f7;">
        <%
            xmlData = _
            "<ManageBlocksResponse>" & vbCrLf & _
            "    <ManageBlocksResult>" & vbCrLf & _
            "        <BlockItemResult key=""string"" Exist=""boolean"">" & vbCrLf & _
            "           <Insert>dateTime</Insert>" & vbCrLf & _
            "        </BlockItemResult>" & vbCrLf & _
            "    </ManageBlocksResult>" & vbCrLf & _
            "</ManageBlocksResponse>"
            Response.Write("<pre>" & Server.HtmlEncode(xmlData) & "</pre>")
        %>
        </td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
						<th width="46">Max<br />Length<br /></th>
						<th width="80" valign="bottom">Required<br /></th>
					</tr>
					<tr>
						<td>ManageBlocksResult</td>
						<td>a list of block items groups one per values item in the request<br /></td>
						<td></td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>BlockItemResult</td>
						<td>
                            a single block result with the following attributes:<br />
                            <ul>
                             <li style="margin-top:5px;">Key = a key that was sent in the request with the block item value</li>
                             <li style="margin-top:5px;">Exist = the status of the lock in our records</li>
                            </ul>
                            <br />
                         </td>
						<td></td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Insert</td>
						<td>
                            the date the record first entered or null if the record does not exist<br />
                         </td>
						<td>20</td>
						<td>No</td>
					</tr>
				</table>
			</td>
		</tr>
    </table>
</asp:Content>