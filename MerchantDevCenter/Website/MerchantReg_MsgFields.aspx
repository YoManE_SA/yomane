<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="MERCHANT REGISTRATION - MESSAGE FIELDS" %>

<script runat="server">
    Dim xmlData As String
    Public Sub Page_Load()
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
		<tr><td class="SecHeading">Service URL<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td><%=CurrentDomain.WebServicesUrl %>Merchants.asmx/Register<br /></td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">Request Formats<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr><td>
            The service accept calls in the following protocols:<br />
            SOAP, XML, HTTP POST and JSON<br /><br />
            for SOAP calls you may use the wsdl declaration in the following address:<br />
            <a href="<%=CurrentDomain.WebServicesUrl %>Merchants.asmx?WSDL"><%=CurrentDomain.WebServicesUrl %>Merchants.asmx?WSDL</a>
            <br /><br />
        </td></tr>
        <tr><td style="border:1px dashed #c0c0c0; padding:5px; background-color:#f7f7f7;">
<pre>
&lt;Register xmlns=&quot;http://netpay-intl.com/&quot;&gt;
	&lt;vo&gt;
		&lt;ID&gt;int&lt;/ID&gt;
		&lt;DbaName&gt;string&lt;/DbaName&gt;
		&lt;LegalBusinessName&gt;string&lt;/LegalBusinessName&gt;
		&lt;FirstName&gt;string&lt;/FirstName&gt;
		&lt;LastName&gt;string&lt;/LastName&gt;
		&lt;Email&gt;string&lt;/Email&gt;
		&lt;Address&gt;string&lt;/Address&gt;
		&lt;City&gt;string&lt;/City&gt;
		&lt;State&gt;string&lt;/State&gt;
		&lt;Zipcode&gt;string&lt;/Zipcode&gt;
		&lt;OwnerDob&gt;dateTime&lt;/OwnerDob&gt;
		&lt;OwnerSsn&gt;string&lt;/OwnerSsn&gt;
		&lt;Phone&gt;string&lt;/Phone&gt;
		&lt;Url&gt;string&lt;/Url&gt;
		&lt;PhisicalAddress&gt;string&lt;/PhisicalAddress&gt;
		&lt;PhisicalCity&gt;string&lt;/PhisicalCity&gt;
		&lt;PhisicalState&gt;string&lt;/PhisicalState&gt;
		&lt;PhisicalZip&gt;string&lt;/PhisicalZip&gt;
		&lt;StateOfIncorporation&gt;string&lt;/StateOfIncorporation&gt;
		&lt;TypeOfBusiness&gt;int&lt;/TypeOfBusiness&gt;
		&lt;BusinessStartDate&gt;dateTime&lt;/BusinessStartDate&gt;
		&lt;Industry&gt;int&lt;/Industry&gt;
		&lt;BusinessDescription&gt;string&lt;/BusinessDescription&gt;
		&lt;AnticipatedMonthlyVolume&gt;decimal&lt;/AnticipatedMonthlyVolume&gt;
		&lt;AnticipatedAverageTransactionAmount&gt;decimal&lt;/AnticipatedAverageTransactionAmount&gt;
		&lt;AnticipatedLargestTransactionAmount&gt;decimal&lt;/AnticipatedLargestTransactionAmount&gt;
		&lt;BankRoutingNumber&gt;string&lt;/BankRoutingNumber&gt;
		&lt;BankAccountNumber&gt;string&lt;/BankAccountNumber&gt;
	&lt;/vo&gt;
&lt;/Register&gt;
</pre>
        </td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field</th>
						<th valign="bottom">Description</th>
						<th width="46">Max<br />Length</th>
						<th width="80" valign="bottom">Required</th>
					</tr>
					<tr>
						<td>ID</td>
						<td>Ignore</td>
						<td>0</td>
						<td>No</td>
					</tr>
					<tr>
						<td>DbaName</td>
						<td>DBA Name</td>
						<td>50</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>LegalBusinessName</td>
						<td>Legal Business Name</td>
						<td>50</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>FirstName</td>
						<td>First Name</td>
						<td>50</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>LastName</td>
						<td>Last Name</td>
						<td>50</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>Email</td>
						<td>100</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Address</td>
						<td>Address</td>
						<td>250</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>City</td>
						<td>City</td>
						<td>50</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>State</td>
						<td>State 2 letter code</td>
						<td>2</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Zipcode</td>
						<td>Zipcode</td>
						<td>15</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>OwnerDob</td>
						<td>Owner Date of Birth</td>
						<td>Valid Date</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>OwnerSsn</td>
						<td>Owner Social Security Number</td>
						<td>12</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Phone</td>
						<td>Phone</td>
						<td>12</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Url</td>
						<td>Web site URL</td>
						<td>300</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>PhisicalAddress</td>
						<td>Phisical Address</td>
						<td>250</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>PhisicalCity</td>
						<td>Phisical City</td>
						<td>50</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>PhisicalState</td>
						<td>Phisical State 2 letter code</td>
						<td>2</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>PhisicalZip</td>
						<td>Phisical Zipcode</td>
						<td>12</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>StateOfIncorporation</td>
						<td>State of Incorporation 2 letter code</td>
						<td>2</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>TypeOfBusiness</td>
						<td>Type of Business code</td>
						<td>Valid Integer</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>BusinessStartDate</td>
						<td>Business Start Date</td>
						<td>Valid Date</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Industry</td>
						<td>Industry code</td>
						<td>Valid Integer</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>BusinessDescription</td>
						<td>Business Description</td>
						<td>500</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>AnticipatedAverageTransactionAmount</td>
						<td>Anticipated Average TransactionAmount</td>
						<td>Valid Decimal</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>AnticipatedLargestTransactionAmount</td>
						<td>Anticipated Largest TransactionAmount</td>
						<td>Valid Decimal</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>AnticipatedMonthlyVolume</td>
						<td>Anticipated Monthly Volume</td>
						<td>Valid Decimal</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>BankAccountNumber</td>
						<td>Bank Account Number</td>
						<td>20</td>
						<td>Yes</td>
					</tr>					
					<tr>
						<td>BankRoutingNumber</td>
						<td>Bank Routing Number</td>
						<td>20</td>
						<td>Yes</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr><td><br /></td></tr>
		<tr><td class="SecHeading">Response Format<br /></td></tr>
		<tr><td><br /></td></tr>
        <tr><td style="border:1px dashed #c0c0c0; padding:5px; background-color:#f7f7f7;">
<pre>
&lt;RegisterResponse xmlns=&quot;http://netpay-intl.com/&quot;&gt;
    &lt;RegisterResult&gt;boolean&lt;/RegisterResult&gt;
&lt;/RegisterResponse&gt;
</pre>
        </td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
						<th width="46">Max<br />Length<br /></th>
						<th width="80" valign="bottom">Required<br /></th>
					</tr>
					<tr>
						<td>RegisterResult</td>
						<td>Boolean, true if registration has succeeded<br /></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:Content>