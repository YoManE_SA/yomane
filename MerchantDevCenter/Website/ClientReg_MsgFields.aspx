<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - CLIENT REGISTRATION" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>

<script runat="server">
	Dim sBrandName as String = CurrentDomain.BrandName
	Dim sProcessURL as String = CurrentDomain.ProcessURL
	
	protected sub Page_Load
		if CurrentDomain.IsHebrewVisible then
			lblIsrael.Text=" (115 = Israel)"
		else
			lblIsrael.Text=""
		End If
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="1"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">PERSONAL ACCOUNT REGISTRATION</span>
			<span class="PageSubHeading">- MESSAGE FIELDS</span><br />
			<custom:AuthAlert PropertyToCheck="RemoteSignup" runat="server" />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">PAGE URL<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td><%=sProcessURL %>auto_personal_signup.asp<br /></td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Request Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
				<th width="46">Max<br />Length<br /></th>
				<th width="80" valign="bottom">Required<br /></th>
			</tr>
			<tr>
				<td>CompanyNum<br /></td>
				<td>Your company number - <%=MerchantNumber%><br /></td>
				<td>7</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>FirstName<br /></td>
				<td>Client first name<br /></td>
				<td>20</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>LastName<br /></td>
				<td>Client surname<br /></td>
				<td>20</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Street<br /></td>
				<td>Client address, including street name & house / flat number<br /></td>
				<td>20</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>City<br /></td>
				<td>Client city name<br /></td>
				<td>20</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Country<br /></td>
				<td>Client country number<asp:Label ID="lblIsrael" runat="server" /></td>
				<td>5</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Phone<br /></td>
				<td>Client phone number<br /></td>
				<td>15</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Mail<br /></td>
				<td>Client email<br /></td>
				<td>30</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>UserName<br /></td>
				<td>Client user name, minimum of 6 digits<br /></td>
				<td>12</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>IsRead<br /></td>
				<td>End user license agreement<br />IsRead = 1<br /></td>
				<td>1</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>ZIP<br /></td>
				<td>Client zip code.<br /></td>
				<td>10</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>CellPhone<br /></td>
				<td>Client cell phone.<br /></td>
				<td>15</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>CustomerFrom<br /></td>
				<td>Client work email.<br /></td>
				<td>50</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>CardNum<br /></td>
				<td>Credit card number<br /></td>
				<td>20</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>ExpMonth<br /></td>
				<td>Credit card expiration month (MM)<br /></td>
				<td>2</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>ExpYear<br /></td>
				<td>Credit card expiration year (YY)<br /></td>
				<td>2</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>Member<br /></td>
				<td>Cardholder name as it appears on the card.<br /></td>
				<td>50</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>CVV2<br /></td>
				<td>3-5 digits from back of the credit card<br /></td>
				<td>5</td>
				<td>Optional</td>
			</tr>
			<tr>
				<td>PersonalNum<br /></td>
				<td>Cardholder ID number.<br /></td>
				<td>9</td>
				<td>Optional</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Response Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td>ReplyCode<br /></td>
				<td>Reply code (see list below)<br /></td>
			</tr>
			<tr>
				<td>ReplyDesc<br /></td>
				<td>Reply description<br /></td>
			</tr>
			<tr>
				<td>CustomerNum<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			<tr>
				<td>Email<br /></td>
				<td >A returned field from the request<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					ReplyCode=00&ReplyDesc=Registration completed successfully
					&CustomerNum=1234567&Email=name@domain.com
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td class="SecHeading">Reply Codes<br /></td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="1">
			<tr><td>00 = Registration completed successfully<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>10 = Merchant number is missing<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>11 = Invalid merchant number<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>12 = Merchant does not exist<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>13 = Merchant closed or inactive<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>14 = Merchant is not allowed to use remote registration<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>15 = First name is missing or invalid<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>16 = Surname is missing or invalid<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>17 = Street & house number is missing or invalid<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>18 = City is missing or invalid<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>19 = Country is missing<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>20 = Invalid country<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>21 = Phone is missing<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>22 = Invalid phone<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>23 = Email is missing or invalid<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>32 = Invalid end user license agreement<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>33 = Email already exists<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>40 = Credit card - invalid number<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>41 = Credit card - month expiration is missing or invalid<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>42 = Credit card - year expiration is missing or invalid<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>43 = Credit card - card holder name is missing or invalid<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr><td>44 = Credit card - card holder ID is missing or invalid<br /></td></tr>
			<tr><td height="4"></td></tr>
			</table>
		</td>
	</tr>
	</table>
		
</asp:Content>

