<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - SILENT POST - Credit Card" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Dim sBgColor as String, strStatus As String = ""
	
	Dim IsRemoteChargePersonalNumber,IsRemoteChargeCVV2,IsRemoteChargePhoneNumber, _
	IsRemoteChargeEmail,IsApprovalOnly,IsBillingAddressMust,IsAllowRecurring,IsUseFraudDetectionMaxMind As Boolean
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

	End Sub
	
	protected sub lbtnMerchant_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Response.Redirect(CurrentDomain.MerchantUrl,True)
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td><span class="PageHeading">CODING - SENDING DATA</span></td>
	</tr>
	<tr>
		<td>
			<br />
				The following examples are a <u>basic</u> overview on how to send data over the internet.<br />
				The code shown is intended to give you the programmer a start point on how to implement the <%= WebUtils.CurrentDomain.BrandName %> API<br />
			<br />
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td class="SecHeading">Sending server-side request<br /></td>
	</tr>
	<tr><td height="8"></td></tr>
	<tr>
		<td class="SecExpandLink">
			<img onclick="showSection('0');" style="cursor:pointer;" id="oListToggle0" src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle">
			<a href="javascript:showSection('0');">1 - Server-side ASP.Net</a> (C#)<br />
		</td>
	</tr>
	<tr>
		<td id="trSec0" style="display:none;">
			<br />
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
<pre>&lt;%@ Page Language="C#" %&gt;
&lt;%@ import Namespace="System.Net" %&gt;
&lt;%@ import Namespace="System.IO" %&gt;
&lt;html xmlns="http://www.w3.org/1999/xhtml"&gt;
   &lt;head&gt;
      &lt;title&gt;Processor - C#&lt;/title&gt;
   &lt;/head&gt;
   &lt;body&gt;
      &lt;script runat="server"&gt;
         public void Page_Load()
         {
            if(! IsPostBack)
            {

               //------------- building url string to send
               String sendStr;
               sendStr = "https://www.domain.com/file.asp?";
               sendStr += "Field1=" + Server.URLEncode(Request.Form["Var1"]) + "&";
               sendStr += "Field2=" + Server.URLEncode(Request.Form["Var2"]) + "&";
               sendStr += "Field3=" + Server.URLEncode(Request.Form["Var3"]);

               //------------- creating the request
               HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(sendStr);
               webReq.Method = "GET";

               //------------- checking the response
               try
               {
                  HttpWebResponse webRes = (HttpWebResponse)webReq.GetResponse();
                  StreamReader sr = new StreamReader(webRes.GetResponseStream());
                  String resStr = sr.ReadToEnd();
                  Response.Write("Response String: " + resStr + "&lt;br /&gt;");
               }
               catch(Exception ex)
               {
                  Response.Write(ex.Message);
               }
            }
         }
      &lt;/script&gt;
   &lt;/body&gt;
&lt;/html&gt;</pre>
				</td>
			</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr>
		<td class="SecExpandLink">
			<img onclick="showSection('1');" style="cursor:pointer;" id="oListToggle1" src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle">
			<a href="javascript:showSection('1');">2 - Server-side ASP.Net</a> (VB)<br />
		</td>
	</tr>
	<tr>
		<td id="trSec1" style="display:none;">
			<br />
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
<pre>&lt;%@ Page Language="VB" %&gt;
&lt;%@ import Namespace="System.Net" %&gt;
&lt;%@ import Namespace="System.IO" %&gt;
&lt;html xmlns="http://www.w3.org/1999/xhtml"&gt;
   &lt;head&gt;
      &lt;title&gt;Processor - VB.NET&lt;/title&gt;
   &lt;/head&gt;
   &lt;body&gt;
      &lt;script runat="server"&gt;
          Sub Page_Load()
              If Not IsPostBack Then
                  '------------- building url string to send
                  Dim sendStr As String
                  sendStr = "https://www.domain.com/file.asp?"
                  sendStr &= "Field1=" & Server.URLEncode(Request.Form("Var1")) & "&"
                  sendStr &= "Field2=" & Server.URLEncode(Request.Form("Var2")) & "&"
                  sendStr &= "Field3=" & Server.URLEncode(Request.Form("Var3"))

                  '------------- creating the request
                  Dim webReq As HttpWebRequest = WebRequest.Create(sendStr)
                  webReq.Method = "GET"

                  '------------- checking the response
                  Try
                      Dim webRes As HttpWebResponse = webReq.GetResponse()
                      Dim sr As New StreamReader(webRes.GetResponseStream())
                      Dim resStr As String = sr.ReadToEnd()
                      Response.Write("Response String: " & resStr & "&lt;br /&gt;")
                  Catch ex As Exception
                      Response.Write(ex.Message)
                  End Try
              End If
          End Sub
      &lt;/script&gt;
   &lt;/body&gt;
&lt;/html&gt;</pre>
				</td>
			</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr>
		<td class="SecExpandLink">
			<img onclick="showSection('2');" style="cursor:pointer;" id="oListToggle2" src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle">
			<a href="javascript:showSection('2');">3 - Server-side Classic ASP</a> (VB Script)<br />
		</td>
	</tr>
	<tr>
		<td id="trSec2" style="display:none;">
			<br />
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
<pre>&lt;%
   '------------- declaration
   Dim SendStr, HttpReq, ResStr
   
   '------------- building url string to send
   sendStr = "https://www.domain.com/file.asp?"
   sendStr = sendStr & "Field1=" & Server.URLEncode(Request.Form("Var1")) & "&"
   sendStr = sendStr & "Field2=" & Server.URLEncode(Request.Form("Var2")) & "&"
   sendStr = sendStr & "Field3=" & Server.URLEncode(Request.Form("Var2"))

   '------------- creating the request
   Set HttpReq = CreateObject("Msxml2.XMLHTTP.3.0")
   HttpReq.open "get", sendStr, False
   HttpReq.send()

   '------------- checking the response
   resStr = HttpReq.responseText

   Response.Write("Response String: " & resStr)
%&gt;</pre>
				</td>
			</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr><td><br />Please note: All code samples displayed above do not contain any validation implementation, It is strongly recommended that you implement validation in your system.<br /></td></tr>	
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Opening a window<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>Using a link (HTML)<br /><br /></td>
	</tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					&lt;a href="https://www.domain.com?Field1=Val1&Field2=Val2&Field3=Val3" target="fraWindow"<br />
					 onclick="window.open('', this.target, 'scrollbars=1, width=450, height=500, resizable=0, Status=1');"&gt;link&lt;/a&gt;
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><br />Using a form (HTML)<br /><br /></td>
	</tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
					&lt;form action="https://www.domain.com" method="post" name="formName1" target="fraWindow"<br />
					onsubmit="window.open('', this.target, 'scrollbars=1, width=450, height=500, resizable=0, Status=1');"&gt;<br />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;input type="hidden" name="Field1" value="Val1"&gt;<br />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;input type="hidden" name="Field2" value="Val2"&gt;<br />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;input type="hidden" name="Field3" value="Val3"&gt;<br />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;input type="submit" value="submit"&gt;<br />
					&lt;/form&gt;<br />
				</td>
			</tr>
			</table>
			<br />
		</td>
	</tr>
	</table>
</asp:Content>