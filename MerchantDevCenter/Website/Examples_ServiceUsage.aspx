﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/MasterPage.master" %>
<script runat="server">
Dim xmlData As String

</script>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">CODING - SERVICE USAGE</span><br />
		</td>
	</tr>	
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">SOAP V 1.1<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
            Every SOAP 1.1 request has to be enveloped within an XML file using the following elements
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">

                <%
                    xmlData = _
                    "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
                    "<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" " & vbCrLf & _
                    "    xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbCrLf & _
                    " <soap:Body>" & vbCrLf & _
                    "  [Message Data]" & vbCrLf & _
                    " </soap:Body>" & vbCrLf & _
                    "</soap:Envelope>"
                    Response.Write("<pre>" & Server.HtmlEncode(xmlData) & "</pre>")
                %>
				</td>
			</tr>
            </table>
            <br />
            SOAP requests in version 1.1 should also include SOAPAction Header in the request which specify the method to use<br />
            Example:<br />
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
                    SOAPAction: http://netpay-intl.com/ManageBlocks
				</td>
			</tr>
			</table>
            <br />
            SOAP requests in version 1.1 should also include Content-Type Header with value of "text/xml;"<br />
            Example:
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
                    Content-Type: text/xml; charset=utf-8
				</td>
			</tr>
			</table>
        </td>
	</tr>
	<tr><td><br /><br /><br /></td></tr>
	<tr><td class="SecHeading">SOAP V 1.2<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
            Every SOAP 1.2 request has to be enveloped within an XML file using the following elements
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">

                <%
                    xmlData = _
                    "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
                    "<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" " & vbCrLf & _
                    "    xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">" & vbCrLf & _
                    "  <soap12:Body>" & vbCrLf & _
                    "  [Message Data]" & vbCrLf & _
                    " </soap12:Body>" & vbCrLf & _
                    "</soap12:Envelope>"
                    Response.Write("<pre>" & Server.HtmlEncode(xmlData) & "</pre>")
                %>
				</td>
			</tr>
			</table>
            <br />
            SOAP requests in version 1.2 should also include Content-Type Header with value of "application/soap+xml;"<br />
            Example:
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td bgcolor="#f7f7f7">
                    Content-Type: application/soap+xml; charset=utf-8
				</td>
			</tr>
			</table>


        </td>
	</tr>
    </table>
</asp:Content>

