<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - REFUND REQUESTS - Submitting Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>

<script runat="server">
    Public Sub Page_Load()
        lblProcessURL.Text = CurrentDomain.ProcessURL & "RefundRequest.aspx"
        litSample1.Text = CurrentDomain.ProcessUrl & "RefundRequest.aspx?MerchantNumber=" & MerchantNumber & "&amp;RefTransID=1234567&amp;Action=CREATE&amp;Signature=cHFg9Dykno0W8uR/G35aIQ=="
        litSample2.Text = CurrentDomain.ProcessUrl & "RefundRequest.aspx?MerchantNumber=" & MerchantNumber & "&amp;RefTransID=1234567&amp;Action=CREATE&amp;Amount=1.23&amp;Signature=cHFg9Dykno0W8uR/G35aIQ=="
        litSample3.Text = CurrentDomain.ProcessUrl & "RefundRequest.aspx?MerchantNumber=" & MerchantNumber & "&amp;RefTransID=1234567&amp;Action=STATUS&amp;Signature=cHFg9Dykno0W8uR/G35aIQ=="
        litSample4.Text = CurrentDomain.ProcessUrl & "RefundRequest.aspx?MerchantNumber=" & MerchantNumber & "&amp;RefTransID=1234567&amp;Action=DELETE&amp;RequestID=123456&amp;Signature=cHFg9Dykno0W8uR/G35aIQ=="
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">REFUND REQUESTS</span>
			<span class="PageSubHeading">- SUBMITTING DATA</span><br />
			<custom:AuthAlert PropertyToCheck="AskRefund" runat="server" />
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td>
			This page describes how to manage refund requests remotely.<br />
			<br />
			<li style="list-style-type:square;">Using this interface you can create new refund request and/or check refund request status</li>
			<li style="list-style-type:square;">Refund request can be either full or partial <sup>1</sup>.</li>
			<li style="list-style-type:square;">Multiple partial refund request can be created for single transaction.</li>
			<li style="list-style-type:square;">The overall amount of all refund requests for a single transaction cannot exceed the initial transaction amount.</li>
			<li style="list-style-type:square;">Expired credit card cannot be refunded.</li>
			<li style="list-style-type:square;">Transaction over a year old cannot be refunded.</li>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">PAGE URL<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td><asp:Label ID="lblProcessURL" runat="server" /><br /></td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Request Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
				<th width="46">Max<br />Length<br /></th>
				<th width="80" valign="bottom">Required<br /></th>
			</tr>
			<tr>
				<td>MerchantNumber<br /></td>
				<td>Your merchant number - <%=MerchantNumber%><br /></td>
				<td>7</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>RefTransID<br /></td>
				<td>Refunded Transaction ID - ID number of the initial transaction (the one to be refunded).<br /></td>
				<td>9</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Action<br /></td>
				<td>
					Action to take - Accepts one of the following values:
					<ul style="margin-top:0;padding-top:10px;margin-bottom:0;padding-bottom:10px;">
						<li>CREATE - create new refund request.</li>
						<li>STATUS - check refund request status*.</li>
						<li>DELETE - delete pending refund request**.</li>
					</ul>
					* If there are multiple refund requests for the transaction, status of each request is returned.
					<br />
					** If there are multiple pending refund requests for the transaction, RequestID must be specified.
				</td>
				<td>6</td>
				<td>Yes</td>
			</tr>			
			<tr>
				<td>Amount<br /></td>
				<td>
					Amount to refund<br />
					When the action is CREATE, a refund amount can be specified.<br />
					This field is needed only for creating a partial refund request <sup>1</sup> on the transaction.<br />
					<br />
					The amount requested to refund cannot exceed the initial transaction amount.<br />
					If omitted, all the initial transaction amount is requested to refund.<br />
					<br />
					If there is a partial refund request already created for the transaction,<br />
					the new refund request amount cannot exceed the remaining amount.<br />
					If omitted in this case, all the remaining amount is requested to refund.<br />
				</td>
				<td>10</td>
				<td>No</td>
			</tr>
			<tr>
				<td>RequestID</td>
				<td>Refund Request ID - ID number of the existing unprocessed (pending) refund request for transaction specified in RefTransID.<br /></td>
				<td>9</td>
				<td>No</td>
			</tr>	
			<tr>
				<td>Signature<br /></td>
				<td>
					Signature for verifying the authenticity of the request parameters.<br />
					Field values to use: <i>MerchantNumber + PersonalHashKey</i><br /><br />
					Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
				</td>
				<td>30</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td>Comment<br /></td>
				<td>
					Optional text used mainly to describe the refund request reason and details
				</td>
				<td>300</td>
				<td>No</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">REQUEST EXAMPLES <sup>3</sup><br /><br /></td></tr>
	<tr>
		<td style="padding-bottom:8px;">
			1. Create new refund request:
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					<textarea cols="1" rows="1" onclick="select();" class="sampleCode"><asp:literal ID="litSample1" runat="server" /></textarea>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-bottom:8px;">
			2. Create new refund request (partial) <sup>1</sup>:
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					<textarea cols="1" rows="1" onclick="select();" class="sampleCode"><asp:literal ID="litSample2" runat="server" /></textarea>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-bottom:8px;">
			3. Check refund request status:
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					<textarea cols="1" rows="1" onclick="select();" class="sampleCode"><asp:literal ID="litSample3" runat="server" /></textarea>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-bottom:8px;">
			4. Delete refund request:
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					<textarea cols="1" rows="1" onclick="select();" class="sampleCode"><asp:literal ID="litSample4" runat="server" /></textarea>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<br /><hr size="1" width="100%" noshade="noshade" />
			(1) Due to the risk management policy, some debit terminals do not allow partial refund requests<br />
			(2) Taken from management --&gt; security settings<br />
			(3) Examples include a sample only signature, for the real request to work replace signiture with your own<br />
		</td>
	</tr>
	</table>
	<br />
</asp:Content>