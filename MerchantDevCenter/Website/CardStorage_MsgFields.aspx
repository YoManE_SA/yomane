﻿<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="CARD STORAGE - MESSAGE FIELDS" %>

<script runat="server">
    Dim xmlData As String
    Public Sub Page_Load()
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
		<tr>
			<td>
				
				<span class="PageHeading">CARD STORAGE</span><br />
			</td>
		</tr>
		<tr>
			<td>
				Card storage management service.
			</td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">Service URL<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td><%=CurrentDomain.WebServicesUrl %>CardStorage.asmx/ManageCards<br /></td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">Request Formats<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr><td>
            The service accept calls in the following protocols:<br />
            SOAP 1.1, SOAP 1.2, HTTP POST and JSON<br /><br />
            for SOAP calls you may use the wsdl declaration in the following address:<br />
            <a href="<%=CurrentDomain.WebServicesUrl %>CardStorage.asmx?WSDL"><%=CurrentDomain.WebServicesUrl %>CardStorage.asmx?WSDL</a>
            <br /><br />
            also you can find more help on web services requests in the following link:<br />
            <a href="Examples_ServiceUsage.aspx">Service Usage Examples</a>
            <br /><br />
        </td></tr>
        <tr><td style="border:1px dashed #c0c0c0; padding:5px; background-color:#f7f7f7;">
        <%
            xmlData = _
            "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
            "<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" " & vbCrLf & vbTab & "xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">" & vbCrLf & _
            "    <soap12:Body>" & vbCrLf & _
            "       <ManageCards xmlns=""http://netpay-intl.com/"">" & vbCrLf & _
            "           <CredentialsToken>guid</credentialsToken>" & vbCrLf & _
            "           <Items>" & vbCrLf & _
            "               <CardStorageItem>" & vbCrLf & _
            "                   <Action>one of the available actions</Action>" & vbCrLf & _
            "                   <CardNumber>string</CardNumber>" & vbCrLf & _
            "                   <ExpirationMonth>string</ExpirationMonth>" & vbCrLf & _
            "                   <ExpirationYear>string</ExpirationYear>" & vbCrLf & _
            "                   <CardholderFullName>string</CardholderFullName>" & vbCrLf & _
            "                   <Comment>string</Comment>" & vbCrLf & _
            "               </CardStorageItem>" & vbCrLf & _
            "           </Items>" & vbCrLf & _
            "           <Signature>string</Signature>" & vbCrLf & _
            "       </ManageCards>" & vbCrLf & _
            "    </soap12:Body>" & vbCrLf & _
            "</soap12:Envelope>" & vbCrLf

            Response.Write("<pre>" & Server.HtmlEncode(xmlData) & "</pre>")
        %>

        </td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
						<th width="46">Max<br />Length<br /></th>
						<th width="80" valign="bottom">Required<br /></th>
					</tr>
					<tr>
						<td>CredentialsToken</td>
						<td>
                            A guid credential token that you have got from a previous call to Login.<br />
                            See <a style="font-size:90%;" href="Login_MsgFields.aspx">SERVICES --> LOGIN TOKEN</a> for more information and API.
                        </td>
						<td>--</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Signature</td>
						<td>
							Signature for verifying the authenticity of the request parameters.<br />
							Field values to use: <i>credentialsToken + CountOf(items) + hashKey</i><br /><br />
							Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explanation.<br />
                        </td>
						<td>--</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>items</td>
						<td>Contains a list of CardStorageItem</td>
						<td>--</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>CardStorageItem</td>
						<td> Card storage management item, each have the following fields:</td>
						<td>--</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Action</td>
						<td>Can have one the following values: Create, Delete, Verify</td>
						<td>--</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>CardNumber</td>
						<td>The credit card number excluding spaces or hyphens</td>
						<td>16</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>ExpirationMonth</td>
                        <td>The credit card expiration month in 2 digits (07)</td>
						<td>2</td>
						<td>Yes</td>
					</tr>
                    <tr>
						<td>ExpirationYear</td>
						<td>The credit card expiration year in 4 digits (2025)</td>
						<td>4</td>
						<td>Yes</td>
					</tr>
                    <tr>
						<td>CardholderFullName</td>
						<td>The full name of the credit card owner as printed on the card</td>
						<td>100</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>Comment</td>
						<td>Comment that will be attached to the stored card<br /></td>
						<td>400</td>
						<td>No</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr><td><br /></td></tr>
		<tr><td class="SecHeading">Response Formats<br /></td></tr>
		<tr><td><br /></td></tr>
        <tr><td style="border:1px dashed #c0c0c0; padding:5px; background-color:#f7f7f7;">
        <%
            xmlData = _
            "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
            "<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" " & vbCrLf & vbTab & "xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbCrLf & _
            "   <soap:Body>" & vbCrLf & _
            "       <ManageCardsResponse xmlns=""http://netpay-intl.com/"">" & vbCrLf & _
            "           <ManageCardsResult>" & vbCrLf & _
            "               <CardStorageItem>" & vbCrLf & _
            "                   <Action>string</Action>" & vbCrLf & _
            "                   <CardNumber>string</CardNumber>" & vbCrLf & _
            "                   <ExpirationMonth>string</ExpirationMonth>" & vbCrLf & _
            "                   <ExpirationYear>string</ExpirationYear>" & vbCrLf & _
            "                   <CardholderFullName>string</CardholderFullName>" & vbCrLf & _
            "                   <Comment>string</Comment>" & vbCrLf & _
            "                   <Result>string</Result>" & vbCrLf & _
            "               </CardStorageItem>" & vbCrLf & _
            "           </ManageCardsResult>" & vbCrLf & _
            "       </ManageCardsResponse>" & vbCrLf & _
            "   </soap:Body>" & vbCrLf & _
            "</soap:Envelope>" & vbCrLf
            Response.Write("<pre>" & Server.HtmlEncode(xmlData) & "</pre>")
        %>
        </td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
						<th width="46"></th>
						<th width="80"></th>
					</tr>
					<tr>
						<td>All items in CardStorageItem</td>
						<td>Same as the request<br /></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
					</tr>
					<tr>
						<td>Result</td>
						<td>
                            Success if the item was processed successfully,
                            Or a brief description if an error has occurred.     
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
    </table>
</asp:Content>