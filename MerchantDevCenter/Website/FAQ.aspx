<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - FAQ" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
	Dim sSQL As String
	
	Sub Page_Load()
		If Not Page.IsPostBack Then
			ddlCategories.DataSource = Netpay.Bll.Faq.GetCategories(WebUtils.DomainHost, WebUtils.CurrentLanguage, WebUtils.CurrentDomain.ThemeFolder, False, False, True, False)
			ddlCategories.DataBind()
			ddlCategories.Items.Insert(0, New System.Web.UI.WebControls.ListItem("< Select FAQ category >", 0))
		End If
	End Sub
	
	Sub ddlCategories_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		lblQuestions.Text = "<ol class=""questions"">"
		lblAnswers.Text = "<ol class=""answers"">"
		Dim categoryID As Integer = Integer.Parse(ddlCategories.SelectedValue)
		Dim Items = Netpay.Bll.Faq.GetFaqs(WebUtils.DomainHost, categoryID)
		For Each item In Items
			lblQuestions.Text = lblQuestions.Text & "<li><a href=""#answer" & item.GetHashCode() & """>" & item.Key & "</a></li>"
			lblAnswers.Text = lblAnswers.Text & "<li><div><a class=""linkTop"" href=""#pageTop"">back to top</a>"
			lblAnswers.Text = lblAnswers.Text & "<a name=""answer" & item.GetHashCode() & """></a>" & item.Key
			lblAnswers.Text = lblAnswers.Text & "</div>" & item.Value & "</li>"
		Next
		lblQuestions.Text = lblQuestions.Text & "</ol>"
		lblAnswers.Text = lblAnswers.Text & "</ol>"
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<style media="all" type="text/css">
		.questions
		{
			font-weight:normal;
			font-size:11px;
			color:Dark Gray;
			margin-left:0px;
			padding-bottom:20px;
			border-bottom:1px solid silver;
		}

		.answers li
		{
			margin-top:10px;
			padding-top:10px;
		}
		.answers div
		{
			color:#003366;
			font-size:11px;
			font-weight:bold;
			padding-bottom:5px;
		}
		.linkTop
		{
			float:right;
			color:#FF8000;
			text-transform:lowercase;
			font-weight:normal;
			font-size:10px;
		}
	</style>
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td><a name="pageTop"></a><span class="PageHeading">FREQUENTLY ASKED QUESTIONS</span><br /></td>
	</tr>
	<tr>
		<td>
			<br />
			<asp:DropDownList ID="ddlCategories" runat="server" DataTextField="Name" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="ddlCategories_SelectedIndexChanged" />
			<br />
			<asp:Label ID="lblQuestions" runat="server" />
			<asp:Label ID="lblAnswers" runat="server" />

			<asp:Repeater id="rptQuestions" runat="server" >
			</asp:Repeater>

		</td>
	</tr>
	</table>
</asp:Content>

