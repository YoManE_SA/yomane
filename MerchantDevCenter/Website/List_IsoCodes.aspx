<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - COUNTRY &amp; STATE CODES" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td colspan="2">
			<span class="PageHeading">ISO CODES</span><br />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<br />
			<div>
				We use ISO standard two letter codes for specifying countries and states.
			</div>
			<br /><br />
		</td>
	</tr>	
	<tr>
		<td class="SecHeading">Country Codes<br /></td>
		<td class="SecHeading">State Codes<br /></td>
	</tr>
	<tr><td><br /></td></tr>	
	<tr>
		<td style="vertical-align:top;">
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3">
			<tr>
				<th valign="bottom">Code<br /></th>
				<th valign="bottom">Name<br /></th>
			</tr>
			<%
			    Dim sSQL As String = "SELECT CountryISOCode, Name FROM [List].[CountryList] ORDER BY Name"
				Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
				While iReader.Read()
					%>
					<tr>
						<td><%= iReader("CountryISOCode") %></td>
						<td><%= iReader("Name") %></td>
					</tr>
					<%
				End while
				iReader.Close()
			%>
			</table>
		</td>
		<td style="vertical-align:top;">
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3">
			<tr>
				<th valign="bottom">Code<br /></th>
				<th valign="bottom">State<br /></th>
			</tr>
			<%
				Dim sCountry As String = String.Empty
			    sSQL = "SELECT c.name AS CountryName, s.StateISOCode, s.name AS StateName FROM [List].[StateList] AS s INNER JOIN [List].[CountryList] AS c ON s.CountryISOCode = c.CountryISOCode ORDER BY CountryName"
				iReader = dbPages.ExecReader(sSQL)
				While iReader.Read()
			        If iReader("CountryName") <> sCountry Then
			            sCountry = iReader("CountryName")
			            Response.Write("<tr><th style=""background-color:white;"" colspan=""2"">" & sCountry & "</th></tr>")
			        End If
					%>
					<tr>
						<td><%= iReader("StateISOCode")%></td>
						<td><%= iReader("StateName")%></td>
					</tr>
					<%
				End while
				iReader.Close()
			%>
			</table>
		</td>
	</tr>
	</table>
</asp:Content>