<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - BASIC INFO - TEST ENVIRONMENT" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    Dim sBgColor as String, strStatus As String = ""

    Dim IsRemoteChargePersonalNumber, IsRemoteChargeCVV2, IsRemoteChargePhoneNumber, _
    IsRemoteChargeEmail, IsApprovalOnly, IsBillingAddressMust, IsUseFraudDetectionMaxMind As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        litVisa4580.Visible = CurrentDomain.IsHebrewVisible
        litMC532614.Visible = CurrentDomain.IsHebrewVisible
        litIsraCard.Visible = CurrentDomain.IsHebrewVisible
        If WebUtils.IsLoggedin Then
            Dim sSQL As String = "SELECT IsRemoteChargePersonalNumber,IsRemoteChargeCVV2,IsRemoteChargePhoneNumber," &
            "IsRemoteChargeEmail, IsApprovalOnly, IsBillingAddressMust, IsUseFraudDetection_MaxMind " &
            " FROM tblCompany" &
            " WHERE ID = " & Merchant.ID
            Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
            If iReader.Read() Then
                IsRemoteChargePersonalNumber = iReader("IsRemoteChargePersonalNumber")
                IsRemoteChargeCVV2 = iReader("IsRemoteChargeCVV2")
                IsRemoteChargePhoneNumber = iReader("IsRemoteChargePhoneNumber")
                IsRemoteChargeEmail = iReader("IsRemoteChargeEmail")
                IsApprovalOnly = iReader("IsApprovalOnly")
                IsBillingAddressMust = iReader("IsBillingAddressMust")
                IsUseFraudDetectionMaxMind = iReader("IsUseFraudDetection_MaxMind")
            End If
            iReader.Close()
        End If
    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">TEST ENVIRONMENT</span>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td colspan="2" class="SecHeading">Test Cards<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr><td>Only the following credit card number are allowed while in test environment:<br /></td></tr>
	<tr>
		<td>
			<br />
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="left">
			<tr><th valign="bottom">Card Number<br /></th><th valign="bottom">Issuer<br /></th></tr>
			<tr><td>4387751111111111<br /></td><td>Visa (US)<br /></td></tr>
			<tr><td>5442987111111111<br /></td><td>MasterCard (US)<br /></td></tr>
			<tr><td>371000911111111<br /></td><td>American Express (US)<br /></td></tr>
			<tr><td>4056850111111111<br /></td><td>Visa (FR)<br /></td></tr>
			<tr><td>5130113111111111<br /></td><td>MasterCard (FR)<br /></td></tr>
			<tr><td>36005131111111<br /></td><td>Diners Club (GB)<br /></td></tr>
			<tr><td>3535111111111111<br /></td><td>JCB (JP)<br /></td></tr>
			<tr><td>6221701111111111<br /></td><td>LivaKash (JP)<br /></td></tr>
			<asp:Literal ID="litVisa4580" runat="server" Text="<tr><td>4580000000000000<br /></td><td>VISA (IL)<br /></td></tr>" />
			<asp:Literal ID="litMC532614" runat="server" Text="<tr><td>5326140000000000<br /></td><td>MasterCard (IL)<br /></td></tr>" />
			<asp:Literal ID="litIsraCard" runat="server" Text="<tr><td>91000000<br /></td><td>IsraCard (IL)<br /></td></tr>" />
			</table>
		</td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr><td>* Use any future expiry date with these test cards.<br /></td>
	<tr><td height="6"></td></tr>
	<tr>
		<td>
			** For American Express, pass any four digits as CVV2.<br />
			<span style="visibility:hidden;">**</span> For other credit card types, pass any three digits as CVV2.
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Controlling Reply<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr><td>Use the following amounts to cause either approval or various declines:<br /></td></tr>
	<tr>
		<td>
			<br />
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="left">
			<tr>
				<th valign="bottom">Amount<br /></th>
				<th valign="bottom">Response<br /></th>
			</tr>
			<tr>
				<td>0.04<br /></td>
				<td>Error 1001: Soft Decline (Call)<br /></td>
			</tr>
			<tr>
				<td>0.05<br /></td>
				<td>Error 1002: Insufficient Funds<br /></td>
			</tr>
			<tr>
				<td>0.90<br /></td>
				<td>Approved with 5 second delay<br /></td>
			</tr>
			<tr>
				<td>0.91<br /></td>
				<td>Approved with 10 second delay<br /></td>
			</tr>
			<tr>
				<td>0.92<br /></td>
				<td>Approved with 20 second delay<br /></td>
			</tr>
			<tr>
				<td>0.93<br /></td>
				<td>Approved with 30 second delay<br /></td>
			</tr>
			<tr>
				<td>0.94<br /></td>
				<td>Approved with 40 second delay<br /></td>
			</tr>
			<tr>
				<td>0.95<br /></td>
				<td>Approved with 50 second delay<br /></td>
			</tr>
			<tr>
				<td>0.96<br /></td>
				<td>Approved with 60 second delay<br /></td>
			</tr>
			<tr>
				<td>0.97<br /></td>
				<td>Approved with 70 second delay<br /></td>
			</tr>
			<tr>
				<td>0.98<br /></td>
				<td>Approved with 80 second delay<br /></td>
			</tr>
			<tr>
				<td>0.99<br /></td>
				<td>Approved with 90 second delay<br /></td>
			</tr>
			<tr>
				<td>553<br /></td>
				<td>3D secure simulator - this will return 553 and redirect Url to complete transaction<br /></td>
               
			</tr>
			<tr>
				<td>1.00 +<br /></td>
				<td>Approved<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr><td>* Enabled only after all data being send are in the correct format and length<br /></td></tr>
	<tr><td>** Specifying any other amount will cause reply 596 (Integration mode - incorrect charge amount)<br /></td></tr>
	</table>
	<br />
</asp:Content>