<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" EnableEventValidation="false" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Dim langTxt As String = ""
	Dim langCode As Integer = 1

	Protected Sub Page_Load()
		Select Case Trim(Request("lang"))
			Case "eng" : langTxt = "English" : langCode = 1
			Case "spa" : langTxt = "Spanish" : langCode = 2
			Case "heb" : langTxt = "Hebrew" : langCode = 0
		End Select

		'Get currency options
		If Not Page.IsPostBack Then
			cblCurrencies.DataSource = WebUtils.DomainCache.Currencies.Values
			cblCurrencies.DataTextField = "IsoCode"
			cblCurrencies.DataValueField = "ID"
			cblCurrencies.DataBind()
			Dim currencyOptions As String = dbPages.ExecScalar("SELECT IsNull(CurrencyOptions, '') FROM tblPublicPayAdmin WHERE (companyID = " & WebUtils.LoggedUser.ID & ") AND (lang = " & langCode & ")")
			If String.IsNullOrEmpty(currencyOptions) Then currencyOptions = "-1"
			For i As Integer = 0 To cblCurrencies.Items.Count - 1
				cblCurrencies.Items(i).Selected = (currencyOptions.IndexOf(cblCurrencies.Items(i).Value) > -1)
			Next
		End If
	End Sub

	Protected Sub btnUpdateCurrencies_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim sChecked As String = ""
		For Each liItem As ListItem In cblCurrencies.Items
			If liItem.Selected Then sChecked &= "," & liItem.Value
		Next
		If sChecked = String.Empty Then
			lblUpdateCurrencies.Text = "Select at least one currency!"
		Else
			lblUpdateCurrencies.Text = ""
			sChecked = sChecked.Substring(1)
			dbPages.ExecSql("UPDATE tblPublicPayAdmin SET CurrencyOptions='" & sChecked.ToSql(True) & "' WHERE CompanyID=" & WebUtils.LoggedUser.ID & " AND lang=" & langCode)
		End If
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<script type="text/javascript">
		function checkForm(frmData)
		{
			var bChecked=false;
			for (var i=0;i<frmData.elements.length;i++) if (frmData.elements[i].type=="checkbox") bChecked|=frmData.elements[i].checked;
			if (bChecked) return true;
			alert("Please select currency!");
			return false;
		}
	</script>
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
		<tr>
			<td>
				<span class="PageHeading">PUBLIC PAYMENT PAGE</span>
				<span class="PageSubHeading">- PAYMENT OPTIONS (<%=langTxt%>)</span><br />
			</td>
			<td align="right">[<a class="faq" href="PublicPage_AdminAppear.aspx?lang=<%= trim(request("lang")) %>">back to page management</a>]<br /></td>
		</tr>
		<tr>
			<td colspan="2">
				<custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" />
			</td>
		</tr>
		<tr>
			<td>
				<br />
				<br />
			</td>
		</tr>
		<tr><td colspan="2" class="SecHeading">Debit currency selection<br /></td></tr>
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<asp:CheckBoxList ID="cblCurrencies" runat="server" />
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<asp:Button ID="btnUpdateCurrencies" runat="server" Text="Update" CssClass="button1" OnClick="btnUpdateCurrencies_Click" OnClientClick="return checkForm(this.form);" />
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<asp:Label ID="lblUpdateCurrencies" runat="server" ForeColor="maroon" />
			</td>
		</tr>
	</table>
</asp:Content>