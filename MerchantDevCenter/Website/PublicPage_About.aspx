<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - PUBLIC PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
	function GetDirectLinkHTML(sLanguage as String, sPrefix as String) as String
		dim sURL as String : sURL=CurrentDomain.ProcessURL & "public_pay_" & sPrefix & ".asp?CompanyNum=" & Session("MerchantNumber")
		return "<li><b>" & sLanguage & "</b>:<br><a target=""_blank"" href=""" & sURL & """>" & sURL & "</a><br /><br /></li>"
	End Function

	function GetDirectLinkHTML(sLanguage as String) as String
		return GetDirectLinkHTML(sLanguage, sLanguage.Substring(0, 3).ToLower())
    End Function
    
    Function GetDirectLinkHTMLGuest(ByVal sLanguage As String, ByVal sPrefix As String) As String
        Dim sURL As String : sURL = CurrentDomain.ProcessURL & "public_pay_" & sPrefix & ".asp?CompanyNum=1415119"
        Return "<li><b>" & sLanguage & "</b>:<br><a target=""_blank"" href=""" & sURL & """>" & sURL & "</a><br /><br /></li>"
    End Function
    
    Function GetDirectLinkHTMLGuest(ByVal sLanguage As String) As String
        Return GetDirectLinkHTMLGuest(sLanguage, sLanguage.Substring(0, 3).ToLower())
    End Function
	
	sub Page_Load
		'
		' 20080423 Tamir
		'
		' Support for multiple devcenter sites
		'
		'
		'lblReplyURL.Text="<input class=""wide"" name=""ReplyURL"" value=""" & CurrentDomain.ProcessURL & "CheckReply.asp"" /><br />"
		'lblCompany1.Text=CurrentDomain.BrandName
		'lblEnglishOption.Text=GetLanguageSelectHTML("English")
		'lblFrenchOption.Text=GetLanguageSelectHTML("French")
		'lblSpanishOption.Text=GetLanguageSelectHTML("Spanish")
		'lblQueryProcessURL.Text="<input type=""hidden"" id=""hidQueryProcessURL"" value=""" & CurrentDomain.ProcessURL & """ />"
        'lblQueryCompany.Text="<input type=""hidden"" id=""hidQueryCompany"" value=""" & CurrentDomain.BrandName & """ />"
        If Not Session.Item("Guest") Then
            lblDirectLinkEng.Text = GetDirectLinkHTML("English")
        Else
            lblDirectLinkEng.Text = GetDirectLinkHTMLGuest("English")
        End If
        If Not Session.Item("Guest") Then
            lblDirectLinkSpa.Text = GetDirectLinkHTML("Spanish")
        Else
            lblDirectLinkSpa.Text = GetDirectLinkHTMLGuest("Spanish")
        End If
        If CurrentDomain.IsHebrewVisible Then
            If Not Session.Item("Guest") Then
                lblDirectLinkHeb.Text = GetDirectLinkHTML("Hebrew")
            Else
                lblDirectLinkHeb.Text = GetDirectLinkHTMLGuest("Hebrew")
            End If
            '	lblCurrencySelect.Text="<select name=""Currency""><option value=""0"">NIS</option><option value=""1"" selected>Dollar</option></select>"
            '	lblHebrewOption.Text=GetLanguageSelectHTML("Hebrew *")
            '	lblHebrewWindow.Text="<div>* Hebrew window does not allow e-check transactions.</div>"
        Else
            lblDirectLinkHeb.Text = ""
            '	lblCurrencySelect.Text="<select name=""Currency""><option value=""1"" selected>Dollar</option></select>"
            '	lblHebrewOption.Text=""
            '	lblHebrewWindow.Text=""
        End If
    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">CREDIT CARD</span>
			<span class="PageSubHeading">- PUBLIC PAYMENT PAGE</span><br />
			<custom:AuthAlert PropertyToCheck="PublicPayment" runat="server" />
		</td>
	</tr>
	<tr>
		<td valign="top">
			<br />
			<table cellspacing="0" cellpadding="1" border="0" width="100%">
			<tr>
				<td valign="top">
				
					<img src="../<%=WebUtils.CurrentDomain.ThemeFolder%>/images/screenshot_publicpage.jpg" style="float:right; margin:0 0 20px 20px;" alt="" border="0">
					<span class="SecHeading">What is Public Payment Page</span><br />
					Public payment page is a web page which contains your own personal payment form.<br />
					Using this web page, clients surfing the net, can make payments to your account.<br />
					Public payment page is accessible to any one who knows its address.<br />
					<br />
					You can manage Public Payment Page, to add text in various places, pre define fixed fields, or let the client have more control.<br />
					For example, payment amount and number of payments fields, can be pre defined by you, or you can let the client decide for himself, how much and with how many payments he wants to pay.<br />
					<br /><br />
					<span class="SecHeading">When to use</span><br />
					Public payment page can be useful for the merchant that has no payment web page and does not want to build one.<br />
					<br /><br />
					<span class="SecHeading">How to use</span><br />
					To use Public payment page, follow these 3 simple steps:<br />
					<br />
					<ol>
						<li>
							Make sure Public Pay is Active, by checking the indicator on the top right side of this page.
							If Inactive is indicated, please contact <%= WebUtils.CurrentDomain.BrandName %> support.
						</li>
						<li>
							Use MANAGEMENT > PUBLIC PAYMENT PAGE >
							<a href="publicPage_AdminAppear.aspx?lang=eng"<%if Session.Item("Guest")%> onclick="return false"<%End If%>>Manage Appearance</a>
							and <a href="publicPage_AdminCode.aspx?lang=eng"<%if Session.Item("Guest")%> onclick="return false"<%End If%>>Manage Code</a>
							to allow access to the page, define it's appearance and to choose the payment options
							to include in the form.
						</li>
						<li>
							Use the following address to refer directly to the payment page from your web site using a hyper link:<br />
							<br />
							<ul style="list-style-type:square;">
								<asp:Label ID="lblDirectLinkEng" runat="server" />
								<asp:Label ID="lblDirectLinkSpa" runat="server" />
								<asp:Label ID="lblDirectLinkHeb" runat="server" />
							</ul>
						</li>
					</ol>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
		
</asp:Content>

