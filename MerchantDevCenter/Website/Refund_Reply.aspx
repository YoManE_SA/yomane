<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - REFUND REQUESTS - Checking Reply" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
	Protected Sub Page_Load()
		lblCompany1.Text = CurrentDomain.BrandName
		dsStatus.ConnectionString = dbPages.DSN
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">REFUND REQUESTS</span>
			<span class="PageSubHeading">- CHECKING REPLY</span><br />
			<custom:AuthAlert PropertyToCheck="AskRefund" runat="server" />
		</td>
	</tr>
	<tr>
		<td>
			<br /><br />
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr>
				<td>
					After the remote refund request is received and processed, <asp:Label ID="lblCompany1" runat="server" /> system returns its reply in QueryString format.<br />
					This reply is for the refund request only.<br /> 
					To recive notification when refund is processed	use the following setting page <a style="font-size:90%;" href="Notifications.aspx">GLOBAL SETTINGS --> NOTIFICATIONS</a>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Response Fields<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td style="color:#003366;vertical-align:top;">Reply<br /></td>
				<td>Reply code (see list below).<br /></td>
			</tr>
			<tr>
				<td style="color:#003366;vertical-align:top;">ReplyDesc<br /></td>
				<td>Reply description<br /></td>
			</tr>
			<tr>
				<td style="color:#003366;vertical-align:top;">RequestID<br /></td>
				<td>
					CurrentDomain number of the created refund request.<br />
					Appears only when the requested action is CREATE.<br />
				</td>
			</tr>
			<tr>
				<td style="color:#003366;vertical-align:top;">RequestCount<br /></td>
				<td>
					Number of refund requests for the specified transaction.<br />
					When the requested action is CREATE, includes the created refund request as well as all previous requests.<br />
					When the requested action is DELETE, the deleted refund request is not counted.<br />
				</td>
			</tr>
			<tr>
				<td style="color:#003366;vertical-align:top;">RequestedAmount<br /></td>
				<td>
					Overall amount of all refund requests for the specified transaction.<br />
					When the requested action is CREATE, includes the created refund request as well as all previous requests.<br />
					When the requested action is DELETE, the deleted refund request is not counted.<br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td>- REPEATED FIELDS<br /></td></tr>
	<tr>
		<td>
			These fields appear for each refund request.<br />
			The name of each field instance consists of the field name followed by the refund request index (from 1 to RequestCount).<br />
			In the following description the refund request index is omitted.<br />
			<br />
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<td style="color:#003366;vertical-align:top;">ID<br /></td>
				<td>CurrentDomain number of the refund request.</td>
			</tr>
			<tr>
				<td style="color:#003366;vertical-align:top;">Amount<br /></td>
				<td>The amount to be refunded in this request.<br /></td>
			</tr>
			<tr>
				<td style="color:#003366;vertical-align:top;">Confirm<br /></td>
				<td>
					Refund confirmation number.<br />
					For status other than ACCEPTED, empty string is returned.<br />
				</td>
			</tr>
			<tr>
				<td style="color:#003366;vertical-align:top;">Date<br /></td>
				<td>
					Creation date and time of the refund request.<br />
					The format is DD/MM/yyyy HH:mm:ss.<br />
				</td>
			</tr>
			<tr>
				<td style="color:#003366;vertical-align:top;">Status<br /></td>
				<td>
					The current status of the refund request.	Can be one of the following values:<br />
					<asp:Repeater DataSourceID="dsStatus" runat="server">
						<ItemTemplate><asp:Label Text='<%# Eval("GD_Text").ToUpper() %>' runat="server" /></ItemTemplate>
						<SeparatorTemplate>, </SeparatorTemplate>
						<FooterTemplate>.</FooterTemplate>
					</asp:Repeater>
					<asp:SqlDataSource ID="dsStatus" runat="server" DataSourceMode="DataReader" SelectCommand="SELECT DISTINCT GD_Text FROM tblGlobalData WHERE GD_Group=63 AND GD_Lng=1" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Response EXAMPLES<br /><br /></td></tr>
	<tr>
		<td style="padding-bottom:4px;">
			1. Reply in case of success (STATUS - two refund requests found):<br />
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					Reply=0&amp;ReplyDesc=Success&amp;RequestCount=2&amp;RequestedAmount=123.00<br />
					&amp;ID1=12345&amp;Amount1=100.00&amp;Confirm1=&amp;Date1=31/05/2009 15:59:00&amp;Status1=Pending<br />
					&amp;ID2=12346&amp;Amount2=23.00&amp;Confirm2=&amp;Date2=31/05/2009 16:01:00&amp;Status2=Pending
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td style="padding-bottom:4px;">
			2. Reply in case of success (STATUS - no refund request found):<br />
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					Reply=0&amp;ReplyDesc=Success&amp;RequestCount=0&amp;RequestedAmount=0.00
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td style="padding-bottom:4px;">
			3. Reply in case of success (DELETE - the only refund request was deleted):<br />
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					Reply=0&amp;ReplyDesc=Success&amp;RequestCount=0&amp;RequestedAmount=0.00
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td style="padding-bottom:4px;">
			4. Reply in case of success (CREATE):<br />
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					Reply=0&amp;ReplyDesc=Success&amp;RequestID=13315&amp;RequestCount=2&amp;RequestedAmount=123.00<br />
					&amp;ID1=12345&amp;Amount1=100.00&amp;Confirm1=&amp;Date1=31/05/2009 15:59:00&amp;Status1=Pending<br />
					&amp;ID2=12346&amp;Amount2=23.00&amp;Confirm2=&amp;Date2=31/05/2009 16:01:00&amp;Status2=Pending
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td style="padding-bottom:4px;">
			5. Reply in case of failure:<br />
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					Reply=4&amp;ReplyDesc=<%=dbPages.ExecScalar("SELECT GD_Text FROM tblGlobalData WHERE GD_Group=64 AND GD_Lng=1 AND GD_ID=4")%>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Reply Codes<br /></td></tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<netpay:GlobalDataGroupValueTable GlobalDataGroup="RefundRequestReplyCode" runat="server" />
		</td>
	</tr>
	</table>
</asp:Content>