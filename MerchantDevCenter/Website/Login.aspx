<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - LOGIN" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Request.QueryString("login") = "outer" Then
                Dim host As String = Request.ServerVariables("HTTP_HOST")
                If host = "localhost" Or host = "localhost:83" Then
                    loginEmail.Text = Request.QueryString("mail")
                    loginUserName.Text = Request.QueryString("username")
                    loginPassword.TextMode = TextBoxMode.SingleLine
                    loginPassword.Text = Request.QueryString("userpassword")
                    loginPassword.Visible = False
                    btnLogin_Click(Nothing, Nothing)
                Else
                    Dim referer As String = Request.ServerVariables("HTTP_REFERER")
                    If referer <> Nothing Then
                        referer = referer.Trim().ToLower()
                        If referer <> "" Then
                            If referer.IndexOf("/admincash/") > 0 Then
                                loginEmail.Text = Request.QueryString("mail")
                                loginUserName.Text = Request.QueryString("username")
                                loginPassword.TextMode = TextBoxMode.SingleLine
                                loginPassword.Text = Request.QueryString("userpassword")
                                loginPassword.Visible = False
                                btnLogin_Click(Nothing, Nothing)
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Sub AlertAndFocus(ByVal sMessage As String, ByRef txtField As TextBox)
        lblError.Text = sMessage & "<br /><br />"
        txtField.Focus()
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim email As String = loginEmail.Text.Trim().ToSql(True)
        Dim userName As String = loginUserName.Text.Trim().ToSql(True)
        Dim password As String = loginPassword.Text.Trim().ToSql(True)

        If email = String.Empty Then
            AlertAndFocus("The field is required: Email", loginEmail)
        ElseIf userName = String.Empty Then
            AlertAndFocus("The field is required: Username", loginUserName)
        ElseIf password = String.Empty Then
            AlertAndFocus("The field is required: Password", loginPassword)
        Else
            Dim lr = WebUtils.Login(New Netpay.Infrastructure.Security.UserRole() {Netpay.Infrastructure.Security.UserRole.Merchant, Netpay.Infrastructure.Security.UserRole.MerchantSubUser}, userName, email, password)
            'If lr = LoginResult.UserNotFound Then
            '	lr = WebUtils.Login(UserType.MerchantLimited, userName, email, password)
            'End If
            If lr <> Netpay.Infrastructure.Security.LoginResult.Success Then
                AlertAndFocus("Merchant not found or access denied!<br>Try to login to your Merchant Control Panel<br>for more detailed information.", loginPassword)
                Return
            End If
        End If

        If Not WebUtils.IsLoggedin Then
            AlertAndFocus("Merchant not found or access denied!<br>Try to login to your Merchant Control Panel<br>for more detailed information.", loginPassword)
        Else
            Response.Redirect("Default.aspx", True)
        End If
    End Sub

    Sub btnGuestLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If WebUtils.IsLoggedin Then WebUtils.Logout()
        Response.Redirect(".", True)
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<br /><br /><br /><br />
	<table align="center" style="width:330px;">
		<tr>
			<td style="text-align:left; font-size:12px; padding-bottom:8px;"">
				To use Developer Center please login<br />
			</td>
		</tr>
		<tr>
			<td>
				<table align="center" style="width:100%; border:1px solid #E6E2D8; background-color:#F7F6F3; padding:8px;">
					<tr>
						<td style="font-size:13px;padding-top:8px; padding-bottom:8px;">
							<asp:ValidationSummary ID="vsLogin" DisplayMode="BulletList" ValidationGroup="Login" ShowMessageBox="True" runat="server" ShowSummary="False" />
							<table align="center" id="loginForm">
								<tr>
									<td colspan="2" style="text-align:left;font-size:12px;font-weight:bold;">
										<asp:Label ID="lblError" runat="server" ForeColor="Maroon" />
									</td>
								</tr>
								<tr>
									<th>Email</th>
									<td>
										<asp:TextBox ID="loginEmail" runat="server" AutoCompleteType="None" autocomplete="off" />
										<asp:RequiredFieldValidator ID="loginEmailValidator" ControlToValidate="loginEmail" ErrorMessage="Email is required" Display="None" ValidationGroup="Login" runat="server" />
										<asp:RegularExpressionValidator id="loginEmailValidatorRegex" ControlToValidate="loginEmail" ErrorMessage="Wrong format of email address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None" ValidationGroup="Login" Runat="server" />
									</td>
								</tr>
								<tr>
									<th>Username</th>
									<td>
										<asp:TextBox ID="loginUserName" runat="server" AutoCompleteType="None" autocomplete="off" />
										<asp:RequiredFieldValidator ID="loginUserNameValidator" ControlToValidate="loginUserName" ErrorMessage="Username is required" Display="None" ValidationGroup="Login" runat="server" />
									</td>
								</tr>
								<tr>
									<th>Password</th>
									<td>
										<asp:TextBox TextMode="Password" ID="loginPassword" runat="server" AutoCompleteType="None" autocomplete="off" />
										<asp:RequiredFieldValidator ID="loginPasswordValidator" ControlToValidate="loginPassword" ErrorMessage="Password is required" Display="None" ValidationGroup="Login" runat="server" />
									</td>
								</tr>
								<tr>
									<td></td>
									<td align="right">
										<asp:Button ID="btnLogin" UseSubmitBehavior="true" ValidationGroup="Login" runat="server" Text="LOGIN" CssClass="submit" OnClick="btnLogin_Click" />
									</td>
								</tr>
								<tr><td colspan="2"><hr size="1" noshade="noshade" color="silver" /></td></tr>
								<tr>
									<th>Guest Login</th>
									<td align="right">
										<asp:LinkButton ID="Button2" runat="server" Text="ENTER" CssClass="submit2" OnClick="btnGuestLogin_Click"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br />
</asp:Content>