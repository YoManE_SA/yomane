<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>

<script runat="server" type="text/VB">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If Not IsPostBack Then
            url_redirect.Text = CurrentDomain.ProcessV2URL & "hosted/checkReply.aspx"
            merchantID.Text = Merchant.Number
            trans_currency.SelectedIndex = 2
            trans_type.Items.Add(New ListItem("Capture", CInt(TransactionType.Capture)))
            trans_type.Items.Add(New ListItem("Authorization", CInt(TransactionType.Authorization)))
            trans_type.SelectedIndex = 0
            trans_recurring1.Enabled = Authorization.IsAllowed(AuthorizationProperty.AllowRecurring)
            trans_recurring2.Enabled = trans_recurring1.Enabled
            cblPaymentType.Items(0).Enabled = Authorization.IsAllowed(AuthorizationProperty.HostedPaymentV2)
            cblPaymentType.Items(1).Enabled = Authorization.IsAllowed(AuthorizationProperty.HostedPaymentV2Echeck)
            cblPaymentType.Items(2).Enabled = Authorization.IsAllowed(AuthorizationProperty.HostedPaymentV2Instant)
            cblPaymentType.Items(3).Enabled = Authorization.IsAllowed(AuthorizationProperty.HostedPaymentV2Wallet)
            For Each li As ListItem In cblPaymentType.Items
                li.Selected = li.Enabled
            Next
            If (Authorization.IsAnyHostedV2Enabled) Then
                Select Case dbPages.TestVar(Request("T"), 0, -1, 1)
                    Case "1"
                        aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2CreditCard
                        aa.formatFeatureMessage("Credit Card Hosted Page")
                    Case "2"
                        aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Echeck
                        aa.formatFeatureMessage("Echeck Hosted Page")
                    Case "3"
                        aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Instant
                        aa.formatFeatureMessage("Alternative payment methods Hosted Page")
                End Select
            Else
                aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2CreditCard
            End If
        End If
    End Sub

    Protected Function GetOpenString() As String
        Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder()
        sb.Append(WebUtils.CurrentDomain.ProcessV2Url & "/Hosted/")
        sb.Append("?merchantID=" & HttpUtility.UrlEncode(merchantID.Text))
        sb.Append("&url_redirect=" & HttpUtility.UrlEncode(url_redirect.Text))
        sb.Append("&url_notify=" & HttpUtility.UrlEncode(url_notify.Text))
        If Not String.IsNullOrEmpty(trans_refNum.Text) Then sb.Append("&trans_refNum=" & HttpUtility.UrlEncode(trans_refNum.Text))
        If Not String.IsNullOrEmpty(disp_payFor.Text) Then sb.Append("&disp_payFor=" & HttpUtility.UrlEncode(disp_payFor.Text))
        sb.Append("&trans_type=" & trans_type.SelectedValue)
        sb.Append("&trans_amount=" & HttpUtility.UrlEncode(trans_amount.Value))
        If trans_installments.SelectedItem IsNot Nothing Then sb.Append("&trans_installments=" & HttpUtility.UrlEncode(trans_installments.SelectedValue))
        If trans_currency.SelectedItem IsNot Nothing Then sb.Append("&trans_currency=" & HttpUtility.UrlEncode(WebUtils.DomainCache.GetCurrency(trans_currency.SelectedValue.ToInt32(0)).IsoCode))
        If Not String.IsNullOrEmpty(trans_comment.Text) Then sb.Append("&trans_comment=" & HttpUtility.UrlEncode(trans_comment.Text))
        Dim sPaymentType As String = String.Empty
        For Each li As ListItem In cblPaymentType.Items
            If li.Selected Then sPaymentType &= IIf(String.IsNullOrEmpty(sPaymentType), String.Empty, ",") & li.Value
        Next
        If Not String.IsNullOrEmpty(sPaymentType) Then sb.Append("&disp_paymentType=" & sPaymentType)
        If disp_lng.SelectedItem IsNot Nothing Then sb.Append("&disp_lng=" & HttpUtility.UrlEncode(disp_lng.SelectedValue))
        If Not String.IsNullOrEmpty(trans_recurring1.Text) Then sb.Append("&trans_recurring1=" & HttpUtility.UrlEncode(trans_recurring1.Text))
        If Not String.IsNullOrEmpty(trans_recurring2.Text) Then sb.Append("&trans_recurring2=" & HttpUtility.UrlEncode(trans_recurring2.Text))
        If Not String.IsNullOrEmpty(merchantID.Text) Then
            Dim pMerchant As MerchantVO = Netpay.Bll.Merchants.GetMerchant(WebUtils.DomainHost, merchantID.Text)
            If pMerchant IsNot Nothing Then
                Dim sSign As String = merchantID.Text + trans_amount.Value & WebUtils.DomainCache.GetCurrency(trans_currency.SelectedValue.ToInt32(0)).IsoCode & pMerchant.HashKey
                Dim md As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()
                sSign = System.Convert.ToBase64String(md.ComputeHash(System.Text.Encoding.Default.GetBytes(sSign.ToString())))
                sb.Append("&signature=" + HttpUtility.UrlEncode(sSign))
            End If
        End If
        Return sb.ToString()
    End Function

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As EventArgs)
        txQuery.Text = GetOpenString()
        txQuery.Style.Remove("display")
        btnHideQuery.Style.Remove("display")
    End Sub

    Protected Sub btnOpenPage_Click(ByVal sender As Object, ByVal e As EventArgs)
        OpenScript.Text = "<script type=""text/javascript"">window.open('" & GetOpenString() & "', 'fraPay', 'scrollbars=1, width=680, height=800, resizable=1, Status=1, top=200, left=250');</" + "script>"
    End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	 <script type="text/javascript" language="JavaScript">
	 	function HideQuery() {
	 		document.getElementById('<%=txQuery.ClientID %>').value = "";
	 		document.getElementById('<%=txQuery.ClientID %>').style.display = "none";
	 		document.getElementById('<%=btnHideQuery.ClientID %>').style.display = "none";
	 	}

	 	function IsKeyDigit() {
	 		var nKey = event.keyCode;
	 		if ((nKey == 8) | (nKey == 9) | (nKey == 13) | (nKey == 35) | (nKey == 36) | (nKey == 37) | (nKey == 39) | (nKey == 46) | (nKey == 48) | (nKey == 49) | (nKey == 50) | (nKey == 51) | (nKey == 52) | (nKey == 53) | (nKey == 54) | (nKey == 55) | (nKey == 56) | (nKey == 57) | (nKey == 96) | (nKey == 97) | (nKey == 98) | (nKey == 99) | (nKey == 100) | (nKey == 101) | (nKey == 102) | (nKey == 103) | (nKey == 104) | (nKey == 105) | (nKey == 110) | (nKey == 190)) { return true; };
	 		return false;
	 	}

	</script>
	<style type="text/css"> input.wide { width:400px; } input.option { vertical-align:middle; } </style>
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">HOSTED PAYMENT PAGE VER.2</span>
			<span class="PageSubHeading">- EXAMPLE FORM</span><br />
			<custom:AuthAlert ID="aa" PropertyToCheck="HostedPaymentV2" runat="server" />
		</td>
	</tr>
	<tr>
		<td>
			<br />
			The following form launches a window with the hosted payment page helping you to see and experience this method of payment.<br />
			<br /><br />
		</td>
	</tr>
	<tr>
		<td valign="top">
			<table class="FormTable" border="0" cellspacing="2" cellpadding="1">
			<tr><td class="SecHeading" colspan="2">Transaction Data<br /><br /></td></tr>
			<tr>
				<td>Merchant ID<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="merchantID" value="" /><br /></td>
			</tr>
			<tr>
				<td>Redirect URL<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="url_redirect" value="" /><br /></td>
			</tr>
			<tr>
				<td>Notify URL<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="url_notify" value="" /><br /></td>
			</tr>
			<tr>
				<td>Reference Code<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="trans_refNum" value="" /><br /></td>
			</tr>
			<tr>
				<td>Comment<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="trans_comment" value="Testing hosted payment page" /><br /></td>
			</tr>
			<tr>
				<td>Amount<br /></td>
				<td><input runat="server" onkeydown="return IsKeyDigit();" id="trans_amount" value="1.50" /><br /></td>
			</tr>
			<tr>
				<td>Currency<br /></td>
				<td><netpay:CurrencyDropDown ID="trans_currency" runat="server" StartEmpty="false" /></td>
			</tr>
			<tr>
				<td>Type<br /></td>
				<td><netpay:DropDownBase ID="trans_type" runat="server" /></td>
			</tr>
			<tr>
				<td valign="top">Installments<br /></td>
				<td>
					<asp:DropDownList runat="server" id="trans_installments">
						<asp:ListItem value="1" Text="1" />
						<asp:ListItem value="2" Text="2" />
						<asp:ListItem value="3" Text="3" />
					</asp:DropDownList>
				</td>
			</tr>
			<tr><td><br /><br /></td></tr>
			<tr><td class="SecHeading" colspan="2">Recurring settings:</td></tr>	
			<tr><td colspan="2"><div><custom:AuthAlert PropertyToCheck="AllowRecurring" runat="server" /></div><br /></td></tr>	
			<tr>
				<td>Recurring String 1<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="trans_recurring1" /><br /></td>
			</tr>
			<tr>
				<td>Recurring String 2<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="trans_recurring2" /><br /></td>
			</tr>
			<tr><td><br /><br /></td></tr>
			<tr><td class="SecHeading" colspan="2">Display Options:<br /><br /></td></tr>
			<tr>
				<td>Pay For text<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="disp_payFor" value="Purchase" /><br /></td>
			</tr>
			<tr>
				<td>Payment Type<br /></td>
				<td>
					<asp:CheckBoxList id="cblPaymentType" CssClass="option" RepeatColumns="4" RepeatLayout="Flow" runat="server">
						<asp:ListItem runat="server" Text="Credit Card" Value="CC" />
						<asp:ListItem runat="server" Text="Electronic Check" Value="EC" />
						<asp:ListItem runat="server" Text="Instant Debit" Value="ID" />
						<asp:ListItem runat="server" Text="Wallet" Value="WT" />
					</asp:CheckBoxList>
				</td>
			</tr>
			<tr>
				<td>Default UI language<br /></td>
				<td>
					<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<asp:DropDownList runat="server" ID="disp_lng">
							 <asp:ListItem Text="English" Value="en-us" Selected="True" />
							 <asp:ListItem Text="Hebrew" Value="he-il" />
							 <asp:ListItem Text="Italian" Value="it-it" />
							</asp:DropDownList>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left">
					<asp:Button runat="server" ID="btnOpenPage" OnClick="btnOpenPage_Click" UseSubmitBehavior="true" Text="Open Window" style="background-color:white;font-size:10px;width:130px;cursor:pointer;" /><br />
				</td>
				<td width="50"></td>
				<td align="left">
					<asp:Button runat="server" ID="btnGenerate" OnClick="btnGenerate_Click" Text="Generate Link" style="background-color:white;font-size:10px;width:130px;cursor:pointer;" /><br />
				</td>
			</tr>
			<tr>
				<td style="text-align:left; padding-top:6px;">		
					See and understand what<br />"Hosted Payment Page" is
				</td>
				<td></td>
				<td style="text-align:left; padding-top:6px;">
					Generate an Html snippet<br />for pasting into a web page
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<asp:TextBox runat="server" ReadOnly="true" id="txQuery" TextMode="MultiLine" Rows="5" style="border:1px dashed #c0c0c0;background-color:#f7f7f7;display:none;overflow:auto;width:550px;height:115px;font:normal 12px Courier New, Monospace;" />
			<br />
			<asp:Button runat="server" id="btnHideQuery" OnClientClick="HideQuery();return false;" Text="Hide Link" style="display:none;background-color:white;font-size:10px;width:115px;cursor:pointer;" />
		</td>
	</tr>
	</table>
  <asp:Literal ID="OpenScript" runat="server" Mode="PassThrough" />
 </asp:Content>