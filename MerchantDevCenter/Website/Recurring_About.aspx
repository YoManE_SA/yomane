<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<script runat="server">

		Dim sBgColor as String = ""
		
	</script>

	<table cellspacing="0"  cellpadding="0" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">HOSTED PAYMENT PAGE</span>
			<span class="PageSubHeading">- CHECK REPLY</span><br />
			<custom:AuthAlert PropertyToCheck="AllowRecurring" runat="server" />
		</td>
		<td valign="top" align="right">

		</td>
	</tr>
	<tr>
		<td colspan="2">
			<br />
			A Remote transaction reply is sent by the <%= WebUtils.CurrentDomain.BrandName %> system to your reply address after a transaction has been submitted and processed by the <%= WebUtils.CurrentDomain.BrandName %> authorization center.
			The reply string will be send to the address that merchant has submitted in the ReplyURL field, Merchant must check that reply URL in order to interpreted authorization center reply.
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td colspan="2" class="SecHeading">Example of a returned answer<br /></td>
	</tr>
	<tr>
		<td colspan="2" >
			<table border="0" style="border:1px dashed #c0c0c0;" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td  bgcolor="#f7f7f7">
					http://www.yoursite.co.il/Reply.asp<sup style="color:navy;">(1)</sup>
					?Reply=000<sup style="color:#003366;">(2)</sup>
					&TransID=1000<sup style="color:#003366;">(3)</sup><br />
					&Date=<%= now() %><sup style="color:#003366;">(4)</sup>
					&TypeCredit=1<sup style="color:#003366;">(5)</sup>
					&Payments=1<sup style="color:#003366;">(6)</sup>
					&Amount=1.50<sup style="color:#003366;">(7)</sup><br />
					&Currency=0<sup style="color:#003366;">(8)</sup>
					&PayFor=shoes<sup  style="color:#003366;">(9)</sup>
					&PayerID=122<sup style="color:#003366;">(10)</sup>
					&Order=12345<sup style="color:#003366;">(11)</sup><br />
					&Comment=red addidas, size 9<sup style="color:#003366;">(12)</sup>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td colspan="2">
			<table border="0" width="100%" cellspacing="0" cellpadding="3">
			<tr>
				<td style="color:#003366;" valign="top">(1)<br /></td>
				<td  colspan="2">
					Location and file name of the page which the reply transaction string is been returned to, this page must contain code for acceptance of data.<br />
				</td>
			</tr>
			<tr><td height="12"></td></tr>
			<tr>
				<td width="7%" style="color:#003366;">(2)<br /></td>
				<td width="20%">(Reply)<br /></td>
				<td >Returns transaction authorization (see list below)<br /></td>
			</tr>
			<tr>
				<td style="color:#003366;">(3)<br /></td>
				<td>(TransID)<br /></td>
				<td >Returns <%= WebUtils.CurrentDomain.BrandName %>'s transaction number<br /></td>
			</tr>
			<tr>
				<td style="color:#003366;">(4)<br /></td>
				<td>(Date)<br /></td>
				<td >Returns <%= WebUtils.CurrentDomain.BrandName %>'s transaction time and date<br /></td>
			</tr>
			<tr>
				<td style="color:#003366;">(5)<br /></td>
				<td>(TypeCredit)<br /></td>
				<td >Returns credit type<br /></td>
			</tr>
			<tr>
				<td style="color:#003366;">(6)<br /></td>
				<td>(Payments)<br /></td>
				<td >Returns number of payments<br /></td>
			</tr>
			<tr>
				<td width="7%" style="color:#003366;">(7)<br /></td>
				<td width="12%">(Amount)<br /></td>
				<td >Returns transaction amount<br /></td>
			</tr>
			<tr>
				<td style="color:#003366;">(8)<br /></td>
				<td>(Currency)<br /></td>
				<td >Returns the transaction currency<br /></td>
			</tr>
			<tr>
				<td width="7%" style="color:#003366;">(9)<br /></td>
				<td width="12%">(PayFor)<br /></td>
				<td >Returns the text submitted in filed<br /></td>
			</tr>
			<tr>
				<td style="color:#003366;">(10)<br /></td>
				<td>(PayerID)<br /></td>
				<td >Returns the variable submitted to <%= WebUtils.CurrentDomain.BrandName %><br /></td>
			</tr>
			<tr>
				<td style="color:#003366;">(11)<br /></td>
				<td>(Order)<br /></td>
				<td >Returns the Transaction property as it was sent by the merchant<br /></td>
			</tr>
			<tr>
				<td style="color:#003366;">(12)<br /></td>
				<td>(Comment)<br /></td>
				<td >Returns the comment field as it was sent by the merchant<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td colspan="2" class="SecHeading">
			Reply Codes<br />
		</td>
	</tr>
	<tr><td height="8"></td></tr>
	<tr>
		<td colspan="2" >
			- For a more detail list of reply codes <a href="List_ReplyCodes.aspx">click here</a><br />
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td colspan="2">
			<table border="0" width="100%" cellspacing="0" cellpadding="1">
			<tr>
				<td>000 = Authorized transaction<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td>001 = Transaction accepted, waiting authorization<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td>xxx = Transaction was not Authorized, see unauthorized comment explanation<br /></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
		
</asp:Content>

