﻿<%@ Page Title="DEVELOPER CENTER - STORAGE ABOUT" Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"   %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
	Sub Page_Load()
		If Not Page.IsPostBack Then
			Select Case Request("T")
				Case "2" : aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Echeck
				Case "3" : aa.PropertyToCheck = AuthorizationProperty.HostedPaymentV2Instant
			End Select
		End If
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<table cellspacing="0"  cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">PAYMENT METHOD STORAGE</span>
			<span class="PageSubHeading">- ABOUT</span><br />
			<custom:AuthAlert ID="aa" PropertyToCheck="HostedPaymentV2" runat="server" />
		</td>
		<td valign="top" align="right">
		</td>
	</tr>		
	<tr>
		<td colspan="2" valign="top">
			<br />
			<table cellspacing="0" cellpadding="1" border="0" width="100%">
			<tr>
				<td>
					Payment Method Storage Page is suitable for all web sites that need to store payment methods for future use.<br />
					We host and manage the storage page, which is secured by it's own SSL certificate and customisable via an online wizard.<br />
					Suited to all sizes of merchants, it is highly cost effective and operates on an extremely robust infrastructure.<br />
					<ul class="aboutList">
						<li>Multilanguage - speak to your customers in their own language</li>
						<li>Cost savings - does not require an SSL certificate</li>
						<li>Ensures merchants are compliant with scheme regulations (PCI DSS compliant solution)</li>
					</ul>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
</asp:Content>
