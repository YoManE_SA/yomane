<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.PopupPage" %>

<script runat="server">
	Dim bClose As Boolean = False
	Protected Sub Upload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim PPCO_ID As Integer = dbPages.TestVar(Request("ID"), 0, -1, 0)
		If Request.Files.Count > 0 Then
			Dim ImageFileName As String = System.IO.Path.GetFileName(Request.Files(0).FileName)
			Dim ext as String = System.IO.Path.GetExtension(ImageFileName).ToLower()
			If ext = ".jpg" Or ext = ".gif" or ext = ".png" Then
				ImageFileName = PPCO_ID.ToString().Trim() & "_" & ImageFileName
				Request.Files(0).SaveAs(Server.MapPath("/GlobalData/PublicPageLogos/" + ImageFileName))
				dbPages.ExecSql("Update tblPublicPayChargeOptions Set ImageFileName='" & ImageFileName.ToSql(True) & "' WHERE companyID=" & WebUtils.LoggedUser.ID & " And id=" & PPCO_ID)
				Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">opener.location.reload();window.close();</s" & "cript>")
				Response.End()
			End If
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Update Image</title>
</head>
<body>
	<form id="form1" runat="server">
		<asp:FileUpload runat="server" ID="ImageFile" /> <br />
		<asp:Button runat="server" Text="Upload" OnClick="Upload_Click" />
	</form>
	<script language="javascript" type="text/javascript">
		form1.ImageFile.focus();
	</script>
</body>
</html>
