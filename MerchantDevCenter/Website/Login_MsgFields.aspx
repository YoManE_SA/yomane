﻿<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="LOGIN - MESSAGE FIELDS" %>

<script runat="server">
    Dim xmlData As String
    Public Sub Page_Load()
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
		<tr>
			<td>
				<span class="PageHeading">LOGIN TOKEN</span><br />
			</td>
		</tr>
		<tr>
			<td>
				Service to request a credentials token, used in other services for getting an authorize access.
			</td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">Service URL<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td><%=CurrentDomain.WebServicesUrl %>Merchants.asmx/Login<br /></td>
		</tr>
		<tr><td><br /><br /></td></tr>
		<tr><td class="SecHeading">Request Formats<br /></td></tr>
		<tr><td><br /></td></tr>
		<tr><td>
            The service accept calls in the following protocols:<br />
            SOAP, XML, HTTP POST and JSON<br /><br />
            for SOAP calls you may use the wsdl declaration in the following address:<br />
            <a href="<%=CurrentDomain.WebServicesUrl %>Merchants.asmx?WSDL"><%=CurrentDomain.WebServicesUrl %>Merchants.asmx?WSDL</a>
            <br /><br />
        </td></tr>
        <tr><td style="border:1px dashed #c0c0c0; padding:5px; background-color:#f7f7f7;">
        <%
            xmlData = _
            "<Login xmlns=""http://netpay-intl.com/"">" & vbCrLf & _
            "   <email>string</email>" & vbCrLf & _
            "   <userName>string</userName>" & vbCrLf & _
            "   <password>string</password>" & vbCrLf & _
            "   <signature>string</signature>" & vbCrLf & _
            "</Login>"
            Response.Write("<pre>" & Server.HtmlEncode(xmlData) & "</pre>")
        %>
        </td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
						<th width="46">Max<br />Length<br /></th>
						<th width="80" valign="bottom">Required<br /></th>
					</tr>
					<tr>
						<td>email</td>
						<td>your login email<br /></td>
						<td>255</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>userName</td>
						<td>your login user name</td>
						<td>50</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>password</td>
						<td>your login password</td>
						<td>50</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>signature</td>
						<td>
							Signature for verifying the authenticity of the request parameters.<br />
							Field values to use: <i>email + userName + password + hashKey</i><br /><br />
							Refer to <a style="font-size:90%;" href="Signature.aspx">BASIC INFO --> SIGNATURE</a> for detailed explaniation.<br />
                        </td>
						<td>64</td>
						<td>Yes</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr><td><br /></td></tr>
		<tr><td class="SecHeading">Response Formats<br /></td></tr>
		<tr><td><br /></td></tr>
        <tr><td style="border:1px dashed #c0c0c0; padding:5px; background-color:#f7f7f7;">
        <%
            xmlData = _
            "<LoginResult>" & vbCrLf & _
            "   <Result>string</Result>" & vbCrLf & _
            "   <CredentialsToken>guid</CredentialsToken>" & vbCrLf & _
            "</LoginResult>"
            Response.Write("<pre>" & Server.HtmlEncode(xmlData) & "</pre>")
        %>
        </td></tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<table class="FieldsTable" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
					<tr>
						<th width="110" valign="bottom">Field<br /></th>
						<th valign="bottom">Description<br /></th>
						<th width="46">Max<br />Length<br /></th>
						<th width="80" valign="bottom">Required<br /></th>
					</tr>
					<tr>
						<td>Result</td>
						<td>a string with result of login<br /></td>
						<td>255</td>
						<td>Yes</td>
					</tr>
					<tr>
						<td>CredentialsToken *</td>
						<td>a guid that will be used to subsequent call to the service</td>
						<td>32</td>
						<td>Yes</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><br />* The token expires after 20 minutes</td></tr>
	</table>
</asp:Content>