<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"  Title="MANAGE RECURRING SERIES - CHECKING REPLY" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Common/AuthAlert.ascx" TagPrefix="custom" TagName="AuthAlert" %>
<script runat="server">
	Protected Sub Page_Load()
		lblCompany1.Text = CurrentDomain.BrandName
	End Sub
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">MANAGE RECURRING SERIES</span>
			<span class="PageSubHeading">- CHECKING REPLY</span><br />
			<custom:AuthAlert PropertyToCheck="AllowRecurringModify" runat="server" />
		</td>
	</tr>
	<tr>
		<td>
			<br />
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr>
				<td>
					After the recurring series management request is received and processed, <asp:Label ID="lblCompany1" runat="server" /> system returns the reply code.<br />
					This reply is for only the recurring management.<br /> 
					To recive notification when recurring transaction is processed use the following setting page <a style="font-size:90%;" href="Notifications.aspx">GLOBAL SETTINGS --> NOTIFICATIONS</a>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td class="SecHeading">Example to how and what is replied<br /><br /></td></tr>
	<tr>
		<td>
			<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
			<tr>
				<th width="110" valign="bottom">Field<br /></th>
				<th valign="bottom">Description<br /></th>
			</tr>
			<tr>
				<td>Reply<br /></td>
				<td>Two digits reply code (possible codes are listed below).<br /></td>
			</tr>
			<tr>
				<td>Description<br /></td>
				<td>The textual representation of the reply code (from the list below)<br /></td>
			</tr>
			<tr>
				<td>Details<br /></td>				
				<td>
					Additional information, that may be useful for debugging purposes.<br />
					Usually contains a list of fields with invalid values.<br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td style="padding-bottom:4px;">
			Reply in case of success:<br />
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					REPLY=00&amp;DESCRIPTION=SUCCESS
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td style="padding-bottom:4px;">
			Reply in case of failure:<br />
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" style="border:1px dashed #c0c0c0;" width="85%" cellspacing="0" cellpadding="5">
			<tr>
				<td style="background-color:#f7f7f7;font-family:Monospace;">
					REPLY=09&amp;DESCRIPTION=INVALID RECURRING STRING&amp;DETAILS=Recurring2 5N1A7.01
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Reply codes<br /><br /></td></tr>
	<tr>
		<td>
			<netpay:GlobalDataGroupValueTable GlobalDataGroup="RecurringModificationStatus" runat="server" />
		</td>
	</tr>
	</table>
	<br />
</asp:Content>