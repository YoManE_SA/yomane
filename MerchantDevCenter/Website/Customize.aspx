<%@ Page Language="VB" Inherits="Netpay.MerchantDevCenter.Code.MasteredPage"   Title="DEVELOPER CENTER - HOSTED PAYMENT PAGE" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">
	Sub LoadAligns(ddlAlign As DropDownList, Optional sSelected As String = Nothing)
		ddlAlign.Items.Clear()
		ddlAlign.Items.Add(New ListItem("< auto >", ""))
		ddlAlign.Items.Add("bottom")
		ddlAlign.Items.Add("middle")
		ddlAlign.Items.Add("top")
		If Not String.IsNullOrEmpty(sSelected) Then
			For Each li As ListItem In ddlAlign.Items
				If sSelected.Equals(li.Value, StringComparison.OrdinalIgnoreCase) Then li.Selected = True
			Next
		End If
	End Sub

	Sub LoadNumericList(ddlList As DropDownList, nMin As Integer, nMax As Integer, postfix As String, Optional sSelected As String = Nothing, Optional sEmptyText As String = Nothing)
		ddlList.Items.Clear()
		ddlList.Items.Add(New ListItem(IIf(String.IsNullOrEmpty(sEmptyText), String.Empty, "< " & sEmptyText & " >"), String.Empty))
		For i As Integer = nMin To nMax
			ddlList.Items.Add(New ListItem(i & postfix, i))
		Next
		If Not String.IsNullOrEmpty(sSelected) Then
			For Each li As ListItem In ddlList.Items
				If sSelected.Equals(li.Value, StringComparison.OrdinalIgnoreCase) Then li.Selected = True
			Next
		End If
    End Sub
    
    Protected Function mapMerchantFileName(skinid As Integer, itemName As String) As String
        Return Account.MapPublicPath(Netpay.Bll.Merchants.Hosted.Customization.GetImageFileName(skinid, itemName))
    End Function

    Sub LoadThumbnail(imgImage As Image)
        Dim sImageType As String = imgImage.ID.Replace("img", String.Empty)
        Dim nSkin As Integer = dbPages.TestVar(ddlSkin.SelectedValue, 1, 0, 0)
        Dim sPath As String = Account.MapPublicVirtualPath(Netpay.Bll.Merchants.Hosted.Customization.GetImageFileName(nSkin, sImageType & ".jpg"))
        If System.IO.File.Exists(mapMerchantFileName(nSkin, sImageType & ".jpg")) Then
            imgImage.ImageUrl = sPath
            imgImage.Visible = True
            pnlSkin.FindControl("btnDelete" & sImageType).Visible = True
            pnlSkin.FindControl("fu" & sImageType).Visible = False
        Else
            imgImage.Visible = False
            pnlSkin.FindControl("btnDelete" & sImageType).Visible = False
            pnlSkin.FindControl("fu" & sImageType).Visible = True
        End If
    End Sub
		
    Sub SaveImage(fuImage As FileUpload)
        If fuImage.HasFile Then
            Dim sImageType As String = fuImage.ID.Replace("fu", String.Empty)
            Dim nSkin As Integer = dbPages.TestVar(ddlSkin.SelectedValue, 1, 0, 0)
            Dim sPath As String = mapMerchantFileName(nSkin, sImageType & ".jpg")
            If Not System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(sPath)) Then System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(sPath))
            If System.IO.File.Exists(sPath) Then System.IO.File.Delete(sPath)
            fuImage.SaveAs(sPath)
        End If
    End Sub
		
    Sub DeleteImage(o As Object, e As EventArgs)
        Dim sImageType As String = CType(o, Button).ID.Replace("btnDelete", String.Empty)
        Dim nSkin As Integer = dbPages.TestVar(ddlSkin.SelectedValue, 1, 0, 0)
        Dim sPath As String = mapMerchantFileName(nSkin, sImageType & ".jpg")
        Try
            My.Computer.FileSystem.DeleteFile(sPath)
        Catch ex As Exception
        Finally
            LoadThumbnail(pnlSkin.FindControl("img" & sImageType))
        End Try
    End Sub
		
    Sub LoadSkins()
        ddlSkin.Items.Clear()
        btnDelete.Visible = False
        For Each v In Netpay.Bll.Merchants.Hosted.Customization.GetCustomizations(Merchant.ID).Split(",")
            btnDelete.Visible = True
            ddlSkin.Items.Add(v)
        Next
        pnlSkin.Visible = False
        lbAdd.Enabled = IIf(ddlSkin.Items.Count < 9, True, False)
    End Sub

    Sub DeleteSkin(o As Object, e As EventArgs)
        Dim nSkin As Integer = dbPages.TestVar(ddlSkin.SelectedValue, 1, 0, 0)
        Netpay.Bll.Merchants.Hosted.Customization.Load(nSkin).Delete()
        LoadSkins()
    End Sub

    Protected Sub LoadFontType(ddlFont As DropDownList, Optional sSelected As String = Nothing)
        ddlFont.Items.Clear()
        ddlFont.Items.Add("")
        ddlFont.Items.Add("Arial")
        ddlFont.Items.Add("Courier New")
        ddlFont.Items.Add("Monospace")
        ddlFont.Items.Add("Tahoma")
        ddlFont.Items.Add("Times New Roman")
        ddlFont.Items.Add("Verdana")
        If Not String.IsNullOrEmpty(sSelected) Then
            For Each li As ListItem In ddlFont.Items
                If sSelected.Equals(li.Value, StringComparison.OrdinalIgnoreCase) Then li.Selected = True
            Next
        End If
    End Sub

    Function ShowIfPositive(nInteger As Integer) As String
        Return IIf(nInteger > 0, nInteger, String.Empty)
    End Function

    Sub LoadSkin(Optional o As Object = Nothing, Optional e As EventArgs = Nothing)
        Dim nSkin As Integer = 0
        If Not Integer.TryParse(ddlSkin.SelectedValue, nSkin) Then Exit Sub
        Dim mcSkin = Netpay.Bll.Merchants.Hosted.Customization.Load(nSkin)
        ddlBackImageRepeat.SelectedValue = mcSkin.BackImageRepeat
        txtPageBackColor.Text = mcSkin.BackColor

        LoadAligns(ddlAlignMain, mcSkin.LogoMain.Align)
        txtHeightMain.Text = ShowIfPositive(mcSkin.LogoMain.Height)
        txtWidthMain.Text = ShowIfPositive(mcSkin.LogoMain.Width)

        LoadAligns(ddlAlignTopRight, mcSkin.LogoTopRight.Align)
        txtHeightTopRight.Text = ShowIfPositive(mcSkin.LogoTopRight.Height)
        txtWidthTopRight.Text = ShowIfPositive(mcSkin.LogoTopRight.Width)

        LoadAligns(ddlAlignTopLeft, mcSkin.LogoTopLeft.Align)
        txtHeightTopLeft.Text = ShowIfPositive(mcSkin.LogoTopLeft.Height)
        txtWidthTopLeft.Text = ShowIfPositive(mcSkin.LogoTopLeft.Width)

        LoadAligns(ddlAlignBottomRight, mcSkin.LogoBottomRight.Align)
        txtHeightBottomRight.Text = ShowIfPositive(mcSkin.LogoBottomRight.Height)
        txtWidthBottomRight.Text = ShowIfPositive(mcSkin.LogoBottomRight.Width)

        LoadAligns(ddlAlignBottomLeft, mcSkin.LogoBottomLeft.Align)
        txtHeightBottomLeft.Text = ShowIfPositive(mcSkin.LogoBottomLeft.Height)
        txtWidthBottomLeft.Text = ShowIfPositive(mcSkin.LogoBottomLeft.Width)

        txtBackColor.Text = mcSkin.Text.BackColor
        ddlFontFamily.Text = mcSkin.Text.FontFamily
        chkFontIsBold.Checked = mcSkin.Text.FontIsBold
        chkFontIsItalic.Checked = mcSkin.Text.FontIsItalic
        LoadNumericList(ddlFontSize, 10, 14, " px", mcSkin.Text.FontSize, "")
        LoadFontType(ddlFontFamily, mcSkin.Text.FontFamily)
        txtForeColor.Text = mcSkin.Text.ForeColor

        txtBackColorButton.Text = mcSkin.TextButton.BackColor
        ddlFontFamilyButton.Text = mcSkin.TextButton.FontFamily
        chkFontIsBoldButton.Checked = mcSkin.TextButton.FontIsBold
        chkFontIsItalicButton.Checked = mcSkin.TextButton.FontIsItalic
        LoadNumericList(ddlFontSizeButton, 10, 16, " px", mcSkin.TextButton.FontSize, "")
        LoadFontType(ddlFontFamilyButton, mcSkin.TextButton.FontFamily)
        txtForeColorButton.Text = mcSkin.TextButton.ForeColor

        txtBackColorField.Text = mcSkin.TextField.BackColor
        ddlFontFamilyField.Text = mcSkin.TextField.FontFamily
        chkFontIsBoldField.Checked = mcSkin.TextField.FontIsBold
        chkFontIsItalicField.Checked = mcSkin.TextField.FontIsItalic
        LoadNumericList(ddlFontSizeField, 10, 14, " px", mcSkin.TextField.FontSize, "")
        LoadFontType(ddlFontFamilyField, mcSkin.TextField.FontFamily)
        txtForeColorField.Text = mcSkin.TextField.ForeColor

        txtBackColorTitle.Text = mcSkin.TextTitle.BackColor
        ddlFontFamilyTitle.Text = mcSkin.TextTitle.FontFamily
        chkFontIsBoldTitle.Checked = mcSkin.TextTitle.FontIsBold
        chkFontIsItalicTitle.Checked = mcSkin.TextTitle.FontIsItalic
        LoadNumericList(ddlFontSizeTitle, 12, 16, " px", mcSkin.TextTitle.FontSize, "")
        LoadFontType(ddlFontFamilyTitle, mcSkin.TextTitle.FontFamily)
        txtForeColorTitle.Text = mcSkin.TextTitle.ForeColor

        txtBackColorFormTitle.Text = mcSkin.TextFormTitle.BackColor
        ddlFontFamilyFormTitle.Text = mcSkin.TextFormTitle.FontFamily
        chkFontIsBoldFormTitle.Checked = mcSkin.TextFormTitle.FontIsBold
        chkFontIsItalicFormTitle.Checked = mcSkin.TextFormTitle.FontIsItalic
        LoadNumericList(ddlFontSizeFormTitle, 12, 16, " px", mcSkin.TextFormTitle.FontSize, "")
        LoadFontType(ddlFontFamilyFormTitle, mcSkin.TextFormTitle.FontFamily)
        txtForeColorFormTitle.Text = mcSkin.TextFormTitle.ForeColor

        txtBackColorGroupTitle.Text = mcSkin.TextGroupTitle.BackColor
        ddlFontFamilyGroupTitle.Text = mcSkin.TextGroupTitle.FontFamily
        chkFontIsBoldGroupTitle.Checked = mcSkin.TextGroupTitle.FontIsBold
        chkFontIsItalicGroupTitle.Checked = mcSkin.TextGroupTitle.FontIsItalic
        LoadNumericList(ddlFontSizeGroupTitle, 12, 16, " px", mcSkin.TextGroupTitle.FontSize, "")
        LoadFontType(ddlFontFamilyGroupTitle, mcSkin.TextGroupTitle.FontFamily)
        txtForeColorGroupTitle.Text = mcSkin.TextGroupTitle.ForeColor

        LoadThumbnail(imgBackImage)
        LoadThumbnail(imgLogoMain)
        LoadThumbnail(imgLogoTopRight)
        LoadThumbnail(imgLogoTopLeft)
        LoadThumbnail(imgLogoBottomRight)
        LoadThumbnail(imgLogoBottomLeft)

        pnlSkin.Visible = True
    End Sub

    Sub SaveSkin(o As Object, e As EventArgs)
        SaveImage(fuBackImage)
        SaveImage(fuLogoMain)
        SaveImage(fuLogoTopRight)
        SaveImage(fuLogoTopLeft)
        SaveImage(fuLogoBottomRight)
        SaveImage(fuLogoBottomLeft)
        Dim mcSkin = Netpay.Bll.Merchants.Hosted.Customization.Load(ddlSkin.SelectedValue)
        If mcSkin Is Nothing Then mcSkin = New Netpay.Bll.Merchants.Hosted.Customization(Merchant.ID) With {.SkinID = ddlSkin.SelectedValue}
        mcSkin.BackImageRepeat = ddlBackImageRepeat.SelectedValue
        mcSkin.BackColor = txtPageBackColor.Text
        mcSkin.LogoMain.Align = ddlAlignMain.SelectedValue
        mcSkin.LogoMain.Height = dbPages.TestVar(txtHeightMain.Text, 1, 0, 0)
        mcSkin.LogoMain.Width = dbPages.TestVar(txtWidthMain.Text, 1, 0, 0)
        mcSkin.LogoTopRight.Align = ddlAlignTopRight.SelectedValue
        mcSkin.LogoTopRight.Height = dbPages.TestVar(txtHeightTopRight.Text, 1, 0, 0)
        mcSkin.LogoTopRight.Width = dbPages.TestVar(txtWidthTopRight.Text, 1, 0, 0)
        mcSkin.LogoTopLeft.Align = ddlAlignTopLeft.SelectedValue
        mcSkin.LogoTopLeft.Height = dbPages.TestVar(txtHeightTopLeft.Text, 1, 0, 0)
        mcSkin.LogoTopLeft.Width = dbPages.TestVar(txtWidthTopLeft.Text, 1, 0, 0)
        mcSkin.LogoBottomRight.Align = ddlAlignBottomRight.SelectedValue
        mcSkin.LogoBottomRight.Height = dbPages.TestVar(txtHeightBottomRight.Text, 1, 0, 0)
        mcSkin.LogoBottomRight.Width = dbPages.TestVar(txtWidthBottomRight.Text, 1, 0, 0)
        mcSkin.LogoBottomLeft.Align = ddlAlignBottomLeft.SelectedValue
        mcSkin.LogoBottomLeft.Height = dbPages.TestVar(txtHeightBottomLeft.Text, 1, 0, 0)
        mcSkin.LogoBottomLeft.Width = dbPages.TestVar(txtWidthBottomLeft.Text, 1, 0, 0)
        mcSkin.Text.BackColor = txtBackColor.Text
        mcSkin.Text.FontFamily = ddlFontFamily.Text
        mcSkin.Text.FontIsBold = chkFontIsBold.Checked
        mcSkin.Text.FontIsItalic = chkFontIsItalic.Checked
        mcSkin.Text.FontSize = ddlFontSize.SelectedValue
        mcSkin.Text.ForeColor = txtForeColor.Text
        mcSkin.TextButton.BackColor = txtBackColorButton.Text
        mcSkin.TextButton.FontFamily = ddlFontFamilyButton.Text
        mcSkin.TextButton.FontIsBold = chkFontIsBoldButton.Checked
        mcSkin.TextButton.FontIsItalic = chkFontIsItalicButton.Checked
        mcSkin.TextButton.FontSize = ddlFontSizeButton.SelectedValue
        mcSkin.TextButton.ForeColor = txtForeColorButton.Text
        mcSkin.TextField.BackColor = txtBackColorField.Text
        mcSkin.TextField.FontFamily = ddlFontFamilyField.Text
        mcSkin.TextField.FontIsBold = chkFontIsBoldField.Checked
        mcSkin.TextField.FontIsItalic = chkFontIsItalicField.Checked
        mcSkin.TextField.FontSize = ddlFontSizeField.SelectedValue
        mcSkin.TextField.ForeColor = txtForeColorField.Text
        mcSkin.TextTitle.BackColor = txtBackColorTitle.Text
        mcSkin.TextTitle.FontFamily = ddlFontFamilyTitle.Text
        mcSkin.TextTitle.FontIsBold = chkFontIsBoldTitle.Checked
        mcSkin.TextTitle.FontIsItalic = chkFontIsItalicTitle.Checked
        mcSkin.TextTitle.FontSize = ddlFontSizeTitle.SelectedValue
        mcSkin.TextTitle.ForeColor = txtForeColorTitle.Text
        mcSkin.TextFormTitle.BackColor = txtBackColorFormTitle.Text
        mcSkin.TextFormTitle.FontFamily = ddlFontFamilyFormTitle.Text
        mcSkin.TextFormTitle.FontIsBold = chkFontIsBoldFormTitle.Checked
        mcSkin.TextFormTitle.FontIsItalic = chkFontIsItalicFormTitle.Checked
        mcSkin.TextFormTitle.FontSize = ddlFontSizeFormTitle.SelectedValue
        mcSkin.TextFormTitle.ForeColor = txtForeColorFormTitle.Text
        mcSkin.TextGroupTitle.BackColor = txtBackColorGroupTitle.Text
        mcSkin.TextGroupTitle.FontFamily = ddlFontFamilyGroupTitle.Text
        mcSkin.TextGroupTitle.FontIsBold = chkFontIsBoldGroupTitle.Checked
        mcSkin.TextGroupTitle.FontIsItalic = chkFontIsItalicGroupTitle.Checked
        mcSkin.TextGroupTitle.FontSize = ddlFontSizeGroupTitle.SelectedValue
        mcSkin.TextGroupTitle.ForeColor = txtForeColorGroupTitle.Text
        mcSkin.Save()
        LoadSkin()
    End Sub

    Sub CancelSaveSkin(o As Object, e As EventArgs)
        pnlSkin.Visible = False
    End Sub

    Sub AddSkin(o As Object, e As EventArgs)
        Dim sSQL As String = "INSERT INTO tblMerchantCustomization (MerchantID, SkinID) SELECT " & Merchant.ID & ", IsNull(Max(SkinID), 0)+1 FROM tblMerchantCustomization WHERE MerchantID=" & Merchant.ID
        dbPages.ExecSql(sSQL)
        LoadSkins()
        ddlSkin.SelectedIndex = ddlSkin.Items.Count - 1
    End Sub
	
    Private Sub UploadLogo(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not IsPostBack Then
            LoadSkins()
        End If
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<script language="javascript" type="text/javascript">
		function IsDigitPressed()
		{
			return (event.keyCode >= "0".charCodeAt(0) && event.keyCode <= "9".charCodeAt(0));
		}
	</script>
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
		<tr>
			<td>
				<span class="PageHeading">CUSTOMIZATION</span><br />
			</td>
		</tr>
        <tr>
            <td>
                <div id="attention-version" >
                    <h3>Attention!</h3>
                    <p>
                In this page you can edit <strong>only</strong> the V1 - version of Payment Page, if you want to edit V2 - version of Payment Page you will need to go to <a href="https://merchants.netpay-intl.com/WebSite/Login.aspx">Merchant Control Panel</a> 
                        and then go to menu <strong>Settings >> Payment Page</strong>
                    </p>

                </div>

            </td>

        </tr>
		<tr>
			<td>
				<br />
				Select skin:
				<asp:DropDownList ID="ddlSkin" runat="server" />
				<asp:Button ID="btnEdit" UseSubmitBehavior="false" Text=" Edit " OnClick="LoadSkin" runat="server" />
				&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:LinkButton ID="lbAdd" Text="Add New Skin" OnClick="AddSkin" runat="server" />
			</td>
		</tr>
	</table>
	<asp:Panel ID="pnlSkin" Visible="false" runat="server">
		<table cellspacing="0" cellpadding="1" border="0" width="100%">
			<tr>
				<td>
					<br />
					<br />
				</td>
			</tr>
			<tr>
				<td class="SecHeading">
					Elements Style<br />
				</td>
			</tr>
			<tr>
				<td>
					<br />
				</td>
			</tr>
			<tr>
				<td>
					<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
						<tr>
							<th>
								Element
							</th>
							<th>
								Font Formating (type/size/color)
							</th>
							<th>
								Bg Color
							</th>
						</tr>
						<tr>
							<td>
								Text
							</td>
							<td>
								<asp:DropDownList ID="ddlFontFamily" CssClass="short" runat="server" />
								<asp:DropDownList ID="ddlFontSize" runat="server" />
								&nbsp;&nbsp;
								<netpay:ColorPicker ID="txtForeColor" CssClass="xxshort" runat="server" HideTextBox="false" />
								&nbsp;&nbsp;
								<asp:CheckBox ID="chkFontIsBold" CssClass="option" runat="server" />Bold
								<asp:CheckBox ID="chkFontIsItalic" CssClass="option" runat="server" />Italic
							</td>
							<td>
								<netpay:ColorPicker ID="txtBackColor" CssClass="xxshort" runat="server" HideTextBox="false" />
							</td>
						</tr>
						<tr>
							<td>
								Page Main Title
							</td>
							<td>
								<asp:DropDownList ID="ddlFontFamilyTitle" CssClass="short" runat="server" />
								<asp:DropDownList ID="ddlFontSizeTitle" runat="server" />
								&nbsp;&nbsp;
								<netpay:ColorPicker ID="txtForeColorTitle" CssClass="xxshort" runat="server" HideTextBox="false" />
								&nbsp;&nbsp;
								<asp:CheckBox ID="chkFontIsBoldTitle" CssClass="option" runat="server" />Bold
								<asp:CheckBox ID="chkFontIsItalicTitle" CssClass="option" runat="server" />Italic
							</td>
							<td>
								<netpay:ColorPicker ID="txtBackColorTitle" CssClass="xxshort" runat="server" HideTextBox="false" />
							</td>
						</tr>
						<tr>
							<td>
								Form Main Title
							</td>
							<td>
								<asp:DropDownList ID="ddlFontFamilyFormTitle" CssClass="short" runat="server" />
								<asp:DropDownList ID="ddlFontSizeFormTitle" runat="server" />
								&nbsp;&nbsp;
								<netpay:ColorPicker ID="txtForeColorFormTitle" CssClass="xxshort" runat="server" HideTextBox="false" />
								&nbsp;&nbsp;
								<asp:CheckBox ID="chkFontIsBoldFormTitle" CssClass="option" runat="server" />Bold
								<asp:CheckBox ID="chkFontIsItalicFormTitle" CssClass="option" runat="server" />Italic
							</td>
							<td>
								<netpay:ColorPicker ID="txtBackColorFormTitle" CssClass="xxshort" runat="server" HideTextBox="false" />
							</td>
						</tr>
						<tr>
							<td>
								Form Sub Titles
							</td>
							<td>
								<asp:DropDownList ID="ddlFontFamilyGroupTitle" CssClass="short" runat="server" />
								<asp:DropDownList ID="ddlFontSizeGroupTitle" runat="server" />
								&nbsp;&nbsp;
								<netpay:ColorPicker ID="txtForeColorGroupTitle" CssClass="xxshort" runat="server" HideTextBox="false" />
								&nbsp;&nbsp;
								<asp:CheckBox ID="chkFontIsBoldGroupTitle" CssClass="option" runat="server" />Bold
								<asp:CheckBox ID="chkFontIsItalicGroupTitle" CssClass="option" runat="server" HideTextBox="false" />Italic
							</td>
							<td>
								<netpay:ColorPicker ID="txtBackColorGroupTitle" CssClass="xxshort" runat="server" HideTextBox="false" />
							</td>
						</tr>
						<tr>
							<td>
								Input Fields
							</td>
							<td>
								<asp:DropDownList ID="ddlFontFamilyField" CssClass="short" runat="server" />
								<asp:DropDownList ID="ddlFontSizeField" runat="server" />
								&nbsp;&nbsp;
								<netpay:ColorPicker ID="txtForeColorField" CssClass="xxshort" runat="server" HideTextBox="false" />
								&nbsp;&nbsp;
								<asp:CheckBox ID="chkFontIsBoldField" CssClass="option" runat="server" />Bold
								<asp:CheckBox ID="chkFontIsItalicField" CssClass="option" runat="server" />Italic
							</td>
							<td>
								<netpay:ColorPicker ID="txtBackColorField" CssClass="xxshort" runat="server" HideTextBox="false" />
							</td>
						</tr>
						<tr>
							<td>
								Buttons
							</td>
							<td>
								<asp:DropDownList ID="ddlFontFamilyButton" CssClass="short" runat="server" />
								<asp:DropDownList ID="ddlFontSizeButton" runat="server" />
								&nbsp;&nbsp;
								<netpay:ColorPicker ID="txtForeColorButton" CssClass="xxshort" runat="server" HideTextBox="false" />
								&nbsp;&nbsp;
								<asp:CheckBox ID="chkFontIsBoldButton" CssClass="option" runat="server" />Bold
								<asp:CheckBox ID="chkFontIsItalicButton" CssClass="option" runat="server" />Italic
							</td>
							<td>
								<netpay:ColorPicker ID="txtBackColorButton" CssClass="xxshort" runat="server" HideTextBox="false" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<br />
					<br />
				</td>
			</tr>
			<tr>
				<td class="SecHeading">
					Page Background<br />
				</td>
			</tr>
			<tr>
				<td>
					<br />
				</td>
			</tr>
			<tr>
				<td>
					<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
						<tr>
							<th>
								Color
							</th>
							<th>
								Image File
							</th>
							<th>
								Repeat
							</th>
						</tr>
						<tr>
							<td>
								<netpay:ColorPicker ID="txtPageBackColor" CssClass="xxshort" runat="server" HideTextBox="false" />
							</td>
							<td>
								<asp:FileUpload ID="fuBackImage" runat="server" />
								<div>
									<asp:Image ID="imgBackImage" runat="server" onerror="this.style.display='none';this.parentNode.nextSibling.style.display='none';" Width="200" />
								</div>
								<asp:Button UseSubmitBehavior="false" ID="btnDeleteBackImage" Text="Delete" CssClass="short" OnClientClick="if (!confirm('Are you sure ?!')) return false;" OnClick="DeleteImage" runat="server" />
							</td>
							<td>
								<asp:DropDownList ID="ddlBackImageRepeat" CssClass="short" runat="server">
									<asp:ListItem Text="" Value="" />
									<asp:ListItem Text="no-repeat" Value="no-repeat" />
									<asp:ListItem Text="repeat" Value="repeat" />
									<asp:ListItem Text="repeat-x" Value="repeat-x" />
									<asp:ListItem Text="repeat-y" Value="repeat-y" />
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<br />
					<br />
				</td>
			</tr>
			<tr>
				<td class="SecHeading">
					Images<br />
				</td>
			</tr>
			<tr>
				<td>
					<br />
				</td>
			</tr>
			<tr>
				<td>
					<table class="FieldsTable" bordercolor="#e0e0e0" border="1" cellspacing="0" cellpadding="3" align="center" width="100%">
						<tr>
							<th>
								&nbsp;
							</th>
							<th>
								Main
							</th>
							<th>
								Top Right
							</th>
							<th>
								Top Left
							</th>
							<th>
								Bottom Right
							</th>
							<th>
								Bottom Left
							</th>
						</tr>
						<tr>
							<td>
								Image file
							</td>
							<td>
								<asp:FileUpload ID="fuLogoMain" CssClass="short" runat="server" />
								<div>
									<asp:Image ID="imgLogoMain" Width="90" runat="server" onerror="this.style.display='none';this.parentNode.nextSibling.style.display='none';" />
								</div>
								<asp:Button UseSubmitBehavior="false" ID="btnDeleteLogoMain" Text="Delete" CssClass="xshort" OnClick="DeleteImage" OnClientClick="if (!confirm('Are you sure ?!')) return false;" runat="server" />
							</td>
							<td>
								<asp:FileUpload ID="fuLogoTopRight" CssClass="short" runat="server" />
								<div>
									<asp:Image ID="imgLogoTopRight" Width="90" runat="server" onerror="this.style.display='none';this.parentNode.nextSibling.style.display='none';" />
								</div>
								<asp:Button UseSubmitBehavior="false" ID="btnDeleteLogoTopRight" Text="Delete" CssClass="xshort" OnClick="DeleteImage" OnClientClick="if (!confirm('Are you sure ?!')) return false;" runat="server" />
							</td>
							<td>
								<asp:FileUpload ID="fuLogoTopLeft" CssClass="short" runat="server" />
								<div>
									<asp:Image ID="imgLogoTopLeft" Width="90" runat="server" onerror="this.style.display='none';this.parentNode.nextSibling.style.display='none';" />
								</div>
								<asp:Button UseSubmitBehavior="false" ID="btnDeleteLogoTopLeft" Text="Delete" CssClass="xshort" OnClick="DeleteImage" OnClientClick="if (!confirm('Are you sure ?!')) return false;" runat="server" />
							</td>
							<td>
								<asp:FileUpload ID="fuLogoBottomRight" CssClass="short" runat="server" />
								<div>
									<asp:Image ID="imgLogoBottomRight" Width="90" runat="server" onerror="this.style.display='none';this.parentNode.nextSibling.style.display='none';" />
								</div>
								<asp:Button UseSubmitBehavior="false" ID="btnDeleteLogoBottomRight" Text="Delete" CssClass="xshort" OnClick="DeleteImage" OnClientClick="if (!confirm('Are you sure ?!')) return false;" runat="server" />
							</td>
							<td>
								<asp:FileUpload ID="fuLogoBottomLeft" CssClass="short" runat="server" />
								<div>
									<asp:Image ID="imgLogoBottomLeft" Width="90" runat="server" onerror="this.style.display='none';this.parentNode.nextSibling.style.display='none';" />
								</div>
								<asp:Button UseSubmitBehavior="false" ID="btnDeleteLogoBottomLeft" Text="Delete" CssClass="xshort" OnClick="DeleteImage" OnClientClick="if (!confirm('Are you sure ?!')) return false;" runat="server" />
							</td>
						</tr>
						<tr>
							<td>
								Width
							</td>
							<td>
								<asp:TextBox ID="txtWidthMain" CssClass="xxshort" onkeypress="return IsDigitPressed()" MaxLength="3" runat="server" />
								px
							</td>
							<td>
								<asp:TextBox ID="txtWidthTopRight" CssClass="xxshort" onkeypress="return IsDigitPressed()" MaxLength="3" runat="server" />
								px
							</td>
							<td>
								<asp:TextBox ID="txtWidthTopLeft" CssClass="xxshort" onkeypress="return IsDigitPressed()" MaxLength="3" runat="server" />
								px
							</td>
							<td>
								<asp:TextBox ID="txtWidthBottomRight" CssClass="xxshort" onkeypress="return IsDigitPressed()" MaxLength="3" runat="server" />
								px
							</td>
							<td>
								<asp:TextBox ID="txtWidthBottomLeft" CssClass="xxshort" onkeypress="return IsDigitPressed()" MaxLength="3" runat="server" />
								px
							</td>
						</tr>
						<tr>
							<td>
								Height
							</td>
							<td>
								<asp:TextBox ID="txtHeightMain" CssClass="xxshort" onkeypress="return IsDigitPressed()" MaxLength="3" runat="server" />
								px
							</td>
							<td>
								<asp:TextBox ID="txtHeightTopRight" CssClass="xxshort" onkeypress="return IsDigitPressed()" MaxLength="3" runat="server" />
								px
							</td>
							<td>
								<asp:TextBox ID="txtHeightTopLeft" CssClass="xxshort" onkeypress="return IsDigitPressed()" MaxLength="3" runat="server" />
								px
							</td>
							<td>
								<asp:TextBox ID="txtHeightBottomRight" CssClass="xxshort" onkeypress="return IsDigitPressed()" MaxLength="3" runat="server" />
								px
							</td>
							<td>
								<asp:TextBox ID="txtHeightBottomLeft" CssClass="xxshort" onkeypress="return IsDigitPressed()" MaxLength="3" runat="server" />
								px
							</td>
						</tr>
						<tr>
							<td>
								Align
							</td>
							<td>
								<asp:DropDownList ID="ddlAlignMain" CssClass="xshort" runat="server" />
							</td>
							<td>
								<asp:DropDownList ID="ddlAlignTopRight" CssClass="xshort" runat="server" />
							</td>
							<td>
								<asp:DropDownList ID="ddlAlignTopLeft" CssClass="xshort" runat="server" />
							</td>
							<td>
								<asp:DropDownList ID="ddlAlignBottomRight" CssClass="xshort" runat="server" />
							</td>
							<td>
								<asp:DropDownList ID="ddlAlignBottomLeft" CssClass="xshort" runat="server" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<br />
					<asp:Button runat="server" UseSubmitBehavior="false" ID="btnSave" Text="Save" CssClass="submit" OnClick="SaveSkin" />
					<asp:Button runat="server" UseSubmitBehavior="false" ID="btnCancel" Text="Cancel" CssClass="submit" OnClick="CancelSaveSkin" OnClientClick="if (!confirm('Are you sure ?!')) return false;" />
					&nbsp;&nbsp;
					<asp:Button runat="server" ID="btnDelete" UseSubmitBehavior="false" Text="Delete" OnClick="DeleteSkin" OnClientClick="if (!confirm('Are you sure ?!')) return false;" ForeColor="Maroon" />
				</td>
			</tr>
		</table>
	</asp:Panel>
	<br />
</asp:Content>
