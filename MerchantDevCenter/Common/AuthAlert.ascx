﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AuthAlert.ascx.vb" Inherits="Netpay.MerchantDevCenter.AuthAlert" %>
<asp:Label ID="lblAlert" runat="server">
    <table width="100%">
      <tr>
	    <td width="20"><img src="/NPCommon/images/alert_small.png" align="middle" style="vertical-align:middle;" alt="Authorization" /></td>
	    <td valign="middle"><asp:Literal ID="litWarning" runat="server" /></td>
      </tr>
    </table>
</asp:Label>