﻿Public Class AuthAlert
    Inherits System.Web.UI.UserControl

    Public MessageText As String
    Private sCssClass As String = "AlertMsg"
    Public Property CssClass() As String
        Get
            Return sCssClass
        End Get
        Set(ByVal value As String)
            sCssClass = value
        End Set
    End Property

    Private nPropertyToCheck As AuthorizationProperty = AuthorizationProperty.None
    Public Property PropertyToCheck() As AuthorizationProperty
        Get
            Return nPropertyToCheck
        End Get
        Set(ByVal value As AuthorizationProperty)
            nPropertyToCheck = value
        End Set
    End Property

    Public Sub formatFeatureMessage(featureName As String)
        MessageText = featureName & " feature is not enabled in your account." & _
            " In order to enable it, contact " & WebUtils.CurrentDomain.BrandName & " customer service."
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not WebUtils.IsLoggedin Then
            lblAlert.Text = ""
            lblAlert.Visible = False
            Exit Sub
        End If
        lblAlert.CssClass = CssClass
        If String.IsNullOrEmpty(MessageText) Then
            MessageText = "This feature is not enabled in your account." & _
                " In order to enable it, contact " & WebUtils.CurrentDomain.BrandName & " customer service."
        End If
        If PropertyToCheck = AuthorizationProperty.None Or CType(Page, NetpayPage).Authorization.IsAllowed(PropertyToCheck) Then
            litWarning.Text = String.Empty
        Else
            litWarning.Text = MessageText
        End If
        lblAlert.Visible = (litWarning.Text <> String.Empty)
    End Sub
End Class