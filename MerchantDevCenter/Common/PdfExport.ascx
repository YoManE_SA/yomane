<%@ Control Language="VB" ClassName="PdfExport" %>

<script runat="server">
	Sub Page_Init()
		If Request("ToPDF") = "1" Then
			lblPrintDateTime.Visible = True
			lblPrintDateTime.Text = Date.Now
		End If
	End Sub
	
	Sub Page_Load()
		Dim sURL As String = Request.Url.PathAndQuery
		sURL &= IIf(sURL.Contains("?"), "&", "?") & "ToPDF=1"
		hlPdfImg.NavigateUrl = sURL
		hlPdfText.NavigateUrl = sURL
		lblPDF.Visible = True
	End Sub
</script>

<style type="text/css">
	a.PdfExport{font-size:8px;text-decoration:none;line-height:8px;}
</style>
	
<span style="float:right;">
	<asp:Label ID="lblPDF" Visible="false" runat="server">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<asp:HyperLink ID="hlPdfImg" Target="_blank" runat="server"><img src="/NPCommon/images/save_as_pdf.gif" style="border:0;" title="Save as PDF" alt="Save as PDF" /></asp:HyperLink>
				</td>
				<td>
					<asp:HyperLink ID="hlPdfText" Target="_blank" CssClass="PdfExport" ToolTip="Save as PDF" runat="server">EXPORT<br />TO PDF</asp:HyperLink>
				</td>
			</tr>
		</table>
	</asp:Label>
	<asp:Label ID="lblPrintDateTime" Visible="false" runat="server" />
</span>