how to create key files on dev machine

use exported keys
1. copy key files in C:\Netpay\Documentation\NetPayCrypt\exportedKeys to C:\bin
2. run NetPayCryptDPAPI to create key files with DPA ext.
3. delete exported key files on C:\bin
4. merge NetpayCryptRegistryKey.reg

export from other computer
1. run NetPayKeyTrans on source machine to create key files at C:\bin without DPA ext.
2. move created key files to target machine C:\bin
3. on target machine run NetPayCryptDPAPI to create key files with DPA ext.
4. delete exported files
4. merge NetpayCryptRegistryKey.reg



