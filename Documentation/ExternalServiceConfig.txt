<?xml version="1.0"?>
<configuration>
	<system.web>
		<compilation debug="false"/>
		<customErrors mode="On"></customErrors>
	</system.web>
	<system.serviceModel>
		<bindings>
			<basicHttpBinding>
				<binding name="soapBinding" >
					<security mode="None"></security>
				</binding>
			</basicHttpBinding>
			<webHttpBinding>
				<binding name="jsonWebBinding">
					<security mode="None"></security>
				</binding>
				<binding name="poxWebBinding">
					<security mode="None"></security>
				</binding>
			</webHttpBinding>
		</bindings>
		<behaviors>
			<serviceBehaviors>
				<behavior>
					<serviceMetadata httpGetEnabled="True" />
					<serviceDebug includeExceptionDetailInFaults="True" />
				</behavior>
			</serviceBehaviors>
			<endpointBehaviors>
				<behavior name="poxBehavior">
					<webHttp/>
				</behavior>
				<behavior name="jsonBehavior">
					<enableWebScript/>
				</behavior>
			</endpointBehaviors>
		</behaviors>
		<services>
			<service name="Netpay.MainService.Merchants">
				<endpoint address="soap" binding="basicHttpBinding" contract="Netpay.MainService.IMerchants" />
				<!--
				<endpoint address="json" behaviorConfiguration="jsonBehavior" binding="webHttpBinding" bindingConfiguration="jsonWebBinding" contract="Netpay.MainService.IMerchantsService" />
				<endpoint address="pox" behaviorConfiguration="poxBehavior" binding="webHttpBinding" bindingConfiguration="poxWebBinding" contract="Netpay.MainService.IMerchantsService" />
				-->
				<endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
				<host>
					<baseAddresses>
						<add baseAddress="http://localhost:8585/Services/Merchants/" />
					</baseAddresses>
				</host>
			</service>

			<service name="Netpay.MainService.Risk">
				<endpoint address="soap" binding="basicHttpBinding" contract="Netpay.MainService.IRisk" />
				<endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
				<host>
					<baseAddresses>
						<add baseAddress="http://localhost:8585/Services/Risk/" />
					</baseAddresses>
				</host>
			</service>
		</services>
	</system.serviceModel>
	<startup>
		<supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.0"/>
	</startup>
</configuration>











<?xml version="1.0"?>
<configuration>
	<system.web>
		<compilation debug="false"/>
		<customErrors mode="On"></customErrors>
	</system.web>
	<system.serviceModel>
		<bindings>
			<basicHttpBinding>
				<binding name="soapBinding" >
					<security mode="None"></security>
				</binding>
			</basicHttpBinding>
		</bindings>
		<behaviors>
			<serviceBehaviors>
				<behavior name="merchantsBehaviour">
					<serviceMetadata httpGetEnabled="True" httpGetUrl="http://localhost:8585/Services/Merchants/" />
					<serviceDebug includeExceptionDetailInFaults="True" />
				</behavior>
				<behavior name="riskBehaviour">
					<serviceMetadata httpGetEnabled="True" httpGetUrl="http://localhost:8585/Services/Risk/" />
					<serviceDebug includeExceptionDetailInFaults="True" />
				</behavior>
			</serviceBehaviors>
		</behaviors>
		<services>
			<service name="Netpay.MainService.Merchants" behaviorConfiguration="merchantsBehaviour">
				<endpoint address="http://localhost:8585/Services/Merchants/" binding="basicHttpBinding" contract="Netpay.MainService.IMerchants" />
				<endpoint address="http://localhost:8585/Services/Merchants/mex" binding="mexHttpBinding" contract="IMetadataExchange" />
			</service>
			<service name="Netpay.MainService.Risk" behaviorConfiguration="riskBehaviour">
				<endpoint address="http://localhost:8585/Services/Risk/" binding="basicHttpBinding" contract="Netpay.MainService.IRisk" />
				<endpoint address="http://localhost:8585/Services/Risk/mex" binding="mexHttpBinding" contract="IMetadataExchange" />
			</service>
		</services>
	</system.serviceModel>
	<startup>
		<supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.0"/>
	</startup>
</configuration>

