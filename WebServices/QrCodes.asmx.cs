﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Gma.QrCodeNet.Encoding.Windows.Render;
using Gma.QrCodeNet.Encoding;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Netpay.Infrastructure;

namespace Netpay.WebServices
{
    /// <summary>
    /// Summary description for QrCodes
    /// </summary>
    [WebService(Namespace = "WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class QrCodes : System.Web.Services.WebService
    {
        public QrCodes()
        {
            Code.Utils.RegisterEmailTemplates();
        }

        public enum TargetType
        {
            HostedPaymentPageV2,
            PublicPaymentPageV2,
            ShopProductPage
        }
        [WebMethod]
        public byte[] GenerateCode(TargetType target, string urlParams)
        {
            if (urlParams.StartsWith("?")) urlParams = urlParams.Substring(1);
            switch (target)
            {
                case TargetType.HostedPaymentPageV2:
                    urlParams = string.Format("{0}Hosted/?{1}", Web.WebUtils.CurrentDomain.ProcessV2Url, urlParams);
                    break;
                case TargetType.PublicPaymentPageV2:
                    string output = Web.WebUtils.CurrentDomain.ShopUrl.Replace("{0}", urlParams.GetUrlValue("merchantID"));
                    if (!string.IsNullOrEmpty(urlParams.GetUrlValue("Item"))) output += "Item=" + urlParams.GetUrlValue("Item");
                    urlParams = output;
                    break;
                case TargetType.ShopProductPage:
                    //http://localhost:83/shops/{0}/
                    //Web.WebUtils.CurrentDomain.ShopUrl + 
                    string prodUrl = string.Format(Web.WebUtils.CurrentDomain.ShopUrl, urlParams.GetUrlValue("merchantNumber"));
                    prodUrl += urlParams.GetUrlValue("shopId") + "/product/" + urlParams.GetUrlValue("productId");
                    urlParams = prodUrl;
                    break;
                default:
                    throw new ApplicationException("parameter target contains an unsupported value");
            }
            QrCode qrCode;
            QrEncoder encoder = new QrEncoder(ErrorCorrectionLevel.M);
            //encoder.TryEncode(url, out qrCode);
            qrCode = encoder.Encode(urlParams);

			using(Brush foreColor = new SolidBrush(Color.FromArgb(0xff, 0xd6, 0x1a, 0x21))){
				GraphicsRenderer gRenderer = new GraphicsRenderer(new FixedModuleSize(2, QuietZoneModules.Two), foreColor, Brushes.White);
				using(MemoryStream ms = new MemoryStream()){
					gRenderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, ms);
					return ms.ToArray();
				}
			}
        }

        [WebMethod]
        public void RenderGenerateCode(TargetType target, string urlParams)
        {
            HttpContext.Current.Response.ContentType = "image/png";
            HttpContext.Current.Response.BinaryWrite(GenerateCode(target, urlParams));
        }

        [WebMethod]
        public string RenderToBase64(TargetType target, string urlParams)
        {
            return System.Convert.ToBase64String(GenerateCode(target, urlParams), Base64FormattingOptions.None);
        }
    }
}
