﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Bll.Merchants;

namespace Netpay.WebServices
{
    /// <summary>
    /// Summary description for MerchantRegistration
    /// </summary>
    [WebService(Namespace = "WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class MerchantRegistration : System.Web.Services.WebService, System.Web.SessionState.IRequiresSessionState
    {
        public MerchantRegistration()
        {
            Code.Utils.RegisterEmailTemplates();
        }
        #region Service Classes

        public class RegistrationData
        {
            public string DbaName { get; set; }
            public string LegalBusinessName { get; set; }
            public string LegalBusinessNumber { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zipcode { get; set; }
            public DateTime? OwnerDob { get; set; }
            public string OwnerSsn { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Url { get; set; }
            public string PhisicalAddress { get; set; }
            public string PhisicalCity { get; set; }
            public string PhisicalState { get; set; }
            public string PhisicalZip { get; set; }
            public string StateOfIncorporation { get; set; }
            public int? TypeOfBusiness { get; set; }
            public DateTime? BusinessStartDate { get; set; }
            public int? Industry { get; set; }
            public string BusinessDescription { get; set; }
            public decimal? AnticipatedMonthlyVolume { get; set; }
            public decimal? AnticipatedAverageTransactionAmount { get; set; }
            public decimal? AnticipatedLargestTransactionAmount { get; set; }
            public byte[] CanceledCheckImage { get; set; }
            public string BankAccountNumber { get; set; }
            public string BankRoutingNumber { get; set; }
            public int PercentDelivery0to7 { get; set; }
            public int PercentDelivery8to14 { get; set; }
            public int PercentDelivery15to30 { get; set; }
            public int PercentDeliveryOver30 { get; set; }
        }

        #endregion
     
        [WebMethod]
        public void Register(RegistrationData RegistrationData)
        { 
            Netpay.Bll.Merchants.Registration registration = new Netpay.Bll.Merchants.Registration();
            registration.DbaName = RegistrationData.DbaName;
            registration.LegalBusinessName = RegistrationData.LegalBusinessName;
            registration.FirstName = RegistrationData.FirstName;
            registration.LastName = RegistrationData.LastName;
            registration.Email = RegistrationData.Email;
            registration.Address = RegistrationData.Address;
            registration.City = RegistrationData.City;
            registration.State = RegistrationData.State;
            registration.Zipcode = RegistrationData.Zipcode;
            registration.OwnerDob = RegistrationData.OwnerDob;
            registration.OwnerSsn = RegistrationData.OwnerSsn;
            registration.Phone = RegistrationData.Phone;
            registration.Fax = RegistrationData.Fax;
            registration.Url = RegistrationData.Url;
            registration.PhisicalAddress = RegistrationData.PhisicalAddress;
            registration.PhisicalCity = RegistrationData.PhisicalCity;
            registration.PhisicalState = RegistrationData.PhisicalState;
            registration.PhisicalZip = RegistrationData.PhisicalZip;
            registration.StateOfIncorporation = RegistrationData.StateOfIncorporation;
            registration.TypeOfBusiness = RegistrationData.TypeOfBusiness;
            registration.BusinessStartDate = RegistrationData.BusinessStartDate;
            registration.Industry = RegistrationData.Industry;
            registration.BusinessDescription = RegistrationData.BusinessDescription;
            registration.AnticipatedMonthlyVolume = RegistrationData.AnticipatedMonthlyVolume;
            registration.AnticipatedAverageTransactionAmount = RegistrationData.AnticipatedAverageTransactionAmount;
            registration.AnticipatedLargestTransactionAmount = RegistrationData.AnticipatedLargestTransactionAmount;
            if (RegistrationData.CanceledCheckImage != null)
            {
                registration.CanceledCheckImageContent = System.Convert.ToBase64String(RegistrationData.CanceledCheckImage);
                registration.CanceledCheckImageFileName = "Register.png";
                registration.CanceledCheckImageMimeType = "image/png";
            }
            
            registration.BankAccountNumber = RegistrationData.BankAccountNumber;
            registration.BankRoutingNumber = RegistrationData.BankRoutingNumber;
            registration.PercentDelivery0to7 = RegistrationData.PercentDelivery0to7;
            registration.PercentDelivery8to14 = RegistrationData.PercentDelivery8to14;
            registration.PercentDelivery15to30 = RegistrationData.PercentDelivery15to30;
            registration.PercentDeliveryOver30 = RegistrationData.PercentDeliveryOver30;

            registration.Save();

            registration.SendRegistrationEmail();
        }
    }
}
