﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.IO;

namespace Netpay.WebServices
{
    public class Global : System.Web.HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            Web.ContextModule.AppName = "WebServices";
        }

        //void Application_End(object sender, EventArgs e) { }
        //void Session_Start(object sender, EventArgs e) { }
        //void Session_End(object sender, EventArgs e) { }
        void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Netpay.Infrastructure.Logger.Log(Netpay.Infrastructure.LogTag.WebService, ex);
        }

        //solution for angular OPTIONS request
        void Application_BeginRequest(object sender, EventArgs e)
        {
            var application = sender as HttpApplication;
            var currentContext = application.Context;

            if (application != null && currentContext != null)
            {
                if (currentContext.Request.CurrentExecutionFilePathExtension == ".svc" ||
                    currentContext.Request.CurrentExecutionFilePathExtension == ".asmx" ||
                    currentContext.Request.CurrentExecutionFilePathExtension == ".aspx")
                {
                    // log the request
                    if (!currentContext.Request.ToRaw().Contains("SOAPAction: \"WebServices/Log\""))
                        Infrastructure.Logger.Log(severity: Infrastructure.LogSeverity.Info,
                            tag: Infrastructure.LogTag.WebServiceLog,
                            message: string.Format("http Request: method:{0}, url:{1}", currentContext.Request.HttpMethod, currentContext.Request.RawUrl),
                            longMessage: currentContext.Request.ToRaw());

                    // Add a response filter that can be accessed from EndRequest event, 
                    // Used for logging
                    HttpResponse response = currentContext.Response;
                    OutputFilterStream filter = new OutputFilterStream(response.Filter);
                    currentContext.Response.Filter = filter;
                    currentContext.Items.Add("OutputFilter", filter);
                }

                currentContext.Response.Headers.Remove("Server");
            }

            if (Request.HttpMethod == "OPTIONS") Response.End();
        }



        void Application_EndRequest(object sender, EventArgs e)
        {
            // log the response value
            HttpContext currentContext = ((HttpApplication)sender).Context;

            if (currentContext != null &&
                (currentContext.Request.CurrentExecutionFilePathExtension == ".svc"
                || currentContext.Request.CurrentExecutionFilePathExtension == ".asmx"
                 || currentContext.Request.CurrentExecutionFilePathExtension == ".aspx"))
            {
                OutputFilterStream filter = currentContext.Items["OutputFilter"] as OutputFilterStream;
                if (filter != null)
                {
                    string longMessage = filter.ReadStream();
                    Infrastructure.Logger.Log(severity: Infrastructure.LogSeverity.Info,
                        tag: Infrastructure.LogTag.WebServiceLog,
                        message: string.Format("Http Response: status:{0}, url:{1}", currentContext.Response.Status, Context.Request.RawUrl),
                        longMessage: string.IsNullOrEmpty(longMessage) ? null : longMessage);
                }
            }
        }
    }
}

