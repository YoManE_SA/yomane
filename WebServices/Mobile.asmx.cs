﻿using System;
using System.Web;
using System.Web.Services;
using Netpay.WebServices.Code;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Web;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Netpay.Bll;

namespace Netpay.WebServices.Code.Mobile
{
    /// <summary>
    /// Mobile application services
    /// </summary>
    [WebService(Namespace = "WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class Mobile : System.Web.Services.WebService
    {
        public Mobile()
        {
            Code.Utils.RegisterEmailTemplates();
        }
        #region Service Classes

        public class MobileSettings
		{
			public string MerchantNumber { set; get; }
			public bool IsMobileAppEnabled { set; get; }
			public bool IsEmailRequired { set; get; }
			public bool IsPersonalNumberRequired { set; get; }
			public bool IsCardNotPresent { set; get; }
			public bool IsInstallments { set; get; }
			public bool IsRefund { set; get; }
			public bool IsAuthorization { set; get; }
			public bool IsFullNameRequired { set; get; }
			public bool IsPhoneRequired { set; get; }
			public bool IsOwnerSignRequired { set; get; }
			public bool IsAllowTaxRateChange { set; get; }
			public string MerchantName { set; get; }
			public string MerchantAddress { set; get; }
			public decimal? ValueAddedTax { get; set; }
			public string SupportedCurrencies { get; set; }

			public MobileSettings() { }
			public MobileSettings(Netpay.Bll.Merchants.Merchant merchant, Netpay.Bll.Merchants.Mobile.MobileAppSettings dataObj)
			{
				MerchantNumber = merchant.Number;
				IsAuthorization = dataObj.IsAuthorization;
				IsCardNotPresent = dataObj.IsCardNotPresent;
				IsEmailRequired = dataObj.IsEmailRequired;
				IsMobileAppEnabled = dataObj.IsMobileAppEnabled;
				IsPersonalNumberRequired = dataObj.IsPersonalNumberRequired;
				IsRefund = dataObj.IsRefund;
				IsInstallments = dataObj.IsInstallments;
				MerchantAddress = merchant.BusinessAddress.City + ", " + merchant.BusinessAddress.AddressLine1 + ", " + merchant.BusinessAddress.CountryISOCode;
				MerchantName = merchant.ContactFirstName + " " + merchant.ContactLastName;
				IsFullNameRequired = dataObj.IsFullNameRequired;
				IsPhoneRequired = dataObj.IsPhoneRequired;
				IsOwnerSignRequired = dataObj.IsOwnerSignRequired;
				IsAllowTaxRateChange = dataObj.IsAllowTaxRateChange;
				ValueAddedTax = dataObj.ValueAddedTax;
				SupportedCurrencies = string.Join(",", Netpay.Bll.Merchants.Merchant.GetSupportedCurrencies(merchant.ID).Select(vo => vo.IsoCode));
			}
		}

		public class ProductDetails
		{
			public ProductDetails() { }
			public ProductDetails(Bll.Shop.Products.Product ppo)
			{
				ID = ppo.ID;
				var text = ppo.GetTextForLanguage(null);
				if(text != null){
					Name = text.Name;
					Description = text.Description;
				}
				Price = ppo.Price;
			}

			public int ID { set; get; }
			public string Name { set; get; }
			public string Description { set; get; }
			public decimal Price { set; get; }
		}
		
		public class GetSettingsResult
        {
			public MobileSettings Settings { set; get; }
            public string Signature { set; get; }
        }

        public class StringResult
        {
            public string Value { set; get; }
            public string Signature { set; get; }
        }

        public class RegistrationResult 
        {
            public bool IsSuccessful { set; get; }
            public string Reason { set; get; }
            public string Signature { set; get; }
        }

        public class BoolResult
        {
            public bool Value { set; get; }
            public string Signature { set; get; }
        }

		public class GenericResult
		{
			public object Data { set; get; }
			public string Signature { set; get; }
		}

        public class ProcessResult
        {
            public bool IsSuccess { get; set; }
            public string TransactionNumber { get; set; }
            public string ReplyCode { set; get; }
            public string ReplyText { get; set; }
            public string Signature { get; set; }
        }

        public class MobileLoginResult
        {
            public bool IsLoginSuccessful { get; set; }
            public bool IsDeviceActivated { set; get; }
            public bool IsDeviceRegistered { set; get; }
			public bool IsDeviceBlocked { set; get; }
			public bool VersionUpdateRequired { get; set; }
            public string MerchantNumber { set; get; }
            public string LoginMessage { set; get; }
            public DateTime LastLogin { set; get; }
            public string CredentialsToken { set; get; }
        }

		public class TransactionInfo
		{
			internal TransactionInfo()
			{
			}

			internal TransactionInfo(Bll.Transactions.Transaction vo)
			{
				ID = vo.ID;
				Amount = vo.Amount;
				ApprovalCode = vo.ApprovalNumber;
				Card = vo.PaymentMethodDisplay;
				InsertDate = vo.InsertDate;
				Currency = vo.CurrencyIsoCode;
				Installments = vo.Installments;
				Comment = vo.Comment;
				PaymentDetails = vo.PayForText;
				var payerInfo = vo.PayerData;
				if (payerInfo != null){
					CardholderName = payerInfo.FullName;
					PersonalNumber = payerInfo.PersonalNumber;
					Email = payerInfo.EmailAddress;
					Phone = payerInfo.PhoneNumber;
				}
				IsRefunded = Bll.Transactions.RefundRequest.GetRefundAbility(vo) != TransactionRefundAbility.FullOnly;
				IsManual = true;
			}

			public int ID { get; set; }
			public decimal Amount { get; set; }
			public string Card { get; set; }
			public DateTime InsertDate { get; set; }
			public string Currency { get; set; }
			public int Installments { get; set; }
			public string Comment { get; set; }
			public string PaymentDetails { get; set; }
			public string Email { get; set; }
			public string Phone { get; set; }
			public bool IsRefunded { get; set; }
			public bool IsManual { get; set; }
			public string CardholderName { get; set; }
			public string ApprovalCode { get; set; }
			public string PersonalNumber { get; set; }
		}
        #endregion

        private static List<string> GetObjectValues(object obj)
        {
            if (obj == null)
                return null;

            List<string> values = new List<string>();
            Type type = obj.GetType();
            if (type.IsArray)
            {
                foreach (var value in ((System.Array)obj))
                {
                    List<string> objValues = GetObjectValues(value);
                    if (objValues != null)
                        values.AddRange(objValues);
                }
            }
            else if (type.IsClass && (type != typeof(string)))
            {
                System.Reflection.PropertyInfo[] fields = obj.GetType().GetProperties();
                foreach (var field in fields)
                {
                    object value = field.GetValue(obj, null);
                    List<string> objValues = GetObjectValues(value);
                    if (objValues != null)
                        values.AddRange(objValues);
                }
            }
            else if (type == typeof(bool))
				values.Add(obj.ToString().ToLower());
			else if (type == typeof(DateTime))
				values.Add(((DateTime)obj).ToString("dd/MM/yyyy HH:mm:ss"));
            else
                values.Add(obj.ToString());

            return values;
        }

		private bool SetContext(string credentialsToken)
		{
			Guid creds = Guid.Parse(credentialsToken);
			Infrastructure.ObjectContext.Current.CredentialsToken = creds;
			return Infrastructure.ObjectContext.Current.IsUserOfType(UserRole.Merchant, null, PermissionValue.Execute, false);
		}

        private static string CalcSignature(object obj, bool withActivationCode = true)
        {

            var activationCode = string.Empty;
            if(withActivationCode)
            {

                activationCode = Bll.Accounts.MobileDevice.Current != null && !string.IsNullOrEmpty(Bll.Accounts.MobileDevice.Current.PassCode) ? Bll.Accounts.MobileDevice.Current.PassCode   : string.Empty;


            }
            List<string> values = GetObjectValues(obj);
            //string valuesStr = values.OrderBy(s => s).Aggregate((current, next) => current + next);
            string valuesStr = values.Aggregate((current, next) => current + next);

			if (activationCode != null) valuesStr += activationCode;

            return valuesStr.ToSha256();
        }

        private static bool ValidateSignature(string signature, params string[] values)
        {
			StringBuilder strValues = new StringBuilder();
            foreach (string s in values)
                strValues.Append(s);
            var device = Bll.Accounts.MobileDevice.Current;
			strValues.Append(device.PassCode);
            String createdSignature = strValues.ToString().ToSha256();
            if (createdSignature != signature)
            {
				device.RegisterDeviceSignatureFailure(device.ID);
                return false;
            }
            return true;
        }

        [WebMethod]
        public bool KeepAlive(string credentialsToken)
        {
			return SetContext(credentialsToken);
            //var user = ObjectContext.Current.User;
            //return true; //Netpay.Infrastructure.Security.SecurityManager.IsLoggedin(creds);
        }

		private ProcessResult CreateFailResult(string errorMessage)
		{
			ProcessResult result = new ProcessResult();
			result.IsSuccess = false;
			result.ReplyText = errorMessage;
			result.Signature = CalcSignature(result);
			return result;
		}

        [WebMethod]
        public ProcessResult Process(string credentialsToken, string signature, string cardholderName, int transType, int typeCredit, string creditcard, string cvv, int expirationMonth, int expirationYear, string currency, string amount, int payments, string email, string personalNumber, string phone, string track2, string deviceId)
        {
			return Process2(credentialsToken, signature, cardholderName, transType, typeCredit, creditcard, cvv, expirationMonth, expirationYear, currency, amount, payments, email, personalNumber, phone, track2, deviceId, "");
		}

        [WebMethod]
		public ProcessResult Process2(string credentialsToken, string signature, string cardholderName, int transType, int typeCredit, string creditcard, string cvv, int expirationMonth, int expirationYear, string currency, string amount, int payments, string email, string personalNumber, string phone, string track2, string deviceId, string payFor)
        {
			return Process3(credentialsToken, signature, cardholderName, transType, typeCredit, creditcard, cvv, expirationMonth, expirationYear, currency, amount, payments, email, personalNumber, phone, track2, deviceId, payFor, 0);
		}
        [WebMethod]
		public ProcessResult Process3(string credentialsToken, string signature, string cardholderName, int transType, int typeCredit, string creditcard, string cvv, int expirationMonth, int expirationYear, string currency, string amount, int payments, string email, string personalNumber, string phone, string track2, string deviceId, string payFor, int productId)
        {
			if (!SetContext(credentialsToken)) return null;
            var activationCode = Bll.Accounts.MobileDevice.Current.PassCode;
			if (!ValidateSignature(signature, credentialsToken, cardholderName, transType.ToString(), typeCredit.ToString(), creditcard, cvv, expirationMonth.ToString(), expirationYear.ToString(), currency, amount, payments.ToString(), email, personalNumber, phone, track2, deviceId, payFor, productId.ToString()))
				return CreateFailResult("signature contains an invalid value");

			if(expirationYear  > 0 && expirationYear < 100) expirationYear += 2000;
            if (cardholderName.NullIfEmpty() != null && !cardholderName.IsName())
				return CreateFailResult("cardholderName contains an invalid Value");

            if (creditcard.NullIfEmpty() != null && !creditcard.IsCreditcard())
				return CreateFailResult("card number contains an invalid Value");

            if (cvv.NullIfEmpty() != null && !cvv.IsCvv())
				return CreateFailResult("cvv contains an invalid Value");

			if (expirationMonth < 1 || expirationMonth > 12)
				return CreateFailResult("experation month contains an invalid Value");

			if (expirationYear < 1800 || expirationYear > (DateTime.Now.Year + 100))
				return CreateFailResult("experation year contains an invalid Value");

            if (currency.NullIfEmpty() != null && !currency.IsCurrency())
				return CreateFailResult("currency contains an invalid Value");

            if (amount.NullIfEmpty() != null && !amount.IsAmount())
				return CreateFailResult("amount contains an invalid Value");

            if (email.NullIfEmpty() != null && !email.IsEmail())
				return CreateFailResult("email contains an invalid Value");

            if (personalNumber.NullIfEmpty() != null && !personalNumber.IsPersonalNumber())
				return CreateFailResult("personal number contains an invalid Value");

            if (track2.NullIfEmpty() != null && !track2.IsTrack2())
				return CreateFailResult("trackII contains an invalid Value");

            if (deviceId.NullIfEmpty() != null && !deviceId.IsImei())
				return CreateFailResult("deviceId contains an invalid Value");

            ProcessResult result = new ProcessResult();
            System.Text.Encoding serviceEncoding = System.Text.Encoding.GetEncoding("windows-1255");
            // process
            StringBuilder urlBuilder = new StringBuilder();
			urlBuilder.Append(Domain.Current.ProcessUrl + "remote_charge.asp");
			urlBuilder.Append("?CompanyNum=" + Bll.Merchants.Merchant.Current.Number);
            urlBuilder.Append("&requestSource=" + (int)TransactionSource.MobileApp);
            urlBuilder.Append("&Member=" + cardholderName.ToEncodedUrl(serviceEncoding));
            urlBuilder.Append("&TransType=" + transType);
            urlBuilder.Append("&TypeCredit=" + typeCredit);
            urlBuilder.Append("&CardNum=" + creditcard.ToEncodedUrl(serviceEncoding));
            urlBuilder.Append("&ExpMonth=" + expirationMonth.ToString("00"));
            urlBuilder.Append("&ExpYear=" + expirationYear);
            urlBuilder.Append("&CVV2=" + cvv);
            urlBuilder.Append("&Currency=" + currency.ToEncodedUrl(serviceEncoding));
            urlBuilder.Append("&Amount=" + amount.ToEncodedUrl(serviceEncoding));
            urlBuilder.Append("&Payments=" + payments);
            urlBuilder.Append("&Email=" + email.ToEncodedUrl(serviceEncoding));
            urlBuilder.Append("&PersonalNum=" + personalNumber.ToEncodedUrl(serviceEncoding));
            urlBuilder.Append("&PhoneNumber=" + phone.ToEncodedUrl(serviceEncoding));
            urlBuilder.Append("&Track2=" + track2.ToEncodedUrl(serviceEncoding));
            urlBuilder.Append("&ClientIP=" + System.Web.HttpContext.Current.Request.UserHostAddress.ToEncodedUrl(serviceEncoding));
			if (productId != 0) urlBuilder.Append("&MerchantProductId=" + productId);
			urlBuilder.Append("&PayFor=" + payFor.Truncate(100).ToEncodedUrl(serviceEncoding));
            urlBuilder.Append("&deviceId=" + Bll.Accounts.MobileDevice.Current.ID);
            string response = null;
            try
            {
                WebClient client = new WebClient();
                response = client.UploadString(urlBuilder.ToString(), "get");
            }
            catch (WebException exception)
            {
                using (var reader = new StreamReader(exception.Response.GetResponseStream()))
                {
                    response = reader.ReadToEnd();
                    Netpay.Infrastructure.Logger.Log(LogSeverity.Error, LogTag.MobileApp, "error while sending request from mobile web services to remote charge", response);

                    result.IsSuccess = false;
                    result.ReplyText = "Unexpected failure";
					result.Signature = CalcSignature(result);
                    return result;
                }
            }

            var parsed = HttpUtility.ParseQueryString(response);
            result.ReplyCode = parsed.Get("Reply");
            result.IsSuccess = result.ReplyCode == "000";
            result.ReplyText = parsed.Get("ReplyDesc");
            result.TransactionNumber = parsed.Get("TransID");
			result.Signature = CalcSignature(result);

            return result;
        }

        [WebMethod]
        public MobileLoginResult Login(string email, string userName, string password, string deviceId, string appVersion)
        {

            //TODO: remove before deploying to .net
            //return SetTestLoginResult();
            if (email == null || userName == null || password == null)
                return new MobileLoginResult() { IsLoginSuccessful = false };

            if (email.Trim() == string.Empty || userName.Trim() == string.Empty || password.Trim() == string.Empty)
                return new MobileLoginResult() { IsLoginSuccessful = false };

            if (!email.IsEmail())
                return new MobileLoginResult() { IsLoginSuccessful = false };

            if (!userName.IsUserName())
                return new MobileLoginResult() { IsLoginSuccessful = false };

            if (!password.IsPassword())
                return new MobileLoginResult() { IsLoginSuccessful = false };

            if (appVersion != null && !appVersion.IsAppVersion())
                return new MobileLoginResult() { IsLoginSuccessful = false };

            Guid creds = Guid.Empty;
            MobileLoginResult result = new MobileLoginResult();
            LoginResult loginResult = LoginResult.UserNotFound;
            loginResult = Infrastructure.Security.Login.DoLogin(new UserRole[] { UserRole.Merchant, UserRole.MerchantSubUser }, userName, email, password, deviceId, out creds);
            //if (loginResult == LoginResult.UserNotFound)
            //    loginResult = SecurityManager.Login(WebUtils.DomainHost, UserRole.MerchantLimited, userName, email, password, deviceId, out creds);
            result.CredentialsToken = creds.ToString();
            result.VersionUpdateRequired = false;
            result.IsLoginSuccessful = (loginResult == LoginResult.Success);
            if (result.IsLoginSuccessful)
            {
				ObjectContext.Current.CredentialsToken = creds;
				if (!Netpay.Bll.Merchants.Mobile.MobileAppSettings.IsMobileEnabled(Bll.Merchants.Merchant.Current.ID))
					return new MobileLoginResult() { IsLoginSuccessful = false };
				if (!Bll.ThirdParty.Orange.checkProvisionStatus(creds))
				{
					result.LoginMessage = "מנוי לא מוגדר לשרות, נא לפנות לחברת הסלולר";
					result.IsLoginSuccessful = false;
					return result;
				}
                var device = Bll.Accounts.MobileDevice.Current;
				if (device != null){
					result.IsDeviceRegistered = true;
					result.IsDeviceActivated = device.IsActivated;
					result.IsDeviceBlocked = !device.IsActive;
				}
				result.MerchantNumber = Bll.Merchants.Merchant.Current.Number;
                result.LastLogin = Infrastructure.Security.Login.Current.LastLogin.GetValueOrDefault();
				if (result.IsDeviceBlocked) result.IsLoginSuccessful = false;
			}

            return result;
        }

        private MobileLoginResult SetTestLoginResult()
        {
            Guid creds = Guid.Empty;
            MobileLoginResult result = new MobileLoginResult
            {
                CredentialsToken = creds.ToString(),
                VersionUpdateRequired = false,
                IsLoginSuccessful = true,
                IsDeviceRegistered = true,
                IsDeviceActivated = true,
                IsDeviceBlocked = false
            };

            return result;
        }

        [WebMethod]
        public MobileLoginResult Relogin(string merchantNumber, string email, string userName, string password, string deviceId, string appVersion)
        {
            if (merchantNumber == null || userName == null || email == null || password == null || deviceId == null || appVersion == null)
                return new MobileLoginResult() { IsLoginSuccessful = false };

            if (password != null && !password.IsPassword())
                return new MobileLoginResult() { IsLoginSuccessful = false };

            if (appVersion != null && !appVersion.IsAppVersion())
                return new MobileLoginResult() { IsLoginSuccessful = false };

            var merchant = Netpay.Bll.Merchants.Merchant.Load(merchantNumber);
            if (merchant == null)
                return new MobileLoginResult() { IsLoginSuccessful = false };
            if (merchant.EmailAddress.ToSha256() != email || merchant.UserName.ToSha256() != userName)
                return new MobileLoginResult() { IsLoginSuccessful = false };

            return Login(merchant.EmailAddress, merchant.UserName, password, deviceId, appVersion);
        }

        [WebMethod]
        public GetSettingsResult GetSettings(string credentialsToken, string signature)
        {
			if (!SetContext(credentialsToken)) return null;
            if (!ValidateSignature(signature, credentialsToken))
                return null;

            GetSettingsResult result = new GetSettingsResult();
            result.Settings = new MobileSettings(Netpay.Bll.Merchants.Merchant.Load(), Netpay.Bll.Merchants.Mobile.MobileAppSettings.Load(Bll.Merchants.Merchant.Current.ID));
			if (result.Settings.ValueAddedTax == null)
                result.Settings.ValueAddedTax = Netpay.Bll.Merchants.Merchant.GetMerchantTaxRate(Bll.Merchants.Merchant.Current.ID) * 100;
            result.Signature = CalcSignature(result);
            return result;
        }

        [WebMethod]
        public void Log(int severityId, string message, string longMessage)
        {
            Netpay.Infrastructure.Logger.Log((LogSeverity)severityId, LogTag.MobileApp, message, longMessage);
        }

        [WebMethod]
        public RegistrationResult RegisterDevice(string credentialsToken, string deviceId, string phoneNumber)
        {
            RegistrationResult result = new RegistrationResult();
			if (!SetContext(credentialsToken)) return null;

            if (deviceId != null && !deviceId.IsImei())
            {
                result.IsSuccessful = false;
                result.Signature = CalcSignature(result);
                return result;
            }

            if (phoneNumber != null && !phoneNumber.IsPhone())
            {
                result.IsSuccessful = false;
				result.Signature = CalcSignature(result);
                return result;
            }
            var appSettings = Netpay.Bll.Merchants.Mobile.MobileAppSettings.Load(Bll.Merchants.Merchant.Current.ID);
            if (appSettings == null || !appSettings.IsMobileAppEnabled)
            {
                result.IsSuccessful = false;
                result.Reason = MobileDeviceRegistrationResult.GeneralFailure.ToString();
                result.Signature = CalcSignature(result, false);
                return result;
            }

            Netpay.Bll.Accounts.MobileDevice device = null;
            var regResult = Netpay.Bll.Accounts.MobileDevice.RegisterMobileDevice(appSettings.MaxDeviceCount, deviceId, null, phoneNumber, Context.Request.Headers["User-Agent"], out device);
            if (regResult != MobileDeviceRegistrationResult.Success)
            {
                result.IsSuccessful = false;
                result.Reason = regResult.ToString();
				result.Signature = CalcSignature(result, false);
                return result;
            }
            BoolResult sendPasscodeResult = SendPassCode(credentialsToken, deviceId);
            if (!sendPasscodeResult.Value)
            {
                result.IsSuccessful = false;
                result.Reason = "sendCodeFailure";
				result.Signature = CalcSignature(result, false);
                return result;
            }

            result.IsSuccessful = true;
			result.Signature = CalcSignature(result, false);
            return result;
        }

        [WebMethod]
        public BoolResult SendPassCode(string credentialsToken, string deviceId)
        {
            BoolResult result = new BoolResult();
			if (!SetContext(credentialsToken)) return null;
            if (deviceId != null && !deviceId.IsImei())
            {
                result.Value = false;
                result.Signature = CalcSignature(result);
                return result;
            }

            result.Value = Netpay.Bll.Accounts.MobileDevice.Current.SendMobilePassCode();
            result.Signature = CalcSignature(result);
            return result;
        }

        [WebMethod]
        public BoolResult ActivateDevice(string credentialsToken, string signature, string deviceId)
        {
            BoolResult result = new BoolResult();
			if (!SetContext(credentialsToken)) return null;
			if (deviceId != null && !deviceId.IsImei())
			{
				result.Value = false;
				result.Signature = CalcSignature(result);
				return result;
			}
            if (!ValidateSignature(signature, credentialsToken, deviceId)) result.Value = false;
            else result.Value = Netpay.Bll.Accounts.MobileDevice.Current.ActivateMobileDevice(Bll.Accounts.MobileDevice.Current.PassCode);
            result.Signature = CalcSignature(result, false);
            return result;
        }

		[WebMethod]
		public List<ProductDetails> GetProductList(string credentialsToken)
		{
			if (!SetContext(credentialsToken)) return null;
			var products = Bll.Shop.Products.Product.Search(new Bll.Shop.Products.Product.SearchFilters() { MerchantId = Bll.Merchants.Merchant.Current.ID }, new SortAndPage() { PageSize = 100 });
			return products.Select(p => new ProductDetails(p)).ToList();
		}

        [WebMethod]
        public BoolResult SendEmail(string credentialsToken, string signature, int transactionID, string email)
        {
			if (!SetContext(credentialsToken)) return null;

            BoolResult result = new BoolResult();
            if (email != null && !email.IsEmail())
            {
                result.Value = false;
                result.Signature = CalcSignature(result);
                return result;
            }

            if (!ValidateSignature(signature, credentialsToken, transactionID.ToString(), email))
                return null;

			Netpay.Bll.Transactions.Transaction.GetTransaction(null, transactionID, TransactionStatus.Captured).SendClientEmail(email);
            result.Signature = CalcSignature(result);

            return result;
        }

        [WebMethod]
        public BoolResult SendSms(string credentialsToken, string signature, int transactionID, string phone, string merchantText)
        {
			if (!SetContext(credentialsToken)) return null;

            BoolResult result = new BoolResult();
            if (phone != null && !phone.IsPhone())
            {
                result.Value = false;
                result.Signature = CalcSignature(result);
                return result;
            }

			if (!ValidateSignature(signature, credentialsToken, transactionID.ToString(), phone, merchantText))
                return null;

            //TODO: Test Data by P Musisha
            var tx = new Bll.Transactions.Transaction();
            tx.SendClientSMS(phone);

            //TODO: Test Data by P Musisha (Uncomment this)
           // Netpay.Bll.Transactions.Transaction.GetTransaction(null, transactionID, TransactionStatus.Captured).SendClientSMS(phone /*,merchantText*/);
			
            
            //result.Value = Netpay.Bll.Merchants.SendClientTransactionSms(WebUtils.DomainHost, user.ID, transactionID, phone, merchantText);
            //result.Signature = CalcSignature(result);

            return result;
        }

        [WebMethod]
        public ProcessResult Refund(string credentialsToken, int transactionID, string deviceID, string signature)
        {
			if (!SetContext(credentialsToken)) return null;
			if (!ValidateSignature(signature, credentialsToken, transactionID.ToString(), deviceID))
                return null;

            //TransactionVO originalTrans = Transactions.GetTransaction(creds, transactionID, TransactionStatus.Captured);
            //if (originalTrans == null)
                //return null;

            ProcessResult result = new ProcessResult();
            System.Text.Encoding serviceEncoding = System.Text.Encoding.GetEncoding("windows-1255");
            StringBuilder urlBuilder = new StringBuilder();
			urlBuilder.Append(Domain.Current.ProcessUrl + "remote_charge.asp");
			urlBuilder.Append("?CompanyNum=" + Bll.Merchants.Merchant.Current.Number);
            urlBuilder.Append("&requestSource=41"); 
            //urlBuilder.Append("&Amount=" + originalTrans.Amount);
            //urlBuilder.Append("&Currency=" + originalTrans.CurrencyIsoCode);
            //urlBuilder.Append("&Payments=" + originalTrans.Payments);
            urlBuilder.Append("&RefTransID=" + transactionID);
            //urlBuilder.Append("&Member=" + "cardholder name");
            //urlBuilder.Append("&CardNum=" + "4580000000000000");
            //urlBuilder.Append("&ExpMonth=" + "01");
            //urlBuilder.Append("&ExpYear=" + "2021");
            urlBuilder.Append("&TransType=0");
            urlBuilder.Append("&TypeCredit=0");
            urlBuilder.Append("&ClientIP=" + System.Web.HttpContext.Current.Request.UserHostAddress.ToEncodedUrl(serviceEncoding));
            urlBuilder.Append("&deviceId=" + Bll.Accounts.MobileDevice.Current.ID);
            
            string response = null;
            try
            {
                WebClient client = new WebClient();
                response = client.UploadString(urlBuilder.ToString(), "get");
            }
            catch (WebException exception)
            {
                using (var reader = new StreamReader(exception.Response.GetResponseStream()))
                {
                    response = reader.ReadToEnd();
                    Netpay.Infrastructure.Logger.Log(LogSeverity.Error, LogTag.MobileApp, "error while sending request from mobile web services to remote charge", response);

                    result.IsSuccess = false;
                    result.ReplyText = "Unexpected failure";
                    result.Signature = CalcSignature(result);
                    return result;
                }
            }

            var parsed = HttpUtility.ParseQueryString(response);
            result.ReplyCode = parsed.Get("Reply");
            result.IsSuccess = result.ReplyCode == "000";
            result.ReplyText = parsed.Get("ReplyDesc");
            result.TransactionNumber = parsed.Get("TransID");
            result.Signature = CalcSignature(result);

            return result;
        }

        [WebMethod]
        public StringResult RefundRequest(string credentialsToken, int transactionID, decimal amount, string comment, string signature) 
        {
			if (!SetContext(credentialsToken)) return null;
			if (!ValidateSignature(signature, credentialsToken, transactionID.ToString(), amount.ToString(), comment))
                return null;

			StringResult result = new StringResult();
			result.Value = Bll.Transactions.RefundRequest.Create(transactionID, TransactionStatus.Captured, amount, comment).ToString();
            result.Signature = CalcSignature(result);
            return result;
        }

        [WebMethod]
        public BoolResult SaveTransImage(string credentialsToken, string signature, int transId, int fileType, string fileData)
        {
            BoolResult result = new BoolResult();
			if (!SetContext(credentialsToken)) return null;
			if (!ValidateSignature(signature, credentialsToken, transId.ToString(), fileType.ToString(), fileData)) return result;
            try
            {
                if (fileData.Length < 100000) //around 75kb file size
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(System.Convert.FromBase64String(fileData));
                    var img = System.Drawing.Image.FromStream(ms);
                    //System.Drawing.Imaging.ImageCodecInfo encInfo = null;
                    //foreach (System.Drawing.Imaging.ImageCodecInfo info in System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders())
                    //    if (info.FilenameExtension == "PNG") { encInfo = info; break; }
                    //Check if the Transaction Images folder exist, if not, create it
                    if (!System.IO.Directory.Exists(Domain.Current.MapPrivateDataPath("Transaction Images")))
                    {
                        System.IO.Directory.CreateDirectory(Domain.Current.MapPrivateDataPath("Transaction Images"));
                    }

                    img.Save(Domain.Current.MapPrivateDataPath(string.Format("Transaction Images/Tran{0}_{1}.png", transId.ToString(), fileType.ToString())), System.Drawing.Imaging.ImageFormat.Png);
                    result.Value = true;
                }
            }
            catch (Exception e)
            {
                Logger.Log(e, "while uploading file from mobile app");
            }
            result.Signature = CalcSignature(result);
            return result;
        }

		[WebMethod]
		public ProcessResult GetTransWithQrId(string credentialsToken, string transQrId)
		{
			if (!SetContext(credentialsToken)) return null;
			var trans = Bll.Transactions.Transaction.GetTransWithQrId(transQrId);
			if (trans == null) return null;
			var result  = new ProcessResult() { IsSuccess = true, ReplyCode = "000", ReplyText = "Success", TransactionNumber = trans.ID.ToString() };
			result.Signature = CalcSignature(result);
			return result;
		}

        [WebMethod]
        public byte[] GetQrCodeImage(string credentialsToken, string signature, string hppParams)
		{
			if (!SetContext(credentialsToken)) return null;
			if (!ValidateSignature(signature, credentialsToken, hppParams)) return null;
			
			StringBuilder sb = new StringBuilder();
			var dic = System.Web.HttpUtility.ParseQueryString(hppParams);
			foreach (string k in dic) sb.Append(dic[k]);
			sb.Append(Bll.Merchants.Merchant.Current.HashKey);
			string hppSign = System.Convert.ToBase64String(System.Security.Cryptography.SHA256.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(sb.ToString())));
			if (!hppParams.EndsWith("&")) hppParams += "&";
			hppParams += "signature=" + HttpUtility.UrlEncode(hppSign);
			return (new QrCodes()).GenerateCode(QrCodes.TargetType.HostedPaymentPageV2, hppParams);
		}

		[WebMethod]
		public string GetQrCodeImageBase64(string credentialsToken, string signature, string hppParams)
		{
			return GetQrCodeImage(credentialsToken, signature, hppParams).ToBase64();
		}

		[WebMethod]
		public List<TransactionInfo> GetTransactionHistory(string credentialsToken, int page, int pageSize)
		{
			if (!SetContext(credentialsToken)) return null;

			var filters = new Bll.Transactions.Transaction.SearchFilters();
			var sortAndPage = new SortAndPage(page - 1, pageSize, "InsertDate", true);
			var transactions = Bll.Transactions.Transaction.Search(TransactionStatus.Captured, filters, sortAndPage, true, false);
			return transactions.Select(t => new TransactionInfo() { ID = t.ID, Amount = t.Amount, Currency = t.CurrencyIsoCode, InsertDate = t.InsertDate }).ToList();
		}

		[WebMethod]
		public GenericResult GetTransaction(string credentialsToken, string signature, int transactionId) 
		{
			if (!SetContext(credentialsToken)) return null;
			if (!ValidateSignature(signature, credentialsToken, transactionId.ToString()))
				return null;

			var transactionVO = Bll.Transactions.Transaction.GetTransaction(null, transactionId, TransactionStatus.Captured);
			TransactionInfo transactionInfo = new TransactionInfo(transactionVO);
			GenericResult result = new GenericResult();
			result.Data = transactionInfo;
			result.Signature = CalcSignature(result);

			return result;
		}

        [WebMethod]
        public string CertPing()
        {
            return "ok";
        }

        /*
        [WebMethod]
        public string[] GetSplashImageUrls()
        {
            return new string[] {
                @"http://localhost/WebServices/SysMessages.jpg",
                @"http://localhost/WebServices/SysAbout.jpg",
            };
        }
        */
    }
}
