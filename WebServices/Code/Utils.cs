﻿using System;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Email;
using System.Collections.Generic;

namespace Netpay.WebServices.Code
{
    public static class Utils
    {
        private static bool mIsEmailTemplatesRegistered = false;

        private enum ContentType 
        {
            unknown,
            soap,
            xml,
            json,
            query
        }

        private static ContentType GetContentType() 
        {
            HttpContext context = HttpContext.Current;
            string contentType;
            if (context.Request["contentType"] != null)
            {
                contentType = context.Request["contentType"].ToLower();
                switch (contentType)
                {
                    case "xml":
                        contentType = "text/xml";
                        break;
                    case "soap":
                        contentType = "application/soap+xml";
                        break;
                    case "query":
                        contentType = "text/html";
                        break;
                    case "json":
                        contentType = "application/json";
                        break;
                }
            }
            else
                contentType = context.Request.ContentType.ToLower();

            if (contentType == "text/xml")
                return ContentType.xml;
            else if (contentType == "application/soap+xml")
                return ContentType.soap;
            else if (contentType == "application/json")
                return ContentType.json;
            else if (contentType == "text/html")
                return ContentType.query;
            else
                return ContentType.unknown;
        }
        
        public static T GetParameter<T>(string paramValue) 
        {
            ContentType contentType = GetContentType();
            if (contentType == ContentType.xml)
                return paramValue.FromXml<T>();
            else if (contentType == ContentType.soap)
                return paramValue.FromSoap<T>();
            else if (contentType == ContentType.json)
                return paramValue.FromJson<T>();
            else
                throw new ApplicationException("Content type not supported");
        }
        
        /// <summary>
        /// Format an object to the relevant protocol
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string FormatResponse(object data)
		{
            ContentType contentType = GetContentType();
            if (contentType == ContentType.xml)
                return data.ToXml();
            else if (contentType == ContentType.soap)
                return data.ToSoap();
            else if (contentType == ContentType.json)
                return data.ToJson();
            else if (contentType == ContentType.query)
                return data.ToQueryString();
            else
                throw new ApplicationException("Content type not supported");
		}

        public static void RegisterEmailTemplates()
        {
            if (!mIsEmailTemplatesRegistered)
            {
                Bll.EmailTemplates.RegisterTemplates(null);
                mIsEmailTemplatesRegistered = true;
            }
        }
    }
}

