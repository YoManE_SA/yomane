﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.WebServices.Code
{
    public abstract class BaseService : System.Web.Services.WebService
    {
        protected UserRole[] AllowedRoles;
        protected BaseService(UserRole[] allowedRoles) { AllowedRoles = allowedRoles; WebServices.Code.Utils.RegisterEmailTemplates(); }
        protected Login validateCredentials(bool noThrowError = false)
        {
            string credentialsToken = null;
            string credsTokenKey = "credentialsToken";
            if (Context.Request.QueryString.AllKeys.Contains(credsTokenKey)) credentialsToken = Context.Request.QueryString.Get(credsTokenKey);
            else if (Context.Request.Cookies.AllKeys.Contains(credsTokenKey)) credentialsToken = Context.Request.Cookies.Get(credsTokenKey).Value;
            if (credentialsToken != null)
            {
                Guid creds;
                if (Guid.TryParse(credentialsToken, out creds))
                {
                    var login = Infrastructure.Security.Login.Get(creds, false);
                    if ((login != null) && (AllowedRoles == null || AllowedRoles.Any(r => login.IsInRole(r))))
                    {
                        ObjectContext.Current.CredentialsToken = creds;
                        return login;
                    }
                }
            }
            if (noThrowError) return null;
            Context.Response.Buffer = false;
            Context.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
            Context.Response.StatusDescription = "Not logged in";
            Context.Response.Write("Not logged in");
            Context.Response.End();
            return null;
        }
        /*
        protected static Guid CredentialsToken
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies.Get("credentialsToken");
                if (cookie == null || cookie.Value == null) return Guid.Empty;
                return cookie.Value.ToGuid();
            }
            set
            {
                var cookie = HttpContext.Current.Request.Cookies.Get("credentialsToken");
                if (cookie == null) cookie = new HttpCookie("credentialsToken");
                cookie.Value = value.ToString();
                HttpContext.Current.Response.Cookies.Set(cookie);
            }
        }
        */
        public class ServiceResult
        {
            public bool IsSuccess { get; set; }
            public int Code { get; set; }
            public string Key { get; set; }
            public string Message { get; set; }
            public string Number { get; set; }
            public ServiceResult() { }
            internal ServiceResult(ValidationResult result, string message = null, string number = null) {
                IsSuccess = (result == ValidationResult.Success);
                Code = (int)result;
                Key = result.ToString();
                if (message == null) message = Infrastructure.ErrorText.GetErrorMessage(null, Code);
                Message = message;
                if (string.IsNullOrEmpty(Message)) Message = string.Format("Error: {0} - {1}", Code, Key);
                Number = number;
            }
        }

    }
}