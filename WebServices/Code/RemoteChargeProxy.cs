﻿using System;
using System.Web;
using Netpay.WebServices.Code;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Web;
using Netpay.Bll;
using System.Net;
using System.IO;

namespace Netpay.WebServices.Code
{
    public class RemoteChargeProxy : IHttpHandler
	{
		#region IHttpHandler Members

		public bool IsReusable
		{
			get { return true; }
		}

		public void ProcessRequest(HttpContext context)
		{
            WebClient client = new WebClient();
            string url = "http://192.168.5.11/member/remote_charge.asp";
            if (context.Request.QueryString.Count > 0)
                url += "?" + context.Request.QueryString.ToQueryString();
            string postData = context.Request.Form.ToQueryString();
            
            try
            {
                string response = client.UploadString(url, "post", postData);
                context.Response.Write(response);
            }
            catch (WebException e)
            {
                string responseText;
                using (var reader = new StreamReader(e.Response.GetResponseStream()))
                    responseText = reader.ReadToEnd();

                throw new Exception(e.ToString() + "<br/><br/>" + responseText);
            }
		}

		#endregion
	}
}
