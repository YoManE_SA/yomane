﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.FraudDetection;
using System.IO;
using System.Text;

using Netpay.Infrastructure;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Netpay.WebServices
{
    /// <summary>
    /// Summary description for FraudDetection
    /// </summary>
    [WebService(Namespace = "WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FraudDetection : System.Web.Services.WebService
    {
        public FraudDetection()
        {
            Code.Utils.RegisterEmailTemplates();
        }

        [WebMethod]
        [OperationContract, WebGet]
        public void Detect()
        {
            DetectionContext detectionContext = new DetectionContext(HttpContext.Current.Request.QueryString);
            FraudDetectionManager.OnlineDetectionResult result = FraudDetectionManager.DetectOnline(detectionContext);
            string queryString = null;
            if (result.IsDetected)
                queryString = string.Format("isDetected={0}&resultCode={1}&logIds={2}", result.IsDetected.ToString().ToLower(), result.Result.Rule.Code, result.LogIds.ToDelimitedString());
            else
                queryString = string.Format("isDetected={0}&resultCode=&logIds=", result.IsDetected.ToString().ToLower());
 
            Context.Response.Write(queryString);
        }
    }
}
