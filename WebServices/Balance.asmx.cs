﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;

namespace Netpay.WebServices
{
    [WebService(Namespace = "WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class Balance : Code.BaseService
    {
        #region Service Classes

        public class BalanceRow
        {
            public int ID { get; set; }
            public string CurrencyIso { get; set; }
            public decimal Amount { get; set; }
            public decimal Total { get; set; }
            public System.DateTime InsertDate { get; set; }
            public string Text { get; set; }
            public bool IsPending { get; set; }
            public int? SourceID { get; set; }
            public string SourceType { get; set; }
            public BalanceRow() { }
            #region Save And Load
            internal BalanceRow(Bll.Accounts.Balance cs)
            {
                if (cs == null) return;
                ID = cs.ID;
                CurrencyIso = cs.CurrencyIso;
                Amount = cs.Amount;
                Total = cs.Total;
                InsertDate = cs.InsertDate;
                Text = cs.Text;
                IsPending = cs.IsPending;
                SourceID = cs.SourceID;
                SourceType = cs.SourceType.ToString();
            }
            #endregion
        }

        public class BalanceRequest
        {
            public int ID { get; set; }
            public System.DateTime RequestDate { get; set; }
            public string SourceAccountNumber { get; set; }
            public string TargetAccountNumber { get; set; }
            public string SourceText { get; set; }
            public string TargetText { get; set; }

            public string SourceAccountName { get; set; }
            public string TargetAccountName { get; set; }

            public bool IsPush { get; set; }
            public decimal Amount { get; set; }
            public string CurrencyISOCode { get; set; }

            public bool? IsApproved { get; set; }
            public System.DateTime? ConfirmDate { get; set; }
            public BalanceRequest() { }
            #region Save And Load
            internal BalanceRequest(Bll.Accounts.BalanceRequest cs, Dictionary<int, Bll.Accounts.AccountInfo> accountNames)
            {
                ID = cs.ID;
                RequestDate = cs.RequestDate;
                SourceText = cs.SourceText;
                TargetText = cs.TargetText;
                IsPush = cs.IsPush;
                Amount = cs.Amount;
                CurrencyISOCode = cs.CurrencyISOCode;
                IsApproved = cs.IsApproved;
                ConfirmDate = cs.ConfirmDate;

                Bll.Accounts.AccountInfo acValue = null;
                if (accountNames.TryGetValue(cs.SourceAccountID, out acValue))
                {
                    SourceAccountNumber = acValue.AccountNumber;
                    SourceAccountName = acValue.AccountName;
                }
                if (accountNames.TryGetValue(cs.TargetAccountID, out acValue))
                {
                    TargetAccountNumber = acValue.AccountNumber;
                    TargetAccountName = acValue.AccountName;
                }
            }
        }
        #endregion
        public class BalanceFilter
        {
            public int? StoredPaymentMethodID { get; set; }
            public int Page { get; set; }
            public int PageSize { get; set; }
            public string CurrencyIso { get; set; }
        }

        public class BalanceTotal
        {
            public string CurrencyIso { get; set; }
            public decimal Value { get; set; }
        }

        #endregion

        public Balance() : base(new UserRole[] { UserRole.Account }) { }

        [WebMethod(Description = "The service will return all types of balance changes to the user with the information about the source type once you identify the source type you can call another service with the source id and get the relevant information you need. The source types are: System.Admin, System.Deposit, System.DepositFee, System.PedingCapture, System.Purchase, System.Transfer, System.TransferFee, System.Withdrawal, System.WithdrawalFee, Wires.AffiliateSettlement, Wires.PaymentRequest, Wires.Settlement, Wires.SettlementFee, Wires.Wire")]
        public List<BalanceRow> GetRows(BalanceFilter filters)
        {
            validateCredentials();
            List<Bll.Accounts.Balance> ret = null;
            if (filters.StoredPaymentMethodID.HasValue)
            {
                var pm = Bll.Accounts.StoredPaymentMethod.Load(filters.StoredPaymentMethodID.Value);
                if (pm == null) throw new Exception("Invalid StoredPaymentMethodID");
                if (pm.Provider == null) new Exception("StoredPaymentMethodID invalid provider");
                ret = pm.Provider.GetBalace(pm, new Range<DateTime>(DateTime.Now.AddYears(-1), DateTime.Now));
            }
            else ret = Bll.Accounts.Balance.Search(new Bll.Accounts.Balance.SearchFilters() { AccountID = Bll.Accounts.Account.Current.AccountID, CurrencyIso = filters.CurrencyIso }, new Netpay.Infrastructure.SortAndPage(filters.Page, filters.PageSize));
            return ret.Select(r => new BalanceRow(r)).ToList();
        }

        [WebMethod(Description = "The service returns the total available balance for the user - per currency")]
        public List<BalanceTotal> GetTotal(string currencyIsoCode, bool includePending)
        {
            validateCredentials();
            var ret = Bll.Accounts.Balance.GetStatus(Bll.Accounts.Account.Current.AccountID).Select(r => new BalanceTotal() { CurrencyIso = r.CurrencyIso, Value = includePending ? r.Expected : r.Current });
            if (!string.IsNullOrEmpty(currencyIsoCode)) ret = ret.Where(r => r.CurrencyIso == currencyIsoCode);
            return ret.ToList();
        }

        [WebMethod(Description = "This service call will allow a user to “push” an amount to another user with an original request. The user will be prompt for a Pin code to approve the transfer as all balance transfer requests. even push transfers gets a request id (number) you can in a later stage to track the request.")]
        public ServiceResult TransferAmount(string destAcocuntId, decimal amount, string currencyIso, string pinCode, string text)
        {
            validateCredentials();
            var targetAccount = Bll.Accounts.Account.AccountIDByNumber(Bll.Accounts.AccountType.Customer, destAcocuntId);
            if (targetAccount == null) return new ServiceResult(Infrastructure.ValidationResult.Item_Not_Exist);
            if (!Bll.Accounts.Account.Current.ValidatePinCode(pinCode)) return new ServiceResult(Infrastructure.ValidationResult.PinCodeConfirmationMissmatch);
            var req = new Bll.Accounts.BalanceRequest(Bll.Accounts.Account.Current.AccountID, targetAccount.Value, amount, currencyIso, text, true);
            req.Save();
            return new ServiceResult(Infrastructure.ValidationResult.Success);
        }

        [WebMethod(Description = "Once a request for an amount reaches a user he has the option to reply either “yes” or “no” – once the user decides he must approve the reply with his pin code.")]
        public ServiceResult ReplyRequest(int requestId, bool approve, string pinCode, string text)
        {
            validateCredentials();
            var request = Bll.Accounts.BalanceRequest.Load(requestId);
            if (request == null) return new ServiceResult(Infrastructure.ValidationResult.Invalid_RequestID);
            //if (request.SourceAccountID != targetCustomer.AccountID) return new ServiceResult(Infrastructure.ValidationResult.Invalid_RequestForAccountID);
            if (!Bll.Accounts.Account.Current.ValidatePinCode(pinCode)) return new ServiceResult(Infrastructure.ValidationResult.PinCodeConfirmationMissmatch);
            request.Reply(approve, text);
            request.Save();
            return new ServiceResult(Infrastructure.ValidationResult.Success);
        }

        [WebMethod(Description = "The base of the balance service is to allow users to help other users and transfer money from one another. The request amount gives the user an option to ask for an amount from his friends (Friends service will be described separately in the Customers service)")]
        public ServiceResult RequestAmount(string destAcocuntId, decimal amount, string currencyIso, string text)
        {
            validateCredentials();
            var targetAccount = Bll.Accounts.Account.AccountIDByNumber(Bll.Accounts.AccountType.Customer, destAcocuntId);
            if (targetAccount == null) return new ServiceResult(Infrastructure.ValidationResult.Item_Not_Exist);
            new Bll.Accounts.BalanceRequest(Bll.Accounts.Account.Current.AccountID, targetAccount.Value, amount, currencyIso, text, false).Save();
            return new ServiceResult(Infrastructure.ValidationResult.Success);
        }

        [WebMethod(Description = "Allows to call for all requests by a certain currency or payment method limited to the logged in user – the service allows to limit the amount of items per page.")]
        public List<BalanceRequest> GetRequests(BalanceFilter filters)
        {
            validateCredentials();
            var items = Bll.Accounts.BalanceRequest.Search(new Bll.Accounts.BalanceRequest.SearchFilters() { AnyAccountID = Bll.Accounts.Account.Current.AccountID, CurrencyISOCode = filters.CurrencyIso }, new Netpay.Infrastructure.SortAndPage(filters.Page, filters.PageSize, "AccountBalanceMoneyRequest_id", true));
            var names = Netpay.Bll.Accounts.Account.GetAccountInfo(items.SelectMany(v => new int[] { v.SourceAccountID, v.TargetAccountID }).ToList());
            return items.Select(r => new BalanceRequest(r, names)).ToList();
        }

        [WebMethod(Description = "The getrequest service will return all the information about a specific balance transfer. From the reply you can learn the status of the request and the date of its confirmation. There are two types of balance transfer – direct or upon request – direct transfer is when a user decides to push an amount to another user without him requesting for it.")]
        public BalanceRequest GetRequest(int requestId)
        {
            validateCredentials();
            var request = Bll.Accounts.BalanceRequest.Load(requestId);
            if (request == null) return null;
            var names = Netpay.Bll.Accounts.Account.GetAccountInfo(new List<int> { request.SourceAccountID, request.TargetAccountID });
            return new BalanceRequest(request, names);
        }

        public class LoadBalanceData
        {
            public decimal Amount;
            public string CurrencyIso;
            public int? Installments;

            public int? StoredPaymentMethodID;
            public string CardNumber;
            public DateTime CardExpDate;
            public string Cvv;
            public string IdNumber;
        }

        [WebMethod(Description = "Load Balance using processing payment Method")]
        public ServiceResult LoadBalance(LoadBalanceData data)
        {
            validateCredentials();
            var payerInfo = Bll.Customers.Customer.Current;
            if (payerInfo == null) return new ServiceResult(ValidationResult.Invalid_AccountID);
            var appIdentity = payerInfo.RefIdentity;
            if (appIdentity == null) return new ServiceResult(ValidationResult.Invalid_AppIdentity);

            //get merchant by app identity
            var trans = new Bll.Transactions.Transaction();
            trans.Merchant = Bll.Merchants.Merchant.Load(appIdentity.ProcessMerchantNumber);
            trans.CustomerID = Bll.Customers.Customer.Current.ID;
            trans.CurrencyIsoCode = data.CurrencyIso;
            trans.Amount = data.Amount;
            trans.Installments = (byte)data.Installments;
            trans.CreditType = Infrastructure.CreditType.Regular;

            trans.PayForText = "Load Balance";
            trans.IP = HttpContext.Current.Request.UserHostAddress;
            //trans.OrderNumber = data.OrderId;
            trans.TransactionSource = Infrastructure.TransactionSource.WalletMobileApp;
            //if (data.ProductId != null) trans.ProductId = data.ProductId;
            //data.Comment = data.Comment;

            Bll.PersonalInfo.Copy(payerInfo, trans.PayerData);
            trans.PaymentData.MethodInstance = new Bll.PaymentMethods.CreditCard(trans.PaymentData)
            {
                CardNumber = data.CardNumber,
                Cvv = data.Cvv,
                ExpirationDate = data.CardExpDate,
            };

            Bll.Accounts.AccountAddress address = payerInfo.PersonalAddress;
            if (address != null)
            {
                if (trans.PaymentData.BillingAddress == null) trans.PaymentData.BillingAddress = new Bll.Transactions.BillingAddress();
                Bll.Address.Copy(address, trans.PaymentData.BillingAddress);
            }
            var reqtType = trans.PaymentData.MethodInstance.PaymentMethodGroupId;
            System.Collections.Specialized.NameValueCollection retParams = null;
            var reqParams = trans.GetProcessParamters();
            try
            {
                retParams = Bll.Transactions.Transaction.ProcessTransaction(reqtType, reqParams);
            }
            catch (Exception ex)
            {
                return new ServiceResult((ValidationResult)500, ex.Message);
            }
            ServiceResult result = null;
            if (reqtType == CommonTypes.PaymentMethodGroupEnum.CreditCard)
                result = new ServiceResult((ValidationResult)retParams.Get("Reply").ToInt32(-1), retParams.Get("ReplyDesc"), retParams.Get("TransID"));
            else if (reqtType == CommonTypes.PaymentMethodGroupEnum.DirectDebit || reqtType == CommonTypes.PaymentMethodGroupEnum.OnlineBankTransfer || reqtType == CommonTypes.PaymentMethodGroupEnum.InstantOnlinkBankTransfer || reqtType == CommonTypes.PaymentMethodGroupEnum.EuropeanDirectDebit)
                result = new ServiceResult((ValidationResult)retParams.Get("replyCode").ToInt32(-1), retParams.Get("replyMessage"), retParams.Get("transId"));
            else result = new ServiceResult((ValidationResult)500, "Unexpected failure");
            return result;
        }

        public class ChargeParams
        {
            public string CardNumber;
            public int ExpMonth;
            public int ExpYear;
            public string PinCode;
            public decimal Amount;
            public string CurrencyIso;
            public string Text;
            public int? RefNumber;
            public int MerchantID;
            public int? MerchantGroupID;
            public string Signature;
        }

        public class ChargeResults : ServiceResult
        {
            public int PaymentMethodId { get; set; }
            public ChargeResults() { }
            internal ChargeResults(ValidationResult result, CommonTypes.PaymentMethodEnum pmId, string message = null, string number = null)
                : base(result, message, number)
            {
                PaymentMethodId = (int)pmId;
            }
        }

        [WebMethod(Description = "Process prepaid charge request")]
        public ChargeResults Charge(ChargeParams data)
        {
            if (data == null || string.IsNullOrEmpty(data.CardNumber) || string.IsNullOrEmpty(data.CurrencyIso))
                return new ChargeResults(ValidationResult.Invalid_Data, CommonTypes.PaymentMethodEnum.CCUnknown, "Either data or currency are null");
            if (data.Signature != Infrastructure.Security.Encryption.GetSHA256Hash(string.Format("NP_{0}_{1}", data.CurrencyIso, data.CardNumber)))
                return new ChargeResults(ValidationResult.Invalid_Data, CommonTypes.PaymentMethodEnum.CCUnknown, "Wrong signature send");
            DateTime validateDate = new DateTime(data.ExpYear, data.ExpMonth, 1).AddMonths(1).AddMilliseconds(-1);
            Infrastructure.ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            try {
                var curObj = Bll.Currency.Get(data.CurrencyIso);
                var spms = Bll.Accounts.StoredPaymentMethod.FindByPAN(data.CardNumber, null);
                var spm = spms.FirstOrDefault();
                if (spm == null) return new ChargeResults(ValidationResult.Invalid_AccountValue1, CommonTypes.PaymentMethodEnum.CCUnknown); //Invalid card number
                var pmId = spm.MethodInstance.PaymentMethodId;
                if (data.Amount == 0) return new ChargeResults(ValidationResult.Invalid_Amount, pmId); //invalid amount
                if (curObj == null) return new ChargeResults(ValidationResult.Invalid_AccountID, pmId); //No credit account
                if (spm.MethodInstance.ExpirationDate.HasValue && spm.MethodInstance.PaymentMethod.IsExpirationDateMandatory) { 
                    if (spm.MethodInstance.ExpirationDate.Value < DateTime.Now) return new ChargeResults(ValidationResult.Invalid_ExpDate, pmId); //Expired card
                    if (spm.MethodInstance.ExpirationDate.Value.Date != validateDate.Date) return new ChargeResults(ValidationResult.Invalid_ExpDate, pmId); //Expired card
                }

                ////TODO: 2018/01/12 - PM Mugisha added bypass if dummy card is used.
                //if(!data.CardNumber.Equals("4580000000000000") || !data.PinCode.Equals("132"))
                //{
                //    if ((!string.IsNullOrEmpty(data.PinCode)) && !Bll.Accounts.Account.ValidatePinCode(spm.Account_id, data.PinCode)) return new ChargeResults(ValidationResult.PinCodeConfirmationMissmatch, pmId); //Incorrect PIN

                //}
                if ((!string.IsNullOrEmpty(data.PinCode)) && !Bll.Accounts.Account.ValidatePinCode(spm.Account_id, data.PinCode)) return new ChargeResults(ValidationResult.PinCodeConfirmationMissmatch, pmId); //Incorrect PIN


                //check if the merchant have terminal with the current payment method and currency
                Bll.Merchants.Merchant merchant = Bll.Merchants.Merchant.Load(data.MerchantID);
                var merchantTerminals = Bll.Merchants.ProcessTerminals.LoadForMerchant(data.MerchantID, true);
                if (merchantTerminals.Count() == 0) return new ChargeResults(ValidationResult.Invalid_PaymentMethod, pmId);

                var terminal = merchantTerminals.Where(x => x.PaymentMethodId == pmId && x.CurrencyID == curObj.ID).ToList();
                if (terminal.Count() == 0) return new ChargeResults(ValidationResult.Invalid_PaymentMethod, pmId);
                
                var customer = Bll.Customers.Customer.LoadObject(spm.Account_id) as Bll.Customers.Customer;
                if (customer != null && !customer.CanAcceptMerchantGroup(data.MerchantGroupID))
                    return new ChargeResults(ValidationResult.Invalid_AppIdentity, pmId); //wrong app identity
                var currenctBalace = Bll.Accounts.Balance.GetStatus(spm.Account_id, curObj.IsoCode).Expected;
                if ((currenctBalace - data.Amount) < 0) return new ChargeResults(ValidationResult.AmountAboveBalace, pmId); //Not sufficient funds
                var balance = Bll.Accounts.Balance.Create(spm.Account_id, curObj.IsoCode, -data.Amount, data.Text.NullIfEmpty().ValueIfNull("Process: " + data.RefNumber), Bll.Accounts.Balance.SOURCE_PURCHASE, data.RefNumber, false);
                return new ChargeResults(Infrastructure.ValidationResult.Success, pmId, null, balance.ID.ToString());
            } finally {
                Infrastructure.ObjectContext.Current.StopImpersonate();
            }
        }
    }
}
