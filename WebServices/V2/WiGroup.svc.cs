﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ComponentModel;
using System.Web;
using Netpay.Bll.ThirdParty;
using Netpay.Infrastructure.Security;
using System.Linq;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// base account services
    /// </summary>
    [System.ServiceModel.ServiceBehavior(AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
    [System.ServiceModel.ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class WiGroupWS : V2.BaseService
    {
        #region Class Declaration

        private string strWiGroupAPIURL_TokenManager = "http://rad2.wigroup.co:8080/wigroup-tokenmanager/vsp/tokens";
        private string strWiGroupAppID = "VSP_50180";
        string strWiGroupPassword = "c0ebc87dd3aa1d8c9e9ebfc8324bb7270faeca8c";

        #endregion

        #region Service Classes
        public class LoginOptions
        {
            public bool setCookie { get; set; }
            public string applicationToken { get; set; }
            public string appName { get; set; }
            public string deviceId { get; set; }
            public string pushToken { get; set; }
            public bool? excludeDataImages { get; set; }
            public UserRole? userRole { get; set; }
        }

        public class LoginResult : ServiceResult
        {
            public LoginResult() { }
            public LoginResult(Infrastructure.ValidationResult result) : base(result) { LastLogin = new DateTime(1970, 1, 1); }
            public string CredentialsToken { set; get; }
            public string CredentialsHeaderName { get; set; }
            public bool VersionUpdateRequired { get; set; }
            public DateTime LastLogin { set; get; }
            public bool IsFirstLogin { set; get; }

            public bool IsDeviceRegistrationRequired { get; set; }
            public bool IsDeviceActivated { set; get; }
            public bool IsDeviceRegistered { set; get; }
            public bool IsDeviceBlocked { set; get; }

            public string EncodedCookie { set; get; }
        }

        public class CookieResult : ServiceResult
        {
            internal CookieResult(Infrastructure.ValidationResult result) : base(result) { }
            public CookieResult() { }
            public string FullName { get; set; }
            public string Email { get; set; }
            public byte[] ProfilePicture { get; set; }
        }


        #endregion

        public WiGroupWS() : base(false, new UserRole[] { UserRole.Account }) { }

        #region Token Manager (We posting to them)

        [OperationContract(), WsdlDocumentation(""), ContextMethod(true, true), WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public WiGroupC.WiJSON.TokenResult GetToken(string maxTrxAmount, string tokenLifeInMin, string transactionType, string vspReference, int nCustomer_ID, string applicationToken)
        {
            if (string.IsNullOrEmpty(maxTrxAmount) || string.IsNullOrEmpty(tokenLifeInMin) || string.IsNullOrEmpty(transactionType) || string.IsNullOrEmpty(vspReference))
                throw new Exception("Parameters null");

            WiGroupC.WiJSON.Token pToken = new WiGroupC.WiJSON.Token()
            {
                maxTrxAmount = (Convert.ToDecimal(maxTrxAmount) * 100).ToString(),
                tokenLifeInMin = tokenLifeInMin,
                transactionType = transactionType,
                vspReference = vspReference
            };

            Guid pApplicationToken = new Guid(applicationToken);
            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(pApplicationToken);

            if (appIdentity == null || appIdentity == Netpay.Bll.ApplicationIdentity.Default)
                return new WiGroupC.WiJSON.TokenResult() { responseCode = "2", responseDesc = "Invalid applicationToken", token = null };

            Dal.DataAccess.NetpayDataContext dc = new Dal.DataAccess.NetpayDataContext(Infrastructure.Domain.Current.Sql1ConnectionString);

            System.Collections.Generic.List<Dal.Netpay.Customer> pCustomer = null;

            pCustomer = (from p in dc.Customers
                         where p.Customer_id == nCustomer_ID
                         select p).ToList();

            //pCustomer = Netpay.Bll.Customers.Customer.Search(new Bll.Customers.Customer.SearchFilters() { CustomersIdsList = new System.Collections.Generic.List<int>() { nCustomer_ID } }, null);

            if (pCustomer == null || pCustomer.Count != 1)
                return new WiGroupC.WiJSON.TokenResult() { responseCode = "3", responseDesc = "Could not find customer", token = null };

            var httpWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(strWiGroupAPIURL_TokenManager);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("apiId", strWiGroupAppID);
            httpWebRequest.Headers.Add("sha1Password", strWiGroupPassword);

            using (var streamWriter = new System.IO.StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string strJSONRequest = Newtonsoft.Json.JsonConvert.SerializeObject(pToken, Newtonsoft.Json.Formatting.Indented);

                streamWriter.Write(strJSONRequest);
                streamWriter.Flush();
            }

            var httpResponse = (System.Net.HttpWebResponse)httpWebRequest.GetResponse();
            string strResult = "";

            using (var streamReader = new System.IO.StreamReader(httpResponse.GetResponseStream()))
            {
                strResult = streamReader.ReadToEnd();
            }
            WiGroupC.WiJSON.TokenResult pTokenResult = Newtonsoft.Json.JsonConvert.DeserializeObject<WiGroupC.WiJSON.TokenResult>(strResult);

            /// TODO: Create coding to store this wiCode in the database against the Customer_ID
            /// 
            pTokenResult.responseDesc = pTokenResult.responseDesc + " - " + pCustomer[0].Customer_id;

            decimal fAmount = Convert.ToDecimal(pToken.maxTrxAmount) / 100.0m;

            if (transactionType == "PAYMENT"
                || transactionType == "DEPOSIT"
                || transactionType == "WITHDRAWAL")
            {
                Dal.Netpay.CustomerWiCode pWiCode = new Dal.Netpay.CustomerWiCode()
                {
                    Customer_id = nCustomer_ID,
                    DateIssued = DateTime.Now,
                    DateExpire = DateTime.Now.AddMinutes(Convert.ToDouble(pToken.tokenLifeInMin)),
                    WiCode = pTokenResult.token.wiCode.ToString(),
                    WiQR = pTokenResult.token.wiQR,
                    Amount = fAmount,
                    transactionType = transactionType,
                };

                dc.CustomerWiCodes.InsertOnSubmit(pWiCode);
                dc.SubmitChanges();


                return pTokenResult;
            }
            else
            {
                pTokenResult.responseCode = "4";
                pTokenResult.responseDesc = "Invalid transactionType.";

                return pTokenResult;
            }


        }

        #endregion

        #region Custom Methods On Yomane Side

        [OperationContract(), WsdlDocumentation(""), ContextMethod(true, true), WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public WiGroupC.WiJSON.GetCustomerIDResult GetCustomerIDFromPAN(string strPAN, string applicationToken)
        {
            if (string.IsNullOrEmpty(strPAN) || string.IsNullOrEmpty(applicationToken))
                throw new Exception("Parameters null");

            Guid pApplicationToken = new Guid(applicationToken);
            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(pApplicationToken);

            if (appIdentity == null || appIdentity == Netpay.Bll.ApplicationIdentity.Default)
                return new WiGroupC.WiJSON.GetCustomerIDResult() { responseCode = "2", responseDesc = "Invalid applicationToken", CustomerID = null };

            Dal.DataAccess.NetpayDataContext dc = new Dal.DataAccess.NetpayDataContext(Infrastructure.Domain.Current.Sql1ConnectionString);

            var pCustomerID = (from p in dc.AccountPaymentMethods
                               where p.Value1Encrypted == dc.GetEncrypted256(strPAN)
                               select p.data_Account.Customer_id).SingleOrDefault();

            //pCustomer = Netpay.Bll.Customers.Customer.Search(new Bll.Customers.Customer.SearchFilters() { CustomersIdsList = new System.Collections.Generic.List<int>() { nCustomer_ID } }, null);

            if (pCustomerID == null)
                return new WiGroupC.WiJSON.GetCustomerIDResult() { responseCode = "3", responseDesc = "Could not find customer", CustomerID = null };

            return new WiGroupC.WiJSON.GetCustomerIDResult() { responseCode = "-1", responseDesc = "Success", CustomerID = pCustomerID };
        }


        #endregion
        /*
                #region JSON Classes

                public class WiJSON
                {
                    #region Token

                    public class Token
                    {
                        public string maxTrxAmount { get; set; }
                        public string tokenLifeInMin { get; set; }
                        public string transactionType { get; set; }
                        public string vspReference { get; set; }
                    }
                    public class TokenResult
                    {
                        public Token token { get; set; }
                        public string responseCode { get; set; }
                        public string responseDesc { get; set; }

                        public class Token
                        {
                            public int wiCode { get; set; }
                            public string wiQR { get; set; }
                        }
                    }

                    #endregion

                    #region Transaction

                    public class Transaction
                    {
                        public int wiTrxId { get; set; }
                        public Token token { get; set; }
                        public string type { get; set; }
                        public Storetrxdetails storeTrxDetails { get; set; }
                        public int totalAmount { get; set; }
                        public int basketAmount { get; set; }
                        public Product[] products { get; set; }
                        public Vspcredentials vspCredentials { get; set; }

                        public class Token
                        {
                            public string id { get; set; }
                            public string type { get; set; }
                            public string vspReference { get; set; }
                        }

                        public class Storetrxdetails
                        {
                            public int storeId { get; set; }
                            public string remoteStoreId { get; set; }
                            public int retailerId { get; set; }
                            public string basketId { get; set; }
                            public string trxId { get; set; }
                            public string posId { get; set; }
                            public string cashierId { get; set; }
                        }

                        public class Vspcredentials
                        {
                            public string id { get; set; }
                            public string password { get; set; }
                        }

                        public class Product
                        {
                            public string id { get; set; }
                            public int units { get; set; }
                            public int pricePerUnit { get; set; }
                        }
                    }

                    public class TransactionResultSuccess
                    {
                        public Token token { get; set; }
                        public string type { get; set; }
                        public Storetrxdetails storeTrxDetails { get; set; }
                        public int wiTrxId { get; set; }
                        public int totalAmountProcessed { get; set; }
                        public int basketAmountProcessed { get; set; }
                        public int tipAmountProcessed { get; set; }
                        public int amountToSettle { get; set; }
                        public Vsp vsp { get; set; }
                        public Discount[] discount { get; set; }
                        public object[] loyalty { get; set; }
                        public string responseCode { get; set; }
                        public string responseDesc { get; set; }

                        public class Token
                        {
                            public string id { get; set; }
                            public string type { get; set; }
                        }

                        public class Storetrxdetails
                        {
                            public int storeId { get; set; }
                            public string basketId { get; set; }
                            public string trxId { get; set; }
                            public string posId { get; set; }
                            public string cashierId { get; set; }
                        }

                        public class Vsp
                        {
                            public int id { get; set; }
                            public string name { get; set; }
                            public string trxId { get; set; }
                            public string responseCode { get; set; }
                            public string responseDesc { get; set; }
                        }

                        public class Discount
                        {
                            public string name { get; set; }
                            public int amount { get; set; }
                            public object[] product { get; set; }
                        }

                    }

                    public class TransactionResultFail
                    {
                        public Token token { get; set; }
                        public string type { get; set; }
                        public Storetrxdetails storeTrxDetails { get; set; }
                        public int totalAmountProcessed { get; set; }
                        public int basketAmountProcessed { get; set; }
                        public string responseCode { get; set; }
                        public string responseDesc { get; set; }

                        public class Token
                        {
                            public string id { get; set; }
                            public string type { get; set; }
                        }

                        public class Storetrxdetails
                        {
                            public int storeId { get; set; }
                            public string basketId { get; set; }
                            public string trxId { get; set; }
                            public string posId { get; set; }
                            public string cashierId { get; set; }
                        }

                    }

                    #endregion

                    #region Advise

                    public class Advise
                    {
                        public string action { get; set; }
                        public Originaltrx originalTrx { get; set; }
                        public Vspcredentials vspCredentials { get; set; }

                        public class Originaltrx
                        {
                            public Storetrxdetails storeTrxDetails { get; set; }
                            public int wiTrxId { get; set; }
                            public string type { get; set; }
                            public int vspTrxId { get; set; }
                        }

                        public class Storetrxdetails
                        {
                            public int storeId { get; set; }
                            public string basketId { get; set; }
                            public string trxId { get; set; }
                            public string posId { get; set; }
                            public string cashierId { get; set; }
                        }

                        public class Vspcredentials
                        {
                            public string id { get; set; }
                            public string password { get; set; }
                        }

                    }

                    public class AdviseResponse
                    {
                        public string responseCode { get; set; }
                        public string responseDesc { get; set; }
                    }

                    #endregion
                }

                #endregion
        */

        #region Generic Classes

        public class MyMapper : System.ServiceModel.Channels.WebContentTypeMapper
        {
            public override System.ServiceModel.Channels.WebContentFormat GetMessageFormatForContentType(string contentType)
            {
                if (contentType.ToUpper().Contains("json".ToUpper()))
                {
                    return System.ServiceModel.Channels.WebContentFormat.Raw;
                }
                else
                {
                    return System.ServiceModel.Channels.WebContentFormat.Default;
                }
            }
        }

        #endregion
    }
}
