﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ComponentModel;
using System.Web;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// base account services
    /// </summary>
    [System.ServiceModel.ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class Account : V2.BaseService
    {
        #region Service Classes
        public class LoginOptions
        {
            public bool setCookie { get; set; }
            public string applicationToken { get; set; }
            public string appName { get; set; }
            public string deviceId { get; set; }
            public string pushToken { get; set; }
            public bool? excludeDataImages { get; set; }
            public UserRole? userRole { get; set; }
        }

        public class LoginResult : ServiceResult
        {
            public LoginResult() { }
            public LoginResult(ValidationResult result) : base(result) { LastLogin = new DateTime(1970, 1, 1); }
            public string CredentialsToken { set; get; }
            public string CredentialsHeaderName { get; set; }
            public bool VersionUpdateRequired { get; set; }
            public DateTime LastLogin { set; get; }
            public bool IsFirstLogin { set; get; }

            public bool IsDeviceRegistrationRequired { get; set; }
            public bool IsDeviceActivated { set; get; }
            public bool IsDeviceRegistered { set; get; }
            public bool IsDeviceBlocked { set; get; }
            
            public string EncodedCookie { set; get; }
        }

        public class CookieResult : ServiceResult
        {
            internal CookieResult(ValidationResult result) : base(result) { }
            public CookieResult() { }
            public string FullName { get; set; }
            public string Email { get; set; }
            public byte[] ProfilePicture { get; set; }
        }


        #endregion

        public Account() : base(true, new UserRole[] { UserRole.Account }) { }

        [WsdlDocumentation("The service will keep the session alive for the user and wont log him out automatically "), ContextMethod(true, false)]
        [OperationContract(), WebGet]
        public bool KeepAlive()
        {
            return (base.LoggedIn != null);
        }

        [OperationContract(), WsdlDocumentation("The login function is used for login in to the app – besides the ability to send the user name password combo it gives the ability to send more info during login for a more secure login ( like – app token, device id etc.)"), ContextMethod(true, true)]
        public LoginResult Login(string email, string userName, string password, LoginOptions options)
        {
            if (email == null || password == null)
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (email.Trim() == string.Empty || password.Trim() == string.Empty)
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (!email.IsEmail())
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (options.userRole == UserRole.Customer && string.IsNullOrEmpty(options.applicationToken))
                return new LoginResult(ValidationResult.Invalid_AppIdentity);

            Guid creds = Guid.Empty;
            Infrastructure.Security.LoginResult loginResult = Infrastructure.Security.LoginResult.UserNotFound;

            if (options.userRole == null || options.userRole == UserRole.Merchant || options.userRole == UserRole.Account)
            {
                loginResult = Infrastructure.Security.Login.DoLogin(new UserRole[] { UserRole.Account, UserRole.Merchant }, userName, email, password, options.deviceId, out creds);
            }
            else if (options.userRole == UserRole.Customer && !string.IsNullOrEmpty(options.applicationToken))
            {
                var identity = Netpay.Bll.ApplicationIdentity.GetIdentity(options.applicationToken.ToNullableGuid().GetValueOrDefault());
                if (identity == null) return new LoginResult(ValidationResult.Invalid_AppIdentity);
                loginResult = Infrastructure.Security.Login.DoLogin(new UserRole[] { UserRole.Customer }, userName, email, password, options.deviceId, out creds, appIdentityId: identity.ID);
            }

            //====================================================================
            // Message "Admin user account is locked"
            //====================================================================

            if (loginResult == Infrastructure.Security.LoginResult.AdminBlocked)
                return new LoginResult(ValidationResult.AdminUserLocked);

            //====================================================================
            // Message "Absence blocked reset password"
            //====================================================================

            if (loginResult == Infrastructure.Security.LoginResult.AbsenceBlocked)
                return new LoginResult(ValidationResult.AbsenceLocked);
            //====================================================================
            // Message "User is locked to many failed attempts"
            //====================================================================

            if (loginResult == Infrastructure.Security.LoginResult.FailedAttemptsBlocked)
                return new LoginResult(ValidationResult.FailedAttemptsLock);



            if (loginResult == Infrastructure.Security.LoginResult.Success)
            {
                ObjectContext.Current.CredentialsToken = creds;
                LoginResult result = new LoginResult(ValidationResult.Success);
                result.CredentialsToken = creds.ToString();
                Login user = Infrastructure.Security.Login.Get(creds);
                result.IsFirstLogin = user.IsFirstLogin;
                result.LastLogin = user.LastLogin.GetValueOrDefault();
                result.Number = Bll.Accounts.Account.Current.AccountNumber;

                Netpay.Bll.ApplicationIdentity identity = null;
                if (Bll.Customers.Customer.Current != null && Bll.Customers.Customer.Current.RefIdentityID != null)
                    identity = Netpay.Bll.ApplicationIdentity.Load(Bll.Customers.Customer.Current.RefIdentityID.Value);
                else if (options != null && !string.IsNullOrEmpty(options.applicationToken))
                    identity = Netpay.Bll.ApplicationIdentity.GetIdentity(options.applicationToken.ToNullableGuid().GetValueOrDefault());
                if (identity != null) user.Items["Session.ApplicationIdentity"] = identity;
                if (options != null) user.Items["excludeDataImages"] = options.excludeDataImages;
                result.EncodedCookie = EncodeLoginCookie(options != null ? options.applicationToken : null, result.Number, email);
                if (options != null && options.setCookie) Context.Response.Cookies.Add(new HttpCookie(Web.WebUtils.CredentialsTokenName, creds.ToString()));
                //result.IsDeviceRegistrationRequired = by app identity
                if (result.IsDeviceRegistrationRequired) {
                    var device = Bll.Accounts.MobileDevice.Current;
				    if (device != null) {
					    result.IsDeviceRegistered = true;
					    result.IsDeviceActivated = device.IsActivated;
					    result.IsDeviceBlocked = !device.IsActive;
				    }
                }
                result.CredentialsHeaderName = Web.WebUtils.CredentialsTokenName;
                return result;
            } else {
                if (options != null && options.setCookie) Context.Response.Cookies.Remove(Web.WebUtils.CredentialsTokenName);
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            }
        }

        [OperationContract(), WsdlDocumentation("The service will log the user out of the app.")]
        public void LogOff()
        {
            Infrastructure.Security.Login.Current.LogOff();
            if (Context.Response.Cookies[Web.WebUtils.CredentialsTokenName] != null)
                Context.Response.Cookies.Remove(Web.WebUtils.CredentialsTokenName);
        }

        [OperationContract, WsdlDocumentation("Using the old password, the user can change his password to a new one as long as it complies with the password roles.")]
        public ServiceResult UpdatePassword(string oldPassword, string newPassword)
        {
            var result = Infrastructure.Security.Login.Current.ChangePassword(oldPassword, newPassword, HttpContext.Current.Request.UserHostAddress);
            switch (result)
            {
                case Infrastructure.Security.Login.ChangePasswordResult.PasswordConfirmationMissmatch:
                case Infrastructure.Security.Login.ChangePasswordResult.WeakPassword:
                case Infrastructure.Security.Login.ChangePasswordResult.PasswordAlreadyExists:
                    return new ServiceResult(ValidationResult.PasswordWeak);
                case Infrastructure.Security.Login.ChangePasswordResult.PasswordUsedInThePast:
                    return new ServiceResult(ValidationResult.PasswordUsedInThePast);
                case Infrastructure.Security.Login.ChangePasswordResult.WrongCurrentPassword:
                    return new ServiceResult(ValidationResult.PasswordWrong);
                case Infrastructure.Security.Login.ChangePasswordResult.Success:
                    return new ServiceResult(ValidationResult.Success);
            }
            return new ServiceResult(ValidationResult.Invalid_Data);
        }

        [OperationContract, WsdlDocumentation("Using the user’s password, the user can change his pin to a new one.")]
        public ServiceResult UpdatePincode(string password, string newPincode)
        {
            return new ServiceResult(Bll.Accounts.Account.Current.UpdatePinCode(password, newPincode));
        }

        [OperationContract, WsdlDocumentation("It happens from time to time that the user forgets his password, the service will reset the password to a temporary password and will generate an email to the user’s email address with instructions on how to change the password."), ContextMethod(true, true)]
        public bool ResetPassword(string applicationToken, string email)
        {
            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());
            if (appIdentity == null) throw new Exception("App identity not found");

            string ip = HttpContext.Current.Request.UserHostAddress;
            bool result = Netpay.Bll.Customers.Customer.ResetPassword(email, appIdentity.ID, ip);
            return result;
        }

        private string EncodeLoginCookie(string applicationToken, string mumber, string email)
        {
            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());
            return string.Format("CustomerNumber={0}&Email={1}&Signature={2}", HttpUtility.UrlEncode(mumber), HttpUtility.UrlEncode(email), (mumber + email + (appIdentity != null ? appIdentity.ID : 0)).ToSha256());
        }

        [OperationContract, WsdlDocumentation("When using the login service to login the service will return an Encodedcookie – using the cookie and the token you will be able to create an auto login function in cases you don’t want the user to relogin every time he enters the app."), ContextMethod(true, true)]
        public CookieResult DecodeLoginCookie(string applicationToken, string cookie)
        {
            var cookieValues = System.Web.HttpUtility.ParseQueryString(cookie);
            byte[] image;
            var ret = new CookieResult(ValidationResult.Success);
            ret.Number = cookieValues["CustomerNumber"].NullIfEmpty();
            ret.Email = cookieValues["Email"].NullIfEmpty();
            if (ret.Number == null || ret.Email == null) return new CookieResult(ValidationResult.Invalid_Email);
            if (EncodeLoginCookie(applicationToken, ret.Number, ret.Email) != cookie) return new CookieResult(ValidationResult.Invalid_Data);
            ret.FullName = Netpay.Bll.Customers.Customer.GetFullName(ret.Number, ret.Email, out image);
            ret.ProfilePicture = image;
            return ret;
        }


        #region Phone Login

        [OperationContract, WsdlDocumentation("Register device service gives you the ability to register the user’s device and then to limit the user’s connectivity when needed. You will be able to control the devices from the Devices tab under merchants/ wallet")]
        [ContextMethod(false, false, true)]
        public ServiceResult RegisterDevice(string deviceId, string phoneNumber)
        {
            if (deviceId != null && !deviceId.IsImei())
                return new ServiceResult(ValidationResult.Invalid_ID);
            if (phoneNumber != null && !phoneNumber.IsPhone())
                return new ServiceResult(ValidationResult.Invalid_Phone);
            var appSettings = Netpay.Bll.Merchants.Mobile.MobileAppSettings.Load(Bll.Merchants.Merchant.Current.ID);
            if (appSettings == null || !appSettings.IsMobileAppEnabled)
                return new ServiceResult(ValidationResult.AdminUserLocked);

            Netpay.Bll.Accounts.MobileDevice device = null;
            var regResult = Netpay.Bll.Accounts.MobileDevice.RegisterMobileDevice(appSettings.MaxDeviceCount, deviceId, null, phoneNumber, Context.Request.Headers["User-Agent"], out device);
            if (regResult != MobileDeviceRegistrationResult.Success)
                return new ServiceResult(ValidationResult.Other) { Message = regResult.ToString() };

            var sendPasscodeResult = DeviceSendActivationCode();
            if (!sendPasscodeResult)
                return new ServiceResult(ValidationResult.Other) { Key = "sendCodeFailure" };

            return new ServiceResult(ValidationResult.Success);
        }

        [OperationContract, WsdlDocumentation("When we register a device to the system we can add an activation process where you use the service to send the device id as it was registered and then the service will return an activation code – the activation code will be sent via SMS to the user’s phone number as it was sent on registration.")]
        [ContextMethod(false, false, true)]
        public bool DeviceSendActivationCode()
        {
            return Netpay.Bll.Accounts.MobileDevice.Current.SendMobilePassCode();
        }

        [OperationContract, WsdlDocumentation("With the activation code and the device ID you can use the service to activate the device – at any time using the admin you can revoke access to the device when needed.")]
        [ContextMethod(false, false, true)]
        public bool DeviceActivate(string activationCode)
        {
            if (Netpay.Bll.Accounts.MobileDevice.Current.PassCode != activationCode)
                return false;
            return Netpay.Bll.Accounts.MobileDevice.Current.ActivateMobileDevice(Bll.Accounts.MobileDevice.Current.PassCode);
        }
        #endregion
    }
}
