﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ServiceModel;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// Summary description for PaymentMethods
    /// </summary>
    [System.ServiceModel.ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class PaymentMethods : V2.BaseService
    {
        #region service types
        public class PaymentMethod : ServiceListItem 
        {
            public string GroupKey { get; set; }
            public string Value1Caption { get; set; }
            public string Value1ValidationRegex { get; set; }
            public string Value2Caption { get; set; }
            public string Value2ValidationRegex { get; set; }
            public bool HasExpirationDate { get; set; }
            public override string Icon { get { return string.Format("{0}NPCommon/ImgPaymentMethod/118X70/{1}.png", Infrastructure.Domain.Get(WebUtils.DomainHost).WebServicesUrl, Key.ToNullableInt().GetValueOrDefault()); } set { } }
        }

        public class StoredPaymentMethod
        {
            public StoredPaymentMethod() { }

            public StoredPaymentMethod(Bll.Accounts.StoredPaymentMethod spm)
            {
                ID = spm.ID;
                Title = spm.Title;

                var method = spm.MethodInstance;
                PaymentMethodKey = ((int)method.PaymentMethodId).ToString();
                PaymentMethodGroupKey = ((int)method.PaymentMethodGroupId).ToString();
                OwnerName = spm.OwnerName;
                ExpirationDate = method.ExpirationDate;
                Last4Digits = method.Value1Last4;
                IssuerCountryIsoCode = spm.IssuerCountryIsoCode;
                Display = method.Display;
                IsDefault = spm.IsDefault;
                if (spm.BillingAddress != null) BillingAddress = new ServiceAddress(spm.BillingAddress);
                Icon = string.Format("{0}NPCommon/ImgPaymentMethod/118X70/{1}.png", WebUtils.CurrentDomain.WebServicesUrl, PaymentMethodKey);
            }

            public int ID { get; set; }
            public string Title { get; set; }
            public string PaymentMethodKey { get; set; }
            public string PaymentMethodGroupKey { get; set; }
            public string OwnerName { get; set; }
            public System.DateTime? ExpirationDate { get; set; }
            public string Last4Digits { get; set; }
            public string IssuerCountryIsoCode { get; set; }
            public bool IsDefault { get; set; }
            public string Icon { get; set; }
            public string Display { get; set; }
            public string AccountValue1 { get; set; }
            public string AccountValue2 { get; set; }
            public ServiceAddress BillingAddress { get; set; }

            #region Save And Load
            internal void Save(Bll.Accounts.StoredPaymentMethod value)
            {
                value.Title = Title;
                value.MethodInstance.PaymentMethodId = PaymentMethodKey.ToNullableEnum<CommonTypes.PaymentMethodEnum>().GetValueOrDefault();
                value.OwnerName = OwnerName;
                value.MethodInstance.ExpirationDate = ExpirationDate;
                value.IssuerCountryIsoCode = IssuerCountryIsoCode;
                value.IsDefault = IsDefault;
                if (ID == 0)
                    value.MethodInstance.SetEncryptedData(AccountValue1, AccountValue2);
            }
            #endregion
        }

        public class LinkMethodInfo
        {
            public LinkMethodInfo() { }
            public string AccountValue1 { get; set; }
            public string PersonalNumber { get; set; }
            public string PhoneNumber { get; set; }
            public DateTime? DateOfBirth { get; set; }
            //public string ProviderID { get; set; }
        }

        public class RequestPhysicalPaymentMethodInfo
        {
            public string ProviderID { get; set; }
            public ServiceAddress Address { get; set; }
        }

        public class LoadPaymentMethodInfo
        {
            public int PaymentMethodID { get; set; }
            public decimal Amount { get; set; }
            public string CurrencyIso { get; set; }
            public string PinCode { get; set; }
            public string ReferenceCode { get; set; }
        }

        public class StaticData
        {
            public List<ServiceListItem> PaymentMethodGroups { get; set; }
            public List<PaymentMethod> PaymentMethods { get; set; }
        }
        #endregion

        public PaymentMethods() : base(true, new Infrastructure.Security.UserRole[] { Infrastructure.Security.UserRole.Account }) { }
        
        [OperationContract]
        [WsdlDocumentation("The service will return all the data relevant to “Payment methods” such as : Payment method group(credit cards, wallets etc), Payment methods(not connected to a group) and their info")]
        public StaticData GetStaticData() 
        {
            var result = new StaticData();
            result.PaymentMethodGroups = Bll.PaymentMethods.Group.Search(null).Where(c => c.IsPopular).Select(c =>
                new ServiceListItem() { Key = c.ID.ToString(), Name = c.Name, Icon = string.Format("{0}NPCommon/ImgPaymentMethodGroups/118X70/{1}.png", WebUtils.CurrentDomain.WebServicesUrl, c.ID) }
            ).ToList();
            result.PaymentMethods = Bll.PaymentMethods.PaymentMethod.Search(null, null).Where(c => c.Type != PaymentMethodType.Unknown && c.Type != PaymentMethodType.System).Select(c =>
                new PaymentMethod()
                {
                    Key = c.ID.ToString(),
                    Name = c.Name,
                    GroupKey = ((int)c.Group).ToString(),
                    Value1Caption = c.Value1EncryptedCaption,
                    Value2Caption = c.Value2EncryptedCaption,
                    HasExpirationDate = c.IsExpirationDateMandatory,
                    Value1ValidationRegex = c.Value1EncryptedValidationRegex,
                    Value2ValidationRegex = c.Value2EncryptedValidationRegex
                }
            ).ToList();

            if(AppIdentity != null){
                result.PaymentMethods = result.PaymentMethods.Where(l => AppIdentity.SupportedPaymentMethods.Contains(int.Parse(l.Key))).ToList();
                result.PaymentMethodGroups = result.PaymentMethodGroups.Where(g => (from p in result.PaymentMethods select p.GroupKey).Contains(g.Key)).ToList();
            }
            return result;
        }

        #region Account Stored Methods
        [OperationContract]
        [WsdlDocumentation("The service will return all the billing addresses saved under the user.")]
        public List<ServiceAddress> GetBillingAddresses()
        {
            return Bll.Accounts.StoredPaymentMethod.BillingAddressesForAccount(Bll.Accounts.Account.Current.AccountID).Select(n => new ServiceAddress(n)).ToList();
        }
        
        [OperationContract]
        [WsdlDocumentation("This service will return all the info for a specific user – all of the user’s payment methods to display in list. (all details will be shown for each payment method)")]
        public List<StoredPaymentMethod> GetStoredPaymentMethods()
        {
            return Bll.Accounts.StoredPaymentMethod.LoadForAccount(Bll.Accounts.Account.Current.AccountID).Select(n => new StoredPaymentMethod(n)).ToList();
        }
        
        [OperationContract]
        [WsdlDocumentation("The service will return all the info about a specific payment method for a user – to search you must have the PaymentMethod ID (PMID)")]
        public StoredPaymentMethod GetStoredPaymentMethod(int pmid)
        {
            var paymentMethod = Bll.Accounts.StoredPaymentMethod.Load(pmid);
            if (paymentMethod == null)
                return null;
            if (paymentMethod.Account_id != Bll.Accounts.Account.Current.AccountID)
                return null;

            return new StoredPaymentMethod(paymentMethod);
        }

        
        [OperationContract]
        [WsdlDocumentation("This is the service to allow the user to save his payment method under his account for future usage. All account information must be provided according to the payment method type.")]
        public ServiceMultiResult StorePaymentMethod(StoredPaymentMethod methodData)
        {
            return StorePaymentMethods(new StoredPaymentMethod[] { methodData });
        }
        
        [OperationContract]
        [WsdlDocumentation("When updating a payment method some changes may effect other payment methods such as a choice of a default payment method. In such case one must save all of the payment methods effected by the change and then call all of the payment methods in order ot show the updated information.")]
        public ServiceMultiResult StorePaymentMethods(StoredPaymentMethod[] data)
        {
            var refNumbers = new List<string>();
            var pmList = new List<Netpay.Bll.Accounts.StoredPaymentMethod>();
            foreach (var methodData in data)
            {
                Bll.Accounts.StoredPaymentMethod dataObject;
                if (methodData.ID == 0)
                {
                    dataObject = new Bll.Accounts.StoredPaymentMethod();
                    dataObject.Account_id = Bll.Accounts.Account.Current.AccountID;
                }
                else
                    dataObject = Bll.Accounts.StoredPaymentMethod.Load(methodData.ID);
                if (methodData.BillingAddress != null)
                {
                    if (dataObject.BillingAddress == null)
                        dataObject.BillingAddress = new Bll.Accounts.AccountAddress();
                    methodData.BillingAddress.Save(dataObject.BillingAddress);
                }
                else
                    methodData.BillingAddress = null;
                methodData.Save(dataObject);
                var result = dataObject.Validate();
                if (result != ValidationResult.Success)
                    return new ServiceMultiResult(result);
                pmList.Add(dataObject);
            }

            foreach (var dataObject in pmList)
            {
                dataObject.Save();
                refNumbers.Add(dataObject.ID.ToString());
            }

            return (new ServiceMultiResult(ValidationResult.Success) { RefNumbers = refNumbers.ToArray() });
        }
        
        [OperationContract]
        [WsdlDocumentation("Each payment method has an ID, by sending the ID to this service the service will remove the payment method from the User’s account.")]
        public bool DeleteStoredPaymentMethod(int pmid)
        {
            var paymentMethod = Bll.Accounts.StoredPaymentMethod.Load(pmid);
            if (paymentMethod == null)
                return false;
            if (paymentMethod.Account_id != Bll.Accounts.Account.Current.AccountID)
                return false;
            
            paymentMethod.Delete();
            return true;
        }
        
        [OperationContract]
        [WsdlDocumentation("Link payment method is used to connect a “system” prepaid card – a form of an activation for a prepaid card.")]
        public ServiceResult LinkPaymentMethod(LinkMethodInfo data)
        {
            var foundCard = Bll.PaymentMethods.BulkStoredMethod.FindByPAN(data.AccountValue1, null).SingleOrDefault();
            if (foundCard == null) return new ServiceResult(ValidationResult.Item_Not_Exist);
            int comCount = 0;
            if (!string.IsNullOrEmpty(data.PersonalNumber)) comCount += (string.Compare(data.PersonalNumber.Trim(), foundCard.PersonalNumber.Trim(), true) == 0) ? 1 : 0;
            if (!string.IsNullOrEmpty(data.PhoneNumber)) comCount += (string.Compare(data.PhoneNumber.Trim(), foundCard.PhoneNumber.Trim(), true) == 0) ? 1 : 0;
            if (data.DateOfBirth.HasValue && foundCard.DateOfBirth.HasValue) comCount += (data.DateOfBirth.GetValueOrDefault().Date == foundCard.DateOfBirth.GetValueOrDefault().Date) ? 1 : 0;
            if (comCount < 1) return new ServiceResult(ValidationResult.Invalid_Phone);
            foundCard.AttachToAccount(Bll.Accounts.Account.Current.AccountID);
            return new ServiceResult(ValidationResult.Success);
        }

        #endregion

        #region Card with provider

        
        [OperationContract]
        [WsdlDocumentation("This service call is used when connected to a 3rd party provider of prepaid MasterCards. The service is used to order the physical card from the provider – user must send his address for the delivery to take place and to specify the “provider” id – the provider is the admin owner.")]
        public ServiceResult RequestPhysicalPaymentMethod(RequestPhysicalPaymentMethodInfo data)
        {
            Bll.Accounts.IStoredPaymentMethodProvider provider;
            if (!Bll.Accounts.StoredPaymentMethod.Providers.TryGetValue(data.ProviderID, out provider))
                return new ServiceResult(ValidationResult.Invalid_PaymentMethod);
            if (!provider.SupportCreate) return new ServiceResult(ValidationResult.Invalid_PaymentMethod);
            try {
                var accountAddress = new Bll.Accounts.AccountAddress(); data.Address.Save(accountAddress);
                var ret = provider.Create(Bll.Customers.Customer.Current, accountAddress, new System.Collections.Specialized.NameValueCollection());
                return new ServiceResult(ValidationResult.Success, null, ret.ID.ToString());
            } catch (Exception ex) {
                return new ServiceResult(ValidationResult.Invalid_ServiceUrl, ex.Message);
            }
        }
        
        [OperationContract]
        [WsdlDocumentation("This service call is used when connected to a 3rd party provider of prepaid MasterCards. The service will allow allocating money from the user’s balance or credit card to the prepaid card.")]
        public ServiceResult LoadPaymentMethod(LoadPaymentMethodInfo data)
        {
            if (!Bll.Customers.Customer.ValidatePinCode(Bll.Accounts.Account.Current.AccountID, data.PinCode))
                return new ServiceResult(ValidationResult.PinCodeWrong);
            var pm = Bll.Accounts.StoredPaymentMethod.Load(data.PaymentMethodID);
            Bll.Accounts.IStoredPaymentMethodProvider provider;
            if (!Bll.Accounts.StoredPaymentMethod.Providers.TryGetValue(pm.ProviderID, out provider))
                return new ServiceResult(ValidationResult.Invalid_PaymentMethod);
            if (!provider.SupportLoad) return new ServiceResult(ValidationResult.Invalid_PaymentMethod);
            try {
                var ret = provider.Load(pm, data.Amount, Bll.Currency.Get(data.CurrencyIso).EnumValue, data.ReferenceCode);
                return new ServiceResult(ValidationResult.Success, null, ret.ID.ToString());
            } catch (Exception ex) {
                return new ServiceResult(ValidationResult.Invalid_ServiceUrl, ex.Message);
            }
        }

        #endregion

    }
}
