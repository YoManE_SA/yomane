﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ServiceModel;
using Netpay.CommonTypes;
using Netpay.Infrastructure;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// Summary description for Transactions
    /// </summary>
    [ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class Transactions : V2.BaseService
    {
        #region Service Types
        public class LookupResult
        {
            public int TransID { get; set; }
            public DateTime TransDate { get; set; }
            public string MethodString { get; set; }
            public string MerchantName { get; set; }
            public string MerchantWebSite { get; set; }
            public string MerchantSupportPhone { get; set; }
            public string MerchantSupportEmail { get; set; }
            public LookupResult() { }
            #region Load and Save
            internal LookupResult(Bll.Transactions.Transaction.TransactionLookupResult data)
            {
                TransID = data.TransID;
                TransDate = data.TransDate;
                MethodString = data.MethodString;
                MerchantName = data.MerchantName;
                MerchantWebSite = data.MerchantWebSite;
                MerchantSupportPhone = data.MerchantSupportPhone;
                MerchantSupportEmail = data.MerchantSupportEmail;
            }
            #endregion
        }

        public class SearchFilters 
        {
            public int? IDFrom { get; set; }
            public int? IDTo { get; set; }
            public DateTime? DateFrom { get; set; }
            public DateTime? DateTo { get; set; }
            public decimal? AmountFrom { get; set; }
            public decimal? AmountTo { get; set; }

            public string CurrencyIso { get; set; }

            public SearchFilters() { }
            #region Load and Save
            internal Bll.Transactions.Transaction.SearchFilters ToTransactionFilters()
            {
                return new Bll.Transactions.Transaction.SearchFilters()
                {
                    ID = new Range<int?>(IDFrom, IDTo),
                    date = new Range<DateTime?>(DateFrom, DateTo),
                    amount = new Range<decimal?>(AmountFrom, AmountTo),
                    currencyID = (!string.IsNullOrEmpty(CurrencyIso) ? (int?)Bll.Currency.Get(CurrencyIso).ID : null)
                };
            }
            #endregion
        }

        public enum TransactionStatus { Captured = 1, Declined = 2, Approval = 3, Pending = 4 }
        public class LoadOptions
        { 
            public TransactionStatus TransType { get; set; }
            public bool LoadPayment { get; set; }
            public bool LoadPayer { get; set; }
            public bool LoadMerchant { get; set; }

            #region Load and Save
            public Bll.Transactions.Transaction.LoadOptions ToTransactionOptions()
            {
                return new Bll.Transactions.Transaction.LoadOptions()
                {
                    dataStores = new List<Infrastructure.TransactionStatus> { (Infrastructure.TransactionStatus)TransType },
                    loadPayerData = LoadPayer,
                    loadPaymentData = LoadPayment,
                };
            }
            #endregion
        } 

        public class Payment
        {
            public string Type { get; set; }
            public string Bin { get; set; }
            public string BinCountry { get; set; }
            public int ExpirationMonth { get; set; }
            public int ExpirationYear { get; set; }
            public string Last4 { get; set; }
            public ServiceAddress BillingAddress { get; set; }
            public Payment() { }
            #region Load and Save
            internal Payment(Bll.Transactions.Payment data) 
            {
                if (data == null || data.MethodInstance == null) return;

                Type = data.MethodInstance.PaymentMethod.Name;
                ExpirationMonth = data.MethodInstance.ExpirationMonth;
                ExpirationYear = data.MethodInstance.ExpirationYear;
                Last4 = data.MethodInstance.Value1Last4;
                BinCountry = data.IssuerCountryIsoCode;
                var storedMethod = data as Bll.PaymentMethods.IStoredMethod;
                if (storedMethod != null)
                    Bin = storedMethod.Value1First6Data;
            }
            #endregion
        }

        public class Payer 
        {
            public string Phone { get; set; }
            public string Email { get; set; }
            public string FullName { get; set; }
            public ServiceAddress ShippingAddress { get; set; }
            public Payer() { }
            #region Load and Save
            internal Payer(Bll.Transactions.Payer data) 
            {
                Phone = data.PersonalNumber;
                Email = data.EmailAddress;
                FullName = data.FullName;
                ShippingAddress = new ServiceAddress(data.ShippingDetails);
            }
            #endregion        
        }

        public class Transaction
        {
            internal Transaction() { }
            internal Transaction(Bll.Transactions.Transaction vo, Bll.Merchants.MerchantPublicInfo merchant, bool loadMinimal)
            {
                ID = vo.ID;
                var paymentMethod = Bll.PaymentMethods.PaymentMethod.Get(vo.PaymentMethodID);
                if (paymentMethod != null)
                {
                    PaymentMethodKey = paymentMethod.ID.ToString();
                    PaymentMethodGroupKey = ((int)paymentMethod.Group).ToString();
                }
                AuthCode = vo.ApprovalNumber;
                Amount = vo.Amount;
                PaymentDisplay = vo.PaymentMethodDisplay;
                InsertDate = vo.InsertDate;
                CurrencyIso = vo.CurrencyIsoCode;
                Comment = vo.Comment;
                Text = vo.PayForText;
                Merchant = new Shop.Merchant(merchant);
                if (loadMinimal) return;
                if (vo.PayerData != null) PayerData = new Payer(vo.PayerData);
                if (vo.PaymentData != null) PaymentData = new Payment(vo.PaymentData);

                if (vo.ProductId != null)
                {
                    var co = Bll.Shop.Products.Product.Load(vo.ProductId.Value);
                    if (co != null)
                    {
                        var text = co.GetTextForLanguage(null);
                        if (text != null)
                        {
                            ReceiptText = text.ReceiptText;
                            ReceiptLink = text.ReceiptLink;
                        }
                    }
                }
            }
            public int ID { get; set; }
            public string PaymentMethodKey { get; set; }
            public string PaymentMethodGroupKey { get; set; }
            public string AuthCode { get; set; }
            public string PaymentDisplay { get; set; }
            public DateTime InsertDate { get; set; }

            public int Installments { get; set; }
            public decimal Amount { get; set; }
            public string CurrencyIso { get; set; }

            public string Comment { get; set; }
            public string Text { get; set; }
            public bool IsRefunded { get; set; }
            public bool IsManual { get; set; }

            public string ReceiptText { get; set; }
            public string ReceiptLink { get; set; }

            public Shop.Merchant Merchant { get; set; }

            public Payer PayerData { get; set; }
            public Payment PaymentData { get; set; }
        }


        public class ProcessData
        {
            public string CustomerNumber { get; set; }
            public string PinCode { get; set; }
            public int? ShippingAddressId { get; set; }
            public Payer PayerData { get; set; }

            public int? StoredPaymentMethodId { get; set; }
            public string AccountValue1 { get; set; }
            public string AccountValue2 { get; set; }
            public DateTime AccountExpDate { get; set; }
            public ServiceAddress BillingAddress { get; set; }

            public string MerchantNumber { get; set; }
            public string PreAuthCode { get; set; }
            public string OrderId { get; set; }
            public int Installments { get; set; }
            public decimal Amount { get; set; }
            public string CurrencyIso { get; set; }
            public string Comment { get; set; }
            public string Text { get; set; }

            public string ShopCartCookie { get; set; }
            public int? ProductId { get; set; }
            public int? Quantity { get; set; }

            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        public class ProcessResult : ServiceResult
        {
            internal ProcessResult(int code, string message) { Code = code; Message = message; IsSuccess = (Code == 0); }
            public ProcessResult() { }
            //public string Signature { get; set; }
        }

        #endregion

        public Transactions(): base(true) { }

        [OperationContract, ContextMethod(true, true)]
        public List<LookupResult> Lookup(DateTime transDate, decimal amount, string last4cc)
        {
            return Bll.Transactions.Transaction.TransLookup(transDate, amount, last4cc).Select(v => new LookupResult(v)).ToList();
        }
        
        [OperationContract]
        [WsdlDocumentation("Search service gives you wider filter options when searching for transaction – not like lookup service, this service will allow to search ranges of the transaction ids or dates and sort by merchant details. The response is much more detailed and will include all available information about the transaction or the accounts involved with it.")]
        public List<Transaction> Search(SearchFilters filters, LoadOptions loadOptions, SortAndPage sortAndPage)
        {
            var lo = loadOptions.ToTransactionOptions();
            lo.sortAndPage = sortAndPage.ToSortAndPage("InsertDate", true);
            var transactions = Bll.Transactions.Transaction.Search(lo, filters.ToTransactionFilters());
            var merchants = Bll.Merchants.MerchantPublicInfo.Load(transactions.Select(t => t.MerchantID.GetValueOrDefault()).Distinct().ToList(), null);
            return transactions.Select(t => new Transaction(t, merchants.Where(m => m.ID == t.MerchantID).SingleOrDefault(), true)).ToList();
        }
        
        [OperationContract]
        [WsdlDocumentation("The gettransaction service will return all of the information about a specific transaction using the transaction id. Beside the details of the charge itself the service will return all of the payer information that was included in the transaction and all supporting elements such as Receipt text and Receipt link.")]
        public Transaction Get(int transactionId)
        {
            var vo = Bll.Transactions.Transaction.GetTransaction(null, transactionId, Infrastructure.TransactionStatus.Captured);
            return new Transaction(vo, Bll.Merchants.MerchantPublicInfo.Load(vo.MerchantID.GetValueOrDefault()), false);
        }
        
        [OperationContract]
        [WsdlDocumentation("The Process service is used to process the transactions from the applications ( web or mobile ) – all transaction information should be sent as part of the request to avoid declines. The response will include the transaction id and the response text – in cases of declines it is recommended to show the user the explanation of why the transaction got declined.")]
        public ProcessResult Process(ProcessData data)
        {
            var payerInfo = Bll.Customers.Customer.Current;
            if (!Bll.Customers.Customer.ValidatePinCode(payerInfo.ID, data.PinCode))
                return new ProcessResult(500, "wrong Pincode was entered");
            Bll.Shop.Cart.Basket basket = null;
            var merchant = Bll.Merchants.Merchant.Load(data.MerchantNumber);
            //Netpay.Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(data.MerchantNumber, out merchantId);
            if (data.ShopCartCookie != null)
            {
                basket = Shop.LoadBasketFromCookie(null, data.ShopCartCookie);
            }
            else if (data.ProductId != null)
            {
                var product = new Bll.Shop.Cart.BasketItem(merchant.ID, data.ProductId.Value, null, null);
                if (product == null) return new ProcessResult(500, "Product does not exist");
                basket = new Bll.Shop.Cart.Basket(merchant.ID, data.OrderId, LoggedIn.LoginID);
                basket.CurrencyISOCode = product.CurrencyISOCode;
                basket.AddItem(product);
            }
            if (basket != null)
            {
                //if (data.Amount == 0) 
                data.Amount = basket.Total;
                //if (string.IsNullOrEmpty(data.CurrencyIso)) 
                data.CurrencyIso = basket.CurrencyISOCode.ToString();
                if (string.IsNullOrEmpty(data.Text))
                {
                    if (basket.BasketItems.Count == 0) data.Text = string.Format("{0} x {1}", basket.BasketItems[0].Quantity, basket.BasketItems[0].Name);
                    else data.Text = "Cart #" + basket.ID;
                }
            }
            if (!string.IsNullOrEmpty(data.OrderId)) data.OrderId = "SQR-ID:" + data.OrderId.ToString();
            //string orderId = "SQR-ID:" + transQrId.ToString();

            var pMethod = Netpay.Bll.Accounts.StoredPaymentMethod.Load(data.StoredPaymentMethodId.Value);
            if (pMethod == null) return new ProcessResult(500, "Card does not exist");

            var trans = new Bll.Transactions.Transaction();
            trans.Merchant = merchant;
            trans.CustomerID = Bll.Customers.Customer.Current.ID;
            trans.CurrencyIsoCode = data.CurrencyIso;
            trans.Amount = data.Amount;
            trans.Installments = (byte)data.Installments;
            trans.CreditType = CreditType.Regular;

            trans.PayForText = data.Text;
            trans.IP = (OperationContext.Current.IncomingMessageProperties[System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name] as System.ServiceModel.Channels.RemoteEndpointMessageProperty).Address; //  HttpContext.Current.Request.UserHostAddress;
            trans.OrderNumber = data.OrderId;
            trans.TransactionSource = TransactionSource.WalletMobileApp;
            if (data.ProductId != null) trans.ProductId = data.ProductId;
            //data.Comment = data.Comment;

            Bll.PersonalInfo.Copy(payerInfo, trans.PayerData);
            pMethod.MethodInstance.CopyTo(trans.PaymentData.MethodInstance);

            Bll.Accounts.AccountAddress address = pMethod.BillingAddress;
            if (address == null) address = payerInfo.PersonalAddress;
            if (address != null)
            {
                if (trans.PaymentData.BillingAddress == null) trans.PaymentData.BillingAddress = new Bll.Transactions.BillingAddress();
                Bll.Address.Copy(address, trans.PaymentData.BillingAddress);
            }
            if (data.ShippingAddressId != null)
            {
                var shAddress = Bll.Customers.ShippingAddress.Load(data.ShippingAddressId.Value);
                if (trans.PayerData.ShippingDetails == null) trans.PayerData.ShippingDetails = new Bll.Transactions.ShippingDetails();
                Bll.Address.Copy(address, trans.PayerData.ShippingDetails);
            }
            var reqtType = trans.PaymentData.MethodInstance.PaymentMethodGroupId;
            System.Collections.Specialized.NameValueCollection retParams = null;
            var reqParams = trans.GetProcessParamters();
            if (basket != null) reqParams.Add("CartId", basket.Identifier.ToString());
            try
            {
                retParams = Bll.Transactions.Transaction.ProcessTransaction(reqtType, reqParams);
            }
            catch (Exception ex)
            {
                return new ProcessResult(500, ex.Message);
            }
            ProcessResult result = null;
            if (reqtType == PaymentMethodGroupEnum.CreditCard)
            {
                result = new ProcessResult(retParams.Get("Reply").ToInt32(-1), retParams.Get("ReplyDesc")) { Number = retParams.Get("TransID") };
            }
            else if (reqtType == PaymentMethodGroupEnum.DirectDebit || reqtType == PaymentMethodGroupEnum.OnlineBankTransfer || reqtType == PaymentMethodGroupEnum.InstantOnlinkBankTransfer || reqtType == PaymentMethodGroupEnum.EuropeanDirectDebit)
            {
                result = new ProcessResult(retParams.Get("replyCode").ToInt32(-1), retParams.Get("replyMessage")) { Number = retParams.Get("transId") };
            }
            else result = new ProcessResult(500, "Unexpected failure");
            return result;
        }

    }
}
