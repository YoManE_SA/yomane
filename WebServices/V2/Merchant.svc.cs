﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Bll;
using System.ComponentModel;
using System.ServiceModel;
using Netpay.Infrastructure;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// Merchants services
    /// </summary>
    [System.ServiceModel.ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class Merchant : V2.BaseService
    {
        #region Service Classes

        public class RegistrationData
        {
            public string DbaName { get; set; }
            public string LegalBusinessName { get; set; }
            public string LegalBusinessNumber { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zipcode { get; set; }
            public DateTime? OwnerDob { get; set; }
            public string OwnerSsn { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Url { get; set; }
            public string PhisicalAddress { get; set; }
            public string PhisicalCity { get; set; }
            public string PhisicalState { get; set; }
            public string PhisicalZip { get; set; }
            public string StateOfIncorporation { get; set; }
            public int? TypeOfBusiness { get; set; }
            public DateTime? BusinessStartDate { get; set; }
            public int? Industry { get; set; }
            public string BusinessDescription { get; set; }
            public decimal? AnticipatedMonthlyVolume { get; set; }
            public decimal? AnticipatedAverageTransactionAmount { get; set; }
            public decimal? AnticipatedLargestTransactionAmount { get; set; }
            public byte[] CanceledCheckImage { get; set; }
            public string BankAccountNumber { get; set; }
            public string BankRoutingNumber { get; set; }
            public int PercentDelivery0to7 { get; set; }
            public int PercentDelivery8to14 { get; set; }
            public int PercentDelivery15to30 { get; set; }
            public int PercentDeliveryOver30 { get; set; }
        }

	    public class CardStorageItem
        {
            public enum StorageAction { Unknown, Delete, Verify, Create }
            public enum StorageResult { Unknown, Success, InvalidCard, CardExpired, CardNotFound }
            public StorageAction Action { get; set; }
            public string CardNumber { get; set; }
            public string ExpirationMonth { get; set; }
            public string ExpirationYear { get; set; }
            public string CardholderFullName { get; set; }
            public string Comment { get; set; }
            public StorageResult Result { get; set; }
        }

        public class BlockItem
        {
            public enum BlockValueType { CreditCard, Email, FullName, PhoneNumber, PersonalID, Bin, BinCountry, /*IpAddress*/ }
            public enum BlockAction { Add, Remove, Check, }
            public class BlockItemValue
            {
                [System.Xml.Serialization.XmlAttribute("Key")]
                public string Key;
                [System.Xml.Serialization.XmlAttribute("Value")]
                public string Value;
            }
            [System.Xml.Serialization.XmlAttribute("Type")]
            public BlockValueType Type;
            [System.Xml.Serialization.XmlAttribute("Action")]
            public BlockAction Action;
            public string Comment;
            public List<BlockItemValue> Values;
        }

        public class BlockItemResult : ServiceResult
        {
            public class ResultItem
            {
                [System.Xml.Serialization.XmlAttribute("key")]
                public string key;
                [System.Xml.Serialization.XmlAttribute("Exist")]
                public bool Exist;
                //[System.Xml.Serialization.XmlAttribute("Insert")]
                public DateTime? Insert;
            }
            public List<ResultItem> Items { get; set; }
            public BlockItemResult() : base(ValidationResult.Success) 
            {
                Items = new List<BlockItemResult.ResultItem>();
            }
        }

        #endregion

        public Merchant() : base(true, new Infrastructure.Security.UserRole[] { Infrastructure.Security.UserRole.Merchant }) { }

        [OperationContract, ContextMethod(true, true)]
        [WsdlDocumentation("This service is used to send an XML to the admin system that includes all the necessary information a merchant needs to provide when signing up to the service.  Once the XML is sent to the system the operator can access it using the registration screen under merchant and decide the next steps(register a merchant in the system or send it to a 3rd party system )")]
        public void Register(RegistrationData RegistrationData)
        {

            Netpay.Bll.Merchants.Registration registration = new Netpay.Bll.Merchants.Registration();
            registration.DbaName = RegistrationData.DbaName;
            registration.LegalBusinessName = RegistrationData.LegalBusinessName;
            registration.FirstName = RegistrationData.FirstName;
            registration.LastName = RegistrationData.LastName;
            registration.Email = RegistrationData.Email;
            registration.Address = RegistrationData.Address;
            registration.City = RegistrationData.City;
            registration.State = RegistrationData.State;
            registration.Zipcode = RegistrationData.Zipcode;
            registration.OwnerDob = RegistrationData.OwnerDob;
            registration.OwnerSsn = RegistrationData.OwnerSsn;
            registration.Phone = RegistrationData.Phone;
            registration.Fax = RegistrationData.Fax;
            registration.Url = RegistrationData.Url;
            registration.PhisicalAddress = RegistrationData.PhisicalAddress;
            registration.PhisicalCity = RegistrationData.PhisicalCity;
            registration.PhisicalState = RegistrationData.PhisicalState;
            registration.PhisicalZip = RegistrationData.PhisicalZip;
            registration.StateOfIncorporation = RegistrationData.StateOfIncorporation;
            registration.TypeOfBusiness = RegistrationData.TypeOfBusiness;
            registration.BusinessStartDate = RegistrationData.BusinessStartDate;
            registration.Industry = RegistrationData.Industry;
            registration.BusinessDescription = RegistrationData.BusinessDescription;
            registration.AnticipatedMonthlyVolume = RegistrationData.AnticipatedMonthlyVolume;
            registration.AnticipatedAverageTransactionAmount = RegistrationData.AnticipatedAverageTransactionAmount;
            registration.AnticipatedLargestTransactionAmount = RegistrationData.AnticipatedLargestTransactionAmount;
            registration.CanceledCheckImageContent = System.Convert.ToBase64String(RegistrationData.CanceledCheckImage);
            registration.CanceledCheckImageFileName = "Register.png";
            registration.CanceledCheckImageMimeType = "image/png";
            registration.BankAccountNumber = RegistrationData.BankAccountNumber;
            registration.BankRoutingNumber = RegistrationData.BankRoutingNumber;
            registration.PercentDelivery0to7 = RegistrationData.PercentDelivery0to7;
            registration.PercentDelivery8to14 = RegistrationData.PercentDelivery8to14;
            registration.PercentDelivery15to30 = RegistrationData.PercentDelivery15to30;
            registration.PercentDeliveryOver30 = RegistrationData.PercentDeliveryOver30;

            registration.Save();
        }
        
        [OperationContract]
        [WsdlDocumentation("Manage merchant stored cards tokens for future use")]
        public CardStorageItem[] StoreCards(CardStorageItem[] items)
        {
			var merchant = Bll.Merchants.Merchant.Current;
            var createItems = items.Where(i => i.Action == CardStorageItem.StorageAction.Create);
            foreach (var item in createItems)
            {
                var vo = new Bll.Merchants.MethodStorage(Bll.Merchants.Merchant.Current.ID);
				vo.MethodInstance.SetEncryptedData(item.CardNumber, null);
				vo.MethodInstance.SetExpirationMonth(item.ExpirationYear.ToNullableInt().GetValueOrDefault(1900), item.ExpirationMonth.ToNullableInt().GetValueOrDefault(1900));
                vo.Owner_FullName = item.CardholderFullName;
                vo.Comment = item.Comment;
				vo.Save();
                item.Result = CardStorageItem.StorageResult.Success;
            }

            var deleteItems = items.Where(i => i.Action == CardStorageItem.StorageAction.Delete);
            foreach (var item in deleteItems)
            {
				var dItem = Bll.Merchants.MethodStorage.Search(new Bll.Merchants.MethodStorage.SearchFilters() { MerchantID = merchant.ID, Value1 = item.CardNumber }, null).SingleOrDefault();
				if (dItem != null) {
					dItem.IsActive = false;
					dItem.Save();
                    item.Result = CardStorageItem.StorageResult.Success;
                } else item.Result = CardStorageItem.StorageResult.CardNotFound;
            }

            var verifyItems = items.Where(i => i.Action == CardStorageItem.StorageAction.Verify);
            foreach (var item in verifyItems)
            {
                var dItem = Bll.Merchants.MethodStorage.Search(new Bll.Merchants.MethodStorage.SearchFilters() { MerchantID = merchant.ID, Value1 = item.CardNumber }, null).SingleOrDefault();
                item.Result = (dItem != null ? CardStorageItem.StorageResult.Success : CardStorageItem.StorageResult.CardNotFound);
			}

            // save log
            var builder = new System.Text.StringBuilder();
            foreach (CardStorageItem item in items) 
            {
                builder.AppendLine("action: " + item.Action);
                builder.AppendLine("cardholder name: " + item.CardholderFullName);
                builder.AppendLine("card number: " + (item.CardNumber.Length > 4 ? item.CardNumber.Substring(item.CardNumber.Length - 4) : item.CardNumber));
                builder.AppendLine("comment: " + item.Comment);
                builder.AppendLine("exp. month: " + item.ExpirationMonth);
                builder.AppendLine("exp. year: " + item.ExpirationYear);
                builder.AppendLine("result: " + item.Result);
                builder.AppendLine();
            }
            Netpay.Bll.Process.GenericLog.Log(HistoryLogType.RemoteCardStorageV2, merchant.ID, builder.ToString(), null);

            return items.ToArray();
        }

        private Bll.Risk.RiskItem.RiskValueType MapValueType(BlockItem.BlockValueType valueType)
        {
            switch (valueType)
            {
                case BlockItem.BlockValueType.CreditCard: return Bll.Risk.RiskItem.RiskValueType.AccountValue1;
                case BlockItem.BlockValueType.Email: return Bll.Risk.RiskItem.RiskValueType.Email;
                case BlockItem.BlockValueType.FullName: return Bll.Risk.RiskItem.RiskValueType.FullName;
                case BlockItem.BlockValueType.PhoneNumber: return Bll.Risk.RiskItem.RiskValueType.Phone;
                case BlockItem.BlockValueType.PersonalID: return Bll.Risk.RiskItem.RiskValueType.PersonalNumber;
                case BlockItem.BlockValueType.Bin: return Bll.Risk.RiskItem.RiskValueType.Bin;
                case BlockItem.BlockValueType.BinCountry: return Bll.Risk.RiskItem.RiskValueType.BinCountry;
            }
            return Bll.Risk.RiskItem.RiskValueType.AccountValue1;
        }
        
        [OperationContract]
        [WsdlDocumentation("The service enables to “add, remove or check” for blocked items such as credit cards, emails or names. This service is meant to assist the risk manager to give merchant a fast customer service when not present in the office. This is a limited service only for internal use and not to be published for outside developers Requires a signature to be sent with every request.")]
        public BlockItemResult ManageBlocks(List<BlockItem> items)
        {
            var merchant = Bll.Merchants.Merchant.Current;
            var result = new BlockItemResult() { };
            foreach (var it in items)
            {
                var values = it.Values.Select(i => new Bll.Risk.RiskItem.RiskItemValue() { ValueType = MapValueType(it.Type), Value = i.Value }).ToList();
                BlockItemResult ir = new BlockItemResult();
                DateTime? dtNotFound = null;
                var searchResult = Netpay.Bll.Risk.RiskItem.Search(true, Bll.Risk.RiskItem.RiskSource.Merchant, values);
                var res = (from v in it.Values join t in searchResult on v.Value equals t.Value select new BlockItemResult.ResultItem() { Exist = true, key = v.Key, Insert = t.InsertDate }).ToList();
                switch (it.Action)
                {
                    case BlockItem.BlockAction.Add:
                        Netpay.Bll.Risk.RiskItem.Create(Bll.Risk.RiskItem.RiskSource.Merchant, Bll.Risk.RiskItem.RiskListType.Black, values, null, null, null, null, null, null, null, it.Comment, null);
                        dtNotFound = DateTime.Now;
                        break;
                    case BlockItem.BlockAction.Remove:
                        Netpay.Bll.Risk.RiskItem.Delete(MapValueType(it.Type), Bll.Risk.RiskItem.RiskListType.Black, searchResult.Select(t => t.ID).ToList());
                        break;
                    case BlockItem.BlockAction.Check:
                        break;
                }
                result.Items.AddRange(res);
                result.Items.AddRange(it.Values.Where(v => !searchResult.Select(t => t.Value).Contains(v.Value)).Select(v => new BlockItemResult.ResultItem() { Exist = false, key = v.Key, Insert = dtNotFound }));
            }

            // save log
            var request = new System.Text.StringBuilder();
            foreach (var item in items)
            {
                request.AppendLine("action: " + item.Action);
                request.AppendLine("type: " + item.Type);
                request.AppendLine("comment: " + item.Comment);
                foreach (BlockItem.BlockItemValue value in item.Values)
                {
                    string val = value.Value;
                    if (item.Type == BlockItem.BlockValueType.CreditCard)
                        val = (val.Length > 4 ? val.Substring(val.Length - 4) : value.Value);
                    request.AppendLine("key: " + value.Key + " value: " + val);
                }

                request.AppendLine();
            }

            var response = new System.Text.StringBuilder();
            foreach (var item in result.Items)
            {
                response.AppendLine("exist: " + item.Exist);
                response.AppendLine("insert: " + item.Insert);
                response.AppendLine("key: " + item.key);
                response.AppendLine();
            }

            Bll.Process.GenericLog.Log(HistoryLogType.BlockedItemsManagementV2, merchant.ID, request.ToString(), response.ToString());

            return result;
        }

    }
}
