﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ComponentModel;
using Netpay.Infrastructure;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// Summary WsdlDocumentation for AppIdentity
    /// </summary>
    [System.ServiceModel.ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class AppIdentity : V2.BaseService
	{
        #region Service Classes
		public class AppIdentityDetails
		{
			public string Name { get; set; }
			public string BrandName { get; set; }
			public string CompanyName { get; set; }
			public string DomainName { get; set; }
			public string Theme { get; set; }
			public string URLDevCenter { get; set; }
			public string URLProcess { get; set; }
			public string URLMerchantCP { get; set; }
			public string URLWallet { get; set; }
			public string URLWebsite { get; set; }
			//public string SmtpServer { get; set; }
			//public string SmtpUsername { get; set; }
			//public string SmtpPassword { get; set; }
			//public string EmailFrom { get; set; }
			//public string EmailContactTo { get; set; }
			public string CopyRightText { get; set; }
			public bool IsActive { get; set; }

			public AppIdentityDetails() {}
			public AppIdentityDetails(Bll.ApplicationIdentity value)
			{
				Name = value.Name;
				BrandName = value.BrandName;
				CompanyName = value.CompanyName;
				DomainName = value.DomainName;
				Theme = value.ThemeName;
				//ContentFolder = value.ContentFolder;
				URLDevCenter = value.URLDevCenter;
				URLProcess = value.URLProcess;
				URLMerchantCP = value.URLMerchantCP;
				URLWallet = value.URLWallet;
				URLWebsite = value.URLWebsite;
				//SmtpServer = value.SmtpServer;
				//SmtpUsername = value.SmtpUsername;
				//SmtpPassword = value.SmtpPassword;
				//EmailFrom = value.EmailFrom;
				//EmailContactTo = value.EmailContactTo;
				CopyRightText = value.CopyRightText;
				IsActive = value.IsActive;
			}
		}
        #endregion

        public AppIdentity() : base(false) { }
        protected override void SetContext()
        {
            base.SetContext();
            if (AppIdentity == null) throw new System.Web.HttpException((int)System.Net.HttpStatusCode.Unauthorized, ApplicationToken_Key + " value not correct");
        }

        [OperationContract, WsdlDocumentation("The service will return all the identity information available in the first tab – this will allow developer to present the information on the client side.")]
		public AppIdentityDetails GetIdentityDetails()
		{
			return new AppIdentityDetails(AppIdentity);
		}

        [OperationContract, WsdlDocumentation("log error or info message")]
		public void Log(int severityId, string message, string longMessage)
		{
			Netpay.Infrastructure.Logger.Log((LogSeverity)severityId, LogTag.MobileApp, message, longMessage);
		}

		[OperationContract, WsdlDocumentation("This service will return the contact email information to enable developer to embed a contact us form easily in their apps.")]
		public void SendContactEmail(string from, string subject, string body)
		{
            AppIdentity.SendEmail(from, subject, body);
		}

        [OperationContract, WsdlDocumentation("The service will return an html file that is placed in the content folder listed in the identity setup in the admincash (folders are managed manually by DBA. Used in cases you with to place designed text on a website or a mobile app.")]
		public string GetContent(string contentName)
		{
            return AppIdentity.GetContent(contentName);
		}

		[OperationContract, WsdlDocumentation("The app identity allows to control the supported payment methods in the app, to prevent users to try and load payment method which are not supported. The service will return the list of supported payment methods.")]
		public int[] GetSupportedPaymentMethods() 
		{
            return AppIdentity.SupportedPaymentMethods.Select(p => (int)p).ToArray();
		}

        [OperationContract, WsdlDocumentation("The app identity allows to control the supported currencies, this service call will return the supported currencies as listed on the app identity management screen.")]
        public string[] GetSupportedCurrencies()
		{
            return AppIdentity.SupportedCurrencies.ToArray();
		}

		[OperationContract, WsdlDocumentation("As you can limit an identity to a certain group of merchants, the service will return which group are relevant to the identity to enable the developer to filter other services by the relevant groups.")]
		public KeyValue<int, string>[] GetMerchantGroups()
		{
            return Bll.Merchants.Group.Cache.Where(v => AppIdentity.RelatedMerchantGroups.Contains(v.ID)).Select(v => new KeyValue<int, string>(v.ID, v.Name)).ToArray();
		}
	}
}
