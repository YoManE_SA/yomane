﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ComponentModel;
using System.Web;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System.Linq;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// base account services
    /// </summary>
    [System.ServiceModel.ServiceBehavior(AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
    [System.ServiceModel.ServiceContract(Namespace = "http://netpay-intl.com/", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class Infobip : V2.BaseService
    {
        #region Class Declaration

        private string strInfobipAPIURL_TokenManager = "http://rad2.Infobip.co:8080/Infobip-tokenmanager/vsp/tokens";
        private string strInfobipAppID = "VSP_50180";
        string strInfobipPassword = "c0ebc87dd3aa1d8c9e9ebfc8324bb7270faeca8c";

        #endregion

        #region Service Classes
        public class LoginOptions
        {
            public bool setCookie { get; set; }
            public string applicationToken { get; set; }
            public string appName { get; set; }
            public string deviceId { get; set; }
            public string pushToken { get; set; }
            public bool? excludeDataImages { get; set; }
            public UserRole? userRole { get; set; }
        }

        public class LoginResult : ServiceResult
        {
            public LoginResult() { }
            public LoginResult(ValidationResult result) : base(result) { LastLogin = new DateTime(1970, 1, 1); }
            public string CredentialsToken { set; get; }
            public string CredentialsHeaderName { get; set; }
            public bool VersionUpdateRequired { get; set; }
            public DateTime LastLogin { set; get; }
            public bool IsFirstLogin { set; get; }

            public bool IsDeviceRegistrationRequired { get; set; }
            public bool IsDeviceActivated { set; get; }
            public bool IsDeviceRegistered { set; get; }
            public bool IsDeviceBlocked { set; get; }

            public string EncodedCookie { set; get; }
        }

        public class CookieResult : ServiceResult
        {
            internal CookieResult(ValidationResult result) : base(result) { }
            public CookieResult() { }
            public string FullName { get; set; }
            public string Email { get; set; }
            public byte[] ProfilePicture { get; set; }
        }


        #endregion

        public Infobip() : base(true, new UserRole[] { UserRole.Admin }) { }


        #region Login


        [OperationContract, WsdlDocumentation(""), ContextMethod(true, true)]
        public LoginResult Login(string email, string userName, string password, LoginOptions options)
        {
            if (email == null || password == null)
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (email.Trim() == string.Empty || password.Trim() == string.Empty)
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (!email.IsEmail())
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (options.userRole == UserRole.Customer && string.IsNullOrEmpty(options.applicationToken))
                return new LoginResult(ValidationResult.Invalid_AppIdentity);

            Guid creds = Guid.Empty;
            Infrastructure.Security.LoginResult loginResult = Infrastructure.Security.LoginResult.UserNotFound;

            if (options.userRole == null || options.userRole == UserRole.Admin)
            {
                loginResult = Infrastructure.Security.Login.DoLogin(new UserRole[] { UserRole.Admin }, userName, email, password, options.deviceId, out creds);
            }


            if (loginResult == Infrastructure.Security.LoginResult.AdminBlocked || loginResult == Infrastructure.Security.LoginResult.AbsenceBlocked || loginResult == Infrastructure.Security.LoginResult.FailedAttemptsBlocked)
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (loginResult == Infrastructure.Security.LoginResult.Success)
            {
                ObjectContext.Current.CredentialsToken = creds;
                LoginResult result = new LoginResult(ValidationResult.Success);
                result.CredentialsToken = creds.ToString();
                Login user = Infrastructure.Security.Login.Get(creds);
                result.IsFirstLogin = user.IsFirstLogin;
                result.LastLogin = user.LastLogin.GetValueOrDefault();
                //result.Number = Bll.Accounts.Account.Current.AccountNumber;

                Netpay.Bll.ApplicationIdentity identity = null;
                if (Bll.Customers.Customer.Current != null && Bll.Customers.Customer.Current.RefIdentityID != null)
                    identity = Netpay.Bll.ApplicationIdentity.Load(Bll.Customers.Customer.Current.RefIdentityID.Value);
                else if (options != null && !string.IsNullOrEmpty(options.applicationToken))
                    identity = Netpay.Bll.ApplicationIdentity.GetIdentity(options.applicationToken.ToNullableGuid().GetValueOrDefault());
                if (identity != null) user.Items["Session.ApplicationIdentity"] = identity;
                if (options != null) user.Items["excludeDataImages"] = options.excludeDataImages;
                result.EncodedCookie = EncodeLoginCookie(options != null ? options.applicationToken : null, result.Number, email);
                if (options != null && options.setCookie) Context.Response.Cookies.Add(new HttpCookie(Web.WebUtils.CredentialsTokenName, creds.ToString()));
                //result.IsDeviceRegistrationRequired = by app identity
                //if (result.IsDeviceRegistrationRequired)
                //{
                //    var device = Bll.Accounts.MobileDevice.Current;
                //    if (device != null)
                //    {
                //        result.IsDeviceRegistered = true;
                //        result.IsDeviceActivated = device.IsActivated;
                //        result.IsDeviceBlocked = !device.IsActive;
                //    }
                //}
                result.CredentialsHeaderName = Web.WebUtils.CredentialsTokenName;
                return result;
            }
            else
            {
                if (options != null && options.setCookie) Context.Response.Cookies.Remove(Web.WebUtils.CredentialsTokenName);
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            }
        }

        private string EncodeLoginCookie(string applicationToken, string mumber, string email)
        {
            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());
            return string.Format("CustomerNumber={0}&Email={1}&Signature={2}", HttpUtility.UrlEncode(mumber), HttpUtility.UrlEncode(email), (mumber + email + (appIdentity != null ? appIdentity.ID : 0)).ToSha256());
        }

        [OperationContract, WsdlDocumentation("When using the login service to login the service will return an Encodedcookie – using the cookie and the token you will be able to create an auto login function in cases you don’t want the user to relogin every time he enters the app."), ContextMethod(true, true)]
        public CookieResult DecodeLoginCookie(string applicationToken, string cookie)
        {
            var cookieValues = System.Web.HttpUtility.ParseQueryString(cookie);
            byte[] image;
            var ret = new CookieResult(ValidationResult.Success);
            ret.Number = cookieValues["CustomerNumber"].NullIfEmpty();
            ret.Email = cookieValues["Email"].NullIfEmpty();
            if (ret.Number == null || ret.Email == null) return new CookieResult(ValidationResult.Invalid_Email);
            if (EncodeLoginCookie(applicationToken, ret.Number, ret.Email) != cookie) return new CookieResult(ValidationResult.Invalid_Data);
            ret.FullName = Netpay.Bll.Customers.Customer.GetFullName(ret.Number, ret.Email, out image);
            ret.ProfilePicture = image;
            return ret;
        }



        #endregion

        #region GetBalance

        [OperationContract(), WsdlDocumentation(""), ContextMethod(true, true), WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetBalance(string strPAN, string applicationToken)
        {

            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());

            if (appIdentity == null || appIdentity == Netpay.Bll.ApplicationIdentity.Default)
                return "Invalid applicationToken";


            var spm = Bll.Accounts.StoredPaymentMethod.FindByPAN(strPAN, null).SingleOrDefault(); //.Where(v => v.ProviderID == STOREDPM_PROVIDERNAME)
            if (spm != null)
            {
                var pCustomer = Netpay.Bll.Customers.Customer.Search(new Bll.Customers.Customer.SearchFilters() {  AccountsIdsList = new System.Collections.Generic.List<int>() { spm.Account_id } }, null);

                if (pCustomer == null || pCustomer.Count == 0)
                    return "Could not find customer";

                bool includePending = false;

                var ret = Bll.Accounts.Balance.GetStatus(Bll.Accounts.Account.Current.AccountID).Select(r => new WebServices.Balance.BalanceTotal() { CurrencyIso = r.CurrencyIso, Value = includePending ? r.Expected : r.Current });

                if (ret.Count() > 0)
                {
                    return ret.ToList()[0].Value + " " + ret.ToList()[0].CurrencyIso;
                }
            }



            return "";

        }

        #endregion

        #region Generic Classes

        public class MyMapper : System.ServiceModel.Channels.WebContentTypeMapper
        {
            public override System.ServiceModel.Channels.WebContentFormat GetMessageFormatForContentType(string contentType)
            {
                if (contentType.ToUpper().Contains("json".ToUpper()))
                {
                    return System.ServiceModel.Channels.WebContentFormat.Raw;
                }
                else
                {
                    return System.ServiceModel.Channels.WebContentFormat.Default;
                }
            }
        }

        #endregion
    }
}
