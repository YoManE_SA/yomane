﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ComponentModel;
using Netpay.Infrastructure.Security;

namespace Netpay.WebServices.V2
{
    [System.ServiceModel.ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class Balance : V2.BaseService
    {
        #region Service Classes

        public class BalanceRow
        {
            public int ID { get; set; }
            public string CurrencyIso { get; set; }
            public decimal Amount { get; set; }
            public decimal Total { get; set; }
            public System.DateTime InsertDate { get; set; }
            public string Text { get; set; }
            public bool IsPending { get; set; }
            public int? SourceID { get; set; }
            public string SourceType { get; set; }
            public BalanceRow() { }
            #region Save And Load
            internal BalanceRow(Bll.Accounts.Balance cs)
            {
                if (cs == null) return;
                ID = cs.ID;
                CurrencyIso = cs.CurrencyIso;
                Amount = cs.Amount;
                Total = cs.Total;
                InsertDate = cs.InsertDate;
                Text = cs.Text;
                IsPending = cs.IsPending;
                SourceID = cs.SourceID;
                SourceType = cs.SourceType.ToString();
            }
            #endregion
        }

        public class BalanceRequest
        {
            public int ID { get; set; }
            public System.DateTime RequestDate { get; set; }
            public int SourceAccountID { get; set; }
            public int TargetAccountID { get; set; }
            public string SourceText { get; set; }
            public string TargetText { get; set; }

            public string SourceAccountName { get; set; }
            public string TargetAccountName { get; set; }

            public bool IsPush { get; set; }
            public decimal Amount { get; set; }
            public string CurrencyISOCode { get; set; }

            public bool? IsApproved { get; set; }
            public System.DateTime? ConfirmDate { get; set; }
            public BalanceRequest() { }
            #region Save And Load
            internal BalanceRequest(Bll.Accounts.BalanceRequest cs, Dictionary<int, string> accountNames)
            {
                ID = cs.ID;
                RequestDate = cs.RequestDate;
                SourceAccountID = cs.SourceAccountID;
                TargetAccountID = cs.TargetAccountID;
                SourceText = cs.SourceText;
                TargetText = cs.TargetText;
                IsPush = cs.IsPush;
                Amount = cs.Amount;
                CurrencyISOCode = cs.CurrencyISOCode;
                IsApproved = cs.IsApproved;
                ConfirmDate = cs.ConfirmDate;
                if (accountNames.ContainsKey(SourceAccountID)) SourceAccountName = accountNames[SourceAccountID];
                if (accountNames.ContainsKey(TargetAccountID)) TargetAccountName = accountNames[TargetAccountID];
            }
            #endregion
        }

        public class BalanceFilter
        {
            public int? StoredPaymentMethodID { get; set; }
            public string CurrencyIso { get; set; }
        }

        public class BalanceTotal
        {
            public string CurrencyIso { get; set; }
            public decimal Current { get; set; }
            public decimal Pending { get; set; }
            public decimal Expected { get; set; }
        }

        #endregion
        public Balance() : base(true, new UserRole[] { UserRole.Account }) { }

        [OperationContract, WsdlDocumentation("Get loggedin user balance list")]
        public List<BalanceRow> GetRows(BalanceFilter filters, SortAndPage sortAndPage)
        {
            List<Bll.Accounts.Balance> ret = null;
            if (filters.StoredPaymentMethodID.HasValue && filters.StoredPaymentMethodID.Value != 0) {
                var pm = Bll.Accounts.StoredPaymentMethod.Load(filters.StoredPaymentMethodID.Value);
                if (pm == null) throw new Exception("Invalid StoredPaymentMethodID");
                if (pm.Account_id != Bll.Accounts.Account.Current.AccountID) throw new Exception("Invalid StoredPaymentMethodID");
                if (pm.Provider == null) new Exception("StoredPaymentMethodID invalid provider");
                ret = pm.Provider.GetBalace(pm, new Infrastructure.Range<DateTime>(DateTime.Now.AddYears(-1), DateTime.Now));
            } else ret = Bll.Accounts.Balance.Search(new Bll.Accounts.Balance.SearchFilters() { AccountID = Bll.Accounts.Account.Current.AccountID, CurrencyIso = filters.CurrencyIso }, sortAndPage.ToSortAndPage());
            return ret.Select(r => new BalanceRow(r)).ToList();
        }

        [OperationContract, WsdlDocumentation("Get loggedin user total balance")]
        public List<BalanceTotal> GetTotal(string currencyIsoCode)
        {
            var ret = Bll.Accounts.Balance.GetStatus(Bll.Accounts.Account.Current.AccountID).Select(r => new BalanceTotal() { CurrencyIso = r.CurrencyIso, Current = r.Expected, Pending = r.Pending, Expected = r.Expected });
            if (!string.IsNullOrEmpty(currencyIsoCode)) ret = ret.Where(r => r.CurrencyIso == currencyIsoCode);
            return ret.ToList();
        }

        [OperationContract, WsdlDocumentation("Push Transfer from balance to another account's balance")]
        public ServiceResult TransferAmount(string destAcocuntId, decimal amount, string currencyIso, string pinCode, string text)
        {
            //Validate currencyiso
            if (string.IsNullOrEmpty(currencyIso) || !Bll.Currency.IsIsoCodeExist(currencyIso)) return new ServiceResult(Infrastructure.ValidationResult.Invalid_Currency);
            var targetAccount = Bll.Accounts.Account.AccountIDByNumber(Bll.Accounts.AccountType.Customer, destAcocuntId);
            if (targetAccount == null)
                return new ServiceResult(Infrastructure.ValidationResult.Item_Not_Exist);
            if (!Bll.Accounts.Account.Current.ValidatePinCode(pinCode))
                return new ServiceResult(Infrastructure.ValidationResult.PinCodeConfirmationMissmatch);
            var request = new Bll.Accounts.BalanceRequest(Bll.Accounts.Account.Current.AccountID, targetAccount.Value, amount, currencyIso, text, true);
            request.Save();
            return new ServiceResult(Infrastructure.ValidationResult.Success);
        }

        [OperationContract, WsdlDocumentation("Reply Pull Transfer from balance to another account's balance")]
        public ServiceResult ReplyRequest(int requestId, bool approve, string pinCode, string text)
        {
            var request = Bll.Accounts.BalanceRequest.Load(requestId);
            if (request == null)
                return new ServiceResult(Infrastructure.ValidationResult.Invalid_RequestID);
            //if (request.SourceAccountID != targetCustomer.AccountID) return new ServiceResult(Infrastructure.ValidationResult.Invalid_RequestForAccountID);
            if (!Bll.Accounts.Account.Current.ValidatePinCode(pinCode))
                return new ServiceResult(Infrastructure.ValidationResult.PinCodeConfirmationMissmatch);
            request.Reply(approve, text);
            request.Save();
            return new ServiceResult(Infrastructure.ValidationResult.Success);
        }

        [OperationContract, WsdlDocumentation("Init Pull Transfer from balance to another account's balance")]
        public ServiceResult RequestAmount(string destAcocuntId, decimal amount, string currencyIso, string text)
        {
            if (string.IsNullOrEmpty(currencyIso) || !Bll.Currency.IsIsoCodeExist(currencyIso)) return new ServiceResult(Infrastructure.ValidationResult.Invalid_Currency);
            var targetAccount = Bll.Accounts.Account.AccountIDByNumber(Bll.Accounts.AccountType.Customer, destAcocuntId);
            if (targetAccount == null) return new ServiceResult(Infrastructure.ValidationResult.Item_Not_Exist);
            var request = new Bll.Accounts.BalanceRequest(Bll.Accounts.Account.Current.AccountID, targetAccount.Value, amount, currencyIso, text, false);
            request.Save();
            return new ServiceResult(Infrastructure.ValidationResult.Success);
        }

        [OperationContract, WsdlDocumentation("Get new Pull Transfers requests")]
        public List<BalanceRequest> GetRequests(BalanceFilter filters, SortAndPage sortAndPage)
        {
            var items = Bll.Accounts.BalanceRequest.Search(new Bll.Accounts.BalanceRequest.SearchFilters() { AnyAccountID = Bll.Accounts.Account.Current.AccountID, CurrencyISOCode = filters.CurrencyIso }, sortAndPage.ToSortAndPage());
            var names = Netpay.Bll.Accounts.Account.GetNames(items.SelectMany(v => new int[] { v.SourceAccountID, v.TargetAccountID }).ToList());
            return items.Select(r => new BalanceRequest(r, names)).ToList();
        }

        [OperationContract, WsdlDocumentation("Get new Pull Transfers requests")]
        public BalanceRequest GetRequest(int requestId)
        {
            var request = Bll.Accounts.BalanceRequest.Load(requestId);
            if (request == null) return null;
            var names = Netpay.Bll.Accounts.Account.GetNames(new List<int> { request.SourceAccountID, request.TargetAccountID });
            return new BalanceRequest(request, names);
        }
    }
}
