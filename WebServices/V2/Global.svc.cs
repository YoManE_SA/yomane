﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ComponentModel;
using Netpay.Infrastructure;
using System.ServiceModel.Web;
using System.Web;
using System.Text;
using System.IO;
using System.Reflection;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// Global service
    /// </summary>
    [System.ServiceModel.ServiceBehavior(AddressFilterMode =System.ServiceModel.AddressFilterMode.Any)]
    [System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
    [System.ServiceModel.ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class Global 
	{
        [OperationContract, WsdlDocumentation("Get current service version"), WebGet]
        public System.IO.Stream GetVersion()
        {
            string result = Netpay.Infrastructure.Application.Version;
            byte[] resultBytes = Encoding.UTF8.GetBytes(result);
            return new MemoryStream(resultBytes);
        }

        [OperationContract, WsdlDocumentation("Get available services"), WebGet]
        public System.IO.Stream GetServices()
        {
            string svcPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase), @"..\V2\");
            svcPath = new Uri(svcPath).LocalPath;
            string[] files = Directory.GetFiles(svcPath, "*.svc");
            List<string> fileNames = files.Select(f => Path.GetFileName(f)).ToList();
            string result = fileNames.ToDelimitedString();
            byte[] resultBytes = Encoding.UTF8.GetBytes(result);
            return new MemoryStream(resultBytes);
        }

        [OperationContract, WsdlDocumentation("Dummy for androied certificate verification"), WebGet]
        public System.IO.Stream CertPing()
        {
            byte[] resultBytes = Encoding.UTF8.GetBytes("ok");
            return new MemoryStream(resultBytes);
        }
    }
}
