using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ServiceModel;
using Netpay.Infrastructure;
using System.Globalization;
using Netpay.Web;
using System.IO;
using System.Web;
using Netpay.Bll.Shop;
using Netpay.Bll;
using System.Net.Mail;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// Summary description for Shop
    /// </summary>
    [System.ServiceModel.ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class Shop : V2.BaseService
    {
        public Shop() : base(false, new Infrastructure.Security.UserRole[] { Infrastructure.Security.UserRole.Customer }) { }

        public class SearchFilter
        {
            public bool PromoOnly;
            public string MerchantNumber;
            public int ShopId;
            public string Tags;
            public string Name;
            public string Language;
            public int[] Categories;
            public int[] MerchantGroups;
            public string[] Regions;
            public string[] Countries;
            public bool IncludeGlobalRegion;
            //public bool loadMerchant;
        }

        public class MerchantFilters
        {
            public string ApplicationToken;
            public int? GroupId;
            public string Text;
            public int? MerchantStatus;
        }

        public class Location
        {
            public string IsoCode { get; set; }
            public string Name { get; set; }
        }

        public class Settings
        {
            public Settings() { }

            internal Settings(RegionShop shop, int merchantId, string merchantNumber, CultureInfo culture)
            {
                ShopId = shop.ShopId;
                LocationsString = shop.Locations.ToString();
                var hppSettings = Bll.Merchants.Hosted.PaymentPage.Load(merchantId);
                if (hppSettings != null && hppSettings.LogoImage != null && hppSettings.LogoImage.Trim() != string.Empty)
                    LogoUrl = (new System.Uri(new System.Uri(Domain.Current.WebServicesUrl), Bll.Accounts.Account.MapPublicVirtualPath(merchantNumber, hppSettings.LogoImage))).ToString();
                BannerUrl = (new System.Uri(new System.Uri(Domain.Current.WebServicesUrl), Bll.Accounts.Account.MapPublicVirtualPath(merchantNumber, shop.BannerFileName))).ToString();
                BannerLinkUrl = shop.BannerLinkUrl;
                UIBaseColor = shop.UIBaseColor;
                FacebookUrl = shop.FacebookUrl;
                GooglePlusUrl = shop.GooglePlusUrl;
                TwitterUrl = shop.TwitterUrl;
                VimeoUrl = shop.VimeoUrl;
                YoutubeUrl = shop.YoutubeUrl;
                LinkedinUrl = shop.LinkedinUrl;
                PinterestUrl = shop.PinterestUrl;
                CurrencyIsoCode = shop.CurrencyIsoCode;

                if (culture != null)
                {
                    Regions = shop.Locations.Regions.Select(i => new Location()
                    {
                        IsoCode = i.IsoCode,
                        Name = Netpay.Web.WebUtils.GetResourceValue("Regions", i.IsoCode, culture)
                    }).ToArray();
                    Countries = shop.Locations.Countries.Select(i => new Location()
                    {
                        IsoCode = i.IsoCode2,
                        Name = Netpay.Web.WebUtils.GetResourceValue("Countries", i.IsoCode2, culture)
                    }).ToArray();
                }
                else
                {
                    Regions = shop.Locations.Regions.Select(i => new Location()
                    {
                        IsoCode = i.IsoCode,
                        Name = i.Name
                    }).ToArray();
                    Countries = shop.Locations.Countries.Select(i => new Location()
                    {
                        IsoCode = i.IsoCode2,
                        Name = i.Name
                    }).ToArray();
                }
            }

            public int ShopId { set; get; }
            public string LocationsString { set; get; }
            public Location[] Regions { set; get; }
            public Location[] Countries { set; get; }
            public string LogoUrl { set; get; }
            public string BannerUrl { set; get; }
            public string BannerLinkUrl { set; get; }
            public string UIBaseColor { set; get; }
            public string FacebookUrl { set; get; }
            public string GooglePlusUrl { set; get; }
            public string TwitterUrl { set; get; }
            public string VimeoUrl { set; get; }
            public string YoutubeUrl { set; get; }
            public string LinkedinUrl { set; get; }
            public string PinterestUrl { set; get; }
            public string CurrencyIsoCode { set; get; }
        }

        public class Merchant
        {
            public string Name { set; get; }
            public string Number { set; get; }
            public string Email { set; get; }
            public string WebsiteUrl { set; get; }
            public string PhoneNumber { set; get; }
            public string FaxNumber { set; get; }
            public string Group { get; set; }
            public ServiceAddress Address { get; set; }
            public string[] Currencies { get; set; }
            public string[] Languages { get; set; }

            #region Save And Load
            public Merchant() { }
            internal Merchant(Bll.Merchants.MerchantPublicInfo merchant)
            {
                if (merchant == null) return;
                Number = merchant.Number;
                Name = merchant.Name;
                Email = merchant.EmailAddress;
                WebsiteUrl = merchant.WebsiteUrl;
                PhoneNumber = merchant.Phone;
                FaxNumber = merchant.Fax;
                Address = new ServiceAddress(merchant.BusinessAddress);
                if (merchant.GroupID != null) Group = Bll.Merchants.Group.Get(merchant.GroupID.Value).Name;
                Currencies = Bll.Merchants.Merchant.GetSupportedCurrencies(merchant.ID).Select(c => c.IsoCode).ToArray();
                Languages = Bll.Merchants.Merchant.GetAvailableContentLanguges(merchant.AccountID).ToArray();
            }
            #endregion
        }

        public class ProductCategory
        {
            public ProductCategory() { }
            public int ID { get; set; }
            public string Name { get; set; }
            public ProductCategory[] SubCategories { get; set; }
            internal ProductCategory(Bll.Shop.Products.Category value)
            {
                ID = value.ID;
                Name = value.Name;
                if (value.Values != null)
                    SubCategories = value.Values.Select(v => new ProductCategory(v)).ToArray();
            }
        }

        public class Product
        {
            public Product() { }

            public Product(Netpay.Bll.Shop.Products.Product.CategorisedProduct product, string language)
            {
                ID = product.ProductId;
                Name = product.ProductName;
                Description = product.ProductDescription;
                Price = product.Price;
                Currency = product.CurencyIso;
                if (!string.IsNullOrEmpty(product.ImageFileName))
                    ImageURL = (new System.Uri(new System.Uri(Domain.Current.WebServicesUrl), product.ImageURL)).ToString();
                if (!string.IsNullOrEmpty(product.ImageSmallFileName))
                    ImageSmallURL = (new System.Uri(new System.Uri(Domain.Current.WebServicesUrl), product.ImageSmallURL)).ToString();
                CategoryId = product.CategoryId;
                CategoryName = product.CategoryName;
                IsRecurring = product.IsRecurring;
                IsDynamicAmount = product.IsDynamic;

                if (language != null && language.Length > 0)
                {
                    CultureInfo culture = CultureInfo.GetCultureInfo(language);
                    if (culture != null)
                        CategoryName = CommonResources.Utils.GetResourceValue("ProductCategories", CategoryName, culture);
                }
            }

            internal Product(Bll.Shop.Products.Product ppo, Bll.Merchants.MerchantPublicInfo merchant, string language, bool loadExtraInfo)
            {
                ID = ppo.ID;
                SKU = ppo.SKU;
                var text = ppo.GetTextForLanguage(language);
                if (text != null)
                {
                    Name = text.Name;
                    Description = text.Description;
                    Meta_Title = text.MetaTitle;
                    Meta_Description = text.MetaDescription;
                    Meta_Keywords = text.MetaKeyword;
                }
                Price = ppo.Price;
                IsDynamicAmount = ppo.IsDynamicProduct;

                this.Type = (byte)ppo.Type;
                QuantityMin = ppo.QtyStart;
                QuantityMax = ppo.QtyEnd;
                QuantityInterval = ppo.QtyStep;
                QuantityAvailable = ppo.ActualQuantity;

                Currency = ppo.CurrencyISOCode;
                if (merchant == null) merchant = Bll.Merchants.MerchantPublicInfo.Load(ppo.MerchantID);
                if (merchant != null) Merchant = new Shop.Merchant(merchant);
                if (Merchant == null) return;

                if (!string.IsNullOrEmpty(ppo.ImageFileName))
                    ImageURL = (new System.Uri(new System.Uri(Domain.Current.WebServicesUrl), ppo.ImageURL)).ToString();
                ProductURL = ppo.ProductURL;
                Categories = ppo.Categories.ToArray();

                // needed for single product only, not loaded with multiple products
                if (loadExtraInfo)
                {
                    PaymentMethods = Bll.Merchants.Hosted.PaymentPage.GetAvailablePaymentMethods(ppo.MerchantID, Bll.Currency.Get(ppo.CurrencyISOCode).ID, false).Select(p => p.ID).ToArray();
                    Stocks = ppo.Stocks.Select(s => new ProductStock(s)).ToArray();
                    Properties = ppo.Properties.Select(s => new ProductProperty(s)).ToArray();
                }

                IsRecurring = false;
                if (ppo.RecurringString != null && ppo.RecurringString.Trim().Length > 0)
                {
                    IsRecurring = true;

                    // add checkout url for recurring prod
                    var vars = HttpUtility.ParseQueryString("");
                    vars.Add("merchantID", Merchant.Number);
                    //vars.Add("trans_installments", Installments.ToString());
                    vars.Add("trans_amount", Price.ToString("0.00"));
                    vars.Add("trans_currency", Currency);
                    vars.Add("disp_payFor", HttpUtility.UrlEncode(Name));

                    string[] series = ppo.RecurringString.Split(';');
                    for (int idx = 0; idx < series.Length; idx++)
                    {
                        vars.Add("trans_recurring" + (idx + 1), series[idx]);
                    }

                    RecurringDisplay = ExtractRcurring(ppo, language);

                    string merchantHashCode = Bll.Merchants.Merchant.Load(Merchant.Number).HashKey;
                    vars.Add("signature", (vars.GetValuesString() + merchantHashCode).ToSha256());
                    CheckoutUrl = WebUtils.CurrentDomain.ProcessV2Url + "/Hosted/?" + vars.ToString();
                }
            }

            public Product(Bll.Merchants.MerchantPublicInfo merchant) { Merchant = new Shop.Merchant(merchant); }

            public int ID { set; get; }
            public string SKU { set; get; }
            public string Name { set; get; }
            public string Description { set; get; }
            public string ImageURL { set; get; }
            public string ImageSmallURL { set; get; }
            public string ProductURL { set; get; }
            public decimal Price { set; get; }
            public string Currency { set; get; }
            public int[] PaymentMethods { get; set; }
            public bool IsDynamicAmount { set; get; }
            public byte Type { get; set; }
            public int QuantityMin { set; get; }
            public int QuantityMax { set; get; }
            public int QuantityInterval { set; get; }
            public int? QuantityAvailable { set; get; }
            public Shop.Merchant Merchant { get; set; }

            public string Meta_Title { get; set; }
            public string Meta_Description { get; set; }
            public string Meta_Keywords { get; set; }

            public ProductProperty[] Properties { get; set; }
            public ProductStock[] Stocks { get; set; }
            public short[] Categories { get; set; }
            public short CategoryId { get; set; }
            public string CategoryName { get; set; }
            public string CheckoutUrl { get; set; }
            public bool IsRecurring { get; set; }
            public string RecurringDisplay { get; set; }

            public int? NetxProductId { get; set; }
            public int? PrevProductId { get; set; }
        }

        public class ProductProperty
        {
            public ProductProperty() { }
            public int ID { get; set; }
            public string Text { get; set; }
            public string Type { get; set; }
            public string Value { get; set; }


            public ProductProperty[] Values { get; set; }
            #region Save And Load
            internal ProductProperty(Bll.Shop.Products.Property value)
            {
                ID = value.ID;
                Text = value.Name;
                Type = value.Type.ToString();
                Value = value.Value;
                if (value.Values != null)
                    Values = value.Values.Select(v => new ProductProperty(v)).ToArray();
            }
            #endregion
        }

        public class ProductStock
        {
            public ProductStock() { }
            public int ID { get; set; }
            public string SKU { get; set; }
            public int? QuantityAvailable { set; get; }
            public int[] PropertyValues { get; set; }

            #region Save And Load
            internal ProductStock(Bll.Shop.Products.Stock value)
            {
                ID = value.ID;
                SKU = value.SKU;
                PropertyValues = value.Properties.Select(p => p.ID).ToArray();
                QuantityAvailable = value.QtyAvailable;
            }
            #endregion
        }

        public class CartItemProperty
        {
            public CartItemProperty() { }
            //public int ID { get; set; }
            public int? PropertyID { get; set; }
            public string Name { get; set; }
            public string Value { get; set; }
            #region Save And Load
            internal CartItemProperty(Bll.Shop.Cart.BasketItemProperty value)
            {
                //ID = value.ID;
                PropertyID = value.PropertyID;
                Name = value.Name;
                Value = value.Value;
            }
            #endregion
        }

        public class CartItem
        {
            public CartItem() { }
            public int ID { get; set; }
            public int? ProductId { get; set; }
            public int? ProductStockId { get; set; }
            public string ProductImageUrl { get; set; }

            public System.DateTime InsertDate { get; set; }
            public string Name { get; set; }
            public short Quantity { get; set; }
            public decimal Price { get; set; }
            public string CurrencyISOCode { get; set; }
            public decimal CurrencyFXRate { get; set; }
            public byte Type { get; set; }

            public decimal ShippingFee { get; set; }
            public decimal VATPercent { get; set; }

            public decimal TotalShipping { get; set; }
            public decimal TotalProduct { get; set; }
            public decimal Total { get; set; }

            public int? MinQuantity { get; set; }
            public int? MaxQuantity { get; set; }
            public int? StepQuantity { get; set; }

            public string DownloadMediaType { get; set; }
            public string GuestDownloadUrl { get; set; }
            public string ReceiptLink { get; set; }
            public string ReceiptText { get; set; }

            public decimal ChangedPrice { get; set; }
            public decimal ChangedTotal { get; set; }
            public string ChangedCurrencyIsoCode { get; set; }
            public bool IsAvailable { get; set; }
            public bool IsChanged { get; set; }

            public CartItemProperty[] ItemProperties { get; set; }

            #region Save And Load
            internal CartItem(Bll.Shop.Cart.BasketItem bi)
            {
                ID = bi.ID;
                ProductId = bi.ProductId;
                ProductStockId = bi.ProductStockId;
                InsertDate = bi.InsertDate;
                Name = bi.Name;
                Quantity = bi.Quantity;
                Price = bi.Price;
                CurrencyISOCode = bi.CurrencyISOCode;
                CurrencyFXRate = bi.CurrencyFXRate;
                this.Type = (byte)bi.Type;
                ShippingFee = bi.ShippingFee;
                VATPercent = bi.VATPercent;
                TotalShipping = bi.TotalShipping;
                TotalProduct = bi.TotalProduct;
                Total = bi.Total;
                MinQuantity = bi.MinQuantity.GetValueOrDefault(0);
                MaxQuantity = bi.MaxQuantity.GetValueOrDefault(int.MaxValue);
                StepQuantity = bi.StepQuantity.GetValueOrDefault(1);
                ChangedPrice = bi.Changes.Price;
                ChangedCurrencyIsoCode = bi.Changes.CurrencyIsoCode;
                ChangedTotal = bi.Changes.Total;
                IsAvailable = bi.Changes.IsAvailable;
                IsChanged = bi.Changes.IsChanged;
                if (bi.Product != null)
                {
                    ProductImageUrl = (new System.Uri(new System.Uri(Domain.Current.WebServicesUrl), bi.Product.ImageURL)).ToString();
                    if (bi.Type == Bll.Shop.Products.ProductType.Download && !string.IsNullOrEmpty(bi.Product.MediaFileName))
                    {
                        DownloadMediaType = System.IO.Path.GetExtension(bi.Product.MediaFileName).TrimStart('.').ToUpper();
                        GuestDownloadUrl = bi.GuestDownloadLink;
                    }
                    if (bi.Product.Text != null)
                    {
                        ReceiptLink = bi.Product.Text.ReceiptLink;
                        ReceiptText = bi.Product.Text.ReceiptText;
                    }
                }
                ItemProperties = bi.ItemProperties.Select(ip => new CartItemProperty(ip)).ToArray();
            }
            #endregion
        }

        public class Cart
        {
            public Cart() { }
            public string Cookie { get; set; }
            public CartItem[] Items { get; set; }
            public decimal Total { get; set; }
            public string CurrencyIso { get; set; }
            public string CheckoutUrl { get; set; }
            public string MerchantReference { get; set; }
            public string MerchantNumber { get; set; }
            public Shop.Merchant Merchant { get; set; }
            public bool IsChanged { get; set; }
            public decimal ChangedTotal { get; set; }
            public byte Installments { get; set; }
            public byte MaxInstallments { get; set; }
            public int? ShopId { get; set; }

            #region Save And Load
            internal Cart(Bll.Shop.Cart.Basket value, string applicationToken)
            {
                MerchantReference = value.MerchantReference;
                CurrencyIso = value.CurrencyISOCode;
                Total = value.Total;
                IsChanged = value.Changes.IsChanged;
                ChangedTotal = value.Changes.Total;
                Merchant = new Shop.Merchant(value.MerchantInfo);
                MerchantNumber = value.MerchantInfo.Number;
                Items = value.BasketItems.Select(bi => new CartItem(bi)).ToArray();
                Cookie = Shop.EncodeBasketCookie(applicationToken, value);
                MaxInstallments = value.MaxInstallments;
                Installments = value.Installments > MaxInstallments ? MaxInstallments : value.Installments;
                ShopId = value.ShopId;

                var vars = HttpUtility.ParseQueryString("");
                vars.Add("merchantID", Merchant.Number);
                vars.Add("trans_installments", Installments.ToString());
                vars.Add("trans_amount", Total.ToString("0.00"));
                vars.Add("trans_currency", value.CurrencyISOCode);
                vars.Add("disp_payFor", HttpUtility.UrlEncode("Cart " + value.ID.ToString()));
                vars.Add("shop_cartId", HttpUtility.UrlEncode(value.Identifier.ToString()));
                vars.Add("signature", (vars.GetValuesString() + value.MerchantInfo.HashCode).ToSha256());
                CheckoutUrl = WebUtils.CurrentDomain.ProcessV2Url + "/Hosted/?" + vars.ToString();
            }
            #endregion
        }

        public class ShopSessionOptions
        {
            public string SuccessUrl { get; set; }
            public string DeclineUrl { get; set; }
            public string PendingUrl { get; set; }
            public int ImageWidth { get; set; }
            public int ImageHeight { get; set; }
        }

        public class ShopIds
        {
            public string MerchantNumber { get; set; }
            public int ShopId { get; set; }
        }


        //private ShopSessionOptions SessionOptions { get { return Session["SessionOptions"] as ShopSessionOptions; } }


        private ShopSessionOptions _sessionOptions;
        private ShopSessionOptions SessionOptions
        {
            get
            {
                if (_sessionOptions != null) return _sessionOptions;
                if (Infrastructure.Security.Login.Current == null) return null;
                var jsc = new System.Web.Script.Serialization.JavaScriptSerializer();
                var data = Infrastructure.Security.Login.Current.GetSessionItem("WS-Shop_SessionOptions");
                if (data == null) return null;
                _sessionOptions = jsc.Deserialize<ShopSessionOptions>(data);
                return _sessionOptions;
            }
        }


        [OperationContract]
        [WsdlDocumentation("Every client wishes to connect to the service must set a session on every connection. The setsession service allows to set the relevant redirect urls for a decline response and for a successful response further more the service enables to set the size of the product image needed to the client so the image will be resized for the interface (to reduce load time for mobile applications needing a smaller image)")]
        public void SetSession(string applicationToken, ShopSessionOptions options)
        {
            _sessionOptions = options;
            var jsc = new System.Web.Script.Serialization.JavaScriptSerializer();
            Infrastructure.Security.Login.Current.SetSessionItem("WS-Shop_SessionOptions", jsc.Serialize(_sessionOptions));
        }

        private bool ValidateWalletCredentias(string walletCredentials, bool redirect = true)
        {
            Infrastructure.ObjectContext.Current.CredentialsToken = walletCredentials.ToNullableGuid().GetValueOrDefault();
            //var user = Infrastructure.Security.Login.Get(walletCredentials.ToNullableGuid().GetValueOrDefault(), false);
            if (Infrastructure.ObjectContext.Current.User == null)
            {
                if (!redirect) return false;
                Context.Response.Buffer = false;
                Context.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
                Context.Response.StatusDescription = "Not logged in";
                Context.Response.Write("Not logged in");
                Context.Response.End();
            }
            return true;
        }

        private void ResponseHttpError(string errorString, bool plainText = false, System.Net.HttpStatusCode code = System.Net.HttpStatusCode.InternalServerError)
        {
            if (!plainText) new Exception(errorString);
            Context.Response.Buffer = false;
            Context.Response.StatusCode = (int)code;
            Context.Response.StatusDescription = errorString;
            Context.Response.Write(errorString);
            Context.Response.End();
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("Each merchant can manage multiple shops � one for each territory he wishes to open his activity or separate the products provided in each shop. This service will return the shop ids available to the merchant so you can pull the relevant data for a specific shop.")]
        public ShopIds GetShopIds(string subDomainName)
        {
            var shop = RegionShop.Load(subDomainName);
            if (shop == null)
                return null;

            ShopIds ids = new ShopIds();
            ids.MerchantNumber = shop.MerchantNumber;
            ids.ShopId = shop.ShopId;

            return ids;
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("Given that each merchant can manage multiple shops under his account, this service will return all of the shop info for a specific shop you requested.")]
        public Settings GetShop(string merchantNumber, int shopId)
        {
            var shop = RegionShop.Load(shopId);
            return new Settings(shop, shop.MerchantId, merchantNumber, null);
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("Each merchant defines the territories relevant for his shop, this service will allow you to filter available shops for a country/region and decide if you wish to get Global shops (shops for which the merchant listed their products are available world wide)")]
        public Settings[] GetShopsByLocation(string[] regions, string[] countries, bool includeGlobalShops, string culture)
        {
            CultureInfo cultureInfo = new CultureInfo(culture);
            var shops = RegionShop.Load(regions.ToList(), countries.ToList(), includeGlobalShops);
            var shopSettings = shops.Select(s => new Settings(s, s.MerchantId, s.MerchantNumber, cultureInfo)).ToArray();
            return shopSettings;
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("The service returns all of the �shop info� for all the merchant�s shops available. This service is aimed to filter shops for a single merchant.")]
        public Settings[] GetShops(string merchantNumber, string culture)
        {
            int merchantId = 0;
            if (!Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(merchantNumber, out merchantId))
                return null;

            CultureInfo cultureInfo = new CultureInfo(culture);
            var shops = RegionShop.LoadAll(merchantId);
            var shopSettings = shops.Select(s => new Settings(s, s.MerchantId, merchantNumber, cultureInfo)).ToArray();
            return shopSettings;
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("This service allows you to get multiple product with their categories. This method returns minimal product data and is optimized for lists.")]
        public Product[] GetCategorisedProducts(int shopId, int itemsPerCategory, string language)
        {
            var prods = Netpay.Bll.Shop.Products.Product.GetCategorised(shopId, itemsPerCategory);
            return prods.Select(p => new Product(p, language)).ToArray();
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("This service allows you to get multiple product by certain filters and to limit the amount of items you get. Filter by categories or merchant groups or just a specific merchant.")]
        public Product[] GetProducts(Shop.SearchFilter filters, SortAndPage sortAndPage)
        {
            //Bll.Process.GenericLog.Log(CommonTypes.LogHistoryType.Other, null,"GetProducts - start" ,"","", "Fail");
            var sf = new Bll.Shop.Products.Product.SearchFilters() { IsActive = true };
            sf.ShopId = filters.ShopId;
            sf.LanguageIso = filters.Language;
            if (!string.IsNullOrEmpty(filters.MerchantNumber)) sf.MerchantId = Bll.Merchants.Merchant.CachedNumbersForDomain()[filters.MerchantNumber];
            sf.PromoOnly = filters.PromoOnly;
            sf.Text = filters.Tags;
            sf.Name = filters.Name;
            sf.Categoryies = filters.Categories == null ? null : filters.Categories.ToList<int>();
            sf.regions = filters.Regions == null ? null : filters.Regions.ToList();
            sf.countries = filters.Countries == null ? null : filters.Countries.ToList();
            sf.includeGlobalRegion = filters.IncludeGlobalRegion;
            if (filters.MerchantGroups != null && filters.MerchantGroups.Length > 0) sf.MerchantGroupID = filters.MerchantGroups.ToList<int>();
            var products = Bll.Shop.Products.Product.Search(sf, sortAndPage.ToSortAndPage());
            var merchants = Bll.Merchants.MerchantPublicInfo.Load(products.Select(p => p.MerchantID).Distinct().ToList(), null);
            var ret = products.Select(p => new Product(p, merchants.Where(m => m.ID == p.MerchantID).SingleOrDefault(), filters.Language, false)).ToList();

            if (SessionOptions != null)
            {
                foreach (var item in ret)
                {
                    string newFileName = Path.GetFileNameWithoutExtension(item.ImageURL) + "_" + SessionOptions.ImageHeight + "x" + SessionOptions.ImageWidth + Path.GetExtension(item.ImageURL);
                    item.ImageURL = Domain.Current.WebServicesUrl + CreateCachedImage(item.ImageURL, item.Merchant.Number, newFileName, SessionOptions.ImageWidth, SessionOptions.ImageHeight, false);
                }
            }
            return ret.ToArray();
        }

        private string CreateCachedImage(string originalFileName, string merchantNumber, string newFileName, int newWidth, int maxHeight, bool OnlyResizeIfWider)
        {
            var pyhiscalCacheFileName = System.IO.Path.Combine(Bll.Accounts.Account.MapPublicPath(merchantNumber, "Cache"), newFileName);
            var virtualCacheFileName = System.IO.Path.Combine(Bll.Accounts.Account.MapPublicVirtualPath(merchantNumber, "Cache"), newFileName);
            if (System.IO.File.Exists(pyhiscalCacheFileName)) return virtualCacheFileName;
            System.Net.WebClient wc = new System.Net.WebClient();
            using (var fs = wc.OpenRead(originalFileName))
            {
                using (var FullsizeImage = System.Drawing.Image.FromStream(fs))
                {
                    if (OnlyResizeIfWider)
                    {
                        if (FullsizeImage.Width <= newWidth)
                        {
                            newWidth = FullsizeImage.Width;
                        }
                    }
                    int NewHeight = FullsizeImage.Height * newWidth / FullsizeImage.Width;
                    if (NewHeight > maxHeight)
                    {
                        // Resize with height instead
                        newWidth = FullsizeImage.Width * maxHeight / FullsizeImage.Height;
                        NewHeight = maxHeight;
                    }

                    using (var NewImage = FullsizeImage.GetThumbnailImage(newWidth, NewHeight, null, IntPtr.Zero))
                        NewImage.Save(pyhiscalCacheFileName);
                    return virtualCacheFileName;
                }
            }
        }

        /*
        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("By sending a specific merchant id and a product id the service will return the next product id available by that merchant � from the response you will be able to use GetProduct to retrieve all of the product info.")]
        public int GetNextProductId(string merchantNumber, int productId)
        {
            int merchantId = 0;
            if (!Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(merchantNumber, out merchantId)) 
                return -1;

            int? id = Bll.Shop.Products.Product.GetNextProductId(merchantId, productId);
            if (id == null)
                return -1;

            return id.Value;
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("By sending a specific merchant id and a product id the service will return the previous product id available by that merchant � from the response you will be able to use GetProduct to retrieve all of the product info.")]
        public int GetPrevProductId(string merchantNumber, int productId)
        {
            int merchantId = 0;
            if (!Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(merchantNumber, out merchantId))
                return -1;

            int? id = Bll.Shop.Products.Product.GetPrevProductId(merchantId, productId);
            if (id == null)
                return -1;

            return id.Value;
        }
        */

        private static string ExtractRcurring(Bll.Shop.Products.Product product, string culture)
        {
            if (culture != null && culture.Trim().Length > 0)
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);

            string recurringString = string.Empty;
            bool hideLast = true;
            string[] series = product.RecurringString.Split(';');
            for (int i = 0; i < series.Length; i++)
            {
                bool isLast = (i == series.Length - 1);
                try
                {
                    var cs = Netpay.Bll.Transactions.Recurring.Series.Parse(series[i]);
                    if (i > 0) recurringString += (isLast && hideLast) ? CommonResources.Resources.Recurring.RecurringLastAnd : CommonResources.Resources.Recurring.RecurringAnd;
                    if (cs.Amount == 0) cs.Amount = product.Price;
                    if (isLast && hideLast)
                        recurringString += string.Format(CommonResources.Resources.Recurring.RecurringTextCont, "", CommonResources.Resources.Recurring.ResourceManager.GetString(cs.IntervalUnit.ToString() + (cs.IntervalCount > 1 ? "s" : "")), cs.IntervalCount > 1 ? cs.IntervalCount.ToString() : "", cs.Amount.ToAmountFormat(product.CurrencyISOCode));
                    else
                        if (cs.Amount == 0)
                        recurringString += string.Format(cs.Charges == 1 ? CommonResources.Resources.Recurring.RecurringSingleNoAmount : CommonResources.Resources.Recurring.RecurringTextNoAmount, cs.Charges.ToString(), CommonResources.Resources.Recurring.ResourceManager.GetString(cs.IntervalUnit.ToString() + (cs.IntervalCount > 1 ? "s" : "")), cs.IntervalCount > 1 ? cs.IntervalCount.ToString() : "");
                    else
                        recurringString += string.Format(cs.Charges == 1 ? CommonResources.Resources.Recurring.RecurringSingle : CommonResources.Resources.Recurring.RecurringText, cs.Charges.ToString(), CommonResources.Resources.Recurring.ResourceManager.GetString(cs.IntervalUnit.ToString() + (cs.IntervalCount > 1 ? "s" : "")), cs.IntervalCount > 1 ? cs.IntervalCount.ToString() : "", cs.Amount.ToAmountFormat(product.CurrencyISOCode));
                }
                catch (Exception e)
                {
                    Logger.Log(e);
                }
            }

            return recurringString;
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("This service call will return all of the product information you are looking for by the language you requested. This will be used in specific product pages both for apps and online � for online pages the service can return the Meta tags Google and other search engines are scanning to allow the product pages to appear in searches. The service will return also all inventory management options and all the merchant details relevant to the product.")]
        public Product GetProduct(string merchantNumber, int itemId, string language = null)
        {
            Product returnShopProduct;
            if (itemId == 0)
            {
                int merchantId = 0;
                if (!Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(merchantNumber, out merchantId)) return null;
                var merchant = Netpay.Bll.Merchants.MerchantPublicInfo.Load(merchantId);
                if (merchant == null) return null;
                returnShopProduct = new Product(merchant);
            }
            else
            {
                var product = Bll.Shop.Products.Product.Load(itemId);
                if (product == null)
                    return null;
                returnShopProduct = new Product(product, null, language, true);

                // get nearest products
                var nearestProducts = product.GetNearestProducts();
                returnShopProduct.NetxProductId = nearestProducts.NextPrductId;
                returnShopProduct.PrevProductId = nearestProducts.PrevProductId;
            }

            if (SessionOptions != null)
            {
                string newFileName = Path.GetFileNameWithoutExtension(returnShopProduct.ImageURL) + "_" + SessionOptions.ImageHeight + "x" + SessionOptions.ImageWidth + Path.GetExtension(returnShopProduct.ImageURL);
                returnShopProduct.ImageURL = Domain.Current.WebServicesUrl + CreateCachedImage(returnShopProduct.ImageURL, returnShopProduct.Merchant.Number, newFileName, SessionOptions.ImageWidth, SessionOptions.ImageHeight, false);
            }

            return returnShopProduct;
        }

        private static string EncodeBasketCookie(string applicationToken, Bll.Shop.Cart.Basket basket)
        {
            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());
            return string.Format("cartId={0}|Signature={1}", HttpUtility.UrlEncode(basket.Identifier.ToString()), (basket.Identifier.ToString() + (appIdentity != null ? appIdentity.ID : 0)).ToSha256());
        }

        public static Bll.Shop.Cart.Basket LoadBasketFromCookie(string applicationToken, string cookie)
        {
            var cookieValues = System.Web.HttpUtility.ParseQueryString(cookie.EmptyIfNull().Replace('|', '&'));
            //var cartWalletId = cookieValues["cartWalletId"].ToNullableInt();
            var cartId = cookieValues["CartId"].ToNullableGuid().GetValueOrDefault();
            Bll.Shop.Cart.Basket basket = null;
            //if (cartWalletId != null) basket = Bll.Shop.Cart.Basket.LoadForCustomer(WebUtils.CurrentDomain.ServiceCredentials, cartWalletId.Value);
            basket = Bll.Shop.Cart.Basket.Load(cartId);
            if (basket == null) return null;
            if (applicationToken != null && EncodeBasketCookie(applicationToken, basket) != cookie) return null; //new WalletCookieResult(ValidationResult.Invalid_Data)
            return basket;
        }

        [ContextMethod(false, false)]
        [OperationContract]
        [WsdlDocumentation("The service will return the active carts for the user (requires login ) � so in case a user logs in from a different device he will be able to see the last products he added to his cart.")]
        public List<Cart> GetActiveCarts()
        {
            //ValidateWalletCredentias(walletCredentials);
            return Bll.Shop.Cart.Basket.LoadActiveForCustomer(Bll.Customers.Customer.Current.ID).Select(b => new Cart(b, ApplicationToken)).ToList();
            //Infrastructure.ObjectContext.Current.Impersonate(Guid.Empty);
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("With the cart id the service will return all of the product still available in the cart and all the relevant info for the cart.")]
        public Cart GetCart(string cookie)
        {
            var basket = LoadBasketFromCookie(ApplicationToken, cookie);
            if (basket == null) return null;
            Cart cart = new Cart(basket, ApplicationToken);
            return cart;
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("This service will allow you to call a number of merchant by a certain filter � you can filter by a group id / merchant status/ application id The service will return all the relevant merchant info to pull more info from other services")]
        public Shop.Merchant[] GetMerchants(Shop.MerchantFilters filters)
        {
            var appIdentity = Bll.ApplicationIdentity.GetIdentity(filters.ApplicationToken.ToNullableGuid().GetValueOrDefault());
            var merchants = Bll.Merchants.MerchantPublicInfo.Search(new Bll.Merchants.MerchantPublicInfo.SearchFilters() { ApplicationIdentityId = (appIdentity != null ? (int?)appIdentity.ID : null), Group = filters.GroupId, Text = filters.Text, Status = filters.MerchantStatus.ToNullableEnum<MerchantStatus>(), HasActiveShops = true }, null);
            return merchants.Take(5).Select(m => new Shop.Merchant(m)).ToArray();
        }

        [ContextMethod(false, false)]
        [OperationContract]
        [WsdlDocumentation("get card by transaction id")]
        public Cart GetCartOfTransaction(int transactionId)
        {
            var basket = Bll.Shop.Cart.Basket.LoadForTransaction(TransactionStatus.Captured, transactionId);
            if (basket == null) return null;
            return new Cart(basket, ApplicationToken);
        }

        [ContextMethod(false, false)]
        [OperationContract]
        [WsdlDocumentation("Get downloads will return only transactions with the product type download � after that you will be able to call the service to return the downloadable file (mp3/mp4/pdf )")]
        public List<CartItem> GetDownloads(SortAndPage sortAndPage)
        {
            var ret = Bll.Shop.Cart.BasketItem.Search(
                new Bll.Shop.Cart.BasketItem.SearchFilters() { CustomerId = new List<int>() { Bll.Customers.Customer.Current.ID }, ProductType = new List<Bll.Shop.Products.ProductType>() { Bll.Shop.Products.ProductType.Download } },
                sortAndPage.ToSortAndPage());
            //var merchants = Bll.Merchants.MerchantPublicInfo.Load(ret.Select(p => p.MerchantId).Distinct().ToList(), null);
            return ret.Select(b => new CartItem(b)).ToList();
        }

        private byte[] SendFile(string fileName, bool asPlainData)
        {
            if (!System.IO.File.Exists(fileName)) ResponseHttpError("File not exist", asPlainData);
            if (!asPlainData) return System.IO.File.ReadAllBytes(fileName);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=\"" + System.IO.Path.GetFileName(fileName) + "\"");
            HttpContext.Current.Response.WriteFile(fileName);
            HttpContext.Current.Response.End();
            return null;
        }

        [OperationContract, ContextMethod(false, false)]
        [WsdlDocumentation("The system enables merchants to create products with the purpose of delivering a digital file � when wanting to access the relevant file you can enter the transaction id to download the attached file.")]
        public byte[] Download(int itemId, bool asPlainData)
        {
            var item = Bll.Shop.Cart.BasketItem.Search(new Bll.Shop.Cart.BasketItem.SearchFilters() { ID = new Range<int?>(itemId) }, null).SingleOrDefault();
            if (item == null) ResponseHttpError("Item not found", asPlainData);
            if (item.Basket.TransPass_id == null) ResponseHttpError("Transaction not completed", asPlainData);
            if (item.Product == null) ResponseHttpError("Item file removed", asPlainData);
            return SendFile(item.Product.MediaPysicalPath, asPlainData);
        }

        [OperationContract, ContextMethod(true, true)]
        [WsdlDocumentation("The system enables merchants to create products with the purpose of delivering a digital file � when wanting to access the relevant file you can enter the transaction id to download the attached file.")]
        public byte[] DownloadUnauthorized(string fileKey, bool asPlainData)
        {
            if (fileKey == null) ResponseHttpError("Wrong filekey", asPlainData);
            var keyParam = fileKey.EmptyIfNull().Split('|');
            if (keyParam.Length != 2) ResponseHttpError("Wrong filekey", asPlainData);
            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            var item = Bll.Shop.Cart.BasketItem.Search(new Bll.Shop.Cart.BasketItem.SearchFilters() { ID = new Range<int?>(keyParam[0].ToNullableInt().GetValueOrDefault()) }, null).SingleOrDefault();
            ObjectContext.Current.StopImpersonate();
            if (item == null) ResponseHttpError("Item not found", asPlainData);
            if (item.GuestDownloadKey != fileKey) ResponseHttpError("Wrong filekey validation", asPlainData);
            if (item.Basket.TransPass_id == null) ResponseHttpError("Transaction not completed", asPlainData);
            if (DateTime.Now.Subtract(item.Basket.CheckoutDate.GetValueOrDefault(DateTime.MinValue)).TotalDays > 7)
                ResponseHttpError("The file available to download up to 7 dyas from purchase", asPlainData);
            return SendFile(item.Product.MediaPysicalPath, asPlainData);
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("The setcart service is used to load the products to the cart along with the relevant information for the payment � after the cookie is set you will be able to getcart with the relevant service.")]
        public string SetCart(Cart cart) /*return cookie*/
        {
            int merchantId = 0;
            Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(cart.MerchantNumber, out merchantId);
            Bll.Shop.Cart.Basket basket = null;
            if (!string.IsNullOrEmpty(cart.Cookie))
            {
                basket = LoadBasketFromCookie(ApplicationToken, cart.Cookie);
                if (basket == null)
                    return null;
            }
            else
            {
                if (Bll.Customers.Customer.Current != null)
                {
                    //ObjectContext.Current.Impersonate(walletCredentials.ToNullableGuid().GetValueOrDefault());
                    basket = Bll.Shop.Cart.Basket.LoadActiveForCustomer(Bll.Customers.Customer.Current.ID, merchantId);
                }
                if (basket == null)
                {
                    basket = new Bll.Shop.Cart.Basket(merchantId, cart.MerchantReference, (Bll.Customers.Customer.Current != null ? (int?)Bll.Customers.Customer.Current.ID : null)) { };
                    basket.CurrencyISOCode = cart.CurrencyIso;
                }
            }
            basket.ShopId = cart.ShopId;
            //basket.CurrencyISOCode = cart.CurrencyIso;
            basket.Installments = cart.Installments;
            var items = basket.BasketItems.ToList();
            basket.RemoveAll();
            foreach (var item in cart.Items)
            {
                Bll.Shop.Cart.BasketItem dataItem = null;
                if (item.ID != 0) dataItem = items.Where(bi => bi.ID == item.ID).SingleOrDefault();
                if (dataItem == null)
                {
                    if (item.ProductId != null) dataItem = new Bll.Shop.Cart.BasketItem(merchantId, item.ProductId.Value, item.ProductStockId, null);
                    else dataItem = new Bll.Shop.Cart.BasketItem(merchantId, item.Name, item.Price, item.CurrencyISOCode, item.MinQuantity, item.MaxQuantity, item.StepQuantity);
                }
                if (dataItem.IsDynamicPrice) dataItem.Price = item.Price;
                dataItem.Quantity = item.Quantity;
                /*
				foreach (var ip in item.ItemProperties) {
					Bll.Shop.Cart.BasketItemProperty propDataItem = null; 
					var prop = Bll.Shop.Products.Property.Load(new List<int> { ip.PropertyID.Value }, true, true);
					if (ip.ID == 0) propDataItem = dataItem.ItemProperties.Where(pdi => pdi.ID == ip.ID).SingleOrDefault();
					if (propDataItem == null) propDataItem = new Bll.Shop.Cart.BasketItemProperty(prop);
					dataItem.ItemProperties.Add(propDataItem);
				}
				*/
                basket.AddItem(dataItem);
            }
            basket.Save();
            items = items.Where(i => !basket.BasketItems.Contains(i)).ToList();
            foreach (var item in items) item.Delete();
            return EncodeBasketCookie(ApplicationToken, basket);
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("Will return all the available categories the merchant has a product in, meaning if only categories with available products will be shown. ")]
        public ProductCategory[] GetMerchantCategories(string merchantNumber, string language)
        {
            int merchantId = 0;
            Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(merchantNumber, out merchantId);

            CultureInfo culture = CultureInfo.GetCultureInfo(language);
            var merchantCategories = Bll.Shop.Products.Category.LoadForMerchant(merchantId);
            var categories = merchantCategories.Select(c => new ProductCategory(c)).ToList();

            // convert lang
            foreach (var category in categories)
            {
                category.Name = CommonResources.Utils.GetResourceValue("ProductCategories", category.Name, culture);
                if (category.SubCategories != null)
                {
                    foreach (var subCategory in category.SubCategories)
                        subCategory.Name = CommonResources.Utils.GetResourceValue("ProductCategories", subCategory.Name, culture);
                }
            }

            return categories.ToArray();
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("This service call will return all of the merchant�s information � can be used to create a merchant profile in an mobile / web app")]
        public Shop.Merchant GetMerchant(string merchantNumber)
        {
            var ret = Bll.Merchants.MerchantPublicInfo.Load(merchantNumber);
            if (ret == null) return null;
            return new Shop.Merchant(ret);
        }

        [OperationContract]
        [WsdlDocumentation("In the control panel each merchant can create an about text, terms and conditions, privacy policy � once the merchant created such text you can call the service with the tags ABOUT.HTML / TERMS.HTML / PRIVACY.HTML � don�t forget � the merchant can create the text in multiple languages so you have to add the language attribute too.")]
        public string GetMerchantContent(string merchantNumber, string language, string contentName)
        {
            CommonTypes.Language lang = getLanguage(language);
            if (lang == CommonTypes.Language.Unknown)
                lang = CommonTypes.Language.English;
            int? accountId = Bll.Accounts.Account.AccountIDByNumber(Bll.Accounts.AccountType.Merchant, merchantNumber);
            if (!accountId.HasValue)
                return null;

            string content = Bll.Merchants.Merchant.GetContent(accountId.Value, lang, contentName);
            if (string.IsNullOrEmpty(content))
                content = Bll.Merchants.Merchant.GetContent(accountId.Value, CommonTypes.Language.English, contentName);
            if (string.IsNullOrEmpty(content))
            {
                string basePath = Domain.Current.MapPrivateDataPath(@"Site Content\Shops\");
                if (!Directory.Exists(basePath))
                    Directory.CreateDirectory(basePath);
                string fileName = Path.GetFileNameWithoutExtension(contentName) + "_" + Bll.International.Language.Get(lang).IsoCode2 + Path.GetExtension(contentName);
                string fileFullName = basePath + fileName;
                if (File.Exists(fileFullName))
                    content = File.ReadAllText(fileFullName);

                if (string.IsNullOrEmpty(content))
                {
                    fileName = Path.GetFileNameWithoutExtension(contentName) + "_EN" + Path.GetExtension(contentName);
                    fileFullName = basePath + fileName;
                    if (File.Exists(fileFullName))
                        content = File.ReadAllText(fileFullName);
                }
            }

            if (string.IsNullOrEmpty(content))
                content = "Couldn't find content";

            return content;
        }

        [OperationContract]
        [WsdlDocumentation("This service will enable you to send a merchant a contact email from a website or an app. The service will use the smtp setting in the app identity and will send the email with the user�s info as provided to the service.")]
        public bool SendMerchantContactEmail(string merchantNumber, string from, string subject, string body)
		{
            try
            {
                Netpay.Bll.Merchants.Merchant merchant = Netpay.Bll.Merchants.Merchant.Load(merchantNumber);
                if (merchant == null)
                    return false;

                MailMessage message = new MailMessage();
                message.From = new MailAddress(from.Sanitize());
                message.To.Add(new MailAddress(merchant.ContactEmail, merchant.Name));
                message.Subject = subject.Sanitize();
                message.Body = body.Sanitize();
                message.IsBodyHtml = false;

                Netpay.Infrastructure.Email.SmtpClient.Send(message);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return false;
            }

            return true;
		}
    }
}
