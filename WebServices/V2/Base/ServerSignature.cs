﻿using System;
using System.Collections.Generic;
using System.Web;
using Netpay.Infrastructure;
using System.ServiceModel.Channels;
using System.ServiceModel;
using System.Linq;

namespace Netpay.WebServices.V2
{
    public class ServerSignatureHeader : SignatureHeader
    {
        private ServerSignatureHeader(string mode, string type, string privateValue, object clacValue)
            : base(mode, type, privateValue, clacValue)
        { }
        public static SignatureHeader Current
        {
            get
            {
                if (System.Web.HttpContext.Current == null)
                    return null;

                var ret = System.Web.HttpContext.Current.Items[SignatureHeader.HEADER_KEY] as SignatureHeader;
                if (ret == null)
                {
                    try {
                        Current = ret = FromHeader(System.Web.HttpContext.Current.Request.Headers[HEADER_KEY]);
                    } catch { return null; }
                    //if(ret == null) ret = new SignatureHeader(DefaultSingatureMode, DefaultSingatureType, null, null);
                }
                return ret;
            }
            set { System.Web.HttpContext.Current.Items[SignatureHeader.HEADER_KEY] = value; }
        }

        public static System.IO.Stream RequestByteStream
        {
            get { return System.Web.HttpContext.Current.Items[SignatureHeader.HEADER_KEY + "_STREAM"] as System.IO.Stream; }
            set { System.Web.HttpContext.Current.Items[SignatureHeader.HEADER_KEY + "_STREAM"] = value; }
        }
    }

    public class ServerSignature : System.ServiceModel.Configuration.BehaviorExtensionElement, System.ServiceModel.Description.IServiceBehavior, System.ServiceModel.Dispatcher.IParameterInspector
    {
        public static string DefaultSingatureType { get; set; } = "SHA256";
        public static string DefaultSingatureMode { get; set; } = "values";

        public ServerSignature() : base() { }

        public override Type BehaviorType { get { return typeof(ServerSignature); } }
        protected override object CreateBehavior() { return this; }

        public void AddBindingParameters(System.ServiceModel.Description.ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<System.ServiceModel.Description.ServiceEndpoint> endpoints, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(System.ServiceModel.Description.ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
            foreach (System.ServiceModel.Dispatcher.ChannelDispatcher channel in serviceHostBase.ChannelDispatchers) {
                if (channel.BindingName == "ServiceMetadataBehaviorHttpGetBinding") continue;
                foreach (var endpoint in channel.Endpoints)
                {
                    foreach (var operation in endpoint.DispatchRuntime.Operations)
                        operation.ParameterInspectors.Add(this);
                }
            }
        }

        public void Validate(System.ServiceModel.Description.ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
            //do default registration
            serviceDescription.Endpoints.Clear();
            //var ep = serviceHostBase.AddServiceEndpoint(serviceDescription.ConfigurationName, new SignatureBinding(new WebMessageEncodingBindingElement()), "js");
            //serviceHostBase.Description.Behaviors.Add(new System.ServiceModel.Discovery.ServiceDiscoveryBehavior());

            var ep = serviceHostBase.AddServiceEndpoint(serviceDescription.ConfigurationName, new SignatureBinding(new WebHttpBinding() { }), "");
            ep.Behaviors.Add(new System.ServiceModel.Description.WebScriptEnablingBehavior() {  });
            if (serviceHostBase.BaseAddresses.Any(v => v.Scheme.ToUpper() == "HTTPS")) {
                ep = serviceHostBase.AddServiceEndpoint(serviceDescription.ConfigurationName, new SignatureBinding(new WebHttpBinding() { Security = new WebHttpSecurity() { Mode = WebHttpSecurityMode.Transport } }), "");
                ep.Behaviors.Add(new System.ServiceModel.Description.WebScriptEnablingBehavior() { });
            }


            //ep.Behaviors.Add(new System.ServiceModel.Description.WebHttpBehavior() { DefaultBodyStyle = System.ServiceModel.Web.WebMessageBodyStyle.Wrapped, DefaultOutgoingResponseFormat = System.ServiceModel.Web.WebMessageFormat.Json, AutomaticFormatSelectionEnabled = false, FaultExceptionEnabled = true });
            var ep3 = serviceHostBase.AddServiceEndpoint(serviceDescription.ConfigurationName, new SignatureBinding(new WSHttpBinding() { Security = new WSHttpSecurity() { Mode = SecurityMode.None, Transport = new HttpTransportSecurity() { ClientCredentialType = HttpClientCredentialType.None }, Message = new NonDualMessageSecurityOverHttp() { NegotiateServiceCredential = false } }, ReliableSession = new OptionalReliableSession() { Enabled = false } }), "ws");
            if (serviceHostBase.BaseAddresses.Any(v=> v.Scheme.ToUpper() == "HTTPS")) { 
                var ep4 = serviceHostBase.AddServiceEndpoint(serviceDescription.ConfigurationName, new SignatureBinding(new WSHttpBinding() { Security = new WSHttpSecurity() { Mode = SecurityMode.Transport, Transport= new HttpTransportSecurity() { ClientCredentialType = HttpClientCredentialType.None },  Message = new NonDualMessageSecurityOverHttp() { NegotiateServiceCredential = false } }, ReliableSession = new OptionalReliableSession() { Enabled = false } }), "ws");
            }
            Logger.Log(LogSeverity.Info, LogTag.WebService, "ServerSignature Begavior added:" + serviceDescription.Name, string.Join(", ", serviceHostBase.BaseAddresses.Select(v => v.ToString())));

            //var ep3 = serviceHostBase.AddServiceEndpoint(serviceDescription.ConfigurationName, new SignatureBinding(new BasicHttpBinding() { Security = new BasicHttpSecurity() { Mode = BasicHttpSecurityMode.None } }), "ws");
            //serviceHostBase.AddServiceEndpoint(new System.ServiceModel.Discovery.UdpDiscoveryEndpoint() {  });

            //var ep2 = serviceHostBase.AddServiceEndpoint(System.ServiceModel.Description.ServiceMetadataBehavior.MexContractName, System.ServiceModel.Description.MetadataExchangeBindings.CreateMexHttpBinding(), "mex");
            //var ep2 = new System.ServiceModel.Description.ServiceMetadataEndpoint(new EndpointAddress(serviceHostBase.BaseAddresses.First() + "helpx"));
            //serviceHostBase.AddServiceEndpoint(ep2);
            //ep.Behaviors.Add(new System.ServiceModel.Description.ServiceMetadataBehavior() { HttpGetEnabled = true });
            //var ep2 = serviceHostBase.AddServiceEndpoint(serviceDescription.ConfigurationName, , "mex");
            /*
            var ep2 = serviceHostBase.AddServiceEndpoint(serviceDescription.ConfigurationName, new WebHttpBinding(), "");
            ep2.Behaviors.Add(new System.ServiceModel.Description.WebHttpBehavior() {
                DefaultBodyStyle = System.ServiceModel.Web.WebMessageBodyStyle.WrappedRequest,
                //DefaultBodyStyle = System.ServiceModel.Web.WebMessageBodyStyle.Bare,
                DefaultOutgoingRequestFormat = System.ServiceModel.Web.WebMessageFormat.Json,
                DefaultOutgoingResponseFormat = System.ServiceModel.Web.WebMessageFormat.Json,
                AutomaticFormatSelectionEnabled = true,
                HelpEnabled = true,
                FaultExceptionEnabled = true,
            });
            */
        }

        public object BeforeCall(string operationName, object[] inputs)
        {
            var baseService = OperationContext.Current.InstanceContext.GetServiceInstance() as BaseService;
            if (baseService == null) return null;
            var sign = ServerSignatureHeader.Current;
            bool shouldChecksignature = baseService.SetContextMethod(operationName); //first set context
            if (shouldChecksignature) {
                bool headerOk = (sign != null) && baseService.IsSignatureTypeAccepted(sign.Type);
                if (!headerOk) sign = ServerSignatureHeader.Current = new SignatureHeader(sign != null ? sign.Mode : SignatureHeader.Mode_Values, SignatureHeader.Type_Default, null, null);
                sign.PrivateKey = baseService.SignatureKey;
                switch (sign.Mode) {
                    case SignatureHeader.Mode_Values:
                        if (!sign.Validate(inputs)) baseService.ThrowHttpException(403, "server wrong signature");
                        break;
                    case SignatureHeader.Mode_Bytes:
                        if (ServerSignatureHeader.RequestByteStream == null) ServerSignatureHeader.RequestByteStream = new System.IO.MemoryStream();
                        if (!sign.Validate(ServerSignatureHeader.RequestByteStream)) baseService.ThrowHttpException(403, "server wrong signature");
                        break;
                    default:
                        baseService.ThrowHttpException(403, "server unknown signature mode");
                        break;
                }
                if (headerOk) return sign;
                baseService.ThrowHttpException(403, "server signature type not supported");
            }
            return null;
        }

        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {
            var baseService = OperationContext.Current.InstanceContext.GetServiceInstance() as BaseService;
            if (baseService == null) return;
            var sign = ServerSignatureHeader.Current;
            if (sign == null) sign = ServerSignatureHeader.Current = new SignatureHeader(DefaultSingatureMode, DefaultSingatureType, null, null);
            sign.PrivateKey = baseService.SignatureKey;
            if (sign.Mode == SignatureHeader.Mode_Values) { 
                sign.SetValue(returnValue);
                System.Web.HttpContext.Current.Response.Headers[SignatureHeader.HEADER_KEY] = sign.ToString();
            }
            //OperationContext.Current.IncomingMessageProperties.Add(SignatureHeader.HEADER_KEY + "_OUT", sign.ToString());
        }

    }
}