﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Web;

namespace Netpay.WebServices.V2
{
    public class SignatureBinding : CustomBinding
    {
        public SignatureBinding(System.ServiceModel.Channels.Binding bindingElement)
            : base(bindingElement)
        {
            
        }

        public override BindingElementCollection CreateBindingElements()
        {
            var elements = base.CreateBindingElements();
            var bndElement = elements.Where(v => v is System.ServiceModel.Channels.MessageEncodingBindingElement).SingleOrDefault() as System.ServiceModel.Channels.MessageEncodingBindingElement;
            var bIndex = elements.IndexOf(bndElement);
            elements.RemoveAt(bIndex);
            elements.Insert(bIndex, new MessageEncodingBindingElement(bndElement));
            return elements;
        }

        class MessageEncodingBindingElement : System.ServiceModel.Channels.MessageEncodingBindingElement /*, IWmiInstanceProvider*/
        {
            System.ServiceModel.Channels.MessageEncodingBindingElement innerBindingElement;
            public MessageEncodingBindingElement(System.ServiceModel.Channels.MessageEncodingBindingElement bindingElement)
            {
                innerBindingElement = bindingElement;
            }
            public override System.ServiceModel.Channels.MessageVersion MessageVersion { get { return innerBindingElement.MessageVersion; } set { innerBindingElement.MessageVersion = value; } }
            public override System.ServiceModel.Channels.BindingElement Clone() { return new MessageEncodingBindingElement(innerBindingElement.Clone() as System.ServiceModel.Channels.MessageEncodingBindingElement); }
            public override System.ServiceModel.Channels.MessageEncoderFactory CreateMessageEncoderFactory() { return new MessageEncoderFactory(innerBindingElement.CreateMessageEncoderFactory()); }
            public override bool CanBuildChannelListener<TChannel>(System.ServiceModel.Channels.BindingContext context) { return innerBindingElement.CanBuildChannelListener< TChannel>(context); }
            public override System.ServiceModel.Channels.IChannelListener<TChannel> BuildChannelListener<TChannel>(System.ServiceModel.Channels.BindingContext context)
            {
                context.BindingParameters.Add(this);
                return context.BuildInnerChannelListener<TChannel>();
            }
        }

        class MessageEncoderFactory : System.ServiceModel.Channels.MessageEncoderFactory
        {
            private System.ServiceModel.Channels.MessageEncoderFactory _innerFactory;
            private System.ServiceModel.Channels.MessageEncoder _encoder;
            public MessageEncoderFactory(System.ServiceModel.Channels.MessageEncoderFactory innerFactory) { _innerFactory = innerFactory; }
            public override System.ServiceModel.Channels.MessageVersion MessageVersion { get { return _innerFactory.MessageVersion; } }
            public override System.ServiceModel.Channels.MessageEncoder Encoder
            {
                get
                {
                    if (_encoder == null) _encoder = new MessageEncoder(_innerFactory.Encoder);
                    return _encoder;
                }
            }
        }

        class MessageEncoder : System.ServiceModel.Channels.MessageEncoder
        {
            private System.ServiceModel.Channels.MessageEncoder innerEncoder;
            public MessageEncoder(System.ServiceModel.Channels.MessageEncoder inner) { innerEncoder = inner; }
            public override T GetProperty<T>() { return innerEncoder.GetProperty<T>(); }
            public override bool IsContentTypeSupported(string contentType) { return innerEncoder.IsContentTypeSupported(contentType); }
            public override string ContentType { get { return innerEncoder.ContentType; } }
            public override string MediaType { get { return innerEncoder.MediaType; } }
            public override System.ServiceModel.Channels.MessageVersion MessageVersion { get { return innerEncoder.MessageVersion; } }

            public override System.ServiceModel.Channels.Message ReadMessage(ArraySegment<byte> buffer, System.ServiceModel.Channels.BufferManager bufferManager, string contentType)
            {
                if (ServerSignatureHeader.Current != null && ServerSignatureHeader.Current.Mode == SignatureHeader.Mode_Bytes)
                    ServerSignatureHeader.RequestByteStream = new System.IO.MemoryStream(buffer.Array, 0, buffer.Count);
                return innerEncoder.ReadMessage(buffer, bufferManager, contentType);
            }

            public override System.ServiceModel.Channels.Message ReadMessage(System.IO.Stream stream, int maxSizeOfHeaders, string contentType)
            {
                if (ServerSignatureHeader.Current != null && ServerSignatureHeader.Current.Mode == SignatureHeader.Mode_Bytes)
                    ServerSignatureHeader.RequestByteStream = stream;
                return innerEncoder.ReadMessage(stream, maxSizeOfHeaders, contentType);
            }

            public override ArraySegment<byte> WriteMessage(System.ServiceModel.Channels.Message message, int maxMessageSize, System.ServiceModel.Channels.BufferManager bufferManager, int messageOffset)
            {
                var ret = innerEncoder.WriteMessage(message, maxMessageSize, bufferManager, messageOffset);
                if (ServerSignatureHeader.Current != null && ServerSignatureHeader.Current.Mode == SignatureHeader.Mode_Bytes) {
                    ServerSignatureHeader.Current.SetValue(ret.Array);
                    System.Web.HttpContext.Current.Response.Headers[ServerSignatureHeader.HEADER_KEY] = ServerSignatureHeader.Current.ToString();
                }
                return ret;
            }

            public override void WriteMessage(System.ServiceModel.Channels.Message message, System.IO.Stream stream)
            {
                innerEncoder.WriteMessage(message, stream);
            }
        }
    }
}