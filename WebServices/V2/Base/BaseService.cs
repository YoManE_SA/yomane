﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System.Runtime.Serialization;

[assembly: ContractNamespace("WebServices", ClrNamespace = "Netpay.WebServices.V2")]
namespace Netpay.WebServices.V2
{
    #region Global Types

    public class ServiceResult
    {
        public bool IsSuccess { get; set; }
        public int Code { get; set; }
        public string Key { get; set; }
        public string Message { get; set; }
        public string Number { get; set; }
        public ServiceResult() { }
        internal ServiceResult(ValidationResult result, string message = null, string number = null)
        {
            IsSuccess = (result == ValidationResult.Success);
            Code = (int)result;
            Key = result.ToString();
            if (message == null) message = Infrastructure.ErrorText.GetErrorMessage(null, Code);
            Message = message;
            if (string.IsNullOrEmpty(Message)) Message = string.Format("Error: {0} - {1}", Code, Key);
        }
    }

    public class ServiceMultiResult : ServiceResult
    {
        internal ServiceMultiResult(ValidationResult result) : base(result) { }
        public ServiceMultiResult() { }
        public string[] RefNumbers { get; set; }
        public int? RecordNumber { get; set; }
    }

    public class ServiceListItem
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public virtual string Icon { get; set; }
    }

    public class SortAndPage
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        internal Infrastructure.SortAndPage ToSortAndPage(string sortKey = null, bool sortDece = false)
        {
            return new Infrastructure.SortAndPage(PageNumber - 1, PageSize, sortKey, sortDece);
        }
    }

    public class ServiceAddress
    {
        public ServiceAddress() { }

        internal ServiceAddress(Bll.IAddress cs)
        {
            if (cs == null)
                return;

            AddressLine1 = cs.AddressLine1;
            AddressLine2 = cs.AddressLine2;
            City = cs.City;
            PostalCode = cs.PostalCode;
            StateIso = cs.StateISOCode;
            CountryIso = cs.CountryISOCode;
        }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string StateIso { get; set; }
        public string CountryIso { get; set; }

        #region Save And Load
        internal void Save(Bll.Accounts.AccountAddress value)
        {
            value.AddressLine1 = AddressLine1;
            value.AddressLine2 = AddressLine2;
            value.City = City;
            value.PostalCode = PostalCode;
            value.StateISOCode = StateIso;
            value.CountryISOCode = CountryIso;
        }
        #endregion
    }
    #endregion

    [System.ServiceModel.ServiceBehavior(AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
    [System.ServiceModel.Activation.AspNetCompatibilityRequirements(RequirementsMode = System.ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)]
    public abstract class BaseService
    {
        public const string ApplicationToken_Key = "applicationToken";

        public class KeyValue<TKey, TValue>
        {
            public TKey Key { get; set; }
            public TValue Value { get; set; }
            public KeyValue() { }
            public KeyValue(TKey key, TValue value) { Key = key; Value = value; }
        }

        public class ContextMethod : System.Attribute
        {
            public bool IsPublicMethod { get; set; }
            public bool IgnoreSignature { get; set; }
            public bool RequireDeviceId { get; set; }

            public ContextMethod(bool isPublic, bool ignoreSignature) : this(isPublic, ignoreSignature, false)
            {

            }

            public ContextMethod(bool isPublic, bool ignoreSignature, bool requireDeviceId)
            {
                IsPublicMethod = isPublic;
                IgnoreSignature = ignoreSignature;
                RequireDeviceId = requireDeviceId;
            }
        }

        protected bool RequireSession { get; private set; }
        protected bool SetContextCompleted { get; private set; }
        protected UserRole[] AllowedRoles { get; private set; }
        protected Bll.ApplicationIdentity AppIdentity { get; set; }
        protected Login LoggedIn { get; set; }
        public string SignatureKey { get; set; }

        protected BaseService(bool requireSession, UserRole[] allowedRoles = null) 
        {
            RequireSession = requireSession;
            AllowedRoles = allowedRoles;
            WebServices.Code.Utils.RegisterEmailTemplates();
        }

        protected HttpContext Context { get { return HttpContext.Current; } }
        public string ApplicationToken { get { return Netpay.Web.WebUtils.GetGlobalParameter(ApplicationToken_Key); } }
        protected System.Web.SessionState.HttpSessionState Session { get { return HttpContext.Current.Session; } }

        public virtual bool IsSignatureTypeAccepted(string value)
        {
            return (new string[] { "MD5", "SHA-256", "SHA256" }).Contains(value.EmptyIfNull());
        }

        protected CommonTypes.Language getLanguage(string culture)
        {
            var ret = Bll.International.Language.Cache.Where(l => l.Culture.ToLower() == culture.ToLower()).SingleOrDefault();
            if (ret != null) return ret.EnumValue;
            return CommonTypes.Language.Unknown;
        }

        protected virtual void SetContext()
        {
            LoggedIn = Infrastructure.Security.Login.Current;
            if (LoggedIn != null) {
                if (LoggedIn.Items.ContainsKey("Session.ApplicationIdentity"))
                    AppIdentity = (LoggedIn.Items["Session.ApplicationIdentity"] as Netpay.Bll.ApplicationIdentity);
            }
            if(AppIdentity == null) {
                var appToken = ApplicationToken.ToNullableGuid();
                if (appToken.HasValue) AppIdentity = Bll.ApplicationIdentity.GetIdentity(appToken.Value);
            }
            if (AppIdentity != null) SignatureKey = AppIdentity.HashKey.NullIfEmpty();
            if (string.IsNullOrEmpty(SignatureKey) && Bll.Accounts.Account.Current != null)
                SignatureKey = Bll.Accounts.Account.Current.HashKey.NullIfEmpty();
        }

        internal bool SetContextMethod(string methodName)
        {
            SetContext();
            ContextMethod contextMethod = GetType().GetMethod(methodName).GetCustomAttributes(typeof(ContextMethod), true).FirstOrDefault() as ContextMethod;
            if (contextMethod == null)
                contextMethod = new ContextMethod(false, !RequireSession);

            if (!contextMethod.IsPublicMethod)
            {
                ValidateCredentials();
                if (contextMethod.RequireDeviceId)
                {
                    if (Netpay.Bll.Accounts.MobileDevice.Current == null)
                        ThrowHttpException((int)System.Net.HttpStatusCode.Forbidden, "Device id required");
                }
            }

            //if (!contextMethod.IgnoreSignature) validateSignature();
            SetContextCompleted = true;
            return !contextMethod.IgnoreSignature;
        }

        public void ThrowHttpException(int code, string message)
        {
            /*
            var response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Buffer = false;
            response.BufferOutput = false;
            response.TrySkipIisCustomErrors = true;
            response.StatusCode = code;
            response.StatusDescription = message;
            response.Write(message);
            response.Flush();
            response.Close();
            try { response.End(); } catch { }
            throw new HttpException(code, message);
            */
            var curResponse = System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse;
            var sign = ServerSignatureHeader.Current;
            if (!Application.IsProduction && sign != null && sign.LastData != null) {
                curResponse.Headers["Debug-" + ServerSignatureHeader.HEADER_KEY + "-Data"] = System.Convert.ToBase64String(sign.LastData);
                sign.SetValue(sign.LastData);
                curResponse.Headers["Debug-" + ServerSignatureHeader.HEADER_KEY] = sign.ToString();
            }
            curResponse.StatusCode = (System.Net.HttpStatusCode)code;
            throw new System.ServiceModel.Web.WebFaultException<string>(message, (System.Net.HttpStatusCode)code);
        }

        protected virtual void ValidateCredentials()
        {
            if (!RequireSession)
                return;

            bool isOk = (LoggedIn != null) && (AllowedRoles == null || AllowedRoles.Any(r => LoggedIn.IsInRole(r)));
            if (isOk)
                return;

            ThrowHttpException((int)System.Net.HttpStatusCode.Forbidden, "Not logged in");
        }
        

	}

}