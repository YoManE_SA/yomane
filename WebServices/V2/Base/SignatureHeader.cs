﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.WebServices.V2
{
    public class SignatureHeader
    {
        public const string HEADER_KEY = "Signature";
        public const string Mode_Values = "values";
        public const string Mode_Bytes = "bytes";

        public const string Type_Default = "SHA256";

        public string Mode { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string PrivateKey { get; set; }
        public byte[] LastData { get; set; }

        private static List<string> GetObjectValues(object obj)
        {
            if (obj == null) return null;

            List<string> values = new List<string>();
            Type type = obj.GetType();
            if (type.IsArray)
            {
                foreach (var value in ((System.Array)obj))
                {
                    List<string> objValues = GetObjectValues(value);
                    if (objValues != null)
                        values.AddRange(objValues);
                }
            }
            else if (type.IsClass && (type != typeof(string)))
            {
                System.Reflection.PropertyInfo[] fields = obj.GetType().GetProperties().OrderBy(v=> v.Name).ToArray();
                foreach (var field in fields)
                {
                    try
                    {
                        object value = field.GetValue(obj, null);
                        List<string> objValues = GetObjectValues(value);
                        if (objValues != null)
                            values.AddRange(objValues);
                    }
                    catch { throw new Exception(field.Name); }
                }
            }
            else if (type == typeof(bool)) values.Add(obj.ToString().ToLower());
            else if (type == typeof(DateTime)) values.Add(((DateTime)obj).ToString("dd/MM/yyyy HH:mm:ss"));
            else if (type == typeof(decimal)) values.Add(((decimal)obj).ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture));
            //else if (type == typeof(double)) values.Add(((double)obj).ToString(System.Globalization.CultureInfo.InvariantCulture));
            //else if (type == typeof(float)) values.Add(((float)obj).ToString(System.Globalization.CultureInfo.InvariantCulture));
            else values.Add(obj.ToString());

            return values;
        }

        public static string GetSignData(object obj)
        {
            if (obj == null) return null;
            List<string> values = GetObjectValues(obj);
            return string.Join("", values);
        }

        private string CalcSignature(object obj)
        {
            var signEngine = System.Security.Cryptography.HashAlgorithm.Create(Type);
            if (obj is System.IO.Stream) {
                var stream = obj as System.IO.Stream;
                obj = new byte[stream.Length];
                stream.Read((byte[])obj, 0, (int)stream.Length);
            }
            byte[] srcBytes;
            if (obj is byte[]) {
                srcBytes = obj as byte[];
            } else {
                string valuesStr = GetSignData(obj);
                if (string.IsNullOrEmpty(valuesStr)) srcBytes = new byte[0];
                else srcBytes = System.Text.Encoding.UTF8.GetBytes(valuesStr);
            }
            LastData = srcBytes;
            if (PrivateKey == null) PrivateKey = "";
            byte[] keyBytes = System.Text.Encoding.UTF8.GetBytes(PrivateKey);
            Array.Resize(ref srcBytes, srcBytes.Length + keyBytes.Length);
            Array.Copy(keyBytes, 0, srcBytes, srcBytes.Length - keyBytes.Length, keyBytes.Length);

            return System.Convert.ToBase64String(signEngine.ComputeHash(srcBytes));
        }

        public static SignatureHeader FromHeader(string value)
        {
            if (value == null) return null;
            var values = value.Split(new char[] { '-', ',' }, 3).Select(v => v.Trim()).ToList();
            if (values.Count < 3) throw new Exception("signature header format is invalid shoud be " + HEADER_KEY + ": mode-type, value");
            return new SignatureHeader(values[0], values[1], null, null) { Value = values[2] };
        }

        public SignatureHeader(string mode, string type, string privateKey, object clacValue)
        {
            Mode = mode;
            Type = type;
            PrivateKey = privateKey;
            if (clacValue != null) Value = CalcSignature(clacValue);
        }

        public bool Validate(object clacValue)
        {
            var newValue = CalcSignature(clacValue);
            return (newValue == Value);
        }

        public void SetValue(object clacValue)
        {
            Value = CalcSignature(clacValue);
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}, {2}", Mode, Type, Value);
        }
    }
}