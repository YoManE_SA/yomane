﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Netpay.WebServices.V2
{
    public class ClientSignature : System.ServiceModel.Configuration.BehaviorExtensionElement, System.ServiceModel.Description.IEndpointBehavior, System.ServiceModel.Dispatcher.IParameterInspector, System.ServiceModel.Dispatcher.IClientMessageInspector
    {
        private string signData = null; //not thread safe
        public const string APPLICATIONTOKEN_HEADERNAME = "applicationToken";
        public static string CredentialsTokenHeaderName = "credentialsToken";
        public static string SingatureType { get; set; } = "SHA256";
        public static string SingatureMode { get; set; } = "values";
        public static string ServiceBaseAddress { get; set; }

        private static string DomainName
        {
            get
            {
                if (HttpContext.Current == null) return string.Empty;
                string host = HttpContext.Current.Request.Url.Host;
                int tmp;
                var subDomains = host.Split('.');
                if (subDomains.Length < 3 || int.TryParse(subDomains[0], out tmp)) return host;
                return string.Join(".", subDomains, subDomains.Length - 2, 2);
            }
        }

        public static string PrivateKey { get; set; }
        public static string ApplicationToken { get; set; }
        private static string _credentialsToken;
        public static string CredentialsToken
        {
            get
            {
                if (System.Web.HttpContext.Current != null)
                    return (string)System.Web.HttpContext.Current.Items["Netpay-WebServices-V2-CredentialsToken"];
                return _credentialsToken;
            }
            set
            {
                if (System.Web.HttpContext.Current != null)
                    System.Web.HttpContext.Current.Items["Netpay-WebServices-V2-CredentialsToken"] = value;
                else _credentialsToken = value;
            }
        }

        static ClientSignature()
        {
            ServiceBaseAddress = System.Configuration.ConfigurationManager.AppSettings["Netpay-WebServices-V2-Address"];
            if (string.IsNullOrEmpty(ServiceBaseAddress)) ServiceBaseAddress = "http://webservices.netpay-intl.com/v2";
            ServiceBaseAddress = ServiceBaseAddress.TrimEnd('/');
            ApplicationToken = System.Configuration.ConfigurationManager.AppSettings["Netpay-WebServices-V2-AppToken"];
            PrivateKey = System.Configuration.ConfigurationManager.AppSettings["Netpay-WebServices-V2-AppToken-Hash"];
        }

        public ClientSignature() { }
        private string remapServiceAddress(string address)
        {
            var lPos = address.IndexOf("/v2/", StringComparison.InvariantCultureIgnoreCase);
            if (lPos == -1) return address;
            return ServiceBaseAddress.Replace("{domain}", DomainName) + address.Substring(lPos + 3);
        }

        public override Type BehaviorType { get { return typeof(ClientSignature); } }
        protected override object CreateBehavior() { return this; }
        public void Validate(ServiceEndpoint endpoint) { }
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { }
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { }
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            endpoint.Address = new System.ServiceModel.EndpointAddress(remapServiceAddress(endpoint.Address.Uri.ToString()));
            //System.Web.HttpContext.Current.Response.Write(endpoint.Address.Uri.ToString() + "<br/>");
            //System.Web.HttpContext.Current.Response.Flush();
            clientRuntime.MessageInspectors.Add(this);
            foreach (var operation in clientRuntime.Operations)
                operation.ParameterInspectors.Add(this);
        }

        public object BeforeCall(string operationName, object[] inputs)
        {
            signData = SignatureHeader.GetSignData(inputs);
            return null;
        }

        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {
            var sign = SignatureHeader.FromHeader(signData);
            if (sign == null) return;
            sign.PrivateKey = PrivateKey;
            if (!sign.Validate(returnValue)) throw new Exception("client wrong signature validation");
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            object httpRequestMessageObject;
            if (!request.Properties.TryGetValue(HttpRequestMessageProperty.Name, out httpRequestMessageObject))
                request.Properties.Add(HttpRequestMessageProperty.Name, httpRequestMessageObject = new HttpRequestMessageProperty());

            var httpRequestMessage = httpRequestMessageObject as HttpRequestMessageProperty;
            if (!string.IsNullOrEmpty(CredentialsToken)) httpRequestMessage.Headers[CredentialsTokenHeaderName] = CredentialsToken;
            if (!string.IsNullOrEmpty(ApplicationToken)) httpRequestMessage.Headers[APPLICATIONTOKEN_HEADERNAME] = ApplicationToken;
            httpRequestMessage.Headers[SignatureHeader.HEADER_KEY] = (new SignatureHeader(SingatureMode, SingatureType, PrivateKey, signData)).ToString();
            return null;
        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            object httpResponseMessageObject;
            signData = null;
            if (reply.Properties.TryGetValue(HttpResponseMessageProperty.Name, out httpResponseMessageObject))
                signData = (httpResponseMessageObject as HttpResponseMessageProperty).Headers.Get(SignatureHeader.HEADER_KEY);
        }

    }
}