﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ComponentModel;
using System.Web;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System.Linq;
using Netpay.Bll.ThirdParty;
using System.Collections.Generic;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// base account services
    /// </summary>
    [System.ServiceModel.ServiceBehavior(AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
    [System.ServiceModel.ServiceContract(Namespace = "http://netpay-intl.com/", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class Multicat : V2.BaseService
    {
        #region Service Classes
        public class LoginOptions
        {
            public bool setCookie { get; set; }
            public string applicationToken { get; set; }
            public string appName { get; set; }
            public string deviceId { get; set; }
            public string pushToken { get; set; }
            public bool? excludeDataImages { get; set; }
            public UserRole? userRole { get; set; }
        }

        public class LoginResult : ServiceResult
        {
            public LoginResult() { }
            public LoginResult(ValidationResult result) : base(result) { LastLogin = new DateTime(1970, 1, 1); }
            public string CredentialsToken { set; get; }
            public string CredentialsHeaderName { get; set; }
            public bool VersionUpdateRequired { get; set; }
            public DateTime LastLogin { set; get; }
            public bool IsFirstLogin { set; get; }

            public bool IsDeviceRegistrationRequired { get; set; }
            public bool IsDeviceActivated { set; get; }
            public bool IsDeviceRegistered { set; get; }
            public bool IsDeviceBlocked { set; get; }

            public string EncodedCookie { set; get; }
        }

        public class CookieResult : ServiceResult
        {
            internal CookieResult(ValidationResult result) : base(result) { }
            public CookieResult() { }
            public string FullName { get; set; }
            public string Email { get; set; }
            public byte[] ProfilePicture { get; set; }
        }


        #endregion

        public Multicat() : base(false, new UserRole[] { UserRole.Admin }) { }


        #region Login


        [OperationContract, WsdlDocumentation(""), ContextMethod(true, true)]
        public LoginResult Login(string email, string userName, string password, LoginOptions options)
        {
            if (email == null || password == null)
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (email.Trim() == string.Empty || password.Trim() == string.Empty)
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (!email.IsEmail())
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (options.userRole == UserRole.Customer && string.IsNullOrEmpty(options.applicationToken))
                return new LoginResult(ValidationResult.Invalid_AppIdentity);

            Guid creds = Guid.Empty;
            Infrastructure.Security.LoginResult loginResult = Infrastructure.Security.LoginResult.UserNotFound;

            if (options.userRole == null || options.userRole == UserRole.Admin)
            {
                loginResult = Infrastructure.Security.Login.DoLogin(new UserRole[] { UserRole.Admin }, userName, email, password, options.deviceId, out creds);
            }


            if (loginResult == Infrastructure.Security.LoginResult.AdminBlocked || loginResult == Infrastructure.Security.LoginResult.AbsenceBlocked || loginResult == Infrastructure.Security.LoginResult.FailedAttemptsBlocked)
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            if (loginResult == Infrastructure.Security.LoginResult.Success)
            {
                ObjectContext.Current.CredentialsToken = creds;
                LoginResult result = new LoginResult(ValidationResult.Success);
                result.CredentialsToken = creds.ToString();
                Login user = Infrastructure.Security.Login.Get(creds);
                result.IsFirstLogin = user.IsFirstLogin;
                result.LastLogin = user.LastLogin.GetValueOrDefault();
                //result.Number = Bll.Accounts.Account.Current.AccountNumber;

                Netpay.Bll.ApplicationIdentity identity = null;
                if (Bll.Customers.Customer.Current != null && Bll.Customers.Customer.Current.RefIdentityID != null)
                    identity = Netpay.Bll.ApplicationIdentity.Load(Bll.Customers.Customer.Current.RefIdentityID.Value);
                else if (options != null && !string.IsNullOrEmpty(options.applicationToken))
                    identity = Netpay.Bll.ApplicationIdentity.GetIdentity(options.applicationToken.ToNullableGuid().GetValueOrDefault());
                if (identity != null) user.Items["Session.ApplicationIdentity"] = identity;
                if (options != null) user.Items["excludeDataImages"] = options.excludeDataImages;
                result.EncodedCookie = EncodeLoginCookie(options != null ? options.applicationToken : null, result.Number, email);
                if (options != null && options.setCookie) Context.Response.Cookies.Add(new HttpCookie(Web.WebUtils.CredentialsTokenName, creds.ToString()));
                //result.IsDeviceRegistrationRequired = by app identity
                //if (result.IsDeviceRegistrationRequired)
                //{
                //    var device = Bll.Accounts.MobileDevice.Current;
                //    if (device != null)
                //    {
                //        result.IsDeviceRegistered = true;
                //        result.IsDeviceActivated = device.IsActivated;
                //        result.IsDeviceBlocked = !device.IsActive;
                //    }
                //}
                result.CredentialsHeaderName = Web.WebUtils.CredentialsTokenName;
                return result;
            }
            else
            {
                if (options != null && options.setCookie) Context.Response.Cookies.Remove(Web.WebUtils.CredentialsTokenName);
                return new LoginResult(ValidationResult.Invalid_UserOrPassword);
            }
        }

        private string EncodeLoginCookie(string applicationToken, string mumber, string email)
        {
            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());
            return string.Format("CustomerNumber={0}&Email={1}&Signature={2}", HttpUtility.UrlEncode(mumber), HttpUtility.UrlEncode(email), (mumber + email + (appIdentity != null ? appIdentity.ID : 0)).ToSha256());
        }

        [OperationContract, WsdlDocumentation("When using the login service to login the service will return an Encodedcookie – using the cookie and the token you will be able to create an auto login function in cases you don’t want the user to relogin every time he enters the app."), ContextMethod(true, true)]
        public CookieResult DecodeLoginCookie(string applicationToken, string cookie)
        {
            var cookieValues = System.Web.HttpUtility.ParseQueryString(cookie);
            byte[] image;
            var ret = new CookieResult(ValidationResult.Success);
            ret.Number = cookieValues["CustomerNumber"].NullIfEmpty();
            ret.Email = cookieValues["Email"].NullIfEmpty();
            if (ret.Number == null || ret.Email == null) return new CookieResult(ValidationResult.Invalid_Email);
            if (EncodeLoginCookie(applicationToken, ret.Number, ret.Email) != cookie) return new CookieResult(ValidationResult.Invalid_Data);
            ret.FullName = Netpay.Bll.Customers.Customer.GetFullName(ret.Number, ret.Email, out image);
            ret.ProfilePicture = image;
            return ret;
        }



        #endregion

        #region GetCardDetail

        [OperationContract]
        [WsdlDocumentation("")]
        public Bll.ThirdParty.MulticatC.GetCardDetailResponse GetCardDetail(string applicationToken, string TerminalID = null, string CardNumber = null)
        {
            try
            {
                ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);

                #region Validate Application Token

                var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());

                if (appIdentity == null || appIdentity == Netpay.Bll.ApplicationIdentity.Default)
                    return new Bll.ThirdParty.MulticatC.GetCardDetailResponse() { ResponseCode = "-1", ResponseMessage = "Invalid applicationToken" };

                #endregion

                #region Validate Params

                if (string.IsNullOrEmpty(TerminalID) && string.IsNullOrEmpty(CardNumber))
                    return new Bll.ThirdParty.MulticatC.GetCardDetailResponse() { ResponseCode = "-1", ResponseMessage = "TerminalID and/or CardNumber not supplied" };

                #endregion

                List<MulticatC.GetCardDetailResponse.CardDetail> lstCardDetail = new List<MulticatC.GetCardDetailResponse.CardDetail>();

                #region CardNumber

                if (!string.IsNullOrEmpty(CardNumber))
                {
                    var pStoredPaymentMethods = Bll.Accounts.StoredPaymentMethod.FindByPAN(CardNumber, null);

                    // If nothing OR nothing found OR more than one found
                    if (pStoredPaymentMethods == null || pStoredPaymentMethods.Count == 0 || pStoredPaymentMethods.Count > 1)
                        return new Bll.ThirdParty.MulticatC.GetCardDetailResponse() { ResponseCode = "-14", ResponseMessage = "Invalid CardNumber" };

                    var pAccount = Bll.Customers.Customer.LoadObject(pStoredPaymentMethods[0].Account_id);
                    var pCustomer = Bll.Customers.Customer.Load(pAccount.CustomerID);

                    var pBalance = Bll.Accounts.Balance.GetStatus(pAccount.AccountID).Select(r => new WebServices.Balance.BalanceTotal() { CurrencyIso = r.CurrencyIso, Value = false ? r.Expected : r.Current });

                    decimal fBalance = 0;

                    if (pBalance != null && pBalance.Count() > 0)
                        fBalance = pBalance.ToList()[0].Value;

                    lstCardDetail.Add(new MulticatC.GetCardDetailResponse.CardDetail()
                    {
                        CardBalance = fBalance,
                        CardHolderName = pStoredPaymentMethods[0].OwnerName,
                        CardNumber = CardNumber,
                        CardStatus = pStoredPaymentMethods[0].Status.ToString(),
                        CustomerCode = pCustomer.AccountNumber,
                        CustomerName = pAccount.AccountName,
                        MobileNumber = pCustomer.PhoneNumber,
                        ReferenceID = pCustomer.CustomerID.ToString()
                    });

                    return new MulticatC.GetCardDetailResponse()
                    {
                        CardDetailList = lstCardDetail.ToArray(),
                        ResponseCode = "0",
                        ResponseMessage = "Success"
                    };

                }

                #endregion

                #region TerminalID

                else if (!string.IsNullOrEmpty(TerminalID))
                {
                    return new Bll.ThirdParty.MulticatC.GetCardDetailResponse() { ResponseCode = "-1", ResponseMessage = "Not implemented" };
                }

                #endregion

                return new Bll.ThirdParty.MulticatC.GetCardDetailResponse() { ResponseCode = "-1", ResponseMessage = "TerminalID and/or CardNumber not supplied" };
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return new Bll.ThirdParty.MulticatC.GetCardDetailResponse() { ResponseCode = "-1", ResponseMessage = "System Error" };
            }
        }

        #endregion

        #region RegisterCard

        [OperationContract]
        [WsdlDocumentation("")]
        public Bll.ThirdParty.MulticatC.RegisterCardResponse RegisterCard(string applicationToken, Bll.Accounts.StoredPaymentMethod.MethodStatus CardStatus, string CardNumber,
            string ReferenceID, string CustomerCode, string CustomerName, string CardHolderName, string MobileNumber, string Email = "N/A")
        {

            if (Email == "N/A")
                Email = CardNumber.TruncStart(6) + "@gearmax.co.za";

            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);

            #region Validate Application Token

            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());

            if (appIdentity == null || appIdentity == Netpay.Bll.ApplicationIdentity.Default)
                return new Bll.ThirdParty.MulticatC.RegisterCardResponse() { ResponseCode = "-1", ResponseMessage = "Invalid applicationToken", CardNumber = CardNumber };

            #endregion

            #region Validate Params

            if (string.IsNullOrEmpty(CardNumber) || string.IsNullOrEmpty(ReferenceID) || string.IsNullOrEmpty(CustomerCode) || string.IsNullOrEmpty(CustomerName)
                || string.IsNullOrEmpty(CardHolderName) || string.IsNullOrEmpty(MobileNumber))
                return new Bll.ThirdParty.MulticatC.RegisterCardResponse() { ResponseCode = "-1", ResponseMessage = "Missing information", CardNumber = CardNumber };

            #endregion

            #region Find and Update / Create - Customer

            var pCustomer = Bll.Customers.Customer.Load(Convert.ToInt32(ReferenceID));

            if (pCustomer == null)
            {
                // Create new
                pCustomer = new Bll.Customers.Customer()
                {
                    AccountName = CustomerName,
                    PhoneNumber = MobileNumber,
                    EmailAddress = Email,
                    RefIdentityID = 6
                };
            }
            else
            {
                // Update existing
                pCustomer.AccountName = CustomerName;
                pCustomer.PhoneNumber = MobileNumber;
                pCustomer.EmailAddress = Email;
            }

            #endregion

            #region Card Details

            if (!CardNumber.StartsWith("971040"))
                return new Bll.ThirdParty.MulticatC.RegisterCardResponse() { ResponseCode = "-1", ResponseMessage = "Invalid BIN", CardNumber = CardNumber };

            var pStoredPaymentMethods = Bll.Accounts.StoredPaymentMethod.FindByPAN(CardNumber, null);

            // If nothing OR nothing found OR more than one found
            if (pStoredPaymentMethods != null && pStoredPaymentMethods.Count > 0)
                return new Bll.ThirdParty.MulticatC.RegisterCardResponse() { ResponseCode = "-1", ResponseMessage = "Card already registered", CardNumber = CardNumber };

            var newCreatedCard = new Bll.Accounts.StoredPaymentMethod();
            newCreatedCard.OwnerName = CustomerName;

            // if it's not a prepaid, only creation of CCOneCard is allowed
            newCreatedCard.MethodInstance.PaymentMethodId = CommonTypes.PaymentMethodEnum.CCTheeCardNetwork;

            var newCreatedCc = (newCreatedCard.MethodInstance as Netpay.Bll.PaymentMethods.CreditCard);
            Bll.PaymentMethods.PaymentMethod pm = null;

            pm = Bll.PaymentMethods.PaymentMethod.Get(CommonTypes.PaymentMethodEnum.CCTheeCardNetwork);
            if (pm == null || string.IsNullOrEmpty(pm.BaseBin)) throw new Exception("Unable to create this kind of payment method, please try another");
            newCreatedCc.CardNumber = CardNumber;

            newCreatedCc.SetExpirationMonth(DateTime.Now.AddYears(1).Year, DateTime.Now.AddYears(1).Month);

            try
            {
                pCustomer.Save();
                newCreatedCard.Account_id = pCustomer.AccountID;
            }
            catch (Exception ex)
            {
                return new Bll.ThirdParty.MulticatC.RegisterCardResponse() { ResponseCode = "-1", ResponseMessage = "Error: " + ex.Message, CardNumber = CardNumber };
            }

            try
            {
                newCreatedCard.Save();
            }
            catch (Exception ex)
            {
                return new Bll.ThirdParty.MulticatC.RegisterCardResponse() { ResponseCode = "-1", ResponseMessage = "Error: " + ex.Message, CardNumber = CardNumber };
            }

            return new Bll.ThirdParty.MulticatC.RegisterCardResponse() { ResponseCode = "0", ResponseMessage = "Success", CardNumber = CardNumber };

            #endregion
        }

        #endregion

        #region CardTransaction

        [OperationContract]
        [WsdlDocumentation("")]
        public Bll.ThirdParty.MulticatC.CardTransactionResponse CardTransaction(string applicationToken, string CardNumber, string ReferenceID, decimal fAmount, string TerminalID)
        {
            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            Netpay.Dal.Netpay.NetpayDataContextBase dc = new Dal.Netpay.NetpayDataContextBase(Domain.Current.Sql1ConnectionString);

            #region Validate Application Token

            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());

            if (appIdentity == null || appIdentity == Netpay.Bll.ApplicationIdentity.Default)
                return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "-1", ResponseMessage = "Invalid applicationToken" };

            #endregion

            #region Validate Params

            if (string.IsNullOrEmpty(CardNumber) || string.IsNullOrEmpty(ReferenceID) || fAmount == 0)
                return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "-1", ResponseMessage = "Missing information" };

            #endregion

            int DEBITCOMPANY_ID = 72 /* System Prepaid */;
            var terminal = Bll.DebitCompanies.Terminal.LoadWithExtAccountID(DEBITCOMPANY_ID, TerminalID);

            if (terminal == null)
                return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "-1", ResponseMessage = "Terminal not found" };

            var refTermianl = (from n in DataContext.Reader.tblCompanyCreditFeesTerminals where n.CCFT_Terminal == terminal.TerminalNumber select n).FirstOrDefault();
            if (refTermianl == null)
                return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "-1", ResponseMessage = "Terminal fees not found" };

            var pStoredPaymentMethods = Bll.Accounts.StoredPaymentMethod.FindByPAN(CardNumber, null);

            // If nothing OR nothing found
            if (pStoredPaymentMethods == null || pStoredPaymentMethods.Count == 0)
                return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "-1", ResponseMessage = "Card not found" };

            var pStoredPaymentMethod = pStoredPaymentMethods.FirstOrDefault();

            var curObj = Bll.Currency.GetByIsoNumber(710);
            //var currenctBalace = Bll.Accounts.Balance.GetStatus(pStoredPaymentMethod.Account_id, curObj.IsoCode).Expected;
            //if (Bll.Accounts.Balance.GetStatus(pStoredPaymentMethod.Account_id, curObj.IsoCode).Expected < fAmount)
            //{
            //    return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "51", ResponseMessage = "Insufficient funds" };
            //    //ret = -51; // Not sufficient funds
            //}

            //if (pStoredPaymentMethod.MethodInstance.ExpirationDate.GetValueOrDefault(DateTime.MaxValue) < DateTime.Now)
            //{
            //    return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "54", ResponseMessage = "Expired card" };
            //    //ret = -54; // Expired card
            //}

            Bll.Customers.Customer customer = Bll.Customers.Customer.LoadObject(pStoredPaymentMethod.Account_id) as Bll.Customers.Customer;
            var pAccount = (from p in dc.Accounts
                            where p.Customer_id == customer.CustomerID
                            select p).SingleOrDefault();


            //validate merchant group by app identity
            var merchantGroup = Bll.Merchants.Merchant.GetMerchantsGroup(new List<int>() { refTermianl.CCFT_CompanyID.GetValueOrDefault() }).FirstOrDefault().Value;
            if (!customer.CanAcceptMerchantGroup(merchantGroup))
            {
                return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "56", ResponseMessage = "No card record" };
                //ret = -56; //No card record
            }

            int OriginalTransId = 0;
            string authorizationCode = DateTime.Now.Ticks.ToString("000000").TruncStart(6);

            int ret = 0;
            string CLASS_NAME = "Multicat";

            #region New

            try
            {
                #region Check if balance already exists

                bool bDoesExist = Bll.Accounts.Balance.Search(new Bll.Accounts.Balance.SearchFilters() { Amount = new Infrastructure.Range<decimal?>(fAmount), AccountID = pAccount.Account_id, CurrencyIso = "ZAR", Text = ReferenceID }, null).Any();

                if (bDoesExist)
                {
                    return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "2", ResponseMessage = "Duplicate detected" };
                }


                #endregion

                var pBalance = Bll.Accounts.Balance.Create(pAccount.Account_id, "ZAR", fAmount, ReferenceID, Bll.Accounts.Balance.SOURCE_DEPOSIT, null, false);

                return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "0", ResponseMessage = "Success", TransactionID = pBalance.ID.ToString() };
            }
            catch (Exception ex)
            {
                Logger.Log(LogTag.WebService, ex);
                return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = ret.ToString(), ResponseMessage = "Error" };

            }

            #endregion

            #region Old

            //try
            //{
            //    Iso8583Base.Iso8583PostXml pXMLRequest = new Iso8583Base.Iso8583PostXml(new Iso8583Base(Iso8583Factory.Implementation.Multicat
            //        , new string[] { "1133557799199776", "B210A3BFD5EE9843" }
            //        , 64
            //        , 10
            //        , "Multicat"
            //        , DEBITCOMPANY_ID
            //        , "Multicat"
            //        , "Multicat"));

            //    pXMLRequest.Amount = fAmount * -1;
            //    pXMLRequest.ReferenceNumber = Convert.ToInt32(ReferenceID);
            //    pXMLRequest.ReferenceCode = ReferenceID;
            //    pXMLRequest.MessageType = "0200";

            //    string strValue1 = "";
            //    string strValue2 = "";
            //    pStoredPaymentMethod.MethodInstance.GetDecryptedData(out strValue1, out strValue2);
            //    pXMLRequest.PAN = strValue1;
            //    string pin = Infrastructure.Security.Encryption.DecodeCvv(pAccount.PincodeSHA256);

            //    System.ServiceModel.Channels.MessageProperties prop = OperationContext.Current.IncomingMessageProperties;
            //    System.ServiceModel.Channels.RemoteEndpointMessageProperty endpoint = prop[System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name] as System.ServiceModel.Channels.RemoteEndpointMessageProperty;
            //    string strIPAddress = endpoint.Address;

            //    var trans = new Bll.Transactions.Transaction()
            //    {
            //        Status = (ret < 0 ? TransactionStatus.Declined : TransactionStatus.Captured),
            //        ReplyCode = System.Math.Abs(ret).ToString("000"),
            //        TerminalNumber = terminal.TerminalNumber,
            //        PaymentsIDs = ";0;",
            //        IP = strIPAddress.ToString(),
            //        OrderNumber = pXMLRequest.ReferenceNumber.GetValueOrDefault().ToString(),
            //        Amount = pXMLRequest.Amount,
            //        CurrencyID = curObj.ID,
            //        MerchantID = refTermianl.CCFT_CompanyID.GetValueOrDefault(),
            //        DebitReferenceCode = pXMLRequest.ReferenceCode,
            //        DebitCompanyID = DEBITCOMPANY_ID,
            //        InsertDate = DateTime.Now,
            //        ApprovalNumber = authorizationCode,
            //        Installments = 1,
            //        TransactionSource = TransactionSource.DebitCards,
            //        TransType = TransactionType.Capture,
            //        CreditType = (pXMLRequest.BalanceAmount < 0 ? CreditType.Regular : CreditType.Refund),
            //        CustomerID = customer.CustomerID,
            //        //UnsettledInstallments = 1,
            //        MerchantPayDate = DateTime.Now.Date.AddDays(1).Date,
            //        PayForText = CLASS_NAME + " POS",
            //        OriginalTransactionID = OriginalTransId,
            //        PaymentData = new Bll.Transactions.Payment() { MerchantID = refTermianl.CCFT_CompanyID.GetValueOrDefault() },
            //        PayerData = new Bll.Transactions.Payer() { FullName = customer.AccountName, EmailAddress = customer.EmailAddress, PersonalNumber = customer.PersonalNumber, PhoneNumber = customer.PhoneNumber }
            //    };
            //    if (trans.Status == TransactionStatus.Captured) trans.BatchData = pXMLRequest.OuterXml;
            //    trans.PaymentData.MethodInstance = new Bll.PaymentMethods.CreditCard(trans.PaymentData) { CardNumber = pXMLRequest.PAN, Cvv = pin, ExpirationDate = pXMLRequest.ExpDate, PaymentMethodId = pStoredPaymentMethod.MethodInstance.PaymentMethodId };

            //    var refFees = Bll.Merchants.ProcessTerminals.Load(refTermianl.CCFT_CCF_ID.GetValueOrDefault());
            //    if (refFees != null) trans.SetTransactionFees(refFees);
            //    trans.Save();
            //    if (ret < 0)
            //    {
            //        return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = ret.ToString(), ResponseMessage = "Error" };
            //    }

            //    var balance = Bll.Accounts.Balance.Create(pStoredPaymentMethod.Account_id, curObj.IsoCode, pXMLRequest.BalanceAmount, CLASS_NAME + ": " + pXMLRequest.ReferenceNumber, Bll.Accounts.Balance.SOURCE_PURCHASE, trans.ID, false);
            //    if (refFees != null && refFees.CashbackPercent != 0 && trans.CreditType == CreditType.Regular)
            //    {
            //        var cashbackAmount = (refFees.CashbackPercent * pXMLRequest.Amount) / 100;
            //        var cashbackTrans = trans.CreateRefundTransaction(cashbackAmount);
            //        cashbackTrans.IsCashback = true;
            //        cashbackTrans.Comment = "Cashback from trans #" + trans.ID;
            //        cashbackTrans.Save();
            //        Bll.Accounts.Balance.Create(pStoredPaymentMethod.Account_id, curObj.IsoCode, cashbackAmount, "Cashback transaction #" + pXMLRequest.ReferenceNumber, Bll.Accounts.Balance.SOURCE_PURCHASE, trans.ID, false);
            //    }

            //    return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = "0", ResponseMessage = "Success", TransactionID = trans.ID.ToString() };
            //}
            //catch (Exception ex)
            //{
            //    Logger.Log(LogTag.WebService, ex);
            //    return new Bll.ThirdParty.MulticatC.CardTransactionResponse() { ResponseCode = ret.ToString(), ResponseMessage = "Error" };

            //}

            #endregion

        }

        #endregion



        #region Generic Classes

        public class MyMapper : System.ServiceModel.Channels.WebContentTypeMapper
        {
            public override System.ServiceModel.Channels.WebContentFormat GetMessageFormatForContentType(string contentType)
            {
                if (contentType.ToUpper().Contains("json".ToUpper()))
                {
                    return System.ServiceModel.Channels.WebContentFormat.Raw;
                }
                else
                {
                    return System.ServiceModel.Channels.WebContentFormat.Default;
                }
            }
        }



        #endregion
    }
}
