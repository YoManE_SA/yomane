﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ComponentModel;
using System.Web;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// Summary description for International
    /// </summary>
    [System.ServiceModel.ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class International : V2.BaseService
    {
        public class StaticData
        {
            public ServiceListItem[] Countries { get; set; }
            public ServiceListItem[] UsaStates { get; set; }
            public ServiceListItem[] CanadaStates { get; set; }
            public ServiceListItem[] Languages { get; set; }
        }

        public International() : base(false) {  }

        [OperationContract, ContextMethod(true, true)]
        [WsdlDocumentation("The system holds lists that serve different aspects of the app – such as countries / states / languages. Common practice to call the service once and store the data locally and trigger a call in a later stage.")]
        public StaticData GetStaticData()
        {
            StaticData lists = new StaticData();
            var countries = Bll.Country.Cache.Select(c => new ServiceListItem() { Key = c.IsoCode2, Name = c.Name }).ToList();
            var usa = countries.Where(x => x.Key == "USA").SingleOrDefault();
            if (usa != null) { countries.Remove(usa); countries.Insert(0, usa); }
            lists.Countries = countries.ToArray();
            lists.UsaStates = Bll.State.Cache.Where(c => c.CountryIsoCode == "US").Select(c => new ServiceListItem() { Key = c.IsoCode, Name = c.Name }).ToArray();
            lists.CanadaStates = Bll.State.Cache.Where(c => c.CountryIsoCode == "CA").Select(c => new ServiceListItem() { Key = c.IsoCode, Name = c.Name }).ToArray();
            lists.Languages = Bll.International.Language.Cache.Select(c => new ServiceListItem() { Key = c.Culture, Name = c.Name }).ToArray();
            return lists;
        }

        [OperationContract, ContextMethod(true, true)]
        [WsdlDocumentation("The service will return all reply codes and their text to allow customization of the responses in an app.")]
        public ServiceResult[] GetErrorCodes(string language, HashSet<string> groups)
        {
            if (groups == null)
                groups = new HashSet<string>("General,Address,Customer,Payment,PaymentMethod,Security".Split(','));
            var result = (from e in Infrastructure.ErrorText.GetErrorGroup(null, groups)
                    select new ServiceResult() { Code = e.Key, Message = e.Value, Key = ((Netpay.Infrastructure.ValidationResult)e.Key).ToString() }).ToArray();
            return result;
        }

        [ContextMethod(true, true)]
        [OperationContract]
        [WsdlDocumentation("Given that an app can be used in different territories the service will return the conversation rates available in the system to maintain the same rates on both admincash and the apps connected to it.")]
        public KeyValue<string, decimal>[] GetCurrencyRates()
        {
            return Bll.Currency.Cache.Where(c => AppIdentity.SupportedCurrencies.Contains(c.IsoCode)).Select(c => new KeyValue<string, decimal>(c.IsoCode, c.BaseRate)).ToArray();
        }

    }
}
