﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ServiceModel;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.WebServices.V2
{
    /// <summary>
    /// Summary WsdlDocumentation for Customer
    /// </summary>
    [System.ServiceModel.ServiceContract(Namespace = "WebServices", ProtectionLevel = System.Net.Security.ProtectionLevel.None)]
    public class Customer : V2.BaseService
    {
        private Bll.Customers.Customer CurrentCustomer
        {
            get
            {
                return Bll.Customers.Customer.Current;
            }
        }

        private List<Bll.Customers.Customer> CurrentCustomerSubAccounts
        {
            get
            {
                List<int> subAccountsIds = Bll.Customers.Relation.GetSubAccountCustomersIds(CurrentCustomer.ID);
                if (subAccountsIds.Count > 0)
                {
                    return Bll.Customers.Customer.Search(new Bll.Customers.Customer.SearchFilters() { CustomersIdsList = subAccountsIds }, null);
                }
                else
                {
                    return new List<Bll.Customers.Customer>();
                }
            }
        }

        #region Service Types
        public class CustomerData : ServiceAddress
        {
            public CustomerData() { }
            public System.DateTime RegistrationDate { get; set; }
            public string CustomerNumber { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string PersonalNumber { get; set; }
            public string PhoneNumber { get; set; }
            public string CellNumber { get; set; }
            public System.DateTime? DateOfBirth { get; set; }
            public string EmailAddress { get; set; }
            public long ProfileImageSize { get; set; }
            public byte[] ProfileImage { get; set; }
            #region Save And Load
            internal void Save(Bll.Customers.Customer value)
            {
                value.FirstName = FirstName;
                value.LastName = LastName;
                value.PersonalNumber = PersonalNumber;
                value.PhoneNumber = PhoneNumber;
                value.CellNumber = CellNumber;
                value.DateOfBirth = DateOfBirth;
                value.EmailAddress = EmailAddress;
                if (ProfileImage != null) value.ProfileImageData = ProfileImage;
                if (value.PersonalAddress == null) value.PersonalAddress = new Bll.Accounts.AccountAddress();
                base.Save(value.PersonalAddress);
            }
            internal CustomerData(Bll.Customers.Customer cs)
                : base(cs.PersonalAddress)
            {
                RegistrationDate = cs.RegistrationDate;
                CustomerNumber = cs.AccountNumber;
                FirstName = cs.FirstName;
                LastName = cs.LastName;
                PersonalNumber = cs.PersonalNumber;
                PhoneNumber = cs.PhoneNumber;
                CellNumber = cs.CellNumber;
                DateOfBirth = cs.DateOfBirth;
                EmailAddress = cs.EmailAddress;
                ProfileImageSize = cs.ProfileImageSize;
                if (ProfileImageSize > 0 && !Login.Current.Items.GetValue<object>("excludeDataImages").ToNullableBool().GetValueOrDefault())
                    ProfileImage = cs.ProfileImageData;
            }
            #endregion
        }

        public class CustomerRegisterData
        {
            public CustomerData info { get; set; }
            public ShippingAddress[] ShippingAddresses { get; set; }
            public PaymentMethods.StoredPaymentMethod[] StoredPaymentMethods { get; set; }
            public string PinCode { get; set; }
            public string Password { get; set; }
            public string ApplicationToken { get; set; }
        }

        //Details that will be send when registerin a SubAccountCustomer
        public class CustomerSubAccountData
        {
            public string PersonalNumber { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string PhoneNumber { get; set; }
            public string CellNumber { get; set; }
            public string EmailAddress { get; set; }
        }

        //Details that will be sent when registering a SubAccountCustomer
        public class CustomerSubAccountRegisterData
        {
            public CustomerSubAccountData info { get; set; }
            public string PinCode { get; set; }
            public string Password { get; set; }
            public string ApplicationToken { get; set; }
        }

        public class ShippingAddress : ServiceAddress
        {
            public ShippingAddress() { }
            public int ID { get; set; }
            public string Title { get; set; }
            public string Comment { get; set; }
            public bool IsDefault { get; set; }
            #region Save And Load
            public ShippingAddress(Bll.Customers.ShippingAddress sa) : base(sa)
            {
                ID = sa.ID;
                Title = sa.Title;
                Comment = sa.Comment;
                IsDefault = sa.IsDefault;
            }
            internal void Save(Bll.Customers.ShippingAddress value)
            {
                value.Title = Title;
                value.Comment = Comment;
                value.IsDefault = IsDefault;
                base.Save(value);
            }
            #endregion
        }

        public class CustomerFriend
        {
            public CustomerFriend() { }
            internal CustomerFriend(Bll.Customers.Relation.RelationInfo data)
            {
                DestWalletId = data.AccountNumber;
                FullName = data.FullName;
                RelationType = (int?)data.PeopleRelationType;
                if (System.IO.File.Exists(data.ImagePath))
                {
                    ProfileImageSize = new System.IO.FileInfo(data.ImagePath).Length;
                    if (!Login.Current.Items.GetValue<object>("excludeDataImages").ToNullableBool().GetValueOrDefault())
                        ProfileImage = System.IO.File.ReadAllBytes(data.ImagePath);
                }
            }
            public string DestWalletId { get; set; }
            public int? RelationType { get; set; }
            public string FullName { get; set; }
            public long ProfileImageSize { get; set; }
            public byte[] ProfileImage { get; set; }
        }

        public class CustomerFriendList : ServiceResult
        {
            internal CustomerFriendList(int code, string message) { Code = code; Message = message; IsSuccess = (Code == 0); }
            public CustomerFriendList() : base(ValidationResult.Success) { }
            public List<CustomerFriend> Items { get; set; }
        }

        public class CustomerSubAccountDetails
        {
            public string CustomerNumber { get; set; }
            public string CustomerName { get; set; }
            //public KeyValue<string, decimal>[] Balances { get; set; }
        }

        #endregion

        public Customer() : base(true, new UserRole[] { UserRole.Customer }) { }

        #region Shipping Address
        [OperationContract, WsdlDocumentation("The service will return all of the user’s shipping addresses.")]
        public List<ShippingAddress> GetShippingAddresses()
        {
            return Bll.Customers.ShippingAddress.LoadForCustomer(Bll.Accounts.Account.Current.CustomerID).Select(n => new ShippingAddress(n)).ToList();
        }

        [OperationContract, WsdlDocumentation("The service will return the shipping address details for a specific shipping address by the shipping address id.")]
        public ShippingAddress GetShippingAddress(int addressId)
        {
            var item = Bll.Customers.ShippingAddress.Load(addressId);
            if (item == null)
                return null;
            if (item.Customer_id != Bll.Customers.Customer.Current.ID)
                return null;

            return new ShippingAddress(item);
        }

        [OperationContract, WsdlDocumentation("This service will enable the user to save a new shipping address or to edit an old address already saved in the system.")]
        public ServiceMultiResult SaveShippingAddress(ShippingAddress address)
        {
            return SaveShippingAddresses(new ShippingAddress[] { address });
        }

        [OperationContract, WsdlDocumentation("Managing shipping addresses can have its complexities – when saving a new “Default shipping address” the rest of the addresses are effected as well – this service will allow to make the change to all addresses at the same time.")]
        public ServiceMultiResult SaveShippingAddresses(ShippingAddress[] data)
        {
            var refNumbers = new List<string>();
            var pmList = new List<Netpay.Bll.Customers.ShippingAddress>();
            foreach (var address in data)
            {
                Bll.Customers.ShippingAddress dataObject;
                if (address.ID == 0)
                {
                    dataObject = new Bll.Customers.ShippingAddress();
                    dataObject.Customer_id = Bll.Customers.Customer.Current.ID;
                }
                else dataObject = Bll.Customers.ShippingAddress.Load(address.ID);
                address.Save(dataObject);
                var result = dataObject.Validate();
                if (result != ValidationResult.Success) return new ServiceMultiResult(result);
                pmList.Add(dataObject);
            }
            foreach (var dataObject in pmList)
            {
                dataObject.Save();
                refNumbers.Add(dataObject.ID.ToString());
            }
            return (new ServiceMultiResult(ValidationResult.Success) { RefNumbers = refNumbers.ToArray() });
        }

        [OperationContract, WsdlDocumentation("With using the addressID you will be able to delete a user’s specific shipping address.")]
        public ServiceResult DeleteShippingAddress(int addressId)
        {
            var item = Bll.Customers.ShippingAddress.Load(addressId);
            if (item == null)
                return new ServiceResult(ValidationResult.Item_Not_Exist);
            if (item.Customer_id != Bll.Customers.Customer.Current.ID)
                return new ServiceResult(ValidationResult.Invalid_AccountID);

            item.Delete();
            return new ServiceResult(ValidationResult.Success);
        }
        #endregion

        #region Customer managment
        [OperationContract, WsdlDocumentation("The service will return all of the user’s information from addresses to private information such as DOB and more.")]
        public CustomerData GetCustomer()
        {
            return new CustomerData(Bll.Customers.Customer.Current);
        }

        /// <summary>
        /// Will return list of customers of relation type "SubAccount" that will be held in 
        /// Data.CustomerRelation table , with the Father as Customer_id , and the childrens as TargetCustomer_id , and with
        /// PeopleRelationType_id of CustomerSubAccount , to each customer the following details will be returned, a list of CustomerSubAccountData
        /// CustomerNumber
        /// CustomerName
        /// CustomerBalances by currencies 
        /// </summary>
        /// <param name="customerNumber"></param>
        /// <returns></returns>
        [OperationContract, WsdlDocumentation("This service will return list of managed accounts under the current logged in customer")]
        public List<CustomerSubAccountDetails> GetManagedAccounts() 
        {
            if (CurrentCustomerSubAccounts.Count > 0)
            {
                return CurrentCustomerSubAccounts.Select(x => new CustomerSubAccountDetails()
                {
                    CustomerName = x.FullName,
                    CustomerNumber = x.AccountNumber,
                    //Balances = Bll.Accounts.Balance.GetStatus(x.AccountID).Select(b => new KeyValue<string, decimal>(b.CurrencyIso, b.Current)).ToArray()
                })
                .ToList();
            }
            else
            {
                return null;
            }
        }

        [OperationContract, WsdlDocumentation("This service will log in a child of the current logged in customer, and return credentials token created")]
        public WebServices.V2.Account.LoginResult GetChildCredentials(string childCustomerNumber)
        {
            var currentCustomerSubAccounts = CurrentCustomerSubAccounts;

            // Check if the ChildCustomerNumber is a child of the Current logged in customer.
            if (!(currentCustomerSubAccounts.Select(x => x.AccountNumber).ToList().Contains(childCustomerNumber))) return new Account.LoginResult(ValidationResult.Invalid_Data);

            // Get the details of the ChildCustomer need to be logged in
            var childCustomerToLogin = currentCustomerSubAccounts.Where(x => x.AccountNumber == childCustomerNumber).SingleOrDefault();

            //Get details of the child customer for the login

            //IP address
            System.Net.IPAddress address;
            System.Net.IPAddress.TryParse(Context.Request.UserHostAddress, out address);

            //Login password
            string loginPassword = Login.GetPassword(childCustomerToLogin.LoginID.Value, address.ToString());

            //App identity token
            string appIdentityToken = Netpay.Web.WebUtils.GetGlobalParameter(WebServices.V2.BaseService.ApplicationToken_Key);

            // Call to the Login method at the Account service
            var client = new WebServices.V2.Account();
            
            var result = client.Login(childCustomerToLogin.Login.EmailAddress, null, loginPassword, new Account.LoginOptions() { applicationToken = appIdentityToken });

            // return the credentials created.
            return result;
        }

        [OperationContract, WsdlDocumentation("This service will register a customer and will add him as a SubAccount of the current logged in customer")]
        public ServiceResult RegisterCustomerAsSubAccount(CustomerSubAccountRegisterData data)
        {
            Guid? appIdentityId = data.ApplicationToken.ToNullableGuid();
            System.Net.IPAddress address;
            System.Net.IPAddress.TryParse(Context.Request.UserHostAddress, out address);

            var customer = new Bll.Customers.Customer();
            if (appIdentityId != null)
            {
                var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(appIdentityId.Value);
                if (appIdentity == null) return new ServiceResult(ValidationResult.Invalid_AppIdentity);
                customer.RefIdentityID = appIdentity.ID;
            }

            //insert values into the new created customer
            customer.FirstName = data.info.FirstName;
            customer.LastName = data.info.LastName;
            customer.PersonalNumber = data.info.PersonalNumber;
            customer.PhoneNumber = data.info.PhoneNumber;
            customer.CellNumber = data.info.CellNumber;
            customer.EmailAddress = data.info.EmailAddress;

            var saveResult = customer.Register(address, data.PinCode, data.Password);
            if (saveResult != ValidationResult.Success) return new ServiceResult(saveResult);

            //Set the created customer as SubAccount of the logged in customer
            Bll.Customers.Relation.setCustomerSubAccount(Bll.Customers.Customer.Current.ID, customer.ID);

            return new ServiceResult(ValidationResult.Success);
        }


        [OperationContract, WsdlDocumentation("This is the “Sign up” service – you will use this service to register a new Customer. The service will enable you to register all of the user’s information along with a shipping address and payment method. Adding a shipping address and a payment method is not a “must” and can be added later on with the relevant service call. IMPORTANT – the label must have an email template for registering a new customer – if the email template is missing the service will return a failure msg."), ContextMethod(true, true)]
        public ServiceResult RegisterCustomer(CustomerRegisterData data)
        {
            //string ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            //string registeredFrom = "mobile";
            System.Net.IPAddress address;
            System.Net.IPAddress.TryParse(Context.Request.UserHostAddress, out address);
            Guid? appIdentityId = data.ApplicationToken.ToNullableGuid();

            var pmList = new List<Netpay.Bll.Accounts.StoredPaymentMethod>();
            if (data.StoredPaymentMethods != null)
            {
                foreach (var spm in data.StoredPaymentMethods)
                {
                    var pm = new Netpay.Bll.Accounts.StoredPaymentMethod();
                    spm.Save(pm);
                    var validationResult = pm.Validate();
                    if (validationResult != ValidationResult.Success) return new ServiceResult(validationResult);
                    pmList.Add(pm);
                }
            }

            var customer = new Bll.Customers.Customer();
            if (appIdentityId != null)
            {
                var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(appIdentityId.Value);
                if (appIdentity == null) return new ServiceResult(ValidationResult.Invalid_AppIdentity);
                customer.RefIdentityID = appIdentity.ID;
            }
            data.info.Save(customer);
            var saveResult = customer.Register(address, data.PinCode, data.Password);
            if (saveResult != ValidationResult.Success) return new ServiceResult(saveResult);
            foreach (var spm in pmList)
            {
                spm.Account_id = customer.AccountID;
                spm.Save();
            }
            return (new ServiceResult(saveResult) { Number = customer.AccountNumber });
        }


        [OperationContract, WsdlDocumentation("After a Customer was successfully registered any change to the customer’s personal information will be saved using this function.")]
        public ServiceResult SaveCustomer(CustomerData info)
        {
            var customer = Bll.Customers.Customer.Current;
            info.Save(customer);
            var res = customer.Validate();
            if (res != ValidationResult.Success)
                return new ServiceResult(res);
            customer.Save();
            return new ServiceResult(ValidationResult.Success);
        }


        #endregion

        #region Relations
        [OperationContract, WsdlDocumentation("The service will return a list of all of the user’s friends and their information such as: Wallet id, Profile pic, Full name, Relation (we will cover relations under the SetRelation service.")]
        public CustomerFriendList GetFriends(string destWalletId)
        {
            var friends = Bll.Customers.Relation.GetFriends(Bll.Customers.Customer.Current.ID);
            if (!string.IsNullOrEmpty(destWalletId))
            {
                var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
                friends = friends.Where(r => r.CustomerId == targetCustomer.CustomerId && r.TargetCustomerId == targetCustomer.CustomerId).ToList();
            }
            var ret = Bll.Customers.Relation.GetInfo(Bll.Customers.Customer.Current.ID, friends).Where(f => f.AccountID != Bll.Accounts.Account.Current.AccountID);
            return new CustomerFriendList() { Items = ret.Select(f => new CustomerFriend(f)).ToList() };
        }

        [OperationContract, WsdlDocumentation("The service will return the image for the wallet ID")]
        public byte[] GetImage(string walletId, bool asRaw)
        {
            var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { walletId }).SingleOrDefault();
            if (targetCustomer == null) return null;
            if (!System.IO.File.Exists(targetCustomer.ImagePath)) return null;
            if (!asRaw) return System.IO.File.ReadAllBytes(targetCustomer.ImagePath);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=\"" + System.IO.Path.GetFileName(targetCustomer.ImagePath) + "\"");
            HttpContext.Current.Response.WriteFile(targetCustomer.ImagePath);
            HttpContext.Current.Response.End();
            return null;
        }

        [OperationContract, WsdlDocumentation("The service will return all pending friend requests for the user and will provide the necessary info in order to reply to such request.")]
        public CustomerFriendList GetFriendRequests(string destWalletId)
        {
            var friends = Bll.Customers.Relation.GetRequests(Bll.Customers.Customer.Current.ID);
            if (!string.IsNullOrEmpty(destWalletId))
            {
                var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
                friends = friends.Where(r => r.CustomerId == targetCustomer.CustomerId && r.TargetCustomerId == targetCustomer.CustomerId).ToList();
            }
            var ret = Bll.Customers.Relation.GetInfo(Bll.Customers.Customer.Current.ID, friends).Where(f => f.AccountID != Bll.Accounts.Account.Current.AccountID);
            return new CustomerFriendList() { Items = ret.Select(f => new CustomerFriend(f)).ToList() };
        }

        [OperationContract, WsdlDocumentation("The search function will allow you send certain term with a definition of the page size and the page number.")]
        public CustomerFriendList FindFriend(string searchTerm, SortAndPage sortAndPage)
        {
            var items = Bll.Customers.Relation.GetInfo(searchTerm, true, Bll.Customers.Customer.Current.RefIdentityID, sortAndPage.ToSortAndPage()).Select(n => new CustomerFriend(n));
            return new CustomerFriendList() { Items = items.ToList() };
        }

        [OperationContract, WsdlDocumentation("By sending the destination wallet ID the source user will send a request to the destination user. After using the find a friend function you will get the account id for that user and you can use the FriendRequest function to invite the user to connect.")]
        public CustomerFriendList FriendRequest(string destWalletId)
        {
            var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
            if (targetCustomer == null) return new CustomerFriendList(2, "destWalletId Not found");
            var newReq = Bll.Customers.Relation.GetRelation(Bll.Customers.Customer.Current.ID, targetCustomer.CustomerId);
            if (newReq != null && (newReq.IsActive.HasValue && !newReq.IsActive.Value))
            { //delete old declined request
                newReq.Delete();
                newReq = null;
            }
            if (newReq == null)
            {
                newReq = new Bll.Customers.Relation(Bll.Customers.Customer.Current.ID, targetCustomer.CustomerId);
                newReq.Save();
            }
            return new CustomerFriendList() { Items = new List<CustomerFriend>() { new CustomerFriend(targetCustomer) } };
        }

        [OperationContract, WsdlDocumentation("Simply enough this service will help you delete the friend you no longer wish to have.")]
        public ServiceResult RemoveFriend(string destWalletId)
        {
            var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
            if (targetCustomer == null) return new ServiceResult(ValidationResult.Item_Not_Exist);
            if (targetCustomer.CustomerId == Bll.Customers.Customer.Current.ID) return new ServiceResult(ValidationResult.Invalid_AccountID);
            var rel = Bll.Customers.Relation.GetRelation(Bll.Customers.Customer.Current.ID, targetCustomer.CustomerId);
            if (rel == null) return new ServiceResult(ValidationResult.Item_Not_Exist);
            rel.Delete();
            return new ServiceResult(ValidationResult.Success);
        }

        [OperationContract, WsdlDocumentation("The combo of “Friend request” / “GetFriendRequests” and “ReplyFriendRequest” give the full management of the friendship cycle. After sending a friend request to a user the user can decide if to approve the request or decline it, this service will allow to send the response to the relevant request.")]
        public ServiceResult ReplyFriendRequest(string destWalletId, bool approve)
        {
            if (string.IsNullOrEmpty(destWalletId)) return new ServiceResult(ValidationResult.Item_Not_Exist);
            var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
            var item = Bll.Customers.Relation.GetRelation(Bll.Customers.Customer.Current.ID, targetCustomer.CustomerId);
            if (item == null) return new ServiceResult(ValidationResult.Item_Not_Exist);
            if (item.IsActive != null) return new ServiceResult(ValidationResult.Item_Wrong_State);
            if (approve) item.Confirm();
            else item.Reject();
            item.Save();
            return new ServiceResult(ValidationResult.Success);
        }

        [OperationContract, WsdlDocumentation("Relationships are important – we give every user the ability to save a relationship setting for every friend he has. Meaning if I say another user is my family member the service can set that user as a family member and I can easily filter the users accordantly. Relation types: 10 - Immediate family - Spouse, parent, brother, sister, son, etc., 20 - Extended family - Grandparent, aunt, uncle, cousin, etc., 30 - Friend, 50 - Colleague, 60 - Acquaintance, 70 - Other")]
        public ServiceResult SetFriendRelation(string destWalletId, int? relationTypeKey)
        {
            if (string.IsNullOrEmpty(destWalletId)) return new ServiceResult(ValidationResult.Item_Not_Exist);
            var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
            if (targetCustomer == null) return new ServiceResult(ValidationResult.Invalid_ID);
            var item = Bll.Customers.Relation.GetRelation(Bll.Customers.Customer.Current.ID, targetCustomer.CustomerId);
            if (item == null) return new ServiceResult(ValidationResult.Item_Not_Exist);
            item.PeopleRelationType = (Bll.Customers.Relation.RelationType?)relationTypeKey;
            item.Save();
            return new ServiceResult(ValidationResult.Success);
        }
        #endregion

        #region Facebook
        public class FacebookFriendDetails
        {
            public FacebookFriendDetails() { }
            public string id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
        }

        /*
        [OperationContract, WsdlDocumentation("This service will use a Facebook app token (needs to be created within Facebook as a facebook developer – (https://developers.facebook.com/docs/apps ) Once the service will be called it will return all of the email addresses (that the users signed up with to Facebook ) and the service will run a search for all wallet users with one of the emails.  When available then the user will be able to invite his registered Facebook friends to the app.")]
        public void ImportFriendsFromFacebook(string accessToken)
        {
            //Login("lavi@netpay-intl.com", "aabb1122", new WalletLoginOptions() { setCookie = true });
            //accessToken = "CAACEdEose0cBAAL18FWMWmxNEwqoj3ffWJm7u3FpTnPrXUyVBrCdZCfuB86FN3GKbe9POIuwyYuOFGFfVOpp4Ttyxwis02s0l9ZCRIZAWDomWb1G0VoAMkMdAFvNldDdixMZCqmFjmJthZChBGVc2CUXbHNZCPknkxIEDM3z5ZAie57fcuEdkkKI8XakxQeoVdypsOSJwQ2m0f0pVi6braXApXJZCxCBspAZD";
            var wc = new System.Net.WebClient();
            string userData = wc.DownloadString("https://graph.facebook.com/me?fields=id,name,email&access_token=" + accessToken); 
            
            FacebookFriendDetails friendDetails = null;
            var ser = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(FacebookFriendDetails));
            using (var ms = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(userData)))
                friendDetails = (FacebookFriendDetails)ser.ReadObject(ms);

            Bll.Customers.Customer.Current.FacebookUserID = friendDetails.id;
            Bll.Customers.Customer.Current.Save();

            string strJSONData = wc.DownloadString("https://graph.facebook.com/me?fields=friends&access_token=" + accessToken);
            strJSONData = string.Format("[{0}]", strJSONData.Split('[')[1].Split(']')[0]);
            var objJavaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            FacebookFriendDetails[] objStatus = objJavaScriptSerializer.Deserialize<FacebookFriendDetails[]>(strJSONData);
            Dictionary<string, string> foundUsers = Bll.Customers.Customer.FindFacebookUsers(objStatus.Select(p => p.id).ToList<string>());
            foreach (var user in foundUsers)
                FriendRequest(user.Key);
        }
        */
        #endregion

    }
}
