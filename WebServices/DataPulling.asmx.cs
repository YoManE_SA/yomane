﻿using Netpay.Bll;
using Netpay.Bll.Merchants;
using Netpay.Bll.Transactions;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Netpay.WebServices
{
    [WebService(Namespace = "WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class DataPulling : Netpay.WebServices.Code.BaseService
    {
        public DataPulling() : base(new UserRole[] { UserRole.Merchant }) { }

        public enum TransactionType
        {
            Captured = 1,
            Declined = 2,
            Approval = 3
        }

        public class Result
        {
            public Result()
            {

            }

            public Result(bool isSuccess, string error)
            {
                IsSuccess = isSuccess;
                Error = error;
            }

            public bool IsSuccess { get; set; }
            public string Error { get; set; }
            public TransactionInfo[] Transactions { set; get; }
        }

        public class TransactionInfo
        {
            internal TransactionInfo()
            {
            }

            internal TransactionInfo(Bll.Transactions.Transaction vo)
            {
                ID = vo.ID;
                Amount = vo.Amount;
                ApprovalCode = vo.ApprovalNumber;
                Card = vo.PaymentMethodDisplay;
                InsertDate = vo.InsertDate;
                Currency = vo.CurrencyIsoCode;
                Installments = vo.Installments;
                Comment = vo.Comment;
                PaymentDetails = vo.PayForText;
                var payerInfo = vo.PayerData;
                if (payerInfo != null)
                {
                    CardholderName = payerInfo.FullName;
                    PersonalNumber = payerInfo.PersonalNumber;
                    Email = payerInfo.EmailAddress;
                    Phone = payerInfo.PhoneNumber;
                }
                IsRefunded = Bll.Transactions.RefundRequest.GetRefundAbility(vo) != TransactionRefundAbility.FullOnly;
                IsManual = true;
            }

            //    <Type>CHB</Type>
            //    <ChargebackDate>24/08/2009 22:33:44</ChargebackDate>7
            //    <ChargebackReason>33</ChargebackReason>7
            //    <Order>A123</Order>
            //    <RecurringCharge>2</RecurringCharge>6
            //    <RecurringSeries>231</RecurringSeries>6
            //    <Comment>White box</Comment>
            //    <Reply>000</Reply>
            //    <OriginalTransID>1234567</OriginalTransID>6
            //    <RefTrans>R1234567</RefTrans>8
            //    <Method>CC</Method>5
            //    <CreditCard>
            //        <Type>Visa</Type>
            //        <BIN>880005</BIN>
            //        <BINCountry>US</BINCountry>
            //        <ExpMM>01</ExpMM>
            //        <ExpYY>10</ExpYY>
            //        <Last4>1234</Last4>
            //        <Holder>John Smith</Holder>
            //    </CreditCard>
            //    <Address>
            //        <Line1>99 Broadway Ave.</Line1>
            //        <Line2></Line2>
            //        <City>Massapequa</City>
            //        <Postal>11758</Postal>
            //        <State>NY</State>
            //        <Country>US</Country>
            //    </Address>
            //</Trans>

            public int ID { get; set; }
            public decimal Amount { get; set; }
            public string Card { get; set; }
            public DateTime InsertDate { get; set; }
            public string Currency { get; set; }
            public int Installments { get; set; }
            public string Comment { get; set; }
            public string PaymentDetails { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public bool IsRefunded { get; set; }
            public bool IsManual { get; set; }
            public string CardholderName { get; set; }
            public string ApprovalCode { get; set; }
            public string PersonalNumber { get; set; }
        }

        [WebMethod]
        public Result GetTransactions(TransactionType type, DateTime from, DateTime to)
        {
            Action<int, TransactionType, DateTime, DateTime, bool> log = (loginId, transType, transFrom, transTo, isSuccess) =>
            {
                string request = string.Format("merchant id:{0}, transaction type: {1}, from:{2}, to:{3}", loginId, type, from, to);
                string response = isSuccess ? "success" : "fail";
                Netpay.Bll.Process.GenericLog.Log(LogHistoryType.DataPullingRequest, loginId, "data pulling", request, response, null);
            };

            Result result = new Result(true, null);
            Login login = validateCredentials();
            Merchant merchant = login.Items.GetValue<Merchant>("Account"); 

            Transaction.SearchFilters filters = new Transaction.SearchFilters();
            filters.merchantIDs = new List<int>() { merchant.ID };
            filters.date = new Range<DateTime?>(from, to);
            TransactionStatus status = TransactionStatus.Captured;
            switch (type)
            {
                case TransactionType.Captured:
                    status = TransactionStatus.Captured;
                    break;
                case TransactionType.Declined:
                    status = TransactionStatus.Declined;
                    break;
                case TransactionType.Approval:
                    status = TransactionStatus.Authorized;
                    break;
                default:
                    break;
            }

            var resultData = Transaction.Search(status, filters, null, false, false);
            result.IsSuccess = true;
            result.Transactions = resultData.Select(t => new TransactionInfo(t)).ToArray();
            log(login.LoginID, type, from, to, result.IsSuccess);

            return result;
        }
    }
}
