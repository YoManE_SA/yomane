﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.WebServices
{
	/// <summary>
	/// Summary description for AppIdentity
	/// </summary>
	[WebService(Namespace = "WebServices")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	[System.Web.Script.Services.ScriptService]
	public class AppIdentity : System.Web.Services.WebService
	{
        public AppIdentity()
        {
            Code.Utils.RegisterEmailTemplates();
        }

        public class KeyValue<TKey, TValue>
        {
            public TKey Key { get; set; }
            public TValue Value { get; set; }
            public KeyValue() { }
            public KeyValue(TKey key, TValue value) { Key = key; Value = value; }
        }

		public class AppIdentityDetails
		{
			public string Name { get; set; }
			public string BrandName { get; set; }
			public string CompanyName { get; set; }
			public string DomainName { get; set; }
			public string Theme { get; set; }
			public string URLDevCenter { get; set; }
			public string URLProcess { get; set; }
			public string URLMerchantCP { get; set; }
			public string URLWallet { get; set; }
			public string URLWebsite { get; set; }
			//public string SmtpServer { get; set; }
			//public string SmtpUsername { get; set; }
			//public string SmtpPassword { get; set; }
			//public string EmailFrom { get; set; }
			//public string EmailContactTo { get; set; }
			public string CopyRightText { get; set; }
			public bool IsActive { get; set; }

			public AppIdentityDetails() {}
			public AppIdentityDetails(Bll.ApplicationIdentity value)
			{
				Name = value.Name;
				BrandName = value.BrandName;
				CompanyName = value.CompanyName;
				DomainName = value.DomainName;
				Theme = value.ThemeName;
				//ContentFolder = value.ContentFolder;
				URLDevCenter = value.URLDevCenter;
				URLProcess = value.URLProcess;
				URLMerchantCP = value.URLMerchantCP;
				URLWallet = value.URLWallet;
				URLWebsite = value.URLWebsite;
				//SmtpServer = value.SmtpServer;
				//SmtpUsername = value.SmtpUsername;
				//SmtpPassword = value.SmtpPassword;
				//EmailFrom = value.EmailFrom;
				//EmailContactTo = value.EmailContactTo;
				CopyRightText = value.CopyRightText;
				IsActive = value.IsActive;
			}
		}

		private Bll.ApplicationIdentity getIdentity()
		{
			var appToken = Context.Request.Headers["applicationToken"].ToNullableGuid();
			if (appToken == null) throw new Infrastructure.NotLoggedinException();
			var ret = Bll.ApplicationIdentity.GetIdentity(appToken.Value);
			if (ret == null) throw new Infrastructure.NotLoggedinException();
			return ret;
		}

		[WebMethod]
		public AppIdentityDetails GetIdentityDetails()
		{
			return new AppIdentityDetails(getIdentity());
		}

		[WebMethod]
		public void Log(int severityId, string message, string longMessage)
		{
			//var app = getIdentity();
			Netpay.Infrastructure.Logger.Log((LogSeverity)severityId, LogTag.MobileApp, message, longMessage);
		}

		[WebMethod]
		public void SendContactEmail(string from, string subject, string body)
		{
			getIdentity().SendEmail(from, subject, body);
		}

		[WebMethod]
		public string GetContent(string contentName)
		{
			return getIdentity().GetContent(contentName);
		}

		[WebMethod]
		public List<int> GetSupportedPaymentMethods() 
		{
			return getIdentity().SupportedPaymentMethods.Select(p => (int)p).ToList();
		}

		[WebMethod]
		public List<string> GetSupportedCurrencies()
		{
			return getIdentity().SupportedCurrencies;
		}

		[WebMethod]
		public List<KeyValue<int, string>> GetMerchantGroups()
		{
            return Bll.Merchants.Group.Cache.Where(v => getIdentity().RelatedMerchantGroups.Contains(v.ID)).Select(v => new KeyValue<int, string>(v.ID, v.Name)).ToList();
		}
		[WebMethod]
		public List<KeyValue<string, decimal>> GetCurrencyRates()
		{
            return Bll.Currency.Cache.Where(c => GetSupportedCurrencies().Contains(c.IsoCode)).Select(c => new KeyValue<string, decimal>(c.IsoCode, c.BaseRate)).ToList();
		}

	}
}
