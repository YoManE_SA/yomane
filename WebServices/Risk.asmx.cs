﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Infrastructure.Security;
using System.Text;
using Netpay.Bll;

namespace Netpay.WebServices
{
    /// <summary>
    /// Merchant Risk Services
    /// </summary>
    [WebService(Namespace = "WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class Risk : Code.BaseService
    {
        public enum BlockValueType
        {
            CreditCard,
            Email,
            FullName,
            PhoneNumber,
            PersonalID,
            Bin,
            BinCountry
            //IpAddress
        }

        public enum BlockAction { Add, Remove, Check, }

        public class BlockItemValue
        {
            [System.Xml.Serialization.XmlAttribute("Key")]
            public string Key;
            [System.Xml.Serialization.XmlAttribute("Value")]
            public string Value;
        }

        public class BlockItem
        {
            [System.Xml.Serialization.XmlAttribute("Type")]
            public BlockValueType Type;
            [System.Xml.Serialization.XmlAttribute("Action")]
            public BlockAction Action;
            public string Comment;
            public BlockItemValue[] Values;
        }

        public class BlockItemResult
        {
            [System.Xml.Serialization.XmlAttribute("key")]
            public string key;
            [System.Xml.Serialization.XmlAttribute("Exist")]
            public bool Exist;
            //[System.Xml.Serialization.XmlAttribute("Insert")]
            public DateTime? Insert;
        }

		private  Bll.Risk.RiskItem.RiskValueType MapValueType(BlockValueType valueType)
		{
			switch (valueType)
            {
                case BlockValueType.CreditCard: return Bll.Risk.RiskItem.RiskValueType.AccountValue1;
				case BlockValueType.Email: return Bll.Risk.RiskItem.RiskValueType.Email;
				case BlockValueType.FullName: return Bll.Risk.RiskItem.RiskValueType.FullName;
				case BlockValueType.PhoneNumber: return Bll.Risk.RiskItem.RiskValueType.Phone;
				case BlockValueType.PersonalID: return Bll.Risk.RiskItem.RiskValueType.PersonalNumber;
				case BlockValueType.Bin: return Bll.Risk.RiskItem.RiskValueType.Bin;
				case BlockValueType.BinCountry: return Bll.Risk.RiskItem.RiskValueType.BinCountry;
            }
			return Bll.Risk.RiskItem.RiskValueType.AccountValue1;
		}

        public Risk() : base(new UserRole[] { UserRole.Merchant }) { }

		[WebMethod]
        public BlockItemResult[] ManageBlocks(BlockItem[] items, string signature)
        {
			validateCredentials();
			var merchant = Bll.Merchants.Merchant.Current;
			if (signature != Netpay.Infrastructure.Security.Encryption.GetSHA256Hash(items.Count() + merchant.HashKey))
                throw new Exception("invalid signature");

            List<BlockItemResult> result = new List<BlockItemResult>();
            foreach (var it in items)
            {
				var values = it.Values.Select(i => new Bll.Risk.RiskItem.RiskItemValue() { ValueType = MapValueType(it.Type), Value = i.Value }).ToList();
				BlockItemResult ir = new BlockItemResult();
				DateTime? dtNotFound = null;
				var searchResult = Netpay.Bll.Risk.RiskItem.Search(true, Bll.Risk.RiskItem.RiskSource.Merchant, values);
				var res = (from v in it.Values join t in searchResult on v.Value equals t.Value select new BlockItemResult() { Exist = true, key= v.Key, Insert = t.InsertDate }).ToList();
                switch (it.Action)
                {
                    case BlockAction.Add:
						Netpay.Bll.Risk.RiskItem.Create(Bll.Risk.RiskItem.RiskSource.Merchant, Bll.Risk.RiskItem.RiskListType.Black, values, null, null, null, null, null, null, null, it.Comment, null);
						dtNotFound = DateTime.Now;
                        break;
                    case BlockAction.Remove:
						Netpay.Bll.Risk.RiskItem.Delete(MapValueType(it.Type), Bll.Risk.RiskItem.RiskListType.Black, searchResult.Select(t => t.ID).ToList());
                        break;
                    case BlockAction.Check:
                        break;
                }
				result.AddRange(res);
				result.AddRange(it.Values.Where(v => !searchResult.Select(t => t.Value).Contains(v.Value)).Select(v => new BlockItemResult() { Exist = false, key = v.Key, Insert = dtNotFound }));
            }

            // save log
            StringBuilder request = new StringBuilder();
            foreach (BlockItem item in items)
            {
                request.AppendLine("action: " + item.Action);
                request.AppendLine("type: " + item.Type);
                request.AppendLine("comment: " + item.Comment);
                foreach (BlockItemValue value in item.Values) 
                {
                    string val = value.Value;
                    if (item.Type == BlockValueType.CreditCard)
                        val = (val.Length > 4 ? val.Substring(val.Length - 4) : value.Value);
                    request.AppendLine("key: " + value.Key + " value: " + val);
                }
                        
                request.AppendLine();
            }

            StringBuilder response = new StringBuilder();
            foreach (BlockItemResult item in result)
            {
                response.AppendLine("exist: " + item.Exist);
                response.AppendLine("insert: " + item.Insert);
                response.AppendLine("key: " + item.key);
                response.AppendLine();
            }

			Bll.Process.GenericLog.Log(HistoryLogType.BlockedItemsManagementV2, merchant.ID, request.ToString(), response.ToString());

            return result.ToArray();
        }
    }
}
