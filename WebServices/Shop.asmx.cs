﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Infrastructure;

using Netpay.Web;
using System.IO;
using System.Net;
using Netpay.Bll.Merchants;
using Netpay.WebServices.V2;

namespace Netpay.WebServices
{
	/// <summary>
	/// Summary description for Shop
	/// </summary>
	[WebService(Namespace = "WebServices")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	[System.Web.Script.Services.ScriptService]
	public class Shop : System.Web.Services.WebService, System.Web.SessionState.IRequiresSessionState
	{
        public Shop()
        {
            Code.Utils.RegisterEmailTemplates();
        }
        #region Service Classes

        public class ShopSearchFilter
		{
			public bool PromoOnly;
			public string MerchantNumber;
			public string Text;
			public string Language;
			public List<int> Categories;
			public List<int> MerchantGroups;
			//public bool loadMerchant;
		}

        public class ShopMerchantFilters 
        { 
            public string ApplicationToken; 
            public int? GroupId;
            public string Text;
            public int? MerchantStatus;
        }

		public class ShopAddress
		{
			public ShopAddress() { }
			public string AddressLine1 { get; set; }
			public string AddressLine2 { get; set; }
			public string City { get; set; }
			public string PostalCode { get; set; }
			public string StateIso { get; set; }
			public string CountryIso { get; set; }
			#region Save And Load
			internal void Save(Bll.Accounts.AccountAddress value)
			{
				value.AddressLine1 = AddressLine1;
				value.AddressLine2 = AddressLine2;
				value.City = City;
				value.PostalCode = PostalCode;
				value.StateISOCode = StateIso;
				value.CountryISOCode = CountryIso;
			}
			internal ShopAddress(Bll.Accounts.AccountAddress cs)
			{
				if (cs == null) return;
				AddressLine1 = cs.AddressLine1;
				AddressLine2 = cs.AddressLine2;
				City = cs.City;
				PostalCode = cs.PostalCode;
				StateIso = cs.StateISOCode;
				CountryIso = cs.CountryISOCode;
			}
			#endregion
		}

		public class ShopMerchant : ShopAddress
		{
			public string Name { set; get; }
			public string Number { set; get; }
			public string Email { set; get; }
			public string WebsiteUrl { set; get; }
			public string PhoneNumber { set; get; }
			public string FaxNumber { set; get; }
			public string Group { get; set; }

			public List<string> Currencies { get; set; }
			/*shop settings*/
			public string LogoUrl { set; get; }
			public string BannerUrl { set; get; }
			public string BannerLinkUrl { set; get; }
			public string UIBaseColor { set; get; }
			public string FacebookUrl { set; get; }
			public string GooglePlusUrl { set; get; }
			public string TwitterUrl { set; get; }
			public string VimeoUrl { set; get; }
			public string YoutubeUrl { set; get; }
			public string LinkedinUrl { set; get; }
			public string PinterestUrl { set; get; }
			#region Save And Load
			public ShopMerchant() { }
			internal ShopMerchant(Bll.Merchants.MerchantPublicInfo merchant, bool withSettings)
				: base(merchant.BusinessAddress)
			{
				if (merchant == null) return;
				Number = merchant.Number;
				Name = merchant.Name;
				Email = merchant.EmailAddress;
				WebsiteUrl = merchant.WebsiteUrl;
				PhoneNumber = merchant.Phone;
				FaxNumber = merchant.Fax;
				if (merchant.GroupID != null) Group = Bll.Merchants.Group.Get(merchant.GroupID.Value).Name;
				if (!withSettings) return;
				if (merchant.ShopSettings != null) {
                    LogoUrl = (new System.Uri(new System.Uri(Domain.Current.WebServicesUrl), Bll.Accounts.Account.MapPublicVirtualPath(merchant.Number, merchant.ShopSettings.LogoFileName))).ToString();
                    BannerUrl = (new System.Uri(new System.Uri(Domain.Current.WebServicesUrl), Bll.Accounts.Account.MapPublicVirtualPath(merchant.Number, merchant.ShopSettings.BannerFileName))).ToString();
                    BannerLinkUrl = merchant.ShopSettings.BannerLinkUrl;
					UIBaseColor = merchant.ShopSettings.UIBaseColor;
					FacebookUrl = merchant.ShopSettings.FacebookUrl;
					GooglePlusUrl = merchant.ShopSettings.GooglePlusUrl;
					TwitterUrl = merchant.ShopSettings.TwitterUrl;
					VimeoUrl = merchant.ShopSettings.VimeoUrl;
					YoutubeUrl = merchant.ShopSettings.YoutubeUrl;
					LinkedinUrl = merchant.ShopSettings.LinkedinUrl;
					PinterestUrl = merchant.ShopSettings.PinterestUrl;
				}
				Currencies = Bll.Merchants.Merchant.GetSupportedCurrencies(merchant.ID).Select(c => c.IsoCode).ToList();
			}
			#endregion
		}		
		
		public class ShopProductCategory
		{
			public ShopProductCategory() { }
			public int ID { get; set; }
			public string Name { get; set; }
			public List<ShopProductCategory> SubCategories { get; set;}
			internal ShopProductCategory(Bll.Shop.Products.Category value)
			{
				ID = value.ID;
				Name = value.Name;
				if (value.Values != null) 
					SubCategories = value.Values.Select(v => new ShopProductCategory(v)).ToList();
			}
		}

		public class ShopProduct
		{
			public ShopProduct() { }
			internal ShopProduct(Bll.Shop.Products.Product ppo, Bll.Merchants.MerchantPublicInfo merchant, string language)
			{
				ID = ppo.ID;
				SKU = ppo.SKU;
				var text = ppo.GetTextForLanguage(language);
				if(text != null) {
					Name = text.Name;
					Description = text.Description;
					Meta_Title = text.MetaTitle;
					Meta_Description = text.MetaDescription;
					Meta_Keywords = text.MetaKeyword;
				}
				Price = ppo.Price;
				IsDynamicAmount = ppo.IsDynamicProduct;

				this.Type = (byte)ppo.Type;
				QuantityMin = ppo.QtyStart;
				QuantityMax = ppo.QtyEnd;
				QuantityInterval = ppo.QtyStep;
				QuantityAvailable = ppo.ActualQuantity;

				Currency = ppo.CurrencyISOCode;
				if (merchant == null) merchant = Bll.Merchants.MerchantPublicInfo.Load(ppo.MerchantID);
				if (merchant != null) Merchant = new ShopMerchant(merchant, false);     
				if (Merchant == null) return;

				if (!string.IsNullOrEmpty(ppo.ImageFileName))
                    ImageURL = (new System.Uri(new System.Uri(Domain.Current.WebServicesUrl), ppo.ImageURL)).ToString();
				ProductURL = ppo.ProductURL; 
				PaymentMethods = Bll.Merchants.Hosted.PaymentPage.GetAvailablePaymentMethods(ppo.MerchantID, Bll.Currency.Get(ppo.CurrencyISOCode).ID, false).Select(p => p.ID).ToList();
				Properties = ppo.Properties.Select(s => new ShopProductProperty(s)).ToList();
				Stocks = ppo.Stocks.Select(s => new ShopProductStock(s)).ToList();
                Categories = ppo.Categories;
			}

			public ShopProduct(Bll.Merchants.MerchantPublicInfo merchant) { Merchant = new ShopMerchant(merchant, false); }

			public int ID { set; get; }
			public string SKU { set; get; }
			public string Name { set; get; }
			public string Description { set; get; }
			public string ImageURL { set; get; }
			public string ProductURL { set; get; }
			public decimal Price { set; get; }
			public string Currency { set; get; }
			public List<int> PaymentMethods { get; set; }
			public bool IsDynamicAmount { set; get; }
			public byte Type { get; set; }
			public int QuantityMin { set; get; }
			public int QuantityMax { set; get; }
			public int QuantityInterval { set; get; }
			public int? QuantityAvailable { set; get; }
			public ShopMerchant Merchant { get; set; }

			public string Meta_Title { get; set; }
			public string Meta_Description { get; set; }
			public string Meta_Keywords { get; set; }

			public List<ShopProductProperty> Properties { get; set; }
			public List<ShopProductStock> Stocks { get; set; }
            public List<short> Categories { get; set; }

		}

		public class ShopProductProperty
		{
            public ShopProductProperty() { }
			public int ID { get; set; }
			public string Text { get; set; }
			public string Type { get; set; }
			public string Value { get; set; }


			public List<ShopProductProperty> Values { get; set; }
			#region Save And Load
			internal ShopProductProperty(Bll.Shop.Products.Property value)
			{
				ID = value.ID;
				Text = value.Name;
				Type = value.Type.ToString();
				Value = value.Value;
				if(value.Values != null) 
					Values = value.Values.Select(v => new ShopProductProperty(v)).ToList();
			}
			#endregion
		}

		public class ShopProductStock
		{
            public ShopProductStock() { }
			public int ID { get; set; }
			public string SKU { get; set; }
			public int? QuantityAvailable { set; get; }
			public List<int> PropertyValues { get; set; }
			
			#region Save And Load
			internal ShopProductStock(Bll.Shop.Products.Stock value)
			{
				ID = value.ID;
				SKU = value.SKU;
				PropertyValues = value.Properties.Select(p => p.ID).ToList();
				QuantityAvailable = value.QtyAvailable;
			}
			#endregion
		}

		public class ShopCartItemProperty
		{
			public ShopCartItemProperty() { }
			//public int ID { get; set; }
			public int? PropertyID { get; set; }
			public string Name { get; set; }
			public string Value { get; set; }
			#region Save And Load
			internal ShopCartItemProperty(Bll.Shop.Cart.BasketItemProperty value)
			{
				//ID = value.ID;
				PropertyID = value.PropertyID;
				Name = value.Name;
				Value = value.Value;
			}
			#endregion
		}

		public class ShopCartItem 
		{
            public ShopCartItem() { }
			public int ID { get; set; }
			public int? ProductId { get; set; }
			public int? ProductStockId { get; set; }
			public string ProductImageUrl { get; set; }

			public System.DateTime InsertDate { get; set; }
			public string Name { get; set; }
			public short Quantity { get; set; }
			public decimal Price { get; set; }
			public string CurrencyISOCode { get; set; }
			public decimal CurrencyFXRate { get; set; }
			public byte Type { get; set; }

			public decimal ShippingFee { get; set; }
			public decimal VATPercent { get; set; }

			public decimal TotalShipping { get; set; }
			public decimal TotalProduct { get; set; }
			public decimal Total { get; set; }

			public int? MinQuantity { get; set; }
			public int? MaxQuantity { get; set; }
			public int? StepQuantity { get; set; }
			
			public string DownloadMediaType { get; set; }
			public string GuestDownloadUrl { get; set; }
			public string ReceiptLink { get; set;}
			public string ReceiptText { get; set; }

            public decimal ChangedPrice { get; set; }
            public decimal ChangedTotal { get; set; }
            public string ChangedCurrencyIsoCode { get; set; }
            public bool IsAvailable { get; set; }
            public bool IsChanged { get; set; }

			public List<ShopCartItemProperty> ItemProperties { get; set; }

			#region Save And Load
			internal ShopCartItem(Bll.Shop.Cart.BasketItem bi)
			{
				ID = bi.ID;
				ProductId = bi.ProductId;
				ProductStockId = bi.ProductStockId;
				InsertDate = bi.InsertDate;
				Name = bi.Name;
				Quantity = bi.Quantity;
				Price = bi.Price;
				CurrencyISOCode = bi.CurrencyISOCode;
				CurrencyFXRate = bi.CurrencyFXRate;
				this.Type = (byte)bi.Type;
				ShippingFee = bi.ShippingFee;
				VATPercent = bi.VATPercent;
				TotalShipping = bi.TotalShipping;
				TotalProduct = bi.TotalProduct;
				Total = bi.Total;
				MinQuantity = bi.MinQuantity.GetValueOrDefault(0);
				MaxQuantity = bi.MaxQuantity.GetValueOrDefault(int.MaxValue);
				StepQuantity = bi.StepQuantity.GetValueOrDefault(1);
                ChangedPrice = bi.Changes.Price;
                ChangedCurrencyIsoCode = bi.Changes.CurrencyIsoCode;
                ChangedTotal = bi.Changes.Total;
                IsAvailable = bi.Changes.IsAvailable;
                IsChanged = bi.Changes.IsChanged;
				if (bi.Product != null) {
					ProductImageUrl = bi.Product.ImageURL;
					if (bi.Type == Bll.Shop.Products.ProductType.Download && !string.IsNullOrEmpty(bi.Product.MediaFileName))
					{
						DownloadMediaType = System.IO.Path.GetExtension(bi.Product.MediaFileName).TrimStart('.').ToUpper();
						GuestDownloadUrl = bi.GuestDownloadLink;
					}
					if (bi.Product.Text != null)
					{
						ReceiptLink = bi.Product.Text.ReceiptLink;
						ReceiptText = bi.Product.Text.ReceiptText;
					}
				}
				ItemProperties = bi.ItemProperties.Select(ip => new ShopCartItemProperty(ip)).ToList();
			}
			#endregion
		}

		public class ShopCart 
		{
            public ShopCart() { }
			public string Cookie { get; set; }
			public List<ShopCartItem> Items { get; set; }
			public decimal Total { get; set; }
			public string CurrencyIso { get; set; }
			public string CheckoutUrl { get; set; }
			public string MerchantReference { get; set; }
			public string MerchantNumber { get; set; }
			public ShopMerchant Merchant { get; set; }
            public bool IsChanged { get; set; }
            public decimal ChangedTotal { get; set; }
            public byte Installments { get; set; }
            public byte MaxInstallments { get; set; }

			#region Save And Load
			internal ShopCart(Bll.Shop.Cart.Basket basket, string applicationToken)
			{
				MerchantReference = basket.MerchantReference;
				CurrencyIso = basket.CurrencyISOCode;
                Total = basket.Total;
                IsChanged = basket.Changes.IsChanged;
                ChangedTotal = basket.Changes.Total;
				Merchant = new ShopMerchant(basket.MerchantInfo, false);
				MerchantNumber = basket.MerchantInfo.Number;
				Items = basket.BasketItems.Select(bi => new ShopCartItem(bi)).ToList();
                Cookie = Shop.EncodeBasketCookie(applicationToken, basket);
                MaxInstallments = 1;

                if (basket.MerchantInfo.ShopSettings != null)
                { 
                    List<Netpay.Bll.Shop.MerchantSettings.InstallmentStep> steps = basket.MerchantInfo.ShopSettings.InstallmentSteps;
                    if (steps != null && steps.Count > 0)
                    {
                        foreach (var step in steps)
                        {
                            if (Total > step.Amount)
                                MaxInstallments = step.MaxInstallments;
                        }
                    }
                    Installments = basket.Installments > MaxInstallments ? MaxInstallments : basket.Installments;
                }
                int itemCount = basket.BasketItems.Count;

                var vars = HttpUtility.ParseQueryString("");
				vars.Add("merchantID", Merchant.Number);
				vars.Add("trans_installments", Installments.ToString());
				vars.Add("trans_amount", Total.ToString("0.00"));
				vars.Add("trans_currency", basket.CurrencyISOCode);
				vars.Add("disp_payFor", HttpUtility.UrlEncode(string.Format("Cart#{0} {1} {2}", basket.ID.ToString(), itemCount, (itemCount == 1 ? "Item" : "Items"))));
				vars.Add("shop_cartId", HttpUtility.UrlEncode(basket.Identifier.ToString()));
				vars.Add("signature", (vars.GetValuesString() + basket.MerchantInfo.HashCode).ToSha256());
				CheckoutUrl = WebUtils.CurrentDomain.ProcessV2Url + "/Hosted/?" + vars.ToString();
			}
			#endregion
		}

		public class ShopSessionOptions
		{
			public string SuccessUrl { get; set; }
			public string DeclineUrl { get; set; }
			public string PendingUrl { get; set; }
			public int ImageWidth { get; set; }
			public int ImageHeight { get; set; }
		}
		#endregion

		private void SetContext(string credentialsToken)
		{
			Guid creds = Guid.Parse(credentialsToken);
			Infrastructure.ObjectContext.Current.IsUserOfType(Infrastructure.Security.UserRole.Merchant);
		}

        private ShopSessionOptions _sessionOptions;
        private ShopSessionOptions SessionOptions {
            get {
                if (_sessionOptions != null) return _sessionOptions;
                if (Infrastructure.Security.Login.Current == null) return null;
                var jsc = new System.Web.Script.Serialization.JavaScriptSerializer();
                var data = Infrastructure.Security.Login.Current.GetSessionItem("WS-Shop_SessionOptions");
                if (data == null) return null;
                _sessionOptions = jsc.Deserialize<ShopSessionOptions>(data);
                return _sessionOptions;
            }
        }

        [WebMethod(EnableSession = true)]
		public void SetSession(string applicationToken, ShopSessionOptions options)
		{
            _sessionOptions = options;
            var jsc = new System.Web.Script.Serialization.JavaScriptSerializer();
            Infrastructure.Security.Login.Current.SetSessionItem("WS-Shop_SessionOptions", jsc.Serialize(_sessionOptions));
		}

		private bool ValidateWalletCredentias(string walletCredentials, bool redirect = true)
		{
			Infrastructure.ObjectContext.Current.CredentialsToken = walletCredentials.ToNullableGuid().GetValueOrDefault();
			//var user = Infrastructure.Security.Login.Get(walletCredentials.ToNullableGuid().GetValueOrDefault(), false);
			if (Infrastructure.ObjectContext.Current.User == null)
			{
				if (!redirect) return false;
				Context.Response.Buffer = false;
				Context.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
				Context.Response.StatusDescription = "Not logged in";
				Context.Response.Write("Not logged in");
				Context.Response.End();
			}
			return true;
		}
		
		private void ResponseHttpError(string errorString, bool plainText = false, System.Net.HttpStatusCode code = System.Net.HttpStatusCode.InternalServerError)
		{
			if (!plainText) new Exception(errorString);
			Context.Response.Buffer = false;
			Context.Response.StatusCode = (int)code;
			Context.Response.StatusDescription = errorString;
			Context.Response.Write(errorString);
			Context.Response.End();
		}

		private Bll.International.Language GetLanguage(string language)
		{
			return Bll.International.Language.Cache.Where(l => l.IsoCode2 == language).SingleOrDefault();
		}

        [WebMethod(EnableSession = true)]
		public List<ShopProduct> GetProducts(ShopSearchFilter filters, int page, int pageSize)
		{
            //Bll.Process.GenericLog.Log(CommonTypes.LogHistoryType.Other, null,"GetProducts - start" ,"","", "Fail");
			var sf = new Bll.Shop.Products.Product.SearchFilters() { IsActive = true };
			sf.LanguageIso = filters.Language;
			if (!string.IsNullOrEmpty(filters.MerchantNumber)) sf.MerchantId = Bll.Merchants.Merchant.CachedNumbersForDomain()[filters.MerchantNumber];
			sf.PromoOnly = filters.PromoOnly;
			sf.Text = filters.Text;
			sf.Categoryies = filters.Categories;
			if (filters.MerchantGroups != null && filters.MerchantGroups.Count > 0) sf.MerchantGroupID = filters.MerchantGroups; 
			var products = Bll.Shop.Products.Product.Search(sf, new Infrastructure.SortAndPage() { PageCurrent = --page, PageSize = pageSize });
			var merchants = Bll.Merchants.MerchantPublicInfo.Load(products.Select(p => p.MerchantID).Distinct().ToList(), null);
			var ret = products.Select(p => new ShopProduct(p, merchants.Where(m => m.ID == p.MerchantID).SingleOrDefault(), filters.Language)).ToList();
            
            if(SessionOptions != null){
                foreach (var item in ret)
                {
                    string newFileName = Path.GetFileNameWithoutExtension(item.ImageURL) + "_" + SessionOptions.ImageHeight + "x" + SessionOptions.ImageWidth + Path.GetExtension(item.ImageURL);
                    item.ImageURL = Domain.Current.WebServicesUrl + CreateCachedImage(item.ImageURL, item.Merchant.Number, newFileName, SessionOptions.ImageWidth, SessionOptions.ImageHeight, false);
                }
            }
			return ret;
		}

        private string CreateCachedImage(string originalFileName, string merchantNumber, string newFileName, int newWidth, int maxHeight, bool OnlyResizeIfWider)
        {            
            var pyhiscalCacheFileName = System.IO.Path.Combine(Bll.Accounts.Account.MapPublicPath(merchantNumber, "Cache"), newFileName);
            var virtualCacheFileName = System.IO.Path.Combine(Bll.Accounts.Account.MapPublicVirtualPath(merchantNumber, "Cache"), newFileName);
            if (System.IO.File.Exists(pyhiscalCacheFileName)) return virtualCacheFileName;
            System.Net.WebClient wc = new System.Net.WebClient();
            using (var fs = wc.OpenRead(originalFileName))
            {
                using (var FullsizeImage = System.Drawing.Image.FromStream(fs))
                {
                    if (OnlyResizeIfWider)
                    {
                        if (FullsizeImage.Width <= newWidth)
                        {
                            newWidth = FullsizeImage.Width;
                        }
                    }
                    int NewHeight = FullsizeImage.Height * newWidth / FullsizeImage.Width;
                    if (NewHeight > maxHeight)
                    {
                        // Resize with height instead
                        newWidth = FullsizeImage.Width * maxHeight / FullsizeImage.Height;
                        NewHeight = maxHeight;
                    }

                    using (var NewImage = FullsizeImage.GetThumbnailImage(newWidth, NewHeight, null, IntPtr.Zero))
                        NewImage.Save(pyhiscalCacheFileName);
                    return virtualCacheFileName;
                }
            }
        }

        /*
        [WebMethod(EnableSession = false)]
        public int GetNextProductId(string merchantNumber, int productId)
        {
            int merchantId = 0;
            if (!Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(merchantNumber, out merchantId)) 
                return -1;

            int? id = Bll.Shop.Products.Product.GetNextProductId(merchantId, productId);
            if (id == null)
                return -1;

            return id.Value;
        }

        [WebMethod(EnableSession = false)]
        public int GetPrevProductId(string merchantNumber, int productId)
        {
            int merchantId = 0;
            if (!Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(merchantNumber, out merchantId))
                return -1;

            int? id = Bll.Shop.Products.Product.GetPrevProductId(merchantId, productId);
            if (id == null)
                return -1;

            return id.Value;
        }
        */

        [WebMethod(EnableSession = true)]
		public ShopProduct GetProduct(string merchantNumber, int itemId, string language = null)
		{
            ShopProduct returnShopProduct;
			if (itemId == 0)
			{
				int merchantId = 0;
				if (!Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(merchantNumber, out merchantId)) return null;
				var merchant = Netpay.Bll.Merchants.MerchantPublicInfo.Load(merchantId);
				if (merchant == null) return null;
                returnShopProduct = new ShopProduct(merchant);
			}
			else
			{
				var product = Bll.Shop.Products.Product.Load(itemId);
				if (product == null)
					return null;			
                returnShopProduct = new ShopProduct(product, null, language);
			}            

            if (SessionOptions != null)
            {
                string newFileName = Path.GetFileNameWithoutExtension(returnShopProduct.ImageURL) + "_" + SessionOptions.ImageHeight + "x" + SessionOptions.ImageWidth + Path.GetExtension(returnShopProduct.ImageURL);
                returnShopProduct.ImageURL = Domain.Current.WebServicesUrl + CreateCachedImage(returnShopProduct.ImageURL, returnShopProduct.Merchant.Number, newFileName, SessionOptions.ImageWidth, SessionOptions.ImageHeight, false);
                
            }
            return returnShopProduct;
		}

		private static string EncodeBasketCookie(string applicationToken, Bll.Shop.Cart.Basket basket)
		{
			var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());
			return string.Format("cartId={0}|Signature={1}", HttpUtility.UrlEncode(basket.Identifier.ToString()), (basket.Identifier.ToString() + (appIdentity != null ? appIdentity.ID : 0)).ToSha256());
		}

        public static Bll.Shop.Cart.Basket LoadBasketFromCookie(string applicationToken, string cookie)
		{
			var cookieValues = System.Web.HttpUtility.ParseQueryString(cookie.EmptyIfNull().Replace('|', '&'));
			//var cartWalletId = cookieValues["cartWalletId"].ToNullableInt();
			var cartId = cookieValues["CartId"].ToNullableGuid().GetValueOrDefault();
			Bll.Shop.Cart.Basket basket = null;
			//if (cartWalletId != null) basket = Bll.Shop.Cart.Basket.LoadForCustomer(WebUtils.CurrentDomain.ServiceCredentials, cartWalletId.Value);
            basket = Bll.Shop.Cart.Basket.Load(cartId);	
			if (basket == null) return null;
			if (applicationToken != null && EncodeBasketCookie(applicationToken, basket) != cookie) return null; //new WalletCookieResult(ValidationResult.Invalid_Data)
			return basket;
		}

		[WebMethod]
		public List<ShopCart> GetActiveCarts(string applicationToken, string walletCredentials)
		{
			ValidateWalletCredentias(walletCredentials);
			return Bll.Shop.Cart.Basket.LoadActiveForCustomer(Bll.Customers.Customer.Current.ID).Select(b => new ShopCart(b, applicationToken)).ToList();
			//Infrastructure.ObjectContext.Current.Impersonate(Guid.Empty);
		}

		[WebMethod]
        public ShopCart GetCart(string applicationToken, string cookie)
		{
            var basket = LoadBasketFromCookie(applicationToken, cookie);
			if (basket == null) return null;
            ShopCart cart = new ShopCart(basket, applicationToken);
            return cart;
		}

		[WebMethod]
		public List<ShopMerchant> GetMerchants(ShopMerchantFilters filters)
		{
            var appIdentity = Bll.ApplicationIdentity.GetIdentity(filters.ApplicationToken.ToNullableGuid().GetValueOrDefault());
            var merchants = Bll.Merchants.MerchantPublicInfo.Search(new Bll.Merchants.MerchantPublicInfo.SearchFilters() { ApplicationIdentityId = (appIdentity != null ? (int?)appIdentity.ID : null), Group = filters.GroupId, Text = filters.Text, Status = filters.MerchantStatus.ToNullableEnum<MerchantStatus>(), IsShopEnabled = true }, null);
            return merchants.Select(m => new ShopMerchant(m, true)).ToList();
		}

		[WebMethod]
		public ShopCart GetCartOfTransaction(string applicationToken, string walletCredentials, int transactionId)
		{
			var basket = Bll.Shop.Cart.Basket.LoadForTransaction(TransactionStatus.Captured, transactionId);
			if (basket == null) return null;
			return new ShopCart(basket, applicationToken);
		}

		[WebMethod]
		public List<ShopCartItem> GetDownloads(string applicationToken, string walletCredentials, int page, int pageSize)
		{
			ValidateWalletCredentias(walletCredentials);
			var ret = Bll.Shop.Cart.BasketItem.Search(
				new Bll.Shop.Cart.BasketItem.SearchFilters() { CustomerId = new List<int>() { Bll.Customers.Customer.Current.ID }, ProductType = new List<Bll.Shop.Products.ProductType>() { Bll.Shop.Products.ProductType.Download } },
				new Infrastructure.SortAndPage(page - 1, pageSize));
			//var merchants = Bll.Merchants.MerchantPublicInfo.Load(ret.Select(p => p.MerchantId).Distinct().ToList(), null);
			return ret.Select(b => new ShopCartItem(b)).ToList();
		}

		private byte[] SendFile(string fileName, bool asPlainData)
		{
			if (!System.IO.File.Exists(fileName)) ResponseHttpError("File not exist", asPlainData);
			if (!asPlainData) return System.IO.File.ReadAllBytes(fileName);
			HttpContext.Current.Response.Clear();
			HttpContext.Current.Response.ContentType = "application/octet-stream";
			HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=\"" + System.IO.Path.GetFileName(fileName) + "\"");
			HttpContext.Current.Response.WriteFile(fileName);
			HttpContext.Current.Response.End();
			return null;
		}
        [WebMethod]
		public byte[] Download(string applicationToken, string walletCredentials, int itemId, bool asPlainData)
		{
			ValidateWalletCredentias(walletCredentials);
			var item = Bll.Shop.Cart.BasketItem.Search(new Bll.Shop.Cart.BasketItem.SearchFilters() { ID = new Range<int?>(itemId) }, null).SingleOrDefault();
			if (item == null) ResponseHttpError("Item not found", asPlainData);
			if (item.Basket.TransPass_id == null) ResponseHttpError("Transaction not completed", asPlainData);
			if (item.Product == null) ResponseHttpError("Item file removed", asPlainData);
			return SendFile(item.Product.MediaPysicalPath, asPlainData);
		}

        [WebMethod]
		public byte[] DownloadUnauthorized(string fileKey, bool asPlainData)
		{
			if (fileKey == null) ResponseHttpError("Wrong filekey", asPlainData);
			var keyParam = fileKey.EmptyIfNull().Split('|');
			if (keyParam.Length != 2) ResponseHttpError("Wrong filekey", asPlainData);
			ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
				var item = Bll.Shop.Cart.BasketItem.Search(new Bll.Shop.Cart.BasketItem.SearchFilters() { ID = new Range<int?>(keyParam[0].ToNullableInt().GetValueOrDefault()) }, null).SingleOrDefault();
			ObjectContext.Current.StopImpersonate();
			if (item == null) ResponseHttpError("Item not found", asPlainData);
			if (item.GuestDownloadKey != fileKey) ResponseHttpError("Wrong filekey validation", asPlainData);
			if (item.Basket.TransPass_id == null) ResponseHttpError("Transaction not completed", asPlainData);
			if (DateTime.Now.Subtract(item.Basket.CheckoutDate.GetValueOrDefault(DateTime.MinValue)).TotalDays > 7)
				ResponseHttpError("The file available to download up to 7 dyas from purchase", asPlainData);
			return SendFile(item.Product.MediaPysicalPath, asPlainData);
		}

        [WebMethod]
		public string SetCart(string applicationToken, ShopCart cart, string walletCredentials) /*return cookie*/
		{
			int merchantId = 0;
			Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(cart.MerchantNumber, out merchantId);
			Bll.Shop.Cart.Basket basket = null;
			if (!string.IsNullOrEmpty(cart.Cookie)) {
				basket = LoadBasketFromCookie(applicationToken, cart.Cookie);
				if (basket == null) return null; 
			} else {
				if (walletCredentials != null) {
					ObjectContext.Current.Impersonate(walletCredentials.ToNullableGuid().GetValueOrDefault());
					basket = Bll.Shop.Cart.Basket.LoadActiveForCustomer(Bll.Customers.Customer.Current.ID, merchantId);
				}
				if (basket == null) basket = new Bll.Shop.Cart.Basket(merchantId, cart.MerchantReference, (walletCredentials != null ? (int?)Bll.Customers.Customer.Current.ID : null)) { };
			}
			basket.CurrencyISOCode = cart.CurrencyIso;
            basket.Installments = cart.Installments;
			var items = basket.BasketItems.ToList();
			basket.RemoveAll();
			foreach (var item in cart.Items) {
				Bll.Shop.Cart.BasketItem dataItem = null;
				if (item.ID != 0) dataItem = items.Where(bi => bi.ID == item.ID).SingleOrDefault();
				if (dataItem == null) {
					if (item.ProductId != null) dataItem = new Bll.Shop.Cart.BasketItem(merchantId, item.ProductId.Value, item.ProductStockId, null);
					else dataItem = new Bll.Shop.Cart.BasketItem(merchantId, item.Name, item.Price, item.CurrencyISOCode, item.MinQuantity, item.MaxQuantity, item.StepQuantity);
				}
                if (dataItem.IsDynamicPrice) dataItem.Price = item.Price;
				dataItem.Quantity = item.Quantity;
				/*
				foreach (var ip in item.ItemProperties) {
					Bll.Shop.Cart.BasketItemProperty propDataItem = null; 
					var prop = Bll.Shop.Products.Property.Load(new List<int> { ip.PropertyID.Value }, true, true);
					if (ip.ID == 0) propDataItem = dataItem.ItemProperties.Where(pdi => pdi.ID == ip.ID).SingleOrDefault();
					if (propDataItem == null) propDataItem = new Bll.Shop.Cart.BasketItemProperty(prop);
					dataItem.ItemProperties.Add(propDataItem);
				}
				*/
				basket.AddItem(dataItem);
			}
			basket.Save();
			items = items.Where(i => !basket.BasketItems.Contains(i)).ToList();
			foreach(var item in items) item.Delete();
			return EncodeBasketCookie(applicationToken, basket);
		}

		[WebMethod]
		public List<ShopProductCategory> GetMerchantCategories(string merchantNumber) 
		{
			int merchantId = 0;
			Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(merchantNumber, out merchantId);
			return Bll.Shop.Products.Category.LoadForMerchant(merchantId).Select(c => new ShopProductCategory(c)).ToList();
		}

		[WebMethod]
		public ShopMerchant GetMerchant(string merchantNumber)
		{
			var ret = Bll.Merchants.MerchantPublicInfo.Load(merchantNumber);
			if (ret == null) return null;
			return new ShopMerchant(ret, true);
		}

		[WebMethod]
		public string GetMerchantContent(string applicationToken, string merchantNumber, string language, string contentName)
		{
			var langauge = GetLanguage(language);
			if (langauge == null) return null;
            int? accountId = Bll.Accounts.Account.AccountIDByNumber(Bll.Accounts.AccountType.Merchant, merchantNumber);
            if (!accountId.HasValue) return null;
            return Bll.Merchants.Merchant.GetContent(accountId.Value, (CommonTypes.Language)langauge.ID, contentName);
		}

		[WebMethod]
		public void SendMerchantContactEmail(string applicationToken, string merchantNumber, string from, string subject, string body)
		{
			//.SendEmail(from, subject, body);
		}
        
	}
}
