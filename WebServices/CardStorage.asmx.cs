﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Bll;
using System.IO;
using System.Text;

namespace Netpay.WebServices
{
    public enum CardStorageAction
    {
        Unknown,
        Delete,
        Verify,
        Create
    }

	public enum CardStorageResult
	{
		Unknown,
		Success,
		InvalidCard,
		CardExpired,
		CardNotFound
	}
	
	public class CardStorageItem
    {
        public string Action { get; set; }
        public string CardNumber { get; set; }
        public string ExpirationMonth { get; set; }
        public string ExpirationYear { get; set; }
        public string CardholderFullName { get; set; }
        public string Comment { get; set; }
        public string Result { get; set; }
    }

    /// <summary>
    /// Merchants card storage service
    /// </summary>
    [WebService(Namespace = "WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class CardStorage : Code.BaseService
    {
        public CardStorage() : base(new UserRole[] { UserRole.Merchant }) { }

        [WebMethod]
        public CardStorageItem[] ManageCards(CardStorageItem[] items, string signature)
        {
            validateCredentials();
			var merchant = Bll.Merchants.Merchant.Current;
			if (signature != Encryption.GetSHA256Hash(items.Count() + merchant.HashKey))
                throw new Exception("invalid signature");

            var createItems = items.Where(i => i.Action == CardStorageAction.Create.ToString());
            foreach (var item in createItems)
            {
                var vo = new Bll.Merchants.MethodStorage(Bll.Merchants.Merchant.Current.ID);
				vo.MethodInstance.SetEncryptedData(item.CardNumber, null);
				vo.MethodInstance.SetExpirationMonth(item.ExpirationYear.ToNullableInt().GetValueOrDefault(1900), item.ExpirationMonth.ToNullableInt().GetValueOrDefault(1900));
                vo.Owner_FullName = item.CardholderFullName;
                vo.Comment = item.Comment;
				vo.Save();
				item.Result = CardStorageResult.Success.ToString();
            }

            var deleteItems = items.Where(i => i.Action == CardStorageAction.Delete.ToString());
            foreach (var item in deleteItems)
            {
				var dItem = Bll.Merchants.MethodStorage.Search(new Bll.Merchants.MethodStorage.SearchFilters() { MerchantID = merchant.ID, Value1 = item.CardNumber }, null).SingleOrDefault();
				if (dItem != null) {
					dItem.IsActive = false;
					dItem.Save();
					item.Result = CardStorageResult.Success.ToString();
				} else item.Result = CardStorageResult.CardNotFound.ToString();
            }

            var verifyItems = items.Where(i => i.Action == CardStorageAction.Verify.ToString());
            foreach (var item in verifyItems)
            {
                var dItem = Bll.Merchants.MethodStorage.Search(new Bll.Merchants.MethodStorage.SearchFilters() { MerchantID = merchant.ID, Value1 = item.CardNumber }, null).SingleOrDefault();
				item.Result = (dItem != null ? CardStorageResult.Success : CardStorageResult.CardNotFound).ToString();
			}

            // save log
            StringBuilder builder = new StringBuilder();
            foreach (CardStorageItem item in items) 
            {
                builder.AppendLine("action: " + item.Action);
                builder.AppendLine("cardholder name: " + item.CardholderFullName);
                builder.AppendLine("card number: " + (item.CardNumber.Length > 4 ? item.CardNumber.Substring(item.CardNumber.Length - 4) : item.CardNumber));
                builder.AppendLine("comment: " + item.Comment);
                builder.AppendLine("exp. month: " + item.ExpirationMonth);
                builder.AppendLine("exp. year: " + item.ExpirationYear);
                builder.AppendLine("result: " + item.Result);
                builder.AppendLine();
            }
            Netpay.Bll.Process.GenericLog.Log(HistoryLogType.RemoteCardStorageV2, merchant.ID, builder.ToString(), null);

            return items.ToArray();
        }
    }
}
