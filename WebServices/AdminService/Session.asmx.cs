﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Web;

namespace Netpay.WebServices
{
	/// <summary>
	/// Summary description for Session
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
	public class Session : Code.WebService
	{
		public class LoginResult
		{
			public bool IsLoginSuccessful { get; set; }
			public bool VersionUpdateRequired { get; set; }
			public string UserNumber { set; get; }
			public string LoginMessage { set; get; }
			public DateTime LastLogin { set; get; }
			public string CredentialsToken { set; get; }

			public bool IsDeviceActivated { set; get; }
			public bool IsDeviceRegistered { set; get; }
			public bool IsDeviceBlocked { set; get; }
		}

		[WebMethod]
		public bool KeepAlive()
		{
			Netpay.Infrastructure.Security.User user = Netpay.Infrastructure.Security.SecurityManager.GetInternalUser(CredentialsToken);
			if (user == null) return false;
			return Netpay.Infrastructure.Security.SecurityManager.IsLoggedin(CredentialsToken);
		}

		[WebMethod]
		public void Log(int severityId, string message, string longMessage)
		{
			Netpay.Infrastructure.Logger.Log((LogSeverity)severityId, LogTag.MobileApp, message, longMessage);
		}

		[WebMethod]
		public LoginResult Login(string email, string password, string deviceId, string appVersion)
		{
			if (email == null || password == null)
				return new LoginResult() { IsLoginSuccessful = false };
			if (email.Trim() == string.Empty || password.Trim() == string.Empty)
				return new LoginResult() { IsLoginSuccessful = false };
			if (!email.IsEmail())
				return new LoginResult() { IsLoginSuccessful = false };

			Guid creds = Guid.Empty;
			LoginResult result = new LoginResult();
			Netpay.Infrastructure.LoginResult loginResult = Netpay.Infrastructure.LoginResult.UserNotFound;
			loginResult = Netpay.Infrastructure.Security.SecurityManager.Login(WebUtils.DomainHost, UserType.Customer, null, email, password, null, out creds);
			result.CredentialsToken = (CredentialsToken = creds).ToString();
			result.VersionUpdateRequired = false;
			result.IsLoginSuccessful = (loginResult == Netpay.Infrastructure.LoginResult.Success);
			return result;
		}

		[WebMethod]
		public string UpdatePassword(string oldPassword, string newPassword)
		{
			ChangePasswordResult result = SecurityManager.ChangePassword(CredentialsToken, newPassword, oldPassword, HttpContext.Current.Request.UserHostAddress);
			switch (result)
			{
				case ChangePasswordResult.PasswordConfirmationMissmatch:
				case ChangePasswordResult.WeakPassword:
				case ChangePasswordResult.PasswordAlreadyExists:
				case ChangePasswordResult.PasswordUsedInThePast:
					return "WeakPassword";
				case ChangePasswordResult.WrongCurrentPassword:
					return "WrongCurrentPassword";
				case ChangePasswordResult.Success:
					return "Success";
				default:
					return "GeneralError";
			}
		}
	}
}
