﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.Web;

namespace Netpay.WebServices
{
	/// <summary>
	/// Summary description for Transactions
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
	public class Transactions : Code.WebService
	{
		public class TransactionInfo
		{
			internal TransactionInfo()
			{
			}

			internal TransactionInfo(TransactionVO vo)
			{
				ID = vo.ID;
				Amount = vo.Amount;
				Card = vo.PaymentMethodDisplay;
				InsertDate = vo.InsertDate;
				Currency = vo.CurrencyIsoCode;
				Comment = vo.Comment;
				Email = vo.PaymentMethodData.Email;
				Phone = vo.PaymentMethodData.Phone;
				PayForText = vo.PayForText;
				FullName = vo.PaymentMethodData.Name;
			}

			public int ID { get; set; }
			public decimal Amount { get; set; }
			public string Card { get; set; }
			public DateTime InsertDate { get; set; }
			public string Currency { get; set; }
			public string Comment { get; set; }
			public string Phone { get; set; }
			public string Email { get; set; }
			public string PayForText { get; set; }
			public string FullName { get; set; }
		}

		[WebMethod]
		public TransactionInfo GetTransaction(int transactionId)
		{
			TransactionVO vo = Bll.Transactions.GetTransaction(CredentialsToken, transactionId, TransactionStatus.Captured);
			return new TransactionInfo(vo);
		}

		[WebMethod]
		public List<TransactionInfo> GetTransactionHistory(int page, int pageSize)
		{
			Netpay.Infrastructure.Security.User user = SecurityManager.GetInternalUser(CredentialsToken);
			if (user.Type != UserType.Customer)
				return null;

			SearchFilters filters = new SearchFilters();

			PagingInfo pi = new PagingInfo();
			pi.PageSize = pageSize;
			pi.CurrentPage = page;

			SortingInfo si = new SortingInfo();
			si.Direction = SortDirection.Descending;
			si.Property = "InsertDate";
			si.VOType = typeof(TransactionVO);

			List<TransactionVO> transactions = Bll.Transactions.Search(CredentialsToken, TransactionStatus.Captured, filters, pi, si);
			return transactions.Select(t => new TransactionInfo() { ID = t.ID, Amount = t.Amount, Currency = t.CurrencyIsoCode, PayForText = t.PayForText, InsertDate = t.InsertDate }).ToList();
		}


	}
}
