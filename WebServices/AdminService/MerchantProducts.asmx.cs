﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.WebServices
{
	/// <summary>
	/// Summary description for MerchantProducts
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
	public class MerchantProducts : System.Web.Services.WebService
	{
		public class ProductDetails
		{
			public ProductDetails() { }
			public ProductDetails(Netpay.Infrastructure.Domains.Domain domain, Netpay.Infrastructure.VO.PublicPayChargeOptionVO ppo)
			{
				ID = ppo.ID;
				SKU = ppo.SKU;
				Name = ppo.Text;
				Description = ppo.Description;
				Price = ppo.Amount;
				IsDynamicAmount = ppo.IsDynamicAmount;

				QuantityMin = ppo.QuantityMin;
				QuantityMax = ppo.QuantityMax;
				QuantityInterval = ppo.QuantityInterval;
				QuantityAvailable = ppo.IsActive ? ppo.QuantityAvailable : 0;

				Currency = domain.Cache.GetCurrency(ppo.CurrencyID).IsoCode;

				var merchant = Bll.Merchants.GetMerchant(domain.Host, ppo.MerchantID);
				if (merchant == null) return;

				if (!string.IsNullOrEmpty(ppo.ImageFileName))
					ImageURL = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + FilesManager.GetGlobalAccessVirtualPath(domain.Host, UserType.MerchantPrimary, merchant.Number) + ppo.ImageFileName;

				var options = Bll.PublicPayment.GetOptions(domain.Host, merchant.Number);
				if (!string.IsNullOrEmpty(options.MobileAppLogo))
					MerchantLogoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + FilesManager.GetGlobalAccessVirtualPath(domain.Host, UserType.MerchantPrimary, merchant.Number) + options.MobileAppLogo;

				ProductURL = domain.ProcessV2Url + "Public/Product.aspx?merchantID=" + merchant.Number + "&productID=" + ppo.ID;

				MerchantNumber = merchant.Number;
				MerchantName = merchant.Name;
				MerchantEmail = merchant.EMail;
				WebsiteUrl = merchant.Url;
				PhoneNumber = merchant.Phone;

				Address = merchant.Address;
				City = merchant.City;
				State = merchant.State;
				var countryVo = domain.Cache.GetCountry(merchant.Country.GetValueOrDefault());
				if (countryVo != null) Country = countryVo.Name;
			}

			public ProductDetails(Netpay.Infrastructure.Domains.Domain domain, Netpay.Infrastructure.VO.MerchantVO merchant)
			{
				MerchantLogoUrl = Netpay.Infrastructure.FilesManager.GetGlobalAccessVirtualPath(domain.Host, UserType.MerchantPrimary, merchant.Number) + "logoImage.png";
				MerchantNumber = merchant.Number;
				MerchantName = merchant.Name;
				MerchantEmail = merchant.EMail;
				WebsiteUrl = merchant.Url;
				PhoneNumber = merchant.Phone;
				Address = merchant.Address;
				City = merchant.City;
				State = merchant.State;
				var countryVo = domain.Cache.GetCountry(merchant.Country.GetValueOrDefault());
				if (countryVo != null) Country = countryVo.Name;
			}

			public int ID { set; get; }
			public string SKU { set; get; }
			public string Name { set; get; }
			public string Description { set; get; }
			public string ImageURL { set; get; }
			public string ProductURL { set; get; }
			public decimal Price { set; get; }
			public string Currency { set; get; }
			public bool IsDynamicAmount { set; get; }

			public int QuantityMin { set; get; }
			public int QuantityMax { set; get; }
			public int QuantityInterval { set; get; }
			public int? QuantityAvailable { set; get; }

			public string MerchantLogoUrl { set; get; }
			public string MerchantName { set; get; }
			public string MerchantNumber { set; get; }
			public string MerchantEmail { set; get; }
			public string WebsiteUrl { set; get; }
			public string PhoneNumber { set; get; }
			public string Address { set; get; }
			public string City { set; get; }
			public string State { set; get; }
			public string Country { set; get; }
		}

		[WebMethod]
		public List<ProductDetails> Search(string keyword, Language language, int page, int pageSize)
		{
			//Guid creds = Guid.Parse(credentialsToken);
			//User user = Netpay.Infrastructure.Security.SecurityManager.GetInternalUser(creds);
			List<string> tagsSplit = keyword.Split(' ').Select(t => t.Trim().ToLower()).ToList();
			var products = Bll.PublicPayment.SearchChargeOptions(WebUtils.CurrentDomain.Host, tagsSplit, language, --page, pageSize);
			var results = (from p in products select new ProductDetails(WebUtils.CurrentDomain, p)).ToList();
			return results;
		}

		[WebMethod]
		public List<ProductDetails> GetMerchantProducts(Language language, string merchantNumber, int page, int pageSize)
		{
			var products = Bll.PublicPayment.GetChargeOptions(WebUtils.DomainHost, merchantNumber, language, --page, pageSize);
			return (from p in products select new ProductDetails(WebUtils.CurrentDomain, p)).ToList();
		}

		[WebMethod]
		public List<ProductDetails> GetPromoProducts(Language language, int page, int pageSize)
		{
			//Guid creds = Guid.Parse(credentialsToken);
			//User user = Netpay.Infrastructure.Security.SecurityManager.GetInternalUser(creds);
			var products = Bll.PublicPayment.GetPromoChargeOptions(WebUtils.CurrentDomain.Host, language, --page, pageSize);
			return (from p in products select new ProductDetails(WebUtils.CurrentDomain, p)).ToList();
		}

		[WebMethod]
		public List<Netpay.Infrastructure.VO.ProductPurchasedInRangeVO> SearchProductBySales(double latitude, double longitude, Language language)
		{
			//Guid creds = Guid.Parse(credentialsToken);
			//User user = Netpay.Infrastructure.Security.SecurityManager.GetInternalUser(creds);
			return Bll.PublicPayment.SearchChargeOptionsByLocation(WebUtils.DomainHost, language, latitude, longitude, 32000);
		}

		[WebMethod]
		public ProductDetails GetProductInfo(string merchantNumber, int itemId)
		{
			if (itemId == 0)
			{
				Netpay.Infrastructure.VO.MerchantVO merchant = Netpay.Bll.Merchants.GetMerchant(WebUtils.DomainHost, merchantNumber);
				if (merchant == null) return null;
				return new ProductDetails(WebUtils.CurrentDomain, merchant);
			}
			else
			{
				var product = Bll.PublicPayment.GetChargeOption(Web.WebUtils.DomainHost, merchantNumber, itemId);
				if (product == null) return null;
				return new ProductDetails(WebUtils.CurrentDomain, product);
			}
		}
	}
}
