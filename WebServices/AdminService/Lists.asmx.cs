﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Infrastructure.VO;
using Netpay.Web;

namespace Netpay.WebServices
{
	/// <summary>
	/// Summary description for Lists
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
	public class Lists : Code.WebService
	{
		[WebMethod]
		public List<CountryVO> GetCountries()
		{
			return WebUtils.CurrentDomain.Cache.GetCountries();
		}

		[WebMethod]
		public List<StateVO> GetStates(string countryIso)
		{
			return WebUtils.CurrentDomain.Cache.GetStates();
		}

	}
}
