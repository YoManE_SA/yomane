﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Infrastructure.VO;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.Infrastructure.Security;
using Netpay.Bll;
using Netpay.CommonTypes;
using Netpay.Bll.Customer;

namespace Netpay.WebServices
{
	[WebService(Namespace = "http://netpay-intl.com/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	[System.Web.Script.Services.ScriptService]
	public class Customers : Code.WebService
	{
		public class PayerInfo
		{
			public List<PaymentMethod> PaymentMethods { set; get; }
			public Customer Info { set; get; }
		}

		public class PaymentMethod
		{
			public PaymentMethod() { }
			public PaymentMethod(Domain d, EcheckStorageVO es)
			{
				Id = es.Id;
				MethodType = PaymentMethodType.ECheck;
				TypeName = "ACH";
				Icon = string.Format("{0}NPCommon/ImgPaymentMethodMobile/{1}.png", d.WebServicesUrl, (int)es.PaymentMethod);
				Last4 = es.Last4;
				OwnerName = es.AccountName;
			}

			public PaymentMethod(Domain d, CreditCardStorageVO cs)
			{
				Id = cs.ID;
				MethodType = PaymentMethodType.CreditCard;
				TypeName = cs.PaymentMethod.ToString();
				if (TypeName.StartsWith("CC")) TypeName = TypeName.Substring(2);
				Icon = string.Format("{0}NPCommon/ImgPaymentMethodMobile/{1}.png", d.WebServicesUrl, (int)cs.PaymentMethod);
				Last4 = cs.CcLast4;
				OwnerName = cs.CardholderFullName;
			}
			public int Id { set; get; }
			public PaymentMethodType MethodType { set; get; }
			public string TypeName { set; get; }
			public string Icon { set; get; }
			public string Last4 { set; get; }
			public string OwnerName { set; get; }
			public bool isDefault { set; get; }
		}

		public class ProcessResult
		{
			public bool IsSuccess { get; set; }
			public string TransactionNumber { get; set; }
			public string ReplyCode { set; get; }
			public string ReplyText { get; set; }
			//public string Signature { get; set; }
		}

		[WebMethod]
		public ShippingAddress GetShippingAddress(int addressId) { return ShippingAddress.Load(CredentialsToken, addressId); }

		[WebMethod]
		public List<ShippingAddress> GetShippingAddresses() { return ShippingAddress.LoadForCustomer(CredentialsToken); }

		[WebMethod]
		public void SaveShippingAddress(ShippingAddress address) { address.Save(CredentialsToken); } 
		
		[WebMethod]
		public bool DeleteShippingAddress(int addressId)
		{
			var address = ShippingAddress.Load(CredentialsToken, addressId);
			if (address == null) return false;
			address.Delete(CredentialsToken);
			return true;
		}


		[WebMethod]
		public List<StoredPaymentMethod> GetPaymentMethods() { return StoredPaymentMethod.LoadForCustomer(CredentialsToken); }

		[WebMethod]
		public StoredPaymentMethod GetPaymentMethod(int cardId)
		{
			StoredPaymentMethod result = StoredPaymentMethod.Load(CredentialsToken, cardId);
			return result;
		}

		[WebMethod]
		public int SavePaymentMethod(StoredPaymentMethod card)
		{
			card.Save(CredentialsToken);
			return card.ID;
		}

		[WebMethod]
		public bool DeletePaymentMethod(int cardId)
		{
			var card = StoredPaymentMethod.Load(CredentialsToken, cardId);
			if (card == null) return false;
			card.Delete(CredentialsToken);
			return true;
		}


		[WebMethod]
		public Customer GetCustomer(int? customerId = null)
		{
			return Customer.Load(CredentialsToken, customerId);
		}

		[WebMethod]
		public int SaveCustomer(Customer customer)
		{
			return customer.Save(CredentialsToken);
		}

		[WebMethod]
		public Customer.RegisterUpdateResult RegisterCustomer(Customer customer)
		{
			return customer.Register(WebUtils.DomainHost, null);
		}

		[WebMethod]
		public bool ResetPassword(string email)
		{
			string ip = HttpContext.Current.Request.UserHostAddress;
			bool result = Customer.ResetPassword(WebUtils.CurrentDomain.Host, email, ip);
			return result;
		}


		[WebMethod]
		public string UpdatePincode(int? customerId, string oldPincode, string newPincode)
		{
			var customer = Customer.Load(CredentialsToken);
			return customer.UpdatePincode(CredentialsToken, oldPincode, newPincode).ToString();
		}

		[WebMethod]
		public bool SaveProfileImage(int? customerId, string data)
		{
			var customer = Customer.Load(CredentialsToken);
			try
			{
				byte[] bytes = System.Convert.FromBase64String(data);
				if (bytes.Length > 0x30000) //196,608
					return false;

				var ms = new System.IO.MemoryStream(bytes);
				var image = System.Drawing.Image.FromStream(ms);
				string fullPath = FilesManager.GetPrivateAccessPath(WebUtils.CurrentDomain.Host, UserType.Customer, customer.ID) + "profileImage.png";
				image.Save(fullPath, System.Drawing.Imaging.ImageFormat.Png);

				return true;
			}
			catch (Exception e)
			{
				Logger.Log(e, "save profile image");
				return false;
			}
		}

		[WebMethod]
		public string GetProfileImage(int? customerId)
		{
			string imagePath = FilesManager.GetPrivateAccessPath(CredentialsToken) + "profileImage.png";
			if (System.IO.File.Exists(imagePath))
			{
				byte[] image = System.IO.File.ReadAllBytes(imagePath);
				return image.ToBase64();
			}
			return null;
		}

		private ProcessResult Process(string credentialsToken, string merchantNumber, CommonTypes.PaymentMethodType methodType, int storageId, string pinCode, string orderId, decimal amount, string currency, int installments, string text, int? itemId, int? quantity, double latitude, double longitude)
		{
			Guid creds = Guid.Parse(credentialsToken);
			User user = Netpay.Infrastructure.Security.SecurityManager.GetInternalUser(creds);
			ProcessResult result = new ProcessResult();
			if (!Bll.Customer.Customer.ValidatePinCode(user.Domain.Host, user.ID, pinCode))
			{
				result.ReplyCode = "500";
				result.IsSuccess = false;
				result.ReplyText = "wrong Pincode was entered";
				return result;
			}
			var payerInfo = Customer.Load(creds);
			System.Text.Encoding serviceEncoding = System.Text.Encoding.GetEncoding("windows-1255");
			System.Text.StringBuilder urlBuilder = new System.Text.StringBuilder();
			string serviceBaseUrl = WebUtils.CurrentDomain.ProcessUrl;
			var pMethod = Netpay.Bll.Customer.StoredPaymentMethod.Load(creds, storageId);
			if (pMethod == null) throw new Exception("Card does not exist");
			string value1, value2;
			pMethod.GetDecryptedData(creds, out value1, out value2);
			if (methodType == PaymentMethodType.CreditCard)
			{
				urlBuilder.Append(serviceBaseUrl + "remote_charge.asp");
				urlBuilder.Append("?CompanyNum=" + merchantNumber.ToEncodedUrl(serviceEncoding));
				urlBuilder.Append("&TypeCredit=" + (int)CreditType.Regular);
				urlBuilder.Append("&CardNum=" + value1.ToEncodedUrl(serviceEncoding));
				urlBuilder.Append("&ExpMonth=" + pMethod.ExpirationDate.GetValueOrDefault().ToString("dd"));
				urlBuilder.Append("&ExpYear=" + pMethod.ExpirationDate.GetValueOrDefault().ToString("yyyy"));
				urlBuilder.Append("&CVV2=" + value2.ToEncodedUrl(serviceEncoding));
				urlBuilder.Append("&Member=" + pMethod.OwnerName.ToEncodedUrl(serviceEncoding));
				urlBuilder.Append("&Payments=" + installments);
			}
			else if (methodType == PaymentMethodType.ECheck)
			{
				urlBuilder.Append(serviceBaseUrl + "remoteCharge_echeck.asp");
				urlBuilder.Append("?CompanyNum=" + merchantNumber.ToEncodedUrl(serviceEncoding));
				urlBuilder.Append("&CreditType=" + (int)CreditType.Debit);
				urlBuilder.Append("&AccountName=" + pMethod.OwnerName.ToEncodedUrl(serviceEncoding));
				urlBuilder.Append("&AccountNumber=" + value1.ToEncodedUrl(serviceEncoding));
				urlBuilder.Append("&RoutingNumber=" + value2.ToEncodedUrl(serviceEncoding));
				//urlBuilder.Append("&Comment=" + pCard.Comment.ToEncodedUrl(serviceEncoding));
			} else throw new Exception("methodType not supported");

			if (itemId != null) urlBuilder.Append("&MerchantProductId=" + itemId);
			urlBuilder.Append("&Currency=" + currency.ToString());
			urlBuilder.Append("&Amount=" + amount.ToString("0.00"));
			urlBuilder.Append("&CustomerID=" + user.ID);
			urlBuilder.Append("&ClientIP=" + System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToEncodedUrl());
			urlBuilder.Append("&BirthDate=" + payerInfo.DateOfBirth.GetValueOrDefault().ToString("yyy-MM-dd").ToEncodedUrl(serviceEncoding));
			urlBuilder.Append("&Order=" + orderId.ToEncodedUrl(serviceEncoding));
			urlBuilder.Append("&requestSource=42");
			urlBuilder.Append("&PayFor=" + text.Truncate(100).ToEncodedUrl(serviceEncoding));
			urlBuilder.Append("&Email=" + payerInfo.EmailAddress.ToEncodedUrl(serviceEncoding));
			urlBuilder.Append("&PhoneNumber=" + payerInfo.PhoneNumber.ToEncodedUrl(serviceEncoding));
			urlBuilder.Append("&PersonalNum=" + payerInfo.PersonalNumber.ToEncodedUrl(serviceEncoding));
			//urlBuilder.Append("&Comment=" + pCard.Comment.ToEncodedUrl(serviceEncoding));

			urlBuilder.Append("&BillingAddress1=" + pMethod.BillingAddress.Street1.ToEncodedUrl(serviceEncoding));
			urlBuilder.Append("&BillingAddress2=" + pMethod.BillingAddress.Street2.ToEncodedUrl(serviceEncoding));
			urlBuilder.Append("&BillingCity=" + pMethod.BillingAddress.City.ToEncodedUrl(serviceEncoding));
			urlBuilder.Append("&BillingZipCode=" + pMethod.BillingAddress.PostalCode.ToEncodedUrl(serviceEncoding));
			urlBuilder.Append("&BillingState=" + pMethod.BillingAddress.StateISOCode.ToEncodedUrl(serviceEncoding));
			urlBuilder.Append("&BillingCountry=" + pMethod.BillingAddress.CountryISOCode.ToEncodedUrl(serviceEncoding));

			string response = null;
			try
			{
				var client = new System.Net.WebClient();
				response = client.UploadString(urlBuilder.ToString(), "get");
			}
			catch (System.Net.WebException exception)
			{
				using (var reader = new System.IO.StreamReader(exception.Response.GetResponseStream()))
				{
					response = reader.ReadToEnd();
					Netpay.Infrastructure.Logger.Log(LogSeverity.Error, LogTag.MobileApp, "error while sending request from mobile web services to remote charge", response);

					result.IsSuccess = false;
					result.ReplyText = "Unexpected failure";
					//result.Signature = CalcSignature(user.activationCode, result);
					return result;
				}
			}
			var parsed = HttpUtility.ParseQueryString(response);
			if (methodType == PaymentMethodType.CreditCard)
			{
				result.ReplyCode = parsed.Get("Reply");
				result.ReplyText = parsed.Get("ReplyDesc");
				result.TransactionNumber = parsed.Get("TransID");
			}
			else if (methodType == PaymentMethodType.ECheck)
			{
				result.ReplyCode = parsed.Get("replyCode");
				result.ReplyText = parsed.Get("replyMessage");
				result.TransactionNumber = parsed.Get("transId");
			}
			result.IsSuccess = (result.ReplyCode == "000");
			if (result.IsSuccess && itemId.HasValue)
				Bll.PublicPayment.SaveMerchantProductPurchased(creds, itemId.GetValueOrDefault(), latitude, longitude);
			//result.Signature = CalcSignature(user.activationCode, result);
			return result;
		}

		[WebMethod]
		public ProcessResult ProcessTransaction(string credentialsToken, string merchantNumber, CommonTypes.PaymentMethodType methodType, int storageId, string pinCode, int itemId, int quantity, string text, decimal amount, string currency, int installments, string transQrId, double latitude, double longitude)
		{
			string orderId = null;
			if (itemId != 0)
			{
				var product = Bll.PublicPayment.GetChargeOption(Web.WebUtils.DomainHost, merchantNumber, itemId);
				if (product == null) throw new Exception("product does not exist");
				if (amount == 0) amount = product.Amount;
				if (string.IsNullOrEmpty(currency)) currency = product.CurrencyID.ToString();
				if (string.IsNullOrEmpty(text)) text = product.Text;
			}
			if (!string.IsNullOrEmpty(transQrId)) orderId = "SQR-ID:" + transQrId.ToString();
			return Process(credentialsToken, merchantNumber, methodType, storageId, pinCode, orderId, amount, currency, installments, text, itemId, quantity, latitude, longitude);
			//need to move inventory checks and remove to remoteCharge
		}
	}
}