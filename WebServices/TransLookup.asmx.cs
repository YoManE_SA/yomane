﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Netpay.WebServices
{
	/// <summary>
	/// Summary description for TransVerify
	/// </summary>
	[WebService(Namespace = "WebServices")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	public class TransLookup : System.Web.Services.WebService
	{
        public TransLookup()
        {
            Code.Utils.RegisterEmailTemplates();
        }

        [WebMethod]
		public List<Bll.Transactions.Transaction.TransactionLookupResult> Lookup(DateTime transDate, decimal amount, string last4cc)
		{
			return Bll.Transactions.Transaction.TransLookup(transDate, amount, last4cc);
		}
	}
}
