﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Netpay.WebServices
{
    public static class HttpRequestExtentions
    {
        /// <summary>
        /// Dump the raw http request to a string. 
        /// </summary>
        /// <param name="request">The <see cref="HttpRequest"/> that should be dumped.       </param>
        /// <returns>The raw HTTP request.</returns>
        public static string ToRaw(this HttpRequest request)
        {
            StringWriter writer = new StringWriter();

            WriteStartLine(request, writer);
            WriteHeader(request, writer);
            WriteBody(request, writer);

            return writer.ToString();
        }

        private static void WriteStartLine(HttpRequest request, StringWriter writer)
        {
            const string SPACE = " ";

            writer.Write(request.HttpMethod);
            writer.Write(SPACE + request.RawUrl);
            writer.WriteLine(SPACE + request.ServerVariables["SERVER_PROTOCOL"]);
        }

        private static void WriteHeader(HttpRequest request, StringWriter writer)
        {
            writer.WriteLine(request.ServerVariables["All_RAW"]);
            writer.WriteLine();
        }

        private static void WriteBody(HttpRequest request, StringWriter writer)
        {
            var strm = request.InputStream;
            var currentPosition = strm.Position;
            var ctx = request.RequestContext.HttpContext;
            using (StreamReader reader = new StreamReader(
                strm,
                System.Text.Encoding.Default,
                true,
                1024,
                true))
            {
                try
                {
                    writer.WriteLine("Body:");
                    writer.WriteLine("");

                    string body = reader.ReadToEnd();
                    writer.WriteLine(body);
                }
                finally
                {
                    strm.Position = currentPosition;
                }
            }
        }
    }
}
