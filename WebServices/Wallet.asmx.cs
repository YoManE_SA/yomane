﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System.Net;
using System.IO;
using Netpay.Bll;

using Netpay.CommonTypes;
using System.Drawing;
using System.Drawing.Imaging;

using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;

namespace Netpay.WebServices
{
    [WebService(Namespace = "WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class Wallet : Code.BaseService
    {
        #region Service Classes
        public class WalletResult : ServiceResult
        {
            public WalletResult() { }
            internal WalletResult(ValidationResult result) : base(result) { }
        }

        public class WalletMultiResult : WalletResult
        {
            internal WalletMultiResult(ValidationResult result) : base(result) { }
            public WalletMultiResult() { }
            public string[] RefNumbers { get; set; }
            public int? RecordNumber { get; set; }
        }

        public class WalletLoginOptions
        {
            public bool setCookie { get; set; }
            public string applicationToken { get; set; }
            public string appName { get; set; }
            public string deviceId { get; set; }
            public string pushToken { get; set; }
            public bool? excludeDataImages { get; set; }
        }

        public class WalletLoginResult : WalletResult
        {
            public WalletLoginResult() { }
            public WalletLoginResult(ValidationResult result) : base(result) { }
            public string CredentialsToken { set; get; }
            public bool VersionUpdateRequired { get; set; }
            public DateTime LastLogin { set; get; }
            public bool IsFirstLogin { set; get; }
            public string EncodedCookie { set; get; }
        }

        public class WalletCookieResult : WalletResult
        {
            internal WalletCookieResult(ValidationResult result) : base(result) { }
            public WalletCookieResult() { }
            public string FullName { get; set; }
            public string Email { get; set; }
            public byte[] ProfilePicture { get; set; }
        }

        public class WalletLists
        {
            public class ListItem
            {
                public string Key { get; set; }
                public string Name { get; set; }
                public virtual string Icon { get; set; }
            }

            public List<ListItem> Countries { get; set; }
            public List<ListItem> UsaStates { get; set; }
            public List<ListItem> CanadaStates { get; set; }
            public List<ListItem> Languages { get; set; }
        }

        public class WalletUserLists
        {
            public class PaymentMethod : WalletLists.ListItem
            {
                public string GroupKey { get; set; }
                public string Value1Caption { get; set; }
                public string Value1ValidationRegex { get; set; }
                public string Value2Caption { get; set; }
                public string Value2ValidationRegex { get; set; }
                public bool HasExpirationDate { get; set; }
                public override string Icon { get { return string.Format("{0}NPCommon/ImgPaymentMethod/118X70/{1}.png", Infrastructure.Domain.Get(WebUtils.DomainHost).WebServicesUrl, Key.ToNullableInt().GetValueOrDefault()); } set { } }
            }
            public List<WalletLists.ListItem> PaymentMethodGroups { get; set; }
            public List<PaymentMethod> PaymentMethods { get; set; }
            public List<WalletLists.ListItem> RelationTypes { get; set; }

        }

        public class WalletAddress
        {
            public WalletAddress() { }
            public string AddressLine1 { get; set; }
            public string AddressLine2 { get; set; }
            public string City { get; set; }
            public string PostalCode { get; set; }
            public string StateIso { get; set; }
            public string CountryIso { get; set; }
            #region Save And Load
            internal void Save(Bll.Accounts.AccountAddress value)
            {
                value.AddressLine1 = AddressLine1;
                value.AddressLine2 = AddressLine2;
                value.City = City;
                value.PostalCode = PostalCode;
                value.StateISOCode = StateIso;
                value.CountryISOCode = CountryIso;
            }
            internal WalletAddress(Bll.IAddress cs)
            {
                if (cs == null) return;
                AddressLine1 = cs.AddressLine1;
                AddressLine2 = cs.AddressLine2;
                City = cs.City;
                PostalCode = cs.PostalCode;
                StateIso = cs.StateISOCode;
                CountryIso = cs.CountryISOCode;
            }
            #endregion
        }

        public class WalletCustomer : WalletAddress
        {
            public WalletCustomer() { }
            public System.DateTime RegistrationDate { get; set; }
            public string CustomerNumber { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string PersonalNumber { get; set; }
            public string PhoneNumber { get; set; }
            public string CellNumber { get; set; }
            public System.DateTime? DateOfBirth { get; set; }
            public string EmailAddress { get; set; }
            public long ProfileImageSize { get; set; }
            public byte[] ProfileImage { get; set; }
            #region Save And Load
            internal void Save(Bll.Customers.Customer value)
            {
                value.FirstName = FirstName;
                value.LastName = LastName;
                value.PersonalNumber = PersonalNumber;
                value.PhoneNumber = PhoneNumber;
                value.CellNumber = CellNumber;
                value.DateOfBirth = DateOfBirth;
                value.EmailAddress = EmailAddress;
                if (ProfileImage != null) value.ProfileImageData = ProfileImage;
                if (value.PersonalAddress == null) value.PersonalAddress = new Bll.Accounts.AccountAddress();
                base.Save(value.PersonalAddress);
            }
            internal WalletCustomer(Bll.Customers.Customer cs)
                : base(cs.PersonalAddress)
            {
                RegistrationDate = cs.RegistrationDate;
                CustomerNumber = cs.AccountNumber;
                FirstName = cs.FirstName;
                LastName = cs.LastName;
                PersonalNumber = cs.PersonalNumber;
                PhoneNumber = cs.PhoneNumber;
                CellNumber = cs.CellNumber;
                DateOfBirth = cs.DateOfBirth;
                EmailAddress = cs.EmailAddress;
                ProfileImageSize = cs.ProfileImageSize;
                if (ProfileImageSize > 0 && !Infrastructure.Security.Login.Current.Items.GetValue<object>("excludeDataImages").ToNullableBool().GetValueOrDefault())
                    ProfileImage = cs.ProfileImageData;
            }
            #endregion
        }

        public class WalletRegisterData
        {
            public WalletCustomer info { get; set; }
            public WalletStoredPaymentMethod[] StoredPaymentMethods { get; set; }
            public WalletShippingAddress[] ShippingAddresses { get; set; }
            public string PinCode { get; set; }
            public string Password { get; set; }
            public string ApplicationToken { get; set; }
        }

        public class WalletStoredPaymentMethod
        {
            public WalletStoredPaymentMethod() { }
            public int ID { get; set; }
            public string Title { get; set; }
            public string PaymentMethodKey { get; set; }
            public string PaymentMethodGroupKey { get; set; }
            public string OwnerName { get; set; }
            public System.DateTime? ExpirationDate { get; set; }
            public string Last4Digits { get; set; }
            public string IssuerCountryIsoCode { get; set; }
            public bool IsDefault { get; set; }
            public string Icon { get { return string.Format("{0}NPCommon/ImgPaymentMethod/118X70/{1}.png", WebUtils.CurrentDomain.WebServicesUrl, PaymentMethodKey); } set { } }
            public string Display { get; set; }
            public string AccountValue1 { get; set; }
            public string AccountValue2 { get; set; }
            public WalletAddress BillingAddress { get; set; }
            #region Save And Load
            public WalletStoredPaymentMethod(Bll.Accounts.StoredPaymentMethod spm)
            {
                ID = spm.ID;
                Title = spm.Title;

                var method = spm.MethodInstance;
                PaymentMethodKey = ((int)method.PaymentMethodId).ToString();
                PaymentMethodGroupKey = ((int)method.PaymentMethodGroupId).ToString();
                OwnerName = spm.OwnerName;
                ExpirationDate = method.ExpirationDate;
                Last4Digits = method.Value1Last4;
                IssuerCountryIsoCode = spm.IssuerCountryIsoCode;
                Display = method.Display;
                IsDefault = spm.IsDefault;
                if (spm.BillingAddress != null) BillingAddress = new WalletAddress(spm.BillingAddress);
            }
            internal void Save(Bll.Accounts.StoredPaymentMethod value)
            {
                value.Title = Title;
                value.MethodInstance.PaymentMethodId = PaymentMethodKey.ToNullableEnum<CommonTypes.PaymentMethodEnum>().GetValueOrDefault();
                value.OwnerName = OwnerName;
                value.MethodInstance.ExpirationDate = ExpirationDate;
                value.IssuerCountryIsoCode = IssuerCountryIsoCode;
                value.IsDefault = IsDefault;
                if (ID == 0)
                    value.MethodInstance.SetEncryptedData(AccountValue1, AccountValue2);
            }
            #endregion
        }

        public class WalletLinkMethodInfo
        {
            public WalletLinkMethodInfo() { }
            public string AccountValue1 { get; set; }
            public string PersonalNumber { get; set; }
            public string PhoneNumber { get; set; }
            public DateTime? DateOfBirth { get; set; }
            //public string ProviderID { get; set; }
        }

        public class RequestPhysicalPaymentMethodInfo {
            public string ProviderID;
            public WalletAddress Address;
        }

        public class LoadPaymentMethodInfo
        {
            public int PaymentMethodID;
            public decimal Amount;
            public string CurrencyIso;
            public string PinCode;
            public string ReferenceCode;
        }

        public class WalletShippingAddress : WalletAddress
        {
            public WalletShippingAddress() { }
            public int ID { get; set; }
            public string Title { get; set; }
            public string Comment { get; set; }
            public bool IsDefault { get; set; }
            #region Save And Load
            public WalletShippingAddress(Bll.Customers.ShippingAddress sa)
                : base(sa)
            {
                ID = sa.ID;
                Title = sa.Title;
                Comment = sa.Comment;
                IsDefault = sa.IsDefault;
            }
            internal void Save(Bll.Customers.ShippingAddress value)
            {
                value.Title = Title;
                value.Comment = Comment;
                value.IsDefault = IsDefault;
                base.Save(value);
            }
            #endregion
        }

        public class WalletMerchant : WalletAddress
        {
            public string LogoUrl { set; get; }
            public string Name { set; get; }
            public string Number { set; get; }
            public string Email { set; get; }
            public string WebsiteUrl { set; get; }
            public string PhoneNumber { set; get; }

            public WalletMerchant() { }
            internal WalletMerchant(Bll.Merchants.MerchantPublicInfo merchant)
            {
                if (merchant == null) return;
                Number = merchant.Number;
                Name = merchant.Name;
                Email = merchant.EmailAddress;
                WebsiteUrl = merchant.WebsiteUrl;
                PhoneNumber = merchant.Phone;

                AddressLine1 = merchant.BusinessAddress.AddressLine1;
                City = merchant.BusinessAddress.City;
                PostalCode = merchant.BusinessAddress.PostalCode;
                StateIso = merchant.BusinessAddress.StateISOCode;
                CountryIso = merchant.BusinessAddress.CountryISOCode;
                if (merchant.ShopSettings != null)
                    LogoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + Bll.Accounts.Account.MapPublicVirtualPath(merchant.Number, merchant.ShopSettings.LogoFileName);
            }
        }

        public class WalletSaleLocation
        {
            //internal ProductPurchasedInRangeVO productPurchasedInRangeVO;
            public WalletSaleLocation() { }
            public WalletSaleLocation(object ppir) { }
            /*
            public int ProductID { get { return productPurchasedInRangeVO.ID; } set { productPurchasedInRangeVO.ID = value; } }
            public string ProductName { get { return productPurchasedInRangeVO.ProductName; } set { productPurchasedInRangeVO.ProductName = value; } }
            public string ProductImage { get { return productPurchasedInRangeVO.ProductImage; } set { productPurchasedInRangeVO.ProductImage = value; } }
            //public string ProductWebUrl { get { return productPurchasedInRangeVO.ProductWebUrl; } set { productPurchasedInRangeVO.ProductWebUrl = value; } }
            public string MerchantNumber { get { return productPurchasedInRangeVO.MerchantNumber; } set { productPurchasedInRangeVO.MerchantNumber = value; } }
            public double Latitude { get { return productPurchasedInRangeVO.Latitude; } set { productPurchasedInRangeVO.Latitude = value; } }
            public double Longitude { get { return productPurchasedInRangeVO.Longitude; } set { productPurchasedInRangeVO.Longitude = value; } }
            */
        }

        public class WalletTransaction
        {
            internal WalletTransaction() { }
            internal WalletTransaction(Bll.Transactions.Transaction vo, Bll.Merchants.MerchantPublicInfo merchant, bool loadMinimal)
            {
                ID = vo.ID;
                var paymentMethod = Bll.PaymentMethods.PaymentMethod.Get(vo.PaymentMethodID);
                if (paymentMethod != null)
                {
                    PaymentMethodKey = paymentMethod.ID.ToString();
                    PaymentMethodGroupKey = ((int)paymentMethod.Group).ToString();
                }
                AutoCode = vo.ApprovalNumber;
                Amount = vo.Amount;
                PaymentDisplay = vo.PaymentMethodDisplay;
                InsertDate = vo.InsertDate;
                CurrencyIso = vo.CurrencyIsoCode;
                Comment = vo.Comment;
                Text = vo.PayForText;
                Merchant = new WalletMerchant(merchant);
                if (loadMinimal) return;
                if (vo.PayerData != null)
                {
                    Email = vo.PayerData.EmailAddress;
                    Phone = vo.PayerData.PhoneNumber;
                    FullName = string.Format(vo.PayerData.FirstName, vo.PayerData.LastName);
                }
                if (vo.PayerData != null && vo.PayerData.ShippingDetails != null)
                    ShippingAddress = new WalletAddress(vo.PayerData.ShippingDetails);

                if (vo.PaymentData != null && vo.PaymentData.BillingAddress != null)
                    ShippingAddress = new WalletAddress(vo.PaymentData.BillingAddress);

                if (vo.ProductId != null)
                {
                    var co = Bll.Shop.Products.Product.Load(vo.ProductId.Value);
                    if (co != null)
                    {
                        var text = co.GetTextForLanguage(null);
                        if (text != null)
                        {
                            ReceiptText = text.ReceiptText;
                            ReceiptLink = text.ReceiptLink;
                        }
                    }
                }
            }
            public int ID { get; set; }
            public string PaymentMethodKey { get; set; }
            public string PaymentMethodGroupKey { get; set; }
            public string AutoCode { get; set; }
            public decimal Amount { get; set; }
            public string PaymentDisplay { get; set; }
            public DateTime InsertDate { get; set; }
            public string CurrencyIso { get; set; }
            public string Comment { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public string Text { get; set; }
            public string FullName { get; set; }
            public string ReceiptText { get; set; }
            public string ReceiptLink { get; set; }
            public WalletMerchant Merchant { get; set; }
            public WalletAddress BillingAddress { get; set; }
            public WalletAddress ShippingAddress { get; set; }
        }
        public class WalletProcess
        {
            public string PinCode { get; set; }
            public int StoredPaymentMethodId { get; set; }
            public int? ShippingAddressId { get; set; }
            public string MerchantNumber { get; set; }
            public string OrderId { get; set; }
            public decimal Amount { get; set; }
            public string CurrencyIso { get; set; }
            public int Installments { get; set; }

            public string ShopCartCookie { get; set; }

            public int? ProductId { get; set; }
            public int? Quantity { get; set; }
            public string Text { get; set; }

            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }
        public class WalletProcessResult : WalletResult
        {
            internal WalletProcessResult(int code, string message) { Code = code; Message = message; IsSuccess = (Code == 0); }
            public WalletProcessResult() { }
            //public string Signature { get; set; }
        }

        public class WalletFriend
        {
            public WalletFriend() { }
            internal WalletFriend(Bll.Customers.Relation.RelationInfo data)
            {
                DestWalletId = data.AccountNumber;
                FullName = data.FullName;
                RelationType = (int?)data.PeopleRelationType;
                if (System.IO.File.Exists(data.ImagePath))
                {
                    ProfileImageSize = new System.IO.FileInfo(data.ImagePath).Length;
                    if (!Infrastructure.Security.Login.Current.Items.GetValue<object>("excludeDataImages").ToNullableBool().GetValueOrDefault())
                        ProfileImage = System.IO.File.ReadAllBytes(data.ImagePath);
                }
            }
            public string DestWalletId { get; set; }
            public int? RelationType { get; set; }
            public string FullName { get; set; }
            public long ProfileImageSize { get; set; }
            public byte[] ProfileImage { get; set; }
        }

        public class WalletFriendList : WalletResult
        {
            internal WalletFriendList(int code, string message) { Code = code; Message = message; IsSuccess = (Code == 0); }
            public WalletFriendList() : base(ValidationResult.Success) { }
            public List<WalletFriend> Items { get; set; }
        }

        #endregion

        public Wallet() : base(new UserRole[] { UserRole.Customer }) { }
        /*
		private Login validateCredentials(bool noThrowError = false)
		{
			var cookie = Context.Request.Cookies["credentialsToken"];
			if (cookie != null) {
				Guid creds;
				if (Guid.TryParse(cookie.Value, out creds)) {
					var login = Infrastructure.Security.Login.Get(creds, false);
					if (login != null && login.IsInRole(UserRole.Customer)) {
						ObjectContext.Current.CredentialsToken = creds;
						return login;
					}
				}
			}
			if (noThrowError) return null;
			Context.Response.Buffer = false;
			Context.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
			Context.Response.StatusDescription = "Not logged in";
			Context.Response.Write("Not logged in");
			Context.Response.End();
			return null;
		}
        */
        private CommonTypes.Language getLanguage(string langauge)
        {
            var ret = Bll.International.Language.Cache.Where(l => l.Culture == langauge).SingleOrDefault();
            if (ret != null) return ret.EnumValue;
            return Language.Unknown;
        }

        [WebMethod(Description = "Keep Session alive")]
        public bool KeepAlive()
        {
            Login user = validateCredentials(false);
            if (user == null) return false;
            return true; //Netpay.Infrastructure.Security.SecurityManager.IsLoggedin(user.CredentialsToken);
        }

        [WebMethod(Description = "dummy for androied certificate verification")]
        public string CertPing() { return "ok"; }

        [WebMethod(Description = "Get some general lists: country, state")]
        public WalletLists GetLists()
        {
            WalletLists lists = new WalletLists();
            lists.Countries = Bll.Country.Cache.Select(c => new WalletLists.ListItem() { Key = c.IsoCode2, Name = c.Name }).ToList();
            var usa = lists.Countries.Where(x => x.Key == "USA").SingleOrDefault();
            if (usa != null) { lists.Countries.Remove(usa); lists.Countries.Insert(0, usa); }
            lists.UsaStates = Bll.State.Cache.Where(c => c.CountryIsoCode == "US").Select(c => new WalletLists.ListItem() { Key = c.IsoCode, Name = c.Name }).ToList();
            lists.CanadaStates = Bll.State.Cache.Where(c => c.CountryIsoCode == "CA").Select(c => new WalletLists.ListItem() { Key = c.IsoCode, Name = c.Name }).ToList();
            lists.Languages = Bll.International.Language.Cache.Select(c => new WalletLists.ListItem() { Key = c.Culture, Name = c.Name }).ToList();
            return lists;
        }

        [WebMethod(Description = "Get list of all error codes in a specific language")]
        public List<WalletResult> GetErrorCodes(string language)
        {
            return (from e in
                        Infrastructure.ErrorText.GetErrorGroup(null, new HashSet<string>("General,Address,Customer,Payment,PaymentMethod,Security".Split(',')))
                    select new WalletResult() { Code = e.Key, Message = e.Value, Key = ((ValidationResult)e.Key).ToString() }).ToList();
        }

        private string EncodeLoginCookie(string applicationToken, string mumber, string email)
        {
            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());
            return string.Format("CustomerNumber={0}&Email={1}&Signature={2}", Server.UrlEncode(mumber), Server.UrlEncode(email), (mumber + email + (appIdentity != null ? appIdentity.ID : 0)).ToSha256());
        }

        [WebMethod(Description = "Use this function to implement web relogin mechanism")]
        public WalletCookieResult DecodeLoginCookie(string applicationToken, string cookie)
        {
            var cookieValues = System.Web.HttpUtility.ParseQueryString(cookie);
            byte[] image;
            var ret = new WalletCookieResult(ValidationResult.Success);
            ret.Number = cookieValues["CustomerNumber"].NullIfEmpty();
            ret.Email = cookieValues["Email"].NullIfEmpty();
            if (ret.Number == null || ret.Email == null) return new WalletCookieResult(ValidationResult.Invalid_Email);
            if (EncodeLoginCookie(applicationToken, ret.Number, ret.Email) != cookie) return new WalletCookieResult(ValidationResult.Invalid_Data);
            ret.FullName = Netpay.Bll.Customers.Customer.GetFullName(ret.Number, ret.Email, out image);
            ret.ProfilePicture = image;
            return ret;
        }

        [WebMethod(Description = "Check if an email address is in use")]
        public WalletResult IsFreeEmailAddress(string applicationToken, string email)
        {
            var identity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());
            if (identity == null) return new WalletResult(ValidationResult.Invalid_AppIdentity);
            return new WalletResult(Bll.Customers.Customer.IsFreeEmailAddress(email));
        }

        [WebMethod(Description = "Login wallet account")]
        public WalletLoginResult Login(string email, string password, WalletLoginOptions options)
        {
            if (email == null || password == null)
                return new WalletLoginResult(ValidationResult.Invalid_UserOrPassword);
            if (email.Trim() == string.Empty || password.Trim() == string.Empty)
                return new WalletLoginResult(ValidationResult.Invalid_UserOrPassword);
            if (!email.IsEmail())
                return new WalletLoginResult(ValidationResult.Invalid_UserOrPassword);
            if (options == null || string.IsNullOrEmpty(options.applicationToken))
                return new WalletLoginResult(ValidationResult.Invalid_AppIdentity);
            Netpay.Bll.ApplicationIdentity identityForLogin = null;
            identityForLogin = Netpay.Bll.ApplicationIdentity.GetIdentity(options.applicationToken.ToNullableGuid().GetValueOrDefault());
            if (identityForLogin == null) return new WalletLoginResult(ValidationResult.Invalid_AppIdentity);
            
            Guid creds = Guid.Empty;
            if (options == null) options = new WalletLoginOptions();
            LoginResult loginResult = LoginResult.UserNotFound;
            
            loginResult = Infrastructure.Security.Login.DoLogin(new UserRole[] { UserRole.Customer }, null, email, password, options.deviceId, out creds, appIdentityId: identityForLogin.ID);
            if (loginResult == LoginResult.AdminBlocked || loginResult == LoginResult.AbsenceBlocked || loginResult == LoginResult.FailedAttemptsBlocked)
                return new WalletLoginResult(ValidationResult.Invalid_UserOrPassword);
            if (loginResult == LoginResult.Success)
            {
                ObjectContext.Current.CredentialsToken = creds;
                WalletLoginResult result = new WalletLoginResult(ValidationResult.Success);
                result.VersionUpdateRequired = false;
                result.CredentialsToken = creds.ToString();
                Login user = Infrastructure.Security.Login.Get(creds);
                result.IsFirstLogin = user.IsFirstLogin;
                result.LastLogin = user.LastLogin.GetValueOrDefault();
                result.Number = Bll.Accounts.Account.Current.AccountNumber;
                if (!string.IsNullOrEmpty(options.pushToken)) {
                    Bll.Accounts.MobileDevice device = null;
                    Bll.Accounts.MobileDevice.RegisterMobileDevice(1000, options.deviceId, options.pushToken, null, null, out device);
                }
                Netpay.Bll.ApplicationIdentity identity = null;
                if (Bll.Customers.Customer.Current.RefIdentityID != null)
                    identity = Netpay.Bll.ApplicationIdentity.Load(Bll.Customers.Customer.Current.RefIdentityID.Value);
                else if (options != null && !string.IsNullOrEmpty(options.applicationToken))
                    identity = Netpay.Bll.ApplicationIdentity.GetIdentity(options.applicationToken.ToNullableGuid().GetValueOrDefault());
                if (identity != null) user.Items["Session.ApplicationIdentity"] = identity;
                if (options != null) user.Items["excludeDataImages"] = options.excludeDataImages;
                result.EncodedCookie = EncodeLoginCookie(options != null ? options.applicationToken : null, result.Number, email);
                if (options != null && options.setCookie) Context.Response.Cookies.Add(new HttpCookie("CredentialsToken", creds.ToString()));
                return result;
            }
            else
            {
                if (options != null && options.setCookie) Context.Response.Cookies.Remove("CredentialsToken");
                return new WalletLoginResult(ValidationResult.Invalid_UserOrPassword);
            }
        }

        [WebMethod(Description = "Get user specific lists: currencies, payment methods, etc...")]
        public WalletUserLists GetCustomerLists()
        {
            var user = validateCredentials();
            var result = new WalletUserLists();
            result.RelationTypes = Bll.Customers.Relation.RelationTypes.Select(v => new WalletLists.ListItem() { Key = v.Key.ToString(), Name = v.Value }).ToList();
            result.PaymentMethodGroups = Bll.PaymentMethods.Group.Search(null).Where(c => c.IsPopular).Select(c =>
                new WalletLists.ListItem() { Key = c.ID.ToString(), Name = c.Name, Icon = string.Format("{0}NPCommon/ImgPaymentMethodGroups/118X70/{1}.png", WebUtils.CurrentDomain.WebServicesUrl, c.ID) }
            ).ToList();
            result.PaymentMethods = Bll.PaymentMethods.PaymentMethod.Search(null, null).Where(c => c.Type != PaymentMethodType.Unknown && c.Type != PaymentMethodType.System).Select(c =>
                new WalletUserLists.PaymentMethod()
                {
                    Key = c.ID.ToString(),
                    Name = c.Name,
                    GroupKey = ((int)c.Group).ToString(),
                    Value1Caption = c.Value1EncryptedCaption,
                    Value2Caption = c.Value2EncryptedCaption,
                    HasExpirationDate = c.IsExpirationDateMandatory,
                    Value1ValidationRegex = c.Value1EncryptedValidationRegex,
                    Value2ValidationRegex = c.Value2EncryptedValidationRegex
                }
            ).ToList();
            if (user.Items.ContainsKey("Session.ApplicationIdentity"))
            {
                var appIdentity = (user.Items["Session.ApplicationIdentity"] as Netpay.Bll.ApplicationIdentity);
                if (appIdentity != null)
                {
                    //Netpay.Bll.ApplicationIdentity appIdentity = Netpay.Bll.ApplicationIdentity.Load(WebUtils.CurrentDomain.GuestCredentials, user.ApplicationIdentity.Value);
                    if (appIdentity != null)
                    {
                        result.PaymentMethods = result.PaymentMethods.Where(l => appIdentity.SupportedPaymentMethods.Contains(int.Parse(l.Key))).ToList();
                        result.PaymentMethodGroups = result.PaymentMethodGroups.Where(g => (from p in result.PaymentMethods select p.GroupKey).Contains(g.Key)).ToList();
                    }
                }
            }
            return result;
        }

        [WebMethod(Description = "Log out and empty session")]
        public void LogOff()
        {
            Login user = validateCredentials();
            user.LogOff();
            Context.Response.Cookies.Remove("CredentialsToken");
        }

        [WebMethod(Description = "Change account login password")]
        public WalletResult UpdatePassword(string oldPassword, string newPassword)
        {
            Login user = validateCredentials();
            var result = user.ChangePassword(oldPassword, newPassword, HttpContext.Current.Request.UserHostAddress);
            switch (result)
            {
                case Infrastructure.Security.Login.ChangePasswordResult.PasswordConfirmationMissmatch:
                case Infrastructure.Security.Login.ChangePasswordResult.WeakPassword:
                case Infrastructure.Security.Login.ChangePasswordResult.PasswordAlreadyExists:
                    return new WalletResult(ValidationResult.PasswordWeak);
                case Infrastructure.Security.Login.ChangePasswordResult.PasswordUsedInThePast:
                    return new WalletResult(ValidationResult.PasswordUsedInThePast);
                case Infrastructure.Security.Login.ChangePasswordResult.WrongCurrentPassword:
                    return new WalletResult(ValidationResult.PasswordWrong);
                case Infrastructure.Security.Login.ChangePasswordResult.Success:
                    return new WalletResult(ValidationResult.Success);
            }
            return new WalletResult(ValidationResult.Invalid_Data);
        }

        [WebMethod(Description = "Change account PIN code")]
        public WalletResult UpdatePincode(string password, string newPincode)
        {
            Login user = validateCredentials();
            return new WalletResult(Bll.Customers.Customer.Current.UpdatePinCode(password, newPincode));
        }

        [WebMethod(Description = "Send password reset email")]
        public bool ResetPassword(string applicationToken, string email)
        {
            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());
            if (appIdentity == null) throw new Exception("App identity not found");

            string ip = HttpContext.Current.Request.UserHostAddress;
            bool result = Netpay.Bll.Customers.Customer.ResetPassword(email, appIdentity.ID, ip);
            return result;
        }

        [WebMethod(Description = "Get wallet account information")]
        public WalletCustomer GetCustomer()
        {
            Login user = validateCredentials();
            return new WalletCustomer(Bll.Customers.Customer.Current);
        }

        [WebMethod(Description = "Save wallet account information")]
        public WalletResult SaveCustomer(WalletCustomer info)
        {
            Login user = validateCredentials();
            var customer = Bll.Customers.Customer.Current;
            info.Save(customer);
            var res = customer.Validate();
            if (res != ValidationResult.Success)
                return new WalletResult(res);
            customer.Save();
            return new WalletResult(ValidationResult.Success);
        }

        [WebMethod(Description = "Register new wallet account")]
        public WalletResult RegisterCustomer(WalletRegisterData data)
        {
            //string ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            //string registeredFrom = "mobile";
            IPAddress address;
            System.Net.IPAddress.TryParse(Context.Request.UserHostAddress, out address);
            Guid? appIdentityId = data.ApplicationToken.ToNullableGuid();

            var pmList = new List<Netpay.Bll.Accounts.StoredPaymentMethod>();
            if (data.StoredPaymentMethods != null)
            {
                foreach (var spm in data.StoredPaymentMethods)
                {
                    var pm = new Netpay.Bll.Accounts.StoredPaymentMethod();
                    spm.Save(pm);
                    var validationResult = pm.Validate();
                    if (validationResult != ValidationResult.Success) return new WalletResult(validationResult);
                    pmList.Add(pm);
                }
            }

            var customer = new Bll.Customers.Customer();
            if (appIdentityId != null)
            {
                var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(appIdentityId.Value);
                if (appIdentity == null) return new WalletResult(ValidationResult.Invalid_AppIdentity);
                customer.RefIdentityID = appIdentity.ID;
            }
            data.info.Save(customer);
            var saveResult = customer.Register(address, data.PinCode.NullIfEmpty(), data.Password.NullIfEmpty());
            if (saveResult != ValidationResult.Success) return new WalletResult(saveResult);
            foreach (var spm in pmList)
            {
                spm.Account_id = customer.AccountID;
                spm.Save();
            }
            return (new WalletResult(saveResult) { Number = customer.AccountNumber });
        }

        [WebMethod(Description = "Get account transactions with paging")]
        public List<WalletTransaction> GetTransactions(int page, int pageSize)
        {
            Login user = validateCredentials();
            var filters = new Bll.Transactions.Transaction.SearchFilters();
            var lo = new Bll.Transactions.Transaction.LoadOptions()
            {
                sortAndPage = new SortAndPage(page - 1, pageSize, "InsertDate", true),
                dataStores = new List<TransactionStatus>() { TransactionStatus.Captured },
                loadPaymentData = true,
            };
            var transactions = Bll.Transactions.Transaction.Search(lo, filters);
            var merchants = Bll.Merchants.MerchantPublicInfo.Load(transactions.Select(t => t.MerchantID.GetValueOrDefault()).Distinct().ToList(), null);
            return transactions.Select(t => new WalletTransaction(t, merchants.Where(m => m.ID == t.MerchantID).SingleOrDefault(), true)).ToList();
        }

        [WebMethod(Description = "Get transactions information")]
        public WalletTransaction GetTransaction(int transactionId)
        {
            Login user = validateCredentials();
            var vo = Bll.Transactions.Transaction.GetTransaction(null, transactionId, TransactionStatus.Captured);
            return new WalletTransaction(vo, Bll.Merchants.MerchantPublicInfo.Load(vo.MerchantID.GetValueOrDefault()), false);
        }

        [WebMethod(Description = "Obsolete - Use Shop.GetProducts instead"), Obsolete("Use Shop.GetProducts instead")]
        public List<Shop.ShopProduct> GetProducts(Shop.ShopSearchFilter filters, int page, int pageSize)
        {
            return new Shop().GetProducts(filters, page, pageSize);
            /*
            List<string> tagsSplit = filters.Text.Split(' ').Select(t => t.Trim().ToLower()).ToList();
            //var products = Bll.PublicPayment.GetChargeOptions(WebUtils.DomainHost, merchantNumber, getLanguage(language), --page, pageSize);
            var products = Bll.PublicPayment.SearchChargeOptions(WebUtils.CurrentDomain.Host, filters.MerchantNumber, tagsSplit, getLanguage(filters.language), --page, pageSize);
            var merchants = Bll.Merchant.Merchant.Load(WebUtils.CurrentDomain.ServiceCredentials, products.Select(p=> p.MerchantID).Distinct().ToList());
            return products.Select(p => new WalletProduct(WebUtils.CurrentDomain, p, merchants.Where(m => m.ID == p.MerchantID).SingleOrDefault())).ToList();
            */
        }

        [WebMethod(Description = "Obsolete - Use Shop.GetProducts instead"), Obsolete("Use Shop.GetProduct instead")]
        public Shop.ShopProduct GetProduct(string merchantNumber, int itemId)
        {
            return new Shop().GetProduct(merchantNumber, itemId);
        }

        [WebMethod(Description = "NOT IMPLEMENTED")]
        public List<WalletSaleLocation> SearchProductBySales(double latitude, double longitude, string language)
        {
            return null;//Bll.PublicPayment.SearchChargeOptionsByLocation(WebUtils.DomainHost, getLanguage(language), latitude, longitude, 32000).Select(r => new WalletSaleLocation(r)).ToList();
        }

        [WebMethod(Description = "Get list of wallet shipping addresses")]
        public List<WalletShippingAddress> GetShippingAddresses()
        {
            Login user = validateCredentials();
            return Bll.Customers.ShippingAddress.LoadForCustomer(Bll.Accounts.Account.Current.CustomerID).Select(n => new WalletShippingAddress(n)).ToList();
        }

        [WebMethod(Description = "Get wallet specific shipping addresses")]
        public WalletShippingAddress GetShippingAddress(int addressId)
        {
            Login user = validateCredentials();
            return new WalletShippingAddress(Bll.Customers.ShippingAddress.Load(addressId));
        }

        [WebMethod(Description = "Save wallet specific shipping addresses")]
        public WalletResult SaveShippingAddress(WalletShippingAddress address)
        {
            return SaveShippingAddresses(new WalletShippingAddress[] { address });
        }

        [WebMethod(Description = "Save a list of wallet shipping addresses")]
        public WalletMultiResult SaveShippingAddresses(WalletShippingAddress[] data)
        {
            Login user = validateCredentials();
            var refNumbers = new List<string>();
            var pmList = new List<Netpay.Bll.Customers.ShippingAddress>();
            foreach (var address in data)
            {
                Bll.Customers.ShippingAddress dataObject;
                if (address.ID == 0)
                {
                    dataObject = new Bll.Customers.ShippingAddress();
                    dataObject.Customer_id = Bll.Customers.Customer.Current.ID;
                }
                else dataObject = Bll.Customers.ShippingAddress.Load(address.ID);
                address.Save(dataObject);
                var result = dataObject.Validate();
                if (result != ValidationResult.Success) return new WalletMultiResult(result);
                pmList.Add(dataObject);
            }
            foreach (var dataObject in pmList)
            {
                dataObject.Save();
                refNumbers.Add(dataObject.ID.ToString());
            }
            return (new WalletMultiResult(ValidationResult.Success) { RefNumbers = refNumbers.ToArray() });
        }

        [WebMethod(Description = "Delete wallet shipping addresses")]
        public bool DeleteShippingAddress(int addressId)
        {
            Login user = validateCredentials();
            var item = Bll.Customers.ShippingAddress.Load(addressId);
            if (item == null) return false;
            item.Delete();
            return true;
        }

        [WebMethod(Description = "Get list of wallet billing addresses")]
        public List<WalletAddress> GetBillingAddresses()
        {
            Login user = validateCredentials();
            return Bll.Accounts.StoredPaymentMethod.BillingAddressesForAccount(Bll.Accounts.Account.Current.AccountID).Select(n => new WalletAddress(n)).ToList();
        }

        [WebMethod(Description = "Get a list of wallet stored payment methods")]
        public List<WalletStoredPaymentMethod> GetStoredPaymentMethods()
        {
            Login user = validateCredentials();
            return Bll.Accounts.StoredPaymentMethod.LoadForAccount(Bll.Accounts.Account.Current.AccountID).Select(n => new WalletStoredPaymentMethod(n)).ToList();
        }

        [WebMethod(Description = "Get information on a wallet stored payment method")]
        public WalletStoredPaymentMethod GetStoredPaymentMethod(int pmid)
        {
            Login user = validateCredentials();
            return new WalletStoredPaymentMethod(Bll.Accounts.StoredPaymentMethod.Load(pmid));
        }


        [WebMethod(Description = "Save a specific wallet stored payment method")]
        public WalletResult StorePaymentMethod(WalletStoredPaymentMethod methodData)
        {
            return StorePaymentMethods(new WalletStoredPaymentMethod[] { methodData });
        }

        [WebMethod(Description = "Save a list of wallet stored payment methods")]
        public WalletMultiResult StorePaymentMethods(WalletStoredPaymentMethod[] data)
        {
            Login user = validateCredentials();
            var refNumbers = new List<string>();
            var pmList = new List<Netpay.Bll.Accounts.StoredPaymentMethod>();
            foreach (var methodData in data)
            {
                Bll.Accounts.StoredPaymentMethod dataObject;
                if (methodData.ID == 0)
                {
                    dataObject = new Bll.Accounts.StoredPaymentMethod();
                    dataObject.Account_id = Bll.Accounts.Account.Current.AccountID;
                }
                else dataObject = Bll.Accounts.StoredPaymentMethod.Load(methodData.ID);
                if (methodData.BillingAddress != null)
                {
                    if (dataObject.BillingAddress == null) dataObject.BillingAddress = new Bll.Accounts.AccountAddress();
                    methodData.BillingAddress.Save(dataObject.BillingAddress);
                }
                else methodData.BillingAddress = null;
                methodData.Save(dataObject);
                var result = dataObject.Validate();
                if (result != ValidationResult.Success) return new WalletMultiResult(result);
                pmList.Add(dataObject);
            }
            foreach (var dataObject in pmList)
            {
                dataObject.Save();
                refNumbers.Add(dataObject.ID.ToString());
            }
            return (new WalletMultiResult(ValidationResult.Success) { RefNumbers = refNumbers.ToArray() });
        }

        [WebMethod(Description = "Delete wallet stored payment method")]
        public bool DeleteStoredPaymentMethod(int pmid)
        {
            Login user = validateCredentials();
            var item = Bll.Accounts.StoredPaymentMethod.Load(pmid);
            if (item == null) return false;
            item.Delete();
            return true;
        }

        [WebMethod(Description = "Link an external physical pre-existing payment method")]
        public ServiceResult LinkPaymentMethod(WalletLinkMethodInfo data)
        {
            Login user = validateCredentials();
            var foundCard = Bll.PaymentMethods.BulkStoredMethod.FindByPAN(data.AccountValue1, null).SingleOrDefault();
            if (foundCard == null) return new ServiceResult(ValidationResult.Item_Not_Exist);
            int comCount = 0;
            if (!string.IsNullOrEmpty(data.PersonalNumber)) comCount += (string.Compare(data.PersonalNumber.Trim(), foundCard.PersonalNumber.Trim(), true) == 0) ? 1 : 0;
            if (!string.IsNullOrEmpty(data.PhoneNumber)) comCount += (string.Compare(data.PhoneNumber.Trim(), foundCard.PhoneNumber.Trim(), true) == 0) ? 1 : 0;
            if (data.DateOfBirth.HasValue && foundCard.DateOfBirth.HasValue) comCount += (data.DateOfBirth.GetValueOrDefault().Date == foundCard.DateOfBirth.GetValueOrDefault().Date) ? 1 : 0;
            if (comCount < 1) return new ServiceResult(ValidationResult.Invalid_Phone);
            foundCard.AttachToAccount(Bll.Accounts.Account.Current.AccountID);
            return new ServiceResult(ValidationResult.Success);
        }

        [WebMethod(Description = "Request physical card")]
        public ServiceResult RequestPhysicalPaymentMethod(RequestPhysicalPaymentMethodInfo data)
        {
            Login user = validateCredentials();
            var customer = Bll.Customers.Customer.Current;
            Bll.Accounts.IStoredPaymentMethodProvider provider;
            if (!Bll.Accounts.StoredPaymentMethod.Providers.TryGetValue(data.ProviderID, out provider))
                return new ServiceResult(ValidationResult.Invalid_PaymentMethod);
            if (!provider.SupportCreate) return new ServiceResult(ValidationResult.Invalid_PaymentMethod);
            try {
                var accountAddress = new Bll.Accounts.AccountAddress(); data.Address.Save(accountAddress);
                var ret = provider.Create(customer, accountAddress, new System.Collections.Specialized.NameValueCollection());
                return new ServiceResult(ValidationResult.Success, null, ret.ID.ToString());
            } catch (Exception ex) {
                return new ServiceResult(ValidationResult.Invalid_ServiceUrl, ex.Message);
            }
        }

        [WebMethod(Description = "Load PaymentMethod from balance")]
        public ServiceResult LoadPaymentMethod(LoadPaymentMethodInfo data) {
            Login user = validateCredentials();
            var customer = Bll.Customers.Customer.Current;
            if (!Bll.Customers.Customer.ValidatePinCode(customer.AccountID, data.PinCode))
                return new ServiceResult(ValidationResult.PinCodeWrong);
            var pm = Bll.Accounts.StoredPaymentMethod.Load(data.PaymentMethodID);
            Bll.Accounts.IStoredPaymentMethodProvider provider;
            if (!Bll.Accounts.StoredPaymentMethod.Providers.TryGetValue(pm.ProviderID, out provider))
                return new ServiceResult(ValidationResult.Invalid_PaymentMethod);
            if (!provider.SupportLoad) return new ServiceResult(ValidationResult.Invalid_PaymentMethod);
            try {
                var ret = provider.Load(pm, data.Amount, Bll.Currency.Get(data.CurrencyIso).EnumValue, data.ReferenceCode);
                return new ServiceResult(ValidationResult.Success, null, ret.ID.ToString());
            } catch (Exception ex) {
                return new ServiceResult(ValidationResult.Invalid_ServiceUrl, ex.Message);
            }
        }

        [WebMethod(Description = "Process wallet transaction")]
        public WalletProcessResult ProcessTransaction(WalletProcess data)
        {
            Login user = validateCredentials();
            var payerInfo = Bll.Customers.Customer.Current;
            if (!Bll.Customers.Customer.ValidatePinCode(payerInfo.AccountID, data.PinCode))
                return new WalletProcessResult(500, "wrong Pincode was entered");
            Bll.Shop.Cart.Basket basket = null;
            var merchant = Bll.Merchants.Merchant.Load(data.MerchantNumber);
            //Netpay.Bll.Merchants.Merchant.CachedNumbersForDomain().TryGetValue(data.MerchantNumber, out merchantId);
            if (data.ShopCartCookie != null)
            {
                basket = Shop.LoadBasketFromCookie(null, data.ShopCartCookie);
            }
            else if (data.ProductId != null)
            {
                var product = new Bll.Shop.Cart.BasketItem(merchant.ID, data.ProductId.Value, null, null);
                if (product == null) return new WalletProcessResult(500, "Product does not exist");
                basket = new Bll.Shop.Cart.Basket(merchant.ID, data.OrderId, user.LoginID);
                basket.CurrencyISOCode = product.CurrencyISOCode;
                basket.AddItem(product);
            }
            if (basket != null)
            {
                //if (data.Amount == 0) 
                data.Amount = basket.Total;
                //if (string.IsNullOrEmpty(data.CurrencyIso)) 
                data.CurrencyIso = basket.CurrencyISOCode.ToString();
                if (string.IsNullOrEmpty(data.Text))
                {
                    if (basket.BasketItems.Count == 0) data.Text = string.Format("{0} x {1}", basket.BasketItems[0].Quantity, basket.BasketItems[0].Name);
                    else data.Text = "Cart #" + basket.ID;
                }
            }
            if (!string.IsNullOrEmpty(data.OrderId)) data.OrderId = "SQR-ID:" + data.OrderId.ToString();
            //string orderId = "SQR-ID:" + transQrId.ToString();

            var pMethod = Netpay.Bll.Accounts.StoredPaymentMethod.Load(data.StoredPaymentMethodId);
            if (pMethod == null) return new WalletProcessResult(500, "Card does not exist");

            var trans = new Bll.Transactions.Transaction();
            trans.Merchant = merchant;
            trans.CustomerID = Bll.Customers.Customer.Current.ID;
            trans.CurrencyIsoCode = data.CurrencyIso;
            trans.Amount = data.Amount;
            trans.Installments = (byte)data.Installments;
            trans.CreditType = CreditType.Regular;

            trans.PayForText = data.Text;
            trans.IP = HttpContext.Current.Request.UserHostAddress;
            trans.OrderNumber = data.OrderId;
            trans.TransactionSource = TransactionSource.WalletMobileApp;
            if (data.ProductId != null) trans.ProductId = data.ProductId;
            //data.Comment = data.Comment;

            PersonalInfo.Copy(payerInfo, trans.PayerData);
            pMethod.MethodInstance.CopyTo(trans.PaymentData.MethodInstance);

            Bll.Accounts.AccountAddress address = pMethod.BillingAddress;
            if (address == null) address = payerInfo.PersonalAddress;
            if (address != null)
            {
                if (trans.PaymentData.BillingAddress == null) trans.PaymentData.BillingAddress = new Bll.Transactions.BillingAddress();
                Address.Copy(address, trans.PaymentData.BillingAddress);
            }
            if (data.ShippingAddressId != null)
            {
                var shAddress = Bll.Customers.ShippingAddress.Load(data.ShippingAddressId.Value);
                if (trans.PayerData.ShippingDetails == null) trans.PayerData.ShippingDetails = new Bll.Transactions.ShippingDetails();
                Address.Copy(address, trans.PayerData.ShippingDetails);
            }
            var reqtType = trans.PaymentData.MethodInstance.PaymentMethodGroupId;
            System.Collections.Specialized.NameValueCollection retParams = null;
            var reqParams = trans.GetProcessParamters();
            if (basket != null) reqParams.Add("CartId", basket.Identifier.ToString());
            try
            {
                retParams = Bll.Transactions.Transaction.ProcessTransaction(reqtType, reqParams);
            }
            catch (Exception ex)
            {
                return new WalletProcessResult(500, ex.Message);
            }
            WalletProcessResult result = null;
            if (reqtType == PaymentMethodGroupEnum.CreditCard)
            {
                result = new WalletProcessResult(retParams.Get("Reply").ToInt32(-1), retParams.Get("ReplyDesc")) { Number = retParams.Get("TransID") };
            }
            else if (reqtType == PaymentMethodGroupEnum.DirectDebit || reqtType == PaymentMethodGroupEnum.OnlineBankTransfer || reqtType == PaymentMethodGroupEnum.InstantOnlinkBankTransfer || reqtType == PaymentMethodGroupEnum.EuropeanDirectDebit)
            {
                result = new WalletProcessResult(retParams.Get("replyCode").ToInt32(-1), retParams.Get("replyMessage")) { Number = retParams.Get("transId") };
            }
            else result = new WalletProcessResult(500, "Unexpected failure");
            return result;
        }

        [WebMethod(Description = "Friends network, get a list of friends(destWalletId=null) or information on specific friend(destWalletId=friend number)")]
        public WalletFriendList GetFriends(string destWalletId)
        {
            validateCredentials();
            var friends = Bll.Customers.Relation.GetFriends(Bll.Customers.Customer.Current.ID);
            if (!string.IsNullOrEmpty(destWalletId))
            {
                var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
                friends = friends.Where(r => r.CustomerId == targetCustomer.CustomerId && r.TargetCustomerId == targetCustomer.CustomerId).ToList();
            }
            var ret = Bll.Customers.Relation.GetInfo(Bll.Customers.Customer.Current.ID, friends).Where(f => f.AccountID != Bll.Accounts.Account.Current.AccountID);
            return new WalletFriendList() { Items = ret.Select(f => new WalletFriend(f)).ToList() };
        }

        [WebMethod(Description = "Get wallet friend image")]
        public byte[] GetImage(string walletId, bool asRaw)
        {
            validateCredentials();
            var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { walletId }).SingleOrDefault();
            if (targetCustomer == null) return null;
            if (!System.IO.File.Exists(targetCustomer.ImagePath)) return null;
            if (!asRaw) return System.IO.File.ReadAllBytes(targetCustomer.ImagePath);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=\"" + System.IO.Path.GetFileName(targetCustomer.ImagePath) + "\"");
            HttpContext.Current.Response.WriteFile(targetCustomer.ImagePath);
            HttpContext.Current.Response.End();
            return null;
        }

        [WebMethod(Description = "Friends network, get list of all friend requests(destWalletId=null), or a specific request(destWalletId = value)")]
        public WalletFriendList GetFriendRequests(string destWalletId)
        {
            validateCredentials();
            var friends = Bll.Customers.Relation.GetRequests(Bll.Customers.Customer.Current.ID);
            if (!string.IsNullOrEmpty(destWalletId))
            {
                var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
                friends = friends.Where(r => r.CustomerId == targetCustomer.CustomerId && r.TargetCustomerId == targetCustomer.CustomerId).ToList();
            }
            var ret = Bll.Customers.Relation.GetInfo(Bll.Customers.Customer.Current.ID, friends).Where(f => f.AccountID != Bll.Accounts.Account.Current.AccountID);
            return new WalletFriendList() { Items = ret.Select(f => new WalletFriend(f)).ToList() };
        }

        [WebMethod(Description = "Friend network, find friend by id or name")]
        public WalletFriendList FindFriend(string searchTerm, int page, int pageSize)
        {
            validateCredentials();
            var items = Bll.Customers.Relation.GetInfo(searchTerm, true, Bll.Customers.Customer.Current.RefIdentityID, new SortAndPage(page - 1, pageSize)).Select(n => new WalletFriend(n));
            return new WalletFriendList() { Items = items.ToList() };
        }

        [WebMethod(Description = "Friends network, get information and make new friend request")]
        public WalletFriendList FriendRequest(string destWalletId)
        {
            validateCredentials();
            var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
            if (targetCustomer == null) return new WalletFriendList(2, "destWalletId Not found");
            var newReq = Bll.Customers.Relation.GetRelation(Bll.Customers.Customer.Current.ID, targetCustomer.CustomerId);
            if (newReq != null && (newReq.IsActive.HasValue && !newReq.IsActive.Value)) { //delete old declined request
                newReq.Delete();
                newReq = null;
            }
            if (newReq == null) {
                newReq = new Bll.Customers.Relation(Bll.Customers.Customer.Current.ID, targetCustomer.CustomerId);
                newReq.Save();
            }
            return new WalletFriendList() { Items = new List<WalletFriend>() { new WalletFriend(targetCustomer) } };
        }

        [WebMethod(Description = "Remove friend from logged user friends network")]
        public WalletResult RemoveFriend(string destWalletId)
        {
            validateCredentials();
            var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
            if (targetCustomer == null) return new WalletResult(ValidationResult.Item_Not_Exist);
            if (targetCustomer.CustomerId == Bll.Customers.Customer.Current.ID) return new WalletResult(ValidationResult.Invalid_AccountID);
            var rel = Bll.Customers.Relation.GetRelation(Bll.Customers.Customer.Current.ID, targetCustomer.CustomerId);
            if (rel == null) return new WalletResult(ValidationResult.Item_Not_Exist);
            rel.Delete();
            return new WalletResult(ValidationResult.Success);
        }

        [WebMethod(Description = "Friends network, reply a friend request")]
        public WalletResult ReplyFriendRequest(string destWalletId, bool approve)
        {
            validateCredentials();
            if (string.IsNullOrEmpty(destWalletId)) return new WalletResult(ValidationResult.Item_Not_Exist);
            var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
            var item = Bll.Customers.Relation.GetRelation(Bll.Customers.Customer.Current.ID, targetCustomer.CustomerId);
            if (item == null) return new WalletResult(ValidationResult.Item_Not_Exist);
            if (item.IsActive != null) return new WalletResult(ValidationResult.Item_Wrong_State);
            if (approve) item.Confirm();
            else item.Reject();
            item.Save();
            return new WalletResult(ValidationResult.Success);
        }

        [WebMethod(Description = "Friends network, set friend relation type")]
        public WalletResult SetFriendRelation(string destWalletId, int? relationTypeKey)
        {
            validateCredentials();
            if (string.IsNullOrEmpty(destWalletId)) return new WalletResult(ValidationResult.Item_Not_Exist);
            var targetCustomer = Bll.Customers.Relation.GetInfo(new string[] { destWalletId }).SingleOrDefault();
            if (targetCustomer == null) return new WalletResult(ValidationResult.Invalid_ID);
            var item = Bll.Customers.Relation.GetRelation(Bll.Customers.Customer.Current.ID, targetCustomer.CustomerId);
            if (item == null) return new WalletResult(ValidationResult.Item_Not_Exist);
            item.PeopleRelationType = (Bll.Customers.Relation.RelationType?)relationTypeKey;
            item.Save();
            return new WalletResult(ValidationResult.Success);
        }

        #region Facebook
        public class FacebookFriendDetails
        {
            public FacebookFriendDetails() { }
            public string id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
        }

        /// <summary>
        /// JSON Deserialization
        /// </summary>
        private static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        [WebMethod(Description = "Create internal friend request for each friend from facebook")]
        public void ImportFriendsFromFacebook(string accessToken)
        {
            //Login("lavi@netpay-intl.com", "aabb1122", new WalletLoginOptions() { setCookie = true });
            //accessToken = "CAACEdEose0cBAAL18FWMWmxNEwqoj3ffWJm7u3FpTnPrXUyVBrCdZCfuB86FN3GKbe9POIuwyYuOFGFfVOpp4Ttyxwis02s0l9ZCRIZAWDomWb1G0VoAMkMdAFvNldDdixMZCqmFjmJthZChBGVc2CUXbHNZCPknkxIEDM3z5ZAie57fcuEdkkKI8XakxQeoVdypsOSJwQ2m0f0pVi6braXApXJZCxCBspAZD";
            validateCredentials();
            WebClient wc = new WebClient();
            string userData = wc.DownloadString("https://graph.facebook.com/me?fields=id,name,email&access_token=" + accessToken); //client.Get("/me");

            FacebookFriendDetails friendDetails = JsonDeserialize<FacebookFriendDetails>(userData);
            Bll.Customers.Customer.Current.FacebookUserID = friendDetails.id;
            Bll.Customers.Customer.Current.Save();

            string strJSONData = wc.DownloadString("https://graph.facebook.com/me?fields=friends&access_token=" + accessToken);
            strJSONData = string.Format("[{0}]", strJSONData.Split('[')[1].Split(']')[0]);
            JavaScriptSerializer objJavaScriptSerializer = new JavaScriptSerializer();
            FacebookFriendDetails[] objStatus = objJavaScriptSerializer.Deserialize<FacebookFriendDetails[]>(strJSONData);
            Dictionary<string, string> foundUsers = Bll.Customers.Customer.FindFacebookUsers(objStatus.Select(p => p.id).ToList<string>());
            foreach (var user in foundUsers)
                FriendRequest(user.Key);
        }
        #endregion
    }
}
