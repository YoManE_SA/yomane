﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;

namespace Netpay.WebServices
{
    [WebService(Namespace = "WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class InfobipWS : System.Web.Services.WebService
    {
        #region GetBalance

        [WebMethod(Description = "The service returns the total available balance for the user - per currency")]
        public string GetBalance(string strPAN, string applicationToken)
        {

            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());

            if (appIdentity == null || appIdentity == Netpay.Bll.ApplicationIdentity.Default)
                return "Invalid applicationToken";

            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);

            var spm = Bll.Accounts.StoredPaymentMethod.FindByPAN(strPAN, null).SingleOrDefault(); //.Where(v => v.ProviderID == STOREDPM_PROVIDERNAME)
            if (spm != null)
            {
                var pCustomer = Netpay.Bll.Customers.Customer.Search(new Bll.Customers.Customer.SearchFilters() { AccountsIdsList = new System.Collections.Generic.List<int>() { spm.Account_id } }, null);
                if (pCustomer == null || pCustomer.Count == 0)
                    return "Error: Could not find customer";

                // Taken out to enable all partners to check balance using same USSD menu
                //if (!pCustomer[0].RefIdentityID.HasValue || appIdentity.ID != pCustomer[0].RefIdentityID.Value)
                //    return "Error: Invalid applicationToken";

                bool includePending = false;

                Dal.Netpay.NetpayDataContextBase dc = new Dal.Netpay.NetpayDataContextBase(Domain.Current.Sql1ConnectionString);
                var curObj = Bll.Currency.GetByIsoNumber(710);
                var pCustomerDetailsForISO = dc.SpGetCustomerDetailsForISOUsingPAN(strPAN).FirstOrDefault();


                if (pCustomerDetailsForISO.TotalBalance.HasValue)
                {
                    return "Your account balance is '" + pCustomerDetailsForISO.TotalBalance + " " + curObj.IsoCode + "'";
                }
                else
                {
                    return "Your account balance is '0.00 " + curObj.IsoCode + "'";
                }

                //var ret = Bll.Accounts.Balance.GetStatus(pCustomer.ToList()[0].AccountID).Select(r => new WebServices.Balance.BalanceTotal() { CurrencyIso = r.CurrencyIso, Value = includePending ? r.Expected : r.Current });

                //if (ret.Count() > 0)
                //{
                //    return "Your account balance is '" + ret.ToList()[0].Value + " " + ret.ToList()[0].CurrencyIso + "'";
                //}
            }
            else
            {
                return "Error: Invalid PAN";
            }

            return "No balance";

        }

        #endregion

        #region Activate Card

        [WebMethod(Description = "This service will stop a card for being able to process transactions")]
        public string Activate(string strPAN, string strIDNumber, string applicationToken, string strPIN)
        {

            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());

            if (appIdentity == null || appIdentity == Netpay.Bll.ApplicationIdentity.Default)
                return "Invalid applicationToken";

            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);

            Bll.Accounts.Account pAccount = null;

            if (!string.IsNullOrEmpty(strPAN) && !string.IsNullOrEmpty(strIDNumber))
            {
                var spm = Bll.Accounts.StoredPaymentMethod.FindByPAN(strPAN, null).SingleOrDefault(); //.Where(v => v.ProviderID == STOREDPM_PROVIDERNAME)
                pAccount = Bll.Accounts.Account.AccountByPersonalNumber(strIDNumber);
                if (spm != null && pAccount != null)
                {
                    pAccount.SetPinCode(strPIN);
                    pAccount.SaveAccount();
                    Bll.Accounts.StoredPaymentMethod.ChangeCardStatus(true, spm.ID);
                    return "Card activated";
                }
                else
                    return "Could not locate customer.";
            }
            else
            {
                return "Card Number and ID number cannot be blank.";
            }

        }


        #endregion

        #region Stop Card

        [WebMethod(Description = "This service will stop a card for being able to process transactions")]
        public string StopCard(string strPAN, string strIDNumber, string applicationToken)
        {

            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());

            if (appIdentity == null || appIdentity == Netpay.Bll.ApplicationIdentity.Default)
                return "Invalid applicationToken";

            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);

            Bll.Accounts.Account pAccount = null;
            try
            {
                if (!string.IsNullOrEmpty(strPAN))
                {
                    var spm = Bll.Accounts.StoredPaymentMethod.FindByPAN(strPAN, null)
                        .SingleOrDefault(); //.Where(v => v.ProviderID == STOREDPM_PROVIDERNAME)
                    if (spm == null)
                        return "Could not locate customer";
                    pAccount = Bll.Accounts.Account.LoadAccount(spm.Account_id);
                    if (pAccount == null)
                        return "Could not locate customer";
                    Bll.Accounts.StoredPaymentMethod.ChangeCardStatus(false, spm.ID);
                    return "Card stopped";
                }
                else if (!string.IsNullOrEmpty(strIDNumber))
                {
                    pAccount = Bll.Accounts.Account.AccountByPersonalNumber(strIDNumber);

                    if (pAccount == null)
                        return "Could not locate customer";

                    var spms = Bll.Accounts.StoredPaymentMethod.LoadForAccount(pAccount.AccountID);
                    foreach (var p in spms)
                    {
                        if (p.MethodInstance.ExpirationDate.HasValue &&
                            p.MethodInstance.ExpirationDate.Value >= DateTime.Now)
                            Bll.Accounts.StoredPaymentMethod.ChangeCardStatus(false, p.ID);
                    }

                    return "Card stopped";
                }
                else
                {
                    return "Card Number or ID number required.";
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LogTag.WebService, ex);
                return "Could not locate customer";
            }
        }


        #endregion

        #region Log

        [WebMethod(Description = "The service to log USSD XML")]
        public void Log(string applicationToken, LogSeverity severity, LogTag tag, string message, string longMessage)
        {
            var appIdentity = Netpay.Bll.ApplicationIdentity.GetIdentity(applicationToken.ToNullableGuid().GetValueOrDefault());

            if (appIdentity == null || appIdentity == Netpay.Bll.ApplicationIdentity.Default)
                return;

            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);

            Infrastructure.Logger.Log(severity: severity, tag: tag, message: message, longMessage: longMessage);
        }

        #endregion

    }
}
