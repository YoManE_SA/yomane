﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.WebServices
{
	/// <summary>
	/// Summary description for Orange1
	/// </summary>
	public class Orange1 : IHttpHandler
	{
		public bool IsReusable
		{
			get { return false; }
		}

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			string postData = null;
			if (context.Request.ContentLength > 0) postData = System.Text.Encoding.UTF8.GetString(context.Request.BinaryRead(context.Request.ContentLength));
			else postData = context.Request.Params["QUERY_STRING"];
			string res = Bll.ThirdParty.Orange.parseNotification(postData);
			context.Response.Write(res);
		}
	}
}