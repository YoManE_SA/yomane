﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Netpay.Bll.ThirdParty.WiGroupC;

namespace Netpay.WebServices
{
    public partial class WiGroup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Clear();
            if (string.IsNullOrEmpty(Request["Action"]))
                return;

            Response.ContentType = "application/json";

            string strJsonRequest = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
            string strDescription = "";
            bool bDoesExist = false;

            Dal.Netpay.CustomerWiCode pWiCode = null;
            Dal.DataAccess.NetpayDataContext dc = new Dal.DataAccess.NetpayDataContext(Infrastructure.Domain.Current.Sql1ConnectionString);
            Dal.Netpay.Account pCustomerAccount = null;

            //Response.Write("Post:");
            //Response.Write(strJsonRequest);

            Infrastructure.ObjectContext.Current.Impersonate(Infrastructure.Domain.Current.ServiceCredentials);

            switch (Request["Action"])
            {
                case "TransactionRequest":
                    WiJSON.Transaction pTransactionInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<WiJSON.Transaction>(strJsonRequest);
                    bool bDidSucceed = false;

                    #region TransactionResultFail

                    var pTransactionFailResult = new WiJSON.TransactionResultFail();
                    pTransactionFailResult.basketAmountProcessed = pTransactionInfo.basketAmount;
                    pTransactionFailResult.storeTrxDetails = new WiJSON.TransactionResultFail.Storetrxdetails()
                    {
                        basketId = pTransactionInfo.storeTrxDetails.basketId,
                        cashierId = pTransactionInfo.storeTrxDetails.cashierId,
                        posId = pTransactionInfo.storeTrxDetails.posId,
                        storeId = pTransactionInfo.storeTrxDetails.storeId,
                        trxId = pTransactionInfo.storeTrxDetails.trxId
                    };
                    pTransactionFailResult.token = new WiJSON.TransactionResultFail.Token()
                    {
                        id = pTransactionInfo.token.id,
                        type = pTransactionInfo.token.type
                    };
                    pTransactionFailResult.totalAmountProcessed = pTransactionInfo.totalAmount;
                    pTransactionFailResult.type = pTransactionInfo.type;

                    #endregion

                    #region Find WiCode previously requested from WiGroup

                    pWiCode = (from p in dc.CustomerWiCodes
                               where p.WiCode == pTransactionInfo.token.id
                               select p).SingleOrDefault();

                    if (pWiCode == null)
                    {
                        // Fail - Not found
                        pTransactionFailResult.responseCode = "1";
                        pTransactionFailResult.responseDesc = "WiCode not found";
                        EndPage(pTransactionFailResult);
                        return;
                    }

                    #endregion

                    #region TransactionResultSuccess

                    var pTransactionSuccessResult = new WiJSON.TransactionResultSuccess();
                    pTransactionSuccessResult.amountToSettle = pTransactionInfo.totalAmount;
                    pTransactionSuccessResult.basketAmountProcessed = pTransactionInfo.basketAmount;
                    pTransactionSuccessResult.storeTrxDetails = new WiJSON.TransactionResultSuccess.Storetrxdetails()
                    {
                        basketId = pTransactionInfo.storeTrxDetails.basketId,
                        cashierId = pTransactionInfo.storeTrxDetails.cashierId,
                        posId = pTransactionInfo.storeTrxDetails.posId,
                        storeId = pTransactionInfo.storeTrxDetails.storeId,
                        trxId = pTransactionInfo.storeTrxDetails.trxId
                    };
                    pTransactionSuccessResult.token = new WiJSON.TransactionResultSuccess.Token()
                    {
                        id = pTransactionInfo.token.id,
                        type = pTransactionInfo.token.type
                    };
                    pTransactionSuccessResult.totalAmountProcessed = pTransactionInfo.totalAmount;
                    pTransactionSuccessResult.type = pTransactionInfo.type;

                    pTransactionSuccessResult.vsp = new WiJSON.TransactionResultSuccess.Vsp()
                    {
                        id = pWiCode.CustomerWiCode_id,
                        name = "WiCode",
                        trxId = pWiCode.wiTrxId.HasValue ? pWiCode.wiTrxId.ToString() : pWiCode.CustomerWiCode_id.ToString()
                    };
                    pTransactionSuccessResult.wiTrxId = pTransactionInfo.wiTrxId;

                    #endregion

                    if (pWiCode != null)
                    {
                        pWiCode.wiTrxId = pTransactionInfo.wiTrxId;
                        dc.SubmitChanges();

                        if (pWiCode.DateExpire <= DateTime.Now)
                        {
                            // Fail - WiCode Expired
                            pTransactionFailResult.responseCode = "2";
                            pTransactionFailResult.responseDesc = "WiCode Expired";
                            EndPage(pTransactionFailResult);
                        }
                        else
                        {
                            if (pWiCode.transactionType != pTransactionInfo.type)
                            {
                                // Fail - TransactionType not what it was originally.
                                pTransactionFailResult.responseCode = "4";
                                pTransactionFailResult.responseDesc = "transactionType mismatch";

                                EndPage(pTransactionFailResult);
                            }


                            pCustomerAccount = (from p in dc.Accounts
                                                where p.Customer_id == pWiCode.Customer_id
                                                select p).SingleOrDefault();

                            strDescription = pWiCode.transactionType + " - " + pWiCode.WiCode;

                            decimal fAmount = pWiCode.Amount;

                            switch (pWiCode.transactionType)
                            {
                                case "DEPOSIT":
                                    var pDepositResult = Bll.ThirdParty.WiGroupC.ExecuteTransaction(pTransactionInfo, Request["REMOTE_ADDR"]);
                                    if (pDepositResult.ReplyCode == -1)
                                    {
                                        bDidSucceed = true;
                                        pTransactionSuccessResult.responseCode = pDepositResult.ReplyCode.ToString();
                                        pTransactionSuccessResult.responseDesc = pDepositResult.ReplyMessage;
                                    }
                                    else
                                    {
                                        // Fail - Already used
                                        bDidSucceed = false;
                                        pTransactionFailResult.responseCode = "3";
                                        pTransactionFailResult.responseDesc = "WiCode already claimed";
                                    }
                                    break;
                                case "PAYMENT":
                                    var pPaymentResult = Bll.ThirdParty.WiGroupC.ExecuteTransaction(pTransactionInfo, Request["REMOTE_ADDR"]);

                                    if (pPaymentResult.ReplyCode == 0)
                                    {
                                        bDidSucceed = true;
                                        pTransactionSuccessResult.responseCode = pPaymentResult.ReplyCode.ToString();
                                        pTransactionSuccessResult.responseDesc = pPaymentResult.ReplyMessage;

                                        pTransactionSuccessResult.vsp.responseCode = pPaymentResult.ReplyCode.ToString();
                                        pTransactionSuccessResult.vsp.responseDesc = pPaymentResult.ReplyMessage;
                                    }
                                    else
                                    {
                                        bDidSucceed = false;
                                        pTransactionFailResult.responseCode = pPaymentResult.ReplyCode.ToString();
                                        pTransactionFailResult.responseDesc = pPaymentResult.ReplyMessage;
                                    }
                                    break;
                                case "WITHDRAWAL":
                                    var pWITHDRAWALResult = Bll.ThirdParty.WiGroupC.ExecuteTransaction(pTransactionInfo, Request["REMOTE_ADDR"]);

                                    if (pWITHDRAWALResult.ReplyCode == 0)
                                    {
                                        bDidSucceed = true;
                                        pTransactionSuccessResult.responseCode = pWITHDRAWALResult.ReplyCode.ToString();
                                        pTransactionSuccessResult.responseDesc = pWITHDRAWALResult.ReplyMessage;

                                        pTransactionSuccessResult.vsp.responseCode = pWITHDRAWALResult.ReplyCode.ToString();
                                        pTransactionSuccessResult.vsp.responseDesc = pWITHDRAWALResult.ReplyMessage;
                                    }
                                    else
                                    {
                                        bDidSucceed = false;
                                        pTransactionFailResult.responseCode = pWITHDRAWALResult.ReplyCode.ToString();
                                        pTransactionFailResult.responseDesc = pWITHDRAWALResult.ReplyMessage;
                                    }
                                    break;
                            }
                        }
                    }
                    else
                    {
                        // Fail - Not found
                        pTransactionFailResult.responseCode = "1";
                        pTransactionFailResult.responseDesc = "WiCode not found";
                    }
                    if (bDidSucceed)
                    {
                        pTransactionSuccessResult.responseCode = "-1";
                        pTransactionSuccessResult.vsp.responseCode = "-1";

                        Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(pTransactionSuccessResult, Newtonsoft.Json.Formatting.Indented));
                    }
                    else
                        Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(pTransactionFailResult, Newtonsoft.Json.Formatting.Indented));
                    break;

                case "AdviseRequest":
                    WiJSON.Advise pAdvice = Newtonsoft.Json.JsonConvert.DeserializeObject<WiJSON.Advise>(strJsonRequest);

                    WiJSON.AdviseResponse pAdviceResult = new WiJSON.AdviseResponse();

                    #region Find WiCode previously requested from WiGroup

                    pWiCode = (from p in dc.CustomerWiCodes
                               where p.wiTrxId == pAdvice.originalTrx.wiTrxId
                               select p).SingleOrDefault();

                    #endregion

                    #region Check if wiCode exists

                    if (pWiCode == null)
                    {
                        pAdviceResult.responseCode = "4";
                        pAdviceResult.responseDesc = "Unable to locate WiCode - " + pAdvice.originalTrx.wiTrxId;
                        EndPage(pAdviceResult);
                    }

                    #endregion

                    #region Check trans type

                    if (pWiCode.transactionType != pAdvice.originalTrx.type)
                    {
                        // Fail - TransactionType not what it was originally.
                        pAdviceResult.responseCode = "4";
                        pAdviceResult.responseDesc = "transactionType mismatch";

                        EndPage(pAdviceResult);
                    }

                    #endregion

                    pCustomerAccount = (from p in dc.Accounts
                                        where p.Customer_id == pWiCode.Customer_id
                                        select p).SingleOrDefault();

                    strDescription = "WiGroupC" + ": " + pWiCode.wiTrxId;


                    bDoesExist = Bll.Accounts.Balance.Search(new Bll.Accounts.Balance.SearchFilters() { Amount = new Infrastructure.Range<decimal?>(pWiCode.Amount * -1.00m, pWiCode.Amount), AccountID = pCustomerAccount.Account_id, CurrencyIso = "ZAR", Text = strDescription, IsPending = null }, null).Any();

                    if (!bDoesExist)
                    {
                        pAdviceResult.responseCode = "4";
                        pAdviceResult.responseDesc = "Unable to locate transaction - " + pAdvice.originalTrx.wiTrxId;
                        EndPage(pAdviceResult);
                    }

                    var pBalance = Bll.Accounts.Balance.Search(new Bll.Accounts.Balance.SearchFilters() { Amount = new Infrastructure.Range<decimal?>(pWiCode.Amount * -1.00m, pWiCode.Amount), AccountID = pCustomerAccount.Account_id, CurrencyIso = "ZAR", Text = strDescription, IsPending = null }, null).First();

                    if (pAdvice.action == "FINALISE")
                    {
                        var pBalanceEntry = (from p in dc.AccountBalances
                                             where p.AccountBalance_id == pBalance.ID
                                             select p).SingleOrDefault();

                        pBalanceEntry.IsPending = false;
                        pBalanceEntry.TotalBalance = (from p in dc.AccountBalances
                                                      where p.AccountBalance_id == pBalanceEntry.AccountBalance_id
                                                      select p.Amount).Sum();
                        dc.SubmitChanges();

                        pAdviceResult.responseCode = "-1";
                        pAdviceResult.responseDesc = "Success - " + pAdvice.originalTrx.wiTrxId;
                    }
                    else if (pAdvice.action == "REVERSE")
                    {
                        var pBalanceEntry = (from p in dc.AccountBalances
                                             where p.AccountBalance_id == pBalance.ID
                                             select p).SingleOrDefault();

                        pBalanceEntry.IsPending = false;
                        pBalanceEntry.TotalBalance = (from p in dc.AccountBalances
                                                      where p.AccountBalance_id == pBalanceEntry.AccountBalance_id
                                                      select p.Amount).Sum();
                        dc.SubmitChanges();
                        pBalance.Reverese();

                        pAdviceResult.responseCode = "-1";
                        pAdviceResult.responseDesc = "Success - " + pAdvice.originalTrx.wiTrxId;
                    }
                    else
                    {
                        pAdviceResult.responseCode = "1";
                        pAdviceResult.responseDesc = "Error - action not supplied";
                    }

                    Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(pAdviceResult, Newtonsoft.Json.Formatting.Indented));
                    break;
            }



            Response.End();
        }


        protected void EndPage(object bJsonObject)
        {
            Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(bJsonObject, Newtonsoft.Json.Formatting.Indented));
            Response.End();
        }
    }
}