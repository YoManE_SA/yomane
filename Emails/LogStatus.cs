﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Emails
{
    public class LogStatus
    {

        public class SearchFilters
        {
            public Infrastructure.Range<int?> ID;
            public Infrastructure.Range<DateTime?> Date;
            public int? StatusNew;
            public int? StatusOld;
            public int? MessageID;
            public string UserName;
        }
        private RootDataObject _rootDataObject;

        private DAL.LogStatus _entity;
        public int ID { get { return _entity.LogStatus_id; } set { _entity.LogStatus_id = value; } }
        public int StatusOld { get { return _entity.StatusOld; } set { _entity.StatusOld = value; } }
        public int StatusNew { get { return _entity.StatusNew; } set { _entity.StatusNew = value; } }
        public DateTime Date { get { return _entity.InsertDate; } set { _entity.InsertDate = value; } }
        public string UserName { get { return _entity.AdmincashUser; } set { _entity.AdmincashUser = value; } }
        public string Message { get { return _entity.Message; } set { _entity.Message = value; } }
        public int? MessageId { get { return _entity.Message_id; } set { _entity.Message_id = value; } }
        public string StatusOldText { get { var value = MessageStatus.Load(StatusOld); if (value != null) return value.Name; return string.Empty; } set { StatusOldText = value; } }
        public string StatusNewText { get { var value = MessageStatus.Load(StatusNew); if (value != null) return value.Name; return string.Empty; } set { StatusNewText = value; } }

        public static List<LogStatus> Search(SearchFilters filters)
        {
            var context = new RootDataObject();
            var exp = (from l in context.DataContext.LogStatus orderby l.LogStatus_id descending select l) as IQueryable<DAL.LogStatus>;
            if (filters != null)
            {
                if (filters.MessageID.HasValue) exp = exp.Where(l => l.Message_id == filters.MessageID);
            }
            return exp.AsEnumerable().Select(l => new LogStatus() { _entity = l }).ToList();
        }

        public LogStatus()
        {
            _rootDataObject = new RootDataObject();
            _entity = new DAL.LogStatus();

        }

        public static LogStatus Create(Guid credentialsToken, int messageId, int? oldStatus, int newStatus, string message)
        {
            return null;
        }

        public static LogStatus Load(int? id)
        {
            var context = new RootDataObject();
            var ret = (from m in context.DataContext.LogStatus where m.LogStatus_id == id
                       select new { entity = m}).SingleOrDefault();
            if (ret == null) return null;
            return new LogStatus() { _entity = ret.entity, _rootDataObject = context };
        }
        public void Save()
        {
            if (string.IsNullOrEmpty(Domain.Current.CRMConnectionString))
                return;
            ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
            var context = new DAL.EmailsEntities(Domain.Current.CRMConnectionString);

            context.AddToLogStatus(_entity);
            context.SaveChanges();
            context.Dispose();

         //   _rootDataObject.DataContext.LogStatus.AddObject(_entity);
         //_rootDataObject.DataContext.SaveChanges();
        }
    }
}
