﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net.Security;

namespace Netpay.Emails.Email
{
    public class PopException : ApplicationException
    {
        public PopException() { }
        public PopException(string ErrorMessage) : base(ErrorMessage) { }
    }

    public struct EmailUid
    {
        public int EmailId { get; set; }
        public string Uid { get; set; }
        public EmailUid(int emailId, string uid) : this() { EmailId = emailId; Uid = uid; }
    }

    public enum PopConnectionStateEnum { None = 0, Disconnected, Authorization, Connected, Closed }
    
    public class PopClient
    {
        public delegate void WarningHandler(string WarningText, string Response);
        public delegate void TraceHandler(string TraceText);        
        public event WarningHandler Warning;
		public event TraceHandler Trace;

        protected void CallWarning(string methodName, string response, string warningText, params object[] warningParameters)
        {
            warningText = string.Format(warningText, warningParameters);
            if (Warning != null)
            {
                Warning(methodName + ": " + warningText, response);
            }
            CallTrace("!! {0}", warningText);
        }

        protected void CallTrace(string text, params object[] parameters)
        {
            if (Trace != null)
            {
                Trace(DateTime.Now.ToString("hh:mm:ss ") + PopServer + " " + string.Format(text, parameters));
            }
        }
        protected void TraceFrom(string text, params object[] parameters)
        {
            if (Trace != null)
            {
                CallTrace("   " + string.Format(text, parameters));
            }
        }

        public string PopServer { get; set; }
        public int Port { get; set; }
        public bool UseSSL { get; set; }
        public bool IsAutoReconnect { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public PopConnectionStateEnum Pop3ConnectionState { get { return pop3ConnectionState; } }

        protected int readTimeout = -1;
        protected PopConnectionStateEnum pop3ConnectionState = PopConnectionStateEnum.Disconnected;
        private bool isTimeoutReconnect = false;
        private TcpClient serverTcpConnection;
        private Stream pop3Stream;
        private StreamReader pop3StreamReader;
        private string CRLF = "\r\n";


        public int ReadTimeout
        {
            get { return readTimeout; }
            set
            {
                readTimeout = value;
                if (pop3Stream != null && pop3Stream.CanTimeout)
                {
                    pop3Stream.ReadTimeout = readTimeout;
                }
            }
        }

        protected void setPop3ConnectionState(PopConnectionStateEnum State)
        {
            pop3ConnectionState = State;
            CallTrace("   Pop3MailClient Connection State {0} reached", State);
        }

        protected void EnsureState(PopConnectionStateEnum requiredState)
        {
            if (pop3ConnectionState != requiredState)
            {
                // wrong connection state
                throw new PopException("GetMailboxStats only accepted during connection state: " + requiredState.ToString() +
                      "\n The connection to server " + PopServer + " is in state " + pop3ConnectionState.ToString());
            }
        }

        public PopClient() { }

        public void Connect()
        {
            if (pop3ConnectionState != PopConnectionStateEnum.Disconnected &&
              pop3ConnectionState != PopConnectionStateEnum.Closed &&
              !isTimeoutReconnect)
            {
                CallWarning("connect", "", "Connect command received, but connection state is: " + pop3ConnectionState.ToString());
            }
            else
            {
                //establish TCP connection
                try
                {
                    CallTrace("   Connect at port {0}", Port);
                    serverTcpConnection = new TcpClient(PopServer, Port);
                }
                catch (Exception ex)
                {
                    throw new PopException("Connection to server " + PopServer + ", port " + Port + " failed.\nRuntime Error: " + ex.ToString());
                }

                if (UseSSL)
                {
                    //get SSL stream
                    try
                    {
                        CallTrace("   Get SSL connection");
                        pop3Stream = new SslStream(serverTcpConnection.GetStream(), false);
                        pop3Stream.ReadTimeout = readTimeout;
                    }
                    catch (Exception ex)
                    {
                        throw new PopException("Server " + PopServer + " found, but cannot get SSL data stream.\nRuntime Error: " + ex.ToString());
                    }

                    //perform SSL authentication
                    try
                    {
                        CallTrace("   Get SSL authentication");
                        ((SslStream)pop3Stream).AuthenticateAsClient(PopServer);
                    }
                    catch (Exception ex)
                    {
                        throw new PopException("Server " + PopServer + " found, but problem with SSL Authentication.\nRuntime Error: " + ex.ToString());
                    }
                }
                else
                {
                    //create a stream to POP3 server without using SSL
                    try
                    {
                        CallTrace("   Get connection without SSL");
                        pop3Stream = serverTcpConnection.GetStream();
                        pop3Stream.ReadTimeout = readTimeout;
                    }
                    catch (Exception ex)
                    {
                        throw new PopException("Server " + PopServer + " found, but cannot get data stream (without SSL).\nRuntime Error: " + ex.ToString());
                    }
                }

                try
                {
                    pop3StreamReader = new StreamReader(pop3Stream, Encoding.ASCII);
                }
                catch (Exception ex)
                {
                    if (UseSSL) throw new PopException("Server " + PopServer + " found, but cannot read from SSL stream.\nRuntime Error: " + ex.ToString());
                    else throw new PopException("Server " + PopServer + " found, but cannot read from stream (without SSL).\nRuntime Error: " + ex.ToString());
                }

                string response;
                if (!readSingleLine(out response))
                    throw new PopException("Server " + PopServer + " not ready to start AUTHORIZATION.\nMessage: " + response);
                setPop3ConnectionState(PopConnectionStateEnum.Authorization);

                if (!executeCommand("USER " + Username, out response))
                    throw new PopException("Server " + PopServer + " doesn't accept username '" + Username + "'.\nMessage: " + response);

                if (!executeCommand("PASS " + Password, out response))
                    throw new PopException("Server " + PopServer + " doesn't accept password '" + Password + "' for user '" + Username + "'.\nMessage: " + response);

                setPop3ConnectionState(PopConnectionStateEnum.Connected);
            }
        }

        public void Disconnect()
        {
            if (pop3ConnectionState == PopConnectionStateEnum.Disconnected ||
              pop3ConnectionState == PopConnectionStateEnum.Closed)
            {
                CallWarning("disconnect", "", "Disconnect received, but was already disconnected.");
            }
            else
            {
                //ask server to end session and possibly to remove emails marked for deletion
                try
                {
                    string response;
                    if (executeCommand("QUIT", out response))
                    {
                        //server says everything is ok
                        setPop3ConnectionState(PopConnectionStateEnum.Closed);
                    }
                    else
                    {
                        //server says there is a problem
                        CallWarning("Disconnect", response, "negative response from server while closing connection: " + response);
                        setPop3ConnectionState(PopConnectionStateEnum.Disconnected);
                    }
                }
                finally
                {
                    //close connection
                    if (pop3Stream != null)
                    {
                        pop3Stream.Close();
                    }

                    pop3StreamReader.Close();
                }
            }
        }

        public bool DeleteEmail(int msg_number)
        {
            EnsureState(PopConnectionStateEnum.Connected);
            string response;
            if (!executeCommand("DELE " + msg_number.ToString(), out response))
            {
                CallWarning("DeleteEmail", response, "negative response for email (Id: {0}) delete request", msg_number);
                return false;
            }
            return true;
        }

        public bool GetEmailIdList(out List<int> EmailIds)
        {
            EnsureState(PopConnectionStateEnum.Connected);
            EmailIds = new List<int>();

            //get server response status line
            string response;
            if (!executeCommand("LIST", out response))
            {
                CallWarning("GetEmailIdList", response, "negative response for email list request");
                return false;
            }

            //get every email id
            int EmailId;
            while (readMultiLine(out response))
            {
				TraceFrom("LIST Item:{0}", response);
                if (int.TryParse(response.Split(' ')[0], out EmailId))
                {
                    EmailIds.Add(EmailId);
                }
                else
                {
                    CallWarning("GetEmailIdList", response, "first characters should be integer (EmailId)");
                }
            }
            TraceFrom("{0} email ids received", EmailIds.Count);
            return true;
        }

        public int GetEmailSize(int msg_number)
        {
            EnsureState(PopConnectionStateEnum.Connected);
            string response;
            executeCommand("LIST " + msg_number.ToString(), out response);
            int EmailSize = 0;
            string[] responseSplit = response.Split(' ');
            if (responseSplit.Length < 2 || !int.TryParse(responseSplit[2], out EmailSize))
            {
                CallWarning("GetEmailSize", response, "'+OK int int' format expected (EmailId, EmailSize)");
            }

            return EmailSize;
        }

        public bool GetUniqueEmailIdList(out List<EmailUid> EmailIds)
        {
            EnsureState(PopConnectionStateEnum.Connected);
            EmailIds = new List<EmailUid>();

            //get server response status line
            string response;
            if (!executeCommand("UIDL ", out response))
            {
                CallWarning("GetUniqueEmailIdList", response, "negative response for email list request");
                return false;
            }

            //get every email unique id
            int EmailId;
            while (readMultiLine(out response))
            {
                string[] responseSplit = response.Split(' ');
                if (responseSplit.Length < 2)
                {
                    CallWarning("GetUniqueEmailIdList", response, "response not in format 'int string'");
                }
                else if (!int.TryParse(responseSplit[0], out EmailId))
                {
                    CallWarning("GetUniqueEmailIdList", response, "first charaters should be integer (Unique EmailId)");
                }
                else
                {
                    EmailIds.Add(new EmailUid(EmailId, responseSplit[1]));
                }
            }
            TraceFrom("{0} unique email ids received", EmailIds.Count);
            return true;
        }

        private bool GetUniqueEmailIdList(out SortedList<string, int> EmailIds)
        {
            EnsureState(PopConnectionStateEnum.Connected);
            EmailIds = new SortedList<string, int>();

            //get server response status line
            string response;
            if (!executeCommand("UIDL", out response))
            {
                CallWarning("GetUniqueEmailIdList", response, "negative response for email list request");
                return false;
            }

            //get every email unique id
            int EmailId;
            while (readMultiLine(out response))
            {
                string[] responseSplit = response.Split(' ');
                if (responseSplit.Length < 2)
                {
                    CallWarning("GetUniqueEmailIdList", response, "response not in format 'int string'");
                }
                else if (!int.TryParse(responseSplit[0], out EmailId))
                {
                    CallWarning("GetUniqueEmailIdList", response, "first charaters should be integer (Unique EmailId)");
                }
                else
                {
                    EmailIds.Add(responseSplit[1], EmailId);
                }
            }
            TraceFrom("{0} unique email ids received", EmailIds.Count);
            return true;
        }

        private int GetUniqueEmailId(EmailUid msg_number)
        {
            EnsureState(PopConnectionStateEnum.Connected);
            string response = "";
            executeCommand("LIST " + msg_number.ToString(), out response);
            int EmailSize = 0;
            string[] responseSplit = response.Split(' ');
            if (responseSplit.Length < 2 || !int.TryParse(responseSplit[2], out EmailSize))
            {
                CallWarning("GetEmailSize", response, "'+OK int int' format expected (EmailId, EmailSize)");
            }

            return EmailSize;
        }

        public bool NOOP()
        {
            EnsureState(PopConnectionStateEnum.Connected);
            string response;
            if (!executeCommand("NOOP", out response))
            {
                CallWarning("NOOP", response, "negative response for NOOP request");
                return false;
            }
            return true;
        }

        protected bool isTraceRawEmail = false;
        protected StringBuilder RawEmailSB;

        public bool GetRawEmail(int MessageNo, out string EmailText)
        {
            //send 'RETR int' command to server
			//if (MessageNo == 85)
			//	System.Diagnostics.Debugger.Break();
			int messageSize = GetEmailSize(MessageNo);
			if (!SendRetrCommand(MessageNo))
            {
                EmailText = null;
                return false;
            }

            //get the lines
            string response;
            int LineCounter = 0;
            //empty StringBuilder
            if (RawEmailSB == null)
            {
                RawEmailSB = new StringBuilder(100000);
            }
            else
            {
                RawEmailSB.Length = 0;
            }
            isTraceRawEmail = true;
			//while (messageSize - RawEmailSB.Length > 20 /*misc fix by udi to handle exchange 2003*/)
			//{
				//if (RawEmailSB.Length > 0) RawEmailSB.Append(CRLF + "." + CRLF);
				//RawEmailSB.Append(readMultiLine());
				while (readMultiLine(out response))
				{
					LineCounter += 1;
				}
			//}
			EmailText = RawEmailSB.ToString();
            TraceFrom("email with {0} lines,  {1} chars received", LineCounter.ToString(), EmailText.Length);
            return true;
        }

        public bool GetMailboxStats(out int NumberOfMails, out int MailboxSize)
        {
            EnsureState(PopConnectionStateEnum.Connected);

            //interpret response
            string response;
            NumberOfMails = 0;
            MailboxSize = 0;
            if (executeCommand("STAT", out response))
            {
                //got a positive response
                string[] responseParts = response.Split(' ');
                if (responseParts.Length < 2)
                {
                    //response format wrong
                    throw new PopException("Server " + PopServer + " sends illegally formatted response." +
                      "\nExpected format: +OK int int" +
                      "\nReceived response: " + response);
                }
                NumberOfMails = int.Parse(responseParts[1]);
                MailboxSize = int.Parse(responseParts[2]);
                return true;
            }
            return false;
        }

        protected bool SendRetrCommand(int MessageNo)
        {
            EnsureState(PopConnectionStateEnum.Connected);
            // retrieve mail with message number
            string response;
            if (!executeCommand("RETR " + MessageNo.ToString(), out response))
            {
                CallWarning("GetRawEmail", response, "negative response for email (ID: {0}) request", MessageNo);
                return false;
            }
            return true;
        }

        protected bool isDebug = false;

        private bool executeCommand(string command, out string response)
        {
            //send command to server
            byte[] commandBytes = System.Text.Encoding.ASCII.GetBytes((command + CRLF).ToCharArray());
            CallTrace("Tx '{0}'", command);
            bool isSupressThrow = false;
            try
            {
                pop3Stream.Write(commandBytes, 0, commandBytes.Length);
                if (isDebug)
                {
                    isDebug = false;
                    throw new IOException("Test", new SocketException(10053));
                }
            }
            catch (IOException ex)
            {
                //Unable to write data to the transport connection. Check if reconnection should be tried
                isSupressThrow = executeReconnect(ex, command, commandBytes);
                if (!isSupressThrow)
                {
                    throw;
                }
            }
            pop3Stream.Flush();

            //read response from server
            response = null;
            try
            {
                response = pop3StreamReader.ReadLine();
            }
            catch (IOException ex)
            {
                //Unable to write data to the transport connection. Check if reconnection should be tried
                isSupressThrow = executeReconnect(ex, command, commandBytes);
                if (isSupressThrow)
                {
                    //wait for response one more time
                    response = pop3StreamReader.ReadLine();
                }
                else
                {
                    throw;
                }
            }
            if (response == null)
            {
                throw new PopException("Server " + PopServer + " has not responded, timeout has occured.");
            }
            CallTrace("Rx '{0}'", response);
            return (response.Length > 0 && response[0] == '+');
        }

        private bool executeReconnect(IOException ex, string command, byte[] commandBytes)
        {
            if (ex.InnerException != null && ex.InnerException is SocketException)
            {
                //SocketException
                SocketException innerEx = (SocketException)ex.InnerException;
                if (innerEx.ErrorCode == 10053)
                {
                    //probably timeout: An established connection was aborted by the software in your host machine.
                    CallWarning("ExecuteCommand", "", "probably timeout occured");
                    if (IsAutoReconnect)
                    {
                        //try to reconnect and send one more time
                        isTimeoutReconnect = true;
                        try
                        {
                            CallTrace("   try to auto reconnect");
                            Connect();

                            CallTrace("   reconnect successful, try to resend command");
                            CallTrace("Tx '{0}'", command);
                            pop3Stream.Write(commandBytes, 0, commandBytes.Length);
                            pop3Stream.Flush();
                            return true;
                        }
                        finally
                        {
                            isTimeoutReconnect = false;
                        }

                    }
                }
            }
            return false;
        }

        protected bool readSingleLine(out string response)
        {
            response = null;
            try
            {
                response = pop3StreamReader.ReadLine();
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            if (response == null)
            {
                throw new PopException("Server " + PopServer + " has not responded, timeout has occured.");
            }
            CallTrace("Rx '{0}'", response);
            return (response.Length > 0 && response[0] == '+');
        }

        protected bool readMultiLine(out string response)
        {
            response = null;
            response = pop3StreamReader.ReadLine();
            if (response == null)
            {
                throw new PopException("Server " + PopServer + " has not responded, probably timeout has occured.");
            }
            //check for byte stuffing, i.e. if a line starts with a '.', another '.' is added, unless
            //it is the last line
            if (response.Length > 0 && response[0] == '.')
            {
                if (response == ".")
                {
                    //closing line found
                    return false;
                }
                //remove the first '.'
                response = response.Substring(1, response.Length - 1);
            }
			if (isTraceRawEmail)
			{
				//collect all responses as received
				RawEmailSB.Append(response + CRLF);
			}
			return true;
        }

    }
}
