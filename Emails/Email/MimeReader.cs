﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Netpay.Emails.Email
{
    public class Utils 
    {
        public static string RemoveQuotes(string value)
        {
            if (value.StartsWith("\"") && value.EndsWith("\""))
                return value.Substring(1, value.Length - 2);
            return value;
        }

        public static byte[] DecodeQuotedPrintable(string value)
        {
            int startPos = 0;
            List<byte> dataList = new List<byte>(value.Length);
            int endPos = value.IndexOf('=', startPos);
            while (endPos > -1)
            {
                byte bValue;
                if ((endPos - startPos) > 0) dataList.AddRange(Encoding.ASCII.GetBytes(value.Substring(startPos, endPos - startPos)));
                string octetValue = value.Substring(endPos + 1, 2);
                if (octetValue == "\r\n") { //soft crlf
                    //dataList.AddRange(Encoding.ASCII.GetBytes("\r\n")); 
                } else {
                    if (!byte.TryParse(octetValue, System.Globalization.NumberStyles.HexNumber, null, out bValue))
                        throw new Exception(string.Format("Unable to parse Quoted Printble value '0x{0}'", octetValue));
                    dataList.Add(bValue);
                }
                startPos = endPos + 3;
                endPos = value.IndexOf('=', startPos);
            }
            endPos = value.Length;
            if ((endPos - startPos) > 0) dataList.AddRange(Encoding.ASCII.GetBytes(value.Substring(startPos, endPos - startPos)));
            return dataList.ToArray();
        }

        public static string EncodeQuotedPrintable(byte[] value)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in value)
            {
                if ((b >= 33 && b <= 126) && (b != 61)) sb.Append(Char.ConvertFromUtf32(b));
                else sb.AppendFormat("={0}", b.ToString("x"));
            }
            return sb.ToString();
        }

        public static string DecodeEncodedWord(string value)
        {
            string[] tokens = value.Split('?');
            if (tokens[0] != "=" || tokens[tokens.Length - 1] != "=")
                throw new Exception("Unable to decode EncodedWord, wrong tokens");
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding(tokens[1]);
            if (tokens[2].ToUpper() == "Q") return enc.GetString(Utils.DecodeQuotedPrintable(tokens[3]));
			else if (tokens[2].ToUpper() == "B") return enc.GetString(System.Convert.FromBase64String(tokens[3]));
            throw new Exception("Unable to decode EncodedWord, encoding token must be Q or B");
        }

        public static string EncodeEncodedWord(System.Text.Encoding encoding, bool encodeBase64, string value, int maxLine)
        {
            int start = 0;
            string data = null;
            System.Text.StringBuilder sb = new StringBuilder();
            string prefix = string.Format("=?{0}?{1}?", encoding.WebName, (encodeBase64 ? "B" : "Q"));
            if (encodeBase64) data = System.Convert.ToBase64String(encoding.GetBytes(value));
            else data = Utils.EncodeQuotedPrintable(encoding.GetBytes(value));
            if (maxLine <= 0) maxLine = 0x7FFFFFFF;
            maxLine -= prefix.Length + 2;
            while (start < data.Length)
            {
                string section = data.Substring(start, (start + maxLine > data.Length) ? data.Length - start : maxLine);
                if (start > 0) sb.Append("\t");
                sb.AppendFormat("{0}{1}?=\r\n", prefix, section);
                start += maxLine;
            }
            return sb.ToString(0, sb.Length - 2);
        }
    }

    public class BodyPart
    {
        public string LocalPath { get { return ""; } }

        protected System.Collections.Specialized.NameValueCollection _headers;
        public System.Collections.Generic.List<BodyPart> Parts;
        public string Data { get; set;  }

        public BodyPart()
        {
            _headers = new System.Collections.Specialized.NameValueCollection();
            Parts = new List<BodyPart>();
        }

        private string GetHeaderSubValue(string value, string subValueName) 
        {
            if (value == null) return null;
            int end, start = value.IndexOf(subValueName);
            if (start < 0) return null;
			start += subValueName.Length;
			while (value[start] == ' ') start++;
			if (value[start] != '=') return null;
            start++;
			while (value[start] == ' ') start++;
            if (value[start] == '\"') {
                end = value.IndexOf('\"', ++start);
                if (end < 0) throw new Exception(string.Format("unable to find end \" in the header '{0}' sub value '{1}'", value, subValueName));
            }
            else end = value.IndexOfAny(new char[] { ';', '\r' }, start);
            if (end < 0) end = value.Length;
            return Utils.RemoveQuotes(value.Substring(start, end - start));
        }

        public string this[string key] { get { if (_headers[key] == null) return null; return decodeHeaderValue(_headers[key]); } set { _headers[key] = encodeHeaderValue(value); } }

        public string ContentID { get { return this["Content-ID"]; } set { this["Content-ID"] = value; } }
        public string ContentType { get { return this["Content-Type"]; } }
        public string ContentTypeCharset { get { return GetHeaderSubValue(ContentType, "charset"); } }
        public string ContentTransferEncoding { get { return this["Content-Transfer-Encoding"]; } set { this["Content-Transfer-Encoding"] = value; } }
        public string Boundary { get { return GetHeaderSubValue(ContentType, "boundary"); } }
        public string ContentDisposition { get { return this["Content-Disposition"]; } }
        public bool IsAttachment { get { return (ContentDisposition != null) && ContentDisposition.StartsWith("attachment;"); } }
		public static BodyPart FromFile(System.IO.Stream data, string fileName)
		{
			var part = new BodyPart();
			part["Content-Disposition"] = "attachment;filename=" + fileName;
			var bytes = new byte[data.Length];
			data.Read(bytes, 0, bytes.Length);
			//part.DecodedData = data;
			return part;
		}
        //public System.Net.Mime.ContentDisposition ContentDispositionObject { get { return new System.Net.Mime.ContentDisposition(Headers["Content-Disposition"]); } }
        //public System.Net.Mime.ContentType ContentTypeObject { get { return new System.Net.Mime.ContentType(Headers["Content-Type"]); } }
        public string FileName
        { 
            get {
                if (IsAttachment) return GetHeaderSubValue(ContentDisposition, "filename");
                return GetHeaderSubValue(ContentType, "name"); 
            } 
        }

        public byte[] DecodedData
        { 
            get {
				string contentTransferEncoding = ContentTransferEncoding;
				if (contentTransferEncoding == null) contentTransferEncoding = "";
				if (contentTransferEncoding.ToLower() == "base64") return System.Convert.FromBase64String(Data);
				else if (contentTransferEncoding.ToLower() == "quoted-printable") return Utils.DecodeQuotedPrintable(Data);
                return System.Text.Encoding.ASCII.GetBytes(Data);
            }  
        }

        public string Text 
        { 
            get { 
				string charset = ContentTypeCharset;
				if (string.IsNullOrEmpty(charset)) charset = System.Text.Encoding.Default.WebName;
				return System.Text.Encoding.GetEncoding(charset).GetString(DecodedData); 
			}
            set { Data = value; } 
        }

        public void SaveFile(string filePath, string newFileName, bool overWrite) 
        {
            if (newFileName == null) newFileName = FileName;
            newFileName = System.IO.Path.Combine(filePath, newFileName);
            System.IO.FileStream fs = new System.IO.FileStream(newFileName, (overWrite ? System.IO.FileMode.Create : System.IO.FileMode.CreateNew));
            byte[] bt = DecodedData;
            fs.Write(bt, 0, bt.Length);
            fs.Close();
        }

        private string decodeHeaderValue(string values)
        {
            StringBuilder sb = new StringBuilder();
            string[] lines = values.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lines)
            {
				string value = line.TrimStart('\t');
				int sIndex = 0, eIndex = value.IndexOf("=?");
                while (eIndex > -1)
                {
                    if ((eIndex - sIndex) > 0) sb.Append(value.Substring(sIndex, eIndex - sIndex));
                    sIndex = eIndex + 2;
					eIndex = value.IndexOf("?", sIndex); //start sentinel
					if (eIndex < 0) throw new Exception("Unable to decode header EncodedWord value missing ? at start:\r\n" + value);
					string encodingText = value.Substring(sIndex, eIndex - sIndex).Trim();
					if (encodingText.ToUpper() == "UTF8") encodingText = "UTF-8";
					System.Text.Encoding enc = System.Text.Encoding.GetEncoding(encodingText);
					sIndex = eIndex + 1;
					char decodeType = value[sIndex];
					sIndex = eIndex + 3;
					eIndex = value.IndexOf("?=", sIndex); //end sentinel
					if (eIndex < 0) throw new Exception("Unable to decode header EncodedWord value missing ?= at end:\r\n" + value);

					if (char.ToUpper(decodeType) == 'Q') sb.Append(enc.GetString(Utils.DecodeQuotedPrintable(value.Substring(sIndex, eIndex - sIndex))));
					else if (char.ToUpper(decodeType) == 'B') sb.Append(enc.GetString(System.Convert.FromBase64String(value.Substring(sIndex, eIndex - sIndex))));
					else throw new Exception("Unable to decode EncodedWord, encoding token must be Q or B");

					sIndex = eIndex + 2;
                    eIndex = value.IndexOf("=?", sIndex);
                }
                eIndex = value.Length;
                if ((eIndex - sIndex) > 0) sb.Append(value.Substring(sIndex, eIndex - sIndex));
            }
            return sb.ToString();
        }

        private string encodeHeaderValue(string value)
        {
            bool nonAnsiFound = false;
            bool nonAsciiFound = false;
            foreach (char c in value)
                if (c > 127) { nonAnsiFound = true; }
                else if (c > 255) { nonAsciiFound = true; break; }
            if (nonAsciiFound)
                return Utils.EncodeEncodedWord(Encoding.UTF8, true, value, 75);
            if (nonAnsiFound)
                return Utils.EncodeEncodedWord(Encoding.Default, true, value, 75);
            else return value;
        }

        public virtual void ParsePart(string data) 
        {
            int startIndex = 0;
            string lastHeaderName = null;
            int nextLine = data.IndexOf("\r\n", startIndex);
            while (nextLine > -1)
            {
                string headLine = data.Substring(startIndex, nextLine - startIndex);
                if (headLine == "") break;
                if (headLine.StartsWith("\t") || headLine.StartsWith(" ")) {
                    var values = _headers.GetValues(lastHeaderName);
                    values[values.Length - 1] += "\r\n" + headLine;
                    _headers.Remove(lastHeaderName);
                    foreach (var v in values)
                        _headers.Add(lastHeaderName, v);
                } else {
                    string[] headParts = headLine.Split(new char[] { ':' }, 2);
                    lastHeaderName = headParts[0];
					if (headParts[1].Length > 0) headParts[1] = headParts[1].Substring(1);
                    _headers.Add(headParts[0], headParts[1]);
                }
                startIndex = nextLine + 2;
                nextLine = data.IndexOf("\r\n", startIndex);
            }
            Data = data.Substring(nextLine + 2);
        }

        public string MessageData
        {
            get {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < _headers.Count; i++)
                    foreach(var v in _headers.GetValues(i))
                        sb.AppendFormat("{0}: {1}\r\n", _headers.GetKey(i), v);
                sb.Append("\r\n");
                sb.Append(Data);
                foreach (var c in Parts)
                    sb.Append(c.MessageData);
                return sb.ToString();
            }
        }
    }

    public class Message : BodyPart
    {
        private BodyPart _htmlPart, _textPart;
        public System.Collections.Generic.List<BodyPart> Attachments;

		public string From { get { return this["From"]; } set { this["From"] = value; } }
		public string To { get { return this["To"]; } set { this["To"] = value; } }
		public string Cc { get { return this["Cc"]; } set { this["Cc"] = value; } }
        public string MessageID { get { return _headers["Message-ID"]; } set { _headers["Message-ID"] = value; } }
        public string InReplyTo { get { return _headers["In-Reply-To"]; } set { _headers["In-Reply-To"] = value; } }
        public string References { get { return _headers["References"]; } set { _headers["References"] = value; } }
        public string Date { get { return _headers["Date"]; } set { _headers["Date"] = value; } }
        public string Subject { get { return this["Subject"]; } set { this["Subject"] = value; } }
        //public string ReplyTo { get { return Headers["Return-Path"]; } }

        public System.Net.Mail.MailAddress FromAddress 
        { 
            get { return new System.Net.Mail.MailAddress(From); } 
            set { From = value.ToString(); } 
        }
        public System.Net.Mail.MailAddressCollection ToCollection
        {
            get { 
				var ret = new System.Net.Mail.MailAddressCollection(); 
				if (To != null) {
					string strTo = To;
					if (strTo.StartsWith(";")) strTo = strTo.Substring(1);
					if (strTo.EndsWith(";")) strTo = strTo.Substring(0, strTo.Length - 1);
					ret.Add(strTo.Replace("\r", "").Replace("\n", "").Replace(';', ',')); 
				}
				return ret; 
			}
            set { To = value.ToString(); } 
        }
        public System.Net.Mail.MailAddressCollection CcCollection {
			get { 
				var ret = new System.Net.Mail.MailAddressCollection();
				if (Cc != null) {
					string strCc = Cc;
					if (strCc.StartsWith(";")) strCc = strCc.Substring(1);
					if (strCc.EndsWith(";")) strCc = strCc.Substring(0, strCc.Length - 1);
					ret.Add(strCc.Replace(';', ',')); 
				}
				return ret; 
			}
            set { Cc = value.ToString(); }
        }

        public string PlainText { get { if (_textPart == null) return null; return _textPart.Text; } set { if (_textPart == null) Parts.Add(_textPart = new BodyPart()); _textPart.Text = value; } }
        public string HTMLText { get { if (_htmlPart == null) return null; return _htmlPart.Text; } set { if (_htmlPart == null) Parts.Add(_htmlPart = new BodyPart()); _htmlPart.Text = value; } }

        public Message() 
        {
            Attachments = new List<BodyPart>();
        }

        public Message(string filePath) 
        { 
            Attachments = new List<BodyPart>();
			Parse(System.IO.File.ReadAllText(filePath));
        }

        public void SaveMessage(string fileName)
        {
            var sw = new System.IO.StreamWriter(fileName);
            sw.Write(MessageData);
            sw.Close();
        }

		public void Parse(string fileData)
		{
			if (fileData.EndsWith("\r\n.\r\n")) fileData = fileData.Substring(0, fileData.Length - 5);
			ParseMessage(this, fileData); 
		} 

        private BodyPart ParseMessage(BodyPart part, string fileData)
        {
            if (part == null) part = new BodyPart();
            part.ParsePart(fileData);
            string contentType = part.ContentType;
            if (contentType == null) contentType = "";
            if (contentType.StartsWith("multipart")) {
                string boundry = part.Boundary;
                if (boundry == null) throw new Exception("unable to find message multipart boundary value");
                string[] strParts = part.Data.Split(new string[] { "--" + boundry }, StringSplitOptions.RemoveEmptyEntries);
                part.Data = strParts[0].Substring(0, strParts[0].Length - 2);
                for (int i = 1; i < strParts.Length; i++)
                {
                    if (strParts[i].StartsWith("--")) break;
                    string strPart = strParts[i].Substring(2, strParts[i].Length - 4);
                    if (strPart.Length > 0) part.Parts.Add(ParseMessage(null, strPart));
                }
            } else {
                if (!part.IsAttachment) {
                    if (contentType.StartsWith("text/plain")) _textPart = part;
                    else if (contentType.StartsWith("text/html")) _htmlPart = part;
                    else Attachments.Add(part);
                }
                else Attachments.Add(part);
            }
            return part;
        }
    }
}