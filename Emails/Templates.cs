﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Dal.DataAccess;

namespace Netpay.Emails
{
    public static class Templates
    {
        /*************************TEMPLATES*************************/
		private static string mapTemplateFileName(string domainHost, string fileName)
		{
			var domain = Netpay.Infrastructure.Domain.Get(domainHost);
			return System.IO.Path.ChangeExtension(System.IO.Path.Combine(domain.CRMEmailTemplatePath, fileName), ".txt");
		}

		public static void SaveTemplete(string domainHost, string fileName, string content)
        {
			using (System.IO.StreamWriter file = new System.IO.StreamWriter(mapTemplateFileName(domainHost, fileName), false))
            {
                file.Write(new System.Text.StringBuilder(content));
                file.Close();
            }
        }

		public static void DeleteTemplate(string domainHost, string fileName)
		{
			fileName = mapTemplateFileName(domainHost, fileName);
			if (System.IO.File.Exists(fileName)) 
				System.IO.File.Delete(fileName);
		}

		public static string[] GetTemplates(string domainHost)
        {
			var domain = Netpay.Infrastructure.Domain.Get(domainHost);
			string[] fileNames = System.IO.Directory.GetFiles(domain.CRMEmailTemplatePath);
			for (int i = 0; i < fileNames.Length; i++)
				fileNames[i] = System.IO.Path.GetFileNameWithoutExtension(fileNames[i]);
			return fileNames;
        }

		public static string GetTemplate(string domainHost, string fileName)
		{
			string ret = null;
			using (System.IO.StreamReader file = new System.IO.StreamReader(mapTemplateFileName(domainHost, fileName)))
			{
				ret = file.ReadToEnd();
				file.Close();
			}
			return ret;
		}

    }
}
