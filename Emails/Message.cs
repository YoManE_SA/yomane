using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Emails
{
	public class Message
	{
		public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Emails.Module.Current, SecuredObjectName); } }

	
		private DAL.Message _entity;
     
        private RootDataObject _rootDataObject;
        
        public class SearchFilters
		{
			public Infrastructure.Range<DateTime?> Date;
            public int? MessageId;
			public int? mailboxId;
			public int? status;
			public bool? sent;
			public bool? deleted;
			public Guid? threadID;
			public int? MerchantId;
			public string user;
			public string mailAddress;
			public string Text;
		}


		public int ID { get { return _entity.Message_id; } }
		public DateTime Date { get { return _entity.MessagsDate; } }
		public string MimeMessageID { get { return _entity.MessageMimeID; } }
		public Guid? ThreadID { get { return _entity.ThreadID; } }
		public string EmailFrom { get { return _entity.EmailFrom; } set { _entity.EmailFrom = value; } }
		public string EmailTo { get { return _entity.EmailTo; } set { _entity.EmailTo = value; } }
		public string Subject { get { return _entity.Subject; } set { _entity.Subject = value; } }
		public string CC { get { return _entity.CC; } set { _entity.CC = value; } }
		public string Text { get { return _entity.BodyText; } set { _entity.BodyText = value; } }
		public int? Status { get { return _entity.MessageStatus_id; } set { _entity.MessageStatus_id = (byte) value; } }
		public string AdmincashUser { get { return _entity.AdmincashUser; } set { _entity.AdmincashUser = value; } }
		public bool IsSent { get { return _entity.IsSent; } set { _entity.IsSent = value; } }
		public bool IsDeleted { get { return _entity.IsDeleted; } set { _entity.IsDeleted = value; } }
		public int AttachmentCount { get { return _entity.AttachmentCount.GetValueOrDefault(); } set { _entity.AttachmentCount = (byte) value; } }
		public int? MerchantID { get { return _entity.Merchant_id; } set { _entity.Merchant_id = value; } }
		public int MailBoxID { get { return _entity.Mailbox_id; } set { _entity.Mailbox_id = (short) value; } }
		public string MailBoxPath { get; private set; }

		private Email.Message _mimeMessage;
		private Message(bool load) { }

		public Message()
		{
			_rootDataObject = new RootDataObject();
			_entity = new DAL.Message();
			_mimeMessage = new Email.Message();
		}

		public Email.Message MimeMessage { 
			get
            {
				if(_mimeMessage != null) return _mimeMessage;
				string fileName = getMessageFileName(ID, MailBoxPath);
				if (System.IO.File.Exists(fileName)) _mimeMessage = new Email.Message(fileName);
				return _mimeMessage; 
			} 
		}

		public static Message Load(int id)
		{
		if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

		
			var context = new RootDataObject();
			var ret = ( from m in context.DataContext.Message
				join b in context.DataContext.Mailbox on m.Mailbox_id equals b.Mailbox_id
				where m.Message_id == id
				select new { entity = m, MailBoxPath = b.MessageStoragePath } ).SingleOrDefault();
			if (ret == null) return null;
			return new Message(true) { _entity = ret.entity, MailBoxPath = ret.MailBoxPath, _rootDataObject = context };
		}

		public static string getMessageFileName(int messageId, string mailBox)
		{
			return System.IO.Path.Combine(mailBox, string.Format("{0}.eml", messageId));
		}

		public void ChangeStatus(byte newStatus, string userMessage)
		{
			Status = newStatus;
			Save();
        }

		public void Save() 
		{
			if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);
			_rootDataObject.DataContext.SaveChanges();
		}


		public void setMerchant(int? merchantId, bool addEmailToMerchant, bool isAssingAllMailsHistory)
		{
			if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

		
			_entity.Merchant_id = merchantId;
			_rootDataObject.DataContext.SaveChanges();
			if (merchantId != null)
			{
				if (isAssingAllMailsHistory)
				{
					var dd = (from mm in _rootDataObject.DataContext.Message where (mm.EmailFrom == _entity.EmailFrom && mm.Merchant_id == null) select mm).ToList();
					foreach (var item in dd)
						item.Merchant_id = merchantId;
					_rootDataObject.DataContext.SaveChanges();
				}
				if (addEmailToMerchant)
					EmailToMerchant.AddEmailToMerchant(_rootDataObject.Domain.Host, merchantId.Value, _entity.EmailFrom);
			}
		}

		public static List<Message> Search(SearchFilters filters, Infrastructure.ISortAndPage sortAndPage)
		{
			if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

		
            var context = new RootDataObject();
			var res = (from m in context.DataContext.Message orderby m.Message_id descending select m) as IQueryable<DAL.Message>;
			if (filters != null)
			{
                if(filters.MessageId !=null) res = res.Where(m => m.Message_id== filters.MessageId);
                if (filters.mailboxId != null) res = res.Where(m => m.Mailbox_id == filters.mailboxId);
				if (filters.status != null) res = res.Where(m => m.MessageStatus_id == filters.status);
                if (filters.deleted.HasValue) res = res.Where(m => m.IsDeleted == filters.deleted);

				if (filters.Date.From.HasValue) res = res.Where(m => m.MessagsDate >= filters.Date.From);
				if (filters.Date.To.HasValue) res = res.Where(m => m.MessagsDate <= filters.Date.To);

				if (filters.threadID != null) res = res.Where(m => m.ThreadID == filters.threadID);
				if (filters.MerchantId != null) res = res.Where(m => m.Merchant_id == filters.MerchantId);
				if (filters.user != null) res = res.Where(m => m.AdmincashUser == filters.user);
				if (filters.sent != null) res = res.Where(m => m.IsSent == filters.sent);

				if (!string.IsNullOrEmpty(filters.mailAddress)) res = res.Where(m => m.EmailFrom.Contains(filters.mailAddress) || m.EmailTo.Contains(filters.mailAddress));
				if (!string.IsNullOrEmpty(filters.Text)) res = res.Where(m => m.Subject.Contains(filters.Text) || m.BodyText.Contains(filters.Text));
			}
			/*
			if (filters.sortKey != null)
			{
				switch (filters.sortKey)
				{
					case "Subject": res = sortDesc ? res.OrderByDescending(m => m.Subject) : res.OrderBy(m => m.Subject); break;
					case "EmailFrom": res = sortDesc ? res.OrderByDescending(m => m.EmailFrom) : res.OrderBy(m => m.EmailFrom); break;
					case "MessageStatus": res = sortDesc ? res.OrderByDescending(m => m.MessageStatus_id) : res.OrderBy(m => m.MessageStatus_id); break;
					case "":
					case "MessageDate": res = sortDesc ? res.OrderByDescending(m => m.MessagsDate) : res.OrderBy(m => m.MessagsDate); break;
					default: throw new Exception(string.Format("Unknown sort key '{0}'", sortKey));
				}
			}
			*/
			return res.ApplySortAndPage(sortAndPage).AsEnumerable().Select(r => new Message(true) { _entity = r, _rootDataObject = context }).ToList();
		}

        public static List<Message> SearchRemovecorrespondent(SearchFilters filters, Infrastructure.ISortAndPage sortAndPage)
        {
            var context = new RootDataObject();
            var res = (from m in context.DataContext.Message orderby m.Message_id descending select m) as IQueryable<DAL.Message>;
            if (filters != null)
            {
                if (filters.mailboxId != null) res = res.Where(m => m.Mailbox_id == filters.mailboxId);
                if (filters.status != null) res = res.Where(m => m.MessageStatus_id == filters.status);
                if (filters.deleted.HasValue) res = res.Where(m => m.IsDeleted == filters.deleted);

                if (filters.Date.From.HasValue) res = res.Where(m => m.MessagsDate >= filters.Date.From);
                if (filters.Date.To.HasValue) res = res.Where(m => m.MessagsDate <= filters.Date.To);

                if (filters.threadID != null) res = res.Where(m => m.ThreadID == filters.threadID);
                if (filters.MerchantId != null) res = res.Where(m => m.Merchant_id == filters.MerchantId);
                if (filters.user != null) res = res.Where(m => m.AdmincashUser == filters.user);
                if (filters.sent != null) res = res.Where(m => m.IsSent == filters.sent);

                if (!string.IsNullOrEmpty(filters.mailAddress)) res = res.Where(m => m.EmailFrom.Contains(filters.mailAddress) || m.EmailTo.Contains(filters.mailAddress));
                if (!string.IsNullOrEmpty(filters.Text)) res = res.Where(m => m.Subject.Contains(filters.Text) || m.BodyText.Contains(filters.Text));
            }
            /*
			if (filters.sortKey != null)
			{
				switch (filters.sortKey)
				{
					case "Subject": res = sortDesc ? res.OrderByDescending(m => m.Subject) : res.OrderBy(m => m.Subject); break;
					case "EmailFrom": res = sortDesc ? res.OrderByDescending(m => m.EmailFrom) : res.OrderBy(m => m.EmailFrom); break;
					case "MessageStatus": res = sortDesc ? res.OrderByDescending(m => m.MessageStatus_id) : res.OrderBy(m => m.MessageStatus_id); break;
					case "":
					case "MessageDate": res = sortDesc ? res.OrderByDescending(m => m.MessagsDate) : res.OrderBy(m => m.MessagsDate); break;
					default: throw new Exception(string.Format("Unknown sort key '{0}'", sortKey));
				}
			}
			*/
            return res.ApplySortAndPage(sortAndPage).AsEnumerable().Select(r => new Message(true) { _entity = r, _rootDataObject = context }).ToList();
        }

        public static List<Message> getThreadMessages(Guid threadID)
		{
			return Search(new SearchFilters() { threadID = threadID }, null).ToList();
		}

		public static List<Message> getMerchantMessages(int merchantId)
		{
			return Search(new SearchFilters() { MerchantId = merchantId }, null).ToList();
		}

		private List<LogStatus> _statusLog;
		public List<LogStatus> StatusLog
		{
			get {
					if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

			
				if (_statusLog != null) return _statusLog;
				_statusLog = LogStatus.Search(new LogStatus.SearchFilters() { MessageID = ID });
				return _statusLog;
			}
		}
	}
}
