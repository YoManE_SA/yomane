﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Emails
{
	internal class RootDataObject
	{
		public Infrastructure.Domain Domain { get { return Infrastructure.Domain.Current; } }
		public Infrastructure.Security.Login User { get { return Infrastructure.ObjectContext.Current.User; } }
		private DAL.EmailsEntities _dataContext;
		public DAL.EmailsEntities DataContext { get { if (_dataContext == null) _dataContext = new DAL.EmailsEntities(Domain.CRMConnectionString); return _dataContext; } }
	}

	public class MailBox
	{
		private DAL.Mailbox _entity = null;
		private RootDataObject _rootDataObject = null;

		public short ID { get { return _entity.Mailbox_id; } }
		public string EmailAddress { get { return _entity.EmailAddress; } set { _entity.EmailAddress = value; } }
		public string MailServer { get { return _entity.MailServer; } set { _entity.MailServer = value; } }
		public string UserName { get { return _entity.UserName; } set { _entity.UserName = value; } }
		public string Password { get { return _entity.Password; } set { _entity.Password = value; } }
		public bool IsBlocked { get { return _entity.IsBlocked; } set { _entity.IsBlocked = value; } }
		public bool IsAutoScan { get { return _entity.IsAutoScan; } set { _entity.IsAutoScan = value; } }
		public bool IsSendFrom { get { return _entity.IsSendFrom; } set { _entity.IsSendFrom = value; } }
        public bool IsSSL { get { return _entity.isSSL; } set { _entity.isSSL = value; } }
        public string Signature { get { return _entity.Signature; } set { _entity.Signature = value; } }
		public string MessageStoragePath { get { return _entity.MessageStoragePath; } set { _entity.MessageStoragePath = value; } }
                

		private MailBox(bool loaded)
		{
		}

		public MailBox()
		{
			_rootDataObject = new RootDataObject();
			_entity = new DAL.Mailbox();
		}

        public static List<MailBox> LoadAll()
        {
            var context = new RootDataObject();
            return (from mb in context.DataContext.Mailbox select mb).AsEnumerable().Select(mb => new MailBox(true) { _entity = mb, _rootDataObject = context }).ToList();
        }


        public static List<MailBox> Load()
		{
			var context = new RootDataObject();
			Infrastructure.ObjectContext.Current.IsUserOfType(Infrastructure.Security.UserRole.Admin);
			List<string> addlist;
			addlist = (from mb in Infrastructure.DataContext.Reader.AdminUserToMailboxs where mb.AdminUser_id == Infrastructure.Security.AdminUser.Current.ID orderby mb.isDefault descending select mb.Mailbox).ToList();
			return Load(addlist);
		}

		public static List<MailBox> Load(List<string> addresses)
		{
			var context = new RootDataObject();
			return (from mb in context.DataContext.Mailbox where addresses.Contains(mb.EmailAddress) select mb).AsEnumerable().Select(mb => new MailBox(true) { _entity = mb, _rootDataObject = context }).ToList();
		}

		public static MailBox Load(int id)
		{
            short shortId = (short)id;
			var context = new RootDataObject();
			return (from mb in context.DataContext.Mailbox where (mb.Mailbox_id == shortId) select mb).AsEnumerable().Select(mb => new MailBox(true) { _entity = mb, _rootDataObject = context }).FirstOrDefault();
		}

		public void Save()
		{
            if (ID == 0)
                _rootDataObject.DataContext.Mailbox.AddObject(_entity);
            _rootDataObject.DataContext.SaveChanges();
        }

        public void Delete()
        {
            if (_entity.Mailbox_id == 0) return;
            _rootDataObject.DataContext.Mailbox.DeleteObject(_entity);
            _rootDataObject.DataContext.SaveChanges();
        }
    }
}
