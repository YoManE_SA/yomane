﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Emails.DAL;

namespace Netpay.Emails
{
    public class MessageStatus
    {
        private DAL.MessageStatus _entity = null;

        private MessageStatus() { }
        //private MessageStatus(RootDataObject rootDataObject) { }


        public int ID { get { return _entity.MessageStatus_id; } }
        public Boolean IsDefault { get { return _entity.IsDefault; } set { _entity.IsDefault = value; } }
        public Boolean IsVisible { get { return _entity.IsVisible; } set { _entity.IsVisible = value; } }
        public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
        public string Description { get { return _entity.Description; } set { _entity.Description = value; } }
        public Byte Order { get { return _entity.Order; } set { _entity.Order = value; } }
        public string Color { get { return _entity.Color; } set { _entity.Color = value; } }

        public static MessageStatus Load(int id)
        {
            MessageStatus ret = null;
            if (!Cache.TryGetValue(id, out ret)) return null;
            return ret;
        }

        public static Dictionary<int, MessageStatus> Cache
        {
            get
            {
                var domain = Infrastructure.Domain.Current;
                return domain.GetCachData("Emails.MessageStatus", () =>
                {
                    using (var context = new EmailsEntities(domain.CRMConnectionString))
                        return new Infrastructure.Domain.CachData((from s in context.MessageStatus orderby s.Order select new MessageStatus() { _entity = s }).ToDictionary(s => s.ID), DateTime.MaxValue);

                }) as Dictionary<int, MessageStatus>;
            }
        }
    }
}
