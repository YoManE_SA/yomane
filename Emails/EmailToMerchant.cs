﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Emails.DAL;

namespace Netpay.Emails
{
    public class EmailToMerchant
    {
        private EmailToMerchant _entity = null;

        public int ID { get{ return _entity.ID; } }
		public string EmailAddress { get { return _entity.EmailAddress; } }
		public int? MerchantID { get { return _entity.MerchantID; } }


		public static List<string> GetEmailsForMerchant(string domainHost, int? merchantID)
		{
			var domain = Netpay.Infrastructure.Domain.Get(domainHost);
			var context = new DAL.EmailsEntities(domain.CRMConnectionString);
			return (from m in context.EmailToMerchant where m.Merchant_id.Value == merchantID.Value select m.EmailAddress).ToList();
		}

		public static Dictionary<int, string> GetMerchantsByMail(string domainHost, string emailAddress)
		{
			var domain = Netpay.Infrastructure.Domain.Get(domainHost);
			var context = new DAL.EmailsEntities(domain.CRMConnectionString);
			var cn = new Netpay.Dal.DataAccess.NetpayDataContext(Infrastructure.Domain.Get(domainHost).Sql1ConnectionString);
			var mids = (from em in context.EmailToMerchant where em.EmailAddress == emailAddress select em.Merchant_id).ToList();
			var bb = (from m in cn.tblCompanies where mids.Contains(m.ID) select new { m.ID, m.CompanyName }).ToDictionary(m => m.ID, m => m.CompanyName);
			return bb;
		}

		public static void RemoveEmailToMerchant(string domainHost, int merchantId, string emailAddress)
		{
			var domain = Netpay.Infrastructure.Domain.Get(domainHost);
			var context = new DAL.EmailsEntities(domain.CRMConnectionString);
			DAL.EmailToMerchant entity = (from e in context.EmailToMerchant where e.Merchant_id == merchantId && e.EmailAddress == emailAddress select e).FirstOrDefault();
			if (entity == null) return;
			context.EmailToMerchant.DeleteObject(entity);
			context.SaveChanges();
		}

		public static void AddEmailToMerchant(string domainHost, int merchantId, string emailAddress)
		{
			var domain = Netpay.Infrastructure.Domain.Get(domainHost);
			var context = new DAL.EmailsEntities(domain.CRMConnectionString);
			DAL.EmailToMerchant entity = (from e in context.EmailToMerchant where e.Merchant_id == merchantId && e.EmailAddress == emailAddress select e).FirstOrDefault();
			if (entity != null) return;
			entity = new DAL.EmailToMerchant();
			entity.Merchant_id = merchantId;
			entity.EmailAddress = emailAddress;
			context.AddToEmailToMerchant(entity);
			context.SaveChanges();
		}

		public static void setEmailsToMerchant(string domainHost, int merchantId, string[] emailAddress)
		{
			var domain = Netpay.Infrastructure.Domain.Get(domainHost);
			var context = new DAL.EmailsEntities(domain.CRMConnectionString);
			var list = (from e in context.EmailToMerchant where e.Merchant_id == merchantId select e.EmailAddress).ToList();
			foreach (var e in list)
				if (!emailAddress.Contains(e))
					RemoveEmailToMerchant(domainHost, merchantId, e);
			foreach (var e in emailAddress)
			{
				if (!list.Contains(e))
					AddEmailToMerchant(domainHost, merchantId, e);
			}
		}

		public static void GetMerchantActiveStatusCompanyName(string domianHost, int idMeschant, out int activeStatus, out string companyName)
		{
			Infrastructure.Domain domain = Infrastructure.Domain.Get(domianHost);
			Dal.DataAccess.NetpayDataContext dc = new Dal.DataAccess.NetpayDataContext(domain.Sql1ConnectionString);
			var mer = (from c in dc.tblCompanies where (c.ID == idMeschant) select new { c.ActiveStatus, c.CompanyName });
			activeStatus = mer.First().ActiveStatus;
			companyName = mer.First().CompanyName;
		}


    }
}
