﻿using System;
using System.Collections.Generic;
using System.Data.EntityClient;
using System.Linq;
using Netpay.Emails.Email;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Emails
{
    public static class Execute
    {
        public static void FetchAll()
        {
			foreach (var d in Netpay.Infrastructure.Domain.Domains.Values)
			{
				Domain.Current = d;
				if (string.IsNullOrEmpty(d.CRMConnectionString)) continue;
				ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;

				var context = new DAL.EmailsEntities(d.CRMConnectionString);
				var mailboxes = (from mb in context.Mailbox where mb.IsAutoScan select mb).ToList();
				foreach (var mb in mailboxes) {
					try {
						FetchPOPMessages(d, context, mb);
					} catch(Exception ex) {
						Netpay.Infrastructure.Logger.Log(ex, string.Format("domain:{0}, mailbox:{1}", d.Host, mb.UserName));
					}
				}
			}
        }

        private static System.Guid? getThreadIDByMessageID(DAL.EmailsEntities context, string messageId)
        {
            return (from m in context.Message where m.MessageMimeID == messageId select m.ThreadID).FirstOrDefault();
        }

		private static void FetchPOPMessages(Netpay.Infrastructure.Domain domain, DAL.EmailsEntities context, DAL.Mailbox mailBox)
        {
            var popClient = new PopClient();
			//popClient.Trace += (string TraceText) => System.Diagnostics.Debug.Print(string.Format("Pop Trace:{0}", TraceText));
			popClient.Warning += (string WarningText, string Response) => System.Diagnostics.Debug.Print(string.Format("Pop Warning:{0}, Response:{1}", WarningText, Response));
			popClient.PopServer = mailBox.MailServer;
			popClient.UseSSL = mailBox.isSSL;
			popClient.Port = popClient.UseSSL ? 995 : 110;
			popClient.Username = mailBox.UserName;
			popClient.Password = mailBox.Password;
            popClient.IsAutoReconnect = true;
            popClient.ReadTimeout = 60000;
            popClient.Connect();

			DAL.MessageStatus defaultMessageStatus = (from s in context.MessageStatus orderby s.IsDefault descending select s).Take(1).SingleOrDefault();
			if (!System.IO.Directory.Exists(mailBox.MessageStoragePath))
				System.IO.Directory.CreateDirectory(mailBox.MessageStoragePath);

            List<int> EmailIds;
            popClient.GetEmailIdList(out EmailIds);

            foreach (int item in EmailIds)
            {
				string emailData = string.Empty;
				string fileName = string.Empty;
				DAL.Message msg = null;
				try {
					if (!popClient.GetRawEmail(item, out emailData)) continue;
					if (emailData == null) continue;

					msg = new DAL.Message();
					Email.Message message = new Email.Message();
					message.Parse(emailData);

					DateTime dtValue;
					if (!DateTime.TryParse(message.Date, out dtValue)) dtValue = DateTime.Now;
					msg.MessagsDate = dtValue;

					msg.MessageMimeID = message.MessageID;
					if (string.IsNullOrEmpty(message.InReplyTo)) msg.ThreadID = Guid.NewGuid();
					else msg.ThreadID = getThreadIDByMessageID(context, message.InReplyTo);
					msg.Subject = message.Subject.Truncate(1000);
					msg.BodyText = message.HTMLText;
					msg.AttachmentCount = (byte) message.Attachments.Count;
					try { msg.EmailFrom = message.FromAddress.Address.Truncate(1000); } catch { msg.EmailFrom = message.From.Truncate(1000); }
					try { msg.EmailTo = message.ToCollection.ToString().Replace(',', ';').Truncate(1000); } catch { msg.EmailTo = message.To.Truncate(1000); }
					try { msg.CC = message.CcCollection.ToString().Replace(',', ';').Truncate(1000); } catch { msg.CC = message.Cc.Truncate(1000); }

					msg.MessageStatus_id = defaultMessageStatus.MessageStatus_id;
					msg.Mailbox_id = mailBox.Mailbox_id;
					msg.Merchant_id = null;

					var v = (from em in context.EmailToMerchant where (em.EmailAddress == msg.EmailFrom) select em).ToList();
					if (v.Count == 1) msg.Merchant_id = v.First().Merchant_id;

					var cxt = new DAL.EmailsEntities(domain.CRMConnectionString);
					cxt.AddToMessage(msg);
					cxt.SaveChanges();
					cxt.Dispose();
					cxt = null;

					fileName = Message.getMessageFileName(msg.Message_id, mailBox.MessageStoragePath);
					System.IO.File.WriteAllText(fileName, emailData, System.Text.Encoding.ASCII);
					if (!System.Diagnostics.Debugger.IsAttached)
						popClient.DeleteEmail(item);
                }
                catch (Exception ex)
				{
					if (emailData == null) emailData = "";
					Netpay.Infrastructure.Logger.Log(ex, string.Format("while save email from mailbox:{0}, fileName:{1} message with data:\r\n{2}", mailBox.UserName, fileName, emailData));
                }
            }
            popClient.Disconnect();
        }

		private static void SendMail(string from, string fromName, string to, string cc, string subject, string body, string inReplyTo, out string messageID, System.Collections.Generic.Dictionary<string, System.IO.Stream> attachments)
        {            
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.IsBodyHtml = true;
            msg.BodyEncoding = System.Text.Encoding.UTF8;
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            msg.HeadersEncoding = System.Text.Encoding.UTF8;
            msg.Priority = System.Net.Mail.MailPriority.Normal;

			msg.From = new System.Net.Mail.MailAddress(from, fromName, System.Text.Encoding.UTF8);
            msg.To.Add(to);
            if (!string.IsNullOrEmpty(cc)) msg.CC.Add(cc);
            msg.Subject = subject;
            msg.Body = body;

            messageID = string.Format("<{0}@{1}>", Guid.NewGuid().ToString().Replace("-", "$").Substring(5), msg.From.Host);
            msg.Headers.Add("Message-ID", messageID);
            if (inReplyTo != null) msg.Headers.Add("In-Reply-To", inReplyTo);
            //msg.Headers.Add("References", messageID);
			if (attachments != null) {
				foreach(var item in attachments)
					msg.Attachments.Add(new System.Net.Mail.Attachment(item.Value, item.Key));
			}
            Netpay.Infrastructure.Email.SmtpClient.Send(msg);
        }

		public static void SendMessage(string domainHost, short mailboxId, int? replyToMessageId, string fromEmailAddress, string formUserName, string to, string cc, string subject, string body, string userMessage, System.Collections.Generic.Dictionary<string, System.IO.Stream> attachments)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Message.SecuredObject, PermissionValue.Add);

            var domain = Netpay.Infrastructure.Domain.Get(domainHost);
			var context = new DAL.EmailsEntities(domain.CRMConnectionString);
            try
            {
                string thisMessageID, prevMessageId;
				System.Guid? threadId;
				if (replyToMessageId != null)
				{
					var sourceMessageInfo = (from m in context.Message where m.Message_id == replyToMessageId.Value select new { m.MessageMimeID, m.ThreadID, m.Mailbox_id }).FirstOrDefault();
					prevMessageId = sourceMessageInfo.MessageMimeID;
					threadId = sourceMessageInfo.ThreadID;
				} else {
					threadId = Guid.NewGuid();
					prevMessageId = null;
				}
				SendMail(fromEmailAddress, formUserName, to, cc, subject, body, prevMessageId, out thisMessageID, attachments);
				DAL.Message msg = new DAL.Message();
                msg.MessagsDate = DateTime.Now;
                msg.MessageMimeID = thisMessageID;
				msg.ThreadID = threadId;
                msg.AdmincashUser = formUserName;
				msg.EmailFrom = fromEmailAddress;
                msg.EmailTo = to;
                msg.CC = cc;
				msg.Mailbox_id = mailboxId;
                msg.Subject = subject;
                msg.BodyText = body;
                msg.MessageStatus_id = null;
                msg.IsSent = true;
                context.AddToMessage(msg);
                context.SaveChanges();
				if (replyToMessageId != null) context.Save_Log(replyToMessageId.Value, 1, 3, formUserName, userMessage);
            }
            catch (Exception ex)
            {
				throw new Exception("Failed to send a message " + subject + " from " + fromEmailAddress + " to " + to + "\r\n", ex);
            }
        }
    }
}
