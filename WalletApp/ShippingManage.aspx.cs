﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalletApp;

public partial class ShippingAddress : BasePage
{
    protected WalletApp.WalletService.WalletShippingAddress Item { get; set; }
    protected bool IsAdd { get; set; }
	protected override void OnLoad(EventArgs e)
	{
        IsAdd = Request.QueryString["Add"] == "1";
		base.OnLoad(e);
        if (!IsPostBack && IsAdd) New_Command(this, e);
    }

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);
        phItemsList.Visible = !IsAdd;
		if (phItemsList.Visible)
		{
			var allShippingAddress = WalletService.GetShippingAddresses();
			if (allShippingAddress.Length == 0) Response.Redirect("AddShippingManage.aspx");
            rptShippingShort.DataSource = allShippingAddress;
            rptShippingShort.DataBind();
            rptDetalisShipping.DataSource = allShippingAddress;
            rptDetalisShipping.DataBind();
		}
    }

	protected void Delete_Command(object sender, CommandEventArgs e)
	{
		int itemId;
		if (!int.TryParse(e.CommandArgument.ToString(), out itemId)) return;
		WalletService.DeleteShippingAddress(itemId);
	}


    protected void Edit_Command(object sender, CommandEventArgs e)
    {
		int itemId;
		if (!int.TryParse(e.CommandArgument.ToString(), out itemId)) return;
		Item = WalletService.GetShippingAddress(itemId);
		btnSave.CommandArgument = Item.ID.ToString();
		phForm.DataBind();
        phForm.Visible = true;
	}

    protected void New_Command(object sender, EventArgs e)
    {
        Item = new WalletApp.WalletService.WalletShippingAddress();
        phForm.DataBind();
        phForm.Visible = true;
	}

	protected void Save_Command(object sender, CommandEventArgs e)
	{
		if (!IsAdd) {
            int itemId;
            if (!int.TryParse(e.CommandArgument.ToString(), out itemId)) return;
            Item = WalletService.GetShippingAddress(itemId);
        }
        if (Item == null) Item = new WalletApp.WalletService.WalletShippingAddress();
        Item.Title = txtTitle.Text;
        Item.IsDefault = IsDefaultShipping.Checked;
        Item.Comment = txtComment.Text;
		baAddress.Address = Item; baAddress.Save();
		WalletService.SaveShippingAddress(Item);
        dvSuccess.Visible = true;
        if(IsAdd) Response.Redirect("ShippingManage.aspx", true);
        phForm.Visible = false;
    }
}
