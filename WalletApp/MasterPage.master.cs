﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using WalletApp;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
	protected BasePage SitePage { get { return Page as BasePage; } }
	protected void Page_Load(object sender, EventArgs e)
    {
		if (SitePage != null) {
			var info = SitePage.WalletService.GetCustomer();
			lblusername.Text = info.FirstName;
			lbllastname.Text = info.LastName;
			lbllastname.Text = info.LastName;
			lblMerchantopID.Text = info.CustomerNumber;
			lblMerchantID.Text = info.CustomerNumber;
		}
    }

    protected override void OnPreRender(EventArgs e)
	{
		Page.Header.Controls.Add(new LiteralControl((new AppIdentityRequest()).PageInclude));
        phNotify.Visible = lblNotifyCount.Visible = SitePage.WalletService.ShouldUpdateDetails;
        base.OnPreRender(e);
	}

   
}



