﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace WalletApp
{
	public class AppIdentityRequest : AppIdentityService.AppIdentity
	{
		private static string _pageInclude;
		public static Guid? ApplicationToken { get { return Guid.Parse("2e84b4cd-b115-43d9-8dcf-6cc2a37d0256"); } }
		public string PageInclude { get { return _pageInclude; } }

		public AppIdentityRequest()
		{
			lock (typeof(AppIdentityRequest))
			{
				if (PageInclude != null) return;
				_pageInclude = GetContent("PageInclude");
			}
		}

		protected override WebRequest GetWebRequest(Uri uri)
		{
			HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(uri);
			if (ApplicationToken != null)
				request.Headers.Add("applicationToken", HttpUtility.UrlEncode(ApplicationToken.Value.ToString()));
			return request;
		}



	}
}