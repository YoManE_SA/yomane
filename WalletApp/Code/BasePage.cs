﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalletApp
{
    public class BasePage : System.Web.UI.Page
    {
		private WalletRequest _walletService;
		public WalletRequest WalletService
		{
			get{
				if (_walletService == null) _walletService = new WalletRequest();
				return _walletService;
			}
		}
        protected override void OnInit(EventArgs e)
        {
            checkUser();
            //security: csrfn fix
            //if (WalletRequest.CredentialsToken == null) Session["EnsureSessionExist"] = ""; // DO NOT REMOVE - THIS ENSURE THAT SEESION SAVED BECOUSE IS NOT EMPTY
            ViewStateUserKey = Request.UserAgent + WalletRequest.CredentialsToken;
            base.OnInit(e);
        }

        protected virtual void checkUser()
        {
			if (!WalletService.IsLoggedIn(false))
                Response.Redirect("Login.aspx");
            
            //security: disable caching
            Response.AddHeader("Cache-Control", "No-Cache");
            Response.AddHeader("Pragma", "no-cache");
            Response.AddHeader("Expires", "-1");
        }


    }
}