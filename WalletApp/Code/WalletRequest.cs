﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace WalletApp
{
	public class WalletRequest : WalletService.Wallet
	{
		private static WalletService.WalletLists _lists;
		public WalletRequest()
		{
			lock(typeof(WalletRequest)) {
				if (_lists != null) return;
				_lists = base.GetLists();
			}
		}
		protected override WebRequest GetWebRequest(Uri uri)
		{
			HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(uri);
			if (!string.IsNullOrEmpty(CredentialsToken))
				request.Headers.Add("Cookie", string.Format("credentialsToken={0}; HttpOnly", HttpUtility.UrlEncode(CredentialsToken)));
			return request;
		}

		protected override WebResponse GetWebResponse(WebRequest request)
		{
			try{
				return request.GetResponse();
			} catch(WebException e) {
				if ((e.Response as HttpWebResponse).StatusCode == HttpStatusCode.Forbidden)
					System.Web.HttpContext.Current.Response.Redirect("~/Login.aspx", true);
				throw e;
			}
		}

        public new WalletService.WalletLists GetLists() { return _lists; }

		public new WalletService.WalletUserLists GetCustomerLists() 
		{ 
			if(CustomerLists == null) CustomerLists = base.GetCustomerLists();
			return CustomerLists; 
		}

		public WalletService.WalletCustomer GetCustomer(bool forceRefresh)
		{
			if(Customer == null || forceRefresh)
				Customer = base.GetCustomer();
			return Customer;
		}

		public new WalletService.WalletResult SaveCustomer(WalletService.WalletCustomer info)
		{
			var ret = base.SaveCustomer(info);
			if (ret.IsSuccess) Customer = info;
			return ret;
		}

		public bool ShouldUpdateDetails
		{
			get{
				var cst = GetCustomer(false);
				return string.IsNullOrEmpty(cst.AddressLine1) || string.IsNullOrEmpty(cst.City) || string.IsNullOrEmpty(cst.PostalCode) || string.IsNullOrEmpty(cst.CountryIso) ||
					string.IsNullOrEmpty(cst.PhoneNumber) || string.IsNullOrEmpty(cst.EmailAddress) || string.IsNullOrEmpty(cst.FirstName)|| string.IsNullOrEmpty(cst.LastName);
			}
		}

		public WalletService.WalletLoginResult Login(string email, string password)
		{
            var ret = base.Login(email, password, new WalletService.WalletLoginOptions() { appName = "Wallet Web App", applicationToken = AppIdentityRequest.ApplicationToken.ToString(), setCookie = false });
			CredentialsToken = ret.IsSuccess ? ret.CredentialsToken : null;
			Customer = null; CustomerLists = null;
			//IsFirstLogin = ret.IsFirstLogin;
			return ret;
		}

		public bool IsLoggedIn(bool serverCheck)
		{
			if (WalletRequest.CredentialsToken == null) return false;
			if (!serverCheck) return true;
			return KeepAlive();
		}

        public void LoggOff() 
        {
            if (!IsLoggedIn(false)) return;
            try { base.LogOff(); } catch { }
            WalletRequest.CredentialsToken = null;
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
        }

		public static string CredentialsToken
		{
			get {
                var cookie = HttpContext.Current.Request.Cookies["credentialsToken"];
                if (cookie == null) return null;
				return cookie.Value;
			} 
			set {
                var cookie = HttpContext.Current.Response.Cookies["credentialsToken"];
                if (cookie == null)
                    HttpContext.Current.Response.SetCookie(cookie = new HttpCookie("credentialsToken") { HttpOnly = true, Secure = HttpContext.Current.Request.IsSecureConnection });
                cookie.Value = value;
            }
		}

		public WalletService.WalletCustomer Customer
		{
			get { return HttpContext.Current.Session["Customer"] as WalletService.WalletCustomer; }
			set { HttpContext.Current.Session["Customer"] = value; }
		}

		public WalletService.WalletUserLists CustomerLists
		{
			get { return HttpContext.Current.Session["CustomerLists"] as WalletService.WalletUserLists; }
			set { HttpContext.Current.Session["CustomerLists"] = value; }
		}

		public byte[] CookieProfileImage
		{
			get { return HttpContext.Current.Session["CookieProfileImage"] as byte[]; }
			set { HttpContext.Current.Session["CookieProfileImage"] = value; }
		}


		public string GetCountryName(string countryIso)
		{
			return (from c in GetLists().Countries where c.Key == countryIso select c.Name).FirstOrDefault();
		}

		public string GetStateName(string countryIso, string stateIso)
		{
			return (from c in GetLists().UsaStates where c.Key == stateIso select c.Name).FirstOrDefault();
		}

		public string GetPaymentMethodIcon(string paymentMethodKey)
		{
			return (from c in GetCustomerLists().PaymentMethods where c.Key == paymentMethodKey select c.Icon).FirstOrDefault();
		}

	}

	public class ProfileImageHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
	{
		public bool IsReusable { get { return true; } }
		public void ProcessRequest(HttpContext context)
		{
			bool bHasImage = false;
			var wr = new WalletRequest();
			if (wr.IsLoggedIn(false)){
				var cst = wr.GetCustomer(false);
				bHasImage = (cst.ProfileImage != null);
				if (bHasImage) context.Response.BinaryWrite(cst.ProfileImage);
			} else {
				bHasImage = (wr.CookieProfileImage != null);
				if (bHasImage) context.Response.BinaryWrite(wr.CookieProfileImage);
			}
			if (!bHasImage) {
				string imagePath = context.Server.MapPath("~/assets/img/profiles/avatar.jpg");
				if (System.IO.File.Exists(imagePath)) context.Response.BinaryWrite(System.IO.File.ReadAllBytes(imagePath));
			}
		}
	}
}