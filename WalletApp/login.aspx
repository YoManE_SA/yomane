﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="login" CodeBehind="login.aspx.cs" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Login - YoManE Wallet</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->

    <!-- BEGIN CSS TEMPLATE -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/custom-icon-set.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicons/favicon.ico?1=1" type="image/x-icon" />

    <!-- END CSS TEMPLATE -->
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        document.createElement('header');
        document.createElement('footer');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('nav');
    </script>

    <!-- BEGIN FAVICO -->
    <link rel="apple-touch-icon" sizes="57x57" href="favicons/YoMaNe_logo.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="favicons/YoMaNe_logo.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="favicons/YoMaNe_logo.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="favicons/YoMaNe_logo.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="favicons/YoMaNe_logo.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="favicons/YoMaNe_logo.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="favicons/YoMaNe_logo.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="favicons/YoMaNe_logo.png" />
    <link rel="icon" type="image/png" href="favicons/YoMaNe_logo.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="favicons/YoMaNe_logo.png" sizes="160x160" />
    <link rel="icon" type="image/png" href="favicons/YoMaNe_logo.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="favicons/YoMaNe_logo.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="favicons/YoMaNe_logo.png" sizes="32x32" />
    <meta name="msapplication-TileColor" content="#da532c" />
    <meta name="msapplication-TileImage" content="/YoMaNe_logo.png" />
    <!-- END FAVICO -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="error-body no-top login-background-image-top">
    <form runat="server">
        <asp:ScriptManager runat="server" />
        <div class="container wrapper">
            <asp:Panel ID="LoginFormCache" runat="server">
                <div class="col-md-12">
                    <div class="col-md-7 col-md-offset-3">
                        <div class="grey-login login-container">
                            <div class="grid simple transparent">
                                <div class="grid-title">
                                    <div class="logo-app pull-left"></div>
                                    <div class="pull-left">
                                        <h2>Sign in to YoManE Wallet</h2>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-body no-border">
                                    <p class="margin-top-10">
                                        As a YoManE Wallet user you can use your online account to view your transactions and edit your account details. 
                                        NEW ! Have the same "Around U" experience from your mobile wallet online. Don't have an account yet?

                                        <asp:LinkButton ID="BtnSignUp" OnClick="BtnSignUp_Click" CausesValidation="false" runat="server" Text="sign up" /><strong></strong>
                                    </p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:Label ID="ltResult" ClientIDMode="Static" runat="server" CssClass="block  alert alert-error" Visible="false" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 pull-left">
                                            <img src="profileImage.ashx" class="profile-login-image" />
                                        </div>
                                        <div class="col-md-6  pull-left">
                                            Are you <strong>
                                                <asp:Literal runat="server" ID="ltUserName" /></strong>
                                            <div class="input-with-icon margin-top-10">
                                                <i class="icon-lock"></i>
                                                <asp:TextBox runat="server" type="password" ID="txtPasswordCache" placeholder="Your Password" class="form-control" />
                                                <asp:RequiredFieldValidator runat="server" CssClass="error-login-screen" Display="Dynamic" ControlToValidate="txtPasswordCache" ErrorMessage="This field is required." />
                                            </div>
                                            <div class="margin-top-10">
                                                <asp:Button runat="server" ClientIDMode="Static" ID="btnLoginCache" type="submit" OnClick="Login_Click" class="btn btn-dark" Text="Login" />
                                                <asp:LinkButton ID="lnkLoginForm" CausesValidation="false" OnClick="lnkLoginForm_Click" runat="server">Not you? &#8594;</asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-md-offset-2 pull-left"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                    </div>
                                    <div class="form-actions">
                                        <div class="pull-left margin-top-10 login-link-footer-size">
                                            <a href="Default.aspx">Homepage</a> | 
                                        <a href="https://merchants.yomane.com/">Merchant control panel</a>

                                        </div>
                                        <div class="pull-right">
                                            <img src="assets/img/logo/pci.png" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-offset-2"></div>
                </div>
                <div class="col-md-7 col-md-offset-3">
                    <div class="grid-body no-border">
                        <div class="margin-20 center-text">
                            <a href="https://play.google.com/store/apps/details?id=com.yomanewallet" target="_blank">
                                <img src="assets/img/logo/GooglePlay.png" /></a>
                            <a href="https://itunes.apple.com/us/app/mahala-wallet/id664557320?mt=8" target="_blank">
                                <img src="assets/img/logo/Appstore.png" /></a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </asp:Panel>
            <asp:Panel ID="LoginFormClear" runat="server" Visible="false">
                <div class="col-md-12">
                    <div class="col-md-7 col-md-offset-3">
                        <div class="grey-login login-container">
                            <div class="grid simple transparent">
                                <div class="grid-title">
                                    <div class="logo-app pull-left"></div>
                                    <div class="pull-left">
                                        <h2>Sign in</h2>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="grid-body no-border">
                                    <p class="margin-top-10">
                                        As a YoManE Wallet user you can use your online account to view your transactions and edit your account details. NEW ! Have the same "Around U" experience from your mobile wallet ONLINE.
										Don't have an account yet? 
                                        <asp:LinkButton ID="BtnSignUpClear" OnClick="BtnSignUpClear_Click" CausesValidation="false" runat="server" Text="Sign up" />
                                    </p>
                                    <div id="login-form" class="login-form">
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'user@email.com' " data-placement="right">Email</label>
                                                <div class="controls">
                                                    <div class="input-with-icon right">
                                                        <i class="icon-user"></i>
                                                        <asp:TextBox runat="server" type="text" ID="txtEmail" class="form-control" />
                                                        <asp:RegularExpressionValidator runat="server" CssClass="error-login-screen" ValidationGroup="LoginFormValidation" Display="Dynamic" ControlToValidate="txtEmail" ErrorMessage="Email not valid." ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                        <asp:RequiredFieldValidator runat="server" CssClass="error-login-screen" ValidationGroup="LoginFormValidation" Display="Dynamic" ControlToValidate="txtEmail" ErrorMessage="This field is required." />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">

                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'aabbcc11' " data-placement="right">Password</label>
                                                <span class="help"></span>
                                                <div class="controls">
                                                    <div class="input-with-icon  right">
                                                        <i class="icon-envelope"></i>
                                                        <asp:TextBox runat="server" type="password" ID="txtPassword" class="form-control" />
                                                        <asp:RequiredFieldValidator runat="server" CssClass="error-login-screen" Display="Dynamic" ValidationGroup="LoginFormValidation" ControlToValidate="txtPassword" ErrorMessage="This field is required." />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="control-group col-md-10">
                                                <div class="pull-left margin-bottom-20">
                                                    <div class="checkbox checkbox check-success">
                                                        <asp:LinkButton runat="server" Text="Forgot your password?" OnClick="SendPassword_Click" />&nbsp;&nbsp;
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <asp:Button runat="server" ClientIDMode="Static" ID="btnLogin" type="submit" OnClick="Login_Click" class="btn btn-dark" Text="Login" ValidationGroup="LoginFormValidation" />
                                                or
												<asp:Button runat="server" ID="btnCreatAccount" OnClick="btnCreatAccount_Click" ClientIDMode="Static" type="submit" class="btn btn-default" CausesValidation="false" Text="Create an account" />
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-md-12">
                                                <asp:Label ID="ltResultLogin" ClientIDMode="Static" runat="server" CssClass="block  alert alert-error" Visible="false" />
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <div class="pull-left margin-top-10 login-link-footer-size">
                                                <a href="Default.aspx">Homepage</a> | 
                                        <a href="https://merchants.yomane.com/">Merchant control panel</a>

                                            </div>
                                            <div class="pull-right">

                                                <img src="assets/img/logo/pci.png" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-3">
                    <div class="grid-body no-border">
                        <div class="margin-20 center-text">
                            <a href="https://play.google.com/store/apps/details?id=com.yomanewallet&hl=en" target="_blank">
                                <img src="assets/img/logo/GooglePlay.png" /></a>
                            <a href="https://itunes.apple.com/us/app/mahala-wallet/id664557320?mt=8" target="_blank">
                                <img src="assets/img/logo/Appstore.png" /></a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </asp:Panel>
            <asp:Panel ID="RegisterStepsForm" runat="server">
                <div class="col-md-12">
                    <div class="col-md-7 col-md-offset-3">
                        <div class="grey-login login-container">
                            <div class="grid simple transparent">
                                <div class="grid-title">
                                    <h2>3 Steps  <span class="semi-bold">and you're done</span></h2>
                                </div>
                                <div class="grid-body no-border">
                                    <br />
                                    <br />

                                    <div class="row column-seperation">
                                        <div id="commentForm">
                                            <div id="rootwizard" class="col-md-12">
                                                <div class="form-wizard-steps">
                                                    <ul class="wizard-steps">
                                                        <li class="active" data-target="#step1"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">Basic information</span> </a></li>
                                                        <li data-target="#step2" class=""><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">Choose your password</span></a></li>
                                                        <li data-target="#step3" class=""><a href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">Choose your pin code</span> </a></li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="tab-content transparent">
                                                    <div class="tab-pane active" id="tab1">
                                                        <br>
                                                        <h4 class="semi-bold">Step 1 - <span class="light">Basic Information</span></h4>
                                                        <br>
                                                        <div runat="server" id="dvRegisterError" class="alert alert-error" visible="false"></div>
                                                        <div class="row form-row">
                                                            <div class="form-group col-md-12">
                                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Joe Doe' " data-placement="right">Full Name</label>
                                                                <div class="controls">
                                                                    <div class="input-with-icon right">
                                                                        <i class="icon-user"></i>
                                                                        <asp:TextBox runat="server" type="text" ID="txtFullName" class="form-control" />
                                                                        <asp:RequiredFieldValidator runat="server" CssClass="error-login-screen" Display="Dynamic" ControlToValidate="txtFullName" ValidationGroup="RegisterValidation1" ErrorMessage="This field is required." />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row form-row">
                                                            <div class="form-group col-md-12">
                                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'user@mail.com' " data-placement="right">Email</label>
                                                                <span class="help"></span>
                                                                <div class="controls">
                                                                    <div class="input-with-icon  right">
                                                                        <i class="icon-envelope"></i>
                                                                        <asp:TextBox runat="server" type="text" ID="txtEmailAddress" class="form-control" />
                                                                        <asp:RegularExpressionValidator runat="server" CssClass="error-login-screen" Display="Dynamic" ControlToValidate="txtEmailAddress" ErrorMessage="Email not valid." ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="RegisterValidation1" />
                                                                        <asp:RequiredFieldValidator runat="server" CssClass="error-login-screen" Display="Dynamic" ControlToValidate="txtEmailAddress" ErrorMessage="This field is required." ValidationGroup="RegisterValidation1" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row form-row">
                                                            <div class="form-group col-md-12">
                                                                <div class="pull-right">
                                                                    <input type="button" class="btn btn-dark" value="Next" onclick="registerWizard_Step(2)" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab2">
                                                        <br>
                                                        <h4 class="semi-bold">Step 2 - <span class="light">Choose your password</span></h4>
                                                        <br>
                                                        <div class="alert alert-info login-link-size">
                                                            <button class="close" data-dismiss="alert"></button>
                                                            The password length must be at least 8 characters<br />
                                                            The password must contain only Latin letters (A-Z) and decimal digits (0-9)<br />
                                                            The password must containt at least 2 decimal digits (0-9)<br />
                                                            The password must contain at least 2 Latin letters (A-Z)
                                                        </div>
                                                        <div class="row form-row">
                                                            <div class="form-group col-md-12">
                                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'aabbcc11' " data-placement="right">Password</label>

                                                                <span class="help"></span>
                                                                <div class="controls">
                                                                    <div class="input-with-icon  right">
                                                                        <i class="icon-lock"></i>
                                                                        <asp:TextBox runat="server" type="password" ID="txtNewPassword" class="form-control" />
                                                                        <asp:RequiredFieldValidator runat="server" CssClass="error-login-screen" Display="Dynamic" ControlToValidate="txtNewPassword" ErrorMessage="This field is required." ValidationGroup="RegisterValidation2" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row form-row">
                                                            <div class="form-group col-md-12">
                                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Retype your password' " data-placement="right">Confirm Password</label>
                                                                <span class="help"></span>
                                                                <div class="controls">
                                                                    <div class="input-with-icon  right">
                                                                        <i class="icon-lock"></i>
                                                                        <asp:TextBox runat="server" type="password" ID="txtConfirmNewPassword" class="form-control" />
                                                                        <asp:RequiredFieldValidator runat="server" CssClass="error-login-screen" Display="Dynamic" ControlToValidate="txtConfirmNewPassword" ErrorMessage="This field is required." ValidationGroup="RegisterValidation2" />
                                                                        <asp:CompareValidator runat="server" CssClass="error-login-screen" ControlToValidate="txtConfirmNewPassword" ControlToCompare="txtNewPassword" Display="Dynamic" ErrorMessage="Password was different" ValidationGroup="RegisterValidation2" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row form-row">
                                                            <div class="form-group col-md-12">
                                                                <div class="pull-left">
                                                                    <input type="button" class="btn btn-default" value="Back" onclick="registerWizard_Step(1)" />
                                                                </div>
                                                                <div class="pull-right">
                                                                    <input type="button" class="btn btn-dark" value="Next" onclick="registerWizard_Step(3)" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab3">
                                                        <br>
                                                        <h4 class="semi-bold">Step 3 - <span class="light">Choose your pin code</span></h4>
                                                        <br>
                                                        <div class="alert alert-info login-link-size">
                                                            <button class="close" data-dismiss="alert"></button>
                                                            The pincode length must be at least 4 numbers
                                                        </div>
                                                        <div class="row form-row">
                                                            <div class="form-group col-md-12">
                                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'must to be 4 numbers' " data-placement="right">Pin Code</label>

                                                                <span class="help"></span>
                                                                <div class="controls">
                                                                    <div class="input-with-icon  right">
                                                                        <i class="icon-lock"></i>
                                                                        <asp:TextBox runat="server" type="password" ID="txtNewPinCode" class="form-control" MaxLength="4" />
                                                                        <asp:RequiredFieldValidator runat="server" CssClass="error-login-screen" Display="Dynamic" ControlToValidate="txtNewPinCode" ValidationGroup="RegisterValidation3" ErrorMessage="This field is required." />
                                                                        <asp:RegularExpressionValidator runat="server" CssClass="error-login-screen" ControlToValidate="txtNewPinCode" Display="Dynamic" ErrorMessage="Please Enter Only Numbers" ValidationExpression="^\d+$" ValidationGroup="RegisterValidation3" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row form-row">
                                                            <div class="form-group col-md-12">
                                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Retype your pin code' " data-placement="right">Retype Pin Code</label>

                                                                <span class="help"></span>
                                                                <div class="controls">
                                                                    <div class="input-with-icon  right">
                                                                        <i class="icon-lock"></i>
                                                                        <asp:TextBox runat="server" type="password" ID="txtConfirmNewPinCode" MaxLength="4" class="form-control" />
                                                                        <asp:RequiredFieldValidator runat="server" CssClass="error-login-screen" Display="Dynamic" ControlToValidate="txtConfirmNewPinCode" ErrorMessage="This field is required." ValidationGroup="RegisterValidation3" />
                                                                        <asp:CompareValidator runat="server" CssClass="error-login-screen" ControlToValidate="txtConfirmNewPinCode" ControlToCompare="txtNewPinCode" Display="Dynamic" ErrorMessage="You have entered a differnet pin code " ValidationGroup="RegisterValidation3" />
                                                                        <asp:RegularExpressionValidator runat="server" CssClass="error-login-screen" ControlToValidate="txtConfirmNewPinCode" Display="Dynamic" ErrorMessage="Please Enter Only Numbers" ValidationExpression="^\d+$" ValidationGroup="RegisterValidation3" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row form-row">
                                                            <div class="form-group col-md-12">
                                                                <div class="pull-left">
                                                                    <input type="button" class="btn btn-default" value="Back" onclick="registerWizard_Step(2)" />
                                                                </div>
                                                                <div class="pull-right">
                                                                    <asp:Button runat="server" CssClass="btn btn-dark" Text="Finish" OnClick="Register_Click" OnClientClick="return registerWizard_Step(4)" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="pull-left margin-top-10">
                                            <asp:LinkButton runat="server" ID="lnkBackLogin" OnClick="lnkBackLogin_Click">&#8592; back to login screen </asp:LinkButton>
                                        </div>
                                        <div class="pull-right">

                                            <img src="assets/img/logo/pci.png" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-3">
                    <div class="grid-body no-border">
                        <div class="margin-20 center-text">
                            <a href="https://https://play.google.com/store/apps/details?id=com.yomanewallet&hl=en" target="_blank">
                                <img src="assets/img/logo/GooglePlay.png" /></a>
                            <a href="https://itunes.apple.com/us/app/mahala-wallet/id664557320?mt=8" target="_blank">
                                <img src="assets/img/logo/Appstore.png" /></a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#rootwizard').bootstrapWizard({ 'tabClass': 'form-wizard' });
                    });
                    function registerWizard_Step(toIndex) {
                        toIndex--;
                        if (!Page_ClientValidate('RegisterValidation' + toIndex)) return false;
                        if (toIndex < 3) $('#rootwizard').bootstrapWizard('show', toIndex);
                        return true;
                    }
                </script>
            </asp:Panel>
            <div class="push"></div>
        </div>

        <div class="footer footer-background">
            <div class="container">
                <div class="col-md-12">
                    <div class="col-md-7 col-md-offset-3">
                        <div class="grey-login login-container">
                            <div class="grid simple transparent">
                                <div class="pull-left">
                                    &copy; YoManE 
                                </div>
                                <div class="pull-right">
                                    <a href="http://www.yomane.com/">
                                        <img src="assets/img/logo/logo-application.png" height="50" class="v-align-middle" /></a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-offset-2"></div>
                </div>


            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN CORE JS FRAMEWORK-->
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
        <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
        <script src="assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
        <script type="text/javascript">
                    $(document).ready(function () {
                        $("img").unveil();
                    });
        </script>

        <script src="assets/js/messages_notifications.js" type="text/javascript"></script>
    </form>
</body>
</html>
