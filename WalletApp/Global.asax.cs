﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using WalletApp;

namespace WalletApp
{
	public class Global : HttpApplication
	{
		private void Application_Start(object sender, EventArgs e)
		{
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
			//BundleConfig.RegisterBundles(BundleTable.Bundles);
			//AuthConfig.RegisterOpenAuth();
		}

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			var ex = e.ExceptionObject as Exception;
			(new AppIdentityRequest()).Log(1, ex.Message, ex.StackTrace);
		}

		private void Application_Error(object sender, EventArgs e)
		{
			Exception ex = Server.GetLastError();
			(new AppIdentityRequest()).Log(1, ex.Message, ex.StackTrace);
		}
        private void Application_BeginRequest(object sender, EventArgs e)
        {
            if ((System.Configuration.ConfigurationManager.AppSettings["ForceSSL"] == "true") && (Context.Request.Url.Scheme.ToLower() != "https")) { 
                Response.Redirect("https" + Context.Request.Url.ToString().Remove(0, Context.Request.Url.Scheme.Length), true);
            }

            var application = sender as HttpApplication;
            if (application != null && application.Context != null)
            {
                application.Context.Response.Headers.Remove("Server");
            }
        }

		private void Application_End(object sender, EventArgs e)
		{
		}
	}
}
