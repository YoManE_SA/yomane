﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalletApp;

public partial class login : System.Web.UI.Page
{
	private WalletRequest _walletService;
	private const string CookieName = "WalletAppLogin";

	public bool HasCookie { get; set; }

	public WalletRequest WalletService
	{
		get {
			if (_walletService == null) _walletService = new WalletRequest();
			return _walletService;
		}
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		if(!IsPostBack)
        {
            WalletService.LoggOff();
			var cookie = Request.Cookies["WalletAppLogin"];
			if (cookie != null) {
				var cookieInfo = WalletService.DecodeLoginCookie(AppIdentityRequest.ApplicationToken.ToString(), cookie.Value);
				if (cookieInfo != null) {
					ltUserName.Text = cookieInfo.FullName;
					txtEmail.Text = cookieInfo.Email;
					WalletService.CookieProfileImage = cookieInfo.ProfilePicture;
					HasCookie = true;
				}
			}

			LoginFormCache.Visible = HasCookie;
			LoginFormClear.Visible = !HasCookie;
			RegisterStepsForm.Visible = false;
		}
    }

	protected void SendPassword_Click(object sender, EventArgs e)
    {
		var result = WalletService.ResetPassword(/*AppIdentityRequest.ApplicationToken.ToString(), */txtEmail.Text);
		if (!result) ltResultLogin.Visible = true;
	}

	protected void Login_Click(object sender, EventArgs e)
    {
		var password = (sender == btnLoginCache) ? txtPasswordCache.Text : txtPassword.Text;
		var result = WalletService.Login(txtEmail.Text, password);
		if (result.IsSuccess) {
			if (result.EncodedCookie != null){
				var ck = new System.Web.HttpCookie("WalletAppLogin", result.EncodedCookie);
				ck.Expires = DateTime.Now.AddMonths(3);
				Response.Cookies.Add(ck);
			}
            Response.Redirect("Default.aspx");
		} else {
			ltResultLogin.Text = result.Message; ltResultLogin.Visible = true;
			ltResult.Text = result.Message; ltResult.Visible = true;
		}
    }

    protected void lnkLoginForm_Click(object sender, EventArgs e)
    {
        LoginFormClear.Visible = true;
        LoginFormCache.Visible = false;
    }

    protected void btnCreatAccount_Click(object sender, EventArgs e)
    {
        LoginFormClear.Visible = false;
        RegisterStepsForm.Visible = true;
    }

    protected void BtnSignUp_Click(object sender, EventArgs e)
    {
        LoginFormCache.Visible = false;
        RegisterStepsForm.Visible = true;
    }

    protected void BtnSignUpClear_Click(object sender, EventArgs e)
    {
        LoginFormClear.Visible = false;
        RegisterStepsForm.Visible = true;
    }

    protected void lnkBackLogin_Click(object sender, EventArgs e)
    {
        RegisterStepsForm.Visible = false;
        LoginFormClear.Visible = true;
    }

	protected void Register_Click(object sender, EventArgs e)
	{
		var cst = new WalletApp.WalletService.WalletCustomer();
		var names = txtFullName.Text.Split(' ');
		if (names.Length > 0) cst.FirstName = names[0];
		if (names.Length > 1) cst.LastName = names[1];
		cst.EmailAddress = txtEmailAddress.Text;
		var regData = new WalletApp.WalletService.WalletRegisterData();
		regData.info = cst;
		regData.Password = txtNewPassword.Text;
		regData.PinCode = txtNewPinCode.Text;
		regData.ApplicationToken = AppIdentityRequest.ApplicationToken.ToString();
		var result = WalletService.RegisterCustomer(regData);
		if (!result.IsSuccess) {
			dvRegisterError.InnerText = result.Message;
			dvRegisterError.Visible = true;
		} else {
			WalletService.Login(txtEmailAddress.Text, txtNewPassword.Text);
			Response.Redirect("~/Default.aspx");
		}
	}
  
}