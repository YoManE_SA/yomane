﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" Inherits="Timeline" CodeBehind="Timeline.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- BEGIN PLUGIN CSS -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-nestable/jquery.nestable.css" rel="stylesheet" type="text/css" media="screen" />
    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/large_screen_tweak.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadcrumb" runat="Server">
    <ul class="breadcrumb">
        <li>
            <p>YOU ARE HERE</p>
        </li>
        <li><a href="#" class="active">Product List</a></li>
    </ul>
</asp:Content>



<asp:Content ID="Content3" ContentPlaceHolderID="HeadPage" runat="Server">
    <i class="icon-custom-left"></i>
    <h3>Product - <span class="semi-bold">List</span></h3>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h4>Search <span class="semi-bold">Products</span></h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove"></a></div>

                </div>
                <div class="grid-body no-border">
                    <asp:TextBox ID="SearchTextBox" runat="server" type="text" placeholder="search" OnKeyDown="//if(event.charCode == 13) document.getElementById('btnSearch').click();" />
                    <asp:LinkButton ID="btnSearch" ClientIDMode="Static" runat="server" CssClass="btn btn-primary" OnClick="Search_Click" CausesValidation="false"><i class="icon-search"></i></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content">
        <!-- BEGIN MODEL-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>Widget Settings</h3>
            </div>
            <div class="modal-body">Widget settings form goes here </div>
        </div>
        <!-- END MODEL-->
        <div class="clearfix"></div>
        <div class="content">
            <div class="row">
                <div class="col-md-10 col-vlg-7">
                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" RenderMode="Block" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ul class="cbp_tmtimeline">
                                <asp:Repeater Visible="false" runat="server" ID="rptProducts">
                                    <ItemTemplate>
                                        <li>
                                            <time class="cbp_tmtime" datetime="2013-04-10 18:30">
                                                <span class="date"><%# Eval("Price", "{0:0.00}") %> </span>
                                                <span class="time"><span class="animate-number" data-value="12" data-animation-duration="600"></span>:<span class="animate-number" data-value="45" data-animation-duration="600"></span> <span class="semi-bold"><%# Eval("Currency") %> </span></span>
                                            </time>
                                            <div class="cbp_tmicon animated bounceIn">
                                                <div class="user-profile">
                                                    <img src="assets/img/profiles/d.jpg" data-src="assets/img/profiles/d.jpg" data-src-retina="assets/img/profiles/d2x.jpg" width="35" height="35" alt="">
                                                </div>
                                            </div>
                                            <div class="cbp_tmlabel">
                                                <div class="p-t-10 p-l-30 p-r-20 p-b-20 xs-p-r-10 xs-p-l-10 xs-p-t-5">
                                                    <h4 class="inline m-b-5"><span class="text-success semi-bold"><%# Eval("Name") %></span> <%# Eval("Price", "{0:0.00}") %> <%# Eval("Currency") %> </h4>
                                                    <h5 class="inline muted semi-bold m-b-5"><%# Eval("Name") %> </h5>
                                                    <div class="muted">Shared publicly - 12:45pm</div>
                                                    <p class="m-t-5 dark-text"><%# Eval("Description") %><a href="<%# Eval("ProductURL") %>" class="text-primary" style="text-overflow: ellipsis; width: 50px;"><%# Eval("ProductURL") %></a> ... </p>
                                                    <a href="<%# Eval("ProductURL") %>" class="muted">Read more</a>
                                                </div>
                                                <div class="m-l-10 m-r-10 xs-m-l-5 xs-m-r-5">
                                                      <img src="https://uiservices.yomane.com/GlobalData/globalAccess/Merchants/3561732/UIServices/PP_635297155207968750.jpg" style="width: 100%" alt=""> 
                                                   <%-- <img src="<%# Eval("ImageURL") %>" style="width: 100%" alt="">--%>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="xs-p-r-10 xs-p-l-10 p-l-30 p-r-20 p-b-10 p-t-20 row">
                                                    <div class="col-md-6">
                                                        <h5 class="inline m-r-10">205 comments</h5>
                                                        <h5 class="inline">21,586 People like this</h5>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <ul class="my-friends no-margin pull-right">
                                                            <li>
                                                                <div class="profile-pic">
                                                                    <img width="35" height="35" data-src-retina="assets/img/profiles/e2x.jpg" data-src="assets/img/profiles/e.jpg" src="assets/img/profiles/e.jpg" alt="">
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="profile-pic">
                                                                    <img width="35" height="35" data-src-retina="assets/img/profiles/b2x.jpg" data-src="assets/img/profiles/b.jpg" src="assets/img/profiles/b.jpg" alt="">
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="profile-pic">
                                                                    <img width="35" height="35" data-src-retina="assets/img/profiles/h2x.jpg" data-src="assets/img/profiles/h.jpg" src="assets/img/profiles/h.jpg" alt="">
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="tiles grey p-t-10 p-b-10 p-l-20">
                                                    <ul class="action-links">
                                                        <li>Like</li>
                                                        <li>Comment</li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
            </div>
            <!-- END PAGE -->
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Plugins" runat="Server">
    <!-- END CHAT -->
    <!-- END CONTAINER -->
    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/breakpoints.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
    <!-- END CORE JS FRAMEWORK -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-mixitup/jquery.mixitup.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="assets/js/search_results.js" type="text/javascript"></script>
    <!-- BEGIN CORE TEMPLATE JS -->
</asp:Content>
