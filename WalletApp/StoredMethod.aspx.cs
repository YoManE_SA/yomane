﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalletApp;

public partial class CreditCard : BasePage
{
    protected WalletApp.WalletService.WalletStoredPaymentMethod Item { get; set; }
	protected string AddPaymentMethodId { get { return Request.QueryString["AddPaymentMethod"]; } }
	protected override void OnLoad(EventArgs e)
	{
		bool isAdd = (AddPaymentMethodId != null);
		if (!IsPostBack && isAdd) New_Command(this, e);
		base.OnLoad(e);
	}

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);
		phItemsList.Visible = (AddPaymentMethodId == null);
		if (phItemsList.Visible)
		{
			var allCards = WalletService.GetStoredPaymentMethods();
			rptDetalisCard.DataSource = allCards;
			rptDetalisCard.DataBind();
			rptShortDetalisCard.DataSource = allCards;
			rptShortDetalisCard.DataBind();
			if (allCards.Length == 0) Response.Redirect("SelectPaymentType.aspx");
		}
    }

	protected void Delete_Command(object sender, CommandEventArgs e)
	{
		int itemId;
		if (!int.TryParse(e.CommandArgument.ToString(), out itemId)) return;
		WalletService.DeleteStoredPaymentMethod(itemId);
	}

	private DateTime parseExpDate(string value)
	{
		var val = value.Split(new char[] { '-', '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
		if (val.Length < 2) return new DateTime(1900, 1, 1);
		int year, month;
		if(!int.TryParse(val[0], out month) || !int.TryParse(val[1], out year))
			return new DateTime(1900, 1, 1);
		if ((month < 1 || month > 12) || (year < DateTime.Now.Year || year > DateTime.Now.Year + 15))
			return new DateTime(1900, 1, 1);
		return new DateTime(year, month, 1).AddMonths(1).AddDays(-1);
	}

	protected void SetMethodType(string pmKey)
	{
		var pmType = WalletService.GetCustomerLists().PaymentMethods.Where(pm => pm.Key == pmKey).SingleOrDefault();
		if (pmType == null) return;
		lblValue1.Text = pmType.Value1Caption;
		lblValue2.Text = pmType.Value2Caption;
		dvValue1.Visible =!string.IsNullOrEmpty(lblValue1.Text);
		dvValue2.Visible = !string.IsNullOrEmpty(lblValue2.Text);
		dvExpData.Visible = pmType.HasExpirationDate;
		hSubTitle.InnerText = string.Format("{0} Details", pmType.Name);
		spAddTitle.InnerText = pmType.Name;
	}

    protected void Edit_Command(object sender, CommandEventArgs e)
    {
		int itemId;
		if (!int.TryParse(e.CommandArgument.ToString(), out itemId)) return;
		Item = WalletService.GetStoredPaymentMethod(itemId);
		btnSave.CommandArgument = Item.ID.ToString();
		SetMethodType(Item.PaymentMethodKey);
		dvValue1.Visible = dvValue2.Visible = false;
        if (Item.BillingAddress == null)
            Item.BillingAddress = new WalletApp.WalletService.WalletAddress();
		phEditForm.DataBind();
		phEditForm.Visible = true;
     
	}

    protected void New_Command(object sender, EventArgs e)
    {
        Item = new WalletApp.WalletService.WalletStoredPaymentMethod();
		SetMethodType(AddPaymentMethodId);
        if (Item.BillingAddress == null)
            Item.BillingAddress = new WalletApp.WalletService.WalletAddress();
		phEditForm.DataBind();
		phEditForm.Visible = true;
	}

	protected void Save_Command(object sender, CommandEventArgs e)
	{
		if (AddPaymentMethodId == null) {
			int itemId;
			if (!int.TryParse(e.CommandArgument.ToString(), out itemId)) return;
			Item = WalletService.GetStoredPaymentMethod(itemId);
			if (Item == null) return;
		} else {
			Item = new WalletApp.WalletService.WalletStoredPaymentMethod();
			Item.PaymentMethodKey = AddPaymentMethodId.ToString();
			Item.AccountValue1 = txtValue1.Text.Replace("-", "");
			Item.AccountValue2 = txtValue2.Text.Replace("-", "");
		}
		Item.OwnerName = txtFullname.Text;
		Item.ExpirationDate = parseExpDate(txtExpiration.Text);
		Item.IsDefault = chkIsDefault.Checked;
        if (Item.BillingAddress == null)
            Item.BillingAddress = new WalletApp.WalletService.WalletAddress();
        baAddress.Address = Item.BillingAddress; baAddress.Save();
		var result = WalletService.StorePaymentMethod(Item);
		if (result.IsSuccess)
			Response.Redirect("StoredMethod.aspx");
		ltResult.Text = result.Message;
		ltResult.Visible = true;
	}
}
