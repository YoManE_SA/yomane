﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" Inherits="CreditCard" CodeBehind="StoredMethod.aspx.cs" %>

<%@ Register TagPrefix="WC" TagName="CustomerAddress" Src="~/Control/CustomerAddress.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- BEGIN PLUGIN CSS -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrap-checkbox/css/bootstrap-checkbox.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="assets/plugins/ios-switch/ios7-switch.css" type="text/css" media="screen">
    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadcrumb" runat="Server">
    <ul class="breadcrumb">
        <li>
            <p>YOU ARE HERE</p>
        </li>
        <li><a href="#" class="active">Accounts</a> </li>
    </ul>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadPage" runat="Server">
    <i class="icon-custom-left"></i>
    <h3>My - <span class="semi-bold">Accounts</span></h3>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <script>
        jQuery(function ($) {
            $("#txtmobile").mask("(999)-99-9999999");
            $("#txtEditmobile").mask("(999)-99-9999999");
            $("#txtExpiration").mask("99/9999");
            $("#txtEditExpiration").mask("99/9999");
            $("#txtCreditCard").mask("9999-9999-9999-9999");
        });

    </script>
    
    <asp:PlaceHolder runat="server" ID="phItemsList">
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs" id="tab-2">
                        <asp:Repeater ID="rptShortDetalisCard" runat="server">
                            <ItemTemplate>
                                <li class='<%# Container.ItemIndex == 0 ? "active" : ""%>'><a href="#div<%# Eval("ID") %>"><%# Eval("Display") %></a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="tab-content">
                        <asp:Repeater ID="rptDetalisCard" runat="server">
                            <ItemTemplate>
                                <div class="tab-pane <%# Container.ItemIndex == 0 ? "active" : ""%>" id="div<%# Eval("ID") %>">
                                    <div class="row column-seperation">
                                        <div class="col-md-6">
                                            <h3><span class="semi-bold">
                                                <%# Eval("OwnerName") %>
                                            </h3>
                                            <p>
                                                <img src="<%# Eval("Icon") %>" alt="Credit Card" style="border: 1px solid #e4e4e4; width: 50px;" />
                                                <%# Eval("Display") %> | <%# Eval("ExpirationDate", "{0:MM / yyyy}") %>
                                            </p>

                                        </div>
                                        <div class="col-md-6">
                                            <h3><span class="semi-bold">Billing</span> Address</h3>
                                            <p class="light">
                                                <%# Eval("BillingAddress.AddressLine1") %>,  <%# Eval("BillingAddress.AddressLine2") %>,  <span class="semi-bold"><%# Eval("BillingAddress.City") %></span>
                                            </p>

                                            <p class="light">
                                                <%# WalletService.GetStateName((string)Eval("BillingAddress.CountryISO"), (string)Eval("BillingAddress.StateISO")) %>  <%# Eval("BillingAddress.PostalCode") %>
                                            </p>

                                            <p class="light">
                                                <%# WalletService.GetCountryName((string)Eval("BillingAddress.CountryISO"))%>
                                            </p>
                                            <p class="pull-right">
                                                <asp:LinkButton runat="server" type="button" ID="btnDelete" CommandArgument='<%# Eval("ID") %>' CausesValidation="false" class="btn btn-white btn-cons" OnClientClick="return confirm('Are you sure you want to delete this user?');" CommandName="Delete" OnCommand="Delete_Command">Delete</asp:LinkButton>
                                                <asp:LinkButton runat="server" type="button" ClientIDMode="Static" ID="btnEdit" CommandArgument='<%# Eval("ID") %>' CausesValidation="false" class="btn btn-default btn-cons" OnCommand="Edit_Command">Edit</asp:LinkButton>
                                                <!--<asp:LinkButton runat="server" type="button" ClientIDMode="Static" ID="btnAdd" CommandArgument='<%# Eval("ID") %>' CausesValidation="false" class="btn btn-info btn-cons" OnCommand="New_Command">Add</asp:LinkButton>-->
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <br />
    <asp:PlaceHolder runat="server" ID="phEditForm" Visible="false">
        <div class="row" runat="server">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><span class="semi-bold" runat="server" id="spAddTitle">Credit Card</span></h4>
                        <div class="tools"><a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove"></a></div>
                    </div>
                    <div class="grid-body no-border">
                        <div class="form-no-horizontal-spacing" id="form-condensed-insert">
                            <div class="row column-seperation">
                                <div class="col-md-6">
                                    <h4 runat="server" id="hSubTitle">Credit Card Detalis</h4>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Joe Dow'" data-placement="right">Owner's Name</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtFullname" runat="server" type="text" class="form-control" Text="<%# Item.OwnerName %>" />
                                            <asp:RequiredFieldValidator ValidationGroup="EditForm" runat="server" Display="Dynamic" ControlToValidate="txtFullname" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                        </div>
                                    </div>
                                    <div class="form-group" runat="server" id="dvValue1">
                                        <asp:Label runat="server" ID="lblValue1" class="form-label">Card Number</asp:Label>
                                        <span class="help">e.g. "4580-0000-0000-0000"</span>
                                        <div class="controls">
                                            <asp:TextBox ClientIDMode="Static" runat="server" ID="txtValue1" MaxLength="16" type="text" class="form-control" Text="<%# Item.AccountValue1 %>" />
                                            <asp:RequiredFieldValidator runat="server" ValidationGroup="EditForm" Display="Dynamic" ControlToValidate="txtValue1" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                        </div>
                                    </div>
                                    <div class="form-group" runat="server" id="dvValue2">
                                        <asp:Label runat="server" ID="lblValue2" class="form-label">CVV</asp:Label>
                                        <span class="help">e.g. "777"</span>
                                        <div class="controls">
                                            <asp:TextBox ClientIDMode="Static" runat="server" ID="txtValue2" type="text" class="form-control" Text="<%# Item.AccountValue2 %>" />
                                            <asp:RequiredFieldValidator runat="server" ValidationGroup="EditForm" Display="Dynamic" ControlToValidate="txtValue2" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                        </div>
                                    </div>
                                    <div class="form-group" runat="server" id="dvExpData">
                                        <label class="form-label">Expiration Date</label>
                                        <span class="help">e.g. "10/2014"</span>
                                        <div class="controls">
                                            <asp:TextBox runat="server"  ID="txtExpiration" ClientIDMode="Static" type="text" class="form-control" Text='<%# Item.ExpirationDate.GetValueOrDefault().ToString("MM / yyyy") %>' />
                                            <asp:RequiredFieldValidator runat="server" ValidationGroup="EditForm" Display="Dynamic" ControlToValidate="txtExpiration" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <asp:CheckBox ID="chkIsDefault" runat="server" Checked="<%# Item.IsDefault %>" />
                                            <label class="form-label">Set as my default payment options</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <WC:CustomerAddress runat="server" ID="baAddress" Address="<%# Item.BillingAddress %>" />
                                </div>
                            </div>
                             <div class="row">
                            <div class="col-md-12">
                                 <asp:Label ID="ltResult" ClientIDMode="Static" runat="server" CssClass="block  alert alert-error"  Visible="false" />
                            </div>
                             </div>
                            <div class="form-actions">
                                <div class="pull-right">
                                    <asp:Button class="btn btn-info btn-cons" runat="server" ID="btnSave" ValidationGroup="EditForm" type="submit" Text="Save" OnCommand="Save_Command" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Plugins" runat="Server">
    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/breakpoints.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <!-- END CORE JS FRAMEWORK -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- PAGE JS -->
    <script src="assets/js/form_validations.js" type="text/javascript"></script>
    <script src="assets/js/tabs_accordian.js" type="text/javascript"></script>
    <script src="assets/js/messages_notifications.js" type="text/javascript"></script>
</asp:Content>
