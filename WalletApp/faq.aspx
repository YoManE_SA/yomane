﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="faq.aspx.cs" Inherits="faq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- BEGIN PLUGIN CSS -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen" />
    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadcrumb" runat="Server">
    <ul class="breadcrumb">
        <li>
            <p>YOU ARE HERE</p>
        </li>
        <li><a href="#" class="active">FAQ</a> </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadPage" runat="Server">
    <i class="icon-custom-left"></i>
    <h3>Frequently - <span class="semi-bold">Asked Questions</span></h3>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion" data-toggle="collapse">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">How is YoManE different from other payment methods?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse  in">
                        <div class="panel-body">
                            It’s more convenient: YoManE does not require a card reader or take you to another site for you to enter in shipping details or to pay. It’s also more secure, because when you scan a S-QR™ with your iPhone or Android phone, you take the products’ information instead of the seller storing your bank info. In addition, you can scan a S-QR anywhere – from retail outlets and websites to social media sites. No other payment solution lets you pay for more things in more places.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">How do I pay for something without cash or cards?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            When you see a product or service you want to buy that has a S-QR, you simply scan it like you would any other QR code. Your funds are securely transferred to the seller after you enter your PIN and confirm your purchase. No card needed! 
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">How is a S-QR different from a regular QR code?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            A YoManE S-QR uses advanced encryption technologies such as PKI. As opposed to linking to a regular website for passive viewing, S-QR links to stock items and payment details for actionable one-step purchasing. 
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">How secure is YoManE?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            With YoManE, you never have to pass personal information to a merchant again! We utilize 256-bit encryption. It minimizes fraud by requiring two-factor authentication. Tokenization reduces “man-in-the-middle” attacks. No bank account information is ever stored at the point of sale. And it’s fully PCI-compliant.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">How is YoManE faster than other payment methods?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            YoManE speeds up the payment experience by allowing for one-step purchasing. Never again enter bank account details when checking out. Never again enter shipping details when checking out, either. 
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Where can I use YoManE?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
                            On personal websites, Facebook, and Twitter. In videos, advertisements and retail outlets. On post cards, t-shirts, even yourself. ANYWHERE!
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Is it free?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse">
                        <div class="panel-body">
                            For consumers, it’s free. It’s also free to use for Merchants! Of course, if someone uses a Visa, Mastercard, etc. linked to their account, the merchant will still have to pay interchange… but we don’t charge any merchant fees regardless. YoManE also utilizes social network feeds by enabling sellers to freely post S-QR codes on sites ranging from Facebook and Pinterest to Twitter. For content owners and others, there is no more need for middlemen such as iTunes or Amazon. Now, the merchant or content owner gets to keep 100 percent of what they sell. Plus there are no subscription fees for buyers or sellers for baseline services.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">How does the system work?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseEight" class="panel-collapse collapse">
                        <div class="panel-body">
                            For online purchases, the merchant creates S-QR tags on YoManE’s intuitive Web-based interface. The merchant is free to distribute these codes via any channel — from Facebook and retail POS to print ads. After downloading the YoManE app, consumers enter payment details only once. At the point of sale, a consumer scans a S-QR, enters PIN, and confirms. The merchant receives payment via PayPal or other payment processor. The consumer, meanwhile, gets confirmation of the transaction within seconds.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">How do I sign up and use YoManE?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseNine" class="panel-collapse collapse">
                        <div class="panel-body">
                            Click here to get started.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Can I purchase something by scanning a S-QR if I don’t have YoManE loaded on my smartphone?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTen" class="panel-collapse collapse">
                        <div class="panel-body">
                            Yes. You can use ANY QR scanner (Redlaser, etc.) and you will be directed to our secure payments page. On this page, you would enter your payment details and you’re done! 
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">How do I get a merchant to offer YoManE payments?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseEleven" class="panel-collapse collapse">
                        <div class="panel-body">
                            Thanks for asking! Have him or her go to HERE 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Plugins" runat="Server">
    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/breakpoints.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
    <!-- END CORE JS FRAMEWORK -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- PAGE JS -->
    <script src="assets/js/tabs_accordian.js" type="text/javascript"></script>
</asp:Content>

