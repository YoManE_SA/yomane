﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalletApp;

public partial class SelectPaymentType : BasePage
{
    protected WalletApp.WalletService.WalletLists UserInfo { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
		var selectedGroup = Request["PaymentGroup"];
		if (!String.IsNullOrEmpty(selectedGroup)) {
			var list = WalletService.GetCustomerLists().PaymentMethods.Where(l => l.GroupKey == selectedGroup);
			if (list.Count() == 1)
				Response.Redirect(string.Format("~/StoredMethod.aspx?AddPaymentMethod={0}", list.First().Key));
			rptPaymentMethod.DataSource = list;
			rptPaymentMethod.DataBind();
			mvState.ActiveViewIndex = 1;
		} else {
			var list = WalletService.GetCustomerLists().PaymentMethodGroups;
			rptPaymentGroups.DataSource = list;
			rptPaymentGroups.DataBind();
		}
    }
  
}