﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" Inherits="GetProducts" CodeBehind="GetProducts.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<!-- BEGIN PLUGIN CSS -->
<link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="assets/plugins/jquery-superbox/css/style.css" rel="stylesheet" type="text/css" media="screen"/>
<!-- END PLUGIN CSS -->
<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadcrumb" runat="Server">
    <ul class="breadcrumb">
        <li>
            <p>YOU ARE HERE</p>
        </li>
        <li><a href="#" class="active">Product List</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadPage" runat="Server">
    <i class="icon-custom-left"></i>
    <h3>Product - <span class="semi-bold">List</span></h3>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
          <div class="pull-left" id="Filters">
        <div class="btn-group"> <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-demo-space"> <span class="anim150">Region</span> <span class="caret"></span> </a>
          <ul class="dropdown-menu">
            <li class="active" data-filter="all" data-dimension="region"><a href="#">All</a></li>
            <li data-filter="alaska" data-dimension="region"><a href="#">Alaska</a></li>
            <li data-filter="intermountain" data-dimension="region"><a href="#">Intermountain</a></li>
            <li data-filter="northeast" data-dimension="region"><a href="#">Northeast</a></li>
            <li data-filter="pacific_west" data-dimension="region"><a href="#">Pacific</a> </li>
            <li data-filter="southeast" data-dimension="region"><a href="#">Southeast</a></li>
          </ul>
        </div>
        <div class="btn-group"> <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-demo-space"> <span class="anim150">Recreation</span> <span class="caret"></span> </a>
          <ul class="dropdown-menu">
            <li class="active" data-filter="all" data-dimension="recreation"><a href="#">All</a></li>
            <li data-filter="camping" data-dimension="recreation"><a href="#">Camping</a></li>
            <li data-filter="climbing" data-dimension="recreation"><a href="#">Climbing</a></li>
            <li data-filter="fishing" data-dimension="recreation"><a href="#">Fishing</a></li>
            <li data-filter="swimming" data-dimension="recreation"><a href="#">AlSwimmingl</a></li>
          </ul>
        </div>
      </div>
      <div class="pull-right">
        <div data-toggle="buttons-radio" class="btn-group">
          <button class="btn btn-primary active" type="button" id="ToList"><i class="icon-th-list"></i></button>
          <button class="btn btn-primary" type="button" id="ToGrid"><i class="icon-th-large"></i></button>
        </div>
      </div>
      <div class="clearfix"></div>
      <br>
      <div id="Parks" class="just">
        <!-- "TABLE" HEADER CONTAINING SORT BUTTONS (HIDDEN IN GRID MODE)-->
        <div class="list_header">
          <div class="meta name active desc" id="SortByName"> Name &nbsp; <span class="sort anim150 asc active" data-sort="data-name" data-order="desc"></span> <span class="sort anim150 desc" data-sort="data-name" data-order="asc"></span> </div>
          <div class="meta region">Region</div>
          <div class="meta rec">Recreation</div>
          <div class="meta area" id="SortByArea"> Area in Acres &nbsp; <span class="sort anim150 asc" data-sort="data-area" data-order="asc"></span> <span class="sort anim150 desc" data-sort="data-area" data-order="desc"></span> </div>
        </div>
        <!-- FAIL ELEMENT -->
        <div class="fail_element anim250">Sorry &mdash; we could not find any parks matching matching these criteria</div>
        <!-- BEGIN LIST OF PARKS (MANY OF THESE ELEMENTS ARE VISIBLE ONLY IN LIST MODE)-->
        <ul>
		<li class="mix northeast camping climbing fishing swimming" data-name="Acadia" data-area="47452.80">
          <div class="meta name">
            <div class="img_wrapper"> <img src="assets/img/others/acadia.jpg" alt="" /> </div>
            <div class="titles">
              <h2>Acadia</h2>
              <p><em>Maine</em></p>
            </div>
          </div>
          <div class="meta region">
            <p>Northeast</p>
          </div>
          <div class="meta rec">
           
              <p>Swimming</p>
           
          </div>
          <div class="meta area">
            <p>47,452.80</p>
          </div>
        </li>
        <!-- END LIST OF PARKS -->
		</ul>
      </div>
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h4>Search <span class="semi-bold">Products</span></h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove"></a></div>

                </div>
                <div class="grid-body no-border">
                     <asp:TextBox ID="SearchTextBox" runat="server" type="text" placeholder="search" OnKeyDown="//if(event.charCode == 13) document.getElementById('btnSearch').click();" />
                      <asp:LinkButton ID="btnSearch" ClientIDMode="Static" runat="server" CssClass="btn btn-primary" OnClick="Search_Click" CausesValidation="false"><i class="icon-search"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h4>Product <span class="semi-bold">list</span></h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="form-no-horizontal-spacing">
                        <div class="row column-seperation">
                            <div class="col-md-7">
                           
        <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" RenderMode="Block" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="side-widget">
                    <div class="side-widget-content" id="friends-list">
                        <asp:Repeater Visible="false" runat="server" ID="rptProducts">
                            <ItemTemplate>
                                <div class="user-details-wrapper">
                                    <div class="SearchProduct">
                                        <a href="<%# Eval("ProductURL") %>" target="_blank">
                                            <img src="<%# Eval("ImageURL") %>" /></a>
                                        <div><a href="<%# Eval("ProductURL") %>" target="_blank"><%# Eval("Name") %></a></div>
                                        <div><%# Eval("Price", "{0:0.00}") %> <%# Eval("Currency") %></div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Label ID="lblResult" Visible="false" ForeColor="White" runat="server" Text="Sorry no result..." />
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="pull-right">
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h4>Contact <span class="semi-bold">us</span></h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="form-no-horizontal-spacing" id="form-condensed">
                        <div class="row column-seperation">
                            <div class="col-md-7">
                                <div class="row form-row">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="pull-right">
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Plugins" runat="Server">
<!-- END CHAT -->
<!-- END CONTAINER -->
<!-- BEGIN CORE JS FRAMEWORK-->
<script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/breakpoints.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script> 
<!-- END CORE JS FRAMEWORK -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-mixitup/jquery.mixitup.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/js/search_results.js" type="text/javascript"></script>
<!-- BEGIN CORE TEMPLATE JS -->
</asp:Content>
