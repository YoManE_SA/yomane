﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="True" CodeBehind="ShippingManage.aspx.cs" MaintainScrollPositionOnPostback="true" Inherits="ShippingAddress" %>

<%@ Register TagPrefix="WC" TagName="CustomerAddress" Src="~/Control/CustomerAddress.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
    <!-- BEGIN PLUGIN CSS -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="assets/plugins/ios-switch/ios7-switch.css" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen" />

    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
    <script src="assets/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
    <link href="assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadcrumb" runat="server">
    <ul class="breadcrumb">
        <li>
            <p>YOU ARE HERE</p>
        </li>
        <li><a href="#" class="active">Shipping Addresses</a> </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadPage" runat="server">
    <i class="icon-custom-left"></i>
    <h3>Shipping Addresses</h3>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="alert alert-success" runat="server" id="dvSuccess" visible="false">
        <button class="close" data-dismiss="alert"></button>
        Success:&nbsp;The <a href="#" class="link">Address</a> Was Updated.
    </div>
    <asp:PlaceHolder runat="server" ID="phItemsList">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs" id="tab-5">
                    <asp:Repeater ID="rptShippingShort" runat="server">
                        <ItemTemplate>
                            <li class='<%# Container.ItemIndex == 0 ? "active pull-left" : ""%>'><a href="#div<%# Eval("ID") %>"><%# Eval("Title") %></a></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <div class="tab-content">
                    <asp:Repeater runat="server" ID="rptDetalisShipping">
                        <ItemTemplate>
                            <div class="tab-pane <%# Container.ItemIndex == 0 ? "active" : ""%>" id="div<%# Eval("ID") %>">
                                <div class="row column-seperation">
                                    <div class="col-md-6">
                                        <h3><span class="semi-bold"><%# Eval("Title") %></span></h3>
                                        <p>
                                            <%# Eval("AddressLine1") %>, <%# Eval("AddressLine2") %>
                                            <%# Eval("City") %>,
                                            <%# Eval("PostalCode") %>
                                            <%# WalletService.GetStateName((string)Eval("CountryISO"), (string)Eval("StateISO")) %>
                                            <%# WalletService.GetCountryName((string)Eval("CountryISO"))%>, 
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <h3>Choose Your option</h3>
                                        <p class="light">
                                            <asp:LinkButton runat="server" type="button" ClientIDMode="Static" ID="btnEdit" CommandArgument='<%# Eval("ID") %>' CausesValidation="false" class="btn btn-default btn-cons" OnCommand="Edit_Command">Edit</asp:LinkButton>
                                            <asp:LinkButton runat="server" type="button" ID="btnDelete" CommandArgument='<%# Eval("ID") %>' CausesValidation="false" class="btn btn-white btn-cons" OnClientClick="return confirm('Are you sure you want to delete this user?');" CommandName="Delete" OnCommand="Delete_Command">Delete</asp:LinkButton>
                                            <!-- <asp:LinkButton runat="server" type="button" ClientIDMode="Static" ID="btnAdd" CommandArgument='<%# Eval("ID") %>' CausesValidation="false" class="btn btn-info btn-cons" OnCommand="New_Command">Add</asp:LinkButton> -->
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="col-md-12">
                <asp:Label ID="ltResultdvResultShipping" ClientIDMode="Static" runat="server" />
            </div>
        </div>
    </asp:PlaceHolder>
    <br />
    <asp:PlaceHolder runat="server" ID="phForm" Visible="false">
        <div class="row" runat="server">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>Edit <span class="semi-bold" runat="server" id="spAddTitle">Shipping Address</span></h4>
                        <div class="tools"><a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove"></a></div>
                    </div>
                    <div class="grid-body no-border">
                        <div class="form-no-horizontal-spacing" id="form-condensed-insert">
                            <div class="row column-seperation">
                                <div class="col-md-6">
                                    <h4>Shipping Description</h4>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. 'My Home' " data-placement="right">Title</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtTitle" runat="server" type="text" class="form-control" Text="<%# Item.Title %>" />
                                            <asp:RequiredFieldValidator ValidationGroup="EditForm" runat="server" Display="Dynamic" ControlToValidate="txtTitle" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Door code'" data-placement="right">Comment:</label>
                                        <div class="controls">
                                            <asp:TextBox ClientIDMode="Static" runat="server" TextMode="MultiLine" ID="txtComment" type="text" Text="<%# Item.Comment %>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <asp:CheckBox ID="IsDefaultShipping" runat="server" Checked="<%# Item.IsDefault %>" />
                                            <label class="form-label">Set as my default payment options</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <WC:CustomerAddress runat="server" ID="baAddress" Address="<%# Item %>" />
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="pull-right">
                                    <asp:Button class="btn btn-info btn-cons" runat="server" ID="btnSave" ValidationGroup="EditForm" type="submit" Text="Save" OnCommand="Save_Command" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Plugins" runat="server">
    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/breakpoints.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <!-- END CORE JS FRAMEWORK -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="assets/js/tabs_accordian.js" type="text/javascript"></script>
    <script src="assets/js/support_ticket.js" type="text/javascript"></script>
    <script src="assets/js/messages_notifications.js" type="text/javascript"></script>
    <script src="assets/js/form_validations.js" type="text/javascript"></script>
    <!-- BEGIN CORE TEMPLATE JS -->
</asp:Content>
