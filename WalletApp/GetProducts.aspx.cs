﻿using System;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Web.UI;
using WalletApp;

public partial class GetProducts : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
        protected void Search_Click(object sender, EventArgs e)
        {
            string sText = SearchTextBox.Text;
            if (sText == null)
            {
                lblResult.Visible = true;
            }
            else
            {
                var sf = new WalletApp.WalletService.ShopSearchFilter();
                sf.Text = sText;
                rptProducts.DataSource = WalletService.GetProducts(sf, 1, 300);
                rptProducts.DataBind();
                rptProducts.Visible = true;
            }
        }

    }

