﻿using System;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Web.UI;
using WalletApp;

public partial class Contactus : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
			var appReq = new AppIdentityRequest();
			appReq.SendContactEmail("info@mahala.us", "Received mail From Wallet", txtMessage.Text);
			ltResult.Text = "Your message was sent!";
            txtMessage.Text = "";
        } catch(Exception ex) {
			ltResult.Text = "Error: " + ex.Message;
        }
    }
}
