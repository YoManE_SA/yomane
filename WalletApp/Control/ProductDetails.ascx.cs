﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalletApp
{
    public partial class ProductDetails : System.Web.UI.UserControl
	{
		private BasePage SitePage { get { return Page as BasePage; } }
        protected void Search_Click(object sender, EventArgs e)
        {
            string sText = SearchTextBox.Text;
            if (sText == null)
            {
                lblResult.Visible = true;
            }
            else
            {
                var sf = new WalletApp.WalletService.ShopSearchFilter();
                sf.Text = sText;
                rptProducts.DataSource = SitePage.WalletService.GetProducts(sf, 1, 10);
                rptProducts.DataBind();
                rptProducts.Visible = true;
            }
        }

	}
}