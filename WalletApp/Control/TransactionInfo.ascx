﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TransactionInfo.ascx.cs" Inherits="WalletApp.TransactionInfo" %>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <i class="PaymentMethodGroup_<%# Transaction.PaymentMethodGroupKey %> icon-7x"></i>
                <h4 id="myModalLabel" class="semi-bold">Transaction info
                    <asp:Label runat="server" Text="<%# Transaction.ID %>" />
                    |
                    <asp:Label runat="server" Text="<%# Transaction.InsertDate %>" />
                </h4>
                <h4>
                    <asp:Label runat="server" Text="<%# Transaction.Text %>" Visible='<%# !string.IsNullOrEmpty(Transaction.Text) %>' />
                    <asp:Label runat="server" Text='<%# Transaction.Amount.ToString("0.00") %>' />
                    <asp:Label runat="server" Text="<%# Transaction.CurrencyIso %>" />
                </h4>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    <div class="col-md-12">
                        <h4>Genreal Details:</h4>
                        <section class="white-transaction" style="line-height: 30px;">
                            <div class="col-md-6 pull-left">
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.FullName) %>'><i class="icon-user grey"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.FullName %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.PaymentDisplay) %>'><i class="icon-credit-card"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.PaymentDisplay %>" /></div>
                            </div>
                            <div class="col-md-6 pull-left">
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Email) %>'><i class="icon-envelope"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.Email %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Phone) %>'><i class="icon-phone"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.Phone %>" /></div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                    </div>
                    <div class="col-md-12">
                        <h4>Merchant Details:</h4>
                        <section class="white-transaction" style="line-height: 30px;">
                            <div class="col-md-6 pull-left">
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.Name) %>'><i class="icon-user"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.Merchant.Name %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.Email) %>'><i class="icon-envelope"></i>&nbsp;&nbsp;<asp:HyperLink runat="server" NavigateUrl='<%# "mailto:" + Transaction.Merchant.Email %>' Text="<%# Transaction.Merchant.Email %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.PhoneNumber) %>'><i class="icon-phone"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.Merchant.PhoneNumber %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.WebsiteUrl) %>'><i class="icon-share-alt"></i>&nbsp;&nbsp;<asp:HyperLink runat="server" NavigateUrl='<%# Transaction.Merchant.WebsiteUrl %>' Target="_blank" Text="<%# Transaction.Merchant.WebsiteUrl %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.Number) %>'><i class="icon-info-sign"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.Merchant.Number %>" /></div>
                            </div>
                            <div class="col-md-6 pull-left">
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.AddressLine1) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.Merchant.AddressLine1 %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.AddressLine2) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.Merchant.AddressLine2 %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.City) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.Merchant.City %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.CountryIso) %>'><i class="icon-globe"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# SitePage.WalletService.GetCountryName(Transaction.Merchant.CountryIso) %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.StateIso) %>'><i class="icon-globe"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# SitePage.WalletService.GetStateName(Transaction.Merchant.CountryIso, Transaction.Merchant.StateIso) %>" /></div>
                                <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.Merchant.PostalCode) %>'><i class="icon-globe"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.Merchant.PostalCode %>" /></div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12" runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.ReceiptText) %>'>
                                <i class="icon-comments"></i>&nbsp;&nbsp;Receipt Text:<asp:Label runat="server" Text="<%# Transaction.ReceiptText %>" />
                            </div>
                            <div class="col-md-12" runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.ReceiptLink) %>'>
                                <i class="icon-link"></i>&nbsp;&nbsp;Receipt Link:<asp:Label runat="server" Text="<%# Transaction.ReceiptLink %>" />
                            </div>
                      </section>
                    </div>
                    <asp:PlaceHolder runat="server" ID="phBillingAddress">
                        <div class="col-md-12">
                            <h4>Billing Address:</h4>
                            <section class="white-transaction" style="line-height: 30px;">
                                <div class="col-md-6 pull-left">
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.BillingAddress.AddressLine1) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.BillingAddress.AddressLine1 %>" /></div>
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.BillingAddress.AddressLine2) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.BillingAddress.AddressLine2 %>" /></div>
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.BillingAddress.City) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.BillingAddress.City %>" /></div>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.BillingAddress.CountryIso) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# SitePage.WalletService.GetCountryName(Transaction.BillingAddress.CountryIso) %>" /></div>
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.BillingAddress.StateIso) %>'><i class="icon-globe"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# SitePage.WalletService.GetStateName(Transaction.BillingAddress.CountryIso, Transaction.BillingAddress.StateIso) %>" /></div>
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.BillingAddress.PostalCode) %>'><i class="icon-globe"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.BillingAddress.PostalCode %>" /></div>
                                </div>
                                <div class="clearfix"></div>
                            </section>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phShippingAddress">
                        <div class="col-md-12">
                            <h4>Shipping Address:</h4>
                            <section class="white-transaction" style="line-height: 30px;">
                                <div class="col-md-6 pull-left">
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.ShippingAddress.AddressLine1) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.ShippingAddress.AddressLine1 %>" /></div>
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.ShippingAddress.AddressLine2) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.ShippingAddress.AddressLine2 %>" /></div>
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.ShippingAddress.City) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.ShippingAddress.City %>" /></div>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.ShippingAddress.CountryIso) %>'><i class="icon-map-marker"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# SitePage.WalletService.GetCountryName(Transaction.ShippingAddress.CountryIso) %>" /></div>
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.ShippingAddress.StateIso) %>'><i class="icon-globe"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# SitePage.WalletService.GetStateName(Transaction.ShippingAddress.CountryIso, Transaction.ShippingAddress.StateIso) %>" /></div>
                                    <div runat="server" visible='<%# !string.IsNullOrEmpty(Transaction.ShippingAddress.PostalCode) %>'><i class="icon-globe"></i>&nbsp;&nbsp;<asp:Label runat="server" Text="<%# Transaction.ShippingAddress.PostalCode %>" /></div>
                                </div>
                                <div class="clearfix"></div>
                            </section>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /Modal -->
