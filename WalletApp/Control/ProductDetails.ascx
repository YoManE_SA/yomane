﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetails.ascx.cs" Inherits="WalletApp.ProductDetails" %>
<div id="sidr">
    <div class="chat-window-wrapper">
        <div class="chat-header">
            <div class="pull-left">
                <asp:TextBox ID="SearchTextBox" runat="server" type="text" placeholder="search" OnKeyDown="//if(event.charCode == 13) document.getElementById('btnSearch').click();" />
            </div>
            <div class="pull-right">
                <div style="margin: 15px 15px 15px 0;">
                    <asp:LinkButton ID="btnSearch" ClientIDMode="Static" runat="server" CssClass="btn btn-primary" OnClick="Search_Click" CausesValidation="false"><i class="icon-search"></i></asp:LinkButton>
                </div>
            </div>
            <div style="clear: both;"></div>
        </div>
        <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" RenderMode="Block" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="side-widget">
                    <div class="side-widget-content" id="friends-list">
                        <asp:Repeater Visible="false" runat="server" ID="rptProducts">
                            <ItemTemplate>
                                <div class="user-details-wrapper">
                                    <div class="SearchProduct">
                                        <a href="<%# Eval("ProductURL") %>" target="_blank">
                                            <img src="<%# Eval("ImageURL") %>" /></a>
                                        <div><a href="<%# Eval("ProductURL") %>" target="_blank"><%# Eval("Name") %></a></div>
                                        <div><%# Eval("Price", "{0:0.00}") %> <%# Eval("Currency") %></div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Label ID="lblResult" Visible="false" ForeColor="White" runat="server" Text="Sorry no result..." />
    </div>
</div>
