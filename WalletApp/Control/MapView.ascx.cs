﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalletApp
{
	public partial class MapView : System.Web.UI.UserControl
	{
        public System.Web.UI.WebControls.Unit Height { get; set; }
        private const string ProductUrl = "https://uiservices.mahala.us/Public/Product.aspx?merchantID={0}&Item={1}";
		protected BasePage SitePage { get { return Page as BasePage; } }
		protected void Page_Load(object sender, EventArgs e)
		{
			var result = SitePage.WalletService.SearchProductBySales(32.085388, 34.800141, null);
            if (result == null) return;
			var sb = new System.Text.StringBuilder();
			sb.AppendLine("var locations = [");
			//foreach (var item in result)
			//	sb.AppendFormat("['<a href=\"{0}\" target=\"product\">{1}</>', {2}, {3}],\r\n", string.Format(ProductUrl, item.MerchantNumber, item.ProductID), item.ProductName, item.Latitude, item.Longitude);
			sb.Remove(sb.Length - 1, 1);
			sb.AppendLine("];");
			Page.ClientScript.RegisterClientScriptBlock(GetType(), "LoadMapLocations", sb.ToString(), true);
		}

        protected override void OnPreRender(EventArgs e)
        {
            dvWorldMap.Style["height"] = Height.ToString();
            base.OnPreRender(e);
        }
	}
}