﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalletApp
{
    public partial class TransactionInfo : System.Web.UI.UserControl
	{
		protected BasePage SitePage { get { return Page as BasePage; } }
		public WalletService.WalletTransaction Transaction { get; set; }
		public override void DataBind()
		{
            phBillingAddress.Visible = (Transaction.BillingAddress != null);
            phShippingAddress.Visible = (Transaction.ShippingAddress != null);
            if (Transaction.BillingAddress == null) Transaction.BillingAddress = new WalletService.WalletAddress();
            if (Transaction.ShippingAddress == null) Transaction.ShippingAddress = new WalletService.WalletAddress();
            base.DataBind();
		}

	}
}