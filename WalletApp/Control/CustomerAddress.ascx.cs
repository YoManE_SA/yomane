﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalletApp
{
    public partial class CustomerAddress : System.Web.UI.UserControl
	{
        
		public WalletService.WalletAddress Address { get; set; }
		private BasePage SitePage { get { return Page as BasePage; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlState.Enabled = ((ddlCountry.SelectedValue == "US") || (ddlCountry.SelectedValue == "CA"));
        }
		public override void DataBind()
		{
			ddlCountry.DataSource = SitePage.WalletService.GetLists().Countries;
			ddlState.DataSource = SitePage.WalletService.GetLists().UsaStates;
			base.DataBind();
			try {ddlState.SelectedValue = Address.StateIso;} catch{ } //ignore error if not exist
			try {ddlCountry.SelectedValue = Address.CountryIso;} catch { } //ignore error if not exist
		}
		public void Save()
		{
            Address.AddressLine1 = txtAddressLine1.Text;
            Address.AddressLine2 = txtAddressLine2.Text;
            Address.City = txtCity.Text;
            Address.PostalCode = txtPostalCode.Text;
            Address.StateIso = ddlState.Text;
            Address.CountryIso = ddlCountry.Text;
            
		}
		protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
		{
			ddlState.Enabled = ((ddlCountry.SelectedValue == "US") || (ddlCountry.SelectedValue == "CA"));
		}

		protected override void OnPreRender(EventArgs e)
		{
            dvStateDP.Style.Add("background-color", (ddlState.Enabled ? "#fff" : "#f4f5f7"));
  			base.OnPreRender(e);
		}

	}
}