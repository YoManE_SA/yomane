﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TransactionList.ascx.cs" Inherits="WalletApp.TransactionList" %>
<%@ Register TagPrefix="WC" TagName="TransactionInfo" Src="~/Control/TransactionInfo.ascx" %>
<div class="row-fluid">
    <div class="span12">
        <div class="grid simple ">
            <div class="grid-title">
                <h4>View <span class="semi-bold">Transaction</span></h4>
                <div class="tools"><a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove"></a></div>
            </div>
            <div class="grid-body ">
                <table class="table table-striped" id="example2">
                    <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Payment Method</th>
                            <th>Amount</th>
                            <th>Merchant Name</th>
                            <th>Product Info</th>
                            <th>Status</th>
                            <th style="display: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater runat="server" ID="rptTransactions" OnItemCommand="Transaction_Command">
                            <ItemTemplate>
                                <tr runat="server" >
                                    <td><asp:LinkButton runat="server" ID="btnShowDetails" CommandArgument='<%# Eval("ID") %>' CssClass="icon-plus-sign" /></td>
                                    <td><%# Eval("ID") %></td>
                                    <td><%# Eval("InsertDate") %></td>
                                    <td><img src='<%# SitePage.WalletService.GetPaymentMethodIcon((string)Eval("PaymentMethodKey")) %>' width="30"  /><%# Eval("PaymentDisplay") %></td>
                                    <td class="center"><%# Eval("Amount", "{0:0.00}") %> <%# Eval("CurrencyIso") %></td>
                                    <td><%# Eval("Merchant.Name") %></td>
                                    <td><%# Eval("Text") %></td>
                                    <td><span class="label label-success">Success</span></td>
                                    <td style="display: none;"></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<asp:UpdatePanel runat="server" ID="upTransactionInfo" RenderMode="Block" UpdateMode="Conditional">
    <ContentTemplate>
        <WC:TransactionInfo runat="server" ID="transInfo" Visible="false" />
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="rptTransactions" />
    </Triggers>
</asp:UpdatePanel>
