﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalletApp
{
	public partial class TransactionList : System.Web.UI.UserControl
	{
		protected BasePage SitePage { get { return Page as BasePage; } }
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack) {
				rptTransactions.DataSource = SitePage.WalletService.GetTransactions(1, 300);
				rptTransactions.DataBind();
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			//ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(
			base.OnPreRender(e);
		}

		protected void Transaction_Command(object sender, RepeaterCommandEventArgs e)
		{
			var transData = SitePage.WalletService.GetTransaction(int.Parse(e.CommandArgument.ToString()));
			transInfo.Transaction = transData;
			transInfo.Visible = true;
			transInfo.DataBind();
			ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ModalShow", "$('#myModal').modal('show');", true);
			//upTransactionInfo.Update();
		}
	}
}