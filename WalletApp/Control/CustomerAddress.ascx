﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerAddress.ascx.cs" Inherits="WalletApp.CustomerAddress" %>
<h4>Postal Information</h4>
<div class="form-group">
    <label class="form-label tip" data-toggle="tooltip" title="e.g. '216 New Hampshire Ave. '" data-placement="right">Street address, P.O. box, company name, c/o</label>

    <div class="controls">
        <asp:TextBox runat="server" ID="txtAddressLine1" Text="<%# Address.AddressLine1 %>" type="text" class="form-control" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ValidationGroup="EditForm" ControlToValidate="txtAddressLine1" CssClass="error-login-screen" ErrorMessage="This field is required." />
    </div>
</div>
<div class="form-group">
    <label class="form-label tip" data-toggle="tooltip" title="e.g. '2 floor'" data-placement="right">Apartment, suite, unit, building, floor, etc.</label>
    <div class="controls">
    <asp:TextBox runat="server" ID="txtAddressLine2" Text="<%# Address.AddressLine2 %>" type="text" class="form-control" />
    </div>
</div>
<div class="form-group">
    <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Massapequa'" data-placement="right">City</label>
    <div class="controls">
        <asp:TextBox runat="server" ID="txtCity" Text="<%# Address.City %>" type="text" class="form-control" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ValidationGroup="EditForm" ControlToValidate="txtCity" CssClass="error-login-screen" ErrorMessage="This field is required." />
    </div>
</div>
<div class="form-group">
    <label class="form-label tip" data-toggle="tooltip" title="e.g. 'United States'" data-placement="right">Country</label>
    <div class="controls">
        <asp:DropDownList runat="server" CssClass="select2 form-control" ID="ddlCountry" DataTextField="Name" DataValueField="Key" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
            <asp:ListItem Text="" />
        </asp:DropDownList>
    </div>
</div>
<div class="form-group">
    <label class="form-label tip" data-toggle="tooltip" title="e.g. 'New York'" data-placement="right">State</label>
    <div class="controls">
        <div id="dvStateDP" runat="server">
            <asp:DropDownList ID="ddlState" CssClass="select2 form-control" Enabled="false"  runat="server" type="text" DataTextField="Name" DataValueField="Key" AppendDataBoundItems="true">
                <asp:ListItem Text="Choose Your State" Value="" />
            </asp:DropDownList>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="form-label tip" data-toggle="tooltip" title="e.g. '11758'" data-placement="right">Zip Code</label>
    <div class="controls">
        <asp:TextBox ID="txtPostalCode" runat="server" type="text" Text="<%# Address.PostalCode %>" class="form-control" />
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" ControlToValidate="txtPostalCode" ValidationGroup="EditForm" CssClass="error-login-screen" ValidationExpression="^\d{5}(?:[-\s]\d{4})?$" runat="server" ErrorMessage="Zip Code not valid." />
    </div>
</div>
