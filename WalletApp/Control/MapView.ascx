﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MapView.ascx.cs" Inherits="WalletApp.MapView" %>
<div class="row">
    <div class="col-md-12 col-sm-12" >
        <div class="row tiles-container tiles white spacing-bottom">  
            <div class="tiles-chart">
                    <div class="controller">
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>

                    <div id="dvWorldMap" class="demo-map" runat="server">
                        <div id="map" style="width: 100%; height: 100%; position: relative; overflow: hidden; background-color: rgb(229, 233, 236);"></div>
                    </div>
                    <!-- SCRIPT GOOGLE MAPS -->
					<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
                    <script type="text/javascript">
                        var styles = [
                            {
                            featureType: 'water',
                            elementType: 'geometry',
                            stylers: [
								{ hue: '#fff' },
								{ saturation: -65 },
								{ lightness: 63 },
								{ visibility: 'on' }
                            ]
                            }, {
                            featureType: 'landscape.natural',
                            elementType: 'all',
                            stylers: [
								{ hue: '#C3C6C9' },
								{ saturation: -65 },
								{ lightness: -18 },
								{ visibility: 'on' }
                            ]
                            }, {
                            featureType: 'landscape',
                            elementType: 'all',
                            stylers: [
								{ hue: '#C3C6C9' },
								{ saturation: -81 },
								{ lightness: -13 },
								{ visibility: 'on' }
                            ]
                            }, {
                            featureType: 'landscape.man_made',
                            elementType: 'all',
                            stylers: [
								{ hue: '#C3C6C9' },
								{ saturation: -81 },
								{ lightness: -13 },
								{ visibility: 'on' }
                            ]
                            }, {
                            featureType: 'road',
                            elementType: 'all',
                            stylers: [
								{ hue: '#C3C6C9' },
								{ saturation: -95 },
								{ lightness: 38 },
								{ visibility: 'on' }
                            ]
                            }
                        ];

                        var options = {
                        	mapTypeControlOptions: { mapTypeIds: ['Styled'] },
                        	zoom: 16,
                        	mapTypeId: 'Styled'
                        };
                        var div = document.getElementById('map');
                        var map = new google.maps.Map(div, options);
                        if (navigator.geolocation) {
                        	navigator.geolocation.getCurrentPosition(
							function (position) { userLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); map.setCenter(userLocation); },
							function () { map.setCenter(new google.maps.LatLng(38.582526, -98.613281)); });
                        }
                        var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
                        map.mapTypes.set('Styled', styledMapType);
                        var infowindow = new google.maps.InfoWindow();
                        var image = 'assets/img/Pin.png';
                        var marker, i;
                        for (i = 0; i < locations.length; i++) {
                        	marker = new google.maps.Marker({
                        		position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        		map: map,
                        		icon: image
                        	});
                        	google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        		return function () {
                        			infowindow.setContent(locations[i][0]);
                        			infowindow.open(map, marker);
                        		}
                        	})(marker, i));
                        }
                        /*
						*/
                    </script>
                    <!-- END GOOGLE SCRIPT MAPS -->

                </div>
        </div>
    </div>
</div>