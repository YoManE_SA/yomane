﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalletApp;

public partial class EditPersonalDetalis : BasePage
{
    protected WalletApp.WalletService.WalletCustomer UserInfo { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
		UserInfo = WalletService.GetCustomer();
        if (!IsPostBack) DataBind();

    }
    protected void Update_Click(object sender, EventArgs e) 
    {
        UserInfo.FirstName = txtFirstName.Text;
        UserInfo.LastName = txtLastName.Text;
        UserInfo.PersonalNumber = txtPassport.Text;
        UserInfo.PhoneNumber = txtPhone.Text.Replace("(", "").Replace(")", "");
        UserInfo.CellNumber = txtMobile.Text.Replace("(", "").Replace(")", "");
        DateTime birthDate;
        if (DateTime.TryParse(txtBirthDate.Text, out birthDate))
            UserInfo.DateOfBirth = birthDate;
        baAddress.Address = UserInfo; baAddress.Save();
        var result = WalletService.SaveCustomer(UserInfo);
        ltResultdvResultCustomerDetalis.CssClass = result.IsSuccess ? "block alert alert-success" : "block alert alert-error";
        ltResultdvResultCustomerDetalis.Text = result.Message;
        ltResultdvResultCustomerDetalis.Visible = true;
    }

    protected void UpdatePassword_Click(object sender, EventArgs e)
    {
        var result = WalletService.UpdatePassword(txtPassword.Text, txtNewPassword.Text);
        ltResultPassword.CssClass = result.IsSuccess ? "block alert alert-success" : "block alert alert-error";
        ltResultPassword.Text = result.Message;
        ltResultPassword.Visible = true;
    }

    protected void UpdatePinCode_Click(object sender, EventArgs e)
    {
       
        var result = WalletService.UpdatePincode(txtPinPassword.Text, txtNewPincode.Text);
        ltResultdvResultPinCode.CssClass = result.IsSuccess ? "block alert alert-success" : "block alert alert-error";
        ltResultdvResultPinCode.Text = result.Message;
        ltResultdvResultPinCode.Visible = true;
        
    }

    protected void UpdatePicture_Click(object sender, EventArgs e)
    {
		var customer = WalletService.GetCustomer();
        customer.ProfileImage = fuProfileImage.FileBytes;
        var result = WalletService.SaveCustomer(customer);
        ltResultUploadImg.CssClass = result.IsSuccess ? "block alert alert-success" : "block alert alert-error";
        ltResultUploadImg.Text = result.Message;
        ltResultUploadImg.Visible = true;

    }
    
   
}