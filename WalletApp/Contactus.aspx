﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" Inherits="Contactus" CodeBehind="Contactus.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- BEGIN PLUGIN CSS -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrap-checkbox/css/bootstrap-checkbox.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="assets/plugins/ios-switch/ios7-switch.css" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen" />
    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
    <!-- END CORE CSS FRAMEWORK -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadcrumb" runat="Server">
    <ul class="breadcrumb">
        <li>
            <p>YOU ARE HERE</p>
        </li>
        <li><a href="#" class="active">Contact us</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadPage" runat="Server">
    <i class="icon-custom-left"></i>
    <h3>Contact - <span class="semi-bold">Us</span></h3>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <script>
        jQuery(function ($) {
            $("#date").mask("99/99/9999");
            $("#txtmobile").mask("(999)-99-9999999");
            $("#txtphone").mask("(999)-99-9999999");
        });
    </script>
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h4>Contact <span class="semi-bold">us</span></h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a><a href="javascript:;" class="remove"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="form-no-horizontal-spacing" id="form-condensed">
                        <div class="row column-seperation">
                            <div class="col-md-7">
                                <div class="row form-row">
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Other'" data-placement="right">Subject</label>

                                        <div class="controls">
                                            <asp:DropDownList name="Subject" ClientIDMode="Static" ID="choice" class="select2 form-control" runat="server">
                                                <asp:ListItem Value="">Please select an option</asp:ListItem>
                                                <asp:ListItem>General</asp:ListItem>
                                                <asp:ListItem>Information</asp:ListItem>
                                                <asp:ListItem>Refund</asp:ListItem>
                                                <asp:ListItem>Order</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Your Message'" data-placement="right">Message</label>

                                        <div class="controls">
                                            <asp:TextBox name="Message" ID="txtMessage" class="form-control" TextMode="MultiLine" runat="server" placeholder="Message" />
                                            <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="txtMessage" CssClass="error" ErrorMessage="This field is required." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-12">
                                 <asp:Label ID="ltResult" ClientIDMode="Static" runat="server" CssClass="block  alert alert-error" Visible="false" />
                            </div>
                             </div>
                        <div class="form-actions">
                            <div class="pull-right">
                                <asp:Button runat="server" OnClick="btnSubmit_Click" class="btn btn-info btn-cons" Text="Send" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Plugins" runat="Server">
    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/breakpoints.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <!-- END CORE JS FRAMEWORK -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="assets/js/form_validations.js" type="text/javascript"></script>
</asp:Content>
