﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="SelectPaymentType" CodeBehind="SelectPaymentType.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadcrumb" runat="Server">
    <ul class="breadcrumb">
        <li>
            <p>YOU ARE HERE</p>
        </li>
        <li><a href="#" class="active">Payment Method</a> </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadPage" runat="Server">
    <i class="icon-custom-left"></i>
    <h3>Payment Method</h3>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <h4>Choose Payment Method</h4>
    <div class="row">
        <div class="col-md-12">
            <asp:MultiView runat="server" ID="mvState" ActiveViewIndex="0">
                <asp:View runat="server" ID="viewSelectGroup">
                    <asp:Repeater runat="server" ID="rptPaymentGroups">
                        <ItemTemplate>
                            <div class="col-md-3">
                                <div class="grid simple">
                                    <div class="grid-body no-border center-text">
                                            <h4 class="semi-bold"><a href="SelectPaymentType.aspx?PaymentGroup=<%# Eval("key") %>"><%# Eval("Name") %></a></h4>
                                        <a href="SelectPaymentType.aspx?PaymentGroup=<%# Eval("key") %>">
                                            <img src="<%# Eval("icon") %>" /></a>
                                    
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:View>
                <asp:View runat="server" ID="viewSelectType">
                    <asp:Repeater runat="server" ID="rptPaymentMethod">
                        <ItemTemplate>
                            <div class="col-md-3">
                                <div class="grid simple">
                                    <div class="grid-body no-border center-text">
                                        <h4 class="semi-bold"><a href="StoredMethod.aspx?AddPaymentMethod=<%# Eval("key") %>"><%# Eval("Name") %></a></h4>
                                        <a href="StoredMethod.aspx?AddPaymentMethod=<%# Eval("key") %>">
                                            <img src="<%# Eval("icon") %>" style="max-width: 100%;" /></a>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:View>
            </asp:MultiView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Plugins" runat="Server">
    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/breakpoints.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
    <!-- END CORE JS FRAMEWORK -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="assets/js/support_ticket.js" type="text/javascript"></script>
    <!-- BEGIN CORE TEMPLATE JS -->
</asp:Content>
