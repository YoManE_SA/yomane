﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalletApp;

public partial class AddShippingAddress : BasePage
{
    protected WalletApp.WalletService.WalletShippingAddress Item { get; set; }
	protected override void OnLoad(EventArgs e)
	{
		if (!IsPostBack) New_Command(this, e);

		base.OnLoad(e);
        
	}

    protected void New_Command(object sender, EventArgs e)
    {
        Item = new WalletApp.WalletService.WalletShippingAddress();
        phAddForm.DataBind();
        phAddForm.Visible = true;
	}

	protected void Save_Command(object sender, CommandEventArgs e)
	{
		int itemId;
		if (!int.TryParse(e.CommandArgument.ToString(), out itemId)) return;
		Item = WalletService.GetShippingAddress(itemId);
		if (Item == null) Item = new WalletApp.WalletService.WalletShippingAddress();
        Item.Title = txtTitle.Text;
        Item.IsDefault = IsDefaultShipping.Checked;
        Item.Comment = txtComment.Text;
		baAddress.Address = Item; baAddress.Save();
		var result = WalletService.SaveShippingAddress(Item);
		if (result.IsSuccess)
			Response.Redirect("ShippingManage.aspx");
		ltResult.Text = result.Message;
		ltResult.Visible = true;
	}
}
