﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" Inherits="EditPersonalDetalis" CodeBehind="EditPersonalDetalis.aspx.cs" %>

<%@ Register TagPrefix="WC" TagName="CustomerAddress" Src="~/Control/CustomerAddress.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- BEGIN PLUGIN CSS -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrap-checkbox/css/bootstrap-checkbox.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="assets/plugins/ios-switch/ios7-switch.css" type="text/css" media="screen" />
    <link href="assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen" />

    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
    <script src="assets/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
    <link href="assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
    <style>
        #choice option {color: black;}
        .empty {color: gray;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadcrumb" runat="Server">
    <ul class="breadcrumb">
        <li>
            <p>YOU ARE HERE</p>
        </li>
        <li><a href="#" class="active">Edit Personal Details</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadPage" runat="Server">
    <i class="icon-custom-left"></i>
    <h3>Edit - <span class="semi-bold">Personal Details</span></h3>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <script>
        jQuery(function ($) {
            $("#txtBirthDate").mask("99/99/9999");
            $("#txtMobile").mask("(999)-99-9999999");
            $("#txtPhone").mask("(999)-9-9999999");
        });
    </script>
    <div class="row" runat="server">
        <div class="col-md-8">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h4>Please <span class="semi-bold">fill your details</span></h4>
                    <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                </div>
                <div class="grid-body no-border">
                    <div class="form-no-horizontal-spacing" id="form-condensed-insert">
                        <div class="row column-seperation">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h4>Basic Information</h4>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. 'John'" data-placement="right">First Name</label>

                                        <div class="controls">
                                            <asp:TextBox runat="server" ID="txtFirstName" type="text" class="form-control" placeholder="First Name" Text="<%# UserInfo.FirstName %>" />
                                            <asp:RequiredFieldValidator ID="FirstNameValidator" runat="server" Display="Dynamic" ControlToValidate="txtFirstName" ValidationGroup="PersonalValidGroup" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Dow'" data-placement="right">Last Name</label>

                                        <div class="controls">
                                            <asp:TextBox runat="server" ID="txtLastName" type="text" class="form-control" placeholder="Last Name" Text="<%# UserInfo.LastName %>" />
                                            <asp:RequiredFieldValidator ID="LastNameValidator" runat="server" Display="Dynamic" ControlToValidate="txtLastName" ValidationGroup="PersonalValidGroup" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. '085698778'" data-placement="right">Passport / National ID</label>
                                        <div class="controls">
                                            <asp:TextBox runat="server" type="text" placeholder="Passport / National ID" class="form-control" MaxLength="16" ID="txtPassport" Text="<%# UserInfo.PersonalNumber %>" />
                                            <asp:RequiredFieldValidator ID="PassportValidator" runat="server" Display="Dynamic" ControlToValidate="txtPassport" ValidationGroup="PersonalValidGroup" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                            <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\d{1,10}" ValidationGroup="PersonalValidGroup" CssClass="error" ControlToValidate="txtPassport" ErrorMessage="Must be 10  characters" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. '10/02/1976'" data-placement="right">Date Of birth</label>

                                        <div class="controls">
                                            <asp:TextBox runat="server" type="text" class="form-control" ID="txtBirthDate" ClientIDMode="Static" placeholder="Date of birth" Text="<%# UserInfo.DateOfBirth %>" />
                                            <asp:RequiredFieldValidator ID="BirthDateValidator" runat="server" Display="Dynamic" ControlToValidate="txtBirthDate" ValidationGroup="PersonalValidGroup" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. '(972)-50-5555555'" data-placement="right">Cell Phone</label>
                                        <div class="controls">
                                            <asp:TextBox runat="server" type="text" class="form-control" ID="txtMobile" ClientIDMode="Static" placeholder="(972)-50-5555555" Text="<%# UserInfo.CellNumber%>" />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. '(972)-3-5555555'" data-placement="right">Phone Number</label>

                                        <div class="controls">
                                            <asp:TextBox runat="server" ID="txtPhone" type="text" class="form-control" ClientIDMode="Static" ValidationGroup="PersonalValidGroup" placeholder="(972)-3-5555555" Text="<%# UserInfo.PhoneNumber %>" />
                                            <asp:RequiredFieldValidator ID="txtPhoneValidator" runat="server" Display="Dynamic" ControlToValidate="txtPhone" ValidationGroup="PersonalValidGroup" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label tip" data-toggle="tooltip" title="e.g. 'joe@dow.com'" data-placement="right">Email Address</label>
                                        <div class="controls">
                                            <asp:TextBox runat="server" type="text" class="form-control" ID="txtEmailAddress" ClientIDMode="Static" placeholder="e.g: liran@obl.me" Text="<%# UserInfo.EmailAddress%>" />
                                            <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="txtEmailAddress" ValidationGroup="PersonalValidGroup" CssClass="error-login-screen" ErrorMessage="This field is required." />
                                            <asp:RegularExpressionValidator runat="server" CssClass="error" Display="Dynamic" ControlToValidate="txtEmailAddress" ValidationGroup="PersonalValidGroup" ErrorMessage="This field is required." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <WC:CustomerAddress runat="server" ID="baAddress" Address="<%# UserInfo %>" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Label ID="ltResultdvResultCustomerDetalis" ClientIDMode="Static" runat="server" />
                            </div>
                            <div class="col-md-12">
                                <div runat="server" id="dvError" class="alert alert-error" visible="false">
                                    <button class="close" data-dismiss="alert"></button>
                                    <span class="semi-bold">Error! </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="pull-right">
                                <asp:Button runat="server" class="btn btn-info btn-cons" UseSubmitBehavior="true" Text="Save" ValidationGroup="PersonalValidGroup" OnClick="Update_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <div class="expand">
                            <h4>Edit Your Profile image</h4>
                            <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                        </div>
                    </div>
                    <div class="grid-body no-border">
                        <div class="form-no-horizontal-spacing">
                            <div class="row column-seperation">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Begin Script  Edit Photo-->
                                             <!-- Load Feather code -->
                                                    <script type="text/javascript" src="http://feather.aviary.com/js/feather.js"></script>

                                                    <!-- Instantiate Feather -->
                                                    <script type='text/javascript'>
                                                        var featherEditor = new Aviary.Feather({
                                                            apiKey: 'your-key-here',
                                                            apiVersion: 3,
                                                            theme: 'light', // Check out our new 'light' and 'dark' themes!
                                                            tools: 'all',
                                                            appendTo: '',
                                                            onSave: function (imageID, newURL) {
                                                                var img = document.getElementById(imageID);
                                                                img.src = newURL;
                                                            },
                                                            onError: function (errorObj) {
                                                                alert(errorObj.message);
                                                            }
                                                        });
                                                        function launchEditor(id, src) {
                                                            featherEditor.launch({
                                                                image: id,
                                                                url: src
                                                            });
                                                            return false;
                                                        }
                                                    </script>

                                                    <div id='injection_site'></div>

                                            <div class="form-group">
                                                <div>
                                                    <label class="form-label tip">Select your image profile:</label>
                                                    <span class="help">Size: 160px on 160px</span>
                                                </div>
                                                <div class="margin-top-10 margin-bottom-10">
                                                      <div class="fileUpload btn btn-cons-short btn-primary pull-left">
                                                    <span><i class="icon-folder-open"></i> Upload</span>
                                                    <asp:FileUpload runat="server" ClientIDMode="Static" CssClass="upload" />
                                                </div>
                                                   <div class="pull-left"><asp:LinkButton ClientIDMode="Static" runat="server" OnClientClick="return launchEditor('image1', 'http://patimer.co.il/img/portrait-blank.png');" CssClass="btn btn-cons-short btn-info"><i class="icon-edit"></i> Edit Photo</asp:LinkButton></div>   
                                                   <div class="pull-left"><asp:LinkButton ClientIDMode="Static" runat="server" OnClientClick="return alert('Are you sure?')" CssClass="btn btn-cons-short btn-default"><i class="icon-trash"></i> Delete</asp:LinkButton></div>
                                                 
                                               </div>
                                                <div class="clearfix"></div>
                                                <div class="wrap-image-upload margin-top-10 pull-left">
                                                  
                                                    <img id="image1" src="assets/img/profiles/avatar.jpg" style="width: 160px; height: 160px;" />
                                                </div> 
                                                <div class="wrap-spinner pull-right">
                                                    <i class="icon-spinner icon-spin icon-5x"></i>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="alert alert-success margin-top-20"><i class="icon-ok-sign" ></i> success <button class="close" data-dismiss="alert"></button></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="pull-right">
                                <asp:Button runat="server" class="btn btn-info btn-cons" UseSubmitBehavior="true" OnClick="UpdatePicture_Click" OnClientClick="$('dvProgress').show();" Text="Save" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <div class="expand">
                            <h4>Edit Your Profile image</h4>
                            <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                        </div>
                    </div>
                    <div class="grid-body no-border">
                        <div class="form-no-horizontal-spacing">
                            <div class="row column-seperation">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div>
                                                <div class="wrap-image-upload">
                                                    <img src="profileImage.ashx" style="width: 160px; height: 160px;" />
                                                </div>
                                                 <div class="fileUpload btn btn-cons-short btn-primary margin-10">
                                                    <span><i class="icon-folder-open"></i> Upload</span>
                                                    <asp:FileUpload id="fuProfileImage" runat="server" ClientIDMode="Static" CssClass="upload" />
                                                </div>
                                               </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 margin-top-10">

                                            <asp:Label ID="ltResultUploadImg" ClientIDMode="Static" runat="server" CssClass="block  alert alert-error" Visible="false" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="pull-right">
                                    <asp:Button runat="server" class="btn btn-info btn-cons" UseSubmitBehavior="true" OnClick="UpdatePicture_Click" OnClientClick="$('dvProgress').show();" Text="Save" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <div class="expand">
                            <h4>Edit Your Password</h4>
                            <div class="tools"><a href="javascript:;" class="expand"></a></div>
                        </div>
                    </div>
                    <div class="grid-body no-border" style="display: none;">
                        <div class="form-no-horizontal-spacing" id="form-condensed-password">
                            <div class="row column-seperation">
                                <div class="col-md-12">
                                    <div class="row form-row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Padfssw0rd' " data-placement="right">Old Password</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtPassword" type="password" class="form-control" placeholder="Password" runat="server" />
                                                    <asp:RequiredFieldValidator runat="server" ValidationGroup="PasswordValidator" CssClass="error" Display="Dynamic" ControlToValidate="txtPassword" ErrorMessage="This field is required." />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Padfssw0rd'" data-placement="right">New Password</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtNewPassword" type="password" class="form-control" placeholder="Password" runat="server" />
                                                    <asp:RequiredFieldValidator runat="server" CssClass="error" Display="Dynamic" ValidationGroup="PasswordValidator" ControlToValidate="txtNewPassword" ErrorMessage="This field is required." />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. 'Padfssw0rd' " data-placement="right">Confirm Password</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtConfirmPassword" type="password" class="form-control" placeholder="Password" runat="server" />
                                                    <asp:RequiredFieldValidator runat="server" CssClass="error" Display="Dynamic" ValidationGroup="PasswordValidator" ControlToValidate="txtConfirmPassword" ErrorMessage="This field is required." />
                                                    <asp:CompareValidator runat="server" ErrorMessage="Please retype Password" ValidationGroup="PasswordValidator" CssClass="error" Display="Dynamic" ControlToValidate="txtConfirmPassword" ControlToCompare="txtNewPassword" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-row">
                                        <div class="col-md-12">
                                            <div class="alert alert">
                                                <span class="semi-bold">The password is subject to the following restrictions</span>
                                                <ul>
                                                    <li>The password length must be at least 8 characters</li>
                                                    <li>The password must contain only Latin letters (A-Z) and decimal digits (0-9)</li>
                                                    <li>The password must containt at least 2 decimal digits (0-9)</li>
                                                    <li>The password must contain at least 2 Latin letters (A-Z)</li>
                                                    <li>You cannot return back to three last passwords you used</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:Label ID="ltResultPassword" ClientIDMode="Static" runat="server" CssClass="block  alert alert-error" Visible="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="pull-right">
                                    <asp:Button runat="server" class="btn btn-info btn-cons" UseSubmitBehavior="true" ValidationGroup="PasswordValidator" OnClick="UpdatePassword_Click" Text="Save" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>Edit Your Pin Code</h4>
                        <div class="tools"><a href="javascript:;" class="expand"></a></div>
                    </div>
                    <div class="grid-body no-border" style="display: none;">
                        <div class="form-no-horizontal-spacing" id="form-condensed-Pincode">
                            <div class="row column-seperation">
                                <div class="col-md-12">
                                    <div class="row form-row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. '9875'" data-placement="right">Your Password</label>

                                                <div class="controls">
                                                    <asp:TextBox ID="txtPinPassword" runat="server" type="password" class="form-control" placeholder="Password" />
                                                    <asp:RequiredFieldValidator runat="server" ValidationGroup="PincodeValidator" CssClass="error" Display="Dynamic" ControlToValidate="txtPinPassword" ErrorMessage="This field is required." />


                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. '5432'" data-placement="right">New Pin Code</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtNewPincode" type="password" class="form-control" placeholder="Pin Code" runat="server" MaxLength="4" />
                                                    <asp:RequiredFieldValidator runat="server" ValidationGroup="PincodeValidator" CssClass="error" Display="Dynamic" ControlToValidate="txtNewPincode" ErrorMessage="This field is required." />
                                                    <asp:RegularExpressionValidator runat="server" ValidationGroup="PincodeValidator" Display="Dynamic" ControlToValidate="txtNewPincode" ValidationExpression="\d{4}" ErrorMessage="4 Numbers Only" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label tip" data-toggle="tooltip" title="e.g. '5432'" data-placement="right">Confirm Pin Code</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtConfirmPincode" type="password" class="form-control" MaxLength="4" placeholder="Pin Code" runat="server" />
                                                    <asp:RequiredFieldValidator runat="server" CssClass="error" Display="Dynamic" ValidationGroup="PincodeValidator" ControlToValidate="txtConfirmPincode" ErrorMessage="This field is required." />
                                                    <asp:CompareValidator runat="server" ValidationGroup="PincodeValidator" CssClass="error" Display="Dynamic" ErrorMessage="Please retype Password" ControlToValidate="txtConfirmPassword" ControlToCompare="txtNewPincode" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-row">
                                        <div class="col-md-12">
                                            <div class="alert alert">
                                                <span class="semi-bold">The pin code is subject to the following restrictions</span>
                                                <ul>
                                                    <li>The pincode length must be at least 4 numbers</li>
                                                    <li>You cannot return back to three last pincodes you used</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:Label ID="ltResultdvResultPinCode" ClientIDMode="Static" runat="server" CssClass="block  alert alert-error" Visible="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="pull-right">
                                    <asp:Button runat="server" class="btn btn-info btn-cons" UseSubmitBehavior="true" ValidationGroup="PincodeValidator" OnClick="UpdatePinCode_Click" Text="Save" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Plugins" runat="Server">
    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/breakpoints.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <!-- END CORE JS FRAMEWORK -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/js/messages_notifications.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="assets/js/form_validations.js" type="text/javascript"></script>
</asp:Content>
