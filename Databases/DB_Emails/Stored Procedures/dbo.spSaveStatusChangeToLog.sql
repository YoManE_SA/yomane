SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		evgeniy.b
-- Create date: 22/08/2012
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSaveStatusChangeToLog]
    @Message_id		AS INT ,
    @StatusOld		AS INT ,
    @StatusNew		AS INT ,
    @AdmincashUser	AS NVARCHAR(50) ,
    @Message		AS NVARCHAR(255)
AS 
    BEGIN
        SET NOCOUNT ON;
        INSERT  INTO dbo.LogStatus
                ( Message_id ,
                  StatusOld ,
                  StatusNew ,
                  AdmincashUser ,
                  [Message]
                )
        VALUES  ( @Message_id ,
                  @StatusOld ,
                  @StatusNew ,
                  @AdmincashUser ,
                  @Message
	            )
        --SELECT  'Success' = 1
    END
GO
