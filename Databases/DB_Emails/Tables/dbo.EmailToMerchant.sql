CREATE TABLE [dbo].[EmailToMerchant]
(
[EmailToMerchant_id] [int] NOT NULL IDENTITY(1, 1),
[EmailAddress] [nvarchar] (255) COLLATE Hebrew_100_CI_AS NULL,
[Merchant_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmailToMerchant] ADD CONSTRAINT [PK_EmailToMerchant] PRIMARY KEY CLUSTERED  ([EmailToMerchant_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmailToMerchant] ADD CONSTRAINT [UIX_EmailToMerchant_EmailAddress_MerchantID] UNIQUE NONCLUSTERED  ([EmailAddress], [Merchant_id]) ON [PRIMARY]
GO
