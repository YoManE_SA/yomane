CREATE TABLE [dbo].[MessageStatus]
(
[MessageStatus_id] [tinyint] NOT NULL,
[IsDefault] [bit] NOT NULL CONSTRAINT [DF_MessageStatus_IsDefault] DEFAULT ((0)),
[IsVisible] [bit] NOT NULL CONSTRAINT [DF_MessageStatus_IsVisible] DEFAULT ((0)),
[Name] [nvarchar] (50) COLLATE Hebrew_100_CI_AS NOT NULL CONSTRAINT [DF_MessageStatus_Name] DEFAULT (''),
[Description] [nvarchar] (500) COLLATE Hebrew_100_CI_AS NOT NULL CONSTRAINT [DF_MessageStatus_Description] DEFAULT (''),
[Order] [tinyint] NOT NULL CONSTRAINT [DF_MessageStatus_Order] DEFAULT ((0)),
[Color] [nvarchar] (50) COLLATE Hebrew_100_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MessageStatus] ADD CONSTRAINT [PK_MessageStatus] PRIMARY KEY CLUSTERED  ([MessageStatus_id]) ON [PRIMARY]
GO
