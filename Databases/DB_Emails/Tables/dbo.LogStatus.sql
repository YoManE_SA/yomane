CREATE TABLE [dbo].[LogStatus]
(
[LogStatus_id] [int] NOT NULL IDENTITY(1, 1),
[StatusOld] [int] NOT NULL CONSTRAINT [DF_LogStatus_StatusOld] DEFAULT ((0)),
[StatusNew] [int] NOT NULL CONSTRAINT [DF_LogStatus_StatusNew] DEFAULT ((0)),
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_LogStatus_InsertDate] DEFAULT (getdate()),
[AdmincashUser] [varchar] (50) COLLATE Hebrew_100_CI_AS NOT NULL CONSTRAINT [DF_LogStatus_AdmincashUser] DEFAULT (''),
[Message] [nvarchar] (255) COLLATE Hebrew_100_CI_AS NULL,
[Message_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogStatus] ADD CONSTRAINT [PK_LogStatus] PRIMARY KEY CLUSTERED  ([LogStatus_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogStatus] ADD CONSTRAINT [FK_LogStatus_Message] FOREIGN KEY ([Message_id]) REFERENCES [dbo].[Message] ([Message_id])
GO
