CREATE TABLE [dbo].[Message]
(
[Message_id] [int] NOT NULL IDENTITY(1, 1),
[MessagsDate] [datetime2] (0) NOT NULL,
[MessageMimeID] [nvarchar] (250) COLLATE Hebrew_100_CI_AS NULL,
[ThreadID] [uniqueidentifier] NULL,
[EmailFrom] [nvarchar] (1000) COLLATE Hebrew_100_CI_AS NULL,
[EmailTo] [nvarchar] (1000) COLLATE Hebrew_100_CI_AS NULL,
[CC] [nvarchar] (1000) COLLATE Hebrew_100_CI_AS NULL,
[Subject] [nvarchar] (1000) COLLATE Hebrew_100_CI_AS NULL,
[BodyText] [nvarchar] (max) COLLATE Hebrew_100_CI_AS NULL,
[AdmincashUser] [nvarchar] (50) COLLATE Hebrew_100_CI_AS NULL,
[MessageStatus_id] [tinyint] NULL,
[IsSent] [bit] NOT NULL CONSTRAINT [DFMessage_IsSent] DEFAULT ((0)),
[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Message_IsDeleted] DEFAULT ((0)),
[Merchant_id] [int] NULL,
[Mailbox_id] [smallint] NOT NULL,
[AttachmentCount] [tinyint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Message] ADD CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED  ([Message_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Message] ADD CONSTRAINT [FK_Message_Mailbox] FOREIGN KEY ([Mailbox_id]) REFERENCES [dbo].[Mailbox] ([Mailbox_id])
GO
ALTER TABLE [dbo].[Message] ADD CONSTRAINT [FK_Message_MessageStatus] FOREIGN KEY ([MessageStatus_id]) REFERENCES [dbo].[MessageStatus] ([MessageStatus_id])
GO
