CREATE TABLE [dbo].[Mailbox]
(
[Mailbox_id] [smallint] NOT NULL IDENTITY(1, 1),
[EmailAddress] [varchar] (100) COLLATE Hebrew_100_CI_AS NOT NULL,
[MailServer] [nvarchar] (50) COLLATE Hebrew_100_CI_AS NULL,
[UserName] [nvarchar] (50) COLLATE Hebrew_100_CI_AS NULL,
[Password] [nvarchar] (50) COLLATE Hebrew_100_CI_AS NULL,
[IsBlocked] [bit] NOT NULL,
[IsAutoScan] [bit] NOT NULL,
[IsSendFrom] [bit] NOT NULL,
[Signature] [varchar] (max) COLLATE Hebrew_100_CI_AS NULL,
[MessageStoragePath] [varchar] (50) COLLATE Hebrew_100_CI_AS NULL,
[isSSL] [bit] NOT NULL CONSTRAINT [DF_Mailbox_isSSL] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mailbox] ADD CONSTRAINT [PK_Mailbox] PRIMARY KEY CLUSTERED  ([Mailbox_id]) ON [PRIMARY]
GO
