CREATE TABLE [dbo].[tblBllLog]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[InsertDate] [datetime] NOT NULL,
[SeverityID] [tinyint] NOT NULL,
[Tag] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[Source] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[Message] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL,
[LongMessage] [ntext] COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBllLog] ADD CONSTRAINT [PK_tblBllLog] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblBllLog_InsertDate] ON [dbo].[tblBllLog] ([InsertDate]) ON [PRIMARY]
GO
