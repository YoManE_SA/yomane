CREATE TABLE [dbo].[tblErrorNet]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ErrorTime] [datetime] NOT NULL CONSTRAINT [DF__tblErrorN__Error__0B91BA14] DEFAULT (getdate()),
[ProjectName] [nvarchar] (25) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Proje__0C85DE4D] DEFAULT (''),
[RemoteIP] [nvarchar] (25) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Remot__0D7A0286] DEFAULT (''),
[LocalIP] [nvarchar] (25) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Local__0E6E26BF] DEFAULT (''),
[RemoteUser] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Remot__0F624AF8] DEFAULT (''),
[ServerName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Serve__10566F31] DEFAULT (''),
[ServerPort] [nvarchar] (5) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Serve__114A936A] DEFAULT (''),
[ScriptName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Scrip__123EB7A3] DEFAULT (''),
[RequestQueryString] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Reque__1332DBDC] DEFAULT (''),
[VirtualPath] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Virtu__14270015] DEFAULT (''),
[PhysicalPath] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Physi__151B244E] DEFAULT (''),
[ExceptionSource] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Excep__160F4887] DEFAULT (''),
[ExceptionMessage] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Excep__17036CC0] DEFAULT (''),
[ExceptionTargetSite] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Excep__17F790F9] DEFAULT (''),
[ExceptionStackTrace] [nvarchar] (1000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Excep__18EBB532] DEFAULT (''),
[ExceptionHelpLink] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Excep__19DFD96B] DEFAULT (''),
[ExceptionLineNumber] [int] NOT NULL CONSTRAINT [DF__tblErrorN__Excep__1AD3FDA4] DEFAULT ((0)),
[InnerExceptionSource] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Inner__1BC821DD] DEFAULT (''),
[InnerExceptionMessage] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Inner__1CBC4616] DEFAULT (''),
[InnerExceptionTargetSite] [nvarchar] (1000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Inner__1DB06A4F] DEFAULT (''),
[InnerExceptionHelpLink] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Inner__1F98B2C1] DEFAULT (''),
[InnerExceptionLineNumber] [int] NOT NULL CONSTRAINT [DF__tblErrorN__Inner__208CD6FA] DEFAULT ((0)),
[IsFailedSQL] [bit] NOT NULL CONSTRAINT [DF__tblErrorN__IsFai__2180FB33] DEFAULT ((0)),
[IsArchive] [bit] NOT NULL CONSTRAINT [DF__tblErrorN__IsArc__236943A5] DEFAULT ((0)),
[InnerExceptionStackTrace] [ntext] COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Inner__245D67DE] DEFAULT (''),
[RequestForm] [varchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF__tblErrorN__Reque__25518C17] DEFAULT (''),
[IsHighlighted] [bit] NOT NULL CONSTRAINT [DF_tblErrorNet_IsHighlighted] DEFAULT ((0)),
[ExceptionLineNumberDisplay] AS (case when [ExceptionLineNumber]>(0) then [ExceptionLineNumber] else [InnerExceptionLineNumber] end) PERSISTED NOT NULL,
[IsProduction] [bit] NOT NULL CONSTRAINT [DF_tblErrorNet_IsProduction] DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgErrorNetAddSetLineNumber] ON [dbo].[tblErrorNet] FOR INSERT AS
BEGIN
	UPDATE
		tblErrorNet
	SET
		ExceptionLineNumber=
			CASE
				CharIndex(':line', ExceptionStackTrace)
			WHEN 0 THEN	0
			ELSE
				CASE
					CharIndex(Char(13), ExceptionStackTrace, CharIndex(':line', ExceptionStackTrace))
				WHEN 0 THEN 0
				ELSE
					Cast(Substring(ExceptionStackTrace, CharIndex(':line', ExceptionStackTrace)+6, CharIndex(Char(13), ExceptionStackTrace, CharIndex(':line', ExceptionStackTrace))-CharIndex(':line', ExceptionStackTrace)-6) as int)
				END
			END,
		InnerExceptionLineNumber=
			CASE
				CharIndex(':line', InnerExceptionStackTrace)
			WHEN 0 THEN	0
			ELSE
				CASE
					CharIndex(Char(13), InnerExceptionStackTrace, CharIndex(':line', InnerExceptionStackTrace))
				WHEN 0 THEN 0
				ELSE
					Cast(Substring(InnerExceptionStackTrace, CharIndex(':line', InnerExceptionStackTrace)+6, CharIndex(Char(13), InnerExceptionStackTrace, CharIndex(':line', InnerExceptionStackTrace))-CharIndex(':line', InnerExceptionStackTrace)-6) as int)
				END
			END
	WHERE
		ID IN (SELECT ID FROM Inserted WHERE ExceptionLineNumber=0)


    UPDATE  dbo.tblErrorNet
    SET    [IsProduction]  = 0
    WHERE   id IN ( SELECT  ID
                    FROM    INSERTED
                    WHERE   LOWER(ISNULL([ServerName], '')) LIKE 'stage%' OR LOWER(ISNULL([ServerName], '')) LIKE 'test%');

END
GO
ALTER TABLE [dbo].[tblErrorNet] ADD CONSTRAINT [PK_tblErrorNet] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblErrorNet_ErrorTime] ON [dbo].[tblErrorNet] ([ErrorTime]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblErrorNet_IsArchive] ON [dbo].[tblErrorNet] ([IsArchive]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblErrorNet_LocalIP] ON [dbo].[tblErrorNet] ([LocalIP]) ON [PRIMARY]
GO
