SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Function [dbo].[fnGetErrorsInSameGroup](@nErrorID INT)
RETURNS TABLE
AS RETURN
	SELECT E2.ID
	FROM tblErrorNet AS E1 LEFT JOIN tblErrorNet AS E2
		ON	E1.ProjectName = E2.ProjectName
		AND E1.VirtualPath = E2.VirtualPath
		AND E1.ExceptionSource = E2.ExceptionSource
		AND E1.ExceptionMessage = E2.ExceptionMessage
		AND E1.IsHighlighted = E2.IsHighlighted
		AND E1.IsArchive = E2.IsArchive
	WHERE E1.ID = @nErrorID
GO
