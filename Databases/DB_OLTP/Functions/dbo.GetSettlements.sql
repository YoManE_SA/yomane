SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetSettlements](@nMerchant int=NULL, @dtFrom datetime=NULL, @dtTo datetime=NULL) RETURNS TABLE AS
RETURN
	SELECT
		s.ID,
		s.CompanyID Merchant,
		CompanyName [Company Name],
		c.CUR_ISOName Currency,
		transTotal [Amount Trans],
		transChargeTotal [Amount Charge],
		transPayTotal [Amount Total],
		CAST(PayDate AS date) [Payment Date],
		CASE IsShow WHEN 1 THEN 'Yes' ELSE 'No' END [Shown To Merchant],
		s.Comment,
		id_InvoiceNumber [Invoice Number],
		CAST(id_InsertDate AS date) [Invoice Date],
		ic.CUR_ISOName [Invoice Currency],
		id_TotalDocument [Invoice Amount],
		it.GD_Text [Invoice Type]
	FROM
		tblTransactionPay s
		LEFT JOIN tblCompany m ON s.CompanyID=m.ID
		LEFT JOIN tblSystemCurrencies c ON s.currency=c.CUR_ID
		LEFT JOIN tblInvoiceDocument i ON s.InvoiceDocumentID=i.ID
		LEFT JOIN tblSystemCurrencies ic ON id_Currency=ic.CUR_ID
		LEFT JOIN GetGlobalData(60) it ON id_Type=it.GD_ID 
	WHERE
		s.CompanyID=IsNull(@nMerchant, s.CompanyID)
		AND
		PayDate between IsNull(@dtFrom, DateAdd(year, -10, GetDate())) AND IsNull(@dtTo, DateAdd(year, 10, GetDate()));
GO
