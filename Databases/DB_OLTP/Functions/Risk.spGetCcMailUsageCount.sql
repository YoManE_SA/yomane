SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [Risk].[spGetCcMailUsageCount](@sMail varchar(100), @sCardNumber varchar(25), @bType BIT = 0)
RETURNS int
AS
BEGIN
    DECLARE @n INT;
	DECLARE @CardNumber256 varbinary(40);
	SET @CardNumber256 = dbo.GetEncrypted256(@sCardNumber);
	
    IF @bType = 0
		BEGIN
			-- Check count for credit cards per one email (default check)
			SELECT  @n = COUNT(*) FROM Risk.CcMailUsage WITH ( NOLOCK ) WHERE EmailAddress = @sMail;
		END
    ELSE
		BEGIN
			-- Check count for emails per one credit card number
			SELECT  @n = COUNT(*) FROM Risk.CcMailUsage WITH ( NOLOCK ) WHERE CreditCardNumber256 = @CardNumber256;
		END
    
    IF NOT EXISTS ( SELECT  1
                    FROM    Risk.CcMailUsage WITH ( NOLOCK )
                    WHERE   EmailAddress = @sMail
                            AND CreditCardNumber256 = @CardNumber256 ) 
        SET @n = @n + 1;
    RETURN @n;
END


GO
