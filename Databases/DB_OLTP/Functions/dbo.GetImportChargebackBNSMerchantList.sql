SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetImportChargebackBNSMerchantList](@sCard varchar(30), @sDate varchar(30), @sAmount varchar(30), @sCurrency varchar(30), @nDebitCompany int=0, @sApprovalNumber nvarchar(50), @sMRF varchar(30))
RETURNS varchar(300)
AS
BEGIN
	DECLARE @sResult varchar(1000); SET @sResult='';
	DECLARE @binCard varbinary(200); SET @binCard=dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups(@sCard));
	DECLARE @dtInsert datetime; SET @dtInsert=Convert(datetime, @sDate, 104);
	DECLARE @nAmount money; SET @nAmount=Convert(money, Replace(Replace(@sAmount, '.', ''), ',', '.'));
	DECLARE @nCurrency int; SET @nCurrency=IsNull((SELECT CUR_ID FROM tblSystemCurrencies WHERE CUR_ISOName=@sCurrency), 0);
	IF LTrim(RTrim(IsNull(@sMRF, '')))<>''
		SELECT DISTINCT
			@sResult=@sResult+', <'+'a href="merchant_data.asp?companyID='+LTrim(RTrim(Str(m.ID)))+'" target="_blank">'+CompanyName+'<'+'/a>'
		FROM
			tblCompanyTransPass t WITH (NOLOCK)
			INNER JOIN tblCompany m ON CompanyID=m.ID
		WHERE
			DeniedStatus IN (0, 3, 9, 11)
			AND
			DebitReferenceCode IN (@sMRF, @sMRF+'_01')
	ELSE
		SELECT DISTINCT
			@sResult=@sResult+', <'+'a href="merchant_data.asp?companyID='+LTrim(RTrim(Str(m.ID)))+'" target="_blank">'+CompanyName+'<'+'/a>'
		FROM
			tblCompanyTransPass t WITH (NOLOCK)
			INNER JOIN tblCompany m ON CompanyID=m.ID
			INNER JOIN tblCreditCard c WITH (NOLOCK) ON CreditCardID=c.ID
		WHERE
			DeniedStatus IN (0, 3, 9, 11)
			AND
			IsTestOnly=0
			AND
			@nDebitCompany IN (0, DebitCompanyID)
			AND
			Amount=@nAmount
			AND
			Currency=@nCurrency
			AND
			t.InsertDate BETWEEN DateAdd(d, -1, @dtInsert) AND DateAdd(d, 2, @dtInsert)
			AND
			CCard_Number256=@binCard
			AND
			IsNull(@sApprovalNumber, '') IN ('', ApprovalNumber);
	RETURN CASE @sResult WHEN '' THEN '' ELSE Right(@sResult, Len(@sResult)-2) END;
END

GO
