SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetAllTrans](@nMerchant int=0)
RETURNS TABLE
AS
RETURN
	SELECT TOP 25
		*
	FROM
		(
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID DESC) Rating,
				'Pass' AS tblName, InsertDate, ID, companyID, TransactionTypeID, Amount, Currency, CreditType, Payments, PaymentMethodDisplay,
				DebitCompanyID, PaymentMethod_id, '000' AS replyCode, PaymentMethod, DeniedStatus, isTestOnly, PayID
			FROM
				dbo.tblCompanyTransPass
			WHERE
				@nMerchant IN (NULL, CompanyID)
			UNION SELECT
				ROW_NUMBER() OVER (ORDER BY ID DESC) Rating,
				'Fail' AS tblName, InsertDate, ID, companyID, TransactionTypeID, Amount, Currency, CreditType, Payments, PaymentMethodDisplay,
				DebitCompanyID, PaymentMethod_id, replyCode, PaymentMethod, 0 AS DeniedStatus, isTestOnly, '' AS PayID
			FROM
				dbo.tblCompanyTransFail
			WHERE
				@nMerchant IN (0, CompanyID)
		) g
	WHERE
		Rating<=25
	ORDER BY
		InsertDate DESC
GO
