SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[FilterCharsAlphaNumeric](@sText nvarchar(max)) RETURNS nvarchar(max)
AS
BEGIN
	RETURN dbo.FilterChars(@sText, 'abcdefghijklmnopqrstuvwxyz1234567890-., |', '');
END
GO
