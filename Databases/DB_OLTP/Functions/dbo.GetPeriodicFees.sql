SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetPeriodicFees](@nMerchantID int, @bAnnual bit=0)
RETURNS TABLE
AS
RETURN
	SELECT
		f.TypeID,
		t.Name,
		t.Amount,
		c.CUR_ISOName CurrencyISO,
		f.IsActive,
		IsNull(f.NextDate, GetDate()) NextDate,
		IsNull(t.FeeLimit, 0) FeeLimit,
		CASE WHEN FeeLimit IS NULL THEN '' ELSE IsNull(b.GD_Text, 'Unknown '+LTrim(RTrim(Str(t.Behavior)))) END BehaviorTitle,
		CASE WHEN FeeLimit IS NULL THEN '' ELSE IsNull(b.GD_Description, '') END BehaviorDescription
	FROM
		tblPeriodicFeeType t
		LEFT JOIN GetGlobalData(33) b ON t.Behavior=b.GD_ID
		INNER JOIN tblSystemCurrencies c ON t.CurrencyID=c.CUR_ID
		INNER JOIN tblPeriodicFee f ON t.ID=f.TypeID
	WHERE
		f.MerchantID=@nMerchantID
		AND
		(@bAnnual IS NULL OR t.IsAnnual=@bAnnual)
		AND
		t.IsActive=1
GO
