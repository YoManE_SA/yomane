SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantCustomizationSkins](@nMerchant int) RETURNS varchar(100)
AS
BEGIN
	DECLARE @s varchar(100);
	SET @s='';
	SELECT @s=@s+LTrim(RTrim(Str(SkinID)))+',' FROM tblMerchantCustomization WHERE MerchantID=@nMerchant ORDER BY SkinID;
	IF @s<>'' SET @s=Left(@s, Len(@s)-1);
	RETURN @s;
END
GO
