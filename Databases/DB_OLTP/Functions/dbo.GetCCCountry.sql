SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCCCountry](@sCardNumber nvarchar(25))
RETURNS char(2) AS
BEGIN
	RETURN 
	IsNull((
		SELECT TOP 1
			isoCode
		FROM
			tblCreditCardBin
		WHERE
			Replace(@sCardNumber, ' ', '') like bin+'%'
		ORDER BY
			BINLen DESC
	), '--')
END
GO
