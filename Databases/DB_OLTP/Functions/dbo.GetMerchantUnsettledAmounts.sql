SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantUnsettledAmounts](@nYear AS int)
RETURNS TABLE
AS
RETURN
	SELECT
		CompanyID Merchant,
		Currency,
		Sum(UnsettledAmount*CASE CreditType WHEN 0 THEN -1 ELSE 1 END) Total
	FROM
		tblCompanyTransPass
	WHERE
		PaymentMethod>=15
		AND
		Year(InsertDate)=2008
	GROUP BY
		CompanyID,
		Currency
GO
