SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetEpaMatchingTransactions](@nID int)
RETURNS @tblTrans TABLE(ID int, CompanyID int, ApprovalNumber nvarchar(50), DebitReferenceCode nvarchar(40), InsertDate datetime)
AS
BEGIN
	DECLARE
		@dtInsert datetime,
		@sDebitRefCode nvarchar(40),
		@sApprovalNumber nvarchar(50),
		@nAmount money,
		@nCurrency int,
		@sTerminalNumber nvarchar(50),
		@binCardNumber256 varbinary(200),
		@nInstallment int,
		@bRefund bit,
		@dtTrans datetime,
		@dtPayout datetime,
		@sCardNumber varchar(50);
	SELECT
		@dtInsert=InsertDate,
		@sDebitRefCode=DebitRefCode,
		@sApprovalNumber=ApprovalNumber,
		@nAmount=Amount,
		@nCurrency=Currency,
		@sTerminalNumber=TerminalNumber,
		@binCardNumber256=CardNumber256,
		@nInstallment=Installment,
		@bRefund=IsRefund,
		@dtTrans=TransDate,
		@dtPayout=PayoutDate
	FROM
		tblEpaPending WITH (NOLOCK)
	WHERE
		ID=@nID;
	IF IsNull(@sDebitRefCode, '')<>''
	BEGIN
		INSERT INTO
			@tblTrans(ID, CompanyID, ApprovalNumber, DebitReferenceCode, InsertDate)
		SELECT
			ID, CompanyID, ApprovalNumber, DebitReferenceCode, InsertDate
		FROM
			tblCompanyTransPass WITH (NOLOCK)
		WHERE
			DebitReferenceCode=@sDebitRefCode
			AND SIGN(CreditType)=1-@bRefund AND (@dtTrans IS NULL OR InsertDate BETWEEN DATEADD(day, -2, @dtTrans) and DATEADD(day, 2, @dtTrans));
		RETURN;
	END
	--try match by other details
	IF IsNull(@sTerminalNumber, '')<>'' AND IsNull(@sApprovalNumber, '')<>'' AND @nCurrency IS NOT NULL AND @nAmount IS NOT NULL AND @dtTrans IS NOT NULL AND @binCardNumber256 IS NOT NULL
	BEGIN
		SET @sCardNumber=Replace(dbo.GetDecrypted256(@binCardNumber256), ' ', '');
		INSERT INTO
			@tblTrans(ID, CompanyID, ApprovalNumber, DebitReferenceCode, InsertDate)
		SELECT
			t.ID, t.CompanyID, ApprovalNumber, DebitReferenceCode, InsertDate
		FROM
			tblCompanyTransPass t WITH (NOLOCK)
			INNER JOIN tblCreditCard c WITH (NOLOCK) ON CreditCardID=c.ID
		WHERE
			TerminalNumber=@sTerminalNumber AND Currency=@nCurrency AND Amount=@nAmount
			AND (ApprovalNumber=@sApprovalNumber OR Right(ApprovalNumber, Len(@sApprovalNumber))=@sApprovalNumber)
			AND CCard_number256 IN (dbo.GetEncrypted256(@sCardNumber), dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups(@sCardNumber)))
			AND SIGN(CreditType)=1-@bRefund AND InsertDate BETWEEN DATEADD(day, -2, @dtTrans) and DATEADD(day, 2, @dtTrans);
		RETURN;
	END
	RETURN;
END
GO
