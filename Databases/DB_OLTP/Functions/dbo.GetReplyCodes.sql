SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetReplyCodes](@nFailSource TINYINT, @nDebitCompany INT=0)
RETURNS TABLE
AS
RETURN
	SELECT
		DebitCompanyID DebitCompany, Code, DescriptionOriginal Description
	FROM
		tblDebitCompanyCode
	WHERE
		FailSource=@nFailSource AND @nDebitCompany IN (0, DebitCompanyID) AND Code<>'000';

GO
