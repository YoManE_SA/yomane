SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Function [dbo].[ConvertCurrency](@Amount money, @FromCur int, @ToCur int) Returns money As
BEGIN
	RETURN 
	(
	(Select CUR_BaseRate From tblSystemCurrencies Where CUR_ID = @FromCur) 
	/
	(Select CUR_BaseRate From tblSystemCurrencies Where CUR_ID = @ToCur)
	)
	* @Amount
End

GO
