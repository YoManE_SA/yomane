SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[SetCUI](@sInput AS nvarchar(10))
RETURNS nvarchar(20)
AS
BEGIN
	IF IsNumeric(@sInput)=0 RETURN ''
	DECLARE @sResult AS nvarchar(20), @i as int
	SET @sResult=Substring('abcdefghijklmnopqrstuvwxyz', DatePart(ms, GetDate())%15+12, 1)
	SET @sResult=@sResult+Substring('abcdefghijklmnopqrstuvwxyz', DatePart(ms, GetDate())%8+19, 1)
	SET @sResult=@sResult+Substring('abcdefghijklmnopqrstuvwxyz', DatePart(ms, GetDate())%26+1, 1)
	SELECT @sInput=LTrim(RTrim(@sInput)), @i=1
	WHILE @i<=Len(@sInput) SELECT @sResult=@sResult+Substring('blackhorse', Cast(Substring(@sInput, @i, 1) AS int)+1, 1), @i=@i+1
	RETURN @sResult
END
GO
