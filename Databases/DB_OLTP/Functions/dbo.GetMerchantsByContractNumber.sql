SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantsByContractNumber](@sContractNumber varchar(20))
RETURNS TABLE
AS
RETURN
	SELECT DISTINCT
		dt_ContractNumber, m.ID, CompanyName
	FROM
		tblCompany m
		INNER JOIN tblCompanyTransPass t ON m.ID=CompanyID
		INNER JOIN tblDebitTerminals d ON t.TerminalNumber=d.TerminalNumber AND t.DebitCompanyID=d.DebitCompany
	WHERE
		dt_ContractNumber LIKE '%'+@sContractNumber;
GO
