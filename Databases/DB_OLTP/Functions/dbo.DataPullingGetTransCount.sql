SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DataPullingGetTransCount](@nMerchant int, @dtFrom datetime=NULL, @dtTo datetime=NULL, @nTransType int=15, @nTransID int=0)
RETURNS int
AS
BEGIN
	IF IsNull(@nTransID, 0)>0
	BEGIN
		SELECT @dtTo=DateAdd(HOUR, 1, InsertDate) FROM tblCompanyTransPass WHERE ID=@nTransID;
		SET @dtFrom=DateAdd(HOUR, -2, @dtTo);
	END

	IF @dtTo IS NULL SET @dtTo=GetDate();
	IF @dtFrom IS NULL SET @dtFrom=DateAdd(d, -1, @dtTo);

	DECLARE @sTransTypes varchar(100);
	SET @sTransTypes=';';
	IF @nTransType%2>=1 SET @sTransTypes=@sTransTypes+'Debit;';
	IF @nTransType%4>=2 SET @sTransTypes=@sTransTypes+'Refund;';
	IF @nTransType%8>=4 SET @sTransTypes=@sTransTypes+'CHB;';
	IF @nTransType%16>=8 SET @sTransTypes=@sTransTypes+'Retrieval;';

	IF @nTransID=0 SET @nTransID=NULL;

	DECLARE @nCount int;
	SELECT
		@nCount=Count(*)
	FROM
		tblCompanyTransPass
	WHERE
		CompanyID=@nMerchant AND InsertDate>=@dtFrom AND InsertDate<=@dtTo
		AND CharIndex(CASE WHEN dbo.IsCHB(DeniedStatus, isTestOnly)=1 THEN 'CHB' WHEN DeniedStatus IN (3, 5) THEN 'Retrieval' WHEN CreditType>0 THEN 'Debit' ELSE 'Refund' END, @sTransTypes)>0
		AND IsNull(@nTransID, ID)=ID
	RETURN IsNull(@nCount, 0)
END
GO
