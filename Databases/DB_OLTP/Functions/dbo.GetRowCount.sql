SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetRowCount](@sTable nvarchar(1000))
RETURNS int
AS
BEGIN
	DECLARE @s nvarchar(1000), @n int;
	SELECT @n=row_count FROM sys.dm_db_partition_stats WHERE object_id=(SELECT TOP 1 object_id FROM sys.objects WHERE name=@sTable AND index_id<2);
	RETURN @n;
END
GO
