SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantBalance](@nMerchant int, @nCurrency int)
RETURNS money
AS
BEGIN
	RETURN
		IsNull((SELECT TOP 1 BalanceCurrent FROM tblCompanyBalance WHERE company_id=@nMerchant AND currency=@nCurrency ORDER BY companyBalance_id DESC), 0)
END
GO
