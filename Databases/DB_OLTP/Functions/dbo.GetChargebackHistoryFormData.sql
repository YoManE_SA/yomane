SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Merchant	Currency	Month	Year	SaleCountVisa	SaleAmountVisa	ChbCountVisa	ChbAmountVisa	SaleCountMC	SaleAmountMC	ChbCountMC	ChbAmountMC
CREATE FUNCTION [dbo].[GetChargebackHistoryFormData](@nMerchant int, @nCurrency int, @nMonthLast int, @nYearLast int)
RETURNS TABLE
AS
RETURN
	SELECT
		CompanyName Merchant,
		CUR_ISOName Currency,
		DateName(month, '2000'+Right('0'+LTrim(RTrim(Str(@nMonthLast))), 2)+'01') [Month],
		@nYearLast [Year],
		d.*,
		@nMonthLast MonthID
	FROM
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN GetChargebackHistoryFormDataMonth(@nMerchant, @nCurrency, @nMonthLast, @nYearLast) d
	WHERE
		m.ID=@nMerchant and CUR_ID=@nCurrency
	UNION SELECT
		CompanyName Merchant,
		CUR_ISOName Currency,
		DateName(month, '2000'+Right('0'+LTrim(RTrim(Str(CASE (11+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (11+@nMonthLast)%12 END))), 2)+'01') [Month],
		@nYearLast-CASE WHEN @nMonthLast<2 THEN 1 ELSE 0 END [Year],
		d.*,
		CASE (11+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (11+@nMonthLast)%12 END MonthID
	FROM
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN GetChargebackHistoryFormDataMonth(@nMerchant, @nCurrency, CASE (11+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (11+@nMonthLast)%12 END, @nYearLast-CASE WHEN @nMonthLast<2 THEN 1 ELSE 0 END) d
	WHERE
		m.ID=@nMerchant and CUR_ID=@nCurrency
	UNION SELECT
		CompanyName Merchant,
		CUR_ISOName Currency,
		DateName(month, '2000'+Right('0'+LTrim(RTrim(Str(CASE (10+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (10+@nMonthLast)%12 END))), 2)+'01') [Month],
		@nYearLast-CASE WHEN @nMonthLast<3 THEN 1 ELSE 0 END [Year],
		d.*,
		CASE (10+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (10+@nMonthLast)%12 END MonthID
	FROM
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN GetChargebackHistoryFormDataMonth(@nMerchant, @nCurrency, CASE (10+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (10+@nMonthLast)%12 END, @nYearLast-CASE WHEN @nMonthLast<3 THEN 1 ELSE 0 END) d
	WHERE
		m.ID=@nMerchant and CUR_ID=@nCurrency
	UNION SELECT
		CompanyName Merchant,
		CUR_ISOName Currency,
		DateName(month, '2000'+Right('0'+LTrim(RTrim(Str(CASE (9+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (9+@nMonthLast)%12 END))), 2)+'01') [Month],
		@nYearLast-CASE WHEN @nMonthLast<4 THEN 1 ELSE 0 END [Year],
		d.*,
		CASE (9+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (9+@nMonthLast)%12 END MonthID
	FROM
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN GetChargebackHistoryFormDataMonth(@nMerchant, @nCurrency, CASE (9+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (9+@nMonthLast)%12 END, @nYearLast-CASE WHEN @nMonthLast<4 THEN 1 ELSE 0 END) d
	WHERE
		m.ID=@nMerchant and CUR_ID=@nCurrency
	UNION SELECT
		CompanyName Merchant,
		CUR_ISOName Currency,
		DateName(month, '2000'+Right('0'+LTrim(RTrim(Str(CASE (8+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (8+@nMonthLast)%12 END))), 2)+'01') [Month],
		@nYearLast-CASE WHEN @nMonthLast<5 THEN 1 ELSE 0 END [Year],
		d.*,
		CASE (8+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (8+@nMonthLast)%12 END MonthID
	FROM
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN GetChargebackHistoryFormDataMonth(@nMerchant, @nCurrency, CASE (8+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (8+@nMonthLast)%12 END, @nYearLast-CASE WHEN @nMonthLast<5 THEN 1 ELSE 0 END) d
	WHERE
		m.ID=@nMerchant and CUR_ID=@nCurrency

	UNION SELECT
		CompanyName Merchant,
		CUR_ISOName Currency,
		DateName(month, '2000'+Right('0'+LTrim(RTrim(Str(CASE (7+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (7+@nMonthLast)%12 END))), 2)+'01') [Month],
		@nYearLast-CASE WHEN @nMonthLast<6 THEN 1 ELSE 0 END [Year],
		d.*,
		CASE (7+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (7+@nMonthLast)%12 END MonthID
	FROM
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN GetChargebackHistoryFormDataMonth(@nMerchant, @nCurrency, CASE (7+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (7+@nMonthLast)%12 END, @nYearLast-CASE WHEN @nMonthLast<6 THEN 1 ELSE 0 END) d
	WHERE
		m.ID=@nMerchant and CUR_ID=@nCurrency
	UNION SELECT
		CompanyName Merchant,
		CUR_ISOName Currency,
		DateName(month, '2000'+Right('0'+LTrim(RTrim(Str(CASE (6+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (6+@nMonthLast)%12 END))), 2)+'01') [Month],
		@nYearLast-CASE WHEN @nMonthLast<7 THEN 1 ELSE 0 END [Year],
		d.*,
		CASE (6+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (6+@nMonthLast)%12 END MonthID
	FROM
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN GetChargebackHistoryFormDataMonth(@nMerchant, @nCurrency, CASE (6+@nMonthLast)%12 WHEN 0 THEN 12 ELSE (6+@nMonthLast)%12 END, @nYearLast-CASE WHEN @nMonthLast<7 THEN 1 ELSE 0 END) d
	WHERE
		m.ID=@nMerchant and CUR_ID=@nCurrency
GO
