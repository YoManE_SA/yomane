SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetChargebacksSettlement](@dtFrom datetime, @dtTo datetime, @nSettlement int) RETURNS TABLE AS RETURN
select
	count(*) Chargebacks,
	sum(Amount) ChbAmount,
	Sum(netpayFee_ratioCharge) ChbProcessingFee,
	Sum(netpayFee_transactionCharge) ChbTransactionCharge,
	sum(netpayFee_chbCharge) ChbCharge
from
	tblCompanyTransPass t with (nolock)
where
	t.denieddate between @dtFrom and @dtTo
	and dbo.IsCHB(DeniedStatus, isTestOnly)=1 and PrimaryPayedID=@nSettlement
GO
