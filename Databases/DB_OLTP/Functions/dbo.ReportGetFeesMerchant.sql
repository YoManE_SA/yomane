SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetFeesMerchant](@dtFrom datetime, @dtTo datetime, @merchantId int)
RETURNS TABLE
AS
RETURN
	select
		CompanyName GroupName,
		CUR_ISOName CurrencyName,
		pm.Name PaymentMethodName,
		FeeTransAuth,
		FeeTransFail,
		FeeTransPass,
		FeeTransPassRatio,
		FeeTransPassChargeback,
		FeeTransPassClarification,
		FeeTransPassHandling
	from
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN List.PaymentMethod pm WITH (NOLOCK)
		LEFT JOIN ReportGetFeesPassMerchant(@dtFrom, @dtTo, @merchantId) p ON CUR_ISOName=p.CurrencyName and pm.Name=p.PaymentMethodName
		LEFT JOIN ReportGetFeesFailMerchant(@dtFrom, @dtTo, @merchantId) f ON CUR_ISOName=f.CurrencyName and pm.Name=f.PaymentMethodName
		LEFT JOIN ReportGetFeesAuthMerchant(@dtFrom, @dtTo, @merchantId) a ON CUR_ISOName=a.CurrencyName and pm.Name=a.PaymentMethodName
	where
		m.ID=@merchantId;
GO
