SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetTransFailAmount]
(
	@nMerchant int,
	@nHours int,
	@nReplySource int=-1,
	@nPaymentMethod int=0,
	@nCurrency money,
	@nRule int=0,
	@sCardNumber nvarchar(50)=''
)
RETURNS money
AS
BEGIN
	DECLARE @nAmount money, @dtStart datetime, @binCardNumberSolid varbinary(200), @binCardNumberNormalized varbinary(200);
	
	SET @sCardNumber = LTrim(RTrim(dbo.fnFormatCcNumToGroups(IsNull(@sCardNumber, ''))));
	SET @binCardNumberNormalized = dbo.GetEncrypted256(@sCardNumber);
	SET @sCardNumber = Replace(@sCardNumber,' ','');
	SET @binCardNumberSolid = dbo.GetEncrypted256(@sCardNumber);
	
	IF @nRule>0 SELECT @dtStart=Max(fcbl_UnblockDate) FROM tblFraudCCBlackList WHERE fcbl_CCRMID=@nRule AND (@sCardNumber='' OR (fcbl_ccNumber256 IN (@binCardNumberNormalized, @binCardNumberSolid)));
	IF @dtStart>=GetDate() RETURN 0;
	IF @dtStart IS NULL OR @dtStart<DateAdd(hh, -@nHours, GetDate()) SET @dtStart=DateAdd(hh, -@nHours, GetDate())
	
	SELECT
		@nAmount = Sum(t.Amount)
	FROM
		tblCompanyTransFail AS t WITH (NOLOCK)
		INNER JOIN tblCreditCard AS c WITH (NOLOCK) ON t.CreditCardID = c.ID
		INNER JOIN tblDebitCompanyCode AS rc WITH (NOLOCK) ON t.DebitCompanyID = rc.DebitcompanyID AND t.replyCode = rc.Code
	WHERE
		t.InsertDate >= @dtStart
		AND
		t.CompanyID = @nMerchant
		AND
		rc.FailSource = CASE @nReplySource WHEN -1 THEN rc.FailSource ELSE @nReplySource END
		AND
		t.Currency = @nCurrency
		AND
		t.PaymentMethod = CASE @nPaymentMethod WHEN 0 THEN t.PaymentMethod ELSE @nPaymentMethod END
		AND
		c.CCard_Number256 IN (CASE @sCardNumber WHEN '' THEN c.CCard_Number256 ELSE @binCardNumberNormalized END, CASE @sCardNumber WHEN '' THEN c.CCard_Number256 ELSE @binCardNumberSolid END);
	RETURN IsNull(@nAmount, 0);
END

GO
