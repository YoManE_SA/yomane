SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantFeeGrades](@nMerchant int)
RETURNS TABLE
AS
RETURN
	SELECT
		CFF_ID ID,
		CFF_TotalTo TotalMin,
		CFF_Precent FeePercent
	FROM
		tblCompanyFeesFloor
	WHERE
		CFF_CompanyID=@nMerchant
GO
