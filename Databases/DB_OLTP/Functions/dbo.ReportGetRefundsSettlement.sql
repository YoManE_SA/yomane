SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetRefundsSettlement](@dtFrom datetime, @dtTo datetime, @nSettlement int) RETURNS TABLE AS RETURN
select
	count(*) Refunds,
	Sum(Amount) RefundsAmount,
	Sum(netpayFee_ratioCharge) RefundsProcessingFee,
	Sum(netpayFee_transactionCharge) RefundsTransactionCharge	
from
	tblCompanyTransPass t with (nolock)
where
	t.insertdate between @dtFrom and @dtTo
	and t.CreditType=0 and dbo.IsCHB(DeniedStatus, isTestOnly)=0 and t.paymentMethod > 15 and PrimaryPayedID=@nSettlement
GO
