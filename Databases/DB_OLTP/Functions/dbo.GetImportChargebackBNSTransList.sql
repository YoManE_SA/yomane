SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetImportChargebackBNSTransList](@sCard varchar(30), @sDate varchar(30), @sAmount varchar(30), @sCurrency varchar(30), @nDebitCompany int=0, @sApprovalNumber nvarchar(50), @sMRF varchar(30))
RETURNS varchar(300)
AS
BEGIN
	DECLARE @sResult varchar(300); SET @sResult='';
	DECLARE @binCard as varbinary(200); SET @binCard=dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups(@sCard));
	DECLARE @dtInsert as datetime; SET @dtInsert=Convert(datetime, @sDate, 104);
	DECLARE @nAmount as money; SET @nAmount=Convert(money, Replace(Replace(@sAmount, '.', ''), ',', '.'));
	DECLARE @nCurrency as int; SET @nCurrency=IsNull((SELECT CUR_ID FROM tblSystemCurrencies WHERE CUR_ISOName=@sCurrency), 0);
	DECLARE @sID varchar(10);
	IF LTrim(RTrim(IsNull(@sMRF, '')))<>''
		SELECT
			@sResult=@sResult+','+LTrim(RTrim(Str(ID)))
		FROM
			tblCompanyTransPass WITH (NOLOCK)
		WHERE
			DeniedStatus IN (0, 3, 9, 11)
			AND
			DebitReferenceCode IN (@sMRF, @sMRF+'_01')
	ELSE
		SELECT
			@sResult=@sResult+','+LTrim(RTrim(Str(t.ID)))
		FROM
			tblCompanyTransPass t WITH (NOLOCK)
			INNER JOIN tblCreditCard c WITH (NOLOCK) ON CreditCardID=c.ID
		WHERE
			DeniedStatus IN (0, 3, 9, 11)
			AND
			IsTestOnly=0
			AND
			@nDebitCompany IN (0, DebitCompanyID)
			AND
			Amount=@nAmount
			AND
			Currency=@nCurrency
			AND
			InsertDate BETWEEN DateAdd(d, -1, @dtInsert) AND DateAdd(d, 2, @dtInsert)
			AND
			CCard_Number256=@binCard
			AND
			IsNull(@sApprovalNumber, '') IN ('', ApprovalNumber);
	RETURN CASE @sResult WHEN '' THEN '' ELSE Right(@sResult, Len(@sResult)-1) END;
END

GO
