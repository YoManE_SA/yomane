SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetVerifyTransaction](@sMerchantNumber char(7), @nTransaction int) RETURNS int
AS
BEGIN
	DECLARE @nMerchant int;
	SELECT @nMerchant=ID FROM tblCompany WITH (NOLOCK) WHERE CustomerNumber=@sMerchantNumber;
	IF @nMerchant IS NULL RETURN -11;
	IF EXISTS (SELECT ID FROM tblCompanyTransPass WITH (NOLOCK) WHERE CompanyID=@nMerchant AND ID=@nTransaction) RETURN 1;
	IF EXISTS (SELECT ID FROM tblCompanyTransApproval WITH (NOLOCK) WHERE CompanyID=@nMerchant AND ID=@nTransaction) RETURN 3;
	RETURN -12;
END
GO
