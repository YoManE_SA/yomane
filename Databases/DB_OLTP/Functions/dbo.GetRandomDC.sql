SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetRandomDC](@nSec int) RETURNS varchar(2) AS
BEGIN
	RETURN
		CASE CAST(@nSec*16.0/60.0 AS INT)+1
			WHEN 1 THEN '1'
			WHEN 2 THEN '3'
			WHEN 3 THEN '8'
			WHEN 4 THEN '9'
			WHEN 5 THEN '11'
			WHEN 6 THEN '18'
			WHEN 7 THEN '19'
			WHEN 8 THEN '20'
			WHEN 9 THEN '18'
			WHEN 10 THEN '24'
			WHEN 11 THEN '25'
			WHEN 12 THEN '26'
			WHEN 13 THEN '28'
			WHEN 14 THEN '30'
			WHEN 15 THEN '32'
			ELSE '34'
		END;
END;
GO
