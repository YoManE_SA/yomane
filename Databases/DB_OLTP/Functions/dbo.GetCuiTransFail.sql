SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCuiTransFail](@nID AS int)
RETURNS nvarchar(10)
AS
BEGIN
	RETURN
	(
		SELECT
			dbo.GetCui(cc_cui)
		FROM
			tblCreditCard
			INNER JOIN tblCompanyTransFail ON tblCreditCard.ID=PaymentMethodID
		WHERE
			tblCompanyTransFail.ID=@nID
	)
END

GO
