SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetEncrypted]
    (
      @nKeyID [INT] ,
      @sText NVARCHAR(1000)
    )
RETURNS VARBINARY(1500)
AS 
    BEGIN
        RETURN [dbo].[GetEncryptedInternal](@nKeyID, @sText)
    END
GO
