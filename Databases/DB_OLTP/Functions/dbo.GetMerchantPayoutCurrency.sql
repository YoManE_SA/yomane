SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantPayoutCurrency](@nMerchant int, @nCurrency int=-1)
RETURNS int
AS
BEGIN
	IF EXISTS (SELECT * FROM tblMerchantBankAccount WHERE mba_Merchant=@nMerchant AND mba_Currency=@nCurrency) RETURN @nCurrency;
	RETURN (SELECT PaymentReceiveCurrency FROM tblCompany WHERE ID=@nMerchant)
END
GO
