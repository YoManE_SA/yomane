SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetTerminalFirstTrans](@sTerminalNumber varchar(20), @sTerminalName nvarchar(100)='')
RETURNS TABLE
AS
RETURN
	SELECT
		@sTerminalNumber TerminalNumber,
		(SELECT Min(InsertDate) FROM tblCompanyTransPass WHERE TerminalNumber=@sTerminalNumber) FirstPass,
		(SELECT Min(InsertDate) FROM tblCompanyTransFail WHERE TerminalNumber=@sTerminalNumber) FirstFail,
		(SELECT Min(InsertDate) FROM tblCompanyTransApproval WHERE TerminalNumber=@sTerminalNumber) FirstAuth,
		@sTerminalName TerminalName
GO
