SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RemoveDomainFromUsername](@sUsername varchar(100)) RETURNS varchar(50)
AS
BEGIN
	RETURN
		CASE
			WHEN CharIndex('\', @sUsername)>0 THEN Right(@sUsername, Len(@sUsername)-CharIndex('\', @sUsername))
			ELSE @sUsername
		END
END
GO
