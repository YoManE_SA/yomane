SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetReplyCodeCount](@nDebitCompany int, @nAttempts int, @sReplyCodes nvarchar(100), @dtCheck datetime=NULL)
RETURNS int
AS
BEGIN
	SET @dtCheck=IsNull(@dtCheck, GetDate())
	DECLARE @nCount int, @nFail int, @sReplyCode nvarchar(50), @dtInsertDate datetime
	SELECT @nCount=0, @nFail=0
	DECLARE curReplies CURSOR FOR
		SELECT
			InsertDate, ReplyCode
		FROM
			tblCompanyTransPass
		WHERE
			DebitCompanyID=@nDebitCompany AND InsertDate BETWEEN DateAdd(d, -1, @dtCheck) AND @dtCheck
		UNION
		SELECT
			InsertDate, ReplyCode
		FROM
			tblCompanyTransFail
		WHERE
			DebitCompanyID=@nDebitCompany AND InsertDate BETWEEN DateAdd(d, -1, @dtCheck) AND @dtCheck
		ORDER BY
			InsertDate DESC
	OPEN curReplies
	FETCH NEXT FROM curReplies INTO @dtInsertDate, @sReplyCode
	WHILE @@FETCH_STATUS=0 AND @nCount<@nAttempts
	BEGIN
		SET @nCount=@nCount+1
		IF ';'+@sReplyCodes+';' LIKE '%;'+@sReplyCode+';%' SET @nFail=@nFail+1
		FETCH NEXT FROM curReplies INTO @dtInsertDate, @sReplyCode
	END
	CLOSE curReplies
	DEALLOCATE curReplies
	RETURN @nFail
END
GO
