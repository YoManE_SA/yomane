SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[GetUnprocessedRefundRequestsWithCardNumber](@nDebitCompany int=0)  
RETURNS TABLE  
AS  
RETURN  
      SELECT DISTINCT TOP 100 PERCENT  
            TransID,  
            CAST(t.InsertDate AS smalldatetime) TransDate,  
            t.TerminalNumber,  
            dt_name TerminalName,  
            dt_Descriptor Descriptor,  
            dt_ContractNumber ContractNumber,  
            t.DebitReferenceCode MRF,  
            CCard_First6 BIN,  
            BINCountry Country,  
            PaymentMethodDisplay CardType,  
            dbo.fnFormatCcNumToHideDigits(dbo.GetDecrypted256(CCard_Number256)) CardNumber,  
            ExpMM MM,  
            ExpYY YY,  
            Member CardHolder,  
            email HolderEmail,  
            ApprovalNumber ApprovalCode,  
            phoneNumber HolderPhone,  
            r.id RefundRequestID,  
            RefundAskDate RequestDate,  
            RefundAskAmount RequestAmount,  
            CUR_ISOName RequestCurrency,  
            ral_date RequestAttemptDate,  
            ral_description RequestAttemptResult,  
            RefundAskStatus, DebitCompany, t.ID TID,  
            r.companyID InternalMID  
  
      FROM  
            dbo.tblRefundAsk r WITH (NOLOCK)  
            inner join dbo.tblSystemCurrencies c WITH (NOLOCK) on r.RefundAskCurrency=c.CUR_ID   
            inner join dbo.tblCompanyTransPass t WITH (NOLOCK) on r.transID=t.ID  
            inner join dbo.tblCreditCard cc WITH (NOLOCK) on t.CreditCardID=cc.ID  
            left join dbo.tblRefundAskLog l WITH (NOLOCK) on r.id=l.refundAsk_id AND l.refundAskLog_id=(SELECT MAX(refundAskLog_id) FROM dbo.tblRefundAskLog WHERE refundAsk_id=r.id)  
            left join dbo.tblDebitTerminals d WITH (NOLOCK) on t.TerminalNumber=d.terminalNumber and t.DebitCompanyID=d.DebitCompany  
      WHERE  
            @nDebitCompany IN (0, DebitCompanyID)  
      ORDER BY  
            r.id DESC,  
            ral_date DESC

GO
