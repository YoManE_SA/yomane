SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetStringBeforeUnderscore](@sText nvarchar(max))
RETURNS nvarchar(max)
AS
BEGIN
	IF @sText IS NULL RETURN NULL;
	DECLARE @nPosition int;
	SET @nPosition=CharIndex('_', @sText);
	IF @nPosition<1 RETURN @sText;
	IF @nPosition=1 RETURN '';
	RETURN Left(@sText, @nPosition-1);
END
GO
