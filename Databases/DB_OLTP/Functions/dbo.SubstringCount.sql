SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[SubstringCount](@sSearchFor nvarchar(1000), @sSearchIn nvarchar(4000))
RETURNS int
AS
BEGIN
	IF @sSearchFor IS NULL OR @sSearchIn IS NULL RETURN 0;
	IF @sSearchFor='' OR @sSearchIn='' RETURN 0;
	IF Len(@sSearchFor)>Len(@sSearchIn) RETURN 0;
	IF @sSearchFor=@sSearchIn RETURN 1;
	IF Len(@sSearchFor)=Len(@sSearchIn) RETURN 0;
	DECLARE @i int, @n int;
	SELECT @i=0, @n=0;
	WHILE @i<=Len(@sSearchIn)-Len(@sSearchFor)+1
	BEGIN
		IF SubString(@sSearchIn, @i, Len(@sSearchFor))=@sSearchFor SET @n=@n+1;
		SET @i=@i+1;
	END;
	RETURN @n;
END
GO
