SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[IsPasswordValid](@sPwd nvarchar(50))
RETURNS bit
AS
BEGIN
	SET @sPwd=IsNull(@sPwd, '')
	IF Len(@sPwd)<8 RETURN 0
	IF @sPwd NOT LIKE '%[0-9]%[0-9]%' RETURN 0
	IF @sPwd NOT LIKE '%[A-Za-z]%[A-Za-z]%' RETURN 0
	RETURN 1
END
GO
