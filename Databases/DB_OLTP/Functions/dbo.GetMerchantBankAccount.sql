SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantBankAccount](@nMerchant int, @nCurrency int)
RETURNS TABLE
AS
RETURN
	SELECT TOP 1 * FROM
	(
		SELECT
			mba_Merchant, mba_Currency,
			mba_ABA, mba_ABA2,
			mba_AccountName, mba_AccountName2,
			mba_AccountNumber, mba_AccountNumber2,
			mba_BankAddress, mba_BankAddress2,
			mba_BankAddressCity, mba_BankAddressCity2,
			mba_BankAddressCountry, mba_BankAddressCountry2,
			mba_BankAddressSecond, mba_BankAddressSecond2,
			mba_BankAddressState, mba_BankAddressState2,
			mba_BankAddressZip, mba_BankAddressZip2,
			mba_BankName, mba_BankName2,
			mba_IBAN, mba_IBAN2,
			mba_SepaBic, mba_SepaBic2,
			mba_SortCode, mba_SortCode2,
			mba_SwiftNumber, mba_SwiftNumber2
		FROM
			tblMerchantBankAccount
		WHERE
			mba_Merchant=@nMerchant AND mba_Currency=@nCurrency
		UNION SELECT
			ID, -1,
			PaymentAbroadABA, PaymentAbroadABA2,
			PaymentAbroadAccountName, PaymentAbroadAccountName2,
			PaymentAbroadAccountNumber, PaymentAbroadAccountNumber2,
			PaymentAbroadBankAddress, PaymentAbroadBankAddress2,
			PaymentAbroadBankAddressCity, PaymentAbroadBankAddressCity2,
			PaymentAbroadBankAddressCountry, PaymentAbroadBankAddressCountry2,
			PaymentAbroadBankAddressSecond, PaymentAbroadBankAddressSecond2,
			PaymentAbroadBankAddressState, PaymentAbroadBankAddressState2,
			PaymentAbroadBankAddressZip, PaymentAbroadBankAddressZip2,
			PaymentAbroadBankName, PaymentAbroadBankName2,
			PaymentAbroadIBAN, PaymentAbroadIBAN2,
			PaymentAbroadSepaBic, PaymentAbroadSepaBic2,
			PaymentAbroadSortCode, PaymentAbroadSortCode2,
			PaymentAbroadSwiftNumber, PaymentAbroadSwiftNumber2
		FROM
			tblCompany
		WHERE
			ID=@nMerchant
	) c
	ORDER BY mba_Currency DESC
GO
