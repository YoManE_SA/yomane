SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantBankAccountList](@nMerchant int)
RETURNS TABLE
AS
	RETURN
		SELECT
			-1 ID,
			'<span class="itemExists">Default ('
			+IsNull((SELECT CUR_ISOName FROM tblSystemCurrencies WITH (NOLOCK) WHERE CUR_ID=(SELECT PaymentReceiveCurrency FROM tblCompany WHERE ID=@nMerchant)), '')
			+')</span>' Name
		UNION SELECT
			c.CUR_ID,
			'<span class="'+CASE WHEN mba_Merchant IS NULL THEN 'itemNotExists' ELSE 'itemExists' END +'">'+c.CUR_ISOName+'</span>'
		FROM
			tblSystemCurrencies c WITH (NOLOCK)
			LEFT JOIN tblMerchantBankAccount a WITH (NOLOCK) ON c.CUR_ID=a.mba_Currency and mba_Merchant=@nMerchant;
GO
