SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetRecurringSeriesFromTransPass](@nTransID int)
RETURNS int AS
BEGIN
	IF @nTransID<1 RETURN 0;
	DECLARE @nSeries int;
	SET @nSeries=
	(
		SELECT TOP 1
			rc_Series
		FROM
			tblRecurringCharge WITH (NOLOCK)
			INNER JOIN tblRecurringAttempt WITH (NOLOCK) ON tblRecurringCharge.ID=tblRecurringAttempt.ra_Charge
		WHERE
			ra_TransPass=@nTransID
	)
	RETURN IsNull(@nSeries, 0);
END
GO
