SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RecurringGetChargeStringECheck](@nCharge AS int)
RETURNS nvarchar(1000)
AS
BEGIN
	RETURN
	(
		SELECT
			'CompanyNum='+CustomerNumber+
			'&RequestSource=20'+
			'&TransType=0'+
			'&accountName='+dbo.UrlEncode(accountName)+
			'&accountNumber='+Replace(dbo.GetDecrypted256(accountNumber256), ' ', '')+
			'&routingNumber='+Replace(dbo.GetDecrypted256(routingNumber256), ' ', '')+
			'&bankAccountType='+LTrim(RTrim(Str(bankAccountTypeId)))+
			'&bankName='+dbo.UrlEncode(bankName)+
			'&bankPhone='+dbo.UrlEncode(bankPhone)+
			'&bankCity='+dbo.UrlEncode(bankCity)+
			'&bankState='+LTrim(RTrim(dbo.UrlEncode(bankState)))+
			'&CreditType=Regular'+
			'&BirthDate='+dbo.UrlEncode(birthDate)+
			'&TypeCredit=1'+
			'&Payments=1'+
			'&Amount='+LTrim(RTrim(Cast(rc_Amount AS nvarchar(10))))+
			'&TransCurrency='+LTrim(RTrim(Str(rc_Currency)))+
			'&ClientIP='+''+
			'&Email='+dbo.UrlEncode(Email)+
			'&PersonalNum='+dbo.UrlEncode(PersonalNumber)+
			'&PhoneNumber='+dbo.UrlEncode(PhoneNumber)+
			'&Comment='+dbo.UrlEncode(dbo.RecurringGetAttemptComment(tblRecurringCharge.ID))+
			'&BillingAddress1='+dbo.UrlEncode(IsNull(Address1, ''))+
			'&BillingAddress2='+dbo.UrlEncode(IsNull(Address2, ''))+
			'&BillingCity='+dbo.UrlEncode(IsNull(tblBillingAddress.City, ''))+
			'&BillingState='+CASE StateID WHEN 0 THEN '' ELSE IsNull(LTrim(RTrim(Str(StateID))), '') END+
			'&BillingCountry='+IsNull(LTrim(RTrim(Str(CountryID))), '')+
			'&BillingZipCode='+dbo.UrlEncode(IsNull(ZipCode, ''))+ 
			'&TrmCode='+LTrim(RTrim(Str(IsNull(rs_ReqTrmCode, 0))))+
			'&RecurringCharge='+LTrim(RTrim(Str(@nCharge)))+
			'&RecurringIdentifier='+IsNull(rs_Identifier, '')+
			'&RecurringSeries='+LTrim(RTrim(Str(rc_Series)))
		FROM
			tblRecurringCharge
			INNER JOIN tblRecurringSeries ON tblRecurringCharge.rc_Series=tblRecurringSeries.ID
			INNER JOIN tblCheckDetails ON tblRecurringCharge.rc_ECheck=tblCheckDetails.ID
			INNER JOIN tblCompany ON tblCheckDetails.CompanyID=tblCompany.ID
			LEFT JOIN tblBillingAddress ON tblCheckDetails.BillingAddressID=tblBillingAddress.ID
		WHERE
			tblRecurringCharge.ID=@nCharge
	)
END
GO
