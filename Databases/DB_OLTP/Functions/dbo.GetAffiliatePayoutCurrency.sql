SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetAffiliatePayoutCurrency](@nAffiliate int, @nCurrency int=-1)
RETURNS int
AS
BEGIN
	IF EXISTS (SELECT * FROM tblAffiliateBankAccount WHERE aba_Affiliate=@nAffiliate AND aba_Currency=@nCurrency) RETURN @nCurrency;
	RETURN IsNull((SELECT PaymentReceiveCurrency FROM tblAffiliates WHERE affiliates_id=@nAffiliate), CASE @nCurrency WHEN -1 THEN NULL ELSE @nCurrency END);
END

GO
