SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RecurringGetText](@sLang char(3), @nIntervalUnit int, @nIntervalLength int, @nChargeCount int)
RETURNS nvarchar(100)
AS
BEGIN
	RETURN
		CASE @sLang
			WHEN 'eng' THEN
				LTrim(RTrim(Str(@nChargeCount)))+' charges, '+
				CASE @nIntervalUnit
					WHEN 1 THEN CASE @nIntervalLength WHEN 1 THEN 'daily' ELSE 'every '+LTrim(RTrim(Str(@nIntervalLength)))+' days' END
					WHEN 2 THEN CASE @nIntervalLength WHEN 1 THEN 'weekly' ELSE 'every '+LTrim(RTrim(Str(@nIntervalLength)))+' weeks' END
					WHEN 3 THEN CASE @nIntervalLength WHEN 1 THEN 'monthly' ELSE 'every '+LTrim(RTrim(Str(@nIntervalLength)))+' months' END
					WHEN 4 THEN CASE @nIntervalLength WHEN 1 THEN 'yearly' ELSE 'every '+LTrim(RTrim(Str(@nIntervalLength)))+' years' END
					WHEN 5 THEN CASE @nIntervalLength WHEN 1 THEN 'quarterly' ELSE 'every '+LTrim(RTrim(Str(@nIntervalLength)))+' quarters' END
					ELSE ''
				END
			WHEN 'fre' THEN
				LTrim(RTrim(Str(@nChargeCount)))+' paiements, '+
				CASE @nIntervalUnit
					WHEN 1 THEN CASE @nIntervalLength WHEN 1 THEN 'journalier' ELSE 'chaque '+LTrim(RTrim(Str(@nIntervalLength)))+' jours' END
					WHEN 2 THEN CASE @nIntervalLength WHEN 1 THEN 'chaque semaine' ELSE 'chaque '+LTrim(RTrim(Str(@nIntervalLength)))+' semaine' END
					WHEN 3 THEN CASE @nIntervalLength WHEN 1 THEN 'chaque moi' ELSE 'chaque '+LTrim(RTrim(Str(@nIntervalLength)))+' mois' END
					WHEN 4 THEN CASE @nIntervalLength WHEN 1 THEN 'chaque ann&#233;e' ELSE 'chaque '+LTrim(RTrim(Str(@nIntervalLength)))+' ann&#233;es' END
					WHEN 5 THEN CASE @nIntervalLength WHEN 1 THEN 'chaque quartere' ELSE 'chaque '+LTrim(RTrim(Str(@nIntervalLength)))+' quarteres' END
					ELSE ''
				END
			WHEN 'heb' THEN
				'תשלום אחד מדי '+
				CASE @nIntervalUnit
					WHEN 1 THEN CASE @nIntervalLength WHEN 1 THEN 'יום' WHEN 2 THEN 'יומיים' ELSE LTrim(RTrim(Str(@nIntervalLength)))+' ימים' END
					WHEN 2 THEN CASE @nIntervalLength WHEN 1 THEN 'שבוע' WHEN 2 THEN 'שבועיים' ELSE LTrim(RTrim(Str(@nIntervalLength)))+' שבועות' END
					WHEN 3 THEN CASE @nIntervalLength WHEN 1 THEN 'חודש' WHEN 2 THEN 'חודשיים' ELSE LTrim(RTrim(Str(@nIntervalLength)))+' חודשים' END
					WHEN 4 THEN CASE @nIntervalLength WHEN 1 THEN 'שנה' WHEN 2 THEN 'שנתיים' ELSE LTrim(RTrim(Str(@nIntervalLength)))+' שנים' END
					WHEN 5 THEN CASE @nIntervalLength WHEN 1 THEN 'רבעון' ELSE LTrim(RTrim(Str(@nIntervalLength)))+' רבעונים' END
					ELSE ''
				END
				+', סה"כ '+LTrim(RTrim(Str(@nChargeCount)))+' תשלומים'
			WHEN 'spa' THEN
				LTrim(RTrim(Str(@nChargeCount)))+' cargos, '+
				CASE @nIntervalUnit
					WHEN 1 THEN CASE @nIntervalLength WHEN 1 THEN 'diario' ELSE 'cada '+LTrim(RTrim(Str(@nIntervalLength)))+' d&#237;as' END
					WHEN 2 THEN CASE @nIntervalLength WHEN 1 THEN 'semanal' ELSE 'cada '+LTrim(RTrim(Str(@nIntervalLength)))+' semanas' END
					WHEN 3 THEN CASE @nIntervalLength WHEN 1 THEN 'mensual' ELSE 'cada '+LTrim(RTrim(Str(@nIntervalLength)))+' meses' END
					WHEN 4 THEN CASE @nIntervalLength WHEN 1 THEN 'anual' ELSE 'cada '+LTrim(RTrim(Str(@nIntervalLength)))+' a&#241;os' END
					WHEN 5 THEN CASE @nIntervalLength WHEN 1 THEN 'quarter' ELSE 'cada '+LTrim(RTrim(Str(@nIntervalLength)))+' quarteros' END
					ELSE ''
				END
			ELSE
				''
		END
END
GO
