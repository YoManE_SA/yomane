SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetUnprocessedRefundRequests](@nDebitCompany int=0)
RETURNS TABLE
AS
RETURN
(
	SELECT DISTINCT TOP 100 PERCENT
		transID,
		t.InsertDate,
		t.terminalNumber,
		dt_name,
		dt_Descriptor,
		dt_ContractNumber,
		t.DebitReferenceCode DebitRef,
		CCard_First6,
		BINCountry,
		PaymentMethodDisplay,
		ExpMM,
		ExpYY,
		Member,
		email,
		phoneNumber,
		r.id,
		RefundAskDate,
		RefundAskAmount,
		CUR_ISOName,
		ral_date,
		ral_description
	FROM
		dbo.tblRefundAsk r WITH (NOLOCK)
		inner join dbo.tblSystemCurrencies c WITH (NOLOCK) on r.RefundAskCurrency=c.CUR_ID 
		inner join dbo.tblCompanyTransPass t WITH (NOLOCK) on r.transID=t.ID
		inner join dbo.tblCreditCard cc WITH (NOLOCK) on t.CreditCardID=cc.ID
		left join dbo.tblRefundAskLog l WITH (NOLOCK) on r.id=l.refundAsk_id AND l.refundAskLog_id=(SELECT MAX(refundAskLog_id) FROM dbo.tblRefundAskLog WHERE refundAsk_id=r.id)
		left join dbo.tblDebitTerminals d WITH (NOLOCK) on t.TerminalNumber=d.terminalNumber and t.DebitCompanyID=d.DebitCompany
	WHERE
		@nDebitCompany IN (0, DebitCompanyID)
		and
		RefundAskStatus=0
	ORDER BY
		r.id DESC,
		ral_date DESC
)
GO
