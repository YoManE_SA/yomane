SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[InvoiceGetUnattendedLines]
(
	@nMerchant int,
	@sBillTo nvarchar(100),
	@nCurrency int
)
RETURNS TABLE
AS
RETURN
	SELECT TOP 100 PERCENT
		ROW_NUMBER() OVER (ORDER BY tblInvoiceLine.ID) LineNumber,
		tblInvoiceLine.*,
		tblSystemCurrencies.CUR_ISOName,
		tblSystemCurrencies.CUR_BaseRate,
		Cast(tblInvoiceLine.il_Quantity*il_Price*CUR_BaseRate/IsNull((SELECT TOP 1 CUR_BaseRate FROM tblSystemCurrencies WHERE CUR_ID=@nCurrency), 0) AS money) AmountConverted
	FROM
		tblInvoiceLine
		INNER JOIN tblSystemCurrencies ON tblInvoiceLine.il_Currency=tblSystemCurrencies.CUR_ID
	WHERE
		IsNull(il_DocumentID, 0)=0
		AND
		(
			(@nMerchant>0 AND il_MerchantID=@nMerchant)
			OR
			(@sBillTo<>'' AND il_BillToName=@sBillTo)
		)
	ORDER BY
		tblInvoiceLine.ID
GO
