SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[AutoCaptureGetChargeString](@nTrans AS int)
RETURNS nvarchar(1000)
AS
BEGIN
	RETURN
	(
		SELECT
			'TransType=2&RequestSource=29'
			+'&CompanyNum='+CustomerNumber
			+'&TransApprovalID='+LTrim(RTrim(Str(@nTrans)))
			+'&Amount='+LTrim(RTrim(Cast(Amount AS nvarchar(10))))
			+'&Currency='+LTrim(RTrim(Str(t.Currency)))
			+'&ClientIP='+IsNull(IPAddress, '')
			+'&Comment='+LTrim(IsNull(t.Comment, '')+' AutoCapture')+LTrim(RTrim(Str(@nTrans)))
		FROM
			tblCompanyTransApproval t
			INNER JOIN tblCompany m ON t.CompanyID=m.ID
		WHERE
			t.ID=@nTrans AND t.PaymentMethod BETWEEN 20 AND 99 AND t.TransAnswerID IS NULL
	)
END
GO
