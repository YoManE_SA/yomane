SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetAffiliateBankAccountList](@nAffiliate int)
RETURNS TABLE
AS
	RETURN
		SELECT
			-1 ID,
			'<span class="itemExists">Default ('
			+IsNull((SELECT CUR_ISOName FROM tblSystemCurrencies WITH (NOLOCK) WHERE CUR_ID=(SELECT PaymentReceiveCurrency FROM tblAffiliates WHERE affiliates_id=@nAffiliate)), '')
			+')</span>' Name
		UNION SELECT
			c.CUR_ID,
			'<span class="'+CASE WHEN aba_Affiliate IS NULL THEN 'itemNotExists' ELSE 'itemExists' END +'">'+c.CUR_ISOName+'</span>'
		FROM
			tblSystemCurrencies c WITH (NOLOCK)
			LEFT JOIN tblAffiliateBankAccount a WITH (NOLOCK) ON c.CUR_ID=a.aba_Currency and aba_Affiliate=@nAffiliate;

GO
