SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[RecurringGetNotificationInfo] ( @nChargeID INT )
RETURNS TABLE
AS
	RETURN
    SELECT  ISNULL(CASE ra.ra_TransPass
                     WHEN 0 THEN ra.ra_TransPending
                     ELSE ra.ra_TransPass
                   END, 0) AS FirstTransID ,
            rs.ID AS SeriesID ,
            rs.rs_ChargeCount AS ChargeCount ,
            rcThis.rc_ChargeNumber AS ChargeNumber ,
            CASE WHEN rs.rs_Blocked = 1 THEN 2 --series blocked
                 WHEN rcThis.rc_Blocked = 1 THEN 1 --charge blocked
                 ELSE 0
            END AS RecurringStatus --nothing blocked
    FROM    tblRecurringSeries AS rs
            INNER JOIN tblRecurringCharge rcThis ON rs.ID = rcThis.rc_Series
            INNER JOIN tblRecurringCharge rcFirst ON rs.ID = rcFirst.rc_Series
            INNER JOIN tblRecurringAttempt AS ra ON rcFirst.ID = ra.ra_Charge
    WHERE   rcFirst.rc_ChargeNumber = 1
            AND rcThis.ID = @nChargeID;
GO
