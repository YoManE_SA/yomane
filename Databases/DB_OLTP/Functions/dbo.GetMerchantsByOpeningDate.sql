SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantsByOpeningDate](@dtFrom datetime=NULL, @dtTo datetime=NULL)
RETURNS TABLE
AS
RETURN
	SELECT
		ID, companyname Name, merchantOpenningDate [Open Date], GD_Text [Status]
	FROM
		tblCompany m INNER JOIN GetGlobalData(44) s ON ActiveStatus=GD_ID
	WHERE
		merchantOpenningDate BETWEEN IsNull(@dtFrom, Convert(datetime, LTrim(RTrim(Str(Year(GetDate()))))+'-01-01', 120)) AND ISNULL(@dtTo, GetDate());
GO
