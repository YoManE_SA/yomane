SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetPendingRefundRequests](@nMerchant int, @nStatus int=0)
RETURNS TABLE
AS
RETURN
	SELECT
		TransID,
		t.Amount,
		c.CUR_ISOName Currency,
		t.InsertDate TransactionDate,
		t.ApprovalNumber,
		t.DebitReferenceCode,
		RefundAskAmount RefundRequestAmount,
		cr.CUR_ISOName RefundRequestCurrency,
		RefundAskDate RefundRequestDate,
		RefundAskComment RefundRequestComment
	FROM
		tblRefundAsk r
		INNER JOIN tblCompanyTransPass t ON transID=t.ID 
		INNER JOIN tblSystemCurrencies c ON Currency=c.CUR_ID
		INNER JOIN tblSystemCurrencies cr ON RefundAskCurrency=cr.CUR_ID
	WHERE
		r.companyID=@nMerchant
		AND
		RefundAskStatus=@nStatus
GO
