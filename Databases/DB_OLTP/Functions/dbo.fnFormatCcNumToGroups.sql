SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnFormatCcNumToGroups](@CardNum as nvarchar(25))
RETURNS nvarchar(25)
AS
BEGIN
	SET @CardNum = LTrim(RTrim(Replace(@CardNum, ' ', '')))
	WHILE Left(@CardNum, 1) = '0' AND Len(@CardNum) > 8 SET @CardNum = Right(@CardNum, Len(@CardNum)-1);
	IF Len(@CardNum) < 8 OR Len(@CardNum) > 19 RETURN ''
	SET @CardNum = Left(@CardNum+Space(19), 19)
	RETURN RTrim(Left(@CardNum, 4)+' '+Substring(@CardNum, 5, 4)+' '+Substring(@CardNum, 9, 4)+' '+Substring(@CardNum, 13, 4)+' '+Substring(@CardNum, 17, 4))
END

GO
