SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetLastFailTerminalNumber](@nDebitCompany int, @sReplyCodes nvarchar(100))
RETURNS nvarchar(10)
AS
BEGIN
	DECLARE @nTransID int
	SELECT
		@nTransID=Max(ID)
	FROM
		tblCompanyTransFail
	WHERE
		DebitCompanyID=@nDebitCompany
		AND
		';'+@sReplyCodes+';' LIKE '%;'+replyCode+';%'
	IF IsNull(@nTransID, 0)<=0 RETURN ''
	RETURN (SELECT TerminalNumber FROM tblCompanyTransFail WHERE ID=@nTransID)
END
GO
