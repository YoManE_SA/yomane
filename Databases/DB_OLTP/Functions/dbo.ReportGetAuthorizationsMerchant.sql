SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[ReportGetAuthorizationsMerchant](@dtFrom datetime, @dtTo datetime, @merchantId int) RETURNS TABLE AS RETURN
select
	CompanyName GroupName,
	CUR_ISOName CurrencyName,
	count(*) Authorizations,
	sum(Amount) AuthorizationsAmount
from
	tblCompany m
	full join tblSystemCurrencies c on c.CUR_ID>-1
	inner join tblCompanyTransApproval t with (nolock) on m.id=t.CompanyID and c.CUR_ID=t.currency
where
	t.insertdate between @dtFrom and @dtTo and m.id=@merchantId
group by
	CompanyName, CUR_ISOName

GO
