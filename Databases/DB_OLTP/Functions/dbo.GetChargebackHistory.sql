SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetChargebackHistory](@nTransPassID int)
RETURNS TABLE
AS
RETURN
	--SELECT
	--	h.ID,
	--	h.TypeID,
	--	h.InsertDate,
	--	h.ReferenceNumber ReasonCode,
	--	IsNull(r.Brand+': '+r.Title, '') Title,
	--	IsNull(r.Description, '') Description,
	--	IsNull(r.MediaRequired, '') RequiredMedia,
	--	IsNull(r.IsPendingChargeback, 0) IsPendingChargeback,
	--	IsNull(r.RefundInfo, '') RefundInfo
	--FROM
	--	tblCompanyTransPass t WITH (NOLOCK)
	--	INNER JOIN tblTransactionHistory h ON h.TransPassID IN (t.ID, t.OriginalTransID)
	--	LEFT JOIN tblChargebackReason r ON h.ReferenceNumber=r.ReasonCode
	--WHERE
	--	t.ID=@nTransPassID
	--	AND h.TypeID IN (4, 6);
	SELECT
		h.TransHistory_id AS 'ID',
		h.TransHistoryType_id AS 'TypeID',
		h.InsertDate,
		h.ReferenceNumber AS 'ReasonCode',
		IsNull(r.Brand+': '+r.Title, '') AS 'Title',
		IsNull(r.Description, '') AS 'Description',
		IsNull(r.MediaRequired, '') AS 'RequiredMedia',
		IsNull(r.IsPendingChargeback, 0) AS 'IsPendingChargeback',
		IsNull(r.RefundInfo, '') AS 'RefundInfo'
	FROM
		tblCompanyTransPass t WITH (NOLOCK)
		INNER JOIN Trans.TransHistory h ON h.TransPass_id IN (t.ID, t.OriginalTransID)
		LEFT JOIN tblChargebackReason r ON h.ReferenceNumber = r.ReasonCode
	WHERE
		t.ID=@nTransPassID
		AND h.TransHistoryType_id IN (4, 6);	

GO
