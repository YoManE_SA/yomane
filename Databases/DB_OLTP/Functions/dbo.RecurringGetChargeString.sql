SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[RecurringGetChargeString] ( @nCharge AS INT )
RETURNS NVARCHAR(1000)
AS 
    BEGIN
        DECLARE @nSeries INT ,
            @nChargeFirst INT ,
            @sOrderNumber NVARCHAR(100);

        SELECT  @nSeries = rc_Series
        FROM    tblRecurringCharge
        WHERE   ID = @nCharge;

        IF ISNULL(@nSeries, 0) = 0 
            RETURN '';

        SELECT  @nChargeFirst = ID
        FROM    tblRecurringCharge
        WHERE   rc_Series = @nSeries
                AND rc_ChargeNumber = 1;

        IF ISNULL(@nChargeFirst, 0) = 0 
            RETURN '';

        SELECT  @sOrderNumber = OrderNumber
        FROM    tblCompanyTransPass
                INNER JOIN tblRecurringAttempt ON tblCompanyTransPass.ID = ra_TransPass
        WHERE   ra_Charge = @nChargeFirst;
        RETURN
	(
		SELECT
			'CompanyNum='+CustomerNumber+
			'&RequestSource=20'+
			'&Order='+dbo.UrlEncode(ISNULL(@sOrderNumber, ''))+
			'&TransType='+LTRIM(RTRIM(STR(rs_Approval)))+
			'&CardNum='+REPLACE(dbo.GetDecrypted256(CCard_Number256), ' ', '')+
			'&ExpMonth='+RIGHT('00'+LTRIM(RTRIM(ExpMM)), 2)+
			'&ExpYear='+RIGHT('00'+LTRIM(RTRIM(ExpYY)), 2)+
			'&Member='+dbo.UrlEncode(Member)+
			'&TypeCredit=1'+
			'&Payments=1'+
			'&Amount='+LTRIM(RTRIM(CAST(rc.rc_Amount AS NVARCHAR(10))))+
			'&Currency='+LTRIM(RTRIM(STR(rc.rc_Currency)))+
			'&CVV2='+ISNULL(dbo.GetCUI(cc_cui),'')+
			'&ClientIP='+dbo.UrlEncode(rs.rs_IP)+
			'&Email='+dbo.UrlEncode(Email)+
			'&PersonalNum='+dbo.UrlEncode(PersonalNumber)+
			'&PhoneNumber='+dbo.UrlEncode(PhoneNumber)+
			'&Comment='+dbo.UrlEncode(dbo.RecurringGetAttemptComment(rc.ID))+
			'&BillingAddress1='+dbo.UrlEncode(ISNULL(Address1, ''))+
			'&BillingAddress2='+dbo.UrlEncode(ISNULL(Address2, ''))+
			'&BillingCity='+dbo.UrlEncode(ISNULL(tblBillingAddress.City, ''))+
			'&BillingState='+ISNULL(CASE StateID WHEN 0 THEN '' ELSE ISNULL(LTRIM(RTRIM(STR(tblBillingAddress.StateID))), '') END, '')+
			'&BillingCountry='+ISNULL(CASE ISNULL(tblBillingAddress.CountryID, 0) WHEN 0 THEN cc.BINCountry ELSE CAST(tblBillingAddress.CountryID AS VARCHAR(3)) END, '')+
			'&BillingZipCode='+dbo.UrlEncode(ISNULL(ZipCode, ''))+
			'&TrmCode='+LTRIM(RTRIM(STR(ISNULL(rs.rs_ReqTrmCode, 0))))+
			'&RecurringCharge='+LTRIM(RTRIM(STR(@nCharge)))+
			'&RecurringIdentifier='+ISNULL(rs.rs_Identifier, '')+
			'&RecurringSeries='+LTRIM(RTRIM(STR(rc.rc_Series)))
		FROM
			tblRecurringCharge AS rc
			INNER JOIN tblRecurringSeries AS rs ON rc.rc_Series=rs.ID
			INNER JOIN tblCreditCard AS cc ON rc.rc_CreditCard=cc.ID
			INNER JOIN tblCompany ON rs.rs_Company=tblCompany.ID
			LEFT JOIN tblBillingAddress ON cc.BillingAddressID=tblBillingAddress.ID
		WHERE
			rc.ID=@nCharge
	)
    END

GO
