SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[CheckCreditCardWhitelist]
(
	@nMerchant int,
	@sCardNumber nvarchar(25),
	@nExpMonth tinyint,
	@nExpYear smallint
)
RETURNS TABLE
AS
RETURN
	SELECT TOP 1
		ID,
		ccwl_Level [Level],
		ccwl_CardHolder [HolderName]
	FROM
		tblCreditCardWhitelist WITH (NOLOCK)
	WHERE
		ccwl_Merchant = @nMerchant
		AND
		ccwl_ExpMonth = @nExpMonth
		AND
		ccwl_ExpYear = @nExpYear+CASE WHEN @nExpYear<100 THEN 2000 ELSE 0 END
		AND
		ccwl_CardNumber256 = dbo.GetEncrypted256(Replace(@sCardNumber,' ',''))
		AND
		ccwl_IsBurnt = 0;

GO
