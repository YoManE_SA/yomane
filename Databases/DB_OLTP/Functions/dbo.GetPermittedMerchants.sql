SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetPermittedMerchants](@sUsername nvarchar(50))
RETURNS table
AS
RETURN
(
	SELECT
		sum_Merchant ID
	FROM
		tblSecurityUserMerchant WITH (NOLOCK) INNER JOIN tblSecurityUser WITH (NOLOCK) ON sum_User=tblSecurityUser.ID
	WHERE
		su_Username=@sUsername
)
GO
