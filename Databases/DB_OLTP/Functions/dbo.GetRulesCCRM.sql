
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[GetRulesCCRM](@nMerchant int, @nPaymentMethod smallint=0, @nCurrency smallint=-1, @nCreditType tinyint=255, @nWhitelistLevel smallint=-1, @fromVT bit = 0)
RETURNS TABLE
AS
RETURN
	SELECT
		*
	FROM
		tblCreditCardRiskManagement
	WHERE
		(@fromVT = 0 Or (@fromVT = 1 AND CCRM_ApplyVT <> 0))
		AND
		CCRM_ReplySource=-1
		AND
		CCRM_CompanyID=@nMerchant
		AND
		CCRM_PaymentMethod IN (0, @nPaymentMethod)
		AND
		CCRM_Currency IN (-1, @nCurrency)
		AND
		CCRM_CreditType IN (255, @nCreditType)
		AND
		IsNull(CCRM_WhitelistLevel, -1)>=@nWhitelistLevel
		AND
		CCRM_IsActive = 1;     

GO
