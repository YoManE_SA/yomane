SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetSalesSettlement](@dtFrom datetime, @dtTo datetime, @nSettlement int)
RETURNS TABLE
AS
RETURN
	SELECT
		Count(*) Sales,
		Sum(Amount) SalesAmount,
		Sum(netpayFee_ratioCharge) SalesProcessingFee,
		Sum(netpayFee_transactionCharge) SalesTransactionCharge	
	FROM
		tblCompanyTransPass t WITH (NOLOCK)
	WHERE
		t.insertdate>@dtFrom AND t.insertdate<@dtTo
		AND t.CreditType>0 AND (IsChargeback=0 OR OriginalTransID=0) AND PrimaryPayedID=@nSettlement
		AND isTestOnly=0 AND t.PaymentMethod>=20
GO
