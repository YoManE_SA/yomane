SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetImportChargebackJCCMerchantList](@sDate varchar(30), @sApprovalNumber varchar(50), @sTerminalNumber nvarchar(50))
RETURNS varchar(300)
AS
BEGIN
	DECLARE @dtInsert as datetime; SET @dtInsert=Convert(datetime, @sDate, 3);
	DECLARE @nCount int;
	SELECT
		@nCount=Count(DISTINCT tblCompanyTransPass.CompanyID)
	FROM
		tblCompanyTransPass
	WHERE
		DeniedStatus IN (0, 3, 9, 11)
		AND
		IsTestOnly=0
		AND
		DebitCompanyID=19
		AND
		InsertDate BETWEEN DateAdd(d, -1, @dtInsert) AND DateAdd(d, 2, @dtInsert)
		AND
		LTrim(RTrim(ApprovalNumber))=LTrim(RTrim(@sApprovalNumber));
	IF @nCount<1 RETURN '';
	IF @nCount=1 RETURN
	(
		SELECT
			'<'+'a href="merchant_data.asp?companyID='+LTrim(RTrim(Str(ID)))+'" target="_blank">'+CompanyName+'<'+'/a>'
		FROM
			tblCompany
		WHERE
			ID IN
			(
				SELECT TOP 1
					tblCompanyTransPass.CompanyID
				FROM
					tblCompanyTransPass
				WHERE
					DeniedStatus IN (0, 3, 9, 11)
					AND
					IsTestOnly=0
					AND
					DebitCompanyID=19
					AND
					InsertDate BETWEEN DateAdd(d, -1, @dtInsert) AND DateAdd(d, 2, @dtInsert)
					AND
					LTrim(RTrim(ApprovalNumber))=LTrim(RTrim(@sApprovalNumber))
			)
	);
	DECLARE @sResult varchar(300); SET @sResult='';
	DECLARE @sMerchant varchar(100);
	DECLARE curMerchants CURSOR FOR
		SELECT DISTINCT
			'<'+'a href="merchant_data.asp?companyID='+LTrim(RTrim(Str(tblCompany.ID)))+'" target="_blank">'+CompanyName+'<'+'/a>'
		FROM
			tblCompany
			INNER JOIN tblCompanyTransPass ON tblCompany.ID=tblCompanyTransPass.CompanyID
		WHERE
			DeniedStatus IN (0, 3, 9, 11)
			AND
			IsTestOnly=0
			AND
			DebitCompanyID=19
			AND
			tblCompanyTransPass.InsertDate BETWEEN DateAdd(d, -1, @dtInsert) AND DateAdd(d, 2, @dtInsert)
			AND
			LTrim(RTrim(ApprovalNumber))=LTrim(RTrim(@sApprovalNumber));
	OPEN curMerchants;
	FETCH NEXT FROM curMerchants INTO @sMerchant;
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @sResult=CASE @sResult WHEN '' THEN @sMerchant ELSE @sResult+', '+@sMerchant END;
		FETCH NEXT FROM curMerchants INTO @sMerchant;
	END
	CLOSE curMerchants;
	DEALLOCATE curMerchants;
	RETURN @sResult
END
GO
