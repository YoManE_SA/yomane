SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetFeesAuthMerchant](@dtFrom datetime, @dtTo datetime, @merchantId int)
RETURNS TABLE
AS
RETURN
	select
		CompanyName GroupName,
		CUR_ISOName CurrencyName,
		pm.Name PaymentMethodName,
		IsNull(Sum(IsNull(t.netpayFee_transactionCharge, 0)), 0) FeeTransAuth
	from
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN List.PaymentMethod pm WITH (NOLOCK)
		left join tblCompanyTransApproval t with (nolock) on m.id=t.companyid and c.CUR_ID=t.currency and pm.PaymentMethod_id=t.PaymentMethod and t.insertdate between @dtFrom and @dtTo
	where
		m.ID=@merchantId
	group by
		CompanyName, CUR_ISOName, pm.Name

GO
