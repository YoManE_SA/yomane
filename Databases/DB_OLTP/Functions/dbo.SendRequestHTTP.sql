SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[SendRequestHTTP] (@sURL [nvarchar] (200), @sString [nvarchar] (2000))
RETURNS [nvarchar] (max)
WITH EXECUTE AS CALLER, 
RETURNS NULL ON NULL INPUT
EXTERNAL NAME [NetPayHTTP].[NetPayHTTP].[SendRequestHTTP]
GO
