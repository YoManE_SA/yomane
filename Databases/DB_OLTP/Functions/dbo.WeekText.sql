SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[WeekText](@dtNow datetime)
RETURNS nvarchar(20)
AS
BEGIN
	DECLARE @dtFirst date, @dtLast date, @sDayFirst nvarchar(4), @sDayLast nvarchar(4), @sMonthFirst nvarchar(4), @sMonthLast nvarchar(4);
	SELECT @dtFirst=DateAdd(day, 1-DatePart(weekday, @dtNow), @dtNow), @dtLast=DateAdd(day, 7-DatePart(weekday, @dtNow), @dtNow);
	SELECT @sDayFirst=DateName(day, @dtFirst), @sMonthFirst=Left(DateName(month, @dtFirst), 3)+' ';
	SELECT @sDayLast=DateName(day, @dtLast), @sMonthLast=Left(DateName(month, @dtLast), 3)+' ';
	SET @sDayFirst=@sDayFirst+CASE @sDayFirst WHEN '1' THEN 'st' WHEN '21' THEN 'st' WHEN '31' THEN 'st' WHEN '2' THEN 'nd' WHEN '22' THEN 'nd' WHEN '3' THEN 'rd' WHEN '23' THEN 'rd' ELSE 'th' END;
	SET @sDayLast=@sDayLast+CASE @sDayLast WHEN '1' THEN 'st' WHEN '21' THEN 'st' WHEN '31' THEN 'st' WHEN '2' THEN 'nd' WHEN '22' THEN 'nd' WHEN '3' THEN 'rd' WHEN '23' THEN 'rd' ELSE 'th' END;
	IF @sMonthLast=@sMonthFirst SET @sMonthLast='';
	RETURN @sMonthFirst+@sDayFirst+' - '+@sMonthLast+@sDayLast;
END
GO
