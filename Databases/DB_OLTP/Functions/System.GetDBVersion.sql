SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [System].[GetDBVersion]()
RETURNS DECIMAL(6,2) AS
BEGIN
	RETURN
		(SELECT TOP 1 [Version] FROM [System].[DBVersion] ORDER BY [Version] DESC)
END
GO
