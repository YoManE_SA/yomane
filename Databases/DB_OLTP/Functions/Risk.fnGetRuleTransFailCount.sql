SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [Risk].[fnGetRuleTransFailCount]
(
	@nMerchant INT,
	@nHours INT,
	@nReplySource INT = -1,
	@nPaymentMethod INT = 0,
	@nCurrency SMALLINT = -1,
	@nRule INT = 0,
	@sCardNumber NVARCHAR(50) = ''
)
RETURNS INT
AS
BEGIN
	DECLARE @nCount INT, 
			@dtStart DATETIME, 
			@binCardNumberEncrypt VARBINARY(200);
	
	SET @sCardNumber = Replace(@sCardNumber,' ','');
	SET @binCardNumberEncrypt = dbo.GetEncrypted256(@sCardNumber);
	
	IF @nRule > 0 BEGIN
		SELECT	@dtStart = MAX(fcbl_UnblockDate) 
		FROM	tblFraudCCBlackList 
		WHERE	fcbl_CCRMID = @nRule AND ( @sCardNumber = '' OR fcbl_ccNumber256 = @binCardNumberEncrypt );
	END
	IF @dtStart >= GetDate() BEGIN
		RETURN 0;
	END
	IF @dtStart IS NULL OR @dtStart < DateAdd(hh,-@nHours,GetDate()) BEGIN
		SET @dtStart = DateAdd(hh,-@nHours,GetDate());
	END
    
	SELECT
		@nCount = Count(*)
	FROM
		tblCompanyTransFail AS t
		INNER JOIN Trans.TransPaymentMethod AS tpm ON t.TransPaymentMethod_id = tpm.TransPaymentMethod_id
		INNER JOIN tblDebitCompanyCode AS d ON t.DebitCompanyID = d.DebitcompanyID AND t.replyCode = d.Code
	WHERE
		t.InsertDate >= @dtStart AND
		t.CompanyID = @nMerchant AND
		d.FailSource = CASE @nReplySource WHEN -1 THEN d.FailSource ELSE @nReplySource END AND
		t.Currency = CASE @nCurrency WHEN -1 THEN t.Currency ELSE @nCurrency END AND
		t.PaymentMethod = CASE @nPaymentMethod WHEN 0 THEN t.PaymentMethod ELSE @nPaymentMethod END AND
		tpm.Value1Encrypted = CASE @sCardNumber WHEN '' THEN tpm.Value1Encrypted ELSE @binCardNumberEncrypt END;

	RETURN IsNull(@nCount, 0);
END



GO
