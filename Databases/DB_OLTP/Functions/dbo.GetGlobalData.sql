SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetGlobalData](@nGroup int)
RETURNS TABLE
AS
RETURN
(
	SELECT * FROM GetGlobalDataLang(@nGroup, 1)
)
GO
