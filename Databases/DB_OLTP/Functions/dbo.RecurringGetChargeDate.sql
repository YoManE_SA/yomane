SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RecurringGetChargeDate](@dtStart AS datetime, @nChargeNumber AS int, @nUnit AS int, @nLength AS int)
RETURNS datetime
AS
BEGIN
	-- 1=Day, 2=Week, 3=Month, 4=Year
	IF @nChargeNumber=1 RETURN Cast(Floor(Cast(@dtStart AS float)) AS datetime)
	IF @nUnit=1 RETURN Cast(Floor(Cast(DateAdd(dd, @nLength*(@nChargeNumber-1), @dtStart) AS float)) AS datetime)
	IF @nUnit=2 RETURN Cast(Floor(Cast(DateAdd(wk, @nLength*(@nChargeNumber-1), @dtStart) AS float)) AS datetime)
	IF @nUnit=3 RETURN Cast(Floor(Cast(DateAdd(mm, @nLength*(@nChargeNumber-1), @dtStart) AS float)) AS datetime)
	IF @nUnit=4 RETURN Cast(Floor(Cast(DateAdd(yy, @nLength*(@nChargeNumber-1), @dtStart) AS float)) AS datetime)
	RETURN NULL
END
GO
