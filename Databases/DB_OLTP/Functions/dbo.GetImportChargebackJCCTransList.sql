SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetImportChargebackJCCTransList](@sDate varchar(30), @sApprovalNumber nvarchar(50), @sTerminalNumber nvarchar(50))
RETURNS varchar(300)
AS
BEGIN
	DECLARE @dtInsert as datetime; SET @dtInsert=Convert(datetime, @sDate, 3);
	RETURN
	IsNull((
		SELECT TOP 1
			LTrim(RTrim(Str(tblCompanyTransPass.ID)))
		FROM
			tblCompanyTransPass WITH (NOLOCK)
		WHERE
			DeniedStatus IN (0, 3, 9, 11)
			AND
			IsTestOnly=0
			AND
			DebitCompanyID=19
			AND
			InsertDate BETWEEN DateAdd(d, -1, @dtInsert) AND DateAdd(d, 2, @dtInsert)
			AND
			LTrim(RTrim(ApprovalNumber))=LTrim(RTrim(@sApprovalNumber))
	), '');
	/*
	DECLARE @sResult varchar(300); SET @sResult='';
	DECLARE @sID varchar(10);
	DECLARE curTrans CURSOR FOR
		SELECT
			LTrim(RTrim(Str(tblCompanyTransPass.ID)))
		FROM
			tblCompanyTransPass
		WHERE
			DeniedStatus IN (0, 3, 9, 11)
			AND
			IsTestOnly=0
			AND
			DebitCompanyID=19
			AND
			InsertDate BETWEEN DateAdd(d, -1, @dtInsert) AND DateAdd(d, 2, @dtInsert)
			AND
			LTrim(RTrim(ApprovalNumber))=LTrim(RTrim(@sApprovalNumber));
	OPEN curTrans;
	FETCH NEXT FROM curTrans INTO @sID;
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @sResult=CASE @sResult WHEN '' THEN @sID ELSE @sResult+','+@sID END;
		FETCH NEXT FROM curTrans INTO @sID;
	END
	CLOSE curTrans;
	DEALLOCATE curTrans;
	RETURN @sResult
	*/
END
GO
