SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetSummarySettlement](@dtFrom datetime, @dtTo datetime, @nSettlement int)
RETURNS TABLE
AS
RETURN
	SELECT
		CASE g.RRState WHEN 1 THEN CASE g.RREnable WHEN 1 THEN (s.SalesAmount/100.0)* g.SecurityDeposit ELSE NULL END ELSE NULL END CalcRollingReserve,	
		'' [-1-],	
		DeclinesSaleGatewayAmount+DeclinesSaleRiskAmount+DeclinesSaleIssuerAmount+SalesAmount [Declined_Amount_Sales],
		DeclinesSaleGateway+DeclinesSaleRisk+DeclinesSaleIssuer+Sales [Declined_Count_Sales],	
		DeclinesTransactionCharge [Declined_Transactions_Fee],		
		'' [-2-],	
		SalesAmount [Transaction_Amount_Sales],
		Sales [Transaction_Count_Sales],
		SalesProcessingFee [Sales_Processing_Fee],
		SalesTransactionCharge [Sales_Transactions_Fee],	
		'' [-3-],
		Chargebacks [ChargeBacks_Count],
		ChbAmount [ChargeBacks_Amount],
		ChbProcessingFee [ChargeBacks_Processing_Fee],
		ChbTransactionCharge [ChargeBacks_Transactions_Fee],
		ChbCharge [ChargeBacks_Fee],		
		'' [-4-],
		Refunds [Refunds_Count],
		IsNull(RefundsAmount, 0) [Refunds_Amount],
		IsNull(RefundsProcessingFee, 0) [Refunds_Processing_Fee],
		IsNull(RefundsTransactionCharge, 0) [Refunds_Transactions_Fee],
		'' [-5-],
		RollingReserveAmount [Rolling_Reserve_Amount],
		'' [-6-],
		RollingReleaseAmount [Rolling_Release_Amount],
		'' [-7-],
		FeeTransAuth [Authorization_Fee],
		FeeTransFail [Decline_Fee],
		FeeTransPass [Transaction_Fee],
		FeeTransPassRatio [Transaction_Ratio_Fee],
		FeeTransPassChargeback [Chargeback_Fee],
		FeeTransPassClarification [Clarification_Fee],
		FeeTransPassHandling	 [Handling_Fee],
		'' [-8-],
		SalesTransactionCharge+ChbTransactionCharge+IsNull(RefundsTransactionCharge, 0) [Total_Approved_Trans_Fee],
		SalesAmount-(SalesTransactionCharge+ChbTransactionCharge+IsNull(RefundsTransactionCharge, 0))-DeclinesTransactionCharge
			-IsNull(RefundsAmount, 0)-ChbAmount-FeeTransPassChargeback-SalesProcessingFee [Net_Sales_Amount],
		SalesAmount-(SalesTransactionCharge+ChbTransactionCharge+IsNull(RefundsTransactionCharge, 0))
			-DeclinesTransactionCharge-IsNull(RefundsAmount, 0)-ChbAmount-ChbCharge
			-CASE g.RRState WHEN 1 THEN CASE g.RREnable WHEN 1 THEN (s.SalesAmount/100.0)* g.SecurityDeposit ELSE NULL END ELSE NULL END
			-SalesProcessingFee+RollingReleaseAmount [Calculated_Net_Sales_Amount]
	FROM
		tblCompany g
		CROSS JOIN ReportGetDeclinesSettlement(@dtFrom, @dtTo, @nSettlement) d
		CROSS JOIN ReportGetSalesSettlement(@dtFrom, @dtTo, @nSettlement) s
		CROSS JOIN ReportGetRefundsSettlement(@dtFrom, @dtTo, @nSettlement) r
		CROSS JOIN ReportGetChargebacksSettlement(@dtFrom, @dtTo, @nSettlement) c
		CROSS JOIN ReportGetRollingReserveSettlement(@dtFrom, @dtTo, @nSettlement) rr
		CROSS JOIN ReportGetRollingReleaseSettlement(@dtFrom, @dtTo, @nSettlement) rrl	
		CROSS JOIN ReportGetFeesSettlement(@dtFrom, @dtTo, @nSettlement) f
	WHERE
		g.ID=(SELECT CompanyID FROM tblTransactionPay WHERE ID=@nSettlement)
GO
