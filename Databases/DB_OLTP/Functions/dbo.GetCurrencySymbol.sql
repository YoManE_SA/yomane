SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCurrencySymbol](@nID AS int)
RETURNS nvarchar(10)
AS
BEGIN
	RETURN
	(
		SELECT
			CUR_Symbol
		FROM
			tblSystemCurrencies
		WHERE
			CUR_ID=@nID
	)
END
GO
