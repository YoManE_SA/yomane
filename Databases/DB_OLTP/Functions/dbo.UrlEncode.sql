SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[UrlEncode](@sString nvarchar(max))
RETURNS nvarchar(max)
AS
BEGIN
	IF @sString IS null RETURN null;
	IF @sString='' RETURN '';
	RETURN Replace(Replace(Replace(@sString, '&', '%26'), '=', '%3D'), '#', '%23')
END
GO
