SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[ReportGetDeclinesMerchant](@dtFrom datetime, @dtTo datetime, @merchantId int)
RETURNS TABLE
AS
RETURN
	select
		CompanyName GroupName,
		CUR_ISOName CurrencyName,	
		pm.Name PaymentMethodName,	
		sum(case when TransType<>1 and FailSource=2 then 1 else 0 end) DeclinesSaleGateway,
		sum(case when TransType<>1 and FailSource=1 then 1 else 0 end) DeclinesSaleRisk,
		sum(case when TransType<>1 and IsNull(FailSource, 0)=0 then 1 else 0 end) DeclinesSaleIssuer,
		sum(case when TransType<>1 and FailSource=2 then 1 else 0 end) + sum(case when TransType<>1 and FailSource=1 then 1 else 0 end) + sum(case when TransType<>1 and IsNull(FailSource, 0)=0 then 1 else 0 end) TotalSaleDeclines,	
		sum(case when TransType<>1 and FailSource=2 then Amount else 0 end) DeclinesSaleGatewayAmount,
		sum(case when TransType<>1 and FailSource=1 then Amount else 0 end) DeclinesSaleRiskAmount,
		sum(case when TransType<>1 and IsNull(FailSource, 0)=0 then Amount else 0 end) DeclinesSaleIssuerAmount,
		sum(case when TransType<>1 and FailSource=2 then Amount else 0 end) + sum(case when TransType<>1 and FailSource=1 then Amount else 0 end) + sum(case when TransType<>1 and IsNull(FailSource, 0)=0 then Amount else 0 end) TotalDeclinesSaleAmount,
		sum(netpayFee_transactionCharge) DeclinesTransactionCharge	
	from
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN List.PaymentMethod pm WITH (NOLOCK)
		INNER JOIN
		(
			select
				id, companyid, replycode, transtype, DebitCompanyID, currency, PaymentMethod, amount, netpayFee_transactionCharge
			from
				tblCompanyTransFail WITH (NOLOCK)
			where
				insertdate between @dtFrom and @dtTo and companyid = @merchantId
				and currency in (select CUR_ID from tblSystemCurrencies WITH (NOLOCK))
				and PaymentMethod in (select PaymentMethod_id from List.PaymentMethod WITH (NOLOCK))
			union select
				id, companyid, replycode, transtype, DebitCompanyID, currency, PaymentMethod, amount, netpayFee_transactionCharge
			from
				dbo.syn_archive_tblCompanyTransFail WITH (NOLOCK)
			where
				insertdate between @dtFrom and @dtTo and companyid = @merchantId
				and currency in (select CUR_ID from tblSystemCurrencies WITH (NOLOCK))
				and PaymentMethod in (select PaymentMethod_id from List.PaymentMethod WITH (NOLOCK))
		) t on m.ID=CompanyID and c.CUR_ID=Currency and pm.PaymentMethod_id=t.PaymentMethod
		left join tblDebitCompanycode rc with (nolock) on t.replycode=rc.code and t.DebitCompanyID=rc.DebitCompanyID
		Where m.ID = @merchantId
	group by
		CompanyName, CUR_ISOName, pm.Name



GO
