SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[WeekName](@dtNow datetime)
RETURNS nvarchar(20)
AS
BEGIN
	DECLARE @dtFirst date, @dtLast date, @sDayFirst nvarchar(2), @sDayLast nvarchar(2), @sMonthFirst nvarchar(2), @sMonthLast nvarchar(2), @sYearFirst nvarchar(2), @sYearLast nvarchar(2);
	SELECT @dtFirst=DateAdd(day, 1-DatePart(weekday, @dtNow), @dtNow), @dtLast=DateAdd(day, 7-DatePart(weekday, @dtNow), @dtNow);
	SELECT
		@sDayFirst=Right('00'+LTrim(RTrim(Str(DatePart(day, @dtFirst)))), 2),
		@sMonthFirst=Right('00'+LTrim(RTrim(Str(DatePart(month, @dtFirst)))), 2),
		@sYearFirst=Right('00'+LTrim(RTrim(Str(DatePart(year, @dtFirst)))), 2),
		@sDayLast=Right('00'+LTrim(RTrim(Str(DatePart(day, @dtLast)))), 2),
		@sMonthLast=Right('00'+LTrim(RTrim(Str(DatePart(month, @dtLast)))), 2),
		@sYearLast=Right('00'+LTrim(RTrim(Str(DatePart(year, @dtLast)))), 2);
	RETURN @sDayFirst+'/'+@sMonthFirst+'/'+@sYearFirst+' - '+@sDayLast+'/'+@sMonthLast+'/'+@sYearLast;
END
GO
