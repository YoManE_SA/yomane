SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetFeesPassMerchant](@dtFrom datetime, @dtTo datetime, @merchantId int)
RETURNS TABLE
AS
RETURN
	select
		CompanyName GroupName,
		CUR_ISOName CurrencyName,
		pm.Name PaymentMethodName,
		IsNull(Sum(IsNull(t.netpayFee_transactionCharge, 0)), 0) FeeTransPass,
		IsNull(Sum(IsNull(t.netpayFee_ratioCharge, 0)), 0) FeeTransPassRatio,
		IsNull(Sum(IsNull(t.netpayFee_chbCharge, 0)), 0) FeeTransPassChargeback,
		IsNull(Sum(IsNull(t.netpayFee_ClrfCharge, 0)), 0) FeeTransPassClarification,
		IsNull(Sum(IsNull(t.HandlingFee, 0)), 0) FeeTransPassHandling
	from
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN List.PaymentMethod pm WITH (NOLOCK)
		left join tblCompanyTransPass t with (nolock) on m.id=t.companyid and c.CUR_ID=t.currency and pm.PaymentMethod_id=t.PaymentMethod and t.insertdate between @dtFrom and @dtTo
	where
		m.ID=@merchantId
	group by
		CompanyName, CUR_ISOName, pm.Name

GO
