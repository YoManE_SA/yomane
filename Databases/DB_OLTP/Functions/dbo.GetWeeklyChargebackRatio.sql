SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetWeeklyChargebackRatio](@nDebitCompany int, @dtFrom datetime, @dtTo datetime)
RETURNS @tbl TABLE
(
	WeekInYear  int,
	FirstDay datetime,
	LastDay datetime,
	VisaTransactions int,
	VisaChargebacks int,
	VisaRatio AS CASE VisaTransactions WHEN 0 THEN 0 ELSE CAST(VisaChargebacks AS real)/VisaTransactions END,
	MastercardTransactions int,
	MastercardChargebacks int,
	MastercardRatio AS CASE MastercardTransactions WHEN 0 THEN 0 ELSE CAST(MastercardChargebacks AS real)/MastercardTransactions END
)
AS
BEGIN
	DECLARE @dtFirst datetime;
	SET @dtFrom=DateAdd(DAY, -(DatePart(WEEKDAY, @dtFrom)-1), @dtFrom);
	SET @dtTo=DateAdd(DAY, 7-(DatePart(WEEKDAY, @dtTo)), @dtTo);
	SET @dtFirst=LTrim(RTrim(Str(Year(@dtFrom)))+'0101');
	SET @dtFirst=DateAdd(DAY, -(DatePart(WEEKDAY, @dtFirst)-1), @dtFirst);
	INSERT INTO
		@tbl
		(
			WeekInYear,
			FirstDay,
			LastDay,
			VisaTransactions,
			MastercardTransactions,
			VisaChargebacks,
			MastercardChargebacks
		)
	SELECT
		IsNull(IsNull(IsNull(tv.WeekInYear, cv.WeekInYear), tm.WeekInYear), cm.WeekInYear) WeekInYear,
		DateAdd(DAY, 7*(IsNull(IsNull(IsNull(tv.WeekInYear, cv.WeekInYear), tm.WeekInYear), cm.WeekInYear)-1), @dtFirst) FirstDay,
		DateAdd(DAY, 7*IsNull(IsNull(IsNull(tv.WeekInYear, cv.WeekInYear), tm.WeekInYear), cm.WeekInYear)-1, @dtFirst) LastDay,
		IsNull(tv.Transactions, 0) [Visa Transactions],
		IsNull(tm.Transactions, 0) [MC Transactions],
		IsNull(cv.Chargebacks, 0) [Visa Chargebacks],
		IsNull(cm.Chargebacks, 0) [MC Chargebacks]
	FROM
		(
			SELECT
				DatePart(WEEK, InsertDate) WeekInYear, COUNT(*) Transactions
			FROM
				tblCompanyTransPass
			WHERE
				DebitCompanyID=@nDebitCompany AND PaymentMethod=22 AND InsertDate BETWEEN @dtFrom AND @dtTo AND CreditType>0 AND isTestOnly=0
				AND (dbo.IsCHB(DeniedStatus, 0)=0 or OriginalTransID=0)
			GROUP BY
				DatePart(WEEK, InsertDate)
		) tv
		FULL JOIN
		(
			SELECT
				DatePart(WEEK, InsertDate) WeekInYear, COUNT(*) Transactions
			FROM
				tblCompanyTransPass
			WHERE
				DebitCompanyID=@nDebitCompany AND PaymentMethod=25 AND InsertDate BETWEEN @dtFrom AND @dtTo AND CreditType>0 AND isTestOnly=0
				AND (dbo.IsCHB(DeniedStatus, 0)=0 or OriginalTransID=0)
			GROUP BY
				DatePart(WEEK, InsertDate)
		) tm ON tv.WeekInYear=tm.WeekInYear 
		FULL JOIN
		(
			SELECT
				DatePart(WEEK, DeniedDate) WeekInYear, COUNT(*) Chargebacks
			FROM
				tblCompanyTransPass
			WHERE
				DebitCompanyID=@nDebitCompany AND PaymentMethod=22 AND DeniedDate BETWEEN @dtFrom AND @dtTo AND dbo.IsCHB(DeniedStatus, 0)=1 AND isTestOnly=0
			GROUP BY
				DatePart(WEEK, DeniedDate)
		) cv ON tv.WeekInYear=cv.WeekInYear
		FULL JOIN
		(
			SELECT
				DatePart(WEEK, DeniedDate) WeekInYear, COUNT(*) Chargebacks
			FROM
				tblCompanyTransPass
			WHERE
				DebitCompanyID=@nDebitCompany AND PaymentMethod=25 AND DeniedDate BETWEEN @dtFrom AND @dtTo AND dbo.IsCHB(DeniedStatus, 0)=1 AND isTestOnly=0
			GROUP BY
				DatePart(WEEK, DeniedDate)
		) cm ON tv.WeekInYear=cm.WeekInYear
	ORDER BY
		FirstDay;
	RETURN
END
GO
