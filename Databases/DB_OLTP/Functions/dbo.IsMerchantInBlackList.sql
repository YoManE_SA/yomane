SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[IsMerchantInBlackList] ( @nCompanyID INT )
RETURNS BIT
AS 
    BEGIN
        RETURN (SELECT SIGN(COUNT(*)) FROM [Risk].[BlacklistMerchant] WHERE ISNULL([Merchant_id], 0)=ISNULL(@nCompanyID, 0))
    END

GO
