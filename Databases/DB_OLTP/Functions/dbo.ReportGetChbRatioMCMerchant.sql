SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetChbRatioMCMerchant](@dtFrom datetime, @dtTo datetime, @merchantId int) RETURNS TABLE AS RETURN
SELECT DISTINCT
	CompanyName GroupName,
	CUR_ISOName CurrencyName,
	ChbCount ChargebacksMC,
	ChbAmount ChbAmountMC,
	SaleCount ChargebacksBaseMC,
	SaleAmount ChbBaseAmountMC,
	CASE SaleCount WHEN 0 THEN 0 ELSE Cast(ChbCount as real)/Cast(SaleCount as real) END ChbRatioMC,
	SaleCurrentCount ChargebacksBaseCurrentMC,
	SaleCurrentAmount ChbBaseAmountCurrentMC
FROM
	tblCompany m
	full join tblSystemCurrencies c on c.CUR_ID>-1
	left join
	(
		select
			Currency,
			count(*) ChbCount,
			sum(Amount) ChbAmount
		from
			tblCompanyTransPass with (nolock)
		where
			CompanyID=@merchantId and denieddate between @dtFrom and @dtTo
			and dbo.IsCHB(DeniedStatus, isTestOnly)=1 and paymentmethod=25
		group by
			Currency
	) chb on c.CUR_ID=chb.Currency
	left join
	(
		select
			Currency,
			count(*) SaleCount,
			sum(Amount) SaleAmount
		from
			tblCompanyTransPass with (nolock)
		where
			CompanyID=@merchantId and insertdate between DateAdd(m, -1, @dtFrom) and DateAdd(m, -1, @dtTo)
			and CreditType>0 and dbo.IsCHB(DeniedStatus, isTestOnly)=0 and paymentmethod=25
		group by
			Currency
	) pass on c.CUR_ID=pass.Currency
	left join
	(
		select
			Currency,
			count(*) SaleCurrentCount,
			sum(Amount) SaleCurrentAmount
		from
			dbo.tblCompanyTransPass with (nolock)
		where
			CompanyID=@merchantId and insertdate between @dtFrom and @dtTo
			and CreditType>0 and dbo.IsCHB(DeniedStatus, isTestOnly)=0 and paymentmethod=25
		group by
			Currency
	) passCurrent on c.CUR_ID=passCurrent.Currency
WHERE
	m.ID=@merchantId
GO
