SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetAccountingTransfers]
(
	@sBank1 varchar(50),
	@sBank2 varchar(50)='',
	@sBank3 varchar(50)='',
	@sBank4 varchar(50)='',
	@sBank5 varchar(50)='',
	@sBank6 varchar(50)='',
	@sBank7 varchar(50)='',
	@sBank8 varchar(50)='',
	@sBank9 varchar(50)=''
)
RETURNS TABLE
AS
RETURN
SELECT TOP 100 PERCENT
	ROW_NUMBER() OVER (ORDER BY logMasavDetails_id) RowNumber,
	tblLogMasavDetails.logMasavDetails_id,
	tblLogMasavDetails.logMasavFile_id,
	tblLogMasavDetails.PayeeName,
	tblLogMasavDetails.PayeeBankDetails,
	tblLogMasavDetails.Amount,
	tblLogMasavDetails.Currency,
	tblLogMasavDetails.LogStatus,
	tblLogMasavDetails.SendStatus,
	tblLogMasavDetails.StatusNote,
	tblLogMasavDetails.StatusUserName,
	tblLogMasavFile.InsertDate,
	tblLogMasavFile.PayedBankCode,
	tblWireMoney.*
FROM tblLogMasavDetails
	INNER JOIN tblLogMasavFile ON tblLogMasavDetails.logMasavFile_id=tblLogMasavFile.logMasavFile_id
	INNER JOIN tblWireMoney ON tblLogMasavDetails.WireMoney_id=tblWireMoney.WireMoney_id
WHERE
	tblWireMoney.WireFlag=1
	AND
	PayedBankCode IN (@sBank1, @sBank2, @sBank3, @sBank4, @sBank5, @sBank6, @sBank7, @sBank8, @sBank9)
ORDER BY
	logMasavDetails_id
GO
