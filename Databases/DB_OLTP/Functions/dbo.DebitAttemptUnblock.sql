SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[DebitAttemptUnblock](@nDebitCompany int, @sDebitTerminal nvarchar(20), @nAttempt int, @sReplyCodes nvarchar(100))
RETURNS bit
AS
BEGIN
	DECLARE @sURL nvarchar(200), @sString nvarchar(2000), @sReply as nvarchar(2000)
	SET @sURL=(SELECT TOP 1 pc_RecurringURL FROM tblParentCompany ORDER BY ID)
	SET @sURL=Replace(Lower(@sURL), '/remote_charge.asp', '/remoteCharge_dcTest.asp')
	SET @sString='TerminalNumber='+@sDebitTerminal
	IF ';'+@sReplyCodes+';' LIKE '%;'+dbo.GetQueryStringValue('replyCode', dbo.SendRequestHTTP(@sURL, @sString))+';%' RETURN 0
	RETURN 1
END

GO
