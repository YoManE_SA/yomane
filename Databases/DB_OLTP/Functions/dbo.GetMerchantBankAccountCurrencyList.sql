SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantBankAccountCurrencyList](@nMerchant int) RETURNS varchar(100)
AS
BEGIN
	DECLARE @sCurrency char(3), @sResult varchar(100);
	DECLARE curCurrencies CURSOR FOR
		SELECT CUR_ISOName FROM tblSystemCurrencies WITH (NOLOCK) INNER JOIN tblMerchantBankAccount WITH (NOLOCK) ON CUR_ID=mba_Currency WHERE mba_Merchant=@nMerchant;
	OPEN curCurrencies
	FETCH NEXT FROM curCurrencies INTO @sCurrency
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @sResult=CASE WHEN Len(@sResult)>0 THEN @sResult+', '+@sCurrency ELSE @sCurrency END
		FETCH NEXT FROM curCurrencies INTO @sCurrency
	END
	CLOSE curCurrencies
	DEALLOCATE curCurrencies
	RETURN @sResult
END
GO
