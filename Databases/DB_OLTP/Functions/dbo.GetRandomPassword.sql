SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetRandomPassword]() RETURNS char(8)
AS
BEGIN
	DECLARE @nMS int, @sLetters as varchar(50), @sPassword nvarchar(6)
	SET @sLetters='abcdefghijklmnopqrstuvwxyz'
	SET @nMS=DatePart(ms, GetDate())
	SET @sPassword=''
	SET @sPassword=@sPassword+Substring(@sLetters, (@nMS+DatePart(ss, GetDate()))/41+1, 1)
	SET @sPassword=@sPassword+Substring(@sLetters, @nMS/40+1, 1)
	SET @sPassword=@sPassword+Substring(@sLetters, DatePart(ss, GetDate())/30+1, 1)
	SET @sPassword=@sPassword+Substring(@sLetters, (@nMS+DatePart(mi, GetDate()))/41+1, 1)
	SET @sPassword=@sPassword+Substring(@sLetters, DatePart(ss, GetDate())/3+1, 1)
	SET @sPassword=@sPassword+Substring(@sLetters, (DatePart(mi, GetDate())+DatePart(ss, GetDate()))/6+1, 1)
	IF @nMS>99 SET @nMS=@nMS/10
	IF @nMS<10 SET @nMS=@nMS*10
	RETURN @sPassword+Cast(@nMS as char(2))
END
GO
