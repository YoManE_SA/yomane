SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetRefundAmountAvailable](@nTransID int)
RETURNS money
AS
BEGIN
	RETURN
		(SELECT Amount FROM tblCompanyTransPass WHERE ID=@nTransID)
		-
		IsNull((SELECT Sum(CASE RefundAskStatus WHEN 3 THEN 0 WHEN 6 THEN 0 ELSE RefundAskAmount END) FROM tblRefundAsk WHERE TransID=@nTransID), 0);
END
GO
