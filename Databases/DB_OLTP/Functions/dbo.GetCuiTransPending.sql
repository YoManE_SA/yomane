SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCuiTransPending](@nID AS int)
RETURNS nvarchar(10)
AS
BEGIN
	RETURN
	(
		SELECT
			dbo.GetCui(cc_cui)
		FROM
			tblCreditCard
			INNER JOIN tblCompanyTransPending ON tblCreditCard.ID=PaymentMethodID
		WHERE
			tblCompanyTransPending.ID=@nID
	)
END
GO
