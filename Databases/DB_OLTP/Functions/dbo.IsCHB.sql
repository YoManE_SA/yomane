SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[IsCHB](@nStatus int, @bIsTestOnly as bit=0)  
RETURNS bit  
AS  
BEGIN  
	IF @bIsTestOnly=1 RETURN 0;  
	IF @nStatus IN (1, 2, 4, 6, 8) RETURN 1;  
	RETURN 0  
END  
GO
