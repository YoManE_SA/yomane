SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create FUNCTION [dbo].[ClearWhiteSpace] (@str varchar(4000))
RETURNS varchar(4000)
AS
BEGIN
     DECLARE @ClearWhiteSpace varchar(4000);
     SET @ClearWhiteSpace = @str
     SET @ClearWhiteSpace = ltrim(rtrim(@ClearWhiteSpace))
     -- SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(32), '')
     SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(13), '')
     SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(10), '')
     SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(9),  '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(1),  '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(2),  '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(3),  '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(4),  '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(5),  '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(6),  '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(7),  '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(8),  '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(11), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(12), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(14), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(15), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(16), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(17), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(18), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(19), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(20), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(21), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(22), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(23), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(24), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(25), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(26), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(27), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(28), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(29), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(30), '')
  SET @ClearWhiteSpace = REPLACE( @ClearWhiteSpace, CHAR(31), '')
     RETURN(@ClearWhiteSpace)
END
GO
