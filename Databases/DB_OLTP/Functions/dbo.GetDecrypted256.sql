SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetDecrypted256](@binBytes varbinary(1500))
RETURNS nvarchar(1000)
AS
BEGIN
	IF @binBytes IS NULL RETURN ''
	RETURN dbo.GetDecryptedInternal(1, @binBytes)
END
GO
