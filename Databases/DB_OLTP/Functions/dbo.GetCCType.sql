SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCCType](@sCardNumber nvarchar(25))
RETURNS int AS
BEGIN
	RETURN 
	IsNull((
		SELECT TOP 1
			CCType
		FROM
			tblCreditCardBin
		WHERE
			Replace(@sCardNumber, ' ', '') like bin+'%'
		ORDER BY
			BINLen DESC
	), 0)
END
GO
