SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetSalesMerchant](@dtFrom datetime, @dtTo datetime, @merchantId int)
RETURNS TABLE
AS
RETURN
	SELECT
		CompanyName GroupName,
		CUR_ISOName CurrencyName,	
		pm.Name PaymentMethodName,	
		Count(*) Sales,
		Sum(Amount) SalesAmount,
		Sum(netpayFee_ratioCharge) SalesProcessingFee,
		Sum(netpayFee_transactionCharge) SalesTransactionCharge	
	FROM
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN List.PaymentMethod pm WITH (NOLOCK)
		INNER JOIN tblCompanyTransPass t WITH (NOLOCK) ON m.id=t.CompanyID AND c.CUR_ID=currency and pm.PaymentMethod_id=t.PaymentMethod
	WHERE
		t.insertdate>@dtFrom AND t.insertdate<@dtTo
		AND t.CreditType>0 AND (IsChargeback=0 OR OriginalTransID=0) AND m.id=@merchantId
		AND isTestOnly=0 AND t.PaymentMethod>=20
	GROUP BY
		CompanyName, CUR_ISOName, pm.Name

GO
