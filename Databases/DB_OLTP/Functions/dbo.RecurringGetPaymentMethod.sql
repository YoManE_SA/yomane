SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[RecurringGetPaymentMethod] ( @nCharge INT )
RETURNS TABLE
AS
RETURN
    ( SELECT    rs.rs_Company ,
                rc.rc_CreditCard ,
                rc.rc_Echeck ,
                REPLACE(dbo.GetDecrypted256(CCard_Number256), ' ', '') AS CardNumber ,
                RIGHT('00' + LTRIM(RTRIM(ExpMM)), 2) ExpMonth ,
                RIGHT('00' + LTRIM(RTRIM(ExpYY)), 2) ExpYear ,
                dbo.GetCUI(cc.cc_cui) AS CUI ,
                cc.Member ,
                cc.PersonalNumber ,
                cc.PhoneNumber ,
                cc.Email ,
                ba.address1 ,
                ba.address2 ,
                ba.city ,
                ba.zipCode ,
                ba.stateId ,
                ba.countryId
      FROM      tblRecurringSeries AS rs
                INNER JOIN tblRecurringCharge AS rc ON rs.ID = rc_Series
                LEFT JOIN tblCreditCard AS cc ON rc_CreditCard = cc.ID
                LEFT JOIN tblCheckDetails AS cd ON rc_ECheck = cd.ID
                LEFT JOIN tblBillingAddress AS ba ON CASE rc_CreditCard
                                                       WHEN 0
                                                       THEN cd.BillingAddressID
                                                       ELSE cc.BillingAddressID
                                                     END = ba.ID
      WHERE     rc.ID = @nCharge
    )

GO
