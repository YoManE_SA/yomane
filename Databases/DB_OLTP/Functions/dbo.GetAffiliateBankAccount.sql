SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetAffiliateBankAccount](@nAffiliate int, @nCurrency int)
RETURNS TABLE
AS
RETURN
	SELECT TOP 1 * FROM
	(
		SELECT
			aba_Affiliate, aba_Currency,
			aba_ABA, aba_ABA2,
			aba_AccountName, aba_AccountName2,
			aba_AccountNumber, aba_AccountNumber2,
			aba_BankAddress, aba_BankAddress2,
			aba_BankAddressCity, aba_BankAddressCity2,
			aba_BankAddressCountry, aba_BankAddressCountry2,
			aba_BankAddressSecond, aba_BankAddressSecond2,
			aba_BankAddressState, aba_BankAddressState2,
			aba_BankAddressZip, aba_BankAddressZip2,
			aba_BankName, aba_BankName2,
			aba_IBAN, aba_IBAN2,
			aba_SepaBic, aba_SepaBic2,
			aba_SortCode, aba_SortCode2,
			aba_SwiftNumber, aba_SwiftNumber2
		FROM
			tblAffiliateBankAccount
		WHERE
			aba_Affiliate=@nAffiliate AND aba_Currency=@nCurrency
		UNION SELECT
			affiliates_id, -1,
			PaymentAbroadABA, PaymentAbroadABA2,
			PaymentAbroadAccountName, PaymentAbroadAccountName2,
			PaymentAbroadAccountNumber, PaymentAbroadAccountNumber2,
			PaymentAbroadBankAddress, PaymentAbroadBankAddress2,
			PaymentAbroadBankAddressCity, PaymentAbroadBankAddressCity2,
			PaymentAbroadBankAddressCountry, PaymentAbroadBankAddressCountry2,
			PaymentAbroadBankAddressSecond, PaymentAbroadBankAddressSecond2,
			PaymentAbroadBankAddressState, PaymentAbroadBankAddressState2,
			PaymentAbroadBankAddressZip, PaymentAbroadBankAddressZip2,
			PaymentAbroadBankName, PaymentAbroadBankName2,
			PaymentAbroadIBAN, PaymentAbroadIBAN2,
			PaymentAbroadSepaBic, PaymentAbroadSepaBic2,
			PaymentAbroadSortCode, PaymentAbroadSortCode2,
			PaymentAbroadSwiftNumber, PaymentAbroadSwiftNumber2
		FROM
			tblAffiliates
		WHERE
			affiliates_id=@nAffiliate
	) c
	ORDER BY aba_Currency DESC
GO
