SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCUITransPass](@nID AS int)
RETURNS nvarchar(10)
AS
BEGIN
	RETURN
	(
		SELECT
			dbo.GetCui(cc_cui)
		FROM
			tblCreditCard
			INNER JOIN tblCompanyTransPass ON tblCreditCard.ID=PaymentMethodID
		WHERE
			tblCompanyTransPass.ID=@nID
	)
END
GO
