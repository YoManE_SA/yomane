SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCreditCardWhitelist]
(
	@nMerchant int=NULL,
	@sFromDate103 varchar(25)=NULL,
	@sToDate103 varchar(25)=NULL,
	@nIsMerchant int=NULL,
	@nIsBurnt int=NULL,
	@nID int=NULL,
	@nLevel int=NULL,
	@sUsername varchar(50)=NULL,
	@nPaymentMethod int=NULL,
	@nBin int=NULL,
	@nLast4 smallint=NULL,
	@sCardHolder nvarchar(100)=NULL,
	@sCardNumber nvarchar(25)=NULL,
	@nMonthFrom tinyint=NULL,
	@nYearFrom smallint=NULL,
	@nMonthTo tinyint=NULL,
	@nYearTo smallint=NULL
)
RETURNS TABLE
AS
RETURN
	SELECT
		w.ID,
		ccwl_Merchant Merchant,
		IsNull(m.CompanyName, '') MerchantName,
		ccwl_PaymentMethod PaymentMethod,
		IsNull(p.GD_Text, '') PaymentMethodName,
		Right('000'+LTrim(RTrim(Str(ccwl_Last4))), 4) Last4,
		ccwl_Bin Bin,
		ccwl_BinCountry BinCountry,
		IsNull(c.Name, '') BinCountryName,
		ccwl_CardHolder CardHolder,
		Right('0'+LTrim(RTrim(Str(ccwl_ExpMonth))), 2) ExpMonth,
		ccwl_ExpYear ExpYear,
		ccwl_IsBurnt IsBurnt,
		IsNull(ccwl_BurnDate, ccwl_InsertDate) BurnDate,
		ccwl_Level RecordLevel,
		IsNull(l.GD_Text, '') RecordLevelName,
		IsNull(l.GD_Color, '') RecordLevelColor,
		ccwl_Comment Comment,
		ccwl_InsertDate InsertDate,
		dbo.RemoveDomainFromUsername(ccwl_Username) Username,
		ccwl_IP IP
	FROM
		tblCreditCardWhitelist w WITH (NOLOCK)
		left join tblCompany m WITH (NOLOCK) ON ccwl_Merchant=m.ID
		left join GetGlobalData(42) p ON ccwl_PaymentMethod=p.GD_ID
		left join GetGlobalData(39) l ON ccwl_Level=l.GD_ID
		left join list.countrylist c ON ccwl_BinCountry=c.CountryISOCode
	WHERE
		(ccwl_Merchant=0 OR IsNull(@nMerchant, ccwl_Merchant) IN (0, ccwl_Merchant))
		AND
		ccwl_InsertDate BETWEEN
			IsNull(CASE WHEN Len(@sFromDate103)>7 THEN Convert(datetime, @sFromDate103, 103) ELSE ccwl_InsertDate END, ccwl_InsertDate)
			AND
			IsNull(CASE WHEN Len(@sToDate103)>7 THEN Convert(datetime, @sToDate103, 103) ELSE ccwl_InsertDate END, ccwl_InsertDate)
		AND
		Sign(IsNull(@nIsMerchant, -1)) IN (-1, Sign(ccwl_Merchant))
		AND
		IsNull(@nIsBurnt, -1) IN (-1, ccwl_IsBurnt)
		AND
		IsNull(@nID, 0) IN (0, w.ID)
		AND
		IsNull(@nLevel, -1) IN (-1, ccwl_Level)
		AND
		IsNull(@sUsername, '') IN ('', dbo.RemoveDomainFromUsername(ccwl_Username))
		AND
		IsNull(@nPaymentMethod, -1) IN (-1, ccwl_PaymentMethod)
		AND
		IsNull(@nBin, 0) IN (0, ccwl_Bin)
		AND
		IsNull(@nLast4, -1) IN (-1, ccwl_Last4)
		AND
		IsNull(@sCardHolder, '') IN ('', ccwl_CardHolder)
		AND
		(IsNull(@sCardNumber, '')='' OR replace(ccwl_CardNumber256,' ','') = dbo.GetEncrypted256(Replace(@sCardNumber,' ','')))
		AND
		(IsNull(@nMonthFrom, 0)=0 OR IsNull(@nYearFrom, 0)=0 OR ccwl_ExpYear>@nYearFrom OR (ccwl_ExpYear=@nYearFrom AND ccwl_ExpMonth>=@nMonthFrom))
		AND
		(IsNull(@nMonthTo, 0)=0 OR IsNull(@nYearTo, 0)=0 OR ccwl_ExpYear<@nYearTo OR (ccwl_ExpYear=@nYearTo AND ccwl_ExpMonth<=@nMonthTo))
GO
