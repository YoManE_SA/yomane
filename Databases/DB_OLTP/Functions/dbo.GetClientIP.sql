SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetClientIP]()
RETURNS varchar(15)
AS
BEGIN
	RETURN (select client_net_address from sys.dm_exec_connections where session_id=@@SPID)
END
GO
