SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[IsValidIPAddress](@sIP nvarchar(50))
RETURNS bit
AS
BEGIN
	IF @sIP NOT LIKE '[0-9]%.%[0-9]%.%[0-9]%.%[0-9]' RETURN 0;
	IF CharIndex('..', @sIP)>0 RETURN 0
	IF IsNumeric(Replace(@sIP, '.', ''))=0 RETURN 0;
	RETURN 1
END
GO
