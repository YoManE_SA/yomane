SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetChargebacksMerchant](@dtFrom datetime, @dtTo datetime, @merchantId int)
RETURNS TABLE
AS
RETURN
	select
		CompanyName GroupName,
		CUR_ISOName CurrencyName,
		pm.Name PaymentMethodName,	
		count(*) Chargebacks,
		sum(Amount) ChbAmount,
		Sum(netpayFee_ratioCharge) ChbProcessingFee,
		Sum(netpayFee_transactionCharge) ChbTransactionCharge,
		sum(netpayFee_chbCharge) ChbCharge
	from
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN List.PaymentMethod pm WITH (NOLOCK)
		inner join tblCompanyTransPass t with (nolock) on m.id=t.CompanyID and c.CUR_ID=t.currency and pm.PaymentMethod_id=t.PaymentMethod
	where
		t.denieddate between @dtFrom and @dtTo
		and t.IsChargeback=1 and m.id=@merchantId
	group by
		CompanyName, CUR_ISOName, pm.Name

GO
