SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCreditCardWhitelistDetails](@nID int)
RETURNS TABLE
AS
RETURN
	SELECT
		w.ID,
		ccwl_Merchant Merchant,
		IsNull(m.CompanyName, '') MerchantName,
		IsNull(m.CustomerNumber, '') MID,
		IsNull(m.ActiveStatus, -1) MerchantStatus,
		IsNull(s.GD_Text, '') MerchantStatusName,
		IsNull(s.GD_Color, '') MerchantStatusColor,
		IsNull(IsNull(ChargebackNotifyMail, merchantSupportEmail), Mail) MerchantMail,
		IsNull(IsNull(merchantSupportPhoneNum, m.Phone), CompanyPhone) MerchantPhone,
		IsNull(i.GD_Text, '') IndustryName,
		IsNull(i.GD_Color, '') IndustryColor,
		dbo.GetDecrypted256(ccwl_CardNumber256) CardNumber,
		ccwl_PaymentMethod PaymentMethod,
		IsNull(p.GD_Text, '') PaymentMethodName,
		Right('000'+LTrim(RTrim(Str(ccwl_Last4))), 4) Last4,
		ccwl_Bin Bin,
		ccwl_BinCountry BinCountry,
		IsNull(c.Name, '') BinCountryName,
		ccwl_CardHolder CardHolder,
		Right('0'+LTrim(RTrim(Str(ccwl_ExpMonth))), 2) ExpMonth,
		ccwl_ExpYear ExpYear,
		ccwl_IsBurnt IsBurnt,
		IsNull(ccwl_BurnDate, ccwl_InsertDate) BurnDate,
		ccwl_Level RecordLevel,
		IsNull(l.GD_Text, '') RecordLevelName,
		IsNull(l.GD_Color, '') RecordLevelColor,
		ccwl_Comment Comment,
		ccwl_InsertDate InsertDate,
		dbo.RemoveDomainFromUsername(ccwl_Username) Username,
		ccwl_IP IP
	FROM
		tblCreditCardWhitelist w WITH (NOLOCK)
		left join tblCompany m WITH (NOLOCK) ON ccwl_Merchant=m.ID
		left join GetGlobalData(44) s ON m.ActiveStatus=s.GD_ID
		left join GetGlobalData(51) i ON m.CompanyIndustry_id=i.GD_ID
		left join GetGlobalData(42) p ON ccwl_PaymentMethod=p.GD_ID
		left join GetGlobalData(39) l ON ccwl_Level=l.GD_ID
		left join list.countrylist c ON ccwl_BinCountry=c.CountryISOCode
	WHERE
		w.ID=@nID;
GO
