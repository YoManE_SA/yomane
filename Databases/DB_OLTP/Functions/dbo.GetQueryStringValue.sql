SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetQueryStringValue](@sFieldName AS nvarchar(100), @sQueryString AS nvarchar(4000))
RETURNS nvarchar(250)
AS
BEGIN
	SET @sFieldName='&'+@sFieldName+'='
	SET @sQueryString='&'+@sQueryString+'&'
	DECLARE @nStart int
	SET @nStart=CharIndex(@sFieldName, @sQueryString)
	IF IsNull(@nStart, 0)=0 RETURN ''
	SET @nStart=@nStart+Len(@sFieldName)
	DECLARE @nEnd int
	SET @nEnd=CharIndex('&', @sQueryString, @nStart)
	IF @nEnd-@nStart<=0 RETURN ''
	RETURN Substring(@sQueryString, @nStart, @nEnd-@nStart)
END
GO
