SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetRollingReleaseSettlement](@dtFrom datetime, @dtTo datetime, @nSettlement int) RETURNS TABLE AS RETURN
select
	count(*) Transactions,
	Sum(Amount) RollingReleaseAmount
from
	tblCompanyTransPass t with (nolock)
where
	t.insertdate between @dtFrom and @dtTo
	and t.CreditType=1 and t.PaymentMethod=4 and dbo.IsCHB(DeniedStatus, isTestOnly)=0 and PrimaryPayedID=@nSettlement
GO
