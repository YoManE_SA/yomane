SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[GetDecryptedInternal] (@nKeyID [int], @binBytes [varbinary] (1500))
RETURNS [nvarchar] (1000)
WITH EXECUTE AS CALLER, 
RETURNS NULL ON NULL INPUT
EXTERNAL NAME [Netpay.Crypt].[Netpay.Crypt.SQL].[Decrypt]
GO
