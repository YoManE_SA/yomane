SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetRefundRequests](@nTransID int)
RETURNS TABLE
AS
RETURN
	SELECT
		ID, Convert(char(8), RefundAskDate, 3)+' '+Convert(char(5), RefundAskDate, 8) [Request Date], Convert(char(10), RefundAskAmount, 1) [Amount], CUR_ISOName [Currency], GD_Description [Current Status], RefundAskComment [Comment]
	FROM
		tblRefundAsk a
		inner join GetGlobalData(63) s on RefundAskStatus=GD_ID
		inner join tblSystemCurrencies c on RefundAskCurrency=CUR_ID
	WHERE
		TransID=@nTransID
GO
