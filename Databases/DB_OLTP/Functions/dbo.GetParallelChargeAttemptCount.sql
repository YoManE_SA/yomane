SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetParallelChargeAttemptCount](@sMerchantNumber varchar(50), @sCardNumber varchar(50))
RETURNS int
AS
BEGIN
	RETURN
	(
		SELECT
			COUNT(*)
		FROM
			(
				SELECT
					Replace(ISNULL(Lca_QueryString, Lca_RequestForm), '|', '&') Arguments
				FROM
					tblLogChargeAttempts WITH (NOLOCK)
				WHERE
					Lca_DateStart>DATEADD(minute, -5, GetDate()) AND Lca_TransNum IS NULL AND Lca_MerchantNumber=@sMerchantNumber
			) g
		WHERE
			dbo.GetQueryStringValue('CardNum', Arguments)=STUFF(@sCardNumber, 7,6, 'xxxxxx')
			OR
			dbo.GetQueryStringValue('CCard_num1', Arguments)
			+dbo.GetQueryStringValue('CCard_num2', Arguments)
			+dbo.GetQueryStringValue('CCard_num3', Arguments)
			+dbo.GetQueryStringValue('CCard_num4', Arguments) IN (STUFF(@sCardNumber, 7,6, 'xxxxxx'), STUFF(@sCardNumber, 9,4, 'xxxxxx'))
	)
END
GO
