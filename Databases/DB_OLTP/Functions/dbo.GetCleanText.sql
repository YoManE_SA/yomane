SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCleanText](@sText nvarchar(100))
RETURNS nvarchar(100)
AS
BEGIN
	RETURN Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(@sText, '-', ''), ' ', ''), '/', ''), '\', ''), '_', ''), '+', ''), '.', ''), ',', ''), ';', ''), '''', ''), '"', '')
END
GO
