SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetChargeAttemptsWithoutTransaction]()
RETURNS TABLE
AS
RETURN
	SELECT
		COUNT(*) Attempts,
		MIN(Lca_DateStart) FirstDate,
		MIN(LogChargeAttempts_ID) FirstID,
		MAX(Lca_DateStart) LastDate,
		MAX(LogChargeAttempts_ID) LastID
	FROM
		tblLogChargeAttempts
	WHERE
		Lca_TransNum IS NULL
		AND Lca_DateStart BETWEEN DATEADD(DAY, -3, GETDATE()) AND DATEADD(MINUTE, -5, GETDATE())
GO
