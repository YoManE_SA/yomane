SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[SendMessageSMS](@sNumber varchar(10), @sText nvarchar(max))
RETURNS bit
AS
BEGIN
	DECLARE @sURL nvarchar(200), @sString nvarchar(2000), @sResult nvarchar(1000)
	SELECT @sURL=pc_SMS_URL FROM tblParentCompany WHERE ID=1
	SET @sString=Left('Action=Send&Number='+@sNumber+'&Text='+@sText, 2000)
	SET @sResult=dbo.SendRequestHTTP(@sURL, @sString)
	RETURN CASE Left(@sResult, 2) WHEN 'OK' THEN 1 ELSE 0 END
END
GO
