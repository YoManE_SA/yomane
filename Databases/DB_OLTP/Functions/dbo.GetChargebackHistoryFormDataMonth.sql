SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetChargebackHistoryFormDataMonth](@nMerchant int, @nCurrency int, @nMonth int, @nYear int)
RETURNS TABLE
AS
RETURN
	SELECT
		IsNull(ChargebacksBaseVisa, 0) SaleCountVisa,
		IsNull(ChbBaseAmountVisa, 0) SaleAmountVisa,
		IsNull(ChargebacksVisa, 0) ChbCountVisa,
		IsNull(ChbAmountVisa, 0) ChbAmountVisa,
		IsNull(ChargebacksBaseCurrentMC, 0) SaleCountMC,
		IsNull(ChbBaseAmountCurrentMC, 0) SaleAmountMC,
		IsNull(ChargebacksMC, 0) ChbCountMC,
		IsNull(ChbAmountMC, 0) ChbAmountMC
	FROM
		ReportGetChbRatioMCMerchant(LTrim(RTrim(Str(@nYear)))+Right('0'+LTrim(RTrim(Str(@nMonth))), 2)+'01', DateAdd(month, 1, LTrim(RTrim(Str(@nYear)))+Right('0'+LTrim(RTrim(Str(@nMonth))), 2)+'01'), @nMerchant) m
		INNER JOIN ReportGetChbRatioVisaMerchant(LTrim(RTrim(Str(@nYear)))+Right('0'+LTrim(RTrim(Str(@nMonth))), 2)+'01', DateAdd(month, 1, LTrim(RTrim(Str(@nYear)))+Right('0'+LTrim(RTrim(Str(@nMonth))), 2)+'01'), @nMerchant) v ON m.CurrencyName=v.CurrencyName
	WHERE
		m.CurrencyName=(SELECT CUR_ISOName FROM tblSystemCurrencies WHERE CUR_ID=@nCurrency)
GO
