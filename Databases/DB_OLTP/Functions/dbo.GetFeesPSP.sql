SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetFeesPSP](@nPSP int, @dtFrom datetime=NULL, @dtTo datetime=NULL)
RETURNS TABLE
AS
RETURN
	SELECT
		Sum
		(
			CASE dbo.IsChb(DeniedStatus, IsTestOnly)
				WHEN 1 THEN CASE OriginalTransID WHEN 0 THEN 0 ELSE -1 END
				ELSE CASE CreditType WHEN 0 THEN -1 ELSE 1 END
			END
			*Amount*(c.CUR_BaseRate/cd.CUR_BaseRate)
		) Volume,
		Sum(CASE WHEN dbo.IsChb(DeniedStatus, IsTestOnly)=1 AND OriginalTransID=0 THEN 2 ELSE 1 END) Transactions
	FROM
		tblSystemCurrencies c WITH (NOLOCK)
		INNER JOIN tblSystemCurrencies cd WITH (NOLOCK) ON cd.CUR_ID=1
		INNER JOIN tblCompanyTransPass t WITH (NOLOCK) ON c.CUR_ID=t.Currency
		INNER JOIN tblDebitTerminals d WITH (NOLOCK) ON t.DebitCompanyID=d.DebitCompany and t.terminalNumber=d.terminalNumber
	WHERE
		dt_ManagingPSP=@nPSP
		AND
		IsTestOnly=0
		AND
		InsertDate BETWEEN
			IsNull(@dtFrom, DateAdd(month, -1, Cast(Cast(DateAdd(day, 1-day(GetDate()), GetDate()) AS date) AS datetime)))
			AND
			IsNull(@dtTo, DateAdd(second, -1, Cast(Cast(DateAdd(day, 1-day(GetDate()), GetDate()) AS date) AS datetime)))
GO
