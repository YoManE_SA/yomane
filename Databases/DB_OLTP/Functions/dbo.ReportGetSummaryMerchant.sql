SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetSummaryMerchant](@dtFrom datetime, @dtTo datetime, @merchantId int)
RETURNS @tblSummary TABLE
(
	[Name] nvarchar(200),
	[ID] int,
	[Currency] nchar(3),
	[Payment_Method] nvarchar(80),
	[CalcRollingReserve] money,
	[-1-] varchar(1),
	[Declined_Amount_Sales] money,
	[Declined_Count_Sales] int,
	[Declined_Transactions_Fee] money,
	[-2-] varchar(1),
	[Transaction_Amount_Sales] money,
	[Transaction_Count_Sales] int,
	[Sales_Processing_Fee] money,
	[Sales_Transactions_Fee] money,
	[-3-] varchar(1),
	[ChargeBacks_Count] int,
	[ChargeBacks_Amount] money,
	[ChargeBacks_Processing_Fee] money,
	[ChargeBacks_Transactions_Fee] money,
	[ChargeBacks_Fee] money,
	[-4-] varchar(1),
	[Refunds_Count] int,
	[Refunds_Amount] money,
	[Refunds_Processing_Fee] money,
	[Refunds_Transactions_Fee] money,
	[-5-] varchar(1),
	[Rolling_Reserve_Amount] money,
	[-6-] varchar(1),
	[Rolling_Release_Amount] money,
	[-7-] varchar(1),
	[Authorization_Fee] money,
	[Decline_Fee] money,
	[Transaction_Fee] money,
	[Transaction_Ratio_Fee] money,
	[Chargeback_Fee] money,
	[Clarification_Fee] money,
	[Handling_Fee] money
)
AS
BEGIN
	INSERT INTO @tblSummary
	(
		Name, ID, Currency, Payment_Method, CalcRollingReserve,
		[-1-],
		Declined_Amount_Sales, Declined_Count_Sales, Declined_Transactions_Fee,
		[-2-],
		Transaction_Amount_Sales, Transaction_Count_Sales, Sales_Processing_Fee, Sales_Transactions_Fee,
		[-3-],
		ChargeBacks_Count, ChargeBacks_Amount, ChargeBacks_Processing_Fee, ChargeBacks_Transactions_Fee, ChargeBacks_Fee,
		[-4-],
		Refunds_Count, Refunds_Amount, Refunds_Processing_Fee, Refunds_Transactions_Fee,
		[-5-],
		Rolling_Reserve_Amount,
		[-6-],
		Rolling_Release_Amount,
		[-7-],
		Authorization_Fee, Decline_Fee, Transaction_Fee, Transaction_Ratio_Fee, Chargeback_Fee, Clarification_Fee, Handling_Fee
	)
	SELECT
		g.CompanyName Name,
		g.ID,
		cu.CUR_ISOName Currency,
		pm.Name AS Payment_Method,
		CAST (CASE g.RRState WHEN 1 THEN CASE g.RREnable WHEN 1 THEN (s.SalesAmount/100.0)* g.SecurityDeposit ELSE NULL END ELSE NULL END AS money) CalcRollingReserve,
		'' [-1-],
		DeclinesSaleGatewayAmount+DeclinesSaleRiskAmount+DeclinesSaleIssuerAmount+SalesAmount [Declined_Amount_Sales],
		DeclinesSaleGateway+DeclinesSaleRisk+DeclinesSaleIssuer+Sales [Declined_Count_Sales],
		DeclinesTransactionCharge [Declined_Transactions_Fee],
		'' [-2-],
		SalesAmount [Transaction_Amount_Sales],
		Sales [Transaction_Count_Sales],
		SalesProcessingFee [Sales_Processing_Fee],
		SalesTransactionCharge [Sales_Transactions_Fee],
		'' [-3-],
		Chargebacks [ChargeBacks_Count],
		ChbAmount [ChargeBacks_Amount],
		ChbProcessingFee [ChargeBacks_Processing_Fee],
		ChbTransactionCharge [ChargeBacks_Transactions_Fee],
		ChbCharge [ChargeBacks_Fee],
		'' [-4-],
		Refunds [Refunds_Count],
		RefundsAmount [Refunds_Amount],
		RefundsProcessingFee [Refunds_Processing_Fee],
		RefundsTransactionCharge [Refunds_Transactions_Fee],
		'' [-5-],
		RollingReserveAmount [Rolling_Reserve_Amount],
		'' [-6-],
		RollingReleaseAmount [Rolling_Release_Amount],
		'' [-7-],
		FeeTransAuth [Authorization_Fee],
		FeeTransFail [Decline_Fee],
		FeeTransPass [Transaction_Fee],
		FeeTransPassRatio [Transaction_Ratio_Fee],
		FeeTransPassChargeback [Chargeback_Fee],
		FeeTransPassClarification [Clarification_Fee],
		FeeTransPassHandling [Handling_Fee]
	FROM
		(SELECT CompanyName, ID, RRState, RREnable, SecurityDeposit FROM tblCompany WITH (NOLOCK) WHERE ID = @merchantId) g
		CROSS JOIN (SELECT CUR_ISOName FROM tblSystemCurrencies WITH (NOLOCK)) cu
		CROSS JOIN (SELECT Name FROM List.PaymentMethod WITH (NOLOCK) WHERE pm_Type>1) pm
		LEFT JOIN ReportGetDeclinesMerchant(@dtFrom, @dtTo, @merchantId) d on g.CompanyName=d.GroupName and cu.CUR_ISOName=d.CurrencyName and Name=d.PaymentMethodName
		LEFT JOIN ReportGetSalesMerchant(@dtFrom, @dtTo, @merchantId) s on g.CompanyName=s.GroupName and cu.CUR_ISOName=s.CurrencyName and Name=s.PaymentMethodName
		LEFT JOIN ReportGetRefundsMerchant(@dtFrom, @dtTo, @merchantId) r on g.CompanyName=r.GroupName and cu.CUR_ISOName=r.CurrencyName and Name=r.PaymentMethodName
		LEFT JOIN ReportGetChargebacksMerchant(@dtFrom, @dtTo, @merchantId) c on g.CompanyName=c.GroupName and cu.CUR_ISOName=c.CurrencyName and Name=c.PaymentMethodName
		LEFT JOIN ReportGetRollingReserveMerchant(@dtFrom, @dtTo, @merchantId) rr on g.CompanyName=rr.GroupName and cu.CUR_ISOName=rr.CurrencyName and Name=rr.PaymentMethodName
		LEFT JOIN ReportGetRollingReleaseMerchant(@dtFrom, @dtTo, @merchantId) rrl on g.CompanyName=rrl.GroupName and cu.CUR_ISOName=rrl.CurrencyName and Name=rrl.PaymentMethodName
		LEFT JOIN ReportGetFeesMerchant(@dtFrom, @dtTo, @merchantId) f on g.CompanyName=f.GroupName and cu.CUR_ISOName=f.CurrencyName and Name=f.PaymentMethodName;
	DELETE FROM
		@tblSummary
	WHERE
		IsNull([Declined_Count_Sales], 0)=0
		AND IsNull([Transaction_Count_Sales], 0)=0
		AND IsNull([Refunds_Count], 0)=0
		AND IsNull([ChargeBacks_Count], 0)=0
		AND IsNull([Rolling_Reserve_Amount], 0)=0
		AND IsNull([Rolling_Release_Amount], 0)=0
		AND IsNull([Authorization_Fee], 0)+IsNull([Decline_Fee], 0)+IsNull([Transaction_Fee], 0)=0;
	RETURN
END


GO
