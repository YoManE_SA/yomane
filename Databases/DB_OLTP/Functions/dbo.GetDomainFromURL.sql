SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetDomainFromURL](@sURL nvarchar(1000))
RETURNS nvarchar(100)
AS
BEGIN
	DECLARE @sURI nvarchar(1000)
	SET @sURI=Right(@sURL, Len(@sURL)-1-CharIndex('//', @sURL))
	RETURN Left(@sURI, CharIndex('/', @sURI+'/')-1)
END
GO
