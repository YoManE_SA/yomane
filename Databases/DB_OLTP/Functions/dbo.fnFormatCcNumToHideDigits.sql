SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[fnFormatCcNumToHideDigits](@sCardNum nvarchar(25))
RETURNS nvarchar(25)
AS
BEGIN
	SET @sCardNum = dbo.fnFormatCcNumToGroups(@sCardNum)
	DECLARE @sTemp nvarchar(25), @i int, @nStart int, @nEnd int
	SELECT @sTemp='', @nStart = CASE WHEN Len(Replace(@sCardNum, ' ', ''))<16 THEN 2 ELSE 7 END
	SELECT @i=@nStart+1, @nEnd = CASE CharIndex(' ', Right(@sCardNum, 4)) WHEN 0 THEN 4 ELSE 5 END
	WHILE @i <= Len(@sCardNum)-@nEnd
	BEGIN
		SET @sTemp = @sTemp+(CASE Substring(@sCardNum, @i, 1) WHEN ' ' THEN ' ' ELSE 'X' END)
		SET @i = @i+1
	END
	RETURN Left(@sCardNum, @nStart)+@sTemp+Right(@sCardNum, @nEnd)
END

GO
