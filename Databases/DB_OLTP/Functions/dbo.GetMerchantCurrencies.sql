SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantCurrencies](@nMerchant int)
RETURNS @tbl TABLE (ID int, Name char(3))
AS
BEGIN
	INSERT INTO @tbl
	SELECT DISTINCT
		CUR_ID,
		CUR_IsoName
	FROM
		tblCompanyCreditFees
		INNER JOIN tblCompanyCreditFeesTerminals ON CCF_ID=CCFT_CCF_ID
		INNER JOIN tblSystemCurrencies ON CCF_CurrencyID=CUR_ID
	WHERE
		CCF_CompanyID=@nMerchant AND CCF_IsDisabled=0;
	IF NOT EXISTS (SELECT ID FROM @tbl)
		INSERT INTO @tbl
		SELECT DISTINCT
			CUR_ID,
			CUR_IsoName
		FROM
			tblSystemCurrencies;
		RETURN;
END
GO
