SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetProtectedECheck](@sNumber nvarchar(25))
RETURNS nvarchar(25)
AS
BEGIN
	RETURN Replicate('X', Len(Ltrim(RTrim(@sNumber)))-4)+Right(@sNumber, 4)
END
GO
