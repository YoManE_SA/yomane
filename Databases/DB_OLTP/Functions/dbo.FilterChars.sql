SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[FilterChars](@sText nvarchar(max), @sFilter nvarchar(max), @sPlaceholder nvarchar(1)) RETURNS nvarchar(max)
AS
BEGIN
	IF @sPlaceholder IS NULL SET @sPlaceholder='';
	DECLARE @i int, @sResult nvarchar(max), @sChar nchar(1);
	SET @i=1;
	SET @sResult='';
	WHILE @i<=Len(@sText)
	BEGIN
		SET @sChar=Substring(@sText, @i, 1);
		SET @sResult=@sResult+CASE WHEN @sFilter LIKE '%'+@sChar+'%' THEN @sChar ELSE @sPlaceholder END;
		SET @i=@i+1;
	END;
	RETURN @sResult;
END
GO
