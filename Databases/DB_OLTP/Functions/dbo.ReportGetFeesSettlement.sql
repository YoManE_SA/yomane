SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetFeesSettlement](@dtFrom datetime, @dtTo datetime, @nSettlement int) RETURNS TABLE AS RETURN
select
	IsNull(Sum(IsNull(a.netpayFee_transactionCharge, 0)), 0) FeeTransAuth,
	IsNull(Sum(IsNull(f.netpayFee_transactionCharge, 0)), 0) FeeTransFail,
	IsNull(Sum(IsNull(p.netpayFee_transactionCharge, 0)), 0) FeeTransPass,
	IsNull(Sum(IsNull(p.netpayFee_ratioCharge, 0)), 0) FeeTransPassRatio,
	IsNull(Sum(IsNull(p.netpayFee_chbCharge, 0)), 0) FeeTransPassChargeback,
	IsNull(Sum(IsNull(p.netpayFee_ClrfCharge, 0)), 0) FeeTransPassClarification,
	IsNull(Sum(IsNull(p.HandlingFee, 0)), 0) FeeTransPassHandling
from
	tblCompanyTransPass p with (nolock)
	cross join tblCompanyTransFail f with (nolock)
	cross join tblCompanyTransApproval a with (nolock)
where
	p.PrimaryPayedID=@nSettlement and p.insertdate between @dtFrom and @dtTo
	AND
	f.PayID=@nSettlement and f.insertdate between @dtFrom and @dtTo
	AND
	a.PayID=@nSettlement and a.insertdate between @dtFrom and @dtTo
GO
