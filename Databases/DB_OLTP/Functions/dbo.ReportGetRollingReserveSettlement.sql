SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetRollingReserveSettlement](@dtFrom datetime, @dtTo datetime, @nSettlement int) RETURNS TABLE AS RETURN
select
	count(*) Transactions,
	Sum(Amount) RollingReserveAmount
from
	tblCompanyTransPass t with (nolock)
where
	t.insertdate between @dtFrom and @dtTo
	and t.CreditType=0 and t.PaymentMethod=4 and dbo.IsCHB(DeniedStatus, isTestOnly)=0 and PrimaryPayedID=@nSettlement
GO
