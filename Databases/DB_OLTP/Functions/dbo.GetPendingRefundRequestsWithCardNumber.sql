SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetPendingRefundRequestsWithCardNumber](@nMerchant int=0, @nStatus int=0)
RETURNS TABLE
AS
RETURN
	SELECT
		TransID,
		dbo.GetDecrypted256(CCard_Number256) CreditCardNumber,
		t.Amount,
		c.CUR_ISOName Currency,
		t.InsertDate TransactionDate,
		t.ApprovalNumber,
		t.DebitReferenceCode,
		RefundAskAmount RefundRequestAmount,
		cr.CUR_ISOName RefundRequestCurrency,
		RefundAskDate RefundRequestDate,
		RefundAskComment RefundRequestComment
	FROM
		tblRefundAsk r
		INNER JOIN tblCompanyTransPass t ON transID=t.ID 
		INNER JOIN tblCreditCard cc ON t.CreditCardID=cc.ID
		INNER JOIN tblSystemCurrencies c ON Currency=c.CUR_ID
		INNER JOIN tblSystemCurrencies cr ON RefundAskCurrency=cr.CUR_ID
	WHERE
		@nMerchant IN (0, r.companyID)
		AND
		RefundAskStatus=@nStatus
GO
