SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetPermittedDebitCompanies](@sUsername nvarchar(50))
RETURNS table
AS
RETURN
(
	SELECT
		sudc_DebitCompany ID
	FROM
		tblSecurityUserDebitCompany INNER JOIN tblSecurityUser ON sudc_User=tblSecurityUser.ID AND su_Username=@sUsername
)
GO
