SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ReportGetRefundsMerchant](@dtFrom datetime, @dtTo datetime, @merchantId int)
RETURNS TABLE
AS
RETURN
	select
		CompanyName GroupName,
		CUR_ISOName CurrencyName,
		pm.Name PaymentMethodName,	
		count(*) Refunds,
		Sum(Amount) RefundsAmount,
		Sum(netpayFee_ratioCharge) RefundsProcessingFee,
		Sum(netpayFee_transactionCharge) RefundsTransactionCharge	
	from
		tblCompany m WITH (NOLOCK)
		CROSS JOIN tblSystemCurrencies c WITH (NOLOCK)
		CROSS JOIN List.PaymentMethod pm WITH (NOLOCK)
		inner join tblCompanyTransPass t with (nolock) on m.id=t.CompanyID and c.CUR_ID=t.currency and pm.PaymentMethod_id=t.PaymentMethod
	where
		t.insertdate between @dtFrom and @dtTo
		and t.CreditType=0 and t.IsChargeback=0 and t.paymentMethod>15 and m.id=@merchantId
	group by
		CompanyName, CUR_ISOName, pm.Name

GO
