SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCcDetailsForWhitelist](@nID int)
RETURNS TABLE
AS
RETURN
	SELECT
		CompanyID Merchant,
		dbo.GetDecrypted256(CCard_Number256) CardNumber,
		Cast(ExpMM AS tinyint) ExpMonth,
		Cast(ExpYY AS smallint)+CASE WHEN Len(ExpYY)<4 THEN 2000 ELSE 0 END ExpYear,
		Member CardHolder
	FROM
		tblCreditCard WITH (NOLOCK)
	WHERE
		ID=@nID	
GO
