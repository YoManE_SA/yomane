SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCUI](@sInput AS nvarchar(20))
RETURNS nvarchar(10)
AS
BEGIN
	IF Len(@sInput)<6 RETURN ''
	DECLARE @nResult AS int, @i as int
	SELECT @sInput=LTrim(RTrim(@sInput)), @nResult=0, @i=4
	WHILE @i<=Len(@sInput) SELECT @nResult=@nResult*10+CharIndex(Substring(@sInput, @i, 1), 'blackhorse')-1, @i=@i+1
	RETURN Right('0000'+LTrim(RTrim(Str(@nResult))), len(LTrim(RTrim(@sInput)))-3)
END
GO
