SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[RecurringGetAttemptComment] ( @nCharge AS INT )
RETURNS NVARCHAR(100)
AS 
    BEGIN
        DECLARE @sReturn AS NVARCHAR(100) ,
            @nSeries INT ,
            @nChargeCount INT ,
            @nChargeNumber INT ,
            @nAttemptNumber INT;

        SELECT  @nSeries = rc.rc_Series ,
                @nChargeCount = rs.rs_ChargeCount ,
                @nChargeNumber = rc.rc_ChargeNumber
        FROM    tblRecurringCharge AS rc
                INNER JOIN tblRecurringSeries AS rs ON rc.rc_Series = rs.ID
        WHERE   rc.ID = @nCharge;

        SET @nAttemptNumber = ( SELECT  COUNT(*)
                                FROM    tblRecurringAttempt
                                WHERE   ra_Charge = @nCharge
                              ) + 1;
        SET @sReturn = 'Recurring series ' + LTRIM(RTRIM(STR(@nSeries)));
        SET @sReturn = @sReturn + ' charge ' + LTRIM(RTRIM(STR(@nChargeNumber)));
        SET @sReturn = @sReturn + '/' + LTRIM(RTRIM(STR(@nChargeCount)));
        SET @sReturn = @sReturn + ' attempt '
            + LTRIM(RTRIM(STR(@nAttemptNumber)));
        RETURN @sReturn
    END;

GO
