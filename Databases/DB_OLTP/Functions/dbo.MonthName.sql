SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[MonthName](@dtNow datetime)
RETURNS nvarchar(20)
AS
BEGIN
	RETURN Left(DateName(month, @dtNow), 3);
END
GO
