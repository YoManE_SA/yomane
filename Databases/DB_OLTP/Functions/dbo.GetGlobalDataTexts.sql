SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetGlobalDataTexts](@nGroup int)
RETURNS TABLE 
AS
RETURN
(
	SELECT
		g1.GD_ID ID, g1.GD_Text Text1, g0.GD_Text Text0
	FROM
		GetGlobalData(IsNull(@nGroup, 0)) g1
		FULL JOIN GetGlobalDataLang(IsNull(@nGroup, 0), 0) g0 ON g1.GD_ID=g0.GD_ID
)
GO
