SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetRefundAmountAvailableMerchant](@nTransID int, @nMerchantID int)
RETURNS money
AS
BEGIN
	IF @nMerchantID>0
	BEGIN
		IF NOT EXISTS (SELECT ID FROM tblCompanyTransPass WHERE ID=@nTransID AND CompanyID=@nMerchantID) RETURN NULL;
	END;
	RETURN
		(SELECT Amount FROM tblCompanyTransPass WHERE ID=@nTransID)
		-
		IsNull((SELECT Sum(CASE RefundAskStatus WHEN 3 THEN 0 WHEN 6 THEN 0 ELSE RefundAskAmount END) FROM tblRefundAsk WHERE TransID=@nTransID), 0);
END
GO
