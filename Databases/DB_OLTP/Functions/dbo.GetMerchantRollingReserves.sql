SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMerchantRollingReserves](@nYear AS int)
RETURNS TABLE
AS
RETURN
	SELECT
		CompanyID Merchant,
		Currency,
		Sum(Amount*CASE CreditType WHEN 0 THEN -1 ELSE 1 END) Total
	FROM
		tblCompanyTransPass
	WHERE
		Year(InsertDate)=@nYear
		AND
		PaymentMethod=4
	GROUP BY
		CompanyID,
		Currency
GO
