SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[ReportGetDeclinesSettlement](@dtFrom datetime, @dtTo datetime, @nSettlement int) RETURNS TABLE AS RETURN
select
	sum(case when TransType<>1 and FailSource=2 then 1 else 0 end) DeclinesSaleGateway,
	sum(case when TransType<>1 and FailSource=1 then 1 else 0 end) DeclinesSaleRisk,
	sum(case when TransType<>1 and IsNull(FailSource, 0)=0 then 1 else 0 end) DeclinesSaleIssuer,
	sum(case when TransType<>1 and FailSource=2 then 1 else 0 end) + sum(case when TransType<>1 and FailSource=1 then 1 else 0 end) + sum(case when TransType<>1 and IsNull(FailSource, 0)=0 then 1 else 0 end) TotalSaleDeclines,	
	sum(case when TransType<>1 and FailSource=2 then Amount else 0 end) DeclinesSaleGatewayAmount,
	sum(case when TransType<>1 and FailSource=1 then Amount else 0 end) DeclinesSaleRiskAmount,
	sum(case when TransType<>1 and IsNull(FailSource, 0)=0 then Amount else 0 end) DeclinesSaleIssuerAmount,
	sum(case when TransType<>1 and FailSource=2 then Amount else 0 end) + sum(case when TransType<>1 and FailSource=1 then Amount else 0 end) + sum(case when TransType<>1 and IsNull(FailSource, 0)=0 then Amount else 0 end) TotalDeclinesSaleAmount,
	sum(netpayFee_transactionCharge) DeclinesTransactionCharge	
from
	(
		select
			id, replycode, transtype, DebitCompanyID, amount, netpayFee_transactionCharge, PayID
		from
			tblCompanytransfail WITH (NOLOCK)
		where
			insertdate between @dtFrom and @dtTo and PayID = @nSettlement
		/*union select
			id, replycode, transtype, DebitCompanyID, amount, netpayFee_transactionCharge, PayID
		from
			dbo.syn_archive_tblCompanyTransFail WITH (NOLOCK)
		where
			insertdate between @dtFrom and @dtTo and PayID = @nSettlement*/
	) t
	LEFT JOIN dbo.tblDebitCompanycode rc with (nolock) on t.replycode=rc.code and t.DebitCompanyID=rc.DebitCompanyID
	Where PayID = @nSettlement

GO
