SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewBIN]
AS
SELECT TOP 100 PERCENT
	BIN,
	isoCode,
	CCName,
	ImportDate,
	InsertDate,
	PaymentMethod,
   '<input type="button" class="buttonWhite buttonDelete" value="Delete" onclick="ConfirmAndDeleteBIN('+BIN+');" />' DeleteButton 
FROM
	tblCreditCardBIN
ORDER BY
	BIN
GO
