SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewCustomerCardDetails]
AS
SELECT
	dbo.fnFormatCcNumToHideDigits(dbo.GetDecrypted256(CCard_Number256)) AS CardNumber,
	dbo.GetCUI(CCard_Cui) AS CardCUI,
	*
FROM
	tblCustomerCC

GO
