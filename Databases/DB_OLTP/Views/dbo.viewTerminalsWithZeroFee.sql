SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewTerminalsWithZeroFee]
AS
SELECT
	CompanyName [Merchant],
	'<'+'img src="/NPCommon/ImgPaymentMethod/'
		+LTrim(RTrim(Str(CCF_PaymentMethod)))+'.gif" alt="'+GD_Text+'" title="'+GD_Text
		+'" align="middle" /> '+GD_Text+' ('+[CurrencyISOCode]
		+(CASE CCF_ListBINs WHEN '' THEN '' ELSE ' - '+Substring(CCF_ListBINs, 2, Len(CCF_ListBINs)-2) END)+')'
		[Payment Method],
	'<'+'img src="/NPCommon/ImgDebitCompanys/'+LTrim(RTrim(Str(tblDebitCompany.DebitCompany_ID)))+'.gif" alt="'
		+dc_name+'" title="'+dc_name+'" align="middle" /> '+dc_name
		[Debit Company],
	dt_name [Terminal Name],
	TerminalNumber [Terminal No.],
	Cast(Round(CCF_FixedFee, 2) AS varchar(20)) [Trans. Fee],
	Cast(Round(CCF_PercentFee, 2) AS varchar(20)) [Clearing Fee],
	Cast(Round(CCF_ApproveFixedFee, 2) AS varchar(20)) [Pre-Auth Fee],
	Cast(Round(CCF_RefundFixedFee, 2) AS varchar(20)) [Refund Fee],
	Cast(Round(CCF_ClarificationFee, 2) AS varchar(20)) [Copy R. Fee],
	Cast(Round(CCF_CBFixedFee, 2) AS varchar(20)) [CHB Fee],
	Cast(Round(CCF_FailFixedFee, 2) AS varchar(20)) [Fail Fee],
	'<'+'input type="button" class="buttonWhite" onclick="location.href=''merchant_dataTerminal.asp?companyID='
		+LTrim(Str(CCF_CompanyID))+''';" value="Edit" />'
		[Edit],
	tblCompany.ID MerchantID,
	tblDebitCompany.DebitCompany_ID DebitCompanyID
FROM
	tblCompanyCreditFees
	LEFT JOIN tblCompany ON tblCompanyCreditFees.CCF_CompanyID=tblCompany.ID
	LEFT JOIN [List].[CurrencyList] AS cu ON tblCompanyCreditFees.CCF_CurrencyID=cu.[CurrencyID]
	LEFT JOIN tblGlobalData ON tblCompanyCreditFees.CCF_PaymentMethod = tblGlobalData.GD_ID
	LEFT JOIN tblCompanyCreditFeesTerminals ON tblCompanyCreditFeesTerminals.CCFT_CCF_ID = tblCompanyCreditFees.CCF_ID
	LEFT JOIN tblDebitTerminals ON CCFT_Terminal=tblDebitTerminals.terminalNumber
	LEFT JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany=tblDebitCompany.DebitCompany_ID
WHERE
	tblCompany.ActiveStatus=30
	AND
	tblGlobalData.GD_LNG=1 AND tblGlobalData.GD_Group=42
	AND
	tblDebitTerminals.isNetpayTerminal=1
	AND
	tblDebitTerminals.isActive=1
	AND
	tblDebitCompany.dc_isActive=1
	AND
	CCF_IsDisabled=0
	AND
	0 IN
	(
		CCF_FixedFee,
		CCF_PercentFee,
		CCF_ApproveFixedFee,
		CCF_RefundFixedFee,
		CCF_ClarificationFee,
		CCF_CBFixedFee,
		CCF_FailFixedFee
	)


GO
