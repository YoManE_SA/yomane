SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewMerchantsIsracard]
AS
    SELECT  dt_CompanyNum_isracard ,
            t.TerminalNumber ,
            companyID ,
            CompanyName ,
            GD_Text ,
            Name ,
            COUNT(*) Transactions ,
            CAST(MAX(t.InsertDate) AS DATE) LastTrans
    FROM    tblCompanyTransPass t
            INNER JOIN tblDebitTerminals dt ON t.TerminalNumber = dt.terminalNumber
            INNER JOIN tblCompany m ON t.companyID = m.ID
            INNER JOIN GetGlobalData (44) ms ON m.ActiveStatus = ms.GD_ID
            INNER JOIN List.TransSource s ON t.TransactionTypeID = s.TransSource_id
    WHERE   dt_CompanyNum_isracard IN ( '3788015', '3946324', '3273059',
                                        '3272895', '3275757', '3918596',
                                        '3946282', '3714797' )
    GROUP BY dt_CompanyNum_isracard ,
            t.TerminalNumber ,
            companyID ,
            CompanyName ,
            GD_Text ,
            Name
GO
