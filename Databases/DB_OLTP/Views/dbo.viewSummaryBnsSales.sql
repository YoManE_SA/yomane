SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewSummaryBnsSales] AS
select
	*
from
	(
		select
			dt_ContractNumber ContractNo,
			m.ID,
			CompanyName,
			s.GD_Text CurrentStatus,
			cu.[CurrencyISOCode] Cur,
			SUM(Amount) SaleAmount,
			Count(*) SaleCount,
			Min(t.InsertDate) FirstSale,
			Max(t.InsertDate) LastSale
		from
			tblCompany m
			inner join GetGlobalData(44) s on ActiveStatus=s.GD_ID
			inner join tblCompanyTransPass t on m.ID=t.companyID
			inner join [List].[CurrencyList] AS cu on t.Currency=cu.[CurrencyID]
			inner join tblDebitTerminals dt on t.TerminalNumber=dt.terminalNumber
		where
			DebitCompanyID=18 and DebitCompany=18 and istestonly=0 and OriginalTransID=0 and CreditType>0 and m.ID not in (12, 35, 4395)
		group by
			dt_ContractNumber, m.ID, CompanyName, s.GD_Text, cu.[CurrencyISOCode]
	) p
where
	p.FirstSale<DATEADD(month, -1, getDate())

GO
