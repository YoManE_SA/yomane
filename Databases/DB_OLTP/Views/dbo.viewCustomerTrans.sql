SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewCustomerTrans]
AS
SELECT
	'Accepted' TransStatus,
	MerchantName,
	SourceName,
	SourceNameHeb,
	ReplyText,
	ReplyTextHeb,
	Amount,
	Comment,
	Currency,
	Id,
	InsertDate,
	PaymentMethodDisplay,
	Payments,
	ReplyCode,
	CustomerID
FROM
	viewCustomerTransPass
UNION
SELECT
	'Rejected' TransStatus,
	MerchantName,
	SourceName,
	SourceNameHeb,
	ReplyText,
	ReplyTextHeb,
	Amount,
	Comment,
	Currency,
	Id,
	InsertDate,
	PaymentMethodDisplay,
	Payments,
	ReplyCode,
	CustomerID
FROM
	viewCustomerTransFail

GO
