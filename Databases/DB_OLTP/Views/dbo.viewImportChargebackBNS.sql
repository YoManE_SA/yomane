SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewImportChargebackBNS] 
AS
SELECT
	18 FileDebitCompany,
	(SELECT dc_Name FROM tblDebitCompany WHERE DebitCompany_ID=18) FileDebitCompanyName,
	dbo.fnFormatCcNumToHideDigits([CARDHOLDER-NUMBER]) CardNumberPartial,
	dbo.GetImportChargebackBNSTransList([CARDHOLDER-NUMBER], [TXN-DATE], [AMOUNT-TXN], CUR, 18, [REF-NO], MRF) TransList,
	dbo.GetImportChargebackBNSMerchantList([CARDHOLDER-NUMBER], [TXN-DATE], [AMOUNT-TXN], CUR, 18, [REF-NO], MRF) MerchantList,
	IsNull(PaymentMethod, 20) PaymentMethod,
	pm.Name PaymentMethodName,
	IsNull(tblCreditCardBIN.isoCode, '--') CountryISO,
	IsNull(c.Name, 'Country Unknown') CountryName,
	Convert(datetime, [TXN-DATE], 103) TransactionDate,
	CUR CurrencyIsoName,
	RSN Reason,
	tblImportChargebackBNS.*
FROM
	tblImportChargebackBNS WITH (NOLOCK)
	LEFT JOIN tblCreditCardBIN WITH (NOLOCK) ON Left([CARDHOLDER-NUMBER], 6)=BIN
	LEFT JOIN [List].[CountryList] AS c WITH (NOLOCK) ON tblCreditCardBIN.isoCode=c.[CountryISOCode]
	LEFT JOIN List.PaymentMethod pm WITH (NOLOCK) ON IsNull(PaymentMethod, 20) = pm.PaymentMethod_id
WHERE
	icb_IsProcessed=0

GO
