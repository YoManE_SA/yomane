SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[viewMerchantsMailing]
AS
SELECT
	ID [MerchantID],
	CustomerNumber [MerchantNum],
	CompanyName [MerchantName],
	CASE ActiveStatus WHEN 30 THEN 'Processing' ELSE 'Integration' END [Status],
	CONVERT(char(10), merchantOpenningDate, 120) [OpenDate],
	careOfAdminUser [AccManager],
	isNetpayTerminal [isGateway],
	CASE languagePreference WHEN 'heb' THEN 'Hebrew' ELSE 'English' END [Language],
	COALESCE
	(
		CASE ContactName WHEN '' THEN NULL ELSE ContactName END,
		CASE FirstName+' '+LastName WHEN ' ' THEN NULL ELSE FirstName+' '+LastName END,
		CASE CCardHolderName WHEN '' THEN NULL ELSE CCardHolderName END,
		UserName
	)
	+' ('+
	COALESCE
	(
		CASE CompanyLegalName WHEN '' THEN NULL ELSE CompanyLegalName END,
		CASE CompanyName WHEN '' THEN NULL ELSE CompanyName END
	)
	+')' [ToName],
	COALESCE
	(
		CASE ContactMail WHEN '' THEN NULL ELSE ContactMail END,
		CASE AlertEmail WHEN '' THEN NULL ELSE AlertEmail END,
		CASE ReportMail WHEN '' THEN NULL ELSE ReportMail END,
		CASE RiskMailCcNotifyEmail WHEN '' THEN NULL ELSE RiskMailCcNotifyEmail END,
		CASE ChargebackNotifyMail WHEN '' THEN NULL ELSE ChargebackNotifyMail END,
		CASE PassNotifyEmail WHEN '' THEN NULL ELSE PassNotifyEmail END,
		CASE merchantSupportEmail WHEN '' THEN NULL ELSE merchantSupportEmail END,
		CASE Mail WHEN '' THEN NULL ELSE Mail END
	) [ToMail],
	IsNull(ContactMail, '') [ContactMail],
	IsNull(AlertEmail, '') [AlertMail],
	IsNull(ReportMail, '') [ReportMail],
	IsNull(RiskMailCcNotifyEmail, '') [NotifyRiskCcMail],
	IsNull(ChargebackNotifyMail, '') [NotifyChbMail],
	IsNull(PassNotifyEmail, '') [NotifyPassMail],
	IsNull(merchantSupportEmail, '') [SupportMail],
	Mail [Login Mail],
	IsNull(ContactName, '') [Contact Name]
FROM
	tblCompany m WITH (NOLOCK)
	INNER JOIN GetGlobalData(44) s ON m.ActiveStatus=s.GD_ID


GO
