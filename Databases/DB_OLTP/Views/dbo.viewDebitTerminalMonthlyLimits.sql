SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewDebitTerminalMonthlyLimits]
AS
SELECT
	DebitCompany DebitCompanyID,
	ID TerminalID,
	dc_Name,
	dt_Name,
	TerminalNumber,
	CASE WHEN dt_MonthlyLimitCHB>0 THEN dt_MonthlyLimitCHB ELSE dc_MonthlyLimitCHB END Limit,
	CASE WHEN dt_MonthlyLimitCHBNotifyUsers='' THEN dc_MonthlyLimitCHBNotifyUsers ELSE dt_MonthlyLimitCHBNotifyUsers END NotifyUsers,
	CASE WHEN dt_MonthlyLimitCHBNotifyUsersSMS='' THEN dc_MonthlyLimitCHBNotifyUsersSMS ELSE dt_MonthlyLimitCHBNotifyUsersSMS END NotifyUsersSMS,
	dt_MonthlyCHB Quantity,
	dt_MonthlyCHBWasSent WasSent,
	CASE WHEN dt_MonthlyMCLimitCHB>0 THEN dt_MonthlyMCLimitCHB ELSE dc_MonthlyMCLimitCHB END LimitMC,
	CASE WHEN dt_MonthlyMCLimitCHBNotifyUsers='' THEN dc_MonthlyMCLimitCHBNotifyUsers ELSE dt_MonthlyMCLimitCHBNotifyUsers END NotifyUsersMC,
	CASE WHEN dt_MonthlyMCLimitCHBNotifyUsersSMS='' THEN dc_MonthlyMCLimitCHBNotifyUsersSMS ELSE dt_MonthlyMCLimitCHBNotifyUsersSMS END NotifyUsersSMSMC,
	dt_MonthlyMCCHB QuantityMC,
	dt_MonthlyMCCHBWasSent WasSentMC
FROM
	tblDebitTerminals
	INNER JOIN tblDebitCompany ON DebitCompany=DebitCompany_ID
WHERE
	(dt_MonthlyLimitCHBNotifyUsers<>'' OR dt_MonthlyLimitCHBNotifyUsersSMS<>'' OR dc_MonthlyMCLimitCHBNotifyUsers<>'' OR dc_MonthlyMCLimitCHBNotifyUsersSMS<>'')
	AND
	(dt_MonthlyLimitCHB>0 OR dc_MonthlyLimitCHB>0 OR dt_MonthlyMCLimitCHB>0 OR dc_MonthlyMCLimitCHB>0)
	AND
	(dt_MonthlyCHBWasSent=0 OR dt_MonthlyMCCHBWasSent=0)
	AND
	IsActive=1
GO
