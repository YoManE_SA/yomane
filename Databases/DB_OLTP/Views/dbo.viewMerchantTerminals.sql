SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewMerchantTerminals]
AS
SELECT DISTINCT
	CCFT_CompanyID MerchantID,
	DebitCompany DebitCompanyID
FROM
	tblCompanyCreditFeesTerminals t WITH (NOLOCK)
	INNER JOIN tblDebitTerminals dt WITH (NOLOCK) ON t.CCFT_Terminal=dt.terminalNumber
GO
