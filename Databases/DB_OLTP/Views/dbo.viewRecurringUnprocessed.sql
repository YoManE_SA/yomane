SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewRecurringUnprocessed] AS
SELECT
	tblRecurringCharge.*,
	tblRecurringSeries.rs_Company
FROM
	tblCompany
	INNER JOIN tblRecurringSeries ON tblCompany.ID=rs_Company
	INNER JOIN tblRecurringCharge ON tblRecurringSeries.ID=rc_Series
WHERE
	rc_Suspended=0 AND rc_Blocked=0 AND rc_Paid=0 AND rc_Pending=0
	AND
	rs_Suspended=0 AND rs_Blocked=0 AND rs_Paid=0 AND rs_Deleted=0
	AND
	DateDiff(d, rc_date, GetDate())>0
	AND
	ActiveStatus>=20
GO
