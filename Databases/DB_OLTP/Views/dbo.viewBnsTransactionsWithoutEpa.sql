SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewBnsTransactionsWithoutEpa]
AS
SELECT
	t.companyID [Merchant],
	Left(CompanyName, 25) [Merchant Name],
	dc.dc_name [Bank],
	''''+t.TerminalNumber+'''' [Terminal No],
	Left(''''+dt_ContractNumber+'''', 14) [Contract No.],
	Left(LTrim(RTrim(Str(t.ID))), 10) [Trans ID],
	Convert(char(16), t.InsertDate, 103) [Trans Date],
	Left(DebitReferenceCode, 15) [Debit Ref],
	Left(''''+ApprovalNumber+'''', 10) [Auth No],
	[CurrencyISOCode] [Currency],
	Convert(varchar(10), t.Amount, 0) [Amount],
	CCard_First6 [BIN],
	Left(PaymentMethodDisplay, 20) [Credit Card]
FROM
	tblCompanyTransPass t WITH (NOLOCK)
	INNER JOIN [List].[CurrencyList] AS cu WITH (NOLOCK) ON t.Currency=cu.[CurrencyID]
	INNER JOIN tblDebitCompany dc WITH (NOLOCK) ON t.DebitCompanyID=dc.DebitCompany_ID
	INNER JOIN tblDebitTerminals dt WITH (NOLOCK) ON t.TerminalNumber=dt.terminalNumber
	INNER JOIN tblCompany m WITH (NOLOCK) ON t.companyID=m.ID
	INNER JOIN tblCreditCard cc WITH (NOLOCK) ON t.CreditCardID=cc.ID
WHERE
	dc.DebitCompany_ID IN (18, 46)
	AND
	t.DebitCompanyID IN (18, 46)
	AND
	dt.DebitCompany IN (18, 46)
	AND
	t.CreditType>0
	AND
	t.ID NOT IN (SELECT TransID FROM tblLogImportEPA WHERE IsPaid=1)
	AND
	DateDiff(day, t.InsertDate, GETDATE()) BETWEEN CASE t.Currency WHEN 2 THEN 10 ELSE 17 END AND 30
	AND
	(t.IsChargeback=0 OR t.OriginalTransID=0)

GO
