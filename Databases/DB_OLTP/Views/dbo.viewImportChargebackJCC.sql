SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[viewImportChargebackJCC]
AS
SELECT
	19 FileDebitCompany,
	(SELECT dc_Name FROM tblDebitCompany WHERE DebitCompany_ID=19) FileDebitCompanyName,
	dbo.fnFormatCcNumToHideDigits(CARD_NUMBER) CardNumberPartial,
	LTrim(RTrim(Str(tblCompanyTransPass.ID))) TransList,
	'<'+'a href="merchant_data.asp?companyID='+LTrim(RTrim(Str(tblCompany.ID)))+'" target="_blank">'+CompanyName+'<'+'/a>' MerchantList,
	IsNull(tblCreditCardBIN.PaymentMethod, 20) PaymentMethod,
	GD_Text PaymentMethodName,
	IsNull(tblCreditCardBIN.isoCode, '--') CountryISO,
	IsNull(c.Name, 'Country Unknown') CountryName,
	Convert(datetime, TRANSACTION_DATE, 3) TransactionDate,
	cu.[Name] CurrencyIsoName,
	REASON_CODE Reason,
	tblImportChargebackJCC.*
FROM
	tblImportChargebackJCC WITH (NOLOCK)
	INNER JOIN [List].[CurrencyList] AS cu WITH (NOLOCK) ON LTrim(RTrim(CURRENCY_CODE))=LTrim(RTrim(Str(cu.[ISONumber])))
	INNER JOIN tblCreditCardBIN WITH (NOLOCK) ON Left(CARD_NUMBER, 6)=BIN
	INNER JOIN [List].[CountryList] AS c WITH (NOLOCK) ON tblCreditCardBIN.isoCode=c.[CountryISOCode]
	INNER JOIN tblGlobalData WITH (NOLOCK) ON GD_Lng=1 AND GD_Group=42 AND IsNull(tblCreditCardBIN.PaymentMethod, 20)=GD_ID
	INNER JOIN tblCompanyTransPass WITH (NOLOCK) ON DebitReferenceCode=TRN_ID AND DeniedStatus=0 AND IsTestOnly=0 AND DebitCompanyID=19 AND AUTHORIZATION_CODE IN ('', ApprovalNumber)
		AND tblCompanyTransPass.InsertDate BETWEEN DateAdd(d, -1, Convert(datetime, TRANSACTION_DATE, 3)) AND DateAdd(d, 2, Convert(datetime, TRANSACTION_DATE, 3))
	INNER JOIN tblCompany WITH (NOLOCK) ON tblCompanyTransPass.CompanyID=tblCompany.ID
WHERE
	icb_IsProcessed=0


GO
