SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewCustomerCards]
AS
SELECT
	ID,
	CustomerID,
	PaymentMethod,
	GD_Text PaymentMethodName,
	dbo.fnFormatCcNumToHideDigits(dbo.GetDecrypted256(CCard_Number256)) CardNumber,
	LTrim(RTrim(CCard_ExpMM))+'/'+(CASE WHEN Len(LTrim(RTrim(CCard_ExpYY)))<3 THEN '20' ELSE '' END)+LTrim(RTrim(CCard_ExpYY)) CardValidity
FROM
	tblCustomerCC
	LEFT JOIN tblGlobalData ON tblCustomerCC.PaymentMethod=tblGlobalData.GD_ID
WHERE
	tblGlobalData.GD_Group=42 AND tblGlobalData.GD_LNG=1

GO
