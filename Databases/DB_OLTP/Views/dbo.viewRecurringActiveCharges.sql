SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewRecurringActiveCharges]
AS
    SELECT  rc.ID ,
            rc.rc_Series ,
            rc.rc_ChargeNumber ,
            rc.rc_Date ,
            rc.rc_CreditCard ,
            rc.rc_ECheck ,
            rc.rc_Comments ,
            rc.rc_Suspended ,
            rc.rc_Blocked ,
            rc.rc_Paid ,
            rc.rc_Amount ,
            rc.rc_Currency ,
            rc.rc_Attempts ,
            rc.rc_Pending ,
            rc.rc_SeriesTemp ,
            rs.rs_Company ,
            rs.rs_Approval
    FROM    tblRecurringCharge AS rc WITH ( NOLOCK )
            INNER JOIN tblRecurringSeries AS rs WITH ( NOLOCK ) ON rc_Series = rs.ID
    WHERE   rs.rs_Deleted = 0
            AND rs.rs_Suspended = 0
            AND rs.rs_Blocked = 0
            AND rs.rs_Paid = 0
            AND rc.rc_Suspended = 0
            AND rc.rc_Blocked = 0
            AND rc.rc_Paid = 0
            AND rs.rs_Company IN ( SELECT   ID
                                   FROM     tblCompany WITH ( NOLOCK )
                                   WHERE    ActiveStatus >= 20 )

GO
