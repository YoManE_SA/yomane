SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[viewRecurringAdmin] AS
SELECT
       rs.ID,
       tblCompany.ID MerchantID,
       tblCompany.CompanyName MerchantName,
       rs_ChargeCount,
       LTrim(RTrim(Str(rs_IntervalLength)))+' '+
              CASE rs_IntervalUnit
                     WHEN 1 THEN CASE rs_IntervalLength WHEN 1 THEN 'day' ELSE 'days' END
                     WHEN 2 THEN CASE rs_IntervalLength WHEN 1 THEN 'week' ELSE 'weeks' END
                     WHEN 3 THEN CASE rs_IntervalLength WHEN 1 THEN 'month' ELSE 'months' END
                     WHEN 4 THEN CASE rs_IntervalLength WHEN 1 THEN 'year' ELSE 'years' END
                     ELSE 'Unknown'
              END IntervalText,
       Convert(varchar(10), rs_StartDate, 3) CreateDate,
       CASE LTrim(RTrim(rs_Comments))
              WHEN '' THEN
                     '<img src="../Images/img/icon_commentOff.gif" border="0" alt="" title="" />'
              ELSE
                     '<a href="#" onclick="alert('''+Replace(Replace(rs_Comments, '"', '``'), '''', '`')
                     +''');return false;"><img src="../Images/img/icon_commentOn.gif" border="0" alt="'
                     +Replace(Replace(rs_Comments, '"', '``'), '''', '`')+'" title="'+Replace(Replace(rs_Comments, '"', '``'), '''', '`')+'" /></a>'
       END Memo,
       CASE rs_Approval WHEN 1 THEN 'Approval' ELSE 'Regular' END TransType,
       rs_Suspended,
       rs_Blocked,
       rs_Deleted,
       rs_Paid,
       rs_Currency,
       cu.[CurrencyISOCode] CurrencyName,
       rs_ChargeAmount,
       Cast(Round(rs_ChargeAmount, 2) as nvarchar(10))+' '+cu.[CurrencyISOCode] Amount,
       rs_ECheck,
       cc.CCTypeID,
       cc.BINCountry,
       CASE rs_ECheck
              WHEN 0 THEN LTrim(RTrim(Str(cc.CCard_First6)))+'XXXXXX'+IsNull(Right('000'+LTrim(RTrim(Str(cc.CCard_Last4))), 4), 0)
              ELSE 'echeck'
       END CardNumber,
       Convert(varchar(10), tblRecurringFirst.rc_Date, 3) ChargeDateFirst,
       Convert(varchar(10), tblRecurringLast.rc_Date, 3) ChargeDateLast,
       Convert(varchar(10), (SELECT Min(rc_Date) FROM tblRecurringCharge WHERE rc_Series=rs.ID AND rc_ChargeNumber>1 AND rc_Date>=GetDate() AND ID NOT IN (SELECT ra_Charge FROM tblRecurringAttempt)), 3) ChargeDateNext,
       ra.ra_TransPass TransPassFirst
FROM
       tblRecurringSeries as rs
       INNER JOIN [List].[CurrencyList] AS cu ON rs_Currency=cu.[CurrencyID]
       INNER JOIN tblCompany ON rs_Company=tblCompany.ID
       INNER JOIN tblRecurringCharge as tblRecurringFirst ON rs.ID=tblRecurringFirst.rc_Series
       INNER JOIN tblRecurringCharge as tblRecurringLast ON rs.ID=tblRecurringLast.rc_Series
       LEFT JOIN tblRecurringAttempt as ra ON tblRecurringFirst.ID=ra.ra_Charge
       LEFT JOIN tblCreditCard as cc ON rs_CreditCard=cc.ID
WHERE
       tblRecurringFirst.rc_ChargeNumber=1
       AND
       tblRecurringLast.rc_ChargeNumber=rs_ChargeCount


GO
