SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewCurrencies] AS
SELECT TOP 100 PERCENT
	tblE.GD_ID ID,
	tblE.GD_Text ISO,
	tblH.GD_Text NameHeb
FROM
	tblGlobalData tblE
	INNER JOIN tblGlobalData tblH ON (tblE.GD_ID=tblH.GD_ID AND tblE.GD_Group=tblH.GD_Group)
WHERE
	tblE.GD_Group=1
	AND
	tblE.GD_LNG=1
	AND
	tblH.GD_LNG=0
ORDER BY
	tblE.GD_ID
GO
