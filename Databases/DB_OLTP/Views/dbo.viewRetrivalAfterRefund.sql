SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE View [dbo].[viewRetrivalAfterRefund] as
Select t.DebitCompany, t.dt_ContractNumber, t.dt_BankExternalID, t.id as 'TRM_ID', t.terminalNumber as 'TRM_Number', 
rr.RR_BankCaseID, 
CONVERT(nvarchar, cc.CCard_First6) + 'XXXXXX' + CONVERT(nvarchar, cc.CCard_Last4) as CardDispaly, cc.Member as CardOwner, 
o.ID as ORG_ID, o.InsertDate as ORG_Date, o.ApprovalNumber as ORG_Approval, o.Amount as ORG_Amount, o.Currency as ORG_Currency, o.DebitReferenceCode as ORG_RefCode, 
r.ID as REF_ID, r.InsertDate as REF_Date, r.ApprovalNumber as REF_Approval, r.Amount as REF_Amount, r.Currency as REF_Currency, r.DebitReferenceCode as REF_RefCode,
cmp.ID as CompanyID, cmp.URL as CMP_Url, cmp.merchantSupportPhoneNum + ' ' + cmp.merchantSupportEmail as CMP_Support
From tblRetrivalRequests rr
Left Join tblCompanyTransPass o ON(rr.TransPass_ID = o.ID)
Left Join tblCompanyTransPass r ON(r.OriginalTransID = rr.TransPass_ID And r.deniedStatus=0 and r.CreditType = 0)
Left Join tblDebitTerminals t ON(t.terminalNumber = o.TerminalNumber)
Left Join tblCreditCard cc ON(cc.ID = o.CreditCardID)
Left Join tblCompany cmp ON(cmp.ID = o.companyID)
Where rr.RR_InsertDate > r.InsertDate 
GO
