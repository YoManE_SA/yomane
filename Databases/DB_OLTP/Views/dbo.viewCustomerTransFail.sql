SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[viewCustomerTransFail]
AS
SELECT
	IsNull(tblCompany.CompanyName, '') MerchantName,
	IsNull(List.TransSource.NameEng, '') SourceName,
	IsNull(List.TransSource.NameHeb, '') SourceNameHeb,
	IsNull(tblDebitCompanyCode.DescriptionCustomerEng, '') ReplyText,
	IsNull(tblDebitCompanyCode.DescriptionCustomerHeb, '') ReplyTextHeb,
	tblCompanyTransFail.id,
	tblCompanyTransFail.Comment,
	tblCompanyTransFail.ReplyCode,
	tblCompanyTransFail.Payments,
	tblCompanyTransFail.PaymentMethodDisplay,
	tblCompanyTransFail.InsertDate,
	tblCompanyTransFail.Currency,
	tblCompanyTransFail.Amount,
	tblCompanyTransFail.CustomerID
FROM
	tblCompanyTransFail
	LEFT JOIN tblCompany ON tblCompanyTransFail.CompanyID=tblCompany.ID
	LEFT JOIN List.TransSource ON tblCompanyTransFail.TransactionTypeID=List.TransSource.TransSource_id
	LEFT JOIN tblDebitCompanyCode ON (tblCompanyTransFail.ReplyCode=tblDebitCompanyCode.Code AND tblCompanyTransFail.DebitCompanyID=tblDebitCompanyCode.DebitCompanyID)
WHERE
	tblCompanyTransFail.CustomerID>0


GO
