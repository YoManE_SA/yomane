SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewInvoices] AS
SELECT
	tblInvoiceDocument.*,
	IsNull(tblCompany.CompanyName, '---') MerchantName,
	Left(id_BillingCompanyName, 1) BillingCompanyFirstLetter,
	tblGlobalData.GD_Text,
	cu.Symbol,
	ROW_NUMBER() OVER
	(
		PARTITION BY id_Type, id_BillingCompanyID
		ORDER BY id_InvoiceNumber DESC
	) InvoiceOrderNumber
FROM
	tblInvoiceDocument
	LEFT JOIN tblCompany ON tblInvoiceDocument.id_MerchantID=tblCompany.ID
	LEFT JOIN tblGlobalData ON tblInvoiceDocument.id_Type=tblGlobalData.GD_ID
	LEFT JOIN [List].[CurrencyList] AS cu ON tblInvoiceDocument.id_Currency=cu.[CurrencyID]
WHERE
	tblGlobalData.GD_Group=60
	AND
	tblGlobalData.GD_LNG=1
GO
