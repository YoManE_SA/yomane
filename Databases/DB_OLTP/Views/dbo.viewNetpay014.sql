SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewNetpay014]
AS
    SELECT  m.ID ,
            m.CompanyName ,
            GD_Text CurrentStatus ,
            s.Name ServiceName ,
            COUNT(*) TransPassCount ,
            MAX(t.InsertDate) LastTransPass
    FROM    tblCompany m WITH ( NOLOCK )
            INNER JOIN tblCompanyTransPass t WITH ( NOLOCK ) ON m.ID = t.companyID
            LEFT JOIN GetGlobalData(44) ON ActiveStatus = GD_ID
            LEFT JOIN List.TransSource s ON TransactionTypeID = s.TransSource_id
    WHERE   TerminalNumber = '5762014'
    GROUP BY m.ID ,
            s.Name ,
            GD_Text ,
            m.CompanyName
GO
