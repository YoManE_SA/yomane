SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewSummaryBns] AS
SELECT
	IsNull(d.ContractNo, s.ContractNo) ContractNo,
	IsNull(d.ID, s.ID) ID,
	IsNull(d.CompanyName, s.CompanyName) CompanyName,
	IsNull(d.CurrentStatus, s.CurrentStatus) CurrentStatus,
	IsNull(d.Cur, s.Cur) Cur,
	IsNull(s.SaleAmount, 0) SaleAmount,
	IsNull(s.SaleCount, 0) SaleCount,
	s.FirstSale,
	s.LastSale,
	IsNull(d.DeclineAmount, 0) DeclineAmount,
	IsNull(d.DeclineCount, 0) DeclineCount,
	d.FirstDecline,
	d.LastDecline
FROM
	(
		select
			IsNull(d.ContractNo, da.ContractNo) ContractNo,
			IsNull(d.ID, da.ID) ID,
			IsNull(d.CompanyName, da.CompanyName) CompanyName,
			IsNull(d.CurrentStatus, da.CurrentStatus) CurrentStatus,
			IsNull(d.Cur, da.Cur) Cur,
			IsNull(d.DeclineAmount, 0)+IsNull(da.DeclineAmount, 0) DeclineAmount,
			IsNull(d.DeclineCount, 0)+IsNull(da.DeclineCount, 0) DeclineCount,
			IsNull(da.FirstDecline, d.FirstDecline) FirstDecline,
			IsNull(d.LastDecline, da.LastDecline) LastDecline
		from
			viewSummaryBnsDeclines d
			full join viewSummaryBnsDeclinesArchive da on d.ID=da.ID and d.Cur=da.Cur
	) d
	FULL JOIN viewSummaryBnsSales s ON d.ID=s.ID and d.Cur=s.Cur and d.ContractNo=s.ContractNo
GO
