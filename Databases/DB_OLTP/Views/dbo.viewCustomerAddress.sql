SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewCustomerAddress]
AS
SELECT
	tblCustomer.ID,
	tblCustomer.CustomerNumber,
	tblCustomer.Street,
	tblCustomer.City,
	tblCustomer.ZIP,
	tblCustomer.Country,
	tblCustomer.Phone,
	tblCustomer.State,
	IsNull(s.name, '') StateNameEng,
	IsNull(s.name, '') StateNameHeb,
	IsNull(s.StateISOCode, '') StateIsoCode,
	IsNull(c.Name, '') CountryNameEng,
	IsNull(c.Name, '') CountryNameHeb
FROM
	tblCustomer
	LEFT JOIN [List].[StateList] AS s ON tblCustomer.State = s.[StateID]
	LEFT JOIN [List].[CountryList] AS c ON tblCustomer.Country = c.[CountryID]

GO
