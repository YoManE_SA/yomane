SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewCustomerProfile]
AS
SELECT
	ID,
	FirstName,
	LastName,
	Phone,
	CellPhone,
	IDNumber,
	Street,
	City,
	Zip,
	Country,
	State,
	DOB
FROM
	tblCustomer
GO
