SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vwQNAGroup]
AS
SELECT  q.* ,
        ( SELECT    COUNT(*)
          FROM      dbo.QNA WITH ( NOLOCK )
          WHERE     QNAGroup_id = q.QNAGroup_id
        ) AS Questions ,
        l.Name AS LanguageName
FROM    [dbo].[QNAGroup] AS q
        LEFT JOIN [List].[LanguageList] AS l ON q.Language_id = l.Language_id



GO
