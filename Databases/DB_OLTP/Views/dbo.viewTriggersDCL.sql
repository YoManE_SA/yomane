SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewTriggersDCL]
AS
SELECT
	tbl.id ID,
	tbl.name+CASE IsNull(trg.name, '') WHEN '' THEN '' ELSE ' ['+Substring(trg.name, Len(trg.name)+2-charindex('_', Reverse(trg.name)), 3)+']' END Name
FROM
	sysobjects tbl
	LEFT JOIN sysobjects trg ON trg.name like 'trgDCL'+LTrim(RTrim(Str(tbl.id)))+'@_%' ESCAPE '@'
WHERE
	tbl.xtype='U'
	AND
	IsNull(trg.xtype, 'TR')='TR'
	AND
	tbl.name<>'dtproperties'
GO
