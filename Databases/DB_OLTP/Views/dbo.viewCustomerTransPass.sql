SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[viewCustomerTransPass]
AS
SELECT
	IsNull(tblCompany.CompanyName, '') MerchantName,
	IsNull(List.TransSource.NameEng, '') SourceName,
	IsNull(List.TransSource.NameHeb, '') SourceNameHeb,
	IsNull(tblDebitCompanyCode.DescriptionCustomerEng, '') ReplyText,
	IsNull(tblDebitCompanyCode.DescriptionCustomerHeb, '') ReplyTextHeb,
	tblCompanyTransPass.id,
	tblCompanyTransPass.Comment,
	tblCompanyTransPass.ReplyCode,
	tblCompanyTransPass.Payments,
	tblCompanyTransPass.PaymentMethodDisplay,
	tblCompanyTransPass.InsertDate,
	tblCompanyTransPass.Currency,
	tblCompanyTransPass.Amount,
	tblCompanyTransPass.CustomerID
FROM
	tblCompanyTransPass
	LEFT JOIN tblCompany ON tblCompanyTransPass.CompanyID=tblCompany.ID
	LEFT JOIN List.TransSource ON tblCompanyTransPass.TransactionTypeID=List.TransSource.TransSource_id
	LEFT JOIN tblDebitCompanyCode ON (tblCompanyTransPass.ReplyCode=tblDebitCompanyCode.Code AND tblCompanyTransPass.DebitCompanyID=tblDebitCompanyCode.DebitCompanyID)
WHERE
	tblCompanyTransPass.CustomerID>0


GO
