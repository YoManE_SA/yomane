SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[viewImportChargebackExcel]
AS
SELECT
	m.ID MerchantID,
	CompanyName MerchantName,
	t.ID TransID,
	t.DebitReferenceCode TransDebitRef,
	t.Amount TransAmount,
	cu.[CurrencyISOCode] TransCurrency,
	r.ID RefundID,
	r.DebitReferenceCode RefundDebitRef,
	cast(r.InsertDate AS date) RefundDate,
	r.Amount RefundAmount,
	rc.[CurrencyISOCode] RefundCurrency,
	CASE t.DeniedStatus WHEN 0 THEN NULL ELSE t.DeniedStatus END ChargebackStatus,
	cast(CASE WHEN t.DeniedStatus=0 OR t.DeniedDate<'20010101' THEN NULL ELSE t.DeniedDate END AS date) ChargebackDate,
	t.DeniedAdminComment ChargebackComment,
	v.MRF,
	v.RSN Reason,
	CAST(SubString(v.icb_FileName, CASE WHEN v.icb_FileName LIKE 'CS_%' THEN 13 WHEN v.icb_FileName LIKE 'TSPUK%' THEN 64 ELSE 65 END, 8) AS DATE) FileDate,
	v.[CASE ID] CaseID
FROM
	viewImportChargeback v WITH (NOLOCK)
	INNER JOIN tblCompanyTransPass t WITH (NOLOCK) ON Cast(v.TransList AS int)=t.ID
	INNER JOIN [List].[CurrencyList] cu WITH (NOLOCK) ON t.Currency=cu.[CurrencyID]
	INNER JOIN tblCompany m WITH (NOLOCK) ON t.companyID=m.ID
	LEFT JOIN tblCompanyTransPass r WITH (NOLOCK) ON t.ID=r.OriginalTransID AND r.CreditType=0 AND r.DeniedStatus=0
	LEFT JOIN [List].[CurrencyList] rc WITH (NOLOCK) ON r.Currency=rc.[CurrencyID]
WHERE
	Len(v.TransList)>0 AND v.TransList NOT LIKE '%,%'





GO
