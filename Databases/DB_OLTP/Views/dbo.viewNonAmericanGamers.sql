SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[viewNonAmericanGamers] 
AS
SELECT DISTINCT TOP 100 PERCENT
	email [Email Address],
	Member [Card Holder],
	pm.Name [Card Type],
	Count(*) [Transaction Count],
	Min(t.InsertDate) [First Transaction],
	Max(t.InsertDate) [Last Transaction]
FROM
	tblCompany m
	INNER JOIN tblCompanyTransPass t ON m.ID=t.companyID
	INNER JOIN tblCreditCard c ON t.CreditCardID=c.ID
	INNER JOIN List.PaymentMethod pm ON ccTypeID+20=pm.PaymentMethod_id
WHERE
	CompanyIndustry_id=4
	AND
	t.PaymentMethod BETWEEN 20 AND 99
	AND
	BINCountry NOT IN ('US', 'VI', 'UM')
	AND
	isTestOnly=0
	AND
	CreditType>0
	AND
	dbo.IsCHB(DeniedStatus, 0)=0
GROUP BY
	email,
	Member,
	pm.Name
ORDER BY
	[Transaction Count] DESC,
	[Card Type],
	[Card Holder],
	[Email Address]

GO
