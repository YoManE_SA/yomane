SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoadWireBankAccountLocal](@nWire int, @nMerchant int=NULL)  
AS  
BEGIN  
 SELECT  
  wirePaymentMethod PaymentType,  
  IsNull(wirePaymentBank, 0) Bank,
  wireIdNumber,  
  wirePaymentBranch BranchNumber,  
  wirePaymentPayeeName Beneficiary,  
  wirePaymentAccount AccountNumber  
 FROM  
  tblWireMoney  
 WHERE  
  WireMoney_ID=@nWire AND ISNULL(@nMerchant, Company_ID)=Company_ID  
END
GO
