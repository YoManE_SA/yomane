SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CheckDebitCompanyRules](@bDebug as bit=0) AS
BEGIN
	EXEC DebitBlockLogWrite 'RULE CHECK', 'Rule check started'
	DECLARE @nID int, @nDebitCompany int, @sReplyCodes nvarchar(100), @sNotifyUsers nvarchar(100), @sNotifyUsersSMS nvarchar(100),
		@nAttemptCount int, @nFailCount int, @bIsAutoDisable bit, @bIsAutoEnable bit, @nAutoEnableMinutes int, @nAutoEnableAttempts int,
		@dtLastUnblockDate datetime, @sDebitCompany nvarchar(50), @sSubject nvarchar(100), @sBody nvarchar(1000),
		@nFailures int, @dtLastFailDate datetime, @dtUnblockDate datetime, @sDebitTerminal nvarchar(20), @sLogText nvarchar(950),
		@nDebitCompanyPrev int, @sEmergencyContact nvarchar(50), @sEmergencyPhone nvarchar(50)
	SET @nDebitCompanyPrev=0
	DECLARE curRules CURSOR FOR
		SELECT
			tblDebitRule.ID, dr_DebitCompany, dr_ReplyCodes, dr_NotifyUsers, dr_NotifyUsersSMS, dr_AttemptCount, dr_FailCount,
			dr_IsAutoDisable, dr_IsAutoEnable, dr_AutoEnableMinutes, dr_AutoEnableAttempts, dr_LastUnblockDate, dc_name, dr_LastFailDate
		FROM
			tblDebitRule
			INNER JOIN tblDebitCompany ON dr_DebitCompany=DebitCompany_ID
		WHERE
			dr_IsActive=1
			AND
			dr_LastFailDate>DateAdd(mi, -10, Getdate())
			AND
			ID NOT IN (SELECT db_DebitRule FROM tblDebitBlock WHERE db_UnblockDate>=GetDate() OR db_AutoEnableAttempts>db_CountAutoEnableAttempts)
		ORDER BY
			dr_DebitCompany,
			dr_Rating
	OPEN curRules
	FETCH NEXT FROM curRules INTO @nID, @nDebitCompany, @sReplyCodes, @sNotifyUsers, @sNotifyUsersSMS, @nAttemptCount,	@nFailCount,
		@bIsAutoDisable, @bIsAutoEnable, @nAutoEnableMinutes, @nAutoEnableAttempts, @dtLastUnblockDate,
		@sDebitCompany, @dtLastFailDate
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @sLogText='Checking Rule '+Cast(@nID AS nvarchar(10))
		EXEC DebitBlockLogWrite 'RULE CHECK', @sLogText, @nID, @nDebitCompany
		IF @nDebitCompanyPrev<>@nDebitCompany
		BEGIN
			SET @nFailures=dbo.GetReplyCodeCount(@nDebitCompany, @nAttemptCount, @sReplyCodes, DEFAULT)
			IF @nFailures>=@nFailCount
			BEGIN
				SET @sLogText='Rule '+Cast(@nID AS nvarchar(10))+' ('+@sDebitCompany+', codes '+@sReplyCodes+') - '
					+Cast(@nFailures AS nvarchar(10))+'/'+Cast(@nAttemptCount AS nvarchar(10))
				EXEC DebitBlockLogWrite 'RULE APPLIED', @sLogText, @nID, @nDebitCompany
				SET @nDebitCompanyPrev=@nDebitCompany
				IF @bIsAutoDisable=1
				BEGIN
					SET @dtUnblockDate=DateAdd(mi, CASE @bIsAutoEnable WHEN 0 THEN -1 ELSE @nAutoEnableMinutes END, @dtLastFailDate)
					SET @sDebitTerminal=dbo.GetLastFailTerminalNumber(@nDebitCompany, @sReplyCodes);
					INSERT INTO tblDebitBlock
						(db_DebitCompany, db_DebitTerminalNumber, db_DebitRule, db_IsAutoEnable, db_UnblockDate, db_AutoEnableAttempts)
					VALUES
						(@nDebitCompany, @sDebitTerminal, @nID, @bIsAutoEnable, @dtUnblockDate, @nAutoEnableAttempts);
					UPDATE tblDebitCompany SET dc_TempBlocks=dc_TempBlocks+1 WHERE DebitCompany_ID=@nDebitCompany
					SET @sLogText='Rule '+Cast(@nID AS nvarchar(10))+', unblock on '+Convert(nvarchar(10), @dtUnblockDate, 8)
						+' after '+Cast(@nAutoEnableAttempts as nvarchar(10))+' attempts.';
					EXEC DebitBlockLogWrite	'BLOCK CREATED', @sLogText, @nID, @nDebitCompany
				END
				IF @sNotifyUsers<>'' OR @sNotifyUsersSMS<>''
					SELECT
						@sEmergencyContact=dc_EmergencyContact,
						@sEmergencyPhone=dc_EmergencyPhone
					FROM
						tblDebitCompany
					WHERE
						DebitCompany_ID=@nDebitCompany;
				IF @sNotifyUsers<>''
				BEGIN
					SET @sSubject=@sDebitCompany+' '+CASE @bIsAutoDisable WHEN 1 THEN 'Block' ELSE 'Warning' END+' At '+Cast(ServerProperty('MachineName') AS nvarchar(10))
					SET @sBody='<b>Debit Company '+CASE @bIsAutoDisable WHEN 1 THEN 'Block' ELSE 'Warning' END+'</b><br />'
					SET @sBody=@sBody+'Server: '+Cast(ServerProperty('MachineName') AS nvarchar(10))+'<br />'
					SET @sBody=@sBody+'Sent on: '+Convert(nvarchar(25), GetDate(), 120)+'<br />'
					SET @sBody=@sBody+'Debit company: '+Cast(@nDebitCompany as nvarchar(10))+' '+@sDebitCompany+'<br />'
					SET @sBody=@sBody+'Rule : '+Cast(@nID as nvarchar(10))+'<br />'
					SET @sBody=@sBody+'Reply codes: '+@sReplyCodes+'<br />'
					SET @sBody=@sBody+'Failures: '+Cast(@nFailures as nvarchar(10))+'/'+Cast(@nAttemptCount as nvarchar(10))+'<br />'
					SET @sBody=@sBody+'Emergency phone: '+@sEmergencyPhone+' ('+@sEmergencyContact+')<br />'
					IF @bIsAutoDisable=1
					BEGIN
						IF @bIsAutoEnable=1
							SET @sBody=@sBody+'Unblock: on '+Convert(nvarchar(10), @dtUnblockDate, 8)+' after '+Cast(@nAutoEnableAttempts as nvarchar(10))+' attempts'
						ELSE
							SET @sBody=@sBody+'Unblock not scheduled!'
					END
					EXEC SendMail @sNotifyUsers, @sSubject, @sBody
					SET @sLogText=@sSubject+', rule '+Cast(@nID AS nvarchar(10))+', sent to '+@sNotifyUsers+'<br />'+@sBody
					EXEC DebitBlockLogWrite	'MAIL SENT', @sLogText, @nID, @nDebitCompany
				END
				IF 1=0 AND @sNotifyUsersSMS<>''
				BEGIN
					SET @sBody=@sDebitCompany+' '+CASE @bIsAutoDisable WHEN 1 THEN 'Block' ELSE 'Warning' END
					SET @sBody=@sBody+' at '+Cast(ServerProperty('MachineName') AS nvarchar(10))+'. '
					SET @sBody=@sBody+'Reply codes: '+@sReplyCodes+'. '
					SET @sBody=@sBody+'Failures: '+Cast(@nFailures as nvarchar(10))+'/'+Cast(@nAttemptCount as nvarchar(10))+'. '
					SET @sBody=@sBody+'Emergency phone: '+@sEmergencyPhone+' ('+@sEmergencyContact+'). '
					SET @sBody=@sBody+'Sent on: '+Convert(nvarchar(25), GetDate(), 108)+'. '
					IF @bIsAutoDisable=1
					BEGIN
						IF @bIsAutoEnable=1
							SET @sBody=@sBody+'Unblock: on '+Convert(nvarchar(10), @dtUnblockDate, 8)+' after '+Cast(@nAutoEnableAttempts as nvarchar(10))+' attempts'
						ELSE
							SET @sBody=@sBody+'Unblock not scheduled!'
					END
					EXEC SendSMS @sNotifyUsersSMS, @sBody
					SET @sLogText=@sSubject+', rule '+Cast(@nID AS nvarchar(10))+', sent to '+@sNotifyUsersSMS+'<br />'+@sBody
					EXEC DebitBlockLogWrite	'SMS SENT', @sLogText, @nID, @nDebitCompany
				END
			END
		END
		FETCH NEXT FROM curRules INTO @nID, @nDebitCompany, @sReplyCodes, @sNotifyUsers, @sNotifyUsersSMS,
			@nAttemptCount, @nFailCount, @bIsAutoDisable, @bIsAutoEnable, @nAutoEnableMinutes,
			@nAutoEnableAttempts, @dtLastUnblockDate, @sDebitCompany, @dtLastFailDate
	END
	CLOSE curRules
	DEALLOCATE curRules
	EXEC DebitBlockLogWrite 'RULE CHECK END', 'Rule check complete.'
END

GO
