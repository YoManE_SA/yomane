SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AddCreditCardWhitelist]
(
	@nMerchant int,
	@sCardNumber nvarchar(25),
	@nExpMonth tinyint,
	@nExpYear smallint,
	@sCardHolder nvarchar(100),
	@nLevel tinyint=0,
	@sComment nvarchar(200)='',
	@sIP varchar(50)=''
)
AS
BEGIN
	IF @nMerchant IS NULL OR @sCardNumber IS NULL OR @nExpMonth IS NULL OR @nExpYear IS NULL OR @sCardHolder IS NULL OR @nLevel IS NULL RETURN -1;
	IF @nExpMonth<1 OR @nExpMonth>12 OR @nExpYear<Year(GetDate()) OR @nExpYear>Year(GetDate())+8 RETURN -2;
	IF @nExpYear=Year(GetDate()) AND @nExpMonth<Month(GetDate()) RETURN -3;
	IF LEN(@sCardNumber)<8 RETURN -4;
	SET @sCardNumber = Replace(@sCardNumber,' ','');
	--IF LEN(@sCardNumber)<9 RETURN -5;
	IF NOT EXISTS (SELECT GD_ID FROM GetGlobalData(39) WHERE GD_ID=@nLevel) RETURN -6;
	IF @nMerchant<>0 AND NOT EXISTS (SELECT ID FROM tblCompany WITH (NOLOCK) WHERE ID=@nMerchant AND ActiveStatus=30 AND IsCcWhitelistEnabled=1) RETURN -7;
	DECLARE @binCardNumber varbinary(200);
	SET @binCardNumber = dbo.GetEncrypted256(@sCardNumber);
	IF EXISTS (SELECT * FROM tblCreditCardWhitelist WITH (NOLOCK) WHERE ccwl_Merchant=@nMerchant AND ccwl_CardNumber256=@binCardNumber) RETURN -8;
	INSERT INTO tblCreditCardWhitelist
	(
		ccwl_Bin,
		ccwl_BinCountry,
		ccwl_CardHolder,
		ccwl_CardNumber256,
		ccwl_Comment,
		ccwl_ExpMonth,
		ccwl_ExpYear,
		ccwl_Last4,
		ccwl_Level,
		ccwl_Merchant,
		ccwl_PaymentMethod,
		ccwl_IP
	)
	VALUES
	(
		Cast(Left(@sCardNumber, 6) AS int),
		dbo.GetCCCountry(@sCardNumber),
		@sCardHolder,
		@binCardNumber,
		@sComment,
		@nExpMonth,
		@nExpYear,
		Cast(Right(@sCardNumber, 4) AS smallint),
		@nLevel,
		@nMerchant,
		dbo.GetCCPaymentMethod(@sCardNumber),
		@sIP
	);
	RETURN @@IDENTITY;
END
GO
