SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SaveLogChargeAttemptRequestResponse]
(
	@nLogID int,
	@sRequest nvarchar(max)='',
	@sResponse nvarchar(max)=''
)
AS
BEGIN
	SET NOCOUNT ON;
	IF LEN(@sRequest)>4000 SET @sRequest=LEFT(@sRequest, 4000);
	IF LEN(@sResponse)>4000 SET @sResponse=LEFT(@sResponse, 4000);
	UPDATE
		tblLogChargeAttempts
	SET
		Lca_RequestString=CASE IsNull(@sRequest, '') WHEN '' THEN Lca_RequestString ELSE @sRequest END,
		Lca_ResponseString=@sResponse
	WHERE
		LogChargeAttempts_id=@nLogID;
	RETURN @@ROWCOUNT;
	SET NOCOUNT OFF;
END
GO
