SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [Risk].[spSaveCcMailUsage](@sMail varchar(100), @sCardNumber varchar(25))
AS
BEGIN
	DECLARE @CardNumber256 varbinary(40);
	SET @CardNumber256 = dbo.GetEncrypted256(@sCardNumber);
	
	IF NOT EXISTS (SELECT * FROM Risk.CcMailUsage WHERE EmailAddress = @sMail AND CreditCardNumber256 = @CardNumber256)
	BEGIN
		INSERT INTO Risk.CcMailUsage(EmailAddress, CreditCardNumber256) VALUES (@sMail, @CardNumber256);
		RETURN 1;
	END
	RETURN 0;
END




GO
