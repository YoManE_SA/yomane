SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetUnsentBnsTransactionsWithoutEpa]
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @tbl TABLE
	(
		[Merchant] int,
		[Merchant Name] nvarchar (25),
		[Bank] nvarchar (50),
		[Terminal No] nvarchar (12),
		[Contract No.] nvarchar (14),
		[Trans ID] varchar (10),
		[Trans Date] char (16),
		[Debit Ref] nvarchar (15),
		[Auth No] nvarchar (10),
		[Currency] nchar (3),
		[Amount] varchar (10),
		[BIN] int,
		[Credit Card] nvarchar (20)
	);
	INSERT INTO @tbl SELECT * FROM viewBnsTransactionsWithoutEpa;
	DELETE FROM @tbl WHERE [Trans ID] IN (SELECT TransID FROM tblLogSendBnsTransactionsWithoutEpa);
	INSERT INTO tblLogSendBnsTransactionsWithoutEpa(TransID) SELECT [Trans ID] FROM @tbl;
	SELECT * FROM @tbl;
	SET NOCOUNT OFF;
END
GO
