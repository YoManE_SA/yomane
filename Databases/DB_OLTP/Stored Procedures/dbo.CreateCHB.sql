SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[CreateCHB](@nTransID int, @dtCHB datetime=NULL, @sComment nvarchar(500)='', @nReasonCode int=0)
AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT ID FROM tblCompanyTransPass WHERE ID=@nTransID)
	BEGIN
		SELECT -1;
		RETURN;
	END

	IF NOT EXISTS(SELECT ID FROM tblCompanyTransPass WHERE ID=@nTransID AND DeniedStatus IN (0, 3, 9, 11))
	BEGIN
		SELECT -2;
		RETURN;
	END

	IF @dtCHB IS NULL SET @dtCHB=GetDate();

    DECLARE @nMerchant INT ,
        @nDebitCompanyId INT ,
        @nCurrency INT ,
        @nPaymentMethod INT ,
        @sBIN VARCHAR(20) ,
        @sTerminalNumber VARCHAR(20) ,
        @nFeeCHB MONEY ,
        @nFeeClarify MONEY ,
        @nDebitFeeCHB MONEY ,
        @nDebitFeeCurrency INT;
	
	-- Get trans data
    SELECT  @nMerchant = tblCompanyTransPass.CompanyID ,
            @nDebitCompanyId = tblCompanyTransPass.DebitCompanyID ,
            @nCurrency = Currency ,
            @nPaymentMethod = tblCompanyTransPass.PaymentMethod ,
            @sBIN = ISNULL(LTRIM(RTRIM(STR(CCard_First6))), 'XXXXXX') ,
            @sTerminalNumber = TerminalNumber
    FROM    tblCompanyTransPass
            LEFT JOIN tblCreditCard ON CreditCardID = tblCreditCard.ID
    WHERE   tblCompanyTransPass.ID = @nTransID;

	-- Get Chb fees
	IF (SELECT IsUsingNewTerminal FROM tblCompany WHERE ID=@nMerchant)=1
		-- New terminals
        SELECT  @nFeeClarify = CCR_ClarificationFee ,
                @nFeeCHB = CCR_CBFixedFee
        FROM    tblCompanyCreditRestrictions
                LEFT JOIN tblCompanyPaymentMethods ON CCR_CPM_ID = CPM_ID
        WHERE   CPM_CompanyID = @nMerchant
                AND CPM_CurrencyID = @nCurrency
                AND CPM_PaymentMethod = @nPaymentMethod
                AND CCR_TerminalNumber = @sTerminalNumber
	ELSE
		-- Old terminals
        SELECT TOP 1
                @nFeeClarify = CCF_ClarificationFee ,
                @nFeeCHB = CCF_CBFixedFee
        FROM    tblCompanyCreditFees
        WHERE   CCF_CompanyID = @nMerchant
                AND CCF_CurrencyID = @nCurrency
                AND CCF_PaymentMethod = @nPaymentMethod
                AND ( CCF_ListBINs LIKE '%' + @sBIN + '%'
                      OR CCF_ListBINs = ''
                    )
        ORDER BY CCF_IsDisabled DESC ,
                LEN(CCF_ListBINs) DESC;

	-- Get debit fees
    SELECT TOP 1
            @nDebitFeeCHB = DCF_CBFixedFee ,
            @nDebitFeeCurrency = DCF_CHBCurrency
    FROM    tblDebitCompanyFees
    WHERE   DCF_DebitCompanyID = @nDebitCompanyId
            AND DCF_CurrencyID IN ( 255, @nCurrency )
            AND DCF_PaymentMethod IN ( 0, @nPaymentMethod )
            AND DCF_TerminalNumber IN ( @sTerminalNumber, '' )
    ORDER BY DCF_TerminalNumber DESC ,
            DCF_CurrencyID ASC ,
            DCF_PaymentMethod DESC
            
    IF @nDebitFeeCurrency <> 255 
        SET @nDebitFeeCHB = dbo.ConvertCurrency(@nDebitFeeCHB,
                                                @nDebitFeeCurrency, @nCurrency)

	-- Add to Trans History
	INSERT INTO [Trans].[TransHistory]
	(
	   [Description]
	   ,[InsertDate]
	   ,[IsSucceeded]
	   ,[Merchant_id]
	   ,[ReferenceNumber]
	   ,[TransPass_id]
	   ,[TransHistoryType_id]
	)
	VALUES
	(
		IsNull((SELECT Title FROM tblChargebackReason WHERE ReasonCode=@nReasonCode), ''),
		@dtCHB,
		1,
		@nMerchant,
		@nReasonCode,
		@nTransID,
		4
	);

	IF (SELECT payID FROM tblCompanyTransPass WHERE ID=@nTransID)=';0;'
	BEGIN
		--Unsettled then update status
        UPDATE  tblCompanyTransPass
        SET     DeniedDate = @dtCHB ,
                DeniedStatus = 1 ,
                DeniedAdminComment = @sComment ,
                UnsettledAmount = 0 ,
                netpayFee_chbCharge = @nFeeCHB ,
                netpayFee_ClrfCharge = @nFeeClarify ,
                DebitFeeChb = @nDebitFeeCHB ,
                DeniedValDate = @dtCHB ,
                IsPendingChargeback = CASE IsPendingChargeback
                                        WHEN 1 THEN 0
                                        ELSE IsPendingChargeback
                                      END
        WHERE   ID = @nTransID;
        SELECT  @nTransID;
        RETURN;
	END
	ELSE
	BEGIN
		--Settled then duplicate transaction
		DECLARE @nOriginalDeniedStatus int;

		SELECT @nOriginalDeniedStatus=DeniedStatus FROM tblCompanyTransPass WHERE ID=@nTransID;

		-- Update original transaction
        UPDATE  tblCompanyTransPass
        SET     DeniedDate = @dtCHB ,
                DeniedStatus = 10 ,
                DeniedAdminComment = @sComment ,
                DeniedValDate = @dtCHB ,
                DebitFee = 0 ,
                IsPendingChargeback = CASE IsPendingChargeback
                                        WHEN 1 THEN 0
                                        ELSE IsPendingChargeback
                                      END
        WHERE   ID = @nTransID;

        UPDATE  tblCompanyTransPass
        SET     IsPendingChargeback = CASE IsPendingChargeback
                                        WHEN 1 THEN 0
                                        ELSE IsPendingChargeback
                                      END
        WHERE   OriginalTransID = @nTransID;

		-- Create new duplicated trans
        INSERT  INTO tblCompanyTransPass
                ( companyID ,
                  TransSource_id ,
                  DebitCompanyID ,
                  CustomerID ,
                  FraudDetectionLog_id ,
                  OriginalTransID ,
                  PrimaryPayedID ,
                  PayID ,
                  InsertDate ,
                  Amount ,
                  Currency ,
                  Payments ,
                  CreditType ,
                  IPAddress ,
                  replyCode ,
                  OrderNumber ,
                  Interest ,
                  Comment ,
                  TerminalNumber ,
                  ApprovalNumber ,
                  DeniedDate ,
                  DeniedStatus ,
                  DeniedPrintDate ,
                  DeniedSendDate ,
                  DeniedAdminComment ,
                  PD ,
                  MerchantPD ,
                  PaymentMethodID ,
                  PaymentMethodDisplay ,
                  isTestOnly ,
                  referringUrl ,
                  payerIdUsed ,
                  netpayFee_transactionCharge ,
                  netpayFee_ratioCharge ,
                  DebitReferenceCode ,
                  OCurrency ,
                  OAmount ,
                  netpayFee_chbCharge ,
                  netpayFee_ClrfCharge ,
                  DebitFeeChb ,
                  PaymentMethod ,
                  UnsettledAmount ,
                  UnsettledInstallments ,
                  IPCountry ,
                  HandlingFee ,
                  DebitFee ,
                  RecurringSeries ,
                  RecurringChargeNumber ,
                  DeniedValDate ,
                  MerchantRealPD ,
                  CreditCardID ,
				  TransPayerInfo_id ,
				  TransPaymentMethod_id
			    )
                SELECT  companyID ,
                        TransSource_id ,
                        DebitCompanyID ,
                        CustomerID ,
                        0 FraudDetectionLog_id ,
                        @nTransID OriginalTransID ,
                        0 PrimaryPayedID ,
                        ';0;' PayID ,
                        GETDATE() InsertDate ,
                        Amount ,
                        Currency ,
                        Payments ,
                        CASE CreditType
                          WHEN 0 THEN 1
                          ELSE 0
                        END CreditType ,
                        IPAddress ,
                        replyCode ,
                        OrderNumber ,
                        Interest ,
                        Comment ,
                        TerminalNumber ,
                        ApprovalNumber ,
                        @dtCHB DeniedDate ,
                        6 DeniedStatus ,
                        DeniedPrintDate ,
                        DeniedSendDate ,
                        @sComment DeniedAdminComment ,
                        0 PD ,
                        0 MerchantPD ,
                        PaymentMethodID ,
                        PaymentMethodDisplay ,
                        isTestOnly ,
                        '' referringUrl ,
                        payerIdUsed ,
                        0 netpayFee_transactionCharge ,
                        0 netpayFee_ratioCharge ,
                        DebitReferenceCode ,
                        OCurrency ,
                        OAmount ,
                        @nFeeCHB netpayFee_chbCharge ,
                        CASE @nOriginalDeniedStatus
                          WHEN 11 THEN 0
                          ELSE @nFeeClarify - netpayFee_ClrfCharge
                        END netpayFee_ClrfCharge ,
                        @nDebitFeeCHB ,
                        PaymentMethod ,
                        UnsettledAmount - Amount UnsettledAmount ,
                        1 UnsettledInstallments ,
                        '--' IPCountry ,
                        HandlingFee ,
                        DebitFee ,
                        RecurringSeries ,
                        RecurringChargeNumber ,
                        @dtCHB DeniedValDate ,
                        NULL MerchantRealPD ,
                        CreditCardID ,
						TransPayerInfo_id ,
						TransPaymentMethod_id
                FROM    tblCompanyTransPass
                WHERE   ID = @nTransID;

		SELECT @@IDENTITY;

		RETURN;

	END
END





GO
