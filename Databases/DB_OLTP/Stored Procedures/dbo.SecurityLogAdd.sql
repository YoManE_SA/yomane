SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SecurityLogAdd](@sUsername nvarchar(50), @sUrl nvarchar(100), @bDenied bit, @bReadOnly bit, @sIP varchar(25))
AS
BEGIN
    INSERT INTO tblSecurityLog
    (
        sl_User,
        sl_Document,
        sl_IP,
        sl_IsDenied,
        sl_IsReadOnly
    )
    VALUES
    (
        (SELECT top 1 ID FROM tblSecurityUser WHERE su_Username=@sUsername),
        (SELECT top 1 ID FROM tblSecurityDocument WHERE sd_URL=@sUrl),
        @sIP,
        @bDenied,
        @bReadOnly
    )
END
GO
