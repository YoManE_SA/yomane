SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RemoveMerchantBankAccount](@nMerchant int, @nCurrency int)
AS
DELETE FROM tblMerchantBankAccount WHERE mba_Merchant=@nMerchant AND mba_Currency=@nCurrency;
GO
