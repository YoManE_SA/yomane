SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DebitRuleMove](@nRule int, @bUp bit=0)
AS
BEGIN
	DECLARE @nRating int, @nRating2 int, @nDebitCompany int
	SELECT @nRating=dr_Rating, @nDebitCompany=dr_DebitCompany FROM tblDebitRule WHERE ID=@nRule;
	IF @bUp=1
		SELECT @nRating2=Max(dr_Rating) FROM tblDebitRule WHERE dr_Rating<@nRating AND dr_DebitCompany=@nDebitCompany
	ELSE
		SELECT @nRating2=Min(dr_Rating) FROM tblDebitRule WHERE dr_Rating>@nRating AND dr_DebitCompany=@nDebitCompany
	IF IsNull(@nRating2, 0)>0
	BEGIN
		UPDATE tblDebitRule SET dr_Rating=@nRating WHERE dr_Rating=@nRating2 AND dr_DebitCompany=@nDebitCompany;
		UPDATE tblDebitRule SET dr_Rating=@nRating2 WHERE ID=@nRule;
	END
END
GO
