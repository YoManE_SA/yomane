SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DebitBlockLogWrite](@sType nvarchar(20), @sText nvarchar(950), @nDebitRule int=0, @nDebitCompany int=0, @nDebitTerminal int=0)
AS
BEGIN
	IF @nDebitRule>0 AND @nDebitCompany=0 SET @nDebitCompany=IsNull((SELECT dr_DebitCompany FROM tblDebitRule WHERE ID=@nDebitRule), 0);
	INSERT INTO tblDebitBlockLog
	(
		dbl_Type, dbl_Text, dbl_DebitRule, dbl_DebitCompany, dbl_DebitTerminal
	)
	VALUES
	(
		@sType, @sText, @nDebitRule, @nDebitCompany, @nDebitTerminal
	)
END
GO
