SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MerchantAddBalance]
(
	@nMerchant int,
	@nCurrency int,
	@nAmount money,
	@nSourceType int,
	@nSourceID int,
	@sSourceText nvarchar(100),
	@nStatus int,
	@sComment nvarchar(100)
)
AS
BEGIN
	INSERT INTO tblCompanyBalance
		(
			company_id,
			sourceType,
			sourceTbl_id,
			sourceInfo,
			amount,
			currency,
			status,
			comment
		)
	VALUES
	(
		@nMerchant,
		@nSourceType,
		@nSourceID,
		@sSourceText,
		@nAmount,
		@nCurrency,
		@nStatus,
		@sComment
	)
END
GO
