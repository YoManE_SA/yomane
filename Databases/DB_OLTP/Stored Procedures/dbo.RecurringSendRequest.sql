SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RecurringSendRequest](@sURL AS nvarchar(200), @sString AS nvarchar(2000), @nReply AS int OUTPUT, @sReply AS nvarchar(1000) OUTPUT)
AS
BEGIN
	SET @sReply=dbo.SendRequestHTTP(@sURL, @sString)
	SET @nReply=CASE @sReply WHEN 'Error' THEN -1 ELSE 0 END
END
GO
