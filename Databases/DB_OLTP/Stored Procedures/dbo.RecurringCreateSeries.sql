SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[RecurringCreateSeries]
(
	@bPass BIT,
	@nTransaction AS INT,
	@nChargeCount AS INT,
	@nIntervalUnit AS INT,
	@nIntervalLength AS INT,
	@sComments NVARCHAR(200),
	@nTrmCode AS INT=0,
	@sIP AS VARCHAR(50)='',
	@sIdentifier VARCHAR(50)=NULL,
	@nPrecreatedSeriesID INT=NULL
)
AS
BEGIN
	--validate arguments
	IF @nChargeCount<1
	BEGIN
		-- invalid number of charges
		SELECT -1
		RETURN
	END

	IF @nIntervalUnit<1 or @nIntervalUnit>4
	BEGIN
		-- invalid interval unit
		SELECT -2
		RETURN
	END
	
	IF @nIntervalLength<1
	BEGIN
		-- invalid interval length
		SELECT -3
		RETURN
	END
	
	-- get credit card or echeck from transaction
	DECLARE @nCreditCard AS int, @nECheck AS int, @nTransPaymentMethod AS int, @nCompany AS int, @bAllowed bit
	DECLARE @nAmount AS money, @nCurrency AS tinyint, @dtTransPass datetime
	IF @bPass=1
		-- tblCompanyTransPass
        SELECT  @nCreditCard = CreditCardID ,
                @nECheck = CheckDetailsID ,
                @nTransPaymentMethod = [TransPaymentMethod_id] ,
                @nCompany = tblCompany.ID ,
                @bAllowed = ISNULL(tblMerchantRecurringSettings.IsEnabled, 0) ,
                @nAmount = Amount ,
                @nCurrency = Currency ,
                @dtTransPass = tblCompanyTransPass.InsertDate
        FROM    tblCompanyTransPass
                INNER JOIN tblCompany ON tblCompanyTransPass.companyID = tblCompany.ID
                LEFT JOIN tblMerchantRecurringSettings ON tblCompany.ID = tblMerchantRecurringSettings.MerchantID
        WHERE   tblCompanyTransPass.ID = @nTransaction
	ELSE
		-- tblCompanyTransApproval
        SELECT  @nCreditCard = CreditCardID ,
                @nECheck = CheckDetailsID ,
                @nTransPaymentMethod = [TransPaymentMethod_id] ,
                @nCompany = tblCompany.ID ,
                @bAllowed = ISNULL(tblMerchantRecurringSettings.IsEnabled, 0) ,
                @nAmount = Amount ,
                @nCurrency = Currency ,
                @dtTransPass = tblCompanyTransApproval.InsertDate
        FROM    tblCompanyTransApproval
                INNER JOIN tblCompany ON tblCompanyTransApproval.CompanyID = tblCompany.ID
                LEFT JOIN tblMerchantRecurringSettings ON tblCompany.ID = tblMerchantRecurringSettings.MerchantID
        WHERE   tblCompanyTransApproval.ID = @nTransaction

	IF @nCompany IS NULL
	BEGIN
		-- invalid transaction ID - no passed transaction found
		SELECT -4
		RETURN
	END
	IF @bAllowed=0
	BEGIN
		-- merchant is not allowed to make recurring transactions
		SELECT -5
		RETURN
	END

	SET NOCOUNT ON

	-- create the series
	DECLARE @nID AS int, @i AS int
	IF @nPrecreatedSeriesID>0
	BEGIN
		--precreated: update existing record
		UPDATE
			tblRecurringSeries
		SET
			rs_Approval=1-@bPass,
			rs_Company=@nCompany,
			rs_ChargeCount=@nChargeCount,
			rs_IntervalUnit=@nIntervalUnit,
			rs_IntervalLength=@nIntervalLength,
			rs_CreditCard=@nCreditCard,
			rs_ECheck=@nECheck,
			[TransPaymentMethod_id] = @nTransPaymentMethod ,
			rs_Comments=@sComments,
			rs_ChargeAmount=@nAmount,
			rs_Currency=@nCurrency,
			rs_ReqTrmCode=@nTrmCode,
			rs_IP=@sIP,
			rs_Identifier=@sIdentifier,
			rs_IsPrecreated=0
		WHERE
			ID=@nPrecreatedSeriesID AND rs_IsPrecreated=1;
		SET @nID=@nPrecreatedSeriesID;
	END
	ELSE
	BEGIN
		--not precreated: create new record
		INSERT INTO tblRecurringSeries
			(rs_Approval, rs_Company, rs_ChargeCount, rs_IntervalUnit, rs_IntervalLength, rs_CreditCard, rs_ECheck, [TransPaymentMethod_id], rs_Comments, rs_ChargeAmount, rs_Currency, rs_ReqTrmCode, rs_IP, rs_Identifier)
		VALUES
			(1-@bPass, @nCompany, @nChargeCount, @nIntervalUnit, @nIntervalLength, @nCreditCard, @nECheck, @nTransPaymentMethod, @sComments, @nAmount, @nCurrency, @nTrmCode, @sIP, @sIdentifier);
		SET @nID=@@IDENTITY;
	END;

	-- create charges
	SET @i=0
	WHILE @i<@nChargeCount
	BEGIN
		SET @i=@i+1
		INSERT INTO tblRecurringCharge
			(rc_Series, rc_ChargeNumber, rc_Date, rc_CreditCard, rc_ECheck, [TransPaymentMethod_id], rc_Amount, rc_Currency)
		VALUES
			(@nID, @i, dbo.RecurringGetChargeDate(@dtTransPass, @i, @nIntervalUnit, @nIntervalLength), @nCreditCard, @nECheck, @nTransPaymentMethod, @nAmount, @nCurrency)
	END

	-- "create" first attempt
	INSERT INTO tblRecurringAttempt
		(ra_Charge, ra_CreditCard, ra_ECheck, [TransPaymentMethod_id], ra_ReplyCode, ra_TransPass, ra_TransApproval, ra_Date)
	SELECT
		ID, @nCreditCard, @nECheck, @nTransPaymentMethod, '000', CASE @bPass WHEN 1 THEN @nTransaction ELSE NULL END, CASE @bPass WHEN 0 THEN @nTransaction ELSE NULL END, @dtTransPass
	FROM
		tblRecurringCharge
	WHERE
		rc_Series=@nID
		AND
		rc_ChargeNumber=1

	-- calculate total
	UPDATE
		tblRecurringSeries
	SET
		rs_SeriesTotal=(SELECT SUM(rc_Amount) FROM tblRecurringCharge WHERE rc_Series=@nID)
	WHERE ID=@nID

	-- select series ID
	SELECT @nID
	SET NOCOUNT OFF
END



GO
