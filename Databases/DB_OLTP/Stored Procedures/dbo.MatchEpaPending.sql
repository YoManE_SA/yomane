SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MatchEpaPending](@bDebug bit=0)
AS
BEGIN
	--enable NOCOUNT, if is disabled initially
	DECLARE @bNoCount bit;
	SET @bNoCount=CASE 512&@@OPTIONS WHEN 512 THEN 1 ELSE 0 END;
	IF @bDebug=1 SET NOCOUNT OFF ELSE SET NOCOUNT ON;

	--write start to log
	DECLARE @sServer varchar(1000);
	SET @sServer=IsNull(Cast(ServerProperty('InstanceName') AS varchar(50)), '');
	IF @sServer<>'' SET @sServer='\'+@sServer;
	SET @sServer=Cast(ServerProperty('ServerName') AS varchar(50))+@sServer;
	INSERT INTO tblEpaFileLog(ActionType, Details) VALUES (6, 'Server: '+@sServer+', User: '+CURRENT_USER+', Process: '+Cast(@@SPID AS varchar(50)));

	--matching attempt
	DECLARE
		@nID int,
		@dtInsert datetime,
		@sDebitRefCode nvarchar(40),
		@sDebitRefNum nvarchar(40),
		@sApprovalNumber nvarchar(50),
		@nAmount money,
		@nCurrency int,
		@sTerminalNumber nvarchar(50),
		@binCardNumber256 varbinary(200),
		@nInstallment int,
		@bRefund bit,
		@dtTrans datetime,
		@dtPayout datetime,
		@nTransCount int,
		@sCardNumber varchar(50),
		@nTransID int;
	DECLARE curEpa CURSOR LOCAL FOR
		SELECT
			ID, InsertDate, DebitRefCode, DebitReferenceNum, ApprovalNumber, Amount, Currency, LTrim(RTrim(TerminalNumber)), CardNumber256, Installment, IsRefund, TransDate, PayoutDate
		FROM
			tblEpaPending WITH (NOLOCK)
		WHERE
			TransCount IS NULL;
	OPEN curEpa;
	FETCH NEXT FROM curEpa INTO @nID, @dtInsert, @sDebitRefCode, @sDebitRefNum, @sApprovalNumber, @nAmount, @nCurrency, @sTerminalNumber, @binCardNumber256, @nInstallment, @bRefund, @dtTrans, @dtPayout;
	WHILE @@FETCH_STATUS=0
	BEGIN
		SELECT @nTransCount=0, @nTransID=NULL;
		--if there is DebitRefCode, try direct match
		IF IsNull(@sDebitRefCode, '')<>''
		BEGIN
			SELECT
				@nTransID=MAX(ID), @nTransCount=COUNT(ID)
			FROM
				tblCompanyTransPass WITH (NOLOCK)
			WHERE
				DebitReferenceCode=@sDebitRefCode
				AND SIGN(CreditType)=1-@bRefund AND (@dtTrans IS NULL OR InsertDate BETWEEN DATEADD(week, -2, @dtTrans) and DATEADD(week, 2, @dtTrans));
			IF @nTransCount>0 GOTO lblMoveNext;
			SELECT
				@nTransID=MAX(ID), @nTransCount=COUNT(ID)
			FROM
				tblCompanyTransPass WITH (NOLOCK)
			WHERE
				DebitReferenceCode LIKE @sDebitRefCode+'[_][0-9][0-9]'
				AND SIGN(CreditType)=1-@bRefund AND (@dtTrans IS NULL OR InsertDate BETWEEN DATEADD(week, -2, @dtTrans) and DATEADD(week, 2, @dtTrans));
			IF @nTransCount>0 GOTO lblMoveNext;
		END
		IF IsNull(@sDebitRefNum, '')<>''
		BEGIN
			SELECT
				@nTransID=MAX(ID), @nTransCount=COUNT(ID)
			FROM
				tblCompanyTransPass WITH (NOLOCK)
			WHERE
				DebitReferenceNum=@sDebitRefNum
				AND SIGN(CreditType)=1-@bRefund AND (@dtTrans IS NULL OR InsertDate BETWEEN DATEADD(week, -2, @dtTrans) and DATEADD(week, 2, @dtTrans));
			IF @nTransCount>0 GOTO lblMoveNext;
			SELECT
				@nTransID=MAX(ID), @nTransCount=COUNT(ID)
			FROM
				tblCompanyTransPass WITH (NOLOCK)
			WHERE
				DebitReferenceNum LIKE @sDebitRefNum+'[_][0-9][0-9]'
				AND SIGN(CreditType)=1-@bRefund AND (@dtTrans IS NULL OR InsertDate BETWEEN DATEADD(week, -2, @dtTrans) and DATEADD(week, 2, @dtTrans));
			IF @nTransCount>0 GOTO lblMoveNext;
		END
		--try match by other details
		IF IsNull(@sTerminalNumber, '')<>'' AND IsNull(@sApprovalNumber, '')<>'' AND @nCurrency IS NOT NULL AND @nAmount IS NOT NULL AND @dtTrans IS NOT NULL AND @binCardNumber256 IS NOT NULL
		BEGIN
			SET @sCardNumber=Replace(dbo.fnFormatCcNumToGroups(dbo.GetDecrypted256(@binCardNumber256)), ' ', '');
			SELECT
				@nTransID=MAX(t.ID), @nTransCount=COUNT(t.ID)
			FROM
				tblCompanyTransPass t WITH (NOLOCK)
				INNER JOIN tblCreditCard c WITH (NOLOCK) ON CreditCardID=c.ID
			WHERE
				TerminalNumber=@sTerminalNumber AND Currency=@nCurrency AND Amount=@nAmount
				AND LTrim(RTrim(ApprovalNumber))=@sApprovalNumber
				AND CCard_number256 IN (dbo.GetEncrypted256(@sCardNumber), dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups(@sCardNumber)))
				AND SIGN(CreditType)=1-@bRefund AND (@dtTrans IS NULL OR InsertDate BETWEEN DATEADD(day, -2, @dtTrans) and DATEADD(day, 2, @dtTrans));
			IF @nTransCount>0 GOTO lblMoveNext;
			SELECT
				@nTransID=MAX(t.ID), @nTransCount=COUNT(t.ID)
			FROM
				tblCompanyTransPass t WITH (NOLOCK)
				INNER JOIN tblCreditCard c WITH (NOLOCK) ON CreditCardID=c.ID
			WHERE
				TerminalNumber=@sTerminalNumber AND Currency=@nCurrency AND Amount=@nAmount
				AND Right(RTrim(ApprovalNumber), Len(@sApprovalNumber))=@sApprovalNumber
				AND CCard_number256 IN (dbo.GetEncrypted256(@sCardNumber), dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups(@sCardNumber)))
				AND SIGN(CreditType)=1-@bRefund AND (@dtTrans IS NULL OR InsertDate BETWEEN DATEADD(day, -2, @dtTrans) and DATEADD(day, 2, @dtTrans));
			IF @nTransCount>0 GOTO lblMoveNext;
			SELECT
				@nTransID=MAX(t.ID), @nTransCount=COUNT(t.ID)
			FROM
				tblCompanyTransPass t WITH (NOLOCK)
				INNER JOIN tblCreditCard c WITH (NOLOCK) ON CreditCardID=c.ID
			WHERE
				TerminalNumber=@sTerminalNumber AND Currency=@nCurrency AND Amount=@nAmount
				AND Left(LTrim(ApprovalNumber), Len(@sApprovalNumber))=@sApprovalNumber
				AND CCard_number256 IN (dbo.GetEncrypted256(@sCardNumber), dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups(@sCardNumber)))
				AND SIGN(CreditType)=1-@bRefund AND (@dtTrans IS NULL OR InsertDate BETWEEN DATEADD(day, -2, @dtTrans) and DATEADD(day, 2, @dtTrans));
			GOTO lblMoveNext;
		END
		lblMoveNext: --update the current record and forward to the next record
		IF @bDebug=1 PRINT Str(@nID)+Str(IsNull(@nTransCount, -1))+Str(IsNull(Str(@nTransID), -1));
		UPDATE tblEpaPending SET TransCount=@nTransCount, TransID=@nTransID WHERE ID=@nID;
		FETCH NEXT FROM curEpa INTO @nID, @dtInsert, @sDebitRefCode, @sDebitRefNum, @sApprovalNumber, @nAmount, @nCurrency, @sTerminalNumber, @binCardNumber256, @nInstallment, @bRefund, @dtTrans, @dtPayout;
	END
	CLOSE curEpa;
	DEALLOCATE curEpa;

	/*delete duplications*/
	WITH CTE (TransID, Installment, DuplicateCount)
	AS
	(
	SELECT TransID, Installment,
	 ROW_NUMBER() OVER(PARTITION BY TransID, Installment ORDER BY TransID) AS DuplicateCount
	FROM tblEpaPending Where TransID is not null
	)
	DELETE
	FROM CTE
	WHERE DuplicateCount > 1

	--update paid in existing transactions
	IF @bDebug=1 PRINT 'Updating paid in existing transactions...';
	UPDATE
		l
	SET
		IsPaid=1, PaidInsertDate=IsNull(PayoutDate, GetDate())
	FROM
		tblLogImportEPA l INNER JOIN tblEpaPending p ON l.TransID=p.TransID AND l.Installment=p.Installment
	WHERE
		TransCount=1 AND p.TransID>0 AND IsRefund=0;

	--update refunded in existing transactions
	IF @bDebug=1 PRINT 'Updating refunded in existing transactions...';
	UPDATE
		l
	SET
		IsRefunded=1, RefundedInsertDate=IsNull(PayoutDate, GetDate())
	FROM
		tblLogImportEPA l INNER JOIN tblEpaPending p ON l.TransID=p.TransID AND l.Installment=p.Installment
	WHERE
		TransCount=1 AND p.TransID>0 AND IsRefund=1;
		
	--insert paid in new transactions
	IF @bDebug=1 PRINT 'Inserting paid in new transactions...';
	INSERT INTO
		tblLogImportEPA(TransID, Installment, IsPaid, PaidInsertDate)
	SELECT DISTINCT
		p.TransID, p.Installment, 1, IsNull(PayoutDate, GetDate())
	FROM
		tblEpaPending p LEFT JOIN tblLogImportEPA l ON p.TransID=l.TransID AND p.Installment=l.Installment
	WHERE
		TransCount=1 AND p.TransID>0 AND IsRefund=0 AND l.TransID IS NULL;

	--insert refunded in new transactions
	IF @bDebug=1 PRINT 'Inserting refunded in new transactions...';
	INSERT INTO
		tblLogImportEPA(TransID, Installment, IsRefunded, RefundedInsertDate)
	SELECT DISTINCT
		p.TransID, p.Installment, 1, IsNull(PayoutDate, GetDate())
	FROM
		tblEpaPending p LEFT JOIN tblLogImportEPA l ON p.TransID=l.TransID AND p.Installment=l.Installment
	WHERE
		TransCount=1 AND p.TransID>0 AND IsRefund=1 AND l.TransID IS NULL;

	DELETE FROM tblEpaPending WHERE TransCount = 1;

	--write end to log
	INSERT INTO tblEpaFileLog(ActionType, Details) VALUES (7, 'Server: '+@sServer+', User: '+CURRENT_USER+', Process: '+Cast(@@SPID AS varchar(50)));

	--disable NOCOUNT, if was disabled initially
	IF @bNoCount=0 SET NOCOUNT OFF ELSE SET NOCOUNT ON;
END

GO
