SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[RecurringPrecreateSeries]
    (
      @bPass BIT ,
      @nTransaction INT ,
      @sRecurring VARCHAR(MAX) ,
      @sComments NVARCHAR(200) = '' ,
      @nTrmCode INT = 0 ,
      @sIP NVARCHAR(50) = '' ,
      @nIsRecurringApproval INT = -1 ,
      @sIdentifier VARCHAR(50) = NULL
    )
AS 
    BEGIN
        SET NOCOUNT ON;
        DECLARE @nMerchant INT ,
            @nSeries INT ,
            @sParameters NVARCHAR(1000) ,
            @nCurrency INT ,
            @bIsApproval BIT ,
            @nCreditCardID INT ,
            @nCheckDetailsID INT ,
			@nTransPaymentMethod int;
        SET @bIsApproval = CASE @nIsRecurringApproval
                             WHEN 1 THEN 0
                             WHEN 0 THEN 1
                             ELSE 1 - @bPass
                           END;
        IF ( @bPass = 1 ) 
            SELECT  @nMerchant = CompanyID ,
                    @nCurrency = Currency ,
                    @nCreditCardID = CreditCardID ,
                    @nCheckDetailsID = CheckDetailsID ,
					@nTransPaymentMethod = [TransPaymentMethod_id]
            FROM    tblCompanyTransPass WITH ( NOLOCK )
            WHERE   ID = @nTransaction
        ELSE 
            SELECT  @nMerchant = CompanyID ,
                    @nCurrency = Currency ,
                    @nCreditCardID = CreditCardID ,
                    @nCheckDetailsID = CheckDetailsID ,
					@nTransPaymentMethod = [TransPaymentMethod_id]
            FROM    tblCompanyTransApproval WITH ( NOLOCK )
            WHERE   ID = @nTransaction;
        SET NOCOUNT OFF;
        IF @nMerchant IS NULL 
            BEGIN
                SELECT  -1;
                RETURN;
            END
        SET NOCOUNT ON;
        DECLARE @bIsFlexible BIT;
        IF dbo.GetQueryStringValue('Recurring2', @sRecurring) = '' 
            BEGIN
                SET @bIsFlexible = 0;
                DECLARE @sRecurring1 VARCHAR(20) ,
                    @nChargeCount INT ,
                    @nIntervalUnit INT ,
                    @nIntervalLength INT ,
                    @sUnit CHAR(1);
                SET @sRecurring1 = dbo.GetQueryStringValue('Recurring1',
                                                           @sRecurring);
                IF @sRecurring1 LIKE '%A%' 
                    SET @sRecurring1 = LEFT(@sRecurring1,
                                            CHARINDEX('A', @sRecurring1) - 1)
                SET @sUnit = CASE WHEN @sRecurring1 LIKE '%D%' THEN 'D'
                                  WHEN @sRecurring1 LIKE '%W%' THEN 'W'
                                  WHEN @sRecurring1 LIKE '%M%' THEN 'M'
                                  WHEN @sRecurring1 LIKE '%Q%' THEN 'Q'
                                  WHEN @sRecurring1 LIKE '%Y%' THEN 'Y'
                             END;
                SET @nChargeCount = CAST(LEFT(@sRecurring1,
                                              CHARINDEX(@sUnit, @sRecurring1)
                                              - 1) AS INT)
                SET @nIntervalUnit = CASE @sUnit
                                       WHEN 'D' THEN 1
                                       WHEN 'W' THEN 2
                                       WHEN 'Y' THEN 4
                                       ELSE 3
                                     END
                SET @nIntervalLength = RIGHT(@sRecurring1,
                                             LEN(@sRecurring1)
                                             - CHARINDEX(@sUnit, @sRecurring1))
                IF @sUnit = 'Q' 
                    SET @nIntervalLength = @nIntervalLength * 3
            END
        ELSE 
            BEGIN
                SET @bIsFlexible = 1;
                SET @nChargeCount = 0;
                SET @nIntervalUnit = 0;
                SET @nIntervalLength = 0;
            END
        DECLARE @sRecurringCurrent VARCHAR(20) ,
            @i INT;
        SET @i = 2;
        SET @sRecurringCurrent = dbo.GetQueryStringValue('Recurring'
                                                         + LTRIM(RTRIM(@i)),
                                                         @sRecurring);
        WHILE LEN(@sRecurringCurrent) > 0 
            BEGIN
                EXEC RecurringExtendSeriesEx @nSeries, @sRecurringCurrent;
                SET @i = @i + 1;
                SET @sRecurringCurrent = dbo.GetQueryStringValue('Recurring'
                                                              + LTRIM(RTRIM(@i)),
                                                              @sRecurring);
            END

        INSERT  INTO tblRecurringSeries
                ( rs_IsPrecreated ,
                  rs_Company ,
                  rs_Comments ,
                  rs_Currency ,
                  rs_Approval ,
                  rs_Flexible ,
                  rs_ChargeCount ,
                  rs_IntervalLength ,
                  rs_IntervalUnit ,
                  rs_CreditCard ,
                  rs_ECheck ,
				  [TransPaymentMethod_id]
	            )
        VALUES  ( 1 ,
                  @nMerchant ,
                  @sComments ,
                  @nCurrency ,
                  @bIsApproval ,
                  @bIsFlexible ,
                  @nChargeCount ,
                  @nIntervalLength ,
                  @nIntervalUnit ,
                  @nCreditCardID ,
                  @nCheckDetailsID ,
				  @nTransPaymentMethod
	            );
        SET @nSeries = @@IDENTITY;
        SET @sParameters = 'SeriesID=' + LTRIM(RTRIM(STR(@nSeries)))
            + '&Pass=' + CASE @bPass
                           WHEN 1 THEN '1'
                           ELSE '0'
                         END + '&Transaction=' + LTRIM(RTRIM(STR(@nTransaction)))
            + '&Comments=' + @sComments + '&TrmCode='
            + LTRIM(RTRIM(STR(@nTrmCode))) + '&IP=' + @sIP
            + '&RecurringApproval' + LTRIM(RTRIM(STR(@nIsRecurringApproval)))
            + '&IDentifier=' + ISNULL(@sIdentifier, '') + '&RecurringStrings='
            + REPLACE(REPLACE(@sRecurring, '&', '|'), '=', '^');
        INSERT  INTO [dbo].[EventPending]
                ( [TransPreAuth_id] ,
                  [TransPass_id] ,
                  [EventPendingType_id] ,
                  [Parameters]
	            )
        VALUES  ( CASE @bPass
                    WHEN 0 THEN @nTransaction
                    ELSE NULL
                  END ,
                  CASE @bPass
                    WHEN 1 THEN @nTransaction
                    ELSE NULL
                  END ,
                  200 ,
                  @sParameters
	            );
        SELECT  @nSeries;
        SET NOCOUNT OFF;
    END

GO
