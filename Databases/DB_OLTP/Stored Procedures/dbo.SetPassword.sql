
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SetPassword](@nRefType int, @nRefID int, @sIP nvarchar(50), @sPassword nvarchar(50))
AS
BEGIN
	IF @nRefType NOT IN (1, 2, 4, 5, 6, 7) RETURN -1;
	IF @nRefID<1 RETURN -2;
	IF dbo.IsValidIPAddress(@sIP)=0 RETURN -3;
	IF dbo.IsPasswordValid(@sPassword)=0 RETURN -4;
	IF @nRefType=2 AND @nRefID NOT IN (SELECT ID FROM tblCompany WITH (NOLOCK)) RETURN -5;
	IF @nRefType=4 AND @nRefID NOT IN (SELECT MerchantSubUser_id FROM MerchantSubUser WITH (NOLOCK)) RETURN -6;
	IF @nRefType=7 AND @nRefID NOT IN (SELECT affiliates_id FROM tblAffiliates WITH (NOLOCK)) RETURN -7;
	IF @nRefType=1 AND @nRefID NOT IN (SELECT Customer_id FROM Data.Customer WITH (NOLOCK)) RETURN -8;
	IF @nRefType IN (5, 6) AND @nRefID NOT IN (SELECT ID FROM tblSecurityUser WITH (NOLOCK)) RETURN -9;
	
	DECLARE @binPassword varbinary(200);
	SET @binPassword=dbo.GetEncrypted256(@sPassword);
	IF EXISTS(SELECT * FROM tblPasswordHistory WITH (NOLOCK) WHERE LPH_RefID=@nRefID AND LPH_RefType=@nRefType AND LPH_Password256=@binPassword) RETURN -10;

	DELETE FROM tblPasswordHistory WHERE LPH_RefType=@nRefType AND LPH_RefID=@nRefID AND LPH_ID=4;
	UPDATE tblPasswordHistory SET LPH_ID=LPH_ID+1 WHERE LPH_RefType=@nRefType AND LPH_RefID=@nRefID;
	INSERT INTO
		tblPasswordHistory (LPH_ID, LPH_RefID, LPH_RefType, LPH_IP, LPH_Password256)
	VALUES
		(1, @nRefID, @nRefType, @sIP, @binPassword);
	RETURN 0;
END

GO
