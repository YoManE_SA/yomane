SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CompanySaveLogin]
(
	@nID int,
	@sUserName nvarchar(50)
)
AS
BEGIN
	-- check validity
	IF @sUserName=''
	BEGIN
		-- there is at least one empty field
		SELECT -1
		RETURN
	END
	-- check if is new
	IF EXISTS(SELECT ID FROM tblCompany WHERE UserNameAlt=@sUserName AND ID=@nID)
	BEGIN
		-- nothing to update
		SELECT -3
		RETURN
	END
	-- prevent duplication
	IF EXISTS(SELECT ID FROM tblCompany WHERE UserNameAlt=@sUserName AND ID<>@nID)
	BEGIN
		-- UserName already exists
		SELECT -2
		RETURN
	END
	UPDATE
		tblCompany
	SET
		UserNameAlt=@sUserName
	WHERE
		ID=@nID
	SELECT @nID
END
GO
