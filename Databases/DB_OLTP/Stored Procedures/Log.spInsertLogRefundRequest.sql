SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [Log].[spInsertLogRefundRequest]
    (
      @MerchantID AS INT ,
      @Action AS NVARCHAR(50) ,
      @ReplyCode AS NVARCHAR(20) ,
      @Request AS NVARCHAR(1500) ,
      @Response AS NVARCHAR(1500) ,
      @SourceIdentity AS INT ,
      @InsertIPAddress AS NVARCHAR(50)
    )
AS
    DECLARE @VariableXML AS XML = N'
	<RefundRequest>
		<ActionType>' + @Action + '</ActionType>
		<ReplyCode>' + @ReplyCode + '</ReplyCode>
		<Request><![CDATA[' + @Request + ']]></Request>
		<Response><![CDATA[' + @Response + ']]></Response>
	</RefundRequest>
	';
	
    INSERT  INTO [Log].[History]
            ( [HistoryType_id] ,
              [Merchant_id] ,
              [SourceIdentity] ,
              [InsertIPAddress] ,
              [VariableXML]
            )
    VALUES  ( 2 ,
              @MerchantID ,
              @SourceIdentity ,
              @InsertIPAddress ,
              @VariableXML
            );

GO
