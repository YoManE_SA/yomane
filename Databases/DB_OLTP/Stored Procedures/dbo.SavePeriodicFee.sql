SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SavePeriodicFee](@nMerchantID int, @nTypeID int, @bIsActive bit, @dtNextDate date=NULL)
AS
BEGIN
	UPDATE tblPeriodicFee SET IsActive=@bIsActive, NextDate=IsNull(@dtNextDate, NextDate) WHERE MerchantID=@nMerchantID AND TypeID=@nTypeID;
	RETURN @@ROWCOUNT;
END
GO
