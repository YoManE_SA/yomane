SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CheckDebitCompanyBlocks] AS
BEGIN
	EXEC DebitBlockLogWrite 'BLOCK CHECK', 'Block check started'
	DECLARE @nBlock int, @nRule int, @nAttempts int, @nCount int, @nDebitCompany int, @sDebitTerminal nvarchar(20),
		@sReplyCodes nvarchar(100), @sNotifyUsers nvarchar(100), @sNotifyUsersSMS nvarchar(100),
		@sSubject nvarchar(100), @sBody nvarchar(1000), @sDebitCompany nvarchar(100), @sLogText nvarchar(950)
	DECLARE curBlocks CURSOR FOR
	SELECT
		tblDebitBlock.ID,
		db_DebitCompany,
		db_DebitTerminalNumber,
		db_DebitRule,
		db_AutoEnableAttempts,
		db_CountAutoEnableAttempts,
		dr_ReplyCodes,
		dr_NotifyUsers,
		dr_NotifyUsersSMS
	FROM
		tblDebitRule
		INNER JOIN tblDebitBlock ON tblDebitRule.ID=db_DebitRule
	WHERE
		db_UnblockDate<=GetDate()
		AND
		db_IsAutoEnable=1
		AND
		db_IsUnblocked=0
	OPEN curBlocks
	FETCH NEXT FROM
		curBlocks
	INTO
		@nBlock, @nDebitCompany, @sDebitTerminal, @nRule, @nAttempts, @nCount, @sReplyCodes, @sNotifyUsers, @sNotifyUsersSMS
	WHILE @@FETCH_STATUS=0
	BEGIN
		IF @nCount<@nAttempts
		BEGIN
			SET @sLogText='Rule '+Cast(@nRule AS nvarchar(10))+', attempt '+Cast(@nCount+1 AS nvarchar(10))+'/'
				+Cast(@nAttempts AS nvarchar(10))
			IF dbo.DebitAttemptUnblock(@nDebitCompany, @sDebitTerminal, @nCount+1, @sReplyCodes)=1
			BEGIN
				SET @nCount=@nCount+1
				UPDATE
					tblDebitBlock
				SET
					db_CountAutoEnableAttempts=db_CountAutoEnableAttempts+1
				WHERE
					ID=@nBlock
				EXEC DebitBlockLogWrite 'UNBLOCK ATTEMPT OK', @sLogText, @nRule, @nDebitCompany
			END
			ELSE
				EXEC DebitBlockLogWrite 'UNBLOCK ATTEMPT FAIL', @sLogText, @nRule, @nDebitCompany
		END
		IF @nCount>=@nAttempts
		BEGIN
			UPDATE
				tblDebitBlock
			SET
				db_CountAutoEnableAttempts=@nCount, db_UnblockDate=GetDate(), db_IsUnblocked=1
			WHERE
				ID=@nBlock
			UPDATE tblDebitRule SET dr_LastUnblockDate=GetDate() WHERE ID=@nRule
			UPDATE tblDebitCompany SET dc_TempBlocks=dc_TempBlocks-1 WHERE DebitCompany_ID=@nDebitCompany
			SET @sLogText='Rule '+Cast(@nRule AS nvarchar(10))
			EXEC DebitBlockLogWrite 'BLOCK REMOVED', @sLogText, @nRule, @nDebitCompany
			IF @sNotifyUsers<>''
			BEGIN
				SELECT @sDebitCompany=dc_name FROM tblDebitCompany WHERE DebitCompany_ID=@nDebitCompany
				SET @sSubject=@sDebitCompany+' Unblock At '+Cast(ServerProperty('MachineName') AS nvarchar(10))
				SET @sBody='<b>Debit Company Unblock</b><br />'
				SET @sBody=@sBody+'Server: '+Cast(ServerProperty('MachineName') AS nvarchar(10))+'<br />'
				SET @sBody=@sBody+'Sent on: '+Convert(nvarchar(25), GetDate(), 120)+'<br />'
				SET @sBody=@sBody+'Debit company: '+Cast(@nDebitCompany as nvarchar(10))+' '+@sDebitCompany+'<br />'
				SET @sBody=@sBody+'Rule : '+Cast(@nRule as nvarchar(10))+'<br />'
				SET @sBody=@sBody+'Reply codes: '+@sReplyCodes+'<br />'
				SET @sBody=@sBody+'Attempts: '+Cast(@nCount as nvarchar(10))+'<br />'
				EXEC SendMail @sNotifyUsers, @sSubject, @sBody
				SET @sLogText=@sSubject+', rule '+Cast(@nRule AS nvarchar(10))+', sent to '+@sNotifyUsers+'<br />'+@sBody
				EXEC DebitBlockLogWrite 'MAIL SENT', @sLogText, @nRule, @nDebitCompany
			END
			IF @sNotifyUsersSMS<>''
			BEGIN
				SELECT @sDebitCompany=dc_name FROM tblDebitCompany WHERE DebitCompany_ID=@nDebitCompany
				SET @sBody=@sDebitCompany+' Unblock'
				SET @sBody=@sBody+' at '+Cast(ServerProperty('MachineName') AS nvarchar(10))+'. '
				SET @sBody=@sBody+'Sent on: '+Convert(nvarchar(25), GetDate(), 108)+'. '
				SET @sBody=@sBody+'Reply codes: '+@sReplyCodes+'. '
				SET @sBody=@sBody+'Attempts: '+Cast(@nCount as nvarchar(10))+'. '
				EXEC SendSMS @sNotifyUsersSMS, @sBody
				SET @sLogText=@sSubject+', rule '+Cast(@nRule AS nvarchar(10))+', sent to '+@sNotifyUsersSMS+'<br />'+@sBody
				EXEC DebitBlockLogWrite 'SMS SENT', @sLogText, @nRule, @nDebitCompany
			END
		END
		FETCH NEXT FROM
			curBlocks
		INTO
			@nBlock, @nDebitCompany, @sDebitTerminal, @nRule, @nAttempts, @nCount, @sReplyCodes, @sNotifyUsers, @sNotifyUsersSMS
	END
	CLOSE curBlocks
	DEALLOCATE curBlocks
	EXEC DebitBlockLogWrite 'BLOCK CHECK END', 'Block check complete.'
END

GO
