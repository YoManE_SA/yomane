SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ChargeAnnualFees]
AS
BEGIN
	DECLARE @nMerchant int, @sType nvarchar(50), @nAmount money, @nCurrency int, @sSQL nvarchar(200), @sDisplay nvarchar(50), @nEuroRate smallmoney;
	SELECT @nEuroRate=CUR_BaseRate FROM tblSystemCurrencies WHERE CUR_ID=2;
	DECLARE curFee CURSOR LOCAL FOR
		SELECT
			ID, '', AnnualFeeAmount/CUR_BaseRate*@nEuroRate, CUR_ID, 'Annual Fee '+LTrim(RTrim(Str(Year(AnnualFeeDate))))
		FROM
			tblCompany CROSS JOIN tblGlobalValues INNER JOIN tblSystemCurrencies ON PaymentReceiveCurrency=CUR_ID
		WHERE
			ActiveStatus=30 AND IsAnnualFee=1 AND AnnualFeeDate<=GETDATE()
		UNION SELECT
			ID, 'LowRisk', AnnualFeeLowRiskAmount/CUR_BaseRate*@nEuroRate, CUR_ID, 'Annual Fee '+LTrim(RTrim(Str(Year(AnnualFeeLowRiskDate))))
		FROM
			tblCompany CROSS JOIN tblGlobalValues INNER JOIN tblSystemCurrencies ON PaymentReceiveCurrency=CUR_ID
		WHERE
			ActiveStatus=30 AND IsAnnualFeeLowRisk=1 AND AnnualFeeLowRiskDate<=GETDATE()
		UNION SELECT
			ID, 'HighRisk', AnnualFeeHighRiskAmount/CUR_BaseRate*@nEuroRate, CUR_ID, 'Annual Fee '+LTrim(RTrim(Str(Year(AnnualFeeHighRiskDate))))
		FROM
			tblCompany CROSS JOIN tblGlobalValues INNER JOIN tblSystemCurrencies ON PaymentReceiveCurrency=CUR_ID
		WHERE
			ActiveStatus=30 AND IsAnnualFeeHighRisk=1 AND AnnualFeeHighRiskDate<=GETDATE()
		UNION SELECT
			ID, '3dSecure', AnnualFee3dSecureAmount/CUR_BaseRate*@nEuroRate, CUR_ID, 'Annual Fee '+LTrim(RTrim(Str(Year(AnnualFee3dSecureDate))))
		FROM
			tblCompany CROSS JOIN tblGlobalValues INNER JOIN tblSystemCurrencies ON PaymentReceiveCurrency=CUR_ID
		WHERE
			ActiveStatus=30 AND IsAnnualFee3dSecure=1 AND AnnualFee3dSecureDate<=GETDATE()
		UNION SELECT
			ID, 'Registration', AnnualFeeRegistrationAmount/CUR_BaseRate*@nEuroRate, CUR_ID, 'Annual Fee '+LTrim(RTrim(Str(Year(AnnualFeeRegistrationDate))))
		FROM
			tblCompany CROSS JOIN tblGlobalValues INNER JOIN tblSystemCurrencies ON PaymentReceiveCurrency=CUR_ID
		WHERE
			ActiveStatus=30 AND IsAnnualFeeRegistration=1 AND AnnualFeeRegistrationDate<=GETDATE();
	OPEN curFee;
	FETCH NEXT FROM curFee INTO @nMerchant, @sType, @nAmount, @nCurrency, @sDisplay;
	WHILE @@FETCH_STATUS=0
	BEGIN
		EXEC CreateFeeTransaction @nMerchant, @nAmount, @nCurrency, 5, 30, @sDisplay, '';
		SET @sSQL='UPDATE tblCompany SET AnnualFee'+@sType+'Date=DateAdd(year, 1, AnnualFee'+@sType+'Date) WHERE ID='+Str(@nMerchant);
		EXEC (@sSQL);
		FETCH NEXT FROM curFee INTO @nMerchant, @sType, @nAmount, @nCurrency, @sDisplay;
	END
	CLOSE curFee;
	DEALLOCATE curFee;
END
GO
