SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spUpdUnsettleTransMerchantGradedFee] ( @MerchantID INT, @MonthsBack INT = 5 )
AS 
    BEGIN
		
		DECLARE
			@GradedCurrency AS TINYINT			,
			@DateStart		AS DATE				,
			@DateEnd		AS DATE				, 
			@Count			AS INT		= 0		,
			@Debug			AS BIT		= 0		;

		SET
			@GradedCurrency = (SELECT CFF_Currency FROM tblCompany WHERE ID = @MerchantID);
		
		-- Exit if merchant not set to use graded fees
		IF @GradedCurrency IS NULL
			RETURN;
		
		-- Create temp table for holding transactions
		CREATE TABLE #TempTable
			(
			  RowNum			INT,		--(Not required)
			  TransID			INT ,
			  Currency			TINYINT ,	--(Not required)
			  TransDate			DATETIME ,	--(Not required)
			  PrimaryPayedID	INT ,
			  OriginalAmount	MONEY ,		--(Not required)
			  Amount			MONEY ,
			  MonthlySum		MONEY ,
			  OldFee			FLOAT ,
			  FeeValue			FLOAT ,
			  FeeAmount			FLOAT
			);

		-- Go through each month as back as requested
		WHILE @MonthsBack >= 0 
			BEGIN
				-- Set date range to 1st of month and 
				SET @DateStart	= DATEADD(mm, DATEDIFF(m,0,DATEADD(MONTH,-@MonthsBack,GETDATE())),0);
				SET @DateEnd	= DATEADD(mm, DATEDIFF(m,0,DATEADD(MONTH,-@MonthsBack,GETDATE()))+1,0);
				
				-- Move all transactions of month in focus to temp table
				INSERT	INTO #TempTable (RowNum, TransID, Currency, OldFee, TransDate, PrimaryPayedID, OriginalAmount, Amount, MonthlySum)
				SELECT	ROW_NUMBER() OVER(ORDER BY a.insertdate), a.Id, a.Currency, a.netpayFee_ratioCharge, a.InsertDate, a.PrimaryPayedID, a.Amount, dbo.ConvertCurrency(a.Amount, a.Currency, @GradedCurrency),
						(SELECT SUM(dbo.ConvertCurrency(b.Amount, c.Currency, @GradedCurrency)) 
						FROM tblCompanyTransPass AS c JOIN tblCompanyTransPass AS b ON c.id = b.Id
						WHERE b.insertdate <= a.insertdate AND b.companyID = @MerchantID AND b.insertdate >= @DateStart AND b.insertdate < @DateEnd AND b.CreditType<>0 AND b.DeniedStatus IN(0,1,2,3,4) AND b.PaymentMethod > 20)
				FROM	tblCompanyTransPass AS a
				WHERE   a.companyID = @MerchantID AND a.insertdate >= @DateStart AND a.insertdate < @DateEnd AND a.CreditType<>0 AND a.DeniedStatus IN(0,1,2,3,4) AND a.PaymentMethod > 20
				ORDER BY a.insertdate ASC;
				
				SET @MonthsBack -= 1;
			END

		-- Update temp table with fees
		UPDATE #TempTable SET FeeValue = (SELECT TOP 1 CFF_Precent FROM tblCompanyFeesFloor WHERE CFF_CompanyID = @MerchantID AND MonthlySum > CFF_TotalTo ORDER BY CFF_TotalTo DESC);
		UPDATE #TempTable SET FeeAmount = ROUND((FeeValue/100) * OriginalAmount ,4);
				
		-- Debug
		IF @Debug = 1
			SELECT * FROM #TempTable;

		-- Update unsetteled transactions with calculated fees
		IF @Debug = 0
			BEGIN
				-- Old fee column in transPass table
				UPDATE  tp
				SET     tp.netpayFee_ratioCharge = tt.FeeAmount
				FROM    tblCompanyTransPass AS tp
						JOIN #TempTable AS tt ON tp.ID = tt.TransID
				WHERE   tt.PrimaryPayedID = 0
				
				-- New TransactionAmount table
				UPDATE  ta
				SET     ta.Amount = tt.FeeAmount, ta.SettledAmount = tt.FeeAmount
				FROM    tblTransactionAmount AS ta
						JOIN #TempTable AS tt ON ta.TransPassID = tt.TransID
				WHERE   tt.PrimaryPayedID = 0 
			END

		-- Clean up
		DROP TABLE #TempTable;

		-- Debug
		IF @Debug = 1
			SELECT * FROM tblCompanyFeesFloor WHERE CFF_CompanyID = @MerchantID ORDER BY CFF_TotalTo ASC

    END


GO
