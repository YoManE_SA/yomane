SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CustomerGetPassword]
(
	@sMail nvarchar(80)
)
AS
BEGIN
	DECLARE @nID int
	SET @nID=(SELECT TOP 1 ID FROM tblCustomer WHERE Mail=@sMail)
	IF IsNull(@nID, 0)=0
	BEGIN
		-- no user with that eMail
		SELECT '-1|No user found with this eMail address'
		RETURN
	END
	DECLARE @binPassword varbinary(200)
	SET @binPassword=
		(
			SELECT TOP 1
				LPH_Password256
			FROM
				tblPasswordHistory
			WHERE
				LPH_ID=1 AND LPH_RefType=1 AND LPH_RefID=@nID
		)
	IF @binPassword IS NULL
	BEGIN
		-- no active password found for the user
		SELECT '-2|No valid password found for this user'
		RETURN
	END
	DECLARE @sPassword nvarchar(50)
	SET @sPassword=dbo.GetDecrypted256(@binPassword)
	IF IsNull(@sPassword, '')=''
	BEGIN
		-- no active password found for the user
		SELECT '-3|The password is damaged or corrupted'
		RETURN
	END
	SELECT '0|'+@sPassword
END
GO
