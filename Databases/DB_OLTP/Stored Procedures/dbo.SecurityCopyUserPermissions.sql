SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SecurityCopyUserPermissions](@nFrom int, @nTo int)
AS
BEGIN
	IF @nFrom IS NULL OR @nTo IS NULL
	BEGIN
		RAISERROR ('At least one argument is NULL!', 18, 1) WITH NOWAIT;
		RETURN;
	END
	IF @nFrom=@nTo
	BEGIN
		RAISERROR ('Source user is same as target user (%d)!', 18, 1, @nTo) WITH NOWAIT;
		RETURN;
	END
	DECLARE @sFrom nvarchar(50), @sTo nvarchar(50);
	SELECT @sFrom='"'+su_Username+'"' FROM tblSecurityUser WHERE ID=@nFrom
	IF @sFrom IS NULL
	BEGIN
		RAISERROR ('Source user not found (%d)!', 18, 1, @nFrom) WITH NOWAIT;
		RETURN;
	END
	SELECT @sTo='"'+su_Username+'"' FROM tblSecurityUser WHERE ID=@nTo
	IF @sTo IS NULL
	BEGIN
		RAISERROR ('Target user not found (%d)!', 18, 1, @nTo) WITH NOWAIT;
		RETURN;
	END
	SET NOCOUNT ON;
	PRINT 'Users found, copying permissions from '+@sFrom+' to '+@sTo+' ...';
	PRINT 'Deleting group memberships for '+@sTo+' ...';
	DELETE FROM tblSecurityUserGroup WHERE sug_User=@nTo;
	PRINT 'Deleting document permissions for '+@sTo+' ...';
	DELETE FROM tblSecurityDocumentGroup WHERE sdg_Group=-@nTo;
	PRINT 'Copying group memberships from '+@sFrom+' to '+@sTo+' ...';
	INSERT INTO tblSecurityUserGroup
		(sug_Group, sug_User, sug_IsMember)
	SELECT
		sug_Group, @nTo, sug_IsMember
	FROM
		tblSecurityUserGroup
	WHERE
		sug_User=@nFrom;
	PRINT 'Copying document permissions from '+@sFrom+' to '+@sTo+' ...';
	INSERT INTO tblSecurityDocumentGroup
		(sdg_Document, sdg_Group, sdg_IsActive, sdg_IsVisible)
	SELECT
		sdg_Document, -@nTo, sdg_IsActive, sdg_IsVisible
	FROM
		tblSecurityDocumentGroup
	WHERE
		sdg_Group=-@nFrom;
	PRINT 'Done!';
	SET NOCOUNT OFF;
END
GO
