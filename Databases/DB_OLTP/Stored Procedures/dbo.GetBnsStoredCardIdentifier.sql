SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetBnsStoredCardIdentifier](@sCardNumber varchar(25), @sCardCvv varchar(5)='', @nDebitTerminalID int=0)
AS
BEGIN
	--RETURNS positive ID when adding new card, returns negative ID when the card already exists
	DECLARE @sIdentifier varchar(32), @binIdentifier varbinary(200), @nID int;
	SET @sIdentifier=dbo.fnFormatCcNumToGroups(IsNull(@sCardNumber, ''))+' '+LTrim(RTrim(Str(IsNull(@nDebitTerminalID, 0))));
	SET @binIdentifier=dbo.GetEncrypted256(@sIdentifier);
	SELECT @nID=-ID FROM tblBnsStoredCard WHERE Identifier256=@binIdentifier;

	--20111023 Tamir - CVV2 is out of use
	IF @nID IS NULL
	BEGIN
		SET @sIdentifier=dbo.fnFormatCcNumToGroups(IsNull(@sCardNumber, ''))+' '+LTrim(RTrim(Str(IsNull(@nDebitTerminalID, 0))));
		SET @binIdentifier=dbo.GetEncrypted256(@sIdentifier);
		SELECT @nID=-ID FROM tblBnsStoredCard WHERE Identifier256=@binIdentifier;
	END;

	IF @nID IS NULL
	BEGIN
		INSERT INTO tblBnsStoredCard(Identifier256) VALUES (@binIdentifier);
		SET @nID=@@IDENTITY;
	END;
	RETURN @nID;
END;
GO
