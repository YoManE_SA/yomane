SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RecurringSetSeriesIdentifier](@nSeries int, @sIdentifier varchar(50)=NULL)
AS
UPDATE tblRecurringSeries SET rs_Identifier=@sIdentifier WHERE ID=@nSeries AND IsNull(rs_Identifier, '')=''
GO
