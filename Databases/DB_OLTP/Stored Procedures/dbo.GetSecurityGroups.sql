SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetSecurityGroups](@sUsername varchar(50), @sGroup varchar(50)=NULL)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		sg_Name
	FROM
		tblSecurityGroup
		INNER JOIN tblSecurityUserGroup ON tblSecurityGroup.ID=tblSecurityUserGroup.sug_Group
		INNER JOIN tblSecurityUser ON tblSecurityUserGroup.sug_User=tblSecurityUser.ID
	WHERE
		su_Username=@sUsername
		AND
		(sg_Name=@sGroup OR @sGroup IS NULL)
		AND
		sug_IsMember=1
		AND
		sg_IsActive=1
		AND
		su_IsActive=1;
	SET NOCOUNT OFF;
END
GO
