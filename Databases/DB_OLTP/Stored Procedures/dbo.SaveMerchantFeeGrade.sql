SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SaveMerchantFeeGrade](@nMerchant int, @nTotalMin money, @nFeePercent money, @nID int=0)
AS
BEGIN
	IF EXISTS (SELECT CFF_ID FROM tblCompanyFeesFloor WHERE CFF_ID=@nID)
		UPDATE tblCompanyFeesFloor SET CFF_TotalTo=@nTotalMin, CFF_Precent=@nFeePercent WHERE CFF_CompanyID=@nMerchant AND CFF_ID=@nID
	ELSE
		INSERT INTO tblCompanyFeesFloor(CFF_CompanyID, CFF_TotalTo, CFF_Precent) VALUES (@nMerchant, @nTotalMin, @nFeePercent);
	RETURN @@IDENTITY;
END
GO
