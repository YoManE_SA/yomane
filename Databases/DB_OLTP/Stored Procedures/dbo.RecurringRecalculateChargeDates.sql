SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[RecurringRecalculateChargeDates] ( @nChargeBase AS INT )
AS 
    BEGIN
        DECLARE @nChargeNumberBase INT ,
            @dtDateBase DATETIME ,
            @nSeries INT;

        SELECT  @nChargeNumberBase = rc_ChargeNumber ,
                @dtDateBase = rc_Date ,
                @nSeries = rc_Series
        FROM    tblRecurringCharge
        WHERE   ID = @nChargeBase;

        UPDATE  rc
        SET     rc_Date = dbo.RecurringGetChargeDate(@dtDateBase,
                                                     rc_ChargeNumber
                                                     - @nChargeNumberBase + 1,
                                                     rs_IntervalUnit,
                                                     rs_IntervalLength)
        FROM    tblRecurringCharge AS rc
                INNER JOIN tblRecurringSeries AS rs ON rc.rc_Series = rs.ID
        WHERE   rs.ID = @nSeries
                AND rc.rc_ChargeNumber > @nChargeNumberBase;

    END

GO
