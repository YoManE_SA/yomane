SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CreateFeeTransaction](@nMerchant INT, @nAmount MONEY, @nCurrency INT, @nPaymentMethod INT=0, @nSource INT=0, @sDisplay NVARCHAR(50)='', @sComment NVARCHAR(1000)='', @dtInsert DATETIME=NULL, @nAmountType INT=NULL)
AS
BEGIN
	IF @nSource IS NOT NULL AND @nAmountType IS NULL SET @nAmountType=@nSource+1000; -- annual: source 30, type 1030; monthly: source 31, type 1031;
	IF @dtInsert IS NULL SET @dtInsert=GetDate();
	DECLARE @nTrans int;
	INSERT INTO tblCompanyTransPass
	(
		InsertDate,
		companyID,
		TransSource_id,
		CreditType,
		Amount,
        UnsettledAmount,
        UnsettledInstallments,
		Currency,
		OCurrency,
		PaymentMethod,
		PaymentMethodDisplay,
		Comment
	)
	VALUES
	(
		@dtInsert,
		@nMerchant,
		@nSource,
		0,
		@nAmount,
		-@nAmount,
		1,
		@nCurrency,
		@nCurrency,
		@nPaymentMethod,
		@sDisplay,
		@sComment
	);
	SET @nTrans=@@IDENTITY;
	INSERT INTO tblTransactionAmount
	(
		Amount,
		Currency,
		MerchantID,
		TransPassID,
		TypeID,
		InsertDate
	)
	VALUES
	(
		@nAmount,
		@nCurrency,
		@nMerchant,
		@nTrans,
		@nAmountType,
		@dtInsert
	);
	RETURN @nTrans;
END

GO
