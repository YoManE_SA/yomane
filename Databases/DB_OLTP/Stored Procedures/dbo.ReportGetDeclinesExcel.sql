SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ReportGetDeclinesExcel]
(
	@nMaxRecords int=NULL,
	@nMerchant int=NULL,
	@dtFrom datetime=NULL,
	@dtTo datetime=NULL,
	@sFromDate varchar(19)=NULL,
	@sToDate varchar(19)=NULL,
	@nDebitCompany int=NULL,
	@sTerminalNumber varchar(50)=NULL,
	@bIsAuth bit=NULL,
	@nReplySource int=NULL,
	@sReplyCode varchar(50)=NULL,
	@sDebitReferenceCode varchar(50)=NULL,
	@nAmountFrom money=NULL,
	@nAmountTo money=NULL,
	@nCurrency int=NULL,
	@sFreeText nvarchar(100)=NULL,
	@sBorderColor varchar(20)='Gray',
	@sTitleForeColor varchar(20)='Black',
	@sTitleBackColor varchar(20)='Silver',
	@sDataForeColor varchar(20)='Black',
	@sDataBackColor varchar(20)='White'
)
AS
BEGIN
	IF IsNull(@nMaxRecords, 0)<1 SET @nMaxRecords=1000000
	SELECT
		1 Tag,
		NULL Parent,
		1 [TABLE!1!border],
		@sBorderColor [TABLE!1!bordercolor],
		NULL [TR!2!ID],
		NULL [TR!2!style],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT],
		NULL [TR!2!TD!ELEMENT]
	UNION SELECT
		2 Tag,
		1 Parent,
		NULL,
		NULL,
		0,
		'font-weight:bold;color:'+@sTitleForeColor+';background-color:'+@sTitleBackColor+';',
		'ID',
		'Test Only',
		'Type',
		'Date',
		'Merchant',
		'Company Name',
		'Merchant No.',
		'Payment Method Type',
		'Payment Method',
		'Payment Method BIN',
		'Credit Type',
		'Installments',
		'Currency',
		'Amount',
		'Debit Company',
		'Debit Company Name',
		'Terminal No.',
		'Transaction Source',
		'Reply Source',
		'Reply Code',
		'Reply Text',
		'IP Address',
		'Order Number',
		'Comment',
		'Debit Ref.'
	UNION SELECT TOP (@nMaxRecords)
		2 Tag,
		1 Parent,
		NULL,
		NULL,
		f.ID,
		'font-weight:normal;color:'+@sDataForeColor+';background-color:'+@sDataBackColor+';',
		LTrim(Str(f.ID)),
		CASE f.isTestOnly WHEN 1 THEN 'Yes' ELSE 'No' END,
		CASE f.TransType WHEN 1 THEN 'Auth.' ELSE 'Debit' END,
		Convert(varchar(10), f.InsertDate, 103)+' '+Convert(varchar(8), f.InsertDate, 14),
		LTrim(Str(f.CompanyID)),
		IsNull(m.CompanyName, ''),
		IsNull(m.CustomerNumber, ''),
		IsNull(pm.GD_Text, ''),
		IsNull(f.PaymentMethodDisplay, ''),
		IsNull(Str(cc.CCard_First6), ''),
		IsNull(ct.NameEng, ''),
		LTrim(Str(f.Payments)),
		IsNull(CUR_ISOName, ''),
		LTrim(Cast(f.Amount as varchar(20))),
		LTrim(Str(f.DebitCompanyID)),
		IsNull(dc.dc_Name, ''),
		f.TerminalNumber,
		IsNull(ts.NameEng, ''),
		IsNull(fs.GD_Text, ''),
		f.replyCode,
		IsNull(rc.DescriptionOriginal, ''),
		f.IPAddress,
		f.OrderNumber,
		f.Comment,
		f.DebitReferenceCode
	FROM
		tblCompanyTransFail f
		LEFT JOIN tblCompany m ON f.companyID=m.ID
		LEFT JOIN GetGlobalData(42) pm ON f.PaymentMethod=pm.GD_ID
		LEFT JOIN List.TransCreditType ct ON f.CreditType=ct.TransCreditType_id
		LEFT JOIN tblSystemCurrencies cur ON f.Currency=cur.CUR_ID
		LEFT JOIN List.TransSource ts ON f.TransactionTypeID=ts.TransSource_id
		LEFT JOIN tblDebitCompany dc ON f.DebitCompanyID=dc.debitCompany_id
		LEFT JOIN tblDebitCompanyCode rc ON f.replyCode=rc.code and dc.debitCompany_id=rc.DebitCompanyID 
		LEFT JOIN GetGlobalData(46) fs ON rc.FailSource=fs.GD_ID
		LEFT JOIN tblCreditCard cc ON f.CreditCardID=cc.ID
	WHERE
		(@nMerchant IS NULL OR f.companyID=@nMerchant) AND
		(@dtFrom IS NULL OR f.InsertDate>=@dtFrom) AND
		(@dtTo IS NULL OR f.InsertDate<=@dtTo) AND
		(@sFromDate IS NULL OR f.InsertDate>=Convert(datetime, @sFromDate, 103)) AND
		(@sToDate IS NULL OR f.InsertDate<=Convert(datetime, @sToDate, 103)) AND
		(@nDebitCompany IS NULL OR f.DebitCompanyID=@nDebitCompany) AND
		(@sTerminalNumber IS NULL OR f.TerminalNumber=@sTerminalNumber) AND
		(@bIsAuth IS NULL OR (@bIsAuth=1 AND f.TransType=1) OR (@bIsAuth=0 AND f.TransType<>1)) AND
		(@nReplySource IS NULL OR rc.FailSource=@nReplySource) AND
		(@sReplyCode IS NULL OR f.replyCode=@sReplyCode) AND
		(@sDebitReferenceCode IS NULL OR f.DebitReferenceCode=@sDebitReferenceCode) AND
		(@nAmountFrom IS NULL OR f.Amount>=@nAmountFrom) AND
		(@nAmountTo IS NULL OR f.Amount<=@nAmountTo) AND
		(@nCurrency IS NULL OR f.Currency=@nCurrency) AND
		(@sFreeText IS NULL OR f.OrderNumber LIKE '%'+@sFreeText+'%' OR f.Comment LIKE '%'+@sFreeText+'%')
	ORDER BY
		Tag,
		[TR!2!ID]
	FOR XML EXPLICIT
END


GO
