SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[InvoiceCorrectCurrencyRate](@nInvoiceNumber as int)
AS
BEGIN
	DECLARE @nID as int;
	SELECT @nID=(SELECT max(ID) FROM tblinvoicedocument where id_invoicenumber=@nInvoiceNumber);
	if @nID is null
	begin
		print cast(@nInvoiceNumber as nvarchar(5))+' already printed'
		return
	end
	update tblinvoiceline set il_CurrencyRate=1 where il_documentid=@nID;
	 UPDATE  
	  tblInvoiceDocument  
	 SET  
	  id_Lines=IsNull((SELECT Count(ID) FROM tblInvoiceLine WHERE il_DocumentID=@nID), 0),
	  id_TotalLines=IsNull((SELECT Sum(il_Amount*il_CurrencyRate) FROM tblInvoiceLine WHERE il_DocumentID=@nID), 0)/id_CurrencyRate
	 WHERE  
	  ID=@nID;  
	 UPDATE  
	  tblInvoiceDocument  
	 SET  
	  id_TotalVAT=id_TotalLines*(CASE id_ApplyVAT WHEN 1 THEN id_VATPercent ELSE 0 END)  
	 WHERE  
	  ID=@nID;  
	 UPDATE  
	  tblInvoiceDocument  
	 SET  
	  id_TotalDocument=id_TotalLines+id_TotalVAT  
	 WHERE  
	  ID=@nID;  
	print cast(@nInvoiceNumber as nvarchar(5))+' corrected'
END
GO
