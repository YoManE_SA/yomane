SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SaveMerchantRecurringSettings](@nMerchant int, @bIsEnabled bit, @bIsEnabledFromTransPass bit, @bIsEnabledModify bit, @nMaxCharges int, @nMaxStages int, @nMaxYears int)
AS
BEGIN
	IF @bIsEnabled=1 OR @bIsEnabledFromTransPass=1 OR @bIsEnabledModify=1 OR @nMaxCharges>0 OR @nMaxStages>0 OR @nMaxYears>0
	BEGIN
		IF EXISTS(SELECT * FROM tblMerchantRecurringSettings WHERE MerchantID=@nMerchant)
			UPDATE
				tblMerchantRecurringSettings
			SET
				IsEnabled=@bIsEnabled,
				IsEnabledFromTransPass=@bIsEnabledFromTransPass,
				IsEnabledModify=@bIsEnabledModify,
				MaxCharges=@nMaxCharges,
				MaxStages=@nMaxStages,
				MaxYears=@nMaxYears
			WHERE
				MerchantID=@nMerchant;
		ELSE
		BEGIN
			INSERT INTO tblMerchantRecurringSettings(MerchantID, IsEnabled, IsEnabledFromTransPass, IsEnabledModify) VALUES (@nMerchant, @bIsEnabled, @bIsEnabledFromTransPass, @bIsEnabledModify);
			IF @nMaxCharges>0 UPDATE tblMerchantRecurringSettings SET MaxCharges=@nMaxCharges;
			IF @nMaxStages>0 UPDATE tblMerchantRecurringSettings SET MaxStages=@nMaxStages;
			IF @nMaxYears>0 UPDATE tblMerchantRecurringSettings SET MaxYears=@nMaxYears;
		END
	END
END
GO
