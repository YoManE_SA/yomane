SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetSummarySettlement](@nSettlement int)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @dtFrom datetime, @dtTo datetime;
	SELECT @dtFrom=DateAdd(month, -1, MIN(InsertDate)) FROM tblCompanyTransPass WHERE PrimaryPayedID=@nSettlement;
	SELECT @dtTo=PayDate FROM tblTransactionPay WHERE ID=@nSettlement;
	SELECT * FROM ReportGetSummarySettlement(@dtFrom, @dtTo, @nSettlement)
	SET NOCOUNT OFF;
END
GO
