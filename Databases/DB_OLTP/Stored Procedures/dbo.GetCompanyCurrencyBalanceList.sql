SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GetCompanyCurrencyBalanceList]
(
	@nCompanyID int,
	@nCurrencyID tinyint
)
AS
BEGIN
	SET NOCOUNT ON;	
		SELECT  companyBalance_id, sourceTbl_id, sourceType, sourceInfo, insertDate, amount, currency,
		   BalanceExpected balance, status, comment, paymentType		   
		   FROM tblCompanyBalance
		   left join tblCompanyMakePaymentsRequests on  tblCompanyBalance.sourceTbl_id = tblCompanyMakePaymentsRequests.CompanyMakePaymentsRequests_id
		   WHERE 
		  tblCompanyBalance.company_id = @nCompanyID AND currency = @nCurrencyID ORDER BY companyBalance_id DESC 
	SET NOCOUNT OFF
END
GO
