SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SaveAffiliateBankAccount]
(
	@nAffiliate int,
	@nCurrency int=-1,
	@nAccountType int,
	@sAccountName nvarchar(80),
	@sAccountNumber nvarchar(80),
	@sBankName nvarchar(80),
	@sBankAddress nvarchar(200),
	@sBankAddressSecond nvarchar(100),
	@sBankAddressCity nvarchar(30),
	@sBankAddressState nvarchar(20),
	@nBankAddressCountry int,
	@sSwiftNumber nvarchar(80),
	@sIBAN nvarchar(80),
	@sABA nvarchar(80),
	@sSortCode nvarchar(80),
	@sBankAddressZip nvarchar(20),
	@sSepaBic nvarchar(11)
)
AS
BEGIN
	DECLARE @s2 varchar(1), @sPrefix varchar(13), @sSQL nvarchar(4000)
	SET @s2=CASE @nAccountType WHEN 2 THEN '2' ELSE '' END;
	SET @sPrefix=CASE @nCurrency WHEN -1 THEN 'paymentAbroad' ELSE 'aba_' END;
	IF NOT EXISTS (SELECT * FROM tblAffiliateBankAccount WHERE aba_Affiliate=@nAffiliate AND aba_Currency=@nCurrency)
		INSERT INTO tblAffiliateBankAccount(aba_Affiliate, aba_Currency) VALUES (@nAffiliate, @nCurrency);
	IF @nCurrency=-1
		SET @sSQL='UPDATE tblAffiliates SET '
	ELSE
		SET @sSQL='UPDATE tblAffiliateBankAccount SET ';
	SET @sSQL=@sSQL+@sPrefix+'AccountName'+@s2+'='''+@sAccountName+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'AccountNumber'+@s2+'='''+@sAccountNumber+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankName'+@s2+'='''+@sBankName+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddress'+@s2+'='''+@sBankAddress+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddressSecond'+@s2+'='''+@sBankAddressSecond+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddressCity'+@s2+'='''+@sBankAddressCity+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddressState'+@s2+'='''+@sBankAddressState+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddressCountry'+@s2+'='+Str(@nBankAddressCountry);
	SET @sSQL=@sSQL+', '+@sPrefix+'SwiftNumber'+@s2+'='''+@sSwiftNumber+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'IBAN'+@s2+'='''+@sIBAN+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'ABA'+@s2+'='''+@sABA+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'SortCode'+@s2+'='''+@sSortCode+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddressZip'+@s2+'='''+@sBankAddressZip+'''';
	SET @sSQL=@sSQL+', '+@sPrefix+'SepaBic'+@s2+'='''+@sSepaBic+'''';
	IF @nCurrency=-1
		SET @sSQL=@sSQL+' WHERE affiliates_id='+Str(@nAffiliate)
	ELSE
		SET @sSQL=@sSQL+' WHERE aba_Affiliate='+Str(@nAffiliate)+' AND aba_Currency='+Str(@nCurrency);
	EXEC sp_executesql @sSQL;
END
GO
