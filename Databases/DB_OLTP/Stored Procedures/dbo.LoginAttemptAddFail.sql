SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoginAttemptAddFail](@nRefType int, @nRefID int)
AS
BEGIN
	UPDATE
		tblPasswordHistory
	SET
		LPH_FailCount=LPH_FailCount+1, LPH_LastFail=GetDate()
	WHERE
		LPH_RefID=@nRefID AND LPH_ID=1 AND LPH_RefType=@nRefType
END
GO
