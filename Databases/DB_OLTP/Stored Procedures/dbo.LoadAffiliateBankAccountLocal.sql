SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoadAffiliateBankAccountLocal](@nAffiliate int)
AS
BEGIN
	SELECT
		PaymentMethod PaymentType,
		IsNull(PaymentBank, 0) Bank,
		PaymentBranch BranchNumber,
		PaymentPayeeName Beneficiary,
		PaymentAccount AccountNumber
	FROM
		tblAffiliates
	WHERE
		affiliates_id=@nAffiliate
END
GO
