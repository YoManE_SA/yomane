SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spAddQNA](@nCategory int, @sQuestion nvarchar(2000))
AS
BEGIN
	INSERT INTO QNA(Question, QNAGroup_id) VALUES(@sQuestion, @nCategory);
	RETURN @@IDENTITY;
END

GO
