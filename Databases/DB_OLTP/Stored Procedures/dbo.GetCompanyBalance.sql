SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetCompanyBalance]
(
	@nCompanyID int	
)
AS
BEGIN
	SET NOCOUNT ON;	
	SELECT
		*
	FROM
		(
			SELECT
				companyBalance_id,
				currency,
				BalanceExpected balance,
				BalanceCurrent,
				status,
				ROW_NUMBER() OVER (PARTITION BY Currency ORDER BY companyBalance_id DESC) RowNumber
			FROM
				tblCompanyBalance
			WHERE 
				tblCompanyBalance.company_id=@nCompanyID
		) q
	WHERE
		RowNumber=1
	ORDER BY
		currency;
	SET NOCOUNT OFF
END
GO
