SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GetInvikRefundBatch] AS
SELECT
                B.ID,
                P.Currency,
                dt_MCC MCC,
                T.TerminalNumber MerchantReference,
                dt_Name+' '+dt_Descriptor MerchantName,
                Replace(dbo.GetDecrypted256(CCard_Number256), ' ', '') CardNumber,
                ExpYY ExpYear,
                ExpMM ExpMonth,
                P.Amount,
                DebitReferenceCode TransRefunded,
                R.ID [Transaction],
                '' MerchantCity,
                P.OrderNumber[Description]
FROM
                tblInvikRefundBatch B
                INNER JOIN tblRefundAsk R ON irb_RefundRequest=R.ID
                INNER JOIN tblCompanyTransPass P ON R.TransID=P.ID AND P.DebitCompanyID=21 AND P.PaymentMethod BETWEEN 20 AND 99
                INNER JOIN tblCreditCard C ON P.CreditCardID=C.ID
                INNER JOIN tblDebitTerminals T ON P.TerminalNumber=T.TerminalNumber AND T.DebitCompany=21
WHERE
                irb_IsDownloaded=0
ORDER BY
                B.ID

GO
