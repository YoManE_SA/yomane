SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CustomerSaveLogin]
(
	@nID int,
	@sMail nvarchar(50)
)
AS
BEGIN
	-- check validity
	IF @sMail=''
	BEGIN
		-- there is at least one empty field
		SELECT -1
		RETURN
	END
	-- check if is new
	IF EXISTS(SELECT ID FROM tblCustomer WHERE Mail=@sMail AND ID=@nID)
	BEGIN
		-- nothing to update
		SELECT -3
		RETURN
	END
	-- prevent duplication
	IF EXISTS(SELECT ID FROM tblCustomer WHERE Mail=@sMail AND ID<>@nID)
	BEGIN
		-- eMail already exists
		SELECT -2
		RETURN
	END
	UPDATE
		tblCustomer
	SET
		Mail=@sMail
	WHERE
		ID=@nID
	SELECT @nID
END
GO
