SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteMerchantFeeGrade](@nMerchant int, @nID int=0)
AS
DELETE FROM tblCompanyFeesFloor WHERE CFF_CompanyID=@nMerchant AND CFF_ID=@nID
GO
