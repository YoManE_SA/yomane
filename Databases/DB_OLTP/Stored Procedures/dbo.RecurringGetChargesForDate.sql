SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RecurringGetChargesForDate](@dtDate AS datetime)
AS
BEGIN
	SELECT
		*
	FROM
		viewRecurringActiveCharges
	WHERE
		rc_Date<=Cast(Cast(IsNull(@dtDate, GetDate()) AS int) AS datetime)
END
GO
