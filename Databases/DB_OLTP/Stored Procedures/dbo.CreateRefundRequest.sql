SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CreateRefundRequest](@nMerchant int, @nTransaction int, @nAmount money=0, @nCurrency int=-1, @sComment nvarchar(300)='', @nRefundType tinyint=0)
AS
BEGIN
	IF NOT EXISTS(SELECT ID FROM tblCompanyTransPass WHERE ID=@nTransaction)
	BEGIN
		SELECT -4;
		RETURN
	END
	IF @nMerchant<>(SELECT CompanyID FROM tblCompanyTransPass WHERE ID=@nTransaction)
	BEGIN
		SELECT -1;
		RETURN;
	END
	IF @nAmount<=0 SET @nAmount=(SELECT Amount FROM tblCompanyTransPass WHERE ID=@nTransaction)-(SELECT Sum(RefundAskAmount) FROM tblRefundAsk WHERE TransID=@nTransaction);
	IF IsNull(@nAmount, 0)<=0
	BEGIN
		SELECT -2;
		RETURN;
	END
	IF @nCurrency<0 SET @nCurrency=(SELECT Currency FROM tblCompanyTransPass WHERE ID=@nTransaction);
	IF IsNull(@nCurrency, -1)<0
	BEGIN
		SELECT -3;
		RETURN;
	END
	SET NOCOUNT ON;
	INSERT INTO tblRefundAsk
	(
		CompanyID, TransID, RefundAskAmount, RefundAskCurrency, RefundAskComment, RefundType
	)
	VALUES
	(
		@nMerchant, @nTransaction, @nAmount, @nCurrency, @sComment, @nRefundType
	);
	SET NOCOUNT OFF;
	SELECT @@IDENTITY;
END

GO
