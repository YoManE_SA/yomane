SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SendMail](@sTo nvarchar(200), @sSubject nvarchar(200), @sBody nvarchar(max))
AS
BEGIN
	DECLARE @sToMail nvarchar(max), @n int, @sTemp nvarchar(200)
	SET @sTo=Replace(@sTo+';', ';;', ';')
	SET @sToMail=''
	WHILE @sTo<>''
	BEGIN
		SET @n=CharIndex(';', @sTo)
		SET @sTemp=CASE @n WHEN 0 THEN @sTo ELSE Left(@sTo, @n-1) END
		SET @sTo=Replace(@sTo, @sTemp+';', '')
		IF CharIndex('@', @sTemp)=0
			SET @sTemp=IsNull((SELECT TOP 1 CASE su_MailEmergency WHEN '' THEN su_Mail ELSE su_MailEmergency END FROM tblSecurityUser WHERE su_Username=@sTemp), '')
		IF @sTemp<>'' SET @sToMail=@sToMail+@sTemp+';'
	END
	EXEC msdb..sp_send_dbmail
		@recipients=@sToMail,
		@subject=@sSubject,
		@body_format='HTML',
		@body=@sBody
END
GO
