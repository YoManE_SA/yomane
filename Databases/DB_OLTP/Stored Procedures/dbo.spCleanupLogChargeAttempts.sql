SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCleanupLogChargeAttempts]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @dtInsertDate AS datetime = DATEADD(MONTH, -9, getdate())
	DECLARE curArchiver CURSOR FOR
		SELECT LogChargeAttempts_id
		FROM tblLogChargeAttempts
		WHERE Lca_DateStart < @dtInsertDate
		ORDER BY Lca_DateStart
	DECLARE @nID AS int
	OPEN curArchiver
	FETCH NEXT FROM curArchiver INTO @nID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM tblLogChargeAttempts WHERE LogChargeAttempts_id = @nID
		WAITFOR DELAY '00:00:00.20'
		FETCH NEXT FROM curArchiver INTO @nID
	END
	CLOSE curArchiver
	DEALLOCATE curArchiver
	SET NOCOUNT OFF
END
GO
