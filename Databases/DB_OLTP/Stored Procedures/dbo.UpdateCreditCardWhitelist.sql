SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[UpdateCreditCardWhitelist]
(
	@nID int,
	@nMerchant int,
	@nLevel tinyint,
	@nExpMonth tinyint,
	@nExpYear smallint,
	@bIsBurnt bit,
	@sComment nvarchar(200)
)
AS
BEGIN
	IF @nMerchant IS NULL OR @nLevel IS NULL RETURN -1;
	IF @nExpMonth<1 OR @nExpMonth>12 OR @nExpYear<Year(GetDate()) OR @nExpYear>Year(GetDate())+3 RETURN -2;
	IF @nExpYear=Year(GetDate()) AND @nExpMonth<Month(GetDate()) RETURN -3;
	IF NOT EXISTS (SELECT GD_ID FROM GetGlobalData(39) WHERE GD_ID=@nLevel) RETURN -6;
	IF @nMerchant<>0 AND NOT EXISTS (SELECT ID FROM tblCompany WITH (NOLOCK) WHERE ID=@nMerchant AND ActiveStatus=30 AND IsCcWhitelistEnabled=1) RETURN -7;
	IF NOT EXISTS (SELECT ID FROM tblCreditCardWhitelist WITH (NOLOCK) WHERE ID=@nID AND ccwl_Merchant=@nMerchant) RETURN -9;
	UPDATE
		tblCreditCardWhitelist
	SET
		ccwl_Level=@nLevel,
		ccwl_ExpMonth=@nExpMonth,
		ccwl_ExpYear=@nExpYear,
		ccwl_BurnDate=CASE @bIsBurnt WHEN 1 THEN CASE WHEN ccwl_BurnDate IS NULL THEN GETDATE() ELSE ccwl_BurnDate END ELSE NULL END,
		ccwl_Comment=@sComment 
	WHERE
		ID=@nID;
	RETURN @nID;
END
GO
