SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetNewCustomerNumber](@sNumber nvarchar(50) OUTPUT)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @nNumber int, @bRepeat bit
	SET @bRepeat=1
	WHILE @bRepeat=1
	BEGIN
		SET @nNumber=Rand()*8999999+1000000
		IF NOT EXISTS (SELECT ID FROM tblCompany WHERE CustomerNumber=LTrim(RTrim(Str(@nNumber)))) SET @bRepeat=0
	END
	SET @sNumber=LTrim(RTrim(Str(@nNumber)))
	SET NOCOUNT OFF
END
GO
