SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[BlackListAddMerchant](@nCompanyID int, @sAdminUser nvarchar(50))
AS
BEGIN
	IF IsNull(@nCompanyID, 0)=0
		INSERT INTO [Risk].[BlacklistMerchant](InsertedBy) VALUES (@sAdminUser)
	ELSE
	BEGIN
		IF dbo.IsMerchantInBlackList(@nCompanyID)=0
			INSERT INTO
				[Risk].[BlacklistMerchant]
				(
					InsertedBy, [Merchant_id], FirstName, LastName, Street, City, Phone, Cellular, IDNumber,
					CompanyName, CompanyLegalName, CompanyLegalNumber, CompanyStreet, CompanyCity,
					CompanyPhone, CompanyFax, Mail, URL, MerchantSupportEmail, MerchantSupportPhoneNum,
					IpOnReg, PaymentPayeeName, PaymentBank, PaymentBranch, PaymentAccount
				)
			SELECT
				@sAdminUser, @nCompanyID, FirstName, LastName, NULL, NULL, Phone, Cellular, IDNumber,
				CompanyName, CompanyLegalName, CompanyLegalNumber, NULL, NULL,
				CompanyPhone, CompanyFax, Mail, URL, MerchantSupportEmail, MerchantSupportPhoneNum,
				IpOnReg, NULL, NULL, NULL, NULL
			FROM
				tblCompany
			WHERE
				ID=@nCompanyID;
	END
	SELECT Merchant_id AS [id], InsertDate, InsertedBy FROM [Risk].[BlacklistMerchant] WHERE IsNull(Merchant_id, 0)=IsNull(@nCompanyID, 0)
END

GO
