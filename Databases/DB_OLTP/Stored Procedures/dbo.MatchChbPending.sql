SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[MatchChbPending](@bDebug bit=0)
AS
BEGIN
	--enable NOCOUNT, if is disabled initially
	DECLARE @resultTable table(res int);
	DECLARE @bNoCount bit;
	SET @bNoCount=CASE 512&@@OPTIONS WHEN 512 THEN 1 ELSE 0 END;
	IF @bDebug=1 SET NOCOUNT OFF ELSE SET NOCOUNT ON;

	--write start to log
	DECLARE @sServer varchar(1000);
	SET @sServer=IsNull(Cast(ServerProperty('InstanceName') AS varchar(50)), '');
	IF @sServer<>'' SET @sServer='\'+@sServer;
	SET @sServer=Cast(ServerProperty('ServerName') AS varchar(50))+@sServer;
	INSERT INTO tblChbFileLog(ActionType, Details) VALUES (6, 'Server: '+@sServer+', User: '+CURRENT_USER+', Process: '+Cast(@@SPID AS varchar(50)));

	--matching attempt
	DECLARE
		@nID int,
		@sCaseID varchar(20),
		@sReasonCode varchar(10),
		@dtInsert datetime,
		@sDebitRefCode varchar(40),
		@sDebitRefNum varchar(40),
		@sApprovalNumber varchar(50),
		@nAmount money,
		@nCurrency int,
		@sTerminalNumber varchar(50),
		@binCardNumber256 varbinary(200),
		@nInstallment int,
		@dtTrans datetime,
		@dtChb datetime,
		@nTransCount int,
		@sCardNumber varchar(50),
		@nTransID int, 
		@bIsChargeback bit, 
		@bIsPhotocopy bit,
		@sComment varchar(100),
		@nReasonCode int,
		@dtDeadline datetime,
		@sFileName varchar(100);
	
	IF @bDebug=1 PRINT 'Selecting from tblChbPending into CURSOR';
	DECLARE curChb CURSOR LOCAL FOR
		SELECT
			ID, CaseID, ReasonCode, InsertDate, DebitRefCode, DebitReferenceNum, ApprovalNumber, Amount, Currency, LTrim(RTrim(TerminalNumber)), CardNumber256, Installment, TransDate, ChargebackDate, IsChargeback, IsPhotocopy, StoredFileName, DeadlineDate
		FROM
			tblChbPending WITH (NOLOCK)
		WHERE
			TransCount IS NULL AND InsertDate > DATEADD(Year,-1,SYSDATETIME());
	OPEN curChb;
	FETCH NEXT FROM
		curChb
	INTO
		@nID, @sCaseID, @sReasonCode, @dtInsert, @sDebitRefCode, @sDebitRefNum, @sApprovalNumber, @nAmount, @nCurrency, @sTerminalNumber, @binCardNumber256, @nInstallment, @dtTrans, @dtChb, @bIsChargeback, @bIsPhotocopy, @sFileName, @dtDeadline;
	WHILE @@FETCH_STATUS=0
	BEGIN
		IF @bDebug=1 PRINT 'Current record: '+LTrim(Str(@nID));

		SELECT @nTransCount = 0, @nTransID = NULL;

		-- Try direct match using DebitRefCode
		IF IsNull(@sDebitRefCode, '')<>''
		BEGIN
            SELECT  @nTransID = MAX(ID) ,
                    @nTransCount = COUNT(ID)
            FROM    tblCompanyTransPass WITH ( NOLOCK )
            WHERE   DebitReferenceCode = @sDebitRefCode
                    AND ( @dtTrans IS NULL
                          OR InsertDate BETWEEN DATEADD(week, -2, @dtTrans)
                                        AND     DATEADD(week, 2, @dtTrans) );

			IF @nTransCount > 0 GOTO lblMoveNext;

            SELECT  @nTransID = MAX(ID) ,
                    @nTransCount = COUNT(ID)
            FROM    tblCompanyTransPass WITH ( NOLOCK )
            WHERE   DebitReferenceCode LIKE @sDebitRefCode + '[_][0-9][0-9]'
                    AND ( @dtTrans IS NULL
                          OR InsertDate BETWEEN DATEADD(week, -2, @dtTrans)
                                        AND     DATEADD(week, 2, @dtTrans) )
                    AND CreditType > 0;

			IF @nTransCount>0 GOTO lblMoveNext;
		END
  
		-- Try direct match using DebitRefNum      
		IF IsNull(@sDebitRefNum, '')<>''
		BEGIN
            SELECT  @nTransID = MAX(ID) ,
                    @nTransCount = COUNT(ID)
            FROM    tblCompanyTransPass WITH ( NOLOCK )
            WHERE   DebitReferenceNum = @sDebitRefNum
                    AND ( @dtTrans IS NULL
                          OR InsertDate BETWEEN DATEADD(week, -2, @dtTrans)
                                        AND     DATEADD(week, 2, @dtTrans) );

			IF @nTransCount > 0 GOTO lblMoveNext;

            SELECT  @nTransID = MAX(ID) ,
                    @nTransCount = COUNT(ID)
            FROM    tblCompanyTransPass WITH ( NOLOCK )
            WHERE   DebitReferenceNum LIKE @sDebitRefNum + '[_][0-9][0-9]'
                    AND ( @dtTrans IS NULL
                          OR InsertDate BETWEEN DATEADD(week, -2, @dtTrans)
                                        AND     DATEADD(week, 2, @dtTrans) )
                    AND CreditType > 0;

			IF @nTransCount>0 GOTO lblMoveNext;
		END

		-- Try match using other details
		IF IsNull(@sTerminalNumber, '')<>'' AND IsNull(@sApprovalNumber, '')<>'' AND @nCurrency IS NOT NULL AND @nAmount IS NOT NULL AND @dtTrans IS NOT NULL AND @binCardNumber256 IS NOT NULL
		BEGIN
            SET @sCardNumber = REPLACE(dbo.fnFormatCcNumToGroups(dbo.GetDecrypted256(@binCardNumber256)), ' ', '');

            SELECT  @nTransID = MAX(t.ID) ,
                    @nTransCount = COUNT(t.ID)
            FROM    tblCompanyTransPass t WITH ( NOLOCK )
                    INNER JOIN tblCreditCard c WITH ( NOLOCK ) ON CreditCardID = c.ID
            WHERE   TerminalNumber = @sTerminalNumber
                    AND Currency = @nCurrency
                    AND Amount = @nAmount
                    AND LTRIM(RTRIM(ApprovalNumber)) = @sApprovalNumber
                    AND CCard_number256 IN ( dbo.GetEncrypted256(@sCardNumber),
                                             dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups(@sCardNumber)) )
                    AND ( @dtTrans IS NULL
                          OR InsertDate BETWEEN DATEADD(day, -2, @dtTrans)
                                        AND     DATEADD(day, 2, @dtTrans) );

			IF @nTransCount>0 GOTO lblMoveNext;

            SELECT  @nTransID = MAX(t.ID) ,
                    @nTransCount = COUNT(t.ID)
            FROM    tblCompanyTransPass t WITH ( NOLOCK )
                    INNER JOIN tblCreditCard c WITH ( NOLOCK ) ON CreditCardID = c.ID
            WHERE   TerminalNumber = @sTerminalNumber
                    AND Currency = @nCurrency
                    AND Amount = @nAmount
                    AND RIGHT(RTRIM(ApprovalNumber), LEN(@sApprovalNumber)) = @sApprovalNumber
                    AND CCard_number256 IN ( dbo.GetEncrypted256(@sCardNumber),
                                             dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups(@sCardNumber)) )
                    AND ( @dtTrans IS NULL
                          OR InsertDate BETWEEN DATEADD(day, -2, @dtTrans)
                                        AND     DATEADD(day, 2, @dtTrans) );

			IF @nTransCount>0 GOTO lblMoveNext;

            SELECT  @nTransID = MAX(t.ID) ,
                    @nTransCount = COUNT(t.ID)
            FROM    tblCompanyTransPass t WITH ( NOLOCK )
                    INNER JOIN tblCreditCard c WITH ( NOLOCK ) ON CreditCardID = c.ID
            WHERE   TerminalNumber = @sTerminalNumber
                    AND Currency = @nCurrency
                    AND Amount = @nAmount
                    AND LEFT(LTRIM(ApprovalNumber), LEN(@sApprovalNumber)) = @sApprovalNumber
                    AND CCard_number256 IN ( dbo.GetEncrypted256(@sCardNumber),
                                             dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups(@sCardNumber)) )
                    AND ( @dtTrans IS NULL
                          OR InsertDate BETWEEN DATEADD(day, -2, @dtTrans)
                                        AND     DATEADD(day, 2, @dtTrans) )
                    AND CreditType > 0;

			IF @nTransCount>0 GOTO lblMoveNext;
		END

		-- Try match DebitRefNum in pre-auth transactions     
		IF IsNull(@sDebitRefNum, '')<>''
		BEGIN
           SELECT   @nTransID = MAX([TransAnswerID]) ,
                    @nTransCount = COUNT([TransAnswerID])
           FROM     [dbo].[tblCompanyTransApproval] WITH ( NOLOCK )
           WHERE    DebitReferenceCode = @sDebitRefNum
                    AND [TransAnswerID] IS NOT NULL
                    AND ( @dtTrans IS NULL
                          OR InsertDate BETWEEN DATEADD(week, -2, CAST(@dtTrans AS DATE))
                                           AND     DATEADD(week, 2, CAST(@dtTrans AS DATE)) );

			GOTO lblMoveNext;
		END

		lblMoveNext: --update the current record and process or forward to the next record
		IF @bDebug=1 PRINT 'Updating the record (Count='+LTrim(Str(IsNull(@nTransCount, -1)))+', ID='+LTrim(Str(IsNull(@nTransID, -1)))+')';
		UPDATE tblChbPending SET TransCount=@nTransCount, TransID=@nTransID WHERE ID=@nID;
		IF @nTransCount<>1 GOTO lblFetchNext;

		-- create chargeback (if no refund) or retrieval request
		SET @sComment = CASE @bIsPhotocopy WHEN 1 THEN 'PCR' ELSE 'CHB' END+' CaseID=' + @sCaseID + ', Reason=' + @sReasonCode;
		SET @nReasonCode=CASE IsNumeric(@sReasonCode) WHEN 1 THEN Cast(@sReasonCode AS int) ELSE 0 END;
		IF @bIsPhotocopy = 1
		BEGIN
			-- Create retrieval request
			IF @bDebug=1 PRINT 'Creating retrieval request';
			Insert into @resultTable(res) EXEC dbo.RequestPhotocopyCHB @nTransID, @dtChb, @sComment, @nReasonCode;
			IF (Select Top 1 res From @resultTable) > 0 BEGIN
				INSERT INTO tblRetrivalRequests(TransPass_ID, RR_ReasonCode, RR_BankCaseID, RR_Deadline) VALUES (@nTransID, @nReasonCode, @sCaseID, @dtDeadline);
				INSERT INTO [dbo].[EventPending](TransPass_id, EventPendingType_id, Parameters, TryCount) VALUES (@nTransID, 2001, 'Reason='+@sReasonCode+'&Deadline='+Convert(char(10), @dtDeadline, 120), 3);
			END	
			Delete From @resultTable
			GOTO lblRemoveMatchedRow;
		END
		ELSE
		BEGIN
			IF @bIsChargeback = 1 AND NOT EXISTS(SELECT ID FROM tblCompanyTransPass WHERE OriginalTransID=@nTransID AND CreditType=0 AND DeniedStatus<>5)
			BEGIN
				-- Create chargeback
				IF @bDebug=1 PRINT 'Creating chargeback';
				Insert into @resultTable(res) EXEC CreateCHB @nTransID, @dtChb, @sComment, @nReasonCode;
				IF (Select Top 1 res From @resultTable) > 0 INSERT INTO [dbo].[EventPending](TransPass_id, EventPendingType_id, Parameters, TryCount) VALUES (@nTransID, 2002, 'Reason='+@sReasonCode, 3);
				Delete From @resultTable
				GOTO lblRemoveMatchedRow;
			END
		END
		-- Must be processed manually
		--IF @bDebug=1 PRINT 'Registering for manual processing, Case ID='+@sCaseID;
		--IF EXISTS (SELECT ID FROM tblImportChargebackBNS WHERE [CASE ID]=@sCaseID)
		--BEGIN
		--	IF @bDebug=1 PRINT 'Already registered!';
		--	GOTO lblRemoveMatchedRow;
		--END
		--INSERT INTO tblImportChargebackBNS
		--(
		--	[CASE ID],
		--	[RSN],
		--	[MER-NO],
		--	[CARDHOLDER-NUMBER],
		--	[TXN-DATE],
		--	[AMOUNT-TXN],
		--	[CUR],
		--	[AMOUNT-EURO],
		--	[REF-NO],
		--	[TICKET-NO],
		--	[DEADLINE],
		--	[COMMENTS],
		--	[MRF],
		--	[icb_User],
		--	[icb_FileName],
		--	[icb_ChargebackDate],
		--	[icb_IsPhotocopy]
		--)
		--VALUES
		--(
		--	@sCaseID,
		--	@sReasonCode,
		--	IsNull((SELECT TOP 1 '00'+RIGHT('000000'+dt_ContractNumber, 6) FROM tblDebitTerminals WHERE DebitCompany in (18, 46) AND terminalNumber=@sTerminalNumber), '--------'),
		--	dbo.GetDecrypted256(@binCardNumber256),
		--	Convert(varchar(10), @dtTrans, 104),
		--	Replace(Convert(varchar(20), @nAmount, 0), '.', ','),
		--	(SELECT CurrencyISOCode FROM [List].[CurrencyList] WITH (NOLOCK) WHERE CurrencyID=@nCurrency),
		--	'',
		--	@sApprovalNumber,
		--	'',
		--	IsNull(@dtDeadline, GETDATE()),
		--	Left(@sComment, 30),
		--	@sDebitRefCode,
		--	'service',
		--	Left(@sFileName, 100),
		--	@dtChb,
		--	CASE IsNull(@bIsChargeback, 0) WHEN 1 THEN 0 ELSE 1 END
		--);

		lblRemoveMatchedRow: --remove the matched row
		IF @bDebug=1 PRINT 'Deleting row';
		DELETE FROM tblChbPending WHERE ID=@nID;

		lblFetchNext: --the row processing complete, fetch the next row
		FETCH NEXT FROM
			curChb
		INTO
			@nID, @sCaseID, @sReasonCode, @dtInsert, @sDebitRefCode, @sDebitRefNum, @sApprovalNumber, @nAmount, @nCurrency, @sTerminalNumber, @binCardNumber256, @nInstallment, @dtTrans,
			@dtChb, @bIsChargeback, @bIsPhotocopy, @sFileName, @dtDeadline;
	END
	CLOSE curChb;
	DEALLOCATE curChb;

	--write end to log
	INSERT INTO tblChbFileLog(ActionType, Details) VALUES (7, 'Server: '+@sServer+', User: '+CURRENT_USER+', Process: '+Cast(@@SPID AS varchar(50)));

	--disable NOCOUNT, if was disabled initially
	IF @bNoCount=0 SET NOCOUNT OFF ELSE SET NOCOUNT ON;
END


GO
