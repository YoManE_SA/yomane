SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [Risk].[spInsertBlacklistItemByCc]
    (
      @nMerchant INT ,
      @sCardNumber VARCHAR(25) ,
      @sComment NVARCHAR(50) = ''
    )
AS 
    BEGIN

        IF @nMerchant = 0 
            SET @nMerchant = NULL;

        INSERT  INTO tblBLCommon
                ( BL_CompanyID ,
                  BL_BlockLevel ,
                  BL_Type ,
                  BL_Value ,
                  BL_Comment
                    
                )
                SELECT  @nMerchant ,
                        1 ,
                        1 ,
                        EmailAddress ,
                        @sComment
                FROM    [Risk].[CcMailUsage]
                WHERE   CreditCardNumber256 = @sCardNumber;

    END
GO
