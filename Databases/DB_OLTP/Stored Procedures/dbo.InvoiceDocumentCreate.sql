SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[InvoiceDocumentCreate]  
(  
 @nTransactionPay int,  
 @nDocumentType int,  
 @nBillingCompany int,  
 @bApplyVAT bit,  
 @bLinesReduceVAT bit,  
 @nMerchant int,  
 @sBillTo nvarchar(100),  
 @nCurrency int,  
 @nCurrencyRate real,  
 @sUsername nvarchar(50),  
 @sDocumentDate nvarchar(20)=NULL,  
 @sLanguage char(3)=NULL  
)  
AS  
BEGIN  
 IF NOT EXISTS (SELECT GD_ID FROM tblGlobalData WHERE GD_Group=60 AND GD_ID=@nDocumentType)  
 BEGIN  
  -- document type not found  
  SELECT -1  
  RETURN  
 END  
 DECLARE @sBillingCompanyName nvarchar(100), @sBillingCompanyAddress nvarchar(500)  
 DECLARE @sBillingCompanyNumber nvarchar(100), @sBillingCompanyEmail nvarchar(100)  
 DECLARE @nVATPercent real, @sBillingCompanyLanguage nchar(3)  
 SELECT  
  @sBillingCompanyName=name,  
  @sBillingCompanyAddress=address,  
  @sBillingCompanyNumber=number,  
  @sBillingCompanyEmail=email,  
  @sBillingCompanyLanguage=LanguageShow,  
  @nVATPercent=VATamount  
 FROM  
  tblBillingCompanys  
 WHERE  
  BillingCompanys_id=@nBillingCompany;  
 IF @sLanguage IS NOT NULL SET @sBillingCompanyLanguage=@sLanguage;  
 IF @nVATPercent IS NULL  
 BEGIN  
  -- Billing company not found  
  SELECT -2  
  RETURN  
 END  
 IF @nMerchant=0 AND IsNull(@sBillTo, '')=''  
 BEGIN  
  -- neither merchant nor Invoice name specified  
  SELECT -3  
  RETURN  
 END  
 IF @nMerchant>0 AND IsNull(@sBillTo, '')<>''  
 BEGIN  
  -- both merchant and Invoice name specified  
  SELECT -4  
  RETURN  
 END  
 IF @nMerchant>0 AND NOT EXISTS (SELECT ID FROM tblCompany WHERE ID=@nMerchant)  
 BEGIN  
  -- merchant not found  
  SELECT -5  
  RETURN  
 END  
 IF @nTransactionPay>0 AND NOT EXISTS (SELECT ID FROM tblTransactionPay WHERE ID=@nTransactionPay AND IsNull(InvoiceDocumentID, 0)=0)  
 BEGIN  
  -- unattended transaction pay not found  
  SELECT -7  
  RETURN  
 END  
 IF @nMerchant>0 AND NOT EXISTS (SELECT ID FROM tblInvoiceLine WHERE il_MerchantID=@nMerchant AND il_TransactionPayID=@nTransactionPay AND il_DocumentID IS NULL)  
 BEGIN  
  -- no lines for this merchant/transaction pay  
  -- if there is a transaction pay, create empty line  
  --IF @nTransactionPay>0 EXEC InvoiceLineCreate @nTransactionPay, '', @nMerchant, 'EMPTY LINE', 1, 0.001, @nCurrency, @nCurrencyRate, @sUsername  
  IF @nMerchant>0 AND NOT EXISTS (SELECT ID FROM tblInvoiceLine WHERE il_MerchantID=@nMerchant AND il_TransactionPayID=@nTransactionPay AND il_DocumentID IS NULL)  
  BEGIN  
   SELECT -8  
   RETURN  
  END  
 END  
 IF @sBillTo<>'' AND NOT EXISTS (SELECT ID FROM tblInvoiceLine WHERE il_BillToName=@sBillTo AND il_TransactionPayID=@nTransactionPay AND il_DocumentID IS NULL)  
 BEGIN  
  -- no lines for this billing name/transaction pay  
  -- if there is a transaction pay, create empty line  
  IF @nTransactionPay>0 EXEC InvoiceLineCreate @nTransactionPay, @sBillTo, 0, 'EMPTY LINE', 1, 0.001, @nCurrency, @nCurrencyRate, @sUsername  
  IF @sBillTo<>'' AND NOT EXISTS (SELECT ID FROM tblInvoiceLine WHERE il_BillToName=@sBillTo AND il_TransactionPayID=@nTransactionPay AND il_DocumentID IS NULL)  
  BEGIN  
   SELECT -9  
   RETURN  
  END  
 END  
 IF @nMerchant>0 BEGIN  
  SELECT @sBillTo=CompanyLegalName FROM tblCompany WHERE ID=@nMerchant  
  IF Replace(@sBillTo, ' ', '')=''  
  BEGIN  
   -- "Company Legal Name" not specified for this merchant  
   SELECT -10  
   RETURN  
  END  
  -- store billing name and address in one field  
  SELECT  
   @sBillTo=CompanyLegalName+'<br />'+CompanyStreet+' '+CompanyCity+' '+CompanyZip  
  FROM  
   tblCompany  
  WHERE  
   ID=@nMerchant  
 END  
  
 DECLARE @nInvoiceNumber int  
 SET @nInvoiceNumber=0  
 IF @nTransactionPay>0  
  SELECT  
   @sBillingCompanyName=CASE BillingCompanyName WHEN '' THEN @sBillingCompanyName ELSE BillingCompanyName END,  
   @sBillingCompanyAddress=CASE BillingCompanyAddress WHEN '' THEN @sBillingCompanyAddress ELSE BillingCompanyAddress END,  
   @sBillingCompanyNumber=CASE BillingCompanyNumber WHEN '' THEN @sBillingCompanyNumber ELSE BillingCompanyNumber END,  
   @sBillingCompanyEmail=CASE BillingCompanyEmail WHEN '' THEN @sBillingCompanyEmail ELSE BillingCompanyEmail END,  
   @sBillTo=CASE BillingToInfo WHEN '' THEN @sBillTo ELSE BillingToInfo END,  
   @nVATPercent=CASE vatamount WHEN 0 THEN @nVATPercent ELSE vatamount END,  
   @nInvoiceNumber=InvoiceNumber  
  FROM  
   tblTransactionPay  
  WHERE  
   ID=@nTransactionPay  
 IF @nInvoiceNumber=0  
  SELECT  
   @nInvoiceNumber=IsNull(Max(id_InvoiceNumber), 0)+1  
  FROM  
   tblInvoiceDocument  
  WHERE  
   id_BillingCompanyID=@nBillingCompany  
   AND  
   id_Type=@nDocumentType  
 -- validated, can create  
 DECLARE @dtDocumentDate datetime  
 SET @dtDocumentDate=GetDate();  
 IF @sDocumentDate IS NOT NULL SET @dtDocumentDate=Convert(datetime, @sDocumentDate, 120);  
 INSERT INTO  
  tblInvoiceDocument  
  (  
   id_Type,  
   id_BillingCompanyID,  
   id_InvoiceNumber,  
   id_ApplyVAT,  
   id_MerchantID,  
   id_BillToName,  
   id_Currency,  
   id_CurrencyRate,  
   id_VATPercent,  
   id_Username,  
   id_TransactionPayID,  
   id_BillingCompanyName,  
   id_BillingCompanyAddress,  
   id_BillingCompanyNumber,  
   id_BillingCompanyEmail,  
   id_BillingCompanyLanguage,  
   id_InsertDate,  
   id_PrintDate   
  )  
 VALUES  
  (  
   @nDocumentType,  
   @nBillingCompany,  
   @nInvoiceNumber,  
   @bApplyVAT,  
   @nMerchant,  
   @sBillTo,  
   @nCurrency,  
   @nCurrencyRate,  
   @nVATPercent,  
   @sUsername,  
   @nTransactionPay,  
   @sBillingCompanyName,  
   @sBillingCompanyAddress,  
   @sBillingCompanyNumber,  
   @sBillingCompanyEmail,  
   @sBillingCompanyLanguage,  
   @dtDocumentDate,  
   @dtDocumentDate  
  )  
 -- succeeded, rebuild  
 DECLARE @nID int  
 SET @nID=@@IDENTITY  
 EXEC InvoiceDocumentRebuild @nID, @bLinesReduceVAT  
 SELECT @nID  
 IF @nTransactionPay>0  
  UPDATE  
   tblTransactionPay  
  SET  
   InvoiceDocumentID=@nID,  
   InvoiceNumber=@nInvoiceNumber  
  WHERE  
   ID=@nTransactionPay  
END

GO
