SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoginAttemptResetFailCount](@nRefType int, @nRefID int)
AS
BEGIN
	UPDATE
		tblPasswordHistory
	SET
		LPH_FailCount=0, LPH_LastFail=LPH_Insert, LPH_LastSuccess=GetDate()
	WHERE
		LPH_RefID=@nRefID AND LPH_ID=1 AND LPH_RefType=@nRefType
END
GO
