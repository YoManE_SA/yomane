SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [Track].[spGetRunningProcessCount]
    (
      @nMerchant INT ,
      @sCardNumber VARCHAR(25)
    )
AS 
    BEGIN
        IF @nMerchant = 35 AND REPLACE(@sCardNumber, ' ', '') = '4580000000000000' 
            RETURN 0;
            
        DECLARE
			@nCount INT ,
            @CCEncrypted VARBINARY(40);
            
        SET @CCEncrypted = dbo.GetEncrypted256(REPLACE(@sCardNumber, ' ', ''));
        
        DELETE  FROM [Track].[RunningProcess]
        WHERE   InsertDate < DATEADD(minute, -5, GETDATE());
        
        SELECT  @nCount = COUNT(*)
        FROM    [Track].[RunningProcess]
        WHERE   [Merchant_id] = @nMerchant
                AND [CreditCardNumber256] = @CCEncrypted;
                
        INSERT  INTO [Track].[RunningProcess]
                ( [Merchant_id], [CreditCardNumber256] )
        VALUES  ( @nMerchant, @CCEncrypted );
        
        RETURN	@nCount;
    END
GO
