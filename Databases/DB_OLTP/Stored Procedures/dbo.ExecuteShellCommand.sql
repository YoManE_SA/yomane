SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ExecuteShellCommand](@sCommand nvarchar(4000), @bDebug bit=0)
AS
BEGIN
	IF @bDebug=1 PRINT 'Executing shell command>'+@sCommand;
	SET @sCommand='EXEC xp_cmdshell '''+@sCommand+'''';
	IF IsNull(@bDebug, 0)=0 SET @sCommand=@sCommand+', no_output';
	EXEC (@sCommand)
END
GO
