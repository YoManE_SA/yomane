SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [Risk].[spInsertBlacklistCreditCard]
    (
      @nMerchant INT ,
      @sCardNumber VARCHAR(25) ,
      @sComment NVARCHAR(500)
    )
AS 
    BEGIN

        INSERT  INTO tblFraudCcBlackList
                ( company_id ,
                  fcbl_ccDisplay ,
                  fcbl_ccNumber256 ,
                  fcbl_comment
	            )
        VALUES  ( @nMerchant ,
                  dbo.fnFormatCcNumToHideDigits(@sCardNumber) ,
                  dbo.GetEncrypted256(REPLACE(@sCardNumber, ' ', '')) ,
                  @sComment
	            );

    END

GO
