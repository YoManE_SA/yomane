SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CreateWalletIdentity]
(
	@sName nvarchar(50),
	@sIdentity nvarchar(50),
	@sFolder nvarchar(50),
	@sBrandName nvarchar(50),
	@sCompanyName nvarchar(50),
	@sCustomerURL nvarchar(50),
	@sContentURL nvarchar(50),
	@sMailFrom nvarchar(50),
	@nMerchantNumber int,
	@bIsHebrewVisible bit
)
AS
BEGIN
	INSERT INTO
		tblWalletIdentity
		(
			wi_Name,
			wi_Identity,
			wi_Folder,
			wi_BrandName,
			wi_CompanyName,
			wi_DevCenterURL,
			wi_ProcessURL,
			wi_MerchantURL,
			wi_CustomerURL,
			wi_ContentURL,
			wi_MerchantUploadsFolder,
			wi_MailServer,
			wi_MailUsername,
			wi_MailPassword,
			wi_MailFrom,
			wi_Currencies,
			wi_FormsInbox,
			wi_DatabaseNumber,
			wi_MerchantNumber,
			wi_IsHebrewVisible,
			wi_IsSendMerchant,
			wi_IsSendCustomer
		)
	SELECT
		wi_Name,
		wi_Identity,
		wi_Folder,
		wi_BrandName,
		wi_CompanyName,
		wi_DevCenterURL,
		wi_ProcessURL,
		wi_MerchantURL,
		wi_CustomerURL,
		wi_ContentURL,
		wi_MerchantUploadsFolder,
		wi_MailServer,
		wi_MailUsername,
		wi_MailPassword,
		wi_MailFrom,
		wi_Currencies,
		wi_FormsInbox,
		wi_DatabaseNumber,
		wi_MerchantNumber,
		wi_IsHebrewVisible,
		wi_IsSendMerchant,
		wi_IsSendCustomer
	FROM
		tblWalletIdentity
	WHERE
		ID=(SELECT Max(ID) FROM tblWalletIdentity);
	UPDATE
		tblWalletIdentity
	SET
		wi_Name=@sName,
		wi_Identity=@sIdentity,
		wi_Folder=@sFolder,
		wi_BrandName=@sBrandName,
		wi_CompanyName=@sCompanyName,
		wi_CustomerURL=@sCustomerURL,
		wi_ContentURL=@sContentURL,
		wi_MailFrom=@sMailFrom,
		wi_FormsInbox='https://www.intercash.net/member/forms_inbox.aspx?Site=UserAdmin&Identity='+@sIdentity,
		wi_MerchantNumber=@nMerchantNumber,
		wi_IsHebrewVisible=@bIsHebrewVisible
	WHERE
		ID=(SELECT Max(ID) FROM tblWalletIdentity);
	SELECT * FROM tblWalletIdentity WHERE ID=(SELECT Max(ID) FROM tblWalletIdentity);
END
GO
