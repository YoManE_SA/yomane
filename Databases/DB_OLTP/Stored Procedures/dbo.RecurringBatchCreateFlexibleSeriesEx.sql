SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RecurringBatchCreateFlexibleSeriesEx]
(
	@bPass bit,
	@nTransaction int,
	@sRecurring varchar(max),
	@sComments nvarchar(200)='',
	@nTrmCode int=0,
	@sIP nvarchar(50)='',
	@nIsRecurringApproval int=-1,
	@sIdentifier varchar(50)=NULL,
	@nPrecreatedSeriesID int=NULL
)
AS
BEGIN
	SET @sRecurring=Replace(Replace(@sRecurring, '|', '&'), '^', '=');
	IF @nIsRecurringApproval IS NULL SET @nIsRecurringApproval=-1;
	DECLARE @nSeries int, @nCharge int, @nAttempt int, @nAttemptOld int, @sRecurringCurrent varchar(20), @i int;
	SELECT @nAttemptOld=Max(ID) FROM tblRecurringAttempt WHERE ra_TransPass=@nTransaction OR ra_TransPending=@nTransaction;
	SET @sRecurringCurrent=dbo.GetQueryStringValue('Recurring1', @sRecurring);
	EXEC RecurringCreateSeriesEx @bPass, @nTransaction, @sRecurringCurrent, @sComments, @nTrmCode, @sIP, @sIdentifier, @nPrecreatedSeriesID;
	SELECT @nAttempt=Max(ID) FROM tblRecurringAttempt WHERE ra_TransPass=@nTransaction OR ra_TransPending=@nTransaction OR ra_TransApproval=@nTransaction;
	IF @nAttempt=@nAttemptOld
	BEGIN
		SELECT -111
		RETURN
	END
	SELECT @nCharge=ra_Charge FROM tblRecurringAttempt WHERE ID=@nAttempt;
	SELECT @nSeries=rc_Series FROM tblRecurringCharge WHERE ID=@nCharge;
	SET @i=2;
	SET @sRecurringCurrent=dbo.GetQueryStringValue('Recurring'+LTrim(RTrim(@i)), @sRecurring);
	WHILE Len(@sRecurringCurrent)>0
	BEGIN
		EXEC RecurringExtendSeriesEx @nSeries, @sRecurringCurrent;
		SET @i=@i+1;
		SET @sRecurringCurrent=dbo.GetQueryStringValue('Recurring'+LTrim(RTrim(@i)), @sRecurring);
	END
	IF @nIsRecurringApproval NOT IN (0, 1) SET @nIsRecurringApproval=1-@bPass;
	UPDATE tblRecurringSeries SET rs_Approval=@nIsRecurringApproval, rs_FirstApproval=1-@bPass WHERE ID=@nSeries;
	SELECT @nSeries;
END
GO
