SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoadWireBankAccount]
(
	@nWire int,
	@nAccountType int,
	@nMerchant int=NULL
)
AS
BEGIN
	DECLARE @s2 varchar(1), @sSQL nvarchar(4000)
	SET @s2=CASE @nAccountType WHEN 2 THEN '2' ELSE '' END;
	SET @sSQL='SELECT wirePaymentAbroadAccountName'+@s2+' AccountName';
	SET @sSQL=@sSQL+', wirePaymentAbroadAccountNumber'+@s2+' AccountNumber';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankName'+@s2+' BankName';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddress'+@s2+' BankAddress';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddressSecond'+@s2+' BankAddressSecond';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddressCity'+@s2+' BankAddressCity';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddressState'+@s2+' BankAddressState';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddressCountry'+@s2+' BankAddressCountry';
	SET @sSQL=@sSQL+', wirePaymentAbroadSwiftNumber'+@s2+' SwiftNumber';
	SET @sSQL=@sSQL+', wirePaymentAbroadIBAN'+@s2+' IBAN';
	SET @sSQL=@sSQL+', wirePaymentAbroadABA'+@s2+' ABA';
	SET @sSQL=@sSQL+', wirePaymentAbroadSortCode'+@s2+' SortCode';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddressZip'+@s2+' BankAddressZip';
	SET @sSQL=@sSQL+', wirePaymentAbroadSepaBic'+@s2+' SepaBic';
	SET @sSQL=@sSQL+' FROM tblWireMoney WHERE WireMoney_ID='+Str(@nWire);
	IF @nMerchant IS NOT NULL SET @sSQL=@sSQL+' AND Company_ID='+Str(@nMerchant);
	EXEC sp_executesql @sSQL
END
GO
