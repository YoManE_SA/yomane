SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [Risk].[spInsertBlacklistCreditCardByEmail]
    (
      @nMerchant INT ,
      @sMail NVARCHAR(80) ,
      @sComment NVARCHAR(500)
    )
AS 
    BEGIN

        INSERT  INTO tblFraudCcBlackList
                ( company_id ,
                  fcbl_ccDisplay ,
                  fcbl_ccNumber256 ,
                  fcbl_comment
	            )
                SELECT  @nMerchant ,
                        dbo.fnFormatCcNumToHideDigits(dbo.GetDecrypted256(CreditCardNumber256)) ,
                        CreditCardNumber256 ,
                        @sComment
                FROM    [Risk].[CcMailUsage]
                WHERE   EmailAddress = @sMail;

    END

GO
