SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [Trans].[spGetPayerTransactions]
    (
      @MerchantID AS INT = NULL ,
	  @FirstName AS NVARCHAR(50) = NULL ,
	  @LastName AS NVARCHAR(50) = NULL ,
	  @PhoneNumber AS NVARCHAR(50) = NULL ,
	  @EmailAddress AS NVARCHAR(80) = NULL ,
      @RowStart AS SMALLINT = NULL ,
	  @RowShowCount AS SMALLINT = NULL ,
	  @RowCount AS INT OUTPUT
    )
AS 
    BEGIN

		SET NOCOUNT ON;
		
		-- Validate input data
		IF @RowStart IS NULL OR @RowShowCount IS NULL
		BEGIN
			SET @RowStart = 1;
			SET @RowShowCount = 19;
		END      

		-- Insert relevent payer id into temp table     
		DECLARE @TempPayerInfo TABLE (TransPayerInfo_id INT NOT NULL);
			 
        INSERT  INTO @TempPayerInfo
                SELECT  [TransPayerInfo_id]
                FROM    [Trans].[TransPayerInfo]
                WHERE   [Merchant_id] = @MerchantID
                        AND ISNULL([FirstName],'') = @FirstName
                        AND ISNULL([LastName],'') = @LastName
                        AND ISNULL([PhoneNumber],'') = @PhoneNumber
                        AND ISNULL([EmailAddress],'') = @EmailAddress

		-- Return count
		SELECT @RowCount = COUNT(*) FROM @TempPayerInfo;

		-- Return transactions
		SELECT  * FROM  ( 
				   SELECT    ROW_NUMBER() OVER ( ORDER BY insertDate DESC ) AS 'RowNumber' ,
                            TransactionStatus ,
                            id AS 'TransID' ,
                            insertDate AS 'TransDate' ,
                            amount AS 'TransAmount' ,
                            currency AS 'TransCurrency' ,
							Payments AS 'TransInstallments' ,
							CreditType AS 'TransCreditType' ,
							TransType ,
							TransSource_id ,
							DeniedStatus AS 'ChbStatus' ,
							DeniedDate AS 'ChbDate' ,
							isTestOnly AS 'TransIsTestOnly' ,
							PayID AS 'PayID' ,
							PrimaryPayedID AS 'PrimaryPayedID' ,
							isPendingChargeback ,
							NetpayFee_transactionCharge AS 'TransactionChargeFee' ,
							PaymentMethod_id ,
							PaymentMethodText ,
							IssuerCountryIsoCode ,
							ReplyCode ,
							ApprovalNumber ,
							TransAnswerID ,
							companyID AS 'Merchant_id'
                  FROM      ( SELECT    1 AS 'TransactionStatus' ,
										T1.ID ,
										T1.InsertDate ,
										T1.Amount ,
										T1.Currency ,
										T1.Payments ,
										T1.CreditType ,
										T1.isTestOnly ,
										T1.PayID ,
										T1.PrimaryPayedID ,
										0 AS 'TransType' ,
										PM1.PaymentMethodText ,
										PM1.IssuerCountryIsoCode ,
										PM1.PaymentMethod_id ,
										T1.NetpayFee_transactionCharge ,
										T1.IsPendingChargeback ,
										T1.TransSource_id ,
										T1.companyID ,
										T1.DeniedStatus ,
										T1.DeniedDate ,
										'000' AS ReplyCode ,
										T1.ApprovalNumber ,
										0 AS 'TransAnswerID'
                              FROM      [dbo].[tblCompanyTransPass] AS T1 LEFT JOIN [Trans].[TransPaymentMethod] AS PM1 ON T1.TransPaymentMethod_id = PM1.TransPaymentMethod_id
                              WHERE     T1.TransPayerInfo_id IN ( SELECT TransPayerInfo_id FROM @TempPayerInfo )
							  UNION ALL
                              SELECT    2 AS 'TransactionStatus' ,
                                        T2.ID ,
                                        T2.InsertDate ,
                                        T2.Amount ,
                                        T2.Currency ,
                                        T2.Payments ,
                                        T2.CreditType ,
                                        T2.isTestOnly ,
                                        NULL AS 'PayID' ,
                                        T2.PayID AS 'PrimaryPayedID' ,
                                        T2.TransType ,
                                        PM2.PaymentMethodText ,
                                        PM2.IssuerCountryIsoCode ,
                                        PM2.PaymentMethod_id ,
                                        T2.NetpayFee_transactionCharge ,
                                        0 AS 'IsPendingChargeback' ,
                                        T2.TransSource_id ,
                                        T2.companyID ,
                                        0 AS 'DeniedStatus' ,
                                        NULL AS 'DeniedDate' ,
										T2.ReplyCode ,
										NULL AS 'ApprovalNumber' ,
										0 AS 'TransAnswerID'
                              FROM      [dbo].[tblCompanyTransFail] AS T2 LEFT JOIN [Trans].[TransPaymentMethod] AS PM2 ON T2.TransPaymentMethod_id = PM2.TransPaymentMethod_id
                              WHERE     T2.TransPayerInfo_id IN ( SELECT TransPayerInfo_id FROM @TempPayerInfo )
                              UNION ALL
                              SELECT    3 AS 'TransactionStatus' ,
                                        T3.ID ,
                                        T3.InsertDate ,
                                        T3.Amount ,
                                        T3.Currency ,
                                        0 AS 'Payments' ,
                                        T3.CreditType ,
                                        T3.isTestOnly ,
                                        NULL AS 'PayID' ,
                                        PayID AS 'PrimaryPayedID' ,
                                        1 AS 'TransType' ,
                                        PM3.PaymentMethodText ,
                                        PM3.IssuerCountryIsoCode ,
                                        PM3.PaymentMethod_id ,
                                        T3.NetpayFee_transactionCharge ,
                                        0 AS 'IsPendingChargeback' ,
                                        T3.TransSource_id ,
                                        T3.companyID ,
                                        0 AS 'DeniedStatus' ,
                                        NULL AS 'DeniedDate' ,
										T3.replyCode ,
										T3.ApprovalNumber ,
										T3.TransAnswerID
                              FROM      [dbo].[tblCompanyTransApproval] AS T3 LEFT JOIN [Trans].[TransPaymentMethod] AS PM3 ON T3.TransPaymentMethod_id = PM3.TransPaymentMethod_id
                              WHERE     T3.TransPayerInfo_id IN ( SELECT TransPayerInfo_id FROM @TempPayerInfo )
                              UNION ALL
                              SELECT    4 AS 'TransactionStatus' ,
                                        T4.ID ,
                                        T4.InsertDate ,
                                        T4.trans_amount AS 'amount' ,
                                        T4.trans_currency AS 'currency' ,
                                        0 AS 'Payments' ,
                                        T4.trans_creditType AS 'CreditType' ,
                                        T4.isTestOnly ,
                                        NULL AS 'PayID' ,
                                        0 AS 'PrimaryPayedID' ,
                                        T4.trans_type ,
                                        PM4.PaymentMethodText ,
                                        PM4.IssuerCountryIsoCode ,
                                        PM4.PaymentMethod_id ,
                                        0 AS 'NetpayFee_transactionCharge' ,
                                        0 AS 'IsPendingChargeback' ,
                                        T4.TransSource_id ,
                                        T4.companyID ,
                                        0 AS 'DeniedStatus' ,
                                        NULL AS 'DeniedDate' ,
										T4.replyCode ,
										T4.DebitApprovalNumber ,
										0 AS 'TransAnswerID'
                              FROM      [dbo].[tblCompanyTransPending] AS T4 LEFT JOIN [Trans].[TransPaymentMethod] AS PM4 ON T4.TransPaymentMethod_id = PM4.TransPaymentMethod_id
                              WHERE     T4.TransPayerInfo_id IN ( SELECT TransPayerInfo_id FROM @TempPayerInfo )
                            ) a
                ) b
        WHERE   RowNumber BETWEEN @RowStart AND @RowStart + @RowShowCount
        ORDER BY RowNumber ASC;

		SET NOCOUNT OFF;

   END
GO
