SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CheckDebitTerminalMonthlyLimits]
AS
BEGIN
	DECLARE @nCompany int, @nTerminal int, @sCompany nvarchar(100), @sTerminal nvarchar(100), @sNumber nvarchar(100), @nLimit int, @nQuantity int
	DECLARE @sMail nvarchar(100), @sSMS nvarchar(100), @sSubject nvarchar(100), @sBody nvarchar(1000), @sLogText nvarchar(950), @sPaymentMethod varchar(20)
	DECLARE curData CURSOR FOR
		SELECT
			DebitCompanyID, TerminalID, dc_Name, dt_Name, TerminalNumber, Limit, Quantity, NotifyUsers, NotifyUsersSMS, 'VISA'
		FROM
			viewDebitTerminalMonthlyLimits
		WHERE
			WasSent=0 AND Limit>0 AND Quantity>=Limit AND NOT (NotifyUsers='' AND NotifyUsersSMS='')
		UNION
		SELECT
			DebitCompanyID, TerminalID, dc_Name, dt_Name, TerminalNumber, LimitMC, QuantityMC, NotifyUsersMC, NotifyUsersSMSMC, 'MC'
		FROM
			viewDebitTerminalMonthlyLimits
		WHERE
			WasSentMC=0 AND LimitMC>0 AND QuantityMC>=LimitMC AND NOT (NotifyUsersMC='' AND NotifyUsersSMSMC='')
	OPEN curData
	FETCH NEXT FROM curData INTO @nCompany, @nTerminal, @sCompany, @sTerminal, @sNumber, @nLimit, @nQuantity, @sMail, @sSMS, @sPaymentMethod
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @sSubject='Monthly Limit: '+@sCompany+' - '+@sTerminal+' - '+@sNumber+' - '+@sPaymentMethod
		IF IsNull(@sMail, '')<>''
		BEGIN
			SET @sBody='Debit Company: '+@sCompany+'<'+'br />'
			SET @sBody=@sBody+'Debit Terminal: '+@sTerminal+'<'+'br />'
			SET @sBody=@sBody+'Terminal Number: '+@sNumber+'<'+'br />'
			SET @sBody=@sBody+'Card Type: '+@sPaymentMethod+'<'+'br />'
			SET @sBody=@sBody+'Monthly Limit: '+Cast(@nLimit as nvarchar(10))+'<'+'br />'
			SET @sBody=@sBody+'Chargebacks: '+Cast(@nQuantity as nvarchar(10))+'<'+'br />'
			SET @sBody=@sBody+'Server: '+Cast(ServerProperty('MachineName') AS nvarchar(10))+'<'+'br />'
			SET @sBody=@sBody+'Sent on: '+Convert(nvarchar(25), GetDate(), 120)+'<'+'br />'
			EXEC SendMail @sMail, @sSubject, @sBody
			SET @sLogText='Sent to: '+@sMail+'<'+'br />'+@sBody
			EXEC DebitBlockLogWrite	'MONTHLY MAIL SENT', @sLogText, 0, @nCompany, @nTerminal
		END
		IF NOT IsNull(@sSMS, '')=''
		BEGIN
			SET @sBody='Monthly CHB Limit: '+@sCompany+'.'
			SET @sBody=@sBody+' Card Type: '+@sPaymentMethod+'.'
			SET @sBody=@sBody+' Terminal: '+@sNumber+' '+@sTerminal+'.'
			SET @sBody=@sBody+' Limit: '+Cast(@nLimit as nvarchar(10))+'.'
			SET @sBody=@sBody+' Chargebacks: '+Cast(@nQuantity as nvarchar(10))+'.'
			SET @sBody=@sBody+' Server: '+Cast(ServerProperty('MachineName') AS nvarchar(10))+'.'
			SET @sBody=@sBody+' Sent on: '+Convert(nvarchar(25), GetDate(), 120)
			EXEC SendSMS @sSMS, @sBody
			SET @sLogText='Sent to: '+@sSMS+'. '+@sBody
			EXEC DebitBlockLogWrite	'MONTHLY SMS SENT', @sLogText, 0, @nCompany, @nTerminal
		END
		IF NOT IsNull(@sSMS, '')+IsNull(@sMail, '')=''
			IF @sPaymentMethod='MC'
				UPDATE tblDebitTerminals SET dt_MonthlyMCCHBWasSent=1, dt_MonthlyMCCHBSendDate=GetDate() WHERE ID=@nTerminal
			ELSE
				UPDATE tblDebitTerminals SET dt_MonthlyCHBWasSent=1, dt_MonthlyCHBSendDate=GetDate() WHERE ID=@nTerminal;
		FETCH NEXT FROM curData INTO @nCompany, @nTerminal, @sCompany, @sTerminal, @sNumber, @nLimit, @nQuantity, @sMail, @sSMS, @sPaymentMethod
	END
	CLOSE curData
	DEALLOCATE curData
END
GO
