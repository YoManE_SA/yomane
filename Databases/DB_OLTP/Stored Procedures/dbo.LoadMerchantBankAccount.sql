SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoadMerchantBankAccount]
(
	@nMerchant int,
	@nCurrency int=-1,
	@nAccountType int
)
AS
BEGIN
	DECLARE @s2 varchar(1), @sPrefix varchar(13), @sSQL nvarchar(4000)
	SET @s2=CASE @nAccountType WHEN 2 THEN '2' ELSE '' END;
	SET @sPrefix=CASE @nCurrency WHEN -1 THEN 'paymentAbroad' ELSE 'mba_' END;
	SET @sSQL='SELECT '+@sPrefix+'AccountName'+@s2+' AccountName';
	SET @sSQL=@sSQL+', '+@sPrefix+'AccountNumber'+@s2+' AccountNumber';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankName'+@s2+' BankName';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddress'+@s2+' BankAddress';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddressSecond'+@s2+' BankAddressSecond';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddressCity'+@s2+' BankAddressCity';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddressState'+@s2+' BankAddressState';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddressCountry'+@s2+' BankAddressCountry';
	SET @sSQL=@sSQL+', '+@sPrefix+'SwiftNumber'+@s2+' SwiftNumber';
	SET @sSQL=@sSQL+', '+@sPrefix+'IBAN'+@s2+' IBAN';
	SET @sSQL=@sSQL+', '+@sPrefix+'ABA'+@s2+' ABA';
	SET @sSQL=@sSQL+', '+@sPrefix+'SortCode'+@s2+' SortCode';
	SET @sSQL=@sSQL+', '+@sPrefix+'BankAddressZip'+@s2+' BankAddressZip';
	SET @sSQL=@sSQL+', '+@sPrefix+'SepaBic'+@s2+' SepaBic';
	IF @nCurrency=-1
		SET @sSQL=@sSQL+' FROM tblCompany WHERE ID='+Str(@nMerchant)
	ELSE
		SET @sSQL=@sSQL+' FROM tblMerchantBankAccount WHERE mba_Merchant='+Str(@nMerchant)+' AND mba_Currency='+Str(@nCurrency);
	EXEC sp_executesql @sSQL;
END
GO
