SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[spCleanupCreditCardNumbers]
AS 
    UPDATE  tblCreditCard
    SET     CCard_Number256 = NULL ,
            cc_cui = NULL
    FROM    tblCreditCard
            INNER JOIN tblCompanyTransPass ON tblCreditCard.ID = tblCompanyTransPass.CreditCardID
    WHERE   tblCreditCard.CCard_Number256 IS NOT NULL
            AND tblCompanyTransPass.PaymentMethod_id = 1
            AND DATEDIFF(d, tblCompanyTransPass.InsertDate, GETDATE()) > 370
            AND tblCreditCard.ID NOT IN ( SELECT DISTINCT
                                                    rc_CreditCard
                                          FROM      viewRecurringActiveCharges )


GO
