SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[RecurringAttemptTodayCharges] ( @nMerchant INT = 0 )
AS 
    BEGIN

        DECLARE @nCharge AS INT ,
            @nReply AS INT ,
            @sReply AS NVARCHAR(1000);

        DECLARE curCharges CURSOR

        FOR
            SELECT  ID
            FROM    viewRecurringActiveCharges
            WHERE   CAST(FLOOR(CAST(rc_Date AS FLOAT)) AS DATETIME) <= CAST(FLOOR(CAST(GETDATE() AS FLOAT)) AS DATETIME)
                    AND @nMerchant IN ( 0, rs_Company )
        OPEN curCharges

        FETCH NEXT FROM curCharges INTO @nCharge

        WHILE @@FETCH_STATUS = 0 
            BEGIN
                EXEC RecurringAttemptCharge @nCharge, @nReply OUTPUT,
                    @sReply OUTPUT;
                PRINT 'Charge ' + LTRIM(RTRIM(STR(@nCharge))) + ': '
                    + LTRIM(RTRIM(STR(@nReply))) + ' - ' + @sReply
                FETCH NEXT FROM curCharges INTO @nCharge
            END
            
        CLOSE curCharges;
        DEALLOCATE curCharges;
    END

GO
