SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[RecurringExtendSeriesEx](@nSeries int, @sRecurring varchar(20))
AS
BEGIN
	DECLARE @nAmount money
	SET @nAmount=0
	IF @sRecurring LIKE '%A%'
	BEGIN
		SET @nAmount=Right(@sRecurring, Len(@sRecurring)-CharIndex('A', @sRecurring))
		SET @sRecurring=Left(@sRecurring, CharIndex('A', @sRecurring)-1)
	END
	DECLARE @nChargeCount int, @nIntervalUnit int, @nIntervalLength int, @sUnit char(1)
	IF @sRecurring LIKE '%D%' SET @sUnit='D'
	IF @sRecurring LIKE '%W%' SET @sUnit='W'
	IF @sRecurring LIKE '%M%' SET @sUnit='M'
	IF @sRecurring LIKE '%Q%' SET @sUnit='Q'
	IF @sRecurring LIKE '%Y%' SET @sUnit='Y'
	SET @nChargeCount=Cast(Left(@sRecurring, CharIndex(@sUnit, @sRecurring)-1) AS int)
	SET @nIntervalUnit=CASE @sUnit WHEN 'D' THEN 1 WHEN 'W' THEN 2 WHEN 'Y' THEN 4 ELSE 3 END
	SET @nIntervalLength=Right(@sRecurring, Len(@sRecurring)-CharIndex(@sUnit, @sRecurring))
	IF @sUnit='Q' SET @nIntervalLength=@nIntervalLength*3;
	INSERT INTO tblRecurringCharge
	(
		rc_Series,
		rc_ChargeNumber,
		rc_Date,	
		rc_CreditCard,
		rc_ECheck,
		[TransPaymentMethod_id],
		rc_Amount,
		rc_Currency
	)
	SELECT
		@nSeries,
		rs.rs_ChargeCount+1,
		CASE rs.rs_IntervalUnit
			WHEN 1 THEN DateAdd(d, rs.rs_IntervalLength, rc_Date)
			WHEN 2 THEN DateAdd(wk, rs.rs_IntervalLength, rc_Date)
			WHEN 3 THEN DateAdd(m, rs.rs_IntervalLength, rc_Date)
			ELSE DateAdd(yy, rs.rs_IntervalLength, rc_Date)
		END,
		rc.rc_CreditCard,
		rc.rc_ECheck,
		rc.[TransPaymentMethod_id],
		CASE @nAmount WHEN 0 THEN rs.rs_ChargeAmount ELSE @nAmount END,
		rs.rs_Currency
	FROM
		tblRecurringSeries as rs WITH (NOLOCK)
		INNER JOIN tblRecurringCharge as rc  WITH (NOLOCK) ON rs.ID=rc.rc_Series
	WHERE
		rs.ID = @nSeries
		AND
		rc.rc_ChargeNumber = rs.rs_ChargeCount;
	
	UPDATE
		tblRecurringSeries
	SET
		rs_Flexible=1,
		rs_SeriesTotal=(SELECT Sum(rc_Amount) FROM tblRecurringCharge WITH (NOLOCK) WHERE rc_Series=@nSeries),
		rs_ChargeCount=rs_ChargeCount+1,
		rs_IntervalUnit=@nIntervalUnit,
		rs_IntervalLength=@nIntervalLength,
		rs_Paid=(SELECT 1-Sign(Count(ID)) FROM tblRecurringCharge WITH (NOLOCK) WHERE rc_Series=@nSeries AND rc_Attempts=0)
	WHERE
		ID=@nSeries;
	
	DECLARE @i int
	SET @i=2
	WHILE @i<=@nChargeCount
	BEGIN
		INSERT INTO tblRecurringCharge
		(
			rc_Series,
			rc_ChargeNumber,
			rc_Date,	
			rc_CreditCard,
			rc_ECheck,
			[TransPaymentMethod_id],
			rc_Amount,
			rc_Currency
		)
		SELECT
			@nSeries,
			rs.rs_ChargeCount+1,
			CASE rs.rs_IntervalUnit
				WHEN 1 THEN DateAdd(d, rs.rs_IntervalLength, rc_Date)
				WHEN 2 THEN DateAdd(wk, rs.rs_IntervalLength, rc_Date)
				WHEN 3 THEN DateAdd(m, rs.rs_IntervalLength, rc_Date)
				ELSE DateAdd(yy, rs.rs_IntervalLength, rc_Date)
			END,
			rc.rc_CreditCard,
			rc.rc_ECheck,
			rc.[TransPaymentMethod_id],
			CASE @nAmount WHEN 0 THEN rs.rs_ChargeAmount ELSE @nAmount END,
			rs.rs_Currency
		FROM
			tblRecurringSeries as rs WITH (NOLOCK)
			INNER JOIN tblRecurringCharge as rc WITH (NOLOCK) ON rs.ID=rc.rc_Series
		WHERE
			rs.ID=@nSeries
			AND
			rc.rc_ChargeNumber=rs.rs_ChargeCount;
		
		UPDATE
			tblRecurringSeries
		SET
			rs_SeriesTotal=(SELECT Sum(rc_Amount) FROM tblRecurringCharge WITH (NOLOCK) WHERE rc_Series=@nSeries),
			rs_ChargeCount=rs_ChargeCount+1,
			rs_IntervalUnit=@nIntervalUnit,
			rs_IntervalLength=@nIntervalLength,
			rs_Paid=(SELECT 1-Sign(Count(ID)) FROM tblRecurringCharge WITH (NOLOCK) WHERE rc_Series=@nSeries AND rc_Attempts=0)
		WHERE
			ID=@nSeries;
		SET @i=@i+1;
	END
END

GO
