SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[RequestPhotocopyCHB](@nTransID int, @dtCHB datetime=NULL, @sComment nvarchar(500)='', @nReasonCode int=0)
AS
BEGIN
	SET NOCOUNT ON;
      IF NOT EXISTS(SELECT ID FROM tblCompanyTransPass WHERE ID=@nTransID)
      BEGIN
            SELECT -1;
            RETURN;
      END

      IF NOT EXISTS(SELECT ID FROM tblCompanyTransPass WHERE ID=@nTransID AND DeniedStatus=0)
      BEGIN
            SELECT -2;
            RETURN;
      END

      IF @dtCHB IS NULL SET @dtCHB=GetDate();

      DECLARE @nMerchant int, @nCurrency int, @nPaymentMethod int, @sBIN varchar(20), @sTerminalNumber varchar(20), @nFeeCHB money, @nFeeClarify money;
      SELECT
            @nMerchant=tblCompanyTransPass.CompanyID,
            @nCurrency=Currency,
            @nPaymentMethod=tblCompanyTransPass.PaymentMethod,
            @sBIN=IsNull(LTrim(RTrim(Str(CCard_First6))), 'XXXXXX'),
            @sTerminalNumber=TerminalNumber
      FROM
            tblCompanyTransPass
            LEFT JOIN tblCreditCard ON CreditCardID=tblCreditCard.ID
      WHERE
            tblCompanyTransPass.ID=@nTransID;
      IF (SELECT IsUsingNewTerminal FROM tblCompany WHERE ID=@nMerchant)=1
            --new terminals
            SELECT
                  @nFeeClarify=CCR_ClarificationFee,
                  @nFeeCHB=CCR_CBFixedFee
            FROM
                  tblCompanyCreditRestrictions
                  LEFT JOIN tblCompanyPaymentMethods ON CCR_CPM_ID = CPM_ID
            WHERE
                  CPM_CompanyID=@nMerchant
                  AND
                  CPM_CurrencyID=@nCurrency
                  AND
                  CPM_PaymentMethod=@nPaymentMethod
                  AND
                  CCR_TerminalNumber=@sTerminalNumber
      ELSE
            --old terminals
            SELECT TOP 1
                  @nFeeClarify=CCF_ClarificationFee,
                  @nFeeCHB=CCF_CBFixedFee
            FROM
                  tblCompanyCreditFees
            WHERE
                  CCF_CompanyID=@nMerchant
                  AND
                  CCF_CurrencyID=@nCurrency
                  AND
                  CCF_PaymentMethod=@nPaymentMethod
                  AND
                  (CCF_ListBINs LIKE '%'+@sBIN+'%' OR CCF_ListBINs='')
            ORDER BY
                  CCF_IsDisabled DESC,
                  Len(CCF_ListBINs) DESC;

	INSERT INTO [Trans].[TransHistory]
	(
	   [Description]
	   ,[InsertDate]
	   ,[IsSucceeded]
	   ,[Merchant_id]
	   ,[ReferenceNumber]
	   ,[TransPass_id]
	   ,[TransHistoryType_id]
	)
	VALUES
	(
       IsNull((SELECT Title FROM tblChargebackReason WHERE ReasonCode=@nReasonCode), ''),
        @dtCHB,
        1,
        @nMerchant,
        @nReasonCode,
        @nTransID,
        6
	);
	
      DECLARE @bIsPendingChargeback bit;
      SET @bIsPendingChargeback=IsNull((SELECT TOP 1 IsPendingChargeback FROM tblChargebackReason WHERE ReasonCode=@nReasonCode), 0);

      IF (SELECT payID FROM tblCompanyTransPass WHERE ID=@nTransID)=';0;'
      BEGIN
            --unsettled
            UPDATE
                  tblCompanyTransPass
            SET
                  DeniedStatus=3,
                  DeniedAdminComment=@sComment,
                  netpayFee_ClrfCharge=@nFeeClarify,
                  IsPendingChargeback=@bIsPendingChargeback,
            DeniedDate=@dtCHB
            WHERE
                  ID=@nTransID;
            SELECT @nTransID;
            RETURN;
      END
      ELSE
      BEGIN
            --settled
            UPDATE
                  tblCompanyTransPass
            SET
                  DeniedDate=@dtCHB,
                  DeniedStatus=11,
                  DeniedAdminComment=@sComment,
                  DeniedValDate=@dtCHB,
                  DebitFee=0
            WHERE
					ID=@nTransID;
            INSERT INTO
                  tblCompanyTransPass
                  (
                        companyID,
                        DebitCompanyID,
                        CustomerID,
                        FraudDetectionLog_id,
                        OriginalTransID,
                        PrimaryPayedID,
                        PayID,
                        InsertDate,
                        Amount,
                        Currency,
                        Payments,
                        CreditType,
                        IPAddress,
                        replyCode,
                        OrderNumber,
                        Interest,
                        Comment,
                        TerminalNumber,
                        ApprovalNumber,
                        DeniedDate,
                        DeniedStatus,
                        DeniedPrintDate,
                        DeniedSendDate,
                        DeniedAdminComment,
                        PD,
                        MerchantPD,
                        PaymentMethodID,
                        PaymentMethodDisplay,
                        isTestOnly,
                        referringUrl,
                        payerIdUsed,
                        netpayFee_transactionCharge,
                        netpayFee_ratioCharge,
                        DebitReferenceCode,
                        OCurrency,
                        OAmount,
                        netpayFee_chbCharge,
                        netpayFee_ClrfCharge,
                        PaymentMethod,
                        UnsettledAmount,
                        UnsettledInstallments,
                        IPCountry,
                        HandlingFee,
                        DebitFee,
                        RecurringSeries,
                        RecurringChargeNumber,
                        DeniedValDate,
                        MerchantRealPD,
                        IsPendingChargeback,
                        TransSource_id,
						CreditCardID ,
						TransPayerInfo_id ,
						TransPaymentMethod_id
                  )
            SELECT
                  companyID,
                  DebitCompanyID,
                  CustomerID,
                  0 FraudDetectionLog_id,
                  @nTransID OriginalTransID,
                  0 PrimaryPayedID,
                  ';0;' PayID,
                  GetDate() InsertDate,
                  0 Amount,
                  Currency,
                  Payments,
                  CASE CreditType WHEN 0 THEN 1 ELSE 0 END CreditType,
                  IPAddress,
                  replyCode,
                  OrderNumber,
                  Interest,
                  Comment,
                  TerminalNumber,
                  ApprovalNumber,
                  @dtCHB DeniedDate,
                  5 DeniedStatus,
                  DeniedPrintDate,
                  DeniedSendDate,
                  @sComment DeniedAdminComment,
                  0 PD,
                  0 MerchantPD,
                  PaymentMethodID,
                  PaymentMethodDisplay,
                  isTestOnly,
                  '' referringUrl,
                  payerIdUsed,
                  0 netpayFee_transactionCharge,
                  0 netpayFee_ratioCharge,
                  DebitReferenceCode,
                  OCurrency,
                  OAmount,
                  0 netpayFee_chbCharge,
                  @nFeeClarify netpayFee_ClrfCharge,
                  PaymentMethod,
                  0 UnsettledAmount,
                  1 UnsettledInstallments,
                  '--' IPCountry,
                  HandlingFee,
                  DebitFee,
                  RecurringSeries,
                  RecurringChargeNumber,
                  @dtCHB DeniedValDate,
                  NULL MerchantRealPD,
                  @bIsPendingChargeback,
                  TransSource_id,
				  CreditCardID ,
				  TransPayerInfo_id ,
				  TransPaymentMethod_id
            FROM
                  tblCompanyTransPass
            WHERE
                  ID=@nTransID;
			SELECT @@IDENTITY;
            RETURN;
      END;
END


GO
