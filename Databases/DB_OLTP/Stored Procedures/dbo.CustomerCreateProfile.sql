SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CustomerCreateProfile]
--selects new customer ID and generated password (negative ID = error)
(
	@sFirstName nvarchar(200),
	@sLastName nvarchar(200),
	@sPhone nvarchar(50),
	@sCellPhone nvarchar(50),
	@sIDNumber nvarchar(20),
	@sStreet nvarchar(500),
	@sCity nvarchar(50),
	@sZip nvarchar(25),
	@nCountry int,
	@nState int,
	@sMail nvarchar(50),
	@sIpOnReg nvarchar(50),
	@sQuestion nvarchar(50),
	@sAnswer nvarchar(50),
	@nRefCompanyID int,
	@sRefCompanyMemberID nvarchar(50)='',
	@nWalletIdentityID int=0,
	@nID int OUTPUT,
	@sPassword nvarchar(100) OUTPUT,
	@sRefCompanyData nvarchar(100)='',
	@sDOB Date=null,
	@sECA nvarchar(50)='',
	@sECS nvarchar(200)=''
)
AS
BEGIN
	-- check validity
	IF @sFirstName='' OR @sLastName='' OR @sPhone=''
	BEGIN
		-- there is at least one empty 'personal' field
		SELECT @nID=-1, @sPassword='there is at least one empty mandatory field in <b>Personal Details</b> section.'
		RETURN
	END
	IF @sStreet='' OR @sCity='' OR @sZip='' OR @nCountry<=0
	BEGIN
		-- there is at least one empty 'address' field
		SELECT @nID=-2, @sPassword='there is at least one empty mandatory field in <b>Address</b> section.'
		RETURN
	END
	IF @sMail=''
	BEGIN
		-- there is at least one empty 'login' field
		SELECT @nID=-3, @sPassword='there is at least one empty mandatory field in <b>Login Details</b> section.'
		RETURN
	END
	-- prevent duplication
	IF EXISTS(SELECT ID FROM tblCustomer WHERE Mail=@sMail)
	BEGIN
		-- eMail already exists
		SELECT @nID=-4, @sPassword='the eMail address <b>'+@sMail+'</b> is already in use.'
		RETURN
	END
	INSERT INTO
		tblCustomer
		(
			FirstName,
			LastName,
			Phone,
			CellPhone,
			IDNumber,
			Street,
			City,
			Zip,
			Country,
			State,
			Mail,
			IpOnReg,
			Question1,
			Answer1,
			RefCompanyID,
			RefCompanyMemberID,
			WalletIdentityID,
			RefCompanyData,
			ECA,
			ECS,
			DOB
		)
	VALUES
		(
			@sFirstName,
			@sLastName,
			@sPhone,
			@sCellPhone,
			@sIDNumber,
			@sStreet,
			@sCity,
			@sZip,
			@nCountry,
			@nState,
			@sMail,
			@sIpOnReg,
			@sQuestion,
			@sAnswer,
			@nRefCompanyID,
			@sRefCompanyMemberID,
			@nWalletIdentityID,
			@sRefCompanyData,
			@sECA,
			@sECS,
			@sDOB
		)
	SELECT @nID=IsNull(ID, 0) FROM tblCustomer WHERE Mail=@sMail
	IF IsNull(@nID, 0)=0
	BEGIN
		-- insert failed
		SELECT @nID=-5, @sPassword='failed to save record in database.'
		RETURN
	END
	SET @sPassword=dbo.GetRandomPassword()
	INSERT INTO
		tblPasswordHistory (LPH_ID, LPH_RefID, LPH_RefType, LPH_IP, LPH_Password256)
	VALUES
		(1, @nID, 1, @sIpOnReg, dbo.GetEncrypted256(@sPassword))
END




GO
