SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[NotifyAccountingAboutUnpaidTransactionsBns](@sMailTo varchar(1000))
AS
BEGIN
	DECLARE @sSubject nvarchar(255), @sFile nvarchar(255), @sBody nvarchar(max);
	SET @sSubject='BNS Transactions Unpaid '+Convert(char(10), GetDate(), 120);
	SET @sFile='unpaid_bns_'+Convert(char(10), GetDate(), 120)+'.csv';
	SET @sBody='The following BNS transactions are still unpaid at '+Convert(char(19), GetDate(), 120);
	IF ISNULL(@sMailTo, '')='' SET @sMailTo='accounting@netpay-intl.com;techsupp@netpay-intl.com';
	EXEC msdb..sp_send_dbmail
		@recipients=@sMailTo,
		@subject=@sSubject,
		@body_format='TEXT',
		@body=@sBody,
		@query='SET NOCOUNT ON;SELECT * FROM viewBnsTransactionsWithoutEpa;SET NOCOUNT OFF;',
		@execute_query_database='netpay',
		@attach_query_result_as_file=1,
		@query_attachment_filename=@sFile,
		@query_result_separator='	',
		@query_result_width=1024,
		@exclude_query_output=1;
END
GO
