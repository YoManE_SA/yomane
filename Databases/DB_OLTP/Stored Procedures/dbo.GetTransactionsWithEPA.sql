SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetTransactionsWithEPA]
(
	@nWireMin int=null,
	@nWireMax int=null,
	@nWire int=null,
	@nSettlement int=NULL,
	@nMerchant int=NULL,
	@nTransaction int=NULL,
	@nDebitCompany int=NULL,
	@dtFrom datetime=NULL,
	@dtTo datetime=NULL,
	@bDebug bit=0
)
AS
BEGIN
	DECLARE @sSQL varchar(1000);
	CREATE TABLE #t
	(
		ID int,
		DebitRefCode varchar(50),
		Amount money,
		SettlementID int,
		WireID int,
		BankID int,
		Currency char(3),
		PaymentMethod varchar(100),
		OriginalTransID int,
		Installment int,
		DebitDate datetime,
		RefundDate datetime
	)
	SET @sSQL='INSERT INTO #t(ID, DebitRefCode, Amount, Currency, PaymentMethod, SettlementID, WireID, BankID, OriginalTransID,'
	+' DebitDate, RefundDate) SELECT'
	+' ID, DebitReferenceCode, Amount, Currency, PaymentMethod, PrimaryPayedID, WireMoney_id, DebitCompanyID, OriginalTransID,'
	+' InsertDate, CASE WHEN DeniedDate>InsertDate THEN DeniedDate ELSE InsertDate END'
	+' FROM tblCompanyTransPass WITH (NOLOCK) LEFT JOIN tblWireMoney WITH (NOLOCK) ON PrimaryPayedID=WireSourceTbl_id'
	+' WHERE DebitCompanyID>1 AND IsTestOnly=0'
	+CASE WHEN @nSettlement>0 THEN ' AND PrimaryPayedID='+LTrim(RTrim(Str(@nSettlement))) ELSE '' END
	+CASE WHEN @nWire>0 THEN ' AND WireMoney_id='+LTrim(RTrim(Str(@nWire))) ELSE '' END
	+CASE WHEN @nWireMin>0 THEN ' AND WireMoney_id>='+LTrim(RTrim(Str(@nWireMin))) ELSE '' END
	+CASE WHEN @nWireMax>0 THEN ' AND WireMoney_id<='+LTrim(RTrim(Str(@nWireMax))) ELSE '' END
	+CASE WHEN @nMerchant>0 THEN ' AND CompanyID='+LTrim(RTrim(Str(@nMerchant))) ELSE '' END
	+CASE WHEN @nTransaction>0 THEN ' AND ID='+LTrim(RTrim(Str(@nTransaction))) ELSE '' END
	+CASE WHEN @nDebitCompany>0 THEN ' AND DebitCompanyID='+LTrim(RTrim(Str(@nDebitCompany))) ELSE '' END
	+CASE WHEN @dtFrom IS NOT NULL THEN ' AND InsertDate>=Convert(datetime, '''+Convert(char(19), @dtFrom, 120)+''', 120)' ELSE '' END
	+CASE WHEN @dtTo IS NOT NULL THEN ' AND InsertDate<=Convert(datetime, '''+Convert(char(19), @dtTo, 120)+''', 120)' ELSE '' END;
	EXEC (@sSQL);
	SELECT
		#t.ID [ID],
		DebitRefCode [Debit Ref.],
		Amount,
		CUR_ISOName [Currency],
		pm.Name [Card Type],
		SettlementID [Settlement No.],
		WireID [Wire No.],
		dc_name [Bank],
		IsNull(IsPaid, 0) [Debited],
		IsNull(IsRefunded, 0) [Credited],
		CASE
			WHEN IsNull(IsRefunded, 0)=1 THEN Convert(varchar(10), RefundDate, 120)
			ELSE Convert(varchar(10), DebitDate, 120)
		END [Trans. Date]
	FROM
		#t
		LEFT JOIN tblLogImportEPA WITH (NOLOCK) ON #t.ID=TransID
		LEFT JOIN tblDebitCompany WITH (NOLOCK) ON BankID=DebitCompany_ID
		LEFT JOIN tblSystemCurrencies WITH (NOLOCK) ON Currency=CUR_ID
		LEFT JOIN List.PaymentMethod pm WITH (NOLOCK) ON PaymentMethod=pm.PaymentMethod_id
	ORDER BY
		[Wire No.], [ID];
END
GO
