SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE
	[Log].[spInsertHistory]
(
	@HistoryType_id		AS INT				= 99,
	@MerchantID			AS INT				= NULL,
	@Action				AS NVARCHAR(50)		= NULL,
	@ReplyCode			AS NVARCHAR(20)		= NULL,
	@Request			AS NVARCHAR(4000)	= NULL,
	@Response			AS NVARCHAR(4000)	= NULL,
	@SourceIdentity		AS INT				= NULL,
	@InsertUserName		AS NVARCHAR(50)		= NULL,
	@InsertIPAddress	AS NVARCHAR(50)		= NULL,
	@SystemComment		AS NVARCHAR(4000)	= NULL
)
AS

DECLARE	@VariableXML	AS XML;

BEGIN TRY

	SET @VariableXML = 
		N'<XML>
			<ActionType>'+ ISNULL(@Action,'') +'</ActionType>
			<ReplyCode>'+ ISNULL(CAST(@ReplyCode AS VARCHAR),'') +'</ReplyCode>
			<Request><![CDATA['+ REPLACE(ISNULL(@Request,''),']]>','') +']]></Request>
			<Response><![CDATA['+ REPLACE(ISNULL(@Response,''),']]>','') +']]></Response>
			<SystemComment><![CDATA['+ ISNULL(@SystemComment,'') +']]></SystemComment>
		</XML>';

	INSERT INTO [Log].[History]
	(
		HistoryType_id,
		Merchant_id,
		VariableXML,
		InsertUserName,
		InsertIPAddress,
		SourceIdentity
	)
	VALUES
	(
		@HistoryType_id,
		@MerchantID ,
		@VariableXML,
		@InsertUserName,
		@InsertIPAddress,
		@SourceIdentity
	);

END TRY
BEGIN CATCH	

	DECLARE	@MailBody AS NVARCHAR(MAX) = '';
	
	SET @MailBody +=	N'Error number is: ' + CAST (ERROR_NUMBER () AS NVARCHAR(MAX)) + '<br />' +
						N'Error message is: ' + CAST (ERROR_MESSAGE () AS NVARCHAR(MAX)) + '<br />' +
						N'Error severity is: ' + CAST (ERROR_SEVERITY () AS NVARCHAR(MAX)) + '<br />' +
						N'Error state is: ' + CAST (ERROR_STATE () AS NVARCHAR(MAX)) + '<br />' +
						N'Error procedure is: ' + CAST (ERROR_PROCEDURE () AS NVARCHAR(MAX)) + '<br />' +
						N'Error line is: ' + CAST (ERROR_LINE () AS NVARCHAR(MAX)) + '<br />';

	EXEC [dbo].[SendMail]
		@sTo = N'lavi@netpay-intl.com',
		@sSubject = N'SQL Server Error: [Log].[spInsertHistory]',
		@sBody = @MailBody

END CATCH;

GO
