SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE
	[Log].[spInsertLogCardStorage]
(
	@MerchantID		AS INT ,
	@Action			AS NVARCHAR(50) ,
	@ReplyCode		AS NVARCHAR(20) ,
	@Request		AS NVARCHAR(1500) ,
	@Response		AS NVARCHAR(1500)
)
AS

DECLARE @VariableXML AS XML = 
	N'
	<CardStorage>
		<ActionType>'+ @Action +'</ActionType>
		<ReplyCode>'+ CAST(@ReplyCode AS VARCHAR) +'</ReplyCode>
		<Request><![CDATA['+ @Request +']]></Request>
		<Response><![CDATA['+ @Response +']]></Response>
	</CardStorage>
	';
	
INSERT INTO [Log].[History]
(
	HistoryType_id,
	Merchant_id,
	VariableXML
)
VALUES
(
	1,
	@MerchantID ,
	@VariableXML
);

GO
