SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [Risk].[spInsertBlacklistItem]
    (
      @nMerchant INT ,
      @nType INT ,
      @sValue NVARCHAR(1000) ,
      @sComment NVARCHAR(50) = ''
    )
AS 
    BEGIN

        IF @nMerchant = 0 
            SET @nMerchant = NULL;

        --IF NOT EXISTS ( SELECT  BL_ID
        --                FROM    tblBLCommon
        --                WHERE   ISNULL(BL_CompanyID, 0) = ISNULL(@nMerchant,
        --                                                      NULL)
        --                        AND BL_Type = @nType
        --                        AND BL_Value = @sValue ) 
            INSERT  INTO tblBLCommon
                    ( BL_CompanyID ,
                      BL_BlockLevel ,
                      BL_Type ,
                      BL_Value ,
                      BL_Comment
                    )
            VALUES  ( @nMerchant ,
                      1 ,
                      @nType ,
                      @sValue ,
                      @sComment
                    );

    END

GO
