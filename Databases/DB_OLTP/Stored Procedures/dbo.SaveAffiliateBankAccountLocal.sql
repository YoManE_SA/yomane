SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SaveAffiliateBankAccountLocal]
(
	@nAffiliate int,
	@sPaymentType nvarchar(80),
	@sBeneficiary nvarchar(80),
	@sBranchNumber nvarchar(80),
	@sAccountNumber nvarchar(80),
	@nBank int
)
AS
BEGIN
	UPDATE
		tblAffiliates
	SET
		PaymentMethod=@sPaymentType,
		PaymentBank=CASE @nBank WHEN 0 THEN NULL ELSE @nBank END,
		PaymentBranch=@sBranchNumber,
		PaymentPayeeName=@sBeneficiary,
		PaymentAccount=@sAccountNumber
	WHERE
		affiliates_id=@nAffiliate
END
GO
