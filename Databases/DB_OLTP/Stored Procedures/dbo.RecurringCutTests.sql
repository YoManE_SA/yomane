SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RecurringCutTests](@nMerchant int)
AS
BEGIN
	DECLARE curSeries CURSOR FOR
		SELECT
			rc_Series
		FROM
			tblRecurringAttempt
			INNER JOIN tblRecurringCharge ON ra_Charge=tblRecurringCharge.ID
			INNER JOIN tblCompanyTransPass ON ra_TransPass=tblCompanyTransPass.ID
		WHERE
			tblCompanyTransPass.CompanyID=@nMerchant
			AND
			tblCompanyTransPass.IsTestOnly=1
	OPEN curSeries
	DECLARE @nSeries int
	FETCH NEXT FROM curSeries INTO @nSeries
	WHILE @@FETCH_STATUS=0
	BEGIN
		EXEC RecurringCutSeriesEx @nSeries;
		FETCH NEXT FROM curSeries INTO @nSeries
	END
	CLOSE curSeries
	DEALLOCATE curSeries
END
GO
