SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RecurringCreateSeriesEx]
(
	@bPass bit,
	@nTransaction int,
	@sRecurring varchar(20),
	@sComments nvarchar(200),
	@nTrmCode int,
	@sIP varchar(50)='',
	@sIdentifier varchar(50)=NULL,
	@nPrecreatedSeriesID int=NULL
)
AS
BEGIN
	IF @sRecurring LIKE '%A%' SET @sRecurring=Left(@sRecurring, CharIndex('A', @sRecurring)-1)
	DECLARE @nChargeCount int, @nIntervalUnit int, @nIntervalLength int, @sUnit char(1)
	IF @sRecurring LIKE '%D%' SET @sUnit='D'
	IF @sRecurring LIKE '%W%' SET @sUnit='W'
	IF @sRecurring LIKE '%M%' SET @sUnit='M'
	IF @sRecurring LIKE '%Q%' SET @sUnit='Q'
	IF @sRecurring LIKE '%Y%' SET @sUnit='Y'
	SET @nChargeCount=Cast(Left(@sRecurring, CharIndex(@sUnit, @sRecurring)-1) AS int)
	SET @nIntervalUnit=CASE @sUnit WHEN 'D' THEN 1 WHEN 'W' THEN 2 WHEN 'Y' THEN 4 ELSE 3 END
	SET @nIntervalLength=Right(@sRecurring, Len(@sRecurring)-CharIndex(@sUnit, @sRecurring))
	IF @sUnit='Q' SET @nIntervalLength=@nIntervalLength*3
	EXEC RecurringCreateSeries @bPass, @nTransaction, @nChargeCount, @nIntervalUnit, @nIntervalLength, @sComments, @nTrmCode, @sIP, @sIdentifier, @nPrecreatedSeriesID;
END
GO
