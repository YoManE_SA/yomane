SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RecurringCutSeriesEx](@nSeries int)
AS
BEGIN
	IF NOT EXISTS(SELECT ID FROM tblRecurringSeries WHERE ID=@nSeries AND rs_Deleted=0 AND rs_Paid=0)
	BEGIN
		SELECT -1
		RETURN
	END
	DELETE FROM tblRecurringCharge WHERE rc_Series=@nSeries AND rc_Paid=0 AND Cast(Floor(Cast(rc_Date AS real)) AS datetime)>=Cast(Floor(Cast(GetDate() AS real)) AS datetime);
	UPDATE
		tblRecurringSeries
	SET
		rs_ChargeCount=IsNull((SELECT Count(*) FROM tblRecurringCharge WHERE rc_Series=@nSeries), 0),
		rs_SeriesTotal=IsNull((SELECT Sum(rc_Amount) FROM tblRecurringCharge WHERE rc_Series=@nSeries), 0)
	WHERE
		ID=@nSeries;
	UPDATE
		tblRecurringCharge
	SET
		rc_ChargeNumber=rc_ChargeNumber+1+
			(SELECT Max(rc_ChargeNumber) FROM tblRecurringCharge WHERE rc_Series=@nSeries)
	WHERE
		rc_Series=@nSeries;
	UPDATE
		tblRecurringCharge
	SET
		rc_ChargeNumber=1+
			(
				SELECT
					Count(*)
				FROM
					tblRecurringCharge tblRecurringChargePrev
				WHERE
					rc_Series=@nSeries AND tblRecurringCharge.ID>ID
			)
	WHERE
		rc_Series=@nSeries;
END

GO
