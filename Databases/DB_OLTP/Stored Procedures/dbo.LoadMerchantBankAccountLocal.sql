SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoadMerchantBankAccountLocal](@nMerchant int)
AS
BEGIN
	SELECT
		PaymentMethod PaymentType,
		IsNull(PaymentBank, 0) Bank,
		PaymentBranch BranchNumber,
		PaymentPayeeName Beneficiary,
		PaymentAccount AccountNumber
	FROM
		tblCompany
	WHERE
		ID=@nMerchant
END
GO
