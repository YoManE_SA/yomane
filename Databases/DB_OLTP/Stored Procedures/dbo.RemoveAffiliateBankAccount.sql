SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RemoveAffiliateBankAccount](@nAffiliate int, @nCurrency int)
AS
DELETE FROM tblAffiliateBankAccount WHERE aba_Affiliate=@nAffiliate AND aba_Currency=@nCurrency;
GO
