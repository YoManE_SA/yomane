SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LogWriteTransactionEPA](@nTransID int, @nInstallment int=1, @bPaid bit=0, @bRefunded bit=0, @sAdminUser varchar(50)=NULL)
AS
BEGIN
	IF EXISTS(SELECT ID FROM tblLogImportEPA WHERE TransID=@nTransID AND Installment=@nInstallment)
		UPDATE
			tblLogImportEPA
		SET
			IsPaid=CASE @bPaid WHEN 1 THEN 1 ELSE IsPaid END,
			IsRefunded=CASE @bRefunded WHEN 1 THEN 1 ELSE IsRefunded END,
			PaidInsertDate=CASE @bPaid WHEN 1 THEN GetDate() ELSE PaidInsertDate END,
			RefundedInsertDate=CASE @bRefunded WHEN 1 THEN GetDate() ELSE RefundedInsertDate END,
			AdminUser=@sAdminUser
		WHERE
			TransID=@nTransID AND Installment=@nInstallment
	ELSE
		INSERT INTO tblLogImportEPA(TransID, Installment, IsPaid, IsRefunded, AdminUser) VALUES (@nTransID, @nInstallment, IsNull(@bPaid, 0), IsNull(@bRefunded, 0), @sAdminUser);
END
GO
