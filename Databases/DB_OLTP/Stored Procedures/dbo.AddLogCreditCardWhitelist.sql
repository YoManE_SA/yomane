SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[AddLogCreditCardWhitelist]
(
	@nID int,
	@nLevel tinyint,
	@nMerchant int,
	@nTransaction int,
	@nTransactionType int=0 -- 1 Pass, 2 Fail, 3 Auth, 4 Pending
)
AS
BEGIN
	IF @nMerchant IS NULL OR @nLevel IS NULL OR @nID IS NULL OR @nTransaction IS NULL RETURN -1;
	IF NOT EXISTS (SELECT GD_ID FROM GetGlobalData(39) WHERE GD_ID=@nLevel) RETURN -6;
	IF NOT EXISTS (SELECT GD_ID FROM GetGlobalData(37) WHERE GD_ID=@nTransactionType) RETURN -6;
	IF NOT EXISTS (SELECT ID FROM tblCompany WITH (NOLOCK) WHERE ID=@nMerchant AND ActiveStatus=30 AND IsCcWhitelistEnabled=1) RETURN -7;

	IF @nTransactionType=1 IF NOT EXISTS (SELECT ID FROM tblCompanyTransPass WITH (NOLOCK) WHERE ID=@nTransaction) RETURN -10;
	IF @nTransactionType=2 IF NOT EXISTS (SELECT ID FROM tblCompanyTransFail WITH (NOLOCK) WHERE ID=@nTransaction) RETURN -10;
	IF @nTransactionType=3 IF NOT EXISTS (SELECT ID FROM tblCompanyTransApproval WITH (NOLOCK) WHERE ID=@nTransaction) RETURN -10;
	IF @nTransactionType=4 IF NOT EXISTS (SELECT companyTransPending_id FROM tblCompanyTransPending WITH (NOLOCK) WHERE companyTransPending_id=@nTransaction) RETURN -10;
	
	INSERT INTO tblLogCreditCardWhitelist
	(
		lccw_ccwlID,
		lccw_Level,
		lccw_Merchant,
		lccw_Transaction,
		lccw_TransactionType
	)
	VALUES
	(
		@nID,
		@nLevel,
		@nMerchant,
		@nTransaction,
		@nTransactionType
	);
	RETURN @@IDENTITY;
END
GO
