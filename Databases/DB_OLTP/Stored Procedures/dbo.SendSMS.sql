SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SendSMS](@sTo nvarchar(200), @sBody nvarchar(max))
AS
BEGIN
	DECLARE @n int, @sTemp nvarchar(200)
	SET @sTo=Replace(@sTo+';', ';;', ';')
	WHILE @sTo<>''
	BEGIN
		SET @n=CharIndex(';', @sTo)
		SET @sTemp=CASE @n WHEN 0 THEN @sTo ELSE Left(@sTo, @n-1) END
		SET @sTo=Replace(@sTo, @sTemp+';', '')
		IF @sTemp NOT LIKE '05[1234567890][1234567890][1234567890][1234567890][1234567890][1234567890][1234567890][1234567890]'
			SET @sTemp=IsNull((SELECT TOP 1 su_SMS FROM tblSecurityUser WHERE su_Username=@sTemp), '')
		IF @sTemp<>'' EXEC SendMessageSMS @sTemp, @sBody
	END
END
GO
