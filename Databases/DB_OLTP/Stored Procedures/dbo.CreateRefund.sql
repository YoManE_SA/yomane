SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[CreateRefund](@nTransID INT, @sIP VARCHAR(20), @nAmount MONEY = NULL, @sComment NVARCHAR(300) = NULL, @nRefundType TINYINT=0)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nTransAmount money, @nDeniedStatus int;

	SELECT @nTransAmount = Amount, @nDeniedStatus = DeniedStatus FROM tblCompanyTransPass WHERE ID = @nTransID AND CreditType > 0;
	IF IsNull(@nTransAmount, 0)<=0
	BEGIN
		SELECT -1;
		RETURN;
	END

	IF @nAmount IS NULL SET @nAmount = @nTransAmount;
	IF @nTransAmount < @nAmount
	BEGIN
		SELECT -2;
		RETURN;
	END

	DECLARE @nRefundCharge money;
	SET @nRefundCharge=
		IsNull
		(
			(
                SELECT TOP 1
                        CASE @nRefundType
                          WHEN 0 THEN CCF_RefundFixedFee
                          WHEN 1 THEN CCF_PartialRefundFixedFee
                          WHEN 2 THEN CCF_FraudRefundFixedFee
                        END
                FROM    tblCompanyCreditFees f
                        INNER JOIN tblCompanyTransPass t ON f.CCF_CompanyID = t.CompanyID
                WHERE   t.ID = @nTransID
                ORDER BY SIGN(ABS(CCF_CurrencyID - t.Currency)) ,
                        SIGN(ABS(CAST(CCF_PaymentMethod AS INT)
                                 - t.PaymentMethod)) ,
                        CCF_IsDisabled
			),
			0
		);

    INSERT  INTO tblCompanyTransPass
            ( TransSource_id ,
              companyID ,
              CustomerID ,
              InsertDate ,
              DeniedStatus ,
              IPAddress ,
              Amount ,
              Currency ,
              Payments ,
              CreditType ,
              replyCode ,
              Interest ,
              Comment ,
              DebitCompanyID ,
              TerminalNumber ,
              ApprovalNumber ,
              OriginalTransID ,
              paymentMethodId ,
              paymentMethodDisplay ,
              paymentMethod ,
              netpayFee_transactionCharge ,
              UnsettledInstallments ,
              UnsettledAmount ,
              CreditCardID ,
              TransPayerInfo_id ,
              TransPaymentMethod_id ,
			  IsFraudByAcquirer ,
			  [OrderNumber]
	        )
            SELECT  16 ,
                    CompanyID ,
                    CustomerID ,
                    GETDATE() ,
                    0 ,
                    @sIP ,
                    @nAmount ,
                    Currency ,
                    1 ,
                    0 ,
                    replyCode ,
                    Interest ,
                    @sComment ,
                    DebitCompanyID ,
                    TerminalNumber ,
                    ApprovalNumber ,
                    @nTransID ,
                    PaymentMethodID ,
                    paymentMethodDisplay ,
                    paymentMethod ,
                    @nRefundCharge ,
                    1 ,
                    -@nAmount ,
                    CreditCardID ,
                    TransPayerInfo_id ,
                    TransPaymentMethod_id ,
                    CASE @nRefundType
                      WHEN 2 THEN 1
                      ELSE 0
                    END ,
					[OrderNumber]
            FROM    tblCompanyTransPass
            WHERE   ID = @nTransID;
		
    IF @nDeniedStatus IN ( 3, 11 ) 
        UPDATE  tblCompanyTransPass
        SET     DeniedStatus = CASE DeniedStatus
                                 WHEN 3 THEN 9
                                 ELSE 12
                               END
        WHERE   ID = @nTransID;

    SET NOCOUNT OFF;

    SELECT  MAX(ID)
    FROM    tblCompanyTransPass
    WHERE   OriginalTransID = @nTransID;
END

GO
