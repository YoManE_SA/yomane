SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCleanupCompanyTransFail]
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @dtInsertDate datetime;
	SET @dtInsertDate = DATEADD(mm, -6, getdate());
	DECLARE curArchiver CURSOR FOR
		SELECT ID FROM tblCompanyTransFail WHERE InsertDate<@dtInsertDate AND ID NOT IN (SELECT ra_TransFail FROM tblRecurringAttempt);
	DECLARE @nID int;
	OPEN curArchiver;
	FETCH NEXT FROM curArchiver INTO @nID;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM tblCompanyTransFail WHERE ID = @nID;
		WAITFOR DELAY '00:00:00.100';
		FETCH NEXT FROM curArchiver INTO @nID;
	END
	CLOSE curArchiver;
	DEALLOCATE curArchiver;
	SET NOCOUNT OFF
END
GO
