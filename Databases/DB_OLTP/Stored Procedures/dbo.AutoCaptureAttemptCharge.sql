SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AutoCaptureAttemptCharge](@nAuth int, @nDebit int OUTPUT, @bSuccess bit OUTPUT)
AS
BEGIN
	IF NOT EXISTS(SELECT ID FROM tblCompanyTransApproval WHERE ID=@nAuth) RETURN 0;
	DECLARE @sRemoteChargeUrl varchar(1000);
	SELECT
		@sRemoteChargeUrl=pc_RecurringUrl
	FROM
		tblParentCompany WITH (NOLOCK)
		INNER JOIN tblCompany ON tblParentCompany.ID=tblCompany.ParentCompany
	WHERE
		tblCompany.ID=(SELECT CompanyID FROM tblCompanyTransApproval WHERE ID=@nAuth);
	IF ISNULL(@sRemoteChargeUrl, '')='' RETURN -1;
	DECLARE @sRemoteChargeString nvarchar(1000);
	SET @sRemoteChargeString=dbo.AutoCaptureGetChargeString(@nAuth);
	IF ISNULL(@sRemoteChargeString, '')='' RETURN -2;
	DECLARE @sReply nvarchar(2000);
	SET @sReply=dbo.SendRequestHTTP(@sRemoteChargeUrl, @sRemoteChargeString);
	If IsNull(@sReply, '')='' RETURN -3;
	DECLARE @sReplyCode nvarchar(50);
	SET @sReplyCode=dbo.GetQueryStringValue('reply', @sReply);
	IF IsNull(@sReplyCode, '')='' RETURN -4;
	DECLARE @sDebitID nvarchar(10);
	SET @sDebitID=dbo.GetQueryStringValue('TransID', @sReply);
	If IsNull(@sDebitID, '')='' RETURN -5;
	BEGIN TRY
		SET @nDebit=CAST(@sDebitID AS int);
	END TRY
	BEGIN CATCH
		RETURN -6;
	END CATCH
	SET @bSuccess=CASE @sReplyCode WHEN '000' THEN 1 ELSE 0 END;
	IF @sReplyCode IN ('520', '521') RETURN -7;
	RETURN 0;
END
GO
