SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SaveWireBankAccount]
(
	@nWire int,
	@nAccountType int,
	@sAccountName nvarchar(80),
	@sAccountNumber nvarchar(80),
	@sBankName nvarchar(80),
	@sBankAddress nvarchar(200),
	@sBankAddressSecond nvarchar(100),
	@sBankAddressCity nvarchar(30),
	@sBankAddressState nvarchar(20),
	@nBankAddressCountry int,
	@sSwiftNumber nvarchar(80),
	@sIBAN nvarchar(80),
	@sABA nvarchar(80),
	@sSortCode nvarchar(80),
	@sBankAddressZip nvarchar(20),
	@sSepaBic nvarchar(11),
	@nMerchant int=NULL
)
AS
BEGIN
	DECLARE @s2 varchar(1), @sSQL nvarchar(4000)
	SET @s2=CASE @nAccountType WHEN 2 THEN '2' ELSE '' END;
	SET @sSQL='UPDATE tblWireMoney SET wirePaymentAbroadAccountName'+@s2+'='''+@sAccountName+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadAccountNumber'+@s2+'='''+@sAccountNumber+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankName'+@s2+'='''+@sBankName+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddress'+@s2+'='''+@sBankAddress+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddressSecond'+@s2+'='''+@sBankAddressSecond+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddressCity'+@s2+'='''+@sBankAddressCity+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddressState'+@s2+'='''+@sBankAddressState+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddressCountry'+@s2+'='+Str(@nBankAddressCountry);
	SET @sSQL=@sSQL+', wirePaymentAbroadSwiftNumber'+@s2+'='''+@sSwiftNumber+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadIBAN'+@s2+'='''+@sIBAN+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadABA'+@s2+'='''+@sABA+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadSortCode'+@s2+'='''+@sSortCode+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadBankAddressZip'+@s2+'='''+@sBankAddressZip+'''';
	SET @sSQL=@sSQL+', wirePaymentAbroadSepaBic'+@s2+'='''+@sSepaBic+'''';
	SET @sSQL=@sSQL+' WHERE WireMoney_ID='+Str(@nWire);
	IF @nMerchant IS NOT NULL SET @sSQL=@sSQL+' AND Company_ID='+Str(@nMerchant);
	EXEC sp_executesql @sSQL;
END
GO
