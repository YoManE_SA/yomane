SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[InsertInstallments](@nTransID int, @MerchantPD Datetime) AS
BEGIN
	DECLARE @nPayments int, @nCompany int, @nPos int, @nComment NVarChar(10), @nFullAmount Money, @nPay int
	SELECT @nCompany=CompanyID, @nFullAmount=Amount, @nPayments=Payments FROM tblCOmpanyTransPass WHERE ID=@nTransID
	SET @nPay = Floor(@nFullAmount / @nPayments);
	SET @nPos = 1
	SET @nComment = CAST(@nPos As NVarChar) + '/' + CAST(@nPayments As NVarChar);
	INSERT INTO tblCompanyTransInstallments(transAnsId, amount, CompanyID, InsID, MerchantPD, comment) 
	VALUES(@nTransID, @nFullAmount - (@nPay * (@nPayments - 1)), @nCompany, @nPos, DateAdd(m, @nPos - 1, @MerchantPD), @nComment)
	While(@nPayments > @nPos) BEGIN
		SET @nPos = @nPos + 1
		SET @nComment = CAST(@nPos As NVarChar) + '/' + CAST(@nPayments As NVarChar)
		INSERT INTO tblCompanyTransInstallments(transAnsId, amount, CompanyID, InsID, MerchantPD, comment) 
		VALUES(@nTransID, @nPay, @nCompany, @nPos, DateAdd(m, @nPos - 1, @MerchantPD), @nComment)
	END
END
GO
