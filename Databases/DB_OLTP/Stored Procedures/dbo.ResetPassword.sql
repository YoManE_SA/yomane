SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ResetPassword](@nRefType int, @nRefID int)
AS
BEGIN
	UPDATE tblPasswordHistory SET LPH_FailCount=0, LPH_LastSuccess=GetDate() WHERE LPH_ID=1 AND LPH_RefType=@nRefType AND LPH_RefID=@nRefID;
	RETURN @@ROWCOUNT;
END
GO
