SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ChargePeriodicFees]
AS
BEGIN
	DECLARE @nMerchant int, @nType int, @sType nvarchar(50), @nAmount money, @nCurrency int, @nSource int, @sSQL nvarchar(200), @sDisplay nvarchar(50), @bAnnual bit, @sComment nvarchar(1000);
	DECLARE @nFeeLimit money, @nBehavior int, @nDifference money, @nAmountType int, @dtStart datetime, @dtEnd datetime;
	DECLARE curFee CURSOR LOCAL FOR
		SELECT
			f.TypeID,
			t.IsAnnual,
			f.MerchantID,
			t.Name,
			t.Amount,
			--t.Amount/cm.CUR_BaseRate*cf.CUR_BaseRate, (Removed, no need to convert)
			t.CurrencyID,
			CASE t.IsAnnual
				WHEN 1 THEN 'Annual Fee '+LTrim(RTrim(Str(Year(f.NextDate))))
				ELSE 'Monthly Fee '+Right('0'+LTrim(RTrim(Str(Month(f.NextDate)))), 2)+' '+LTrim(RTrim(Str(Year(f.NextDate))))
			END,
			CASE t.IsAnnual
				WHEN 1 THEN 30
				ELSE 31
			END,
			t.Name,
			t.FeeLimit,
			t.Behavior,
			CASE t.IsAnnual
				WHEN 1 THEN 1030
				ELSE 1031
			END
		FROM
			tblPeriodicFeeType t
			INNER JOIN tblSystemCurrencies cf ON t.CurrencyID=cf.CUR_ID
			INNER JOIN tblPeriodicFee f ON t.ID=f.TypeID
			INNER JOIN tblCompany m ON f.MerchantID=m.ID
			INNER JOIN tblSystemCurrencies cm ON m.PaymentReceiveCurrency=cm.CUR_ID
 		WHERE
			t.IsActive=1 AND f.IsActive=1 AND m.ActiveStatus=30 AND f.NextDate<=GETDATE();
	OPEN curFee;
	FETCH NEXT FROM curFee INTO @nType, @bAnnual, @nMerchant, @sType, @nAmount, @nCurrency, @sDisplay, @nSource, @sComment, @nFeeLimit, @nBehavior, @nAmountType;
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @sComment=@sComment+' - '+@sDisplay;
		IF ISNULL(@nFeeLimit, 0)>0
		BEGIN
			SET @dtEnd=CONVERT(datetime, LTrim(RTrim(Str(Year(GetDate()))))+'-'+LTrim(RTrim(Str(Month(GetDate()))))+'-01', 120);
			SET @dtStart=CASE @bAnnual WHEN 1 THEN DATEADD(YEAR, -1, @dtEnd) ELSE DATEADD(MONTH, -1, @dtEnd) END;
			IF @nBehavior BETWEEN 0 AND 2 -- Fee-dependent
			BEGIN
				SELECT
					@nDifference=@nFeeLimit-SUM(a.Amount) --SUM(a.Amount/cd.CUR_BaseRate*ca.CUR_BaseRate)
				FROM
					tblTransactionAmount a WITH (NOLOCK)
					INNER JOIN [List].[TransAmountTypeToGroup] gt WITH (NOLOCK) ON a.TypeID = gt.TransAmountType_id
					INNER JOIN tblSystemCurrencies ca WITH (NOLOCK) ON a.Currency = ca.CUR_ID
					INNER JOIN tblSystemCurrencies cd WITH (NOLOCK) ON @nCurrency = cd.CUR_ID
				WHERE
					gt.TransAmountTypeGroup_id=1001 AND a.TypeID NOT IN (1030, 1031) AND a.InsertDate>=@dtStart AND a.InsertDate<@dtEnd AND a.MerchantID=@nMerchant;
				IF @nDifference>0
					SET @nAmount=CASE @nBehavior WHEN 1 THEN @nDifference WHEN 2 THEN @nDifference/@nFeeLimit*@nAmount ELSE @nAmount END
				ELSE
					SET @nAmount=0;
			END
			ELSE -- Volume-dependent
			BEGIN
				SELECT
					@nDifference=@nFeeLimit-SUM(t.Amount*CASE t.CreditType WHEN 0 THEN -1 ELSE 1 END) --/cd.CUR_BaseRate*ca.CUR_BaseRate
				FROM
					tblCompanyTransPass t WITH (NOLOCK)
					INNER JOIN tblSystemCurrencies ca WITH (NOLOCK) ON t.Currency=ca.CUR_ID
					INNER JOIN tblSystemCurrencies cd WITH (NOLOCK) ON @nCurrency=cd.CUR_ID
				WHERE
					t.InsertDate>=@dtStart AND t.InsertDate<@dtEnd AND t.companyID=@nMerchant AND t.PaymentMethod>=20;
				IF @nDifference>0
					SET @nAmount=CASE @nBehavior WHEN 1 THEN @nDifference WHEN 2 THEN @nDifference/@nFeeLimit*@nAmount ELSE @nAmount END
				ELSE
					SET @nAmount=0;
			END;
		END;
		EXEC CreateFeeTransaction @nMerchant, @nAmount, @nCurrency, 5, @nSource, @sDisplay, @sComment;
		UPDATE
			tblPeriodicFee
		SET
			NextDate=CASE @bAnnual WHEN 1 THEN DATEADD(YEAR, 1, NextDate) ELSE DATEADD(MONTH, 1, NextDate) END
		WHERE
			MerchantID=@nMerchant AND TypeID=@nType;
		FETCH NEXT FROM curFee INTO @nType, @bAnnual, @nMerchant, @sType, @nAmount, @nCurrency, @sDisplay, @nSource, @sComment, @nFeeLimit, @nBehavior, @nAmountType;
	END
	CLOSE curFee;
	DEALLOCATE curFee;
END


GO
