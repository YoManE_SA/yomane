SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[InvoiceDocumentRebuild]
(
	@nID int,
	@bLinesReduceVAT bit
)
AS
BEGIN
	DECLARE @bIsPrinted bit
	SELECT @bIsPrinted=id_IsPrinted FROM tblInvoiceDocument WHERE ID=@nID
	IF @bIsPrinted IS NULL
	BEGIN
		-- document not found
		SELECT -111
		RETURN
	END
	IF @bIsPrinted=1
	BEGIN
		-- document is already printed
		SELECT -222
		RETURN
	END
	IF @bLinesReduceVAT=1
		UPDATE
			tblInvoiceLine
		SET
			il_Price=il_Price/(1+id_VATPercent)
		FROM
			tblInvoiceLine
			INNER JOIN tblInvoiceDocument ON tblInvoiceLine.il_DocumentID=tblInvoiceDocument.ID
		WHERE
			tblInvoiceDocument.ID=@nID;
	UPDATE
		tblInvoiceDocument
	SET
		id_Lines=IsNull((SELECT Count(ID) FROM tblInvoiceLine WHERE il_DocumentID=@nID), 0),
		id_TotalLines=IsNull((SELECT Sum(il_Amount*il_CurrencyRate) FROM tblInvoiceLine WHERE il_DocumentID=@nID), 0)/id_CurrencyRate
	WHERE
		ID=@nID;
	UPDATE
		tblInvoiceDocument
	SET
		id_TotalVAT=id_TotalLines*(CASE id_ApplyVAT WHEN 1 THEN id_VATPercent ELSE 0 END)
	WHERE
		ID=@nID;
	UPDATE
		tblInvoiceDocument
	SET
		id_TotalDocument=id_TotalLines+id_TotalVAT
	WHERE
		ID=@nID;
	-- succeeded
	SELECT @nID
	RETURN
END
GO
