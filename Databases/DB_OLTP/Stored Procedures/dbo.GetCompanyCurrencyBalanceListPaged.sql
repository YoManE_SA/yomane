SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[GetCompanyCurrencyBalanceListPaged]
(
	@nCompanyID int,
	@nCurrencyID tinyint,
	@nSkip int,
	@nTake int
)
AS
BEGIN
	SET NOCOUNT ON;
		select
		*
		from
		(	
			SELECT  companyBalance_id, sourceTbl_id, sourceType, sourceInfo, insertDate, amount, currency,
			BalanceExpected balance, status, comment, paymentType,
			ROW_NUMBER() OVER (PARTITION BY Currency ORDER BY companyBalance_id DESC) RowNumber		   
			FROM tblCompanyBalance
			left join tblCompanyMakePaymentsRequests on  tblCompanyBalance.sourceTbl_id = tblCompanyMakePaymentsRequests.CompanyMakePaymentsRequests_id
			WHERE 
			tblCompanyBalance.company_id = @nCompanyID AND currency = @nCurrencyID 
		)q
		where
		RowNumber BETWEEN @nSkip+1 AND @nSkip+@nTake
		order by companyBalance_id DESC
		
	SET NOCOUNT OFF
END
GO
