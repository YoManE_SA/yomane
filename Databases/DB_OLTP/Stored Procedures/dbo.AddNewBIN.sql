SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AddNewBIN](@nBIN int, @nPaymentMethod int, @sCountryIso char(2)='--')
AS
BEGIN
	DECLARE @nID int;
	SELECT @nID=BINID FROM tblCreditCardBIN WHERE BINNumber=@nBIN;
	IF @nID>0 RETURN @nID;
	INSERT INTO tblCreditCardBIN
	(
		BIN,
		isoCode,
		PaymentMethod,
		CCName,
		CCType
	)
	SELECT
		LTrim(RTrim(STR(@nBIN))),
		@sCountryIso,
		@nPaymentMethod,
		Abbreviation,
		@nPaymentMethod-20
	FROM
		List.PaymentMethod
	WHERE
		PaymentMethod_id=@nPaymentMethod;
END

GO
