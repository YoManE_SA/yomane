SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ResetDebitTerminalMonthlyLimits]
AS
UPDATE
	tblDebitTerminals
SET
	dt_MonthlyCHBWasSent=0,
	dt_MonthlyMCCHBWasSent=0,
	dt_MonthlyCHB=
	(
		SELECT
			Count(*)
		FROM
			tblCompanyTransPass
		WHERE
			tblCompanyTransPass.TerminalNumber=tblDebitTerminals.TerminalNumber
			AND
			DebitCompanyID=DebitCompany
			AND
			dbo.IsCHB(DeniedStatus, IsTestOnly)=1
			AND
			IsTestOnly=0
			AND
			PaymentMethod=22
			AND
			CASE WHEN InsertDate>DeniedDate THEN InsertDate ELSE DeniedDate END>=Convert(datetime, LTrim(RTrim(Str(Year(GetDate()))))+'-'+LTrim(RTrim(Str(Month(GetDate()))))+'-01 00:00:00', 120)
	),
	dt_MonthlyMCCHB=
	(
		SELECT
			Count(*)
		FROM
			tblCompanyTransPass
		WHERE
			tblCompanyTransPass.TerminalNumber=tblDebitTerminals.TerminalNumber
			AND
			DebitCompanyID=DebitCompany
			AND
			dbo.IsCHB(DeniedStatus, IsTestOnly)=1
			AND
			IsTestOnly=0
			AND
			PaymentMethod=25
			AND
			CASE WHEN InsertDate>DeniedDate THEN InsertDate ELSE DeniedDate END>=Convert(datetime, LTrim(RTrim(Str(Year(GetDate()))))+'-'+LTrim(RTrim(Str(Month(GetDate()))))+'-01 00:00:00', 120)
	)
GO
