SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[RecurringAttemptCharge](@nCharge AS int, @nReply AS int OUTPUT, @sReply AS nvarchar(1000) OUTPUT)
AS
BEGIN
	DECLARE @nCompany AS int, @nCreditCard AS int, @nECheck AS int, @nSeries int, @nChargeNumber int, @bApproval bit
	SELECT
		@nCompany=rs_Company,
		@nCreditCard=rc_CreditCard,
		@nECheck=rc_ECheck,
		@nSeries=rc_Series,
		@nChargeNumber=rc_ChargeNumber,
		@bApproval=rs_Approval
	FROM
		viewRecurringActiveCharges
	WHERE
		ID=@nCharge AND rc_Date<=Cast(Cast(GetDate() AS int) AS datetime)
	Order BY ID asc
	IF @nCompany IS NULL
	BEGIN
		-- charge not found or not active or is in future
		SET @nReply=-1
		SET @sReply='The requested charge not found. It may be either stopped or blocked, or it is a future charge.'
		RETURN
	END
	IF NOT EXISTS (SELECT ID FROM tblCompany WHERE IsAllowRecurring=1 AND Blocked=0 AND Closed=0 AND ID=@nCompany)
	BEGIN
		-- merchant not found or not active or not allowed to make recurring charges
		SET @nReply=-2
		SET @sReply='The requested merchant not found. The merchant may be either closed or blocked, or there is no permission to make recurring charges.'
	END
	DECLARE @sRemoteChargeString AS nvarchar(2000), @sRemoteChargeURL AS nvarchar(200)
	IF @nCreditCard>0
	BEGIN
		-- validate credit card
		SELECT
			@sRemoteChargeURL=pc_RecurringURL
		FROM
			tblParentCompany
			INNER JOIN tblCompany ON tblParentCompany.ID=tblCompany.ParentCompany
		WHERE
			tblCompany.ID=@nCompany
		IF IsNull(@sRemoteChargeURL, '')=''
		BEGIN
			-- parent company does not provide URL for recurring remote charges of credit cards
			SET @nReply=-3
			SET @sReply='The parent company prohibits recurring remote charge of credit cards.'
		END
		SET @sRemoteChargeString=dbo.RecurringGetChargeString(@nCharge)
		IF IsNull(@sRemoteChargeString, '')=''
		BEGIN
			-- cannot build QueryString
			SET @nReply=-4
			SET @sReply='Request cannot be formed for remote charge. Important credit card information is missing or invalid.'
		END
	END
	ELSE
	BEGIN
		-- validate echeck
		SELECT
			@sRemoteChargeURL=pc_RecurringECheckURL
		FROM
			tblParentCompany
			INNER JOIN tblCompany ON tblParentCompany.ID=tblCompany.ParentCompany
		WHERE
			tblCompany.ID=@nCompany
		IF IsNull(@sRemoteChargeURL, '')=''
		BEGIN
			-- parent company does not provide URL for recurring remote charges of echecks
			SET @nReply=-5
			SET @sReply='The parent company prohibits recurring remote charge of echecks.'
		END
		SET @sRemoteChargeString=dbo.RecurringGetChargeStringEcheck(@nCharge)
		IF IsNull(@sRemoteChargeString, '')=''
		BEGIN
			-- cannot build QueryString
			SET @nReply=-6
			SET @sReply='Request cannot be formed for remote charge. Important echeck information is missing or invalid.'
		END
	END
	-- data OK, attempt to charge now
	EXEC RecurringSendRequest @sRemoteChargeURL, @sRemoteChargeString, @nReply OUTPUT, @sReply OUTPUT
	IF @nReply<>0
	BEGIN
		-- charge failed
		SET @sReply='Failed to send request for remote charge to '+@sRemoteChargeURL+'. Reply code '+LTrim(RTrim(Str(@nReply)))
		SET @nReply=-7
	END
	-- response received from remote charge
	DECLARE @sResponseReply AS nvarchar(1000), @sTransID AS nvarchar(1000)
	SET @sResponseReply=dbo.GetQueryStringValue('Reply', @sReply)
	IF @sResponseReply='' SET @sResponseReply=dbo.GetQueryStringValue('replyCode', @sReply)
	SET @sTransID = dbo.GetQueryStringValue('TransID', @sReply)
	IF @sTransID = '' OR @sTransID = 0 SET @sTransID = NULL

    --get notification target
	DECLARE @sRecurringReplyUrl AS nvarchar(1000)
	SELECT @sRecurringReplyUrl = recurringReplyUrl FROM tblCompanyChargeAdmin WHERE Company_ID=@nCompany And isRecurringReply = 1
	SET @sRecurringReplyUrl = 'RecurringCharge=' + CAST(@nCharge As NVarChar) + '&Url=' + dbo.UrlEncode(@sRecurringReplyUrl) -- + '&' + @sReply
	
	
	IF @sResponseReply='000'
	BEGIN
		-- transaction passed
		IF @bApproval=1
		BEGIN
			-- pre-auth.
			INSERT INTO tblRecurringAttempt
			(
				ra_Charge, ra_CreditCard, ra_ECheck, TransPaymentMethod_id, ra_ReplyCode, ra_TransApproval, ra_Comments
			)
			SELECT
				ID, rc_CreditCard, rc_ECheck, TransPaymentMethod_id, @sResponseReply, @sTransID, dbo.GetQueryStringValue('ReplyDesc', @sReply)
			FROM
				tblRecurringCharge
			WHERE
				ID=@nCharge;
			UPDATE tblCompanyTransApproval SET RecurringSeries=@nSeries, RecurringChargeNumber=@nChargeNumber WHERE ID=CAST(@sTransID AS int)
	    	IF @sRecurringReplyUrl Is Not Null Insert Into [dbo].[EventPending](TransPreAuth_id, Parameters, EventPendingType_id, TryCount)Values(@sTransID, @sRecurringReplyUrl, 201, 3)
		END
		ELSE
		BEGIN
			-- charge
			INSERT INTO tblRecurringAttempt
			(
				ra_Charge, ra_CreditCard, ra_ECheck, TransPaymentMethod_id, ra_ReplyCode, ra_TransPass, ra_Comments
			)
			SELECT
				ID, rc_CreditCard, rc_ECheck, TransPaymentMethod_id, @sResponseReply, @sTransID, dbo.GetQueryStringValue('ReplyDesc', @sReply)
			FROM
				tblRecurringCharge
			WHERE
				ID=@nCharge;
			UPDATE tblCompanyTransPass SET RecurringSeries=@nSeries, RecurringChargeNumber=@nChargeNumber WHERE ID=CAST(@sTransID AS int);
	    	IF @sRecurringReplyUrl Is Not Null Insert Into [dbo].[EventPending](TransPass_id, Parameters, EventPendingType_id, TryCount)Values(@sTransID, @sRecurringReplyUrl, 201, 3)
		END
	END
	ELSE
	BEGIN
		IF @sResponseReply='001'
		BEGIN
			-- transaction pending
			INSERT INTO tblRecurringAttempt
			(
				ra_Charge, ra_CreditCard, ra_ECheck, TransPaymentMethod_id, ra_ReplyCode, ra_TransPending, ra_Comments
			)
			SELECT
				ID, rc_CreditCard, rc_ECheck, TransPaymentMethod_id, @sResponseReply, @sTransID, dbo.GetQueryStringValue('ReplyDesc', @sReply)
			FROM
				tblRecurringCharge
			WHERE
				ID=@nCharge
	    	IF @sRecurringReplyUrl Is Not Null Insert Into [dbo].[EventPending](TransPending_id, Parameters, EventPendingType_id, TryCount)Values(@sTransID, @sRecurringReplyUrl, 201, 3)
		END
		ELSE
		BEGIN
			-- transaction failed
			INSERT INTO tblRecurringAttempt
			(
				ra_Charge, ra_CreditCard, ra_ECheck, TransPaymentMethod_id, ra_ReplyCode, ra_TransFail, ra_Comments
			)
			SELECT
				ID, rc_CreditCard, rc_ECheck, TransPaymentMethod_id, @sResponseReply, @sTransID, dbo.GetQueryStringValue('ReplyDesc', @sReply)
			FROM
				tblRecurringCharge
			WHERE
				ID=@nCharge;
			-- block charge and/or series if limit exceeded
			DECLARE @nLimit int, @nCount int, @nDebitCompany int
			SET @nDebitCompany=IsNull((SELECT DebitCompanyID FROM tblCompanyTransFail WHERE ID=@sTransID), 0)
			SET @nCount=IsNull((SELECT Count(*) FROM tblRecurringAttempt WHERE ra_ReplyCode=@sResponseReply AND ra_Charge=@nCharge), 0)
			SET @nLimit=IsNull((SELECT RecurringAttemptsCharge FROM tblDebitCompanyCode WHERE DebitCompanyID=@nDebitCompany AND Code=@sResponseReply), 0)
			IF @nCount>@nLimit UPDATE tblRecurringCharge SET rc_Blocked=1 WHERE ID=@nCharge
			SET @nCount=IsNull((SELECT Count(*) FROM tblRecurringAttempt WHERE ra_ReplyCode=@sResponseReply AND ra_Charge IN (SELECT ID FROM tblRecurringCharge WHERE rc_Series=@nSeries)), 0)
			SET @nLimit=IsNull((SELECT RecurringAttemptsSeries FROM tblDebitCompanyCode WHERE DebitCompanyID=@nDebitCompany AND Code=@sResponseReply), 0)
			IF @nCount>@nLimit UPDATE tblRecurringSeries SET rs_Blocked=1 WHERE ID=@nSeries
	    	IF @sRecurringReplyUrl Is Not Null Insert Into [dbo].[EventPending](TransFail_id, Parameters, EventPendingType_id, TryCount)Values(@sTransID, @sRecurringReplyUrl, 201, 3)
		END
	END
END



GO
