SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RecurringSetSeriesIntervalEx](@nSeries int, @dtNext datetime)
AS
BEGIN
	IF NOT EXISTS(SELECT ID FROM tblRecurringSeries WHERE ID=@nSeries AND rs_Deleted=0)
	BEGIN
		SELECT -1
		RETURN
	END
	DECLARE @dtLast datetime;
	SELECT @dtLast=Max(rc_Date) FROM tblRecurringCharge WHERE rc_Series=@nSeries;
	IF @dtLast IS NULL
	BEGIN
		SELECT -1
		RETURN
	END
	IF @dtNext IS NULL SET @dtNext=Cast(Floor(Cast(GetDate() AS real)) AS datetime);
	UPDATE
		tblRecurringSeries
	SET
		rs_IntervalUnit=1,
		rs_IntervalLength=DateDiff(dd, @dtLast, @dtNext)
	WHERE
		ID=@nSeries;
	SELECT rs_IntervalLength FROM tblRecurringSeries WHERE ID=@nSeries
END
GO
