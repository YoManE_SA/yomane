SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CloneCreditCard](@nID int, @nCompanyID int, @nExpMonth int, @nExpYear int, @sCUI nvarchar(10), @sPhoneNumber nvarchar(50), @sEMail nvarchar(80), @nBillingAddressID int)
AS
BEGIN
	SET NOCOUNT ON
	INSERT INTO tblCreditCard
	(
		companyID,
		ccTypeID,
		BillingAddressId,
		PersonalNumber,
		ExpMM,
		ExpYY,
		Member,
		cc_cui,
		phoneNumber,
		email,
		BINCountry,
		Comment,
		CCard_First6,
		CCard_Last4,
		CCard_number256
	)
	SELECT
		@nCompanyID,
		ccTypeID,
		@nBillingAddressID,
		PersonalNumber,
		Right('00'+LTrim(RTrim(Str(@nExpMonth))), 2),
		Right('00'+LTrim(RTrim(Str(@nExpYear))), 2),
		Member,
		CASE @sCUI WHEN '' THEN cc_cui ELSE dbo.SetCUI(@sCUI) END,
		@sPhoneNumber,
		@sEMail,
		BINCountry,
		Comment,
		CCard_First6,
		CCard_Last4,
		CCard_number256
	FROM
		tblCreditCard
	WHERE
		ID=@nID
	SELECT @@IDENTITY
	SET NOCOUNT OFF
END

GO
