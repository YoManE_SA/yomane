SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SaveWireBankAccountLocal]  
(  
 @nWire int,  
 @sPaymentType nvarchar(80),  
 @sBeneficiary nvarchar(80),  
 @sBranchNumber nvarchar(80),  
 @sAccountNumber nvarchar(80),  
 @sIDNumber nvarchar(20),  
 @nBank int,  
 @nMerchant int=NULL  
)  
AS  
BEGIN  
 UPDATE  
  tblWireMoney  
 SET  
  wirePaymentMethod=@sPaymentType,  
  wirePaymentBank=CASE @nBank WHEN 0 THEN NULL ELSE @nBank END,  
  wirePaymentBranch=@sBranchNumber,  
  wirePaymentPayeeName=@sBeneficiary,  
  wirePaymentAccount=@sAccountNumber,
  wireIdNumber = @sIDNumber
 WHERE  
  WireMoney_ID=@nWire AND IsNull(@nMerchant, Company_ID)=Company_ID  
END
GO
