SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spAddQNAGroup](@nLanguage int, @sName nvarchar(80))
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @nID int;
	SELECT @nID = MAX(QNAGroup_id) FROM QNAGroup;
	SET @nID = ISNULL(@nID, 0)+1;
	INSERT INTO QNAGroup(QNAGroup_id, name, Language_id) VALUES(@nID, @sName, @nLanguage);
	SET NOCOUNT OFF;
	RETURN @nID;
END

GO
