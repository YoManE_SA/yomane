SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AutoCaptureDelete](@nAuthorizedTransID int)
AS
BEGIN
	DELETE FROM tblAutoCapture WHERE AuthorizedTransactionID=@nAuthorizedTransID AND ActualDate IS NULL;
END
GO
