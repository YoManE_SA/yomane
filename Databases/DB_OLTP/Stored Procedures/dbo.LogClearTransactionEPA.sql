SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LogClearTransactionEPA](@nTransID int, @nInstallment int=1, @bClearPaid bit=0, @bClearRefunded bit=0, @sAdminUser varchar(50)=NULL)
AS
BEGIN
	UPDATE
		tblLogImportEPA
	SET
		IsPaid=CASE @bClearPaid WHEN 1 THEN 0 ELSE IsPaid END,
		IsRefunded=CASE @bClearRefunded WHEN 1 THEN 0 ELSE IsRefunded END,
		AdminUser=@sAdminUser
	WHERE
		TransID=@nTransID AND Installment=@nInstallment;
END
GO
