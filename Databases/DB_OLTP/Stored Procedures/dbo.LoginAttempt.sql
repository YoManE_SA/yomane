SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoginAttempt](@nRefType int, @nRefID int, @sPassword nvarchar(50), @nResult int OUTPUT)
AS
BEGIN
	DECLARE @binPassword varbinary(200), @nFailCount int, @dtLastFail datetime, @dtLastSuccess datetime
	SELECT
		@binPassword=LPH_Password256, @nFailCount=LPH_FailCount, @dtLastFail=LPH_LastFail, @dtLastSuccess=LPH_LastSuccess
	FROM
		tblPasswordHistory
	WHERE
		LPH_RefID=@nRefID AND LPH_ID=1 AND LPH_RefType=@nRefType
	IF @nFailCount IS NULL
	BEGIN
		SET @nResult=-1
		RETURN
	END
	IF @nFailCount>2 AND DateDiff(mi, @dtLastFail, GetDate())<31
	BEGIN
		EXEC LoginAttemptAddFail @nRefType, @nRefID
		SET @nResult=-2
		RETURN
	END
	IF DateDiff(d, @dtLastSuccess, GetDate())>90
	BEGIN
		EXEC LoginAttemptAddFail @nRefType, @nRefID
		SET @nResult=-4
		RETURN
	END
	IF dbo.GetEncrypted256(@sPassword)<>@binPassword
	BEGIN
		EXEC LoginAttemptAddFail @nRefType, @nRefID
		SET @nResult=-3
		RETURN
	END
	EXEC LoginAttemptResetFailCount @nRefType, @nRefID
	SET @nResult=0
END
GO
