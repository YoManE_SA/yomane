SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[BlackListRemoveMerchant] ( @nCompanyID INT )
AS 
    BEGIN
        DELETE  FROM [Risk].[BlacklistMerchant]
        WHERE   [Merchant_id] = @nCompanyID
    END

GO
