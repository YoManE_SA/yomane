SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[InsertTempBlock]
    (
      @nMerchant INT ,
      @sCardNumber VARCHAR(25) ,
      @nCCRM INT ,
      @sReplyCode NVARCHAR(11)
    )
AS 
    BEGIN
        DECLARE @nHours INT ,
            @nBlockHours INT ,
            @nCurrency INT ,
            @nFailCount INT ,
            @nReplySource INT ,
            @nPaymentMethod INT;
        SELECT  @nHours = CCRM_Hours ,
                @nBlockHours = CCRM_BlockHours ,
                @nCurrency = CCRM_Currency ,
                @nReplySource = CCRM_ReplySource ,
                @nPaymentMethod = CCRM_PaymentMethod
        FROM    tblCreditCardRiskManagement WITH ( NOLOCK )
        WHERE   CCRM_ID = @nCCRM;
        IF ISNULL(@nBlockHours, 0) = 0 
            RETURN;
        IF @nCurrency < 0 
            IF @nPaymentMethod <= 0 
                SET @nFailCount = dbo.GetTransFailCount(@nMerchant, @nHours,
                                                        @nReplySource, DEFAULT,
                                                        DEFAULT, @nCCRM,
                                                        @sCardNumber)
            ELSE 
                SET @nFailCount = dbo.GetTransFailCount(@nMerchant, @nHours,
                                                        @nReplySource,
                                                        @nPaymentMethod,
                                                        DEFAULT, @nCCRM,
                                                        @sCardNumber)
        ELSE 
            IF @nPaymentMethod <= 0 
                SET @nFailCount = dbo.GetTransFailCount(@nMerchant, @nHours,
                                                        @nReplySource, DEFAULT,
                                                        @nCurrency, @nCCRM,
                                                        @sCardNumber)
            ELSE 
                SET @nFailCount = dbo.GetTransFailCount(@nMerchant, @nHours,
                                                        @nReplySource,
                                                        @nPaymentMethod,
                                                        @nCurrency, @nCCRM,
                                                        @sCardNumber);
        DECLARE @sComment NVARCHAR(500) ,
				@binCardNumber256 VARBINARY(200);
        SET @sComment = 'CCRM Rule ' + CAST(@nCCRM AS VARCHAR(9)) + ' - '
            + CASE @nBlockHours % 24
                WHEN 0 THEN CAST(@nBlockHours / 24 AS VARCHAR(9)) + ' days'
                ELSE CAST(@nBlockHours AS VARCHAR(9)) + ' hours'
              END;
        SET @binCardNumber256 = dbo.GetEncrypted256(REPLACE(@sCardNumber, ' ',
                                                            ''));
        IF NOT EXISTS ( SELECT  FraudCcBlackList_id
                        FROM    tblFraudCcBlackList WITH ( NOLOCK )
                        WHERE   fcbl_ccNumber256 = @binCardNumber256
                                AND fcbl_BlockLevel = 2
                                AND fcbl_CCRMID = @nCCRM
                                AND fcbl_UnblockDate >= GETDATE()
                                AND fcbl_comment = @sComment ) 
            INSERT  INTO tblFraudCcBlackList
                    ( company_id ,
                      fcbl_ccDisplay ,
                      fcbl_comment ,
                      fcbl_ccNumber256 ,
                      fcbl_BlockLevel ,
                      fcbl_BlockCount ,
                      fcbl_UnblockDate ,
                      fcbl_CCRMID ,
                      fcbl_ReplyCode ,
                      PaymentMethod_id
		            )
            VALUES  ( @nMerchant ,
                      dbo.fnFormatCcNumToHideDigits(@sCardNumber) ,
                      @sComment ,
                      @binCardNumber256 ,
                      2 ,
                      @nFailCount ,
                      DATEADD(hh, @nBlockHours, GETDATE()) ,
                      @nCCRM ,
                      @sReplyCode ,
                      dbo.GetPaymentMethodCC(@sCardNumber)
		            );
    END


GO
