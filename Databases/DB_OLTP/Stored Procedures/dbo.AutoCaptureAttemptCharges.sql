SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AutoCaptureAttemptCharges](@nTransAuthID int=0)
AS
BEGIN
	SET @nTransAuthID=IsNull(@nTransAuthID, 0);
	DECLARE @nRC int, @nAuthorizedTransID int, @nTransID int, @bSuccess bit;
	DECLARE curAuth CURSOR FOR
		SELECT AuthorizedTransactionID FROM tblAutoCapture WHERE ScheduledDate<=GetDate() AND ActualDate IS NULL AND @nTransAuthID IN (0, AuthorizedTransactionID);
	OPEN curAuth
	FETCH NEXT FROM curAuth INTO @nAuthorizedTransID
	WHILE @@FETCH_STATUS=0
	BEGIN
		EXEC @nRC=AutoCaptureAttemptCharge @nAuthorizedTransID, @nTransID OUTPUT, @bSuccess OUTPUT;
		--PRINT 'AuthorizedTransID '+LTrim(RTrim(Str(@nAuthorizedTransID)))+' RC '+LTrim(RTrim(Str(@nRC)));
		IF @nRC=0
			UPDATE
				tblAutoCapture
			SET
				CaptureTransactionID=CASE @bSuccess WHEN 1 THEN @nTransID ELSE NULL END,
				DeclineTransactionID=CASE @bSuccess WHEN 0 THEN @nTransID ELSE NULL END,
				ActualDate=GETDATE()
			WHERE
				AuthorizedTransactionID=@nAuthorizedTransID;
		FETCH NEXT FROM curAuth INTO @nAuthorizedTransID;
	END
	CLOSE curAuth
	DEALLOCATE curAuth
END
GO
