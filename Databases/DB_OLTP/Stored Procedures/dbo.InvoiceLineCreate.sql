SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[InvoiceLineCreate]  
 (  
  @nTransactionPayID int,  
  @sBillTo nvarchar(50),  
  @nMerchant int,  
  @sText nvarchar(200),  
  @nQuantity real,  
  @nPrice money,  
  @nCurrency int,  
  @nCurrencyRate real,  
  @sUsername nvarchar(50)
 )  
AS  
BEGIN  
 IF @nMerchant=0 AND IsNull(@sBillTo, '')=''  
 BEGIN  
  -- neither merchant nor billto name specified  
  SELECT -11  
  RETURN  
 END  
 IF @nMerchant>0 AND IsNull(@sBillTo, '')<>''  
 BEGIN  
  -- both merchant and Invoice name specified  
  SELECT -22  
  RETURN  
 END  
 IF @nMerchant>0 AND NOT EXISTS (SELECT ID FROM tblCompany WHERE ID=@nMerchant)  
 BEGIN  
  -- merchant not found  
  SELECT -33  
  RETURN  
 END  

 INSERT INTO  
  tblInvoiceLine  
  (  
   il_TransactionPayID,  
   il_BillToName,  
   il_MerchantID,  
   il_Text,  
   il_Quantity,  
   il_Price,  
   il_Currency,  
   il_CurrencyRate,  
   il_Username  
  )  
 VALUES  
  (  
   @nTransactionPayID,  
   @sBillTo,  
   @nMerchant,  
   @sText,  
   @nQuantity,  
   @nPrice,  
   @nCurrency,  
   @nCurrencyRate,  
   @sUsername  
  )  
 SELECT @@IDENTITY  
END  


GO
