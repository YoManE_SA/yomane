SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetRecurringDailyRatioBNS] AS
BEGIN
	SELECT
		TransDate,
		SUM(IsPassed) CountPass,
		SUM(IsFailed) CountFail
	FROM
		(
			SELECT
				ID, Cast(InsertDate AS date) TransDate, companyID, 1 IsPassed, 0 IsFailed
			FROM
				tblCompanyTransPass
			WHERE
				TransactionTypeID=20 and InsertDate BETWEEN '20110201' AND '20110228' and DebitCompanyID=18 and isTestOnly=0 and IsChargeback=0
			UNION SELECT
				ID, Cast(InsertDate AS date), companyID, 0, 1
			FROM
				tblCompanyTransFail
			WHERE
				TransactionTypeID=20 and InsertDate BETWEEN '20110201' AND '20110228' and DebitCompanyID=18 and isTestOnly=0
		) q
	GROUP BY
		TransDate
	ORDER BY
		TransDate;
	SELECT
		m.ID,
		CompanyName,
		TransDate,
		SUM(IsPassed) CountPass,
		SUM(IsFailed) CountFail
	FROM
		(
			SELECT
				ID, Cast(InsertDate AS date) TransDate, companyID, 1 IsPassed, 0 IsFailed
			FROM
				tblCompanyTransPass
			WHERE
				TransactionTypeID=20 and InsertDate BETWEEN '20110201' AND '20110228' and DebitCompanyID=18 and isTestOnly=0 and IsChargeback=0
			UNION SELECT
				ID, Cast(InsertDate AS date), companyID, 0, 1
			FROM
				tblCompanyTransFail
			WHERE
				TransactionTypeID=20 and InsertDate BETWEEN '20110201' AND '20110228' and DebitCompanyID=18 and isTestOnly=0
		) q
		inner join tblCompany m on companyID=m.ID
	GROUP BY
		m.ID,
		CompanyName,
		TransDate
	ORDER BY
		m.ID,
		CompanyName,
		TransDate;
END
GO
