SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CustomerSaveCard]
(
      @nID int,
      @nCustomer int,
      @sCardNumber varchar(20),
      @nMonth as int,
      @nYear as int,
      @sCUI as varchar(4),
      @sFullName as nvarchar(100),
      @sPersonalNumber as nvarchar(50),
      @sPhone as nvarchar(50),
      @sMail as nvarchar(80),
      @sStreet as nvarchar(50),
      @sCity as nvarchar(50),
      @nState as int,
      @nCountry as int,
      @sZipcode as nvarchar(50)
)
AS
BEGIN
      IF IsNull(@nID, 0)<1    -- if ID not positive, insert new card
      BEGIN
            INSERT INTO tblCustomerCC(CustomerID) VALUES(@nCustomer)
            SET @nID = @@IDENTITY
      END
      IF @nYear<100 SET @nYear = @nYear+(CASE WHEN @nYear>50 THEN 1900 ELSE 2000 END)
      UPDATE
            tblCustomerCC
      SET
            CCard_Number256 = CASE CharIndex('*', @sCardNumber) WHEN 0 THEN dbo.GetEncrypted256(@sCardNumber) ELSE CCard_Number256 END,
            CCard_ExpMM = Right('00'+LTrim(RTrim(Str(@nMonth))), 2),
            CCard_ExpYY = Right('0000'+LTrim(RTrim(Str(@nYear+(CASE WHEN @nYear<100 THEN 2000 ELSE 0 END)))), 4),
            CCard_Cui = CASE CharIndex('*', @sCUI) WHEN 0 THEN dbo.SetCUI(@sCUI) ELSE CCard_Cui END,
            CH_FullName = @sFullName,
            CH_PersonalNumber = @sPersonalNumber,
            CH_PhoneNumber = @sPhone,
            CH_Email = @sMail,
            Billing_Street = @sStreet,
            Billing_City = @sCity,
            Billing_State = @nState,
            Billing_Country = @nCountry,
            Billing_Zipcode = @sZipcode,
            CCard_Last4 = Right(@sCardNumber, 4),
            CCard_TypeID = CASE CharIndex('*', @sCardNumber) WHEN 0 THEN dbo.GetCCType(@sCardNumber) ELSE CCard_TypeID END,
            PaymentMethod = CASE CharIndex('*', @sCardNumber) WHEN 0 THEN dbo.GetPaymentMethodCC(@sCardNumber) ELSE PaymentMethod END
      WHERE
            ID = @nID AND CustomerID = @nCustomer
END
GO
