SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[InsertBIN](@sBin char(6), @sCountryIso char(2), @sCCTypeAbbreviation char(2))
AS
BEGIN
	IF @sBin IS NULL RETURN -1;
	IF LTrim(RTrim(@sBin))='' RETURN -2;
	IF @sCountryIso IS NULL RETURN -3;
	IF Len(LTrim(RTrim(@sCountryIso)))<>2 RETURN -4;
	IF @sCCTypeAbbreviation IS NULL RETURN -5;
	IF Len(LTrim(RTrim(@sCCTypeAbbreviation)))<>2 RETURN -6;
	IF NOT EXISTS (SELECT 1 FROM [List].[CountryList] WHERE [CountryISOCode]=@sCountryIso) RETURN -7;
	DECLARE @nPaymentMethod int;
	SELECT @nPaymentMethod=(SELECT TOP 1 PaymentMethod_id FROM List.PaymentMethod WHERE pm_Type=2 AND Abbreviation=@sCCTypeAbbreviation);
	IF @nPaymentMethod IS NULL RETURN -8;
	IF EXISTS (SELECT BINID FROM tblCreditCardBIN WHERE BIN=@sBin) RETURN -9;
	INSERT INTO tblCreditCardBIN(BIN, isoCode, PaymentMethod, CCName, CCType, ImportDate) VALUES (@sBin, @sCountryIso, @nPaymentMethod, @sCCTypeAbbreviation, @nPaymentMethod-20, NULL);
	RETURN 0;
END


GO
