SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetDebitBlockLog](@bShowChecks bit=0, @dtBegin datetime=NULL, @dtEnd datetime=NULL)
AS
BEGIN
	IF @dtBegin IS NULL SET @dtBegin=0;
	IF @dtEnd IS NULL SET @dtEnd=GetDate();
	DECLARE @s1 varchar(20), @s2 varchar(20), @s3 varchar(20), @s4 varchar(20);
	IF @bShowChecks=0
		SELECT @s1='RULE CHECK', @s2='RULE CHECK END', @s3='BLOCK CHECK', @s4='BLOCK CHECK END';
	ELSE
		SELECT @s1='', @s2='', @s3='', @s4='';
	SELECT TOP 1000
		*
	FROM
		tblDebitBlockLog
	WHERE
		dbl_Date BETWEEN @dtBegin AND @dtEnd
		AND
		dbl_Type NOT IN (@s1, @s2, @s3, @s4)
	ORDER BY
		ID DESC
END
GO
