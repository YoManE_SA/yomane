SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ChargeMonthlyFees]
AS
BEGIN
	DECLARE @nMerchant int, @sType nvarchar(50), @nAmount money, @nCurrency int, @sSQL nvarchar(200), @sDisplay nvarchar(50), @nEuroRate smallmoney, @nDollarRate smallmoney;
	SELECT @nEuroRate=CUR_BaseRate FROM tblSystemCurrencies WHERE CUR_ID=2;
	SELECT @nDollarRate=CUR_BaseRate FROM tblSystemCurrencies WHERE CUR_ID=1;
	DECLARE curFee CURSOR LOCAL FOR
		SELECT
			ID, 'Euro', MonthlyFeeEuro/CUR_BaseRate*@nEuroRate, CUR_ID, 'Monthly Fee '+Right('0'+LTrim(RTrim(Str(Month(MonthlyFeeEuroDate)))), 2)+' '+LTrim(RTrim(Str(Year(MonthlyFeeEuroDate))))
		FROM
			tblCompany CROSS JOIN tblGlobalValues INNER JOIN tblSystemCurrencies ON PaymentReceiveCurrency=CUR_ID
		WHERE
			ActiveStatus=30 AND IsMonthlyFeeEuro=1 AND MonthlyFeeEuroDate<=GETDATE()
		UNION SELECT
			ID, 'Dollar', MonthlyFeeDollar/CUR_BaseRate*@nDollarRate, CUR_ID, 'Monthly Fee '+Right('0'+LTrim(RTrim(Str(Month(MonthlyFeeDollarDate)))), 2)+' '+LTrim(RTrim(Str(Year(MonthlyFeeDollarDate))))
		FROM
			tblCompany CROSS JOIN tblGlobalValues INNER JOIN tblSystemCurrencies ON PaymentReceiveCurrency=CUR_ID
		WHERE
			ActiveStatus=30 AND IsMonthlyFeeDollar=1 AND MonthlyFeeDollarDate<=GETDATE()
		UNION SELECT
			ID, 'BankHigh', MonthlyFeeBankHigh/CUR_BaseRate*@nEuroRate, CUR_ID, 'Monthly Fee '+Right('0'+LTrim(RTrim(Str(Month(MonthlyFeeBankHighDate)))), 2)+' '+LTrim(RTrim(Str(Year(MonthlyFeeBankHighDate))))
		FROM
			tblCompany CROSS JOIN tblGlobalValues INNER JOIN tblSystemCurrencies ON PaymentReceiveCurrency=CUR_ID
		WHERE
			ActiveStatus=30 AND IsMonthlyFeeBankHigh=1 AND MonthlyFeeBankHighDate<=GETDATE()
		UNION SELECT
			ID, 'BankLow', MonthlyFeeBankLow/CUR_BaseRate*@nEuroRate, CUR_ID, 'Monthly Fee '+Right('0'+LTrim(RTrim(Str(Month(MonthlyFeeBankLowDate)))), 2)+' '+LTrim(RTrim(Str(Year(MonthlyFeeBankLowDate))))
		FROM
			tblCompany CROSS JOIN tblGlobalValues INNER JOIN tblSystemCurrencies ON PaymentReceiveCurrency=CUR_ID
		WHERE
			ActiveStatus=30 AND IsMonthlyFeeBankLow=1 AND MonthlyFeeBankLowDate<=GETDATE();
	OPEN curFee;
	FETCH NEXT FROM curFee INTO @nMerchant, @sType, @nAmount, @nCurrency, @sDisplay;
	WHILE @@FETCH_STATUS=0
	BEGIN
		EXEC CreateFeeTransaction @nMerchant, @nAmount, @nCurrency, 5, 31, @sDisplay, '';
		SET @sSQL='UPDATE tblCompany SET MonthlyFee'+@sType+'Date=DateAdd(month, 1, MonthlyFee'+@sType+'Date) WHERE ID='+Str(@nMerchant);
		EXEC (@sSQL);
		FETCH NEXT FROM curFee INTO @nMerchant, @sType, @nAmount, @nCurrency, @sDisplay;
	END
	CLOSE curFee;
	DEALLOCATE curFee;
END
GO
