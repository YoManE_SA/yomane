SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteLogBnsTransactionsWithoutEpaLast]
AS
BEGIN
	DECLARE @dtLast smalldatetime;
	SELECT @dtLast=MAX(LogDate) FROM tblLogSendBnsTransactionsWithoutEpa;
	IF @dtLast IS NOT NULL DELETE FROM tblLogSendBnsTransactionsWithoutEpa WHERE LogDate=@dtLast;
END
GO
