SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spDataPullingGetTransXML]
    (
      @nMerchant INT ,
      @dtFrom DATETIME = NULL ,
      @dtTo DATETIME = NULL ,
      @nTransType INT = 15 ,
      @nTransID INT = 0
    )
AS 
    BEGIN
        IF ISNULL(@nTransID, 0) > 0 
            BEGIN
                SELECT  @dtTo = DATEADD(HOUR, 1, InsertDate)
                FROM    tblCompanyTransPass
                WHERE   ID = @nTransID;
                SET @dtFrom = DATEADD(HOUR, -2, @dtTo);
            END

        IF @dtTo IS NULL 
            SET @dtTo = GETDATE();
        IF @dtFrom IS NULL 
            SET @dtFrom = DATEADD(d, -1, @dtTo);

        DECLARE @sTransTypes VARCHAR(100);
        SET @sTransTypes = ';';
        IF @nTransType % 2 >= 1 
            SET @sTransTypes = @sTransTypes + 'Debit;';
        IF @nTransType % 4 >= 2 
            SET @sTransTypes = @sTransTypes + 'Refund;';
        IF @nTransType % 8 >= 4 
            SET @sTransTypes = @sTransTypes + 'CHB;';
        IF @nTransType % 16 >= 8 
            SET @sTransTypes = @sTransTypes + 'Retrieval;';
	
        IF @nTransID = 0 
            SET @nTransID = NULL;
		
		
        SELECT  1 Tag ,
                NULL Parent ,
                t.ID [Trans!1!ID!ELEMENT] ,
                CONVERT(VARCHAR(10), t.InsertDate, 103)
                + ' ' + CONVERT(VARCHAR(10), t.InsertDate, 108) [Trans!1!Date!ELEMENT] ,
                t.Currency [Trans!1!Currency!ELEMENT] ,
                t.Amount [Trans!1!Amount!ELEMENT] ,
                COALESCE(t.ApprovalNumber, '') [Trans!1!Approval!ELEMENT] ,
                COALESCE(tpi.[EmailAddress], '') AS [Trans!1!Email!ELEMENT] ,
                COALESCE(tpi.[PhoneNumber], '') AS [Trans!1!Phone!ELEMENT] ,
                COALESCE(tpi.[PersonalNumber], '') AS [Trans!1!PersonalNum!ELEMENT] ,
                CASE WHEN dbo.IsCHB(DeniedStatus, isTestOnly) = 1 THEN 'CHB'
                     WHEN DeniedStatus IN ( 3, 5 ) THEN 'Retrieval'
                     WHEN CreditType > 0 THEN 'Debit'
                     ELSE 'Refund'
                END [Trans!1!Type!ELEMENT] ,
                CASE WHEN dbo.IsCHB(DeniedStatus, isTestOnly) = 1
                     THEN CONVERT(VARCHAR(10), DeniedDate, 103) + ' '
                          + CONVERT(VARCHAR(10), DeniedDate, 108)
                     ELSE NULL
                END [Trans!1!ChargebackDate!ELEMENT] ,
                CASE WHEN dbo.IsCHB(DeniedStatus, isTestOnly) = 1
                          AND DeniedAdminComment LIKE 'AutoCHB (%)%=%, Reason=%'
                     THEN RIGHT(DeniedAdminComment,
                                LEN(DeniedAdminComment)
                                - CHARINDEX(', Reason=', DeniedAdminComment)
                                - 8)
                     WHEN DeniedStatus IN ( 3, 5 )
                          AND DeniedAdminComment LIKE '%Reason=_%'
                     THEN RIGHT(DeniedAdminComment,
                                LEN(DeniedAdminComment)
                                - CHARINDEX(', Reason=', DeniedAdminComment)
                                - 8)
                     ELSE NULL
                END [Trans!1!ChargebackReason!ELEMENT] ,
                COALESCE(t.OrderNumber, '') [Trans!1!Order!ELEMENT] ,
                t.RecurringChargeNumber [Trans!1!RecurringCharge!ELEMENT] ,
                t.RecurringSeries [Trans!1!RecurringSeries!ELEMENT] ,
                t.Comment [Trans!1!Comment!ELEMENT] ,
                '000' [Trans!1!Reply!ELEMENT] ,
                t.OriginalTransID [Trans!1!OriginalTransID!ELEMENT] ,
                t.RefTrans [Trans!1!RefTrans!ELEMENT] ,
                CASE WHEN pm.PaymentMethodType_id = 2 THEN 'CC'
                     ELSE pm.Name
                END [Trans!1!Method!ELEMENT] ,
                NULL [CreditCard!2!ID] ,
                NULL [CreditCard!2!Type!ELEMENT] ,
                NULL [CreditCard!2!BIN!ELEMENT] ,
                NULL [CreditCard!2!BINCountry!ELEMENT] ,
                NULL [CreditCard!2!ExpMM!ELEMENT] ,
                NULL [CreditCard!2!ExpYY!ELEMENT] ,
                NULL [CreditCard!2!Last4!ELEMENT] ,
                NULL [CreditCard!2!Holder!ELEMENT] ,
                NULL [Address!3!ID] ,
                NULL [Address!3!Line1!ELEMENT] ,
                NULL [Address!3!Line2!ELEMENT] ,
                NULL [Address!3!City!ELEMENT] ,
                NULL [Address!3!Postal!ELEMENT] ,
                NULL [Address!3!State!ELEMENT] ,
                NULL [Address!3!Country!ELEMENT]
        FROM    tblCompanyTransPass AS t
                INNER JOIN List.PaymentMethod AS pm ON t.PaymentMethod = pm.PaymentMethod_id
                LEFT JOIN Trans.TransPayerInfo AS tpi ON ( tpi.TransPayerInfo_id = t.TransPayerInfo_id )
        WHERE   t.CompanyID = @nMerchant
                AND ( t.InsertDate BETWEEN @dtFrom AND @dtTo OR t.DeniedDate BETWEEN @dtFrom AND   @dtTo )
                AND CHARINDEX(CASE WHEN dbo.IsCHB(DeniedStatus, isTestOnly) = 1
                                   THEN 'CHB'
                                   WHEN DeniedStatus IN ( 3, 5 )
                                   THEN 'Retrieval'
                                   WHEN CreditType > 0 THEN 'Debit'
                                   ELSE 'Refund'
                              END, @sTransTypes) > 0
                AND ISNULL(@nTransID, t.ID) = t.ID
        UNION
        SELECT  2 Tag ,
                1 Parent ,
                t.ID ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL , --'tblCreditCard.ID' ,
                COALESCE(pm.Name, '') ,
                COALESCE(tpm.Value1First6Text, '') ,
                COALESCE(tpm.IssuerCountryIsoCode, '') ,
                RIGHT('00' + LTRIM(RTRIM(STR(DATEPART(MONTH,tpm.ExpirationDate)))), 2) ,
                RIGHT('00' + LTRIM(RTRIM(STR(DATEPART(YEAR,tpm.ExpirationDate)))), 2) ,
                RIGHT('0000' + LTRIM(RTRIM(STR(tpm.Value1Last4Text))), 4) ,
                COALESCE(tpi.FirstName + ' ' + tpi.LastName, ''),
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL
        FROM    tblCompanyTransPass AS t
                INNER JOIN List.PaymentMethod AS pm ON t.PaymentMethod = pm.PaymentMethod_id
                INNER JOIN Trans.TransPayerInfo AS tpi ON tpi.TransPayerInfo_id = t.TransPayerInfo_id
                INNER JOIN Trans.TransPaymentMethod AS tpm ON tpm.TransPaymentMethod_id = t.TransPaymentMethod_id
        WHERE   t.CompanyID = @nMerchant
                AND ( t.InsertDate BETWEEN @dtFrom AND @dtTo OR t.DeniedDate BETWEEN @dtFrom AND @dtTo )
                AND CHARINDEX(CASE WHEN dbo.IsCHB(DeniedStatus, isTestOnly) = 1
                                   THEN 'CHB'
                                   WHEN DeniedStatus IN ( 3, 5 )
                                   THEN 'Retrieval'
                                   WHEN CreditType > 0 THEN 'Debit'
                                   ELSE 'Refund'
                              END, @sTransTypes) > 0
                AND pm.PaymentMethodType_id  = 2
                AND ISNULL(@nTransID, t.ID) = t.ID
        UNION
        SELECT  3 Tag ,
                1 Parent ,
                t.ID ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL ,
                NULL , --'tblBillingAddress.ID' ,
                COALESCE(tpb.Street1, '') ,
                COALESCE(tpb.Street2, '') ,
                COALESCE(tpb.City, '') ,
                COALESCE(tpb.PostalCode, '') ,
                COALESCE(tpb.StateISOCode, '') ,
                COALESCE(tpb.CountryISOCode, '')
        FROM    tblCompanyTransPass AS t
                INNER JOIN Trans.TransPaymentMethod AS tpm ON tpm.TransPaymentMethod_id = t.TransPaymentMethod_id
                INNER JOIN Trans.TransPaymentBillingAddress AS tpb ON tpb.TransPaymentBillingAddress_id = tpm.TransPaymentBillingAddress_id
                INNER JOIN List.PaymentMethod AS pm ON t.PaymentMethod = pm.PaymentMethod_id AND pm.PaymentMethodType_id = 2
        WHERE   t.CompanyID = @nMerchant
                AND ( t.InsertDate BETWEEN @dtFrom
                                                     AND     @dtTo
                      OR t.DeniedDate BETWEEN @dtFrom
                                                        AND   @dtTo
                    )
                AND CHARINDEX(CASE WHEN dbo.IsCHB(DeniedStatus, isTestOnly) = 1
                                   THEN 'CHB'
                                   WHEN DeniedStatus IN ( 3, 5 )
                                   THEN 'Retrieval'
                                   WHEN CreditType > 0 THEN 'Debit'
                                   ELSE 'Refund'
                              END, @sTransTypes) > 0
                AND ISNULL(@nTransID, t.ID) = t.ID
		ORDER BY [Trans!1!ID!ELEMENT]
        FOR     XML EXPLICIT
    END

GO
