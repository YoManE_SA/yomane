SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetNewHashKey](@nLength int=10)
AS
BEGIN
	DECLARE @i int, @n int, @sKey varchar(32)
	SET @sKey=''
	SET @i=0
	WHILE @i<@nLength
	BEGIN
		SET @n=Rand()*36
		IF @n<1 SET @n=1
		IF @n>36 SET @n=36
		SET @sKey=@sKey+Substring('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', @n, 1)
		SET @i=@i+1
	END
	SELECT @sKey
END
GO
