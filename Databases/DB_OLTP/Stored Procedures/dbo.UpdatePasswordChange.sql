SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[UpdatePasswordChange]
    (
      @nRefType INT ,
      @nRefID INT ,
      @sIP NVARCHAR(50) ,
      @sPassword NVARCHAR(200)
    )
AS 
    BEGIN
        SET NOCOUNT ON;
        DECLARE @Encrypted VARBINARY(200) ,
            @nQuantity INT;
        SET @Encrypted = dbo.GetEncrypted256(@sPassword);
        SELECT  @nQuantity = COUNT(*)
        FROM    tblPasswordHistory
        WHERE   LPH_RefType = @nRefType
                AND LPH_RefID = @nRefID
                AND LPH_Password256 = @Encrypted;
        IF @nQuantity > 0 
            SELECT  CAST(0 AS BIT) IsInserted;
        ELSE 
            BEGIN
                DELETE  FROM tblPasswordHistory
                WHERE   LPH_RefType = @nRefType
                        AND LPH_RefID = @nRefID
                        AND LPH_ID = 4;
                UPDATE  tblPasswordHistory
                SET     LPH_ID = LPH_ID + 1
                WHERE   LPH_RefType = @nRefType
                        AND LPH_RefID = @nRefID;
                INSERT  INTO tblPasswordHistory
                        ( LPH_ID ,
                          LPH_RefID ,
                          LPH_RefType ,
                          LPH_IP ,
                          LPH_Password256
                        )
                VALUES  ( 1 ,
                          @nRefID ,
                          @nRefType ,
                          @sIP ,
                          @Encrypted
                        );
                SELECT  CAST(1 AS BIT) IsInserted;
            END
        SET NOCOUNT OFF;
    END

GO
