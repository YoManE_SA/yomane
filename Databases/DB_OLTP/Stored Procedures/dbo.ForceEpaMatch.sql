SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ForceEpaMatch](@nID int, @nTransID int, @bDebug bit=0)
AS
BEGIN
	DECLARE @bRefund bit;
	SELECT @bRefund=IsRefund FROM tblEpaPending WHERE ID=@nID;
	IF @bRefund IS NULL RETURN -1;
	IF NOT EXISTS (SELECT ID FROM tblCompany WHERE ID=@nTransID) RETURN -2;
	DELETE FROM tblEpaPending WHERE ID=@nID;
	IF @bRefund=0
	BEGIN
		IF @bDebug=1 PRINT 'Updating paid in existing transactions...';
		UPDATE
			l
		SET
			IsPaid=1, PaidInsertDate=IsNull(PayoutDate, GetDate())
		FROM
			tblLogImportEPA l INNER JOIN tblEpaPending p ON l.TransID=p.TransID AND l.Installment=p.Installment
		WHERE
			p.ID=@nID;
		IF @bDebug=1 PRINT 'Inserting paid in new transactions...';
		INSERT INTO
			tblLogImportEPA(TransID, Installment, IsPaid, PaidInsertDate)
		SELECT
			p.TransID, p.Installment, 1, IsNull(PayoutDate, GetDate())
		FROM
			tblEpaPending p LEFT JOIN tblLogImportEPA l ON p.TransID=l.TransID AND p.Installment=l.Installment
		WHERE
			p.ID=@nID;
		RETURN 1;
	END
	ELSE
	BEGIN
		IF @bDebug=1 PRINT 'Updating refunded in existing transactions...';
		UPDATE
			l
		SET
			IsRefunded=1, RefundedInsertDate=IsNull(PayoutDate, GetDate())
		FROM
			tblLogImportEPA l INNER JOIN tblEpaPending p ON l.TransID=p.TransID AND l.Installment=p.Installment
		WHERE
			p.ID=@nID;
		IF @bDebug=1 PRINT 'Inserting refunded in new transactions...';
		INSERT INTO
			tblLogImportEPA(TransID, Installment, IsRefunded, RefundedInsertDate)
		SELECT
			p.TransID, p.Installment, 1, IsNull(PayoutDate, GetDate())
		FROM
			tblEpaPending p LEFT JOIN tblLogImportEPA l ON p.TransID=l.TransID AND p.Installment=l.Installment
		WHERE
			p.ID=@nID;
		RETURN 2;
	END
END
GO
