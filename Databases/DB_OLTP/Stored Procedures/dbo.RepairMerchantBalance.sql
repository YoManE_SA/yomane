SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RepairMerchantBalance](@nMerchantID int=NULL, @nCurrency int=NULL)
AS
BEGIN
	UPDATE
		tblCompanyBalance
	SET
		BalanceExpected=
			(
				SELECT
					Sum(amount)
				FROM
					tblCompanyBalance t
				WHERE
					t.company_id=tblCompanyBalance.company_id
					AND
					t.currency=tblCompanyBalance.currency
					AND
					t.companyBalance_ID<=tblCompanyBalance.companyBalance_ID
			),
		BalanceCurrent=
			(
				SELECT
					Sum(amount)
				FROM
					tblCompanyBalance t
				WHERE
					t.company_id=tblCompanyBalance.company_id
					AND
					t.currency=tblCompanyBalance.currency
					AND
					t.companyBalance_ID<=tblCompanyBalance.companyBalance_ID
					AND
					t.status<>1
			)
	WHERE
		CompanyBalance_ID=
			(
				SELECT
					Min(CompanyBalance_ID)
				FROM
					tblCompanyBalance
				WHERE
					(@nMerchantID IS NULL OR company_ID=@nMerchantID)
					AND
					(@nCurrency IS NULL OR currency=@nCurrency)
			)
END
GO
