SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spSelect_TerminalSelectList] (@sTerminalNumber as nvarchar(20)='')
AS
SELECT
	tblDebitTerminals.terminalNumber,
	tblDebitTerminals.processingMethod,
	tblDebitTerminals.dt_name,
	tblDebitTerminals.dt_name as terminalName_heb,
	tblDebitTerminals.isActive,
	tblDebitTerminals.isNetpayTerminal,
	tblDebitCompany.dc_name,
	tblDebitTerminals.id
FROM
	tblDebitTerminals
	INNER JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany=tblDebitCompany.debitCompany_id
WHERE
	@sTerminalNumber IN ('', terminalNumber)
ORDER BY
	tblDebitTerminals.DebitCompany,
	tblDebitTerminals.dt_name

GO
