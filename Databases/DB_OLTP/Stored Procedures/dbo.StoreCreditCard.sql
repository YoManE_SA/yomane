SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[StoreCreditCard]
    (
      @nMerchant INT ,
      @nPaymentMethod INT ,
      @sPersonalNum NVARCHAR(50) ,
      @sCardNumber VARCHAR(25) ,
      @nExpireMonth INT ,
      @nExpireYear INT ,
      @sCUI VARCHAR(10) ,
      @sPhone NVARCHAR(50) ,
      @sMail NVARCHAR(80) ,
      @sFullName NVARCHAR(100) ,
      @sComment NVARCHAR(500) ,
      @sDisplay NVARCHAR(50) ,
      @sIP NVARCHAR(50) ,
      @nCountry INT ,
      @nState INT ,
      @sAddress1 NVARCHAR(100) ,
      @sAddress2 NVARCHAR(100) ,
      @sCity NVARCHAR(60) ,
      @sZip NVARCHAR(20)
    )
AS 
    BEGIN
        SET NOCOUNT ON;
        DECLARE @nID INT;
        IF NOT EXISTS (SELECT [CountryISOCode] FROM [List].[CountryList] WHERE [CountryID] = @nCountry)
            SET @nCountry = NULL;
        IF NOT EXISTS (SELECT [StateISOCode] FROM [List].[StateList] WHERE [StateID] = @nState)
            SET @nState = NULL;
        SET @nID = ( SELECT TOP 1
                            ID
                     FROM   tblCCStorage WITH ( NOLOCK )
                     WHERE  isDeleted = 0
                            AND companyID = @nMerchant
                            AND CCard_number256 IN (
                            dbo.GetEncrypted256(@sCardNumber),
                            dbo.GetEncrypted256(REPLACE(@sCardNumber, ' ', '')) )
                   );
        IF @nID IS NULL 
            BEGIN
                INSERT  INTO tblCCStorage
                        ( companyID ,
                          PaymentMethod ,
                          CHPersonalNum ,
                          CCard_number256 ,
                          ExpMM ,
                          ExpYY ,
                          cc_cui ,
                          CHPhoneNumber ,
                          CHEmail ,
                          CHFullName ,
                          Comment ,
                          Ccard_display ,
                          IPAddress ,
                          countryId ,
                          stateId ,
                          CHStreet ,
                          CHStreet1 ,
                          CHSCity ,
                          CHSZipCode
		                )
                VALUES  ( @nMerchant ,
                          @nPaymentMethod ,
                          @sPersonalNum ,
                          dbo.GetEncrypted256(@sCardNumber) ,
                          @nExpireMonth ,
                          @nExpireYear ,
                          @sCUI ,
                          @sPhone ,
                          @sMail ,
                          @sFullName ,
                          @sComment ,
                          @sDisplay ,
                          @sIP ,
                          @nCountry ,
                          @nState ,
                          @sAddress1 ,
                          @sAddress2 ,
                          @sCity ,
                          @sZip
		                );
                SET @nID = @@IDENTITY;
            END	
        ELSE 
            UPDATE  tblCCStorage
            SET     PaymentMethod = @nPaymentMethod ,
                    CHPersonalNum = @sPersonalNum ,
                    ExpMM = @nExpireMonth ,
                    ExpYY = @nExpireYear ,
                    cc_cui = @sCUI ,
                    CHPhoneNumber = @sPhone ,
                    CHEmail = @sMail ,
                    CHFullName = @sFullName ,
                    Comment = @sComment ,
                    Ccard_display = @sDisplay ,
                    IPAddress = @sIP ,
                    countryId = @nCountry ,
                    stateId = @nState ,
                    CHStreet = @sAddress1 ,
                    CHStreet1 = @sAddress2 ,
                    CHSCity = @sCity ,
                    CHSZipCode = @sZip ,
                    InsertDate = GETDATE()
            WHERE   ID = @nID;
        SET NOCOUNT OFF;
        RETURN @nID;
    END
GO
