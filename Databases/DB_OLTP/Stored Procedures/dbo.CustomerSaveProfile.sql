SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CustomerSaveProfile]
(
	@nID int,
	@sFirstName nvarchar(200),
	@sLastName nvarchar(200),
	@sPhone nvarchar(50),
	@sCellPhone nvarchar(50),
	@sIDNumber nvarchar(20),
	@sStreet nvarchar(500),
	@sCity nvarchar(50),
	@sZip nvarchar(25),
	@nCountry int,
	@nState int,
	@sDOB Date

)
AS
BEGIN
	-- check validity
	IF @sFirstName='' OR @sLastName='' OR @sPhone=''
	BEGIN
		-- there is at least one empty 'personal' field
		SELECT -1
		RETURN
	END
	IF @sStreet='' OR @sCity='' OR @sZip='' OR @nCountry<=0
	BEGIN
		-- there is at least one empty 'address' field
		SELECT -2
		RETURN
	END
	UPDATE
		tblCustomer
	SET
		FirstName=@sFirstName,
		LastName=@sLastName,
		Phone=@sPhone,
		CellPhone=@sCellPhone,
		IDNumber=@sIDNumber,
		Street=@sStreet,
		City=@sCity,
		Zip=@sZip,
		Country=@nCountry,
		State=@nState,
		DOB = @sDOB
	WHERE
		ID=@nID
	SELECT @nID
END

GO
