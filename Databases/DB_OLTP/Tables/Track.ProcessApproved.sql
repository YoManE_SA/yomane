CREATE TABLE [Track].[ProcessApproved]
(
[ProcessApproved_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NULL,
[PaymentMethod_id] [smallint] NULL,
[TransCreditType_id] [tinyint] NULL,
[TransDate] [datetime] NOT NULL CONSTRAINT [DF_ProcessApproved_TransDate] DEFAULT (getdate()),
[TransAmount] [money] NULL,
[TransCurrency] [tinyint] NULL,
[TransInstallments] [tinyint] NULL,
[TransIpAddress] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[CreditCardNumber256] [varbinary] (40) NULL,
[CheckingAccountNumber256] [varbinary] (40) NULL,
[PaymentMethodStamp] [int] NULL,
[TransPending_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Track].[ProcessApproved] ADD CONSTRAINT [PK_TrackProcessApproved] PRIMARY KEY CLUSTERED  ([ProcessApproved_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ProcessApproved_PaymentMethodStamp] ON [Track].[ProcessApproved] ([PaymentMethodStamp]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [Track].[ProcessApproved] WITH NOCHECK ADD CONSTRAINT [FK_ProcessApproved_tblCompany] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Track].[ProcessApproved] WITH NOCHECK ADD CONSTRAINT [FK_ProcessApproved_PaymentMethod] FOREIGN KEY ([PaymentMethod_id]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Track approved transaction', 'SCHEMA', N'Track', 'TABLE', N'ProcessApproved', NULL, NULL
GO
