CREATE TABLE [Data].[Cart]
(
[Cart_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[Customer_id] [int] NULL,
[TransPass_id] [int] NULL,
[TransPreAuth_id] [int] NULL,
[TransPending_id] [int] NULL,
[StartDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_Cart_StartDate] DEFAULT (sysdatetime()),
[CheckoutDate] [datetime2] (0) NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[TotalProducts] [decimal] (10, 2) NOT NULL,
[TotalShipping] [decimal] (10, 2) NOT NULL,
[Total] AS ([TotalProducts]+[TotalShipping]) PERSISTED,
[Installments] [tinyint] NOT NULL,
[RecepientName] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[RecepientPhone] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[RecepientMail] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Comment] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[ReferenceNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[GeogCoord] [sys].[geography] NULL,
[Identifier] [uniqueidentifier] NOT NULL,
[UISetting] [varchar] (1000) COLLATE Hebrew_CI_AS NULL,
[MerchantSetShop_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[Cart] ADD CONSTRAINT [PK_Cart] PRIMARY KEY CLUSTERED  ([Cart_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[Cart] ADD CONSTRAINT [FK_Cart_Currency] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Data].[Cart] ADD CONSTRAINT [FK_Cart_Customer_CustomerID] FOREIGN KEY ([Customer_id]) REFERENCES [Data].[Customer] ([Customer_id])
GO
ALTER TABLE [Data].[Cart] ADD CONSTRAINT [FK_Cart_tblCompany_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Data].[Cart] ADD CONSTRAINT [FK_Cart_tblCompanyTransPass_TransPassID] FOREIGN KEY ([TransPass_id]) REFERENCES [dbo].[tblCompanyTransPass] ([ID])
GO
ALTER TABLE [Data].[Cart] ADD CONSTRAINT [FK_Cart_tblCompanyTransPending_TransPendingID] FOREIGN KEY ([TransPending_id]) REFERENCES [dbo].[tblCompanyTransPending] ([companyTransPending_id])
GO
ALTER TABLE [Data].[Cart] ADD CONSTRAINT [FK_Cart_tblCompanyTransApproval_TransApprovalID] FOREIGN KEY ([TransPreAuth_id]) REFERENCES [dbo].[tblCompanyTransApproval] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Shopping cart', 'SCHEMA', N'Data', 'TABLE', N'Cart', NULL, NULL
GO
