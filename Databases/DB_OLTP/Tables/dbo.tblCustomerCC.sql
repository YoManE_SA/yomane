CREATE TABLE [dbo].[tblCustomerCC]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CustomerID] [int] NOT NULL CONSTRAINT [DF_tblCustomerCC_CustomerID] DEFAULT ((0)),
[CCard_Number256] [varbinary] (200) NULL,
[CCard_ExpMM] [char] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerCC_CCard_ExpMM] DEFAULT (''),
[CCard_ExpYY] [char] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerCC_CCard_ExpYY] DEFAULT (''),
[CCard_TypeID] [int] NOT NULL CONSTRAINT [DF_tblCustomerCC_CCard_TypeID] DEFAULT ((0)),
[CCard_Cui] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerCC_CCard_Cui] DEFAULT (''),
[CH_FullName] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerCC_CH_FullName] DEFAULT (''),
[CH_PersonalNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerCC_CH_PersonalNumber] DEFAULT (''),
[CH_PhoneNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerCC_CH_PhoneNumber] DEFAULT (''),
[CH_Email] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerCC_CH_Email] DEFAULT (''),
[Billing_Street] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerCC_Billing_Street] DEFAULT (''),
[Billing_City] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerCC_Billing_City] DEFAULT (''),
[Billing_State] [int] NOT NULL CONSTRAINT [DF_tblCustomerCC_Billing_State] DEFAULT ((0)),
[Billing_Country] [int] NOT NULL CONSTRAINT [DF_tblCustomerCC_Billing_Country] DEFAULT ((0)),
[Billing_Zipcode] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerCC_Billing_Zipcode] DEFAULT (''),
[IsRemote] [bit] NOT NULL CONSTRAINT [DF_tblCustomerCC_IsRemote] DEFAULT ((0)),
[IsBlocked] [bit] NOT NULL CONSTRAINT [DF_tblCustomerCC_IsBlocked] DEFAULT ((0)),
[IsShow] [bit] NOT NULL CONSTRAINT [DF_tblCustomerCC_IsShow] DEFAULT ((1)),
[PaymentMethod] [smallint] NULL,
[CCard_Last4] [nvarchar] (4) COLLATE Hebrew_CI_AS NULL,
[BINCountry] [char] (2) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgCustomerCcAddSetLast4AndBinCountry] ON [dbo].[tblCustomerCC] AFTER INSERT AS
UPDATE
	tblCustomerCC
SET
	CCard_Last4 = CASE WHEN Len(IsNull(CCard_Last4, ''))<4 THEN Right(dbo.GetDecrypted256(CCard_Number256), 4) ELSE CCard_Last4 END,
	BinCountry = CASE WHEN Len(IsNull(BinCountry, ''))<2 THEN (SELECT TOP 1 isoCode FROM tblCreditCardBIN WHERE BIN = Left(Replace(dbo.GetDecrypted256(CCard_Number256), ' ', ''), 6)) ELSE BinCountry END
WHERE
	ID IN (SELECT ID FROM Inserted);
GO
ALTER TABLE [dbo].[tblCustomerCC] ADD CONSTRAINT [PK_tblCustomerCC] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCustomerCC_CustomerID] ON [dbo].[tblCustomerCC] ([CustomerID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCustomerCC] ADD CONSTRAINT [FK_tblCustomerCC_CustomerID] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[tblCustomer] ([ID])
GO
ALTER TABLE [dbo].[tblCustomerCC] ADD CONSTRAINT [FK_tblCustomerCC_PaymentMethod_PaymentMethod] FOREIGN KEY ([PaymentMethod]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Replaced by [Data].[CustomerPaymentMethod]', 'SCHEMA', N'dbo', 'TABLE', N'tblCustomerCC', NULL, NULL
GO
