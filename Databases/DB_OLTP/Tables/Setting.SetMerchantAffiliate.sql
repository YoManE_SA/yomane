CREATE TABLE [Setting].[SetMerchantAffiliate]
(
[SetMerchantAffiliate_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[Affiliate_id] [int] NOT NULL,
[UserID] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[ConfigurationValues] [nvarchar] (1000) COLLATE Hebrew_CI_AS NULL,
[FeesReducedPercentage] [decimal] (4, 2) NULL,
[SyncDateTime] [datetime2] (2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantAffiliate] ADD CONSTRAINT [PK_SetMerchantAffiliate] PRIMARY KEY CLUSTERED  ([SetMerchantAffiliate_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantAffiliate] ADD CONSTRAINT [FK_SetMerchantAffiliate_tblAffiliates_Affiliate_id] FOREIGN KEY ([Affiliate_id]) REFERENCES [dbo].[tblAffiliates] ([affiliates_id])
GO
ALTER TABLE [Setting].[SetMerchantAffiliate] ADD CONSTRAINT [FK_SetMerchantAffiliate_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
