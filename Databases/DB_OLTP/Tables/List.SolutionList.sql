CREATE TABLE [List].[SolutionList]
(
[SolutionList_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[SolutionList] ADD CONSTRAINT [PK_SolutionList] PRIMARY KEY CLUSTERED  ([SolutionList_id]) ON [PRIMARY]
GO
