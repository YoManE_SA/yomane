CREATE TABLE [dbo].[tblDebitCompanyLoginData]
(
[debitCompanyLoginData_id] [int] NOT NULL IDENTITY(1, 1),
[debitCompany_id] [int] NOT NULL CONSTRAINT [DF_tblDebitCompanyLoginData_debitCompany_id] DEFAULT ((0)),
[username] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyLoginData_username] DEFAULT (''),
[password] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyLoginData_password] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDebitCompanyLoginData] ADD CONSTRAINT [PK_tblDebitCompanyLoginData] PRIMARY KEY CLUSTERED  ([debitCompanyLoginData_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[Not used]', 'SCHEMA', N'dbo', 'TABLE', N'tblDebitCompanyLoginData', NULL, NULL
GO
