CREATE TABLE [Data].[Product]
(
[Product_id] [int] NOT NULL IDENTITY(1, 1),
[ProductType_id] [tinyint] NOT NULL,
[Merchant_id] [int] NOT NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[Price] [decimal] (10, 2) NOT NULL,
[TransType] [tinyint] NOT NULL,
[Installments] [tinyint] NOT NULL,
[RecurringString] [varchar] (30) COLLATE Hebrew_CI_AS NULL,
[QtyStart] [smallint] NOT NULL,
[QtyEnd] [smallint] NOT NULL,
[QtyStep] [smallint] NOT NULL,
[QtyAvailable] [smallint] NULL,
[SKU] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[SortOrder] [tinyint] NULL,
[isActive] [bit] NOT NULL,
[isDynamicPrice] [bit] NOT NULL,
[ImageFileName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[DownloadFileName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[OLDProductID] [int] NULL,
[MerchantSetShop_id] [int] NOT NULL CONSTRAINT [DF_Product_MerchantSetShop] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [Data].[Product] ADD CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED  ([Product_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[Product] ADD CONSTRAINT [FK_Product_Currency] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Data].[Product] ADD CONSTRAINT [FK_Product_tblCompany_CompanyID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Data].[Product] ADD CONSTRAINT [FK_Product_MerchantSetShop_MerchantSetShopID] FOREIGN KEY ([MerchantSetShop_id]) REFERENCES [Setting].[MerchantSetShop] ([MerchantSetShop_id])
GO
ALTER TABLE [Data].[Product] ADD CONSTRAINT [FK_Product_ProductTypeID] FOREIGN KEY ([ProductType_id]) REFERENCES [List].[ProductType] ([ProductType_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s products', 'SCHEMA', N'Data', 'TABLE', N'Product', NULL, NULL
GO
