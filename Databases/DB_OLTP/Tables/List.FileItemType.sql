CREATE TABLE [List].[FileItemType]
(
[FileItemType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[FileItemType] ADD CONSTRAINT [PK_FileItemType] PRIMARY KEY CLUSTERED  ([FileItemType_id]) ON [PRIMARY]
GO
