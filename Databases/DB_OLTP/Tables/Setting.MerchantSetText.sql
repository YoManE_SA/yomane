CREATE TABLE [Setting].[MerchantSetText]
(
[MerchantSetText_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[Language_id] [tinyint] NOT NULL,
[SolutionList_id] [tinyint] NOT NULL,
[TextKey] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[TextValue] [nvarchar] (2500) COLLATE Hebrew_CI_AS NULL,
[IsDefault] [bit] NOT NULL CONSTRAINT [DF_MerchantSetText_IsDefault] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[MerchantSetText] ADD CONSTRAINT [PK_MerchantSetText] PRIMARY KEY CLUSTERED  ([MerchantSetText_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[MerchantSetText] ADD CONSTRAINT [FK_MerchantSetText_LanguageList_Language_id] FOREIGN KEY ([Language_id]) REFERENCES [List].[LanguageList] ([Language_id]) ON DELETE CASCADE
GO
ALTER TABLE [Setting].[MerchantSetText] ADD CONSTRAINT [FK_MerchantSetText_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [Setting].[MerchantSetText] ADD CONSTRAINT [FK_MerchantSetText_SolutionList_SolutionList_id] FOREIGN KEY ([SolutionList_id]) REFERENCES [List].[SolutionList] ([SolutionList_id]) ON DELETE CASCADE
GO
