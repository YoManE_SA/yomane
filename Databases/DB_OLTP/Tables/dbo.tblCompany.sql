CREATE TABLE [dbo].[tblCompany]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CustomerNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CustomerNumber] DEFAULT (''),
[FirstName] [nvarchar] (150) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_FirstName] DEFAULT (''),
[LastName] [nvarchar] (150) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_LastName] DEFAULT (''),
[Street] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_Street] DEFAULT (''),
[City] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_City] DEFAULT (''),
[ZIP] [nvarchar] (25) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_ZIP] DEFAULT (''),
[State] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_State] DEFAULT (''),
[Country] [int] NULL,
[Phone] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_Phone] DEFAULT (''),
[cellular] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_cellular] DEFAULT (''),
[IDnumber] [nvarchar] (15) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_IDnumber] DEFAULT (''),
[CompanyName] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CompanyName] DEFAULT (''),
[CompanyLegalName] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CompanyLegalName] DEFAULT (''),
[CompanyLegalNumber] [nvarchar] (15) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CompanyLegalNumber] DEFAULT (''),
[CompanyStreet] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CompanyStreet] DEFAULT (''),
[CompanyCity] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CompanyCity] DEFAULT (''),
[CompanyZIP] [nvarchar] (25) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CompanyZIP] DEFAULT (''),
[CompanyState] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CompanyState] DEFAULT (''),
[CompanyCountry] [int] NULL,
[CompanyPhone] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CompanyPhone] DEFAULT (''),
[CompanyFax] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CompanyFax] DEFAULT (''),
[CompanyIndustry_id] [smallint] NOT NULL CONSTRAINT [DF_tblCompany_CompanyIndustry_id] DEFAULT ((0)),
[BillingFor] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_BillingFor] DEFAULT (''),
[comment] [nvarchar] (150) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_comment] DEFAULT (''),
[comment_misc] [nvarchar] (4000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_comment_misc] DEFAULT (''),
[UserName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_UserName] DEFAULT (''),
[Mail] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_Mail] DEFAULT (''),
[URL] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_URL] DEFAULT (''),
[merchantSupportEmail] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_merchantSupportEmail] DEFAULT (''),
[merchantSupportPhoneNum] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_merchantSupportPhoneNum] DEFAULT (''),
[merchantOpenningDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblCompany_merchantOpenningDate] DEFAULT (''),
[merchantClosingDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblCompany_merchantClosingDate] DEFAULT (''),
[Blocked] [bit] NOT NULL CONSTRAINT [DF_tblCompany_Blocked] DEFAULT ((1)),
[Closed] [bit] NOT NULL CONSTRAINT [DF_tblCompany_Closed] DEFAULT ((0)),
[IpOnReg] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_IpOnReg] DEFAULT ('0.0.0.0'),
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompany_InsertDate] DEFAULT (getdate()),
[transCcStorageShekel] [money] NOT NULL CONSTRAINT [DF_tblCompany_transCcStorageShekel] DEFAULT ((0)),
[transCcStorageDollar] [money] NOT NULL CONSTRAINT [DF_tblCompany_transCcStorageDollar] DEFAULT ((0)),
[MakePaymentsFeeShekel] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_MakePaymentsFeeShekel] DEFAULT ((0)),
[MakePaymentsFeeDollar] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_MakePaymentsFeeDollar] DEFAULT ((0)),
[MinimumPayout] [money] NOT NULL CONSTRAINT [DF_tblCompany_MinimumPayout] DEFAULT ((0)),
[ChargeLimit_Shekel] [money] NOT NULL CONSTRAINT [DF_tblCompany_ChargeLimit_Shekel] DEFAULT ((50000.00)),
[ChargeLimit_Dollar] [money] NOT NULL CONSTRAINT [DF_tblCompany_ChargeLimit_Dollar] DEFAULT ((50000.00)),
[PaymentReceiveCurrency] [int] NULL,
[PaymentMethod] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentMethod] DEFAULT (''),
[PaymentPayeeName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentPayeeName] DEFAULT (''),
[PaymentBank] [int] NULL,
[PaymentBranch] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentBranch] DEFAULT (''),
[PaymentAccount] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAccount] DEFAULT (''),
[PaymentAbroadAccountName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadAccountName] DEFAULT (''),
[PaymentAbroadAccountNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadAccountNumber] DEFAULT (''),
[PaymentAbroadBankName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankName] DEFAULT (''),
[PaymentAbroadBankAddress] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddress] DEFAULT (''),
[PaymentAbroadSwiftNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadSwiftNumber] DEFAULT (''),
[PaymentAbroadIBAN] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadIBAN] DEFAULT (''),
[PaymentAbroadAccountName2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadAccountName2] DEFAULT (''),
[PaymentAbroadAccountNumber2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadAccountNumber2] DEFAULT (''),
[PaymentAbroadBankName2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankName2] DEFAULT (''),
[PaymentAbroadBankAddress2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddress2] DEFAULT (''),
[PaymentAbroadSwiftNumber2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadSwiftNumber2] DEFAULT (''),
[PaymentAbroadIBAN2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadIBAN2] DEFAULT (''),
[PaymentAbroadABA2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadABA2] DEFAULT (''),
[PaymentAbroadSortCode2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadSortCode2] DEFAULT (''),
[CCardExpMM] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CCardExpMM] DEFAULT (''),
[CCardExpYY] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CCardExpYY] DEFAULT (''),
[CCardHolderName] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CCardHolderName] DEFAULT (''),
[CCardCUI] [nvarchar] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CCardCUI] DEFAULT (''),
[BillingCompanys_id] [int] NULL,
[isChargeVAT] [bit] NOT NULL CONSTRAINT [DF_tblCompany_isChargeVAT] DEFAULT ((0)),
[IsSystemPayEcheck] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsSystemPayEcheck] DEFAULT ((0)),
[IsSystemPay] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsSystemPay] DEFAULT ((0)),
[IsSystemPayCVV2] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsSystemPayCVV2] DEFAULT ((1)),
[IsSystemPayPersonalNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsSystemPayPersonalNumber] DEFAULT ((1)),
[IsSystemPayPhoneNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsSystemPayPhoneNumber] DEFAULT ((1)),
[IsSystemPayEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsSystemPayEmail] DEFAULT ((1)),
[IsRemoteChargeEcheck] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRemoteChargeEcheck] DEFAULT ((0)),
[IsRemoteChargeEchCVV2] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRemoteChargeEchCVV2] DEFAULT ((1)),
[IsRemoteChargeEchPersonalNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRemoteChargeEchPersonalNumber] DEFAULT ((1)),
[IsRemoteChargeEchPhoneNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRemoteChargeEchPhoneNumber] DEFAULT ((1)),
[IsRemoteChargeEchEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRemoteChargeEchEmail] DEFAULT ((1)),
[IsRemoteCharge] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRemoteCharge] DEFAULT ((0)),
[IsRemoteChargeCVV2] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRemoteChargeCVV2] DEFAULT ((1)),
[IsRemoteChargePersonalNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRemoteChargePersonalNumber] DEFAULT ((1)),
[IsRemoteChargePhoneNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRemoteChargePhoneNumber] DEFAULT ((1)),
[IsRemoteChargeEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRemoteChargeEmail] DEFAULT ((1)),
[IsCustomerPurchaseEcheck] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsCustomerPurchaseEcheck] DEFAULT ((0)),
[IsCustomerPurchase] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsCustomerPurchase] DEFAULT ((0)),
[IsCustomerPurchaseCVV2] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsCustomerPurchaseCVV2] DEFAULT ((1)),
[IsCustomerPurchasePersonalNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsCustomerPurchasePersonalNumber] DEFAULT ((1)),
[IsCustomerPurchasePhoneNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsCustomerPurchasePhoneNumber] DEFAULT ((1)),
[IsCustomerPurchaseEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsCustomerPurchaseEmail] DEFAULT ((1)),
[IsCustomerPurchasePayerID] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsCustomerPurchasePayerID] DEFAULT ((0)),
[IsWindowChargeEwallet] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsWindowChargeEwallet] DEFAULT ((0)),
[IsWindowChargeEwalletMust] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsWindowChargeEwalletMust] DEFAULT ((0)),
[IsWindowChargeHiddenLogin] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsWindowChargeHiddenLogin] DEFAULT ((0)),
[CustomerPurchasePayerIDText] [nvarchar] (150) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CustomerPurchasePayerIDText] DEFAULT (''),
[IsPublicPay] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsPublicPay] DEFAULT ((0)),
[IsPublicPayCVV2] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsPublicPayCVV2] DEFAULT ((1)),
[IsPublicPayPersonalNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsPublicPayPersonalNumber] DEFAULT ((1)),
[IsPublicPayPhoneNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsPublicPayPhoneNumber] DEFAULT ((1)),
[IsPublicPayEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsPublicPayEmail] DEFAULT ((1)),
[IsMultiChargeProtection] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsMultiChargeProtection] DEFAULT ((1)),
[IsBillingAddressMust] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsBillingAddressMust] DEFAULT ((0)),
[IsCVV2] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsCVV2] DEFAULT ((1)),
[IsPersonalNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsPersonalNumber] DEFAULT ((1)),
[IsPhoneNumber] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsPhoneNumber] DEFAULT ((1)),
[IsEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsEmail] DEFAULT ((1)),
[IsRefund] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRefund] DEFAULT ((0)),
[IsAskRefund] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAskRefund] DEFAULT ((0)),
[IsAllowRecurring] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAllowRecurring] DEFAULT ((0)),
[IsConfirmation] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsConfirmation] DEFAULT ((0)),
[IsApprovalOnly] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsApprovalOnly] DEFAULT ((0)),
[IsAutoPersonalSignup] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAutoPersonalSignup] DEFAULT ((0)),
[IsCcStorage] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsCcStorage] DEFAULT ((0)),
[IsCcStorageCharge] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsCcStorageCharge] DEFAULT ((0)),
[IsTerminalProbLog] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsTerminalProbLog] DEFAULT ((0)),
[IsConnectionProbLog] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsConnectionProbLog] DEFAULT ((0)),
[IsTransLookup] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsTransLookup] DEFAULT ((0)),
[IsSendUserConfirmationEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsSendUserConfirmationEmail] DEFAULT ((0)),
[IsAllowMakePayments] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAllowMakePayments] DEFAULT ((0)),
[IsShowSensitiveData] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsShowSensitiveData] DEFAULT ((0)),
[IsUseFraudDetection_MaxMind] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsUseFraudDetection_MaxMind] DEFAULT ((0)),
[IsUseFraudDetection_Local] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsUseFraudDetection_Local] DEFAULT ((0)),
[MaxMind_MinScore] [decimal] (5, 2) NOT NULL CONSTRAINT [DF_tblCompany_MaxMind_MinScore] DEFAULT ((5)),
[lastInsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompany_lastInsertDate] DEFAULT (getdate()),
[CountAdminView] [int] NOT NULL CONSTRAINT [DF_tblCompany_CountAdminView] DEFAULT ((0)),
[PayingDaysMargin] [tinyint] NOT NULL CONSTRAINT [DF_tblCompany_PayingDaysMargin] DEFAULT ((0)),
[payingDates1] [nvarchar] (6) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_payingDates1] DEFAULT (''),
[payingDates2] [nvarchar] (6) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_payingDates2] DEFAULT (''),
[payingDates3] [nvarchar] (6) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_payingDates3] DEFAULT (''),
[referralID] [int] NOT NULL CONSTRAINT [DF_tblCompany_referralID] DEFAULT ((0)),
[referralName] [nvarchar] (150) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_referralName] DEFAULT (''),
[languagePreference] [nvarchar] (3) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_languagePreference] DEFAULT ('eng'),
[isNetpayTerminal] [bit] NOT NULL CONSTRAINT [DF_tblCompany_isNetpayTerminal] DEFAULT ((1)),
[PslWalletId] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PslWalletId] DEFAULT (''),
[PslWalletCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PslWalletCode] DEFAULT (''),
[IsAllowBatchFiles] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAllowBatchFiles] DEFAULT ((0)),
[IsAllow3DTrans] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAllow3DTrans] DEFAULT ((0)),
[IsAllowBasket] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAllowBasket] DEFAULT ((0)),
[isCcTransCostFailFee] [bit] NOT NULL CONSTRAINT [DF_tblCompany_isCcTransCostFailFee] DEFAULT ((0)),
[isAllowFreeMail] [bit] NOT NULL CONSTRAINT [DF_tblCompany_isAllowFreeMail] DEFAULT ((0)),
[descriptor] [nvarchar] (150) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_descriptor] DEFAULT (''),
[countryBlackList] [varchar] (720) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_countryBlackList] DEFAULT (''),
[dailyCcMaxFailCount] [tinyint] NOT NULL CONSTRAINT [DF_tblCompany_dailyCcMaxFailCount] DEFAULT ((0)),
[debitCompanyExID] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_debitCompanyExID] DEFAULT (''),
[AffiliateFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_AffiliateFee] DEFAULT ((0)),
[SecurityKey] [nvarchar] (12) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_SecurityKey] DEFAULT (''),
[SubAffiliateID] [int] NOT NULL CONSTRAINT [DF_tblCompany_SubAffiliateID] DEFAULT ((0)),
[SubAffiliateFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_SubAffiliateFee] DEFAULT ((0)),
[AffiliateFeeView] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_AffiliateFeeView] DEFAULT ((100)),
[SubAffiliateFeeView] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_SubAffiliateFeeView] DEFAULT ((100)),
[PasswordUpdate] [datetime] NOT NULL CONSTRAINT [DF_tblCompany_PasswordUpdate] DEFAULT (getdate()),
[LastLoginCount] [int] NOT NULL CONSTRAINT [DF_tblCompany_LastLoginCount] DEFAULT ((0)),
[transCcStorage] [money] NOT NULL CONSTRAINT [DF_tblCompany_transCcStorage] DEFAULT ((0)),
[careOfAdminUser] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_careOfAdminUser] DEFAULT (''),
[SecurityDeposit] [money] NOT NULL CONSTRAINT [DF_tblCompany_SecurityDeposit] DEFAULT ((10)),
[PaymentAbroadABA] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadABA] DEFAULT (''),
[PaymentAbroadSortCode] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadSortCode] DEFAULT (''),
[PaymentAbroadBankAddressSecond] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddressSecond] DEFAULT (''),
[PaymentAbroadBankAddressCity] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddressCity] DEFAULT (''),
[PaymentAbroadBankAddressState] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddressState] DEFAULT (''),
[PaymentAbroadBankAddressZip] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddressZip] DEFAULT (''),
[PaymentAbroadBankAddressCountry] [int] NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddressCountry] DEFAULT ((0)),
[PaymentAbroadBankAddressSecond2] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddressSecond2] DEFAULT (''),
[PaymentAbroadBankAddressCity2] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddressCity2] DEFAULT (''),
[PaymentAbroadBankAddressState2] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddressState2] DEFAULT (''),
[PaymentAbroadBankAddressZip2] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddressZip2] DEFAULT (''),
[PaymentAbroadBankAddressCountry2] [int] NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadBankAddressCountry2] DEFAULT ((0)),
[ParentCompany] [int] NULL,
[IsAllowRecurringFromTransPass] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAllowRecurringFromTransPass] DEFAULT ((0)),
[AllowedAmounts] [nvarchar] (max) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_AllowedAmounts] DEFAULT (''),
[ActiveStatus] [tinyint] NOT NULL CONSTRAINT [DF_tblCompany_ActiveStatus] DEFAULT ((1)),
[ChargebackNotifyMail] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_ChargebackNotifyMail] DEFAULT (''),
[CyclePeriod] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_CyclePeriod] DEFAULT (''),
[PayingDaysMarginInitial] [tinyint] NOT NULL CONSTRAINT [DF_tblCompany_PayingDaysMarginInitial] DEFAULT ((0)),
[RREnable] [bit] NOT NULL CONSTRAINT [DF_tblCompany_RREnable] DEFAULT ((0)),
[RROnlyOnce] [bit] NOT NULL CONSTRAINT [DF_tblCompany_RROnlyOnce] DEFAULT ((0)),
[MakePaymentsFixFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_MakePaymentsFixFee] DEFAULT ((0)),
[ReportMail] [nvarchar] (512) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblCompany_ReportMail] DEFAULT (''),
[ReportMailOptions] [tinyint] NULL CONSTRAINT [DF_tblCompany_ReportMailOptions] DEFAULT ((0)),
[HandlingFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_HandlingFee] DEFAULT ((0)),
[IsUsePPWList] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsUsePPWList] DEFAULT ((0)),
[SecurityPeriod] [smallint] NOT NULL CONSTRAINT [DF_tblCompany_SecurityPeriod] DEFAULT ((6)),
[WalletUB2MBFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_WalletUB2MBFixedFee] DEFAULT ((0)),
[WalletUB2MBPrcFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_WalletUB2MBPrcFee] DEFAULT ((0)),
[UserNameAlt] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_UserNameAlt] DEFAULT (''),
[HashKey] [nvarchar] (32) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_HashKey] DEFAULT (''),
[MerchantIdentityID] [int] NOT NULL CONSTRAINT [DF_tblCompany_MerchantIdentityID] DEFAULT ((0)),
[isHidePayNeed] [bit] NOT NULL CONSTRAINT [DF_tblCompany_isHidePayNeed] DEFAULT ((1)),
[IsAllowRemotePull] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAllowRemotePull] DEFAULT ((0)),
[RemotePullIPs] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_RemotePullIPs] DEFAULT (''),
[CCardNumber256] [varbinary] (200) NULL,
[PayPercent] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompany_PayPercent] DEFAULT ((100)),
[RRAutoRet] [bit] NULL CONSTRAINT [DF_tblCompany_RRAutoRet] DEFAULT ((0)),
[PaymentAbroadSepaBic] [varchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadSepaBic] DEFAULT (''),
[PaymentAbroadSepaBic2] [varchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PaymentAbroadSepaBic2] DEFAULT (''),
[IsUsingNewTerminal] [bit] NULL CONSTRAINT [DF_tblCompany_IsUsingNewTerminal] DEFAULT ((0)),
[IsBillingCityOptional] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsBillingCityOptional] DEFAULT ((0)),
[ZecureUsername] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_ZecureUsername] DEFAULT (''),
[ZecurePassword] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_ZecurePassword] DEFAULT (''),
[ZecureAccount] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_ZecureAccount] DEFAULT (''),
[IsAskRefundRemote] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAskRefundRemote] DEFAULT ((0)),
[RemoteRefundRequestIPs] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_RemoteRefundRequestIPs] DEFAULT (''),
[RiskScore] [real] NOT NULL CONSTRAINT [DF_tblCompany_RiskScore] DEFAULT ((-1)),
[WalletIsEnable] [bit] NOT NULL CONSTRAINT [DF_tblCompany_WalletIsEnable] DEFAULT ((1)),
[GroupID] [int] NULL,
[DailyVolumeLimit] [money] NULL,
[IsSkipFraudDetectionInVirtualTerminal] [bit] NULL,
[RRKeepAmount] [money] NOT NULL CONSTRAINT [DF_tblCompany_RRKeepAmount] DEFAULT ((0)),
[RRKeepCurrency] [tinyint] NOT NULL CONSTRAINT [DF_tblCompany_RRKeepCurrency] DEFAULT ((0)),
[RRComment] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[IsCcWhiteListEnabled] [bit] NULL,
[RRState] [tinyint] NOT NULL CONSTRAINT [DF_tblCompany_RRState] DEFAULT ((0)),
[RRPayID] [int] NOT NULL CONSTRAINT [DF_tblCompany_RRPayID] DEFAULT ((0)),
[countryWhiteList] [varchar] (720) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_countryWhiteList] DEFAULT (''),
[CFF_CurAmount] [money] NOT NULL CONSTRAINT [DF_tblCompany_CFF_CurAmount] DEFAULT ((0)),
[CFF_Currency] [int] NULL,
[CFF_ResetDate] [date] NOT NULL CONSTRAINT [DF_tblCompany_CFF_ResetDate] DEFAULT (getdate()),
[IsBillingAddressMustIDebit] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsBillingAddressMustIDebit] DEFAULT ((0)),
[IPWhiteList] [varchar] (150) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_IPWhiteList] DEFAULT (''),
[AutoCaptureHours] [int] NOT NULL CONSTRAINT [DF_tblCompany_AutoCaptureHours] DEFAULT ((0)),
[ContactName] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[ContactMail] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[AlertEmail] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[IsAnnualFee] [bit] NULL,
[AnnualFeeDate] [datetime] NULL,
[IsAnnualFeeLowRisk] [bit] NULL,
[AnnualFeeLowRiskDate] [datetime] NULL,
[IsAnnualFeeHighRisk] [bit] NULL,
[AnnualFeeHighRiskDate] [datetime] NULL,
[AffiliateID] [int] NOT NULL CONSTRAINT [DF_tblCompany_AffiliateID] DEFAULT ((0)),
[MonthlyFeeEuroDate] [datetime] NULL,
[IsMonthlyFeeEuro] [bit] NULL,
[MonthlyFeeDollarDate] [datetime] NULL,
[IsMonthlyFeeDollar] [bit] NULL,
[MultiChargeProtectionMins] [smallint] NULL CONSTRAINT [DF_tblCompany_MultiChargeProtectionMins] DEFAULT ((5)),
[MonthlyFeeBankHighDate] [datetime] NULL,
[IsMonthlyFeeBankHigh] [bit] NULL,
[MonthlyFeeBankLowDate] [datetime] NULL,
[IsMonthlyFeeBankLow] [bit] NULL,
[ForceRecurringMD5] [bit] NULL,
[ForceCCStorageMD5] [bit] NULL,
[IsAnnualFee3dSecure] [bit] NULL,
[AnnualFee3dSecureDate] [datetime] NULL,
[RiskMailCcIsEnabled] [bit] NULL,
[RiskMailCcIsTransactionDeclined] [bit] NULL,
[RiskMailCcIsMailBlacklisted] [bit] NULL,
[RiskMailCcIsCardBlacklisted] [bit] NULL,
[RiskMailCcIsCardsBlacklisted] [bit] NULL,
[RiskMailCcIsMerchantNotified] [bit] NULL,
[RiskMailCcLimit] [int] NOT NULL CONSTRAINT [DF_tblCompany_RiskMailCcLimit] DEFAULT ((3)),
[RiskMailCcNotifyEmail] [varchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_RiskMailCcNotifyEmail] DEFAULT (''),
[IsMerchantNotifiedOnPass] [bit] NULL,
[PassNotifyEmail] [varchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompany_PassNotifyEmail] DEFAULT (''),
[IsAnnualFeeRegistration] [bit] NULL,
[AnnualFeeRegistrationDate] [datetime] NULL,
[PreferredWireType] [tinyint] NULL,
[RecurringLimitYears] [int] NOT NULL CONSTRAINT [DF_tblCompany_RecurringLimitYears] DEFAULT ((3)),
[RecurringLimitCharges] [int] NOT NULL CONSTRAINT [DF_tblCompany_RecurringLimitCharges] DEFAULT ((400)),
[RecurringLimitStages] [int] NOT NULL CONSTRAINT [DF_tblCompany_RecurringLimitStages] DEFAULT ((4)),
[MerchantLinkName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[IsAllowSilentPostCcDetails] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsAllowSilentPostCcDetails] DEFAULT ((1)),
[IsInterestedInNewsletter] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsInterestedInNewsletter] DEFAULT ((1)),
[IsRequiredClientIP] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsRequiredClientIP] DEFAULT ((0)),
[IsMerchantNotifiedOnFail] [bit] NOT NULL CONSTRAINT [DF_tblCompany_IsMerchantNotifiedOnFail] DEFAULT ((0)),
[MerchantDepartment_id] [tinyint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[trCompany_MerchantActivity_InsDel] ON [dbo].[tblCompany]
    AFTER INSERT, DELETE
AS
		IF @@ROWCOUNT = 0 RETURN;
		SET NOCOUNT ON;
		
		-- Add Merchant to MerchantActivity table
		IF EXISTS( SELECT ID FROM INSERTED )
		BEGIN
			INSERT  INTO [Track].[MerchantActivity]
					( [Merchant_id] )
					SELECT ID FROM INSERTED
		END
		
		
		-- Remove Merchant from MerchantActivity table
		IF EXISTS( SELECT ID FROM DELETED )
		BEGIN
			DELETE  FROM [Track].[MerchantActivity]
			WHERE   [Merchant_id] IN ( SELECT ID FROM DELETED )
		END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgCompanyAddMerchantProcessingData] ON [dbo].[tblCompany] AFTER INSERT, UPDATE AS
INSERT INTO tblMerchantProcessingData
	(MerchantID, MPD_CffCurAmount, MPD_CffResetDate)
SELECT
	ID, CFF_CurAmount, CFF_ResetDate
FROM
	Inserted
WHERE
	ActiveStatus=30 AND ID NOT IN (SELECT MerchantID FROM tblMerchantProcessingData);
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgCompanyAddPeriodicFee] ON [dbo].[tblCompany] AFTER INSERT AS
INSERT INTO tblPeriodicFee(MerchantID, TypeID) SELECT m.ID, ft.ID FROM tblPeriodicFeeType ft CROSS JOIN Inserted m
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgCompanyAddSetCustomerNumber] ON [dbo].[tblCompany] FOR INSERT AS
BEGIN
	DECLARE @nID int, @sNumber nvarchar(50)
	DECLARE curCompany CURSOR FOR SELECT ID FROM Inserted WHERE CustomerNumber=''
	OPEN curCompany
	FETCH NEXT FROM curCompany INTO @nID
	WHILE @@FETCH_STATUS=0
	BEGIN
		EXEC GetNewCustomerNumber @sNumber OUTPUT
		UPDATE tblCompany SET CustomerNumber=@sNumber WHERE ID=@nID
		FETCH NEXT FROM curCompany INTO @nID
	END
	CLOSE curCompany
	DEALLOCATE curCompany
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgCompanyAddSetDefaultBillingCompany] ON [dbo].[tblCompany] AFTER INSERT AS
UPDATE tblCompany SET BillingCompanys_id=(SELECT TOP 1 BillingCompanys_id FROM tblBillingCompanys WHERE IsDefault=1) WHERE ID IN (SELECT ID FROM Inserted)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgCompanyAddSetParentCompany] ON [dbo].[tblCompany] FOR Insert AS
UPDATE tblCompany SET ParentCompany=IsNull((SELECT TOP 1 ID FROM tblParentCompany ORDER BY pc_IsDefault DESC, pc_Code), 0) WHERE ID IN (SELECT ID FROM Inserted) AND ParentCompany=0
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgCompanyUpdateCutRecurringTests] ON [dbo].[tblCompany] FOR UPDATE AS
UPDATE tblRecurringSeries SET rs_Deleted=1 WHERE rs_Company IN (SELECT ID FROM Inserted WHERE ActiveStatus=30) AND rs_Company IN (SELECT ID FROM Deleted WHERE ActiveStatus=20)
GO
DISABLE TRIGGER [dbo].[trgCompanyUpdateCutRecurringTests] ON [dbo].[tblCompany]
GO
ALTER TABLE [dbo].[tblCompany] ADD CONSTRAINT [PK_tblCompany] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompany_ActiveStatus] ON [dbo].[tblCompany] ([ActiveStatus]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblCompany_CustomerNumber] ON [dbo].[tblCompany] ([CustomerNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompany_PayingNoNeed] ON [dbo].[tblCompany] ([isHidePayNeed]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompany_MerchantLinkName] ON [dbo].[tblCompany] ([MerchantLinkName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompany_PayPercent] ON [dbo].[tblCompany] ([PayPercent] DESC, [CompanyName], [SecurityDeposit], [PayingDaysMargin], [PayingDaysMarginInitial]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompany] ADD CONSTRAINT [FK_tblCompany_tblBillingCompanys_BillingCompanys_id] FOREIGN KEY ([BillingCompanys_id]) REFERENCES [dbo].[tblBillingCompanys] ([BillingCompanys_id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompany] ADD CONSTRAINT [FK_tblCompany_tblSystemCurrencies_CFF_Currency] FOREIGN KEY ([CFF_Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
ALTER TABLE [dbo].[tblCompany] ADD CONSTRAINT [FK_tblCompany_CompanyCountry_CountryList_CountryID] FOREIGN KEY ([CompanyCountry]) REFERENCES [List].[CountryList] ([CountryID])
GO
ALTER TABLE [dbo].[tblCompany] ADD CONSTRAINT [FK_tblCompany_Country_CountryList_CountryID] FOREIGN KEY ([Country]) REFERENCES [List].[CountryList] ([CountryID])
GO
ALTER TABLE [dbo].[tblCompany] ADD CONSTRAINT [FK_tblCompany_tblMerchantGroup_GroupID] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[tblMerchantGroup] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompany] ADD CONSTRAINT [FK_tblCompany_MerchantDepartment_MerchantDepartment_id] FOREIGN KEY ([MerchantDepartment_id]) REFERENCES [dbo].[MerchantDepartment] ([MerchantDepartment_id])
GO
ALTER TABLE [dbo].[tblCompany] ADD CONSTRAINT [FK_tblCompany_tblParentCompany_ParentCompany] FOREIGN KEY ([ParentCompany]) REFERENCES [dbo].[tblParentCompany] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompany] ADD CONSTRAINT [FK_tblCompany_tblSystemBankList_PaymentBank] FOREIGN KEY ([PaymentBank]) REFERENCES [dbo].[tblSystemBankList] ([id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompany] ADD CONSTRAINT [FK_tblCompany_tblSystemCurrencies_PaymentReceiveCurrency] FOREIGN KEY ([PaymentReceiveCurrency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant data', 'SCHEMA', N'dbo', 'TABLE', N'tblCompany', NULL, NULL
GO
