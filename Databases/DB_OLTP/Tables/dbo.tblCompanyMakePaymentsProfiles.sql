CREATE TABLE [dbo].[tblCompanyMakePaymentsProfiles]
(
[CompanyMakePaymentsProfiles_id] [int] NOT NULL IDENTITY(101, 1),
[ProfileType] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_ProfileType] DEFAULT ((1)),
[basicInfo_costumerNumber] [nvarchar] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_basicInfo_costumerNumber] DEFAULT (''),
[basicInfo_costumerName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_basicInfo_costumerName] DEFAULT (''),
[basicInfo_contactPersonName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_basicInfo_contactPersonName] DEFAULT (''),
[basicInfo_phoneNumber] [nvarchar] (25) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_basicInfo_phoneNumber] DEFAULT (''),
[basicInfo_faxNumber] [nvarchar] (25) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_basicInfo_faxNumber] DEFAULT (''),
[basicInfo_email] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_basicInfo_email] DEFAULT (''),
[basicInfo_address] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_basicInfo_address] DEFAULT (''),
[basicInfo_comment] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_basicInfo_comment] DEFAULT (''),
[bankIsraelInfo_PayeeName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankIsraelInfo_PayeeName] DEFAULT (''),
[bankIsraelInfo_CompanyLegalNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankIsraelInfo_CompanyLegalNumber] DEFAULT (''),
[bankIsraelInfo_personalIdNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankIsraelInfo_personalIdNumber] DEFAULT (''),
[bankIsraelInfo_bankBranch] [nvarchar] (5) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankIsraelInfo_bankBranch] DEFAULT (''),
[bankIsraelInfo_AccountNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankIsraelInfo_AccountNumber] DEFAULT (''),
[bankIsraelInfo_PaymentMethod] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankIsraelInfo_PaymentMethod] DEFAULT (''),
[bankAbroadAccountName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadAccountName] DEFAULT (''),
[bankAbroadAccountNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadAccountNumber] DEFAULT (''),
[bankAbroadBankName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankName] DEFAULT (''),
[bankAbroadBankAddress] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddress] DEFAULT (''),
[bankAbroadSwiftNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadSwiftNumber] DEFAULT (''),
[bankAbroadIBAN] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadIBAN] DEFAULT (''),
[bankAbroadABA] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadABA] DEFAULT (''),
[bankAbroadSortCode] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadSortCode] DEFAULT (''),
[bankAbroadAccountName2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadAccountName2] DEFAULT (''),
[bankAbroadAccountNumber2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadAccountNumber2] DEFAULT (''),
[bankAbroadBankName2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankName2] DEFAULT (''),
[bankAbroadBankAddress2] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddress2] DEFAULT (''),
[bankAbroadSwiftNumber2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadSwiftNumber2] DEFAULT (''),
[bankAbroadIBAN2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadIBAN2] DEFAULT (''),
[bankAbroadABA2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadABA2] DEFAULT (''),
[bankAbroadSortCode2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadSortCode2] DEFAULT (''),
[bankAbroadBankAddressSecond] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddressSecond] DEFAULT (''),
[bankAbroadBankAddressCity] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddressCity] DEFAULT (''),
[bankAbroadBankAddressState] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddressState] DEFAULT (''),
[bankAbroadBankAddressZip] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddressZip] DEFAULT (''),
[bankAbroadBankAddressCountry] [int] NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddressCountry] DEFAULT ((0)),
[bankAbroadBankAddressSecond2] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddressSecond2] DEFAULT (''),
[bankAbroadBankAddressCity2] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddressCity2] DEFAULT (''),
[bankAbroadBankAddressState2] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddressState2] DEFAULT (''),
[bankAbroadBankAddressZip2] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddressZip2] DEFAULT (''),
[bankAbroadBankAddressCountry2] [int] NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadBankAddressCountry2] DEFAULT ((0)),
[bankAbroadSepaBic] [varchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadSepaBic] DEFAULT (''),
[bankAbroadSepaBic2] [varchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankAbroadSepaBic2] DEFAULT (''),
[bankIsraelInfo_BankCode] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_bankIsraelInfo_BankCode] DEFAULT ('0'),
[company_id] [int] NULL,
[isSystem] [bit] NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsProfiles_isSystem] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyMakePaymentsProfiles] ADD CONSTRAINT [PK_tblCompanyMakePaymentsProfiles] PRIMARY KEY CLUSTERED  ([CompanyMakePaymentsProfiles_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyMakePaymentsProfiles] ADD CONSTRAINT [FK_tblCompanyMakePaymentsProfiles_tblCompany_company_id] FOREIGN KEY ([company_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE SET NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s own beneficiary list for transferring money to', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyMakePaymentsProfiles', NULL, NULL
GO
