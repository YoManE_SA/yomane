CREATE TABLE [dbo].[tblExternalCardCustomerPayment]
(
[ExternalCardCustomerPayment_id] [int] NOT NULL IDENTITY(1, 1),
[ExternalCardCustomer_id] [int] NOT NULL,
[UniqueID] [uniqueidentifier] NOT NULL,
[Amount] [money] NOT NULL,
[Result] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[ResultDescription] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[InsertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblExternalCardCustomerPayment_InsertDate] DEFAULT (sysdatetime())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExternalCardCustomerPayment] ADD CONSTRAINT [PK_tblExternalCardCustomerPayment] PRIMARY KEY CLUSTERED  ([ExternalCardCustomerPayment_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExternalCardCustomerPayment] ADD CONSTRAINT [FK_tblExternalCardCustomerPayment_tblExternalCardCustomer_ExternalCardCustomer_id] FOREIGN KEY ([ExternalCardCustomer_id]) REFERENCES [dbo].[tblExternalCardCustomer] ([ExternalCardCustomer_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblExternalCardCustomerPayment', NULL, NULL
GO
