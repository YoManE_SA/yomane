CREATE TABLE [dbo].[tblCompanyTransPending]
(
[companyTransPending_id] [int] NOT NULL IDENTITY(1, 1),
[companyBatchFiles_id] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_CompanyBatchFiles_id] DEFAULT ((0)),
[transactionSource_id] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_transactionSource_id] DEFAULT ((22)),
[CustomerID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_CustomerID] DEFAULT ((0)),
[FraudDetectionLog_id] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_FraudDetectionLog_id] DEFAULT ((0)),
[insertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_insertDate] DEFAULT (getdate()),
[TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPending_TerminalNumber] DEFAULT (''),
[PaymentMethodID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_PaymentMethodID] DEFAULT ((0)),
[PaymentMethodDisplay] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPending_PaymentMethodDisplay] DEFAULT (''),
[IPAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPending_IPAddress] DEFAULT (''),
[replyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPending_replyCode] DEFAULT (''),
[trans_amount] [money] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_trans_amount] DEFAULT ((0)),
[trans_creditType] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_trans_creditType] DEFAULT ((0)),
[trans_payments] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_trans_payments] DEFAULT ((1)),
[trans_originalID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_trans_originalID] DEFAULT ((0)),
[trans_order] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPending_trans_order] DEFAULT (''),
[DebitReferenceCode] [nvarchar] (40) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPending_DebitReferenceCode] DEFAULT (''),
[DebitApprovalNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPending_DebitApprovalNumber] DEFAULT (''),
[Locked] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_Locked] DEFAULT ((0)),
[ID] AS ([companyTransPending_id]),
[payerIdUsed] [nvarchar] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPending_payerIdUsed] DEFAULT (''),
[OrderNumber] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPending_OrderNumber] DEFAULT (''),
[PaymentMethod] [smallint] NULL,
[trans_currency] [int] NULL,
[Currency] AS ([trans_currency]),
[company_id] [int] NULL,
[CompanyID] AS ([company_id]),
[DebitCompanyID] [int] NULL,
[IDNew] [int] NULL,
[isTestOnly] [bit] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_isTestOnly] DEFAULT ((0)),
[trans_type] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPending_trans_type] DEFAULT ((0)),
[DebitReferenceNum] [varchar] (40) COLLATE Hebrew_CI_AS NULL,
[OriginalTransID] [int] NULL,
[TransSource_id] [tinyint] NULL,
[MobileDevice_id] [int] NULL,
[Comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblCompanyTransPending_Comment] DEFAULT (''),
[SystemText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[PayforText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[CreditCardID] [int] NULL,
[CheckDetailsID] [int] NULL,
[PhoneDetailsID] [int] NULL,
[MerchantProduct_id] [int] NULL,
[PayerInfo_id] [int] NULL,
[TransPayerInfo_id] [int] NULL,
[TransPaymentMethod_id] [int] NULL,
[Is3DSecure] [bit] NULL CONSTRAINT [DF_CompanyTransPending_Is3DSecure] DEFAULT ((0)),
[AcquirerReferenceNum] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[IsCardPresent] [bit] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trTransPending_SetOldTransSource_InsUpd] ON [dbo].[tblCompanyTransPending]
    AFTER INSERT, UPDATE
AS
    IF @@ROWCOUNT = 0 
        RETURN;
    SET NOCOUNT ON;
	
    IF UPDATE(TransSource_id) 
        BEGIN
	
            UPDATE  tblCompanyTransPending
            SET     transactionSource_id = TransSource_id
            WHERE   id IN ( SELECT  ID
                            FROM    INSERTED
                            WHERE   ISNULL(TransSource_id, 0) > 0 );
		
        END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[trTransPending_SetPMDetailID_InsUpd] ON [dbo].[tblCompanyTransPending]
AFTER INSERT, UPDATE
AS

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
	IF UPDATE(PaymentMethodID)
	BEGIN
	
		--Update PhoneDetailsID Column
		UPDATE tblCompanyTransPending SET PhoneDetailsID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID <> 0 AND PaymentMethod >= 200 AND PaymentMethod <= 229);
		
		--Update CheckDetailsID Column
		UPDATE tblCompanyTransPending SET CheckDetailsID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID > 0 AND PaymentMethod >= 100 AND PaymentMethod <= 199); 
		
		--Update CreditCardID Column
		UPDATE tblCompanyTransPending SET CreditCardID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID > 0 AND PaymentMethod >= 20 AND PaymentMethod <= 49); 
		
	END


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/****** Object:  Trigger [trgInsertTransFailUpdTracking] Script Date: 06/01/2008 16:26:55 ******/
CREATE TRIGGER [dbo].[trTransPending_TrackMerchantActivity_Ins] ON [dbo].[tblCompanyTransPending] AFTER INSERT
AS

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
    UPDATE  Track.MerchantActivity
    SET     DateLastTransPending = SYSDATETIME()
    WHERE	Merchant_id IN ( SELECT companyID FROM INSERTED );



GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [PK_tblCompanyTransPending] PRIMARY KEY CLUSTERED  ([companyTransPending_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [tblCompanyTransPending_FraudDetectionLog_id] ON [dbo].[tblCompanyTransPending] ([FraudDetectionLog_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPending] ON [dbo].[tblCompanyTransPending] ([insertDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_tblCheckDetails_CheckDetailsID] FOREIGN KEY ([CheckDetailsID]) REFERENCES [dbo].[tblCheckDetails] ([id])
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_tblCompany_company_id] FOREIGN KEY ([company_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_tblCreditCard_CreditCardID] FOREIGN KEY ([CreditCardID]) REFERENCES [dbo].[tblCreditCard] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_tblDebitCompany_DebitCompanyID] FOREIGN KEY ([DebitCompanyID]) REFERENCES [dbo].[tblDebitCompany] ([DebitCompany_ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_MobileDevice_MobileDevice_id] FOREIGN KEY ([MobileDevice_id]) REFERENCES [dbo].[MobileDevice] ([MobileDevice_id])
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_PaymentMethod_PaymentMethod] FOREIGN KEY ([PaymentMethod]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_PhoneDetail_PhoneDetailsID] FOREIGN KEY ([PhoneDetailsID]) REFERENCES [Trans].[PhoneDetail] ([PhoneDetail_id])
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_tblSystemCurrencies_trans_currency] FOREIGN KEY ([trans_currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_TransPayerInfo] FOREIGN KEY ([TransPayerInfo_id]) REFERENCES [Trans].[TransPayerInfo] ([TransPayerInfo_id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_TransPaymentMethod] FOREIGN KEY ([TransPaymentMethod_id]) REFERENCES [Trans].[TransPaymentMethod] ([TransPaymentMethod_id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPending] ADD CONSTRAINT [FK_tblCompanyTransPending_TransSource_TransSource_id] FOREIGN KEY ([TransSource_id]) REFERENCES [List].[TransSource] ([TransSource_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transactions pending to process', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyTransPending', NULL, NULL
GO
