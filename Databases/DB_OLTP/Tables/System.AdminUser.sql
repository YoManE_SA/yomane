CREATE TABLE [System].[AdminUser]
(
[AdminUser_id] [smallint] NOT NULL IDENTITY(1, 1),
[LoginAccount_id] [int] NULL,
[FullName] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[NotifyEmail] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[NotifyCellPhone] [varchar] (30) COLLATE Hebrew_CI_AS NULL,
[ProfileImageURL] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[StartDate] [date] NULL CONSTRAINT [DF_AdminUser_StartDate] DEFAULT (sysdatetime()),
[IsAdmin] [bit] NOT NULL,
[IsActive] [bit] NULL CONSTRAINT [DF_AdminUser_IsActive] DEFAULT ((1)),
[MultiFactorMode] [varchar] (10) COLLATE Hebrew_CI_AS NULL,
[TimeZoneOffsetUI] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[AdminUser] ADD CONSTRAINT [PK_AdminUser] PRIMARY KEY CLUSTERED  ([AdminUser_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[AdminUser] ADD CONSTRAINT [FK_AdminUser_LoginAccount_LoginAccountID] FOREIGN KEY ([LoginAccount_id]) REFERENCES [Data].[LoginAccount] ([LoginAccount_id])
GO
ALTER TABLE [System].[AdminUser] ADD CONSTRAINT [FK_AdminUser_TimeZone_TimeZoneOffsetUI] FOREIGN KEY ([TimeZoneOffsetUI]) REFERENCES [List].[TimeZone] ([TimeZoneOffsetMinutes])
GO
EXEC sp_addextendedproperty N'MS_Description', N'System admin users', 'SCHEMA', N'System', 'TABLE', N'AdminUser', NULL, NULL
GO
