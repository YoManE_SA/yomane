CREATE TABLE [List].[PaymentMethodGroup]
(
[PaymentMethodGroup_id] [tinyint] NOT NULL,
[pmg_Type] [int] NULL,
[Name] [varchar] (80) COLLATE Hebrew_CI_AS NULL,
[ShortName] [varchar] (2) COLLATE Hebrew_CI_AS NULL,
[SortOrder] [tinyint] NULL,
[IsPopular] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[PaymentMethodGroup] ADD CONSTRAINT [PK_PaymentMethodGroup] PRIMARY KEY CLUSTERED  ([PaymentMethodGroup_id]) ON [PRIMARY]
GO
ALTER TABLE [List].[PaymentMethodGroup] ADD CONSTRAINT [UNC_PaymentMethodGroup_ShortName] UNIQUE NONCLUSTERED  ([ShortName]) ON [PRIMARY]
GO
