CREATE TABLE [dbo].[tblChbPending]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CaseID] [nvarchar] (40) COLLATE Hebrew_CI_AS NULL,
[ReasonCode] [varchar] (10) COLLATE Hebrew_CI_AS NULL,
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblChbPending_InsertDate] DEFAULT (getdate()),
[StoredFileName] [varchar] (100) COLLATE Hebrew_CI_AS NULL,
[DebitRefCode] [varchar] (40) COLLATE Hebrew_CI_AS NULL,
[ApprovalNumber] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[Amount] [money] NULL,
[Currency] [int] NULL,
[TerminalNumber] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[CardNumber256] [varbinary] (200) NULL,
[Installment] [int] NOT NULL,
[IsChargeback] [bit] NULL,
[IsPhotocopy] [bit] NULL,
[TransDate] [datetime] NULL,
[ChargebackDate] [datetime] NULL,
[TransCount] [int] NULL,
[TransID] [int] NULL,
[DeadlineDate] [datetime] NULL,
[DebitCompany_id] [int] NULL,
[AcquirerReferenceNum] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[DebitReferenceNum] [varchar] (40) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblChbPending] ADD CONSTRAINT [PK_tblChbPending] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'CHB recived and waiting for transaction match', 'SCHEMA', N'dbo', 'TABLE', N'tblChbPending', NULL, NULL
GO
