CREATE TABLE [dbo].[tblMerchantBankAccount]
(
[mba_Merchant] [int] NOT NULL,
[mba_Currency] [int] NOT NULL,
[mba_ABA] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_ABA] DEFAULT (''),
[mba_ABA2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_ABA2] DEFAULT (''),
[mba_AccountName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_AccountName] DEFAULT (''),
[mba_AccountName2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_AccountName2] DEFAULT (''),
[mba_AccountNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_AccountNumber] DEFAULT (''),
[mba_AccountNumber2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_AccountNumber2] DEFAULT (''),
[mba_BankAddress] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddress] DEFAULT (''),
[mba_BankAddress2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddress2] DEFAULT (''),
[mba_BankAddressCity] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddressCity] DEFAULT (''),
[mba_BankAddressCity2] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddressCity2] DEFAULT (''),
[mba_BankAddressCountry] [int] NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddressCountry] DEFAULT ((0)),
[mba_BankAddressCountry2] [int] NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddressCountry2] DEFAULT ((0)),
[mba_BankAddressSecond] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddressSecond] DEFAULT (''),
[mba_BankAddressSecond2] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddressSecond2] DEFAULT (''),
[mba_BankAddressState] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddressState] DEFAULT (''),
[mba_BankAddressState2] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddressState2] DEFAULT (''),
[mba_BankAddressZip] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddressZip] DEFAULT (''),
[mba_BankAddressZip2] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankAddressZip2] DEFAULT (''),
[mba_BankName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankName] DEFAULT (''),
[mba_BankName2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_BankName2] DEFAULT (''),
[mba_IBAN] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_IBAN] DEFAULT (''),
[mba_IBAN2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_IBAN2] DEFAULT (''),
[mba_SepaBic] [varchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_SepaBic] DEFAULT (''),
[mba_SepaBic2] [varchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_SepaBic2] DEFAULT (''),
[mba_SortCode] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_SortCode] DEFAULT (''),
[mba_SortCode2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_SortCode2] DEFAULT (''),
[mba_SwiftNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_SwiftNumber] DEFAULT (''),
[mba_SwiftNumber2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblMerchantBankAccount_mba_SwiftNumber2] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMerchantBankAccount] ADD CONSTRAINT [PK_tblMerchantBankAccount] PRIMARY KEY CLUSTERED  ([mba_Merchant], [mba_Currency]) ON [PRIMARY]
GO
