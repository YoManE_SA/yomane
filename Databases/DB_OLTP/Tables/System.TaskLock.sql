CREATE TABLE [System].[TaskLock]
(
[TaskName] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL,
[IsTaksRunning] [bit] NOT NULL CONSTRAINT [DF_TaskLock_IsTaksRunning] DEFAULT ((0)),
[LastRunDate] [datetime2] (2) NOT NULL CONSTRAINT [DF_TaskLock_LastRunDate] DEFAULT (sysdatetime()),
[MachineName] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[TaskLock] ADD CONSTRAINT [PK_TaskLock] PRIMARY KEY CLUSTERED  ([TaskName]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'System tasks running info, used to prevent multiple instances running same task', 'SCHEMA', N'System', 'TABLE', N'TaskLock', NULL, NULL
GO
