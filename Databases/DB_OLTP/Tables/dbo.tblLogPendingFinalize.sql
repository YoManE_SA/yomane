CREATE TABLE [dbo].[tblLogPendingFinalize]
(
[PendingID] [int] NOT NULL,
[FinalizeDate] [datetime] NOT NULL CONSTRAINT [DF_tblLogPendingFinalize_FinalizeDate] DEFAULT (getdate()),
[TransPassID] [int] NULL,
[TransFailID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogPendingFinalize] ADD CONSTRAINT [PK_tblLogPendingFinalize] PRIMARY KEY CLUSTERED  ([PendingID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLogPendingFinalize_TransFailID] ON [dbo].[tblLogPendingFinalize] ([FinalizeDate] DESC) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblLogPendingFinalize_TransFailID_TransPassID] ON [dbo].[tblLogPendingFinalize] ([TransFailID] DESC, [TransPassID] DESC) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblLogPendingFinalize_TransPassID_TransFailID] ON [dbo].[tblLogPendingFinalize] ([TransPassID] DESC, [TransFailID] DESC) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogPendingFinalize] ADD CONSTRAINT [FK_tblLogPendingFinalize_tblCompanyTransFail_TransFailID] FOREIGN KEY ([TransFailID]) REFERENCES [dbo].[tblCompanyTransFail] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblLogPendingFinalize] ADD CONSTRAINT [FK_tblLogPendingFinalize_tblCompanyTransPass_TransPassID] FOREIGN KEY ([TransPassID]) REFERENCES [dbo].[tblCompanyTransPass] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log pass or fail outcome of pending transaction', 'SCHEMA', N'dbo', 'TABLE', N'tblLogPendingFinalize', NULL, NULL
GO
