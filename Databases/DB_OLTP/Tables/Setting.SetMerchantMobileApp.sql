CREATE TABLE [Setting].[SetMerchantMobileApp]
(
[SetMerchantMobileApp_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[IsEnableMobileApp] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsEnableMobileApp] DEFAULT ((0)),
[IsAllowCardNotPresent] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsAllowCardNotPresent] DEFAULT ((0)),
[IsAllowRefund] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsAllowRefund] DEFAULT ((0)),
[IsAllowAuthorization] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsAllowAuthorization] DEFAULT ((0)),
[IsAllowInstallments] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsAllowInstallments] DEFAULT ((0)),
[IsRequireEmail] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsRequireEmail] DEFAULT ((0)),
[IsRequirePersonalNumber] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsRequirePersonalNumber] DEFAULT ((0)),
[IsRequireFullName] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsRequireFullName] DEFAULT ((0)),
[IsRequirePhoneNumber] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsRequirePhoneNumber] DEFAULT ((0)),
[IsRequireCVV] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsRequireCVV] DEFAULT ((0)),
[IsRequireCardholderSignature] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsRequireCardholderSignature] DEFAULT ((0)),
[SyncToken] [uniqueidentifier] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_SyncToken] DEFAULT (newid()),
[ValueAddedTax] [decimal] (4, 2) NULL,
[IsAllowTaxRateChange] [bit] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_IsAllowTaxRateChange] DEFAULT ((1)),
[MaxDeviceCount] [smallint] NOT NULL CONSTRAINT [DF_SetMerchantMobileApp_MaxDeviceCount] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantMobileApp] ADD CONSTRAINT [PK_SetMerchantMobileApp] PRIMARY KEY CLUSTERED  ([SetMerchantMobileApp_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantMobileApp] ADD CONSTRAINT [UIX_SetMerchantMobileAppMerchant_id] UNIQUE NONCLUSTERED  ([Merchant_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantMobileApp] ADD CONSTRAINT [FK_SetMerchantMobileApp_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
