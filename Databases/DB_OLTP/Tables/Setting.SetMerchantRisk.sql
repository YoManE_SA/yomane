CREATE TABLE [Setting].[SetMerchantRisk]
(
[Merchant_id] [int] NOT NULL,
[LimitCcForEmailAllowedCount] [tinyint] NULL,
[IsLimitCcForEmailBlockNewCc] [bit] NULL,
[IsLimitCcForEmailBlockAllCc] [bit] NULL,
[IsLimitCcForEmailBlockEmail] [bit] NULL,
[IsLimitCcForEmailDeclineTrans] [bit] NULL,
[IsLimitCcForEmailNotifyByEmail] [bit] NULL,
[LimitCcForEmailNotifyByEmailList] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[LimitEmailForCcAllowedCount] [tinyint] NULL,
[IsLimitEmailForCcBlockNewEmail] [bit] NULL,
[IsLimitEmailForCcBlockAllEmails] [bit] NULL,
[IsLimitEmailForCcBlockCc] [bit] NULL,
[IsLimitEmailForCcDeclineTrans] [bit] NULL,
[IsLimitEmailForCcNotifyByEmail] [bit] NULL,
[LimitEmailForCcNotifyByEmailList] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[WhitelistCountry] [varchar] (500) COLLATE Hebrew_CI_AS NULL,
[WhitelistState] [varchar] (500) COLLATE Hebrew_CI_AS NULL,
[BlacklistCountry] [varchar] (1000) COLLATE Hebrew_CI_AS NULL,
[BlacklistState] [varchar] (500) COLLATE Hebrew_CI_AS NULL,
[IsEnableCcWhiteList] [bit] NULL,
[AmountAllowedList] [varchar] (1000) COLLATE Hebrew_CI_AS NULL,
[BlacklistIPCountry] [varchar] (500) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantRisk] ADD CONSTRAINT [PK_SetMerchantRisk] PRIMARY KEY CLUSTERED  ([Merchant_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantRisk] ADD CONSTRAINT [FK_SetMerchantRisk_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
