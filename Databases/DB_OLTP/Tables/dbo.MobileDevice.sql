CREATE TABLE [dbo].[MobileDevice]
(
[MobileDevice_id] [int] NOT NULL IDENTITY(1, 1),
[InsertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_MobileDevice_InsertDate] DEFAULT (sysdatetime()),
[DeviceIdentity] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[DeviceUserAgent] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[DevicePhoneNumber] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[PassCode] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[LastLogin] [smalldatetime] NULL,
[IsActivated] [bit] NOT NULL CONSTRAINT [DF_MobileDevice_IsActivated] DEFAULT ((0)),
[IsActive] [bit] NOT NULL CONSTRAINT [DF_MobileDevice_IsActive] DEFAULT ((0)),
[AppVersion] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[SignatureFailCount] [tinyint] NOT NULL CONSTRAINT [DF_MobileDevice_SignatureFailCount] DEFAULT ((0)),
[FriendlyName] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[AccountSubUser_id] [int] NULL,
[Account_id] [int] NOT NULL,
[AppPushToken] [varchar] (255) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MobileDevice] ADD CONSTRAINT [PK_MobileDevice] PRIMARY KEY CLUSTERED  ([MobileDevice_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MobileDevice] ADD CONSTRAINT [FK_MobileDevice_Account_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [dbo].[MobileDevice] ADD CONSTRAINT [FK_MobileDevice_AccountSubUser_AccountSubUserID] FOREIGN KEY ([AccountSubUser_id]) REFERENCES [Data].[AccountSubUser] ([AccountSubUser_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account''s mobile device list', 'SCHEMA', N'dbo', 'TABLE', N'MobileDevice', NULL, NULL
GO
