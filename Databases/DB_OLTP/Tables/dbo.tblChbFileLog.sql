CREATE TABLE [dbo].[tblChbFileLog]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ActionDate] [datetime] NOT NULL CONSTRAINT [DF_tblChbFileLog_ActionDate] DEFAULT (getdate()),
[ActionType] [int] NOT NULL,
[StoredFileName] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[OriginalFileName] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Details] [nvarchar] (1500) COLLATE Hebrew_CI_AS NULL,
[Username] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblChbFileLog] ADD CONSTRAINT [PK_tblChbFileLog] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblChbFileLog] ADD CONSTRAINT [FK_tblChbFileLog_tblEpaActionType_ActionType] FOREIGN KEY ([ActionType]) REFERENCES [dbo].[tblEpaActionType] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'CHB import log', 'SCHEMA', N'dbo', 'TABLE', N'tblChbFileLog', NULL, NULL
GO
