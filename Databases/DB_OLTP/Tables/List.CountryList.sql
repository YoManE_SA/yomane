CREATE TABLE [List].[CountryList]
(
[CountryISOCode] [char] (2) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL,
[ISOCode3] [char] (3) COLLATE Hebrew_CI_AS NULL,
[ISONumber] [char] (3) COLLATE Hebrew_CI_AS NULL,
[PhoneCode] [varchar] (5) COLLATE Hebrew_CI_AS NOT NULL,
[IBANLength] [tinyint] NOT NULL,
[IsABARequired] [bit] NOT NULL,
[IsSEPA] [bit] NOT NULL,
[ZipCodeRegExPattern] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[SortOrder] [tinyint] NULL,
[CountryID] [int] NULL,
[WorldRegionISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[CountryList] ADD CONSTRAINT [PK_CountryList] PRIMARY KEY CLUSTERED  ([CountryISOCode]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [List].[CountryList] ADD CONSTRAINT [UIX_CountryList_CountryID] UNIQUE NONCLUSTERED  ([CountryID]) ON [PRIMARY]
GO
ALTER TABLE [List].[CountryList] ADD CONSTRAINT [FK_CountryList_WorldRegion_WorldRegionISOCode] FOREIGN KEY ([WorldRegionISOCode]) REFERENCES [List].[WorldRegion] ([WorldRegionISOCode])
GO
