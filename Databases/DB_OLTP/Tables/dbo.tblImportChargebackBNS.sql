CREATE TABLE [dbo].[tblImportChargebackBNS]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[icb_IsProcessed] [bit] NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_icb_IsProcessed] DEFAULT ((0)),
[CASE ID] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_CASE ID] DEFAULT (''),
[RSN] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_RSN] DEFAULT (''),
[MER-NO] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_MER-NO] DEFAULT (''),
[CARDHOLDER-NUMBER] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_CARDHOLDER-NUMBER] DEFAULT (''),
[TXN-DATE] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_TXN-DATE] DEFAULT (''),
[AMOUNT-TXN] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_AMOUNT-TXN] DEFAULT (''),
[CUR] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_CUR] DEFAULT (''),
[AMOUNT-EURO] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_AMOUNT-EURO] DEFAULT (''),
[REF-NO] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_REF-NO] DEFAULT (''),
[TICKET-NO] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_TICKET-NO] DEFAULT (''),
[DEADLINE] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_DEADLINE] DEFAULT (''),
[COMMENTS] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_COMMENTS] DEFAULT (''),
[MRF] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_MRF] DEFAULT (''),
[icb_User] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_icb_User] DEFAULT (user_name()),
[icb_FileName] [varchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_icb_FileName] DEFAULT (''),
[icb_ChargebackDate] [datetime] NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_icb_ChargebackDate] DEFAULT (getdate()),
[icb_IsPhotocopy] [bit] NOT NULL CONSTRAINT [DF_tblImportChargebackBNS_icb_IsPhotocopy] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblImportChargebackBNS] ADD CONSTRAINT [PK_tblImportChargebackBNS] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblImportChargebackBNS_CASEID] ON [dbo].[tblImportChargebackBNS] ([CASE ID] DESC) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
