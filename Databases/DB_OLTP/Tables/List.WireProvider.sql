CREATE TABLE [List].[WireProvider]
(
[WireProvider_id] [varchar] (16) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL,
[ServerURL] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[Username] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[PasswordEncrypted] [varbinary] (100) NULL,
[EncryptionKey] [tinyint] NOT NULL,
[NextWireBatchNum] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[WireProvider] ADD CONSTRAINT [PK_WireProvider] PRIMARY KEY CLUSTERED  ([WireProvider_id]) ON [PRIMARY]
GO
