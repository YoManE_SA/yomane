CREATE TABLE [dbo].[tblLogCreditCardWhitelist]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[lccw_ccwlID] [int] NULL,
[lccw_Merchant] [int] NULL,
[lccw_Level] [tinyint] NULL,
[lccw_Transaction] [int] NULL,
[lccw_TransactionType] [int] NULL,
[lccw_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblLogCreditCardWhitelist_lccw_InsertDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogCreditCardWhitelist] ADD CONSTRAINT [PK_tblLogCreditCardWhitelist] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLogCreditCardWhitelist_ccwlID] ON [dbo].[tblLogCreditCardWhitelist] ([lccw_ccwlID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLogCreditCardWhitelist_InsertDate] ON [dbo].[tblLogCreditCardWhitelist] ([lccw_InsertDate] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLogCreditCardWhitelist_Merchant] ON [dbo].[tblLogCreditCardWhitelist] ([lccw_Merchant]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblLogCreditCardWhitelist_Transaction_TransactionType] ON [dbo].[tblLogCreditCardWhitelist] ([lccw_Transaction], [lccw_TransactionType]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblLogCreditCardWhitelist', NULL, NULL
GO
