CREATE TABLE [Log].[RecurringModify]
(
[RecurringModify_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NULL,
[RecurringSeries_id] [int] NULL,
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_RecurringModify_InsertDate] DEFAULT (getdate()),
[IPAddress] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[Action] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[RequestString] [nvarchar] (1000) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Log].[RecurringModify] ADD CONSTRAINT [PK_RecurringModify] PRIMARY KEY CLUSTERED  ([RecurringModify_id]) ON [PRIMARY]
GO
ALTER TABLE [Log].[RecurringModify] ADD CONSTRAINT [FK_RecurringModify_tblCompany] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log merchant remote modifications to recurring', 'SCHEMA', N'Log', 'TABLE', N'RecurringModify', NULL, NULL
GO
