CREATE TABLE [List].[ProductCategory]
(
[ProductCategory_id] [smallint] NOT NULL IDENTITY(1, 1),
[ParentID] [smallint] NULL,
[Name] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[SortOrder] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[ProductCategory] ADD CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED  ([ProductCategory_id]) ON [PRIMARY]
GO
ALTER TABLE [List].[ProductCategory] ADD CONSTRAINT [FK_ProductProperty_ProductCategory_ParentID] FOREIGN KEY ([ParentID]) REFERENCES [List].[ProductCategory] ([ProductCategory_id])
GO
