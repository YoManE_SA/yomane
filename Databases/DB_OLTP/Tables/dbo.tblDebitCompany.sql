CREATE TABLE [dbo].[tblDebitCompany]
(
[DebitCompany_ID] [int] NOT NULL CONSTRAINT [DF_tblDebitCompany_DebitCompany_ID] DEFAULT ((0)),
[dc_name] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_name] DEFAULT (''),
[dc_description] [nvarchar] (1000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_description] DEFAULT (''),
[dc_isAllowRefund] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_isAllowRefund] DEFAULT ((0)),
[dc_isAllowPartialRefund] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_isAllowPartialRefund] DEFAULT ((1)),
[dc_isReturnCode] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_isReturnCode] DEFAULT ((1)),
[dc_isActive] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_isActive] DEFAULT ((1)),
[dc_nextTransID] [int] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_nextTransID] DEFAULT ((0)),
[dc_TempBlocks] [int] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_TempBlocks] DEFAULT ((0)),
[dc_UnblockAttemptString] [nvarchar] (2000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_UnblockAttemptString] DEFAULT (''),
[dc_IsBlocked] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_IsBlocked] DEFAULT ((0)),
[dc_EmergencyPhone] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_EmergencyPhone] DEFAULT (''),
[dc_EmergencyContact] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_EmergencyContact] DEFAULT (''),
[dc_MonthlyLimitCHB] [int] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_MonthlyLimitCHB] DEFAULT ((0)),
[dc_MonthlyMCLimitCHB] [int] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_MonthlyMCLimitCHB] DEFAULT ((0)),
[dc_MonthlyLimitCHBNotifyUsers] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_MonthlyLimitCHBNotifyUsers] DEFAULT (''),
[dc_MonthlyLimitCHBNotifyUsersSMS] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_MonthlyLimitCHBNotifyUsersSMS] DEFAULT (''),
[dc_MonthlyMCLimitCHBNotifyUsers] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_MonthlyMCLimitCHBNotifyUsers] DEFAULT (''),
[dc_MonthlyMCLimitCHBNotifyUsersSMS] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_MonthlyMCLimitCHBNotifyUsersSMS] DEFAULT (''),
[dc_AutoRefundNotifyUsers] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_AutoRefundNotifyUsers] DEFAULT (''),
[dc_IsAutoRefund] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_IsAutoRefund] DEFAULT ((0)),
[dc_IsAllowPayoutWithoutTransfer] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompany_dc_IsAllowPayoutWithoutTransfer] DEFAULT ((0)),
[dc_IsNotifyRetReqAfterRefund] [bit] NULL CONSTRAINT [DF_tblDebitCompany_dc_IsNotifyRetReqAfterRefund] DEFAULT ((0)),
[RegistrationUsrname] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[RegistrationPassword256] [varbinary] (50) NULL,
[IsDynamicDescriptor] [bit] NULL,
[dc_isSupportPartialRefund] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDebitCompany] ADD CONSTRAINT [PK_tblDebitCompany] PRIMARY KEY CLUSTERED  ([DebitCompany_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Debit companies (processing banks)', 'SCHEMA', N'dbo', 'TABLE', N'tblDebitCompany', NULL, NULL
GO
