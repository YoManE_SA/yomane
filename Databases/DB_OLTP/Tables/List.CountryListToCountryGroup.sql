CREATE TABLE [List].[CountryListToCountryGroup]
(
[CountryISOCode] [char] (2) COLLATE Hebrew_CI_AS NOT NULL,
[CountryGroup_id] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[CountryListToCountryGroup] ADD CONSTRAINT [PK_CountryListToCountryGroup] PRIMARY KEY CLUSTERED  ([CountryISOCode], [CountryGroup_id]) ON [PRIMARY]
GO
ALTER TABLE [List].[CountryListToCountryGroup] ADD CONSTRAINT [FK_CountryListToCountryGroup_CountryGroup_CountryGroup_id] FOREIGN KEY ([CountryGroup_id]) REFERENCES [List].[CountryGroup] ([CountryGroup_id]) ON DELETE CASCADE
GO
ALTER TABLE [List].[CountryListToCountryGroup] ADD CONSTRAINT [FK_CountryListToCountryGroup_CountryList_CountryISOCode] FOREIGN KEY ([CountryISOCode]) REFERENCES [List].[CountryList] ([CountryISOCode]) ON DELETE CASCADE
GO
