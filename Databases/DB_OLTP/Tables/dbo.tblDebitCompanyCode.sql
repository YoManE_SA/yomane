CREATE TABLE [dbo].[tblDebitCompanyCode]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[DebitCompanyID] [smallint] NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_DebitCompanyID] DEFAULT ((0)),
[Code] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_Code] DEFAULT (''),
[DescriptionOriginal] [nvarchar] (400) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_DescriptionOriginal] DEFAULT (''),
[DescriptionMerchantHeb] [nvarchar] (550) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_DescriptionMerchantHeb] DEFAULT (''),
[DescriptionMerchantEng] [nvarchar] (550) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_DescriptionMerchantEng] DEFAULT (''),
[DescriptionCustomerHeb] [nvarchar] (550) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_DescriptionCustomerHeb] DEFAULT (''),
[DescriptionCustomerEng] [nvarchar] (550) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_DescriptionCustomerEng] DEFAULT (''),
[ChargeFail] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_ChargeFail] DEFAULT ((1)),
[RecurringAttemptsCharge] [int] NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_RecurringAttemptsCharge] DEFAULT ((0)),
[RecurringAttemptsSeries] [int] NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_RecurringAttemptsSeries] DEFAULT ((0)),
[FailSource] [tinyint] NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_FailSource] DEFAULT ((0)),
[LocalError] [nvarchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_LocalError] DEFAULT (''),
[BlockCC] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_BlockCC] DEFAULT ((0)),
[BlockMail] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_BlockMail] DEFAULT ((0)),
[IsCascade] [bit] NOT NULL CONSTRAINT [DF_tblDebitCompanyCode_IsCascade] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDebitCompanyCode] ADD CONSTRAINT [PK_tblDebitCompanyCode] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblDebitCompanyCode_DebitCompanyID_Code] ON [dbo].[tblDebitCompanyCode] ([DebitCompanyID], [Code]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Debit companies'' reply codes', 'SCHEMA', N'dbo', 'TABLE', N'tblDebitCompanyCode', NULL, NULL
GO
