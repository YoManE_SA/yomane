CREATE TABLE [dbo].[tblPeriodicFeeType]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[CurrencyID] [int] NOT NULL,
[IsAnnual] [bit] NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_tblPeriodicFeeType_IsActive] DEFAULT ((0)),
[FeeLimit] [money] NULL,
[Behavior] [int] NOT NULL CONSTRAINT [DF_tblPeriodicFeeType_Behavior] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgPeriodicFeeTypeAddPeriodicFee] ON [dbo].[tblPeriodicFeeType] AFTER INSERT AS
INSERT INTO tblPeriodicFee(MerchantID, TypeID) SELECT m.ID, ft.ID FROM tblCompany m CROSS JOIN Inserted ft
GO
ALTER TABLE [dbo].[tblPeriodicFeeType] ADD CONSTRAINT [PK_tblPeriodicFeeType] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPeriodicFeeType] ADD CONSTRAINT [FK_tblPeriodicFeeType_tblSystemCurrencies_CurrencyID] FOREIGN KEY ([CurrencyID]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] merchant''s periodic fee (Replaced in new Admin by [List].[PeriodicFeeType])', 'SCHEMA', N'dbo', 'TABLE', N'tblPeriodicFeeType', NULL, NULL
GO
