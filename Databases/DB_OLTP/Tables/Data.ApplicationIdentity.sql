CREATE TABLE [Data].[ApplicationIdentity]
(
[ApplicationIdentity_id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[BrandName] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[CompanyName] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[Domain] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[ThemeName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[ContentFolder] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[URLDevCenter] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[URLProcess] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[URLMerchantCP] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[URLWallet] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[URLWebsite] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[SmtpServer] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[SmtpUsername] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[SmtpPassword] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[EmailFrom] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[EmailContactTo] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[CopyRightText] [nvarchar] (300) COLLATE Hebrew_CI_AS NULL,
[IsActive] [bit] NOT NULL,
[WireAccount_id] [smallint] NULL,
[HashKey] [nvarchar] (32) COLLATE Hebrew_CI_AS NULL,
[ProcessMerchantNumber] [varchar] (10) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ApplicationIdentity] ADD CONSTRAINT [PK_ApplicationIdentity] PRIMARY KEY CLUSTERED  ([ApplicationIdentity_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ApplicationIdentity] ADD CONSTRAINT [FK_ApplicationIdentity_WireAccount] FOREIGN KEY ([WireAccount_id]) REFERENCES [Finance].[WireAccount] ([WireAccount_id]) ON DELETE SET NULL
GO
