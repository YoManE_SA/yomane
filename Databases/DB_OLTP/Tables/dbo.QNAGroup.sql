CREATE TABLE [dbo].[QNAGroup]
(
[QNAGroup_id] [int] NOT NULL,
[Language_id] [tinyint] NOT NULL,
[IsVisible] [bit] NOT NULL CONSTRAINT [DF_QNAGroup_IsVisible] DEFAULT ((0)),
[name] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL,
[IsMerchantCP] [bit] NOT NULL CONSTRAINT [DF_QNAGroup_IsMerchantCP] DEFAULT ((0)),
[IsDevCenter] [bit] NOT NULL CONSTRAINT [DF_QNAGroup_IsDevCenter] DEFAULT ((0)),
[IsWallet] [bit] NOT NULL CONSTRAINT [DF_QNAGroup_IsWallet] DEFAULT ((0)),
[IsWebsite] [bit] NOT NULL CONSTRAINT [DF_QNAGroup_IsWebsite] DEFAULT ((0)),
[TemplateRestriction] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[ParentCompny_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QNAGroup] ADD CONSTRAINT [PK_QNAGroup] PRIMARY KEY CLUSTERED  ([QNAGroup_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QNAGroup] ADD CONSTRAINT [FK_QNAGroup_LanguageList_Language_id] FOREIGN KEY ([Language_id]) REFERENCES [List].[LanguageList] ([Language_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Questions and answers group', 'SCHEMA', N'dbo', 'TABLE', N'QNAGroup', NULL, NULL
GO
