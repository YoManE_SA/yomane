CREATE TABLE [Track].[MerchantActivity]
(
[Merchant_id] [int] NOT NULL,
[DateFirstLogin] [datetime2] (2) NULL,
[DateFirstTransPass] [datetime2] (2) NULL,
[DateLastTransPass] [datetime2] (2) NULL,
[DateLastTransFail] [datetime2] (2) NULL,
[DateLastTransPreAuth] [datetime2] (2) NULL,
[DateLastTransPending] [datetime2] (2) NULL,
[DateLastSettlement] [datetime2] (2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [Track].[MerchantActivity] ADD CONSTRAINT [PK_MerchantActivity] PRIMARY KEY CLUSTERED  ([Merchant_id]) ON [PRIMARY]
GO
ALTER TABLE [Track].[MerchantActivity] ADD CONSTRAINT [FK_MerchantActivity_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
