CREATE TABLE [dbo].[tblBankAccounts]
(
[BA_ID] [int] NOT NULL IDENTITY(1, 1),
[BA_InsDate] [datetime] NULL CONSTRAINT [DF_tblBankAccounts_BA_InsDate] DEFAULT (getdate()),
[BA_Update] [datetime] NULL CONSTRAINT [DF_tblBankAccounts_BA_Update] DEFAULT (getdate()),
[BA_BankName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL,
[BA_AccountName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL,
[BA_AccountNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL,
[BA_Currency] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBankAccounts] ADD CONSTRAINT [PK_tblBankAccounts] PRIMARY KEY CLUSTERED  ([BA_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBankAccounts] ADD CONSTRAINT [FK_tblBankAccounts_tblSystemCurrencies_BA_Currency] FOREIGN KEY ([BA_Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Source wire account - replaced by [Finance].[WireAccount]', 'SCHEMA', N'dbo', 'TABLE', N'tblBankAccounts', NULL, NULL
GO
