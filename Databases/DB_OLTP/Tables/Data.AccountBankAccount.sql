CREATE TABLE [Data].[AccountBankAccount]
(
[AccountBankAccount_id] [int] NOT NULL IDENTITY(1, 1),
[Account_id] [int] NOT NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[RefAccountBankAccount_id] [int] NULL,
[IsDefault] [bit] NOT NULL,
[AccountName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[AccountNumber] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[ABA] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[IBAN] [nvarchar] (40) COLLATE Hebrew_CI_AS NULL,
[SwiftNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[BankName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[BankStreet1] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[BankStreet2] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[BankCity] [nvarchar] (60) COLLATE Hebrew_CI_AS NULL,
[BankPostalCode] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[BankStateISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[BankCountryISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[BankBranch] [varchar] (5) COLLATE Hebrew_CI_AS NULL,
[BankCode] [varchar] (5) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountBankAccount] ADD CONSTRAINT [PK_AccountBankAccount] PRIMARY KEY CLUSTERED  ([AccountBankAccount_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountBankAccount] ADD CONSTRAINT [FK_AccountBankAccount_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Data].[AccountBankAccount] ADD CONSTRAINT [FK_AccountBankAccount_CountryList_CountryISOCode] FOREIGN KEY ([BankCountryISOCode]) REFERENCES [List].[CountryList] ([CountryISOCode])
GO
ALTER TABLE [Data].[AccountBankAccount] ADD CONSTRAINT [FK_AccountBankAccount_StateList_StateISOCode] FOREIGN KEY ([BankStateISOCode]) REFERENCES [List].[StateList] ([StateISOCode])
GO
ALTER TABLE [Data].[AccountBankAccount] ADD CONSTRAINT [FK_AccountBankAccount_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Data].[AccountBankAccount] ADD CONSTRAINT [FK_AccountBankAccount_RefAccountBankAccountID] FOREIGN KEY ([RefAccountBankAccount_id]) REFERENCES [Data].[AccountBankAccount] ([AccountBankAccount_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account''s bank account details', 'SCHEMA', N'Data', 'TABLE', N'AccountBankAccount', NULL, NULL
GO
