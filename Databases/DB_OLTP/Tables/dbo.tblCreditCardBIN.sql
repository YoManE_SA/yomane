CREATE TABLE [dbo].[tblCreditCardBIN]
(
[BIN] [nvarchar] (19) COLLATE Hebrew_CI_AS NOT NULL,
[isoCode] [varchar] (2) COLLATE Hebrew_CI_AS NOT NULL,
[PaymentMethod] [smallint] NOT NULL CONSTRAINT [DF_tblCreditCardBIN_PaymentMethod] DEFAULT ((0)),
[CCName] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL,
[CCType] [tinyint] NOT NULL CONSTRAINT [DF_tblCreditCardBIN_CCType] DEFAULT ((0)),
[ImportDate] [datetime] NULL,
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCreditCardBIN_InsertDate] DEFAULT (getdate()),
[BINID] [int] NOT NULL IDENTITY(1, 1),
[BINLen] AS (len([BIN])) PERSISTED,
[BINNumber] AS (CONVERT([int],case when len([BIN])>(6) then left([BIN],(6)) else [BIN] end,0)) PERSISTED
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCreditCardBIN] ADD CONSTRAINT [PK_tblCreditCardBIN] PRIMARY KEY CLUSTERED  ([BIN]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Credit card BIN', 'SCHEMA', N'dbo', 'TABLE', N'tblCreditCardBIN', NULL, NULL
GO
