CREATE TABLE [Data].[AccountBalanceMoneyRequest]
(
[AccountBalanceMoneyRequest_id] [int] NOT NULL IDENTITY(1, 1),
[SourceAccount_id] [int] NOT NULL,
[SourceAccountBalance_id] [int] NULL,
[SourceText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[TargetAccount_id] [int] NOT NULL,
[TargetAccountBalance_id] [int] NULL,
[TargetText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Amount] [decimal] (10, 2) NOT NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[RequestDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_AccountBalanceMoneyRequest_RequestDate] DEFAULT (sysdatetime()),
[ConfirmDate] [datetime2] (0) NULL,
[IsPush] [bit] NOT NULL,
[IsApproved] [bit] NULL,
[PaybackDate] [datetime2] (0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountBalanceMoneyRequest] ADD CONSTRAINT [PK_AccountBalanceMoneyRequest] PRIMARY KEY CLUSTERED  ([AccountBalanceMoneyRequest_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountBalanceMoneyRequest] ADD CONSTRAINT [FK_AccountBalanceMoneyRequest_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Data].[AccountBalanceMoneyRequest] ADD CONSTRAINT [FK_AccountBalanceMoneyRequest_SourceAccountID] FOREIGN KEY ([SourceAccount_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Data].[AccountBalanceMoneyRequest] ADD CONSTRAINT [FK_AccountBalanceMoneyRequest_SourceBalanceID] FOREIGN KEY ([SourceAccountBalance_id]) REFERENCES [Data].[AccountBalance] ([AccountBalance_id])
GO
ALTER TABLE [Data].[AccountBalanceMoneyRequest] ADD CONSTRAINT [FK_AccountBalanceMoneyRequest_TargetAccountID] FOREIGN KEY ([TargetAccount_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Data].[AccountBalanceMoneyRequest] ADD CONSTRAINT [FK_AccountBalanceMoneyRequest_TargetBalanceID] FOREIGN KEY ([TargetAccountBalance_id]) REFERENCES [Data].[AccountBalance] ([AccountBalance_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account requesting money transfer from/to balance', 'SCHEMA', N'Data', 'TABLE', N'AccountBalanceMoneyRequest', NULL, NULL
GO
