CREATE TABLE [Log].[LoginHistory]
(
[LoginHistory_id] [int] NOT NULL IDENTITY(1, 1),
[LoginAccount_id] [int] NOT NULL,
[LoginResult_id] [tinyint] NULL,
[InsertDate] [datetime2] (2) NOT NULL CONSTRAINT [DF_LoginHistory_InsertDate] DEFAULT (sysdatetime()),
[IPAddress] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[VariableChar] [varchar] (250) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Log].[LoginHistory] ADD CONSTRAINT [PK_LoginHistory] PRIMARY KEY CLUSTERED  ([LoginHistory_id]) ON [PRIMARY]
GO
ALTER TABLE [Log].[LoginHistory] ADD CONSTRAINT [FK_LoginHistory_LoginAccountID] FOREIGN KEY ([LoginAccount_id]) REFERENCES [Data].[LoginAccount] ([LoginAccount_id]) ON DELETE CASCADE
GO
ALTER TABLE [Log].[LoginHistory] ADD CONSTRAINT [FK_LoginHistory_LoginResultID] FOREIGN KEY ([LoginResult_id]) REFERENCES [List].[LoginResult] ([LoginResult_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log login tries', 'SCHEMA', N'Log', 'TABLE', N'LoginHistory', NULL, NULL
GO
