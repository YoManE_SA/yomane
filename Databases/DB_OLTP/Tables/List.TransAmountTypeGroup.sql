CREATE TABLE [List].[TransAmountTypeGroup]
(
[TransAmountTypeGroup_id] [int] NOT NULL,
[Name] [varchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[TransAmountTypeGroup] ADD CONSTRAINT [PK_TransAmountTypeGroup] PRIMARY KEY CLUSTERED  ([TransAmountTypeGroup_id]) ON [PRIMARY]
GO
