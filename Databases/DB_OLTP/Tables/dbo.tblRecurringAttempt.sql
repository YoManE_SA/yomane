CREATE TABLE [dbo].[tblRecurringAttempt]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ra_Charge] [int] NULL,
[ra_Date] [datetime] NOT NULL CONSTRAINT [DF_tblRecurringAttempt_ra_Date] DEFAULT (getdate()),
[ra_CreditCard] [int] NULL,
[ra_ECheck] [int] NULL,
[ra_ReplyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblRecurringAttempt_ra_ReplyCode] DEFAULT (''),
[ra_TransFail] [int] NULL,
[ra_TransPass] [int] NULL,
[ra_TransPending] [int] NULL,
[ra_Comments] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblRecurringAttempt_ra_Comments] DEFAULT (''),
[ra_TransApproval] [int] NULL,
[TransPaymentMethod_id] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgRecurringAttemptAddSetTransPassRecurringFields] ON [dbo].[tblRecurringAttempt] FOR INSERT AS
UPDATE
	tblCompanyTransPass
SET
	RecurringSeries=rc_Series,
	RecurringChargeNumber=rc_ChargeNumber
FROM
	Inserted
	INNER JOIN tblRecurringCharge ON ra_Charge=tblRecurringCharge.ID
	INNER JOIN tblCompanyTransPass ON ra_TransPass=tblCompanyTransPass.ID
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgRecurringAttemptUpdateCharge] ON [dbo].[tblRecurringAttempt] FOR INSERT AS
BEGIN
	SET NOCOUNT ON
	UPDATE tblRecurringCharge SET rc_Paid=1 WHERE ID IN (SELECT ra_Charge FROM Inserted WHERE ra_TransPass IS NOT NULL OR ra_TransApproval IS NOT NULL)
	SET NOCOUNT OFF
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgRecurringAttemptUpdateChargeAttemptCount] ON [dbo].[tblRecurringAttempt] AFTER INSERT AS
BEGIN
	SET NOCOUNT ON
	UPDATE
		tblRecurringCharge
	SET
		rc_Attempts=rc_Attempts+1
	FROM
		tblRecurringCharge
		INNER JOIN Inserted ON tblRecurringCharge.ID=Inserted.ra_Charge
	SET NOCOUNT OFF
END
GO
ALTER TABLE [dbo].[tblRecurringAttempt] ADD CONSTRAINT [PK_tblRecurringAttempt] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblRecurringAttempt_Charge] ON [dbo].[tblRecurringAttempt] ([ra_Charge] DESC) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblRecurringAttempt_TransApproval] ON [dbo].[tblRecurringAttempt] ([ra_TransApproval] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblRecurringAttempt_TransFail] ON [dbo].[tblRecurringAttempt] ([ra_TransFail] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblRecurringAttempt_TransPass] ON [dbo].[tblRecurringAttempt] ([ra_TransPass] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblRecurringAttempt_TransPending] ON [dbo].[tblRecurringAttempt] ([ra_TransPending] DESC) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRecurringAttempt] ADD CONSTRAINT [FK_tblRecurringAttempt_tblRecurringCharge_ra_Charge] FOREIGN KEY ([ra_Charge]) REFERENCES [dbo].[tblRecurringCharge] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblRecurringAttempt] ADD CONSTRAINT [FK_tblRecurringAttempt_tblCreditCard_ra_CreditCard] FOREIGN KEY ([ra_CreditCard]) REFERENCES [dbo].[tblCreditCard] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblRecurringAttempt] ADD CONSTRAINT [FK_tblRecurringAttempt_tblCheckDetails_ra_ECheck] FOREIGN KEY ([ra_ECheck]) REFERENCES [dbo].[tblCheckDetails] ([id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblRecurringAttempt] ADD CONSTRAINT [FK_tblRecurringAttempt_tblCompanyTransApproval_ra_TransApproval] FOREIGN KEY ([ra_TransApproval]) REFERENCES [dbo].[tblCompanyTransApproval] ([ID])
GO
ALTER TABLE [dbo].[tblRecurringAttempt] ADD CONSTRAINT [FK_tblRecurringAttempt_tblCompanyTransFail_ra_TransFail] FOREIGN KEY ([ra_TransFail]) REFERENCES [dbo].[tblCompanyTransFail] ([ID])
GO
ALTER TABLE [dbo].[tblRecurringAttempt] ADD CONSTRAINT [FK_tblRecurringAttempt_tblCompanyTransPass_ra_TransPass] FOREIGN KEY ([ra_TransPass]) REFERENCES [dbo].[tblCompanyTransPass] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblRecurringAttempt] ADD CONSTRAINT [FK_tblRecurringAttempt_tblCompanyTransPending_ra_TransPending] FOREIGN KEY ([ra_TransPending]) REFERENCES [dbo].[tblCompanyTransPending] ([companyTransPending_id])
GO
ALTER TABLE [dbo].[tblRecurringAttempt] ADD CONSTRAINT [FK_RecurringAttempt_TransPaymentMethod] FOREIGN KEY ([TransPaymentMethod_id]) REFERENCES [Trans].[TransPaymentMethod] ([TransPaymentMethod_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s recurring charge attempt log', 'SCHEMA', N'dbo', 'TABLE', N'tblRecurringAttempt', NULL, NULL
GO
