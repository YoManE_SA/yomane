CREATE TABLE [List].[ExternalCardProvider]
(
[ExternalCardProvider_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_ExternalCardProvider_IsActive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [List].[ExternalCardProvider] ADD CONSTRAINT [PK_ExternalCardProvider] PRIMARY KEY CLUSTERED  ([ExternalCardProvider_id]) ON [PRIMARY]
GO
