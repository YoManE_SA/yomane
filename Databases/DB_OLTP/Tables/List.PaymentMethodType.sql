CREATE TABLE [List].[PaymentMethodType]
(
[PaymentMethodType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[PaymentMethodType] ADD CONSTRAINT [PK_PaymentMethodType] PRIMARY KEY CLUSTERED  ([PaymentMethodType_id]) ON [PRIMARY]
GO
