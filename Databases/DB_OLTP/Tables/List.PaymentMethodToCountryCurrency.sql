CREATE TABLE [List].[PaymentMethodToCountryCurrency]
(
[PaymentMethod_id] [smallint] NOT NULL,
[Country_id] [smallint] NULL,
[Currency_id] [smallint] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PaymentMethodToCountryCurrency_CountryID_PaymentMethodID] ON [List].[PaymentMethodToCountryCurrency] ([Country_id], [PaymentMethod_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PaymentMethodToCountryCurrency_CurrencyID_PaymentMethodID] ON [List].[PaymentMethodToCountryCurrency] ([Currency_id], [PaymentMethod_id]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PaymentMethodToCountryCurrency_PaymentMethodID] ON [List].[PaymentMethodToCountryCurrency] ([PaymentMethod_id], [Country_id], [Currency_id]) ON [PRIMARY]
GO
ALTER TABLE [List].[PaymentMethodToCountryCurrency] ADD CONSTRAINT [FK_PaymentMethodToCountryCurrency_PaymentMethod_PaymentMethod_id] FOREIGN KEY ([PaymentMethod_id]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id]) ON DELETE CASCADE
GO
