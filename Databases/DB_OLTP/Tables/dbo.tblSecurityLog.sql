CREATE TABLE [dbo].[tblSecurityLog]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[sl_User] [int] NULL,
[sl_Document] [int] NULL,
[sl_Date] [datetime] NOT NULL CONSTRAINT [DF_tblSecurityLog_sl_Date] DEFAULT (getdate()),
[sl_IP] [varchar] (25) COLLATE Hebrew_CI_AS NOT NULL,
[sl_IsDenied] [bit] NOT NULL,
[sl_IsReadOnly] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSecurityLog] ADD CONSTRAINT [PK_tblSecurityLog] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSecurityLog] ADD CONSTRAINT [FK_tblSecurityLog_tblSecurityDocument_sl_Document] FOREIGN KEY ([sl_Document]) REFERENCES [dbo].[tblSecurityDocument] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblSecurityLog] ADD CONSTRAINT [FK_tblSecurityLog_tblSecurityUser_sl_User] FOREIGN KEY ([sl_User]) REFERENCES [dbo].[tblSecurityUser] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds Admincash security user access log (Replace by Admin new security method)', 'SCHEMA', N'dbo', 'TABLE', N'tblSecurityLog', NULL, NULL
GO
