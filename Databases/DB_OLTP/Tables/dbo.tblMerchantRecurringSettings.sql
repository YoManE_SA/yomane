CREATE TABLE [dbo].[tblMerchantRecurringSettings]
(
[MerchantID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL CONSTRAINT [DF_tblMerchantRecurringSettings_IsEnabled] DEFAULT ((0)),
[IsEnabledFromTransPass] [bit] NOT NULL CONSTRAINT [DF_tblMerchantRecurringSettings_IsEnabledFromTransPass] DEFAULT ((0)),
[IsEnabledModify] [bit] NOT NULL CONSTRAINT [DF_tblMerchantRecurringSettings_IsEnabledModify] DEFAULT ((0)),
[ForceMD5OnModify] [bit] NOT NULL CONSTRAINT [DF_tblMerchantRecurringSettings_ForceMD5OnModify] DEFAULT ((1)),
[MaxYears] [int] NOT NULL CONSTRAINT [DF_tblMerchantRecurringSettings_MaxYears] DEFAULT ((3)),
[MaxCharges] [int] NOT NULL CONSTRAINT [DF_tblMerchantRecurringSettings_MaxCharges] DEFAULT ((200)),
[MaxStages] [int] NOT NULL CONSTRAINT [DF_tblMerchantRecurringSettings_MaxStages] DEFAULT ((6))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMerchantRecurringSettings] ADD CONSTRAINT [PK_tblMerchantRecurringSettings] PRIMARY KEY CLUSTERED  ([MerchantID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMerchantRecurringSettings] ADD CONSTRAINT [FK_tblMerchantRecurringSettings_tblCompany_MerchantID] FOREIGN KEY ([MerchantID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Holds recurring settings for the merchant', 'SCHEMA', N'dbo', 'TABLE', N'tblMerchantRecurringSettings', NULL, NULL
GO
