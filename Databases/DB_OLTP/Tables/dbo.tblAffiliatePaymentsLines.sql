CREATE TABLE [dbo].[tblAffiliatePaymentsLines]
(
[AFPL_ID] [int] NOT NULL IDENTITY(1, 1),
[AFPL_AFP_ID] [int] NULL,
[AFPL_Text] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[AFPL_Quantity] [int] NULL,
[AFPL_Amount] [smallmoney] NULL,
[AFPL_Total] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliatePaymentsLines] ADD CONSTRAINT [PK_tblAffiliatePaymentsLines] PRIMARY KEY CLUSTERED  ([AFPL_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliatePaymentsLines] ADD CONSTRAINT [FK_tblAffiliatePaymentsLines_tblAffiliatePayments_AFPL_AFP_ID] FOREIGN KEY ([AFPL_AFP_ID]) REFERENCES [dbo].[tblAffiliatePayments] ([AFP_ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Affiliate''s payment detail', 'SCHEMA', N'dbo', 'TABLE', N'tblAffiliatePaymentsLines', NULL, NULL
GO
