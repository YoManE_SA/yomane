CREATE TABLE [List].[TransAmountTypeToGroup]
(
[TransAmountTypeGroup_id] [int] NOT NULL,
[TransAmountType_id] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[TransAmountTypeToGroup] ADD CONSTRAINT [PK_TransAmountTypeToGroup] PRIMARY KEY CLUSTERED  ([TransAmountTypeGroup_id], [TransAmountType_id]) ON [PRIMARY]
GO
ALTER TABLE [List].[TransAmountTypeToGroup] ADD CONSTRAINT [FK_TransAmountTypeToGroup_TransAmountType_TransAmountType_id] FOREIGN KEY ([TransAmountType_id]) REFERENCES [List].[TransAmountType] ([TransAmountType_id]) ON DELETE CASCADE
GO
ALTER TABLE [List].[TransAmountTypeToGroup] ADD CONSTRAINT [FK_TransAmountTypeToGroup_TransAmountTypeGroup_TransAmountTypeGroup_id] FOREIGN KEY ([TransAmountTypeGroup_id]) REFERENCES [List].[TransAmountTypeGroup] ([TransAmountTypeGroup_id]) ON DELETE CASCADE
GO
