CREATE TABLE [Log].[ExternalServiceHistory]
(
[ExternalServiceHistory_id] [int] NOT NULL IDENTITY(1, 1),
[ExternalServiceType_id] [varchar] (16) COLLATE Hebrew_CI_AS NULL,
[ExternalServiceAction_id] [varchar] (32) COLLATE Hebrew_CI_AS NULL,
[Account_id] [int] NOT NULL,
[InsertDate] [datetime2] (4) NOT NULL,
[FileName] [varchar] (250) COLLATE Hebrew_CI_AS NULL,
[SystemText] [nvarchar] (2000) COLLATE Hebrew_CI_AS NULL,
[IsError] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Log].[ExternalServiceHistory] ADD CONSTRAINT [PK_ExternalServiceHistory] PRIMARY KEY CLUSTERED  ([ExternalServiceHistory_id]) ON [PRIMARY]
GO
ALTER TABLE [Log].[ExternalServiceHistory] ADD CONSTRAINT [FK_ExternalServiceHistory_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Log].[ExternalServiceHistory] ADD CONSTRAINT [FK_ExternalServiceHistory_ExternalServiceActionID] FOREIGN KEY ([ExternalServiceAction_id]) REFERENCES [List].[ExternalServiceAction] ([ExternalServiceAction_id])
GO
ALTER TABLE [Log].[ExternalServiceHistory] ADD CONSTRAINT [FK_ExternalServiceHistory_ExternalServiceTypeID] FOREIGN KEY ([ExternalServiceType_id]) REFERENCES [List].[ExternalServiceType] ([ExternalServiceType_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log external services usage', 'SCHEMA', N'Log', 'TABLE', N'ExternalServiceHistory', NULL, NULL
GO
