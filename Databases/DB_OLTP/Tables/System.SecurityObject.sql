CREATE TABLE [System].[SecurityObject]
(
[SecurityObject_id] [int] NOT NULL IDENTITY(1, 1),
[AppModule_id] [int] NOT NULL,
[Name] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL,
[Value] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[SecurityObject] ADD CONSTRAINT [PK_AppModuleObject] PRIMARY KEY CLUSTERED  ([SecurityObject_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[SecurityObject] ADD CONSTRAINT [UIX_SecurityObject_AppModuleID_Name] UNIQUE NONCLUSTERED  ([AppModule_id], [Name]) ON [PRIMARY]
GO
ALTER TABLE [System].[SecurityObject] ADD CONSTRAINT [FK_SecurityObject_AppModule_AppModuleID] FOREIGN KEY ([AppModule_id]) REFERENCES [System].[AppModule] ([AppModule_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'System security objects', 'SCHEMA', N'System', 'TABLE', N'SecurityObject', NULL, NULL
GO
