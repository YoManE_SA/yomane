CREATE TABLE [dbo].[tblCompanyTransApproval]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TransactionTypeID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_TransactionTypeID] DEFAULT ((7)),
[CustomerID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_CustomerID] DEFAULT ((0)),
[FraudDetectionLog_id] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_FraudDetectionLog_id] DEFAULT ((0)),
[InsertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_InsertDate] DEFAULT (getdate()),
[PayID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_PayID] DEFAULT ((0)),
[IPAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_IPAddress] DEFAULT (''),
[Amount] [money] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_Amount] DEFAULT ((0)),
[Payments] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_Payments] DEFAULT ((1)),
[CreditType] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_CreditType] DEFAULT ((0)),
[replyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_replyCode] DEFAULT (''),
[OrderNumber] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_OrderNumber] DEFAULT (''),
[Interest] [float] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_Interest] DEFAULT ((0)),
[TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_TerminalNumber] DEFAULT (''),
[approvalNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_approvalNumber] DEFAULT (''),
[PaymentMethod_id] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_PaymentMethod_id] DEFAULT ((1)),
[PaymentMethodID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_PaymentMethodID] DEFAULT ((0)),
[PaymentMethodDisplay] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_PaymentMethodDisplay] DEFAULT (''),
[referringUrl] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_referringUrl] DEFAULT (''),
[DebitReferenceCode] [nvarchar] (40) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_DebitReferenceCode] DEFAULT (''),
[netpayFee_transactionCharge] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_netpayFee_transactionCharge] DEFAULT ((0)),
[RecurringSeries] [int] NULL,
[RecurringChargeNumber] [int] NULL,
[PaymentMethod] [smallint] NULL,
[Currency] [int] NULL,
[CompanyID] [int] NULL,
[DebitCompanyID] [int] NULL,
[TransAnswerID] [int] NULL,
[DebitFee] [smallmoney] NULL CONSTRAINT [DF_tblCompanyTransApproval_DebitFee] DEFAULT ((0)),
[IsGateway] [bit] NULL,
[isTestOnly] [bit] NOT NULL CONSTRAINT [DF_tblCompanyTransApproval_isTestOnly] DEFAULT ((0)),
[DebitReferenceNum] [varchar] (40) COLLATE Hebrew_CI_AS NULL,
[TransSource_id] [tinyint] NULL,
[MobileDevice_id] [int] NULL,
[Comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblCompanyTransApproval_Comment] DEFAULT (''),
[SystemText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[PayforText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[CreditCardID] [int] NULL,
[CheckDetailsID] [int] NULL,
[PhoneDetailsID] [int] NULL,
[MerchantProduct_id] [int] NULL,
[PayerInfo_id] [int] NULL,
[TransPayerInfo_id] [int] NULL,
[TransPaymentMethod_id] [int] NULL,
[Is3DSecure] [bit] NULL CONSTRAINT [DF_CompanyTransApproval_Is3DSecure] DEFAULT ((0)),
[AcquirerReferenceNum] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[IsCardPresent] [bit] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trTransApproval_SetOldTransSource_InsUpd] ON [dbo].[tblCompanyTransApproval]
    AFTER INSERT, UPDATE
AS
    IF @@ROWCOUNT = 0 
        RETURN;
    SET NOCOUNT ON;
	
    IF UPDATE(TransSource_id) 
        BEGIN
	
            UPDATE  tblCompanyTransApproval
            SET     TransactionTypeID = TransSource_id
            WHERE   id IN ( SELECT  ID
                            FROM    INSERTED
                            WHERE   ISNULL(TransSource_id, 0) > 0 );
		
        END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[trTransApproval_SetPMDetailID_InsUpd] ON [dbo].[tblCompanyTransApproval]
AFTER INSERT, UPDATE
AS

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
	IF UPDATE(PaymentMethodID)
	BEGIN
	
		--Update PhoneDetailsID Column
		UPDATE tblCompanyTransApproval SET PhoneDetailsID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID <> 0 AND PaymentMethod >= 200 AND PaymentMethod <= 229);
		
		--Update CheckDetailsID Column
		UPDATE tblCompanyTransApproval SET CheckDetailsID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID > 0 AND PaymentMethod >= 100 AND PaymentMethod <= 199); 
		
		--Update CreditCardID Column
		UPDATE tblCompanyTransApproval SET CreditCardID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID > 0 AND PaymentMethod >= 20 AND PaymentMethod <= 49); 
		
	END



GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/****** Object:  Trigger [trgInsertTransFailUpdTracking] Script Date: 06/01/2008 16:26:55 ******/
CREATE TRIGGER [dbo].[trTransPreAuth_TrackMerchantActivity_Ins] ON [dbo].[tblCompanyTransApproval] AFTER INSERT
AS

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
    UPDATE  Track.MerchantActivity
    SET     DateLastTransPreAuth = SYSDATETIME()
    WHERE	Merchant_id IN ( SELECT companyID FROM INSERTED );


GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [PK_tblCompanyTransApproval] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransApproval_companyID] ON [dbo].[tblCompanyTransApproval] ([CompanyID] DESC) WITH (FILLFACTOR=85) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransApproval_CustomerID] ON [dbo].[tblCompanyTransApproval] ([CustomerID]) WHERE ([CustomerID]>(0)) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransApproval_InsertDate_CompanyID_Currency] ON [dbo].[tblCompanyTransApproval] ([InsertDate], [CompanyID], [Currency]) INCLUDE ([netpayFee_transactionCharge]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransApproval_terminalNumber] ON [dbo].[tblCompanyTransApproval] ([TerminalNumber]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblCompanyTransApproval_TransAnswerID] ON [dbo].[tblCompanyTransApproval] ([TransAnswerID]) WHERE ([TransAnswerID] IS NOT NULL) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_tblCheckDetails_CheckDetailsID] FOREIGN KEY ([CheckDetailsID]) REFERENCES [dbo].[tblCheckDetails] ([id])
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_tblCompany_CompanyID] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_tblCreditCard_CreditCardID] FOREIGN KEY ([CreditCardID]) REFERENCES [dbo].[tblCreditCard] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_tblSystemCurrencies_Currency] FOREIGN KEY ([Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_tblDebitCompany_DebitCompanyID] FOREIGN KEY ([DebitCompanyID]) REFERENCES [dbo].[tblDebitCompany] ([DebitCompany_ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_MobileDevice_MobileDevice_id] FOREIGN KEY ([MobileDevice_id]) REFERENCES [dbo].[MobileDevice] ([MobileDevice_id])
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_PaymentMethod_PaymentMethod] FOREIGN KEY ([PaymentMethod]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_PhoneDetail_PhoneDetailsID] FOREIGN KEY ([PhoneDetailsID]) REFERENCES [Trans].[PhoneDetail] ([PhoneDetail_id])
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_tblRecurringSeries_RecurringSeries] FOREIGN KEY ([RecurringSeries]) REFERENCES [dbo].[tblRecurringSeries] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_tblCompanyTransPass_TransAnswerID] FOREIGN KEY ([TransAnswerID]) REFERENCES [dbo].[tblCompanyTransPass] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_TransPayerInfo] FOREIGN KEY ([TransPayerInfo_id]) REFERENCES [Trans].[TransPayerInfo] ([TransPayerInfo_id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_TransPaymentMethod] FOREIGN KEY ([TransPaymentMethod_id]) REFERENCES [Trans].[TransPaymentMethod] ([TransPaymentMethod_id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransApproval] ADD CONSTRAINT [FK_tblCompanyTransApproval_TransSource_TransSource_id] FOREIGN KEY ([TransSource_id]) REFERENCES [List].[TransSource] ([TransSource_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transactions approved', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyTransApproval', NULL, NULL
GO
