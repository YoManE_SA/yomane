CREATE TABLE [Trans].[TransPaymentBillingAddress]
(
[TransPaymentBillingAddress_id] [int] NOT NULL IDENTITY(1, 1),
[Street1] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Street2] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[City] [nvarchar] (60) COLLATE Hebrew_CI_AS NULL,
[PostalCode] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[StateISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[CountryISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[CreditCard_id] [int] NULL,
[Echeck_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransPaymentBillingAddress] ADD CONSTRAINT [PK_PaymentBillingAddress] PRIMARY KEY CLUSTERED  ([TransPaymentBillingAddress_id]) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransPaymentBillingAddress] ADD CONSTRAINT [FK_TransPaymentBillingAddress_CountryList_CountryISOCode] FOREIGN KEY ([CountryISOCode]) REFERENCES [List].[CountryList] ([CountryISOCode])
GO
ALTER TABLE [Trans].[TransPaymentBillingAddress] ADD CONSTRAINT [FK_TransPaymentBillingAddress_StateList_StateISOCode] FOREIGN KEY ([StateISOCode]) REFERENCES [List].[StateList] ([StateISOCode])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transaction billing address', 'SCHEMA', N'Trans', 'TABLE', N'TransPaymentBillingAddress', NULL, NULL
GO
