CREATE TABLE [dbo].[tblRefundAskLog]
(
[refundAskLog_id] [int] NOT NULL IDENTITY(1, 1),
[refundAsk_id] [int] NOT NULL CONSTRAINT [DF_tblRefundAskLog_refundAsk_id] DEFAULT ((0)),
[ral_description] [nvarchar] (2500) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblRefundAskLog_ral_description] DEFAULT (''),
[ral_date] [smalldatetime] NOT NULL CONSTRAINT [DF_tblRefundAskLog_ral_date] DEFAULT (getdate()),
[ral_user] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblRefundAskLog_ral_user] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRefundAskLog] ADD CONSTRAINT [PK_tblRefundAskLog] PRIMARY KEY CLUSTERED  ([refundAskLog_id]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log of refund processing for [tblRefundAsk]', 'SCHEMA', N'dbo', 'TABLE', N'tblRefundAskLog', NULL, NULL
GO
