CREATE TABLE [List].[BalanceSourceType]
(
[BalanceSourceType_id] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[IsFee] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[BalanceSourceType] ADD CONSTRAINT [PK_BalanceSourceType] PRIMARY KEY CLUSTERED  ([BalanceSourceType_id]) ON [PRIMARY]
GO
