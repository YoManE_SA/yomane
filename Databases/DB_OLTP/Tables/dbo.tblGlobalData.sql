CREATE TABLE [dbo].[tblGlobalData]
(
[GD_ID] [int] NOT NULL,
[GD_Group] [int] NOT NULL,
[GD_LNG] [tinyint] NOT NULL CONSTRAINT [DF_tblGlobalData_GD_LNG] DEFAULT ((0)),
[GD_Text] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL,
[GD_Description] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[GD_Color] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[GD_Country] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[GD_Flags] [smallint] NOT NULL CONSTRAINT [DF_tblGlobalData_GD_Flags] DEFAULT ((0)),
[GD_Currency] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblGlobalData] ADD CONSTRAINT [PK_tblGlobalData] PRIMARY KEY CLUSTERED  ([GD_ID], [GD_Group], [GD_LNG]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblGlobalData_GD_Group] ON [dbo].[tblGlobalData] ([GD_Group]) INCLUDE ([GD_ID], [GD_LNG]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Old Look up table, replaced by tables with "List" schema ', 'SCHEMA', N'dbo', 'TABLE', N'tblGlobalData', NULL, NULL
GO
