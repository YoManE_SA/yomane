CREATE TABLE [List].[ProtocolType]
(
[ProtocolType_id] [varchar] (10) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[ProtocolType] ADD CONSTRAINT [PK_ProtocolType] PRIMARY KEY CLUSTERED  ([ProtocolType_id]) ON [PRIMARY]
GO
