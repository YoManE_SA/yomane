CREATE TABLE [Track].[MonthlyFloorTotal]
(
[MonthlyFloorTotal_id] [int] NOT NULL IDENTITY(1, 1),
[SetTransactionFloor_id] [int] NOT NULL,
[DateInFocus] [datetime2] (2) NULL CONSTRAINT [DF_MonthlyFloorTotal_DateInFocus] DEFAULT (sysdatetime()),
[Amount] [decimal] (19, 4) NULL
) ON [PRIMARY]
GO
ALTER TABLE [Track].[MonthlyFloorTotal] ADD CONSTRAINT [PK_MonthlyFloorTotal] PRIMARY KEY CLUSTERED  ([MonthlyFloorTotal_id]) ON [PRIMARY]
GO
ALTER TABLE [Track].[MonthlyFloorTotal] ADD CONSTRAINT [FK_MonthlyFloorTotal_SetTransactionFloor_SetTransactionFloor_id] FOREIGN KEY ([SetTransactionFloor_id]) REFERENCES [Setting].[SetTransactionFloor] ([SetTransactionFloor_id])
GO
