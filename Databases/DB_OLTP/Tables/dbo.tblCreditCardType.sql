CREATE TABLE [dbo].[tblCreditCardType]
(
[ID] [int] NOT NULL CONSTRAINT [DF_tblCreditCardType_ID] DEFAULT ((0)),
[NameEng] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCardType_NameEng] DEFAULT (''),
[NameHeb] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCardType_NameHeb] DEFAULT (''),
[IconFileName] [nvarchar] (40) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCardType_IconFileName] DEFAULT (''),
[isShow] [bit] NOT NULL CONSTRAINT [DF_tblCreditCardType_isShow] DEFAULT ((0)),
[DBName] [nvarchar] (40) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCardType_DBName] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCreditCardType] ADD CONSTRAINT [PK_tblCreditCardType] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblCreditCardType', NULL, NULL
GO
