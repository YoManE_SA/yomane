CREATE TABLE [dbo].[tblCreditCard]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ccTypeID] [smallint] NOT NULL CONSTRAINT [DF_tblCreditCard_ccTypeID] DEFAULT ((0)),
[PersonalNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCard_PersonalNumber] DEFAULT (''),
[ExpMM] [char] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCard_ExpMM] DEFAULT (''),
[ExpYY] [char] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCard_ExpYY] DEFAULT (''),
[Member] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCard_Member] DEFAULT (N''''),
[phoneNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCard_phoneNumber] DEFAULT (''),
[email] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCard_email] DEFAULT (''),
[Comment] [nvarchar] (3000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCard_Comment] DEFAULT (''),
[CCard_First6] [int] NOT NULL CONSTRAINT [DF_tblCreditCard_CCard_First6] DEFAULT ((0)),
[CCard_Last4] [smallint] NOT NULL CONSTRAINT [DF_tblCreditCard_CCard_Last4] DEFAULT ((0)),
[CCard_number256] [varbinary] (200) NULL,
[cc_InsertDate] [datetime] NULL CONSTRAINT [DF_tblCreditCard_cc_InsertDate] DEFAULT (getdate()),
[CompanyID] [int] NULL,
[BillingAddressID] [int] NULL,
[BINCountry] [char] (2) COLLATE Hebrew_CI_AS NULL,
[cc_cui] [varchar] (20) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblCreditCard_cc_cui] DEFAULT (''),
[CardholderDOB] [date] NULL,
[InvoiceName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[trgCreditCardDeleteMoveToArchive] ON [dbo].[tblCreditCard] AFTER DELETE AS
INSERT INTO dbo.syn_archive_tblCreditCard
(
	ID,
	companyID,
	ccTypeID,
	BillingAddressId,
	PersonalNumber,
	ExpMM,
	ExpYY,
	Member,
	cc_cui,
	phoneNumber,
	email,
	BINCountry,
	Comment,
	CCard_First6,
	CCard_Last4,
	CCard_number256,
	cc_InsertDate
)
SELECT
	ID,
	companyID,
	ccTypeID,
	BillingAddressId,
	PersonalNumber,
	ExpMM,
	ExpYY,
	Member,
	cc_cui,
	phoneNumber,
	email,
	BINCountry,
	Comment,
	CCard_First6,
	CCard_Last4,
	CCard_number256,
	cc_InsertDate
FROM
	DELETED
GO
ALTER TABLE [dbo].[tblCreditCard] ADD CONSTRAINT [PK_tblCreditCard] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCreditCard_CCard_First6] ON [dbo].[tblCreditCard] ([CCard_First6]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCreditCard_CCard_Last4] ON [dbo].[tblCreditCard] ([CCard_Last4]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCreditCard_CCard_number] ON [dbo].[tblCreditCard] ([CCard_number256]) INCLUDE ([ID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCreditCard_CompanyID] ON [dbo].[tblCreditCard] ([CompanyID]) WITH (FILLFACTOR=85) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCreditCard_email] ON [dbo].[tblCreditCard] ([email]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCreditCard_Member] ON [dbo].[tblCreditCard] ([Member]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCreditCard] ADD CONSTRAINT [FK_tblCreditCard_tblBillingAddress_BillingAddressID] FOREIGN KEY ([BillingAddressID]) REFERENCES [dbo].[tblBillingAddress] ([id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCreditCard] ADD CONSTRAINT [FK_tblCreditCard_CountryList_CountryISOCode] FOREIGN KEY ([BINCountry]) REFERENCES [List].[CountryList] ([CountryISOCode])
GO
ALTER TABLE [dbo].[tblCreditCard] ADD CONSTRAINT [FK_tblCreditCard_tblCompany_CompanyID] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[tblCompany] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds credit card processed info (Replace by Trans.TransPaymentMethod)', 'SCHEMA', N'dbo', 'TABLE', N'tblCreditCard', NULL, NULL
GO
