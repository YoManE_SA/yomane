CREATE TABLE [dbo].[tblTransactionPay]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[CompanyID] [int] NOT NULL CONSTRAINT [DF_tblTransactionPay_CompanyID] DEFAULT ((0)),
[PayDate] [datetime] NOT NULL CONSTRAINT [DF_tblTransactionPay_PayDate] DEFAULT (getdate()),
[isChargeVAT] [bit] NOT NULL CONSTRAINT [DF_tblTransactionPay_isChargeVAT] DEFAULT ((1)),
[ExchangeRate] [float] NOT NULL CONSTRAINT [DF_tblTransactionPay_ExchangeRate] DEFAULT ((0)),
[transTotal] [float] NOT NULL CONSTRAINT [DF_tblTransactionPay_transTotal] DEFAULT ((0)),
[transChargeTotal] [float] NOT NULL CONSTRAINT [DF_tblTransactionPay_transChargeTotal] DEFAULT ((0)),
[transPayTotal] [float] NOT NULL CONSTRAINT [DF_tblTransactionPay_transPayTotal] DEFAULT ((0)),
[SecurityDeposit] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPay_SecurityDeposit] DEFAULT ((0)),
[transRollingReserve] [money] NOT NULL CONSTRAINT [DF_tblTransactionPay_transRollingReserve] DEFAULT ((0)),
[Comment] [nvarchar] (250) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblTransactionPay_comment] DEFAULT (''),
[BillingCompanys_id] [int] NOT NULL CONSTRAINT [DF_tblTransactionPay_BillingCompanys_id] DEFAULT ((2)),
[BillingLanguageShow] [nvarchar] (3) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblTransactionPay_BillingLanguageShow] DEFAULT (''),
[BillingCurrencyShow] [tinyint] NOT NULL CONSTRAINT [DF_tblTransactionPay_BillingCurrencyShow] DEFAULT ((0)),
[BillingCompanyName] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblTransactionPay_BillingCompanyName] DEFAULT (''),
[BillingCompanyAddress] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblTransactionPay_BillingCompanyAddress] DEFAULT (''),
[BillingCompanyNumber] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblTransactionPay_BillingCompanyNumber] DEFAULT (''),
[BillingCompanyEmail] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblTransactionPay_BillingCompanyEmail] DEFAULT (''),
[BillingCreateDate] [datetime] NOT NULL CONSTRAINT [DF_tblTransactionPay_BillingCreateDate] DEFAULT (getdate()),
[isBillingPrintOriginal] [bit] NOT NULL CONSTRAINT [DF_tblTransactionPay_isBillingPrintOriginal] DEFAULT ((0)),
[BillingToInfo] [nvarchar] (2000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblTransactionPay_BillingToInfo] DEFAULT (''),
[PayType] [tinyint] NOT NULL CONSTRAINT [DF_tblTransactionPay_PayType] DEFAULT ((0)),
[invoiceType] [smallint] NOT NULL CONSTRAINT [DF_tblTransactionPay_invoiceType] DEFAULT ((0)),
[TerminalType] [smallint] NOT NULL CONSTRAINT [DF_tblTransactionPay_TerminalType] DEFAULT ((1)),
[PrimePercent] [float] NOT NULL CONSTRAINT [DF_tblTransactionPay_PrimePercent] DEFAULT ((10)),
[isShow] [bit] NOT NULL CONSTRAINT [DF_tblTransactionPay_isShow] DEFAULT ((0)),
[InvoiceDocumentID] [int] NOT NULL CONSTRAINT [DF_tblTransactionPay_InvoiceDocumentID] DEFAULT ((0)),
[InvoiceNumber] [int] NOT NULL CONSTRAINT [DF_tblTransactionPay_InvoiceNumber] DEFAULT ((0)),
[tp_Note] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[PayPercent] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPay_PayPercent] DEFAULT ((100)),
[PayPrecent] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPay_PayPrecent] DEFAULT ((100)),
[VatAmount] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPay_VatAmount] DEFAULT ((0)),
[TotalCaptureCount] [int] NULL,
[TotalCaptureAmount] [money] NULL,
[TotalAdminCount] [int] NULL,
[TotalAdminAmount] [money] NULL,
[TotalSystemCount] [int] NULL,
[TotalSystemAmount] [money] NULL,
[TotalRefundCount] [int] NULL,
[TotalRefundAmount] [money] NULL,
[TotalChbCount] [int] NULL,
[TotalChbAmount] [money] NULL,
[TotalFeeProcessCapture] [money] NULL,
[TotalFeeClarification] [money] NULL,
[TotalFeeFinancing] [money] NULL,
[TotalFeeHandling] [money] NULL,
[TotalFeeBank] [money] NULL,
[TotalRollingReserve] [money] NULL,
[TotalRollingRelease] [money] NULL,
[TotalDirectDeposit] [money] NULL,
[TotalFeeChb] [money] NULL,
[TotalAmountTrans] AS (([TotalCaptureAmount]-[TotalRefundAmount])-[TotalChbAmount]) PERSISTED,
[TotalAmountFee] AS ((((([TotalFeeProcessCapture]+[TotalFeeClarification])+[TotalFeeFinancing])+[TotalFeeHandling])+[TotalFeeChb])+[TotalFeeBank]) PERSISTED,
[TotalPayout] AS (round(CONVERT([money],((((((([TotalCaptureAmount]-[TotalRefundAmount])-[TotalChbAmount])+[TotalSystemAmount])+[TotalAdminAmount])-((((([TotalFeeProcessCapture]+[TotalFeeClarification])+[TotalFeeFinancing])+[TotalFeeHandling])+[TotalFeeChb])+[TotalFeeBank])*((1.0)+case [IsChargeVAT] when (1) then [VatAmount] else (0) end))-[TotalDirectDeposit])+[TotalRollingRelease])-[TotalRollingReserve],0),(2))) PERSISTED,
[Currency] [int] NULL,
[IsGradedFees] [bit] NOT NULL CONSTRAINT [DF_tblTransactionPay_IsGradedFees] DEFAULT ((0)),
[IsTotalsCached] [bit] NOT NULL CONSTRAINT [DF_tblTransactionPay_IsTotalsCached] DEFAULT ((0)),
[PaymentMethod_id] [smallint] NULL,
[TotalCashbackCount] [int] NULL,
[TotalCashbackAmount] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTransactionPay] ADD CONSTRAINT [PK_tblTransactionPay] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionPay_CompanyID] ON [dbo].[tblTransactionPay] ([CompanyID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionPay_Currency] ON [dbo].[tblTransactionPay] ([Currency]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionPay_PayDate] ON [dbo].[tblTransactionPay] ([PayDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTransactionPay] ADD CONSTRAINT [FK_tblTransactionPay_tblSystemCurrencies_Currency] FOREIGN KEY ([Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Settlements created from merchant''s transactions', 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionPay', NULL, NULL
GO
