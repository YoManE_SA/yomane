CREATE TABLE [System].[SysErrorCode]
(
[SysErrorCode_id] [int] NOT NULL IDENTITY(1, 1),
[GroupName] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL,
[LanguageISOCode] [char] (2) COLLATE Hebrew_CI_AS NOT NULL,
[ErrorCode] [int] NOT NULL,
[ErrorMessage] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[SysErrorCode] ADD CONSTRAINT [PK_SysErrorCode] PRIMARY KEY CLUSTERED  ([SysErrorCode_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'System error codes', 'SCHEMA', N'System', 'TABLE', N'SysErrorCode', NULL, NULL
GO
