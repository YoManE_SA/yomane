CREATE TABLE [List].[PaymentMethodProvider]
(
[PaymentMethodProvider_id] [varchar] (16) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_PaymentMethodProvider_IsActive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [List].[PaymentMethodProvider] ADD CONSTRAINT [PK_PaymentMethodProvider] PRIMARY KEY CLUSTERED  ([PaymentMethodProvider_id]) ON [PRIMARY]
GO
