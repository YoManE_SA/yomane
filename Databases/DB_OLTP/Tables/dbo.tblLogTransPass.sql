CREATE TABLE [dbo].[tblLogTransPass]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ltp_InsertDate] [datetime] NULL,
[ltp_Transaction] [int] NULL,
[ltp_ActionType] [int] NULL,
[ltp_User] [int] NULL,
[ltp_Username] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[ltp_IP] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[ltp_Comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogTransPass] ADD CONSTRAINT [PK_tblLogTransPass] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLogTransPass_Transaction_InsertDate] ON [dbo].[tblLogTransPass] ([ltp_Transaction], [ltp_InsertDate]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved?] Used in 2 file in member code (insert)', 'SCHEMA', N'dbo', 'TABLE', N'tblLogTransPass', NULL, NULL
GO
