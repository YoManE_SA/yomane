CREATE TABLE [dbo].[tblGeoIP]
(
[GI_Start] [bigint] NOT NULL,
[GI_End] [bigint] NOT NULL,
[GI_Diff] [bigint] NOT NULL,
[GI_IsoCode] [nvarchar] (2) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblGeoIP] ADD CONSTRAINT [PK_tblGeoIP] PRIMARY KEY CLUSTERED  ([GI_Start], [GI_End]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [tblGeoIP_GI_Diff] ON [dbo].[tblGeoIP] ([GI_Diff]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblGeoIP_Start_End_IsoCode] ON [dbo].[tblGeoIP] ([GI_Start] DESC, [GI_End]) INCLUDE ([GI_IsoCode]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'IP range to country ISO code', 'SCHEMA', N'dbo', 'TABLE', N'tblGeoIP', NULL, NULL
GO
