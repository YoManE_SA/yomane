CREATE TABLE [Data].[AccountBalance]
(
[AccountBalance_id] [int] NOT NULL IDENTITY(1, 1),
[Account_id] [int] NOT NULL,
[BalanceSourceType_id] [varchar] (30) COLLATE Hebrew_CI_AS NOT NULL,
[SourceID] [int] NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[Amount] [decimal] (10, 2) NOT NULL,
[TotalBalance] [decimal] (10, 2) NOT NULL,
[InsertDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_AccountBalance_InsertDate] DEFAULT (sysdatetime()),
[SystemText] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[IsPending] [bit] NOT NULL CONSTRAINT [DF_AccountBalance_IsPending] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [Data].[trAccountBalance_UpdateTotalBalance_Ins] ON [Data].[AccountBalance]
    AFTER INSERT
AS
    BEGIN
        UPDATE  t0
        SET     t0.TotalBalance = t0.Amount
                + ISNULL(( SELECT   SUM(t1.Amount)
                           FROM     [Data].[AccountBalance] t1
                           WHERE    t1.Account_id = t0.Account_id
                                    AND t1.CurrencyISOCode = t0.CurrencyISOCode
                                    AND t1.IsPending = t0.IsPending
                                    AND t1.AccountBalance_id < t0.AccountBalance_id
                         ), 0)
        FROM    [Data].[AccountBalance] t0
        WHERE   t0.AccountBalance_id IN ( SELECT    Inserted.AccountBalance_id
                                          FROM      Inserted )
    END
GO
ALTER TABLE [Data].[AccountBalance] ADD CONSTRAINT [PK_AccountBalance] PRIMARY KEY CLUSTERED  ([AccountBalance_id] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AccountBalance_AccountID_CurrencyISOCode] ON [Data].[AccountBalance] ([Account_id], [CurrencyISOCode]) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountBalance] ADD CONSTRAINT [FK_AccountBalance_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Data].[AccountBalance] ADD CONSTRAINT [FK_AccountBalance_BalanceSourceTypeID] FOREIGN KEY ([BalanceSourceType_id]) REFERENCES [List].[BalanceSourceType] ([BalanceSourceType_id])
GO
ALTER TABLE [Data].[AccountBalance] ADD CONSTRAINT [FK_AccountBalance_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account''s balance', 'SCHEMA', N'Data', 'TABLE', N'AccountBalance', NULL, NULL
GO
