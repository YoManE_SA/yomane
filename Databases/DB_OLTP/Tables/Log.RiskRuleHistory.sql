CREATE TABLE [Log].[RiskRuleHistory]
(
[RiskRuleHistory_id] [int] NOT NULL IDENTITY(1, 1),
[InsertDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_RiskRuleHistory_InsertDate] DEFAULT (sysdatetime()),
[Account_id] [int] NULL,
[RuleName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[RuleCode] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[RuleDescription] [varchar] (200) COLLATE Hebrew_CI_AS NULL,
[RuleMode] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[Action] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Reason] [nvarchar] (1000) COLLATE Hebrew_CI_AS NULL,
[IsRuleFail] [bit] NULL,
[TransFail_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Log].[RiskRuleHistory] ADD CONSTRAINT [PK_RiskRuleHistory] PRIMARY KEY CLUSTERED  ([RiskRuleHistory_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [Log].[RiskRuleHistory] ADD CONSTRAINT [FK_RiskRuleHistory_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Log].[RiskRuleHistory] ADD CONSTRAINT [FK_RiskRuleHistory_TransFailID] FOREIGN KEY ([TransFail_id]) REFERENCES [dbo].[tblCompanyTransFail] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log risk rules', 'SCHEMA', N'Log', 'TABLE', N'RiskRuleHistory', NULL, NULL
GO
