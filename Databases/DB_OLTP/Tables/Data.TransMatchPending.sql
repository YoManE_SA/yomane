CREATE TABLE [Data].[TransMatchPending]
(
[TransMatchPending_id] [int] NOT NULL IDENTITY(1, 1),
[ExternalServiceType_id] [varchar] (16) COLLATE Hebrew_CI_AS NULL,
[DebitCompany_id] [int] NULL,
[InsertDate] [datetime2] (0) NULL,
[FileName] [varchar] (250) COLLATE Hebrew_CI_AS NULL,
[ReasonCode] [varchar] (10) COLLATE Hebrew_CI_AS NULL,
[Amount] [decimal] (19, 2) NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[CardNumber] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[TransDate] [datetime2] (0) NULL,
[ARN] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[DebitReferenceCode] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[DebitReferenceNum] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[AcquirerReferenceNum] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[ApprovalNumber] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[MatchDate] [datetime2] (0) NULL,
[MatchCount] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[TransMatchPending] ADD CONSTRAINT [PK_TransMatchPending] PRIMARY KEY CLUSTERED  ([TransMatchPending_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[TransMatchPending] ADD CONSTRAINT [FK_TransMatchPending_CurrencyList_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Data].[TransMatchPending] ADD CONSTRAINT [FK_SetRiskRule_DebitCompany] FOREIGN KEY ([DebitCompany_id]) REFERENCES [dbo].[tblDebitCompany] ([DebitCompany_ID]) ON DELETE CASCADE
GO
ALTER TABLE [Data].[TransMatchPending] ADD CONSTRAINT [FK_TransMatchPending_ExternalServiceTypeID] FOREIGN KEY ([ExternalServiceType_id]) REFERENCES [List].[ExternalServiceType] ([ExternalServiceType_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Updates of fraud waiting to be matched to a transaction, records here are deleted after moved to trans history', 'SCHEMA', N'Data', 'TABLE', N'TransMatchPending', NULL, NULL
GO
