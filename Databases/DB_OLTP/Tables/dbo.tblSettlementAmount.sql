CREATE TABLE [dbo].[tblSettlementAmount]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ItemCount] [int] NULL,
[ReleaseDate] [datetime] NULL,
[Description] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[ReferenceNumber] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementAmount] ADD CONSTRAINT [PK_tblSettlementAmount] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Not used', 'SCHEMA', N'dbo', 'TABLE', N'tblSettlementAmount', NULL, NULL
GO
