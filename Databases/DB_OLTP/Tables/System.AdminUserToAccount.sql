CREATE TABLE [System].[AdminUserToAccount]
(
[AdminUser_id] [smallint] NOT NULL,
[Account_id] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[AdminUserToAccount] ADD CONSTRAINT [PK_AdminUserToAccount] PRIMARY KEY CLUSTERED  ([AdminUser_id], [Account_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[AdminUserToAccount] ADD CONSTRAINT [FK_AdminUserToAccount_Account_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [System].[AdminUserToAccount] ADD CONSTRAINT [FK_AdminUserToAccount_AdminUser_AdminUserID] FOREIGN KEY ([AdminUser_id]) REFERENCES [System].[AdminUser] ([AdminUser_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'System admin users to account', 'SCHEMA', N'System', 'TABLE', N'AdminUserToAccount', NULL, NULL
GO
