CREATE TABLE [Data].[PeriodicFeeType]
(
[PeriodicFeeType_id] [int] NOT NULL IDENTITY(1, 1),
[AccountType_id] [tinyint] NOT NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[Amount] [decimal] (19, 3) NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL,
[TimeInterval_id] [tinyint] NOT NULL,
[IsActive] [bit] NOT NULL,
[ProcessMerchant_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[PeriodicFeeType] ADD CONSTRAINT [PK_PeriodicFeeType] PRIMARY KEY CLUSTERED  ([PeriodicFeeType_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[PeriodicFeeType] ADD CONSTRAINT [FK_PeriodicFeeType_AccountTypeID] FOREIGN KEY ([AccountType_id]) REFERENCES [List].[AccountType] ([AccountType_id])
GO
ALTER TABLE [Data].[PeriodicFeeType] ADD CONSTRAINT [FK_PeriodicFeeType_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Data].[PeriodicFeeType] ADD CONSTRAINT [FK_PeriodicFeeType_ProcessMerchantID] FOREIGN KEY ([ProcessMerchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Data].[PeriodicFeeType] ADD CONSTRAINT [FK_PeriodicFeeType_TimeUnitID] FOREIGN KEY ([TimeInterval_id]) REFERENCES [List].[TimeUnit] ([TimeUnit_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Periodic fee types set by admin', 'SCHEMA', N'Data', 'TABLE', N'PeriodicFeeType', NULL, NULL
GO
