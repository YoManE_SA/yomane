CREATE TABLE [dbo].[FacebookPageToMerchant]
(
[FacebookPageToMerchant_id] [int] NOT NULL IDENTITY(1, 1),
[FBPageID] [bigint] NOT NULL,
[Merchant] [varchar] (7) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FacebookPageToMerchant] ADD CONSTRAINT [PK_FacebookPageToMerchant] PRIMARY KEY CLUSTERED  ([FacebookPageToMerchant_id]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'FacebookPageToMerchant', NULL, NULL
GO
