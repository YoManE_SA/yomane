CREATE TABLE [List].[TransHistoryType]
(
[TransHistoryType_id] [tinyint] NOT NULL,
[Name] [varchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[TransHistoryType] ADD CONSTRAINT [PK_TransHistoryType] PRIMARY KEY CLUSTERED  ([TransHistoryType_id]) ON [PRIMARY]
GO
