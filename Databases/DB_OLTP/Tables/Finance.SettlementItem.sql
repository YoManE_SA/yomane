CREATE TABLE [Finance].[SettlementItem]
(
[SettlementItem_id] [int] NOT NULL IDENTITY(1, 1),
[Settlement_id] [int] NOT NULL,
[AmountType_id] [tinyint] NULL,
[LineText] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL,
[Amount] [decimal] (19, 4) NULL,
[Quantity] [int] NULL,
[Total] [decimal] (19, 4) NULL,
[PercentValue] [decimal] (7, 4) NULL,
[SortOrder] [tinyint] NULL,
[IsFee] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Finance].[SettlementItem] ADD CONSTRAINT [[PK_SettlementItem] PRIMARY KEY CLUSTERED  ([SettlementItem_id]) ON [PRIMARY]
GO
ALTER TABLE [Finance].[SettlementItem] ADD CONSTRAINT [FK_SettlementItem_AmountTypeID] FOREIGN KEY ([AmountType_id]) REFERENCES [List].[AmountType] ([AmountType_id])
GO
ALTER TABLE [Finance].[SettlementItem] ADD CONSTRAINT [FK_SettlementItem_SettlementID] FOREIGN KEY ([Settlement_id]) REFERENCES [Finance].[Settlement] ([Settlement_id])
GO
