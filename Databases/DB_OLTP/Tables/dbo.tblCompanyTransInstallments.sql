CREATE TABLE [dbo].[tblCompanyTransInstallments]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[InsID] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransInstallments_InsID] DEFAULT ((0)),
[payID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransInstallments_payID] DEFAULT ((0)),
[amount] [money] NOT NULL CONSTRAINT [DF_tblCompanyTransInstallments_amount] DEFAULT ((0)),
[comment] [nchar] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransInstallments_comment] DEFAULT (''),
[MerchantPD] [smalldatetime] NOT NULL CONSTRAINT [DF_tblCompanyTransInstallments_MerchantPD] DEFAULT (''),
[MerchantRealPD] [datetime] NULL,
[CompanyID] [int] NULL,
[transAnsID] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgCompanyTransInstallmentsUpdateSetUnsettled] ON [dbo].[tblCompanyTransInstallments] FOR UPDATE AS
BEGIN
	UPDATE
		tblCompanyTransPass
	SET
		UnsettledAmount=
			CASE
				isTestOnly
			WHEN
				1
			THEN
				0
			ELSE
				CASE
					DeniedStatus
				WHEN
					0
				THEN
					Amount-IsNull((SELECT Sum(Amount) FROM tblCompanyTransInstallments WHERE transAnsID=tblCompanyTransPass.ID AND PayID>0), 0)
				WHEN
					2
				THEN
					IsNull((SELECT Sum(Amount) FROM tblCompanyTransInstallments WHERE transAnsID=tblCompanyTransPass.ID AND PayID>0), 0)-Amount
				ELSE
					0
				END
			END,
		UnsettledInstallments=Payments-IsNull((SELECT Count(*) FROM tblCompanyTransInstallments WHERE transAnsID=tblCompanyTransPass.ID AND PayID>0), 0)
	WHERE
		ID IN (SELECT transAnsID FROM Inserted);
END
GO
ALTER TABLE [dbo].[tblCompanyTransInstallments] ADD CONSTRAINT [PK_tblCompanyTransInstallments] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransInstallments_payID] ON [dbo].[tblCompanyTransInstallments] ([payID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblCompanyTransInstallments_transAnsID_InsID] ON [dbo].[tblCompanyTransInstallments] ([transAnsID] DESC, [InsID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyTransInstallments] ADD CONSTRAINT [FK_tblCompanyTransInstallments_tblCompany_CompanyID] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [dbo].[tblCompanyTransInstallments] ADD CONSTRAINT [FK_tblCompanyTransInstallments_tblCompanyTransPass_transAnsID] FOREIGN KEY ([transAnsID]) REFERENCES [dbo].[tblCompanyTransPass] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transactions installment detail', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyTransInstallments', NULL, NULL
GO
