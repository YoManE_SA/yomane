CREATE TABLE [dbo].[tblCompanyPaymentMethods]
(
[CPM_ID] [int] NOT NULL IDENTITY(1, 1),
[CPM_CreditTypeID] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyPaymentMethods_CPM_CreditTypeID] DEFAULT ((0)),
[CPM_IsDisabled] [bit] NOT NULL CONSTRAINT [DF_tblCompanyPaymentMethods_CPM_IsDisabled] DEFAULT ((0)),
[CPM_PercentFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyPaymentMethods_CPM_PercentFee] DEFAULT ((0)),
[CPM_FixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyPaymentMethods_CPM_FixedFee] DEFAULT ((0)),
[CPM_ApproveFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyPaymentMethods_CPM_ApproveFixedFee] DEFAULT ((0)),
[CPM_RefundFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyPaymentMethods_CPM_RefundFixedFee] DEFAULT ((0)),
[CPM_ClarificationFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyPaymentMethods_CPM_ClarificationFee] DEFAULT ((0)),
[CPM_CBFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyPaymentMethods_CPM_CBFixedFee] DEFAULT ((0)),
[CPM_FailFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyPaymentMethods_CPM_FailFixedFee] DEFAULT ((0)),
[CPM_CompanyID] [int] NULL,
[CPM_CurrencyID] [int] NULL,
[CPM_PaymentMethod] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyPaymentMethods] ADD CONSTRAINT [PK_tblCompanyPaymentMethods] PRIMARY KEY CLUSTERED  ([CPM_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyPaymentMethods] ADD CONSTRAINT [FK_tblCompanyPaymentMethods_tblCompany_CPM_CompanyID] FOREIGN KEY ([CPM_CompanyID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompanyPaymentMethods] ADD CONSTRAINT [FK_tblCompanyPaymentMethods_tblSystemCurrencies_CPM_CurrencyID] FOREIGN KEY ([CPM_CurrencyID]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompanyPaymentMethods] ADD CONSTRAINT [FK_tblCompanyPaymentMethods_PaymentMethod_CPM_PaymentMethod] FOREIGN KEY ([CPM_PaymentMethod]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyPaymentMethods', NULL, NULL
GO
