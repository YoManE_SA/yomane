CREATE TABLE [Trans].[TransPayerShippingDetail]
(
[TransPayerShippingDetail_id] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Street1] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Street2] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[City] [nvarchar] (60) COLLATE Hebrew_CI_AS NULL,
[PostalCode] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[StateISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[CountryISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransPayerShippingDetail] ADD CONSTRAINT [PK_TransPayerShippingDetail] PRIMARY KEY CLUSTERED  ([TransPayerShippingDetail_id]) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransPayerShippingDetail] ADD CONSTRAINT [FK_TransPayerShippingDetail_CountryList_CountryISOCode] FOREIGN KEY ([CountryISOCode]) REFERENCES [List].[CountryList] ([CountryISOCode])
GO
ALTER TABLE [Trans].[TransPayerShippingDetail] ADD CONSTRAINT [FK_TransPayerShippingDetail_StateList_StateISOCode] FOREIGN KEY ([StateISOCode]) REFERENCES [List].[StateList] ([StateISOCode])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transaction payer shipping address', 'SCHEMA', N'Trans', 'TABLE', N'TransPayerShippingDetail', NULL, NULL
GO
