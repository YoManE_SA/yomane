CREATE TABLE [Data].[ProductProperty]
(
[ProductProperty_id] [int] NOT NULL IDENTITY(1, 1),
[ParentID] [int] NULL,
[Merchant_id] [int] NULL,
[ProductPropertyType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[Value] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductProperty] ADD CONSTRAINT [PK_ProductProperty] PRIMARY KEY CLUSTERED  ([ProductProperty_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductProperty] ADD CONSTRAINT [FK_ProductProperty_tblCompany_CompanyID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Data].[ProductProperty] ADD CONSTRAINT [FK_ProductProperty_ProductProperty_ParentID] FOREIGN KEY ([ParentID]) REFERENCES [Data].[ProductProperty] ([ProductProperty_id])
GO
ALTER TABLE [Data].[ProductProperty] ADD CONSTRAINT [FK_ProductProperty_ProductPropertyTypeID] FOREIGN KEY ([ProductPropertyType_id]) REFERENCES [List].[ProductPropertyType] ([ProductPropertyType_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s product''s property (type: color/size...)', 'SCHEMA', N'Data', 'TABLE', N'ProductProperty', NULL, NULL
GO
