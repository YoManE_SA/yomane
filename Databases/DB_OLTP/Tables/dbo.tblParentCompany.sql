CREATE TABLE [dbo].[tblParentCompany]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[pc_Code] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblParentCompany_pc_Code] DEFAULT (''),
[pc_RecurringURL] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblParentCompany_pc_RecurringURL] DEFAULT (''),
[pc_IsDefault] [bit] NOT NULL CONSTRAINT [DF_tblParentCompany_pc_IsDefault] DEFAULT ((0)),
[pc_RecurringEcheckURL] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblParentCompany_pc_RecurringEcheckURL] DEFAULT (''),
[pc_SMS_URL] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblParentCompany_pc_SMS_URL] DEFAULT (''),
[pc_TemplateName] [varchar] (25) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblParentCompany] ADD CONSTRAINT [PK_tblParentCompany] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used by DB to get URLs for sending Recurring/SMS/AutoCaptureAttemptCharge', 'SCHEMA', N'dbo', 'TABLE', N'tblParentCompany', NULL, NULL
GO
