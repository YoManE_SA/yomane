CREATE TABLE [dbo].[MerchantDepartment]
(
[MerchantDepartment_id] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MerchantDepartment] ADD CONSTRAINT [PK_MerchantDepartment] PRIMARY KEY CLUSTERED  ([MerchantDepartment_id]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'List of merchant''s department', 'SCHEMA', N'dbo', 'TABLE', N'MerchantDepartment', NULL, NULL
GO
