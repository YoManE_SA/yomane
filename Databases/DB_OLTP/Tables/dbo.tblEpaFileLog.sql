CREATE TABLE [dbo].[tblEpaFileLog]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ActionDate] [datetime] NOT NULL CONSTRAINT [DF_tblEpaFileLog_ActionDate] DEFAULT (getdate()),
[ActionType] [int] NOT NULL,
[StoredFileName] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[OriginalFileName] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Details] [nvarchar] (1500) COLLATE Hebrew_CI_AS NULL,
[Username] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEpaFileLog] ADD CONSTRAINT [PK_tblEpaFileLog] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEpaFileLog] ADD CONSTRAINT [FK_tblEpaFileLog_tblEpaActionType_ActionType] FOREIGN KEY ([ActionType]) REFERENCES [dbo].[tblEpaActionType] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'EPA file import log', 'SCHEMA', N'dbo', 'TABLE', N'tblEpaFileLog', NULL, NULL
GO
