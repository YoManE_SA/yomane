CREATE TABLE [Trans].[TransPayerInfo]
(
[TransPayerInfo_id] [int] NOT NULL IDENTITY(1, 1),
[TransPayerShippingDetail_id] [int] NULL,
[FirstName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[InvoiceName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[PersonalNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[PhoneNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[EmailAddress] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[DateOfBirth] [date] NULL,
[Comment] [nvarchar] (1500) COLLATE Hebrew_CI_AS NULL,
[Echeck_id] [int] NULL,
[CreditCard_id] [int] NULL,
[Merchant_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransPayerInfo] ADD CONSTRAINT [PK_TransPayerInfo] PRIMARY KEY CLUSTERED  ([TransPayerInfo_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransPayerInfo_MerchantID] ON [Trans].[TransPayerInfo] ([Merchant_id]) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransPayerInfo] ADD CONSTRAINT [FK_TransPayerInfo_tblCompany_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Trans].[TransPayerInfo] ADD CONSTRAINT [FK_TransPayerInfo_TransPayerShippingDetail] FOREIGN KEY ([TransPayerShippingDetail_id]) REFERENCES [Trans].[TransPayerShippingDetail] ([TransPayerShippingDetail_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transaction payer info', 'SCHEMA', N'Trans', 'TABLE', N'TransPayerInfo', NULL, NULL
GO
