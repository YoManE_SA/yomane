CREATE TABLE [dbo].[tblAutoCapture]
(
[AuthorizedTransactionID] [int] NOT NULL,
[ScheduledDate] [datetime] NOT NULL,
[ActualDate] [datetime] NULL,
[CaptureTransactionID] [int] NULL,
[DeclineTransactionID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAutoCapture] ADD CONSTRAINT [PK_tblAutoCapture] PRIMARY KEY CLUSTERED  ([AuthorizedTransactionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblAutoCapture_ActualDate] ON [dbo].[tblAutoCapture] ([ActualDate] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblAutoCapture_CaptureTransactionID] ON [dbo].[tblAutoCapture] ([CaptureTransactionID] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblAutoCapture_DeclineTransactionID] ON [dbo].[tblAutoCapture] ([DeclineTransactionID] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblAutoCapture_ScheduledDate] ON [dbo].[tblAutoCapture] ([ScheduledDate] DESC) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAutoCapture] ADD CONSTRAINT [FK_tblAutoCapture_tblCompanyTransApproval_AuthorizedTransactionID] FOREIGN KEY ([AuthorizedTransactionID]) REFERENCES [dbo].[tblCompanyTransApproval] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblAutoCapture] ADD CONSTRAINT [FK_tblAutoCapture_tblCompanyTransPass_CaptureTransactionID] FOREIGN KEY ([CaptureTransactionID]) REFERENCES [dbo].[tblCompanyTransPass] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblAutoCapture] ADD CONSTRAINT [FK_tblAutoCapture_tblCompanyTransFail_DeclineTransactionID] FOREIGN KEY ([DeclineTransactionID]) REFERENCES [dbo].[tblCompanyTransFail] ([ID]) ON DELETE SET NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s setting for auto capture of pre-auth transaction', 'SCHEMA', N'dbo', 'TABLE', N'tblAutoCapture', NULL, NULL
GO
