CREATE TABLE [Finance].[WireAccount]
(
[WireAccount_id] [smallint] NOT NULL IDENTITY(1, 1),
[WireProvider_id] [varchar] (16) COLLATE Hebrew_CI_AS NOT NULL,
[AccountName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[AccountNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[AccountBranch] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Finance].[WireAccount] ADD CONSTRAINT [PK_WireAccount] PRIMARY KEY CLUSTERED  ([WireAccount_id]) ON [PRIMARY]
GO
ALTER TABLE [Finance].[WireAccount] ADD CONSTRAINT [FK_WireAccount_WireProvider] FOREIGN KEY ([WireProvider_id]) REFERENCES [List].[WireProvider] ([WireProvider_id])
GO
