CREATE TABLE [Data].[PreCreatedPaymentMethod]
(
[PreCreatedPaymentMethod_id] [int] NOT NULL IDENTITY(1, 1),
[PaymentMethodProvider_id] [varchar] (16) COLLATE Hebrew_CI_AS NULL,
[PaymentMethod_id] [smallint] NOT NULL,
[Value1Encrypted] [varbinary] (40) NULL,
[Value2Encrypted] [varbinary] (40) NULL,
[PaymentMethodText] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[Value1Last4Text] [char] (4) COLLATE Hebrew_CI_AS NULL,
[Value1First6Text] [char] (6) COLLATE Hebrew_CI_AS NULL,
[ExpirationDate] [date] NULL,
[OwnerName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[OwnerDateOfBirth] [date] NULL,
[OwnerPersonalID] [varchar] (15) COLLATE Hebrew_CI_AS NULL,
[OwnerPhoneNumber] [varchar] (30) COLLATE Hebrew_CI_AS NULL,
[EncryptionKey] [tinyint] NOT NULL,
[IsAssigned] [bit] NOT NULL CONSTRAINT [DF_PreCreatedPaymentMethod_IsAssigned] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [Data].[PreCreatedPaymentMethod] ADD CONSTRAINT [PK_PreCreatedPaymentMethod] PRIMARY KEY CLUSTERED  ([PreCreatedPaymentMethod_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[PreCreatedPaymentMethod] ADD CONSTRAINT [FK_PreCreatedPaymentMethod_PaymentMethodID] FOREIGN KEY ([PaymentMethod_id]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
ALTER TABLE [Data].[PreCreatedPaymentMethod] ADD CONSTRAINT [FK_PreCreatedPaymentMethod_PaymentMethodProviderID] FOREIGN KEY ([PaymentMethodProvider_id]) REFERENCES [List].[PaymentMethodProvider] ([PaymentMethodProvider_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary hold unassigned payment methods provided by external issuers.', 'SCHEMA', N'Data', 'TABLE', N'PreCreatedPaymentMethod', NULL, NULL
GO
