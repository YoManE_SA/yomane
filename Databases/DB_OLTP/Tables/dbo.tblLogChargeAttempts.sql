CREATE TABLE [dbo].[tblLogChargeAttempts]
(
[LogChargeAttempts_id] [int] NOT NULL IDENTITY(1, 1),
[TransactionType_id] [smallint] NULL CONSTRAINT [DF_tblLogChargeAttempts_TransactionType_id] DEFAULT ((0)),
[Lca_MerchantNumber] [varchar] (7) COLLATE Hebrew_CI_AS NULL,
[Lca_RemoteAddress] [varchar] (200) COLLATE Hebrew_CI_AS NULL,
[Lca_RequestMethod] [nvarchar] (12) COLLATE Hebrew_CI_AS NULL,
[Lca_PathTranslate] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lca_HttpReferer] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lca_QueryString] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lca_RequestForm] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lca_SessionContents] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lca_ReplyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Lca_ReplyDesc] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[Lca_TransNum] [int] NULL,
[Lca_DateStart] [datetime] NULL CONSTRAINT [DF_tblLogChargeAttempts_Lca_DateStart] DEFAULT (getdate()),
[Lca_DateEnd] [datetime] NULL,
[Lca_HttpHost] [nvarchar] (400) COLLATE Hebrew_CI_AS NULL,
[Lca_TimeString] [nvarchar] (2000) COLLATE Hebrew_CI_AS NULL,
[Lca_LocalAddr] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[Lca_RequestString] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lca_ResponseString] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lca_IsSecure] [bit] NULL,
[Lca_DebitCompanyID] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[trgLogChargeAttemptsDeleteMoveToArchive] ON [dbo].[tblLogChargeAttempts] FOR DELETE AS
INSERT INTO dbo.syn_archive_tblLogChargeAttempts
(
	LogChargeAttempts_id,
	TransactionType_id,
	Lca_MerchantNumber,
	Lca_RemoteAddress,
	Lca_RequestMethod,
	Lca_PathTranslate,
	Lca_HttpHost,
	Lca_HttpReferer,
	Lca_QueryString,
	Lca_RequestForm,
	Lca_SessionContents,
	Lca_ReplyCode,
	Lca_ReplyDesc,
	Lca_TransNum,
	Lca_DateStart,
	Lca_DateEnd,
	Lca_TimeString,
	Lca_LocalAddr,
	Lca_RequestString,
	Lca_ResponseString,
	Lca_IsSecure,
	Lca_DebitCompanyID
)
SELECT
	LogChargeAttempts_id,
	TransactionType_id,
	Lca_MerchantNumber,
	Lca_RemoteAddress,
	Lca_RequestMethod,
	Lca_PathTranslate,
	Lca_HttpHost,
	Lca_HttpReferer,
	Lca_QueryString,
	Lca_RequestForm,
	Lca_SessionContents,
	Lca_ReplyCode,
	Lca_ReplyDesc,
	Lca_TransNum,
	Lca_DateStart,
	Lca_DateEnd,
	Lca_TimeString,
	Lca_LocalAddr,
	Lca_RequestString,
	Lca_ResponseString,
	Lca_IsSecure,
	Lca_DebitCompanyID
FROM
	Deleted

GO
ALTER TABLE [dbo].[tblLogChargeAttempts] ADD CONSTRAINT [PK_tblLogChargeAttempts] PRIMARY KEY CLUSTERED  ([LogChargeAttempts_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LcaDateStart] ON [dbo].[tblLogChargeAttempts] ([Lca_DateStart] DESC) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_MerchantNumber] ON [dbo].[tblLogChargeAttempts] ([Lca_MerchantNumber]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLogChargeAttempts_TransNum_ReplyCode] ON [dbo].[tblLogChargeAttempts] ([Lca_TransNum] DESC, [Lca_ReplyCode]) WHERE ([Lca_TransNum]>(0)) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogChargeAttempts] ADD CONSTRAINT [FK_tblLogChargeAttempts_tblDebitCompany_Lca_DebitCompanyID] FOREIGN KEY ([Lca_DebitCompanyID]) REFERENCES [dbo].[tblDebitCompany] ([DebitCompany_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log charge attempts', 'SCHEMA', N'dbo', 'TABLE', N'tblLogChargeAttempts', NULL, NULL
GO
