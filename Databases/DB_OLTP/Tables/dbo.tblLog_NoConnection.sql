CREATE TABLE [dbo].[tblLog_NoConnection]
(
[LogNoConnection_id] [int] NOT NULL IDENTITY(1, 1),
[Lnc_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_InsertDate] DEFAULT (getdate()),
[Lnc_DebitReturnCode] [nchar] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_DebitReturnCode] DEFAULT (''),
[Lnc_DebitReferenceCode] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_DebitReferenceCode] DEFAULT (''),
[Lnc_companyName] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_companyName] DEFAULT (''),
[Lnc_CompanyID] [int] NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_CompanyID] DEFAULT ((0)),
[Lnc_TransactionTypeID] [int] NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_TransactionTypeID] DEFAULT ((0)),
[Lnc_Amount] [money] NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_Amount] DEFAULT ((0)),
[Lnc_Currency] [tinyint] NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_Currency] DEFAULT ((1)),
[Lnc_TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_TerminalNumber] DEFAULT (''),
[Lnc_IpAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_IpAddress] DEFAULT (''),
[Lnc_DebitCompany] [tinyint] NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_DebitCompany] DEFAULT ((0)),
[lnc_TransactionFailID] [int] NOT NULL CONSTRAINT [DF_tblLog_NoConnection_lnc_TransactionFailID] DEFAULT ((0)),
[Lnc_HTTP_Error] [nvarchar] (max) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLog_NoConnection_Lnc_HTTP_Error] DEFAULT (''),
[lnc_AutoRefundStatus] [int] NOT NULL CONSTRAINT [DF_tblLog_NoConnection_lnc_AutoRefundStatus] DEFAULT ((0)),
[lnc_AutoRefundDate] [datetime] NOT NULL CONSTRAINT [DF_tblLog_NoConnection_lnc_AutoRefundDate] DEFAULT (getdate()),
[lnc_AutoRefundReply] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLog_NoConnection_lnc_AutoRefundReply] DEFAULT ('')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgLogNoConnectionAddSetAutoRefundStatus] ON [dbo].[tblLog_NoConnection] AFTER INSERT AS
UPDATE
	tblLog_NoConnection
SET
	lnc_AutoRefundStatus=4
WHERE
	LogNoConnection_id IN
	(
		SELECT
			Inserted.LogNoConnection_id
		FROM
			Inserted
			INNER JOIN tblDebitTerminals WITH (NOLOCK) ON lnc_TerminalNumber=TerminalNumber
			INNER JOIN tblDebitCompany WITH (NOLOCK) ON DebitCompany=DebitCompany_ID AND dc_IsAutoRefund=0
	)
GO
ALTER TABLE [dbo].[tblLog_NoConnection] ADD CONSTRAINT [PK_tblLog_NoConnection] PRIMARY KEY CLUSTERED  ([LogNoConnection_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLog_NoConnection_CompanyID] ON [dbo].[tblLog_NoConnection] ([Lnc_CompanyID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLog_NoConnection_InsertDate] ON [dbo].[tblLog_NoConnection] ([Lnc_InsertDate]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log for processing attempts where debit company not answered or answered uncorrectly', 'SCHEMA', N'dbo', 'TABLE', N'tblLog_NoConnection', NULL, NULL
GO
