CREATE TABLE [dbo].[tblCompanyBatchFiles]
(
[CompanyBatchFiles_id] [int] NOT NULL IDENTITY(1, 1),
[CBFCompany_id] [int] NOT NULL CONSTRAINT [DF_tblCompanyBatchFiles_CBFCompany_id] DEFAULT ((0)),
[CBFFileName] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL,
[CBFFileType] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyBatchFiles_CBFFileType] DEFAULT ((0)),
[CBFInsDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyBatchFiles_CBFInsDate] DEFAULT (getdate()),
[CBFParseDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyBatchFiles_CBFParseDate] DEFAULT ((0)),
[CBFStatus] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyBatchFiles_CBFStatus] DEFAULT ((0)),
[CBFTotalRows] [int] NOT NULL CONSTRAINT [DF_tblCompanyBatchFiles_CBFTotalRows] DEFAULT ((0)),
[CBFRows] [int] NOT NULL CONSTRAINT [DF_tblCompanyBatchFiles_CBFRows] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyBatchFiles] ADD CONSTRAINT [PK_tblCompanyBatchFiles] PRIMARY KEY CLUSTERED  ([CompanyBatchFiles_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyBatchFiles] ADD CONSTRAINT [FK_tblCompanyBatchFiles_tblCompany_CBFCompany_id] FOREIGN KEY ([CBFCompany_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyBatchFiles', NULL, NULL
GO
