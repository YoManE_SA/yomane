CREATE TABLE [dbo].[tblExternalCardTerminal]
(
[ExternalCardTerminal_id] [int] NOT NULL IDENTITY(1, 1),
[ExternalCardProvider_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[AuthUsername] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[AuthPassword256] [varbinary] (200) NULL,
[AuthVar1] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[AuthVar2] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_tblExternalCardTerminal_IsActive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExternalCardTerminal] ADD CONSTRAINT [PK_tblExternalCardTerminal] PRIMARY KEY CLUSTERED  ([ExternalCardTerminal_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExternalCardTerminal] ADD CONSTRAINT [FK_tblExternalCardTerminal_ExternalCardProvider_ExternalCardProvider_id] FOREIGN KEY ([ExternalCardProvider_id]) REFERENCES [List].[ExternalCardProvider] ([ExternalCardProvider_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblExternalCardTerminal', NULL, NULL
GO
