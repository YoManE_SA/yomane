CREATE TABLE [List].[PhoneCarrier]
(
[PhoneCarrier_id] [smallint] NOT NULL,
[Name] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[PhoneCarrier] ADD CONSTRAINT [PK_PhoneCarrier] PRIMARY KEY CLUSTERED  ([PhoneCarrier_id]) ON [PRIMARY]
GO
