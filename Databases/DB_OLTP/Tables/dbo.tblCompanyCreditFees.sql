CREATE TABLE [dbo].[tblCompanyCreditFees]
(
[CCF_ID] [int] NOT NULL IDENTITY(1, 1),
[CCF_CompanyID] [int] NOT NULL,
[CCF_CurrencyID] [int] NOT NULL,
[CCF_PaymentMethod] [smallint] NULL,
[CCF_CreditTypeID] [int] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_CreditTypeID] DEFAULT ((0)),
[CCF_IsDisabled] [bit] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_IsDisabled] DEFAULT ((0)),
[CCF_ListBINs] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_ListBINs] DEFAULT (''),
[CCF_ExchangeTo] [smallint] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_ExchangeTo] DEFAULT ((-1)),
[CCF_TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_TerminalNumber] DEFAULT (''),
[CCF_MaxAmount] [money] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_MaxAmount] DEFAULT ((0)),
[CCF_PercentFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_PercentFee] DEFAULT ((0)),
[CCF_FixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_FixedFee] DEFAULT ((0)),
[CCF_ApproveFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_ApproveFixedFee] DEFAULT ((0)),
[CCF_RefundFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_RefundFixedFee] DEFAULT ((0)),
[CCF_ClarificationFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_ClarificationFee] DEFAULT ((0)),
[CCF_CBFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_CBFixedFee] DEFAULT ((0)),
[CCF_FailFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_FailFixedFee] DEFAULT ((0)),
[CCF_TSelMode] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_TSelMode] DEFAULT ((0)),
[CCF_PartialRefundFixedFee] [smallmoney] NOT NULL,
[CCF_FraudRefundFixedFee] [smallmoney] NOT NULL,
[CCF_PercentCashback] [decimal] (4, 2) NOT NULL CONSTRAINT [DF_tblCompanyCreditFees_CCF_PercentCashback] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyCreditFees] ADD CONSTRAINT [PK_tblCompanyCreditFees] PRIMARY KEY CLUSTERED  ([CCF_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyCreditFees_CCF_CompanyID] ON [dbo].[tblCompanyCreditFees] ([CCF_CompanyID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyCreditFees_CCF_CreditTypeID] ON [dbo].[tblCompanyCreditFees] ([CCF_CreditTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyCreditFees_CCF_CurrencyID] ON [dbo].[tblCompanyCreditFees] ([CCF_CurrencyID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyCreditFees] ADD CONSTRAINT [FK_tblCompanyCreditFees_tblCompany_CCF_CompanyID] FOREIGN KEY ([CCF_CompanyID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompanyCreditFees] ADD CONSTRAINT [FK_tblCompanyCreditFees_tblSystemCurrencies_CCF_CurrencyID] FOREIGN KEY ([CCF_CurrencyID]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompanyCreditFees] ADD CONSTRAINT [FK_tblCompanyCreditFees_PaymentMethod_CCF_PaymentMethod] FOREIGN KEY ([CCF_PaymentMethod]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s terminals fees', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyCreditFees', NULL, NULL
GO
