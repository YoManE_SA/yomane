CREATE TABLE [dbo].[tblCompanyTransFail]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TransactionTypeID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_TransactionTypeID] DEFAULT ((7)),
[CustomerID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_CustomerID] DEFAULT ((0)),
[FraudDetectionLog_id] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_FraudDetectionLog_id] DEFAULT ((0)),
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_InsertDate] DEFAULT (getdate()),
[IPAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransFail_IPAddress] DEFAULT (''),
[Amount] [money] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_Amount] DEFAULT ((0)),
[Payments] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_Payments] DEFAULT ((1)),
[CreditType] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_CreditType] DEFAULT ((0)),
[replyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransFail_replyCode] DEFAULT (''),
[OrderNumber] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransFail_OrderNumber] DEFAULT (''),
[TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransFail_TerminalNumber] DEFAULT (''),
[TransType] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_TransType] DEFAULT ((0)),
[PaymentMethod_id] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_PaymentMethod_id] DEFAULT ((1)),
[PaymentMethodID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_PaymentMethodID] DEFAULT ((0)),
[PaymentMethodDisplay] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransFail_PaymentMethodDisplay] DEFAULT (''),
[isTestOnly] [bit] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_isTestOnly] DEFAULT ((0)),
[referringUrl] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransFail_referringUrl] DEFAULT (''),
[payerIdUsed] [nvarchar] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransFail_payerIdUsed] DEFAULT (''),
[DebitReferenceCode] [nvarchar] (40) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransFail_DebitReferenceCode] DEFAULT (''),
[netpayFee_transactionCharge] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_netpayFee_transactionCharge] DEFAULT ((0)),
[PayID] [int] NULL CONSTRAINT [DF_tblCompanyTransFail_PayID] DEFAULT ((0)),
[IPCountry] [varchar] (2) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransFail_IPCountry] DEFAULT ('--'),
[ctf_JumpIndex] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_ctf_JumpIndex] DEFAULT ((0)),
[AutoRefundStatus] [int] NULL CONSTRAINT [DF_tblCompanyTransFail_AutoRefundStatus] DEFAULT ((0)),
[AutoRefundDate] [datetime] NULL,
[AutoRefundReply] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[DeclineCount] [smallint] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_DeclineCount] DEFAULT ((0)),
[DeclineSourceCount] [smallint] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_DeclineSourceCount] DEFAULT ((0)),
[DeclineReplyCount] [smallint] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_DeclineReplyCount] DEFAULT ((0)),
[IsGateway] [bit] NULL,
[PaymentMethod] [smallint] NULL,
[Currency] [int] NULL,
[CompanyID] [int] NULL,
[DebitCompanyID] [int] NULL,
[DebitFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyTransFail_DebitFee] DEFAULT ((0)),
[DebitReferenceNum] [varchar] (40) COLLATE Hebrew_CI_AS NULL,
[PhoneDetailsID] [int] NULL,
[TransSource_id] [tinyint] NULL,
[MobileDevice_id] [int] NULL,
[DebitDeclineReason] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[Comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblCompanyTransFail_Comment] DEFAULT (''),
[SystemText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[PayforText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[MerchantProduct_id] [int] NULL,
[CreditCardID] [int] NULL,
[CheckDetailsID] [int] NULL,
[PayerInfo_id] [int] NULL,
[TransPayerInfo_id] [int] NULL,
[TransPaymentMethod_id] [int] NULL,
[Is3DSecure] [bit] NULL CONSTRAINT [DF_CompanyTransFaill_Is3DSecure] DEFAULT ((0)),
[IsCardPresent] [bit] NULL
) ON [PRIMARY]
WITH
(
DATA_COMPRESSION = PAGE
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[trgCompanyTransFailAddBlockCC] ON [dbo].[tblCompanyTransFail]
    FOR INSERT
AS
    BEGIN

	-- Add CC number to card blacklist
        INSERT  INTO tblFraudCCBlackList
                ( company_id ,
                  fcbl_ccDisplay ,
                  fcbl_ccNumber256 ,
                  fcbl_ReplyCode ,
                  fcbl_BlockLevel ,
                  fcbl_comment ,
                  PaymentMethod_id
	            )
                SELECT DISTINCT
                        t.CompanyID ,
                        dbo.fnFormatCcNumToHideDigits(dbo.GetDecrypted256(CCard_Number256)) ,
                        CCard_Number256 ,
                        ReplyCode ,
                        1 ,
                        'AutoBlock ' + LTRIM(RTRIM(STR(t.ID))) + '; Code '
                        + LTRIM(RTRIM(STR(t.DebitCompanyID))) + '/'
                        + ReplyCode ,
                        PaymentMethod
                FROM    INSERTED t WITH ( NOLOCK )
                        INNER JOIN tblCreditCard c WITH ( NOLOCK ) ON CreditCardID = c.ID
                        INNER JOIN tblDebitCompanyCode r WITH ( NOLOCK ) ON t.DebitCompanyID = r.DebitCompanyID
                                                              AND ReplyCode = Code
                                                              AND BlockCC = 1;
	
	-- Add Email to item blacklist
        INSERT  INTO tblBLCommon
                ( BL_Value ,
                  BL_Type ,
                  BL_Comment
	            )
                SELECT DISTINCT
                        email ,
                        1 ,
                        'AutoBlock ' + LTRIM(RTRIM(STR(t.ID))) + '; Code '
                        + LTRIM(RTRIM(STR(t.DebitCompanyID))) + '/'
                        + ReplyCode
                FROM    INSERTED t WITH ( NOLOCK )
                        INNER JOIN tblCreditCard c WITH ( NOLOCK ) ON CreditCardID = c.ID
                        INNER JOIN tblDebitCompanyCode r WITH ( NOLOCK ) ON t.DebitCompanyID = r.DebitCompanyID
                                                              AND ReplyCode = Code
                                                              AND BlockMail = 1
                WHERE   ISNULL(email, '') <> '';
    END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[trTransFail_MoveToArchive_Del] ON [dbo].[tblCompanyTransFail] AFTER DELETE AS
BEGIN

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
	INSERT INTO dbo.syn_archive_tblCompanyTransFail
	(
		ID,
		TransactionTypeID,
		CustomerID,
		FraudDetectionLog_id,
		InsertDate,
		IPAddress,
		Amount,
		Payments,
		CreditType,
		replyCode,
		OrderNumber,
		Comment,
		TerminalNumber,
		TransType,
		PaymentMethod_id,
		PaymentMethodID,
		PaymentMethodDisplay,
		isTestOnly,
		referringUrl,
		payerIdUsed,
		DebitReferenceCode,
		netpayFee_transactionCharge,
		PayID,
		IPCountry,
		ctf_JumpIndex,
		AutoRefundStatus,
		AutoRefundDate,
		AutoRefundReply,
		DeclineCount,
		DeclineSourceCount,
		DeclineReplyCount,
		PaymentMethod,
		Currency,
		CompanyID,
		DebitCompanyID,
		IsGateway,
		CreditCardID,
		CheckDetailsID,
		DebitFee,
		DebitReferenceNum
	)
	SELECT
		ID,
		TransactionTypeID,
		CustomerID,
		FraudDetectionLog_id,
		InsertDate,
		IPAddress,
		Amount,
		Payments,
		CreditType,
		replyCode,
		OrderNumber,
		Comment,
		TerminalNumber,
		TransType,
		PaymentMethod_id,
		PaymentMethodID,
		PaymentMethodDisplay,
		isTestOnly,
		referringUrl,
		payerIdUsed,
		DebitReferenceCode,
		netpayFee_transactionCharge,
		PayID,
		IPCountry,
		ctf_JumpIndex,
		AutoRefundStatus,
		AutoRefundDate,
		AutoRefundReply,
		DeclineCount,
		DeclineSourceCount,
		DeclineReplyCount,
		PaymentMethod,
		Currency,
		CompanyID,
		DebitCompanyID,
		IsGateway,
		CreditCardID,
		CheckDetailsID,
		DebitFee,
		DebitReferenceNum
	FROM
		DELETED
	WHERE
		isTestOnly = 0;
	DELETE FROM tblCreditCard WHERE ID IN (SELECT CreditCardID FROM Deleted);
	DELETE FROM tblCheckDetails WHERE ID IN (SELECT CheckDetailsID FROM Deleted);
	
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[trTransFail_SetDebitRuleLastFailDate_Ins] ON [dbo].[tblCompanyTransFail] AFTER INSERT
AS

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
	UPDATE
		tblDebitRule
	SET
		dr_LastFailDate = GetDate()
	WHERE
		dr_IsActive = 1
		AND
		';'+dr_ReplyCodes+';' LIKE '%;'+(SELECT replyCode FROM Inserted)+';%'
		AND
		dr_DebitCompany IN (SELECT DebitCompanyID FROM Inserted);
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trTransFail_SetOldTransSource_InsUpd] ON [dbo].[tblCompanyTransFail]
    AFTER INSERT, UPDATE
AS
    IF @@ROWCOUNT = 0 
        RETURN;
    SET NOCOUNT ON;
	
    IF UPDATE(TransSource_id) 
        BEGIN
	
            UPDATE  tblCompanyTransFail
            SET     TransactionTypeID = TransSource_id
            WHERE   id IN ( SELECT  ID
                            FROM    INSERTED
                            WHERE   ISNULL(TransSource_id, 0) > 0 );
		
        END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[trTransFail_SetPMDetailID_InsUpd] ON [dbo].[tblCompanyTransFail]
AFTER INSERT, UPDATE
AS

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
	IF UPDATE(PaymentMethodID)
	BEGIN
	
		--Update PhoneDetailsID Column
		UPDATE tblCompanyTransFail SET PhoneDetailsID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID > 0 AND PaymentMethod >= 200 AND PaymentMethod <= 229);
		
		--Update CheckDetailsID Column
		UPDATE tblCompanyTransFail SET CheckDetailsID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID > 0 AND PaymentMethod >= 100 AND PaymentMethod <= 199); 
		
		--Update CreditCardID Column
		UPDATE tblCompanyTransFail SET CreditCardID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID > 0 AND PaymentMethod >= 20 AND PaymentMethod <= 49); 
		
	END



GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Object:  Trigger [trgInsertTransFailUpdTracking] Script Date: 06/01/2008 16:26:55 ******/
CREATE TRIGGER [dbo].[trTransFail_TrackMerchantActivity_Ins] ON [dbo].[tblCompanyTransFail] AFTER INSERT
AS

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
    UPDATE  Track.MerchantActivity
    SET     DateLastTransFail = SYSDATETIME()
    WHERE	Merchant_id IN ( SELECT companyID FROM INSERTED );
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [PK_tblCompanyTransFail] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90, DATA_COMPRESSION = PAGE) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransFail_companyID_InsertDate_Currency] ON [dbo].[tblCompanyTransFail] ([CompanyID], [InsertDate] DESC, [Currency]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransFail_CustomerID] ON [dbo].[tblCompanyTransFail] ([CustomerID]) WHERE ([CustomerID]>(0)) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransFail_DebitReferenceCode] ON [dbo].[tblCompanyTransFail] ([DebitReferenceCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransFail_InsertDate_CompanyID_Currency] ON [dbo].[tblCompanyTransFail] ([InsertDate], [CompanyID], [Currency]) INCLUDE ([Amount], [DebitCompanyID], [ID], [netpayFee_transactionCharge], [replyCode], [TransType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransFail_terminalNumber] ON [dbo].[tblCompanyTransFail] ([TerminalNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransFail_TransPayerInfoID] ON [dbo].[tblCompanyTransFail] ([TransPayerInfo_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_tblCheckDetails_CheckDetailsID] FOREIGN KEY ([CheckDetailsID]) REFERENCES [dbo].[tblCheckDetails] ([id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_tblCompany_CompanyID] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_tblCreditCard_CreditCardID] FOREIGN KEY ([CreditCardID]) REFERENCES [dbo].[tblCreditCard] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_tblSystemCurrencies_Currency] FOREIGN KEY ([Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_tblDebitCompany_DebitCompanyID] FOREIGN KEY ([DebitCompanyID]) REFERENCES [dbo].[tblDebitCompany] ([DebitCompany_ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_MobileDevice_MobileDevice_id] FOREIGN KEY ([MobileDevice_id]) REFERENCES [dbo].[MobileDevice] ([MobileDevice_id])
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_PaymentMethod_PaymentMethod] FOREIGN KEY ([PaymentMethod]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_PhoneDetail_PhoneDetailsID] FOREIGN KEY ([PhoneDetailsID]) REFERENCES [Trans].[PhoneDetail] ([PhoneDetail_id])
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_TransPayerInfo] FOREIGN KEY ([TransPayerInfo_id]) REFERENCES [Trans].[TransPayerInfo] ([TransPayerInfo_id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_TransPaymentMethod] FOREIGN KEY ([TransPaymentMethod_id]) REFERENCES [Trans].[TransPaymentMethod] ([TransPaymentMethod_id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [FK_tblCompanyTransFail_TransSource_TransSource_id] FOREIGN KEY ([TransSource_id]) REFERENCES [List].[TransSource] ([TransSource_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transactions decline', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyTransFail', NULL, NULL
GO
