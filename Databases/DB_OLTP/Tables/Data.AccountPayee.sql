CREATE TABLE [Data].[AccountPayee]
(
[AccountPayee_id] [int] NOT NULL IDENTITY(1, 1),
[Account_id] [int] NOT NULL,
[AccountBankAccount_id] [int] NULL,
[SearchTag] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[LegalNumber] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[CompanyName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[ContactName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[PhoneNumber] [nvarchar] (25) COLLATE Hebrew_CI_AS NULL,
[faxNumber] [nvarchar] (25) COLLATE Hebrew_CI_AS NULL,
[EmailAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[StreetAddress] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Comment] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountPayee] ADD CONSTRAINT [PK_AccountPayee] PRIMARY KEY CLUSTERED  ([AccountPayee_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountPayee] ADD CONSTRAINT [FK_AccountPayee_Account_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Data].[AccountPayee] ADD CONSTRAINT [FK_AccountPayee_AccountBankAccount_AccountBankAccountID] FOREIGN KEY ([AccountBankAccount_id]) REFERENCES [Data].[AccountBankAccount] ([AccountBankAccount_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account''s payees', 'SCHEMA', N'Data', 'TABLE', N'AccountPayee', NULL, NULL
GO
