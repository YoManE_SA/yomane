CREATE TABLE [dbo].[tblSystemBankList]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[bankCode] [int] NOT NULL CONSTRAINT [DF_tblSystemBankList_bankCode] DEFAULT ((0)),
[bankName] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSystemBankList_bankName] DEFAULT (''),
[address] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSystemBankList_address] DEFAULT (''),
[zip] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSystemBankList_zip] DEFAULT (''),
[phone] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSystemBankList_phone] DEFAULT (''),
[fax] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSystemBankList_fax] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSystemBankList] ADD CONSTRAINT [PK_tblSystemBankList] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'List of banks', 'SCHEMA', N'dbo', 'TABLE', N'tblSystemBankList', NULL, NULL
GO
