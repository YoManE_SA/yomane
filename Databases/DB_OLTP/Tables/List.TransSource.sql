CREATE TABLE [List].[TransSource]
(
[TransSource_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[NameHeb] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[NameEng] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Description] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL,
[TransSourceCode] [varchar] (10) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[TransSource] ADD CONSTRAINT [PK_TransSource] PRIMARY KEY CLUSTERED  ([TransSource_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
