CREATE TABLE [dbo].[tblChargebackReason]
(
[Brand] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL,
[ReasonCode] [int] NOT NULL,
[Title] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Description] [nvarchar] (3000) COLLATE Hebrew_CI_AS NULL,
[MediaRequired] [nvarchar] (3000) COLLATE Hebrew_CI_AS NULL,
[IsPendingChargeback] [bit] NOT NULL CONSTRAINT [DF_tblChargebackReason_IsPendingChargeback] DEFAULT ((0)),
[RefundInfo] [nvarchar] (512) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblChargebackReason] ADD CONSTRAINT [PK_tblChargebackReason] PRIMARY KEY CLUSTERED  ([Brand], [ReasonCode]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Creditcard''s issuer CHB reason list', 'SCHEMA', N'dbo', 'TABLE', N'tblChargebackReason', NULL, NULL
GO
