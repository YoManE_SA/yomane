CREATE TABLE [Trans].[TransactionAmount]
(
[TransactionAmount_id] [int] NOT NULL IDENTITY(1, 1),
[InsertDate] [datetime2] (2) NOT NULL,
[Account_id] [int] NULL,
[TransPass_id] [int] NULL,
[TransPreAuth_id] [int] NULL,
[TransFail_id] [int] NULL,
[SetSettlement_id] [int] NULL,
[SettlementType_id] [tinyint] NOT NULL,
[Settlement_id] [int] NULL,
[SettlementDate] [datetime2] (2) NULL,
[AmountType_id] [tinyint] NOT NULL,
[Installment] [int] NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[PercentValue] [decimal] (7, 4) NULL,
[PercentAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF_TransactionAmount_PercentAmount] DEFAULT ((0)),
[FixedAmount] [decimal] (19, 4) NOT NULL CONSTRAINT [DF_TransactionAmount_FixedAmount] DEFAULT ((0)),
[Total] AS ([FixedAmount]+[PercentAmount]),
[IsFee] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransactionAmount] ADD CONSTRAINT [PK_TransactionAmount] PRIMARY KEY CLUSTERED  ([TransactionAmount_id]) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransactionAmount] ADD CONSTRAINT [FK_TransactionAmount_AmountType_AmountType_id] FOREIGN KEY ([AmountType_id]) REFERENCES [List].[AmountType] ([AmountType_id])
GO
ALTER TABLE [Trans].[TransactionAmount] ADD CONSTRAINT [FK_TransactionAmount_CurrencyList_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Trans].[TransactionAmount] ADD CONSTRAINT [FK_TransactionAmount_SettlementType] FOREIGN KEY ([SettlementType_id]) REFERENCES [List].[SettlemenType] ([SettlementType_id])
GO
ALTER TABLE [Trans].[TransactionAmount] ADD CONSTRAINT [FK_TransactionAmount_tblCompanyTransFail] FOREIGN KEY ([TransFail_id]) REFERENCES [dbo].[tblCompanyTransFail] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [Trans].[TransactionAmount] ADD CONSTRAINT [FK_TransactionAmount_tblCompanyTransPass] FOREIGN KEY ([TransPass_id]) REFERENCES [dbo].[tblCompanyTransPass] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [Trans].[TransactionAmount] ADD CONSTRAINT [FK_TransactionAmount_tblCompanyTransApproval] FOREIGN KEY ([TransPreAuth_id]) REFERENCES [dbo].[tblCompanyTransApproval] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transaction amount', 'SCHEMA', N'Trans', 'TABLE', N'TransactionAmount', NULL, NULL
GO
