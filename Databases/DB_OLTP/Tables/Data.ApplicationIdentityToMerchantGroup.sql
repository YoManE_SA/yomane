CREATE TABLE [Data].[ApplicationIdentityToMerchantGroup]
(
[ApplicationIdentityToMerchantGroup_id] [int] NOT NULL IDENTITY(1, 1),
[ApplicationIdentity_id] [int] NOT NULL,
[MerchantGroup_id] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ApplicationIdentityToMerchantGroup] ADD CONSTRAINT [PK_ApplicationIdentityToMerchantGroup] PRIMARY KEY NONCLUSTERED  ([ApplicationIdentityToMerchantGroup_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ApplicationIdentityToMerchantGroup] ADD CONSTRAINT [FK_ApplicationIdentityToMerchantGroup_ApplicationIdentity] FOREIGN KEY ([ApplicationIdentity_id]) REFERENCES [Data].[ApplicationIdentity] ([ApplicationIdentity_id]) ON DELETE CASCADE
GO
ALTER TABLE [Data].[ApplicationIdentityToMerchantGroup] ADD CONSTRAINT [FK_ApplicationIdentityToMerchantGroup_MerchantGroup] FOREIGN KEY ([MerchantGroup_id]) REFERENCES [dbo].[tblMerchantGroup] ([ID]) ON DELETE CASCADE
GO
