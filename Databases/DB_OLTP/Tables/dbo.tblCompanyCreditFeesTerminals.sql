CREATE TABLE [dbo].[tblCompanyCreditFeesTerminals]
(
[CCFT_ID] [int] NOT NULL IDENTITY(1, 1),
[CCFT_Terminal] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyCreditFeesTerminals_CCFT_Terminal] DEFAULT (''),
[CCFT_Ratio] [int] NOT NULL CONSTRAINT [DF_tblCompanyCreditFeesTerminals_CCFT_Ratio] DEFAULT ((0)),
[CCFT_UseCount] [int] NOT NULL CONSTRAINT [DF_tblCompanyCreditFeesTerminals_CCFT_UseCount] DEFAULT ((0)),
[CCFT_CompanyID] [int] NULL,
[CCFT_CCF_ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyCreditFeesTerminals] ADD CONSTRAINT [PK_tblCompanyCreditFeesTerminals] PRIMARY KEY CLUSTERED  ([CCFT_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyCreditFeesTerminals] ADD CONSTRAINT [FK_tblCompanyCreditFeesTerminals_tblCompanyCreditFees_CCFT_CCF_ID] FOREIGN KEY ([CCFT_CCF_ID]) REFERENCES [dbo].[tblCompanyCreditFees] ([CCF_ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompanyCreditFeesTerminals] ADD CONSTRAINT [FK_tblCompanyCreditFeesTerminals_tblCompany_CCFT_CompanyID] FOREIGN KEY ([CCFT_CompanyID]) REFERENCES [dbo].[tblCompany] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s terminals', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyCreditFeesTerminals', NULL, NULL
GO
