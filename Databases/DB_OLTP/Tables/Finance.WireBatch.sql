CREATE TABLE [Finance].[WireBatch]
(
[WireBatch_id] [int] NOT NULL IDENTITY(1, 1),
[WireProvider_Id] [varchar] (16) COLLATE Hebrew_CI_AS NULL,
[WireCount] [smallint] NOT NULL,
[InsertDate] [datetime2] (0) NOT NULL,
[InsertUserName] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[BatchStatus] [tinyint] NULL,
[BatchAmount] [decimal] (19, 2) NOT NULL,
[BatchCurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[ConfirmationFileName] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[ProviderReference1] [varchar] (40) COLLATE Hebrew_CI_AS NULL,
[SourceAccountText] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[IsSendRequire] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Finance].[WireBatch] ADD CONSTRAINT [PK_WireBatch] PRIMARY KEY CLUSTERED  ([WireBatch_id]) WITH (FILLFACTOR=95) ON [PRIMARY]
GO
ALTER TABLE [Finance].[WireBatch] ADD CONSTRAINT [FK_WireBatch_[BatchCurrencyISOCode] FOREIGN KEY ([BatchCurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Finance].[WireBatch] ADD CONSTRAINT [FK_WireBatch_WireProviderID] FOREIGN KEY ([WireProvider_Id]) REFERENCES [List].[WireProvider] ([WireProvider_id])
GO
