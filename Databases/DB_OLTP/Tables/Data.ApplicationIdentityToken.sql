CREATE TABLE [Data].[ApplicationIdentityToken]
(
[ApplicationIdentityToken_id] [int] NOT NULL IDENTITY(1, 1),
[ApplicationIdentity_id] [int] NOT NULL,
[Title] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL,
[IPAddressAllowed] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[Token] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Applicati__Token__5D33C24C] DEFAULT (newid())
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ApplicationIdentityToken] ADD CONSTRAINT [PK_ApplicationIdentityToken] PRIMARY KEY NONCLUSTERED  ([ApplicationIdentityToken_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ApplicationIdentityToken] ADD CONSTRAINT [FK_ApplicationIdentityToken_ApplicationIdentity] FOREIGN KEY ([ApplicationIdentity_id]) REFERENCES [Data].[ApplicationIdentity] ([ApplicationIdentity_id]) ON DELETE CASCADE
GO
