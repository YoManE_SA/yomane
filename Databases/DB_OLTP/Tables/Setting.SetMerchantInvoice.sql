CREATE TABLE [Setting].[SetMerchantInvoice]
(
[SetMerchantInvoice_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[ExternalProviderID] [tinyint] NOT NULL CONSTRAINT [DF_SetMerchantInvoice_ExternalProviderID] DEFAULT ((1)),
[ExternalUseName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[ExternalUserID] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[ExternalPassword] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[ItemText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[IsEnable] [bit] NOT NULL CONSTRAINT [DF_SetMerchantInvoice_IsEnable] DEFAULT ((0)),
[IsAutoGenerateILS] [bit] NOT NULL CONSTRAINT [DF_SetMerchantInvoice_IsAutoGenerateILS] DEFAULT ((0)),
[IsAutoGenerateOther] [bit] NOT NULL CONSTRAINT [DF_SetMerchantInvoice_IsAutoGenerateOther] DEFAULT ((0)),
[IsAutoGenerateRefund] [bit] NOT NULL CONSTRAINT [DF_SetMerchantInvoice_IsAutoGenerateRefund] DEFAULT ((0)),
[IsIncludeTax] [bit] NOT NULL CONSTRAINT [DF_SetMerchantInvoice_IsIncludeTax] DEFAULT ((0)),
[IsCreateInvoice] [bit] NOT NULL CONSTRAINT [DF_SetMerchantInvoice_IsCreateInvoice] DEFAULT ((0)),
[IsCreateReceipt] [bit] NOT NULL CONSTRAINT [DF_SetMerchantInvoice_IsCreateReceipt] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantInvoice] ADD CONSTRAINT [PK_SetMerchantInvoice] PRIMARY KEY CLUSTERED  ([SetMerchantInvoice_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantInvoice] ADD CONSTRAINT [UIX_SetMerchantInvoiceMerchant_id] UNIQUE NONCLUSTERED  ([Merchant_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantInvoice] ADD CONSTRAINT [FK_SetMerchantInvoice_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
