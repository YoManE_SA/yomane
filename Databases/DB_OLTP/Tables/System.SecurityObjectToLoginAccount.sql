CREATE TABLE [System].[SecurityObjectToLoginAccount]
(
[SecurityObject_id] [int] NOT NULL,
[LoginAccount_id] [int] NOT NULL,
[Value] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[SecurityObjectToLoginAccount] ADD CONSTRAINT [PK_SecurityObjectToLoginAccount] PRIMARY KEY CLUSTERED  ([SecurityObject_id], [LoginAccount_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[SecurityObjectToLoginAccount] ADD CONSTRAINT [FK_SecurityObjectToLoginAccount_LoginAccount_LoginAccountID] FOREIGN KEY ([LoginAccount_id]) REFERENCES [Data].[LoginAccount] ([LoginAccount_id]) ON DELETE CASCADE
GO
ALTER TABLE [System].[SecurityObjectToLoginAccount] ADD CONSTRAINT [FK_SecurityObjectToLoginAccount_SecurityObject_SecurityObjectID] FOREIGN KEY ([SecurityObject_id]) REFERENCES [System].[SecurityObject] ([SecurityObject_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'System security objects to login account', 'SCHEMA', N'System', 'TABLE', N'SecurityObjectToLoginAccount', NULL, NULL
GO
