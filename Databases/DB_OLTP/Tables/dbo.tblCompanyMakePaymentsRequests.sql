CREATE TABLE [dbo].[tblCompanyMakePaymentsRequests]
(
[CompanyMakePaymentsRequests_id] [int] NOT NULL IDENTITY(1, 1),
[paymentType] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_paymentType] DEFAULT ((0)),
[paymentDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_paymentDate] DEFAULT (getdate()),
[paymentAmount] [float] NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_paymentAmount] DEFAULT ((0)),
[paymentExchangeRate] [float] NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_paymentExchangeRate] DEFAULT ((0)),
[paymentMerchantComment] [nvarchar] (250) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_paymentMerchantComment] DEFAULT (''),
[bankIsraelInfo_PayeeName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_bankIsraelInfo_PayeeName] DEFAULT (''),
[bankIsraelInfo_CompanyLegalNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_bankIsraelInfo_CompanyLegalNumber] DEFAULT (''),
[bankIsraelInfo_personalIdNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_bankIsraelInfo_personalIdNumber] DEFAULT (''),
[bankIsraelInfo_bankBranch] [nvarchar] (5) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_bankIsraelInfo_bankBranch] DEFAULT (''),
[bankIsraelInfo_AccountNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_bankIsraelInfo_AccountNumber] DEFAULT (''),
[bankIsraelInfo_PaymentMethod] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_bankIsraelInfo_PaymentMethod] DEFAULT (''),
[bankIsraelInfo_BankCode] [int] NOT NULL CONSTRAINT [DF_tblCompanyMakePaymentsRequests_bankIsraelInfo_BankCode] DEFAULT ((0)),
[Company_id] [int] NULL,
[CompanyMakePaymentsProfiles_id] [int] NULL,
[paymentCurrency] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyMakePaymentsRequests] ADD CONSTRAINT [PK_tblCompanyMakePaymentsRequests] PRIMARY KEY CLUSTERED  ([CompanyMakePaymentsRequests_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyMakePaymentsRequests] ADD CONSTRAINT [FK_tblCompanyMakePaymentsRequests_tblCompany_Company_id] FOREIGN KEY ([Company_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompanyMakePaymentsRequests] ADD CONSTRAINT [FK_tblCompanyMakePaymentsRequests_tblCompanyMakePaymentsProfiles_CompanyMakePaymentsProfiles_id] FOREIGN KEY ([CompanyMakePaymentsProfiles_id]) REFERENCES [dbo].[tblCompanyMakePaymentsProfiles] ([CompanyMakePaymentsProfiles_id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyMakePaymentsRequests] ADD CONSTRAINT [FK_tblCompanyMakePaymentsRequests_tblSystemCurrencies_paymentCurrency] FOREIGN KEY ([paymentCurrency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s request to wire money to beneficiary ', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyMakePaymentsRequests', NULL, NULL
GO
