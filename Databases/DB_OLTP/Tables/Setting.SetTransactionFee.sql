CREATE TABLE [Setting].[SetTransactionFee]
(
[SetTransactionFee_id] [int] NOT NULL IDENTITY(1, 1),
[FeeCalcMethod_id] [tinyint] NOT NULL,
[AmountType_id] [tinyint] NOT NULL,
[SettlementType_id] [tinyint] NOT NULL,
[Account_id] [int] NULL,
[SetTransactionFloor_id] [int] NULL,
[PercentValue] [decimal] (7, 4) NULL,
[FixedAmount] [decimal] (19, 4) NULL,
[SettlementPercentValue] [decimal] (7, 4) NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetTransactionFee] ADD CONSTRAINT [PK_SetTransactionFee] PRIMARY KEY CLUSTERED  ([SetTransactionFee_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetTransactionFee] ADD CONSTRAINT [FK_SetTransactionFee_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id]) ON DELETE CASCADE
GO
ALTER TABLE [Setting].[SetTransactionFee] ADD CONSTRAINT [FK_SetTransactionFee_AmountType_AmountType_id] FOREIGN KEY ([AmountType_id]) REFERENCES [List].[AmountType] ([AmountType_id])
GO
ALTER TABLE [Setting].[SetTransactionFee] ADD CONSTRAINT [FK_SetTransactionFee_CurrencyList_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Setting].[SetTransactionFee] ADD CONSTRAINT [FK_SetTransactionFee_FeeCalcMethod_FeeCalcMethod_id] FOREIGN KEY ([FeeCalcMethod_id]) REFERENCES [List].[FeeCalcMethod] ([FeeCalcMethod_id])
GO
ALTER TABLE [Setting].[SetTransactionFee] ADD CONSTRAINT [FK_SetTransactionFee_SettlementType_SettlementType_id] FOREIGN KEY ([SettlementType_id]) REFERENCES [List].[SettlemenType] ([SettlementType_id])
GO
ALTER TABLE [Setting].[SetTransactionFee] ADD CONSTRAINT [FK_SetTransactionFee_SetTransactionFloor_SetTransactionFloor_id] FOREIGN KEY ([SetTransactionFloor_id]) REFERENCES [Setting].[SetTransactionFloor] ([SetTransactionFloor_id])
GO
