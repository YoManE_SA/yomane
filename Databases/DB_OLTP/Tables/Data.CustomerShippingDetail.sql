CREATE TABLE [Data].[CustomerShippingDetail]
(
[CustomerShippingDetail_id] [int] NOT NULL IDENTITY(1, 1),
[Customer_id] [int] NOT NULL,
[AccountAddress_id] [int] NULL,
[IsDefault] [bit] NOT NULL CONSTRAINT [DF__CustomerS__IsDef__5C89B6D6] DEFAULT ((0)),
[Title] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Comment] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[CustomerShippingDetail] ADD CONSTRAINT [PK_CustomerShippingDetail] PRIMARY KEY CLUSTERED  ([CustomerShippingDetail_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[CustomerShippingDetail] WITH NOCHECK ADD CONSTRAINT [FK_CustomerShippingDetail_AccountAddress] FOREIGN KEY ([AccountAddress_id]) REFERENCES [Data].[AccountAddress] ([AccountAddress_id]) ON DELETE SET NULL
GO
ALTER TABLE [Data].[CustomerShippingDetail] WITH NOCHECK ADD CONSTRAINT [FK_CustomerShippingDetail_CustomerID] FOREIGN KEY ([Customer_id]) REFERENCES [Data].[Customer] ([Customer_id])
GO
ALTER TABLE [Data].[CustomerShippingDetail] NOCHECK CONSTRAINT [FK_CustomerShippingDetail_AccountAddress]
GO
ALTER TABLE [Data].[CustomerShippingDetail] NOCHECK CONSTRAINT [FK_CustomerShippingDetail_CustomerID]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Customer''s shipping details', 'SCHEMA', N'Data', 'TABLE', N'CustomerShippingDetail', NULL, NULL
GO
