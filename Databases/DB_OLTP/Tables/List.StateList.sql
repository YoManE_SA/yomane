CREATE TABLE [List].[StateList]
(
[StateISOCode] [char] (2) COLLATE Hebrew_CI_AS NOT NULL,
[CountryISOCode] [char] (2) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (60) COLLATE Hebrew_CI_AS NOT NULL,
[SortOrder] [tinyint] NULL,
[StateID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[StateList] ADD CONSTRAINT [PK_StateList] PRIMARY KEY CLUSTERED  ([StateISOCode]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [List].[StateList] ADD CONSTRAINT [UIX_StateList_StateID] UNIQUE NONCLUSTERED  ([StateID]) ON [PRIMARY]
GO
ALTER TABLE [List].[StateList] ADD CONSTRAINT [FK_StateList_CountryList_CountryISOCode] FOREIGN KEY ([CountryISOCode]) REFERENCES [List].[CountryList] ([CountryISOCode])
GO
