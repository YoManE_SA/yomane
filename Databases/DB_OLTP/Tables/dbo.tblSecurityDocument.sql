CREATE TABLE [dbo].[tblSecurityDocument]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[sd_URL] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSecurityDocument_sd_URL] DEFAULT (''),
[sd_Title] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSecurityDocument_sd_Title] DEFAULT ('Untitled'),
[sd_IsManaged] [bit] NOT NULL CONSTRAINT [DF_tblSecurityDocument_sd_IsManaged] DEFAULT ((0)),
[sd_IsLogged] [bit] NOT NULL CONSTRAINT [DF_tblSecurityDocument_sd_IsLogged] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgSecurityDocumentAdd] ON [dbo].[tblSecurityDocument] FOR INSERT AS
BEGIN
	INSERT INTO
		tblSecurityDocumentGroup (sdg_Document, sdg_Group)
		SELECT Inserted.ID, tblSecurityGroup.ID FROM Inserted, tblSecurityGroup
	INSERT INTO
		tblSecurityDocumentGroup (sdg_Document, sdg_Group)
		SELECT Inserted.ID, -tblSecurityUser.ID FROM Inserted, tblSecurityUser Where tblSecurityUser.su_IsActive=1
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgSecurityDocumentRemove] ON [dbo].[tblSecurityDocument] FOR DELETE AS
DELETE FROM tblSecurityDocumentGroup WHERE sdg_Document IN (SELECT ID FROM Deleted)
GO
ALTER TABLE [dbo].[tblSecurityDocument] ADD CONSTRAINT [PK_tblSecurityDocument] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds Admincash pages list (Replace by Admin new security method)', 'SCHEMA', N'dbo', 'TABLE', N'tblSecurityDocument', NULL, NULL
GO
