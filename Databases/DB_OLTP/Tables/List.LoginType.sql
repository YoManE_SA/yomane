CREATE TABLE [List].[LoginType]
(
[LoginType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[LoginType] ADD CONSTRAINT [PK_LoginType] PRIMARY KEY CLUSTERED  ([LoginType_id]) ON [PRIMARY]
GO
