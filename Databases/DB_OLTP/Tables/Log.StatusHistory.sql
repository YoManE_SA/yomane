CREATE TABLE [Log].[StatusHistory]
(
[StatusHistory_id] [int] NOT NULL IDENTITY(1, 1),
[StatusHistoryType_id] [varchar] (16) COLLATE Hebrew_CI_AS NOT NULL,
[ActionStatus_id] [tinyint] NOT NULL,
[SourceIdentity] [int] NOT NULL,
[InsertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_StatusHistory_InsertDate] DEFAULT (sysdatetime()),
[InsertUserName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[InsertIPAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[VariableChar] [nvarchar] (1000) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Log].[StatusHistory] ADD CONSTRAINT [PK_StatusHistory] PRIMARY KEY CLUSTERED  ([StatusHistory_id]) ON [PRIMARY]
GO
ALTER TABLE [Log].[StatusHistory] ADD CONSTRAINT [FK_StatusHistory_ActionStatusID] FOREIGN KEY ([ActionStatus_id]) REFERENCES [List].[ActionStatus] ([ActionStatus_id])
GO
ALTER TABLE [Log].[StatusHistory] ADD CONSTRAINT [FK_StatusHistory_StatusHistoryTypeID] FOREIGN KEY ([StatusHistoryType_id]) REFERENCES [List].[StatusHistoryType] ([StatusHistoryType_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log status changes', 'SCHEMA', N'Log', 'TABLE', N'StatusHistory', NULL, NULL
GO
