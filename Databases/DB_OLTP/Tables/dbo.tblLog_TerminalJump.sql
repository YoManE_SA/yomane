CREATE TABLE [dbo].[tblLog_TerminalJump]
(
[ltj_ID] [int] NOT NULL IDENTITY(1, 1),
[ltj_GroupID] [int] NOT NULL,
[ltj_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblLog_TerminalJump_ltj_InsertDate] DEFAULT (getdate()),
[ltj_CompanyID] [int] NOT NULL,
[ltj_TransID] [int] NOT NULL CONSTRAINT [DF_tblLog_TerminalJump_ltj_TransID] DEFAULT ((0)),
[ltj_TransReply] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL,
[ltj_DebitCompanyID] [int] NOT NULL,
[ltj_TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLog_TerminalJump_ltj_TerminalNumber] DEFAULT (''),
[ltj_JumpIndex] [int] NOT NULL CONSTRAINT [DF_tblLog_TerminalJump_ltj_JumpIndex] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLog_TerminalJump] ADD CONSTRAINT [PK_tblLog_TerminalJump] PRIMARY KEY CLUSTERED  ([ltj_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLog_TerminalJump_ltj_GroupID] ON [dbo].[tblLog_TerminalJump] ([ltj_GroupID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLog_TerminalJump_ltj_InsertDate] ON [dbo].[tblLog_TerminalJump] ([ltj_InsertDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLog_TerminalJump_ltj_TransID] ON [dbo].[tblLog_TerminalJump] ([ltj_TransID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log usage of terminal jump (cascading)', 'SCHEMA', N'dbo', 'TABLE', N'tblLog_TerminalJump', NULL, NULL
GO
