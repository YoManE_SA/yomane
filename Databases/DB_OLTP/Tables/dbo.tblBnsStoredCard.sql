CREATE TABLE [dbo].[tblBnsStoredCard]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Identifier256] [varbinary] (200) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBnsStoredCard] ADD CONSTRAINT [PK_tblBnsStoredCard] PRIMARY KEY CLUSTERED  ([ID] DESC) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblBnsStoredCard_Identifier256] ON [dbo].[tblBnsStoredCard] ([Identifier256]) INCLUDE ([ID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'credit card storage on debit company BNS', 'SCHEMA', N'dbo', 'TABLE', N'tblBnsStoredCard', NULL, NULL
GO
