CREATE TABLE [System].[AppModule]
(
[AppModule_id] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL,
[Version] [decimal] (5, 2) NOT NULL,
[InstallDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_AppModule_InstallDate] DEFAULT (sysdatetime()),
[IsInstalled] [bit] NOT NULL,
[IsActive] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[AppModule] ADD CONSTRAINT [PK_ModuleID] PRIMARY KEY CLUSTERED  ([AppModule_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[AppModule] ADD CONSTRAINT [UIX_AppModule_Name] UNIQUE NONCLUSTERED  ([Name]) ON [PRIMARY]
GO
