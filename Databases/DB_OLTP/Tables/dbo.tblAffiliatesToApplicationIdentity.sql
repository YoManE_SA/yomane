CREATE TABLE [dbo].[tblAffiliatesToApplicationIdentity]
(
[Affiliates_id] [int] NOT NULL,
[ApplicationIdentity_id] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliatesToApplicationIdentity] ADD CONSTRAINT [PK_AffiliatesToApplicationIdentity] PRIMARY KEY CLUSTERED  ([Affiliates_id], [ApplicationIdentity_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliatesToApplicationIdentity] ADD CONSTRAINT [FK_AffiliatesToApplicationIdentity_AffiliatesID] FOREIGN KEY ([Affiliates_id]) REFERENCES [dbo].[tblAffiliates] ([affiliates_id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblAffiliatesToApplicationIdentity] ADD CONSTRAINT [FK_AffiliatesToApplicationIdentity_ApplicationIdentityID] FOREIGN KEY ([ApplicationIdentity_id]) REFERENCES [Data].[ApplicationIdentity] ([ApplicationIdentity_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Affiliate to Application Identity', 'SCHEMA', N'dbo', 'TABLE', N'tblAffiliatesToApplicationIdentity', NULL, NULL
GO
