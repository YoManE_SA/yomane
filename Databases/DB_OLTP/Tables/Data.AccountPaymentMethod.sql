CREATE TABLE [Data].[AccountPaymentMethod]
(
[AccountPaymentMethod_id] [int] NOT NULL IDENTITY(1, 1),
[Account_id] [int] NOT NULL,
[AccountAddress_id] [int] NULL,
[PaymentMethod_id] [smallint] NOT NULL,
[IsDefault] [bit] NOT NULL CONSTRAINT [DF_AccountPaymentMethod_IsDefault] DEFAULT ((0)),
[Title] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[OwnerName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[OwnerPersonalID] [varchar] (15) COLLATE Hebrew_CI_AS NULL,
[OwnerDateOfBirth] [date] NULL,
[ExpirationDate] [date] NULL,
[Value1Encrypted] [varbinary] (40) NULL,
[Value2Encrypted] [varbinary] (40) NULL,
[PaymentMethodText] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[Value1Last4Text] [char] (4) COLLATE Hebrew_CI_AS NULL,
[Value1First6Text] [char] (6) COLLATE Hebrew_CI_AS NULL,
[IssuerCountryIsoCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[EncryptionKey] [tinyint] NOT NULL,
[PaymentMethodProvider_id] [varchar] (16) COLLATE Hebrew_CI_AS NULL,
[PaymentMethodStatus_id] [tinyint] NOT NULL CONSTRAINT [DF_PaymentMethodProvider_PaymentMethodStatusID] DEFAULT ((0)),
[ProviderReference1] [varchar] (32) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountPaymentMethod] ADD CONSTRAINT [PK_AccountPaymentMethod] PRIMARY KEY CLUSTERED  ([AccountPaymentMethod_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AccountPaymentMethod_Value1Encrypted] ON [Data].[AccountPaymentMethod] ([Value1Encrypted]) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountPaymentMethod] ADD CONSTRAINT [FK_AccountPaymentMethod_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Data].[AccountPaymentMethod] ADD CONSTRAINT [FK_AccountPaymentMethod_AccountAddress] FOREIGN KEY ([AccountAddress_id]) REFERENCES [Data].[AccountAddress] ([AccountAddress_id]) ON DELETE SET NULL
GO
ALTER TABLE [Data].[AccountPaymentMethod] ADD CONSTRAINT [FK_AccountPaymentMethod_IssuerCountryIsoCode] FOREIGN KEY ([IssuerCountryIsoCode]) REFERENCES [List].[CountryList] ([CountryISOCode])
GO
ALTER TABLE [Data].[AccountPaymentMethod] ADD CONSTRAINT [FK_AccountPaymentMethod_PaymentMethodID] FOREIGN KEY ([PaymentMethod_id]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
ALTER TABLE [Data].[AccountPaymentMethod] ADD CONSTRAINT [FK_AccountPaymentMethod_PaymentMethodProviderID] FOREIGN KEY ([PaymentMethodProvider_id]) REFERENCES [List].[PaymentMethodProvider] ([PaymentMethodProvider_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Hold the account personal PM + PM provided by the gateway ', 'SCHEMA', N'Data', 'TABLE', N'AccountPaymentMethod', NULL, NULL
GO
