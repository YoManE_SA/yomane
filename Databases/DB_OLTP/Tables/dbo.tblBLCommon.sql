CREATE TABLE [dbo].[tblBLCommon]
(
[BL_ID] [int] NOT NULL IDENTITY(1, 1),
[BL_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblBLCommon_BL_InsertDate] DEFAULT (getdate()),
[BL_CompanyID] [int] NULL,
[BL_BlockLevel] [tinyint] NOT NULL CONSTRAINT [DF_tblBLCommon_BL_BlockLevel] DEFAULT ((0)),
[BL_Value] [nvarchar] (max) COLLATE Hebrew_CI_AS NOT NULL,
[BL_Type] [tinyint] NOT NULL CONSTRAINT [DF_tblBLCommon_BL_Type] DEFAULT ((0)),
[BL_Comment] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBLCommon_BL_Comment] DEFAULT (''),
[BL_User] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBLCommon_BL_User] DEFAULT (user_name()),
[BL_BlockSourceID] [tinyint] NOT NULL CONSTRAINT [DF_tblBLCommon_BL_BlockSourceID] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBLCommon] ADD CONSTRAINT [PK_tblBLCommon] PRIMARY KEY CLUSTERED  ([BL_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBLCommon] ADD CONSTRAINT [FK_tblBLCommon_BlockLevel_BL_BlockLevel] FOREIGN KEY ([BL_BlockLevel]) REFERENCES [List].[BlockLevel] ([BlockLevel_id])
GO
ALTER TABLE [dbo].[tblBLCommon] ADD CONSTRAINT [FK_tblBLCommon_tblCompany_BL_CompanyID] FOREIGN KEY ([BL_CompanyID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Item black list (email / name / IP...)', 'SCHEMA', N'dbo', 'TABLE', N'tblBLCommon', NULL, NULL
GO
