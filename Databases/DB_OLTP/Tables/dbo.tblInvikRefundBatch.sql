CREATE TABLE [dbo].[tblInvikRefundBatch]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[irb_RefundRequest] [int] NOT NULL CONSTRAINT [DF_tblInvikRefundBatch_irb_RefundRequest] DEFAULT ((0)),
[irb_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblInvikRefundBatch_irb_InsertDate] DEFAULT (getdate()),
[irb_User] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvikRefundBatch_irb_User] DEFAULT (user_name()),
[irb_IsDownloaded] [bit] NOT NULL CONSTRAINT [DF_tblInvikRefundBatch_irb_IsDownloaded] DEFAULT ((0)),
[irb_DownloadDate] [datetime] NOT NULL CONSTRAINT [DF_tblInvikRefundBatch_irb_DownloadDate] DEFAULT (getdate()),
[irb_DownloadFileNumber] [int] NOT NULL CONSTRAINT [DF_tblInvikRefundBatch_irb_DownloadFileNumber] DEFAULT ((0))
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[Remove after Admincash deleted] Invik bank refund request batch file log ', 'SCHEMA', N'dbo', 'TABLE', N'tblInvikRefundBatch', NULL, NULL
GO

ALTER TABLE [dbo].[tblInvikRefundBatch] ADD CONSTRAINT [PK_tblInvikRefundBatch] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblInvikRefundBatch_RefundRequest] ON [dbo].[tblInvikRefundBatch] ([irb_RefundRequest] DESC) ON [PRIMARY]
GO
