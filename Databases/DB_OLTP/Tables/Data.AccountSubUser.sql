CREATE TABLE [Data].[AccountSubUser]
(
[AccountSubUser_id] [int] NOT NULL IDENTITY(1, 1),
[Account_id] [int] NOT NULL,
[LoginAccount_id] [int] NOT NULL,
[Description] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountSubUser] ADD CONSTRAINT [PK_SecurityObjectToLoginAccount] PRIMARY KEY CLUSTERED  ([AccountSubUser_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountSubUser] ADD CONSTRAINT [FK_AccountSubUser_Account_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id]) ON DELETE CASCADE
GO
ALTER TABLE [Data].[AccountSubUser] ADD CONSTRAINT [FK_AccountSubUser_LoginAccount_LoginAccountID] FOREIGN KEY ([LoginAccount_id]) REFERENCES [Data].[LoginAccount] ([LoginAccount_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Sub users of account ', 'SCHEMA', N'Data', 'TABLE', N'AccountSubUser', NULL, NULL
GO
