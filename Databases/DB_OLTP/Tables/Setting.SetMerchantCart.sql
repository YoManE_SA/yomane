CREATE TABLE [Setting].[SetMerchantCart]
(
[Merchant_id] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[IsKeepCart] [bit] NOT NULL,
[IsAllowPreAuth] [bit] NOT NULL,
[IsAllowDynamicProduct] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantCart] ADD CONSTRAINT [PK_SetMerchantCart] PRIMARY KEY CLUSTERED  ([Merchant_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantCart] ADD CONSTRAINT [FK_SetMerchantCart_tblCompany_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
