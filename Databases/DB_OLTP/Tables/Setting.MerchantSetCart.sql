CREATE TABLE [Setting].[MerchantSetCart]
(
[Merchant_id] [int] NOT NULL,
[LogoFileName] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[BannerFileName] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[BannerLinkUrl] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Facebook] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Twitter] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Vimeo] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Youtube] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Linkedin] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Pinterest] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_GooglePlus] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[ShopSubDomainName] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[UIBaseColor] [varchar] (7) COLLATE Hebrew_CI_AS NULL,
[IsEnabled] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[MerchantSetCart] ADD CONSTRAINT [PK_MerchantSetCart] PRIMARY KEY CLUSTERED  ([Merchant_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[MerchantSetCart] ADD CONSTRAINT [FK_MerchantSetCart_tblCompany_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
