CREATE TABLE [dbo].[tblLogPaymentPage]
(
[LogPaymentPage_id] [int] NOT NULL IDENTITY(1, 1),
[Lpp_MerchantNumber] [varchar] (7) COLLATE Hebrew_CI_AS NULL,
[Lpp_RemoteAddress] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[Lpp_RequestMethod] [nvarchar] (12) COLLATE Hebrew_CI_AS NULL,
[Lpp_PathTranslate] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lpp_HttpHost] [nvarchar] (400) COLLATE Hebrew_CI_AS NULL,
[Lpp_HttpReferer] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lpp_QueryString] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lpp_RequestForm] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lpp_SessionContents] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lpp_ReplyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Lpp_ReplyDesc] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[Lpp_TransNum] [int] NULL,
[Lpp_DateStart] [datetime] NULL CONSTRAINT [DF_tblLogPaymentPage_Lpp_DateStart] DEFAULT (getdate()),
[Lpp_DateEnd] [datetime] NULL,
[Lpp_LocalAddr] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[Lpp_IsSecure] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogPaymentPage] ADD CONSTRAINT [PK_tblLogPaymentPage] PRIMARY KEY CLUSTERED  ([LogPaymentPage_id]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log Hosted Payment Page usage ', 'SCHEMA', N'dbo', 'TABLE', N'tblLogPaymentPage', NULL, NULL
GO
