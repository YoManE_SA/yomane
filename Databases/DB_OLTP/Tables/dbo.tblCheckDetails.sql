CREATE TABLE [dbo].[tblCheckDetails]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[insertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCheckDetails_insertDate] DEFAULT (getdate()),
[CompanyId] [int] NOT NULL CONSTRAINT [DF_tblCheckDetails_companyId] DEFAULT ((0)),
[CustomerId] [int] NULL CONSTRAINT [DF_tblCheckDetails_customerId] DEFAULT ((0)),
[BillingAddressId] [int] NULL CONSTRAINT [DF_tblCheckDetails_BillingAddressId] DEFAULT ((0)),
[AccountName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_accountName] DEFAULT (''),
[AccountNumber256] [varbinary] (200) NULL,
[RoutingNumber256] [varbinary] (200) NULL,
[PersonalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_personalNumber] DEFAULT (''),
[PhoneNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_phoneNumber] DEFAULT (''),
[Email] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_email] DEFAULT (''),
[Comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_comment] DEFAULT (''),
[BirthDate] [nvarchar] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_BirthDate] DEFAULT (''),
[BankAccountTypeId] [tinyint] NOT NULL CONSTRAINT [DF_tblCheckDetails_bankAccountTypeId] DEFAULT ((0)),
[BankName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_BankName] DEFAULT (''),
[BankCity] [nvarchar] (15) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_BankCity] DEFAULT (''),
[BankPhone] [nvarchar] (15) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_BankPhone] DEFAULT (''),
[BankState] [nvarchar] (2) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_BankState] DEFAULT (''),
[BankCountry] [nvarchar] (2) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCheckDetails_BankCountry] DEFAULT ('US'),
[InvoiceName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[trgCheckDetailsDeleteMoveToArchive] ON [dbo].[tblCheckDetails] AFTER DELETE AS
INSERT INTO dbo.syn_archive_tblCheckDetails SELECT * FROM DELETED

GO
ALTER TABLE [dbo].[tblCheckDetails] ADD CONSTRAINT [PK_tblCheckDetails] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCheckDetails_accountName] ON [dbo].[tblCheckDetails] ([AccountName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCheckDetails_BillingAddressId] ON [dbo].[tblCheckDetails] ([BillingAddressId]) WHERE ([BillingAddressId] IS NOT NULL) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCheckDetails_companyId] ON [dbo].[tblCheckDetails] ([CompanyId]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCheckDetails] ADD CONSTRAINT [FK_tblCheckDetails_tblBillingAddress_BillingAddressId] FOREIGN KEY ([BillingAddressId]) REFERENCES [dbo].[tblBillingAddress] ([id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCheckDetails] ADD CONSTRAINT [FK_tblCheckDetails_tblCompany_CompanyId] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO

EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds ECheck processed info (Replace by Trans.TransPaymentMethod)', 'SCHEMA', N'dbo', 'TABLE', N'tblCheckDetails', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblCheckDetails', 'COLUMN', N'CustomerId'
GO
