CREATE TABLE [dbo].[tblBillingAddress]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[insertDate] [datetime] NOT NULL CONSTRAINT [DF_tblBillingAddress_insertDate] DEFAULT (getdate()),
[address1] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingAddress_address1] DEFAULT (''),
[address2] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingAddress_address2] DEFAULT (''),
[city] [nvarchar] (60) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingAddress_city] DEFAULT (''),
[zipCode] [nvarchar] (15) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingAddress_zipCode] DEFAULT (''),
[stateId] [int] NOT NULL CONSTRAINT [DF_tblBillingAddress_stateId] DEFAULT ((0)),
[countryId] [int] NOT NULL CONSTRAINT [DF_tblBillingAddress_countryId] DEFAULT ((0)),
[stateIso] [nvarchar] (2) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingAddress_stateIso] DEFAULT (''),
[countryIso] [nvarchar] (2) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingAddress_countryIso] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillingAddress] ADD CONSTRAINT [PK_tblBillingAddress] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds billing address for old PM tables (tblCreditCard...)', 'SCHEMA', N'dbo', 'TABLE', N'tblBillingAddress', NULL, NULL
GO
