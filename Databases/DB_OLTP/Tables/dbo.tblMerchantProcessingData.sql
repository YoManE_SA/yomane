CREATE TABLE [dbo].[tblMerchantProcessingData]
(
[MerchantID] [int] NOT NULL,
[MPD_CffCurAmount] [money] NOT NULL CONSTRAINT [DF_tblMerchantProcessingData_MPD_CffCurAmount] DEFAULT ((0)),
[MPD_CffResetDate] [date] NOT NULL CONSTRAINT [DF_tblMerchantProcessingData_MPD_CffResetDate] DEFAULT (getdate()),
[MPD_CffCurAmount2] [money] NOT NULL CONSTRAINT [DF_tblMerchantProcessingData_MPD_CffCurAmount2] DEFAULT ((0)),
[MPD_CffResetDate2] [date] NOT NULL CONSTRAINT [DF_tblMerchantProcessingData_MPD_CffResetDate2] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMerchantProcessingData] ADD CONSTRAINT [PK_tblMerchantProcessingData] PRIMARY KEY CLUSTERED  ([MerchantID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMerchantProcessingData] ADD CONSTRAINT [FK_tblMerchantProcessingData_tblCompany_MerchantID] FOREIGN KEY ([MerchantID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used for saving processing totals and fee rates for old floored fees', 'SCHEMA', N'dbo', 'TABLE', N'tblMerchantProcessingData', NULL, NULL
GO
