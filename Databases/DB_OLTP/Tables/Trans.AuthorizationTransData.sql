CREATE TABLE [Trans].[AuthorizationTransData]
(
[AuthorizationTransData_id] [int] NOT NULL IDENTITY(1, 1),
[TransPass_id] [int] NULL,
[TransPending_id] [int] NULL,
[TransPreAuth_id] [int] NULL,
[VariableChar] [nvarchar] (4000) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Trans].[AuthorizationTransData] ADD CONSTRAINT [PK_AuthorizationTransData] PRIMARY KEY NONCLUSTERED  ([AuthorizationTransData_id]) ON [PRIMARY]
GO
ALTER TABLE [Trans].[AuthorizationTransData] ADD CONSTRAINT [FK_AuthorizationTransData_tblCompanyTransPass_TransPassID] FOREIGN KEY ([TransPass_id]) REFERENCES [dbo].[tblCompanyTransPass] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [Trans].[AuthorizationTransData] ADD CONSTRAINT [FK_AuthorizationTransData_tblCompanyTransPending_TransPendingID] FOREIGN KEY ([TransPending_id]) REFERENCES [dbo].[tblCompanyTransPending] ([companyTransPending_id]) ON DELETE CASCADE
GO
ALTER TABLE [Trans].[AuthorizationTransData] ADD CONSTRAINT [FK_AuthorizationTransData_tblCompanyTransApproval_TransApprovalID] FOREIGN KEY ([TransPreAuth_id]) REFERENCES [dbo].[tblCompanyTransApproval] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transaction that need to be send by batch for autorization, records are deleted once sent', 'SCHEMA', N'Trans', 'TABLE', N'AuthorizationTransData', NULL, NULL
GO
