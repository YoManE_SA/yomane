CREATE TABLE [dbo].[tblPeriodicFee]
(
[MerchantID] [int] NOT NULL,
[TypeID] [int] NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_tblPeriodicFee_IsActive] DEFAULT ((0)),
[NextDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPeriodicFee] ADD CONSTRAINT [PK_tblPeriodicFee] PRIMARY KEY CLUSTERED  ([MerchantID], [TypeID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPeriodicFee] ADD CONSTRAINT [FK_tblPeriodicFee_tblCompany_MerchantID] FOREIGN KEY ([MerchantID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPeriodicFee] ADD CONSTRAINT [FK_tblPeriodicFee_tblPeriodicFeeType_TypeID] FOREIGN KEY ([TypeID]) REFERENCES [dbo].[tblPeriodicFeeType] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] merchant''s periodic fee (Replaced in new Admin by [Setting].[SetMerchantPeriodicFee])', 'SCHEMA', N'dbo', 'TABLE', N'tblPeriodicFee', NULL, NULL
GO
