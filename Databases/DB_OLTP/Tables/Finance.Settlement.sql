CREATE TABLE [Finance].[Settlement]
(
[Settlement_id] [int] NOT NULL IDENTITY(1, 1),
[SetSettlement_id] [int] NULL,
[TransactionPay_id] [int] NULL,
[InsertDate] [datetime2] (2) NOT NULL CONSTRAINT [DF_Settlement_InsertDate] DEFAULT (sysdatetime()),
[PayeeName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[CommentText] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL,
[IsVisible] [bit] NOT NULL CONSTRAINT [DF_Settlement_IsVisible] DEFAULT ((0)),
[SettlementDate] [datetime2] (2) NOT NULL,
[SettlementTotal] [decimal] (19, 2) NULL,
[SettlementCurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[SettlementType_id] [tinyint] NOT NULL,
[Account_id] [int] NULL,
[InvoiceIssuer_id] [tinyint] NULL,
[InvoiceProvider_id] [tinyint] NULL,
[InvoiceURL] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[InvoiceNumber] [int] NULL,
[InvoiceTotal] [decimal] (19, 2) NULL,
[InvoiceCurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[InvoiceVATPercent] [decimal] (7, 4) NULL,
[InvoiceCurrencyExchangeRate] [decimal] (7, 4) NULL,
[WireTotal] [decimal] (19, 2) NULL,
[WireCurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[WireCurrencyExchangeRate] [decimal] (7, 4) NULL,
[WireFeeFixedAmount] [decimal] (19, 4) NULL,
[WireFeePercentValue] [decimal] (7, 4) NULL
) ON [PRIMARY]
GO
ALTER TABLE [Finance].[Settlement] ADD CONSTRAINT [PK_Settlement] PRIMARY KEY CLUSTERED  ([Settlement_id]) ON [PRIMARY]
GO
ALTER TABLE [Finance].[Settlement] ADD CONSTRAINT [FK_Settlement_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Finance].[Settlement] ADD CONSTRAINT [FK_Settlement_InvoiceCurrencyISOCode] FOREIGN KEY ([InvoiceCurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Finance].[Settlement] ADD CONSTRAINT [FK_Settlement_SetSettlementID] FOREIGN KEY ([SetSettlement_id]) REFERENCES [Setting].[SetSettlement] ([SetSettlement_id]) ON DELETE SET NULL
GO
ALTER TABLE [Finance].[Settlement] ADD CONSTRAINT [FK_Settlement_SettlementCurrencyISOCode] FOREIGN KEY ([SettlementCurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Finance].[Settlement] ADD CONSTRAINT [FK_Settlement_WireCurrencyISOCode] FOREIGN KEY ([WireCurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
