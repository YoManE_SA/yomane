CREATE TABLE [dbo].[tblLogImportEPA]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TransID] [int] NOT NULL,
[Installment] [tinyint] NOT NULL CONSTRAINT [DF_tblLogImportEPA_Installment] DEFAULT ((1)),
[IsPaid] [bit] NOT NULL CONSTRAINT [DF_tblLogImportEPA_IsPaid] DEFAULT ((0)),
[IsRefunded] [bit] NOT NULL CONSTRAINT [DF_tblLogImportEPA_IsRefunded] DEFAULT ((0)),
[PaidInsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblLogImportEPA_PaidInsertDate] DEFAULT (getdate()),
[RefundedInsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblLogImportEPA_RefundedInsertDate] DEFAULT (getdate()),
[IsManual] [bit] NULL,
[TransInstallment] AS ((str([TransID])+'_')+str([Installment])) PERSISTED,
[AdminUser] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[AcquirerReferenceNum] [varchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogImportEPA] ADD CONSTRAINT [PK_tblLogImportEPA] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblLogImportEPA_TransID_Installment] ON [dbo].[tblLogImportEPA] ([TransID], [Installment]) INCLUDE ([IsPaid], [IsRefunded]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogImportEPA] ADD CONSTRAINT [FK_tblLogImportEPA_tblCompanyTransPass_TransID] FOREIGN KEY ([TransID]) REFERENCES [dbo].[tblCompanyTransPass] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transaction''s EPA details', 'SCHEMA', N'dbo', 'TABLE', N'tblLogImportEPA', NULL, NULL
GO
