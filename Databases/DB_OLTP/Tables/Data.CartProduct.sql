CREATE TABLE [Data].[CartProduct]
(
[CartProduct_id] [int] NOT NULL IDENTITY(1, 1),
[Cart_id] [int] NOT NULL,
[Product_id] [int] NULL,
[ProductType_id] [tinyint] NOT NULL,
[ProductStock_id] [int] NULL,
[Merchant_id] [int] NOT NULL,
[InsertDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_CartProduct_InsertDate] DEFAULT (sysdatetime()),
[Name] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL,
[Quantity] [smallint] NOT NULL,
[Price] [decimal] (10, 2) NOT NULL,
[ShippingFee] [decimal] (8, 2) NOT NULL,
[VATPercent] [decimal] (5, 2) NOT NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[CurrencyFXRate] [decimal] (8, 5) NOT NULL,
[TotalProduct] AS (([Price]*[Quantity])*((1)+[VATPercent])) PERSISTED,
[TotalShipping] AS (([ShippingFee]*[Quantity])*((1)+[VATPercent])) PERSISTED,
[Total] AS ((([Price]+[ShippingFee])*[Quantity])*((1)+[VATPercent])) PERSISTED,
[UISetting] [varchar] (200) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[CartProduct] ADD CONSTRAINT [PK_CartProduct] PRIMARY KEY CLUSTERED  ([CartProduct_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[CartProduct] ADD CONSTRAINT [FK_CartProduct_Cart_CartID] FOREIGN KEY ([Cart_id]) REFERENCES [Data].[Cart] ([Cart_id]) ON DELETE CASCADE
GO
ALTER TABLE [Data].[CartProduct] ADD CONSTRAINT [FK_CartProduct_Currency] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Data].[CartProduct] ADD CONSTRAINT [FK_CartProduct_tblCompany_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Data].[CartProduct] ADD CONSTRAINT [FK_CartProduct_Product_ProductID] FOREIGN KEY ([Product_id]) REFERENCES [Data].[Product] ([Product_id])
GO
ALTER TABLE [Data].[CartProduct] ADD CONSTRAINT [FK_CartProduct_ProductStock_ProductStockID] FOREIGN KEY ([ProductStock_id]) REFERENCES [Data].[ProductStock] ([ProductStock_id])
GO
ALTER TABLE [Data].[CartProduct] ADD CONSTRAINT [FK_CartProduct_ProductTypeID] FOREIGN KEY ([ProductType_id]) REFERENCES [List].[ProductType] ([ProductType_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Shopping cart products', 'SCHEMA', N'Data', 'TABLE', N'CartProduct', NULL, NULL
GO
