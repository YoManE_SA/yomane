CREATE TABLE [List].[SettlemenType]
(
[SettlementType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[SettlemenType] ADD CONSTRAINT [PK_SettlementType] PRIMARY KEY CLUSTERED  ([SettlementType_id]) ON [PRIMARY]
GO
