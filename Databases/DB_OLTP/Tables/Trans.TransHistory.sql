CREATE TABLE [Trans].[TransHistory]
(
[TransHistory_id] [int] NOT NULL IDENTITY(1, 1),
[TransHistoryType_id] [tinyint] NOT NULL,
[Merchant_id] [int] NOT NULL,
[TransPass_id] [int] NULL,
[TransPreAuth_id] [int] NULL,
[TransPending_id] [int] NULL,
[TransFail_id] [int] NULL,
[InsertDate] [datetime2] (2) NOT NULL CONSTRAINT [DF_TransHistory_InsertDate] DEFAULT (getdate()),
[IsSucceeded] [bit] NULL,
[Description] [varchar] (250) COLLATE Hebrew_CI_AS NULL,
[ReferenceNumber] [int] NULL,
[ReferenceUrl] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransHistory] ADD CONSTRAINT [PK_TransHistory] PRIMARY KEY CLUSTERED  ([TransHistory_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransHistory_InsertDate] ON [Trans].[TransHistory] ([InsertDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransHistory_TransFail_id] ON [Trans].[TransHistory] ([TransFail_id]) WHERE ([TransFail_id] IS NOT NULL) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransHistory_HistoryType_id] ON [Trans].[TransHistory] ([TransHistoryType_id]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransHistory_TransPass_id] ON [Trans].[TransHistory] ([TransPass_id]) WHERE ([TransPass_id] IS NOT NULL) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransHistory_Pending_id] ON [Trans].[TransHistory] ([TransPending_id]) WHERE ([TransPending_id] IS NOT NULL) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransHistory_TransPending_id] ON [Trans].[TransHistory] ([TransPending_id]) WHERE ([TransPending_id] IS NOT NULL) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransHistory] ADD CONSTRAINT [FK_TransHistory_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Trans].[TransHistory] ADD CONSTRAINT [FK_TransHistory_tblCompanyTransFail_TransFail_id] FOREIGN KEY ([TransFail_id]) REFERENCES [dbo].[tblCompanyTransFail] ([ID])
GO
ALTER TABLE [Trans].[TransHistory] ADD CONSTRAINT [FK_TransHistory_TransHistoryType_TransHistoryType_id] FOREIGN KEY ([TransHistoryType_id]) REFERENCES [List].[TransHistoryType] ([TransHistoryType_id])
GO
ALTER TABLE [Trans].[TransHistory] ADD CONSTRAINT [FK_TransHistory_tblCompanyTransPass_TransPass_id] FOREIGN KEY ([TransPass_id]) REFERENCES [dbo].[tblCompanyTransPass] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [Trans].[TransHistory] ADD CONSTRAINT [FK_TransHistory_tblCompanyTransPending_TransPending_id] FOREIGN KEY ([TransPending_id]) REFERENCES [dbo].[tblCompanyTransPending] ([companyTransPending_id]) ON DELETE CASCADE
GO
ALTER TABLE [Trans].[TransHistory] ADD CONSTRAINT [FK_TransHistory_tblCompanyTransApproval_TransPreAuth_id] FOREIGN KEY ([TransPreAuth_id]) REFERENCES [dbo].[tblCompanyTransApproval] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transaction history', 'SCHEMA', N'Trans', 'TABLE', N'TransHistory', NULL, NULL
GO
