CREATE TABLE [dbo].[tblTransactionAmount]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TransPassID] [int] NULL,
[TransApprovalID] [int] NULL,
[TransPendingID] [int] NULL,
[TransFailID] [int] NULL,
[TypeID] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblTransactionAmount_InsertDate] DEFAULT (getdate()),
[Amount] [money] NOT NULL,
[Currency] [int] NOT NULL,
[SettlementDate] [datetime] NULL,
[SettlementID] [int] NULL,
[SettledAmount] [money] NULL,
[SettledCurrency] [int] NULL,
[Installment] [int] NULL,
[SettlementAmountID] [int] NULL,
[MerchantID] [int] NULL,
[DebitCompany_id] [int] NULL,
[Affiliates_id] [int] NULL,
[PercentValue] [decimal] (6, 4) NULL
) ON [SEC_TransAmount]
WITH
(
DATA_COMPRESSION = PAGE
)
GO
ALTER TABLE [dbo].[tblTransactionAmount] ADD CONSTRAINT [PK_tblTransactionAmount] PRIMARY KEY CLUSTERED  ([ID]) WITH (DATA_COMPRESSION = PAGE) ON [SEC_TransAmount]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionAmount_InsertDate] ON [dbo].[tblTransactionAmount] ([InsertDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionAmount_TransApprovalID] ON [dbo].[tblTransactionAmount] ([TransApprovalID] DESC) WHERE ([TransApprovalID] IS NOT NULL) ON [SEC_TransAmount]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionAmount_TransFailID] ON [dbo].[tblTransactionAmount] ([TransFailID] DESC) WHERE ([TransFailID] IS NOT NULL) ON [SEC_TransAmount]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionAmount_TransPassID] ON [dbo].[tblTransactionAmount] ([TransPassID] DESC) WHERE ([TransPassID] IS NOT NULL) ON [SEC_TransAmount]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionAmount_TransPendingID] ON [dbo].[tblTransactionAmount] ([TransPendingID] DESC) WHERE ([TransPendingID] IS NOT NULL) ON [SEC_TransAmount]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionAmount_TypeID] ON [dbo].[tblTransactionAmount] ([TypeID] DESC) ON [SEC_TransAmount]
GO
ALTER TABLE [dbo].[tblTransactionAmount] ADD CONSTRAINT [FK_tblTransactionAmount_tblSettlementAmount_SettlementAmountID] FOREIGN KEY ([SettlementAmountID]) REFERENCES [dbo].[tblSettlementAmount] ([ID]) ON DELETE CASCADE
GO
