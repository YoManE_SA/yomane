CREATE TABLE [dbo].[tblCompanyTransRemoved]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[companyID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_companyID] DEFAULT ((0)),
[TransactionTypeID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_TransactionTypeID] DEFAULT ((7)),
[DebitCompanyID] [smallint] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_DebitCompanyID] DEFAULT ((1)),
[CustomerID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_CustomerID] DEFAULT ((0)),
[FraudDetectionLog_id] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_FraudDetectionLog_id] DEFAULT ((0)),
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_InsertDate] DEFAULT (getdate()),
[PayDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_PayDate] DEFAULT (getdate()),
[IsPay] [bit] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_IsPay] DEFAULT ((0)),
[IPAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_IPAddress] DEFAULT (''),
[Amount] [money] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_Amount] DEFAULT ((0)),
[Currency] [smallint] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_Currency] DEFAULT ((1)),
[Payments] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_Payments] DEFAULT ((1)),
[CreditType] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_CreditType] DEFAULT ((0)),
[replyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_replyCode] DEFAULT (''),
[OrderNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_OrderNumber] DEFAULT (''),
[Interest] [float] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_Interest] DEFAULT ((0)),
[TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_TerminalNumber] DEFAULT (''),
[ApprovalNumber] [nvarchar] (7) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_ApprovalNumber] DEFAULT (''),
[OriginalTransID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_OriginalTransID] DEFAULT ((0)),
[IPAddressDel] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_IPAddressDel] DEFAULT (''),
[PaymentMethod_id] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_PaymentMethod_id] DEFAULT ((1)),
[PaymentMethodID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_PaymentMethodID] DEFAULT ((0)),
[PaymentMethodDisplay] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_PaymentMethodDisplay] DEFAULT (''),
[DeniedDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_DeniedDate] DEFAULT (getdate()),
[DeniedStatus] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_DeniedStatus] DEFAULT ((0)),
[DateDel] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_DateDel] DEFAULT (getdate()),
[isTestOnly] [bit] NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_isTestOnly] DEFAULT ((0)),
[referringUrl] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransRemoved_referringUrl] DEFAULT (''),
[TransSource_id] [tinyint] NULL,
[Comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblCompanyTransRemoved_Comment] DEFAULT (''),
[SystemText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[PayforText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[MerchantProduct_id] [int] NULL,
[PayerInfo_id] [int] NULL,
[TransPayerInfo_id] [int] NULL,
[TransPaymentMethod_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyTransRemoved] ADD CONSTRAINT [PK_tblCompanyTransRemoved] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransRemoved_terminalNumber] ON [dbo].[tblCompanyTransRemoved] ([TerminalNumber]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyTransRemoved] ADD CONSTRAINT [FK_tblCompanyTransRemoved_TransPayerInfo] FOREIGN KEY ([TransPayerInfo_id]) REFERENCES [Trans].[TransPayerInfo] ([TransPayerInfo_id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransRemoved] ADD CONSTRAINT [FK_tblCompanyTransRemoved_TransPaymentMethod] FOREIGN KEY ([TransPaymentMethod_id]) REFERENCES [Trans].[TransPaymentMethod] ([TransPaymentMethod_id]) ON DELETE SET NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transactions deleted', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyTransRemoved', NULL, NULL
GO
