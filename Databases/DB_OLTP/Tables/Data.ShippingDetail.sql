CREATE TABLE [Data].[ShippingDetail]
(
[ShippingDetail_id] [int] NOT NULL IDENTITY(1, 1),
[TableList_id] [tinyint] NOT NULL,
[AddressLine1] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[AddressLine2] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[AddressCity] [nvarchar] (60) COLLATE Hebrew_CI_AS NULL,
[AddressPostalCode] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[AddressStateISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[AddressCountryISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ShippingDetail] ADD CONSTRAINT [PK_tblBillingAddress] PRIMARY KEY CLUSTERED  ([ShippingDetail_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ShippingDetail] ADD CONSTRAINT [FK_ShippingDetail_AddressCountryISOCode] FOREIGN KEY ([AddressCountryISOCode]) REFERENCES [List].[CountryList] ([CountryISOCode])
GO
ALTER TABLE [Data].[ShippingDetail] ADD CONSTRAINT [FK_ShippingDetail_AddressStateISOCode] FOREIGN KEY ([AddressStateISOCode]) REFERENCES [List].[StateList] ([StateISOCode])
GO
ALTER TABLE [Data].[ShippingDetail] ADD CONSTRAINT [FK_ShippingDetail_TableList] FOREIGN KEY ([TableList_id]) REFERENCES [List].[TableList] ([TableList_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'Data', 'TABLE', N'ShippingDetail', NULL, NULL
GO
