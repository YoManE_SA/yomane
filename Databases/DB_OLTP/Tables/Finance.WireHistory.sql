CREATE TABLE [Finance].[WireHistory]
(
[WireHistory_id] [int] NOT NULL IDENTITY(1, 1),
[Wire_id] [int] NOT NULL,
[InsertDate] [datetime2] (2) NOT NULL,
[UserName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[FileName] [varchar] (80) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Finance].[WireHistory] ADD CONSTRAINT [PK_WireHistory] PRIMARY KEY CLUSTERED  ([WireHistory_id]) WITH (FILLFACTOR=95) ON [PRIMARY]
GO
ALTER TABLE [Finance].[WireHistory] ADD CONSTRAINT [FK_Wire_WireID] FOREIGN KEY ([Wire_id]) REFERENCES [Finance].[Wire] ([Wire_id])
GO
