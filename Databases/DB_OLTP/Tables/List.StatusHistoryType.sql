CREATE TABLE [List].[StatusHistoryType]
(
[StatusHistoryType_id] [varchar] (16) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[StatusHistoryType] ADD CONSTRAINT [PK_StatusHistoryType] PRIMARY KEY CLUSTERED  ([StatusHistoryType_id]) ON [PRIMARY]
GO
