CREATE TABLE [dbo].[tblCompanyTransPass]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[companyID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_companyID] DEFAULT ((0)),
[TransactionTypeID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_TransactionTypeID] DEFAULT ((7)),
[DebitCompanyID] [int] NULL,
[CustomerID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_CustomerID] DEFAULT ((0)),
[FraudDetectionLog_id] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_FraudDetectionLog_id] DEFAULT ((0)),
[OriginalTransID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_OriginalTransID] DEFAULT ((0)),
[PrimaryPayedID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_PrimaryPayedID] DEFAULT ((0)),
[PayID] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_PayID] DEFAULT (';0;'),
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_InsertDate] DEFAULT (getdate()),
[Amount] [money] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_Amount] DEFAULT ((0)),
[Currency] [int] NULL,
[Payments] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_Payments] DEFAULT ((1)),
[CreditType] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_CreditType] DEFAULT ((0)),
[IPAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_IPAddress] DEFAULT (''),
[replyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_replyCode] DEFAULT (''),
[OrderNumber] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_OrderNumber] DEFAULT (''),
[Interest] [float] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_Interest] DEFAULT ((0)),
[TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_TerminalNumber] DEFAULT (''),
[ApprovalNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_ApprovalNumber] DEFAULT (''),
[DeniedDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_DeniedDate] DEFAULT ('01/01/1900 00:00:01'),
[DeniedStatus] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_DeniedStatus] DEFAULT ((0)),
[DeniedPrintDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_DeniedPrintDate] DEFAULT ('01/01/1900 00:00:01'),
[DeniedSendDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_DeniedSendDate] DEFAULT ('01/01/1900 00:00:01'),
[DeniedAdminComment] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_DeniedAdminComment] DEFAULT (''),
[PD] [smalldatetime] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_PD] DEFAULT (''),
[MerchantPD] [smalldatetime] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_MerchantPD] DEFAULT (''),
[PaymentMethod_id] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_PaymentMethod_id] DEFAULT ((1)),
[PaymentMethodID] [int] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_PaymentMethodID] DEFAULT ((0)),
[PaymentMethodDisplay] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_PaymentMethodDisplay] DEFAULT (''),
[isTestOnly] [bit] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_isTestOnly] DEFAULT ((0)),
[referringUrl] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_referringUrl] DEFAULT (''),
[payerIdUsed] [nvarchar] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_payerIdUsed] DEFAULT (''),
[netpayFee_transactionCharge] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_netpayFee_transactionCharge] DEFAULT ((0)),
[netpayFee_ratioCharge] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_netpayFee_ratioCharge] DEFAULT ((0)),
[DebitReferenceCode] [nvarchar] (40) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_DebitReferenceCode] DEFAULT (''),
[OCurrency] [tinyint] NULL CONSTRAINT [DF_tblCompanyTransPass_OCurrency] DEFAULT ((0)),
[OAmount] [money] NULL CONSTRAINT [DF_tblCompanyTransPass_OAmount] DEFAULT ((0)),
[netpayFee_chbCharge] [money] NULL CONSTRAINT [DF_tblCompanyTransPass_netpayFee_chbCharge] DEFAULT ((0)),
[netpayFee_ClrfCharge] [money] NULL CONSTRAINT [DF_tblCompanyTransPass_netpayFee_ClrfCharge] DEFAULT ((0)),
[PaymentMethod] [smallint] NULL,
[UnsettledAmount] [money] NULL,
[UnsettledInstallments] [int] NULL,
[IPCountry] [varchar] (2) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyTransPass_IPCountry] DEFAULT ('--'),
[HandlingFee] [money] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_HandlingFee] DEFAULT ((0)),
[CTP_Status] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_CTP_Status] DEFAULT ((0)),
[DebitFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_DebitFee] DEFAULT ((0)),
[BTFileName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[RecurringSeries] [int] NULL,
[RecurringChargeNumber] [int] NULL,
[DeniedValDate] [datetime] NULL,
[MerchantRealPD] [datetime] NULL,
[RefTrans] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[IsChargeback] AS (CONVERT([bit],case when [DeniedStatus]=(8) OR [DeniedStatus]=(6) OR [DeniedStatus]=(4) OR [DeniedStatus]=(2) OR [DeniedStatus]=(1) then (1) else (0) end,0)) PERSISTED,
[PaymentMethodTemp] [smallint] NULL,
[PhoneDetailsID] [int] NULL,
[DebitFeeChb] [smallmoney] NULL,
[IsRetrievalRequest] AS (CONVERT([bit],case when [DeniedStatus]=(3) OR [DeniedStatus]=(5) then (1) else (0) end,(0))),
[IsPendingChargeback] [bit] NULL,
[DebitReferenceNum] [varchar] (40) COLLATE Hebrew_CI_AS NULL,
[AcquirerReferenceNum] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[TransSource_id] [tinyint] NULL,
[MobileDevice_id] [int] NULL,
[AuthorizationBatchID] [int] NULL,
[Comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblCompanyTransPass_Comment] DEFAULT (''),
[SystemText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[PayforText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[MerchantProduct_id] [int] NULL,
[CreditCardID] [int] NULL,
[CheckDetailsID] [int] NULL,
[PayerInfo_id] [int] NULL,
[TransPayerInfo_id] [int] NULL,
[TransPaymentMethod_id] [int] NULL,
[Is3DSecure] [bit] NULL CONSTRAINT [DF_CompanyTransPass_Is3DSecure] DEFAULT ((0)),
[IsFraudByAcquirer] [bit] NULL,
[IsCashback] [bit] NOT NULL CONSTRAINT [DF_tblCompanyTransPass_IsCashback] DEFAULT ((0)),
[IsCardPresent] [bit] NULL
) ON [PRIMARY]
WITH
(
DATA_COMPRESSION = PAGE
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[trgCompanyTransPassSetTerminalCHBCount] ON [dbo].[tblCompanyTransPass] AFTER UPDATE, INSERT AS
BEGIN
	IF Update(DeniedStatus)
	BEGIN
		DECLARE @nDebitCompany int, @sTerminal nvarchar(20), @nPaymentMethod int, @nDelta int;
		DECLARE curData CURSOR FOR
			SELECT
				Inserted.DebitCompanyID,
				Inserted.TerminalNumber,
				Inserted.PaymentMethod,
				Sum(IsNull(Cast(Inserted.IsChargeback AS int), 0)-IsNull(Cast(Deleted.IsChargeback AS int), 0))
			FROM
				Inserted
				LEFT JOIN Deleted ON Inserted.ID=Deleted.ID
			WHERE
				Inserted.PaymentMethod IN (22, 25)
			GROUP BY
				Inserted.DebitCompanyID,
				Inserted.TerminalNumber,
				Inserted.PaymentMethod;
		OPEN curData;
		FETCH NEXT FROM curData INTO @nDebitCompany, @sTerminal, @nPaymentMethod, @nDelta;
		WHILE @@FETCH_STATUS=0
		BEGIN
			IF @nDelta<>0 BEGIN
				IF @nPaymentMethod=25
					UPDATE tblDebitTerminals SET dt_MonthlyMCCHB=dt_MonthlyMCCHB+@nDelta WHERE DebitCompany=@nDebitCompany AND TerminalNumber=@sTerminal
				ELSE
					UPDATE tblDebitTerminals SET dt_MonthlyCHB=dt_MonthlyCHB+@nDelta WHERE DebitCompany=@nDebitCompany AND TerminalNumber=@sTerminal;
			END
			FETCH NEXT FROM curData INTO @nDebitCompany, @sTerminal, @nPaymentMethod, @nDelta;
		END
		CLOSE curData;
		DEALLOCATE curData;
	END
END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[trTransPass_RecurringBlockOnCHB_InsUpd] ON [dbo].[tblCompanyTransPass] AFTER INSERT, UPDATE AS
BEGIN

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
	----Column 'IsChargeback' cannot be used in an IF UPDATE clause because it is a computed column.
	--IF UPDATE(IsChargeback)
	--BEGIN
		
        UPDATE  tblRecurringSeries
        SET     rs_Blocked = 1
        FROM    INSERTED i
                INNER JOIN tblRecurringAttempt a ON i.ID = a.ra_TransPass
                INNER JOIN tblRecurringCharge c ON a.ra_Charge = c.ID
                INNER JOIN tblRecurringSeries s ON c.rc_Series = s.ID
        WHERE   i.IsChargeback = 1
			
	--END
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------
-- Zeev Lavi 12/11/2012
-- Set Column PhoneDetailsID with PaymentMethodID value in case this is a Phone transaction
---------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[trTransPass_SetOldTransSource_InsUpd] ON [dbo].[tblCompanyTransPass]
    AFTER INSERT, UPDATE
AS
    IF @@ROWCOUNT = 0 
        RETURN;
    SET NOCOUNT ON;
	
    IF UPDATE(TransSource_id) 
        BEGIN
	
            UPDATE  tblCompanyTransPass
            SET     TransactionTypeID = TransSource_id
            WHERE   id IN ( SELECT  ID
                            FROM    INSERTED
                            WHERE   ISNULL(TransSource_id, 0) > 0 );
		
        END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------
-- Zeev Lavi 12/11/2012
-- Set Column PhoneDetailsID with PaymentMethodID value in case this is a Phone transaction
---------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[trTransPass_SetPMDetailID_InsUpd] ON [dbo].[tblCompanyTransPass]
AFTER INSERT, UPDATE
AS

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
	IF UPDATE(PaymentMethodID)
	BEGIN
	
		--Update PhoneDetailsID Column
		UPDATE tblCompanyTransPass SET PhoneDetailsID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID <> 0 AND PaymentMethod >= 200 AND PaymentMethod <= 229);
		
		--Update CheckDetailsID Column
		UPDATE tblCompanyTransPass SET CheckDetailsID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID > 0 AND PaymentMethod >= 100 AND PaymentMethod <= 199); 
		
		--Update CreditCardID Column
		UPDATE tblCompanyTransPass SET CreditCardID = PaymentMethodID WHERE id IN (
			SELECT ID FROM INSERTED WHERE PaymentMethodID > 0 AND PaymentMethod >= 20 AND PaymentMethod <= 49); 
		
	END


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trTransPass_SetUnsettled_Upd] ON [dbo].[tblCompanyTransPass]
    FOR UPDATE
AS
    BEGIN
    	
		IF @@ROWCOUNT = 0 RETURN;
		SET NOCOUNT ON;
		
        IF UPDATE(PayID) OR UPDATE(CreditType) OR UPDATE(DeniedStatus) OR UPDATE(Amount) OR UPDATE(Payments) 
        BEGIN
            UPDATE  tblCompanyTransPass
            SET     UnsettledAmount = CASE Inserted.PayID
                                        WHEN ';X;' THEN 0
                                        WHEN ';0;'
                                        THEN CASE Inserted.CreditType
                                               WHEN 0
                                               THEN CASE Inserted.DeniedStatus
                                                      WHEN 0 THEN -Inserted.Amount
                                                      WHEN 2 THEN Inserted.Amount
                                                      WHEN 6 THEN -Inserted.Amount
                                                      WHEN 7 THEN -Inserted.Amount
                                                      WHEN 10 THEN -Inserted.Amount
                                                      ELSE 0
                                                    END
                                               ELSE CASE Inserted.DeniedStatus
                                                      WHEN 0 THEN Inserted.Amount
                                                      WHEN 2 THEN -Inserted.Amount
                                                      WHEN 7 THEN Inserted.Amount
                                                      WHEN 10 THEN Inserted.Amount
                                                      ELSE 0
                                                    END
                                             END
                                        ELSE CASE Inserted.DeniedStatus
                                               WHEN 2 THEN -Inserted.Amount
                                               ELSE 0
                                             END
                                      END ,
                    UnsettledInstallments = CASE Inserted.DeniedStatus
                                              WHEN 1 THEN 1
                                              WHEN 2 THEN 1
                                              ELSE CASE Inserted.PayID
													 WHEN ';X;' THEN 0
                                                     WHEN ';0;'
                                                     THEN CASE Inserted.CreditType
                                                          WHEN 8 THEN Inserted.Payments
                                                          ELSE 1
                                                          END
                                                     ELSE 0
                                                   END
                                            END
            FROM    Inserted
                    INNER JOIN tblCompanyTransPass ON Inserted.ID = tblCompanyTransPass.ID
            WHERE   Inserted.CreditType <> 8
                        OR Inserted.Payments = 1;
		END
    END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/****** Object:  Trigger [trgInsertTransPassUpdTracking] Script Date: 06/01/2008 16:26:55 ******/
CREATE TRIGGER [dbo].[trTransPass_TrackMerchantActivity_Ins] ON [dbo].[tblCompanyTransPass] AFTER INSERT
AS

	IF @@ROWCOUNT = 0 RETURN;
	SET NOCOUNT ON;
	
    UPDATE  Track.MerchantActivity
    SET     DateLastTransPass = SYSDATETIME()
    WHERE	Merchant_id IN ( SELECT companyID FROM INSERTED );


GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [PK_tblCompanyTransPass] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90, DATA_COMPRESSION = PAGE) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_CompanyID_UnsettledInstallments_Currency_InsertDate_ID] ON [dbo].[tblCompanyTransPass] ([companyID], [UnsettledInstallments], [Currency], [InsertDate], [ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_CompanyID_UnsettledInstallments_isTestOnly] ON [dbo].[tblCompanyTransPass] ([companyID], [UnsettledInstallments], [isTestOnly]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_CreditType] ON [dbo].[tblCompanyTransPass] ([CreditType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_Currency] ON [dbo].[tblCompanyTransPass] ([Currency]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_Currency_CompanyID_MerchantPD_IsTestOnly_UnsettledInstallments] ON [dbo].[tblCompanyTransPass] ([Currency], [companyID] DESC, [MerchantPD] DESC, [isTestOnly], [UnsettledInstallments]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_CustomerID] ON [dbo].[tblCompanyTransPass] ([CustomerID]) WHERE ([CustomerID]>(0)) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_DebitCompanyID] ON [dbo].[tblCompanyTransPass] ([DebitCompanyID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_DebitReferenceCode(InsertDate)] ON [dbo].[tblCompanyTransPass] ([DebitReferenceCode]) INCLUDE ([InsertDate]) WHERE ([DebitReferenceCode] IS NOT NULL) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_DebitReferenceNum] ON [dbo].[tblCompanyTransPass] ([DebitReferenceNum]) WHERE ([DebitReferenceNum] IS NOT NULL) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_DeniedDate] ON [dbo].[tblCompanyTransPass] ([DeniedDate] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_InsertDate] ON [dbo].[tblCompanyTransPass] ([InsertDate] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_OriginalTransID_DeniedStatus_InsertDate_CompanyID_isTestOnly] ON [dbo].[tblCompanyTransPass] ([OriginalTransID], [DeniedStatus], [InsertDate], [companyID], [isTestOnly]) INCLUDE ([Amount], [Currency], [netpayFee_ratioCharge], [netpayFee_transactionCharge]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_PayID] ON [dbo].[tblCompanyTransPass] ([PayID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_PaymentMethod] ON [dbo].[tblCompanyTransPass] ([PaymentMethod]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_PhoneDetailsID] ON [dbo].[tblCompanyTransPass] ([PhoneDetailsID]) WHERE ([PhoneDetailsID] IS NOT NULL) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_PrimaryPayedID] ON [dbo].[tblCompanyTransPass] ([PrimaryPayedID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_terminalNumber] ON [dbo].[tblCompanyTransPass] ([TerminalNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_TransPayerInfoID] ON [dbo].[tblCompanyTransPass] ([TransPayerInfo_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransPass_UnsettledAmount] ON [dbo].[tblCompanyTransPass] ([UnsettledAmount]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_AuthorizationBatch_AuthorizationBatchID] FOREIGN KEY ([AuthorizationBatchID]) REFERENCES [Trans].[AuthorizationBatch] ([AuthorizationBatch_id])
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_tblCheckDetails_CheckDetailsID] FOREIGN KEY ([CheckDetailsID]) REFERENCES [dbo].[tblCheckDetails] ([id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_tblCompany_companyID] FOREIGN KEY ([companyID]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_tblCreditCard_CreditCardID] FOREIGN KEY ([CreditCardID]) REFERENCES [dbo].[tblCreditCard] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_tblSystemCurrencies_Currency] FOREIGN KEY ([Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_tblDebitCompany_DebitCompanyID] FOREIGN KEY ([DebitCompanyID]) REFERENCES [dbo].[tblDebitCompany] ([DebitCompany_ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_MobileDevice_MobileDevice_id] FOREIGN KEY ([MobileDevice_id]) REFERENCES [dbo].[MobileDevice] ([MobileDevice_id])
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_PaymentMethod_PaymentMethod] FOREIGN KEY ([PaymentMethod]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_PhoneDetail_PhoneDetailsID] FOREIGN KEY ([PhoneDetailsID]) REFERENCES [Trans].[PhoneDetail] ([PhoneDetail_id])
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_tblRecurringSeries_RecurringSeries] FOREIGN KEY ([RecurringSeries]) REFERENCES [dbo].[tblRecurringSeries] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_TransPayerInfo] FOREIGN KEY ([TransPayerInfo_id]) REFERENCES [Trans].[TransPayerInfo] ([TransPayerInfo_id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_TransPaymentMethod] FOREIGN KEY ([TransPaymentMethod_id]) REFERENCES [Trans].[TransPaymentMethod] ([TransPaymentMethod_id])
GO
ALTER TABLE [dbo].[tblCompanyTransPass] ADD CONSTRAINT [FK_tblCompanyTransPass_TransSource_TransSource_id] FOREIGN KEY ([TransSource_id]) REFERENCES [List].[TransSource] ([TransSource_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transactions approved', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyTransPass', NULL, NULL
GO
