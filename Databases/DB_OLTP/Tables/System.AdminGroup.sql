CREATE TABLE [System].[AdminGroup]
(
[AdminGroup_id] [smallint] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL,
[Description] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[IsActive] [bit] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'System admin users group', 'SCHEMA', N'System', 'TABLE', N'AdminGroup', NULL, NULL
GO

ALTER TABLE [System].[AdminGroup] ADD CONSTRAINT [PK_AdminGroup] PRIMARY KEY CLUSTERED  ([AdminGroup_id]) ON [PRIMARY]
GO
