CREATE TABLE [Setting].[SetMerchantRollingReserve]
(
[SetMerchantRollingReserve_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[ActionType] [tinyint] NOT NULL CONSTRAINT [DF_SetMerchantRollingReserve_ActionType] DEFAULT ((0)),
[IsAutoReserve] [bit] NOT NULL CONSTRAINT [DF_SetMerchantRollingReserve_IsAutoReserve] DEFAULT ((0)),
[IsAutoReturn] [bit] NOT NULL CONSTRAINT [DF_SetMerchantRollingReserve_IsAutoReturn] DEFAULT ((0)),
[FixHoldAmount] [money] NULL,
[FixHoldCurrency] [tinyint] NULL,
[ReservePercentage] [float] NULL,
[ReservePeriod] [smallint] NULL,
[Comment] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantRollingReserve] ADD CONSTRAINT [PK_SetMerchantRollingReserve] PRIMARY KEY CLUSTERED  ([SetMerchantRollingReserve_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantRollingReserve] ADD CONSTRAINT [UIX_SetMerchantRollingReserveMerchant_id] UNIQUE NONCLUSTERED  ([Merchant_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantRollingReserve] ADD CONSTRAINT [FK_SetMerchantRollingReserve_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
