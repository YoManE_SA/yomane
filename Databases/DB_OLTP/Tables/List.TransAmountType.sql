CREATE TABLE [List].[TransAmountType]
(
[TransAmountType_id] [int] NOT NULL,
[Name] [varchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[TransAmountType] ADD CONSTRAINT [PK_TransAmountType] PRIMARY KEY CLUSTERED  ([TransAmountType_id]) ON [PRIMARY]
GO
