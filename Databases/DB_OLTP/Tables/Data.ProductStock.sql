CREATE TABLE [Data].[ProductStock]
(
[ProductStock_id] [int] NOT NULL IDENTITY(1, 1),
[Product_id] [int] NOT NULL,
[SKU] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[QtyAvailable] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductStock] ADD CONSTRAINT [PK_ProductStock] PRIMARY KEY CLUSTERED  ([ProductStock_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductStock] ADD CONSTRAINT [FK_ProductStock_Product] FOREIGN KEY ([Product_id]) REFERENCES [Data].[Product] ([Product_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s product''s stock', 'SCHEMA', N'Data', 'TABLE', N'ProductStock', NULL, NULL
GO
