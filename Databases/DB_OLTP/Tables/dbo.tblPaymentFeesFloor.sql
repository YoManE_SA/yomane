CREATE TABLE [dbo].[tblPaymentFeesFloor]
(
[CFF_ID] [int] NOT NULL IDENTITY(1, 1),
[CFF_CompanyID] [int] NOT NULL,
[CFF_PayID] [int] NOT NULL,
[CFF_TotalTo] [money] NOT NULL,
[CFF_Precent] [money] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPaymentFeesFloor] ADD CONSTRAINT [FK_tblPaymentFeesFloor_tblCompany_CFF_CompanyID] FOREIGN KEY ([CFF_CompanyID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPaymentFeesFloor] ADD CONSTRAINT [FK_tblPaymentFeesFloor_tblTransactionPay_CFF_PayID] FOREIGN KEY ([CFF_PayID]) REFERENCES [dbo].[tblTransactionPay] ([id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'[Not used]', 'SCHEMA', N'dbo', 'TABLE', N'tblPaymentFeesFloor', NULL, NULL
GO
