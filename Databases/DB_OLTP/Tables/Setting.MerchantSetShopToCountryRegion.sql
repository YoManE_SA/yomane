CREATE TABLE [Setting].[MerchantSetShopToCountryRegion]
(
[MerchantSetShopToCountryRegion_id] [int] NOT NULL IDENTITY(1, 1),
[MerchantSetShop_id] [int] NOT NULL,
[CountryISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[WorldRegionISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [Setting].[MerchantSetShopToCountryRegion] ADD
CONSTRAINT [FK_MerchantSetShopToCountryRegion_MerchantSetShop_MerchantSetShopID] FOREIGN KEY ([MerchantSetShop_id]) REFERENCES [Setting].[MerchantSetShop] ([MerchantSetShop_id]) ON DELETE CASCADE
GO
ALTER TABLE [Setting].[MerchantSetShopToCountryRegion] ADD CONSTRAINT [PK_MerchantSetShopToCountryRegion] PRIMARY KEY CLUSTERED  ([MerchantSetShopToCountryRegion_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[MerchantSetShopToCountryRegion] ADD CONSTRAINT [FK_MerchantSetShopToCountryRegion_CountryList_CountryISOCode] FOREIGN KEY ([CountryISOCode]) REFERENCES [List].[CountryList] ([CountryISOCode])
GO

ALTER TABLE [Setting].[MerchantSetShopToCountryRegion] ADD CONSTRAINT [FK_MerchantSetShopToCountryRegion_WorldRegion_WorldRegionISOCode] FOREIGN KEY ([WorldRegionISOCode]) REFERENCES [List].[WorldRegion] ([WorldRegionISOCode])
GO
