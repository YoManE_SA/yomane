CREATE TABLE [List].[ExternalServiceType]
(
[ExternalServiceType_id] [varchar] (16) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[ExternalServiceType] ADD CONSTRAINT [PK_ExternalServiceType] PRIMARY KEY CLUSTERED  ([ExternalServiceType_id]) ON [PRIMARY]
GO
