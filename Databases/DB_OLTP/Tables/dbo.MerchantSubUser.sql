CREATE TABLE [dbo].[MerchantSubUser]
(
[MerchantSubUser_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[UserName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[trgMerchantSubUserRemovePasswordHistory] ON [dbo].[MerchantSubUser] AFTER DELETE AS
DELETE FROM tblPasswordHistory WHERE LPH_RefType=4 AND LPH_RefID IN (SELECT [MerchantSubUser_id] FROM Deleted)

GO
ALTER TABLE [dbo].[MerchantSubUser] ADD CONSTRAINT [PK_MerchantSubUser] PRIMARY KEY CLUSTERED  ([MerchantSubUser_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MerchantSubUser] ADD CONSTRAINT [FK_MerchantSubUser_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'MerchantSubUser', NULL, NULL
GO
