CREATE TABLE [Trans].[TransPaymentMethod]
(
[TransPaymentMethod_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[TransPaymentBillingAddress_id] [int] NULL,
[PaymentMethod_id] [smallint] NOT NULL,
[ExpirationDate] [date] NULL,
[Value1Encrypted] [varbinary] (40) NULL,
[Value2Encrypted] [varbinary] (40) NULL,
[PaymentMethodText] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[Value1Last4Text] [char] (4) COLLATE Hebrew_CI_AS NULL,
[Value1First6Text] [char] (6) COLLATE Hebrew_CI_AS NULL,
[IssuerCountryIsoCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[EncryptionKey] [tinyint] NOT NULL,
[CreditCard_id] [int] NULL,
[Echeck_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransPaymentMethod] ADD CONSTRAINT [PK_TransPaymentMethod] PRIMARY KEY CLUSTERED  ([TransPaymentMethod_id]) ON [PRIMARY]
GO
ALTER TABLE [Trans].[TransPaymentMethod] ADD CONSTRAINT [FK_TransPaymentMethod_CountryList_IssuerCountryIsoCode] FOREIGN KEY ([IssuerCountryIsoCode]) REFERENCES [List].[CountryList] ([CountryISOCode])
GO
ALTER TABLE [Trans].[TransPaymentMethod] ADD CONSTRAINT [FK_TransPaymentMethod_tblCompany] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Trans].[TransPaymentMethod] ADD CONSTRAINT [FK_PaymentMethod_PaymentMethod] FOREIGN KEY ([PaymentMethod_id]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
ALTER TABLE [Trans].[TransPaymentMethod] ADD CONSTRAINT [FK_TransPaymentMethod_TransPaymentBillingAddress] FOREIGN KEY ([TransPaymentBillingAddress_id]) REFERENCES [Trans].[TransPaymentBillingAddress] ([TransPaymentBillingAddress_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transaction payment method', 'SCHEMA', N'Trans', 'TABLE', N'TransPaymentMethod', NULL, NULL
GO
