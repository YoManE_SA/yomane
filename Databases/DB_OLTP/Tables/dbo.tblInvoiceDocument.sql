CREATE TABLE [dbo].[tblInvoiceDocument]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[id_BillingCompanyID] [int] NOT NULL,
[id_Type] [int] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_Type] DEFAULT ((0)),
[id_InvoiceNumber] [int] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_InvoiceNumber] DEFAULT ((0)),
[id_TransactionPayID] [int] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_TransactionPayID] DEFAULT ((0)),
[id_BillToName] [nvarchar] (850) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_BillToName] DEFAULT (''),
[id_MerchantID] [int] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_MerchantID] DEFAULT ((0)),
[id_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_InsertDate] DEFAULT (getdate()),
[id_PrintDate] [datetime] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_PrintDate] DEFAULT (getdate()),
[id_Username] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_Username] DEFAULT (''),
[id_Lines] [int] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_Lines] DEFAULT ((0)),
[id_TotalLines] [money] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_TotalLines] DEFAULT ((0)),
[id_ApplyVAT] [bit] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_ApplyVAT] DEFAULT ((0)),
[id_VATPercent] [real] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_VATPercent] DEFAULT ((0)),
[id_TotalVAT] [money] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_TotalVAT] DEFAULT ((0)),
[id_TotalDocument] [money] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_TotalDocument] DEFAULT ((0)),
[id_Currency] [int] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_Currency] DEFAULT ((0)),
[id_CurrencyRate] [money] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_CurrencyRate] DEFAULT ((1)),
[id_IsPrinted] AS (CONVERT([bit],case when [id_InsertDate]=[id_PrintDate] then (0) else (1) end,0)),
[id_BillingCompanyName] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_BillingCompanyName] DEFAULT (''),
[id_BillingCompanyAddress] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_BillingCompanyAddress] DEFAULT (''),
[id_BillingCompanyNumber] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_BillingCompanyNumber] DEFAULT (''),
[id_BillingCompanyEmail] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_BillingCompanyEmail] DEFAULT (''),
[id_BillingCompanyLanguage] [nchar] (3) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_BillingCompanyLanguage] DEFAULT ('heb'),
[id_IsManual] [bit] NOT NULL CONSTRAINT [DF_tblInvoiceDocument_id_IsManual] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgInvoiceDocumentAddGetLines] ON [dbo].[tblInvoiceDocument] AFTER INSERT
AS
BEGIN
	UPDATE
		tblInvoiceLine
	SET
		il_DocumentID=Inserted.ID
	FROM
		tblInvoiceLine,
		Inserted
	WHERE
		(
			(Inserted.id_MerchantID>0 AND Inserted.id_MerchantID=tblInvoiceLine.il_MerchantID)
			OR
			(Inserted.id_BillToName<>'' AND Inserted.id_BillToName=tblInvoiceLine.il_BillToName)
		)
		AND
		IsNull(il_DocumentID, 0)=0
		AND
		il_TransactionPayID=id_TransactionPayID
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgInvoiceDocumentDeleteResetTransactionPay] ON [dbo].[tblInvoiceDocument] FOR DELETE
AS
UPDATE
	tblTransactionPay
SET
	InvoiceDocumentID=0,
	InvoiceNumber=0
WHERE
	InvoiceDocumentID IN (SELECT ID FROM Deleted)
GO
ALTER TABLE [dbo].[tblInvoiceDocument] ADD CONSTRAINT [PK_tblInvoiceDocument] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblInvoiceDocument_BillingCompany_InvoiceNumber_Type] ON [dbo].[tblInvoiceDocument] ([id_BillingCompanyID], [id_InvoiceNumber], [id_Type]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblInvoiceDocument] ADD CONSTRAINT [FK_tblInvoiceDocument_tblBillingCompanys_id_BillingCompanyID] FOREIGN KEY ([id_BillingCompanyID]) REFERENCES [dbo].[tblBillingCompanys] ([BillingCompanys_id])
GO
ALTER TABLE [dbo].[tblInvoiceDocument] ADD CONSTRAINT [FK_tblInvoiceDocument_tblSystemCurrencies_id_Currency] FOREIGN KEY ([id_Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s invoices', 'SCHEMA', N'dbo', 'TABLE', N'tblInvoiceDocument', NULL, NULL
GO
