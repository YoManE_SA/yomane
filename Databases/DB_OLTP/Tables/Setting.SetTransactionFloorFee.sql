CREATE TABLE [Setting].[SetTransactionFloorFee]
(
[SetTransactionFloorFee_id] [int] NOT NULL IDENTITY(1, 1),
[SetTransactionFloor_id] [int] NOT NULL,
[AmountTop] [decimal] (19, 4) NOT NULL,
[PercentValue] [decimal] (7, 4) NULL,
[FixedAmount] [decimal] (7, 4) NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetTransactionFloorFee] ADD CONSTRAINT [PK_SetTransactionFloorFee] PRIMARY KEY CLUSTERED  ([SetTransactionFloorFee_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetTransactionFloorFee] ADD CONSTRAINT [FK_SetTransactionFloorFee_SetTransactionFloor_SetTransactionFloor_id] FOREIGN KEY ([SetTransactionFloor_id]) REFERENCES [Setting].[SetTransactionFloor] ([SetTransactionFloor_id])
GO
