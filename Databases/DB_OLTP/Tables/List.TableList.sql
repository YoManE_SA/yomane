CREATE TABLE [List].[TableList]
(
[TableList_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[TableList] ADD CONSTRAINT [PK_TableList] PRIMARY KEY CLUSTERED  ([TableList_id]) ON [PRIMARY]
GO
