CREATE TABLE [dbo].[tblRefundAsk]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[companyID] [int] NOT NULL,
[transID] [int] NOT NULL CONSTRAINT [DF_tblRefundAsk_transID] DEFAULT ((0)),
[RefundAskAmount] [money] NOT NULL CONSTRAINT [DF_tblRefundAsk_RefundAskAmount] DEFAULT ((0)),
[RefundAskCurrency] [int] NOT NULL CONSTRAINT [DF_tblRefundAsk_RefundAskCurrency] DEFAULT ((0)),
[RefundAskComment] [nvarchar] (300) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblRefundAsk_RefundAskComment] DEFAULT (''),
[RefundAskConfirmationNum] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblRefundAsk_RefundAskConfirmationNum] DEFAULT (''),
[RefundAskDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblRefundAsk_RefundAskDate] DEFAULT (getdate()),
[RefundAskStatus] [int] NOT NULL CONSTRAINT [DF_tblRefundAsk_RefundAskStatus] DEFAULT ((0)),
[RefundAskStatusHistory] [nvarchar] (4000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblRefundAsk_RefundAskStatusHistory] DEFAULT (''),
[RefundFlag] [int] NOT NULL CONSTRAINT [DF_tblRefundAsk_RefundFlag] DEFAULT ((0)),
[RefundType] [tinyint] NOT NULL CONSTRAINT [DF_tblRefundAsk_RefundType] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRefundAsk] ADD CONSTRAINT [PK_tblRefundAsk] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblRefundAsk_CompanyID] ON [dbo].[tblRefundAsk] ([companyID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [askDate] ON [dbo].[tblRefundAsk] ([RefundAskDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblRefundAsk_transID] ON [dbo].[tblRefundAsk] ([transID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRefundAsk] ADD CONSTRAINT [FK_tblRefundAsk_tblCompany_companyID] FOREIGN KEY ([companyID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblRefundAsk] ADD CONSTRAINT [FK_tblRefundAsk_tblSystemCurrencies_RefundAskCurrency] FOREIGN KEY ([RefundAskCurrency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
ALTER TABLE [dbo].[tblRefundAsk] ADD CONSTRAINT [FK_tblRefundAsk_tblCompanyTransPass_transID] FOREIGN KEY ([transID]) REFERENCES [dbo].[tblCompanyTransPass] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s request to refund an approved transaction', 'SCHEMA', N'dbo', 'TABLE', N'tblRefundAsk', NULL, NULL
GO
