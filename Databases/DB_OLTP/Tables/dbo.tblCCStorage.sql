CREATE TABLE [dbo].[tblCCStorage]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[companyID] [int] NULL,
[PaymentMethod] [smallint] NOT NULL,
[payID] [int] NOT NULL CONSTRAINT [DF_tblCCStorage_payID] DEFAULT ((0)),
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCCStorage_InsertDate] DEFAULT (getdate()),
[CHPersonalNum] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCCStorage_CHPersonalNum] DEFAULT (''),
[ExpMM] [char] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCCStorage_ExpMM] DEFAULT (''),
[ExpYY] [char] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCCStorage_ExpYY] DEFAULT (''),
[cc_cui] [nvarchar] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCCStorage_cc_cui] DEFAULT (''),
[CHPhoneNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCCStorage_CHPhoneNumber] DEFAULT (''),
[CHEmail] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCCStorage_CHEmail] DEFAULT (''),
[CHFullName] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCCStorage_CHFullName] DEFAULT (''),
[CCard_display] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCCStorage_CCard_display] DEFAULT (''),
[IPAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCCStorage_IPAddress] DEFAULT (''),
[isDeleted] [bit] NOT NULL CONSTRAINT [DF_tblCCStorage_isDeleted] DEFAULT ((0)),
[Comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCCStorage_Comment] DEFAULT (''),
[CCard_number256] [varbinary] (200) NULL,
[CCardLast4] [nvarchar] (4) COLLATE Hebrew_CI_AS NULL,
[CHStreet] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[CHSCity] [nvarchar] (60) COLLATE Hebrew_CI_AS NULL,
[CHSZipCode] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[stateId] [int] NULL,
[countryId] [int] NULL,
[BINCountry] [nchar] (2) COLLATE Hebrew_CI_AS NULL,
[CHStreet1] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCCStorage] ADD CONSTRAINT [PK_tblCCStorage] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_StoreCreditCard_companyID_CCard_number256_isDeleted] ON [dbo].[tblCCStorage] ([companyID] DESC, [CCard_number256], [isDeleted]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCCStorage] ADD CONSTRAINT [FK_tblCCStorage_tblCompany_companyID] FOREIGN KEY ([companyID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCCStorage] ADD CONSTRAINT [FK_tblCCStorage_CountryList_CountryId] FOREIGN KEY ([countryId]) REFERENCES [List].[CountryList] ([CountryID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblCCStorage] ADD CONSTRAINT [FK_tblCCStorage_PaymentMethod_PaymentMethod] FOREIGN KEY ([PaymentMethod]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
ALTER TABLE [dbo].[tblCCStorage] ADD CONSTRAINT [FK_tblCCStorage_StateList_StateId] FOREIGN KEY ([stateId]) REFERENCES [List].[StateList] ([StateID]) ON DELETE SET NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s credit card storage', 'SCHEMA', N'dbo', 'TABLE', N'tblCCStorage', NULL, NULL
GO
