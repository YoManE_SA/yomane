CREATE TABLE [Setting].[SetMerchantPeriodicFee]
(
[SetMerchantPeriodicFee_id] [int] NOT NULL IDENTITY(1, 1),
[PeriodicFeeType_id] [tinyint] NOT NULL,
[Merchant_id] [int] NOT NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[Amount] [decimal] (19, 3) NOT NULL,
[IsActive] [bit] NOT NULL,
[DateNextCharge] [date] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantPeriodicFee] ADD CONSTRAINT [PK_SetMerchantPeriodicFee] PRIMARY KEY CLUSTERED  ([SetMerchantPeriodicFee_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantPeriodicFee] ADD CONSTRAINT [FK_SetMerchantPeriodicFee_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Setting].[SetMerchantPeriodicFee] ADD CONSTRAINT [FK_SetMerchantPeriodicFee_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
