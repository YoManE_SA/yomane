CREATE TABLE [Data].[LoginPassword]
(
[LoginPassword_id] [int] NOT NULL IDENTITY(1, 1),
[LoginAccount_id] [int] NOT NULL,
[InsertDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_LoginPassword_InsertDate] DEFAULT (sysdatetime()),
[PasswordEncrypted] [varbinary] (100) NOT NULL,
[EncryptionKey] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[LoginPassword] ADD CONSTRAINT [PK_LoginPassword] PRIMARY KEY CLUSTERED  ([LoginPassword_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[LoginPassword] ADD CONSTRAINT [FK_LoginPassword_LoginAccountID] FOREIGN KEY ([LoginAccount_id]) REFERENCES [Data].[LoginAccount] ([LoginAccount_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account login password details', 'SCHEMA', N'Data', 'TABLE', N'LoginPassword', NULL, NULL
GO
