CREATE TABLE [List].[SystemUserType]
(
[UserType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (25) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[SystemUserType] ADD CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED  ([UserType_id]) ON [PRIMARY]
GO
