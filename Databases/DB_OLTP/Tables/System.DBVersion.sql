CREATE TABLE [System].[DBVersion]
(
[DBVersion_id] [int] NOT NULL IDENTITY(1, 1),
[DeployDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_DBVersion_DeployDate] DEFAULT (sysdatetime()),
[Version] [decimal] (6, 2) NOT NULL,
[Comment] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[DBVersion] ADD CONSTRAINT [PK_DBVersion] PRIMARY KEY CLUSTERED  ([DBVersion_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'System DB version', 'SCHEMA', N'System', 'TABLE', N'DBVersion', NULL, NULL
GO
