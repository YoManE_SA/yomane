CREATE TABLE [List].[LoginResult]
(
[LoginResult_id] [tinyint] NOT NULL,
[Name] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[LoginResult] ADD CONSTRAINT [PK_LoginResult] PRIMARY KEY CLUSTERED  ([LoginResult_id]) ON [PRIMARY]
GO
