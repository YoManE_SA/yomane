CREATE TABLE [List].[TransDeniedStatus]
(
[DeniedStatus] [tinyint] NOT NULL,
[Name] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[TransDeniedStatus] ADD CONSTRAINT [PK_TransDeniedStatus] PRIMARY KEY CLUSTERED  ([DeniedStatus]) ON [PRIMARY]
GO
