CREATE TABLE [List].[LanguageList]
(
[Language_id] [tinyint] NOT NULL IDENTITY(0, 1),
[ISO2] [char] (2) COLLATE Hebrew_CI_AS NULL,
[ISO3] [char] (3) COLLATE Hebrew_CI_AS NULL,
[Name] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[NativeName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Culture] [varchar] (5) COLLATE Hebrew_CI_AS NULL,
[IsRTL] [bit] NOT NULL CONSTRAINT [DF_LanguageList_IsRTL] DEFAULT ((0)),
[LanguageISOCode] [char] (2) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[LanguageList] ADD CONSTRAINT [PK_LanguageList] PRIMARY KEY CLUSTERED  ([Language_id]) ON [PRIMARY]
GO
ALTER TABLE [List].[LanguageList] ADD CONSTRAINT [UIX_LanguageList_LanguageISOCode] UNIQUE NONCLUSTERED  ([LanguageISOCode]) ON [PRIMARY]
GO
