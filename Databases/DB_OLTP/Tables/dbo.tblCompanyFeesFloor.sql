CREATE TABLE [dbo].[tblCompanyFeesFloor]
(
[CFF_ID] [int] NOT NULL IDENTITY(1, 1),
[CFF_CompanyID] [int] NOT NULL,
[CFF_TotalTo] [money] NOT NULL CONSTRAINT [DF_tblCompanyFeesFloor_CFF_TotalTo] DEFAULT ((0)),
[CFF_Precent] [money] NOT NULL CONSTRAINT [DF_tblCompanyFeesFloor_CFF_Precent] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyFeesFloor] ADD CONSTRAINT [PK_tblCompanyFeesFloor] PRIMARY KEY CLUSTERED  ([CFF_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyFeesFloor] ADD CONSTRAINT [FK_tblCompanyFeesFloor_tblCompany_CFF_CompanyID] FOREIGN KEY ([CFF_CompanyID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
