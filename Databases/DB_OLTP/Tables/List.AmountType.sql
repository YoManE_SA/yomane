CREATE TABLE [List].[AmountType]
(
[AmountType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[SortOrder] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[AmountType] ADD CONSTRAINT [PK_AmountType] PRIMARY KEY CLUSTERED  ([AmountType_id]) ON [PRIMARY]
GO
