CREATE TABLE [dbo].[tblFraudCcBlackList]
(
[fraudCcBlackList_id] [int] NOT NULL IDENTITY(1, 1),
[company_id] [int] NOT NULL CONSTRAINT [DF_tblFraudCcBlackList_company_id] DEFAULT ((0)),
[fcbl_ccDisplay] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblFraudCcBlackList_fcbl_ccDisplay] DEFAULT (''),
[fcbl_comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblFraudCcBlackList_fcbl_comment] DEFAULT (''),
[fcbl_ccNumber256] [varbinary] (200) NULL,
[fcbl_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblFraudCcBlackList_fcbl_InsertDate] DEFAULT (getdate()),
[fcbl_BlockLevel] [tinyint] NOT NULL CONSTRAINT [DF_tblFraudCcBlackList_fcbl_BlockLevel] DEFAULT ((0)),
[fcbl_BlockCount] [int] NOT NULL CONSTRAINT [DF_tblFraudCcBlackList_fcbl_BlockCount] DEFAULT ((0)),
[fcbl_ReplyCode] [nvarchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblFraudCcBlackList_fcbl_ReplyCode] DEFAULT (''),
[fcbl_UnblockDate] [datetime] NOT NULL CONSTRAINT [DF_tblFraudCcBlackList_fcbl_UnblockDate] DEFAULT (getdate()),
[fcbl_CCRMID] [int] NOT NULL CONSTRAINT [DF_tblFraudCcBlackList_fcbl_CCRMID] DEFAULT ((0)),
[PaymentMethod_id] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFraudCcBlackList] ADD CONSTRAINT [PK_tblFraudCcBlackList] PRIMARY KEY CLUSTERED  ([fraudCcBlackList_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblFraudCCBlackList_fcbl_ccNumber256_CCRMID_InsertDate_id] ON [dbo].[tblFraudCcBlackList] ([fcbl_ccNumber256], [fcbl_CCRMID] DESC, [fcbl_InsertDate] DESC, [fraudCcBlackList_id] DESC) WITH (FILLFACTOR=85) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFraudCcBlackList] ADD CONSTRAINT [FK_tblFraudCcBlackList_BlockLevel_fcbl_BlockLevel] FOREIGN KEY ([fcbl_BlockLevel]) REFERENCES [List].[BlockLevel] ([BlockLevel_id])
GO
ALTER TABLE [dbo].[tblFraudCcBlackList] ADD CONSTRAINT [FK_tblFraudCcBlackList_PaymentMethod_PaymentMethod_id] FOREIGN KEY ([PaymentMethod_id]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Credit card black list', 'SCHEMA', N'dbo', 'TABLE', N'tblFraudCcBlackList', NULL, NULL
GO
