CREATE TABLE [Data].[Account]
(
[Account_id] [int] NOT NULL IDENTITY(1, 1),
[AccountType_id] [tinyint] NOT NULL,
[Merchant_id] [int] NULL,
[Customer_id] [int] NULL,
[Affiliate_id] [int] NULL,
[DebitCompany_id] [int] NULL,
[PersonalAddress_id] [int] NULL,
[BusinessAddress_id] [int] NULL,
[PreferredWireProvider_id] [varchar] (16) COLLATE Hebrew_CI_AS NULL,
[AccountNumber] [char] (7) COLLATE Hebrew_CI_AS NULL,
[LoginAccount_id] [int] NULL,
[PincodeSHA256] [varchar] (64) COLLATE Hebrew_CI_AS NULL,
[Name] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[HashKey] [nvarchar] (32) COLLATE Hebrew_CI_AS NULL,
[TimeZoneOffsetUI] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED  ([Account_id]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Account_Affiliate_id] ON [Data].[Account] ([Affiliate_id]) WHERE ([Affiliate_id] IS NOT NULL) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Account_Customer_id] ON [Data].[Account] ([Customer_id]) WHERE ([Customer_id] IS NOT NULL) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Account_DebitCompany_id] ON [Data].[Account] ([DebitCompany_id]) WHERE ([DebitCompany_id] IS NOT NULL) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Account_Merchant_id] ON [Data].[Account] ([Merchant_id]) WHERE ([Merchant_id] IS NOT NULL) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [FK_Account_AccountType_AccountTypeID] FOREIGN KEY ([AccountType_id]) REFERENCES [List].[AccountType] ([AccountType_id])
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [FK_Account_tblAffiliates_AffiliateID] FOREIGN KEY ([Affiliate_id]) REFERENCES [dbo].[tblAffiliates] ([affiliates_id])
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [FK_Account_AccountAddress_BusinessAddressID] FOREIGN KEY ([BusinessAddress_id]) REFERENCES [Data].[AccountAddress] ([AccountAddress_id])
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [FK_Account_Customer_CustomerID] FOREIGN KEY ([Customer_id]) REFERENCES [Data].[Customer] ([Customer_id])
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [FK_Account_tblDebitCompany_DebitCompanyID] FOREIGN KEY ([DebitCompany_id]) REFERENCES [dbo].[tblDebitCompany] ([DebitCompany_ID])
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [FK_Account_LoginAccount_LoginAccountID] FOREIGN KEY ([LoginAccount_id]) REFERENCES [Data].[LoginAccount] ([LoginAccount_id]) ON DELETE SET NULL
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [FK_Account_tblCompany_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [FK_Account_AccountAddress_PersonalAddressID] FOREIGN KEY ([PersonalAddress_id]) REFERENCES [Data].[AccountAddress] ([AccountAddress_id]) ON DELETE SET NULL
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [FK_Account_WireProvider_WireProviderID] FOREIGN KEY ([PreferredWireProvider_id]) REFERENCES [List].[WireProvider] ([WireProvider_id])
GO
ALTER TABLE [Data].[Account] ADD CONSTRAINT [FK_Account_TimeZone_TimeZoneOffsetUI] FOREIGN KEY ([TimeZoneOffsetUI]) REFERENCES [List].[TimeZone] ([TimeZoneOffsetMinutes])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Accounts - Merchants, Customers, DebitCompanys and Affiliates', 'SCHEMA', N'Data', 'TABLE', N'Account', NULL, NULL
GO
