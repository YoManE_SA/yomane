CREATE TABLE [Risk].[BlacklistMerchant]
(
[BlacklistMerchant_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NULL,
[InsertDate] [datetime] NULL CONSTRAINT [DF_BlacklistMerchant_InsertDate] DEFAULT (getdate()),
[InsertedBy] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[FirstName] [nvarchar] (150) COLLATE Hebrew_CI_AS NULL,
[LastName] [nvarchar] (150) COLLATE Hebrew_CI_AS NULL,
[Street] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[City] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Phone] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Cellular] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[IDNumber] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[CompanyName] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[CompanyLegalName] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[CompanyLegalNumber] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[CompanyStreet] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[CompanyCity] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[CompanyPhone] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[CompanyFax] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Mail] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[URL] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[MerchantSupportEmail] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[MerchantSupportPhoneNum] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[IpOnReg] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[PaymentPayeeName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[PaymentBranch] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[PaymentAccount] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[SearchLink] AS (('<a href="#" onclick="OpenMerchantSearch('+str([BlacklistMerchant_id]))+');return false;">Merchants</a>'),
[DeleteButton] AS (('<input type="button" class="buttonWhite buttonDelete" value="Delete" onclick="ConfirmAndDeleteRecord('+str([BlacklistMerchant_id]))+');" />'),
[PaymentBank] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Risk].[BlacklistMerchant] ADD CONSTRAINT [PK_BlacklistMerchant] PRIMARY KEY CLUSTERED  ([BlacklistMerchant_id]) ON [PRIMARY]
GO
ALTER TABLE [Risk].[BlacklistMerchant] ADD CONSTRAINT [FK_BlacklistMerchant_tblCompany] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE SET NULL
GO
