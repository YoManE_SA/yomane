CREATE TABLE [List].[BlockLevel]
(
[BlockLevel_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[BlockLevel] ADD CONSTRAINT [PK_BlockLevel] PRIMARY KEY CLUSTERED  ([BlockLevel_id]) ON [PRIMARY]
GO
