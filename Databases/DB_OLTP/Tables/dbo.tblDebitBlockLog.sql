CREATE TABLE [dbo].[tblDebitBlockLog]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[dbl_Date] [datetime] NOT NULL CONSTRAINT [DF_tblDebitBlockLog_dbl_Date] DEFAULT (getdate()),
[dbl_Type] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitBlockLog_dbl_Type] DEFAULT (''),
[dbl_Text] [nvarchar] (950) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitBlockLog_dbl_Text] DEFAULT (''),
[dbl_DebitCompany] [int] NOT NULL CONSTRAINT [DF_tblDebitBlockLog_dbl_DebitCompany] DEFAULT ((0)),
[dbl_DebitRule] [int] NOT NULL CONSTRAINT [DF_tblDebitBlockLog_dbl_DebitRule] DEFAULT ((0)),
[dbl_DebitTerminal] [int] NOT NULL CONSTRAINT [DF_tblDebitBlockLog_dbl_DebitTerminal] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDebitBlockLog] ADD CONSTRAINT [PK_tblDebitBlockLog] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Debit company block log', 'SCHEMA', N'dbo', 'TABLE', N'tblDebitBlockLog', NULL, NULL
GO
