CREATE TABLE [List].[CurrencyList]
(
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[ISONumber] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Symbol] [nvarchar] (25) COLLATE Hebrew_CI_AS NULL,
[BaseRate] [decimal] (7, 4) NULL,
[ExchangeFeeInd] [decimal] (6, 4) NULL,
[RateRequestDate] [datetime2] (0) NULL,
[RateValueDate] [datetime2] (0) NULL,
[MaxTransactionAmount] [money] NULL,
[IsSymbolBeforeAmount] [bit] NULL CONSTRAINT [DF_CurrencyList_IsSymbolBeforeAmount] DEFAULT ((1)),
[InsertDate] [datetime2] (0) NULL CONSTRAINT [DF_CurrencyList_InsertDate] DEFAULT (sysdatetime()),
[CurrencyID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[CurrencyList] ADD CONSTRAINT [PK_CurrencyList] PRIMARY KEY CLUSTERED  ([CurrencyISOCode]) ON [PRIMARY]
GO
