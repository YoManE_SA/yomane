CREATE TABLE [dbo].[tblBlackListBIN]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[BIN] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBlackListBIN_BIN] DEFAULT (''),
[PrimaryID] [int] NULL CONSTRAINT [DF_tblBlackListBIN_PrimaryID] DEFAULT ((0)),
[TransFailCountCC] [int] NOT NULL CONSTRAINT [DF_tblBlackListBIN_TransFailCountCC] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBlackListBIN] ADD CONSTRAINT [PK_tblBlackListBIN] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBlackListBIN] ADD CONSTRAINT [FK_tblBlackListBIN_tblBlackListBIN_PrimaryID] FOREIGN KEY ([PrimaryID]) REFERENCES [dbo].[tblBlackListBIN] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used in code, check why and rename id not delete', 'SCHEMA', N'dbo', 'TABLE', N'tblBlackListBIN', NULL, NULL
GO
