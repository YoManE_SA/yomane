CREATE TABLE [Data].[LoginAccount]
(
[LoginAccount_id] [int] NOT NULL IDENTITY(1, 1),
[LoginRole_id] [tinyint] NOT NULL,
[IsActive] [bit] NOT NULL,
[LoginUser] [varchar] (30) COLLATE Hebrew_CI_AS NULL,
[LoginEmail] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FailCount] [tinyint] NULL,
[LastFailTime] [datetime2] (2) NULL,
[LastSuccessTime] [datetime2] (2) NULL,
[BlockEndTime] [datetime2] (2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[LoginAccount] ADD CONSTRAINT [PK_LoginAccount] PRIMARY KEY CLUSTERED  ([LoginAccount_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[LoginAccount] ADD CONSTRAINT [FK_LoginAccount_LoginRole_LoginRoleID] FOREIGN KEY ([LoginRole_id]) REFERENCES [List].[LoginRole] ([LoginRole_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account login details', 'SCHEMA', N'Data', 'TABLE', N'LoginAccount', NULL, NULL
GO
