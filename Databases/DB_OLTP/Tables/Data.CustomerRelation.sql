CREATE TABLE [Data].[CustomerRelation]
(
[Customer_id] [int] NOT NULL,
[TargetCustomer_id] [int] NOT NULL,
[PeopleRelationType_id] [tinyint] NULL,
[IsActive] [bit] NULL CONSTRAINT [DF_CustomerRelation_IsActive] DEFAULT ((0)),
[ConfirmationDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[CustomerRelation] ADD CONSTRAINT [PK_CustomerRelation] PRIMARY KEY CLUSTERED  ([Customer_id], [TargetCustomer_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [Data].[CustomerRelation] ADD CONSTRAINT [FK_CustomerRelation_CustomerID] FOREIGN KEY ([Customer_id]) REFERENCES [Data].[Customer] ([Customer_id])
GO
ALTER TABLE [Data].[CustomerRelation] ADD CONSTRAINT [FK_CustomerRelation_PeopleRelationTypeID] FOREIGN KEY ([PeopleRelationType_id]) REFERENCES [List].[PeopleRelationType] ([PeopleRelationType_id]) ON DELETE SET NULL
GO
ALTER TABLE [Data].[CustomerRelation] ADD CONSTRAINT [FK_CustomerRelation_TargetCustomerID] FOREIGN KEY ([TargetCustomer_id]) REFERENCES [Data].[Customer] ([Customer_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Customer to customer relation', 'SCHEMA', N'Data', 'TABLE', N'CustomerRelation', NULL, NULL
GO
