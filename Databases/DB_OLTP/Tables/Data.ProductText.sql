CREATE TABLE [Data].[ProductText]
(
[Product_id] [int] NOT NULL,
[LanguageISOCode] [char] (2) COLLATE Hebrew_CI_AS NOT NULL,
[IsDefaultLanguage] [bit] NOT NULL,
[Name] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL,
[Description] [nvarchar] (1000) COLLATE Hebrew_CI_AS NULL,
[ReceiptText] [nvarchar] (600) COLLATE Hebrew_CI_AS NULL,
[ReceiptLink] [nvarchar] (600) COLLATE Hebrew_CI_AS NULL,
[MetaTitle] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[MetaDescription] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL,
[MetaKeyword] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductText] ADD CONSTRAINT [PK_ProductText] PRIMARY KEY CLUSTERED  ([Product_id], [LanguageISOCode]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductText] ADD CONSTRAINT [FK_ProductText_LanguageISOCode] FOREIGN KEY ([LanguageISOCode]) REFERENCES [List].[LanguageList] ([LanguageISOCode])
GO
ALTER TABLE [Data].[ProductText] ADD CONSTRAINT [FK_ProductText_Product] FOREIGN KEY ([Product_id]) REFERENCES [Data].[Product] ([Product_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s product''s text by language', 'SCHEMA', N'Data', 'TABLE', N'ProductText', NULL, NULL
GO
