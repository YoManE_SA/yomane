CREATE TABLE [Setting].[SetSettlement]
(
[SetSettlement_id] [int] NOT NULL IDENTITY(1, 1),
[SettlementType_id] [tinyint] NOT NULL,
[Account_id] [int] NULL,
[InvoiceIssuer_id] [tinyint] NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[IsShowToSettle] [bit] NOT NULL CONSTRAINT [DF_SetSettlement_IsShowToSettle] DEFAULT ((0)),
[MinPayoutAmount] [decimal] (19, 2) NULL,
[MinSettlementAmount] [decimal] (19, 2) NULL,
[SettleInitialHoldDays] [tinyint] NULL,
[SettleHoldDays] [tinyint] NULL,
[WireFeePercent] [decimal] (7, 4) NULL,
[WireFeeAmount] [decimal] (19, 4) NULL,
[WireCurrencyIsoCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[StorageFeeAmount] [decimal] (19, 4) NULL,
[StorageFeeCurrencyIsoCode] [char] (3) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetSettlement] ADD CONSTRAINT [PK_SetSettlement] PRIMARY KEY CLUSTERED  ([SetSettlement_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetSettlement] ADD CONSTRAINT [FK_SetSettlement_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id]) ON DELETE CASCADE
GO
ALTER TABLE [Setting].[SetSettlement] ADD CONSTRAINT [FK_Settlement_SetSettlementID] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Setting].[SetSettlement] ADD CONSTRAINT [FK_SetSettlement_InvoiceIssuerID] FOREIGN KEY ([InvoiceIssuer_id]) REFERENCES [Finance].[InvoiceIssuer] ([InvoiceIssuer_id])
GO
