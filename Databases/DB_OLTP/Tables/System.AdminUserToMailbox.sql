CREATE TABLE [System].[AdminUserToMailbox]
(
[AdminUserToMailbox_id] [smallint] NOT NULL IDENTITY(1, 1),
[AdminUser_id] [smallint] NOT NULL,
[Mailbox] [varchar] (100) COLLATE Hebrew_CI_AS NOT NULL,
[isDefault] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[AdminUserToMailbox] ADD CONSTRAINT [PK_AdminUserToMailbox] PRIMARY KEY CLUSTERED  ([AdminUserToMailbox_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[AdminUserToMailbox] ADD CONSTRAINT [FK_AdminUserToMailbox_AdminUser_AdminUserID] FOREIGN KEY ([AdminUser_id]) REFERENCES [System].[AdminUser] ([AdminUser_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'System admin users to mailbox', 'SCHEMA', N'System', 'TABLE', N'AdminUserToMailbox', NULL, NULL
GO
