CREATE TABLE [List].[PeopleRelationType]
(
[PeopleRelationType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[Description] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[PeopleRelationType] ADD CONSTRAINT [PK_PeopleRelationType] PRIMARY KEY CLUSTERED  ([PeopleRelationType_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
