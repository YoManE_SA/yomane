CREATE TABLE [Log].[History]
(
[History_id] [int] NOT NULL IDENTITY(1, 1),
[HistoryType_id] [tinyint] NOT NULL,
[Merchant_id] [int] NULL,
[SourceIdentity] [int] NULL,
[InsertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_History_InsertDate] DEFAULT (sysdatetime()),
[InsertUserName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[InsertIPAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[VariableChar] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[VariableXML] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Log].[History] ADD CONSTRAINT [PK_History] PRIMARY KEY CLUSTERED  ([History_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogHistory_InsertDate] ON [Log].[History] ([InsertDate]) ON [PRIMARY]
GO
ALTER TABLE [Log].[History] ADD CONSTRAINT [FK_History_HistoryType_HistoryType_id] FOREIGN KEY ([HistoryType_id]) REFERENCES [List].[HistoryType] ([HistoryType_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log various types of request/response', 'SCHEMA', N'Log', 'TABLE', N'History', NULL, NULL
GO
