CREATE TABLE [List].[HistoryType]
(
[HistoryType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[HistoryType] ADD CONSTRAINT [PK_HistoryType] PRIMARY KEY CLUSTERED  ([HistoryType_id]) ON [PRIMARY]
GO
