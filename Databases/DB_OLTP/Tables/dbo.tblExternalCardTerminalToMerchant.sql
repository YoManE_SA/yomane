CREATE TABLE [dbo].[tblExternalCardTerminalToMerchant]
(
[ExternalCardTerminalToMerchant_id] [int] NOT NULL IDENTITY(1, 1),
[ExternalCardTerminal_id] [int] NOT NULL,
[Merchant_id] [int] NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_tblExternalCardTerminalToMerchant_IsActive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExternalCardTerminalToMerchant] ADD CONSTRAINT [PK_tblExternalCardTerminalToMerchant] PRIMARY KEY CLUSTERED  ([ExternalCardTerminalToMerchant_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExternalCardTerminalToMerchant] ADD CONSTRAINT [FK_tblExternalCardTerminalToMerchant_tblExternalCardTerminal_ExternalCardTerminal_id] FOREIGN KEY ([ExternalCardTerminal_id]) REFERENCES [dbo].[tblExternalCardTerminal] ([ExternalCardTerminal_id])
GO
ALTER TABLE [dbo].[tblExternalCardTerminalToMerchant] ADD CONSTRAINT [FK_tblExternalCardTerminalToMerchant_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblExternalCardTerminalToMerchant', NULL, NULL
GO
