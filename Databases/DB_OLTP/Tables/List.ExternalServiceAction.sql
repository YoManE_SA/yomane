CREATE TABLE [List].[ExternalServiceAction]
(
[ExternalServiceAction_id] [varchar] (32) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[ExternalServiceAction] ADD CONSTRAINT [PK_ExternalServiceAction] PRIMARY KEY CLUSTERED  ([ExternalServiceAction_id]) ON [PRIMARY]
GO
