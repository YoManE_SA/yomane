CREATE TABLE [dbo].[tblRecurringCharge]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[rc_Series] [int] NOT NULL CONSTRAINT [DF_tblRecurringCharge_rc_Series] DEFAULT ((0)),
[rc_ChargeNumber] [int] NOT NULL CONSTRAINT [DF_tblRecurringCharge_rc_ChargeNumber] DEFAULT ((0)),
[rc_Date] [datetime] NOT NULL CONSTRAINT [DF_tblRecurringCharge_rc_Date] DEFAULT (getdate()),
[rc_CreditCard] [int] NULL CONSTRAINT [DF_tblRecurringCharge_rc_CreditCard] DEFAULT ((0)),
[rc_ECheck] [int] NULL CONSTRAINT [DF_tblRecurringCharge_rc_ECheck] DEFAULT ((0)),
[rc_Comments] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblRecurringCharge_rc_Comments] DEFAULT (''),
[rc_Suspended] [bit] NOT NULL CONSTRAINT [DF_tblRecurringCharge_rc_Suspended] DEFAULT ((0)),
[rc_Blocked] [bit] NOT NULL CONSTRAINT [DF_tblRecurringCharge_rc_Blocked] DEFAULT ((0)),
[rc_Paid] [bit] NOT NULL CONSTRAINT [DF_tblRecurringCharge_rc_Paid] DEFAULT ((0)),
[rc_Amount] [money] NOT NULL CONSTRAINT [DF_tblRecurringCharge_rc_Amount] DEFAULT ((0)),
[rc_Currency] [int] NULL,
[rc_Attempts] [int] NOT NULL CONSTRAINT [DF_tblRecurringCharge_rc_Attempts] DEFAULT ((0)),
[rc_Pending] [bit] NOT NULL CONSTRAINT [DF_tblRecurringCharge_rc_Pending] DEFAULT ((0)),
[rc_SeriesTemp] [int] NULL,
[TransPaymentMethod_id] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgRecurringChargeUpdateSeries] ON [dbo].[tblRecurringCharge] FOR UPDATE AS
BEGIN
	SET NOCOUNT ON
	UPDATE
		tblRecurringSeries
	SET
		rs_Paid=1
	WHERE
		ID IN (SELECT rc_Series FROM Inserted WHERE rc_Paid=1)
		AND
		ID NOT IN (SELECT rc_Series FROM tblRecurringCharge WHERE rc_Paid=0)
	SET NOCOUNT OFF
END
GO
ALTER TABLE [dbo].[tblRecurringCharge] ADD CONSTRAINT [PK_tblRecurringCharge] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblRecurringCharge_CreditCard] ON [dbo].[tblRecurringCharge] ([rc_CreditCard] DESC) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_RecurringCharge_Series_ChargeNumber] ON [dbo].[tblRecurringCharge] ([rc_Series], [rc_ChargeNumber]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRecurringCharge] ADD CONSTRAINT [FK_tblRecurringCharge_tblCreditCard_rc_CreditCard] FOREIGN KEY ([rc_CreditCard]) REFERENCES [dbo].[tblCreditCard] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblRecurringCharge] ADD CONSTRAINT [FK_tblRecurringCharge_tblSystemCurrencies_rc_Currency] FOREIGN KEY ([rc_Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblRecurringCharge] ADD CONSTRAINT [FK_tblRecurringCharge_tblCheckDetails_rc_ECheck] FOREIGN KEY ([rc_ECheck]) REFERENCES [dbo].[tblCheckDetails] ([id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblRecurringCharge] ADD CONSTRAINT [FK_tblRecurringCharge_tblRecurringSeries_rc_Series] FOREIGN KEY ([rc_Series]) REFERENCES [dbo].[tblRecurringSeries] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblRecurringCharge] ADD CONSTRAINT [FK_RecurringCharge_TransPaymentMethod] FOREIGN KEY ([TransPaymentMethod_id]) REFERENCES [Trans].[TransPaymentMethod] ([TransPaymentMethod_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s recurring charge', 'SCHEMA', N'dbo', 'TABLE', N'tblRecurringCharge', NULL, NULL
GO
