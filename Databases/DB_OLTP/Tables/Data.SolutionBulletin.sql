CREATE TABLE [Data].[SolutionBulletin]
(
[SolutionBulletin_id] [int] NOT NULL IDENTITY(1, 1),
[SolutionList_id] [tinyint] NULL,
[MessageText] [nvarchar] (2000) COLLATE Hebrew_CI_AS NOT NULL,
[MessageDate] [datetime2] (2) NOT NULL,
[MessageExpirationDate] [datetime2] (2) NULL,
[InsertUserName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[BulletinType] [varchar] (10) COLLATE Hebrew_CI_AS NOT NULL,
[IsVisible] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[SolutionBulletin] ADD CONSTRAINT [PK_SolutionBulletin] PRIMARY KEY CLUSTERED  ([SolutionBulletin_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[SolutionBulletin] ADD CONSTRAINT [FK_SolutionBulletin_SolutionListID] FOREIGN KEY ([SolutionList_id]) REFERENCES [List].[SolutionList] ([SolutionList_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Solution''s bulletin text', 'SCHEMA', N'Data', 'TABLE', N'SolutionBulletin', NULL, NULL
GO
