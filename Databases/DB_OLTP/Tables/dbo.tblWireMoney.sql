CREATE TABLE [dbo].[tblWireMoney]
(
[WireMoney_id] [int] NOT NULL IDENTITY(1, 1),
[Company_id] [int] NOT NULL CONSTRAINT [DF_tblWireMoney_Company_id] DEFAULT ((0)),
[WireSourceTbl_id] [int] NOT NULL CONSTRAINT [DF_tblWireMoney_WireSourceTbl_id] DEFAULT ((0)),
[wireInsertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblWireMoney_wireInsertDate] DEFAULT (getdate()),
[WireType] [tinyint] NOT NULL CONSTRAINT [DF_tblWireMoney_WireType] DEFAULT ((1)),
[WireDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblWireMoney_WireDate] DEFAULT (getdate()),
[WireAmount] [money] NOT NULL CONSTRAINT [DF_tblWireMoney_WireAmount] DEFAULT ((0)),
[WireCurrency] [int] NOT NULL CONSTRAINT [DF_tblWireMoney_WireCurrency] DEFAULT ((0)),
[WireExchangeRate] [float] NOT NULL CONSTRAINT [DF_tblWireMoney_WireExchangeRate] DEFAULT ((0)),
[WireStatusDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblWireMoney_WireStatusDate] DEFAULT (''),
[WireStatusUser] [nvarchar] (150) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WireStatusUser] DEFAULT (''),
[WireStatus] [tinyint] NOT NULL CONSTRAINT [DF_tblWireMoney_WireStatus] DEFAULT ((0)),
[WireComment] [nvarchar] (1000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WireComment] DEFAULT (''),
[WireFlag] [tinyint] NOT NULL CONSTRAINT [DF_tblWireMoney_WireFlag] DEFAULT ((0)),
[WireConfirmationNum] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WireConfirmationNum] DEFAULT (''),
[wirePrintApprovalStatusDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblWireMoney_wirePrintApprovalStatusDate] DEFAULT (''),
[wirePrintApprovalStatusUser] [nvarchar] (150) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePrintApprovalStatusUser] DEFAULT (''),
[wirePrintApprovalStatus] [bit] NOT NULL CONSTRAINT [DF_tblWireMoney_wirePrintApprovalStatus] DEFAULT ((0)),
[wireCompanyName] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wireCompanyName] DEFAULT (''),
[wireCompanyLegalName] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wireCompanyLegalName] DEFAULT (''),
[wireIDnumber] [nvarchar] (15) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wireIDnumber] DEFAULT (''),
[wireCompanyLegalNumber] [nvarchar] (15) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wireCompanyLegalNumber] DEFAULT (''),
[wirePaymentMethod] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentMethod] DEFAULT (''),
[wirePaymentPayeeName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentPayeeName] DEFAULT (''),
[wirePaymentBank] [int] NULL CONSTRAINT [DF_tblWireMoney_wirePaymentBank] DEFAULT ((0)),
[wirePaymentBranch] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentBranch] DEFAULT (''),
[wirePaymentAccount] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAccount] DEFAULT (''),
[wirePaymentAbroadAccountName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadAccountName] DEFAULT (''),
[wirePaymentAbroadAccountNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadAccountNumber] DEFAULT (''),
[wirePaymentAbroadBankName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadBankName] DEFAULT (''),
[wirePaymentAbroadBankAddress] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadBankAddress] DEFAULT (''),
[wirePaymentAbroadSwiftNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadSwiftNumber] DEFAULT (''),
[wirePaymentAbroadIBAN] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadIBAN] DEFAULT (''),
[wirePaymentAbroadAccountName2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadAccountName2] DEFAULT (''),
[wirePaymentAbroadAccountNumber2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadAccountNumber2] DEFAULT (''),
[wirePaymentAbroadBankName2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadBankName2] DEFAULT (''),
[wirePaymentAbroadBankAddress2] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadBankAddress2] DEFAULT (''),
[wirePaymentAbroadSwiftNumber2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadSwiftNumber2] DEFAULT (''),
[wirePaymentAbroadIBAN2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadIBAN2] DEFAULT (''),
[wirePaymentAbroadABA2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadABA2] DEFAULT (''),
[wirePaymentAbroadSortCode2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadSortCode2] DEFAULT (''),
[isShow] [bit] NOT NULL CONSTRAINT [DF_tblWireMoney_isShow] DEFAULT ((1)),
[wirePaymentAbroadABA] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadABA] DEFAULT (''),
[wirePaymentAbroadSortCode] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadSortCode] DEFAULT (''),
[WirePaymentAbroadBankAddressSecond] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WirePaymentAbroadBankAddressSecond] DEFAULT (''),
[WirePaymentAbroadBankAddressCity] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WirePaymentAbroadBankAddressCity] DEFAULT (''),
[WirePaymentAbroadBankAddressState] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WirePaymentAbroadBankAddressState] DEFAULT (''),
[WirePaymentAbroadBankAddressZip] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WirePaymentAbroadBankAddressZip] DEFAULT (''),
[WirePaymentAbroadBankAddressCountry] [int] NOT NULL CONSTRAINT [DF_tblWireMoney_WirePaymentAbroadBankAddressCountry] DEFAULT ((0)),
[WirePaymentAbroadBankAddressSecond2] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WirePaymentAbroadBankAddressSecond2] DEFAULT (''),
[WirePaymentAbroadBankAddressCity2] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WirePaymentAbroadBankAddressCity2] DEFAULT (''),
[WirePaymentAbroadBankAddressState2] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WirePaymentAbroadBankAddressState2] DEFAULT (''),
[WirePaymentAbroadBankAddressZip2] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_WirePaymentAbroadBankAddressZip2] DEFAULT (''),
[WirePaymentAbroadBankAddressCountry2] [int] NOT NULL CONSTRAINT [DF_tblWireMoney_WirePaymentAbroadBankAddressCountry2] DEFAULT ((0)),
[LastLogDate] [datetime] NULL,
[WireProcessingCurrency] [int] NOT NULL CONSTRAINT [DF_tblWireMoney_WireProcessingCurrency] DEFAULT ((0)),
[wireFee] [money] NOT NULL CONSTRAINT [DF_tblWireMoney_WireFee] DEFAULT ((0)),
[wirePaymentAbroadSepaBic] [varchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadSepaBic] DEFAULT (''),
[wirePaymentAbroadSepaBic2] [varchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoney_wirePaymentAbroadSepaBic2] DEFAULT (''),
[SettlementID] AS (case [WireType] when (1) then [WireSourceTbl_id]  end) PERSISTED,
[PaymentOrderID] AS (case [WireType] when (2) then [WireSourceTbl_id]  end) PERSISTED,
[AffiliateID] [int] NULL,
[AffiliatePaymentsID] AS (case [WireType] when (3) then [WireSourceTbl_id]  end) PERSISTED,
[wireApproveLevel1] [bit] NULL,
[wireApproveLevel2] [bit] NULL,
[ConfirmationFileName] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[AccountBankAccount_id] [int] NULL,
[AccountPayee_id] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgWireMoneyAdd] ON [dbo].[tblWireMoney] AFTER INSERT AS
BEGIN
	UPDATE
		tblWireMoney
	SET
		WireProcessingCurrency=dbo.GetMerchantPayoutCurrency(Company_id, WireCurrency)
	FROM
		tblWireMoney
		INNER JOIN tblCompany ON tblWireMoney.Company_ID=tblCompany.ID
	WHERE
		IsNull(Company_ID, 0)>0 AND tblWireMoney.WireMoney_ID IN (SELECT WireMoney_ID FROM Inserted);
	UPDATE
		tblWireMoney
	SET
		WireProcessingCurrency=dbo.GetAffiliatePayoutCurrency(AffiliateID, WireCurrency)
	FROM
		tblWireMoney
		INNER JOIN tblAffiliates ON tblWireMoney.AffiliateID=tblAffiliates.affiliates_id
	WHERE
		IsNull(AffiliateID, 0)>0 AND tblWireMoney.WireMoney_ID IN (SELECT WireMoney_ID FROM Inserted);
END
GO
ALTER TABLE [dbo].[tblWireMoney] ADD CONSTRAINT [PK_tblWireMoney] PRIMARY KEY CLUSTERED  ([WireMoney_id] DESC) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblWireMoney_Company_id] ON [dbo].[tblWireMoney] ([Company_id] DESC) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [wireDate] ON [dbo].[tblWireMoney] ([WireDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionPayWire_TransPayDateID] ON [dbo].[tblWireMoney] ([WireSourceTbl_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblWireMoney_WireType] ON [dbo].[tblWireMoney] ([WireType]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblWireMoney] ADD CONSTRAINT [FK_tblWireMoney_tblAffiliates_AffiliateID] FOREIGN KEY ([AffiliateID]) REFERENCES [dbo].[tblAffiliates] ([affiliates_id])
GO
ALTER TABLE [dbo].[tblWireMoney] ADD CONSTRAINT [FK_tblWireMoney_tblAffiliatePayments_AffiliatePaymentsID] FOREIGN KEY ([AffiliatePaymentsID]) REFERENCES [dbo].[tblAffiliatePayments] ([AFP_ID])
GO
ALTER TABLE [dbo].[tblWireMoney] ADD CONSTRAINT [FK_tblWireMoney_tblCompanyMakePaymentsRequests_PaymentOrderID] FOREIGN KEY ([PaymentOrderID]) REFERENCES [dbo].[tblCompanyMakePaymentsRequests] ([CompanyMakePaymentsRequests_id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblWireMoney] ADD CONSTRAINT [FK_tblWireMoney_tblTransactionPay_SettlementID] FOREIGN KEY ([SettlementID]) REFERENCES [dbo].[tblTransactionPay] ([id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Wires for transfering money to merchants/affiliates', 'SCHEMA', N'dbo', 'TABLE', N'tblWireMoney', NULL, NULL
GO
