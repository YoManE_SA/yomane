CREATE TABLE [dbo].[tblSecurityGroup]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[sg_Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSecurityGroup_sg_Name] DEFAULT (''),
[sg_Description] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSecurityGroup_sg_Description] DEFAULT (''),
[sg_IsActive] [bit] NOT NULL CONSTRAINT [DF_tblSecurityGroup_sg_IsActive] DEFAULT ((1)),
[sg_SeeUnmanaged] [bit] NOT NULL CONSTRAINT [DF_tblSecurityGroup_sg_SeeUnmanaged] DEFAULT ((0)),
[sg_IsLogged] [bit] NOT NULL CONSTRAINT [DF_tblSecurityGroup_sg_IsLogged] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgSecurityGroupAdd] ON [dbo].[tblSecurityGroup] FOR INSERT AS
BEGIN
	INSERT INTO
		tblSecurityDocumentGroup (sdg_Document, sdg_Group)
		SELECT tblSecurityDocument.ID, Inserted.ID FROM Inserted, tblSecurityDocument
	INSERT INTO
		tblSecurityUserGroup (sug_User, sug_Group)
		SELECT tblSecurityUser.ID, Inserted.ID FROM Inserted, tblSecurityUser
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgSecurityGroupRemove] ON [dbo].[tblSecurityGroup] FOR DELETE AS
BEGIN
	DELETE FROM tblSecurityDocumentGroup WHERE sdg_Group IN (SELECT ID FROM Deleted)
	DELETE FROM tblSecurityUserGroup WHERE sug_Group IN (SELECT ID FROM Deleted)
END
GO
ALTER TABLE [dbo].[tblSecurityGroup] ADD CONSTRAINT [PK_tblSecurityGroup] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds Admincash security groups (Replace by Admin new security method)', 'SCHEMA', N'dbo', 'TABLE', N'tblSecurityGroup', NULL, NULL
GO
