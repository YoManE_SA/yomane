CREATE TABLE [dbo].[tblMerchantCustomization]
(
[MerchantID] [int] NOT NULL,
[SkinID] [int] NOT NULL,
[LogoTopLeftAlign] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[LogoTopLeftWidth] [int] NULL,
[LogoTopLeftHeight] [int] NULL,
[LogoTopRightAlign] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[LogoTopRightWidth] [int] NULL,
[LogoTopRightHeight] [int] NULL,
[LogoBottomLeftAlign] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[LogoBottomLeftWidth] [int] NULL,
[LogoBottomLeftHeight] [int] NULL,
[LogoBottomRightAlign] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[LogoBottomRightWidth] [int] NULL,
[LogoBottomRightHeight] [int] NULL,
[BackImageRepeat] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[BackColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[ForeColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FontFamily] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FontSize] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FontIsBold] [bit] NULL,
[FontIsItalic] [bit] NULL,
[TitleBackColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[TitleForeColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[TitleFontFamily] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[TitleFontSize] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[TitleFontIsBold] [bit] NULL,
[TitleFontIsItalic] [bit] NULL,
[FieldBackColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FieldForeColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FieldFontFamily] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FieldFontSize] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FieldFontIsBold] [bit] NULL,
[FieldFontIsItalic] [bit] NULL,
[ButtonBackColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[ButtonForeColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[ButtonFontFamily] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[ButtonFontSize] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[ButtonFontIsBold] [bit] NULL,
[ButtonFontIsItalic] [bit] NULL,
[PageBackColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[GroupTitleBackColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[GroupTitleForeColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[GroupTitleFontFamily] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[GroupTitleFontSize] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[GroupTitleFontIsBold] [bit] NULL,
[GroupTitleFontIsItalic] [bit] NULL,
[FormTitleBackColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FormTitleForeColor] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FormTitleFontFamily] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FormTitleFontSize] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[FormTitleFontIsBold] [bit] NULL,
[FormTitleFontIsItalic] [bit] NULL,
[LogoMainAlign] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[LogoMainWidth] [int] NULL,
[LogoMainHeight] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMerchantCustomization] ADD CONSTRAINT [PK_tblMerchantCustomization] PRIMARY KEY CLUSTERED  ([MerchantID], [SkinID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMerchantCustomization] ADD CONSTRAINT [FK_tblMerchantCustomization_tblCompany_MerchantID] FOREIGN KEY ([MerchantID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s hosted page settings', 'SCHEMA', N'dbo', 'TABLE', N'tblMerchantCustomization', NULL, NULL
GO
