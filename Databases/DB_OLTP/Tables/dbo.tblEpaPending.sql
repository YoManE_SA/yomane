CREATE TABLE [dbo].[tblEpaPending]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblEpaPending_InsertDate] DEFAULT (getdate()),
[StoredFileName] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[DebitRefCode] [nvarchar] (40) COLLATE Hebrew_CI_AS NULL,
[ApprovalNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Amount] [money] NULL,
[Currency] [int] NULL,
[TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[CardNumber256] [varbinary] (200) NULL,
[Installment] [int] NOT NULL CONSTRAINT [DF_tblEpaPending_Installment] DEFAULT ((1)),
[IsRefund] [bit] NULL,
[TransDate] [datetime] NULL,
[PayoutDate] [datetime] NULL,
[TransCount] [int] NULL,
[TransID] [int] NULL,
[AcquirerReferenceNum] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[DebitReferenceNum] [varchar] (40) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEpaPending] ADD CONSTRAINT [PK_tblEpaPending] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'EPA recived and waiting for transaction match', 'SCHEMA', N'dbo', 'TABLE', N'tblEpaPending', NULL, NULL
GO
