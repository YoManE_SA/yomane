CREATE TABLE [Data].[ProductStockReference]
(
[ProductStock_id] [int] NOT NULL,
[Product_id] [int] NOT NULL,
[ProductProperty_id] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductStockReference] ADD CONSTRAINT [PK_ProductStockReference] PRIMARY KEY CLUSTERED  ([ProductStock_id], [Product_id], [ProductProperty_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductStockReference] ADD CONSTRAINT [FK_ProductStockReference_Product] FOREIGN KEY ([Product_id]) REFERENCES [Data].[Product] ([Product_id]) ON DELETE CASCADE
GO
ALTER TABLE [Data].[ProductStockReference] ADD CONSTRAINT [FK_ProductStockReference_ProductProperty] FOREIGN KEY ([ProductProperty_id]) REFERENCES [Data].[ProductProperty] ([ProductProperty_id])
GO
ALTER TABLE [Data].[ProductStockReference] ADD CONSTRAINT [FK_ProductStockReference_ProductStock] FOREIGN KEY ([ProductStock_id]) REFERENCES [Data].[ProductStock] ([ProductStock_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s product''s stock to property', 'SCHEMA', N'Data', 'TABLE', N'ProductStockReference', NULL, NULL
GO
