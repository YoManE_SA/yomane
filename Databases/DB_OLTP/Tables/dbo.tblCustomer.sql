CREATE TABLE [dbo].[tblCustomer]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[FirstName] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_FirstName] DEFAULT (''),
[LastName] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_LastName] DEFAULT (''),
[Street] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_Street] DEFAULT (''),
[City] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_City] DEFAULT (''),
[ZIP] [nvarchar] (25) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_ZIP] DEFAULT (''),
[Country] [int] NOT NULL CONSTRAINT [DF_tblCustomer_Country] DEFAULT ((0)),
[Phone] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_Phone] DEFAULT (''),
[CellPhone] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_CellPhone] DEFAULT (''),
[UserName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_UserName] DEFAULT (''),
[Mail] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_Mail] DEFAULT (''),
[Question1] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_Question1] DEFAULT (''),
[Answer1] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_Answer1] DEFAULT (''),
[Question2] [int] NOT NULL CONSTRAINT [DF_tblCustomer_Question2] DEFAULT ((0)),
[Answer2] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_Answer2] DEFAULT (''),
[Comment] [nvarchar] (1000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_comment] DEFAULT (''),
[CustomerFrom] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_CustomerFrom] DEFAULT (''),
[IsRead] [bit] NOT NULL CONSTRAINT [DF_tblCustomer_IsRead] DEFAULT ((0)),
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCustomer_InsertDate] DEFAULT (getdate()),
[Blocked] [bit] NOT NULL CONSTRAINT [DF_tblCustomer_Blocked] DEFAULT ((0)),
[Closed] [bit] NOT NULL CONSTRAINT [DF_tblCustomer_Closed] DEFAULT ((0)),
[IpOnReg] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_IpOnReg] DEFAULT ('0.0.0.0'),
[CustomerNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_CustomerNumber] DEFAULT (''),
[Cash] [money] NOT NULL CONSTRAINT [DF_tblCustomer_Cash] DEFAULT ((0)),
[LastInsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCustomer_LastInsertDate] DEFAULT (getdate()),
[IDNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_IDNumber] DEFAULT (''),
[PaymentAccount] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_PaymentAccount] DEFAULT (''),
[PaymentBranch] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_PaymentBranch] DEFAULT (''),
[PaymentBank] [int] NOT NULL CONSTRAINT [DF_tblCustomer_PaymentBank] DEFAULT ((0)),
[PaymentPayeeName] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_PaymentPayeeName] DEFAULT (''),
[PaymentIDNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_PaymentIDNumber] DEFAULT (''),
[fromCompanyID] [smallint] NOT NULL CONSTRAINT [DF_tblCustomer_fromCompanyID] DEFAULT ((0)),
[LastLoginCount] [int] NOT NULL CONSTRAINT [DF_tblCustomer_LastLoginCount] DEFAULT ((0)),
[State] [int] NOT NULL CONSTRAINT [DF_tblCustomer_State] DEFAULT ((0)),
[RefCompanyMemberID] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_RefCompanyMemberID] DEFAULT (''),
[IsVerified] [bit] NOT NULL CONSTRAINT [DF_tblCustomer_IsVerified] DEFAULT ((0)),
[RefCompanyID] [int] NOT NULL CONSTRAINT [DF_tblCustomer_RefCompanyID] DEFAULT ((0)),
[WalletIdentityID] [int] NOT NULL CONSTRAINT [DF_tblCustomer_WalletIdentityID] DEFAULT ((0)),
[RefCompanyData] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_RefCompanyData] DEFAULT (''),
[ECA] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_ECA] DEFAULT (''),
[ECS] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomer_ECS] DEFAULT (''),
[DOB] [date] NULL,
[PinCode] [char] (4) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgCustomerAddSetCustomerNumber] ON [dbo].[tblCustomer] FOR INSERT AS
BEGIN
	DECLARE @nID int, @sNumber nvarchar(50)
	DECLARE curCustomer CURSOR FOR SELECT ID FROM Inserted WHERE CustomerNumber=''
	OPEN curCustomer
	FETCH NEXT FROM curCustomer INTO @nID
	WHILE @@FETCH_STATUS=0
	BEGIN
		EXEC GetNewCustomerNumber @sNumber OUTPUT
		UPDATE tblCustomer SET CustomerNumber=@sNumber WHERE ID=@nID
		FETCH NEXT FROM curCustomer INTO @nID
	END
	CLOSE curCustomer
	DEALLOCATE curCustomer
END
GO
ALTER TABLE [dbo].[tblCustomer] ADD CONSTRAINT [PK_tblCustomer] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[Not Used]', 'SCHEMA', N'dbo', 'TABLE', N'tblCustomer', NULL, NULL
GO
