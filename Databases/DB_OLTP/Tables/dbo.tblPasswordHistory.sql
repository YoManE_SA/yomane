CREATE TABLE [dbo].[tblPasswordHistory]
(
[LPH_ID] [int] NOT NULL,
[LPH_RefID] [int] NOT NULL,
[LPH_RefType] [int] NOT NULL,
[LPH_Insert] [datetime] NOT NULL CONSTRAINT [DF_tblPasswordHistory_LPH_Insert] DEFAULT (getdate()),
[LPH_IP] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblPasswordHistory_LPH_IP] DEFAULT (''),
[LPH_FailCount] [int] NOT NULL CONSTRAINT [DF_tblPasswordHistory_LPH_FailCount] DEFAULT ((0)),
[LPH_LastFail] [datetime] NOT NULL CONSTRAINT [DF_tblPasswordHistory_LPH_LastFail] DEFAULT (getdate()),
[LPH_Password256] [varbinary] (200) NULL,
[LPH_LastSuccess] [datetime] NOT NULL CONSTRAINT [DF_tblPasswordHistory_LPH_LastSuccess] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPasswordHistory] ADD CONSTRAINT [PK_tblPasswordHistory] PRIMARY KEY CLUSTERED  ([LPH_ID], [LPH_RefID], [LPH_RefType]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved?] Replaced by Data.LoginPassword', 'SCHEMA', N'dbo', 'TABLE', N'tblPasswordHistory', NULL, NULL
GO
