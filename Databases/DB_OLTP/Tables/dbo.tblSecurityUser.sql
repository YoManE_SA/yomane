CREATE TABLE [dbo].[tblSecurityUser]
(
[ID] [int] NOT NULL,
[su_Username] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSecurityUser_su_Username] DEFAULT (''),
[su_Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSecurityUser_su_Name] DEFAULT (''),
[su_IsAdmin] [bit] NOT NULL CONSTRAINT [DF_tblSecurityUser_su_IsAdmin] DEFAULT ((0)),
[su_IsActive] [bit] NOT NULL CONSTRAINT [DF_tblSecurityUser_su_IsActive] DEFAULT ((1)),
[su_Mail] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSecurityUser_su_Mail] DEFAULT (''),
[su_SMS] [varchar] (10) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSecurityUser_su_SMS] DEFAULT (''),
[su_MailEmergency] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSecurityUser_su_MailEmergency] DEFAULT (''),
[su_IsLogged] [bit] NOT NULL CONSTRAINT [DF_tblSecurityUser_su_IsLogged] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgSecurityUserAdd] ON [dbo].[tblSecurityUser] FOR INSERT AS
BEGIN
	INSERT INTO
		tblSecurityUserGroup (sug_User, sug_Group)
		SELECT Inserted.ID, tblSecurityGroup.ID FROM Inserted, tblSecurityGroup
	INSERT INTO
		tblSecurityDocumentGroup (sdg_Document,sdg_Group)
		SELECT tblSecurityDocument.ID, -Inserted.ID FROM Inserted,tblSecurityDocument
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Trigger [dbo].[trgSecurityUserDelete] on [dbo].[tblSecurityUser] For Delete as 
Begin
	Delete From tblPasswordHistory Where LPH_RefID IN(Select ID From Deleted) And LPH_RefType IN(5, 6)
End
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgSecurityUserRemove] ON [dbo].[tblSecurityUser] FOR DELETE AS
BEGIN
	DELETE FROM tblSecurityUserGroup WHERE sug_User IN (SELECT ID FROM Deleted)
	DELETE FROM tblSecurityDocumentGroup WHERE -sdg_Group IN (SELECT ID FROM Deleted)
END
GO
ALTER TABLE [dbo].[tblSecurityUser] ADD CONSTRAINT [PK_tblSecurityUser] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblSecurityUser_Username] ON [dbo].[tblSecurityUser] ([su_Username]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds Admincash security users (Replace by Admin new security method)', 'SCHEMA', N'dbo', 'TABLE', N'tblSecurityUser', NULL, NULL
GO
