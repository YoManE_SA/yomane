CREATE TABLE [Finance].[InvoiceIssuer]
(
[InvoiceIssuer_id] [tinyint] NOT NULL IDENTITY(1, 1),
[InvoiceProvider_id] [tinyint] NOT NULL,
[IsDefault] [bit] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Address] [nvarchar] (300) COLLATE Hebrew_CI_AS NULL,
[Email] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Number] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[DisplayLanguageISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[InvoiceCurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[VATPercent] [decimal] (5, 2) NULL,
[InvoiceProviderUseName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[InvoiceProviderUserID] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[InvoiceProviderPassword] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Finance].[InvoiceIssuer] ADD CONSTRAINT [PK_InvoiceIssuer] PRIMARY KEY CLUSTERED  ([InvoiceIssuer_id]) ON [PRIMARY]
GO
ALTER TABLE [Finance].[InvoiceIssuer] ADD CONSTRAINT [FK_InvoiceIssuer_InvoiceCurrencyISOCode] FOREIGN KEY ([InvoiceCurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Finance].[InvoiceIssuer] ADD CONSTRAINT [FK_InvoiceIssuer_InvoiceProviderID] FOREIGN KEY ([InvoiceProvider_id]) REFERENCES [List].[InvoiceProvider] ([InvoiceProvider_id])
GO
