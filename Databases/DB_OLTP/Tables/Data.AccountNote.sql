CREATE TABLE [Data].[AccountNote]
(
[AccountNote_id] [int] NOT NULL IDENTITY(1, 1),
[Account_id] [int] NULL,
[InsertDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_AccountNote_InsertDate] DEFAULT (sysdatetime()),
[InsertUser] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[NoteText] [nvarchar] (1000) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountNote] ADD CONSTRAINT [PK_AccountNote] PRIMARY KEY CLUSTERED  ([AccountNote_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountNote] ADD CONSTRAINT [FK_AccountNote_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account Notes/Comments', 'SCHEMA', N'Data', 'TABLE', N'AccountNote', NULL, NULL
GO
