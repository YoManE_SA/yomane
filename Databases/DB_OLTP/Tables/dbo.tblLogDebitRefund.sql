CREATE TABLE [dbo].[tblLogDebitRefund]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ldr_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblLogDebitRefund_ldr_InsertDate] DEFAULT (getdate()),
[ldr_TransFail] [int] NOT NULL CONSTRAINT [DF_tblLogDebitRefund_ldr_TransFail] DEFAULT ((0)),
[ldr_ReplyCode] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLogDebitRefund_ldr_ReplyCode] DEFAULT (''),
[ldr_LogNoConnection] [int] NOT NULL CONSTRAINT [DF_tblLogDebitRefund_ldr_LogNoConnection] DEFAULT ((0)),
[ldr_Answer] [nvarchar] (1000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLogDebitRefund_ldr_Answer] DEFAULT ('')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgLogDebitRefund] ON [dbo].[tblLogDebitRefund] FOR INSERT
AS
BEGIN
	DECLARE @nTransFail int, @nDebitCompany int, @sReplyCode varchar(10), @bFailed bit;
	DECLARE @sDebitCompany varchar(50), @sDebitPhone varchar(50), @sDebitContact varchar(50);
	DECLARE @sTo varchar(200), @sSubject varchar(200), @sBody varchar(max);
	DECLARE curRefunds CURSOR FOR
		SELECT
			ldr_TransFail,
			lnc_DebitCompany,
			dc_Name,
			dc_EmergencyPhone,
			dc_EmergencyContact,
			dc_AutoRefundNotifyUsers,
			ldr_ReplyCode,
			CASE
				WHEN ldr_ReplyCode='000' THEN 0
				WHEN lnc_DebitCompany=18 AND ldr_ReplyCode='1316' THEN 0
				WHEN lnc_DebitCompany=19 AND ldr_ReplyCode='2' THEN 0
				WHEN lnc_DebitCompany=21 AND ldr_ReplyCode='12' THEN 0
				ELSE 1
			END
		FROM
			Inserted
			INNER JOIN tblLog_NoConnection ON ldr_TransFail=lnc_TransactionFailID
			INNER JOIN tblDebitCompany ON lnc_DebitCompany=tblDebitCompany.DebitCompany_ID
		WHERE
			ldr_TransFail IN (SELECT DISTINCT ldr_TransFail FROM tblLogDebitRefund WHERE ID<Inserted.ID);
	OPEN curRefunds
	FETCH NEXT FROM curRefunds INTO @nTransFail, @nDebitCompany, @sDebitCompany, @sDebitPhone, @sDebitContact, @sTo, @sReplyCode, @bFailed
	WHILE @@FETCH_STATUS=0
	BEGIN
		IF @sTo<>'' AND @bFailed=1
		BEGIN
			SET @sSubject=@sDebitCompany+' AutoRefund '+CASE @bFailed WHEN 1 THEN 'Failed' ELSE 'Succeeded' END+' At '+Cast(ServerProperty('MachineName') AS nvarchar(10))
			SET @sBody='<b>AutoRefund '+CASE @bFailed WHEN 1 THEN 'Failed' ELSE 'Succeeded' END+'</b><br />'
			SET @sBody=@sBody+'Server: '+Cast(ServerProperty('MachineName') AS nvarchar(10))+'<br />'
			SET @sBody=@sBody+'Sent on: '+Convert(nvarchar(25), GetDate(), 120)+'<br />'
			SET @sBody=@sBody+'Debit company: '+Cast(@nDebitCompany as nvarchar(10))+' '+@sDebitCompany+'<br />'
			SET @sBody=@sBody+'Initial Failed Transaction : '+Cast(@nTransFail as nvarchar(10))+'<br />'
			SET @sBody=@sBody+'Reply code: '+@sReplyCode+'<br />'
			SET @sBody=@sBody+'Emergency phone: '+@sDebitPhone+' ('+@sDebitContact+')<br />'
			EXEC SendMail @sTo, @sSubject, @sBody
		END
		FETCH NEXT FROM curRefunds INTO @nTransFail, @nDebitCompany, @sDebitCompany, @sDebitPhone, @sDebitContact, @sTo, @sReplyCode, @bFailed
	END
	CLOSE curRefunds
	DEALLOCATE curRefunds
END
GO
ALTER TABLE [dbo].[tblLogDebitRefund] ADD CONSTRAINT [PK_tblLogDebitRefund] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log for refund attempts after getting no connection from debit company', 'SCHEMA', N'dbo', 'TABLE', N'tblLogDebitRefund', NULL, NULL
GO
