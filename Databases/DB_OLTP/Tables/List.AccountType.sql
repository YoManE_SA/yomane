CREATE TABLE [List].[AccountType]
(
[AccountType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[AccountType] ADD CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED  ([AccountType_id]) ON [PRIMARY]
GO
