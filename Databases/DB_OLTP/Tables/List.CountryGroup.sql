CREATE TABLE [List].[CountryGroup]
(
[CountryGroup_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[CountryGroup] ADD CONSTRAINT [PK_CountryGroup] PRIMARY KEY CLUSTERED  ([CountryGroup_id]) ON [PRIMARY]
GO
