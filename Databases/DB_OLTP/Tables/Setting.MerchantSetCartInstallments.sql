CREATE TABLE [Setting].[MerchantSetCartInstallments]
(
[MerchantSetCartInstallments] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[Amount] [decimal] (9, 0) NOT NULL,
[MaxInstallments] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[MerchantSetCartInstallments] ADD CONSTRAINT [PK_MerchantSetCartInstallments] PRIMARY KEY CLUSTERED  ([MerchantSetCartInstallments]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[MerchantSetCartInstallments] ADD CONSTRAINT [FK_MerchantSetCartInstallments_tblCompany_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
