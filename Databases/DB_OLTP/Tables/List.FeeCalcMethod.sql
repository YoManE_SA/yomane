CREATE TABLE [List].[FeeCalcMethod]
(
[FeeCalcMethod_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[FeeCalcMethod] ADD CONSTRAINT [PK_FeeCalcMethod] PRIMARY KEY CLUSTERED  ([FeeCalcMethod_id]) ON [PRIMARY]
GO
