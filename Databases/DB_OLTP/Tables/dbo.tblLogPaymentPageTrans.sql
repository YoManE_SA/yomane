CREATE TABLE [dbo].[tblLogPaymentPageTrans]
(
[LogPaymentPageTrans_id] [int] NOT NULL IDENTITY(1, 1),
[Lppt_Date] [date] NOT NULL CONSTRAINT [DF_tblLogPaymentPageTrans_Lppt_Date] DEFAULT (getdate()),
[Lppt_LogPaymentPage_id] [int] NOT NULL,
[Lppt_PaymentMethodType] [smallint] NULL,
[Lppt_RequestString] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lppt_ResponseString] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Lppt_ReplyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Lppt_ReplyDesc] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[Lppt_TransNum] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogPaymentPageTrans] ADD CONSTRAINT [PK_tblLogPaymentPageTrans] PRIMARY KEY CLUSTERED  ([LogPaymentPageTrans_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogPaymentPageTrans] ADD CONSTRAINT [FK_tblLogPaymentPageTrans_tblLogPaymentPage_Lppt_LogPaymentPage_id] FOREIGN KEY ([Lppt_LogPaymentPage_id]) REFERENCES [dbo].[tblLogPaymentPage] ([LogPaymentPage_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log Hosted Payment Page processing attempts', 'SCHEMA', N'dbo', 'TABLE', N'tblLogPaymentPageTrans', NULL, NULL
GO
