CREATE TABLE [Setting].[MerchantSetShop]
(
[MerchantSetShop_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[SubDomainName] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[UIBaseColor] [varchar] (7) COLLATE Hebrew_CI_AS NULL,
[LogoFileName] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[BannerFileName] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[BannerLinkUrl] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Facebook] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Twitter] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Vimeo] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Youtube] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Linkedin] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_Pinterest] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[URL_GooglePlus] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[StreetAddress] [nvarchar] (300) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[MerchantSetShop] ADD CONSTRAINT [UIX_MerchantSetShop_SubDomainName] UNIQUE NONCLUSTERED  ([SubDomainName]) ON [PRIMARY]

GO
ALTER TABLE [Setting].[MerchantSetShop] ADD CONSTRAINT [PK_MerchantSetShop] PRIMARY KEY CLUSTERED  ([MerchantSetShop_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[MerchantSetShop] ADD CONSTRAINT [FK_MerchantSetShop_CurrencyList_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode]) ON DELETE CASCADE
GO
ALTER TABLE [Setting].[MerchantSetShop] ADD CONSTRAINT [FK_MerchantSetShop_tblCompany_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
