CREATE TABLE [System].[AppModuleSetting]
(
[AppModuleSetting_id] [int] NOT NULL IDENTITY(1, 1),
[AppModule_id] [int] NOT NULL,
[ValueName] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL,
[Value] [varchar] (250) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[AppModuleSetting] ADD CONSTRAINT [PK_AppModuleSetting] PRIMARY KEY CLUSTERED  ([AppModuleSetting_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[AppModuleSetting] ADD CONSTRAINT [UIX_AppModuleSetting_AppModuleID_ValueName] UNIQUE NONCLUSTERED  ([AppModule_id], [ValueName]) ON [PRIMARY]
GO
ALTER TABLE [System].[AppModuleSetting] ADD CONSTRAINT [FK_AppModuleSetting_AppModule_AppModuleID] FOREIGN KEY ([AppModule_id]) REFERENCES [System].[AppModule] ([AppModule_id])
GO
