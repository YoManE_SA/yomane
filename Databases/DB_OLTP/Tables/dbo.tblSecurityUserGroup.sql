CREATE TABLE [dbo].[tblSecurityUserGroup]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[sug_User] [int] NOT NULL CONSTRAINT [DF_tblSecurityUserGroup_sug_User] DEFAULT ((0)),
[sug_Group] [int] NOT NULL CONSTRAINT [DF_tblSecurityUserGroup_sug_Group] DEFAULT ((0)),
[sug_IsMember] [bit] NOT NULL CONSTRAINT [DF_tblSecurityUserGroup_sug_IsMember] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSecurityUserGroup] ADD CONSTRAINT [PK_tblSecurityUserGroup] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSecurityUserGroup] ADD CONSTRAINT [FK_tblSecurityUserGroup_tblSecurityGroup_sug_Group] FOREIGN KEY ([sug_Group]) REFERENCES [dbo].[tblSecurityGroup] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblSecurityUserGroup] ADD CONSTRAINT [FK_tblSecurityUserGroup_tblSecurityUser_sug_User] FOREIGN KEY ([sug_User]) REFERENCES [dbo].[tblSecurityUser] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds Admincash security user to groups (Replace by Admin new security method)', 'SCHEMA', N'dbo', 'TABLE', N'tblSecurityUserGroup', NULL, NULL
GO
