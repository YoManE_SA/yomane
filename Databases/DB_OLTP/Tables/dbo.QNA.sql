CREATE TABLE [dbo].[QNA]
(
[QNA_id] [int] NOT NULL IDENTITY(1, 1),
[QNAGroup_id] [int] NULL,
[Question] [nvarchar] (1000) COLLATE Hebrew_CI_AS NULL,
[Answer] [nvarchar] (4000) COLLATE Hebrew_CI_AS NULL,
[Rating] [int] NOT NULL CONSTRAINT [DF_QNA_Rating] DEFAULT ((0)),
[IsVisible] [bit] NOT NULL CONSTRAINT [DF_QNA_IsVisible] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QNA] ADD CONSTRAINT [PK_QNA] PRIMARY KEY CLUSTERED  ([QNA_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QNA] ADD CONSTRAINT [FK_QNA_QNAGroup_QNAGroup_id] FOREIGN KEY ([QNAGroup_id]) REFERENCES [dbo].[QNAGroup] ([QNAGroup_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Questions and answers used in web apps', 'SCHEMA', N'dbo', 'TABLE', N'QNA', NULL, NULL
GO
