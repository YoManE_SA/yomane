CREATE TABLE [dbo].[tblWireMoneyLog]
(
[wireMoneyLog_id] [int] NOT NULL IDENTITY(1, 1),
[WireMoney_id] [int] NOT NULL CONSTRAINT [DF_tblWireMoneyLog_WireMoney_id] DEFAULT ((0)),
[wml_description] [nvarchar] (250) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoneyLog_wml_description] DEFAULT (''),
[wml_date] [smalldatetime] NOT NULL CONSTRAINT [DF_tblWireMoneyLog_wml_date] DEFAULT (getdate()),
[wml_user] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoneyLog_wml_user] DEFAULT ('')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgWireMoneyLogAddSetWireMoneyLastLogDate] ON [dbo].[tblWireMoneyLog] FOR INSERT AS
BEGIN
	UPDATE
		tblWireMoney
	SET
		LastLogDate=(SELECT max(wml_date) from tblWireMoneyLog where WireMoney_id=tblWireMoney.WireMoney_id)
	WHERE
		WireMoney_id IN (SELECT WireMoney_id FROM INSERTED)
END
GO
ALTER TABLE [dbo].[tblWireMoneyLog] ADD CONSTRAINT [PK_tblWireMoneyLog] PRIMARY KEY CLUSTERED  ([wireMoneyLog_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [wireMoneyId] ON [dbo].[tblWireMoneyLog] ([WireMoney_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log the changes to wire status', 'SCHEMA', N'dbo', 'TABLE', N'tblWireMoneyLog', NULL, NULL
GO
