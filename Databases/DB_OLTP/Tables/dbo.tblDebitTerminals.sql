CREATE TABLE [dbo].[tblDebitTerminals]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[DebitCompany] [tinyint] NOT NULL CONSTRAINT [DF_tblDebitTerminals_DebitCompany] DEFAULT ((1)),
[terminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_terminalNumber] DEFAULT (''),
[accountId] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_accountId] DEFAULT (''),
[accountSubId] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_accountSubId] DEFAULT (''),
[terminalNumber3D] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_terminalNumber3D] DEFAULT (''),
[accountId3D] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_accountId3D] DEFAULT (''),
[accountSubId3D] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_accountSubId3D] DEFAULT (''),
[isNetpayTerminal] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_isNetpayTerminal] DEFAULT ((0)),
[isShvaMasterTerminal] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_isShvaMasterTerminal] DEFAULT ((0)),
[processingMethod] [tinyint] NOT NULL CONSTRAINT [DF_tblDebitTerminals_processingMethod] DEFAULT ((1)),
[terminalMonthlyCost] [money] NOT NULL CONSTRAINT [DF_tblDebitTerminals_terminalMonthlyCost] DEFAULT ((0)),
[terminalNotes] [nvarchar] (3000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_terminalNotes] DEFAULT (''),
[dt_CompanyNum_visa] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_CompanyNum_visa] DEFAULT (''),
[dt_Comments_visa] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_Comments_visa] DEFAULT (''),
[dt_CompanyNum_isracard] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_CompanyNum_isracard] DEFAULT (''),
[dt_Comments_isracard] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_Comments_isracard] DEFAULT (''),
[dt_CompanyNum_direct] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_CompanyNum_direct] DEFAULT (''),
[dt_Comments_direct] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_Comments_direct] DEFAULT (''),
[dt_CompanyNum_mastercard] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_CompanyNum_mastercard] DEFAULT (''),
[dt_Comments_mastercard] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_Comments_mastercard] DEFAULT (''),
[dt_CompanyNum_diners] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_CompanyNum_diners] DEFAULT (''),
[dt_Comments_diners] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_Comments_diners] DEFAULT (''),
[dt_CompanyNum_americanexp] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_CompanyNum_americanexp] DEFAULT (''),
[dt_Comments_americanexp] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_Comments_americanexp] DEFAULT (''),
[isActive] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_isActive] DEFAULT ((1)),
[accountPassword256] [varbinary] (1500) NULL,
[accountPassword3D256] [varbinary] (1500) NULL,
[dt_mcc] [varchar] (15) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_mcc] DEFAULT (''),
[dt_name] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_name] DEFAULT (''),
[dt_isManipulateAmount] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_isManipulateAmount] DEFAULT ((0)),
[dt_IsUseBlackList] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_IsUseBlackList] DEFAULT ((0)),
[dt_Descriptor] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblDebitTerminals_dt_Descriptor] DEFAULT (''),
[dt_MonthlyLimitCHB] [int] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyLimitCHB] DEFAULT ((0)),
[dt_MonthlyCHB] [int] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyCHB] DEFAULT ((0)),
[dt_MonthlyCHBWasSent] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyCHBWasSent] DEFAULT ((0)),
[dt_MonthlyCHBSendDate] [datetime] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyCHBSendDate] DEFAULT (getdate()),
[dt_MonthlyMCLimitCHB] [int] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyMCLimitCHB] DEFAULT ((0)),
[dt_MonthlyMCCHB] [int] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyMCCHB] DEFAULT ((0)),
[dt_MonthlyMCCHBWasSent] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyMCCHBWasSent] DEFAULT ((0)),
[dt_MonthlyMCCHBSendDate] [datetime] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyMCCHBSendDate] DEFAULT (getdate()),
[dt_BankAccountID] [int] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_BankAccountID] DEFAULT ((0)),
[dt_IsRefundBlocked] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_IsRefundBlocked] DEFAULT ((0)),
[dt_MonthlyLimitCHBNotifyUsers] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyLimitCHBNotifyUsers] DEFAULT (''),
[dt_MonthlyLimitCHBNotifyUsersSMS] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyLimitCHBNotifyUsersSMS] DEFAULT (''),
[dt_MonthlyMCLimitCHBNotifyUsers] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyMCLimitCHBNotifyUsers] DEFAULT (''),
[dt_MonthlyMCLimitCHBNotifyUsersSMS] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_MonthlyMCLimitCHBNotifyUsersSMS] DEFAULT (''),
[dt_ContractNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_ContractNumber] DEFAULT (''),
[dt_ManagingCompany] [int] NULL,
[dt_ManagingPSP] [int] NULL,
[dt_EnableRecurringBank] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_EnableRecurringBank] DEFAULT ((0)),
[dt_EnableAuthorization] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_EnableAuthorization] DEFAULT ((0)),
[dt_Enable3dsecure] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_Enable3dsecure] DEFAULT ((0)),
[dt_mobileCat] [int] NULL,
[dt_SMSShortCode] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[dt_IsPersonalNumberRequired] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_IsPersonalNumberRequired] DEFAULT ((0)),
[dt_IsTestTerminal] [bit] NOT NULL CONSTRAINT [DF_tblDebitTerminals_dt_IsTestTerminal] DEFAULT ((0)),
[dt_BankExternalID] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[AuthenticationCode1] [varchar] (40) COLLATE Hebrew_CI_AS NULL,
[AuthenticationCode3D] [varchar] (40) COLLATE Hebrew_CI_AS NULL,
[SICCodeNumber] [smallint] NULL,
[SearchTag] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDebitTerminals] ADD CONSTRAINT [PK_tblDebitTerminals] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Company_Terminal] ON [dbo].[tblDebitTerminals] ([DebitCompany], [terminalNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_tblDebitTerminals_dt_BankAccountID] ON [dbo].[tblDebitTerminals] ([dt_BankAccountID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblTerminalsInfo_terminalNumber] ON [dbo].[tblDebitTerminals] ([terminalNumber]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDebitTerminals] ADD CONSTRAINT [FK_DebitTerminals_SICCodeNumber] FOREIGN KEY ([SICCodeNumber]) REFERENCES [List].[SICCode] ([SICCodeNumber]) ON DELETE SET NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Debit company terminals', 'SCHEMA', N'dbo', 'TABLE', N'tblDebitTerminals', NULL, NULL
GO
