CREATE TABLE [List].[WorldRegion]
(
[WorldRegionISOCode] [char] (2) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[WorldRegion] ADD CONSTRAINT [PK_WorldRegion] PRIMARY KEY CLUSTERED  ([WorldRegionISOCode]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
