CREATE TABLE [List].[TransCreditType]
(
[TransCreditType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[NameHeb] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[NameEng] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[IsShow] [bit] NULL,
[ShowOrder] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[TransCreditType] ADD CONSTRAINT [PK_TransCreditType] PRIMARY KEY CLUSTERED  ([TransCreditType_id]) ON [PRIMARY]
GO
