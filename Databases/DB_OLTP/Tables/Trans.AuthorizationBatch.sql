CREATE TABLE [Trans].[AuthorizationBatch]
(
[AuthorizationBatch_id] [int] NOT NULL IDENTITY(1, 1),
[BatchDate] [datetime2] (2) NOT NULL CONSTRAINT [DF_AuthorizationBatch_BatchDate] DEFAULT (sysdatetime()),
[BatchInternalKey] [int] NOT NULL,
[TransTerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[TransDebitCompanyID] [int] NULL,
[TransCount] [int] NULL,
[TransTotalDebit] [money] NULL,
[TransTotalCredit] [money] NULL,
[ResultDescription] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[BatchFileName] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[ActionStatus_id] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Trans].[AuthorizationBatch] ADD CONSTRAINT [PK_AuthorizationBatch] PRIMARY KEY NONCLUSTERED  ([AuthorizationBatch_id]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_AuthorizationBatch_InternalKey_TerminalNumber] ON [Trans].[AuthorizationBatch] ([BatchInternalKey], [TransTerminalNumber]) ON [PRIMARY]
GO
ALTER TABLE [Trans].[AuthorizationBatch] ADD CONSTRAINT [FK_AuthorizationBatch_ActionStatusID] FOREIGN KEY ([ActionStatus_id]) REFERENCES [List].[ActionStatus] ([ActionStatus_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Autorization batches that were sent', 'SCHEMA', N'Trans', 'TABLE', N'AuthorizationBatch', NULL, NULL
GO
