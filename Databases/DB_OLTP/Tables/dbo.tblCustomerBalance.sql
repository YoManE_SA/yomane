CREATE TABLE [dbo].[tblCustomerBalance]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CustomerID] [int] NOT NULL CONSTRAINT [DF_tblCustomerBalance_CustomerID] DEFAULT ((0)),
[Amount] [money] NOT NULL CONSTRAINT [DF_tblCustomerBalance_Amount] DEFAULT ((0)),
[Currency] [int] NOT NULL CONSTRAINT [DF_tblCustomerBalance_Currency] DEFAULT ((0)),
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCustomerBalance_InsertDate] DEFAULT (getdate()),
[Type] [int] NOT NULL CONSTRAINT [DF_tblCustomerBalance_Type] DEFAULT ((0)),
[Balance] [money] NOT NULL CONSTRAINT [DF_tblCustomerBalance_Balance] DEFAULT ((0)),
[Comment] [nvarchar] (4000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerBalance_Comment] DEFAULT (''),
[IP] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCustomerBalance_IP] DEFAULT (''),
[Status] [bit] NOT NULL CONSTRAINT [DF_tblCustomerBalance_Status] DEFAULT ((0)),
[CustomerWithdrawalID] [int] NOT NULL CONSTRAINT [DF_tblCustomerBalance_CustomerWithdrawalID] DEFAULT ((0)),
[BalanceSourceID] [int] NOT NULL CONSTRAINT [DF_tblCustomerBalance_balanceSourceID] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCustomerBalance] ADD CONSTRAINT [PK_tblCustomerBalance] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Replaced by [Data].[AccountBalance]', 'SCHEMA', N'dbo', 'TABLE', N'tblCustomerBalance', NULL, NULL
GO
