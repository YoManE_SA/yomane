CREATE TABLE [Log].[FraudDetection]
(
[FraudDetection_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NULL,
[TransPass_id] [int] NULL,
[TransPreAuth_id] [int] NULL,
[TransPending_id] [int] NULL,
[TransFail_id] [int] NULL,
[InsertDate] [datetime2] NOT NULL CONSTRAINT [DF_FraudDetection_insertDate] DEFAULT (sysdatetime()),
[SendingString] [nvarchar] (1500) COLLATE Hebrew_CI_AS NULL,
[ReturnAnswer] [nvarchar] (1500) COLLATE Hebrew_CI_AS NULL,
[ReturnExplanation] [nvarchar] (1500) COLLATE Hebrew_CI_AS NULL,
[ReturnScore] [decimal] (5, 2) NULL,
[ReturnRiskScore] [decimal] (5, 2) NULL,
[ReturnBinCountry] [nvarchar] (2) COLLATE Hebrew_CI_AS NULL,
[ReturnQueriesRemaining] [nchar] (5) COLLATE Hebrew_CI_AS NULL,
[ReferenceCode] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[allowedScore] [decimal] (5, 2) NULL,
[replyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[IsTemperScore] [bit] NULL,
[IsProceed] [bit] NULL,
[transAmount] [money] NULL,
[transCurrency] [tinyint] NULL,
[PaymentMethodDisplay] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[BinCountry] [nchar] (2) COLLATE Hebrew_CI_AS NULL,
[IsDuplicateAnswer] [bit] NULL,
[SendingIp] [char] (15) COLLATE Hebrew_CI_AS NULL,
[SendingCity] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[SendingRegion] [nchar] (2) COLLATE Hebrew_CI_AS NULL,
[SendingPostal] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[SendingCountry] [nchar] (2) COLLATE Hebrew_CI_AS NULL,
[SendingDomain] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[SendingBin] [char] (6) COLLATE Hebrew_CI_AS NULL,
[AllowedRiskScore] [decimal] (18, 2) NULL,
[IsCountryWhitelisted] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Log].[FraudDetection] ADD CONSTRAINT [PK_FraudDetection] PRIMARY KEY NONCLUSTERED  ([FraudDetection_id]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_FraudDetection_InsertDate] ON [Log].[FraudDetection] ([InsertDate]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log MaxMind fraud detaction usage', 'SCHEMA', N'Log', 'TABLE', N'FraudDetection', NULL, NULL
GO
