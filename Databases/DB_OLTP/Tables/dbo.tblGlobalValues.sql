CREATE TABLE [dbo].[tblGlobalValues]
(
[Prime] [real] NULL,
[PSPFeeMonthly] [money] NULL,
[PSPFeeTransaction] [money] NULL,
[PSPFeeVolumePercent] [real] NULL,
[DebitTerminalVolumePercent] [real] NULL,
[DeclineCountHours] [int] NULL,
[AnnualFeeAmount] [money] NULL,
[AnnualFeeLowRiskAmount] [money] NULL,
[AnnualFeeHighRiskAmount] [money] NULL,
[MonthlyFeeDollar] [money] NULL,
[MonthlyFeeEuro] [money] NULL,
[MonthlyFeeBankHigh] [money] NULL,
[MonthlyFeeBankLow] [money] NULL,
[AnnualFee3dSecureAmount] [money] NULL,
[AnnualFeeRegistrationAmount] [money] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Hold few values for system use', 'SCHEMA', N'dbo', 'TABLE', N'tblGlobalValues', NULL, NULL
GO
