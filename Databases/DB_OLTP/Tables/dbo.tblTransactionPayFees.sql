CREATE TABLE [dbo].[tblTransactionPayFees]
(
[CCF_ID] [int] NOT NULL IDENTITY(1, 1),
[CCF_TransactionPayID] [int] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_TransactionPayID] DEFAULT ((0)),
[CCF_CompanyID] [int] NOT NULL,
[CCF_CurrencyID] [int] NOT NULL,
[CCF_PaymentMethod] [smallint] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_PaymentMethod] DEFAULT ((0)),
[CCF_CreditTypeID] [int] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_CreditTypeID] DEFAULT ((0)),
[CCF_ListBINs] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL,
[CCF_ExchangeTo] [smallint] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_ExchangeTo] DEFAULT ((-1)),
[CCF_MaxAmount] [money] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_MaxAmount] DEFAULT ((0)),
[CCF_PercentFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_PercentFee] DEFAULT ((0)),
[CCF_FixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_FixedFee] DEFAULT ((0)),
[CCF_ApproveFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_ApproveFixedFee] DEFAULT ((0)),
[CCF_RefundFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_RefundFixedFee] DEFAULT ((0)),
[CCF_ClarificationFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_ClarificationFee] DEFAULT ((0)),
[CCF_CBFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_CBFixedFee] DEFAULT ((0)),
[CCF_FailFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblTransactionPayFees_CCF_FailFixedFee] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTransactionPayFees] ADD CONSTRAINT [PK_tblTransactionPayFees] PRIMARY KEY CLUSTERED  ([CCF_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionPayFees_CCF_CompanyID] ON [dbo].[tblTransactionPayFees] ([CCF_CompanyID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblTransactionPayFees_CCF_TransactionPayID] ON [dbo].[tblTransactionPayFees] ([CCF_TransactionPayID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionPayFees', NULL, NULL
GO
