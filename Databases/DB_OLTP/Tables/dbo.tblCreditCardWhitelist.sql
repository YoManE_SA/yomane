CREATE TABLE [dbo].[tblCreditCardWhitelist]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ccwl_Merchant] [int] NOT NULL,
[ccwl_CardNumber256] [varbinary] (200) NOT NULL,
[ccwl_PaymentMethod] [smallint] NULL,
[ccwl_Last4] [smallint] NULL,
[ccwl_Bin] [int] NULL,
[ccwl_BinCountry] [char] (2) COLLATE Hebrew_CI_AS NULL,
[ccwl_CardHolder] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[ccwl_ExpMonth] [tinyint] NULL,
[ccwl_ExpYear] [smallint] NULL,
[ccwl_BurnDate] [datetime] NULL,
[ccwl_Level] [tinyint] NOT NULL CONSTRAINT [DF_tblCreditCardWhitelist_ccwl_Level] DEFAULT ((0)),
[ccwl_Comment] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[ccwl_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCreditCardWhitelist_ccwl_InsertDate] DEFAULT (getdate()),
[ccwl_Username] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCardWhitelist_ccwl_Username] DEFAULT (user_name()),
[ccwl_IP] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCardWhitelist_ccwl_IP] DEFAULT (''),
[ccwl_IsBurnt] AS (CONVERT([bit],case when [ccwl_BurnDate] IS NULL then (0) else (1) end,0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCreditCardWhitelist] ADD CONSTRAINT [PK_tblCreditCardWhitelist] PRIMARY KEY CLUSTERED  ([ccwl_Merchant], [ccwl_CardNumber256]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCreditCardWhitelist_InsertDate] ON [dbo].[tblCreditCardWhitelist] ([ccwl_InsertDate] DESC) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblCreditCardWhitelist_ID] ON [dbo].[tblCreditCardWhitelist] ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Credit card white list', 'SCHEMA', N'dbo', 'TABLE', N'tblCreditCardWhitelist', NULL, NULL
GO
