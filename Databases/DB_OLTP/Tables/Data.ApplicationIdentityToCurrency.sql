CREATE TABLE [Data].[ApplicationIdentityToCurrency]
(
[ApplicationIdentityToCurrency_id] [int] NOT NULL IDENTITY(1, 1),
[ApplicationIdentity_id] [int] NOT NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ApplicationIdentityToCurrency] ADD CONSTRAINT [PK_ApplicationIdentityToCurrency] PRIMARY KEY NONCLUSTERED  ([ApplicationIdentityToCurrency_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ApplicationIdentityToCurrency] ADD CONSTRAINT [FK_ApplicationIdentityToCurrency_ApplicationIdentityID] FOREIGN KEY ([ApplicationIdentity_id]) REFERENCES [Data].[ApplicationIdentity] ([ApplicationIdentity_id]) ON DELETE CASCADE
GO
ALTER TABLE [Data].[ApplicationIdentityToCurrency] ADD CONSTRAINT [FK_ApplicationIdentityToCurrency_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode]) ON DELETE CASCADE
GO
