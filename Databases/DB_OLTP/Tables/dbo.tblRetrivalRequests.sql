CREATE TABLE [dbo].[tblRetrivalRequests]
(
[RetrivalRequests_ID] [int] NOT NULL IDENTITY(1, 1),
[TransPass_ID] [int] NULL,
[RR_InsertDate] [datetime] NULL CONSTRAINT [DF_tblRetrivalRequests_RR_InsertDate] DEFAULT (getdate()),
[RR_ReasonCode] [int] NULL,
[RR_BankCaseID] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[RR_Ticket] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[RR_Deadline] [datetime] NULL,
[RR_Comments] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRetrivalRequests] ADD CONSTRAINT [PK_tblRetrivalRequests] PRIMARY KEY CLUSTERED  ([RetrivalRequests_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRetrivalRequests] ADD CONSTRAINT [FK_tblRetrivalRequests_tblCompanyTransPass_TransPass_ID] FOREIGN KEY ([TransPass_ID]) REFERENCES [dbo].[tblCompanyTransPass] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Approved transaction marked as a retrival request', 'SCHEMA', N'dbo', 'TABLE', N'tblRetrivalRequests', NULL, NULL
GO
