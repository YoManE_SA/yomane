CREATE TABLE [dbo].[EventPending]
(
[EventPending_id] [int] NOT NULL IDENTITY(1, 1),
[EventPendingType_id] [smallint] NOT NULL,
[TransPass_id] [int] NULL,
[TransPreAuth_id] [int] NULL,
[TransPending_id] [int] NULL,
[TransFail_id] [int] NULL,
[Merchant_id] [int] NULL,
[Customer_id] [int] NULL,
[Parameters] [nvarchar] (1000) COLLATE Hebrew_CI_AS NULL,
[InsertDate] [datetime2] (2) NOT NULL CONSTRAINT [DF_EventPending_InsertDate] DEFAULT (sysdatetime()),
[TryCount] [tinyint] NOT NULL CONSTRAINT [DF_EventPending_TryCount] DEFAULT ((1)),
[ProcessStartTime] [datetime2] NULL,
[processServer] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[EventPending] ADD
CONSTRAINT [FK_EventPending_Customer] FOREIGN KEY ([Customer_id]) REFERENCES [Data].[Customer] ([Customer_id])
GO
ALTER TABLE [dbo].[EventPending] ADD CONSTRAINT [PK_EventPending] PRIMARY KEY CLUSTERED  ([EventPending_id]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[EventPending] ADD CONSTRAINT [FK_EventPending_EventPendingType] FOREIGN KEY ([EventPendingType_id]) REFERENCES [List].[EventPendingType] ([EventPendingType_id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EventPending] ADD CONSTRAINT [FK_EventPending_Merchant] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [dbo].[EventPending] ADD CONSTRAINT [FK_EventPending_tblCompanyTransFail] FOREIGN KEY ([TransFail_id]) REFERENCES [dbo].[tblCompanyTransFail] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EventPending] ADD CONSTRAINT [FK_EventPending_tblCompanyTransPass] FOREIGN KEY ([TransPass_id]) REFERENCES [dbo].[tblCompanyTransPass] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EventPending] ADD CONSTRAINT [FK_EventPending_tblCompanyTransPending] FOREIGN KEY ([TransPending_id]) REFERENCES [dbo].[tblCompanyTransPending] ([companyTransPending_id])
GO
ALTER TABLE [dbo].[EventPending] ADD CONSTRAINT [FK_EventPending_tblCompanyTransApproval] FOREIGN KEY ([TransPreAuth_id]) REFERENCES [dbo].[tblCompanyTransApproval] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Pending events - run by WinService', 'SCHEMA', N'dbo', 'TABLE', N'EventPending', NULL, NULL
GO
