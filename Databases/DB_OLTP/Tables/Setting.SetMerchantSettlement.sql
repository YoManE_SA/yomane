CREATE TABLE [Setting].[SetMerchantSettlement]
(
[SetMerchantSettlement_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[Currency_id] [int] NOT NULL,
[WireFee] [money] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_WireFee] DEFAULT ((0)),
[WireFeePercent] [money] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_WireFeePercent] DEFAULT ((0)),
[StorageFee] [money] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_StorageFee] DEFAULT ((0)),
[HandlingFee] [money] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_HandlingFee] DEFAULT ((0)),
[MinPayoutAmount] [money] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_MinPayoutAmount] DEFAULT ((0)),
[MinSettlementAmount] [money] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_MinSettlementAmount] DEFAULT ((0)),
[IsAutoInvoice] [bit] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_IsAutoInvoice] DEFAULT ((0)),
[IsShowToSettle] [bit] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_IsShowToSettle] DEFAULT ((0)),
[IsWireExcludeDebit] [bit] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_IsWireExcludeDebit] DEFAULT ((0)),
[IsWireExcludeRefund] [bit] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_IsWireExcludeRefund] DEFAULT ((0)),
[IsWireExcludeFee] [bit] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_IsWireExcludeFee] DEFAULT ((0)),
[IsWireExcludeChb] [bit] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_IsWireExcludeChb] DEFAULT ((0)),
[IsWireExcludeCashback] [bit] NOT NULL CONSTRAINT [DF_SetMerchantSettlement_IsWireExcludeCashback] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [Setting].[trSetMerchantSettlement_RemoveNonSetRecords_Upd]
   ON  [Setting].[SetMerchantSettlement]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS
	(
		SELECT * FROM INSERTED
		WHERE
			[WireFee]+[WireFeePercent]+[StorageFee]+[HandlingFee]+[MinPayoutAmount]+[MinSettlementAmount] = 0
			AND [IsShowToSettle] = 0
			AND [IsAutoInvoice] = 0
	)
	BEGIN
	
		DELETE [Setting].[SetMerchantSettlement]
		WHERE 
			[SetMerchantSettlement_id] = (SELECT [SetMerchantSettlement_id] FROM INSERTED)
	END
END

GO
ALTER TABLE [Setting].[SetMerchantSettlement] ADD CONSTRAINT [PK_SetMerchantSettlement] PRIMARY KEY CLUSTERED  ([SetMerchantSettlement_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantSettlement] ADD CONSTRAINT [UIX_SetMerchantSettlementMerchantCurrency] UNIQUE NONCLUSTERED  ([Merchant_id], [Currency_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantSettlement] ADD CONSTRAINT [FK_SetMerchantSettlement_tblSystemCurrencies_Currency_id] FOREIGN KEY ([Currency_id]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
ALTER TABLE [Setting].[SetMerchantSettlement] ADD CONSTRAINT [FK_SetMerchantSettlement_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
