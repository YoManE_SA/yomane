CREATE TABLE [dbo].[tblCompanyCreditRestrictions]
(
[CCR_ID] [int] NOT NULL IDENTITY(1, 1),
[CCR_ParentID] [int] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_ParentID] DEFAULT ((0)),
[CCR_CPM_ID] [int] NOT NULL,
[CCR_Order] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_Order] DEFAULT ((0)),
[CCR_Type] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_Type] DEFAULT ((0)),
[CCR_ChildType] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_ChildType] DEFAULT ((0)),
[CCR_ListValue] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_ListValue] DEFAULT (''),
[CCR_Ratio] [int] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_Ratio] DEFAULT ((1)),
[CCR_UseCount] [int] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_UseCount] DEFAULT ((0)),
[CCR_ExchangeTo] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_ExchangeTo] DEFAULT ((0)),
[CCR_TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_TerminalNumber] DEFAULT (''),
[CCR_MaxAmount] [int] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_MaxAmount] DEFAULT ((0)),
[CCR_PercentFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_PercentFee] DEFAULT ((0)),
[CCR_FixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_FixedFee] DEFAULT ((0)),
[CCR_ApproveFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_ApproveFixedFee] DEFAULT ((0)),
[CCR_RefundFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_RefundFixedFee] DEFAULT ((0)),
[CCR_ClarificationFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_ClarificationFee] DEFAULT ((0)),
[CCR_CBFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_CBFixedFee] DEFAULT ((0)),
[CCR_FailFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblCompanyCreditRestrictions_CCR_FailFixedFee] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyCreditRestrictions] ADD CONSTRAINT [PK_tblCompanyCreditRestrictions] PRIMARY KEY CLUSTERED  ([CCR_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyCreditRestrictions', NULL, NULL
GO
