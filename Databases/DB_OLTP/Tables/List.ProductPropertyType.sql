CREATE TABLE [List].[ProductPropertyType]
(
[ProductPropertyType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[ProductPropertyType] ADD CONSTRAINT [PK_ProductPropertyType] PRIMARY KEY CLUSTERED  ([ProductPropertyType_id]) ON [PRIMARY]
GO
