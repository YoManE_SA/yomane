CREATE TABLE [dbo].[DebitCompanyFeeExcludeInvoice]
(
[DebitCompany_id] [int] NOT NULL,
[TransAmountType_id] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DebitCompanyFeeExcludeInvoice] ADD CONSTRAINT [PK_DebitCompanyFeeExcludeInvoice] PRIMARY KEY CLUSTERED  ([DebitCompany_id], [TransAmountType_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DebitCompanyFeeExcludeInvoice] ADD CONSTRAINT [FK_DebitCompanyFeeExcludeInvoice_tblDebitCompany_DebitCompany_id] FOREIGN KEY ([DebitCompany_id]) REFERENCES [dbo].[tblDebitCompany] ([DebitCompany_ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DebitCompanyFeeExcludeInvoice] ADD CONSTRAINT [FK_DebitCompanyFeeExcludeInvoice_TransAmountType_TransAmountType_id] FOREIGN KEY ([TransAmountType_id]) REFERENCES [List].[TransAmountType] ([TransAmountType_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'DebitCompanyFeeExcludeInvoice', NULL, NULL
GO
