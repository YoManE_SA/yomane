CREATE TABLE [dbo].[tblCreditCardRiskManagement]
(
[CCRM_ID] [int] NOT NULL IDENTITY(1, 1),
[CCRM_InsDate] [datetime] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_InsDate] DEFAULT (getdate()),
[CCRM_CompanyID] [int] NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_CompanyID] DEFAULT ((0)),
[CCRM_PaymentMethod] [smallint] NULL,
[CCRM_CreditType] [tinyint] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_CreditType] DEFAULT ((1)),
[CCRM_Currency] [smallint] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_Currency] DEFAULT ((-1)),
[CCRM_Amount] [money] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_Amount] DEFAULT ((0)),
[CCRM_MaxTrans] [int] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_MaxTrans] DEFAULT ((0)),
[CCRM_Act] [tinyint] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_Act] DEFAULT ((0)),
[CCRM_ReplySource] [int] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_ReplySource] DEFAULT ((-1)),
[CCRM_Hours] [int] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_Hours] DEFAULT ((0)),
[CCRM_IsActive] [bit] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_IsActive] DEFAULT ((0)),
[CCRM_BlockHours] [int] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_BlockHours] DEFAULT ((0)),
[ccrm_days] AS (CONVERT([int],[ccrm_hours]/(24),0)),
[ccrm_blockdays] AS (CONVERT([int],[ccrm_blockhours]/(24),0)),
[CCRM_ReplyAmount] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_ReplyAmount] DEFAULT ('586'),
[CCRM_ReplyMaxTrans] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_ReplyMaxTrans] DEFAULT ('585'),
[CCRM_WhitelistLevel] [int] NULL,
[CCRM_ApplyVT] [bit] NOT NULL CONSTRAINT [DF_tblCreditCardRiskManagement_CCRM_ApplyVT] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCreditCardRiskManagement] ADD CONSTRAINT [PK_tblCreditCardRiskManagement] PRIMARY KEY CLUSTERED  ([CCRM_ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCreditCardRiskManagement_CCRM_CompanyID] ON [dbo].[tblCreditCardRiskManagement] ([CCRM_CompanyID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCreditCardRiskManagement] ADD CONSTRAINT [FK_tblCreditCardRiskManagement_tblCompany_CCRM_CompanyID] FOREIGN KEY ([CCRM_CompanyID]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [dbo].[tblCreditCardRiskManagement] ADD CONSTRAINT [FK_tblCreditCardRiskManagement_PaymentMethod_CCRM_PaymentMethod] FOREIGN KEY ([CCRM_PaymentMethod]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s risk rules', 'SCHEMA', N'dbo', 'TABLE', N'tblCreditCardRiskManagement', NULL, NULL
GO
