CREATE TABLE [dbo].[tblLogMasavFile]
(
[logMasavFile_id] [int] NOT NULL IDENTITY(1, 1),
[insertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_tblLogMasavFile_insertDate] DEFAULT (''),
[PayedBankCode] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLogMasavFile_PayedBankCode] DEFAULT (''),
[PayedBankDesc] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLogMasavFile_PayedBankDesc] DEFAULT (''),
[LogonUser] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLogMasavFile_LogonUser] DEFAULT (''),
[Flag] [tinyint] NOT NULL CONSTRAINT [DF_tblLogMasavFile_Flag] DEFAULT ((0)),
[PayAmount] [float] NOT NULL CONSTRAINT [DF_tblLogMasavFile_PayAmount] DEFAULT ((0)),
[PayCurrency] [tinyint] NOT NULL CONSTRAINT [DF_tblLogMasavFile_PayCurrency] DEFAULT ((0)),
[PayCount] [smallint] NOT NULL CONSTRAINT [DF_tblLogMasavFile_PayCount] DEFAULT ((0)),
[DoneFlag] [tinyint] NOT NULL CONSTRAINT [DF_tblLogMasavFile_DoneFlag] DEFAULT ((0)),
[AttachedFileExt] [varchar] (5) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLogMasavFile_AttachedFileExt] DEFAULT (''),
[FileName] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[WireProvider_Id] [varchar] (16) COLLATE Hebrew_CI_AS NULL,
[IsSendRequire] [bit] NOT NULL CONSTRAINT [DF_tblLogMasavFile_IsSendRequire] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogMasavFile] ADD CONSTRAINT [PK_tblLogMasavFile] PRIMARY KEY CLUSTERED  ([logMasavFile_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log money wired to the bank per bank order', 'SCHEMA', N'dbo', 'TABLE', N'tblLogMasavFile', NULL, NULL
GO
