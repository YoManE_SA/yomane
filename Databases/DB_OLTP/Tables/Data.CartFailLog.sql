CREATE TABLE [Data].[CartFailLog]
(
[CartFailLog_id] [int] NOT NULL IDENTITY(1, 1),
[Merchant_id] [int] NOT NULL,
[Cart_id] [int] NOT NULL,
[TransFail_id] [int] NULL,
[InsertDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_CartFailLog_InsertDate] DEFAULT (sysdatetime())
) ON [PRIMARY]
GO
ALTER TABLE [Data].[CartFailLog] ADD CONSTRAINT [PK_CartFailLog] PRIMARY KEY CLUSTERED  ([CartFailLog_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[CartFailLog] ADD CONSTRAINT [FK_CartFailLog_Cart_CartID] FOREIGN KEY ([Cart_id]) REFERENCES [Data].[Cart] ([Cart_id]) ON DELETE CASCADE
GO
ALTER TABLE [Data].[CartFailLog] ADD CONSTRAINT [FK_CartFailLog_tblCompany_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Data].[CartFailLog] ADD CONSTRAINT [FK_CartFailLog_tblCompanyTransFail_TransFailID] FOREIGN KEY ([TransFail_id]) REFERENCES [dbo].[tblCompanyTransFail] ([ID]) ON DELETE CASCADE
GO
