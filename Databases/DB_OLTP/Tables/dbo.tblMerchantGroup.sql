CREATE TABLE [dbo].[tblMerchantGroup]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[mg_Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgMerchantGroupRemoveResetMerchants] ON [dbo].[tblMerchantGroup] AFTER DELETE AS
UPDATE tblCompany SET GroupID=NULL WHERE GroupID IN (SELECT ID FROM DELETED)
GO
ALTER TABLE [dbo].[tblMerchantGroup] ADD CONSTRAINT [PK_tblMerchantGroup] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant group', 'SCHEMA', N'dbo', 'TABLE', N'tblMerchantGroup', NULL, NULL
GO
