CREATE TABLE [dbo].[tblAffiliatePayments]
(
[AFP_ID] [int] NOT NULL IDENTITY(1, 1),
[AFP_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblAffiliatePayments_AFP_InsertDate] DEFAULT (getdate()),
[AFP_Affiliate_ID] [int] NOT NULL CONSTRAINT [DF_tblAffiliatePayments_AFP_Affiliate_ID] DEFAULT ((0)),
[AFP_TransPaymentID] [int] NOT NULL CONSTRAINT [DF_tblAffiliatePayments_AFP_TransPaymentID] DEFAULT ((0)),
[AFP_FeeRatio] [money] NOT NULL CONSTRAINT [DF_tblAffiliatePayments_AFP_FeeRatio] DEFAULT ((0)),
[AFP_PaymentAmount] [money] NOT NULL CONSTRAINT [DF_tblAffiliatePayments_AFP_PaymentAmount] DEFAULT ((0)),
[AFP_PaymentNote] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliatePayments_AFP_PaymentNote] DEFAULT (''),
[AFP_CompanyID] [int] NOT NULL CONSTRAINT [DF_tblAffiliatePayments_AFP_CompanyID] DEFAULT ((0)),
[AFP_PaymentCurrency] [int] NOT NULL CONSTRAINT [DF_tblAffiliatePayments_AFP_PaymentCurrency] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliatePayments] ADD CONSTRAINT [PK_tblAffiliatePayments] PRIMARY KEY CLUSTERED  ([AFP_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliatePayments] ADD CONSTRAINT [FK_tblAffiliatePayments_tblAffiliates_AFP_Affiliate_ID] FOREIGN KEY ([AFP_Affiliate_ID]) REFERENCES [dbo].[tblAffiliates] ([affiliates_id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblAffiliatePayments] ADD CONSTRAINT [FK_tblAffiliatePayments_tblSystemCurrencies_AFP_PaymentCurrency] FOREIGN KEY ([AFP_PaymentCurrency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Affiliate''s payment', 'SCHEMA', N'dbo', 'TABLE', N'tblAffiliatePayments', NULL, NULL
GO
