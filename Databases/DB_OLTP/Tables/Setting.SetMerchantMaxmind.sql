CREATE TABLE [Setting].[SetMerchantMaxmind]
(
[Merchant_id] [int] NOT NULL,
[IsEnabled] [bit] NULL,
[ScoreAllowed] [decimal] (5, 2) NULL,
[RiskScoreAllowed] [decimal] (5, 2) NULL,
[IsAllowPublicEmail] [bit] NULL,
[SkipIPList] [varchar] (500) COLLATE Hebrew_CI_AS NULL,
[IsSkipVirtualTerminal] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantMaxmind] ADD CONSTRAINT [PK_SetMerchantMaxmind] PRIMARY KEY CLUSTERED  ([Merchant_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetMerchantMaxmind] ADD CONSTRAINT [FK_SetMerchantMaxmind_tblCompany_MerchantID] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
