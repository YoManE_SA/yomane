CREATE TABLE [dbo].[tblAffiliateFeeSteps]
(
[AFS_ID] [int] NOT NULL IDENTITY(1, 1),
[AFS_AffiliateID] [int] NULL,
[AFS_CompanyID] [int] NULL,
[AFS_UpToAmount] [money] NOT NULL,
[AFS_Currency] [int] NOT NULL,
[AFS_PaymentMethod] [smallint] NULL,
[AFS_SlicePercent] [money] NULL CONSTRAINT [DF_tblAffiliateFeeSteps_AFS_SlicePercent] DEFAULT ((0)),
[AFS_AFPID] [int] NULL,
[AFS_PercentFee] [smallmoney] NOT NULL,
[AFS_CalcMethod] [tinyint] NOT NULL CONSTRAINT [DF_tblAffiliateFeeSteps_AFS_CalcMethod] DEFAULT ((0)),
[AFS_TransType] [tinyint] NOT NULL CONSTRAINT [DF_tblAffiliateFeeSteps_AFS_TransType] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliateFeeSteps] ADD CONSTRAINT [PK_tblAffiliateFeeSteps] PRIMARY KEY CLUSTERED  ([AFS_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliateFeeSteps] ADD CONSTRAINT [FK_tblAffiliateFeeSteps_tblAffiliates_AFS_AffiliateID] FOREIGN KEY ([AFS_AffiliateID]) REFERENCES [dbo].[tblAffiliates] ([affiliates_id])
GO
ALTER TABLE [dbo].[tblAffiliateFeeSteps] ADD CONSTRAINT [FK_tblAffiliateFeeSteps_tblAffiliatePayments_AFS_AFPID] FOREIGN KEY ([AFS_AFPID]) REFERENCES [dbo].[tblAffiliatePayments] ([AFP_ID])
GO
ALTER TABLE [dbo].[tblAffiliateFeeSteps] ADD CONSTRAINT [FK_tblAffiliateFeeSteps_tblCompany_AFS_CompanyID] FOREIGN KEY ([AFS_CompanyID]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [dbo].[tblAffiliateFeeSteps] ADD CONSTRAINT [FK_tblAffiliateFeeSteps_tblSystemCurrencies_AFS_Currency] FOREIGN KEY ([AFS_Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Affiliate fees', 'SCHEMA', N'dbo', 'TABLE', N'tblAffiliateFeeSteps', NULL, NULL
GO
