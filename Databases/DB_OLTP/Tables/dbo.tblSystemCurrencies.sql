CREATE TABLE [dbo].[tblSystemCurrencies]
(
[CUR_ID] [int] NOT NULL CONSTRAINT [DF_tblSystemCurrencies_CUR_ID] DEFAULT ((0)),
[CUR_BaseRate] [smallmoney] NOT NULL CONSTRAINT [DF_tblSystemCurrencies_CUR_BaseRate] DEFAULT ((1)),
[CUR_ISOName] [nchar] (3) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSystemCurrencies_CUR_ISOName] DEFAULT ('   '),
[CUR_Symbol] [nvarchar] (25) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblSystemCurrencies_CUR_Symbol] DEFAULT (''),
[CUR_FullName] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblSystemCurrencies_CUR_FullName] DEFAULT (''),
[CUR_ExchangeFeeInd] [float] NOT NULL CONSTRAINT [DF_tblSystemCurrencies_CUR_ExchangeFeeInd] DEFAULT ((0)),
[CUR_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblSystemCurrencies_CUR_InsertDate] DEFAULT (getdate()),
[CUR_ISOCode] [int] NOT NULL CONSTRAINT [DF_tblSystemCurrencies_CUR_ISOCode] DEFAULT ((0)),
[CUR_TransactionAmountMax] [money] NOT NULL CONSTRAINT [DF_tblSystemCurrencies_CUR_TransactionAmountMax] DEFAULT ((0)),
[CUR_IsSymbolBeforeAmount] [bit] NOT NULL CONSTRAINT [DF_tblSystemCurrencies_CUR_IsSymbolBeforeAmount] DEFAULT ((0)),
[CUR_RateRequestDate] [datetime] NULL,
[CUR_RateValueDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[SystemCurrencies_NewCurrencyList_Upd] ON [dbo].[tblSystemCurrencies]
    FOR UPDATE
AS
    BEGIN
    	
		IF @@ROWCOUNT = 0 RETURN;
		SET NOCOUNT ON;
		
        UPDATE  [List].[CurrencyList]
        SET     [BaseRate] = Inserted.CUR_BaseRate ,
				[ExchangeFeeInd] = Inserted.CUR_ExchangeFeeInd ,
				[RateRequestDate] = Inserted.CUR_RateRequestDate ,
				[RateValueDate] = Inserted.CUR_RateValueDate ,
				[MaxTransactionAmount] = Inserted.CUR_TransactionAmountMax ,
				[IsSymbolBeforeAmount] = Inserted.CUR_IsSymbolBeforeAmount
        FROM	[List].[CurrencyList]AS c1 JOIN INSERTED ON c1.[CurrencyISOCode] = inserted.[CUR_ISOName]

    END

GO
ALTER TABLE [dbo].[tblSystemCurrencies] ADD CONSTRAINT [PK_tblSystemCurrencies] PRIMARY KEY CLUSTERED  ([CUR_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds currency list (Replace by List.CurrencyList)', 'SCHEMA', N'dbo', 'TABLE', N'tblSystemCurrencies', NULL, NULL
GO
