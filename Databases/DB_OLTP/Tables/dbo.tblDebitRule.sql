CREATE TABLE [dbo].[tblDebitRule]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[dr_IsActive] [bit] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_IsActive] DEFAULT ((0)),
[dr_DebitCompany] [int] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_DebitCompany] DEFAULT ((0)),
[dr_ReplyCodes] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitRule_dr_ReplyCodes] DEFAULT (''),
[dr_NotifyUsers] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitRule_dr_NotifyUsers] DEFAULT (''),
[dr_AttemptCount] [int] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_AttemptCount] DEFAULT ((0)),
[dr_FailCount] [int] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_FailCount] DEFAULT ((0)),
[dr_IsAutoDisable] [bit] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_IsAutoDisable] DEFAULT ((0)),
[dr_IsAutoEnable] [bit] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_IsAutoEnable] DEFAULT ((0)),
[dr_AutoEnableMinutes] [int] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_AutoEnableMinutes] DEFAULT ((0)),
[dr_AutoEnableAttempts] [int] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_AutoEnableAttempts] DEFAULT ((0)),
[dr_LastFailDate] [datetime] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_LastFailDate] DEFAULT ((0)),
[dr_LastUnblockDate] [datetime] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_LastUnblockDate] DEFAULT ((0)),
[dr_NotifyUsersSMS] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitRule_dr_NotifyUsersSMS] DEFAULT (''),
[dr_Rating] [int] NOT NULL CONSTRAINT [DF_tblDebitRule_dr_Rating] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgDebitRuleAddSetRating] ON [dbo].[tblDebitRule] FOR INSERT AS
UPDATE tblDebitRule SET dr_Rating=ID WHERE ID IN (SELECT ID FROM Inserted)
GO
ALTER TABLE [dbo].[tblDebitRule] ADD CONSTRAINT [PK_tblDebitRule] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Debit company notification/block rules', 'SCHEMA', N'dbo', 'TABLE', N'tblDebitRule', NULL, NULL
GO
