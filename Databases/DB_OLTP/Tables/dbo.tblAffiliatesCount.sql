CREATE TABLE [dbo].[tblAffiliatesCount]
(
[AFCID] [int] NOT NULL IDENTITY(1, 1),
[AFCAFFID] [int] NULL,
[AFCType] [tinyint] NULL,
[AFCIPAddress] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[AFCDate] [date] NULL CONSTRAINT [DF_tblAffiliatesCount_AFCDate] DEFAULT (getdate()),
[AFCReferal] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliatesCount] ADD CONSTRAINT [PK_tblAffiliatesCount] PRIMARY KEY CLUSTERED  ([AFCID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliatesCount] ADD CONSTRAINT [FK_tblAffiliatesCount_tblAffiliates_AFCAFFID] FOREIGN KEY ([AFCAFFID]) REFERENCES [dbo].[tblAffiliates] ([affiliates_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Count leads from affiliates', 'SCHEMA', N'dbo', 'TABLE', N'tblAffiliatesCount', NULL, NULL
GO
