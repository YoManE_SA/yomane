CREATE TABLE [Risk].[CcMailUsage]
(
[CreditCardNumber256] [varbinary] (40) NOT NULL,
[EmailAddress] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Risk].[CcMailUsage] ADD CONSTRAINT [PK_CcMailUsage] PRIMARY KEY CLUSTERED  ([EmailAddress], [CreditCardNumber256]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Holds for risk the use of email address with creditcard number ', 'SCHEMA', N'Risk', 'TABLE', N'CcMailUsage', NULL, NULL
GO
