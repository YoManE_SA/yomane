CREATE TABLE [List].[InvoiceType]
(
[InvoiceType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[InvoiceType] ADD CONSTRAINT [PK_InvoiceType] PRIMARY KEY CLUSTERED  ([InvoiceType_id]) ON [PRIMARY]
GO
