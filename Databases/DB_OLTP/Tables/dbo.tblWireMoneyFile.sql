CREATE TABLE [dbo].[tblWireMoneyFile]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[wmf_WireMoneyID] [int] NOT NULL,
[wmf_FileName] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoneyFile_wmf_FileName] DEFAULT (''),
[wmf_Description] [nvarchar] (350) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoneyFile_wmf_Description] DEFAULT (''),
[wmf_Date] [datetime] NOT NULL CONSTRAINT [DF_tblWireMoneyFile_wmf_Date] DEFAULT (getdate()),
[wmf_User] [nvarchar] (40) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWireMoneyFile_wmf_User] DEFAULT (''),
[wmf_ParseResult] [tinyint] NOT NULL CONSTRAINT [DF_tblWireMoneyFile_wmf_ParseResult] DEFAULT ((0)),
[wmf_ParseLog] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblWireMoneyFile] ADD CONSTRAINT [PK_tblWireMoneyFile] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblWireMoneyFile] ADD CONSTRAINT [FK_tblWireMoneyFile_tblWireMoney_wmf_WireMoneyID] FOREIGN KEY ([wmf_WireMoneyID]) REFERENCES [dbo].[tblWireMoney] ([WireMoney_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Wires uploaded files', 'SCHEMA', N'dbo', 'TABLE', N'tblWireMoneyFile', NULL, NULL
GO
