CREATE TABLE [List].[EventPendingType]
(
[EventPendingType_id] [smallint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[MinutesForWarning] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[EventPendingType] ADD CONSTRAINT [PK_EventPendingType] PRIMARY KEY CLUSTERED  ([EventPendingType_id]) ON [PRIMARY]
GO
