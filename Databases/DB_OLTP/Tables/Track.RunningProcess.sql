CREATE TABLE [Track].[RunningProcess]
(
[CreditCardNumber256] [varbinary] (40) NOT NULL,
[Merchant_id] [int] NOT NULL,
[InsertDate] [datetime2] (2) NOT NULL CONSTRAINT [DF_RunningProcess_InsertDate] DEFAULT (sysdatetime())
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Track credit card numbers currently in process', 'SCHEMA', N'Track', 'TABLE', N'RunningProcess', NULL, NULL
GO
