CREATE TABLE [Data].[ApplicationIdentityToPaymentMethod]
(
[ApplicationIdentityToPaymentMethod_id] [int] NOT NULL IDENTITY(1, 1),
[ApplicationIdentity_id] [int] NOT NULL,
[PaymentMethod_id] [smallint] NOT NULL,
[IsAllowCreate] [bit] NOT NULL CONSTRAINT [DF_ApplicationIdentityToPaymentMethod_IsAllowCreate] DEFAULT ((0)),
[IsAllowProcess] [bit] NOT NULL CONSTRAINT [DF_ApplicationIdentityToPaymentMethod_IsAllowProcess] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ApplicationIdentityToPaymentMethod] ADD CONSTRAINT [PK_ApplicationIdentityToPaymentMethod] PRIMARY KEY NONCLUSTERED  ([ApplicationIdentityToPaymentMethod_id]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [UIX_ApplicationIdentityToPaymentMethod] ON [Data].[ApplicationIdentityToPaymentMethod] ([ApplicationIdentity_id], [PaymentMethod_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ApplicationIdentityToPaymentMethod] ADD CONSTRAINT [FK_ApplicationIdentityToPaymentMethod_ApplicationIdentity] FOREIGN KEY ([ApplicationIdentity_id]) REFERENCES [Data].[ApplicationIdentity] ([ApplicationIdentity_id]) ON DELETE CASCADE
GO
ALTER TABLE [Data].[ApplicationIdentityToPaymentMethod] ADD CONSTRAINT [FK_ApplicationIdentityToPaymentMethod_PaymentMethod] FOREIGN KEY ([PaymentMethod_id]) REFERENCES [List].[PaymentMethod] ([PaymentMethod_id]) ON DELETE CASCADE
GO
