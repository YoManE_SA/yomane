CREATE TABLE [System].[SecurityObjectToAdminGroup]
(
[SecurityObject_id] [int] NOT NULL,
[AdminGroup_id] [smallint] NOT NULL,
[Value] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[SecurityObjectToAdminGroup] ADD CONSTRAINT [PK_SecurityObjectToAdminGroup] PRIMARY KEY CLUSTERED  ([SecurityObject_id], [AdminGroup_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[SecurityObjectToAdminGroup] ADD CONSTRAINT [FK_SecurityObjectToAdminGroup_AdminGroup_AdminGroupID] FOREIGN KEY ([AdminGroup_id]) REFERENCES [System].[AdminGroup] ([AdminGroup_id]) ON DELETE CASCADE
GO
ALTER TABLE [System].[SecurityObjectToAdminGroup] ADD CONSTRAINT [FK_SecurityObjectToAdminGroup_SecurityObject_SecurityObjectID] FOREIGN KEY ([SecurityObject_id]) REFERENCES [System].[SecurityObject] ([SecurityObject_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'System security objects to admin group', 'SCHEMA', N'System', 'TABLE', N'SecurityObjectToAdminGroup', NULL, NULL
GO
