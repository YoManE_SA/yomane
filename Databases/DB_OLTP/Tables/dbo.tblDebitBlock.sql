CREATE TABLE [dbo].[tblDebitBlock]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[db_DebitRule] [int] NOT NULL CONSTRAINT [DF_tblDebitBlock_db_DebitRule] DEFAULT ((0)),
[db_InsertDate] [datetime] NOT NULL CONSTRAINT [DF_tblDebitBlock_db_InsertDate] DEFAULT (getdate()),
[db_UnblockDate] [datetime] NOT NULL CONSTRAINT [DF_tblDebitBlock_db_UnblockDate] DEFAULT (getdate()),
[db_AutoEnableAttempts] [int] NOT NULL CONSTRAINT [DF_tblDebitBlock_db_AutoEnableAttempts] DEFAULT ((0)),
[db_CountAutoEnableAttempts] [int] NOT NULL CONSTRAINT [DF_tblDebitBlock_db_CountAutoEnableAttempts] DEFAULT ((0)),
[db_IsAutoEnable] [bit] NOT NULL CONSTRAINT [DF_tblDebitBlock_db_IsAutoEnable] DEFAULT ((0)),
[db_IsUnblocked] [bit] NOT NULL CONSTRAINT [DF_tblDebitBlock_db_IsUnblocked] DEFAULT ((0)),
[db_DebitCompany] [int] NOT NULL CONSTRAINT [DF_tblDebitBlock_db_DebitCompany] DEFAULT ((0)),
[db_DebitTerminalNumber] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitBlock_db_DebitTerminalNumber] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDebitBlock] ADD CONSTRAINT [PK_tblDebitBlock] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Debit company block', 'SCHEMA', N'dbo', 'TABLE', N'tblDebitBlock', NULL, NULL
GO
