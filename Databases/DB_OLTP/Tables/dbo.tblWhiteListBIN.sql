CREATE TABLE [dbo].[tblWhiteListBIN]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[BIN] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWhiteListBIN_BIN] DEFAULT (''),
[PrimaryID] [int] NOT NULL CONSTRAINT [DF_tblWhiteListBIN_PrimaryID] DEFAULT ((0)),
[LastTransPass] [int] NOT NULL CONSTRAINT [DF_tblWhiteListBIN_LastTransPass] DEFAULT ((0)),
[LastTransPassDate] [datetime] NOT NULL CONSTRAINT [DF_tblWhiteListBIN_LastTransPassDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblWhiteListBIN] ADD CONSTRAINT [PK_tblWhiteListBIN] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Credit card BIN white list', 'SCHEMA', N'dbo', 'TABLE', N'tblWhiteListBIN', NULL, NULL
GO
