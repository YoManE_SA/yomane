CREATE TABLE [List].[ActiveStatus]
(
[ActiveStatus_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[ActiveStatus] ADD CONSTRAINT [PK_ActiveStatus] PRIMARY KEY CLUSTERED  ([ActiveStatus_id]) ON [PRIMARY]
GO
