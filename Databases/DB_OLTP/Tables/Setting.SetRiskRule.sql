CREATE TABLE [Setting].[SetRiskRule]
(
[SetRiskRule_id] [int] NOT NULL IDENTITY(1, 1),
[Account_id] [int] NULL,
[RuleName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[RuleSetting] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetRiskRule] ADD CONSTRAINT [PK_SetRiskRule] PRIMARY KEY CLUSTERED  ([SetRiskRule_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetRiskRule] ADD CONSTRAINT [FK_SetRiskRule_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Risk rules set for account transactions, When account is NULL it is a global rule', 'SCHEMA', N'Setting', 'TABLE', N'SetRiskRule', NULL, NULL
GO
