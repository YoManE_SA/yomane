CREATE TABLE [Data].[Customer]
(
[Customer_id] [int] NOT NULL IDENTITY(1, 1),
[ActiveStatus_id] [tinyint] NOT NULL,
[ApplicationIdentity_id] [int] NULL,
[CustomerNumber] [char] (7) COLLATE Hebrew_CI_AS NOT NULL,
[RegistrationDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_Customer_RegistrationDate] DEFAULT (sysdatetime()),
[RulesApproveDate] [datetime2] (0) NULL,
[FirstName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[PersonalNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[PhoneNumber] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[CellNumber] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[DateOfBirth] [date] NULL,
[EmailAddress] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[EmailToken] [uniqueidentifier] NULL,
[Pincode] [char] (4) COLLATE Hebrew_CI_AS NULL,
[FacebookUserID] [varchar] (20) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[Customer] ADD CONSTRAINT [PK_tblCustomer] PRIMARY KEY CLUSTERED  ([Customer_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[Customer] ADD CONSTRAINT [FK_Customer_ActiveStatus] FOREIGN KEY ([ActiveStatus_id]) REFERENCES [List].[ActiveStatus] ([ActiveStatus_id])
GO
ALTER TABLE [Data].[Customer] ADD CONSTRAINT [FK_Customer_ApplicationIdentityID] FOREIGN KEY ([ApplicationIdentity_id]) REFERENCES [Data].[ApplicationIdentity] ([ApplicationIdentity_id]) ON DELETE SET NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Customer data', 'SCHEMA', N'Data', 'TABLE', N'Customer', NULL, NULL
GO
