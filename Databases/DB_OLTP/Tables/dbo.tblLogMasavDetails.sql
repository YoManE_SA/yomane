CREATE TABLE [dbo].[tblLogMasavDetails]
(
[logMasavDetails_id] [int] NOT NULL IDENTITY(1, 1),
[logMasavFile_id] [int] NOT NULL CONSTRAINT [DF_tblLogMasavDetails_logMasavFile_id] DEFAULT ((0)),
[Company_id] [int] NOT NULL CONSTRAINT [DF_tblLogMasavDetails_Company_id] DEFAULT ((0)),
[WireMoney_id] [int] NOT NULL CONSTRAINT [DF_tblLogMasavDetails_WireMoney_id] DEFAULT ((0)),
[PayeeName] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL,
[PayeeBankDetails] [nvarchar] (2000) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLogMasavDetails_PayeeBankDetails] DEFAULT (''),
[Amount] [float] NOT NULL CONSTRAINT [DF_tblLogMasavDetails_Amount] DEFAULT ((0)),
[Currency] [tinyint] NOT NULL CONSTRAINT [DF_tblLogMasavDetails_Currency] DEFAULT ((0)),
[LogStatus] [tinyint] NOT NULL CONSTRAINT [DF_tblLogMasavDetails_LogStatus] DEFAULT ((0)),
[SendStatus] [tinyint] NOT NULL CONSTRAINT [DF_tblLogMasavDetails_SendStatus] DEFAULT ((0)),
[StatusNote] [nvarchar] (255) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLogMasavDetails_StatusNote] DEFAULT (''),
[StatusUserName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblLogMasavDetails_StatusUserName] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogMasavDetails] ADD CONSTRAINT [PK_tblLogMasavDetails] PRIMARY KEY CLUSTERED  ([logMasavDetails_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLogMasavDetails_Company_id] ON [dbo].[tblLogMasavDetails] ([Company_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLogMasavDetails_logMasavFile_id] ON [dbo].[tblLogMasavDetails] ([logMasavFile_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblLogMasavDetails_WireMoney_id] ON [dbo].[tblLogMasavDetails] ([WireMoney_id]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log money wired to the bank per merchant''s bank', 'SCHEMA', N'dbo', 'TABLE', N'tblLogMasavDetails', NULL, NULL
GO
