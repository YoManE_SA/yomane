CREATE TABLE [dbo].[tblBillingCompanys]
(
[BillingCompanys_id] [int] NOT NULL IDENTITY(1, 1),
[LanguageShow] [nvarchar] (3) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingCompanys_LanguageShow] DEFAULT (''),
[currencyShow] [int] NOT NULL,
[name] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingCompanys_name] DEFAULT (''),
[address] [nvarchar] (500) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingCompanys_address] DEFAULT (''),
[number] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingCompanys_number] DEFAULT (''),
[email] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblBillingCompanys_email] DEFAULT (''),
[VATamount] [float] NOT NULL CONSTRAINT [DF_tblBillingCompanys_VATamount] DEFAULT ((0)),
[IsDefault] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillingCompanys] ADD CONSTRAINT [PK_tblBillingCompanys] PRIMARY KEY CLUSTERED  ([BillingCompanys_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillingCompanys] ADD CONSTRAINT [FK_tblBillingCompanys_tblSystemCurrencies_currencyShow] FOREIGN KEY ([currencyShow]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'[NeedRename] Holds invoice issuing company info, currently in use', 'SCHEMA', N'dbo', 'TABLE', N'tblBillingCompanys', NULL, NULL
GO
