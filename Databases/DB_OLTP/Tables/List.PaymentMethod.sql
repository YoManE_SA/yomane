CREATE TABLE [List].[PaymentMethod]
(
[PaymentMethod_id] [smallint] NOT NULL,
[PaymentMethodGroup_id] [tinyint] NULL,
[pm_Type] [int] NULL,
[Name] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[Abbreviation] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[IsBillingAddressMandatory] [bit] NOT NULL,
[IsPopular] [bit] NOT NULL,
[IsPull] [bit] NOT NULL,
[IsPMInfoMandatory] [bit] NOT NULL,
[IsTerminalRequired] [bit] NOT NULL,
[IsExpirationDateMandatory] [bit] NOT NULL CONSTRAINT [DF__PaymentMe__IsExp__26D7B19B] DEFAULT ((0)),
[Value1EncryptedCaption] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[Value2EncryptedCaption] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[Value1EncryptedValidationRegex] [varchar] (30) COLLATE Hebrew_CI_AS NULL,
[Value2EncryptedValidationRegex] [varchar] (30) COLLATE Hebrew_CI_AS NULL,
[PaymentMethodType_id] [tinyint] NULL,
[IsPersonalIDRequired] [bit] NOT NULL CONSTRAINT [DF_PaymentMethod_IsPersonalIDRequired] DEFAULT ((0)),
[PendingKeepAliveMinutes] [int] NULL,
[PendingCleanupDays] [smallint] NULL,
[BaseBIN] [varchar] (10) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[PaymentMethod] ADD CONSTRAINT [PK_PaymentMethod] PRIMARY KEY CLUSTERED  ([PaymentMethod_id]) ON [PRIMARY]
GO
ALTER TABLE [List].[PaymentMethod] ADD CONSTRAINT [UNC_PaymentMethod_Abbreviation] UNIQUE NONCLUSTERED  ([Abbreviation]) ON [PRIMARY]
GO
ALTER TABLE [List].[PaymentMethod] ADD CONSTRAINT [FK_PaymentMethod_PaymentMethodGroup_PaymentMethodGroup_id] FOREIGN KEY ([PaymentMethodGroup_id]) REFERENCES [List].[PaymentMethodGroup] ([PaymentMethodGroup_id])
GO
