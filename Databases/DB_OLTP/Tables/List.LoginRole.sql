CREATE TABLE [List].[LoginRole]
(
[LoginRole_id] [tinyint] NOT NULL,
[ParentID] [tinyint] NULL,
[Name] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[IsLoginWithEmail] [bit] NOT NULL,
[IsLoginWithUsername] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[LoginRole] ADD CONSTRAINT [PK_LoginRole] PRIMARY KEY CLUSTERED  ([LoginRole_id]) ON [PRIMARY]
GO
