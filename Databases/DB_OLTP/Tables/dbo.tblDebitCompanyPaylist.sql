CREATE TABLE [dbo].[tblDebitCompanyPaylist]
(
[DebitCompanyPaylist_id] [int] NOT NULL IDENTITY(1, 1),
[DCP_DebitCompany_id] [int] NOT NULL CONSTRAINT [DF_tblDebitCompanyPaylist_DCP_DebitCompany_id] DEFAULT ((0)),
[DCP_Date] [datetime] NOT NULL CONSTRAINT [DF_tblDebitCompanyPaylist_DCP_Date] DEFAULT (getdate()),
[DCP_PayedAmount] [money] NOT NULL CONSTRAINT [DF_tblDebitCompanyPaylist_DCP_PayedAmount] DEFAULT ((0)),
[DCP_PayedTo] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyPaylist_DCP_PayedTo] DEFAULT ((0)),
[DCP_Description] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyPaylist_DCP_Description] DEFAULT (''),
[DCP_PayedCurrency] [tinyint] NOT NULL CONSTRAINT [DF_tblDebitCompanyPaylist_DCP_PayedCurrency] DEFAULT ((0)),
[DCP_SwiftDate] [datetime] NOT NULL CONSTRAINT [DF_tblDebitCompanyPaylist_DCP_SwiftDate] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDebitCompanyPaylist] ADD CONSTRAINT [PK_tblDebitCompanyPaylist] PRIMARY KEY CLUSTERED  ([DebitCompanyPaylist_id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblDebitCompanyPaylist', NULL, NULL
GO
