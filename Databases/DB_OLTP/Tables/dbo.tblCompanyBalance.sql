CREATE TABLE [dbo].[tblCompanyBalance]
(
[companyBalance_id] [int] NOT NULL IDENTITY(1, 1),
[sourceTbl_id] [int] NOT NULL CONSTRAINT [DF_tblCompanyBalance_sourceTbl_id] DEFAULT ((0)),
[sourceType] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyBalance_sourceType] DEFAULT ((0)),
[sourceInfo] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyBalance_sourceInfo] DEFAULT (''),
[insertDate] [datetime] NOT NULL CONSTRAINT [DF_tblCompanyBalance_insertDate] DEFAULT (getdate()),
[amount] [money] NOT NULL CONSTRAINT [DF_tblCompanyBalance_amount] DEFAULT ((0)),
[status] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanyBalance_status] DEFAULT ((0)),
[comment] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyBalance_comment] DEFAULT (''),
[BalanceExpected] [money] NOT NULL CONSTRAINT [DF_tblCompanyBalance_BalanceExpected] DEFAULT ((0)),
[BalanceCurrent] [money] NOT NULL CONSTRAINT [DF_tblCompanyBalance_BalanceCurrent] DEFAULT ((0)),
[company_id] [int] NULL,
[currency] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trgCompanyBalanceRecalcBalance] ON [dbo].[tblCompanyBalance] AFTER INSERT, UPDATE AS
BEGIN
	UPDATE
		tblCompanyBalance
	SET
		BalanceExpected=
			(
				SELECT
					Sum(amount)
				FROM
					tblCompanyBalance t
				WHERE
					t.company_id=tblCompanyBalance.company_id
					AND
					t.currency=tblCompanyBalance.currency
					AND
					t.companyBalance_ID<=tblCompanyBalance.companyBalance_ID
			),
		BalanceCurrent=
			(
				SELECT
					Sum(amount)
				FROM
					tblCompanyBalance t
				WHERE
					t.company_id=tblCompanyBalance.company_id
					AND
					t.currency=tblCompanyBalance.currency
					AND
					t.companyBalance_ID<=tblCompanyBalance.companyBalance_ID
					AND
					t.status<>1
			)
	WHERE
		companyBalance_ID>=(SELECT Min(companyBalance_ID) FROM Inserted)
		AND
		currency in (SELECT DISTINCT currency FROM Inserted)
		AND
		company_id in (SELECT DISTINCT company_id FROM Inserted)
END
GO
ALTER TABLE [dbo].[tblCompanyBalance] ADD CONSTRAINT [PK_tblCompanyBalance] PRIMARY KEY CLUSTERED  ([companyBalance_id] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyBalance_company_id_currency] ON [dbo].[tblCompanyBalance] ([company_id], [currency]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyBalance] ADD CONSTRAINT [FK_tblCompanyBalance_tblCompany_company_id] FOREIGN KEY ([company_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompanyBalance] ADD CONSTRAINT [FK_tblCompanyBalance_tblSystemCurrencies_currency] FOREIGN KEY ([currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'[Not Used]', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyBalance', NULL, NULL
GO
