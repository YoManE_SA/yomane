CREATE TABLE [List].[InvoiceProvider]
(
[InvoiceProvider_id] [tinyint] NOT NULL,
[Name] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[InvoiceProvider] ADD CONSTRAINT [PK_InvoiceProvider] PRIMARY KEY CLUSTERED  ([InvoiceProvider_id]) ON [PRIMARY]
GO
