CREATE TABLE [Data].[ProductTag]
(
[ProductTag_id] [int] NOT NULL IDENTITY(1, 1),
[Product_id] [int] NOT NULL,
[Tag] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductTag] ADD CONSTRAINT [PK_ProductTag] PRIMARY KEY CLUSTERED  ([ProductTag_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductTag] ADD CONSTRAINT [FK_ProductTag_Product] FOREIGN KEY ([Product_id]) REFERENCES [Data].[Product] ([Product_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s product''s tag', 'SCHEMA', N'Data', 'TABLE', N'ProductTag', NULL, NULL
GO
