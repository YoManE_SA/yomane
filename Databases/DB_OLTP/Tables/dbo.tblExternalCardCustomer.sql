CREATE TABLE [dbo].[tblExternalCardCustomer]
(
[ExternalCardCustomer_id] [int] NOT NULL IDENTITY(1, 1),
[ExternalCardTerminal_id] [int] NOT NULL,
[Merchant_id] [int] NOT NULL,
[UniqueID] [uniqueidentifier] NOT NULL,
[Email] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL,
[Status] [smallint] NOT NULL,
[Comment] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[ExtraData] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExternalCardCustomer] ADD CONSTRAINT [PK_tblExternalCardCustomer] PRIMARY KEY CLUSTERED  ([ExternalCardCustomer_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExternalCardCustomer] ADD CONSTRAINT [FK_tblExternalCardCustomer_tblExternalCardTerminal_ExternalCardTerminal_id] FOREIGN KEY ([ExternalCardTerminal_id]) REFERENCES [dbo].[tblExternalCardTerminal] ([ExternalCardTerminal_id])
GO
ALTER TABLE [dbo].[tblExternalCardCustomer] ADD CONSTRAINT [FK_tblExternalCardCustomer_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'-', 'SCHEMA', N'dbo', 'TABLE', N'tblExternalCardCustomer', NULL, NULL
GO
