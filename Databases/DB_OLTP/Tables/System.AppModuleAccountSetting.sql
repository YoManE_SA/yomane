CREATE TABLE [System].[AppModuleAccountSetting]
(
[AppModuleAccountSetting_id] [int] NOT NULL IDENTITY(1, 1),
[AppModule_id] [int] NOT NULL,
[ValueName] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL,
[Value] [varchar] (250) COLLATE Hebrew_CI_AS NOT NULL,
[Account_id] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[AppModuleAccountSetting] ADD CONSTRAINT [PK_AppModuleAccountSetting] PRIMARY KEY CLUSTERED  ([AppModuleAccountSetting_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[AppModuleAccountSetting] ADD CONSTRAINT [UIX_AppModuleAccountSetting_AppModuleID_ValueName_AccountID] UNIQUE NONCLUSTERED  ([AppModule_id], [ValueName], [Account_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[AppModuleAccountSetting] ADD CONSTRAINT [FK_AppModuleAccountSetting_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [System].[AppModuleAccountSetting] ADD CONSTRAINT [FK_AppModuleAccountSetting_AppModuleID] FOREIGN KEY ([AppModule_id]) REFERENCES [System].[AppModule] ([AppModule_id])
GO
