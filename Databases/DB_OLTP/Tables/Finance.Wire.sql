CREATE TABLE [Finance].[Wire]
(
[Wire_id] [int] NOT NULL IDENTITY(1, 1),
[WireProvider_id] [varchar] (16) COLLATE Hebrew_CI_AS NULL,
[WireBatch_id] [int] NULL,
[Account_id] [int] NOT NULL,
[AccountPayee_id] [int] NULL,
[MerchantSettlement_id] [int] NULL,
[AffiliateSettlement_id] [int] NULL,
[IsShow] [bit] NOT NULL,
[Amount] [decimal] (19, 2) NOT NULL,
[AmountProcessed] [decimal] (19, 2) NOT NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[CurrencyISOCodeProcessed] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[CreateDate] [datetime2] (0) NOT NULL,
[WireDate] [datetime2] (0) NULL,
[WireFee] [decimal] (9, 2) NOT NULL,
[WireStatus] [tinyint] NOT NULL,
[ActionUser] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[ActionDate] [datetime2] (0) NULL,
[IsApproveLevel1] [bit] NULL,
[IsApproveLevel2] [bit] NULL,
[ProviderReference1] [varchar] (40) COLLATE Hebrew_CI_AS NULL,
[TargetTitle] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[TargetBankAccountText] [varchar] (500) COLLATE Hebrew_CI_AS NULL,
[Comment] [nvarchar] (1000) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Finance].[Wire] ADD CONSTRAINT [PK_Wire] PRIMARY KEY CLUSTERED  ([Wire_id] DESC) WITH (FILLFACTOR=95) ON [PRIMARY]
GO
ALTER TABLE [Finance].[Wire] ADD CONSTRAINT [FK_Wire_AccountrID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Finance].[Wire] ADD CONSTRAINT [FK_Wire_AccountPayeeID] FOREIGN KEY ([AccountPayee_id]) REFERENCES [Data].[AccountPayee] ([AccountPayee_id])
GO
ALTER TABLE [Finance].[Wire] ADD CONSTRAINT [FK_Wire_AffiliateSettlementID] FOREIGN KEY ([AffiliateSettlement_id]) REFERENCES [dbo].[tblAffiliatePayments] ([AFP_ID])
GO
ALTER TABLE [Finance].[Wire] ADD CONSTRAINT [FK_Wire_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Finance].[Wire] ADD CONSTRAINT [FK_Wire_CurrencyISOCodeProcessed] FOREIGN KEY ([CurrencyISOCodeProcessed]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Finance].[Wire] ADD CONSTRAINT [FK_Wire_MerchantSettlementID] FOREIGN KEY ([MerchantSettlement_id]) REFERENCES [dbo].[tblTransactionPay] ([id])
GO
ALTER TABLE [Finance].[Wire] ADD CONSTRAINT [FK_Wire_WireBatchID] FOREIGN KEY ([WireBatch_id]) REFERENCES [Finance].[WireBatch] ([WireBatch_id])
GO
ALTER TABLE [Finance].[Wire] ADD CONSTRAINT [FK_Wire_WireProviderID] FOREIGN KEY ([WireProvider_id]) REFERENCES [List].[WireProvider] ([WireProvider_id])
GO
