CREATE TABLE [List].[ProductType]
(
[ProductType_id] [tinyint] NOT NULL,
[Name] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[ProductType] ADD CONSTRAINT [PK_ProductType] PRIMARY KEY CLUSTERED  ([ProductType_id]) ON [PRIMARY]
GO
