CREATE TABLE [dbo].[tblDebitCompanyFees]
(
[DCF_ID] [int] NOT NULL IDENTITY(1, 1),
[DCF_DebitCompanyID] [int] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_DebitCompanyID] DEFAULT ((0)),
[DCF_CurrencyID] [tinyint] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_CurrencyID] DEFAULT ((0)),
[DCF_PaymentMethod] [smallint] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_PaymentMethod] DEFAULT ((0)),
[DCF_TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_TerminalNumber] DEFAULT (''),
[DCF_FixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_FixedFee] DEFAULT ((0)),
[DCF_RefundFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_RefundFixedFee] DEFAULT ((0)),
[DCF_PercentFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_PercentFee] DEFAULT ((0)),
[DCF_ApproveFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_ApproveFixedFee] DEFAULT ((0)),
[DCF_ClarificationFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_ClarificationFee] DEFAULT ((0)),
[DCF_CBFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_CBFixedFee] DEFAULT ((0)),
[DCF_FailFixedFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_FailFixedFee] DEFAULT ((0)),
[DCF_RegisterFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_RegisterFee] DEFAULT ((0)),
[DCF_YearFee] [smallmoney] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_YearFee] DEFAULT ((0)),
[DCF_StatIncDays] [tinyint] NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_StatIncDays] DEFAULT ((0)),
[DCF_PayTransDays] [varchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_PayTransDays] DEFAULT (''),
[DCF_PayINDays] [varchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_PayINDays] DEFAULT (''),
[DCF_FixedCurrency] [tinyint] NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_FixedCurrency] DEFAULT ((0)),
[DCF_CHBCurrency] [tinyint] NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_CHBCurrency] DEFAULT ((0)),
[DCF_MinPrecFee] [smallmoney] NULL CONSTRAINT [DF_tblDebitCompanyFees_DCF_MinPrecFee] DEFAULT ((0)),
[DCF_MaxPrecFee] [smallmoney] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDebitCompanyFees] ADD CONSTRAINT [PK_tblDebitCompanyFees] PRIMARY KEY CLUSTERED  ([DCF_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Debit companies'' fees', 'SCHEMA', N'dbo', 'TABLE', N'tblDebitCompanyFees', NULL, NULL
GO
