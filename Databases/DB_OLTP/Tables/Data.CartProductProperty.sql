CREATE TABLE [Data].[CartProductProperty]
(
[CartProductProperty_id] [int] NOT NULL IDENTITY(1, 1),
[CartProduct_id] [int] NOT NULL,
[ProductProperty_id] [int] NULL,
[PropertyName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL,
[PropertyValue] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[CartProductProperty] ADD CONSTRAINT [PK_CartProductProperty] PRIMARY KEY CLUSTERED  ([CartProductProperty_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[CartProductProperty] ADD CONSTRAINT [FK_CartProductProperty_CartProduct_CartProductID] FOREIGN KEY ([CartProduct_id]) REFERENCES [Data].[CartProduct] ([CartProduct_id])
GO
ALTER TABLE [Data].[CartProductProperty] ADD CONSTRAINT [FK_CartProductProperty_ProductProperty_ProductPropertyID] FOREIGN KEY ([ProductProperty_id]) REFERENCES [Data].[ProductProperty] ([ProductProperty_id])
GO
