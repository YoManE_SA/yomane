CREATE TABLE [dbo].[tblSecurityDocumentGroup]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[sdg_Document] [int] NOT NULL CONSTRAINT [DF_tblSecurityDocumentGroup_sdg_Document] DEFAULT ((0)),
[sdg_Group] [int] NOT NULL CONSTRAINT [DF_tblSecurityDocumentGroup_sdg_Group] DEFAULT ((0)),
[sdg_IsVisible] [bit] NOT NULL CONSTRAINT [DF_tblSecurityDocumentGroup_sdg_IsVisible] DEFAULT ((0)),
[sdg_IsActive] [bit] NOT NULL CONSTRAINT [DF_tblSecurityDocumentGroup_sdg_IsActive] DEFAULT ((0)),
[sdg_GroupID] AS (case when [sdg_Group]>(0) then [sdg_Group]  end) PERSISTED,
[sdg_UserID] AS (case when [sdg_Group]<(0) then  -[sdg_Group]  end) PERSISTED
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSecurityDocumentGroup] ADD CONSTRAINT [PK_tblSecurityDocumentGroup] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSecurityDocumentGroup] ADD CONSTRAINT [FK_tblSecurityDocumentGroup_tblSecurityDocument_sdg_Document] FOREIGN KEY ([sdg_Document]) REFERENCES [dbo].[tblSecurityDocument] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblSecurityDocumentGroup] ADD CONSTRAINT [FK_tblSecurityDocumentGroup_tblSecurityGroup_sdg_GroupID] FOREIGN KEY ([sdg_GroupID]) REFERENCES [dbo].[tblSecurityGroup] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblSecurityDocumentGroup] ADD CONSTRAINT [FK_tblSecurityDocumentGroup_tblSecurityUser_sdg_UserID] FOREIGN KEY ([sdg_UserID]) REFERENCES [dbo].[tblSecurityUser] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds Admincash pages to group (Replace by Admin new security method)', 'SCHEMA', N'dbo', 'TABLE', N'tblSecurityDocumentGroup', NULL, NULL
GO
