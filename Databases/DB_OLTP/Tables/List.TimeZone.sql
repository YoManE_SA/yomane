CREATE TABLE [List].[TimeZone]
(
[TimeZoneOffsetMinutes] [smallint] NOT NULL,
[Name] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[TimeZone] ADD CONSTRAINT [PK_TimeZone] PRIMARY KEY CLUSTERED  ([TimeZoneOffsetMinutes]) ON [PRIMARY]
GO
