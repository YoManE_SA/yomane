CREATE TABLE [dbo].[tblSecurityUserMerchant]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[sum_User] [int] NOT NULL CONSTRAINT [DF_tblSecurityUserMerchant_sum_User] DEFAULT ((0)),
[sum_Merchant] [int] NOT NULL CONSTRAINT [DF_tblSecurityUserMerchant_sum_Merchant] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSecurityUserMerchant] ADD CONSTRAINT [PK_tblSecurityUserMerchant] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblSecurityUserMerchant_User_Merchant] ON [dbo].[tblSecurityUserMerchant] ([sum_User], [sum_Merchant]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSecurityUserMerchant] ADD CONSTRAINT [FK_tblSecurityUserMerchant_tblCompany_sum_Merchant] FOREIGN KEY ([sum_Merchant]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblSecurityUserMerchant] ADD CONSTRAINT [FK_tblSecurityUserMerchant_tblSecurityUser_sum_User] FOREIGN KEY ([sum_User]) REFERENCES [dbo].[tblSecurityUser] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds Admincash security user to merchant (Replace by Admin new security method)', 'SCHEMA', N'dbo', 'TABLE', N'tblSecurityUserMerchant', NULL, NULL
GO
