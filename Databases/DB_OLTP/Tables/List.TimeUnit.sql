CREATE TABLE [List].[TimeUnit]
(
[TimeUnit_id] [tinyint] NOT NULL,
[Name] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[TimeUnit] ADD CONSTRAINT [PK_TimeUnit] PRIMARY KEY CLUSTERED  ([TimeUnit_id]) ON [PRIMARY]
GO
