CREATE TABLE [dbo].[tblSecurityUserDebitCompany]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[sudc_User] [int] NOT NULL CONSTRAINT [DF_tblSecurityUserDebitCompany_sudc_User] DEFAULT ((0)),
[sudc_DebitCompany] [int] NOT NULL CONSTRAINT [DF_tblSecurityUserDebitCompany_sudc_DebitCompany] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSecurityUserDebitCompany] ADD CONSTRAINT [PK_tblSecurityUserDebitCompany] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSecurityUserDebitCompany] ADD CONSTRAINT [FK_tblSecurityUserDebitCompany_tblDebitCompany_sudc_DebitCompany] FOREIGN KEY ([sudc_DebitCompany]) REFERENCES [dbo].[tblDebitCompany] ([DebitCompany_ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblSecurityUserDebitCompany] ADD CONSTRAINT [FK_tblSecurityUserDebitCompany_tblSecurityUser_sudc_User] FOREIGN KEY ([sudc_User]) REFERENCES [dbo].[tblSecurityUser] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds Admincash security user to debit company (Replace by Admin new security method)', 'SCHEMA', N'dbo', 'TABLE', N'tblSecurityUserDebitCompany', NULL, NULL
GO
