CREATE TABLE [dbo].[tblRecurringSeries]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[rs_Company] [int] NULL CONSTRAINT [DF_tblRecurringSeries_rs_Company] DEFAULT ((0)),
[rs_SeriesNumber] [int] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_SeriesNumber] DEFAULT ((0)),
[rs_ChargeCount] [int] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_ChargeCount] DEFAULT ((0)),
[rs_IntervalUnit] [int] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_IntervalUnit] DEFAULT ((0)),
[rs_IntervalLength] [int] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_IntervalLength] DEFAULT ((0)),
[rs_StartDate] [datetime] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_StartDate] DEFAULT (getdate()),
[rs_CreditCard] [int] NULL CONSTRAINT [DF_tblRecurringSeries_rs_CreditCard] DEFAULT ((0)),
[rs_ECheck] [int] NULL CONSTRAINT [DF_tblRecurringSeries_rs_ECheck] DEFAULT ((0)),
[rs_Comments] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_Comments] DEFAULT (''),
[rs_Suspended] [bit] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_Suspended] DEFAULT ((0)),
[rs_Blocked] [bit] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_Blocked] DEFAULT ((0)),
[rs_Paid] [bit] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_Paid] DEFAULT ((0)),
[rs_ChargeAmount] [money] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_ChargeAmount] DEFAULT ((0)),
[rs_Currency] [int] NULL,
[rs_Deleted] [bit] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_Deleted] DEFAULT ((0)),
[rs_Approval] [bit] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_Approval] DEFAULT ((0)),
[rs_Flexible] [bit] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_Flexible] DEFAULT ((0)),
[rs_SeriesTotal] [money] NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_SeriesTotal] DEFAULT ((0)),
[rs_ReqTrmCode] [int] NULL,
[rs_IP] [varchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblRecurringSeries_rs_IP] DEFAULT (''),
[rs_FirstApproval] [bit] NULL,
[rs_Identifier] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[rs_IsPrecreated] [bit] NULL,
[TransPaymentMethod_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRecurringSeries] ADD CONSTRAINT [PK_tblRecurringSeries] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblRecurringSeries_CreditCard] ON [dbo].[tblRecurringSeries] ([rs_CreditCard] DESC) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRecurringSeries] ADD CONSTRAINT [FK_tblRecurringSeries_tblCompany_rs_Company] FOREIGN KEY ([rs_Company]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [dbo].[tblRecurringSeries] ADD CONSTRAINT [FK_tblRecurringSeries_tblCreditCard_rs_CreditCard] FOREIGN KEY ([rs_CreditCard]) REFERENCES [dbo].[tblCreditCard] ([ID]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblRecurringSeries] ADD CONSTRAINT [FK_tblRecurringSeries_tblSystemCurrencies_rs_Currency] FOREIGN KEY ([rs_Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
ALTER TABLE [dbo].[tblRecurringSeries] ADD CONSTRAINT [FK_tblRecurringSeries_tblCheckDetails_rs_ECheck] FOREIGN KEY ([rs_ECheck]) REFERENCES [dbo].[tblCheckDetails] ([id]) ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblRecurringSeries] ADD CONSTRAINT [FK_RecurringSeries_TransPaymentMethod] FOREIGN KEY ([TransPaymentMethod_id]) REFERENCES [Trans].[TransPaymentMethod] ([TransPaymentMethod_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s recurring series', 'SCHEMA', N'dbo', 'TABLE', N'tblRecurringSeries', NULL, NULL
GO
