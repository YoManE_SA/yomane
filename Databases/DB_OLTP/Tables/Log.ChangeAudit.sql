CREATE TABLE [Log].[ChangeAudit]
(
[ChangeAudit_id] [int] NOT NULL IDENTITY(1, 1),
[InsertDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_ChangeAudit_InsertDate] DEFAULT (sysdatetime()),
[ActionType] [varchar] (10) COLLATE Hebrew_CI_AS NULL,
[ObjectLocation] [varchar] (100) COLLATE Hebrew_CI_AS NULL,
[ObjectName] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[ValueOld] [varchar] (1000) COLLATE Hebrew_CI_AS NULL,
[ValueNew] [varchar] (1000) COLLATE Hebrew_CI_AS NULL,
[InitiatedBy] [varchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Log].[ChangeAudit] ADD CONSTRAINT [PK_ChangeAudit] PRIMARY KEY CLUSTERED  ([ChangeAudit_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Auditing admin changes', 'SCHEMA', N'Log', 'TABLE', N'ChangeAudit', NULL, NULL
GO
