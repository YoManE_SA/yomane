CREATE TABLE [dbo].[tblAffiliateBankAccount]
(
[aba_Affiliate] [int] NOT NULL,
[aba_Currency] [int] NOT NULL,
[aba_ABA] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_ABA] DEFAULT (''),
[aba_ABA2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_ABA2] DEFAULT (''),
[aba_AccountName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_AccountName] DEFAULT (''),
[aba_AccountName2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_AccountName2] DEFAULT (''),
[aba_AccountNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_AccountNumber] DEFAULT (''),
[aba_AccountNumber2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_AccountNumber2] DEFAULT (''),
[aba_BankAddress] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddress] DEFAULT (''),
[aba_BankAddress2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddress2] DEFAULT (''),
[aba_BankAddressCity] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddressCity] DEFAULT (''),
[aba_BankAddressCity2] [nvarchar] (30) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddressCity2] DEFAULT (''),
[aba_BankAddressCountry] [int] NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddressCountry] DEFAULT ((0)),
[aba_BankAddressCountry2] [int] NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddressCountry2] DEFAULT ((0)),
[aba_BankAddressSecond] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddressSecond] DEFAULT (''),
[aba_BankAddressSecond2] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddressSecond2] DEFAULT (''),
[aba_BankAddressState] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddressState] DEFAULT (''),
[aba_BankAddressState2] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddressState2] DEFAULT (''),
[aba_BankAddressZip] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddressZip] DEFAULT (''),
[aba_BankAddressZip2] [nvarchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankAddressZip2] DEFAULT (''),
[aba_BankName] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankName] DEFAULT (''),
[aba_BankName2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_BankName2] DEFAULT (''),
[aba_IBAN] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_IBAN] DEFAULT (''),
[aba_IBAN2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_IBAN2] DEFAULT (''),
[aba_SepaBic] [varchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_SepaBic] DEFAULT (''),
[aba_SepaBic2] [varchar] (11) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_SepaBic2] DEFAULT (''),
[aba_SortCode] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_SortCode] DEFAULT (''),
[aba_SortCode2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_SortCode2] DEFAULT (''),
[aba_SwiftNumber] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_SwiftNumber] DEFAULT (''),
[aba_SwiftNumber2] [nvarchar] (80) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblAffiliateBankAccount_aba_SwiftNumber2] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAffiliateBankAccount] ADD CONSTRAINT [PK_tblAffiliateBankAccount] PRIMARY KEY CLUSTERED  ([aba_Affiliate], [aba_Currency]) ON [PRIMARY]
GO
