CREATE TABLE [Data].[AccountAddress]
(
[AccountAddress_id] [int] NOT NULL IDENTITY(1, 1),
[Street1] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Street2] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[City] [nvarchar] (60) COLLATE Hebrew_CI_AS NULL,
[PostalCode] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[StateISOCode] [char] (2) COLLATE Hebrew_CI_AS NULL,
[CountryISOCode] [char] (2) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountAddress] ADD CONSTRAINT [PK_AccountAddress] PRIMARY KEY CLUSTERED  ([AccountAddress_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountAddress] ADD CONSTRAINT [FK_AccountAddress_CountryList_CountryISOCode] FOREIGN KEY ([CountryISOCode]) REFERENCES [List].[CountryList] ([CountryISOCode])
GO
ALTER TABLE [Data].[AccountAddress] ADD CONSTRAINT [FK_AccountAddress_StateList_StateISOCode] FOREIGN KEY ([StateISOCode]) REFERENCES [List].[StateList] ([StateISOCode])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account''s personal address', 'SCHEMA', N'Data', 'TABLE', N'AccountAddress', NULL, NULL
GO
