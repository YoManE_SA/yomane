CREATE TABLE [Data].[AccountExternalService]
(
[AccountExternalService_id] [smallint] NOT NULL IDENTITY(1, 1),
[ExternalServiceType_id] [varchar] (16) COLLATE Hebrew_CI_AS NULL,
[Account_id] [int] NOT NULL,
[ProtocolType_id] [varchar] (10) COLLATE Hebrew_CI_AS NULL,
[IsActive] [bit] NOT NULL,
[ServerURL] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[Username] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[PasswordEncrypted] [varbinary] (100) NULL,
[EncryptionKey] [tinyint] NOT NULL,
[Identifier1] [varchar] (30) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountExternalService] ADD CONSTRAINT [PK_AccountExternalService] PRIMARY KEY CLUSTERED  ([AccountExternalService_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountExternalService] ADD CONSTRAINT [FK_AccountExternalService_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Data].[AccountExternalService] ADD CONSTRAINT [FK_ExternalServiceType_ExternalServiceTypeID] FOREIGN KEY ([ExternalServiceType_id]) REFERENCES [List].[ExternalServiceType] ([ExternalServiceType_id])
GO
ALTER TABLE [Data].[AccountExternalService] ADD CONSTRAINT [FK_ExternalServiceType_ProtocolTypeID] FOREIGN KEY ([ProtocolType_id]) REFERENCES [List].[ProtocolType] ([ProtocolType_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account''s external services info', 'SCHEMA', N'Data', 'TABLE', N'AccountExternalService', NULL, NULL
GO
