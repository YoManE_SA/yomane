CREATE TABLE [Data].[ProductToProductCategory]
(
[Product_id] [int] NOT NULL,
[ProductCategory_id] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductToProductCategory] ADD CONSTRAINT [PK_ProductToProductCategory] PRIMARY KEY CLUSTERED  ([Product_id], [ProductCategory_id]) ON [PRIMARY]
GO
ALTER TABLE [Data].[ProductToProductCategory] ADD CONSTRAINT [FK_ProductToProductCategory_Product] FOREIGN KEY ([Product_id]) REFERENCES [Data].[Product] ([Product_id]) ON DELETE CASCADE
GO
ALTER TABLE [Data].[ProductToProductCategory] ADD CONSTRAINT [FK_ProductToProductCategory_ProductCategory] FOREIGN KEY ([ProductCategory_id]) REFERENCES [List].[ProductCategory] ([ProductCategory_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s product''s category', 'SCHEMA', N'Data', 'TABLE', N'ProductToProductCategory', NULL, NULL
GO
