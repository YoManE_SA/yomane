CREATE TABLE [dbo].[tblCompanyChargeAdmin]
(
[company_id] [int] NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_company_id] DEFAULT ((0)),
[isRecurringReply] [bit] NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_isRecurringReply] DEFAULT ((0)),
[recurringReplyUrl] [nvarchar] (250) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_recurringReplyUrl] DEFAULT (''),
[isPendingReply] [bit] NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_isPendingReply] DEFAULT ((0)),
[PendingReplyUrl] [nvarchar] (250) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_PendingReplyUrl] DEFAULT (''),
[WalletReplyURL] [nvarchar] (250) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_WalletReplyURL] DEFAULT (''),
[IsWalletReply] [bit] NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_IsWalletReply] DEFAULT ((0)),
[IsWalletReplyFail] [bit] NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_IsWalletReplyFail] DEFAULT ((0)),
[UseHasKeyInWalletReply] [bit] NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_UseHasKeyInWalletReply] DEFAULT ((0)),
[isRecurringModifyReply] [bit] NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_isRecurringModifyReply] DEFAULT ((0)),
[recurringModifyReplyUrl] [nvarchar] (250) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_recurringModifyReplyUrl] DEFAULT (''),
[isNotifyRefundRequestApproved] [bit] NOT NULL CONSTRAINT [DF_tblCompanyChargeAdmin_isNotifyRefundRequestApproved] DEFAULT ((0)),
[NotifyRefundRequestApprovedUrl] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL,
[NotifyProcessURL] [varchar] (200) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyChargeAdmin] ADD CONSTRAINT [PK_tblCompanyChargeAdmin] PRIMARY KEY CLUSTERED  ([company_id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanyChargeAdmin] ADD CONSTRAINT [FK_tblCompanyChargeAdmin_tblCompany_company_id] FOREIGN KEY ([company_id]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant set URL notidications and if hash key is required', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanyChargeAdmin', NULL, NULL
GO
