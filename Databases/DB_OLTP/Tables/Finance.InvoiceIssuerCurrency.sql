CREATE TABLE [Finance].[InvoiceIssuerCurrency]
(
[InvoiceIssuerCurrency_id] [smallint] NOT NULL IDENTITY(1, 1),
[InvoiceIssuer_id] [tinyint] NULL,
[CurrencySettlement] [smallint] NULL,
[CurrencyInvoice] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [Finance].[InvoiceIssuerCurrency] ADD CONSTRAINT [PK_InvoiceIssuerCurrency] PRIMARY KEY CLUSTERED  ([InvoiceIssuerCurrency_id]) ON [PRIMARY]
GO
