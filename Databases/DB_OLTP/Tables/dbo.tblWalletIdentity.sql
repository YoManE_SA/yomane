CREATE TABLE [dbo].[tblWalletIdentity]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[wi_Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_Name] DEFAULT (''),
[wi_Identity] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_Identity] DEFAULT (''),
[wi_Folder] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_Folder] DEFAULT (''),
[wi_BrandName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_BrandName] DEFAULT (''),
[wi_CompanyName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_CompanyName] DEFAULT (''),
[wi_DevCenterURL] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_DevCenterURL] DEFAULT (''),
[wi_ProcessURL] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_ProcessURL] DEFAULT (''),
[wi_MerchantURL] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_MerchantURL] DEFAULT (''),
[wi_CustomerURL] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_CustomerURL] DEFAULT (''),
[wi_ContentURL] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_ContentURL] DEFAULT (''),
[wi_MerchantUploadsFolder] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_MerchantUploadsFolder] DEFAULT (''),
[wi_MailServer] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_MailServer] DEFAULT (''),
[wi_MailUsername] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_MailUsername] DEFAULT (''),
[wi_MailPassword] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_MailPassword] DEFAULT (''),
[wi_MailFrom] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_MailFrom] DEFAULT (''),
[wi_Currencies] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_Currencies] DEFAULT (''),
[wi_FormsInbox] [nvarchar] (150) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_FormsInbox] DEFAULT (''),
[wi_DatabaseNumber] [int] NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_DatabaseNumber] DEFAULT ((0)),
[wi_MerchantNumber] [int] NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_MerchantNumber] DEFAULT ((0)),
[wi_IsHebrewVisible] [bit] NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_IsHebrewVisible] DEFAULT ((0)),
[wi_IsSendMerchant] [bit] NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_IsSendMerchant] DEFAULT ((0)),
[wi_IsSendCustomer] [bit] NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_IsSendCustomer] DEFAULT ((0)),
[wi_IsDirectEbanking] [bit] NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_IsDirectEbanking] DEFAULT ((0)),
[wi_ContentFolder] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_ContentFolder] DEFAULT (''),
[wi_MailTo] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL CONSTRAINT [DF_tblWalletIdentity_wi_MailTo] DEFAULT (''),
[wi_CopyRight] [nvarchar] (250) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_CopyRight] DEFAULT (''),
[wi_IsHandledByMerchant] [bit] NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_IsHandledByMerchant] DEFAULT ((0)),
[wi_IsContactTransFields] [bit] NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_IsContactTransFields] DEFAULT ((0)),
[wi_isEnable] [bit] NOT NULL CONSTRAINT [DF_tblWalletIdentity_wi_isEnable] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblWalletIdentity] ADD CONSTRAINT [PK_tblWalletIdentity] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Wallet identity for web app', 'SCHEMA', N'dbo', 'TABLE', N'tblWalletIdentity', NULL, NULL
GO
