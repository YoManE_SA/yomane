CREATE TABLE [dbo].[tblInvoiceLine]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[il_DocumentID] [int] NULL,
[il_Date] [datetime] NOT NULL CONSTRAINT [DF_tblInvoiceLine_il_Date] DEFAULT (getdate()),
[il_TransactionPayID] [int] NOT NULL CONSTRAINT [DF_tblInvoiceLine_il_TransactionPayID] DEFAULT ((0)),
[il_Username] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvoiceLine_il_Username] DEFAULT (''),
[il_BillToName] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvoiceLine_il_BillToName] DEFAULT (''),
[il_MerchantID] [int] NOT NULL CONSTRAINT [DF_tblInvoiceLine_il_MerchantID] DEFAULT ((0)),
[il_Text] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL CONSTRAINT [DF_tblInvoiceLine_il_Text] DEFAULT (''),
[il_Quantity] [real] NOT NULL CONSTRAINT [DF_tblInvoiceLine_il_Quantity] DEFAULT ((1)),
[il_Price] [money] NOT NULL CONSTRAINT [DF_tblInvoiceLine_il_Price] DEFAULT ((0)),
[il_Currency] [int] NOT NULL CONSTRAINT [DF_tblInvoiceLine_il_Currency] DEFAULT ((0)),
[il_CurrencyRate] [money] NOT NULL CONSTRAINT [DF_tblInvoiceLine_il_CurrencyRate] DEFAULT ((1)),
[il_Amount] AS (CONVERT([money],[il_Price]*[il_Quantity],0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblInvoiceLine] ADD CONSTRAINT [PK_tblInvoiceLine] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblInvoiceLine] ADD CONSTRAINT [FK_tblInvoiceLine_tblSystemCurrencies_il_Currency] FOREIGN KEY ([il_Currency]) REFERENCES [dbo].[tblSystemCurrencies] ([CUR_ID])
GO
ALTER TABLE [dbo].[tblInvoiceLine] ADD CONSTRAINT [FK_tblInvoiceLine_tblInvoiceDocument_il_DocumentID] FOREIGN KEY ([il_DocumentID]) REFERENCES [dbo].[tblInvoiceDocument] ([ID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Merchant''s invoice line detail', 'SCHEMA', N'dbo', 'TABLE', N'tblInvoiceLine', NULL, NULL
GO
