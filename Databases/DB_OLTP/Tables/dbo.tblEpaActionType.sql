CREATE TABLE [dbo].[tblEpaActionType]
(
[ID] [int] NOT NULL IDENTITY(0, 1),
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL,
[Brief] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEpaActionType] ADD CONSTRAINT [PK_tblEpaActionType] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'EPA/CHB import status', 'SCHEMA', N'dbo', 'TABLE', N'tblEpaActionType', NULL, NULL
GO
