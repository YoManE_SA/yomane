CREATE TABLE [Trans].[PhoneDetail]
(
[PhoneDetail_id] [int] NOT NULL IDENTITY(1, 1),
[PhoneNumber] [varchar] (20) COLLATE Hebrew_CI_AS NOT NULL,
[FullName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Email] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL,
[BillingAddress_id] [int] NULL,
[Merchant_id] [int] NULL,
[PhoneCarrier_id] [smallint] NULL,
[InvoiceName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Trans].[PhoneDetail] ADD CONSTRAINT [PK_PhoneDetail] PRIMARY KEY CLUSTERED  ([PhoneDetail_id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransPhoneDetail_BillingAddress_id] ON [Trans].[PhoneDetail] ([BillingAddress_id]) WHERE ([BillingAddress_id] IS NOT NULL) ON [PRIMARY]
GO
ALTER TABLE [Trans].[PhoneDetail] ADD CONSTRAINT [FK_PhoneDetail_tblBillingAddress_BillingAddress_id] FOREIGN KEY ([BillingAddress_id]) REFERENCES [dbo].[tblBillingAddress] ([id]) ON DELETE SET NULL
GO
ALTER TABLE [Trans].[PhoneDetail] ADD CONSTRAINT [FK_PhoneDetail_tblCompany_Merchant_id] FOREIGN KEY ([Merchant_id]) REFERENCES [dbo].[tblCompany] ([ID])
GO
ALTER TABLE [Trans].[PhoneDetail] ADD CONSTRAINT [FK_PhoneDetail_PhoneCarrier_PhoneCarrier_id] FOREIGN KEY ([PhoneCarrier_id]) REFERENCES [List].[PhoneCarrier] ([PhoneCarrier_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'[ToBeRemoved] Holds phone processed info (Replace by Trans.TransPaymentMethod)', 'SCHEMA', N'Trans', 'TABLE', N'PhoneDetail', NULL, NULL
GO
