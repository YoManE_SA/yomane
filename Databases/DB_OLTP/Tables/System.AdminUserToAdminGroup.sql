CREATE TABLE [System].[AdminUserToAdminGroup]
(
[AdminUser_id] [smallint] NOT NULL,
[AdminGroup_id] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [System].[AdminUserToAdminGroup] ADD CONSTRAINT [PK_AdminUserToRole] PRIMARY KEY CLUSTERED  ([AdminUser_id], [AdminGroup_id]) ON [PRIMARY]
GO
ALTER TABLE [System].[AdminUserToAdminGroup] ADD CONSTRAINT [FK_AdminUserToAdminGroup_AdminGroup_AdminGroupID] FOREIGN KEY ([AdminGroup_id]) REFERENCES [System].[AdminGroup] ([AdminGroup_id])
GO
ALTER TABLE [System].[AdminUserToAdminGroup] ADD CONSTRAINT [FK_AdminUserToAdminGroup_AdminUser_AdminUserID] FOREIGN KEY ([AdminUser_id]) REFERENCES [System].[AdminUser] ([AdminUser_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'System admin users to admin group', 'SCHEMA', N'System', 'TABLE', N'AdminUserToAdminGroup', NULL, NULL
GO
