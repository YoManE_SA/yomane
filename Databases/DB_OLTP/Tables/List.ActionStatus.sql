CREATE TABLE [List].[ActionStatus]
(
[ActionStatus_id] [tinyint] NOT NULL,
[Name] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [List].[ActionStatus] ADD CONSTRAINT [PK_ActionStatus] PRIMARY KEY CLUSTERED  ([ActionStatus_id]) ON [PRIMARY]
GO
