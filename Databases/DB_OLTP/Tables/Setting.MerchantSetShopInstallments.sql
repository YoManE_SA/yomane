CREATE TABLE [Setting].[MerchantSetShopInstallments]
(
[MerchantSetShopInstallments_id] [int] NOT NULL IDENTITY(1, 1),
[MerchantSetShop_id] [int] NOT NULL,
[Amount] [decimal] (9, 0) NOT NULL,
[MaxInstallments] [tinyint] NOT NULL
) ON [PRIMARY]
ALTER TABLE [Setting].[MerchantSetShopInstallments] ADD
CONSTRAINT [FK_MerchantSetShopInstallments_MerchantSetShop_MerchantSetShopID] FOREIGN KEY ([MerchantSetShop_id]) REFERENCES [Setting].[MerchantSetShop] ([MerchantSetShop_id]) ON DELETE CASCADE
GO
ALTER TABLE [Setting].[MerchantSetShopInstallments] ADD CONSTRAINT [PK_MerchantSetShopInstallments] PRIMARY KEY CLUSTERED  ([MerchantSetShopInstallments_id]) ON [PRIMARY]
GO
