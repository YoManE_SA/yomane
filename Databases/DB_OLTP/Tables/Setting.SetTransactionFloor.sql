CREATE TABLE [Setting].[SetTransactionFloor]
(
[SetTransactionFloor_id] [int] NOT NULL IDENTITY(1, 1),
[SettlementType_id] [tinyint] NOT NULL,
[Account_id] [int] NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[Title] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetTransactionFloor] ADD CONSTRAINT [PK_SetTransactionFloor] PRIMARY KEY CLUSTERED  ([SetTransactionFloor_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetTransactionFloor] ADD CONSTRAINT [FK_SetTransactionFloor_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id]) ON DELETE CASCADE
GO
ALTER TABLE [Setting].[SetTransactionFloor] ADD CONSTRAINT [FK_SetTransactionFloore_CurrencyList_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Setting].[SetTransactionFloor] ADD CONSTRAINT [FK_SetTransactionFloor_SettlementType_SettlementType_id] FOREIGN KEY ([SettlementType_id]) REFERENCES [List].[SettlemenType] ([SettlementType_id])
GO
