CREATE TABLE [Setting].[SetPeriodicFee]
(
[SetPeriodicFee_id] [int] NOT NULL IDENTITY(1, 1),
[PeriodicFeeType_id] [int] NOT NULL,
[Account_id] [int] NOT NULL,
[AccountPaymentMethod_id] [int] NULL,
[CurrencyISOCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[Amount] [decimal] (19, 3) NOT NULL,
[IsActive] [bit] NOT NULL,
[DateNextCharge] [date] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetPeriodicFee] ADD CONSTRAINT [PK_SetPeriodicFee] PRIMARY KEY CLUSTERED  ([SetPeriodicFee_id]) ON [PRIMARY]
GO
ALTER TABLE [Setting].[SetPeriodicFee] ADD CONSTRAINT [FK_SetPeriodicFee_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id]) ON DELETE CASCADE
GO
ALTER TABLE [Setting].[SetPeriodicFee] ADD CONSTRAINT [FK_SetPeriodicFee_AccountPaymentMethodID] FOREIGN KEY ([AccountPaymentMethod_id]) REFERENCES [Data].[AccountPaymentMethod] ([AccountPaymentMethod_id]) ON DELETE CASCADE
GO
ALTER TABLE [Setting].[SetPeriodicFee] ADD CONSTRAINT [FK_SetPeriodicFee_CurrencyISOCode] FOREIGN KEY ([CurrencyISOCode]) REFERENCES [List].[CurrencyList] ([CurrencyISOCode])
GO
ALTER TABLE [Setting].[SetPeriodicFee] ADD CONSTRAINT [FK_SetPeriodicFee_PeriodicFeeTypeID] FOREIGN KEY ([PeriodicFeeType_id]) REFERENCES [Data].[PeriodicFeeType] ([PeriodicFeeType_id]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'Set merchant''s periodic fees', 'SCHEMA', N'Setting', 'TABLE', N'SetPeriodicFee', NULL, NULL
GO
