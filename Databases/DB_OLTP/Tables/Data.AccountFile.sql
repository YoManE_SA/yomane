CREATE TABLE [Data].[AccountFile]
(
[AccountFile_id] [int] NOT NULL IDENTITY(1, 1),
[InsertDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_AccountFile_InsertDate] DEFAULT (sysdatetime()),
[Account_id] [int] NULL,
[FileTitle] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL,
[FileName] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL,
[FileExt] [varchar] (4) COLLATE Hebrew_CI_AS NULL,
[FileItemType_id] [tinyint] NULL,
[AdminComment] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[AdminApprovalDate] [datetime2] (0) NULL,
[AdminApprovalUser] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountFile] ADD CONSTRAINT [PK_AccountFile] PRIMARY KEY CLUSTERED  ([AccountFile_id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
ALTER TABLE [Data].[AccountFile] ADD CONSTRAINT [FK_AccountFile_AccountID] FOREIGN KEY ([Account_id]) REFERENCES [Data].[Account] ([Account_id])
GO
ALTER TABLE [Data].[AccountFile] ADD CONSTRAINT [FK_AccountFile_FileItemType_FileItemTypeID] FOREIGN KEY ([FileItemType_id]) REFERENCES [List].[FileItemType] ([FileItemType_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account Files uploaded by admin users', 'SCHEMA', N'Data', 'TABLE', N'AccountFile', NULL, NULL
GO
