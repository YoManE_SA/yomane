CREATE TABLE [dbo].[tblCompanySettingsHosted]
(
[CompanyID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsEnabled] DEFAULT ((0)),
[IsCreditCard] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsCreditCard] DEFAULT ((0)),
[IsCreditCardRequiredEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsCreditCardRequiredEmail] DEFAULT ((0)),
[IsCreditCardRequiredPhone] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsCreditCardRequiredPhone] DEFAULT ((0)),
[IsCreditCardRequiredID] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsCreditCardRequiredID] DEFAULT ((0)),
[IsCreditCardRequiredCVV] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsCreditCardRequiredCVV] DEFAULT ((0)),
[IsEcheck] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsEcheck] DEFAULT ((0)),
[IsEcheckRequiredEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsEcheckRequiredEmail] DEFAULT ((0)),
[IsEcheckRequiredPhone] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsEcheckRequiredPhone] DEFAULT ((0)),
[IsEcheckRequiredID] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsEcheckRequiredID] DEFAULT ((0)),
[IsDirectDebit] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsDirectDebit] DEFAULT ((0)),
[IsDirectDebitRequiredEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsDirectDebitRequiredEmail] DEFAULT ((0)),
[IsDirectDebitRequiredPhone] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsDirectDebitRequiredPhone] DEFAULT ((0)),
[IsDirectDebitRequiredID] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsDirectDebitRequiredID] DEFAULT ((0)),
[IsCustomer] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsCustomer] DEFAULT ((0)),
[IsSignatureOptional] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsSignatureOptional] DEFAULT ((0)),
[IsPhoneDebit] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsPhoneDebit] DEFAULT ((0)),
[IsPhoneDebitRequiredPhone] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsPhoneDebitRequiredPhone] DEFAULT ((0)),
[IsPhoneDebitRequiredEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsPhoneDebitRequiredEmail] DEFAULT ((0)),
[HideCustomParametersInRedirection] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_HideCustomParametersInRedirection] DEFAULT ((0)),
[WebMoneySecretKey] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[IsWebMoney] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsWebMoney] DEFAULT ((0)),
[WebMoneyMerchantPurse] [varchar] (15) COLLATE Hebrew_CI_AS NULL,
[IsWhiteLabel] [bit] NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsWhiteLabel] DEFAULT ((0)),
[IsShowAddress1] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsShowAddress1] DEFAULT ((1)),
[IsShowAddress2] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsShowAddress2] DEFAULT ((1)),
[IsShowCity] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsShowCity] DEFAULT ((1)),
[IsShowZipCode] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsShowZipCode] DEFAULT ((1)),
[IsShowState] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsShowState] DEFAULT ((1)),
[IsShowCountry] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsShowCountry] DEFAULT ((1)),
[IsShowEmail] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsShowEmail] DEFAULT ((1)),
[IsShowPhone] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsShowPhone] DEFAULT ((1)),
[IsHideLanguageSwitch] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsHideLanguageSwitch] DEFAULT ((0)),
[IsShowPersonalID] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsShowPersonalID] DEFAULT ((1)),
[IsPayPal] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsPayPal] DEFAULT ((0)),
[IsGoogleCheckout] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsGoogleCheckout] DEFAULT ((0)),
[PayPalMerchantID] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[GoogleCheckoutMerchantID] [nvarchar] (255) COLLATE Hebrew_CI_AS NULL,
[GoogleCheckoutMerchantKey] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[IsMoneyBookers] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsMoneyBookers] DEFAULT ((0)),
[MoneyBookersAccount] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[NotificationUrl] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[RedirectionUrl] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[LogoPath] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[MerchantTextDefaultLanguage] [tinyint] NULL,
[UIVersion] [tinyint] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_UIVersion] DEFAULT ((2)),
[ThemeCssFileName] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[IsShippingAddressRequired] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsRequireShippingAddress] DEFAULT ((0)),
[IsShowBirthDate] [bit] NOT NULL CONSTRAINT [DF_tblCompanySettingsHosted_IsShowBirthDate] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[trgCompanySettingsHosted_SetIsEnabled_InsUpd] ON [dbo].[tblCompanySettingsHosted]
    AFTER INSERT, UPDATE
AS
    IF @@ROWCOUNT = 0 
        RETURN;
    SET NOCOUNT ON;
	
    UPDATE  [dbo].[tblCompanySettingsHosted]
    SET     [IsEnabled] = CASE WHEN ( IsCreditCard = 1
                                      OR IsEcheck = 1
                                      OR IsDirectDebit = 1
                                      OR IsPhoneDebit = 1
                                      OR IsCustomer = 1
                                      OR IsWebMoney = 1
                                      OR IsPayPal = 1
                                      OR IsGoogleCheckout = 1
                                      OR IsMoneyBookers = 1
                                    ) THEN 1
                               ELSE 0
                          END
    WHERE   [CompanyID] IN ( SELECT [CompanyID]
                             FROM   INSERTED );
		
GO

ALTER TABLE [dbo].[tblCompanySettingsHosted] ADD CONSTRAINT [PK_tblCompanySettingsHosted] PRIMARY KEY CLUSTERED  ([CompanyID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCompanySettingsHosted] ADD CONSTRAINT [FK_tblCompanySettingsHosted_tblCompany_CompanyID] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[tblCompany] ([ID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompanySettingsHosted] ADD CONSTRAINT [FK_tblCompanySettingsHosted_LanguageList_MerchantTextDefaultLanguage] FOREIGN KEY ([MerchantTextDefaultLanguage]) REFERENCES [List].[LanguageList] ([Language_id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Store merchant''s HPP settings', 'SCHEMA', N'dbo', 'TABLE', N'tblCompanySettingsHosted', NULL, NULL
GO
