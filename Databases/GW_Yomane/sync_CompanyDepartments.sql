DECLARE @temp TABLE
    (
      [MerchantDepartment_id] [tinyint] ,
      [Name] [nvarchar](50)
    )

INSERT  INTO @temp
VALUES  ( 1, 'Accounting and Finance' )
INSERT  INTO @temp
VALUES  ( 2, 'Administrative' )
INSERT  INTO @temp
VALUES  ( 3, 'Customer Service' )
INSERT  INTO @temp
VALUES  ( 4, 'Distribution' )
INSERT  INTO @temp
VALUES  ( 5, 'Finance' )
INSERT  INTO @temp
VALUES  ( 6, 'Human Resource' )
INSERT  INTO @temp
VALUES  ( 7, 'IT Support' )
INSERT  INTO @temp
VALUES  ( 8, 'Legal Department' )
INSERT  INTO @temp
VALUES  ( 9, 'Management' )
INSERT  INTO @temp
VALUES  ( 10, 'Marketing/Promotions' )
INSERT  INTO @temp
VALUES  ( 11, 'Operations' )
INSERT  INTO @temp
VALUES  ( 12, 'Production' )
INSERT  INTO @temp
VALUES  ( 13, 'Purchasing' )
INSERT  INTO @temp
VALUES  ( 14, 'Research and Development' )
INSERT  INTO @temp
VALUES  ( 15, 'Sales' )
INSERT  INTO @temp
VALUES  ( 16, 'Traning' )

INSERT  INTO dbo.MerchantDepartment
        ( Name
        )
        SELECT  t.Name
        FROM    @temp t
                LEFT OUTER JOIN dbo.MerchantDepartment md ON md.MerchantDepartment_id = t.MerchantDepartment_id
        WHERE   md.MerchantDepartment_id IS NULL


UPDATE  dbo.MerchantDepartment
SET     Name = t.Name
FROM    MerchantDepartment x
        INNER JOIN @temp t ON x.MerchantDepartment_id = t.MerchantDepartment_id 

SELECT  *
FROM    dbo.MerchantDepartment