
-- =============================================
-- Author:		PM Mugisha
-- Create date: 2018/02/01
-- Description:	Updated from https://en.wikipedia.org/wiki/Standard_Industrial_Classification
--				
-- =============================================

SET NOCOUNT ON;
DECLARE @temp TABLE
    (
      SICCodeNumber SMALLINT ,
      IndustryName VARCHAR(80)
    )

INSERT  INTO @temp
VALUES  ( 100, 'Agricultural Production-Crops' )
INSERT  INTO @temp
VALUES  ( 200, 'Agricultural Prod-Livestock & Animal Specialties' )
INSERT  INTO @temp
VALUES  ( 700, 'Agricultural Services' )
INSERT  INTO @temp
VALUES  ( 800, 'Forestry' )
INSERT  INTO @temp
VALUES  ( 900, 'Fishing, Hunting and Trapping' )
INSERT  INTO @temp
VALUES  ( 1000, 'Metal Mining' )
INSERT  INTO @temp
VALUES  ( 1040, 'Gold and Silver Ores' )
INSERT  INTO @temp
VALUES  ( 1090, 'Miscellaneous Metal Ores' )
INSERT  INTO @temp
VALUES  ( 1220, 'Bituminous Coal & Lignite Mining' )
INSERT  INTO @temp
VALUES  ( 1221, 'Bituminous Coal & Lignite Surface Mining' )
INSERT  INTO @temp
VALUES  ( 1311, 'Crude Petroleum & Natural Gas' )
INSERT  INTO @temp
VALUES  ( 1381, 'Drilling Oil & Gas Wells' )
INSERT  INTO @temp
VALUES  ( 1382, 'Oil & Gas Field Exploration Services' )
INSERT  INTO @temp
VALUES  ( 1389, 'Oil & Gas Field Services, NEC' )
INSERT  INTO @temp
VALUES  ( 1400, 'Mining & Quarrying of Nonmetallic Minerals (No Fuels)' )
INSERT  INTO @temp
VALUES  ( 1520, 'General Bldg Contractors - Residential Bldgs' )
INSERT  INTO @temp
VALUES  ( 1531, 'Operative Builders' )
INSERT  INTO @temp
VALUES  ( 1540, 'General Bldg Contractors - Nonresidential Bldgs' )
INSERT  INTO @temp
VALUES  ( 1600, 'Heavy Construction Other Than Bldg Const - Contractors' )
INSERT  INTO @temp
VALUES  ( 1623, 'Water, Sewer, Pipeline, Comm & Power Line Construction' )
INSERT  INTO @temp
VALUES  ( 1629, 'Heavy Construction, Not Elsewhere Classified[6]' )
INSERT  INTO @temp
VALUES  ( 1700, 'Construction - Special Trade Contractors' )
INSERT  INTO @temp
VALUES  ( 1731, 'Electrical Work' )
INSERT  INTO @temp
VALUES  ( 2000, 'Food and Kindred Products' )
INSERT  INTO @temp
VALUES  ( 2011, 'Meat Packing Plants' )
INSERT  INTO @temp
VALUES  ( 2013, 'Sausages & Other Prepared Meat Products' )
INSERT  INTO @temp
VALUES  ( 2015, 'Poultry Slaughtering and Processing' )
INSERT  INTO @temp
VALUES  ( 2020, 'Dairy Products' )
INSERT  INTO @temp
VALUES  ( 2024, 'Ice Cream & Frozen Desserts' )
INSERT  INTO @temp
VALUES  ( 2030, 'Canned, Frozen & Preserved Fruit, Veg & Food Specialties' )
INSERT  INTO @temp
VALUES  ( 2033, 'Canned, Fruits, Veg, Preserves, Jams & Jellies' )
INSERT  INTO @temp
VALUES  ( 2040, 'Grain Mill Products' )
INSERT  INTO @temp
VALUES  ( 2050, 'Bakery Products' )
INSERT  INTO @temp
VALUES  ( 2052, 'Cookies & Crackers' )
INSERT  INTO @temp
VALUES  ( 2060, 'Sugar & Confectionery Products' )
INSERT  INTO @temp
VALUES  ( 2070, 'Fats & Oils' )
INSERT  INTO @temp
VALUES  ( 2080, 'Beverages' )
INSERT  INTO @temp
VALUES  ( 2082, 'Malt Beverages' )
INSERT  INTO @temp
VALUES  ( 2086, 'Bottled & Canned Soft Drinks & Carbonated Waters' )
INSERT  INTO @temp
VALUES  ( 2090, 'Miscellaneous Food Preparations & Kindred Products' )
INSERT  INTO @temp
VALUES  ( 2092, 'Prepared Fresh or Frozen Fish & Seafood' )
INSERT  INTO @temp
VALUES  ( 2100, 'Tobacco Products' )
INSERT  INTO @temp
VALUES  ( 2111, 'Cigarettes' )
INSERT  INTO @temp
VALUES  ( 2200, 'Textile Mill Products' )
INSERT  INTO @temp
VALUES  ( 2211, 'Broadwoven Fabric Mills, Cotton' )
INSERT  INTO @temp
VALUES  ( 2221, 'Broadwoven Fabric Mills, Man Made Fiber & Silk' )
INSERT  INTO @temp
VALUES  ( 2250, 'Knitting Mills' )
INSERT  INTO @temp
VALUES  ( 2253, 'Knit Outerwear Mills' )
INSERT  INTO @temp
VALUES  ( 2273, 'Carpets & Rugs' )
INSERT  INTO @temp
VALUES  ( 2300, 'Apparel & Other Finished Prods of Fabrics & Similar Matl' )
INSERT  INTO @temp
VALUES  ( 2320,
          'Men''s & Boys'' Furnishings, Work Clothing, & Allied Garments' )
INSERT  INTO @temp
VALUES  ( 2330, 'Women''s, Misses'', and Juniors Outerwear' )
INSERT  INTO @temp
VALUES  ( 2340, 'Women''s, Misses'', Children''s & Infant''s Undergarments' )
INSERT  INTO @temp
VALUES  ( 2390, 'Miscellaneous Fabricated Textile Products' )
INSERT  INTO @temp
VALUES  ( 2400, 'Lumber & Wood Products (No Furniture)' )
INSERT  INTO @temp
VALUES  ( 2421, 'Sawmills & Planing Mills, General' )
INSERT  INTO @temp
VALUES  ( 2430, 'Millwood, Veneer, Plywood, & Structural Wood Members' )
INSERT  INTO @temp
VALUES  ( 2451, 'Mobile Homes' )
INSERT  INTO @temp
VALUES  ( 2452, 'Prefabricated Wood Bldgs & Components' )
INSERT  INTO @temp
VALUES  ( 2510, 'Household Furniture' )
INSERT  INTO @temp
VALUES  ( 2511, 'Wood Household Furniture, (No Upholstered)' )
INSERT  INTO @temp
VALUES  ( 2520, 'Office Furniture' )
INSERT  INTO @temp
VALUES  ( 2522, 'Office Furniture (No Wood)' )
INSERT  INTO @temp
VALUES  ( 2531, 'Public Bldg & Related Furniture' )
INSERT  INTO @temp
VALUES  ( 2540, 'Partitions, Shelvg, Lockers, & office & Store Fixtures' )
INSERT  INTO @temp
VALUES  ( 2590, 'Miscellaneous Furniture & Fixtures' )
INSERT  INTO @temp
VALUES  ( 2600, 'Papers & Allied Products' )
INSERT  INTO @temp
VALUES  ( 2611, 'Pulp Mills' )
INSERT  INTO @temp
VALUES  ( 2621, 'Paper Mills' )
INSERT  INTO @temp
VALUES  ( 2631, 'Paperboard Mills' )
INSERT  INTO @temp
VALUES  ( 2650, 'Paperboard Containers & Boxes' )
INSERT  INTO @temp
VALUES  ( 2670, 'Converted Paper & Paperboard Prods (No Containers/Boxes)' )
INSERT  INTO @temp
VALUES  ( 2673, 'Plastics, Foil & Coated Paper Bags' )
INSERT  INTO @temp
VALUES  ( 2711, 'Newspapers: Publishing or Publishing & Printing' )
INSERT  INTO @temp
VALUES  ( 2721, 'Periodicals: Publishing or Publishing & Printing' )
INSERT  INTO @temp
VALUES  ( 2731, 'Books: Publishing or Publishing & Printing' )
INSERT  INTO @temp
VALUES  ( 2732, 'Book Printing' )
INSERT  INTO @temp
VALUES  ( 2741, 'Miscellaneous Publishing' )
INSERT  INTO @temp
VALUES  ( 2750, 'Commercial Printing' )
INSERT  INTO @temp
VALUES  ( 2761, 'Manifold Business Forms' )
INSERT  INTO @temp
VALUES  ( 2771, 'Greeting Cards' )
INSERT  INTO @temp
VALUES  ( 2780, 'Blankbooks, Looseleaf Binders & Bookbinding & Related Work' )
INSERT  INTO @temp
VALUES  ( 2790, 'Service Industries For The Printing Trade' )
INSERT  INTO @temp
VALUES  ( 2800, 'Chemicals & Allied Products' )
INSERT  INTO @temp
VALUES  ( 2810, 'Industrial Inorganic Chemicals' )
INSERT  INTO @temp
VALUES  ( 2820, 'Plastic Material, Synth Resin/Rubber, Cellulos (No Glass)' )
INSERT  INTO @temp
VALUES  ( 2821, 'Plastic Materials, Synth Resins & Nonvulcan Elastomers' )
INSERT  INTO @temp
VALUES  ( 2833, 'Medicinal Chemicals & Botanical Products' )
INSERT  INTO @temp
VALUES  ( 2834, 'Pharmaceutical Preparations' )
INSERT  INTO @temp
VALUES  ( 2835, 'In Vitro & In Vivo Diagnostic Substances' )
INSERT  INTO @temp
VALUES  ( 2836, 'Biological Products, (No Diagnostic Substances)' )
INSERT  INTO @temp
VALUES  ( 2840, 'Soap, Detergents, Cleaning Preparations, Perfumes, Cosmetics' )
INSERT  INTO @temp
VALUES  ( 2842, 'Specialty Cleaning, Polishing and Sanitation Preparations' )
INSERT  INTO @temp
VALUES  ( 2844, 'Perfumes, Cosmetics & Other Toilet Preparations' )
INSERT  INTO @temp
VALUES  ( 2851, 'Paints, Varnishes, Lacquers, Enamels & Allied Prods' )
INSERT  INTO @temp
VALUES  ( 2860, 'Industrial Organic Chemicals' )
INSERT  INTO @temp
VALUES  ( 2870, 'Agricultural Chemicals' )
INSERT  INTO @temp
VALUES  ( 2890, 'Miscellaneous Chemical Products' )
INSERT  INTO @temp
VALUES  ( 2891, 'Adhesives & Sealants' )
INSERT  INTO @temp
VALUES  ( 2911, 'Petroleum Refining' )
INSERT  INTO @temp
VALUES  ( 2950, 'Asphalt Paving & Roofing Materials' )
INSERT  INTO @temp
VALUES  ( 2990, 'Miscellaneous Products of Petroleum & Coal' )
INSERT  INTO @temp
VALUES  ( 3011, 'Tires & Inner Tubes' )
INSERT  INTO @temp
VALUES  ( 3021, 'Rubber & Plastics Footwear' )
INSERT  INTO @temp
VALUES  ( 3050, 'Gaskets, Packg & Sealg Devices & Rubber & Plastics Hose' )
INSERT  INTO @temp
VALUES  ( 3060, 'Fabricated Rubber Products, NEC' )
INSERT  INTO @temp
VALUES  ( 3080, 'Miscellaneous Plastics Products' )
INSERT  INTO @temp
VALUES  ( 3081, 'Unsupported Plastics Film & Sheet' )
INSERT  INTO @temp
VALUES  ( 3086, 'Plastics Foam Products' )
INSERT  INTO @temp
VALUES  ( 3089, 'Plastics Products, NEC' )
INSERT  INTO @temp
VALUES  ( 3100, 'Leather & Leather Products' )
INSERT  INTO @temp
VALUES  ( 3140, 'Footwear, (No Rubber)' )
INSERT  INTO @temp
VALUES  ( 3211, 'Flat Glass' )
INSERT  INTO @temp
VALUES  ( 3220, 'Glass & Glassware, Pressed or Blown' )
INSERT  INTO @temp
VALUES  ( 3221, 'Glass Containers' )
INSERT  INTO @temp
VALUES  ( 3231, 'Glass Products, Made of Purchased Glass' )
INSERT  INTO @temp
VALUES  ( 3241, 'Cement, Hydraulic' )
INSERT  INTO @temp
VALUES  ( 3250, 'Structural Clay Products' )
INSERT  INTO @temp
VALUES  ( 3260, 'Pottery & Related Products' )
INSERT  INTO @temp
VALUES  ( 3270, 'Concrete, Gypsum & Plaster Products' )
INSERT  INTO @temp
VALUES  ( 3272, 'Concrete Products, Except Block & Brick' )
INSERT  INTO @temp
VALUES  ( 3281, 'Cut Stone & Stone Products' )
INSERT  INTO @temp
VALUES  ( 3290, 'Abrasive, Asbestos & Misc Nonmetallic Mineral Prods' )
INSERT  INTO @temp
VALUES  ( 3310, 'Steel Works, Blast Furnaces & Rolling & Finishing Mills' )
INSERT  INTO @temp
VALUES  ( 3312, 'Steel Works, Blast Furnaces & Rolling Mills (Coke Ovens)' )
INSERT  INTO @temp
VALUES  ( 3317, 'Steel Pipe & Tubes' )
INSERT  INTO @temp
VALUES  ( 3320, 'Iron & Steel Foundries' )
INSERT  INTO @temp
VALUES  ( 3330, 'Primary Smelting & Refining of Nonferrous Metals' )
INSERT  INTO @temp
VALUES  ( 3334, 'Primary Production of Aluminum' )
INSERT  INTO @temp
VALUES  ( 3341, 'Secondary Smelting & Refining of Nonferrous Metals' )
INSERT  INTO @temp
VALUES  ( 3350, 'Rolling Drawing & Extruding of Nonferrous Metals' )
INSERT  INTO @temp
VALUES  ( 3357, 'Drawing & Insulating of Nonferrous Wire' )
INSERT  INTO @temp
VALUES  ( 3360, 'Nonferrous Foundries (Castings)' )
INSERT  INTO @temp
VALUES  ( 3390, 'Miscellaneous Primary Metal Products' )
INSERT  INTO @temp
VALUES  ( 3411, 'Metal Cans' )
INSERT  INTO @temp
VALUES  ( 3412, 'Metal Shipping Barrels, Drums, Kegs & Pails' )
INSERT  INTO @temp
VALUES  ( 3420, 'Cutlery, Handtools & General Hardware' )
INSERT  INTO @temp
VALUES  ( 3430, 'Heating Equip, Except Elec & Warm Air; & Plumbing Fixtures' )
INSERT  INTO @temp
VALUES  ( 3433, 'Heating Equipment, Except Electric & Warm Air Furnaces' )
INSERT  INTO @temp
VALUES  ( 3440, 'Fabricated Structural Metal Products' )
INSERT  INTO @temp
VALUES  ( 3442, 'Metal Doors, Sash, Frames, Moldings & Trim' )
INSERT  INTO @temp
VALUES  ( 3443, 'Fabricated Plate Work (Boiler Shops)' )
INSERT  INTO @temp
VALUES  ( 3444, 'Sheet Metal Work' )
INSERT  INTO @temp
VALUES  ( 3448, 'Prefabricated Metal Buildings & Components' )
INSERT  INTO @temp
VALUES  ( 3451, 'Screw Machine Products' )
INSERT  INTO @temp
VALUES  ( 3452, 'Bolts, Nuts, Screws, Rivets & Washers' )
INSERT  INTO @temp
VALUES  ( 3460, 'Metal Forgings & Stampings' )
INSERT  INTO @temp
VALUES  ( 3470, 'Coating, Engraving & Allied Services' )
INSERT  INTO @temp
VALUES  ( 3480, 'Ordnance & Accessories, (No Vehicles/Guided Missiles)' )
INSERT  INTO @temp
VALUES  ( 3490, 'Miscellaneous Fabricated Metal Products' )
INSERT  INTO @temp
VALUES  ( 3510, 'Engines & Turbines' )
INSERT  INTO @temp
VALUES  ( 3523, 'Farm Machinery & Equipment' )
INSERT  INTO @temp
VALUES  ( 3524, 'Lawn & Garden Tractors & Home Lawn & Gardens Equip' )
INSERT  INTO @temp
VALUES  ( 3530, 'Construction, Mining & Materials Handling Machinery & Equip' )
INSERT  INTO @temp
VALUES  ( 3531, 'Construction Machinery & Equip' )
INSERT  INTO @temp
VALUES  ( 3532, 'Mining Machinery & Equip (No Oil & Gas Field Mach & Equip)' )
INSERT  INTO @temp
VALUES  ( 3533, 'Oil & Gas Field Machinery & Equipment' )
INSERT  INTO @temp
VALUES  ( 3537, 'Industrial Trucks, Tractors, Trailers & Stackers' )
INSERT  INTO @temp
VALUES  ( 3540, 'Metalworkg Machinery & Equipment' )
INSERT  INTO @temp
VALUES  ( 3541, 'Machine Tools, Metal Cutting Types' )
INSERT  INTO @temp
VALUES  ( 3550, 'Special Industry Machinery (No Metalworking Machinery)' )
INSERT  INTO @temp
VALUES  ( 3555, 'Printing Trades Machinery & Equipment' )
INSERT  INTO @temp
VALUES  ( 3559, 'Special Industry Machinery, NEC' )
INSERT  INTO @temp
VALUES  ( 3560, 'General Industrial Machinery & Equipment' )
INSERT  INTO @temp
VALUES  ( 3561, 'Pumps & Pumping Equipment' )
INSERT  INTO @temp
VALUES  ( 3562, 'Ball & Roller Bearings' )
INSERT  INTO @temp
VALUES  ( 3564, 'Industrial & Commercial Fans & Blowers & Air Purifying Equip' )
INSERT  INTO @temp
VALUES  ( 3567, 'Industrial Process Furnaces & Ovens' )
INSERT  INTO @temp
VALUES  ( 3569, 'General Industrial Machinery & Equipment, NEC' )
INSERT  INTO @temp
VALUES  ( 3570, 'Computer & office Equipment' )
INSERT  INTO @temp
VALUES  ( 3571, 'Electronic Computers' )
INSERT  INTO @temp
VALUES  ( 3572, 'Computer Storage Devices' )
INSERT  INTO @temp
VALUES  ( 3575, 'Computer Terminals' )
INSERT  INTO @temp
VALUES  ( 3576, 'Computer Communications Equipment' )
INSERT  INTO @temp
VALUES  ( 3577, 'Computer Peripheral Equipment, NEC' )
INSERT  INTO @temp
VALUES  ( 3578, 'Calculating & Accounting Machines (No Electronic Computers)' )
INSERT  INTO @temp
VALUES  ( 3579, 'Office Machines, NEC' )
INSERT  INTO @temp
VALUES  ( 3580, 'Refrigeration & Service Industry Machinery' )
INSERT  INTO @temp
VALUES  ( 3585, 'Air-Cond & Warm Air Heatg Equip & Comm & Indl Refrig Equip' )
INSERT  INTO @temp
VALUES  ( 3590, 'Misc Industrial & Commercial Machinery & Equipment' )
INSERT  INTO @temp
VALUES  ( 3600, 'Electronic & Other Electrical Equipment (No Computer Equip)' )
INSERT  INTO @temp
VALUES  ( 3612, 'Power, Distribution & Specialty Transformers' )
INSERT  INTO @temp
VALUES  ( 3613, 'Switchgear & Switchboard Apparatus' )
INSERT  INTO @temp
VALUES  ( 3620, 'Electrical Industrial Apparatus' )
INSERT  INTO @temp
VALUES  ( 3621, 'Motors & Generators' )
INSERT  INTO @temp
VALUES  ( 3630, 'Household Appliances' )
INSERT  INTO @temp
VALUES  ( 3634, 'Electric Housewares & Fans' )
INSERT  INTO @temp
VALUES  ( 3640, 'Electric Lighting & Wiring Equipment' )
INSERT  INTO @temp
VALUES  ( 3651, 'Household Audio & Video Equipment' )
INSERT  INTO @temp
VALUES  ( 3652, 'Phonograph Records & Prerecorded Audio Tapes & Disks' )
INSERT  INTO @temp
VALUES  ( 3661, 'Telephone & Telegraph Apparatus' )
INSERT  INTO @temp
VALUES  ( 3663, 'Radio & TV Broadcasting & Communications Equipment' )
INSERT  INTO @temp
VALUES  ( 3669, 'Communications Equipment, NEC' )
INSERT  INTO @temp
VALUES  ( 3670, 'Electronic Components & Accessories' )
INSERT  INTO @temp
VALUES  ( 3672, 'Printed Circuit Boards' )
INSERT  INTO @temp
VALUES  ( 3674, 'Semiconductors & Related Devices' )
INSERT  INTO @temp
VALUES  ( 3677, 'Electronic Coils, Transformers & Other Inductors' )
INSERT  INTO @temp
VALUES  ( 3678, 'Electronic Connectors' )
INSERT  INTO @temp
VALUES  ( 3679, 'Electronic Components, NEC' )
INSERT  INTO @temp
VALUES  ( 3690, 'Miscellaneous Electrical Machinery, Equipment & Supplies' )
INSERT  INTO @temp
VALUES  ( 3695, 'Magnetic & Optical Recording Media' )
INSERT  INTO @temp
VALUES  ( 3711, 'Motor Vehicles & Passenger Car Bodies' )
INSERT  INTO @temp
VALUES  ( 3713, 'Truck & Bus Bodies' )
INSERT  INTO @temp
VALUES  ( 3714, 'Motor Vehicle Parts & Accessories' )
INSERT  INTO @temp
VALUES  ( 3715, 'Truck Trailers' )
INSERT  INTO @temp
VALUES  ( 3716, 'Motor Homes' )
INSERT  INTO @temp
VALUES  ( 3720, 'Aircraft & Parts' )
INSERT  INTO @temp
VALUES  ( 3721, 'Aircraft' )
INSERT  INTO @temp
VALUES  ( 3724, 'Aircraft Engines & Engine Parts' )
INSERT  INTO @temp
VALUES  ( 3728, 'Aircraft Parts & Auxiliary Equipment, NEC' )
INSERT  INTO @temp
VALUES  ( 3730, 'Ship & Boat Building & Repairing' )
INSERT  INTO @temp
VALUES  ( 3743, 'Railroad Equipment' )
INSERT  INTO @temp
VALUES  ( 3751, 'Motorcycles, Bicycles & Parts' )
INSERT  INTO @temp
VALUES  ( 3760, 'Guided Missiles & Space Vehicles & Parts' )
INSERT  INTO @temp
VALUES  ( 3790, 'Miscellaneous Transportation Equipment' )
INSERT  INTO @temp
VALUES  ( 3812, 'Search, Detection, Navigation, Guidance, Aeronautical Sys' )
INSERT  INTO @temp
VALUES  ( 3821, 'Laboratory Apparatus & Furniture' )
INSERT  INTO @temp
VALUES  ( 3822,
          'Auto Controls For Regulating Residential & Comml Environments' )
INSERT  INTO @temp
VALUES  ( 3823, 'Industrial Instruments For Measurement, Display, and Control' )
INSERT  INTO @temp
VALUES  ( 3824, 'Totalizing Fluid Meters & Counting Devices' )
INSERT  INTO @temp
VALUES  ( 3825, 'Instruments For Meas & Testing of Electricity & Elec Signals' )
INSERT  INTO @temp
VALUES  ( 3826, 'Laboratory Analytical Instruments' )
INSERT  INTO @temp
VALUES  ( 3827, 'Optical Instruments & Lenses' )
INSERT  INTO @temp
VALUES  ( 3829, 'Measuring & Controlling Devices, NEC' )
INSERT  INTO @temp
VALUES  ( 3841, 'Surgical & Medical Instruments & Apparatus' )
INSERT  INTO @temp
VALUES  ( 3842, 'Orthopedic, Prosthetic & Surgical Appliances & Supplies' )
INSERT  INTO @temp
VALUES  ( 3843, 'Dental Equipment & Supplies' )
INSERT  INTO @temp
VALUES  ( 3844, 'X-Ray Apparatus & Tubes & Related Irradiation Apparatus' )
INSERT  INTO @temp
VALUES  ( 3845, 'Electromedical & Electrotherapeutic Apparatus' )
INSERT  INTO @temp
VALUES  ( 3851, 'Ophthalmic Goods' )
INSERT  INTO @temp
VALUES  ( 3861, 'Photographic Equipment & Supplies' )
INSERT  INTO @temp
VALUES  ( 3873, 'Watches, Clocks, Clockwork Operated Devices/Parts' )
INSERT  INTO @temp
VALUES  ( 3910, 'Jewelry, Silverware & Plated Ware' )
INSERT  INTO @temp
VALUES  ( 3911, 'Jewelry, Precious Metal' )
INSERT  INTO @temp
VALUES  ( 3931, 'Musical Instruments' )
INSERT  INTO @temp
VALUES  ( 3942, 'Dolls & Stuffed Toys' )
INSERT  INTO @temp
VALUES  ( 3944, 'Games, Toys & Children''s Vehicles (No Dolls & Bicycles)' )
INSERT  INTO @temp
VALUES  ( 3949, 'Sporting & Athletic Goods, NEC' )
INSERT  INTO @temp
VALUES  ( 3950, 'Pens, Pencils & Other Artists'' Materials' )
INSERT  INTO @temp
VALUES  ( 3960, 'Costume Jewelry & Novelties' )
INSERT  INTO @temp
VALUES  ( 3990, 'Miscellaneous Manufacturing Industries' )
INSERT  INTO @temp
VALUES  ( 4011, 'Railroads, Line-Haul Operating' )
INSERT  INTO @temp
VALUES  ( 4013, 'Railroad Switching & Terminal Establishments' )
INSERT  INTO @temp
VALUES  ( 4100, 'Local & Suburban Transit & Interurban Hwy Passenger Trans' )
INSERT  INTO @temp
VALUES  ( 4210, 'Trucking & Courier Services (No Air)' )
INSERT  INTO @temp
VALUES  ( 4213, 'Trucking (No Local)' )
INSERT  INTO @temp
VALUES  ( 4220, 'Public Warehousing & Storage' )
INSERT  INTO @temp
VALUES  ( 4231, 'Terminal Maintenance Facilities For Motor Freight Transport' )
INSERT  INTO @temp
VALUES  ( 4400, 'Water Transportation' )
INSERT  INTO @temp
VALUES  ( 4412, 'Deep Sea Foreign Transportation of Freight' )
INSERT  INTO @temp
VALUES  ( 4512, 'Air Transportation, Scheduled' )
INSERT  INTO @temp
VALUES  ( 4513, 'Air Courier Services' )
INSERT  INTO @temp
VALUES  ( 4522, 'Air Transportation, Nonscheduled' )
INSERT  INTO @temp
VALUES  ( 4581, 'Airports, Flying Fields & Airport Terminal Services' )
INSERT  INTO @temp
VALUES  ( 4610, 'Pipe Lines (No Natural Gas)' )
INSERT  INTO @temp
VALUES  ( 4700, 'Transportation Services' )
INSERT  INTO @temp
VALUES  ( 4731, 'Arrangement of Transportation of Freight & Cargo' )
INSERT  INTO @temp
VALUES  ( 4812, 'Radiotelephone Communications' )
INSERT  INTO @temp
VALUES  ( 4813, 'Telephone Communications (No Radiotelephone)' )
INSERT  INTO @temp
VALUES  ( 4822, 'Telegraph & Other Message Communications' )
INSERT  INTO @temp
VALUES  ( 4832, 'Radio Broadcasting Stations' )
INSERT  INTO @temp
VALUES  ( 4833, 'Television Broadcasting Stations' )
INSERT  INTO @temp
VALUES  ( 4841, 'Cable & Other Pay Television Services' )
INSERT  INTO @temp
VALUES  ( 4899, 'Communications Services, NEC' )
INSERT  INTO @temp
VALUES  ( 4900, 'Electric, Gas & Sanitary Services' )
INSERT  INTO @temp
VALUES  ( 4911, 'Electric Services' )
INSERT  INTO @temp
VALUES  ( 4922, 'Natural Gas Transmission' )
INSERT  INTO @temp
VALUES  ( 4923, 'Natural Gas Transmission & Distribution' )
INSERT  INTO @temp
VALUES  ( 4924, 'Natural Gas Distribution' )
INSERT  INTO @temp
VALUES  ( 4931, 'Electric & Other Services Combined' )
INSERT  INTO @temp
VALUES  ( 4932, 'Gas & Other Services Combined' )
INSERT  INTO @temp
VALUES  ( 4941, 'Water Supply' )
INSERT  INTO @temp
VALUES  ( 4950, 'Sanitary Services' )
INSERT  INTO @temp
VALUES  ( 4953, 'Refuse Systems' )
INSERT  INTO @temp
VALUES  ( 4955, 'Hazardous Waste Management' )
INSERT  INTO @temp
VALUES  ( 4961, 'Steam & Air-Conditioning Supply' )
INSERT  INTO @temp
VALUES  ( 4991, 'Co-generation Services & Small Power Producers' )
INSERT  INTO @temp
VALUES  ( 5000, 'Wholesale-Durable Goods' )
INSERT  INTO @temp
VALUES  ( 5010, 'Wholesale-Motor Vehicles & Motor Vehicle Parts & Supplies' )
INSERT  INTO @temp
VALUES  ( 5013, 'Wholesale-Motor Vehicle Supplies & New Parts' )
INSERT  INTO @temp
VALUES  ( 5020, 'Wholesale-Furniture & Home Furnishings' )
INSERT  INTO @temp
VALUES  ( 5030, 'Wholesale-Lumber & Other Construction Materials' )
INSERT  INTO @temp
VALUES  ( 5031, 'Wholesale-Lumber, Plywood, millwork & Wood Panels' )
INSERT  INTO @temp
VALUES  ( 5040, 'Wholesale-Professional & Commercial Equipment & Supplies' )
INSERT  INTO @temp
VALUES  ( 5045, 'Wholesale-Computers & Peripheral Equipment & Software' )
INSERT  INTO @temp
VALUES  ( 5047, 'Wholesale-Medical, Dental & Hospital Equipment & Supplies' )
INSERT  INTO @temp
VALUES  ( 5050, 'Wholesale-Metals & Minerals (No Petroleum)' )
INSERT  INTO @temp
VALUES  ( 5051, 'Wholesale-Metals Service Centers & Offices' )
INSERT  INTO @temp
VALUES  ( 5063, 'Wholesale-Electrical Apparatus & Equipment, Wiring Supplies' )
INSERT  INTO @temp
VALUES  ( 5064, 'Wholesale-Electrical Appliances, TV & Radio Sets' )
INSERT  INTO @temp
VALUES  ( 5065, 'Wholesale-Electronic Parts & Equipment, NEC' )
INSERT  INTO @temp
VALUES  ( 5070, 'Wholesale-Hardware & Plumbing & Heating Equipment & Supplies' )
INSERT  INTO @temp
VALUES  ( 5072, 'Wholesale-Hardware' )
INSERT  INTO @temp
VALUES  ( 5080, 'Wholesale-Machinery, Equipment & Supplies' )
INSERT  INTO @temp
VALUES  ( 5082, 'Wholesale-Construction & Mining (No Petro) Machinery & Equip' )
INSERT  INTO @temp
VALUES  ( 5084, 'Wholesale-Industrial Machinery & Equipment' )
INSERT  INTO @temp
VALUES  ( 5090, 'Wholesale-Misc Durable Goods' )
INSERT  INTO @temp
VALUES  ( 5094, 'Wholesale-Jewelry, Watches, Precious Stones & Metals' )
INSERT  INTO @temp
VALUES  ( 5099, 'Wholesale-Durable Goods, NEC' )
INSERT  INTO @temp
VALUES  ( 5110, 'Wholesale-Paper & Paper Products' )
INSERT  INTO @temp
VALUES  ( 5122, 'Wholesale-Drugs, Proprietaries & Druggists'' Sundries' )
INSERT  INTO @temp
VALUES  ( 5130, 'Wholesale-Apparel, Piece Goods & Notions' )
INSERT  INTO @temp
VALUES  ( 5140, 'Wholesale-Groceries & Related Products' )
INSERT  INTO @temp
VALUES  ( 5141, 'Wholesale-Groceries, General Line (merchandise)' )
INSERT  INTO @temp
VALUES  ( 5150, 'Wholesale-Farm Product Raw Materials' )
INSERT  INTO @temp
VALUES  ( 5160, 'Wholesale-Chemicals & Allied Products' )
INSERT  INTO @temp
VALUES  ( 5171, 'Wholesale-Petroleum Bulk Stations & Terminals' )
INSERT  INTO @temp
VALUES  ( 5172, 'Wholesale-Petroleum & Petroleum Products (No Bulk Stations)' )
INSERT  INTO @temp
VALUES  ( 5180, 'Wholesale-Beer, Wine & Distilled Alcoholic Beverages' )
INSERT  INTO @temp
VALUES  ( 5190, 'Wholesale-Miscellaneous Non-durable Goods' )
INSERT  INTO @temp
VALUES  ( 5200, 'Retail-Building Materials, Hardware, Garden Supply' )
INSERT  INTO @temp
VALUES  ( 5211, 'Retail-Lumber & Other Building Materials Dealers' )
INSERT  INTO @temp
VALUES  ( 5271, 'Retail-Mobile Home Dealers' )
INSERT  INTO @temp
VALUES  ( 5311, 'Retail-Department Stores' )
INSERT  INTO @temp
VALUES  ( 5331, 'Retail-Variety Stores' )
INSERT  INTO @temp
VALUES  ( 5399, 'Retail-Misc General Merchandise Stores' )
INSERT  INTO @temp
VALUES  ( 5400, 'Retail-Food Stores' )
INSERT  INTO @temp
VALUES  ( 5411, 'Retail-Grocery Stores' )
INSERT  INTO @temp
VALUES  ( 5412, 'Retail-Convenience Stores' )
INSERT  INTO @temp
VALUES  ( 5500, 'Retail-Auto Dealers & Gasoline Stations' )
INSERT  INTO @temp
VALUES  ( 5531, 'Retail-Auto & Home Supply Stores' )
INSERT  INTO @temp
VALUES  ( 5551, 'Boat Dealers' )
INSERT  INTO @temp
VALUES  ( 5600, 'Retail-Apparel & Accessory Stores' )
INSERT  INTO @temp
VALUES  ( 5621, 'Retail-Women''s Clothing Stores' )
INSERT  INTO @temp
VALUES  ( 5651, 'Retail-Family Clothing Stores' )
INSERT  INTO @temp
VALUES  ( 5661, 'Retail-Shoe Stores' )
INSERT  INTO @temp
VALUES  ( 5700, 'Retail-Home Furniture, Furnishings & Equipment Stores' )
INSERT  INTO @temp
VALUES  ( 5712, 'Retail-Furniture Stores' )
INSERT  INTO @temp
VALUES  ( 5731, 'Retail-Radio, TV & Consumer Electronics Stores' )
INSERT  INTO @temp
VALUES  ( 5734, 'Retail-Computer & Computer Software Stores' )
INSERT  INTO @temp
VALUES  ( 5735, 'Retail-Record & Prerecorded Tape Stores' )
INSERT  INTO @temp
VALUES  ( 5810, 'Retail-Eating & Drinking Places' )
INSERT  INTO @temp
VALUES  ( 5812, 'Retail-Eating Places' )
INSERT  INTO @temp
VALUES  ( 5900, 'Retail-Miscellaneous Retail' )
INSERT  INTO @temp
VALUES  ( 5912, 'Retail-Drug Stores and Proprietary Stores' )
INSERT  INTO @temp
VALUES  ( 5940, 'Retail-Miscellaneous Shopping Goods Stores' )
INSERT  INTO @temp
VALUES  ( 5944, 'Retail-Jewelry Stores' )
INSERT  INTO @temp
VALUES  ( 5945, 'Retail-Hobby, Toy & Game Shops' )
INSERT  INTO @temp
VALUES  ( 5960, 'Retail-Nonstore Retailers' )
INSERT  INTO @temp
VALUES  ( 5961, 'Retail-Catalog & Mail-Order Houses' )
INSERT  INTO @temp
VALUES  ( 5990, 'Retail-Retail Stores, NEC' )
INSERT  INTO @temp
VALUES  ( 6012, 'Pay Day Lenders' )
INSERT  INTO @temp
VALUES  ( 6021, 'National Commercial Banks' )
INSERT  INTO @temp
VALUES  ( 6022, 'State Commercial Banks' )
INSERT  INTO @temp
VALUES  ( 6029, 'Commercial Banks, NEC' )
INSERT  INTO @temp
VALUES  ( 6035, 'Savings Institution, Federally Chartered' )
INSERT  INTO @temp
VALUES  ( 6036, 'Savings Institutions, Not Federally Chartered' )
INSERT  INTO @temp
VALUES  ( 6099, 'Functions Related To Depository Banking, NEC' )
INSERT  INTO @temp
VALUES  ( 6111, 'Federal & Federally Sponsored Credit Agencies' )
INSERT  INTO @temp
VALUES  ( 6141, 'Personal Credit Institutions' )
INSERT  INTO @temp
VALUES  ( 6153, 'Short-Term Business Credit Institutions' )
INSERT  INTO @temp
VALUES  ( 6159, 'Miscellaneous Business Credit Institution' )
INSERT  INTO @temp
VALUES  ( 6162, 'Mortgage Bankers & Loan Correspondents' )
INSERT  INTO @temp
VALUES  ( 6163, 'Loan Brokers' )
INSERT  INTO @temp
VALUES  ( 6172, 'Finance Lessors' )
INSERT  INTO @temp
VALUES  ( 6189, 'Asset-Backed Securities' )
INSERT  INTO @temp
VALUES  ( 6199, 'Finance Services' )
INSERT  INTO @temp
VALUES  ( 6200, 'Security & Commodity Brokers, Dealers, Exchanges & Services' )
INSERT  INTO @temp
VALUES  ( 6211, 'Security Brokers, Dealers & Flotation Companies' )
INSERT  INTO @temp
VALUES  ( 6221, 'Commodity Contracts Brokers & Dealers' )
INSERT  INTO @temp
VALUES  ( 6282, 'Investment Advice' )
INSERT  INTO @temp
VALUES  ( 6311, 'Life Insurance' )
INSERT  INTO @temp
VALUES  ( 6321, 'Accident & Health Insurance' )
INSERT  INTO @temp
VALUES  ( 6324, 'Hospital & Medical Service Plans' )
INSERT  INTO @temp
VALUES  ( 6331, 'Fire, Marine & Casualty Insurance' )
INSERT  INTO @temp
VALUES  ( 6351, 'Surety Insurance' )
INSERT  INTO @temp
VALUES  ( 6361, 'Title Insurance' )
INSERT  INTO @temp
VALUES  ( 6399, 'Insurance Carriers, NEC' )
INSERT  INTO @temp
VALUES  ( 6411, 'Insurance Agents, Brokers & Service' )
INSERT  INTO @temp
VALUES  ( 6500, 'Real Estate' )
INSERT  INTO @temp
VALUES  ( 6510, 'Real Estate Operators (No Developers) & Lessors' )
INSERT  INTO @temp
VALUES  ( 6512, 'Operators of Nonresidential Buildings' )
INSERT  INTO @temp
VALUES  ( 6513, 'Operators of Apartment Buildings' )
INSERT  INTO @temp
VALUES  ( 6519, 'Lessors of Real Property, NEC' )
INSERT  INTO @temp
VALUES  ( 6531, 'Real Estate Agents & Managers (For Others)' )
INSERT  INTO @temp
VALUES  ( 6532, 'Real Estate Dealers (For Their Own Account)' )
INSERT  INTO @temp
VALUES  ( 6552, 'Land Subdividers & Developers (No Cemeteries)' )
INSERT  INTO @temp
VALUES  ( 6770, 'Blank Checks' )
INSERT  INTO @temp
VALUES  ( 6792, 'Oil Royalty Traders' )
INSERT  INTO @temp
VALUES  ( 6794, 'Patent Owners & Lessors' )
INSERT  INTO @temp
VALUES  ( 6795, 'Mineral Royalty Traders' )
INSERT  INTO @temp
VALUES  ( 6798, 'Real Estate Investment Trusts' )
INSERT  INTO @temp
VALUES  ( 6799, 'Investors, NEC' )
INSERT  INTO @temp
VALUES  ( 7000, 'Hotels, Rooming Houses, Camps & Other Lodging Places' )
INSERT  INTO @temp
VALUES  ( 7011, 'Hotels & Motels' )
INSERT  INTO @temp
VALUES  ( 7200, 'Services-Personal Services' )
INSERT  INTO @temp
VALUES  ( 7310, 'Services-Advertising' )
INSERT  INTO @temp
VALUES  ( 7311, 'Services-Advertising Agencies' )
INSERT  INTO @temp
VALUES  ( 7320, 'Services-Consumer Credit Reporting, Collection Agencies' )
INSERT  INTO @temp
VALUES  ( 7330, 'Services-Mailing, Reproduction, Commercial Art & Photography' )
INSERT  INTO @temp
VALUES  ( 7331, 'Services-Direct Mail Advertising Services' )
INSERT  INTO @temp
VALUES  ( 7334, 'Services-Photocopying and Duplicating Services' )
INSERT  INTO @temp
VALUES  ( 7340, 'Services-To Dwellings & Other Buildings' )
INSERT  INTO @temp
VALUES  ( 7350, 'Services-Miscellaneous Equipment Rental & Leasing' )
INSERT  INTO @temp
VALUES  ( 7359, 'Services-Equipment Rental & Leasing, NEC' )
INSERT  INTO @temp
VALUES  ( 7361, 'Services-Employment Agencies' )
INSERT  INTO @temp
VALUES  ( 7363, 'Services-Help Supply Services' )
INSERT  INTO @temp
VALUES  ( 7370, 'Services-Computer Programming, Data Processing, Etc.' )
INSERT  INTO @temp
VALUES  ( 7371, 'Services-Computer Programming Services' )
INSERT  INTO @temp
VALUES  ( 7372, 'Services-Prepackaged Software' )
INSERT  INTO @temp
VALUES  ( 7373, 'Services-Computer Integrated Systems Design' )
INSERT  INTO @temp
VALUES  ( 7374, 'Services-Computer Processing & Data Preparation' )
INSERT  INTO @temp
VALUES  ( 7377, 'Services-Computer Rental & Leasing' )
INSERT  INTO @temp
VALUES  ( 7380, 'Services-Miscellaneous Business Services' )
INSERT  INTO @temp
VALUES  ( 7381, 'Services-Detective, Guard & Armored Car Services' )
INSERT  INTO @temp
VALUES  ( 7384, 'Services-Photofinishing Laboratories' )
INSERT  INTO @temp
VALUES  ( 7385, 'Services-Telephone Interconnect Systems' )
INSERT  INTO @temp
VALUES  ( 7389, 'Services-Business Services, NEC' )
INSERT  INTO @temp
VALUES  ( 7500, 'Services-Automotive Repair, Services & Parking' )
INSERT  INTO @temp
VALUES  ( 7510, 'Services-Auto Rental & Leasing (No Drivers)' )
INSERT  INTO @temp
VALUES  ( 7600, 'Services-Miscellaneous Repair Services' )
INSERT  INTO @temp
VALUES  ( 7812, 'Services-Motion Picture & Video Tape Production' )
INSERT  INTO @temp
VALUES  ( 7819, 'Services-Allied To Motion Picture Production' )
INSERT  INTO @temp
VALUES  ( 7822, 'Services-Motion Picture & Video Tape Distribution' )
INSERT  INTO @temp
VALUES  ( 7829, 'Services-Allied To Motion Picture Distribution' )
INSERT  INTO @temp
VALUES  ( 7830, 'Services-Motion Picture Theaters' )
INSERT  INTO @temp
VALUES  ( 7841, 'Services-Video Tape Rental' )
INSERT  INTO @temp
VALUES  ( 7900, 'Services-Amusement & Recreation Services' )
INSERT  INTO @temp
VALUES  ( 7948, 'Services-Racing, Including Track Operation' )
INSERT  INTO @temp
VALUES  ( 7990, 'Services-Miscellaneous Amusement & Recreation' )
INSERT  INTO @temp
VALUES  ( 7994, 'Services-Video Game Arcades' )
INSERT  INTO @temp
VALUES  ( 7995, 'Services-Gambling Transactions' )
INSERT  INTO @temp
VALUES  ( 7996, 'Services-Amusement Parks' )
INSERT  INTO @temp
VALUES  ( 7997, 'Services-Membership Sports & Recreation Clubs' )
INSERT  INTO @temp
VALUES  ( 8000, 'Services-Health Services' )
INSERT  INTO @temp
VALUES  ( 8011, 'Services-Offices & Clinics of Doctors of Medicine' )
INSERT  INTO @temp
VALUES  ( 8050, 'Services-Nursing & Personal Care Facilities' )
INSERT  INTO @temp
VALUES  ( 8051, 'Services-Skilled Nursing Care Facilities' )
INSERT  INTO @temp
VALUES  ( 8060, 'Services-Hospitals' )
INSERT  INTO @temp
VALUES  ( 8062, 'Services-General Medical & Surgical Hospitals, NEC' )
INSERT  INTO @temp
VALUES  ( 8071, 'Services-Medical Laboratories' )
INSERT  INTO @temp
VALUES  ( 8082, 'Services-Home Health Care Services' )
INSERT  INTO @temp
VALUES  ( 8090, 'Services-Misc Health & Allied Services, NEC' )
INSERT  INTO @temp
VALUES  ( 8093, 'Services-Specialty Outpatient Facilities, NEC' )
INSERT  INTO @temp
VALUES  ( 8111, 'Services-Legal Services' )
INSERT  INTO @temp
VALUES  ( 8200, 'Services-Educational Services' )
INSERT  INTO @temp
VALUES  ( 8300, 'Services-Social Services' )
INSERT  INTO @temp
VALUES  ( 8351, 'Services-Child Day Care Services' )
INSERT  INTO @temp
VALUES  ( 8600, 'Services-Membership organizations' )
INSERT  INTO @temp
VALUES  ( 8700, 'Services-Engineering, Accounting, Research, Management' )
INSERT  INTO @temp
VALUES  ( 8711, 'Services-Engineering Services' )
INSERT  INTO @temp
VALUES  ( 8731, 'Services-Commercial Physical & Biological Research' )
INSERT  INTO @temp
VALUES  ( 8734, 'Services-Testing Laboratories' )
INSERT  INTO @temp
VALUES  ( 8741, 'Services-Management Services' )
INSERT  INTO @temp
VALUES  ( 8742, 'Services-Management Consulting Services' )
INSERT  INTO @temp
VALUES  ( 8744, 'Services-Facilities Support Management Services' )
INSERT  INTO @temp
VALUES  ( 8748, 'Business Consulting Services, Not Elsewhere Classified' )
INSERT  INTO @temp
VALUES  ( 8880, 'American Depositary Receipts' )
INSERT  INTO @temp
VALUES  ( 8888, 'Foreign Governments' )
INSERT  INTO @temp
VALUES  ( 8900, 'Services-Services, NEC' )
INSERT  INTO @temp
VALUES  ( 9721, 'International Affairs' )
INSERT  INTO @temp
VALUES  ( 9995, 'Non-Operating Establishments' )
 
INSERT  INTO List.SICCode
        ( SICCodeNumber ,
          IndustryName
        )
        SELECT  t.SICCodeNumber ,
                t.IndustryName
        FROM    @temp t
                LEFT OUTER JOIN List.SICCode s ON t.SICCodeNumber = s.SICCodeNumber
        WHERE   s.SICCodeNumber IS NULL
 

	 
UPDATE  List.SICCode
SET     IndustryName = b.IndustryName
FROM    List.SICCode a
        INNER JOIN @temp b ON a.SICCodeNumber = b.SICCodeNumber
	
SET NOCOUNT OFF; 
