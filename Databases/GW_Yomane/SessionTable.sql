CREATE TABLE [dbo].[Session]
(
[SessionId] [bigint] NOT NULL IDENTITY(1, 1),
[CredToken] [uniqueidentifier] NOT NULL,
[Key] [varchar] (150) COLLATE Hebrew_CI_AS NULL,
[Value] [varchar] (550) COLLATE Hebrew_CI_AS NULL,
[DateAdded] [datetime] NOT NULL CONSTRAINT [DF_Session_DateAdded] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Session] ADD CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED  ([SessionId]) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.Session_Delete
    @CredToken UNIQUEIDENTIFIER ,
    @Key VARCHAR(150) = NULL
AS
    IF ( @Key IS NULL )
        DELETE  FROM dbo.Session
        WHERE   CredToken = @CredToken
    ELSE
        DELETE  FROM dbo.Session
        WHERE   CredToken = @CredToken
                AND [Key] = @Key


				 
GO
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.Session_Get
    @CredToken UNIQUEIDENTIFIER ,
    @Key VARCHAR(150) = NULL
AS
    SELECT  Value
    FROM    dbo.Session
    WHERE   CredToken = @CredToken
            AND ( [key] = @Key
                  OR @Key IS NULL
                )

GO

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.Session_Save
    @CredToken VARCHAR(150) ,
    @Key VARCHAR(150) = '' ,
    @Value VARCHAR(550)
AS
    IF EXISTS ( SELECT  1
                FROM    dbo.Session (NOLOCK)
                WHERE   CredToken = @CredToken
                        AND [Key] = @Key )
        BEGIN
            UPDATE  dbo.Session
            SET     Value = @Value
            WHERE   CredToken = @CredToken
                    AND [Key] = @Key
        END
    ELSE
        BEGIN
            INSERT  INTO dbo.Session
                    ( CredToken, [Key], Value )
            VALUES  ( @CredToken, -- CredToken - varchar(150)
                      @Key, -- Key - varchar(150)
                      @Value -- Value - varchar(150) 
                      )
        END


GO
