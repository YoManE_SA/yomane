SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WriteIntegrationServicesLogEnd]
    (
      @Name NVARCHAR(MAX) ,
      @isSuccess BIT
    )
AS 
    BEGIN
        SET @Name = LEFT(@Name, 50);
        DECLARE @nID INT;
        SET @nID = ( SELECT MAX(ID)
                     FROM   tblIntegrationServicesLog
                     WHERE  PackageName = @Name
                            AND EndDate < StartDate
                   );
        IF @nID IS NULL 
            INSERT  INTO tblIntegrationServicesLog
                    ( PackageName ,
                      StartDate ,
                      EndDate ,
                      IsSuccessful
                    )
            VALUES  ( @Name ,
                      GETDATE() ,
                      GETDATE() ,
                      @isSuccess
                    )
        ELSE 
            UPDATE  tblIntegrationServicesLog
            SET     EndDate = GETDATE() ,
                    IsSuccessful = @isSuccess
            WHERE   ID = @nID;
    END
GO
GRANT EXECUTE ON  [dbo].[WriteIntegrationServicesLogEnd] TO [gw_db_executor]
GO
