SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spUpdMerchantDailyTotal]
AS 
    BEGIN
		
		DECLARE @LastTotalDate AS DATE = NULL
		
		-- Get last day this month that total exist in report table
		SELECT	@LastTotalDate = MAX(TotalDate)
		FROM	MerchantDailyTotal 
		WHERE	MONTH(TotalDate) = MONTH(GETDATE());

		-- If no data in report set LastTotalDate to last day of last month
		IF @LastTotalDate IS NULL
			SET @LastTotalDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));


		-- Exit if last day is older then yesterday
		IF @LastTotalDate >= CAST(DATEADD(d,-1,GETDATE()) AS DATE)
			RETURN;


		-- Create temp table for holding totals
		CREATE TABLE #TempTotalTable
			(
			  MerchantID	INT ,
			  TotalDate		DATETIME ,
			  CurrencyID	INT ,
			  CreditTypeID	INT ,
			  TotalAmount	MONEY
			);

		-- Move all totals in focus to temp table
		INSERT	INTO #TempTotalTable (MerchantID, TotalDate, CurrencyID, CreditTypeID, TotalAmount)
		SELECT		companyID					AS 'MerchantID' ,
					CAST(InsertDate AS DATE)	AS 'TotalDate' ,
					Currency					AS 'CurrencyID' ,
					CreditType					AS 'CreditTypeID',
					SUM(amount)					AS 'TotalAmount'
		FROM		dbo.syn_sourceDB_tblCompanyTransPass
		WHERE		CAST(InsertDate AS DATE) > @LastTotalDate --CAST to date cause '2013-01-01 00:00:01' is larger then '2013-01-01'
					AND InsertDate < CAST(GETDATE() AS DATE)
					AND isTestOnly = 0
					AND PaymentMethod >= 20
		GROUP BY	companyID ,
					CAST(InsertDate AS DATE) ,
					Currency ,
					CreditType
		HAVING		SUM(amount) > 0;
				

		-- Move totals by merchant and date to report table
		INSERT	INTO MerchantDailyTotal (MerchantID, TotalDate, CurrencyID, AmountDebit, AmountCredit)
		SELECT		MerchantID ,
					TotalDate ,
					CurrencyID ,
					SUM(CASE WHEN CreditTypeID NOT IN (0,31) THEN TotalAmount ELSE 0 END) ,
					SUM(CASE WHEN CreditTypeID IN (0,31) THEN TotalAmount ELSE 0 END)
		FROM		#TempTotalTable
		GROUP BY	MerchantID ,
					TotalDate ,
					CurrencyID;

		-- Clean up
		DROP TABLE #TempTotalTable;

    END

GO
GRANT EXECUTE ON  [dbo].[spUpdMerchantDailyTotal] TO [gw_db_executor]
GO
