SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WriteIntegrationServicesLogStart](@sName nvarchar(max))
AS
BEGIN
	INSERT INTO tblIntegrationServicesLog(PackageName, StartDate, EndDate, IsSuccessful) VALUES (LEFT(@sName, 50), GETDATE(), DATEADD(hour, -1, GETDATE()), 0);
	RETURN @@IDENTITY;
END
GO
GRANT EXECUTE ON  [dbo].[WriteIntegrationServicesLogStart] TO [gw_db_executor]
GO
