SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[spGetMerchantMonthlyTotal](@MerchantID INT)
AS 
    BEGIN
		SET NOCOUNT ON;
		
		SELECT		CurrencyID ,
					SUM(AmountDebit) AS MonthlyDebitAmount ,
					MAX(TotalDate) AS LastDate
		FROM		MerchantDailyTotal
		WHERE		MerchantID = @MerchantID
					AND TotalDate >= CAST(DATEADD(mm,DATEDIFF(m,0,GETDATE()),0) AS DATE)
		GROUP BY	CurrencyID
		
		SET NOCOUNT OFF;
    END

GO
GRANT EXECUTE ON  [dbo].[spGetMerchantMonthlyTotal] TO [gw_db_executor]
GRANT EXECUTE ON  [dbo].[spGetMerchantMonthlyTotal] TO [InternetUser]
GO
