CREATE TABLE [dbo].[RollingReserveTotal]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MerchantID] [int] NOT NULL,
[MerchantName] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL,
[CurrencyID] [smallint] NOT NULL,
[CurrencyIsoCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[RolingReleaseAmount] [money] NULL,
[RolingReserveAmount] [money] NULL,
[RolingReleaseCount] [smallint] NULL,
[RolingReserveCount] [smallint] NULL,
[UnsettledCreditAmount] [money] NULL,
[UnsettledDebitAmount] [money] NULL,
[LastTransactionDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RollingReserveTotal] ADD CONSTRAINT [PK_RollingReserveTotal] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
