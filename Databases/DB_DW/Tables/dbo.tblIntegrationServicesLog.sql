CREATE TABLE [dbo].[tblIntegrationServicesLog]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[PackageName] [nvarchar] (50) COLLATE Hebrew_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[IsSuccessful] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblIntegrationServicesLog] ADD CONSTRAINT [PK_tblIntegrationServicesLog] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
