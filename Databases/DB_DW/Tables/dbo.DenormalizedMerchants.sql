CREATE TABLE [dbo].[DenormalizedMerchants]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MerchantID] [int] NULL,
[MerchantNumber] [int] NULL,
[MerchantName] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[MerchantEmail] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[MerchantStatusID] [int] NULL,
[MerchantStatusText] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[MerchantGroupID] [int] NULL,
[MerchantGroupName] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[IndustryID] [int] NULL,
[IndustryText] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[AccountManager] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[MerchantUrl] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[MerchantOpenningDate] [smalldatetime] NULL,
[MerchantClosingDate] [smalldatetime] NULL,
[MerchantContactName] [nvarchar] (350) COLLATE Hebrew_CI_AS NULL,
[MerchantCountry] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[MerchantRollingReservePercent] [money] NULL,
[MerchantRollingReserveAmount] [money] NULL,
[MerchantRollingReserveAmountCurrencyIsoCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[MerchantRollingReserveState] [varchar] (50) COLLATE Hebrew_CI_AS NULL,
[MerchantContactEmail] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[MerchantAlertEmail] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[MerchantSupportEmail] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[MerchantSupportPhone] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[MerchantPhone] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[MerchantCompanyPhone] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DenormalizedMerchants] ADD CONSTRAINT [PK_DenormalizedMerchants] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
