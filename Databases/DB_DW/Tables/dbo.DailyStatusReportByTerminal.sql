CREATE TABLE [dbo].[DailyStatusReportByTerminal]
(
[AcquiringBankName] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[TerminalName] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[TransactionAmountSales] [money] NULL,
[TransactionCountSales] [int] NULL,
[NumberOfVisaCHBMonthToDate] [int] NULL,
[PercentOfVisaCHBMonthToDate] [real] NULL,
[NumberOfMCCHBMonthToDate] [int] NULL,
[PercentOfMCCHBMonthToDate] [real] NULL,
[AmountOfVisaChbMonthToDate] [money] NULL,
[AmountOfMCChbMonthToDate] [money] NULL,
[RefundsCount] [int] NULL,
[RefundsAmount] [money] NULL,
[AmountBalance] AS ((([TransactionAmountSales]-[AmountOfVisaChbMonthToDate])-[AmountOfMCChbMonthToDate])-[RefundsAmount]),
[TerminalIsActive] [bit] NULL
) ON [PRIMARY]
GO
