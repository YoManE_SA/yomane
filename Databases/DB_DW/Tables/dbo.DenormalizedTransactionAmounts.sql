CREATE TABLE [dbo].[DenormalizedTransactionAmounts]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[AmountID] [int] NULL,
[AmountTypeID] [int] NULL,
[AmountTypeName] [varchar] (100) COLLATE Hebrew_CI_AS NULL,
[AmountDate] [datetime] NULL,
[Amount] [money] NULL,
[AmountUSD] [money] NULL,
[AmountSettlementID] [int] NULL,
[MerchantID] [int] NULL,
[MerchantNumber] [int] NULL,
[MerchantName] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[TransactionID] [int] NULL,
[TransactionStatus] [varchar] (15) COLLATE Hebrew_CI_AS NULL,
[TransactionCurrencyID] [int] NULL,
[TransactionCurrencyIsoCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[TerminalID] [int] NULL,
[TerminalName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[AcquiringBankID] [int] NULL,
[AcquiringBankName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[PaymentMethodID] [smallint] NULL,
[PaymentMethodName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[IPCountry] [char] (2) COLLATE Hebrew_CI_AS NULL,
[BINCountry] [char] (2) COLLATE Hebrew_CI_AS NULL,
[SettledAmount] [money] NULL,
[SettledAmountUSD] [money] NULL,
[SettledCurrencyID] [int] NULL,
[SettledCurrencyIsoCode] [char] (3) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DenormalizedTransactionAmounts] ADD CONSTRAINT [PK_DenormalizedTransactionAmounts] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
