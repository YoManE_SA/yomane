CREATE TABLE [dbo].[MerchantBanks]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MerchantID] [int] NOT NULL,
[AcquiringBankID] [int] NULL,
[AcquiringBankName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MerchantBanks] ADD CONSTRAINT [PK_MerchantBanks] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
