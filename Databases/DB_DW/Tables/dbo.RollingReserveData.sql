CREATE TABLE [dbo].[RollingReserveData]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TransactionID] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[CompanyID] [int] NOT NULL,
[CompanyNumber] [int] NOT NULL,
[CompanyName] [nvarchar] (200) COLLATE Hebrew_CI_AS NOT NULL,
[CurrencyID] [int] NOT NULL,
[CurrencySymbol] [nvarchar] (25) COLLATE Hebrew_CI_AS NOT NULL,
[CurrencyIsoCode] [char] (3) COLLATE Hebrew_CI_AS NOT NULL,
[CreditType] [int] NOT NULL,
[Amount] [money] NOT NULL,
[SettlementID] [int] NOT NULL CONSTRAINT [DF_RollingReserveData_SettelmentID] DEFAULT ((0)),
[Comment] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[RollingReserveState] [int] NULL,
[CompanyGroupID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RollingReserveData] ADD CONSTRAINT [PK_RollingReserve] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
