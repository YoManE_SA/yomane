CREATE TABLE [dbo].[MerchantAffiliates]
(
[MerchantID] [int] NOT NULL,
[AffiliateID] [int] NOT NULL,
[AffiliateName] [nvarchar] (100) COLLATE Hebrew_CI_AS NOT NULL
) ON [PRIMARY]
GO
