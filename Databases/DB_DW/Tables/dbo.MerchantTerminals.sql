CREATE TABLE [dbo].[MerchantTerminals]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MerchantID] [int] NOT NULL,
[TerminalID] [int] NULL,
[TerminalName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MerchantTerminals] ADD CONSTRAINT [PK_MerchantTerminals] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
