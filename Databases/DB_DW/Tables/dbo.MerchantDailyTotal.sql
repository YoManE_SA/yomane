CREATE TABLE [dbo].[MerchantDailyTotal]
(
[MerchantDailyTotal_id] [int] NOT NULL IDENTITY(1, 1),
[MerchantID] [int] NOT NULL,
[TotalDate] [date] NULL,
[CurrencyID] [int] NULL,
[AmountDebit] [money] NULL,
[AmountCredit] [money] NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_MerchantDailyTotal_MerchantID] ON [dbo].[MerchantDailyTotal] ([MerchantID]) ON [PRIMARY]

GO
ALTER TABLE [dbo].[MerchantDailyTotal] ADD CONSTRAINT [PK_MerchantDailyTotal] PRIMARY KEY CLUSTERED  ([MerchantDailyTotal_id]) ON [PRIMARY]
GO
