CREATE TABLE [dbo].[DenormalizedTransactionHistory]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[HistoryID] [int] NULL,
[HistoryTypeID] [int] NOT NULL,
[HistoryTypeName] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[HistoryInsertDate] [datetime] NOT NULL,
[HistoryIsSucceeded] [bit] NULL,
[HistoryReferenceNumber] [int] NULL,
[HistoryDescription] [varchar] (250) COLLATE Hebrew_CI_AS NULL,
[MerchantID] [int] NULL,
[MerchantNumber] [int] NULL,
[MerchantName] [nvarchar] (200) COLLATE Hebrew_CI_AS NULL,
[TransactionID] [int] NULL,
[TransactionStatus] [varchar] (15) COLLATE Hebrew_CI_AS NULL,
[TransactionInsertDate] [datetime] NOT NULL,
[TransactionCurrencyID] [int] NULL,
[TransactionCurrencyIsoCode] [char] (3) COLLATE Hebrew_CI_AS NULL,
[TransactionAmount] [money] NULL,
[TransactionPaymentMethodID] [smallint] NULL,
[TransactionPaymentMethodName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[TerminalID] [int] NULL,
[TerminalName] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[TerminalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[AcquiringBankID] [int] NULL,
[AcquiringBankName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[TransactionAmountUSD] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DenormalizedTransactionHistory] ADD CONSTRAINT [PK_DenormalizedTransactionHistory] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
