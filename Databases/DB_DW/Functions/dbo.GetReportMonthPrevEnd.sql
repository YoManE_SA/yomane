SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetReportMonthPrevEnd](@dtNow datetime)
RETURNS datetime
AS
BEGIN
	RETURN DateAdd(second, -1, DateAdd(month, 1, dbo.GetReportMonthPrevBegin(@dtNow)));
END
GO
GRANT EXECUTE ON  [dbo].[GetReportMonthPrevEnd] TO [gw_db_executor]
GO
