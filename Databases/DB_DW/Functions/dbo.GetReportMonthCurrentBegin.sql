SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetReportMonthCurrentBegin](@dtNow datetime)
RETURNS datetime
AS
BEGIN
	DECLARE @sYear char(4), @sMonth char(2), @dtTemp datetime;
	SET @dtTemp=DateAdd(day, CASE Day(@dtNow) WHEN 1 THEN -1 ELSE 0 END, @dtNow);
	SET @sYear=LTrim(RTrim(Str(Year(@dtTemp))));
	SET @sMonth=Replace(LTrim(RTrim(Str(Month(@dtTemp)))), ' ', '0');
	RETURN Convert(datetime, @sYear+'-'+@sMonth+'-01 00:00:00', 120)
END
GO
GRANT EXECUTE ON  [dbo].[GetReportMonthCurrentBegin] TO [gw_db_executor]
GO
