SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetReportMonthCurrentEnd](@dtNow datetime)
RETURNS datetime
AS
BEGIN
	RETURN DateAdd(second, -1, DateAdd(month, 1, dbo.GetReportMonthCurrentBegin(@dtNow)));
END
GO
GRANT EXECUTE ON  [dbo].[GetReportMonthCurrentEnd] TO [gw_db_executor]
GO
