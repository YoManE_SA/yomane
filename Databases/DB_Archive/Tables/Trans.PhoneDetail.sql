CREATE TABLE [Trans].[PhoneDetail]
(
[PhoneDetail_id] [int] NOT NULL IDENTITY(1, 1),
[PhoneNumber] [varchar] (20) COLLATE Hebrew_CI_AS NULL,
[FullName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Email] [nvarchar] (250) COLLATE Hebrew_CI_AS NULL,
[BillingAddress_id] [int] NULL,
[Merchant_id] [int] NULL,
[PhoneCarrier_id] [smallint] NULL,
[InvoiceName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Trans].[PhoneDetail] ADD CONSTRAINT [PK_PhoneDetail] PRIMARY KEY CLUSTERED  ([PhoneDetail_id]) ON [PRIMARY]
GO
