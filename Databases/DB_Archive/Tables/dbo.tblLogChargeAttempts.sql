CREATE TABLE [dbo].[tblLogChargeAttempts]
(
[LogChargeAttempts_id] [int] NOT NULL,
[TransactionType_id] [smallint] NULL,
[Lca_MerchantNumber] [varchar] (7) COLLATE Greek_CI_AS NULL,
[Lca_RemoteAddress] [varchar] (20) COLLATE Greek_CI_AS NULL,
[Lca_RequestMethod] [nvarchar] (12) COLLATE Greek_CI_AS NULL,
[Lca_PathTranslate] [nvarchar] (4000) COLLATE Greek_CI_AS NULL,
[Lca_HttpHost] [nvarchar] (400) COLLATE Greek_CI_AS NULL,
[Lca_HttpReferer] [nvarchar] (4000) COLLATE Greek_CI_AS NULL,
[Lca_QueryString] [nvarchar] (4000) COLLATE Greek_CI_AS NULL,
[Lca_RequestForm] [nvarchar] (4000) COLLATE Greek_CI_AS NULL,
[Lca_SessionContents] [nvarchar] (4000) COLLATE Greek_CI_AS NULL,
[Lca_ReplyCode] [nvarchar] (50) COLLATE Greek_CI_AS NULL,
[Lca_ReplyDesc] [nvarchar] (500) COLLATE Greek_CI_AS NULL,
[Lca_TransNum] [int] NULL,
[Lca_DateStart] [datetime] NULL,
[Lca_DateEnd] [datetime] NULL,
[Lca_TimeString] [nvarchar] (2000) COLLATE Greek_CI_AS NULL,
[Lca_LocalAddr] [varchar] (20) COLLATE Greek_CI_AS NULL,
[Lca_RequestString] [nvarchar] (4000) COLLATE Greek_CI_AS NULL,
[Lca_ResponseString] [nvarchar] (4000) COLLATE Greek_CI_AS NULL,
[Lca_IsSecure] [bit] NULL,
[Lca_DebitCompanyID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogChargeAttempts] ADD CONSTRAINT [PK_LogChargeRequests_id] PRIMARY KEY CLUSTERED  ([LogChargeAttempts_id]) ON [PRIMARY]
GO
