CREATE TABLE [dbo].[tblBillingAddress]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[insertDate] [datetime] NULL,
[address1] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[address2] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[city] [nvarchar] (60) COLLATE Hebrew_CI_AS NULL,
[zipCode] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[stateId] [int] NULL,
[countryId] [int] NULL,
[stateIso] [nvarchar] (2) COLLATE Hebrew_CI_AS NULL,
[countryIso] [nvarchar] (2) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillingAddress] ADD CONSTRAINT [PK_tblBillingAddress] PRIMARY KEY CLUSTERED  ([id]) WITH (FILLFACTOR=100) ON [PRIMARY]
GO
