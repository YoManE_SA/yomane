CREATE TABLE [dbo].[tblFraudCcBlackList]
(
[fraudCcBlackList_id] [int] NOT NULL,
[company_id] [int] NULL,
[creditCardType_id] [smallint] NULL,
[fcbl_ccDisplay] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[fcbl_ccExpMonth] [nchar] (2) COLLATE Hebrew_CI_AS NULL,
[fcbl_ccExpYear] [nchar] (4) COLLATE Hebrew_CI_AS NULL,
[fcbl_comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[fcbl_ccNumber256] [varbinary] (200) NULL,
[fcbl_InsertDate] [datetime] NULL,
[fcbl_BlockLevel] [tinyint] NULL,
[fcbl_BlockCount] [int] NULL,
[fcbl_ReplyCode] [nvarchar] (11) COLLATE Hebrew_CI_AS NULL,
[fcbl_UnblockDate] [datetime] NULL,
[fcbl_CCRMID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFraudCcBlackList] ADD CONSTRAINT [PK_tblFraudCcBlackList] PRIMARY KEY CLUSTERED  ([fraudCcBlackList_id]) ON [PRIMARY]
GO
