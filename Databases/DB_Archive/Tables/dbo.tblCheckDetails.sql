CREATE TABLE [dbo].[tblCheckDetails]
(
[id] [int] NOT NULL,
[insertDate] [datetime] NULL,
[companyId] [int] NULL,
[customerId] [int] NULL,
[billingAddressId] [int] NULL,
[accountName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[accountNumber256] [varbinary] (200) NULL,
[routingNumber256] [varbinary] (200) NULL,
[personalNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[phoneNumber] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[email] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[comment] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[birthDate] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[bankAccountTypeId] [tinyint] NULL,
[bankName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[bankCity] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[bankPhone] [nvarchar] (15) COLLATE Hebrew_CI_AS NULL,
[bankState] [nvarchar] (2) COLLATE Hebrew_CI_AS NULL,
[bankCountry] [nvarchar] (2) COLLATE Hebrew_CI_AS NULL,
[InvoiceName] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCheckDetails] ADD CONSTRAINT [PK_tblCheckDetails] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
