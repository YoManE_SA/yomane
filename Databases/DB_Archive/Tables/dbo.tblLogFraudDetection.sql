CREATE TABLE [dbo].[tblLogFraudDetection]
(
[LogFraudDetection_id] [int] NOT NULL,
[Company_id] [int] NULL,
[InsertDate] [smalldatetime] NULL,
[SendingString] [nvarchar] (1500) COLLATE Hebrew_CI_AS NULL,
[ReturnAnswer] [nvarchar] (1500) COLLATE Hebrew_CI_AS NULL,
[ReturnExplanation] [nvarchar] (1500) COLLATE Hebrew_CI_AS NULL,
[ReturnScore] [decimal] (18, 0) NULL,
[ReturnRiskScore] [decimal] (18, 0) NULL,
[ReturnBinCountry] [nvarchar] (2) COLLATE Hebrew_CI_AS NULL,
[ReturnQueriesRemaining] [nchar] (5) COLLATE Hebrew_CI_AS NULL,
[ReferenceCode] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[allowedScore] [decimal] (18, 0) NULL,
[replyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[IsTemperScore] [bit] NULL,
[IsProceed] [bit] NULL,
[transAmount] [money] NULL,
[transCurrency] [tinyint] NULL,
[transCreditType] [tinyint] NULL,
[PaymentMethodDisplay] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[BinCountry] [nchar] (2) COLLATE Hebrew_CI_AS NULL,
[IsDuplicateAnswer] [bit] NULL,
[SendingIp] [char] (15) COLLATE Hebrew_CI_AS NULL,
[SendingCity] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[SendingRegion] [nchar] (2) COLLATE Hebrew_CI_AS NULL,
[SendingPostal] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[SendingCountry] [nchar] (2) COLLATE Hebrew_CI_AS NULL,
[SendingDomain] [nvarchar] (30) COLLATE Hebrew_CI_AS NULL,
[SendingBin] [char] (6) COLLATE Hebrew_CI_AS NULL,
[AllowedRiskScore] [decimal] (18, 0) NULL,
[IsCountryWhitelisted] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLogFraudDetection] ADD CONSTRAINT [PK_tblLogFraudDetection] PRIMARY KEY CLUSTERED  ([LogFraudDetection_id]) ON [PRIMARY]
GO
