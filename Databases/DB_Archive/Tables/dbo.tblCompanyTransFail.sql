CREATE TABLE [dbo].[tblCompanyTransFail]
(
[ID] [int] NOT NULL,
[companyID] [int] NULL,
[TransactionTypeID] [int] NULL,
[DebitCompanyID] [int] NULL,
[CustomerID] [int] NULL,
[FraudDetectionLog_id] [int] NULL,
[InsertDate] [datetime] NULL,
[IPAddress] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[Amount] [money] NULL,
[Currency] [int] NULL,
[Payments] [tinyint] NULL,
[CreditType] [tinyint] NULL,
[replyCode] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[OrderNumber] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[Comment] [nvarchar] (3000) COLLATE Hebrew_CI_AS NULL,
[TerminalNumber] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[TransType] [int] NULL,
[PaymentMethod_id] [tinyint] NULL,
[PaymentMethodID] [int] NULL,
[PaymentMethodDisplay] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[isTestOnly] [bit] NULL,
[referringUrl] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[payerIdUsed] [nvarchar] (10) COLLATE Hebrew_CI_AS NULL,
[DebitReferenceCode] [nvarchar] (40) COLLATE Hebrew_CI_AS NULL,
[netpayFee_transactionCharge] [smallmoney] NULL,
[PayID] [int] NULL,
[PaymentMethod] [smallint] NULL,
[IPCountry] [varchar] (2) COLLATE Hebrew_CI_AS NULL,
[ctf_JumpIndex] [tinyint] NULL,
[AutoRefundStatus] [int] NULL,
[AutoRefundDate] [datetime] NULL,
[AutoRefundReply] [nvarchar] (20) COLLATE Hebrew_CI_AS NULL,
[DeclineCount] [smallint] NULL,
[DeclineSourceCount] [smallint] NULL,
[DeclineReplyCount] [smallint] NULL,
[IsGateway] [bit] NULL,
[CreditCardID] [int] NULL,
[CheckDetailsID] [int] NULL,
[PhoneDetailsID] [int] NULL,
[DebitReferenceNum] [nvarchar] (40) COLLATE Hebrew_CI_AS NULL,
[DebitFee] [smallmoney] NULL,
[DebitDeclineReason] [nvarchar] (500) COLLATE Hebrew_CI_AS NULL,
[Is3DSecure] [bit] NULL,
[MerchantProduct_id] [int] NULL,
[MobileDevice_id] [int] NULL,
[PayerInfo_id] [int] NULL,
[PayforText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[SystemText] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[TransPaymentMethod_id] [int] NULL,
[TransPayerInfo_id] [int] NULL,
[TransSource_id] [tinyint] NULL
) ON [PRIMARY]
WITH
(
DATA_COMPRESSION = PAGE
)
GO
ALTER TABLE [dbo].[tblCompanyTransFail] ADD CONSTRAINT [PK_tblCompanyTransFail] PRIMARY KEY CLUSTERED  ([ID]) WITH (DATA_COMPRESSION = PAGE) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCompanyTransFail_InsertDate] ON [dbo].[tblCompanyTransFail] ([InsertDate] DESC) ON [PRIMARY]
GO
