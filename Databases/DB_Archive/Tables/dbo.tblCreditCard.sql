CREATE TABLE [dbo].[tblCreditCard]
(
[ID] [int] NOT NULL,
[companyID] [smallint] NULL,
[ccTypeID] [smallint] NULL,
[BillingAddressId] [int] NULL,
[PersonalNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[ExpMM] [char] (10) COLLATE Hebrew_CI_AS NULL,
[ExpYY] [char] (10) COLLATE Hebrew_CI_AS NULL,
[Member] [nvarchar] (100) COLLATE Hebrew_CI_AS NULL,
[cc_cui] [char] (10) COLLATE Hebrew_CI_AS NULL,
[phoneNumber] [nvarchar] (50) COLLATE Hebrew_CI_AS NULL,
[email] [nvarchar] (80) COLLATE Hebrew_CI_AS NULL,
[BINCountry] [nchar] (2) COLLATE Hebrew_CI_AS NULL,
[Comment] [nvarchar] (3000) COLLATE Hebrew_CI_AS NULL,
[CCard_First6] [int] NULL,
[CCard_Last4] [smallint] NULL,
[CCard_number256] [varbinary] (200) NULL,
[cc_InsertDate] [datetime] NULL
) ON [PRIMARY]
WITH
(
DATA_COMPRESSION = PAGE
)
GO
ALTER TABLE [dbo].[tblCreditCard] ADD CONSTRAINT [PK_tblCreditCard] PRIMARY KEY CLUSTERED  ([ID]) WITH (DATA_COMPRESSION = PAGE) ON [PRIMARY]
GO
