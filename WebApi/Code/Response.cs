﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Netpay.WebApi.Code
{
    public enum Result
    {
        Success = 0,
        LoginRequired = 1,
        SignatureRequired = 2,
        SslRequiredd = 3,
        GeneralError = 4,
        InvalidInput = 5
    }

    public class Response
    {
        public Response(Result result, object data) : this(result.ToString(), data)
        {
        }

        public Response(string result, object data)
        {
            if (!Regex.IsMatch(result, @"^[a-zA-Z]+$"))
                throw new ArgumentException("Result must contain only letters");

            _result = result;
            _data = data;
        }

        public Response(Result result) : this(result, null) { }

        public Response(string result) : this(result, null) { }

        private string _result;
        private object _data;

        public string Result { get { return _result; } }
        public object Data { get { return _data; } }
    }
}