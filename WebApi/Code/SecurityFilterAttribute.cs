﻿using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Netpay.WebApi.Code
{
    public class SecurityFilterAttribute : ActionFilterAttribute
    {
        public SecurityFilterAttribute() : this(true, true, true){}

        public SecurityFilterAttribute(bool requireLogin, bool requireSignature, bool requireSsl)
        {
            _requireLogin = requireLogin;
            _requireSignature = requireSignature;
            _requireSsl = requireSsl;
        }

        private bool _requireLogin;
        private bool _requireSignature;
        private bool _requireSsl;
        private const string _credentialsTokenKey = "credentialsToken";
        private const string _signatureKey = "signature";

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            // login check
            Login login = null;
            if (_requireLogin)
            {
                if (!actionContext.Request.Headers.Contains(_credentialsTokenKey) || actionContext.Request.Headers.GetValues(_credentialsTokenKey).Count() == 0)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, new Response(Result.LoginRequired, "Credentials not provided"));
                    return;
                }

                Guid creds;
                string credentialsToken = actionContext.Request.Headers.GetValues(_credentialsTokenKey).First();
                if (!Guid.TryParse(credentialsToken, out creds))
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, new Response(Result.LoginRequired, "Bad credentials"));
                    return;
                }

                login = Netpay.Infrastructure.Security.Login.Get(creds, false);
                if (login == null)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, new Response(Result.LoginRequired, "Not logged in"));
                    return;
                }

                Infrastructure.ObjectContext.Current.CredentialsToken = creds;
            }
            
            // signature check
            if (_requireSignature)
            {
                if (login == null)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, new Response(Result.LoginRequired, "Login required for signature check"));
                    return;
                }

                if (!actionContext.Request.Headers.Contains(_signatureKey) || actionContext.Request.Headers.GetValues(_signatureKey).Count() == 0)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, new Response(Result.SignatureRequired, "Signature not provided"));
                    return;
                }

                string signature = actionContext.Request.Headers.GetValues(_signatureKey).First();

                string content;
                using (var stream = new StreamReader(actionContext.Request.Content.ReadAsStreamAsync().Result))
                {
                    stream.BaseStream.Position = 0;
                    content = stream.ReadToEnd();
                }
            }

            // ssl check
            if (_requireSsl && !actionContext.Request.IsLocal() && actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, new Response(Result.SslRequiredd));
                return;
            }
            
            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            Infrastructure.ObjectContext.Current?.Dispose();

            // append signature
            //string content;
            //using (var stream = new StreamReader(actionExecutedContext.Request.Content.ReadAsStreamAsync().Result))
            //{
            //    stream.BaseStream.Position = 0;
            //    content = stream.ReadToEnd();
            //}
            
            base.OnActionExecuted(actionExecutedContext);
        }
    }
}