﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Netpay.WebApi.Code
{
    public class UnhandledExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            Netpay.Infrastructure.Logger.Log(LogTag.WebApi, actionExecutedContext.Exception);
            string error = Netpay.Infrastructure.Application.IsProduction ? "General error, see logs." : actionExecutedContext.Exception.ToString();
            actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
            return;
        }
    }
}