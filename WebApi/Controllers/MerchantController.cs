﻿using Netpay.WebApi.Code;
using Netpay.WebApi.Models;
using System;
using System.Web.Http;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Bll;
using System.Web;

namespace WebApi.Controllers
{
    public class MerchantController : ControllerBase
    {
        [HttpPost]
        [SecurityFilter(false, false, true)]
        public Response Login(LoginRequest request)
        {
            if (request == null)
                return new Response(Result.InvalidInput);
            if (string.IsNullOrEmpty(request.Email))
                return new Response("EmailRequired");
            if (!request.Email.IsEmail())
                return new Response("InvalidEmail");
            if (string.IsNullOrEmpty(request.DeviceId))
                return new Response("DeviceIdRequired");
            if (!request.DeviceId.IsGuid())
                return new Response("InvalidDeviceId");
            if (string.IsNullOrEmpty(request.Password))
                return new Response("PasswordRequired");
            
            Guid creds = Guid.Empty;
            LoginResult result = Netpay.Infrastructure.Security.Login.DoLogin(new UserRole[] { UserRole.Merchant }, null, request.Email, request.Password, request.DeviceId, out creds);
            if (result == LoginResult.Success)
                return new Response(Result.Success, creds.ToString());
            else
                return new Response(result.ToString());
        }

        [HttpPost]
        [SecurityFilter(true, true, true)]
        public Response RegisterDevice(RegisterDeviceRequest request)
        {
            if (request == null)
                return new Response(Result.InvalidInput);
            if (request.DeviceId == null || !request.DeviceId.IsGuid())
                return new Response("InvalidDeviceId");
            if (request.PhoneNumber == null || !request.PhoneNumber.IsPhone())
                return new Response("InvalidPhone");

            var appSettings = Netpay.Bll.Merchants.Mobile.MobileAppSettings.Load(Netpay.Bll.Merchants.Merchant.Current.ID);
            if (appSettings == null)
                return new Response("InvalidSettings");

            string userAgent = HttpContext.Current.Request.Headers["User-Agent"];
            Netpay.Bll.Accounts.MobileDevice device = null;
            var result = Netpay.Bll.Accounts.MobileDevice.RegisterMobileDevice(appSettings.MaxDeviceCount, request.DeviceId, null, request.PhoneNumber, userAgent, out device);
            if (result != MobileDeviceRegistrationResult.Success)
                return new Response(result.ToString());

            if (!Netpay.Bll.Accounts.MobileDevice.Current.SendMobilePassCode())
                return new Response("CouldNotSendActivationCode");

            return new Response(Result.Success);
        }

        [HttpPost]
        [SecurityFilter(true, true, true)]
        public Response ActivateDevice([FromBody]string activationCode)
        {
            if (string.IsNullOrEmpty(activationCode))
                return new Response("InvalidActivationCode");

            if (Netpay.Bll.Accounts.MobileDevice.Current.PassCode != activationCode)
                return new Response("WrongActivationCode");
            if (!Netpay.Bll.Accounts.MobileDevice.Current.ActivateMobileDevice(Netpay.Bll.Accounts.MobileDevice.Current.PassCode))
                return new Response("CouldNotActivate");

            return new Response(Result.Success);
        }
    }
}
