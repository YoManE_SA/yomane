﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            Netpay.Infrastructure.Application.Init("MiniVT");
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Netpay.Infrastructure.Domain.Current = Netpay.Web.WebUtils.CurrentDomain;
        }

        private void Application_EndRequest(object sender, EventArgs e)
        {

        }
    }
}
