﻿
Partial Class Logout
    Inherits NetpayPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Logout()
        Response.Redirect("Login.aspx")
    End Sub

End Class
