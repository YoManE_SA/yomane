﻿Imports System.Collections.Generic
Imports System.Security.Principal
Imports System.Web.Configuration
Imports Netpay
Imports Netpay.Web
Imports Netpay.Infrastructure.Security

Public Class NetpayPage
    Inherits BasePage
    Private Shared _freeAccessPages As List(Of String) = Nothing
    Private Shared _synclock As Object = New Object

    Private _userGUID As Guid = Guid.Empty

    Private Property UserGUID() As Guid
        Get
            Return _userGUID
        End Get
        Set
            _userGUID = Value
        End Set
    End Property

    Private Shared ReadOnly Property FreeAccessPages() As List(Of String)
        Get
            If _freeAccessPages Is Nothing Then
                SyncLock _synclock
                    If _freeAccessPages Is Nothing Then
                        _freeAccessPages = New List(Of String)
                        _freeAccessPages.Add("login.aspx")
                    End If
                End SyncLock
            End If

            Return _freeAccessPages
        End Get
    End Property

    Public Function IsAuthorized() As Boolean
        ' Is free access
        Dim strPageFileName As String = System.IO.Path.GetFileName(Request.Path).Trim().ToLower()
        If FreeAccessPages.Contains(strPageFileName) Then
            Return True
        End If

        ' Is logged in
        If Not IsLoggedIn Then
            Return False
        End If

        Return True
    End Function


    Protected Overrides Sub OnPreInit(e As EventArgs)
        If Not IsAuthorized() Then
            Response.Redirect("Login.aspx", True)
        End If

        MyBase.OnPreInit(e)
    End Sub
End Class
