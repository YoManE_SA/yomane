﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Security.Principal
Imports System.Web.Configuration
Imports Netpay
Imports Netpay.Infrastructure.Logger
Imports Netpay.Infrastructure
Imports Netpay.Web
Imports Netpay.Infrastructure.Security
Imports System.Linq
Imports System.Globalization

Public Class LogsPage
    Inherits NetpayPage

    Protected Sub DeleteLogs(sender As Object, e As EventArgs)
        Infrastructure.Logger.DeleteLogs(Nothing)
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)
    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not IsPostBack Then
            ' Building the filters
            LoadSeverityData()
            LoadTagData()

            ' set default dates
            SetDefaultDates()
        End If
        ' Getting the Logs
        LoadLogs()
    End Sub

    Public Sub LoadMigrateCSS(e As EventArgs)
        Dim literal As New LiteralControl("<" & Page.Header.Controls.Count & ">")

        Page.Controls.Add(literal)
    End Sub

    Protected Overrides Sub OnPreRenderComplete(e As EventArgs)
        MyBase.OnPreRenderComplete(e)
        Dim literal As New LiteralControl("<link rel='stylesheet' text='text/css' href='assets/css/migrate.css' />")
        Page.Header.Controls.Add(literal)
    End Sub

    Protected Function FormatInfo(item As Infrastructure.Logger) As String
        If item.LongMessage Is Nothing Then
            Return String.Empty
        Else
            Return "<strong>More info</strong></br/>" + item.LongMessage
        End If
    End Function

    Public Shared Function GetLogs(filters As SearchFilters, sortAndPage As ISortAndPage) As List(Of Logger)
        If sortAndPage Is Nothing Then
            sortAndPage = New SortAndPage(0, 100, "InsertDate", True)
        End If
        If String.IsNullOrEmpty(sortAndPage.SortKey) Then
            sortAndPage.SortKey = "InsertDate"
            sortAndPage.SortDesc = True
        End If
        Using dc = New System.Data.Linq.DataContext(Netpay.Infrastructure.Application.GetParameter("errorNetConnectionString"))
            Return GetLogs(dc, filters).ApplySortAndPage(sortAndPage).ToList()
        End Using
    End Function

    Private Shared Function GetLogs(dc As System.Data.Linq.DataContext, filters As SearchFilters) As IQueryable(Of Logger)
        Dim exp = From l In dc.GetTable(Of Logger)() Select l
        If filters.[Date].From.HasValue Then
            exp = exp.Where(Function(l) l.InsertDate >= filters.[Date].From)
        End If
        If filters.[Date].[To].HasValue Then
            exp = exp.Where(Function(l) l.InsertDate <= filters.[Date].[To])
        End If
        If filters.Severity.HasValue Then
            exp = exp.Where(Function(l) l.Severity = filters.Severity)
        End If
        If filters.Tag IsNot Nothing Then
            exp = exp.Where(Function(l) l.Tag.ToLower() = filters.Tag.ToLower())
        End If
        If Not String.IsNullOrEmpty(filters.Text) Then
            exp = exp.Where(Function(l) l.Message.Contains(filters.Text) OrElse l.LongMessage.Contains(filters.Text))
        End If
        Return exp
    End Function

    Public Function GetFilters() As Infrastructure.Logger.SearchFilters
        Dim filters As Logger.SearchFilters = New Infrastructure.Logger.SearchFilters()
        Dim dateFrom As DateTime, dateTo As DateTime
        Dim dateCulture = CultureInfo.CreateSpecificCulture("he-IL")
        Dim dateStyle = DateTimeStyles.None
        If DateTime.TryParse(DatePickerFrom.Value, dateCulture, dateStyle, dateFrom) Then
            filters.[Date].From = dateFrom.MinTime()
            FiltersView.Add("From date", dateFrom.ToString("MM/dd/yyyy"))
        End If
        If DateTime.TryParse(DatePickerTo.Value, dateCulture, dateStyle, dateTo) Then
            filters.[Date].[To] = dateTo.MaxTime()
            FiltersView.Add("To date", dateTo.ToString("MM/dd/yyyy"))
        End If
        If Integer.Parse(SeverityDropDown.SelectedValue) >= 0 Then
            filters.Severity = SeverityDropDown.SelectedValue.ToNullableEnumByValue(Of Infrastructure.LogSeverity)()
            FiltersView.Add("Severity", SeverityDropDown.SelectedItem.Text)
        End If
        If LogTagDropDown.SelectedValue <> "-1" Then
            filters.Tag = LogTagDropDown.SelectedValue
            FiltersView.Add("Tag", LogTagDropDown.SelectedItem.Text)
        End If
        If Not String.IsNullOrEmpty(TextSearch.Text) Then
            filters.Text = TextSearch.Text
            FiltersView.Add("Text", TextSearch.Text)
        End If
        Return filters
    End Function

    Public Overridable Sub LoadSeverityData()
        SeverityDropDown.DataTextField = "Key"
        SeverityDropDown.DataValueField = "Value"
        SeverityDropDown.DataBind()
    End Sub

    Public Overridable Sub LoadTagData()
        LogTagDropDown.DataTextField = "Key"
        LogTagDropDown.DataValueField = "Key"
        LogTagDropDown.DataBind()
    End Sub

    Public Sub LoadLogs()
        Dim logs As List(Of Logger) = GetLogs(GetFilters(), Pager.Info.CopySortFrom(SortableColumns.Info))
        LogsRepeater.DataSource = logs
        LogsRepeater.DataBind()
    End Sub

    Public Sub SetDefaultDates()
        DatePickerFrom.Value = DateTime.Now.AddDays(-2).ToString("dd-MM-yyyy")
        DatePickerTo.Value = DateTime.Now.ToString("dd-MM-yyyy")
    End Sub

    Public ReadOnly Property Pager As Controls.Pager
        Get
            Return CType(Page.FindControl("pager"), Controls.Pager)
        End Get
    End Property

    Public ReadOnly Property SortableColumns As Web.Controls.SortableColumns
        Get
            Return CType(Page.FindControl("ccSorting"), Controls.SortableColumns)
        End Get
    End Property

    Public ReadOnly Property DatePickerFrom As HtmlInputText
        Get
            Return CType(Page.FindControl("datepickerFrom"), HtmlInputText)
        End Get
    End Property

    Public ReadOnly Property DatePickerTo As HtmlInputText
        Get
            Return CType(Page.FindControl("datepickerTo"), HtmlInputText)
        End Get
    End Property

    Public ReadOnly Property FiltersView As Controls.SearchFiltersView
        Get
            Return CType(Page.FindControl("wcFiltersView"), Controls.SearchFiltersView)
        End Get
    End Property

    Public ReadOnly Property SeverityDropDown As DropDownList
        Get
            Return CType(Page.FindControl("ddlSeverityID"), DropDownList)
        End Get
    End Property

    Public ReadOnly Property LogTagDropDown As DropDownList
        Get
            Return CType(Page.FindControl("ddlTag"), DropDownList)
        End Get
    End Property

    Public ReadOnly Property TextSearch As TextBox
        Get
            Return CType(Page.FindControl("txtTextSearch"), TextBox)
        End Get
    End Property

    Public ReadOnly Property LogsRepeater As Repeater
        Get
            Return CType(Page.FindControl("repeaterLogs"), Repeater)
        End Get
    End Property


    Protected Sub ErrorsRedirect(sender As Object, e As EventArgs)
        Response.Redirect("Default.aspx")
    End Sub

    Protected Sub LogoutRedirect(sender As Object, e As EventArgs)
        Response.Redirect("Logout.aspx")
    End Sub

End Class
