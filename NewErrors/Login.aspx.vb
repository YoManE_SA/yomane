﻿Imports Netpay
Imports Netpay.Infrastructure.Security

Partial Class Login
    Inherits NetpayPage

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsLoggedin Then
            Response.Redirect("~/Default.aspx")

        ElseIf ((Not IsPostBack) And (Page.Request.QueryString.ToString() = "PasswordChanged")) Then
            acLogin.SetSuccess("Password changed, login with the new one.")
            DataBindChildren()

        ElseIf IsPostBack Then
            pnlWrongCredPanel.Visible = False
            'If txtUserName.Text IsNot "" And txtPassword.Text IsNot "" And txtEmail.Text IsNot "" Then
            If txtPassword.Text IsNot "" And txtEmail.Text IsNot "" Then
                Dim result = Login(New Netpay.Infrastructure.Security.UserRole() {Netpay.Infrastructure.Security.UserRole.Admin}, vbNull, txtEmail.Text, txtPassword.Text)
                If result = Netpay.Infrastructure.Security.LoginResult.Success Then
                    Response.Redirect("~/Default.aspx")
                ElseIf result = Netpay.Infrastructure.Security.LoginResult.ForcePasswordChange Then
                    dlgChangePassword.RegisterShow()
                Else
                    pnlWrongCredPanel.Visible = True
                    lblError.Text = "Error: " & "Unknown username or bad password"
                End If
            End If
        End If
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        btnClose.OnClientClick = dlgChangePassword.HideJSCommand
        MyBase.OnPreRender(e)
    End Sub


    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        'Handle the case that the user inserted the old password again.
        If (txtPassword.Text.ToSql(True).Trim() = txtNewPassword.Text.ToSql(True).Trim()) Then
            acnPasswordMessage.SetMessage("Please insert a new password.", True)
            dlgChangePassword.BindAndUpdate()
        End If

        'Handle case when both passwords not equal
        If (txtNewPassword.Text.ToSql(True).Trim() <> txtConfirmPassword.Text.ToSql(True).Trim()) Then
            acnPasswordMessage.SetMessage("Passwords must be the same.", True)
            dlgChangePassword.BindAndUpdate()
        Else
            Dim email As String = txtEmail.Text.ToSql(True).Trim()
            'Dim userName As String = txtUserName.Text.ToSql(True).Trim()
            Dim oldPassword As String = txtOldPassword.Text.ToSql(True).Trim()
            Dim newPassword As String = txtNewPassword.Text.ToSql(True).Trim()

            Dim ret = Netpay.Infrastructure.Security.Login.SetNewPasswordAfterExpiredOrReset(New Netpay.Infrastructure.Security.UserRole() {Netpay.Infrastructure.Security.UserRole.Admin}, vbNull, email, oldPassword, newPassword, Request.UserHostAddress)
            If (ret = Netpay.Infrastructure.Security.Login.ChangePasswordResult.Success) Then
                Response.Redirect(Request.RawUrl + "?PasswordChanged", False)
            Else
                acnPasswordMessage.SetMessage("Unable to change password: " + ret.ToString(), True)
            End If
        End If
    End Sub
End Class
