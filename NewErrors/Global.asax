﻿<%@ Application Language="VB" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        Netpay.Web.ContextModule.AppName = "Errors"
        'Netpay.Infrastructure.Application.Init("Errors")
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        Dim app = TryCast(sender, System.Web.HttpApplication)
        If (app IsNot Nothing And app.Context IsNot Nothing) Then
            app.Context.Response.Headers.Remove("Server")
        End If
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        '
        'If Not Netpay.Web.WebUtils.IsLoggedin Then
        'Netpay.Web.WebUtils.Login()
        'End If
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub

    Public Sub Application_AcquireRequestState(ByVal sender As Object, ByVal e As EventArgs)
        If Request.IsSecureConnection Then Exit Sub
        If Request.ServerVariables("HTTP_HOST").StartsWith("192.168") Or Request.ServerVariables("HTTP_HOST") = "127.0.0.1" _
            Or Request.ServerVariables("HTTP_HOST") = "localhost" Or Request.ServerVariables("HTTP_HOST").StartsWith("80.179.180.") _
        Then Exit Sub
        If WebUtils.CurrentDomain.ForceSSL Then Response.Redirect("https://" & Request.Url.ToString().Substring(7))
    End Sub

</script>