﻿<%@ Page Language="VB" validaterequest="false" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    Dim errorId As String
    Dim tableRowNUm As String
    Dim i As Integer

    Protected Overrides Sub OnLoad(e As System.EventArgs)
        MyBase.OnLoad(e)
        Dim sSQL As String
        Dim errorId As Integer = Request.QueryString("errorid")
        If Request.QueryString("rblArchive") = "newerrors" Then
            sSQL = "SELECT top 1 * FROM tblErrorNet WHERE ID=" & errorId & " and IsArchive=0 "
        ElseIf Request.QueryString("rblArchive") = "archive" Then
            sSQL = "SELECT top 1 * FROM tblErrorNet WHERE ID=" & errorId & " and IsArchive=1 "
        ElseIf Request.QueryString("rblArchive") = "highlighted" Then
            sSQL = "SELECT top 1 * FROM tblErrorNet WHERE ID=" & errorId & " and IsHighlighted=1 "
        Else
            sSQL = "SELECT top 1 * FROM tblErrorNet WHERE ID=" & errorId
        End If
        tableRowNUm = Request.QueryString("rowNum")

        repError.DataSource = dbPages.ExecReader(sSQL)
        repError.DataBind()
        repError.DataSource.Close()

        repErrorContent.DataSource = dbPages.ExecReader("SELECT ID, ErrorTime, LocalIP From tblErrorNet Where ID IN(Select ID From fnGetErrorsInSameGroup(" & errorId & ")) ORDER BY ErrorTime DESC")
        repErrorContent.DataBind()
        repErrorContent.DataSource.Close()

    End Sub
</script>
       <div id="derror_<%=tableRowNUm %>">
       <table cellpadding="0" cellspacing="0" border="0" style="width:100%;">
       <tr>
			<asp:repeater id="repError" enableviewstate="false" runat="server">
				<itemtemplate>
					<td style="vertical-align:top;">
                        <table cellpadding="0" cellspacing="0" border="0" style="width:100%;">
						<tr style="display:<%# IIf(Eval("ID").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">ID:</td><td class="keyvalue"><%#Eval("ID")%></td>
						</tr>
						<tr style="display:<%# IIf(Eval("ErrorTime").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">ErrorTime:</td><td class="keyvalue"><%#Eval("ErrorTime")%></td></tr>
						<tr style="display:<%# IIf(Eval("ProjectName").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">ProjectName:</td><td class="keyvalue"><%#Eval("ProjectName")%></td></tr>
						<tr style="display:<%# IIf(Eval("RemoteIP").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">RemoteIP:</td><td class="keyvalue"><%#Eval("RemoteIP")%></td></tr>
						<tr style="display:<%# IIf(Eval("LocalIP").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">LocalIP:</td><td class="keyvalue"><%#Eval("LocalIP")%></td></tr>
						<tr style="display:<%# IIf(Eval("RemoteUser").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">RemoteUser:</td><td class="keyvalue"><%#Eval("RemoteUser")%></td></tr>
						<tr style="display:<%# IIf(Eval("ServerName").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">ServerName:</td><td class="keyvalue"><%#Eval("ServerName")%></td></tr>
						<tr style="display:<%# IIf(Eval("ServerPort").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">ServerPort:</td><td class="keyvalue"><%#Eval("ServerPort")%></td></tr>
						<tr style="display:<%# IIf(Eval("IsFailedSQL").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">IsFailedSQL:</td><td class="keyvalue"><%#Eval("IsFailedSQL")%></td></tr>
						<tr style="display:<%# IIf(Eval("ScriptName").ToString().Length = 0, "none", "normal") %>;">
						<tr style="display:<%# IIf(Eval("PhysicalPath").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">PhysicalPath:</td><td class="keyvalue"><%#Eval("PhysicalPath")%></td></tr>
						<tr style="display:<%# IIf(Eval("ExceptionSource").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">ExceptionSource:</td><td class="keyvalue"><%#Eval("ExceptionSource")%></td></tr>
						<tr style="display:<%# IIf(Eval("ExceptionMessage").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">ExceptionMessage:</td><td><%#Server.HtmlEncode(Eval("ExceptionMessage"))%></td></tr>
						<tr style="display:<%# IIf(Eval("ExceptionStackTrace").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">ExceptionStackTrace:</td><td class="keyvalue"><%#Eval("ExceptionStackTrace")%></td></tr>
						<tr style="display:<%# IIf(Eval("ExceptionHelpLink").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">ExceptionHelpLink:</td><td class="keyvalue"><%#Eval("ExceptionHelpLink")%></td></tr>
						<tr style="display:<%# IIf(Eval("ExceptionLineNumber").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">ExceptionLineNumber:</td><td class="keyvalue"><%#Eval("ExceptionLineNumber")%></td></tr>
						<tr style="display:<%# IIf(Eval("InnerExceptionSource").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">InnerExceptionSource:</td><td class="keyvalue"><%#Eval("InnerExceptionSource")%></td></tr>
						<tr style="display:<%# IIf(Eval("InnerExceptionMessage").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">InnerExceptionMessage:</td><td><%#Server.HtmlEncode(Eval("InnerExceptionMessage"))%></td></tr>
						<tr style="display:<%# IIf(Eval("InnerExceptionTargetSite").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">InnerExceptionTargetSite:</td><td class="keyvalue"><%#Eval("InnerExceptionTargetSite")%></td></tr>
						<tr style="display:<%# IIf(Eval("InnerExceptionStackTrace").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">InnerExceptionStackTrace:</td><td class="keyvalue"><%#Eval("InnerExceptionStackTrace") %></td></tr>
						<tr style="display:<%# IIf(Eval("InnerExceptionHelpLink").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">InnerExceptionHelpLink:</td><td class="keyvalue"><%#Eval("InnerExceptionHelpLink")%></td></tr>
						<tr style="display:<%# IIf(Eval("InnerExceptionLineNumber").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">InnerExceptionLineNumber:</td><td class="keyvalue"><%#Eval("InnerExceptionLineNumber")%></td></tr>
						<tr style="display:<%# IIf(Eval("RequestQueryString").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">RequestQueryString:</td><td class="keyvalue"><%#Eval("RequestQueryString").Replace("&", " &")%></td></tr>
						<tr style="display:<%# IIf(Eval("RequestForm").ToString().Length = 0, "none", "normal") %>;">
                            <td class="key">RequestForm:</td><td class="keyvalue"><%#Eval("RequestForm").Replace("&", " &")%></td></tr>
                        </table>
					</td>
					</itemtemplate>
			</asp:repeater>
			<td style="vertical-align:top; text-align:right; padding:2px; width:300px;">
				<asp:repeater id="repErrorContent" enableviewstate="false" runat="server" >
					<itemtemplate>
						<div style="padding:1px; margin:1px;">
							<a title="<%#Eval("ID") %>" style="text-decoration:none" onclick=ShowListErrorMess('<%#Eval("ID") %>','<%=tableRowNUm%>')>
								<span  style="<%# IIF(Request.QueryString("errorid").ToString()=Eval("ID").ToString(),"color:orange;text-decoration:underline;","color:White;text-decoration:none;")  %>" >
									<%# Date.Parse(Eval("ErrorTime").ToString()).ToString("dd/MM/yy HH:mm:ss")%>
                                    | <%#Eval("LocalIP") %>
								</span>	
							</a>
						</div> 
					</itemtemplate>
				</asp:repeater>
			</td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    function ShowListErrorMess(id,table_RowNUm) {
        var bb = "";
        if ($("#rblArchive_0").attr("checked")) { bb = "&rblArchive=newerrors " }
        if ($("#rblArchive_1").attr("checked")) { bb = "&rblArchive=archive" }
        if ($("#rblArchive_2").attr("checked")) { bb = "&rblArchive=highlighted" }
        $.ajax({
            beforeSend: function () { },
            type: 'GET',
            async: false,
            contentType: "utf-8",
            dataType: "text",
            global: false,
            data: "",
            url: 'ErrorMesseg.aspx?rowNum='+table_RowNUm+'&errorid=' + id + bb ,
            success: function (data) {
                var d = data;
                $('#derror_' + table_RowNUm).html(d);
                $('#derror').width("width:100%");
            },
            error: function () { alert("error"); }
        });
    }
</script>