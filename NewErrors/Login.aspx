﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Error Logging System - Sign in</title>
    <link rel="shortcut icon" type="image/x-icon" href="~/favicon.ico?1=1" />
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="assets/css/LoginStyle.css" rel="stylesheet" />
    <script src="assets/js/jquery-3.2.0.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
</head>
<body>
    <div class="container">
        <div class="card card-container">
            <form id="LoginFrom" class="form-signin" runat="server">
                <asp:ScriptManager runat="server"></asp:ScriptManager>
                <img src="assets/images/YoMaNe_logo.png" alt="Netpay Logo" class="loginLogo img-responsive" />

                <netpay:ActionNotify runat="server" ID="acLogin" />
               <%-- <asp:TextBox ID="txtUserName" class="form-control" placeholder="Username" runat="server" CssClass="form-control" autofocus></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Username field is required." ControlToValidate="txtUserName" Display="None"></asp:RequiredFieldValidator>--%>
                <asp:TextBox ID="txtEmail" class="form-control" placeholder="Email" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Email field is required." ControlToValidate="txtEmail" Display="None"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtPassword" AutoCompleteType="Disabled" class="form-control" placeholder="Password" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                <input style="display: none" type="password" id="txtPassword">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Password field is required." ControlToValidate="txtPassword" Display="None"></asp:RequiredFieldValidator>
                <button type="submit" class="btn btn-lg btn-primary btn-block btn-signin">Sign in</button>
                <asp:ValidationSummary ID="vsValidationSummary" runat="server" DisplayMode="BulletList" class="validationSummary" />

                <asp:Panel ID="pnlWrongCredPanel" runat="server" Visible="false" CssClass="validationSummary">
                    <ul>
                        <li>
                            <asp:Label ID="lblError" Text="The credentials entered did not match." runat="server"></asp:Label>
                        </li>
                    </ul>
                </asp:Panel>

                <%-- enforce change password modal dialog --%>
                <netpay:ModalDialog runat="server" ID="dlgChangePassword" Title="Change Password">
                    <Body>
                        <div class="alert alert-danger">
                            Password is too old. Policy requires changing it.
                    <br />
                            Please insert a new password and than log in.
                        </div>
                        <netpay:ActionNotify runat="server" ID="acnPasswordMessage" />
                        <div class="form-group">
                            Old Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtOldPassword" TextMode="Password" />
                        </div>
                        <div class="form-group">
                            New Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtNewPassword" TextMode="Password" />
                        </div>
                        <div class="form-group">
                            Confirm Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtConfirmPassword" TextMode="Password" />
                        </div>
                    </Body>
                    <Footer>
                        <asp:Button runat="server" ID="btnClose" ClientIDMode="Static" CssClass="btn btn-inverse btn-cons-short" Text="Cancel" />
                        <asp:Button CausesValidation="false" runat="server" ID="btnSave" CssClass="btn btn-primary btn-cons-short" Text="Save" OnClick="btnSave_Click" />
                    </Footer>
                </netpay:ModalDialog>
            </form>
        </div>
    </div>
</body>
</html>
