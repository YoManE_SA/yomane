﻿Imports System.Collections.Generic
Imports Netpay
Imports Netpay.Infrastructure.Logger
Imports Netpay.Infrastructure
Imports Netpay.Web
Imports System.Linq
Imports System.Globalization

Partial Class BllLogs
    Inherits LogsPage

    Public Overrides Sub LoadSeverityData()
        SeverityDropDown.DataSource = Enums.GetEnumDataSource(Of LogSeverity)()
        MyBase.LoadSeverityData()
        SeverityDropDown.Items.Insert(0, New System.Web.UI.WebControls.ListItem("", "-1"))
    End Sub

    Public Overrides Sub LoadTagData()
        LogTagDropDown.DataSource = Enums.GetEnumDataSource(Of LogTag)(LogTag.WebServiceLog.ToString())
        MyBase.LoadTagData()
        LogTagDropDown.Items.Insert(0, New System.Web.UI.WebControls.ListItem("", "-1"))
    End Sub

End Class
