﻿if (netpay == null) { var netpay = new Object() }
if (netpay.adminReports == null) { netpay.adminReports = new Object() }

netpay.adminReports.Utils =
{
	ExpandRow: function(rowId)
	{
		divElement = document.getElementById("div_" + rowId);
		imageElement = document.getElementById("img_" + rowId);
		
		if (divElement.style.display == "none") 
		{
			divElement.style.display = "";
			imageElement.src = "assets/images/tree_collapse.gif"
		}
		else 
		{
			divElement.style.display = "none";
			imageElement.src = "assets/images/tree_expand.gif"
		}
	},
	
	ClearForm: function(formElement) 
	{
		for (i = 0; i < formElement.elements.length; i++) {
			if (formElement.elements[i].type == "text") {
				formElement.elements[i].value = "";
			}
			if (formElement.elements[i].type == "select-one") {
				formElement.elements[i].selectedIndex = 0;
			}
			if (formElement.elements[i].type == "checkbox") {
				formElement.elements[i].checked = false;
			}
		}
	}
}