﻿Imports System.Collections.Generic
Imports Netpay
Imports Netpay.Infrastructure.Logger
Imports Netpay.Infrastructure
Imports Netpay.Web
Imports System.Linq
Imports System.Globalization

Partial Class WebServicesLogs
    Inherits LogsPage

    Public Overrides Sub LoadSeverityData()
        SeverityDropDown.DataSource = Enums.GetEnumSpecificEntry(Of LogSeverity)(LogSeverity.Info.ToString())
        MyBase.LoadSeverityData()
    End Sub

    Public Overrides Sub LoadTagData()
        LogTagDropDown.DataSource = Enums.GetEnumSpecificEntry(Of LogTag)(LogTag.WebServiceLog.ToString())
        MyBase.LoadTagData()
    End Sub

End Class
