﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WebServicesLogs.aspx.vb" Inherits="WebServicesLogs" %>

<%@ Import Namespace="Netpay.Infrastructure" %>
<%@ Import Namespace="Netpay.Web" %>
<%@ Import Namespace="Netpay.Infrastructure.Logger" %>

<%@ Register TagPrefix="netpay" Namespace="Netpay.Web.Controls" Assembly="Netpay.Web" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error Logging System - BLL LOGS</title>
    <link rel="shortcut icon" type="image/x-icon" href="~/favicon.ico" />
    <link href="assets/css/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
    <link href="assets/css/StyleEng.css" rel="stylesheet" />
    
</head>
<body>
    <form id="form1" runat="server">
         <asp:Button OnClick="LogoutRedirect" runat="server" Text="LOGOUT" CssClass="btnStandard LogoutRedirectButton" />

        <div>
            <table class="formFilter" width="95%" border="0">
                <tr>
                    <td class="pageMainHeading" style="font-size: 13px; text-transform: uppercase; color: orange; text-decoration: underline">Web Services Logs</td>
                </tr>
                <tr style="text-align: left; margin: 0; padding: 0">
                    <td>From date:<br />
                        <input id="datepickerFrom" type="text" runat="server" class="grayInput" /></td>
                    <td>To date:<br />
                        <input id="datepickerTo" type="text" runat="server" class="grayInput" /></td>
                    <td>Severity:<br />
                        <asp:DropDownList ID="ddlSeverityID" runat="server" EnableViewState="true" class="grayInput" /></td>
                    <td>Tag:<br />
                        <asp:DropDownList ID="ddlTag" runat="server" EnableViewState="true" CssClass="grayInput" /></td>
                    <td>Text:<br />
                        <asp:TextBox ID="txtTextSearch" runat="server" EnableViewState="true" CssClass="grayInput" /></td>
                    <td style="vertical-align: bottom;">
                        <asp:Button ID="btnFilter" runat="server" Text=" FILTER " UseSubmitBehavior="true" CssClass="btnStandard" /></td>
                </tr>
            </table>
            <br />
            <netpay:SearchFiltersView ID="wcFiltersView" runat="server" />
            <table border="0" width="95%" class="formNormal">
                <netpay:SortableColumns ID="ccSorting" VOType="BllLogVO" runat="server">
                    <netpay:SortableColumn ColumnTitle="" Enabled="false" runat="server" />
                    <netpay:SortableColumn ColumnTitle="Severity" Enabled="false" ColumnName="SeverityID" runat="server" CssStyle="text-align:left" />
                    <netpay:SortableColumn ColumnTitle="Date" ColumnName="InsertDate" runat="server" CssStyle="text-align:left" />
                    <netpay:SortableColumn ColumnTitle="Instance" ColumnName="Source" Enabled="false" runat="server" CssStyle="text-align:left" />
                    <netpay:SortableColumn ColumnTitle="Tag" ColumnName="Tag" runat="server" CssStyle="text-align:left" />
                    <netpay:SortableColumn ColumnTitle="Info" Enabled="false" runat="server" CssStyle="text-align:left" />
                </netpay:SortableColumns>
                <asp:Repeater ID="repeaterLogs" runat="server">
                    <ItemTemplate>
                        <tr onmouseover="this.style.backgroundColor='#555555';" onmouseout="this.style.backgroundColor='#000000';">
                            <td>
                                <img src="assets/images/tree_expand.gif" style="visibility: <%# IIf(CType(Container.DataItem, Logger).LongMessage Is Nothing, "hidden", "visible") %>;" id='img_<%# CType(Container.DataItem, Logger).ID %>' onclick='netpay.adminReports.Utils.ExpandRow(<%# CType(Container.DataItem, Logger).ID %>);' style="cursor: pointer" /></td>
                            <td nowrap="nowrap">
                                <span class="logSeverity<%# CType(Container.DataItem, Logger).Severity.ToString() %>" style="width: 15px; height: 5px; margin-bottom: 2px;">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <%# CType(Container.DataItem, Logger).Severity.ToString() %>
                            </td>
                            <td nowrap="nowrap">
                                <%# CType(Container.DataItem, Logger).InsertDate %>
                            </td>
                            <td nowrap="nowrap">
                                <%# CType(Container.DataItem, Logger).Source %>					
                            </td>
                            <td nowrap="nowrap">
                                <%# CType(Container.DataItem, Logger).Tag %>					
                            </td>
                            <td>
                                <%# CType(Container.DataItem, Logger).Message %>					
                            </td>
                        </tr>
                        <tr style="margin: 0; padding: 0; display: none;" id='div_<%# CType(Container.DataItem, Logger).ID %>'>
                            <td colspan="1" style="margin: 0; padding: 0"></td>
                            <td colspan="5" style="margin: 0; padding: 0">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <pre style="width: 0px;"><asp:Literal ID="ltMoreInfo" Text='<%# FormatInfo(CType(Container.DataItem, Logger)) %>' runat="server" /></pre>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-top: 10px;">
                <tr>
                    <td style="text-align: left; margin: 0; padding: 0">
                        <netpay:Pager ID="pager" runat="server" />
                    </td>
                </tr>
            </table>

            <asp:Button OnClick="ErrorsRedirect" runat="server" Text="ERRORS" CssClass="btnStandard RedirectButton" />
        </div>
    </form>

    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui-1.8.10.custom.min.js"></script>
    <netpay:ScriptManager EnableBlocker="false" runat="server" EnableSessionAlerts="true" />
    <script src="assets/js/netpay.adminReports.Utils.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= datepickerFrom.ClientID %>").datepicker({ dateFormat: 'dd-mm-yy' });
                $("#<%= datepickerTo.ClientID %>").datepicker({ dateFormat: 'dd-mm-yy' });
                $(".pagerWhite").attr('class', 'pagerBlack');
            });
    </script>
</body>
</html>
