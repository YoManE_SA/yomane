﻿<%@ Page Language="VB" ValidateRequest="false" Debug="true" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Register Src="Common/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    Dim sSQL As String, sQueryWhere As String, nFromDate As String, nToDate As String, sDateDiff As String, _
sStatusColor As String, sAnd As String, sDivider As String, ETSite As String, sTimeDelta As String
    Dim querySQL As StringBuilder
    Dim sQueryString As String = "1=1"
    Dim i As Integer, nTmpNum As Integer, nSeconds As Integer
    Dim reader As System.Data.IDataReader

    'Sub DisplayContent(ByVal sVar As String, Optional ByVal nIndex As Integer = 0)
    '    Dim sVarShow As String = PGData(sVar).ToString.Trim
    '    If Not String.IsNullOrEmpty(sVarShow) Then
    '        Select Case sVar
    '            Case "RequestQueryString", "RequestForm"
    '                sVarShow = "<textarea id=""" & sVar & nIndex & """>" & Server.UrlDecode(sVarShow).Replace("&", vbCrLf & "&") & "</textarea>"
    '            Case "InnerExceptionStackTrace", "ExceptionStackTrace"
    '                sVarShow = sVarShow.Replace(" at ", "<br />at ")
    '        End Select
    '        Response.Write("<span class=""key"">" & Trim(sVar) & ":</span> " & sVarShow & "<br />")
    '    End If
    'End Sub

    Sub FilterArchive(ByVal o As Object, ByVal e As EventArgs)
        Session("IsArchive") = rblArchive.SelectedIndex
        sQueryWhere &= IIf(String.IsNullOrEmpty(sQueryWhere), " WHERE ", " AND ")
        sQueryWhere &= "IsArchive=" & dbPages.TestVar(Session("IsArchive"), 0, 1, 0)
        If rblArchive.SelectedIndex = 2 then
            sQueryWhere &= IIf(String.IsNullOrEmpty(sQueryWhere), " WHERE ", " AND ")
            sQueryWhere &= "IsHighlighted=1"
        End If
    End Sub

    Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        nSeconds = dbPages.TestVar(Request.Form("Seconds"), 60, 1800, dbPages.TestVar(Request.QueryString("Seconds"), 60, 1800, 600))

        ' Added for fixing <%=nSeconds%> in runat bug
        Dim metaRefresh As New HtmlMeta()
        metaRefresh.HttpEquiv = "refresh"
        metaRefresh.Content = nSeconds & ";url=.?Seconds=" & nSeconds
        Me.Header.Controls.Add(metaRefresh)

        If IsPostBack Then
            Dim querySQL2 As New StringBuilder
            Dim sep As Char() = New Char() {","}
            Dim ps() As String
            If Trim(Request("chkDel")) <> "" Then
                Select Case Request("ActionType")
                    Case "DELETE"
                        ps = Request("chkDel").Split(sep)
                        If ps.Length > 0 Then
                            For Each i In ps
                                dbPages.ExecSql("DELETE FROM tblErrorNet WHERE ID IN (Select ID From fnGetErrorsInSameGroup(" + i.ToString() + "))")
                            Next i
                        End If
                    Case "ARCHIVE"
                        ps = Request("chkDel").Split(sep)
                        If ps.Length > 0 Then
                            For Each i In ps
                                dbPages.ExecSql("UPDATE tblErrorNet SET IsArchive = 1 WHERE ID IN (Select ID From fnGetErrorsInSameGroup(" + i.ToString() + "))")
                            Next i
                        End If
                    Case "RETURN"
                        Dim ll As New List(Of String)
                        ps = Request("chkDel").Split(sep)
                        If ps.Length > 0 Then
                            For Each i In ps
                                If rblArchive.SelectedIndex = "2" Then querySQL2.Append("UPDATE tblErrorNet SET IsHighlighted = 0") _
                                Else querySQL2.Append("UPDATE tblErrorNet SET IsArchive = 0")
                                querySQL2.Append("WHERE ID IN(Select ID FROM fnGetErrorsInSameGroup(" + i.ToString() + "))")
                                dbPages.ExecSql(querySQL2.ToString())
                            Next
                        End If
                End Select
            End If
        End If

        Dim envWhere As String = IIf(Trim(Request("envr")) = "STAGE", "NOT", " ") & "(IsProduction = 1)"
        If Trim(Request("iErrorTime")) <> "" Then sQueryWhere &= sAnd & "(CAST(MaxTime AS DATE) = '" & CType(Request("iErrorTime"), DateTime) & "')" : sAnd = " AND "
        If Trim(Request("iProjectName")) <> "" Then sQueryWhere &= sAnd & "(ProjectName = '" & Trim(Request("iProjectName")) & "')" : sAnd = " AND "
        If Trim(Request("iInnerExceptionSource")) <> "" Then sQueryWhere &= sAnd & "(InnerExceptionSource = '" & Trim(Request("iInnerExceptionSource")) & "')" : sAnd = " AND "
        sQueryWhere &= sAnd & envWhere : sAnd = " AND "


        If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize")
        sQueryString = dbPages.CleanUrl(Request.QueryString)
        If sQueryWhere <> "" Then sQueryWhere = " WHERE " & sQueryWhere
        FilterArchive(Nothing, Nothing)
        Dim nErrorsNew As Integer = dbPages.ExecScalar("SELECT COUNT(ID) FROM tblErrorNet WHERE IsArchive = 0 And " & envWhere)
        litTitleErrors.Text = IIf(nErrorsNew > 0, "(" & nErrorsNew & ")", String.Empty)
        rblArchive.Items(0).Text = "<span class=""archiveOption" & IIf(rblArchive.SelectedIndex = 0, " current", String.Empty) & """>NEW ERRORS</span> (" & nErrorsNew & ")"
        Dim nErrorsArchive As Integer = dbPages.ExecScalar("SELECT COUNT(ID) FROM tblErrorNet WHERE IsArchive = 1 And " & envWhere)
        rblArchive.Items(1).Text = "<span class=""archiveOption" & IIf(rblArchive.SelectedIndex = 1, " current", String.Empty) & """>ARCHIVE</span> (" & nErrorsArchive & ")"
        Dim nErrorsHighlighted As Integer = dbPages.ExecScalar("SELECT COUNT(ID) FROM tblErrorNet WHERE IsArchive = 0 AND IsHighlighted = 1 And " & envWhere)
        rblArchive.Items(2).Text = "<span class=""archiveOption" & IIf(rblArchive.SelectedIndex = 2, " current", String.Empty) & """>HIGHLIGHTED</span> (" & nErrorsHighlighted & ")"
        'lblMainTitle.Text = UCase(dbPages.getConfig("MainTitle"))
    End Sub

    Protected Sub BllLogsRedirect(sender As Object, e As EventArgs)
        Response.Redirect("BllLogs.aspx")
    End Sub

    Protected Sub WebServicesRedirect(sender As Object, e As EventArgs)
        Response.Redirect("WebServicesLogs.aspx")
    End Sub

    Protected Sub LogoutRedirect(sender As Object, e As EventArgs)
        Response.Redirect("Logout.aspx")
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error Logging System - ERRORS (<asp:Literal ID="litTitleErrors" runat="server" />)</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link rel="shortcut icon" type="image/x-icon" href="~/favicon.ico?1=1" />
    <link href="assets/css/StyleEng.css" rel="stylesheet" />

    <%-- jQuery --%>
    <link type="text/css" href="assets/css/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
    
</head>
<body>
    <form id="Form1" runat="server" method="post">
        <asp:Button OnClick="LogoutRedirect" runat="server" Text="LOGOUT" CssClass="btnStandard LogoutRedirectButton" />

        <style>
            input[type="radio"] {
                visibility: hidden;
            }
        </style>
        <table align="center" style="width: 95%">
            <tr>
                <td style="text-align: left; width: 150px;">
                    <select name="envr" class="selStandard" onchange="submit();">
                        <option value="PRODUCTION" <%If Request("envr") = "PRODUCTION" Then%>selected<%End If%>>PRODUCTION</option>
                        <option value="STAGE" <%If Request("envr") = "STAGE" Then%>selected<%End if%>>STAGE</option>
                    </select>
                </td>
                <td style="text-align: left;">
                    <asp:RadioButtonList ID="rblArchive" AutoPostBack="true" RepeatColumns="3" CssClass="radio" runat="server">
                        <asp:ListItem Text="NEW ERRORS" Selected="True" />
                        <asp:ListItem Text="ARCHIVE" />
                        <asp:ListItem Text="HIGHLIGHTED" />
                    </asp:RadioButtonList>
                </td>
                <td style="text-align: right;">
                    <span style="font-size: 11px;">SET REFRESH</span>
                    <select name="Seconds" class="selStandard" onchange="submit();">
                        <option value="60" <%If nSeconds = 60 Then%>selected<%End If%>>Every 1 Min </option>
                        <option value="300" <%If nSeconds = 300 Then%>selected<%End If%>>Every 5 Min </option>
                        <option value="600" <%If nSeconds = 600 Then%>selected<%End if%>>Every 10 Min </option>
                        <option value="1800" <%If nSeconds = 1800 Then%>selected<%End if%>>Every 30 Min </option>
                    </select>
                    &nbsp;&nbsp;&nbsp;
				<input type="button" class="btnStandard" value="REFRESH NOW" onclick="submit();">
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
                <td>
                    <br />
                </td>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 11px; padding-bottom: 4px;">(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');">Collapse All</a>)
                </td>
                <td align="right" style="font-size: 11px;">Filters:
				<%
                    If Trim(Request("iErrorTime")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iErrorTime", Nothing) & "';"">Error Time</a>") : sDivider = ", "
                    If Trim(Request("iProjectName")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iProjectName", Nothing) & "';"">Project Name</a>") : sDivider = ", "
                    If Trim(Request("iInnerExceptionSource")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iInnerExceptionSource", Nothing) & "';"">Inner Exception Source</a>") : sDivider = ", "
                    If sDivider <> "" Then Response.Write(" | (click to remove)") Else Response.Write("Add to filter by clicking on returned fields")
                %>
                </td>
                <td></td>
            </tr>
        </table>
        <table id="formNormal" class="formNormal" align="center" style="width: 95%;">

            <!-- START Coulmn titles -->
            <tr>
                <th colspan="2">
                    <br />
                </th>
                <th>Count (∆)</th>
                <th>Date & Time</th>
                <th>Project Name </th>
                <th>Virtual Path </th>
                <th>Inner Exception Source</th>
                <th>Inner Exception Message</th>

                <th style="text-align: right;">
                    <input type="checkbox" onclick="checkAll();" /></th>
            </tr>
            <!-- END Coulmn titles -->

            <!-- START Short info about the error -->
            <%
                sSQL = "SELECT * FROM vwErrorListInGroup " & sQueryWhere & " ORDER BY MaxTime DESC"
                'Response.Write(sSQL) : Response.End()

                PGData.OpenDataset(sSQL)
                While PGData.Read()
                    i = i + 1

                    sStatusColor = ""
                    Select Case Trim(PGData("ProjectName"))
                        Case "Win Service"
                            sStatusColor = "silver"
                        Case "MerchantControlPanel"
                            sStatusColor = "orange"
                        Case "WebServices"
                            sStatusColor = "green"
                        Case "Payment Page"
                            sStatusColor = "yellow"
                        Case "Dev Center"
                            sStatusColor = "brown"
                        Case "Process"
                            sStatusColor = "#FA4343"
                        Case "AdminCash", "NPAdmincash"
                            sStatusColor = "#4374FA"
                        Case "Admin"
                            sStatusColor = "#4374FA"
                        Case "Admin Reports"
                            sStatusColor = "#4374FA"
                        Case Else
                            sStatusColor = "#ffffff"
                    End Select
            %>

            <tr class="forj"
                style="background-color: <%=iif(PGData("IsHighlighted"),"#7d1b07","#000000")%>;"
                onmouseover="this.style.backgroundColor='<%=IIf(PGData("IsHighlighted"), "#7d1b07", "#555555")%>';"
                onmouseout="this.style.backgroundColor='<%=IIf(PGData("IsHighlighted"), "#7d1b07", "#000000")%>';"
                id="trHead<%=PGData("MaxID")%>">
                <td>
                    <%--<img onclick="ExpandNode('<%=i%>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle">--%>
                    <img onclick="ExpandNode('<%=i%>','<%=PGData("MaxID")%>');" style="cursor: pointer;" id="oListImg<%=i%>" src="Images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle">
                </td>
                <td>
                    <span style="background-color: <%= sStatusColor %>;">&nbsp;&nbsp;</span>
                </td>
                <td nowrap="nowrap">
                    <%=PGData("ErrorCount")%>
                    <%  sTimeDelta = ""
                        If (Conversion.Int(PGData("TimeDelta")) > 0) Then
                            Select Case Conversion.Int(PGData("TimeDelta"))
                                Case Is > 216000
                                    sTimeDelta = "(" & Conversion.Int(PGData("TimeDelta") / 60 / 60 / 24) & " Days)"
                                Case Is > 3600
                                    sTimeDelta = "(" & Conversion.Int(PGData("TimeDelta") / 60 / 60) & " Hours)"
                                Case Is > 60
                                    sTimeDelta = "(" & Conversion.Int(PGData("TimeDelta") / 60) & " min)"
                                Case Is > 0
                                    sTimeDelta = "(" & Conversion.Int(PGData("TimeDelta")) & " sec)"
                            End Select
                        End If
                        Response.Write(sTimeDelta)
                    %>
                </td>
                <td nowrap="nowrap" style="direction: ltr">
                    <%
                        If Trim(Request("iErrorTime")) <> "" Then
                            Response.Write(CType(PGData("MaxTime"), DateTime).ToString("dd/MM/yy HH:mm:ss"))
                        Else
                            dbPages.showFilter(CType(PGData("MaxTime"), DateTime).ToString("dd/MM/yy HH:mm:ss"), "?" & sQueryString & "&iErrorTime=" & CType(PGData("MaxTime"), DateTime).ToString("dd/MM/yy"), 1)
                        End If
                    %>				
                </td>
                <td nowrap="nowrap">
                    <%  If Not IsDBNull(PGData("ProjectName")) Then
                            If Trim(Request("iProjectName")) <> "" Then
                                Response.Write(Trim(PGData("ProjectName")))
                            Else
                                dbPages.showFilter(Trim(PGData("ProjectName")), "?" & sQueryString & "&iProjectName=" & Trim(PGData("ProjectName")), 1)
                            End If
                        End If
                    %>
                </td>
                <td nowrap="nowrap">
                    <%=Server.HtmlEncode(Trim(PGData("VirtualPath")))%>
                    <% If ETSite <> Trim(PGData("VirtualPath")) Then Response.Write(Right(ETSite, Len(ETSite) - InStrRev(ETSite, "\"))) %><br />
                </td>
                <td nowrap="nowrap">
                    <%  If Trim(Request("iInnerExceptionSource")) <> "" Then
                            Response.Write(Trim(PGData("InnerExceptionSource")))
                        Else
                            dbPages.showFilter(PGData("InnerExceptionSource"), "?" & sQueryString & "&iInnerExceptionSource=" & Trim(PGData("InnerExceptionSource")), 1)
                        End If
                    %>
                </td>
                <td><%=Server.HtmlEncode(Left(PGData("InnerExceptionMessage"), 70))%></td>
                <td style="text-align: right;">
                    <input type="checkbox" name="chkDel" value="<%=PGData("MaxID")%>" class="shiftCheckbox" />
                </td>
            </tr>
            <!-- END Short info about the error -->

            <!-- START More info about the error (hidden at first) -->
            <tr id="trInfo<%=i%>" style="display: none;">
                <td colspan="2">&nbsp;</td>
                <td colspan="7">
                    <table border="0" class="formNormal" style="border-top: 1px dashed #4D4D4D; width: 100%;">
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td id="place_error_messeg<%=i%>" style="vertical-align: top;" colspan="3"></td>
                        </tr>
                        <tr>
                            <td height="8"></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: bottom; padding-left: 10px;">
                                <input type="button" class="btnHighlight" name="HighlightRow" value="Highlight" onclick="highLightRow(<%=PGData("MaxID")%>,<%=IIf(PGData("IsHighlighted"), 0, 1)%>)" />
                            </td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- END More info about the error (hidden at first) -->

            <tr>
                <td height="1" colspan="9" bgcolor="#4D4D4D"></td>
            </tr>
            <%  End while
                PGData.CloseDataset()
            %>
        </table>
        <script type="text/javascript">
            function ExpandAll(fDisplay, fImgStatus) {
                for(i = 0; i <= <%= i %>; i++) {
                    trObj = document.getElementById("trInfo" + i)
                    imgObj = document.getElementById("oListImg" + i)
                    if (trObj) {
                        imgObj.src = 'Images/tree_' + fImgStatus + '.gif';
                        trObj.style.display = fDisplay;
                    }
                }
            }
        </script>
        <table align="center" style="width: 95%">
            <tr>
                <td>
                    <UC:Paging runat="Server" ID="PGData"
                        PageID="PageID"
                        Form="Form1"
                        GenerateFieldForID="true"
                        PageSize="20"
                        ShowPageCount="true"
                        ShowPageFirst="true"
                        ShowPageLast="true"
                        ShowPageNumber="false"
                        ShowRecordCount="true"
                        ShowRecordNumbers="true"
                        ShowPageNumbers="true"
                        ShowPageNumbersRange="5"
                        ShowPageNumbersRecords="true"
                        PagerCssClass="pagerBlack" />
                    <asp:HiddenField ID="PageID" runat="server" />
                </td>
                <td style="text-align: right;">
                    <div style="padding-top: 5px;">
                        <%  If rblArchive.SelectedIndex = 0 Then
                                'new errors - archive
                        %>
                        <input type="submit" class="btnStandard" name="ActionType" value="ARCHIVE" />
                        <%  Else
                                'archive - restore or delete
                        %>
                        <input type="submit" class="btnStandard" name="ActionType" value="RETURN" style="width: 50px;" />
                        <%  End If
                        %>
                        <input type="submit" class="btnStandard" name="ActionType" value="DELETE" style="width: 50px;" onclick="return confirm('Are you sure ?!')" />
                    </div>
                </td>
            </tr>
        </table>

        <asp:Button OnClick="BllLogsRedirect" runat="server" Text="BLL LOGS" CssClass="btnStandard RedirectButton" />
        <asp:Button OnClick="WebServicesRedirect" runat="server" Text="Web Services" CssClass="btnStandard WebServicesRedirectButton" />
    </form>

    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui-1.8.10.custom.min.js"></script>
    <netpay:ScriptManager EnableBlocker="false" runat="server" EnableSessionAlerts="true" />
    <script src="assets/js/jquery.shiftcheckbox.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
 
            try {
                $('.shiftCheckbox').shiftcheckbox();
            }
            catch (error) { }
        });
        function IconVisibility(fVar) {
            spanObj = document.getElementById(fVar)
            if (spanObj.style.visibility == 'hidden') {
                spanObj.style.visibility = 'visible';
            }
            else {
                spanObj.style.visibility = 'hidden';
            }
        }
        function ExpandNode(fTrNum,id) 
        {
            trObj = document.getElementById("trInfo" + fTrNum);
            imgObj = document.getElementById("oListImg" + fTrNum);
            var nWidth = Math.round(document.body.clientWidth * 0.92 - 450);
            if (document.getElementById("RequestQueryString" + fTrNum)) document.getElementById("RequestQueryString" + fTrNum).style.width = nWidth;
            if (document.getElementById("RequestForm" + fTrNum)) document.getElementById("RequestForm" + fTrNum).style.width = nWidth;
            if (trObj) {
                if (trObj.style.display == '') 
                {
                    imgObj.src = 'Images/tree_expand.gif';
                    trObj.style.display = 'none';
                    $('#place_error_messeg' + fTrNum).html("");
                }
                else 
                {
                    imgObj.src = 'Images/tree_collapse.gif';

                    ShowErrorMesseg(fTrNum,id);
                    //$(trObj).show('slow', function () { });
					
                    trObj.style.display = '';
                }
            }
        }
        function ShowErrorMesseg(fTrNum, id) {
            var bb = "";
            if ($("#rblArchive_0").attr("checked"))
            { bb = "&rblArchive=newerrors " }
            if ($("#rblArchive_1").attr("checked"))
            { bb = "&rblArchive=archive" }
            if ($("#rblArchive_2").attr("checked"))
            { bb = "&rblArchive=highlighted" }
            //            $("#rblArchive_1").attr("checked")
            //            $("#rblArchive_2").attr("checked")

            
            $.ajax({
                type: 'GET',
                async: false,
                contentType: "utf-8",
                dataType: "text",
                global: false,
                data: "",
                url: 'ErrorMesseg.aspx?rowNum=' + fTrNum+'&errorid=' + id + bb,
                success: function (data) {
                    var d = data;
                    //$('#place_error_messeg').empty();
                    $('#place_error_messeg' + fTrNum).html(d);
                    $('#place_error_messeg').width("width:100%");
                },
                error: function () { alert("error"); }

            });
        }
		
        var checkflag = "false";
        function checkAll() 
        {
            if (checkflag == "false") {
                if(Form1.chkDel) {
                    if(Form1.chkDel.length) {
                        for (i = 0; i < Form1.chkDel.length; i++) 
                            Form1.chkDel[i].checked = true;
                    }
                    else {
                        Form1.chkDel.checked = true
                    }}
                checkflag = "true";
            }
            else {
                if(Form1.chkDel) {
                    if(Form1.chkDel.length) {
                        for (i = 0; i < Form1.chkDel.length; i++) 
                            Form1.chkDel[i].checked = false;
                    }
                    else {
                        Form1.chkDel.checked = false
                    }}
                checkflag = "false";
            }
        }

        function highLightRow(ID, bHighlight)
        {			
            var sHighlight;			
            if (bHighlight==1)	{
                sHighlight = "setHighlight.aspx?id=" + ID + "&hlight=1";
            }
            else {				
                sHighlight = "setHighlight.aspx?id=" + ID + "&hlight=0";
            }
            loadXMLDoc(sHighlight);
        }

        var req;
        function loadXMLDoc(url) {

            $.ajax({
                type: 'GET',
                async: false,
                contentType: "utf-8",
                dataType: "text",
                global: false,
                data: "",
                url: url,
                success: function (data) {
                    window.location = data;
                },
                error: function () { alert("error"); }

            });
        }

        function processReqChange() 
        {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    document.location.href="/";
                } else {
                    alert("There was a problem: " + req.status + ',' + req.statusText);
                }
            }
        }
    </script>
</body>
</html>
