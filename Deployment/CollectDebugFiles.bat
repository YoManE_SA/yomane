Del Output\*.* /q

copy ..\Crypt\bin\Debug\Netpay.Crypt.dll Output\
copy ..\Crypt\bin\Debug\Netpay.Crypt.pdb Output\

copy ..\Process\bin\Debug\Netpay.CommonTypes.dll Output\
copy ..\Process\bin\Debug\Netpay.CommonTypes.pdb Output\

copy ..\Dal\bin\Debug\Netpay.Dal.dll Output\
copy ..\Dal\bin\Debug\Netpay.Dal.pdb Output\

copy ..\Infrastructure\bin\Debug\Netpay.Infrastructure.dll Output\
copy ..\Infrastructure\bin\Debug\Netpay.Infrastructure.pdb Output\

copy ..\Bll\bin\Debug\Netpay.Bll.dll Output\
copy ..\Bll\bin\Debug\Netpay.Bll.pdb Output\

copy ..\Process\bin\Debug\Netpay.Process.dll Output\
copy ..\Process\bin\Debug\Netpay.Process.pdb Output\

copy ..\NetpayConnector\bin\Debug\Netpay.Process.NetpayConnector.dll Output\
copy ..\NetpayConnector\bin\Debug\Netpay.Process.NetpayConnector.pdb Output\

copy ..\LocalEntity\bin\Debug\Netpay.Process.LocalEntity.dll Output\
copy ..\LocalEntity\bin\Debug\Netpay.Process.LocalEntity.pdb Output\

copy ..\LocalOldDb\bin\Debug\Netpay.Process.LocalOldDb.dll Output\
copy ..\LocalOldDb\bin\Debug\Netpay.Process.LocalOldDb.pdb Output\

copy ..\WinService\bin\Debug\Netpay.WinService.exe Output\
copy ..\WinService\bin\Debug\Netpay.WinService.pdb Output\

copy ..\Emails\bin\Debug\Netpay.Emails.dll Output\
copy ..\Emails\bin\Debug\Netpay.Emails.pdb Output\

copy ..\FraudDetection\bin\Debug\Netpay.FraudDetection.dll Output\
copy ..\FraudDetection\bin\Debug\Netpay.FraudDetection.pdb Output\

copy ..\Monitoring\bin\Debug\Netpay.Monitoring.dll Output\
copy ..\Monitoring\bin\Debug\Netpay.Monitoring.pdb Output\

copy ..\DevDocsGenerator\bin\Debug\Netpay.DevDocsGenerator.dll Output\
copy ..\DevDocsGenerator\bin\Debug\Netpay.DevDocsGenerator.pdb Output\
copy ..\DevDocsGenerator\bin\Debug\ABCpdf.dll Output\