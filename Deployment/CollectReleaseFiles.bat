Del Output\*.* /q

copy C:\Netpay\NetPayCrypt\bin\Release\Netpay.Crypt.dll Output\

copy C:\Netpay\Process\bin\Release\Netpay.CommonTypes.dll Output\

copy C:\Netpay\Dal\bin\Release\Netpay.Dal.dll Output\

copy C:\Netpay\Infrastructure\bin\Release\Netpay.Infrastructure.dll Output\

copy C:\Netpay\Bll\bin\Release\Netpay.Bll.dll Output\

copy C:\Netpay\Process\bin\Release\Netpay.Process.dll Output\

copy C:\Netpay\NetpayConnector\bin\Release\Netpay.Process.NetpayConnector.dll Output\

copy C:\Netpay\LocalEntity\bin\Release\Netpay.Process.LocalEntity.dll Output\

copy C:\Netpay\LocalOldDb\bin\Release\Netpay.Process.LocalOldDb.dll Output\

copy C:\Netpay\WinService\bin\Release\Netpay.WinService.exe Output\

copy C:\Netpay\Emails\bin\Release\Netpay.Emails.dll Output\
