﻿using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary> 
    /// 927 - checks that a refund original trx is not chb or retreival requst
    ///  mode: online
    ///  scope: transaction
    /// settings: global
    ///  actions: decline trx
    /// </summary>
    public class RefundPerChargeback : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Online;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Global; }
        }

        public override string Code
        {
            get
            {
                return "DR927";
            }
        }

        public override string Description
        {
            get
            {
                return "Will decline a refund if original transaction was not provided, not found, is CHB or retrieval request.";
            }
        }

        public override void SetDefaults()
        {

        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();
            
            if (context.CreditTypeId == (short)CreditType.Refund)
            {
                if (!context.ReferenceTransactionId.HasValue)
                {
                    // refund trx must have an original trx id
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = context.MerchantId };
                    result.Message = "Original transaction id was not provided for refund transaction.";
                    result.Action = new Action(Action.ActionType.DeclineAuthorization, this, result.Message);
                    results.Add(result);
                }
                else
                {
                    var origTrx = (from t in DataContext.Reader.tblCompanyTransPasses where t.ID == context.ReferenceTransactionId select t).SingleOrDefault();
                    if (origTrx == null)
                    {
                        // original trx not found
                        DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = context.MerchantId };
                        result.Message = $"Original transaction {context.ReferenceTransactionId} was not found for refund transaction.";
                        result.Action = new Action(Action.ActionType.DeclineAuthorization, this, result.Message);
                        results.Add(result);
                    }
                    else
                    {
                        if ((origTrx.IsChargeback.HasValue && origTrx.IsChargeback.Value) || (origTrx.IsRetrievalRequest.HasValue && origTrx.IsRetrievalRequest.Value))
                        {
                            // original trx is chb or retreival requst
                            DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = context.MerchantId };
                            result.Message = $"Original transaction {context.ReferenceTransactionId} for refund transaction is CHB or retreival request.";
                            result.Action = new Action(Action.ActionType.DeclineAuthorization, this, result.Message);
                            results.Add(result);
                        }
                        else
                        {
                            var origTrxChbs = from t in DataContext.Reader.tblCompanyTransPasses where t.OriginalTransID == origTrx.ID select t;
                            foreach (var origTrxChb in origTrxChbs)
                            {
                                if ((origTrxChb.IsChargeback.HasValue && origTrxChb.IsChargeback.Value) || (origTrxChb.IsRetrievalRequest.HasValue && origTrxChb.IsRetrievalRequest.Value))
                                {
                                    // original trx has a trx which is chb or retreival requst
                                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = context.MerchantId };
                                    result.Message = $"Original transaction {origTrx.ID} => {origTrxChb.ID} for refund transaction is CHB or retreival request.";
                                    result.Action = new Action(Action.ActionType.DeclineAuthorization, this, result.Message);
                                    results.Add(result);
                                }
                            }
                        }
                    }
                }
            }

            /*
            var timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Week);
            var chargebacks = from t in dc.DenormalizedTransactions
                       where (t.IsChargeback.Value || t.IsRetrievalRequest.Value) &&
                                t.PaymentMethodID > (byte)CommonTypes.PaymentMethodEnum.MaxInternal &&
                                t.TransactionDate >= timeFrame.From &&
                                t.TransactionDate <= timeFrame.To
                       select t;

            if (chargebacks != null && chargebacks.Count() > 0)
            {
                foreach (var chargeback in chargebacks)
                {
                    var amountFrom = Infrastructure.Math.SubtractPercent(10, chargeback.TransactionAmountUSD.Value);
                    var amountTo = Infrastructure.Math.AddPercent(10, chargeback.TransactionAmountUSD.Value);
                    var foundRefunds = from r in dc.DenormalizedTransactions
                                       where r.IsRefund.Value &&
                                                r.PaymentMethodID > (byte)CommonTypes.PaymentMethodEnum.MaxInternal &&
                                               r.TransactionID != chargeback.TransactionID &&
                                               r.TransactionDate >= timeFrame.From &&
                                               r.TransactionDate <= timeFrame.To &&
                                               r.TransactionDate > chargeback.TransactionDate && // refund must be after chb
                                               r.TerminalNumber == chargeback.TerminalNumber &&
                                               r.TransactionAmountUSD >= amountFrom &&
                                               r.TransactionAmountUSD <= amountTo &&
                                               r.PaymentMethodFirst6Text == chargeback.PaymentMethodFirst6Text &&
                                               r.PaymentMethodLast4Text == chargeback.PaymentMethodLast4Text &&
                                               r.PaymentMethodExpirationDate.Value.Month == chargeback.PaymentMethodExpirationDate.Value.Month &&
                                               r.PaymentMethodExpirationDate.Value.Year == chargeback.PaymentMethodExpirationDate.Value.Year &&
                                               r.PaymentMethodName == chargeback.PaymentMethodName
                                       select r;

                    foreach (var foundRefund in foundRefunds)
                    {
                        DetectionResult result = new DetectionResult() { Rule = this, IsDetected = false, MerchantId = foundRefund.CompanyID };
                        result.IsDetected = true;
                        result.Message = string.Format("Refund trans. id: {0} found for chargeback / retrieval trans. id: {1}", foundRefund.TransactionID, chargeback.TransactionID);
                        result.Action = new Action(Action.ActionType.Alert, this, result.Message);
                        results.Add(result);
                    }
                }
            } 
            */

            if (results.Count() == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }
    }
}
