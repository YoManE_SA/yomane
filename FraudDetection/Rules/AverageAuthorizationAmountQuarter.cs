﻿using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 922.1 - average authorization amount over quarter and month
	/// mode: offline
    /// settings: merchant
	/// actions: alert
    /// </summary>
    public class AverageAuthorizationAmountQuarter : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Merchant; }
        }

        public override string Code
        {
            get
            {
                return "DR922.1";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks daily average authorization amount over month & quarter. Checks authorization average amount over quarter";
            }
        }

        public override void SetDefaults()
        {
            Thresholds = new List<Threshold>();
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Quarter, Type = Threshold.ThresholdType.AmountAverage, Value = 1, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Month, Type = Threshold.ThresholdType.DailyAmountAverage, Value = 2, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Quarter, Type = Threshold.ThresholdType.DailyAmountAverage, Value = 3, Action = Action.ActionType.Alert });
        }

        public override ValidationResult Validate()
        {
            if (Thresholds.Count < 3)
                return new ValidationResult() { IsValid = false, Reason = "3 thresholds required" };
            foreach (Threshold<decimal> threshold in Thresholds)
                if (threshold.Value <= 0)
                    return new ValidationResult() { IsValid = false, Reason = "All values must be positive integers" };

            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

            // month daily average
            Range<DateTime> timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Month);
            var exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { timeframe = timeFrame, MerchntID = RuleMerchantId });
            var totalAmount = exp.Sum(t => t.TransactionAmountUSD).ToDecimal(0);
            var totalDays = (timeFrame.To - timeFrame.From).Days;
            var dailyAverage = totalAmount / totalDays;
            var threshhold = Thresholds.Where(t => t.Period == Threshold.ThresholdPeriod.Month).Single() as Threshold<decimal>;

            if (dailyAverage > threshhold.Value)
            {
                DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                result.Message = $"The daily average amount of approved & declined authorizations from {timeFrame.From.ToString(dateFormat)} to {timeFrame.To.ToString(dateFormat)}, ${dailyAverage.ToString(decimalFormat)} is over ${threshhold.Value.ToString(decimalFormat)}. {threshhold}";
                result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = RuleMerchantId };
                results.Add(result);
            }

            // quarter daily average
            timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Quarter);
            exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { timeframe = timeFrame, MerchntID = RuleMerchantId });
            totalAmount = exp.Sum(t => t.TransactionAmountUSD).ToDecimal(0);
            totalDays = (timeFrame.To - timeFrame.From).Days;
            dailyAverage = totalAmount / totalDays;
            threshhold = Thresholds.Where(t => t.Period == Threshold.ThresholdPeriod.Quarter && t.Type == Threshold.ThresholdType.DailyAmountAverage).Single() as Threshold<decimal>;

            if (dailyAverage > threshhold.Value)
            {
                DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                result.Message = $"The daily average amount of approved & declined authorizations from {timeFrame.From.ToString(dateFormat)} to {timeFrame.To.ToString(dateFormat)}, ${dailyAverage.ToString(decimalFormat)} is over ${threshhold.Value.ToString(decimalFormat)}. {threshhold}";
                result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = RuleMerchantId };
                results.Add(result);
            }

            // quarter trx average
            timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Quarter);
            exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { timeframe = timeFrame, MerchntID = RuleMerchantId });
            var averageTrxAmount = exp.Average(t => t.TransactionAmountUSD).ToDecimal(0);
            threshhold = Thresholds.Where(t => t.Period == Threshold.ThresholdPeriod.Quarter && t.Type == Threshold.ThresholdType.AmountAverage).Single() as Threshold<decimal>;

            if (averageTrxAmount > threshhold.Value)
            {
                DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                result.Message = $"The average authorization amount of approved & declined authorizations from {timeFrame.From.ToString(dateFormat)} to {timeFrame.To.ToString(dateFormat)}, ${averageTrxAmount.ToString(decimalFormat)} is over ${threshhold.Value.ToString(decimalFormat)}. {threshhold}";
                result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = RuleMerchantId };
                results.Add(result);
            }

            if (results.Count == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }
    }
}
