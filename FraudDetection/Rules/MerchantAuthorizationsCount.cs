﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 912 - authorizations count
    /// mode: online
    /// scope: merchant
    /// settings: merchant
    /// periods: hour, day, week, month
    /// threshold: alert, block transaction, block merchant
    /// defaults: minimums
    /// </summary>
    public class MerchantAuthorizationsCount : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Online;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Merchant; }
        }

        public override string Code
        {
            get
            {
                return "DR912";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks if a merchant authorizations count in a period is over the threshold.";
            }
        }

        public override void SetDefaults()
        {
            Thresholds = new List<Threshold>();
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Hour, Type = Threshold.ThresholdType.CountSum, Value = 10, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Hour, Type = Threshold.ThresholdType.CountSum, Value = 20, Action = Action.ActionType.DeclineAuthorization });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Hour, Type = Threshold.ThresholdType.CountSum, Value = 30, Action = Action.ActionType.BlockMerchantRecommendation });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.CountSum, Value = 10, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.CountSum, Value = 20, Action = Action.ActionType.DeclineAuthorization });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.CountSum, Value = 30, Action = Action.ActionType.BlockMerchantRecommendation });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Week, Type = Threshold.ThresholdType.CountSum, Value = 10, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Week, Type = Threshold.ThresholdType.CountSum, Value = 20, Action = Action.ActionType.DeclineAuthorization });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Week, Type = Threshold.ThresholdType.CountSum, Value = 30, Action = Action.ActionType.BlockMerchantRecommendation });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Month, Type = Threshold.ThresholdType.CountSum, Value = 10, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Month, Type = Threshold.ThresholdType.CountSum, Value = 20, Action = Action.ActionType.DeclineAuthorization });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Month, Type = Threshold.ThresholdType.CountSum, Value = 30, Action = Action.ActionType.BlockMerchantRecommendation });
        }

        public override ValidationResult Validate()
        {
            if (Thresholds.Count < 12)
                return new ValidationResult() { IsValid = false, Reason = "12 thresholds required" };
            foreach (Threshold<int> threshold in Thresholds)
                if (threshold.Value <= 0)
                    return new ValidationResult() { IsValid = false, Reason = "All values must be positive integers" };

            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>() { new DetectionResult() { Rule = this, IsDetected = false, MerchantId = RuleMerchantId } };

            var approved = (from t in DataContext.Reader.tblCompanyTransPasses
                        where t.companyID == context.MerchantId && 
                            t.PaymentMethod > (byte)CommonTypes.PaymentMethodEnum.MaxInternal &&
                            t.OriginalTransID == 0 &&
                            t.IsChargeback == false &&
                            t.CreditType > 0 && // exclude refunds
                            t.isTestOnly == false &&
                            t.PayID != ";X;" // exclude gateway
                        group t by t.companyID into g
                        select new
                        {
                            Hour = g.Sum(t => t.InsertDate >= DateTime.Now.AddHours(-1) ? 1 : 0),
                            Day = g.Sum(t => t.InsertDate >= DateTime.Now.AddDays(-1) ? 1 : 0),
                            Week = g.Sum(t => t.InsertDate >= DateTime.Now.AddDays(-7) ? 1 : 0),
                            Month = g.Sum(t => t.InsertDate >= DateTime.Now.AddMonths(-1) ? 1 : 0)
                        }).SingleOrDefault();
            if (approved == null)
                approved = new { Hour = 0, Day = 0, Week = 0, Month = 0 };

            var declined = (from t in DataContext.Reader.tblCompanyTransFails
                        where t.CompanyID.Value == context.MerchantId &&
                            t.PaymentMethod > (byte)CommonTypes.PaymentMethodEnum.MaxInternal &&
                            t.isTestOnly == false &&
                            //t.IsGateway == false &&
                            t.TransType != 1 // exclude declined authorizations
                        group t by t.CompanyID.Value into g
                        select new
                        {
                            Hour = g.Sum(t => t.InsertDate >= DateTime.Now.AddHours(-1) ? 1 : 0),
                            Day = g.Sum(t => t.InsertDate >= DateTime.Now.AddDays(-1) ? 1 : 0),
                            Week = g.Sum(t => t.InsertDate >= DateTime.Now.AddDays(-7) ? 1 : 0),
                            Month = g.Sum(t => t.InsertDate >= DateTime.Now.AddMonths(-1) ? 1 : 0)
                        }).SingleOrDefault();
            if (declined == null)
                declined = new { Hour = 0, Day = 0, Week = 0, Month = 0 };

            var data = new
            {
                Hour = approved.Hour + declined.Hour + 1,
                Day = approved.Day + declined.Day + 1,
                Week = approved.Week + declined.Week + 1,
                Month = approved.Month + declined.Month + 1
            };

            Threshold<int> detectedThreshold = null;
            bool isDetected = FindThreshold(data, Thresholds, out detectedThreshold);

            if (isDetected)
            {
                results.Single().IsDetected = true;
                var timeFrame = GetTimeFrame(detectedThreshold.Period);
                results.Single().Message = string.Format("The count of approved & declined authorizations from {0} to {1}, {2} is over {3}. {4}", timeFrame.From.ToString(dateFormat), timeFrame.To.ToString(dateFormat), detectedThreshold.DetectedValue, detectedThreshold.Value, detectedThreshold);
                results.Single().Action = new Action(detectedThreshold.Action, this, results.Single().Message) { MerchantId = RuleMerchantId };
            }

            return results;
        }
    }
}
