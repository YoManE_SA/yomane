﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    public class CreditcardToIpUsage : DetectionRuleBase
    {
        public override string Code
        {
            get
            {
                return "DR944.1";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks different creditcards per same ip and vice versa, in the last 5 minutes. If an IP is used with 2 or more different cards, the IP is blocked. If a card is used from 2 or more different IPs, the card is blocked. Timeframe is 5 minutes";
            }
        }

        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Online;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get
            {
                return DetectionRuleSettingsScope.Global;
            }
        }

        private class CacheItem
        {
            public string Ip { get; set; }
            public string Card { get; set; }
            public DateTime Time { get; set; }
        }

        private static List<CacheItem> _cache = new List<CacheItem>();

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            if (context.CustomerIp != null && context.CustomerIp.Trim() != string.Empty && context.CreditcardNumber != null && context.CreditcardNumber.Trim() != string.Empty)
            {
                // remove old entries from cach and add new entry if distinct
                _cache = _cache.Where(c => c.Time > DateTime.Now.AddMinutes(-5)).ToList();
                bool isInCache = _cache.Where(c => c.Card == context.CreditcardNumber && c.Ip == context.CustomerIp).Any();
                if (!isInCache)
                    _cache.Add(new CacheItem() { Ip = context.CustomerIp, Card = context.CreditcardNumber, Time = DateTime.Now });
                
                // check ip per cc
                int sameIpCount = _cache.Where(c => c.Ip == context.CustomerIp).Count();
                if (sameIpCount > 1)
                {
                    DetectionResult result = new DetectionResult() { IsDetected = true, MerchantId = context.MerchantId, Rule = this };
                    result.Message = string.Format("IP {0} was used with {1} different cards", context.CustomerIp, sameIpCount);
                    result.Action = new Action(Action.ActionType.BlockIp, this, result.Message) { IP = context.CustomerIp };
                    results.Add(result);
                }

                // check cc per ip
                int sameCardCount = _cache.Where(c => c.Card == context.CreditcardNumber).Count();
                if (sameCardCount > 1)
                {
                    DetectionResult result = new DetectionResult() { IsDetected = true, MerchantId = context.MerchantId, Rule = this };
                    result.Message = string.Format("Card {0} was used with {1} different IPs", context.CreditcardDisplay, sameCardCount);
                    result.Action = new Action(Action.ActionType.BlockCard, this, result.Message) { CardNumber = context.CreditcardNumber };
                    results.Add(result);
                }
            }
            
            if (results.Count() == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }

        public override void SetDefaults()
        {
            return;
        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }
    }
}
