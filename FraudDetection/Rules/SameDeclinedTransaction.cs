﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 915 - same declined authorization in the last day
	/// mode: online
    /// scope: global
    /// settings: global
    /// actions: decline transaction 
    /// comparison params: cc number, cc date, cc name, mcc, amount +-10%, currency
    /// </summary>
    public class SameDeclinedTransaction : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Online;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get
            {
                return DetectionRuleSettingsScope.Global;
            }
        }

        public override string Code
        {
            get
            {
                return "DR915";
            }
        }

        public override string Description
        {
            get
            {
                return "Searches for a declined authorization in the last day that is similar to this authorization. If found, this transaction is declined.";
            }
        }

        public override void SetDefaults()
        {
        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            var amountFrom = Infrastructure.Math.SubtractPercent(10, context.Amount);
            var amountTo = Infrastructure.Math.AddPercent(10, context.Amount);
            var query = from trans in DataContext.Reader.tblCompanyTransFails
                        join card in DataContext.Reader.tblCreditCards on trans.CreditCardID equals card.ID
                        join currency in DataContext.Reader.CurrencyLists on trans.Currency equals currency.CurrencyID
                        //join terminal in DataContext.Reader.tblDebitTerminals on trans.TerminalNumber equals terminal.terminalNumber
                        where
                        trans.isTestOnly == false &&
                        trans.PaymentMethod > (byte)CommonTypes.PaymentMethodEnum.MaxInternal &&
                        trans.InsertDate >= DateTime.Now.AddDays(-1) &&
                        DataContext.Reader.GetDecrypted256(card.CCard_number256) == context.CreditcardNumber &&
                        card.ExpMM == context.CreditcardExpirationMM &&
                        card.ExpYY == context.CreditcardExpirationYY &&
                        card.Member == context.CreditcardName &&
                        trans.Amount >= amountFrom &&
                        trans.Amount <= amountTo &&
                        //terminal.dt_mcc == context.mcc &&
                        currency.CurrencyISOCode == context.TransCurrency.IsoCode
                select trans;

            if (query.Count() > 0)
            {
                string message = "Same declined authorization found in the last day";
                DetectionResult resultBlock = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = context.MerchantId };
                resultBlock.Action = new Action(Action.ActionType.DeclineAuthorization, this, message) { MerchantId = context.MerchantId };
                results.Add(resultBlock);
            }

            if (results.Count == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }
    }
}
