﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 924.2 - authorizations count
	/// mode: online
    /// scope: card
    /// settings: global
    /// periods: today, today -1, today -2 (dd, dd + dd-1, dd + dd-2 + dd-3: sum amount & sum count)
	/// thresholds: 3
    /// actions: block transaction if any fail
    /// defaults: minimums
    /// </summary>
    public class CardAuthorizationsAmount : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Online;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get
            {
                return DetectionRuleSettingsScope.Global;
            }
        }

        public override string Code
        {
            get
            {
                return "DR924.2";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks that the card authorizations amount + this transaction amount is not over the period threshold.";
            }
        }

        public override void SetDefaults()
        {
            Thresholds = new List<Threshold>();
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Day1, Type = Threshold.ThresholdType.AmountSum, Value = 1, Action = Action.ActionType.DeclineAuthorization });
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Day2, Type = Threshold.ThresholdType.AmountSum, Value = 2, Action = Action.ActionType.DeclineAuthorization });
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Day3, Type = Threshold.ThresholdType.AmountSum, Value = 3, Action = Action.ActionType.DeclineAuthorization });
        }

        public override ValidationResult Validate()
        {
            if (Thresholds.Count < 3)
                return new ValidationResult() { IsValid = false, Reason = "3 thresholds required" };
            foreach (Threshold<decimal> threshold in Thresholds)
                if (threshold.Value <= 0)
                    return new ValidationResult() { IsValid = false, Reason = "All values must be positive integers" };

            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>() { new DetectionResult() { Rule = this, IsDetected = false, MerchantId = context.MerchantId } };

            if (context.CreditcardNumber != null && context.CreditcardNumber.Trim() != string.Empty)
            {
                var dayTimeframe1 = GetTimeFrame(Threshold.ThresholdPeriod.Day1);
                var dayTimeframe2 = GetTimeFrame(Threshold.ThresholdPeriod.Day2);
                var dayTimeframe3 = GetTimeFrame(Threshold.ThresholdPeriod.Day3);

                decimal usdRate = (from c in DataContext.Reader.CurrencyLists where c.CurrencyISOCode == "USD" select c.BaseRate.Value).Single();

                var approvedTrans = from trans in DataContext.Reader.tblCompanyTransPasses
                                    join curr in DataContext.Reader.CurrencyLists on trans.Currency equals curr.CurrencyID
                                    join card in DataContext.Reader.TransPaymentMethods on trans.TransPaymentMethod_id equals card.TransPaymentMethod_id
                                    where card.CreditCard_id != null &&
                                        trans.PaymentMethod > (byte)CommonTypes.PaymentMethodEnum.MaxInternal &&
                                        trans.OriginalTransID == 0 &&
                                        trans.InsertDate >= dayTimeframe3.From &&
                                        trans.InsertDate <= dayTimeframe3.To &&
                                        card.Value1Encrypted != null &&
                                        card.Value1Encrypted.Equals(new System.Data.Linq.Binary(context.CreditcardNumberEncrypted))
                                    select new
                                    {
                                        InsertDate = trans.InsertDate,
                                        Amount = trans.Amount * curr.BaseRate.Value / usdRate,
                                        Card = context.CreditcardNumber
                                    };

                var approvedSum = (from trans in approvedTrans
                                   group trans by trans.Card into g
                                   select new
                                   {
                                       Day1 = g.Sum(t => t.InsertDate >= dayTimeframe1.From && t.InsertDate <= dayTimeframe1.To ? t.Amount : 0),
                                       Day2 = g.Sum(t => t.InsertDate >= dayTimeframe2.From && t.InsertDate <= dayTimeframe2.To ? t.Amount : 0),
                                       Day3 = g.Sum(t => t.InsertDate >= dayTimeframe3.From && t.InsertDate <= dayTimeframe3.To ? t.Amount : 0)
                                   }).SingleOrDefault();
                if (approvedSum == null)
                    approvedSum = new { Day1 = 0m, Day2 = 0m, Day3 = 0m };

                var declinedTrans = from trans in DataContext.Reader.tblCompanyTransFails
                                    join curr in DataContext.Reader.CurrencyLists on trans.Currency equals curr.CurrencyID
                                    join card in DataContext.Reader.TransPaymentMethods on trans.TransPaymentMethod_id equals card.TransPaymentMethod_id
                                    where card.CreditCard_id != null &&
                                        trans.PaymentMethod > (byte)CommonTypes.PaymentMethodEnum.MaxInternal &&
                                        trans.TransType != 1 && // exclude declined authorizations
                                        card.Value1Encrypted != null &&
                                        trans.InsertDate >= dayTimeframe3.From &&
                                        trans.InsertDate <= dayTimeframe3.To &&
                                        DataContext.Reader.GetDecrypted256(card.Value1Encrypted) == context.CreditcardNumber
                                    select new
                                    {
                                        InsertDate = trans.InsertDate,
                                        Amount = trans.Amount * curr.BaseRate.Value / usdRate,
                                        Card = context.CreditcardNumber
                                    };

                var declinedSum = (from trans in declinedTrans
                                   group trans by trans.Card into g
                                   select new
                                   {
                                       Day1 = g.Sum(t => t.InsertDate >= dayTimeframe1.From && t.InsertDate <= dayTimeframe1.To ? t.Amount : 0),
                                       Day2 = g.Sum(t => t.InsertDate >= dayTimeframe2.From && t.InsertDate <= dayTimeframe2.To ? t.Amount : 0),
                                       Day3 = g.Sum(t => t.InsertDate >= dayTimeframe3.From && t.InsertDate <= dayTimeframe3.To ? t.Amount : 0)
                                   }).SingleOrDefault();
                if (declinedSum == null)
                    declinedSum = new { Day1 = 0m, Day2 = 0m, Day3 = 0m };

                var data = new
                {
                    Day1 = approvedSum.Day1 + declinedSum.Day1 + context.AmountUsd,
                    Day2 = approvedSum.Day2 + declinedSum.Day2 + context.AmountUsd,
                    Day3 = approvedSum.Day3 + declinedSum.Day3 + context.AmountUsd
                };

                Threshold<decimal> detectedThreshold = null;
                bool isDetected = FindThreshold(data, Thresholds, out detectedThreshold); 

                if (isDetected)
                {
                    results.Single().IsDetected = true;
                    var timeFrame = GetTimeFrame(detectedThreshold.Period);
                    string cardDisplay = Netpay.Bll.PaymentMethods.CreditCard.FromCardNumber(context.CreditcardNumber).Display;
                    results.Single().Message = string.Format("The amount of approved & declined authorizations for card {0} from {1} to {2}, ${3} is over ${4}. {5}", cardDisplay, timeFrame.From.ToString(dateFormat), timeFrame.To.ToString(dateFormat), detectedThreshold.DetectedValue.ToString(decimalFormat), detectedThreshold.Value.ToString(decimalFormat), detectedThreshold);
                    results.Single().Action = new Action(detectedThreshold.Action, this, results.Single().Message) { MerchantId = context.MerchantId };
                }
            }

            return results;
        }
    }
}
