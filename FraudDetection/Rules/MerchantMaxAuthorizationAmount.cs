﻿using Netpay.Bll;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 914 - max authorization amount
	/// mode: online
    /// scope: merchant
    /// settings: merchant
    /// thresholds: 2
	/// actions: alert, decline
    /// defaults: minimums
    /// </summary>
    public class MerchantMaxAuthorizationAmount : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Online;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Merchant; }
        }

        public override string Code
        {
            get
            {
                return "DR914";
            }
        }

        public override string Description
        {
            get
            {
                return "The merchant authorization amount limit.";
            }
        }

        public override void SetDefaults()
        {
            Thresholds = new List<Threshold>();
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Unknown, Type = Threshold.ThresholdType.AmountMax, Value = 10, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Unknown, Type = Threshold.ThresholdType.AmountMax, Value = 20, Action = Action.ActionType.DeclineAuthorization });
        }

        public override ValidationResult Validate()
        {
            if (Thresholds.Count < 2)
                return new ValidationResult() { IsValid = false, Reason = "2 thresholds required" };
            foreach (Threshold<decimal> threshold in Thresholds)
                if (threshold.Value <= 0)
                    return new ValidationResult() { IsValid = false, Reason = "All values must be positive integers" };

            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>() { new DetectionResult() { Rule = this, IsDetected = false, MerchantId = RuleMerchantId } };

            Currency currencyFrom = Currency.Get(context.TransCurrency.IsoCode);
            Currency currencyTo = Currency.Get("USD");
            decimal amountUsd = Currency.Convert(context.Amount, currencyFrom, currencyTo);

            Threshold<decimal> detectedThreshold = null;
            bool isDetected = FindThreshold(amountUsd, Thresholds, out detectedThreshold);

            if (isDetected)
            {
                results.Single().IsDetected = true;
                results.Single().Message = string.Format("Authorization amount ${0} is over ${1}", detectedThreshold.DetectedValue.ToString(decimalFormat), detectedThreshold.Value.ToString(decimalFormat)); 
                results.Single().Action = new Action(detectedThreshold.Action, this, results.Single().Message) { MerchantId = RuleMerchantId };
            }

            return results;
        }
    }
}
