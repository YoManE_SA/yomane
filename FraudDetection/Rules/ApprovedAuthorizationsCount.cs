﻿using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 921.2 - approved authorizations count
	/// mode: offline
    /// scope: merchant
    /// settings: merchant
    /// periods: day, week, month, quarter
    /// defaults: minimums
    /// actions: alert, block merchant
    /// define 2 thresholds for each period,
    /// each threshold with its own action.
    /// </summary>
    public class ApprovedAuthorizationsCount : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }


        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Merchant; }
        }

        public override string Code
        {
            get
            {
                return "DR921.2";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks that the merchant approved authorizations count in period is not over a threshold.";
            }
        }

        public override void SetDefaults()
        {
            Thresholds = new List<Threshold>();
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.CountSum, Value = 10, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.CountSum, Value = 20, Action = Action.ActionType.BlockMerchantRecommendation });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Week, Type = Threshold.ThresholdType.CountSum, Value = 10, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Week, Type = Threshold.ThresholdType.CountSum, Value = 20, Action = Action.ActionType.BlockMerchantRecommendation });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Month, Type = Threshold.ThresholdType.CountSum, Value = 10, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Month, Type = Threshold.ThresholdType.CountSum, Value = 20, Action = Action.ActionType.BlockMerchantRecommendation });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Quarter, Type = Threshold.ThresholdType.CountSum, Value = 10, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Quarter, Type = Threshold.ThresholdType.CountSum, Value = 20, Action = Action.ActionType.BlockMerchantRecommendation });
        }

        public override ValidationResult Validate()
        {
            if (Thresholds.Count < 8)
                return new ValidationResult() { IsValid = false, Reason = "8 thresholds required" };
            foreach (Threshold<int> threshold in Thresholds)
                if (threshold.Value <= 0)
                    return new ValidationResult() { IsValid = false, Reason = "All values must be positive integers" };

            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>() { new DetectionResult() { Rule = this, IsDetected = false, MerchantId = RuleMerchantId } };

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
            var dayTimeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Day);
            var weekTimeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Week);
            var monthTimeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Month);
            var quarterTimeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Quarter);
            var data = (from t in dc.DenormalizedTransactions
                            where t.CompanyID == RuleMerchantId && t.TransactionStatus == transactionStatusApproved &&
                                t.PaymentMethodID > (byte)CommonTypes.PaymentMethodEnum.MaxInternal
                            group t by t.CompanyID into g
                            select new
                            {
                                Day = g.Sum(t => t.TransactionDate >= dayTimeFrame.From && t.TransactionDate <= dayTimeFrame.To ? 1 : 0),
                                Week = g.Sum(t => t.TransactionDate >= weekTimeFrame.From && t.TransactionDate <= weekTimeFrame.To ? 1 : 0),
                                Month = g.Sum(t => t.TransactionDate >= monthTimeFrame.From && t.TransactionDate <= monthTimeFrame.To ? 1 : 0),
                                Quarter = g.Sum(t => t.TransactionDate >= quarterTimeFrame.From && t.TransactionDate <= quarterTimeFrame.To ? 1 : 0)
                            }).SingleOrDefault();
            if (data == null)
                data = new { Day = 0, Week = 0, Month = 0, Quarter = 0 };

            Threshold<int> detectedThreshold = null;
            bool isDetected = FindThreshold(data, Thresholds, out detectedThreshold);
            
            if (isDetected)
            {
                results.Single().IsDetected = true;
                var timeFrame = GetTimeFrame(detectedThreshold.Period);
                results.Single().Message = string.Format("The count of approved authorizations from {0} to {1}, {2} is over {3}. {4}", timeFrame.From.ToString(dateFormat), timeFrame.To.ToString(dateFormat), detectedThreshold.DetectedValue, detectedThreshold.Value, detectedThreshold);
                results.Single().Action = new Action(detectedThreshold.Action, this, results.Single().Message) { MerchantId = RuleMerchantId };
            }

            return results;
        }
    }
}
