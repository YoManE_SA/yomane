﻿using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 922 - average authorization amount
	/// mode: offline
    /// scope: merchant
    /// settings: merchant
    /// compare average threshold with average last day transaction amount
    /// thresholds: 2
	/// actions: alert, merchant block
    /// defaults: minimums
    /// </summary>
    public class AverageAuthorizationAmount : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Merchant; }
        }

        public override string Code
        {
            get
            {
                return "DR922";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks that the average last day transaction amount is not over a threshold.";
            }
        }

        public override void SetDefaults()
        {
            Thresholds = new List<Threshold>();
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.AmountAverage, Value = 1, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.AmountAverage, Value = 2, Action = Action.ActionType.BlockMerchantRecommendation });
        }

        public override ValidationResult Validate()
        {
            if (Thresholds.Count < 2)
                return new ValidationResult() { IsValid = false, Reason = "2 thresholds required" };
            foreach (Threshold<decimal> threshold in Thresholds)
                if (threshold.Value <= 0)
                    return new ValidationResult() { IsValid = false, Reason = "All values must be positive integers" };

            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>() { new DetectionResult() { Rule = this, IsDetected = false, MerchantId = RuleMerchantId } };

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
            Range<DateTime> timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Day);

            var data = (from t in GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { timeframe = timeFrame })
                        where t.CompanyID == RuleMerchantId
                            group t by t.CompanyID into g
                            select new
                            {
                                Day = g.Average(t => t.TransactionAmountUSD)
                            }).SingleOrDefault();
            if (data == null)
                return results;

            Threshold<decimal> detectedThreshold = null;
            bool isDetected = FindThreshold(data, Thresholds, out detectedThreshold);

            if (isDetected)
            {
                results.Single().IsDetected = true;
                results.Single().Message = string.Format("The average amount of approved & declined authorizations from {0} to {1}, ${2} is over ${3}. {4}", timeFrame.From.ToString(dateFormat), timeFrame.To.ToString(dateFormat), detectedThreshold.DetectedValue.ToString(decimalFormat), detectedThreshold.Value.ToString(decimalFormat), detectedThreshold); 
                results.Single().Action = new Action(detectedThreshold.Action, this, results.Single().Message) { MerchantId = RuleMerchantId };
            }

            return results;
        }
    }
}
