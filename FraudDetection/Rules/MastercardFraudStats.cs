﻿using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 931.3 - mastercard tc40 / fraud stats in last month
    ///  mode: offline
    ///  desc: 
    /// if all 3 thresholds in a level are passed, alert indicating the level
    /// 1. 	fraud count > 3
    ///  fraud amount > 3000 usd
    ///  fraud to authorizations ratio > 3% < 5% 
    /// 
    /// 2. 	fraud count > 4
    ///  fraud amount > 4000 usd
    /// fraud to authorizations ratio > 5% < 8%
    /// 
    /// 3. 	fraud count > 5
    ///  amount > 5000 usd
    /// fraud to authorizations ratio > 8%
    /// </summary>
    public class MastercardFraudStats : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Global; }
        }

        public override string Code
        {
            get
            {
                return "DR931.3";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks if all 3 thresholds in a level are passed, if so an alert is sent indicating the level. Level 1: fraud count > 3, fraud amount > 3000, fraud to authorizations ratio > 3% < 5%. Level 2: fraud count > 4, fraud amount > 4000, fraud to authorizations ratio > 5% < 8%. Level 3: fraud count > 5, amount > 5000, fraud to authorizations ratio > 8%.";
            }
        }

        public override void SetDefaults()
        {

        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }

        private class StatsHolder
        {
            public int MerchantId;
            public string TerminalNumber;
            public int AuthorizationsCount;
            public int FraudCount;
            public decimal FraudAmount;
        }

        private List<DetectionResult> DetectByMerchant(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();
            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

            DateTime date = DateTime.Now.AddMonths(-1);
            Range<DateTime> timeframe = new Range<DateTime>(new DateTime(date.Year, date.Month, 1).MinTime(), new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month)).MaxTime());
            //timeframe.From = timeframe.From.AddYears(-5);

            // approved authorization stats
            var exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { paymentMethod = PaymentMethodEnum.CCMastercard, timeframe = timeframe });
            var authStats = exp.GroupBy(t => t.CompanyID).Select(g => new StatsHolder
            {
                MerchantId = g.Key.Value,
                AuthorizationsCount = g.Count(),
                FraudCount = 0,
                FraudAmount = 0
            }).ToList();

            // fraud stats
            // 7 / 8 = set / unset fraud
            string sql = $@"WITH histories AS
                            (
                                 SELECT ROW_NUMBER() OVER
                                     (
                                         PARTITION BY [TransactionID]
                                         ORDER BY [HistoryInsertDate] DESC
                                     ) AS ordinal, *
                                 FROM [DenormalizedTransactionHistory]
	                             WHERE ([HistoryTypeID] = 7 or [HistoryTypeID] = 8) AND 
                                    TransactionStatus = '{transactionStatusApproved}' AND
                                    CAST([HistoryInsertDate] AS DATE) >= '{ timeframe.From.ToString("yyyy-MM-dd") }' AND 
                                    CAST([HistoryInsertDate] AS DATE) <= '{ timeframe.To.ToString("yyyy-MM-dd") }' AND
                                    TransactionPaymentMethodID = {(int)PaymentMethodEnum.CCMastercard} AND
                                    AcquiringBankID > 1
                            )
                            SELECT [MerchantID], COUNT(1) AS FraudCount, SUM(TransactionAmountUSD) AS FraudAmount, 0 AS AuthorizationsCount
                            FROM histories
                            WHERE ordinal = 1 AND [HistoryTypeID] = 7
							GROUP BY [MerchantID]";
            var fraudStats = dc.ExecuteQuery<StatsHolder>(sql, new object[] { }).ToList();

            // merge
            foreach (var authStat in authStats)
            {
                var fraudStat = fraudStats.Where(fs => fs.MerchantId == authStat.MerchantId).SingleOrDefault();
                if (fraudStat != null)
                {
                    authStat.FraudCount = fraudStat.FraudCount;
                    authStat.FraudAmount = fraudStat.FraudAmount;
                }
            }

            // remove 0 frauds
            authStats = authStats.Where(s => s.FraudCount > 0).ToList();

            // detect
            foreach (var merchant in authStats)
            {
                var fraudRatio = Infrastructure.Math.GetPercentage(merchant.FraudCount, merchant.AuthorizationsCount);
                if (merchant.FraudCount > 3 && merchant.FraudAmount > 3000 && fraudRatio > 3)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = string.Format("Level 1 - Mastercard fraud count {0} > 3, amount {1} USD > 3000.00 USD, ratio %{2} > %3.00 and  from {3} to {4})", merchant.FraudCount, merchant.FraudAmount.ToString(decimalFormat), fraudRatio.ToString(decimalFormat), timeframe.From.ToString(dateFormat), timeframe.To.ToString(dateFormat));
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }
                else if (merchant.FraudCount > 4 && merchant.FraudAmount > 4000 && fraudRatio > 5)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = string.Format("Level 2 - Mastercard fraud count {0} > 4, amount {1} USD > 4000.00 USD, ratio %{2} > %5.00 and  from {3} to {4})", merchant.FraudCount, merchant.FraudAmount.ToString(decimalFormat), fraudRatio.ToString(decimalFormat), timeframe.From.ToString(dateFormat), timeframe.To.ToString(dateFormat));
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }
                else if (merchant.FraudCount > 5 && merchant.FraudAmount > 5000 && fraudRatio > 8)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = string.Format("Level 3 - Mastercard fraud count {0} > 5, amount {1} USD > 5000.00 USD, ratio %{2} > %8.00 and  from {3} to {4})", merchant.FraudCount, merchant.FraudAmount.ToString(decimalFormat), fraudRatio.ToString(decimalFormat), timeframe.From.ToString(dateFormat), timeframe.To.ToString(dateFormat));
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }
            }

            return results;
        }

        private List<DetectionResult> DetectByTerminal(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();
            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

            DateTime date = DateTime.Now.AddMonths(-1);
            Range<DateTime> timeframe = new Range<DateTime>(new DateTime(date.Year, date.Month, 1).MinTime(), new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month)).MaxTime());
            //timeframe.From = timeframe.From.AddYears(-5);

            // authorization stats
            var exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { paymentMethod = PaymentMethodEnum.CCMastercard, timeframe = timeframe });
            var authStats = exp.GroupBy(t => t.TerminalNumber).Select(g => new StatsHolder
            {
                TerminalNumber = g.Key,
                AuthorizationsCount = g.Count(),
                FraudCount = 0,
                FraudAmount = 0
            }).ToList();

            // fraud stats
            // 7 / 8 = set / unset fraud
            string sql = $@"WITH histories AS
                            (
                                 SELECT ROW_NUMBER() OVER
                                     (
                                         PARTITION BY [TransactionID]
                                         ORDER BY [HistoryInsertDate] DESC
                                     ) AS ordinal, *
                                 FROM [DenormalizedTransactionHistory]
	                             WHERE ([HistoryTypeID] = 7 or [HistoryTypeID] = 8) AND 
                                    CAST([HistoryInsertDate] AS DATE) >= '{ timeframe.From.ToString("yyyy-MM-dd") }' AND 
                                    CAST([HistoryInsertDate] AS DATE) <= '{ timeframe.To.ToString("yyyy-MM-dd") }' AND
                                    TransactionPaymentMethodID = {(int)PaymentMethodEnum.CCMastercard} AND
                                    AcquiringBankID > 1
                            )
                            SELECT TerminalNumber, COUNT(1) AS FraudCount, SUM(TransactionAmountUSD) AS FraudAmount, 0 AS AuthorizationsCount, 0 AS MerchantId
                            FROM histories
                            WHERE ordinal = 1 AND [HistoryTypeID] = 7
							GROUP BY [TerminalNumber]";
            var fraudStats = dc.ExecuteQuery<StatsHolder>(sql, new object[] { }).ToList();
            
            // merge
            foreach (var authStat in authStats)
            {
                var fraudStat = fraudStats.Where(fs => fs.TerminalNumber == authStat.TerminalNumber).SingleOrDefault();
                if (fraudStat != null)
                {
                    authStat.FraudCount = fraudStat.FraudCount;
                    authStat.FraudAmount = fraudStat.FraudAmount;
                }
            }

            // remove 0 frauds
            authStats = authStats.Where(s => s.FraudCount > 0).ToList();

            // detect
            foreach (var terminal in authStats)
            {
                var fraudRatio = Infrastructure.Math.GetPercentage(terminal.FraudCount, terminal.AuthorizationsCount);
                if (terminal.FraudCount > 3 && terminal.FraudAmount > 3000 && fraudRatio > 3)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Level 1 - Terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' Mastercard fraud count {terminal.FraudCount} > 3, amount {terminal.FraudAmount.ToString(decimalFormat)} USD > 3000.00 USD, ratio %{fraudRatio.ToString(decimalFormat)} > %3.00 and from {timeframe.From.ToString(dateFormat)} to {timeframe.To.ToString(dateFormat)})";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }
                else if (terminal.FraudCount > 4 && terminal.FraudAmount > 4000 && fraudRatio > 5)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Level 2 - Terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' Mastercard fraud count {terminal.FraudCount} > 4, amount {terminal.FraudAmount.ToString(decimalFormat)} USD > 4000.00 USD, ratio %{fraudRatio.ToString(decimalFormat)} > %5.00 and  from {timeframe.From.ToString(dateFormat)} to {timeframe.To.ToString(dateFormat)})";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }
                else if (terminal.FraudCount > 5 && terminal.FraudAmount > 5000 && fraudRatio > 8)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Level 3 - Terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' Mastercard fraud count {terminal.FraudCount} > 5, amount {terminal.FraudAmount.ToString(decimalFormat)} USD > 5000.00 USD, ratio %{fraudRatio.ToString(decimalFormat)} > %8.00 and  from {timeframe.From.ToString(dateFormat)} to {timeframe.To.ToString(dateFormat)})";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }
            }

            return results;
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();
            results.AddRange(DetectByMerchant(context));
            results.AddRange(DetectByTerminal(context));

            if (results.Count() == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }
    }
}
