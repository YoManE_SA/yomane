﻿using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 925.2 checks that refund amounts in month are not over approved trans amounts.
    ///  mode: offline
    ///  scope: merchant
    ///  settings: global
    ///  periods: month
    ///  thresholds: 0
    /// 	actions: alert & merchant block
    ///  defaults: minimums
    /// </summary>
    public class RefundAmountOverApproved : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Global; }
        }

        public override string Code
        {
            get
            {
                return "DR925.2";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks that refund amounts in a rolling month are not over the approved authorizations amounts. If so the merchant is block recommended.";
            }
        }

        public override void SetDefaults()
        {
        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
            var timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Month); 
            var data = from t in dc.DenormalizedTransactions
                        where
                            t.CompanyID != null &&
                            t.PaymentMethodID > (byte)CommonTypes.PaymentMethodEnum.MaxInternal &&
                            t.TransactionStatus == transactionStatusApproved &&
                            !t.IsGateway.Value &&
                            !t.IsChargeback.Value &&
                            t.TransactionDate >= timeFrame.From &&
                            t.TransactionDate <= timeFrame.To
                        group t by t.CompanyID into g
                        select new
                        {
                            MerchantId = g.Key.Value,
                            ApprovedAmount = g.Sum(t => t.IsRefund == false ? t.TransactionAmountUSD.Value : 0m),
                            RefundsAmount = g.Sum(t => t.IsRefund == true ? t.TransactionAmountUSD.Value : 0m)
                        };
            if (data == null)
                return results;
            
            foreach (var merchant in data)
            {
                if (merchant.RefundsAmount > merchant.ApprovedAmount)
                {
                    if (merchant.RefundsAmount - merchant.ApprovedAmount > 1000)
                    {
                        string message = string.Format("The amount of approved refunds from {0} to {1}, ${2} > amount of approved authorizations ${3}", timeFrame.From.ToString(dateFormat), timeFrame.To.ToString(dateFormat), merchant.RefundsAmount.ToString(decimalFormat), merchant.ApprovedAmount.ToString(decimalFormat));
                        DetectionResult resultBlock = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId, Message = message };
                        resultBlock.Action = new Action(Action.ActionType.BlockMerchantRecommendation, this, message) { MerchantId = merchant.MerchantId };
                        results.Add(resultBlock);
                    }
                }
            }

            if (results.Count == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }
    }
}
