﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    public class CreditcardToEmailUsage : DetectionRuleBase
    {
        public override string Code
        {
            get
            {
                return "DR944.2";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks different creditcards per same email and vice versa. If an email uses 2 or more different cards, the email is blocked. If a card uses 2 or more emails, the card is blocked. This check has no time frame.";
            }
        }

        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Online;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get
            {
                return DetectionRuleSettingsScope.Global;
            }
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            if (context.CreditcardEmail != null && context.CreditcardEmail.Trim() != string.Empty && context.CreditcardNumber != null && context.CreditcardNumber.Trim() != string.Empty)
            {
                var data = from u in DataContext.Reader.CcMailUsages
                           where u.EmailAddress == context.CreditcardEmail || u.CreditCardNumber256.Equals(new System.Data.Linq.Binary(context.CreditcardNumberEncrypted))
                           select u;

                if (data != null && data.Count() > 0)
                {
                    int sameEmailCount = data.Where(u => u.EmailAddress == context.CreditcardEmail).Count();
                    if (sameEmailCount > 1)
                    {
                        DetectionResult result = new DetectionResult() { IsDetected = true, MerchantId = context.MerchantId, Rule = this };
                        result.Message = string.Format("Email {0} was used with {1} different cards", context.CreditcardEmail, sameEmailCount);
                        result.Action = new Action(Action.ActionType.BlockEmail, this, result.Message) { Email = context.CreditcardEmail };
                        results.Add(result);
                    }

                    int sameCCCount = data.Where(u => u.CreditCardNumber256.Equals(new System.Data.Linq.Binary(context.CreditcardNumberEncrypted))).Count();
                    if (sameCCCount > 1)
                    {
                        DetectionResult result = new DetectionResult() { IsDetected = true, MerchantId = context.MerchantId, Rule = this };
                        result.Message = string.Format("Creditcard {0} was used with {1} different emails", context.CreditcardDisplay, sameCCCount);
                        result.Action = new Action(Action.ActionType.BlockCard, this, result.Message) { CardNumber = context.CreditcardNumber };
                        results.Add(result);
                    }
                } 
            }

            if (results.Count() == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }

        public override void SetDefaults()
        {
            return;
        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }
    }
}
