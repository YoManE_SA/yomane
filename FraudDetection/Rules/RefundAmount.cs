﻿using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 925.1 - refund amounts
    /// mode: offline
    /// scope: merchant
    /// settings: merchant
    /// periods: month
    /// thresholds: 2
    /// actions: alert, merchant block
    /// defaults: minimums
    /// </summary>
    public class RefundAmount : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }
        
        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Merchant; }
        }

        public override string Code
        {
            get
            {
                return "DR925.1";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks if the rolling month, approved refunds amount sum, is over a threshold.";
            }
        }

        public override void SetDefaults()
        {
            Thresholds = new List<Threshold>();
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Month, Type = Threshold.ThresholdType.AmountSum, Value = 50, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Month, Type = Threshold.ThresholdType.AmountSum, Value = 100, Action = Action.ActionType.BlockMerchantRecommendation });
        }

        public override ValidationResult Validate()
        {
            if (Thresholds.Count < 2)
                return new ValidationResult() { IsValid = false, Reason = "2 thresholds required" };
            foreach (Threshold<decimal> threshold in Thresholds)
                if (threshold.Value <= 0)
                    return new ValidationResult() { IsValid = false, Reason = "All values must be positive integers" };

            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>() { new DetectionResult() { Rule = this, IsDetected = false, MerchantId = RuleMerchantId } };

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
            var timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Month); 
            var data = (from t in dc.DenormalizedTransactions
                            where t.CompanyID == RuleMerchantId && 
                                t.TransactionStatus == transactionStatusApproved &&
                                t.IsRefund.Value &&
                                t.PaymentMethodID > (byte)CommonTypes.PaymentMethodEnum.MaxInternal && 
                                t.TransactionDate >= timeFrame.From && 
                                t.TransactionDate <= timeFrame.To
                            group t by t.CompanyID into g
                            select new
                            {
                                Month = g.Sum(t => t.TransactionAmountUSD)
                            }).SingleOrDefault();
            if (data == null)
                return results;

            Threshold<decimal> detectedThreshold = null;
            bool isDetected = FindThreshold(data, Thresholds, out detectedThreshold);

            if (isDetected)
            {
                results.Single().IsDetected = true;
                results.Single().Message = string.Format("The amount of approved refunds from {0} to {1}, ${2} is over ${3}. {4}", timeFrame.From.ToString(dateFormat), timeFrame.To.ToString(dateFormat), detectedThreshold.DetectedValue.ToString(decimalFormat), detectedThreshold.Value.ToString(decimalFormat), detectedThreshold);
                results.Single().Action = new Action(detectedThreshold.Action, this, results.Single().Message) { MerchantId = RuleMerchantId };
            }

            return results;
        }
    }
}
