﻿using Netpay.Bll.PaymentMethods;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 933 - authorizations bin sequential in the past day
    /// mode: offline
    /// scope: global
    /// desc: AAAA AABB BBBB BBBC
    ///  A = bin
    ///  B = issuer defined
    ///  C = check digit
    /// search for same bin(A) numbers and 10 tolerance sequential(B)
    /// thresholds: 2 
    /// defaults: threshold1 = 2, threshold2 = 5 positive counts
    ///  actions: 
    /// threshold1 alert
    ///  threshold2, if different merchants: block bin, if same merchant: block merchant
    /// </summary>
    public class CardSequentials : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Global; }
        }

        public override string Code
        {
            get
            {
                return "DR933";
            }
        }

        public override string Description
        {
            get
            {
                return "Searches for card sequentials. If 5 or more sequentials are found, if all have the same merchant then the merchant is blocked, otherwise the card BIN is blocked.";
            }
        }

        public override void SetDefaults()
        {

        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }

        private class Card
        {
            public int TransId;
            public string TransType;
            public int MerchantId;
            public string CardNum;
            public string CardBin;
            public long CardBinAndAccountNum;
        }

        private class CardComparer : IEqualityComparer<Card>
        {
            public bool Equals(Card x, Card y)
            {
                return x.CardNum == y.CardNum;
            }

            public int GetHashCode(Card obj)
            {
                return obj.CardNum.GetHashCode();
            }
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();
            
            var timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Day, false); 
            var passCards = from trans in DataContext.Reader.tblCompanyTransPasses                    
                            join card in DataContext.Reader.TransPaymentMethods on trans.TransPaymentMethod_id equals card.TransPaymentMethod_id
                            where card != null &&
                                card.CreditCard_id != null &&
                                trans.PaymentMethod > (byte)CommonTypes.PaymentMethodEnum.MaxInternal &&
                                trans.OriginalTransID == 0 &&
                                card.Value1Encrypted != null &&
                                trans.InsertDate >= timeFrame.From &&
                                trans.InsertDate <= timeFrame.To
                            select new Card
                            {
                                TransId = trans.ID,
                                TransType = "approved",
                                MerchantId = trans.companyID,
                                CardNum= DataContext.Reader.GetDecrypted256(card.Value1Encrypted),
                            };

            var failCards = from trans in DataContext.Reader.tblCompanyTransFails
                            join card in DataContext.Reader.TransPaymentMethods on trans.TransPaymentMethod_id equals card.TransPaymentMethod_id
                            where card != null &&
                                card.CreditCard_id != null &&
                                trans.PaymentMethod > (byte)CommonTypes.PaymentMethodEnum.MaxInternal &&
                                card.Value1Encrypted != null &&
                                trans.InsertDate >= timeFrame.From &&
                                trans.InsertDate <= timeFrame.To
                            select new Card
                            {
                                TransId = trans.ID,
                                TransType = "declined",
                                MerchantId = trans.CompanyID.Value,
                                CardNum = DataContext.Reader.GetDecrypted256(card.Value1Encrypted)
                            };

            /*
            List<Card> cards = new List<Card>();
            cards.Add(new Card() { CardNum = "4580000000000000" });
            cards.Add(new Card() { CardNum = "4580000000000010" });
            cards.Add(new Card() { CardNum = "4580000000000030" });
            cards.Add(new Card() { CardNum = "4580000000000040" });
            cards.Add(new Card() { CardNum = "4580000000000100" });
            cards.Add(new Card() { CardNum = "4580000000000101" });
            */

            // parse cards
            var cards = passCards.Union(failCards).ToList().Distinct(new CardComparer());
            var cardsParsed = new List<Card>();
            foreach (var card in cards)
            {
                cardsParsed.Add(new Card
                {
                    TransId = card.TransId,
                    TransType = card.TransType,
                    MerchantId = card.MerchantId,
                    CardNum = card.CardNum,
                    CardBin = card.CardNum.Substring(0, 6),
                    //CardAccountNum = card.CardNum.Substring(6, card.CardNum.Length - 7).ToInt32()
                    CardBinAndAccountNum = card.CardNum.Substring(0, card.CardNum.Length - 1).ToInt64(0)
                });
            }
         
            // group by tolrance
            int tolerance = 10;
            Card prevCard = null;
            var currentGroup = new List<Card>();
            var groups = new List<List<Card>>();
            cardsParsed = cardsParsed.OrderBy(c => c.CardBinAndAccountNum).ToList();
            foreach (var card in cardsParsed)
            {
                if (prevCard == null)
                {
                    prevCard = card;
                    continue;
                }

                if (card.CardBinAndAccountNum != prevCard.CardBinAndAccountNum && card.CardBinAndAccountNum - prevCard.CardBinAndAccountNum <= tolerance)
                {
                    currentGroup.Add(card);
                }
                else
                {
                    if (currentGroup.Count() > 1)
                        groups.Add(currentGroup);
                    currentGroup = new List<Card>();
                }

                prevCard = card;
            }

            // create results
            foreach (var group in groups)
            {
                StringBuilder cardsDisplay = new StringBuilder();
                foreach (var card in group)
                {
                    CreditCard cc = CreditCard.FromCardNumber(card.CardNum);
                    cardsDisplay.Append(cc.Display);
                    cardsDisplay.Append(", ");
                }

                DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                result.Message = "Sequntial card numbers found: " + cardsDisplay.ToString();

                /// if cards >= 5, different merchants: block bin, same merchant: block merchant
                Action action = new Action(Action.ActionType.Alert, this, result.Message);
                if (group.Count >= 5)
                {
                    int merchantsCount = group.Select(c => c.MerchantId).Distinct().Count();
                    if (merchantsCount == 1)
                    {
                        action.Type = Action.ActionType.BlockMerchant;
                        action.MerchantId = group.First().MerchantId;
                    }
                    else
                    {
                        action.Type = Action.ActionType.BlockBin;
                        action.BinNumber = group.First().CardBin;
                    }   
                }
                result.Action = action;
                results.Add(result);
            }

            if (results.Count() == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }
    }
}
