﻿using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 991.1 - chb stats by visa
    /// mode: offline
    /// </summary>
    public class VisaChargebackStats : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Global; }
        }

        public override string Code
        {
            get
            {
                return "DR991.1";
            }
        }

        public override string Description
        {
            get
            {
                return "This calendar month approved authorizations / chb count > 1% or 2% or this calendar month chb count > 100 or 500. Either will alert, both will block the merchant. compare ratio & count values depends on low / high risk mcc";
            }
        }

        public override void SetDefaults()
        {

        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }
        
        private const decimal _maxVisaChbRatioStandart = 1;
        private const int _maxVisaChbCountStandart = 100;
        private const decimal _maxVisaChbRatioHighRisk = 2;
        private const int _maxVisaChbCountHighRisk = 500;

        private List<DetectionResult> DetectByMerchant(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

            DateTime now = DateTime.Now;
            Range<DateTime> timeframe = new Range<DateTime>(new DateTime(now.Year, now.Month, 1).MinTime(), now.MaxTime());
            //timeframe.From = timeframe.From.AddYears(-5);

            var exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizationsAndChargebacks) { paymentMethod = PaymentMethodEnum.CCVisa, timeframe = timeframe });
            var visaStats = exp.GroupBy(t => new { MerchantId = t.CompanyID, IsHighRisk = _highRiskMcc.Contains(t.SICCodeNumber.HasValue ? t.SICCodeNumber.Value : (short)0) }).Select(g => new
            {
                MerchantId = g.Key.MerchantId,
                IsHighRisk = g.Key.IsHighRisk,
                AuthorizationsCount = g.Sum(t => t.IsChargeback.Value ? 0 : 1),
                ChargebacksCount = g.Sum(t => t.IsChargeback.Value ? 1 : 0)
            }).ToList();

            // remove 0 chb
            visaStats = visaStats.Where(s => s.ChargebacksCount > 0).ToList();

            foreach (var merchant in visaStats)
            {
                var maxVisaChbRatio = _maxVisaChbRatioStandart;
                var maxVisaChbCount = _maxVisaChbCountStandart;
                if (merchant.IsHighRisk)
                {
                    maxVisaChbRatio = _maxVisaChbRatioHighRisk;
                    maxVisaChbCount = _maxVisaChbCountHighRisk;
                }
                
                decimal chargebackRatio = Infrastructure.Math.GetPercentage(merchant.ChargebacksCount, merchant.AuthorizationsCount);
                string statsInfo = $"( %{chargebackRatio.ToString(decimalFormat)}, #{merchant.ChargebacksCount}, {timeframe.From.ToString(dateFormat)} - {timeframe.To.ToString(dateFormat)} )";
                if (chargebackRatio > maxVisaChbRatio && merchant.ChargebacksCount > maxVisaChbCount)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = $"Visa chargebacks ratio and count are over the limits of %{maxVisaChbRatio.ToString(decimalFormat)}, #{maxVisaChbCount} {statsInfo}";
                    result.Action = new Action(Action.ActionType.BlockMerchant, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }
                else if (chargebackRatio > maxVisaChbRatio)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = $"Visa chargebacks ratio is over the limit of %{maxVisaChbRatio.ToString(decimalFormat)} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }
                else if (merchant.ChargebacksCount > maxVisaChbCount)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = $"Visa chargebacks count is over the limit of #{maxVisaChbCount} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }
            }

            return results;
        }

        private List<DetectionResult> DetectByTerminal(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

            DateTime now = DateTime.Now;
            Range<DateTime> timeframe = new Range<DateTime>(new DateTime(now.Year, now.Month, 1).MinTime(), now.MaxTime());
            //timeframe.From = timeframe.From.AddYears(-5);

            var exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizationsAndChargebacks) { paymentMethod = PaymentMethodEnum.CCVisa, timeframe = timeframe });
            var visaStats = exp.GroupBy(t => t.TerminalNumber).Select(g => new
            {
                TerminalNumber = g.Key,
                AuthorizationsCount = g.Sum(t => t.IsChargeback.Value ? 0 : 1),
                ChargebacksCount = g.Sum(t => t.IsChargeback.Value ? 1 : 0)
            }).ToList();

            // remove 0 chb
            visaStats = visaStats.Where(s => s.ChargebacksCount > 0).ToList();

            foreach (var terminal in visaStats)
            {
                // determine high risk
                var maxVisaChbRatio = _maxVisaChbRatioStandart;
                var maxVisaChbCount = _maxVisaChbCountStandart;
                var terminalCache = FraudDetectionManager.GetTerminal(terminal.TerminalNumber);
                if (terminalCache != null && terminalCache.Mcc != null && _highRiskMcc.Contains(terminalCache.Mcc.Value))
                {
                    maxVisaChbRatio = _maxVisaChbRatioHighRisk;
                    maxVisaChbCount = _maxVisaChbCountHighRisk;
                }

                // calc ratio
                decimal chargebackRatio = Infrastructure.Math.GetPercentage(terminal.ChargebacksCount, terminal.AuthorizationsCount);
                string statsInfo = $"( %{chargebackRatio.ToString(decimalFormat)}, #{terminal.ChargebacksCount}, {timeframe.From.ToString(dateFormat)} - {timeframe.To.ToString(dateFormat)} )";
                if (chargebackRatio > maxVisaChbRatio && terminal.ChargebacksCount > maxVisaChbCount)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Visa chargebacks ratio and count for terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' are over the limits of %{maxVisaChbRatio.ToString(decimalFormat)}, #{maxVisaChbCount} {statsInfo}";
                    result.Action = new Action(Action.ActionType.BlockMerchant, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }
                else if (chargebackRatio > maxVisaChbRatio)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Visa chargebacks ratio for terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' is over the limit of %{maxVisaChbRatio.ToString(decimalFormat)} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }
                else if (terminal.ChargebacksCount > maxVisaChbCount)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Visa chargebacks count for terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' is over the limit of #{maxVisaChbCount} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }
            }

            return results;
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            results.AddRange(DetectByMerchant(context));
            results.AddRange(DetectByTerminal(context));

            if (results.Count() == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }
    }
}
