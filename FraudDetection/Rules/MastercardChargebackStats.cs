﻿using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 991.2 - chb stats by mastercard
    /// mastercard: 1% & 100# in previous calendar month
    /// if % or # > alert
    /// if % and # > block merchant
    /// mode: offline
    /// </summary>
    public class MastercardChargebackStats : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Global; }
        }

        public override string Code
        {
            get
            {
                return "DR991.2";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks that CHB count & ratio is not over mastercard limits (100 & 1%). If count or ratio is over the limit an alert is send. If both are over the limit the merchant is blocked. CHB count is taken from this calendar month and compared with previous calendar month approved authorizations.";
            }
        }

        public override void SetDefaults()
        {

        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }

        private const decimal maxMastercardChbRatio = 1;
        private const int maxMastercardChbCount = 100;

        private List<DetectionResult> DetectByMerchant(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

            DateTime date = DateTime.Now.AddMonths(-1);
            Range<DateTime> timeframe = new Range<DateTime>(new DateTime(date.Year, date.Month, 1).MinTime(), new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month)).MaxTime());
            //timeframe.From = timeframe.From.AddYears(-5);

            var expAuth = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { paymentMethod = PaymentMethodEnum.CCMastercard, timeframe = timeframe });
            var authorizations = expAuth.GroupBy(t => t.CompanyID).Select(g => new
            {
                MerchantId = g.Key.Value,
                AuthorizationsCount = g.Count(),
                ChargebacksCount = 0,
            }).ToList();

            date = DateTime.Now;
            timeframe = new Range<DateTime>(new DateTime(date.Year, date.Month, 1), date);
            //timeframe.From = timeframe.From.AddYears(-5);

            var expChb = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.Chargebacks) { paymentMethod = PaymentMethodEnum.CCMastercard, timeframe = timeframe });
            var chargebacks = expChb.GroupBy(t => t.CompanyID).Select(g => new
            {
                MerchantId = g.Key.Value,
                AuthorizationsCount = 0,
                ChargebacksCount = g.Sum(t => t.IsChargeback.Value ? 1 : 0)
            }).ToList();

            // merge
            var mastercardStats = new List<dynamic>();
            foreach (var authorization in authorizations)
            {
                var chargeback = chargebacks.Where(c => c.MerchantId == authorization.MerchantId).SingleOrDefault();
                int chargebackCount = chargeback == null ? 0 : chargeback.ChargebacksCount;
                var stat = new
                {
                    MerchantId = authorization.MerchantId,
                    AuthorizationsCount = authorization.AuthorizationsCount,
                    ChargebacksCount = chargebackCount
                };

                mastercardStats.Add(stat);
            }

            // remove 0 chb
            mastercardStats = mastercardStats.Where(s => s.ChargebacksCount > 0).ToList();

            // detect
            foreach (var merchant in mastercardStats)
            {
                decimal chargebackRatio = Infrastructure.Math.GetPercentage(merchant.ChargebacksCount, merchant.AuthorizationsCount);
                string statsInfo = $"( %{chargebackRatio.ToString(decimalFormat)}, #{merchant.ChargebacksCount}, {timeframe.From.ToString(dateFormat)} - {timeframe.To.ToString(dateFormat)} )";
                if (chargebackRatio > maxMastercardChbRatio && merchant.ChargebacksCount > maxMastercardChbCount)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = $"Mastercard chargebacks ratio and count are over the limits of %{maxMastercardChbRatio.ToString(decimalFormat)}, #{maxMastercardChbCount} {statsInfo}";
                    result.Action = new Action(Action.ActionType.BlockMerchant, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }
                else if (chargebackRatio > maxMastercardChbRatio)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = $"Mastercard chargebacks ratio is over the limit of %{maxMastercardChbRatio} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }
                else if (merchant.ChargebacksCount > maxMastercardChbCount)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = $"Mastercard chargebacks count is over the limit of #{maxMastercardChbCount} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }
            }

            return results;
        }

        private List<DetectionResult> DetectByTerminal(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

            DateTime date = DateTime.Now.AddMonths(-1);
            Range<DateTime> timeframe = new Range<DateTime>(new DateTime(date.Year, date.Month, 1).MinTime(), new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month)).MaxTime());
            //timeframe.From = timeframe.From.AddYears(-5);

            var expAuth = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { paymentMethod = PaymentMethodEnum.CCMastercard, timeframe = timeframe });
            var authorizations = expAuth.GroupBy(t => t.TerminalNumber).Select(g => new
            {
                TerminalNumber = g.Key,
                AuthorizationsCount = g.Count(),
                ChargebacksCount = 0
            }).ToList();

            date = DateTime.Now;
            timeframe = new Range<DateTime>(new DateTime(date.Year, date.Month, 1), date);
            //timeframe.From = timeframe.From.AddYears(-5);

            var expChb = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.Chargebacks) { paymentMethod = PaymentMethodEnum.CCMastercard, timeframe = timeframe });
            var chargebacks = expChb.GroupBy(t => t.TerminalNumber).Select(g => new
            {
                TerminalNumber = g.Key,
                AuthorizationsCount = 0,
                ChargebacksCount = g.Sum(t => t.IsChargeback.Value ? 1 : 0)
            }).ToList();

            // merge
            var mastercardStats = new List<dynamic>();
            foreach (var authorization in authorizations)
            {
                var chargeback = chargebacks.Where(c => c.TerminalNumber == authorization.TerminalNumber).SingleOrDefault();
                int chargebackCount = chargeback == null ? 0 : chargeback.ChargebacksCount;
                var stat = new
                {
                    TerminalNumber = authorization.TerminalNumber,
                    AuthorizationsCount = authorization.AuthorizationsCount,
                    ChargebacksCount = chargebackCount
                };

                mastercardStats.Add(stat);
            }

            // remove 0 chb
            mastercardStats = mastercardStats.Where(s => s.ChargebacksCount > 0).ToList();

            // detect
            foreach (var terminal in mastercardStats)
            {
                decimal chargebackRatio = Infrastructure.Math.GetPercentage(terminal.ChargebacksCount, terminal.AuthorizationsCount);
                string statsInfo = $"( %{chargebackRatio.ToString(decimalFormat)}, #{terminal.ChargebacksCount}, {timeframe.From.ToString(dateFormat)} - {timeframe.To.ToString(dateFormat)} )";
                if (chargebackRatio > maxMastercardChbRatio && terminal.ChargebacksCount > maxMastercardChbCount)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Mastercard chargebacks ratio and count for terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' are over the limits of %{maxMastercardChbRatio.ToString(decimalFormat)}, #{maxMastercardChbCount} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }
                else if (chargebackRatio > maxMastercardChbRatio)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Mastercard chargebacks ratio for terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' is over the limit of %{maxMastercardChbRatio} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }
                else if (terminal.ChargebacksCount > maxMastercardChbCount)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Mastercard chargebacks count for terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' is over the limit of #{maxMastercardChbCount} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }
            }

            return results;
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            results.AddRange(DetectByMerchant(context));
            results.AddRange(DetectByTerminal(context));

            if (results.Count() == 0)
            {
                DetectionResult result = new DetectionResult() { Rule = this, IsDetected = false };
                results.Add(result);
            }

            return results;
        }
    }
}
