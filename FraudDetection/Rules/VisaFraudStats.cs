﻿using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 931.4 - fraud stats by visa
    /// mode: offline
    /// </summary>
    public class VisaFraudStats : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Global; }
        }

        public override string Code
        {
            get
            {
                return "DR931.4";
            }
        }

        public override string Description
        {
            get
            {
                return "This calendar month authorizations / fraud amount > 1%, 2% or this calendar month fraud amount > 75000, 250000. Either will alert.";
            }
        }

        public override void SetDefaults()
        {

        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }
        
        private const decimal _maxVisaFraudRatioStandart = 1;
        private const int _maxVisaFraudAmontStandart = 75000;
        private const decimal _maxVisaFraudRatioHighRisk = 2;
        private const int _maxVisaFraudAmountHighRisk = 250000;

        private class StatsHolder
        {
            public int MerchantId;
            public string TerminalNumber;
            public decimal AuthorizationsAmount;
            public decimal FraudsAmount;
            public bool IsHighRisk;
        }

        private List<DetectionResult> DetectByMerchant(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();
            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

            DateTime now = DateTime.Now;
            Range<DateTime> timeframe = new Range<DateTime>(new DateTime(now.Year, now.Month, 1).MinTime(), now.MaxTime());
            //timeframe.From = DateTime.Now.AddYears(-5);

            // approved authorization stats
            var exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { paymentMethod = PaymentMethodEnum.CCVisa, timeframe = timeframe });
            var authStats = exp.GroupBy(t => new { MerchantId = t.CompanyID.Value, TerminalNumber = t.TerminalNumber }).Select(g => new StatsHolder
            {
                MerchantId = g.Key.MerchantId,
                TerminalNumber = g.Key.TerminalNumber,
                AuthorizationsAmount = g.Sum(t => t.TransactionAmountUSD.HasValue ? t.TransactionAmountUSD.Value : 0),
                FraudsAmount = 0
            }).ToList();
            
            // fraud stats
            // 7 / 8 = set / unset fraud
            string sql = $@"WITH histories AS
                            (
                                 SELECT ROW_NUMBER() OVER
                                     (
                                         PARTITION BY TransactionID
                                         ORDER BY HistoryInsertDate DESC
                                     ) AS ordinal, *
                                 FROM [DenormalizedTransactionHistory]
	                             WHERE (HistoryTypeID = 7 or HistoryTypeID = 8) AND 
                                    TransactionStatus = '{transactionStatusApproved}' AND
                                    CAST(HistoryInsertDate AS DATE) >= '{ timeframe.From.ToString("yyyy-MM-dd") }' AND 
                                    CAST(HistoryInsertDate AS DATE) <= '{ timeframe.To.ToString("yyyy-MM-dd") }' AND
                                    TransactionPaymentMethodID = {(int)PaymentMethodEnum.CCVisa} AND
                                    AcquiringBankID > 1
                            )
                            SELECT MerchantID, TerminalNumber, SUM(TransactionAmountUSD) AS FraudsAmount, CAST(0 AS money) AS AuthorizationsAmount, CAST(0 AS bit) AS IsHighRisk
                            FROM histories
                            WHERE ordinal = 1 AND HistoryTypeID = 7
							GROUP BY MerchantID, TerminalNumber";
            var fraudStats = dc.ExecuteQuery<StatsHolder>(sql, new object[] { }).ToList();

            // merge into authStats & mark risk
            foreach (var authStat in authStats)
            {
                var fraudStat = fraudStats.Where(fs => fs.MerchantId == authStat.MerchantId && fs.TerminalNumber == authStat.TerminalNumber).SingleOrDefault();
                if (fraudStat != null)
                    authStat.FraudsAmount = fraudStat.FraudsAmount;

                var terminalCache = FraudDetectionManager.GetTerminal(authStat.TerminalNumber);
                authStat.IsHighRisk = terminalCache != null && terminalCache.Mcc != null && _highRiskMcc.Contains(terminalCache.Mcc.Value);
            }

            // remove 0 frauds
            authStats = authStats.Where(a => a.FraudsAmount > 0).ToList();

            foreach (var merchant in authStats)
            {
                // determine high risk
                var maxVisaFraudRatio = _maxVisaFraudRatioStandart;
                var maxVisaFraudAmount = _maxVisaFraudAmontStandart;
                if (merchant.IsHighRisk)
                {
                    maxVisaFraudRatio = _maxVisaFraudRatioHighRisk;
                    maxVisaFraudAmount = _maxVisaFraudAmountHighRisk;
                }

                var fraudRatio = Infrastructure.Math.GetPercentage(merchant.FraudsAmount, merchant.AuthorizationsAmount);
                string statsInfo = $"( %{fraudRatio.ToString(decimalFormat)}, ${merchant.FraudsAmount.ToString(decimalFormat)}, {timeframe.From.ToString(dateFormat)} - {timeframe.To.ToString(dateFormat)} )";
                if (fraudRatio > maxVisaFraudRatio)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = $"Visa fraud ratio is over the limit of %{maxVisaFraudRatio.ToString(decimalFormat)} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }

                if (merchant.FraudsAmount > maxVisaFraudAmount)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true, MerchantId = merchant.MerchantId };
                    result.Message = $"Visa fraud amount is over the limit of ${maxVisaFraudAmount} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = merchant.MerchantId };
                    results.Add(result);
                }
            }

            return results;
        }

        private List<DetectionResult> DetectByTerminal(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();
            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

            DateTime now = DateTime.Now;
            Range<DateTime> timeframe = new Range<DateTime>(new DateTime(now.Year, now.Month, 1).MinTime(), now.MaxTime());
            //timeframe.From = DateTime.Now.AddYears(-5);

            // authorization stats
            var exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { paymentMethod = PaymentMethodEnum.CCVisa, timeframe = timeframe });
            var authStats = exp.GroupBy(t => t.TerminalNumber).Select(g => new StatsHolder
            {
                TerminalNumber = g.Key,
                AuthorizationsAmount = g.Sum(t => t.TransactionAmountUSD.HasValue ? t.TransactionAmountUSD.Value : 0),
                FraudsAmount = 0
            }).ToList();

            // fraud stats
            // 7 / 8 = set / unset fraud
            string sql = $@"WITH histories AS
                            (
                                 SELECT ROW_NUMBER() OVER
                                     (
                                         PARTITION BY TransactionID
                                         ORDER BY HistoryInsertDate DESC
                                     ) AS ordinal, *
                                 FROM DenormalizedTransactionHistory
	                             WHERE (HistoryTypeID = 7 or [HistoryTypeID] = 8) AND 
                                    CAST(HistoryInsertDate AS DATE) >= '{ timeframe.From.ToString("yyyy-MM-dd") }' AND 
                                    CAST(HistoryInsertDate AS DATE) <= '{ timeframe.To.ToString("yyyy-MM-dd") }' AND
                                    TransactionPaymentMethodID = {(int)PaymentMethodEnum.CCVisa} AND
                                    AcquiringBankID > 1
                            )
                            SELECT 0 AS MerchantId, TerminalNumber, SUM(TransactionAmountUSD) AS FraudsAmount, CAST(0 AS money) AS AuthorizationsAmount, CAST(0 AS bit) AS IsHighRisk
                            FROM histories
                            WHERE ordinal = 1 AND HistoryTypeID = 7
							GROUP BY TerminalNumber";
            var fraudStats = dc.ExecuteQuery<StatsHolder>(sql, new object[] { }).ToList();

            // merge
            foreach (var authStat in authStats)
            {
                var fraudStat = fraudStats.Where(fs => fs.TerminalNumber == authStat.TerminalNumber).SingleOrDefault();
                if (fraudStat != null)
                    authStat.FraudsAmount = fraudStat.FraudsAmount;
            }

            // remove 0 frauds
            authStats = authStats.Where(a => a.FraudsAmount > 0).ToList();

            foreach (var terminal in authStats)
            {
                // determine high risk
                var maxVisaFraudRatio = _maxVisaFraudRatioStandart;
                var maxVisaFraudAmount = _maxVisaFraudAmontStandart;
                var terminalCache = FraudDetectionManager.GetTerminal(terminal.TerminalNumber);
                if (terminalCache != null && terminalCache.Mcc != null && _highRiskMcc.Contains(terminalCache.Mcc.Value))
                {
                    maxVisaFraudRatio = _maxVisaFraudRatioHighRisk;
                    maxVisaFraudAmount = _maxVisaFraudAmountHighRisk;
                }

                // calc ratio
                var fraudRatio = Infrastructure.Math.GetPercentage(terminal.FraudsAmount, terminal.AuthorizationsAmount);
                string statsInfo = $"( %{fraudRatio.ToString(decimalFormat)}, ${terminal.FraudsAmount.ToString(decimalFormat)}, {timeframe.From.ToString(dateFormat)} - {timeframe.To.ToString(dateFormat)} )";
                if (fraudRatio > maxVisaFraudRatio)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Visa fraud ratio for terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' is over the limit of %{maxVisaFraudRatio.ToString(decimalFormat)} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }

                if (terminal.FraudsAmount > maxVisaFraudAmount)
                {
                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                    result.Message = $"Visa fraud amount for terminal '{terminal.TerminalNumber} - {FraudDetectionManager.GetTerminal(terminal.TerminalNumber).Name}' is over the limit of ${maxVisaFraudAmount} {statsInfo}";
                    result.Action = new Action(Action.ActionType.Alert, this, result.Message) { TerminalNumber = terminal.TerminalNumber };
                    results.Add(result);
                }
            }

            return results;
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();
            results.AddRange(DetectByMerchant(context));
            results.AddRange(DetectByTerminal(context));

            if (results.Count() == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }
    }
}
