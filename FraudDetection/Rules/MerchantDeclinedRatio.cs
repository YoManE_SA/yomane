﻿using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 931.1 - declined ratio per merchant per day
    /// mode: offline
    /// thresholds: 2
    /// defaults: minimums
    /// actions: alert, block merchant
    /// </summary>
    public class MerchantDeclinedRatio : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Merchant; }
        }

        public override string Code
        {
            get
            {
                return "DR931.1";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks that merchant declined ratio is not over the threshold. Declined ratio = authorizations / declined authorizations";
            }
        }

        public override void SetDefaults()
        {
            Thresholds = new List<Threshold>();
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.Ratio, Value = 5, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<decimal>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.Ratio, Value = 10, Action = Action.ActionType.BlockMerchantRecommendation });
        }

        public override ValidationResult Validate()
        {
            if (Thresholds.Count < 2)
                return new ValidationResult() { IsValid = false, Reason = "2 thresholds required" };
            foreach (Threshold<decimal> threshold in Thresholds)
                if (threshold.Value <= 0)
                    return new ValidationResult() { IsValid = false, Reason = "All values must be positive integers" };

            return new ValidationResult() { IsValid = true };
        }

        /// <summary>
        /// Runs only on sunday
        /// </summary>
        public override DayOfWeek? RunAt
        {
            get
            {
                return DayOfWeek.Sunday;
            }
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
            var timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Day);
            var exp = from t in GetTransactions(new TransactionsFilters(TransactionFiltersGroup.Authorizations) { MerchntID = RuleMerchantId, timeframe = timeFrame, isSystemAcquiringBank = null })
                      group t by t.CompanyID into g
                      select new
                      {
                          ApprovedCount = g.Sum(t => t.TransactionStatus == transactionStatusApproved ? 1 : 0),
                          DeclinedCount = g.Sum(t => t.TransactionStatus == transactionStatusDeclined ? 1 : 0)
                      };
            var data = exp.SingleOrDefault();
            if (data != null)
            {
                    decimal ratio = Infrastructure.Math.GetPercentage(data.DeclinedCount, data.DeclinedCount + data.ApprovedCount);

                    Threshold<decimal> detectedThreshold = null;
                    bool isDetected = FindThreshold(ratio, Thresholds, out detectedThreshold);

                    if (isDetected)
                    {
                        DetectionResult result = new DetectionResult() { IsDetected = true, MerchantId = RuleMerchantId, Rule = this };
                        result.Message = string.Format("Merchant declined ratio %{0} from {1} to {2} is over %{3}", detectedThreshold.DetectedValue.ToString(decimalFormat), timeFrame.From.ToString(dateFormat), timeFrame.To.ToString(dateFormat), detectedThreshold.Value.ToString(decimalFormat));
                        result.Action = new Action(detectedThreshold.Action, this, result.Message) { MerchantId = RuleMerchantId };
                        results.Add(result);
                    }
            }

            if (results.Count == 0)
            {
                DetectionResult result = new DetectionResult() { Rule = this, IsDetected = false };
                results.Add(result);
            }

            return results;
        }
    }
}
