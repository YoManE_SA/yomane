﻿using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 922.2 - average authorization count over quarter and month
	/// mode: offline
    /// settings: merchant
	/// actions: alert
    /// </summary>
    public class AverageAuthorizationCountQuarter : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Merchant; }
        }

        public override string Code
        {
            get
            {
                return "DR922.2";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks daily average authorization count over month & quarter";
            }
        }

        public override void SetDefaults()
        {
            Thresholds = new List<Threshold>();
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Month, Type = Threshold.ThresholdType.DailyCountAverage, Value = 1, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Quarter, Type = Threshold.ThresholdType.DailyCountAverage, Value = 2, Action = Action.ActionType.Alert });
        }

        public override ValidationResult Validate()
        {
            if (Thresholds.Count < 2)
                return new ValidationResult() { IsValid = false, Reason = "2 thresholds required" };
            foreach (Threshold<decimal> threshold in Thresholds)
                if (threshold.Value <= 0)
                    return new ValidationResult() { IsValid = false, Reason = "All values must be positive integers" };

            return new ValidationResult() { IsValid = true };
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

            // month daily average
            Range<DateTime> timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Month);
            var exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { timeframe = timeFrame, MerchntID = RuleMerchantId });
            var totalCount = exp.Count();
            var totalDays = (timeFrame.To - timeFrame.From).Days;
            var dailyAverage = totalCount / totalDays;
            var threshhold = Thresholds.Where(t => t.Period == Threshold.ThresholdPeriod.Month).Single() as Threshold<int>;

            if (dailyAverage > threshhold.Value)
            {
                DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                result.Message = $"The daily average count of approved & declined authorizations from {timeFrame.From.ToString(dateFormat)} to {timeFrame.To.ToString(dateFormat)}, {dailyAverage} is over {threshhold.Value}. {threshhold}";
                result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = RuleMerchantId };
                results.Add(result);
            }

            // quarter daily average
            timeFrame = GetTimeFrame(Threshold.ThresholdPeriod.Quarter);
            exp = GetTransactions(new TransactionsFilters(TransactionFiltersGroup.ApprovedAuthorizations) { timeframe = timeFrame, MerchntID = RuleMerchantId });
            totalCount = exp.Count();
            totalDays = (timeFrame.To - timeFrame.From).Days;
            dailyAverage = totalCount / totalDays;
            threshhold = Thresholds.Where(t => t.Period == Threshold.ThresholdPeriod.Quarter).Single() as Threshold<int>;

            if (dailyAverage > threshhold.Value)
            {
                DetectionResult result = new DetectionResult() { Rule = this, IsDetected = true };
                result.Message = $"The daily average count of approved & declined authorizations from {timeFrame.From.ToString(dateFormat)} to {timeFrame.To.ToString(dateFormat)}, {dailyAverage} is over {threshhold.Value}. {threshhold}";
                result.Action = new Action(Action.ActionType.Alert, this, result.Message) { MerchantId = RuleMerchantId };
                results.Add(result);
            }

            if (results.Count == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }
    }
}
