﻿using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection.Rules
{
    /// <summary>
    /// 939 - alert / block merchants that are inactive for x days. only processing merchants are checked
    ///  mode: offline
    ///  scope: merchant
    /// settings: global
    ///  thresholds: 2 
    /// defaults: x = 3
    /// actions: alert & block merchant
    /// </summary>
    public class InactiveMerchants : DetectionRuleBase
    {
        public override DetectionRuleMode Mode
        {
            get
            {
                return DetectionRuleMode.Offline;
            }
        }

        public override DetectionRuleSettingsScope SettingsScope
        {
            get { return DetectionRuleSettingsScope.Global; }
        }

        public override string Code
        {
            get
            {
                return "DR939";
            }
        }

        public override string Description
        {
            get
            {
                return "Checks that the days since merchant last transaction are not over the threshold. Only merchants in processing status are checked.";
            }
        }

        public override void SetDefaults()
        {
            Thresholds = new List<Threshold>();
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.CountSum, Value = 10, Action = Action.ActionType.Alert });
            Thresholds.Add(new Threshold<int>() { Period = Threshold.ThresholdPeriod.Day, Type = Threshold.ThresholdType.CountSum, Value = 20, Action = Action.ActionType.BlockMerchantRecommendation });
        }

        public override ValidationResult Validate()
        {
            return new ValidationResult() { IsValid = true };
        }

        /// <summary>
        /// Runs only on sunday
        /// </summary>
        public override DayOfWeek? RunAt
        {
            get
            {
                return DayOfWeek.Sunday;
            }
        }

        public override List<DetectionResult> Detect(DetectionContext context)
        {
            List<DetectionResult> results = new List<DetectionResult>();

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
            var data = from t in dc.DenormalizedTransactions
                       where t.CompanyID != null && t.TransactionDate != null
                       group t by t.CompanyID into g
                       select new
                       {
                           MerchantId = g.Key.Value,
                           Day = (DateTime.Now - g.Max(t => t.TransactionDate.Value)).Days
                       };

            if (data != null)
            {
                foreach (var merchant in data)
                {
                    FraudDetectionManager.MerchantCacheItem merchantCache = FraudDetectionManager.GetMerchantCache(merchant.MerchantId);
                    if (merchantCache != null && merchantCache.Status != MerchantStatus.Processing)
                        continue;

                    DetectionResult result = new DetectionResult() { Rule = this, IsDetected = false, MerchantId = merchant.MerchantId };

                    Threshold<int> detectedThreshold = null;
                    bool isDetected = FindThreshold(merchant, Thresholds, out detectedThreshold);

                    if (isDetected)
                    {
                        result.IsDetected = true;
                        result.Message = string.Format("Merchant last transaction was {0} days ago. Merchant status: {1}", merchant.Day, merchantCache.Status.ToString());
                        result.Action = new Action(detectedThreshold.Action, this, result.Message) { MerchantId = merchant.MerchantId, Message = result.Message };
                    }

                    results.Add(result);
                } 
            }

            if (results.Count() == 0)
                results.Add(new DetectionResult() { Rule = this, IsDetected = false });

            return results;
        }
    }
}
