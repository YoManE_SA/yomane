﻿
using System;
using System.Xml.Serialization;

namespace Netpay.FraudDetection
{
    [Serializable]
    [XmlInclude(typeof(Threshold<int>))]
    [XmlInclude(typeof(Threshold<decimal>))]
    public abstract class Threshold
    {
        public enum ThresholdPeriod
        {
            Unknown = 0,
            Hour = 1,
            Day = 2,
            Week = 3,
            Month = 4,
            Quarter = 5,
            Day1 = 6,
            Day2 = 7,
            Day3 = 8,
        }

        public enum ThresholdType
        {
            Unknown = 0,
            CountSum = 1,
            AmountSum = 2,
            AmountAverage = 3,
            Ratio = 4,
            AmountMax = 5,
            DailyAmountAverage = 6,
            DailyCountAverage = 7
        }

        public ThresholdPeriod Period { get; set; }
        public ThresholdType Type { get; set; }
        public Action.ActionType Action { get; set; }
    }

    public class Threshold<T> : Threshold
    {
        public T Value { get; set; }

        [XmlIgnore]
        public T DetectedValue { get; set; }
        public override string ToString()
        {
            return "Threshold period " + Period + ", type " + Type + ", value " + Value + ", action " + Action;
        }
    }
}
