﻿using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection
{
    public class DetectionResult
    {
        public DetectionRuleBase Rule { get; set; }
        public string Message { get; set; }
        public int? MerchantId { get; set; }
        public bool IsDetected { get; set; }
        public Action Action { get; set; }
        public int? DeclinedTransactionId { get; set; }

        public override string ToString()
        {
            return string.Format("Result:{0}, Rule:{1}, Merchant:{2}, Message:{3}, ", IsDetected ? "fail" : "pass", Rule, MerchantId, Message);
        }
    }
}
