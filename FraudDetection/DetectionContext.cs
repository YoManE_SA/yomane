﻿using Netpay.Bll.Accounts;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Bll.Transactions;
using Netpay.Bll.PaymentMethods;
using Netpay.Dal.Reports;
using Netpay.Bll.Merchants;

namespace Netpay.FraudDetection
{
    public class DetectionContext
    {
        public DetectionContext() { }

        public DetectionContext(DenormalizedTransaction transaction)
        {
            Merchant merchant = Merchant.Load(transaction.CompanyID);
            AccountId = merchant.AccountID;
            MerchantId = merchant.ID;
            Amount = transaction.TransactionAmount.Value;
            //CreditcardNumber = transaction.;
            CreditcardExpiration = transaction.PaymentMethodExpirationDate.HasValue ? transaction.PaymentMethodExpirationDate.Value : DateTime.Now;
            CreditcardName = transaction.PaymentMethodName;
            TransCurrency = Currency.Get(transaction.TransactionCurrencyIsoCode);
            TerminalNumber = transaction.TerminalNumber;

            //CustomerIp = "192.168.01";
            //CreditcardEmail = "test@tester.com";
            //Random rnd = new Random();
            //CreditcardNumber = "458000000000000" + rnd.Next(0, 9);
        }

        /// <summary>
        /// Instantiate from query string values
        /// </summary>
        /// <param name="values"></param>
        public DetectionContext(NameValueCollection inputValues)
        {
            try
            {
                NameValueCollection values = new NameValueCollection(inputValues);
                foreach (var key in values.AllKeys)
                    values[key] = values[key].Sanitize();

                int? accountId = Account.AccountIDByNumber(AccountType.Merchant, values.Get("merchantNumber"));
                if (accountId == null)
                    throw new Exception("Account id not found");

                AccountId = accountId.Value;
                MerchantId = FraudDetectionManager.GetMerchantId(AccountId).Value;

                int currencyId = values.Get("currencyId").ToInt32();
                TransCurrency = Currency.Get(currencyId);
                Amount = values.Get("amount").ToDecimal(0);
                AmountUsd = Currency.Convert(Amount, TransCurrency, Currency.Get(CommonTypes.Currency.USD));
                TerminalNumber = values.Get("terminalNumber");

                CreditcardNumber = values.Get("ccNumber");
                CreditcardNumberEncrypted = Netpay.Infrastructure.Security.Encryption.Encrypt(CreditcardNumber);
                CreditcardName = values.Get("ccName");
                string expMonth = values.Get("expMonth");
                string expYear = values.Get("expYear");
                if (expYear != null && expYear.Length == 2)
                    expYear = "20" + expYear;
                if (expYear != null && expYear.Trim() != string.Empty && expMonth != null && expMonth.Trim() != string.Empty)
                    CreditcardExpiration = new DateTime(expYear.ToInt32(), expMonth.ToInt32(), 1);
                CreditcardPhone = values.Get("ccPhoneNumber");
                CreditcardEmail = values.Get("ccEmail");
                CreditcardPersonalNumber = values.Get("ccPersonalNumber");
                CreditcardBinCountry = values.Get("ccBINCountry");
                CreditcardDisplay = values.Get("ccTypeEngString");
                if (values.AllKeys.Contains("ccDateOfBirth"))
                    CreditcardDateOfBirth = values.Get("ccDateOfBirth").ToNullableDate();
                if (values.AllKeys.Contains("ccTypeId"))
                    CreditcardTypeId = values.Get("ccTypeId").ToNullableShort();

                CountryIso = values.Get("countryISO");
                StateIso = values.Get("stateISO");
                City = values.Get("city");
                Address1 = values.Get("address1");
                Address2 = values.Get("address2");
                ZipCode = values.Get("zipCode");
                if (values.AllKeys.Contains("transactionTypeId"))
                    TransactionSourceId = values.Get("transactionTypeId").ToByte(0);
                if (values.AllKeys.Contains("transTypeId"))
                    TransactionTypeId = values.Get("transTypeId").ToInt32(0);
                if (values.AllKeys.Contains("creditTypeId"))
                    CreditTypeId = values.Get("creditTypeId").ToByte(0);
                CustomerIp = values.Get("customerIp");
                if (values.AllKeys.Contains("payments"))
                    Payments = values.Get("payments").ToByte(0);
                OrderNumber = values.Get("orderNumber");
                DebitDeclineReason = values.Get("debitDeclineReason");
                PayFor = values.Get("payFor");
                if (values.AllKeys.Contains("productId"))
                    ProductId = values.Get("productId").ToNullableInt32();
                Comment = values.Get("comment");
                if (values.AllKeys.Contains("paymentMethod"))
                    PaymentMethod = values.Get("paymentMethod").ToNullableShort();
                if (values.AllKeys.Contains("iMethodType"))
                    IMethodType = values.Get("iMethodType").ToByte(0);
                if (values.AllKeys.Contains("isTest"))
                    IsTest = values.Get("isTest").ToBoolean(false);
                ReferringUrl = values.Get("referringUrl");
                if (values.AllKeys.Contains("debitCompanyId"))
                    DebitCompanyId = values.Get("debitCompanyId").ToNullableInt32();
                if (values.AllKeys.Contains("transPayerInfoId"))
                    TransPayerInfoId = values.Get("transPayerInfoId").ToNullableInt32();
                if (values.AllKeys.Contains("transPaymentMethodId"))
                    TransPaymentMethodId = values.Get("transPaymentMethodId").ToNullableInt32();
                PayerId = values.Get("payerId").ToInt32(0);
                PayerName = values.Get("payerName");
                if (values.AllKeys.Contains("is3DSecure"))
                    Is3DSecure = values.Get("is3DSecure").ToNullableBool();
                DeviceId = values.Get("deviceId");
                DebitReferenceCode = values.Get("debitReferenceCode");
                DebitReferenceNumer = values.Get("debitReferenceNum");
                if (values.AllKeys.Contains("transactionChargeFee"))
                    TransactionChargeFee = values.Get("transactionChargeFee").ToDecimal(0);
                if (values.AllKeys.Contains("debitFee"))
                    DebitFee = values.Get("debitFee").ToDecimal(0);
                IpCountry = values.Get("ipCountry");
                if (values.AllKeys.Contains("ctfJumpIndex"))
                    CtfJumpIndex = values.Get("ctfJumpIndex").ToByte(0);
                if (values.AllKeys.Contains("isGateway"))
                    IsGateway = values.Get("isGateway").ToNullableBool();
                if (values.AllKeys.Contains("mobileDeviceId"))
                    MobileDeviceId = values.Get("mobileDeviceId").ToNullableInt32();
                if (values.AllKeys.Contains("referenceTransactionId"))
                    ReferenceTransactionId = values.Get("referenceTransactionId").ToNullableInt32();
            }
            catch (Exception ex)
            {
                Logger.Log(LogSeverity.Error, LogTag.Fraud, ex.Message, ex.StackTrace);
            }
        } 

        public int AccountId { get; set; }
        public int MerchantId { get; set; }
        public Currency TransCurrency { get; set; }
        public string Mcc { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountUsd { get; set; }
        public string TerminalNumber { get; set; }

        public string CreditcardNumber { get; set; }
        public byte[] CreditcardNumberEncrypted { get; set; }
        public DateTime CreditcardExpiration { get; set; }
        public string CreditcardExpirationYY
        {
            get
            {
                string year = CreditcardExpiration.Year.ToString();
                if (year.Length == 4)
                    return year.Substring(2, 2);
                else
                    return CreditcardExpiration.Year.ToString("00");
            }
        }
        public string CreditcardExpirationMM
        {
            get
            {
                if (CreditcardExpiration == null)
                    return null;
                return CreditcardExpiration.Month.ToString("00");
            }
        }
        public string CreditcardName { get; set; }
        public string CreditcardPhone { get; set; }
        public string CreditcardEmail { get; set; }
        public string CreditcardPersonalNumber { get; set; }
        public DateTime? CreditcardDateOfBirth { get; set; }
        public short? CreditcardTypeId { get; set; }
        public string CreditcardBinCountry { get; set; }
        public string CreditcardDisplay { get; set; }
        
        public string CountryIso { get; set; }
        public string StateIso { get; set; }
        public string City { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipCode { get; set; }
        public byte TransactionSourceId { get; set; }
        public int TransactionTypeId { get; set; }
        public byte CreditTypeId { get; set; }
        public string CustomerIp { get; set; }
        public byte Payments { get; set; }
        public string OrderNumber { get; set; }
        public string DebitDeclineReason { get; set; }
        public string PayFor { get; set; }
        public int? ProductId { get; set; }
        public string Comment { get; set; }
        public short? PaymentMethod { get; set; }
        public byte IMethodType { get; set; }
        public bool IsTest { get; set; }
        public string ReferringUrl { get; set; }
        public int? DebitCompanyId { get; set; }
        public int? TransPayerInfoId { get; set; }
        public int? TransPaymentMethodId { get; set; }
        public int PayerId { get; set; }
        public string PayerName { get; set; }
        public bool? Is3DSecure { get; set; }
        public string DeviceId { get; set; }
        public string DebitReferenceCode { get; set; }
        public string DebitReferenceNumer { get; set; }
        public decimal TransactionChargeFee { get; set; }
        public decimal DebitFee { get; set; }
        public string IpCountry { get; set; }
        public byte CtfJumpIndex { get; set; }
        public bool? IsGateway { get; set; }
        public int? MobileDeviceId { get; set; }
        public int? ReferenceTransactionId { get; set; }
    }
}
