﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection
{
    public class FraudDetectionLog
    {
        private FraudDetectionLog(Dal.Netpay.RiskRuleHistory entity)
        {
            _entity = entity;
        }
                

        public static Dictionary<bool, System.Drawing.Color> FraudLogColor = new Dictionary<bool, System.Drawing.Color>
        {
            { true, System.Drawing.ColorTranslator.FromHtml("#efbdc0") } ,
            { false, System.Drawing.ColorTranslator.FromHtml("#abeab6") }
        };

        public static Dictionary<bool, string> FraudLogText = new Dictionary<bool, string>
        {
            {true, "DETECTED" },
            {false, "NOT DETECTED" }
        };

        private Dal.Netpay.RiskRuleHistory _entity;

        public int ID { get { return _entity.RiskRuleHistory_id; } }
        public bool IsDetected { get { return _entity.IsRuleFail.HasValue ? _entity.IsRuleFail.Value : false; } }
        public int? AccountId { get { return _entity.Account_id; } }
        public int? MerchantId { get { return FraudDetectionManager.GetMerchantId(_entity.Account_id); } }
        public DateTime Date { get { return _entity.InsertDate; } }
        public string Reason { get { return _entity.Reason; } }
        public string RuleCode { get { return _entity.RuleCode; } }
        public string RuleMode { get { return _entity.RuleMode; } }
        public string RuleName { get { return _entity.RuleName; } }
        public string Action { get { return _entity.Action; } }

        public class SearchFilters
        {
            public bool? IsDetected;
            public int? AccountId;
            public Range<DateTime?> Date;
            public string RuleCode;
            public string RuleName;
            public DetectionRuleBase.DetectionRuleMode? RuleMode;
            public string Action;
        }

        public static List<FraudDetectionLog> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Module.SecuredObject, PermissionValue.Read);

            var exp = (from l in DataContext.Reader.RiskRuleHistories orderby l.InsertDate descending select l) as IQueryable<Dal.Netpay.RiskRuleHistory>;
            if (filters != null)
            {
                if (filters.IsDetected != null)
                    exp = exp.Where(l => l.IsRuleFail == filters.IsDetected.Value);
                if (filters.AccountId != null)
                    exp = exp.Where(l => l.Account_id == filters.AccountId);
                if (filters.Date.From != null)
                    exp = exp.Where(l => l.InsertDate >= filters.Date.From);
                if (filters.Date.To != null)
                    exp = exp.Where(l => l.InsertDate <= filters.Date.To);
                if (filters.RuleCode != null)
                    exp = exp.Where(l => l.RuleCode == filters.RuleCode);
                if (filters.RuleName != null)
                    exp = exp.Where(l => l.RuleName == filters.RuleName);
                if (filters.RuleMode != null)
                    exp = exp.Where(l => l.RuleMode == filters.RuleMode.ToString());
                if (filters.Action != null)
                    exp = exp.Where(l => l.Action == filters.Action); 
            }

            var data = exp.ApplySortAndPage(sortAndPage).Select(l => new FraudDetectionLog(l)).ToList();
            return data;
        }

        public override string ToString()
        {
            return string.Format("Code:{0}, Name:{1}, Account:{2}, Reason:{3}, ", RuleCode, RuleName, AccountId, Reason);
        }
    }
}
