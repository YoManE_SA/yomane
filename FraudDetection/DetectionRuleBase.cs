﻿using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.FraudDetection.Rules;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Netpay.FraudDetection
{
    public abstract class DetectionRuleBase
    {
        public class RuleNameComparer : IEqualityComparer<DetectionRuleBase>
        {
            public bool Equals(DetectionRuleBase x, DetectionRuleBase y)
            {
                return x.Name == y.Name;
            }

            public int GetHashCode(DetectionRuleBase obj)
            {
                return obj.Name.GetHashCode();
            }
        }

        public enum DetectionRuleMode
        {
            Unknown = 0,
            Offline = 1,
            Online = 2
        }

        public enum DetectionRuleSettingsScope
        {
            Unknown = 0,
            Global = 1,
            Merchant = 2
        }

        public const string transactionStatusApproved = "Captured";
        public const string transactionStatusDeclined = "Declined";
        public const string dateFormat = "dd/MM/yyyy HH:mm";
        public const string decimalFormat = "0.00";

        private SetRiskRule _entity;

        [XmlIgnore]
        private SetRiskRule Entity { get { return _entity; } set { _entity = value; } }

        [XmlIgnore]
        public int ID { get { return Entity == null ? 0 : Entity.SetRiskRule_id; } }

        public bool IsEnabled { get; set; }

        [XmlIgnore]
        public int? RuleAccountId { get; set; }

        [XmlIgnore]
        public int? RuleMerchantId { get { return FraudDetectionManager.GetMerchantId(RuleAccountId); } }

        [XmlIgnore]
        public abstract string Code { get; }

        [XmlIgnore]
        public string Name { get { return GetType().Name; } }

        [XmlIgnore]
        public abstract string Description { get; }

        [XmlIgnore]
        public abstract DetectionRuleMode Mode { get; }

        [XmlIgnore]
        public abstract DetectionRuleSettingsScope SettingsScope { get; }

        /// <summary>
        /// Runs every day if null
        /// </summary>
        [XmlIgnore]
        public virtual System.DayOfWeek? RunAt { get { return null; } }

        public List<Threshold> Thresholds { get; set; } 

        public abstract void SetDefaults();

        public abstract ValidationResult Validate();

        public abstract List<DetectionResult> Detect(DetectionContext context);

        protected static List<short> _highRiskMcc = new List<short> { 5122, 5912, 5962, 5966, 5967, 5993, 7995 };

        protected enum TransactionFiltersGroup
        {
            Authorizations = 1,
            Chargebacks = 2,
            AuthorizationsAndChargebacks = 3, 
            ApprovedAuthorizations = 4, 
            ApprovedAuthorizationsAndChargebacks = 5
        }

        protected enum TransactionStatus
        {
            Unknown = 0,
            ApprovedOnly = 1,
            DeclinedOnly = 2,
            ApprovedAndDeclined = 3
        }

        protected class TransactionsFilters
        {
            public TransactionsFilters() { }
            public TransactionsFilters(TransactionFiltersGroup group)
            {
                switch (group)
                {
                    case TransactionFiltersGroup.Authorizations:
                        transactionStatus = TransactionStatus.ApprovedAndDeclined;
                        isChargeback = false;
                        isRefund = false;
                        isGateway = false;
                        isSystemAcquiringBank = false;
                        break;
                    case TransactionFiltersGroup.Chargebacks:
                        transactionStatus = TransactionStatus.ApprovedOnly;
                        isChargeback = true;
                        isRefund = false;
                        isGateway = false;
                        isSystemAcquiringBank = false;
                        break;
                    case TransactionFiltersGroup.AuthorizationsAndChargebacks:
                        transactionStatus = TransactionStatus.ApprovedAndDeclined;
                        isChargeback = null;
                        isRefund = false;
                        isGateway = false;
                        isSystemAcquiringBank = false;
                        break;
                    case TransactionFiltersGroup.ApprovedAuthorizations:
                        transactionStatus = TransactionStatus.ApprovedOnly;
                        isChargeback = false;
                        isRefund = false;
                        isGateway = false;
                        isSystemAcquiringBank = false;
                        break;
                    case TransactionFiltersGroup.ApprovedAuthorizationsAndChargebacks:
                        transactionStatus = TransactionStatus.ApprovedOnly;
                        isChargeback = null;
                        isRefund = false;
                        isGateway = false;
                        isSystemAcquiringBank = false;
                        break;
                    default:
                        break;
                }
            }

            public int? MerchntID = null;
            public TransactionStatus transactionStatus = TransactionStatus.Unknown;
            public bool? isChargeback = null;
            public bool? isRefund = null;
            public bool? isGateway = null;
            public bool? isSystemAcquiringBank = null;
            public Range<DateTime> timeframe = new Range<DateTime>(DateTime.Now.AddYears(-5), DateTime.Now);
            public PaymentMethodEnum? paymentMethod = null;
        }

        protected IQueryable<Dal.Reports.DenormalizedTransaction> GetTransactions(TransactionsFilters filters)
        {
            // relevant to approved only when getting both approved and declined
            // data for these fields in declined trx is unreliable
            var approvedOnlyPredicate = PredicateBuilder.True<Dal.Reports.DenormalizedTransaction>();
            if (filters.isChargeback != null)
                approvedOnlyPredicate = approvedOnlyPredicate.And(t => t.IsChargeback == filters.isChargeback.Value);
            if (filters.isRefund != null)
                approvedOnlyPredicate = approvedOnlyPredicate.And(t => t.IsRefund == filters.isRefund.Value);
            if (filters.isGateway != null)
                approvedOnlyPredicate = approvedOnlyPredicate.And(t => t.IsGateway == filters.isGateway.Value);
            
            // relevant to approved and declined
            var approvedDeclinedPredicate = PredicateBuilder.True<Dal.Reports.DenormalizedTransaction>();
            if (filters.MerchntID != null)
                approvedDeclinedPredicate = approvedDeclinedPredicate.And(t => t.CompanyID == filters.MerchntID.Value);
            if (filters.timeframe.From != null)
                approvedDeclinedPredicate = approvedDeclinedPredicate.And(t => t.TransactionDate >= filters.timeframe.From);
            if (filters.timeframe.To != null)
                approvedDeclinedPredicate = approvedDeclinedPredicate.And(t => t.TransactionDate <= filters.timeframe.To);
            if (filters.paymentMethod != null)
                approvedOnlyPredicate = approvedOnlyPredicate.And(t => t.PaymentMethodID == (byte)filters.paymentMethod.Value);
            else
                approvedOnlyPredicate = approvedOnlyPredicate.And(t => t.PaymentMethodID > (byte)PaymentMethodEnum.MaxInternal);
            if (filters.isSystemAcquiringBank != null)
            {
                if (filters.isSystemAcquiringBank.Value)
                    approvedOnlyPredicate = approvedOnlyPredicate.And(t => t.AcquiringBankID == 1);
                else
                    approvedOnlyPredicate = approvedOnlyPredicate.And(t => t.AcquiringBankID != 1);
            }

            var predicate = PredicateBuilder.True<Dal.Reports.DenormalizedTransaction>();
            switch (filters.transactionStatus)
            {
                case TransactionStatus.Unknown:
                    break;
                case TransactionStatus.ApprovedOnly:
                    predicate = predicate.And(t => t.TransactionStatus == transactionStatusApproved);
                    predicate = predicate.And(approvedOnlyPredicate);
                    predicate = predicate.And(approvedDeclinedPredicate);
                    break;
                case TransactionStatus.DeclinedOnly:
                    predicate = predicate.And(t => t.TransactionStatus == transactionStatusDeclined);
                    predicate = predicate.And(t => !t.IsAuthorization.Value);
                    predicate = predicate.And(approvedOnlyPredicate);
                    predicate = predicate.And(approvedDeclinedPredicate);
                    break;
                case TransactionStatus.ApprovedAndDeclined:
                    var approvedPredicate = PredicateBuilder.True<Dal.Reports.DenormalizedTransaction>();
                    approvedPredicate = approvedPredicate.And(t => t.TransactionStatus == transactionStatusApproved);
                    approvedPredicate = approvedPredicate.And(approvedOnlyPredicate);
                    approvedPredicate = approvedPredicate.And(approvedDeclinedPredicate);

                    var declinedPredicate = PredicateBuilder.True<Dal.Reports.DenormalizedTransaction>();
                    declinedPredicate = declinedPredicate.And(t => t.TransactionStatus == transactionStatusDeclined);
                    declinedPredicate = declinedPredicate.And(t => !t.IsAuthorization.Value);
                    declinedPredicate = declinedPredicate.And(approvedDeclinedPredicate);

                    predicate = approvedPredicate.Or(declinedPredicate);
                    break;
                default:
                    break;
            }

            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
            var exp = from t in dc.DenormalizedTransactions select t;
            exp = exp.Where(predicate);
            return exp;
        }

        protected Range<DateTime> GetTimeFrame(Threshold.ThresholdPeriod period)
        {
            return GetTimeFrame(period, Mode == DetectionRuleMode.Offline);
        }

        /// <summary>
        /// Gets the time frame for a threshold period
        /// Offline rules time frame is shifted 1 day back
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected Range<DateTime> GetTimeFrame(Threshold.ThresholdPeriod period, bool offlineOffset)
        {
            Range<DateTime> range = new Range<DateTime>();
            DateTime now = DateTime.Now;
            range.To = now;
            switch (period)
            {
                case Threshold.ThresholdPeriod.Hour:
                    range.From = now.AddHours(-1);
                    break;
                case Threshold.ThresholdPeriod.Day:
                    range.From = now.AddDays(-1);
                    break;
                case Threshold.ThresholdPeriod.Week:
                    range.From = now.AddDays(-7);
                    break;
                case Threshold.ThresholdPeriod.Month:
                    range.From = now.AddMonths(-1);
                    break;
                case Threshold.ThresholdPeriod.Quarter:
                    range.From = now.AddMonths(-3);
                    break;
                case Threshold.ThresholdPeriod.Day1:
                    range.From = now.AddDays(-1);
                    break;
                case Threshold.ThresholdPeriod.Day2:
                    range.From = now.AddDays(-2);
                    break;
                case Threshold.ThresholdPeriod.Day3:
                    range.From = now.AddDays(-3);
                    break;
                default:
                    throw new ArgumentException("Invalid threshold type");
            }

            if (offlineOffset)
            {
                range.From = range.From.AddDays(-1);
                range.To = range.To.AddDays(-1);
            }

            // debug
            //range.From = range.From.AddYears(-5);

            return range;
        }

        protected bool FindThreshold<T>(object data, List<Threshold> thresholds, out Threshold<T> detectedThreshold) where T : IComparable
        {
            detectedThreshold = null;
            foreach (Threshold<T> threshold in Thresholds)
            {
                T value;
                if (data.GetType().IsValueType)
                    value = (T)data;
                else
                    value = (T)data.GetType().GetProperty(threshold.Period.ToString()).GetValue(data);
                if (value.CompareTo(threshold.Value) > 0)
                    if (detectedThreshold == null || threshold.Action > detectedThreshold.Action)
                    {
                        detectedThreshold = threshold;
                        detectedThreshold.DetectedValue = value;
                    }
            }

            return detectedThreshold != null;
        }

        private static List<DetectionRuleBase> DefinedRules
        {
            get
            {
                List<DetectionRuleBase> rules = new List<DetectionRuleBase>();
                Type[] types = System.Reflection.Assembly.GetExecutingAssembly().GetTypes().Where(t => t.Namespace == "Netpay.FraudDetection.Rules" && t.BaseType == typeof(DetectionRuleBase)).ToArray();
                foreach (Type type in types)
                {
                    DetectionRuleBase rule = Activator.CreateInstance(type) as DetectionRuleBase;
                    if (rule != null)
                        rules.Add(rule);
                }

                return rules;
            }
        }

        private static List<DetectionRuleBase> GetDefaultRules(int? accounId)
        {
            List<DetectionRuleBase> rules = DefinedRules;
             
            if (accounId == null)
            {
                // globals
                rules = rules.Where(r => r.SettingsScope == DetectionRuleSettingsScope.Global).ToList();
            }
            else
            {
                // merchant
                rules = rules.Where(r => r.SettingsScope == DetectionRuleSettingsScope.Merchant).ToList();
                rules.ForEach(r => r.RuleAccountId = accounId);
            }

            rules.ForEach(r => r.SetDefaults());
            return rules;
        }

        public class ValueNameItem
        {
            public string Name { get; set; }
            public string FriendlyName { get; set; }
        }

        public static List<ValueNameItem> RuleNames
        {
            get
            {
                List<ValueNameItem> names = DefinedRules.Select(r => new ValueNameItem() { Name = r.Name, FriendlyName = r.Name.ToSentence() } ).OrderBy(r => r.Name).ToList();
                return names;
            }
        }

        public static List<string> RuleCodes
        {
            get
            {
                List<string> codes = DefinedRules.Select(r => r.Code).OrderBy(r => r).ToList();
                return codes;
            }
        }

        /// <summary>
        /// Loads account or global rules
        /// Missing rules with their default values are added
        /// </summary>
        /// <param name="accountId">Global rules are loaded if null</param>
        /// <returns></returns>
        public static List<DetectionRuleBase> Load(int? accountId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Module.SecuredObject, PermissionValue.Read);

            var rules = Load();
            rules = rules.Where(r => r.RuleAccountId == accountId).ToList();

            // add missing rules
            List<DetectionRuleBase> defaultRules = GetDefaultRules(accountId);
            defaultRules = defaultRules.Except(rules, new RuleNameComparer()).ToList();
            rules.AddRange(defaultRules);

            // remove duplicated
            rules = rules.Distinct(new RuleNameComparer()).ToList();
            
            return rules;
        }

        private const string _cacheKey = "fraudDetectionRuleCache";

        /// <summary>
        /// Loads all rules
        /// Missing rules are not added
        /// </summary>
        /// <returns></returns>
        internal static List<DetectionRuleBase> Load()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Module.SecuredObject, PermissionValue.Read);

            List<DetectionRuleBase> rules = (List<DetectionRuleBase>)Domain.Current.GetCachData(_cacheKey);
            if (rules == null)
            {
                rules = new List<DetectionRuleBase>();
                var entities = from r in DataContext.Reader.SetRiskRules select r;
                foreach (var entity in entities)
                    rules.Add(FromEntity(entity));

                Domain.Current.SetCachData(_cacheKey, rules, DateTime.Now.AddHours(1));
            }

            return rules;
        }

        private static DetectionRuleBase FromEntity(SetRiskRule entity)
        {
            if (entity == null)
                throw new Exception("Cannot create rule from null entity");

            Type ruleType = Type.GetType("Netpay.FraudDetection.Rules." + entity.RuleName);
            XmlSerializer serializer = new XmlSerializer(ruleType);
            DetectionRuleBase rule = (DetectionRuleBase)serializer.Deserialize(entity.RuleSetting.CreateReader());
            rule.RuleAccountId = entity.Account_id;
            rule.Entity = entity;
            return rule;
        }

        public int Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Module.SecuredObject, PermissionValue.Edit);
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Module.SecuredObject, PermissionValue.Add);
            }
            
            if (SettingsScope == DetectionRuleSettingsScope.Merchant && RuleAccountId == null)
                throw new ArgumentException("Must provide account id for merchant rule");

            // log changes
            ChangeLog log;
            if (Entity == null || ID == 0)
                log = new ChangeLog(this);
            else
            {
                var oldRule = (from r in DataContext.Reader.SetRiskRules where r.SetRiskRule_id == Entity.SetRiskRule_id select r).SingleOrDefault();
                log = new ChangeLog(FromEntity(oldRule), this);
            }
            log.Location = string.Format("Fraud detection rule, accountId:{0}", RuleAccountId);
            log.Name = string.Format("Code:{0}, Name:{1}", Code, Name);
            log.Save();

            // remove old
            IQueryable<SetRiskRule> deleted = null;
            if (RuleAccountId == null)
                deleted = from r in DataContext.Writer.SetRiskRules where r.Account_id == null && r.RuleName == Name select r;
            else
                deleted = from r in DataContext.Writer.SetRiskRules where r.Account_id == RuleAccountId && r.RuleName == Name select r;
            DataContext.Writer.SetRiskRules.DeleteAllOnSubmit(deleted);
            DataContext.Writer.SubmitChanges();

            // save new
            XElement xml = XElement.Parse(this.ToXml());
            SetRiskRule entity = new SetRiskRule();
            entity.Account_id = RuleAccountId;
            entity.RuleName = Name;
            entity.RuleSetting = xml;

            DataContext.Writer.SetRiskRules.Update(entity, false);
            DataContext.Writer.SubmitChanges();

            Domain.Current.SetCachData(_cacheKey, null); // clear cache

            return entity.SetRiskRule_id;
        }

        public override string ToString()
        {
            return string.Format("Code:{0}, Name:{1}, Mode:{2}, Settings:{3}", Code, Name, Mode, SettingsScope);
        }
    }
}
