﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Netpay.FraudDetection
{
    public class FraudDetectionReport
    {
        public int? AccountId { get; set; }
        public string Action { get; set; }
        public int Count { get; set; }

        public class SearchFilters
        {
            public int? AccountId;
            public Range<DateTime?> Date;
            public string RuleCode;
            public string RuleName;
            public DetectionRuleBase.DetectionRuleMode? RuleMode;
            public Action.ActionType? Action;
        }

        public static List<FraudDetectionReport> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Module.SecuredObject, PermissionValue.Read);

            var exp = from l in DataContext.Reader.RiskRuleHistories where l.IsRuleFail.Value select l;
            if (filters != null)
            {
                if (filters.AccountId != null)
                    exp = exp.Where(l => l.Account_id == filters.AccountId);
                if (filters.Date.From != null)
                    exp = exp.Where(l => l.InsertDate >= filters.Date.From);
                if (filters.Date.To != null)
                    exp = exp.Where(l => l.InsertDate <= filters.Date.To);
                if (filters.RuleCode != null)
                    exp = exp.Where(l => l.RuleCode == filters.RuleCode);
                if (filters.RuleName != null)
                    exp = exp.Where(l => l.RuleName == filters.RuleName);
                if (filters.RuleMode != null)
                    exp = exp.Where(l => l.RuleMode == filters.RuleMode.ToString());
                if (filters.Action != null)
                    exp = exp.Where(l => l.Action == filters.Action.ToString()); 
            }
                        
            var grouped = exp.GroupBy(l => new { AccountId = l.Account_id, Action = l.Action }).OrderByDescending(g => g.Count());
            
            List<FraudDetectionReport> results = new List<FraudDetectionReport>();
            foreach (var group in grouped)
                results.Add(new FraudDetectionReport() { AccountId = group.Key.AccountId, Action = group.Key.Action, Count = group.Count() });

            var SortedResult = results.ApplySortAndPage(sortAndPage).ToList();

            return SortedResult;
        }

        public override string ToString()
        {
            return string.Format("AccountId:{0}, Action:{1}, Count:{2}", AccountId, Action, Count);
        }
    }
}
