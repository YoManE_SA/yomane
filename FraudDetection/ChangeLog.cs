﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection
{
    public class ChangeLog
    {
        public ChangeLog()
        {
            _entity = new Dal.Netpay.ChangeAudit();
            Date = DateTime.Now;

            Login login = Login.Current;
            if (login != null)
                User = string.Format("User:{0}, LoginId:{1}, Role:{2}", login.UserName, login.LoginID, login.Role);
        }

        /// <summary>
        /// For newly created objects
        /// </summary>
        /// <param name="newObj"></param>
        public ChangeLog(object newObj) : this()
        {
            Action = ActionType.Add;
            StringBuilder sb = new StringBuilder();
            foreach (var prop in newObj.GetType().GetProperties())
            {
                string name = prop.Name;
                object value = prop.GetValue(newObj);
                sb.Append(name);
                sb.Append("=");
                sb.Append(value == null ? "null" : value.ToString());
                sb.Append(" ");
            }

            NewValue = sb.ToString();
        }

        /// <summary>
        /// Creates new & old value string by comparing objects
        /// Objects must be of same type
        /// </summary>
        public ChangeLog(object oldObj, object newObj) : this()
        {
            Action = ActionType.Unchanged;
            var changes = GetChanges(oldObj, newObj);
            if (changes.Count > 0)
            {
                Action = ActionType.Change;
                StringBuilder oldVals = new StringBuilder();
                StringBuilder newVals = new StringBuilder();

                foreach (var change in changes)
                {
                    oldVals.Append(change.PropertyName);
                    oldVals.Append("=");
                    oldVals.Append(change.PropertyOldValue == null ? "null" : change.PropertyOldValue.ToString());
                    oldVals.Append(" ");

                    newVals.Append(change.PropertyName);
                    newVals.Append("=");
                    newVals.Append(change.PropertyNewValue == null ? "null" : change.PropertyNewValue.ToString());
                    newVals.Append(" ");
                }

                OldValue = oldVals.ToString();
                NewValue = newVals.ToString();
            }
        }

        private ChangeLog(Dal.Netpay.ChangeAudit entity)
        {
            _entity = entity;
        }

        public enum ActionType
        {
            Unknown = 0,
            Add = 1,
            Change = 2,
            Delete = 3,
            Unchanged = 4
        }

        private Dal.Netpay.ChangeAudit _entity;

        public ActionType Action { get { return (ActionType)Enum.Parse(typeof(ActionType), _entity.ActionType, true); } set { _entity.ActionType = value.ToString(); } }
        public string User { get { return _entity.InitiatedBy; } private set { _entity.InitiatedBy = value; } }
        public DateTime Date { get { return _entity.InsertDate; } private set { _entity.InsertDate = value; } }
        public string Location { get { return _entity.ObjectLocation; } set { _entity.ObjectLocation = value.Truncate(100); } }
        public string Name { get { return _entity.ObjectName; } set { _entity.ObjectName = value.Truncate(50); } }
        public string OldValue { get { return _entity.ValueOld; } set { _entity.ValueOld = value.Truncate(1000); } }
        public string NewValue { get { return _entity.ValueNew; } set { _entity.ValueNew = value.Truncate(1000); } }
        
        public class Filters
        {
            public Range<DateTime?> Date;
            public string User;
            public string Location;
            public string Name;
        }

        private List<dynamic> GetChanges(object oldObj, object newObj)
        {
            var results = new List<dynamic>();
            foreach (PropertyInfo oldProp in oldObj.GetType().GetProperties())
            {
                object oldValue = oldProp.GetValue(oldObj);
                if (oldValue == null)
                    oldValue = "null";
                PropertyInfo newProp = newObj.GetType().GetProperties().Where(p => p.Name == oldProp.Name).SingleOrDefault();
                if (newProp == null)
                    continue;

                object newValue = newProp.GetValue(newObj);
                if (newValue == null)
                    newValue = "null";
                if (oldValue.GetType().IsGenericType && oldValue.GetType().GetGenericTypeDefinition() == typeof(List<>))
                {
                    System.Collections.IList oldList = oldValue as System.Collections.IList;
                    System.Collections.IList newList = newValue as System.Collections.IList;
                    if (oldList.Count != newList.Count)
                        results.Add(new { PropertyName = oldProp.Name, PropertyOldValue = "Count=" + oldList.Count, PropertyNewValue = "Count=" + newList.Count });
                    else
                    {
                        for (int idx = 0; idx < oldList.Count; idx++)
                        {
                            var changes = GetChanges(oldList[idx], newList[idx]);
                            for (int idx2 = 0; idx2 < changes.Count; idx2++)
                                results.Add(new { PropertyName = oldProp.Name + "[" + idx2 + "]." + changes[idx2].PropertyName, PropertyOldValue = changes[idx2].PropertyOldValue, PropertyNewValue = changes[idx2].PropertyNewValue });
                        }
                    }
                }
                else if (oldValue.ToString() != newValue.ToString())
                    results.Add(new {PropertyName = oldProp.Name, PropertyOldValue = oldValue, PropertyNewValue = newValue });
            }

            return results;
        }

        public static List<ChangeLog> Search(Filters filters)
        {
            var exp = from l in DataContext.Reader.ChangeAudits select l;
            if (filters != null)
            {
                if (filters.Date.From != null)
                    exp = exp.Where(l => l.InsertDate >= filters.Date.From);
                if (filters.Date.To != null)
                    exp = exp.Where(l => l.InsertDate <= filters.Date.To);
                if (filters.User != null)
                    exp = exp.Where(l => l.InitiatedBy == filters.User);
                if (filters.Location != null)
                    exp = exp.Where(l => l.ObjectLocation == filters.Location);
                if (filters.Name != null)
                    exp = exp.Where(l => l.ObjectName == filters.Name);
            }

            var data = exp.Select(l => new ChangeLog(l)).ToList();
            return data;
        }

        public void Save()
        {
            if (Action == ActionType.Unchanged || Action == ActionType.Unknown)
                return;

            if (_entity.ChangeAudit_id != 0)
                throw new Exception("Logs cannot be modified");
            
            DataContext.Writer.ChangeAudits.Insert(_entity);
            DataContext.Writer.SubmitChanges();
        }
    }
}
