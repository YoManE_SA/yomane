﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetection
{
    public class ValidationResult
    {
        public bool IsValid { get; set; }
        public string Reason { get; set; }
    }
}
