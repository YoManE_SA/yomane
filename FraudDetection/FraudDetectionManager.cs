﻿using Netpay.Bll.Transactions;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.Dal.Reports;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Caching;

namespace Netpay.FraudDetection
{
    public static class FraudDetectionManager
    {
        public const bool _isActivated = false; // if false, only alerts are sent, nothing is blocked, online detection always returns 0 detections

        private static CacheItemPolicy _cachePolicy = new CacheItemPolicy() { SlidingExpiration = new TimeSpan(1, 0, 0) };
        private static MemoryCache _cache = new MemoryCache("fraudDetectionManagerCache");

        static FraudDetectionManager()
        {
            Maintenance();
        }

        private static void Maintenance()
        {
            try
            {
                //var entities = from r in DataContext.Writer.SetRiskRules select r;
                //foreach (var entity in entities)
                //{
                //    var root = entity.RuleSetting;
                //    var threshholds = root.Element("Thresholds").Elements("Threshold");
                //    foreach (var threshold in threshholds)
                //    {
                //        var action = threshold.Element("Action");
                //        if (action.Value == "BlockMerchant")
                //            action.Value = "BlockMerchantRecommendation";
                //    }
                //    entity.RuleSetting = new System.Xml.Linq.XElement(root);
                //    //DataContext.Writer.SetRiskRules.Update(entity, true);
                //}
                //DataContext.Writer.SubmitChanges();

                Logger.Log(LogSeverity.Info, LogTag.Fraud, "Fraud detection maintenance successful");
            }
            catch (Exception ex)
            {
                Logger.Log(LogSeverity.Error, LogTag.Fraud, "Fraud detection maintenance fail", ex.ToString());
            }
        }

        public class SimulationResult
        {
            internal SimulationResult(List<DetectionResult> results)
            {
                Results = results;
            }

            public class ActionSummary
            {
                public Action.ActionType ActionType { set; get; }
                public int Count { set; get; }
            }

            public List<DetectionResult> Results { private set; get; }
            public List<ActionSummary> Summary
            {
                get
                {
                    return (from r in Results where r.IsDetected group r by r.Action.Type into g select new ActionSummary { ActionType = g.Key, Count = g.Count() }).ToList();
                }
            }
        }

        /// <summary>
        /// Simulate over a set of rules & context
        /// Actions are not invoked
        /// </summary>
        /// <param name="context"></param>
        /// <param name="rules"></param>
        /// <returns></returns>
        public static SimulationResult Simulate(DetectionContext context, List<DetectionRuleBase> rules)
        {
            TimeSpan totalRuntime = new TimeSpan();
            List<DetectionResult> results = new List<DetectionResult>();

            // online
            foreach (var rule in rules.Where(r => r.Mode == DetectionRuleBase.DetectionRuleMode.Online))
            {
                if (!rule.IsEnabled)
                    continue;

                DateTime start = DateTime.Now;
                List<DetectionResult> ruleResults = rule.Detect(context);
                DateTime end = DateTime.Now;
                TimeSpan ruleRuntime = end - start;
                Console.WriteLine(rule.Name + " run time: " + ruleRuntime);
                totalRuntime += ruleRuntime;

                results.AddRange(ruleResults);
            }

            // offline
            foreach (var rule in rules.Where(r => r.Mode == DetectionRuleBase.DetectionRuleMode.Offline))
            {
                if (!rule.IsEnabled)
                    continue;

                DateTime start = DateTime.Now;
                List<DetectionResult> ruleResults = rule.Detect(null);
                DateTime end = DateTime.Now;
                TimeSpan ruleRuntime = end - start;
                Console.WriteLine(rule.Name + " run time: " + ruleRuntime);
                totalRuntime += ruleRuntime;

                results.AddRange(ruleResults);
            }

            Console.WriteLine("Total run time: " + totalRuntime);

            return new SimulationResult(results);
        }

        /// <summary>
        /// Simulate over a set of rules & merchant
        /// Actions are not invoked 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="rules"></param>
        /// <returns></returns>
        public static SimulationResult Simulate(int accountId, List<DetectionRuleBase> rules)
        {
            TimeSpan totalRuntime = new TimeSpan();
            List<DetectionResult> results = new List<DetectionResult>();
            int merchantId = GetMerchantId(accountId).Value;

            // online
            ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
            var lastTransactions = (from t in dc.DenormalizedTransactions
                                    where t.CompanyID == merchantId && t.TransactionStatus == DetectionRuleBase.transactionStatusApproved
                                    orderby t.TransactionDate descending
                                    select t).Take(20);

            foreach (DenormalizedTransaction transaction in lastTransactions)
            {
                DetectionContext context = new DetectionContext(transaction);
                foreach (var rule in rules.Where(r => r.Mode == DetectionRuleBase.DetectionRuleMode.Online))
                {
                    if (!rule.IsEnabled)
                        continue;

                    DateTime start = DateTime.Now;
                    List<DetectionResult> ruleResults = rule.Detect(context);
                    DateTime end = DateTime.Now;
                    TimeSpan ruleRuntime = end - start;
                    Console.WriteLine(rule.Name + " run time: " + ruleRuntime);
                    totalRuntime += ruleRuntime;

                    results.AddRange(ruleResults);
                }
            }

            // offline
            foreach (var rule in rules.Where(r => r.Mode == DetectionRuleBase.DetectionRuleMode.Offline))
            {
                if (!rule.IsEnabled)
                    continue;

                DateTime start = DateTime.Now;
                List<DetectionResult> ruleResults = rule.Detect(null);
                DateTime end = DateTime.Now;
                TimeSpan ruleRuntime = end - start;
                Console.WriteLine(rule.Name + " run time: " + ruleRuntime);
                totalRuntime += ruleRuntime;

                results.AddRange(ruleResults);
            }

            Console.WriteLine("Total run time: " + totalRuntime);

            return new SimulationResult(results);
        }

        /// <summary>
        /// Offline detection
        /// </summary>
        public static void DetectOffline()
        {
            foreach (Domain currentDomain in Domain.Domains.Values)
            {
                try
                {
                    Domain.Current = currentDomain;
                    if (!Infrastructure.Tasks.Lock.TryLock("FraudDetectionOffline", DateTime.Now.AddMinutes(-15)))
                        return;

                    // load offline merchant & global rules
                    var rules = DetectionRuleBase.Load();
                    rules = rules.Where(r => r.Mode == DetectionRuleBase.DetectionRuleMode.Offline).ToList();

                    // detect
                    List<DetectionResult> results = new List<DetectionResult>();
                    foreach (var rule in rules)
                    {
                        if (!rule.IsEnabled)
                            continue;

                        if (rule.RunAt != null)
                            if (rule.RunAt != DateTime.Now.DayOfWeek)
                                continue;

                        List<DetectionResult> ruleResults = rule.Detect(null);
                        results.AddRange(ruleResults);
                    }

                    // log  
                    Log(results);

                    // invoke
                    foreach (var result in results)
                    {
                        if (result.IsDetected)
                            result.Action.Invoke();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Logger.Log(LogSeverity.Error, LogTag.Fraud, ex.Message, ex.StackTrace);
                }
                finally
                {
                    Infrastructure.Tasks.Lock.Release("FraudDetectionOffline");
                }
            }
        }

        public class OnlineDetectionResult
        {
            public bool IsDetected { get; set; }
            public DetectionResult Result { get; set; }
            public List<int> LogIds { get; set; }
        }

        private static Action.ActionType[] declineActions = { Action.ActionType.BlockBin, Action.ActionType.BlockCard, Action.ActionType.BlockMerchant, Action.ActionType.DeclineAuthorization };
        
        /// <summary>
        /// Online transaction detection
        /// </summary>
        public static OnlineDetectionResult DetectOnline(DetectionContext context)
        {
            try
            {
                // load online merchant & global rules
                var rules = DetectionRuleBase.Load();
                rules = rules.Where(r => r.Mode == DetectionRuleBase.DetectionRuleMode.Online && (r.RuleAccountId == context.AccountId || r.RuleAccountId == null)).ToList();

                // detect
                List<DetectionResult> results = new List<DetectionResult>();
                foreach (var rule in rules)
                {
                    if (rule.IsEnabled)
                    {
                        List<DetectionResult> ruleResults = rule.Detect(context);
                        results.AddRange(ruleResults);
                    }
                }

                // invoke
                foreach (var result in results)
                {
                    if (result.IsDetected)
                        result.Action.Invoke();
                }

                List<int> logIds = Log(results);
                OnlineDetectionResult onlineResult = new OnlineDetectionResult() { IsDetected = false };
#pragma warning disable CS0162
                if (_isActivated)
                {
                    // select one declining result & log
                    onlineResult.Result = results.Where(r => r.IsDetected && declineActions.Contains(r.Action.Type)).FirstOrDefault();
                    onlineResult.IsDetected = onlineResult.Result != null;
                    onlineResult.LogIds = logIds;
                }
#pragma warning restore CS0162                
                return onlineResult;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Logger.Log(LogSeverity.Error, LogTag.Fraud, ex.Message, ex.StackTrace);
                return new OnlineDetectionResult() { IsDetected = false };
            }
        }

        public class TerminalCacheItem
        {
            public TerminalCacheItem(tblDebitTerminal terminal)
            {
                TerminalNumber = terminal.terminalNumber;
                Mcc = terminal.SICCodeNumber;
                Name = terminal.dt_name;
            }

            public string TerminalNumber { get; private set; }
            public short? Mcc { get; private set; }
            public string Name { get; private set; }
            //public string Acquirer { get; private set; }
        }

        public static TerminalCacheItem GetTerminal(string terminalNumber)
        {
            if (terminalNumber == null)
                return null;

            Dictionary<string, TerminalCacheItem> terminalCache = _cache.Get("terminalCache") as Dictionary<string, TerminalCacheItem>;
            if (terminalCache == null)
            {
                lock (_cache)
                {
                    if (terminalCache == null)
                    {
                        terminalCache = new Dictionary<string, TerminalCacheItem>();
                        var terminals = from t in DataContext.Reader.tblDebitTerminals where t.terminalNumber.Length > 5 select new TerminalCacheItem(t);
                        foreach (var terminal in terminals)
                            terminalCache.Add(terminal.TerminalNumber, terminal);

                        _cache.Add(new CacheItem("terminalCache", terminalCache), _cachePolicy);
                    }
                }
            }

            if (terminalCache.ContainsKey(terminalNumber))
                return terminalCache[terminalNumber];
            else
                return null;
        }
        
        public class MerchantCacheItem
        {
            public MerchantCacheItem(tblCompany merchant)
            {
                MerchantId = merchant.ID;
                Status = (MerchantStatus)merchant.ActiveStatus;
                MerchantNumber = merchant.CustomerNumber;
            }

            public int MerchantId { get; private set; }
            public string MerchantNumber { get; private set; }
            public MerchantStatus Status { get; private set; }
        }

        public static MerchantCacheItem GetMerchantCache(int merchantId)
        {
            Dictionary<int, MerchantCacheItem> merchantCache = _cache.Get("merchantCache") as Dictionary<int, MerchantCacheItem>;
            if (merchantCache == null)
            {
                lock (_cache)
                {
                    if (merchantCache == null)
                    {
                        merchantCache = new Dictionary<int, MerchantCacheItem>();
                        var merchants = from m in DataContext.Reader.tblCompanies select new MerchantCacheItem(m);
                        foreach (var merchant in merchants)
                            merchantCache.Add(merchant.MerchantId, merchant);

                        _cache.Add(new CacheItem("merchantCache", merchantCache), _cachePolicy);
                    }
                }
            }

            if (merchantCache.ContainsKey(merchantId))
                return merchantCache[merchantId];
            else
                return null;
        }

        private static Dictionary<int, int> GetIdConversionCache(string key)
        {
            Dictionary<int, int> conversionCache = _cache.Get(key) as Dictionary<int, int>;
            if (conversionCache == null)
            {
                lock (_cache)
                {
                    if (conversionCache == null)
                    {
                        var ids = from a in DataContext.Reader.Accounts
                                  where a.AccountType_id == (byte)Bll.Accounts.AccountType.Merchant
                                  select new { accountId = a.Account_id, merchantId = a.Merchant_id.Value };

                        Dictionary<int, int> accountIdToMerchantId = new Dictionary<int, int>();
                        Dictionary<int, int> merchantIdToAccountId = new Dictionary<int, int>();

                        foreach (var id in ids)
                        {
                            accountIdToMerchantId.Add(id.accountId, id.merchantId);
                            merchantIdToAccountId.Add(id.merchantId, id.accountId);
                        }

                        _cache.Add(new CacheItem("accountIdToMerchantId", accountIdToMerchantId), _cachePolicy);
                        _cache.Add(new CacheItem("merchantIdToAccountId", merchantIdToAccountId), _cachePolicy);

                        conversionCache = _cache.Get(key) as Dictionary<int, int>;
                    }
                }
            }

            return conversionCache;
        }

        public static int? GetAccountId(int? merchantId)
        {
            if (merchantId == null)
                return null;

            var cache = GetIdConversionCache("merchantIdToAccountId");
            if (!cache.ContainsKey(merchantId.Value))
                return null;

            return cache[merchantId.Value];
        }

        public static int? GetMerchantId(int? accountId)
        {
            if (accountId == null)
                return null;

            var cache = GetIdConversionCache("accountIdToMerchantId");
            if (!cache.ContainsKey(accountId.Value))
                return null;

            return cache[accountId.Value];
        }

        public static int Log(DetectionResult result)
        {
            return Log(new List<DetectionResult>() { result }).First();
        }

        public static List<int> Log(List<DetectionResult> results)
        {
            List<int> logIds = new List<int>();
            foreach (var result in results)
            {
                int? accountId = GetAccountId(result.MerchantId);
                Dal.Netpay.RiskRuleHistory entity = new Dal.Netpay.RiskRuleHistory();
                entity.Account_id = accountId;
                entity.InsertDate = DateTime.Now;
                entity.Reason = result.Message.Truncate(1000);
                entity.RuleCode = result.Rule.Code;
                entity.RuleMode = result.Rule.Mode.ToString();
                entity.RuleName = result.Rule.Name;
                entity.Action = result.Action == null ? null : result.Action.Type.ToString();
                entity.IsRuleFail = result.IsDetected;
                entity.TransFail_id = result.DeclinedTransactionId;

                DataContext.Writer.RiskRuleHistories.InsertOnSubmit(entity);
                DataContext.Writer.SubmitChanges();

                logIds.Add(entity.RiskRuleHistory_id);
            }

            return logIds;
        }

        private static int InsertDeclinedAuthorization(DetectionContext context, DetectionResult declineResult)
        {
            // billing address
            BillingAddress billingAddress = new BillingAddress();
            billingAddress.AddressLine1 = context.Address1;
            billingAddress.AddressLine2 = context.Address2;
            billingAddress.City = context.City;
            billingAddress.CountryISOCode = context.CountryIso;
            billingAddress.PostalCode = context.ZipCode;
            billingAddress.StateISOCode = context.StateIso;
            int billingAddressId = billingAddress.Save();

            // creditcard
            tblCreditCard creditcard = new tblCreditCard();
            creditcard.CompanyID = context.MerchantId;
            creditcard.BillingAddressID = billingAddressId;
            creditcard.CCard_number256 = context.CreditcardNumberEncrypted;
            creditcard.ExpMM = context.CreditcardExpirationMM;
            creditcard.ExpYY = context.CreditcardExpirationYY;
            creditcard.Member = context.CreditcardName;
            creditcard.email = context.CreditcardEmail;
            creditcard.phoneNumber = context.CreditcardPhone;
            creditcard.PersonalNumber = context.CreditcardPersonalNumber;
            creditcard.CardholderDOB = context.CreditcardDateOfBirth;
            creditcard.ccTypeID = context.CreditcardTypeId.HasValue ? context.CreditcardTypeId.Value : (short)0;
            creditcard.Comment = context.Comment;
            creditcard.BINCountry = context.CreditcardBinCountry;
            DataContext.Writer.tblCreditCards.Update(creditcard, false);
            DataContext.Writer.SubmitChanges();
            int creditcardId = creditcard.ID;

            // declined trans
            tblCompanyTransFail declinedTrans = new tblCompanyTransFail();
            declinedTrans.CompanyID = context.MerchantId;
            declinedTrans.InsertDate = DateTime.Now;
            declinedTrans.TransSource_id = context.TransactionSourceId;
            declinedTrans.TransType = context.TransactionTypeId;
            declinedTrans.CreditType = context.CreditTypeId;
            declinedTrans.CustomerID = context.PayerId;
            declinedTrans.IPAddress = context.CustomerIp;
            declinedTrans.Amount = context.Amount;
            declinedTrans.Currency = context.TransCurrency.ID;
            declinedTrans.Payments = context.Payments;
            declinedTrans.OrderNumber = context.OrderNumber;
            declinedTrans.replyCode = declineResult.Rule.Code;
            declinedTrans.DebitDeclineReason = declineResult.Message.Truncate(500);
            declinedTrans.PayforText = context.PayFor;
            declinedTrans.MerchantProduct_id = context.ProductId;
            declinedTrans.Comment = context.Comment;
            declinedTrans.TerminalNumber = context.TerminalNumber;
            declinedTrans.PaymentMethod = context.PaymentMethod;
            declinedTrans.PaymentMethod_id = context.IMethodType;
            declinedTrans.PaymentMethodID = creditcard.ID;
            declinedTrans.PaymentMethodDisplay = context.CreditcardDisplay;
            declinedTrans.isTestOnly = context.IsTest;
            declinedTrans.referringUrl = context.ReferringUrl;
            declinedTrans.DebitCompanyID = context.DebitCompanyId;
            declinedTrans.payerIdUsed = string.Empty;
            declinedTrans.DebitReferenceCode = context.DebitReferenceCode;
            declinedTrans.DebitReferenceNum = context.DebitReferenceNumer;
            declinedTrans.netpayFee_transactionCharge = context.TransactionChargeFee;
            declinedTrans.DebitFee = context.DebitFee;
            declinedTrans.IPCountry = context.IpCountry;
            declinedTrans.ctf_JumpIndex = context.CtfJumpIndex;
            declinedTrans.IsGateway = context.IsGateway;
            declinedTrans.MobileDevice_id = context.MobileDeviceId;
            declinedTrans.TransPayerInfo_id = context.TransPayerInfoId;
            declinedTrans.TransPaymentMethod_id = context.TransPaymentMethodId;
            declinedTrans.Is3DSecure = context.Is3DSecure;
            DataContext.Writer.tblCompanyTransFails.Update(declinedTrans, false);
            DataContext.Writer.SubmitChanges();

            return declinedTrans.ID;
        }

        public static List<MailAddress> AlertRecipients
        {
            get
            {
                List<MailAddress> recipients = _cache.Get("alertRecipients") as List<MailAddress>;
                if (recipients == null)
                {
                    lock (_cache)
                    {
                        if (recipients == null)
                        {
                            recipients = new List<MailAddress>();
                            string[] split = Domain.Current.FraudDetectionAlertRecipients.Split(',');
                            foreach (string recipient in split)
                                if (recipient.Trim().Length > 0)
                                    recipients.Add(new MailAddress(recipient));
                            _cache.Set("alertRecipients", recipients, _cachePolicy);
                        }
                    }
                }

                return recipients;
            }
        }
    }
}
