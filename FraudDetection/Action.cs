﻿using Netpay.Bll.Accounts;
using Netpay.Bll.Merchants;
using Netpay.Bll.Risk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using System.Threading;
using Netpay.Dal.DataAccess;
using Netpay.Bll;
using static Netpay.FraudDetection.FraudDetectionManager;

namespace Netpay.FraudDetection
{
    public class Action
    {
        // the order of values is by sevrity and must not change
        public enum ActionType
        {
            Alert = 1,
            DeclineAuthorization = 2,
            BlockMerchant = 3,
            BlockCard = 4,
            BlockBin = 5,
            BlockEmail = 6,
            BlockIp = 7,
            BlockMerchantRecommendation = 8
        }

        public Action(ActionType type, DetectionRuleBase rule, string message)
        {
            Type = type;
            Rule = rule;
            Message = message;
        }

        public ActionType Type { get; set; }
        public DetectionRuleBase Rule { get; set; }
        public int? MerchantId { get; set; }
        public string MerchantNumber
        {
            get
            {
                if (MerchantId == null)
                    return null;
                return Merchant.CachedNumbersForDomain().Where(v => v.Value == MerchantId.Value).First().Key;
            }
        }
        public string BinNumber { get; set; }
        public string Email { get; set; }
        public string IP { get; set; }
        public string CardNumber { get; set; }
        public string TerminalNumber { get; set; }
        public string Message { get; set; }

        private void SendAlert()
        {
            Merchant merchant = null;

            StringBuilder emailBody = new StringBuilder();
            if (MerchantId != null)
            {
                emailBody.Append("Merchant id: "); emailBody.Append(MerchantId); emailBody.Append("<br/>");
                merchant = Merchant.Load(MerchantNumber);
                if (merchant != null)
                    emailBody.Append("Merchant name: "); emailBody.Append(merchant.Name); emailBody.Append("<br/>");
            }
            
            if (TerminalNumber != null)
            {
                emailBody.Append("Terminal number: "); emailBody.Append(TerminalNumber); emailBody.Append("<br/>");
                TerminalCacheItem terminal = FraudDetectionManager.GetTerminal(TerminalNumber);
                if (terminal != null)
                    emailBody.Append("Terminal name: "); emailBody.Append(terminal.Name); emailBody.Append("<br/>");
            }
            
            emailBody.Append("Action: "); emailBody.Append(Type); emailBody.Append("<br/>");
            emailBody.Append("Rule code: "); emailBody.Append(Rule.Code); emailBody.Append("<br/>");
            emailBody.Append("Rule name: "); emailBody.Append(Rule.Name.ToSentence()); emailBody.Append("<br/>");
            if (BinNumber != null) { emailBody.Append("BIN: "); emailBody.Append(BinNumber); emailBody.Append("<br/>");}          
            if (Email != null) { emailBody.Append("Email: "); emailBody.Append(Email); emailBody.Append("<br/>"); }    
            if (IP != null) { emailBody.Append("IP: "); emailBody.Append(IP); emailBody.Append("<br/>"); }   
            emailBody.Append("Message: "); emailBody.Append(Message); emailBody.Append("<br/>");
            
            MailMessage email = new MailMessage();

            string fromEmailAddress = Netpay.Infrastructure.Application.MailAddressFrom;
            if (string.IsNullOrEmpty(fromEmailAddress))
            {
                throw new Exception("mailAddressFrom is empty in config file, fraud detection alert email can not be sent.");
            }

            email.From = new MailAddress(fromEmailAddress, "Fraud detection");
            foreach (MailAddress emailAddress in FraudDetectionManager.AlertRecipients)
                email.To.Add(emailAddress);
            email.Subject = "Detection Alert";
            email.BodyEncoding = Encoding.UTF8;
            email.IsBodyHtml = true;
            email.Body = emailBody.ToString();

            try
            {
                Netpay.Infrastructure.Email.SmtpClient.Send(email);
            }
            catch (Exception ex)
            {
                Logger.Log(LogSeverity.Error, LogTag.Fraud, ex.Message, ex.StackTrace);
            }

            // merchant will receive alert if blocked
            if (FraudDetectionManager._isActivated && Type == ActionType.BlockMerchant && merchant != null)
            {
                PendingEvents.CreateForMerchant(CommonTypes.PendingEventType.EmailMerchantAccountBlocked, merchant.ID, null);
            }
        }

#pragma warning disable CS0162
        public void Invoke()
        {
            try
            {
                if (FraudDetectionManager._isActivated)
                {
                    switch (Type)
                    {
                        case Action.ActionType.Alert:
                        case Action.ActionType.BlockMerchantRecommendation:
                            SendAlert();
                            break;
                        case Action.ActionType.DeclineAuthorization:
                            // implemented in remote_charge.asp
                            break;
                        case Action.ActionType.BlockMerchant:
                            var merchant = (from m in DataContext.Writer.tblCompanies where m.ID == MerchantId select m).SingleOrDefault();
                            if (merchant != null && merchant.ActiveStatus == (int)MerchantStatus.Processing)
                            {
                                merchant.ActiveStatus = (int)Infrastructure.MerchantStatus.Blocked;
                                DataContext.Writer.tblCompanies.Update(merchant, true);
                                DataContext.Writer.SubmitChanges();
                                SendAlert();
                            }
                            break;
                        case Action.ActionType.BlockCard:
                            RiskItem.RiskItemValue ccVal = new RiskItem.RiskItemValue();
                            ccVal.Value = CardNumber;
                            ccVal.ValueType = RiskItem.RiskValueType.AccountValue1;
                            RiskItem.Create(RiskItem.RiskSource.System, RiskItem.RiskListType.Black, new List<RiskItem.RiskItemValue>() { ccVal }, null, null, null, null, null, null, Rule.Code, "Blocked by rule: " + Rule.Name, null);
                            break;
                        case Action.ActionType.BlockBin:
                            RiskItem.RiskItemValue binVal = new RiskItem.RiskItemValue();
                            binVal.Value = BinNumber;
                            binVal.ValueType = RiskItem.RiskValueType.Bin;
                            RiskItem.Create(RiskItem.RiskSource.System, RiskItem.RiskListType.Black, new List<RiskItem.RiskItemValue>() { binVal }, null, null, null, null, null, null, Rule.Code, "Blocked by rule: " + Rule.Name, null);
                            break;
                        case Action.ActionType.BlockEmail:
                            RiskItem.RiskItemValue emailVal = new RiskItem.RiskItemValue();
                            emailVal.Value = Email;
                            emailVal.ValueType = RiskItem.RiskValueType.Email;
                            RiskItem.Create(RiskItem.RiskSource.System, RiskItem.RiskListType.Black, new List<RiskItem.RiskItemValue>() { emailVal }, null, null, null, null, null, null, Rule.Code, "Blocked by rule: " + Rule.Name, null);
                            break;
                        case Action.ActionType.BlockIp:
                            RiskItem.RiskItemValue ipVal = new RiskItem.RiskItemValue();
                            ipVal.Value = IP;
                            ipVal.ValueType = RiskItem.RiskValueType.IP;
                            RiskItem.Create(RiskItem.RiskSource.System, RiskItem.RiskListType.Black, new List<RiskItem.RiskItemValue>() { ipVal }, null, null, null, null, null, null, Rule.Code, "Blocked by rule: " + Rule.Name, null);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    switch (Type)
                    {
                        case Action.ActionType.Alert:
                        case Action.ActionType.BlockMerchantRecommendation:
                        case Action.ActionType.BlockMerchant:
                            SendAlert();
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LogSeverity.Error, LogTag.Fraud, ex.Message, ex.StackTrace);
            }
        }
#pragma warning restore CS0162

        public override string ToString()
        {
            return String.Format("{0}, {1}", this.Type.ToString(), Message);
        }
    }
}
