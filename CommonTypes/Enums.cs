﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.CommonTypes
{
    public enum PaymentMethodType
    {
        Unknown = 0,
        System = 1,
        CreditCard = 2,
        ECheck = 3, // usa ach transaction
        BankTransfer = 4, // european pull / push bank transfer
        PhoneDebit = 5,
        Wallet = 6,
        ExternalWallet = 7
    }

    public enum PaymentMethodGroupEnum
    {
        Other = 0,
        CreditCard = 1,
        DirectDebit = 2,
        InstantOnlinkBankTransfer = 3,
        EuropeanDirectDebit = 4,
        OnlineBankTransfer = 7,
        Micropay = 6,
        Wallet = 8,
        Prepaid = 9
    }

    public enum PaymentMethodEnum
    {
        Unknown = 0,
        ManualFees = 1,
        Admin = 2,
        SystemFees = 3,
        RollingReserve = 4,
        BankFees = 5,
        MaxInternal = 14,

        CC_MIN = 20,
        CCUnknown = 20,
        CCIsracard = 21,
        CCVisa = 22,
        CCDiners = 23,
        CCAmex = 24,
        CCMastercard = 25,
        CCDirect = 26,
        CCOther = 27,
        CCMaestro = 28,
        CCTogglecard = 29,
        CCJcb = 31,
        CCDiscover = 32,
        CCCup = 33,
        CCLiveCash = 34,
        CCOneCard = 35,

        CC_LamdaCard = 98,
        CC_MAX = 99,

        EC_MIN = 100,
        ECCheck = 100,
        Giropay = 101,
        DirectPay24 = 102,
        PinELV = 103,
        PaySafeCard = 104,
        CashTicket = 105,
        Pezelewy24 = 106,
        EPS = 107,
        Wallie = 108,
        IDEAL = 109,
        TeleIngreso = 110,
        MoneyBookers = 111,
        ELV = 112,
        YellowPay = 113,
        Lastschrift = 114,
        Bankeinzug = 115,
        UKInstantDebit = 116,
        AutomatischeIncasso = 117,
        CargoBancario = 118,
        UKDirectDebit = 119,
        AustriaDirectDebit = 120,
        GermanyDirectDebit = 121,
        NetherlandsDirectDebit = 122,
        SpainDirectDebit = 123,
        PushBankTransfer = 149,
        EC_MAX = 150,

        CS_MIN = 151,
        WebMoney = 151, //custom group
        CS_MAX = 199,

        PD_MIN = 200,
        MicroPaymentsIN = 200,
        MicroPaymentsOut = 201,
        IvrPerCall = 202,
        IvrPerMin = 203,
        IvrFreeTime = 204,
        Qiwi = 205,

        PD_MAX = 230,
        PayPal = 231,
        GoogleCheckout = 232,
        MoneyBookersHpp = 233,
        PaymentMethodMax = 20000,

        CCTheeCardNetwork = 5001,

        //Min and Max values for PrePaid payment methods.
        PP_MIN = 5000, // For testing the value is 240 , the real value will be 5000
        PP_MAX = 20000 // For testing only , the real value will be 20000
    }

    public enum ProcessTransactionStatus
    {
        Unknown = -1,
        Unsupported = 1,
        Pending = 2,
        Approved = 3,
        Declined = 4,
        Redirect = 5
    }

    public enum ProcessTransactionType
    {
        Unknown = -1,
        Authorize = 1,
        Capture = 2,
        Sale = 3,
        Reverse = 4,
        Refund = 5,
        Credit = 6,
        BankBack = 7,
    }

    public enum ProcessTransactionSource
    {
        Unknown = -1,
        Manual = 0,
        Swipe = 1,
        Moto = 2,
        Ivr = 3,
        ECommerce = 4,
        ECommerce3D = 5,
        Sms = 6,
        PaymentPage = 7,
        System = 8,
    }

    public enum TransactionStage
    {
        Unknown = -1,
        Created = 0,
        DetectMethod = 10,
        LoadConfig = 20,
        Validate = 30,
        LoadPrevTrans = 40,
        Risk = 50,
        Process = 60,
        Save = 70,
        PostProcess = 80,
        Complete = 100,
    }

    public enum PendingEventType
    {
        Unknown = 0,
        FeesTransaction = 10,
        InfoEmailSendClient = 100,
        InfoEmailSendMerchant = 101,
        InfoEmailSendAffiliate = 102,
        InfoEmailSendRiskMultipleCardsOnEmail = 103,
        CreateRecurringSeries = 200,
        RecurringSendNotify = 201,
        RefundRequestSendNotify = 202,
        PendingSendNotify = 203,
        AutoCapture = 300,
        GenerateInstallments = 400,
        GenerateOldInstallments = 401,
        CreateInvoice = 402,
        InfoSmsSendClient = 403,
        SmsRegisterDevice = 404,
        WalletRegisterEmail = 600,
        EmailPhotoCopy = 2001,
        EmailChargeBack = 2002,
        EmailMerchantFraud = 2003,
        EmailMerchantAccountBlocked = 2004,
    }

    public enum TransactionAmountGroup
    {
        Unknown = 0,
        Merchants = 100,
        Transactions = 1000,
        Fees = 1001,
        DirectBank = 1002,
        Reserve = 1003,
        OtherCharges = 1004,
        Taxes = 1005,
    }

    public enum TransactionAmountType
    {
        Unknown = 0,
        Authorize = 1,
        Capture = 2,
        Refund = 3,
        Chargeback = 4,
        InstallmentCapture = 6,
        InstallmentItem = 7,
        AmountMax = 500,
        FeeAuthorization = 1000,
        FeeTransaction = 1001,
        FeeLine = 1002,
        FeeRefund = 1003,
        FeeClarification = 1004,
        FeeChb = 1005,
        FeeFinance = 1010,
        FeeDeclined = 1300,
        FeeCCStorage = 1400,
        FeeChargeCCStorage = 1401,
        FeeCreateInvoice = 1450,
        FeeMax = 1500,
        BankPayed = 2000,
        //BankDirectDebit = 2001,
        BankFeeAuthorization = 3000,
        BankFeeTransaction = 3001,
        BankFeeLine = 3002,
        BankFeeRefund = 3003,
        BankFeeClarification = 3004,
        BankFeeChb = 3005,
        BankFeeMax = 3500,
        AffiliateFeeProcess = 4000,
        AffiliateFeeChargeback = 4001,
        RollingRelease = 5001,
        RollingReserve = 5002,
        SystemCharge = 6001,
        AdminCharge = 6002,
        HandlingFee = 6003,
        DebitorDirectPayment = 7000,
        DebitorDirectFees = 7001,
        Tax = 8000,
    }

    public enum TransactionHistoryType
    {
        Unknown = 0,
        Authorize = 1,
        Capture = 2,
        Refund = 3,
        Chargeback = 4,
        UpdateFees = 5,
        RetrievalRequest = 6,
        FraudSet = 7,
        FraudUnset = 8,
        InfoEmailSendPhotoCopy = 20,
        InfoEmailSendChargeBack = 21,
        InfoEmailSendFraud = 22,
        InfoEmailSendAccountBlocked = 23,
        InfoCreateRecurring = 41,
        InfoAutoRefund = 42,
        InfoRecurringSendNotify = 43,
        InfoRefundRequestSendNotify = 44,
        InfoPendingSendNotify = 45,
        InfoEmailSendClient = 51,
        InfoSmsSendClient = 52,
        InfoEmailSendMerchant = 53,
        InfoEmailSendAffiliate = 54,
        InfoEmailSendRiskMultipleCardsOnEmail = 55,
        AutoCapture = 80,
        Installment = 91,
        OldInstallment = 92,
        DetectRetrivalReqAfterRefund = 100,
        CreateInvoice = 120,
    }

    public enum Currency
    {
        UnsignedUnknown = 255,
        Unknown = -1,
        ILS = 0,
        USD = 1,
        EUR = 2,
        GBP = 3,
        AUD = 4,
        CAD = 5,
        JPY = 6,
        NOK = 7,
        PLN = 8,
        MXN = 9,
        ZAR = 10,
        RUB = 11,
        TRY = 12,
        CHF = 13,
        INR = 14,
        DKK = 15,
        SEK = 16,
        CNY = 17,
        HUF = 18,
        NZD = 19,
        HKD = 20,
        KRW = 21,
        SGD = 22,
        THB = 23,
        BSD = 24,
    }

    public enum Language
    {
        Unknown = -1,
        Hebrew = 0,
        English = 1,
        French = 2,
        Spanish = 3,
        Italian = 4,
        Portuguese = 5,
        Lithuanian = 6,
        Russian = 7,
        German = 8,
        Chinese = 9,
    }

    public enum LogHistoryType
    {
        RemoteCardStorage = 1,
        RemoteRefundRequest = 2,
        ThirdPartyInvoices = 3,
        RemoteCardStorageV2 = 4,
        BlockedItemsManagementV2 = 5,
        CustomerRegistration = 6,
        CardConnectRegister = 7,
        DataPullingRequest = 8,
        HostedPageCharge_SystemProcess = 31,
        HostedPageCharge_MerchantNotification = 32,
        OrangeOutboundCommunication = 50,
        OrangeInboundNotification = 51,
        Other = 99
    }

    public enum FailSource
    {
        ISSUER = 0,
        RISK = 1,
        GATEWAY = 2
    }
}
