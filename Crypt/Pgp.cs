﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Netpay.Crypt
{
	public class Pgp
	{
		const string GNUPG_PATH = @"C:\Program Files\GNU\GnuPG\decrypt.cmd";

		const string GNUPG_PATH_X64 = @"C:\Program Files (x86)\GNU\GnuPG\decrypt.cmd";

		public static string DecryptFile(string sPathSource, string sUsername, string sPassword, bool bUseHomeDir)
		{
			FileInfo file = new FileInfo(GNUPG_PATH);
			Process p = new Process();
			p.StartInfo.UseShellExecute = false;
			p.StartInfo.RedirectStandardOutput = true;
			p.StartInfo.FileName = (file.Exists ? GNUPG_PATH : GNUPG_PATH_X64);
			if (bUseHomeDir) p.StartInfo.FileName = p.StartInfo.FileName.Replace("decrypt.cmd", "decrypt_homedir.cmd");
			if (!string.IsNullOrEmpty(sUsername))
			{
				p.StartInfo.UserName = sUsername;
				p.StartInfo.Password = new System.Security.SecureString();
				foreach (char chr in sPassword.ToCharArray()) p.StartInfo.Password.AppendChar(chr);
			}
			p.StartInfo.Arguments = sPathSource;
			p.Start();
			p.WaitForExit();
			string output = p.StandardOutput.ReadToEnd();
			p.Close();
			return output;
		}

		public static string DecryptFile(string sPathSource, string sUsername, string sPassword)
		{
			return DecryptFile(sPathSource, sUsername, sPassword, false);
		}

		public static string DecryptFile(string sPathSource, bool bUseHomeDir)
		{
			return DecryptFile(sPathSource, null, null, true);
		}

		public static string DecryptFile(string sPathSource)
		{
			return DecryptFile(sPathSource, null, null, false);
		}
	}
}
