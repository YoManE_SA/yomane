﻿/* COM interface to .Net Cryptography By Udi Azulay 2015-03-05 */
using System;
using System.Runtime.InteropServices;
using System.Data;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

namespace Netpay.Crypt
{
    [ComVisible(true), Guid("107E9D44-D381-412A-ABED-46AA7FC1A7E3"), ClassInterface(ClassInterfaceType.AutoDual)]
    public class Utility
    {
        public Utility() { }

        [return: MarshalAs(UnmanagedType.IDispatch)] public Blob CreateBlob() { return new Blob(); }
        [return: MarshalAs(UnmanagedType.IDispatch)] public Blob CreateTextBlob(string text) { return new Blob() { Text = text }; }
        [return: MarshalAs(UnmanagedType.IDispatch)] public SymEncryption CreateSymetric(string algorithmName) { return new SymEncryption(algorithmName); }
        [return: MarshalAs(UnmanagedType.IDispatch)] public SymEncryption LoadSymetricFromFile(string fileName, string password = null) { return SymEncryption.FromFile(fileName, password); }
        [return: MarshalAs(UnmanagedType.IDispatch)] public SymEncryption GetSymetricByKeyIndex(int keyIndex) { return SymEncryption.GetKey(keyIndex); }
        [return: MarshalAs(UnmanagedType.IDispatch)] public Hmac CreateHmac(string algorithmName) { return Hmac.Create(algorithmName); }
        [return: MarshalAs(UnmanagedType.IDispatch)] public Hmac CreateHmacFromFile(string fileName) { return new Hmac(fileName); }

        public void EncryptFile(string keyFile, string srcFileName, string destFileName, int bufferSize = 4096)
        {
            SymEncryption.EncryptFile(keyFile, srcFileName, destFileName, bufferSize);
        }

        public void DecryptFile(string keyFile, string srcFileName, string destFileName, int bufferSize = 4096)
        {
            SymEncryption.DecryptFile(keyFile, srcFileName, destFileName, bufferSize);
        }

        public string DefaultKeyStore { get { return SymEncryption.DefaultKeyStore; } /*set { SymEncryption.DefaultKeyStore = value; } */}


        public void Delay(int miliseconds) { System.Threading.Thread.Sleep(miliseconds); }
    }

    public static class SQL 
    {
        [SqlFunction(DataAccess = DataAccessKind.None)]
        public static SqlBinary Encrypt(SqlInt32 sqlnKeyID, SqlString sqlsText)
        {
            return (SqlBinary)SymEncryption.GetKey((int)sqlnKeyID).Encrypt(new Blob(sqlsText.ToString())).Bytes;
        }

        [SqlFunction(DataAccess = DataAccessKind.None)]
        public static SqlString Decrypt(SqlInt32 sqlnKeyID, SqlBinary sqlbBinary)
        {
            return (SqlString)SymEncryption.GetKey((int)sqlnKeyID).Decrypt(new Blob((byte[])sqlbBinary)).Text;
        }

        /*
        [SqlProcedure]
        public static void EncryptStr(SqlInt32 sqlnKeyID, SqlString sqlsText)
        {
            byte[] baEncrypted = GetKey((int)sqlnKeyID).Encrypt(new Blob() { Text = sqlsText.ToString() }).Bytes;
            SqlMetaData mdColumn = new SqlMetaData("Encrypted", SqlDbType.Binary);
            SqlMetaData[] mdRecord = new SqlMetaData[1];
            mdRecord[0] = mdColumn;
            SqlDataRecord rsData = new SqlDataRecord(mdRecord);
            rsData.SetBytes(0, 0, baEncrypted, 0, baEncrypted.GetLength(0));
            SqlContext.Pipe.SendResultsRow(rsData);
        }

        [SqlProcedure]
        public static void DecryptStr(SqlInt32 sqlnKeyID, SqlBinary sqlbBinary)
        {
            SqlContext.Pipe.Send(GetKey((int)sqlnKeyID).Decrypt(new Blob((byte[])sqlbBinary)).Text);
        }

        [SqlProcedure]
        public static void EncryptFileDPAPI(SqlInt32 sqlnKeyID)
        {
            GetKey((int)sqlnKeyID)
            byte[] bytesKey = Encoding.ASCII.GetBytes("12345678901234567890123456789012");
            string KEY_PATH = @"c:\bin\key." + sqlnKeyID.ToString().Trim();
            FileStream fsKey = new FileStream(KEY_PATH, FileMode.Open, FileAccess.Read);
            fsKey.Read(bytesKey, 0, 32);
            fsKey.Close();
            byte[] bytesKeyEncrypted;
            bytesKeyEncrypted = DPAPI.Encrypt(DPAPI.KeyType.MachineKey, bytesKey, null, null);
            FileStream fsKeyEncrypted = new FileStream(KEY_PATH + ".dpa", FileMode.Create, FileAccess.ReadWrite);
            fsKeyEncrypted.Write(bytesKeyEncrypted, 0, bytesKeyEncrypted.Length);
            fsKeyEncrypted.Close();
        }    
        */
    }

}
