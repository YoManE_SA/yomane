﻿/* COM interface to .Net Cryptography By Udi Azulay 2015-03-05 */
using System;
using System.Runtime.InteropServices;

namespace Netpay.Crypt
{
    [ComVisible(true)]
    public enum ProtectScope { None = -1, CurrentUser = 0, LocalMachine = 1 }

    [ComVisible(true)]
    public enum FileType { BinaryFile, TextFile, HexFile }

    [ComVisible(true), ClassInterface(ClassInterfaceType.AutoDual)]
    public class Blob
    {
        private byte[] _data;
        private System.Text.Encoding _textEncoding;
        public Blob() { _data = new byte[0]; _textEncoding = System.Text.Encoding.Default; }
        public Blob(byte[] data) : this() { _data = data; }
        public Blob(string text) { _textEncoding = System.Text.Encoding.Default; Text = text; }

        [DispId(0)]
        public byte this[int index] { get { return _data[index]; } set { _data[index] = value; } }
        public int Size { get { if (_data == null) return -1; return _data.Length; } set { _data = new byte[value]; } }
        public byte[] Bytes {
            [return: MarshalAs(UnmanagedType.Struct, SafeArraySubType = VarEnum.VT_ARRAY)] /*used in classic asp for Respose.BinaryWrite*/
            get { return _data; }
            set { _data = value; } 
        }

        public System.Text.Encoding TextEncoding { get { return _textEncoding; } set { _textEncoding = value; } }
        public string TextEncodingCode { get { return _textEncoding.EncodingName; } set { _textEncoding = System.Text.Encoding.GetEncoding(value); } }
        public string Text { get { return _textEncoding.GetString(_data); } set { _data = _textEncoding.GetBytes(value); } }

        public string Ascii { get { return System.Text.Encoding.ASCII.GetString(_data); } set { _data = System.Text.Encoding.ASCII.GetBytes(value); } }
        public string Unicode { get { return System.Text.Encoding.Unicode.GetString(_data); } set { _data = System.Text.Encoding.Unicode.GetBytes(value); } }
        public string UTF8 { get { return System.Text.Encoding.UTF8.GetString(_data); } set { _data = System.Text.Encoding.UTF8.GetBytes(value); } }
        public string Windows1255 { get { return System.Text.Encoding.GetEncoding("windows-1255").GetString(_data); } set { _data = System.Text.Encoding.GetEncoding("windows-1255").GetBytes(value); } }

        public string UrlEncode { get { return System.Uri.EscapeDataString(UTF8); } set { UTF8 = System.Uri.UnescapeDataString(value); } }
        public string Base64 { get { return System.Convert.ToBase64String(_data); } set { _data = System.Convert.FromBase64String(value); } }

        public void XorWith([MarshalAs(UnmanagedType.IDispatch)] Blob blob) 
        {
            Xor(_data, blob.Bytes);
        }

        public static void Xor(byte[] dest, byte[] src)
        {
            for (var i = 0; i < dest.Length; i++) dest[i] ^= src[i % src.Length];
        }
        
        public static string ToHexString(byte[] value, string seperator = "")
        {
            var ret = new System.Text.StringBuilder(value.Length * 2);
            for (int i = 0; i < value.Length; i++)
            {
                if (i > 0) ret.Append(seperator);
                ret.Append(value[i].ToString("X2"));
            }
            return ret.ToString();
        }

        public static byte[] FromHexString(string value, string seperator = "")
        {
            if (value == null) return null;
            var ret = new byte[value.Length / 2 + seperator.Length];
            for (int i = 0; i < value.Length; i += 2 + seperator.Length)
                ret[i / 2] = Convert.ToByte(value.Substring(i, 2), 16);
            return ret;
        }

        public string Hex { get { return ToHexString(_data); } set { _data = FromHexString(value); } }

        public object[] Binary 
        {
            get { var ret = new object[_data.Length]; for (int i = 0; i < _data.Length; i++) ret[i] = _data[i]; return ret; } 
            set { _data = new byte[value.Length]; for (int i = 0; i < value.Length; i++) _data[i] = byte.Parse(value.ToString()); } 
        }

        public void LoadFromFile(string fileName, FileType fileType = FileType.BinaryFile) 
        {
            switch (fileType) {
                case FileType.BinaryFile: _data = System.IO.File.ReadAllBytes(fileName); break;
                case FileType.TextFile: Text = System.IO.File.ReadAllText(fileName, _textEncoding); break;
                case FileType.HexFile: Hex = System.IO.File.ReadAllText(fileName, _textEncoding); break;
            }
        }

        public void SaveToFile(string fileName, FileType fileType = FileType.BinaryFile) 
        {
            switch (fileType)
            {
                case FileType.BinaryFile: System.IO.File.WriteAllBytes(fileName, _data); break;
                case FileType.TextFile: System.IO.File.WriteAllText(fileName, Text, _textEncoding); break;
                case FileType.HexFile: System.IO.File.WriteAllText(fileName, Hex); break;
            }
        }

        public void SaveToRegistry(string keyName, string valueName, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.BinaryFile: Microsoft.Win32.Registry.SetValue(keyName, valueName, _data); break;
                case FileType.TextFile: Microsoft.Win32.Registry.SetValue(keyName, valueName, Text); break;
                case FileType.HexFile: Microsoft.Win32.Registry.SetValue(keyName, valueName, Hex); break;
            }
        }

        public void LoadFromRegistry(string keyName, string valueName, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.BinaryFile: _data = (byte[])Microsoft.Win32.Registry.GetValue(keyName, valueName, null); break;
                case FileType.TextFile: Text = (string)Microsoft.Win32.Registry.GetValue(keyName, valueName, null); break;
                case FileType.HexFile: Hex = (string)Microsoft.Win32.Registry.GetValue(keyName, valueName, null); break;
            }
        }

        public Blob Protect(ProtectScope scope, Blob optionalEntropy = null) 
        {
            return new Blob(System.Security.Cryptography.ProtectedData.Protect(_data, optionalEntropy != null ? optionalEntropy.Bytes : null, (System.Security.Cryptography.DataProtectionScope)(int) scope));
        }

        public Blob Unprotect(ProtectScope scope, Blob optionalEntropy = null)
        {
            return new Blob(System.Security.Cryptography.ProtectedData.Unprotect(_data, optionalEntropy != null ? optionalEntropy.Bytes : null, (System.Security.Cryptography.DataProtectionScope)(int) scope));
        }

        [return: MarshalAs(UnmanagedType.IDispatch)]
        public Blob Hash(string algorithmName)
        {
            using (var crypto = System.Security.Cryptography.HashAlgorithm.Create(algorithmName))
                return new Blob(crypto.ComputeHash(_data));
        }

    }


  
}
