﻿/* COM interface to .Net Cryptography By Udi Azulay 2015-03-05 */
using System;
using System.Runtime.InteropServices;

namespace Netpay.Crypt
{
    [ComVisible(true), ClassInterface(ClassInterfaceType.AutoDual)]
    public class Hmac : IDisposable
    {
        private System.Security.Cryptography.KeyedHashAlgorithm _crypto;
        public Hmac(System.Security.Cryptography.KeyedHashAlgorithm crypto) { _crypto = crypto; }
        public Hmac(string fileName) { LoadToFile(fileName); }
        public static Hmac Create(string algorithmName) { return new Hmac(System.Security.Cryptography.KeyedHashAlgorithm.Create(algorithmName)); }
        public void Dispose() { if (_crypto != null) { (_crypto as IDisposable).Dispose(); _crypto = null; } }

        public int InputBlockSize { get { return _crypto.InputBlockSize; }  }
        public int OutputBlockSize { get { return _crypto.OutputBlockSize; }  }

        public Blob Key { [return: MarshalAs(UnmanagedType.IDispatch)] get { return new Blob(_crypto.Key); } [param: MarshalAs(UnmanagedType.IDispatch)] set { _crypto.Key = value.Bytes; } }

        public void Clear() { _crypto.Clear(); }

        public Blob Calculate(Blob data) 
        {
            return new Blob(_crypto.ComputeHash(data.Bytes));
        }

        public void SaveToFile(string fileName, ProtectScope protectScope = ProtectScope.None)
        {
            using (var fs = new System.IO.StreamWriter(fileName))
            {
                fs.WriteLine("//Key File Info, Create Date:" + DateTime.Now.ToLongDateString());
                fs.WriteLine("ALG:" + _crypto.ToString());
                if (protectScope != ProtectScope.None) fs.WriteLine("KEY:" + Blob.ToHexString(System.Security.Cryptography.ProtectedData.Protect(_crypto.Key, null, (System.Security.Cryptography.DataProtectionScope)(int) protectScope)));
                else fs.WriteLine("KEY:" + Blob.ToHexString(_crypto.Key));
                fs.Close();
            }
        }

        public void LoadToFile(string fileName)
        {
            using (var fs = new System.IO.StreamReader(fileName))
            {
                while (!fs.EndOfStream)
                {
                    var line = fs.ReadLine();
                    if (line.StartsWith("//")) continue;
                    var valuePair = line.Split(':');
                    if (valuePair.Length != 2) throw new Exception("Unknown value:" + line);
                    var value = valuePair[1].Trim();
                    try {
                        switch (valuePair[0].Trim())
                        {
                            case "ALG": _crypto = System.Security.Cryptography.HMAC.Create(value); break;
                            case "KEY": _crypto.Key = Blob.FromHexString(value); break;
                            case "DPAPI":
                                var scope = (System.Security.Cryptography.DataProtectionScope)Enum.Parse(typeof(System.Security.Cryptography.DataProtectionScope), value);
                                _crypto.Key = System.Security.Cryptography.ProtectedData.Unprotect(_crypto.Key, null, scope);
                                break;
                        }
                    } catch { throw new Exception("Unknown value:" + line); }

                }
                fs.Close();
            }
        }
    }
}
