﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Process.Modules;
using System.Collections.Specialized;
using System.Web;

using Netpay.Infrastructure;

namespace Netpay.Process.NetpayConnector
{
    public class RecurringSendNotify : IPostProcessor
    {
		public bool Disabled { get; set; }

        private string formatResponseString(TransactionContext transaction, System.Text.Encoding enc) 
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("TransType=" + (transaction.Request.type == ProcessTransactionType.Authorize ? 1 : 0));
            sb.Append("&Reply=" + transaction.Response.ErrorCode);
            sb.Append("&TransID=" + transaction.ID);
            sb.Append("&Date=" + System.Web.HttpUtility.UrlEncode(transaction.Response.TimeStamp.ToString(), enc));
            sb.Append("&Order=" + System.Web.HttpUtility.UrlEncode(transaction.Request.merchantRefCode, enc)); 
            sb.Append("&Amount=" + transaction.Request.amount.ToString("0.00").Replace(".00", "")); 
            sb.Append("&Payments=" + transaction.Request.installmets); 
            sb.Append("&Currency=" + transaction.CurrencyID);
            sb.Append("&ConfirmationNum=" + System.Web.HttpUtility.UrlEncode(transaction.Response.ApprovalNumber, enc));
            sb.Append("&Comment=" + System.Web.HttpUtility.UrlEncode(transaction.Request.comment, enc));
            sb.Append("&ReplyDesc=" + System.Web.HttpUtility.UrlEncode(transaction.Response.Reason, enc));
            sb.Append("&CCType=" + System.Web.HttpUtility.UrlEncode(transaction.Method.paymentMethod.ToString().Substring(2), enc));
            sb.Append("&Descriptor=" + System.Web.HttpUtility.UrlEncode(transaction.Config.Descriptor, enc));
            sb.Append("&RecurringSeries=");
            if (transaction.MethodText != null && transaction.MethodText.Length >= 4) sb.Append("&Last4=" + System.Web.HttpUtility.UrlEncode(transaction.MethodText.Substring(transaction.MethodText.Length - 4), enc));
            sb.Append("&ccStorageID=");
            return sb.ToString();
        }

        PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData) 
        {
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding("windows-1255");
            NameValueCollection reqString = HttpUtility.ParseQueryString(pendingData.Parameters);
			NetpayDataContext dc = new NetpayDataContext(Domain.Get(transaction.Domain).Sql1ConnectionString);
            string notifyUrl = reqString["Url"];
            if (notifyUrl == null) notifyUrl = (from c in dc.tblCompanyChargeAdmins where c.company_id == transaction.Config.MerchantID && c.isRecurringReply select c.recurringReplyUrl).SingleOrDefault();
            if (notifyUrl == null) return PostProcessResult.Succseeded;
            var notifyInfo = dc.RecurringGetNotificationInfo(reqString["RecurringCharge"].ToInt32());
            foreach (var r in notifyInfo)
            {
                string recurringQueryString = formatResponseString(transaction, enc) + "&recur_initialID=" + r.FirstTransID + "&recur_seriesID=" + r.SeriesID + "&recur_chargeCount=" + r.ChargeCount + "&recur_chargeNum=" + r.ChargeNumber + "&recur_status=" + r.RecurringStatus;
			    notifyUrl += (notifyUrl.IndexOf('?') > -1 ? '&' : '?') + recurringQueryString;
                string retText = "";
                System.Net.HttpStatusCode httpRet = HttpClient.SendHttpRequest(notifyUrl, out retText, null, enc, 0);
                processEngine.AddLog(transaction, 0, TransactionHistoryType.InfoRecurringSendNotify, (httpRet != System.Net.HttpStatusCode.OK ? "Fail" : ""), (int)httpRet, notifyUrl, httpRet == System.Net.HttpStatusCode.OK);
				return (httpRet == System.Net.HttpStatusCode.OK) ? PostProcessResult.Succseeded : (pendingData.Retries > 0 ? PostProcessResult.Failed : PostProcessResult.FailedNoRetry);
            }
            return PostProcessResult.Succseeded;
        }

    }
}
