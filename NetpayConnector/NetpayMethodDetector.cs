﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;

using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{
	public class NetpayMethodDetector : IPaymentMethodDetector
	{
		public bool DetectMethod(DetectMethodData data)
		{
			if (data.paymentMethod >= PaymentMethodEnum.CC_MIN && data.paymentMethod <= PaymentMethodEnum.CC_MAX)
			{
				return DetectCreditcard(data);
			}
			else if (data.paymentMethod >= PaymentMethodEnum.EC_MIN && data.paymentMethod <= PaymentMethodEnum.EC_MAX)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool DetectCreditcard(DetectMethodData data)
		{
			string cardPan = data.account;
			if (cardPan == null)
				throw new Exception("account number is null");
			data.binCountry = null;
			data.paymentMethod = PaymentMethodEnum.Unknown;

			if (cardPan.Length == 8)
			{	// Isracard local
				data.binCountry = "IL";
				data.paymentMethod = PaymentMethodEnum.CCIsracard;
				data.binValue = cardPan.Substring(0, 6);
			}
			else if (cardPan.Length == 9)
			{
				if (cardPan.StartsWith("7"))
				{	// Direct local
					data.binCountry = "IL";
					data.paymentMethod = PaymentMethodEnum.CCDirect;
				}
				else
				{	// Isracard local
					data.binCountry = "IL";
					data.paymentMethod = PaymentMethodEnum.CCIsracard;
				}
				data.binValue = cardPan.Substring(0, 6);
			}
			if (data.paymentMethod != PaymentMethodEnum.Unknown)
				return true;

			NetpayDataContext dc = Infrastructure.DataContext.Reader;
			var creditcardBin = (from ccb in dc.tblCreditCardBINs where ccb.BIN == cardPan.Substring(0, ccb.BINLen.GetValueOrDefault()) orderby ccb.BINLen descending select ccb).FirstOrDefault();
			if (creditcardBin == null)
			{
				if (cardPan.Length == 16)
				{
					data.binValue = cardPan.Substring(0, 6);
					switch (cardPan.Substring(0, 1))
					{
						case "4":
							//oledbData.Execute "IF NOT EXISTS (SELECT BIN FROM tblCreditCardBIN WHERE BIN='" & sBin & "') INSERT INTO tblCreditCardBIN(BIN, isoCode, PaymentMethod, CCName, CCType) VALUES ('" & sBin & "', '--', 22, 'VI', 2)"
							data.paymentMethod = PaymentMethodEnum.CCVisa;
							break;
						case "5":
							//oledbData.Execute "IF NOT EXISTS (SELECT BIN FROM tblCreditCardBIN WHERE BIN='" & sBin & "') INSERT INTO tblCreditCardBIN(BIN, isoCode, PaymentMethod, CCName, CCType) VALUES ('" & sBin & "', '--', 25, 'MC', 5)"
							data.paymentMethod = PaymentMethodEnum.CCMastercard;
							break;
					}
				}
				return true;
			}
			else
			{
				data.paymentMethod = (PaymentMethodEnum)creditcardBin.PaymentMethod;
				data.binCountry = creditcardBin.isoCode;
				data.binValue = creditcardBin.BIN;
				return true;
			}
		}
	}
}
