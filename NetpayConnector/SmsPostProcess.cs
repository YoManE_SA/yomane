﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.Bll;

using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{
	public class SmsPostProcess : IPostProcessor
	{
		public bool Disabled { get; set; }
        public SmsPostProcess() { }
		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			System.Collections.Specialized.NameValueCollection pendingParams = System.Web.HttpUtility.ParseQueryString(pendingData.Parameters == null ? "" : pendingData.Parameters);
			if (transaction.Response.Status == ProcessTransactionStatus.Approved && (transaction.Request.type == ProcessTransactionType.Capture || transaction.Request.type == ProcessTransactionType.Sale))
				Bll.SMS.SMSService.SendSms(transaction.ID, pendingParams["Phone"], pendingParams["MerchantText"]);
			return PostProcessResult.Succseeded;
		}
	}
}
