﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Process.Modules;
using System.Collections.Specialized;
using System.Web;

using Netpay.Infrastructure;


namespace Netpay.Process.NetpayConnector
{
    public class PendingSendNotify : IPostProcessor
    {
		public bool Disabled { get; set; }

        private string formatResponseString(int pendingId, TransactionContext transaction, System.Text.Encoding enc) 
        {
			TransactionType netpayTransactionType; CreditType netpayCreditType;
			NetpayPersistor.MapProcessTypeToNetpayType(transaction.Request.type, out netpayTransactionType, out netpayCreditType);

			StringBuilder sb = new StringBuilder();
			sb.Append("Reply=" + transaction.Response.ErrorCode);
			sb.Append("&transID=" + transaction.ID);
			sb.Append("&Date=" + transaction.Response.TimeStamp.ToString("dd/MM/yyy hh:mm:ss").ToEncodedUrl(enc));
			sb.Append("&TypeCredit=" + (int) netpayCreditType);
			sb.Append("&Payments=" + transaction.Request.installmets);
			sb.Append("&Amount=" + transaction.Request.amount.ToString("0.00"));
			sb.Append("&Currency=" + transaction.CurrencyID);
			sb.Append("&Order=" + transaction.Request.merchantRefCode.ToEncodedUrl(enc));
			sb.Append("&reference=" + pendingId);
            return sb.ToString();
        }

        PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData) 
        {
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
            NameValueCollection reqString = HttpUtility.ParseQueryString(pendingData.Parameters == null ? "" : pendingData.Parameters);
			NetpayDataContext dc = new NetpayDataContext(Domain.Get(transaction.Domain).Sql1ConnectionString);
            string notifyUrl = reqString["Url"];
            if (notifyUrl == null) notifyUrl = (from c in dc.tblCompanyChargeAdmins where c.company_id == transaction.Config.MerchantID && c.isPendingReply select c.PendingReplyUrl).SingleOrDefault();
            if (notifyUrl == null) return PostProcessResult.Succseeded;
			int pendingId = 0;
			if (transaction.Response.Status == ProcessTransactionStatus.Approved)
				pendingId = (from p in dc.tblLogPendingFinalizes where p.TransPassID == transaction.Response.ID select p.PendingID).SingleOrDefault();
			else if (transaction.Response.Status == ProcessTransactionStatus.Declined)
				pendingId = (from p in dc.tblLogPendingFinalizes where p.TransFailID == transaction.Response.ID select p.PendingID).SingleOrDefault();
			if (pendingId != 0)
			{
                string retText = "";
				string reqData = formatResponseString(pendingId, transaction, enc);
                System.Net.HttpStatusCode httpRet = HttpClient.SendHttpRequest(notifyUrl, out retText, reqData, enc, 0);
                notifyUrl += (notifyUrl.IndexOf('?') > -1 ? '&' : '?') + reqData;
				processEngine.AddLog(transaction, 0, TransactionHistoryType.InfoPendingSendNotify, (httpRet != System.Net.HttpStatusCode.OK ? "Fail" : ""), (int)httpRet, notifyUrl, httpRet == System.Net.HttpStatusCode.OK);
                return (httpRet == System.Net.HttpStatusCode.OK) ? PostProcessResult.Succseeded : (pendingData.Retries > 0 ? PostProcessResult.Failed : PostProcessResult.FailedNoRetry);
            }
            return PostProcessResult.Failed;
        }
    }
}
