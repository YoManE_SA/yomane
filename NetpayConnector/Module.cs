﻿using System;
using System.Linq;
using Netpay.Infrastructure;
using System.Collections.Generic;
namespace Netpay.Process.NetpayConnector
{
    public class Module : Infrastructure.Module
    {
        public const string ModuleName = "NetpayConnector";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }

        protected override void OnActivate(EventArgs e)
        {
            Dictionary<string, object> customData = new Dictionary<string, object>();
            List<string> fields = Infrastructure.Email.Template.GetFields(typeof(TransactionContext));
            Infrastructure.Email.Template.EmailTestData emailTestData = GetEmailTestData;
            customData.Add("EmailTestData", emailTestData);
            customData.Add("Fields", fields);
            Bll.EmailTemplates.RegisterTemplates(ModuleName, customData);

            base.OnActivate(e);
        }

        private ProcessEngine _testEngine = null;
        public ProcessEngine GetTestEngine()
        {
            if (_testEngine == null)
            {
                _testEngine = new ProcessEngine();
                _testEngine.Persistors.Add(new NetpayPersistor());
            }
            return _testEngine;
        }

        private object GetEmailTestData(Infrastructure.Email.Template template, string param)
        {
            var emailType = (EmailTransaction.EmailType) Enum.Parse(typeof(EmailTransaction.EmailType), System.IO.Path.GetFileNameWithoutExtension(template.FileName));
            CommonTypes.ProcessTransactionStatus transStatus = CommonTypes.ProcessTransactionStatus.Unknown;
            var sf = new Bll.Transactions.Transaction.SearchFilters();
            switch (emailType) {
                case EmailTransaction.EmailType.CustomerNotify_ApprovedTrans:
                case EmailTransaction.EmailType.MerchantNotify_ApprovedTrans:
                case EmailTransaction.EmailType.PartnerNotify_ApprovedTrans:
                    transStatus = CommonTypes.ProcessTransactionStatus.Approved; break;
                case EmailTransaction.EmailType.MerchantNotify_DeclinedTrans:
                    transStatus = CommonTypes.ProcessTransactionStatus.Declined; break;
                case EmailTransaction.EmailType.MerchantNotify_RiskMultipleCardsOnEmail:
                    transStatus = CommonTypes.ProcessTransactionStatus.Declined; break;
                case EmailTransaction.EmailType.MerchantNotify_Denied:
                case EmailTransaction.EmailType.MerchantNotify_Clarify:
                    sf.isChargeback = true;
                    transStatus = CommonTypes.ProcessTransactionStatus.Approved; break;
                case EmailTransaction.EmailType.CartSection:
                case EmailTransaction.EmailType.CartItem:
                    sf.hasCart = true;
                    transStatus = CommonTypes.ProcessTransactionStatus.Approved; break;
            }
            int? transId = param.ToNullableInt();
            if (transId == null) {
                var npStatus = NetpayPersistor.MapProcessStatusToNetpayStatus(transStatus, CommonTypes.ProcessTransactionType.Sale);
                transId = Bll.Transactions.Transaction.Search(new Bll.Transactions.Transaction.LoadOptions(npStatus, new SortAndPage(0, 1), true, true), sf).Select(v=> (int?)v.ID).FirstOrDefault();
            }
            if (!transId.HasValue) throw new Exception("Test Transaction not found");
            var ret = GetTestEngine().Load(new TransLoadInfo(Domain.Current.Host, transId.Value, transStatus, CommonTypes.ProcessTransactionType.Sale));
            ret.CartTemplate = EmailTransaction.FormatCart(ret.Response.Status, ret.Request.type, ret.Response.ID, Bll.International.Language.KnownLanguageFromCulture(template.Language));
            return ret;
        }
    }
}
