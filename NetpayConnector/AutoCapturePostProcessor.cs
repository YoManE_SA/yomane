﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.Netpay;
using Netpay.Process.Modules;
using Netpay.Infrastructure;
using System.Collections.Specialized;
using System.Web;
using Netpay.Dal.DataAccess;


namespace Netpay.Process.NetpayConnector
{
	public class AutoCapturePostProcessor : IPostProcessor
	{
		public bool Disabled { get; set; }

		private enum AutoCaptureChargeAttemptResult
		{
			Success = 1,
			NoTransAuth = 0,
			NoRemoteChargeUrl = -1,
			NoRemoteChargeString = -2,
			NoReply = -3,
			NoReplyCode = -4,
			NoTransID = -5,
			InvalidTransID = -6,
			ConnectionError = -7
		}

		AutoCaptureChargeAttemptResult AutoCaptureAttemptCharge(TransactionContext transaction, out int captureID, out bool isCaptured, NetpayDataContextBase dc)
		{
			isCaptured = false;
			captureID = 0;
			string remoteChargeUrl = Domain.Get(transaction.Domain).ProcessUrl;
			if (string.IsNullOrEmpty(remoteChargeUrl)) 
				return AutoCaptureChargeAttemptResult.NoRemoteChargeUrl;
			string remoteChargeString = "TransType=2&RequestSource=29&CompanyNum=" + transaction.Request.loginInfo.MerchantID + "&TransApprovalID=" + transaction.Response.ID.ToString("0") + "&Amount=" + transaction.Request.amount.ToString("0.00") + "&Currency=" + transaction.Request.currencyISO + "&ClientIP=" + transaction.Request.clientIP + "&Comment=" + transaction.Request.comment + " AutoCapture " + transaction.Response.ID.ToString("0");
			if (string.IsNullOrEmpty(remoteChargeString)) 
				return AutoCaptureChargeAttemptResult.NoRemoteChargeString;
			string remoteChargeReply = string.Empty;
			HttpClient.SendHttpRequest(remoteChargeUrl, out remoteChargeReply, remoteChargeString);
			if (string.IsNullOrEmpty(remoteChargeReply)) 
				return AutoCaptureChargeAttemptResult.NoReply;
			string replyCode = HttpClient.GetQueryStringValue(remoteChargeReply, "Reply");
			if (string.IsNullOrEmpty(replyCode)) 
				return AutoCaptureChargeAttemptResult.NoReplyCode;
			string transID = HttpClient.GetQueryStringValue(remoteChargeReply, "TransID");
			if (string.IsNullOrEmpty(transID)) 
				return AutoCaptureChargeAttemptResult.NoTransID;
			if (!int.TryParse(transID, out captureID)) 
				return AutoCaptureChargeAttemptResult.InvalidTransID;
			if ((replyCode == "520") || (replyCode == "521")) 
				return AutoCaptureChargeAttemptResult.ConnectionError;

			isCaptured = (replyCode == "000");
			return AutoCaptureChargeAttemptResult.Success;
		}

		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			NameValueCollection reqString = HttpUtility.ParseQueryString(pendingData.Parameters);
			if (pendingData.EventType != PendingEventType.AutoCapture) 
				throw new ApplicationException("AutoCapturePostProcessor does not support event " + pendingData.EventType.ToString());
			NetpayDataContext dc = new NetpayDataContext(Domain.Get(transaction.Domain).Sql1ConnectionString);
			
			int transID;
			bool isCaptured;
			AutoCaptureChargeAttemptResult result = AutoCaptureAttemptCharge(transaction, out transID, out isCaptured, dc);
			if (result == AutoCaptureChargeAttemptResult.Success)
			{
				tblAutoCapture ac = (from tbl in dc.tblAutoCaptures where tbl.AuthorizedTransactionID == transaction.ID select tbl).SingleOrDefault();
				ac.ActualDate = DateTime.Now;
				if (isCaptured) 
					ac.CaptureTransactionID = transID; 
				else 
					ac.DeclineTransactionID = transID;
				processEngine.AddLog(transaction, SaveOptions.None, TransactionHistoryType.AutoCapture, null, null, null, isCaptured);
				dc.SubmitChanges();
			}
			else
				throw new ApplicationException("AutoCapture attempt failed for transaction " + transaction.ID + " (" + result + ")");

			return PostProcessResult.Succseeded;
		}
	}
}