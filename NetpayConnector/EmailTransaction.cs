﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Netpay.CommonTypes;
using Netpay.Process.Modules;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Email;

namespace Netpay.Process.NetpayConnector
{
	/// <summary>
	/// handling email sends for transactions
	/// </summary>
	public class EmailTransaction : IPostProcessor
	{
		public enum EmailType
		{
			CustomerNotify_ApprovedTrans,
			MerchantNotify_ApprovedTrans,
            MerchantNotify_DeclinedTrans,
            PartnerNotify_ApprovedTrans,
			MerchantNotify_RiskMultipleCardsOnEmail,
            MerchantNotify_Denied,
            MerchantNotify_Clarify,
            MerchantNotify_Fraud,
            MerchantNotify_AccountBlocked,
            CartSection,
			CartItem,
		}

		public string Bcc { get; set; }
		public bool PreviewFile { get; set; }
		public bool Disabled { get; set; }

		public EmailTransaction() { }

		/// <summary>
		/// get email message file name
		/// </summary>
		public static string GetFileName(Netpay.Infrastructure.Domain domain, EmailType emailType, Language mailLanguage, string department)
		{
			string ret;
			if (!string.IsNullOrEmpty(department)) {
				ret = domain.EmailTemplatePath + @"\" + department + "\\" + emailType.ToString() + (mailLanguage == Language.Hebrew ? "_he" : string.Empty) + ".htm";
				if (System.IO.File.Exists(ret)) return ret;
			}
			ret = domain.EmailTemplatePath + @"\" + emailType.ToString() + (mailLanguage == Language.Hebrew ? "_he" : string.Empty) + ".htm";
			return ret;
		}

		private void SaveEmailFile(string filePath, string message)
		{
			TextWriter txw = new StreamWriter(filePath);
			txw.Write(message);
			txw.Close();
			txw.Dispose();
		}

        public string getEmailText(ProcessEngine processEngine, string domain, int transID, EmailType emailType, Language? mailLanguage) 
        {
			var d = Netpay.Infrastructure.Domain.Get(domain);
			TransactionContext transaction = processEngine.Load(new TransLoadInfo(domain, transID, ProcessTransactionStatus.Approved, ProcessTransactionType.Sale));
            if (transaction == null) throw new ApplicationException("Could not load transaction #" + transID);
			if (mailLanguage == null) mailLanguage = transaction.Config.MerchantLanguage;
			string subject, body = FormatMesage.GetMessageText(GetFileName(d, emailType, mailLanguage.GetValueOrDefault(0), transaction.Config.MerchantParentCompanyName), out subject);
            System.Globalization.CultureInfo cultureInfo = mailLanguage == Language.Hebrew ? System.Globalization.CultureInfo.GetCultureInfo("he-il") : System.Globalization.CultureInfo.GetCultureInfo("en-us");
			System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Netpay.Process.Resources.Enums", typeof(ProcessEngine).Assembly);
			return FormatMesage.FormatMessage(body, transaction, rm, cultureInfo);
        }

		public static string FormatCart(CommonTypes.ProcessTransactionStatus status, CommonTypes.ProcessTransactionType type, int transID, Language mailLanguage)
		{
			string subject;
			Bll.Shop.Cart.Basket cart = Bll.Shop.Cart.Basket.LoadForTransaction(NetpayPersistor.MapProcessStatusToNetpayStatus(status, type), transID);
			if (cart == null) return null;
			System.Globalization.CultureInfo cultureInfo = mailLanguage == Language.Hebrew ? System.Globalization.CultureInfo.GetCultureInfo("he-il") : System.Globalization.CultureInfo.GetCultureInfo("en-us");
			System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Netpay.Process.Resources.Enums", typeof(ProcessEngine).Assembly);
			string body = FormatMesage.GetMessageText(GetFileName(Infrastructure.Domain.Current, EmailType.CartSection, mailLanguage, null), out subject);
			string itemBody = FormatMesage.GetMessageText(GetFileName(Infrastructure.Domain.Current, EmailType.CartItem, mailLanguage, null), out subject);
			var sbItems = new System.Text.StringBuilder();
            var items = cart.BasketItems;
            if (items != null) { 
                foreach (var item in items)
				    sbItems.Append(FormatMesage.FormatMessage(itemBody, item, rm, cultureInfo));
            }
            body = body.Replace("[Cart.Items]", sbItems.ToString());
			return FormatMesage.FormatMessage(body, cart, rm, cultureInfo);
		}

		/// <summary>
		/// interface IPostProcess method implementation
		/// </summary>
		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
			var domain =  Netpay.Infrastructure.Domain.Get(transaction.Domain);
            TransactionHistoryType eventType = 0;
            try
            {
                string subject = null, body = null, toEmail = null;
			    string fromEmail = transaction.Config.GatewaySupportEmail;
			    string fromName = transaction.Config.GatewayBrandName;
                System.Collections.Specialized.NameValueCollection pendingParams = System.Web.HttpUtility.ParseQueryString(pendingData.Parameters.EmptyIfNull());
                Language language = Language.English;
				switch (pendingData.EventType)
				{
					case PendingEventType.InfoEmailSendClient:
						eventType = TransactionHistoryType.InfoEmailSendClient;
						message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                        toEmail = transaction.Request.customer.email.Replace(';', ',').Replace(" ", "");
						if (transaction.Method != null && transaction.Method.binCountry != null) language = (transaction.Method.binCountry.ToUpper() == "IL" ? Language.Hebrew : Language.English);
						body = FormatMesage.GetMessageText(GetFileName(domain, EmailType.CustomerNotify_ApprovedTrans, language, transaction.Config.MerchantParentCompanyName), out subject);
						break;
					case PendingEventType.InfoEmailSendMerchant:
						eventType = TransactionHistoryType.InfoEmailSendMerchant;
						message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                        toEmail = transaction.Config.MerchantNotifyEmail.Replace(';', ',').Replace(" ", "");
						language = transaction.Config.MerchantLanguage;
						body = FormatMesage.GetMessageText(GetFileName(domain, (transaction.Response.Status == ProcessTransactionStatus.Declined ? EmailType.MerchantNotify_DeclinedTrans : EmailType.MerchantNotify_ApprovedTrans), language, transaction.Config.MerchantParentCompanyName), out subject);
						break;
					case PendingEventType.InfoEmailSendAffiliate:
						eventType = TransactionHistoryType.InfoEmailSendAffiliate;
						message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                        toEmail = "udi@netpay.co.il";
						body = FormatMesage.GetMessageText(GetFileName(domain, EmailType.PartnerNotify_ApprovedTrans, language, transaction.Config.MerchantParentCompanyName), out subject);
						break;
					case PendingEventType.InfoEmailSendRiskMultipleCardsOnEmail:
						eventType = TransactionHistoryType.InfoEmailSendRiskMultipleCardsOnEmail;
						message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                        toEmail = transaction.Config.MerchantSupportEmail.Replace(';', ',').Replace(" ", "");
						body = FormatMesage.GetMessageText(GetFileName(domain, EmailType.MerchantNotify_RiskMultipleCardsOnEmail, language, transaction.Config.MerchantParentCompanyName), out subject);
						break;
                    case PendingEventType.EmailPhotoCopy:
                        eventType = TransactionHistoryType.InfoEmailSendPhotoCopy;
                        fromEmail = transaction.Config.GatewayComplianceEmail;
						language = transaction.Config.MerchantLanguage;
                        message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
						body = FormatMesage.GetMessageText(GetFileName(domain, EmailType.MerchantNotify_Clarify, language, transaction.Config.MerchantParentCompanyName), out subject);
                        if (string.IsNullOrEmpty(transaction.Config.MerchantNotifyEmailChb)) {
                            toEmail = fromEmail;
                            subject = "mailbox missing: " + subject;
                        } else toEmail = transaction.Config.MerchantNotifyEmailChb.Replace(';', ',').Replace(" ", "");
                        break;
                    case PendingEventType.EmailChargeBack:
                        eventType = TransactionHistoryType.InfoEmailSendChargeBack;
                        fromEmail = transaction.Config.GatewayComplianceEmail;
                        message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
						language = transaction.Config.MerchantLanguage;
						body = FormatMesage.GetMessageText(GetFileName(domain, EmailType.MerchantNotify_Denied, language, transaction.Config.MerchantParentCompanyName), out subject);
                        if (string.IsNullOrEmpty(transaction.Config.MerchantNotifyEmailChb)) {
                            toEmail = fromEmail;
                            subject = "mailbox missing: " + subject;
                        } else toEmail = transaction.Config.MerchantNotifyEmailChb.Replace(';', ',').Replace(" ", "");
                        break;

                    case PendingEventType.EmailMerchantAccountBlocked:
                        eventType = TransactionHistoryType.InfoEmailSendAccountBlocked;
                        fromEmail = transaction.Config.GatewayComplianceEmail;
                        message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                        language = transaction.Config.MerchantLanguage;
                        body = FormatMesage.GetMessageText(GetFileName(domain, EmailType.MerchantNotify_AccountBlocked, language, transaction.Config.MerchantParentCompanyName), out subject);
                        if (string.IsNullOrEmpty(transaction.Config.MerchantNotifyEmailChb))
                        {
                            toEmail = fromEmail;
                            subject = "mailbox missing: " + subject;
                        }
                        else toEmail = transaction.Config.MerchantNotifyEmailChb.Replace(';', ',').Replace(" ", "");
                        break;

                    case PendingEventType.EmailMerchantFraud:
                        eventType = TransactionHistoryType.InfoEmailSendFraud;
                        fromEmail = transaction.Config.GatewayComplianceEmail;
                        message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                        language = transaction.Config.MerchantLanguage;
                        body = FormatMesage.GetMessageText(GetFileName(domain, EmailType.MerchantNotify_Fraud, language, transaction.Config.MerchantParentCompanyName), out subject);
                        if (string.IsNullOrEmpty(transaction.Config.MerchantNotifyEmailChb)) {
                            toEmail = fromEmail;
                            subject = "mailbox missing: " + subject;
                        } else toEmail = transaction.Config.MerchantNotifyEmailChb.Replace(';', ',').Replace(" ", "");
                        break;


                    default:
						throw new Exception("EmailTransaction does not support event " + pendingData.EventType.ToString());
				}
                if (!string.IsNullOrEmpty(pendingParams["Email"])) toEmail = pendingParams["Email"];
                try { message.To.Add(toEmail); }
                catch (Exception ex) { processEngine.AddLog(transaction, 0, eventType, ex.Message, null, null, false); return PostProcessResult.FailedNoRetry; }
                message.IsBodyHtml = true;
				if (string.IsNullOrEmpty(subject)) subject = "Transaction Confirmation";
				
				transaction.CartTemplate = FormatCart(transaction.Response.Status, transaction.Request.type, transaction.Response.ID, language);

				System.Globalization.CultureInfo cultureInfo = language == Language.Hebrew ? System.Globalization.CultureInfo.GetCultureInfo("he-il") : System.Globalization.CultureInfo.GetCultureInfo("en-us");
				System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Netpay.Process.Resources.Enums", typeof(ProcessEngine).Assembly);
				message.Subject = FormatMesage.FormatMessage(subject, transaction, rm, cultureInfo);
				message.Body = FormatMesage.FormatMessage(body, transaction, rm, cultureInfo);
				if (!string.IsNullOrEmpty(Bcc)) message.Bcc.Add(Bcc.Replace(';', ',').Replace(" ", ""));
				System.Net.Mail.SmtpClient smtpClient = SmtpClient.GetClient();
				//if (PreviewFile) SaveEmailFile(AppDomain.CurrentDomain.BaseDirectory + pendingData.EventType.ToString() + ".htm", message.Body);
				smtpClient.Send(message);
				processEngine.AddLog(transaction, 0, eventType, "Sent To: " + message.To.ToString(), null, null, true);
			}
			catch (System.Net.Mail.SmtpFailedRecipientException ex) 
			{
				processEngine.AddLog(transaction, 0, eventType, ex.Message, null, null, false);
				return PostProcessResult.FailedNoRetry;
			} 
			catch (System.Net.Mail.SmtpException ex) 
			{
				processEngine.AddLog(transaction, 0, eventType, ex.Message, null, null, false);
				return PostProcessResult.Failed;
			}
			catch (Exception ex)
			{
				processEngine.AddLog(transaction, 0, eventType, ex.Message, null, null, false);
				throw new Exception("Email:" + ex.Message, ex);
			}
			return PostProcessResult.Succseeded;
		}
	}
}
