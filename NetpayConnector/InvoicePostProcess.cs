﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.Bll;

using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{
	public class InvoicePostProcess : IPostProcessor
	{
		public bool Disabled { get; set; }
		public InvoicePostProcess() { }
		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			if (transaction.Response.Status == ProcessTransactionStatus.Approved && (transaction.Request.type == ProcessTransactionType.Capture || transaction.Request.type == ProcessTransactionType.Sale || transaction.Request.type == ProcessTransactionType.Refund || transaction.Request.type == ProcessTransactionType.Reverse))
				Bll.Invoices.InvoiceCreator.CreateInvoice(transaction.ID);
			return PostProcessResult.Succseeded;
		}
	}
}
