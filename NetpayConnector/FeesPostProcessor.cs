﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;

using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{

	public class FeesPostProcessor : IPostProcessor, IPersistComplete
	{
		public bool Disabled { get; set; }
        public void LoadComplete(TransactionContext transaction) { }
        public void SaveComplete(TransactionContext transaction, SaveOptions options)
        {
            if ((options & SaveOptions.UpdateExisting) == 0)
                Application.PendingEventManager.RegisterPending(transaction, PendingEventType.FeesTransaction, null);
        }

        private tblCompanyCreditFee getCreditFees(TransactionContext transaction, NetpayDataContext dc)
		{
			tblCompanyCreditFee fees;
			if((transaction.Request.type == ProcessTransactionType.Capture || transaction.Request.type == ProcessTransactionType.Sale) && transaction.Response.Status == ProcessTransactionStatus.Approved){
				fees = (from ccf in dc.tblCompanyCreditFees where
					ccf.CCF_CompanyID == transaction.Config.MerchantID &&
					ccf.CCF_CurrencyID == transaction.CurrencyID &&
					ccf.CCF_PaymentMethod == (int)transaction.Request.paymentMethod &&
					(ccf.CCF_ListBINs == "" || ccf.CCF_ListBINs.Contains(transaction.Method.binCountry.EmptyIfNull()))
					orderby ccf.CCF_ListBINs descending
					select ccf).FirstOrDefault();
				//if (fees == null)
				//	throw new ApplicationException(string.Format("Company fees not found. pass transID={0} tblCompanyCreditFees.CCF_ID={1}", transaction.ID, transaction.Config.TerminalID));
			} else {
				fees = (from ccf in dc.tblCompanyCreditFees where
					ccf.CCF_CompanyID == transaction.Config.MerchantID &&
					ccf.CCF_CurrencyID == transaction.CurrencyID &&
					ccf.CCF_PaymentMethod == (int)transaction.Request.paymentMethod &&
					(ccf.CCF_ListBINs == "" || ccf.CCF_ListBINs.Contains(transaction.Method.binCountry.EmptyIfNull()))
					orderby ccf.CCF_IsDisabled ascending, ccf.CCF_ListBINs descending
					select ccf).FirstOrDefault();
				//if (fees == null)
				//	throw new ApplicationException(string.Format("Company fees not found. fail/approval/refund transID={0} tblCompanyCreditFees.CCF_ID={1}", transaction.ID, transaction.Config.TerminalID));
			}
			return fees;
		}

		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			Domain domain = Domain.Get(transaction.Domain);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			tblCompanyCreditFee fees = getCreditFees(transaction, dc);
			if (fees == null){
				processEngine.AddLog(transaction, 0, TransactionHistoryType.UpdateFees, "Merchant fees not found", null, null, false);
				return PostProcessResult.FailedNoRetry;
			}
			if (transaction.Response.Status == ProcessTransactionStatus.Approved)
			{
				bool usedGradedFees = false;
				if (transaction.Config.MerchantFeeCurrency.GetValueOrDefault(Currency.Unknown) != Currency.Unknown)
				{
					if (transaction.Request.type == ProcessTransactionType.Capture || transaction.Request.type == ProcessTransactionType.Sale)
					{
						tblMerchantProcessingData pd = (from mpd in dc.tblMerchantProcessingDatas where mpd.MerchantID == transaction.Config.MerchantID select mpd).SingleOrDefault();
						if (pd != null) {
							if (pd.MPD_CffResetDate2 == null) pd.MPD_CffResetDate2 = DateTime.Now;
							if ((DateTime.Now.Month != pd.MPD_CffResetDate2.Month) || (DateTime.Now.Year != pd.MPD_CffResetDate2.Year)) {
								dc.ExecuteCommand("Update tblMerchantProcessingData SET MPD_CffResetDate2 = GetDate(), MPD_CffCurAmount2 = 0 Where MerchantID=" + transaction.Config.MerchantID);
								pd.MPD_CffCurAmount2 = 0;
							}
							decimal CCF_Precent = (from t in dc.tblCompanyFeesFloors where t.CFF_CompanyID == transaction.Config.MerchantID && t.CFF_TotalTo <= pd.MPD_CffCurAmount2 orderby t.CFF_TotalTo descending select t.CFF_Precent).FirstOrDefault();
							if (CCF_Precent != 0) {
								processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.FeeTransaction, (transaction.Request.amount * (CCF_Precent / 100)), (transaction.Request.amount * (CCF_Precent / 100)), null));
								dc.ExecuteCommand("Update tblMerchantProcessingData SET " +
									" MPD_CffCurAmount2 = MPD_CffCurAmount2 + " + (new Netpay.Bll.Money(transaction.Request.amount, Bll.Currency.Get(transaction.Request.currencyISO)).ConvertTo((int)transaction.Config.MerchantFeeCurrency.Value)).Amount.ToString("0.00") + 
									" Where MerchantID=" + transaction.Config.MerchantID);
								usedGradedFees = true;
							}
						}
					}
				}				
				
				switch (transaction.Request.type)
				{
					case ProcessTransactionType.Unknown:
						break;
					case ProcessTransactionType.Authorize:
						processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.Authorize, -transaction.Request.amount, null, null));
						processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.FeeAuthorization, fees.CCF_ApproveFixedFee, fees.CCF_ApproveFixedFee, null));
						break;
					case ProcessTransactionType.Capture:
					case ProcessTransactionType.Sale:
						if (transaction.Request.installmets > 1) processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.InstallmentCapture, -transaction.Request.amount, null, null));
						else processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.Capture, -transaction.Request.amount, -transaction.Request.amount, null));
                        processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.FeeLine, fees.CCF_FixedFee, fees.CCF_FixedFee, null));
						if (!usedGradedFees) processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.FeeTransaction, transaction.Request.amount * (fees.CCF_PercentFee / 100), transaction.Request.amount * (fees.CCF_PercentFee / 100), null));
						break;
					case ProcessTransactionType.Refund:
					case ProcessTransactionType.Reverse:
						processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.FeeRefund, fees.CCF_RefundFixedFee, fees.CCF_RefundFixedFee, null));
						break;
					default:
						break;
				}

			}
			else if (transaction.Response.Status == ProcessTransactionStatus.Declined)
			{
				processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.FeeDeclined, fees.CCF_FailFixedFee, fees.CCF_FailFixedFee, null));
			}
			processEngine.AddLog(transaction, 0, TransactionHistoryType.UpdateFees, null, null, null, true);
			return PostProcessResult.Succseeded;
		}
    }
}
