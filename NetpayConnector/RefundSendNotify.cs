﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Process.Modules;
using System.Collections.Specialized;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Bll;


namespace Netpay.Process.NetpayConnector
{
    public class RefundSendNotify : IPostProcessor
    {
		public bool Disabled { get; set; }

        private string formatResponseString(Bll.Transactions.RefundRequest refReq, TransactionContext transaction, System.Text.Encoding enc) 
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("RequestId=" + refReq.ID);
            sb.Append("&OriginalTransId=" + refReq.TransactionID);
            sb.Append("&RefundTransId=" + transaction.ID);
            sb.Append("&Amount=" + refReq.RequestAmount.ToAmountFormat());
			sb.Append("&Currency=" + Bll.Currency.Get(refReq.CurrencyIso));
            sb.Append("&RefundStatus=" + System.Web.HttpUtility.UrlEncode(refReq.Status.ToString(), enc));
            sb.Append("&Date=" + System.Web.HttpUtility.UrlEncode(refReq.Date.ToDateFormat(), enc));
            return sb.ToString();
        }

        PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData) 
        {
            System.Text.Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
            NameValueCollection reqString = HttpUtility.ParseQueryString(pendingData.Parameters == null ? "" : pendingData.Parameters);
			NetpayDataContext dc = new NetpayDataContext(Domain.Get(transaction.Domain).Sql1ConnectionString);
            string notifyUrl = reqString["Url"];
            if (notifyUrl == null) notifyUrl = (from c in dc.tblCompanyChargeAdmins where c.company_id == transaction.Config.MerchantID && c.isNotifyRefundRequestApproved select c.NotifyRefundRequestApprovedUrl).SingleOrDefault();
            if (notifyUrl == null) return PostProcessResult.Succseeded;
            var refReq = Bll.Transactions.RefundRequest.RequestsForTrasaction(transaction.Request.refTransID.Value).FirstOrDefault();
            if (refReq != null) {
                string retText = "";
                string reqData = formatResponseString(refReq, transaction, enc);
                System.Net.HttpStatusCode httpRet = HttpClient.SendHttpRequest(notifyUrl, out retText, reqData, enc, 0);
                notifyUrl += (notifyUrl.IndexOf('?') > -1 ? '&' : '?') + reqData;
                processEngine.AddLog(transaction, 0, TransactionHistoryType.InfoRefundRequestSendNotify, (httpRet != System.Net.HttpStatusCode.OK ? "Fail" : ""), (int)httpRet, notifyUrl, httpRet == System.Net.HttpStatusCode.OK);
                return (httpRet == System.Net.HttpStatusCode.OK) ? PostProcessResult.Succseeded : PostProcessResult.Failed;
            }
            return PostProcessResult.Failed;
        }
    }
}
