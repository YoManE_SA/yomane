﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using Netpay.CommonTypes;
using Netpay.Process.Modules;
using Netpay.Infrastructure.Email;

namespace Netpay.Process.NetpayConnector
{
	/// <summary>
	/// handling email sends for transactions
	/// </summary>
	public class EmailEvent : IPostProcessor
	{
		public enum EmailType
		{
            Wallet_Register,
            Wallet_Password,
			Merchant_Register,
			Merchant_Password,
        }

		public string Bcc { get; set; }
		public bool PreviewFile { get; set; }
		public bool Disabled { get; set; }

		public EmailEvent() { }

		/// <summary>
		/// get email message file name
		/// </summary>
		public string GetFileName(Netpay.Infrastructure.Domain domain, EmailType emailType, Language mailLanguage)
		{
			return domain.EmailTemplatePath + @"\" + emailType.ToString() + (mailLanguage == Language.Hebrew ? "_he" : string.Empty) + ".htm";
		}

		private void SaveEmailFile(string filePath, string message)
		{
			TextWriter txw = new StreamWriter(filePath);
			txw.Write(message);
			txw.Close();
			txw.Dispose();
		}

		public class EmailDataObject
		{
			public Bll.Customers.Customer Customer { get; set; }
			public Bll.Merchants.Merchant Merchant { get; set; }
			public Netpay.Infrastructure.Domain Domain { get; set; }
			public EmailDataObject(Infrastructure.Domain domain, int? merchantId, int? customerId)
			{
				Domain = domain;
				if (merchantId != null) {
					Merchant = Bll.Merchants.Merchant.Load(merchantId.GetValueOrDefault());
					if (Merchant == null) throw new ApplicationException("Could not load merchant #" + merchantId);
				} else if (customerId != null) {
					Customer = Bll.Customers.Customer.Load(customerId.GetValueOrDefault());
					if (Customer == null) throw new ApplicationException("Could not load customer #" + customerId);
				}
			}
		}

		public string getEmailText(ProcessEngine processEngine, string domain, int? merchantId, int? customerId, EmailType emailType, Language? mailLanguage) 
        {
			var d = Netpay.Infrastructure.Domain.Get(domain);
			var objData = new EmailDataObject(Infrastructure.Domain.Get(domain), merchantId, customerId);
			if (mailLanguage == null) mailLanguage = Language.English;
			string subject, body = FormatMesage.GetMessageText(GetFileName(d, emailType, mailLanguage.GetValueOrDefault(0)), out subject);
            System.Globalization.CultureInfo cultureInfo = mailLanguage == Language.Hebrew ? System.Globalization.CultureInfo.GetCultureInfo("he-il") : System.Globalization.CultureInfo.GetCultureInfo("en-us");
			System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Netpay.Process.Resources.Enums", typeof(ProcessEngine).Assembly);
			return FormatMesage.FormatMessage(body, objData, rm, cultureInfo);
        }

		public string FormatCart(ProcessEngine processEngine, TransactionContext t, PendingEvent pendingData)
		{
			var cart = Bll.Shop.Cart.Basket.LoadForTransaction(Infrastructure.TransactionStatus.Captured, t.ID);
			var sb = new System.Text.StringBuilder();
			sb.Append("<table>");
			foreach (var item in cart.BasketItems) {
				var propValues = item.ItemProperties.Select(i => string.Format("{0}:{1}", i.Name, i.Value));
				sb.AppendFormat("<tr><td>{0}{1}</td><td>{2}</td><td>{3}</td></tr>", item.Name, string.Join(", ", propValues), item.Quantity, item.Total);
			}
			sb.AppendFormat("<tr><td colspan=\"2\">Total</td><td>{0}</td></td>", cart.Total);
			sb.Append("</table>");
			return null;
		}

		/// <summary>
		/// interface IPostProcess method implementation
		/// </summary>
		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext t, PendingEvent pendingData)
		{
			System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
			//TransactionHistoryType eventType = 0;
			string subject = null, body = null, toEmail = null;
			string fromEmail = Infrastructure.Domain.Current.MailAddressFrom;
            string fromName = Infrastructure.Domain.Current.BrandName;
            System.Collections.Specialized.NameValueCollection pendingParams = System.Web.HttpUtility.ParseQueryString(pendingData.Parameters == null ? "" : pendingData.Parameters);
            Language language = Language.English;
            var objData = new EmailDataObject(Infrastructure.Domain.Current, pendingData.MerchantID, pendingData.CustomerID);
			//var objData = new EmailDataObject(Infrastructure.Security.SecurityManager.ServiceCredentials(domain.Host), pendingData.MerchantID, pendingData.CustomerID);
			try
			{
				switch (pendingData.EventType)
				{
					case PendingEventType.WalletRegisterEmail:
						//Netpay.Bll.Customers.SendRegistrationEmail(objData.Domain.Host, objData.Customer);
						//return PostProcessResult.Succseeded;
						//eventType = TransactionHistoryType.InfoEmailSendClient;
						message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
						toEmail = objData.Customer.EmailAddress.Replace(';', ',').Replace(" ", "");
                        body = FormatMesage.GetMessageText(GetFileName(Infrastructure.Domain.Current, EmailType.Wallet_Register, language), out subject);
						break;
                    default:
						throw new Exception("EmailEvent does not support event " + pendingData.EventType.ToString());
				}
                if (!string.IsNullOrEmpty(pendingParams["Email"])) toEmail = pendingParams["Email"];
                try { message.To.Add(toEmail); }
                catch (Exception) { return PostProcessResult.FailedNoRetry; }
                message.IsBodyHtml = true;
				if (string.IsNullOrEmpty(subject)) subject = "Registration";
				System.Globalization.CultureInfo cultureInfo = language == Language.Hebrew ? System.Globalization.CultureInfo.GetCultureInfo("he-il") : System.Globalization.CultureInfo.GetCultureInfo("en-us");
				System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Netpay.Process.Resources.Enums", typeof(ProcessEngine).Assembly);
				message.Subject = FormatMesage.FormatMessage(subject, objData, rm, cultureInfo);
				message.Body = FormatMesage.FormatMessage(body, objData, rm, cultureInfo);
				if (Bcc != null) message.Bcc.Add(Bcc.Replace(';', ',').Replace(" ", ""));
				System.Net.Mail.SmtpClient smtpClient = SmtpClient.GetClient();
				if (PreviewFile) SaveEmailFile(AppDomain.CurrentDomain.BaseDirectory + pendingData.EventType.ToString() + ".htm", message.Body);
				smtpClient.Send(message);
				//processEngine.AddLog(transaction, 0, eventType, "Sent To: " + message.To.ToString(), true);
			}
			catch (System.Net.Mail.SmtpFailedRecipientException) 
			{
				//processEngine.AddLog(transaction, 0, eventType, ex.Message, false);
				return PostProcessResult.FailedNoRetry;
			} 
			catch (System.Net.Mail.SmtpException) 
			{
				//processEngine.AddLog(transaction, 0, eventType, ex.Message, false);
				return PostProcessResult.Failed;
			}
			catch (Exception ex)
			{
				//processEngine.AddLog(transaction, 0, eventType, ex.Message, false);
				throw new Exception(pendingData.EventType.ToString() + ":" + ex.Message, ex);
			}
			return PostProcessResult.Succseeded;
		}
	}
}
