﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{
	public class NetpayLogger : ILogger
	{
		public void LogError(CommonTypes.TransactionStage stage, Exception ex)
		{
			Netpay.Infrastructure.Logger.Log(stage == CommonTypes.TransactionStage.PostProcess ? Netpay.Infrastructure.LogTag.Process : Netpay.Infrastructure.LogTag.Process, ex);
		}

		public void LogTransaction(bool preStage, TransactionContext trans, Exception ex)
		{
			if (ex != null) 
				Netpay.Infrastructure.Logger.Log(trans.Stage == CommonTypes.TransactionStage.PostProcess ? Netpay.Infrastructure.LogTag.Process : Netpay.Infrastructure.LogTag.Process, ex);
		}
	}
}
