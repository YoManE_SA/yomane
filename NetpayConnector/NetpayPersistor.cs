﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Process;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using System.Net;
using Netpay.CommonTypes;
using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{
	public class NetpayPersistor : IPersistor
	{
		private struct QueueItem
		{
			public TransactionContext transactionContext;
			public SaveOptions options;
			public QueueItem(TransactionContext transaction, SaveOptions options)
			{
				this.transactionContext = transaction;
				this.options = options;
			}
		}

		private DataQueue<QueueItem> saveQueue = null;

		public bool SaveAsync { get; set; }
		~NetpayPersistor()
		{
			if (saveQueue is IDisposable) (saveQueue as IDisposable).Dispose();
		}

		public static void MapProcessTypeToNetpayType(ProcessTransactionType transactionType, out TransactionType returnTransactionType, out CreditType returnCreditType)
		{
			switch (transactionType)
			{
				case ProcessTransactionType.Authorize:
					returnTransactionType = TransactionType.Authorization;
					returnCreditType = CreditType.Regular;
					break;
				case ProcessTransactionType.Capture:
					returnTransactionType = TransactionType.AuthorizationCapture;
					returnCreditType = CreditType.Regular;
					break;
				case ProcessTransactionType.Refund:
					returnTransactionType = TransactionType.Capture;
					returnCreditType = CreditType.Refund;
					break;
				case ProcessTransactionType.Sale:
					returnTransactionType = TransactionType.Capture;
					returnCreditType = CreditType.Regular;
					break;
				default:
					returnTransactionType = TransactionType.Unknown;
					returnCreditType = CreditType.Unknown;
					break;
			}
		}

		public static Infrastructure.TransactionStatus MapProcessStatusToNetpayStatus(CommonTypes.ProcessTransactionStatus status, ProcessTransactionType transactionType)
		{
			if (status == ProcessTransactionStatus.Approved)
			{
				switch (transactionType)
				{
					case ProcessTransactionType.Authorize:
					case ProcessTransactionType.Reverse:
						return TransactionStatus.Authorized;
					case ProcessTransactionType.Capture:
					case ProcessTransactionType.Credit:
					case ProcessTransactionType.Refund:
					case ProcessTransactionType.Sale:
						return TransactionStatus.Captured;
				}
			}
			else if (status == ProcessTransactionStatus.Declined) return TransactionStatus.Declined;
			else if (status == ProcessTransactionStatus.Pending) return TransactionStatus.Pending;
			return TransactionStatus.Pending;
		}

		private TransactionContext createContext(string domainName)
		{
			TransactionContext transaction = new TransactionContext(domainName, new TransactionRequest());
			transaction.Response = new TransactionResponse();
			transaction.Request.customer = new CustomerData();
			transaction.Config = new ProcessConfig();
			transaction.Method = new DetectMethodData();
			return transaction;
		}

		private TransactionContext InternalLoad(string domian, tblCompanyTransApproval entityApproval)
		{
			var transaction = createContext(domian);
			transaction.ID = entityApproval.ID;
			transaction.MethodText = entityApproval.PaymentMethodDisplay;
			transaction.CurrencyID = entityApproval.Currency.GetValueOrDefault();
			transaction.Config.MerchantID = entityApproval.CompanyID.GetValueOrDefault();
			transaction.Config.DebitProcessorId = entityApproval.DebitCompanyID.GetValueOrDefault(1);
			transaction.Config.TerminalNumber = entityApproval.TerminalNumber;
			transaction.Method.paymentMethod = (PaymentMethodEnum)entityApproval.PaymentMethod;
			transaction.Response.ApprovalNumber = entityApproval.approvalNumber;
			transaction.Response.Status = ProcessTransactionStatus.Approved;
			transaction.Response.TimeStamp = entityApproval.InsertDate;
			transaction.Response.ID = transaction.ID;
			//transaction.Request.type = (entityApproval.CreditType == (int)CreditType.Refund ? ProcessTransactionType.Refund : ProcessTransactionType.Authorize);
			transaction.Request.type = ProcessTransactionType.Authorize;
			transaction.Request.amount = entityApproval.Amount;
			transaction.Request.comment = entityApproval.Comment;
			transaction.Request.clientIP = entityApproval.IPAddress;
			transaction.Request.paymentMethod = (PaymentMethodEnum)entityApproval.PaymentMethod.GetValueOrDefault();
			transaction.Request.merchantRefCode = entityApproval.OrderNumber;
			transaction.Request.debitorRefCode = entityApproval.DebitReferenceCode;
			transaction.Request.installmets = entityApproval.Payments;
			transaction.Request.productID = entityApproval.MerchantProduct_id;
			//loadCreditCardID = entityApproval.PaymentMethodID;
			return transaction;
		}

		private TransactionContext InternalLoad(string domian, tblCompanyTransPass entityPass)
		{
			var transaction = createContext(domian);
			IPAddress parsedAddress;
			transaction.ID = entityPass.ID;
			transaction.MethodText = entityPass.PaymentMethodDisplay;
			transaction.CurrencyID = entityPass.Currency.GetValueOrDefault();
			transaction.Config.MerchantID = entityPass.companyID;
			transaction.Config.DebitProcessorId = entityPass.DebitCompanyID.GetValueOrDefault(1);
			transaction.Config.TerminalNumber = entityPass.TerminalNumber;
			//transaction.Config.TerminalID = transaction.ter;
			transaction.Method.paymentMethod = (PaymentMethodEnum)entityPass.PaymentMethod.GetValueOrDefault();
			transaction.Response.ApprovalNumber = entityPass.ApprovalNumber;
			transaction.Response.TimeStamp = entityPass.InsertDate;
			transaction.Response.ID = transaction.ID;
			transaction.Response.Status = ProcessTransactionStatus.Approved;
			transaction.Request.type = (entityPass.CreditType == (int)CreditType.Refund ? ProcessTransactionType.Refund : ProcessTransactionType.Capture);
			transaction.Request.amount = entityPass.Amount;
			transaction.Request.comment = entityPass.Comment;
			transaction.Request.clientIP = entityPass.IPAddress;
			transaction.Request.paymentMethod = (PaymentMethodEnum)entityPass.PaymentMethod.GetValueOrDefault();
			transaction.Request.debitorRefCode = entityPass.DebitReferenceCode;
			transaction.Request.merchantRefCode = entityPass.OrderNumber;
			transaction.Request.installmets = entityPass.Payments;
			transaction.Request.refTransID = entityPass.OriginalTransID;
			transaction.Request.productID = entityPass.MerchantProduct_id;
			if (System.Net.IPAddress.TryParse(entityPass.IPAddress, out parsedAddress))
				transaction.IPAddress = parsedAddress;
			return transaction;
		}

		private TransactionContext InternalLoad(string domian, tblCompanyTransFail entityFail)
		{
			var transaction = createContext(domian);
			IPAddress parsedAddress;
			transaction.ID = entityFail.ID;
			transaction.MethodText = entityFail.PaymentMethodDisplay;
			transaction.CurrencyID = entityFail.Currency.GetValueOrDefault();
			transaction.Config.MerchantID = entityFail.CompanyID.GetValueOrDefault();
			transaction.Config.DebitProcessorId = entityFail.DebitCompanyID.GetValueOrDefault(1);
			transaction.Config.TerminalNumber = entityFail.TerminalNumber;
			transaction.Method.paymentMethod = (PaymentMethodEnum)entityFail.PaymentMethod.GetValueOrDefault();
			transaction.Response.ErrorCode = entityFail.replyCode;
			transaction.Response.TimeStamp = entityFail.InsertDate;
			transaction.Response.ID = transaction.ID;
			transaction.Response.Status = ProcessTransactionStatus.Declined;
			transaction.Request.type = entityFail.TransType == 1 ? ProcessTransactionType.Authorize : ProcessTransactionType.Capture;
			transaction.Request.amount = entityFail.Amount;
			transaction.Request.comment = entityFail.Comment;
			transaction.Request.clientIP = entityFail.IPAddress;
			transaction.Request.paymentMethod = (PaymentMethodEnum)entityFail.PaymentMethod.GetValueOrDefault();
			entityFail.payerIdUsed = transaction.Request.PayerIDUsed.EmptyIfNull();
			transaction.Request.debitorRefCode = entityFail.DebitReferenceCode;
			transaction.Request.installmets = entityFail.Payments;
			transaction.Request.productID = entityFail.MerchantProduct_id;
			if (System.Net.IPAddress.TryParse(entityFail.IPAddress, out parsedAddress))
				transaction.IPAddress = parsedAddress;
			transaction.Request.merchantRefCode = entityFail.OrderNumber;
			return transaction;
		}

		private TransactionContext InternalLoad(string domian, tblCompanyTransPending entityPending)
		{
			var transaction = createContext(domian);
			transaction.ID = entityPending.ID;
			transaction.MethodText = entityPending.PaymentMethodDisplay;
			transaction.CurrencyID = entityPending.Currency.GetValueOrDefault();
			transaction.Config.MerchantID = entityPending.company_id.GetValueOrDefault();
			transaction.Config.DebitProcessorId = entityPending.DebitCompanyID.GetValueOrDefault(1);
			transaction.Config.TerminalNumber = entityPending.TerminalNumber;
			transaction.Method.paymentMethod = (PaymentMethodEnum)entityPending.PaymentMethod.GetValueOrDefault();
			transaction.Response.ErrorCode = entityPending.replyCode;
			transaction.Response.Status = ProcessTransactionStatus.Pending;
			transaction.Response.TimeStamp = entityPending.insertDate;
			transaction.Response.ID = transaction.ID;
			transaction.Request.type = entityPending.trans_type == 1 ? ProcessTransactionType.Authorize : ProcessTransactionType.Capture;
			transaction.Request.amount = entityPending.trans_amount;
			transaction.Request.comment = entityPending.Comment;
			transaction.Request.clientIP = entityPending.IPAddress;
			transaction.Request.paymentMethod = (PaymentMethodEnum)entityPending.PaymentMethod.GetValueOrDefault();
			transaction.Request.debitorRefCode = entityPending.DebitReferenceCode;
			transaction.Request.merchantRefCode = entityPending.OrderNumber;
			transaction.Request.installmets = entityPending.trans_payments;
			transaction.Request.productID = entityPending.MerchantProduct_id;
			return transaction;
		}

		private TransactionContext ContextCompleteLoad(NetpayDataContext dc, TransactionContext transaction, bool loadTerminal, System.Collections.Generic.Dictionary<string, object> loadCach)
		{
			Domain domain = Domain.Get(transaction.Domain);
			transaction.Config.GatewayBrandName = domain.BrandName;
			transaction.Config.GatewayLegalName = domain.LegalName;
			transaction.Config.GatewayLogoPath = domain.LogoPath;
			transaction.Config.GatewaySupportWebSite = domain.ContentUrl;
			transaction.Config.GatewaySupportEmail = domain.MailAddressFrom;
			transaction.Config.GatewaySupportFax = domain.CustomerServiceFax;
			transaction.Config.GatewayCompliancePhone = domain.CustomerServicePhone;
			transaction.Config.GatewayComplianceUSPhone = domain.CustomerServiceUSPhone;
			transaction.Config.GatewayComplianceEmail = domain.MailAddressFromChb;
			transaction.Config.GatewayUIServicesUrl = domain.WebServicesUrl.Trim(new char[] { '/', '\\' });

			transaction.Request.currencyISO = Bll.Currency.Get((int)transaction.CurrencyID).IsoCode;
			tblCompany merchant = null;
			string merchantKey = "M" + transaction.Config.MerchantID;
			if (loadCach != null && loadCach.ContainsKey(merchantKey)) merchant = loadCach[merchantKey] as tblCompany;
			if (merchant == null) {
				merchant = (from m in dc.tblCompanies where m.ID == transaction.Config.MerchantID select m).SingleOrDefault();
				if (loadCach != null) loadCach.Add(merchantKey, merchant);
			}
			if (merchant != null)
			{
				transaction.Config.MerchantID = merchant.ID;
				transaction.Config.MerchantName = merchant.CompanyLegalName;
				transaction.Config.MerchantSupportEmail = merchant.merchantSupportEmail;
				transaction.Config.MerchantSupportPhone = merchant.merchantSupportPhoneNum;
				transaction.Config.MerchantNotifyEmail = merchant.PassNotifyEmail;
				transaction.Config.MerchantNotifyEmailChb = merchant.ChargebackNotifyMail;
				transaction.Config.MerchantWebsiteURL = merchant.URL.Replace(" ", ", ");
				transaction.Config.MerchantFeeCurrency = (Currency?)merchant.CFF_Currency;
				transaction.Config.MerchantLanguage = (merchant.languagePreference == "heb" ? Language.Hebrew : Language.English);
				if (loadTerminal) transaction.Config.TransactionPayDate = Bll.Merchants.Merchant.GetTransactionSettlementDate(merchant.ID, transaction.Response.TimeStamp);
				transaction.Config.Descriptor = merchant.descriptor;
				if (merchant.merchantDepartment != null && merchant.MerchantDepartment_id == 3) //for orange
					transaction.Config.MerchantDepartment = merchant.merchantDepartment.Name;
				if (merchant.ParentCompany.HasValue && Bll.DataManager.ParentCompany.Get(merchant.ParentCompany.Value) != null)
					transaction.Config.MerchantParentCompanyName = Bll.DataManager.ParentCompany.Get(merchant.ParentCompany.Value).Code;
			}
			/*
			if (transaction.Request.productID != null) {
				var prd = Bll.Shop.Products.Product.Load(transaction.Request.productID.GetValueOrDefault());
				if (prd != null) {
					transaction.Response.ProductPostText = prd.GetTextForLanguage(null).ReceiptText;
					transaction.Response.ProductPostLink = prd.GetTextForLanguage(null).ReceiptLink;
				}
			}
			*/
			if (loadTerminal) NetpayAccountManager.GetTerminalInfo(transaction, false);
			return transaction;
		}
		/*
		public List<TransactionContext> Load(string domian, Bll.SettlementSettings.SettingsFilter filters, DateTime? dateFrom, DateTime? dateTo, ProcessTransactionStatus status, ProcessTransactionType transactionType)
		{
			Domain domain = Domain.Get(domian);
			TransactionContext transaction = createContext(domian);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			int[] debitCompanyId = null, merchantId = null;
			if (filters != null)
			{
				switch (filters.SettlementType_id){
					case Bll.SettlementType.Merchant:
						if (filters.Merchant_id.HasValue) merchantId = new int[] { filters.Merchant_id.Value };
						break;
					case Bll.SettlementType.DebitCompany:
						if (filters.DebitCompany_id.HasValue) debitCompanyId = new int[] { filters.DebitCompany_id.Value };
						break;
					case Bll.SettlementType.DebitCompanyAffiliate:
						debitCompanyId = (from da in dc.SetAffiliateDebitCompanies where da.Affiliate_id == filters.Affiliate_id.Value select da.DebitCompany_id).ToArray();
						break;
					case Bll.SettlementType.MerchantAffiliate:
						merchantId = (from da in dc.SetMerchantAffiliates where da.Affiliate_id == filters.Affiliate_id.Value select da.Merchant_id).ToArray();
						break;
				}
			}
			System.Collections.Generic.Dictionary<string, object> loadCach = new Dictionary<string,object>();
			if (debitCompanyId == null) debitCompanyId = new int[] { };
			if (merchantId == null) merchantId = new int[] { };
			switch (status)
			{
				case ProcessTransactionStatus.Approved:
					if (transactionType == ProcessTransactionType.Authorize)
						return (from t in dc.tblCompanyTransApprovals where
									(merchantId.Contains(t.CompanyID.GetValueOrDefault()) || (merchantId.Length == 0)) &&
									(debitCompanyId.Contains(t.DebitCompanyID.GetValueOrDefault()) || (debitCompanyId.Length == 0)) &&
									(dateFrom == null || t.InsertDate >= dateFrom) && (dateTo == null || t.InsertDate <= dateTo)
									orderby t.InsertDate ascending
									select ContextCompleteLoad(dc, InternalLoad(domian, t), false, loadCach)).ToList();
					else
						return (from t in dc.tblCompanyTransPasses where
									(merchantId.Contains(t.companyID) || (merchantId.Length == 0)) &&
									(debitCompanyId.Contains(t.DebitCompanyID.GetValueOrDefault()) || (debitCompanyId.Length == 0)) &&
									(dateFrom == null || t.InsertDate >= dateFrom) && (dateTo == null || t.InsertDate <= dateTo)
									orderby t.InsertDate ascending
									select ContextCompleteLoad(dc, InternalLoad(domian, t), false, loadCach)).ToList();
				case ProcessTransactionStatus.Declined:
					return (from t in dc.tblCompanyTransFails where
								(merchantId.Contains(t.CompanyID.GetValueOrDefault()) || (merchantId.Length == 0)) &&
								(debitCompanyId.Contains(t.DebitCompanyID.GetValueOrDefault()) || (debitCompanyId.Length == 0)) &&
								(dateFrom == null || t.InsertDate >= dateFrom) && (dateTo == null || t.InsertDate <= dateTo)
								orderby t.InsertDate ascending
								select ContextCompleteLoad(dc, InternalLoad(domian, t), false, loadCach)).ToList();
				/*
				case ProcessTransactionStatus.Pending:
					return (from t in dc.tblCompanyTransPendings
						where t.CompanyID == merchantID && (t.PayID == settlmentId || (t.PayID == null && settlmentId == null))
						select ContextCompleteLoad(dc, InternalLoad(domian, t), true, true)).ToList();
				*
			}
			return new List<TransactionContext>();
		}
		*/
		/// <summary>
		/// Begin Load Section
		/// </summary>
		/// <param name="transLoadInfo"></param>
		/// <returns></returns>
		public TransactionContext Load(TransLoadInfo transLoadInfo)
		{
			int? paymentMethodId, payerId;
			TransactionContext transaction = null;
			Domain domain = Domain.Get(transLoadInfo.domain);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			switch (transLoadInfo.status)
			{
				case ProcessTransactionStatus.Approved:
					if (transLoadInfo.type == ProcessTransactionType.Authorize)
					{
						tblCompanyTransApproval entityApproval = (from t in dc.tblCompanyTransApprovals where t.ID == transLoadInfo.id select t).SingleOrDefault();
						if (entityApproval == null) throw new ApplicationException("Transaction not found");
						transaction = InternalLoad(transLoadInfo.domain, entityApproval);
						paymentMethodId = entityApproval.TransPaymentMethod_id;
						payerId = entityApproval.TransPayerInfo_id;
					}
					else
					{
						tblCompanyTransPass entityPass = (from t in dc.tblCompanyTransPasses where t.ID == transLoadInfo.id select t).SingleOrDefault();
						if (entityPass == null) throw new ApplicationException("Transaction not found");
						transaction = InternalLoad(transLoadInfo.domain, entityPass);
						paymentMethodId = entityPass.TransPaymentMethod_id;
						payerId = entityPass.TransPayerInfo_id;
						if (entityPass.DeniedStatus != 0) //detect charge back
						{
							transaction.Response.chargeback = new ChargebackInfo();
							var rrInfo = (from h in dc.TransHistories where h.TransPass_id == entityPass.ID && h.TransHistoryType_id == (int)TransactionHistoryType.RetrievalRequest select h).FirstOrDefault();
							if (rrInfo != null)
							{
								transaction.Response.chargeback.reasonCode = rrInfo.ReferenceNumber;
								transaction.Response.chargeback.retrivalDate = rrInfo.InsertDate;
								transaction.Response.chargeback.description = (from rc in dc.tblChargebackReasons where rc.ReasonCode == rrInfo.ReferenceNumber select rc.Description + "\r\n" + rc.RefundInfo).SingleOrDefault();
							}
						}
					}
					transaction.Response.ErrorCode = "000";
					break;
				case ProcessTransactionStatus.Declined:
					tblCompanyTransFail entityFail = (from t in dc.tblCompanyTransFails where t.ID == transLoadInfo.id select t).SingleOrDefault();
					if (entityFail == null) throw new ApplicationException("Transaction not found");
					transaction = InternalLoad(transLoadInfo.domain, entityFail);
					paymentMethodId = entityFail.TransPaymentMethod_id;
					payerId = entityFail.TransPayerInfo_id;
					if (!string.IsNullOrEmpty(transaction.Response.ErrorCode) && transaction.Response.Reason == null)
						transaction.Response.Reason = (from c in dc.tblDebitCompanyCodes where c.DebitCompanyID == transaction.Config.DebitProcessorId && c.Code == transaction.Response.ErrorCode select c.DescriptionMerchantEng).SingleOrDefault();
					break;
				case ProcessTransactionStatus.Pending:
					tblCompanyTransPending entityPending = (from t in dc.tblCompanyTransPendings where t.ID == transLoadInfo.id select t).SingleOrDefault();
					if (entityPending == null) throw new ApplicationException("Transaction not found");
					transaction = InternalLoad(transLoadInfo.domain, entityPending);
					paymentMethodId = entityPending.TransPaymentMethod_id;
					payerId = entityPending.TransPayerInfo_id;
					break;
				default:
					throw new NotImplementedException();
			}
			if (paymentMethodId.HasValue) LoadPaymentMethod(dc, paymentMethodId.Value, transaction);
			if (payerId.HasValue) LoadPayerInfo(dc, payerId.Value, transaction);
			return ContextCompleteLoad(dc, transaction, true, null);
		}

		private bool LoadPayerInfo(NetpayDataContext dc, int id, TransactionContext transaction)
		{
			var entityAddr = (from t in dc.TransPayerInfos where t.TransPayerInfo_id == id select t).SingleOrDefault();
			if (entityAddr == null)
				throw new ApplicationException("Payer not found");
			//transaction.Request.creditCard.OwnerName = entityCard.Member;
			//transaction.Request.creditCard.PersonalID = entityCard.PersonalNumber;
			transaction.Request.customer.firstName = entityAddr.FirstName;
			transaction.Request.customer.lastName = entityAddr.LastName;
			transaction.Request.customer.email = entityAddr.EmailAddress;
			transaction.Request.customer.personalID = entityAddr.PersonalNumber;
			transaction.Request.customer.phone = entityAddr.PhoneNumber;
			return true;
		}

		private bool LoadAddress(NetpayDataContext dc, int id, Address address)
		{
			var entityAddr = (from t in dc.TransPaymentBillingAddresses where t.TransPaymentBillingAddress_id == id select t).SingleOrDefault();
			if (entityAddr == null)
				throw new ApplicationException("Address not found");
			address.Address1 = entityAddr.Street1;
			address.Address2 = entityAddr.Street2;
			address.City = entityAddr.City;
			address.StateISO = entityAddr.StateISOCode;
			address.CountryISO = entityAddr.CountryISOCode;
			address.Postal = entityAddr.PostalCode;
			return true;
		}

		private bool LoadPaymentMethod(NetpayDataContext dc, int id, TransactionContext transaction)
		{
			var paymentMethod = (from t in dc.TransPaymentMethods where t.TransPaymentMethod_id == id select t).SingleOrDefault();
			if (paymentMethod == null)
				throw new ApplicationException("Transaction not found");
			transaction.Request.creditCard = new CreditCardData();
			if (paymentMethod.ExpirationDate != null) {
				transaction.Request.creditCard.ExpMonth = paymentMethod.ExpirationDate.Value.Month;
				transaction.Request.creditCard.ExpYear = paymentMethod.ExpirationDate.Value.Year;
			}
			if (paymentMethod.Value2Encrypted != null) transaction.Request.creditCard.Cvv = Netpay.Infrastructure.Security.Encryption.DecodeCvv(Netpay.Infrastructure.Security.Encryption.Decrypt(transaction.Domain, paymentMethod.Value2Encrypted.ToArray()));
			if (paymentMethod.Value1Encrypted != null) transaction.Request.creditCard.Pan = Netpay.Infrastructure.Security.Encryption.Decrypt(transaction.Domain, paymentMethod.Value1Encrypted.ToArray());
			transaction.Method.binValue = paymentMethod.Value1First6Text.ToString();
			transaction.Method.binCountry = paymentMethod.IssuerCountryIsoCode;
			if (paymentMethod.TransPaymentBillingAddress_id != null)
			{
				transaction.Request.billingAddress = new Address();
				LoadAddress(dc, paymentMethod.TransPaymentBillingAddress_id.Value, transaction.Request.billingAddress);
			}
			return true;
		}

		/// <summary>
		/// Begin Save Section	
		/// </summary>
		/// <param name="dc"></param>
		/// <param name="transaction"></param>
		private void SaveAddress(Domain domain, NetpayDataContext dc, SaveOptions options, Address address)
		{
			dc.ExecuteCommand("SET IDENTITY_INSERT tblBillingAddress ON");
			TransPaymentBillingAddress entityAddr = null;
			if ((options & SaveOptions.UpdateExisting) != 0)
			{
				entityAddr = (from t in dc.TransPaymentBillingAddresses where t.TransPaymentBillingAddress_id == address.ID select t).SingleOrDefault();
				if (entityAddr == null) throw new ApplicationException("Transaction not found");
			}
			else
			{
				entityAddr = new TransPaymentBillingAddress();
				if ((options & SaveOptions.GenIds) == 0) entityAddr.TransPaymentBillingAddress_id = address.ID;
				dc.TransPaymentBillingAddresses.InsertOnSubmit(entityAddr);
			}
			//entityAddr.insertDate = DateTime.Now;
			entityAddr.Street1 = address.Address1;
			entityAddr.Street2 = address.Address2;
			entityAddr.City = address.City;
			entityAddr.StateISOCode = address.StateISO;
			entityAddr.CountryISOCode = address.CountryISO;
			entityAddr.PostalCode = address.Postal;
			if (entityAddr.PostalCode == null) entityAddr.PostalCode = string.Empty;
			if (entityAddr.Street2 == null) entityAddr.Street2 = string.Empty;
			entityAddr.CountryISOCode = address.CountryISO;
			entityAddr.StateISOCode = address.StateISO;

			dc.SubmitChanges();
			//if ((options & SaveOptions.GenIds) != 0) 
			address.ID = entityAddr.TransPaymentBillingAddress_id;
			dc.ExecuteCommand("SET IDENTITY_INSERT tblBillingAddress OFF");
		}

		private int SavePaymentMethod(NetpayDataContext dc, SaveOptions options, TransactionContext transaction)
		{
			TransPaymentMethod entityMethod = null;
			if ((options & SaveOptions.UpdateExisting) != 0)
			{
				entityMethod = (from t in dc.TransPaymentMethods where t.TransPaymentMethod_id == transaction.Request.creditCard.ID select t).SingleOrDefault();
				if (entityMethod == null) throw new ApplicationException("Transaction not found");
			}
			else
			{
				entityMethod = new TransPaymentMethod();
				dc.TransPaymentMethods.InsertOnSubmit(entityMethod);
				if ((options & SaveOptions.GenIds) == 0) entityMethod.TransPaymentMethod_id = transaction.Request.creditCard.ID;
			}
			entityMethod.PaymentMethod_id = (short)transaction.Request.paymentMethod;
			entityMethod.ExpirationDate = new DateTime(transaction.Request.creditCard.ExpYear, transaction.Request.creditCard.ExpMonth, 1).AddMonths(1).AddDays(-1);
			entityMethod.Value2Encrypted = Netpay.Infrastructure.Security.Encryption.Encrypt(Netpay.Infrastructure.Security.Encryption.EncodeCvv(transaction.Request.creditCard.Cvv));
			entityMethod.Value1First6Text = transaction.Request.creditCard.Pan.Replace(" ", "").Substring(0, 6);
			entityMethod.Value1Last4Text = transaction.Request.creditCard.Pan.Replace(" ", "").Substring(transaction.Request.creditCard.Pan.Length - 4);
			entityMethod.Value1Encrypted = Netpay.Infrastructure.Security.Encryption.Encrypt(transaction.Request.creditCard.Pan);
			entityMethod.IssuerCountryIsoCode = transaction.Method.binCountry;
			if (transaction.Request.billingAddress != null)
				entityMethod.TransPaymentBillingAddress_id = transaction.Request.billingAddress.ID;
			dc.SubmitChanges();
			if ((options & SaveOptions.GenIds) != 0) transaction.Request.creditCard.ID = entityMethod.TransPaymentMethod_id;
			return entityMethod.TransPaymentMethod_id;
		}

		private void SaveTracking(NetpayDataContext dc, TransactionContext transaction)
		{
			/*
			ProcessApproved ctt = new ProcessApproved();
			ctt.Merchant_id = transaction.Config.MerchantID;
			ctt.TransDate = DateTime.Now;
			ctt.PaymentMethod_id = (short)transaction.Request.paymentMethod;
			ctt.TransCurrency = transaction.Request.currencyISO.ToInt32();
			ctt.TransAmount = transaction.Request.amount;
			ctt.TransCreditType_id = (byte)transaction.Request.type;
			ctt.TransInstallments = (short)transaction.Request.installmets;
			ctt.TransIpAddress = transaction.IPAddress.ToString();
			ctt.CreditCardNumber = Netpay.Infrastructure.Security.Encryption.Encrypt(transaction.Domain, transaction.Request.creditCard.Pan);
			*/
		}

		private void Save(QueueItem item)
		{
			InternalSave(item.transactionContext, item.options);
		}

		public void Save(TransactionContext transaction, SaveOptions options)
		{
			if (SaveAsync)
			{
				if (saveQueue == null)
				{
					saveQueue = new DataQueue<QueueItem>(Save);
					saveQueue.Start();
				}
				saveQueue.AddItem(new QueueItem(transaction, options));
			}
			else InternalSave(transaction, options);
		}

		public void InternalSave(TransactionContext transaction, SaveOptions options)
		{
			//if (transaction.MerchantID == null) throw new ArgumentException("MerchantID cannot be null.");
			//if (transaction.IPCountry == null) transaction.IPCountry = "--";
			TransactionType netpayTransactionType; CreditType netpayCreditType;
			MapProcessTypeToNetpayType(transaction.Request.type, out netpayTransactionType, out netpayCreditType);

			Domain domain = Domain.Get(transaction.Domain);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString); /*domain*/
			dc.Log = new Netpay.Dal.DataAccess.DebuggerWriter();
			int paymetMethodID = 0;
			if (transaction.Request.billingAddress != null) SaveAddress(domain, dc, options, transaction.Request.billingAddress);
			if (transaction.Request.creditCard != null)
				paymetMethodID = SavePaymentMethod(dc, options, transaction);
			int currencyId = Bll.Currency.Get(transaction.Request.currencyISO).ID;
			switch (transaction.Response.Status)
			{
				case ProcessTransactionStatus.Approved:
					if (transaction.Request.type == ProcessTransactionType.Authorize)
					{
						tblCompanyTransApproval entityApproval = null;
						if ((options & SaveOptions.UpdateExisting) != 0)
						{
							entityApproval = (from t in dc.tblCompanyTransApprovals where t.ID == transaction.ID select t).SingleOrDefault();
							if (entityApproval == null) throw new ApplicationException("Transaction not found");
						}
						else
						{
							entityApproval = new tblCompanyTransApproval();
							dc.tblCompanyTransApprovals.InsertOnSubmit(entityApproval);
							if ((options & SaveOptions.GenIds) == 0) entityApproval.ID = transaction.ID;
						}
						entityApproval.referringUrl = "";
						entityApproval.Amount = transaction.Request.amount;
						entityApproval.approvalNumber = transaction.Response.ApprovalNumber.EmptyIfNull();
						entityApproval.Comment = transaction.Request.comment.EmptyIfNull();
						entityApproval.CompanyID = transaction.Config.MerchantID;
						entityApproval.Currency = currencyId;
						entityApproval.CreditType = (byte)netpayCreditType;
						entityApproval.DebitCompanyID = transaction.Config.DebitProcessorId;
						entityApproval.Payments = transaction.Request.installmets;
						entityApproval.InsertDate = transaction.Response.TimeStamp;
						entityApproval.IPAddress = transaction.Request.clientIP.EmptyIfNull();
						entityApproval.PaymentMethod = (short)transaction.Request.paymentMethod;
						entityApproval.PaymentMethodID = paymetMethodID;
						entityApproval.PaymentMethodDisplay = transaction.MethodText.EmptyIfNull();
						entityApproval.replyCode = "000";
						entityApproval.OrderNumber = transaction.Request.merchantRefCode.EmptyIfNull();
						entityApproval.TerminalNumber = transaction.Config.TerminalNumber;
						entityApproval.DebitReferenceCode = transaction.Request.debitorRefCode.EmptyIfNull();
						//entityApproval.isTestOnly = transaction.IsTest;
						dc.SubmitChanges();
						if ((options & SaveOptions.GenIds) != 0) transaction.ID = entityApproval.ID;
					}
					else
					{
						tblCompanyTransPass entityPass = null;
						if ((options & SaveOptions.UpdateExisting) != 0)
						{
							entityPass = (from t in dc.tblCompanyTransPasses where t.ID == transaction.ID select t).SingleOrDefault();
							if (entityPass == null) throw new ApplicationException("Transaction not found");
						}
						else
						{
							entityPass = new tblCompanyTransPass();
							dc.tblCompanyTransPasses.InsertOnSubmit(entityPass);
							if ((options & SaveOptions.GenIds) == 0) entityPass.ID = transaction.ID;
						}
						entityPass.Amount = transaction.Request.amount;
						entityPass.ApprovalNumber = transaction.Response.ApprovalNumber.EmptyIfNull();
						entityPass.Comment = transaction.Request.comment.EmptyIfNull();
						entityPass.companyID = transaction.Config.MerchantID;
						entityPass.Currency = currencyId;
						entityPass.CreditType = (byte)netpayCreditType;
						entityPass.DebitCompanyID = transaction.Config.DebitProcessorId;
						entityPass.Payments = transaction.Request.installmets;
						entityPass.InsertDate = transaction.Response.TimeStamp;
						entityPass.IPAddress = transaction.Request.clientIP.EmptyIfNull();
						entityPass.PaymentMethod = (short)transaction.Request.paymentMethod;
						entityPass.PaymentMethodID = paymetMethodID;
						entityPass.PaymentMethodDisplay = transaction.MethodText.EmptyIfNull();
						entityPass.replyCode = "000";
						entityPass.OrderNumber = transaction.Request.merchantRefCode.EmptyIfNull();
						entityPass.TerminalNumber = transaction.Config.TerminalNumber;
						entityPass.DebitReferenceCode = transaction.Request.debitorRefCode.EmptyIfNull();
						entityPass.referringUrl = transaction.Request.referringUrl.EmptyIfNull();
						entityPass.IPCountry = "--";
						entityPass.PayID = string.Empty;
						entityPass.DeniedAdminComment = string.Empty;
						entityPass.payerIdUsed = string.Empty;
						entityPass.DeniedDate = new DateTime(1901, 1, 1);
						entityPass.DeniedPrintDate = new DateTime(1901, 1, 1);
						entityPass.DeniedSendDate = new DateTime(1901, 1, 1);
						entityPass.MerchantPD = new DateTime(1901, 1, 1);
						entityPass.PD = new DateTime(1901, 1, 1);
						//entityPass.DeniedStatus = transaction.DeniedStatusID;
						//entityPass.DebitCompanyID = transaction.DebitCompanyID;
						//entityPass.isTestOnly = transaction.IsTest;
						if ((options & SaveOptions.GenIds) != 0) transaction.ID = entityPass.ID;
						dc.SubmitChanges();
					}
					break;
				case ProcessTransactionStatus.Declined:
					tblCompanyTransFail entityFail = null;
					if ((options & SaveOptions.UpdateExisting) != 0)
					{
						entityFail = (from t in dc.tblCompanyTransFails where t.ID == transaction.ID select t).SingleOrDefault();
						if (entityFail == null) throw new ApplicationException("Transaction not found");
					}
					else
					{
						entityFail = new tblCompanyTransFail();
						dc.tblCompanyTransFails.InsertOnSubmit(entityFail);
						if ((options & SaveOptions.GenIds) == 0) entityFail.ID = transaction.ID;
					}
					//if (transaction.ReplyCode == null) throw new ArgumentException("ReplyCode cannot be null.");
					entityFail.Amount = transaction.Request.amount;
					entityFail.Comment = transaction.Request.comment.EmptyIfNull();
					entityFail.CompanyID = transaction.Config.MerchantID;
					entityFail.Currency = currencyId;
					entityFail.CreditType = (byte)netpayCreditType;
					entityFail.TransType = (byte)netpayTransactionType;
					entityFail.DebitCompanyID = transaction.Config.DebitProcessorId;
					entityFail.Payments = transaction.Request.installmets;
					entityFail.InsertDate = transaction.Response.TimeStamp;
					entityFail.IPAddress = transaction.Request.clientIP.EmptyIfNull();
					entityFail.PaymentMethod = (short)transaction.Request.paymentMethod;
					entityFail.PaymentMethodID = paymetMethodID;
					entityFail.PaymentMethodDisplay = transaction.MethodText.EmptyIfNull();
					entityFail.replyCode = transaction.Response.ErrorCode;
					entityFail.TerminalNumber = transaction.Config.TerminalNumber;
					entityFail.referringUrl = transaction.Request.referringUrl.EmptyIfNull();
					entityFail.DebitReferenceCode = transaction.Request.debitorRefCode.EmptyIfNull();
					entityFail.OrderNumber = transaction.Request.merchantRefCode.EmptyIfNull();
					entityFail.payerIdUsed = string.Empty;
					entityFail.IPCountry = "--";
					//if (transaction.IPAddress != null && transaction.IPAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) 
					//		entityFail.IPCountry = transaction.IPAddress.ToString();
					//entityFail.CreditType = transaction.CreditTypeID;
					//entityFail.DebitCompanyID = transaction.DebitCompanyID;
					//entityFail.isTestOnly = transaction.IsTest;
					dc.SubmitChanges();
					if ((options & SaveOptions.GenIds) != 0) transaction.ID = entityFail.ID;
					break;
				case ProcessTransactionStatus.Pending:
					tblCompanyTransPending entityPending = null;
					if ((options & SaveOptions.UpdateExisting) != 0)
					{
						entityPending = (from t in dc.tblCompanyTransPendings where t.ID == transaction.ID select t).SingleOrDefault();
						if (entityPending == null) throw new ApplicationException("Transaction not found");
					}
					else
					{
						entityPending = new tblCompanyTransPending();
						dc.tblCompanyTransPendings.InsertOnSubmit(entityPending);
						if ((options & SaveOptions.GenIds) == 0) entityPending.ID = transaction.ID;
					}
					entityPending.trans_amount = transaction.Request.amount;
					entityPending.Comment = transaction.Request.comment.EmptyIfNull();
					entityPending.company_id = transaction.Config.MerchantID;
					entityPending.Currency = currencyId;
					entityPending.trans_currency = currencyId;
					entityPending.trans_creditType = (byte)netpayCreditType;
					entityPending.trans_type = (byte)netpayTransactionType;
					entityPending.DebitCompanyID = transaction.Config.DebitProcessorId;
					entityPending.trans_payments = transaction.Request.installmets;
					entityPending.insertDate = transaction.Response.TimeStamp;
					entityPending.IPAddress = transaction.Request.clientIP;
					entityPending.PaymentMethod = (short)transaction.Request.paymentMethod;
					entityPending.PaymentMethodID = paymetMethodID;
					entityPending.PaymentMethodDisplay = transaction.MethodText.EmptyIfNull();
					entityPending.replyCode = transaction.Response.ErrorCode;
					entityPending.TerminalNumber = transaction.Config.TerminalNumber;
					entityPending.DebitReferenceCode = transaction.Request.debitorRefCode.EmptyIfNull();
					entityPending.DebitApprovalNumber = transaction.Response.ApprovalNumber.EmptyIfNull();
					entityPending.OrderNumber = transaction.Request.merchantRefCode.EmptyIfNull();
					entityPending.trans_order = transaction.Request.merchantRefCode.EmptyIfNull();
					entityPending.payerIdUsed = string.Empty;
					dc.SubmitChanges();
					if ((options & SaveOptions.GenIds) != 0) transaction.ID = entityPending.ID;
					break;
				default:
					throw new NotImplementedException();
			}
		}
		
		public void AddAmount(TransactionContext transaction, SaveOptions options, TransactionAmount transactionAmount)
		{
			NetpayDataContext dc = new NetpayDataContext(Domain.Get(transaction.Domain).Sql1ConnectionString);
			tblTransactionAmount entity = new tblTransactionAmount();
			entity.InsertDate = DateTime.Now;
			entity.TypeID = (int)transactionAmount.eventType;
			entity.Amount = transactionAmount.amount.GetValueOrDefault(0);
			entity.Currency = transaction.CurrencyID;
			entity.SettledAmount = transactionAmount.settleAmount.GetValueOrDefault(0);
			if (transaction != null && transaction.Config != null) entity.MerchantID = transaction.Config.MerchantID;
			entity.SettlementDate = transactionAmount.settleDate;
			switch (transaction.Response.Status)
			{
				case ProcessTransactionStatus.Approved:
					if (transaction.Request.type == ProcessTransactionType.Authorize)
						entity.TransApprovalID = transaction.ID;
					else entity.TransPassID = transaction.ID;
					break;
				case ProcessTransactionStatus.Pending:
					entity.TransPendingID = transaction.ID;
					break;
				case ProcessTransactionStatus.Declined:
					entity.TransFailID = transaction.ID;
					break;
			}
			dc.tblTransactionAmounts.InsertOnSubmit(entity);
			dc.SubmitChanges();
		}

		public void AddLog(TransactionContext transaction, SaveOptions options, TransactionHistoryType historyType, string description, int? refNumber, string refUrl, bool? isSucceeded)
		{
			NetpayDataContext dc = new NetpayDataContext(Domain.Get(transaction.Domain).Sql1ConnectionString);
			TransHistory entity = new TransHistory();
			entity.InsertDate = DateTime.Now;
			entity.TransHistoryType_id = (byte)historyType;
			entity.Description = description.Truncate(250);
			entity.ReferenceNumber = refNumber;
			entity.ReferenceUrl = refUrl.Truncate(500);

			entity.IsSucceeded = isSucceeded;
			if (transaction != null && transaction.Config != null) entity.Merchant_id = transaction.Config.MerchantID;
			switch (transaction.Response.Status)
			{
				case ProcessTransactionStatus.Approved:
					if (transaction.Request.type == ProcessTransactionType.Authorize)
						entity.TransPreAuth_id = transaction.ID;
					else entity.TransPass_id = transaction.ID;
					break;
				case ProcessTransactionStatus.Pending:
					entity.TransPending_id = transaction.ID;
					break;
				case ProcessTransactionStatus.Declined:
					entity.TransFail_id = transaction.ID;
					break;
			}
			dc.TransHistories.InsertOnSubmit(entity);
			dc.SubmitChanges();
		}

		/*
		public TransactionVO CreateTransactionVO(TransactionContext context)
		{
			var ret = new TransactionVO();
			TransactionType returnTransactionType;
			CreditType returnCreditType;
			ret.ID = context.Response.ID;
			//ret.PassedTransactionID = context.Response.ID;
			MapProcessTypeToNetpayType(context.Request.type, out returnTransactionType, out returnCreditType);
			ret.CreditTypeID = (byte) returnCreditType;
			ret.TransType = (int)returnTransactionType;
			if (context.Response.Status == ProcessTransactionStatus.Approved) {
				switch (context.Request.type) {
				case ProcessTransactionType.Authorize:
				case ProcessTransactionType.Reverse:
					ret.Status = TransactionStatus.Authorized;
					break;
				case ProcessTransactionType.Capture:
				case ProcessTransactionType.Credit:
				case ProcessTransactionType.Refund:
				case ProcessTransactionType.Sale:
					ret.Status = TransactionStatus.Captured;
					break;
				}
			} else if (context.Response.Status == ProcessTransactionStatus.Declined) {
				ret.Status = TransactionStatus.Declined;
			} else if (context.Response.Status == ProcessTransactionStatus.Pending) {
				ret.Status = TransactionStatus.Pending;
			}

			ret.Amount = context.Request.amount;
			ret.CurrencyID = context.CurrencyID;
			ret.Payments = context.Request.installmets;
			ret.PaymentMethodID = (short?)context.Request.paymentMethod;
			//ret.PaymentMethodReferenceID
			//ret.PaymentMethodDisplay = context.Method;
			//ret.IsTest 
			//ret.DeniedStatusID
			//ret.TransactionSourceID
			//ret.PrimaryPaymentID
			//ret.PaymentsIDs}
			ret.InsertDate = context.Response.TimeStamp;
			//ret.ChargebackDate 
			ret.ApprovalNumber = context.Response.ApprovalNumber;
			ret.CurrencyIsoCode = context.Request.currencyISO;
			//ret.CurrencySymbol
			//ret.CreditTypeText 
			//ret.DeniedStatusText
			//ret.TransactionSourceText 
			//ret.TransactionTypeText 
			ret.OrderNumber = context.Request.merchantRefCode;
			ret.Comment = context.Request.comment;
			//ret.PaymentMethodData 
			//ret.PaymentMethod 
			ret.DebitCompanyID = context.Config.DebitProcessorId;
			//ret.Merchant
			ret.MerchantID = context.Config.MerchantID;
			//ret.RecurringSeriesID
			//ret.IP
			ret.Installments = context.Request.installmets;
			//ret.IsChargeback
			ret.ReplyCode = context.Response.ErrorCode;
			ret.TerminalNumber = context.Config.TerminalNumber;
			ret.ReplyDescriptionText = context.Response.Reason;
			//ret.ReplyDescription = context.Response.Reason;
			//ret.InstallmentList 
			//ret.EpaImportLogList
			//ret.DeniedAdminComment 
			//ret.ReferringUrl 
			//ret.PayerIDUsed 
			ret.DebitReferenceCode = context.Request.debitorRefCode;
			//ret.IPCountry 
			//ret.OriginalTransactionID 
			//ret.IsPendingChargeback 
			//ret.PayForText 
			// fees
			//ret.TransactionFee
			//ret.RatioFee
			//ret.ClarificationFee 
			//ret.ChargebackFee 
			//ret.HandlingFee 
			return ret;
		}
		*/
		/*
		public void RetroactiveFees(string domainHost, Netpay.CommonTypes.ProcessTransactionStatus status, Bll.SettlementSettings.SettingsFilter filter, bool unsettledOnly, DateTime? dateFrom, DateTime? dateTo)
		{
			Domain domain = Domain.Get(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var fees = new Netpay.Process.NetpayConnector.FeesProcessor();

			List<TransactionContext> transactions = null;
			transactions = Load(domainHost, filter, dateFrom, dateTo, status, Netpay.CommonTypes.ProcessTransactionType.Sale);
			if (transactions.Count == 0) return;
			var cachedFloors = new CachObject();
			foreach (var trn in transactions)
			{
				fees.CalclTransactionFees(trn, filter != null ? (Bll.SettlementType?)filter.SettlementType_id : null, cachedFloors, unsettledOnly);
			}
		}
		*/
	}
}
