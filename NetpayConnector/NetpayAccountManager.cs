﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;

using Netpay.Dal.Netpay;
using Netpay.CommonTypes;
using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{
	public class NetpayAccountManager : IAccountManager
	{
		/// <summary>
		/// Begin Config Section 
		/// </summary>
		/// <param name="transaction"></param>
		/// <param name="processConfig"></param>
		/// <returns></returns>
		public bool GetConfig(TransactionContext transaction, out ProcessConfig processConfig)
		{
			NetpayDataContext dc = new NetpayDataContext(Domain.Get(transaction.Domain).Sql1ConnectionString);
			var merchant = (from c in dc.tblCompanies where c.CustomerNumber == transaction.Request.loginInfo.MerchantID select c).SingleOrDefault();
			if (merchant == null) throw new TransactionException(ProcessTransactionStatus.Declined, 500, "Merchant not found");
			//var merchantRisk = (from c in dc.SetMerchantRisks where c.Merchant_id == merchant.ID select c).SingleOrDefault();
			ProcessConfig pcfg = new ProcessConfig();
			if (transaction.Request.source == ProcessTransactionSource.System)
			{
				if (merchant.IsSystemPayCVV2) pcfg.Options |= ProcessOptions.IsCVVRequired;
				if (merchant.IsSystemPayEmail) pcfg.Options |= ProcessOptions.IsEmailRequired;
				if (merchant.IsSystemPayPersonalNumber) pcfg.Options |= ProcessOptions.IsPIDRequired;
				if (merchant.IsSystemPayPhoneNumber) pcfg.Options |= ProcessOptions.IsPhoneRequired;
			}
			else if (transaction.Request.source == ProcessTransactionSource.Manual)
			{
				if (merchant.IsSystemPayCVV2) pcfg.Options |= ProcessOptions.IsCVVRequired;
				if (merchant.IsSystemPayEmail) pcfg.Options |= ProcessOptions.IsEmailRequired;
				if (merchant.IsSystemPayPersonalNumber) pcfg.Options |= ProcessOptions.IsPIDRequired;
				if (merchant.IsSystemPayPhoneNumber) pcfg.Options |= ProcessOptions.IsPhoneRequired;
			}
			else
			{
				if (merchant.IsRemoteChargeCVV2) pcfg.Options |= ProcessOptions.IsCVVRequired;
				if (merchant.IsRemoteChargeEmail) pcfg.Options |= ProcessOptions.IsEmailRequired;
				if (merchant.IsRemoteChargePersonalNumber) pcfg.Options |= ProcessOptions.IsPIDRequired;
				if (merchant.IsRemoteChargePhoneNumber) pcfg.Options |= ProcessOptions.IsPhoneRequired;
			}
			pcfg.MerchantID = merchant.ID;
			pcfg.MerchantName = merchant.CompanyLegalName;
			pcfg.MerchantSupportEmail = merchant.merchantSupportEmail;
			pcfg.MerchantSupportPhone = merchant.merchantSupportPhoneNum;
			pcfg.MerchantNotifyEmail = merchant.PassNotifyEmail;
            pcfg.MerchantNotifyEmailChb = merchant.ChargebackNotifyMail;
			pcfg.MerchantWebsiteURL = merchant.URL.Replace(" ", ", ");
			pcfg.MerchantLanguage = (merchant.languagePreference == "heb" ? Language.Hebrew : Language.English);
			pcfg.MerchantFeeCurrency = (Currency?) merchant.CFF_Currency;
			pcfg.Descriptor = merchant.descriptor;
			Domain d = Domain.Get(transaction.Domain);
            pcfg.GatewayBrandName = d.BrandName;
			pcfg.GatewayLegalName = d.LegalName;
			pcfg.GatewayLogoPath = d.LogoPath;
			pcfg.GatewaySupportWebSite = d.ContentUrl;
			pcfg.GatewaySupportEmail = d.MailAddressFrom;
            pcfg.GatewaySupportPhone = d.TechnicalSupportPhone;
            pcfg.GatewaySupportFax = d.CustomerServiceFax;
            pcfg.GatewayComplianceEmail = d.MailAddressFromChb;
            pcfg.GatewayCompliancePhone = d.CustomerServicePhone;
            pcfg.GatewayComplianceUSPhone = d.CustomerServiceUSPhone;
            pcfg.GatewayUIServicesUrl = d.WebServicesUrl.Trim(new char[] { '/' , '\\' });
            transaction.CurrencyID = Bll.Currency.Get(transaction.Request.currencyISO).ID;
            transaction.Config = pcfg;

			SelectTerminal(transaction);
			GetTerminalInfo(transaction, true);
			processConfig = pcfg;
			return true;
		}

		public void SaveStoredMethod(string alias, CreditCardData creditcard)
		{
			return;
		}

		public CreditCardData LoadStoredMethod(string alias)
		{
			return null;
		}

		private void SelectTerminal(TransactionContext transaction)
		{
			NetpayDataContext dc = new NetpayDataContext(Domain.Get(transaction.Domain).Sql1ConnectionString);
			var rcf = from cf in dc.tblCompanyCreditFees where cf.CCF_CompanyID == transaction.Config.MerchantID && cf.CCF_CurrencyID == transaction.CurrencyID && cf.CCF_PaymentMethod == (short)transaction.Request.paymentMethod orderby cf.CCF_IsDisabled ascending, cf.CCF_ListBINs descending, cf.CCF_ID ascending select cf;
			if (transaction.Request.type != ProcessTransactionType.Refund && transaction.Request.type != ProcessTransactionType.Reverse)
				rcf.Where(cf => cf.CCF_IsDisabled == false);
			if (transaction.Method.binCountry != null) rcf.Where(cf => string.IsNullOrEmpty(cf.CCF_ListBINs) || cf.CCF_ListBINs.Contains(transaction.Method.binCountry));
			else rcf.Where(cf => cf.CCF_ListBINs == "");
			tblCompanyCreditFee ccf = rcf.FirstOrDefault();
			if (ccf != null)
			{
				switch (ccf.CCF_TSelMode)
				{
					case 0: //ratio
						{
							var ccft = (from cft in dc.tblCompanyCreditFeesTerminals
										where cft.CCFT_Ratio > 0 && cft.CCFT_CCF_ID == ccf.CCF_ID
										orderby ((cft.CCFT_UseCount * 1.0) / cft.CCFT_Ratio) ascending, cft.CCFT_Ratio descending
										select cft).First();
							if (ccft != null)
							{
								transaction.Config.TerminalNumber = ccft.CCFT_Terminal;
								transaction.Config.TerminalID = ccft.CCFT_ID;
							}
						}
						break;
					case 1: //B-W list
						{
							bool bValue = false;
							var ccftt = (from cft in dc.tblCompanyCreditFeesTerminals
										 join trm in dc.tblDebitTerminals on cft.CCFT_Terminal equals trm.terminalNumber
										 where cft.CCFT_Ratio > 0 && cft.CCFT_CCF_ID == ccf.CCF_ID
										 orderby cft.CCFT_Ratio ascending
										 select new { cft, trm }).ToList();
							foreach (var v in ccftt)
							{
								if (v.trm.dt_mcc == "7995" && v.cft.CCFT_Ratio == 1)
									bValue = (from wlb in dc.tblWhiteListBINs where wlb.BIN == transaction.Method.binValue select wlb).First() != null;
								else if (v.trm.dt_mcc == "7995" && v.cft.CCFT_Ratio == 2)
									bValue = (from wlb in dc.tblBlackListBINs where wlb.BIN == transaction.Method.binValue select wlb).First() == null;
								else if (string.IsNullOrEmpty(v.trm.dt_mcc))
									bValue = true;
								if (bValue)
								{
									transaction.Config.TerminalNumber = v.cft.CCFT_Terminal;
									transaction.Config.TerminalID = v.cft.CCFT_ID;
								}
							}
						}
						break;
					case 2: //Priority List
						{
							int bValue = 0;
							var ccftt = (from cft in dc.tblCompanyCreditFeesTerminals
										 join trm in dc.tblDebitTerminals on cft.CCFT_Terminal equals trm.terminalNumber
										 where cft.CCFT_Ratio > 0 && cft.CCFT_CCF_ID == ccf.CCF_ID
										 orderby cft.CCFT_ID ascending
										 select new { cft, trm }).ToList();
							foreach (var v in ccftt)
							{
								if (bValue == 0)
								{
									transaction.Config.TerminalNumber = v.cft.CCFT_Terminal;
									transaction.Config.TerminalID = v.cft.CCFT_ID;
									break;
								}
								else bValue = bValue - 1;
							}
						}
						break;
					case 3: //Merchant control
						{
							if (string.IsNullOrEmpty(transaction.Request.terminalCode))
								throw new TransactionException(ProcessTransactionStatus.Declined, 518, "terminal is required, TrmCode missing or invalid");
							var ccftt = (from cft in dc.tblCompanyCreditFeesTerminals
										 join trm in dc.tblDebitTerminals on cft.CCFT_Terminal equals trm.terminalNumber
										 where cft.CCFT_CCF_ID == ccf.CCF_ID && trm.id == transaction.Request.terminalCode.ToInt32()
										 select new { cft, trm }).First();
							if (ccftt != null)
							{
								transaction.Config.TerminalNumber = ccftt.cft.CCFT_Terminal;
								transaction.Config.TerminalID = ccftt.cft.CCFT_ID;
							}
							else throw new TransactionException(ProcessTransactionStatus.Declined, 518, "terminal specified in TrmCode not found");
						}
						break;
				}
			}
			if (transaction.Config.TerminalID > 0) //DO NOT REPLACE WITH LINQ (becouse of linq's read then update operation the updated value may be incorrect)
				dc.ExecuteCommand("Update tblCompanyCreditFeesTerminals Set CCFT_UseCount=CCFT_UseCount+1 Where CCFT_ID=" + transaction.Config.TerminalID);
		}

		public static void GetTerminalInfo(TransactionContext transaction, bool throwError)
		{
			NetpayDataContext dc = new NetpayDataContext(Domain.Get(transaction.Domain).Sql1ConnectionString);
			var trmInfo = (from t in dc.tblDebitTerminals
						   join d in dc.tblDebitCompanies on t.DebitCompany equals d.DebitCompany_ID
						   where t.terminalNumber == transaction.Config.TerminalNumber
						   select new { t, d.dc_name, d.dc_isActive, d.dc_TempBlocks }).FirstOrDefault();
			if (trmInfo != null)
			{
				if (throwError && !trmInfo.dc_isActive) throw new TransactionException(ProcessTransactionStatus.Declined, 592, null);
				//If (Trim(requestSource) <> "6" Or CInt(X_TypeCredit) <> 0) And Not rsTerminal("isActive") Then GetDebitTerminalInfo = "592"
                if (throwError && trmInfo.dc_TempBlocks > 0) throw new TransactionException(ProcessTransactionStatus.Declined, 524, null);
				//terminalActive = rsTerminal("isActive") Or (Trim(requestSource) = "6" And CInt(X_TypeCredit) = 0)
                transaction.Config.DebitProcessorId = trmInfo.t.DebitCompany;
                transaction.Config.DebitProcessor = trmInfo.dc_name;
				transaction.Config.Mcc = trmInfo.t.dt_mcc;
				if (transaction.Config.Descriptor.EmptyIfNull().Trim() == string.Empty) 
					transaction.Config.Descriptor = trmInfo.t.dt_Descriptor;

				if (trmInfo.t.isNetpayTerminal) transaction.Config.Options |= ProcessOptions.IsGateway;
				if (trmInfo.t.isShvaMasterTerminal) transaction.Config.Options |= ProcessOptions.IsMaster;
				if (trmInfo.t.dt_isManipulateAmount) transaction.Config.Options |= ProcessOptions.IsManipAmount;
				if (trmInfo.t.processingMethod != 0) transaction.Config.Options |= ProcessOptions.IsForcePending;
				if (trmInfo.t.dt_IsTestTerminal) transaction.Config.Options |= ProcessOptions.IsTestTerminal;

				if (trmInfo.t.dt_Enable3dsecure) transaction.Config.Options |= ProcessOptions.Enable3d;
				if (trmInfo.t.dt_EnableAuthorization) transaction.Config.Options |= ProcessOptions.EnableAuth;
				if (!trmInfo.t.dt_IsRefundBlocked) transaction.Config.Options |= ProcessOptions.EnableRefund;
				//if (!trmInfo.t.dt_) transaction.Config.Options |= ProcessOptions.EnablePartialRefund;


				transaction.Config.AccountName = trmInfo.t.accountId;
				transaction.Config.UserName = trmInfo.t.accountSubId;
				if (trmInfo.t.accountPassword256 != null) 
					transaction.Config.Password = Netpay.Infrastructure.Security.Encryption.Decrypt(transaction.Domain, trmInfo.t.accountPassword256.ToArray());

				transaction.Config.AccountName3D = trmInfo.t.accountId3D;
				transaction.Config.UserName3D = trmInfo.t.accountSubId3D;
				if (trmInfo.t.accountPassword3D256 != null) 
					transaction.Config.Password3D = Netpay.Infrastructure.Security.Encryption.Decrypt(transaction.Domain, trmInfo.t.accountPassword3D256.ToArray());

				//TerminalId = rsTerminal("id")
				//sTermName = Trim(rsTerminal("dt_Descriptor"))
				//dt_EnableRecurringBank = IIF(IsNull(rsTerminal("dt_EnableRecurringBank")), False, rsTerminal("dt_EnableRecurringBank"))
				//dt_EnableAuthorization = IIF(IsNull(rsTerminal("dt_EnableAuthorization")), False, rsTerminal("dt_EnableAuthorization"))
				//dt_Enable3dsecure = IIF(IsNull(rsTerminal("dt_Enable3dsecure")), False, rsTerminal("dt_Enable3dsecure"))
			}
			//If debitCompany = 20 Then bIsUseMaxMind = False
		}
	}
}
