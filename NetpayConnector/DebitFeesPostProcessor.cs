﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{

	public class DebitFeesPostProcessor : IPostProcessor, IPersistComplete
	{
		public bool Disabled { get; set; }
        public void LoadComplete(TransactionContext transaction) { }
        public void SaveComplete(TransactionContext transaction, SaveOptions options)
        {
            if ((options & SaveOptions.UpdateExisting) == 0)
                Application.PendingEventManager.RegisterPending(transaction, PendingEventType.FeesTransaction, null);
        }

		private tblDebitCompanyFee getCreditFees(TransactionContext transaction, NetpayDataContext dc)
		{
			tblDebitCompanyFee fees = 
				(from ccf in dc.tblDebitCompanyFees where
				ccf.DCF_DebitCompanyID == transaction.Config.DebitProcessorId &&
				(ccf.DCF_CurrencyID == 255 || ccf.DCF_CurrencyID == (int)transaction.CurrencyID) &&
				(ccf.DCF_PaymentMethod == 0 || ccf.DCF_PaymentMethod == (int)transaction.Request.paymentMethod) &&
				(ccf.DCF_TerminalNumber == "" || ccf.DCF_TerminalNumber == transaction.Config.TerminalNumber)
				orderby ccf.DCF_TerminalNumber descending, ccf.DCF_CurrencyID ascending, ccf.DCF_PaymentMethod descending
				select ccf).FirstOrDefault();
			//if (fees == null)
			//		throw new ApplicationException(string.Format("Company fees not found. pass transID={0} tblCompanyCreditFees.CCF_ID={1}", transaction.ID, transaction.Config.TerminalID));
			return fees;
		}

		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			Domain domain = DomainsManager.GetDomain(transaction.Domain);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			tblDebitCompanyFee fees = getCreditFees(transaction, dc);
			if (fees == null) {
				processEngine.AddLog(transaction, 0, TransactionHistoryType.UpdateFees, "Debit fees not found", false);
				return PostProcessResult.FailedNoRetry;
			}
			if (transaction.Response.Status == ProcessTransactionStatus.Approved)
			{
				switch (transaction.Request.type)
				{
					case ProcessTransactionType.Unknown:
						break;
					case ProcessTransactionType.Authorize:
						processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.BankFeeAuthorization, null, fees.DCF_ApproveFixedFee, fees.DCF_ApproveFixedFee, TransactionAmountFor.DebitCompany, null, null));
						break;
					case ProcessTransactionType.Capture:
					case ProcessTransactionType.Sale:
						processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.BankFeeLine, null, fees.DCF_FixedFee, fees.DCF_FixedFee, TransactionAmountFor.DebitCompany, null, null));
						processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.BankFeeTransaction, fees.DCF_PercentFee, transaction.Request.amount * (fees.DCF_PercentFee / 100), transaction.Request.amount * (fees.DCF_PercentFee / 100), TransactionAmountFor.DebitCompany, null, null));
						break;
					case ProcessTransactionType.Refund:
					case ProcessTransactionType.Reverse:
						processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.BankFeeRefund, null, fees.DCF_RefundFixedFee, fees.DCF_RefundFixedFee, TransactionAmountFor.DebitCompany, null, null));
						break;
					default:
						break;
				}

			}
			else if (transaction.Response.Status == ProcessTransactionStatus.Declined)
			{
				processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.BankFeeDeclined, null, fees.DCF_FailFixedFee, fees.DCF_FailFixedFee, TransactionAmountFor.DebitCompany, null, null));
			}
			processEngine.AddLog(transaction, 0, TransactionHistoryType.UpdateDebitFees, null, true);
			return PostProcessResult.Succseeded;
		}
    }
}
