﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;

using Netpay.CommonTypes;
using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{
	public class NetpayPendingEventManager : IPendingEventManager
	{
		public void RegisterPending(TransactionContext transaction, PendingEventType eventTypeID, string parameters)
		{
			var dc = Infrastructure.DataContext.Writer;
			EventPending entity = new EventPending();
			entity.InsertDate = DateTime.Now;
			entity.Parameters = parameters;
			entity.TryCount = 1;
			if (transaction.Config != null) entity.Merchant_id = transaction.Config.MerchantID;
			switch(transaction.Response.Status){
			case ProcessTransactionStatus.Approved:
				if(transaction.Request.type == ProcessTransactionType.Authorize)
					entity.TransPreAuth_id = transaction.ID;
				else entity.TransPass_id = transaction.ID;
				break;
			case ProcessTransactionStatus.Declined:
				entity.TransFail_id = transaction.ID;
				break;
			case ProcessTransactionStatus.Pending:
				entity.TransPending_id = transaction.ID;
				break;
			}
			dc.EventPendings.InsertOnSubmit(entity);
			dc.SubmitChanges();
		}

		private PendingEvent CreatePendingStruct(EventPending entity)
		{
			PendingEvent pendingData = new PendingEvent();
			pendingData.EventType = (PendingEventType)entity.EventPendingType_id;
			pendingData.ID = entity.EventPending_id;
			pendingData.Parameters = entity.Parameters;
			pendingData.Retries = entity.TryCount;
			pendingData.MerchantID = entity.Merchant_id;
			pendingData.CustomerID = entity.Customer_id;
			if (entity.TransPass_id != null)
			{
				pendingData.TransID = entity.TransPass_id.GetValueOrDefault();
				pendingData.TransType = ProcessTransactionType.Capture;
				pendingData.TransStatus = ProcessTransactionStatus.Approved;
			}
			else if (entity.TransPending_id != null)
			{
				pendingData.TransID = entity.TransPending_id.GetValueOrDefault();
				pendingData.TransType = ProcessTransactionType.Capture;
				pendingData.TransStatus = ProcessTransactionStatus.Pending;
			}
			else if (entity.TransPreAuth_id != null)
			{
				pendingData.TransID = entity.TransPreAuth_id.GetValueOrDefault();
				pendingData.TransType = ProcessTransactionType.Authorize;
				pendingData.TransStatus = ProcessTransactionStatus.Approved;
			} 
			else if (entity.TransFail_id != null)
			{
				pendingData.TransID = entity.TransFail_id.GetValueOrDefault();
				pendingData.TransType = ProcessTransactionType.Capture;
				pendingData.TransStatus = ProcessTransactionStatus.Declined;
			}
			return pendingData;
		}

        public PendingEvent GetNext(PendingEventType[] eventTypeIdFilter, PendingEventType[] eventTypeIdExcludeFilter)
		{
            string sql = "";
            if (Application.TestModeMID != 0) sql += " And Merchant_id=" + Application.TestModeMID;
            if (eventTypeIdFilter != null) sql += string.Format(" And EventPendingType_id IN ({0})", string.Join(",", eventTypeIdFilter.Select(p => ((int)p).ToString())));
            if (eventTypeIdExcludeFilter != null) sql += string.Format(" And EventPendingType_id Not IN ({0})", string.Join(",", eventTypeIdExcludeFilter.Select(p => ((int)p).ToString())));
            sql = "With q as (Select Top 1 * From EventPending Where TryCount > 0 And ProcessStartTime Is Null" + sql + " Order By EventPending_id Asc) Update q Set processServer={0}, ProcessStartTime=getDate()";
            Infrastructure.DataContext.Reader.ExecuteCommand(sql, System.Environment.MachineName);
			var query = from p in Infrastructure.DataContext.Reader.EventPendings where p.processServer == System.Environment.MachineName && p.TryCount > 0 && p.ProcessStartTime != null orderby p.EventPending_id ascending select p;
			return query.Select(p => CreatePendingStruct(p)).FirstOrDefault();
		}

		public void SetPendingStatus(PendingEvent pendingData, bool deleteEvent, string logInfo)
		{
			NetpayDataContext dc = Infrastructure.DataContext.Writer;
			var eventPendings = (from p in dc.EventPendings where p.EventPending_id == pendingData.ID select p).SingleOrDefault();
            if (eventPendings != null)
            {
                if (deleteEvent)
                    dc.EventPendings.DeleteOnSubmit(eventPendings);
                else
                {
                    eventPendings.TryCount = pendingData.Retries;
                    eventPendings.ProcessStartTime = null;
                }
                dc.SubmitChanges(); 
            }
		}
	}
}
