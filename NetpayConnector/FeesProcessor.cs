﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{
	public class FeesProcessor : IPostProcessor /*, IPersistComplete*/
	{
		public bool Disabled { get; set; }
		private SetSettlement getSetSettlement(NetpayDataContext dc, Domain domain, TransactionContext transaction, Bll.SettlementSettings.SettingsFilter filter)
		{
			var exp = (from stf in dc.SetSettlements where stf.SettlementType_id == (int)filter.SettlementType_id select stf);
			if (filter.SettlementType_id == Bll.SettlementType.Merchant) {
				exp = exp.Where(stf => stf.Merchant_id == filter.Merchant_id);
				if (filter.Affiliate_id != null) exp = exp.Where(stf => stf.Affiliate_id == filter.Affiliate_id || stf.Affiliate_id == null);
				if (filter.DebitCompany_id != null) exp = exp.Where(stf => stf.DebitCompany_id == filter.DebitCompany_id || stf.DebitCompany_id == null);
			} else if (filter.SettlementType_id == Bll.SettlementType.DebitCompany) {
				exp = exp.Where(stf => stf.DebitCompany_id == filter.DebitCompany_id);
				if (filter.Merchant_id != null) exp = exp.Where(stf => stf.Merchant_id == filter.Merchant_id || stf.Merchant_id == null);
				if (filter.Affiliate_id != null) exp = exp.Where(stf => stf.Affiliate_id == filter.Affiliate_id || stf.Affiliate_id == null);
			} else if (filter.SettlementType_id == Bll.SettlementType.DebitCompanyAffiliate) {
				exp = exp.Where(stf => stf.Affiliate_id == filter.Affiliate_id);
				if (filter.Merchant_id != null) exp = exp.Where(stf => stf.Merchant_id == filter.Merchant_id || stf.Merchant_id == null);
				if (filter.DebitCompany_id != null) exp = exp.Where(stf => stf.DebitCompany_id == filter.DebitCompany_id || stf.DebitCompany_id == null);
			} else if (filter.SettlementType_id == Bll.SettlementType.MerchantAffiliate) {
				exp = exp.Where(stf => stf.Affiliate_id == filter.Affiliate_id);
				if (filter.Merchant_id != null) exp = exp.Where(stf => stf.Merchant_id == filter.Merchant_id || stf.Merchant_id == null);
				if (filter.DebitCompany_id != null) exp = exp.Where(stf => stf.DebitCompany_id == filter.DebitCompany_id || stf.DebitCompany_id == null);
			}
			exp = exp.Where(stf => stf.CurrencyISOCode == transaction.Request.currencyISO || stf.CurrencyISOCode == null);

			return exp.FirstOrDefault(); //.OrderBy(stf => stf.priority)
		}

		private SetTransactionFee getTransactionFee(NetpayDataContext dc, Domain domain, TransactionContext transaction, Netpay.Bll.AmountType amountType, Bll.SettlementSettings.SettingsFilter filter)
		{
			var exp = (from stf in dc.SetTransactionFees where stf.SettlementType_id == (int)filter.SettlementType_id && stf.AmountType_id == (int)amountType select stf);
			if (filter.SettlementType_id == Bll.SettlementType.Merchant) {
				exp = exp.Where(stf => stf.Merchant_id == filter.Merchant_id);
				if (filter.Affiliate_id != null) exp = exp.Where(stf => stf.Affiliate_id == filter.Affiliate_id || stf.Affiliate_id == null);
				if (filter.DebitCompany_id != null) exp = exp.Where(stf => stf.DebitCompany_id == filter.DebitCompany_id || stf.DebitCompany_id == null);
			} else if (filter.SettlementType_id == Bll.SettlementType.DebitCompany) {
				exp = exp.Where(stf => stf.DebitCompany_id == filter.DebitCompany_id);
				if (filter.Merchant_id != null) exp = exp.Where(stf => stf.Merchant_id == filter.Merchant_id || stf.Merchant_id == null);
				if (filter.Affiliate_id != null) exp = exp.Where(stf => stf.Affiliate_id == filter.Affiliate_id || stf.Affiliate_id == null);
			} else if (filter.SettlementType_id == Bll.SettlementType.DebitCompanyAffiliate) {
				exp = exp.Where(stf => stf.Affiliate_id == filter.Affiliate_id);
				if (filter.Merchant_id != null) exp = exp.Where(stf => stf.Merchant_id == filter.Merchant_id || stf.Merchant_id == null);
				if (filter.DebitCompany_id != null) exp = exp.Where(stf => stf.DebitCompany_id == filter.DebitCompany_id || stf.DebitCompany_id == null);
			} else if (filter.SettlementType_id == Bll.SettlementType.MerchantAffiliate) {
				exp = exp.Where(stf => stf.Affiliate_id == filter.Affiliate_id);
				if (filter.Merchant_id != null) exp = exp.Where(stf => stf.Merchant_id == filter.Merchant_id || stf.Merchant_id == null);
				if (filter.DebitCompany_id != null) exp = exp.Where(stf => stf.DebitCompany_id == filter.DebitCompany_id || stf.DebitCompany_id == null);
			}
			exp = exp.Where(stf => stf.CurrencyISOCode == transaction.Request.currencyISO || stf.CurrencyISOCode == null);
			exp = exp.Where(stf => stf.PaymentMethod_id == (int)transaction.Method.paymentMethod || stf.PaymentMethod_id == null);
			
			if (transaction.Method.binCountry == null) {
				exp = exp.Where(stf => stf.CountryISOCode == null && stf.CountryGroup_id == null);
			} else {
				exp = exp.Where(stf => stf.CountryISOCode == transaction.Method.binCountry || stf.CountryISOCode == null);
				var countryGroups = (from cg in domain.Cache.CountryGroups where cg.Value.Value.Contains(domain.Cache.GetCountry(transaction.Method.binCountry)) select cg.Key).ToList();
				if (countryGroups != null) exp = exp.Where(stf => countryGroups.Contains(stf.CountryGroup_id.GetValueOrDefault()));
			}
			
			return exp.OrderBy(stf => stf.SortOrder).FirstOrDefault();
		}

		private List<Netpay.Dal.Netpay.TransactionAmount> getRelatedAmounts(List<Netpay.Dal.Netpay.TransactionAmount> amounts, Netpay.Bll.AmountType amountType, bool isFee, Bll.SettlementSettings.SettingsFilter filter)
		{
			return amounts.Where(r =>
				r.AmountType_id == (int)amountType &&
				r.SettlementType_id == (byte)filter.SettlementType_id &&
				(r.Merchant_id == filter.Merchant_id) &&
				(r.DebitCompany_id == filter.DebitCompany_id) &&
				(r.Affiliate_id == filter.Affiliate_id) &&
				(r.IsFee == isFee)).ToList();
		}

		private Netpay.Dal.Netpay.TransactionAmount initTransactionAmount(Netpay.Dal.Netpay.TransactionAmount sav, TransactionContext transaction, Netpay.Bll.AmountType amountType, bool isFee, Bll.SettlementSettings.SettingsFilter filter)
		{
			if (sav == null) sav = new Netpay.Dal.Netpay.TransactionAmount();
			switch (transaction.Response.Status)
			{
				case ProcessTransactionStatus.Approved:
					if (transaction.Request.type == ProcessTransactionType.Authorize) sav.TransPreAuth_id = transaction.ID;
					else sav.TransPass_id = transaction.ID;
					break;
				case ProcessTransactionStatus.Pending: /*sav. = transaction.ID;*/ throw new ApplicationException("unable to create TransactionAmount to pending transaction");
				case ProcessTransactionStatus.Declined: sav.TransFail_id = transaction.ID; break;
			}
			sav.InsertDate = DateTime.Now;
			sav.IsFee = isFee;
			sav.SettlementType_id = (byte)filter.SettlementType_id;
			sav.Merchant_id = filter.Merchant_id;
			sav.DebitCompany_id = filter.DebitCompany_id;
			sav.Affiliate_id = filter.Affiliate_id;
			sav.AmountType_id = (byte)amountType;
			sav.Installment = 1;
			sav.CurrencyISOCode = transaction.Request.currencyISO;
			return sav;
		}

		private Netpay.Dal.Netpay.TransactionAmount getUnsettledRow(NetpayDataContext dc, Domain domain, List<Netpay.Dal.Netpay.TransactionAmount> amounts, TransactionContext transaction, Netpay.Bll.AmountType amountType, bool isFee, Bll.SettlementSettings.SettingsFilter filter, out decimal settledTotal)
		{
			Netpay.Dal.Netpay.TransactionAmount sav = null;
			var amountList = getRelatedAmounts(amounts, amountType, isFee, filter);
			settledTotal = amountList.Where(r => r.Settlement_id != null).Sum(r => r.PercentAmount + r.FixedAmount);
			var unsettledAmounts = amountList.Where(r => r.Settlement_id == null).ToList();
			if (unsettledAmounts.Count > 1) {
				sav = unsettledAmounts[0];
				foreach (var item in unsettledAmounts)
					if (item != sav) dc.TransactionAmounts.DeleteOnSubmit(item);
			} else if (unsettledAmounts.Count == 1) {
				sav = unsettledAmounts[0];
			} else {
				sav = new Netpay.Dal.Netpay.TransactionAmount();
				amounts.Add(sav);
			}
			sav = initTransactionAmount(sav, transaction, amountType, isFee, filter);
			return sav;
		}

		private DateTime? getSettlementPayDate(NetpayDataContext dc, SetSettlement setSettlemt, DateTime transDate)
		{
			bool hasSettlements = true;
			if (setSettlemt.SettleInitialHoldDays != null) 
				hasSettlements = (from s in dc.Settlements where s.SettlementType_id == setSettlemt.SettlementType_id && s.Merchant_id == setSettlemt.Merchant_id && s.Affiliate_id == setSettlemt.Affiliate_id && s.DebitCompany_id == setSettlemt.DebitCompany_id select s.Settlement_id).Count() > 0;
			if (setSettlemt.SettleInitialHoldDays != null && !hasSettlements) 
				return transDate.AddDays(setSettlemt.SettleInitialHoldDays.GetValueOrDefault());
			else if (setSettlemt.SettleHoldDays != null)
				return transDate.AddDays(setSettlemt.SettleHoldDays.GetValueOrDefault());
			else{
				var settlementSchedule = (from s in dc.SetSettlementSchedules where s.SetSettlement_id == setSettlemt.SetSettlement_id && s.DayTo >= transDate.Day orderby s.DayTo descending select s).FirstOrDefault();
				if (settlementSchedule != null) return new DateTime(transDate.Year, transDate.Month, settlementSchedule.DaySettlement).AddMonths(1);
			}
			return null;
		}

		private void AddCancelRows(List<Netpay.Dal.Netpay.TransactionAmount> amounts, Netpay.Bll.AmountType amountType, bool isFee, Bll.SettlementSettings.SettingsFilter filter, CachObject cache)
		{
			var settledRows = getRelatedAmounts(amounts, amountType, isFee, filter).Where(r => r.Settlement_id != null).ToList();
			//filter already canceled raws
			var nonCanceledRows = new List<Netpay.Dal.Netpay.TransactionAmount>();
			foreach(var sav in settledRows){
				if (!settledRows.Any(r => (r.FixedAmount + r.PercentAmount) == -(sav.FixedAmount + sav.PercentAmount)))
					nonCanceledRows.Add(sav);
			}
			foreach(var sav in nonCanceledRows)
			{
				var newTa = new Netpay.Dal.Netpay.TransactionAmount();
				newTa.InsertDate = DateTime.Now;
				newTa.TransPass_id = sav.TransPass_id;
				newTa.TransPreAuth_id = sav.TransPreAuth_id;
				newTa.TransFail_id = sav.TransFail_id;
				newTa.Merchant_id = sav.Merchant_id;
				newTa.DebitCompany_id = sav.DebitCompany_id;
				newTa.Affiliate_id = sav.Affiliate_id;
				newTa.SetSettlement_id = sav.SetSettlement_id;
				newTa.SettlementType_id = sav.SettlementType_id;
				newTa.Settlement_id = sav.Settlement_id;
				newTa.SettlementDate = sav.SettlementDate;
				newTa.AmountType_id = sav.AmountType_id;
				newTa.Installment = sav.Installment;
				newTa.CurrencyISOCode = sav.CurrencyISOCode;
				newTa.PercentValue = -sav.PercentValue;
				newTa.PercentAmount = -sav.PercentAmount;
				newTa.FixedAmount = -sav.FixedAmount;
				newTa.IsFee = sav.IsFee;
				//newTa.Total = sav.Total;
				amounts.Add(newTa);
			}
		}

		private void compareToSettledAmount(NetpayDataContext dc, List<Netpay.Dal.Netpay.TransactionAmount> amounts, Netpay.Dal.Netpay.TransactionAmount sav, Bll.SettlementSettings.SettingsFilter filter, CachObject cache, decimal settledAmount)
		{
			settledAmount -= (sav.PercentAmount + sav.FixedAmount);
			if (settledAmount == 0) {
				amounts.Remove(sav);
				if (sav.TransactionAmount_id != 0)
					dc.TransactionAmounts.DeleteOnSubmit(sav);
			} else if (settledAmount != -sav.PercentAmount) {
				AddCancelRows(amounts, (Netpay.Bll.AmountType)sav.AmountType_id, sav.IsFee, filter, cache);
			}
		}

		private void CalcTransactionFee(NetpayDataContext dc, Domain domain, List<Netpay.Dal.Netpay.TransactionAmount> amounts, TransactionContext transaction, Netpay.Bll.AmountType amountType, Bll.SettlementSettings.SettingsFilter filter, CachObject cache)
		{
			var fee = getTransactionFee(dc, domain, transaction, amountType, filter);
			if (fee == null) return;
			var setSettlemt = getSetSettlement(dc, domain, transaction, filter);

			Netpay.Dal.Netpay.TransactionAmount settleAmount = null, feeAmount = null;
			decimal settleDelta = 0, feeDelta = 0;

			if (fee.SettlementPercentValue.GetValueOrDefault() != 0) {
				settleAmount = getUnsettledRow(dc, domain, amounts, transaction, amountType, false, filter, out settleDelta);
				settleAmount.PercentValue = fee.SettlementPercentValue;
				settleAmount.PercentAmount = (transaction.Request.amount * (fee.SettlementPercentValue.GetValueOrDefault(0) / 100));
				if (settleDelta != 0) compareToSettledAmount(dc, amounts, settleAmount, filter, cache, settleDelta);
			}

			if (fee.PercentValue.GetValueOrDefault() != 0 || fee.FixedAmount.GetValueOrDefault() != 0)
				feeAmount = getUnsettledRow(dc, domain, amounts, transaction, amountType, true, filter, out feeDelta);

			if (setSettlemt != null)
			{
				var settleDate = getSettlementPayDate(dc, setSettlemt, transaction.Response.TimeStamp.Date);
				if (feeAmount != null) {
					feeAmount.Settlement_id = null;
					feeAmount.SettlementDate = settleDate;
					feeAmount.SetSettlement_id = setSettlemt.SetSettlement_id;
				}
				if (settleAmount != null){
					settleAmount.Settlement_id = null;
					settleAmount.SettlementDate = settleDate;
					settleAmount.SetSettlement_id = setSettlemt.SetSettlement_id;
				}
			}

			if (feeAmount != null) {
				decimal? percentAmount;
				if (fee.SetTransactionFloor_id != null) {
					Bll.TransactionFloor trf = null;
					if (cache != null) {
						trf = cache.GetValue<Bll.TransactionFloor>(fee.SetTransactionFloor_id.Value);
						if (trf == null) {
							trf = new Bll.TransactionFloor(dc, fee.SetTransactionFloor_id.Value);
							trf.LoadToDate(domain, transaction.Response.TimeStamp);
							cache.Add(trf, fee.SetTransactionFloor_id.Value);
						}
					} else trf = new Bll.TransactionFloor(dc, fee.SetTransactionFloor_id.Value);
					var floorFee = trf.EvaluateNext(domain, transaction.Request.amount, transaction.CurrencyID, transaction.Response.TimeStamp, cache == null);
					feeAmount.FixedAmount = floorFee.FixedAmount.GetValueOrDefault();
					percentAmount = floorFee.PercentValue;
				}else{
					feeAmount.FixedAmount = fee.FixedAmount.GetValueOrDefault();
					percentAmount = fee.PercentValue;
				}
				if (percentAmount != null)
				{
					decimal amountFrom = 0;
					feeAmount.PercentValue = percentAmount;
					switch ((Netpay.Bll.FeeCalcMethod)fee.FeeCalcMethod_id){
					case Netpay.Bll.FeeCalcMethod.TransactionAmount:
						amountFrom = transaction.Request.amount;
						break;
					case Netpay.Bll.FeeCalcMethod.MerchantFee:
						amountFrom = amounts.Where(t => t.SettlementType_id == (byte)Bll.SettlementType.Merchant && t.IsFee).Sum(t => (t.PercentAmount + t.FixedAmount));
						break;
					case Netpay.Bll.FeeCalcMethod.RevenueFee:
						amountFrom =
							amounts.Where(t => t.SettlementType_id == (byte)Bll.SettlementType.Merchant && t.IsFee).Sum(t => (t.PercentAmount + t.FixedAmount)) -
							amounts.Where(t => t.SettlementType_id == (byte)Bll.SettlementType.DebitCompany && t.IsFee).Sum(t => (t.PercentAmount + t.FixedAmount));
						break;
					case Netpay.Bll.FeeCalcMethod.BankFee:
						amounts.Where(t => t.SettlementType_id == (byte)Bll.SettlementType.DebitCompany && t.IsFee).Sum(t => (t.PercentAmount + t.FixedAmount));
						break;
					}
					feeAmount.PercentAmount = amountFrom * (feeAmount.PercentValue.Value / 100);
				}
				if (true) { //flipDirection
					feeAmount.PercentAmount = -feeAmount.PercentAmount;
					feeAmount.FixedAmount = -feeAmount.FixedAmount;
				}
				if (feeDelta != 0)
					compareToSettledAmount(dc, amounts, feeAmount, filter, cache, feeDelta);
			}
			//sav.Total = sav.FixedAmount + sav.PercentAmount;
		}

		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			CalclTransactionFees(transaction, null, null, true);
			return PostProcessResult.Succseeded;
		}

		private void AddRollingReserve(NetpayDataContext dc, Domain domain, List<Netpay.Dal.Netpay.TransactionAmount> amounts, TransactionContext transaction, Bll.SettlementSettings.SettingsFilter filter, CachObject cache)
		{
			Bll.RollingReserve rollingReserve = null;
			if (cache != null) rollingReserve = cache.GetValue<Bll.RollingReserve>(transaction.Config.MerchantID);
			if (rollingReserve == null) {
				rollingReserve = new Bll.RollingReserve(domain, transaction.Config.MerchantID);
				if (cache != null) cache.Add(rollingReserve, transaction.Config.MerchantID);
			}
			DateTime? releaseDate = null;

			var fee = getTransactionFee(dc, domain, transaction, Bll.AmountType.RollingReserve, filter);
			var setSettlemt = getSetSettlement(dc, domain, transaction, filter);
			var takeAmount = rollingReserve.EvalTransactionReserve(dc, transaction.Request.amount, transaction.CurrencyID, (fee != null ? fee.SettlementPercentValue : null), out releaseDate);
			if (takeAmount != 0)
			{
				if (fee != null) CalcTransactionFee(dc, domain, amounts, transaction, Bll.AmountType.RollingReserve, filter, cache);

				Netpay.Dal.Netpay.TransactionAmount settleAmount = null;
				DateTime? settleDate = null;
				if (setSettlemt != null) 
					settleDate = getSettlementPayDate(dc, setSettlemt, transaction.Response.TimeStamp.Date);
				decimal setttledAmount = 0;
				//reserve row
				settleAmount = getUnsettledRow(dc, domain, amounts, transaction, Bll.AmountType.RollingReserve, false, filter, out setttledAmount);
				if (setttledAmount != 0) takeAmount -= setttledAmount;

				settleAmount.PercentValue = (takeAmount / transaction.Request.amount) * 100;
				settleAmount.PercentAmount = takeAmount;
				settleAmount.SettlementDate = settleDate;
				if (setSettlemt != null) settleAmount.SetSettlement_id = setSettlemt.SetSettlement_id;
				//release row
				var releaseRow = initTransactionAmount(null, transaction, Bll.AmountType.RollingReserve, false, filter);
				amounts.Add(releaseRow);
				releaseRow.PercentValue = -settleAmount.PercentValue;
				releaseRow.PercentAmount = -settleAmount.PercentAmount;
				releaseRow.SettlementDate = releaseDate;
				if (setSettlemt != null) releaseRow.SetSettlement_id = setSettlemt.SetSettlement_id;
			}
		}

		public void CalclTransactionFees(TransactionContext transaction, Bll.SettlementType? settlementType, CachObject cache, bool unsettledOnly, bool generateByDeniedStatus = false)
		{
			Domain domain = DomainsManager.GetDomain(transaction.Domain);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			dc.SetupLogger(true);
			List<Netpay.Dal.Netpay.TransactionAmount> amounts = (from amt in dc.TransactionAmounts where amt.TransPass_id == transaction.ID select amt).ToList();

			Bll.AmountType antType = Bll.AmountType.Capture;
			if (transaction.Response.Status == ProcessTransactionStatus.Approved) {
				switch (transaction.Request.type){
				case ProcessTransactionType.Authorize: antType = Bll.AmountType.Authorization; break;
				case ProcessTransactionType.Capture: antType = Bll.AmountType.Capture; break;
				case ProcessTransactionType.Credit: /*antType = Bll.AmountType.Refund;*/ break;
				case ProcessTransactionType.Refund: antType = Bll.AmountType.Refund; break;
				case ProcessTransactionType.Reverse: /*antType = Bll.AmountType.Refund;*/ break;
				case ProcessTransactionType.Sale: antType = Bll.AmountType.Sale; break;
				}
			} else if (transaction.Response.Status == ProcessTransactionStatus.Declined) {
				antType = Bll.AmountType.Decline;			
			}
			
			var sf = new Bll.SettlementSettings.SettingsFilter();
			sf.Merchant_id = transaction.Config.MerchantID;
			sf.DebitCompany_id = transaction.Config.DebitProcessorId;

			if (settlementType == null || settlementType.Value == Bll.SettlementType.DebitCompany) 
			{
				sf.SettlementType_id = Bll.SettlementType.DebitCompany;
				CalcTransactionFee(dc, domain, amounts, transaction, antType, sf, cache);
			}

			if (settlementType == null || settlementType.Value == Bll.SettlementType.DebitCompanyAffiliate)
			{
				sf.SettlementType_id = Bll.SettlementType.DebitCompanyAffiliate;
				var afList = (from a in dc.SetAffiliateDebitCompanies where a.DebitCompany_id == transaction.Config.DebitProcessorId select a.Affiliate_id).ToList();
				foreach (var affiliateId in afList) {
					sf.Affiliate_id = affiliateId;
					CalcTransactionFee(dc, domain, amounts, transaction, antType, sf, cache);
					sf.Affiliate_id = null;
				}
			}

			if (settlementType == null || settlementType.Value == Bll.SettlementType.Merchant)
			{
				sf.SettlementType_id = Bll.SettlementType.Merchant;
				if (antType == Bll.AmountType.Capture || antType == Bll.AmountType.Sale)
					AddRollingReserve(dc, domain, amounts, transaction, sf, cache);
				CalcTransactionFee(dc, domain, amounts, transaction, antType, sf, cache);
			}

			if (settlementType == null || settlementType.Value == Bll.SettlementType.MerchantAffiliate)
			{
				sf.SettlementType_id = Bll.SettlementType.DebitCompanyAffiliate;
				var afList = (from a in dc.SetAffiliateMerchants where a.Merchant_id == transaction.Config.MerchantID select a.Affiliate_id).ToList();
				foreach (var affiliateId in afList) {
					sf.Affiliate_id = affiliateId;
					CalcTransactionFee(dc, domain, amounts, transaction, antType, sf, cache);
					sf.Affiliate_id = null;
				}
			}
			foreach (var a in amounts)
				if (a.TransactionAmount_id == 0)
					dc.TransactionAmounts.InsertOnSubmit(a);
			dc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
		}
	}
}
