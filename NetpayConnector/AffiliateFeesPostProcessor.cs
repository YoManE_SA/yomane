﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Process.Modules;
using Netpay.Bll;

namespace Netpay.Process.NetpayConnector
{
	public class AffiliateFeesPostProcessor : IPostProcessor, IPersistComplete
	{
		public bool Disabled { get; set; }
        public void LoadComplete(TransactionContext transaction) { }
        public void SaveComplete(TransactionContext transaction, SaveOptions options)
        {
            if ((options & SaveOptions.UpdateExisting) == 0)
                Application.PendingEventManager.RegisterPending(transaction, PendingEventType.FeesTransaction, null);
        }

		private SetAffiliateMerchantFee getMerchantCreditFees(NetpayDataContext dc, Domain domain, TransactionContext transaction, int affiliateId, int? companyId)
		{
			decimal monthlyAmount = Bll.MonthlyProcessTotal.GetCurrentMonthlyTotal(domain, companyId, affiliateId, null, transaction.CurrencyID, transaction.CurrencyID, transaction.Request.amount);
			SetAffiliateMerchantFee fees = 
				(from ccf in dc.SetAffiliateMerchantFees where
					(ccf.Affiliate_id == affiliateId) &&
					(ccf.Merchant_id == null || ccf.Merchant_id == companyId) &&
					(ccf.Currency_id == null || ccf.Currency_id == (int) transaction.CurrencyID) &&
					(ccf.PaymentMethod_id == null || ccf.PaymentMethod_id == (int)transaction.Request.paymentMethod) &&
					(ccf.AmountTop >= monthlyAmount)
				orderby ccf.PaymentMethod_id ascending, ccf.AmountTop ascending
				select ccf).FirstOrDefault();
			//if (fees == null)
			//		throw new ApplicationException(string.Format("Company fees not found. pass transID={0} tblCompanyCreditFees.CCF_ID={1}", transaction.ID, transaction.Config.TerminalID));
			return fees;
		}

		private SetAffiliateDebitCompanyFee getDebitCreditFees(NetpayDataContext dc, Domain domain, TransactionContext transaction, int affiliateId, int? debitCompanyId)
		{
			decimal monthlyAmount = Bll.MonthlyProcessTotal.GetCurrentMonthlyTotal(domain, null, affiliateId, debitCompanyId, transaction.CurrencyID, transaction.CurrencyID, transaction.Request.amount);
			SetAffiliateDebitCompanyFee fees =
				(from ccf in dc.SetAffiliateDebitCompanyFees
				 where
					 (ccf.Affiliate_id == affiliateId) &&
					 (ccf.DebitCompany_id == null || ccf.DebitCompany_id == debitCompanyId) &&
					 (ccf.Currency_id == null || ccf.Currency_id == (int)transaction.CurrencyID) &&
					 (ccf.PaymentMethod_id == null || ccf.PaymentMethod_id == (int)transaction.Request.paymentMethod) &&
					 (ccf.AmountTop >= monthlyAmount)
				 orderby ccf.PaymentMethod_id ascending, ccf.AmountTop ascending
				 select ccf).FirstOrDefault();
			//if (fees == null)
			//		throw new ApplicationException(string.Format("Company fees not found. pass transID={0} tblCompanyCreditFees.CCF_ID={1}", transaction.ID, transaction.Config.TerminalID));
			return fees;
		}

		private decimal getAffiliateGrossAmount(AffiliateCalcMethod calcMethod, TransactionContext transaction, Dictionary<TransactionAmountType, CountAmount> amounts)
		{
			decimal fromAmount = 0;
			switch (calcMethod)
			{
				case AffiliateCalcMethod.Volume:
					fromAmount = transaction.Request.amount;
					break;
				case AffiliateCalcMethod.Fees:
					fromAmount = (amounts[TransactionAmountType.FeeLine].Amount + amounts[TransactionAmountType.FeeTransaction].Amount);
					break;
				case AffiliateCalcMethod.MercSale_TermBuy:
					fromAmount = (amounts[TransactionAmountType.FeeLine].Amount + amounts[TransactionAmountType.FeeTransaction].Amount);
					fromAmount -= (amounts[TransactionAmountType.BankFeeLine].Amount + amounts[TransactionAmountType.BankFeeTransaction].Amount);
					break;
				case AffiliateCalcMethod.MercSale_AffiBuy:
					fromAmount = transaction.Request.amount;
					break;
				case AffiliateCalcMethod.MercSale_AffiBuySPercent:
					fromAmount = transaction.Request.amount;
					break;
			}
			return fromAmount;
		}

		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			Domain domain = DomainsManager.GetDomain(transaction.Domain);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			if (transaction.Response.Status == ProcessTransactionStatus.Approved)
			{
				if (transaction.Request.type == ProcessTransactionType.Capture || transaction.Request.type == ProcessTransactionType.Sale)
				{
					var trnCurrency = (CommonTypes.Currency) transaction.CurrencyID;
					var amounts = (from ta in dc.tblTransactionAmounts where ta.TransPassID == transaction.Response.ID select ta).GroupBy(e => e.TypeID).Select(g => new { amountTypeID = g.Key, sumAmount = g.Sum(e => e.Amount), rowCount = g.Count() }).ToDictionary(g => (TransactionAmountType)g.amountTypeID, g => new CountAmount(g.sumAmount, g.rowCount));
					var affDebitList = (from a in dc.SetAffiliateDebitCompanies where a.DebitCompany_id == transaction.Config.DebitProcessorId select a).ToList();
					decimal bankAffiliateFees = 0;
					foreach (var aff in affDebitList)
					{
						SetAffiliateDebitCompanyFee fees = getDebitCreditFees(dc, domain, transaction, aff.Affiliate_id, aff.DebitCompany_id);
						if (fees == null) continue;
						var grossValue = getAffiliateGrossAmount((AffiliateCalcMethod)fees.CalcMethod, transaction, amounts);
						grossValue = grossValue * (fees.PercentFee / 100);
						bankAffiliateFees += grossValue;
						processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.BankAffiliateFeeProcess, fees.PercentFee, grossValue, grossValue, TransactionAmountFor.DebitCompany, aff.Affiliate_id, null));
					}

					var affMerchantList = (from a in dc.SetAffiliateMerchants where a.Merchant_id == transaction.Config.MerchantID select a).ToList();
					foreach (var aff in affMerchantList) 
					{
						SetAffiliateMerchantFee fees = getMerchantCreditFees(dc, domain, transaction, aff.Affiliate_id, aff.Merchant_id);
						if (fees == null) continue;
						var grossValue = getAffiliateGrossAmount((AffiliateCalcMethod)fees.CalcMethod, transaction, amounts);
						if ((AffiliateCalcMethod)fees.CalcMethod == AffiliateCalcMethod.MercSale_TermBuy)
							grossValue -= bankAffiliateFees;
						grossValue = grossValue * (fees.PercentFee / 100);
						processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.AffiliateFeeProcess, fees.PercentFee, grossValue, grossValue, TransactionAmountFor.Merchant, aff.Affiliate_id, null));
					}
					
					processEngine.AddLog(transaction, 0, TransactionHistoryType.UpdateAffiliateFees, null, true);
				}
			}
			return PostProcessResult.Succseeded;
		}
    }
}
