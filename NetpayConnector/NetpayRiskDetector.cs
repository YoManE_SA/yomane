﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Bll.RiskManagement;
using Netpay.Bll;
using Netpay.CommonTypes;
using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{
	public class NetpayRiskDetector : IRiskDetector, IProcessCompleteHandler
	{
		private bool Detect(TransactionContext transaction, FraudDetectionRuleGroup ruleGroup) 
		{
			TransactionType transType;
			CreditType creditType;
            DateTime? blockDate;
			NetpayPersistor.MapProcessTypeToNetpayType(transaction.Request.type, out transType, out creditType);

			FraudDetectionResult fraudDetectionResult = new FraudDetectionResult();
			bool result = FraudDetection.Detect(
				transaction.Domain, ruleGroup,
				transaction.Request.loginInfo.MerchantID,
				(int)creditType,
				transaction.Request.clientIP,
				transaction.Request.customer.email,
				transaction.Request.creditCard.Pan.Substring(0, 6).ToInt32(),
				transaction.Request.amount,
				transaction.Request.currencyISO,
				transaction.Request.billingAddress.CountryISO,
				transaction.Request.creditCard.OwnerName,
				transaction.Request.installmets,
				Netpay.Infrastructure.Security.Encryption.Encrypt(transaction.Domain, transaction.Request.creditCard.Pan).ToArray(), out fraudDetectionResult);

			// create blocks
			if (!result && fraudDetectionResult.CreateBlock)
			{
				CreditCard card = new CreditCard(transaction.Request.creditCard.Pan);
                Netpay.Bll.RiskManagement.BlockedItems.AddBlockedCard(transaction.Domain, transaction.Config.MerchantID, card, fraudDetectionResult.RuleID, DateTime.Now.Add(fraudDetectionResult.BlockDuration), fraudDetectionResult.ReplyCode, "blocked", out blockDate);
			}

			// handle reply
			if (ruleGroup == FraudDetectionRuleGroup.PreProcess)
			{
				TransactionResponse reply = new TransactionResponse();
				if (result)
				{
					reply.Status = ProcessTransactionStatus.Approved;
					transaction.Response = reply;
				}
				else
				{
					// reply
					reply.Status = ProcessTransactionStatus.Declined;
					reply.Reason = fraudDetectionResult.Description;
					reply.ErrorCode = fraudDetectionResult.ReplyCode;
					transaction.Response = reply;
				} 
			}

			return result;
		}

		public bool DetectRisk(TransactionContext transaction)
		{
			return Detect(transaction, FraudDetectionRuleGroup.PreProcess);
		}

		public void ProcessComplete(TransactionContext transaction)
		{
			Detect(transaction, FraudDetectionRuleGroup.PostProcess);
		}
	}
}
