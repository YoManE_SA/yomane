﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Process.Modules;
using System.Collections.Specialized;
using System.Web;


namespace Netpay.Process.NetpayConnector
{
	public class RecurringPostProcessor : IPostProcessor
	{
		public RecurringPostProcessor() { }

		public bool Disabled { get; set; }

		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			NameValueCollection reqString = HttpUtility.ParseQueryString(pendingData.Parameters);
			if (pendingData.EventType != PendingEventType.CreateRecurringSeries) 
				throw new ApplicationException("RecurringPostProcessor does not support event " + pendingData.EventType.ToString());

			int seriesID = Infrastructure.DataContext.Writer.RecurringBatchCreateFlexibleSeriesEx(
				reqString["Pass"] == "1", 
				transaction.ID,
				reqString["RecurringStrings"], 
				transaction.Request.comment,
				Validation.TestVar(reqString["TrmCode"], 0, -1, 0), 
				transaction.Request.clientIP, 
				Validation.TestVar(reqString["RecurringApproval"], 0, -1, 0), 
				reqString["IDentifier"], 
				Validation.TestVar(reqString["SeriesID"], 0, -1, 0)).SingleOrDefault().Column1.GetValueOrDefault();
			
			if (seriesID <= 0) 
				throw new ApplicationException("RecurringPostProcessor RecurringBatchCreateFlexibleSeriesEx Error " + seriesID + " Transaction:" + transaction.ID);
			processEngine.AddLog(transaction, 0, TransactionHistoryType.InfoCreateRecurring, null, seriesID, null, true);
			
			return PostProcessResult.Succseeded;
		}
	}
}