﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.Bll;

using Netpay.Process.Modules;

namespace Netpay.Process.NetpayConnector
{
	/// <summary>
	/// Generates installments and transaction installments events
	/// </summary>
	public class InstallmentsPostProcessor : IPostProcessor
	{
		public bool Disabled { get; set; }
		public InstallmentsPostProcessor() { }

		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			if (transaction.Request.amount == 0)
				throw new ApplicationException("Transaction amount is 0");
			if (transaction.Request.installmets == 0)
				throw new ApplicationException("Transaction installments is 0");
			
			//Domain domain = Domain.Get(transaction.Domain);
			var dc = Infrastructure.DataContext.Writer;

			decimal installmentAmount = System.Math.Floor(transaction.Request.amount / transaction.Request.installmets);
			decimal firstInstallmentAmount = transaction.Request.amount - (installmentAmount * (transaction.Request.installmets - 1));
			DateTime nextPaymentDate = transaction.Config.TransactionPayDate.GetValueOrDefault();
			for (byte installmentNumber = 1; installmentNumber <= transaction.Request.installmets; installmentNumber++) 
			{
				DateTime installmentPaymentDate = nextPaymentDate.AddMonths(installmentNumber - 1);
				string comment = installmentNumber + " / " + transaction.Request.installmets;
				decimal currentInstallmentAmount = installmentNumber == 1 ? firstInstallmentAmount : installmentAmount;

				// installments
				tblCompanyTransInstallment installment = new tblCompanyTransInstallment();
				installment.transAnsID = transaction.ID;
				installment.amount = currentInstallmentAmount;
				installment.comment = comment;
				installment.CompanyID = transaction.Config.MerchantID;
				installment.InsID = installmentNumber;
				installment.MerchantPD = installmentPaymentDate;

				dc.tblCompanyTransInstallments.InsertOnSubmit(installment);
			}

			try
			{
				dc.SubmitChanges();
			}
			catch (Exception ex)
			{
				// rollback
				var installments = from i in dc.tblCompanyTransInstallments where i.transAnsID == transaction.ID select i;
				dc.tblCompanyTransInstallments.DeleteAllOnSubmit(installments);
				dc.SubmitChanges();
				throw ex;
			}
			return PostProcessResult.Succseeded;
		}
	}
}
