﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AdminUtils
{
	public partial class Encryption : Form
	{
		public Encryption()
		{
			InitializeComponent();
		}

		private void btnEncrypt_Click(object sender, EventArgs e)
		{
			string encrypted = null;
			//Netpay.Infrastructure.Security.Encryption.Encrypt(txtSource.Text, out encrypted);
			txtDestination.Text = encrypted;
		}

		private void btnDecrypt_Click(object sender, EventArgs e)
		{
			txtDestination.Text = Netpay.Infrastructure.Security.Encryption.DecryptHex(txtSource.Text);
		}
	}
}
