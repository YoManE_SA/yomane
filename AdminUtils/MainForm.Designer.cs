﻿namespace AdminUtils
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnExecutePackage = new System.Windows.Forms.Button();
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.btnCreateExcells = new System.Windows.Forms.Button();
            this.btnTransactionsGenerator = new System.Windows.Forms.Button();
            this.btnCopyHPPImages = new System.Windows.Forms.Button();
            this.btnCopyPPImages = new System.Windows.Forms.Button();
            this.btnCopyPPBanners = new System.Windows.Forms.Button();
            this.btnRemapData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnExecutePackage
            // 
            this.btnExecutePackage.Location = new System.Drawing.Point(12, 12);
            this.btnExecutePackage.Name = "btnExecutePackage";
            this.btnExecutePackage.Size = new System.Drawing.Size(122, 23);
            this.btnExecutePackage.TabIndex = 0;
            this.btnExecutePackage.Text = "Execute Package...";
            this.btnExecutePackage.UseVisualStyleBackColor = true;
            this.btnExecutePackage.Click += new System.EventHandler(this.btnExecutePackage_Click);
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.Location = new System.Drawing.Point(13, 41);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(121, 23);
            this.btnEncrypt.TabIndex = 1;
            this.btnEncrypt.Text = "Encryption...";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
            // 
            // btnCreateExcells
            // 
            this.btnCreateExcells.Location = new System.Drawing.Point(13, 69);
            this.btnCreateExcells.Margin = new System.Windows.Forms.Padding(2);
            this.btnCreateExcells.Name = "btnCreateExcells";
            this.btnCreateExcells.Size = new System.Drawing.Size(121, 24);
            this.btnCreateExcells.TabIndex = 2;
            this.btnCreateExcells.Text = "Create Excells";
            this.btnCreateExcells.UseVisualStyleBackColor = true;
            this.btnCreateExcells.Click += new System.EventHandler(this.btnCreateExcells_Click);
            // 
            // btnTransactionsGenerator
            // 
            this.btnTransactionsGenerator.Location = new System.Drawing.Point(13, 97);
            this.btnTransactionsGenerator.Margin = new System.Windows.Forms.Padding(2);
            this.btnTransactionsGenerator.Name = "btnTransactionsGenerator";
            this.btnTransactionsGenerator.Size = new System.Drawing.Size(121, 24);
            this.btnTransactionsGenerator.TabIndex = 3;
            this.btnTransactionsGenerator.Text = "Transactions Generator";
            this.btnTransactionsGenerator.UseVisualStyleBackColor = true;
            this.btnTransactionsGenerator.Click += new System.EventHandler(this.btnTransactionsGenerator_Click);
            // 
            // btnCopyHPPImages
            // 
            this.btnCopyHPPImages.Location = new System.Drawing.Point(13, 126);
            this.btnCopyHPPImages.Name = "btnCopyHPPImages";
            this.btnCopyHPPImages.Size = new System.Drawing.Size(121, 23);
            this.btnCopyHPPImages.TabIndex = 4;
            this.btnCopyHPPImages.Text = "Copy Hpp Images";
            this.btnCopyHPPImages.UseVisualStyleBackColor = true;
            this.btnCopyHPPImages.Click += new System.EventHandler(this.btnCopyHPPImages_Click);
            // 
            // btnCopyPPImages
            // 
            this.btnCopyPPImages.Location = new System.Drawing.Point(13, 155);
            this.btnCopyPPImages.Name = "btnCopyPPImages";
            this.btnCopyPPImages.Size = new System.Drawing.Size(121, 23);
            this.btnCopyPPImages.TabIndex = 5;
            this.btnCopyPPImages.Text = "Copy PP Images";
            this.btnCopyPPImages.UseVisualStyleBackColor = true;
            this.btnCopyPPImages.Click += new System.EventHandler(this.btnCopyPPImages_Click);
            // 
            // btnCopyPPBanners
            // 
            this.btnCopyPPBanners.Location = new System.Drawing.Point(13, 184);
            this.btnCopyPPBanners.Name = "btnCopyPPBanners";
            this.btnCopyPPBanners.Size = new System.Drawing.Size(121, 23);
            this.btnCopyPPBanners.TabIndex = 6;
            this.btnCopyPPBanners.Text = "Copy PP Banners";
            this.btnCopyPPBanners.UseVisualStyleBackColor = true;
            this.btnCopyPPBanners.Click += new System.EventHandler(this.btnCopyPPBanners_Click);
            // 
            // btnRemapData
            // 
            this.btnRemapData.Location = new System.Drawing.Point(158, 12);
            this.btnRemapData.Name = "btnRemapData";
            this.btnRemapData.Size = new System.Drawing.Size(114, 23);
            this.btnRemapData.TabIndex = 7;
            this.btnRemapData.Text = "Remap Data Path";
            this.btnRemapData.UseVisualStyleBackColor = true;
            this.btnRemapData.Click += new System.EventHandler(this.btnRemapData_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 261);
            this.Controls.Add(this.btnRemapData);
            this.Controls.Add(this.btnCopyPPBanners);
            this.Controls.Add(this.btnCopyPPImages);
            this.Controls.Add(this.btnCopyHPPImages);
            this.Controls.Add(this.btnTransactionsGenerator);
            this.Controls.Add(this.btnCreateExcells);
            this.Controls.Add(this.btnEncrypt);
            this.Controls.Add(this.btnExecutePackage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin Utils";
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnExecutePackage;
		private System.Windows.Forms.Button btnEncrypt;
		private System.Windows.Forms.Button btnCreateExcells;
		private System.Windows.Forms.Button btnTransactionsGenerator;
		private System.Windows.Forms.Button btnCopyHPPImages;
		private System.Windows.Forms.Button btnCopyPPImages;
		private System.Windows.Forms.Button btnCopyPPBanners;
        private System.Windows.Forms.Button btnRemapData;
	}
}