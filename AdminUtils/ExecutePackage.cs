﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Netpay.Infrastructure.VO;

namespace AdminUtils
{
	public partial class ExecutePackage : Form
	{
		public ExecutePackage()
		{
			InitializeComponent();
			
			List<string> packages = Netpay.Dal.DataAccess.IntegrationServices.IntegrationServicesManager.GetPackageFiles();
			cbPackages.DataSource = packages;
			
		}

		private void btnExecutePackage_Click(object sender, EventArgs e)
		{
			UserVO user = Netpay.Infrastructure.Security.SecurityManager.GetUser(((MainForm)Owner).CredentialsToken);
			Enabled = false;
			Netpay.Infrastructure.DataAccess.IntegrationServices.IntegrationServicesManager.ExecutePackage(user.Domain, cbPackages.SelectedValue.ToString());
			Dispose();
		}
	}
}
