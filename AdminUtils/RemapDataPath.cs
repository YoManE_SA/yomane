﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Netpay.Infrastructure;

namespace AdminUtils
{
    public partial class RemapDataPath : Form
    {
        public RemapDataPath()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (chkDeleteFiles.Checked && System.IO.Directory.Exists(txtTarget.Text)) System.IO.Directory.Delete(txtTarget.Text, true);
            if (!string.IsNullOrEmpty(txtNPProcess.Text)) RemapProcessData(txtNPProcess.Text, txtTarget.Text);
            if (!string.IsNullOrEmpty(txtAdminData.Text)) RemapAdminData(txtAdminData.Text, txtTarget.Text);
            if (!string.IsNullOrEmpty(txtNasData.Text)) RemapNasData(txtNasData.Text, txtTarget.Text);
            MessageBox.Show("Completed");
        }

        private void CopyDirectory(string srcPath, string targetPath, bool recursive = true)
        {
            if (!System.IO.Directory.Exists(targetPath))
                System.IO.Directory.CreateDirectory(targetPath);
            if (!System.IO.Directory.Exists(srcPath)) return;
            foreach (var f in System.IO.Directory.GetFiles(srcPath)) { 
                string target =  System.IO.Path.Combine(targetPath, System.IO.Path.GetFileName(f));
                System.IO.File.Copy(f, target, true);
                try { System.IO.File.SetCreationTime(target, System.IO.File.GetCreationTime(f)); } catch { }
            }
            if (!recursive) return;
            foreach (var f in System.IO.Directory.GetDirectories(srcPath)) { 
                string target = System.IO.Path.Combine(targetPath, System.IO.Path.GetFileName(f));
                CopyDirectory(f, target, recursive);
                try { System.IO.Directory.SetCreationTime(target, System.IO.Directory.GetCreationTime(f));  } catch { }
            }
        }

        private void CopyFile(string srcPath, string targetPath)
        {
            var dirName = System.IO.Path.GetDirectoryName(targetPath);
            if (!System.IO.Directory.Exists(dirName))
                System.IO.Directory.CreateDirectory(dirName);
            System.IO.File.Copy(srcPath, targetPath, true);
            System.IO.File.SetCreationTime(targetPath, System.IO.File.GetCreationTime(srcPath));
        }

        private void RemapProcessData(string srcPath, string targetPath)
        {
            CopyDirectory(System.IO.Path.Combine(srcPath, "Uploads"), System.IO.Path.Combine(targetPath, "Private/Logs"), false);
        }

        private void RemapAdminData(string srcPath, string targetPath)
        {
            CopyDirectory(System.IO.Path.Combine(srcPath, "WireTransfer"), System.IO.Path.Combine(targetPath, "Private/Wires/BatchFiles"));
            CopyDirectory(System.IO.Path.Combine(srcPath, "ChbHistory"), System.IO.Path.Combine(targetPath, "Private/Process/BnS/CHB/Uploads"));
        }

        private void RemapBankHandler(string source, string target, string folder)
        {
            var epaFolders = Enum.GetValues(typeof(Netpay.Bll.BankHandler.EpaFolder));
            var dcNames = Enum.GetValues(typeof(Netpay.Bll.BankHandler.EpaDebitCompany));
            foreach (var dc in dcNames)
                foreach (var d in epaFolders)
                    CopyDirectory(System.IO.Path.Combine(source, d.ToString(), dc.ToString()), System.IO.Path.Combine(target, dc.ToString(), folder, d.ToString()));
        }

        private void RemapNasData(string srcPath, string targetPath)
        {
            int accountId;
            RemapBankHandler(System.IO.Path.Combine(srcPath, "EPA"), System.IO.Path.Combine(targetPath, "Private/Process/"), "EPA");
            RemapBankHandler(System.IO.Path.Combine(srcPath, "CHB"), System.IO.Path.Combine(targetPath, "Private/Process/"), "CHB");
            RemapBankHandler(System.IO.Path.Combine(srcPath, "Auth"), System.IO.Path.Combine(targetPath, "Private/Process/"), "Auth");
            CopyDirectory(System.IO.Path.Combine(srcPath, "TransactionImages"), System.IO.Path.Combine(targetPath, "Private/Transaction Images"));
            CopyDirectory(System.IO.Path.Combine(srcPath, "MerchantContentPages"), System.IO.Path.Combine(targetPath, "Private/Site Content/MCP"));
            CopyDirectory(System.IO.Path.Combine(srcPath, "WireFileUploads"), System.IO.Path.Combine(targetPath, "Private/Wires/WireFiles"));
            CopyDirectory(System.IO.Path.Combine(srcPath, "EmailTemplates"), System.IO.Path.Combine(targetPath, "Private/Email Templates"));
            CopyDirectory(System.IO.Path.Combine(srcPath, "EmailsCRM"), System.IO.Path.Combine(targetPath, "Private/Emails"));

            //CopyDirectory(System.IO.Path.Combine(srcPath, "GlobalAccess"), System.IO.Path.Combine(targetPath, "Public"));
            CopyDirectory(System.IO.Path.Combine(srcPath, "Storage/Admin"), System.IO.Path.Combine(targetPath, "Private/AdminUser Files"));
            if (System.IO.Directory.Exists(System.IO.Path.Combine(targetPath, "Private/AdminUser Files/MerchantRegistrations"))) System.IO.Directory.Move(System.IO.Path.Combine(targetPath, "Private/AdminUser Files/MerchantRegistrations"), System.IO.Path.Combine(targetPath, "Private/Merchant Registrations"));

            if (System.IO.Directory.Exists(System.IO.Path.Combine(srcPath, "Storage/Merchants")))
            { 
                //CopyDirectory(System.IO.Path.Combine(srcPath, "Storage/Merchants"), System.IO.Path.Combine(targetPath, "Private/Account Files"));
                var mapTable = (from n in Netpay.Infrastructure.DataContext.Reader.Accounts where n.Merchant_id != null select new {n.Account_id, n.Merchant_id }).ToDictionary(x => x.Merchant_id, x => x.Account_id); 
                var dirNames = System.IO.Directory.GetDirectories(System.IO.Path.Combine(srcPath, "Storage/Merchants"));
                foreach (var d in dirNames) {
                    if (!mapTable.TryGetValue(System.IO.Path.GetFileName(d).ToNullableInt().GetValueOrDefault(), out accountId)) continue;
                    CopyDirectory(d, System.IO.Path.Combine(targetPath, "Private/Account Files/" + accountId));
                }
            }
            if (System.IO.Directory.Exists(System.IO.Path.Combine(srcPath, "Storage/Customers")))
            { 
                //CopyDirectory(System.IO.Path.Combine(srcPath, "Storage/Customers"), System.IO.Path.Combine(targetPath, "Private/Account Files"));
                var mapTable = (from n in Netpay.Infrastructure.DataContext.Reader.Accounts where n.Customer_id != null select new { n.Account_id, n.Customer_id }).ToDictionary(x => x.Customer_id, x => x.Account_id); 
                var dirNames = System.IO.Directory.GetDirectories(System.IO.Path.Combine(srcPath, "Storage/Customers"));
                foreach (var d in dirNames) {
                    if (!mapTable.TryGetValue(System.IO.Path.GetFileName(d).ToNullableInt().GetValueOrDefault(), out accountId)) continue;
                    CopyDirectory(d, System.IO.Path.Combine(targetPath, "Private/Account Files/" + accountId));
                }
            }
            if (System.IO.Directory.Exists(System.IO.Path.Combine(srcPath, "Storage/Partners")))
            { 
                //CopyDirectory(System.IO.Path.Combine(srcPath, "Storage/Partners"), System.IO.Path.Combine(targetPath, "Private/Account Files"));
                var mapTable = (from n in Netpay.Infrastructure.DataContext.Reader.Accounts where n.Affiliate_id != null select new { n.Account_id, n.Affiliate_id }).ToDictionary(x => x.Affiliate_id, x => x.Account_id);
                var dirNames = System.IO.Directory.GetDirectories(System.IO.Path.Combine(srcPath, "Storage/Partners"));
                foreach (var d in dirNames) {
                    if (!mapTable.TryGetValue(System.IO.Path.GetFileName(d).ToNullableInt().GetValueOrDefault(), out accountId)) continue;
                    CopyDirectory(d, System.IO.Path.Combine(targetPath, "Private/Account Files/" + accountId));
                }
            }


            CopyDirectory(System.IO.Path.Combine(srcPath, "GlobalAccess/Merchants"), System.IO.Path.Combine(targetPath, "Public/Accounts"));
            CopyDirectory(System.IO.Path.Combine(srcPath, "GlobalAccess/Customers"), System.IO.Path.Combine(targetPath, "Public/Accounts"));
            CopyDirectory(System.IO.Path.Combine(srcPath, "GlobalAccess/Partners"), System.IO.Path.Combine(targetPath, "Public/Accounts"));
            
            var files = System.IO.Directory.GetFiles(System.IO.Path.Combine(srcPath, "HostedPage2Logos"));
            foreach (var f in files)
            {
                var nameParts = System.IO.Path.GetFileName(f).Split('_');
                if (nameParts.Length == 2)
                    CopyFile(f, System.IO.Path.Combine(targetPath, "Public/Accounts", nameParts[0], "UIServices/V1", System.IO.Path.GetFileName(f)));
                else if (nameParts.Length == 3) CopyFile(f, System.IO.Path.Combine(targetPath, "Public/Accounts", nameParts[0], "UIServices/V1", nameParts[1], nameParts[2])); //templates
            }
            if (System.IO.Directory.Exists(System.IO.Path.Combine(srcPath, "PartnersLogos"))) { 
                files = System.IO.Directory.GetFiles(System.IO.Path.Combine(srcPath, "PartnersLogos"));
                foreach (var f in files)
                {
                    var nameParts = System.IO.Path.GetFileName(f).Split('_');
                    CopyFile(f, System.IO.Path.Combine(targetPath, "Public/Accounts/", nameParts[1], "Logo.jpg"));
                }
            }
            var mnames = (from n in Netpay.Infrastructure.DataContext.Reader.tblFiles
                join a in Netpay.Infrastructure.DataContext.Reader.Accounts on n.Company_id equals a.Merchant_id
                select new { merchantId = n.Company_id, accountId = a.Account_id, fileName = n.FileName }).ToList();
            foreach (var f in mnames)
            {
                var srcFile = System.IO.Path.Combine(srcPath, "MerchantsDocs", f.fileName);
                if (System.IO.File.Exists(srcFile)) CopyFile(srcFile, System.IO.Path.Combine(targetPath, "Private/Account Files", f.accountId.ToString(), "Files", System.IO.Path.GetFileName(srcFile)));
            }
            
            var cnames = (from n in Netpay.Infrastructure.DataContext.Reader.tblFiles
                join a in Netpay.Infrastructure.DataContext.Reader.Accounts on n.Customer_id equals a.Customer_id
                select new { customerId = n.Customer_id, accountId = a.Account_id, fileName = n.FileName }).ToList();
            foreach (var f in cnames)
            {
                var srcFile = System.IO.Path.Combine(srcPath, "CustomersDocs", f.fileName);
                if (System.IO.File.Exists(srcFile)) CopyFile(srcFile, System.IO.Path.Combine(targetPath, "Private/Account Files", f.accountId.ToString(), "Files", System.IO.Path.GetFileName(srcFile)));
            }

            //cleanup
            if (System.IO.Directory.Exists(System.IO.Path.Combine(targetPath, "Public/Accounts"))) { 
                var dirs = System.IO.Directory.GetDirectories(System.IO.Path.Combine(targetPath, "Public/Accounts"));
                var mapTable = (from n in Netpay.Infrastructure.DataContext.Reader.Accounts where n.Merchant_id != null select new { n.Account_id, n.Merchant_id }).ToDictionary(x => x.Merchant_id, x => x.Account_id); 
                foreach (var d in dirs) {
                    var dirName = System.IO.Path.GetFileName(d);
                    if (dirName.Length != 7) {
                        if (mapTable.TryGetValue(System.IO.Path.GetFileName(d).ToNullableInt().GetValueOrDefault(), out accountId)) {
                            string strTarget = System.IO.Path.Combine(targetPath, "Private/Account Files", accountId.ToString(), "RulesAndRegulations.html");
                            if (System.IO.File.Exists(System.IO.Path.Combine(d, "RulesAndRegulations.html")) && !System.IO.File.Exists(strTarget))
                                System.IO.File.Copy(System.IO.Path.Combine(d, "RulesAndRegulations.html"), strTarget);
                        }
                        System.IO.Directory.Delete(d, true);
                    }
                }
            }

            if (System.IO.Directory.Exists(System.IO.Path.Combine(targetPath, "Private/Account Files"))) { 
                var dirs = System.IO.Directory.GetDirectories(System.IO.Path.Combine(targetPath, "Private/Account Files"));
                var contentExt = new string[] { ".html", ".htm"};
                foreach (var d in dirs) {
                    var moveFiles = System.IO.Directory.GetFiles(d).Where(f => Array.IndexOf(contentExt, System.IO.Path.GetExtension(f).ToLower()) > -1);
                    if (moveFiles.Count() == 0) continue;
                    var tDirName = System.IO.Path.Combine(d, "Content");
                    if (!System.IO.Directory.Exists(tDirName)) System.IO.Directory.CreateDirectory(tDirName);
                    foreach(var mv in moveFiles)
                        System.IO.File.Move(mv, System.IO.Path.Combine(tDirName, System.IO.Path.GetFileName(mv)));
                }
            }

            if (System.IO.File.Exists(System.IO.Path.Combine(srcPath, Domain.Current.Host + ".Infrastructure.config.xml")))
                System.IO.File.Copy(System.IO.Path.Combine(srcPath, Domain.Current.Host + ".Infrastructure.config.xml"), System.IO.Path.Combine(targetPath, Domain.Current.Host + ".Infrastructure.config.xml"), true);

            //HostedPage2Logos //ignore
            //var names = (from n in Netpay.Infrastructure.DataContext.Reader.tblFiles select new { merchantId = n.Company_id, cutstomerId = n.Customer_id, fileName = n.FileName }).ToList();

        }    
    
    }
}
