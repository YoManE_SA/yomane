﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Netpay.Bll;
using Netpay.Infrastructure;

namespace AdminUtils
{
	public partial class Login : Form
	{
		public Login()
		{
			InitializeComponent();
			cbUserType.SelectedIndex = 0;
            ddlDomain.DataSource = Netpay.Infrastructure.Domain.Domains.Keys.ToList();
            cbUserType.DataSource = System.Enum.GetValues(typeof(Netpay.Infrastructure.Security.UserRole));
		}

		private void btnLogin_Click(object sender, EventArgs e)
		{
			Guid creds;
            Netpay.Infrastructure.Domain.Current = Netpay.Infrastructure.Domain.Get(ddlDomain.Text);
			var result = Netpay.Infrastructure.Security.Login.DoLogin(new Netpay.Infrastructure.Security.UserRole[] { (Netpay.Infrastructure.Security.UserRole)cbUserType.SelectedItem }, txtUserName.Text, txtEmail.Text, txtPassword.Text, null, out creds);
			if (result != Netpay.Infrastructure.Security.LoginResult.Success)
				MessageBox.Show(result.ToString());
			else
			{
				((MainForm)Owner).IsLoggedin = true;
				((MainForm)Owner).CredentialsToken = creds;
				((MainForm)Owner).Enabled = true;
				Close();
			}
		}

		private void Login_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)
				btnLogin_Click(null, null);
		}

		private void Login_FormClosed(object sender, FormClosedEventArgs e)
		{
			((MainForm)Owner).IsLoggedin = true;
		}

		private void label5_Click(object sender, EventArgs e)
		{

		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
