﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AdminUtils
{
	public partial class SourceTarget : Form
	{
		public SourceTarget()
		{
			InitializeComponent();
		}

		public string DomainHost { get { return txtDomain.Text; } set { txtDomain.Text = value; } }
		public string SourceText { get { return txtSource.Text; } set { txtSource.Text = value; } }
		public string TargetText { get { return txtTarget.Text; } set { txtTarget.Text = value; } }
		
	}
}
