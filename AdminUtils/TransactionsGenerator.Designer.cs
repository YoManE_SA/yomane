﻿namespace AdminUtils
{
	partial class TransactionsGenerator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblSuccessCount = new System.Windows.Forms.Label();
			this.btnStartStop = new System.Windows.Forms.Button();
			this.lblFailCount = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lblSuccessCount
			// 
			this.lblSuccessCount.AutoSize = true;
			this.lblSuccessCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
			this.lblSuccessCount.ForeColor = System.Drawing.Color.Green;
			this.lblSuccessCount.Location = new System.Drawing.Point(24, 9);
			this.lblSuccessCount.Name = "lblSuccessCount";
			this.lblSuccessCount.Size = new System.Drawing.Size(17, 17);
			this.lblSuccessCount.TabIndex = 0;
			this.lblSuccessCount.Text = "0";
			// 
			// btnStartStop
			// 
			this.btnStartStop.Location = new System.Drawing.Point(27, 67);
			this.btnStartStop.Name = "btnStartStop";
			this.btnStartStop.Size = new System.Drawing.Size(75, 34);
			this.btnStartStop.TabIndex = 1;
			this.btnStartStop.Text = "Start";
			this.btnStartStop.UseVisualStyleBackColor = true;
			this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
			// 
			// lblFailCount
			// 
			this.lblFailCount.AutoSize = true;
			this.lblFailCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
			this.lblFailCount.ForeColor = System.Drawing.Color.Red;
			this.lblFailCount.Location = new System.Drawing.Point(24, 36);
			this.lblFailCount.Name = "lblFailCount";
			this.lblFailCount.Size = new System.Drawing.Size(17, 17);
			this.lblFailCount.TabIndex = 2;
			this.lblFailCount.Text = "0";
			// 
			// TransactionsGenerator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(132, 125);
			this.Controls.Add(this.lblFailCount);
			this.Controls.Add(this.btnStartStop);
			this.Controls.Add(this.lblSuccessCount);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "TransactionsGenerator";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "TransactionsGenerator";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblSuccessCount;
		private System.Windows.Forms.Button btnStartStop;
		private System.Windows.Forms.Label lblFailCount;
	}
}