﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Netpay.Bll;
using Netpay.Bll.Reports;

namespace AdminUtils
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
			Enabled = false;
			
			Login login = new Login();
			login.Show(this);
			login.BringToFront();

		}
		
		private bool _isLoggedin = false;
		private Guid _credentialsToken = Guid.Empty;

		public Guid CredentialsToken
		{
			get { return _credentialsToken; }
			set { _credentialsToken = value; }
		}
		
		internal bool IsLoggedin
		{
			get 
			{
				return _isLoggedin;
			}
			set
			{
				this.Enabled = _isLoggedin = value;
			}
		}

		private void btnExecutePackage_Click(object sender, EventArgs e)
		{
			//ExecutePackage ep = new ExecutePackage();
			//ep.Show(this);
		}

		private void btnEncrypt_Click(object sender, EventArgs e)
		{
			Encryption en = new Encryption();
			en.Show(this);
		}

		private void btnCreateExcells_Click(object sender, EventArgs e)
		{
			FiledReports.CreateAdminDailyRiskByMerchantReport();
			FiledReports.CreateAdminDailyRiskByTerminalReport();
			FiledReports.CreateAdminDailyStatusByMerchantReport();
			FiledReports.CreateAdminDailyStatusByTerminalReport();
			
			MessageBox.Show("Done!");
		}

		private void btnTransactionsGenerator_Click(object sender, EventArgs e)
		{
			TransactionsGenerator tg = new TransactionsGenerator();
			tg.Show(this);
		}

		private void btnCopyHPPImages_Click(object sender, EventArgs e)
		{
			var frmDir = new SourceTarget();
			if (frmDir.ShowDialog(this) != System.Windows.Forms.DialogResult.OK) return;
			var srcDir = frmDir.SourceText;
			var domian = Netpay.Infrastructure.Domain.Get(frmDir.DomainHost);
			var reader = Netpay.Dal.DataAccess.ADO.ExecReader("Select c.ID, LogoPath, CustomerNumber From tblCompanySettingsHosted csh Left Join tblCompany c ON(c.ID = csh.CompanyID) Where LogoPath <> ''", domian.Sql1ConnectionString);
			while (reader.Read()){
				string srcFileName = System.IO.Path.Combine(srcDir, (string) reader["LogoPath"]);
				if (System.IO.File.Exists(srcFileName)) {
					string fileExt = System.IO.Path.GetExtension(srcFileName);
					string dstFileName = "UIServices/HP_Logo" + fileExt;
                    string merchantFile = Netpay.Bll.Accounts.Account.MapPublicPath((string)reader["CustomerNumber"], dstFileName);
					if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(merchantFile)))
						System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(merchantFile));
					else if (System.IO.File.Exists(merchantFile)) System.IO.File.Delete(merchantFile);
					System.IO.File.Copy(srcFileName, merchantFile);
					Netpay.Dal.DataAccess.ADO.ExecQuery(string.Format("Update tblCompanySettingsHosted Set LogoPath='{0}' Where CompanyID={1}", dstFileName, reader["ID"]), domian.Sql1ConnectionString);
				}
			}
			reader.Close();
		}

		private void btnCopyPPImages_Click(object sender, EventArgs e)
		{
			var frmDir = new SourceTarget();
			if (frmDir.ShowDialog(this) != System.Windows.Forms.DialogResult.OK) return;
			var srcDir = frmDir.SourceText;
			var domian = Netpay.Infrastructure.Domain.Get(frmDir.DomainHost);
			var reader = Netpay.Dal.DataAccess.ADO.ExecReader("Select ppco.id, companyID, CustomerNumber, ImageFileName From tblPublicPayChargeOptions ppco Left Join tblCompany c ON(c.ID = ppco.CompanyID) Where ImageFileName <> ''", domian.Sql1ConnectionString);
			while (reader.Read())
			{
				string srcFileName = System.IO.Path.Combine(srcDir, (string)reader["ImageFileName"]);
				if (System.IO.File.Exists(srcFileName)) {
					string fileExt = System.IO.Path.GetExtension(srcFileName);
					string dstFileName = "UIServices/PP_" + DateTime.Now.Ticks + fileExt;
                    string merchantFile = Netpay.Bll.Accounts.Account.MapPublicPath((string)reader["CustomerNumber"], dstFileName);
					if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(merchantFile)))
						System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(merchantFile));
					else if (System.IO.File.Exists(merchantFile)) System.IO.File.Delete(merchantFile);
					System.IO.File.Copy(srcFileName, merchantFile);
					Netpay.Dal.DataAccess.ADO.ExecQuery(string.Format("Update tblPublicPayChargeOptions Set ImageFileName='{0}' Where id={1}", dstFileName, reader["ID"]), domian.Sql1ConnectionString);
				}
			}
			reader.Close();
		}

		private void btnCopyPPBanners_Click(object sender, EventArgs e)
		{
			var frmDir = new SourceTarget();
			if (frmDir.ShowDialog(this) != System.Windows.Forms.DialogResult.OK) return;
			var srcDir = frmDir.SourceText;
			var domian = Netpay.Infrastructure.Domain.Get(frmDir.DomainHost);
			var reader = Netpay.Dal.DataAccess.ADO.ExecReader("Select ppco.id, MerchantID, CustomerNumber, BannerImageName From tblPublicPayOptions ppco Left Join tblCompany c ON(c.ID = ppco.MerchantID)", domian.Sql1ConnectionString);
			while (reader.Read())
			{
				string srcFileName = System.IO.Path.Combine(srcDir, reader["MerchantID"] + "/promotionBanner.png");
				if (System.IO.File.Exists(srcFileName))
				{
					string fileExt = System.IO.Path.GetExtension(srcFileName);
					string dstFileName = "UIServices/PP_Banner" + fileExt;
                    string merchantFile = Netpay.Bll.Accounts.Account.MapPublicPath((string)reader["CustomerNumber"], dstFileName);
					if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(merchantFile)))
						System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(merchantFile));
					else if (System.IO.File.Exists(merchantFile)) System.IO.File.Delete(merchantFile);
					System.IO.File.Copy(srcFileName, merchantFile);
					Netpay.Dal.DataAccess.ADO.ExecQuery(string.Format("Update tblPublicPayOptions Set BannerImageName='{0}' Where id={1}", dstFileName, reader["ID"]), domian.Sql1ConnectionString);
				}
			}
			reader.Close();
		}

        private void btnRemapData_Click(object sender, EventArgs e)
        {
            //new RemapDataPath().ShowDialog(this);
        }

	}
}
