﻿namespace AdminUtils
{
	partial class Encryption
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Encryption));
			this.txtSource = new System.Windows.Forms.TextBox();
			this.btnEncrypt = new System.Windows.Forms.Button();
			this.txtDestination = new System.Windows.Forms.TextBox();
			this.btnDecrypt = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txtSource
			// 
			this.txtSource.Location = new System.Drawing.Point(16, 15);
			this.txtSource.Margin = new System.Windows.Forms.Padding(4);
			this.txtSource.Name = "txtSource";
			this.txtSource.Size = new System.Drawing.Size(356, 22);
			this.txtSource.TabIndex = 0;
			// 
			// btnEncrypt
			// 
			this.btnEncrypt.Location = new System.Drawing.Point(63, 98);
			this.btnEncrypt.Margin = new System.Windows.Forms.Padding(4);
			this.btnEncrypt.Name = "btnEncrypt";
			this.btnEncrypt.Size = new System.Drawing.Size(100, 28);
			this.btnEncrypt.TabIndex = 1;
			this.btnEncrypt.Text = "Encrypt";
			this.btnEncrypt.UseVisualStyleBackColor = true;
			this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
			// 
			// txtDestination
			// 
			this.txtDestination.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtDestination.Location = new System.Drawing.Point(16, 48);
			this.txtDestination.Margin = new System.Windows.Forms.Padding(4);
			this.txtDestination.Name = "txtDestination";
			this.txtDestination.ReadOnly = true;
			this.txtDestination.Size = new System.Drawing.Size(356, 23);
			this.txtDestination.TabIndex = 2;
			// 
			// btnDecrypt
			// 
			this.btnDecrypt.Location = new System.Drawing.Point(207, 98);
			this.btnDecrypt.Name = "btnDecrypt";
			this.btnDecrypt.Size = new System.Drawing.Size(100, 28);
			this.btnDecrypt.TabIndex = 3;
			this.btnDecrypt.Text = "Decrypt";
			this.btnDecrypt.UseVisualStyleBackColor = true;
			this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
			// 
			// Encryption
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(389, 145);
			this.Controls.Add(this.btnDecrypt);
			this.Controls.Add(this.txtDestination);
			this.Controls.Add(this.btnEncrypt);
			this.Controls.Add(this.txtSource);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "Encryption";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Encryption";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtSource;
		private System.Windows.Forms.Button btnEncrypt;
		private System.Windows.Forms.TextBox txtDestination;
		private System.Windows.Forms.Button btnDecrypt;
	}
}