﻿namespace AdminUtils
{
	partial class ExecutePackage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExecutePackage));
			this.btnExecutePackage = new System.Windows.Forms.Button();
			this.cbPackages = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// btnExecutePackage
			// 
			this.btnExecutePackage.Location = new System.Drawing.Point(387, 25);
			this.btnExecutePackage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.btnExecutePackage.Name = "btnExecutePackage";
			this.btnExecutePackage.Size = new System.Drawing.Size(100, 28);
			this.btnExecutePackage.TabIndex = 1;
			this.btnExecutePackage.Text = "Execute";
			this.btnExecutePackage.UseVisualStyleBackColor = true;
			this.btnExecutePackage.Click += new System.EventHandler(this.btnExecutePackage_Click);
			// 
			// cbPackages
			// 
			this.cbPackages.FormattingEnabled = true;
			this.cbPackages.Location = new System.Drawing.Point(29, 25);
			this.cbPackages.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.cbPackages.Name = "cbPackages";
			this.cbPackages.Size = new System.Drawing.Size(333, 24);
			this.cbPackages.TabIndex = 2;
			// 
			// ExecutePackage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(519, 76);
			this.Controls.Add(this.cbPackages);
			this.Controls.Add(this.btnExecutePackage);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.MaximizeBox = false;
			this.Name = "ExecutePackage";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Execute Package";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnExecutePackage;
		private System.Windows.Forms.ComboBox cbPackages;
	}
}