﻿namespace AdminUtils
{
    partial class RemapDataPath
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNPProcess = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAdminData = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNasData = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTarget = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.chkDeleteFiles = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtNPProcess
            // 
            this.txtNPProcess.Location = new System.Drawing.Point(41, 44);
            this.txtNPProcess.Name = "txtNPProcess";
            this.txtNPProcess.Size = new System.Drawing.Size(325, 20);
            this.txtNPProcess.TabIndex = 1;
            this.txtNPProcess.Text = "\\\\Nas\\Data\\MainWebData\\NPProcessing\\Data";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Process data folder";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Private admincash content";
            // 
            // txtAdminData
            // 
            this.txtAdminData.Location = new System.Drawing.Point(40, 93);
            this.txtAdminData.Name = "txtAdminData";
            this.txtAdminData.Size = new System.Drawing.Size(325, 20);
            this.txtAdminData.TabIndex = 3;
            this.txtAdminData.Text = "D:\\MainWebdata\\DataNetpay\\";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nas Data Folder";
            // 
            // txtNasData
            // 
            this.txtNasData.Location = new System.Drawing.Point(39, 144);
            this.txtNasData.Name = "txtNasData";
            this.txtNasData.Size = new System.Drawing.Size(325, 20);
            this.txtNasData.TabIndex = 5;
            this.txtNasData.Text = "\\\\nas\\Data\\FilesNetpay";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.Location = new System.Drawing.Point(34, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Target Data Folder";
            // 
            // txtTarget
            // 
            this.txtTarget.Location = new System.Drawing.Point(38, 200);
            this.txtTarget.Name = "txtTarget";
            this.txtTarget.Size = new System.Drawing.Size(325, 20);
            this.txtTarget.TabIndex = 7;
            this.txtTarget.Text = "\\\\nas\\Data\\FilesNetpay New";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(153, 264);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(129, 32);
            this.btnStart.TabIndex = 8;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // chkDeleteFiles
            // 
            this.chkDeleteFiles.AutoSize = true;
            this.chkDeleteFiles.Location = new System.Drawing.Point(38, 226);
            this.chkDeleteFiles.Name = "chkDeleteFiles";
            this.chkDeleteFiles.Size = new System.Drawing.Size(95, 17);
            this.chkDeleteFiles.TabIndex = 9;
            this.chkDeleteFiles.Text = "Delete All Files";
            this.chkDeleteFiles.UseVisualStyleBackColor = true;
            // 
            // RemapDataPath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 308);
            this.Controls.Add(this.chkDeleteFiles);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTarget);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNasData);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAdminData);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNPProcess);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RemapDataPath";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RemapDataPath";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNPProcess;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAdminData;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNasData;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTarget;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.CheckBox chkDeleteFiles;
    }
}