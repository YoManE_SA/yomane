﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Web;
using System.Collections.Specialized;
using System.Reflection;

namespace AdminUtils
{
	public partial class TransactionsGenerator : Form
	{
		public TransactionsGenerator()
		{
			InitializeComponent();
		}

		private delegate void InvocationDelegate();
		private bool _run = false;
		private	int successCount = 0;
		private	int failCount = 0;

		private void Run()
		{
			while (_run)
			{
				if ((successCount + failCount) > 3000000)
					_run = false;
				
				Random random = new Random();
				int amount = random.Next(1, 100 + 1);
				int currency = 7; //random.Next(0, 7 + 1);

				WebRequest request = WebRequest.Create("http://192.168.5.11/member/remote_charge.asp?cardnum=4580000000000000&amount=" + amount.ToString() + "&currency="+ currency.ToString() +"&companynum=5722306&transtype=0&typecredit=1&expmonth=9&expyear=2011&cvv2=123&phonenumber=%2B0%28%291234567&email=tamir@netpay-intl.com&member=tamir%20gerber&BillingZipCode=12345&billingcountry=IL&BillingAddress1=123%20MyStreet&BillingAddress2=3rd%20floor&BillingCity=Ramat%20Gan&PersonalNum=123123123");
				WebResponse response = request.GetResponse();
				string responseText = new StreamReader(response.GetResponseStream()).ReadToEnd();
				NameValueCollection responseParams = HttpUtility.ParseQueryString(responseText);

				if (responseParams["Reply"] == "000")
				{
					successCount++;
					SetControlPropertyValue(lblSuccessCount, "Text", successCount.ToString());
				}
				else
				{
					failCount++;
					SetControlPropertyValue(lblFailCount, "Text", failCount.ToString());
				}	 
			}	
		}

		private void btnStartStop_Click(object sender, EventArgs e)
		{
			if (_run)
			{
				_run = false;
				btnStartStop.Text = "Start";	
			}
			else
			{
				_run = true;
				btnStartStop.Text = "Stop";	
				InvocationDelegate invocationDelegate = Run;
				invocationDelegate.BeginInvoke(null, null);					
			}
		}

		delegate void SetControlValueCallback(Control oControl, string propName, object propValue);
		private void SetControlPropertyValue(Control oControl, string propName, object propValue)
		{
			if (oControl.InvokeRequired)
			{
				SetControlValueCallback d = new SetControlValueCallback(SetControlPropertyValue);
				oControl.Invoke(d, new object[] { oControl, propName, propValue });
			}
			else
			{
				Type t = oControl.GetType();
				PropertyInfo[] props = t.GetProperties();
				foreach (PropertyInfo p in props)
				{
					if (p.Name.ToUpper() == propName.ToUpper())
					{
						p.SetValue(oControl, propValue, null);
					}
				}
			}
		}

	}
}
