﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Dal.DataAccess;
using System.IO;
using System.Net.Mime;

namespace Netpay.Monitoring
{
	public static class EpaMonitoring
	{
        /*
		public static void SendBnsTransactionsWithoutEpaReport()
		{
            foreach (Domain domain in Domain.Domains.Values)
            {
                try
                {
                    Domain.Current = domain;
                    if (!Infrastructure.Tasks.Lock.TryLock("SendBnsTransactionsWithoutEpaReport", DateTime.Now.AddMinutes(-15)))
                        return;

                    ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
                    DateTime start = DateTime.Now;
                    MemoryStream memoryStream = new MemoryStream();
                    StreamWriter memoryWriter = new StreamWriter(memoryStream);

                    System.Data.SqlClient.SqlDataReader drResults = ADO.ExecReader("EXEC GetUnsentBnsTransactionsWithoutEpa", Domain.Current.Sql1ConnectionString, 600);
                    for (var i = 0; i < drResults.FieldCount - 2; i++) memoryWriter.Write(drResults.GetName(i) + ",");
                    memoryWriter.WriteLine(drResults.GetName(drResults.FieldCount - 1));
                    while (drResults.Read())
                    {
                        for (var i = 0; i < drResults.FieldCount - 2; i++) memoryWriter.Write(ADO.EmptyIfDbNull(drResults[i]) + ",");
                        memoryWriter.WriteLine(ADO.EmptyIfDbNull(drResults[drResults.FieldCount - 1]));
                    }
                    drResults.Close();

                    MailMessage message = new MailMessage();
                    message.Body = "BNS Transactions Without EPA Report";
                    message.From = new MailAddress(Infrastructure.Application.MailAddressFrom, "Netpay Monitoring");
                    message.IsBodyHtml = false;
                    message.Priority = MailPriority.Low;
                    message.Subject = "BNS Transactions Without EPA Report";
                    memoryWriter.Flush();
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    Attachment attachment = new Attachment(memoryStream, "BnsTransactionWithoutEpa_" + start.ToString("yyyyMMdd") + ".csv", "text/csv");
                    message.Attachments.Add(attachment);
                    var domainRecipients = domain.BnsTransactionsWithoutEpaReportRecipients.Split(',');
                    foreach (string currentRecipient in domainRecipients)
                        message.To.Add(currentRecipient);
                    try
                    {
                        Netpay.Infrastructure.Email.SmtpClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(LogTag.Monitoring, ex);
                        ADO.ExecQuery("EXEC DeleteLogBnsTransactionsWithoutEpaLast", Domain.Current.Sql1ConnectionString);
                    }

                    TimeSpan duration = DateTime.Now - start;
                    Logger.Log(LogSeverity.Info, LogTag.Monitoring, string.Format("BNS transactions without EPA report sent for domain '{0}'. Process took {1}", Domain.Current.Host, duration.ToString()));
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    Infrastructure.Tasks.Lock.Release("SendBnsTransactionsWithoutEpaReport");
                }
            }
		}
        */
	}
}