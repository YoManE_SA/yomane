﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Dal.DataAccess;
using System.IO;
using System.Net.Mime;

namespace Netpay.Monitoring
{
    public static class MerchantMonitoring
    {      
        public static void SendActiveMerchantReport() 
        {
            foreach (Domain domain in Domain.Domains.Values)
            {
                try
                {
                    Domain.Current = domain;
                    if (!Infrastructure.Tasks.Lock.TryLock("ActiveMerchantReport", DateTime.Now.AddMinutes(-15)))
                        return;

                    ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
                    DateTime processStart = DateTime.Now;
                    DateTime reportStart = DateTime.Now.AddDays(-7);
                    ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
                    var results = from dt in dc.DenormalizedTransactions where dt.TransactionDate > reportStart && dt.AcquiringBankID != 1 group dt by new { MerchantCompanyPhone = dt.MerchantCompanyPhone, MerchantPhone = dt.MerchantPhone, MerchantSupportPhone = dt.MerchantSupportPhone, MerchantSupportEmail = dt.MerchantSupportEmail, MerchantAlertEmail = dt.MerchantAlertEmail, MerchantContactEmail = dt.MerchantContactEmail, AcquiringBankName = dt.AcquiringBankName, AcquiringBankID = dt.AcquiringBankID, MerchantID = dt.CompanyID, MerchantName = dt.CompanyName, MerchantEmail = dt.CompanyEmail } into g orderby g.Count() descending select g;

                    // buils message
                    MailMessage message = new MailMessage();
                    message.Body = "Active Merchant Report";
                    message.From = new MailAddress(Infrastructure.Application.MailAddressFrom, "Netpay Monitoring");
                    message.IsBodyHtml = false;
                    message.Priority = MailPriority.Low;
                    message.Subject = "Active Merchant Report";
                    var domainRecipients = domain.ActiveMerchantReportRecipients.Split(',');
                    foreach (string currentRecipient in domainRecipients)
                        message.To.Add(currentRecipient);

                    // add attachment & send
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (StreamWriter memoryWriter = new StreamWriter(memoryStream))
                        {
                            memoryWriter.WriteLine("<html xmlns='http://www.w3.org/TR/REC-html40'><head><meta http-equiv=Content-Type content='text/html; charset=utf-8'><style> .decimal{mso-number-format:\"\\#\\#0\\.00\";} BODY,DIV,TABLE,THEAD,TBODY,TFOOT,TR,TH,TD,P { font-family:'arial'; font-size:x-small; text-align: left; } </style></head><body><table border='1'>");
                            memoryWriter.WriteLine("<COLGROUP><COL WIDTH=68><COL WIDTH=149><COL WIDTH=106><COL WIDTH=129><COL WIDTH=186><COL WIDTH=183><COL WIDTH=116><COL WIDTH=173><COL WIDTH=186><COL WIDTH=186><COL WIDTH=186></COLGROUP>");
                            memoryWriter.WriteLine(string.Format("<tr><td colspan='11'><font size='4'>Netpay active merchants report for domain '{0}'.<font></td></tr>", domain));
                            memoryWriter.WriteLine(string.Format("<tr><td colspan=\"11\">From {0} to {1}.</td></tr>", reportStart, DateTime.Now.AddDays(-1)));
                            memoryWriter.WriteLine("<tr><td colspan='11'>&nbsp;</td></tr>");
                            memoryWriter.WriteLine("<tr><th height='20' align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Acquiring Bank ID</th><th align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Acquiring Bank Name</th><th align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Merchant ID</th><th align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Merchant Name</th><th align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Merchant Email</th><th align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Merchant Company Phone</th><th align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Merchant Phone</th><th align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Merchant Support Phone</th><th align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Merchant Support Email</th><th align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Merchant Alert Email</th><th align='left' bgcolor='#ccffcc' style='border-bottom: 1px solid #000000' width='200px'>Merchannt Contact Email</th></tr>");
                            foreach (var currentResult in results)
                                memoryWriter.WriteLine(string.Format("<tr><td height='20' align='left'>{0}</td><td align='left'>{1}</td><td align='left'>{2}</td><td align='left'>{3}</td><td align='left'>{4}</td><td align='left'>{5}</td><td align='left'>{6}</td><td align='left'>{7}</td><td align='left'>{8}</td><td align='left'>{9}</td><td align='left'>{10}</td></tr>", currentResult.Key.AcquiringBankID, currentResult.Key.AcquiringBankName, currentResult.Key.MerchantID, currentResult.Key.MerchantName, currentResult.Key.MerchantEmail, currentResult.Key.MerchantCompanyPhone, currentResult.Key.MerchantPhone, currentResult.Key.MerchantSupportPhone, currentResult.Key.MerchantSupportEmail, currentResult.Key.MerchantAlertEmail, currentResult.Key.MerchantContactEmail));
                            memoryWriter.WriteLine("</table><body><html>");
                            memoryWriter.Flush();
                            memoryStream.Seek(0, SeekOrigin.Begin);

                            // ================================== debug
                            /*
                            FileStream fs = new FileStream(@"C:\Netpay\Files\ActiveMrchantReport.xls", FileMode.Create, FileAccess.Write);
                            memoryStream.WriteTo(fs);
                            fs.Dispose();
                            return;
                             */
                            //=========================================

                            Attachment attachment = new Attachment(memoryStream, "ActiveMrchantReport.xls", "text/csv");
                            message.Attachments.Add(attachment);
                            Netpay.Infrastructure.Email.SmtpClient.Send(message);
                        }
                    }

                    TimeSpan duration = DateTime.Now - processStart;
                    Logger.Log(LogSeverity.Info, LogTag.Monitoring, string.Format("Active merchants report sent for domain '{0}'. Process took {1}", Domain.Current.Host, duration.ToString()));
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    Infrastructure.Tasks.Lock.Release("ActiveMerchantReport");
                }
            }
        }
    }
}
