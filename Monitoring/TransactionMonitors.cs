﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;

namespace Netpay.Monitoring
{
	public static class TransactionMonitors
	{
		private static DateTime _feesMonitorLastCheck = DateTime.Now.AddDays(-1);
		private static DateTime _installmentsMonitorLastCheck = DateTime.Now.AddDays(-1);
		private static DateTime _recurringMonitorLastCheck = DateTime.Now.AddDays(-1);
		private static TimeSpan _offset = new TimeSpan(1, 0, 0);
		
		public static void FeesMonitor()
		{
            foreach (Domain domain in Domain.Domains.Values)
            {
                try
                {
                    Domain.Current = domain;
                    if (!Infrastructure.Tasks.Lock.TryLock("FeesMonitor", DateTime.Now.AddMinutes(-15)))
                        return;
                    
                    ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
                    NetpayDataContext dc = new NetpayDataContext(Domain.Current.Sql1ConnectionString);
                    DateTime start = _feesMonitorLastCheck - _offset;
                    DateTime end = DateTime.Now - _offset;
                    int[] feesAmountTypes = new int[] { }; //currentDomain.Cache.GetTransactionAmountGroup(TransactionAmountGroup.Fees).Select(t => (int)t).ToArray();
                    int[] newTransactions = (from ta in dc.tblTransactionAmounts where ta.InsertDate >= _feesMonitorLastCheck select ta.ID).ToArray();
                    int[] newTransactionsWithFees = (from ta in dc.tblTransactionAmounts where ta.InsertDate >= start && ta.InsertDate <= end && feesAmountTypes.Contains(ta.TypeID) select ta.ID).ToArray();
                    int[] newTransactionsWithoutFees = newTransactions.Except(newTransactionsWithFees).ToArray();

                    Logger.Log(LogSeverity.Info, LogTag.Monitoring, string.Format("Monitoring fees. Time frame: {0} - {1}", start, end));
                    if (newTransactionsWithoutFees.Length > 0)
                        Logger.Log(LogSeverity.Error, LogTag.Monitoring, string.Format("{0} transactions without fees found. Time frame: {1} - {2}", newTransactionsWithoutFees.Length, _feesMonitorLastCheck, DateTime.Now));

                    _feesMonitorLastCheck = DateTime.Now;
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    Infrastructure.Tasks.Lock.Release("FeesMonitor");
                }

            }
		}

		public static void InstallmentsMonitor()
		{
            foreach (Domain domain in Domain.Domains.Values)
            {
                try
                {
                    Domain.Current = domain;
                    if (!Infrastructure.Tasks.Lock.TryLock("InstallmentsMonitor", DateTime.Now.AddMinutes(-15)))
                        return;

                    NetpayDataContext dc = new NetpayDataContext(Domain.Current.Sql1ConnectionString);
                    DateTime start = _feesMonitorLastCheck - _offset;
                    DateTime end = DateTime.Now - _offset;
                    var missmatch = (from transactions in dc.tblCompanyTransPasses
                                     join installments in dc.tblCompanyTransInstallments on transactions.ID equals installments.transAnsID into transactionGroup
                                     from defaultItem in transactionGroup.DefaultIfEmpty()
                                     where (transactions.Payments > 1 && transactions.InsertDate >= start && transactions.InsertDate <= end) && (defaultItem == null || transactionGroup.Count() != transactions.Payments)
                                     select new { transPassID = transactions.ID, transPassInstallmentsCount = transactions.Payments, installmentsTransPassID = defaultItem.transAnsID, installmentsCount = transactionGroup.Count() }).Distinct();

                    Logger.Log(LogSeverity.Info, LogTag.Monitoring, string.Format("Monitoring installments. Time frame: {0} - {1}", start, end));
                    int transactionsWithoutInstallments = missmatch.Where(m => m.installmentsTransPassID == null).Count();
                    int transactionsWithInstallmentsMissmathch = missmatch.Where(m => m.installmentsTransPassID != null).Count();
                    if (transactionsWithoutInstallments > 0)
                        Logger.Log(LogSeverity.Error, LogTag.Monitoring, string.Format("{0} transactions without installments found. Time frame: {1} - {2}", transactionsWithoutInstallments, _installmentsMonitorLastCheck, DateTime.Now));
                    if (transactionsWithInstallmentsMissmathch > 0)
                        Logger.Log(LogSeverity.Error, LogTag.Monitoring, string.Format("{0} transactions with wrong number of installments found. Time frame: {1} - {2}", transactionsWithInstallmentsMissmathch, _installmentsMonitorLastCheck, DateTime.Now));

                    _installmentsMonitorLastCheck = DateTime.Now;
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    Infrastructure.Tasks.Lock.Release("InstallmentsMonitor");
                }
            }
		}

		public static void RecurringMonitor()
		{
            foreach (Domain domain in Domain.Domains.Values)
            {
                try
                {
                    Domain.Current = domain;
                    if (!Infrastructure.Tasks.Lock.TryLock("RecurringMonitor", DateTime.Now.AddMinutes(-15)))
                        return;

                    NetpayDataContext dc = new NetpayDataContext(Domain.Current.Sql1ConnectionString);
                    DateTime start = _feesMonitorLastCheck - _offset;
                    DateTime end = DateTime.Now - _offset;
                    var missmatch = (from series in dc.tblRecurringSeries
                                     join charges in dc.tblRecurringCharges on series.ID equals charges.rc_Series into seriesGroup
                                     from defaultItem in seriesGroup.DefaultIfEmpty()
                                     where (series.rs_StartDate >= start && series.rs_StartDate <= end) && (defaultItem == null || seriesGroup.Count() != series.rs_ChargeCount)
                                     select new { seriesID = series.ID, seriesChargeCount = series.rs_ChargeCount, chrgesSeriesID = defaultItem.rc_Series, chargesCount = seriesGroup.Count() }).Distinct();

                    Logger.Log(LogSeverity.Info, LogTag.Monitoring, string.Format("Monitoring recurring. Time frame: {0} - {1}", start, end));
                    int seriesWithoutCharges = missmatch.Where(s => s.chrgesSeriesID == 0).Count();
                    if (seriesWithoutCharges > 0)
                        Logger.Log(LogSeverity.Error, LogTag.Monitoring, string.Format("{0} series without charges found. Time frame: {1} - {2}", seriesWithoutCharges, _recurringMonitorLastCheck, DateTime.Now));
                    int seriesWithChargeMissmatch = missmatch.Where(s => s.chargesCount != s.seriesChargeCount).Count();
                    if (seriesWithChargeMissmatch > 0)
                        Logger.Log(LogSeverity.Error, LogTag.Monitoring, string.Format("{0} series with wrong number of charges found. Time frame: {1} - {2}", seriesWithChargeMissmatch, _recurringMonitorLastCheck, DateTime.Now));

                    _installmentsMonitorLastCheck = DateTime.Now;
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    Infrastructure.Tasks.Lock.Release("RecurringMonitor");
                }
            }
		}
	}
}
