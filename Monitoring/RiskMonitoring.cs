﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Reports;
using Netpay.Infrastructure;
using System.Net.Mail;
using Netpay.CommonTypes;

namespace Netpay.Monitoring
{
    public static class RiskMonitoring
    {
        private class DeclineStatsResult
        {
            public string MerchantName { get; set; }
            public int MerchantID { get; set; }
            public int CapturedCount { get; set; }
            public int DeclinedCount { get; set; }
        }

        private class RetrievalRequests
        {
            public string MerchantName { get; set; }
            public int MerchantID { get; set; }
            public int PassedCount { get; set; }
            public int RetrievalCount { get; set; }
            public decimal Ratio
            {
                get
                {
                    return Netpay.Infrastructure.Math.GetPercentage(RetrievalCount, PassedCount);
                }
                set { }
            }
        }

        public static void SendIrregularityReport()
        {
            foreach (Domain domain in Domain.Domains.Values)
            {
                try
                {
                    Domain.Current = domain;
                    if (!Infrastructure.Tasks.Lock.TryLock("SendIrregularityReport", DateTime.Now.AddMinutes(-15)))
                        return;

                    ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
                    ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);

                    // limits
                    decimal maxRetreivelRequestPercent = 2;
                    decimal maxVisaChbPercent = 1;
                    int maxVisaChbCount = 100;
                    decimal maxMastercardChbPercent = 1;
                    int maxMastercardChbCount = 100;
                    int maxDeclinedRatio = 30;
                    decimal maxTransactionAmount = 50000;
                    int minTotalTransForChbPercentCheck = 50;

                    // time frame
                    DateTime yesterday = DateTime.Now.AddDays(-1).MaxTime();
                    DateTime yesterdayMonthStart = new DateTime(yesterday.Year, yesterday.Month, 1).MinTime();
                    if (!Infrastructure.Application.IsProduction)
                        yesterdayMonthStart = yesterdayMonthStart.AddYears(-2);

                    // retrieval requests data
                    string sql = "SELECT SUM(CASE WHEN DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsChargeback = 0 AND DenormalizedTransactions.IsRefund = 0 THEN 1 ELSE 0 END) AS [PassedCount], SUM(CASE WHEN IsRetrievalRequest = 1 THEN 1 ELSE 0 END) AS [RetrievalCount], [CompanyName] as [MerchantName], [CompanyID] as [MerchantID] from DenormalizedTransactions GROUP BY [CompanyName], [CompanyID]";
                    List<RetrievalRequests> resultsForRetrievalAlerts = ((IEnumerable<RetrievalRequests>)dc.ExecuteQuery(typeof(RetrievalRequests), sql, new object[] { })).ToList();

                    // chb data
                    List<DailyRiskReportByMerchant> resultsForCCAlerts = (from drrc in dc.DailyRiskReportByMerchants orderby drrc.CompanyName select drrc).ToList();

                    // declined data
                    sql = string.Format("SELECT [CompanyName] AS [MerchantName], [CompanyID] AS [MerchantID], SUM(CASE WHEN (TransactionStatus = 'Captured') THEN 1 ELSE 0 END) AS [CapturedCount], SUM(CASE WHEN (TransactionStatus = 'Declined') THEN 1 ELSE 0 END) AS [DeclinedCount] FROM [DenormalizedTransactions] WHERE [MerchantStatusID] = 30 AND IsGateway = 0 AND CAST(TransactionDate AS DATE) >= CAST('{0}' AS DATE) AND CAST(TransactionDate AS DATE) <= CAST('{1}' AS DATE) GROUP BY [CompanyName], [CompanyID] ORDER BY [CapturedCount] DESC, [DeclinedCount] DESC", yesterdayMonthStart.ToSql(), yesterday.ToSql());
                    List<DeclineStatsResult> resultsForDeclinedAlerts = ((IEnumerable<DeclineStatsResult>)dc.ExecuteQuery(typeof(DeclineStatsResult), sql, new object[] { })).ToList();

                    // amounts data
                    List<DenormalizedTransaction> resultsForAmountAlerts = (from dt in dc.DenormalizedTransactions where dt.MerchantStatusID == (int)MerchantStatus.Processing && !dt.IsGateway.GetValueOrDefault() && dt.TransactionDate >= yesterdayMonthStart && dt.TransactionDate <= yesterday && (dt.TransactionStatus == TransactionStatus.Captured.ToString() || dt.TransactionStatus == TransactionStatus.Authorized.ToString()) && dt.PaymentMethodID > (int)PaymentMethodEnum.MaxInternal && dt.TransactionAmountUSD > maxTransactionAmount select dt).ToList();
                    resultsForAmountAlerts = resultsForAmountAlerts.OrderByDescending(r => r.TransactionAmountUSD).ToList();

                    // process count change by country data
                    int releventMinPercentageIncrease = 50;
                    int relevantMinPrevWeekCount = 500;
                    var prevDayTrnasByCountry = (from dt in dc.DenormalizedTransactions
                                                 where dt.TransactionDate.Value.Date == DateTime.Now.AddDays(-1).Date
                                                 group dt by new { dt.CompanyID, dt.CompanyName, dt.BinCountry } into g
                                                 select new { merchantID = g.Key.CompanyID, merchantName = g.Key.CompanyName, countryCode = g.Key.BinCountry, count = g.Count() }).ToList();
                    var prevWeekTransByCountry = (from dt in dc.DenormalizedTransactions
                                                  where dt.TransactionDate.Value.Date == DateTime.Now.AddDays(-8).Date
                                                  group dt by new { dt.CompanyID, dt.CompanyName, dt.BinCountry } into g
                                                  select new { merchantID = g.Key.CompanyID, merchantName = g.Key.CompanyName, countryCode = g.Key.BinCountry, count = g.Count() }).ToList();
                    var transByCountryJoin = (from prevDaySum in prevDayTrnasByCountry
                                              join prevWeekSum in prevWeekTransByCountry on new { merchantID = prevDaySum.merchantID, country = prevDaySum.countryCode } equals new { merchantID = prevWeekSum.merchantID, country = prevWeekSum.countryCode }
                                              select new { merchantID = prevDaySum.merchantID, merchantName = prevDaySum.merchantName, countryCode = prevDaySum.countryCode, prevDayCount = prevDaySum.count, prevWeekCount = prevWeekSum.count, percent = Netpay.Infrastructure.Math.GetPercentageDifference(prevDaySum.count, prevWeekSum.count) });
                    var transByCountryRelevant = transByCountryJoin.Where(s => s.percent >= releventMinPercentageIncrease && s.prevWeekCount >= relevantMinPrevWeekCount);

                    // iterate recipients
                    var recipients = Infrastructure.Security.AdminUser.Search(null, null);
                    var domainRecipients = domain.IrregularityReportRecipients.Split(',');
                    foreach (string currentRecipientEmail in domainRecipients)
                    {
                        var currentRecipient = recipients.Where(r => (r.Login != null) && (r.Login.EmailAddress.EmptyIfNull().Trim().ToLower() == currentRecipientEmail.Trim().ToLower())).SingleOrDefault();
                        if (currentRecipient == null)
                        {
                            string loadedSecurityUsers = recipients.Select(r => r.Login.EmailAddress).Aggregate((current, next) => current + "\n" + next);
                            Logger.Log(LogSeverity.Error, LogTag.Monitoring, "Security user with the email '" + currentRecipientEmail + "' was not found for risk alerts.", "Loaded emails: \n" + loadedSecurityUsers);
                            continue;
                        }
                        var allowedMerchants = Bll.Accounts.AccountFilter.LoadForAdminUser(currentRecipient.ID).FilterIDList(Bll.Accounts.AccountType.Merchant, null);

                        List<string> visaChbPercentAlerts = new List<string>();
                        List<string> visaChbCountAlers = new List<string>();
                        List<string> mastercardChbPercentAlerts = new List<string>();
                        List<string> mastercardChbCountAlers = new List<string>();
                        List<string> declineRatioAlers = new List<string>();
                        List<string> amountAlers = new List<string>();
                        List<string> transByCountryAlers = new List<string>();
                        List<string> retrievalAlers = new List<string>();

                        // retrievals
                        foreach (RetrievalRequests retrievalRequest in resultsForRetrievalAlerts)
                            if (retrievalRequest.Ratio > maxRetreivelRequestPercent)
                                retrievalAlers.Add(string.Format("<tr><td>{0} ({1})</td> <td>%{2} ({3}/{4})</td></tr>", retrievalRequest.MerchantName, retrievalRequest.MerchantID, retrievalRequest.Ratio.ToString(Infrastructure.Application.AmountFormat), retrievalRequest.PassedCount, retrievalRequest.RetrievalCount));

                        // chb
                        var filteredResultsForCCAlerts = resultsForCCAlerts;
                        if (allowedMerchants.Count > 0)
                            filteredResultsForCCAlerts = filteredResultsForCCAlerts.Where(r => allowedMerchants.Contains(r.CompanyID.Value)).ToList();
                        var filteredResultsForCCAlertsPrepared = filteredResultsForCCAlerts.Select(d => new { data = d, visaChbPercent = Netpay.Infrastructure.Math.GetPercentage(d.VisaThisMonthChargebacksCount.GetValueOrDefault(), d.VisaThisMonthNotChargebacksCount.GetValueOrDefault()), mastercardChbPercent = Netpay.Infrastructure.Math.GetPercentage(d.MastercardThisMonthChargebacksCount.GetValueOrDefault(), d.MastercardLastMonthNotChargebacksCount.GetValueOrDefault()) });

                        // visa chb %
                        var visaChbPercent = filteredResultsForCCAlertsPrepared.Where(c => c.data.VisaThisMonthNotChargebacksCount.GetValueOrDefault() >= minTotalTransForChbPercentCheck && c.visaChbPercent > maxVisaChbPercent).OrderByDescending(d => d.visaChbPercent);
                        foreach (var item in visaChbPercent)
                            visaChbPercentAlerts.Add(string.Format("<tr><td>{0} ({1})</td> <td>%{2} ({3}/{4})</td></tr>", item.data.CompanyName, item.data.CompanyID, item.visaChbPercent.ToString(Infrastructure.Application.AmountFormat), item.data.VisaThisMonthChargebacksCount, item.data.VisaThisMonthNotChargebacksCount.GetValueOrDefault()));

                        // visa chb count
                        var visaChbCount = filteredResultsForCCAlertsPrepared.Where(c => c.data.VisaThisMonthChargebacksCount > maxVisaChbCount).OrderByDescending(c => c.data.VisaThisMonthChargebacksCount);
                        foreach (var item in visaChbCount)
                            visaChbCountAlers.Add(string.Format("<tr><td>{0} ({1})</td> <td>{1}</td></tr>", item.data.CompanyName, item.data.CompanyID, item.data.VisaThisMonthChargebacksCount));

                        // mastercard chb %
                        var mastercardChbPercent = filteredResultsForCCAlertsPrepared.Where(c => c.data.VisaThisMonthNotChargebacksCount.GetValueOrDefault() >= minTotalTransForChbPercentCheck && c.mastercardChbPercent > maxMastercardChbPercent).OrderByDescending(d => d.mastercardChbPercent);
                        foreach (var item in mastercardChbPercent)
                            mastercardChbPercentAlerts.Add(string.Format("<tr><td>{0} ({1})</td> <td>%{2} ({3}/{4})</td></tr>", item.data.CompanyName, item.data.CompanyID, item.mastercardChbPercent.ToString(Infrastructure.Application.AmountFormat), item.data.MastercardThisMonthChargebacksCount, item.data.MastercardLastMonthNotChargebacksCount.GetValueOrDefault()));

                        // mastercard chb count
                        var mastercardChbCount = filteredResultsForCCAlertsPrepared.Where(c => c.data.MastercardThisMonthChargebacksCount > maxMastercardChbCount).OrderByDescending(c => c.data.MastercardThisMonthChargebacksCount);
                        foreach (var item in mastercardChbCount)
                            mastercardChbCountAlers.Add(string.Format("<tr><td>{0} ({1})</td> <td>{2}</td></tr>", item.data.CompanyName, item.data.CompanyID, item.data.MastercardThisMonthChargebacksCount));

                        // declined ratio
                        var filteredResultsForDeclinedAlerts = resultsForDeclinedAlerts;
                        if (allowedMerchants.Count > 0)
                            filteredResultsForDeclinedAlerts = filteredResultsForDeclinedAlerts.Where(r => allowedMerchants.Contains(r.MerchantID)).ToList();
                        var filteredResultsForDeclinedAlertsPrepared = filteredResultsForDeclinedAlerts.Select(d => new { data = d, totalCount = d.DeclinedCount + d.CapturedCount, declinedRatio = Netpay.Infrastructure.Math.GetPercentage(d.DeclinedCount, d.DeclinedCount + d.CapturedCount) });
                        filteredResultsForDeclinedAlertsPrepared = filteredResultsForDeclinedAlertsPrepared.Where(d => d.totalCount > 100 && d.declinedRatio > maxDeclinedRatio).OrderByDescending(d => d.declinedRatio);
                        foreach (var item in filteredResultsForDeclinedAlertsPrepared)
                            declineRatioAlers.Add(string.Format("<tr><td>{0} ({1})</td> <td>%{2} ({3}/{4})</td></tr>", item.data.MerchantName, item.data.MerchantID, item.declinedRatio.ToString(Infrastructure.Application.AmountFormat), item.data.DeclinedCount, item.totalCount));

                        // amount limit
                        var filteredResultsForAmountAlerts = resultsForAmountAlerts;
                        if (allowedMerchants.Count > 0)
                            filteredResultsForAmountAlerts = filteredResultsForAmountAlerts.Where(r => allowedMerchants.Contains(r.CompanyID.Value)).ToList();
                        foreach (var currentResult in filteredResultsForAmountAlerts)
                        {
                            amountAlers.Add(string.Format("<tr><td>{0} ({1})</td> <td>transaction {2}, ${3}</td></tr>", currentResult.CompanyName, currentResult.CompanyID, currentResult.TransactionID, currentResult.TransactionAmountUSD));
                        }

                        // trans by country change
                        var filteredTransByCountryRelevant = transByCountryRelevant;
                        if (allowedMerchants.Count > 0)
                            filteredTransByCountryRelevant = filteredTransByCountryRelevant.Where(r => allowedMerchants.Contains(r.merchantID.Value)).OrderByDescending(r => r.percent); ;
                        foreach (var currentResult in filteredTransByCountryRelevant)
                        {
                            transByCountryAlers.Add(string.Format("<tr><td>{0} ({1}) {5}</td> <td>%{2} ({3}/{4})</td></tr>", currentResult.merchantName, currentResult.merchantID, currentResult.percent.ToString(Infrastructure.Application.AmountFormat), currentResult.prevDayCount, currentResult.prevWeekCount, currentResult.countryCode));
                        }

                        // build message
                        StringBuilder messageBuilder = new StringBuilder();
                        messageBuilder.Append("<table style=\"font-family: calibri; font-size: 16px;\">");
                        messageBuilder.AppendLine(string.Format("<tr><td colspan='2'><b>Netpay risk alerts for '{0}' at domain '{1}'.</b></td></tr>", currentRecipient.FullName, Domain.Current.Host));
                        messageBuilder.AppendLine(string.Format("<tr><td colspan='2'><b>From {0} to {1}.</b></td></tr>", yesterdayMonthStart.ToShortDateString(), yesterday.ToShortDateString()));
                        int riskCount = visaChbPercentAlerts.Count + visaChbCountAlers.Count + mastercardChbPercentAlerts.Count + mastercardChbCountAlers.Count + declineRatioAlers.Count + amountAlers.Count + transByCountryAlers.Count;
                        if (riskCount > 0)
                        {
                            messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                            if (visaChbPercentAlerts.Count > 0)
                            {
                                messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                                messageBuilder.AppendLine(string.Format("<tr><td colspan='2'><b>Visa CHB above %{0}</b></td></tr>", maxVisaChbPercent));
                                visaChbPercentAlerts.ForEach(s => messageBuilder.AppendLine(s));
                            }
                            if (visaChbCountAlers.Count > 0)
                            {
                                messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                                messageBuilder.AppendLine(string.Format("<tr><td colspan='2'><b>Visa CHB count above {0}</b></td></tr>", maxVisaChbCount));
                                visaChbCountAlers.ForEach(s => messageBuilder.AppendLine(s));
                            }
                            if (mastercardChbPercentAlerts.Count > 0)
                            {
                                messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                                messageBuilder.AppendLine(string.Format("<tr><td colspan='2'><b>Mastercard CHB above %{0}</b></td></tr>", maxMastercardChbPercent));
                                mastercardChbPercentAlerts.ForEach(s => messageBuilder.AppendLine(s));
                            }
                            if (mastercardChbCountAlers.Count > 0)
                            {
                                messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                                messageBuilder.AppendLine(string.Format("<tr><td colspan='2'><b>Mastercard CHB count above {0}</b></td></tr>", maxMastercardChbCount));
                                mastercardChbCountAlers.ForEach(s => messageBuilder.AppendLine(s));
                            }
                            if (declineRatioAlers.Count > 0)
                            {
                                messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                                messageBuilder.AppendLine(string.Format("<tr><td colspan='2'><b>Decline ratio above %{0}</b></td></tr>", maxDeclinedRatio));
                                declineRatioAlers.ForEach(s => messageBuilder.AppendLine(s));
                            }
                            if (amountAlers.Count > 0)
                            {
                                messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                                messageBuilder.AppendLine(string.Format("<tr><td colspan='2'><b>Amounts above ${0}</b></td></tr>", maxTransactionAmount));
                                amountAlers.ForEach(s => messageBuilder.AppendLine(s));
                            }
                            if (transByCountryAlers.Count > 0)
                            {
                                messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                                messageBuilder.AppendLine(string.Format("<tr><td colspan='2'><b>Trans. per country above %{0} increase since last week</b></td></tr>", releventMinPercentageIncrease));
                                transByCountryAlers.ForEach(s => messageBuilder.AppendLine(s));
                            }
                            if (retrievalAlers.Count > 0)
                            {
                                messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                                messageBuilder.AppendLine(string.Format("<tr><td colspan='2'><b>Retrieval requests ratio above %{0}</b></td></tr>", maxRetreivelRequestPercent));
                                retrievalAlers.ForEach(s => messageBuilder.AppendLine(s));
                            }
                        }
                        else
                        {
                            messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                            messageBuilder.AppendLine("<tr><td colspan='2'>&nbsp;</td></tr>");
                            messageBuilder.AppendLine("<tr><td colspan='2'>No risks found.</td></tr>");
                        }
                        messageBuilder.Append("</table>");

                        // send
                        MailMessage message = new MailMessage();
                        message.Body = messageBuilder.ToString();
                        message.From = new MailAddress(Infrastructure.Application.MailAddressFrom, "Netpay Monitoring");
                        message.IsBodyHtml = true;
                        message.Priority = MailPriority.High;
                        message.Subject = string.Format("Netpay risk alerts for domain '{0}'", Domain.Current.Host);
                        message.To.Add(currentRecipient.Login.EmailAddress);

                        //Console.WriteLine(message.Body);
                        Netpay.Infrastructure.Email.SmtpClient.Send(message);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    Infrastructure.Tasks.Lock.Release("SendIrregularityReport");
                }
            }
        }
    }
}
