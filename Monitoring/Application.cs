﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Tasks;

namespace Netpay.Monitoring
{
	public static class Application
	{
		private static TaskScheduler _scheduler = null;

		public static List<ScheduledTask> Tasks {get;set;}

		public static void Init() 
		{
            Netpay.Infrastructure.Application.Init("Monitoring");
			ConfigurationLoader.Load();

			// setup scheduler
			_scheduler = new TaskScheduler();
			_scheduler.Add(Tasks);
			_scheduler.Start();

			Logger.Log(LogSeverity.Info, LogTag.Monitoring, string.Format("Monitoring initialized, {0} task(s) loaded.", Tasks.Count));
		}
	}
}
