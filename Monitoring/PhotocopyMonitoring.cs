﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Dal.DataAccess;
using System.IO;
using System.Net.Mime;

namespace Netpay.Monitoring
{
    public static class PhotocopyMonitoring
    {
        public static void SendPhotocopyWithRefund()
        {
            foreach (Domain domain in Domain.Domains.Values)
            {
                try
                {
                    Domain.Current = domain;
                    if (!Infrastructure.Tasks.Lock.TryLock("SendPhotocopyWithRefund", DateTime.Now.AddMinutes(-15)))
                        return;

                    DateTime start = DateTime.Now;
                    ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
                    NetpayDataContext dc = new NetpayDataContext(Domain.Current.Sql1ConnectionString);
                    System.Data.SqlClient.SqlDataReader drResults = ADO.ExecReader("Select r.* From viewRetrivalAfterRefund r " +
                        "Inner Join tblDebitCompany d ON(d.DebitCompany_ID = r.DebitCompany) " +
                        "Where d.dc_IsNotifyRetReqAfterRefund = 1 And " +
                        "r.ORG_ID Not IN (Select th.TransPass_id From Trans.TransHistory th Where th.TransHistoryType_id = " + (int)Netpay.CommonTypes.TransactionHistoryType.DetectRetrivalReqAfterRefund + ")",
                        Domain.Current.Sql1ConnectionString, 600);
                    while (drResults.Read())
                    {
                        Currency cvo;
                        string valueFormat = "<b>{0}:</b> {1}<br/>";
                        StringWriter sw = new StringWriter();
                        sw.WriteLine("Hello,<br/><br/>" +
                            "We received a retrieval request for a transaction that was already refunded by the merchant.<br/>" +
                            "Below please find the transaction and refund details:<br/><br/>");
                        sw.WriteLine(string.Format(valueFormat, "Contract Number", drResults["dt_ContractNumber"]));
                        sw.WriteLine(string.Format(valueFormat, "Bank ExternalID", drResults["dt_BankExternalID"]));
                        sw.WriteLine(string.Format(valueFormat, "Bank CaseID", drResults["RR_BankCaseID"]));
                        sw.WriteLine(string.Format(valueFormat, "Card Dispaly", drResults["CardDispaly"]));
                        sw.WriteLine(string.Format(valueFormat, "Card Owner", drResults["CardOwner"]));

                        cvo = Currency.Get((int)drResults["ORG_Currency"]);
                        sw.WriteLine("<br/>");
                        sw.WriteLine(string.Format(valueFormat, "Original Transaction Date", drResults["ORG_Date"]));
                        if (cvo != null) sw.WriteLine(string.Format(valueFormat, "Original Transaction Currency", cvo.IsoCode));
                        sw.WriteLine(string.Format(valueFormat, "Original Transaction Amount", drResults["ORG_Amount"]));
                        sw.WriteLine(string.Format(valueFormat, "Original Transaction Approval", drResults["ORG_Approval"]));
                        sw.WriteLine(string.Format(valueFormat, "Original Transaction RefCode", drResults["ORG_RefCode"]));

                        cvo = Currency.Get((int)drResults["REF_Currency"]);
                        sw.WriteLine("<br/>");
                        sw.WriteLine(string.Format(valueFormat, "Refund Transaction Date", drResults["REF_Date"]));
                        sw.WriteLine(string.Format(valueFormat, "Refund Transaction Amount", drResults["REF_Amount"]));
                        if (cvo != null) sw.WriteLine(string.Format(valueFormat, "Refund Transaction Currency", cvo.IsoCode));
                        sw.WriteLine(string.Format(valueFormat, "Refund Transaction Approval", drResults["REF_Approval"]));
                        sw.WriteLine(string.Format(valueFormat, "Refund Transaction RefCode", drResults["REF_RefCode"]));

                        sw.WriteLine("<br/>");
                        sw.WriteLine(string.Format(valueFormat, "Merchant Url", drResults["CMP_Url"]));
                        sw.WriteLine(string.Format(valueFormat, "Merchant Support", drResults["CMP_Support"]));

                        sw.WriteLine("<br/><br/>Kind regards, <br/>Netpay team");

                        Netpay.Dal.Netpay.TransHistory entity = new Netpay.Dal.Netpay.TransHistory();
                        entity.InsertDate = DateTime.Now;
                        entity.TransPass_id = (int)drResults["ORG_ID"];
                        entity.TransHistoryType_id = (int)Netpay.CommonTypes.TransactionHistoryType.DetectRetrivalReqAfterRefund;
                        entity.Description = "retrival request arrived after refund";
                        entity.IsSucceeded = true;
                        entity.Merchant_id = (int)drResults["CompanyID"];
                        dc.TransHistories.InsertOnSubmit(entity);

                        MailMessage message = new MailMessage();
                        message.From = new MailAddress(Infrastructure.Application.MailAddressFrom, "Netpay Monitoring");
                        message.IsBodyHtml = true;
                        message.Subject = "Retrival request after refund notification";
                        message.Body = string.Format("<html><body>{0}</body></html>", sw.ToString());

                        var domainRecipients = domain.DetectPhotocopyWithRefundRecipients.Split(',');
                        foreach (string currentRecipient in domainRecipients)
                            message.To.Add(currentRecipient);
                        try { Netpay.Infrastructure.Email.SmtpClient.Send(message); }
                        catch (Exception ex) { Logger.Log(LogTag.Monitoring, ex); }
                    }
                    drResults.Close();
                    dc.SubmitChanges();
                    TimeSpan duration = DateTime.Now - start;
                    Logger.Log(LogSeverity.Info, LogTag.Monitoring, string.Format("Photocopy After Refund sent for domain '{0}'. Process took {1}", Domain.Current.Host, duration.ToString()));
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    Infrastructure.Tasks.Lock.Release("SendPhotocopyWithRefund");
                }
            }
        }    
    
    }
}
