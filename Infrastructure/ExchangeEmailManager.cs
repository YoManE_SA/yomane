﻿using System;
using System.Collections.Generic;
using Microsoft.Exchange.WebServices.Data;

namespace Netpay.Infrastructure
{

    public class ExchangeEmailManager
    {
        private readonly string _emailAddress;
        private readonly string _password;
        public ExchangeEmailManager(string emailAddress, string password)
        {
            _emailAddress = emailAddress;
            _password = password;
        }
         
        public ExchangeEmailManager():this(Application.GetParameter("smtpUserName"), Application.GetParameter("smtpPassword"))
        { 
        }
        public void SendEmail(string subject,string emailBody, IList<string> attachmentList, IList<string> toList)
        {
            SendEmail(Application.GetParameter("smtpUserName"),subject,emailBody,attachmentList,toList);
        }
        public void SendEmail(string fromAddress, string subject, string emailBody, IList<string> attachmentList, IList<string> toList, bool isHtmlBody = true)
        {
            var service = new ExchangeService
            {
                Credentials = new WebCredentials(_emailAddress, _password),
                TraceEnabled = true,
                TraceFlags = TraceFlags.All
                
            };

            var exchangeServiceUrl = Application.GetParameter("exchangeServiceUrl");
            service.Url = new Uri(exchangeServiceUrl);
            service.AutodiscoverUrl(fromAddress, RedirectionUrlValidationCallback);

            var email = new EmailMessage(service);
            
            foreach (string to in toList)
            {
                email.ToRecipients.Add(to);
            }
            if(attachmentList!=null )
            foreach (string attachment in attachmentList)
            {
                email.Attachments.AddFileAttachment(attachment);
            }
            email.Subject = subject;
            email.Body = new MessageBody(emailBody);
            email.Body.BodyType = isHtmlBody? BodyType.HTML:BodyType.Text;
            email.Send();
        }

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            var result = false;
            var redirectionUri = new Uri(redirectionUrl);
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }
    }
}
