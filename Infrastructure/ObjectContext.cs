﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Netpay.Infrastructure
{
    public class ObjectContext : System.Security.Principal.IPrincipal, IDisposable
    {
        private Stack<Guid> _credentialsTokens;
        private Security.Login _user = null;
        public Dictionary<string, object> Items { get; private set; }
        public Guid CredentialsToken
        {
            get
            {
                if (_credentialsTokens.Count == 0) return Guid.Empty;
                return _credentialsTokens.Peek();
            }
            set
            {
                if (_credentialsTokens.Count == 1 && _credentialsTokens.Peek() == value) return;
                _credentialsTokens.Clear();
                _credentialsTokens.Push(value);
                _user = null;
            }
        }

        public Security.Login User
        {
            get
            {
                if (_user == null)
                {
                    var cred = CredentialsToken;
                    if (cred == Guid.Empty) return null;
                    _user = Security.Login.Get(cred, false);
                }
                return _user;
            }
        }
        private const string _dataKey = "Netpay.Infrastructure.ObjectContext";

        private static System.Threading.ThreadLocal<ObjectContext> _threadLocalSlot = new System.Threading.ThreadLocal<ObjectContext>();
        public static ObjectContext Current
        {
            get
            {
                ObjectContext ret = null;
                var httpContext = System.Web.HttpContext.Current;
                if (httpContext != null) ret = httpContext.Items[_dataKey] as ObjectContext;
                else ret = _threadLocalSlot.Value; //ret = System.Runtime.Remoting.Messaging.CallContext.LogicalGetData(_dataKey) as ObjectContext;
                if (ret == null) ret = Current = new ObjectContext();
                return ret;
                //if(_contextSlot == null) _contextSlot = System.Runtime.Remoting.Messaging.CallContext.FreeNamedDataSlot("Netpay.Infrastructure.ObjectContext");
                //System.Threading.ExecutionContext.con
                //System.Threading.SynchronizationContext.Current.
                //System.Threading.Thread.
                //return System.Runtime.Remoting.Messaging.CallContext.LogicalGetData("Netpay.Infrastructure.ObjectContext") as ObjectContext;
                //return _threadLocalSlot.Value; 
                //return System.Threading.Thread.CurrentPrincipal as ObjectContext;
            }
            private set
            {
                var httpContext = System.Web.HttpContext.Current;
                if (httpContext != null)
                    httpContext.Items[_dataKey] = value;
                else
                    _threadLocalSlot.Value = value; //System.Runtime.Remoting.Messaging.CallContext.LogicalSetData(_dataKey, value);
                                                    //_threadLocalSlot.Value = value; 
                                                    //System.Threading.Thread.CurrentPrincipal = value;
            }
        }

        private ObjectContext()
        {
            Current = this;
            Items = new Dictionary<string, object>();
            _credentialsTokens = new Stack<Guid>();
        }

        public bool HasPermission(string moduleName, string roleName, Security.PermissionValue value)
        {
            return Security.SecuredObject.Get(moduleName, roleName).HasPermission(value);
        }

        public bool IsUserOfType(Security.UserRole[] roles = null, Security.SecuredObject securedObject = null, Security.PermissionValue value = Security.PermissionValue.None, bool throwError = true)
        {
            if (User != null)
            {
                bool roleOk = (roles == null) || roles.Where(v => User.IsInRole(v)).Any();
                if (roleOk)
                {
                    if (User.Role != Security.UserRole.Admin || CredentialsToken == Domain.Current.ServiceCredentials) return true; //user.Role != UserRole.MerchantSubUser && 
                    if ((securedObject == null) || securedObject.HasPermission(value)) return true;
                }
            }

            if (throwError)
            {
                string message = "?NotAuthorized";
                if (value != Security.PermissionValue.None && securedObject != null)
                {
                    message = string.Format("?NotAuthorized=True&ModuleID={0}&Permission={1}&SecuredObject={2}", securedObject.ModuleID.ToString(), Enum.GetName(typeof(Security.PermissionValue), value), securedObject.Name);
                }
                throw new MethodAccessDeniedException(message);
            };
            
            return false;
        }

    public bool IsCurrentLoginAdminOfAdmins
    {
        get
        {
            if (User == null)
            {
                return false;
            }
            else
            {
                if (User.IsInRole(Security.UserRole.Admin) && Security.AdminUser.LoadForLogin(User.LoginID) != null && Security.AdminUser.LoadForLogin(User.LoginID).IsAdmin == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }


    public bool IsUserOfType(Security.UserRole role, Security.SecuredObject securedObject = null, Security.PermissionValue value = Security.PermissionValue.None, bool throwError = true)
    {
        return IsUserOfType(new Security.UserRole[] { role }, securedObject, value, throwError);
    }

    public void Impersonate(Guid credentialsToken)
    {
        _credentialsTokens.Push(credentialsToken);
        _user = null;
    }

    public void StopImpersonate()
    {
        var value = _credentialsTokens.Pop();
        _user = null;
    }

    public void Dispose()
    {
        foreach (var v in Items.Values)
            if (v is IDisposable) (v as IDisposable).Dispose();
        Items.Clear(); Items = null;
        _credentialsTokens.Clear();
        _user = null;
        Current = null;
    }

    System.Security.Principal.IIdentity System.Security.Principal.IPrincipal.Identity
    {
        get { return new System.Security.Principal.GenericIdentity("NetpayUser", "NetpayUser"); }
    }

    bool System.Security.Principal.IPrincipal.IsInRole(string role)
    {
        return false;
    }
}


public abstract class BaseDataObject
{
    protected BaseDataObject() { }
    protected virtual ObjectContext Context { get { return ObjectContext.Current; } }
    protected Netpay.Infrastructure.Domain Domain { get { return Domain.Current; } }
    public virtual Netpay.Infrastructure.ValidationResult Validate() { return Infrastructure.ValidationResult.Success; }

    public virtual Netpay.Infrastructure.ValidationResult Validate(Dal.Netpay.Account _accountEntity = null) { return Infrastructure.ValidationResult.Success; }
    }
}
