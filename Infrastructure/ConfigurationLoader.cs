﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Reflection;

namespace Netpay.Infrastructure
{
	public class ConfigurationLoader
	{
		public static void Load() 
		{
			Assembly assembly = Assembly.GetCallingAssembly();
			string configFileFullPath = assembly.Location.Replace(".dll", ".config.xml");
			if (!File.Exists(configFileFullPath))
				throw new ApplicationException("Config file not found: " + configFileFullPath);

			XElement root = XElement.Load(configFileFullPath);
			if (root == null)
				throw new ApplicationException("Root not found");

			foreach (XElement currentElement in root.Elements())
			{
				if (currentElement.Attribute("type") == null)
					throw new ApplicationException(string.Format("Must provide the type for '{0}'", currentElement.Name.ToString()));
				Type currentType = Type.GetType(currentElement.Attribute("type").Value);
				if (currentType == null)
					throw new ApplicationException(string.Format("Could not resolve '{0}'", currentElement.Attribute("type").ToString()));
				SetObjectProperties(currentType, currentElement);
			}	
		}

		public static object ParseObject(string value, Type type, XElement element)
		{
			object parsed = null;
			if (type.IsPrimitive)
			{
				if (type == typeof(Boolean))
				{
					parsed = bool.Parse(value);
				}
				else
				{
					long obj = Int64.Parse(value);
					parsed = ((IConvertible)obj).ToType(type, null);
				}
			}
			else if (type.IsEnum)
			{
				parsed = Enum.Parse(type, value);
			}
			else if (type == typeof(string))
			{
				parsed = value;
			}
			else if (type == typeof(DateTime))
			{
				parsed = DateTime.Parse(value);
			}
            else if (type == typeof(string[]))
            {
                parsed = new string[element.Elements().Count()];
            }
			else
			{
				ConstructorInfo constructor = null;
				if (value != null)
				{
					type.GetConstructor(new Type[] { typeof(string) });
					if (constructor != null) 
						parsed = constructor.Invoke(new object[] { value });
				}
				if (parsed == null)
				{
					constructor = type.GetConstructor(new Type[] { });
					if (constructor == null) 
						throw new Exception("Unable to find suitable constructor for type " + type.FullName);
					parsed = constructor.Invoke(null);
				}
			}

			return parsed;
		}

		public static void SetObjectProperties(object obj, XElement element)
		{
			Type type;
			if (obj.GetType().Name == "RuntimeType")
			{
				type = (Type)obj;
				obj = null;
			}
			else
			{
				type = obj.GetType();
			}

			foreach (XElement currentElement in element.Elements())
			{
				string propertyName = currentElement.Name.ToString();
				PropertyInfo propertyInfo = type.GetProperty(propertyName);
				if (propertyInfo != null)
				{
					if (propertyInfo.PropertyType.GetInterface("System.Collections.IList") != null)
					{
						object list = propertyInfo.GetValue(obj, null);
						if (list == null) 
						{
							list = ParseObject(null, propertyInfo.PropertyType, currentElement);
							propertyInfo.SetValue(obj, list, null);
						}
							
						LoadCollection(list as IList, currentElement);
					}
					else
					{
						object newObj;
						if (propertyInfo.CanWrite)
						{
							newObj = ParseObject(currentElement.Value, propertyInfo.PropertyType, currentElement);
							propertyInfo.SetValue(obj, newObj, null);
						}
						else
						{
							newObj = propertyInfo.GetValue(obj, null);
							if (newObj == null) 
								throw new Exception(string.Format("property {0} of object {1} is null", propertyName, type.FullName));
						}

						if (currentElement.Elements().Count() > 0) 
							SetObjectProperties(newObj, currentElement);
					}
				}
				else
					Logger.Log(new Exception(string.Format("Type '{0}' does not support property '{1}'", type.FullName, propertyName)));
			}
		}

		private static void LoadCollection(IList collection, XElement element)
		{
			if (collection == null || element == null) 
				return;

            if (collection.GetType().GetGenericArguments().Length > 0)
            {
                Type itemType = collection.GetType().GetGenericArguments().Single();
                foreach (XElement currentElement in element.Elements())
                {
                    object parsed = ParseObject(currentElement.Value.ToString(), itemType, currentElement);
                    SetObjectProperties(parsed, currentElement);
                    collection.Add(parsed);
                }
            }
            else
            {
                //Type itemType = collection.GetType().GetElementType();        
                throw new ApplicationException("Array not supported");
            }
		}
	}
}
