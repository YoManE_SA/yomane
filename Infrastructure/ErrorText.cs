﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Infrastructure
{
	public class ErrorText : BaseDataObject
	{
		private Netpay.Dal.Netpay.SysErrorCode _entity;

		public string GroupName { get { return _entity.GroupName; } }
		public int ErrorCode { get { return _entity.ErrorCode; } }

		public string ErrorMessage { get { return _entity.ErrorMessage; } set { _entity.ErrorMessage = value; } }
		public string LanguageISOCode { get { return _entity.LanguageISOCode; } set { _entity.LanguageISOCode = value; } }

		private ErrorText(Netpay.Dal.Netpay.SysErrorCode entity)  { _entity = entity; }

		public void Save()
		{
			DataContext.Writer.SysErrorCodes.Update(_entity, true);
			DataContext.Writer.SubmitChanges();
		}


		public static Dictionary<int, ErrorText> Cache
		{
			get
			{
                return  Infrastructure.Domain.Current.GetCachData("ErrorText", () => { 
                    return new Domain.CachData((from e in DataContext.Reader.SysErrorCodes select new ErrorText(e)).ToDictionary(k => k.ErrorCode), DateTime.Now.AddDays(3));
                }) as Dictionary<int, ErrorText>;
			}
		}

		public static string GetErrorMessage(string language, int errorCode)
		{
			if (string.IsNullOrEmpty(language)) language = "en";
			if (!Cache.ContainsKey(errorCode)) return null;
			var err = Cache[errorCode];
			if (err == null) return null;
			return err.ErrorMessage;
		}

		public static Dictionary<int, string> GetErrorGroup(string language, HashSet<string> groups)
		{
			var errs = Cache;
			if (string.IsNullOrEmpty(language)) language = "en";
			return (from e in errs.Values where e.LanguageISOCode == language && groups.Contains(e.GroupName) select e).ToDictionary(k => k.ErrorCode, v => v.ErrorMessage);
		}
	}
}
