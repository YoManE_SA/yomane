﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class TransactionSourceVO
	{
		public TransactionSourceVO() { }

		public int ID { get; set; }
		public string Name { get; set; }
		//public Language Language { get; set; }
	}
}