﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class MerchantSummaryVO
	{
		public MerchantSummaryVO() { }

		public MerchantSummaryVO(ReportGetSummaryMerchantResult entity)
		{
			if (entity == null)
				return;

			MerchantID = entity.ID.GetValueOrDefault();
			MerchantCompanyName = entity.Name;
			CurrencyName = entity.Currency;
			DeclinedAmount = entity.Declined_Amount_Sales.GetValueOrDefault();
			DeclinedCount = entity.Declined_Count_Sales.GetValueOrDefault();
			DeclinedTransactionsFee = entity.Declined_Transactions_Fee.GetValueOrDefault();
			SalesAmount = entity.Transaction_Amount_Sales.GetValueOrDefault();
			SalesCount = entity.Transaction_Count_Sales.GetValueOrDefault();
			SalesProcessingFee = entity.Sales_Processing_Fee.GetValueOrDefault();
			SalesTransactionFee = entity.Sales_Transactions_Fee.GetValueOrDefault();
			ChargeBacksCount = entity.ChargeBacks_Count.GetValueOrDefault();
			ChargeBacksAmount = entity.ChargeBacks_Amount.GetValueOrDefault();
			ChargeBacksProcessingFee = entity.ChargeBacks_Processing_Fee.GetValueOrDefault();
			ChargeBacksTransactionsFee = entity.ChargeBacks_Transactions_Fee.GetValueOrDefault();
			ChargeBacksFee = entity.ChargeBacks_Fee.GetValueOrDefault();
			RefundsCount = entity.Refunds_Count.GetValueOrDefault();
			RefundsAmount = entity.Refunds_Amount.GetValueOrDefault();
			RefundsProcessingFee = entity.Refunds_Processing_Fee.GetValueOrDefault();
			RefundsTransactionsFee = entity.Refunds_Transactions_Fee.GetValueOrDefault();
			RollingReserveAmount = entity.Rolling_Reserve_Amount.GetValueOrDefault();
			RollingReleaseAmount = entity.Rolling_Release_Amount.GetValueOrDefault();
			CalculatedRollingReserve = entity.CalcRollingReserve.GetValueOrDefault();
			TotalApprovedTransFee = SalesTransactionFee + ChargeBacksTransactionsFee + RefundsTransactionsFee;
			TotalApprovedProcessFee = SalesProcessingFee + ChargeBacksProcessingFee + RefundsProcessingFee;
			TotalApprovedFees = TotalApprovedTransFee + TotalApprovedProcessFee;
			TotalTransAmount = SalesAmount - RefundsAmount - ChargeBacksAmount;
			TotalFees = TotalApprovedFees + ChargeBacksFee + DeclinedTransactionsFee;
			NetSalesAmount = TotalTransAmount - TotalFees;
			CalculatedNetSalesAmount = NetSalesAmount - CalculatedRollingReserve + RollingReleaseAmount;
			PaymentMethod = entity.Payment_Method;
		}

		public int MerchantID { get; set; }
		public string MerchantCompanyName { get; set; }
		public string CurrencyName { get; set; }
		public decimal CalculatedRollingReserve { get; set; }
		public decimal DeclinedAmount { get; set; }
		public int DeclinedCount { get; set; }
		public decimal DeclinedTransactionsFee { get; set; }
		public decimal SalesAmount { get; set; }
		public int SalesCount { get; set; }
		public decimal SalesProcessingFee { get; set; }
		public decimal SalesTransactionFee { get; set; }
		public int ChargeBacksCount { get; set; }
		public decimal ChargeBacksAmount { get; set; }
		public decimal ChargeBacksProcessingFee { get; set; }
		public decimal ChargeBacksTransactionsFee { get; set; }
		public decimal ChargeBacksFee { get; set; }
		public int RefundsCount { get; set; }
		public decimal RefundsAmount { get; set; }
		public decimal RefundsProcessingFee { get; set; }
		public decimal RefundsTransactionsFee { get; set; }
		public decimal RollingReserveAmount { get; set; }
		public decimal RollingReleaseAmount { get; set; }
		public decimal TotalApprovedTransFee { get; set; }
		public decimal TotalApprovedProcessFee { get; set; }
		public decimal TotalApprovedFees { get; set;  }
		public decimal TotalTransAmount { get; set;  }
		public decimal TotalFees { get; set; }
		public decimal NetSalesAmount { get; set; }
		public decimal CalculatedNetSalesAmount { get; set; }
		public string PaymentMethod { get; set; }
	}
}
