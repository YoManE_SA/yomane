﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Reports;

namespace Netpay.Infrastructure.VO
{
	public class FailStatReportVO
	{
		public struct ReplyGroup
		{
			public string ErrCode { get; set; }
			public int GroupCount { get; set; }
			public string FailText { get; set; }
			public string CodeText { get; set; }
		}


		public FailStatReportVO() { }

		public FailStatReportVO(FailStatReportData entity)
		{
			InsertDate = entity.InsertDate;
			ReplyCode = entity.ReplyCode;
			ReplyText = entity.ReplyText;
			FailSource = entity.FailSource;
			FailSourceText = entity.FailSourceText;
			CompanyID = entity.CompanyID;
			CompanyName = entity.CompanyName;
			TerminalNumber = entity.TerminalNumber;
			TerminalName = entity.TerminalName;
			TerminalContractNum = entity.TerminalContractNum;
			DebitCompanyID = entity.DebitCompanyID;
			DebitCompanyName = entity.DebitCompanyName;
			PaymentMethodID = entity.PaymentMethod;
			PaymentMethodText = entity.PaymentMethodText;
			Amount = entity.Amount;
			CurrencyID = entity.CurrencyCode;
			CurrencySymbol = entity.CUR_Symbol;
			CurrencyISO = entity.CurrencyIso;
			CardNumber = entity.CardNumber;
			ID = entity.ID;
			TransType = entity.TransType;
		}

		public DateTime InsertDate { get; set; }
		public string ReplyCode { get; set; }
		public string ReplyText { get; set; }
		public byte? FailSource { get; set; }
		public string FailSourceText { get; set; }
		public int CompanyID { set; get; }
		public string CompanyName { set; get; }
		public string TerminalNumber { get; set; }
		public string TerminalName { get; set; }
		public string TerminalContractNum { get; set; }
		public int DebitCompanyID { get; set; }
		public string DebitCompanyName { get; set; }
		public int PaymentMethodID { get; set; }
		public string PaymentMethodText { get; set; }
		public decimal Amount { get; set; }
		public int CurrencyID { set; get; }
		public string CurrencySymbol { set; get; }
		public string CurrencyISO { get; set; }
		public System.Data.Linq.Binary CardNumber { get; set; }
		public int ID { set; get; }
		public int TransType { set; get; }
	}
}
