﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class MerchantRegistrationVO
    {
		public MerchantRegistrationVO() { IntegrationData = new PropertyList(); } 
       
        public Guid ID { get; set; }
		public int? MerchantID { get; set; }
        public string DbaName { get; set; }
        public string LegalBusinessName { get; set; }
		public string LegalBusinessNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public DateTime? OwnerDob { get; set; }
        public string OwnerSsn { get; set; }
        public string Phone { get; set; }
		public string Fax { get; set; }
        public string Url { get; set; }
        public string PhisicalAddress { get; set; }
        public string PhisicalCity { get; set; }
        public string PhisicalState { get; set; }
        public string PhisicalZip { get; set; }
        public string StateOfIncorporation { get; set; }
        public int? TypeOfBusiness { get; set; }
        public DateTime? BusinessStartDate { get; set; }
        public int? Industry { get; set; }
        public string BusinessDescription { get; set; }
        public decimal? AnticipatedMonthlyVolume { get; set; }
        public decimal? AnticipatedAverageTransactionAmount { get; set; }
        public decimal? AnticipatedLargestTransactionAmount { get; set; }
		public string CanceledCheckImageFileName { get; set; }
		public string CanceledCheckImageMimeType { get; set; }
		public string CanceledCheckImageContent { get; set; }
		public string BankAccountNumber { get; set; }
		public string BankRoutingNumber { get; set; }
		public int PercentDelivery0to7 { get; set; }
		public int PercentDelivery8to14 { get; set; }
		public int PercentDelivery15to30 { get; set; }
		public int PercentDeliveryOver30 { get; set; }

		public PropertyList IntegrationData { get; set; }

		public void EncryptBankInfo(string domainHost) 
		{
			try
			{
				BankAccountNumber = Security.Encryption.Encrypt(domainHost, BankAccountNumber).ToBase64();
				BankRoutingNumber = Security.Encryption.Encrypt(domainHost, BankRoutingNumber).ToBase64();
			}
			catch (Exception)
			{
				BankAccountNumber = "";
				BankRoutingNumber = "";
			}
		}

		public void DecryptBankInfo(string domainHost)
		{
			try
			{
				BankAccountNumber = Security.Encryption.Decrypt(domainHost, BankAccountNumber.FromBase64());
				BankRoutingNumber = Security.Encryption.Decrypt(domainHost, BankRoutingNumber.FromBase64());
			}
			catch (Exception)
			{
				BankAccountNumber = "";
				BankRoutingNumber = "";
			}
		}
    }
}
