﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class FileItemTypeVO
    {
        public FileItemTypeVO(FileItemType entity)
        {
            ID = entity.FileItemType_id;
            Name = entity.Name;
        }

        public byte ID { get; set; }
        public string Name {get; set;}
    }
}
