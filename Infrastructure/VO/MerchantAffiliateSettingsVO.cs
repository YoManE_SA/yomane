﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class MerchantPartnerSettingsVO
    {
        public MerchantPartnerSettingsVO() { }
      
        public MerchantPartnerSettingsVO(SetMerchantAffiliate settings) 
        {
			ID = settings.SetMerchantAffiliate_id;
            UserID = settings.UserID;
			MerchantId = settings.Merchant_id;
			PartnerId = settings.Affiliate_id;
			ConfigurationValues = System.Web.HttpUtility.ParseQueryString(settings.ConfigurationValues ?? "");
        }
		
		public int ID { get; set; }
        public string UserID { get; set; }
		public int MerchantId { get; set; }
		public int PartnerId { get; set; }
		public System.Collections.Specialized.NameValueCollection ConfigurationValues { get; set; }
    }
}
