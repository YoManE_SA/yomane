﻿using System;
using System.Text;
using System.Collections.Generic;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class FileVO
    {
        public FileVO(tblFile entity) 
        { 
            File_id = entity.File_id;
            InsertDate = entity.InsertDate;
            Company_id = entity.Company_id;
            Customer_id = entity.Customer_id;
            FileTitle = entity.FileTitle;
            FileName = entity.FileName;
            FileExt = entity.FileExt;
            AdminComment = entity.AdminComment;
			FileItemType_id = entity.FileItemType_id;
            AdminApprovalDate = entity.AdminApprovalDate;
            AdminApprovalUser = entity.AdminApprovalUser;
        }
        public FileVO() { }

        public int File_id { get; set; }
        public DateTime? InsertDate { get; set; }
        public int? Company_id { get; set; }
        public int? Customer_id { get; set; }
        public string FileTitle { get; set; }
        public string FileName { get; set; }
        public string FileExt { get; set; }
        public string AdminComment { get; set; }
		public byte? FileItemType_id { get; set; }
        public DateTime? AdminApprovalDate { get; set; }
        public string AdminApprovalUser { get; set; }
    }
}
