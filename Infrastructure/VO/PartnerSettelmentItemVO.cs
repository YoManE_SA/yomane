﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class SettelmentItemVO
	{
		public int ID { get; set; }
		public int Settelment_id { get; set; }
		public string ItemText { get; set; }
		public int Quantity { get; set; }
		public decimal? Amount { get; set; }
		public decimal Total { get; set; }
		public int? TransAmountType_id { get; set; }
		public SettelmentItemVO(object entity)
		{
		/*
			ID = entity.AffiliateSettelmentItem_id;
			Settelment_id = entity.AffiliateSettelment_id;
			ItemText = entity.ItemText;
			Quantity = entity.Quantity;
			Amount = entity.Amount;
			Total = entity.Total;
			TransAmountType_id = entity.TransAmountType_id;
		*/
		}
	}
}
