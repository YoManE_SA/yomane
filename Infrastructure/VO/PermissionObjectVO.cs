﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class PermissionObjectVO
	{
		public PermissionObjectVO(){}

		public PermissionObjectVO(tblLimitedLoginSecurity entity)
		{
			Name = entity.lls_Item;
			Type = (PermissionObjectType)entity.lls_ItemType;
		}

		public PermissionObjectVO(tblUserPermission entity)
		{
			Name = entity.PermissionValue;
			Type = (PermissionObjectType)entity.PermissionTypeID;
		}
		
		public string Name { get; set; }
		public PermissionObjectType Type { get; set; }
	}
}
