﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class PaymentMethodCountryCurrencyVO
    {
        public CommonTypes.PaymentMethodEnum PaymentMethodID { get; set; }
        public int? CountryID { get; set; }
        public int? CurrencyID { get; set; }

		public PaymentMethodCountryCurrencyVO(PaymentMethodToCountryCurrency entity)
        {
            PaymentMethodID = (CommonTypes.PaymentMethodEnum)entity.PaymentMethod_id;
            CountryID = entity.Country_id;
            CurrencyID = entity.Currency_id;
        }
    }
}
