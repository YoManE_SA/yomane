﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class CreditcardWhitelistVO
	{
		public CreditcardWhitelistVO() { }

		public CreditcardWhitelistVO(tblCreditCardWhitelist entity) 
		{
			MerchantID = entity.ccwl_Merchant;
			LevelID = entity.ccwl_Level;
			InsertDate = entity.ccwl_InsertDate;
			ExpirationDate = new DateTime((short)entity.ccwl_ExpYear, (byte)entity.ccwl_ExpMonth, 1);
		}
		
		public int MerchantID { get; set; }
		public int LevelID { get; set; }
		public DateTime InsertDate { get; set; }
		public DateTime ExpirationDate { get; set; }
	}
}
