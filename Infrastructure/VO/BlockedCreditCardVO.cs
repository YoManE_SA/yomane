﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class BlockedCreditCardVO
	{
		public BlockedCreditCardVO(){}

		public BlockedCreditCardVO(tblFraudCcBlackList entity)
		{
			ID = entity.fraudCcBlackList_id;
            merchantId = entity.company_id;
			InsertDate = entity.fcbl_InsertDate;
            UnblockDate = entity.fcbl_UnblockDate;
            //CreditCardTypeID = entity.creditCardType_id;
			CreditCardNumber = entity.fcbl_ccDisplay;
			//CreditCardExpirationMonth = int.Parse(entity.fcbl_ccExpMonth);
			//CreditCardExpirationYear = int.Parse(entity.fcbl_ccExpYear);
			PaymentMethod = (CommonTypes.PaymentMethodEnum) entity.PaymentMethod_id.GetValueOrDefault();
			Comments = entity.fcbl_comment;
			BlockLevel = (BlockedCardLevel)entity.fcbl_BlockLevel;
			Display = entity.fcbl_ccDisplay;
		}

		public int ID { get; set; }
        public int merchantId { get; set; }
        [EntityPropertyMapping("fcbl_InsertDate")]
        public DateTime InsertDate { get; set; }
        public DateTime UnblockDate { get; set; }
		public CommonTypes.PaymentMethodEnum PaymentMethod { get; set; }
		//public int CreditCardTypeID { get; set; }
		public string CreditCardNumber { get; set; }
		public int CreditCardExpirationMonth { get; set; }
		public int CreditCardExpirationYear { get; set; }
		public string Display { get; set; }
		public string Comments { get; set; }
		public BlockedCardLevel BlockLevel { get; set; }
	}
}
