﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.ErrorNet;

namespace Netpay.Infrastructure.VO
{
	public class BllLogVO
	{
		public BllLogVO(){}
		
		public BllLogVO(tblBllLog entity)
		{
			ID = entity.ID;
			InsertDate = entity.InsertDate;
			SeverityID = entity.SeverityID;
			Tag = entity.Tag;
			Source = entity.Source;
			Message = entity.Message;
			LongMessage = entity.LongMessage;
		}
		
		public int ID { get; set; }
		[EntityPropertyMapping("InsertDate")]
		public DateTime InsertDate { get; set; }
		[EntityPropertyMapping("SeverityID")]
		public byte SeverityID { get; set; }
		[EntityPropertyMapping("Tag")]
		public string Tag { get; set; }
		public string Source { get; set; }
		public string Message { get; set; }
		public string LongMessage { get; set; }
	}
}
