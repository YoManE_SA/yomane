﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class MerchantLimitedUserVO
	{
		public MerchantLimitedUserVO(){}

		public MerchantLimitedUserVO(MerchantSubUser entity)
		{
			ID = entity.MerchantSubUser_id;
			MerchantID = entity.Merchant_id;
			UserName = entity.UserName;
		}
		
		public int ID { get; set; }
		public int MerchantID { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
	}
}
