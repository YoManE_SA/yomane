﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class CartItemVO
	{
		public CartItemVO() {}

		public CartItemVO(tblCartItem entity)
		{
			ID = entity.ID;
			ItemID = entity.ItemID;
			Date = entity.InsertDate;
			CartID = entity.CartID;
			Name = entity.Name;
			Details = entity.Details;
			Quantity = entity.Quantity;
			Price = entity.Price;
			CurrencyID = entity.CurrencyID;
			ShippingFee = entity.ShippingFee;
			TaxRatio = entity.TaxRatio;
			MerchantID = entity.MerchantID;
			ItemUrl = entity.ItemURL;
			ImageUrl = entity.ItemImage;
			QuantityMin = entity.QuantityMin;
			QuantityMax = entity.QuantityMax;
			QuantityStep = entity.QuantityStep;

		}

		public int ID { get; set; }
		public string ItemID { get; set; }
		public DateTime Date { get; set; }
		public int MerchantID { get; set; }
		public System.Guid CartID { get; set; }
		public string Name { get; set; }
		public string Details { get; set; }
		public int Quantity { get; set; }
		public decimal Price { get; set; }
		public int CurrencyID { get; set; }
		public decimal ShippingFee { get; set; }
		public decimal TaxRatio { get; set; }
		public string ItemUrl { get; set; }
		public string ImageUrl { get; set; }
		public int? QuantityMin { get; set; }
		public int? QuantityMax { get; set; }
		public int? QuantityStep { get; set; }
	}
}
