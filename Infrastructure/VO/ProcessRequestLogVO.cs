﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Domains;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class ProcessRequestLogVO
	{
		public ProcessRequestLogVO(){}

		public ProcessRequestLogVO(string domainHost, tblLogChargeAttempt entity)
		{
			ID = entity.LogChargeAttempts_id;
			Date = entity.Lca_DateStart;
			if (entity.TransactionType_id != null)
			{
				TransactionSourceVO sourceVO = DomainsManager.GetDomain(domainHost).Cache.GetTransactionSource(Convert.ToInt32(entity.TransactionType_id.Value));
				if (sourceVO!=null) Source = sourceVO.Name;
			}
			RemoteAddress = entity.Lca_RemoteAddress;
			RequestMethod = entity.Lca_RequestMethod;
			ReplyCode = entity.Lca_ReplyCode;
			TransactionNumber = entity.Lca_TransNum;
			HttpHost = entity.Lca_HttpHost;
			QueryString = entity.Lca_QueryString;
			RequestForm = entity.Lca_RequestForm;
			ReplyDescription = entity.Lca_ReplyDesc;
			TransactionTypeId = entity.TransactionType_id;
            DateStart = entity.Lca_DateStart;
            DateEnd = entity.Lca_DateEnd;
            MerchantNumber = entity.Lca_MerchantNumber;
        }
		
		public int ID { get; set; }
		public DateTime? Date { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public string MerchantNumber { get; set; }
        public double Duration { get { return (DateEnd.GetValueOrDefault() - DateStart.GetValueOrDefault()).TotalSeconds; } }
		public string Source { get; set; }
		public string RemoteAddress { get; set; }
		public string RequestMethod { get; set; }
		public string ReplyCode { get; set; }
		public int? TransactionNumber { get; set; }
		public string HttpHost { get; set; }
		public string QueryString { get; set; }
		public string RequestForm { get; set; }
		public string ReplyDescription { get; set; }
		public short? TransactionTypeId { get; set; }
	}
}
