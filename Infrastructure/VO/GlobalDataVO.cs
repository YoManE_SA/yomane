﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class GlobalDataVO
	{
		public GlobalDataVO(){}

		public GlobalDataVO(tblGlobalData entity)
		{
			ID = entity.GD_ID;
			GroupID = entity.GD_Group;
			LanguageID = entity.GD_LNG;
			Value = entity.GD_Text;
			Description = entity.GD_Description;
			Flags = entity.GD_Flags;
			if (entity.GD_Country != null && entity.GD_Country.Trim() != "")
				CountryCodes = entity.GD_Country.Split(',').ToList<string>();
		}

		public int ID { get; set; }
		public int GroupID { get; set; }
		public int LanguageID { get; set; }
		public string Value { get; set; }
		public string Description { get; set; }
		public List<string> CountryCodes { get; set; }
		public short Flags { get; set; }
	}
}
