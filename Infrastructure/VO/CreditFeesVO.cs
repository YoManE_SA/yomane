﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class CreditFeesVO
	{
		public int ID { get; set; }
		public int CompanyID { get; set; }
		public int CurrencyID { get; set; }
		public short PaymentMethod { get; set; }
		public int CreditTypeID { get; set; }
		public string ListBINs { get; set; }
		public short ExchangeTo { get; set; }
		public decimal MaxAmount { get; set; }
		public decimal PercentFee { get; set; }
		public decimal FixedFee { get; set; }
		public decimal FailFixedFee { get; set; }
		public decimal ApproveFixedFee { get; set; }
		public decimal RefundFixedFee { get; set; }
		public decimal ClarificationFee { get; set; }
		public decimal CBFixedFee { get; set; }

		public CreditFeesVO (tblTransactionPayFee entity)
		{
			ID = entity.CCF_ID;
			CompanyID = entity.CCF_CompanyID;
			CurrencyID = entity.CCF_CurrencyID;
			PaymentMethod = entity.CCF_PaymentMethod;
			CreditTypeID = entity.CCF_CreditTypeID;
			ListBINs = entity.CCF_ListBINs;
			ExchangeTo = entity.CCF_ExchangeTo;
			MaxAmount = entity.CCF_MaxAmount;
			PercentFee = entity.CCF_PercentFee;
			FixedFee = entity.CCF_FixedFee;
			ApproveFixedFee = entity.CCF_ApproveFixedFee;
			RefundFixedFee = entity.CCF_RefundFixedFee;
			ClarificationFee = entity.CCF_ClarificationFee;
			CBFixedFee = entity.CCF_CBFixedFee;
			FailFixedFee = entity.CCF_FailFixedFee;
		}

		public CreditFeesVO (tblCompanyCreditFee entity)
		{
			ID = entity.CCF_ID;
			CompanyID = entity.CCF_CompanyID;
			CurrencyID = entity.CCF_CurrencyID;
			PaymentMethod = entity.CCF_PaymentMethod.GetValueOrDefault(0);
			CreditTypeID = entity.CCF_CreditTypeID;
			ListBINs = entity.CCF_ListBINs;
			ExchangeTo = entity.CCF_ExchangeTo;
			MaxAmount = entity.CCF_MaxAmount;
			PercentFee = entity.CCF_PercentFee;
			FixedFee = entity.CCF_FixedFee;
			ApproveFixedFee = entity.CCF_ApproveFixedFee;
			RefundFixedFee = entity.CCF_RefundFixedFee;
			ClarificationFee = entity.CCF_ClarificationFee;
			CBFixedFee = entity.CCF_CBFixedFee;
			FailFixedFee = entity.CCF_FailFixedFee;
		}
	}
}
