﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class TransactionAmountGroupVO
	{
		public TransactionAmountGroupVO() { AmountTypes = new List<TransactionAmountType>(); }
		public TransactionAmountGroupVO(TransAmountTypeGroup entity)
		{
			ID = entity.TransAmountTypeGroup_id;
			Name = entity.Name;
			AmountTypes = entity.list_TransAmountTypeToGroups.Select(e => (TransactionAmountType)e.TransAmountType_id).ToList();
		}
		public int ID { get; set; }
		public string Name { get; set; }
		public List<TransactionAmountType> AmountTypes{ get; set; }
	}
}
