﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class CartVO
	{
		public CartVO(tblCart entity)
		{
			ID = entity.ID;
			MerchantID = entity.MerchantID;
			CustomerID = entity.CustomerID;
			StartDate = entity.StartDate;
			CheckoutDate = entity.CheckoutDate;
			Currency = entity.CurrencyID;
			TotalProducts = entity.TotalProducts;
			TotalShipping = entity.TotalShipping;
			Total = entity.Total.GetValueOrDefault();
			Installments = entity.Installments;
			RecepientName = entity.RecepientName;
			RecepientPhone = entity.RecepientPhone;
			RecepientMail = entity.RecepientMail;
			Comment = entity.Comment;
			OrderStatus = (CartStatus)entity.OrderStatus;
			CustomStatus = entity.CustomStatus;
			ShippingAddress = entity.ShippingAddress;
			ShippingAddress2 = entity.ShippingAddress2;
			ShippingCity = entity.ShippingCity;
			ShippingZip = entity.ShippingZip;
			ShippingState = entity.ShippingState;
			ShippingCountry = entity.ShippingCountry;
			TransPassID = entity.TransPassID;
			TransApprovalID = entity.TransApprovalID;
			TransPendingID = entity.TransPendingID;
			ReferenceNumber = entity.ReferenceNumber;
		}

		public System.Guid ID { get; set; }
		public int MerchantID { get; set; }
		public int? CustomerID { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime? CheckoutDate { get; set; }
		public int Currency { get; set; }
		public decimal TotalProducts { get; set; }
		public decimal TotalShipping { get; set; }
		public decimal TotalTax { get; set; }
		public decimal Total { get; set; }
		public int TotalItems { get; set; }
		public int Installments { get; set; }
		public string RecepientName { get; set; }
		public string RecepientPhone { get; set; }
		public string RecepientMail { get; set; }
		public string Comment { get; set; }
		public string ShippingAddress { get; set; }
		public string ShippingAddress2 { get; set; }
		public string ShippingCity { get; set; }
		public string ShippingZip { get; set; }
		public CartStatus OrderStatus { get; set; }
		public int? ShippingState { get; set; }
		public int? ShippingCountry { get; set; }
		public int? TransPassID { get; set; }
		public int? TransApprovalID { get; set; }
		public int? TransPendingID { get; set; }
		public int? CustomStatus { get; set; }
		public int ReferenceNumber { get; set; }
	}
}
