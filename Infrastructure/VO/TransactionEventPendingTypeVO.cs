﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class EventPendingTypeVO
	{
		public EventPendingTypeVO(EventPendingType entity)
		{
			ID = entity.EventPendingType_id;
			Text = entity.Name;
		}
		public int ID { get; set; }
		public string Text { get; set; }
	}
}
