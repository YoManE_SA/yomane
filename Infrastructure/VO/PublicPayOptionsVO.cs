﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class PublicPayOptionsVO
	{
		public PublicPayOptionsVO(){}

		public PublicPayOptionsVO(tblPublicPayOption entity)
		{
			ID = entity.ID;
			MerchantID = entity.MerchantID;
			IsPublicPay = entity.IsPublicPay;
			LogoPic = entity.LogoPic;
			IsAsk4PersonalInfo = entity.IsAsk4PersonalInfo;
			ChargeOptions = (PublicPayChargeOptions?)entity.ChargeOptions;
			IsSendAnswer = entity.IsSendAnswer;
			SendAnswerURL = entity.SendAnswerURL;
			CurrencyOptions = entity.CurrencyOptions;
            PromotionBannerUrl = entity.BannerUrl;
			BannerImageName = entity.BannerImageName;
			MobileAppLogo = entity.MobileAppLogo;
			IsFullName = entity.IsFullName;
			IsRequiredFullName = entity.IsRequiredFullName;
			IsAddress = entity.IsAddress;
			IsRequiredAddress = entity.IsRequiredAddress;
			IsPhone = entity.IsPhone;
			IsRequiredPhone = entity.IsRequiredPhone;
			IsCellular = entity.IsCellular;
			IsRequiredCellular = entity.IsRequiredCellular;
			IsEmail = entity.IsEmail;
			IsRequiredEmail = entity.IsRequiredEmail;
			IsHowGetHere = entity.IsHowGetHere;
			IsRequiredHowGetHere = entity.IsRequiredHowGetHere;
            IsVar1 = entity.IsVar1.GetValueOrDefault();
			IsRequiredVar1 = entity.IsRequiredVar1.GetValueOrDefault();
            IsVar2 = entity.IsVar2.GetValueOrDefault();
            IsRequiredVar2 = entity.IsRequiredVar2.GetValueOrDefault();
            IsVar3 = entity.IsVar3.GetValueOrDefault();
            IsRequiredVar3 = entity.IsRequiredVar3.GetValueOrDefault();
            IsVar4 = entity.IsVar4.GetValueOrDefault();
            IsRequiredVar4 = entity.IsRequiredVar4.GetValueOrDefault();
		}

		public int ID { get; set; }
		public int MerchantID { get; set; }
		public bool IsPublicPay { get; set; }
		public string LogoPic { get; set; }
		public bool IsAsk4PersonalInfo { get; set; }
		public PublicPayChargeOptions? ChargeOptions { get; set; }
		public bool IsSendAnswer { get; set; }
		public string SendAnswerURL { get; set; }
        public string CurrencyOptions { get; set; }
        public string PromotionBannerUrl { get; set; }
		public string BannerImageName { get; set; }
		public string MobileAppLogo { get; set; }
		public bool IsFullName { get; set; }
		public bool IsRequiredFullName { get; set; }
		public bool IsAddress { get; set; }
		public bool IsRequiredAddress { get; set; }
		public bool IsPhone { get; set; }
		public bool IsRequiredPhone { get; set; }
		public bool IsCellular { get; set; }
		public bool IsRequiredCellular { get; set; }
		public bool IsEmail { get; set; }
		public bool IsRequiredEmail { get; set; }
		public bool IsHowGetHere { get; set; }
		public bool IsRequiredHowGetHere { get; set; }
		public bool IsVar1 { get; set; }
		public bool IsRequiredVar1 { get; set; }
		public bool IsVar2 { get; set; }
		public bool IsRequiredVar2 { get; set; }
		public bool IsVar3 { get; set; }
		public bool IsRequiredVar3 { get; set; }
		public bool IsVar4 { get; set; }
		public bool IsRequiredVar4 { get; set; }
	}
}