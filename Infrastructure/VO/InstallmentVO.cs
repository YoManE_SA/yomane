﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    [Serializable]
    public class InstallmentVO
	{
		public InstallmentVO() { }

		public InstallmentVO(tblCompanyTransInstallment entity)
		{
			ID = entity.id;
			InstallmentID = entity.InsID;
			SettlementID = entity.payID;
			MerchantID = entity.CompanyID;
			Amount = entity.amount;
			Comment = entity.comment;
		}

		public int ID { get; set; }
		public int InstallmentID { get; set; }
		public int SettlementID { get; set; }
		public int? MerchantID { get; set; }
		public decimal Amount { get; set; }
		public string Comment { get; set; }
	}
}
