﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.VO
{
    public class MerchantExtendedVO
    {
        public int MerchantID { get; set; }
        public string MerchantNumber { get; set; }
        public string MerchantName { get; set; }
        public string MerchantEmail { get; set; }
        public int? MerchantStatusID { get; set; }
        public string MerchantStatusText { get; set; }
        public int? MerchantGroupID { get; set; }
        public string MerchantGroupName { get; set; }
        public int? IndustryID { get; set; }
        public string IndustryText { get; set; }
        public string AccountManager { get; set; }
        public string MerchantUrl { get; set; }
        public DateTime MerchantOpeningDate { get; set; }
        public DateTime MerchantClosingDate { get; set; }
        public string MerchantContactName { get; set; }
        public string MerchantCountry { get; set; }
        public string MerchantCity { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int? ParentCompanyID { get; set; }
        public string ParentCompanyName { get; set; }
    }
}
