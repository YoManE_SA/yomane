﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class TransactionEventPendingVO
	{
		public TransactionEventPendingVO(EventPending entity)
		{
			ID = entity.EventPending_id;
			Date = entity.InsertDate;
			Parameters = entity.Parameters;
			Type = (PendingEventType)entity.EventPendingType_id;
		}

		public int ID { get; set; }
		public DateTime Date { get; set; }
		public string Parameters { get; set; }
		public PendingEventType Type { get; set; }
	}
}
