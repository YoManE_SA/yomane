﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class PartnerSettlmentVO
	{
		
		
		public int AffiliatePaymentID {get;set;}
		public int AffiliateID {get;set;}
		public int MerchantID {get;set;}
		public int PayID {get;set;}
		public decimal Total {get;set;}
		public Currency Currency {get;set;}

		public PartnerSettlmentVO(tblAffiliatePayment entity)
		{
			AffiliatePaymentID = entity.AFP_TransPaymentID;
			AffiliateID = entity.AFP_Affiliate_ID;
			MerchantID = entity.AFP_CompanyID;
			PayID = entity.AFP_TransPaymentID;
			Total = entity.AFP_PaymentAmount;
			Currency = (Currency)entity.AFP_PaymentCurrency;
		}
	}
}
