﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;

namespace Netpay.Infrastructure.VO
{
	public struct MerchantCustomizationLogo
	{
		public string Align;
		public int Width;
		public int Height;

		public MerchantCustomizationLogo(string align = null, int? width = null, int? height = null)
		{
			Align = align;
			Width = width.GetValueOrDefault(0);
			Height = height.GetValueOrDefault(0);
		}
	}

	public struct MerchantCustomizationText
	{
		public string BackColor;
		public string ForeColor;
		public string FontFamily;
		public string FontSize;
		public bool FontIsBold;
		public bool FontIsItalic;

		public MerchantCustomizationText(string backColor = null, string foreColor = null, string fontFamily = null, string fontSize = null, bool? isBold = null, bool? isItalic = null)
		{
			BackColor = backColor;
			ForeColor = foreColor;
			FontFamily = fontFamily;
			FontSize = fontSize;
			FontIsBold = isBold.GetValueOrDefault(false);
			FontIsItalic = isItalic.GetValueOrDefault(false);
		}
	}

	public class MerchantCustomizationVO
	{
		public int MerchantID;
		public int? SkinID;
		public MerchantCustomizationLogo LogoMain;
		public MerchantCustomizationLogo LogoTopRight;
		public MerchantCustomizationLogo LogoTopLeft;
		public MerchantCustomizationLogo LogoBottomRight;
		public MerchantCustomizationLogo LogoBottomLeft;
		public string BackImageRepeat;
		public string BackColor;
		public MerchantCustomizationText Text;
		public MerchantCustomizationText TextTitle;
		public MerchantCustomizationText TextFormTitle;
		public MerchantCustomizationText TextGroupTitle;
		public MerchantCustomizationText TextField;
		public MerchantCustomizationText TextButton;

		public MerchantCustomizationVO() { }

		public MerchantCustomizationVO(tblMerchantCustomization entity)
		{
			MerchantID = entity.MerchantID;
			SkinID = entity.SkinID;
			BackImageRepeat = entity.BackImageRepeat;
			BackColor = entity.PageBackColor;
			LogoMain = new MerchantCustomizationLogo(entity.LogoMainAlign, entity.LogoMainWidth, entity.LogoMainHeight);
			LogoTopRight = new MerchantCustomizationLogo(entity.LogoTopRightAlign, entity.LogoTopRightWidth, entity.LogoTopRightHeight);
			LogoTopLeft = new MerchantCustomizationLogo(entity.LogoTopLeftAlign, entity.LogoTopLeftWidth, entity.LogoTopLeftHeight);
			LogoBottomRight = new MerchantCustomizationLogo(entity.LogoBottomRightAlign, entity.LogoBottomRightWidth, entity.LogoBottomRightHeight);
			LogoBottomLeft = new MerchantCustomizationLogo(entity.LogoBottomLeftAlign, entity.LogoBottomLeftWidth, entity.LogoBottomLeftHeight);
			Text = new MerchantCustomizationText(entity.BackColor, entity.ForeColor, entity.FontFamily, entity.FontSize, entity.FontIsBold, entity.FontIsItalic);
			TextTitle = new MerchantCustomizationText(entity.TitleBackColor, entity.TitleForeColor, entity.TitleFontFamily, entity.TitleFontSize, entity.TitleFontIsBold, entity.TitleFontIsItalic);
			TextFormTitle = new MerchantCustomizationText(entity.FormTitleBackColor, entity.FormTitleForeColor, entity.FormTitleFontFamily, entity.FormTitleFontSize, entity.FormTitleFontIsBold, entity.FormTitleFontIsItalic);
			TextGroupTitle = new MerchantCustomizationText(entity.GroupTitleBackColor, entity.GroupTitleForeColor, entity.GroupTitleFontFamily, entity.GroupTitleFontSize, entity.GroupTitleFontIsBold, entity.GroupTitleFontIsItalic);
			TextField = new MerchantCustomizationText(entity.FieldBackColor, entity.FieldForeColor, entity.FieldFontFamily, entity.FieldFontSize, entity.FieldFontIsBold, entity.FieldFontIsItalic);
			TextButton = new MerchantCustomizationText(entity.ButtonBackColor, entity.ButtonForeColor, entity.ButtonFontFamily, entity.ButtonFontSize, entity.ButtonFontIsBold, entity.ButtonFontIsItalic);
		}
	}
}