﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class MerchantDepositVO
	{
		public MerchantDepositVO(tblCompany entity)
		{
			SecurityDeposit = entity.SecurityDeposit;
			SecurityPeriod = entity.SecurityPeriod;
			Enable = entity.RREnable;
			AutoRet = entity.RRAutoRet.GetValueOrDefault();
			KeepAmount = entity.RRKeepAmount;
			KeepCurrency = (Currency)entity.RRKeepCurrency;
			State = (MerchantDepositState)entity.RRState;
		}

		public decimal SecurityDeposit { get; set; }
		public int SecurityPeriod { get; set; }
		public bool Enable { get; set; }
		public bool AutoRet { get; set; }
		public decimal KeepAmount { get; set; }
		public Currency KeepCurrency { get; set; }
		public MerchantDepositState State { get; set; }
	}
}
