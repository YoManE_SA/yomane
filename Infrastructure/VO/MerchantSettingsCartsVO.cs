﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;

namespace Netpay.Infrastructure.VO
{
	public class MerchantSettingsCartVO
	{
		public MerchantSettingsCartVO() {}

		public MerchantSettingsCartVO(tblCompanySettingsCart entity)
		{
			MerchantID = entity.CompanyID;
			IsEnabled = entity.IsEnabled;
			IsCartStored = entity.IsCartStored;
			IsAuthOnly = entity.IsAuthOnly;
			IsShippingEnabled = entity.IsShippingEnabled;
			IsShippingRequired = entity.IsShippingRequired;

		}

		public int MerchantID { get; set; }
		public bool IsEnabled { get; set; }
		public bool IsCartStored { get; set; }
		public bool IsAuthOnly { get; set; }
		public bool IsShippingEnabled { get; set; }
		public bool IsShippingRequired { get; set; }

	}
}
