﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class BankVO
	{
		public BankVO(){}

		public BankVO(tblSystemBankList entity)
		{
			Address = entity.address;
			BankCode = entity.bankCode;
			BankName = entity.bankName;
			Fax = entity.fax;
			ID = entity.id;
			Phone = entity.phone;
			Zip = entity.zip;
		}

		public string Address { get; set; }
		public int BankCode { get; set; }
		public string BankName { get; set; }
		public string Fax { get; set; }
		public int ID { get; set; }
		public string Phone { get; set; }
		public string Zip { get; set; }
	}
}
