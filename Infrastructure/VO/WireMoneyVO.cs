﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;
using Netpay.Infrastructure.Domains;


namespace Netpay.Infrastructure.VO
{
    public class WireMoneyVO
    {
        public WireMoneyVO(tblWireMoney entity, string companyName)
        {
            if (entity == null) return;
            ID = entity.WireMoney_id;
            Companyid = entity.Company_id;
            SourceTblid = entity.WireSourceTbl_id;
            InsertDate = entity.wireInsertDate;
            Type = entity.WireType;
            Date = entity.WireDate;
            Amount = entity.WireAmount;
            Currency = entity.WireCurrency;
            ExchangeRate = entity.WireExchangeRate;
            StatusDate = entity.WireStatusDate;
            StatusUser = entity.WireStatusUser;
            Status = entity.WireStatus;
            Comment = entity.WireComment;
            Flag = entity.WireFlag;
            ConfirmationNum = entity.WireConfirmationNum;
            PrintApprovalStatusDate = entity.wirePrintApprovalStatusDate;
            PrintApprovalStatusUser = entity.wirePrintApprovalStatusUser;
            PrintApprovalStatu = entity.wirePrintApprovalStatus;
            CompanyName = entity.wireCompanyName;
            CompanyLegalName = entity.wireCompanyLegalName;
            IDnumber = entity.wireIDnumber;
            CompanyLegalNumber = entity.wireCompanyLegalNumber;
            PaymentMethod = entity.wirePaymentMethod;
            PaymentPayeeName = entity.wirePaymentPayeeName;
            PaymentBank = entity.wirePaymentBank;
            PaymentBranch = entity.wirePaymentBranch;
            PaymentAccount = entity.wirePaymentAccount;
            PaymentAbroadAccountName = entity.wirePaymentAbroadAccountName;
            PaymentAbroadAccountNumber = entity.wirePaymentAbroadAccountNumber;
            PaymentAbroadBankName = entity.wirePaymentAbroadBankName;
            PaymentAbroadBankAddress = entity.wirePaymentAbroadBankAddress;
            PaymentAbroadSwiftNumber = entity.wirePaymentAbroadSwiftNumber;
            PaymentAbroadIBAN = entity.wirePaymentAbroadIBAN;
            PaymentAbroadAccountName2 = entity.wirePaymentAbroadAccountName2;
            PaymentAbroadAccountNumber2 = entity.wirePaymentAbroadAccountNumber2;
            PaymentAbroadBankName2 = entity.wirePaymentAbroadBankName2;
            PaymentAbroadBankAddress2 = entity.wirePaymentAbroadBankAddress2;
            PaymentAbroadSwiftNumber2 = entity.wirePaymentAbroadSwiftNumber2;
            PaymentAbroadIBAN2 = entity.wirePaymentAbroadIBAN2;
            PaymentAbroadABA2 = entity.wirePaymentAbroadABA2;
            PaymentAbroadSortCode2 = entity.wirePaymentAbroadSortCode2;
            isShow = entity.isShow;
            PaymentAbroadABA = entity.wirePaymentAbroadABA;
            PaymentAbroadSortCode = entity.wirePaymentAbroadSortCode;
            PaymentAbroadBankAddressSecond = entity.WirePaymentAbroadBankAddressSecond;
            PaymentAbroadBankAddressCity = entity.WirePaymentAbroadBankAddressCity;
            PaymentAbroadBankAddressState = entity.WirePaymentAbroadBankAddressState;
            PaymentAbroadBankAddressZip = entity.WirePaymentAbroadBankAddressZip;
            PaymentAbroadBankAddressCountry = entity.WirePaymentAbroadBankAddressCountry;
            PaymentAbroadBankAddressSecond2 = entity.WirePaymentAbroadBankAddressSecond2;
            PaymentAbroadBankAddressCity2 = entity.WirePaymentAbroadBankAddressCity2;
            PaymentAbroadBankAddressState2 = entity.WirePaymentAbroadBankAddressState2;
            PaymentAbroadBankAddressZip2 = entity.WirePaymentAbroadBankAddressZip2;
            PaymentAbroadBankAddressCountry2 = entity.WirePaymentAbroadBankAddressCountry2;
            LastLogDate = entity.LastLogDate;
            ProcessingCurrency = entity.WireProcessingCurrency;
            Fee = entity.wireFee;
            PaymentAbroadSepaBic = entity.wirePaymentAbroadSepaBic;
            PaymentAbroadSepaBic2 = entity.wirePaymentAbroadSepaBic2;
            SettlementID = entity.SettlementID;
            PaymentOrderID = entity.PaymentOrderID;
            ApprovalLevel1 = entity.wireApproveLevel1;
            ApprovalLevel2 = entity.wireApproveLevel2;
            if (companyName != null) CompanyName = companyName;
        }

        public int ID { get; set; }
        public int Companyid { get; set; }
        public int SourceTblid { get; set; }
        public System.DateTime InsertDate { get; set; }
        public byte Type { get; set; }
        public System.DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public int Currency { get; set; }
        public double ExchangeRate { get; set; }
        public System.DateTime StatusDate { get; set; }
        public string StatusUser { get; set; }
        public byte Status { get; set; }
        public string Comment { get; set; }
        public byte Flag { get; set; }
        public string ConfirmationNum { get; set; }
        public System.DateTime PrintApprovalStatusDate { get; set; }
        public string PrintApprovalStatusUser { get; set; }
        public bool PrintApprovalStatu { get; set; }
        public string CompanyName { get; set; }
        public string CompanyLegalName { get; set; }
        public string IDnumber { get; set; }
        public string CompanyLegalNumber { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentPayeeName { get; set; }
        public int? PaymentBank { get; set; }
        public string PaymentBranch { get; set; }
        public string PaymentAccount { get; set; }
        public string PaymentAbroadAccountName { get; set; }
        public string PaymentAbroadAccountNumber { get; set; }
        public string PaymentAbroadBankName { get; set; }
        public string PaymentAbroadBankAddress { get; set; }
        public string PaymentAbroadSwiftNumber { get; set; }
        public string PaymentAbroadIBAN { get; set; }
        public string PaymentAbroadAccountName2 { get; set; }
        public string PaymentAbroadAccountNumber2 { get; set; }
        public string PaymentAbroadBankName2 { get; set; }
        public string PaymentAbroadBankAddress2 { get; set; }
        public string PaymentAbroadSwiftNumber2 { get; set; }
        public string PaymentAbroadIBAN2 { get; set; }
        public string PaymentAbroadABA2 { get; set; }
        public string PaymentAbroadSortCode2 { get; set; }
        public string PaymentAbroadABA { get; set; }
        public string PaymentAbroadSortCode { get; set; }
        public string PaymentAbroadBankAddressSecond { get; set; }
        public string PaymentAbroadBankAddressCity { get; set; }
        public string PaymentAbroadBankAddressState { get; set; }
        public string PaymentAbroadBankAddressZip { get; set; }
        public int PaymentAbroadBankAddressCountry { get; set; }
        public string PaymentAbroadBankAddressSecond2 { get; set; }
        public string PaymentAbroadBankAddressCity2 { get; set; }
        public string PaymentAbroadBankAddressState2 { get; set; }
        public string PaymentAbroadBankAddressZip2 { get; set; }
        public int PaymentAbroadBankAddressCountry2 { get; set; }
        public System.Nullable<System.DateTime> LastLogDate { get; set; }
        public int ProcessingCurrency { get; set; }
        public decimal Fee { get; set; }
        public string PaymentAbroadSepaBic { get; set; }
        public string PaymentAbroadSepaBic2 { get; set; }
        public System.Nullable<int> SettlementID { get; set; }
        public System.Nullable<int> PaymentOrderID { get; set; }
        public bool? ApprovalLevel1 { get; set; }
        public bool? ApprovalLevel2 { get; set; }
		public bool isShow { get; set; }
	}
}
