﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class LogPaymentPageTranVO
	{
		public LogPaymentPageTranVO() { }

		public LogPaymentPageTranVO(Netpay.Dal.Netpay.tblLogPaymentPageTran entity)
		{ 
			LogPaymentPageTrans_id = entity.LogPaymentPageTrans_id;
			Date = entity.Lppt_Date;
			LogPaymentPage_id = entity.Lppt_LogPaymentPage_id;
			PaymentMethodType = (PaymentMethodType) entity.Lppt_PaymentMethodType;
			RequestString = entity.Lppt_RequestString;
			ResponseString = entity.Lppt_ResponseString;
			TransNum = entity.Lppt_TransNum;
		}

		public int LogPaymentPageTrans_id { get; set; }
		public DateTime Date { get; set; }
		public int LogPaymentPage_id { get; set; }
		public PaymentMethodType? PaymentMethodType { get; set; }
		public string RequestString { get; set; }
		public string ResponseString { get; set; }
		public int? TransNum { get; set; }
	}
}
