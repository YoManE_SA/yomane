﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Reports;

namespace Netpay.Infrastructure.VO
{
	public class IntegrationServicesLogVO
	{
		public IntegrationServicesLogVO() { }

		public IntegrationServicesLogVO(tblIntegrationServicesLog entity)
		{
			ID = entity.ID;
			PackageName = entity.PackageName;
			StartDate = entity.StartDate;
			EndDate = entity.EndDate;
			IsSuccessful = entity.IsSuccessful;
		}

		public int ID { get; set; }
		public string PackageName { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public bool IsSuccessful { get; set; }
	}
}
