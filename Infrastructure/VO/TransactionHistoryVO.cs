﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class TransactionHistoryVO
	{
		public TransactionHistoryVO() { }

		public TransactionHistoryVO(TransHistory data)
		{
			ID = data.TransHistory_id;
			MerchantID = data.Merchant_id;
			Type = (TransactionHistoryType)data.TransHistoryType_id;
			InsertDate = data.InsertDate;
			ReferenceNumber = data.ReferenceNumber;
			Description = data.Description;
			if (data.TransFail_id != null)
			{
				TransactionID = data.TransFail_id.Value;
				TransactionStatus = Infrastructure.TransactionStatus.Declined;
			}
			else if (data.TransPass_id != null)
			{
				TransactionID = data.TransPass_id.Value;
				TransactionStatus = Infrastructure.TransactionStatus.Captured;
			}
			else if (data.TransPending_id != null)
			{
				TransactionID = data.TransPending_id.Value;
				TransactionStatus = Infrastructure.TransactionStatus.Pending;
			}
			else if (data.TransPreAuth_id != null)
			{
				TransactionID = data.TransPreAuth_id.Value;
				TransactionStatus = Infrastructure.TransactionStatus.Authorized;
			}
		}

		public int ID { get; set; }
		public int MerchantID { get; set; }
		public TransactionHistoryType Type { get; set; }
		public DateTime InsertDate { get; set; }
		public int? ReferenceNumber { get; set; }
		public string Description { get; set; }
		public int TransactionID { get; set; }
		public TransactionStatus TransactionStatus { get; set; }
	}
}
