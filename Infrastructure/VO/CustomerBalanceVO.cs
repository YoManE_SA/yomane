﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class CustomerBalanceVO
	{
		public CustomerBalanceVO() { }

		public CustomerBalanceVO(GetCustomerBalanceTableResult entity)
		{
			Balance = entity.Balance.GetValueOrDefault(0);
			BalanceCurrency = (Currency)entity.CUR_ID;
			Iso = entity.CUR_IsoName;
		}

		public decimal Balance { get; set; }
		public string Iso { get; set; }
		public Currency BalanceCurrency { get; set; }
	}
}
