﻿using System;
using System.Collections.Generic;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class QNAVo
	{
		public int ID { get; set; }
		public int? GroupID { get; set; }
		public string Question { get; set; }
		public string Answer { get; set; }
		public int Rating { get; set; }
		public bool IsVisible { get; set; }
		public QNAVo() {}
		public QNAVo(QNA entity) 
		{
			ID = entity.QNA_id;
			GroupID = entity.QNAGroup_id;
			Question = entity.Question;
			Answer = entity.Answer;
			Rating = entity.Rating;
			IsVisible = entity.IsVisible;
		}
	}
}
