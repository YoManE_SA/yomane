﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.VO
{
	public class PartnerDebitCompanyFeeVO
	{
		public int ID { get; set; }
		public int Affiliate_id { get; set; }
		public int? DebitCompany_id { get; set; }
		public byte? TransType_id { get; set; }
		public int? Currency_id { get; set; }
		public short? PaymentMethod_id { get; set; }
		public decimal AmountTop { get; set; }
		public decimal PercentFee { get; set; }
		public byte CalcMethod { get; set; }

		public PartnerDebitCompanyFeeVO() { }
		public PartnerDebitCompanyFeeVO(Netpay.Dal.Netpay.SetAffiliateDebitCompanyFee entity)
		{
			ID = entity.SetAffiliateDebitCompanyFee_id;
			Affiliate_id = entity.Affiliate_id;
			DebitCompany_id = entity.DebitCompany_id;
			TransType_id = entity.TransType_id;
			Currency_id = entity.Currency_id;
			PaymentMethod_id = entity.PaymentMethod_id;
			AmountTop = entity.AmountTop;
			PercentFee = entity.PercentFee;
			CalcMethod = entity.CalcMethod;
		}
	}
}
