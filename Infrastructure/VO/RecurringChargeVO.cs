﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Domains;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class RecurringChargeVO
	{
		public RecurringChargeVO(){}

		public RecurringChargeVO(string domainHost, tblRecurringCharge entity)
		{
			ID = entity.ID;
			SeriesID = entity.rc_Series;
			Number = entity.rc_ChargeNumber;
			ChargeDate = entity.rc_Date;
			Amount = entity.rc_Amount;
			Currency = DomainsManager.GetDomain(domainHost).Cache.GetCurrency(entity.rc_Currency.Value);
			CurrencyIsoCode = Currency.IsoCode;
			IsPaid = entity.rc_Paid;
			IsPending = entity.rc_Pending;
			IsBlocked = entity.rc_Blocked;
			IsSuspended = entity.rc_Suspended;
			IsSeriesSuspended = entity.tblRecurringSery.rs_Suspended;
			IsSeriesDeleted = entity.tblRecurringSery.rs_Deleted;
			IsSeriesPaid = entity.tblRecurringSery.rs_Paid;
			IsSeriesBlocked = entity.tblRecurringSery.rs_Blocked;
			IsSeriesPreAuthorized = entity.tblRecurringSery.rs_Approval;
			IsChargePreAuthorized = (entity.tblRecurringSery.rs_FirstApproval != null && entity.rc_ChargeNumber == 1) ? (bool)entity.tblRecurringSery.rs_FirstApproval : IsSeriesPreAuthorized;
			Attempts = entity.rc_Attempts;
			if (entity.tblRecurringSery.rs_ECheck != null)
			{
				SeriesPaymentMethodID = (int)PaymentMethodEnum.ECCheck;
				SeriesBinCountryCode = "";
				PaymentMethodDisplay = PaymentMethodEnum.ECCheck.ToString();
			}
			else
			{
				SeriesPaymentMethodID = entity.tblCreditCard.ccTypeID + 20;
				SeriesBinCountryCode = entity.tblCreditCard.BINCountry;
                PaymentMethodVO paymentMethod = DomainsManager.GetDomain(domainHost).Cache.PaymentMethods.Where(pm => pm.ID == SeriesPaymentMethodID).SingleOrDefault();
                if (paymentMethod != null && entity.tblCreditCard != null)
                    PaymentMethodDisplay = paymentMethod.Name + "..." + entity.tblCreditCard.CCard_Last4.ToString("0000");
			}
			if (entity.tblRecurringSery.rs_Approval)
			{
				SeriesTypeText = "PreAuthorized";
			}
			else
			{
				SeriesTypeText = "Debit";
			}
		}

		public int ID { get; set; }
		public int? SeriesID { get; set; }
		public int Number { get; set; }
		public DateTime ChargeDate { get; set; }
		public decimal Amount { get; set; }
		public CurrencyVO Currency { get; set; }
		public bool IsPaid { get; set; }
		public bool IsPending { get; set; }
		public bool IsBlocked { get; set; }
		public bool IsSuspended { get; set; }
		public bool IsSeriesSuspended { get; set; }
		public bool IsSeriesDeleted { get; set; }
		public bool IsSeriesPaid { get; set; }
		public bool IsSeriesBlocked { get; set; }
		public bool IsSeriesPreAuthorized { get; set; }
		public bool IsChargePreAuthorized { get; set; }
		public int SeriesPaymentMethodID { get; set; }
		public string SeriesBinCountryCode { get; set; }
		public string PaymentMethodDisplay { get; set; }
		public string SeriesTypeText { get; set; }
		public string CurrencyIsoCode { get; set; }
		public int Attempts { get; set; }
	}
}
