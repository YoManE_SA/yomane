﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class CreditCardVO
    {
		public CreditCardVO() { }
		
		private string _CardNumber { set; get; }

		public CreditCardVO(tblCreditCard entity) 
		{
			First6Digits = entity.CCard_First6;
			Last4Digits = entity.CCard_Last4;
			BinCountryCode = entity.BINCountry;
			CardholderEmail = entity.email;
			CardholderName = entity.Member;
			CardholderID = entity.PersonalNumber;
			PhoneNumber = entity.phoneNumber;
			CardTypeID = entity.ccTypeID;
			ExpirationMonth = entity.ExpMM;
			ExpirationYear = entity.ExpYY;
		}
		
		public string CardNumber 
		{ 
			get { return _CardNumber; } 
			set 
			{
				if (string.IsNullOrEmpty(_CardNumber = value)) return;
				if (value.Length >= 6) First6Digits = value.Substring(0, 6).ToInt32(0);
				if (value.Length >= 4) Last4Digits = (short)(value.Substring(value.Length - 4, 4).ToInt32(0));
			}
		}

		public int First6Digits { set; get; }
		public short Last4Digits { set; get; }
		public string BinCountryCode { set; get; }
		public string CardholderName { set; get; }
		public string CardholderEmail { set; get; }
		public string CardholderID { set; get; }
		public string PhoneNumber { set; get; }
		public short CardTypeID { set; get; }
		public string ExpirationMonth { set; get; }
		public string ExpirationYear { set; get; }
		public string Cvv { set; get; }
		public int Count { set; get; }
		public int PaymentMethodID {  get { return CardTypeID + 20; }  }

		public override string ToString()
		{
			return string.Format("{0}...{1} - {2}/{3}", First6Digits.ToString("000000"), Last4Digits.ToString("0000"), ExpirationMonth, ExpirationYear);
		}
    }
}
