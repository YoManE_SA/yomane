﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class SecurityGroupVO
    {
        public SecurityGroupVO(tblSecurityGroup entity) 
        {
            ID = entity.ID;
            Name = entity.sg_Name;
            Description = entity.sg_Description;
            SeeUnmanaged = entity.sg_SeeUnmanaged;
            IsActive = entity.sg_IsActive;
        }
        public int ID;
        public string Name;
        public string Description;
        public bool SeeUnmanaged;
        public bool IsActive;
    }
}
