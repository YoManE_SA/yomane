﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.VO
{
	public class ManagedPagePermissionVO
	{
		public ManagedPagePermissionVO(){}
		
		public int PageID { get; set; }
		public int PagePermissionID { get; set; }
		public SecurityObjectType Type { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public bool IsActive { get; set; }
		public bool IsVisible { get; set; }
	}
}
