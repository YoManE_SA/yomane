﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class TransactionAmountTypeVO
	{
		public TransactionAmountTypeVO(TransAmountType entity)
		{
			ID = entity.TransAmountType_id;
			Text = entity.Name;
		}
		public int ID { get; set; }
		public string Text { get; set; }
	}
}
