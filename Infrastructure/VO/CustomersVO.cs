﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class CustomersVO
	{
		public CustomersVO(){}
		public CustomersVO(Domains.Domain domain, tblCustomer entity)
		{
			ID = entity.ID;
			CustomerNumber = entity.CustomerNumber;
			FirstName = entity.FirstName;
			LastName = entity.LastName;
			IDNumber = entity.IDNumber;
			Email = entity.Mail;
			Street = entity.Street;
			City = entity.City;
			ZIP = entity.ZIP;
            Phone = entity.Phone;
			CellPhone = entity.CellPhone;
			Pincode = entity.PinCode;
            if (entity.Country > 0)
            {
                Country = domain.Cache.GetCountry(entity.Country).Name;
                CountryIso = domain.Cache.GetCountry(entity.Country).IsoCode2;
            }
            if (entity.State > 0) 
            {
                State = domain.Cache.GetState(entity.State).Name;
                StateIso = domain.Cache.GetState(entity.State).IsoCode; 
            }
		}

		public int ID { get; set; }
		public string CustomerNumber { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string IDNumber { get; set; }
		public string Email { get; set; }
		public string Street { get; set; }
		public string City { get; set; }
		public string ZIP { get; set; }
		public string State { get; set; }
        public string Country { get; set; }
        public string StateIso { get; set; }
        public string CountryIso { get; set; }
        public string Phone { get; set; }
		public string CellPhone { get; set; }
        public string Pincode { get; set; }
	}
}
