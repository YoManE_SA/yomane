﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Domains;

namespace Netpay.Infrastructure.VO
{
    public class AddressVO
    {
		public AddressVO() { }

		public AddressVO(tblBillingAddress entity) 
		{
			ID = entity.id;
			Address1 = entity.address1;
			Address2 = entity.address2;
			City = entity.city;
			Zipcode = entity.zipCode;
			StateID = entity.stateId;
			CountryID = entity.countryId;
		}

		public AddressVO(string domainHost, tblBillingAddress entity) : this(entity)
		{
			Domain domain = Domains.DomainsManager.GetDomain(domainHost);
			CountryVO country = domain.Cache.GetCountry(entity.countryId);
			if (country != null)
				CountryIso = country.IsoCode3;
			StateVO state = domain.Cache.GetState(entity.stateId);
			if (state != null)
				StateIso = state.IsoCode;
		}
		/*
		public AddressVO(ShippingDetail entity)
		{
			ID = entity.ShippingDetail_id;
			Title = entity.Title;
			Address1 = entity.AddressLine1;
			Address2 = entity.AddressLine2;
			City = entity.AddressCity;
			Zipcode = entity.AddressPostalCode;
			StateIso = entity.AddressStateISOCode;
			CountryIso = entity.AddressCountryISOCode;
		}
		*/
		public int ID { get; set; }
		public string Title { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string Zipcode { get; set; }
		public int StateID { get; set; }
		public int CountryID { get; set; }
		public string StateIso { get; set; }
		public string CountryIso { get; set; }
    }
}
