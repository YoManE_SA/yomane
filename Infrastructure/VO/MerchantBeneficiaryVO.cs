﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class MerchantBeneficiaryVO
	{
		public MerchantBeneficiaryVO(){}

		public MerchantBeneficiaryVO(tblCompanyMakePaymentsProfile entity) 
		{
			ID = entity.CompanyMakePaymentsProfiles_id;
			MerchantID = entity.company_id;
			ProfileType = entity.ProfileType;
			ProfileTypeEnum = (MerchantBeneficiaryProfileType)entity.ProfileType;
			BasicInfoCostumerNumber = entity.basicInfo_costumerNumber;
			BasicInfoCostumerName = entity.basicInfo_costumerName;
			BasicInfoContactPersonName = entity.basicInfo_contactPersonName;
			BasicInfoPhoneNumber = entity.basicInfo_phoneNumber;
			BasicInfoFaxNumber = entity.basicInfo_faxNumber;
			BasicInfoEmail = entity.basicInfo_email;
			BasicInfoAddress = entity.basicInfo_address;
			BasicInfoComment = entity.basicInfo_comment;
			BankIsraelInfoPayeeName = entity.bankIsraelInfo_PayeeName;
			BankIsraelInfoCompanyLegalNumber = entity.bankIsraelInfo_CompanyLegalNumber;
			BankIsraelInfoPersonalIDNumber = entity.bankIsraelInfo_personalIdNumber;
			BankIsraelInfoBankBranch = entity.bankIsraelInfo_bankBranch;
			BankIsraelInfoAccountNumber = entity.bankIsraelInfo_AccountNumber;
			BankIsraelInfoPaymentMethod = entity.bankIsraelInfo_PaymentMethod;
			BankIsraelInfoBankCode = entity.bankIsraelInfo_BankCode;
			BankAbroadAccountName = entity.bankAbroadAccountName;
			BankAbroadAccountNumber = entity.bankAbroadAccountNumber;
			BankAbroadBankName = entity.bankAbroadBankName;
			BankAbroadBankAddress = entity.bankAbroadBankAddress;
			BankAbroadSwiftNumber = entity.bankAbroadSwiftNumber;
			BankAbroadIBAN = entity.bankAbroadIBAN;
			BankAbroadABA = entity.bankAbroadABA;
			BankAbroadSortCode = entity.bankAbroadSortCode;
			BankAbroadAccountName2 = entity.bankAbroadAccountName2;
			BankAbroadAccountNumber2 = entity.bankAbroadAccountNumber2;
			BankAbroadBankName2 = entity.bankAbroadBankName2;
			BankAbroadBankAddress2 = entity.bankAbroadBankAddress2;
			BankAbroadSwiftNumber2 = entity.bankAbroadSwiftNumber2;
			BankAbroadIBAN2 = entity.bankAbroadIBAN2;
			BankAbroadABA2 = entity.bankAbroadABA2;
			BankAbroadSortCode2 = entity.bankAbroadSortCode2;
			BankAbroadBankAddressSecond = entity.bankAbroadBankAddressSecond;
			BankAbroadBankAddressCity = entity.bankAbroadBankAddressCity;
			BankAbroadBankAddressState = entity.bankAbroadBankAddressState;
			BankAbroadBankAddressZip = entity.bankAbroadBankAddressZip;
			BankAbroadBankAddressCountry = entity.bankAbroadBankAddressCountry;
			BankAbroadBankAddressSecond2 = entity.bankAbroadBankAddressSecond2;
			BankAbroadBankAddressCity2 = entity.bankAbroadBankAddressCity2;
			BankAbroadBankAddressState2 = entity.bankAbroadBankAddressState2;
			BankAbroadBankAddressZip2 = entity.bankAbroadBankAddressZip2;
			BankAbroadBankAddressCountry2 = entity.bankAbroadBankAddressCountry2;
			BankAbroadSepaBic = entity.bankAbroadSepaBic;
			BankAbroadSepaBic2 = entity.bankAbroadSepaBic2;
			IsSystem = entity.isSystem;
		}

		public int ID { get; set; }
		public int? MerchantID { get; set; }
		public byte ProfileType { get; set; }
		public MerchantBeneficiaryProfileType ProfileTypeEnum { get; set; }
		public string BasicInfoCostumerNumber { get; set; }
		public string BasicInfoCostumerName { get; set; }
		public string BasicInfoContactPersonName { get; set; }
		public string BasicInfoPhoneNumber { get; set; }
		public string BasicInfoFaxNumber { get; set; }
		public string BasicInfoEmail { get; set; }
		public string BasicInfoAddress { get; set; }
		public string BasicInfoComment { get; set; }
		public string BankIsraelInfoPayeeName { get; set; }
		public string BankIsraelInfoCompanyLegalNumber { get; set; }
		public string BankIsraelInfoPersonalIDNumber { get; set; }
		public string BankIsraelInfoBankBranch { get; set; }
		public string BankIsraelInfoAccountNumber { get; set; }
		public string BankIsraelInfoPaymentMethod { get; set; }
		public string BankIsraelInfoBankCode { get; set; }
		public string BankAbroadAccountName { get; set; }
		public string BankAbroadAccountNumber { get; set; }
		public string BankAbroadBankName { get; set; }
		public string BankAbroadBankAddress { get; set; }
		public string BankAbroadSwiftNumber { get; set; }
		public string BankAbroadIBAN { get; set; }
		public string BankAbroadABA { get; set; }
		public string BankAbroadSortCode { get; set; }
		public string BankAbroadAccountName2 { get; set; }
		public string BankAbroadAccountNumber2 { get; set; }
		public string BankAbroadBankName2 { get; set; }
		public string BankAbroadBankAddress2 { get; set; }
		public string BankAbroadSwiftNumber2 { get; set; }
		public string BankAbroadIBAN2 { get; set; }
		public string BankAbroadABA2 { get; set; }
		public string BankAbroadSortCode2 { get; set; }
		public string BankAbroadBankAddressSecond { get; set; }
		public string BankAbroadBankAddressCity { get; set; }
		public string BankAbroadBankAddressState { get; set; }
		public string BankAbroadBankAddressZip { get; set; }
		public int BankAbroadBankAddressCountry { get; set; }
		public string BankAbroadBankAddressSecond2 { get; set; }
		public string BankAbroadBankAddressCity2 { get; set; }
		public string BankAbroadBankAddressState2 { get; set; }
		public string BankAbroadBankAddressZip2 { get; set; }
		public int BankAbroadBankAddressCountry2 { get; set; }
		public string BankAbroadSepaBic { get; set; }
		public string BankAbroadSepaBic2 { get; set; }
		public bool IsSystem { get; set; }
	}
}
