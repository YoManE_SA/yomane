﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class MerchantSettingsHostedVO
	{
		public MerchantSettingsHostedVO() { }

		public MerchantSettingsHostedVO(tblCompanySettingsHosted entity)
		{
			MerchantID = entity.CompanyID;
			IsEnabled = entity.IsEnabled;
			IsCreditCard = entity.IsCreditCard;
			IsCreditCardRequiredEmail = entity.IsCreditCardRequiredEmail;
			IsCreditCardRequiredPhone = entity.IsCreditCardRequiredPhone;
			IsCreditCardRequiredID = entity.IsCreditCardRequiredID;
			IsCreditCardRequiredCVV = entity.IsCreditCardRequiredCVV;
			IsEcheck = entity.IsEcheck;
			IsEcheckRequiredEmail = entity.IsEcheckRequiredEmail;
			IsEcheckRequiredPhone = entity.IsEcheckRequiredPhone;
			IsEcheckRequiredID = entity.IsEcheckRequiredID;
			IsDirectDebit = entity.IsDirectDebit;
			IsDirectDebitRequiredEmail = entity.IsDirectDebitRequiredEmail;
			IsDirectDebitRequiredPhone = entity.IsDirectDebitRequiredPhone;
			IsDirectDebitRequiredID = entity.IsDirectDebitRequiredID;
			IsCustomer = entity.IsCustomer;
			IsPhoneDebit = entity.IsPhoneDebit;
			IsPhoneDebitRequiredEmail = entity.IsPhoneDebitRequiredEmail;
			IsPhoneDebitRequiredPhone = entity.IsPhoneDebitRequiredPhone;
			IsSignatureOptional = entity.IsSignatureOptional;
			IsWhiteLabel = entity.IsWhiteLabel.GetValueOrDefault();
			HideCustomParametersInRedirection = entity.HideCustomParametersInRedirection;
			IsWebMoney = entity.IsWebMoney;
			WebMoneyPurse = entity.WebMoneyMerchantPurse;
			WebMoneySecretKey = entity.WebMoneySecretKey;
			IsShowAddress1 = entity.IsShowAddress1;
			IsShowAddress2 = entity.IsShowAddress2;
			IsShowCity = entity.IsShowCity;
			IsShowZipCode = entity.IsShowZipCode;
			IsShowState = entity.IsShowState;
			IsShowCountry = entity.IsShowCountry;
			IsShowEmail = entity.IsShowEmail;
			IsShowPhone = entity.IsShowPhone;
			IsShowPersonalID = entity.IsShowPersonalID;
			IsPayPal = entity.IsPayPal;
			IsGoogleCheckout = entity.IsGoogleCheckout;
			IsShippingAddressRequire = entity.IsShippingAddressRequired;
			PayPalMerchantID = entity.PayPalMerchantID;
			GoogleCheckoutMerchantID = entity.GoogleCheckoutMerchantID;
			IsMoneyBookers = entity.IsMoneyBookers;
			MoneyBookersAccount = entity.MoneyBookersAccount;
			LogoImage = entity.LogoPath;
			RedirectionUrl = entity.RedirectionUrl;
			NotificationUrl = entity.NotificationUrl;
			IsNotification = !String.IsNullOrWhiteSpace(NotificationUrl);
			MerchantTextDefaultLanguage = (CommonTypes.Language) entity.MerchantTextDefaultLanguage.GetValueOrDefault(1); /*default to english*/
			UIVersion = entity.UIVersion;
			ThemeCssFileName = entity.ThemeCssFileName;
            //Languages = entity.tblMerchantLanguagesHosteds.ToDictionary(x => (Language)x.LanguageID, x => new MerchantSettingsHostedLanguageVO(x));
		}

		public int MerchantID { get; set; }
		public bool IsEnabled { get; set; }
		public bool IsCreditCard { get; set; }
		public bool IsCreditCardRequiredEmail { get; set; }
		public bool IsCreditCardRequiredPhone { get; set; }
		public bool IsCreditCardRequiredID { get; set; }
		public bool IsCreditCardRequiredCVV { get; set; }
		public bool IsEcheck { get; set; }
		public bool IsEcheckRequiredEmail { get; set; }
		public bool IsEcheckRequiredPhone { get; set; }
		public bool IsEcheckRequiredID { get; set; }
		public bool IsDirectDebit { get; set; }
		public bool IsDirectDebitRequiredEmail { get; set; }
		public bool IsDirectDebitRequiredPhone { get; set; }
		public bool IsDirectDebitRequiredID { get; set; }
		public bool IsCustomer { get; set; }
		public bool IsPhoneDebit { get; set; }
		public bool IsPhoneDebitRequiredEmail { get; set; }
		public bool IsPhoneDebitRequiredPhone { get; set; }
		public bool IsSignatureOptional { get; set; }
		public bool HideCustomParametersInRedirection { get; set; }
		public bool IsWebMoney { get; set; }
		public bool IsPayPal { get; set; }
		public bool IsGoogleCheckout { get; set; }
		public bool IsWhiteLabel { get; set; }
		public bool IsShowAddress1 { get; set; }
		public bool IsShowAddress2 { get; set; }
		public bool IsShowCity { get; set; }
		public bool IsShowZipCode { get; set; }
		public bool IsShowState { get; set; }
		public bool IsShowCountry { get; set; }
		public bool IsShowEmail { get; set; }
		public bool IsShowPhone { get; set; }
		public bool IsShowPersonalID { get; set; }
		public bool IsShippingAddressRequire { get; set; }
		public string WebMoneyPurse { get; set; }
		public string WebMoneySecretKey { get; set; }
		public string PayPalMerchantID { get; set; }
		public string GoogleCheckoutMerchantID { get; set; }
		public bool IsMoneyBookers { get; set; }
		public string MoneyBookersAccount { get; set; }
		public string LogoImage { get; set; }
		public string NotificationUrl { get; set; }
		public string RedirectionUrl { get; set; }
		public bool IsNotification { get; set; }
		public byte UIVersion { get; set; }
		public string ThemeCssFileName { get; set; }
		public CommonTypes.Language MerchantTextDefaultLanguage { get; set; }

		//public Dictionary<Language, MerchantSettingsHostedLanguageVO> Languages { get; set; }
	}
}
