﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class CountryVO
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string IsoCode3 { get; set; }
		public string IsoCode2 { get; set; }
		public bool IsAbaRequired { get; set; }
		public int IbanLength { get; set; }
		public bool IsSepa { get; set; }
		//public Language Language { get; set; }
	}
}