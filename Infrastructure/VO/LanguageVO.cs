﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class LanguageVO
	{
		public LanguageVO(){}

		public LanguageVO(LanguageList entity)
		{
			EnglishName = entity.Name;
			NativeName = entity.NativeName;
			Culture = entity.Culture;
            LanguageID = entity.Language_id;
		}

		public string EnglishName { get; set; }
		public string NativeName { get; set; }
		public string Culture { get; set; }
        public byte LanguageID { get; set; }
        public CommonTypes.Language Language { get { return (CommonTypes.Language)LanguageID; } set { LanguageID = (byte) value; } }
    }
}
