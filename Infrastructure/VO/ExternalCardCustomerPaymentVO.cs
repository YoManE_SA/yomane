﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class ExternalCardCustomerPaymentVO
	{
		public ExternalCardCustomerPaymentVO() {}

		public ExternalCardCustomerPaymentVO(tblExternalCardCustomerPayment entity) 
		{
			ID = entity.ExternalCardCustomerPayment_id;
			CustomerID = entity.ExternalCardCustomer_id;
			CustomerName = entity.tblExternalCardCustomer.Name;
			PaymentID = entity.UniqueID;
			Amount = entity.Amount;
			Result = entity.Result;
			ResultDescription = entity.ResultDescription;
			InsertDate = entity.InsertDate;
		}

		public int ID { get; set; }
		public int CustomerID { get; set; }
		public string CustomerName { get; set; }
		public Guid PaymentID { get; set; }
		public decimal Amount { get; set; }
		public string Result { get; set; }
		public string ResultDescription { get; set; }
		public DateTime InsertDate { get; set; }
	}
}
