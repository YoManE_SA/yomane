﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class MerchantBalanceVO
	{
		public MerchantBalanceVO() { }

		public MerchantBalanceVO(tblCompanyBalance entity)
		{
			CompanyBalanceID = entity.companyBalance_id;
			SourceTableID = entity.sourceTbl_id;
			SourceType = entity.sourceType;
			SourceInfo = entity.sourceInfo;
			InsertDate = entity.insertDate;
			Amount = entity.amount;
			CurrencyID = entity.currency;
			Balance = entity.BalanceExpected;
			CompanyBalanceStatus = entity.status;
			Comment = entity.comment;
			//PaymentType = entity.tblCompanyMakePaymentsRequests.paymentType;
		}		

		public int CompanyBalanceID { get; set; }
		public int SourceTableID { get; set; }
		public byte SourceType { get; set; }
		public string SourceInfo { get; set; }
		public DateTime InsertDate { get; set; }
		public decimal Amount { get; set; }
		public int? CurrencyID { get; set; }
		public decimal Balance { get; set; }
		public byte CompanyBalanceStatus { get; set; }
		public string Comment { get; set; }
		//public byte PaymentType { get; set; }
	}
}
