﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;
using System.Runtime.Serialization;

namespace Netpay.Infrastructure.VO
{
    [Serializable]
    [DataContract]
	public class MerchantVO
	{
		public MerchantVO() { }

		public MerchantVO(tblCompany entity)
		{
			if (entity == null)
				return;
			
			ID = entity.ID;
			Status = ((MerchantStatus)entity.ActiveStatus);
			Number = entity.CustomerNumber;
			Name = entity.CompanyName;
			LegalName = entity.CompanyLegalName;
			EMail = entity.Mail;
			UserName = entity.UserName;

			Phone = entity.CompanyPhone;
			Country = entity.CompanyCountry;
			State = entity.CompanyState;
			City = entity.CompanyCity;
			Address = entity.CompanyStreet;
			Zipcode = entity.CompanyZIP;

			ContactPhone = entity.Phone;
			ContactCountry = entity.Country;
			ContactCity = entity.City;
			ContactAddress = entity.Street;
			ContactZipcode = entity.ZIP;
			ContactState = entity.State;

			Url = entity.URL;
			Fax = entity.CompanyFax;
			ContactMobilePhone = entity.cellular;
			IsSystemPayCVV2 = entity.IsSystemPayCVV2;
			IsSystemPayPhoneNumber = entity.IsSystemPayPhoneNumber;
			IsSystemPayPersonalNumber = entity.IsSystemPayPersonalNumber;
			IsSystemPayEmail = entity.IsSystemPayEmail;
			IsRefund = entity.IsRefund;
			IsAskRefund = entity.IsAskRefund;
			IsConfirmation = entity.IsConfirmation;
			IsBillingAddressMust = entity.IsBillingAddressMust;
			IsBillingAddressMustIDebit = entity.IsBillingAddressMustIDebit;
			SupportEmail = entity.merchantSupportEmail;
			SupportPhone = entity.merchantSupportPhoneNum;
			ContactFirstName = entity.FirstName;
			ContactLastName = entity.LastName;
			IsCustomerPurchaseEmail = entity.IsCustomerPurchaseEmail;
			IsCustomerPurchasePersonalNumber = entity.IsCustomerPurchasePersonalNumber;
			IsCustomerPurchasePhoneNumber = entity.IsCustomerPurchasePhoneNumber;
			HashKey = entity.HashKey;
			IsSystemPayCreditCard = entity.IsSystemPay;
			IsSystemPayECheck = entity.IsSystemPayEcheck;
			PayPercent = entity.PayPercent;
			IsApprovalOnly = entity.IsApprovalOnly;
			IsRemoteChargeCVV2 = entity.IsRemoteChargeCVV2;
			IsRemoteChargePersonalNumber = entity.IsRemoteChargePersonalNumber;
			IsRemoteChargePhoneNumber = entity.IsRemoteChargePhoneNumber;
			IsRemoteChargeEmail = entity.IsRemoteChargeEmail;
			Descriptor = entity.descriptor;
			IsPassNotificationSentToPayer = entity.IsSendUserConfirmationEmail;
			IsPassNotificationSentToMerchant = entity.IsMerchantNotifiedOnPass.GetValueOrDefault(false);

			//risk
			IsCreditCardWhitelistEnabled = entity.IsCcWhiteListEnabled.GetValueOrDefault(false);
			if (entity.countryBlackList != null && entity.countryBlackList.Trim() != "")
				CountryBlackList = entity.countryBlackList.Split(',').ToList();
			if (entity.AllowedAmounts != null && entity.AllowedAmounts.Trim() != "")
				AllowedAmounts = entity.AllowedAmounts.Split(',').Select(amount => decimal.Parse(amount)).ToList();
			SecurityKey = entity.SecurityKey;
			if (entity.ForceRecurringMD5 != null)
				ForceRecurringMD5 = entity.ForceRecurringMD5.Value;
			BillingCompanysId = entity.BillingCompanys_id.GetValueOrDefault();
			IsChargeVAT = entity.isChargeVAT;
			IsBillingCityOptional = entity.IsBillingCityOptional;
			GradedFeeCurrency = (Currency) entity.CFF_Currency.GetValueOrDefault((int)Currency.Unknown);
            DepositInfo = new MerchantDepositVO(entity);
		}

		[DataMember]
		public int ID { get; set; }
		public MerchantStatus? Status { get; set; }
		public string Number { get; set; }
		//public decimal HandlingFee { get; set; }
		public string Name { get; set; }
		public string LegalName { get; set; }
		public string EMail { get; set; }
		public string UserName { get; set; }

		public string Phone { get; set; }
		public int? Country { get; set; }
		public string State { get; set; }
		public string City { get; set; }
		public string Address { get; set; }
		public string Zipcode { get; set; }

		public int? ContactCountry { get; set; }
		public string ContactState { get; set; }
		public string ContactCity { get; set; }
		public string ContactAddress { get; set; }
		public string ContactZipcode { get; set; }
		public string ContactFirstName { get; set; }
		public string ContactLastName { get; set; }
		public string ContactPhone { get; set; }
		public string ContactMobilePhone { get; set; }

		public string Url { get; set; }
		public string Fax { get; set; }
		public string SupportEmail { get; set; }
		public string SupportPhone { get; set; }
		public string HashKey { get; set; }
		public bool IsSystemPayCVV2 { get; set; }
		public bool IsApprovalOnly { get; set; }
		/// <summary>
		/// Is merchant allowed to use cc virtual terminal
		/// </summary>
		public bool IsSystemPayCreditCard { get; set; }
		/// <summary>
		/// Is merchant allowed to use echeck virtual terminal
		/// </summary>
		public bool IsSystemPayECheck { get; set; }
		public bool IsSystemPayPhoneNumber { get; set; }
		public bool IsSystemPayPersonalNumber { get; set; }
		public bool IsSystemPayEmail { get; set; }
		public bool IsRefund { get; set; }
		public bool IsAskRefund { get; set; }
		public bool IsConfirmation { get; set; }
		public bool IsBillingAddressMust { get; set; }
		public bool IsBillingCityOptional { get; set; }
		public bool IsBillingAddressMustIDebit { get; set; }
		public bool IsCustomerPurchasePersonalNumber { get; set; }
		public bool IsCustomerPurchasePhoneNumber { get; set; }
		public bool IsCustomerPurchaseEmail { get; set; }

		public bool IsRemoteChargeCVV2 { get; set; }
		public bool IsRemoteChargePersonalNumber { get; set; }
		public bool IsRemoteChargePhoneNumber { get; set; }
		public bool IsRemoteChargeEmail { get; set; }
		public string Descriptor { get; set; }

		public decimal PayPercent { get; set; }
		public List<string> CountryBlackList { get; set; }
		public List<decimal> AllowedAmounts { get; set; }
		public string SecurityKey { get; set; }

		public bool IsPassNotificationSentToPayer { get; set; }
		public bool IsPassNotificationSentToMerchant { get; set; }
		public bool ForceRecurringMD5 { get; set; }

		public int BillingCompanysId { get; set; }
		public bool IsChargeVAT { get; set; }
		public MerchantDepositVO DepositInfo { get; private set; }
		public Currency GradedFeeCurrency { get; set; }

		public bool IsCreditCardWhitelistEnabled { get; set; }
	}
}
