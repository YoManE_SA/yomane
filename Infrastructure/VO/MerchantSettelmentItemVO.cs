﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class MerchantSettelmentItemVO
	{
		public int ID { get; set; }
		public int Settelment_id { get; set; }
		public string ItemText { get; set; }
		public int Quantity { get; set; }
		public decimal? Amount { get; set; }
		public decimal Total { get; set; }
		public int? TransAmountType_id { get; set; }
		/*
		public MerchantSettelmentItemVO(MerchantSettelmentItem entity)
		{
			ID = entity.MerchantSettelmentItem_id;
			Settelment_id = entity.MerchantSettelment_id;
			ItemText = entity.ItemText;
			Quantity = entity.Quantity;
			Amount = entity.Amount;
			Total = entity.Total;
			TransAmountType_id = entity.TransAmountType_id;
		}
		*/
	}
}
