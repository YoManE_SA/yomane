﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.VO
{
    public class MerchantMobileAppSettingsVO
    {
        public string MerchantNumber { set; get; }
        public bool IsMobileAppEnabled { set; get; }
        public bool IsEmailRequired { set; get; }
        public bool IsPersonalNumberRequired { set; get; }
        public bool IsCardNotPresent { set; get; }
        public bool IsInstallments { set; get; }
        public bool IsRefund { set; get; }
        public bool IsAuthorization { set; get; }
        public bool IsFullNameRequired { set; get; }
        public bool IsPhoneRequired { set; get; }
        public bool IsOwnerSignRequired { set; get; }
		public bool IsAllowTaxRateChange { set; get; }
        public string MerchantName { set; get; }
        public string MerchantAddress { set; get; }
        public decimal? ValueAddedTax { get; set; }
        public string SupportedCurrencies { get; set; }

        public MerchantMobileAppSettingsVO() { }
        public MerchantMobileAppSettingsVO(Netpay.Dal.Netpay.SetMerchantMobileApp entity, Netpay.Dal.Netpay.tblCompany merchant, string currencies) 
        {
            MerchantNumber = merchant.CustomerNumber;
            IsAuthorization = entity.IsAllowAuthorization;
            IsCardNotPresent = entity.IsAllowCardNotPresent;
            IsEmailRequired = entity.IsRequireEmail;
            IsMobileAppEnabled = entity.IsEnableMobileApp;
            IsPersonalNumberRequired = entity.IsRequirePersonalNumber;
            IsRefund = entity.IsAllowRefund;
            IsInstallments = entity.IsAllowInstallments;
            MerchantAddress = merchant.City + ", " + merchant.Street + ", " + merchant.Country;
            MerchantName = merchant.FirstName + " " + merchant.LastName;
            IsFullNameRequired = entity.IsRequireFullName;
            IsPhoneRequired = entity.IsRequirePhoneNumber;
            IsOwnerSignRequired = entity.IsRequireCardholderSignature;
			IsAllowTaxRateChange = entity.IsAllowTaxRateChange;
			ValueAddedTax = entity.ValueAddedTax;
            SupportedCurrencies = currencies;
        }

    }
}
