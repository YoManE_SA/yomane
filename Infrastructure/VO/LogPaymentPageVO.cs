﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class LogPaymentPageVO
	{
		public LogPaymentPageVO(){}

		public LogPaymentPageVO(tblLogPaymentPage entity) 
		{ 
			PaymentPage_id = entity.LogPaymentPage_id;
			MerchantNumber = entity.Lpp_MerchantNumber;
			RemoteAddress = entity.Lpp_RemoteAddress;
			RequestMethod = entity.Lpp_RequestMethod;
			PathTranslate = entity.Lpp_PathTranslate;
			HttpHost = entity.Lpp_HttpHost;
			HttpReferer = entity.Lpp_HttpReferer;
			QueryString = entity.Lpp_QueryString;
			RequestForm = entity.Lpp_RequestForm;
			SessionContents = entity.Lpp_SessionContents;
			ReplyCode = entity.Lpp_ReplyCode;
			ReplyDesc = entity.Lpp_ReplyDesc;
			TransNum = entity.Lpp_TransNum;
			DateStart = entity.Lpp_DateStart;
			DateEnd = entity.Lpp_DateEnd;
			LocalAddr = entity.Lpp_LocalAddr;
			IsSecure = entity.Lpp_IsSecure;
		}

		public int PaymentPage_id { get; set; }
		public short? TransactionType_id { get; set; }
		public string MerchantNumber { get; set; }
		public string RemoteAddress { get; set; }
		public string RequestMethod { get; set; }
		public string PathTranslate { get; set; }
		public string HttpHost { get; set; }
		public string HttpReferer { get; set; }
		public string QueryString { get; set; }
		public string RequestForm { get; set; }
		public string SessionContents { get; set; }
		public string ReplyCode { get; set; }
		public string ReplyDesc { get; set; }
		public int? TransNum { get; set; }
		public System.DateTime? DateStart { get; set; }
		public System.DateTime? DateEnd { get; set; }
		public string TimeString { get; set; }
		public string LocalAddr { get; set; }
		public bool? IsSecure { get; set; }
		public int? DebitCompanyID { get; set; }
		//public EntityRef<tblDebitCompany> tblDebitCompany { get; set; }
	}
}
