﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Domains;


namespace Netpay.Infrastructure.VO
{
	public class ProductPurchasedInRangeVO
	{
		public int ID { set; get; }
		public string ProductName { set; get; }
		public string MerchantNumber { set; get; }
		public double Latitude { set; get; }
        public double Longitude { set; get; }
        public string ProductImage { set; get; }
		
		public ProductPurchasedInRangeVO() {}
		public ProductPurchasedInRangeVO(Domain domain, SpGetProductPurchasedInRangeResult value)
		{
			ID = value.MerchantProduct_id.GetValueOrDefault();
			ProductName = value.MerchantProductName;
			MerchantNumber = value.MerchantNumber;
			Latitude = value.Latitude.GetValueOrDefault();
			Longitude = value.Longitude.GetValueOrDefault();
            ProductImage = domain.WebServicesUrl + "GlobalData/PublicPageLogos/" + value.MerchantProductImage;
		}
	}
}
