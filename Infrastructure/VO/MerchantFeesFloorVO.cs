﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;


namespace Netpay.Infrastructure.VO
{
	public class MerchantFeesFloorVO
	{
		public MerchantFeesFloorVO(tblCompanyFeesFloor entity)
		{
			ID = entity.CFF_ID;
			MerchantID = entity.CFF_CompanyID;
			TotalTo = entity.CFF_TotalTo;
			Precent = entity.CFF_Precent;
		}

		public MerchantFeesFloorVO(tblPaymentFeesFloor entity)
		{
			ID = entity.CFF_ID;
			MerchantID = entity.CFF_CompanyID;
			TotalTo = entity.CFF_TotalTo;
			Precent = entity.CFF_Precent;
		}

		public int ID { get; set; }
		public int MerchantID { get; set; }
		public decimal TotalTo { get; set; }
		public decimal Precent { get; set; }
	}
}
