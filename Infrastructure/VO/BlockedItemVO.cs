﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class BlockedItemVO
	{
		public BlockedItemVO(){}

		public BlockedItemVO(tblBLCommon entity) 
		{
			ID = entity.BL_ID;
			InsertDate = entity.BL_InsertDate;
			MerchantID = entity.BL_CompanyID;
			Value = entity.BL_Value;
			Comment = entity.BL_Comment;
			Type = (BlockedItemType)entity.BL_Type;
			Source = (BlockedItemSource)entity.BL_BlockSourceID;
		}

		public int ID { get; set; }
		public DateTime InsertDate { get; set; }
		public int? MerchantID { get; set; }
		public string Value { get; set; }
		public string Comment { get; set; }
		public BlockedItemType Type { get; set; }
		public BlockedItemSource Source { get; set; }
	}
}
