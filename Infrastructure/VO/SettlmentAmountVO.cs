﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class SettlementAmountVO
	{
		public SettlementAmountVO() { }

		public SettlementAmountVO(tblSettlementAmount sa, tblTransactionAmount ta) 
		{
			ID = sa.ID;
			SettlementID = ta.SettlementID;
			TypeID = (TransactionAmountType)ta.TypeID;
			InsertDate = ta.InsertDate;
			MerchantID = ta.MerchantID.GetValueOrDefault();
			SettledCount = sa.ItemCount;
			SettledAmount = ta.SettledAmount;
			SettledCurrency = ta.SettledCurrency;
			ReleaseDate = sa.ReleaseDate;
			Description = sa.Description;
			ReferenceNumber = sa.ReferenceNumber;
		}

		public int ID { get; set; }
		public int? SettlementID { get; set; }
		public TransactionAmountType TypeID { get; set; }
		public System.DateTime InsertDate { get; set; }
		public int MerchantID { get; set; }
		public int? SettledCount { get; set; }
		public decimal? SettledAmount { get; set; }
		public int? SettledCurrency { get; set; }
		public System.DateTime? ReleaseDate { get; set; }
		public string Description { get; set; }
		public int? ReferenceNumber { get; set; }

	}
}
