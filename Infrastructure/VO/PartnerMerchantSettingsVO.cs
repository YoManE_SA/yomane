﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class PartnerMerchantSettingsVO
    {
        public PartnerMerchantSettingsVO() { }
      
        public PartnerMerchantSettingsVO(SetMerchantAffiliate settings) 
        {
			ID = settings.SetMerchantAffiliate_id;
            UserID = settings.UserID;
			MerchantId = settings.Merchant_id;
			PartnerId = settings.Affiliate_id;
			SyncDate = settings.SyncDateTime;
			ConfigurationValues = System.Web.HttpUtility.ParseQueryString(settings.ConfigurationValues == null ? "" : settings.ConfigurationValues);
        }
		
		public int ID { get; set; }
        public string UserID { get; set; }
		public int MerchantId { get; set; }
		public int PartnerId { get; set; }
		public DateTime? SyncDate { get; set; }
		public System.Collections.Specialized.NameValueCollection ConfigurationValues { get; set; }
    }
}
