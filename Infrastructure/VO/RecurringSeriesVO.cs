﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Domains;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class RecurringSeriesVO
	{
		public RecurringSeriesVO(){}

		public RecurringSeriesVO(string domainHost, tblRecurringSery entity) : this(domainHost, entity, false) { }

		public RecurringSeriesVO(string domainHost, tblRecurringSery entity, bool preLoadCharges)
		{
			ID = entity.ID;
			IsPrecreated = entity.rs_IsPrecreated.GetValueOrDefault(false);
			Currency = DomainsManager.GetDomain(domainHost).Cache.GetCurrency(entity.rs_Currency.Value);
			CurrencyIsoCode = Currency.IsoCode;
			IsPreAuthorized = entity.rs_Approval;
			StartDate = entity.rs_StartDate;
			Comment = entity.rs_Comments;
			IsSuspended = entity.rs_Suspended;
			IsDeleted = entity.rs_Deleted;
			IsBlocked = entity.rs_Blocked;
			Amount = entity.rs_SeriesTotal;
			IsFlexible = entity.rs_Flexible;
			ChargeCount = entity.rs_ChargeCount;
			ChargeIntervalUnit = entity.rs_IntervalUnit;
			ChargeIntervalLength = entity.rs_IntervalLength;
			IsPaid = entity.rs_Paid;
			if (IsPrecreated)
			{
			}
			else
			{
				if (entity.rs_ECheck != null)
				{
                    PaymentMethodID = (int)PaymentMethodEnum.ECCheck;
					BinCountryCode = "";
                    PaymentMethodDisplay = PaymentMethodEnum.ECCheck.ToString();
				}
				else
				{
					PaymentMethodID = entity.tblCreditCard.ccTypeID + 20;
					BinCountryCode = entity.tblCreditCard.BINCountry;
                    PaymentMethodVO paymentMethod = DomainsManager.GetDomain(domainHost).Cache.PaymentMethods.Where(pm => pm.ID == PaymentMethodID).SingleOrDefault();
                    if (paymentMethod != null && entity.tblCreditCard != null)
					    PaymentMethodDisplay = paymentMethod.Name + "..." + entity.tblCreditCard.CCard_Last4.ToString("0000");
				}
				IsPreAuthorizedFirst = ((entity.rs_FirstApproval != null) && (bool)entity.rs_FirstApproval);

				if (preLoadCharges)
				{
					Charges = new List<RecurringChargeVO>();

					foreach (tblRecurringCharge currentCharge in entity.tblRecurringCharges)
					{
						RecurringChargeVO currentChargeVO = new RecurringChargeVO(domainHost, currentCharge);
						Charges.Add(currentChargeVO);
					}
				}

				if (entity.rs_Approval)
				{
					SeriesTypeText = "PreAuthorized";
				}
				else
				{
					SeriesTypeText = "Debit";
				}

				ChargeIntervalUnitText = DomainsManager.GetDomain(domainHost).Cache.GetGlobalDataValue(GlobalDataGroup.IntervalUnit, Language.English, entity.rs_IntervalUnit);

				if (entity.rs_Deleted)
				{
					SeriesStatus = RecurringSeriesStatus.Deleted;
				}
				else if (entity.rs_Paid)
				{
					SeriesStatus = RecurringSeriesStatus.Paid;
				}
				else if (entity.rs_Blocked)
				{
					SeriesStatus = RecurringSeriesStatus.Blocked;
				}
				else if (entity.rs_Suspended)
				{
					SeriesStatus = RecurringSeriesStatus.Suspended;
				}
				else
				{
					SeriesStatus = RecurringSeriesStatus.Active;
				}

				SeriesStatusText = SeriesStatus.ToString();
			}
		}

		public int ID { get; set; }
		public DateTime StartDate { get; set; }
		public int PaymentMethodID { get; set; }
		public bool IsPreAuthorized { get; set; }
		public bool IsPreAuthorizedFirst { get; set; }
		public string BinCountryCode { get; set; }
		public decimal Amount { get; set; }
		public CurrencyVO Currency { get; set; }
		public bool IsFlexible { get; set; }
		public int ChargeCount { get; set; }
		public int ChargeIntervalUnit { get; set; }
		public int ChargeIntervalLength { get; set; }
		public bool IsSuspended { get; set; }
		public bool IsDeleted { get; set; }
		public bool IsPaid { get; set; }
		public bool IsBlocked { get; set; }	
		public List<RecurringChargeVO> Charges { get; set; }
		public string PaymentMethodDisplay { get; set; }
		public string SeriesTypeText { get; set; }
		public string CurrencyIsoCode { get; set; }
		public string ChargeIntervalUnitText { get; set; }
		public string SeriesStatusText { get; set; }
		public RecurringSeriesStatus SeriesStatus { get; set; }
		public string Comment { get; set; }
		public bool IsPrecreated { get; set; }
	}
}
