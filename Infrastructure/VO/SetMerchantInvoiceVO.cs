﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class SetMerchantInvoiceVO
    {
        public SetMerchantInvoiceVO(SetMerchantInvoice entity) 
        { 
            ID = entity.SetMerchantInvoice_id;
            ExternalProviderID = entity.ExternalProviderID;
            ExternalUseName = entity.ExternalUseName;
            ExternalUserID = entity.ExternalUserID;
            ExternalPassword = entity.ExternalPassword;
            IsEnable = entity.IsEnable;
            IsIncludeTax = entity.IsIncludeTax;
            IsCreateInvoice = entity.IsCreateInvoice;
            IsCreateReceipt = entity.IsCreateReceipt;

			ItemText = entity.ItemText;
			Merchant_id = entity.Merchant_id;
			IsAutoGenerateILS = entity.IsAutoGenerateILS;
			IsAutoGenerateOther = entity.IsAutoGenerateOther;
			IsAutoGenerateRefund = entity.IsAutoGenerateRefund;

        }
        public int ID { get; set; }
        public int Merchant_id { get; set; }
        public byte ExternalProviderID { get; set; }
        public string ExternalUseName { get; set; }
        public string ExternalUserID { get; set; }
        public string ExternalPassword { get; set; }
        public string ItemText { get; set; }
        public bool IsEnable { get; set; }
        public bool IsAutoGenerateILS { get; set; }
        public bool IsAutoGenerateOther { get; set; }
        public bool IsAutoGenerateRefund { get; set; }
        public bool IsIncludeTax { get; set; }
        public bool IsCreateInvoice { get; set; }
        public bool IsCreateReceipt { get; set; }
    
    }
}
