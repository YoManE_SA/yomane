﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class PaymentMethodGroupVO
	{
		public PaymentMethodGroupVO()
		{
		}

		public PaymentMethodGroupVO(Netpay.Dal.Netpay.PaymentMethodGroup entity)
		{
			ID = entity.PaymentMethodGroup_id;
			Name = entity.Name;
			isPopular = entity.IsPopular;
			shortCode = entity.ShortName;
			Type = ((PaymentMethodType?)entity.pmg_Type).GetValueOrDefault(PaymentMethodType.Unknown);
		}

		public int ID { get; set; }
		public string Name { get; set; }
		public string shortCode { get; set; }
		public bool isPopular { get; set; }
		public PaymentMethodType Type;
	}
}
