﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class PublicPayCustomerVO
	{
		public PublicPayCustomerVO()
		{
		}

		public PublicPayCustomerVO(tblPublicPayCostumerData entity)
		{
			if (entity == null) 
				return;

			ID = entity.id;
			TransactionID = entity.transID;
			switch (entity.transLocation)
			{
				case "pass": Status = TransactionStatus.Captured; break;
				case "fail": Status = TransactionStatus.Declined; break;
				case "pending": Status = TransactionStatus.Pending; break;
				case "approval": Status = TransactionStatus.Authorized; break;
				default: throw new ApplicationException("Unrecognized transLocation: \"" + entity.transLocation + "\"");
			}
			Address = entity.address;
			Cellular = entity.cellular;
			Mail = entity.email;
			FullName = entity.fullName;
			HowGetHere = entity.HowGetHere;
			Phone = entity.phone;
			CustomName1 = entity.Var1_name;
			CustomValue1 = entity.Var1_data;
			CustomName2 = entity.Var2_name;
			CustomValue2 = entity.Var2_data;
			CustomName3 = entity.Var3_name;
			CustomValue3 = entity.Var3_data;
			CustomName4 = entity.Var4_name;
			CustomValue4 = entity.Var4_data;
		}

		public int ID { get; set; }
		public int TransactionID { get; set; }
		public TransactionStatus Status { get; set; }
		public string Address { get; set; }
		public string Cellular { get; set; }
		public string Mail { get; set; }
		public string FullName { get; set; }
		public string HowGetHere { get; set; }
		public string Phone { get; set; }
		public string CustomName1 { get; set; }
		public string CustomValue1 { get; set; }
		public string CustomName2 { get; set; }
		public string CustomValue2 { get; set; }
		public string CustomName3 { get; set; }
		public string CustomValue3 { get; set; }
		public string CustomName4 { get; set; }
		public string CustomValue4 { get; set; }

		public bool IsEmpty
		{
			get
			{
				if (!string.IsNullOrEmpty(Address)) return false;
				if (!string.IsNullOrEmpty(Cellular)) return false;
				if (!string.IsNullOrEmpty(Mail)) return false;
				if (!string.IsNullOrEmpty(FullName)) return false;
				if (!string.IsNullOrEmpty(HowGetHere)) return false;
				if (!string.IsNullOrEmpty(Phone)) return false;
				if (!string.IsNullOrEmpty(CustomValue1)) return false;
				if (!string.IsNullOrEmpty(CustomValue2)) return false;
				if (!string.IsNullOrEmpty(CustomValue3)) return false;
				if (!string.IsNullOrEmpty(CustomValue4)) return false;
				
				return true;
			}
		}
	}
}