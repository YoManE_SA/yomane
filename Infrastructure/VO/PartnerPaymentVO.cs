﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class PartnerPaymentVO
	{
		public PartnerPaymentVO() { }

		public PartnerPaymentVO(tblAffiliatePayment entity, tblTransactionPay payment)
		{
			ID = entity.AFP_ID;
			InsertDate = entity.AFP_InsertDate;
			TransPaymentID = entity.AFP_TransPaymentID;
			FeeRatio = entity.AFP_FeeRatio;
			Amount = entity.AFP_PaymentAmount;
			PaymentNote = entity.AFP_PaymentNote;
			CompanyID = entity.AFP_CompanyID;
			CurrencyID = entity.AFP_PaymentCurrency;
			if(payment != null) {
				MerchantPaidAmount = (decimal) payment.transPayTotal;
				PaymentDate = payment.PayDate;
			}
		}

		public int ID { get; set; }
		public DateTime InsertDate { get; set; }
		public DateTime PaymentDate { get; set; }
		public decimal MerchantPaidAmount { get; set; }
		public int TransPaymentID { get; set; }
		public decimal FeeRatio { get; set; }
		public decimal Amount { get; set; }
		public string PaymentNote { get; set; }
		public int CompanyID { get; set; }
		public int CurrencyID { get; set; }
	}
}
