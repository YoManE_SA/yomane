﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Domains;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	[Serializable]
    public class TransactionVO
	{
		public TransactionVO() { }

		public TransactionVO(string domainHost, Netpay.Dal.Netpay.tblCompanyTransPass entity) : this(domainHost, entity, false, false, false) { }

		public TransactionVO(string domainHost, Netpay.Dal.Netpay.tblCompanyTransPass entity, bool loadPaymentMethod, bool loadMerchant, bool loadEpaImportLog)
		{
			if (entity == null)
				return;
			ID = entity.ID;
			Status = TransactionStatus.Captured;
			Amount = entity.Amount;
			CurrencyID = entity.Currency.Value;
			Payments = entity.Payments;
			CreditTypeID = entity.CreditType;
			PaymentMethodID = entity.PaymentMethod;
			PaymentMethodReferenceID = entity.PaymentMethodID;
			PaymentMethodDisplay = entity.PaymentMethodDisplay;
			IsTest = entity.isTestOnly;
			DeniedStatusID = entity.DeniedStatus;
			TransactionSourceID = entity.TransSource_id.GetValueOrDefault();
			PaymentsIDs = entity.PayID;
			PrimaryPaymentID = entity.PrimaryPayedID;
			InsertDate = entity.InsertDate;
			ChargebackDate = entity.DeniedDate;
			ApprovalNumber = entity.ApprovalNumber;
			CurrencyVO currency = DomainsManager.GetDomain(domainHost).Cache.GetCurrency(CurrencyID);
			CurrencyIsoCode = currency.IsoCode;
			CurrencySymbol = currency.Symbol;
			CreditTypeText = ((Netpay.Infrastructure.CreditType)CreditTypeID).ToString();
			DeniedStatusText = ((Netpay.Infrastructure.DeniedStatus)DeniedStatusID).ToString();
			TransactionSourceText = ((Netpay.Infrastructure.TransactionSource)TransactionSourceID).ToString();
			OrderNumber = entity.OrderNumber;
			DebitCompanyID = entity.DebitCompanyID;
			Comment = entity.Comment;
			RecurringSeriesID = entity.RecurringSeries;
			IP = entity.IPAddress;
			Installments = entity.Payments;
			IsChargeback = entity.IsChargeback;
			TerminalNumber = entity.TerminalNumber;
			DeniedAdminComment = entity.DeniedAdminComment;
			ReferringUrl = entity.referringUrl;
			PayerIDUsed = entity.payerIdUsed;
			DebitReferenceCode = entity.DebitReferenceCode;
			IPCountry = entity.IPCountry;
			OriginalTransactionID = entity.OriginalTransID;
            IsPendingChargeback = entity.IsPendingChargeback.GetValueOrDefault(false);
            PayForText = entity.PayforText;
			ProductId = entity.MerchantProduct_id;
			// fees
			TransactionFee = entity.netpayFee_transactionCharge;
			RatioFee = entity.netpayFee_ratioCharge;
			ClarificationFee = entity.netpayFee_ClrfCharge.GetValueOrDefault();
			ChargebackFee = entity.netpayFee_chbCharge.GetValueOrDefault();
			HandlingFee = entity.HandlingFee;

			// payment method
			if (loadPaymentMethod)
			{
				if (entity.list_PaymentMethod != null)
                {
                    // load payment method
                    PaymentMethod = new PaymentMethodVO(domainHost, entity.list_PaymentMethod);

                    // load transaction payment method
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.CreditCard)
						PaymentMethodData = new PaymentMethodDataVO(entity.tblCreditCard);
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.ECheck || entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.BankTransfer)
						PaymentMethodData = new PaymentMethodDataVO(domainHost, entity.tblCheckDetail);
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.PhoneDebit)
                        PaymentMethodData = new PaymentMethodDataVO(entity.trans_PhoneDetail);
				}
			}

			// merchnt
			MerchantID = entity.companyID;
			if (loadMerchant)
				Merchant = new MerchantVO(entity.tblCompany);

			// installments
			if (entity.Payments > 1)
				InstallmentList = entity.tblCompanyTransInstallments.Select(i => new InstallmentVO(i)).ToList();

			//EPA import log (is the transaction paid)
			if (loadEpaImportLog)
				EpaImportLogList = entity.tblLogImportEPAs.Select(i => new EpaImportLogVO(i)).OrderBy(l => l.Installment).ToList();
		}

		public TransactionVO(string domainHost, Netpay.Dal.Netpay.tblCompanyTransFail entity) : this(domainHost, entity, false, false) { }

		public TransactionVO(string domainHost, Netpay.Dal.Netpay.tblCompanyTransFail entity, bool loadPaymentMethod, bool loadMerchant)
		{
			if (entity == null)
				return;

			ID = entity.ID;
			Status = TransactionStatus.Declined;
			Amount = entity.Amount;
			CurrencyID = entity.Currency.Value;
			Payments = entity.Payments;
			CreditTypeID = entity.CreditType;
			PaymentMethodID = entity.PaymentMethod;
			PaymentMethodReferenceID = entity.PaymentMethodID;
			PaymentMethodDisplay = entity.PaymentMethodDisplay;
			IsTest = entity.isTestOnly;
			TransactionSourceID = entity.TransSource_id.GetValueOrDefault();
			InsertDate = entity.InsertDate;
			TransType = entity.TransType;
			TransactionTypeText = ((Netpay.Infrastructure.FailedTransactionType)TransType).ToString();
			CurrencyVO currency = DomainsManager.GetDomain(domainHost).Cache.GetCurrency(CurrencyID);
			if (currency != null)
			{
				CurrencyIsoCode = currency.IsoCode;
				CurrencySymbol = currency.Symbol;
			}
			CreditTypeText = ((Netpay.Infrastructure.CreditType)CreditTypeID).ToString();
			DeniedStatusText = ((Netpay.Infrastructure.DeniedStatus)DeniedStatusID).ToString();
			TransactionSourceText = ((Netpay.Infrastructure.TransactionSource)TransactionSourceID).ToString();
			OrderNumber = entity.OrderNumber;
			DebitCompanyID = entity.DebitCompanyID;
			Comment = entity.Comment;
			IP = entity.IPAddress;
			Installments = entity.Payments;
			TerminalNumber = entity.TerminalNumber;
			ReferringUrl = entity.referringUrl;
			PayerIDUsed = entity.payerIdUsed;
			DebitReferenceCode = entity.DebitReferenceCode;
			IPCountry = entity.IPCountry;
            PrimaryPaymentID = entity.PayID.GetValueOrDefault();
			ProductId = entity.MerchantProduct_id;
			// fees
			TransactionFee = entity.netpayFee_transactionCharge;

			ReplyDescriptionText = entity.DebitDeclineReason;
			ReplyCode = entity.replyCode;
			if (entity.DebitCompanyID != null && string.IsNullOrEmpty(ReplyDescriptionText))
				ReplyDescription = DomainsManager.GetDomain(domainHost).Cache.GetDebitCompanyCode(entity.DebitCompanyID.Value, entity.replyCode);

			if (loadPaymentMethod)
			{
				if (entity.list_PaymentMethod != null)
                {
                    // load payment method
                    PaymentMethod = new PaymentMethodVO(domainHost, entity.list_PaymentMethod);

                    // load transaction payment method
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.CreditCard)
						PaymentMethodData = new PaymentMethodDataVO(entity.tblCreditCard);
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.ECheck || entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.BankTransfer)
						PaymentMethodData = new PaymentMethodDataVO(domainHost, entity.tblCheckDetail);
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.PhoneDebit)
						PaymentMethodData = new PaymentMethodDataVO(entity.trans_PhoneDetail);
				}
			}

			MerchantID = entity.CompanyID;
			if (loadMerchant)
				Merchant = new MerchantVO(entity.tblCompany);
		}

		public TransactionVO(string domainHost, Netpay.Dal.Netpay.tblCompanyTransApproval entity) : this(domainHost, entity, false, false) { }

		public TransactionVO(string domainHost, Netpay.Dal.Netpay.tblCompanyTransApproval entity, bool loadPaymentMethod, bool loadMerchant)
		{
			if (entity == null)
				return;

			ID = entity.ID;
			Status = TransactionStatus.Authorized;
			Amount = entity.Amount;
			CurrencyID = entity.Currency.Value;
			Payments = entity.Payments;
			CreditTypeID = entity.CreditType;
			PaymentMethodID = entity.PaymentMethod;
			PaymentMethodReferenceID = entity.PaymentMethodID;
			PaymentMethodDisplay = entity.PaymentMethodDisplay;
			TransactionSourceID = entity.TransSource_id.GetValueOrDefault();
			InsertDate = entity.InsertDate;
			ApprovalNumber = entity.approvalNumber;
			PaymentsIDs = entity.PayID.ToString();
            PrimaryPaymentID = entity.PayID;
            PassedTransactionID = entity.TransAnswerID;
			CurrencyVO currency = DomainsManager.GetDomain(domainHost).Cache.GetCurrency(CurrencyID);
			CurrencyIsoCode = currency.IsoCode;
			CurrencySymbol = currency.Symbol;
			CreditTypeText = ((Netpay.Infrastructure.CreditType)CreditTypeID).ToString();
			TransactionSourceText = ((Netpay.Infrastructure.TransactionSource)TransactionSourceID).ToString();
			DebitCompanyID = entity.DebitCompanyID;
			Comment = entity.Comment;
			RecurringSeriesID = entity.RecurringSeries;
			IP = entity.IPAddress;
			Installments = entity.Payments;
			TerminalNumber = entity.TerminalNumber;
			ReferringUrl = entity.referringUrl;
			DebitReferenceCode = entity.DebitReferenceCode;
			ProductId = entity.MerchantProduct_id;

			// fees
			TransactionFee = entity.netpayFee_transactionCharge;

			if (loadPaymentMethod)
			{
				if (entity.list_PaymentMethod != null)
                {					
                    // load payment method
                    PaymentMethod = new PaymentMethodVO(domainHost, entity.list_PaymentMethod);

                    // load transaction payment method
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.CreditCard)
						PaymentMethodData = new PaymentMethodDataVO(entity.tblCreditCard);
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.ECheck || entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.BankTransfer)
						PaymentMethodData = new PaymentMethodDataVO(domainHost, entity.tblCheckDetail);
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.PhoneDebit)
                        PaymentMethodData = new PaymentMethodDataVO(entity.trans_PhoneDetail);
				}
			}

			MerchantID = entity.CompanyID;
			if (loadMerchant)
				Merchant = new MerchantVO(entity.tblCompany);
		}

		public TransactionVO(string domainHost, Netpay.Dal.Netpay.tblCompanyTransPending entity) : this(domainHost, entity, false, false) { }

		public TransactionVO(string domainHost, Netpay.Dal.Netpay.tblCompanyTransPending entity, bool loadPaymentMethod, bool loadMerchant)
		{
			if (entity == null)
				return;

			ID = entity.companyTransPending_id;
			Status = TransactionStatus.Pending;
			Amount = entity.trans_amount;
			if (entity.trans_currency != null)
				CurrencyID = entity.trans_currency.Value;
			Payments = entity.trans_payments;
			
			CreditTypeID = entity.trans_creditType;
			PaymentMethodID = entity.PaymentMethod;
			PaymentMethodReferenceID = entity.PaymentMethodID;
			PaymentMethodDisplay = entity.PaymentMethodDisplay;
			TransactionSourceID = entity.TransSource_id.GetValueOrDefault();
			TransType = entity.trans_type;
			InsertDate = entity.insertDate;
			CurrencyVO currency = DomainsManager.GetDomain(domainHost).Cache.GetCurrency(CurrencyID);
			CurrencyIsoCode = currency.IsoCode;
			CurrencySymbol = currency.Symbol;
			CreditTypeText = ((Netpay.Infrastructure.CreditType)CreditTypeID).ToString();
			TransactionSourceText = ((Netpay.Infrastructure.TransactionSource)TransactionSourceID).ToString();
			DebitCompanyID = entity.DebitCompanyID;
			Comment = entity.Comment;
			IP = entity.IPAddress;
			Installments = 0;
			TerminalNumber = entity.TerminalNumber;
			PayerIDUsed = entity.payerIdUsed;
			DebitReferenceCode = entity.DebitReferenceCode;
			ProductId = entity.MerchantProduct_id;

			if (loadPaymentMethod)
			{
				if (entity.list_PaymentMethod != null)
				{
					// load payment method
                    PaymentMethod = new PaymentMethodVO(domainHost, entity.list_PaymentMethod);
                    
                    // load transaction payment method
                    if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.CreditCard)
						PaymentMethodData = new PaymentMethodDataVO(entity.tblCreditCard);
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.ECheck || entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.BankTransfer)
						PaymentMethodData = new PaymentMethodDataVO(domainHost, entity.tblCheckDetail);
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.PhoneDebit)
                        PaymentMethodData = new PaymentMethodDataVO(entity.trans_PhoneDetail);
				}
			}

			MerchantID = entity.CompanyID;
			if (loadMerchant)
				Merchant = new MerchantVO(entity.tblCompany);
		}

		public string MoneyTransferText
		{
			get
			{
				const string NO_RECORD = "No record";
				if (EpaImportLogList == null) return NO_RECORD;
				if (EpaImportLogList.Count == 0) return NO_RECORD;
				if (Installments == 1)
				{
					if (EpaImportLogList[0].IsPaid && EpaImportLogList[0].IsRefunded)
						return "Paid and refunded (" + EpaImportLogList[0].PaidInsertDate.ToShortDateString() + ", " + EpaImportLogList[0].RefundedInsertDate.ToShortDateString() + ")";
					if (EpaImportLogList[0].IsPaid)
						return "Paid (" + EpaImportLogList[0].PaidInsertDate.ToShortDateString() + ")";
					if (EpaImportLogList[0].IsRefunded)
						return "Refunded (" + EpaImportLogList[0].RefundedInsertDate.ToShortDateString() + ")";
					return NO_RECORD;
				}
				string sTemp, sOut = "<table>";
				foreach (EpaImportLogVO record in EpaImportLogList)
				{
					if (EpaImportLogList[0].IsPaid && EpaImportLogList[0].IsRefunded)
						sTemp = "Paid and refunded (" + EpaImportLogList[0].PaidInsertDate.ToShortDateString() + ", " + EpaImportLogList[0].RefundedInsertDate.ToShortDateString() + ")";
					else if (EpaImportLogList[0].IsPaid)
						sTemp = "Paid (" + EpaImportLogList[0].PaidInsertDate.ToShortDateString() + ")";
					else if (EpaImportLogList[0].IsRefunded)
						sTemp = "Refunded (" + EpaImportLogList[0].RefundedInsertDate.ToShortDateString() + ")";
					else
						sTemp = string.Empty;
					sOut += "<tr><td>" + sTemp + "</td></tr>";
				}
				return (sOut == "<table>" ? NO_RECORD : sOut + "</table>");
			}
		}

		public string StatusText
		{
			get
			{
				return Status.ToString();
			}
			set { }
		}
		public decimal SignedAmount
		{
			get
			{
				if (TransactionSourceID == (int)TransactionSource.CcStorageFee || CreditTypeID == (byte)CreditType.Refund)
					return Amount *= -1;
				else
					return Amount;
			}
			set { }
		}
		public int ID { get; set; }
		public TransactionStatus Status { get; set; }
		public decimal Amount { get; set; }

		[EntityPropertyMapping("Currency")]
		public int CurrencyID { get; set; }
		public byte Payments { get; set; }
		public byte CreditTypeID { get; set; }
		public short? PaymentMethodID { get; set; }
		public int PaymentMethodReferenceID { get; set; }
		public string PaymentMethodDisplay { get; set; }
		public bool IsTest { get; set; }
		public byte DeniedStatusID { get; set; }
		public byte TransactionSourceID { get; set; }
		public int PrimaryPaymentID { get; set; }
		public string PaymentsIDs { get; set; }
		[EntityPropertyMapping("InsertDate")]
		public DateTime InsertDate { get; set; }
		public DateTime ChargebackDate { get; set; }
		public string ApprovalNumber { get; set; }
		public int? PassedTransactionID { get; set; }
		public int TransType { get; set; }
		public string CurrencyIsoCode { get; set; }
		public string CurrencySymbol { get; set; }
		public string CreditTypeText { get; set; }
		public string DeniedStatusText { get; set; }
		public string TransactionSourceText { get; set; }
		public string TransactionTypeText { get; set; }
		public string OrderNumber { get; set; }
		public string Comment { get; set; }
		public PaymentMethodDataVO PaymentMethodData { get; set; }
		public PaymentMethodVO PaymentMethod { get; set; }
		public int? DebitCompanyID { get; set; }
		public MerchantVO Merchant { get; set; }
		public int? MerchantID { get; set; }
		public int? RecurringSeriesID { get; set; }
		public string IP { get; set; }
		public byte Installments { get; set; }
		public bool? IsChargeback { get; set; }
		public string ReplyCode { get; set; }
		public string TerminalNumber { get; set; }
		public string ReplyDescriptionText { get; set; }
		public DebitCompanyCodeVO ReplyDescription { get; set; }
		public List<InstallmentVO> InstallmentList { get; set; }
		public List<EpaImportLogVO> EpaImportLogList { get; set; }
		public string DeniedAdminComment { get; set; }
		public string ReferringUrl { get; set; }
		public string PayerIDUsed { get; set; }
		public string DebitReferenceCode { get; set; }
		public string IPCountry { get; set; }
		public int OriginalTransactionID { get; set; }
        public bool IsPendingChargeback { get; set; }
        public string PayForText { get; set; }
		public int? ProductId { get; set; }

		// fees
		public decimal TransactionFee { get; set; } // used to be NetpayFee
		public decimal RatioFee { get; set; }
		public decimal ClarificationFee { get; set; }
		public decimal ChargebackFee { get; set; }
		public decimal HandlingFee { get; set; }

		public bool IsInstallments
		{
			get
			{
				return Payments > 1;
			}
			set { }
		}

        public decimal AnyOtherInstallment
        {
            get {
                if (!IsInstallments) return 0;
                return (int) (Amount / Payments);
            }
        }

        public decimal FirstInstallment { 
            get{
                if (!IsInstallments) return Amount;
                return AnyOtherInstallment + (Amount - (AnyOtherInstallment * Payments));
            }
        }

	}
}
