﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class SetMerchantSettlementsVO
    {
        public SetMerchantSettlementsVO(SetMerchantSettlement entity)
        {
            MerchantID = entity.Merchant_id;
            CurrencyID= entity.Currency_id;

            WireFee = entity.WireFee;
            WireFeePercent = entity.WireFeePercent;
            StorageFee = entity.StorageFee;
            HandlingFee = entity.HandlingFee;
            MinPayoutAmount = entity.MinPayoutAmount;
		    MinSettlementAmount = entity.MinSettlementAmount;
            IsShowToSettle = entity.IsShowToSettle;
            IsAutoInvoice = entity.IsAutoInvoice;
        }

        public SetMerchantSettlementsVO() {}

        public int MerchantID { get; set; }
        public int CurrencyID { get; set; }
        public bool IsShowToSettle { get; set; }
        public bool IsAutoInvoice { get; set; }
        public decimal WireFee { get; set; }
        public decimal WireFeePercent { get; set; }
        public decimal StorageFee { get; set; }
        public decimal HandlingFee { get; set; }
        public decimal MinPayoutAmount { get; set; }
        public decimal MinSettlementAmount { get; set;}
    }
}
