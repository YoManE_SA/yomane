﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.VO
{
	public class SettlementInfoVO
	{
		public int ID { get; set; }
		public DateTime? MinTrans { get; set; }
		public DateTime? MaxTrans { get; set; }
		public int? MinTransID { get; set; }
		public int? MaxTransID { get; set; }
	}
}
