﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.VO
{
    public class MobileDeviceVO
    {
        public MobileDeviceVO(Netpay.Dal.Netpay.MobileDevice entiry) 
        {
            ID = entiry.MobileDevice_id;
            Merchant_id = entiry.Merchant_id;
            InsertDate = entiry.InsertDate;
            DeviceIdentity = entiry.DeviceIdentity;
            DeviceUserAgent = entiry.DeviceUserAgent;
            DevicePhoneNumber = entiry.DevicePhoneNumber;
            PassCode = entiry.PassCode;
            LastLogin = entiry.LastLogin;
            IsActivated = entiry.IsActivated;
            IsActive = entiry.IsActive;
            AppVersion = entiry.AppVersion;
			SignatureFailCount = entiry.SignatureFailCount;
			FriendlyName = entiry.FriendlyName;
			SubUserID = entiry.MerchantSubUser_id;
			if (entiry.merchantSubUser != null)
				SubUserName = entiry.merchantSubUser.UserName;
			
        }

		public MobileDeviceVO() {}
        public int ID { get; set; }
        public int Merchant_id { get; set; }
        public System.DateTime InsertDate { get; set; }
        public string DeviceIdentity { get; set; }
        public string DeviceUserAgent { get; set; }
        public string DevicePhoneNumber { get; set; }
        public string PassCode { get; set; }
        public System.DateTime? LastLogin { get; set; }
        public bool IsActivated { get; set; }
        public bool IsActive { get; set; }
        public string AppVersion { get; set; }
		public int SignatureFailCount { get; set; }
		public string FriendlyName { get; set; }

		public int? SubUserID { get; set; }
		public string SubUserName { get; set; }

    }
}
