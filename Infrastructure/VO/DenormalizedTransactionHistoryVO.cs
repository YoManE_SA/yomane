﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Reports;

namespace Netpay.Infrastructure.VO
{
	public class DenormalizedTransactionHistoryVO
	{
		public DenormalizedTransactionHistoryVO(){}

		public DenormalizedTransactionHistoryVO(DenormalizedTransactionHistory entity)
		{
			ID = entity.ID;
			TransactionStatus = entity.TransactionStatus;
			TerminalID = entity.TerminalID;
			TerminalName = entity.TerminalName;
			TerminalNumber = entity.TerminalNumber;
			AcquiringBankID = entity.AcquiringBankID;
			AcquiringBankName = entity.AcquiringBankName;
			MerchantID = entity.MerchantID;
			MerchantNumber = entity.MerchantNumber;
			MerchantName = entity.MerchantName;
			TransactionID = entity.TransactionID;
			TransactionInsertDate = entity.TransactionInsertDate;
			TransactionCurrencyID = entity.TransactionCurrencyID;
			TransactionCurrencyIsoCode = entity.TransactionCurrencyIsoCode;
			TransactionAmount = entity.TransactionAmount;
			TransactionPaymentMethodID = entity.TransactionPaymentMethodID;
			TransactionPaymentMethodName = entity.TransactionPaymentMethodName;
			HistoryID = entity.HistoryID;
			HistoryTypeID = entity.HistoryTypeID;
			HistoryTypeName = entity.HistoryTypeName;
			HistoryInsertDate = entity.HistoryInsertDate;
			HistoryReferenceNumber = entity.HistoryReferenceNumber;
			HistoryDescription = entity.HistoryDescription;
			HistoryIsSucceeded = entity.HistoryIsSucceeded;
		}

		public int ID { get; set; }
		public int? HistoryID { get; set; }
		public int HistoryTypeID { get; set; }
		public string HistoryTypeName { get; set; }
		public DateTime HistoryInsertDate { get; set; }
		public bool? HistoryIsSucceeded { get; set; }
		public int? HistoryReferenceNumber { get; set; }
		public string HistoryDescription { get; set; }
		public int? MerchantID { get; set; }
		public int? MerchantNumber { get; set; }
		public string MerchantName { get; set; }
		public int? TransactionID { get; set; }
		public string TransactionStatus { get; set; }
		public DateTime TransactionInsertDate { get; set; }
		public int? TransactionCurrencyID { get; set; }
		public string TransactionCurrencyIsoCode { get; set; }
		public decimal? TransactionAmount { get; set; }
		public byte? TransactionPaymentMethodID { get; set; }
		public string TransactionPaymentMethodName { get; set; }
		public int? TerminalID { get; set; }
		public string TerminalName { get; set; }
		public string TerminalNumber { get; set; }
		public int? AcquiringBankID { get; set; }
		public string AcquiringBankName { get; set; }
	}
}
