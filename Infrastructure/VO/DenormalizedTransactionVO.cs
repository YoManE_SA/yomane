﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Reports;

namespace Netpay.Infrastructure.VO
{
	public class DenormalizedTransactionVO
	{
		public DenormalizedTransactionVO(){}
		
		public DenormalizedTransactionVO(DenormalizedTransaction entity)
		{
			TransactionStatus = entity.TransactionStatus;
			TerminalID = entity.TerminalID;
			TerminalName = entity.TerminalName;
			TerminalNumber = entity.TerminalNumber;
			TerminalContractNumber = entity.TerminalContractNumber;
			AcquiringBankID = entity.AcquiringBankID;
			AcquiringBankName = entity.AcquiringBankName;
			MerchantID = entity.CompanyID;
			MerchantNumber = entity.CompanyNumber;
			MerchantName = entity.CompanyName;
			TransactionID = entity.TransactionID;
			TransactionDate = entity.TransactionDate;
			TransactionAmount = entity.TransactionAmount;
			TransactionAmountUSD = entity.TransactionAmountUSD;
			TransactionCurrencyID = entity.TransactionCurrencyID;
			TransactionCurrencyIsoCode = entity.TransactionCurrencyIsoCode;
			PaymentMethodID = entity.PaymentMethodID;
			TransTypeID = entity.TransTypeID;
			RejectingSourceID = entity.RejectingSourceID;
			RejectionCode = entity.RejectionCode;
			RejectionText = entity.RejectionText;
			IsChargeback = entity.IsChargeback;
			IsCaptured = entity.IsCaptured;
			IsRefund = entity.IsRefund;
			IsAuthorization = entity.IsAuthorization;
			MerchantStatusID = entity.MerchantStatusID;
			MerchantStatusText = entity.MerchantStatusText;
			TerminalIsActive = entity.TerminalIsActive;
			PspID = entity.PspID;
			PspName = entity.PspName;
			ManagingCompanyID = entity.ManagingCompanyID;
			ManagingCompanyName = entity.ManagingCompanyName;
			MerchantGroupID = entity.MerchantGroupID;
			MerchantGroupName = entity.MerchantGroupName;
			TransactionFee = entity.TransactionFee;
			RatioFee = entity.RatioFee;
			ChargebackFee = entity.ChargebackFee;
			HandlingFee = entity.HandlingFee;
			BankTransferID = entity.BankTransferID;
			BankTransferDate = entity.BankTransferDate;		
		}
		
		public string TransactionStatus{ get; set; }
		public int? TerminalID{ get; set; }
		public string TerminalName{ get; set; }
		public string TerminalNumber{ get; set; }
		public string TerminalContractNumber{ get; set; }
		public int? AcquiringBankID{ get; set; }
		public string AcquiringBankName{ get; set; }
		public int? MerchantID{ get; set; }
		public int? MerchantNumber { get; set; }
		public string MerchantName { get; set; }
		public int? TransactionID{ get; set; }
		public DateTime? TransactionDate{ get; set; }
		public decimal? TransactionAmount{ get; set; }
		public decimal? TransactionAmountUSD{ get; set; }
		public int? TransactionCurrencyID{ get; set; }
		public string TransactionCurrencyIsoCode{ get; set; }
		public byte? PaymentMethodID{ get; set; }
		public int? TransTypeID{ get; set; }
		public byte? RejectingSourceID{ get; set; }
		public string RejectionCode{ get; set; }
		public string RejectionText{ get; set; }
		public bool? IsChargeback{ get; set; }
		public bool? IsCaptured{ get; set; }
		public bool? IsRefund{ get; set; }
		public bool? IsAuthorization{ get; set; }
		public int? MerchantStatusID{ get; set; }
		public string MerchantStatusText{ get; set; }
		public bool? TerminalIsActive{ get; set; }
		public int? PspID{ get; set; }
		public string PspName{ get; set; }
		public int? ManagingCompanyID{ get; set; }
		public string ManagingCompanyName{ get; set; }
		public int? MerchantGroupID{ get; set; }
		public string MerchantGroupName{ get; set; }
		public decimal? TransactionFee{ get; set; }
		public decimal? RatioFee{ get; set; }
		public decimal? ChargebackFee{ get; set; }
		public decimal? HandlingFee{ get; set; }
		public string BankTransferID{ get; set; }
		public DateTime? BankTransferDate{ get; set; }
	}
}
