﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class PartnerVO
    {
        public PartnerVO() { }

        public PartnerVO(tblAffiliate entity) 
        {
            ID = entity.affiliates_id;
            Name = entity.name;
			OutboundUsername = entity.OutboundUsername;
			OutboundPassword = entity.OutboundPassword;
        }

        public int ID { get; set; }
        public string Name { get; set; }
		public string OutboundUsername { get; set; }
		public string OutboundPassword { get; set; }

    }
}
