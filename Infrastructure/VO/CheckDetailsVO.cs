﻿using System;
using System.Collections.Generic;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class CheckDetailsVO
	{
		public CheckDetailsVO() { }

		public CheckDetailsVO(tblCheckDetail entity) 
		{
			accountName = entity.AccountName;
			//accountNumber = entity.accountNumber;
			//routingNumber = entity.routingNumber;
			personalNumber = entity.PersonalNumber;
			phoneNumber = entity.PhoneNumber;
			email = entity.Email;
			comment = entity.Comment;
			birthDate = entity.BirthDate;
			bankAccountTypeId = entity.BankAccountTypeId;
			bankName = entity.BankName;
			bankCity = entity.BankCity;
			bankPhone = entity.BankPhone;
			bankState = entity.BankState; 
		}

		public string accountName { get; set; }
		public string accountNumber { get; set; }
		public string routingNumber { get; set; }
		public string personalNumber { get; set; }
		public string phoneNumber { get; set; }
		public string email { get; set; }
		public string comment { get; set; }
		public string birthDate { get; set; }
		public byte bankAccountTypeId { get; set; }
		public string bankName { get; set; }
		public string bankCity { get; set; }
		public string bankPhone { get; set; }
		public string bankState { get; set; }
		public int bankID { get; set; }
		public PaymentMethodEnum PaymentMethod { get; set; }
	}
}
