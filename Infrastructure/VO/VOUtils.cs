﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Netpay.Infrastructure.Security;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public abstract class VOUtils
	{	
		/// <summary>
		/// Gets the database entity property that is mapped to the VO property.
		/// The mapping is defined by the EntityPropertyMapping attribute.
		/// </summary>
		/// <param name="voType"></param>
		/// <param name="voPropertyName"></param>
		/// <returns></returns>
		public static string GetMappedProperty(Type voType, string voPropertyName)
		{
			PropertyInfo property = voType.GetProperty(voPropertyName);
			if (property == null)
				throw new ApplicationException(string.Format("Property '{0}' was not found in type '{1}'.", voPropertyName, voType.Name));
			object[] customAttributes = property.GetCustomAttributes(typeof(EntityPropertyMapping), false);
			if (customAttributes.Length == 0)
				throw new ApplicationException(string.Format("Could not find a mapping attribute for '{0}.{1}'", voType.Name, voPropertyName));
			EntityPropertyMapping mapAttribute = (EntityPropertyMapping)customAttributes[0];

			return mapAttribute.EntityPropertyName;
		}
	}
}
