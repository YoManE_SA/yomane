﻿using System;
using System.Collections.Generic;
using Netpay.CommonTypes;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class Settlement2VO
	{
		public int payId;
		public int merchantId;
		public System.DateTime payDate;
		public decimal vatRate;
		public double exchangeRate;
		public string comment;
		public Currency currencyID;
		public decimal payPercent;
		public int invoiceNumber;
		public bool isGradedFees;
		public decimal securityDeposit;
		public decimal Total;
		public decimal TotalWithTax;
		public decimal NotPayedAmount;
		public Dictionary<TransactionAmountType, CountAmount> Totals;
		public List<SettlementAmountVO> SettlmentAmounts;

		public Settlement2VO(tblTransactionPay entity)
		{
			payId = entity.id;
			merchantId = entity.CompanyID;
			payDate = entity.PayDate;
			vatRate = entity.isChargeVAT ? entity.VatAmount : 0;
			exchangeRate = entity.ExchangeRate;
			comment = entity.Comment;
			currencyID = (Currency)entity.Currency.GetValueOrDefault((int)Currency.Unknown);
			payPercent = entity.PayPercent;
			invoiceNumber = entity.InvoiceNumber;
			isGradedFees = entity.IsGradedFees;
			securityDeposit = entity.SecurityDeposit;
			Totals = new Dictionary<TransactionAmountType, CountAmount>();
			SettlmentAmounts = new List<SettlementAmountVO>();

		}
	}
}
