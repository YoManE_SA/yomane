﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    public class MerchantRecurringSettingVO
    {
        public MerchantRecurringSettingVO(tblMerchantRecurringSetting entity)
        {
            MerchantID = entity.MerchantID;
            IsEnabled = entity.IsEnabled;
            IsEnabledFromTransPass = entity.IsEnabledFromTransPass;
            IsEnabledModify = entity.IsEnabledModify;
            ForceMD5OnModify = entity.ForceMD5OnModify;
            MaxYears = entity.MaxYears;
            MaxCharges = entity.MaxCharges;
            MaxStages = entity.MaxStages;
        }

        public int MerchantID;
        public bool IsEnabled;
        public bool IsEnabledFromTransPass;
        public bool IsEnabledModify;
        public bool ForceMD5OnModify;
        public int MaxYears;
        public int MaxCharges;
        public int MaxStages;
    
    }
}
