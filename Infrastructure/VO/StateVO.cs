﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class StateVO
	{
		public StateVO() { }

		public int ID { get; set; }
		public string Name { get; set; }
		public string IsoCode { get; set; }
		public int CountryID { get; set; }
		//public Language Language { get; set; }
	}
}
