﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Infrastructure.Domains;

namespace Netpay.Infrastructure.VO
{
    [Serializable]
    public class PaymentMethodDataVO
	{
		public PaymentMethodDataVO() { }

		public PaymentMethodDataVO(Netpay.Dal.Netpay.tblCreditCard entity)
		{
			if (entity == null)
				return;

            PaymentMethodRowID = entity.ID;
			PaymentMethodType = PaymentMethodType.CreditCard;
			First6Digits = entity.CCard_First6;
			Last4Digits = entity.CCard_Last4;
			BinCountryCode = entity.BINCountry;
			Email = entity.email;
			Name = entity.Member;
			CardTypeID = entity.ccTypeID;
			ExpirationMonth = entity.ExpMM;
			ExpirationYear = entity.ExpYY;
			Phone = entity.phoneNumber;
			PersonalNumber = entity.PersonalNumber;

			if (entity.tblBillingAddress != null)
				Address = new AddressVO(entity.tblBillingAddress);
		}

		public PaymentMethodDataVO(Netpay.Dal.Netpay.tblCheckDetail entity)
		{
			if (entity == null)
				return;

            PaymentMethodRowID = entity.id;
			PaymentMethodType = PaymentMethodType.ECheck;
			Email = entity.Email;
			Name = entity.AccountName;
			Phone = entity.PhoneNumber;
			BankName = entity.BankName;
			BankCountry = entity.BankCountry;
			BankCity = entity.BankCity;
			BankState = entity.BankState;
			BankPhone = entity.BankPhone;

			if (entity.tblBillingAddress != null)
				Address = new AddressVO(entity.tblBillingAddress);
		}

		public PaymentMethodDataVO(string domainHost, Netpay.Dal.Netpay.tblCheckDetail entity) : this(entity) 
		{
			if (entity == null)
				return;

			try
			{
                PaymentMethodRowID = entity.id;
                AccountNumber = Netpay.Infrastructure.Security.Encryption.Decrypt(domainHost, entity.AccountNumber256.ToArray()).ToSafeCCString();
				RoutingNumber = Netpay.Infrastructure.Security.Encryption.Decrypt(domainHost, entity.RoutingNumber256.ToArray()).ToSafeCCString();
			}
			catch { }
		}

        public PaymentMethodDataVO(Netpay.Dal.Netpay.PhoneDetail entity)
		{
			if (entity == null)
				return;

            PaymentMethodRowID = entity.PhoneDetail_id;
			PaymentMethodType = PaymentMethodType.PhoneDebit;
			Name = entity.FullName;
			Phone = entity.PhoneNumber;

			if (entity.tblBillingAddress != null)
				Address = new AddressVO(entity.tblBillingAddress);
		}

        public int PaymentMethodRowID { set; get; }
		public PaymentMethodType PaymentMethodType { set; get; }
		public PaymentMethodEnum PaymentMethod { set; get; }
		public AddressVO Address { set; get; }
		public int First6Digits { set; get; }
		public short Last4Digits { set; get; }
		public string BinCountryCode { set; get; }
		public string Name { set; get; }
		public string Email { set; get; }
		public string Phone { set; get; }
		public int Count { set; get; }
		public short CardTypeID { set; get; }
		public string CardTypeText
		{
			get
			{
				return ((CreditCardType)CardTypeID).ToString();
			}
		}
		public string ExpirationMonth { set; get; }
		public string ExpirationYear { set; get; }
		public string PersonalNumber { set; get; }
        public DateTime Expiration {
            get {
                int month, year;
                if (!(int.TryParse(ExpirationMonth.Trim(), out month) && int.TryParse(ExpirationYear.Trim(), out year))) return DateTime.MinValue;
                if (year < 100) year += 2000; 
                if ((month < 1) || (month > 12) || (year < 1800) || (year > DateTime.Now.Year + 500)) return DateTime.MinValue;
                return new DateTime(year, month, 1).AddMonths(1).AddDays(-1); 
            }
        }
		public int PaymentMethodID
		{
			get
			{
				return CardTypeID + 20;
			}
		}
		public string AccountNumber { set; get; }
		public string RoutingNumber { set; get; }
		public string BankName { set; get; }
		public string BankCountry { set; get; }
		public string BankState { set; get; }
		public string BankCity { set; get; }
		public string BankPhone { set; get; }
	}
}
