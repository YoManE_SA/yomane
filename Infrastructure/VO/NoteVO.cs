﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.VO
{
    public class NoteVO
    {
		public NoteVO(Netpay.Dal.Netpay.tblNote entity)
		{
			ID = entity.Note_id;
			Merchant_id = entity.Merchant_id;
			Customer_id = entity.Customer_id;
			InsertDate = entity.InsertDate.GetValueOrDefault();
			InsertUser = entity.InsertUser;
			NoteText = entity.NoteText;
		}
		public int ID { get; set; }
		public int? Merchant_id { get; set; }
		public int? Customer_id { get; set; }
		public System.DateTime InsertDate { get; set; }
		public string InsertUser { get; set; }
		public string NoteText { get; set; }
	}
}
