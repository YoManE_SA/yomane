﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using System.Xml.Linq;

namespace Netpay.Infrastructure.VO
{
	public class ExternalCardCustomerVO
	{
		public ExternalCardCustomerVO() {}
	
		public ExternalCardCustomerVO(tblExternalCardCustomer entity) 
		{
			ID = entity.ExternalCardCustomer_id;
			UniqueID = entity.UniqueID;
			Status = (ExternalCardCustomerStatus)entity.Status;
			Email = entity.Email;
			Name = entity.Name;
			Comment = entity.Comment;
            Provider = (ExternalCardProviderEnum)entity.tblExternalCardTerminal.ExternalCardProvider_id;
            ExtraData = entity.ExtraData;
		}

		public int ID { get; set; }
		public Guid UniqueID { get; set; }
		public ExternalCardCustomerStatus Status { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public string Comment { get; set; }
        public ExternalCardProviderEnum Provider { get; set; }
        public XElement ExtraData { get; set; }
		public bool IsPayable 
		{ 
			get 
			{
				return Status == ExternalCardCustomerStatus.Approved || Status == ExternalCardCustomerStatus.Activated;
			} 
		}
	}
}
