﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;

namespace Netpay.Infrastructure.VO
{
	public class TransactionAmountsReportVO
	{
		public TransactionAmountsReportVO() { }
		
		[EntityPropertyMapping("TerminalID")]
		public int? TerminalID { get; set; }
		[EntityPropertyMapping("TerminalName")]
		public string TerminalName { get; set; }
		[EntityPropertyMapping("TerminalNumber")]
		public string TerminalNumber { get; set; }
		[EntityPropertyMapping("AcquiringBankID")]
		public int? AcquiringBankID { get; set; }
		[EntityPropertyMapping("AcquiringBankName")]
		public string AcquiringBankName { get; set; }
		[EntityPropertyMapping("MerchantID")]
		public int? MerchantID { get; set; }
		[EntityPropertyMapping("MerchantNumber")]
		public int? MerchantNumber { get; set; }
		[EntityPropertyMapping("MerchantName")]
		public string MerchantName { get; set; }
		public int TransactionID { get; set; }
		[EntityPropertyMapping("TransactionDate")]
		public DateTime TransactionDate { get; set; }
		[EntityPropertyMapping("TotalCount")]
		public int TotalCount { get; set; }
		[EntityPropertyMapping("TotalAmount")]
		public decimal TotalAmount { get; set; }
		[EntityPropertyMapping("AmountTypeID")]
		public int AmountTypeID { get; set; }
		[EntityPropertyMapping("AmountTypeName")]
		public string AmountTypeName { get; set; }
		[EntityPropertyMapping("TransactionCurrencyID")]
		public int? TransactionCurrencyID { get; set; }
		[EntityPropertyMapping("TransactionCurrencyIsoCode")]
		public string TransactionCurrencyIsoCode { get; set; }

		public static TransactionAmountsReportVO operator +(TransactionAmountsReportVO a, TransactionAmountsReportVO b)
		{
			TransactionAmountsReportVO result = new TransactionAmountsReportVO();
			result.TotalAmount = a.TotalAmount + b.TotalAmount;
			result.TotalCount = a.TotalCount + b.TotalCount;

			return result;
		}
	}
}
