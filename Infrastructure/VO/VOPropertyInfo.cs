﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.VO;

namespace Netpay.Infrastructure.VO
{
	public class VOPropertyInfo
	{
		private Type _voType = null;
		private string _property = null;
		private string _parameter = null;

		public Type VOType
		{
			get { return _voType; }
			set { _voType = value; }
		}

		public string Property
		{
			get { return _property; }
			set { _property = value; }
		}

		public string Parameter
		{
			get { return _parameter; }
			set { _parameter = value; }
		}

		public string MappedEntityProperty
		{
			get
			{
				return VOUtils.GetMappedProperty(VOType, _property);
			}
		}
	}
}
