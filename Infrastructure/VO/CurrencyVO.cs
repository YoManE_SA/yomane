﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.VO
{
	public class CurrencyVO
	{
		public int ID { get; set; }
		public CommonTypes.Currency EnumValue { get { return (CommonTypes.Currency)ID; } set { ID = (int)value; } }
		public string Name { get; set; }
		public string IsoCode { get; set; }
		public string Symbol { get; set; }
		public decimal BaseRate { get; set; }
		public int IsoNumber { get; set; }
		public decimal ConversionFee { get; set; }
		public bool IsSymbolBeforeAccount { get; set; }
	}
}