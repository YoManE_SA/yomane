﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
	public class TransactionAmountVO
	{
		public TransactionAmountVO() { }

		public TransactionAmountVO(tblTransactionAmount entity) 
		{
			ID = entity.ID;
			CurrencyID = entity.Currency;
			MarchantID = entity.MerchantID;
			Amount = entity.Amount;
			Date = entity.InsertDate;
			SettledAmount = entity.SettledAmount;
			SettledCurrencyID = entity.SettledCurrency;
			SettlementDate = entity.SettlementDate;
			Type = (TransactionAmountType)entity.TypeID;

			if (entity.TransApprovalID != null) 
			{
				TransStatus = TransactionStatus.Authorized;
				TransactionID = entity.TransApprovalID.GetValueOrDefault();
			}
			else if (entity.TransFailID != null)
			{
				TransStatus = TransactionStatus.Declined;
				TransactionID = entity.TransFailID.GetValueOrDefault();
			}
			else if (entity.TransPassID != null)
			{
				TransStatus = TransactionStatus.Captured;
				TransactionID = entity.TransPassID.GetValueOrDefault();
			}
			else if (entity.TransPendingID != null)
			{
				TransStatus = TransactionStatus.Pending;
				TransactionID = entity.TransPendingID.GetValueOrDefault();
			}
		}

		public int ID { get; set; }
		public int? CurrencyID { get; set; }
		public int? MarchantID { get; set; }
		public decimal? Amount { get; set; }
		public DateTime Date { get; set; }
		public int? SettledCurrencyID { get; set; }
		public decimal? SettledAmount { get; set; }
		public DateTime? SettlementDate { get; set; }
		public TransactionAmountType Type { get; set; }
		public TransactionStatus TransStatus { get; set; }
		public int TransactionID { get; set; }
	}
}
