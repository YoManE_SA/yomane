﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;
using Netpay.Infrastructure.Domains;

namespace Netpay.Infrastructure.VO
{
	public class EcheckStorageVO
	{
		public EcheckStorageVO() { }

		public EcheckStorageVO(tblCustomerEcheckAccount entity, string domainHost)
		{
			Id = entity.id;
			PaymentMethod = PaymentMethodEnum.ECCheck;
			PaymentMethodName = PaymentMethod.ToString();
            if (PaymentMethodName.StartsWith("EC")) PaymentMethodName = PaymentMethodName.Substring(2);

			CustomerId = entity.customerId;
			InsertDate = entity.insertDate;
			DateOfBirth = entity.DateOfBirth;
			AccountName = entity.accountName;
			AccountType = entity.accountType;
			PhoneNumber = entity.phoneNumber;
			EmailAddress = entity.emailAddress;
			AccountNumberEncrypted = entity.accountNumber256.ToArray();
			RoutingNumberEncrypted = entity.routingNumber256.ToArray();
			if (AccountNumberEncrypted != null && AccountNumberEncrypted.Length > 0) {
				string value = Netpay.Infrastructure.Security.Encryption.Decrypt(domainHost, AccountNumberEncrypted);
				if (value.Length > 4) Last4 = value.Substring(value.Length - 4);
			}
		}

		public int Id { get; set; }
        public PaymentMethodEnum PaymentMethod { get; set; }
        public string PaymentMethodName { get; set; }
		public int CustomerId { get; set; }
		public DateTime InsertDate { get; set; }
		public DateTime? DateOfBirth { get; set; }
		public string AccountName { get; set; }
		public byte AccountType { get; set; }
		public string PhoneNumber { get; set; }
		public string EmailAddress { get; set; }
		public byte[] AccountNumberEncrypted { get; set; }
		public string AccountNumber { get; set; }
		public byte[] RoutingNumberEncrypted { get; set; }
		public string RoutingNumber { get; set; }
		public string Last4{ get; set; }
	}
}
