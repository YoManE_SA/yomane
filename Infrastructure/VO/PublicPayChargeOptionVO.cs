﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using System.Collections;

namespace Netpay.Infrastructure.VO
{
	public class PublicPayChargeOptionVO
	{
		public PublicPayChargeOptionVO(){}

		public PublicPayChargeOptionVO(string domainHost, tblPublicPayChargeOption entity)
		{
			if (entity == null)
				return;
			
			ID = entity.id;
			MerchantID = entity.companyID;
			SKU = entity.SKU;
			Text = entity.Text;
            Tags = entity.tblPublicPayChargeOptionTags.Select(t => t.Tag).ToList();
			Description = entity.Description;
            ReceiptText = entity.ReceiptText;
			ReceiptLink = entity.ReceiptLink;
			Amount = entity.Amount;
			CurrencyID = entity.Currency.Value;
			CreditType = entity.CreditType;
			PaymentsMax = entity.Payments;
			IsDefault = entity.isDefaultCheck;
			QuantityMin = entity.QtyStart;
			QuantityMax = entity.QtyEnd;
			QuantityInterval = entity.QtyStep;
			ImageFileName = entity.ImageFileName;
			QuantityAvailable = entity.QtyAvailable;
			IsDynamicAmount = entity.isDynamicProduct;
			IsActive = entity.isActive;
			Priority = entity.Priority;
			Language = (CommonTypes.Language?)entity.Lang;

			CurrencyVO currency = Domains.DomainsManager.GetDomain(domainHost).Cache.GetCurrency(CurrencyID);
			AmountFormatted = entity.Amount.ToAmountFormat(currency);
			CurrencySymbol = currency.Symbol;
            TransType = entity.TransType;
		}

		public int ID { get; set; }
		public int MerchantID { get; set; }
		public string Text { get; set; }
        public List<string> Tags { get; set; }
		public string SKU { get; set; }
		public string Description { get; set; }
		public string ImageFileName { get; set; }
        public string ReceiptText { get; set; }
		public string ReceiptLink { get; set; }
		public decimal Amount { get; set; }
		public string AmountFormatted { get; set; }
		public string CurrencySymbol { get; set; }
		public int CurrencyID { get; set; }
		public int CreditType { get; set; }
		public int PaymentsMax { get; set; }
		public bool IsDefault { get; set; }
		public int QuantityMin { get; set; }
		public int QuantityMax { get; set; }
		public int QuantityInterval { get; set; }
		public bool IsActive { get; set; }
		public int Priority { get; set; }
		public CommonTypes.Language? Language { get; set; }
		public int? QuantityAvailable { get; set; }
		public bool IsDynamicAmount { get; set; }
        public byte TransType { get; set; }

		public bool IsDynamicQuantiy 
		{ 
			get
			{
				return QuantityMax > QuantityMin;
			} 
		}
		public bool IsDynamicPayments
		{
			get
			{
				return PaymentsMax > 1;
			}
		}

	}
}
