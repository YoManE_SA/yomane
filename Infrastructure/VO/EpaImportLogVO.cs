﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    [Serializable]
    public class EpaImportLogVO
	{
		public EpaImportLogVO(){}

		public EpaImportLogVO(tblLogImportEPA entity)
		{
			ID = entity.ID;
			TransactionID = entity.TransID;
			Installment = entity.Installment;
			IsPaid = entity.IsPaid;
			IsRefunded = entity.IsRefunded;
			PaidInsertDate = entity.PaidInsertDate;
			RefundedInsertDate = entity.RefundedInsertDate;
		}

		public readonly int ID = 0;
		public readonly int TransactionID = 0;
		public readonly byte Installment = 0;
		public readonly bool IsPaid = false;
		public readonly bool IsRefunded = false;
		public readonly DateTime PaidInsertDate;
		public readonly DateTime RefundedInsertDate;
	}
}
