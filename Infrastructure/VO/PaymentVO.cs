﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class PaymentVO
	{
		public PaymentVO() { }

		public PaymentVO(viewPaymentsInfo entity)
		{
			CompanyMakePaymentsRequestsID = entity.CompanyMakePaymentsRequests_id;
			CompanyMakePaymentsProfilesID = entity.CompanyMakePaymentsProfiles_id;
			CompanyID = entity.Company_id;
			PaymentType = entity.paymentType;
			PaymentDate = entity.paymentDate;
			PaymentAmount = entity.paymentAmount;
			PaymentCurrency = entity.paymentCurrency;
			PaymentExchangeRate = entity.paymentExchangeRate;
			PaymentMerchantComment = entity.paymentMerchantComment;
			BankIsraelInfo_PayeeName = entity.bankIsraelInfo_PayeeName;
			BankIsraelInfo_CompanyLegalNumber = entity.bankIsraelInfo_CompanyLegalNumber;
			BankIsraelInfo_personalIdNumber = entity.bankIsraelInfo_personalIdNumber;
			BankIsraelInfo_bankBranch = entity.bankIsraelInfo_bankBranch;
			BankIsraelInfo_AccountNumber = entity.bankIsraelInfo_AccountNumber;
			BankIsraelInfo_PaymentMethod = entity.bankIsraelInfo_PaymentMethod;
			BankIsraelInfo_BankCode = entity.bankIsraelInfo_BankCode;
						
			BasicInfo_costumerName = entity.basicInfo_costumerName;
			BasicInfo_costumerNumber = entity.basicInfo_costumerNumber;
			BankAbroadAccountName = entity.bankAbroadAccountName;
			BankAbroadAccountName2 = entity.bankAbroadAccountName2;

			WireStatus = entity.WireStatus;
			WireStatusDate = entity.WireStatusDate;
			WireComment = entity.WireComment;
			WireMoneyID = entity.WireMoney_id;
			WireFileName = entity.WireFileName;
		}
		

		public int CompanyMakePaymentsRequestsID { get; set; }
		public int? CompanyMakePaymentsProfilesID { get; set; }
		public int? CompanyID { get; set; }
		public byte PaymentType { get; set; }
		public DateTime PaymentDate { get; set; }
		public double PaymentAmount { get; set; }
		public int? PaymentCurrency { get; set; }
		public double PaymentExchangeRate { get; set; }
		public string PaymentMerchantComment { get; set; }
		public string BankIsraelInfo_PayeeName { get; set; }
		public string BankIsraelInfo_CompanyLegalNumber { get; set; }
		public string BankIsraelInfo_personalIdNumber { get; set; }
		public string BankIsraelInfo_bankBranch { get; set; }
		public string BankIsraelInfo_AccountNumber { get; set; }
		public string BankIsraelInfo_PaymentMethod { get; set; }
		public int BankIsraelInfo_BankCode { get; set; }
		public string BasicInfo_costumerName { get; set; }
		public string BasicInfo_costumerNumber { get; set; }
		public string BankAbroadAccountName { get; set; }
		public string BankAbroadAccountName2 { get; set; }
		public byte? WireStatus { get; set; }
		public DateTime? WireStatusDate { get; set; }
		public string WireComment { get; set; }
		public int? WireMoneyID { get; set; }
		public string WireFileName { get; set; }
	}
}
