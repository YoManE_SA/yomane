﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Domains;

namespace Netpay.Infrastructure.VO
{
	public class RefundRequestVO
	{
		public RefundRequestVO()
		{
			
		}

		public RefundRequestVO(string domainHost, tblRefundAsk entity)
		{
			ID = entity.id;
			MerchantID = entity.companyID;
			TransactionID = entity.transID;
			Amount = entity.RefundAskAmount;
			CurrencyIso = DomainsManager.GetDomain(domainHost).Cache.GetCurrency(entity.RefundAskCurrency).IsoCode;
			Comment = entity.RefundAskComment;
			Status = (RefundRequestStatus)entity.RefundAskStatus;
			Date = entity.RefundAskDate;
		}

		public RefundRequestVO(string domainHost, tblRefundAsk entity, tblCompanyTransPass transactionEntity)
		{
			ID = entity.id;
			MerchantID = entity.companyID;
			TransactionID = entity.transID;
			Amount = entity.RefundAskAmount;
			CurrencyIso = DomainsManager.GetDomain(domainHost).Cache.GetCurrency(entity.RefundAskCurrency).IsoCode;
			Comment = entity.RefundAskComment;
			Status = (RefundRequestStatus)entity.RefundAskStatus;
			Date = entity.RefundAskDate;
			//if (transactionEntity != null) OriginalTransaction = new TransactionVO(domainHost, transactionEntity);
		}

		public int ID { get; set; }
		public int? MerchantID { get; set; }
		public int TransactionID { get; set; }
		//public TransactionVO OriginalTransaction { get; private set; }
		public decimal Amount { get; set; }
		public string CurrencyIso { get; set; }
		public string Comment { get; set; }
		public RefundRequestStatus Status { get; set; }
		public DateTime Date { get; set; }

	}
}
