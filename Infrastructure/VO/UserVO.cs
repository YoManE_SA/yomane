﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;

namespace Netpay.Infrastructure.VO
{
	public class UserVO
	{
		public UserVO(){}

		public UserVO(User user)
		{
			if (user == null)
				return;
			
			Type = user.Type;
			CredentialsToken = user.CredentialsToken;
			Name = user.Name;
			Number = user.Number;
			Email = user.Email;
			ID = user.ID;
			SubID = user.SubID;
			AccountID = user.AccountID;
			UserName = user.UserName;
            IsVerified = user.Verified;
			IsFirstLogin = user.IsFirstLogin;
			AccessList = user.AccessList;
			LastLogin = user.LastLogin;
			Domain = user.Domain.Host;
			PayPercent = user.PayPercent;
			LinkName = user.LinkName;
			MerchantLinks = user.MerchantLinks;
            AllowedMerchants = user.AllowedMerchantsIDs;
            HashKey = user.HashKey;
		}

		public UserType Type { get; set; }
		public Guid CredentialsToken { get; set; }
		public int ID { get; set; }
		public int? SubID { get; set; }
		public int? AccountID { get; set; }
		public string UserName { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Number { get; set; }
        public bool? IsVerified { get; set; }
        public bool IsFirstLogin { get; set; }
		public List<PermissionObjectVO> AccessList { get; set; }
		public DateTime LastLogin { get; set; }
		public string Domain { get; set; }
		public string LinkName { get; set; }
		public decimal? PayPercent { get; set; }
		public Dictionary<string, string> MerchantLinks { get; set; }
		public List<int> AllowedMerchants { get; set; }
        public string HashKey { get; set; }
	}
}
