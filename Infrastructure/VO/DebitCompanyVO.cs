﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class DebitCompanyVO
	{
		public DebitCompanyVO() { }

		public DebitCompanyVO(tblDebitCompany entity)
		{
			ID = entity.DebitCompany_ID;
			DebitCompanyName = entity.dc_name;
			DebitCompanyDescription = entity.dc_description;
			IsActive = entity.dc_isActive;
			IsBlocked = entity.dc_IsBlocked;
			IsRefundAllowed = entity.dc_isAllowRefund;
			IsRefundAllowedPartial = entity.dc_isAllowPartialRefund;
		}

		public int ID { get; set; }
		public string DebitCompanyName { get; set; }
		public string DebitCompanyDescription { get; set; }
		public bool IsActive { get; set; }
		public bool IsBlocked { get; set; }
		public bool IsRefundAllowed { get; set; }
		public bool IsRefundAllowedPartial { get; set; }
	}
}
