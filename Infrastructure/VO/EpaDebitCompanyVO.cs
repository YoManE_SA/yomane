﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class EpaDebitCompanyVO
	{
		public EpaDebitCompanyVO() { }

		public enum FetchMethod{
            HTTP,
            HTTPS,
            FTP,
            SFTP,
            FTPS,
        }
        public EpaDebitCompanyVO(tblEpaDebitCompany entity)
		{
			ID = entity.ID;
			Name = entity.Name;
			IsActive = entity.IsActive;
			EpaFetchServer = entity.EpaFetchServer;
			EpaFetchUsername = entity.EpaFetchUsername;
			EpaFetchPassword = entity.EpaFetchPassword;
		}

		public int ID { get; set; }
		public string Name { get; set; }
		public bool IsActive { get; set; }
		public string EpaFetchServer { get; set; }
		public string EpaFetchUsername { get; set; }
		public string EpaFetchPassword { get; set; }
        public FetchMethod EpaFetchMethod { get; set; }
	}
}
