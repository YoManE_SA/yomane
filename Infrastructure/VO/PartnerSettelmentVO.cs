﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class FinanceSettelmentVO
	{
		public int ID { get; set; }
		public int Affiliate_id { get; set; }
		public int? MerchantSettelment_id { get; set; }
		public int? Merchant_id { get; set; }
		public int? DebitCompany_id { get; set; }
		public DateTime InsertDate { get; set; }
		public decimal Amount { get; set; }
		public Currency Currency { get; set; }
		public string CommentText { get; set; }
		public FinanceSettelmentVO(object entity)
		{
		/*
			ID = entity.AffiliateSettelment_id;
			Affiliate_id = entity.Affiliate_id;
			Merchant_id = entity.MerchantSettelment_id;
			DebitCompany_id = entity.DebitCompany_id;
			InsertDate = entity.InsertDate;
			Amount = entity.Amount;
			Currency = (Currency) entity.Currency;
			CommentText = entity.CommentText;
		*/
		}
	}
}
