﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Domains;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.VO
{
    [Serializable]
    public class PaymentMethodVO
	{
		public PaymentMethodVO() { }

		public PaymentMethodVO(string domainHost, Netpay.Dal.Netpay.PaymentMethod entity) 
		{
			if (entity == null) return;
			ID = entity.PaymentMethod_id;
			Name = entity.Name;
			Abbreviation = entity.Abbreviation;
			IsPopular = entity.IsPopular;
            Gruop = (PaymentMethodGroupEnum)entity.PaymentMethodGroup_id.GetValueOrDefault((int)PaymentMethodGroupEnum.Other);
			if (entity.pm_Type != null)
				Type = (PaymentMethodType)Enum.Parse(typeof(PaymentMethodType), entity.pm_Type.Value.ToString());
			else
				Type = PaymentMethodType.Unknown;
			IsBillingAddressRequired = entity.IsBillingAddressMandatory;
			IsPMInfoRequired = entity.IsPMInfoMandatory;
			IsTerminalRequired = entity.IsTerminalRequired;

			ExpirationDateRequired = entity.IsExpirationDateMandatory;
			Value1Caption = entity.Value1EncryptedCaption;
			Value1ValidationRegex = entity.Value1EncryptedValidationRegex;
			Value2Caption = entity.Value2EncryptedCaption;
			Value2ValidationRegex = entity.Value2EncryptedValidationRegex;

            Dal.DataAccess.NetpayDataContext dc = new Dal.DataAccess.NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			CurrencyCountry = (from cc in dc.PaymentMethodToCountryCurrencies where cc.PaymentMethod_id == ID select new PaymentMethodCountryCurrencyVO(cc)).ToList();
		}

		public int ID { get; set; }
		public string Name { get; set; }
		public string Abbreviation { get; set; }
		public bool IsPopular { get; set; }
		public PaymentMethodType Type { get; set; }
        public PaymentMethodGroupEnum Gruop { get; set; }
		public bool IsBillingAddressRequired { get; set; }
		public bool IsPMInfoRequired { get; set; }
		public bool IsTerminalRequired { get; set; }
		public bool ExpirationDateRequired { get; set; }
		public string Value1Caption { get; set; }
		public string Value1ValidationRegex { get; set; }
		public string Value2Caption { get; set; }
		public string Value2ValidationRegex { get; set; }
		public List<PaymentMethodCountryCurrencyVO> CurrencyCountry { get; set; }
	}
}
