﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class PhoneDetailsVO
	{
        public PhoneDetailsVO(PhoneDetail entity)
		{
			ID = entity.PhoneDetail_id;
			PhoneNumber = entity.PhoneNumber;
			FullName = entity.FullName;
            PhoneCarrierID = entity.PhoneCarrier_id;
			MerchantID = entity.Merchant_id;
			BillingAddressID = entity.BillingAddress_id;
		}
		public PhoneDetailsVO() { }
		public int ID { get; set; }
		public string PhoneNumber { get; set; }
		public string FullName { get; set; }
		public string Email { get; set; }
        public short? PhoneCarrierID { get; set; }
		public int? MerchantID { get; set; }
		public int? BillingAddressID { get; set; }
		public PhoneMessageDirection MessageDirection { get; set; }
	}
}
