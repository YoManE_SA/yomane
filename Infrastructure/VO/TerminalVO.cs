﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class TerminalVO
	{
		public class TerminalComparer : IEqualityComparer<TerminalVO>
		{
			public bool Equals(TerminalVO x, TerminalVO y)
			{
				if (x.TerminalNumber == null || y.TerminalNumber == null) return false;
				if (x.TerminalName == null || y.TerminalName == null) return false;
				return (x.TerminalName.ToLower() == y.TerminalName.ToLower()) && (x.TerminalNumber == y.TerminalNumber);
			}

			public int GetHashCode(TerminalVO obj)
			{
				try
				{
					return obj.TerminalName.GetHashCode();
				}
				catch { return -1; }
			}
		}

		public TerminalVO() { }

		public TerminalVO(string domainHost, tblDebitTerminal entity)
		{
			TerminalID = entity.id;
			DebitCompany = entity.DebitCompany;
			TerminalNumber = entity.terminalNumber;
			IsActive = entity.isActive;
			TerminalName = entity.dt_name;
			ContractNumber = entity.dt_ContractNumber;

			AccountId = entity.accountId;
			AccountSubId = entity.accountSubId;
			AccountPassword = Security.Encryption.Decrypt(domainHost, entity.accountPassword256.ToArray());

			TerminalNumber3D = entity.terminalNumber3D;
			AccountId3D = entity.accountId3D;
			AccountSubId3D = entity.accountSubId3D;
		}

		public int TerminalID { get; set; }
		public byte DebitCompany { get; set; }
		public bool IsActive { get; set; }
		public string TerminalName { get; set; }
		public string ContractNumber { get; set; }

		public string TerminalNumber { get; set; }
		public string AccountId { get; set; }
		public string AccountSubId { get; set; }
		public string AccountPassword { get; set; }

		public string TerminalNumber3D { get; set; }
		public string AccountId3D { get; set; }
		public string AccountSubId3D { get; set; }
	}
}
