﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class FailedConnectionLogVO
	{
		public FailedConnectionLogVO(){}

		public FailedConnectionLogVO(tblLog_NoConnection entity)
		{
			ID = entity.LogNoConnection_id;
			InsertDate = entity.Lnc_InsertDate;
			Amount = entity.Lnc_Amount;
			TransactionSourceID = entity.Lnc_TransactionTypeID;
			Currency = entity.Lnc_Currency;
		}

		public int ID { get; set; }
		public DateTime InsertDate { get; set; }
		public decimal Amount { get; set; }
		public int TransactionSourceID { get; set; }
		public int Currency { get; set; }
	}
}
