﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class RecurringChargeHistoryVO
	{
		public RecurringChargeHistoryVO(){}

		public RecurringChargeHistoryVO(string domainHost, tblRecurringAttempt entity) : this(domainHost, entity, false) { }

		public RecurringChargeHistoryVO(string domainHost, tblRecurringAttempt entity, bool loadPaymentMethod)
		{
			ID = entity.ID;
			Charge = new RecurringChargeVO(domainHost, entity.tblRecurringCharge);
			ReplyCode = entity.ra_ReplyCode;

			if (entity.ra_TransPass != null)
			{
				Transaction = new TransactionVO(domainHost, entity.tblCompanyTransPass, loadPaymentMethod, false, false);
				return;
			}
			if (entity.ra_TransFail != null)
			{
				Transaction = new TransactionVO(domainHost, entity.tblCompanyTransFail, loadPaymentMethod, false);
				return;
			}
			if (entity.ra_TransApproval != null)
			{
				Transaction = new TransactionVO(domainHost, entity.tblCompanyTransApproval, loadPaymentMethod, false);
				return;
			}
			if (entity.ra_TransPending != null)
			{
				Transaction = new TransactionVO(domainHost, entity.tblCompanyTransPending, loadPaymentMethod, false);
				return;
			}
		}

		public int ID { get; set; }
		public RecurringChargeVO Charge { get; set; }
		public TransactionVO Transaction { get; set; }
		public string ReplyCode { get; set; }
	}
}
