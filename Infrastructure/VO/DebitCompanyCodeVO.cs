﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
    [Serializable]
    public class DebitCompanyCodeVO
	{
		public DebitCompanyCodeVO(){}

		public DebitCompanyCodeVO(tblDebitCompanyCode entity)
		{
			DebitCompanyID = entity.DebitCompanyID;
			Code = entity.Code;
			DescriptionMerchantEng = entity.DescriptionMerchantEng;
			DescriptionMerchantHeb = entity.DescriptionMerchantHeb;
			DescriptionCustomerEng = entity.DescriptionCustomerEng;
			DescriptionCustomerHeb = entity.DescriptionCustomerHeb;
		}
		
		public short DebitCompanyID { get; set; }
		public string Code { get; set; }
		public string DescriptionMerchantHeb { get; set; }
		public string DescriptionMerchantEng { get; set; }
		public string DescriptionCustomerHeb { get; set; }
		public string DescriptionCustomerEng { get; set; }
	}
}
