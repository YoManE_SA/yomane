﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.VO
{
	public class CreditTypeVO
	{
		public int ID { get; set; }
		public int Name { get; set; }
	}
}
