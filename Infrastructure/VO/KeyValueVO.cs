﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.VO
{
	public class KeyValueVO<TKey, TValue>
	{
        public KeyValueVO() { }
        
        public KeyValueVO(TKey key, TValue value) 
        {
            Key = key;
            Value = value;
        }

		public TKey Key { get; set; }
		public TValue Value { get; set; }
	}
}
