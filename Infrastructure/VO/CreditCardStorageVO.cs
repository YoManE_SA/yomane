﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;
using Netpay.Infrastructure.Domains;

namespace Netpay.Infrastructure.VO
{
	public class CreditCardStorageVO
	{
		public CreditCardStorageVO() { }

		public CreditCardStorageVO(tblCCStorage entity)
		{
			ID = entity.ID;
			MerchantID = entity.companyID.GetValueOrDefault();
			//CcType = (CreditCardType)entity.ccTypeID;
			PaymentMethod = (PaymentMethodEnum)entity.PaymentMethod;
			PayID = entity.payID;
			InsertDate = entity.InsertDate;
			CardholderID = entity.CHPersonalNum;
			ExpirationMonth = entity.ExpMM;
			ExpirationYear = entity.ExpYY;
			CardholderPhone = entity.CHPhoneNumber;
			CardholderEmail = entity.CHEmail;
			CardholderFullName = entity.CHFullName;
			IPAddress = entity.IPAddress;
			IsDeleted = entity.isDeleted;
			Comment = entity.Comment;			
			CardholderStreet1 = entity.CHStreet;		
			CardholderStreet2 = entity.CHStreet1;
			CardholderCity = entity.CHSCity;
			CardholderZipCode = entity.CHSZipCode;
			StateID = entity.stateId;
			CountryID = entity.countryId;
			BinCountry = entity.BINCountry;
			CreditcardDisplay = entity.CCard_display;
			CcLast4 = entity.CCardLast4;
		}

		public CreditCardStorageVO(tblCustomerCC entity, string domainHost)
		{
			ID = entity.ID;
			//CcType = (CreditCardType)entity.CCard_TypeID;
            PaymentMethod = (PaymentMethodEnum)entity.PaymentMethod;
            PaymentMethodName = PaymentMethod.ToString();
            if (PaymentMethodName.StartsWith("CC")) PaymentMethodName = PaymentMethodName.Substring(2);
			InsertDate = DateTime.MinValue;
			CardholderID = entity.CH_PersonalNumber;
			ExpirationMonth = entity.CCard_ExpMM;
			ExpirationYear = entity.CCard_ExpYY;
			CardholderPhone = entity.CH_PhoneNumber;
			CardholderEmail = entity.CH_Email;
			CardholderFullName = entity.CH_FullName;
			CardholderStreet1 = entity.Billing_Street;
			CardholderCity = entity.Billing_City;
			CardholderZipCode = entity.Billing_Zipcode;
			Cui = Encryption.DecodeCvv(entity.CCard_Cui);
			CardNumberEncrypted = entity.CCard_Number256.ToArray();
			StateID = entity.Billing_State;
			CountryID = entity.Billing_Country;
            Domain domain = DomainsManager.GetDomain(domainHost);
            if (entity.Billing_State > 0)
                State = domain.Cache.GetState(entity.Billing_State).Name;
            if (entity.Billing_Country > 0)
                Country = domain.Cache.GetCountry(entity.Billing_Country).Name;
            CcLast4 = entity.CCard_Last4;
			BinCountry = entity.BINCountry;
		}

		public int ID { get; set; }
		public int MerchantID { get; set; }
		//public CreditCardType CcType { get; set; }
        public PaymentMethodEnum PaymentMethod { get; set; }
        public string PaymentMethodName { get; set; }
		public int PayID { get; set; }
		public DateTime InsertDate { get; set; }
		public string CardholderID { get; set; }
		public string ExpirationMonth { get; set; }
		public string ExpirationYear { get; set; }
		public string CardholderPhone { get; set; }
		public string CardholderEmail { get; set; }
		public string CardholderFullName { get; set; }
		public string CardholderStreet1 { get; set; }
		public string CardholderStreet2 { get; set; }
		public string CardholderCity { get; set; }
		public string CardholderZipCode { get; set; }
		public string IPAddress { get; set; }
		public bool IsDeleted { get; set; }
		public string Comment { get; set; }
		public byte[] CardNumberEncrypted { get; set; }
		public int? StateID { get; set; }
		public int? CountryID { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
		public string BinCountry { get; set; }
		public string Cui { get; set; }
		public string CardNumber { get; set; }
		public string CreditcardDisplay { get; set; }
		public string CcLast4 { get; set; }
	}
}
