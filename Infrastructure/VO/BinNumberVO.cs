﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class BinNumberVO
	{
		public BinNumberVO(){}
		
		public BinNumberVO(tblCreditCardBIN entity)
		{
			BinNumber = entity.BIN;
			CreditCardTypeID = entity.CCType;
			PaymentMethodID = entity.PaymentMethod;
			BinCountryIsoCode2 = entity.isoCode;
		}
		
		public string BinNumber { get; set; }
		public byte CreditCardTypeID { get; set; }
		public byte PaymentMethodID { get; set; }
		public string BinCountryIsoCode2 { get; set; }	
	}
}
