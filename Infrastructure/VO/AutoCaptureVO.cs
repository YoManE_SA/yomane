﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class AutoCaptureVO
	{
		public AutoCaptureVO() { }

		public AutoCaptureVO(tblAutoCapture entity)
		{
			AuthTransactionID = entity.AuthorizedTransactionID;
			CaptureTransactionID = entity.CaptureTransactionID;
			DeclineTransactionID = entity.DeclineTransactionID;
			ActualDate = entity.ActualDate;
			ScheduledDate = entity.ScheduledDate;
		}

		public int AuthTransactionID { get; set; }
		public int? CaptureTransactionID { get; set; }
		public int? DeclineTransactionID { get; set; }
		public DateTime? ActualDate { get; set; }
		public DateTime ScheduledDate { get; set; }
	}
}
