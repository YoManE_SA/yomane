﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure
{
	[AttributeUsage(AttributeTargets.Property)]
	public class EntityPropertyMapping : Attribute
	{
		public EntityPropertyMapping(string entityPropertyName)
		{
			_entityPropertyName = entityPropertyName;
		}

		private string _entityPropertyName = null;

		public string EntityPropertyName
		{
			get { return _entityPropertyName; }
		}
	}
}
