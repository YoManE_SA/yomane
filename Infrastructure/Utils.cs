﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Netpay.Infrastructure
{
    public static class Utils
    {
        public static byte[] Compress(List<FileInfo> files)
        {
            string[] paths = files.Select(i => i.FullName).ToArray();
            return Compress(paths);
        }

        /// <summary>
        /// Compresses files into a zip archive
        /// </summary>
        /// <param name="filePaths"></param>
        /// <returns>zip archive</returns>
        public static byte[] Compress(string[] filePaths)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(stream, ZipArchiveMode.Create))
                {
                    foreach (string filePath in filePaths)
                    {
                        FileInfo file = new FileInfo(filePath);
                        if (!file.Exists)
                            continue;

                        using (FileStream fileStream = file.OpenRead())
                        {
                            ZipArchiveEntry zipFile = archive.CreateEntry(file.Name);
                            using (Stream zipStream = zipFile.Open())
                            {
                                for (long idx = 0; idx < fileStream.Length; idx++)
                                    zipStream.WriteByte((byte)fileStream.ReadByte());
                            }
                        }
                    }
                }

                return stream.GetBuffer();
            }
        }
    }
}
