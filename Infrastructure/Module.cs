﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure
{
	public abstract class Module : BaseDataObject
	{
		private class ModuleNotExist : Module
		{
			private string _name;
			private decimal _version;
			public ModuleNotExist(string name, decimal version) { _name = name; _version = version; }
			public override string Name { get { return _name; } }
			public override decimal Version { get { return _version; } }
			public override string Author { get { return "Unknown"; } }
			public override string Description { get { return "The Code for this module is not installed"; } }

			public override void Install(EventArgs e)
			{
				//throw new Exception("ModuleNotExist " + Name); 
			}
			public override void UnInstall(EventArgs e)
			{
				//throw new Exception("ModuleNotExist " + Name); 
			}
			public override void Activate(EventArgs e)
			{
				//throw new Exception("ModuleNotExist " + Name); 
			}
			public override void Deactivate(EventArgs e)
			{
				//throw new Exception("ModuleNotExist " + Name); 
			}
			protected override void OnActivate(EventArgs e) { base.OnDeactivate(e); }
			protected override void OnDeactivate(EventArgs e) { }

		}

		//[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false )]
		//public class ModuleClass : Attribute {}
		private Dal.Netpay.AppModule _entity;
		
		//public string Name { get { return _entity.Module_id; } }
		//public abstract string Version { get; }
		public int ID { get{ return _entity.AppModule_id; } }
		public abstract string Name { get; }
		public abstract decimal Version { get; }
		public virtual string Author { get { return "Obl ltd."; } }
		public virtual string Description { get { return null; } }

		//public decimal? Version { get { return _entity.Version; } }
		public System.DateTime? InstallDate { get { return _entity.InstallDate; } }
		public bool IsInstalled { get { return _entity.IsInstalled; } }
		public bool IsActive { get { return _entity.IsActive; } }

		public string GetSetting(string key)
		{
			return DataContext.Reader.AppModuleSettings.Where(v => v.AppModule_id == ID && v.ValueName == key).Select(v => v.Value).SingleOrDefault();
		}
		public void SetSetting(string key, string value)
		{
			using (var dc = new DataContext(true)) {
				var f = dc.AppModuleSettings.Where(v => v.AppModule_id == ID && v.ValueName == key).FirstOrDefault();
				if (f == null) {
					f = new Dal.Netpay.AppModuleSetting() { AppModule_id = ID, ValueName = key };
					dc.AppModuleSettings.InsertOnSubmit(f);
				}
				f.Value = value;
				dc.SubmitChanges();
			}
		}

		public string GetSetting(int accountId, string key)
		{
			return DataContext.Reader.AppModuleAccountSettings.Where(v => v.AppModule_id == ID && v.Account_id == accountId && v.ValueName == key).Select(v => v.Value).SingleOrDefault();
		}

		public void SetSetting(int accountId, string key, string value)
		{
			using (var dc = new DataContext(true))
			{
				var f = dc.AppModuleAccountSettings.Where(v => v.AppModule_id == ID && v.Account_id == accountId && v.ValueName == key).FirstOrDefault();
				if (f == null)
				{
					f = new Dal.Netpay.AppModuleAccountSetting() { AppModule_id = ID, Account_id = accountId, ValueName = key };
					dc.AppModuleAccountSettings.InsertOnSubmit(f);
				}
				f.Value = value;
				dc.SubmitChanges();
			}
		}



		protected virtual void OnLoad(EventArgs e) { }
		protected virtual void OnInstall(EventArgs e) { _entity.IsInstalled = true; Save(); }
		protected virtual void OnUninstall(EventArgs e) { _entity.IsInstalled = false; Save(); }
		protected virtual void OnActivate(EventArgs e) { _entity.IsActive = true; }
		protected virtual void OnDeactivate(EventArgs e) { _entity.IsActive = false; }
		protected virtual void OnUnload(EventArgs e) { }

		public virtual void Install(EventArgs e) 
		{ 
			if (_entity.AppModule_id == 0) Save();
			OnInstall(e); 
		}
		public virtual void UnInstall(EventArgs e) { OnUninstall(e); }
		public virtual void Activate(EventArgs e) { if (IsActive) throw new Exception("Wrong module state"); OnActivate(e); Save(); }
		public virtual void Deactivate(EventArgs e) { if (!IsActive) throw new Exception("Wrong module state"); OnDeactivate(e); Save(); }


		protected void Save()
		{
			_entity.Name = Name;
			_entity.Version = Version;
			DataContext.Writer.AppModules.Update(_entity, (_entity.AppModule_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public static void Init()
		{
			foreach (var d in Domain.Domains.Values) {
				try {
					Domain.Current = d;
					GetCache();
				} catch(Exception ex) {
					Logger.Log(ex, "While loading modules for domain:" + d.Host);
				}
			}
		}

		public static Dictionary<string, Module> GetCache()
		{
			lock(typeof(Module))
			{
				return Domain.Current.GetCachData("Application.Module", () =>
				{
					ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
					try { 
						var dbModules = (from g in DataContext.Reader.AppModules select g).ToList();
						var value = new Dictionary<string, Module>();

						var types = (from a in AppDomain.CurrentDomain.GetAssemblies().AsParallel()
									 from t in a.GetTypes()
									 where !t.IsAbstract && t.IsPublic && t.IsSubclassOf(typeof(Infrastructure.Module))
									 select t).ToList();

						//var allTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(v => v.GetTypes()).ToList();
						//var types = (from t in allTypes where !t.IsAbstract && t.IsPublic && t.IsSubclassOf(typeof(Infrastructure.Module)) select t).ToList();
						foreach (var t in types) {
							var c = t.GetConstructor(new Type[] { }).Invoke(new object[] { }) as Module;
							var entity = dbModules.Where(n => n.Name == c.Name).SingleOrDefault(); 
							if (entity == null) entity = new Dal.Netpay.AppModule();
							else dbModules.Remove(entity);
							c._entity = entity;
							value.Add(c.Name, c);
						}
						foreach (var v in dbModules) {
							v.IsInstalled = true; v.IsActive = false;
							value.Add(v.Name, new ModuleNotExist(v.Name, v.Version) { _entity = v });
						}
						foreach (var m in value)
							try { m.Value.OnLoad(EventArgs.Empty); }
							catch (Exception ex) { Logger.Log(ex, "Fail Loading module:" + m.Key); }
						foreach (var m in value) {
							if ((m.Value.Version > m.Value._entity.Version) || m.Value.IsInstalled) {
								try { m.Value.Install(EventArgs.Empty); }
								catch (Exception ex) { Logger.Log(ex, "Fail Installing module:" + m.Key); }
							}
						}
						foreach (var m in value)
							if (m.Value.IsInstalled && m.Value.IsActive) { 
								try { m.Value.OnActivate(EventArgs.Empty); }
								catch (Exception ex) { Logger.Log(ex, "Fail Activating module:" + m.Key); }
							}
						return new Domain.CachData(value, DateTime.MaxValue);
					} finally {
						ObjectContext.Current.StopImpersonate();
					}
				}) as Dictionary<string, Module>;
			}
		}

		public static Module Get(string name)
		{
			Module module;
			if (!GetCache().TryGetValue(name, out module)) return null;
			return module;
		}

		public static Module Get(int id)
		{
			return GetCache().Values.Where(n => n.ID == id).SingleOrDefault();
		}

	}
}
