﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure
{
	public class Domain
	{
		public static bool EnableClearTempDirectory { get; set; }
		public class CachData
		{
			public CachData(object data, DateTime validDate) { Value = data; ValidToDate = validDate; }
			public object Value;
			public DateTime ValidToDate;
		}

		private Guid _serviceCredentials;
		private Dictionary<string, CachData> _cachData;
		private Dictionary<string, string> _config;
		private Dictionary<string, object> _items;

		//public string ActiveDirectoryDomain { get { return GetConfig("ActiveDirectoryDomain", false); } }
		public string Host { get; private set; }
		public Guid GuestCredentials { get; private set; }
		public int EncryptionKeyNumber { get; set; }
		public string NetpayAdminsNTGroupName { get; set; }
		public string ServiceUser { get; set; }
		public string ServiceUserPassword { get; set; }
		public string BrandName { get; set; }
		public string LegalName { get; set; }
		public string ShortCode { get; set; }
        public int HostCountryId { get; set; }
        public string ThemeFolder { get; set; }
		public string DataPath { get; set; }
		public string PublicDataVirtualPath { get; set; }
		public string CalcPublicDataVirtualPath {
			get {
				if (string.IsNullOrEmpty(Infrastructure.Application.ApplicationPath)) return PublicDataVirtualPath;
				return System.IO.Path.Combine(Infrastructure.Application.ApplicationPath, PublicDataVirtualPath);
			}
		}

		public string MapFilesCommonPath
		{
			get
			{
				string filesCommonPath = System.Web.Hosting.HostingEnvironment.MapPath(CommonVirtualPath);
				return filesCommonPath;
			}
		}

		public string MapPaymentMethoddImage23On12
		{
			get
			{
				return System.IO.Path.Combine(MapFilesCommonPath, "ImgPaymentMethod", "23X12");
			}
		}

		public bool EnableEpa { get; set; }
		public bool ForceSSL { get; set; }
		public bool IsHebrewVisible { get; set; }

		public string Sql1ConnectionString { get; set; }
		public string Sql2ConnectionString { get; set; }
		public string ReportsConnectionString { get; set; }
		public string SqlArchiveConnectionString { get; set; }
		public string AnalysisServicesConnectionString { get; set; }
		//public string IntegrationServicesNetpayConnectionString { get; set; }
		public string IntegrationServicesReportsConnectionString { get; set; }

		public string MailNameFrom { get; set; }
		public string MailAddressFrom { get; set; }
		public string MailNameTo { get; set; }
		public string MailAddressTo { get; set; }
		public string MailAddressFromChb { get; set; }

		public string SmsServiceProvider { get; set; }
		public string SmsServiceUserName { get; set; }
		public string SmsServicePassword { get; set; }
		public string SmsServiceFrom { get; set; }

		//public string SftHost { get; set; }
		//public string SftpPort { get; set; }
		//public string SftpUsername { get; set; }
		//public string SftpPassword { get; set; }
		 
		public string Sw2RecipientEmail { get; set; }
		public string Sw2RecipientEmailCashbuild { get; set; }

        public string MerchantUrl { get; set; }
		public string ReportsUrl { get; set; }
		public string DevCentertUrl { get; set; }
		public string ProcessUrl { get; set; }
		public string ProcessV2Url { get; set; }
		public string CartUrl { get; set; }
		public string WalletUrl { get; set; }
		public string WebServicesUrl { get; set; }
		public string PartnersUrl { get; set; }
		public string ShopUrl { get; set; }
		public string ContentUrl { get; set; }
		public string SignupUrl { get; set; }
		public string CommonPhisicalPath { get; set; }
		public bool UsePublicPage { get; set; }

		public string SocialMediaFacebook { get; set; }
		public string SocialMediaGooglePlus { get; set; }
		public string SocialMediaLinkedIn { get; set; }
		public string SocialMediaTwitter { get; set; }

		public string CustomerServiceEmail { get; set; }
		public string CustomerServiceSkype { get; set; }
		public string CustomerServiceFax { get; set; }
		public string CustomerServicePhone { get; set; }
		public string CustomerServiceUSPhone { get; set; }

		public string TechnicalSupportEmail { get; set; }
		public string TechnicalSupportPhone { get; set; }
		public string TechnicalSupportFax { get; set; }
		public string TechnicalSupportSkype { get; set; }

		public string IdentityAddress { get; set; }
		public string IdentityCity { get; set; }
		public string IdentityZipcode { get; set; }
		public string IdentityCountry { get; set; }

		public string CRMEmailTemplatePath { get; set; }
		public string CRMConnectionString { get; set; }

		public string FinanceLevel1Group { get; set; }
		public string FinanceLevel2Group { get; set; }

		// monitoring & fraud detection recipients
		public string FraudDetectionAlertRecipients { get; set; }
		public string ActiveMerchantReportRecipients { get; set; }
		public string BnsTransactionsWithoutEpaReportRecipients { get; set; }
		public string IrregularityReportRecipients { get; set; }
		public string DetectPhotocopyWithRefundRecipients { get; set; }

		/*
		public string FacebookShopsAppID { get; set; }
		public string FacebookShopsSecretID { get; set; }
		public string FacebookShopsNamespace { get; set; }
		public string ZohoCRMAuth { get; set; }

		public string PayoneerServiceBase { get; set; }
		public string PayoneerUniqueID { get; set; }
		public string PayoneerUsername { get; set; }
		public string PayoneerPassword { get; set; }
		public string PayoneerProgramID { get; set; }
		*/

		public Domain()
		{
			_cachData = new Dictionary<string, CachData>();
			_config = new Dictionary<string, string>();
			_items = new Dictionary<string, object>();
			GuestCredentials = Guid.NewGuid();
		}

		private string LoadConfigValue(System.Xml.XmlElement paramElement)
		{
			string paramValue = paramElement.GetAttribute("value");
			if (paramElement.GetAttribute("isEncrypted").ToNullableBool().GetValueOrDefault())
				paramValue = Security.Encryption.DecryptHex(EncryptionKeyNumber, paramValue);
			return paramValue;
		}

		private void LoadFromXml(System.Xml.XmlElement configElement)
		{
			foreach (System.Xml.XmlElement v in configElement.GetElementsByTagName("parameter"))
			{
				string key = v.GetAttribute("key");
				if (_config.ContainsKey(key)) _config[key] = LoadConfigValue(v);
				else _config.Add(key, LoadConfigValue(v));
				if (key == "encryptionKeyNumber") EncryptionKeyNumber = int.Parse(v.GetAttribute("value"));
			}
		}

		private string GetHardConfig(string key)
		{
			if (!_config.ContainsKey(key))
				throw new ApplicationException("Parameter '" + key + "' not found in configuration file");
			var ret = _config[key];
			_config.Remove(key);
			return ret;
		}

		public string GetConfig(string key, bool throwError = true) 
		{
			if (!_config.ContainsKey(key)) {
				if (!throwError) return null;
				throw new ApplicationException("Parameter '" + key + "' not found in configuration file");
			}
			return _config[key];
		}

		public string GetConfig(string key, string defValue)
		{
			if (!_config.ContainsKey(key)) return null;
			return _config[key];
		}

		public string CommonVirtualPath { get { return GetConfig("CommonVirtualPath", false).NullIfEmpty().ValueIfNull("/NPCommon/"); } }

		private void Init(string filePath)
		{
			Host = GetHardConfig("host");
			EncryptionKeyNumber = int.Parse(GetHardConfig("encryptionKeyNumber"));
			NetpayAdminsNTGroupName = GetHardConfig("netpayAdminsNTGroupName");
			ServiceUser = GetHardConfig("ServiceUser");
			ServiceUserPassword = GetHardConfig("ServiceUserPassword");
			BrandName = GetHardConfig("brandName");
			LegalName = GetHardConfig("legalName");
			ShortCode = GetHardConfig("shortCode");
            HostCountryId = Convert.ToInt32( GetHardConfig("HostCountryId"));
            ThemeFolder = GetHardConfig("themeFolder");
			DataPath = GetHardConfig("DataPath");
			PublicDataVirtualPath = GetHardConfig("PublicDataVirtualPath");
			EnableEpa = bool.Parse(GetHardConfig("enableEpa"));
			ForceSSL = bool.Parse(GetHardConfig("forceSSL"));
			IsHebrewVisible = bool.Parse(GetHardConfig("isHebrewVisible"));

			Sql1ConnectionString = GetHardConfig("sql1ConnectionString");
			Sql2ConnectionString = GetHardConfig("sql2ConnectionString");
			ReportsConnectionString = GetHardConfig("reportsConnectionString");
			SqlArchiveConnectionString = GetHardConfig("sqlArchiveConnectionString");
			AnalysisServicesConnectionString = GetHardConfig("analysisServicesConnectionString");
			IntegrationServicesReportsConnectionString = GetHardConfig("integrationServicesReportsConnectionString");

			MailNameFrom = GetHardConfig("mailNameFrom");
			MailAddressFrom = GetHardConfig("mailAddressFrom");
			MailNameTo = GetHardConfig("mailNameTo");
			MailAddressTo = GetHardConfig("mailAddressTo");
			MailAddressFromChb = GetHardConfig("mailAddressFromChb");

			SmsServiceProvider = GetHardConfig("SmsServiceProvider");
			SmsServiceUserName = GetHardConfig("SmsServiceUserName");
			SmsServicePassword = GetHardConfig("SmsServicePassword");
			SmsServiceFrom = GetHardConfig("SmsServiceFrom");

			//SftHost = GetHardConfig("SftHost");
			//SftpPort = GetHardConfig("SftpPort");
			//SftpUsername = GetHardConfig("SftpUsername");
			//SftpPassword = GetHardConfig("SftpPassword");
			Sw2RecipientEmail   = GetHardConfig("Sw2RecipientEmail");
			Sw2RecipientEmailCashbuild   = GetHardConfig("Sw2RecipientEmailCashbuild");


            MerchantUrl = GetHardConfig("merchantUrl");
			ReportsUrl = GetHardConfig("reportsUrl");
			DevCentertUrl = GetHardConfig("devCentertUrl");
			ProcessUrl = GetHardConfig("processUrl");
			ProcessV2Url = GetHardConfig("processV2Url");
			WebServicesUrl = GetHardConfig("WebServicesUrl");
			PartnersUrl = GetHardConfig("partnersUrl");
			CartUrl = GetHardConfig("cartUrl");
			ShopUrl = GetHardConfig("shopUrl");
			WalletUrl = GetHardConfig("walletUrl");
			ContentUrl = GetHardConfig("contentUrl");
			SignupUrl = GetHardConfig("signupUrl");
			UsePublicPage = GetConfig("usePublicPage", false).ToNullableBool().GetValueOrDefault(false);

			//CommonVirtualPath = GetHardConfig("commonVirtualPath"); -- soft config
			CommonPhisicalPath = GetHardConfig("commonPhisicalPath");

			CRMEmailTemplatePath = GetHardConfig("CRMEmailTemplatePath");
			CRMConnectionString = GetHardConfig("CRMConnectionString");

			TechnicalSupportEmail = GetHardConfig("TechnicalSupport_Email");
			TechnicalSupportPhone = GetHardConfig("TechnicalSupport_Phone");
			TechnicalSupportFax = GetHardConfig("TechnicalSupport_Fax");
			TechnicalSupportSkype = GetHardConfig("TechnicalSupport_Skype");

			CustomerServiceEmail = GetHardConfig("CustomerService_Email");
			CustomerServiceSkype = GetHardConfig("CustomerService_Skype");
			CustomerServiceFax = GetHardConfig("CustomerService_Fax");
			CustomerServicePhone = GetHardConfig("CustomerService_Phone");
			CustomerServiceUSPhone = GetHardConfig("CustomerService_USPhone");

			SocialMediaFacebook = GetHardConfig("SocialMedia_Facebook");
			SocialMediaGooglePlus = GetHardConfig("SocialMedia_GooglePlus");
			SocialMediaLinkedIn = GetHardConfig("SocialMedia_LinkedIn");
			SocialMediaTwitter = GetHardConfig("SocialMedia_Twitter");

			IdentityAddress = GetHardConfig("Identity_Address");
			IdentityCity = GetHardConfig("Identity_City");
			IdentityZipcode = GetHardConfig("Identity_Zipcode");
			IdentityCountry = GetHardConfig("Identity_Country");

			FinanceLevel1Group = GetHardConfig("FinanceLevel1Group");
			FinanceLevel2Group = GetHardConfig("FinanceLevel2Group");
			
			FraudDetectionAlertRecipients = GetHardConfig("fraudDetectionAlertRecipients");
			ActiveMerchantReportRecipients = GetHardConfig("ActiveMerchantReportRecipients");
			BnsTransactionsWithoutEpaReportRecipients = GetHardConfig("BnsTransactionsWithoutEpaReportRecipients");
			IrregularityReportRecipients = GetHardConfig("IrregularityReportRecipients");
			DetectPhotocopyWithRefundRecipients = GetHardConfig("DetectPhotocopyWithRefundRecipients");
			
			//check paths
			if (DataPath != null) { 
				if (DataPath.StartsWith("~/"))
					DataPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(Application.PhysicalPath, DataPath.Substring(2)));
				else if ((DataPath.StartsWith(@"/") || DataPath.StartsWith(@"\")) && !(DataPath.StartsWith(@"//") || DataPath.StartsWith(@"\\")))
					DataPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(filePath), DataPath.Substring(1)));
				if (System.IO.Directory.Exists(DataPath))
					if (!System.IO.Directory.Exists(MapTempPath(null)))
						System.IO.Directory.CreateDirectory(MapTempPath(null));
			}
		}

		public Guid ServiceCredentials
		{
			get
			{
				lock (this)
				{
					if (_serviceCredentials != Guid.Empty) return _serviceCredentials;
					Guid retId;
					if (Security.Login.DoLogin(new Security.UserRole[] { Security.UserRole.Admin }, ServiceUser, ServiceUser, ServiceUserPassword, null, out retId, IsWinServiceUser:true) == Security.LoginResult.Success) _serviceCredentials = retId;
					else throw new NotLoggedinException(string.Format("Service unable to login to domain {0}", Host));
					return _serviceCredentials;
				}
			}
		}

		public string LogoPath
		{
			get{
				Uri siteURL = new Uri(ProcessUrl);
				return siteURL.Scheme + "://" + siteURL.Host + "/NPCommon/Images";
			}
		}

		public string MapDataPath(string fileName = null)
		{
			if (fileName != null) return System.IO.Path.Combine(DataPath, fileName);
			return DataPath;
		}

		public string MapPrivateDataPath(string fileName = null)
		{
			if (fileName != null) return System.IO.Path.Combine(DataPath, "Private", fileName);
			return DataPath;
		}

		public string MapPublicDataPath(string fileName = null)
		{
			if (fileName != null) return System.IO.Path.Combine(DataPath, "Public", fileName);
			return DataPath;
		}

		public string MapPublicDataVirtualPath(string fileName = null)
		{
			if (fileName != null) return System.IO.Path.Combine(CalcPublicDataVirtualPath, fileName);
			return CalcPublicDataVirtualPath;
		}

		public string ConvertPublicVirtualToPhysical(string fileName = null) 
		{ 
			if (string.IsNullOrEmpty(fileName)) return fileName;
			if (!fileName.StartsWith(CalcPublicDataVirtualPath)) throw new Exception("address must start with '" + CalcPublicDataVirtualPath + "'");
			fileName = fileName.Substring(CalcPublicDataVirtualPath.Length);
			return MapPublicDataPath(fileName);
		}

		public string MapTempPath(string fileName = null)
		{
			if (fileName != null) return System.IO.Path.Combine(MapPrivateDataPath("Temp"), fileName);
			return MapPrivateDataPath("Temp");
		}

		public static string GetUniqueFileName(string fileName = null)
		{
			int index = 1;
			string tempFileFullName = fileName;
			while (System.IO.File.Exists(tempFileFullName))
			{
				tempFileFullName = string.Format(@"{0}\{1}({2}){3}", System.IO.Path.GetDirectoryName(fileName), System.IO.Path.GetFileNameWithoutExtension(fileName), index.ToString(), System.IO.Path.GetExtension(fileName));
				index++;
			}
			return tempFileFullName;
		}

		public string EmailTemplatePath { get { return MapPrivateDataPath("Email Templates/"); } }

		/********************************************* items new design by udi ********************************************/
		public T GetItem<T>(string key, Func<T> initFunction)
		{
			lock (_items)
			{
				object ret;
				if (!_items.TryGetValue(key, out ret))
				{
					if (initFunction == null) return default(T);
					_items.Add(key, ret = initFunction());
				}
				return (T)ret;
			}
		}


		/********************************************* new design by udi ********************************************/

		public void SetCachData(string key, object value, DateTime? validToDate = null)
		{
			lock (_cachData) _cachData[key] = new CachData(value, validToDate.GetValueOrDefault(DateTime.MaxValue));
		}

		public object GetCachData(string key, Func<CachData> initFunction)
		{
			lock (_cachData)
			{
				if (_cachData.ContainsKey(key)) return _cachData[key].Value;
				if (initFunction != null)
				{
					var ret = initFunction();
					_cachData.Add(key, ret);
					return ret.Value;
				}
			}
			return null;
		}

		public object GetCachData(string key)
		{
			CachData ret;
			lock (_cachData)
				if (_cachData.TryGetValue(key, out ret)) return ret.Value;
			return null;
		}

		public void RemoveCachData(string key)
		{
			lock (_cachData) _cachData.Remove(key);
		}

		public void SetCachValidDate(string key, DateTime value)
		{
			lock (_cachData) _cachData[key].ValidToDate = value;
		}

		public void ClearCach() 
		{ 
			lock(_cachData) {
				var removeKeys = (from v in _cachData where v.Value.ValidToDate < DateTime.Now select v.Key).ToList();
				foreach (var v in removeKeys)
					_cachData.Remove(v);
			}
		}

		public void ClearTempDirecrory() 
		{
			if (!Infrastructure.Tasks.Lock.TryLock("Domain.ClearTemp", DateTime.Now.AddMinutes(-5))) return;
			try {
				var files = new System.IO.DirectoryInfo(MapTempPath()).GetFiles().Where(v => v.CreationTime < DateTime.Now.AddHours(-2));
				foreach (var v in files) try { v.Delete(); } catch { }
			} catch (Exception ex) {
				Logger.Log(LogTag.Updates, ex);
			} finally {
				Infrastructure.Tasks.Lock.Release("Domain.ClearTemp");
			}
		}

		private static System.Threading.Timer _cachTimer = null;
		public static Dictionary<string, Domain> Domains { get; private set; }

		private static void TimerClearCach(object state)
		{
			foreach (var d in Domains)
			{
				Domain.Current = d.Value;
				Domain.Current.ClearCach();
				if (EnableClearTempDirectory) Domain.Current.ClearTempDirecrory();
			}
		}

		public static void Init(System.Xml.XmlElement configElement)
		{
			Domains = new Dictionary<string,Domain>();
			foreach (System.Xml.XmlElement v in configElement.GetElementsByTagName("domain"))
			{
				var domainElement = v;
				var fileNameAtt = v.GetAttribute("fileName");
				try {
					Domain domain = new Domain();
					if (!string.IsNullOrEmpty(fileNameAtt))
					{
						if (fileNameAtt.StartsWith("~/")) fileNameAtt = Application.MapAppPath(fileNameAtt.Substring(2));
						var doaminFile = new System.Xml.XmlDocument();
						doaminFile.Load(fileNameAtt);
						domain.LoadFromXml(doaminFile.DocumentElement);
					} else fileNameAtt = Application.PhysicalPath;
					domain.LoadFromXml(v);
					domain.Init(fileNameAtt);
					Domains.Add(domain.Host, domain);
				} catch (Exception ex) {
					throw new Exception(string.Format("while loading domain #{0} : {1}", Domains.Count, string.IsNullOrEmpty(fileNameAtt) ? Application.ConfigFileName : fileNameAtt), ex);
				}
			}
			if (Domains.Count > 0 && _cachTimer == null)
				_cachTimer = new System.Threading.Timer(TimerClearCach, null, (5 * 60 * 1000), (5 * 60 * 1000));
		}

		public static Domain Current
		{
			get { 
				if (!ObjectContext.Current.Items.ContainsKey("Domain")) return null; 
				return ObjectContext.Current.Items["Domain"] as Domain; 
			} 
			set {
				//if (ObjectContext.Current.Items.ContainsKey("Domain") && ObjectContext.Current.Items["Domain"] == value) return;
				ObjectContext.Current.Items["Domain"] = value; 
				DataContext.Reader = null; DataContext.Writer = null;
			}
		}

		public static Domain Get(string domainHost)
		{
			if (domainHost == null)
				throw new ArgumentNullException("domainHost");

			if (Domains == null || Domains.Count == 0)
				throw new ApplicationException("No domains loaded. Init infrastructure?");

			string originalName = domainHost;
			while (domainHost != "" && !Domains.ContainsKey(domainHost))
			{
				int nextComponent = domainHost.IndexOf('.');
				if (nextComponent == -1) nextComponent = domainHost.Length - 1;
				domainHost = domainHost.Substring(nextComponent + 1);
			}

			if (domainHost == "")
			{
				if (Domains.ContainsKey("default")) domainHost = "default";
				else throw new ApplicationException(string.Format("Domain '{0}' was not found.", originalName));
			}
			return Domains[domainHost];
		}



	}
}