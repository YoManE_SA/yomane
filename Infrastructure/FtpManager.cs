﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WinSCP;



namespace Netpay.Infrastructure
{

    public class FtpManager
    {
        private readonly SessionOptions _sessionOptions;
        private static string ExecutableFilePath
        {
            get
            {
                return Path.ChangeExtension(Infrastructure.Application.GetParameter("sftpExecutablePath"), ".exe");
              //  return @"C:\Program Files (x86)\WinSCP\V_511_2\WinSCP.exe";
            }
        }
        private IList<FtpDirectory> Directories { get; set; }

        public FtpManager(FtpCredentials settings)
        {
            _sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Ftp,
                HostName = settings.HostIp,
                PortNumber = settings.PortNumber,
                UserName = settings.Username,
                Password = settings.Password,
                //FtpSecure = FtpSecure.Explicit,
                //GiveUpSecurityAndAcceptAnyTlsHostCertificate = true
            };
            Directories = new List<FtpDirectory>();
        }
        /// <summary>
        /// Recursively lists the folder contents
        /// </summary>
        /// <param name="rootFolder">Root folder</param>
        /// <param name="directoryMask">only folders with that name will be listed (nullable)</param>
        /// <param name="fileMask">only files with that mask will be listed (nullable)</param>
        /// <returns></returns>
        public IList<FtpDirectory> ListAllContents(string rootFolder, string directoryMask, string fileMask)
        {
            RecursiveFolderSearch(rootFolder);
            IList<FtpDirectory> finalList = new List<FtpDirectory>();
            if (!string.IsNullOrEmpty(directoryMask))
            {
                var item = Directories.Where(x => x.DirectoryName.ToLower().EndsWith(directoryMask.ToLower())).ToList();
                foreach (var directory in item)
                {
                    IList<string> filteredFileList = new List<string>();
                    if (!string.IsNullOrEmpty(fileMask))
                    {
                        var temp = directory.FileList.Where(x => x.ToLower().EndsWith(fileMask.ToLower()));

                        foreach (var fileName in temp)
                        {
                            filteredFileList.Add(fileName);
                        }
                    }
                    else
                    {
                        filteredFileList = directory.FileList;
                    }
                    finalList.Add(new FtpDirectory
                    {
                        DirectoryName = directory.DirectoryName,
                        FileList = filteredFileList
                    });
                }
            }
            else
            {
                finalList = Directories;
            }
            return finalList;
        }
        private void RecursiveFolderSearch(string rootFolder)
        {
            using (var session = new Session())
            {
                session.ExecutablePath = ExecutableFilePath;
                session.Open(_sessionOptions);
                var items = session.ListDirectory(rootFolder);
                var subDirectories = new List<string>();
                var files = new List<string>();

                foreach (RemoteFileInfo item in items.Files)
                {
                    if (item.IsDirectory)
                    {
                        //Exclude parent DIR
                        if (!item.Name.ToLower().Equals(rootFolder.ToLower()))
                        {
                            if (!item.Name.Equals(".."))
                                subDirectories.Add(Path.Combine(rootFolder, item.Name));
                        }
                    }
                    else
                    {
                        if (!item.Name.Equals(".."))
                            files.Add(Path.Combine(rootFolder, item.Name));
                    }

                    //if (item.IsDirectory )
                    //{
                    //    //Exclude parent DIR
                    //    if (!item.IsParentDirectory)
                    //    {
                    //        subDirectories.Add(item.FullName);
                    //    }
                    //}
                    //else
                    //{
                    //    files.Add(item.FullName);
                    //}
                }
                var dir = new FtpDirectory { DirectoryName = rootFolder, FileList = files };
                Directories.Add(dir);
                foreach (var subDirectory in subDirectories)
                {
                    RecursiveFolderSearch(subDirectory);
                }
            }
        }
        public string DownloadFile(string remoteFileLocation, string localFileLocation, bool deleteRemote = true)
        {
            remoteFileLocation = remoteFileLocation.Replace("\\", "/");
            remoteFileLocation = remoteFileLocation.Replace("//", "/");
            string result = string.Empty;
            using (var session = new Session())
            {
                session.ExecutablePath = ExecutableFilePath;

                session.DisableVersionCheck = true;
                // Connect
                session.Open(_sessionOptions);
                // Upload files
                var transferOptions = new TransferOptions { TransferMode = TransferMode.Binary };
                var tor = session.GetFiles(remoteFileLocation, localFileLocation, deleteRemote,
                    transferOptions);
                tor.Check();
                var x = tor.Transfers.ToList();
                if (x.Any())
                {
                    result = x[0].Destination;
                }

            }
            return result;
        }

        public static KeyValuePair<bool, StringBuilder> UploadFileToSftpServer(string sourceFileName, string destinationFolder,
            FtpCredentials credentials)
        {
            var dic = new Dictionary<bool, StringBuilder>();

            var sb = new StringBuilder();
            try
            {
                var sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = credentials.HostIp,
                    UserName = credentials.Username
                };
                if (!string.IsNullOrEmpty(credentials.Password))
                {
                    sessionOptions.Password = credentials.Password;
                }
                if (!string.IsNullOrEmpty(credentials.SshHostKeyFingerprint))
                {
                    sessionOptions.SshHostKeyFingerprint = credentials.SshHostKeyFingerprint;
                }
                if (!string.IsNullOrEmpty(credentials.SshPrivateKeyPath))
                {
                    sessionOptions.SshPrivateKeyPath = credentials.SshPrivateKeyPath;
                }
                using (var session = new Session())
                {

                    // Connect
                    session.ExecutablePath = ExecutableFilePath;
                    session.Open(sessionOptions);
                    // Upload files 
                    var transferOptions = new TransferOptions { TransferMode = TransferMode.Binary };

                    var tor = session.PutFiles(sourceFileName, destinationFolder, false, transferOptions);
                    var uploadTime = DateTime.Now;
                    tor.Check();
                    var transferList = tor.Transfers.ToList();
                    foreach (TransferEventArgs transfer in transferList)
                    {
                        sb.AppendFormat("{0} was created on {1} then moved to {2} on {3}{4}", transfer.FileName,
                            transfer.Touch != null ? transfer.Touch.LastWriteTime.ToString("f") : "N/A",
                            transfer.Destination, uploadTime.ToString("f"),
                            Environment.NewLine);
                    }
                    dic.Add(true, sb);
                }
            }
            catch (Exception ex)
            {
                sb = new StringBuilder();
                sb.AppendFormat("An error occured when trying to upload {0}. Error Message: {1}{2}", sourceFileName, ex.Message, Environment.NewLine);
                dic.Add(false, sb);

            }
            return dic.FirstOrDefault();
        }
    }

    public class FtpDirectory
    {
        public string DirectoryName { get; set; }
        public IList<string> FileList { get; set; }
    }

    public class FtpCredentials
    {
        public string HostIp { get; set; }
        public int PortNumber { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string SshHostKeyFingerprint { get; set; }
        public string SshPrivateKeyPath { get; set; }
    }

    public enum FtpProtocol
    {
        Ftp = 1,
        Sftp = 2,
        Ftpes = 3,
    }
}
