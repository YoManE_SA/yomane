﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.Security
{
    public enum PermissionValue { Deny = 0x00, Execute = 0x01, None = 0x00, Read = 0x02, Add = 0x04, Edit = 0x08, Delete = 0x10, FullControl = 0xFF }
    public enum PermissionGroup
    {
        Execute = PermissionValue.Execute,
        AllowDeney = PermissionValue.Execute | PermissionValue.Deny,
        Read = PermissionValue.Read,
        ReadEdit = PermissionValue.Read | PermissionValue.Edit,
        ReadDelete = PermissionValue.Read | PermissionValue.Delete,
        ReadEditDelete = PermissionValue.Read | PermissionValue.Edit | PermissionValue.Delete,
        ExecuteReadEditDelete = PermissionValue.Execute | PermissionValue.Read | PermissionValue.Edit | PermissionValue.Delete,
        ExecuteReadEdit = PermissionValue.Execute | PermissionValue.Read | PermissionValue.Edit
    }

    public class SecuredObject : BaseDataObject
    {
        private Dal.Netpay.SecurityObject _entity;
        public int ID { get { return _entity.SecurityObject_id; } }
        public int ModuleID { get { return _entity.AppModule_id; } }
        public string Name { get { return _entity.Name; } }
        public string Description { get { return _entity.Description; } set { _entity.Description = value; } }

        public PermissionGroup ValueGroup { get; private set; }

        private SecuredObject(Dal.Netpay.SecurityObject entity)
        {
            _entity = entity;
            ValueGroup = (PermissionGroup)ParseString(_entity.Value);
        }
        public SecuredObject(int moduleId, string name, PermissionGroup valueGroup)
        {
            _entity = new Dal.Netpay.SecurityObject();
            _entity.AppModule_id = moduleId;
            _entity.Name = name;
            ValueGroup = valueGroup;
        }

        public static SecuredObject Create(Module module, string name, PermissionGroup valueGroup)
        {
            var ret = Get(module, name);
            if (ret == null)
            {
                ret = new SecuredObject(module.ID, name, valueGroup);
                ret.Save();
                Cache.Add(ret);
            }
            else if (ret.ValueGroup != valueGroup)
            {
                ret.ValueGroup = valueGroup;
                ret.Save();
            }
            return ret;
        }

        public static List<int> AppModulesNotAdmin
        {
            get
            {
                return (DataContext.Reader.AppModules
                        .Where(x => (x.Name == "MCP.Security") || (x.Name == "PCP.Security") || (x.Name == "MA.Security") || (x.Name == "Reports.Security") || (x.Name == "Reports"))
                        .Select(x => x.AppModule_id)
                        .ToList());
            }
        }

        public static SecuredObject Create(string moduleName, string name, PermissionGroup valueGroup)
        {
            return Create(Module.Get(moduleName), name, valueGroup);
        }

        public static void Remove(Module module)
        {
            Domain.Current.RemoveCachData("Security.Objects");
            DataContext.Writer.SecurityObjects.DeleteAllOnSubmit(DataContext.Writer.SecurityObjects.Where(c => c.AppModule_id == module.ID));
            DataContext.Writer.SubmitChanges();
        }

        public static List<SecuredObject> Cache
        {
            get
            {
                return Domain.Current.GetCachData("Security.Objects", () =>
                {
                    return new Domain.CachData((from g in DataContext.Reader.SecurityObjects select new SecuredObject(g)).ToList(), DateTime.Now.AddHours(24));
                }) as List<SecuredObject>;
            }
        }

        public static List<SecuredObject> AdminSecuredObjects
        {
            get
            {
                return Cache
                    .Where(x => !(AppModulesNotAdmin.Contains(x.ModuleID)) && (x.Name != "LimitedLogin") && (x.Name != "MethodStorage"))
                    .OrderBy(x=>x.ModuleID)
                    .ToList();
            }
        }

        public static SecuredObject Get(string moduleName, string objectName)
        {
            var module = Module.Get(moduleName);
            return Cache.Where(s => s.ModuleID == module.ID && s.Name == objectName).SingleOrDefault();
        }

        public static SecuredObject Get(Module module, string objectName)
        {
            return Cache.Where(s => s.ModuleID == module.ID && s.Name == objectName).SingleOrDefault();
        }

        public static List<SecuredObject> LoadForModule(string moduleName)
        {
            var module = Module.Get(moduleName);
            return Cache.Where(s => s.ModuleID == module.ID).ToList();
        }

        public bool HasPermission(PermissionValue value)
        {
            if (ObjectContext.Current.IsCurrentLoginAdminOfAdmins) return true;
                     
            var user = Login.Current;
            //All users come from Admin have UserRole - Admin , 
            //All users that Come from MCP that should check theire permission have MerchantSubUser role
            //All users that come from PCP have partner role
            //So if the current user is not one of them , return true.
            if (user.Role != UserRole.MerchantSubUser && user.Role != UserRole.Admin && user.Role != UserRole.Partner) return true; 
            var userObjects = user.Items.GetValue<List<Permission>>("Security.ObjectPermissions");
            if (userObjects == null)
            {
                userObjects = Permission.LoadForLogin(user.LoginID);
                user.Items.Add("Security.ObjectPermissions", userObjects);
            }
            var permission = userObjects.Where(o => o.ObjectID == ID).FirstOrDefault();
            if (permission != null) return (permission.Value & value) != 0;
            if (user.Role != UserRole.Admin || AdminUser.Current == null) return false;
            foreach (var g in AdminUser.Current.Groups)
            {
                userObjects = g.Items.GetValue<List<Permission>>("Security.ObjectPermissions");
                if (userObjects == null)
                {
                    userObjects = Permission.LoadForGroup(g.ID);
                    g.Items.Add("Security.ObjectPermissions", userObjects);
                }
                permission = userObjects.Where(o => o.ObjectID == ID).FirstOrDefault();
                if (permission != null) return (permission.Value & value) != 0;
            }
            return false;
        }

        public void Save()
        {
            _entity.Value = ValueToString((PermissionValue)ValueGroup);
            DataContext.Writer.SecurityObjects.Update(_entity, (_entity.SecurityObject_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (_entity.SecurityObject_id == 0) return;
            DataContext.Writer.SecurityObjects.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        private const string SecurityBitsChars = "xrawd";
        public static PermissionValue ParseString(string value, PermissionValue initial = PermissionValue.None)
        {
            foreach (var c in value.ToCharArray())
            {
                //if(c == '+')
                var idx = SecurityBitsChars.IndexOf(c);
                if (idx >= 0) initial |= (PermissionValue)(1 << idx);
            }
            return initial;
        }

        public static string ValueToString(PermissionValue value, bool includeZeros = false)
        {
            string ret = "";
            ret += ((value & PermissionValue.Execute) != PermissionValue.None ? "x" : (includeZeros ? "-x" : ""));
            ret += ((value & PermissionValue.Read) != PermissionValue.None ? "r" : (includeZeros ? "-r" : ""));
            ret += ((value & PermissionValue.Add) != PermissionValue.None ? "a" : (includeZeros ? "-a" : ""));
            ret += ((value & PermissionValue.Edit) != PermissionValue.None ? "w" : (includeZeros ? "-w" : ""));
            ret += ((value & PermissionValue.Delete) != PermissionValue.None ? "d" : (includeZeros ? "-d" : ""));
            return ret;
        }
    }
}
