﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Infrastructure.Security
{
	public class Log : BaseDataObject
	{
		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<DateTime?> InsertDate;
			public int? LoginID;
		}

		private Dal.Netpay.LoginHistory _entity;

		public int ID { get { return _entity.LoginHistory_id; } }
		public int UserId { get { return _entity.LoginAccount_id; } }
		public System.DateTime InsertDate { get { return _entity.InsertDate; } }
		public LoginResult? LoginResult { get { return (LoginResult?)_entity.LoginResult_id; } }
		public string IPAddress { get { return _entity.IPAddress; } }
		public string Text { get { return _entity.VariableChar; } }
		//public System.DateTime? BlockEndTime { get { return _entity.BlockEndTime; } }

		private Log(Dal.Netpay.LoginHistory entity) 
		{
			_entity = entity;
		}

		internal static LoginResult Add(int userId, string ipAddress, LoginResult result)
		{
			var le = new Dal.Netpay.LoginHistory();
			le.LoginAccount_id = userId;
			le.InsertDate = DateTime.Now;
			le.IPAddress = ipAddress;
			le.LoginResult_id = (byte)result;
			DataContext.Writer.LoginHistories.Insert(le);
			DataContext.Writer.SubmitChanges();
			return result;
		}

		internal static void Add(int userId, string ipAddress, string text)
		{
			var le = new Dal.Netpay.LoginHistory();
			le.LoginAccount_id = userId;
			le.InsertDate = DateTime.Now;
			le.IPAddress = ipAddress;
			le.VariableChar = text;
			DataContext.Writer.LoginHistories.Insert(le);
			DataContext.Writer.SubmitChanges();
		}

		public static List<Log> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			var exp = (from l in DataContext.Reader.LoginHistories orderby l.InsertDate descending select l) as IQueryable<Dal.Netpay.LoginHistory>;
			if(filters != null)
			{
				if (filters.ID.From.HasValue) exp = exp.Where(l => l.LoginHistory_id >= filters.ID.From.Value);
				if (filters.ID.To.HasValue) exp = exp.Where(l => l.LoginHistory_id <= filters.ID.To.Value);
				if (filters.InsertDate.From.HasValue) exp = exp.Where(l => l.InsertDate >= filters.InsertDate.From.Value);
				if (filters.InsertDate.To.HasValue) exp = exp.Where(l => l.InsertDate <= filters.InsertDate.To.Value.AlignToEnd());
				if (filters.LoginID.HasValue) exp = exp.Where(l => l.LoginAccount_id == filters.LoginID.Value);
			}
			return exp.ApplySortAndPage(sortAndPage).Select(l => new Log(l)).ToList();
		}
	}
}
