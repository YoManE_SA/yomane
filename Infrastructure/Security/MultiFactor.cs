﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.Security
{
    public class MultiFactor
    {
        private static HashSet<int> _OnProcess = new HashSet<int>();
        public enum Mode { None, Auto, Explicit, SMS, Phone }
        public class Parameters
        {
            public Mode Mode;
            public string PhoneNumber;
            public string AuthCode;
            public string Error;
        }

        public static Parameters ContextParameters
        {
            get {
                var ret = ObjectContext.Current.Items.GetValue<Parameters>("MultiFactor.Parameters");
                if (ret == null) ret = new Parameters() { Mode = Mode.None };
                return ret;
            }
            set { ObjectContext.Current.Items["MultiFactor.Parameters"] = value; }
        }

        private static Dictionary<string, string> SentCodes {
            get {
                return Domain.Current.GetCachData("MultiFactor.SentCodes", () => { return new Domain.CachData(new Dictionary<string, string>(), DateTime.Now.AddMinutes(15)); }) as Dictionary<string, string>;
            }
        }

        public static bool RequireTwoFactor(UserRole role)
        {
            if (!Domain.Current.GetConfig("MultiFactor_Admin", null).ToNullableBool().GetValueOrDefault()) return false;
            return (role == UserRole.Admin) && (ContextParameters.Mode != Mode.None);
        }

        public static bool ValidateMultiFactor(Login login)
        {
            var parameters = ContextParameters;
            if (login.Role != UserRole.Admin)
                throw new Exception("Only AdminUser is allowed for multi-factor authentication");
            if (parameters.Mode == Mode.None) return true;
            var user = AdminUser.LoadForLogin(login.LoginID);
            if (user == null)
                throw new Exception("AdminUser not found for multi-factor authentication, LoginID=" + login.LoginID);
            if (string.IsNullOrEmpty(user.MultiFactorMode)) return true;
            if (parameters.Mode == Mode.None) return true;
            else if (parameters.Mode == Mode.Explicit) {
                switch (user.MultiFactorMode) {
                    case "SMS": parameters.Mode = Mode.SMS; break;
                    case "Phone": parameters.Mode = Mode.Phone; break;
                    default: return true;
                }
                parameters.PhoneNumber = user.NotifyCellPhone;
                return false;
            } else if (parameters.Mode == Mode.Auto) {
                parameters.Mode = user.MultiFactorMode == "SMS" ? Mode.SMS : Mode.Phone;
            }
            if (string.IsNullOrEmpty(parameters.PhoneNumber)) parameters.PhoneNumber = user.NotifyCellPhone;
            if (parameters.Mode == Mode.SMS && !string.IsNullOrEmpty(parameters.AuthCode))
            {
                string sentPass;
                if (!SentCodes.TryGetValue(parameters.PhoneNumber, out sentPass)) return false;
                bool bOk = (parameters.AuthCode == sentPass);
                if (bOk) SentCodes.Remove(parameters.PhoneNumber);
                else parameters.Error = "MF auth wrong pin code";
                return bOk;
            }

            string certFileName = Domain.Current.MapPrivateDataPath("Certificates/Azure-MFA.p12");
            if (!System.IO.File.Exists(certFileName)) throw new Exception("MFA certificate file not found");
            lock (_OnProcess)
            {
                if (_OnProcess.Contains(login.LoginID)) return false;
                _OnProcess.Add(login.LoginID);
            }
            var authParams = new PfAuthParams();
            authParams.Phone = parameters.PhoneNumber;
            authParams.CountryCode = "972";
            authParams.Username = login.UserName;
            authParams.AccountName = user.FullName;
            authParams.Pin = user.ID.ToString();
            authParams.Mode = parameters.Mode == Mode.SMS ? pf_auth.MODE_SMS_ONE_WAY_OTP : pf_auth.MODE_STANDARD;
            authParams.CertFilePath = certFileName;

            string outOtp;
            int callStatus, error;
            var ret = pf_auth.pf_authenticate(authParams, out outOtp, out callStatus, out error);
            _OnProcess.Remove(login.LoginID);
            if (error != 0) parameters.Error = "MF Auth Error Code:" + error;
            if (ret && parameters.Mode == Mode.SMS) {
                SentCodes[parameters.PhoneNumber] = outOtp;
                return false; //wait for user to enter it
            }
            return ret;
        }

    }
}
