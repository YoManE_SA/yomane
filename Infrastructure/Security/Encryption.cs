﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using Netpay.Crypt;
using System.Data.Linq;

namespace Netpay.Infrastructure.Security
{
	public class Encryption
	{
		public static string GetHashKey(int length = 10) 
		{
			string key = "";
			string chars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			Random rnd = new Random((int)DateTime.Now.Ticks);
			for (int index = 0; index < length; index++)
			{
				int random = (int)(rnd.NextDouble() * 36);
				if (random < 1)
					random = 1;
				if (random > 36)
					random = 36;

				key += chars.Substring(random, 1);
			}

			return key;
		}

		public static string GetMd5Hash(string input)
		{
			MD5 md5Hasher = MD5.Create();
			byte[] hashData = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
			string result = System.Convert.ToBase64String(hashData);
			return result;
		}

        public static string GetSHA256Hash(string input)
        {
            SHA256 md5Hasher = SHA256.Create();
            byte[] hashData = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            string result = System.Convert.ToBase64String(hashData);
            return result;
        }


		/// <summary>
		/// Decrypts a byte array.
		/// </summary>
		/// <param name="domainHost"></param>
		/// <param name="source"></param>
		/// <returns></returns>
        /// 

        public static string Decrypt(int keyIndex, System.Data.Linq.Binary source)
        {
            if (source == null) return null;
            if (source.Length == 0) return string.Empty;
            return Decrypt(keyIndex, source.ToArray());
        }

        public static string Decrypt(int keyIndex, byte[] source)
        {
            if (source == null) return null;
            if (source.Length == 0) return string.Empty;
            return Netpay.Crypt.SymEncryption.GetKey(keyIndex).Decrypt(new Blob(source)).Text;
        }

        public static string Decrypt(byte[] source)
        {
            return Decrypt(Domain.Current.EncryptionKeyNumber, source);
        }

        public static string Decrypt(string domainHost, byte[] source)
		{
			return Decrypt(Domain.Get(domainHost).EncryptionKeyNumber, source);
		}

        public static string DecryptHex(int keyIndex, string source)
        {
            if (source == null) return null;
            if (source.Length == 0) return string.Empty;
            return Netpay.Crypt.SymEncryption.GetKey(keyIndex).Decrypt(new Blob() { Hex = source }).Text;
        }

        public static string DecryptHex(string source)
        {
            return DecryptHex(Domain.Current.EncryptionKeyNumber, source);
        }

        public static byte[] Encrypt(int keyIndex, string source)
        {
            if (source == null) return null;
            return Netpay.Crypt.SymEncryption.GetKey(keyIndex).Encrypt(new Blob(source)).Bytes;
        }

        public static byte[] Encrypt(int keyIndex, byte[] source)
        {
            if (source == null) return null;
            if (source.Length == 0) return new byte[0];
            return Netpay.Crypt.SymEncryption.GetKey(keyIndex).Encrypt(new Blob(source)).Bytes;
        }

        public static byte[] Encrypt(string source)
		{
            return Encrypt(Domain.Current.EncryptionKeyNumber, source);
		}

        public static string EncryptHex(string source)
        {
            if (source == null) return null;
            if (source.Length == 0) return string.Empty;
            return Netpay.Crypt.SymEncryption.GetKey(Domain.Current.EncryptionKeyNumber).Encrypt(new Blob(source)).Hex;
        }

        private const string enrcyptionCodeWord = "blackhorse";
        public static string DecodeCvv(string cvv)
		{
            if (cvv == null) return null;
            if (cvv.Length == 0) return string.Empty;
            char temp;
			string result = "";
			for (var cvvIdx = 3; cvvIdx < cvv.Length; cvvIdx++)
			{
				temp = cvv[cvvIdx];
				for (var codeWordIdx = 0; codeWordIdx < enrcyptionCodeWord.Length; codeWordIdx++)
				{
					if (enrcyptionCodeWord[codeWordIdx] == temp)
					{
						result += codeWordIdx.ToString();
						break;
					}
				}
			}
			return result;
		}

        public static string EncodeCvv(string cvv)
        {
            if (cvv == null) return null;
            //if (cvv.Length == 0) return string.Empty;
            Random rnd = new Random();
            string encodedValue = "";
            for(int i = 0; i < 3; i++) cvv = rnd.Next(10) + cvv;
            for(int i = 0; i < cvv.Length; i++){
		        if (cvv[i] >= '0' && cvv[i] <= '9')
                    encodedValue += enrcyptionCodeWord.Substring(cvv[i] - '0', 1);
            }
            return encodedValue;
        }
        
	}
}