﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.Security
{
	public class SecurityModule : Infrastructure.Module
	{        
		public const string ModuleName = "Security";

        public const string ModuleManagerSecuredObjectName = "ModuleManager";

        public static SecuredObject ModuleManagerSecuredObject { get { return SecuredObject.Get(Current, ModuleManagerSecuredObjectName); } }

		public override string Name { get { return ModuleName; } }
		public override decimal Version { get { return 1.0m; } }
		public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }
		
		protected override void OnInstall(EventArgs e)
		{
            Infrastructure.Security.SecuredObject.Create(this, AdminUser.SecuredObjectName, PermissionGroup.ReadEditDelete);
            Infrastructure.Security.SecuredObject.Create(this, Group.SecuredObjectName, PermissionGroup.ReadEditDelete);
            Infrastructure.Security.SecuredObject.Create(this, Login.SecuredObjectName, PermissionGroup.ReadEditDelete);
            Infrastructure.Security.SecuredObject.Create(this, ModuleManagerSecuredObjectName, PermissionGroup.ReadEditDelete);
            base.OnInstall(e);
		}

        protected override void OnUninstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Remove(this);
            base.OnUninstall(e);
        }

        protected override void OnActivate(EventArgs e)
		{
			Login.EnableLogOffTimer = true;
			base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{
			Login.EnableLogOffTimer = false;
			base.OnDeactivate(e);
		}
	}
}
