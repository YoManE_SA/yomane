﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.Security
{
	public class Group : BaseDataObject
	{
        public const string SecuredObjectName = "Groups";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return SecuredObject.Get(SecurityModule.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public bool? IsActive;
        }

		private Dal.Netpay.AdminGroup _entity;
		public int ID { get { return _entity.AdminGroup_id; } }
		public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
		public string Description { get { return _entity.Description; } set { _entity.Description = value; } }
		public bool IsActive { get { return _entity.IsActive; } set { _entity.IsActive = value; } }
		public Dictionary<string, object> Items { get; private set; }

		private Group(Dal.Netpay.AdminGroup entity) 
		{            
            _entity = entity;
			Items = new Dictionary<string, object>();
		}

		public Group(string name) 
		{            
            _entity = new Dal.Netpay.AdminGroup();
			_entity.Name = name;
		}

		public static List<Group> Cache
		{
			get {
                //Permission check causes stack over flow exception
                                                       
                return  Domain.Current.GetCachData("Security.Roles", () => {
                    return new Domain.CachData((from g in DataContext.Reader.AdminGroups select new Group(g)).ToList(), DateTime.Now.AddHours(6));            
                }) as List<Group>;
			}
		}

		public static Group Get(int id)
		{
			return Cache.Where(g => g.ID == id).SingleOrDefault();
		}

		public void Save()
		{
            if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins) throw new MethodAccessDeniedException();

            DataContext.Writer.AdminGroups.Update(_entity, (_entity.AdminGroup_id != 0));
			DataContext.Writer.SubmitChanges();
			Domain.Current.RemoveCachData("Security.Roles");
		}

		public void Delete()
		{
            if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins) throw new MethodAccessDeniedException();

            if (_entity.AdminGroup_id != 0) DataContext.Writer.AdminGroups.Delete(_entity);
			DataContext.Writer.SubmitChanges();
			Domain.Current.RemoveCachData("Security.Roles");
		}

		public static HashSet<Group> LoadForAdminUser(AdminUser user)
		{
            // Permission check causes stack over flow exception.

            var sr = (from s in DataContext.Reader.AdminUserToAdminGroups where s.AdminUser_id == user.ID select (int)s.AdminGroup_id).ToList();
			return new HashSet<Group>(Cache.Where(g => sr.Contains(g.ID)).Distinct());
		}

		public static void SaveForAdminUser(AdminUser user, HashSet<Group> set)
		{
            if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins) throw new MethodAccessDeniedException();

            var existing = DataContext.Writer.AdminUserToAdminGroups.Where(ug => ug.AdminUser_id == user.ID).ToList();
			DataContext.Writer.AdminUserToAdminGroups.DeleteAllOnSubmit(existing.Where(ug => !set.Select(x => x.ID).Contains(ug.AdminGroup_id)));
			var newItems = set.Where(ug => !existing.Select(x => x.AdminGroup_id).Contains((short)ug.ID));
			foreach(var n in newItems)
				DataContext.Writer.AdminUserToAdminGroups.InsertOnSubmit(new Dal.Netpay.AdminUserToAdminGroup() { AdminGroup_id = (short)n.ID, AdminUser_id = user.ID } );
			DataContext.Writer.SubmitChanges();
		}

        public static List<Group> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from g in DataContext.Reader.AdminGroups select g);

            if (filters != null)
            {
                if (filters.IsActive.HasValue) exp = exp.Where((g) => g.IsActive == filters.IsActive.Value);
            }

            return exp.ApplySortAndPage(sortAndPage).Select(g => new Group(g)).ToList();
        }

        public static Group Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from g in DataContext.Reader.AdminGroups where g.AdminGroup_id == id select new Group(g)).SingleOrDefault();
        }

        public static PermissionValue? GetAdminUserPermissionsForSecuredObjectByGroups(int loginId , int securedObjectId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            AdminUser currentUser = AdminUser.LoadForLogin(loginId);

            //No adminuser found.
            if (currentUser == null) return null;

            //No groups issued with the adminuser found.
            if (currentUser.Groups.Count == 0) return null;

            List<PermissionValue> GroupsPermissions = new List<PermissionValue>();
            foreach(var group in currentUser.Groups)
            {
                Permission currentGroupPermission = Permission.LoadForGroup(group.ID).Where(x => x.ObjectID == securedObjectId).SingleOrDefault();
                if (currentGroupPermission != null)
                {
                    GroupsPermissions.Add(currentGroupPermission.Value);
                }
            }

            //No permission value for the required secured object was found
            if (GroupsPermissions.Count == 0) return null;

            var finalValue = GroupsPermissions.Aggregate((sum, item) => sum |= item);

            return finalValue;
            //Combine all groups permissions to one PermissionValue object
            /*
            string[] permissionsArray = GroupsPermissions.Select(z => (SecuredObject.ValueToString(z))).ToArray();
            char[] allGroupsPermissionValueAsCharArray = string.Join("", permissionsArray).ToCharArray().Distinct().ToArray();
            string finalStringValue = string.Join("", allGroupsPermissionValueAsCharArray);
            PermissionValue finalValue = SecuredObject.ParseString(finalStringValue);
            */
        }
	}
}
