﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.Security
{
    public class Permission
    {
        public int ObjectID { get; set; }
        public PermissionValue Value { get; set; }

        public static List<Permission> LoadForLogin(int loginId)
        {
            return (from p in DataContext.Reader.SecurityObjectToLoginAccounts where p.LoginAccount_id == loginId select p).AsEnumerable().Select(p => new Permission() { ObjectID = p.SecurityObject_id, Value = SecuredObject.ParseString(p.Value) }).ToList();
        }

        public static List<Permission> LoadForGroup(int groupId)
        {
            return (from p in DataContext.Reader.SecurityObjectToAdminGroups where p.AdminGroup_id == groupId select p).AsEnumerable().Select(p => new Permission() { ObjectID = p.SecurityObject_id, Value = SecuredObject.ParseString(p.Value) }).ToList();
        }


        public static void SetToLogin(int loginId, Dictionary<int, PermissionValue?> values)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins)
                {
                    throw new MethodAccessDeniedException();
                }
            }

            DataContext.Writer.SecurityObjectToLoginAccounts.DeleteAllOnSubmit(DataContext.Writer.SecurityObjectToLoginAccounts.Where(s => s.LoginAccount_id == loginId && values.Where(v => (v.Value == null)).Select(v => v.Key).Contains(s.SecurityObject_id)));
            DataContext.Writer.SubmitChanges();
            var existing = DataContext.Writer.SecurityObjectToLoginAccounts.Where(s => s.LoginAccount_id == loginId && values.Keys.Contains(s.SecurityObject_id)).ToList();
            foreach (var v in values.Where(x => x.Value != null))
            {
                var item = existing.Where(e => e.SecurityObject_id == v.Key).SingleOrDefault();
                bool isNew = (item == null);
                if (isNew) item = new Dal.Netpay.SecurityObjectToLoginAccount() { SecurityObject_id = v.Key, LoginAccount_id = loginId };
                item.Value = SecuredObject.ValueToString(v.Value.Value);
                DataContext.Writer.SecurityObjectToLoginAccounts.Update(item, !isNew);
            }
            DataContext.Writer.SubmitChanges();
        }

        public static void SetToLoginAffiliate(int loginId, Dictionary<int, PermissionValue?> values)
        {          
            DataContext.Writer.SecurityObjectToLoginAccounts.DeleteAllOnSubmit(DataContext.Writer.SecurityObjectToLoginAccounts.Where(s => s.LoginAccount_id == loginId && values.Where(v => (v.Value == null)).Select(v => v.Key).Contains(s.SecurityObject_id)));
            DataContext.Writer.SubmitChanges();
            var existing = DataContext.Writer.SecurityObjectToLoginAccounts.Where(s => s.LoginAccount_id == loginId && values.Keys.Contains(s.SecurityObject_id)).ToList();
            foreach (var v in values.Where(x => x.Value != null))
            {
                var item = existing.Where(e => e.SecurityObject_id == v.Key).SingleOrDefault();
                bool isNew = (item == null);
                if (isNew) item = new Dal.Netpay.SecurityObjectToLoginAccount() { SecurityObject_id = v.Key, LoginAccount_id = loginId };
                item.Value = SecuredObject.ValueToString(v.Value.Value);
                DataContext.Writer.SecurityObjectToLoginAccounts.Update(item, !isNew);
            }
            DataContext.Writer.SubmitChanges();
        }

        public static void SetToGroup(int groupId, Dictionary<int, PermissionValue?> values)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins) throw new MethodAccessDeniedException();
            }

            DataContext.Writer.SecurityObjectToAdminGroups.DeleteAllOnSubmit(DataContext.Writer.SecurityObjectToAdminGroups.Where(s => s.AdminGroup_id == groupId && values.Where(v => v.Value == null).Select(v => v.Key).Contains(s.SecurityObject_id)));
            DataContext.Writer.SubmitChanges();
            var existing = DataContext.Writer.SecurityObjectToAdminGroups.Where(s => s.AdminGroup_id == groupId && values.Keys.Contains(s.SecurityObject_id)).ToList();
            foreach (var v in values.Where(x => x.Value != null))
            {
                var item = existing.Where(e => e.SecurityObject_id == v.Key).SingleOrDefault();
                bool isNew = (item == null);
                if (isNew) item = new Dal.Netpay.SecurityObjectToAdminGroup() { SecurityObject_id = v.Key, AdminGroup_id = (short)groupId };
                item.Value = SecuredObject.ValueToString(v.Value.Value);
                DataContext.Writer.SecurityObjectToAdminGroups.Update(item, !isNew);
            }
            DataContext.Writer.SubmitChanges();
        }

        public static bool IsAnySecuredObjectForLogin(int loginId, int objectId)
        {
            return (DataContext.Reader.SecurityObjectToLoginAccounts).Any(x => x.LoginAccount_id == loginId && x.SecurityObject_id == objectId);
        }

        public static void DeleteUserObjects(List<int> ids)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins) throw new MethodAccessDeniedException();
            }

            DataContext.Writer.SecurityObjectToLoginAccounts.DeleteAllOnSubmit(DataContext.Writer.SecurityObjectToLoginAccounts.Where(x => ids.Contains(x.SecurityObject_id)));
            DataContext.Writer.SubmitChanges();
        }
    }
}
