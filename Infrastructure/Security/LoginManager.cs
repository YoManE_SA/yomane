﻿using System;
using System.Linq;
using System.Data.Linq;
using System.Collections.Generic;
using StackExchange.Redis;
using System.Runtime.Serialization.Json;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace Netpay.Infrastructure.Security
{
    public abstract class LoginManager
    {
        public class LoginData
        {
            public Guid CredToken;
            public int LoginID;
            public string DeviceId;
            public DateTime? PrevLogin;
            public string Cookie;
        }

        public abstract LoginData GetLogin(Guid credToken);
        public abstract void RemoveLogin(Guid credToken);
        public abstract void AddLogin(LoginData data);

        public abstract void SetLoginData(Guid credToken, string key, string value);
        public abstract string GetLoginData(Guid credToken, string key);

        public static string ConnectionString { get { return Domain.Current.GetConfig("LoginManager_ConnectionString"); } }
        public string LoginDataCookie(LoginData data) { return Infrastructure.Security.Encryption.GetSHA256Hash(string.Format("LM_Cookie_{0}_{1}_{2}", data.CredToken, data.LoginID, Domain.Current.BrandName)); }
        protected virtual bool ValidateCookie(LoginData data) { return LoginDataCookie(data) == data.Cookie; }

        protected DataContractJsonSerializer Serializer { get { return new DataContractJsonSerializer(typeof(LoginData)); } }

        protected LoginData DeserializeLoginData(string data) {
            if (data == null) return null;
            var ret = Serializer.ReadObject(new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(data))) as LoginData;
            if (ret != null && !ValidateCookie(ret)) {
                Logger.Log(LogSeverity.Warning, LogTag.Security, "LoginManager fetch data but security cookie was incorrect, possible of session data tempred.");
                ret = null;
            }
            return ret;
        }

        protected string SerializeLoginData(LoginData data)
        {
            if (data.Cookie == null) data.Cookie = LoginDataCookie(data);
            using (var ms = new System.IO.MemoryStream()) {
                Serializer.WriteObject(ms, data);
                ms.Position = 0;
                using (var sr = new System.IO.StreamReader(ms))
                    return sr.ReadToEnd();
            }
        }

        public class RedisManager : LoginManager
        {
            private static Lazy<ConnectionMultiplexer> Connection = new Lazy<ConnectionMultiplexer>(() =>
            {
                string connectionString = ConnectionString;
                if (string.IsNullOrEmpty(connectionString)) Logger.Log(string.Format("LoginManager_ConnectionString for redis not found on domain '{0}' config.", Domain.Current.Host));

                var RedisConfig = ConfigurationOptions.Parse(connectionString);
                RedisConfig.SyncTimeout = 3000;
                return ConnectionMultiplexer.Connect(RedisConfig);
            });

            public static IDatabase Database { get { return Connection.Value.GetDatabase(); } }
            private string GetKey(Guid credToken) { return string.Format("S_" + credToken.ToString()); }

            public override string GetLoginData(Guid credToken, string key)
            {
                return Database.HashGet(GetKey(credToken), key);
            }

            public override void SetLoginData(Guid credToken, string key, string value)
            {
                Database.HashSet(GetKey(credToken), key, value);
            }

            public override void RemoveLogin(Guid credToken)
            {
                Database.KeyDelete(GetKey(credToken));
            }

            public override void AddLogin(LoginData data)
            {
                SetLoginData(data.CredToken, "", SerializeLoginData(data));
                var expRet = Database.KeyExpire(GetKey(data.CredToken), DateTime.Now.AddMinutes(Application.SessionTimeout));
            }

            public override LoginData GetLogin(Guid credToken)
            {
                var ret = DeserializeLoginData(GetLoginData(credToken, ""));
                if (ret != null) Database.KeyExpire(GetKey(credToken), DateTime.Now.AddMinutes(Application.SessionTimeout));
                return ret;
            }
        }

        public class SqlManager : LoginManager
        {
            private const string SQL_SELECT = "Session_Get"; 
            private const string SQL_SAVE = "Session_Save"; 
            private const string SQL_DELETE = "Session_Delete"; 


            //private const string SQL_SELECT = "Select Value From [Session] Where CredToken={0} And Key={1}";
            //private const string SQL_INSERT = "Insert Into [Session] (CredToken, Key, Value) Values ({0}, {1}, {2})"; 
            //private const string SQL_UPDATE = "Update [Session] Set Value={2} Where CredToken={0} And Key={1}";
            //private const string SQL_DELETE = "Delete From [Session] Where CredToken={0} And Key={1}";
            //private const string SQL_DELETEALL = "Delete From [Session] Where CredToken={0}";

            protected override bool ValidateCookie(LoginData data) { return true; }

            private System.Data.SqlClient.SqlConnection CreateConnection()
            {
                string connectionString = ConnectionString;
                if (string.IsNullOrEmpty(connectionString)) Logger.Log(string.Format("LoginManager_ConnectionString for sql not found on domain '{0}' config.", Domain.Current.Host));
                var ret = new System.Data.SqlClient.SqlConnection(connectionString);
                ret.Open();
                return ret;
            }

            public override string GetLoginData(Guid credToken, string key)
            {
                using (var con = CreateConnection())
                {
                    try {
                        using (var cmd = new System.Data.SqlClient.SqlCommand(SQL_SELECT, con)) {

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@CredToken", SqlDbType.UniqueIdentifier).Value = credToken;
                            cmd.Parameters.Add("@Key", SqlDbType.VarChar).Value = key;

                            //cmd.Parameters.Add(credToken.ToString());
                            //cmd.Parameters.Add(key);

                            var ret = cmd.ExecuteScalar();
                            if (ret  == DBNull.Value || ret == null) return null;
                            return (string)ret;
                        }
                    } finally {
                        con.Close();
                    }
                }
            }
            /// <summary>
            /// updates the credential token to the database. if one does not exist, a new one is inserted.
            /// </summary>
            /// <param name="credToken">Credential Token</param>
            /// <param name="key">Key</param>
            /// <param name="value">Value</param>
            public override void SetLoginData(Guid credToken, string key, string value)
            {
                using (var con = CreateConnection())
                {
                    try {
                        using (var cmd = new System.Data.SqlClient.SqlCommand(SQL_SAVE, con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@CredToken", SqlDbType.UniqueIdentifier).Value = credToken;
                            cmd.Parameters.Add("@Key", SqlDbType.VarChar).Value = key;
                            cmd.Parameters.Add("@Value", SqlDbType.VarChar).Value = value;
                            var affectedRows = cmd.ExecuteNonQuery();

                            //cmd.Parameters.Add(credToken.ToString());
                            //cmd.Parameters.Add(key);
                            //cmd.Parameters.Add(value);
                            //if (cmd.ExecuteNonQuery() == 0) {
                            //    cmd.CommandText = SQL_INSERT;
                            //    cmd.ExecuteNonQuery();
                            //}

                            if (affectedRows > 0 && HttpContext.Current != null && HttpContext.Current.Session != null)
                            {
                                HttpContext.Current.Session.Timeout = Application.SessionTimeout;
                            }
                        }
                    } finally {
                        con.Close();
                    }
                }
            }

           

            public override void RemoveLogin(Guid credToken )
            {
                using (var con = CreateConnection()) {
                    try {
                        using (var cmd = new System.Data.SqlClient.SqlCommand(SQL_DELETE, con)) {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@CredToken", SqlDbType.UniqueIdentifier).Value = credToken;
                            //cmd.Parameters.Add("@Key", SqlDbType.VarChar).Value = key;
                            //   cmd.Parameters.Add(credToken.ToString());

                            cmd.ExecuteNonQuery();
                        }
                    } finally {
                        con.Close();
                    }
                }
            }

            public override LoginData GetLogin(Guid credToken)
            {
                return DeserializeLoginData(GetLoginData(credToken, ""));
            }

            public override void AddLogin(LoginData data)
            {
                SetLoginData(data.CredToken, "", SerializeLoginData(data));
            }
        }


    }
}
