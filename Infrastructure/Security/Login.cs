﻿using System;
using System.Linq;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Collections.Generic;

namespace Netpay.Infrastructure.Security
{


    public enum UserRole
    {
        System = 0,
        Admin = 5,
        Account = 10,
        Customer = 15,
        Merchant = 20,
        MerchantSubUser = 21,
        Partner = 25,
        DebitCompany = 30,
        //Unknown = 0,
    }

    public enum AccountTypeForLogin
    {
        Merchant = 1,
        Customer = 2,
        Affiliate = 3,
        DebitCompany = 4,
    }

    public enum LoginResult
    {
        Success = 0,
        UserNotFound = 2,
        WrongPassword = 3,
        AbsenceBlocked = 4,
        FailedAttemptsBlocked = 5,
        GeneralFailure = 6,
        FailedSignatureBlocked = 7,
        AdminBlocked = 8,
        AdminClosed = 9,
        ForcePasswordChange = 10,
        MultiFactorFailed = 11
    }

    //[Table(Name = "Data.LoginAccount")]
    public class Login : BaseDataObject
    {
        public static UserRole[] RolesAllowNewPassword = { UserRole.Customer, UserRole.Merchant, UserRole.Partner, UserRole.Admin };

        public enum ChangePasswordResult
        {
            PasswordConfirmationMissmatch = 1,
            PasswordAlreadyExists = 2,
            Success = 3,
            WeakPassword = 4,
            PasswordUsedInThePast = 5,
            WrongCurrentPassword = 6
        }
        private Guid _credentialsToken;
        private DateTime _lastFetch, _lastValidate;
        private DateTime? _lastLogin = null;
        private bool _isNew;
        private Dal.Netpay.LoginAccount _loginEntity;

        [Column(Name = "LoginAccount_id", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true)]
        public int LoginID { get { return _loginEntity.LoginAccount_id; } }

        [Column(Name = "LoginRole_id", DbType = "TinyInt NOT NULL")]
        public UserRole Role { get { return (UserRole)_loginEntity.LoginRole_id; } }

        [Column(Name = "LoginUser", DbType = "VarChar(30)")]
        public string UserName { get { return _loginEntity.LoginUser; } set { _loginEntity.LoginUser = value; } }

        [Column(Name = "LoginEmail", DbType = "VarChar(50)")]
        public string EmailAddress { get { return _loginEntity.LoginEmail; } set { _loginEntity.LoginEmail = value; } }

        public byte? LoginFailCount { get { return _loginEntity.FailCount; } }

        [Column(Name = "LastFailTime", DbType = "DateTime2(2)")]
        public System.DateTime? LastFail { get { return _loginEntity.LastFailTime; } }

        [Column(Name = "LastSuccessTime", DbType = "DateTime2(2)")]
        public System.DateTime? LoginTime { get { return _loginEntity.LastSuccessTime; } }

        [Column(Name = "BlockEndTime", DbType = "DateTime2(2)")]
        public System.DateTime? BlockEndTime { get { return _loginEntity.BlockEndTime; } }

        [Column(DbType = "Bit NOT NULL")]
        public bool IsActive { get { return _loginEntity.IsActive; } set { _loginEntity.IsActive = value; } }

        //[Association(Name = "FK_Account_LoginAccount_LoginAccountID", Storage = "_data_Accounts", ThisKey = "LoginAccount_id", OtherKey = "LoginAccount_id", DeleteRule = "NO ACTION")]
        //[Association(Name = "FK_AccountSubUser_LoginAccount_LoginAccountID", Storage = "_data_AccountSubUsers", ThisKey = "LoginAccount_id", OtherKey = "LoginAccount_id", DeleteRule = "CASCADE")]
        //[Association(Name="FK_LoginAccount_LoginRole_LoginRoleID", Storage="_list_LoginRole", ThisKey="LoginRole_id", OtherKey="LoginRole_id", IsForeignKey=true)]
        //[Association(Name="FK_LoginPassword_LoginAccountID", Storage="_data_LoginPasswords", ThisKey="LoginAccount_id", OtherKey="LoginAccount_id", DeleteRule="CASCADE")]
        //[Association(Name = "FK_LoginHistory_LoginAccountID", Storage = "_log_LoginHistories", ThisKey = "LoginAccount_id", OtherKey = "LoginAccount_id", DeleteRule = "CASCADE")]
        //[Association(Name="FK_AdminUser_LoginAccount_LoginAccountID", Storage="_system_AdminUsers", ThisKey="LoginAccount_id", OtherKey="LoginAccount_id", DeleteRule="NO ACTION")]
        //[Association(Name="FK_SecurityObjectToLoginAccount_LoginAccount_LoginAccountID", Storage="_system_SecurityObjectToLoginAccounts", ThisKey="LoginAccount_id", OtherKey="LoginAccount_id", DeleteRule="CASCADE")]


        public Dictionary<string, object> Items { get; private set; }
        public System.DateTime? LastLogin { get { return _lastLogin; } }
        public string LoggedInDevice { get; private set; }

        public const string SecuredObjectName = "Login";
        public static SecuredObject SecuredObject { get { return SecuredObject.Get(SecurityModule.Current, SecuredObjectName); } }

        public static event EventHandler LoggedIn;

        protected Login(Dal.Netpay.LoginAccount entity)
        {
            _loginEntity = entity;
            _lastLogin = entity.LastSuccessTime;
            Items = new Dictionary<string, object>();
        }

        public Login(UserRole role)
        {
            _loginEntity = new Dal.Netpay.LoginAccount() { LoginRole_id = (byte)role, IsActive = true };
            _isNew = true;
        }

        public bool IsFirstLogin
        {
            get
            {
                return !_loginEntity.LastSuccessTime.HasValue;
            }
        }

        public Table<Login> GetTable(DataContext dc)
        {
            return dc.GetTable<Login>();
        }


        private static LoginManager DomainManager
        {
            get
            {
                LoginManager lm = null;
                if (Domain.Current != null)
                {

                    lm = Domain.Current.GetItem<LoginManager>("Login.LoginManager", () =>
                {
                    string managerType = Domain.Current.GetConfig("LoginManager", "").NullIfEmpty();
                    if (managerType == null) return null;
                    if (managerType.ToLower() == "none") return null;
                    if (managerType == "RedisManager") return new LoginManager.RedisManager();
                    else if (managerType == "SqlManager") return new LoginManager.SqlManager();
                    throw new Exception("LoginManager settings not correct for domain: can be empty or RedisManager or SqlManager");
                });
                }
                return lm;
            }
        }

        private static Dictionary<Guid, Login> LoggedUsers
        {
            get
            {
                if (Domain.Current == null) return null;
                return Domain.Current.GetItem("Login.LoggedUsers", () => new Dictionary<Guid, Login>());
            }
        }

        public T GetItem<T>(string key, Func<T> initProc = null)
        {
            object ret = null;
            lock (Items)
            {
                if (Items.TryGetValue(key, out ret)) return (T)ret;
                if (initProc == null) return default(T);
                if ((ret = initProc()) == null) return default(T);
                Items.Add(key, ret);
            }
            return (T)ret;
        }

        public void SetItem<T>(string key, T value)
        {
            lock (Items)
            {
                if (value == null)
                {
                    if (Items.ContainsKey(key)) Items.Remove(key);
                }
                else Items[key] = value;
            }
        }


        public string GetSessionItem(string key)
        {
            var domainManager = DomainManager;
            if (domainManager != null) return domainManager.GetLoginData(_credentialsToken, key);
            if (!Items.ContainsKey(key)) return null;
            return (string)Items[key];
        }

        public void SetSessionItem(string key, string value)
        {
            var domainManager = DomainManager;
            if (domainManager != null) domainManager.SetLoginData(_credentialsToken, key, value);
            else
            {
                if (value == null)
                {
                    if (Items.ContainsKey(key)) Items.Remove(key);
                }
                else Items[key] = value;
            }
        }

        public enum TokenStatus
        {
            NotExist, ExistLocally, NotExistRemotly, CantDuplicate, ValidationFaild
        }

        public static Login Get(Guid credentialsToken, bool throwError = true)
        {
            TokenStatus status;
            var ret = Get(credentialsToken, out status);
            if (ret == null && throwError) throw new Infrastructure.NotLoggedinException();
            return ret;
        }

        public static Login Get(Guid credentialsToken, out TokenStatus status)
        {
            Login ret;
            var domainManager = DomainManager;
            var loggedUsers = LoggedUsers;
            status = TokenStatus.NotExist;
            lock (((System.Collections.ICollection)loggedUsers).SyncRoot)
            {
                if (!loggedUsers.TryGetValue(credentialsToken, out ret))
                {
                    LoginManager.LoginData lret = null;
                    if (domainManager != null)
                    {
                        lret = domainManager.GetLogin(credentialsToken);
                        if (lret == null) status = TokenStatus.NotExistRemotly;
                    }
                    if (lret == null) return null;
                    ret = (from n in DataContext.Reader.LoginAccounts where n.LoginAccount_id == lret.LoginID select new Login(n)).SingleOrDefault();
                    if (ret != null)
                    {
                        ret._credentialsToken = lret.CredToken;
                        ret._lastLogin = lret.PrevLogin;
                        ret._lastValidate = DateTime.Now;
                        ret.LoggedInDevice = lret.DeviceId;
                        loggedUsers.Add(credentialsToken, ret);
                    }
                    else status = TokenStatus.CantDuplicate;
                }
                else
                {
                    status = TokenStatus.ExistLocally;
                    if (domainManager != null && (credentialsToken != Domain.Current.ServiceCredentials))
                    {
                        if (ret._lastValidate < DateTime.Now.AddMinutes(-1))
                        {
                            var lret = domainManager.GetLogin(credentialsToken);
                            if (lret == null || lret.LoginID != ret.LoginID)
                            {
                                loggedUsers.Remove(credentialsToken);
                                ret = null;
                                status = TokenStatus.ValidationFaild;
                            }
                            else ret._lastValidate = DateTime.Now;
                        }
                    }
                }
                if (ret != null) ret._lastFetch = DateTime.Now;
            }
            return ret;
        }

        public static Login LoadLogin(int id)
        {
            if (Login.Current != null && Login.Current.LoginID != id)
                ObjectContext.Current.IsUserOfType(null, SecuredObject, PermissionValue.Read);
            return (from u in DataContext.Reader.LoginAccounts where u.LoginAccount_id == id select new Login(u)).SingleOrDefault();
        }

        private static string GetActivePassword(int Id, out DateTime? insertDate)
        {
            insertDate = null;
            var activePassword = (from p in DataContext.Reader.LoginPasswords where p.LoginAccount_id == Id orderby p.LoginPassword_id descending select new { p.PasswordEncrypted, p.EncryptionKey, p.InsertDate }).FirstOrDefault();
            if (activePassword == null) return null;
            insertDate = activePassword.InsertDate;
            return Encryption.Decrypt(activePassword.EncryptionKey, activePassword.PasswordEncrypted.ToArray());
        }

        private static string GetActivePassword(int Id) { DateTime? retValue; return GetActivePassword(Id, out retValue); }

        protected virtual bool VerifySecurity(bool throwError = true)
        {
            if (_isNew) return true;
            if (Login.Current == null)
            {
                if (_loginEntity.LoginAccount_id != 0)
                {
                    if (throwError) throw new MethodAccessDeniedException();
                    return false;
                }
            }
            if (Login.Current.LoginID != _loginEntity.LoginAccount_id)
                return ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Edit, throwError);
            return true;
        }

        public ChangePasswordResult SetPassword(string newPassword, string clientIP, bool isLoginInfoAlreadyVerified = false, bool isForgotPasswordFlow = false)
        {
            if (Current != null && Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            //The isLoginInfoAlreadyVerified variable can be true in 2 cases,
            //In case that the user was asked to change his password after 3 months and
            //After 3 months there is no need for verification cause the
            //Credentials where already checked in the DoLogin method
            //And the other case is that Admin user is reseting a user's password,
            //And there is already a security check in the beginning of the current method
            if (!isLoginInfoAlreadyVerified) VerifySecurity();
            if (!CheckPasswordStrength(newPassword)) return ChangePasswordResult.WeakPassword;
            var entity = new Dal.Netpay.LoginPassword();
            entity.InsertDate = DateTime.Now;
            entity.LoginAccount_id = _loginEntity.LoginAccount_id;
            entity.EncryptionKey = (byte)Domain.Current.EncryptionKeyNumber;
            entity.PasswordEncrypted = Encryption.Encrypt(entity.EncryptionKey, newPassword);
            if (DataContext.Reader.LoginPasswords.Where(p => p.LoginAccount_id == _loginEntity.LoginAccount_id && p.PasswordEncrypted == entity.PasswordEncrypted).Any())
                return ChangePasswordResult.PasswordUsedInThePast;
            DataContext.Writer.LoginPasswords.InsertOnSubmit(entity);
            DataContext.Writer.SubmitChanges();
            DataContext.Writer.LoginPasswords.DeleteAllOnSubmit(
                (from p in DataContext.Writer.LoginPasswords where p.LoginAccount_id == _loginEntity.LoginAccount_id orderby p.LoginPassword_id descending select p).Skip(3));
            //reset lock
            //If the current flow is ForgotPasswordFlow then the LastSuccessTime should be null,
            //Beacuse then the next login of the user will be considered as the first login and
            //That user will be asked to change his password.
            //That is a pci requirement - the user must change his password after it was reset.
            _loginEntity.LastSuccessTime = isForgotPasswordFlow ? null : DateTime.Now.ToNullableDate();
            _loginEntity.FailCount = 0;
            DataContext.Writer.LoginAccounts.Update(_loginEntity, true);
            DataContext.Writer.SubmitChanges();
            return ChangePasswordResult.Success;
        }

        public static ChangePasswordResult SetPassword(int userId, string newPassword, string clientIP, bool isLoginInfoAlreadyVerified = false)
        {
            if (Current != null && Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            var login = LoadLogin(userId);
            return login.SetPassword(newPassword, clientIP, isLoginInfoAlreadyVerified);
        }

        public static bool ValidatePassword(int loginId, string password, string clientIP)
        {
            var storedPassword = GetActivePassword(loginId);
            if (storedPassword == null) return false;
            return storedPassword == password;
        }

        public static string GetPassword(int loginId, string clientIP)
        {
            if (Login.Current == null) throw new MethodAccessException("current login is null");
            if (Login.Current.LoginID != loginId)
            {
                if (!(IsSubAccount(Login.Current.LoginID, loginId)))
                {
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
                }
            }
            var storedPassword = GetActivePassword(loginId);
            if (storedPassword != null && (Login.Current == null || Login.Current.LoginID != loginId) && (ObjectContext.Current.CredentialsToken != Domain.Current.ServiceCredentials))
                Log.Add(loginId, clientIP, "Get Password");
            return storedPassword;
        }

        public static bool IsSubAccount(int fatherCustomerIdLoginId, int ChildCustomerLoginId)
        {
            int? fatherCustomerId = GetCustomerIdByLoginId(fatherCustomerIdLoginId);
            int? childCustomerId = GetCustomerIdByLoginId(ChildCustomerLoginId);

            if (fatherCustomerId.HasValue && childCustomerId.HasValue)
            {
                return DataContext.Reader.CustomerRelations.Any(x =>
                (x.Customer_id == fatherCustomerId.Value &&
                 x.TargetCustomer_id == childCustomerId.Value &&
                 x.IsActive.HasValue &&
                 x.IsActive.Value &&
                 x.PeopleRelationType_id == 99 //This value is static beacuse there is no reference to bll project value 
                 ));
            }
            else
            {
                return false;
            }
        }

        public static int? GetCustomerIdByLoginId(int loginId)
        {
            if (DataContext.Reader.Accounts.Any(x => x.LoginAccount_id == loginId && x.Customer_id.HasValue))
            {
                return DataContext.Reader.Accounts.Where(x => x.LoginAccount_id == loginId).Select(x => x.Customer_id).SingleOrDefault();
            }
            else
            {
                return null;
            }
        }

        public static void ResetLock(int loginId, string clientIP)
        {
            if (Current != null && Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            var login = LoadLogin(loginId);
            login._loginEntity.LastSuccessTime = DateTime.Now;
            login._loginEntity.FailCount = 0;
            DataContext.Writer.LoginAccounts.Update(login._loginEntity, true);
            Log.Add(loginId, clientIP, "Reset lock");
        }

        /// <summary>
        /// Not in use for now.
        /// </summary>
        /// <param name="userType"></param>
        /// <param name="emailAddress"></param>
        /// <param name="clientIP"></param>
        /// <returns></returns>
        public static bool ResetPassword(UserRole userType, string emailAddress, string clientIP)
        {
            //Not checking permission , it's not used inside admin project.
            string password = null;
            var user = (from u in DataContext.Reader.LoginAccounts where u.LoginRole_id == (int)userType && u.LoginEmail == emailAddress select new Login(u)).SingleOrDefault();
            if (user == null) return false;
            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            do password = GeneratePassword();
            while (user.SetPassword(password, clientIP, false, true) != ChangePasswordResult.Success);
            ObjectContext.Current.StopImpersonate();
            return true;
        }

        /// <summary>
        /// ResetPassword when you already have the Login object.
        /// </summary>
        /// <param name="clientIP"></param>
        /// <returns></returns>
        public bool ResetPassword(string clientIP)
        {
            string password = null;
            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            do password = GeneratePassword();
            while (SetPassword(password, clientIP, false, true) != ChangePasswordResult.Success);
            ObjectContext.Current.StopImpersonate();
            return true;
        }

        /// <summary>
        /// ResetPassword used from the Login pages of Admin,Pcp,Mcp
        /// When you have the UserName,Email and Account type.
        /// </summary>
        /// <param name="roles"></param>
        /// <param name="userName"></param>
        /// <param name="emailAddress"></param>
        /// <param name="clientIP"></param>
        /// <returns></returns>
        public static bool ResetPassword(UserRole[] roles, string userName, string emailAddress, string clientIP)
        {
            if (roles.Contains(UserRole.Customer)) throw new Exception("Invalid user type");

            int? userId;
            Login userLogin = null;

            try
            {
                //Get the loginAccountId. 
                userId = GetLoginIdByRoleNameEmail(roles, userName, emailAddress);
                if (!userId.HasValue) return false;

                //Load the login with the loginId found
                userLogin = LoadLogin(userId.Value);
                if (userLogin == null) return false;

                if (userLogin.ResetPassword(clientIP))
                {
                    userLogin.SendLoginAccountPasswordResetMail();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (ValidationException ex)
            {
                Logger.Log(ex);
                return false;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return false;
            }
        }

        

        public static LoginResult DoLogin(System.Security.Principal.WindowsIdentity identity, string deviceIdentity, out Guid credentialsToken)
        {
            credentialsToken = Guid.Empty;
            // get identity & check authentication
            if (identity == null) identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (!(identity.IsAuthenticated || identity.ImpersonationLevel == System.Security.Principal.TokenImpersonationLevel.Impersonation)) return LoginResult.GeneralFailure;

            string groupName = Domain.Current.NetpayAdminsNTGroupName;
            var prn = new System.Security.Principal.WindowsPrincipal(identity);
            bool groupOk = false;
            groupOk = prn.IsInRole(groupName);
            if (!groupOk)
            {
                var groups = identity.Groups.Translate(typeof(System.Security.Principal.NTAccount));
                groupOk = (from g in groups where g.Value.EndsWith(groupName) select g).Any();
            }

            if (!groupOk)
            {
                Logger.Log(LogSeverity.Warning, LogTag.Security, string.Format("User '{0}' must be a member of group '{1}' to use authenticated login.", identity.Name, groupName));
                return LoginResult.GeneralFailure;
            }
            string userName = identity.Name.Split('\\')[1];
            var securityUser = (from su in DataContext.Reader.LoginAccounts where su.LoginRole_id == (int)UserRole.Admin && su.LoginUser == userName select su).SingleOrDefault();
            if (securityUser == null)
            {
                Logger.Log(LogSeverity.Warning, LogTag.Security, string.Format("User '{0}' was not found in 'AdminUsers'.", identity.Name));
                return LoginResult.GeneralFailure;
            }
            return DoLogin(new UserRole[] { (UserRole)securityUser.LoginRole_id }, securityUser.LoginUser, securityUser.LoginEmail, GetActivePassword(securityUser.LoginAccount_id), deviceIdentity, out credentialsToken);
        }

        private static UserRole GetSpecificRoleForSettingNewPassword(UserRole[] roles)
        {
            int count = roles.Count();
            UserRole? result = null;
            switch (count)
            {
                case 1:
                    result = roles[0];
                    break;

                case 2:
                    //The only flow that is allowing two types of roles is only Merchant and MerchantSubUser
                    if (roles.Any(x => (x != UserRole.Merchant && x != UserRole.MerchantSubUser)))
                    {
                        throw new Exception("Roles sent for setting new password are wrong");
                    }
                    else
                    {
                        result = UserRole.Merchant;
                    }
                    break;

                default:
                    throw new Exception("You can't pass roles array with more than two roles");
            }
            return result.Value;
        }

        public static ChangePasswordResult SetNewPasswordAfterExpiredOrReset(UserRole[] roles, string userName, string email, string oldPassword, string newPassword, string clientIP)
        {
            int? loginAccountId = GetLoginIdByCredentials(roles, userName, email, oldPassword);
            if (loginAccountId.HasValue)
            {
                var ret = SetPassword(loginAccountId.Value, newPassword, clientIP, true);
                return ret;
            }
            else
            {
                return ChangePasswordResult.WrongCurrentPassword;
            }
        }

        private static int? GetLoginIdByCredentials(UserRole[] roles, string userName, string email, string password)
        {
            if (!roles.Contains(UserRole.Customer))
            {
                var user = DataContext.Reader.LoginAccounts
                    .Where(x =>
                           //x.LoginUser == userName &&
                           x.LoginEmail == email &&
                           roles.Contains((UserRole)x.LoginRole_id))
                           .FirstOrDefault();
                if (user != null)
                {
                    string activePassword = GetActivePassword(user.LoginAccount_id);
                    if (activePassword == password)
                    {
                        return user.LoginAccount_id;
                    }
                    else
                    {
                        return null;
                    }
                }
                else return null;
            }

            return null;
        }

        /// <summary>
        /// That method assumes that users which are not customers
        /// Can't have duplicated email and username values.
        /// That means that only one user should be found when
        /// Sending role, name and password to that function,
        /// And if more than one user was found, an exception is thrown
        /// </summary>
        /// <param name="roles"></param>
        /// <param name="emailAddress"></param>
        /// <param name="userName"></param>
        /// <returns>LoginAccount_id</returns>
        private static int? GetLoginIdByRoleNameEmail(UserRole[] roles, string userName, string emailAddress)
        {
            if (!roles.Contains(UserRole.Customer))
            {
                var user = DataContext.Reader.LoginAccounts
                    .Where(x => roles.Contains((UserRole)x.LoginRole_id) &&
                                x.LoginEmail == emailAddress.Trim() 
                                //&& x.LoginUser == userName
                                )
                                .ToList();

                switch (user.Count)
                {
                    case 0:
                        return null;
                    case 1:
                        return user.Single().LoginAccount_id;
                    default:
                        throw new ValidationException(ValidationResult.Invalid_Data);

                }
            }
            else
            {
                return null;
            }
        }

        public static LoginResult DoLogin(UserRole[] userType, string userName, string email, string password, string deviceIdentity, out Guid credentialsToken, bool IsWinServiceUser = false, int? parentCompanyId = null, int? appIdentityId = null)
        {
            credentialsToken = Guid.Empty;
            Dal.Netpay.LoginAccount user = null;
            DateTime? passwordDate;
            Login retUser = null;
            IQueryable<int> customersLoginAccountIdsQuery = null;

            lock (typeof(Login))
            {
                bool isCustomer = (userType.Count() == 1 && userType.FirstOrDefault() == UserRole.Customer);
                if (isCustomer)
                {
                    if (appIdentityId == null) return LoginResult.GeneralFailure;
                    customersLoginAccountIdsQuery = from accounts in DataContext.Writer.Accounts.Where(x => x.Customer_id.HasValue && x.LoginAccount_id.HasValue)
                                                    join customers in DataContext.Writer.Customers.Where(x => x.ApplicationIdentity_id.HasValue && x.ApplicationIdentity_id.Value == appIdentityId.Value) on accounts.Customer_id.Value equals customers.Customer_id
                                                    select accounts.LoginAccount_id.Value;
                }
                var xy = DataContext.Writer.LoginAccounts;
                var exp = (from u in DataContext.Writer.LoginAccounts where userType.Contains((UserRole)u.LoginRole_id) select u);

                //If it is a customer, check only loginaccounts that are under the current customer app identity.
                if (isCustomer) exp = exp.Where(u => customersLoginAccountIdsQuery.Contains(u.LoginAccount_id));

                //if (userName != null) exp = exp.Where(u => u.LoginUser == userName); else exp = exp.Where(u => u.LoginUser == null);
                if (email != null) exp = exp.Where(u => u.LoginEmail == email); else if (!isCustomer) exp = exp.Where(u => u.LoginEmail == null);
                user = exp.FirstOrDefault();
                if (user == null) return LoginResult.UserNotFound;
                if (!user.IsActive) return LoginResult.AdminClosed;

                //Check if the AdminUser is active or not
                if (user.LoginRole_id == (byte)UserRole.Admin)
                {
                    var adminuser = DataContext.Reader.AdminUsers.Where(x => x.LoginAccount_id == user.LoginAccount_id).SingleOrDefault();
                    if (adminuser != null)
                    {
                        bool isValid = (adminuser.IsActive.HasValue && adminuser.IsActive.Value);
                        if (!isValid)
                        {
                            return LoginResult.AdminBlocked;
                        }

                        // when the login is for the winservice user, check if IsAdmin is true, 
                        // IsAdmin must be true in order to give all permissions
                        if (IsWinServiceUser)
                        {
                            if (!adminuser.IsAdmin)
                            {
                                throw new Exception("Win service user must be admin of admins.");
                            }
                        }
                        
                    }
                    else
                    {
                        throw new Exception("AdminUser not found");
                    }
                }
                //if (user == null) Log.Add(user.LoginAccount_id, null, LoginResult.UserNotFound);


                #region Removed - see Tickets / #INC-1216
                // check last login
                //if (user.LastSuccessTime != null)
                //{
                //    TimeSpan maxLogoutSpan = new TimeSpan(60, 0, 0, 0, 0);  //if last login is more than 60 days - block
                //    TimeSpan logoutSpan = (DateTime.Now - user.LastSuccessTime.Value);
                //    if (logoutSpan >= maxLogoutSpan)
                //        return Log.Add(user.LoginAccount_id, null, LoginResult.AbsenceBlocked);
                //}

                #endregion Removed - see Tickets / #INC-1216
                // check failed attempts
                if (user.LastFailTime != null)
                {
                    TimeSpan maxBlockedSpan = new TimeSpan(0, 30, 0);       //if last try is within last 30 mins
                    TimeSpan blockedSpan = (DateTime.Now - user.LastFailTime.Value);
                    if (blockedSpan >= maxBlockedSpan) // the block can be removed
                    {
                        user.FailCount = 0;
                        //context.DataContext.SubmitChanges();
                    }
                }
                if (user.FailCount > 2) // if more than 3 fails on last 30 mins - return false
                    return Log.Add(user.LoginAccount_id, null, LoginResult.FailedAttemptsBlocked);

                /*
				// check if the user is already logged in 
				lock (_loggedUsers)
				{
					User loggedUser = (from u in _loggedUsers where u.ID == userID && u.Type == userType && u.SubID == userSubID && u.Password == password && u.deviceIdentity == deviceIdentity select u).SingleOrDefault();
					if (loggedUser != null)
					{
						credentialsToken = loggedUser.CredentialsToken;
						return LoginResult.Success;
					}
				}
				*/
                // check password
                var storedPassword = GetActivePassword(user.LoginAccount_id, out passwordDate);
                if (storedPassword == null) return Log.Add(user.LoginAccount_id, null, LoginResult.UserNotFound);
                if (password != storedPassword)
                {
                    user.FailCount++;
                    user.LastFailTime = DateTime.Now;
                    //DataContext.Writer.SubmitChanges();
                    var ret = (user.FailCount > 2) ? LoginResult.FailedAttemptsBlocked : LoginResult.WrongPassword;
                    return Log.Add(user.LoginAccount_id, null, ret);
                }

                // check if more than three months passed since the insert date of the password
                // If the code reached here it means that the user passed the validation
                // And in the UI login screen the username and password can be used
                // In order to get the LoginAccount_id of the user and set the new password
                // To the LoginAccount entity.
                if (DateTime.Now > passwordDate.Value.AddMonths(3) && RolesAllowNewPassword.Contains((UserRole)user.LoginRole_id) && !IsWinServiceUser)
                {
                    var ret = LoginResult.ForcePasswordChange;
                    return Log.Add(user.LoginAccount_id, null, ret);
                }

                credentialsToken = Guid.NewGuid();
                retUser = new Login(user);

                //Change the LastSuccessTime only if it is not null,
                //If it is null then it means that's a login after
                //Forgot password flow, need to check that
                user.LastSuccessTime = user.LastSuccessTime.HasValue ? DateTime.Now.ToNullableDate() : null;
                if (retUser._lastLogin == null) retUser._lastLogin = DateTime.Now;
                retUser.LoggedInDevice = deviceIdentity;
                retUser._credentialsToken = credentialsToken;
                if (MultiFactor.RequireTwoFactor(retUser.Role) && !MultiFactor.ValidateMultiFactor(retUser))
                    return Log.Add(user.LoginAccount_id, null, LoginResult.MultiFactorFailed);
                DataContext.Writer.LoginAccounts.EnsureAttached(user, true);
                DataContext.Writer.SubmitChanges();
            }
            //add to table
            var loggedUsers = LoggedUsers;
            lock (((System.Collections.ICollection)loggedUsers).SyncRoot)
            {
                loggedUsers.Add(credentialsToken, retUser);
                retUser._lastValidate = DateTime.Now;
                if (DomainManager != null)
                    DomainManager.AddLogin(new LoginManager.LoginData() { CredToken = credentialsToken, LoginID = retUser.LoginID, DeviceId = deviceIdentity, PrevLogin = retUser._lastLogin });
            }
            if (LoggedIn != null) LoggedIn(retUser, EventArgs.Empty);
            //if (DateTime.Now.Subtract(passwordDate.Value).TotalDays > 90)
            //	return Log.Add(user.LoginAccount_id, null, LoginResult.ForcePasswordChange); else 

           return Log.Add(user.LoginAccount_id, null, LoginResult.Success);
        }
        public void LogOff() { LogOff(false); }

        private void LogOff(bool localOnly)
        {
            var loggedUsers = LoggedUsers;
            lock (((System.Collections.ICollection)loggedUsers).SyncRoot)
            {
                var cred = (from l in loggedUsers where l.Value == this select l.Key).SingleOrDefault();
                if (DomainManager != null && !localOnly) DomainManager.RemoveLogin(cred);
                loggedUsers.Remove(cred);
            }
        }

        public static int GetCustomerId(int accountId)
        {
            if (accountId <= 0) throw new Exception("Can't find account with accountId=" + accountId);
            if (!(DataContext.Reader.Accounts.Any(x => x.Account_id == accountId && x.AccountType_id == (byte)AccountTypeForLogin.Customer))) throw new Exception("There is no account entity of type customer with account id :" + accountId);
            return DataContext.Reader.Accounts.Where(x => x.Account_id == accountId).Select(x => x.Customer_id).SingleOrDefault().Value;
        }

        public static int GetMerchantId(int accountId)
        {
            if (accountId <= 0) throw new Exception("Can't find account with accountId=" + accountId);
            if (!(DataContext.Reader.Accounts.Any(x => x.Account_id == accountId && x.AccountType_id == (byte)AccountTypeForLogin.Merchant))) throw new Exception("There is no account entity of type merchant with account id :" + accountId);
            return DataContext.Reader.Accounts.Where(x => x.Account_id == accountId).Select(x => x.Merchant_id).SingleOrDefault().Value;
        }

        public override ValidationResult Validate(Dal.Netpay.Account _accountEntity = null)
        {
            if (string.IsNullOrEmpty(EmailAddress) && string.IsNullOrEmpty(UserName))
                return ValidationResult.Invalid_Email;

            int existID = 0;
            List<int> accountIds = new List<int>();

            switch (Role)
            {
                case UserRole.Customer:
                    //When validating login entity of a customer, the validation will include only customers under 
                    //The same app identity.
                    int customerId = _accountEntity != null ? _accountEntity.Customer_id.Value : GetCustomerId(_loginEntity.LoginAccount_id);
                    int? currentCustomerAppIdentityId = DataContext.Reader.Customers.Where(x => x.Customer_id == customerId).Select(x => x.ApplicationIdentity_id).SingleOrDefault();
                    if (!currentCustomerAppIdentityId.HasValue) throw new Exception("Customer with id:" + customerId + " doesn't have app identity.");

                    //Get the login entities id's of the customers which are under the validated customer's app identity
                    var customersLoginAccountIdsQuery = from accounts in DataContext.Reader.Accounts.Where(x => x.Customer_id.HasValue && x.LoginAccount_id.HasValue)
                                                        join customers in DataContext.Reader.Customers.Where(x => x.ApplicationIdentity_id.HasValue && x.ApplicationIdentity_id.Value == currentCustomerAppIdentityId) on accounts.Customer_id.Value equals customers.Customer_id
                                                        select accounts.LoginAccount_id.Value;

                    existID = DataContext.Reader.LoginAccounts.Where(v =>
                    customersLoginAccountIdsQuery.Contains(v.LoginAccount_id) &&
                    (v.LoginEmail.Trim().ToLower() == EmailAddress.Trim().ToLower()) || (v.LoginEmail == null && EmailAddress == null)) 
                    .Select(v => v.LoginAccount_id)
                    .FirstOrDefault();

                    if (existID != 0 && existID != _loginEntity.LoginAccount_id)
                        return ValidationResult.EmailAlreadyExist;
                    break;
                                        
                case UserRole.Merchant:
                    //When validating login entity of a merchant, The username should be unique
                    existID = DataContext.Reader.LoginAccounts.Where(v =>
                    ((v.LoginUser.Trim().ToLower() == UserName.Trim().ToLower()) || (v.LoginUser == null && UserName == null)))
                    .Select(v => v.LoginAccount_id)
                    .FirstOrDefault();

                    if (existID != 0 && existID != _loginEntity.LoginAccount_id)
                        return ValidationResult.UserNameAlreadyExist;
                    break;

                default:
                    existID = DataContext.Reader.LoginAccounts.Where(v =>
                    v.LoginRole_id == (byte)Role &&
                    ((v.LoginEmail.Trim().ToLower() == EmailAddress.Trim().ToLower()) || (v.LoginEmail == null && EmailAddress == null)) &&
                    ((v.LoginUser.Trim().ToLower() == UserName.Trim().ToLower()) || (v.LoginUser == null && UserName == null)))
                    .Select(v => v.LoginAccount_id).FirstOrDefault();
                    break;
            }

            if (existID != 0 && existID != _loginEntity.LoginAccount_id)
                return ValidationResult.EmailAlreadyExist;

            return base.Validate();
        }

        public void Save(Dal.Netpay.Account _accountEntity = null)
        {
            if (Current != null && Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            var ret = Validate(_accountEntity);
            if (ret != ValidationResult.Success) throw new ValidationException(ret);
            DataContext.Writer.LoginAccounts.Update(_loginEntity, (_loginEntity.LoginAccount_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public virtual void Delete()
        {
            if (Current != null && Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_loginEntity.LoginAccount_id == 0) return;
            VerifySecurity();
            DataContext.Writer.SecurityObjectToLoginAccounts.DeleteAllOnSubmit(DataContext.Writer.SecurityObjectToLoginAccounts.Where(x => x.LoginAccount_id == _loginEntity.LoginAccount_id));
            DataContext.Writer.LoginPasswords.DeleteAllOnSubmit(DataContext.Writer.LoginPasswords.Where(p => p.LoginAccount_id == _loginEntity.LoginAccount_id));
            DataContext.Writer.LoginAccounts.Delete(_loginEntity);
            DataContext.Writer.SubmitChanges();
        }

        public static string GeneratePassword()
        {
            string retValue = "";
            var rnd = new Random();
            for (int i = 0; i < 8; i++)
            {
                if ((i % 3) == 0) retValue += rnd.Next(9).ToString();
                else if ((i % 2) == 0) retValue += char.ConvertFromUtf32('a' + rnd.Next(26));
                else retValue += char.ConvertFromUtf32('A' + rnd.Next(26));
            }
            return retValue;
        }

        public static bool CheckPasswordStrength(string password)
        {
            if (password.Trim().Length < 8) return false;
            int letters = 0;
            int numbers = 0;
            foreach (Char currentChar in password.ToCharArray())
            {
                if (char.IsLetter(currentChar)) letters++;
                if (char.IsDigit(currentChar)) numbers++;
            }
            if (letters < 2 || numbers < 2) return false;
            return true;
        }

        public ChangePasswordResult ChangePassword(string oldPassword, string newPassword, string clientIP)
        {
            //if (newPassword != newPasswordConfirm) return ChangePasswordResult.PasswordConfirmationMissmatch;
            if (!CheckPasswordStrength(newPassword)) return ChangePasswordResult.WeakPassword;
            if (!ValidatePassword(Login.Current.LoginID, oldPassword, clientIP))
                return ChangePasswordResult.WrongCurrentPassword;
            return SetPassword(newPassword, clientIP);
        }

        public bool IsInRole(UserRole role)
        {
            var roles = new List<UserRole>() { Role };
            if (Role == UserRole.MerchantSubUser) roles.Add(UserRole.Merchant);
            if (Role == UserRole.Merchant || Role == UserRole.Customer || Role == UserRole.Partner || Role == UserRole.DebitCompany || Role == UserRole.MerchantSubUser) roles.Add(UserRole.Account);
            return roles.Contains(role);
        }

        public static Login Current
        {
            get
            {
                return ObjectContext.Current.User;
            }
        }

        private static System.Threading.Timer _timerAutoLogOff = null;
        internal static bool EnableLogOffTimer
        {
            set
            {
                lock (typeof(Login))
                {
                    if (value)
                    {
                        if (_timerAutoLogOff == null) _timerAutoLogOff = new System.Threading.Timer(AutoLogOff_Timer, null, 60 * 1000, 60 * 1000);
                    }
                    else
                    {
                        if (_timerAutoLogOff == null) return;
                        _timerAutoLogOff.Dispose(); _timerAutoLogOff = null;
                    }
                }
            }
            get
            {
                lock (typeof(Login))
                {
                    if (_timerAutoLogOff == null) return false;
                    return true;
                }
            }
        }

        private static void AutoLogOff_Timer(object state)
        {
            foreach (var d in Domain.Domains.Values)
            {
                Domain.Current = d;
                try { AutoLogOff(); }
                catch (Exception ex)
                {
                    Logger.Log(ex, "on AutoLogOff Timer for domain:" + d.Host);
                }
            }
        }

        public const int SessionTimout_Min = 25;
        public static void AutoLogOff()
        {
            var loggedUsers = LoggedUsers;
            lock (((System.Collections.ICollection)loggedUsers).SyncRoot)
            {
                var serviceCred = Domain.Current.ServiceCredentials;
                var removeList = new List<KeyValuePair<Guid, Login>>();
                foreach (var v in loggedUsers)
                {
                    if (serviceCred == v.Key) continue;
                    if (DateTime.Now.Subtract(v.Value._lastFetch).TotalMinutes > SessionTimout_Min)
                        removeList.Add(v);
                }
                foreach (var v in removeList)
                {
                    try { v.Value.LogOff(true); }
                    catch { if (loggedUsers.ContainsKey(v.Key)) loggedUsers.Remove(v.Key); }
                }
            }
        }

        public void SendLoginAccountPasswordResetMail()
        {
            switch (Role)
            {
                case UserRole.Admin:
                    SendLoginAccountMail("AdminUser_ResetPassword.htm");
                    break;
                case UserRole.Merchant:
                    SendLoginAccountMail("Merchant_ResetPassword.htm");
                    break;
                case UserRole.Customer:
                    SendLoginAccountMail("Wallet_ResetPassword.htm");
                    break;
                case UserRole.Partner:
                    SendLoginAccountMail("Affiliate_ResetPassword.htm");
                    break;
            }
        }

        public void SendLoginAccountMail(string templateName, object dataObj = null)
        {
            var template = Infrastructure.Email.Template.Get(templateName);
            if (template == null) throw new Exception("Email Template not found " + templateName);
            if (dataObj == null) dataObj = GetEmailData();
            template.Send(dataObj, null, new System.Net.Mail.MailAddress(EmailAddress, UserName));
        }

        public object GetEmailData()
        {
            string password = null;
            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            try
            {
                password = GetPassword(LoginID, null);
            }
            finally
            {
                ObjectContext.Current.StopImpersonate();
            }
            return new ResetEmailData(){ Domain = Domain.Current, LoginAccount = this, LoginPassword = password };
        }

        public class ResetEmailData
        {
            public Domain Domain { get; set; }
            public Login LoginAccount { get; set; }
            public string LoginPassword { get; set; }
        }
    }
}
