﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using System.Text.RegularExpressions;
using System.Web;
using System.Reflection;
using System.Timers;
using Netpay.Infrastructure.VO;
using System.Diagnostics;
using System.Security;
using System.Security.Principal;
using System.Threading;
using Netpay.Infrastructure.Domains;
using Netpay.Dal.DataAccess;


namespace Netpay.Infrastructure.Security
{
	public static class SecurityManager
	{
		private static List<User> _loggedUsers = new List<User>();
		private static System.Timers.Timer _timer = new System.Timers.Timer();
		private static TimeSpan _maxIdleTime = new TimeSpan(0, 20, 0);
        private static object _loginSyncRoot = new object();

		public static void Init()
		{
			// setup vulture timer
			_timer.Elapsed += new ElapsedEventHandler(TimerElapsed);
			_timer.AutoReset = true;
			_timer.Interval = 60000;
			_timer.Start();
		}

		private static void TimerElapsed(object sender, ElapsedEventArgs e)
		{
			if (_loggedUsers.Count == 0)
				return;

			lock (_loggedUsers)
			{
				// remove idle users
				for (int index = _loggedUsers.Count - 1; index >= 0; index--)
				{
					User currentUser = _loggedUsers.ElementAt(index);
					if ((currentUser.Type == UserType.NetpayUser) || (currentUser.Type == UserType.NetpayAdmin)) continue;
					TimeSpan currentIdleTime = DateTime.Now - currentUser.LastUsed;
					if (currentIdleTime >= _maxIdleTime)
						_loggedUsers.Remove(currentUser);
				}
			}
		}

		public static int GetLoggedUsersCount()
		{
			return _loggedUsers.Count;
		}

		public static bool IsPermitted(Guid credentialsToken, PermissionObjectType permissionObjectType, PermissionObjects permissionObject)
		{
			User user = GetInternalUser(credentialsToken);
			if (user.AccessList == null)
				return false;
			if (user.AccessList != null && user.AccessList.Where(po => po.Type == permissionObjectType && po.Name == permissionObject.ToString()).SingleOrDefault() != null)
				return true;

			return false;
		}

        public static bool IsAllowdPageAccess(Guid credentialsToken, string pageUrl, string pageTitle)
        {
            User internalUser = GetInternalUser(credentialsToken);
            if (internalUser == null) return false;
            if (internalUser.AccessList != null && internalUser.AccessList.Where(p => p.Name == pageUrl).Count() > 0) return true;
            NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(internalUser.Domain.Host).Sql1ConnectionString);
            AddManagedPage(credentialsToken, pageUrl, pageTitle.EmptyIfNull());
            return false;
        }

		/// <summary>
        /// Determine whether the user is logged in.
        /// User session is refreshed.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <returns></returns>
		public static bool IsLoggedin(Guid credentialsToken)
		{
			return GetInternalUser(credentialsToken) != null;
		}

		/// <summary>
		/// Gets a logged user by its credentials token.
		/// Returns the user value object which can be used outside bll.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <returns></returns>
		public static UserVO GetUser(Guid credentialsToken)
		{
			User internalUser = GetInternalUser(credentialsToken);
			if (internalUser == null)
				return null;

			return new UserVO(internalUser);
		}

        /// <summary>
		/// Gets the internal logged user.
        /// User session is refreshed.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <returns></returns>
		public static User GetInternalUser(Guid credentialsToken)
		{
			User user = null;
			lock (_loggedUsers)
				user = (from lu in _loggedUsers where lu.CredentialsToken == credentialsToken select lu).SingleOrDefault();

			if (user != null)
				user.LastUsed = DateTime.Now;

			return user;
		}

		/// <summary>
		/// Returns the requested user, if it has permissions to invoke the calling method.
		/// If the user is not permitted, an exception is thrown.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="allowedUserTypes">User types that are allowed to invoke this method.</param>
		/// <returns></returns>
		public static User GetInternalUser(Guid credentialsToken, UserType[] allowedUserTypes)
		{
			// get user
			User user = GetInternalUser(credentialsToken);
			if (user == null)
				throw new NotLoggedinException("Action permission denied, not logged in.");

			// check permissions
			if (!allowedUserTypes.Contains(user.Type))
			{
				StackTrace stackTrace = new StackTrace();
				MethodBase calledMethod = stackTrace.GetFrame(1).GetMethod();
				string calledMethodFullName = string.Format("{0}.{1}", calledMethod.DeclaringType.FullName, calledMethod.Name);

				Logger.Log(LogSeverity.Warning, LogTag.Security, "Method access denied.", string.Format("The following user was denied access to {0}.\n{1}", calledMethodFullName, user.ToString()));
				throw new MethodAccessDeniedException("Method access denied, see log.");
			}

			return user;
		}

		public static void AuditInvocation(Guid credentialsToken, object[] args)
		{
			// get user
			User user = GetInternalUser(credentialsToken);
			if (user == null)
				throw new NotLoggedinException("Cannot audit method call, user is not logged in.");

			StackTrace stackTrace = new StackTrace();
			MethodBase calledMethod = stackTrace.GetFrame(1).GetMethod();
			string calledMethodFullName = string.Format("{0}.{1}", calledMethod.DeclaringType.FullName, calledMethod.Name);

			// build args string
			StringBuilder argsBuilder = new StringBuilder();
			for (int idx = 0; idx < args.Length; idx++)
			{
				string argName = calledMethod.GetParameters()[idx].Name;
				string argValue = "null";
				if (args[idx] != null)
					argValue = args[idx].ToString();
				argsBuilder.Append(string.Format("{0} = {1}, ", argName, argValue));
			}

			Logger.Log(string.Format("Method call audit: \n{0}\n{1}({2})", user.ToString(), calledMethodFullName, argsBuilder.ToString()));
		}

		/// <summary>
		/// Logs out a user.
		/// </summary>
		/// <param name="credentialsToken"></param>
		public static void Logout(Guid credentialsToken)
		{
			lock (_loggedUsers)
			{
				User user = (from u in _loggedUsers where u.CredentialsToken == credentialsToken select u).SingleOrDefault();
				if (user != null)
					_loggedUsers.Remove(user);
			}
		}

		private static System.Collections.Generic.Dictionary<string, Guid>  _serviceLogins = new System.Collections.Generic.Dictionary<string, Guid>();

		private static LoginResult addLoginLog(NetpayDataContext dc, UserType userType, int userId, string ipAddress, LoginResult result)
		{
			var le = new Dal.Netpay.LoginHistory();
			//le.LoginType_id = (byte)userType;
			//le.SourceIdentity = userId;
			le.InsertDate = DateTime.Now;
			le.IPAddress = ipAddress;
			le.LoginResult_id = (byte)result;
			dc.LoginHistories.InsertOnSubmit(le);
			dc.SubmitChanges();
			return result;
		}

		/// <summary>
		/// Logs in a user which is NT authenticated.
		/// </summary>
		/// <param name="domainHost"></param>
		/// <param name="credentialsToken"></param>
		/// <returns></returns>
		public static LoginResult Login(string domainHost, WindowsIdentity identity, out Guid credentialsToken)
		{
            lock (_loginSyncRoot)
            {
                credentialsToken = Guid.Empty;
                // get identity & check authentication
                if (identity == null) identity = System.Security.Principal.WindowsIdentity.GetCurrent();
                if (!identity.IsAuthenticated)
                    return LoginResult.GeneralFailure;

                // check group
                string groupName = DomainsManager.GetDomain(domainHost).NetpayAdminsNTGroupName;
                IdentityReferenceCollection groups = identity.Groups.Translate(typeof(NTAccount));
                IdentityReference foundGroup = (from g in groups where g.Value.EndsWith(groupName) select g).SingleOrDefault();
                if (foundGroup == null)
                {
                    Logger.Log(LogSeverity.Warning, LogTag.Security, string.Format("User '{0}' must be a member of '{1}' to use authenticated login.", identity.Name, groupName));
                    return LoginResult.GeneralFailure;
                }

                // get security user
                NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
                string userName = identity.Name.Split('\\')[1];
                tblSecurityUser securityUser = (from su in dc.tblSecurityUsers where su.su_Username == userName select su).SingleOrDefault();
                if (securityUser == null)
                {
                    Logger.Log(LogSeverity.Warning, LogTag.Security, string.Format("User '{0}' was not found in 'tblSecurityUser'.", identity.Name));
                    return LoginResult.GeneralFailure;
                }

                // set user type
                UserType userType = UserType.NetpayUser;
                if (securityUser.su_IsAdmin)
                    userType = UserType.NetpayAdmin;

                // get user password, 1 is the current password
                tblPasswordHistory passwordHistory = (from ph in dc.tblPasswordHistories where ph.LPH_RefID == securityUser.ID && ph.LPH_RefType == (int)userType && ph.LPH_ID == 1 select ph).SingleOrDefault();
                if (passwordHistory == null)
                {
                    Logger.Log(LogSeverity.Warning, LogTag.Security, string.Format("User '{0}', ID '{1}', Type '{2}' was not found in 'tblPasswordHistory'.", identity.Name, securityUser.ID, userType), "Password history search is based on security user entry, look for incompatibility.");
                    return LoginResult.GeneralFailure;
                }

                // log in 
                string decryptedPassword = Encryption.Decrypt(domainHost, passwordHistory.LPH_Password256.ToArray());
                return Login(domainHost, userType, userName, securityUser.su_Mail, decryptedPassword, out credentialsToken); 
            }
		}

		public static LoginResult Login(string domainHost, UserType userType, string userName, string email, string password, out Guid credentialsToken)
		{
			return Login(domainHost, userType, userName, email, password, null, out credentialsToken);
		}

		/// <summary>
		/// Logs a user in.
		/// </summary>
		/// <param name="domainHost"></param>
		/// <param name="userType"></param>
		/// <param name="userName"></param>
		/// <param name="email"></param>
		/// <param name="password"></param>
		/// <param name="credentialsToken"></param>
		/// <returns></returns>
		public static LoginResult Login(string domainHost, UserType userType, string userName, string email, string password, string deviceIdentity, out Guid credentialsToken)
		{
			credentialsToken = Guid.Empty;
			int userID = 0;
			int? userSubID = null;
			int? applicationIdentity = null;
			int? accountId = null;
            bool? verified = null;
            string hashKey = null;
            string name = null;
            string number = null;
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);

            lock (_loginSyncRoot)
            {
                // get user id by type and login
                tblAffiliate affiliate = null;
                tblCompany merchant = null;
                if (userType == UserType.Partner)
                {
                    affiliate = (from a in dc.tblAffiliates where a.ControlPanelUsername == userName && a.ControlPanelEmail == email && a.IsActive select a).SingleOrDefault();
                    if (affiliate == null)
                        return LoginResult.UserNotFound;
                    userID = affiliate.affiliates_id;
                    name = affiliate.name;
					accountId = (from a in dc.Accounts where a.Affiliate_id == userID select a.Account_id).SingleOrDefault();
				}
                else if (userType == UserType.MerchantPrimary)
                {
                    merchant = (from c in dc.tblCompanies where c.UserName == userName && c.Mail == email select c).SingleOrDefault();
                    if (merchant == null)
                        return LoginResult.UserNotFound;
                    userID = merchant.ID;
                    name = merchant.CompanyName;
                    number = merchant.CustomerNumber;
                    hashKey = merchant.HashKey;
					accountId = (from a in dc.Accounts where a.Merchant_id == userID select a.Account_id).SingleOrDefault();
				}
                else if (userType == UserType.MerchantLimited)
                {
                    MerchantSubUser limitedUser = (from m in dc.tblCompanies join lu in dc.MerchantSubUsers on m.ID equals lu.Merchant_id where m.Mail == email && lu.UserName == userName select lu).SingleOrDefault();
                    if (limitedUser == null)
                        return LoginResult.UserNotFound;
                    userID = limitedUser.Merchant_id;
                    userSubID = limitedUser.MerchantSubUser_id;
                    merchant = limitedUser.tblCompany;
                    name = merchant.CompanyName;
                    number = merchant.CustomerNumber;
                    hashKey = merchant.HashKey;
					accountId = (from a in dc.Accounts where a.Merchant_id == userID select a.Account_id).SingleOrDefault();
				}
                else if (userType == UserType.Customer)
                {
                    Customer customer = (from c in dc.Customers where c.EmailAddress == email select c).FirstOrDefault();
                    if (customer == null)
                        return LoginResult.UserNotFound;
                    //if (customer.Blocked)
                    //    return LoginResult.AdminBlocked;
                    //if (customer.Closed)
                    //    return LoginResult.AdminClosed;
                    userID = customer.Customer_id;
                    name = customer.FirstName + " " + customer.LastName;
                    number = customer.CustomerNumber;
                    verified = (customer.EmailToken == null);
					applicationIdentity = customer.ApplicationIdentity_id;
					accountId = (from a in dc.Accounts where a.Customer_id == userID select a.Account_id).SingleOrDefault();
                }
                else if (userType == UserType.NetpayAdmin || userType == UserType.NetpayUser)
                {
                    tblSecurityUser user = (from u in dc.tblSecurityUsers where u.su_Username == userName && u.su_Mail == email select u).SingleOrDefault();
                    if (user == null)
                        return LoginResult.UserNotFound;
                    userID = user.ID;
                    name = user.su_Name;
                }
                else
                {
                    throw new ApplicationException("Invalid user type.");
                }

                // get user password history
                int refID = userID;
                if (userType == UserType.MerchantLimited)
                    refID = userSubID.Value;
                tblPasswordHistory passwordHistory = (from ph in dc.tblPasswordHistories where ph.LPH_ID == 1 && ph.LPH_RefType == (int)userType && ph.LPH_RefID == refID select ph).SingleOrDefault();
                if (passwordHistory == null)
                    return addLoginLog(dc, userType, refID, null, LoginResult.UserNotFound);

                // check last login
                TimeSpan maxLogoutSpan = new TimeSpan(60, 0, 0, 0, 0);
                TimeSpan logoutSpan = (DateTime.Now - passwordHistory.LPH_LastSuccess);
                if (logoutSpan >= maxLogoutSpan)
					return addLoginLog(dc, userType, refID, null, LoginResult.AbsenceBlocked);

                // check failed attempts
                TimeSpan maxBlockedSpan = new TimeSpan(0, 30, 0);
                TimeSpan blockedSpan = (DateTime.Now - passwordHistory.LPH_LastFail);
				if (blockedSpan >= maxBlockedSpan) // the block can be removed
                {
                    passwordHistory.LPH_FailCount = 0;
                    //dc.SubmitChanges();
                }

                if (passwordHistory.LPH_FailCount > 2)
					return addLoginLog(dc, userType, refID, null, LoginResult.FailedAttemptsBlocked);

                // check if the user is already logged in 
                lock (_loggedUsers)
                {
					User loggedUser = (from u in _loggedUsers where u.ID == userID && u.Type == userType && u.SubID == userSubID && u.Password == password && u.deviceIdentity == deviceIdentity select u).SingleOrDefault();
                    if (loggedUser != null)
                    {
                        credentialsToken = loggedUser.CredentialsToken;
                        return LoginResult.Success;
                    }
                }

                // check password
                string storedPassword = Encryption.Decrypt(domainHost, passwordHistory.LPH_Password256.ToArray());
                if (password != storedPassword)
                {
                    passwordHistory.LPH_FailCount++;
                    passwordHistory.LPH_LastFail = DateTime.Now;
                    dc.SubmitChanges();
					var ret = (passwordHistory.LPH_FailCount > 2) ? LoginResult.FailedAttemptsBlocked : LoginResult.WrongPassword;
					return addLoginLog(dc, userType, refID, null, ret);

                }

                // log successful attempt
                tblLoginHistory loginHistory = new tblLoginHistory();
                loginHistory.lh_Date = DateTime.Now;
                loginHistory.lh_UserID = userID;
                loginHistory.lh_UserType = (int)userType;
                if (HttpContext.Current == null)
                {
                    loginHistory.lh_IP = "no context";
                    loginHistory.lh_Identity = "no context";
                    loginHistory.lh_Site = "no context";
                }
                else
                {
                    loginHistory.lh_IP = HttpContext.Current.Request.UserHostAddress;
                    loginHistory.lh_Identity = HttpContext.Current.Request.ApplicationPath;
                    loginHistory.lh_Site = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
                }

                dc.tblLoginHistories.InsertOnSubmit(loginHistory);
                dc.SubmitChanges();

                // update last successful login date
                DateTime lastLogin = passwordHistory.LPH_LastSuccess;
				passwordHistory.LPH_FailCount = 0;
                passwordHistory.LPH_LastSuccess = DateTime.Now;
                dc.SubmitChanges();

                // add logged user
                User newUser = new User();
                newUser.CredentialsToken = credentialsToken = Guid.NewGuid();
                newUser.ID = userID;
                newUser.SubID = userSubID;
                newUser.Verified = verified;
                newUser.Type = userType;
                newUser.LastUsed = DateTime.Now;
                newUser.Email = email;
                newUser.LastLogin = lastLogin;
                newUser.UserName = userName;
                newUser.Password = password;
                newUser.Domain = DomainsManager.GetDomain(domainHost);
                newUser.Name = name;
                newUser.Number = number;
				newUser.AccountID = accountId;
				newUser.HashKey = hashKey;
				newUser.ApplicationIdentity = applicationIdentity;

                if (userType == UserType.Partner)
                {
					newUser.AllowedMerchantsIDs = (from m in dc.SetMerchantAffiliates where m.Affiliate_id == userID select m.Merchant_id).ToList<int>();

                    // get affiliate access list
                    newUser.AccessList = (from up in dc.tblUserPermissions where up.UserTypeID == (int)UserType.Partner && up.UserID == userID select new PermissionObjectVO(up)).ToList();
                    if (newUser.AccessList == null)
                        newUser.AccessList = new List<PermissionObjectVO>();
                }
                else if (userType == UserType.MerchantPrimary || userType == UserType.MerchantLimited)
                {
                    newUser.Partners = (from a in dc.SetMerchantAffiliates where a.Merchant_id == merchant.ID select a.Affiliate_id).ToArray();
                    newUser.LinkName = merchant.MerchantLinkName;
                    newUser.PayPercent = merchant.PayPercent;
                    newUser.AllowedMerchantsIDs.Add(newUser.ID);
                    if (!string.IsNullOrEmpty(newUser.LinkName))
                        newUser.MerchantLinks = (from c in dc.tblCompanies where c.MerchantLinkName == newUser.LinkName select new { id = c.CustomerNumber, name = c.CustomerNumber + " - " + c.CompanyLegalName }).ToDictionary(c => c.id, c => c.name);

                    // get limited user access list
                    if (userType == UserType.MerchantLimited)
                        newUser.AccessList = (from ll in dc.tblLimitedLoginSecurities where ll.lls_MerchantID == userID && ll.lls_UserID == userSubID select new PermissionObjectVO(ll)).ToList();
                    if (newUser.AccessList == null)
                        newUser.AccessList = new List<PermissionObjectVO>();
					newUser.IsFirstLogin = (from cat in dc.MerchantActivities where cat.Merchant_id == userID select cat.DateFirstLogin).SingleOrDefault() == null;
					if (newUser.IsFirstLogin) dc.ExecuteCommand("Update [Track].[MerchantActivity] Set DateFirstLogin=getDate() Where DateFirstLogin is null And Merchant_id={0}", userID);

					if (deviceIdentity != null) {
						newUser.deviceIdentity = deviceIdentity;
						MobileDevice device = null;
						if (newUser.SubID == null) device = (from d in dc.MobileDevices where d.Merchant_id == newUser.ID && d.MerchantSubUser_id == null && d.DeviceIdentity == deviceIdentity select d).SingleOrDefault();
						else device = (from d in dc.MobileDevices where d.Merchant_id == newUser.ID && d.MerchantSubUser_id == newUser.SubID && d.DeviceIdentity == deviceIdentity select d).SingleOrDefault();
						if (device != null)
						{
							if (device.SignatureFailCount >= 3)
								return addLoginLog(dc, userType, refID, null, LoginResult.FailedSignatureBlocked);
							newUser.deviceID = device.MobileDevice_id;
							newUser.activationCode = device.PassCode;
							newUser.isDeviceActivated = device.IsActivated;
							newUser.isDeviceBlocked = !device.IsActive;
							newUser.isDeviceRegistered = true;
						}//else return LoginResult.UserNotFound;
					}
				}
                else if (userType == UserType.NetpayUser || userType == UserType.NetpayAdmin)
                {
                    // get netpay user page access list
                    // the access list contains pages that both the user and the user groups have access to
                    var userAllowedDocs = (from securityUser in dc.tblSecurityUsers
                                           join securityDocumentGroup in dc.tblSecurityDocumentGroups on securityUser.ID * -1 equals securityDocumentGroup.sdg_Group
                                           join securityDocument in dc.tblSecurityDocuments on securityDocumentGroup.sdg_Document equals securityDocument.ID
                                           where securityUser.ID == newUser.ID && (securityDocumentGroup.sdg_IsVisible || securityDocumentGroup.sdg_IsActive)
                                           select securityDocument.sd_URL).Distinct();

                    var userGroupsAllowedDocs = (from securityUser in dc.tblSecurityUsers
                                                 join securityUserGroup in dc.tblSecurityUserGroups on securityUser.ID equals securityUserGroup.sug_User
                                                 join securityDocumentGroup in dc.tblSecurityDocumentGroups on securityUserGroup.sug_Group equals securityDocumentGroup.sdg_Group
                                                 join securityDocument in dc.tblSecurityDocuments on securityDocumentGroup.sdg_Document equals securityDocument.ID
                                                 where securityUser.ID == newUser.ID && (securityDocumentGroup.sdg_IsVisible || securityDocumentGroup.sdg_IsActive)
                                                 select securityDocument.sd_URL).Distinct();

                    newUser.AccessList = userAllowedDocs.Intersect(userGroupsAllowedDocs).Select(s => new PermissionObjectVO() { Name = s, Type = PermissionObjectType.WebPage }).ToList();

                    // get allowed banks
                    newUser.AllowedBanksIDs = (from ab in dc.tblSecurityUserDebitCompanies where ab.sudc_User == userID select ab.sudc_DebitCompany).ToList<int>();

                    // get allowed merchants
                    newUser.AllowedMerchantsIDs = (from ab in dc.tblSecurityUserMerchants where ab.sum_User == userID select ab.sum_Merchant).ToList<int>();

                    //load gorups
                    newUser.memberOfGourps = (from g in dc.tblSecurityUserGroups where g.sug_User == userID select g.sug_Group).ToArray();
                }

                // on first login by merchant's primary user - set login date in activity tracking
                lock (_loggedUsers)
                {
					//u.UserName == userName && u.Password == password && u.Email == email && 
					User loggedUser = (from u in _loggedUsers where u.ID == newUser.ID && u.SubID == newUser.SubID && u.Type == newUser.Type && u.deviceID == newUser.deviceID select u).SingleOrDefault();
                    if (loggedUser != null)
                    {
                        newUser.CredentialsToken = loggedUser.CredentialsToken;
						_loggedUsers.Remove(loggedUser);
                    }
                    _loggedUsers.Add(newUser);
                }
				credentialsToken = newUser.CredentialsToken;
				return addLoginLog(dc, userType, refID, null, LoginResult.Success);
            }
		}

        public static bool IsMemberOf(Guid credentialsToken, int gourpID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin });
            if (user.memberOfGourps == null) return false;
            if (user.memberOfGourps.Contains(gourpID)) return true;
            return false;
        }

        public static bool IsMemberOf(Guid credentialsToken, string groupName)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin });
            if (!user.Domain.Cache.SecurityGroups.ContainsKey(groupName)) throw new ApplicationException(string.Format("group {0} could not be found", groupName));
            if (user.memberOfGourps == null) return false;
            if (user.memberOfGourps.Contains(user.Domain.Cache.SecurityGroups[groupName].ID)) return true;
            return false;
        }

        public static List<int> GetAllowedMerchants(string domainHost, string securityUserEmail) 
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var entity = (from su in dc.tblSecurityUsers where su.su_Mail == securityUserEmail select su).SingleOrDefault();
            if (entity == null)
                return null;

            return GetAllowedMerchants(domainHost, entity.ID);
        }

        public static List<int> GetAllowedMerchants(string domainHost, int securityUserID)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var entities = from sum in dc.tblSecurityUserMerchants where sum.sum_User == securityUserID select sum.sum_Merchant;

            return entities.ToList();
        }

        /// <summary>
        /// Gets a list of security users with their allowed merchants list
        /// </summary>
        /// <param name="domainHost"></param>
        /// <returns></returns>
        public static List<UserVO> GetAllowedMerchants(string domainHost)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var entities = from su in dc.tblSecurityUsers select su;
            List<UserVO> users = new List<UserVO>();
            foreach (var currentEntity in entities)
            {
                UserVO currentUser = new UserVO();
                currentUser.ID = currentEntity.ID;
                currentUser.Email = currentEntity.su_Mail;
                currentUser.AllowedMerchants = currentEntity.tblSecurityUserMerchants.Select(m => m.sum_Merchant).ToList();
                currentUser.Name = currentEntity.su_Name;
				currentUser.UserName = currentEntity.su_Username;
                users.Add(currentUser);
            }

            return users;
        }

		public static string GeneratePassword()
		{
			string retValue = "";
			var rnd = new Random();
			for (int i = 0; i < 8; i++){
				if ((i % 3) == 0) retValue += rnd.Next(9).ToString();
				else if ((i % 2) == 0) retValue += 'a' + rnd.Next(26).ToString();
				else retValue += 'A' + rnd.Next(26).ToString();
			}
			return retValue;
		}

		/// <summary>
		/// Returns true if the password contains atleast 2 alphanumeric chars, 2 numeric chars and is atleast 8 chars long.
		/// This is the PCI DSS requirement.
		/// </summary>
		/// <param name="password"></param>
		/// <returns></returns>
		public static bool CheckPasswordStrength(string password)
		{
			if (password.Trim().Length < 8)
				return false;

			int letters = 0;
			int numbers = 0;

			foreach (Char currentChar in password.ToCharArray())
			{
				if (char.IsLetter(currentChar))
					letters++;
				if (char.IsDigit(currentChar))
					numbers++;
			}

			if (letters < 2 || numbers < 2)
				return false;

			return true;
		}

		public static void SetNewPassword(string domainHost, UserType userType, int userID, string password)
		{
            byte[] encryptedPass;
            Netpay.Infrastructure.Security.Encryption.Encrypt(domainHost, password, out encryptedPass);
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			tblPasswordHistory foundCurrentPassword = (from ph in dc.tblPasswordHistories where ph.LPH_ID == 1 && ph.LPH_RefType == (int)userType && ph.LPH_RefID == userID select ph).FirstOrDefault();
			if (foundCurrentPassword != null) throw new ApplicationException("Can't set new password to existing user, use changePassword instead");
            tblPasswordHistory passwordHistory = new tblPasswordHistory();
            passwordHistory.LPH_ID = 1;
            passwordHistory.LPH_Insert = DateTime.Now;
            passwordHistory.LPH_LastFail = DateTime.Now;
            passwordHistory.LPH_LastSuccess = DateTime.Now;
			passwordHistory.LPH_RefType = (int)userType;
            passwordHistory.LPH_RefID = userID;
            passwordHistory.LPH_Password256 = encryptedPass;
            passwordHistory.LPH_IP = "";
            dc.tblPasswordHistories.InsertOnSubmit(passwordHistory);
            dc.SubmitChanges();
		}

        public static string ResetPassword(string domainHost, int userID, UserType userType, string clientIP)
		{
			string password = null;
			do {
				password = System.Guid.NewGuid().ToString().Replace("-", "").Substring(24);
			} while (ChangePassword(domainHost, userID, userType, password, clientIP) != ChangePasswordResult.Success);
			return password;
		}

		public static string GetPassword(Guid credentialsToken, UserType userType, int userId)
		{
			var user = GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin } );
			var context = new Dal.DataAccess.NetpayDataContext(user.Domain.Sql1ConnectionString);
			string password = "";
			var encPassword = (from h in context.tblPasswordHistories where h.LPH_RefID == userId && h.LPH_RefType == (int)userType&& h.LPH_ID == 1 select h.LPH_Password256).SingleOrDefault();
			if (encPassword != null) password = Infrastructure.Security.Encryption.Decrypt(user.Domain.EncryptionKeyNumber, encPassword.ToArray());
			return password;
		}

		public static ChangePasswordResult ChangePassword(Guid credentialsToken, string newPassword, string oldPassword, string clientIP)
		{
			var user = GetInternalUser(credentialsToken);
			if (user == null) 
				return ChangePasswordResult.WrongCurrentPassword;
			return ChangePassword(user.Domain.Host, user.ID, oldPassword, newPassword, newPassword, user.Type, clientIP);
		}

		/*
		public static ChangePasswordResult ChangePassword(Guid credentialsToken, string newPassword, string newPasswordConfirm, string clientIP)
		{
			var user = GetInternalUser(credentialsToken);
			if (user == null) return ChangePasswordResult.WrongCurrentPassword;
			return ChangePassword(user.Domain.Host, user.ID, user.Password, newPassword, newPasswordConfirm, user.Type, clientIP);
		}
		*/

		private static ChangePasswordResult ChangePassword(string domainHost, int userID, UserType userType, string newPassword, string clientIP)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var updateResult = dc.UpdatePasswordChange((int)userType, userID, clientIP, newPassword).FirstOrDefault();
			if (updateResult.IsInserted.Value)
			{
				User loggedUser = (from u in _loggedUsers where u.ID == userID && u.Type == userType select u).SingleOrDefault();
				if (loggedUser != null)
				{
					loggedUser.Password = newPassword;
					loggedUser.Verified = true;
				}
				return ChangePasswordResult.Success;
			}
			else return ChangePasswordResult.PasswordUsedInThePast;
		}

        public static ChangePasswordResult ChangePassword(string domainHost, int userID, string currentPassword, string newPassword, string newPasswordConfirm, UserType userType, string clientIP)
		{
			// check new password confirmation
			if (newPassword != newPasswordConfirm)
				return ChangePasswordResult.PasswordConfirmationMissmatch;

			// check new password strength
			if (!CheckPasswordStrength(newPassword))
				return ChangePasswordResult.WeakPassword;

			// check current password
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			byte[] encryptedPassword;
			Encryption.Encrypt(domainHost, currentPassword, out encryptedPassword);
			tblPasswordHistory foundCurrentPassword = (from ph in dc.tblPasswordHistories where ph.LPH_ID == 1 && ph.LPH_RefType == (int)userType && ph.LPH_RefID == userID && ph.LPH_Password256 == encryptedPassword select ph).SingleOrDefault();
			if (foundCurrentPassword == null)
				return ChangePasswordResult.WrongCurrentPassword;
			return ChangePassword(domainHost, userID, userType, newPassword, clientIP);
		}

		public static bool IsManagedPage(Guid credentialsToken, string pageFileName)
		{
			User user = GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblSecurityDocument foundPage = (from p in dc.tblSecurityDocuments where p.sd_URL.ToLower() == pageFileName.ToLower() select p).SingleOrDefault();

			return foundPage != null;
		}

		public static void AddManagedPage(Guid credentialsToken, string pageFileName, string pageTitle)
		{
			User user = GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblSecurityDocument foundPage = (from p in dc.tblSecurityDocuments where p.sd_URL.ToLower() == pageFileName.ToLower() select p).SingleOrDefault();
			if (foundPage != null)
				return;

			tblSecurityDocument mangedPage = new tblSecurityDocument();
			mangedPage.sd_Title = pageTitle;
			mangedPage.sd_URL = pageFileName;
			dc.tblSecurityDocuments.InsertOnSubmit(mangedPage);
			dc.SubmitChanges();
		}

		public static List<ManagedPagePermissionVO> GetManagedPagePermissions(Guid credentialsToken, string pageFileName)
		{
			User user = GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblSecurityDocument managedPage = (from mp in dc.tblSecurityDocuments where mp.sd_URL == pageFileName select mp).SingleOrDefault();
			if (managedPage == null)
				throw new ApplicationException("Managed page not found.");

			// get groups
			List<ManagedPagePermissionVO> groupPermissions = (from securityDocumentGroup in dc.tblSecurityDocumentGroups
															  join securityGroup in dc.tblSecurityGroups on securityDocumentGroup.sdg_Group equals securityGroup.ID
															  where securityDocumentGroup.sdg_Document == managedPage.ID
															  select new ManagedPagePermissionVO() { PageID = securityDocumentGroup.sdg_Document, PagePermissionID = securityDocumentGroup.ID, Type = SecurityObjectType.Group, Name = securityGroup.sg_Name, Description = securityGroup.sg_Description, IsActive = securityDocumentGroup.sdg_IsActive, IsVisible = securityDocumentGroup.sdg_IsVisible }).ToList<ManagedPagePermissionVO>();

			// get users
			List<ManagedPagePermissionVO> usersPermissions = (from securityDocumentGroup in dc.tblSecurityDocumentGroups
															  join securityUser in dc.tblSecurityUsers on securityDocumentGroup.sdg_Group * -1 equals securityUser.ID
															  where securityDocumentGroup.sdg_Document == managedPage.ID
															  select new ManagedPagePermissionVO() { PageID = securityDocumentGroup.sdg_Document, PagePermissionID = securityDocumentGroup.ID, Type = SecurityObjectType.User, Name = securityUser.su_Username, Description = securityUser.su_Name, IsActive = securityDocumentGroup.sdg_IsActive, IsVisible = securityDocumentGroup.sdg_IsVisible }).ToList<ManagedPagePermissionVO>();

			List<ManagedPagePermissionVO> result = new List<ManagedPagePermissionVO>();
			result.AddRange(groupPermissions);
			result.AddRange(usersPermissions);

			return result;
		}

		public static List<int> FilterRequestedIDs(List<int> requestedIDs, List<int> userAllowedIDs)
		{
			if (requestedIDs == null)
				return userAllowedIDs;

			if (requestedIDs.Count == 0)
				return userAllowedIDs;

			return requestedIDs.Intersect(userAllowedIDs).ToList();
		}

		public static void RemoveManagedPage(Guid credentialsToken, string pageFileName)
		{
			User user = GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblSecurityDocument foundPage = (from p in dc.tblSecurityDocuments where p.sd_URL.ToLower() == pageFileName.ToLower() select p).SingleOrDefault();
			if (foundPage == null)
				return;

			dc.tblSecurityDocuments.DeleteOnSubmit(foundPage);
			dc.SubmitChanges();
		}

		/// <summary>
		/// Iterates over the string properties of an insstance and html encodes them.
		/// </summary>
		/// <param name="source"></param>
		public static void HtmlEncodeStringProperties(object source)
		{
			MemberInfo[] properties = source.GetType().FindMembers(MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance, null, null);
			foreach (MemberInfo currentProperty in properties)
			{
				PropertyInfo currentPropertyInfo = source.GetType().GetProperty(currentProperty.Name);
				if (currentPropertyInfo.PropertyType == typeof(string))
				{
					object currentPropertyValue = currentPropertyInfo.GetValue(source, null);
					if (currentPropertyValue != null)
					{
						string currentPropertyCleanValue = currentPropertyValue.ToString().ToEncodedHtml();
						if (currentPropertyInfo.CanWrite)
							currentPropertyInfo.SetValue(source, currentPropertyCleanValue, null);
					}
				}
			}
		}

		/// <summary>
		/// Iterates over the string properties of an insstance and html decodes them.
		/// </summary>
		/// <param name="source"></param>
		public static void HtmlDecodeStringProperties(object source)
		{
			MemberInfo[] properties = source.GetType().FindMembers(MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance, null, null);
			foreach (MemberInfo currentProperty in properties)
			{
				PropertyInfo currentPropertyInfo = source.GetType().GetProperty(currentProperty.Name);
				if (currentPropertyInfo.PropertyType == typeof(string))
				{
					object currentPropertyValue = currentPropertyInfo.GetValue(source, null);
					if (currentPropertyValue != null)
					{
						string currentPropertyCleanValue = currentPropertyValue.ToString().ToDecodedHtml();
						if (currentPropertyInfo.CanWrite)
							currentPropertyInfo.SetValue(source, currentPropertyCleanValue, null);
					}
				}
			}
		}
	}
}
