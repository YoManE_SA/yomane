﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.VO;
using Netpay.Infrastructure.Domains;

namespace Netpay.Infrastructure.Security
{
	/// <summary>
	/// Netpay user.
	/// </summary>
	public class User
	{
		private Guid _credentialsToken = Guid.Empty;
		public int? AccountID;
		private int? _ID = null;
		private int? _subID = null;
		private string _number = null;
		private string _name = null;
		private string _email = null;
		private UserType _type = UserType.Unknown;
		private string _password = null;
		private string _userName = null;
        private bool? _isVerified = null; /*if needs to answer verification email*/
		private DateTime _lastUsed = DateTime.MinValue;
		private bool _isFirstLogin = true;
		private List<PermissionObjectVO> _accessList = null;
		private List<int> _allowedMerchantsIDs = new List<int>();
		private List<int> _allowedBanksIDs = new List<int>();
		private DateTime _lastLogin = DateTime.MinValue;
		private Domain _domain = null;
		private Decimal? _PayPercent = null;
		private string _linkName = null;
		private Dictionary<string, string> _merchantLinks;
        public int[] memberOfGourps = null;
        public string HashKey = null;
        private int[] _Partners;

		public int? ApplicationIdentity = null;
        public int? deviceID = null;
		public string deviceIdentity = null;
		public string activationCode = null;
        public bool isDeviceActivated = false;
        public bool isDeviceRegistered = false;
		public bool isDeviceBlocked = false;

		public Decimal? PayPercent
		{
			get { return _PayPercent; }
			set { _PayPercent = value; }
		}

		public Domain Domain
		{
			get { return _domain; }
			set { _domain = value; }
		}

		public int? SubID
		{
			get { return _subID; }
			set { _subID = value; }
		}

		public DateTime LastLogin
		{
			get { return _lastLogin; }
			set { _lastLogin = value; }
		}

		public List<int> AllowedMerchantsIDs
		{
			get { return _allowedMerchantsIDs; }
			set { _allowedMerchantsIDs = value; }
		}

		public List<int> AllowedBanksIDs
		{
			get { return _allowedBanksIDs; }
			set { _allowedBanksIDs = value; }
		}

		public List<PermissionObjectVO> AccessList
		{
			get { return _accessList; }
			set { _accessList = value; }
		}

		public bool IsFirstLogin
		{
			get { return _isFirstLogin; }
			set { _isFirstLogin = value; }
		}
		
		public int ID
		{
			get { return _ID.Value; }
			set { _ID = value; }
		}

		public Guid CredentialsToken
		{
			get { return _credentialsToken; }
			set { _credentialsToken = value; }
		}

		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		public string Email
		{
			get { return _email; }
			set { _email = value; }
		}

		public string Number
		{
			get { return _number; }
			set { _number = value; }
		}
		
		public UserType Type
		{
			get { return _type; }
			set { _type = value; }
		}

		public string UserName
		{
			get { return _userName; }
			set { _userName = value; }
		}

		public string Password
		{
			get { return _password; }
			set { _password = value; }
		}

		public DateTime LastUsed
		{
			get { return _lastUsed; }
			set { _lastUsed = value; }
		}

		public string LinkName
		{
			get { return _linkName; }
			set { _linkName = value; }
		}

        public bool? Verified
        {
            get { return _isVerified; }
            set { _isVerified = value; }
        }

        public Dictionary<string, string> MerchantLinks
		{
			get { return _merchantLinks; }
			set { _merchantLinks = value; }
		}

        public int[] Partners
        {
			get { return _Partners; }
			set { _Partners = value; }
        }

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(base.ToString());
			sb.AppendLine("ID: " + ID.ToString());
			sb.AppendLine("Name: " + Name);
			sb.AppendLine("User Name: " + UserName);
			sb.AppendLine("Type: " + Type.ToString() + " (" + (int)Type + ")");
			if (AccessList != null) sb.AppendLine("Access List: " + AccessList.Select(so => so.Name).ToList().ToDelimitedString());
			sb.AppendLine("Allowed Merchants: " + AllowedMerchantsIDs.ToDelimitedString());
			sb.AppendLine("Allowed Banks: " + AllowedBanksIDs.ToDelimitedString());
			sb.AppendLine("Merchant Links: " + MerchantLinks.ToDelimitedString());
			if (PayPercent != null) sb.AppendLine("Pay Percent: " + PayPercent.ToString());
			if (_linkName != null) sb.AppendLine("LinkKey: " + LinkName);
			return sb.ToString();
		}
	}
}
