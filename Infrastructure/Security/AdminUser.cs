﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Netpay.Infrastructure.Security
{
    public class AdminUser : BaseDataObject
    {
        public const string SecuredObjectName = "AdminUsers";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return SecuredObject.Get(SecurityModule.Current, SecuredObjectName); } }


        public class SearchFilters
        {
            public bool? MustHaveLoginDetails;
            public Range<int?> ID;
            public Range<DateTime?> StartDate;
            public bool? IsAdmin;
            public int? GroupID;
            public string Text;
        }

        private Dal.Netpay.AdminUser _entity;

        public short ID { get { return _entity.AdminUser_id; } }
        public string FullName { get { return _entity.FullName; } set { _entity.FullName = value; } }
        public string NotifyEmail { get { return _entity.NotifyEmail; } set { _entity.NotifyEmail = value; } }

        public string LoginEmailAddress { get { return Login.EmailAddress; } }

        public string NotifyCellPhone { get { return _entity.NotifyCellPhone; } set { _entity.NotifyCellPhone = value; } }
        public string ProfileImageURL { get { return _entity.ProfileImageURL; } set { _entity.ProfileImageURL = value; } }
        public bool IsAdmin { get { return _entity.IsAdmin; } set { _entity.IsAdmin = value; } }
        public System.DateTime? StartDate { get { return _entity.StartDate; } }
        public string MultiFactorMode { get { return _entity.MultiFactorMode; } set { _entity.MultiFactorMode = value; } }
        public bool? IsActive { get { return _entity.IsActive; } set { _entity.IsActive = value; } }

        private HashSet<Group> _groups = null;
        public HashSet<Group> Groups
        {
            get
            {
                //Security permission invoked inside "Group" class.

                if (_groups != null) return _groups;
                _groups = Group.LoadForAdminUser(this);
                return _groups;
            }
            set
            {
                _groups = value;
            }
        }

        public bool IsInGroup(string groupName) { return Groups.Any(v => v.Name == groupName); }

        private AdminUser(Dal.Netpay.AdminUser entity)
        {
            _entity = entity;
        }

        public AdminUser()
        {
            if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins) throw new MethodAccessDeniedException();
            _entity = new Dal.Netpay.AdminUser();
        }

        public static AdminUser Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from u in DataContext.Reader.AdminUsers where u.AdminUser_id == id select new AdminUser(u)).SingleOrDefault();
        }

        public static AdminUser LoadForLogin(int id)
        {
            //Permission check causes stackoverflow exception.           
            return (from u in DataContext.Reader.AdminUsers where u.LoginAccount_id == id select new AdminUser(u)).SingleOrDefault();
        }

        public void Save()
        {
            if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins) throw new MethodAccessDeniedException();

            if (_login != null)
            {
                _login.Save();
                _entity.LoginAccount_id = _login.LoginID;
            }
            DataContext.Writer.AdminUsers.Update(_entity, (_entity.AdminUser_id != 0));
            DataContext.Writer.SubmitChanges();
            if (_groups != null) Group.SaveForAdminUser(this, _groups);
            Domain.Current.RemoveCachData("Security.AdminUser");
        }

        public void Delete()
        {
            if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins) throw new MethodAccessDeniedException();

            // Get the Login object that needs to be deleted (before deleting AdminUsers entry).
            Login loginToDelete = null;
            if (_entity.LoginAccount_id != null)
                loginToDelete = Login;

            // Due to DB constraint need to remove AdminUsers entry before deleting the Login
            if (_entity.AdminUser_id != 0) DataContext.Writer.AdminUsers.Delete(_entity);
            DataContext.Writer.SubmitChanges();
            Domain.Current.RemoveCachData("Security.AdminUser");

            // Remove from DB corresponding entries to LoginAccount_id in LoginAccounts and LoginPasswords tables
            if (loginToDelete != null)
                loginToDelete.Delete();
        }

        public static List<AdminUser> Cache
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("Security.AdminUser", () =>
                {
                    return new Domain.CachData((from g in DataContext.Reader.AdminUsers select new AdminUser(g)).ToList(), DateTime.Now.AddHours(6));
                }) as List<AdminUser>;
            }
        }

        public static string GetAdminUserIdByName(string name)
        {
            if (Cache.Any(x => x.FullName == name))
            {
                return Cache.Where(x => x.FullName == name).SingleOrDefault().ID.ToString();
            }
            else
            {
                return "0";
            }
        }

        public static AdminUser Current
        {
            get
            {
                //Check permission here will cause StackOverFlow exception.

                var login = Login.Current;
                if (login == null) return null;
                if (login.Role != UserRole.Admin) return null;
                object obj;
                if (!login.Items.TryGetValue("AdminUser", out obj))
                {
                    var ret = AdminUser.LoadForLogin(login.LoginID);
                    login.Items["AdminUser"] = obj = ret;
                }
                return obj as AdminUser;
            }
        }

        public bool IsWinServiceUser
        {
            get
            {
                return (Login != null &&
                        Login.UserName == Domain.Current.ServiceUser &&
                        Login.EmailAddress == Domain.Current.ServiceUser &&
                        Login.GetPassword(Login.LoginID, null) == Domain.Current.ServiceUserPassword);
            }
        }

        private Login _login;
        public Login Login
        {
            get
            {
                //Security check is invoked inside "Login" class.

                if (_login != null) return _login;
                if (_entity.LoginAccount_id == null) _login = new Security.Login(UserRole.Admin);
                else _login = Login.LoadLogin(_entity.LoginAccount_id.Value);
                return _login;
            }
        }

        public static List<AdminUser> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from g in DataContext.Reader.AdminUsers select g);
            if (filters != null)
            {
                if (filters.MustHaveLoginDetails.HasValue && filters.MustHaveLoginDetails.Value) exp = exp.Where(x => x.LoginAccount_id.HasValue);
                if (filters.ID.From.HasValue) exp = exp.Where((g) => g.AdminUser_id >= filters.ID.From.Value);
                if (filters.ID.To.HasValue) exp = exp.Where((g) => g.AdminUser_id <= filters.ID.To.Value);
                if (filters.IsAdmin.HasValue) exp = exp.Where((g) => g.IsAdmin == filters.IsAdmin.Value);
                if (filters.GroupID.HasValue) exp = exp.Where(u => (from g in DataContext.Reader.AdminUserToAdminGroups where g.AdminGroup_id == filters.GroupID.Value select g.AdminUser_id).Contains(u.AdminUser_id));
            }
            return exp.ApplySortAndPage(sortAndPage)
                .Select(g => new AdminUser(g))
                .ToList()
                .Where(x => !x.IsWinServiceUser)
                .ToList();
        }

        public static string MapPrivatePath(int? userId, string fileName = null)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            string path = Domain.Current.MapPrivateDataPath("AdminUser Files");
            if (userId != null) path = System.IO.Path.Combine(path, userId.ToString() + "/");
            if (fileName != null) path = System.IO.Path.Combine(path, fileName);
            //var dirName = System.IO.Path.GetDirectoryName(path);
            //if (!System.IO.Directory.Exists(dirName)) System.IO.Directory.CreateDirectory(dirName);
            return path;
        }

        public string MapPrivatePath(string fileName = null)
        {
            return MapPrivatePath(ID, fileName);
        }

        public static string GetAdminUserNameById(int id)
        {
            if (!Cache.Any(x => x.ID == id))
            {
                return "";
            }

            return Cache.Where(x => x.ID == id).SingleOrDefault().FullName;
        }

        public void SendAdminUserMail(string templateName, object dataObj = null)
        {
            var template = Infrastructure.Email.Template.Get(templateName);
            if (template == null) throw new Exception("Email Template not found " + templateName);
            if (dataObj == null) dataObj = GetEmailData();
            template.Send(dataObj, null, new System.Net.Mail.MailAddress(Login.EmailAddress, FullName));
        }

        public void SendAdminUserResetPasswordMail()
        {
            SendAdminUserMail("AdminUser_ResetPassword.htm");
        }

        public object GetEmailData()
        {
            string password = null;
            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            try
            {
                password = Login.GetPassword(Login.LoginID, null);
            }
            finally
            {
                ObjectContext.Current.StopImpersonate();
            }
            return new { Domain = Domain.Current, AdminUser = this, AdminUserPassword = password };
        }
    }
}
