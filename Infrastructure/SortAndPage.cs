﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure
{
	public struct Range<T>
	{
		public T From; public T To;
		public Range(T from, T to) { From = from; To = to; }
		public Range(T value) { From = value; To = value; }
		public static Range<T> FromValue(T value) { return new Range<T>(value); }
		public override string ToString() { return string.Format("{0} - {1}", From, To); }
	}
    public struct Pair<T1, T2> { public T1 First; public T2 Second; public Pair(T1 first, T2 second) { First = first; Second = second; } }

	public interface ISortAndPage
	{
		string SortKey { get; set; }
        bool SortDesc { get; set; }
		int RowCount { get; set; }
		int PageSize { get; }
		int PageCurrent { get; set; }
		//bool CountOnly { get; set; }
		Func<ISortAndPage, System.Collections.IEnumerable> DataFunction { get; set; }		
	}

	public interface IHasSortAndPage
	{
		ISortAndPage SortAndPage { get; }
	}

	[Serializable]
	public class SortAndPage : ISortAndPage
	{
		public SortAndPage(int page, int pageSize, string sortKey = null, bool sortDesc = false) { PageCurrent = page; PageSize = pageSize; SortKey = sortKey; SortDesc = sortDesc; }
		public SortAndPage() { PageSize = 10; }
		public string SortKey { get; set; }
		public bool SortDesc { get; set; }
		public int RowCount { get; set; }
		public int PageSize { get; set; }
		public int PageCurrent { get; set; }
		public int PageCount { get { return (int)System.Math.Ceiling((double)RowCount / (double)PageSize); } }
		//public bool CountOnly { get; set; }
		public Func<ISortAndPage, System.Collections.IEnumerable> DataFunction { get; set; }

		public int RowFrom { get { return (PageCurrent * PageSize) + 1; } }
		public int RowTo { get { return System.Math.Min(RowCount, ((PageCurrent + 1) * PageSize)); } }
        public SortAndPage CopySortFrom(ISortAndPage psort) 
        {
            if (psort == null) return this;
            SortKey = psort.SortKey;
            SortDesc = psort.SortDesc;
            return this;
        }
	
	}

	public static partial class Extensions
	{
		public static void SearchText<T>(this IEnumerable<T> source, string text, Action<string> proc)
		{
			if (string.IsNullOrEmpty(text)) return;
			var words = text.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
			foreach (var word in words) proc(word);
		}

		public static IOrderedQueryable<T> ApplyOrder<T>(this IQueryable<T> source, string property, string methodName)
		{
			string[] props = property.Split('.');
			Type type = typeof(T);
			System.Linq.Expressions.ParameterExpression arg = System.Linq.Expressions.Expression.Parameter(type, "x");
			System.Linq.Expressions.Expression expr = arg;
			foreach (string prop in props)
			{
				// use reflection (not ComponentModel) to mirror LINQ
				System.Reflection.PropertyInfo pi = type.GetProperty(prop);
				expr = System.Linq.Expressions.Expression.Property(expr, pi);
				type = pi.PropertyType;
			}
			Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
			System.Linq.Expressions.LambdaExpression lambda = System.Linq.Expressions.Expression.Lambda(delegateType, expr, arg);

			object result = typeof(Queryable).GetMethods().Single(
					method => method.Name == methodName
							&& method.IsGenericMethodDefinition
							&& method.GetGenericArguments().Length == 2
							&& method.GetParameters().Length == 2)
					.MakeGenericMethod(typeof(T), type)
					.Invoke(null, new object[] { source, lambda });
			return (IOrderedQueryable<T>)result;
		}

        public static IList<T> ApplyOrder<T>(this IList<T> source, string property, string methodName)
        {
            string[] props = property.Split('.');

            IOrderedEnumerable<T> orderedList = null;
            foreach (string prop in props)
            {
                if (orderedList == null)
                {
                    if (methodName == "OrderByDescending")
                        orderedList = source.OrderByDescending(i => i.GetType()
                                            .GetProperty(prop)
                                            .GetValue(i, null));
                    else
                        orderedList = source.OrderBy(i => i.GetType()
                                            .GetProperty(prop)
                                            .GetValue(i, null));
                }
                else
                {
                    if (methodName == "OrderByDescending")
                        orderedList = orderedList.ThenByDescending(i => i.GetType()
                                            .GetProperty(prop)
                                            .GetValue(i, null));
                    else
                        orderedList = orderedList.ThenBy(i => i.GetType()
                                            .GetProperty(prop)
                                            .GetValue(i, null));
                }
            }

            return orderedList.ToList();
        }

        public static IQueryable<T> ApplySortAndPage<T>(this IQueryable<T> source, ISortAndPage sortAndPage, string fieldPrefix = "")
		{
			if (sortAndPage != null)
			{
				if (!string.IsNullOrEmpty(sortAndPage.SortKey)) source = source.ApplyOrder(fieldPrefix + sortAndPage.SortKey, sortAndPage.SortDesc ? "OrderByDescending" : "OrderBy");
				if (sortAndPage.PageSize <= 0) return source;
                bool countOnly = sortAndPage.RowCount == int.MinValue;
				sortAndPage.RowCount = source.Count();
                if (countOnly) return new List<T>().AsQueryable();
				source = source.Skip(sortAndPage.PageSize * sortAndPage.PageCurrent).Take(sortAndPage.PageSize);
			}
			return source;
		}

        public static IList<T> ApplySortAndPage<T>(this IList<T> source, ISortAndPage sortAndPage, string fieldPrefix = "")
        {
            if (sortAndPage != null)
            {
                if (!string.IsNullOrEmpty(sortAndPage.SortKey)) source = source.ApplyOrder(fieldPrefix + sortAndPage.SortKey, sortAndPage.SortDesc ? "OrderByDescending" : "OrderBy");
                if (sortAndPage.PageSize <= 0) return source;
                bool countOnly = sortAndPage.RowCount == int.MinValue;
                sortAndPage.RowCount = source.Count();
                if (countOnly) return new List<T>();
                source = source
                            .Skip(sortAndPage.PageSize * sortAndPage.PageCurrent)
                            .Take(sortAndPage.PageSize)
                            .ToList();
            }
            return source;
        }


        public static System.Data.DataTable ToDataTable<T>(this IList<T> data)
		{
			System.ComponentModel.PropertyDescriptorCollection props = System.ComponentModel.TypeDescriptor.GetProperties(typeof(T));
			System.Data.DataTable table = new System.Data.DataTable();
			for (int i = 0; i < props.Count; i++)
			{
				System.ComponentModel.PropertyDescriptor prop = props[i];
				table.Columns.Add(prop.Name, prop.PropertyType);
			}
			object[] values = new object[props.Count];
			foreach (T item in data)
			{
				for (int i = 0; i < values.Length; i++)
					values[i] = props[i].GetValue(item);
				table.Rows.Add(values);
			}
			return table;
		}

		public static void Detach<T>(this System.Data.Linq.Table<T> table, T entity) where T : class
		{
			typeof(T).GetMethod("Initialize", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).Invoke(entity, null);
		}

		public static void Insert<T>(this System.Data.Linq.Table<T> table, T entity) where T : class
		{
			table.InsertOnSubmit(entity);
		}

		public static void EnsureAttached<T>(this System.Data.Linq.Table<T> table, T entity, bool refresh = false) where T:class
		{
			if (table.GetOriginalEntityState(entity) == null)
				table.Attach(entity);
			if (refresh)
				table.Context.Refresh(System.Data.Linq.RefreshMode.KeepCurrentValues, entity);
		}

		public static void Update<T>(this System.Data.Linq.Table<T> table, T entity, bool rowExist) where T : class
		{
			if (rowExist) EnsureAttached(table, entity, true);
			else Insert(table, entity);
			Detach(table, entity);
		}

		public static void Delete<T>(this System.Data.Linq.Table<T> table, T entity) where T : class
		{
			EnsureAttached(table, entity, true);
			table.DeleteOnSubmit(entity);
			Detach(table, entity);
		}
	}
}
