﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using System.Data.Linq.Mapping;
using Netpay.Infrastructure.Security;

namespace Netpay.Infrastructure
{
    /// <summary>
    /// Contains logging logic.
    /// </summary>
    [Table(Name = "tblBllLog")]
    public class Logger : BaseDataObject
    {
        public class SearchFilters
        {
            public Infrastructure.Range<DateTime?> Date;
            public LogSeverity? Severity;
            public string Tag;
            public string Text;

        }

        public class LogHandle
        {
            public LogSeverity Severity { get; set; }
            public LogTag Tag { get; set; }
            public string MailTo { get; set; }
        }

        public static List<LogHandle> LogHandlers;

        //private Dal.ErrorNet.tblBllLog _entity;

        [Column(AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true)]
        public int ID { get; private set; }

        [Column(DbType = "DateTime NOT NULL")]
        public System.DateTime InsertDate { get; private set; }

        [Column(DbType = "TinyInt NOT NULL", Name = "SeverityID")]
        public LogSeverity Severity { get; private set; } //{ return (LogSeverity) _entity.SeverityID; }

        [Column(DbType = "NVarChar(200)")]
        public string Tag { get; private set; }

        [Column(DbType = "NVarChar(500)")]
        public string Source { get; private set; }

        [Column(DbType = "NVarChar(500) NOT NULL", CanBeNull = false)]
        public string Message { get; private set; }

        [Column(DbType = "NVarChar(MAX)")]
        public string LongMessage { get; private set; }

        public static string LogsPhisicalBasePath { get { return Application.GetParameter("logsPhisicalBasePath", @"c:\temp\"); } }
        public Logger() { } /*forced by linq activator*/

        private static IQueryable<Logger> GetLogs(System.Data.Linq.DataContext dc, SearchFilters filters)
		{
            var exp = from l in dc.GetTable<Logger>() select l;
			if (filters.Date.From.HasValue) exp = exp.Where(l => l.InsertDate >= filters.Date.From);
			if (filters.Date.To.HasValue) exp = exp.Where(l => l.InsertDate <= filters.Date.To);
			if (filters.Severity.HasValue) exp = exp.Where(l => l.Severity == filters.Severity);
			if (filters.Tag != null) exp = exp.Where(l => l.Tag.ToLower() == filters.Tag.ToLower());
            if (!string.IsNullOrEmpty(filters.Text)) exp = exp.Where(l => l.Message.Contains(filters.Text) || l.LongMessage.Contains(filters.Text));
			return exp;
		}

		public static List<Logger> GetLogs(SearchFilters filters, ISortAndPage sortAndPage)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			if (sortAndPage == null) sortAndPage = new SortAndPage(0, 100, "InsertDate", true);
            if (string.IsNullOrEmpty(sortAndPage.SortKey)) { sortAndPage.SortKey = "InsertDate"; sortAndPage.SortDesc = true; }
            using (var dc = new System.Data.Linq.DataContext(Application.GetParameter("errorNetConnectionString")))
                return GetLogs(dc, filters).ApplySortAndPage(sortAndPage).ToList();
        }

		public static void DeleteLogs()
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
            using (var dc = new System.Data.Linq.DataContext(Application.GetParameter("errorNetConnectionString")))
                dc.ExecuteCommand("DELETE FROM [Errors].[dbo].[tblBllLog]");
		}

		public static void DeleteLogs(SearchFilters filters)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			using (var dc = new System.Data.Linq.DataContext(Application.GetParameter("errorNetConnectionString")))
			{
				dc.GetTable<Logger>().DeleteAllOnSubmit(GetLogs(dc, filters));
				dc.SubmitChanges();
			}
		}

        public static void Init(System.Xml.XmlElement element)
		{
            LogHandlers = new List<Logger.LogHandle>();
            foreach (System.Xml.XmlElement v in element.GetElementsByTagName("notify"))
            {
                Logger.LogHandle logHandle = new Logger.LogHandle();
                logHandle.Severity = (LogSeverity)Enum.Parse(typeof(LogSeverity), v.GetAttribute("severity"), true);
                logHandle.Tag = (LogTag)Enum.Parse(typeof(LogTag), v.GetAttribute("tag"), true);
                logHandle.MailTo = v.GetAttribute("mailTo");
                LogHandlers.Add(logHandle);
            }
		}

        public static void Log(LogTag tag, Exception ex, string moreInfo = null, bool execHandlers = true)
		{
            var longData = new System.Text.StringBuilder();
            if (moreInfo != null) longData.AppendLine(moreInfo);
            var curEx = ex;
            while (curEx != null) {
                if (curEx != ex) longData.AppendLine("----------------------INNER-----------------");
                longData.AppendLine(curEx.ToString());
                curEx = curEx.InnerException;
            }
            Log(LogSeverity.Error, tag, ex.Message, longData.ToString(), execHandlers);
			ErrorNet.Write(ex, tag);
		}

        public static void Log(Exception ex, string moreInfo = null, bool execHandlers = true)
        {
            Log(LogTag.Exception, ex, moreInfo, execHandlers);
        }

        public static void Log(LogTag tag, string message, bool execHandlers = true)
        {
            Log(LogSeverity.Info, tag, message, null, execHandlers);
        }
        
        public static void Log(string message, bool execHandlers = true)
		{
			Log(LogSeverity.Info, LogTag.None, message, null, execHandlers);
		}

		public static void Log(LogSeverity severity, LogTag tag, string message, bool execHandlers = true)
		{
			Log(severity, tag, message, null, execHandlers);
		}

		public static void Log(LogSeverity severity, LogTag tag, string message, string longMessage, bool execHandlers = true)
		{

			var log = new Logger();
			log.Source = Application.InstanceName;
			log.InsertDate = DateTime.Now;
			log.Message = message.Truncate(400).ToSql(true);
			log.Severity = severity;
            log.LongMessage = longMessage.Truncate(8192).ToSql(true);
			log.Tag = tag.ToString();
            
			if (severity == LogSeverity.Error)
				log.LongMessage += GetWebContextInfo().ToSql(true);
			try
			{
                using (var dc = new System.Data.Linq.DataContext(Application.GetParameter("errorNetConnectionString")))
                {
                    dc.GetTable<Logger>().InsertOnSubmit(log);
                    dc.SubmitChanges();
                }
			}
			catch (Exception ex)
			{
				// write to file if db fails
                string currentLogFile = System.IO.Path.Combine(LogsPhisicalBasePath, DateTime.Now.ToString("dd-MM-yyyy") + ".log");
				StringBuilder logContent = new StringBuilder();
                logContent.AppendLine();
                logContent.AppendLine("=================== log string =========================");
				logContent.AppendLine(DateTime.Now.ToString());
				logContent.AppendLine(log.Source);
				logContent.AppendLine(severity.ToString());
				logContent.AppendLine(tag.ToString());
				logContent.AppendLine(log.Message);
				logContent.AppendLine(log.LongMessage);
				logContent.AppendLine("========================================================");
                logContent.AppendLine("Could not save to BllLog: " + ex.Message);
				logContent.AppendLine("========================================================");

                try {
                    if (!Directory.Exists(LogsPhisicalBasePath)) Directory.CreateDirectory(LogsPhisicalBasePath);
                    File.AppendAllText(currentLogFile, logContent.ToString()); 
                }
                catch (Exception exx) {
                    if (HttpContext.Current != null) {
                        logContent.AppendLine("Could not save to " + currentLogFile + ": " + exx.Message);
                        logContent.AppendLine("========================================================");
                        HttpContext.Current.Response.Write(logContent.ToString());
                    }
                }
			}

			try
			{
				if (execHandlers) LogHanlde(severity, tag, message, longMessage);
			}
			catch { }
		}

		public static void LogHanlde(LogSeverity severity, LogTag tag, string message, string longMessage)
		{
			if (LogHandlers == null) 
				return;

            var logHandleList = LogHandlers.Where(l => (l.Severity == severity && l.Tag == tag));
			if (logHandleList != null)
			{
				string serverName = "";
                if (Domain.Current != null) serverName = Domain.Current.Host;
                else { 
				    try { serverName = (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null) ? System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] : ""; } catch { };
                }
                string emailBody = string.Format("DateTime: {0}<br/>Instance: {1}<br/>Server Name: {2}<br/>Severity: {3}<br/>Tag: {4}<br/>Message: {5}<br/>Long Message: {6}<br/>", new object[] { DateTime.Now, Application.InstanceName, Environment.MachineName, severity.ToString(), tag.ToString(), message, longMessage });
				foreach (var currentHandler in logHandleList)
                    Netpay.Infrastructure.Email.SmtpClient.Send(null, Application.MailAddressFrom, currentHandler.MailTo, string.Format("Error {0} [{1}] {2}", Application.InstanceName, serverName, tag.ToString()), emailBody);
			}
		}

        [Table(Name = "tblErrorNet")]
        private class ErrorNet
        {
            [Column(AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true)]
            public int ID { get; private set; }
            [Column(DbType = "DateTime NOT NULL")]
            public System.DateTime ErrorTime { get; set; }
            [Column(DbType = "NVarChar(25) NOT NULL", CanBeNull = false)]
            public string ProjectName { get; set; }
            [Column(DbType = "NVarChar(25) NOT NULL", CanBeNull = false)]
            public string RemoteIP { get; set; }
            [Column(DbType = "NVarChar(25) NOT NULL", CanBeNull = false)]
            public string LocalIP { get; set; }
            [Column(DbType = "NVarChar(50) NOT NULL", CanBeNull = false)]
            public string RemoteUser { get; set; }
            [Column(DbType = "NVarChar(50) NOT NULL", CanBeNull = false)]
            public string ServerName { get; set; }
            [Column(DbType = "NVarChar(5) NOT NULL", CanBeNull = false)]
            public string ServerPort { get; set; }
            [Column(DbType = "NVarChar(50) NOT NULL", CanBeNull = false)]
            public string ScriptName { get; set; }
            [Column(DbType = "NVarChar(500) NOT NULL", CanBeNull = false)]
            public string RequestQueryString { get; set; }
            [Column(DbType = "NVarChar(50) NOT NULL", CanBeNull = false)]
            public string VirtualPath { get; set; }
            [Column(DbType = "NVarChar(100) NOT NULL", CanBeNull = false)]
            public string PhysicalPath { get; set; }
            [Column(DbType = "NVarChar(100) NOT NULL", CanBeNull = false)]
            public string ExceptionSource { get; set; }
            [Column(DbType = "NVarChar(500) NOT NULL", CanBeNull = false)]
            public string ExceptionMessage { get; set; }
            [Column(DbType = "NVarChar(100) NOT NULL", CanBeNull = false)]
            public string ExceptionTargetSite { get; set; }
            [Column(DbType = "NVarChar(1000) NOT NULL", CanBeNull = false)]
            public string ExceptionStackTrace { get; set; }
            [Column(DbType = "NVarChar(100) NOT NULL", CanBeNull = false)]
            public string ExceptionHelpLink { get; set; }
            [Column(DbType = "Int NOT NULL")]
            public int ExceptionLineNumber { get; set; }
            [Column(DbType = "NVarChar(100) NOT NULL", CanBeNull = false)]
            public string InnerExceptionSource { get; set; }
            [Column(DbType = "NVarChar(500) NOT NULL", CanBeNull = false)]
            public string InnerExceptionMessage { get; set; }
            [Column(DbType = "NVarChar(1000) NOT NULL", CanBeNull = false)]
            public string InnerExceptionTargetSite { get; set; }
            [Column(DbType = "NVarChar(100) NOT NULL", CanBeNull = false)]
            public string InnerExceptionHelpLink { get; set; }
            [Column(DbType = "Int NOT NULL")]
            public int InnerExceptionLineNumber { get; set; }
            [Column(DbType = "Bit NOT NULL")]
            public bool IsFailedSQL { get; set; }
            [Column(DbType = "Bit NOT NULL")]
            public bool IsArchive { get; set; }
            [Column(DbType = "NText NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
            public string InnerExceptionStackTrace { get; set; }
            [Column(DbType = "VarChar(500) NOT NULL", CanBeNull = false)]
            public string RequestForm { get; set; }
            [Column(DbType = "Bit NOT NULL")]
            public bool IsHighlighted { get; set; }


            public static void Write(Exception exception, LogTag tag)
            {

                var context = HttpContext.Current;
                var entity = new ErrorNet();
                entity.ProjectName = Application.InstanceName.EmptyIfNull().Truncate(25);
                entity.ErrorTime = DateTime.Now;

                if (context != null && context.Request != null)
                {
                    if (context.Request.ServerVariables != null)
                    {
                        entity.RemoteIP = context.Request.ServerVariables["REMOTE_ADDR"].Truncate(25);
                        entity.LocalIP = context.Request.ServerVariables["LOCAL_ADDR"].Truncate(25);
                        entity.RemoteUser = context.Request.ServerVariables["REMOTE_USER"].Truncate(50);
                        entity.ServerName = context.Request.ServerVariables["SERVER_NAME"].Truncate(50);
                        entity.ServerPort = context.Request.ServerVariables["SERVER_PORT"].Truncate(5);
                        entity.ScriptName = context.Request.ServerVariables["SCRIPT_NAME"].Truncate(50);
                        entity.VirtualPath = context.Request.ServerVariables["PATH_INFO"].Truncate(50);
                        entity.PhysicalPath = context.Request.ServerVariables["PATH_TRANSLATED"].Truncate(100);
                    }

                    if (context.Request.QueryString != null)
                        entity.RequestQueryString = context.Request.QueryString.ToString().Truncate(500);

                    if (context.Request.Form != null)
                        entity.RequestForm = context.Request.Form.ToString().Truncate(500);
                }
                else
                    entity.RemoteIP = entity.LocalIP = entity.RemoteUser = entity.ServerName = entity.ServerPort = entity.ScriptName = entity.RequestQueryString = entity.RequestForm = entity.VirtualPath = entity.PhysicalPath = "";

                if (exception != null)
                {
                    entity.ExceptionTargetSite = "";
                    entity.ExceptionSource = exception.Source.EmptyIfNull().Truncate(100);
                    entity.ExceptionMessage = exception.Message.EmptyIfNull().Truncate(500);
                    entity.ExceptionStackTrace = exception.StackTrace.EmptyIfNull().Truncate(1000);
                    entity.ExceptionHelpLink = exception.HelpLink.EmptyIfNull().Truncate(100);

                    entity.InnerExceptionTargetSite = "";
                    if (exception.InnerException != null)
                    {
                        entity.InnerExceptionSource = exception.InnerException.Source.EmptyIfNull().Truncate(100);
                        entity.InnerExceptionMessage = exception.InnerException.Message.EmptyIfNull().Truncate(500);
                        entity.InnerExceptionStackTrace = exception.InnerException.StackTrace.EmptyIfNull().Truncate(1000);
                        entity.InnerExceptionHelpLink = exception.InnerException.HelpLink.EmptyIfNull().Truncate(100);
                    }
                    else
                        entity.InnerExceptionSource = entity.InnerExceptionMessage = entity.InnerExceptionTargetSite = entity.InnerExceptionStackTrace = entity.InnerExceptionHelpLink = "";
                }
                try
                {
                    using (var dc = new System.Data.Linq.DataContext(Application.GetParameter("errorNetConnectionString")))
                    {
                        dc.GetTable<ErrorNet>().InsertOnSubmit(entity);
                        dc.SubmitChanges();
                    }
                }
                catch (Exception ex)
                {
                    // write to file if db fails
                    string currentLogFile = System.IO.Path.Combine(LogsPhisicalBasePath, DateTime.Now.ToString("dd-MM-yyyy") + ".log");
                    StringBuilder logContent = new StringBuilder();
                    logContent.AppendLine();
                    logContent.AppendLine("=================== log string =========================");
                    logContent.AppendLine(DateTime.Now.ToString());
                    logContent.AppendLine(entity.ExceptionSource);
                    logContent.AppendLine(entity.ExceptionMessage);
                    logContent.AppendLine(entity.ExceptionStackTrace);
                    logContent.AppendLine(entity.ExceptionMessage);
                    logContent.AppendLine(entity.InnerExceptionMessage);
                    logContent.AppendLine("========================================================");
                    logContent.AppendLine("Could not save to ErrorNet: " + ex.Message);
                    logContent.AppendLine("========================================================");
                    try
                    {
                        if (!Directory.Exists(LogsPhisicalBasePath)) Directory.CreateDirectory(LogsPhisicalBasePath);
                        File.AppendAllText(currentLogFile, logContent.ToString());
                    }
                    catch (Exception exx)
                    {
                        if (HttpContext.Current != null)
                        {
                            logContent.AppendLine("Could not save to " + currentLogFile + ": " + exx.Message);
                            logContent.AppendLine("========================================================");
                            HttpContext.Current.Response.Write(logContent.ToString());
                        }
                    }
                }
            }
        }

		/// <summary>
		/// Adds web context info such as session, query and form values, if the context exists.
		/// </summary>
		private static string GetWebContextInfo()
		{
			StringBuilder sb = new StringBuilder("\n\n");
			if (System.Web.HttpContext.Current != null)
			{
				HttpContext context = HttpContext.Current;

				// source
				sb.AppendLine();
				sb.AppendFormat("Authentication: {0} \n", context.Request.ServerVariables["AUTH_USER"]);
				sb.AppendFormat("Url: {0} \n", context.Request.Url);
				sb.AppendFormat("Client IP: {0} \n", context.Request.ServerVariables["REMOTE_ADDR"]);

				// header vals
				sb.AppendLine();
				sb.AppendLine("Headers:");
				if (context.Request.Headers != null && context.Request.Headers.Count > 0)
					foreach (string currentKey in context.Request.Headers.Keys)
						sb.AppendFormat("{0}: {1} \n", currentKey, context.Request.Headers[currentKey]);

				// cookie vals
				//sb.AppendLine();
				//sb.AppendLine("Cookie:");
				//if (context.Request.Cookies != null && context.Request.Cookies.Count > 0)
				//foreach (HttpCookie currentCookie in context.Request.Cookies)
				//sb.AppendFormat("{0}: {1} \n", currentCookie.Name, currentCookie.Value);

				// query vals
				sb.AppendLine();
				sb.AppendLine("Query:");
				if (context.Request.QueryString != null && context.Request.QueryString.Count > 0)
					foreach (string currentKey in context.Request.QueryString.Keys)
						sb.AppendFormat("{0}: {1} \n", currentKey, context.Request.QueryString[currentKey]);

				// form vals
				sb.AppendLine();
				sb.AppendLine("Form:");
				if (context.Request.Form != null && context.Request.Form.Count > 0)
					foreach (string currentKey in context.Request.Form.Keys)
					{
						if (currentKey != null && currentKey.ToLower().Contains("password"))
							sb.AppendFormat("{0}: {1} \n", currentKey, "********");
						else
							sb.AppendFormat("{0}: {1} \n", currentKey, context.Request.Form[currentKey]);
					}

				// session vals
				sb.AppendLine();
				sb.AppendLine("Session:");
				if (context.Session != null && context.Session.Count > 0)
					foreach (string currentKey in context.Session)
						sb.AppendFormat("{0}: {1} \n", currentKey, context.Session[currentKey]);

				// user
				sb.AppendLine();
				sb.AppendLine("User:");
				if (HttpContext.Current.Session != null)
				{
					Guid creds;
					string credentialsToken = (string)HttpContext.Current.Session["credentialsToken"];
					if (credentialsToken == null || credentialsToken.Trim() == "")
						creds = Guid.Empty;
					else
						creds = new Guid(credentialsToken);

					Login user = Login.Current;
					if (user != null) sb.Append(user.ToString());
				}
			}

			return sb.ToString();
		}

		/*
		public static void Log(string text)
		{
			lock (typeof(Logger))
			{
				string currentLogFilePath = string.Format("{0}{1}.log", _logBasePath, DateTime.Now.ToString("dd_MM_yyyy"));

				StringBuilder logBuilder = new StringBuilder();
				logBuilder.AppendLine(string.Format("[{0}]", DateTime.Now.ToString()));
				logBuilder.AppendLine(text);
				logBuilder.AppendLine();

				try
				{
					File.AppendAllText(currentLogFilePath, logBuilder.ToString());
				}
				catch
				{
				}
			}
		}
		*/
	}
}
