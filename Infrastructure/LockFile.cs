﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure
{
    public class LockFile
    {
        public const string FILESIGNATURE = "LOCK v1.0";
        public bool IsActive { get; private set; }
        public DateTime UpdateTme { get; private set; }
        public string MachineName { get; private set; }

        public static string MapLockFile(string lockName)
        {
            return Domain.Current.MapPrivateDataPath("Locks/" + lockName + ".lock");
        }

        private LockFile(System.IO.Stream dataStream)
        {
            if (dataStream == null) return;
            int idx = 0;
            byte[] dataRead = new byte[128];
            var readCount = dataStream.Read(dataRead, 0, System.Math.Min(dataRead.Length, (int)dataStream.Length));
            var data = System.Text.Encoding.UTF8.GetString(dataRead, 0, readCount);
            foreach (var s in data.TrimEnd().Split('|'))
            {
                switch (idx)
                {
                    case 0: if (!s.StartsWith(FILESIGNATURE)) throw new Exception("lock file invalid format, should begin with:" + FILESIGNATURE); break;
                    case 1: IsActive = s.ToNullableBool().GetValueOrDefault(false); break;
                    case 2: UpdateTme = s.ToNullableDate().GetValueOrDefault(DateTime.MinValue); break;
                    case 3: MachineName = s; break;
                }
                idx++;
            }

        }

        public static LockFile Load(string fileName)
        {
            LockFile ret = null;
            int tryCount = 0;
            Exception lastExp = null;
            do {
                if (++tryCount >= 3) break; 
                try {
                    using (var f = System.IO.File.Open(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
                    {
                        try { ret = new LockFile(f); }
                        finally { f.Close(); }
                    }
                    return ret;
                } catch (System.IO.FileNotFoundException) { 
                    return null; 
                } catch (System.IO.IOException ex) {
                    lastExp = ex;
                    System.Threading.Thread.Sleep(200);
                }
            } while (true);
            throw new Exception("Unable to read FileLock " + fileName, lastExp);
        }

        private static bool TryUpdate(string fileName, bool isActive, DateTime? minActiveDate)
        {
            string curMachineName = System.Environment.MachineName.TruncEnd(100);
            int tryCount = 0;
            Exception lastExp = null;
            var dirName = System.IO.Path.GetDirectoryName(fileName);
            if (!System.IO.Directory.Exists(dirName)) System.IO.Directory.CreateDirectory(dirName);
            do {
                if (++tryCount >= 3) break; 
                try {
                    using (var f = System.IO.File.Open(fileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
                    {
                        try {
                            if (f.Length != 0) {
                                var curLock = new LockFile(f);
                                if (curLock.IsActive) {
                                    if (isActive && curLock.UpdateTme > DateTime.Now.AddHours(-16)) return false; //max lock time 16 hours
                                    if (!isActive && (curLock.MachineName != curMachineName)) throw new Exception("Unable to release, original lock is occupied by another machine '" + curLock.MachineName + "', lock:" + System.IO.Path.GetFileName(fileName));
                                } else {
                                    if (!isActive) throw new Exception("Unable to release, lock is not occupied by any machine, lock:" + System.IO.Path.GetFileName(fileName));
                                    if (minActiveDate.HasValue && curLock.UpdateTme > minActiveDate) return false;
                                }
                                f.Seek(0, System.IO.SeekOrigin.Begin);
                            }
                            var data = System.Text.Encoding.UTF8.GetBytes(string.Format("{0}|{1}|{2}|{3}", FILESIGNATURE, isActive, DateTime.Now.ToUniversalTime().ToString("u"), curMachineName));
                            f.Write(data, 0, data.Length);
                            f.SetLength(data.Length);
                        } finally { 
                            f.Close();
                        }
                    }
                    return true;
                }
                catch (System.IO.IOException ex)
                {
                    lastExp = ex;
                    System.Threading.Thread.Sleep(300);
                }
            } while (true);
            throw new Exception("Unable to set FileLock on " + fileName, lastExp);
        }

        public static bool TryLock(string lockName, DateTime? minActiveDate)
        {
            return TryUpdate(MapLockFile(lockName), true, minActiveDate);
        }

        public static bool Release(string lockName)
        {
            return TryUpdate(MapLockFile(lockName), false, null);
        }
    }
}
