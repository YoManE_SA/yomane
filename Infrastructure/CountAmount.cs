﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Netpay.Infrastructure
{
	public class CountAmount
	{
		public decimal Amount { get; set; }
		public int Count { get; set; }

		public CountAmount() {}
		public CountAmount(decimal amount, int count) 
		{ 
			this.Amount = amount; 
			this.Count = count; 
		}

		public void Reset() 
		{ 
			Amount = 0m; Count = 0; 
		}

		public bool IsZero 
		{ 
			get { return Amount == 0m && Count == 0; }
			set { if(value) { Amount = 0m; Count = 0; } }
		}

		public void Set(decimal amount, int count) 
		{ 
			this.Amount = amount; 
			this.Count = count; 
		}

		public void Add(decimal amount, int count) 
		{ 
			this.Amount += amount; 
			this.Count += count; 
		}

		public void Add(CountAmount amount) 
		{ 
			this.Amount += amount.Amount; 
			this.Count += amount.Count; 
		}
	}
}
