﻿using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.Email
{
    public class Template
    {
        public static string[] AppIdentityTemplates =  {
            "Wallet_Register.htm",
            "Wallet_ResetPassword.htm",
            "Relation_Request.htm",
            "Relation_Confirmed.htm",
            "Balance_Request.htm",
            "Balance_RequestConfirmed.htm"
        };


        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Infrastructure.Email.Module.Current, SecuredObjectName); } }

        public delegate object EmailTestData(Template template, string testValue);
        private EmailTestData _mEmailTestDataProc;
        private string _mHtmlData;

        public string Name { get; private set; }
        public string FileName { get; private set; }
        public string ParamName { get; set; }
        public string[] Fields { get; set; }

        public string Language { get; set; }
        public string HtmlData
        {
            get
            {
                if (_mHtmlData != null) return _mHtmlData;
                string fileName = MapPhysicalPath(Language);
                if (System.IO.File.Exists(fileName)) _mHtmlData = System.IO.File.ReadAllText(fileName);
                return _mHtmlData;
            }
            set { _mHtmlData = value; }
        }


        public bool SupportTest { get { return _mEmailTestDataProc != null; } }

        private Template() { }

        //Here should be a security check for EmailTemplates secured object.
        public static List<Template> Items
        {
            get
            {
                //Security check here causes stackoverflow.
                //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
                return Domain.Current.GetItem("EmailTemplate.Items", () => new List<Template>());
            }
        }

        public static Template Register(string fileName, string name)
        {
            //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Add);

            var ret = new Template() { Name = name, FileName = fileName };
            if (!Items.Any(x => x.FileName == fileName))
            {
                Items.Add(ret);
            }
            return ret;
        }

        public static void UnRegister(string fileName)
        {
            //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Delete);

            Items.RemoveAll(v => v.FileName == fileName);
        }
        public static Template Get(string fileName)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return Items.Where(v => v.FileName == fileName).SingleOrDefault();
        }

        private string MapPhysicalPath(string language)
        {
            if (!string.IsNullOrEmpty(language)) language = "_" + language;
            string pattern = string.Format("{0}{2}{1}", System.IO.Path.GetFileNameWithoutExtension(FileName), System.IO.Path.GetExtension(FileName), language);
            return System.IO.Path.Combine(Domain.Current.EmailTemplatePath, pattern);
        }

        public string MapPhysicalPathWithThemeFolder(string language, string themename)
        {
            if (!string.IsNullOrEmpty(language)) language = "_" + language;
            string pattern = string.Format("{0}{2}{1}", System.IO.Path.GetFileNameWithoutExtension(FileName), System.IO.Path.GetExtension(FileName), language);
            return System.IO.Path.Combine(Domain.Current.EmailTemplatePath, themename, pattern);
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            System.IO.File.WriteAllText(MapPhysicalPath(Language), HtmlData);
            HtmlData = null;
        }

        public string[] Languages
        {
            get
            {
                string pattern = string.Format("{0}_*{1}", System.IO.Path.GetFileNameWithoutExtension(FileName), System.IO.Path.GetExtension(FileName));
                var files = System.IO.Directory.GetFiles(Domain.Current.EmailTemplatePath, pattern, System.IO.SearchOption.TopDirectoryOnly);
                var ret = new List<string>();
                //ret.Add("[Default]");
                foreach (var v in files)
                {
                    var language = System.IO.Path.GetFileNameWithoutExtension(v);
                    language = language.Substring(language.Length - 2);
                    ret.Add(language);
                }
                return ret.ToArray();
            }
        }

        public void Send(object dataObj, string language, System.Net.Mail.MailAddress emailTo, System.Net.Mail.MailAddress emailFrom = null, List<System.Net.Mail.Attachment> attachments = null, string templateData = null, string themename = null)
        {
            try
            {
                System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.GetCultureInfo(language.NullIfEmpty().ValueIfNull("en-us"));
                string subject, body = templateData;
                if (body == null)
                {
                    string filename = null;
                    if (themename == null)
                    {
                        filename = MapPhysicalPath(language);
                    }
                    else
                    {
                        filename = MapPhysicalPathWithThemeFolder(language, themename);
                    }

                    body = FormatMesage.GetMessageText(filename, out subject);
                }
                else subject = FormatMesage.GetMessageTitle(body);

                var message = new System.Net.Mail.MailMessage();
                message.IsBodyHtml = true;
                ObjectContext.Current.Items["EmailMessage"] = message;
                message.Subject = FormatMesage.FormatMessage(subject, dataObj, null, cultureInfo);
                message.Body = FormatMesage.FormatMessage(body, dataObj, null, cultureInfo);
                ObjectContext.Current.Items["EmailMessage"] = null;
                message.From = emailFrom != null ? emailFrom : new System.Net.Mail.MailAddress(Domain.Current.MailAddressFrom, Domain.Current.MailNameFrom);
                message.To.Add(emailTo);
                SmtpClient.Send(message);
                Logger.Log(LogTag.WebService, "Sent Email Template " + Name + " To " + emailTo.ToString());
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
        }

        public static List<string> GetFields(Type obj, int maxDepth = int.MaxValue, string path = "")
        {
            //Causes stackoverflow.
            //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            var ret = new List<string>();
            var fields = obj.GetMembers(System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.GetProperty | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            foreach (var v in fields)
            {
                Type targetType = null;
                string fieldName = null;
                if (v.MemberType == System.Reflection.MemberTypes.Property)
                {
                    fieldName = (v as System.Reflection.PropertyInfo).Name;
                    targetType = (v as System.Reflection.PropertyInfo).PropertyType;
                }
                else if (v.MemberType == System.Reflection.MemberTypes.Field)
                {
                    fieldName = (v as System.Reflection.FieldInfo).Name;
                    targetType = (v as System.Reflection.FieldInfo).FieldType;
                }
                else continue;

                bool isReferenceType = false;
                if (targetType != null)
                {
                    if (targetType.IsClass && !targetType.IsArray && targetType != typeof(string) && !targetType.IsInstanceOfType(typeof(System.Collections.ICollection)))
                        isReferenceType = true;
                }

                if (isReferenceType)
                {
                    if (maxDepth > 0) ret.AddRange(GetFields(targetType, maxDepth - 1, path + fieldName + "."));
                }
                else ret.Add(path + fieldName);

            }
            return ret;
        }

        public void SetCustomData(List<string> fields, string testParamName = null, EmailTestData getTestData = null)
        {
            Fields = fields.ToArray();
            ParamName = testParamName;
            _mEmailTestDataProc = getTestData;
        }

        public void Test(string toEmail, string testValue = null)
        {
            var objData = _mEmailTestDataProc(this, testValue);
            Send(objData, Language, new System.Net.Mail.MailAddress(toEmail), null, null, HtmlData);
        }
    }
}
