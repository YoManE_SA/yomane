﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.Email.Pop3
{
	/// <summary>
	/// If anything goes wrong within Pop3MailClient, a Pop3Exception is raised
	/// </summary>
	public class Pop3Exception : ApplicationException
	{
		/// <summary>
		/// 
		/// </summary>
		public Pop3Exception() { }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="errorMessage"></param>
		public Pop3Exception(string errorMessage) : base(errorMessage) { }
	}
}
