﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net.Security;

namespace Netpay.Infrastructure.Email.Pop3
{
	/// <summary>
	/// If POP3 Server doesn't react as expected or this code has a problem, but
	/// can continue with the execution, a Warning is called.
	/// </summary>
	/// <param name="warningText"></param>
	/// <param name="response">string received from POP3 server</param>
	public delegate void WarningHandler(string warningText, string response);

	/// <summary>
	/// Traces all the information exchanged between POP3 client and POP3 server plus some
	/// status messages from POP3 client.
	/// Helpful to investigate any problem.
	/// Console.WriteLine() can be used
	/// </summary>
	/// <param name="traceText"></param>
	public delegate void TraceHandler(string traceText);
	
	/// <summary>
	/// Provides access to emails on a POP3 Server
	/// </summary>
	public class Pop3Client
	{
		/// <summary>
		/// Called whenever POP3 server doesn't react as expected, but no runtime error is thrown.
		/// </summary>
		public event WarningHandler Warning;

		/// <summary>
		/// Call warning event
		/// </summary>
		/// <param name="methodName">Name of the method where warning is needed</param>
		/// <param name="response">Answer from POP3 server causing the warning</param>
		/// <param name="warningText">Explanation what went wrong</param>
		/// <param name="warningParameters"></param>
		protected void CallWarning(string methodName, string response, string warningText, params object[] warningParameters)
		{
			warningText = string.Format(warningText, warningParameters);
			if (Warning != null)
			{
				Warning(methodName + ": " + warningText, response);
			}
			CallTrace("!! {0}", warningText);
		}

		/// <summary>
		/// Shows the communication between PopClient and PopServer, including warnings
		/// </summary>
		public event TraceHandler Trace;

		/// <summary>
		/// call Trace event
		/// </summary>
		/// <param name="text">string to be traced</param>
		/// <param name="parameters"></param>
		protected void CallTrace(string text, params object[] parameters)
		{
			if (Trace != null)
			{
				Trace(DateTime.Now.ToString("hh:mm:ss ") + _popServer + " " + string.Format(text, parameters));
			}
		}

		/// <summary>
		/// Trace information received from POP3 server
		/// </summary>
		/// <param name="text">string to be traced</param>
		/// <param name="parameters"></param>
		protected void TraceFrom(string text, params object[] parameters)
		{
			if (Trace != null)
			{
				CallTrace("   " + string.Format(text, parameters));
			}
		}

		/// <summary>
		/// Get POP3 server name
		/// </summary>
		public string PopServer
		{
			get { return _popServer; }
		}

		/// <summary>
		/// POP3 server name
		/// </summary>
		protected string _popServer;


		/// <summary>
		/// Get POP3 server port
		/// </summary>
		public int Port
		{
			get { return _port; }
		}
		/// <summary>
		/// POP3 server port
		/// </summary>
		protected int _port;


		/// <summary>
		/// Should SSL be used for connection with POP3 server ?
		/// </summary>
		public bool UseSSL
		{
			get { return _useSSL; }
		}
		/// <summary>
		/// Should SSL be used for connection with POP3 server ?
		/// </summary>
		private bool _useSSL;

		/// <summary>
		/// should Pop3MailClient automatically reconnect if POP3 server has dropped the 
		/// connection due to a timeout ?
		/// </summary>
		public bool IsAutoReconnect
		{
			get { return _isAutoReconnect; }
			set { _isAutoReconnect = value; }
		}
		private bool _isAutoReconnect = false;

		//timeout has occured, we try to perform an autoreconnect
		private bool _isTimeoutReconnect = false;

		/// <summary>
		/// Get / set read timeout (miliseconds)
		/// </summary>
		public int ReadTimeout
		{
			get { return _readTimeout; }
			set
			{
				_readTimeout = value;
				if (_pop3Stream != null && _pop3Stream.CanTimeout)
				{
					_pop3Stream.ReadTimeout = _readTimeout;
				}
			}
		}
		/// <summary>
		/// POP3 server read timeout
		/// </summary>
		protected int _readTimeout = -1;

		/// <summary>
		/// Get owner name of mailbox on POP3 server
		/// </summary>
		public string Username
		{
			get { return _username; }
		}
		/// <summary>
		/// Owner name of mailbox on POP3 server
		/// </summary>
		protected string _username;

		/// <summary>
		/// Get password for mailbox on POP3 server
		/// </summary>
		public string Password
		{
			get { return _password; }
		}
		/// <summary>
		/// Password for mailbox on POP3 server
		/// </summary>
		protected string _password;


		/// <summary>
		/// Get connection status with POP3 server
		/// </summary>
		public Pop3ConnectionState Pop3ConnectionState
		{
			get { return _pop3ConnectionState; }
		}
		/// <summary>
		/// connection status with POP3 server
		/// </summary>
		protected Pop3ConnectionState _pop3ConnectionState = Pop3ConnectionState.Disconnected;

		/// <summary>
		/// set POP3 connection state
		/// </summary>
		/// <param name="State"></param>
		protected void SetPop3ConnectionState(Pop3ConnectionState State)
		{
			_pop3ConnectionState = State;
			CallTrace("   Pop3MailClient Connection State {0} reached", State);
		}

		/// <summary>
		/// throw exception if POP3 connection is not in the required state
		/// </summary>
		/// <param name="requiredState"></param>
		protected void EnsureState(Pop3ConnectionState requiredState)
		{
			if (_pop3ConnectionState != requiredState)
			{
				// wrong connection state
				throw new Pop3Exception("GetMailboxStats only accepted during connection state: " + requiredState.ToString() +
					  "\n The connection to server " + _popServer + " is in state " + _pop3ConnectionState.ToString());
			}
		}

		//private fields
		//--------------
		/// <summary>
		/// TCP to POP3 server
		/// </summary>
		private TcpClient _serverTcpConnection;
		/// <summary>
		/// Stream from POP3 server with or without SSL
		/// </summary>
		private Stream _pop3Stream;
		/// <summary>
		/// Reader for POP3 message
		/// </summary>
		protected StreamReader _pop3StreamReader;
		/// <summary>
		/// char 'array' for carriage return / line feed
		/// </summary>
		protected string _crlf = "\r\n";

		/// <summary>
		/// Make POP3 client ready to connect to POP3 server
		/// </summary>
		/// <param name="popServer"><example>pop.gmail.com</example></param>
		/// <param name="port"><example>995</example></param>
		/// <param name="useSsl">True: SSL is used for connection to POP3 server</param>
		/// <param name="username"><example>abc@gmail.com</example></param>
		/// <param name="password">Secret</param>
		public Pop3Client(string popServer, int port, bool useSsl, string username, string password)
		{
			this._popServer = popServer;
			this._port = port;
			this._useSSL = useSsl;
			this._username = username;
			this._password = password;
		}

		/// <summary>
		/// Connect to POP3 server
		/// </summary>
		public void Connect()
		{
			if (_pop3ConnectionState != Pop3ConnectionState.Disconnected &&
			  _pop3ConnectionState != Pop3ConnectionState.Closed &&
			  !_isTimeoutReconnect)
			{
				CallWarning("connect", "", "Connect command received, but connection state is: " + _pop3ConnectionState.ToString());
			}
			else
			{
				//establish TCP connection
				try
				{
					CallTrace("   Connect at port {0}", _port);
					_serverTcpConnection = new TcpClient(_popServer, _port);
				}
				catch (Exception ex)
				{
					throw new Pop3Exception("Connection to server " + _popServer + ", port " + _port + " failed.\nRuntime Error: " + ex.ToString());
				}

				if (_useSSL)
				{
					//get SSL stream
					try
					{
						CallTrace("   Get SSL connection");
						_pop3Stream = new SslStream(_serverTcpConnection.GetStream(), false);
						_pop3Stream.ReadTimeout = _readTimeout;
					}
					catch (Exception ex)
					{
						throw new Pop3Exception("Server " + _popServer + " found, but cannot get SSL data stream.\nRuntime Error: " + ex.ToString());
					}

					//perform SSL authentication
					try
					{
						CallTrace("   Get SSL authentication");
						((SslStream)_pop3Stream).AuthenticateAsClient(_popServer);
					}
					catch (Exception ex)
					{
						throw new Pop3Exception("Server " + _popServer + " found, but problem with SSL Authentication.\nRuntime Error: " + ex.ToString());
					}
				}
				else
				{
					//create a stream to POP3 server without using SSL
					try
					{
						CallTrace("   Get connection without SSL");
						_pop3Stream = _serverTcpConnection.GetStream();
						_pop3Stream.ReadTimeout = _readTimeout;
					}
					catch (Exception ex)
					{
						throw new Pop3Exception("Server " + _popServer + " found, but cannot get data stream (without SSL).\nRuntime Error: " + ex.ToString());
					}
				}
				//get stream for reading from pop server
				//POP3 allows only US-ASCII. The message will be translated in the proper encoding in a later step
				try
				{
					_pop3StreamReader = new StreamReader(_pop3Stream, Encoding.ASCII);
				}
				catch (Exception ex)
				{
					if (_useSSL)
					{
						throw new Pop3Exception("Server " + _popServer + " found, but cannot read from SSL stream.\nRuntime Error: " + ex.ToString());
					}
					else
					{
						throw new Pop3Exception("Server " + _popServer + " found, but cannot read from stream (without SSL).\nRuntime Error: " + ex.ToString());
					}
				}

				//ready for authorisation
				string response;
				if (!ReadSingleLine(out response))
				{
					throw new Pop3Exception("Server " + _popServer + " not ready to start AUTHORIZATION.\nMessage: " + response);
				}
				SetPop3ConnectionState(Pop3ConnectionState.Authorization);

				//send user name
				if (!ExecuteCommand("USER " + _username, out response))
				{
					throw new Pop3Exception("Server " + _popServer + " doesn't accept username '" + _username + "'.\nMessage: " + response);
				}

				//send password
				if (!ExecuteCommand("PASS " + _password, out response))
				{
					throw new Pop3Exception("Server " + _popServer + " doesn't accept password '" + _password + "' for user '" + _username + "'.\nMessage: " + response);
				}

				SetPop3ConnectionState(Pop3ConnectionState.Connected);
			}
		}


		/// <summary>
		/// Disconnect from POP3 Server
		/// </summary>
		public void Disconnect()
		{
			if (_pop3ConnectionState == Pop3ConnectionState.Disconnected ||
			  _pop3ConnectionState == Pop3ConnectionState.Closed)
			{
				CallWarning("disconnect", "", "Disconnect received, but was already disconnected.");
			}
			else
			{
				//ask server to end session and possibly to remove emails marked for deletion
				try
				{
					string response;
					if (ExecuteCommand("QUIT", out response))
					{
						//server says everything is ok
						SetPop3ConnectionState(Pop3ConnectionState.Closed);
					}
					else
					{
						//server says there is a problem
						CallWarning("Disconnect", response, "negative response from server while closing connection: " + response);
						SetPop3ConnectionState(Pop3ConnectionState.Disconnected);
					}
				}
				finally
				{
					//close connection
					if (_pop3Stream != null)
					{
						_pop3Stream.Close();
					}

					_pop3StreamReader.Close();
				}
			}
		}


		/// <summary>
		/// Delete message from server.
		/// The POP3 server marks the message as deleted.  Any future
		/// reference to the message-number associated with the message
		/// in a POP3 command generates an error.  The POP3 server does
		/// not actually delete the message until the POP3 session
		/// enters the UPDATE state.
		/// </summary>
		/// <param name="msg_number"></param>
		/// <returns></returns>
		public bool DeleteEmail(int msg_number)
		{
			EnsureState(Pop3ConnectionState.Connected);
			string response;
			if (!ExecuteCommand("DELE " + msg_number.ToString(), out response))
			{
				CallWarning("DeleteEmail", response, "negative response for email (Id: {0}) delete request", msg_number);
				return false;
			}
			return true;
		}

		/// <summary>
		/// Get a list of all Email IDs available in mailbox
		/// </summary>
		/// <returns></returns>
		public bool GetEmailIdList(out List<int> EmailIds)
		{
			EnsureState(Pop3ConnectionState.Connected);
			EmailIds = new List<int>();

			//get server response status line
			string response;
			if (!ExecuteCommand("LIST", out response))
			{
				CallWarning("GetEmailIdList", response, "negative response for email list request");
				return false;
			}

			//get every email id
			int EmailId;
			while (ReadMultiLine(out response))
			{
				if (int.TryParse(response.Split(' ')[0], out EmailId))
				{
					EmailIds.Add(EmailId);
				}
				else
				{
					CallWarning("GetEmailIdList", response, "first characters should be integer (EmailId)");
				}
			}
			TraceFrom("{0} email ids received", EmailIds.Count);
			return true;
		}


		/// <summary>
		/// get size of one particular email
		/// </summary>
		/// <param name="msg_number"></param>
		/// <returns></returns>
		public int GetEmailSize(int msg_number)
		{
			EnsureState(Pop3ConnectionState.Connected);
			string response;
			ExecuteCommand("LIST " + msg_number.ToString(), out response);
			int EmailSize = 0;
			string[] responseSplit = response.Split(' ');
			if (responseSplit.Length < 2 || !int.TryParse(responseSplit[2], out EmailSize))
			{
				CallWarning("GetEmailSize", response, "'+OK int int' format expected (EmailId, EmailSize)");
			}

			return EmailSize;
		}


		/// <summary>
		/// Get a list with the unique IDs of all Email available in mailbox.
		/// 
		/// Explanation:
		/// EmailIds for the same email can change between sessions, whereas the unique Email id
		/// never changes for an email.
		/// </summary>
		/// <param name="EmailIds"></param>
		/// <returns></returns>
		public bool GetUniqueEmailIdList(out List<EmailUid> EmailIds)
		{
			EnsureState(Pop3ConnectionState.Connected);
			EmailIds = new List<EmailUid>();

			//get server response status line
			string response;
			if (!ExecuteCommand("UIDL ", out response))
			{
				CallWarning("GetUniqueEmailIdList", response, "negative response for email list request");
				return false;
			}

			//get every email unique id
			int EmailId;
			while (ReadMultiLine(out response))
			{
				string[] responseSplit = response.Split(' ');
				if (responseSplit.Length < 2)
				{
					CallWarning("GetUniqueEmailIdList", response, "response not in format 'int string'");
				}
				else if (!int.TryParse(responseSplit[0], out EmailId))
				{
					CallWarning("GetUniqueEmailIdList", response, "first charaters should be integer (Unique EmailId)");
				}
				else
				{
					EmailIds.Add(new EmailUid(EmailId, responseSplit[1]));
				}
			}
			TraceFrom("{0} unique email ids received", EmailIds.Count);
			return true;
		}


		/// <summary>
		/// get a list with all currently available messages and the UIDs
		/// </summary>
		/// <param name="EmailIds">EmailId Uid list</param>
		/// <returns>false: server sent negative response (didn't send list)</returns>
		public bool GetUniqueEmailIdList(out SortedList<string, int> EmailIds)
		{
			EnsureState(Pop3ConnectionState.Connected);
			EmailIds = new SortedList<string, int>();

			//get server response status line
			string response;
			if (!ExecuteCommand("UIDL", out response))
			{
				CallWarning("GetUniqueEmailIdList", response, "negative response for email list request");
				return false;
			}

			//get every email unique id
			int EmailId;
			while (ReadMultiLine(out response))
			{
				string[] responseSplit = response.Split(' ');
				if (responseSplit.Length < 2)
				{
					CallWarning("GetUniqueEmailIdList", response, "response not in format 'int string'");
				}
				else if (!int.TryParse(responseSplit[0], out EmailId))
				{
					CallWarning("GetUniqueEmailIdList", response, "first charaters should be integer (Unique EmailId)");
				}
				else
				{
					EmailIds.Add(responseSplit[1], EmailId);
				}
			}
			TraceFrom("{0} unique email ids received", EmailIds.Count);
			return true;
		}


		/// <summary>
		/// get size of one particular email
		/// </summary>
		/// <param name="msg_number"></param>
		/// <returns></returns>
		public int GetUniqueEmailId(EmailUid msg_number)
		{
			EnsureState(Pop3ConnectionState.Connected);
			string response;
			ExecuteCommand("LIST " + msg_number.ToString(), out response);
			int EmailSize = 0;
			string[] responseSplit = response.Split(' ');
			if (responseSplit.Length < 2 || !int.TryParse(responseSplit[2], out EmailSize))
			{
				CallWarning("GetEmailSize", response, "'+OK int int' format expected (EmailId, EmailSize)");
			}

			return EmailSize;
		}


		/// <summary>
		/// Sends an 'empty' command to the POP3 server. Server has to respond with +OK
		/// </summary>
		/// <returns>true: server responds as expected</returns>
		public bool NOOP()
		{
			EnsureState(Pop3ConnectionState.Connected);
			string response;
			if (!ExecuteCommand("NOOP", out response))
			{
				CallWarning("NOOP", response, "negative response for NOOP request");
				return false;
			}
			return true;
		}

		/// <summary>
		/// Should the raw content, the US-ASCII code as received, be traced
		/// GetRawEmail will switch it on when it starts and off once finished
		/// 
		/// Inheritors might use it to get the raw email
		/// </summary>
		protected bool _isTraceRawEmail = false;

		/// <summary>
		/// contains one MIME part of the email in US-ASCII, needs to be translated in .NET string (Unicode)
		/// contains the complete email in US-ASCII, needs to be translated in .NET string (Unicode)
		/// For speed reasons, reuse StringBuilder
		/// </summary>
		protected StringBuilder _rawEmailBuilder;


		/// <summary>
		/// Reads the complete text of a message
		/// </summary>
		/// <param name="messageNo">Email to retrieve</param>
		/// <param name="emailText">ASCII string of complete message</param>
		/// <returns></returns>
		public bool GetRawEmail(int messageNo, out string emailText)
		{
			//send 'RETR int' command to server
			if (!SendRetrCommand(messageNo))
			{
				emailText = null;
				return false;
			}

			//get the lines
			string response;
			int LineCounter = 0;
			//empty StringBuilder
			if (_rawEmailBuilder == null)
			{
				_rawEmailBuilder = new StringBuilder(100000);
			}
			else
			{
				_rawEmailBuilder.Length = 0;
			}
			_isTraceRawEmail = true;
			while (ReadMultiLine(out response))
			{
				LineCounter += 1;
			}
			emailText = _rawEmailBuilder.ToString();
			TraceFrom("email with {0} lines,  {1} chars received", LineCounter.ToString(), emailText.Length);
			return true;
		}

		/// <summary>
		/// Unmark any emails from deletion. The server only deletes email really
		/// once the connection is properly closed.
		/// </summary>
		/// <returns>true: emails are unmarked from deletion</returns>
		public bool UndeleteAllEmails()
		{
			EnsureState(Pop3ConnectionState.Connected);
			string response;
			return ExecuteCommand("RSET", out response);
		}

		/// <summary>
		/// Get mailbox statistics
		/// </summary>
		/// <param name="numberOfMails"></param>
		/// <param name="mailboxSize"></param>
		/// <returns></returns>
		public bool GetMailboxStats(out int numberOfMails, out int mailboxSize)
		{
			EnsureState(Pop3ConnectionState.Connected);

			//interpret response
			string response;
			numberOfMails = 0;
			mailboxSize = 0;
			if (ExecuteCommand("STAT", out response))
			{
				//got a positive response
				string[] responseParts = response.Split(' ');
				if (responseParts.Length < 2)
				{
					//response format wrong
					throw new Pop3Exception("Server " + _popServer + " sends illegally formatted response." +
					  "\nExpected format: +OK int int" +
					  "\nReceived response: " + response);
				}
				numberOfMails = int.Parse(responseParts[1]);
				mailboxSize = int.Parse(responseParts[2]);
				return true;
			}
			return false;
		}


		/// <summary>
		/// Send RETR command to POP 3 server to fetch one particular message
		/// </summary>
		/// <param name="messageNo">ID of message required</param>
		/// <returns>false: negative server respond, message not delivered</returns>
		protected bool SendRetrCommand(int messageNo)
		{
			EnsureState(Pop3ConnectionState.Connected);
			// retrieve mail with message number
			string response;
			if (!ExecuteCommand("RETR " + messageNo.ToString(), out response))
			{
				CallWarning("GetRawEmail", response, "negative response for email (ID: {0}) request", messageNo);
				return false;
			}
			return true;
		}

		public bool _isDebug = false;

		/// <summary>
		/// sends the 4 letter command to POP3 server (adds CRLF) and waits for the
		/// response of the server
		/// </summary>
		/// <param name="command">command to be sent to server</param>
		/// <param name="response">answer from server</param>
		/// <returns>false: server sent negative acknowledge, i.e. server could not execute command</returns>
		private bool ExecuteCommand(string command, out string response)
		{
			//send command to server
			byte[] commandBytes = System.Text.Encoding.ASCII.GetBytes((command + _crlf).ToCharArray());
			CallTrace("Tx '{0}'", command);
			bool isSupressThrow = false;
			try
			{
				_pop3Stream.Write(commandBytes, 0, commandBytes.Length);
				if (_isDebug)
				{
					_isDebug = false;
					throw new IOException("Test", new SocketException(10053));
				}
			}
			catch (IOException ex)
			{
				//Unable to write data to the transport connection. Check if reconnection should be tried
				isSupressThrow = ExecuteReconnect(ex, command, commandBytes);
				if (!isSupressThrow)
				{
					throw;
				}
			}
			_pop3Stream.Flush();

			//read response from server
			response = null;
			try
			{
				response = _pop3StreamReader.ReadLine();
			}
			catch (IOException ex)
			{
				//Unable to write data to the transport connection. Check if reconnection should be tried
				isSupressThrow = ExecuteReconnect(ex, command, commandBytes);
				if (isSupressThrow)
				{
					//wait for response one more time
					response = _pop3StreamReader.ReadLine();
				}
				else
				{
					throw;
				}
			}
			if (response == null)
			{
				throw new Pop3Exception("Server " + _popServer + " has not responded, timeout has occured.");
			}
			CallTrace("Rx '{0}'", response);
			return (response.Length > 0 && response[0] == '+');
		}


		/// <summary>
		/// reconnect, if there is a timeout exception and isAutoReconnect is true
		/// </summary>
		private bool ExecuteReconnect(IOException ex, string command, byte[] commandBytes)
		{
			if (ex.InnerException != null && ex.InnerException is SocketException)
			{
				//SocketException
				SocketException innerEx = (SocketException)ex.InnerException;
				if (innerEx.ErrorCode == 10053)
				{
					//probably timeout: An established connection was aborted by the software in your host machine.
					CallWarning("ExecuteCommand", "", "probably timeout occured");
					if (_isAutoReconnect)
					{
						//try to reconnect and send one more time
						_isTimeoutReconnect = true;
						try
						{
							CallTrace("   try to auto reconnect");
							Connect();

							CallTrace("   reconnect successful, try to resend command");
							CallTrace("Tx '{0}'", command);
							_pop3Stream.Write(commandBytes, 0, commandBytes.Length);
							_pop3Stream.Flush();
							return true;
						}
						finally
						{
							_isTimeoutReconnect = false;
						}

					}
				}
			}
			return false;
		}

		/// <summary>
		/// read single line response from POP3 server. 
		/// <example>Example server response: +OK asdfkjahsf</example>
		/// </summary>
		/// <param name="response">response from POP3 server</param>
		/// <returns>true: positive response</returns>
		protected bool ReadSingleLine(out string response)
		{
			response = null;
			try
			{
				response = _pop3StreamReader.ReadLine();
			}
			catch (Exception ex)
			{
				string s = ex.Message;
			}
			if (response == null)
			{
				throw new Pop3Exception("Server " + _popServer + " has not responded, timeout has occured.");
			}
			CallTrace("Rx '{0}'", response);
			return (response.Length > 0 && response[0] == '+');
		}


		/// <summary>
		/// read one line in multiline mode from the POP3 server. 
		/// </summary>
		/// <param name="response">line received</param>
		/// <returns>false: end of message</returns>
		/// <returns></returns>
		protected bool ReadMultiLine(out string response)
		{
			response = null;
			response = _pop3StreamReader.ReadLine();
			if (response == null)
			{
				throw new Pop3Exception("Server " + _popServer + " has not responded, probably timeout has occured.");
			}
			if (_isTraceRawEmail)
			{
				//collect all responses as received
				_rawEmailBuilder.Append(response + _crlf);
			}
			//check for byte stuffing, i.e. if a line starts with a '.', another '.' is added, unless
			//it is the last line
			if (response.Length > 0 && response[0] == '.')
			{
				if (response == ".")
				{
					//closing line found
					return false;
				}
				//remove the first '.'
				response = response.Substring(1, response.Length - 1);
			}
			return true;
		}

	}
}
