﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Netpay.Infrastructure.Email
{
	public class FormatMesage
	{
		/// <summary>
		/// get text of nested property
		/// </summary>
		public static string GetNestedText(object obj, string methodName, System.Resources.ResourceManager rm, System.Globalization.CultureInfo cultureInfo)
		{
            if (methodName.Contains('<') || methodName.Contains('>'))
            {
                methodName = Regex.Match(methodName, "(?<=>)(.*)(?=<)").Value;
            }

            int start = 0, pos = 0;
			Type varType = null;
			Type objType = null;
			MemberInfo[] method = null;
			BindingFlags bndAttr = System.Reflection.BindingFlags.GetProperty | System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.IgnoreCase;
			while (true)
			{
				objType = obj.GetType();
				pos = methodName.IndexOf('.', start);
				if (pos == -1) pos = methodName.Length;
				string subMethodName = methodName.Substring(start, pos - start);
				method = obj.GetType().GetMember(subMethodName, bndAttr);
				if (method.Length < 1) throw new Exception(string.Format("Unable to call find member {0} on expression {1}", subMethodName, methodName));
				try
				{
					if (method[0].MemberType == MemberTypes.Property)
					{
						PropertyInfo pi = objType.GetProperty(method[0].Name);
						obj = pi.GetValue(obj, null);
						varType = pi.PropertyType;
					}
					else if (method[0].MemberType == MemberTypes.Field)
					{
						FieldInfo fi = objType.GetField(method[0].Name);
						obj = fi.GetValue(obj);
						varType = fi.FieldType;
					}
					else if (method[0].MemberType == MemberTypes.Method)
					{
						MethodInfo mi = objType.GetMethod(method[0].Name, new Type[] { });
						if (mi == null) throw new Exception(string.Format("Unable to call method {0}, with no params or method not found on expression {1}", subMethodName, methodName));
						obj = mi.Invoke(obj, null);
						varType = mi.ReturnType;
					}
				}
				catch (Exception ex)
				{
					throw new Exception(string.Format("Error in call to {0} / {1} Error: {2}", methodName, subMethodName, ex.Message), ex);
				}
				if (obj == null) return null;
				if (varType != null && varType.IsEnum)
				{
					if(rm != null) obj = rm.GetString(varType.Name + "." + obj.ToString(), cultureInfo);
					else obj = obj.ToString();
				}
				start = pos + 1;
				if (start >= methodName.Length) break;
			}
			if (obj == null) return null;
			return obj.ToString().Replace("\r\n", "<br/>");
		}

		/// <summary>
		/// fast replace property expression in string
		/// </summary>
		public static string FormatMessage(string source, object obj, System.Resources.ResourceManager rm, System.Globalization.CultureInfo cultureInfo)
		{
			int start = 0, pos = 0, keywordEnd = 0;
			StringBuilder sb = new StringBuilder();
			while ((pos = source.IndexOf('[', start)) > -1)
			{
				if (pos < source.Length && source[pos + 1] == '[')
				{
					start = pos + 2;
					continue;
				}
				sb.Append(source.Substring(start, pos - start));
				keywordEnd = source.IndexOf(']', pos + 1);
				if (keywordEnd == -1) throw new ApplicationException("missing ] before end of the file");
				sb.Append(GetNestedText(obj, source.Substring(pos + 1, keywordEnd - pos - 1), rm, cultureInfo));
				start = keywordEnd + 1;
			}
			sb.Append(source.Substring(start));
			return sb.ToString();
		}

		public static string GetMessageText(string fileName, out string subject)
		{
            var ret = System.IO.File.ReadAllText(fileName);
            subject = GetMessageTitle(ret);
            return ret;
        }

        public static string GetMessageTitle(string fileText)
        {
            int subjectStart = fileText.IndexOf("<title>");
            if (subjectStart > -1)
            {
                subjectStart += "<title>".Length;
                int subjectEnd = fileText.IndexOf("</title>", subjectStart);
                return fileText.Substring(subjectStart, subjectEnd - subjectStart);
            }
            return string.Empty;
        }



    }
}
