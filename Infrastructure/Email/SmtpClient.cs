﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace Netpay.Infrastructure.Email
{
    public static class SmtpClient
    {


        public static System.Net.Mail.SmtpClient GetClient()
        {
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true; // This line of code is used for Gmail accounts.

            var smtpHost = Application.GetParameter("smtpHost");
            if (!string.IsNullOrEmpty(smtpHost))
            {
                client.Host = smtpHost;
                client.Port = int.Parse(Application.GetParameter("smtpPort"));
            }
            var smtpUserName = Application.GetParameter("smtpUserName");
            if (!string.IsNullOrEmpty(smtpUserName))
            {
                var password = Application.GetParameter("smtpPassword");
                client.Credentials = new NetworkCredential(smtpUserName, password, "yomane.com");
            }
            else // Case when there are no credentials , initialze the credentials with empty string
            {
                client.Credentials = new NetworkCredential(string.Empty, string.Empty);
            }
            return client;
        }

        public static void Send(MailMessage message)
        {
            try
            {
                Send(message, null);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw ex;
            }
        }
        public static void Send(MailMessage message, IList<string> attachmentNameList)
        {
            try
            {
                var username = Application.GetParameter("smtpUserName");
                var password = Application.GetParameter("smtpPassword");
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {

                    ExchangeEmailManager emm = new ExchangeEmailManager(username, password);
                    var attList = new List<string>();
                    var toList = new List<string>();
                    foreach (var email in message.To)
                    {
                        toList.Add(email.Address);
                    }
                    emm.SendEmail(message.Subject, message.Body, attachmentNameList, toList);
                    Logger.Log(LogSeverity.Info, LogTag.Process, string.Format("PM Test: Email successfully sent using {0} to {1}",username,toList.FirstOrDefault()));
                }
           

                //if (!Application.EnableEmail) return;

                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                //ServicePointManager.ServerCertificateValidationCallback = delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
                //{
                //    return true;
                //};
                //var client = GetClient();
                //client.Send(message); 
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw ex;
            }
        }

        public static void Send(MailAddress from, MailAddress to, string subject, string body)
        {
            Send(from, to, subject, body, null, null);
        }

        public static void Send(MailAddress from, MailAddress to, string subject, string body, string replyToAddress, List<System.Net.Mail.Attachment> attachments)
        {
            MailMessage message = new MailMessage(from, to);
            message.Subject = subject;
            message.Body = body;
            if (replyToAddress != null) message.ReplyToList.Add(new MailAddress(replyToAddress));
            message.IsBodyHtml = true;
            if (attachments != null)
            {
                foreach (var att in attachments)
                    message.Attachments.Add(att);
            }
            Send(message);
        }

        public static void Send(string fromName, string fromAddress, string toName, string toAddress, string subject, string body)
        {
            MailAddress from = new MailAddress(fromAddress, fromName);
            MailAddress to = new MailAddress(toAddress, toName);

            Send(from, to, subject, body);
        }

        public static void Send(string fromName, string fromAddress, string toAddresses, string subject, string body)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromAddress, fromName);
            message.To.Add(toAddresses.Replace(';', ','));
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            Send(message);
        }
    }
}
