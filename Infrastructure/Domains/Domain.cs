﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.Domains
{
	public class Domain
	{
		public string Host { get; set; }
		public string ShortCode { get; set; }
		public string ThemeFolder { get; set; }
		public bool IsHebrewVisible { get; set; }
		public string MerchantUrl { get; set; }
		public string DevCentertUrl { get; set; }
		public string ProcessUrl { get; set; }
		public string ProcessV2Url { get; set; }
		public string CartUrl { get; set; }
		public string WalletUrl { get; set; }
        public string WebServicesUrl { get; set; }
        public string MerchantsStoragePhisicalBasePath { get; set; }
		public string AdminStoragePhisicalBasePath { get; set; }
        public string PartnerStoragePhisicalBasePath { get; set; }
        public string CustomerStoragePhisicalBasePath { get; set; }
		public string Sql1ConnectionString { get; set; }
		public string Sql2ConnectionString { get; set; }
        public string ReportsConnectionString { get; set; }
        public string SqlArchiveConnectionString { get; set; }
        public string IntegrationServicesNetpayConnectionString { get; set; }
		public string IntegrationServicesReportsConnectionString { get; set; }
		public string AnalysisServicesConnectionString { get; set; }
        public string MailNameFrom { get; set; }
        public string MailAddressFrom { get; set; }
		public string MailNameTo { get; set; }
		public string MailAddressTo { get; set; }
        public string MailAddressFromChb { get; set; }
        public string BrandName { get; set; }
		public string LegalName { get; set; }
		public string FormsInboxUrl { get; set; }
		public string ContentUrl { get; set; }
		public string SignupUrl { get; set; }
		public bool ForceSSL { get; set; }
		public string NetpayAdminsNTGroupName { get; set; }
		public DomainCache Cache { get; set; }
		public int EncryptionKeyNumber { get; set; }
		public string MsnContact { get; set; }
		public string SkypeContact { get; set; }
		public string EpaFilesPhisicalBasePath { get; set; }
		public string ChbFilesPhisicalBasePath { get; set; }
		public string AuthFilesPhisicalBasePath { get; set; }
		public string PayoneerServiceBase { get; set; }
		public string PayoneerUniqueID { get; set; }
		public string PayoneerUsername { get; set; }
		public string PayoneerPassword { get; set; }
		public string PayoneerProgramID { get; set; }
		public bool EnableEpa { get; set; }
        public string CustomerFilesFolder { get; set; }
        public string merchantFilesFolder { get; set; }
		public string SmsServiceProvider { get; set; }
		public string SmsServiceUserName { get; set; }
        public string SmsServicePassword { get; set; }
		public string SmsServiceFrom { get; set; }
        public string TransactionImagesPath { get; set; }
        public string MerchantContentFolder { get; set; }
        public string GlobalAccessFilesVirtualPath { get; set; }
        public string GlobalAccessFilesPhisicalPath { get; set; }
		public string EmailTemplatePath { get; set; }
		public string CRMEmailTemplatePath { get; set; }
		public string CRMConnectionString { get; set; }
		public string FacebookShopsAppID { get; set; }
		public string FacebookShopsSecretID { get; set; }
		public string FacebookShopsNamespace { get; set; }
		public string ZohoCRMAuth { get; set; }
		public string ServiceUser { get; set; }
		public string ServiceUserPassword { get; set; }

		private Guid _serviceCredentials;
		public Guid ServiceCredentials
		{ 
			get {
				if (_serviceCredentials != Guid.Empty) return _serviceCredentials;
				Guid retId;
				if (Security.SecurityManager.Login(Host, UserType.NetpayAdmin, ServiceUser, ServiceUser, ServiceUserPassword, out retId) == LoginResult.Success) _serviceCredentials = retId;
				else throw new NotLoggedinException(string.Format("Service unable to login to domain {0}", Host));
				return _serviceCredentials;
			}
		}
		public Guid GuestCredentials { get; private set; }

        public string LogoPath
        {
			get{
				Uri siteURL = new Uri(ProcessUrl);
				return siteURL.Scheme + "://" + siteURL.Host + "/NPCommon/Images";
			}
		}

		private Dictionary<string, object> _cachData;
		public Domain()
		{
			_cachData = new Dictionary<string, object>();
			GuestCredentials = Guid.NewGuid();
		}

		public object GetCachData(string key)
		{
            
			object ret;
			lock(_cachData)
				if (_cachData.TryGetValue(key, out ret)) return ret;
			return null;
		}
		public void SetCachData(string key, object value)
		{
			lock (_cachData) _cachData[key] = value;
		}
		public void RemoveCachData(string key)
		{
			lock (_cachData) _cachData.Remove(key);
		}

	}
}