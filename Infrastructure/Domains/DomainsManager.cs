﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using System.Web;
using System.Web.Caching;

namespace Netpay.Infrastructure.Domains
{
	public static class DomainsManager
	{
		private static Dictionary<string, Domain> _domains = null;

		public static void Init()
		{
			_domains = Configuration.GetDomains();
		}

		public static Dictionary<string, Domain> Domains
		{
			get { return DomainsManager._domains; }
		}

		public static Domain GetDomain(string domainHost)
		{
			if (domainHost == null)
				throw new ArgumentNullException("domainHost");

			if (_domains == null || _domains.Count == 0)
				throw new ApplicationException("No domains loaded. Init infrastructure?");
			
			string originalName = domainHost;
			while (domainHost != "" && !_domains.ContainsKey(domainHost))
			{
				int nextComponent = domainHost.IndexOf('.');
				if (nextComponent == -1) nextComponent = domainHost.Length - 1;
				domainHost = domainHost.Substring(nextComponent + 1);
            }
			
			if(domainHost == "") {
				if (_domains.ContainsKey("default")) domainHost = "default";
				else throw new ApplicationException(string.Format("Domain '{0}' was not found.", originalName));
			}
			return _domains[domainHost];
		}


		/*
		public static Domain GetDomain(Guid credentialsToken)
		{
			var user = Security.SecurityManager.GetInternalUser(credentialsToken);
			if (user != null) return user.Domain;
			return _domains.Values.Where(d => d.GuestCredentials == credentialsToken).SingleOrDefault();
		}
		*/ 
	}
}