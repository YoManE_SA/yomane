﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.VO;
using System.Web;
using System.Web.Caching;
using Netpay.Infrastructure.Security;
using System.Net;
using Netpay.Dal.DataAccess;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure.Domains
{
	/// <summary>
	/// Contains a cached version of netpay collections such as countries and currencies.
	/// Also contains methods for easy access.
	/// Most methods in this class should implement double-checked locking pattern for thread safety.
	/// </summary>
	public class DomainCache
	{
		public DomainCache(Domain domain)
		{
			_domain = domain;
		}

		private Domain _domain = null;
		private object _syncRoot = new object();
		/*
		private List<StateVO> _states = null;
		private List<CountryVO> _countries = null;
		private List<GlobalDataVO> _globalData = null;
		private List<TransactionSourceVO> _transactionSources = null;
		private List<PaymentMethodVO> _paymentMethods = null;
		//private List<DebitCompanyCodeVO> _debitCompanyCodes = null;
		private Dictionary<string, string> _merchantAccountManagers = null;
		private List<PaymentMethodGroupVO> _paymentMethodGroups = null;
        private Dictionary<CommonTypes.Language, LanguageVO> _languages = null;
        private Dictionary<int, BinNumberVO> _bins = null;
        private Dictionary<int, FileItemTypeVO> _fileTypes = null;
        private Dictionary<string, SecurityGroupVO> _groups = null;
		private Dictionary<TransactionAmountType, TransactionAmountTypeVO> _transactionAmountTypes = null;
		private Dictionary<int, KeyValuePair<string, List<CountryVO> > > _countryGroups = null;

		public Dictionary<TransactionAmountType, TransactionAmountTypeVO> TransactionAmountTypes
		{
			get
			{
				if (_transactionAmountTypes == null)
				{
					lock (_syncRoot)
					{
						Netpay.Dal.DataAccess.NetpayDataContext dc = new Netpay.Dal.DataAccess.NetpayDataContext(_domain.Sql1ConnectionString);
						_transactionAmountTypes = (from et in dc.TransAmountTypes select new TransactionAmountTypeVO(et)).ToDictionary(et => (TransactionAmountType)et.ID);
					}
				}
				return _transactionAmountTypes;
			}
		}


		public Dictionary<string, string> MerchantAccountManagers
		{
			get
			{
				if (_merchantAccountManagers == null)
				{
					lock (_syncRoot)
					{
						if (_merchantAccountManagers == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							_merchantAccountManagers = (from m in dc.tblSecurityUsers select new { m.su_Username, m.su_Name }).ToDictionary(u => u.su_Username, u => string.Format("{0} ({1})", u.su_Name, u.su_Username));
						}
					}
				}

				return _merchantAccountManagers;
			}
		}

		/// <summary>
		/// Gets the card bin number entry.
		/// </summary>
		/// <returns></returns>
		public BinNumberVO GetBin(int bin)
		{
			if (_bins == null)
				_bins = new Dictionary<int, BinNumberVO>();

			if (_bins.ContainsKey(bin))
				return _bins[bin];

			NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCreditCardBIN entity = (from b in dc.tblCreditCardBINs where b.BINNumber == bin select b).FirstOrDefault();
			if (entity == null)
				return null;

			BinNumberVO binVO = new BinNumberVO(entity);
			_bins.Add(bin, binVO);

			return binVO;
		}

		public EpaDebitCompanyVO GetEpaDebitCompany(int debitCompanyID)
		{
			return (from edc in EpaDebitCompanies where edc.ID == debitCompanyID select edc).SingleOrDefault();
		}
		/*
		public DebitCompanyVO GetDebitCompany(int debitCompanyID)
		{
			return (from dc in DebitCompanies where dc.ID == debitCompanyID select dc).SingleOrDefault();
		}

		public string GetDebitCompanyName(int? debitCompanyID)
		{
			if (debitCompanyID == null) return null;
			var ret = GetDebitCompany(debitCompanyID.Value);
			if (ret == null) return null;
			return ret.DebitCompanyName;
		}
		public DebitCompanyCodeVO GetDebitCompanyCode(int debitCompanyID, string code)
		{
			return (from dcc in DebitCompanyCodes where dcc.DebitCompanyID == debitCompanyID && dcc.Code == code select dcc).SingleOrDefault();
		}
		*/

		public Dictionary<string, SecurityGroupVO> SecurityGroups
		{
            get
            {
                if (_groups == null)
                {
					Netpay.Dal.DataAccess.NetpayDataContext dc = new Netpay.Dal.DataAccess.NetpayDataContext(_domain.Sql1ConnectionString);
                    _groups = (from g in dc.tblSecurityGroups select new SecurityGroupVO(g)).ToDictionary(g => g.Name);
                }
                return _groups;
            }
		}
		/*
		public List<DebitCompanyCodeVO> DebitCompanyCodes
		{
			get
			{
				if (_debitCompanyCodes == null)
				{
					lock (_syncRoot)
					{
						if (_debitCompanyCodes == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							_debitCompanyCodes = (from dcc in dc.tblDebitCompanyCodes select new DebitCompanyCodeVO(dcc)).ToList<DebitCompanyCodeVO>();
						}
					}
				}

				return _debitCompanyCodes;
			}
		}
		*/
		public List<PaymentMethodVO> PaymentMethods
		{
			get
			{
				if (_paymentMethods == null)
				{
					lock (_syncRoot)
					{
						if (_paymentMethods == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							_paymentMethods = (from pm in dc.PaymentMethods select new PaymentMethodVO(_domain.Host, pm)).ToList<PaymentMethodVO>();
						}
					}
				}

				return _paymentMethods;
			}
		}

		public PaymentMethodGroupVO GetPaymentMethodGroup(int paymentMethodGroup)
		{
			return PaymentMethodGroup.Where(pm => pm.ID == paymentMethodGroup).SingleOrDefault();
		}

		public List<PaymentMethodGroupVO> PaymentMethodGroup
		{
			get
			{
				if (_paymentMethodGroups == null)
				{
					lock (_syncRoot)
					{
						if (_paymentMethodGroups == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							_paymentMethodGroups = (from pmg in dc.PaymentMethodGroups select new PaymentMethodGroupVO(pmg)).ToList<PaymentMethodGroupVO>();
						}
					}
				}
				return _paymentMethodGroups;
			}
		}

		public PaymentMethodVO GetPaymentMethod(int paymentMethodID)
		{
			return PaymentMethods.Where(pm => pm.ID == paymentMethodID).SingleOrDefault();
		}

		public string GetPaymentMethodName(int? paymentMethodID)
		{
			if (paymentMethodID == null) return null;
			var item = PaymentMethods.Where(pm => pm.ID == paymentMethodID).SingleOrDefault();
			if (item == null) return null;
			return item.Name;
		}

		public IEnumerable<CountryVO> GetPaymentMethodCountries(int? currencyID, PaymentMethodType[] paymentMethodType, PaymentMethodGroupEnum[] paymentMethodGroup)
		{
			IEnumerable<PaymentMethodVO> pMethods = PaymentMethods.ToArray();
			if (paymentMethodType != null) pMethods = pMethods.Where(pm => paymentMethodType.Contains(pm.Type));
			if (paymentMethodGroup != null) pMethods = pMethods.Where(pm => paymentMethodGroup.Contains(pm.Gruop));
            List<PaymentMethodCountryCurrencyVO> pmcc = null;
            if (currencyID != null) {
                pmcc = pMethods.SelectMany(pm => pm.CurrencyCountry).Where(cc => cc.CurrencyID == null || cc.CurrencyID == currencyID).ToList();
            } else {
                pmcc = pMethods.SelectMany(pm => pm.CurrencyCountry).ToList();
            }
            if (pmcc.Exists(cc => cc.CountryID == null)) return GetCountries();
            return (from c in pmcc select GetCountry(c.CountryID.GetValueOrDefault())).Distinct();
		}

        public List<PaymentMethodVO> GetCountryPaymentMethods(int countryID, int? currencyID, PaymentMethodType[] paymentMethodType, PaymentMethodGroupEnum[] paymentMethodGroup)
		{
            var exp = PaymentMethods.Where(pm => pm.CurrencyCountry.Exists(cc => cc.CountryID == null || cc.CountryID == countryID));
			if (paymentMethodType != null) exp = exp.Where(pm => paymentMethodType.Contains(pm.Type));
			if (paymentMethodGroup != null) exp = exp.Where(pm => paymentMethodGroup.Contains(pm.Gruop));
            if (currencyID != null) exp = exp.Where(pm => pm.CurrencyCountry.Exists(cc => cc.CurrencyID == null || cc.CurrencyID == currencyID));
			return exp.ToList<PaymentMethodVO>();
		}

        public Dictionary<Language, LanguageVO> Languages
		{
			get
			{
				if (_languages == null)
				{
					lock (_syncRoot)
					{
                        if (_languages == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
                            _languages = (from l in dc.LanguageLists select new LanguageVO(l)).ToDictionary(l => l.Language);
						}
					}
				}
                return _languages;
			}
		}
        
		/// <summary>
		/// Returns a cached list of all global values.
		/// </summary>
		public List<GlobalDataVO> GlobalData
		{
			get
			{
				if (_globalData == null)
				{
					lock (_syncRoot)
					{
						if (_globalData == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							_globalData = (from gd in dc.tblGlobalDatas select new GlobalDataVO(gd)).ToList<GlobalDataVO>();
						}
					}
				}

				return _globalData;
			}
		}

		/// <summary>
		/// Returns cached global values by group, country code (2 letter iso code) and language.
		/// </summary>
		/// <param name="globalDataGroup"></param>
		/// <param name="language"></param>
		/// <param name="countryCode"></param>
		/// <returns></returns>
		public List<GlobalDataVO> GetGlobalDatas(GlobalDataGroup globalDataGroup, Language language, string countryCode)
		{
			return (from gd in GlobalData where gd.GroupID == (int)globalDataGroup && gd.LanguageID == (int)language && gd.CountryCodes != null && gd.CountryCodes.Contains(countryCode) select gd).ToList<GlobalDataVO>();
		}

		/// <summary>
		/// Returns cached global values by group and language.
		/// </summary>
		/// <param name="globalDataGroup"></param>
		/// <param name="language"></param>
		/// <returns></returns>
		public List<GlobalDataVO> GetGlobalDatas(GlobalDataGroup globalDataGroup, Language language)
		{
			return (from gd in GlobalData where gd.GroupID == (int)globalDataGroup && gd.LanguageID == (int)language select gd).ToList<GlobalDataVO>();
		}

		public GlobalDataVO GetGlobalData(GlobalDataGroup globalDataGroup, Language language, int ID)
		{
			return (from gd in GlobalData where gd.GroupID == (int)globalDataGroup && gd.LanguageID == (int)language && gd.ID == ID select gd).SingleOrDefault();
		}

		/// <summary>
		/// Returns a cached global data value.
		/// </summary>
		/// <param name="globalDataGroup"></param>
		/// <param name="language"></param>
		/// <param name="ID"></param>
		/// <returns></returns>
		public string GetGlobalDataValue(GlobalDataGroup globalDataGroup, Language language, int ID)
		{
			GlobalDataVO gdvo = (from gd in GlobalData where gd.GroupID == (int)globalDataGroup && gd.LanguageID == (int)language && gd.ID == ID select gd).SingleOrDefault();
			if (gdvo == null)
				return "";
			else
				return gdvo.Value;
		}

		/// <summary>
		/// Returns a cached global data description.
		/// </summary>
		/// <param name="globalDataGroup"></param>
		/// <param name="language"></param>
		/// <param name="ID"></param>
		/// <returns></returns>
		public string GetGlobalDataDescription(GlobalDataGroup globalDataGroup, Language language, int ID)
		{
			GlobalDataVO gdvo = (from gd in GlobalData where gd.GroupID == (int)globalDataGroup && gd.LanguageID == (int)language && gd.ID == ID select gd).SingleOrDefault();
			if (gdvo == null)
				return "";
			else
				return gdvo.Description;
		}

		public Dictionary<int, KeyValuePair<string, List<CountryVO>>> CountryGroups
		{
			get {
				if (_countryGroups == null) {
					NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
					_countryGroups = (from c in dc.CountryGroups select c).ToDictionary(a => (int) a.CountryGroup_id, a => new KeyValuePair<string, List<CountryVO>>(a.Name, a.list_CountryListToCountryGroups.Select(c => GetCountry(c.CountryISOCode)).ToList()));
				}
				return _countryGroups;
			}
		}

		public List<CountryVO> Countries
		{
			get
			{
				if (_countries == null)
				{
					lock (_syncRoot)
					{
						if (_countries == null)
						{
							_countries = new List<CountryVO>();

							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							var counties = from c in dc.CountryLists select c;
							foreach (Netpay.Dal.Netpay.CountryList currentCountry in counties)
							{
								CountryVO countryVo = new CountryVO();
								countryVo.ID = currentCountry.CountryID.Value;
								countryVo.IsoCode2 = currentCountry.CountryISOCode;
								countryVo.IsoCode3 = currentCountry.ISOCode3;
								countryVo.IbanLength = currentCountry.IBANLength;
								countryVo.IsAbaRequired = currentCountry.IsABARequired;
								countryVo.IsSepa = currentCountry.IsSEPA;
								countryVo.Name = currentCountry.Name;
								_countries.Add(countryVo);
							}
						}
					}
				}

				return _countries;
			}
		}

		public string GetStateName(int? stateID)
		{
			if (stateID == null)
				return "";

			StateVO state = GetState(stateID.Value);
			if (state == null)
				return "";

			return state.Name;
		}

        public CountryVO GetCountryByName(string countryName)
        {
            if (countryName == null || countryName.Trim() == "")
                return null;

            CountryVO found = GetCountries().Where(c => c.Name.ToLower() == countryName.ToLower()).SingleOrDefault();
            return found;
        }

        public StateVO GetStateByName(string stateName)
        {
            if (stateName == null || stateName.Trim() == "")
                return null;

            StateVO found = GetStates().Where(s => s.Name.ToLower() == stateName.ToLower()).SingleOrDefault();
            return found;
        }

		public string GetCountryName(string countryCode)
		{
			if (countryCode == null || countryCode.Trim() == "")
				return "";

			CountryVO country = GetCountry(countryCode);
			if (country == null)
				return "";

			return country.Name;
		}

		public string GetCountryName(Language language, int? countryID)
		{
			if (countryID == null)
				return "";

			CountryVO country = GetCountry(countryID.Value);
			if (country == null)
				return "";

			return country.Name;
		}

		/// <summary>
		/// Gets a country by country id and language.
		/// </summary>
		/// <returns></returns>
		public CountryVO GetCountry(int countryID)
		{
			return Countries.Where(c => c.ID == countryID).SingleOrDefault();
		}

		public CountryVO GetCountry(string isoCode2)
		{
			if (isoCode2 == null)
				return null;
			
			return Countries.Where(c => c.IsoCode2.Trim().ToLower() == isoCode2.Trim().ToLower()).SingleOrDefault();
		}

		public CountryVO GetCountry(IPAddress ip)
		{
			byte[] splitIP = ip.GetAddressBytes();
			long calculatedIP = (splitIP[0] * (long)System.Math.Pow(256, 3)) + (splitIP[1] * (long)System.Math.Pow(256, 2)) + (splitIP[2] * 256) + splitIP[3];
			NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
			string isoCode = (from gip in dc.tblGeoIPs where gip.GI_Start <= calculatedIP && gip.GI_End >= calculatedIP  orderby gip.GI_Diff ascending select gip.GI_IsoCode).FirstOrDefault();
			if (isoCode == null) return null;
			return GetCountry(isoCode);
		}

		/// <summary>
		/// Returns a cached list of all countries by languages.
		/// </summary>
		/// <param name="language"></param>
		/// <returns></returns>
		public List<CountryVO> GetCountries()
		{
			return Countries.OrderBy(c => c.Name).ToList<CountryVO>();
		}

		/// <summary>
		/// Gets available countries for a payment method type.
		/// Some payment methods are only available for certain countries.
		/// </summary>
		/// <param name="language"></param>
		/// <param name="paymentMethodType"></param>
		/// <returns></returns>
		public IEnumerable<CountryVO> GetCountries(PaymentMethodType paymentMethodType)
		{
			var pmcc = PaymentMethods.Where(pm => pm.Type == paymentMethodType).SelectMany(pm => pm.CurrencyCountry).ToList();
            return (from c in pmcc select GetCountry(c.CountryID.GetValueOrDefault())).Distinct();
		}

		/// <summary>
		/// Returns cached states list by language.
		/// </summary>
		/// <returns></returns>
		private List<StateVO> States
		{
			get
			{
				if (_states == null)
				{
					lock (_syncRoot)
					{
						if (_states == null)
						{
							_states = new List<StateVO>();

							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							var states = from s in dc.StateLists select s;
							foreach (Netpay.Dal.Netpay.StateList currentState in states)
							{
								StateVO stateVo = new StateVO();
                                stateVo.Name = currentState.Name;
                                stateVo.CountryID = currentState.list_CountryList.CountryID.Value;
                                stateVo.ID = currentState.StateID.Value;
                                stateVo.IsoCode = currentState.StateISOCode;
                                _states.Add(stateVo);
							}
						}
					}
				}

				return _states;
			}
		}

		public List<StateVO> GetStates()
		{
			return States.OrderBy(c => c.Name).ToList<StateVO>();
		}

		public List<StateVO> GetStates(int countryID)
		{
			return States.Where(s => s.CountryID == countryID).OrderBy(c => c.Name).ToList<StateVO>();
		}

		public StateVO GetState(int stateID)
		{
			return States.Where(s => s.ID == stateID).SingleOrDefault();
		}

		public StateVO GetState(string isoCode)
		{
			return States.Where(s => s.IsoCode == isoCode).SingleOrDefault();
		}

		public List<TransactionAmountType> GetTransactionAmountGroup(TransactionAmountGroup group) 
		{
			var retlist = TransactionAmountGroups.Where(g => g.Key == group).FirstOrDefault();
			if (retlist.Value == null) return new List<TransactionAmountType>();
			return retlist.Value.AmountTypes;
		}

		public Dictionary<TransactionAmountGroup, TransactionAmountGroupVO> TransactionAmountGroups
		{
			get
			{
				Dictionary<TransactionAmountGroup, TransactionAmountGroupVO> groups = (Dictionary<TransactionAmountGroup, TransactionAmountGroupVO>)HttpRuntime.Cache.Get("TransactionAmountGroupCollection");

				if (groups == null)
				{
					lock (_syncRoot)
					{
						if (groups == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							groups = (from etg in dc.TransAmountTypeGroups select etg).ToDictionary(g => (TransactionAmountGroup)g.TransAmountTypeGroup_id, g => new TransactionAmountGroupVO(g));
							HttpRuntime.Cache.Add("TransactionAmountGroupCollection", groups, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return groups;
			}
		}

        /// <summary>
        /// Collection with key = affiliate id & value = affiliate name
        /// </summary>
        public Dictionary<int, string> Affiliates
        {
            get
            {
                Dictionary<int, string> affiliates = (Dictionary<int, string>)HttpRuntime.Cache.Get("affiliatesCollection");
                if (affiliates == null)
                {
                    lock (_syncRoot)
                    {
                        if (affiliates == null)
                        {
                            NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
                            affiliates = (from a in dc.tblAffiliates select new {a.affiliates_id, a.name}).ToDictionary(kvp => kvp.affiliates_id, kvp => kvp.name);
                            HttpRuntime.Cache.Add("affiliatesCollection", affiliates, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
                        }
                    }
                }

                return affiliates;
            }
        }

		public string GetAffiliateName(int? affiliateId)
		{
			if (affiliateId == null) return null;
			if (Affiliates.ContainsKey(affiliateId.Value)) return null;
			return Affiliates[affiliateId.Value];
		}

        /// <summary>
        /// Collection with key = trans source id & value = trans source name
        /// </summary>
        public Dictionary<byte, string> TransactionSources
        {
            get
            {
                Dictionary<byte, string> transSources = (Dictionary<byte, string>)HttpRuntime.Cache.Get("transSourcesCollection");
                if (transSources == null)
                {
                    lock (_syncRoot)
                    {
                        if (transSources == null)
                        {
                            NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
                            transSources = (from e in dc.TransSources select new { e.TransSource_id, e.Name }).ToDictionary(kvp => kvp.TransSource_id, kvp => kvp.Name);
                            HttpRuntime.Cache.Add("transSourcesCollection", transSources, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
                        }
                    }
                }

                return transSources;
            }
        }

        /// <summary>
        /// Collection with key = department id & value = department name
        /// </summary>
        public Dictionary<byte, string> MerchnatsDepartments
        {
            get
            {
                Dictionary<byte, string> merchantsDepartments = (Dictionary<byte, string>)HttpRuntime.Cache.Get("merchantsDepartmentsCollection");
                if (merchantsDepartments == null)
                {
                    lock (_syncRoot)
                    {
                        if (merchantsDepartments == null)
                        {
                            NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
                            merchantsDepartments = (from e in dc.MerchantDepartments select new { e.MerchantDepartment_id, e.Name }).ToDictionary(kvp => kvp.MerchantDepartment_id, kvp => kvp.Name);
                            HttpRuntime.Cache.Add("merchantsDepartmentsCollection", merchantsDepartments, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
                        }
                    }
                }

                return merchantsDepartments;
            }
        }
		/*
		/// <summary>
		/// Returns a cached list of all merchants.
		/// Cache refresh every 1 hrs.
		/// If updated merchant data is needed, use Merchants.GetMerchant()
		/// </summary>
		public Dictionary<string, MerchantVO> Merchants
		{
			get
			{
				Dictionary<string, MerchantVO> merchants = (Dictionary<string, MerchantVO>)HttpRuntime.Cache.Get("merchantsCollection");

				if (merchants == null)
				{
					lock (_syncRoot)
					{
						if (merchants == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							List<Netpay.Dal.Netpay.tblCompany> entities = (from m in dc.tblCompanies select m).ToList();
							merchants = entities.Select(m => new MerchantVO(m)).ToDictionary(m => m.Number);
							HttpRuntime.Cache.Add("merchantsCollection", merchants, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return merchants;
			}
		}
		*/
		/*
		/// <summary>
		/// Returns a cached list of all merchants.
		/// </summary>
		public Dictionary<int, MerchantVO> MerchantsByIDs
		{
			get
			{
				Dictionary<int, MerchantVO> merchants = (Dictionary<int, MerchantVO>)HttpRuntime.Cache.Get("merchantsByIDsCollection");

				if (merchants == null)
				{
					lock (_syncRoot)
					{
						if (merchants == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							List<Netpay.Dal.Netpay.tblCompany> entities = (from m in dc.tblCompanies select m).ToList();
							merchants = Merchants.Values.ToDictionary(m => m.ID);
							HttpRuntime.Cache.Add("merchantsByIDsCollection", merchants, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return merchants;
			}
		}

		public MerchantVO GetMerchant(string merchantNumber)
		{
			if (!Merchants.ContainsKey(merchantNumber))
				return null;

			return Merchants[merchantNumber];
		}

		public MerchantVO GetMerchant(int merchantID)
		{
			if (!MerchantsByIDs.ContainsKey(merchantID))
				return null;

			return MerchantsByIDs[merchantID];
		}

		public string GetMerchantName(int? merchantID)
		{
			if (merchantID == null) return null;
			if (!MerchantsByIDs.ContainsKey(merchantID.Value)) return null;
			return MerchantsByIDs[merchantID.Value].Name;
		}

		public List<MerchantVO> GetMerchants(List<int> merchantsIDs)
		{
			return MerchantsByIDs.Where(kvp => merchantsIDs.Contains(kvp.Key)).Select(kvp => kvp.Value).ToList();
		}

		public List<MerchantVO> GetMerchants(Guid credentialsToken)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.Partner, UserType.NetpayUser, UserType.NetpayAdmin });
            if ((user.AllowedMerchantsIDs == null || user.AllowedMerchantsIDs.Count == 0) && (user.Type == UserType.NetpayAdmin || user.Type == UserType.NetpayUser))
                return user.Domain.Cache.Merchants.Values.ToList();
            return GetMerchants(user.AllowedMerchantsIDs);
		}
		*/

		/// <summary>
		/// Returns a cached list of all currencies.
		/// </summary>
        public Dictionary<int, FileItemTypeVO> FileTypes
        {
            get {
                if (_fileTypes == null) {
                    NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
                    _fileTypes = (from c in dc.FileItemTypes select new FileItemTypeVO(c)).ToDictionary<FileItemTypeVO, int>(c => c.ID);
                }
                return _fileTypes;
            }
        
        }

		public string GetFileTypeText(int? typeId)
		{
			if(typeId == null) return string.Empty;
			if(!FileTypes.ContainsKey(typeId.Value)) return string.Empty;
			return FileTypes[typeId.Value].Name;
		}

		/// <summary>
		/// Returns a cached list of all currencies.
		/// </summary>
		public Dictionary<int, CurrencyVO> Currencies
		{
			get
			{
				Dictionary<int, CurrencyVO> currencies = (Dictionary<int, CurrencyVO>)HttpRuntime.Cache.Get("currenciesCollection");

				if (currencies == null)
				{
					lock (_syncRoot)
					{
						if (currencies == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							currencies = (from c in dc.tblSystemCurrencies select new CurrencyVO { ID = c.CUR_ID, IsSymbolBeforeAccount=c.CUR_IsSymbolBeforeAmount, IsoNumber = c.CUR_ISOCode, IsoCode = c.CUR_ISOName, ConversionFee = (decimal) c.CUR_ExchangeFeeInd, Name = c.CUR_FullName, Symbol = HttpUtility.HtmlDecode(c.CUR_Symbol), BaseRate = c.CUR_BaseRate }).OrderBy(c => c.ID).ToDictionary<CurrencyVO, int>(c => c.ID);
							HttpRuntime.Cache.Add("currenciesCollection", currencies, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 10, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return currencies;
			}
		}

		/// <summary>
		/// Returns a cached currency by id.
		/// </summary>
		/// <param name="currencyID"></param>
		/// <returns></returns>
		public CurrencyVO GetCurrency(int currencyID)
		{
			return Currencies[currencyID];
		}

		public CurrencyVO GetCurrency(string isoCode)
		{
			return (from c in Currencies where c.Value.IsoCode == isoCode select c.Value).SingleOrDefault();
		}

		public CurrencyVO GetCurrencyByIsoNumber(int isoNumber)
		{
			return (from c in Currencies where c.Value.IsoNumber == isoNumber select c.Value).SingleOrDefault();
		}

		public List<CurrencyVO> GetCurrencies(int[] currencyIDs)
		{
			List<CurrencyVO> results = (from c in Currencies where currencyIDs.Contains(c.Value.ID) select c.Value).ToList();
			return results;
		}

		/// <summary>
		/// Returns the cached list of transaction sources.
		/// </summary>
		/// <param name="language"></param>
		/// <returns></returns>
		public List<TransactionSourceVO> GetTransactionSources()
		{
			if (_transactionSources == null)
			{
				lock (_syncRoot)
				{
					if (_transactionSources == null)
					{
						_transactionSources = new List<TransactionSourceVO>();

						NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
						var transactionSources = from ts in dc.TransSources select ts;
						foreach (Netpay.Dal.Netpay.TransSource currentSource in transactionSources)
						{
							/*
                            TransactionSourceVO hebSource = new TransactionSourceVO();
							hebSource.Name = currentSource.NameHeb;
							hebSource.ID = currentSource.TransSource_id;
							hebSource.Language = Language.Hebrew;
							_transactionSources.Add(hebSource);
                            */

							TransactionSourceVO source = new TransactionSourceVO();
							source.Name = currentSource.Name;
							source.ID = currentSource.TransSource_id;
							//source.Language = Language.English;
							_transactionSources.Add(source);
						}
					}
				}
			}

			return _transactionSources.ToList<TransactionSourceVO>();
		}

		/// <summary>
		/// Gets a transaction source by language and id.
		/// </summary>
		/// <param name="language"></param>
		/// <param name="transactionSourceID"></param>
		/// <returns></returns>
		public TransactionSourceVO GetTransactionSource(int transactionSourceID)
		{
			return GetTransactionSources().Where(ts => ts.ID == transactionSourceID).SingleOrDefault();
		}

		/*
		/// <summary>
		/// Returns a cached list of all terminals.
		/// </summary>

		public List<TerminalVO> Terminals
		{
			get
			{
				List<TerminalVO> terminals = (List<TerminalVO>)HttpRuntime.Cache.Get("terminalsCollection");

				if (terminals == null)
				{
					lock (_syncRoot)
					{
						if (terminals == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							terminals = (from t in dc.tblDebitTerminals select new TerminalVO { TerminalID = t.id, DebitCompany = t.DebitCompany, TerminalNumber = t.terminalNumber, IsActive = t.isActive, TerminalName = t.dt_name, ContractNumber = t.dt_ContractNumber }).OrderBy(t => t.TerminalName).ToList<TerminalVO>();
							HttpRuntime.Cache.Add("terminalsCollection", terminals, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return terminals;
			}
		}
		*/
		/// <summary>
		/// Returns a cached list of all debit companies.
		/// </summary>
		public List<EpaDebitCompanyVO> EpaDebitCompanies
		{
			get
			{
				List<EpaDebitCompanyVO> debitCompanies = (List<EpaDebitCompanyVO>)HttpRuntime.Cache.Get("epaDebitCompaniesCollection");

				if (debitCompanies == null)
				{
					lock (_syncRoot)
					{
						if (debitCompanies == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							debitCompanies = (from edc in dc.tblEpaDebitCompanies select new EpaDebitCompanyVO(edc)).ToList<EpaDebitCompanyVO>();
							HttpRuntime.Cache.Add("epaDebitCompaniesCollection", debitCompanies, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return debitCompanies;
			}
		}
		/*
		/// <summary>
		/// Returns a cached list of all debit companies.
		/// </summary>
		public List<DebitCompanyVO> DebitCompanies
		{
			get
			{
				List<DebitCompanyVO> debitCompanies = (List<DebitCompanyVO>)HttpRuntime.Cache.Get("debitCompaniesCollection");

				if (debitCompanies == null)
				{
					lock (_syncRoot)
					{
						if (debitCompanies == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							debitCompanies = (from t in dc.tblDebitCompanies select t).OrderBy(t => t.dc_name).Select(t => new DebitCompanyVO(t)).ToList();
							HttpRuntime.Cache.Add("debitCompaniesCollection", debitCompanies, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return debitCompanies;
			}
		}
		*/
		/*
		/// <summary>
		/// Returns a cached list of payment banks (not debit companies).
		/// </summary>
		public List<BankVO> Banks
		{
			get
			{
				List<BankVO> banks = (List<BankVO>)HttpRuntime.Cache.Get("banksCollection");

				if (banks == null)
				{
					lock (_syncRoot)
					{
						if (banks == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							banks = (from b in dc.tblSystemBankLists orderby b.bankName select new BankVO(b)).ToList();
							HttpRuntime.Cache.Add("banksCollection", banks, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return banks;
			}
		}
		*/
		/// <summary>
		/// Returns a cached list of all bll log tags.
		/// </summary>
		/*
		[Obsolete("Use enum LogTag")]
		public List<string> LogTags
		{
			get
			{
				List<string> bllLogTags = (List<string>)HttpRuntime.Cache.Get("bllLogTagsCollection");

				if (bllLogTags == null)
				{
					lock (_syncRoot)
					{
						if (bllLogTags == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							bllLogTags = (from l in dc.tblBllLogs where l.Tag != null select l.Tag).Distinct().OrderBy(l => l).ToList<string>();
							HttpRuntime.Cache.Add("bllLogTagsCollection", bllLogTags, null, Cache.NoAbsoluteExpiration, new TimeSpan(5, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return bllLogTags;
			}
		}
		*/

		/// <summary>
		/// Returns a cached list of all merchant groups.
		/// </summary>
		public List<KeyValueVO<int, string>> MerchantGroups
		{
			get
			{
				List<KeyValueVO<int, string>> groups = (List<KeyValueVO<int, string>>)HttpRuntime.Cache.Get("merchantGroupsCollection");

				if (groups == null)
				{
					lock (_syncRoot)
					{
						if (groups == null)
						{
							NetpayDataContext dc = new NetpayDataContext(_domain.Sql1ConnectionString);
							groups = (from mg in dc.tblMerchantGroups select new KeyValueVO<int, string>() { Key = mg.ID, Value = mg.mg_Name }).ToList<KeyValueVO<int, string>>();
							HttpRuntime.Cache.Add("merchantGroupsCollection", groups, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return groups;
			}
		}

		/// <summary>
		/// Returns the latest denormalized transaction date.
		/// </summary>
		public DateTime DenormalizedMaxDate
		{
			get
			{
				DateTime? maxDate = (DateTime?)HttpRuntime.Cache.Get("denormalizedMaxDate");

				if (maxDate == null)
				{
					lock (_syncRoot)
					{
						if (maxDate == null)
						{
							ReportsDataContext dc = new ReportsDataContext(_domain.ReportsConnectionString);
							maxDate = (from md in dc.DenormalizedTransactions select md).Max(md => md.TransactionDate);
							if (maxDate == null)
								maxDate = DateTime.Now;
							HttpRuntime.Cache.Add("denormalizedMaxDate", maxDate, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return maxDate.Value;
			}
		}

		/// <summary>
		/// Returns the earliest denormalized transaction date.
		/// </summary>
		public DateTime DenormalizedMinDate
		{
			get
			{
				DateTime? minDate = (DateTime?)HttpRuntime.Cache.Get("denormalizedMinDate");

				if (minDate == null)
				{
					lock (_syncRoot)
					{
						if (minDate == null)
						{
							ReportsDataContext dc = new ReportsDataContext(_domain.ReportsConnectionString);
							minDate = (from md in dc.DenormalizedTransactions select md).Min(md => md.TransactionDate);
							if (minDate == null)
								minDate = DateTime.Now;
							HttpRuntime.Cache.Add("denormalizedMinDate", minDate, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
						}
					}
				}

				return minDate.Value;
			}
		}
	}
}
