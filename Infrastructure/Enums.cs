using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Netpay.CommonTypes;

namespace Netpay.Infrastructure
{
    public static class Enums
    {
        public static System.Drawing.Color[] TwoSateColor = new System.Drawing.Color[] { System.Drawing.Color.Green, System.Drawing.Color.Red };
        public static System.Drawing.Color[] TreeSateColor = new System.Drawing.Color[] { System.Drawing.Color.Silver, System.Drawing.Color.Green, System.Drawing.Color.Red };
        public static Dictionary<bool, System.Drawing.Color> BoolColorMap = new Dictionary<bool, System.Drawing.Color>() { { true, System.Drawing.Color.Green }, { false, System.Drawing.Color.Red } };

        public static SortedList<string, int> GetEnumDataSource<T>() where T : struct
        {
            Type myEnumType = typeof(T);
            if (myEnumType.BaseType != typeof(Enum))
            {
                throw new ArgumentException("Type T must inherit from System.Enum.");
            }

            SortedList<string, int> returnCollection = new SortedList<string, int>();
            string[] enumNames = Enum.GetNames(myEnumType);
            for (int i = 0; i < enumNames.Length; i++)
            {
                returnCollection.Add(enumNames[i],
                    (int)Enum.Parse(myEnumType, enumNames[i]));
            }

            return returnCollection;
        }

        public static SortedList<string, int> GetEnumDataSource<T>(string exceptKey) where T : struct
        {
            Type myEnumType = typeof(T);
            if (myEnumType.BaseType != typeof(Enum))
            {
                throw new ArgumentException("Type T must inherit from System.Enum.");
            }

            SortedList<string, int> returnCollection = new SortedList<string, int>();
            string[] enumNames = Enum.GetNames(myEnumType);
            for (int i = 0; i < enumNames.Length; i++)
            {
                if (enumNames[i] != exceptKey)
                {
                    returnCollection.Add(enumNames[i],
                        (int)Enum.Parse(myEnumType, enumNames[i]));
                }
            }

            return returnCollection;
        }

        public static SortedList<string, int> GetEnumSpecificEntry<T>(string specificKey) where T : struct
        {
            Type myEnumType = typeof(T);
            if (myEnumType.BaseType != typeof(Enum))
            {
                throw new ArgumentException("Type T must inherit from System.Enum.");
            }

            SortedList<string, int> returnCollection = new SortedList<string, int>();
            string[] enumNames = Enum.GetNames(myEnumType).Where(x => x == specificKey).ToArray();
            if (enumNames.Count() > 0)
            {
                returnCollection.Add((enumNames[0]),
                    (int)Enum.Parse(myEnumType, enumNames[0]));
            }

            return returnCollection;
        }

        public static CreditCardType GetCreditCardType(short creditCardTypeID)
        {
            return (CreditCardType)Enum.Parse(typeof(CreditCardType), creditCardTypeID.ToString());
        }

        /// <summary>
        /// Returns the language enum from a locale string
        /// </summary>
        /// <param name="locale"></param>
        /// <returns></returns>
        public static Language GetLanguage(string locale)
        {
            locale = locale.ToLower();
            switch (locale)
            {
                case "he-il":
                    return Language.Hebrew;
                case "en-us":
                    return Language.English;
                default:
                    return Language.Unknown;
            }
        }

        /// <summary>
        /// Returns the language enum from culture info
        /// </summary>
        /// <param name="culture"></param>
        /// <returns></returns>
        public static Language GetLanguage(CultureInfo culture)
        {
            if (Enum.GetNames(typeof(Language)).Contains(culture.Parent.EnglishName))
                return (Language)Enum.Parse(typeof(Language), culture.Parent.EnglishName);
            else
                return Language.Unknown;
        }

        public static PaymentsStatus GetPaymentsStatus(string paymentsIDs)
        {
            PaymentsStatus paymentsStatus = PaymentsStatus.None;
            if (paymentsIDs == null)
            {
                paymentsStatus = PaymentsStatus.None;
            }
            else if (paymentsIDs.Contains(";X;"))
            {
                paymentsStatus = PaymentsStatus.Archived;
            }
            else if (paymentsIDs.Replace(";", "") == "0")
            {
                paymentsStatus = PaymentsStatus.Unsettled;
            }
            else if (!paymentsIDs.Contains(";0;"))
            {
                paymentsStatus = PaymentsStatus.Settled;
            }
            else if (paymentsIDs.Contains(";0;"))
            {
                paymentsStatus = PaymentsStatus.PartiallySettled;
            }

            return paymentsStatus;
        }
    }

    public enum ActiveOrNot
    {
        Active = 0,
        NotActive = 1
    }

    public enum SuccessOrFail
    {
        Success = 0,
        Fail = 1
    }

    public enum Gender
    {
        Unknown = 0,
        Male = 1,
        Female = 2
    }

    public enum ExternalCardProviderEnum
    {
        Unknown = 0,
        Payoneer = 1,
        LiveKash = 2
    }

    public enum ExternalCardCustomerStatus
    {
        Unknown = 0,
        Added = 1,
        RegistrationEmailSent = 2,
        Registered = 3, // payee has completed registration
        Approved = 4, // card approved, can load payee card
        Declined = 5, // card declined, cannot load payee card
        RegistrationEmailFailed = 6,
        Activated = 7 // payee activated card and can use it
    }

    /*
	public enum PayoneerPaymentStatus
	{
		Unknown = 0
	}
	*/

    public enum MerchantDepositState
    {
        None = 0,
        HoldDynamically = 1,
        HoldSingle = 2,
        HoldFixedAmount = 3,
        HoldExternal = 4
    }

    public enum PublicPayChargeOptions
    {
        Custom = 0,
        Selection = 1,
        Both = 2
    }

    public enum BlockedItemType
    {
        Unknown = 0,
        Email = 1,
        FullName = 2,
        Phone = 3,
        PersonalNumber = 4,
        Bin = 5,
        BinCountry = 6
    }

    public enum TranslationGroup
    {
        HostedPaymentPage = 1,
        PublicPaymentPage = 2,
    }

    public enum TransactionType
    {
        Unknown = -1,
        Capture = 0,
        Authorization = 1,
        AuthorizationCapture = 2,
        StoredCreditCardCapture = 3,
        PendingCapture = 4
    }

    public enum MerchantStatus
    {
        Archived = 0,
        New = 1,
        Blocked = 2,
        Closed = 3,
        LoginOnly = 10,
        Integration = 20,
        Processing = 30
    }

    public enum CartStatus
    {
        OnGoing = 0,
        Pending = 1,
        Authorized = 2,
        Approved = 3,
        Declined = 4,
    }

    public enum SecurityObjectType
    {
        User = 1,
        Group = 2
    }

    public enum PermissionObjectType
    {
        WebPage = 1,
        WebFeature = 2,
        PartnerControlPanelWebPage = 3
    }

    public enum PermissionObjects
    {
        TransactionPrintSlip,
        RefundRequest,
        CreateRecurringSeries,
        AddTransactionCardToBacklist,
        ShowTotals,
        SwitchLinkedMerchants,
        ShowMainPageNotifications,
    }

    public enum BlockedItemSource
    {
        Unknown = -1,
        Netpay = 0,
        Merchant = 1
    }

    public enum BlockedCardLevel
    {
        Unknown = -1,
        Merchant = 0,
        SystemPermanent = 1,
        SystemTemporary = 2
    }

    public enum BeneficiaryPaymentResult
    {
        AmountTooLow = 1,
        AmountTooHigh = 2,
        Success = 3
    }

    public enum SortDirection
    {
        Ascending = 1,
        Descending = 2
    }

    public enum LogSeverity
    {
        Info = 1,
        Warning = 2,
        Error = 3
    }

    public enum LogTag
    {
        MerchantControlPanel,
        DevCenter,
        NetpayAdmin,
        DataAccess,
        Security,
        AppInit,
        Exception,
        TaskInvocation,
        TaskScheduler,
        Ssis,
        Ssas,
        None,
        PaymentPage,
        RiskAlerts,
        Epa,
        Chb,
        Fraud,
        AuthBatch,
        Reports,
        Process,
        PostProcess,
        Monitoring,
        AdminCash,
        Updates,
        ExternalCards,
        MobileApp,
        Wallet,
        WebService,
        Wires,
        DevDocs,
        WebApi,
        WebServiceLog
    }

    public enum ScheduleInterval
    {
        Minute = 1,
        Hour = 2,
        Day = 3,
        Week = 4,
        Month = 5,
        Year = 6,
        Quarter = 7
    }

    public enum FailedTransactionType
    {
        Debit = 0, // Sale - Red
        PreAuthorized = 1,  // Authorization - #ff6666(red) / #ffffff(white)
        PreAuthorizedCapture = 2, //Capture -  #ff6666(red) / #c5c5c5(gray)
        StoredCardDebit = 3 // Sale - Red
    }

    public enum DataSource
    {
        Sql1 = 1,
        Sql2 = 2,
        Archive = 3
    }

    public enum TransactionStatus
    {        
        Captured = 1,
        Declined = 2,
        Authorized = 3,
        Pending = 4,
        DeclinedArchived = 5
    }

    public enum CreditType
    {
        Unknown = -1,
        Refund = 0,
        Regular = 1,
        DelayedCharge = 2,
        CreditCharge = 6,
        Installments = 8,
        Debit = 30,
        Credit = 31
    }

    public enum TransactionSource
    {
        Unknown = 0,
        ApprovalFee = 1,
        CcStorageFee = 2,
        EcheckFailedFee = 3,
        SystemCharge = 5,
        SystemRefund = 6,
        ManualChargeHebrew = 7,
        RemoteCharge = 8,
        PopupCharge = 9,
        AdminCharge = 10,
        PopupChargeHebrew = 11,
        PublicChargeHebrew = 12,
        IvrCharge = 14,
        HandCharge = 15,
        PhoneRefund = 16,
        ApprovedCharge = 17,
        ManualCharge = 18,
        PublicCharge = 19,
        Recurring = 20,
        MakeingAPayment = 21,
        BatchFile = 22,
        PendingTransaction = 23,
        TestFromAdmin = 24,
        FailedFee = 25,
        DebitCards = 26,
        ChargeWithoutProcess = 27,
        RemoteChargeCcStorage = 28,
        AutoCapture = 29,
        AnnualFee = 30,
        MonthlyFee = 31,
        HostedPageV2He = 32,
        HostedPageV2En = 33,
        MobileWeb = 40,
        MobileApp = 41,
        WalletMobileApp = 42,
        DailyFee = 43,
        WeeklyFee = 44,
        SemiMonthlyFee = 45,
        QuarterYearlyFee = 46,
        SemiYearlyFee = 47,
        FraudFee = 50
    }

    public enum DeniedStatus
    {
        ValidTransaction = 0,
        UnsettledInVerification = 1,
        SettledInVerification = 2,
        UnsettledBeenSettledAndValid = 3, //
        UnsettledBeenSettledAndDeducted = 4,
        DuplicateValid = 5, //
        DuplicateDeducted = 6,
        RefundedValid = 7,
        RefundedChargedBack = 8,
        SetFoundValidRefunded = 9,     //photocopy unsettled then refunded (instead of chargeback)
        WasWorkedOut = 10,
        DuplicateTransactionWorkedOut = 11,
        DupWasWorkedOutRefunded = 12
    }

    public enum RejectingSource
    {
        Unknown = -1,

        // rejected by the bank
        Issuer = 0,

        // rejected by netpay due to high risk
        Risk = 1,

        // rejected by netpay due to incomplete or invalid data
        Gateway = 2
    }

    public enum CreditCardType
    {
        Unknown = 0,
        Isracard = 1,
        Visa = 2,
        Diners = 3,
        AmericanExpress = 4,
        Mastercard = 5,
        Direct = 6,
        Other = 7,
        Maestro = 8,
        ToggleCard = 9,
        eCheck = 10,
        JCB = 11
    }

    public enum RollingReserveState
    {
        None = 0,
        Dynamic = 1,
        Single = 2,
        Fixed = 3,
        External = 4
    }

    public enum GlobalDataGroup
    {
        Group = 0,
        CcStorageReturnCode = 34,
        PasswordChangeAttemptResult = 35,
        PaymentMethodType = 36,
        TransactionType = 37,
        WhiteListReply = 38,
        WhiteListLevel = 39,
        //WireStatus = 40,
        DeniedStatus = 41,
        CreditType = 43,
        MerchantStatus = 44,
        MerchantBalanceStatus = 45,
        RejectingSource = 46,
        CommonBlockTypes = 47,
        WireTransferStatus = 48,
        TranspassAction = 49,
        PendingFilesStatus = 50,
        Industry = 51,
        BeneficiaryProfileType = 52,
        BalanceSource = 53,
        RecurringModificationStatus = 54,
        RollingReserveState = 55,
        DocumentType = 60,
        LimitedLoginPermissions = 61,
        RecurringChargeStatus = 62,
        //RefundRequestStatus = 63,
        RefundRequestReplyCode = 64,
        AutorefundStatus = 65,
        IntervalUnit = 66,
        CustomerBalanceSource = 67,
        ManagingCompany = 69,
        Psp = 70
    }

    public enum MerchantBeneficiaryProfileType
    {
        MerchantCard = 0,
        SelfPay = 1,
        Salary = 2,
        Supplier = 3,
        PrivateMember = 4,
        Other = 9,
        Payoneer = 10
    }


    public enum CreateLimitedUserResult
    {
        Success = 1,
        WeakPassword = 2,
        UserAlreadyExists = 3,
        InvalidUserName = 4
    }

    //the "PaymentsStatus" + string must correspond to the respective entry in the Multilang resource
    public enum PaymentsStatus
    {
        None = 1,
        Archived = 2,
        Unsettled = 3,
        Settled = 4,
        PartiallySettled = 5
    }

    public enum TransactionRefundAbility
    {
        None,
        FullOnly,
        FullOrPartial
    }

    public enum PartnerFeeCalc
    {
        Volume = 0,
        Fees = 1,
        MercSale_TermBuy = 2,
        MercSale_AffiBuy = 3,
        //MercSale_TermBuySPercent = 4,
        MercSale_AffiBuySPercent = 5,
    }

    public enum PartnerTransType { 
        Debit = 0,
        Refund = 1,
        Authorize = 2,
    }

    public enum PhoneMessageDirection
    {
        SystemToUser = 0,
        UserToSystem = 1,
    }

    public enum VerifyTransReturnValues
    {
        TransactionExists = 0,
        InvalidData = 10,
        InvalidMerchant = 11,
        InvalidTransID = 12,
        InvalidTransDate = 13,
        InvalidTransAmount = 14,
        InvalidTransCurrency = 15,

        PendingTransApproved = 16,
        PendingTransFailed = 17,
    }

    public enum DebitCompany
    {
        NetpayLocal = 1,
        MPartners = 2,
        Pelecard = 3,
        PConnexions = 4,
        Cyprus = 5,
        TurkishBank = 6,
        PaymentSystems = 7,
        NetpayPelecard = 8,
        IndependantPelecard = 9,
        CyberSystems = 10,
        ShvaCal = 11,
        Nikos = 12,
        Albis = 13,
        Sbm = 14,
        MauritiusBank = 15,
        Evertec = 16,
        ECard = 17,
        BnS = 18,
        Jcc = 19,
        PrepaidCards = 20,
        Catella = 21,
        Commercialline = 22,
        CSearch = 23,
        ETriton = 24,
        Pago = 25,
        PayOn = 26,
        Clear4Web = 27,
        Inatec = 28,
        Way4 = 29,
        SwissNet = 30,
        DotPay = 31,
        Jcc2 = 32,
        EBanking = 33,
        WireCard = 34,
        CyberBit = 35,
        Wts = 36,
        Webbiling = 37,
        PPro = 38,
        WireCard3D = 39,
        AllCharge = 40,
        BillingPartners = 41,
        InPay = 42,
        Atlas = 43,
        PayVision = 44,
        CredoRax = 45,
        BnS_Ind = 46,
        WebMoney = 47,
        AltCharge = 48,
        DeltaPay = 49,
        TrustPay = 50,
        WebbillingHpp = 51,
        LiveKash = 52,
        Morrocow = 53,
        //Atlas = 54,
        ShvaMobile = 55,
        ShvaIsracard = 56,
        QIWI = 57,
        Allied = 58,
        FirstData = 59,
        CardConnect = 60,
        Actum = 61,
        YuuPay = 62,
        Lamda = 63,
        EXP = 64,
        BSafe = 65,
        TSYS = 66,
        eCentric = 67,
        PayonDirect = 68,
        CreditGuard = 69,
        ACS = 80
        //FNB = 58,
    }

    public enum PeriodicFeeChargeType
    {
        OnlyProcessing = 1,
        ProcessingOrCC = 2,
        OnlyCC = 3
    }

    public enum PeriodicInterval
    {
        Day = 20,
        Week = 30,
        SemiMonth = 40,
        Month = 50,
        Quarter = 60,
        SemiYear = 70,
        Year = 80
    }
}