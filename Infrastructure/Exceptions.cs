﻿using System;

namespace Netpay.Infrastructure
{
	public class NotLoggedinException : ApplicationException
	{
		public NotLoggedinException() :  base() { }
		public NotLoggedinException(string message) : base(message) { }
	}

	public class MethodAccessDeniedException : ApplicationException
	{
		public MethodAccessDeniedException() :  base() { }

		public MethodAccessDeniedException(string message) : base(message) { }
	}

    public class MerchantDuplicateFoundException : ApplicationException
    {
        public MerchantDuplicateFoundException() : base() { }
        
        public MerchantDuplicateFoundException(string message) : base(message) { }
    }
    
    public class ExceptionWithInfo : ApplicationException 
    {
        public string Info { get; private set; }
        public ExceptionWithInfo(string message, string info) : base(message) { Info = info; }
        public ExceptionWithInfo(string message, Exception inner, string info) : base(message, inner) { Info = info; }
        public override string ToString()
        {
            var ret = base.ToString();
            if (!string.IsNullOrEmpty(Info)) ret+= "\r\nMore Info: " + Info;
            return ret;
        }
    }
}
