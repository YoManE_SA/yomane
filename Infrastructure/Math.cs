﻿using System;

namespace Netpay.Infrastructure
{
	public static class Math
	{
		/// <summary>
		/// Returns pecentage difference of 'a' from 'b'
		/// Example: -55.00 is the difference of 90 from 200
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>	
		public static decimal GetPercentageDifference(int a, int b)
		{
			if (a == 0 && b == 0)
				return 0;

			if (b == 0)
				return 100;

			if (a == 0)
				return -100;

			decimal difference = a - b;
			decimal differencePercent = (difference / b) * 100;

			return differencePercent;
		}
		
		/// <summary>
		/// Returns pecentage difference of 'a' from 'b'
		/// Example: -55.00 is the difference of 90 from 200
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>	
		public static decimal GetPercentageDifference(decimal a, decimal b)
		{
			if (a == 0 && b == 0)
				return 0;

			if (b == 0)
				return 100;

			if (a == 0)
				return -100;

			decimal difference = a - b;
			decimal differencePercent = (difference / b) * 100;

			return differencePercent;
		}

		/// <summary>
		/// Returns pecentage difference of 'a' from 'b'
		/// Example: -55.00 is the difference of 90 from 200
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>	
		public static double GetPercentageDifference(double a, double b)
		{
			if (a == 0 && b == 0)
				return 0;

			if (b == 0)
				return 100;

			if (a == 0)
				return -100;

			double difference = a - b;
			double differencePercent = (difference / b) * 100;

			return differencePercent;
		}

		/// <summary>
		/// Returns what pecentage 'a' is of 'b'
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>		
		public static decimal GetPercentage(decimal a, decimal b)
		{
			if (a == 0 || b == 0)
				return 0;

			decimal percent = (a / b) * 100;

			return percent;
		}

		/// <summary>
		/// Returns what pecentage 'a' is of 'b'
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static double GetPercentage(double a, double b)
		{
			if (a == 0 || b == 0)
				return 0;

			double percent = (a / b) * 100;

			return percent;
		}

		/// <summary>
		/// Returns what pecentage 'a' is of 'b'
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static decimal GetPercentage(int a, int b)
		{
			return GetPercentage(Convert.ToDecimal(a), Convert.ToDecimal(b));
		}

		/// <summary>
		/// Returns what pecentage 'a' is of 'b'
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static decimal GetPercentage(int? a, int? b)
		{
			if (a == null || b == null)
				return 0;
			
			return GetPercentage(Convert.ToDecimal(a), Convert.ToDecimal(b));
		}

		/// <summary>
		/// Returns the percentage of a number.
		/// Example: 10% of 200 is 20
		/// </summary>
		/// <param name="percent"></param>
		/// <param name="of"></param>
		/// <returns></returns>
		public static decimal GetPercentageOf(decimal percent, decimal of)
		{
			return (percent * .01m) * of;
		}

        /// <summary>
        /// 200 + 10% = 220
        /// </summary>
        /// <param name="percent"></param>
        /// <param name="addTo"></param>
        /// <returns></returns>
        public static decimal AddPercent(decimal percent, decimal addTo)
        {
            return addTo + GetPercentageOf(percent, addTo);
        }

        /// <summary>
        /// 200 - 10% = 180
        /// </summary>
        /// <param name="percent"></param>
        /// <param name="addTo"></param>
        /// <returns></returns>
        public static decimal SubtractPercent(decimal percent, decimal addTo)
        {
            return addTo - GetPercentageOf(percent, addTo);
        }
    }
}
