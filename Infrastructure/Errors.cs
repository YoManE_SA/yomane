﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure
{
	public enum ValidationResult
	{
		Success = 0,
        //bellow 800 is save for legacy errors
        //************************General 8XX-999 **********************//

        //WebAccessError = 801,
        //ServiceAccessError = 802,
        //ProcessError = 803,

        //************************System 1XXX **********************//
        //password & pin code 11XX
        Invalid_UserOrPassword = 1001,
		AdminUserLocked = 1002,
        AbsenceLocked = 1003,
        FailedAttemptsLock = 1004,

        PasswordWeak = 1151,
		PasswordWrong = 1152,
		PasswordUsedInThePast = 1153,
		PasswordConfirmationMissmatch = 1154,

		PinCodeWeak = 1161,
		PinCodeWrong = 1162,
		PinCodeConfirmationMissmatch = 1163,

		//user managment 12XX
		Invalid_Email = 1201,
		EmailAlreadyExist = 1202,
		Invalid_UserName = 1203,
		UserNameAlreadyExist = 1204,
		Invalid_Name = 1205,
		Invalid_FirstName = 1206,
		Invalid_LastName = 1207,
		Invalid_SSN = 1208,
		Invalid_Phone = 1209,

		//general 15XX
		Invalid_Data = 1501,
		Invalid_ID = 1502,
		Invalid_Date = 1503,
		Invalid_AppIdentity = 1504,
        Item_Not_Exist = 1505,
        Item_Wrong_State = 1506,
        Invalid_AccountID = 1507,

        //************************external login **********************//
        Invalid_ServiceUrl = 1601,

		//************************components 2XXX **********************//

		//address 21XX
		Invalid_Country = 2100,
		Invalid_State = 2101,
		Invalid_Address = 2102,
		Invalid_PostalCode = 2103,
		Invalid_City = 2104,

		//payment 22XX
		Invalid_Currency = 2201,
		Invalid_PaymentMethod = 2202,
		Invalid_Amount = 2203,
		Invalid_Merchant = 2204,
		Invalid_Customer = 2206,

		//payment method 23XX
		Invalid_AccountValue1 = 2301, //account
		Invalid_AccountValue2 = 2302, //cvv, routing
		Invalid_ExpDate = 2303,
		Invalid_OwnerName = 2304,
        CardNoAlreadyExist = 2305,

        //balance 24XX
        Invalid_RequestID = 2401,
        Invalid_RequestForAccountID = 2402,
        AmountAboveBalace = 2403,
        AmountBelowMinimum = 2404,
        AmountAboveMaximum = 2405,

		//************************Other 9999 **********************//
        Other = 9999,
	}

	public class ValidationException : Exception
	{
		public ValidationResult ResultCode { get; private set; }
		public ValidationException(ValidationResult result) : base(string.Format("data validation error: '{0}'", result.ToString()))
		{
			ResultCode = result;
		}

		public static void RaiseIfNeeded(ValidationResult result)
		{
			if (result != ValidationResult.Success)
				throw new ValidationException(result);
		}
	}



}
