﻿using System;
using System.Reflection;
using System.Collections.Generic;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.Tasks;

namespace Netpay.Infrastructure
{
    public static class Application
    {
        private static Dictionary<string, string> _parameters = null;
        public static TaskScheduler Scheduler = null;
        public static TaskQueue Queue { get; set; }
        public static bool IsInit { get; set; }
        public static string InstanceName { get; private set; }
        public static bool IsProduction { get; private set; }
        public static bool EnableEmail { get; private set; }
        public static string PhysicalPath { get; private set; }
        public static string ApplicationPath { get; set; }
        public static int SessionTimeout { get; private set; }

        public static string ConfigFileName
        {
            get
            {
                string dllPath = System.IO.Path.GetDirectoryName(Assembly.GetAssembly(typeof(Application)).CodeBase).Remove(0, 6);
                if (dllPath[1] != ':') dllPath = "\\\\" + dllPath;
                return dllPath + @"\Netpay.Infrastructure.config.xml";
            }
        }

        private static string _version = null;

        public static string Version
        {
            get
            {
                if (_version == null)
                {
                    var version = Assembly.GetAssembly(typeof(Netpay.Infrastructure.Application)).GetName().Version;
                    _version = $"v{version.Major}.{version.Minor}";
                }

                return _version;
            }
        }

        public static string GetParameter(string key)
        {
            if (!_parameters.ContainsKey(key))
                throw new ApplicationException(string.Format("The configuration parameter ({0}) was not found.", key));
            return _parameters[key];
        }

        public static string GetParameter(string key, string defValue)
        {
            if (_parameters == null || !_parameters.ContainsKey(key)) return defValue;
            return _parameters[key];
        }

        public static string MapAppPath(string path)
        {
            var ret = System.IO.Path.Combine(PhysicalPath, path);
            return System.IO.Path.GetFullPath(ret);
        }

        private static void InitConfig()
        {
            // load config file
            if (!System.IO.File.Exists(ConfigFileName)) throw new ApplicationException(string.Format("Could not find configuration file 'Netpay.Infrastructure.config.xml' at: {0}", ConfigFileName));
            var configFile = new System.Xml.XmlDocument();
            configFile.Load(ConfigFileName);

            // get application parameters
            _parameters = new Dictionary<string, string>();
            var elemList = (configFile.DocumentElement.SelectSingleNode("application") as System.Xml.XmlElement).GetElementsByTagName("parameter");
            foreach (System.Xml.XmlElement v in elemList)
                _parameters.Add(v.GetAttribute("key"), v.GetAttribute("value"));
            IsProduction = GetParameter("isProduction").ToNullableBool().GetValueOrDefault(true);
            EnableEmail = GetParameter("enableEmail").ToNullableBool().GetValueOrDefault(true);
            SessionTimeout = GetParameter("SessionTimeout", "20").ToNullableInt().GetValueOrDefault(20);

            //load error handlers
            var groupNode = (configFile.DocumentElement.SelectSingleNode("logHandle") as System.Xml.XmlElement);
            if (groupNode != null) Logger.Init(groupNode);

            //load domains
            groupNode = configFile.DocumentElement.SelectSingleNode("domains") as System.Xml.XmlElement;
            if (groupNode != null) Domain.Init(groupNode);

            //load taks
            groupNode = (configFile.DocumentElement.SelectSingleNode("scheduledTasks") as System.Xml.XmlElement);
            if (groupNode != null) Scheduler.Init(groupNode);

        }

        /// <summary>
        /// <para>Inits the application.</para>
        /// <para>Sets up various parts of the application such as <see cref="Netpay.Infrastructure.Logger"/>.</para>
        /// <para>The application will not run properly without initialization.</para>
        /// </summary>
        public static void Init(string instanceName)
        {
            if (IsInit) return;
/*
            {
                var processInfo = new System.Diagnostics.ProcessStartInfo("cmd") {
                    Arguments = string.Format("/c net use {0} /user:{1} {2}", @"\\nas\IISNas", "IISNas", "123"),
                    CreateNoWindow = false,
                    UseShellExecute = true
                };
                using (var process = System.Diagnostics.Process.Start(processInfo))
                {
                    process.WaitForExit(5000);
                    process.Close();
                }
            }
*/

            InstanceName = instanceName;
            PhysicalPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetCallingAssembly().CodeBase).TrimEnd(new char[] { '/', '\\' }).ToLower();
            if (PhysicalPath.EndsWith(@"debug")) PhysicalPath = PhysicalPath.Substring(0, PhysicalPath.Length - @"debug".Length - 1);
            if (PhysicalPath.EndsWith(@"bin")) PhysicalPath = PhysicalPath.Substring(0, PhysicalPath.Length - @"bin".Length - 1);
            if (PhysicalPath.StartsWith("file:")) PhysicalPath = PhysicalPath.Substring("file:".Length).TrimStart(new char[] { '/', '\\' });

            try
            {
                // subscribe UnhandledException
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
                if (Scheduler == null) Scheduler = new TaskScheduler();
                InitConfig();
                Logger.Log(LogSeverity.Info, LogTag.AppInit, "Application, InitConfig completed.");

                if (bool.Parse(GetParameter("enableQueuedTasks")))
                {
                    Queue = new TaskQueue();
                    Queue.Start();
                    Logger.Log(LogSeverity.Info, LogTag.AppInit, "TaskQueue initialized.");
                }
                else Logger.Log(LogSeverity.Warning, LogTag.AppInit, "TaskQueue not initialized.");

                if (bool.Parse(GetParameter("enableTaskScheduler")))
                {
                    Scheduler.Start();
                    Logger.Log(LogSeverity.Info, LogTag.AppInit, "TaskScheduler initialized.");
                }
                else Logger.Log(LogSeverity.Warning, LogTag.AppInit, "TaskScheduler not initialized.");

                if (!EnableEmail) Logger.Log(LogSeverity.Warning, LogTag.AppInit, "Email sends are disabled.");

                Logger.Log(LogSeverity.Info, LogTag.AppInit, "Application initialized.");
                IsInit = true;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw ex;
                //throw new ApplicationException("Application failed to initialize, see log file for more details.");
            }
        }

        public static string MailAddressFrom { get { return GetParameter("mailAddressFrom").EmptyIfNull(); } }
        public static int? MaxImmediateReportSize { get { return GetParameter("maxImmediateReportSize").ToNullableInt(); } }
        public static int MaxAdminFileManagerUploadSizeBytes
        {
            get
            {
                var defaultValueBytes = 4000000; 
                var valueStr = GetParameter("maxAdminFileManagerUploadSizeBytes", defaultValueBytes.ToString());
                int valueInt;
                if (Int32.TryParse(valueStr, out valueInt))
                    return valueInt;
                return defaultValueBytes;
            }
        }
        public static int? MaxFiledReportSize { get { return GetParameter("maxFiledReportSize").ToNullableInt(); } }
        public static string NumericFormat { get { return GetParameter("numericFormat"); } }
        public static string AmountFormat { get { return GetParameter("amountFormat"); } }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            Logger.Log(ex);
        }



    }
}
