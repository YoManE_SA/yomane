﻿using Netpay.Dal.Netpay;
using System;

namespace Netpay.Infrastructure.Tasks
{
	/// <summary>
	/// Task value object
	/// </summary>
	public class TaskInfo
	{
		public TaskInfo() { }

		public TaskInfo(Task entity)
		{
			_taskID = entity.TaskID;
			_ownerID = entity.GroupID;
			_description = entity.Description;
			_insertDate = entity.CreationDate;
		}

		private Guid _taskID = Guid.Empty;
		private string _ownerID = null;
		private string _description = null;
		private DateTime _insertDate = DateTime.MinValue;

		public DateTime InsertDate
		{
			get { return _insertDate; }
			set { _insertDate = value; }
		}

		public Guid TaskID
		{
			get { return _taskID; }
			set { _taskID = value; }
		}

		public string OwnerID
		{
			get { return _ownerID; }
			set { _ownerID = value; }
		}

		public string Description
		{
			get { return _description; }
			set { _description = value; }
		}
	}
}

