﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Netpay.Dal.Netpay;
using System.Reflection;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;

namespace Netpay.Infrastructure.Tasks
{
	/// <summary>
	/// Represents a method that can be queued or scheduled.
	/// The method referenced must be a public static method.
	/// Optionally contains a user credentials for methods that require authentication.
	/// </summary>
	public class Task
	{
		private Guid _taskID = Guid.NewGuid();
		private string _groupID = null;
		private string _description = null;
		private DateTime _creationDate = DateTime.MinValue;
		private string _assemblyName = null;
		private string _typeName = null;
		private string _methodName = null;
		private List<object> _args = null;
		private bool _canceled = false;
		private CredentialsInfo _credentials = null;
		private LogTag _tag = LogTag.TaskInvocation;

		public Task()
		{
		}

		public Task(string groupID, string description, LogTag tag, string assemblyName, string typeName, string methodName, List<object> args)
		{
			_credentials = null;
			_groupID = groupID;
			_description = description;
			_tag = tag;
			_assemblyName = assemblyName;
			_typeName = typeName;
			_methodName = methodName;
			_args = args;
			_creationDate = DateTime.Now;
		}

		public Task(CredentialsInfo credentials, string groupID, string description, LogTag tag, string assemblyName, string typeName, string methodName, List<object> args)
		{
			_credentials = credentials;
			_groupID = groupID;
			_description = description;
			_tag = tag;
			_assemblyName = assemblyName;
			_typeName = typeName;
			_methodName = methodName;
			_args = args;
			_creationDate = DateTime.Now;
		}

		public Task(Security.Login login, string groupID, string description, LogTag tag, string assemblyName, string typeName, string methodName, List<object> args)
		{
                _credentials = new CredentialsInfo(login);
                _groupID = groupID;
                _description = description;
                _tag = tag;
                _assemblyName = assemblyName;
                _typeName = typeName;
                _methodName = methodName;
                _args = args;
                _creationDate = DateTime.Now; 

         
        }

        public Task(System.Xml.XmlElement xmlElement)
        {
            GroupID = xmlElement.GetAttribute("groupID");
            Description = xmlElement.GetAttribute("description");
            if (!string.IsNullOrEmpty(xmlElement.GetAttribute("tag"))) 
                Tag = (LogTag)Enum.Parse(typeof(LogTag), xmlElement.GetAttribute("tag"));
            if (!string.IsNullOrEmpty(xmlElement.GetAttribute("assemblyName"))) 
                AssemblyName = xmlElement.GetAttribute("assemblyName");
            TypeName = xmlElement.GetAttribute("typeName");
            MethodName = xmlElement.GetAttribute("methodName");
            string args = xmlElement.GetAttribute("args");
            if (args.Trim() != "") Args = args.Split(',').ToList<object>();
            var ciElement = xmlElement.SelectSingleNode("credentialsInfo") as System.Xml.XmlElement;
            if (ciElement != null) Credentials = new CredentialsInfo(ciElement);
        }

		public static string GetUserGroupID(Guid credentialToken)
		{
			Login user = Login.Current;
			return GetUserGroupID(user);
		}

		public static string GetUserGroupID(Login user)
		{
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            //Logger.Log(LogSeverity.Info, LogTag.Reports, "Inside GetUserGroupID() method.");

            return user.Role.ToString() + "_" + user.LoginID.ToString();
		}

		public override string ToString()
		{
			string argsConcat = "";
			if(Args != null && Args.Count > 0)
				argsConcat = Args.Aggregate((currentObject, nextObject) => string.Format("({0}){1}, ({2}){3}", currentObject.GetType().Name, currentObject.ToString(), nextObject.GetType().Name, nextObject.ToString())).ToString();
			return string.Format("Task ID '{0}' in group '{1}': {2}.{3}({4})", _taskID.ToString(), _groupID, _typeName, _methodName, argsConcat);
		}

        /// <summary>
        /// The tag used when the task needs to write a log.
        /// Default value is TaskInvocation.
        /// </summary>
        public LogTag Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

		/// <summary>
		/// Is used to invoke the task under a user credentials
		/// If Credentials are provided (not null), the task will attempt login prior to invocation,
		/// If the login is successful, the first / only parameter passed to the method will be the credentials token.
		/// So any method referenced by a task with authenticated user, should accept credentials token as the first / only parameter.
		/// </summary>
		public CredentialsInfo Credentials
		{
			get { return _credentials; }
			set { _credentials = value; }
		}

		/// <summary>
		/// Indicates that the task has been canceled.
		/// Canceled tasks will not invoke.
		/// </summary>
		public bool Canceled
		{
			get { return _canceled; }
			set { _canceled = value; }
		}

		public DateTime CreationDate
		{
			get { return _creationDate; }
			set { _creationDate = value; }
		}

		/// <summary>
		/// The task id for later reference.
		/// </summary>
		public Guid TaskID
		{
			get { return _taskID; }
			set { _taskID = value; }
		}
		
		/// <summary>
		/// The task gruop id.
		/// This can be any string that will identify several tasks as a group.
		/// </summary>
		public string GroupID
		{
			get { return _groupID; }
			set { _groupID = value; }
		}

		/// <summary>
		/// Description line for the task.
		/// </summary>
		public string Description
		{
			get { return _description; }
			set { _description = value; }
		}

		public string AssemblyName
		{
			get { return _assemblyName; }
			set { _assemblyName = value; }
		}

		/// <summary>
		/// The type of the class which contains the method to invoke.
		/// </summary>
		public string TypeName
		{
			get { return _typeName; }
			set { _typeName = value; }
		}

		/// <summary>
		/// The method name to invoke.
		/// Must be a public static method.
		/// </summary>
		public string MethodName
		{
			get { return _methodName; }
			set { _methodName = value; }
		}

		/// <summary>
		/// Arguments to pass to the invoked method.
		/// </summary>
		public List<object> Args
		{
			get { return _args; }
			set { _args = value; }
		}
		
		/// <summary>
		/// Invokes the task asynchronously if it is not canceled.
		/// </summary>
		private delegate void InvocationDelegate();
		public void BeginInvoke()
		{
			InvocationDelegate invocationDelegate = Invoke;
			invocationDelegate.BeginInvoke(null, null);
		}
		
		/// <summary>
		/// Invokes the task if it is not canceled
		/// </summary>
		public void Invoke()
		{
			if (Canceled)
				return;
			
			try
			{
				// log start
				//Logger.Log(LogSeverity.Info, LogTag.TaskInvocation, "Task begin: " + ToString());
				// authenticate if needed
				if (Credentials != null)
				{
					Guid credentialsToken;
					Domain.Current = Domain.Get(Credentials.UserDomain);
					var result = Security.Login.DoLogin(new Security.UserRole[] { Credentials.Type }, Credentials.UserName, Credentials.Email, Credentials.Password, null, out credentialsToken);
					if (result != Security.LoginResult.Success)
					{
						Logger.Log(LogSeverity.Error, Tag, string.Format("Task failed to execute because the login failed ({0}) for {1}", result, Credentials.ToString()), ToString());
						return;	
					}
					ObjectContext.Current.CredentialsToken = credentialsToken;
					//if (Args != null && Args.Count > 0) Args.Insert(0, credentialsToken);	
					//else Args = new List<object> { credentialsToken };
				}

				// get type
				string typeFullName = _typeName;
				if (_assemblyName != null)
					typeFullName += "," + _assemblyName;
				Type taskType = Type.GetType(typeFullName);
				if (taskType == null)
				{
					Logger.Log(LogSeverity.Error, Tag, string.Format("Type '{0}' not found.", typeFullName), ToString());
					return;
				}

				// get method
				MethodInfo taskMethod = null;
				if (Args != null && Args.Count > 0) 
				{
					Type[] argsTypes = Args.Select(a => a.GetType()).ToArray();
					taskMethod = taskType.GetMethod(_methodName, argsTypes);				
				}
				else
					taskMethod = taskType.GetMethod(_methodName);

				if (taskMethod == null)
				{
					Logger.Log(LogSeverity.Error, Tag, string.Format("Method '{0}' in type '{1}' not found.", _methodName, typeFullName), ToString());	
					return;			
				}
				
				// invoke
				if (Args == null)
					Args = new List<object>();
				DateTime start = DateTime.Now;
				if (taskMethod.IsStatic)
					taskMethod.Invoke(null, Args.ToArray());
				else
				{
					object instance = Activator.CreateInstance(taskType);
					taskMethod.Invoke(instance, Args.ToArray());
				}
				
				// log success
				TimeSpan duration = (DateTime.Now - start);
                Logger.Log(LogSeverity.Info, Tag, ToString(), "Duration: " + duration.ToString());
			}
			catch (Exception ex)
			{
				// log failure
				StringBuilder sb = new StringBuilder();
				sb.AppendLine(ex.ToString());
				sb.AppendLine(this.ToString());
				if (Domain.Current != null) sb.AppendLine("Domain: " + Domain.Current.Host);
                Logger.Log(LogSeverity.Error, Tag, ex.Message, sb.ToString());
			}
			finally
			{
			}
		}
	}
}
