﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Reflection;
using Netpay.Dal.Netpay;
using System.Xml.Serialization;
using System.IO;

namespace Netpay.Infrastructure.Tasks
{
	/// <summary>
	/// Provides a mechanism for queueing tasks.
	/// </summary>
	public class TaskQueue
	{
		private Queue<Task> _queue = new Queue<Task>();
		private Task _activeTask = null;
		private Timer _timer = new Timer();
		private bool _isBusy = false;

		public TaskQueue()
		{
			// setup and start the timer
			_timer.Elapsed += new ElapsedEventHandler(TimerElapsed);
			_timer.Interval = 5000;
			_timer.AutoReset = true;
		}

		public void Start() 
		{
			_timer.Start();
		}

		public void Stop() 
		{
			_timer.Stop();
		}

		public bool IsTaskQueued(Task task)
		{
			return (from t in GetTasks(task.GroupID) where t.TypeName == task.TypeName && t.MethodName == task.MethodName && t.Args == task.Args select t).SingleOrDefault() != null; 
		}

		/// <summary>
		/// Adds a task to the queue
		/// </summary>
		/// <param name="task"></param>
		public void Enqueue(Task task)
		{
			_queue.Enqueue(task);
		}

		/// <summary>
		/// Gets queued tasks for a specific group
		/// </summary>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public List<Task> GetTasks(string groupID)
		{
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            //Logger.Log(LogSeverity.Info, LogTag.Reports, "Inside GetTasks() method.");

            List<Task> tasks = (from t in _queue where t.GroupID == groupID && !t.Canceled select t).ToList<Task>();
			return tasks;
		}

		/// <summary>
		/// Gets a task by its group and unique task id
		/// </summary>
		/// <param name="geoupID"></param>
		/// <param name="taskID"></param>
		/// <returns></returns>
		public Task GetTask(string geoupID, Guid taskID)
		{
			Task task = (from t in _queue where t.GroupID == geoupID && t.TaskID == taskID && !t.Canceled select t).SingleOrDefault();
			return task;
		}

		public Task ActiveTask
		{
			get { return _activeTask; }
		}

		/// <summary>
		/// Cancels a queued task
		/// </summary>
		/// <param name="geoupID"></param>
		/// <param name="taskUniqueID"></param>
		public void CancelTask(string geoupID, Guid taskUniqueID)
		{
			Task task = GetTask(geoupID, taskUniqueID);
			if (task != null)
				task.Canceled = true;
		}

		/// <summary>
		/// On timer elapse, if there are tasks in the queue and there is no task running, dequeues the next task and invokes it.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void TimerElapsed(object sender, ElapsedEventArgs e)
		{
			// return if a task is still running
			if (_isBusy)
				return;
			
			// return if there are no tasks
			if (_queue.Count == 0)
				return;
			
			// invoke the task
			_isBusy = true;
			_activeTask = _queue.Peek();
			_activeTask.Invoke();
			_activeTask = null;
			_queue.Dequeue();
			_isBusy = false;
		}
	}
}
