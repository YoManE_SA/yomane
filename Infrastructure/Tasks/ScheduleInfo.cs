﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Netpay.Infrastructure;

namespace Netpay.Infrastructure.Tasks
{
	/// <summary>
	/// Scheduling info for task
    /// interval = Minute, every = 30: will run every half hour
    /// interval = Hour, every = 2: will run every 2 hours
    /// interval = Day, every = 1: will run every day at 1:01
    /// interval = Day, every = 1, Hour = 15, Minute = 22: will run every day at 15:22
    /// interval = Week, every = 1, DayOfWeek = Monday, Hour = 15, Minute = 22: will run every monday at 15:22
    /// interval = Week, every = 2, DayOfWeek = Monday, Hour = 15, Minute = 22: will run every other monday at 15:22
    /// interval = Month, every = 1, DayOfMonth = 15, Hour = 15, Minute = 22: will run every month on the 15th at 15:22
	/// </summary>
	public class ScheduleInfo
	{
        private int _dayOfMonth = 1;
        private int _hour = 1;
        private int _minute = 1;
        private int _every = 1;

        public ScheduleInterval Interval { get; set; }
        public DayOfWeek DayOfWeek { get; set; }

        public ScheduleInfo()
        {
            Interval = ScheduleInterval.Day;
            DayOfWeek = System.DayOfWeek.Sunday;
        }

        public ScheduleInfo(System.Xml.XmlElement element)
        {
            if (!string.IsNullOrEmpty(element.GetAttribute("dayOfMonth"))) 
                DayOfMonth = int.Parse(element.GetAttribute("dayOfMonth"));
            if (!string.IsNullOrEmpty(element.GetAttribute("dayOfWeek"))) 
                DayOfWeek = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), element.GetAttribute("dayOfWeek"));
            if (!string.IsNullOrEmpty(element.GetAttribute("interval"))) 
                Interval = (ScheduleInterval)Enum.Parse(typeof(ScheduleInterval), element.GetAttribute("interval"));
            if (!string.IsNullOrEmpty(element.GetAttribute("hour"))) 
                Hour = int.Parse(element.GetAttribute("hour"));
            if (!string.IsNullOrEmpty(element.GetAttribute("minute"))) 
                Minute = int.Parse(element.GetAttribute("minute"));
            if (!string.IsNullOrEmpty(element.GetAttribute("every"))) 
                Every = int.Parse(element.GetAttribute("every"));
        }

		public int DayOfMonth 
		{
			get { return _dayOfMonth; }
			set 
			{
				if (value < 1 || value > 31) throw new ArgumentOutOfRangeException("DayOfMonth", "Must be 1 or higher");
				_dayOfMonth = value;
			} 
		}
		public int Hour
		{
			get { return _hour; }
			set
			{
                if (value < 0 || value > 24) throw new ArgumentOutOfRangeException("Hour", "Must be 1 or higher");
				_hour = value;
			}
		}

		public int Minute
		{
			get { return _minute; }
			set
			{
                if (value < 0 || value > 60) throw new ArgumentOutOfRangeException("Minute", "Must be 0 or higher");
				_minute = value;
			}
		}

		public int Every
		{
			get { return _every; }
			set
			{
				if (value < 1) throw new ArgumentOutOfRangeException("Every", "Must be 1 or higher");
				_every = value;
			}
		}
	}
}
