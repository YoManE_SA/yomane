﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.Tasks
{
    public class ScheduledTask : Task
	{
        public ScheduleInfo Schedule { get; set; }
        public ScheduledTask() { }

        public ScheduledTask(System.Xml.XmlElement xmlElement)
            : base(xmlElement)
        {

            var ciElement = xmlElement.SelectSingleNode("scheduleInfo") as System.Xml.XmlElement;
            if (ciElement != null) Schedule = new ScheduleInfo(ciElement);
        }

    }
}
