﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;

namespace Netpay.Infrastructure.Tasks
{
	public class CredentialsInfo
	{
		public CredentialsInfo() {}

		public CredentialsInfo(Login user)
		{ 

            UserDomain = Domain.Current.Host;
			Type = user.Role;
			Email = user.EmailAddress;
			UserName = user.UserName;
			//ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
				Password = Login.GetPassword(user.LoginID, null);
            //ObjectContext.Current.StopImpersonate(); 
        }

        public CredentialsInfo(System.Xml.XmlElement xmlElement) 
        {
            if (!string.IsNullOrEmpty(xmlElement.GetAttribute("userDomain"))) 
                UserDomain = xmlElement.GetAttribute("userDomain").NullIfEmpty();
            if (!string.IsNullOrEmpty(xmlElement.GetAttribute("userType"))) 
                Type = xmlElement.GetAttribute("userType").ToNullableEnumByName<UserRole>().GetValueOrDefault();
            if (!string.IsNullOrEmpty(xmlElement.GetAttribute("email"))) 
                Email = xmlElement.GetAttribute("email").NullIfEmpty();
            if (!string.IsNullOrEmpty(xmlElement.GetAttribute("userName"))) 
                UserName = xmlElement.GetAttribute("userName").NullIfEmpty();
            if (!string.IsNullOrEmpty(xmlElement.GetAttribute("password"))) 
                Password = Encryption.DecryptHex(Domain.Get(UserDomain).EncryptionKeyNumber, xmlElement.GetAttribute("password").NullIfEmpty());
        }

		public string UserDomain { get; set; }
		public UserRole Type { get; set; }
		public string Email { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }

		public override string ToString()
		{
			return string.Format("{0}, {1}, {2}", Type, Email, UserName);
		} 
	}
}
