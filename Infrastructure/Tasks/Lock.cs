﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure.Tasks
{
    public class Lock : BaseDataObject
    {
        private Dal.Netpay.TaskLock _entity;
        public string Name { get { return _entity.TaskName; } }
        public bool IsRunning { get { return _entity.IsTaksRunning; } }
        public DateTime LastRunDate { get { return _entity.LastRunDate; } }
        public string MachineName { get { return _entity.MachineName; } }

        private Lock(Dal.Netpay.TaskLock entity)
        {
            _entity = entity;
        }

        public static Lock Load(string lockName)
        {
            return (from n in DataContext.Reader.TaskLocks where n.TaskName == lockName select new Lock(n)).SingleOrDefault();
        }

        public static bool TryLock(string lockName, DateTime? minActiveDate)
        {
            string curMachineName = System.Environment.MachineName.TruncEnd(100);
            lockName = lockName.TruncEnd(30);
            var minActiveValue = minActiveDate.GetValueOrDefault(DateTime.Now.AddMinutes(-2));
            var recUpdated = DataContext.Reader.ExecuteCommand("Update System.TaskLock Set IsTaksRunning=1, LastRunDate=getDate(), MachineName={1} Where TaskName={0} And (IsTaksRunning=0 Or LastRunDate < {2})", lockName, curMachineName, minActiveValue);
            if (recUpdated > 0) return true;
            try {
                recUpdated = DataContext.Reader.ExecuteCommand("Insert Into System.TaskLock(TaskName, IsTaksRunning, LastRunDate, MachineName) Select {0}, 1, getDate(), {1} Where Not Exists (Select 0 From System.TaskLock WITH(UPDLOCK, HOLDLOCK) Where TaskName={0})", lockName, curMachineName, minActiveValue);
                if (recUpdated > 0) return true;
            }
            catch (Exception) { return false; /*someone else already created*/ }
            return false;
        }

        public static bool Release(string lockName)
        {
            string curMachineName = System.Environment.MachineName.TruncEnd(100);
            lockName = lockName.TruncEnd(30);
            var recUpdated = DataContext.Reader.ExecuteCommand("Update System.TaskLock Set IsTaksRunning=0 Where TaskName={0} And (IsTaksRunning=1 And MachineName={1})", lockName, curMachineName);
            if (recUpdated == 0) Logger.Log(new Exception("Task Release Called for unlocked/exist task '" + lockName + "'"));
            return recUpdated > 0;
        }
    }
}
