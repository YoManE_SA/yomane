﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Netpay.Infrastructure;

namespace Netpay.Infrastructure.Tasks
{
	/// <summary>
	/// Provides a mechanism for <see cref="Netpay.Infrastructure.Tasks.Task">task</see> scheduling.
	/// </summary>
	public class TaskScheduler
	{
        private Timer _timer = null;
		private Dictionary<ScheduleInfo, Task> _tasks = new Dictionary<ScheduleInfo,Task>();

        internal void Init(System.Xml.XmlElement xmlElement)
        {
            _tasks = new Dictionary<ScheduleInfo, Task>();
            foreach (System.Xml.XmlElement v in xmlElement.GetElementsByTagName("task"))
                Add(new ScheduledTask(v));
        }

        public void Add(ScheduleInfo info, Task task)
        {
            lock (_tasks)
                _tasks.Add(info, task);
        }

        public void Add(List<ScheduledTask> tasks)
        {
            foreach (ScheduledTask currentTask in tasks)
                Add(currentTask);
        }

        public void Add(ScheduledTask task)
        {
            Add(task.Schedule, task);
        }	

		public void Start()
		{
            lock (this)
            {
                if (_timer == null) {
                    _timer = new Timer(60000);
                    _timer.AutoReset = true;
                    _timer.Elapsed += new ElapsedEventHandler(TimerElapsed);
                }
            }
			_timer.Start();
		}

		public void Stop()
		{
            lock (this) { 
                if (_timer == null) return;
			    _timer.Stop();
            }
		}

		private void TimerElapsed(object sender, ElapsedEventArgs e)
		{
			int mod = 0;
			DateTime now = DateTime.Now;

			lock (_tasks)
			{
				foreach (KeyValuePair<ScheduleInfo, Task> currentPair in _tasks)
				{
					switch (currentPair.Key.Interval)
					{
						case ScheduleInterval.Minute:
							mod = now.Minute % currentPair.Key.Every;
							if (mod == 0)
								currentPair.Value.BeginInvoke();
							break;
						case ScheduleInterval.Hour:
							mod = now.Hour % currentPair.Key.Every;
							if (mod == 0 && currentPair.Key.Minute == now.Minute)
								currentPair.Value.BeginInvoke();		
							break;
						case ScheduleInterval.Day:
							mod = now.Day % currentPair.Key.Every;
							if (mod == 0 && currentPair.Key.Hour == now.Hour && currentPair.Key.Minute == now.Minute)
								currentPair.Value.BeginInvoke();
							break;
						case ScheduleInterval.Week:
							mod = now.WeekOfYear() % currentPair.Key.Every;
                            if (mod == 0 && currentPair.Key.DayOfWeek == now.DayOfWeek && currentPair.Key.Hour == now.Hour && currentPair.Key.Minute == now.Minute)
								currentPair.Value.BeginInvoke();
							break;
						case ScheduleInterval.Month:
							mod = now.Month % currentPair.Key.Every;
                            if (mod == 0 && currentPair.Key.DayOfMonth == now.Day && currentPair.Key.Hour == now.Hour && currentPair.Key.Minute == now.Minute)
								currentPair.Value.BeginInvoke();
							break;
						case ScheduleInterval.Year:
							Logger.Log(LogSeverity.Warning, LogTag.TaskScheduler, "Year Schedule Interval is not implemented.");
							break;
						default:
							Logger.Log(LogSeverity.Warning, LogTag.TaskScheduler, "Invalid Schedule Interval.");
							break;
					}
				} 
			}
		}
	}
}
