﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Netpay.Infrastructure
{
    public static class RegularExpressions
    {
        public static readonly Regex repetition = new Regex(@"(.)\1{2,}");
        public static readonly Regex notLetter = new Regex(@"[^a-zA-Z\s\-]");
        public static readonly Regex notNumeric = new Regex(@"[^0-9]");
        public static readonly Regex phoneNumber = new Regex(@"[0-9\+\-\(\)]");
        public static readonly Regex wordVariations = new Regex(@"\bst\b\.|\bst\b|\bmt\b\.|\bmt\b|\bmount\b|\bcity\b|\bnorth\b|\bsouth\b|\beast\b|\bwest\b", RegexOptions.IgnoreCase);
        public static readonly Regex guid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
        public static readonly Regex email = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\w");
    }
}
