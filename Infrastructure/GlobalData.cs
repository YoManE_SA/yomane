﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure
{
	public class GlobalData : BaseDataObject
	{
		private Dal.Netpay.tblGlobalData _entity;

		public int ID { get { return _entity.GD_ID; } }
		public GlobalDataGroup Group { get { return (GlobalDataGroup) _entity.GD_Group; } }
		public CommonTypes.Language LNG { get { return (CommonTypes.Language) _entity.GD_LNG; } }
		public string Value { get { return _entity.GD_Text; } }
		public string Description { get { return _entity.GD_Description; } }
		public string Color { get { return _entity.GD_Color; } }
		public string Country { get { return _entity.GD_Country; } }
		public short Flags { get { return _entity.GD_Flags; } }
		public string Currency { get { return _entity.GD_Currency; } }

		private GlobalData(Dal.Netpay.tblGlobalData entity)
		{ 
			_entity = entity;
		}

		public static List<GlobalData> Cache
		{
			get
			{
                return  Domain.Current.GetCachData("GlobalData", () => {
                    return new Domain.CachData((from c in DataContext.Reader.tblGlobalDatas select new GlobalData(c)).ToList(), DateTime.Now.AddHours(12));
                }) as List<GlobalData>;
			}
		}

		public static List<GlobalData> GetGroup(GlobalDataGroup groupId, CommonTypes.Language language)
		{
			return Cache.Where(c => c.Group == groupId && c.LNG == language).ToList();
		}

		public static GlobalData GetValue(GlobalDataGroup groupId, CommonTypes.Language language, int valueId)
		{
			return Cache.Where(c => c.Group == groupId && c.LNG == language && c.ID == valueId).SingleOrDefault();
		}

		public static string GetText(GlobalDataGroup groupId, CommonTypes.Language language, int valueId)
		{
			return Cache.Where(c => c.Group == groupId && c.LNG == language && c.ID == valueId).Select(c=> c.Value).SingleOrDefault();
		}

	}
}
