﻿using System.IO;
using System.Net;
using System.Text;

namespace Netpay.Infrastructure
{
	public static class HttpClient
	{
		public static HttpStatusCode SendHttpRequest(string url, out string returnText)
		{
			return SendHttpRequest(url, out returnText, null);
		}

		public static HttpStatusCode SendHttpRequest(string url, out string returnText, string postData, Encoding encoding = null, int timeout = 0, System.Collections.Generic.Dictionary<string, string> headers = null)
		{
			returnText = string.Empty;
			if (encoding == null) encoding = Encoding.UTF8;
			try
			{
				HttpWebRequest hwrRequest = (HttpWebRequest)WebRequest.Create(url);
				if(timeout != 0){
					hwrRequest.ReadWriteTimeout = timeout;
					hwrRequest.Timeout = timeout;
				}
				if (!string.IsNullOrEmpty(postData))
				{
					hwrRequest.Method = "POST";
					hwrRequest.Headers.Add("Content-Encoding", encoding.WebName);
					byte[] bytesPostData = encoding.GetBytes(postData);
                    if (headers == null || !headers.ContainsKey("Content-Type")) 
                        hwrRequest.ContentType = "application/x-www-form-urlencoded";
                    if (headers != null)
                    {
                        if (headers.ContainsKey("Content-Type"))
                        {
                            hwrRequest.ContentType = headers["Content-Type"];
                            headers.Remove("Content-Type");
                        }
                        foreach (var h in headers) hwrRequest.Headers.Add(h.Key, h.Value);
                    }
					hwrRequest.ContentLength = bytesPostData.Length;
					hwrRequest.GetRequestStream().Write(bytesPostData, 0, bytesPostData.Length);
					hwrRequest.GetRequestStream().Close();
				}
				HttpWebResponse hwrResponse = (HttpWebResponse)hwrRequest.GetResponse();
				StreamReader srResponse = new StreamReader(hwrResponse.GetResponseStream(), encoding);
				returnText = (hwrResponse.StatusCode == HttpStatusCode.OK ? srResponse.ReadToEnd() : hwrResponse.StatusDescription);
			}
			catch (WebException e)
			{
				returnText = "Unexpected reply (Exception)";
				if (e.Response == null) return HttpStatusCode.ExpectationFailed;
                returnText = (new StreamReader(e.Response.GetResponseStream(), encoding)).ReadToEnd();
				//returnText = ((HttpWebResponse)e.Response).StatusDescription;
				return ((HttpWebResponse)e.Response).StatusCode;
			}
			catch (System.Exception e)
			{
				returnText = e.Message;
				return HttpStatusCode.ServiceUnavailable;
			}
			return HttpStatusCode.OK;
		}

		public static string GetQueryStringValue(string queryString, string name)
		{
			if (!queryString.StartsWith("&")) queryString = "&" + queryString;
			if (!queryString.EndsWith("&")) queryString += "&";
			int start = queryString.IndexOf("&" + name + "=", System.StringComparison.OrdinalIgnoreCase);
			if (start < 0) return string.Empty;
			start += name.Length + 2;
			int end = queryString.IndexOf("&", start, System.StringComparison.OrdinalIgnoreCase);
			return queryString.Substring(start, end - start);
		}
	}
}