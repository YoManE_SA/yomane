﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure
{
	public static class ImageUtils
	{
		public static Bitmap FromBytes(byte[] bytes)
		{
            Bitmap bitmap;
            using (var stream = new MemoryStream(bytes))
            {
                bitmap = new Bitmap(stream);
            }

            return bitmap;
        }
		public static bool FromStream(Stream stream, out Bitmap bitmap)
		{
			try
			{
				bitmap = (Bitmap)Bitmap.FromStream(stream);
				return true;
			}
			catch
			{
				bitmap = null;
				return false;
			}
		}

		public static Bitmap FromStream(Stream stream)
		{
			Bitmap bitmap = null;
			try
			{
				bitmap = (Bitmap)Bitmap.FromStream(stream);
			}
			catch
			{
				bitmap = new Bitmap(100, 200);
				Graphics graphics = Graphics.FromImage(bitmap);
				Font font = new Font("Arial", 20, GraphicsUnit.Pixel);
				SolidBrush brush = new SolidBrush(Color.White);
				PointF point = new PointF(0.0F, 0.0F);
				graphics.DrawString("ERROR", font, brush, point);
			}

			return bitmap;
		}

		public static Bitmap ResizeByWidth(Bitmap bitmap, int newWidth)
		{
			int newHeight = (int)(((float)newWidth / bitmap.Width) * bitmap.Height);
			return Resize(bitmap, newWidth, newHeight);
		}

		public static Bitmap ResizeByHeight(Bitmap bitmap, int newHeight)
		{
			int newWidth = (int)(((float)newHeight / bitmap.Height) * bitmap.Width);
			return Resize(bitmap, newWidth, newHeight);
		}

        public static Bitmap Resize(Bitmap bitmap, int percent)
        {
            int newHeight = (int)Math.GetPercentageOf(percent, bitmap.Height);
            int newWidth = (int)Math.GetPercentageOf(percent, bitmap.Width);

            return Resize(bitmap, newWidth, newHeight);
        }

        public static Bitmap Resize(Bitmap bitmap, int newWidth, int newHeight)
		{
			Bitmap resized = new Bitmap(newWidth, newHeight);
			Graphics graphics = Graphics.FromImage(resized);
			//graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
			graphics.DrawImage(bitmap, 0, 0, newWidth, newHeight);

			return resized;
		}

		public static Bitmap CropByWidth(Bitmap bitmap, int newWidth)
		{
			int newHeight = (int)(((float)newWidth / bitmap.Width) * bitmap.Height);
			return Crop(bitmap, newWidth, newHeight);
		}

		public static Bitmap CropByHeight(Bitmap bitmap, int newHeight)
		{
			int newWidth = (int)(((float)newHeight / bitmap.Height) * bitmap.Width);
			return Crop(bitmap, newWidth, newHeight);
		}

		public static Bitmap Crop(Bitmap bitmap, int newWidth, int newHeight)
		{
			Bitmap resized = new Bitmap(newWidth, newHeight);
			Graphics graphics = Graphics.FromImage(resized);
			//graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
			graphics.DrawImage(bitmap, 0, 0);

			return resized;
		}

		public static byte[] Compress(Bitmap bitmap, ImageFormat format, long quality)
		{
			if (format == ImageFormat.Jpeg) 
			{ 
				//  white background
				Bitmap temp = new Bitmap(bitmap.Width, bitmap.Height);
				Graphics graphics = Graphics.FromImage(temp);
				graphics.Clear(Color.White);
				graphics.DrawImage(bitmap, 0, 0);
				bitmap = temp;
			}
			
			ImageCodecInfo encoder = GetEncoder(format);
			EncoderParameters encoderParameters = new EncoderParameters(1);
			EncoderParameter encoderParameter = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
			encoderParameters.Param[0] = encoderParameter;

			byte[] bytes = null;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				bitmap.Save(memoryStream, encoder, encoderParameters);
				bytes = memoryStream.ToArray();
				memoryStream.Close();
			}

			return bytes;
		}

		private static ImageCodecInfo GetEncoder(ImageFormat format)
		{
			ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
			foreach (ImageCodecInfo codec in codecs)
			{
				if (codec.FormatID == format.Guid)
				{
					return codec;
				}
			}
			return null;
		}
	}
}
