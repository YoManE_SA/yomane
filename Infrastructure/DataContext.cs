﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Infrastructure
{
	public class DataContext : Dal.DataAccess.NetpayDataContext
	{
		public static DateTime SQLMinDateValue = new DateTime(1900, 1, 1);
		public DataContext(string connectionString, bool withTracking = false) : base(connectionString) { ObjectTrackingEnabled = withTracking; if (System.Diagnostics.Debugger.IsAttached) SetupLogger(true); }
		public DataContext(bool withTracking = false) : this(Domain.Current.Sql1ConnectionString, withTracking) { }
        /*
		private static System.Threading.ThreadLocal<DataContext> _threadLocalSlot = new System.Threading.ThreadLocal<DataContext>(() => new DataContext(false));
        public static DataContext Reader
		{
			get { 
				if (_threadLocalSlot.Value == null) _threadLocalSlot.Value = new DataContext(false);
				return _threadLocalSlot.Value; 
			}
			set { if(_threadLocalSlot.Value != null) _threadLocalSlot.Value.Dispose(); _threadLocalSlot.Value = value; }
		}
        */
        private DateTime _lastGet;
        private static Dictionary<System.Threading.Thread, DataContext> _connections = new Dictionary<System.Threading.Thread, DataContext>();
        public static DataContext Reader
        {
            get
            {
                lock (_connections)
                {
                    DataContext ret;
                    if (!_connections.TryGetValue(System.Threading.Thread.CurrentThread, out ret))
                    {
                        ret = new DataContext() { ObjectTrackingEnabled = false, _lastGet = DateTime.Now };
                        _connections.Add(System.Threading.Thread.CurrentThread, ret);
                    }
                    ret._lastGet = DateTime.Now;    
                    return ret;
                }
                /*
                if ((!_reader.IsValueCreated) || (_reader.Value == null)) { 
                    //System.Diagnostics.Debug.WriteLine("THREADS CONNECTED: " + _reader.Values.Count);
                    _reader.Value = new DataContext() { ObjectTrackingEnabled = false };
                }
                return _reader.Value;
                */
            }
            set {
                lock (_connections)
                {
                    DataContext ret;
                    if (!_connections.TryGetValue(System.Threading.Thread.CurrentThread, out ret)) return;
                    ret.Dispose();
                    _connections.Remove(System.Threading.Thread.CurrentThread);
                    if (value != null) _connections.Add(System.Threading.Thread.CurrentThread, value);
                }
            }
        }

        public static void ClearUnused() { Clear(5); }

        public static void Clear(int keepMinutes = -1)
        {
            lock (_connections)
            {
                var lDate = DateTime.Now.AddMinutes(-keepMinutes);
                var keys = _connections.Where(v => v.Value._lastGet < lDate).ToList();
                foreach (var v in keys)
                {
                    //if (v.Value.Second.Connection.State != System.Data.ConnectionState.Closed) continue;
                    v.Value.Dispose();
                    _connections.Remove(v.Key);
                }
            }
        }

		public static DataContext Writer
		{
			get{
				var ret = ObjectContext.Current.Items.GetValue<DataContext>("Netpay.Infrastructure.DataContext.Writer");
				if (ret == null) ObjectContext.Current.Items.Add("Netpay.Infrastructure.DataContext.Writer", ret = new DataContext(true));
				return ret;
			}
			set {
				var ret = ObjectContext.Current.Items.GetValue<DataContext>("Netpay.Infrastructure.DataContext.Writer");
				if (ret != null) { 
					ret.Dispose();
					ObjectContext.Current.Items.Remove("Netpay.Infrastructure.DataContext.Writer");
				}
			}
		}

	}
}
