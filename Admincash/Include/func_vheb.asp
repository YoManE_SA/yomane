<%
Function MultipleLines2Heb(sText, nSize)
dim i, k, sNew, sRows(2000)
	if isNull(sText) then sText="   "
	if sText="" then sText="   "
	if len(sText & "@")>2 then
		k=1
		n=1
		l=0
		for i=1 to len(sText)
			if mid(sText,i,1)=chr(13) or i-k=nSize then
				n=i
				do until mid(sText,n,1)=chr(13) or mid(sText,n,1)=" " or n=k
					n=n-1
				loop
				if n=k then n=i
				l=l+1
				if n=k then
					sRows(l)=" "
				else
					sRows(l)=mid(sText,k,n-k)
				end if
				k=n
			end if
		next
		l=l+1
		sRows(l)=mid(sText,k,i-k)
		for i=l to 1 step -1
			sNew=tovheb(sRows(i)) & "<br />" & sNew
		next
		sNew=replace(sNew,")","$4#3@2!1")
		sNew=replace(sNew,"(",")")
		sNew=replace(sNew,"$4#3@2!1","(")
		sNew=replace(sNew,"]","$4#3@2!1")
		sNew=replace(sNew,"[","]")
		sNew=replace(sNew,"$4#3@2!1","[")
		sNew=replace(sNew,"}","$4#3@2!1")
		sNew=replace(sNew,"{","}")
		sNew=replace(sNew,"$4#3@2!1","{")
		MultipleLines2Heb=sNew
	else
		MultipleLines2Heb=sText
	end if
end Function

Function InvertPars(sText)
	InvertPars=replace(replace(replace(sText,"(","joklmn"),")","("),"joklmn",")")
end Function

function IsNotHebrew(sWord)
	dim sLetter, nCode
	IsNotHebrew=true
	if trim(sWord)<>"" then
		sLetter=left(trim(sWord),1)
		for nCode=asc("�") to asc("�")
			if chr(nCode)=sLetter then IsNotHebrew=false
		next
	end if
end function

function ReplaceInvalidPars(sText)
	dim i
	for i=2 to len(sText)-1
		if mid(sText,i,1)=")" and mid(sText,i+1,1)<>" " then sText=left(sText,i-1) & "(" & right(sText,len(sText)-i)
		if mid(sText,i,1)="(" and mid(sText,i-1,1)<>" " then sText=left(sText,i-1) & ")" & right(sText,len(sText)-i)
	next
	ReplaceInvalidPars=sText
end function

Function ToVHeb (HWord)
Dim i
Dim ReturnString
ReturnString=""
	' I should check for words that are numbers
	If isNull(HWord) then 
		ToVHeb=""
	Exit Function
	End If 
	' split HWord to words,
	' foreach word in HWord check whether
	' a number. If a number, reverse it.
	
	WordsArray=split(HWord, " ")
	nWordCount=UBound(WordsArray)
	for i=0 to nWordCount-1
		if IsNotHebrew(WordsArray(i)) and IsNotHebrew(WordsArray(i+1)) then
			WordsArray(i+1)=WordsArray(i) & " " & WordsArray(i+1)
			WordsArray(i+1)=replace(WordsArray(i+1),")","$4#3@2!1")
			WordsArray(i+1)=replace(WordsArray(i+1),"(",")")
			WordsArray(i+1)=replace(WordsArray(i+1),"$4#3@2!1","(")
			WordsArray(i+1)=replace(WordsArray(i+1),"]","$4#3@2!1")
			WordsArray(i+1)=replace(WordsArray(i+1),"[","]")
			WordsArray(i+1)=replace(WordsArray(i+1),"$4#3@2!1","[")
			WordsArray(i+1)=replace(WordsArray(i+1),"}","$4#3@2!1")
			WordsArray(i+1)=replace(WordsArray(i+1),"{","}")
			WordsArray(i+1)=replace(WordsArray(i+1),"$4#3@2!1","{")
			WordsArray(i)=""
		end if
	next
	for i=0 to nWordCount
		if left(trim(WordsArray(i)),7)="http://" or left(trim(WordsArray(i)),6)="ftp://" or left(trim(WordsArray(i)),7)="mailto:" then
		WordsArray(i)="<a href=""" & WordsArray(i) & """ target=""_blank"">" & WordsArray(i) & "</a>"
		end if
	next
	for i=0 to UBound(WordsArray)
		if isNumber(WordsArray(i)) then WordsArray (i) = strReverse(wordsArray(i))
	Next
	ReturnString=ReplaceInvalidPars(" " & Join(WordsArray, " ") & " ")
	ToVHeb = strReverse(ReturnString)
End Function

Function LongHeb(Paragraph, n)
on error resume next
paragraph=cstr(paragraph)
Dim I
Dim k
Dim OutputLineLength
Dim OutputLine
Dim Word
Dim EnglishParagraph
Dim wordsArray()
Dim thisword
If isnull(paragraph) Then
	LongHeb=""
	Exit Function
End If
redim wordsArray(0)
maxar=0
i=1
for i=1 to len(paragraph)
	if Mid(paragraph,i,1)<>" " and not isnull(Mid(paragraph,i,1))then
		 thisword=thisword & Mid(paragraph,i,1)
	else
	 if trim(thisword)<>"" then
	 wordsArray(maxar)=trim(thisword)
	 maxar=ubound(wordsArray)+1
	 redim preserve wordsArray(maxar)
	 end if
	 thisword=""
	end if 
next	
wordsArray(maxar)=trim(thisword)
maxar=ubound(wordsArray)+1
redim preserve wordsArray(maxar)

'i=i+1
'loop
'wordsArray=split(Paragraph , " ")
' If the text is an English text return the text as is.
If UBound(wordsArray) > 3 Then
	K=4
Else 
	K=UBound(wordsArray)
End If

EnglishParagraph=True
For I=0 to K
	If NOT isnumber(wordsArray(I)) Then
		EnglishParagraph=False
	End If
Next

If EnglishParagraph Then
	' Response.Write("English Paragraph")
	LongHeb=Paragraph
	Exit Function
End If


If Len(Paragraph) <= n Then
	LongHeb=ToVHeb(Paragraph)
	'response.write("null")
	Exit Function
End If 


' This function receives a long paragraph that probably spans
' across more than few lines, and n - the required number of characters
' in a line.
' It returns a Hebrew paragrapg suitabl for displaying on web pages.
'
' Algorithm:
' Create a long stream of words from the input string.
OutputLine=""
' LongHeb="<DIV ALIGN=right><FONT FACE=""""Web Hebrew AD""""> "
LongHeb=""
CRLF = chr(13) + chr(10)

' Now the real work...

For Each Word in wordsArray
'res.write(word)
	' Response.write(Word & "<br />")

	If (isNumber(Word)) Then
		Word=StrReverse(Word)
	End If
    ' Response.Write (OutputLineLength)
	If (Len(OutputLine)+Len(Word)) <= n Then
		OutputLine=OutputLine & Word & " "
		OutputLineLength=OutputLineLength+Len(Word)
	Else 
		LongHeb=LongHeb & StrReverse(OutputLine)  & "<br />" & CRLF
		OutputLine=Word & " "
		OutputLineLength=Len(Word)
	End If
Next
LongHeb=LongHeb & StrReverse(OutputLine) & "<br />" & CRLF
' LongHeb=LongHeb & "</FONT></DIV>"
End Function

Function isNumber(Word)
  ' Checks whether a word is a number.
  if isNull(Word) then
    isNumber=False
	Exit Function
  End If
  if Len(Word) < 1 Then
    isNumber=False
	Exit Function
  End If
  for i=1 to Len(Word)
    ' Response.Write(mid(Word,i,1) & "<br />")
	LetterCode=asc(mid(Word,i,1))
	if (LetterCode >= asc("0") and LetterCode <= asc("9")) or  (LetterCode >= asc("A") and LetterCode <= asc("Z")) or (LetterCode >= asc("a") and LetterCode <= asc("z")) then
	  isNumber = True
	  Exit Function
	  ' Response.Write("This is a number <br />")
	End If
	  Next
 
 isNumber=False
End Function
 


Function MultipleLines2Lang(lang,sText, nSize)
    if lang = 0 then 
        MultipleLines2Lang = sText
    End If
    if lang = 1 then 
        MultipleLines2Lang = MultipleLines2Heb (sText, nSize)
    End If
end Function

Function ToLang (lang,HWord)
    if lang = 0 then 
        ToLang = HWord
    End If
    if lang = 1 then 
        ToLang = ToVHeb (HWord)
    End If
End Function


Function ToLang_browser (browser,lang,HWord)
    if lang = 0 then 
        ToLang_browser = HWord
    End If
    if lang = 1 then 
      if browser = "netscape" then
        ToLang_browser = ToVHeb (HWord)
      else
        ToLang_browser = HWord
      end if
    End If
End Function


Function LongLang(lang,Paragraph, n)
    if lang = 0 then 
	LongLang = Paragraph
    End If
    if lang = 1 then 
	LongLang = LongHeb(Paragraph, n)
    End If
End Function

Function b()
   if session("browser") <> "netscape" then 
       response.write "<b>"
   end if
End Function

Function eb()
   if session("browser") <> "netscape" then 
       response.write "</b>"
   end if
End Function


Function browser_font_face()
   if session("browser") = "netscape" then 
       str = "<font face=""globes"">"
       response.write str 
   else
       response.write "<font>"
   end if
End Function

Function browser_value(val1,val2)
   if session("browser") = "netscape" then 
       response.write val1
   else
       response.write val2
   end if
End Function
%>

