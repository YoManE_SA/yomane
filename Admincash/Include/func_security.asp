<%
	'
	' 20080513 Tamir
	'
	' New security mechanism - groups
	'
%>
	<!--#include file="security_class.asp"-->
<%
	'20100712 Tamir - page access logging added
	Sub LogPageAccessByUser(bBlocked, bReadonly)
		Dim nBlocked : If bBlocked Then nBlocked = 1 Else nBlocked = 0
		Dim nReadonly : If bReadonly Then nReadonly = 1 Else nReadonly = 0
		Dim sIP : sIP = Request.ServerVariables("REMOTE_ADDR")
		Dim dbData : Set dbData = Server.CreateObject("ADODB.Connection")
		dbData.Open "Provider=SQLOLEDB;" & Application("DBConnect")
		dbData.Execute "EXEC SecurityLogAdd '" & PageSecurity.Username & "', '" & PageSecurity.Path & "', " & nBlocked & ", " & nReadonly & ", '" & sIP & "'"
		dbData.Close
	End Sub

	Function AccessLevelCheck(fPageLevel, sPageClosingTags, bRedirect)
		dim sManage, bManaged, sActive, bActive, sText, bPermitted
		bManaged=PageSecurity.IsDocumentManaged
		if bManaged then
			sText="+"
		else
			sText="-"
		end if		
		bActive = PageSecurity.IsActive AND PageSecurity.IsActiveUser
		if bActive then
			sActive="Full Access"
		else
			sActive="Read Only"
		end if
		%>
			<script language="javascript" type="text/javascript" src="../js/security.js"></script>
		<%
		if PageSecurity.IsAdmin then
			sManage=" style=\""cursor:pointer\"" title=\""" & sActive & ", "
			if bManaged then
				sManage=sManage & "Managed"
			else
				sManage=sManage & "Unmanaged"
			end if
			sManage=sManage & " - click to manage permissions\"" class=\""alwaysactive\"" onclick=\""OpenSecurityManager();\"""
		else
			sManage=" title=\""" & sActive & "\"""
		end if
		bPermitted = PageSecurity.IsPermitted Or PageSecurity.IsPermittedUser
		If PageSecurity.IsUserLogged Or PageSecurity.IsDocumentLogged Then LogPageAccessByUser Not(bPermitted), Not(bActive)
		if bPermitted then
			%>
			<script language="JavaScript" type="text/javascript" defer="defer">
				var objRef = document.getElementById("pageMainHeadingTd")
				objRef.innerHTML = "<span<%= sManage %> id=\"pageSecurityHeading\"><%= sText %></span>&nbsp; " + objRef.innerHTML
			</script>
			<%
			if not bActive then
				%>
					<script language="javascript" type="text/javascript">addEvent(window, "load", DeactivateContent);</script>
				<%
			end if
		else
			if bRedirect then
				response.Redirect "security_message.aspx?PageURL=" & PageSecurity.Path
			else
				%>
				<script language="JavaScript" type="text/javascript">
					var objRef = document.getElementById("pageMainHeadingTd")
					objRef.innerHTML = "<span<%= sManage %> id=\"pageSecurityHeading\"><%= sText %></span>&nbsp; " + objRef.innerHTML
				</script>				
				<div align="center" style="width:100%">
				    <div align="center" style="direction:ltr;text-align:center;">
				        <br /><br /><hr color="silver" size="1" noshade  />
					    <span class="txt13b">Security message: access denied.</span><br />
					    <span class="txt11">You are not authorized to see the content of this page.</span><br />
					    <hr color="silver" size="1" noshade  />
				    </div>
				</div>
					<%				
				Response.Write(sPageClosingTags & "</body></html>")
				Response.End
			end if
		end if
	End Function
%>