<%
	Sub AddInvoiceLine(nTransactionPayID, nMerchantID, nCurrency, curExchangeRate, sText, sUnitAmount, sQuantity, sTotal)
		If sQuantity = 0 Then Exit Sub
		if sUnitAmount="---" then sUnitAmount=sTotal/sQuantity
		dim sSQL : sSQL="EXEC InvoiceLineCreate " & nTransactionPayID & ", '', " & nMerchantID & ", '" & sText & "', " & sQuantity & ", " & FormatNumber(sUnitAmount * curExchangeRate, 2, True, False, False) & ", " & nCurrency & ", " & FormatNumber(curExchangeRate, 4, True, False, False) & ", '" & PageSecurity.Username & "';"
		ExecSQL sSQL
	End Sub

	Function GetDebitCompanyFeeExcludeInvoice(dbId)
		Dim rs, res : res = ","
		Set rs = oledbData.Execute("Select TransAmountType_id From DebitCompanyFeeExcludeInvoice Where DebitCompany_id=" & dbId)
		Do While Not rs.EOF
			res = res & rs(0) & ","
		  rs.MoveNext
		Loop
		rs.Close
		If res = "," Then res = ""
		GetDebitCompanyFeeExcludeInvoice = res
	End Function

	Function GetPaymentDebitCompany(nPayID)
		GetPaymentDebitCompany = -1
		If TestNumVar(ExecScalar( _
			"Select Count(Distinct DebitCompany_id) From DebitCompanyFeeExcludeInvoice Where DebitCompany_id IN(" & _
			"	Select DebitCompanyID " & _
			"	From tblCompanyTransPass " & _
			"	Where PrimaryPayedID=" & nPayID & " And IsNull(DebitCompanyID, 0) Not IN(0, 1) " & _
			"	Group By DebitCompanyID" & _
			") Group By DebitCompany_id", 0), 0, -1, 0) > 1 Then Exit Function
		GetPaymentDebitCompany = TestNumVar(ExecScalar("Select Top 1 DebitCompanyID From tblCompanyTransPass Where PrimaryPayedID=" & nPayID & " And IsNull(DebitCompanyID, 0) Not IN(0, 1) Group By DebitCompanyID", 0), 0, -1, 0)
	End Function

	Function CreateInvoiceForSettlement(nPayID)
		dim sSQL, rsData, invDebitComapny, excludeAmounts, invCurrency
		invDebitComapny = GetPaymentDebitCompany(nPayID)
		If invDebitComapny = -1 Then 
			Response.Write("the settlemnt include transactions from more than one direct debit copmany with invoice exclude rows, can't produce invoice")
			Response.End
		End If
		excludeAmounts = GetDebitCompanyFeeExcludeInvoice(invDebitComapny)
		sSQL="SELECT tblCompany.ID, tblCompany.BillingCompanys_id, tblCompany.isChargeVAT," & _
		" tblBillingCompanys.LanguageShow, tblTransactionPay.Currency, tblBillingCompanys.currencyShow " & _
		" FROM tblTransactionPay INNER JOIN tblCompany ON tblTransactionPay.CompanyID=tblCompany.ID" & _
		" INNER JOIN tblBillingCompanys ON tblTransactionPay.BillingCompanys_id=tblBillingCompanys.BillingCompanys_id" & _
		" WHERE tblTransactionPay.ID=" & nPayID
		set rsData=oledbData.Execute(sSQL)
		if rsData.EOF then
			rsData.Close
			CreateInvoiceForSettlement=-1 'merchant not found
		Else
			Dim nTextList, AmountSum, gTotal, nNewID, textAdd
			If lCase(rsData("LanguageShow")) = "heb" Then
				nTextList = Array("���� ���� ����", "���� ���� �����", "���� �����", "���� ����� �����", "���� ����� ����", "���� ����� ���� ������", "����� �����", "���� ����� ����")
			Else
				nTextList = Array("Authorized transaction fee", "Refund transaction fee", "Processing fee", "Financing fees", "Transaction clarification fee", "Transaction Chargeback fee", "Other fees", "Handling Fee")
			end if
			Set AmountSum = CalcCompanyPaymentTotal(nPayID, rsData("ID"), rsData("Currency"), "")
			Set gTotal = AmountSum.PaymentMethodTotal(PMD_CC_MIN, PMD_EC_MAX)
			'Set gTotal = AmountSum.Totals
			invCurrency = ExecScalar("Select CurrencyInvoice From [Finance].[InvoiceIssuerCurrency] Where InvoiceIssuer_id= " & rsData("BillingCompanys_id") & " And CurrencySettlement=" & AmountSum.SettlementCurrency, rsData("currencyShow"))
			If invCurrency <> AmountSum.SettlementCurrency Then textAdd = " (" & GetCurISO(AmountSum.SettlementCurrency) & ")"
			curExchangeRate = ConvertCurrencyRate(AmountSum.SettlementCurrency, invCurrency)

			If InStr(1, excludeAmounts, "," & TAT_TransactionFee & ",") = 0 Then _
				AddInvoiceLine nPayID, rsData("ID"), rsData("Currency"), curExchangeRate, nTextList(0) & textAdd, "---", gTotal.NormCount, gTotal.NormLineFee
			If InStr(1, excludeAmounts, "," & TAT_RefundFee & ",") = 0 Then _
				AddInvoiceLine nPayID, rsData("ID"), rsData("Currency"), curExchangeRate, nTextList(1) & textAdd, "---", gTotal.RefCount, gTotal.RefFees
			If InStr(1, excludeAmounts, "," & TAT_ProcessingFee & ",") = 0 Then _
				AddInvoiceLine nPayID, rsData("ID"), rsData("Currency"), curExchangeRate, nTextList(2) & textAdd, "---", gTotal.NormCount, gTotal.NormRatioFee
			'If InStr(1, excludeAmounts, "," &  & ",") = 0 Then
				AddInvoiceLine nPayID, rsData("ID"), rsData("Currency"), curExchangeRate, nTextList(3) & textAdd, "---", gTotal.FinanceCount, gTotal.FinanceFee
			If InStr(1, excludeAmounts, "," & TAT_ClarificationFee & ",") = 0 Then _
				AddInvoiceLine nPayID, rsData("ID"), rsData("Currency"), curExchangeRate, nTextList(4) & textAdd, "---", gTotal.ClrfCount, gTotal.ClrfFees
			If InStr(1, excludeAmounts, "," & TAT_ChargebackFee & ",") = 0 Then _
				AddInvoiceLine nPayID, rsData("ID"), rsData("Currency"), curExchangeRate, nTextList(5) & textAdd, "---", gTotal.DenCount, gTotal.DenFees
			If InStr(1, excludeAmounts, "," & TAT_SystemCharge & ",") = 0 Then _
				AddInvoiceLine nPayID, rsData("ID"), rsData("Currency"), curExchangeRate, nTextList(6) & textAdd, "---", AmountSum.FeesAutoAndManual.TotalCount, AmountSum.FeesAutoAndManual.TotalFees
			If InStr(1, excludeAmounts, "," & TAT_HandlingFee & ",") = 0 Then _
				AddInvoiceLine nPayID, rsData("ID"), rsData("Currency"), curExchangeRate, nTextList(7) & textAdd, "---", gTotal.HandlingCount, gTotal.HandlingFee

			sSQL = "EXEC InvoiceDocumentCreate" & _
				" " & nPayID & "," & _
				" " & IIF(excludeAmounts <> "", "1", "0") & "," & _
				" " & rsData("BillingCompanys_id") & "," & _
				" " & rsData("isChargeVAT") & "," & _
				" 0," & _
				" " & rsData("ID") & "," & _
				" ''," & _
				" " & invCurrency & "," & _
				" " & FormatNumber(curExchangeRate, 4, True, False, False) & "," & _
				" '" & PageSecurity.Username & "';"
			ExecSQL sSQL
			CreateInvoiceForSettlement = ExecScalar("SELECT id_InvoiceNumber FROM tblInvoiceDocument Where id_TransactionPayID=" & nPayID, -2)
		end if
		rsData.Close
	End Function
%>