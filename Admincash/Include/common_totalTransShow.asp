<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/CalcTransPayment.asp"-->
<%Server.ScriptTimeout = 600%>
<html>
<head>
	<title><%= COMPANY_NAME_1%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/MainIE.css">
	<style type="text/css">.grandTotal { font-size:12px; font-weight:bold; }</style>
	<script type="text/javascript">
		function showSection(SecNum) {
			if(trSec) { // obj exist
			if(trSec.length) { // means if(obj.length > 0)
				for (i = 0; i < trSec.length; i++) {
					if(i==SecNum) { trSec[SecNum].style.display=''; }
					else { trSec[i].style.display='none'; }
				}
				<% If request("IsIFrame")<>"1" then %>window.resizeTo(760, (SecNum==2?672:372));<% end if %>
			}
			else { // means not an array (!obj.length)
				trSec.style.display='';
			}}
		}
	</script>
</head>
<%
Dim FeeRs, sWhere, nCompanyID, nPayID, nCurrency, nCompanyName, nTextArr, nCountDraw
Set FeeRs = Server.CreateObject("Adodb.Recordset")
sTransIdToShow = TestNumVar(Request("transID"), 0, -1, 0)
nCompanyID = TestNumVar(Request("CompanyID"), 0, -1, Session("CompanyID"))
nPayID = TestNumVar(Request("PayID"), 0, -1, 0)
nCurrency = TestNumVar(Request("Currency"), 0, MAX_CURRENCY, 0)
nLng = TestNumVar(Request("LNG"), 0, 1, 0)

If trim(Request("isFilter"))="1" Then
	dDateTo = dbText(request("toDate"))
	dDateFrom = dbText(request("fromDate"))
	If IsDate(dDateFrom) then sWhere = sWhere & " And tblCompanyTransPass.InsertDate>='" & DateValue(dDateFrom) & " 00:00:00'"
	If IsDate(dDateTo) then sWhere = sWhere & " And tblCompanyTransPass.InsertDate<='" & DateValue(dDateTo) & " 23:59:59'"
	If Request("TransID") <> "" Then sWhere = sWhere & " And tblCompanyTransPass.ID IN(" & Request("TransID") & ")"
	If Request("PaymentMethod") <> "" And Request("PaymentMethod") <> "1, 2, 3, 15, 20, 21, 22, 23, 24, 25, 26, 28, 29" Then _
		sWhere = sWhere & " And PaymentMethod IN(" & Request("PaymentMethod") & ")"
	If Request("CreditType") <> "" And Request("CreditType") <> "1, 2, 6, 8, 0" Then _
		sWhere = sWhere & " And CreditType IN(" & Request("CreditType") & ")"
	If Request("PaymentMethod_ID") <> "" Then sWhere = sWhere & " And PaymentMethod_id IN(" & Request("PaymentMethod_ID") & ")"
	If Request("TypeTerminal") <> "" Then sWhere = sWhere & " And (tblCompanyTransPass.TerminalNumber='" & Request("TypeTerminal") & "' OR tblCompanyTransPass.TerminalNumber='')"
	If Request("DeniedStatus") <> "" Then sWhere = sWhere & " And DeniedStatus " & IIF(Request("DeniedStatus") = "Y", ">0", "IN(" & Request("DeniedStatus") & ")")
	if sWhere <> "" Then sWhere = Right(sWhere, Len(sWhere) - 4)
ElseIf nPayID = 0 Then
	sWhere = "IsTestOnly=0"
End if
Set AmountSum = CalcCompanyPaymentTotal(nPayID, nCompanyID, nCurrency, sWhere)
nCurrency = AmountSum.mCurrency
%>
<body leftmargin="6" topmargin="6" rightmargin="6" bottommargin="6" class="itext" dir="<%=IIF(nLng = 0, "rtl", "ltr") %>">
	<%
	If request("isIframe")="0" then
		%>
		<table width="100%" style="border:1px solid gray;" border="0" cellspacing="2" cellpadding="1" align="center">
		<tr>
			<td align="left" bgcolor="#e9e9e9" class="txt15b">
				&nbsp;<%= dbtextShow(AmountSum.mCompanyName) %> - <%= GetCurText(AmountSum.mCurrency) %><br />
			</TD>
		</tr>
		</table>
		<br />	
		<%
	End If
	%>
	<div align="center" id="waitMsg" style="text-decoration:blink">
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="txt12" style="color:gray;" align="center">
				please wait<br />
				calculating amounts<br />
			</td>
		</tr>
		<tr><td height="4"></td></tr>
		<tr>
			<td><img src="../images/loading_animation_orange2.gif" style="border:1px solid silver;" alt="" width="123" height="6" border="0"><br /></td>
		</tr>
		</table>
	</div>

	<%
	Function drawSumLine(xTitle, xQty, wDebit, xDeposit, xFuture, xCssClass, xDrawLine)
		If(xQty = "" Or CStr(xQty) = "0") And (xDeposit = "" Or CStr(xDeposit) = "0") And (wDebit = "" Or CStr(wDebit) = "0") Then Exit Function
		nCountDraw = nCountDraw + 1
		If xDrawLine And nCountDraw > 1 Then drawSumLine = "<tr><td height=""1"" colspan=""4"" style=""border-bottom:1px dashed silver""><img src=""../images/1_space.gif"" width=""1"" height=""1"" border=""0""><br /></td></tr>"
		drawSumLine = drawSumLine & "<tr bgcolor=""white"">" & _
			"<td class=""" & xCssClass & """ valign=""top"">" & xTitle & "<br /></td>" & _
			"<td class=""" & xCssClass & """ valign=""top"">" & xQty & "<br /></td>"
		If wDebit <> "" Then drawSumLine = drawSumLine & "<td valign=""top"" class=""" & xCssClass & """ style=""color:red;"">" & FormatCurr(nCurrency, wDebit) & "<br /></td>" _
		Else drawSumLine = drawSumLine & "<td></td>"
		If xDeposit <> "" Then drawSumLine = drawSumLine & "<td class=""" & xCssClass & """>" & FormatCurr(nCurrency, xDeposit) & "&nbsp;" _
		Else drawSumLine = drawSumLine & "<td>"
		If nPayID = 0 And xFuture <> 0 Then drawSumLine = drawSumLine & "(" & FormatCurr(nCurrency, xDeposit + xFuture) & ")"
		drawSumLine = drawSumLine & "</td></tr>"
	End Function
	
	if Not AmountSum Is Nothing Then
		If nLng = 1 Then nTextArr = Array("Totals", "Show Fees", "Description", "Qty.", "Fee", "Deposit") _
		Else nTextArr = Array("�������", "��� �����", "�����", "����.", "����", "����") 
		%>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td id="trSec" style="display:block;">
				<table width="100%" border="0" cellspacing="0" cellpadding="1">
				<tr><td colspan="4">
				 <b style="font-size:12px;">Show Totals</b>
				 <span class="txt10">|</span> <a href="#" onclick="showSection(1);">Show Fees</a>
				 <span class="txt10">|</span> <a href="#" onclick="showSection(2);">Show RR</a>
				</td></tr>
				<tr><td height="6"></td></tr>
				<tr>
					<td class="txt11b" width="55%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;"><%=nTextArr(2)%><br /></td>
					<td class="txt11b" width="12%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;"><%=nTextArr(3)%><br /></td>
					<td class="txt11b" width="12%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;"><%=nTextArr(4)%><br /></td>
					<td class="txt11b" width="21%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;"><%=nTextArr(5)%><br /></td>
				</tr>
				<tr><td height="3"></td></tr>
				<%
				Set gTotal = AmountSum.Totals
				Set gPMTotal = AmountSum.PaymentMethodTotal(PMD_CC_MIN, PMD_EC_MAX)
				If nLng = 1 Then nTextArr = Array("Authorized Transactions", "&#187; Processing Fee", "Admin Transactions", "Other Fees", "Refund Transactions", "Customer Chargebacks", "&#187; Clarification Fee", "Financing Fee", "Total Amount & Fees", "Total Fees including VAT", "Total Payment to Merchant", "Rolling Reserve", "Reserve Release", "Handling Fee", "Direct Deposit", "Bank Fees", "Cashback") _
				Else nTextArr = Array("������ �������", "&#187; ���� �����", "������ ����", "����� �����", "������ ����� ����", "������ �������", "&#171; ���� ����� ����", "���� ����� ����� (������)", "����� ������ ������", "����� ����� ���� ��""�", "���� ������ ������ ������", "���� ����� �����", "���� ����� �����", "���� ����� ����", "����� �����", "����� ���", "�����")
				%>
				<%=drawSumLine(nTextArr(0), gPMTotal.NormCount, gPMTotal.NormLineFee, gPMTotal.NormAmount, gPMTotal.NormFutInst, "txt11", False) %>
				<%=drawSumLine(nTextArr(1), gPMTotal.NormCount, gPMTotal.NormFees - gPMTotal.NormLineFee, "", 0, "txt11", False) %>
				<%=drawSumLine(nTextArr(4), gPMTotal.RefCount, gPMTotal.RefFees, gPMTotal.RefAmount, 0, "txt11", True) %>
				<%=drawSumLine(nTextArr(16), gPMTotal.CashbackCount, gPMTotal.CashbackFees, gPMTotal.CashbackAmount, 0, "txt11", True) %>
                    
				<%=drawSumLine(nTextArr(5), gPMTotal.DenCount, gPMTotal.DenFees, -gPMTotal.DenAmount, 0, "txt11", True) %>
				<%=drawSumLine(nTextArr(6), gPMTotal.ClrfCount, gPMTotal.ClrfFees, "", 0, "txt11", False) %>
				<%=drawSumLine(nTextArr(15), AmountSum.BankFees.TotalCount, AmountSum.BankFees.TotalFees, AmountSum.BankFees.TotalAmount, 0, "txt11", True) %>
				<%
			    If AmountSum.NotPayPercent > 0 Then _
			        Response.Write(drawSumLine(nTextArr(14), "(-" & AmountSum.NotPayPercent & "%)", "", -AmountSum.TotalNotPayedAmount, 0, "txt11", False))
				%>
				<%=drawSumLine(nTextArr(2), AmountSum.Admin.TotalCount, AmountSum.Admin.TotalFees, AmountSum.Admin.TotalAmount, 0, "txt11", True) %>
				<%=drawSumLine(nTextArr(3), AmountSum.FeesAutoAndManual.TotalCount, AmountSum.FeesAutoAndManual.TotalFees, AmountSum.FeesAutoAndManual.TotalAmount, 0, "txt11", False) %>

				<%=drawSumLine(nTextArr(13), gTotal.HandlingCount, gTotal.HandlingFee, "", 0, "txt11", False) %>
				<%=drawSumLine(nTextArr(7), gPMTotal.FinanceCount, gPMTotal.FinanceFee, "", 0, "txt11", True) %>
				<%=drawSumLine(nTextArr(11), IIF(AmountSum.RollingReserve <> 0, 1, 0), 0, AmountSum.RollingReserve, 0, "txt11", True) %>
				<%=drawSumLine(nTextArr(12), AmountSum.mRRRetCount, 0, AmountSum.ReserveReturn, 0, "txt11", False) %>
				<tr><td height="1" bgcolor="gray" colspan="4" class="txt11"></td></tr>
				<%=drawSumLine(nTextArr(8), "", AmountSum.TotalFeesAmount, AmountSum.TotalAmount, gPMTotal.NormFutInst, "txt11", False) %>
				<%
				If AmountSum.mVAT > 0 Then _
					Response.Write(drawSumLine(nTextArr(9) & "(" & (AmountSum.mVAT * 100) & "%)", "", AmountSum.VATAmount, "", 0, "txt11", False))
				%>
				<%=drawSumLine(nTextArr(10), "", "", AmountSum.Total, gPMTotal.NormFutInst, "grandTotal", False) %>
				</table>
			</td>
		</tr>
		<tr id="trSec" style="display:none;">
			<td>	
				<table border="0" width="100%" cellspacing="0" cellpadding="1">
				<tr><td colspan="7">
				 <a href="#" onclick="showSection(0);">Show Totals</a>
				 <span class="txt10">|</span> <b style="font-size:12px;">Show Fees</b>
				 <span class="txt10">|</span> <a href="#" onclick="showSection(2);">Show RR</a>
				</td></tr>
				<tr><td height="6"></td></tr>
				<tr>
					<th class="txt11b" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;">Credit Card</th>
					<th class="txt11b" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;">Country</th>
					<th class="txt11b" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;">Transaction</th>
					<th class="txt11b" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;">Pre-Auth</th>
					<th class="txt11b" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;">Refund</th>
					<th class="txt11b" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;">Clarification</th>
					<th class="txt11b" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;">Charge<br />back</th>
				</tr>
				<%
				If nPayID > 0 Then
					sSQL="SELECT tblTransactionPayFees.*,  Name AS pm_Name FROM tblTransactionPayFees LEFT JOIN List.PaymentMethod AS pm ON CCF_PaymentMethod = pm.PaymentMethod_id" & _
					" WHERE CCF_TransactionPayID=" & nPayID & " AND CCF_CompanyID=" & nCompanyID & " AND CCF_ExchangeTo=" & nCurrency
				Else
					sSQL="SELECT tblCompanyCreditFees.*,  Name AS pm_Name FROM tblCompanyCreditFees LEFT JOIN List.PaymentMethod AS pm ON CCF_PaymentMethod = pm.PaymentMethod_id" & _
					" WHERE CCF_CompanyID=" & nCompanyID & " And CCF_ExchangeTo>-1 And CCF_ExchangeTo=" & nCurrency
				End If
				FeeRs.Open sSQL & " ORDER BY pm.PaymentMethod_id", oledbData, 0, 1
				Do While Not FeeRs.EOF
				%>
					<tr>
					<td class="txt11"><%=FeeRs("pm_Name")%> - <%=GetCurText(FeeRs("CCF_CurrencyID"))%></td>
					<td class="txt11"><%=FeeRs("CCF_ListBins")%></td>
					<td class="txt11"><%=FormatCurr(nCurrency, FeeRs("CCF_FixedFee"))%> / <%=FeeRs("CCF_PercentFee")%>%</td>
					<td class="txt11"><%=FormatCurr(nCurrency, FeeRs("CCF_ApproveFixedFee"))%></td>
					<td class="txt11"><%=FormatCurr(nCurrency, FeeRs("CCF_RefundFixedFee"))%></td>
					<td class="txt11"><%=FormatCurr(nCurrency, FeeRs("CCF_ClarificationFee"))%></td>
					<td class="txt11"><%=FormatCurr(nCurrency, FeeRs("CCF_CBFixedFee"))%></td>
					</tr>			
				<%
				 FeeRs.MoveNext
				Loop
				FeeRs.Close
				%>
				</table>
			</td>
		</tr>
		<tr id="trSec" style="display:none;">
			<td>	
				<table border="0" width="100%" cellspacing="0" cellpadding="1">
				<tr><td colspan="4">
				 <a href="#" onclick="showSection(0);">Show Totals</a>
				 <span class="txt10">|</span> <a href="#" onclick="showSection(1);">Show Fees</a>
				 <span class="txt10">|</span> <b style="font-size:12px;">Show RR</b>
				</td></tr>
				<tr><td height="6"></td></tr>
				<tr>
					<th style="width:16px; background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;"><br /></th>
					<th class="txt11b" width="170" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;">Date & Time</th>
					<th class="txt11b" width="100" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;">Settlement #</th>
					<th class="txt11b" width="100" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:right;">Amount</th>
					<th class="txt11b" valign="bottom" style="background-color:#e9e9e9; border-bottom:1px solid gray; text-align:left;padding-left:50px;">Comment</th>
				</tr>
				<%
				nAmountTotal = 0
				nReturnTotal = 0
				nAmountCount = 0
				nReturnCount = 0
				FeeRs.Open "Select InsertDate, PrimaryPayedID, Currency, Amount, CreditType, PaymentMethodDisplay From tblCompanyTransPass Where" & _
				" PaymentMethod=" & PMD_RolRes & " And CompanyID=" & nCompanyID & " And Currency=" & nCurrency & _
				" ORDER BY CASE OriginalTransID WHEN 0 THEN ID ELSE OriginalTransID END DESC,	OriginalTransID DESC, ID DESC", oledbData, 0, 1
				Do While Not FeeRs.EOF
					If FeeRs("CreditType") = 0 Then
						sIconName = "icon_ArrowR.gif"
						sFontColor = "Red"
						sBGColor = ""
						nAmountTotal=nAmountTotal+FeeRs("Amount")
						nAmountCount=nAmountCount+1
					Else
						sIconName = "icon_ArrowG.gif"
						sFontColor = "#000000"
						sBGColor = "rgb(239, 255, 239)"
						nReturnTotal=nReturnTotal+FeeRs("Amount")
						nReturnCount=nReturnCount+1
					End If
					%>
					<tr style="background-color:<%= sBGColor %>;">
						<td><img src="../images/<%= sIconName %>" align="middle" /></td>
						<td class="txt11"><%=FeeRs("InsertDate")%></td>
						<td class="txt11"><%=FeeRs("PrimaryPayedID")%></td>
						<td style="color:<%=sFontColor%>;text-align:right;" class="txt11"><%= sAmountIndent %><%=FormatCurrWCT(FeeRs("Currency"), FeeRs("Amount"), FeeRs("CreditType"))%></td>
						<td class="txt11" style="padding-left:50px;"><%=FeeRs("PaymentMethodDisplay")%></td>
					</tr>
					<%
					FeeRs.MoveNext
				Loop
				FeeRs.Close
				
				%>
				<tr><td colspan="5" style="border-top:1px solid gray;"><IMG SRC="../images/1_space.gif" /></td></tr>
				<tr>
					<td></td>
					<td></td>
					<td class="txt11b">Total Rolling (<%= nAmountCount %>)</td>
					<td class="txt11b" style="text-align:right;color:Red;"><%=FormatCurr(nCurrency, -nAmountTotal)%></td>
					<td></td>
				</tr>
				<tr style="background-color:rgb(239, 255, 239);">
					<td></td>
					<td></td>
					<td class="txt11b">Total Released (<%= nReturnCount %>)</td>
					<td class="txt11b" style="text-align:right;"><%=FormatCurr(nCurrency, nReturnTotal)%></td>
					<td></td>
				</tr>
				<tr><td colspan="5" style="border-top:1px solid gray;"><IMG SRC="../images/1_space.gif" /></td></tr>
				<tr>
					<td></td>
					<td></td>
					<td class="txt11b">Total</td>
					<td class="txt11b" style="text-align:right;"><%=FormatCurr(nCurrency, nReturnTotal-nAmountTotal)%></td>
					<td></td>
				</tr>
				</table>
			</td>
		</tr>		
		</table>
		<br />
		<%
	Else
		%>
		<table border="0" cellspacing="2" cellpadding="2" align="center">
		<tr>
			<td align="right" class="txt12">
				No records found<br />
			</td>
		</tr>
		</table>
		<%
	End if
	%>
	<script language="JavaScript1.2" type="text/javascript">document.getElementById('waitMsg').style.display='none';</script>
	<%
call closeConnection()
Server.ScriptTimeout = 90
%>
</body>
</html>
<script language="javascript" defer="defer">
	focus();
</script>