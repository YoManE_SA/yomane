<%
Const CCRT_None			= 0
Const CCRT_Terminal		= 1
Const CCRT_Country		= 2
Const CCRT_Bin			= 3
Const CCRT_BlackList	= 4
Const CCRT_WhiteList	= 5

Public Function GetMerchantTerminal(ixCompanyID, ixPaymentMethod, ixCurrency, binsCountry, transBin, creditType, RetTerminalNumber, RetCC_Options)
	Dim iRs
	RetCC_Options = ixCurrency
	Set iRs = oleDbData.Execute("Select * From tblCompanyPaymentMethods Where CPM_CompanyID=" & ixCompanyID & " And CPM_PaymentMethod=" & ixPaymentMethod & " And CPM_CurrencyID=" & ixCurrency)
	If iRs.EOF Then
		GetMerchantTerminal = -2
	Else
		If iRs("CPM_IsDisabled") Then 
			GetMerchantTerminal	= -1
		Else
			GetMerchantTerminal	= CalcMerchantRestrictions(iRs("CPM_ID"), 0, binsCountry, transBin, RetTerminalNumber, RetCC_Options)
		End If
	End If
	iRs.Close()
	Call fn_returnResponse(LogSavingChargeDataID, requestSource, debitCompany, X_TransType, dpRet, nTransactionID, nInsertDate, X_OrderNumber, Formatnumber(X_Amount,2,-1,0,0), X_Payments, X_Currency, dpApproval, X_Comment)
End Function

Public Function CalcMerchantRestrictions(nPMID, nParentID, binsCountry, transBin, RetTerminalNumber, RetCC_Options)
	Dim iRs
	Set iRs = oleDbData.Execute("Select * From tblCompanyCreditRestrictions Where CCR_CPM_ID=" & nPMID & " And CCR_ParentID=" & nParentID)
	Do While Not iRs.EOF
		Select Case TestNumVar(iRs("CCR_Type"), 0, -1, 0)
		Case CCRT_None
			iRs.Close()
			Exit Function
		Case CCRT_Terminal
			RetTerminalNumber = iRs("CCR_TerminalNumber")
			RetCC_Options = TestNumVar(iRs("CCR_ExchangeTo"), 0, -1, RetCC_Options)
			CalcMerchantRestrictions = iRs("CCR_ID")
			If SendCharge() Then 
				iRs.Close()
				Exit Function
			End if	
		Case CCRT_Country
			bValue = (InStr(1, iRs("CCR_ListValue"), binsCountry) > 0)
		Case CCRT_Bin
			bValue = (InStr(1, iRs("CCR_ListValue"), transBin) > 0)
		Case CCRT_BlackList
			bValue = (TestNumVar(ExecScalar("Select ID From tblBlackListBIN Where BIN='" & transBin & "'", 0), 0, -1, 0) > 0)
		Case CCRT_WhiteList
			bValue = (TestNumVar(ExecScalar("Select ID From tblWhiteListBIN Where BIN='" & transBin & "'", 0), 0, -1, 0) > 0)
		End Select
		If bValue Then
			CalcMerchantRestrictions = iRs("CCR_ID")
			iRs.Close()
			CalcMerchantRestrictions = CalcMerchantRestrictions(nPMID, CalcMerchantRestrictions, binsCountry, transBin, RetTerminalNumber, RetCC_Options)
			Exit Function
		End If
	  iRs.MoveNext
	Loop
	iRs.Close()
End Function
Public Function GetTransactionFeesEx(RatioChargeParam, ixCompanyID, ixPaymentMethod, ixOCurrency, ixAmount, ixTypeCredit, ixTerminalNumber)
	Dim rsFees
	sSQL = "Select CCR_PercentFee, CCR_RefundFixedFee, CCR_FixedFee From tblCompanyCreditRestrictions Left Join tblCompanyPaymentMethods ON(tblCompanyCreditRestrictions.CCR_CPM_ID = tblCompanyPaymentMethods.CPM_ID) " & _
			"Where CPM_CompanyID=" & ixCompanyID & " And CPM_CurrencyID=" & ixOCurrency & " And CPM_PaymentMethod=" & ixPaymentMethod & _
			"And CCR_TerminalNumber='" & ixTerminalNumber & "'"
	'Response.Write(sSQL) : Response.End
	set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then
		If ixTypeCredit = 0 Then
			GetTransactionFeesEx = rsFees("CCR_RefundFixedFee")
			RatioChargeParam = 0
		Else
			GetTransactionFeesEx = rsFees("CCR_FixedFee")
			RatioChargeParam = FormatNumber(ixAmount * (rsFees("CCR_PercentFee") / 100), 4, True)
		End if
	Else 
		RatioChargeParam = 0 : GetTransactionFeesEx = 0
	End if
	rsFees.Close
End Function

Function GetUsageFeesEx(ixCompanyID, ixPaymentMethod, ixOCurrency, binsCountry, ixTerminalNumber, bGetSource) '1=Faill, 2=Approval
	Dim rsFees
	GetUsageFeesEx = 0
	sSQL = "Select CCR_ApproveFixedFee, CCR_FailFixedFee From tblCompanyCreditRestrictions Left Join tblCompanyPaymentMethods ON(tblCompanyCreditRestrictions.CCR_CPM_ID = tblCompanyPaymentMethods.CPM_ID) " & _
			"Where CPM_CompanyID=" & ixCompanyID & " And CPM_CurrencyID=" & ixOCurrency & " And CPM_PaymentMethod=" & ixPaymentMethod & _
			" And CCR_TerminalNumber='" & ixTerminalNumber & "'"
	Set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then 
		if bGetSource = 1 Then
			GetUsageFeesEx = rsFees("CCR_FailFixedFee")
		Elseif bGetSource = 2 Then
			GetUsageFeesEx = rsFees("CCR_ApproveFixedFee")
		End if
	End if	
	rsFees.Close
End Function

Function GetChbFeesEx(ixCompanyID, ixPaymentMethod, ixOCurrency, ixBin, ixTerminalNumber, refChb, retClrf)
	Dim rsFees
	GetChbFeesEx = False
	sSQL = "Select CCR_ClarificationFee, CCR_CBFixedFee From tblCompanyCreditRestrictions Left Join tblCompanyPaymentMethods ON(tblCompanyCreditRestrictions.CCR_CPM_ID = tblCompanyPaymentMethods.CPM_ID) " & _
			"Where CPM_CompanyID=" & ixCompanyID & " And CPM_CurrencyID=" & ixOCurrency & " And CPM_PaymentMethod=" & ixPaymentMethod & _
			"And CCR_TerminalNumber='" & ixTerminalNumber & "'"
	set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then 
		GetChbFeesEx = True
		retClrf = rsFees("CCR_ClarificationFee")
		refChb = rsFees("CCR_CBFixedFee")
	End if
	rsFees.Close
End Function

%>