<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/CalcTransPayment.asp"-->
<%Server.ScriptTimeout = 600%>
<html>
<head>
	<title><%= COMPANY_NAME_1%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/MainIE.css">
	<style type="text/css">.grandTotal { font-size:12px; font-weight:bold; }</style>
	<script type="text/javascript">
		function showSection(SecNum) {
			if(trSec) { // obj exist
			if(trSec.length) { // means if(obj.length > 0)
				for (i = 0; i < trSec.length; i++) {
					if(i==SecNum) { trSec[SecNum].style.display=''; }
					else { trSec[i].style.display='none'; }
				}
				<% If request("IsIFrame")<>"1" then %>window.resizeTo(760, (SecNum==2?672:372));<% end if %>
			}
			else { // means not an array (!obj.length)
				trSec.style.display='';
			}}
		}
	</script>
</head>
<%
Dim sWhere, nCompanyID, nPayID, nCurrency, nCompanyName, nAffiliateID, AffiliateName
sTransIdToShow = TestNumVar(Request("transID"), 0, -1, 0)
nCompanyID = TestNumVar(Request("CompanyID"), 0, -1, Session("CompanyID"))
nAffiliateID = TestNumVar(Request("AffiliateID"), 0, -1, Session("AffiliateID"))
nAFP_ID = TestNumVar(Request("AFP_ID"), 0, -1, 0)
nPayID = TestNumVar(Request("PayID"), 0, -1, 0)
nCurrency = TestNumVar(Request("Currency"), 0, MAX_CURRENCY, 0)
nLng = TestNumVar(Request("LNG"), 0, 1, 0)

If trim(Request("isFilter"))="1" Then
	dDateTo = dbText(request("toDate"))
	dDateFrom = dbText(request("fromDate"))
	If IsDate(dDateFrom) then sWhere = sWhere & " And tblCompanyTransPass.InsertDate>='" & DateValue(dDateFrom) & " 00:00:00'"
	If IsDate(dDateTo) then sWhere = sWhere & " And tblCompanyTransPass.InsertDate<='" & DateValue(dDateTo) & " 23:59:59'"
	If Request("TransID") <> "" Then sWhere = sWhere & " And tblCompanyTransPass.ID IN(" & Request("TransID") & ")"
	If Request("PaymentMethod") <> "" And Request("PaymentMethod") <> "1, 2, 3, 15, 20, 21, 22, 23, 24, 25, 26, 28, 29" Then _
		sWhere = sWhere & " And PaymentMethod IN(" & Request("PaymentMethod") & ")"
	If Request("CreditType") <> "" And Request("CreditType") <> "1, 2, 6, 8, 0" Then _
		sWhere = sWhere & " And CreditType IN(" & Request("CreditType") & ")"
	If Request("PaymentMethod_ID") <> "" Then sWhere = sWhere & " And PaymentMethod_id IN(" & Request("PaymentMethod_ID") & ")"
	If Request("TypeTerminal") <> "" Then sWhere = sWhere & " And (tblCompanyTransPass.TerminalNumber='" & Request("TypeTerminal") & "' OR tblCompanyTransPass.TerminalNumber='')"
	If Request("DeniedStatus") <> "" Then sWhere = sWhere & " And DeniedStatus " & IIF(Request("DeniedStatus") = "Y", ">0", "IN(" & Request("DeniedStatus") & ")")
	if sWhere <> "" Then sWhere = Right(sWhere, Len(sWhere) - 4)
ElseIf nPayID = 0 Then
	sWhere = "IsTestOnly=0"
End if

If nAFP_ID = 0 And nPayID <> 0 And nAffiliateID <> 0 Then _
    nAFP_ID = ExecScalar("Select AFP_ID From tblAffiliatePayments Where AFP_Affiliate_ID=" & nAffiliateID & " And AFP_TransPaymentID=" & nPayID, 0)

Set AmountSum = CalcCompanyPaymentTotal(nPayID, nCompanyID, nCurrency, sWhere)
Set AffSum = CalcAffiliateFees(nAFP_ID, AmountSum, nAffiliateID)
If Request("CreateAffPay") = "1" Then 
    AffSum.UpdatePayment()
    If nAFP_ID = 0 Then 
	    nReceiveCurrency = ExecScalar("Select dbo.GetAffiliatePayoutCurrency(" & nAffiliateID & ", " & nCurrency & ")", nCurrency)
	    If nReceiveCurrency = "" Then nReceiveCurrency = nCurrency
	    nExchangeRate = Round("" & ConvertCurrencyRateWFee(nCurrency, nReceiveCurrency), 5)
	    sSQL="INSERT INTO tblWireMoney (AffiliateID, WireSourceTbl_id, WireInsertDate, WireType, WireCurrency, WireExchangeRate, wireCompanyName," & _
	    " wireCompanyLegalName, wireIDnumber, wireCompanyLegalNumber, wirePaymentMethod, wirePaymentPayeeName, wirePaymentBank, wirePaymentBranch," & _
	    " wirePaymentAccount, wirePaymentAbroadAccountName, wirePaymentAbroadAccountNumber, wirePaymentAbroadBankName," & _
	    " wirePaymentAbroadBankAddress, wirePaymentAbroadBankAddressSecond, wirePaymentAbroadBankAddressCity, wirePaymentAbroadBankAddressState," &_
	    " wirePaymentAbroadBankAddressZip, wirePaymentAbroadBankAddressCountry, wirePaymentAbroadSwiftNumber, wirePaymentAbroadIBAN," & _
	    " wirePaymentAbroadABA, wirePaymentAbroadSortCode, wirePaymentAbroadAccountName2, wirePaymentAbroadAccountNumber2," & _
	    " wirePaymentAbroadBankName2, wirePaymentAbroadBankAddress2, wirePaymentAbroadBankAddressSecond2, wirePaymentAbroadBankAddressCity2," &_
	    " wirePaymentAbroadBankAddressState2, wirePaymentAbroadBankAddressZip2, wirePaymentAbroadBankAddressCountry2," & _
	    " wirePaymentAbroadSwiftNumber2, wirePaymentAbroadIBAN2, wirePaymentAbroadABA2, wirePaymentAbroadSortCode2)" & _
	    " SELECT " & nAffiliateID & ", " & AffSum.AFP_ID & ", '" & dPaymentDate & "', 3, " & nCurrency & "," & nExchangeRate & ", name," & _
	    " LegalName, IDnumber, LegalNumber, PaymentMethod, PaymentPayeeName, PaymentBank, PaymentBranch, PaymentAccount," & _
	    " aba_AccountName, aba_AccountNumber, aba_BankName, aba_BankAddress, aba_BankAddressSecond, aba_BankAddressCity, aba_BankAddressState," &_
	    " aba_BankAddressZip, aba_BankAddressCountry, aba_SwiftNumber, aba_IBAN, aba_ABA, aba_SortCode, aba_AccountName2, aba_AccountNumber2," &_
	    " aba_BankName2, aba_BankAddress2, aba_BankAddressSecond2, aba_BankAddressCity2, aba_BankAddressState2, aba_BankAddressZip2," & _
	    " aba_BankAddressCountry2, aba_SwiftNumber2, aba_IBAN2, aba_ABA2, aba_SortCode2" & _
	    " FROM tblAffiliates INNER JOIN GetAffiliateBankAccount(" & nAffiliateID & ", " & nReceiveCurrency & ") ON affiliates_id=aba_Affiliate"
		'Response.Write(sSQL)
	    oledbData.Execute sSQL
    End If
    oledbData.execute "UPDATE tblWireMoney SET WireAmount=" & AffSum.Total & "-wireFee WHERE WireType=3 AND WireSourceTbl_id=" & AffSum.AFP_ID
    If Request("Redirect") <> "" Then Response.Redirect Request("Redirect")
End If    

AffiliateName = ExecScalar("Select name From tblAffiliates Where affiliates_id=" & nAffiliateID, "") 
nCurrency = AmountSum.mCurrency
%>
<body leftmargin="6" topmargin="6" rightmargin="6" bottommargin="6" class="itext" dir="<%=IIF(nLng = 0, "rtl", "ltr") %>">
	<%
	If request("isIframe")="0" then
		%>
		<table width="100%" style="border:1px solid gray;" border="0" cellspacing="2" cellpadding="1" align="center">
		<tr>
			<td align="left" bgcolor="#e9e9e9" class="txt15b">
				&nbsp;<%= AffiliateName %> - <%= dbtextShow(AmountSum.mCompanyName) %> - <%= GetCurText(AmountSum.mCurrency) %><br />
			</TD>
		</tr>
		</table>
		<br />	
		<%
	End If
	%>
	<div align="center" id="waitMsg" style="text-decoration:blink">
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="txt12" style="color:gray;" align="center">
				please wait<br />
				calculating amounts<br />
			</td>
		</tr>
		<tr><td height="4"></td></tr>
		<tr>
			<td><img src="../images/loading_animation_orange2.gif" style="border:1px solid silver;" alt="" width="123" height="6" border="0"><br /></td>
		</tr>
		</table>
	</div>

	<%
	If (Not AmountSum Is Nothing) And (Not AffSum Is Nothing) Then
		%>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td id="trSec" style="display:block;">
				<table width="100%" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td class="txt11b" width="14%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Payment Method<br /></td>
					<td class="txt11b" width="6%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Count<br /></td>
					<td class="txt11b" width="10%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Amount<br /></td>
					<td class="txt11b" width="10%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Refunds<br /></td>
					<td class="txt11b" width="10%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">CHB's<br /></td>
					<td class="txt11b" width="10%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">SubTotal<br /></td>
					<td class="txt11b" width="10%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Credit Amount<br /></td>
					<td class="txt11b" width="30%" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Fee<br /></td>
				</tr>
				<tr><td height="3"></td></tr>
				<%
				Dim Notes, NoteIndex : NoteIndex = 1
				Set iRs = oledbData.Execute("Select Name, PaymentMethod_id From List.PaymentMethod Where PaymentMethod_id >= " & PMD_CC_MIN & " And PaymentMethod_id <= " & PMD_EC_MAX)
				Do While Not iRs.EOF
				    If AffSum.HasPaymentMethodAmount(iRs("PaymentMethod_id")) Then
				        Set AffTotal = AffSum.GetPaymentMethodAmount(iRs("PaymentMethod_id"))
    				    Set lineTotal = AmountSum.PaymentMethodTotal(iRs("PaymentMethod_id"), iRs("PaymentMethod_id"))
    				    Dim xSubTotal : xSubTotal = lineTotal.NormAmount + lineTotal.RefAmount - lineTotal.DenAmount
    			    %>	
				        <tr>
					        <td class="txt11" valign="top"><%=iRs("Name")%></td>
					        <td class="txt11" valign="top"><%=lineTotal.TotalCount%></td>
					        <td class="txt11" valign="top"><%=FormatCurr(nCurrency, lineTotal.NormAmount)%></td>
					        <td class="txt11" valign="top"><%=FormatCurr(nCurrency, lineTotal.RefAmount)%></td>
					        <td class="txt11" valign="top"><%=FormatCurr(nCurrency, lineTotal.DenAmount)%></td>
					        <td class="txt11" valign="top"><%=FormatCurr(nCurrency, xSubTotal)%></td>
					        <td class="txt11" valign="top"><%=FormatCurr(nCurrency, AffTotal.CreditAmount)%></td>
					        <td class="txt11" valign="top">
					            <%
					                If Ubound(AffTotal.Items) > 0 Then
					                    Dim i
					                    For i = 0 To Ubound(AffTotal.Items)
				                            If AffTotal.Items(i).CreditInfo <> "" Then
				                                If NoteIndex = 1 Then Notes = "<br/>Notes:<br/>"
				                                Notes = Notes & NoteIndex & " - " & AffTotal.Items(i).CreditInfo & "<br/>"
				                                NoteIndex = NoteIndex + 1
				                            End If    
					                        Response.Write(FormatCurr(nCurrency, AffTotal.Items(i).Amount) & " X " & FormatNumber(AffTotal.Items(i).FeePercent, 2, True) & "%" & IIF(AffTotal.Items(i).CreditInfo <> "", "<sup>(" & (NoteIndex - 1) & ")</sup>", "") & " = " & FormatCurr(nCurrency, AffTotal.Items(i).Total) & "<br/>")
					                    Next
					                    Response.Write("<div style=""border-top:solid 1px #C0C0C0;margin-top:2px;"">Total Fee: " & FormatCurr(nCurrency, AffTotal.Total) & "</div>")
					                Else 
					                    Response.Write("Total Fee: " & FormatCurr(nCurrency, AffTotal.Total))
					                End If
					            %>
					        </td>
				        </tr>
		                <tr><td height="1" colspan="8" style="border-bottom:1px dashed silver"><img src="../images/1_space.gif" width="1" height="1" border="0"><br /></td></tr>
    			    <% 
    			    End If
    			    iRs.MoveNext
    			Loop 
    			iRs.Close()
    			%>
				<tr><td height="1" bgcolor="gray" colspan="8" class="txt11"></td></tr>
    			<tr><td class="txt11B" colspan="7">Sub Total :</td><td class="txt11B"><%= FormatCurr(nCurrency, AffSum.SubTotal) %></td></tr>
				<tr><td height="1" bgcolor="gray" colspan="8" class="txt11"></td></tr>
    			<tr><td class="txt11B" colspan="8"><br />Other services :</td></tr>
				<tr><td height="1" bgcolor="gray" colspan="8" class="txt11"></td></tr>
				<%
				   For i = 0 To UBound(AffSum.PaymentLines)
				     %>
				     <tr>
				      <td class="txt11" colspan="5"><%=AffSum.PaymentLines(i).Text%></td>
				      <td class="txt11" colspan="2"><%=AffSum.PaymentLines(i).Quantity & " X " & FormatCurr(nCurrency, AffSum.PaymentLines(i).Amount) %></td>
				      <td class="txt11"><%=FormatCurr(nCurrency, AffSum.PaymentLines(i).Total) %></td>
				     </tr>
				     <%
				   Next
				%>
				<tr><td height="1" bgcolor="gray" colspan="8" class="txt11"></td></tr>
				<tr>
			        <td class="txt11" valign="top" colspan="7">Total Pay To Affiliate : </td>
			        <td class="txt11b"><%= FormatCurr(nCurrency, AffSum.Total) %></td>
				</tr>
				<tr><td colspan="8" class="txt10"><%=Notes%></td></tr>
				</table>
			</td>
		</tr>
		</table>
		<br />
		<%
	Else
		%>
		<table border="0" cellspacing="2" cellpadding="2" align="center">
		<tr>
			<td align="right" class="txt12">
				No records found<br />
			</td>
		</tr>
		</table>
		<%
	End if
	%>
	<script language="JavaScript1.2" type="text/javascript">document.getElementById('waitMsg').style.display='none';</script>
	<%
call closeConnection()
Server.ScriptTimeout = 90
%>
</body>
</html>
<script language="javascript" defer="defer">
	focus();
</script>