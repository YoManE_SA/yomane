<%
sWhere = ""
'Not admin need to check company owns this invoice
If Session("CompanyID") > 0 Then
	sWhere = "companyID=" & Session("CompanyID") & " AND "
End if

sSQL="SELECT id AS payID, invoiceType, IsChargeVAT, VATamount, currency, exchangeRate, BillingCurrencyShow, BillingLanguageShow, BillingCreateDate," &_
"BillingNumber, BillingCompanyName, BillingCompanyAddress, BillingCompanyNumber, BillingCompanyEmail, BillingToInfo " &_
"FROM tblTransactionPay WHERE " & sWhere & " BillingCompanys_id=" & nBillingCompanysID & " AND BillingNumber=" & nBillingNumber & " AND invoiceType=" & nInvoiceType
set rsDataPaydate = oledbData.execute(sSQL)
If NOT rsDataPaydate.EOF Then

	sBillingCreateDate=""
	if day(rsDataPaydate("BillingCreateDate"))<10 then sBillingCreateDate=sBillingCreateDate & "0" end if
	sBillingCreateDate=sBillingCreateDate & day(rsDataPaydate("BillingCreateDate")) & "/"
	if month(rsDataPaydate("BillingCreateDate"))<10 then sBillingCreateDate=sBillingCreateDate & "0" end if
	sBillingCreateDate=sBillingCreateDate & month(rsDataPaydate("BillingCreateDate")) & "/" & right(year(rsDataPaydate("BillingCreateDate")),2)

	nBillingNumber = trim(rsDataPaydate("BillingNumber"))
	sIsChargeVAT = rsDataPaydate("IsChargeVAT")
	sVATamount = rsDataPaydate("VATamount")
	If NOT sIsChargeVAT then
		sVATamount = 0
	End if
	
	CR_Symbol = GetCurText(rsDataPaydate("BillingCurrencyShow"))

	If trim(lCase(rsDataPaydate("BillingLanguageShow")))="heb" Then
		sInvoiceTypeText = sInvoiceTypeTextHeb
		sBillTypeText = sBillTypeTextHeb
		
		sTextDispaly_1 = "�����"
		sTextDispaly_2 = "�����"
		sTextDispaly_3 = "�����"
		sTextDispaly_4 = "����"
		sTextDispaly_5 = "��&quot;� ����"
		sTextDispaly_6 = "��&quot;� ��&quot;�"
		sTextDispaly_7 = "��&quot;� ���� ���� ��&quot;�"
		
		sStyleTable = "direction:rtl;"
		sStyleAlign = "text-align:right;"
		sStyleAlignReverse = "text-align:left;"
	Elseif trim(lCase(rsDataPaydate("BillingLanguageShow")))="eng" Then
		sInvoiceTypeText = sInvoiceTypeTextEng
		sBillTypeText = sBillTypeTextEng
		
		sTextDispaly_1 = "To"
		sTextDispaly_2 = "Date"
		sTextDispaly_3 = "Description"
		sTextDispaly_4 = "Amount"
		sTextDispaly_5 = "Total Amount"
		sTextDispaly_6 = "VAT"
		sTextDispaly_7 = "Total Amount Inc VAT"
		
		sStyleTable = "direction:ltr;"
		sStyleAlign = "text-align:left;"
		sStyleAlignReverse = "text-align:right;"
	End if
	%>
	<br />
	<table align="center" border="0" cellpadding="1" cellspacing="2" width="620" style="<%= sStyleTable %>">
	<tr>
		<td valign="bottom" nowrap style="<%= sStyleAlign %>">
			<span class="txt14b" dir="rtl"><%= dbtextShow(rsDataPaydate("BillingCompanyName")) %></span><br />
			<span class="txt12" dir="rtl"><%= dbtextShow(rsDataPaydate("BillingCompanyAddress")) %></span><br />
		</td>
		<td valign="bottom" nowrap style="<%= sStyleAlignReverse %>">
			<span class="txt14b"><%= dbtextShow(rsDataPaydate("BillingCompanyNumber")) %></span><br />
			<span class="txt12"><%= dbtextShow(rsDataPaydate("BillingCompanyEmail")) %></span><br />
		</td>
	</tr>
	<tr><td colspan="2"><hr width="100%" size="1" style="border:1px dashed black" noshade></td></tr>
	<tr>
		<td colspan="2" height="10"></td>
	</tr>
	<tr>
		<td colspan="2" align="center" nowrap class="txt15b">
			<%= sInvoiceTypeText  & " " & nBillingNumber & sBillTypeText %><br />
		</td>
	</tr>
	<tr>
		<td colspan="2" height="16"></td>
	</tr>
	<tr>
		<td colspan="2" align="right">
			<table align="right" width="100%" border="0" cellpadding="0" cellspacing="0" style="<%= sStyleTable %>">
				<tr>
					<td valign="top" class="txt14" style="<%= sStyleAlign %>">
						<span class="txt14b"><%= sTextDispaly_1 %></span><br />
						<%= dbtextShow(rsDataPaydate("BillingToInfo")) %>
					</td>
					<td width="25"></td>
					<td valign="top" class="txt14" style="<%= sStyleAlignReverse %>">
						<span class="txt14b"><%= sTextDispaly_2 %></span><br />
						<span class="txt12"><%= sBillingCreateDate %></span><br />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<td colspan="2"><br /></td>
	</table>
	<br />
	<table align="center" border="0" cellpadding="1" cellspacing="2" width="620" style="<%= sStyleTable %>">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="1" align="left">
			<tr><td height="1" colspan="2" style="padding:0px; border-top:1px solid black"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td></tr>
			<tr>
				<td class="txt14b" style="padding-right:10px; <%= sStyleAlign %>"><%= sTextDispaly_3 %><br /></td>
				<td class="txt14b" style="<%= sStyleAlignReverse %>"><%= sTextDispaly_4 %><br /></td>
			</tr>
			<tr><td height="1" colspan="2" style="padding:0px; border-bottom:1px solid black"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td></tr>
			<tr><td height="3"></td></tr>
			<%
			sSQL="SELECT * FROM tblBillingHandCreate WHERE TransactionPayDateID=" & rsDataPaydate("payID")
			set rsData3=oledbData.execute(sSQL)
			nAmount=0
			for i=0 to 8
				if trim(rsData3("Description" & i))<>"" OR int(rsData3("Amount" & i))<>0 then
				nAmount=nAmount+rsData3("Amount" & i)
				
				If not i=0 then
					%>
					<tr><td height="1" colspan="2" style="border-bottom:1px dashed silver"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td></tr>
					<%
				End If
				%>
				<tr bgcolor="white">
					<td class="txt14" width="85%" dir="rtl" style="padding-right:10px; <%= sStyleAlign %>">
						<%= dbtextShow(rsData3("Description" & i)) %><br />
					</td>
					<td class="txt13" dir="ltr" align="right">
						<%
						If int(rsData3("Amount" & i))<>0 then 
							response.write CR_Symbol & FormatNumber(rsData3("Amount" & i),2,-1,0,-1) & "<br />"
						End If
						%>
					</td>
				</tr>
				<%
				end if
			next
			
			fExchangeRate = 1
			If int(rsDataPaydate("Currency"))<>0 AND int(rsDataPaydate("BillingCurrencyShow"))=0 Then
				If rsDataPaydate("exchangeRate")>0 Then fExchangeRate = rsDataPaydate("exchangeRate")
			End if
			%>
			</tr>
			<tr><td height="1" colspan="2" style="border-bottom:1px dashed silver"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td></tr>
			<tr><td><br /></td></tr>
			<tr><td height="1" colspan="2" style="border-bottom:1px solid black"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td></tr>
			<tr>
				<td style="<%= sStyleAlignReverse %>">
					<table border="0" cellspacing="0" cellpadding="0" width="160">
					<tr><td class="txt13b" style="<%= sStyleAlign %>"><%= sTextDispaly_5 %><br /></td></tr>
					</table>
				</td>
				<td class="txt13" dir="ltr" align="right">
					<%= CR_Symbol & FormatNumber(nAmount*fExchangeRate, 2, -1, 0, -1) %><br />
				</td>
			</tr>
			<tr>
				<td style="<%= sStyleAlignReverse %>">
					<table border="0" cellspacing="0" cellpadding="0" width="160">
					<tr><td class="txt13b" style="<%= sStyleAlign %>"><%= sTextDispaly_6 %> (<%= sVATamount*100 %>%)<br /></td></tr>
					</table>
				</td>
				<td class="txt13" dir="ltr" align="right">
					<%= CR_Symbol & FormatNumber((nAmount*sVATamount)*fExchangeRate, 2, -1, 0, -1) %><br />
				</td>
			</tr>
			<tr>
				<td style="<%= sStyleAlignReverse %>">
					<table border="0" cellspacing="0" cellpadding="0" width="160">
					<tr><td class="txt13b" style="<%= sStyleAlign %>"><%= sTextDispaly_7 %><br /></td></tr>
					</table>
				</td>
				<td class="txt13" dir="ltr" align="right">
					<%= CR_Symbol & FormatNumber((nAmount*(sVATamount+1))*fExchangeRate, 2, -1, 0, -1) %><br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
	<%
End If
%>