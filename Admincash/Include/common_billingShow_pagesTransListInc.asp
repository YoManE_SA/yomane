<%
sSQL="SELECT List.TransCreditType.Name AS CreditName, tblCompanyTransPass.* FROM tblCompanyTransPass " &_
"LEFT OUTER JOIN List.TransCreditType ON tblCompanyTransPass.CreditType = List.TransCreditType.TransCreditType_id " &_
"WHERE (tblCompanyTransPass.PayID LIKE ('%;" & trim(nPayID) & ";%')) AND tblCompanyTransPass.CompanyID=" & nCompanyID & " " &_
"ORDER BY tblCompanyTransPass.InsertDate DESC"
set rsData=oledbData.execute(sSQL)
If not rsData.EOF Then
	%>
	<table width="620" align="center" border="0" cellpadding="1" cellspacing="0" >
	<tr>
		<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;" valign="bottom">Date<br /></td>
		<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;" valign="bottom">Payment method<br /></td>
		<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;" valign="bottom">Amount<br /></td>
		<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;" valign="bottom">Credit Type<br /></td>
		<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;" valign="bottom">Installments<br /></td>
		<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;" valign="bottom">Fees<br />Transaction<br /></td>
		<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;" valign="bottom">Fees<br />Processing<br /></td>
	</tr>
	<tr><td height="3"></td></tr>
	<%
	nCount = 0
	Do until rsData.EOF
	
		nCount = nCount + 1
		sShowFeeTrans = false
		sShowDeniedMessege = false
		
		If rsData("PaymentMethod_id")=1 OR rsData("PaymentMethod_id")=2 then
		'Credit cards and eChecks
			if int(rsData("CreditType"))=8 then
				sSQL="SELECT * FROM tblCompanyTransInstallments WHERE PayID='" & nPayID & "' AND transAnsID=" & rsData("id") & " ORDER BY id"
				set rsDataPayments=oledbData.execute(sSQL)
				sIsFirstPayment=false
				if not rsDataPayments.EOF then
					if left(rsDataPayments("comment"),2)="1/" then sIsFirstPayment=true
				end if
				if rsData("deniedstatus")=DNS_SetFoundValid Or rsData("deniedstatus")=DNS_SetFoundValidRefunded then
					if sIsFirstPayment then
						sShowDeniedMessege=true
					end if
				elseif rsData("deniedstatus")=DNS_SetFoundUnValid then
					sShowDeniedMessege=true
				end if
			Else  ' �� ���� �� ��������
				if rsData("deniedstatus")=DNS_SetFoundValid Or rsData("deniedstatus")=DNS_SetFoundValidRefunded then
					sShowDeniedMessege=true
				elseif rsData("deniedstatus")=DNS_SetFoundUnValid then
					sShowDeniedMessege=true
				elseif rsData("deniedstatus")=DNS_DupFoundValid then
					sShowDeniedMessege=true
				elseif rsData("deniedstatus")=DNS_DupFoundUnValid then
					sShowDeniedMessege=true
				end if
			end if
		Elseif int(rsData("PaymentMethod_id")) = 5 then
		    'Fees transactions
			If int(rsData("TransSource_id"))=1 OR int(rsData("TransSource_id"))=2 then
				sShowFeeTrans = true
			End if
		End if
	
		If nCount<>1 then
		%>
		<tr><td height="1" colspan="7"  bgcolor="#ffffff"></td></tr>
		<tr><td height="1" colspan="7"  bgcolor="silver"></td></tr>
		<tr><td height="1" colspan="7"  bgcolor="#ffffff"></td></tr>
		<%
		End If
		IF Not sShowFeeTrans then
			%>
			<tr>
				<td class="txt11" nowrap><%= FormatDatesTimes(rsData("InsertDate"),1,0,0) %><br /></td>
				<td class="txt10"><%= rsData("PaymentMethodDisplay") %><br /></td>
				<td class="txt11" nowrap ><%=FormatCurrWCT(rsData("Currency"), rsData("Amount"), rsData("CreditType"))%><br /></td>
				<td class="txt11"><%= rsData("CreditName") %><br /></td>
				<td class="txt11"><%= rsData("Payments") %><br /></td>
				<td class="txt11" nowrap ><%=FormatCurr(rsData("Currency"), rsData("netpayFee_transactionCharge"))%><br /></td>
				<td class="txt11" nowrap ><%=FormatCurr(rsData("Currency"), rsData("NetpayFee_RatioCharge"))%><br /></td>
			</tr>
			<%
		Elseif sShowFeeTrans then
			%>
			<tr>
				<td class="txt11" nowrap><%= sDate %><br /></td>
				<td class="txt11" colspan="4"><%= dbtextShow(rsData("Comment")) %><br /></td>
				<td class="txt11" nowrap ><%=FormatCurr(rsData("Currency"), -rsData("Amount"))%><br /></td>
			</tr>
			<%
		End if
		if rsData("PaymentMethod_id")="4" then
			%>
			<tr>
				<td colspan="7">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td bgcolor="#ffffff" width="40"></td>
						<td class="txt11" style="padding-right:6px;"><%= dbtextShow("Note: " & rsData("Comment")) %></td>
					</tr>
					</table>
				</td>
			</tr>
			<%
		end if
		if sShowDeniedMessege then ' ���� ���� �����
			%>
			<tr>
				<td colspan="7">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="6"></td>
						<td class="txt11" style="padding-right:6px;">
							<%
							select case int(rsData("deniedstatus"))
								case DNS_SetFoundValid, DNS_SetFoundValidRefunded
									response.write "This charge was disputed. claim was found unjustified. handling and disputing fee will apply."
								case DNS_SetFoundUnValid
									response.write "This charge was disputed. claim was found justified. The payment will be cancelled. Handling fee will apply."
								case DNS_DupFoundValid
									response.write "This charge was already paid, then disputed. claim was found justified. payment will be Subtracted. handling fee will apply."
								case DNS_DupFoundUnValid
									response.write "This charge was already paid, then disputed. claim was found unjustified. The payment will be cancelled. handling and disputing fee will apply."
							end select
							%>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<%
		End If
		If rsData("CreditType")="8" AND rsData("deniedstatus")<>DNS_SetFoundUnValid then ' ���� ���� �������� ��� �� ����� �����
			if not rsDataPayments.EOF then
				do until rsDataPayments.EOF
					%>
					<tr>
						<td colspan="7">
							<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td bgcolor="#ffffff" width="40"></td>
								<td class="txt12" style="padding-right:10px;">
									Payment <span class="txt11"><%= rsDataPayments("comment") %></span>
								</td>
								<td width="20"></td>
								<td class="txt12">
									Amount <span class="txt11"><%=FormatCurr(rsData("Currency"), rsDataPayments("Amount"))%></span>
								</td>
							</tr>
							</table>
						</td>
					</tr>
					<%
				rsDataPayments.movenext
				loop
			end if
		End if
			
	rsData.movenext
	loop
	%>
	<tr><td height="1" colspan="7"  bgcolor="#ffffff"></td></tr>
	<tr><td height="1" colspan="7"  bgcolor="silver"></td></tr>
	<tr><td height="1" colspan="7"  bgcolor="#ffffff"></td></tr>
	</table>
	<br />
	<iframe marginheight="0" marginwidth="0" align="center" frameborder="0" width="100%" height="250" scrolling="No" src="../include/common_totalTransShow.asp?LNG=1&CompanyID=<%= nCompanyID %>&PayID=<%= nPayID %>&isIframe=1"></iframe><br />
	<%
End If
%>