<%
Private Function TestExpirationDate(dExpiresOn)
	If NOT IsDate(dExpiresOn) Then
		TestExpirationDate = false : Exit Function
	End If
	dim dMonth, dYear, dThisYear, dThisMonth
	dMonth     = month(dExpiresOn)
	dThisMonth = month(date)
	dYear      = year(dExpiresOn)
	dThisYear  = year(date)
	if dYear = dThisYear then
		 ' check month
		if dMonth >= dThisMonth then
			TestExpirationDate = true  : Exit Function
		else
			TestExpirationDate = false : Exit Function
		end if
	elseif dYear > dThisYear then
		TestExpirationDate = true  : Exit Function
	else
		TestExpirationDate = false : Exit Function
	end if
End Function
	
Function PutCombo (sName, sList, sSeparator)
	dim i
	sName=trim(sName)
	sList=trim(sList)
	if instr(Request.ServerVariables("HTTP_USER_AGENT"),"MSIE")>0 then
		bNetscape=false
	else
		bNetscape=true
	end if

	if bNetscape then
		sStyle="font-family: Globes;"
	else
		sStyle=""
	end if
	%>
		<select name="<%= sName %>" style="<%= sStyle %>">
	<%
	m=1
	for i=1 to Len(sList)
		if instr(sSeparator,mid(sList, i, 1))>0 then
			n=i-1
			sTmp=mid(sList,m,n-m+1)
			if trim(sTmp)=trim(Request(sName)) then
				sSelected=" selected"
			else
				sSelected=""
			end if
			sTemp=sTmp
			%>
				<option value="<%= sTemp %>"<%= sSelected %>><%= sTemp %>
			<%
			m=i+1
		end if
	next
	%>
		</select>
	<%
End Function

Function PutComboDefault (sName, sList, sSeparator, sDefault)
	sName=trim(sName)
	sList=trim(sList)
	%>
		<select name="<%= sName %>">
			<%
				m=1
				for i=1 to Len(sList)
					if instr(sSeparator,mid(sList, i, 1))>0 then
						n=i-1
						sTmp=mid(sList,m,n-m+1)
						if trim(sTmp)=trim(sDefault) then
							sSelected=" selected"
						else
							sSelected=""
						end if
						sTemp=sTmp
						%>
							<option value="<%= sTemp %>"<%= sSelected %>><%= sTemp %>
						<%
						m=i+1
					end if
				next
			%>
		</select>
	<%
End Function

Function PutNumberedCombo (sName, sList, sSeparator)
	k=0
	sName=trim(sName)
	sList=trim(sList)
	%>
		<select name="<%= sName %>">
	<%
	m=1
	for i=1 to Len(sList)
		if instr(sSeparator,mid(sList, i, 1))>0 then
			k=k+1
			n=i-1
			sTmp=mid(sList,m,n-m+1)
			if trim(k)=trim(Request(sName)) then
				sSelected=" selected"
			else
				sSelected=""
			end if
			%>
				<option value="<%= trim(k) %>"<%= sSelected %>><%= sTmp %>
			<%
			m=i+1
		end if
	next
	if not instr(sSeparator, right(sList,1))>0 then
		sTmp=mid(sList,m,Len(sList)-m+1)
		if trim(k+1)=trim(Request(sName)) then
			sSelected=" selected"
		else
			sSelected=""
		end if
		%>
			<option value="<%= trim(k+1) %>"<%= sSelected %>><%= sTmp %>
		<%
	end if
	%>
		</select>
	<%
End Function

Function PutRecordsetCombo(rsList,sName,sValueField,sTextField)
Dim sSelected
	%>
	<select name="<%= sName %>">
	<%
	rsList.MoveFirst
	do until rsList.EOF
		if trim(rsList(sValueField))=trim(Request(sName)) then
			sSelected=" selected"
		else
			sSelected=""
		end if
			%>
			<option value="<%= rsList(sValueField) %>"<%= sSelected %>><%= rsList(sTextField) %>
			<%
		rsList.MoveNext
	loop
	%>
	</select>
	<%
End Function


Function PutRecordsetComboNEW(rsList,sName,sValueField,sTextField,sValueSelected,sStyle,isShowEmptyOption)
Dim sSelected
	%>
	<select name="<%= sName %>" <%= sStyle %>>
	<%
	If isShowEmptyOption Then Response.Write("<option value=""""></option>")
	rsList.MoveFirst
	do until rsList.EOF
		if trim(rsList(sValueField))=trim(sValueSelected) then
			sSelected=" selected"
		else
			sSelected=""
		end if
			%>
			<option value="<%= rsList(sValueField) %>"<%= sSelected %>><%= rsList(sTextField) %>
			<%
		rsList.MoveNext
	loop
	%>
	</select>
	<%
End Function

Function PutRecordsetCombodefault(rsList,sName,sValueField,sTextField,sValueDefault,sTextDefault)
Dim sSelected, sFieldName
	if InStr(sName,"""")>0 then
		sFieldName=left(sName,InStr(sName,"""")-1)
	else
		sFieldName=sName
	end if
	%>
	<select name="<%= sName %>">
		<option value="<%= sValueDefault %>"><%= sTextDefault %>
		<%
		if not (rsList.Eof and rsList.BOF) then 
		rsList.MoveFirst
		do until rsList.EOF
			if trim(rsList(sValueField))=trim(Request(sFieldName)) then
				sSelected=" selected"
			else
				sSelected=""
			end if
				%>
				<option value="<%= rsList(sValueField) %>"<%= sSelected %>><%= rsList(sTextField) %>
				<%
			rsList.MoveNext
		loop
		end if
		%>
	</select>
	<%
End Function
%>
<% Function All_Days(C_Name,sSelect,sEmpty) %>
	<select name="<%= C_Name %>">
		<%
		if sEmpty="1" then response.write "<option value=>"
		for i=1 to 31
			if i<10 then i="0" & i
			%>
			<option <% If int(sSelect)=int(i) then %>selected<% End If %> value="<%= i %>"><%= i %>
			<%
		next
		%>
	</select>
<% End Function %>

<% Function All_Months(C_Name,sSelect) %>
	<select name="<%= C_Name %>">
		<%
		for i=1 to 12
			if i<10 then i="0" & i
			%>
			<option <% If int(sSelect)=int(i) then %>selected<% End If %> value="<%= i %>"><%= i %>
			<%
		next
		%>
	</select>
<% End Function %>

<% Function All_Years(C_Name,sSelect,C_Start,C_End) %>
	<select name="<%= C_Name %>">
		<%
		for i=int(C_Start) to int(C_End)
			%>
			<option <% If int(sSelect)=int(i) then %>selected<% End If %> value="<%= i %>"><%= i %>
			<%
		next
		%>
	</select>
<%
End Function


Function PutRecordsetCombodefaultNEW(rsList,sName,sValueField,sTextField,sValueDefault,sTextDefault,sValueSelected)
%>
<select name="<%= sName %>">
	<option value="<%= sValueDefault %>"><%= sTextDefault %>
	<%
	if not (rsList.Eof and rsList.BOF) then 
		rsList.MoveFirst
		do until rsList.EOF
			%>
			<option <% If trim(rsList(sValueField))=sValueSelected then %>selected<% End If %> value="<%= rsList(sValueField) %>"><%= rsList(sTextField) %>
			<%
		rsList.MoveNext
		loop
	end if
	%>
</select>
<%
End Function
%>