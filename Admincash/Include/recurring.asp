<%
	function GetRecurringText(sLang, nUnit, nLength, nChargeCount)
		dim sTemp : sTemp=LCase(sLang)
		if sTemp<>"fre" and sTemp<>"heb" and sTemp<>"spa" then sTemp="eng"
		GetRecurringText=ExecScalar("SELECT dbo.RecurringGetText('" & sTemp & "', " & nUnit & ", " & nLength & ", " & nChargeCount & ")", "")
	end function

	function GetRecurringNotice(sLang)
		dim sText : sText=""
		select case LCase(sLang)
			case "eng"
				sText=" each charge"
			case "fre"
				sText=" chaque charge"
			case "heb"
				sText=" �� �����"
			case "spa"
				sText=" cada cargo"
		end select
		GetRecurringNotice=sText
	end function

	function GetRecurringTransactionType(sLang)
		dim sText : sText=""
		select case LCase(sLang)
			case "eng"
				sText="Transaction Type"
			case "fre"
				sText="Type de transaction"
			case "heb"
				sText="��� ����"
			case "spa"
				sText="Tipo de transacci&#243;n"
		end select
		GetRecurringTransactionType=sText
	end function
%>