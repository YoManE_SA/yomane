<%
' Convert numers into a sentence

ss = "One,Two,Three,Four,Five,Six,Seven,Eight,Nine" 
ds = "Ten,Eleven,Twelve,Thirteen,Fourteen,Fifteen,Sixteen,Seventeen,Eighteen,Nineteen" 
ts = "Twenty,Thirty,Forty,Fifty,Sixty,Seventy,Eighty,Ninety" 
qs = ",Thousand,Million,Billion" 
 
Function nnn2words(iNum) 
    a = split(ss,",") 
    i = iNum mod 10 
    if i > 0 then s = a(i-1) 
    ii = int(iNum mod 100)\10 
    if ii = 1 then  
        s = split(ds,",")(i) 
    elseif ((ii>1) and (ii<10)) then 
        s = split(ts,",")(ii-2) & " " & s 
    end if 
    i = (iNum \ 100) mod 10 
    if i > 0 then s = a(i-1) & " Hundred " & s 
    nnn2words = s 
End Function 
 
Function num2words(iNum) 
    i = iNum 
    if i < 0 then 
		b = true
		i = i*-1
	End if
    if i = 0 then 
        s="Zero" 
    elseif i <= 2147483647 then 
        a = split(qs,",") 
        for j = 0 to 3 
            iii = i mod 1000 
            i = i \ 1000 
            if iii > 0 then s = nnn2words(iii) & _ 
                " " & a(j) & " " & s 
        next 
    else 
        s = "Out of range value" 
    end if 
    if b then s = "Negative " & s 
    num2words = trim(s & " Dollars") 
End Function
%>
