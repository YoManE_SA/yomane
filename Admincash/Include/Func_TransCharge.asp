<%

'---------------------------------------------------------------------------------------------------
'	Converts binary data to string
'---------------------------------------------------------------------------------------------------
Function bts(data)
	sOut = ""
	For i = 0 to UBound(data)
		sOut = sOut & chrw(ascw(chr(ascb(midb(data,i+1,1)))))
	Next
	bts=sOut
End Function

'---------------------------------------------------------------------------------------------------
'	���� ���� ����� ����� ������ ����� �� �� �������
'---------------------------------------------------------------------------------------------------
Public Function Fn_CleanCCNumber(CardNo)

	TempCardNum = trim(replace(CardNo," ",""))
	TempCardNumLen = len(TempCardNum)
	for i = 1 to TempCardNumLen
		if mid(TempCardNum, i, 1) > "0" then exit for
	next
	Fn_CleanCCNumber = right(TempCardNum, TempCardNumLen-i+1)
	
End Function

'---------------------------------------------------------------------------------------------------
'	Return Credit Card Type
'	X;X = type ; region
'---------------------------------------------------------------------------------------------------
Public Function Fn_ccTypeAndRegion(CardNo)
	
	Fn_ccTypeAndRegion = "0;0"
	CardNo = Replace(Replace(Replace(CardNo, " ", ""), "-", ""), "*", "")

	If IsNumeric(CardNo) Then
		If Len(CardNo)=8 Then
			Fn_ccTypeAndRegion = "1;0" 'Isracard local
		elseIf Len(CardNo)=9 Then
			Select Case CInt(Left(CardNo, 1))
				Case 7
					Fn_ccTypeAndRegion = "6;0" 'Direct local
				Case Else
					Fn_ccTypeAndRegion = "1;0" 'Isracard local
			End Select
		elseIf Len(CardNo)>9 Then
			Select Case CInt(Left(CardNo, 4))
				Case 5180, 5326, 5189, 5521, 5100, 5477, 5418
					Fn_ccTypeAndRegion = "5;0" 'Master Card local
				Case 4580, 4557
					Fn_ccTypeAndRegion = "2;0" 'Visa local
				Case 2014, 2149
					Fn_ccTypeAndRegion = "7;1" 'EnRoute abroad
				Case 2131, 1800
					Fn_ccTypeAndRegion = "7;1" 'JCB abroad
				Case 6011
					Fn_ccTypeAndRegion = "7;1" 'Discover abroad
				Case 3755
					Fn_ccTypeAndRegion = "4;0" 'American Express local
				Case Else
					Select Case CInt(Left(CardNo, 3))
						Case 300,301,302,303,304,305
							Fn_ccTypeAndRegion = "3;1" 'American Diners Club abroad
						Case Else
							Select Case CInt(Left(CardNo, 2))
								Case 34, 37
									Fn_ccTypeAndRegion = "4;1" 'American Express abroad
								Case 36
									Fn_ccTypeAndRegion = "3;1" 'Diners Club abroad
								Case 38 :
									Fn_ccTypeAndRegion = "7;1" 'Carte Blanche abroad
								Case 51,52,53,54,55,50,14,71
									Fn_ccTypeAndRegion = "5;1" 'Master Card abroad
								Case Else
									Select Case CInt(Left(CardNo, 1))
										Case 3
											Fn_ccTypeAndRegion = "7;1" 'JCB abroad
										Case 4
											Fn_ccTypeAndRegion = "2;1" 'Visa abroad
									End Select
							End Select
					End Select
			End Select
		else
			Fn_ccTypeAndRegion = "0;0" 'less then 8
		end if
	else
		Fn_ccTypeAndRegion = "0;0" 'not numeric
	end if
	
End Function

'---------------------------------------------------------------------------------------------------
'	Return credit card name in english
'---------------------------------------------------------------------------------------------------
Public Function Fn_ccNameEnglish(CCTypeID)
	Select Case CCTypeID
		Case 1
			Fn_ccNameEnglish = "Isracard"
		Case 2
			Fn_ccNameEnglish = "Visa"
		Case 3
			Fn_ccNameEnglish = "Diners"
		Case 4
			Fn_ccNameEnglish = "American Exp"
		Case 6
			Fn_ccNameEnglish = "Direct"
		Case 5
			Fn_ccNameEnglish = "Master Card"
		Case 7
			Fn_ccNameEnglish = "Other"
		Case Else
			Fn_ccNameEnglish = "Credit Card"
	End Select
End Function

Function SendTransResponse(tblName, lTrans_ID, sUrlSend, ordrID, refID)
	If tblName = "tblCompanyTransPass" Then
		ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & lTrans_ID & "," & PET_PendingSendNotify & ",'', 3)")
	ElseIf tblName = "tblCompanyTransFail" Then
		ExecSql("Insert Into EventPending(TransFail_id, EventPendingType_id, Parameters, TryCount)Values(" & lTrans_ID & "," & PET_PendingSendNotify & ",'', 3)")
	End If

	'set siRs = OleDbData.Execute("Select * From " & tblName & " Where ID=" & lTrans_ID)
	'If Not siRs.EOF Then
	'	rCode = siRs("replyCode")
	'	If tblName = "tblCompanyTransFail" And TestNumVar(rCode, 0, -1, 0) = 0 Then rCode = "F" & rCode
	'	queryString = "Reply=" & rCode & "&transID=" & siRs("ID") & "&Date=" & replace(siRs("InsertDate"), " ", "+")
	'	queryString = queryString & "&TypeCredit=" & siRs("CreditType") & "&Payments=" & siRs("Payments")
	'	queryString = queryString & "&Amount=" & siRs("Amount") & "&Currency=" & siRs("Currency")
	'	queryString = queryString & "&Order=" & ordrID & "&reference=" & refID
	'	On Error Resume Next
	'		Dim HttpReq2
	'		Set HttpReq2 = CreateObject("Microsoft.XmlHttp")
	'		HttpReq2.open "POST", sUrlSend, true
	'		HttpReq2.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	'		HttpReq2.send queryString
	'		Set HttpReq2 = Nothing
	'	On Error GoTo 0
	'End if
	'siRs.Close
End Function

Function GetTransRefCode()
	GetTransRefCode = ExecScalar("Select Max(ID) + 1 From tblCompanyTransPass", 1) & GetRandomNum(3)
End Function

Function GetDebitCLogin(xTerminal, xTermName, xAccountId, xAccountSubId, xAccountPassword, xIs3D)
	Dim rsLoginInfo
	if xIs3D Then xIs3D = "3D" Else xIs3D = ""
	Set rsLoginInfo = oledbData.execute("SELECT accountId" & xIs3D & ", accountSubId" & xIs3D & ", dbo.GetDecrypted256(accountPassword" & xIs3D & "256) accountPassword" & xIs3D & ", dc_isActive, dt_Name " & _
		" FROM tblDebitTerminals Left Join tblDebitCompany ON(tblDebitCompany.debitCompany_id = tblDebitTerminals.DebitCompany) WHERE terminalNumber = '" & xTerminal & "'")
	If NOT rsLoginInfo.EOF Then
	  If rsLoginInfo("dc_isActive") Then 
		GetDebitCLogin = 0
		xTermName = Trim(rsLoginInfo("dt_Name"))
		xAccountId = Trim(rsLoginInfo("accountId" & xIs3D))
		xAccountSubId = Trim(rsLoginInfo("accountSubId" & xIs3D))
		if rsLoginInfo("accountPassword" & xIs3D) <> "" Then _
			xAccountPassword = rsLoginInfo("accountPassword" & xIs3D)
	  Else
		GetDebitCLogin = 503
	  End if	  
	Else
	  GetDebitCLogin = 503
	End if
	rsLoginInfo.Close	
	if GetDebitCLogin <> 0 Then Call throwError(GetDebitCLogin)
End Function

Public Function GetCompanyTerminal(ixCompanyID, ixPaymentMethod, ixCurrency, binsCountry, transBin, creditType, RetTerminalNumber, RetCC_Options)
	Dim rsDataTerminal, CCF_TSelMode, CCF_ID, bValue
	GetCompanyTerminal = -1 : RetCC_Options = -2 : RetTerminalNumber = "" : CCF_ID = -1
    sSQL = "Select CCF_ID, CCF_ExchangeTo, CCF_TSelMode From tblCompanyCreditFees " & _
        " Where " & IIF(CInt(creditType) = 0, "", "CCF_IsDisabled=0 And") & " CCF_CompanyID=" & ixCompanyID & " And CCF_CurrencyID=" & ixCurrency & " And CCF_PaymentMethod=" & ixPaymentMethod & _
        " And (CCF_ListBINs LIKE '%," & binsCountry & ",%' Or CCF_ListBINs='') " & _
        " Order By CCF_IsDisabled Asc, CCF_ListBINs Desc, CCF_ID Asc"
    'Response.Write(sSQL) : Response.End
    Set rsDataTerminal = oledbData.execute(sSQL)
    If Not rsDataTerminal.EOF Then
	    CCF_ID = rsDataTerminal("CCF_ID")
	    RetCC_Options = rsDataTerminal("CCF_ExchangeTo")
	    CCF_TSelMode = rsDataTerminal("CCF_TSelMode")
	End If
	rsDataTerminal.Close
	If CCF_ID > -1 Then
		Select Case CCF_TSelMode
		Case 0 'ratio
		    sSQL = "Select Top 1 CCFT_ID, CCFT_Terminal From tblCompanyCreditFeesTerminals " & _
				" Where CCFT_Ratio > 0 And CCFT_CCF_ID=" & CCF_ID & _
				" Order By ((CCFT_UseCount * 1.0) / CCFT_Ratio) Asc, CCFT_Ratio Desc"
		    rsDataTerminal.Open sSQL, oleDbData, 0, 1
		    If Not rsDataTerminal.EOF Then
				RetTerminalNumber = rsDataTerminal("CCFT_Terminal")
				GetCompanyTerminal = rsDataTerminal("CCFT_ID")
		    End If
			rsDataTerminal.Close
		Case 1 'B/L list
		    sSQL = "Select CCFT_ID, CCFT_Terminal, CCFT_Ratio, dt_mcc From tblCompanyCreditFeesTerminals " & _
				" Left Join tblDebitTerminals ON(tblCompanyCreditFeesTerminals.CCFT_Terminal = tblDebitTerminals.terminalNumber) " & _ 
				" Where CCFT_Ratio > 0 And CCFT_CCF_ID=" & CCF_ID & _
				" Order By CCFT_Ratio Asc"
		    rsDataTerminal.Open sSQL, oleDbData, 0, 1
		    Do While Not rsDataTerminal.EOF	
				If (rsDataTerminal("dt_mcc") = "7995" And rsDataTerminal("CCFT_Ratio") = 1) Then
					bValue = (TestNumVar(ExecScalar("Select ID From tblWhiteListBIN Where BIN='" & transBin & "'", 0), 0, -1, 0) > 0)
				ElseIf (rsDataTerminal("dt_mcc") = "7995" And rsDataTerminal("CCFT_Ratio") = 2) Then
					bValue = (TestNumVar(ExecScalar("Select ID From tblBlackListBIN Where BIN='" & transBin & "'", 0), 0, -1, 0) = 0)
				ElseIf rsDataTerminal("dt_mcc") = "" Then
					bValue = True
				End if
				If bValue Then
					RetTerminalNumber = rsDataTerminal("CCFT_Terminal")
					GetCompanyTerminal = rsDataTerminal("CCFT_ID")
					Exit Do
				End if
			  rsDataTerminal.MoveNext
			Loop
			rsDataTerminal.Close
		Case 2 'Priority List
			bValue = TestNumVar(Session(X_TransINC), 0, -1, 0)
		    sSQL = "Select CCFT_ID, CCFT_Terminal, CCFT_Ratio, dt_mcc From tblCompanyCreditFeesTerminals " & _
				" Left Join tblDebitTerminals ON(tblCompanyCreditFeesTerminals.CCFT_Terminal = tblDebitTerminals.terminalNumber) " & _ 
				" Where CCFT_Ratio > 0 And CCFT_CCF_ID=" & CCF_ID & _
				" Order By CCFT_ID Asc"
		    rsDataTerminal.Open sSQL, oleDbData, 0, 1
		    Do While Not rsDataTerminal.EOF
				If bValue = 0 Then
					RetTerminalNumber = rsDataTerminal("CCFT_Terminal")
					GetCompanyTerminal = rsDataTerminal("CCFT_ID")
					rsDataTerminal.MoveNext
					Exit Do
				Else	
					bValue = bValue - 1
					rsDataTerminal.MoveNext
				End if
			Loop
			X_TerminalHasNext = Not rsDataTerminal.EOF
			rsDataTerminal.Close
		End Select
	End If
	If GetCompanyTerminal > -1 Then 
		oledbData.execute("Update tblCompanyCreditFeesTerminals Set CCFT_UseCount=CCFT_UseCount+1 Where CCFT_ID=" & GetCompanyTerminal)
	Else
		RetCC_Options = -2
	End If	
    Set rsDataTerminal = Nothing
End Function

Function GetUsageFees(ixCompanyID, PaymentMethod, ixOCurrency, binsCountry, bGetSource) '1=Faill, 2=Approval
	Dim rsFees
	GetUsageFees = 0
	sSQL = "SELECT CCF_ApproveFixedFee, CCF_FailFixedFee From tblCompanyCreditFees " &_
			"WHERE (CCF_CompanyID=" & ixCompanyID & ") AND (CCF_CurrencyID=" & ixOCurrency & ") AND (CCF_PaymentMethod=" & PaymentMethod & ") " &_
			"AND (CCF_ListBINs LIKE '%," & binsCountry & ",%' OR CCF_ListBINs='') ORDER BY CCF_IsDisabled Asc, CCF_ListBINs Desc"
	Set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then 
		if bGetSource = 1 Then
			GetUsageFees = rsFees("CCF_FailFixedFee")
		Elseif bGetSource = 2 Then
			GetUsageFees = rsFees("CCF_ApproveFixedFee")
		End if
	End if	
	rsFees.Close
End Function

Function GetChbFees(ixCompanyID, ixPaymentMethod, ixOCurrency, ixBin, refChb, retClrf)
	Dim rsFees
	GetChbFees = False
	sSQL = "Select CCF_ClarificationFee, CCF_CBFixedFee From tblCompanyCreditFees " &_
			"Where CCF_CompanyID=" & ixCompanyID & " And CCF_CurrencyID=" & ixOCurrency & " And CCF_PaymentMethod=" & ixPaymentMethod &_
			" And (CCF_ListBINs LIKE '%" & ixBin & "%' Or CCF_ListBINs='') Order By CCF_ListBINs Desc, CCF_IsDisabled Asc"
	set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then 
		GetChbFees = True
		retClrf = rsFees("CCF_ClarificationFee")
		refChb = rsFees("CCF_CBFixedFee")
	Else
	    retClrf = 0	
	    refChb = 0
	End if
	rsFees.Close
End Function

Function GetTransactionFees(RatioChargeParam, ixCompanyID, ixPaymentMethod, ixOCurrency, ixAmount, binsCountry, ixTypeCredit)
	Dim rsFees
	sSQL = "Select CCF_PercentFee, CCF_RefundFixedFee, CCF_FixedFee From tblCompanyCreditFees " & _
			"Where CCF_CompanyID=" & ixCompanyID & " And CCF_CurrencyID=" & ixOCurrency & " And CCF_PaymentMethod=" & ixPaymentMethod &_
			" And (CCF_ListBINs LIKE '%," & binsCountry & ",%' Or CCF_ListBINs='') Order By CCF_ListBINs Desc"
	'Response.Write(sSQL) : Response.End
	set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then
		if ixTypeCredit = 0 Then
			GetTransactionFees = rsFees("CCF_RefundFixedFee")
			RatioChargeParam = 0
		Else	
			GetTransactionFees = rsFees("CCF_FixedFee")
			RatioChargeParam = FormatNumber(ixAmount * (rsFees("CCF_PercentFee") / 100), 4, True)
		End if
	Else 
		RatioChargeParam = 0 : GetTransactionFees = 0
	End if
	rsFees.Close
End Function

Function GetTransactionDebitPayDate(nDate, DCF_PayTransDays, DCF_PayINDays)
	Dim i
	If DCF_PayTransDays = "" Or DCF_PayINDays = "" Then 
		GetTransactionDebitPayDate = nDate
		Exit Function
	End if	
	DCF_PayTransDays = Split(DCF_PayTransDays, ",")
	DCF_PayINDays = Split(DCF_PayINDays, ",")
	For i = Ubound(DCF_PayTransDays) To 0 Step -1
		If Day(nDate) >= CLng(DCF_PayTransDays(i)) Then 
			Exit For
		End if	
	Next
	If UBound(DCF_PayINDays) >= i Then
		GetTransactionDebitPayDate = DateSerial(Year(nDate), Month(nDate), DCF_PayINDays(i))
		If CLng(DCF_PayINDays(i)) <= Day(nDate) Then _
			GetTransactionDebitPayDate = DateAdd("m", 1, GetTransactionDebitPayDate)
	Else
		GetTransactionDebitPayDate = nDate
	End if	
End Function

Function GetTransactionDebitUsageFees(ixDebitCompanyID, sTerminal, ixPaymentMethod, ixCurrency, ixAmount, ixTypeCredit, outApprovalFee, outDeneyFee)
	Dim rsFees
	outChbFee = 0
	sSQL = "Select * From tblDebitCompanyFees" & _
			" Where DCF_DebitCompanyID=" & ixDebitCompanyID & " And DCF_CurrencyID IN(255, " & ixCurrency & ") And DCF_PaymentMethod IN(0, " & ixPaymentMethod & ")" & _
			" And DCF_TerminalNumber IN('" & sTerminal & "', '')" & _
			" Order By DCF_TerminalNumber Desc, DCF_CurrencyID Asc, DCF_PaymentMethod Desc"
	Set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then
    	outApprovalFee = rsFees("DCF_ApproveFixedFee")
    	outDeneyFee = rsFees("DCF_FailFixedFee")
	Else
    	outApprovalFee = 0
    	outDeneyFee = 0
	End If
	rsFees.Close()
End Function

Function GetTransactionDebitFees(ixDebitCompanyID, sTerminal, ixPaymentMethod, ixCurrency, ixAmount, ixTypeCredit, payDate, deniedSt, transRatioFeeCharge, outChbFee)
	Dim rsFees, nAmount, nMaxPrec
	outChbFee = 0
	sSQL = "Select * From tblDebitCompanyFees" & _
			" Where DCF_DebitCompanyID=" & ixDebitCompanyID & " And DCF_CurrencyID IN(255, " & ixCurrency & ") And DCF_PaymentMethod IN(0, " & ixPaymentMethod & ")" & _
			" And DCF_TerminalNumber IN('" & sTerminal & "', '')" & _
			" Order By DCF_TerminalNumber Desc, DCF_CurrencyID Asc, DCF_PaymentMethod Desc"
	'Response.Write(sSQL) : Response.End
	Set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then
		If ixTypeCredit = 0 Then
			GetTransactionDebitFees = rsFees("DCF_RefundFixedFee")
			RatioChargeParam = 0 : outChbFee = 0
		Else
			GetTransactionDebitFees = rsFees("DCF_FixedFee")
			If rsFees("DCF_FixedCurrency") <> 255 Then GetTransactionDebitFees = ConvertCurrency(rsFees("DCF_FixedCurrency"), ixCurrency, GetTransactionDebitFees)
			If Not IsNull(rsFees("DCF_MaxPrecFee")) Then
			    nMaxPrec = rsFees("DCF_MaxPrecFee")
			    If rsFees("DCF_MaxPrecFee") = 0 And transRatioFeeCharge <> 0 Then nMaxPrec = (transRatioFeeCharge / ixAmount) * 100
    			 GetTransactionDebitFees = Round(GetTransactionDebitFees + ((ixAmount * ( (rsFees("DCF_MinPrecFee") / 100) + ( (nMaxPrec - rsFees("DCF_MinPrecFee") ) / 100)  * (rsFees("DCF_PercentFee") / 100)))), 2)
    		ElseIf rsFees("DCF_MinPrecFee") <> 0 Then
    			 GetTransactionDebitFees = Round(GetTransactionDebitFees + ((ixAmount * (rsFees("DCF_MinPrecFee") / 100))) * (rsFees("DCF_PercentFee") / 100), 2)
    		Else
    			 GetTransactionDebitFees = Round(GetTransactionDebitFees + (ixAmount * (rsFees("DCF_PercentFee") / 100)), 2)
			End If
			If deniedSt > 0 Then
				outChbFee = rsFees("DCF_CBFixedFee")
				If rsFees("DCF_CHBCurrency") <> 255 Then outChbFee = ConvertCurrency(rsFees("DCF_CHBCurrency"), ixCurrency, outChbFee)
			End If
		End if
		payDate = GetTransactionDebitPayDate(Date(), rsFees("DCF_PayTransDays"), rsFees("DCF_PayINDays"))
	Else 
		RatioChargeParam = 0 : GetTransactionDebitFees = 0 : outChbFee = 0
	End if
	rsFees.Close
End Function

Function IsDayInRange(strDateRng, xDay)
	Dim xFromDay, xToDay
	xFromDay = TestNumVar(Mid(strDateRng, 1, 2), 0, 31, 0)
	xToDay = TestNumVar(Mid(strDateRng, 3, 2), 0, 31, 0)
	if (xFromDay <= xDay) And (xToDay >= xDay) Then	_
		IsDayInRange = TestNumVar(Mid(strDateRng, 5, 2), 0, -1, 0) _
	Else IsDayInRange = -1
End Function

Sub RecalcMerchantPO(nCompanyID, nPayID)
	Dim tRs, transRs, i, nDay
	Set tRs = oledbData.Execute("Select PayingDaysMargin, payingDates1, payingDates2, payingDates3 From tblCompany Where ID=" & nCompanyID)
	If Not tRs.EOF Then
		If tRs("PayingDaysMargin") > 0 Then 
			oledbData.Execute("Update tblCompanyTransPass Set MerchantPD=DateAdd(d, " & tRs("PayingDaysMargin") & ", InsertDate) " & _
				" Where CompanyID=" & nCompanyID & " And PrimaryPayedID=" & nPayID)
		Else
			Set transRs = oledbData.Execute("Select ID, InsertDate From tblCompanyTransPass Where CompanyID=" & nCompanyID & " And PrimaryPayedID=" & nPayID)
			Do While Not transRs.EOF
				nDay = Day(transRs("InsertDate"))
				For i = 1 To 3
					xRet = IsDayInRange(tRs("payingDates" & i), nDay)
					If xRet > -1 Then
						GetTransPayDate = DateSerial(Year(transRs("InsertDate")), Month(transRs("InsertDate")) + IIF(xRet < nDay, 1, 0), xRet)
						oledbData.Execute("Update tblCompanyTransPass Set MerchantPD='" & GetTransPayDate & "' Where ID=" & transRs("ID"))
						Exit For
					End If	
				Next
			 transRs.MoveNext
			Loop
			transRs.Close()
			OleDbData.Execute("Update tblCompanyTransInstallments Set MerchantPD=DateAdd(month, IsNull(InsID, 0), IsNull(tblCompanyTransPass.MerchantPD, 0))" & _
				" From tblCompanyTransInstallments Left Join tblCompanyTransPass ON(tblCompanyTransInstallments.transAnsID = tblCompanyTransPass.ID)" & _
				" Where tblCompanyTransPass.CompanyID=" & nCompanyID & " And PrimaryPayedID=" & nPayID)
		End If	
	End If
	tRs.Close
End Sub

Sub RecalcCompanyFees(nCompanyID, nPayID)
	Dim isGradedFees
    Server.ScriptTimeout = 1000
    oleDbData.CommandTimeOut = 360
	isGradedFees = TestNumVar(ExecScalar("SELECT CFF_Currency FROM tblCompany WHERE ID=" & nCompanyID, ""), 0, -1, -1) >= 0
	If isGradedFees Then ExecSql("Exec spUpdUnsettleTransMerchantGradedFee " & nCompanyID & ", 5")
	Set iRs = OleDbData.Execute("Select * From tblCompanyCreditFees Where CCF_CompanyID=" & nCompanyID & " And CCF_IsDisabled=0 Order By CCF_ListBINs Asc")
	Do While Not iRs.EOF
		xBinsList = iRs("CCF_ListBINs")
		if Left(xBinsList, 1) = "," Then xBinsList = Right(xBinsList, Len(xBinsList) - 1)
		if Right(xBinsList, 1) = "," Then xBinsList = Left(xBinsList, Len(xBinsList) - 1)
		if xBinsList <> "" Then xBinsList = " And BINCountry IN('" & Replace(xBinsList, ",", "','") & "')"
		sQuery = "Update tblCompanyTransPass Set "
		sFrom = " From tblCompanyTransPass Left Join tblCreditCard ON(tblCreditCard.ID=tblCompanyTransPass.CreditCardID) " & _
				" Left Join tblCheckDetails ON(tblCheckDetails.ID=tblCompanyTransPass.CheckDetailsID)"
		sDefWhere = sFrom & " Where tblCompanyTransPass.CompanyID=" & nCompanyID & " And (tblCompanyTransPass.PaymentMethod=" & iRs("CCF_PaymentMethod") & ")" & xBinsList

		oleDbData.Execute(sQuery & "netpayFee_chbCharge=" & iRs("CCF_CBFixedFee") & sDefWhere & " And OCurrency=" & iRs("CCF_CurrencyID") & " And PrimaryPayedID=" & nPayID & " And DeniedStatus IN(1, 2, 4, 6)")
		oleDbData.Execute(sQuery & "netpayFee_ClrfCharge=" & iRs("CCF_ClarificationFee") & sDefWhere & " And OCurrency=" & iRs("CCF_CurrencyID") & " And PrimaryPayedID=" & nPayID & " And DeniedStatus IN(1, 3, 5, 11)") 'IN(1, 2, 3, 4, 5, 6, 9)

		oleDbData.Execute(sQuery & "netpayFee_transactionCharge=" & iRs("CCF_FixedFee") & _
			IIF(isGradedFees, " ", ", netpayFee_ratioCharge= (Amount * " & (iRs("CCF_PercentFee") / 100) & ")") & sDefWhere & " And OCurrency=" & iRs("CCF_CurrencyID") & " And PrimaryPayedID=" & nPayID & " And DeniedStatus Not IN(6, 7)")
		oleDbData.Execute(sQuery & "netpayFee_transactionCharge=" & iRs("CCF_RefundFixedFee") & ", netpayFee_ratioCharge=0" & sDefWhere & " And OCurrency=" & iRs("CCF_CurrencyID") & " And PrimaryPayedID=" & nPayID & " And CreditType=0 And DeniedStatus=0")

		sQuery = "Update tblCompanyTransApproval Set "
		sDefWhere = Replace(sDefWhere, "tblCompanyTransPass", "tblCompanyTransApproval") & " And Currency=" & iRs("CCF_CurrencyID") & " And PayID=" & nPayID
		oleDbData.Execute(sQuery & "netpayFee_transactionCharge=" & iRs("CCF_ApproveFixedFee") & sDefWhere)

		sQuery = "Update tblCompanyTransFail Set "
		sDefWhere = Replace(sDefWhere, "tblCompanyTransApproval", "tblCompanyTransFail") & " And DebitCompanyID > 1"
		oleDbData.Execute(sQuery & "netpayFee_transactionCharge=" & iRs("CCF_FailFixedFee") & sDefWhere)
	 iRs.MoveNext
	Loop
	iRs.Close
    oleDbData.CommandTimeOut = 30
    Server.ScriptTimeout = 90
End Sub

'---------------------------------------------------------------------------------------------------
' ����� ���� ���� ���� ��� ��� �����
'---------------------------------------------------------------------------------------------------

'Public Function GetTerminalInfo(terminalNumber, outDebitCompanyID, outIsNPTermianl, outIsShvaMaster, outProcMethod, outMcc, outIsManAmount)
	
'End Function

'Public Function Fn_TerminalInfo(DebitCompanyOutParam, isNetpayTerminalOutParam, isShvaMasterTerminalOutParam, processingMethodOutParam, mccOutParam, isManipulateAmountOutParam, terminalNumber)
'	Dim rsDataTmp
'	sSQL = "SELECT DebitCompany, isNetpayTerminal, isShvaMasterTerminal, processingMethod, dt_mcc, dt_isManipulateAmount FROM tblDebitTerminals WHERE (terminalNumber = '" & trim(terminalNumber) & "')"
'	Set rsDataTmp = oledbData.execute(sSQL)
'	If NOT rsDataTmp.EOF Then
'		DebitCompanyOutParam = rsDataTmp("DebitCompany")
'		isNetpayTerminalOutParam = rsDataTmp("isNetpayTerminal")
'		isShvaMasterTerminalOutParam = rsDataTmp("isShvaMasterTerminal")
'		processingMethodOutParam = rsDataTmp("processingMethod")
'		mccOutParam = rsDataTmp("dt_mcc")
'		isManipulateAmountOutParam = rsDataTmp("dt_isManipulateAmount")
'	End if
'	rsDataTmp.close
'	Set rsDataTmp = Nothing
'End Function


'---------------------------------------------------------------------------------------------------
' ���� ��� ����� ����� ����� ��� ������ ����
'---------------------------------------------------------------------------------------------------
Public Function Fn_IsCreditCardTestOnly(sCardNumber)
	If sCardNumber = "4580000000000000" Or sCardNumber = "4387751111111111" Or sCardNumber = "5442987111111111" _
	Or sCardNumber = "4111111111111111" Or sCardNumber =  "371000911111111" Or sCardNumber =   "36005131111111" _
	Or sCardNumber = "3535111111111111" Or sCardNumber = "5326300000000000" Or sCardNumber = "5326140000000000" _
	Or sCardNumber = "4511742700707855" Or sCardNumber = "4056850111111111" Or sCardNumber = "5130113111111111" _
	Or sCardNumber = "91000000" Then
		Fn_IsCreditCardTestOnly = 1
	Else
		Fn_IsCreditCardTestOnly = 0
	End If
End Function


'---------------------------------------------------------------------------------------------------
' ���� ���� ������ ����� �������
'---------------------------------------------------------------------------------------------------
Public Function Fn_CheckCcStatus(CcStatus, CompanyID, TransID, CcID)

End Function


'---------------------------------------------------------------------------------------------------
' LOG - ����� ����� ���� ���� ����� ����
'---------------------------------------------------------------------------------------------------
Public Function Sub_LogSavingChargeData(fTransType, fMerchantNumber, fIpAddress)
	On Error Resume Next
		sPATH_TRANSLATED = Request.ServerVariables("PATH_TRANSLATED")
		sHTTP_REFERER = Request.ServerVariables("HTTP_REFERER")
		sREQUEST_METHOD = Request.ServerVariables("REQUEST_METHOD")
		
		For Each x In Request.QueryString
			If Trim(x)<>"" Then sRequestQueryString = sRequestQueryString & Trim(x) & "=" & Trim(Request.QueryString(x)) & "|"
		Next
		For Each x In Request.Form
			If Trim(x)<>"" Then sRequestForm = sRequestForm & x & "=" & Trim(Request.Form(x)) & "|"
		Next
		For Each x In Session.Contents
			If Trim(x)<>"" Then sSessionContents = sSessionContents & x & "=" & Trim(Session.Contents(x)) & "|"
		Next
		
		'Hide Data
		sRequestQueryString = HideUrlParam(sRequestQueryString, "CardNum", 7, 8)
		sRequestQueryString = HideUrlParam(sRequestQueryString, "CVV2", 1, 5)
		sRequestQueryString = HideUrlParam(sRequestQueryString, "CCard_num3", 1, 4)
		sRequestForm = HideUrlParam(sRequestForm, "CardNum", 7, 8)
		sRequestForm = HideUrlParam(sRequestForm, "CVV2", 1, 5)
		sRequestForm = HideUrlParam(sRequestForm, "CCard_num3", 1, 4)
        
		If Trim(sPATH_TRANSLATED) = "" Then sPATH_TRANSLATED = "null" _
		        Else sPATH_TRANSLATED = "'" & dbText(sPATH_TRANSLATED) & "'"
		If Trim(sHTTP_REFERER) = "" Then sHTTP_REFERER = "null" _
		        Else sHTTP_REFERER = "'" & dbText(sHTTP_REFERER) & "'"
		If Trim(sREQUEST_METHOD) = "" Then sREQUEST_METHOD = "null" _
		        Else sREQUEST_METHOD = "'" & dbText(sREQUEST_METHOD) & "'"
		If Trim(sRequestQueryString) = "" Then sRequestQueryString = "null" _
		        Else sRequestQueryString = "'" & dbText(sRequestQueryString) & "'"
		If Trim(sRequestForm) = "" Then sRequestForm = "null" _
		        Else sRequestForm = "'" & dbText(sRequestForm) & "'"
		If Trim(sSessionContents) = "" Then sSessionContents = "null" _
		        Else sSessionContents = "'" & dbText(sSessionContents) & "'"
		
		sSQL = "Set Nocount on INSERT INTO tblLogChargeAttempts " &_
		"(TransactionType_id, Lca_MerchantNumber, Lca_RequestForm, Lca_SessionContents, Lca_RemoteAddress, Lca_PathTranslate, Lca_QueryString, Lca_HttpReferer, Lca_RequestMethod) VALUES " &_
		"(" & fTransType & ", " & left(fMerchantNumber,7) & ", " & sRequestForm & ", " & sSessionContents & ",'" & dbText(fIpAddress) & "', " & sPATH_TRANSLATED & ", " & sRequestQueryString & ", " & sHTTP_REFERER & ", " & sREQUEST_METHOD & ") " &_
		"SELECT @@IDENTITY AS NewLogID set nocount off"
		'Response.Write(sSQL)
		'Response.End
		set rsDataTmp = oledbData.execute(sSQL)
		If NOT rsDataTmp.EOF Then
			Sub_LogSavingChargeData = rsDataTmp("NewLogID")
		Else
			Sub_LogSavingChargeData = 0
		End if
		rsDataTmp.close
		Set rsDataTmp = Nothing
	On Error GoTo 0
End Function

'---------------------------------------------------------------------------------------------------
' LOG - ����� ����� ���� ���� ����� ����
'---------------------------------------------------------------------------------------------------
Public Function Fn_UpdatingChargeDataLog(fLogSavingChargeDataID, fReply, fReplyDesc, fTransNum)
	On Error Resume Next
	    If Trim(fTransNum) = "" Then fTransNum = "null"
	    sSQL="UPDATE tblLogChargeAttempts SET Lca_TransNum=" & fTransNum & ",  Lca_ReplyCode='" & fReply & "', Lca_ReplyDesc='" & fReplyDesc & "', Lca_DateEnd=getDate() WHERE LogChargeAttempts_id=" & fLogSavingChargeDataID
	    oledbData.execute sSQL
	On Error GoTo 0
End Function

'---------------------------------------------------------------------------------------------------
' LOG - ����� ����� �� ���� ����� ����� ������
'---------------------------------------------------------------------------------------------------
Public Sub Sub_LogConnectionProblem(sCompanyName, DebitReturnCode, sTransactionTypeID, sAmount, sCurrency, sTerminalNumber, sCompanyID, sIpAddress, DebitReferenceCode, DebitCompany, HTTP_Error)
	sSQL="INSERT INTO tblLog_NoConnection (lnc_CompanyName, lnc_DebitReturnCode, lnc_TransactionTypeID, lnc_Amount, lnc_Currency, lnc_TerminalNumber, lnc_CompanyID, lnc_IpAddress, lnc_DebitReferenceCode, lnc_DebitCompany, lnc_TransactionFailID, lnc_HTTP_Error)" &_
	" VALUES('" & sCompanyName & "', '" & DebitReturnCode & "', " & int(sTransactionTypeID) & ", " & sAmount & ", " & int(sCurrency) & ", '" & sTerminalNumber & "', " & int(sCompanyID) & ", '" & sIpAddress & "', '" & DebitReferenceCode & "'," & DebitCompany & "," & nTransactionID & ",'" & Replace(HTTP_Error, "'", "''") & "')"
	oledbData.Execute sSQL
End Sub


Public function fn_IsValidValidCC(sCCNum)
	Dim i, cchr, nOut
	fn_IsValidValidCC = False
	if IsNull(sCCNum) Or IsEmpty(sCCNum) Then Exit Function
	If Len(sCCNum) < 1 Then Exit Function
	For i = 1 To Len(sCCNum)
		cchr = Mid(sCCNum, i, 1)
		if(cchr >= "0" And cchr <= "9") Then
			nOut = nOut & cchr
		Elseif cchr  = "-" Then
		Else
			Exit Function
		End if
	Next
	sCCNum = nOut
	fn_IsValidValidCC = True
End Function

'---------------------------------------------------------------------------------------------------
' checks the validity of a cc number using LUHN Mod 10 algorithm
' sCCNum: the card number to check
' bLogFailed: boolean, true will log failed cards to db
'---------------------------------------------------------------------------------------------------
public function fn_IsValidCCNum(sCCNum, bLogFailed, sCompanyID, sTransactionTypeID, sAmount, sCurrency)
	dim i
	dim total 
	dim TempMultiplier
	
	for i=1 to Len(sCCNum)
		if inStr("1234567890", mid(sCCNum, i, 1))<1 then
			fn_IsValidCCNum=false
			exit function
		end if
	next

	for i = Len(sCCNum) to 2 Step -2
		total = total + cInt(mid(sCCNum, i, 1))
		TempMultiplier = cStr((mid(sCCNum, i - 1, 1)) * 2)
		total = total + cInt(left(TempMultiplier, 1))
		If Len(TempMultiplier) > 1 then total = total + CInt(Right(TempMultiplier, 1))
	next
	
	If Len(sCCNum) Mod 2 = 1 then total = total + CInt(Left(sCCNum, 1))

	If total Mod 10 = 0 Then
		fn_IsValidCCNum = True
	else
		sCCardNum_Encrypt = ""
		sIp = trim(request.ServerVariables("REMOTE_ADDR"))
		fn_IsValidCCNum = False
	end If
end Function

'---------------------------------------------------------------------------------------------------
' Check system and company's credit card blocked list
'---------------------------------------------------------------------------------------------------
'Public Function fn_IsBlockCCNum(CompanyID,CCardNum,ccExpMM,ccExpYY)
'	fn_IsBlockCCNum = ExecScalar("IF EXISTS(SELECT 1 FROM tblFraudCcBlackList WHERE" &_
'	" fcbl_ccNumber256 IN (dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups('" & CCardNum & "')), dbo.GetEncrypted256(Replace('" & CCardNum & "', ' ', '')))" & _
'	" ) SELECT CAST(1 AS bit) ELSE SELECT CAST(0 AS bit)")
'End Function

'---------------------------------------------------------------------------------------------------
' Get country of cc from bin number
'---------------------------------------------------------------------------------------------------
Function CCNumToCountry(sCardNum, RetCCType, RetBin)
	Dim rsDataTmp, RetCCName
	If Len(sCardNum) = 8 Then 'Isracard local
		CCNumToCountry = "IL"
		RetCCType = PMD_CC_ISRACARD
	ElseIf Len(sCardNum) = 9 Then
		if Left(sCardNum, 1) = "7" Then 'Direct local
			CCNumToCountry = "IL"
			RetCCType = PMD_CC_DIRECT
		Else 'Isracard local
			CCNumToCountry = "IL"
			RetCCType = PMD_CC_ISRACARD
		End if
	End if
	if CCNumToCountry <> "" Then Exit Function
	sql = "Select TOP 1 isoCode, PaymentMethod, BIN From tblCreditCardBIN Where BIN=SubString('" & sCardNum & "', 1, BinLen) Order by BinLen Desc"
	Set rsDataTmp = oledbData.Execute(sql)
	If rsDataTmp.EOF Then	
		CCNumToCountry = "--" 
        RetCCType = PMD_CC_UNKNOWN
	Else 
		CCNumToCountry = rsDataTmp(0)
        RetCCType = rsDataTmp(1)
        RetBin = rsDataTmp(2)
	End if
	rsDataTmp.close
	set rsDataTmp = Nothing
End Function

'---------------------------------------------------------------------------------------------------
' Get country of IP - UDI 21-07-2010
'---------------------------------------------------------------------------------------------------
Function GetIPGeoCountry(sIP)
    Dim nIpInt, ipParts 
    ipParts = Split(sIP, ".")
    If UBound(ipParts) < 3 Then 
        GetIPGeoCountry = "IP"
        Exit Function
    End If    
    nIpInt = (CLng(ipParts(0)) * (256 ^ 3)) + (CLng(ipParts(1)) * (256 ^ 2))  + (CLng(ipParts(2)) * (256)) + CLng(ipParts(3))
    GetIPGeoCountry = ExecScalar("Select GI_IsoCode From tblGeoIP Where GI_Start <= " & nIpInt & " And GI_End >= " & nIpInt & " Order By GI_Diff Asc", "")
End Function
%>