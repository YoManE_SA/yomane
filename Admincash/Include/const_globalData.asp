<%
	Const GGROUP_GroupNames = 0

	Dim MAX_CURRENCY
	Const GC_NIS = 0
	Const GC_USD = 1
	Const GC_EUR = 2
	Const GC_GBP = 3
	Const GC_AUD = 4
	Const GC_CAD = 5
	Const GC_JPY = 6
	Const GC_NOK = 7
	Const GC_PLN = 8
	Const GC_MXN = 9
	Const GC_ZAR = 10
	Const GC_RUB = 11
	Const GC_TRY = 12
	Const GC_CHF = 13
	Const GC_INR = 14
	Const GC_DKK = 15
	Const GC_SEK = 16
	Const GC_CNY = 17
	Const GC_HUF = 18
    Const GC_NZD = 19
    Const GC_HKD = 20
    Const GC_KRW = 21
    Const GC_SGD = 22
    Const GC_THB = 23
    Const GC_BSD = 24
	MAX_CURRENCY = 24
    	
	Const GGROUP_BatchFileStatus = 50
	Const BFS_New  = 0
	Const BFS_Parse  = 1
	Const BFS_Fail  = 2
	Const BFS_Pending  = 3

	Const GGROUP_MerchantMakePayments = 52
	Const MMP_Merchant  = 0
	Const MMP_Self  = 1
	Const MMP_Salary  = 2
	Const MMP_Supplier  = 3
	Const MMP_NetpayCustomer  = 4
	Const MMP_Other  = 9

	Const GGROUP_MerchantBalanceSource = 53
	Const MBS_TransactionPay  = 1
	Const MBS_MerchantMakePayments  = 2
	Const MBS_Manual = 3
	Const MBS_Fee = 4

	Const GGROUP_MerchantBalanceStatus = 45
	Const CBS_Initial = "CPV"

	Const GGROUP_WireStatusFlag	= 48
	Const WRF_Waiting			= 0
	Const WRF_Approved		= 1
	Const WRF_Rejected		= 2
	Const WRF_Done				= 3
	Const WRF_PartiallyApproved	= 4

	Const GGROUP_WireStatus			= 40
	Const WRS_New					= 0
	Const WRS_Canceled				= 1
	Const WRS_Pending				= 2
	Const WRS_SentByMasav			= 3
	Const WRS_SentByPrint			= 4
	Const WRS_SetBalance			= 5
	Const WRS_SentByExcell			= 6

	Const GGROUP_DeniedStatus		= 41
	Const DNS_NoDenied				= 0
	Const DNS_UnSetInVar			= 1
	Const DNS_SetInVar				= 2
	Const DNS_SetFoundValid			= 3     'photocopy unsettled
	Const DNS_SetFoundUnValid		= 4
	Const DNS_DupFoundValid			= 5     'photocopy settled
	Const DNS_DupFoundUnValid		= 6
	Const DNS_ChbRefundRTrans		= 7		'should not included in the refund stat list
	Const DNS_ChbRefundCTrans		= 8		'should be included in the refund stat list as chb
	Const DNS_SetFoundValidRefunded = 9     'photocopy unsettled then refunded (instead of chargeback)
	Const DNS_WasWorkedOut			= 10    'the original transaction that charged back (duplicated)
	Const DNS_DupWasWorkedOut		= 11    'the original transaction that asked for photocopy (duplicated)
	Const DNS_DupWasWorkedOutRefunded = 12  'transaction clarified then refunded (instead of changedback)

	Const PMD_Unknown				= 00
	Const PMD_ManualFee				= 01
	Const PMD_Admin					= 02
	Const PMD_SystemFees			= 03
	Const PMD_RolRes				= 04
    Const PMD_BankFees              = 05
	Const PMD_MAXINTERNAL			= 14

	'Const PMD_EC_CHECK				= 15
	Const PMD_CC_MIN			    = 20
	Const PMD_CC_UNKNOWN			= 20
	Const PMD_CC_ISRACARD			= 21
	Const PMD_CC_VISA				= 22
	Const PMD_CC_DINERS				= 23
	Const PMD_CC_AMEX				= 24
	Const PMD_CC_MASTERCARD			= 25
	Const PMD_CC_DIRECT				= 26
	Const PMD_CC_OTHER				= 27    'NOT IN USE
	Const PMD_CC_MAESTRO			= 28
	Const PMD_CC_TOGGLECARD			= 29
	Const PMD_CC_JCB			    = 31
	Const PMD_CC_MAX			    = 99
	
	Const PMD_EC_MIN				= 100
	Const PMD_EC_CHECK				= 100
	Const PMD_EC_GIROPAY            = 101
	Const PMD_EC_DIRECTPAY24        = 102
	Const PMD_EC_PINELV             = 103
	Const PMD_EC_PAYSAFECARD        = 104
	Const PMD_EC_CASH_TICKET        = 105
	Const PMD_EC_PRZELEWY24         = 106
	Const PMD_EC_EPS                = 107
	Const PMD_EC_WALLIE             = 108
	Const PMD_EC_IDEAL              = 109
	Const PMD_EC_TELEINGRESO        = 110
	Const PMD_EC_MONEYBOOKERS       = 111
	Const PMD_EC_ELV                = 112
	Const PMD_EC_YELLOWPAY          = 113
	Const PMD_EC_MAX				= 150
	Const PMD_MAX				    = 150

    Const PMD_PP_MIN                = 5000
    Const PMD_PP_MAX                = 20000
	Const PaymentMethodMax          = 20000
	
	Const GGROUP_CREDITTYPE			= 43
	Const CDT_REFUND				= 0
	Const CDT_NORMAL				= 1
	Const CDT_CREDIT				= 6
	Const CDT_INTALLMENTS			= 8
	
	Const GGROUP_COMPANYSTATUS		= 44
	Const CMPS_ARCHIVED				= 0
	Const CMPS_NEW					= 1
	Const CMPS_BLOCKED				= 2
	Const CMPS_CLOSED				= 3
	Const CMPS_CPONLY				= 10
	Const CMPS_INTEGRATION			= 20
	Const CMPS_PROCESSING			= 30
	Const CMPS_MAXCLOSED			= 9  '*
	Const CMPS_MINOK				= 30 '*
	
	Const GGROUP_FAILSTATUSSOURCE	= 46
	Const FSC_ISSUER				= 0
	Const FSC_GATEWAY				= 1
	Const FSC_RISK					= 2
	
	Const eBL_Email					= 1
	Const eBL_FullName				= 2
	Const eBL_Phone					= 3
	Const eBL_PNumber				= 4
	Const eBL_BIN					= 5
	Const eBL_BINCountry			= 6
	
	Const AFC_Volume                = 0
	Const AFC_Fees                  = 1
	Const AFC_MercSale_TermBuy      = 2
	Const AFC_MercSale_AffiBuy      = 3
	'Const AFC_MercSale_TermBuySPercent = 4
	Const AFC_MercSale_AffiBuySPercent = 5
	
	
	'PendingEventTypes
	Const PET_Unknown						= 0
	Const PET_FeesTransaction				= 10
	Const PET_InfoEmailSendClient			= 100
	Const PET_InfoEmailSendMerchant			= 101
	Const PET_InfoEmailSendAffiliate		= 102
	Const PET_InfoEmailSendRiskMultipleCardsOnEmail = 103
	Const PET_CreateRecurringSeries			= 200
    Const PET_RecurringSendNotify           = 201
    Const PET_RefundRequestSendNotify       = 202
	Const PET_PendingSendNotify				= 203
	Const PET_AutoCapture					= 300
	Const PET_GenerateInstallments			= 400
	Const PET_GenerateOldInstallments		= 401
	Const PET_CreateInvoice					= 402
    Const PET_EmailPhotoCopy                = 2001
    Const PET_EmailChargeBack               = 2002

	'EventTypes
	Const ET_Authorize					= 0001
	Const ET_Capture					= 0002
    Const ET_Refund						= 0003
    Const ET_Chargeback					= 0004
    Const ET_Fee_Authorization			= 1000
    Const ET_Fee_Transaction			= 1001
    Const ET_Fee_Line					= 1002
    Const ET_Fee_Refund					= 1003
    Const ET_Fee_Clarification			= 1004
    Const ET_Fee_Chb					= 1005
    Const ET_Bank_payed					= 2000
    Const ET_Bank_DirectDeposit		    = 2001
    Const ET_BankFee_Authorization	    = 3000
    Const ET_BankFee_Transaction		= 3001
    Const ET_BankFee_Line				= 3002
    Const ET_BankFee_Refund				= 3003
    Const ET_BankFee_Clarification	    = 3004
    Const ET_BankFee_Chb				= 3005
    Const ET_AffiliateFee_Process		= 4000
    Const ET_AffiliateFee_Chargeback    = 4001
    'Const PET_Info_EmailSend = 5000

	'TransAmountType
	Const TAT_Authorize = 1
	Const TAT_Capture = 2
	Const TAT_Refund = 3
	Const TAT_Chargeback = 4
	Const TAT_InstallmentCapture = 6
	Const TAT_InstallmentItem = 7
	Const TAT_AuthorizationFee = 1000
	Const TAT_ProcessingFee = 1001
	Const TAT_TransactionFee  = 1002
	Const TAT_RefundFee = 1003
	Const TAT_ClarificationFee = 1004
	Const TAT_ChargebackFee = 1005
	Const TAT_AnnualFee = 1030
	Const TAT_MonthlyFee = 1031
	Const TAT_DeclineFee = 1300
	Const TAT_CCStorageFee = 1400
	Const TAT_StoredCCChargeFee = 1401
	Const TAT_BankPayed = 2000
	Const TAT_BankDirectDeposit = 2001
	Const TAT_BankFeeAuthorization = 3000
	Const TAT_BankFeeTransaction = 3001
	Const TAT_BankFeeLine = 3002
	Const TAT_BankFeeRefund = 3003
	Const TAT_BankFeeClarification = 3004
	Const TAT_BankFeeChb = 3005
	Const TAT_BankFeeDecline = 3006
	Const TAT_AffiliateFeeProcess = 4000
	Const TAT_AffiliateFeeChargeback = 4001
	Const TAT_RollingRelease = 5001
	Const TAT_RollingReserve = 5002
	Const TAT_SystemCharge = 6001
	Const TAT_AdminCharge = 6002
	Const TAT_HandlingFee = 6003
	Const TAT_DebitorDirectPayment = 7000
	Const TAT_DebitorDirectFees = 7001
	Const TAT_Tax = 8000
	'----------------------------------------------------------------------------------------------

	Public Function GetConstArrayEx(tmpRS, sGroup, sTextField, vLng, vTitle, sRetArr, sRetColor)
		tmpRS.Open "Select GD_ID, GD_Group, GD_" & sTextField & ", GD_Color From tblGlobalData Where ((GD_Group=" & sGroup & ") Or (GD_Group=" & GGROUP_GroupNames & " And GD_ID=" & sGroup & ")) And GD_LNG=" & vLng & " Order By GD_Group Asc, GD_ID Desc", oledbData, 0, 1
		If Not tmpRS.EOF Then
			If (tmpRS(1) = GGROUP_GroupNames) And (tmpRS(0) = sGroup) Then
				vTitle = tmpRS(2)
				tmpRS.MoveNext
			End if
			If Not tmpRS.EOF Then
				'If IsArray(sRetArr) Then GetConstArrayEx = UBound(sRetArr) Else GetConstArrayEx = -1
				'If GetConstArrayEx < tmpRS(1) Then
					If sRetArr <> VbNull Then Redim sRetArr(tmpRS(0))
					If sRetColor <> VbNull Then Redim sRetColor(tmpRS(0))
				'End if	
			End if
		End if
		GetConstArrayEx = 0
		Do While Not tmpRS.EOF
			GetConstArrayEx = GetConstArrayEx + 1
			If IsArray(sRetArr) Then sRetArr(tmpRS(0)) = tmpRS(2)
			If IsArray(sRetColor) Then sRetColor(tmpRS(0)) = tmpRS(3)
			tmpRS.MoveNext
		Loop
		tmpRS.Close
	End Function

	public function GetConstArray(tmpRS, sGroup, vLng, vTitle, sRetArr, sRetColor)
		GetConstArray = GetConstArrayEx(tmpRS, sGroup, "Text", vLng, vTitle, sRetArr, sRetColor)
	End Function

	public function GetDescConstArray(tmpRS, sGroup, vLng, vTitle, sRetArr, sRetColor)
		GetDescConstArray = GetConstArrayEx(tmpRS, sGroup, "Description", vLng, vTitle, sRetArr, sRetColor)
	End Function

	public Function DrawComboConst(tmpRS, sGroup, vLng, isShowDes, selectName, selectExtras, optionValueSelected, optionValueDefault, optionTextDefault)
		Set tmpRS = Server.CreateObject("adodb.recordset")
		sSQL = "SELECT GD_ID, GD_Group, GD_Text, GD_Description FROM tblGlobalData WHERE GD_Group=" & sGroup & " AND GD_LNG=" & vLng & " ORDER BY GD_Group Asc, GD_ID Asc"
		tmpRS.Open sSQL, oledbData, 0, 1
		If NOT tmpRS.EOF Then 
			response.write "<select id=""" & selectName & """ name=""" & selectName & """ " & selectExtras & ">"
			If optionValueDefault<>"false" AND optionTextDefault<>"false" Then response.write "<option value=""" & optionValueDefault & """>" & optionTextDefault & "</option>"
			Do until tmpRS.EOF
				sSelected=""
				If trim(tmpRs(0))=trim(optionValueSelected) Then sSelected=" selected"
				response.write "<option value=""" & tmpRs(0) & """"  & sSelected & ">"
				response.write tmpRs(2)
				If isShowDes Then response.write " - " & tmpRs(3)
				response.write "</option>"
			tmpRS.MoveNext
			Loop
			response.write "</select>"
		End if
		tmpRS.Close
	End Function
%>