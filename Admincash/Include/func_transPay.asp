<%
'----------------------------------------------------------------------
'���� ���� ������� ������� �������
'----------------------------------------------------------------------
function Fn_calcAmountNikPay(fAmount,fPercent,fVarName)
		nAmountNikPay = 0
		nAmountNikPay = fAmount*(fPercent/100)
		fVarName = fVarName+nAmountNikPay
end function


'----------------------------------------------------------------------
'���� ���� ������� ����� ���� �����
'----------------------------------------------------------------------
function calcAmountNikPayAfter(fPayID,fPayments,fAmount,fPercent,fVarName)
	if int(fPayID)>800 then
		nAmountNikPay = 0
		nAmountNikPay = fAmount*(fPercent/100)
		fVarName = fVarName+nAmountNikPay
	else
		testPercent = 0
		testPercent = (fPercent/12)*(fPayments/2)
		testPercentShow = "(" & fPercent & "/" & 12 & ")*(" & fPayments & "/" & 2 & ")"
		testdetact = 0
		testdetact = fAmount*(testPercent/100)
		testdetactShow = "(" & fAmount & "*" & formatnumber(testPercent,4,-1) & ")/" & 100
		fVarName = fVarName+testdetact
	end if
end function

'----------------------------------------------------------------------
'���� ���� ����� �����
'----------------------------------------------------------------------
function showCCNumber(ccID)
	sSQL="SELECT dbo.GetDecrypted256(CCard_number256) FROM tblCreditCard WHERE ID =" & ccID
	set rsDataCC = oledbData.execute(sSQL)
	if rsDataCC.EOF then showCCNumber = "" _
	Else showCCNumber = rsDataCC("CCard_number")
end function

'----------------------------------------------------------------------
'���� ��� ���� ����
'----------------------------------------------------------------------
Function Fn_chkWasRefund(transID)
	sSQL="SELECT id FROM tblCompanyTransPass WHERE originalTransID =" & transID
	Set rsDataTmp = oledbData.execute(sSQL)
	If not rsDataTmp.EOF Then
		sBgColor="green_strip_sqr"
	Else
		sBgColor="#66cc66"
	End if
    Call rsDataTmp.Close
    Set rsDataTmp = Nothing
End function

'----------------------------------------------------------------------
'	������� ����
'----------------------------------------------------------------------
Function Fn_TransParam(RS, sTrBkgColor, sTdBkgColorFirst, sTdFontColor, sPayStatusText, bShowDeniedOptions, bAllowDelete)
	If not RS.EOF Then
		bAllowDelete = False
		bShowDeniedOptions = False
		sPayStatusText = ""
		sTrBkgColor = "#F6FEF6" 'LightGreen
		sTdBkgColorFirst = "#66cc66" 'Green
		sTdFontColor = "#000000"
		
		If RS("PaymentMethod") = PMD_SystemFees Then
			bAllowDelete = True
			sTrBkgColor="f5f5f5" 'LightGray
			sTdBkgColorFirst="#484848" 'DarkGray
		Elseif RS("PaymentMethod") = PMD_Admin Then
			bAllowDelete = True
			sTrBkgColor="#eeeffd" 'LightBlue
			sTdBkgColorFirst="#003399" 'Blue
		Elseif RS("PaymentMethod") = PMD_RolRes Then
			bAllowDelete = True
			sTrBkgColor="#f5f5f5" 'LightGray
			sTdBkgColorFirst="#399496" 'DarkGreen
		End if
		
		If RS("CreditType") = 0 then
			sTdFontColor="#cc0000" 'Red
		End if
		If RS("deniedstatus") = DNS_SetFoundValid Or RS("deniedstatus") = DNS_SetFoundValidRefunded Or RS("deniedstatus") = DNS_DupFoundValid Or RS("deniedstatus") = DNS_DupWasWorkedOut Then
			sTrBkgColor="#fff3dd" 'LightOrange
	        bShowDeniedOptions = False
		ElseIf RS("deniedstatus") > 0 Then
			sTrBkgColor="#ffebe1" 'LightOrange
	        If rsData("deniedstatus") = DNS_UnSetInVar OR rsData("deniedstatus") = DNS_SetInVar Then
		        bShowDeniedOptions = True
		    End if
		End if
		If RS("isTestOnly") Then
			bAllowDelete = True
			sTrBkgColor = "#ffffff"
			sTdFontColor = "#6b6b6b"
		End if
		
	    If trim(RS("payID")) = "" Then
		    sPayStatusText = ""
	    ElseIf Instr(trim(RS("payID")),";X;")>0 Then
		    sPayStatusText = "Archived"
	    ElseIf replace(trim(RS("payID")),";","")="0" Then
		    sPayStatusText = "Unsettled"
	    ElseIf Instr(trim(RS("payID")),";0;")=0 Then
			bAllowDelete = False
		    sPayStatusText = "Settled"
	    ElseIf Instr(trim(RS("payID")),";0;")>0 Then
			bAllowDelete = False
		    sPayStatusText = "Partial Settled"
	    End If
		    
	End if
End function
%>