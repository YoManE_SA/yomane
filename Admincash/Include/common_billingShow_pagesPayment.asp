<%
'Not admin, need to check if company owns this invoice
If Session("CompanyID") > 0 Then
	sWhere = "id_MerchantID=" & Session("CompanyID") & " AND "
else
	sWhere = ""
end if
sSQL = "SELECT * FROM tblInvoiceDocument WHERE " & sWhere & _
" id_BillingCompanyID=" & dbText(nBillingCompanysID) & " AND id_InvoiceNumber=" & dbText(nBillingNumber) & " AND id_Type=" & dbText(nInvoiceType)
set rsDataPaydate = oledbData.execute(sSQL)

nPayID = TestNumVar(rsDataPaydate("id_TransactionPayID"), 0, -1, 0)
nCurrency = TestNumVar(rsDataPaydate("id_Currency"), 0, -1, 0)
nCompanyID =  TestNumVar(rsDataPaydate("id_MerchantID"), 0, -1, 0)
nBillingCompanys_id =  TestNumVar(rsDataPaydate("id_BillingCompanyID"), 0, -1, 0)
nBillingNumber = trim(rsDataPaydate("id_InvoiceNumber"))

sInvoiceTypeTextHeb=ExecScalar("SELECT GD_Text FROM GetGlobalDataLang(60,0) where GD_ID=" & rsDataPaydate("id_Type"), "����")
sInvoiceTypeTextEng=ExecScalar("SELECT GD_Text FROM GetGlobalData(60) where GD_ID=" & rsDataPaydate("id_Type"), "Document")

If trim(request("PrintType")) = "billCopy" then
	sBillTypeTextHeb = " - ����"
	sBillTypeTextEng = " "
ElseIf trim(request("PrintType")) = "billOriginal" then
	If rsDataPaydate("isBillingPrintOriginal") then
		sBillTypeTextHeb = " - ����"
		sBillTypeTextEng = " - Copy"
	Else
		sBillTypeTextHeb = " - ����"
		sBillTypeTextEng = " - Origin"
	End If
End If

sIsChargeVAT = rsDataPaydate("id_ApplyVAT")
sVATamount = rsDataPaydate("id_VATPercent")
if NOT sIsChargeVAT then sVATamount = 0	
'nPrimePercent = rsDataPaydate("PrimePercent")

fExchangeRate = 1
'If rsDataPaydate("Currency") <> rsDataPaydate("BillingCurrencyShow") Then
'	If rsDataPaydate("exchangeRate") > 0 Then fExchangeRate = rsDataPaydate("exchangeRate")
'End if

'All sep 2006 invoices should not be calculated because of a mistake
'If int(nPayID)<= 4557 AND int(nPayID)>= 4439 Then fExchangeRate = 1
if trim(lCase(rsDataPaydate("id_BillingCompanyLanguage")))="eng" then
	sInvoiceTypeText = sInvoiceTypeTextEng
	sBillTypeText = sBillTypeTextEng
	sTextDispaly_1 = "To"
	sTextDispaly_2 = "Date"
	sHeadTextDispaly_1 = "Description"
	sHeadTextDispaly_2 = "Amount"
	sHeadTextDispaly_3 = "Qty"
	sHeadTextDispaly_4 = "Total"
	sFeeTextDispaly_1 = "Authorized transaction fee"
	sFeeTextDispaly_2 = "Refund transaction fee"
	sFeeTextDispaly_3 = "Processing fee"
	sFeeTextDispaly_4 = "Financing fees"
	sFeeTextDispaly_5 = "Transaction clarification fee - credit cards"
	sFeeTextDispaly_6 = "Transaction clarification fee - eCheck"
	sFeeTextDispaly_7 = "Transaction Chargeback fee - credit cards"
	sFeeTextDispaly_8 = "Transaction Chargeback fee - eCheck"
	sFeeTextDispaly_9 = "System transaction"
	sTextDispaly_5 = "Total Amount"
	sTextDispaly_6 = "VAT"
	sTextDispaly_7 = "Total Amount Inc VAT"
	sStyleTable = "direction:ltr;"
	sStyleAlign = "text-align:left;"
	sStyleAlignReverse = "text-align:right;"
else
	sInvoiceTypeText = sInvoiceTypeTextHeb
	sBillTypeText = sBillTypeTextHeb
	sTextDispaly_1 = "�����"
	sTextDispaly_2 = "�����"
	sHeadTextDispaly_1 = "�����"
	sHeadTextDispaly_2 = "����"
	sHeadTextDispaly_3 = "����"
	sHeadTextDispaly_4 = "��&quot;�"
	sFeeTextDispaly_1 = "���� ���� ����"
	sFeeTextDispaly_2 = "���� ���� �����"
	sFeeTextDispaly_3 = "���� �����"
	sFeeTextDispaly_4 = "���� ����� �����"
	sFeeTextDispaly_5 = "���� ����� ���� - ����� �����"
	sFeeTextDispaly_6 = "���� ����� ���� - �'� ��������"
	sFeeTextDispaly_7 = "���� ����� ���� ������ - ����� �����"
	sFeeTextDispaly_8 = "���� ����� ���� ������ - �'� ��������"
	sFeeTextDispaly_9 = "���� ���� �����"
	sTextDispaly_5 = "��&quot;� ����"
	sTextDispaly_6 = "��&quot;� ��&quot;�"
	sTextDispaly_7 = "��&quot;� ���� ���� ��&quot;�"
	sStyleTable = "direction:rtl;"
	sStyleAlign = "text-align:right;"
	sStyleAlignReverse = "text-align:left;"
end if

Function drawInvoiceLine(sText, sUnitAmount, sQuantity, sTotal)
	if sQuantity = 0 Then Exit Function
	drawInvoiceLine = "<tr bgcolor=""white"">" & _
		"<td class=""txt14"">" & sText & "<br /></td>" & _
		"<td class=""txt13"" dir=""ltr"" align=""right"" width=""18%"">" 
	If IsNumeric(sUnitAmount) Then drawInvoiceLine = drawInvoiceLine & FormatCurr(nCurrency, sUnitAmount * fExchangeRate) _
	Else drawInvoiceLine = drawInvoiceLine & sUnitAmount
	drawInvoiceLine = drawInvoiceLine & "<br /></td>" & _
		"<td class=""txt13"" dir=""ltr"" align=""right"" width=""12%"">" & sQuantity & "</td>" & _
		"<td class=""txt13"" dir=""ltr"" align=""right"" width=""18%"">" & FormatCurr(nCurrency, sTotal * fExchangeRate) & "<br /></td></tr>"
End Function
%>
<br />
<table align="center" border="0" cellpadding="1" cellspacing="2" width="620" style="<%= sStyleTable %>">
<tr>
	<td valign="bottom" nowrap style="<%= sStyleAlign %>">
		<span class="txt14b"><%= dbtextShow(rsDataPaydate("id_BillingCompanyName")) %></span><br />
		<span class="txt12"><%= dbtextShow(rsDataPaydate("id_BillingCompanyAddress")) %></span><br />
	</td>
	<td valign="bottom" nowrap style="<%= sStyleAlignReverse %>">
		<span class="txt14b"><%= dbtextShow(rsDataPaydate("id_BillingCompanyNumber")) %></span><br />
		<span class="txt12"><%= dbtextShow(rsDataPaydate("id_BillingCompanyEmail")) %></span><br />
	</td>
</tr>
<tr><td colspan="2"><hr width="100%" size="1" style="border:1px dashed black" noshade></td></tr>
<tr><td colspan="2" height="10"></td></tr>
<tr>
	<td colspan="2" align="center" nowrap class="txt15b" dir="rtl">
		<%= sInvoiceTypeText  & " " & nBillingNumber & sBillTypeText %><br /><%= sBillType %>
	</td>
</tr>
<tr>
	<td colspan="2" height="16"></td>
</tr>
<tr>
	<td colspan="2" align="right">
		<table align="right" width="100%" border="0" cellpadding="0" cellspacing="0" style="<%= sStyleTable %>">
			<tr>
				<td valign="top" class="txt14" style="<%= sStyleAlign %>">
					<span class="txt14b"><%= sTextDispaly_1 %></span><br />
					<%= dbtextShow(rsDataPaydate("id_BillToName")) %>
				</td>
				<td width="25"></td>
				<td valign="top" class="txt14" style="<%= sStyleAlignReverse %>">
					<span class="txt14b"><%= sTextDispaly_2 %></span><br />
					<span class="txt12"><%= FormatDatesTimes(rsDataPaydate("id_InsertDate"),1,0,0) %></span><br />
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr><td colspan="2"><br /></td></tr>
</table>
	<br />
	<table align="center" border="0" cellpadding="1" cellspacing="2" width="620" style="<%= sStyleTable %>">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="1" style="<%= sStyleTable %>">
			<tr><td height="1" colspan="4" style="padding:0px; border-top:1px solid black"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td></tr>
			<tr>
				<td class="txt14b" style="<%= sStyleAlign %>"><%= sHeadTextDispaly_1 %><br /></td>
				<td class="txt14b" align="right"><%= sHeadTextDispaly_2 %><br /></td>
				<td class="txt14b" align="right"><%= sHeadTextDispaly_3 %><br /></td>
				<td class="txt14b" align="right"><%= sHeadTextDispaly_4 %><br /></td>
			</tr>
			<tr><td height="1" colspan="4" style="padding:0px; border-top:1px solid black"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td></tr>
			<tr><td height="3"></td></tr>
			<%
			  sSQL="SELECT tblInvoiceLine.*, id_CurrencyRate FROM tblInvoiceLine INNER JOIN tblInvoiceDocument ON il_DocumentID=tblInvoiceDocument.ID WHERE tblInvoiceDocument.ID=" & rsDataPaydate("ID")
			  set rsData=oledbData.Execute(sSQL)
			  do until rsData.EOF
				  if nTransactionPay>0 then
						sAmount="---"
					else
						sAmount=rsData("il_Price")
					end if
					response.Write drawInvoiceLine(rsData("il_Text"), sAmount, rsData("il_Quantity"), rsData("il_Amount")) '*rsData("il_CurrencyRate")/rsData("id_CurrencyRate")
					rsData.MoveNext
			  loop
			  rsData.Close
			%>
			<tr>
				<td height="1" colspan="4" style="border-bottom:1px solid black"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td>
			</tr>
			<tr bgcolor="white">
				<td colspan="3" style="<%= sStyleAlignReverse %>">
					<table border="0" cellspacing="0" cellpadding="0" width="170">
					<tr><td class="txt13b" style="<%= sStyleAlign %>"><%= sTextDispaly_5 %><br /></td></tr>
					</table>
				</td>
				<td class="txt13" dir="ltr" align="right">
					<%= FormatCurr(nCurrency, rsDataPaydate("id_TotalLines")) %><br />
				</td>
			</tr>
			<tr bgcolor="white">
				<td colspan="3" style="<%= sStyleAlignReverse %>">
					<table border="0" cellspacing="0" cellpadding="0" width="170">
					<tr><td class="txt13b" style="<%= sStyleAlign %>"><%= sTextDispaly_6 %> (<%= sVATamount*100 %>%)<br /></td></tr>
					</table>
				</td>
				<td class="txt13" dir="ltr" align="right">
					<%= FormatCurr(nCurrency, rsDataPaydate("id_TotalVAT")) %><br />
				</td>
			</tr>
			<tr bgcolor="white">
				<td colspan="3" style="<%= sStyleAlignReverse %>">
					<table border="0" cellspacing="0" cellpadding="0" width="170">
					<tr><td class="txt13b" style="<%= sStyleAlign %>"><%= sTextDispaly_7 %><br /></td></tr>
					</table>
				</td>
				<td class="txt13" dir="ltr" align="right">
					<%= FormatCurr(nCurrency, rsDataPaydate("id_TotalDocument")) %><br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
	<%
%>