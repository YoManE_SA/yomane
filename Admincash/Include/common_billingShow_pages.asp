<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/common_sessionCheckCompany.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_controls.asp"-->
<!--#include file="../include/func_transPay.asp"-->
<!--#include file="../include/CalcTransPayment.asp"-->
<html>
<head>
	<title><%= COMPANY_NAME_1%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/MainIE.css">
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">
<%
nBillingCompanysID = dbText(request("BillingCompanysID"))
nBillingNumber = dbText(request("billingNumber"))
nInvoiceType = dbText(request("invoiceType"))
sType = dbText(request("type"))

sWhere = ""
'Not admin need to check company owns this invoice
If Session("CompanyID") > 0 Then sWhere = "id_MerchantID=" & Session("CompanyID") & " AND "

sSQL="SELECT id_MerchantID, IsNull(payDate, id_InsertDate) payDate, payType, id_Currency," & _
" tblTransactionPay.id AS transPayId, id_InsertDate, id_IsPrinted, id_Type FROM tblInvoiceDocument" & _
" LEFT JOIN tblTransactionPay ON tblInvoiceDocument.ID=tblTransactionPay.InvoiceDocumentID" & _
" WHERE " & sWhere & " id_BillingCompanyID=" & nBillingCompanysID & " AND id_InvoiceNumber=" & nBillingNumber & " AND id_Type=" & nInvoiceType
set rsData2=oledbData.execute(sSQL)
If rsData2.EOF Then
	response.write "No records found<br />"
	response.end
End if

nTransactionPay = rsData2("transPayId")
nCompanyID = trim(rsData2("id_MerchantID"))
nPayID = trim(rsData2("transPayId"))
sPayDate = trim(rsData2("payDate"))
nCurrency = trim(rsData2("id_Currency"))

sInvoiceTypeTextHeb=ExecScalar("SELECT GD_Text FROM GetGlobalDataLang(60,0) where GD_ID=" & rsData2("id_Type"), "����")
sInvoiceTypeTextEng=ExecScalar("SELECT GD_Text FROM GetGlobalData(60) where GD_ID=" & rsData2("id_Type"), "Document")

If NOT rsData2("id_IsPrinted") AND request("billOriginal")<>"" then
	sBillTypeTextHeb = " - ����"
	sBillTypeTextEng = " - Origin"
	If sType="preview" then
		%>
		<table align="center" border="0"  cellpadding="2" cellspacing="0" width="650">
		<tr>
			<td align="right" class="txt14" style="color:#00254a;">
				<li type="square"><%= sInvoiceTypeTextHeb & sBillTypeTextHeb %> &nbsp;&nbsp;&nbsp;&nbsp; Print copies: <span class="txt13" style="color:#00254a;">1</span></li><br />
			</td>
		</tr>
		</table>
		<table align="center" border="0" style="background-color:white; border:1px solid black;" cellpadding="1" cellspacing="2" width="650">
		<tr>
			<td style="padding-top:10px;">
				<div id="divbillOriginal">
					<!--#include file="../include/common_billingShow_pagesPayment.asp"-->
				</div>
			</td>
		</tr>
		</table>
		<br /><br />
		<%
	Else
		for pNumCopy=1 to 1
			 'Check to see if need to page break
			if trim(request("billCopy"))<>"" OR trim(request("chargeList"))<>"" OR int(pNumCopy)<>1 then
				sStyle="page-break-after:always;"
			else
				sStyle="page-break-after:auto;"
			end if
			%>
			<div id="divbillOriginal<%= pNumCopy %>" style="<%= sStyle %>">
				<script language="JavaScript">
					divbillOriginal<%= pNumCopy %>.innerHTML=top.fraBillprintPreview.divbillOriginal.innerHTML;
				</script>
			</div>
			<%
		next
	End If
End If

If trim(request("billCopy")) <> "" then
	sBillTypeTextHeb=" - ����"
	sBillTypeTextEng = " "
	If sType = "preview" then
		%>
		<table align="center" border="0" cellpadding="2" cellspacing="0" width="650">
		<tr>
			<td align="right" class="txt14" style="color:#00254a;">
				<li type="square"><%= sInvoiceTypeTextHeb & sBillTypeTextHeb %> &nbsp;&nbsp;&nbsp;&nbsp; Print Copies: <span class="txt13" style="color:#00254a;"><%= request("billCopyNum") %></span></li><br />
			</td>
		</tr>
		<tr><td height="3"></td></tr>
		</table>
		<table align="center" border="0" style="background-color:white; border:1px solid black;" cellpadding="1" cellspacing="2" width="650">
		<tr>
			<td style="padding-top:10px;">
				<div id="divBillCopy">
					<!--#include file="../include/common_billingShow_pagesPayment.asp"-->
				</div>
			</td>
		</tr>
		</table>
		<br /><br />
		<%
	Else
		for pNumCopy=1 to int(request("billCopyNum"))
			'Check to see if need to page break
			if trim(request("chargeList"))<>"" OR int(pNumCopy)<>int(request("billCopyNum")) then
				sStyle="page-break-after:always;"
			else
				sStyle="page-break-after:auto;"
			end if
			%>
			<div id="divBillCopy<%= pNumCopy %>" style="<%= sStyle %>">
				<script language="JavaScript">
					divBillCopy<%= pNumCopy %>.innerHTML=top.fraBillprintPreview.divBillCopy.innerHTML;
				</script>
			</div>			
			<%
		next
	End If
End If

If trim(request("chargeList"))<>"" then
	If sType="preview" then
		%>
		<table align="center" border="0" cellpadding="2" cellspacing="0" width="650">
		<tr>
			<td align="right" class="txt14" style="color:#00254a;">
				<li type="square">Transaction List &nbsp;&nbsp;&nbsp;&nbsp; Print copies: <span class="txt13" style="color:#00254a;"><%= request("chargeListNum") %></span></li><br />
			</td>
		</tr>
		</table>
		<table align="center" border="0" cellpadding="1" cellspacing="2" width="650">
		<tr>
			<td style="padding-top:10px;">
				<div id="divChargeList">
					<!--#include file="../include/common_billingShow_pagesTransList.asp"-->
				</div>
			</td>
		</tr>
		</table>
		<br /><br />
		<%
	Else
		for pNumCopy=1 to int(request("chargeListNum"))
			if int(pNumCopy)<>int(request("chargeListNum")) then ' check to see if need to page break
				sStyle="page-break-after:always;"
			else
				sStyle="page-break-after:auto;"
			end if
			%>
			<div id="divChargeList<%= pNumCopy %>" style="<%= sStyle %>">
				<script language="JavaScript">
					divChargeList<%= pNumCopy %>.innerHTML=top.fraBillprintPreview.divChargeList.innerHTML;
				</script>
			</div>			
			<%
		next
	End If
End If
%>
<script language="JavaScript">
	<%
	If sType="preview" then
		sQuery = "&BillingCompanysID=" & nBillingCompanysID
		sQuery = sQuery & "&billingNumber=" & nBillingNumber
		sQuery = sQuery & "&InvoiceType=" & nInvoiceType
		sQuery = sQuery & "&billOriginal=" & request("billOriginal")
		sQuery = sQuery & "&billCopy=" & request("billCopy")
		sQuery = sQuery & "&chargeList=" & request("chargeList")
		sQuery = sQuery & "&billCopyNum=" & request("billCopyNum")
		sQuery = sQuery & "&chargeListNum=" & request("chargeListNum")
		%>
		top.fraBillprint.location.href='../include/common_billingShow_pages.asp?type=print<%= sQuery %>';
		<%
	Else
		%>
		top.fraBillMenu.frmBilling.bPrint.style.cursor = 'hand';
		top.fraBillMenu.frmBilling.bPrint.disabled = false;
		<%
	End If
	%>
</script>
</body>
</html>