<!--#include file="../globalConst.asp"-->
<%
'----------------------------------------------------------------------------
'	Database connection
'----------------------------------------------------------------------------
'Opens Connection
Dim oledbData, DSNReq, sUrlDsn, GlobalSaveToFile : GlobalSaveToFile = False
Set oledbData = Server.CreateObject("ADODB.Connection")

If Not IS_PRODUCTION Then Application("DBConnect") = ""

If trim(Application("DBConnect"))="" Then
	Set DSNReq = Server.CreateObject("Msxml2.ServerXMLHTTP")
	if request.ServerVariables("HTTPS")="on" then
		sUrlDsn="https://"
	else
		sUrlDsn="http://"
	end if

	If Request.ServerVariables("LOCAL_ADDR")="192.168.5.11" Then
		sUrlDsn = sUrlDsn & request.ServerVariables("SERVER_NAME") & ":" & request.ServerVariables("SERVER_PORT")
	Else
		If session("TempOpenURL") = "" Then _
			session("TempOpenURL") = sUrlDsn & request.ServerVariables("SERVER_NAME") & ":" & request.ServerVariables("SERVER_PORT") + "/Admincash"
		sUrlDsn = Replace(session("TempOpenURL"), "http://", sUrlDsn)
	End If
	sUrlDsn = sUrlDsn & Left(request.ServerVariables("SCRIPT_NAME"), inStr(request.ServerVariables("SCRIPT_NAME"), "/")) & "dsn.aspx"
	DSNReq.Open "POST", sUrlDsn, false
	DSNReq.Send

	Application("DBConnect")=DSNReq.responseText
	set DSNReq = Nothing
End if
if trim(Application("DBConnect2"))="" then
	Set DSNReq = Server.CreateObject("Msxml2.ServerXMLHTTP")
	if request.ServerVariables("HTTPS")="on" then
		sUrlDsn="https://"
	else
		sUrlDsn="http://"
	end if

	If Request.ServerVariables("LOCAL_ADDR")="192.168.5.11" Then
		sUrlDsn=sUrlDsn & request.ServerVariables("SERVER_NAME") & ":" & request.ServerVariables("SERVER_PORT")
	Else
		sUrlDsn=Replace(session("TempOpenURL"), "http://", sUrlDsn)
	End If
	sUrlDsn=sUrlDsn & Left(request.ServerVariables("SCRIPT_NAME"), inStr(request.ServerVariables("SCRIPT_NAME"), "/")) & "dsn.aspx?DSN=2"

	DSNReq.Open "POST", sUrlDsn, false
	DSNReq.Send
	
	Application("DBConnect2")=DSNReq.responseText
	set DSNReq = Nothing
end if
Dim sCurrentConnectionString
sCurrentConnectionString="Provider=SQLOLEDB" & IIF(IS_PRODUCTION,".1","") & ";" & Application("DBConnect")

Dim NetpayCryptoObj : Set NetpayCryptoObj = Nothing

if trim(Session("CurrentDSN") & " ")="2" then
	Dim nMinDSN2, nSecDSN2
	nMinDSN2=Minute(Now()) mod HOTDB_INTERVAL_MINUTES
	nSecDSN2=Second(Now())
	if nMinDSN2*60+nSecDSN2>HOTDB_DURATION_SECONDS And nMinDSN2*60+nSecDSN2<(HOTDB_INTERVAL_MINUTES-1)*60+HOTDB_PREVENTION_SECONDS then
		sCurrentConnectionString="Provider=SQLOLEDB" & IIF(IS_PRODUCTION,".1","") & ";" & Application("DBConnect2")
		%>
			<script language="javascript" type="text/javascript" defer="defer">
				function ShowSQL2()
				{
					var spnHead = document.getElementById("pageMainHeading");
					if (spnHead)
					{
						if (spnHead.innerHTML.indexOf("SQL2") < 0) spnHead.innerHTML = "<div class=\"dsnHead\">SQL2</div>" + spnHead.innerHTML;
					}
					else
					{
						document.getElementById("divDSN2").style.display = "";
					}
				}
				setInterval("ShowSQL2();", 500)
			</script>
			
		<%
		Response.Write "<div id=""divDSN2"" class=""dsn"" style=""display:none;"">SQL2</div>"
	end if
	Session("CurrentDSN") = ""
end if

oledbData.Open sCurrentConnectionString

if Application("PendingUpdated")<>"1" and sCurrentConnectionString="Provider=SQLOLEDB" & IIF(IS_PRODUCTION,".1","") & ";" & Application("DBConnect") then
	oledbData.Execute "UPDATE tblCompanyTransPending SET Locked=0"
	Application("PendingUpdated")="1"
end if
'Close Connection
Function closeConnection()
	If lcase(TypeName(oledbData))="connection" Then
		oledbData.close
		Set oledbData = nothing
	End If
    If Not NetpayCryptoObj Is Nothing Then Set NetpayCryptoObj = Nothing
End Function

Function ExecScalar(sql, lDef)
	Dim iRs
	Set iRs = oledbData.Execute(sql)
	If iRs.EOF Then ExecScalar = lDef Else ExecScalar = iRs(0)
	iRs.Close
End Function

Function ExecSQL(sql)
	Dim nRecords
	If GlobalSaveToFile Then
		FileAppendData "ERROR_PASS.txt", sql & vbcrlf
		ExecSQL = -1
	Else 
		oledbData.Execute sql, nRecords
		ExecSQL = nRecords
	End If	
End Function

Function IIf(valTest, valTrue, valFalse)
	If valTest Then IIf = valTrue Else IIf = valFalse
End Function

Function URLDecode(sConvert)
	Dim aSplit, sOutput, I
	If IsNull(sConvert) Then
		URLDecode = ""
		Exit Function
	End If
	sOutput = REPLACE(sConvert, "+", " ")
	aSplit = Split(sOutput, "%")
	If UBound(aSplit) > 0 Then
		sOutput = aSplit(0)
		For I = 0 To UBound(aSplit) - 1
			sOutput = sOutput & _
				Chr("&H" & Left(aSplit(i + 1), 2)) &_
				Right(aSplit(i + 1), Len(aSplit(i + 1)) - 2)
		Next
	End If
	URLDecode = sOutput
End Function

'----------------------------------------------------------------------------
'	Data Validation
'----------------------------------------------------------------------------
Function TestNumVar(vrVal, vrMin, vrMax, vrDef)
	If vrVal <> "" And IsNumericDecimalString(vrVal, 0, 100) Then
		TestNumVar = cdbl(vrVal)
		If((vrMin < vrMax) And ((vrMin > TestNumVar) Or (vrMax < TestNumVar))) Then _
			TestNumVar = vrDef
	Else
		TestNumVar = vrDef
	End If
End Function

function TestNumOptional(vrVal)
	TestNumOptional=TestNumVar(vrVal, 1, 0, "")
end function

Function IsNumericDecimalString(xStr, nMinLen, nMaxLen)
	Dim xChar, i
	If IsNull(xStr) Then 
		IsNumericDecimalString = (nMinLen > 0)
		Exit Function
	End If
	IsNumericDecimalString = False
	If (nMinLen < nMaxLen) And ( (Len(xStr) < nMinLen) Or (Len(xStr) > nMaxLen) ) Then Exit Function
	For i = 1 To Len(xStr)
		xChar = Mid(xStr, i, 1)
		If ((Asc(xChar) < 48) Or (Asc(xChar) > 57)) And xChar <> "." And xChar <> "-" Then Exit Function
	Next
	IsNumericDecimalString = True
End Function

Function IsNumericString(xStr, nMinLen, nMaxLen)
	Dim xChar, i
	IsNumericString = False
	If (nMinLen < nMaxLen) And ( (Len(xStr) < nMinLen) Or (Len(xStr) > nMaxLen) ) Then Exit Function
	For i = 1 To Len(xStr)
		xChar = Mid(xStr, i, 1)
		If (Asc(xChar) < 48) Or (Asc(xChar) > 57) Then 
            Exit Function
        End If
	Next
	IsNumericString = True
End Function

Function TestStrVar(vrVal, nMinLen, nMaxLen, vrDef)
	If vrVal <> "" Then
		TestStrVar = vrVal
		If((nMinLen < nMaxLen) And ((nMinLen > len(vrVal)) Or (nMaxLen < len(vrVal)))) Then _
			TestStrVar = vrDef
	Else
		TestStrVar = vrDef
	End If
End Function

Function TestNumList(xStr)
	Dim xVars
	xVars = Split(xStr, ",")
	For i = 0 To UBound(xVars)
		xVal = TestNumVar(xVars(i), 0, -1, 0)
		if xVal <> 0 Then 
			If TestNumList <> "" Then TestNumList = TestNumList & ","
			TestNumList = TestNumList & xVal
		End if	
	Next
End Function
'
' 20080309 Tamir
'
' Function GetDomainSSL added
'
' This function returns the current domain with HTTP, if the address includes "192.", HTTPS otherwise.
'
function GetDomainSSL()
	if inStr(LCase(Session("TempOpenURL")), "192.168")>0 then
		GetDomainSSL = Session("TempOpenURL")
	else
		GetDomainSSL = Replace(LCase(Session("TempOpenURL")), "http:", "https:")
	end if
end function

'----------------------------------------------------------------------------
'	Manipulate URL
'----------------------------------------------------------------------------
Function UrlWithout(qString, strParam)
	Dim lIndex, eIndex, lChar : lIndex = 0 : lChar = ""
	UrlWithout = qString
	Do
		lIndex = InStr(lIndex + 1, qString, strParam & "=")
		If(lIndex < 1) Then Exit Function _
		Else If(lIndex > 1) Then lChar = Mid(qString, lIndex - 1, 1)
	Loop While lIndex > 1 And lChar <> "&" And lChar <> "?"
	eIndex = InStr(lIndex, UrlWithout, "&")
	If(eIndex < 1) Then eIndex = Len(UrlWithout)
	UrlWithout = Left(UrlWithout, lIndex - 1) & Right(UrlWithout, Len(UrlWithout) - eIndex)
End Function

Function GetURLValue(strUrl, strValue)
	Dim lIndex, eIndex, lChar : lIndex = 0 : lChar = ""
	Do
		lIndex = InStr(lIndex + 1, strUrl, strValue & "=")
		If(lIndex < 1) Then Exit Function _
		Else If(lIndex > 1) Then lChar = Mid(strUrl, lIndex - 1, 1)
	Loop While lIndex > 1 And lChar <> "&" And lChar <> "?"
	lIndex = lIndex + Len(strValue & "=")
	eIndex = InStr(lIndex, strUrl, "&")
	If(eIndex < 1) Then eIndex = Len(strUrl) + 1
	GetURLValue = Mid(strUrl, lIndex, eIndex - lIndex)
End Function

Function SetStringValue(str, nVal, start, slen)
	If Len(str) < start Then 
		SetStringValue = str
	Else
		If Len(str) < (start + slen) Then slen = Len(str) - start
			SetStringValue = Left(str, start - 1) & nVal & Right(str, (Len(str) - (start + slen)) + 1)
	End if	
End Function

Function HideUrlParam(strUrl, xParam, nStart, nLen)
	Dim lIndex, eIndex, i
	lIndex = InStr(1, strUrl, xParam & "=", 1)
	If(lIndex < 1) Then
		HideUrlParam = strUrl
		Exit Function
	End If
	lIndex = lIndex + Len(xParam & "=")
	eIndex = InStr(lIndex, strUrl, "|")
	If(eIndex < 1) Then eIndex = Len(strUrl) + 1
	HideUrlParam = Mid(strUrl, lIndex, eIndex - lIndex)
	If Len(HideUrlParam) < nLen Then nLen = Len(HideUrlParam)
	HideUrlParam = SetStringValue(HideUrlParam, String(nLen, "x"), nStart, nLen)
	HideUrlParam = SetStringValue(strUrl, HideUrlParam, lIndex, eIndex - lIndex)
End Function

'----------------------------------------------------------------------------
'	Password history and policy
'----------------------------------------------------------------------------
Function CheckPasswordPolicy(pss)
	Dim i, bNumFound, bCharFound
	CheckPasswordPolicy = False
	If Len(pss) < 8 Then Exit Function
	bNumFound = 0 : bCharFound = 0
	For i = 1 To Len(pss)
		 xChar = Mid(pss, i, 1)
	 If Asc(xChar) >= Asc("0") And Asc(xChar) <= Asc("9") Then bNumFound = bNumFound + 1 _
	 Else bCharFound = bCharFound + 1
	Next
	CheckPasswordPolicy = (bNumFound > 1) And (bCharFound > 1)
End Function

Function UpdatePasswordChange(refType, refID, sPassword)
	UpdatePasswordChange = ExecScalar("UpdatePasswordChange " & refType & ", " & refID & ", '" & Request.ServerVariables("REMOTE_ADDR") & "', '" & Replace(sPassword, "'", "''") & "'", 0)
End Function

Function IsPassword(refType, refID, sPassword)
	IsPassword = (ExecScalar("Select Count(*) From tblPasswordHistory Where LPH_ID = 1 And LPH_RefType=" & refType & " And LPH_RefID=" & refID & " And LPH_Password256=dbo.GetEncrypted256('" & Replace(sPassword, "'", "''") & "')", 0) > 0)
End Function

Sub DeletePassword(refType, refID)
	oleDbData.Execute "Delete From tblPasswordHistory Where LPH_RefType=" & refType & " And LPH_RefID=" & refID
End Sub

'----------------------------------------------------------------------------
'	Manipulate text string
'----------------------------------------------------------------------------
Function DBText(sText)
	DBText = sText
	If NOT isNull(sText) Then
		If trim(sText)<>"" Then
			DBText = Replace(Replace(trim(sText),"'","`"),"""","``")
			DBText = Replace(Replace(DBText, "<", "&lt;"), ">", "&gt;")
		End If
	End If
End Function

Function dbtextShow(sText)
	dbtextShow = sText
	If NOT isNull(sText) Then
		If trim(sText)<>"" Then
			dbtextShow = replace(replace(replace(trim(sText),"``",""""),"`","'"),chr(13),"<br />")
		End If
	End If
End Function
Function dbtextShowForm(sText)
	dbtextShowForm = sText
	If NOT isNull(sText) Then
		If trim(sText)<>"" Then
			dbtextShowForm = replace(replace(trim(sText),"``",""""),"`","'")
		End If
	End If
End Function
Function dbtextShowHtml(sText)
	dbtextShowHtml = sText
	If NOT isNull(sText) Then
		If trim(sText)<>"" Then
			dbtextShowHtml = replace(replace(trim(sText),"``",""""),"`","'")
		End If
	End If
End Function


'----------------------------------------------------------------------------
'	Misc
'----------------------------------------------------------------------------
Function FormatDatesTimes(sString,bShowDate,bShowTime,bShowSec)
	sDateTime = ""
	If bShowDate = 1 Then
		If day(sString)<10 Then sDateTime = sDateTime & "0"
		sDateTime = sDateTime & day(sString) & "/"
		If month(sString)<10 Then sDateTime = sDateTime & "0"
		sDateTime = sDateTime & month(sString) & "/" & right(year(sString),2)
	End If
	If bShowDate = 1 AND	bShowTime = 1 Then sDateTime = sDateTime & "&nbsp;&nbsp;"
	If bShowTime = 1 Then
		If hour(sString)<10 Then sDateTime = sDateTime & "0"
		sDateTime = sDateTime & hour(sString) & ":"
		If minute(sString)<10 Then sDateTime = sDateTime & "0"
		sDateTime = sDateTime & minute(sString)
		If bShowSec = 1 Then sDateTime = sDateTime & ":" & second(sString)
	End If
	FormatDatesTimes = sDateTime
End Function
					
Function GetRandomNum(sLength)
	Dim i : GetRandomNum = ""
	Randomize(Timer)
	While Len(GetRandomNum) < sLength
		GetRandomNum = GetRandomNum & Left(CLng(Rnd() * 10), 1)
	Wend
End Function

Function FileAppendData(strFile, strData)
	Set fso = Server.CreateObject("Scripting.FileSystemObject")
	Set obfFile = fso.OpenTextFile(Server.MapPath("/Data/Uploads/" & strFile), 8, True)
	obfFile.Write strData
	obfFile.Close
	Set obfFile = Nothing
	Set fso = Nothing
End Function

Function GetCurText(ByVal trnCurr)
	trnCurr = CInt(trnCurr)
	If IsEmpty(Application("CUR_CHARS")) Then LoadCurrencies()
	If trnCurr <> 255 And trnCurr > UBound(Application("CUR_CHARS")) Then LoadCurrencies()
	If (trnCurr < 0) Or (trnCurr = 255) Then GetCurText = "[All]" Else GetCurText = Application("CUR_CHARS")(trnCurr)
End Function

Function GetCurISO(ByVal trnCurr)
	trnCurr = CInt(trnCurr)
	If IsEmpty(Application("CUR_ISOCODES")) Then LoadCurrencies()
	If trnCurr <> 255 And trnCurr > UBound(Application("CUR_ISOCODES")) Then LoadCurrencies()
	If (trnCurr < 0) Or (trnCurr = 255) Then GetCurText = "[All]" Else GetCurISO = Application("CUR_ISOCODES")(trnCurr)
End Function

Function FormatCurr(nCurrency, nAmount)
	Dim sColor: 
	If IsNull(nAmount) Then nAmount = 0
	If nAmount<0 Then sColor="color:#9b2424;" Else sColor=""
	FormatCurr = "<span style=""white-space:nowrap;" & sColor & """>" & GetCurISO(nCurrency) & "&nbsp;" & FormatNumber(nAmount, 2, True, False, True) & "</span>"
End Function

Function FormatCurrClean(nCurrency, nAmount)
	FormatCurrClean = GetCurText(nCurrency) & " " & FormatNumber(nAmount, 2, True, False, True)
End Function

Function FormatCurrWCT(trnCurr, trnValue, crType)
	If isNumeric(crType) And CInt(crType) = 0 Then trnValue = -trnValue
	FormatCurrWCT = FormatCurr(trnCurr, trnValue)
End Function

Function GetCurrIDByIso(isoCode)
	Dim i
	If IsEmpty(Application("CUR_ISOCODES")) Then LoadCurrencies()
	For i = LBound(Application("CUR_ISOCODES")) To UBound(Application("CUR_ISOCODES"))
		If Application("CUR_ISOCODES")(i) = isoCode Then 
			GetCurrIDByIso = i
			Exit Function
		End If
	Next
End Function

Public Function LoadCurrencies()
	If DateDiff("h", Application("CURR_UPDATETIME"), Now) < 24 Then Exit Function
	Dim crRs, CUR_CHARS(), CUR_RATES(), CUR_ISOCODES()
	Set crRs = Server.CreateObject("ADODB.Recordset")
	crRs.Open "Select CUR_ID, CUR_BaseRate, CUR_Symbol, CUR_ISOName From tblSystemCurrencies Order By CUR_ID Desc", OleDbData, 0, 1
	If Not crRs.EOF Then
		MAX_CURRENCY = crRs("CUR_ID")
		Redim CUR_CHARS(MAX_CURRENCY)
		Redim CUR_RATES(MAX_CURRENCY)
		Redim CUR_ISOCODES(MAX_CURRENCY)
	End if
	Do While Not crRs.EOF
		If crRs("CUR_ID") - MAX_CURRENCY <= 0 Then
			CUR_CHARS(crRs("CUR_ID")) = crRs("CUR_Symbol")
			CUR_RATES(crRs("CUR_ID")) = crRs("CUR_BaseRate")
			CUR_ISOCODES(crRs("CUR_ID")) = crRs("CUR_ISOName")
		End If
		crRs.MoveNext
	Loop
	crRs.Close()
	Set crRs = Nothing
	Application("CUR_CHARS") = CUR_CHARS : Application("CUR_RATES") = CUR_RATES : Application("CUR_ISOCODES") = CUR_ISOCODES
	Application("CURR_UPDATETIME") = Now
End Function

Public Function ConvertCurrencyRate(xCurFrom, xCurTo)
	If xCurFrom <> xCurTo Then
		LoadCurrencies()
		ConvertCurrencyRate = Application("CUR_RATES")(xCurFrom) / Application("CUR_RATES")(xCurTo)		
		'ConvertCurrencyRate = ExecScalar("SELECT (SELECT CUR_BaseRate From tblSystemCurrencies WHERE CUR_ID=" & xCurFrom & ")/(SELECT CUR_BaseRate FROM tblSystemCurrencies WHERE CUR_ID=" & xCurTo & ")",Nothing)
	Else
		ConvertCurrencyRate = 1
	End if
End Function

Public Function ConvertCurrencyRateWFee(xCurFrom, xCurTo)
	If xCurFrom <> xCurTo Then
		LoadCurrencies()
		'ConvertCurrencyRate = Application("CUR_RATES")(xCurFrom) / Application("CUR_RATES")(xCurTo)		
		ConvertCurrencyRateWFee = ExecScalar("Select (((Select CUR_BaseRate From tblSystemCurrencies Where CUR_ID=" & xCurFrom & ")* (1-(Select CUR_ExchangeFeeInd From tblSystemCurrencies Where CUR_ID=" & xCurTo & ")))/(Select CUR_BaseRate From tblSystemCurrencies Where CUR_ID=" & xCurTo & ")) as Rate",Nothing)		
	Else
		ConvertCurrencyRateWFee = 1
	End if
End Function 

Public Function ConvertCurrency(xCurFrom, xCurTo, xAmount)
	ConvertCurrency = CCur(ConvertCurrencyRate(xCurFrom, xCurTo) * xAmount)
End Function

Public Function ConvertCurrencyWFee(xCurFrom, xCurTo, xAmount)
	ConvertCurrencyWFee = CCur(ConvertCurrencyRateWFee(xCurFrom, xCurTo) * xAmount)
End Function

Public Function ConvertCurrencyWRate(xCurFrom, xCurTo,xRate,xAmount)
	if not isnumeric(xRate) then
		Err.Raise 8,"ConvertCurrencyWRate()","Exchange rate must be numeric"
		Exit Function
	end if
	if xRate < 0 then
		Err.Raise 9,"ConvertCurrencyWRate()","Exchange rate must be positive"
		Exit Function	
	end if 
	ConvertCurrencyWRate = CCur(xRate * xAmount)
End Function

Function showDivBox(fType, fDir, fWidth, fText) '(help;note, ltr,rtl)
	fWidth = Replace(fWidth, "px", "")
	sAlign = IIf(fDir="ltr","left","right")
	sOnMouseOver = "previousSibling.style.visibility='visible';"
	sOnMouseOver = sOnMouseOver & "previousSibling.style.left=getAbsPos(this).Left+15;"
	sOnMouseOver = sOnMouseOver & "previousSibling.style.top=getAbsPos(this).Top+15;"
	sOnMouseOver = sOnMouseOver & "if ((document.body.clientWidth-previousSibling.style.left.replace('px', ''))-" & fWidth & "<0)"
	sOnMouseOver = sOnMouseOver & "	previousSibling.style.left=document.body.clientWidth-" & fWidth & ";"
	sOnMouseOut = "previousSibling.style.visibility='hidden';"
	Response.Write("<div class=""box " & IIf(LCase(Trim(fType))="help", "boxHelp", "boxNote") & " " & fDir & """ style=""left:0px; top:0px; font-weight:normal!important; text-align:" & sAlign & "!important; width:" & fWidth & "px;"">" & fText & "</div>")
	Response.Write("<img class=""boxIcon"" src=""../images/iconQuestionGrayS.gif"" onmouseover=""" & sOnMouseOver & """ onmouseout=""" & sOnMouseOut & """ />")
End Function

Function CreateTextBlob(text)
    If NetpayCryptoObj is Nothing Then Set NetpayCryptoObj = Server.CreateObject("Netpay.Crypt.Utility")
    Set CreateTextBlob = NetpayCryptoObj.CreateBlob()
    if Not IsEmpty(text) Then CreateTextBlob.Text = text
End Function

Function Hash(hashType, hashString)
    If hashType = "SHA" Then hashType = "SHA256"
    Set Hash = CreateTextBlob(hashString).Hash(hashType)
End Function

Function CalcHash(hashType, hashString, useRawOutput)
    Dim obj : Set obj = Hash(hashType, hashString)
    if Not useRawOutput Then obj.ASCII = LCase(obj.Hex)
    CalcHash = obj.Base64
    Set obj = Nothing
End Function

Function ValidateHash(hashType, hashString, hashValue)
	Dim hashCalcValue
	hashCalcValue = CalcHash(hashType, hashString, True)
	ValidateHash = (hashCalcValue = hashValue)
	If ValidateHash Then Exit Function
	ValidateHash = CalcHash(hashType, hashString, False) = hashValue
End Function

'
' 20080819 Tamir
'
' Support of error handling after response.flush
'
sub HandleErrorAfterFlush
	if err.number<>0 then
		sErrNumber=err.number 
		sErrSource=err.Source
		sErrDescription=err.Description 
		Session("ErrNumber")=err.number 
		Session("ErrSource")=err.Source
		Session("ErrDescription")=err.Description 
		Session("ErrSQL")=sSQL
		if Session("ErrSQL")="" then Session("ErrSQL")=SQL
		on error goto 0
		server.Transfer "../error.asp"
	end if
end sub

'
' 20081104 Tamir
'
' Checks, if the string matches the regular expression pattern
'
function IsValidStringRegex(sString, sPattern)
	if sString="" then
		IsValidStringRegex=false
	else
		dim regxTest : set regxTest=new RegExp
		regxTest.Pattern = sPattern
		regxTest.IgnoreCase=True
		regxTest.Global=false
		IsValidStringRegex = regxTest.test(sRecurring)
		set regxTest=nothing
	end if
end function

'
' 20081104 Tamir - Validate recurring string format
'
' Recurring field format: {number of charges}{gap unit}{gap length}A{amount}
' Having:
' {number of charges} � a number of charges in this part of recurring series
' {gap unit} � one of the following values: D (day), W (week), M (month), Q (quarter), Y (year)
' {gap length} � a number of gap units
' A{amount} � an amount to charge (optional). If the amount is omitted, letter A must be omitted, too.
'
' Examples:
' 12M1A30 - twelwe monthly charges, 30 currency units each charge
' 1D3 - one charge with 3 days delay after the charge, with default amount
' 
function IsValidRecurringString(sRecurring)
	IsValidRecurringString=IsValidStringRegex(sRecurring, "^[1-9][0-9]{0,1}[DWMQY][1-9][0-9]*(A[0-9]+\.*[0-9]*)*$")
end function

'
' 20081203 Tamir
'
' Returns appropriate icon for the passed filename extension
'
function GetFileIcon(sFileName)
	dim sFileExt, sIcon
	sFileExt=LCase(mid(sFileName, inStrRev(sFileName, ".")+1))
	if len(sFileExt)>3 then sFileExt=Left(sFileExt, 3)
	select case sFileExt
		case "rtf" : sFileExt="doc"
		case "jpe" : sFileExt="jpg"
		case "mp4", "mov", "wmv", "asf" : sFileExt="avi"
		case "mp3", "mid" : sFileExt="wav"
		case "rar", "arj", "7z" : sFileExt="zip"
	end select
	if sFileExt="rtf" then sFileExt="doc"
	if sFileExt="jpe" then sFileExt="jpg"
	if instr(".avi.bmp.doc.eps.gif.jpg.mpg.pdf.png.ppt.tif.txt.wav.xls.zip.", "." & sFileExt & ".")<1 then
		GetFileIcon=""
	else
		GetFileIcon="<img src=""/NPCommon/ImgFileExt/" & sFileExt & ".gif"" style=""border-width:0;margin:0 5px 0 0;vertical-align:middle;"" />"
	end if
end function

Function MapWirePath(fileName)
    fileName = Replace(fileName, "/", "\")
    MapWirePath = Session("DataPath") & "\Private\Wires\" & fileName
End Function

Function DownloadPrivateFile(fileName)
    Dim fileData, fileExt
    If NetpayCryptoObj is Nothing Then Set NetpayCryptoObj = Server.CreateObject("Netpay.Crypt.Utility")
    fileExt = Right(fileName, Len(fileName) - InStrRev(fileName, "."))
    fileName = Session("DataPath") & "\Private\" & fileName
    Set fileData = NetpayCryptoObj.CreateBlob()
    fileData.LoadFromFile fileName, 0
    fileName = Right(fileName, Len(fileName) - InStrRev(fileName, "\"))
    Response.Clear()
    Response.Buffer = False
    Response.AddHeader "content-disposition", "attachment;filename=" & fileName
    Response.ContentType = "application/" + fileExt
    Response.BinaryWrite(fileData.Bytes)
    Set fileData = Nothing
    closeConnection()
    Response.End()
End Function
If Request("DownloadPrivateFile") <> "" Then DownloadPrivateFile Request("DownloadPrivateFile")

Function GetDomainConfig(domainName, configName, defValue)
    Dim Domains : Set Domains = Nothing
    'If IsObject(Application("Domains")) Then Set Domains = Application("Domains")
    If Domains Is Nothing Then
        Dim domainsfileName, domainData, binXmlFile, domainXmlFile
        Set Domains = Server.CreateObject("Scripting.Dictionary")
        domainsfileName = Replace(LCase(Server.MapPath("bin/Netpay.Infrastructure.config.xml")), "admincash\admincash", "admincash")
        Set binXmlFile = Server.CreateObject("Msxml2.DOMDocument.6.0")
        binXmlFile.load(domainsfileName)
        For Each d in binXmlFile.documentElement.getElementsByTagName("domain")
            Set domainData = Server.CreateObject("Scripting.Dictionary")
            Set domainXmlFile = Server.CreateObject("Msxml2.DOMDocument.6.0")
            if Not IsEmpty(d.getAttribute("fileName")) Then
                domainXmlFile.load(d.getAttribute("fileName"))
                For Each n in domainXmlFile.getElementsByTagName("parameter")
                    domainData.Add n.getAttribute("key"), n.getAttribute("value")
                Next
            End If
            For Each n in d.getElementsByTagName("parameter")
                if domainData.Exists(n.getAttribute("key")) Then domainData.Item(n.getAttribute("key")) = n.getAttribute("value") _
                Else domainData.Add n.getAttribute("key"), n.getAttribute("value")
            Next
            Domains.Add domainData.Item("host"), domainData
        Next
        'Set Application("Domains") = Domains
    End If
    GetDomainConfig = defValue
    If IsEmpty(domainName) Then
        domainName = Request.ServerVariables("HTTP_HOST")
        While (domainName <> "" And Not Domains.Exists(domainName))
		    Dim nextComponent : nextComponent = InStr(1, domainName, ".")
		    if nextComponent < 1 Then nextComponent = Len(domainName)
		    domainName = Mid(domainName, nextComponent + 1, Len(domainName) - nextComponent)
	    Wend
        If domainName = "" And Domains.Exists("default") Then domainName = "default"
    End If
    If Not Domains.Exists(domainName) Then Exit Function
    GetDomainConfig = Domains.Item(domainName).Item(configName)
End Function

%>