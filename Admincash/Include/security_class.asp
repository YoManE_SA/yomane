<%
	'
	' 20080513 Tamir
	'
	' New security mechanism - groups
	'

	class Security
		public property get Username
			dim sUsername
			sUsername=LCase(Request.ServerVariables("REMOTE_USER"))
			if Request.Cookies("userName") <> "" and sUsername = "" then
                sUsername=LCase(Request.Cookies("userName"))
            end if
			If inStrRev(sUsername, "\") > 0 Then sUsername=mid(sUsername, inStrRev(sUsername, "\")+1)
			Username=sUsername
		end property

		public property get FullName
			dim sSQL, sUsername, rsData
			sUsername=Username
			sSQL="SELECT su_Name FROM tblSecurityUser WHERE su_Username='" & DBText(sUsername) & "'"
			Set rsData=oledbData.Execute(sSQL)
			if rsData.EOF then
				FullName=sUsername
			else
				FullName=rsData(0)
			end if
			rsData.Close
		end property

		public property get Path
			dim sPath
			sPath = LCase(Request.ServerVariables("PATH_INFO"))
			If inStr(sPath, "?") > 0 Then sPath=mid(sPath, inStr(sPath, "?")+1)
			If inStrRev(sPath, "/") > 0 Then sPath=mid(sPath, inStrRev(sPath, "/")+1)			
			If inStr(sPath, "#") > 0 Then sPath=mid(sPath, inStr(sPath, "#")+1)			
			Path=sPath
		end property

		public property get IsUserActive
			dim sSQL, rsData
			sSQL="IF EXISTS (SELECT ID FROM tblSecurityUser WHERE su_IsActive=1 AND su_Username='" & DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
			Set rsData=oledbData.Execute(sSQL)
			if rsData.EOF then
				IsUserActive=false
			else
				IsUserActive=rsData(0)
			end if
			rsData.Close
		end property

		public property get IsUserLogged
			dim sSQL, rsData
			sSQL="IF EXISTS (SELECT ID FROM tblSecurityUser WITH (NOLOCK) WHERE su_IsLogged=1 AND su_Username='" & DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
			Set rsData=oledbData.Execute(sSQL)
			if not rsData.EOF then
				IsUserLogged=false
			else
				IsUserLogged=rsData(0)
			end if
			rsData.Close
			if IsUserLogged then exit property
			sSQL="IF EXISTS (SELECT * FROM tblSecurityGroup g INNER JOIN tblSecurityUserGroup ug ON g.ID=sug_Group INNER JOIN tblSecurityUser u ON sug_User=u.ID WHERE sg_IsLogged=1 AND su_Username='" & DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
			Set rsData=oledbData.Execute(sSQL)
			if not rsData.EOF then
				IsUserLogged=false
			else
				IsUserLogged=rsData(0)
			end if
			rsData.Close
		end property

		public property get IsDocumentLogged
			dim sSQL, rsData
			sSQL="IF EXISTS (SELECT ID FROM tblSecurityDocument WITH (NOLOCK) WHERE sd_IsLogged=1 AND sd_URL='" & DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
			Set rsData=oledbData.Execute(sSQL)
			if rsData.EOF then
				IsDocumentLogged=false
			else
				IsDocumentLogged=rsData(0)
			end if
			rsData.Close
		end property

		public property get IsAdmin
			dim sSQL, rsData
			sSQL="IF EXISTS (SELECT ID FROM tblSecurityUser WHERE su_IsAdmin=1 AND su_Username='" & DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
			Set rsData=oledbData.Execute(sSQL)
			if rsData.EOF then
				IsAdmin=false
			else
				IsAdmin=rsData(0)
			end if
			rsData.Close
		end property

		public property get IsLimitedMerchant
			dim sSQL, rsData
			sSQL="IF EXISTS (SELECT u.ID FROM tblSecurityUser u INNER JOIN tblSecurityUserMerchant ON u.ID=sum_User AND su_Username='" & DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
			Set rsData=oledbData.Execute(sSQL)
			if rsData.EOF then
				IsLimitedMerchant=true
			else
				IsLimitedMerchant=rsData(0)
			end if
			rsData.Close
		end property

		public property get IsLimitedDebitCompany
			dim sSQL, rsData
			sSQL="IF EXISTS (SELECT u.ID FROM tblSecurityUser u INNER JOIN tblSecurityUserDebitCompany ON u.ID=sudc_User AND su_Username='" & DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
			Set rsData=oledbData.Execute(sSQL)
			if rsData.EOF then
				IsLimitedDebitCompany=true
			else
				IsLimitedDebitCompany=rsData(0)
			end if
			rsData.Close
		end property

		public property get IsDocumentManaged
			dim sSQL, rsData
			sSQL="IF EXISTS (SELECT ID FROM tblSecurityDocument WHERE sd_URL='" & DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
			Set rsData=oledbData.Execute(sSQL)
			if rsData.EOF then
				IsDocumentManaged=false
			else
				IsDocumentManaged=rsData(0)
			end if
			rsData.Close
		end property

		public property get IsPermitted
			if not IsUserActive then
				IsPermitted=false
			elseif IsAdmin then
				IsPermitted=true
			elseif not IsDocumentManaged then
				IsPermitted=true
			else
				dim sSQL, rsData
				sSQL="IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument INNER JOIN tblSecurityDocumentGroup ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityGroup ON tblSecurityDocumentGroup.sdg_Group=tblSecurityGroup.ID INNER JOIN tblSecurityUserGroup ON tblSecurityGroup.ID=tblSecurityUserGroup.sug_Group INNER JOIN tblSecurityUser ON tblSecurityUserGroup.sug_User=tblSecurityUser.ID WHERE sdg_IsVisible=1 AND sug_IsMember=1 AND su_IsActive=1 AND sg_IsActive=1 AND su_Username='" & DBText(Username) & "' AND sd_URL='" & DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
				Set rsData=oledbData.Execute(sSQL)
				if rsData.EOF then
					IsPermitted=false
				else
					IsPermitted=rsData(0)
				end if
				rsData.Close
			end if
		end property
		
		public property get IsPermittedUser
			if not IsUserActive then
				IsPermittedUser=false
			elseif IsAdmin then
				IsPermittedUser=true
			elseif not IsDocumentManaged then
				IsPermittedUser=true
			else
				dim sSQL, rsData
				'sSQL="IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument INNER JOIN tblSecurityDocumentGroup ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityGroup ON tblSecurityDocumentGroup.sdg_Group=tblSecurityGroup.ID INNER JOIN tblSecurityUserGroup ON tblSecurityGroup.ID=tblSecurityUserGroup.sug_Group INNER JOIN tblSecurityUser ON tblSecurityUserGroup.sug_User=tblSecurityUser.ID WHERE sdg_IsVisible=1 AND sug_IsMember=1 AND su_IsActive=1 AND sg_IsActive=1 AND su_Username='" & DBText(Username) & "' AND sd_URL='" & DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
				sSQL="IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument INNER JOIN tblSecurityDocumentGroup ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityUser ON tblSecurityDocumentGroup.sdg_Group=-tblSecurityUser.ID WHERE sdg_IsVisible=1 AND su_IsActive=1 AND su_Username='" & DBText(Username) & "' AND sd_URL='" & DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
				Set rsData=oledbData.Execute(sSQL)
				if rsData.EOF then
					IsPermittedUser=false
				else
					IsPermittedUser=rsData(0)
				end if
				rsData.Close
			end if
		end property		

		public property get IsActive
			if not IsUserActive then
				IsActive=false
			elseif IsAdmin then
				IsActive=true
			elseif not IsDocumentManaged then
				IsActive=true
			else
				dim sSQL, rsData
				sSQL="IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument INNER JOIN tblSecurityDocumentGroup ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityGroup ON tblSecurityDocumentGroup.sdg_Group=tblSecurityGroup.ID INNER JOIN tblSecurityUserGroup ON tblSecurityGroup.ID=tblSecurityUserGroup.sug_Group INNER JOIN tblSecurityUser ON tblSecurityUserGroup.sug_User=tblSecurityUser.ID WHERE sdg_IsVisible=1 AND sdg_IsActive=1 AND sug_IsMember=1 AND su_IsActive=1 AND sg_IsActive=1 AND su_Username='" & DBText(Username) & "' AND sd_URL='" & DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
				Set rsData=oledbData.Execute(sSQL)
				if rsData.EOF then
					IsActive=false
				else
					IsActive=rsData(0)
				end if
				rsData.Close
			end if
		end property
		
		public property get IsActiveUser
			if not IsUserActive then
				IsActiveUser=false
			elseif IsAdmin then
				IsActiveUser=true
			elseif not IsDocumentManaged then
				IsActiveUser=true
			elseif IsAdmin then
				IsActiveUser=true
			else
				dim sSQL, rsData
				'sSQL="IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument INNER JOIN tblSecurityDocumentGroup ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityGroup ON tblSecurityDocumentGroup.sdg_Group=tblSecurityGroup.ID INNER JOIN tblSecurityUserGroup ON tblSecurityGroup.ID=tblSecurityUserGroup.sug_Group INNER JOIN tblSecurityUser ON tblSecurityUserGroup.sug_User=tblSecurityUser.ID WHERE sdg_IsVisible=1 AND sdg_IsActive=1 AND sug_IsMember=1 AND su_IsActive=1 AND sg_IsActive=1 AND su_Username='" & DBText(Username) & "' AND sd_URL='" & DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
				sSQL="IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument INNER JOIN tblSecurityDocumentGroup ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityUser ON tblSecurityDocumentGroup.sdg_Group=-tblSecurityUser.ID WHERE sdg_IsVisible=1 AND sdg_IsActive=1 AND su_IsActive=1 AND su_Username='" & DBText(Username) & "' AND sd_URL='" & DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
				Set rsData=oledbData.Execute(sSQL)
				if rsData.EOF then
					IsActiveUser=false
				else
					IsActiveUser=rsData(0)
				end if
				rsData.Close
			end if
		end property		

		public function IsMemberOf(groupName)
			Dim rsData
			Set rsData = oledbData.Execute("GetSecurityGroups '" & DBText(Username) & "', '" & DBText(groupName) & "'")
			IsMemberOf = Not rsData.EOF
			rsData.Close
			Set rsData = Nothing
		end function

	end class

	dim PageSecurity : set PageSecurity=new Security
	'response.Write "User: "
	'response.Write PageSecurity.Username
	'response.Write " ("
	'if PageSecurity.IsUserActive then response.Write "active" else response.Write "blocked"
	'response.Write ", "
	'if PageSecurity.IsAdmin then response.Write "admin" else response.Write "not admin"
	'response.Write ")<br />"
	'response.Write "File: "
	'response.Write PageSecurity.Path
	'response.Write " ("
	'if PageSecurity.IsDocumentManaged then response.Write "managed" else response.Write "not managed"
	'response.Write ")<br />"
	'response.Write "Access: "
	'response.Write PageSecurity.Path
	'response.Write " ("
	'if PageSecurity.IsPermitted then response.Write "granted" else response.Write "denied"
	'response.Write ")<br />"
%>