<%
	Dim bUseImprov : bUseImprov = True
	Class CAmount
		Dim FinanceFee, FinanceCount, NormLineFee, NormFutInst
		Dim HandlingCount, HandlingFee
		Dim NormAmount, NormCount, NormFees
		Dim RefAmount, RefCount, RefFees
		Dim CashbackAmount, CashbackCount, CashbackFees
		Dim ClrfAmount, ClrfCount, ClrfFees
		Dim DenAmount, DenCount, DenFees
		Dim NormDebitFee, RefDebitFee, DenDebitFees
        Dim BankPaidAmount, BankPaidCount

		Public Sub Class_Initialize()
			FinanceFee = 0.0 : FinanceCount = 0 : NormFutInst = 0.0 : NormLineFee = 0.0
			HandlingCount = 0 : HandlingFee = 0.0
			NormAmount = 0.0: NormCount = 0: NormFees = 0.0
			RefAmount = 0.0: RefCount = 0: RefFees = 0.0
		    CashbackAmount = 0.0: CashbackCount = 0: CashbackFees = 0.0
			ClrfAmount = 0.0: ClrfCount = 0: ClrfFees = 0.0
			DenAmount = 0.0: DenCount = 0: DenFees = 0.0
			NormDebitFee = 0.0: RefDebitFee = 0.0: DenDebitFees = 0.0
			BankPaidAmount = 0.0: BankPaidCount = 0.0
		End Sub
		
		Public Property Get NormRatioFee()
			NormRatioFee = NormFees - NormLineFee
		End Property

		Public Property Get TotalCount()
			TotalCount = (NormCount + RefCount + CashbackCount + ClrfCount + DenCount)
		End Property

		Public Property Get TotalDebitFees()
			TotalDebitFees = NormDebitFee + RefDebitFee + DenDebitFees
		End Property
		
		Public Property Get TotalAmount()
			TotalAmount = (NormAmount + RefAmount + CashbackAmount) - DenAmount
		End Property
		
		Public Property Get TotalFees()
			TotalFees = (NormFees + RefFees + CashbackFees + ClrfFees + DenFees + FinanceFee)
		End Property
		
		Public Property Get TotalPay()
			TotalPay = TotalAmount - TotalFees
		End Property

		Public Property Get TotalProfit()
			TotalProfit = TotalFees - TotalDebitFees
		End Property
		
		Public Property Get TotalFeePercent()
		    If (NormFees - NormLineFee = 0) Or (NormAmount = 0) Then 
		        TotalFeePercent = 0
		        Exit Property
		    End If
		    TotalFeePercent = Round(((NormFees - NormLineFee) / NormAmount) * 100, 2)
		End Property

		Function calcDeniedTrans(iRs, xPayID, bAddClrf, bAddDen)
			Dim nAmount
			If iRs("CreditType") = DNS_ChbRefundCTrans And iRs("DeniedStatus") = DNS_SetInVar And xPayID = 0 Then
				nAmount = TestNumVar(ExecScalar("Select Sum(amount) From tblCompanyTransInstallments Where payID > 0 And transAnsID=" & iRs("ID"), 0), 0, -1, 0)
			Else	
				nAmount = iRs("Amount")
			End If 
			If bAddClrf Then
				ClrfCount = ClrfCount + 1
				ClrfAmount = ClrfAmount + nAmount
				ClrfFees = ClrfFees + TestNumVar(iRs("netpayFee_ClrfCharge"), 0, -1, 0)
			End If	
			If bAddDen Then
				DenCount = DenCount + 1
				DenAmount = DenAmount + nAmount
				DenFees = DenFees + TestNumVar(iRs("netpayFee_chbCharge"), 0, -1, 0)
				DenDebitFees = DenDebitFees + iRs("DebitFee")
			End If
			calcDeniedTrans = nAmount
		End Function
		
		Function calcFinanceFee(iRs, xPayID, xPrime)
			If iRs("Interest") <> 0 And iRs("Payments") > 1 Then
				FinanceCount = FinanceCount + 1
				If xPayID > 800 Then 'a different calculation for old invoices
					FinanceFee = FinanceFee + (iRs("Amount") * ((iRs("Interest") + xPrime) / 100))
				Else
					FinanceFee = FinanceFee + (iRs("Amount") * ((((iRs("Interest") + xPrime) / 12) * (iRs("Payments") / 2)) / 100))
				End if	
			End If
		End Function
		
		Public Function AddTransaction(iRs, xPayID, xPayDate, xPrime, bApply, bFirstNPInts)
			Dim bIncCharge, nStatus, xValue, bPayTrans : bIncCharge = 0
			Dim netpayFee_chbCharge, netpayFee_ClrfCharge
			If xPayID = 0 And iRs("isTestOnly") Then Exit Function 
			netpayFee_chbCharge = "netpayFee_chbCharge" : netpayFee_ClrfCharge = "netpayFee_ClrfCharge"
			nStatus = iRs("DeniedStatus")
			If bApply Then
				If nStatus = DNS_UnSetInVar Then nStatus = TestNumVar(Request("deniedID" & iRs("ID")), 0, -1, IIF(Request("AutoCHB") = "1", DNS_SetFoundUnValid, nStatus)) _
				Else If nStatus = DNS_SetInVar Then nStatus = TestNumVar(Request("deniedID" & iRs("ID")), 0, -1, IIF(Request("AutoCHB") = "1", DNS_DupFoundUnValid, nStatus))
			End If	
			bPayTrans = False
			Select Case nStatus
			Case 0, DNS_ChbRefundRTrans '���� ����
				bIncCharge = 1
			Case DNS_UnSetInVar '������ ������
				If xPayID = 0 Then 
					If (iRs("CreditType") <> 8) Or (iRs("InsID") = 1) Then _
						calcDeniedTrans iRs, xPayID, True, True
				End if
				bIncCharge = 1
			Case DNS_SetInVar '���� ������
				If xPayID = 0 Then
					If (iRs("CreditType") <> 8) Or (iRs("InsID") = 1) Then _
						calcDeniedTrans iRs, xPayID, True, True
					bIncCharge = 0
				Else 
					bIncCharge = 1
				End if
			Case DNS_SetFoundValid, DNS_SetFoundValidRefunded '��� ����� �����
				If (iRs("CreditType") <> 8) Or (iRs("InsID") = 1) Then _
					calcDeniedTrans iRs, xPayID, True, False
				netpayFee_chbCharge = 0 
				If iRs("CreditType") = 8 Then netpayFee_ClrfCharge = 0 'do not charge
				bIncCharge = 1
			Case DNS_SetFoundUnValid '��� ����� �����
				If (iRs("CreditType") <> 8) Or (iRs("InsID") = 1) Then
					calcDeniedTrans iRs, xPayID, True, True
					'If bApply Then oledbData.Execute "UPDATE tblCompanyTransPass SET DeniedStatus=" & nStatus & ", PayID=';" & xPayID & ";' WHERE ID=" & iRs("ID")
				End if
				bIncCharge = 1
			Case DNS_DupFoundValid '������ �� ���� ����� ����
				If iRs("CreditType") <> 8 Then
					calcDeniedTrans iRs, xPayID, True, False
					If bApply And (iRs("DeniedStatus") <> DNS_DupFoundValid) Then
						CopyTransaction iRs, xPayID, xPayDate, nStatus, 0
						oledbData.Execute "UPDATE tblCompanyTransPass SET DeniedStatus=" & DNS_DupWasWorkedOut & ", netpayFee_chbCharge=0, netpayFee_ClrfCharge=0 WHERE ID=" & iRs("ID")
					End if
					bPayTrans = True
					bIncCharge = 0
				Else
					netpayFee_chbCharge = 0 : netpayFee_ClrfCharge = 0
					bIncCharge = 1
				End if
			Case DNS_DupFoundUnValid '������ �� ���� �����
				bIncCharge = 0
				If (iRs("CreditType") <> 8) Or (iRs("InsID") = 1 Or IsNull(iRs("InsID"))) Then
					xValue = calcDeniedTrans(iRs, IIF(bApply, 0, xPayID), True, True)
					If bApply Then
						If (iRs("DeniedStatus") = DNS_DupFoundUnValid) Then
							bPayTrans = True
						Else
							If iRs("CreditType") = 8 Then oledbData.Execute "UPDATE tblCompanyTransPass SET PayID='" & Replace(iRs("PayID"), "0;", "") & "' WHERE ID=" & iRs("ID")
							CopyTransaction iRs, xPayID, xPayDate, nStatus, xValue
							oledbData.Execute "UPDATE tblCompanyTransPass SET DeniedStatus=" & DNS_WasWorkedOut & ", netpayFee_chbCharge=0, netpayFee_ClrfCharge=0 WHERE ID=" & iRs("ID")
						End if	
					End if
				End if
			Case DNS_ChbRefundCTrans ' ����� �� ���� ����� ������
				bIncCharge = 0
				bPayTrans = True
			Case DNS_WasWorkedOut, DNS_DupWasWorkedOut, DNS_DupWasWorkedOutRefunded
				bIncCharge = 1
				calcDeniedTrans iRs, xPayID, True, False
			End Select
			'Response.Write(iRs("ID") & " " & NormAmount & " " & NormCount & " " & bIncCharge & "<br />")
			If bIncCharge <> 0 Then
				bPayTrans = True
				If iRs("CreditType") = 0 Then
                    If iRs("IsCashback") Then
					    CashbackAmount = CashbackAmount - (iRs("Amount") * bIncCharge)
					    If bIncCharge > 0 Then 
                            CashbackCount = CashbackCount + 1
    						CashbackFees = CashbackFees + iRs("netpayFee_transactionCharge") + iRs("netpayFee_ratioCharge")
                        End If
                    Else
    					RefAmount = RefAmount - (iRs("Amount") * bIncCharge)
				        If bIncCharge > 0 Then
						    RefCount = RefCount + 1
						    RefFees = RefFees + iRs("netpayFee_transactionCharge") + iRs("netpayFee_ratioCharge")
    						RefDebitFee = RefDebitFee + iRs("DebitFee")
                        End If
                    End If
					If bIncCharge > 0 And iRs("HandlingFee") > 0 Then
						HandlingCount = HandlingCount + 1
						HandlingFee = HandlingFee + iRs("HandlingFee")
					End if	
				ElseIf iRs("CreditType") = 8 Then
					xValue = Int(iRs("Amount") / iRs("Payments"))
					If iRs("InsID") = 1 Then xValue = (iRs("Amount") - (xValue * iRs("Payments"))) + xValue
					If (iRs("PrimaryPayedID") = IIF(bApply, 0, xPayID)) And (iRs("InsID") = 1) Then
						NormCount = NormCount + 1
						NormFees = NormFees + iRs("netpayFee_transactionCharge") + iRs("netpayFee_ratioCharge")
						NormLineFee = NormLineFee + iRs("netpayFee_transactionCharge")
						NormDebitFee = NormDebitFee + iRs("DebitFee")
						calcFinanceFee iRs, xPayID, xPrime
					End If
					bPayTrans = False
					If (xPayID <> 0 And iRs("IPayID") = xPayID) Or (nStatus = DNS_SetFoundUnValid) Then 
						NormAmount = NormAmount + (xValue * bIncCharge)
						bPayTrans = (nStatus = DNS_SetFoundUnValid)
					ElseIf (iRs("IPayID") = 0) And (xPayID = 0 Or bApply) Then
						If bFirstNPInts Or (InStr(1, ", " & Request("IDpayments") & ",", ", " & iRs("IID") & ",") > 0) Then
							bPayTrans = True
							NormAmount = NormAmount + (xValue * bIncCharge)
						Else
							NormFutInst = NormFutInst + (xValue * bIncCharge)
						End if
					End if
				Else
					NormAmount = NormAmount + (iRs("Amount") * bIncCharge)
					If iRs("BankPaid") Then
					    BankPaidCount = BankPaidCount + 1: BankPaidAmount = BankPaidAmount + iRs("Amount")
					End If    
					If bIncCharge > 0 Then
						NormCount = NormCount + 1
						NormFees = NormFees + iRs("netpayFee_transactionCharge") + iRs("netpayFee_ratioCharge")
						NormLineFee = NormLineFee + iRs("netpayFee_transactionCharge")
						If iRs("HandlingFee") > 0 Then
							HandlingCount = HandlingCount + 1
							HandlingFee = HandlingFee + iRs("HandlingFee")
						End if
						NormDebitFee = NormDebitFee + iRs("DebitFee")
					End If
					calcFinanceFee iRs, xPayID, xPrime
				End if
			End If
			If bApply And bPayTrans Then
				Dim sUpdate, nPayID
				If iRs("CreditType") = 8 Then
					nPayID = ExecScalar("Select PayID From tblCompanyTransPass Where ID=" & iRs("ID"), "")
					nPayID = Replace(nPayID, ";0;", ";" & xPayID & ";", 1, 1, 0)
					If iRs("InsID") = 1 Then sUpdate = "PrimaryPayedID=" & xPayID & ","
					sUpdate = sUpdate & "MerchantPD='" & DateAdd("m", 1, iRs("IMerchantPD")) & "',"
					If Not IsNull(iRs("IID")) Then oledbData.Execute "Update tblCompanyTransInstallments SET PayID=" & xPayID & ", MerchantRealPD='" & xPayDate & "' WHERE ID=" & iRs("IID")
				Else
					nPayID = ";" & xPayID & ";"
					sUpdate = "PrimaryPayedID=" & xPayID & ","
				End If
				oledbData.Execute "UPDATE tblCompanyTransPass SET " & sUpdate & " payID='" & nPayID & "', MerchantRealPD='" & xPayDate & "'," & _
					" DeniedStatus=" & nStatus & ", netpayFee_chbCharge=" & netpayFee_chbCharge & ", netpayFee_ClrfCharge=" & netpayFee_ClrfCharge & _
					" WHERE ID=" & iRs("ID") 'DeniedDate='" & xPayDate
			End if
		End Function

		Function CopyTransaction(iRs, xPayID, xPayDate, nStatus, nAmount)
			Dim sSQL, netpayFee_chbCharge, netpayFee_ClrfCharge : sSQL = ""
			netpayFee_chbCharge = iRs("netpayFee_chbCharge") : netpayFee_ClrfCharge = iRs("netpayFee_ClrfCharge")
			If nStatus = DNS_DupFoundValid Then netpayFee_chbCharge = 0
			sSQL="INSERT INTO tblCompanyTransPass(CompanyID, DebitCompanyID, OriginalTransID, TransSource_id, CustomerID, InsertDate, PrimaryPayedID, PayID, DeniedDate," &_
				"DeniedStatus, IPAddress, Amount, Currency, Payments, CreditType," &_
				"OrderNumber, Interest, Comment, DeniedPrintDate, DeniedSendDate, TerminalNumber, paymentMethodId, paymentMethodDisplay, PaymentMethod, OCurrency, OAmount, netpayFee_chbCharge, netpayFee_ClrfCharge) VALUES (" &_
				iRs("CompanyID") & "," & iRs("DebitCompanyID") & "," & iRs("ID") & "," & iRs("TransSource_id") &_
				"," & iRs("CustomerID") & ", '" & Now() & "', " & xPayID & ", ';" & xPayID & ";" &_
				"','" & iRs("DeniedDate") & "', " & nStatus & ", '" & iRs("IPAddress") &_
				"'," & nAmount & ", " & iRs("Currency") & ", 1, " & IIF(nStatus = DNS_DupFoundUnValid, 0, iRs("CreditType")) &_
				",'" & iRs("OrderNumber") & _
				"', " & iRs("Interest") & ", '" & iRs("Comment") & "', '" & iRs("DeniedPrintDate") & "', '" & iRs("DeniedSendDate") & "', '" & iRs("TerminalNumber") & "', " & iRs("PaymentMethodID") & ", '" &  iRs("paymentMethodDisplay") & "'" & _
				"," & iRs("PaymentMethod") & "," & iRs("OCurrency") & "," & nAmount & "," & netpayFee_chbCharge & "," & netpayFee_ClrfCharge & ")"
			oledbData.Execute sSQL
		End Function

		Public Sub ApplyMultiplyer(pValue)
            NormAmount = NormAmount * pValue
			RefAmount = RefAmount * pValue
			CashbackAmount = CashbackAmount * pValue
			ClrfAmount = ClrfAmount * pValue
			DenAmount = DenAmount * pValue

			NormFees = NormFees * pValue
			RefFees = RefFees * pValue
			ClrfFees = ClrfFees * pValue
			DenFees = DenFees * pValue
			FinanceFee = FinanceFee * pValue
			NormLineFee = NormLineFee * pValue
			NormDebitFee = NormDebitFee * pValue
			RefDebitFee = RefDebitFee * pValue
            CashbackFees = CashbackFees * pValue
			DenDebitFees = DenDebitFees * pValue

			NormCount = NormCount * pValue
			RefCount = RefCount * pValue
    		CashbackCount = CashbackCount * pValue
			ClrfCount = ClrfCount * pValue
			DenCount = DenCount * pValue
			FinanceCount = FinanceCount * pValue

			HandlingCount = HandlingCount * pValue
			HandlingFee = HandlingFee * pValue
			NormFutInst = NormFutInst * pValue
		End Sub

		Public Sub AddAmount(pAmount)
			NormAmount = NormAmount + pAmount.NormAmount
			RefAmount = RefAmount + pAmount.RefAmount
			CashbackAmount = CashbackAmount + pAmount.CashbackAmount
			ClrfAmount = ClrfAmount + pAmount.ClrfAmount
			DenAmount = DenAmount + pAmount.DenAmount

			NormFees = NormFees + pAmount.NormFees
			RefFees = RefFees + pAmount.RefFees
			ClrfFees = ClrfFees + pAmount.ClrfFees
			DenFees = DenFees + pAmount.DenFees
			FinanceFee = FinanceFee + pAmount.FinanceFee
			NormLineFee = NormLineFee + pAmount.NormLineFee
			NormDebitFee = NormDebitFee + pAmount.NormDebitFee
			RefDebitFee = RefDebitFee + pAmount.RefDebitFee
            CashbackFees = CashbackFees + pAmount.CashbackFees
			DenDebitFees = DenDebitFees + pAmount.DenDebitFees

			NormCount = NormCount + pAmount.NormCount
			RefCount = RefCount + pAmount.RefCount
    		CashbackCount = CashbackCount + pAmount.CashbackCount
			ClrfCount = ClrfCount + pAmount.ClrfCount
			DenCount = DenCount + pAmount.DenCount
			FinanceCount = FinanceCount + pAmount.FinanceCount

			HandlingCount = HandlingCount + pAmount.HandlingCount
			HandlingFee = HandlingFee + pAmount.HandlingFee
			NormFutInst = NormFutInst + pAmount.NormFutInst
		End Sub
		
	End Class

	Class CPayment
		Dim mRs, mCompanyID, mCompanyName, mPayDate, mPayID, mCurrency, mVAT, mPrime, mExchangeRate, mApply, mHasRows
		Dim mRRRetAmount, mRRRetCount, mRRTransID, mRRAmount, mRRState, mSecDeposit, mSecPeriod, mSecKeepAmount, mSecKeepCurrency, mCurRes, mSecFlags, mPayPercent
        Dim mFeeProcessDecline, mFeeProcessAuth, mFeeStorageCards
		Dim PaymentMethodAmount()

		Public Sub Class_Initialize()
			mCompanyID = 0 : mPayID = 0 : mCurrency = 0 : mVAT = 0 : mRRState = 0
			mPrime = 0 : mExchangeRate = 0 : mSecDeposit = 0 : mSecPeriod = 0 : mApply = False : mHasRows = False
			mRRRetCount = 0 : mRRRetAmount = 0 : mRRTransID = 0 : mRRAmount = 0 : mSecKeepAmount = 0 : mSecKeepCurrency = 255 : mCurRes = 0
			Redim PaymentMethodAmount(PMD_EC_MAX)
			Set mRs = Server.CreateObject("Adodb.Recordset")
		End Sub
		
		Public Sub Class_Terminate()
			Dim i
			Set mRs = Nothing
			For i = 1 To UBound(PaymentMethodAmount)
			    If Not IsEmpty(PaymentMethodAmount(i)) Then Set PaymentMethodAmount(i) = Nothing
			Next
    		Erase PaymentMethodAmount
		End Sub

		Public Sub InitByPayID(nPayID, nCompanyID, nCurrency)
			If nPayID > 0 Then
				mRs.Open "SELECT tblTransactionPay.*, tblCompany.CompanyName, tblCompany.SecurityPeriod, tblCompany.RREnable, tblCompany.RRAutoRet, tblCompany.RRKeepAmount, tblCompany.RRKeepCurrency, tblCompany.RRState " & _
					" FROM tblTransactionPay Left Join tblCompany ON(tblCompany.ID = tblTransactionPay.CompanyID)" & _
					" WHERE tblTransactionPay.id=" & nPayID, oledbData, 0, 1
				If Not mRs.EOF Then InitFromPaymentRS mRs
				mRs.Close
			Else
				mRs.Open "SELECT tblCompany.*, tblBillingCompanys.VATamount " & _
					" FROM tblCompany Left Join tblBillingCompanys ON(tblCompany.BillingCompanys_id = tblBillingCompanys.BillingCompanys_id) WHERE ID=" & nCompanyID, oledbData, 0, 1
				If Not mRs.EOF Then InitFromCompanyRS mRs, nCurrency
				mRs.Close
			End If
	        mCurRes = 0
		    If mSecKeepAmount > 0 Then
                mRs.Open "Select Amount, Currency From tblCompanyTransPass Where " & _
		                 "CompanyID=" & mCompanyID & IIF(mSecKeepCurrency <> 255, "", " And Currency=" & mCurrency) & " And PaymentMethod=" & PMD_RolRes & " And CreditType=0 And OriginalTransID=0", oleDbData, 0, 1
			    Do While Not mRs.EOF
		            If mSecKeepCurrency <> 255 Then mCurRes = mCurRes + ConvertCurrency(mRs("Currency"), mSecKeepCurrency, mRs("Amount")) _
		            Else mCurRes = mCurRes + mRs("Amount")
			      mRs.MoveNext
			    Loop
			    mRs.Close
		    End if
		End Sub

		Public Sub InitFromCompanyRS(xRs, nCurrency)
			mPayID = 0
			mCurrency = nCurrency
			mCompanyID = xRs("ID")
			mCompanyName = xRs("CompanyName")
			mVAT = IIF(xRs("IsChargeVAT"), xRs("VATamount"), 0)
			mPayDate = Now
			mSecDeposit = xRs("SecurityDeposit")
			mSecPeriod = xRs("SecurityPeriod")
			mSecKeepAmount = xRs("RRKeepAmount")
			mSecKeepCurrency = xRs("RRKeepCurrency")
			mRRState = TestNumVar(xRs("RRState"), 0, -1, 0)
			mSecFlags = IIF(xRs("RREnable"), 1, 0) Or IIF(xRs("RRAutoRet"), 2, 0)
			mPayPercent = TestNumVar(xRs("PayPercent"), 0, 100, 100)
			If mCurrency <> 0 Then mExchangeRate = ConvertCurrencyRate(mCurrency, 0) _
			Else mExchangeRate = ConvertCurrencyRate(1, mCurrency)
			mPrime = ExecScalar("SELECT Prime FROM tblGlobalValues", 0)
		End Sub
		
		Public Sub InitFromPaymentRS(xRs)
			mPayID = xRs("id")
			mCurrency = xRs("Currency")
			mCompanyID = xRs("CompanyID")
			mCompanyName = xRs("CompanyName")
			mVAT = IIF(xRs("IsChargeVAT"), xRs("VATAmount"), 0)
			mPayDate = xRs("Paydate")
			mSecDeposit = xRs("SecurityDeposit")
			mSecPeriod = xRs("SecurityPeriod")
			mSecKeepAmount = xRs("RRKeepAmount")
			mSecKeepCurrency = xRs("RRKeepCurrency")
			mRRState = TestNumVar(xRs("RRState"), 0, -1, 0)
			mPayPercent = TestNumVar(xRs("PayPercent"), 0, 100, 100)
			mSecFlags = IIF(xRs("RREnable"), 1, 0) Or IIF(xRs("RRAutoRet"), 2, 0)
			mExchangeRate = xRs("ExchangeRate")
			mPrime = xRs("PrimePercent")
			'nCompanyName = xRs("BillingCompanyName")
		End Sub

		Public Function CreatePayment(xPayDate)
			Dim sSQL
			If mPayID > 0 Then
				AddTransactions ""
			Else
				mRs.Open "SELECT tblBillingCompanys.* FROM tblBillingCompanys LEFT JOIN tblCompany ON(tblCompany.BillingCompanys_id = tblBillingCompanys.BillingCompanys_id) " &_
					" WHERE tblCompany.ID=" & mCompanyID, oledbData, 0, 1
				If Not mRs.EOF Then
					mPayDate = xPayDate
					sSQL = "INSERT INTO tblTransactionPay " & _
						" (PayDate, CompanyID, TerminalType, PrimePercent, ExchangeRate, Currency, BillingCompanys_id, BillingLanguageShow, BillingCurrencyShow, BillingCompanyName, BillingCompanyAddress, BillingCompanyNumber, BillingCompanyEmail, IsChargeVAT, VATamount, SecurityDeposit, PayPercent)VALUES(" & _
						"'" & mPayDate & "'," & mCompanyID & "," & 1 & "," & mPrime & "," & mExchangeRate & "," & mCurrency & "," & mRs("BillingCompanys_id") & ",'" & _
						mRs("LanguageShow") & "'," & mRs("CurrencyShow") & ",'" & mRs("name") & "','" & mRs("address") & "','" & _
						mRs("number") & "','" & mRs("email") & "'," & IIF(mVAT > 0, 1, 0) & "," & mVAT & "," & mSecDeposit & "," & mPayPercent & ")"
					'Response.Write(sSQL & "<br />")
					oledbData.Execute(sSQL)
					mPayID = ExecScalar("Select Max(id) From tblTransactionPay Where companyID=" & mCompanyID, 0)
				End if
				mRs.Close
				'Copy Fees From tblCompanyCreditFees To tblTransactionPayFees
				oleDbData.Execute("Insert Into tblTransactionPayFees (CCF_TransactionPayID, CCF_CompanyID, CCF_CurrencyID, CCF_PaymentMethod, CCF_ExchangeTo, CCF_MaxAmount, CCF_PercentFee, CCF_FixedFee, CCF_ApproveFixedFee, CCF_RefundFixedFee, CCF_ClarificationFee, CCF_CBFixedFee, CCF_ListBINs)" & _
					"Select " & mPayID & ",CCF_CompanyID, CCF_CurrencyID, CCF_PaymentMethod, CCF_ExchangeTo, CCF_MaxAmount, CCF_PercentFee, CCF_FixedFee, CCF_ApproveFixedFee, CCF_RefundFixedFee, CCF_ClarificationFee, CCF_CBFixedFee, CCF_ListBINs From tblCompanyCreditFees Where CCF_CompanyID=" & mCompanyID & " And CCF_ExchangeTo=" & mCurrency)
			End if
			mApply = True
			CreatePayment = mPayID
		End Function

		Public Property Get RRID()
		    RRID = mRRTransID
		End Property

        Public Property Get PayID() 
            PayID = mPayID
        End Property

        Public Property Get CurrencyID() 
            CurrencyID = mCurrency
        End Property

        Public Property Get CompanyName() 
            CompanyName = mCompanyName
        End Property

		Public Property Get SettlementCurrency
			SettlementCurrency = mCurrency
		End Property
		
		Public Function UpdatePayment()
			If mRRTransID > 0 Then
				If RollingReserve <> 0 Then oledbData.Execute "Update tblCompanyTransPass Set Amount=" & -RollingReserve & " Where ID=" & mRRTransID _
				Else oledbData.Execute "Delete From tblCompanyTransPass Where ID=" & mRRTransID
 			Else
				If RollingReserve <> 0 Then mRRTransID = AddAdminTransAmount(Nothing, RollingReserve, "Rolling Reserve (" & IIF(mRRState = 4, "fixed external", mSecDeposit & "%") & ")", PMD_RolRes, 5, DateAdd("m", mSecPeriod, mPayDate))
			End if
			If mHasRows Then
                UpdateTotals
				UpdatePayment = True
				'oledbData.execute "UPDATE tblWireMoney SET WireAmount=" & Total & "-wireFee WHERE WireType=1 AND WireSourceTbl_id=" & mPayID
			Else
				oledbData.execute "DELETE FROM tblTransactionPay WHERE ID=" & mPayID
				oledbData.execute "DELETE FROM tblWireMoney WHERE WireType=1 AND WireSourceTbl_id=" & mPayID
				UpdatePayment = False
			End If
		End Function
		
		Public Sub UpdateTotals()
		    Dim updateSql, gPMTotal : Set gPMTotal = PaymentMethodTotal(PMD_CC_MIN, PMD_EC_MAX)
    		updateSql = "UPDATE tblTransactionPay SET " & _
			" transTotal=" & TotalAmount & _
			", TransChargeTotal=" & TotalFeesAmount & _
			", TransPayTotal=" & Total & _
			", transRollingReserve=" & RollingReserve & _
            ", TotalCaptureCount=" & gPMTotal.NormCount & _
            ", TotalCaptureAmount=" & gPMTotal.NormAmount & _
            ", TotalAdminCount=" & Admin.TotalCount & _
            ", TotalAdminAmount=" & Admin.TotalAmount & _
            ", TotalSystemCount=" & FeesAutoAndManual.TotalCount & _
            ", TotalSystemAmount=" & FeesAutoAndManual.TotalAmount & _
            ", TotalRefundCount=" & gPMTotal.RefCount & _
            ", TotalRefundAmount=" & -gPMTotal.RefAmount & _
            ", TotalCashbackCount=" & gPMTotal.CashbackCount & _
            ", TotalCashbackAmount=" & -gPMTotal.CashbackAmount & _
            ", TotalChbCount=" & gPMTotal.DenCount & _
            ", TotalChbAmount=" & gPMTotal.DenAmount & _
            ", TotalFeeProcessCapture=" & gPMTotal.NormFees + gPMTotal.RefFees & _
            ", TotalFeeClarification=" & Totals.ClrfFees & _
            ", TotalFeeFinancing=" & Totals.FinanceFee & _
            ", TotalFeeHandling=" & Totals.HandlingFee & _
            ", TotalFeeBank=" & -BankFees.TotalAmount & _
            ", TotalFeeChb=" & gPMTotal.DenFees & _
            ", TotalRollingReserve=" & -RollingReserve & _
            ", TotalRollingRelease=" & ReserveReturn & _
            ", TotalDirectDeposit=" & TotalNotPayedAmount & _
			" WHERE ID=" & mPayID
			'Response.Write(updateSql & "<br />")
		    oledbData.Execute updateSql
		End Sub

		Function AddAdminTransAmount(pAmountT, ByVal nAmount, nCommant, nPaymentMethod, transTypeID, pDate)
			Dim xAmount : AddAdminTransAmount = 0
			Dim amountField
			If nAmount <> 0 Then
				Select Case nPaymentMethod
				Case PMD_SystemFees, PMD_ManualFee
					If Not pAmountT Is Nothing Then
						pAmountT.NormCount = pAmountT.NormCount + 1
						pAmountT.NormFees = pAmountT.NormFees + nAmount
						pAmountT.NormLineFee = pAmountT.NormLineFee + nAmount
					End If
					amountField = "netpayFee_transactionCharge"
				Case Else 'PMD_RolRes
					If Not pAmountT Is Nothing Then
						pAmountT.NormCount = pAmountT.NormCount + 1
						pAmountT.NormAmount = pAmountT.NormAmount + nAmount
					End If
					amountField = "Amount"
				End Select
				mHasRows = True
				If mApply Then
				    Dim sSQL : sSQL = ""
					If pDate = "" Then pDate = "19000101 00:00:00"
					xAmount = IIF(nAmount < 0, -nAmount, nAmount)
					oledbData.Execute("INSERT INTO tblCompanyTransPass(companyID, InsertDate, PrimaryPayedID, payID, TransSource_id, CreditType, IPAddress, " & amountField & ", Currency, OCurrency, Comment, PaymentMethod, PaymentMethod_id, paymentMethodId, paymentMethodDisplay, PD, UnsettledAmount, UnsettledInstallments)" &_
						" VALUES(" & mCompanyID & ",'" & Now() & "', " & mPayID & ",';" & mPayID & ";', " & transTypeID & ", " & IIF(nAmount < 0, 0, 1) & ", '" & Request.ServerVariables("REMOTE_ADDR") & "', " & xAmount & ", " & mCurrency & "," & mCurrency & ", '', " & nPaymentMethod & ", 3, 0, '" & Replace(Left(nCommant, 50), "'", "''") & "','" & pDate & "', 0, 0)")
					AddAdminTransAmount = TestNumVar(ExecScalar("Select Max(ID) From tblCompanyTransPass Where PrimaryPayedID=" & mPayID, 0), 0, -1, 0)
				End if
			End if
		End Function

		Function AddSqlAdminTransAmount(pAmountT, nSQL, nCommant, transTypeID)
			oledbData.CommandTimeout = 600
			mRs.Open nSQL, oledbData, 0, 1
			Do While Not mRs.EOF
				nCommant = Replace(nCommant, "%1", TestNumVar(mRs(1), 0, -1, 0))
				If mRs.Fields.Count > 2 Then nCommant = Replace(nCommant, "%2", TestNumVar(mRs(2), 0, -1, 0))
				AddSqlAdminTransAmount = AddAdminTransAmount(pAmountT, TestNumVar(mRs(0), 0, -1, 0), nCommant, PMD_SystemFees, transTypeID, "")
              mRs.MoveNext
			Loop
			mRs.Close
		End Function

		Sub AddSpecialAdminTransAmount()
			If Request("isApprovalOnlyFee") = "1" Or Not mApply Then
				AddSqlAdminTransAmount GetAmountByPaymentMethod(PMD_SystemFees, 0), "Select Sum(netpayFee_transactionCharge), Count(*) From tblCompanyTransApproval WHERE PayID=0 AND CompanyID=" & mCompanyID & " And Currency=" & mCurrency & " And isTestOnly=0", _
					"Pre-Authorized fee (%1)", 1
				If mApply Then oledbData.Execute("Update tblCompanyTransApproval Set PayID=" & mPayID & " WHERE PayID=0 AND CompanyID=" & mCompanyID & " And Currency=" & mCurrency & " And isTestOnly=0")
			End if
			If Request("isFailFee") = "1" Or Not mApply Then
				AddSqlAdminTransAmount GetAmountByPaymentMethod(PMD_SystemFees, 0), "Select Sum(netpayFee_transactionCharge), Count(*) From tblCompanyTransFail WHERE PayID=0 AND CompanyID=" & mCompanyID & " And Currency=" & mCurrency & " And netpayFee_transactionCharge <> 0 And isTestOnly=0", _
					"Rejected fee (%1)", 25
				If mApply Then oledbData.Execute("Update tblCompanyTransFail Set PayID=" & mPayID & " WHERE PayID=0 AND CompanyID=" & mCompanyID & " And Currency=" & mCurrency & " And isTestOnly=0")
			End if

			If Request("isCcStorageFee") = "1" Or Not mApply Then
				AddSqlAdminTransAmount GetAmountByPaymentMethod(PMD_SystemFees, 0), "SELECT (Count(*) * StorageFee), Count(*), StorageFee FROM tblCCStorage Left Join Setting.SetMerchantSettlement mcs ON(tblCCStorage.companyID = mcs.Merchant_ID And mcs.Currency_ID=" & mCurrency & ") WHERE PayID=0 AND CompanyID=" & mCompanyID & " Group By StorageFee", _
					"CC storage fee (" & GetCurText(mCurrency) & "%2 X %1)", 2
				If mApply Then oledbData.Execute("Update tblCCStorage Set PayID=" & mPayID & " WHERE PayID=0 AND CompanyID=" & mCompanyID)
			End if
			If Request("RetRR") = "1" Or Not mApply Then CalcRetReserve
		End Sub

		Sub AddRetReserve(nID)
			mRs.Open "Select ID, Amount, InsertDate From tblCompanyTransPass Where " & _
				"CompanyID=" & mCompanyID & " And Currency=" & mCurrency & " And OriginalTransID=0 And ID=" & nID, oleDbData, 0, 1
			If Not mRs.EOF Then IAddRetReserve(mRs)
			mRs.Close
		End Sub

		Function IAddRetReserve(iRs)
			IAddRetReserve = AddAdminTransAmount(Nothing, iRs("Amount"), "Release Reserve (" & DateValue(iRs("InsertDate")) & ")", PMD_RolRes, 5, "")
			mRRRetAmount = mRRRetAmount + iRs("Amount")
			mRRRetCount = mRRRetCount + 1
			If mApply Then oleDbData.Execute "Update tblCompanyTransPass Set OriginalTransID=" & IAddRetReserve & " Where ID=" & iRs("ID")
		End Function

		Sub CalcRetReserve()
			If ((mRRState = 1 And (mSecFlags And 2) > 0) Or mRRState = 2 Or mRRState = 3) Then
				mRs.Open "Select ID, Amount, Currency, InsertDate From tblCompanyTransPass Where " & _
					"CompanyID=" & mCompanyID & " And Currency=" & mCurrency & " And PaymentMethod=" & PMD_RolRes & " And CreditType=0 And Cast(InsertDate as Date)<='" & DateAdd("m", -mSecPeriod, mPayDate) & "' And OriginalTransID=0", oleDbData, 0, 1
				Do While Not mRs.EOF
				    If mRRState = 3 Then
				        Dim nItemValue
		                If mSecKeepCurrency <> 255 Then nItemValue = ConvertCurrency(mRs("Currency"), mSecKeepCurrency, mRs("Amount")) _
		                Else nItemValue = mRs("Amount")
				        If mCurRes < mSecKeepAmount Then Exit Do
		                mCurRes = mCurRes - nItemValue
				    End If
					IAddRetReserve(mRs)
				  mRs.MoveNext
				Loop
				mRs.Close
			End If
		End Sub

		Function GetAmountByPaymentMethod(pm, oldPM)
			If (pm = PMD_Unknown) Then 'old style detection
				If oldPM = 1 Then
				    Set GetAmountByPaymentMethod = GetAmountByPaymentMethod(PMD_CC_MIN, 0)
				ElseIf oldPM = 2 Then 
				    Set GetAmountByPaymentMethod = GetAmountByPaymentMethod(PMD_EC_MIN, 0)
				Else	
				    Set GetAmountByPaymentMethod = GetAmountByPaymentMethod(PMD_ManualFee, 0)
				End If
			Else
			    If IsEmpty(Me.PaymentMethodAmount(pm)) Then Set Me.PaymentMethodAmount(pm) = New CAmount
				Set GetAmountByPaymentMethod = Me.PaymentMethodAmount(pm)
			End if
		End Function 
		
		Private Function AddCountTransactions(xWhere)
			Dim pSumItem
			xWhere = "SELECT CreditType, IsCashback, PaymentMethod," & _
				" Count(*) As tVal, Sum(Amount) As Amount," & _
				" Sum(netpayFee_transactionCharge) As netpayFee_transactionCharge, Sum(netpayFee_ratioCharge) As netpayFee_ratioCharge, ep.IsPaid as BankPaid, " & _
				" Sum(DebitFee) As DebitFee" & _
				" FROM tblCompanyTransPass " & _
				" Left Join tblLogImportEPA As ep ON(ep.TransID = tblCompanyTransPass.id And ep.Installment = 1) " & _
				" WHERE " & xWhere & _
				" Group By CreditType, IsCashback, PaymentMethod, ep.IsPaid"
			'Response.Write(xWhere) ': Response.End
			mRs.Open xWhere, oleDbData, 0, 1 '0, 3
			Do While Not mRs.EOF
				mHasRows = True
				Set pSumItem = GetAmountByPaymentMethod(mRs("PaymentMethod"), 1)
				If mRs("CreditType") = 0 Then
                    If mRs("IsCashback") Then
					    pSumItem.CashbackAmount = pSumItem.CashbackAmount - mRs("Amount")
					    pSumItem.CashbackCount = pSumItem.CashbackCount + mRs("tVal")
    					pSumItem.CashbackFees = pSumItem.CashbackFees + mRs("netpayFee_transactionCharge") + mRs("netpayFee_ratioCharge")
                    Else
					    pSumItem.RefAmount = pSumItem.RefAmount - mRs("Amount")
					    pSumItem.RefCount = pSumItem.RefCount + mRs("tVal")
					    pSumItem.RefFees = pSumItem.RefFees + mRs("netpayFee_transactionCharge") + mRs("netpayFee_ratioCharge")
					    pSumItem.RefDebitFee = pSumItem.RefDebitFee + mRs("DebitFee") 
                    End If
				Else
					pSumItem.NormAmount = pSumItem.NormAmount + mRs("Amount")
					pSumItem.NormCount = pSumItem.NormCount + mRs("tVal")
					pSumItem.NormFees = pSumItem.NormFees + mRs("netpayFee_transactionCharge") + mRs("netpayFee_ratioCharge")
					pSumItem.NormLineFee = pSumItem.NormLineFee + mRs("netpayFee_transactionCharge")
					pSumItem.NormDebitFee = pSumItem.NormDebitFee + mRs("DebitFee")
					If mRs("BankPaid") Then
						pSumItem.BankPaidCount = pSumItem.BankPaidCount + TestNumVar(mRs("tVal"), 0, -1, 0)
						pSumItem.BankPaidAmount = pSumItem.BankPaidAmount + TestNumVar(mRs("Amount"), 0, -1, 0)
					End If
				End If
			  mRs.MoveNext	
			Loop
			mRs.Close
		End Function

		Function GetTransIDUpTo(ByVal xWhere, nAmount)
			If xWhere <> "" Then xWhere = " And " & xWhere
			xWhere = "CompanyID=" & mCompanyID & " And Currency=" & mCurrency & " And PayID LIKE ('%;" & IIF(mApply, 0, mPayID) & ";%')" & xWhere
			xWhere = "Select ID, UnsettledAmount, UnsettledInstallments From tblCompanyTransPass Where " & xWhere & " And CreditType IN(1,6,8) And PaymentMethod > " & PMD_MAXINTERNAL & " And deniedStatus=0 Order By ID Desc"
			mRs.Open xWhere, oleDbData, 0, 1
			Do While Not mRs.EOF
				If mRs("UnsettledInstallments")>0 Then nAmount = nAmount - (mRs("UnsettledAmount") / mRs("UnsettledInstallments"))
				If nAmount < 0 Then Exit Do
				GetTransIDUpTo = GetTransIDUpTo & mRs("ID") & ","
			  mRs.MoveNext
			Loop
			mRs.Close()
			If Len(GetTransIDUpTo) > 1 Then GetTransIDUpTo = Left(GetTransIDUpTo, Len(GetTransIDUpTo) - 1)
		End Function

		Function AddTransactions(xWhere)
			Dim sWhere, sWhereNot, xPayID, lTransID, bFirstUnpayIns
			oleDbData.CommandTimeout = 180
			xPayID = IIF(mApply, 0, mPayID)
			If xWhere <> "" Then xWhere = " And " & xWhere
			sWhere = "tblCompanyTransPass.CompanyID=" & mCompanyID & " And Currency=" & mCurrency & " And (tblCompanyTransPass.PayID LIKE ('%;" & xPayID & ";%')"
			If bUseImprov Then 'use improvments
				sWhereNot = " (deniedStatus=0 And CreditType<>8 And PaymentMethod > " & PMD_MAXINTERNAL & " And HandlingFee=0 And Interest=0)"
				AddCountTransactions sWhere & ") And " & sWhereNot & xWhere
				If mApply Then _
					oledbData.Execute "UPDATE tblCompanyTransPass SET PrimaryPayedID=" & mPayID & ", payID=';" & mPayID & ";', MerchantRealPD='" & mPayDate & "'" & _ 
				        " FROM tblCompanyTransPass Left Join tblLogImportEPA As ep ON(ep.TransID = tblCompanyTransPass.id And ep.Installment = 1) " & _
					    " WHERE " & sWhere & ") And " & sWhereNot & xWhere
				xWhere = " And (Not " & sWhereNot & ")" & xWhere
			End If
			If xPayID = 0 Then sWhere = sWhere & " OR DeniedStatus = " & DNS_SetInVar
			sWhere = sWhere & ")" & xWhere
			If mApply Then sWhere = sWhere & " And (p1.payID=0 Or p1.transAnsID Is Null)"
			sWhere = "SELECT tblCompanyTransPass.*, p1.id As IID, p1.InsID, p1.payID As IPayID, p1.MerchantPD As IMerchantPD, p1.Amount As IAmount, ep.IsPaid as BankPaid " & _
			    " FROM tblCompanyTransPass " & _
				" Left Join tblCompanyTransInstallments As p1 ON(p1.transAnsID = tblCompanyTransPass.id) " & _
				" Left Join tblLogImportEPA As ep ON(ep.TransID = tblCompanyTransPass.id And IsNull(p1.InsID, 1) = ep.Installment) " & _
				" Where " & sWhere & " Order By tblCompanyTransPass.ID Asc"
			mRs.Open sWhere, oleDbData, 3, 1 '0, 3
			Do While Not mRs.EOF
				If mRs("PaymentMethod") = PMD_RolRes Then
					If mRs("CreditType") = 0 Then
						mRRTransID = mRs("ID")
						mRRAmount = mRRAmount - mRs("Amount")
					Else
						mHasRows = True
						mRRRetAmount = mRRRetAmount + mRs("Amount")
						mRRRetCount = mRRRetCount + 1
					End If
				Else
					mHasRows = True
					If (lTransID <> mRs("ID")) Then bFirstUnpayIns = True
					If Not ( (mRs("PaymentMethod") = PMD_Admin) And (mPayID = 0) And (mRs("CreditType") = 1) And (mRs("Amount") > 100) And (mRs("InsertDate") < DateSerial(2012, 1, 1)) ) Then _
						GetAmountByPaymentMethod(mRs("PaymentMethod"), mRs("PaymentMethod_id")).AddTransaction mRs, mPayID, mPayDate, mPrime, mApply, bFirstUnpayIns
					If (TestNumVar(mRs("IPayID"), 0, -1, 0) = 0) Then bFirstUnpayIns = False	
				End If
				lTransID = mRs("ID")
			  mRs.MoveNext
			Loop
			mRs.Close
			oleDbData.CommandTimeout = 30
		End Function
		
		Property Get HasRows()
			HasRows = mHasRows
		End Property
		
		Public Property Get HasPaymentMethodTotal(lPM)
		    HasPaymentMethodTotal = Not IsEmpty(Me.PaymentMethodAmount(lPM))
		End Property 
		
		Public Property Get PaymentMethodTotal(lMin, lMax)
		    Dim i,pRet
		    Set pRet = New CAmount
		    For i = lMin To lMax
	            If Not IsEmpty(Me.PaymentMethodAmount(i)) Then pRet.AddAmount(Me.PaymentMethodAmount(i))
		    Next
    
            'If (lMin = PMD_CC_MIN And lMax = PMD_EC_MAX) Then
            '    For i = PMD_PP_MIN To PMD_PP_MAX
	        '        If Not IsEmpty(Me.PaymentMethodAmount(i)) Then pRet.AddAmount(Me.PaymentMethodAmount(i))
		    '    Next                             
            'End If   

		    Set PaymentMethodTotal = pRet
		End Property

		Public Property Get Totals()
		    Set Totals = PaymentMethodTotal(0, PMD_MAX)
		End Property
		
		Public Property Let RollingReserve(newVal)
		    If mRRState = 4 Then mRRAmount = -newVal
		End Property
		
		Public Property Get RollingReserve()
			If mPayID > 0 And Not mApply Then
				RollingReserve = mRRAmount
			ElseIf mRRState = 4 Then
			    RollingReserve = mRRAmount
			Else
				RollingReserve = 0
				If (mRRState = 2) Then
					Dim sSQL : sSQL = "Select TOP 1 ID From tblCompanyTransPass Where CompanyID=" & mCompanyID & _
					    " And PaymentMethod=" & PMD_RolRes & " And CreditType=0 And InsertDate < '" & DateAdd("m", -mSecPeriod, mPayDate) & "'"
					If TestNumVar(ExecScalar(sSQL, 0), 0, -1, 0) > 0 Then mSecFlags = mSecFlags And Not 1 Else mSecFlags = mSecFlags Or 1
				End If
				Dim RRAmt : RRAmt = PaymentMethodTotal(PMD_CC_MIN, PMD_EC_MAX).NormAmount '* (mPayPercent / 100)
				If (((mRRState = 1 OR mRRState = 2) And ((mSecFlags And 1) = 1)) OR mRRState = 3) And (Request("IncRR") = "1" Or Not mApply) And RRAmt > 0 Then
				    If mRRState = 1 Then
        		        RollingReserve = -(RRAmt * (mSecDeposit / 100))
				    Else
				        Dim nMax : nMax = (mSecKeepAmount - mCurRes)
        		        If nMax < 0 Then
        		            RollingReserve = 0
        		        Else 
        		            RollingReserve = (RRAmt * (mSecDeposit / 100))
        		            If nMax < RollingReserve Then RollingReserve = nMax
        		            RollingReserve = -RollingReserve
        		        End if
				    End If
				End If
			End If
		End Property
		
		Public Property Get ReserveReturn()
			ReserveReturn = mRRRetAmount
		End Property
		
		Public Property Get ReserveTotal()
			ReserveTotal = RollingReserve + mRRRetAmount
		End Property

    	Public Property Get TotalProcess()
			Set TotalProcess = PaymentMethodTotal(PMD_CC_MIN, PMD_EC_MAX)
            TotalProcess.AddAmount(BankFees)
		End Property

		Public Property Get Admin()
		    Set Admin = PaymentMethodTotal(PMD_Admin, PMD_Admin)
		End Property

		Public Property Get FeesAutoAndManual()
		    Set FeesAutoAndManual = PaymentMethodTotal(PMD_ManualFee, PMD_ManualFee)
			FeesAutoAndManual.AddAmount(PaymentMethodTotal(PMD_SystemFees, PMD_SystemFees))
		End Property

		Public Property Get BankFees()
		    Set BankFees = PaymentMethodTotal(PMD_BankFees, PMD_BankFees)
		End Property

        'Public Property Get WireTotal
        '    Dim pRet : Set pRet = TotalProcess
        '    pRet.ApplyMultiplyer((mPayPercent / 100))
        '    pRet.AddAmount(Admin)
        '    pRet.AddAmount(FeesAutoAndManual)
        '    ReserveTotal
		'	Set WireTotal = pRet
        'End Property

		Public Property Get TotalFeesAmount()
			TotalFeesAmount = Totals.TotalFees + Totals.HandlingFee
		End Property
    
		Public Property Get TotalProcessAmount()
			TotalProcessAmount = TotalProcess.TotalAmount
		End Property
		
    	Public Property Get TotalPayedAmount()
            TotalPayedAmount = (TotalProcessAmount * (mPayPercent / 100))
		End Property

		Property Get TotalAmount()
			TotalAmount = TotalPayedAmount + ReserveTotal + Admin.TotalAmount + FeesAutoAndManual.TotalAmount
		End Property

		Public Property Get TotalNotPayedAmount()
		    If mPayPercent = 100 Then
		        TotalNotPayedAmount = 0 : Exit Property
		    End If
			TotalNotPayedAmount = (TotalProcessAmount * (NotPayPercent / 100))
		End Property

		Public Property Get NotPayPercent()
		    NotPayPercent = 100 - mPayPercent
		End Property
		
		Public Property Get VATAmount()
			VATAmount = TotalFeesAmount * (mVAT + 1)
		End Property

		Public Property Get Total()
			Total = TotalAmount - VATAmount
		End Property

		
	End Class
	
	Function CalcCompanyPaymentTotal(xPayID, xCompanyID, xCurrency, xWhere)
		Set xPayment = New CPayment
		xPayment.InitByPayID xPayID, xCompanyID, xCurrency
		xPayment.AddTransactions xWhere
		If xPayID = 0 Then xPayment.AddSpecialAdminTransAmount
		Set CalcCompanyPaymentTotal = xPayment
	End Function

    Public Sub ClacDeletePayment(nPayID, nCompanyID, rowsOnly)
	    oledbData.execute "DELETE From tblCompanyTransPass WHERE CompanyID=" & nCompanyID & " And PrimaryPayedID=" & nPayID & " And TransSource_id IN(1, 2, 25)"
	    oledbData.execute "UPDATE tblCompanyTransPass SET PrimaryPayedID=0, payID=';0;', MerchantRealPD=Null, Interest=0 WHERE CompanyID=" & nCompanyID & " And PrimaryPayedID=" & nPayID & " And PaymentMethod<>" & PMD_RolRes & " And CreditType<>8"
	    oledbData.execute "UPDATE tblCompanyTransPass SET payID=Replace(Replace(payID, ';" & nPayID & ";', ';0;'), ';" & nPayID & ";', ';0;') WHERE CompanyID=" & nCompanyID & " And ID IN(Select TransAnsID From tblCompanyTransInstallments WHERE CompanyID=" & nCompanyID & " And payID=" & nPayID & ")"
	    oledbData.execute "UPDATE tblCompanyTransPass SET PrimaryPayedID=0 WHERE PrimaryPayedID=" & nPayID & " And CreditType=8"
	    oledbData.execute "UPDATE tblCompanyTransInstallments SET payID=0 WHERE CompanyID=" & nCompanyID & " And payID=" & nPayID
	    oledbData.execute "UPDATE tblCompanyTransFail SET PayID=0 WHERE CompanyID=" & nCompanyID & " And PayID=" & nPayID
	    oledbData.execute "UPDATE tblCompanyTransApproval SET PayID=0 WHERE CompanyID=" & nCompanyID & " And PayID=" & nPayID
	    oledbData.execute "UPDATE tblCCStorage SET PayID=0 WHERE CompanyID=" & nCompanyID & " And PayID=" & nPayID
        oleDbData.Execute "UPDATE tblCompanyTransPass Set OriginalTransID=0 Where OriginalTransID IN(Select ID From tblCompanyTransPass Where PrimaryPayedID=" & nPayID & ")"
	    oledbData.execute "DELETE From tblCompanyTransPass WHERE PrimaryPayedID=" & nPayID & " And PaymentMethod=" & PMD_RolRes
        If rowsOnly Then Exit Sub
		oledbData.execute "DELETE FROM tblTransactionPay WHERE ID=" & nPayID
		oledbData.execute "DELETE FROM tblWireMoney WHERE WireType=1 AND WireSourceTbl_id=" & nPayID
    End Sub

	Class AffiliateAmount
	    Dim Amount, FeePercent, CreditInfo
	    Public Property Get Total
	        If FeePercent = 0 Then 
	            Total = 0
	            Exit Property
	        End If    
	        Total = Amount * (FeePercent / 100)
	    End Property
	End Class

	Class AffiliateFeeItem
	    Dim PaymentMethod, CreditAmount
	    Dim Items()

		Public Sub Class_Initialize()
		    CreditAmount = 0 
			Redim Items(-1)
        End Sub

	    Public Function AddAmount(nPercent, nAmount, sCreditInfo)
	        Set pItem = New AffiliateAmount
	        Redim Preserve Items(UBound(Items) + 1)
	        Set Items(UBound(Items)) = pItem
	        pItem.Amount = nAmount : pItem.FeePercent = nPercent : pItem.CreditInfo = sCreditInfo
	        Set AddAmount = pItem
	    End Function
	    
	    Public Property Get Total()
	        Dim i, pRet
	        For i = 0 To UBound(Items)
	            pRet = pRet + Items(i).Total
	        Next
	        Total = pRet
	    End Property 
	End Class

	Class AffiliateTotalLine
	    Dim Text 
	    Dim Quantity 
	    Dim Amount
	    Public Property Get Total
	        Total = Quantity * Amount
	    End Property
	End Class

	Class AffiliateFees
	    Dim PaymentMethodAmount()
	    Dim PaymentLines()
        Dim Payment, AffiliateID, AFP_ID
        
		Public Sub Class_Initialize()
			Redim PaymentMethodAmount(PMD_EC_MAX)
			Redim PaymentLines(-1)
        End Sub

		Public Sub LoadLines()
            Set mRs = oledbData.Execute("Select * From tblAffiliatePaymentsLines Where AFPL_AFP_ID=" & AFP_ID)
            Do While Not mRs.EOF
    	        Set pItem = New AffiliateTotalLine
	            Redim Preserve PaymentLines(UBound(PaymentLines) + 1)
	            Set PaymentLines(UBound(PaymentLines)) = pItem
    	        pItem.Text = mRs("AFPL_Text") : pItem.Quantity = mRs("AFPL_Quantity") : pItem.Amount = mRs("AFPL_Amount")
             mRs.MoveNext
            Loop
            mRs.Close
		End Sub 
        
	    Public Function HasPaymentMethodAmount(lPM)
	        HasPaymentMethodAmount = Not IsEmpty(PaymentMethodAmount(lPM))
	    End Function
	    
	    Public Function GetPaymentMethodAmount(lPM)
	        If IsEmpty(PaymentMethodAmount(lPM)) Then
	            Set PaymentMethodAmount(lPM) = New AffiliateFeeItem
	            PaymentMethodAmount(lPM).PaymentMethod = lPM
	        End If    
	        Set GetPaymentMethodAmount = PaymentMethodAmount(lPM)
	    End Function
	    
	    Public Property Get SubTotal()
	        Dim i, pRet
	        For i = 0 To UBound(PaymentMethodAmount)
	            If Not IsEmpty(PaymentMethodAmount(i)) Then pRet = pRet + PaymentMethodAmount(i).Total
	        Next
	        SubTotal = pRet
	    End Property 
	    
	    Public Property Get Total()
	        Dim i, pRet
	        For i = 0 To UBound(PaymentLines)
	            If Not IsEmpty(PaymentLines(i)) Then pRet = pRet + PaymentLines(i).Total
	        Next
	        Total = pRet + SubTotal
	    End Property 
	    
	    Public Function UpdatePayment()
            Dim feeRatio, AFCompanyID : feeRatio = 0
            If Payment.Total <> 0 Then feeRatio = Total / Payment.Total
	        If AFP_ID <> 0 Then
                oleDbData.Execute("Update tblAffiliatePayments Set AFP_FeeRatio=" & Round(feeRatio, 2) & ", AFP_PaymentAmount=" & Round(Total, 2) & " Where AFP_ID=" & AFP_ID)
				UpdatePayment = False
	        Else
                oleDbData.Execute("Insert Into tblAffiliatePayments(AFP_Affiliate_ID, AFP_TransPaymentID, AFP_CompanyID, AFP_FeeRatio, AFP_PaymentAmount, AFP_PaymentCurrency)Values(" & _
                     AffiliateID & "," & Payment.mPayID & "," & Payment.mCompanyID & "," & Round(feeRatio, 2) & "," & Round(Total, 2) & "," & Payment.mCurrency & ")")
                AFP_ID = TestNumVar(ExecScalar("Select Max(AFP_ID) From tblAffiliatePayments Where AFP_Affiliate_ID=" & AffiliateID, ""), 0, -1, 0)
				oleDbData.Execute("INSERT INTO tblAffiliateFeeSteps(AFS_AffiliateID, AFS_UpToAmount, AFS_Currency, AFS_PercentFee, AFS_PaymentMethod, AFS_TransType, AFS_CompanyID, AFS_CalcMethod, AFS_SlicePercent, AFS_AFPID)" & _
					"Select AFS_AffiliateID, AFS_UpToAmount, AFS_Currency, AFS_PercentFee, AFS_PaymentMethod, AFS_TransType, AFS_CompanyID, AFS_CalcMethod, AFS_SlicePercent, " & AFP_ID & " From tblAffiliateFeeSteps Where AFS_AffiliateID=" & AffiliateID & " And AFS_AFPID is null And AFS_CompanyID=" & Payment.mCompanyID & " And AFS_Currency=" & Payment.mCurrency)
				UpdatePayment = True
            End If
        End Function
	End Class
	
    Function AddSteps(pRet, mRs, PmdID)
        Dim pItem, lineTotal, lStepTotalAmount, lineSubTotal, nPercentFee, nSlicePercent, oldPmdID
        Set pItem = pRet.GetPaymentMethodAmount(PmdID)
	    Set lineTotal = pRet.Payment.PaymentMethodTotal(PmdID, PmdID)
        lStepTotalAmount = 0
        If Not mRs.EOF Then 
            oldPmdID = mRs("AFS_PaymentMethod")
            Select Case mRs("AFS_CalcMethod")
            Case AFC_Volume
	            lineSubTotal = lineTotal.TotalAmount
            Case AFC_Fees
	            lineSubTotal = lineTotal.TotalFees
            Case AFC_MercSale_TermBuy
	            lineSubTotal = lineTotal.TotalProfit
            Case AFC_MercSale_AffiBuy
                lineSubTotal = lineTotal.TotalAmount
            'Case AFC_MercSale_TermBuySPercent
	        '    lineSubTotal = lineTotal.TotalProfit
            Case AFC_MercSale_AffiBuySPercent
                lineSubTotal = lineTotal.TotalAmount
            End Select
        End If    
        pItem.CreditAmount = lineSubTotal
        Do While Not mRs.EOF
            Dim sCreditInfo, nStepPercent, nStepAmount
            If oldPmdID <> mRs("AFS_PaymentMethod") Then Exit Do
            nPercentFee = mRs("AFS_PercentFee")
            nSlicePercent = (mRs("AFS_SlicePercent") / 100)
            nStepAmount = IIF(lineSubTotal > mRs("AFS_UpToAmount"), mRs("AFS_UpToAmount"), lineSubTotal) - lStepTotalAmount
            If mRs("AFS_CalcMethod") = AFC_MercSale_AffiBuy Then 
				sCreditInfo = "(" & lineTotal.TotalFeePercent & "% - " & FormatNumber(nPercentFee, 2, True) & "%) X " & FormatNumber(nSlicePercent * 100, 1, True) & "% = " & FormatNumber((lineTotal.TotalFeePercent - nPercentFee) * nSlicePercent, 2, True) & "%"
                nPercentFee = lineTotal.TotalFeePercent - nPercentFee
            ElseIf mRs("AFS_CalcMethod") = AFC_MercSale_AffiBuySPercent Then 
                nStepPercent = IIF(mRs("AFS_UpToAmount") < lineTotal.TotalFeePercent, mRs("AFS_UpToAmount"), lineTotal.TotalFeePercent)
				sCreditInfo = "(" & nStepPercent & "% - " & FormatNumber(nPercentFee, 2, True) & "%) X " & FormatNumber(nSlicePercent * 100, 1, True) & "% = " & FormatNumber((nStepPercent - nPercentFee) * nSlicePercent, 2, True) & "%"
                nPercentFee = nStepPercent - nPercentFee
                nStepAmount = lineSubTotal
            End If
            pItem.AddAmount nPercentFee * nSlicePercent, nStepAmount, sCreditInfo
            If lineSubTotal < mRs("AFS_UpToAmount") Then Exit Do
            lStepTotalAmount = mRs("AFS_UpToAmount")
          mRs.MoveNext
        Loop
    End Function

	Function AddAffiliatePMFees(pRet, companyId, transType)
        Dim mRs, i
        Set mRs = oledbData.Execute("Select * From tblAffiliateFeeSteps Where AFS_AffiliateID=" & pRet.AffiliateID & _
			" And AFS_CompanyID" & IIF(companyId = 0, " Is Null", "=" & companyId) & _
			" And AFS_Currency=" & pRet.Payment.mCurrency & " And AFS_AFPID" & IIF(xAFP_ID <> 0, "=" & xAFP_ID, " is null") & " And AFS_PaymentMethod <> 0 And AFS_TransType=" & transType & " Order By AFS_PaymentMethod Asc, AFS_UpToAmount Asc")
        For i = PMD_CC_MIN To PMD_MAX
            If pRet.Payment.HasPaymentMethodTotal(i) Then
                If Not mRs.EOF Then
                    Do While mRs("AFS_PaymentMethod") < i
                        mRs.MoveNext
                        If mRs.EOF Then Exit Do
                    Loop
                End If
                If Not mRs.EOF Then 
                    If Not pRet.HasPaymentMethodAmount(i) Then _
						AddSteps pRet, mRs, i
				End If
            End If
        Next
        mRs.Close
		Set mRs = Nothing
	End Function

	Function AddAffiliateNPMFees(pRet, companyId, transType)
        Dim mRs, i
		AddAffiliateNPMFees = False
        Set mRs = oledbData.Execute("Select * From tblAffiliateFeeSteps Where AFS_AffiliateID=" & pRet.AffiliateID & _
			" And AFS_CompanyID" & IIF(companyId = 0, " Is Null", "=" & companyId) & _
			" And AFS_Currency=" & pRet.Payment.mCurrency & " And AFS_AFPID" & IIF(xAFP_ID <> 0, "=" & xAFP_ID, " is null") & " And AFS_PaymentMethod = 0 And AFS_TransType=" & transType & " Order By AFS_PaymentMethod Asc, AFS_UpToAmount Asc")
        If Not mRs.EOF Then
            For i = PMD_CC_MIN To PMD_MAX
                If pRet.Payment.HasPaymentMethodTotal(i) Then
                    If Not pRet.HasPaymentMethodAmount(i) Then
                        AddSteps pRet, mRs, i
                        mRs.MoveFirst
                    End If
                End If
            Next
			AddAffiliateNPMFees = True
        End If
        mRs.Close
	End Function

	Function AddAffiliateFees(pRet, transType)
		AddAffiliatePMFees pRet, pRet.Payment.mCompanyID, transType
		If Not AddAffiliateNPMFees(pRet, pRet.Payment.mCompanyID, transType) Then
			AddAffiliatePMFees pRet, 0, transType
			AddAffiliateNPMFees pRet, 0, transType
		End If
	End Function

	Function CalcAffiliateFees(xAFP_ID, xPayment, xAffID)
        Dim pRet
        Set pRet = New AffiliateFees
        Set pRet.Payment = xPayment
        pRet.AffiliateID = xAffID : pRet.AFP_ID = xAFP_ID
        if xAFP_ID <> 0 Then pRet.LoadLines()
        AddAffiliateFees pRet, 0
        'AddAffiliateFees pRet, 1
        'AddAffiliateFees pRet, 2
        Set CalcAffiliateFees = pRet
	End Function
%>