<!--#include file="../include/common_sessionCheckCompany.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<%
nBillingCompanysID = dbText(request("BillingCompanysID"))
nBillingNumber = dbText(request("billingNumber"))
nInvoiceType = dbText(request("invoiceType"))

sWhere = ""
'Not admin need to check company owns this invoice
If Session("CompanyID") > 0 Then sWhere = "id_MerchantID=" & Session("CompanyID") & " AND "

sSQL="SELECT IsNull(payDate, id_InsertDate) payDate, payType, id_MerchantID, id_InsertDate, id_IsPrinted " &_
" FROM tblInvoiceDocument LEFT JOIN tblTransactionPay ON tblInvoiceDocument.ID=tblTransactionPay.InvoiceDocumentID" & _
" WHERE " & sWhere & " id_BillingCompanyID=" & nBillingCompanysID & " AND id_InvoiceNumber=" & nBillingNumber & " AND id_Type=" & nInvoiceType
set rsData2=oledbData.execute(sSQL)
If rsData2.EOF Then
    rsData2.close
    Set rsData2 = nothing
	response.write "No records found<br />"
	response.end
End if

sBillOriginalDisabled = ""
sBillOriginalText = ""
if rsData2("id_IsPrinted") then
	sBillOriginalDisabled = "disabled"
	sBillOriginalText = "(�����)"
end if
%>
<html>
<head>
	<title><%= COMPANY_NAME_1%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/MainIE.css" type="text/css" rel="stylesheet">
	<script language="JavaScript">
		function CheckForm()
		{
			if (document.frmBilling.chargeList.checked==false & document.frmBilling.billOriginal.checked==false & document.frmBilling.billCopy.checked==false)
			{
				alert('Please select what to print from the list');
				return false;
			}
			return true;
		}
		function printAction()
		{
			if (document.frmBilling.billOriginal.checked==true)
			{
				parent.fraBillactions.location.href='../include/common_billingShow_actions.asp?BillingCompanysID=<%= nBillingCompanysID %>&billingNumber=<%= nBillingNumber %>&invoiceType=<%= nInvoiceType %>';
				document.frmBilling.billOriginal.disabled = true;
				document.frmBilling.billOriginal.checked = false;
			}
			parent.fraBillprint.focus();
			parent.fraBillprint.print();
		}
		function printStatus()
		{
			document.frmBilling.bPrint.style.cursor = 'not-allowed';
			document.frmBilling.bPrint.disabled = true;
		}
	</script>
</head>
<body class="itext" dir="rtl" bgcolor="#f5f5f5" marginheight="0" marginwidth="0" leftmargin=0 topmargin=0>
<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" dir="rtl" align="center">
<tr>
	<td bgcolor="#00254a" align="right" colspan="8" style="padding:4px; padding-right:30px;">
		<span class="txt14b" style="color:white;">����� ������� ��'</span>
		<span class="txt13b" style="color:white;"><%= nBillingNumber %></span>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<span class="txt14b" style="color:white;">����� �����</span>
		<span class="txt13b" style="color:white;"><%= rsData2("id_InsertDate") %></span><br />
	</td>
</tr>
<tr><td height="10"></td></tr>
<tr>
	<form action="../include/common_billingShow_pages.asp?type=preview" name="frmBilling" id="frmBilling" method="post" target="fraBillprintPreview" onsubmit="return CheckForm();">
	<input type="Hidden" name="BillingCompanysID" value="<%= nBillingCompanysID %>">
	<input type="Hidden" name="billingNumber" value="<%= nBillingNumber %>">
	<input type="Hidden" name="InvoiceType" value="<%= nInvoiceType %>">
	<td width="28%" style="padding-right:25px" valign="top">
		<table border="0" cellspacing="0" cellpadding="1" width="90%">
		<tr>
			<td colspan="2" align="right" class="txt12" dir="rtl">
				<input <%= sBillOriginalDisabled %> type="checkbox" name="billOriginal" value="1" <% If sBillOriginalDisabled="" then %>onclick="printStatus();"<% End If %>> ������� ���� <%= sBillOriginalText %><br />
			</td>
		</tr>
		<tr>
			<td align="right" class="txt12" dir="rtl">
				<input type="checkbox" name="billCopy" value="1" onclick="printStatus();"> ������� ����<strong></strong>
			</td>
			<td align="left" class="txt12" dir="rtl">
				<select name="billCopyNum" class="inputData" onchange="printStatus();">
					<option value="1">1
					<option value="2">2
					<option value="3">3
					<option value="4">4
					<option value="5">5
				</select><br />
			</td>
		</tr>
		<tr>
			<td align="right" class="txt12" dir="rtl">
				<% If int(rsData2("payType"))=0 then %>
					<input type="checkbox" name="chargeList" value="1" onclick="printStatus();"> ����� ������<br />
				<% Else %>
					<input type="checkbox" name="chargeList" value="1" disabled style="display: none;"><br />
				<% End If %>
			</td>
			<td align="left" class="txt12" dir="rtl">
				<% If int(rsData2("payType"))=0 then %>
					<select name="chargeListNum" class="inputData" onchange="printStatus();">
						<option value="1">1
						<option value="2">2
						<option value="3">3
						<option value="4">4
						<option value="5">5
					</select><br />
				<% End If %>			
			</td>
		</tr>
		<tr><td height="10"></td></tr>
		<tr>
			<td colspan="2" align="right" class="txt12" dir="rtl">
				<input type="submit" name="action" value="����� ������ " class="button1" style="width:80px;">
				<input type="button" name="bPrint" id="bPrint" value="����" disabled  class="button1" style="cursor:not-allowed; width:40px;" onclick="printAction();"><br />		
			</td>
		</tr>
		</table>
	</td>
	</form>
	<%
	if int(rsData2("id_MerchantID")) > 0 then
		sSQL="SELECT companyLegalName, firstName, lastName, PaymentMethod, payingDates1," & _
		" payingDates2, payingDates3, PaymentMethod, PaymentPayeeName, PaymentBank," & _
		" PaymentBranch, PaymentAccount FROM tblCompany WHERE id=" & rsData2("id_MerchantID")
		set rsData=oledbData.execute(sSQL)
		%>
		<td width="1" bgcolor="#000000"></td>
		<td width="4%"></td>
		<td valign="top" width="32%">
			<table border="0" cellspacing="0" cellpadding="0" align="right">
			<tr>
				<td align="right" class="txt12" valign="top" width="65">�� ����:</td>
				<td align="right" class="txt12">
					<% if len(rsData("companyLegalName"))=<1 then %>
						(<%= dbTextShow(rsData("firstName")) & " " & dbTextShow(rsData("lastName")) %>)<br />
					<% Else  %>
						<%= dbTextShow(rsData("companyLegalName")) %><br />
					<% End If %>			
				</td>
			</tr>
			<tr>
				<td align="right" class="txt12">��� �����:</td>
				<td>
					<span class="txt11"><%= dbTextShow(rsData2("payDate")) %></span><br />
				</td>
			</tr>
			<tr>
				<td align="right" class="txt12">��� �����:</td>
				<td align="right" class="txt12">
					<%= rsData("PaymentMethod") %><br />
				</td>
			</tr>
			<tr>
				<td align="right" class="txt12">����� �����:</td>
				<td align="right" class="txt12">
					<% if len(trim(rsData("payingDates1")))=6 then %>
						� <span class="txt11"><%= mid(trim(rsData("payingDates1")),1,2) %></span> ��
						<span class="txt11"><%= mid(trim(rsData("payingDates1")),3,2) %></span> ����� �
						<span class="txt11"><%= mid(trim(rsData("payingDates1")),5,2) %></span><br />
					<% End If %>			
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right" class="txt12">
					<% if len(trim(rsData("payingDates2")))=6 then %>
						� <span class="txt11"><%= mid(trim(rsData("payingDates2")),1,2) %></span> ��
						<span class="txt11"><%= mid(trim(rsData("payingDates2")),3,2) %></span> ����� �
						<span class="txt11"><%= mid(trim(rsData("payingDates2")),5,2) %></span><br />
					<% End If %>			
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right" class="txt12">
					<% if len(trim(rsData("payingDates3")))=6 then %>
						� <span class="txt11"><%= mid(trim(rsData("payingDates3")),1,2) %></span> ��
						<span class="txt11"><%= mid(trim(rsData("payingDates3")),3,2) %></span> ����� �
						<span class="txt11"><%= mid(trim(rsData("payingDates3")),5,2) %></span><br />
					<% End If %>			
				</td>
			</tr>
			</table>
		</td>
		<td width="1" bgcolor="#000000"></td>
		<td width="4%"></td>
		<td valign="top">
			<table border="0" cellspacing="0" cellpadding="0" align="right">
			<tr>
				<td align="right" class="txt12" width="65">��� �����:</td>
				<td align="right" class="txt12">
					<%= dbTextShow(rsData("PaymentMethod")) %><br />
				</td>
			</tr>
			<tr>
				<td align="right" class="txt12">�����:</td>
				<td align="right" class="txt12">
					<%= dbTextShow(rsData("PaymentPayeeName")) %><br />
				</td>
			</tr>
			<tr>
				<td align="right" class="txt12">���� ���:</td>
				<td align="right" class="txt12">
					<%= dbTextShow(rsData("PaymentBank")) %><br />
				</td>
			</tr>
			<tr>
				<td align="right" class="txt12">���� ����:</td>
				<td align="right" class="txt12">
					<%= dbTextShow(rsData("PaymentBranch")) %><br />		
				</td>
			</tr>
			<tr>
				<td align="right" class="txt12">���� �����:</td>
				<td align="right" class="txt12">
					<%= dbTextShow(rsData("PaymentAccount")) %><br />		
				</td>
			</tr>
			</table>
		</td>
		<%
        rsData.close
        Set rsData = nothing
	Else
		%>
		<td valign="top" colspan="6"></td>
		<%
	End If
	%>
</tr>
<tr><td height="100%"></td></tr>
<tr><td bgcolor="#c5c5c5" colspan="8"></td></tr>
</table>
</body>
</html>
<%
rsData2.close
Set rsData2 = nothing
call closeConnection()
%>