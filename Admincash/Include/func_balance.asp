<%
'Old status contants
Const CBS_Cleared  = 0
Const CBS_InProcess = 1
Const CBS_Voided = 2

'wireType: 1="payment to merchants"  2="merchant make payments" 3="manual" 4="fee", 13 = "PendingCapture"
Private Function MapBalanceSourceTypes(oldType)
    Select Case oldType
        Case 1 : MapBalanceSourceTypes = "Wires.Settlement" 'PayedTransactions
        Case 2 : MapBalanceSourceTypes = "Wires.PaymentRequest" 'Merchant
        Case 3 : MapBalanceSourceTypes = "System.Admin" 'Admin
        Case 4 : MapBalanceSourceTypes = "Wires.SettlementFee" 'Fee
        'CreditCardDeposit = 5, - NOT IS USE
        'PaymentForPurchase = 6, - NOT IS USE
        Case 7 : MapBalanceSourceTypes = "System.Admin" 'WalletTransferToMerchant
        Case 8 : MapBalanceSourceTypes = "System.Admin" 'FeeWalletTransfer
        'EBankingDeposit = 9,  - NOT IS USE
        'PayoneerTransaction = 10, - NOT IS USE
        'PayoneerFee = 11,  - NOT IS USE
        Case 12 : MapBalanceSourceTypes = "System.Deposit" 'UserTr
        Case 13 : MapBalanceSourceTypes = "System.PedingCapture"
    End Select
End Function

Public Function fn_BalanceInsert(fCompany_id, fSourceTbl_id, fSourceType, fSourceInfo, fAmount, fCurrency, fStatus, fComment, fIsAddFee)
    Dim lAccountId : lAccountId = ExecScalar("Select Account_id from Data.Account Where Merchant_id=" & fCompany_id, 0)
    Dim status : status = IIF(fStatus = CBS_InProcess, 1, 0)
    Dim text : text = Trim(fSourceInfo)
    If (text <> "" And fComment <> "") Then text = text & " - "
    text = text & fComment
	sSQL = "SET NOCOUNT ON;Insert Into Data.AccountBalance(Account_id, BalanceSourceType_id, SourceID, CurrencyISOCode, Amount, TotalBalance, SystemText, IsPending) Values(" & _
            lAccountId & ", '" & MapBalanceSourceTypes(fSourceType) & "', " & fSourceTbl_id & ", '" & GetCurISO(fCurrency) & "', " & fAmount & ", 0, '" & Replace(text, "'", "''") & "', " & status & ");" & _
            ";SELECT @@IDENTITY AS NewID;SET NOCOUNT OFF"
	fn_BalanceInsert = ExecScalar(sSQL, "Null")
	If CBool(fIsAddFee) Then
		sSQL="SELECT WireFee, WireFeePercent FROM Setting.SetMerchantSettlement mcs WHERE mcs.Merchant_ID=" & fCompany_id & " And mcs.Currency_ID=" & fCurrency
		set rsFnData = oledbData.execute(sSQL)
		If NOT rsFnData.EOF Then
			MakePaymentsFee = rsFnData("WireFee") - (fAmount * (rsFnData("WireFeePercent") / 100))
			If CCur(MakePaymentsFee) > 0 Then
				'Insert fee line into balance
	            sSQL = "Insert Into Data.AccountBalance(Account_id, BalanceSourceType_id, SourceID, CurrencyISOCode, Amount, TotalBalance, SystemText, IsPending) Values(" & _
                        lAccountId & ", 'System.WithdrawalFee', " & fn_BalanceInsert & ", '" & GetCurISO(fCurrency) & "', " & -MakePaymentsFee & ", 0, '" & Replace(text, "'", "''") & "', 0)"
				oledbData.execute sSQL
			End if
		End if
		rsFnData.Close
		Set rsFnData = nothing
	End if

	'Dim sSQL, currencyName, sSourceInfoTmp, MakePaymentsFee
	'sSQL="INSERT INTO tblCompanyBalance(company_id, sourceTbl_id, sourceType, sourceInfo, amount, currency, status, comment) VALUES" & _
	'		" (" & fCompany_id & ", " & fSourceTbl_id & ", " & fSourceType & ", '" & dbText(fSourceInfo) & "', " & fAmount & "," & _
	'		" " & fCurrency & "," & fStatus & ", '" & fComment & "')"
	'oledbData.execute sSQL
    '
	'If CBool(fIsAddFee) Then
	'	sSourceInfoTmp = "Bank transfer"'IIF(fCurrency = 0, "����� ������", "Bank transfer")
	'	sSQL="SELECT WireFee, WireFeePercent FROM Setting.SetMerchantSettlement mcs WHERE mcs.Merchant_ID=" & fCompany_id & " And mcs.Currency_ID=" & fCurrency
	'	set rsFnData = oledbData.execute(sSQL)
	'	If NOT rsFnData.EOF Then
	'		MakePaymentsFee = rsFnData("WireFee") - (fAmount * (rsFnData("WireFeePercent") / 100))
	'		If CCur(MakePaymentsFee) > 0 Then
	'			'Insert fee line into balance
	'			sSQL="INSERT INTO tblCompanyBalance(company_id, sourceTbl_id, sourceType, sourceInfo, amount, currency, status, comment)" &_
	'			" VALUES(" & fCompany_id & ", 0, 4, '" & sSourceInfoTmp & "', " & -MakePaymentsFee & "," & fCurrency & ", 0, '')"
	'			oledbData.execute sSQL
	'		End if
	'	End if
	'	rsFnData.Close
	'	Set rsFnData = nothing
	'End if
End Function

'-----------------------------------------------------------------------------------
'	Show balance
'-----------------------------------------------------------------------------------

Class BalaceStatus
    Public CurrencyId, Current, Pending
    Public Sub Initialize()
        Current = 0 : Pending = 0
    End Sub
    Public Property Get Expected()
        Expected = Current + Pending 
    End Property

End Class

Public Function fn_GetBalances(fCompany_id)
    Dim sql, rsFnData, ret, curItem, curId
    sql =  "SELECT * From Data.AccountBalance Where AccountBalance_id IN " & _
	       "(SELECT Max(AccountBalance_id) FROM Data.AccountBalance Where " & _
            "Account_id IN (Select Account_id from Data.Account Where Merchant_id=" & fCompany_id & ") " & _
            "Group By Account_id, CurrencyISOCode, IsPending)"
    Set rsFnData = Server.CreateObject("Adodb.Recordset")
    rsFnData.Open sql, oledbData, 0, 1
    Set ret = Server.CreateObject("Scripting.Dictionary")
	Do While Not rsFnData.EOF
        curId = GetCurrIDByIso(rsFnData("CurrencyISOCode"))
        If ret.Exists(curId) Then 
            Set curItem = ret.Item(curId)
        Else 
            Set curItem = New BalaceStatus
            curItem.CurrencyId = curId
            Call ret.Add(curId, curItem)
        End If
        If rsFnData("IsPending") Then curItem.Pending = CCur(rsFnData("TotalBalance")) _
        Else curItem.Current = CCur(rsFnData("TotalBalance"))
        rsFnData.MoveNext
    Loop
    rsFnData.Close()
    Set fn_GetBalances = ret
End Function

Public Function fn_GetBalance(fCompany_id, fCurrency)
    fn_GetBalance = 0
    Dim list : Set list = fn_GetBalances(fCompany_id)
    If Not list.Exists(fCurrency) Then Exit Function
    fn_GetBalance = list(fCurrency).Expected
End Function

'-----------------------------------------------------------------------------------
'	Update balance status
'-----------------------------------------------------------------------------------
Public Function UpdateBalanceStatus(fCompany_id, fSourceTbl_id, fSourceType, nStatus)
	Dim rsFnData, oAmount, oCurrency, oStatus, oText, oCompanyBalanceID, sSQL
    Dim lAccountId : lAccountId = ExecScalar("Select Account_id from Data.Account Where Merchant_id=" & fCompany_id, 0)
	UpdateBalanceStatus = False
	Set rsFnData = Server.CreateObject("Adodb.Recordset")
	rsFnData.Open "Select * From Data.AccountBalance Where Account_Id=" & lAccountId & _
		" And BalanceSourceType_id='" & MapBalanceSourceTypes(fSourceType) & "' And SourceID=" & fSourceTbl_id, oledbData, 0, 1
	If Not rsFnData.EOF Then
		oAmount = CDbl(rsFnData("Amount"))
		oCurrency = GetCurrIDByIso(rsFnData("CurrencyISOCode"))
		oStatus = IIF(rsFnData("IsPending"), CBS_InProcess, CBS_Cleared)
		oText = rsFnData("SystemText")
		oCompanyBalanceID = rsFnData("AccountBalance_id")
	Else
		fSourceTbl_id = 0
	End if
	rsFnData.Close
	Set rsFnData = Nothing
	If fSourceTbl_id = 0 Then Exit function
	If nStatus = oStatus Then UpdateBalanceStatus = True : Exit function
    If oStatus <> CBS_InProcess Then Exit function
	Select case nStatus
	    Case CBS_InProcess 'does not support void or clear to process
            Exit Function
        Case CBS_Cleared 
            If oStatus = CBS_Voided Then Exit Function 'does not support void to clear
    		call fn_BalanceInsert(fCompany_id, fSourceTbl_id, fSourceType, oText, oAmount, oCurrency, CBS_Cleared, "", false)
    		call fn_BalanceInsert(fCompany_id, oCompanyBalanceID, 13, oText, -oAmount, oCurrency, CBS_InProcess, "", false) 'reverse pending row //System.PedingCapture
            Exit Function
        Case CBS_Voided
            If oStatus = CBS_Cleared Then Exit Function 'does not support clear to void
    		call fn_BalanceInsert(fCompany_id, fSourceTbl_id, fSourceType, oText, -oAmount, oCurrency, CBS_InProcess, "", false) 'cancel pending row
    End Select
    UpdateBalanceStatus = True
	'Select case nStatus
	'case CBS_Cleared 'does not support void to clear
	'	If oStatus = CBS_InProcess Then UpdateBalanceStatus = True
	'case CBS_InProcess
	'case CBS_Voided
	'	If oStatus = CBS_InProcess Then 
	'		call fn_BalanceInsert(fCompany_id, fSourceTbl_id, fSourceType, "Cancel - " & oText, -oAmount, oCurrency, CBS_Cleared, "", false)
	'		UpdateBalanceStatus = True
    '    Else 
	'	End if
	'End Select
	'if UpdateBalanceStatus then
	'	sSQL = "Update tblCompanyBalance Set status=" & nStatus & " Where CompanyBalance_ID=" & oCompanyBalanceID
	'	oledbData.execute sSQL
	'End if
End Function

%>