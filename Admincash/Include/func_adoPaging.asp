<%
'Open Recordset for Paging
Function openRsDataPaging(pageSizeValue)
	openRsDataPagingEx pageSizeValue, Request
End Function

Function WrapQueryForPaging(sourceQuery, startRow, endRow)
	wrapperQuery = "SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY %%orderFields%%) AS 'calculatedRowNumber' FROM (%%originalQuery%%) AS t1) AS t2 WHERE calculatedRowNumber BETWEEN %%startRow%% AND %%endRow%%"
	wrappedQuery = ""
	originalOrderFields = ""

	If InStr(1, LCase(sourceQuery), "order by") > 0 Then
		orderClauseStart = InStr(1, LCase(sourceQuery), "order by")
		orderClause = Mid(sourceQuery, orderClauseStart, (Len(sourceQuery) - orderClauseStart) + 1)
		originalOrderFields = Mid(orderClause, 10)
		sourceQuery = Mid(sourceQuery, 1, orderClauseStart - 1)
	Else
		firstFieldEnd = InStr(1, sourceQuery, ",")
		originalOrderFields = Mid(sourceQuery , 6, firstFieldEnd - 6) 
	End IF

	orderFieldsSplit = Split(originalOrderFields, ",")
	orderFieldsJoin = ""
	
	For Each currentField in orderFieldsSplit
		currentFieldSplit = Split(currentField, ".")
		If UBound(currentFieldSplit) = 1 Then
			currentFieldJoin = "t1." + currentFieldSplit(1)
			orderFieldsJoin = orderFieldsJoin + currentFieldJoin + ","
		Else
			orderFieldsJoin = orderFieldsJoin + currentField + ","
		End If
	Next
	
	orderFields = Mid(orderFieldsJoin, 1, Len(orderFieldsJoin) - 1)

	wrappedQuery = Replace(wrapperQuery, "%%originalQuery%%", sourceQuery)
	wrappedQuery = Replace(wrappedQuery, "%%orderFields%%", orderFields)
	wrappedQuery = Replace(wrappedQuery, "%%startRow%%", startRow)
	wrappedQuery = Replace(wrappedQuery, "%%endRow%%", endRow)

	WrapQueryForPaging = wrappedQuery
End Function

Function GetTotalCount(sourceQuery)
	totalCountQuery = "SELECT COUNT(*) " & Mid(sourceQuery, InStr(1, LCase(sourceQuery), "from"))
	If InStr(1, LCase(totalCountQuery), "order by") > 0 Then
		orderClauseStart = InStr(1, LCase(totalCountQuery), "order by")
		totalCountQuery = Mid(totalCountQuery, 1, orderClauseStart - 1)
	End IF	
	set recordSet = oledbData.execute(totalCountQuery)
	totalCount = CLng(recordSet(0))
	'If totalCount > 1000 Then totalCount = 1000
	GetTotalCount = totalCount
End Function

totalRecords = 0
recordsInPage = 0
totalPages = 0
currentPage = 0
Function openRsDataPagingEx(pageSizeValue, Rq)
	session("PageSize") = pageSizeValue
	currentPage = 1
	If trim(Rq("page")) <> "" And isNumeric(trim(Rq("page"))) then currentPage = CInt(trim(Rq("page")))
	session("ActualPage") = currentPage
	totalRecords = GetTotalCount(sSQL)	
	recordsInPage = pageSizeValue
	totalPages = Int(totalRecords / recordsInPage)
	If totalRecords mod recordsInPage > 0 Then totalPages = totalPages + 1
	startRow = ((currentPage - 1) * recordsInPage) + 1
	endRow = (startRow + recordsInPage) - 1	
	sSQL = WrapQueryForPaging(sSQL, startRow, endRow)

	'response.Write("totalRecords=" + cstr(totalRecords) + "<br/>")
	'response.Write("totalPages=" + cstr(totalPages) + "<br/>")
	'response.Write("recordsInPage=" + cstr(recordsInPage) + "<br/>")
	'response.Write("currentPage=" + cstr(currentPage) + "<br/>")
	'response.Write("startRow=" + cstr(startRow) + "<br/>")
	'response.Write("endRow=" + cstr(endRow) + "<br/>")
	'response.Write("sSQL=" + cstr(sSQL) + "<br/>")
	'response.Write("queryString=" + request.QueryString() + "<br/>")
	'response.End

	'startProc = now
	'Response.Write(sSQL)
	rsData.Open sSQL, oledbData, 3, 1
	'endProc = now
	'response.Write(endProc-startProc)	
	'response.Write("Records fetched=" + cstr(rsData.RecordCount)	+ "<br/>")
End Function

Function showPaging(pageUrl, lang, queryString)
	sStyle = "direction:ltr;"
	sMsgAlign = "left"
	sButtonsAlign = "right"
	sMsgTxt1 = "Records "
	sMsgTxt2 = "of"
	sMsgTxt3 = " Records shown"
	If Left(queryString & " ", 1)<>"&" Then sTemp="&" Else sTemp=""
	
	If totalPages = 1 then
		Response.Write("<span class=""txt14"">" & totalRecords & "</span><span class=""txt15"">" & sMsgTxt3 & "</span>")
	ElseIf totalPages > 1 Then
		%>
		<table width="100%" align="center" border="0" cellpadding="1" cellspacing="0" style="<%= sStyle %>">
		<tr>
			<td align="<%= sMsgAlign %>" class="txt15" style="<%= sStyle %>">
				 <%= sMsgTxt1 & " <span class=""txt14"">" & ((currentPage - 1) * recordsInPage) + 1 & "-" & IIF((currentPage * recordsInPage) > totalRecords, totalRecords, (currentPage * recordsInPage))	& "</span> " & sMsgTxt2 & " <span class=""txt14"">" & totalRecords & "</span><br />" %>
			</td>
			<td width="12"></td>
			<td>
				<table border="0" align="<%= sButtonsAlign %>" cellspacing="1" cellpadding="0" style="<%= sStyle %>">
				<tr>
					<%
					if currentPage > 1 then
						%>
						<td class="paging_PageNevigation alwaysActive" onmouseout="this.style.border='1px solid #d7d7d7'"
						 	onmouseover="this.style.border='1px solid black'"
						 	onclick="location.href='<%= pageUrl %>?page=1<%= sTemp & queryString %>';">&lt;&lt;</td>
						<td class="paging_PageNevigation alwaysActive" onmouseout="this.style.border='1px solid #d7d7d7'"
							onmouseover="this.style.border='1px solid black'" 
							onclick="location.href='<%= pageUrl %>?page=<%= (currentPage-1) & sTemp & queryString %>';">&lt;</td>
						<%
					end if
					for nPage=1 to totalPages
						if int(nPage) >= int(currentPage)-5 AND int(nPage) <= int(currentPage)+5 then
							if trim(nPage) = trim(currentPage) then
								response.write "<td class=""paging_currentPage"">" & nPage & "</td>"
							Else
								response.write "<td class=""alwaysActive paging_otherPages"" onmouseout=""this.style.border='1px solid #d7d7d7'"" onmouseover=""this.style.border='1px solid black'"" onclick=""location.href='" & pageUrl & "?page=" & nPage & sTemp & queryString & "';"">" & nPage & "</td>"
							end if
						end if
					next
					if int(currentPage) < totalPages then
						%>
						<td class="paging_PageNevigation alwaysActive" onmouseout="this.style.border='1px solid #d7d7d7'" 
							onmouseover="this.style.border='1px solid black'" 
							onclick="location.href='<%= pageUrl %>?page=<%= currentPage+1 & sTemp & queryString %>';">&gt;</td>
						<td class="paging_PageNevigation alwaysActive" onmouseout="this.style.border='1px solid #d7d7d7'" 
							onmouseover="this.style.border='1px solid black'" 
							onclick="location.href='<%= pageUrl %>?page=<%= totalPages & sTemp & queryString %>';">&gt;&gt;</td>
						<%
					end if
					%>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<%
	End If
End Function
%>