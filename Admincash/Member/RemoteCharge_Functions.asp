<%
Dim bUseOldTerminal : bUseOldTerminal = True
'---------------------------------------------------------------------
'	insert pass transaction
'---------------------------------------------------------------------
' transactionTypeId = request source id
Function insertPassedTransaction(transactionIdOutParam, insertDateOutParam, companyId, fraudDetectionLog_id, transactionTypeId, iMethodType, creditTypeId, customerId, customerIp, amount, transCurrcency, ccTypeEngString, payments, orderNumber, payFor, productId, comment, terminalNumber, approvalNumber, isTestOnly, PaymentMethod, pDate, cardId, referringUrl, debitCompanyId, transPayerInfo_id, transPaymentMethod_id, PayerID, is3DSecure, deviceId, OriginalTransID)
	Dim sTempChr, xBin, paymentId, xBinFraud, i
	If PaymentMethod >= PMD_CC_UNKNOWN Then xBin = ExecScalar("Select BINCountry From tblCreditCard Where ID=" & cardId, "--") Else xBin = "--"
	If bUseOldTerminal Then netpayFeeTransactionCharge = FormatNumber(GetTransactionFees(netpayFeeRatioCharge, companyId, PaymentMethod, X_OCurrency, amount, xBin, creditTypeId), 2, True, False, False) _
	Else netpayFeeTransactionCharge = FormatNumber(GetTransactionFeesEx(netpayFeeRatioCharge, companyId, PaymentMethod, X_OCurrency, amount, creditTypeId, terminalNumber), 2, True, False, False)
	debitFeeTransactionCharge = FormatNumber(GetTransactionDebitFees(debitCompanyId, terminalNumber, PaymentMethod, transCurrcency, amount, creditTypeId, pDate, 0, netpayFeeRatioCharge, 0), 2, True, False, False)
	netpayFeeRatioCharge = FormatNumber(netpayFeeRatioCharge, 2, True, False, False)
	sInsertDate = now()

	sTempChr = "0"
	If Not bIsNetpayTerminal Then sTempChr = "X"
	If int(creditTypeId) = 8 AND int(payments) > 1 AND trim(sTempChr) = "0" Then
		For i = 1 To int(payments)
			paymentId = paymentId & ";" & sTempChr
		Next
		paymentId = paymentId & ";"
	Else
		paymentId = ";" & sTempChr & ";"
	End If
	If fraudDetectionLog_id <> "" And fraudDetectionLog_id <> "0" Then _
		xBinFraud = ExecScalar("Select ReturnBinCountry From Log.FraudDetection Where FraudDetection_id=" & fraudDetectionLog_id, "--")
	If IsEmpty(CTP_Status) Then CTP_Status = 0
	MerchantPD = GetTransPayDate(companyId, Now, creditTypeId)
	
	' 20090224 Tamir - Save recurring series&chargenumber in transpass table (for remote data pulling)
	nRecurringSeries = "NULL"
	nRecurringChargeNumber = "NULL"
	If trim(request("RequestSource"))="20" And TestNumVar(Request("RecurringCharge"), 1, 0, 0)>0 Then
		sSQL="SELECT rc_Series, rc_ChargeNumber FROM tblRecurringCharge WHERE ID=" & Request("RecurringCharge")
		Set rsData2=oledbData.Execute(sSQL)
		If Not rsData2.EOF Then
			nRecurringSeries = rsData2("rc_Series")
			nRecurringChargeNumber = rsData2("rc_ChargeNumber")
		End If
		rsData2.Close
	End If
	transactionIdOutParam = -1
	Dim nUnsettledAmount, nUnsettledInstallments
	If UCase(paymentId)=";X;" Then
		nUnsettledAmount = 0
	ElseIf creditTypeId = 0 Then
		nUnsettledAmount = -amount
	Else
		nUnsettledAmount = amount
	End If
	If UCase(paymentId) = ";X;" Then
		nUnsettledInstallments = 0
	ElseIf creditTypeId=6 Then
		nUnsettledInstallments = 1
	Else
		nUnsettledInstallments = payments
	End If

    sRefTrans = "NULL"

	sSQL="SET NOCOUNT ON;INSERT INTO tblCompanyTransPass " & _
		"(companyID, insertDate, fraudDetectionLog_id, TransSource_id, payID, CreditType, CustomerID, IPAddress," & _
		" Amount, Currency, Payments, OrderNumber, PayforText, MerchantProduct_id, Comment, TerminalNumber, ApprovalNumber," & _
		" isTestOnly, PD, MerchantPD, PaymentMethod_id, PaymentMethodID, PaymentMethod, paymentMethodDisplay, " & _
		" referringUrl, DebitCompanyID, payerIdUsed, OriginalTransID, NetpayFee_RatioCharge, netpayFee_transactionCharge," & _
		" DebitReferenceCode, DebitReferenceNum, AcquirerReferenceNum, OCurrency, OAmount, IPCountry, CTP_Status, DebitFee, RecurringSeries, RecurringChargeNumber," & _
		" UnsettledAmount, UnsettledInstallments, RefTrans, MobileDevice_id, transPayerInfo_id, transPaymentMethod_id, Is3DSecure, DeniedAdminComment) " &_
		" VALUES(" & companyId & ",Convert(datetime, '" & sInsertDate & "', 103)," & fraudDetectionLog_id & "," & transactionTypeId & "," & _
		" LEFT('" & paymentId & "', 305), " & creditTypeId & ", " & customerId & ", LEFT('" & customerIp & "', 50)," & _
		" " & FormatNumber(amount, 2, -1, 0, 0) & ", " & transCurrcency & ", " & payments & ", LEFT('" & dbText(orderNumber) & "', 100)," & _
		" LEFT('" & dbText(payFor) & "', 100), " & IIF(TestNumVar(productId, 0, -1, 0) > 0, productId, "Null") & ", LEFT('" & dbText(comment) & "', 500), LEFT('" & terminalNumber & "', 10), LEFT('" & approvalNumber & "', 50)," & _
		" " & isTestOnly & ", Convert(datetime, '" & pDate & "', 103), Convert(datetime, '" & MerchantPD & "', 103), " & iMethodType & ", " & cardId & "," & PaymentMethod & "," & _
		" LEFT('" & ccTypeEngString & "', 50), LEFT('" & dbText(referringUrl) & "', 500), " & debitCompanyId & "," & _
		" LEFT('" & PayerID & "', 10), " & OriginalTransID & ", " & NetpayFeeRatioCharge & ", " & netpayFeeTransactionCharge & "," & _
		" LEFT('" & X_DebitReferenceCode & "', 40), Left('" & X_DebitReferenceNum & "', 40), " & IIF(X_AcquirerReferenceNum<> "", "Left('" & X_AcquirerReferenceNum & "', 40)", "Null") & ", " & X_OCurrency & ", " & X_OAmount & ", LEFT('" & xBinFraud & "', 2)," & _
		" " & CTP_Status & ", " & debitFeeTransactionCharge & ", " & nRecurringSeries & ", " & nRecurringChargeNumber & "," & _
		" " & nUnsettledAmount & ", " & nUnsettledInstallments & ", " & sRefTrans & "," & IIF(deviceId <> "", deviceId, "Null") & _
		"," & IIF(transPayerInfo_id <> "", transPayerInfo_id, "Null") & "," & IIF(transPaymentMethod_id <> "", transPaymentMethod_id, "Null") & "," & IIF(is3DSecure, "1", "0") & ",'" & Replace(X_DeniedAdminComment, "'", "''") & "');" & _
		"SELECT SCOPE_IDENTITY();SET NOCOUNT OFF;"

	oledbData.CommandTimeout = 180
	'On Error Resume Next
	Err.Clear
	Set rsData2 = oledbData.execute(sSQL)
	If Err.Number <> 0 Then 
		GlobalSaveToFile = True
		ExecSQL "-- BEGIN PASS TRANSACTION " & Now & vbCrLf & "DECLARE @TransID int;"
		ExecSQL Replace(sSQL, "SCOPE_IDENTITY", "@TransID=SCOPE_IDENTITY")
		transactionIdOutParam = "@TransID"
		insertDateOutParam = Now
	Else
		transactionIdOutParam = CLng(rsData2(0))
		insertDateOutParam = sInsertDate
	End If
	oledbData.CommandTimeout = 30
	rsData2.Close()
	Set rsData2 = nothing
	On Error Goto 0

	'If PaymentMethod >= PMD_CC_MIN And PaymentMethod <= PMD_CC_MAX Then
	'	sSQL="INSERT INTO tblCompanyTransTracking(ctt_MerchantID, ctt_TransInsertDate, ctt_TransPaymentMethod," & _
	'	" ctt_TransCurrency, ctt_TransAmount, ctt_TransCreditType, ctt_TransIsPending, ctt_TransInstallments," & _
	'	" ctt_TransIP, ctt_CardCountry, ctt_CardNumber256, ctt_CardMonth, ctt_CardYear) VALUES(" & _
	'	" " & companyId & ", '" & sInsertDate & "', " & PaymentMethod & ", " & transCurrcency & "," & _
	'	" " & amount & ", " & creditTypeId & ", 0, " & payments & ", LEFT('" & customerIp & "', 20)," & _
	'	" '" & Trim(ccBINCountry) & "', dbo.GetEncrypted256('" & TestStrVar(sCCardNum_Encrypt, 0, 25, "") & "')," & _
	'	" " & TestNumVar(X_ccExpMM, 1, 12, 0) & ", " & TestNumVar(X_ccExpYY, 1, 0, 0) Mod 2000+2000 & ")"
	'	ExecSQL sSQL
	'ElseIf PaymentMethod >= PMD_EC_MIN And PaymentMethod <= PMD_EC_MAX Then
	'	sSQL="INSERT INTO tblCompanyTransTracking(ctt_MerchantID, ctt_TransInsertDate, ctt_TransPaymentMethod," & _
	'	" ctt_TransCurrency, ctt_TransAmount, ctt_TransCreditType, ctt_TransIsPending, ctt_TransInstallments," & _
	'	" ctt_TransIP, ctt_CheckRoutingNumber256, ctt_CheckAccountNumber256) VALUES(" & companyId & ", '" & sInsertDate & "'," & _
	'	" " & PaymentMethod & ", " & transCurrcency & ", " & FormatNumber(amount, 2, -1, 0, 0) & "," & _
	'	" " & creditTypeId & ", 0, " & payments & ", LEFT('" & customerIp & "', 20)," & _
	'	" dbo.GetEncrypted256('" & ckabaEnc & "'), dbo.GetEncrypted256('" & ckacctEnc & "'))"
	'	ExecSQL sSQL
	'End If

	'Create recurring series if needed
	Call CreateRecurringSeries(1, transactionIdOutParam)
	
	If creditTypeId="8" AND payments > 1 Then
        'ExecSQL "Exec InsertInstallments " & transactionIdOutParam & ", '" & MerchantPD & "'"
        ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_GenerateOldInstallments & ",'', 3)")
        ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_GenerateInstallments & ",'', 3)")
    End If

	'Post processing to add fees
	ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & transactionIdOutParam & "," & PET_FeesTransaction & ",'', 3)")

	If Not isTestOnly Then ExecSql("Update [Track].[MerchantActivity] Set DateFirstTransPass=getDate() Where DateFirstTransPass is null And Merchant_id=" & companyId)

	If GlobalSaveToFile Then 
		transactionIdOutParam = 0
		ExecSQL "GO" & vbCrLf & "--END PASS TRANSACTION" & vbCrLf
		GlobalSaveToFile = False
	End If
End Function

'---------------------------------------------------------------------
'	insert failed transaction
'---------------------------------------------------------------------
' transactionTypeId = request source id
' transTypeId = transaction type
Function insertFailedTransaction(transactionIdOutParam, insertDateOutParam, companyId, fraudDetectionLog_id, transactionTypeId, transTypeId, creditTypeId, customerId, customerIp, amount, transCurrcency, ccTypeEngString, payments, orderNumber, replyCode, debitDeclineReason, payFor, productId, comment, terminalNumber, PaymentMethod, iMethodType, cardId, isTestOnly, referringUrl, debitCompanyId, transPayerInfo_id, transPaymentMethod_id, PayerID, is3DSecure, deviceId)
	Dim xBinFraud
	netpayFeeTransactionCharge = 0
	If ExecScalar("Select ChargeFail From tblDebitCompanyCode Where DebitCompanyID=" & debitCompanyId & " And Code='" & replyCode & "'", False) Then
		If bUseOldTerminal Then netpayFeeTransactionCharge = GetUsageFees(companyId, PaymentMethod, transCurrcency, xBin, 1) _
		Else netpayFeeTransactionCharge = GetUsageFeesEx(companyId, PaymentMethod, transCurrcency, xBin, terminalNumber, 1)
	End If
	GetTransactionDebitUsageFees debitCompanyId, terminalNumber, PaymentMethod, transCurrcency, amount, creditTypeId, 0, debitFee
	debitFee = FormatNumber(debitFee, True, False, False)
	If fraudDetectionLog_id <> "" And fraudDetectionLog_id <> "0" Then _
		xBinFraud = ExecScalar("Select ReturnBinCountry From Log.FraudDetection Where FraudDetection_id=" & fraudDetectionLog_id, "--")
	sInsertDate = Now()
	sSQL = "SET NOCOUNT ON;INSERT INTO tblCompanyTransFail(companyID, insertDate, fraudDetectionLog_id, TransSource_id, TransType, CreditType, CustomerID, IPAddress," & _
	" Amount, Currency, Payments, OrderNumber, replyCode, DebitDeclineReason, PayforText, MerchantProduct_id, Comment, TerminalNumber, PaymentMethod, PaymentMethod_id, PaymentMethodID, paymentMethodDisplay, isTestOnly," & _
	" referringUrl, DebitCompanyID, payerIdUsed, DebitReferenceCode, DebitReferenceNum, netpayFee_transactionCharge, debitFee, IPCountry, ctf_JumpIndex, IsGateway, MobileDevice_id, transPayerInfo_id, transPaymentMethod_id, Is3DSecure) VALUES (" & IIF(UseLocalID, "@curID, ", "") & companyId & ", '" & sInsertDate & "'," & _
	" " & fraudDetectionLog_id & ", " & transactionTypeId & ", " & transTypeId & ", " & creditTypeId & ", " & customerId & ", Left('" & customerIp & "', 50)," & _
	" " & formatnumber(amount, 2, -1, 0, 0) & ", " & TestNumVar(transCurrcency, 0, MAX_CURRENCY, 1) & ", " & payments & ", Left('" & dbText(orderNumber) & "', 100), Left('" & dbText(replyCode) & "', 50), " & IIF(debitDeclineReason <> "",  "Left('" & dbText(debitDeclineReason) & "', 500)", "Null") & "," & _
	" LEFT('" & dbText(PayFor) & "', 100), " & IIF(TestNumVar(productId, 0, -1, 0) > 0, productId, "Null") & ", Left(N'" & DBText(comment) & "', 500), Left('" & terminalNumber & "', 10), " & PaymentMethod & ", " & iMethodType & "," & _
	" " & cardId & ", Left('" & ccTypeEngString & "', 50), " & isTestOnly & ", Left(N'" & dbText(referringUrl) & "', 500), " & debitCompanyId & ", Left('" & PayerID & "', 10)," & _
	" Left('" & X_DebitReferenceCode & "', 40), Left('" & X_DebitReferenceNum & "', 40)," & netpayFeeTransactionCharge & ", " & debitFee & ", Left('" & xBinFraud & "', 2), " & TestNumVar(Session(X_TransINC), 0, 255, 0) & ", " & IIF(bIsNetpayTerminal, 0, 1) & "," & IIF(deviceId <> "", deviceId, "Null") & _
	"," & IIF(transPayerInfo_id <> "", transPayerInfo_id, "Null") & "," & IIF(transPaymentMethod_id <> "", transPaymentMethod_id, "Null") & "," & IIF(is3DSecure, "1", "0") & ");" &_
	"SELECT SCOPE_IDENTITY() AS NewTransFailID;SET NOCOUNT OFF;"

	set rsData2 = oledbData.execute(sSQL)
	If NOT rsData2.EOF Then
		transactionIdOutParam = rsData2("NewTransFailID")
		insertDateOutParam = sInsertDate
	Else
		transactionIdOutParam = 0
		insertDateOutParam = ""
	End if
	rsData2.close
	Set rsData2 = Nothing
End function

Function IsDayInRange(strDateRng, xDay)
	Dim xFromDay, xToDay
	xFromDay = TestNumVar(Mid(strDateRng, 1, 2), 0, 31, 0)
	xToDay = TestNumVar(Mid(strDateRng, 3, 2), 0, 31, 0)
	if (xFromDay <= xDay) And (xToDay >= xDay) Then	_
		IsDayInRange = TestNumVar(Mid(strDateRng, 5, 2), 0, -1, 0) _
	Else IsDayInRange = -1
End Function

Sub RecalcMerchantPO(nCompanyID, nPayID)
	Dim tRs, transRs, i, nDay, payDate
	Set tRs = oledbData.Execute("Select PayingDaysMarginInitial, PayingDaysMargin, payingDates1, payingDates2, payingDates3, Cast(ma.DateFirstTransPass as datetime) as DateFirstTransPass From tblCompany" & _
		" Left Join [Track].[MerchantActivity] ma ON (ma.Merchant_id = tblCompany.ID) Where ID=" & nCompanyID)
	If Not tRs.EOF Then
		If (tRs("PayingDaysMarginInitial") > 0) And (DateDiff("d", IIf(IsNull(tRs("DateFirstTransPass")), Now, tRs("DateFirstTransPass")), Now) < tRs("PayingDaysMarginInitial")) Then
			oledbData.Execute("Update tblCompanyTransPass Set MerchantPD=DateAdd(d, " & tRs("PayingDaysMarginInitial") & ", InsertDate) " & _
				" Where CompanyID=" & nCompanyID & " And PrimaryPayedID=" & nPayID)
		ElseIf tRs("PayingDaysMargin") > 0 Then
			oledbData.Execute("Update tblCompanyTransPass Set MerchantPD=DateAdd(d, " & tRs("PayingDaysMargin") & ", InsertDate) " & _
				" Where CompanyID=" & nCompanyID & " And PrimaryPayedID=" & nPayID)
		Else
			Set transRs = oledbData.Execute("Select ID, InsertDate From tblCompanyTransPass Where CompanyID=" & nCompanyID & " And PrimaryPayedID=" & nPayID)
			Do While Not transRs.EOF
				nDay = Day(transRs("InsertDate"))
				For i = 1 To 3
					xRet = IsDayInRange(tRs("payingDates" & i), nDay)
					If xRet > -1 Then
						payDate = DateSerial(Year(transRs("InsertDate")), Month(transRs("InsertDate")) + 1, xRet)
						oledbData.Execute("Update tblCompanyTransPass Set MerchantPD='" & payDate & "' Where ID=" & transRs("ID"))
						Exit For
					End If
				Next
			 transRs.MoveNext
			Loop
			transRs.Close()
			OleDbData.Execute("Update tblCompanyTransInstallments Set MerchantPD=DateAdd(month, IsNull(InsID, 0), IsNull(tblCompanyTransPass.MerchantPD, 0))" & _
				" From tblCompanyTransInstallments Left Join tblCompanyTransPass ON(tblCompanyTransInstallments.transAnsID = tblCompanyTransPass.ID)" & _
				" Where tblCompanyTransPass.CompanyID=" & nCompanyID & " And PrimaryPayedID=" & nPayID)
		End If
	End If
	tRs.Close
End Sub

Function GetTransPayDate(cmpID, xTransDate, xCreditType)
	Dim tRs, i, xRet
	If int(xCreditType) = 0 Then
		GetTransPayDate = xTransDate
		Exit Function
	End if
	GetTransPayDate = DateSerial(1901, 1, 1)
	Set tRs = oledbData.Execute("Select PayingDaysMarginInitial, PayingDaysMargin, payingDates1, payingDates2, payingDates3, Cast(ma.DateFirstTransPass as datetime) as DateFirstTransPass From tblCompany" & _
		" Left Join [Track].[MerchantActivity] ma ON (ma.Merchant_id = tblCompany.ID) Where ID=" & cmpID)
	If Not tRs.EOF Then
		If (tRs("PayingDaysMarginInitial") > 0) And (DateDiff("d", IIf(IsNull(tRs("DateFirstTransPass")), Now, tRs("DateFirstTransPass")), Now) < tRs("PayingDaysMarginInitial")) Then
			GetTransPayDate = DateAdd("d", tRs("PayingDaysMarginInitial"), xTransDate)
		ElseIf tRs("PayingDaysMargin") > 0 Then 
			GetTransPayDate = DateAdd("d", tRs("PayingDaysMargin"), xTransDate)
		Else
			On Error Resume Next
			nDay = Day(xTransDate)
			For i = 1 To 3
				xRet = IsDayInRange(tRs("payingDates" & i), nDay)
				If xRet > -1 Then
					GetTransPayDate = DateSerial(Year(xTransDate), Month(xTransDate) + 1, xRet)
					'Month(xTransDate) + IIf(xRet < nDay, 1, 0)
					Exit For
				End If	
			Next
			On Error Goto 0
		End If	
	End If
	tRs.Close
End Function 

'---------------------------------------------------------------------
'	Move From Pending transaction, by Udi Azulay 07-04-2010
'---------------------------------------------------------------------
Function MovePendingTransaction(sWhere, nRes)
    Dim lChTransID, cmpRs, X_Email, lPendingID : lChTransID = 0
	Set iRs = oleDbData.Execute("SELECT tblCompanyTransPending.*, tblCompanyChargeAdmin.isPendingReply, tblCompanyChargeAdmin.PendingReplyUrl, " & _
		" tblCreditCard.email ccEmail, tblCheckDetails.Email chekEmail" & _
		" FROM tblCompanyTransPending" & _
		" LEFT Join tblCompanyChargeAdmin ON (tblCompanyChargeAdmin.company_id = tblCompanyTransPending.CompanyID)" & _
		" Left Join tblCreditCard ON(tblCreditCard.ID = tblCompanyTransPending.CreditCardID)" & _
		" Left Join tblCheckDetails ON(tblCheckDetails.ID = tblCompanyTransPending.CheckDetailsID)" & _
		" Where " & sWhere & " ORDER BY companyTransPending_id Desc")
	If Not iRs.EOF Then
	    lPendingID = iRs("companyTransPending_id")
		If iRs("PaymentMethod") >= PMD_EC_MIN And iRs("PaymentMethod") <= PMD_EC_MAX Then xOldMethod = 2 Else xOldMethod = 1
		X_DebitReferenceCode = iRs("DebitReferenceCode")
        X_DebitReferenceNum = iRs("DebitReferenceNum")
        X_AcquirerReferenceNum = iRs("AcquirerReferenceNum")
		X_Email = IIF(IsNull(iRs("CreditCardID")), iRs("chekEmail"), iRs("ccEmail"))
		If (nRes = "000") Then
			X_OCurrency = iRs("trans_currency") : X_OAmount = iRs("trans_amount")
			bIsNetpayTerminal = ExecScalar("Select isNetpayTerminal From tblDebitTerminals Where terminalNumber='" & iRs("TerminalNumber") & "'", False)
			call insertPassedTransaction(lChTransID, "", iRs("CompanyID"), iRs("FraudDetectionLog_id"), iRs("transactionSource_id"), xOldMethod, _
				iRs("trans_creditType"), iRs("CustomerID"), iRs("IPAddress"), iRs("trans_amount"), iRs("trans_currency"), _
				iRs("PaymentMethodDisplay"), iRs("trans_payments"), iRs("trans_order"), _
				iRs("PayforText"), iRs("MerchantProduct_id"), iRs("Comment"), iRs("TerminalNumber"), iRs("DebitApprovalNumber"), IIF(iRs("isTestOnly"), 1, 0), iRs("PaymentMethod"), "", _
				iRs("PaymentMethodID"), "", iRs("DebitCompanyID"), iRs("transPayerInfo_id"), iRs("transPaymentMethod_id"), 0, iRs("Is3dSecure"), iRs("MobileDevice_id"), iRs("trans_originalID"))
			If trim(X_Email) <> "" Then
				Set cmpRs = oleDbData.Execute("Select IsSendUserConfirmationEmail, IsMerchantNotifiedOnPass From tblCompany Where ID=" & iRs("CompanyId"))
				If Not cmpRs.EOF Then
					If IIF(IsNull(cmpRs("IsSendUserConfirmationEmail")), False, cmpRs("IsSendUserConfirmationEmail")) Then _
						ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & lChTransID & "," & PET_InfoEmailSendClient & ",'', 3)")
					If IIF(IsNull(cmpRs("IsMerchantNotifiedOnPass")), False, cmpRs("IsMerchantNotifiedOnPass")) Then _
						ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & lChTransID & "," & PET_InfoEmailSendMerchant & ",'', 3)")
				End If
				cmpRs.Close()
			End If
		Else
			call insertFailedTransaction(lChTransID, "", iRs("CompanyID"), iRs("FraudDetectionLog_id"), iRs("transactionSource_id"), _
				iRs("trans_type"), iRs("trans_creditType"), iRs("CustomerID"), iRs("IPAddress"), iRs("trans_amount"), _
				iRs("trans_currency"), iRs("PaymentMethodDisplay"), iRs("trans_payments"), iRs("trans_order"), nRes, "", _
				iRs("PayforText"), iRs("MerchantProduct_id"), iRs("Comment"), iRs("TerminalNumber"), iRs("PaymentMethod"), xOldMethod, _
				iRs("PaymentMethodID"), IIF(iRs("isTestOnly"), 1, 0), "", iRs("DebitCompanyID"), iRs("transPayerInfo_id"), iRs("transPaymentMethod_id"), 0, iRs("Is3dSecure"), iRs("MobileDevice_id"))
			If trim(X_Email) <> "" Then
				Set cmpRs = oleDbData.Execute("Select IsMerchantNotifiedOnFail From tblCompany Where ID=" & iRs("CompanyId"))
				If Not cmpRs.EOF Then
					If IIF(IsNull(cmpRs("IsMerchantNotifiedOnFail")), False, cmpRs("IsMerchantNotifiedOnFail")) Then _
						ExecSql("Insert Into EventPending(TransFail_id, EventPendingType_id, Parameters, TryCount)Values(" & lChTransID & "," & PET_InfoEmailSendMerchant & ",'', 3)")
				End If
				cmpRs.Close()
			End If
		End If
        oleDbData.Execute "Update Data.Cart Set TransPending_id=Null" & IIF(nRes = "000", ", TransPass_id=" & lChTransID, "") & " Where TransPending_id=" & lPendingID
		oleDbData.Execute "Delete From EventPending Where TransPending_id=" & lPendingID
		oleDbData.Execute "Delete From tblCompanyTransPending Where " & sWhere
		oleDbData.Execute "Insert Into tblLogPendingFinalize (PendingID, TransPassID, TransFailID) Values(" & lPendingID & "," & IIF(nRes = "000", lChTransID, "null") & "," & IIF(nRes <> "000", lChTransID, "null") & ")"
		If (iRs("isPendingReply") And Trim(iRs("PendingReplyUrl")) <> "") Then SendTransResponse IIF(nRes = "000", "tblCompanyTransPass", "tblCompanyTransFail"), lChTransID, iRs("PendingReplyUrl"), iRs("trans_order"), iRs("companyTransPending_id")
		'oledbData.Execute "Update tblPublicPayCostumerData Set transID = " & lChTransID & ", transLocation='" & IIF(nRes = "000", "pass", "fail") & "' Where transID=" & iRs("companyTransPending_id") & " And transLocation='pending'"
	End If
	iRs.Close()		
	MovePendingTransaction = lChTransID	
End Function

' 20081104 Tamir
' Creating recurring series.
' nTransType: 0=pre-auth, 1=charge
Function CreateRecurringSeries(nTransType, nTransID)
	nTransID = trim(nTransID)
	If TestNumVar(nTransType, 0, 1, -1) >= 0 And TestNumVar(nTransID, 1, 0, 0) > 0 And TestNumVar(nRecurringCount, 1, 0, 0) > 0 Then 
		sSQL = "EXEC RecurringCreateSeriesEx " & nTransType & ", " & nTransID & ", '" & saRecurring(1) & "', '" & X_Comment & "'"
		Dim rsData : Set rsData = oledbData.Execute(sSQL)
		Dim nSeries : nSeries = rsData(0)
		Dim i
		For i = 2 To nRecurringCount
			sSQL = "EXEC RecurringExtendSeriesEx " & nSeries & ", '" & saRecurring(i) & "'"
			oledbData.Execute sSQL
		Next
		rsData.Close
	End If
End Function


'---------------------------------------------------------------------
'	Change Transaction Status
'---------------------------------------------------------------------

Function CopyPassTrans(iRs, xPayID, xPayDate, nStatus, nAmount, netpayFee_chbCharge, netpayFee_ClrfCharge)
	If nStatus = 5 Then netpayFee_chbCharge = 0
	Dim nUnsettledAmount, nUnsettledInstallments
	If UCase(xPayID)=";X;" Then
		nUnsettledAmount = 0
	ElseIf iRs("CreditType") = 0 Then
		nUnsettledAmount = -nAmount
	ElseIf nStatus=6 Then
		nUnsettledAmount = -nAmount
	Else
		nUnsettledAmount = nAmount
	End If
	If UCase(xPayID) = ";X;" Then
		nUnsettledInstallments = 0
	Else
		nUnsettledInstallments = 1
	End If
	Dim sSQL : sSQL = ""
	sSQL="INSERT INTO tblCompanyTransPass(CompanyID, OriginalTransID, TransSource_id, CustomerID, InsertDate, PrimaryPayedID, PayID, DeniedDate, DeniedStatus, IPAddress," &_
		" Amount, Currency, Payments, CreditType, OrderNumber, Interest, Comment, DeniedPrintDate, DeniedSendDate, TerminalNumber, paymentMethodId," & _
		" paymentMethodDisplay, PaymentMethod, OCurrency, OAmount, DebitReferenceCode, DebitReferenceNum, netpayFee_chbCharge, netpayFee_ClrfCharge, UnsettledAmount, UnsettledTransactions, TransPayerInfo_id, TransPaymentMethod_id, MobileDevice_id) VALUES (" & IIF(UseLocalID, "@curID, ", "") &_
		iRs("CompanyID") & "," & iRs("ID") & "," & iRs("TransSource_id") &_
		"," & iRs("CustomerID") & ", GetDate(), " & xPayID & ", ';" & xPayID & ";" &_
		"','" & iRs("DeniedDate") & "', " & nStatus & ", '" & iRs("IPAddress") &_
		"'," & nAmount & ", " & iRs("Currency") & ", 1, " & IIf(nStatus = 6, 0, iRs("CreditType")) &_
		",'" & Replace(iRs("OrderNumber"), "'", "''") &_
		"', " & iRs("Interest") & ", '" & Replace(iRs("Comment"), "'", "''") & "', '" & iRs("DeniedPrintDate") & "', '" & iRs("DeniedSendDate") & "', '" & Replace(iRs("TerminalNumber"), "'", "''") & "', " & iRs("PaymentMethodID") & ", '" &	Replace(iRs("paymentMethodDisplay"), "'", "''") & "'" & _
		"," & iRs("PaymentMethod") & "," & iRs("OCurrency") & "," & nAmount & ",'" & Replace(iRs("DebitReferenceCode"), "'", "''") & "','" & Replace(IIF(IsNull(iRs("DebitReferenceNum")), "", iRs("DebitReferenceNum")), "'", "''") & "'," & netpayFee_chbCharge & "," & netpayFee_ClrfCharge & _
		", " & nUnsettledAmount & ", " & nUnsettledInstallments & "," & _
		IIF(iRs("TransPayerInfo_id") <> "", iRs("TransPayerInfo_id"), "Null") & "," & IIF(iRs("TransPaymentMethod_id") <> "", iRs("TransPaymentMethod_id"), "Null") & "," & _
		IIF(iRs("MobileDevice_id") <> "", iRs("MobileDevice_id"), "Null") & ")"
	'Response.Write(sSQL)
	oledbData.Execute sSQL
End Function

Function ChargeBackTransDB(transId, reason, reasonCode)
	Dim procRet
	procRet = CLng(ExecScalar("DECLARE @TodayDate AS DATETIME = GETDATE(); Exec CreateCHB " & transId & ",@TodayDate, '" & Replace(reason, "'", "''") & "'," & TestNumVar(reasonCode, 0, -1, 0), 0))
	If procRet > 0 Then ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & transId & "," & PET_EmailChargeBack & ",'', 3)")
	ChargeBackTransDB = procRet
End Function

Function PhotocopyTransDB(transId, reason, reasonCode)
	Dim procRet
	procRet = CLng(ExecScalar("DECLARE @TodayDate AS DATETIME = GETDATE(); Exec RequestPhotocopyCHB " & transId & ",@TodayDate, '" & Replace(reason, "'", "''") & "'," & TestNumVar(reasonCode, 0, -1, 0), 0))
	If procRet > 0 Then ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & transId & "," & PET_EmailPhotoCopy & ",'', 3)")
	PhotocopyTransDB = procRet
End Function

Function ChargeBackTrans(nAmount, bClarifyOnly, sWhere)
	Dim bUpdate, trnasID, netpayFee_chbCharge, netpayFee_ClrfCharge, DebitFeeChb, bNewSt, nPaymentMethod, nRefAmount, nRefundsCharge
	Dim sCardNumber, nMonth, nYear '20090104 Tamir
	Set iRs = Server.CreateObject("Adodb.Recordset")
	iRs.Open "Select tblCompanyTransPass.*, dbo.GetDecrypted256(CCard_Number256) As CardNumber, ExpMM, ExpYY, BINCountry FROM tblCompanyTransPass LEFT JOIN tblCreditCard ON (tblCompanyTransPass.CreditCardID = tblCreditCard.ID) Where " & sWhere, oleDbData, 0, 1
	If Not iRs.EOF Then
		If nAmount > iRs("Amount") Then
			iRs.Close()
			Exit Function
		End If
		sCardNumber = iRs("CardNumber")
		nMonth = iRs("ExpMM") : nYear = iRs("ExpYY")
		trnasID = iRs("ID")
		nPaymentMethod = iRs("PaymentMethod")
		If iRs("CreditType") <> 0 Then
			bUseOldTerminal = TestNumVar(ExecScalar("Select IsUsingNewTerminal From tblCompany Where ID=" & iRs("companyID"), 0), 0, -1, 0) = 0
			If bUseOldTerminal Then ChargeBackTrans = GetChbFees(iRs("companyID"), iRs("PaymentMethod"), iRs("OCurrency"), iRs("BINCountry"), netpayFee_chbCharge, netpayFee_ClrfCharge) _
			Else ChargeBackTrans = GetChbFeesEx(iRs("companyID"), iRs("PaymentMethod"), iRs("OCurrency"), iRs("BINCountry"), iRs("TerminalNumber"), netpayFee_chbCharge, netpayFee_ClrfCharge)
			nRefAmount = TestNumVar(ExecScalar("Select Sum(OAmount) From tblCompanyTransPass Where CreditType=0 And OriginalTransID=" & trnasID & " And DeniedStatus=0", 0), 0, -1, 0)
			If nAmount = 0 Then nAmount = iRs("Amount")
			If nAmount > 0 Then
				If(nAmount > (iRs("Amount") - nRefAmount)) Then
					nRefundsCharge = IIF(nRefAmount < nAmount, False, True)
					bNewSt = DNS_WasWorkedOut
					bUpdate = True
				End If
        		If bClarifyOnly Then
				    If iRs("PrimaryPayedID") > 0 Then
    					bNewSt = DNS_DupWasWorkedOut
					    CopyPassTrans iRs, 0, Now, DNS_DupFoundValid, 0, 0, netpayFee_ClrfCharge
					    netpayFee_chbCharge = 0 : netpayFee_ClrfCharge = 0
    				Else
    					bNewSt = DNS_SetFoundValid
    				    netpayFee_chbCharge = 0
				    End If
				    DebitFeeChb = 0
				    bUpdate = True : ChargeBackTrans = True
        		Else
				    If nAmount > (iRs("Amount") - nRefAmount) Then 'sign read refunds
					    Set rRs = OleDbData.Execute("Select * From tblCompanyTransPass Where CreditType=0 And OriginalTransID=" & trnasID & " And DeniedStatus=0")
					    Do While Not rRs.EOF
						    oleDbData.Execute "Update tblCompanyTransPass Set DeniedStatus=" & DNS_ChbRefundRTrans & " Where ID=" & rRs("ID")
					      rRs.MoveNext
					    Loop
					    rRs.Close()
					    If nRefundsCharge Then CopyPassTrans iRs, 0, Now, DNS_ChbRefundCTrans, nRefAmount, netpayFee_chbCharge, netpayFee_ClrfCharge _
					    Else CopyPassTrans iRs, 0, Now, DNS_ChbRefundCTrans, nRefAmount, 0, 0
					    nAmount = nAmount - nRefAmount
				    End If
				    If (nAmount > 0) And ChargeBackTrans Then
					    If (iRs("PrimaryPayedID") > 0) Or (nAmount <> iRs("Amount")) Then
						    bNewSt = DNS_WasWorkedOut
					        If iRs("deniedstatus") = DNS_SetFoundValid Or iRs("deniedstatus") = DNS_SetFoundValidRefunded Or iRs("deniedstatus") = DNS_DupWasWorkedOut Then
						        CopyPassTrans iRs, 0, Now, DNS_DupFoundUnValid, nAmount, netpayFee_chbCharge, 0
						        netpayFee_chbCharge = 0 : netpayFee_ClrfCharge = "netpayFee_ClrfCharge"
					        Else
						        CopyPassTrans iRs, 0, Now, DNS_DupFoundUnValid, nAmount, netpayFee_chbCharge, netpayFee_ClrfCharge
						        netpayFee_chbCharge = 0 : netpayFee_ClrfCharge = 0
					        End If
					    Else
						    bNewSt = DNS_UnSetInVar
					    End If
					    bUpdate = True
				    End If
				    GetTransactionDebitFees iRs("DebitCompanyID"), iRs("TerminalNumber"), iRs("PaymentMethod"), iRs("Currency"), iRs("Amount"), iRs("CreditType"), "", 1, 0, DebitFeeChb
				End If    
			Else
				bUpdate = False : ChargeBackTrans = True
			End If
		Else	
			ChargeBackTrans = False
		End If
	Else 
		trnasID	= 0
	End If
	iRs.Close
	If bUpdate And ChargeBackTrans Then
		sSQL="UPDATE tblCompanyTransPass SET DeniedDate=GetDate(), deniedstatus=" & bNewSt & ", netpayFee_chbCharge=" & netpayFee_chbCharge & ", netpayFee_ClrfCharge=" & netpayFee_ClrfCharge & ", DebitFeeChb=" & DebitFeeChb & " WHERE id=" & trnasID
		oledbData.Execute sSQL
		If Not bClarifyOnly Then
		    sSQL = "INSERT INTO tblLogTransPass(ltp_ActionType, ltp_Transaction, ltp_User, ltp_Username, ltp_InsertDate, ltp_IP)" & _
		    " SELECT 0, " & trnasID & ", ID, su_Username, GetDate(), '" & Request.ServerVariables("REMOTE_ADDR") & "'" & _
		    " FROM tblSecurityUser WHERE su_Username='" & PageSecurity.Username & "'"
		    oledbData.Execute sSQL
		End If    
	End if
End Function

Function ClearChargeBack(sTransID)
	ClearChargeBack = False
	Set iRs = oleDbData.Execute("Select * From tblCompanyTransPass Where ID=" & sTransID)
	If Not iRs.EOF Then
		If (iRs("DeniedStatus") = DNS_UnSetInVar) And (iRs("PrimaryPayedID") <= 0) Then 
			oleDbData.Execute "Update tblCompanyTransPass Set DeniedStatus=0, netpayFee_chbCharge=0, netpayFee_ClrfCharge=0 Where ID=" & sTransID
			ClearChargeBack = True
		ElseIf (iRs("DeniedStatus") = DNS_SetFoundValid Or iRs("DeniedStatus") = DNS_SetFoundValidRefunded) And (iRs("PrimaryPayedID") <= 0) Then 'clear photocopy
			oleDbData.Execute "Update tblCompanyTransPass Set DeniedStatus=0, netpayFee_chbCharge=0, netpayFee_ClrfCharge=0 Where ID=" & sTransID
			ClearChargeBack = True
		ElseIf (iRs("DeniedStatus") = DNS_DupFoundValid) And (iRs("PrimaryPayedID") <= 0) Then 
			xChbCount = TestNumVar(ExecScalar("Select Count(ID) From tblCompanyTransPass Where OriginalTransID=" & iRs("OriginalTransID") & " And DeniedStatus>0", 0), 0, -1, 0)
			If xChbCount = 1 Then oleDbData.Execute "Update tblCompanyTransPass Set DeniedStatus=0 Where ID=" & iRs("OriginalTransID")
			oleDbData.Execute "Delete From tblCompanyTransPass Where ID=" & sTransID
			ClearChargeBack = True
		ElseIf (iRs("DeniedStatus") = DNS_DupFoundUnValid) And (iRs("PrimaryPayedID") <= 0) Then 
			xChbCount = TestNumVar(ExecScalar("Select Count(ID) From tblCompanyTransPass Where OriginalTransID=" & iRs("OriginalTransID") & " And DeniedStatus>0", 0), 0, -1, 0)
			If xChbCount = 1 Then oleDbData.Execute "Update tblCompanyTransPass Set DeniedStatus=0 Where ID=" & iRs("OriginalTransID")
			oleDbData.Execute "Delete From tblCompanyTransPass Where ID=" & sTransID
			ClearChargeBack = True
		End If
	End If
	iRs.Close()
	'If ClearChargeBack Then
	'	sSQL = "INSERT INTO tblLogTransPass(ltp_ActionType, ltp_Transaction, ltp_User, ltp_Username, ltp_InsertDate, ltp_IP)" & _
	'	" SELECT 1, " & sTransID & ", ID, su_Username, GetDate(), '" & Request.ServerVariables("REMOTE_ADDR") & "'" & _
	'	" FROM tblSecurityUser WHERE su_Username='" & PageSecurity.Username & "'"
	'	oledbData.Execute sSQL
	'End If
End Function

Sub SetTransFraud(sTransID, bSet)
    Dim sMerchantId : sMerchantId = ExecScalar("Select CompanyID From tblCompanyTransPass Where ID=" & sTransID, 0) 
    ExecSql("Update tblCompanyTransPass Set IsFraudByAcquirer=" & IIF(bSet, "1", "0") & " Where ID=" & sTransID)
    ExecSql("Insert Into Trans.TransHistory(TransHistoryType_id, TransPass_id, Merchant_id, InsertDate, IsSucceeded, Description)Values(7, " & sTransID & "," & sMerchantId &", getDate(), 1, '" & IIF(bSet, "Set", "Cancel" ) & " fraud by " & PageSecurity.Username & "')")
End Sub
%>