<%
'----------------------------------------------------------------------------
'	Const
'----------------------------------------------------------------------------
Const HOTDB_ENABLED=False
Const HOTDB_INTERVAL_MINUTES=60
Const HOTDB_DURATION_SECONDS=600
Const HOTDB_PREVENTION_SECONDS=90

Const SMTP_SERVER="192.168.0.5"
Dim IS_PRODUCTION, DOMAIN_TEXT_1, COMPANY_NAME_1, REPORT_URL, MERCHANT_URL, PROCESS_URL, DEVCENTER_URL, SHOW_HEBREW, PARTNERS_URL

IS_PRODUCTION = False

Select case LCase(Trim(session("Identity")))
	Case "netpay", "netpayintl"
		DOMAIN_TEXT_1 = "netpay-intl.com"
		COMPANY_NAME_1 = "Netpay"
		REPORT_URL = "https://adminreports.netpay-intl.com/"
		MERCHANT_URL = "https://merchants.netpay-intl.com/"
		PARTNERS_URL = "https://partners.netpay-intl.com/"
		PROCESS_URL = "https://process.netpay-intl.com/"
		DEVCENTER_URL = "https://devcenter.netpay-intl.com/"
		SHOW_HEBREW = True
	Case "testnetpay"
		DOMAIN_TEXT_1 = "netpay-intl.com"
		COMPANY_NAME_1 = "Netpay"
		REPORT_URL = "https://adminreports.netpay-intl.com/"
		MERCHANT_URL = "https://testmerchants.netpay-intl.com/"
		PARTNERS_URL = "https://testpartners.netpay-intl.com/"
		PROCESS_URL = "https://testprocess.netpay-intl.com/"
		DEVCENTER_URL = "https://testdevcenter.netpay-intl.com/"
		SHOW_HEBREW = True
	Case "stagenetpay"
		DOMAIN_TEXT_1 = "netpay-intl.com"
		COMPANY_NAME_1 = "Netpay"
		REPORT_URL = "https://adminreports.netpay-intl.com/"
		MERCHANT_URL = "https://stagemerchants.netpay-intl.com/"
		PARTNERS_URL = "https://stagepartners.netpay-intl.com/"
		PROCESS_URL = "https://stageprocess.netpay-intl.com/"
		DEVCENTER_URL = "https://stagedevcenter.netpay-intl.com/"
		SHOW_HEBREW = True		
	Case "pbtpay"
		DOMAIN_TEXT_1 = "pbtpay.eu"
		COMPANY_NAME_1 = "PBTPay"
		REPORT_URL = "https://adminreports.pbtpay.eu/"
		MERCHANT_URL = "https://merchants.pbtpay.eu/"
		PARTNERS_URL = "https://partners.pbtpay.eu/"
		PROCESS_URL = "https://process.pbtpay.eu/"
		DEVCENTER_URL = "https://devcenter.pbtpay.eu/"
		SHOW_HEBREW = True
	Case "jubba"
		DOMAIN_TEXT_1 = "jubba.me"
		COMPANY_NAME_1 = "Jubba"
		REPORT_URL = "https://adminreports.jubba.me/"
		MERCHANT_URL = "https://merchants.jubba.me/"
		PARTNERS_URL = "https://partners.jubba.me/"
		PROCESS_URL = "https://process.jubba.me/"
		DEVCENTER_URL = "https://devcenter.jubba.me/"
		SHOW_HEBREW = True
	Case "yomane"
		DOMAIN_TEXT_1 = "yomane.com"
		COMPANY_NAME_1 = "YoManE"
		REPORT_URL = "https://adminreports.yomane.com/"
		MERCHANT_URL = "https://merchants.yomane.com/"
		PARTNERS_URL = "https://partners.yomane.com/"
		PROCESS_URL = "https://process.yomane.com/"
		DEVCENTER_URL = "https://devcenter.yomane.com/"
		SHOW_HEBREW = True
	Case "obl"
		DOMAIN_TEXT_1 = "obl.me"
		COMPANY_NAME_1 = "OBL"
		REPORT_URL = "https://adminreports.obl.me/"
		MERCHANT_URL = "https://merchants.obl.me/"
		PARTNERS_URL = "https://partners.obl.me/"
		PROCESS_URL = "https://process.obl.me/"
		DEVCENTER_URL = "https://devcenter.obl.me/"
		SHOW_HEBREW = True
	Case "demoobl"
		DOMAIN_TEXT_1 = "demoobl.me"
		COMPANY_NAME_1 = "DemoOBL"
		REPORT_URL = "https://adminreports.demoobl.me/"
		MERCHANT_URL = "https://merchants.demoobl.me/"
		PARTNERS_URL = "https://partners.demoobl.me/"
		PROCESS_URL = "https://process.demoobl.me/"
		DEVCENTER_URL = "https://devcenter.demoobl.me/"
		SHOW_HEBREW = True
	Case "local"
		DOMAIN_TEXT_1 = "Netpay-intl.com"
		COMPANY_NAME_1 = "YoManE"
		REPORT_URL = "http://localhost/AdminReports/"
		MERCHANT_URL = "http://localhost/MerchantControlPanel/"
		PARTNERS_URL = "http://localhost/PartnerControlPanel/"
		PROCESS_URL = "http://localhost:83/processWebSite/"
		DEVCENTER_URL = "http://localhost/DevCenter/"
		SHOW_HEBREW = True
End Select
%>