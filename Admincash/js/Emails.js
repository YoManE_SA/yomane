﻿
/// <reference path="js/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js" />

var counterFor_checkSelectedRB = 0;
$(document).ready(function () {
	if (($('#btn_userInfo').attr("maxlength") == (-1)) || ($('#btn_userInfo').attr("maxlength") == (0))) { $('#seperator_userInfo').css("display", "none"); $('#btn_userInfo').css("display", "none"); $('#btn_userInfo').attr("onclick", "return false;"); }

	if ($('#updp_deleteAssignedMail').text() == '') { $('#updp_deleteAssignedMail').css('display', 'none'); }

	$('#ddl_merchant').change(function () {
		if ($('#ddl_merchant').val() != '') {
			if ($('#ddl_merchant').val() == "Other") {
				$('#txt_merchant_text').css("display", "inline-block");
				$('.chb').css("display", "inline-block");
				counterFor_checkSelectedRB = 0;
			}
			else {
				$('#btn_assing_merchant').attr('onclick', 'return checkSelectedRB();');
				$('#txt_merchant_text').css("display", "none");
				$('#hf_rbl_merchant').val($('#txt_merchant_value').val(''));
				$('.chb').css("display", "none");
				$('#hf_rbl_merchant').val($('#ddl_merchant').val());
				counterFor_checkSelectedRB = 2;
			}
		} else {
			counterFor_checkSelectedRB = 0;
		} $('#btn_assing_merchant').css('display', 'inline-block');
	});

	$('#txt_merchant_text').click(function () {
		$('#btn_assing_merchant').show();
		$('#btn_assing_merchant').attr('onclick', '');
		$('#btn_assing_merchant').click(function () { return checkSelectedRB2() });
	});

	$('#close_me').click(function () { $('#assing_mail').hide(); });


	$('#ddlStatus').change(function () { $('#hf_selectedstatus').val($('#ddlStatus').val()); });

	try {
		$.urlParam = function (name) { var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href); return results[1] || 0; }
		$("#tbl_ch_" + $.urlParam('row_counter') + "").css("color", "red");
	} catch (error) { }
});


function leftSide() { clearHistoryByUser(); /*fillfieldM();*/return true; }
function clearHistoryByUser() { $('#historyuser').html(""); $('#historyuserContent').html(""); $('#pluss_minus').text('+'); return true; }
function ShowHistoryFromCurentUser(mail, tempMailId) { window.parent.frames["fraMenu"].location = "Emails_MessageFilter.aspx?historyUserMail=" + mail; }
function redirectTOmarchent(idmarchent) { window.parent.location = 'Common_framesetTabB.aspx?pageMenuUrl=' + escape('merchant_list.asp') + '&pageBodyUrl=' + escape('merchant_data.asp?companyID=' + idmarchent); }
function showconversationhistory(idpost, rowcounter) { this.location = "Emails_MessageView.aspx?MessageId=" + idpost + "&row_counter=" + rowcounter; }
function merchantIsExited() { alert("The Merchant Is Exited!!"); $('#btn_addNewMarchant').hide(); }
function hiderptAttachments(giv) { $('#"+giv+"').hide(); }
function display_assing_merchant() {
	$('#assing_mail').addClass("assing_mailD");
	var offfs = $('#display_ddl_merchant').offset();
	$('#assing_mail').css("top", (offfs.top + 18));
	$('#assing_mail').css("left", (offfs.left - 18)); 

	
	if ($('#assing_mail').css('display') == 'none') {

		$('#ddl_merchant option').each(function (index) {
			if (this.value == $('#hf_mailID').val()) {
				this.selected = true;
				counterFor_checkSelectedRB = 2;
				$('#assing_mail').css("display", "inline-block");
				$('#hf_selectedstatus').val(this.value);
				$('#btn_assing_merchant').css('display', 'inline-block');
			}
		});

		$('#assing_mail').css('display', 'inline-block');
	} else { $('#assing_mail').css('display', 'none'); }

}
function checkSelectedRB() {
	if (counterFor_checkSelectedRB >= 1)
	{ return true; }
	else { alert('Please select Merchant'); return false; } 
}
function checkSelectedRB2() {
	if ($('#txt_merchant_value').val() != '') {
		$('#hf_rbl_merchant').val($('#txt_merchant_value').val()); return true;
	}
	else {
		alert('Please Enter Merchant'); return false;
	}
}