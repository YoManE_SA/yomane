var ie4=document.all
var ns6=document.getElementById&&!document.all

function OpenPop(sURL,sName,sWidth,sHeight,sScroll) {
	placeLeft = screen.width/2-sWidth/2
	placeTop = screen.height/2-sHeight/2
	var winPrint=window.open(sURL,sName,'TOP='+placeTop+',LEFT='+placeLeft+',WIDTH='+sWidth+',HEIGHT='+sHeight+',scrollbars='+sScroll);
}
function showInfo(sType) {
	sType.style.visibility = 'visible';
	sType.style.left = document.body.scrollLeft+event.clientX+20; //window.event.clientX;
	sType.style.top = document.body.scrollTop+event.clientY-20; //window.event.clientY;
}
function showInfo2(sType,offsetX,offsetY) {
	document.getElementById(sType).style.visibility = 'visible';
	if (ie4) {
		document.getElementById(sType).style.left = document.body.scrollLeft+event.clientX+offsetX; //window.event.clientX;
		document.getElementById(sType).style.top = document.body.scrollTop+event.clientY+offsetY; //window.event.clientY;
	}
	else if (ns6) {
		document.getElementById(sType).style.left = 10
		document.getElementById(sType).style.top = 100
	}
}
function showInfo3(sType) {
	document.getElementById(sType).style.visibility = 'visible';
} 
function hideInfo(sType) {
	document.getElementById(sType).style.visibility = 'hidden';
}

function getAbsPos(xObj){
	var xVar = null;
	xVar = xObj.offsetParent ? getAbsPos(xObj.offsetParent) : {Top:0, Left:0};
	return {Top:xVar.Top + xObj.offsetTop, Left: xVar.Left + xObj.offsetLeft};
}

function PosObject(xObj, xRefObj, sPos, xOffset, yOffset){
	var lPos = getAbsPos(xRefObj);
	switch(sPos.substring(0, 2)){
		case 'LL': break;
		case 'LR': lPos.Left -= xObj.clientWidth; break;
		case 'RL': lPos.Left += xRefObj.clientWidth; break;
		case 'RR': lPos.Left += (xRefObj.clientWidth - xObj.clientWidth); break;
	}
	switch(sPos.substring(2, 4)){
		case 'TT': break;
		case 'TB': lPos.Top -= xObj.clientHeight; break;
		case 'BT': lPos.Top += xRefObj.clientHeight; break;
		case 'BB': lPos.Top += (xRefObj.clientHeight - xObj.clientHeight); break;
	}
	xObj.style.left = lPos.Left + xOffset; xObj.style.top = lPos.Top + yOffset;
}	

function createHttpRequest()
{
	var xmlhttp = null;
	try { netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead"); } catch (e) {}
	if (window.XMLHttpRequest){
		xmlhttp = new XMLHttpRequest();
	}else if (window.ActiveXObject){
		try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP")} 
		catch (e){
			try{ xmlhttp = new ActiveXObject("Microsoft.XMLHTTP")}
			catch (e){} 
		}
	}
	return xmlhttp;
}

function ProcAjaxHtml()
{
	if (this.readyState != 4)
		return;

	this.HTMLObj.innerHTML = this.responseText;
	
	if (this.HTMLObj.onresize) 
		this.HTMLObj.onresize();
}

function setAjaxHTML(nSrc, pObj, bAsyn) 
{
	var nRequest = createHttpRequest();
	if (bAsyn)
	{
		nRequest.onreadystatechange = ProcAjaxHtml;
		nRequest.HTMLObj = pObj;
	}
	
	nRequest.open("GET", nSrc + '&t=' + (new Date()), bAsyn);
	if (window.ActiveXObject)
		nRequest.send();
	else
		nRequest.send(null);
	
	if (!bAsyn)
	{
		if (bAsyn && nRequest.readyState == 4) 
			pObj.innerHTML = nRequest.responseText;
		else 
			pObj.innerHTML = "Error " + nRequest.status + ":" + nRequest.statusText
	}
}

function showSection(SecNum, IsHideRest) {	
	for(i = 0; i <= 7; i++) {
		if (document.getElementById("Tbl" + i)) {
			if (i == SecNum) {
				trSecObj = document.getElementById("Tbl" + SecNum)
				oListToggleObj = document.getElementById("oListToggle" + SecNum)
				if(trSecObj.style.display == "none") {
					oListToggleObj.src = "../images/tree_collapse.gif";	
					trSecObj.style.display = "";
				}
				else {
					oListToggleObj.src = "../images/tree_expand.gif";
					trSecObj.style.display = "none";	
				}
			}
			if (i != SecNum & IsHideRest == 1) {
				document.getElementById("oListToggle" + i).src = "../images/tree_expand.gif";
				document.getElementById("Tbl" + i).style.display = "none";
			}
		}
	}
}
	
function IsKeyDigit() {
	return event.keyCode >= '0'.charCodeAt(0) && event.keyCode <= '9'.charCodeAt(0);
}
	
function clock() {
	if (!document.layers && !document.all) return;
	var digital = new Date();
	
	var dates
	dates = digital.getDate() + "/";
	dates += (digital.getMonth() + 1) + "/";
	dates += digital.getYear();
	
	var hours = digital.getHours();
	var minutes = digital.getMinutes();
	var seconds = digital.getSeconds();
	
	if (minutes <= 9) minutes = "0" + minutes;
	if (seconds <= 9) seconds = "0" + seconds;
	dispTime = hours + ":" + minutes + ":" + seconds + "&nbsp;&nbsp;&nbsp;" + dates;
	if (document.layers) {
	document.layers.pendule.document.write(dispTime);
	document.layers.pendule.document.close();
	}
	else
	if (document.all)
	pendule.innerHTML = dispTime;
	setTimeout("clock()", 1000);
}

function EncryptStatic(sNumber) {
	var i, sTemp, nDigit;
	sTemp='';
	for (i=0;i<sNumber.length;i++) {
		sTemp=Math.round(Math.random()*9)+sTemp;
		sTemp=(9-sNumber.substr(i, 1))+sTemp;
	}
	return sTemp;
}

function DecryptStatic(sNumber) {
	var i, sTemp, nDigit, bGarbage;
	sTemp='';
	bGarbage=false;
	for (i=0;i<sNumber.length;i++) {
		if (!bGarbage) sTemp=(9-sNumber.substr(i, 1))+sTemp;
		bGarbage=!bGarbage;
	}
	return sTemp;
}

function checkCount(sCheckBoxName){
	var pRet = 0;
	var xValues = document.all[sCheckBoxName];
	if(xValues.length){
		for(var i = 0; i < xValues.length; i++)
			if(xValues[i].checked) pRet++;
	}else if(xValues.checked) pRet++;
	return pRet;
}