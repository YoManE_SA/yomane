﻿//
// 20080513 Tamir
//
// Script for opening Permissions Manager window from both ASP.NET and ASP pages
//
function OpenSecurityManagerURL(sPageURL)
{	
	if (sPageURL.indexOf("?") > 0) sPageURL = sPageURL.substr(0, sPageURL.indexOf("?"));
	if (sPageURL.indexOf("/") > 0) sPageURL = sPageURL.substr(sPageURL.lastIndexOf("/") + 1);		
	if (sPageURL.indexOf("#")>0) sPageURL=sPageURL.substr(0, sPageURL.indexOf("#"));
	window.open("security_document.aspx?PageURL="+sPageURL, "fraSecurity", "top=100,left=100,width=600,height=510,scrollbars=yes,resizable");
}
function OpenSecurityManager()
{
	OpenSecurityManagerURL(window.location.href)
}

//
// 20080515 Tamir
//
// Opening Permissions Manager window in frame
//
function OpenSecurityManagerFrameURL(sPageURL)
{
	if (sPageURL.indexOf(">")>0)
	{
		sPageURL=sPageURL.substr(sPageURL.indexOf(">")+1);
		sPageURL=sPageURL.substr(0, sPageURL.indexOf("<"));
	}
	if (sPageURL.indexOf("?") > 0) sPageURL = sPageURL.substr(0, sPageURL.indexOf("?"));
	if (sPageURL.indexOf("/")>0) sPageURL=sPageURL.substr(sPageURL.lastIndexOf("/")+1);	
	if (sPageURL.indexOf("#")>0) sPageURL=sPageURL.substr(0, sPageURL.indexOf("#"));
	parent.frmBody.location.replace("security_document.aspx?frame=yes&PageURL="+sPageURL);
}

function addEvent(obj, strEvent, objFunction)
{	
	if (obj.addEventListener)
	{ 
		obj.addEventListener(strEvent, objFunction, true);
		return true;
	}
	else if (obj.attachEvent)
	{ 
		return obj.attachEvent("on"+strEvent, objFunction);
	} 
	else return false; 
}

function ReturnFalse()
{
	return false;
}

function DeactivateContent()
{	
	arrTags=document.getElementsByTagName("INPUT");
	for (var i=0;i<arrTags.length;i++)
	{
		if (arrTags[i].className.toLowerCase().indexOf("alwaysactive")>=0)
			arrTags[i].disabled=arrTags[i].disabled;
		else
		{
			arrTags[i].disabled=true;
			arrTags[i].style.cursor="default";
		}
	}
	arrTags=document.getElementsByTagName("SELECT");
	for (var i=0;i<arrTags.length;i++)
	{
		if (arrTags[i].className.toLowerCase().indexOf("alwaysactive")>=0)
			arrTags[i].disabled=arrTags[i].disabled;
		else
		{
			arrTags[i].disabled=true;
			arrTags[i].style.cursor="default";
		}
	}
	arrTags=document.getElementsByTagName("TD");
	for (var i=0;i<arrTags.length;i++)
	{
		if (arrTags[i].className.toLowerCase().indexOf("alwaysactive")>=0)
			arrTags[i].disabled=arrTags[i].disabled;
		else
		{
			if (arrTags[i].onClick!=null)
			{
				arrTags[i].disabled=true;
				arrTags[i].style.cursor="default";
			}
		}
	}
	arrTags=document.getElementsByTagName("A");
	for (var i=0;i<arrTags.length;i++)
	{
		if (arrTags[i].className.toLowerCase().indexOf("alwaysactive")>=0)
			arrTags[i].style.cursor="pointer";
		else
		{
			arrTags[i].disabled=true;
			arrTags[i].href="#";
			arrTags[i].target="_self";
			arrTags[i].style.cursor="default";
		}
	}
	if (document.getElementById("pageMainHeadingTd"))
	{
		document.getElementById("pageMainHeadingTd").disabled=false;
	}
}