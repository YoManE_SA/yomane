<%@ Application Language="VB" %>
<%@ Import Namespace="Netpay.Infrastructure" %>
<%@ Import Namespace="Netpay.Web" %>
<script runat="server">
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        Netpay.Web.ContextModule.AppName = "AdminCash"
        Netpay.Web.WebUtils.SessionKeyNotUsePath = True
        Netpay.Process.Application.Init()
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        Dim app = TryCast(sender, System.Web.HttpApplication)
        If (app IsNot Nothing And app.Context IsNot Nothing) Then
            app.Context.Response.Headers.Remove("Server")
        End If
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Dim ex As Exception = Server.GetLastError()
        Netpay.Infrastructure.Logger.Log(LogTag.AdminCash, ex)

        Dim config As System.Configuration.Configuration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~")
        Dim configSection As System.Web.Configuration.CompilationSection = CType(config.GetSection("system.web/compilation"), System.Web.Configuration.CompilationSection)
        If Not configSection.Debug Then
            Response.Write("Unexpected system error occurred.")
            Response.End()
        End If
    End Sub

    Sub Application_OnBeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        WebUtils.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("en-us")
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        If String.IsNullOrEmpty(Session("DomainName")) Then Exit Sub
        Domain.Current = Domain.Get(Session("DomainName"))
        If WebUtils.IsLoggedin Then WebUtils.Logout(False)
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        SyncLock Me
            If Not WebUtils.IsLoggedin Then WebUtils.Login()
        End SyncLock
        Select Case Request.ServerVariables("HTTP_HOST").ToString.ToLower.Replace("admincash.", "")
            Case "obl.me"
                Session("Identity") = "obl"
            Case "netpay-intl.com"
                Session("Identity") = "Netpay"
            case "testenetpay-intl.com"
                session("Identity") = "testnetpay"
            case "stagenetpay-intl.com"
                session("Identity") = "stagenetpay"
            Case "demoobl.com"
                Session("Identity") = "DemoOBL"
            Case "yomane.com"
                Session("Identity") = "Yomane"
                Session("EncryptionKeyNumber") = 5
            Case "pbtpay.eu"
                Session("Identity") = "PBTPay"
                Session("EncryptionKeyNumber") = 4
            Case "jubba.me"
                Session("Identity") = "Jubba"
                Session("EncryptionKeyNumber") = 6
            Case "pandapsp.com"
                Session("Identity") = "Panda"
                Session("EncryptionKeyNumber") = 3
            Case Else
                Session("Identity") = "Local"
        End Select
    End Sub
</script>