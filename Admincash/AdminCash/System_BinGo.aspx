<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient" %>
<script runat="server">
	Sub Page_Load()
		Security.CheckPermission("BIN Numbers - Delete")
		Dim sSQL As String = String.Empty
		If Request("DeleteBIN") <> String.Empty Then
			sSQL = "DELETE FROM tblCreditCardBIN WHERE BIN='" & Request("DeleteBIN") & "'"
		End If
		If sSQL <> String.Empty Then dbPages.ExecSql(sSQL)
		Response.Write("<s" + "cript language=""javascript"" type=""text/javascript"">")
		Response.Write("parent.location.replace(""System_BinData.aspx?BIN=" & Request("BIN") & "&Country=" & Request("Country") & "&CardType=" & Request("CardType") & """);")
		Response.Write("</s" + "cript>")
	End Sub
</script>