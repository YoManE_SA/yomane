<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_controls.asp"-->
<%
sSQL="SELECT ID AS customerID, FirstName, LastName FROM tblCustomer WHERE id=" & trim(request("CustomerID"))
'response.write sSQL & "<br />"
set rsData = oledbData.Execute(sSQL)
if not rsData.EOF then
	sFullName = dbTextShow(rsData("FirstName") & " " & rsData("LastName"))
end if

sSQL="SELECT tblCompany.CompanyName, tblCompanyTransPass.*, List.TransCreditType.Name AS CreditTypeName " &_
"FROM tblCompanyTransPass LEFT OUTER JOIN tblCompany ON tblCompanyTransPass.CompanyID = tblCompany.ID LEFT OUTER JOIN List.TransCreditType ON tblCompanyTransPass.CreditType = List.TransCreditType.TransCreditType_id " &_
"WHERE tblCompanyTransPass.CustomerID=" & trim(request("CustomerID")) & " ORDER BY tblCompanyTransPass.insertdate desc"
set rsData = oledbData.execute(sSQL)
%>
<html>
<head>
<title><%= COMPANY_NAME_1 %> - Control Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
</head>
<body bgcolor="#ffFFFF" leftmargin="0" topmargin="0" rightmargin="0" marginwidth="0" class="itextM2" dir="rtl">
<br />
<table width="90%" align="center" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right">
		<span class="txt16b" style="color:#FF8000;">���� �����</span>
		<span class="txt16"><%= sFullName %></span><br />
	</td>
</tr>
<tr><td><br /></td></tr>
</table>
<br />
	<table width="90%" align="center" dir="ltr" border="0" cellpadding="1" cellspacing="0">
		
		<tr bgcolor="#E9E5D9">
			<td align="right" class="txt12b" style="padding-right:3px; border:1px solid #C3BFB3; border-right:0px solid #C3BFB3;">����</td>
			<td align="right" class="txt12b" style="padding-right:3px; border:1px solid #C3BFB3; border-right:0px solid #C3BFB3;">��� �����<br /></td>
			<td align="right" class="txt12b" style="padding-right:3px; border:1px solid #C3BFB3; border-right:0px solid #C3BFB3;">'��<br /></td>
			<td align="right" class="txt12b" style="padding-right:3px; border:1px solid #C3BFB3; border-right:0px solid #C3BFB3;">����<br /></td>
			<td align="right" class="txt12b" style="padding-right:3px; border:1px solid #C3BFB3; border-right:0px solid #C3BFB3;">����� �����<br /></td>
			<td align="right" class="txt12b" style="padding-right:3px; border:1px solid #C3BFB3; border-right:0px solid #C3BFB3;">�� ���<br /></td>
			<td align="right" class="txt12b" style="padding-right:3px; border:1px solid #C3BFB3; border-right:0px solid #C3BFB3;">�����<br /></td>
			<td align="right" class="txt12b" style="padding-right:3px; border:1px solid #C3BFB3;">����<br /></td>
		</tr>
		<tr>
			<td align="right" colspan="8"></td>
		</tr>
		<%
		if not rsData.EOF then
			sWhichColor = true
			do until rsData.EOF
				if sWhichColor then
					sColorBkg = "white"
				else
					sColorBkg = "#F7F7EF"
				end if
				sWhichColor = not(sWhichColor)
				
				sDate=""
				if day(rsData("InsertDate"))<10 then sDate=sDate & "0" end if
				sDate=sDate & day(rsData("InsertDate")) & "/"
				if month(rsData("InsertDate"))<10 then sDate=sDate & "0" end if
				sDate=sDate & month(rsData("InsertDate")) & "/" & right(year(rsData("InsertDate")),2) & "&nbsp;&nbsp;"
				sDate=sDate & FormatDateTime(rsData("InsertDate"),4)
				
				sComment = replace(dbtextShow(rsData("Comment")),chr(13),"<br />")
				%>
				<tr bgcolor="<%= sColorBkg %>">
					<td align="right" class="txt11" style="padding-right:3px; border-top:1px solid #EFEFE0;">
						<%= sComment %><br />
					</td>
					<td align="right" class="txt12" style="padding-right:3px; border-top:1px solid #EFEFE0;">
						<%= rsData("CreditTypeName") %><br />
					</td>
					<td align="right" class="txt12" valign="top" style="padding-right:3px; border-top:1px solid #EFEFE0;">
						<%= rsData("Payments") %><br />
					</td>
					<td align="right" class="txt11" valign="top" style="padding-right:3px; border-top:1px solid #EFEFE0;">
						<%=FormatCurr(rsData("Currency"), rsData("Amount"))%><br />
					</td>
					<td align="right" class="txt11" valign="top" style="padding-right:3px; border-top:1px solid #EFEFE0;">
						<%= rsData("PaymentMethodDisplay") %><br />
					</td>
					<td align="right" class="txt11" valign="top" style="padding-right:3px; border-top:1px solid #EFEFE0;">
						<%= dbTextShow(rsData("CompanyName")) %><br />
					</td>
					<td align="right" class="txt11" valign="top" style="padding-right:3px; border-top:1px solid #EFEFE0;">
						<%= sDate %><br />
					</td>
					<td align="right" class="txt11" valign="top" style="padding-right:3px; border-top:1px solid #EFEFE0; border-right:1px solid #DDD9CD;">
						<%= rsData("ID") %><br />
					</td>
				</tr>
				<%
				rsData.movenext
			loop
		else
			%>
			<tr bgcolor="White">
				<td align="center" colspan="8" dir="rtl" class="txt12" style="border:1px solid #EFEFE0;">��� ������</td>
			</tr>
			<%
		end if
	%>
	<tr><td colspan="8" style="border-top:1px solid #EFEFE0;"><img src="../images/1_space.gif" alt="" border="0" /><br /></td></tr>
	</table>
<br />
<% call closeConnection() %>
</body>
</html>
