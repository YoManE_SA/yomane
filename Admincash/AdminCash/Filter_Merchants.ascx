﻿<%@ Control Language="VB" ClassName="Filter_Merchants" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
	Private sbJS As New StringBuilder(String.Empty)
	Dim i As Integer = 0

	Private sSqlString As String = "SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany" & _
	" INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1"

	Public Property SqlString() As String
		Get
			Return sSqlString
		End Get
		Set(ByVal value As String)
			sSqlString = value
		End Set
	End Property

	Private sClientField As String
	Public Property ClientField() As String
		Get
			Return sClientField
		End Get
		Set(ByVal value As String)
			sClientField = value
		End Set
	End Property

	Private sWidth As String = "180px"
	Public Property Width() As String
		Get
			Return sWidth
		End Get
		Set(ByVal value As String)
			sWidth = value
		End Set
	End Property

	Private sActiveItemBGColor As String = "#E0EFFF"
	Public Property ActiveItemBGColor() As String
		Get
			Return sActiveItemBGColor
		End Get
		Set(ByVal value As String)
			sActiveItemBGColor = value
		End Set
	End Property

	Private sListBGColor As String = "White"
	Public Property ListBGColor() As String
		Get
			Return sListBGColor
		End Get
		Set(ByVal value As String)
			sListBGColor = value
		End Set
	End Property

	Private sListBorderColor As String = "Silver"
	Public Property ListBorderColor() As String
		Get
			Return sListBorderColor
		End Get
		Set(ByVal value As String)
			sListBorderColor = value
		End Set
	End Property

	Private nRecordsVisible As Integer = 15
	Public Property RecordsVisible() As Integer
		Get
			Return nRecordsVisible
		End Get
		Set(ByVal value As Integer)
			nRecordsVisible = value
		End Set
	End Property

	Dim sSelectedValue As String
	Public ReadOnly Property SelectedValue() As String
		Get
			Return sSelectedValue
		End Get
	End Property
	
	Dim sSelectedText As String 
	Public ReadOnly Property SelectedText As String
		Get
			Return sSelectedText
		End Get
	End Property

	Dim bIsSingleLine As Boolean = False
	Public Property IsSingleLine As Boolean
		Get
			Return bIsSingleLine
		End Get
		Set(ByVal value As Boolean)
			bIsSingleLine = value
		End Set
	End Property
	
	Dim bShowTitle As Boolean = True
	Public Property ShowTitle As Boolean
		Get
			Return bShowTitle
		End Get
		Set(ByVal value As Boolean)
			bShowTitle = value
		End Set
	End Property
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		If ClientField = String.Empty Then ClientField = "ShowCompanyID"
		If SqlString = String.Empty Then SqlString = "SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1"
		If Security.IsLimitedMerchant Then SqlString &= " AND ID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
		SqlString &= " ORDER BY Cast(Upper(tblCompany.CompanyName) AS binary)"
		Dim drData As SqlDataReader = dbPages.ExecReader(sSqlString)
		Dim bColor As Boolean = False, sColorText As String
		Try
			If drData.GetOrdinal("StatusColor") >= 0 Then bColor = True
		Catch ex As Exception
			'nothing to do
		End Try
		Dim nSelItem As Integer = dbPages.TestVar(SelectedValue, 0, -1, 0)
		Do While drData.Read
			If drData("CompanyID") = nSelItem Then sSelectedText = drData("CompanyName")
			sbJS.Append("n[" & i.ToString & "]=" & drData("CompanyID") & ";" & vbCrLf)
			sbJS.Append("t[" & i.ToString & "]='" & drData("CompanyName").ToString.Replace("'", "\'") & "';" & vbCrLf)
			sColorText = IIf(bColor, "<span style=\""background-color:" & drData("StatusColor") & ";\"">&nbsp;</span> ", String.Empty)
			sbJS.Append("c[" & i.ToString & "]='" & sColorText & "';" & vbCrLf)
			i += 1
		Loop
		drData.Close()
		If Page.IsPostBack Then
			sSelectedText = Request("FilterMerchantName")
			sSelectedValue = Request("FilterMerchantID")
		Else
			If Not String.IsNullOrEmpty(Request("FilterMerchantDefaultID")) Then
				sSelectedValue = Request("FilterMerchantDefaultID")
				If Not String.IsNullOrEmpty(Request("FilterMerchantDefaultName")) Then
					sSelectedText = Request("FilterMerchantDefaultName")
				Else
					sSelectedText = dbPages.ExecScalar("SELECT CompanyName FROM tblCompany WHERE ID=" & sSelectedValue)
				End If
			End If
		End If
	End Sub
</script>
<style type="text/css">
	DIV { }
	.headertemplate { border-collapse: collapse; background-color:silver; font-family:Tahoma; font-weight:bold; font-size:11px; padding-left:2px; }
	.itemtemplate { border-bottom:1px solid silver; margin:0; padding:1; text-align:left; font-size:11px; padding-left:2px;}
	.itemtemplate td { border-bottom:0px!important; }
</style>
<script language="javascript" type="text/javascript">
	var n=new Array(), t=new Array(), c=new Array();
	<%= sbJS.ToString() %>
	
	function UseName(i)
	{
		document.getElementById('FilterMerchantName').value=t[i];
		if (document.getElementById('<%= ClientField %>')) document.getElementById('<%= ClientField %>').value=n[i];
		document.getElementById('FilterMerchantID').value=n[i];
		document.getElementById('divFilterList').style.display='none';
		document.getElementById('divFilterList').innerHTML='';
		return false;
	}

	function SetFilterValue(nID)
	{
		document.getElementById('FilterMerchantName').value=t[i];
		if (document.getElementById('<%= ClientField %>')) document.getElementById('<%= ClientField %>').value=n[i];
		document.getElementById('divFilterList').style.display='none';
		document.getElementById('divFilterList').innerHTML='';
		return false;
	}

	function FilterFocus(obj)
	{
		obj.style.backgroundColor='<%= ActiveItemBGColor %>';
		obj.focus();
	}

	function FilterBlur(obj)
	{
		obj.style.backgroundColor="";
	}

	function FilterMove()
	{
		if	(event.keyCode==40)
		{
			FilterBlur(event.srcElement);
			FilterFocus(event.srcElement.nextSibling?event.srcElement.nextSibling:divFilterList.firstChild);
			return true;
		}
		if	(event.keyCode==38)
		{
			FilterBlur(event.srcElement);
			FilterFocus(event.srcElement.previousSibling?event.srcElement.previousSibling:divFilterList.lastChild);
			return true;
		}
		if	(event.keyCode==27)
		{
			document.getElementById('FilterMerchantName').focus();
			divFilterList.style.display='none';
			divFilterList.innerHTML='';
			return true;
		}
	}
	function SetNames(sText){
		if (document.getElementById('divFilterList').style.display == 'block')
		{
			if	(event.keyCode==40)
			{
				FilterFocus(document.getElementById('divFilterList').firstChild)
				return true;
			}
			if	(event.keyCode==38)
			{
				FilterFocus(document.getElementById('divFilterList').lastChild)
				return true;
			}
			if	(event.keyCode==27)
			{
				document.getElementById('divFilterList').style.display='none';
				document.getElementById('divFilterList').innerHTML='';
				return true;
			}
		}
		if (document.getElementById('<%= ClientField %>')) document.getElementById('<%= ClientField %>').value='<%= SelectedValue %>';
		var nLength=<%= i %>;
		var sHTML='';
		if (sText!='')
		{
			sText=sText.toLowerCase();
			var nFirst=-1;
			var nLast=-1;
			for (var i=0;i<nLength;i++)
			{
				if (nFirst==-1)
				{
					if (t[i].toLowerCase().substr(0,sText.length)>=sText) nFirst=i;
				}
				if ((nFirst!=-1)&&(nLast==-1))
				{
					if (t[i].toLowerCase().substr(0,sText.length)>sText) nLast=i;
				}
			}
			if (nFirst>=0 && nLast<0) nLast=nLength;
			if (nLast-nFirst><%= nRecordsVisible %>) nLast=nFirst+<%= nRecordsVisible %>;
			for (var i=nFirst;i<nLast;i++)
				sHTML += '<a href=\"#\" onclick=\"return UseName(' + i + ');\" onkeyup=\"return FilterMove();\" title=\"' + t[i] + '\">'+ c[i] + t[i] +'</a>';
				
		}
		document.getElementById('divFilterList').innerHTML=sHTML;
		document.getElementById('divFilterList').style.display=(sHTML==''?'none':'block');
		if (document.getElementById('divFilterList').style.display=='block')
		{
			document.getElementById('divFilterList').style.direction=('אבגדהוזחטיכךלמםנןסעפףצץקרשת'.indexOf(t[nFirst].substr(0, 1))>-1?'rtl':'ltr');
			document.getElementById('divFilterList').style.textAlign=('אבגדהוזחטיכךלמםנןסעפףצץקרשת'.indexOf(t[nFirst].substr(0, 1))>-1?'right':'left');
		}
	}

	function Filter_IsKeyDigit()
	{
		return event.keyCode >= '0'.charCodeAt(0) && event.keyCode <= '9'.charCodeAt(0);
	}

	function SetMerchantID()
	{
		var FilterMerchantID=document.getElementById("FilterMerchantID");
		if ((FilterMerchantID.value!="")&&(parseInt(FilterMerchantID.value)!=FilterMerchantID.value))
		{
			alert('Merchant ID must be a number!')
			document.getElementById('FilterMerchantID').focus();
			document.getElementById('FilterMerchantID').select();
			return false;
		}
		if (document.getElementById('<%= ClientField %>')) document.getElementById('<%= ClientField %>').value = document.getElementById('FilterMerchantID').value;
		document.getElementById('FilterMerchantName').value = '';
		document.getElementById('divFilterList').style.display='none';
		document.getElementById('divFilterList').innerHTML='';
		return true;
	}

	function FilterCloseList()
	{
		document.getElementById('divFilterList').style.display='none';
		document.getElementById('divFilterList').innerHTML='';
	}
	
	function FilterAddEvent(obj, strEvent, objFunction)
	{ 
		if (obj.addEventListener)
		{ 
			obj.addEventListener(strEvent, objFunction, true); 
			return true; 
		}
		else if (obj.attachEvent)
		{ 
			var returnValue = obj.attachEvent('on'+strEvent, objFunction); 
			return returnValue; 
		} 
		else return false; 
	}
	FilterAddEvent(document, 'click', FilterCloseList);

</script>
<style media="all" type="text/css">
	#divFilterList
	{
		width:<%= sWidth %>;
		overflow-y:auto;
		position:absolute;
		background-color:<%= ListBGColor %>;
		border:1px solid <%= ListBorderColor %>;
		margin-top:-3px;
	}
	#divFilterList a
	{
		display:block;
		text-decoration:none;
		padding:2px;
		height:17px;
		overflow:hidden;
	}
	#FilterMerchantName {width:<%= sWidth %>;}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="filterNormal">
<%If bShowTitle Then Response.Write("<tr><th>Merchant</th></tr>")%>
<tr>
	<td>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>Name&nbsp;</td>
			<td>
				<input id="FilterMerchantName" name="FilterMerchantName" value="<%=sSelectedText%>" class="input1" dir="ltr" onkeyup="SetNames(this.value);return true;" onclick="event.cancelBubble=true;" ondblclick="SetNames(this.value);return true;" /><br />
				<div id="divFilterList" style="display:none;"></div>
			</td>
		<%If Not bIsSingleLine Then Response.Write("</tr><tr>")%>
			<td>ID</td>
			<td><input id="FilterMerchantID" name="FilterMerchantID" value="<%=sSelectedValue%>" class="input1" size="5" dir="ltr" onkeypress="return Filter_IsKeyDigit();" onchange="return SetMerchantID();" /></td>
		</tr>
		</table>
	</td>
</tr>
</table>