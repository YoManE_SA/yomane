<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script language="VB" runat="server">

    Sub btnLogin_OnClick(Src As Object, E As EventArgs)
        If FormsAuthentication.Authenticate(txtUsername.Text, txtPassword.Text) Then
            FormsAuthentication.RedirectFromLoginPage(txtUsername.Text, True)
        Else
            lblInvalid.Style.Value = "color:maroon;"
            lblInvalid.Text = "����� ����� �� ������ - ��� ���"
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        txtUsername.Style.Value = "direction:ltr;"
        txtPassword.Style.Value = "direction:ltr;"
    End Sub

</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Admin Pages</title>
    <link  href="../StyleSheet/StyleAdminNet.css" rel="stylesheet" type="text/css" />
</head>
<body dir="rtl">

    <br /><br /><br /><br />
    <table align="center" style="width:92%">
    <tr>
        <td>
            <table align="center" style="width:300px; border:1px solid #E6E2D8; background-color:#F7F6F3; padding:8px;">
            <tr>
                <td style="text-align:center; background-color:#ff9933; font-size:13px; font-weight:bold; color:white;">
                    ����� ��� ������
                </td>
            </tr>
            <tr>
                <td style="font-size:13px;padding-top:8px; padding-bottom:8px;">
                
                    <form runat="server">
                        <table align="center">
                        <tr><td colspan="2"><asp:Label id="lblInvalid" runat="server" /></td></tr>
                        <tr>
                            <td>�� �����:</td>
                            <td><asp:TextBox id="txtUsername" runat="server" MaxLength="10" Width="150px" /></td>
                        </tr>
                        <tr>
                            <td>�����:</td>
                            <td><asp:TextBox id="txtPassword" TextMode="password" runat="server" MaxLength="10" Width="150px" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><asp:Button id="btnLogin" runat="server" text="����" OnClick="btnLogin_OnClick" /></td>
                        </tr>
                        </table>
                    </form>
                         
                </td>
            </tr>
            </table>
        </td>
    </tr>
    </table>
    <br />

</body>
</html>


