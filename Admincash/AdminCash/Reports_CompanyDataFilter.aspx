<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="radC" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
	Dim sSQL, sWhere, sBlocked, sColor, sLineColor As String
	Dim iReader As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Security.CheckPermission(lblPermissions)
        If Not Page.IsPostBack Then
            fdtrControl.FromDateTime = Date.Today.AddMonths(-1).AddDays(1)
            fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
            fdtrTrans.FromDateTime = Date.Today.AddMonths(-1).AddDays(1)
            fdtrTrans.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
            NetpayConst.FillCombo(S_ActiveStatus, eGroupNames.GGROUP_COMPANYSTATUS, 1, -1)
            NPControls.FillList(S_Bank, dbPages.ExecReader("SELECT DebitCompany_id, dc_name FROM tblDebitCompany ORDER BY dc_IsActive DESC, dc_name"), , "!@#$", Nothing, Nothing)
            NPControls.FillList(S_AccountManager, dbPages.ExecReader("SELECT DISTINCT CASE careOfAdminUser WHEN '' THEN '1234' ELSE careOfAdminUser END ID, careOfAdminUser Name FROM tblCompany ORDER BY careOfAdminUser"), , "!@#$", Nothing, Nothing)
        Else
            Dim sSearchString As String = SearchString.Text
            If sSearchString <> "" Then
                sWhere &= " AND (tblCompany.CompanyName LIKE '%" & sSearchString & "%'"
                sWhere &= " OR tblCompany.companyLegalName LIKE '%" & sSearchString & "%'"
                sWhere &= " OR tblCompany.FirstName LIKE '%" & sSearchString & "%'"
                sWhere &= " OR tblCompany.LastName LIKE '%" & sSearchString & "%')"
            End If
            Dim sMerchantIndustry As String = Trim(Request("MerchantIndustry"))
            If sMerchantIndustry <> "" Then sWhere &= " AND tblCompany.CompanyIndustry_id IN(" & sMerchantIndustry & ")"
            Dim sFromDate As String = Trim(Request("FromDate"))
            If sFromDate <> "" Then sWhere &= " AND tblCompany.InsertDate >= '" & sFromDate & "'"
            Dim sToDate As String = Trim(Request("ToDate"))
            If sToDate <> "" Then sWhere &= " AND tblCompany.InsertDate <= '" & sToDate & "'"
            If Security.IsLimitedMerchant Then sWhere &= " AND tblCompany.ID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
            Dim sActiveStatuses As String = String.Empty
            For Each liStatus As ListItem In S_ActiveStatus.Items
                If liStatus.Selected Then sActiveStatuses &= ", " & liStatus.Value
            Next

            Dim sBanks As String = String.Empty
            For Each liBank As ListItem In S_Bank.Items
                If liBank.Selected Then sBanks &= ", " & liBank.Value
            Next
            If Not String.IsNullOrEmpty(sBanks) Then sWhere &= " AND tblCompany.ID IN (SELECT MerchantID FROM viewMerchantTerminals WHERE DebitCompanyID IN (" & sBanks.Substring(2) & "))"

            Dim sAccountManagers As String = String.Empty
            For Each liAccountManager As ListItem In S_AccountManager.Items
                If liAccountManager.Selected Then sAccountManagers &= ", '" & liAccountManager.Text & "'"
            Next
            If Not String.IsNullOrEmpty(sAccountManagers) Then sWhere &= " AND tblCompany.careOfAdminUser IN (" & sAccountManagers.Substring(2) & ")"

            Dim sFromDateTrans As String = Request("FromDateTrans").Trim, sToDateTrans As String = Request("ToDateTrans").Trim, sTransDateCondition As String
            If String.IsNullOrEmpty(sFromDateTrans) Then
                sTransDateCondition = IIf(String.IsNullOrEmpty(sToDateTrans), String.Empty, "InsertDate<='" & sToDateTrans & "'")
            Else
                sTransDateCondition = IIf(String.IsNullOrEmpty(sToDateTrans), "InsertDate>='" & sFromDateTrans & "'", "InsertDate BETWEEN '" & sFromDateTrans & "' AND '" & sToDateTrans & "'")
            End If
            If Not String.IsNullOrEmpty(sTransDateCondition) Then sWhere &= " AND tblCompany.ID IN (SELECT DISTINCT CompanyID FROM tblCompanyTransPass WITH (NOLOCK) WHERE " & sTransDateCondition & ")"

            If sWhere <> "" Then sWhere = " WHERE " & Mid(sWhere, 5)
            sSQL = "SELECT ID, CompanyName, ActiveStatus, UserName, comment, PaymentMethod, CompanyIndustry_id, FirstName, LastName FROM tblCompany " & sWhere & " ORDER BY tblCompany.CompanyName ASC"
            iReader = dbPages.ExecReader(sSQL)
            panFilter.Visible = False
        End If
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" type="text/css" media="all" href="../StyleSheet/calendar-blue.css" title="winter" />
	<script language="javascript" type="text/javascript">
	    function printAction() {
	        window.parent.frmBody.focus();
	        window.parent.frmBody.print();
	    }
	    var checkflag = "false";
	    function CheckAllCompanys() {
	        if (checkflag == "false") {
	            if (frmList.CompanyID) {
	                if (frmList.CompanyID.length) {
	                    for (i = 0; i < frmList.CompanyID.length; i++)
	                        frmList.CompanyID[i].checked = true;
	                }
	                else {
	                    frmList.CompanyID.checked = true
	                }
	            }
	            checkflag = "true";
	        }
	        else {
	            if (frmList.CompanyID) {
	                if (frmList.CompanyID.length) {
	                    for (i = 0; i < frmList.CompanyID.length; i++)
	                        frmList.CompanyID[i].checked = false;
	                }
	                else {
	                    frmList.CompanyID.checked = false
	                }
	            }
	            checkflag = "false";
	        }
	    }
	    function ResizeMerchantList() {
	        if (document.getElementById("divCompanyDis")) document.getElementById("divCompanyDis").style.height = document.documentElement.clientHeight - 250;
	    }
	</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<form runat="server" >
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>

	<h1><asp:Label ID="lblPermissions" runat="server" /> Merchants Info<span> - Filter</span></h1>

	<asp:Panel ID="panFilter" runat="server">
		<table class="filterNormal">
			<tr>
				<th colspan="4">
					Signup Date<br />
				</th>
			</tr>
			<tr>
				<td colspan="4">
					<Uc1:FilterDateTimeRange Title="" ID="fdtrControl" FromTimeFieldName = "fromTime" ToTimeFieldName = "toTime" FromDateFieldName = "fromDate" ToDateFieldName = "toDate"  runat="server" />
				</td>
			</tr>
		</table>
		<table class="filterNormal">
			<tr>
				<th colspan="4">
					Transaction Date<br />
				</th>
			</tr>
			<tr>
				<td colspan="4">
					<Uc1:FilterDateTimeRange Title="" ID="fdtrTrans" FromTimeFieldName = "fromTimeTrans" ToTimeFieldName = "toTimeTrans" FromDateFieldName = "fromDateTrans" ToDateFieldName = "toDateTrans"  runat="server" />
				</td>
			</tr>
		</table>
		<table width="100%">
			<tr>
				<td style="vertical-align:top;">
					<table class="filterNormal">
						<tr>
							<th>
								Account Manager
							</th>
						</tr>
						<tr>
							<td>
								<asp:ListBox runat="Server" Rows="7" Width="105px" ID="S_AccountManager" SelectionMode="Multiple" />
							</td>
						</tr>
					</table>
				</td>
				<td style="vertical-align:top;">
					<table class="filterNormal">
						<tr>
							<th>
								Bank
							</th>
						</tr>
						<tr>
							<td>
								<asp:ListBox runat="Server" Rows="7" Width="105px" ID="S_Bank" SelectionMode="Multiple" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="vertical-align:top;">
					<table class="filterNormal">
						<tr>
							<th>
								Industry
							</th>
						</tr>
						<tr>
							<td>
								<select id="MerchantIndustry" name="MerchantIndustry" multiple size="11" style="font-size: 11px;">
									<option value="1" <%If InStr(1, Request("MerchantIndustry"), "1") > 0 Then%>selected<%End if%>
										style="background-color: LightBlue;">E-Commerce</option>
									<option value="2" <%If InStr(1, Request("MerchantIndustry"), "2") > 0 Then%>selected<%End if%>
										style="background-color: LightCoral;">Adult</option>
									<option value="3" <%If InStr(1, Request("MerchantIndustry"), "3") > 0 Then%>selected<%End if%>
										style="background-color: LightCyan;">Dating</option>
									<option value="4" <%If InStr(1, Request("MerchantIndustry"), "4") > 0 Then%>selected<%End if%>
										style="background-color: LightGoldenrodYellow;">Gaming</option>
									<option value="5" <%If InStr(1, Request("MerchantIndustry"), "5") > 0 Then%>selected<%End if%>
										style="background-color: LightGreen;">Pharmacy</option>
									<option value="6" <%If InStr(1, Request("MerchantIndustry"), "6") > 0 Then%>selected<%End if%>
										style="background-color: LightGrey;">Computer Software</option>
									<option value="7" <%If InStr(1, Request("MerchantIndustry"), "7") > 0 Then%>selected<%End if%>
										style="background-color: LightPink;">Direct Marketing</option>
									<option value="8" <%If InStr(1, Request("MerchantIndustry"), "8") > 0 Then%>selected<%End if%>
										style="background-color: LightSalmon;">Money Transfer</option>
									<option value="9" <%If InStr(1, Request("MerchantIndustry"), "9") > 0 Then%>selected<%End if%>
										style="background-color: LightSeaGreen;">E-wallet</option>
									<option value="10" <%If InStr(1, Request("MerchantIndustry"), "10") > 0 Then%>selected<%End if%>
										style="background-color: LightSkyBlue;">Prepay Wallet</option>
									<option value="20" <%If InStr(1, Request("MerchantIndustry"), "20") > 0 Then%>selected<%End if%>
										style="background-color: LightYellow;">Other</option>
								</select>
							</td>
						</tr>
					</table>
				</td>
				<td style="vertical-align:top;">
					<table class="filterNormal">
						<tr>
							<th>
								Status
							</th>
						</tr>
						<tr>
							<td>
								<asp:ListBox runat="Server" Rows="7" Width="105px" ID="S_ActiveStatus" SelectionMode="Multiple" />
							</td>
						</tr>
						<tr>
							<th style="padding-top:14px;">
								Search String
							</th>
						</tr>
						<tr>
							<td>
								<asp:TextBox ID="SearchString" Width="100px" runat="server" />
							</td>
						</tr>
						<tr>
							<td style="text-align:right;">
								<input type="submit" name="Action" value="  SHOW  " />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

	</asp:Panel>
	</form>
	<%
	If Page.IsPostBack Then
		If iReader.HasRows Then
			%>
			<form target="frmBody" name="frmList" id="frmList" method="post" action="Reports_CompanyDataForApprove.aspx">
			<table class="filterNormal" width="98%">
				<tr>
					<th>
						<input type="Checkbox" onclick="this.value=CheckAllCompanys();">
						Merchants<br />
					</th>
				</tr>
				<tr>
					<td>
						<div id="divCompanyDis" style="background-color:#f5f5f5;border:1px solid silver;height:200px; width:95%; overflow-y:auto; overflow-x:hidden;">
							<table cellspacing="1" class="filterNormal">
								<%
								Dim nMerchantCount As Integer = 0
								While iReader.Read()
									nMerchantCount += 1
									sColor = "#66cc66"
									If dbPages.TestVar(iReader("ActiveStatus"), 0, -1, eCompanyStatus.CMPS_NEW) = eCompanyStatus.CMPS_CLOSED Then
										sColor = "#ff6666"
									ElseIf dbPages.TestVar(iReader("ActiveStatus"), 0, -1, eCompanyStatus.CMPS_NEW) <= eCompanyStatus.CMPS_MAXCLOSED Then
										sColor = "#ff8040"
									End If
									sLineColor = ""
									Select Case iReader("CompanyIndustry_id")
										Case 1 : sLineColor = "LightBlue"
										Case 2 : sLineColor = "LightCoral"
										Case 3 : sLineColor = "LightCyan"
										Case 4 : sLineColor = "LightGoldenrodYellow"
										Case 5 : sLineColor = "LightGreen"
										Case 6 : sLineColor = "LightGrey"
										Case 7 : sLineColor = "LightPink"
										Case 8 : sLineColor = "LightSalmon"
										Case 9 : sLineColor = "LightSeaGreen"
										Case 10 : sLineColor = "LightSkyBlue"
										Case 20 : sLineColor = "LightYellow"
									End Select
									%>
										<tr>
											<td bgcolor="<%= sColor %>" width="1%" valign="top">
												<br />
											</td>
											<td bgcolor="<%= sLineColor %>" width="4%" valign="top" style="border-bottom: 1px dotted gray;">
												<input type="Checkbox" name="CompanyID" style="background-color: <%= sLineColor %>;" value="<%= iReader("ID") %>"><br />
											</td>
											<td bgcolor="<%= sLineColor %>" valign="top" style="border-bottom: 1px dotted gray;">
												<a href="merchant_data.asp?companyID=<%= iReader("ID") %>&isShowDelete=0" target="frmBody">
													<%
													If Trim(iReader("CompanyName")) = "" Or Trim(iReader("CompanyName")) = "-" Then
														Response.Write("(" & dbPages.dbtextShow(iReader("firstName")) & " " & dbPages.dbtextShow(iReader("lastName")) & ")")
													Else
														Response.Write(dbPages.dbtextShow(iReader("CompanyName")))
													End If
													%>
												</a>
											</td>
										</tr>
									<%
								End While
								%>
							</table>
						</div>
						<script language="javascript" type="text/javascript" defer="defer">
							ResizeMerchantList();
						</script>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" class="filterNormal">
							<tr>
								<th colspan="2">
									Report Type
								</th>
							</tr>
							<tr>
								<td>
									<select name="reportType" onchange="this.form.action=this.value; this.form.bPrint.disabled=false;">
										<!--<option value="Reports_CompanyDataInfo.asp" selected="selected">Details</option>-->
										<option value="Reports_CompanyDataForApprove.aspx" selected="selected">For Approval</option>
										<option value="Reports_CompanyDataMailing.aspx">Mailing List</option>
									</select>
								</td>
								<td style="text-align:right;">
									<input type="submit" name="bSubmit" value="PREVIEW" style="width: 70px;">
									<input type="button" name="bPrint" id="bPrint" value="PRINT" disabled="disabled" style="cursor:not-allowed; width: 50px;" onclick="printAction();"><br />
								</td>
							</tr>
							<tr>
								<td colspan="2" style="text-align:right;">
									<hr class="filter" />
									<input type="button" value="BACK" style="width: 50px;" onclick="history.back();"><br />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</form>
			<%
		Else
			%>
			<br />
			<table width="95%" align="center" class="filterNormal">
				<tr>
					<td style="font-size: 13px;">
						<hr class="filter" />
						Did not find any merchants<br />
						<hr class="filter" />
						<input type="button" value="BACK" style="width: 50px;" onclick="history.back();"><br />
					</td>
				</tr>
			</table>
			<%
		End If
		iReader.Close()
	End If
	%>
</asp:Content>