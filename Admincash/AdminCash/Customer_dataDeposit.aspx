<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">	
	Dim CurrencyText() As String = Nothing
	Dim nCustomerID As Integer = 0
	Dim iReader As SqlDataReader
	Dim sSQL As New StringBuilder(String.Empty)
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		dbPages.CurrentDSN = 2
		Response.Write(dbPages.ShowCurrentDSN)
		If Not IsPostBack then
			if dbPages.TestVar(Request("Customer"), 0, -1, 0) >= 0 then
				nCustomerID = int(request("Customer"))
				Customer.Value = nCustomerID				
				For i As Integer=0 To eCurrencies.MAX_CURRENCY
					If i > 0 then
						sSQL.Append(" Union ")
					End If
					sSQL.Append("SELECT TOP 15 tblCustomerBalance.*, tblGlobalData.GD_Text AS balanceSource," & i & " AS CurrencyID ")
					sSQL.Append("FROM tblCustomerBalance LEFT JOIN tblGlobalData ON tblCustomerBalance.balanceSourceID = tblGlobalData.GD_ID+1 AND tblGlobalData.GD_Group=67 AND tblGlobalData.gd_lng=1 ")
					sSQL.Append("WHERE(tblCustomerBalance.Currency = " & i & ") AND (tblCustomerBalance.CustomerID = " & nCustomerID & ") ")
				Next
				sSQL.Append("ORDER BY CurrencyID, tblCustomerBalance.InsertDate asc")			
				iReader = dbPages.ExecReader(sSQL.ToString)				
			end if
			NetpayConst.GetCurrencyArray(CurrencyText)		
			For i As Integer = 0 To UBound(CurrencyText)
				Currency.Items.Add(New ListItem(dbPages.GetCurText(i) & " " & CurrencyText(i), i))			
			Next
			BalanceSource.Items.Add("")
			NetpayConst.FillCombo(BalanceSource,67,1,-10)			
		Else
			if dbPages.TestVar(Customer.Value, 0, -1, 0) >= 0 then
				nCustomerID = Customer.Value
				For i As Integer=0 To eCurrencies.MAX_CURRENCY
					If i > 0 then
						sSQL.Append(" Union ")
					End If
					sSQL.Append("SELECT TOP 15 tblCustomerBalance.*, tblGlobalData.GD_Text AS balanceSource," & i & " AS CurrencyID ")
					sSQL.Append("FROM tblCustomerBalance LEFT JOIN tblGlobalData ON tblCustomerBalance.balanceSourceID = tblGlobalData.GD_ID+1 AND tblGlobalData.GD_Group=67 AND tblGlobalData.gd_lng=1 ")
					sSQL.Append("WHERE(tblCustomerBalance.Currency = " & i & ") AND (tblCustomerBalance.CustomerID = " & nCustomerID & ") ")
				Next
				sSQL.Append("ORDER BY CurrencyID, tblCustomerBalance.InsertDate asc")			
				iReader = dbPages.ExecReader(sSQL.ToString)
			End If
		End If
		Dim sFullName As String=""
		sFullName = dbPages.ExecScalar("SELECT LastName+' '+FirstName AS FullName FROM tblCustomer WHERE ID=" & nCustomerID)		
		client_name.Text = sFullName
	End Sub
	
	Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		If not IsNumeric(""&txtAmount.Text) then	
			txtAmount.Focus
			Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")			
			Response.Write("alert('No or invalid amount!')")
			Response.Write("</s" & "cript>")
			Exit Sub
		End If
		Dim nAmountTotal As Decimal
		Dim nAmountTotalObj As Object
		nAmountTotalObj = dbPages.ExecScalar("SELECT Top 1 Balance FROM tblCustomerBalance WHERE CustomerID=" & nCustomerID & " AND Currency=" & Currency.SelectedItem.Value & " ORDER BY ID DESC")
		nAmountTotal = IIf(IsDBNull(nAmountTotalObj),0,CType(nAmountTotalObj,Decimal))
		Dim nAmount As Decimal = CType(txtAmount.Text,Decimal)
		Dim nCreditType As Integer = 0
		If CreditType.SelectedItem.Value="0" then
			nAmount = nAmount * -1.0
		Else
			nCreditType = 1
		End If
		nAmountTotal=nAmountTotal+nAmount
		Dim nBalanceSourceID as Integer = 0
		If BalanceSource.SelectedItem.Value <> "" then
			nBalanceSourceID = CType(BalanceSource.SelectedItem.Value,Integer) + 1
		End If
		dbPages.ExecSql("INSERT INTO tblCustomerBalance(CustomerID, Amount, Currency, Type, Balance, Comment, IP, BalanceSourceID) VALUES(" & nCustomerID & ", " & nAmount & ", " & Currency.SelectedItem.Value & ", " & nCreditType & ", " & nAmountTotal & ", '" & dbPages.DBText(Comment.Text) & "', '" & Request.ServerVariables("REMOTE_ADDR") & "', " & nBalanceSourceID & ")")
		Response.Redirect("customer_dataDeposit.aspx?Customer=" & nCustomerID)
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
		span.security
		{
			font-weight:bold;
			font-size:12px;
			color:rgb(245, 245, 245);
			background-color:#2566AB;
			padding-left:3px; padding-right:3px;
			vertical-align:middle;
		}
		span.securityManaged { cursor:pointer; }
	</style>
</head>
<body>
	<script language="JavaScript" type="text/javascript">

		function CheckFormInter() {
			if (document.form1.txtAmount.value == '' || !IsNumeric(document.form1.txtAmount.value)) {
				alert('None or illegal sum entered!');
				document.form1.txtAmount.focus();
				return false;
			}
			return true;
		}

		function IsNumeric(sText) {
			var ValidChars = "0123456789.";
			var IsNumber = true;
			var Char;


			for (i = 0; i < sText.length && IsNumber == true; i++) {
				Char = sText.charAt(i);
				if (ValidChars.indexOf(Char) == -1) {
					IsNumber = false;
				}
			}
			return IsNumber;

		}

	</script>
    <form id="form1" runat="server">
    <table align="center" style="width:72%" border="0">
		<tr>
			<td>
			<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">DEPOSIT INTERFACE</span> - <span style="color:#000000;"><asp:Label ID="client_name" runat="server" /></span>
			</td>
		</tr>
		<tr><td colspan="2"><br /></td></tr>
		<tr>
			<td colspan="2"  class="txt13b">
			<table align="center" border="0" cellpadding="1" cellspacing="2" width="600px" style="border:1px solid #000000;background-color:#f5f5f5;" class="formThin" dir="ltr">
				<tr>					
					<td class="txt12" bgcolor="#f5f5f5">
						<asp:RadioButtonList ID="CreditType" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table">
							<asp:ListItem Value="1" Selected="True">Credit&nbsp;&nbsp;&nbsp;</asp:ListItem>
							<asp:ListItem Value="0">Debit</asp:ListItem>							
						</asp:RadioButtonList>
					</td>
					<td>&nbsp;</td>	
					<td class="head" width="10%">Amount&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtAmount" runat="server" /></td>					
					<td>&nbsp;</td>	
					<td class="head" width="10%">Currency&nbsp;<asp:ListBox ID="Currency" runat="server" Rows="1" style="width:80px"/></td>
				</tr>
				<tr>
					<td class="head">Action&nbsp;<asp:ListBox ID="BalanceSource" runat="server" Rows="1" /></td>						
					<td>&nbsp;</td>
					<td class="head" width="10%">Comment&nbsp;<asp:TextBox ID="Comment" runat="server" MaxLength="100" /></td>
					<td>&nbsp;</td>					
					<td width="50%">&nbsp;</td>
				</tr>
				<tr><td colspan="5" style="text-align:right"><asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" OnClientClick="return CheckFormInter()" /></td></tr>
			</table>
			</td>
		</tr>
    </table>
		<asp:HiddenField ID="Customer" runat="server" />
    </form>
    <table class="formNormal" align="center" style="width:72%" border="0" cellpadding="0" cellspacing="0">
    <%
    Dim tmpCurrency As Integer = -1
    Dim sFontColor As String = ""
    Dim dBalance As Decimal = 0D
    While iReader.Read		
		If tmpCurrency <> cint(""&iReader("CurrencyID")) then
    		If tmpCurrency > -1 then
    		Response.Write ("<tr><td colspan=""6"">Account Balance :" & dbPages.FormatCurr(tmpCurrency,dBalance) & "</td></tr>" )
    		End If
    		tmpCurrency = cint(""&iReader("CurrencyID"))
    %>
			<tr><td colspan="6"><br /><br /><strong><%=dbpages.GetCurText(iReader("CurrencyID"))%> - Balance</strong></td></tr>
			<tr>
				<th>Date</th>
				<th>Transaction</th>
				<th style="text-align:right">Debit</th>
				<th style="text-align:right">Credit</th>
				<th style="text-align:right">Balance</th>
				<th>Comment</th>
			</tr>
    <%	
		End If
		if int(""&iReader("Balance"))<0 then 
    		sFontColor = "red"
    	Else
    		sFontColor="white"
    	End If    	
    %>
				<tr>
					<td align="right" class="txt11" nowrap style="padding-right:10px;">
						<%Response.Write( dbPages.FormatDateTime(iReader("InsertDate"),True))%><br />
					</td>
					<td align="right" class="txt12">
						<%Response.Write( iReader("balanceSource")) %><br />
					</td>
					<td align="right" class="txt11" style="color:red;text-align:right" dir="ltr" nowrap>
						<%
						if int(""&iReader("Type"))=0 then
							response.write (dbPages.FormatCurr(iReader("CurrencyID"),iReader("Amount")))
						Else
							Response.Write("&nbsp;")
						end if
						%><br />
					</td>
					<td align="right" class="txt11" dir="ltr" nowrap style="text-align:right">
						<%
						if int(""&iReader("Type"))=1 then
							response.write (dbPages.FormatCurr(iReader("CurrencyID"),iReader("Amount")))
						Else
							Response.Write("&nbsp;")
						end if
						%>
					</td>
					<td align="right" class="txt11" dir="ltr" nowrap style="color:<%= sFontColor %>;text-align:right">
						<%Response.Write( dbPages.FormatCurr(iReader("CurrencyID"),iReader("Balance"))) %>
					</td>																				
					<td align="center" class="txt11" style="width:20%">						
							<span class="txt12" dir="rtl">
								<%Response.Write( dbPages.dbtextShow(iReader("Comment"))) %>
							</span>												
					</td>
				</tr>
				<tr>
					<td height="1" colspan="6" bgcolor="#c5c5c5"></td>
				</tr>    
    <%
		dBalance = iReader("Balance")
    End While
    If nCustomerID > 0 and dBalance <> 0D then
		Response.Write ("<tr><td colspan=""6"">Account Balance :" & dbPages.FormatCurr(tmpCurrency,dBalance) & "</td></tr>" )
    End If
    iReader.Close
    %>
    </table>    
</body>
</html>
