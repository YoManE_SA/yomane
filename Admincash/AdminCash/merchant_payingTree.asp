<%
Server.ScriptTimeout = 6000

nNumDays = 1
nPayDate = CDate(Left(DateAdd("d", 1, Now), 10))

nCurrency = TestNumVar(Request("Currency"), 0, MAX_CURRENCY, 0)
If trim(request("Action"))="SEARCH" Then nPayDate = CDate(Request("PayDate"))

Function GetDateName(xDate)
	xDiff = Cint(DateDiff("d", Date, xDate))
	If xDiff = 0 Then 
		GetDateName = "(Today)<br />"
	ElseIf xDiff < 0 Then
		If xDiff = -1 Then GetDateName = "(Yesterday)<br />" _
		Else GetDateName = "(" & -xDiff & " Days ago)<br />"
	Else
		If xDiff = 1 Then GetDateName = "(Tomorrow)<br />" _
		Else : GetDateName = "(" & xDiff & " More days)<br />"
	End If
End Function
%>
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_controls.asp"-->
<!--#include file="../include/func_security.asp" -->
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/styleAdminEng.css" rel="stylesheet" type="text/css" />
	<script language="JavaScript" src="../js/func_common.js"></script>
	<script language="JavaScript">
		function toggleEditing(objId,CompanyID, mpd, mtoDate, nMaxAmount) {	
			objRef = document.getElementById("Row" + objId )
			if (objRef.innerHTML == '') {
				objRef.innerHTML = '<br /><iframe src="merchant_listExtra.asp?company_id=' + CompanyID + '&Currency=<%=nCurrency%>&PaymentDate=' + mpd + '&PayToPD=' + mtoDate + '&MaxTransAmount=' + nMaxAmount + '&showTerminal=0" width="100%" height="0px" framespacing="0" marginwidth="0" frameborder="0" border="0" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe><br />';
				objRef.style.borderTop = '1px dashed silver';
			}else {
				objRef.innerHTML = '';
				objRef.style.borderTop = '';
			}
		}
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" bgcolor="#eeeeee">
<br />
<table align="center" style="width:92%;">
<tr>
    <td id="pageMainHeadingTd"><span id="pageMainHeading">SETTLE PAYOUTS</span> </td>
	<td>
		<table border="0" cellspacing="0" cellpadding="1" align="right">
		<tr>
			<td width="5" bgcolor="green"></td>
			<td width="60">To Settle</td>
			<td width="5" bgcolor="#b3b3b3"></td>
			<td width="100">Already Payed</td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<br />
<%
pageSecurityLevel = 0
pageClosingTags = ""
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
%>
<form method="post">
<table style="border:1px solid gray; width:92%;" align="center">
<tr>
	<td class="txt12" style="padding:4px;">
		Date:
		<select name="PayDate" class="input2">
			<% 
			For n = 0 To 10 
				nDate = Left(DateAdd("d", n, Now), 10)
				Response.Write("<option value=""" & nDate & """ " & IIF(nDate = Cstr(nPayDate), " selected", "") & ">" & nDate)
			Next
			nDate = Left(DateAdd("d", 20, Now), 10)
			Response.Write("<option value=""" & nDate & """ " & IIF(nDate = Cstr(nPayDate), " selected", "") & ">" & nDate)
			nDate = Left(DateAdd("d", 30, Now), 10)
			Response.Write("<option value=""" & nDate & """ " & IIF(nDate = Cstr(nPayDate), " selected", "") & ">" & nDate)
			%>
		</select>&nbsp;
		Currency:
		<select name="currency" class="input2">
			<%
			For i = 0 To MAX_CURRENCY
				Response.Write("<option value=""" & i & """ " & IIF(i = nCurrency, " selected", "") & ">" & GetCurISO(i))
			Next
			%>
		</select>
		Payout Precent:
		<select name="PayoutPrecent" class="input2">
		    <option value="100"> 100%
		    <option value="-100" <%If Request("PayoutPrecent") = "-100" Then Response.Write("selected")%>> &lt; 100%
		</select>
	</td>
	<td>
		<input type="submit" value="SEARCH" name="Action" class="button1">
	</td>
</tr>
</table>
</form>
<br />
<div align="center" id="waitMsg">
	<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="txt12" style="color:gray;">Loading &nbsp;-&nbsp; Please Wait<br /></td>
			</tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><img src="../images/loading_animation_orange2.gif" style="border:1px solid silver;" alt="" width="123" height="6" border="0"><br /></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
</div>
<% Response.Flush %>
<table class="formNormal" style="width:92%;" align="center">
<tr>
	<th style="background-color:#d8d8d8;"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></th>
	<th style="background-color:#d8d8d8;">Merchant<br /></th>
	<th style="background-color:#d8d8d8;">Trans to pay /<br />Trans to hold</th>
	<th style="background-color:#d8d8d8;">100% to pay /<br />100% to hold</th>
	<th style="background-color:#d8d8d8;">Rolling R.<br />Pay/hold ratio<br /></th>
	<th style="background-color:#d8d8d8;">Last<br />Settlement<br /></th>
	<th style="background-color:#d8d8d8; text-align:center;">Info<br /></th>
</tr>
<%
If Request.ServerVariables("REQUEST_METHOD")="POST" Then
	Dim xWhere : xWhere = " And MerchantPD <= '" & nPayDate & " 23:59:59'"
	xWhere = xWhere & " And PayPercent" & IIF(Request("PayoutPrecent") = "100", "=100", "<100")

    '"DECLARE @tbl TABLE(ID int, minSettleAmount money);" & vbCrLf & _
    '"INSERT INTO @tbl SELECT tblCompany.ID, IsNull(mcs.MinSettlementAmount, 0) From tblCompany LEFT JOIN Setting.SetMerchantSettlement AS mcs ON(tblCompany.ID = mcs.Merchant_ID) WHERE IsNull(mcs.IsShowToSettle, 1)=1 AND IsNull(mcs.Currency_ID, " & nCurrency & ")=" & nCurrency & vbCrLf & _
	'"Select tblCompany.ID CompanyID, Currency, CompanyName, SecurityDeposit, PayingDaysMargin, PayingDaysMarginInitial, " & vbCrLf & _

	Dim totalToPaySql: totalToPaySql = "(Sum(floor(UnsettledAmount / UnsettledInstallments) + (UnsettledAmount % UnsettledInstallments)))"
    sSQL = "SET NOCOUNT ON;" & vbCrLf & _
    "DECLARE @tbl TABLE(ID int, minSettleAmount money);" & vbCrLf & _
    "INSERT INTO @tbl Select Merchant_ID, MinSettlementAmount From Setting.SetMerchantSettlement Where Currency_ID=" & nCurrency & " And IsShowToSettle=1;" & vbCrLf & _
	"SELECT tblCompany.ID CompanyID, Currency, CompanyName, SecurityDeposit, PayingDaysMargin, PayingDaysMarginInitial, " & vbCrLf & _
    " Count(*) As numTransToPay, PayPercent, " & vbCrLf & _
	" Sum(netpayFee_transactionCharge + netpayFee_ratioCharge + netpayFee_chbCharge + netpayFee_ClrfCharge) As SumFees, " & vbCrLf & _
	" " & totalToPaySql & " As SumTransToPay" & vbCrLf & _
	" FROM @tbl t INNER JOIN tblCompany WITH (NOLOCK) ON t.ID=tblCompany.ID" & vbCrLf & _
	" INNER JOIN tblCompanyTransPass WITH (NOLOCK) ON tblCompany.ID=tblCompanyTransPass.CompanyID" & vbCrLf & _
	" Where IsTestOnly=0 And UnsettledInstallments <> 0 And Currency=" & nCurrency & " " & xWhere & vbCrLf
	If PageSecurity.IsLimitedMerchant Then sSQL = sSQL & " AND CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))" & vbCrLf
	sSQL = sSQL & _
    " Group By tblCompany.ID, CompanyName, Currency, SecurityDeposit, PayingDaysMargin, PayingDaysMarginInitial, PayPercent, t.minSettleAmount" & vbCrLf & _
    " Having " & totalToPaySql & " >= t.minSettleAmount" & vbCrLf & _
	" Order By numTransToPay Desc;" & vbCrLf & _
    " SET NOCOUNT OFF;" & vbCrLf
	'Response.Write(sSQL & "<br />")
    'Response.Flush
	oledbData.CommandTimeout = 120
	Set rsData = oledbData.Execute(sSQL)
	if Not rsData.EOF then
		xLastDate = "" : nComCount = 0 : nMerchantCount = 0 : TransSum = 0 : RRSum = 0
		Do Until rsData.EOF
			Dim bShow, bPayed, lPayDate
			lPayDate = ExecScalar("Select Max(PayDate) From tblTransactionPay Where CompanyID=" & rsData("CompanyID") & " And currency=" & nCurrency, DateSerial(1900, 1, 1))
			lPayDate = CDate(Left(IIF(IsNull(lPayDate), DateSerial(1900, 1, 1), lPayDate), 10))
			bShow = True '(lPayDate < DateSerial(1960, 1, 1)) Or (DateDiff("d", lPayDate, Now) <= 90)
			If bShow Then
				If rsData("PayingDaysMargin") > 0 Then
					bShow = DateDiff("d", lPayDate, nPayDate) >= rsData("PayingDaysMargin")
					bPayed = DateDiff("d", lPayDate, nPayDate) < rsData("PayingDaysMargin")
				End If
			End If
			
			If bShow Then
				PayPercent = TestNumVar(rsData("PayPercent"), 0, -1, 0) / 100
		        PayAmount = rsData("SumTransToPay") - rsData("SumFees")
		        
		        If xLastDate <> nPayDate Then 
    		        xLastDate = nPayDate
    		        If nMerchantCount > 0 Then Response.Write("<tr><td colspan=""2"" style=""padding-bottom:10px;"">Count: " & nMerchantCount & "</td><td style=""padding-bottom:10px;"">" & FormatCurr(nCurrency, TransSum) & "</td></tr>") _
    		        Else Response.Write("<tr><td colspan=""6"" height=""20""></td></tr>")
    		        nMerchantCount = 0 : TransSum = 0 : RRSum = 0
		            %>
		            <tr><td colspan="7" class="txt13b"><%= xLastDate %> &nbsp;<%= GetDateName(xLastDate) %></td></tr>
		            <%
		            response.write "<tr><td height=""1"" colspan=""7"" bgcolor=""silver""></td></tr>"
		        End If	
		        Set iRs = oleDbData.Execute("Select Sum(UnsettledAmount), Count(*) From tblCompanyTransPass Where CompanyID=" & rsData("companyID") & " And Currency=" & rsData("Currency") & " And IsTestOnly=0 And UnsettledInstallments>0")
		        If Not iRs.EOF Then
					fullAmount = TestNumVar(iRs(0), 0, -1, 0)
					fullCount = TestNumVar(iRs(1), 0, -1, 0)
		        End If
		        iRs.Close()
				Dim nRRSum : nRRSum = 0
				iRs.Open "Select CreditType, Sum(Amount) From tblCompanyTransPass Where CompanyID=" & rsData("companyID") & " And Currency=" & rsData("Currency") & " And PaymentMethod=" & PMD_RolRes & " Group By CreditType", oleDbData, 0, 1
				Do While Not iRs.EOF
					nRRSum = nRRSum + IIF(iRs("CreditType") = 0, iRs(1), -iRs(1))
				 iRs.MoveNext
				Loop
				iRs.Close

		        isNeedCompanyPay = False
		        nComCount = nComCount + 1 : nMerchantCount = nMerchantCount + 1
		        TransSum = TransSum + PayAmount
		        RRSum = RRSum + nRRSum
		        If bPayed Then
			        sStyleColor = "color:#515151;"
			        sStyleBkgColor = "background-color:#b3b3b3;"
		        ElseIf PayPercent <> 1 Then
			        sStyleColor = ""
			        sStyleBkgColor = "background-color:blue;"
		        Else
			        sStyleColor = ""
			        sStyleBkgColor = "background-color:green;"
		        End If
		        
		        If fullAmount = 0 Then fullAmount = PayAmount
		        If fullAmount > 0 Then
					If (PayAmount / (fullAmount + nRRSum)) > 0.60 Then sStyleBkgColor = "background-color:orange;"
					If (PayAmount / (fullAmount + nRRSum)) > 0.75 Then sStyleBkgColor = "background-color:red;"
				End if	
		        %>
		        <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
			        <td width="2" style="<%= sStyleBkgColor %>">
       					<%'=DateDiff("d", Now, nPayDate) & "<br />" %>
			        </td>
			        <td class="txt12" style="cursor:pointer; <%= sStyleColor %>;" onclick="parent.frmBody.location.href='merchant_data.asp?companyID=<%= rsData("companyID") %>';parent.fraButton.FrmResize();" onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';">
				        <%= rsData("companyID") %><br />
				        <%
				        if trim(rsData("CompanyName"))="" or trim(rsData("CompanyName"))="-" then
					        sShowName = "(" & trim(rsData("firstName")) & " " & trim(rsData("lastName")) & ")"
				        Else
					        sShowName = trim(rsData("CompanyName"))
				        End If
				        If int(len(dbtextShow(sShowName)))<14 then
					        response.write dbTextShow(sShowName) & "<br />"
				        else
					        response.write "<span title="""& dbtextShow(sShowName) & """>" & left(dbtextShow(sShowName),13) & "..</span><br />"
				        end if
				        %>
			        </td>
			        <td style="white-space:nowrap; cursor:pointer; <%= sStyleColor %>" onclick="parent.frmBody.location.href='merchant_transPassFilter.asp?companyID=<%= rsData("companyID") %>&Currency=<%= rsData("Currency") %>&ShowName=<%= sShowName %>';" onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';">							
				        <%If PayPercent <> 1 Then %>~
							<b><%= FormatCurr(rsData("Currency"), PayAmount * PayPercent) %></b>  (<%=PayPercent * 100%>%)<br />
							<%= FormatCurr(rsData("Currency"), (fullAmount - PayAmount) * PayPercent) %>
				        <%Else%>
					        <b><%= FormatCurr(rsData("Currency"), PayAmount) %></b> (<%= rsData("numTransToPay") %>)<br />
					        <%= FormatCurr(rsData("Currency"), fullAmount - PayAmount) %> (<%= fullCount - rsData("numTransToPay")%>)<br />
				        <%End If%>
			        </td>
			        <td>
				        <%If PayPercent <> 1 Then %>
					        <b><%= FormatCurr(rsData("Currency"), PayAmount) %></b> (<%= rsData("numTransToPay") %>)<br />
					        <%= FormatCurr(rsData("Currency"), fullAmount - PayAmount) %> (<%= fullCount - rsData("numTransToPay")%>)<br />
				        <%End if%>
			        </td>
			        <td style="<%= sStyleColor %>" nowrap>
				        <%=trim(rsData("SecurityDeposit"))%> % / <%=FormatCurr(rsData("Currency"), nRRSum)%><br />
						<b>
						<%
						If PayAmount <> 0 And (fullAmount + nRRSum) <> 0 Then _
							Response.Write FormatNumber((PayAmount / (fullAmount + nRRSum)) * 100, 1, True, False, True) _
						Else Response.Write FormatNumber(0, 1, True, False, True)
						%> %</b><br />
					</td>
					<%
			        If lPayDate > DateSerial(1960, 1, 1) Then
				        %>
				        <td style="cursor:pointer; <%= sStyleColor %>" onclick="parent.frmBody.location.href='Merchant_transSettledListData.asp?Source=filter&ShowCompanyID=<%= rsData("companyID") %>';parent.fraButton.FrmResize();" onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';">
					        <%=FormatDatesTimes(lPayDate,1,0,0)  %><br />
					        <%=FormatCurr(rsData("Currency"), TestNumVar(ExecScalar("Select transTotal From tblTransactionPay Where ID=(SELECT Max(ID) FROM tblTransactionPay WHERE CompanyID=" & rsData("companyID") & " And Currency=" & rsData("Currency") & ")", "0"), 0, -1, 0))%><br />
				        </td>
				        <%
			        Else
				        %>
				        <td style="<%= sStyleColor %>">- - -<br /></td>
				        <%
			        End If
					%>
		            <td style="cursor:pointer; text-align:center;" 
			            onclick="toggleEditing('<%=nComCount%>', <%=rsData("companyID")%>, '<%=nPayDate%>', '<%=xLastDate%>', <%=IIF(PayPercent <> 1, Round(PayAmount * PayPercent, 2), 0) %>);"
			            onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';">
			            <img src="../images/b_showWhiteEng.gif" align="middle" /><br />
		            </td>
		        </tr>
		        <tr><td id="Row<%=nComCount%>" colspan="7"></td></tr>
		        <%
		        response.write "<tr><td height=""1"" colspan=""7"" bgcolor=""silver""></td></tr>"
            End if	
        rsData.movenext
        Loop
	End if
    If nMerchantCount > 0 Then Response.Write("<tr><td colspan=""2"">Count: " & nMerchantCount & "</td><td></td><td>" & FormatCurr(nCurrency, TransSum) & "</td><td>" & FormatCurr(nCurrency, RRSum) & "</td></tr>")
    rsData.close
    Set rsData = nothing	
Else
	response.write "<tr><td colspan=""7""><br />Click on search button<br /></td></tr>"
End if
call closeConnection()
Server.ScriptTimeout = 90
%>
</table>
<br />
</body>
</html>
<script language="javascript">
	document.getElementById("waitMsg").style.display = 'none';
</script>
