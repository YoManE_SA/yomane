﻿<%@ Control Language="VB" ClassName="MerchantBankAccount" EnableViewState="true" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
	Private nRefID As Integer = 0
    Private bIsMerchant As Boolean
	Private nCurrency As Integer = -1
	Private bIsCorrespondent As Boolean = False

	Public Property Merchant() As Integer
		Get
			Return nRefID
		End Get
		Set(ByVal value As Integer)
            bIsMerchant = True
			nRefID = value
		End Set
	End Property

	Public Property Affiliate() As Integer
		Get
			Return nRefID
		End Get
		Set(ByVal value As Integer)
            bIsMerchant = False
			nRefID = value
		End Set
	End Property
    
	Public Property Currency() As eCurrencies
		Get
			Return nCurrency
		End Get
		Set(ByVal value As eCurrencies)
			nCurrency = value
			If nCurrency >= 0 And nCurrency <= eCurrencies.MAX_CURRENCY Then
				litName.Text = dbPages.GetCurISO(nCurrency)
			Else
				litName.Text = "Default"
			End If
		End Set
	End Property

	Public Property IsCorrespondent() As Boolean
		Get
			Return bIsCorrespondent
		End Get
		Set(ByVal value As Boolean)
			bIsCorrespondent = value
			litType.Text = IIf(bIsCorrespondent, "Correspondent", "Primary")
		End Set
	End Property

	Public Property ABA() As String
		Get
			Return txtABA.Text
		End Get
		Set(ByVal value As String)
			txtABA.Text = value
		End Set
	End Property

	Public Property AccountName() As String
		Get
			Return txtAccountName.Text
		End Get
		Set(ByVal value As String)
			txtAccountName.Text = value
		End Set
	End Property

	Public Property AccountNumber() As String
		Get
			Return txtAccountNumber.Text
		End Get
		Set(ByVal value As String)
			txtAccountNumber.Text = value
		End Set
	End Property

	Public Property BankAddress() As String
		Get
			Return txtBankAddress.Text
		End Get
		Set(ByVal value As String)
			txtBankAddress.Text = value
		End Set
	End Property

	Public Property BankAddressCity() As String
		Get
			Return txtBankAddressCity.Text
		End Get
		Set(ByVal value As String)
			txtBankAddressCity.Text = value
		End Set
	End Property

	Public Property BankAddressCountry() As Integer
		Get
			Return ddlBankAddressCountry.SelectedValue
		End Get
		Set(ByVal value As Integer)
			ddlBankAddressCountry.SelectedValue = value
		End Set
	End Property

	Public Property BankAddressSecond() As String
		Get
			Return txtBankAddressSecond.Text
		End Get
		Set(ByVal value As String)
			txtBankAddressSecond.Text = value
		End Set
	End Property

	Public Property BankAddressState() As String
		Get
			Return txtBankAddressState.Text
		End Get
		Set(ByVal value As String)
			txtBankAddressState.Text = value
		End Set
	End Property

	Public Property BankAddressZip() As String
		Get
			Return txtBankAddressZip.Text
		End Get
		Set(ByVal value As String)
			txtBankAddressZip.Text = value
		End Set
	End Property

	Public Property BankName() As String
		Get
			Return txtBankName.Text
		End Get
		Set(ByVal value As String)
			txtBankName.Text = value
		End Set
	End Property

	Public Property IBAN() As String
		Get
			Return txtIBAN.Text
		End Get
		Set(ByVal value As String)
			txtIBAN.Text = value
		End Set
	End Property

	Public Property SepaBic() As String
		Get
			Return txtSepaBic.Text
		End Get
		Set(ByVal value As String)
			txtSepaBic.Text = value
		End Set
	End Property

	Public Property SortCode() As String
		Get
			Return txtSortCode.Text
		End Get
		Set(ByVal value As String)
			txtSortCode.Text = value
		End Set
	End Property

	Public Property SwiftNumber() As String
		Get
			Return txtSwiftNumber.Text
		End Get
		Set(ByVal value As String)
			txtSwiftNumber.Text = value
		End Set
	End Property

	Public Sub ClearValues(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		txtAccountName.Text = String.Empty
		txtAccountNumber.Text = String.Empty
		txtBankName.Text = String.Empty
		txtBankAddress.Text = String.Empty
		txtBankAddressSecond.Text = String.Empty
		txtBankAddressCity.Text = String.Empty
		txtBankAddressState.Text = String.Empty
		ddlBankAddressCountry.SelectedIndex = 0
		txtSwiftNumber.Text = String.Empty
		txtIBAN.Text = String.Empty
		txtABA.Text = String.Empty
		txtSortCode.Text = String.Empty
		txtBankAddressZip.Text = String.Empty
		txtSepaBic.Text = String.Empty
	End Sub

	Public ReadOnly Property IsEmpty() As Boolean
		Get
			If Not String.IsNullOrEmpty(txtAccountName.Text) Then Return False
			If Not String.IsNullOrEmpty(txtAccountNumber.Text) Then Return False
			If Not String.IsNullOrEmpty(txtBankName.Text) Then Return False
			If Not String.IsNullOrEmpty(txtBankAddress.Text) Then Return False
			If Not String.IsNullOrEmpty(txtBankAddressSecond.Text) Then Return False
			If Not String.IsNullOrEmpty(txtBankAddressCity.Text) Then Return False
			If Not String.IsNullOrEmpty(txtBankAddressState.Text) Then Return False
			If ddlBankAddressCountry.SelectedIndex > 0 Then Return False
			If Not String.IsNullOrEmpty(txtSwiftNumber.Text) Then Return False
			If Not String.IsNullOrEmpty(txtIBAN.Text) Then Return False
			If Not String.IsNullOrEmpty(txtABA.Text) Then Return False
			If Not String.IsNullOrEmpty(txtSortCode.Text) Then Return False
			If Not String.IsNullOrEmpty(txtBankAddressZip.Text) Then Return False
			If Not String.IsNullOrEmpty(txtSepaBic.Text) Then Return False
			Return True
		End Get
	End Property

	Public Sub SaveToMerchant(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		If nRefID > 0 And nCurrency > -1 Then
			Dim sbSQL As New StringBuilder("EXEC " & IIF(bIsMerchant, "SaveMerchantBankAccount ", "SaveAffiliateBankAccount ").ToString())
			sbSQL.AppendFormat("{0}, {1}, {2},", nRefID, nCurrency, IIf(bIsCorrespondent, 2, 1))
			sbSQL.AppendFormat("'{0}', '{1}', '{2}',", dbPages.DBText(txtAccountName.Text), dbPages.DBText(txtAccountNumber.Text), dbPages.DBText(txtBankName.Text))
			sbSQL.AppendFormat("'{0}', '{1}', '{2}',", dbPages.DBText(txtBankAddress.Text), dbPages.DBText(txtBankAddressSecond.Text), dbPages.DBText(txtBankAddressCity.Text))
			sbSQL.AppendFormat("'{0}', {1}, '{2}',", dbPages.DBText(txtBankAddressState.Text), ddlBankAddressCountry.SelectedValue, dbPages.DBText(txtSwiftNumber.Text))
			sbSQL.AppendFormat("'{0}', '{1}', '{2}',", dbPages.DBText(txtIBAN.Text), dbPages.DBText(txtABA.Text), dbPages.DBText(txtSortCode.Text))
			sbSQL.AppendFormat("'{0}', '{1}'", dbPages.DBText(txtBankAddressZip.Text), dbPages.DBText(txtSepaBic.Text))
			dbPages.ExecSql(sbSQL.ToString)
		End If
	End Sub

	Public Sub SaveToDefault(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		If nRefID > 0 Then
			Dim sbSQL As New StringBuilder("EXEC " & IIF(bIsMerchant, "SaveMerchantBankAccount ", "SaveAffiliateBankAccount ").ToString())
			sbSQL.AppendFormat("{0}, {1}, {2},", nRefID, "DEFAULT", IIf(bIsCorrespondent, 2, 1))
			sbSQL.AppendFormat("'{0}', '{1}', '{2}',", dbPages.DBText(txtAccountName.Text), dbPages.DBText(txtAccountNumber.Text), dbPages.DBText(txtBankName.Text))
			sbSQL.AppendFormat("'{0}', '{1}', '{2}',", dbPages.DBText(txtBankAddress.Text), dbPages.DBText(txtBankAddressSecond.Text), dbPages.DBText(txtBankAddressCity.Text))
			sbSQL.AppendFormat("'{0}', {1}, '{2}',", dbPages.DBText(txtBankAddressState.Text), ddlBankAddressCountry.SelectedValue, dbPages.DBText(txtSwiftNumber.Text))
			sbSQL.AppendFormat("'{0}', '{1}', '{2}',", dbPages.DBText(txtIBAN.Text), dbPages.DBText(txtABA.Text), dbPages.DBText(txtSortCode.Text))
			sbSQL.AppendFormat("'{0}', '{1}'", dbPages.DBText(txtBankAddressZip.Text), dbPages.DBText(txtSepaBic.Text))
			dbPages.ExecSql(sbSQL.ToString)
		End If
	End Sub

	Public Sub SaveToWire(ByVal nWire As Integer)
		If nRefID > 0 And nCurrency > -1 Then
			Dim sbSQL As New StringBuilder("EXEC SaveWireBankAccount ")
			sbSQL.AppendFormat("{0}, {1},", nWire, IIf(bIsCorrespondent, 2, 1))
			sbSQL.AppendFormat("'{0}', '{1}', '{2}',", dbPages.DBText(txtAccountName.Text), dbPages.DBText(txtAccountNumber.Text), dbPages.DBText(txtBankName.Text))
			sbSQL.AppendFormat("'{0}', '{1}', '{2}',", dbPages.DBText(txtBankAddress.Text), dbPages.DBText(txtBankAddressSecond.Text), dbPages.DBText(txtBankAddressCity.Text))
			sbSQL.AppendFormat("'{0}', {1}, '{2}',", dbPages.DBText(txtBankAddressState.Text), ddlBankAddressCountry.SelectedValue, dbPages.DBText(txtSwiftNumber.Text))
			sbSQL.AppendFormat("'{0}', '{1}', '{2}',", dbPages.DBText(txtIBAN.Text), dbPages.DBText(txtABA.Text), dbPages.DBText(txtSortCode.Text))
			sbSQL.AppendFormat("'{0}', '{1}', {2}", dbPages.DBText(txtBankAddressZip.Text), dbPages.DBText(txtSepaBic.Text), nRefID)
			dbPages.ExecSql(sbSQL.ToString)
		End If
	End Sub

	Public Sub RemoveFromDB(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		If nRefID > 0 And nCurrency > -1 Then dbPages.ExecSql("EXEC " & IIF(bIsMerchant, "RemoveMerchantBankAccount ", "RemoveAffiliateBankAccount ") & nRefID & "," & nCurrency)
	End Sub
	
	Public Sub LoadFromMerchant(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		dsCountry.ConnectionString = dbPages.DSN
		ddlBankAddressCountry.DataBind()
		ClearValues()
		If nRefID > 0 And nCurrency > -1 Then
			Dim sSQL As String = "EXEC " & IIF(bIsMerchant, "LoadMerchantBankAccount ", "LoadAffiliateBankAccount ") & nRefID & ", " & nCurrency & ", " & IIf(bIsCorrespondent, 2, 1)
			Dim drData As SqlDataReader = dbPages.ExecReader(sSQL, dbPages.DSN)
			If drData.Read Then
				txtAccountName.Text = IIF(drData("AccountName") Is DBNull.Value, "", drData("AccountName")) 
				txtAccountNumber.Text = IIF(drData("AccountNumber") Is DBNull.Value, "", drData("AccountNumber")) 
				txtBankName.Text = IIF(drData("BankName") Is DBNull.Value, "", drData("BankName")) 
				txtBankAddress.Text = IIF(drData("BankAddress") Is DBNull.Value, "", drData("BankAddress")) 
				txtBankAddressSecond.Text = IIF(drData("BankAddressSecond") Is DBNull.Value, "", drData("BankAddressSecond")) 
				txtBankAddressCity.Text = IIF(drData("BankAddressCity") Is DBNull.Value, "", drData("BankAddressCity")) 
				txtBankAddressState.Text = IIF(drData("BankAddressState") Is DBNull.Value, "", drData("BankAddressState")) 
				ddlBankAddressCountry.SelectedValue = IIF(drData("BankAddressCountry") Is DBNull.Value, 0, drData("BankAddressCountry")) 
				txtSwiftNumber.Text = IIF(drData("SwiftNumber") Is DBNull.Value, "", drData("SwiftNumber")) 
				txtIBAN.Text = IIF(drData("IBAN") Is DBNull.Value, "", drData("IBAN")) 
				txtABA.Text = IIF(drData("ABA") Is DBNull.Value, "", drData("ABA")) 
				txtSortCode.Text = IIF(drData("SortCode") Is DBNull.Value, "", drData("SortCode")) 
				txtBankAddressZip.Text = IIF(drData("BankAddressZip") Is DBNull.Value, "", drData("BankAddressZip")) 
				txtSepaBic.Text = IIF(drData("SepaBic") Is DBNull.Value, "", drData("SepaBic")) 
			End If
			drData.Close()
			litType.Text = IIf(IsCorrespondent, "Correspondent", "Primary")
			litName.Text = dbPages.GetCurISO(nCurrency)
		End If
	End Sub
	
	Public Sub LoadFromDefault(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		dsCountry.ConnectionString = dbPages.DSN
		ddlBankAddressCountry.DataBind()
		ClearValues()
		If nRefID > 0 Then
			Dim sSQL As String = "EXEC " & IIF(bIsMerchant, "LoadMerchantBankAccount ", "LoadAffiliateBankAccount ") & nRefID & ", DEFAULT, " & IIf(bIsCorrespondent, 2, 1)
			Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
			If drData.Read Then
				txtAccountName.Text = IIF(drData("AccountName") Is DBNull.Value, "", drData("AccountName"))
				txtAccountNumber.Text = IIF(drData("AccountNumber") Is DBNull.Value, "", drData("AccountNumber"))
				txtBankName.Text = IIF(drData("BankName") Is DBNull.Value, "", drData("BankName"))
				txtBankAddress.Text = IIF(drData("BankAddress") Is DBNull.Value, "", drData("BankAddress"))
				txtBankAddressSecond.Text = IIF(drData("BankAddressSecond") Is DBNull.Value, "", drData("BankAddressSecond"))
				txtBankAddressCity.Text = IIF(drData("BankAddressCity") Is DBNull.Value, "", drData("BankAddressCity"))
				txtBankAddressState.Text = IIF(drData("BankAddressState") Is DBNull.Value, "", drData("BankAddressState"))
				ddlBankAddressCountry.SelectedValue = IIF(drData("BankAddressCountry") Is DBNull.Value, 0, drData("BankAddressCountry"))
				txtSwiftNumber.Text = IIF(drData("SwiftNumber") Is DBNull.Value, "", drData("SwiftNumber"))
				txtIBAN.Text = IIF(drData("IBAN") Is DBNull.Value, "", drData("IBAN"))
				txtABA.Text = IIF(drData("ABA") Is DBNull.Value, "", drData("ABA"))
				txtSortCode.Text = IIF(drData("SortCode") Is DBNull.Value, "", drData("SortCode"))
				txtBankAddressZip.Text = IIF(drData("BankAddressZip") Is DBNull.Value, "", drData("BankAddressZip"))
				txtSepaBic.Text = IIF(drData("SepaBic") Is DBNull.Value, "", drData("SepaBic"))
			End If
			drData.Close()
			litType.Text = IIf(IsCorrespondent, "Correspondent", "Primary")
			litName.Text = "Default"
		End If
	End Sub
	
	Public Sub LoadFromWire(ByVal nWire As Integer)
		dsCountry.ConnectionString = dbPages.DSN
		ddlBankAddressCountry.DataBind()
		ClearValues()
		If nRefID > 0 And nCurrency > -1 Then
			Dim drData As SqlDataReader = dbPages.ExecReader("EXEC LoadWireBankAccount " & nWire & ", " & IIf(bIsCorrespondent, 2, 1) & ", NULL")
			If drData.Read Then
				txtAccountName.Text = drData("AccountName")
				txtAccountNumber.Text = drData("AccountNumber")
				txtBankName.Text = drData("BankName")
				txtBankAddress.Text = drData("BankAddress")
				txtBankAddressSecond.Text = drData("BankAddressSecond")
				txtBankAddressCity.Text = drData("BankAddressCity")
				txtBankAddressState.Text = drData("BankAddressState")
				ddlBankAddressCountry.SelectedValue = drData("BankAddressCountry")
				txtSwiftNumber.Text = drData("SwiftNumber")
				txtIBAN.Text = drData("IBAN")
				txtABA.Text = drData("ABA")
				txtSortCode.Text = drData("SortCode")
				txtBankAddressZip.Text = drData("BankAddressZip")
				txtSepaBic.Text = drData("SepaBic")
			End If
			drData.Close()
			litType.Text = IIf(IsCorrespondent, "Correspondent", "Primary")
			litName.Text = dbPages.GetCurISO(nCurrency)
		End If
	End Sub
	
	Sub Page_Load()
		Dim sbJS As New StringBuilder
		If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "BankAccountValidation") Then
			Dim drData As SqlDataReader
			sbJS.Append("var sCountriesABA=""__")
			drData = dbPages.ExecReader("SELECT CountryID FROM [List].[CountryList] WHERE IsABARequired=1")
			While drData.Read
				sbJS.AppendFormat("{0}_", drData(0))
			End While
			drData.Close()
			sbJS.Append(""";var sCountriesWithStates=""__")
            drData = dbPages.ExecReader("SELECT DISTINCT stateID FROM [List].[StateList] WITH (NOLOCK)")
			While drData.Read
				sbJS.AppendFormat("{0}_", drData(0))
			End While
			drData.Close()
			sbJS.AppendLine(""";var naIBANs=new Array();var saISOCodes=new Array();")
			drData = dbPages.ExecReader("SELECT CountryID, IBANLength, CountryISOCode FROM List.CountryList WITH (NOLOCK)")
			While drData.Read
				sbJS.AppendFormat("saISOCodes[""{0}""]=""{1}"";", drData(0), drData(2))
				If drData.GetByte(1) > 0 Then
					sbJS.AppendFormat("naIBANs[""{0}""]={1};", drData(0), drData(1), drData(2))
				End If
				sbJS.AppendLine()
			End While
			drData.Close()
			sbJS.AppendLine("function GetIBANLength(nCountryID){return naIBANs[nCountryID];}")
			sbJS.AppendLine("function GetISOCode(nCountryID){return saISOCodes[nCountryID];}")
			sbJS.AppendLine("function calc_ABA_routeno_sum(r){")
			sbJS.AppendLine("	var s=0,m=0,n=new Array(3,7,1);")
			sbJS.AppendLine("	for (var i=0; i<r.length; i++) {s += parseInt(r.charAt(i))*n[m];m=++m%3;}")
			sbJS.AppendLine("	return s;}")
			sbJS.AppendLine("function check_ABA_routeno(r){")
			sbJS.AppendLine("	var s='';")
			sbJS.AppendLine("	for (var i=0; i<r.length; i++) if (!isNaN(r.charAt(i))) s+=r.charAt(i);")
			sbJS.AppendLine("	if (s.length!=9) return false;")
			sbJS.AppendLine("	return (calc_ABA_routeno_sum(s)%10==0);}")
			sbJS.AppendLine("function ReturnCheckFalseWithText(sID, sText){")
			sbJS.AppendLine("	if (sText=="""") sText=document.getElementById(sID).title;")
			sbJS.AppendLine("	alert(""Please specify ""+sText);")
			sbJS.AppendLine("	window.scrollTo(0, 10000);document.getElementById(sID).focus();window.scrollBy(0, -20);")
			sbJS.AppendLine("	return false;}")
			sbJS.AppendLine("function ReturnCheckFalse(sID){return ReturnCheckFalseWithText(sID, document.getElementById(sID).title);}")
			Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "BankAccountValidation", sbJS.ToString, True)
			sbJS.Remove(0, sbJS.Length)
		End If
		If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "BankAccountValidation" & Me.ClientID) Then
			sbJS.AppendLine("function CheckFormBankAccount" & Me.ClientID & "(){")
			If IsCorrespondent Then
				sbJS.AppendLine("	if (document.getElementById(""" & txtSwiftNumber.ClientID & """).value!=""""){")
				sbJS.AppendLine("		if (document.getElementById(""" & txtBankName.ClientID & """).value=="""") return ReturnCheckFalse(""" & txtBankName.ClientID & """);")
				sbJS.AppendLine("		var sSepaBic=document.getElementById(""" & txtSepaBic.ClientID & """).value.toUpperCase().replace(/ /g, """");")
				sbJS.AppendLine("		document.getElementById(""" & txtSepaBic.ClientID & """).value=sSepaBic;")
				sbJS.AppendLine("		if ((sSepaBic.length!=0)&&(sSepaBic.length!=8)&&(sSepaBic.length!=11))")
				sbJS.AppendLine("			return ReturnCheckFalseWithText(""" & txtSepaBic.ClientID & """, ""valid SEPA BIC (8 or 11 characters)"");")
				sbJS.AppendLine("		var sIBAN=document.getElementById(""" & txtIBAN.ClientID & """).value.toUpperCase().replace(/ /g, """");")
				sbJS.AppendLine("		document.getElementById(""" & txtIBAN.ClientID & """).value=sIBAN;")
				sbJS.AppendLine("	}")
				sbJS.AppendLine("	return true;}")
			Else
				sbJS.AppendLine("	if (document.getElementById(""" & txtAccountName.ClientID & """).value!=""""){")
				sbJS.AppendLine("		if (document.getElementById(""" & txtAccountNumber.ClientID & """).value=="""") return ReturnCheckFalse(""" & txtAccountNumber.ClientID & """);")
				sbJS.AppendLine("		if (document.getElementById(""" & txtBankName.ClientID & """).value=="""") return ReturnCheckFalse(""" & txtBankName.ClientID & """);")
				sbJS.AppendLine("		if (document.getElementById(""" & txtBankAddress.ClientID & """).value=="""") return ReturnCheckFalse(""" & txtBankAddress.ClientID & """);")
				sbJS.AppendLine("		if (document.getElementById(""" & txtBankAddressCity.ClientID & """).value=="""") return ReturnCheckFalse(""" & txtBankAddressCity.ClientID & """);")
				sbJS.AppendLine("		var nCountry=document.getElementById(""" & ddlBankAddressCountry.ClientID & """).value;")
				sbJS.AppendLine("		if ((sCountriesABA.indexOf(""_""+nCountry+""_"")>0)&&(document.getElementById(""" & txtBankAddressState.ClientID & """).value==""""))")
				sbJS.AppendLine("			return ReturnCheckFalse(""" & txtBankAddressState.ClientID & """);")
				sbJS.AppendLine("		if (document.getElementById(""" & ddlBankAddressCountry.ClientID & """).selectedIndex==0) return ReturnCheckFalse(""" & ddlBankAddressCountry.ClientID & """);")
				sbJS.AppendLine("		//if (document.getElementById(""" & txtSwiftNumber.ClientID & """).value=="""") return ReturnCheckFalse(""" & txtSwiftNumber.ClientID & """);")
				sbJS.AppendLine("		var sISOCode=GetISOCode(nCountry);")
				sbJS.AppendLine("		var sSepaBic=document.getElementById(""" & txtSepaBic.ClientID & """).value.toUpperCase().replace(/ /g, """");")
				sbJS.AppendLine("		document.getElementById(""" & txtSepaBic.ClientID & """).value=sSepaBic;")
				sbJS.AppendLine("		if (sSepaBic.length!=0){")
				sbJS.AppendLine("		if ((sSepaBic.length!=8)&&(sSepaBic.length!=11))")
				sbJS.AppendLine("			return ReturnCheckFalseWithText(""" & txtSepaBic.ClientID & """, ""valid SEPA BIC (8 or 11 characters)"");")
				sbJS.AppendLine("		if (sSepaBic.substr(4, 2)!=sISOCode)")
				sbJS.AppendLine("			return ReturnCheckFalseWithText(""" & txtSepaBic.ClientID & """, ""valid SEPA BIC, containing '""+sISOCode+""', e.g. 'BANK""+sISOCode+""CT' or 'BANK""+sISOCode+""CT123')"");")
				sbJS.AppendLine("		}")
				sbJS.AppendLine("		var sIBAN=document.getElementById(""" & txtIBAN.ClientID & """).value.toUpperCase().replace(/ /g, """");")
				sbJS.AppendLine("		document.getElementById(""" & txtIBAN.ClientID & """).value=sIBAN;")
				sbJS.AppendLine("		var nIBANLength=GetIBANLength(nCountry);")
				sbJS.AppendLine("		if (nIBANLength > 0){")
				sbJS.AppendLine("			if ((sIBAN.length!=nIBANLength)||(sIBAN.substr(0, 2)!=sISOCode))")
				sbJS.AppendLine("				return ReturnCheckFalseWithText(""" & txtIBAN.ClientID & """, ""IBAN (""+nIBANLength+"" characters, starting with '""+sISOCode+""')"");")
				sbJS.AppendLine("		}")
				sbJS.AppendLine("		if ((sCountriesABA.indexOf(""_""+nCountry+""_"")>0)&&(!check_ABA_routeno(document.getElementById(""" & txtABA.ClientID & """).value)))")
				sbJS.AppendLine("			return ReturnCheckFalseWithText(""" & txtABA.ClientID & """, ""valid bank ABA (9 digits)"");")
				sbJS.AppendLine("	}")
				sbJS.AppendLine("	return true;}")
			End If
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "BankAccountValidation" & Me.ClientID, sbJS.ToString, True)
		End If
	End Sub
</script>
<table border="0" cellpadding="1" cellspacing="0" class="formNormal">
	<tr>
		<td colspan="2">
			<div class="MerchantSubHead">
				<asp:Literal ID="litType" Text="Empty" runat="server" />
				Bank Account -
				<asp:Literal ID="litName" Text="Default" runat="server" />
			</div><br />
		</td>
	</tr>
	<tr>
		<td width="40%">Account Name</td>
		<td><asp:TextBox ID="txtAccountName" Width="230px" ToolTip="Account Name" runat="server" /></td>
	</tr>
	<tr>
		<td>Account Number</td>
		<td><asp:TextBox ID="txtAccountNumber" Width="230px" ToolTip="Account Number" runat="server" /></td>
	</tr>
	<tr>
		<td>Bank Name</td>
		<td><asp:TextBox ID="txtBankName" Width="230px" ToolTip="Bank Name" runat="server" /></td>
	</tr>
	<tr>
		<td>SEPA BIC</td>
		<td><asp:TextBox ID="txtSepaBic" Width="230px" ToolTip="SEPA BIC" runat="server" /></td>
	</tr>
	<tr>
		<td>
			Address Line 1
			<AC:DivBox Text="Street address, P.O. box, company name, c/o" runat="server" />
		</td>
		<td><asp:TextBox ID="txtBankAddress" Width="230px" ToolTip="Address Line 1 (Street address, P.O. box, company name, c/o)" runat="server" /></td>
	</tr>
	<tr>
		<td>
			Address Line 2
			<AC:DivBox Text="Apartment, suite, unit, building, floor, etc." runat="server" />
		</td>
		<td><asp:TextBox ID="txtBankAddressSecond" Width="230px" ToolTip="Address Line 2 (Apartment, suite, unit, building, floor, etc.)" runat="server" /></td>
	</tr>
	<tr>
		<td>City</td>
		<td><asp:TextBox ID="txtBankAddressCity" Width="230px" ToolTip="City" runat="server" /></td>
	</tr>
	<tr>
		<td>State/Province</td>
		<td><asp:TextBox ID="txtBankAddressState" Width="230px" ToolTip="State/Province" runat="server" /></td>
	</tr>
	<tr>
		<td>ZIP (Postal) Code</td>
		<td><asp:TextBox ID="txtBankAddressZip" Width="230px" ToolTip="ZIP (Postal) Code" runat="server" /></td>
	</tr>
	<tr>
		<td>Country</td>
		<td>
			<asp:DropDownList ID="ddlBankAddressCountry" ToolTip="Country" DataSourceID="dsCountry" Width="230px" DataTextField="Name" DataValueField="ID" runat="server" />
			<asp:SqlDataSource ID="dsCountry" SelectCommand="SELECT '&lt; Select Country &gt;' Name, 0 ID UNION SELECT Name, CountryID FROM List.CountryList ORDER BY Name" runat="server" />
		</td>
	</tr>
	<tr>
		<td>Swift Code</td>
		<td colspan="3"><asp:TextBox ID="txtSwiftNumber" Width="230px" ToolTip="Swift Code" runat="server" /></td>
	</tr>
	<tr>
		<td>IBAN</td>
		<td><asp:TextBox ID="txtIBAN" Width="230px" ToolTip="IBAN" runat="server" /></td>
	</tr>
	<tr>
		<td>ABA (Routing) Code</td>
		<td><asp:TextBox ID="txtABA" Width="230px" ToolTip="ABA (Routing) Code" runat="server" /></td>
	</tr>
	<tr>
		<td>Sort Code</td>
		<td><asp:TextBox ID="txtSortCode" Width="230px" ToolTip="Sort Code" runat="server" /></td>
	</tr>
</table>