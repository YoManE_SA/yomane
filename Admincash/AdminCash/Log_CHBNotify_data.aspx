<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script runat="server">
		Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
			Security.CheckPermission(lblPermissions)
			If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") Else PGData.PageSize = 25
			
			Dim sWhere As String = ""
			If Request("DebitReferenceCode") <> "" Then
				sWhere &= " And CHBNT_TransID='" & Replace(Request("DebitReferenceCode"), "''", "''") & "'"
			ElseIf Request("transID") <> "" Then
				sWhere &= " And tblCompanyTransPass.ID=" & dbPages.TestVar(Request("transID"), 0, -1, 0)
			Else
				If Request("fromDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And CHBNT_InsDate >= '" & Request("fromDate") & " " & Request("fromTime") & "'"
				If Request("toDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And CHBNT_InsDate <= '" & Request("toDate") & " " & Request("toTime") & "'"
				If Request("MerchantID") <> "" Then sWhere &= " And tblCompanyTransPass.CompanyID=" & dbPages.TestVar(Request("MerchantID"), 0, -1, 0)
				If Request("CcLast4Num") <> "" Then sWhere &= " And tblCreditCard.CCard_Last4=" & dbPages.TestVar(Request("CcLast4Num"), 0, -1, 0)
				If Request("Terminal") <> "" Then sWhere &= " And CHBNT_TerminalNumber='" & Request("Terminal") & "'"
			End If
			If Security.IsLimitedMerchant Then sWhere &= " AND tblCompany.ID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
			If Security.IsLimitedDebitCompany Then sWhere &= " AND tblDebitCompany.DebitCompany_id IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"

			If Request("Export") <> "" Then PGData.PageSize = 10000
	        PGData.OpenDataset("Select " & IIf(sWhere <> "", "", "Top 20") & " tblLog_CHBNotification.*, tblCompanyTransPass.ID TID, CreditType, IsTestOnly, dc_Name," & _
	        " tblCompanyTransPass.InsertDate TInsertDate, deniedStatus, PaymentMethodDisplay, PaymentMethod_id, tblCompanyTransPass.PaymentMethod, CompanyName " & _
	        " FROM tblLog_CHBNotification INNER JOIN tblDebitCompany ON tblLog_CHBNotification.CHBNT_DebitCompanyID=tblDebitCompany.debitCompany_id" & _
	        " INNER JOIN tblCompanyTransPass ON tblCompanyTransPass.DebitReferenceCode = tblLog_CHBNotification.CHBNT_TransID And tblCompanyTransPass.CreditType=1" & _
	        " INNER JOIN tblCreditCard ON tblCreditCard.ID = tblCompanyTransPass.CreditCardID" & _
	        " INNER JOIN tblCompany ON tblCompany.ID = tblCompanyTransPass.CompanyID" & _
	        " WHERE tblCompanyTransPass.DebitCompanyID=21 AND CreditType<>0 " & sWhere & " Order By CHBNT_ID Desc")
			If Request("Export") <> "" Then
				Response.ContentType = "application/vnd.ms-excel"
				Response.AddHeader("Content-Disposition", "attachment;filename=report.xls")
				Response.Write("<table><tr><th>Row ID</th><th>Trans ID</th><th>Import Date</th><th>Reference Code</th><th>Merchant</th><th>Terminal</th><th>Debit</th><th>Trans Date</th><th>CHB Date</th><th>Payment Method</th><th>Amount</th><th>Status</th></tr>")
				While PGData.Read()
					Response.Write("<tr>")
					Response.Write("<td>" & PGData("CHBNT_ID") & "</td>")
					Response.Write("<td>" & PGData("TID") & "</td>")
					Response.Write("<td>" & PGData("CHBNT_InsDate") & "</td>")
					Response.Write("<td>" & PGData("CHBNT_TransID") & "</td>")
					Response.Write("<td>" & PGData("CompanyName") & "</td>")
					Response.Write("<td>" & PGData("CHBNT_TerminalNumber") & "</td>")
					Response.Write("<td>" & PGData("dc_Name") & "</td>")
					Response.Write("<td>" & PGData("TInsertDate") & "</td>")
					Response.Write("<td>" & PGData("CHBNT_DateRec") & "</td>")
					Response.Write("<td>" & PGData("PaymentMethodDisplay") & "</td>")
					Response.Write("<td style=""text-align:right;"">" & CType(PGData("CHBNT_Amount"), Decimal).ToString("0.00") & "</td>")
					Response.Write("<td style=""text-align:center;"">" & PGData("CHBNT_Status") & "</td>")
					Response.Write("</tr>")
				End While
				Response.Write("</table>")
				PGData.CloseDataset()
				Response.End()
			End If
		End Sub
		
		Protected Sub GetTransColor(iReader As Object, ByRef sTrBkgColor As String, ByRef sTdBkgColorFirst As String, ByRef sTdFontColor As String)
			sTrBkgColor = "#F6FEF6" 'LightGreen
			sTdBkgColorFirst = "#66cc66" 'Green
			sTdFontColor = "#000000"
			Dim nPaymentMethod As Integer = IIF(IsDbNull(iReader("PaymentMethod")), 0, iReader("PaymentMethod")) 
			If nPaymentMethod = ePaymentMethod.PMD_SystemFees Then
				sTrBkgColor="f5f5f5" 'LightGray
				sTdBkgColorFirst="#484848" 'DarkGray
			Elseif nPaymentMethod = ePaymentMethod.PMD_Admin Then
				sTrBkgColor="#eeeffd" 'LightBlue
				sTdBkgColorFirst="#003399" 'Blue
			Elseif nPaymentMethod = ePaymentMethod.PMD_RolRes Then
				sTrBkgColor="#f5f5f5" 'LightGray
				sTdBkgColorFirst="#399496" 'DarkGreen
			End if
			If IIF(IsDbNull(iReader("CreditType")), 0, iReader("CreditType")) = 0 then sTdFontColor="#cc0000" 'Red
			If IIF(IsDbNull(iReader("deniedstatus")), 0, iReader("deniedstatus")) > 0 Then sTrBkgColor="#FFEECD" 'LightOrange
			If IIF(IsDbNull(iReader("isTestOnly")), 0, iReader("isTestOnly")) Then
				sTrBkgColor = "#ffffff"
				sTdFontColor = "#6b6b6b"
			End if
		End Sub
		
	</script>
	<script language="jscript">
		function toggleInfo(TransId,PaymentMethod,bkgColor)
		{
			objRefB = document.getElementById('Row' + TransId + 'B')
			if (objRefB.innerHTML == '') {
				if (PaymentMethod == '1'){ UrlAddress = 'trans_detail_cc.asp'; } else { UrlAddress = 'trans_detail_eCheck.asp'; }
				objRefB.innerHTML = '<iframe framespacing="0" scrolling="auto" align="right" marginwidth="0" frameborder="0" width="100%" height="0px" src="'+UrlAddress+'?transID='+TransId+'&PaymentMethod='+PaymentMethod+'&bkgColor='+bkgColor+'&TransTableType=pass" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>';
			} else {
				objRefB.innerHTML = '';
			}
		}
	</script>
</head>
<body>
	<form id="Form2" runat="server">

		<table align="center" style="width:92%">
		<tr>
			<td colspan="2" id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> <asp:label ID="Label1" runat="server" /> LOG CHB NOTIFICATIONS <br />
			</td>
		</tr>
		</table>
		<br />
		<table class="formNormal" align="center" style="width:92%" cellpadding="1" cellspacing="0">
		<tr>
			<th>Row ID</th>
			<th>Trans ID</th>
			<th>Import Date</th>
			<th>Reference Code</th>
			<th>Merchant</th>
			<th>Terminal</th>
			<th>Debit</th>
			<th>Trans Date</th>
			<th>CHB Date</th>
			<th>Payment Method</th>
			<th style="text-align:right;">Amount</th>
			<th style="text-align:center;">Status</th>
			<th>Details</th>
		</tr>
		<%
		Dim nTotal as Decimal = 0
		While PGData.Read()
			Dim sTrBkgColor As String = "", sTdBkgColorFirst As String = "", sTdFontColor As String = ""
	   		nTotal = nTotal + CType(PGData("CHBNT_Amount"), Decimal)
	   		GetTransColor(PGData, sTrBkgColor, sTdBkgColorFirst, sTdFontColor)
		%>
		<tr style="background-color:<%=sTrBkgColor%>">
			<td><%=PGData("CHBNT_ID")%></td>
			<td><%=PGData("TID")%></td>
			<td><%=PGData("CHBNT_InsDate")%></td>
			<td><%=PGData("CHBNT_TransID")%></td>
			<td><%=PGData("CompanyName")%></td>
			<td><%=PGData("CHBNT_TerminalNumber")%></td>
			<td><img src="/NPCommon/ImgDebitCompanys/23X12/<%=PGData("CHBNT_DebitCompanyID")%>.gif" align="middle" alt="<%=PGData("dc_Name")%>" /></td>
			<td><%=PGData("TInsertDate")%></td>
			<td><%=PGData("CHBNT_DateRec")%></td>
			<td><%=PGData("PaymentMethodDisplay")%></td>
			<td style="text-align:right;"><%=CType(PGData("CHBNT_Amount"), Decimal).ToString("0.00")%></td>
			<td style="text-align:center;"><%=PGData("CHBNT_Status")%></td>
			<td><a onclick="toggleInfo('<%= PGData("TID") %>','<%= PGData("PaymentMethod_id") %>','FFFFFF')" name="<%= PGData("TID") %>" class="go" style="cursor:pointer; font-size:11px;"><img src="../images/b_showWhiteEng.gif" align="middle" /></a><br /></td>
		</tr><tr>
		  <td colspan="13" id="Row<%=PGData("TID")%>B"></td>
		</tr>
		<tr><td height="1" colspan="13"  bgcolor="#ffffff"></td></tr>
		<tr><td height="1" colspan="13"  bgcolor="silver"></td></tr>
		<tr><td height="1" colspan="13"  bgcolor="#ffffff"></td></tr>
		<%
		End While
		PGData.CloseDataset()%>
		<tr>
		   <td colspan="10"></td>
		   <td style="text-align:right;"><%=nTotal.ToString("0.00")%></td>
		</tr>
		</table>
	</form>
	<br />
	<table align="center" style="width:92%">
	 <tr>
	  <td><UC:Paging runat="Server" id="PGData" PageID="PageID"  /></td>
	 </tr>
	</table>
</body>
</html>