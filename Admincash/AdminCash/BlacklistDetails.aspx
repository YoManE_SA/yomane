<%@ Page Language="VB" %>
<script runat="server">
	Sub Page_Load()
		If Not Page.IsPostBack Then
			Dim nID As Integer = dbPages.TestVar(Request("ID"), 1, 0, 0)
			If nID > 0 Then
				Dim sSQL As String = "SELECT fcbl_comment, dbo.GetDecrypted256(fcbl_ccNumber256) CardNumber FROM tblFraudCCBlackList WHERE fraudCcBlackList_id=" & nID
				Dim drData As System.Data.SqlClient.SqlDataReader = dbPages.ExecReader(sSQL)
				If drData.Read Then
					litCardNumber.Text = drData("CardNumber")
					litComment.Text = drData("fcbl_comment")
				End If
				drData.Close()
			End If
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>CC Blacklist Entry Details</title>
</head>
<body style="margin:0;padding:10px 0;font:normal 11px Arial;background-color:#<%=IIF(Trim(Request("color"))<>"",Trim(Request("color")),"ffffff")%>;">
	<b>Full Credit Card Number</b>: <asp:Literal ID="litCardNumber" Text="Unavailable" runat="server" /><br />
	<b>Comment</b>: <asp:Literal ID="litComment" Text="Unavailable" runat="server" />
</body>
</html>
