<%@ Page Language="VB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    Dim pageMenuWidth As String
    Dim pageBodyWidth As String
    Dim pageMenuUrl As String
    Dim pageBodyUrl As String
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        pageMenuWidth = dbPages.TestVar(Request.QueryString("pageMenuWidth"), -1, "*")
        pageBodyWidth = dbPages.TestVar(Request.Querystring("pageBodyWidth"), -1, "*")
        pageMenuUrl = dbPages.TestVar(Request.Querystring("pageMenuUrl"), -1, "common_blank.htm")
        pageBodyUrl = dbPages.TestVar(Request.Querystring("pageBodyUrl"), -1, "common_blank.htm")
    End Sub
    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title><%= dbPages.getConfig("COMPANY_NAME_1") %> - Control Panel</title>
    <link  href="../StyleSheet/StyleAdminNet.css" rel="stylesheet" type="text/css" />
</head>
<frameset cols="<%=pageMenuWidth%>,3,<%=pageBodyWidth%>" framespacing="0" frameborder="0">
	<frame src="<%=pageMenuUrl%>" name="fraMenu" id="fraMenu" frameborder="0" scrolling="auto" noresize marginwidth="0" marginheight="0">
	<frame src="Common_blank.aspx?color=484848"  scrolling="no" noresize marginwidth="0" marginheight="0">
	<frame src="<%=pageBodyUrl%>" name="frmBody" id="frmBody" frameborder="0" scrolling="Auto">
</frameset>

</html>