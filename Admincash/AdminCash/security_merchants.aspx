<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Netpay" %>
<script runat="server">
	'
	' 20090923 Tamir
	'
	' Security management
	'
	Dim nUser As Integer = 0
	Dim ClrConv As New System.Drawing.ColorConverter
	
	Sub Page_PreInit()
		If dbPages.TestVar(Request.QueryString("User"), 1, 0, 0) = 0 Then Response.Redirect("common_blank.htm")
		nUser = Request.QueryString("User")
		If dbPages.ExecScalar("IF EXISTS (SELECT ID FROM tblSecurityUser WHERE ID=" & nUser & " AND su_IsActive=1) SELECT 1 ELSE SELECT 0") = 0 Then
			Response.Redirect("common_blank.htm")
		End If
	End Sub
	
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsData.ConnectionString = dbPages.DSN
		If Not Page.IsPostBack Then
			litUser.Text = dbPages.ExecScalar("SELECT su_Name FROM tblSecurityUser WHERE ID=" & nUser)
			hlID.NavigateUrl = "security_merchants.aspx?OrderBy=ID&User=" & nUser
			hlName.NavigateUrl = "security_merchants.aspx?OrderBy=Name&User=" & nUser
			hlMID.NavigateUrl = "security_merchants.aspx?OrderBy=MID&User=" & nUser
		End If
		Select Case Request("OrderBy")
			Case "ID" : dsData.SelectCommand &= " ORDER BY tblCompany.ID"
			Case "MID" : dsData.SelectCommand &= " ORDER BY CustomerNumber"
			Case Else : dsData.SelectCommand &= " ORDER BY CompanyName"
		End Select
	End Sub

	Sub AddRemoveMerchant(ByVal o As Object, ByVal e As CommandEventArgs)
		Dim nMerchant As Integer = dbPages.TestVar(IIf(e.CommandName.ToLower = "add", hidNew.Value, e.CommandArgument), 1, 0, 0)
		If e.CommandName.ToLower = "account" Then
			dbPages.ExecSql("DELETE FROM tblSecurityUserMerchant WHERE sum_User=" & nUser)
			dbPages.ExecSql("INSERT INTO tblSecurityUserMerchant(sum_User, sum_Merchant) SELECT " & nUser & ", ID FROM tblCompany WHERE careOfAdminUser='" & dbPages.DBText(dbPages.ExecScalar("SELECT su_Username FROM tblSecurityUser WHERE ID=" & nUser)) & "'")
		ElseIf nMerchant > 0 Then
			dbPages.ExecSql("DELETE FROM tblSecurityUserMerchant WHERE sum_User=" & nUser & " AND sum_Merchant=" & nMerchant)
			If e.CommandName.ToLower = "add" Then dbPages.ExecSql("INSERT INTO tblSecurityUserMerchant(sum_User, sum_Merchant) VALUES (" & nUser & ", " & nMerchant & ")")
		End If
		hidNew.Value = String.Empty
		repData.DataBind()
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<form id="frmServer" runat="server">
	<table width="95%" align="center">
	<tr>
		<td>
			<table>
			<tr>
				<td id="pageMainHeading">
					<asp:label ID="lblPermissions" runat="server" /> Security<span id="pageMainSubHeading"> - Merchants</span>
				</td>
			</tr>
			</table>
			<div class="bordered normal">
				<table class="formNormal" style="float:right;">
					<tr>
						<td>
							<asp:Button Text="Limit to managed merchants" OnCommand="AddRemoveMerchant" CommandName="Account" UseSubmitBehavior="false" runat="server" />
						</td>
					</tr>
				</table>
				You can limit
				<b><asp:Literal ID="litUser" runat="server" /></b> to specific merchants only.
				<br />
				When none selected all are allowed
			</div>
			<div class="bordered normal">
				<table class="formNormal">
					<tr>
						<td>Add new merchant to the permitted merchant list:</td>
						<td><Netpay:FilterMerchants ID="fmNew" ClientField="hidNew" IsSingleLine="true" ShowTitle="false" runat="server" /></td>
						<td style="text-align:right;">
							<asp:HiddenField ID="hidNew" runat="server" />
							<asp:Button Text=" Add " OnCommand="AddRemoveMerchant" CommandName="Add" UseSubmitBehavior="false" runat="server" />
						</td>
					</tr>
				</table>
			</div>
			<asp:SqlDataSource ID="dsData" runat="server" SelectCommand="SELECT tblCompany.ID, CustomerNumber, CompanyName, GD_Color, GD_Text
			  FROM tblCompany INNER JOIN tblGlobalData ON ActiveStatus=GD_ID AND GD_Lng=1 AND GD_Group=44
			   INNER JOIN tblSecurityUserMerchant ON tblCompany.ID=sum_Merchant AND sum_User=@nUser">
				<SelectParameters>
					<asp:QueryStringParameter Name="nUser" QueryStringField="User" Type="Int32" />
				</SelectParameters>
			</asp:SqlDataSource>
			<table class="formNormal">
			<tr>
				<th colspan="2" style="text-align:right;">
					ID
					<asp:HyperLink ID="hlID" runat="server"><img src="../images/arrowTreeD.gif" title="Order By This Field" border="0" /></asp:HyperLink>
				</th>
				<th>
					Company Name
					<asp:HyperLink ID="hlName" runat="server"><img src="../images/arrowTreeD.gif" title="Order By This Field" border="0" /></asp:HyperLink>
				</th>
				<th style="text-align:right;">
					MID
					<asp:HyperLink ID="hlMID" runat="server"><img src="../images/arrowTreeD.gif" title="Order By This Field" border="0" /></asp:HyperLink>
				</th>
				<th style="text-align:center;">Remove</th>
			</tr>
			<asp:Repeater ID="repData" DataSourceID="dsData" runat="server">
				<ItemTemplate>
					<tr>
						<td class="underlined"><asp:Label Text="&nbsp;&nbsp;" BackColor='<%#ClrConv.ConvertFromString(Eval("GD_Color"))%>' runat="server" /></td>
						<td class="underlined" style="text-align:right;"><asp:Literal ID="litMerchant" Text='<%#Eval("ID")%>' runat="server" /></td>
						<td class="underlined"><asp:Label Text='<%#Eval("CompanyName")%>' runat="server" /></td>
						<td class="underlined" style="text-align:right;"><asp:Literal Text='<%#Eval("CustomerNumber")%>' runat="server" /></td>
						<td class="underlined" style="text-align:center;">
							<asp:Button OnCommand="AddRemoveMerchant" CommandArgument='<%#Eval("ID")%>' UseSubmitBehavior="false" CssClass="buttonWhite buttonDelete"
							 OnClientClick="if (!confirm('Do you really want to remove this permission ?!')) return false;" Text="Remove" runat="server" />
						</td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
			</table>
		</td>
	</tr>
	</table>
	</form>
</body>
</html>