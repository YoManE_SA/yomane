<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script runat="server">
	Sub Page_Load()
		Dim sSQL As String, drData As SqlDataReader
		lblCurrency.Text &= "<select name=""Currency"" style=""width:80px;""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT CUR_ID, CUR_ISOName FROM tblSystemCurrencies ORDER BY CUR_ID"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblCurrency.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		lblCurrency.Text &= "</select>"
		drData.Close()
		fdtrControl.FromDateTime = Date.Now.AddMonths(-1).AddDays(1).ToShortDateString
		fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <form runat="server" action="Log_MasavFile_data.asp" method="get" target="frmBody" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" name="from" value="menu" />

	<h1><asp:label ID="lblPermissions" runat="server" /> Wire Control<span> - Filter</span></h1>

	<table class="filterNormal">
		<tr><th colspan="2">Transfer Details</th></tr>
		<tr>
			<td colspan="2">
				<Uc1:FilterDateTimeRange Title="" ID="fdtrControl" runat="server" />
			</td>
		</tr>
		<tr>
			<td>Currency</td>
			<td><asp:Label ID="lblCurrency" runat="server" /></td>
		</tr>
		<tr>
			<td>Beneficiary</td>
			<td><input name="SearchText" class="GrayBG" /></td>
		</tr>
		<tr>
			<td>Merchant(s)</td>
			<td><input name="MerchantIDs" class="GrayBG" /></td>
		</tr>
		<tr>
			<td>Wire(s)</td>
			<td><input name="WireIDs" class="GrayBG" /></td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
			<td colspan="2" style="text-align:left;">
				<input type="submit" value="Search" onclick="this.form.action='Log_MasavFile_data.asp';" />
				<input type="reset" value="Reset" /><br />
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:left;">
				<br /><input type="submit" value="Export to XLS" onclick="this.form.action='Log_MasavFile_Excel.aspx';" />
			</td>
		</tr>
	</table>
	</form>	
</asp:Content>