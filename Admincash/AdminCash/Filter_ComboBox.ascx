﻿<%@ Control Language="VB" ClassName="Filter_ComboBox" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
	'Usage of ComboBox Filter control:
	'
	'Common fields:
	'		ClientField: name of the field in HTML Form
	'		Width: control (both textbox and dropdown list with) in CSS format
	'		RecordsVisible: number of records to show on screen (actually limits the dropdown list height)
	'		ListBGColor: CSS-supported color - background color for the dropdown list
	'		ActiveItemBGColor: CSS-supported color - background color for both selected item and for hovered item in the dropdown list
	'		ListBorderColor: CSS-supported color - color for dropdown list border and for separator between the items
	'
	'1. Data - List from database:
	'		SqlQuery: query to load values from the database
	'		ValueColumn: name of the column containing values (shown in the dropdown list and taken to the textbox on click)
	'		TextColumn: name of the column containing texts (shown in the dropdown list, not taken to the textbox on click)
	'		ColorColumn: name of the column containing CSS-supported colors (optional, color bar can be shown before the item text in the dropdown list)
	'
	'2. Data - Static List (if SqlQuery is empty or not set):
	'		ListSeparator: string used to separate items in lists
	'		ValueList: list of values (shown in the dropdown list and taken to the textbox on click) separated by ListSeparator
	'		TextList: list of texts (shown in the dropdown list, not taken to the textbox on click) separated by ListSeparator
	'		ColorList: list of CSS-supported colors (optional, color bar can be shown before the item text in the dropdown list) separated by ListSeparator

	Private sbJS As New StringBuilder(String.Empty)
	Dim i As Integer = 0

	Private sSeparator As String = ";"
	Public Property ListSeparator() As String
		Get
			Return sSeparator
		End Get
		Set(ByVal value As String)
			sSeparator = value
		End Set
	End Property

	Private sValues As String
	Public Property ValueList() As String
		Get
			Return sValues
		End Get
		Set(ByVal value As String)
			sValues = value
		End Set
	End Property
	
	Private sTexts As String
	Public Property TextList() As String
		Get
			Return sTexts
		End Get
		Set(ByVal value As String)
			sTexts = value
		End Set
	End Property
	
	Private sColors As String
	Public Property ColorList() As String
		Get
			Return sColors
		End Get
		Set(ByVal value As String)
			sColors = value
		End Set
	End Property
	
	Private sSQL As String
	Public Property SqlQuery() As String
		Get
			Return sSQL
		End Get
		Set(ByVal value As String)
			sSQL = value
		End Set
	End Property

	Private sValueColumn As String
	Public Property ValueColumn() As String
		Get
			Return sValueColumn
		End Get
		Set(ByVal value As String)
			sValueColumn = value
		End Set
	End Property

	Private sTextColumn As String
	Public Property TextColumn() As String
		Get
			Return sTextColumn
		End Get
		Set(ByVal value As String)
			sTextColumn = value
		End Set
	End Property

	Private sColorColumn As String
	Public Property ColorColumn() As String
		Get
			Return sColorColumn
		End Get
		Set(ByVal value As String)
			sColorColumn = value
		End Set
	End Property

	Private sClientField As String
	Public Property ClientField() As String
		Get
			Return sClientField
		End Get
		Set(ByVal value As String)
			sClientField = value
		End Set
	End Property

	Private sWidth As String = "180px"
	Public Property Width() As String
		Get
			Return sWidth
		End Get
		Set(ByVal value As String)
			sWidth = value
		End Set
	End Property

	Private sActiveItemBGColor As String = "#E0EFFF"
	Public Property ActiveItemBGColor() As String
		Get
			Return sActiveItemBGColor
		End Get
		Set(ByVal value As String)
			sActiveItemBGColor = value
		End Set
	End Property

	Private sListBGColor As String = "White"
	Public Property ListBGColor() As String
		Get
			Return sListBGColor
		End Get
		Set(ByVal value As String)
			sListBGColor = value
		End Set
	End Property

	Private sListBorderColor As String = "Silver"
	Public Property ListBorderColor() As String
		Get
			Return sListBorderColor
		End Get
		Set(ByVal value As String)
			sListBorderColor = value
		End Set
	End Property

	Private nRecordsVisible As Integer = 10
	Public Property RecordsVisible() As Integer
		Get
			Return nRecordsVisible
		End Get
		Set(ByVal value As Integer)
			nRecordsVisible = value
		End Set
	End Property

	Public ReadOnly Property SelectedValue As String
		Get
			Return Request(sClientField)
		End Get
	End Property
	
	Dim sSelectedText As String 
	Public ReadOnly Property SelectedText As String
		Get
			Return sSelectedText
		End Get
	End Property
	
	Dim sbComboList As New StringBuilder(String.Empty)

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim bColor As Boolean = False, sColorText As String
		If String.IsNullOrEmpty(SqlQuery) Then
			Dim saValues() As String = ValueList.Split(ListSeparator)
			Dim saTexts() As String = TextList.Split(ListSeparator)
			Dim saColors() As String = ColorList.Split(ListSeparator)
			bColor = Not String.IsNullOrEmpty(ColorList)
			For i As Integer = 0 To saValues.Length - 1
				sColorText = String.Empty
				If bColor Then sColorText = "<span style=""background-color:" & saColors(i) & ";"">&nbsp;</span> "
				sbComboList.AppendFormat("<a href=""#"" " & IIf(i = 0, " class=""first""", String.Empty) & " onclick=""SelectValue" & ClientField & "('{1}');return false;"">{0}{1} {2}</a>", sColorText, saValues(i), saTexts(i))
			Next
			If RecordsVisible > saValues.Length Then RecordsVisible = saValues.Length
		Else
			Dim drData As SqlDataReader = dbPages.ExecReader(SqlQuery)
			Try
				If drData.GetOrdinal(ColorColumn) >= 0 Then bColor = True
			Catch ex As Exception
				'nothing to do
			End Try
			Dim i As Integer = 0
			Do While drData.Read
				sColorText = String.Empty
				If bColor Then sColorText = "<span style=""background-color:" & drData(ColorColumn) & ";"">&nbsp;</span> "
				sbComboList.AppendFormat("<a href=""#"" " & IIf(i = 0, " class=""first""", String.Empty) & " onclick=""SelectValue" & ClientField & "('{1}');return false;"">{0}{1} {2}</a>", sColorText, drData(ValueColumn), drData(TextColumn))
				i += 1
			Loop
			drData.Close()
			If RecordsVisible > i Then RecordsVisible = i
		End If
	End Sub
</script>
<style type="text/css">
	#divComboList<%= ClientField %>
	{
		width:<%= sWidth %>;
		overflow-y:auto;
		position:absolute;
		background-color:<%= ListBGColor %>;
		border:1px solid <%= ListBorderColor %>;
		margin-top:-3px;
		padding:0 2px;
		height:<%= 21*nRecordsVisible %>;
		overflow-y:auto;
	}
	#divComboList<%= ClientField %> a
	{
		display:block;
		text-decoration:none;
		padding:2px;
		height:20px;
		border-top:1px solid <%= ListBorderColor %>;
	}
	#divComboList<%= ClientField %> a:hover
	{
		background-color:<%= ActiveItemBGColor %>;
	}
	#divComboList<%= ClientField %> a.first
	{
		border-top-width:0;
	}
</style>
<script language="javascript" type="text/javascript">
	function ShowComboList<%= ClientField %>()
	{
		document.getElementById("divComboList<%= ClientField %>").style.display="block";
		FilterAddEvent<%= ClientField %>(document, 'click', HideComboList<%= ClientField %>);
	}

	function HideComboList<%= ClientField %>()
	{
		document.getElementById("divComboList<%= ClientField %>").style.display="none";
	}

	function SelectValue<%= ClientField %>(sValue)
	{
		event.cancelBubble=true;
		for(var i=0;i<event.srcElement.parentNode.childNodes.length;i++) event.srcElement.parentNode.childNodes[i].style.backgroundColor="";
		event.srcElement.style.backgroundColor="<%= ActiveItemBGColor %>";
		document.getElementById("<%= ClientField %>").value=sValue;
		HideComboList<%= ClientField %>();
	}

	function FilterAddEvent<%= ClientField %>(obj, strEvent, objFunction)
	{ 
		if (obj.addEventListener)
		{ 
			obj.addEventListener(strEvent, objFunction, true); 
			return true; 
		}
		else if (obj.attachEvent)
		{ 
			var returnValue = obj.attachEvent('on'+strEvent, objFunction); 
			return returnValue; 
		} 
		else return false; 
	}
</script>
<input id="<%= ClientField %>" name="<%= ClientField %>" value="<%=sSelectedText%>" class="input1" style="width:<%= sWidth %>;"
	onfocus="event.cancelBubble=true;ShowComboList<%= ClientField %>();" onclick="event.cancelBubble=true;"
	 ondblclick="event.cancelBubble=true;ShowComboList<%= ClientField %>();" onkeyup="if (event.keyCode==27) HideComboList<%= ClientField %>();" /><br />
<div id="divComboList<%= ClientField %>" style="display:none;"><%=sbComboList.ToString%></div>