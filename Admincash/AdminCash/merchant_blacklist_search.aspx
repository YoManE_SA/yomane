<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20080629 Tamir
	'
	' Merchant blacklist management
	'
	Sub AddConditionToSQL(ByRef sSQL As String, ByVal sCondition As String)
		sSQL &= IIf(sSQL.IndexOf("WHERE") > 0, " OR ", " WHERE ") & sCondition
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsMerchants.ConnectionString = dbPages.DSN()
		Dim sSQL As String
		If Page.IsPostBack Then
			sSQL = Session("RecurringAdminCashSQL").ToString
		Else
			sSQL = "SELECT * FROM [Risk].[BlacklistMerchant] WHERE BlacklistMerchant_id=" & Request("BlacklistMerchant_id").ToString()
			Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
			sSQL = "SELECT TOP 15 ID, CustomerNumber, '<a href=""merchant_data.asp?companyID='+LTrim(RTrim(Str(ID)))+'"">'+CompanyName+'</a>' CompanyName, FirstName, LastName, Mail FROM tblCompany"
			If drData.Read() Then
				If drData("CompanyID").ToString <> "" Then AddConditionToSQL(sSQL, "ID=" & drData("CompanyID").ToString)
				If drData("CompanyName").ToString <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(CompanyName) LIKE dbo.GetCleanText('%" & drData("CompanyName").ToString & "%') OR dbo.GetCleanText(CompanyLegalName) LIKE dbo.GetCleanText('%" & drData("CompanyName").ToString & "%'))")
				If drData("CompanyLegalNumber").ToString <> "" Then AddConditionToSQL(sSQL, "dbo.GetCleanText(CompanyLegalNumber) LIKE dbo.GetCleanText('%" & drData("CompanyLegalNumber").ToString & "%')")
				If drData("IDNumber").ToString <> "" Then AddConditionToSQL(sSQL, "dbo.GetCleanText(IDNumber) LIKE dbo.GetCleanText('%" & drData("IDNumber").ToString & "%')")
				If drData("FirstName").ToString <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(FirstName) LIKE dbo.GetCleanText('%" & drData("FirstName").ToString & "%') OR dbo.GetCleanText(PaymentPayeeName) LIKE dbo.GetCleanText('%" & drData("FirstName").ToString & "%'))")
				If drData("LastName").ToString <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(LastName) LIKE dbo.GetCleanText('%" & drData("LastName").ToString & "%') OR dbo.GetCleanText(PaymentPayeeName) LIKE dbo.GetCleanText('%" & drData("LastName").ToString & "%'))")
				If drData("Phone").ToString <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(Phone) LIKE dbo.GetCleanText('%" & drData("Phone").ToString & "%') OR dbo.GetCleanText(Cellular) LIKE dbo.GetCleanText('%" & drData("Phone").ToString & "%') OR dbo.GetCleanText(CompanyPhone) LIKE dbo.GetCleanText('%" & drData("Phone").ToString & "%') OR dbo.GetCleanText(CompanyFax) LIKE dbo.GetCleanText('%" & drData("Phone").ToString & "%') OR dbo.GetCleanText(MerchantSupportPhoneNum) LIKE dbo.GetCleanText('%" & drData("Phone").ToString & "%'))")
				If drData("CompanyFax").ToString <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(Phone) LIKE dbo.GetCleanText('%" & drData("CompanyFax").ToString & "%') OR dbo.GetCleanText(Cellular) LIKE dbo.GetCleanText('%" & drData("CompanyFax").ToString & "%') OR dbo.GetCleanText(CompanyPhone) LIKE dbo.GetCleanText('%" & drData("CompanyFax").ToString & "%') OR dbo.GetCleanText(CompanyFax) LIKE dbo.GetCleanText('%" & drData("CompanyFax").ToString & "%') OR dbo.GetCleanText(MerchantSupportPhoneNum) LIKE dbo.GetCleanText('%" & drData("CompanyFax").ToString & "%'))")
				If drData("Cellular").ToString <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(Phone) LIKE dbo.GetCleanText('%" & drData("Cellular").ToString & "%') OR dbo.GetCleanText(Cellular) LIKE dbo.GetCleanText('%" & drData("Cellular").ToString & "%') OR dbo.GetCleanText(CompanyPhone) LIKE dbo.GetCleanText('%" & drData("Cellular").ToString & "%') OR dbo.GetCleanText(CompanyFax) LIKE dbo.GetCleanText('%" & drData("Cellular").ToString & "%') OR dbo.GetCleanText(MerchantSupportPhoneNum) LIKE dbo.GetCleanText('%" & drData("Cellular").ToString & "%'))")
				If drData("Mail").ToString <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(Mail) LIKE dbo.GetCleanText('%" & drData("Mail").ToString & "%') OR dbo.GetCleanText(MerchantSupportEmail) LIKE dbo.GetCleanText('%" & drData("Mail").ToString & "%'))")
				If drData("URL").ToString <> "" Then AddConditionToSQL(sSQL, "dbo.GetCleanText('http://'+URL) LIKE dbo.GetCleanText('%" & drData("URL").ToString & "%')")
				sSQL &= " ORDER BY ID DESC"
			Else
				sSQL &= " WHERE 1=0"
			End If
			drData.Close()
			Session("RecurringAdminCashSQL") = sSQL
		End If
		lblSQL.Text = sSQL
		dsMerchants.SelectCommand = sSQL
		dsMerchants.DataBind()
		If gvMerchants.Rows.Count = 15 Then
			lblMsg.Text = "More than 15 records found. Only first 15 records are shown. Please refine your search."
		ElseIf gvMerchants.Rows.Count = 0 Then
			lblMsg.Text = "No records found. Try specifying less parameters for search."
		Else
			lblMsg.Text = gvMerchants.Rows.Count.ToString & " records found."
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Merchant Blacklist - Search</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<script language="javascript" type="text/javascript">
		function OpenMerchantSearch(nID)
		{
			var sOpenURL = "merchant_blacklist_search.aspx?BlacklistMerchant_id = " + nID;
			var winBlacklistSearch=window.open(sOpenURL, "fraBlacklistSearch", "top=120,left=250,width="+(screen.width-500)+",height="+(screen.height-240)+",scrollbars=auto,resizable");
		}
	</script>
	<form runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Merchant Blacklist <span> - Search</span></h1>
		<div class="bordered">
			<asp:label ID="lblMsg" runat="server" />
		</div>
		<asp:label ID="lblSQL" runat="server" Visible="false" />
		<asp:GridView ID="gvMerchants" runat="server" AutoGenerateColumns="False" DataKeyNames="BlacklistMerchant_id" DataSourceID="dsMerchants" CssClass="grid" Width="100%">
			<Columns>
				<asp:BoundField DataField="BlacklistMerchant_id" HeaderText="ID" InsertVisible="False" ReadOnly="True" />
				<asp:BoundField DataField="CustomerNumber" HeaderText="Merchant Number"/>
				<asp:BoundField DataField="FirstName" HeaderText="First Name"/>
				<asp:BoundField DataField="LastName" HeaderText="Last Name"/>
				<asp:BoundField DataField="CompanyName" HeaderText="Name" HtmlEncode="false"/>
				<asp:BoundField DataField="Mail" HeaderText="Mail"/>
			</Columns>
			<SelectedRowStyle CssClass="rowSelected" />
		</asp:GridView>
		<asp:SqlDataSource ID="dsMerchants" runat="server" ProviderName="System.Data.SqlClient"
			SelectCommand="SELECT * FROM tblCompany" ConnectionString="">
		</asp:SqlDataSource>
	</form>
</body>
</html>