<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader("Content-Disposition", "attachment;filename=chb_clrfy_" & Today.ToString("yyyyMMdd") & ".xls")
	Response.Write("<table border=""1"" bordercolor=""black"">")
	Response.Write("<tr><th colspan=""13"" font=""""><h4>NETPAY TRANSACTIONS</h4></th><th colspan=""4""><h4>B+S REQUESTS</h4></th></tr>")
	Response.Write("<tr><th colspan=""2""><h5>MERCHANT</h5></th><th colspan=""4""><h5>TRANSACTION</h5></th><th colspan=""4""><h5>REFUND</h5></th><th colspan=""3""><h5>CHARGEBACK</h5></th><th colspan=""4""><h5>CHARGEBACK</h5></th></tr>")
	Response.Write("<tr><th>ID</th><th>Name</th><th>ID</th><th>DebitRefCode</th><th>Amount</th><th>Currency</th><th>ID</th><th>DebitRefCode</th><th>Date</th><th>Amount</th><th>Currency</th><th>Status</th><th>Date</th><th>Comment</th><th>MRF</th><th>Reason</th><th>File Date</th><th>Case ID</th></tr>")
	Dim drData As SqlDataReader = dbPages.ExecReader("SELECT * FROM viewImportChargebackExcel")
	While drData.Read()
		Response.Write("<tr>")
		For i As Integer = 0 To drData.FieldCount - 1
			Response.Write("<td " & IIf(drData.GetName(i).Contains("Comment"), "width=""370"" ", String.Empty) & " nowrap>")
			If Not Convert.IsDBNull(drData(i)) Then
				If drData.GetName(i).Contains("CaseID") Then
					Response.Write(drData(i) & "&nbsp;")
				ElseIf TypeOf (drData(i)) Is Date Then
					Response.Write(CType(drData(i), Date).ToString("dd.MM.yyyy"))
				Else
					Response.Write(drData(i))
				End If
			End If
			Response.Write("</td>")
		Next
		Response.Write("</tr>")
	End While
	drData.Close()
	Response.Write("</table>")
	Response.End()
%>
