<% 
sStartTime = Timer
Server.ScriptTimeout = 6000
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp" -->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/func_controls.asp"-->
<%
sLoginURL = session("TempOpenURL")
If trim(COMPANY_NAME_1) = "Netpay" Then
	sLoginURL = sLoginURL & "site_heb/"
Elseif trim(COMPANY_NAME_1) = "AsTech" Then
	sLoginURL = sLoginURL & "site_eng/"
End If

sAction = dbText(request("Action"))
sSortType = dbText(request("SortType"))
sNumShow = dbText(request("NumShow"))
sMerchantID = dbText(request("iMerchantID"))
sTextSearch = dbText(request("iTextSearch"))
If Trim(sMerchantID)<>"" AND NOT IsNumeric(sMerchantID) Then
	sTextSearch = sMerchantID
	sMerchantID = ""
End if
If sTextSearch <> "" And IsNumeric(sTextSearch) Then
	If Len(sTextSearch) < 7 Then 'Means wanted to search for id
		sMerchantID = sTextSearch
		sTextSearch = ""
	End if
End if
lActiveStatus = TestNumVar(request("ActiveStatus"), 0, -1, -1)
sDebitCompanyID = dbText(request("DebitCompanyID"))

If sNumShow<>"" Then
	sTopnum="top " & sNumShow
Else
	sTopnum="top 25"
End If

If Request("AddNew") = "1" Then 
	Response.Redirect("MiscActions.aspx?action=createMerchant&ref=" & Server.URLEncode("merchant_list.asp?Action=SearchFilter&iMerchantID="))
End if

sWhere = ""
Select Case sAction
	Case "LetterFilter"
		sEngShow = dbText(request("EngShow"))
		sHebShow = dbText(request("HebShow"))
		If sEngShow<>"" Then
			sWhere = " AND (ASCII(CompanyName) IN (" & sEngShow & ", " & sEngShow-32 & ") OR ASCII(CompanyLegalName) IN (" & sEngShow & ", " & sEngShow-32 & "))"
		ElseIf sHebShow<>"" Then
			sWhere = " AND (ASCII(CompanyName)=" & sHebShow & " OR ASCII(CompanyLegalName)=" & sHebShow & ")"
		End If
	Case "SearchFilter"
		If sMerchantID<>"" Then
			CharPosition = InStr(sMerchantID,"-")
			If CharPosition>0 Then
				sFromID = left(sMerchantID,CharPosition-1)
				sToID = right(sMerchantID,len(sMerchantID)-CharPosition)
				sWhere = sWhere & " AND c.ID>=" & sFromID
				sWhere = sWhere & " AND c.ID<=" & sToID
			Else
				sWhere = sWhere & " AND c.ID=" & TestNumVar(sMerchantID, 0, -1, 0)
			End if
		Elseif sTextSearch<>"" Then
			sWhere = sWhere & " AND (c.CompanyName LIKE '%" & sTextSearch & "%'"
			sWhere = sWhere & " OR c.companyLegalName LIKE '%" & sTextSearch & "%'"
			sWhere = sWhere & " OR c.CustomerNumber LIKE '%" & sTextSearch & "%'"
			sWhere = sWhere & " OR c.FirstName LIKE '%" & sTextSearch & "%'"
			sWhere = sWhere & " OR c.LastName LIKE '%" & sTextSearch & "%'"
			sWhere = sWhere & " OR c.url LIKE '%" & sTextSearch & "%'"
			sWhere = sWhere & " OR c.Mail LIKE '%" & sTextSearch & "%'"
			sWhere = sWhere & " OR c.MerchantSupportEMail LIKE '%" & sTextSearch & "%'"
			sWhere = sWhere & " OR c.GroupID IN (SELECT ID FROM tblMerchantGroup WHERE mg_Name LIKE '%" & sTextSearch & "%'))"
		End If
		If lActiveStatus <> - 1 Then sWhere = sWhere & " AND c.ActiveStatus=" & lActiveStatus
		If trim(sDebitCompanyID)<>"" Then _
			sWhere = sWhere & " AND ID IN (Select CCFT_CompanyID From tblDebitTerminals Left Join tblCompanyCreditFeesTerminals ON (tblDebitTerminals.terminalNumber = tblCompanyCreditFeesTerminals.CCFT_Terminal) Where DebitCompany=" & sDebitCompanyID & ")"
	Case Else
		sTopnum = "TOP 25"
		lActiveStatus = CMPS_PROCESSING
		sWhere = sWhere & " AND c.ActiveStatus=" & lActiveStatus
End Select
If PageSecurity.IsLimitedMerchant Then sWhere = sWhere & " AND c.ID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
If TestNumVar(request("GroupID"), 1, 0, 0) > 0 Then sWhere = sWhere & " AND c.GroupID=" & request("GroupID")
If sWhere<>"" Then sWhere = " WHERE " & mid(sWhere, 5)
Select Case sSortType
	Case "IDasc"				: sOrderBy="ORDER BY c.id asc"
	Case "IDdesc"				: sOrderBy="ORDER BY c.id desc"
	Case "compNameasc"			: sOrderBy="ORDER BY c.CompanyName asc"
	Case "compNamedesc"			: sOrderBy="ORDER BY c.CompanyName desc"
	Case "TransToPayasc"		: sOrderBy="ORDER BY ma.DateLastTransPass asc, c.CompanyName asc"
	Case "TransToPaydesc"		: sOrderBy="ORDER BY ma.DateLastTransPass desc, c.CompanyName asc"
	Case "TransPendingasc"		: sOrderBy="ORDER BY ma.DateLastTransPending asc, c.CompanyName asc"
	Case "TransPendingdesc"		: sOrderBy="ORDER BY ma.DateLastTransPending desc, c.CompanyName asc"
	Case "TimesPayasc"			: sOrderBy="ORDER BY numTimesPay asc, c.CompanyName asc"
	Case "TimesPaydesc"			: sOrderBy="ORDER BY numTimesPay desc, c.CompanyName asc"	
	Case "TransFailasc"			: sOrderBy="ORDER BY ma.DateLastTransFail asc, c.CompanyName asc"
	Case "TransFaildesc"		: sOrderBy="ORDER BY ma.DateLastTransFail desc, c.CompanyName asc"
	Case "TransApprovalasc"		: sOrderBy="ORDER BY ma.DateLastTransPreAuth asc, c.CompanyName asc"
	Case "TransApprovaldesc"	: sOrderBy="ORDER BY ma.DateLastTransPreAuth desc, c.CompanyName asc"
	Case Else					: sOrderBy="ORDER BY c.CountAdminView desc, c.CompanyName asc"
End Select

nQueryStr="NumShow=" & sNumShow & "&iTextSearch=" & sTextSearch & "&ActiveStatus=" & lActiveStatus & "&DebitCompanyID=" & sDebitCompanyID & "&iMerchantID=" & sMerchantID & "&Action=" & sAction & "&EngShow=" & trim(request("EngShow")) & "&HebShow=" & trim(request("HebShow"))

Dim MStatusText, MStatusColor
Set rsData = server.createobject("adodb.recordset")
GetConstArray rsData, GGROUP_COMPANYSTATUS, 1, "", MStatusText, MStatusColor

Function FormatLastDate(lsDate)
	If IsNull(lsDate) Then 
		FormatLastDate = "---"
		Exit Function
	End If
	If FormatDateTime(lsDate, 2) = FormatDateTime(Now(), 2) Then
		FormatLastDate = FormatDatesTimes(lsDate, 0, 1, 0) 'FormatDateTime(lsDate, 4)
	Else 
		FormatLastDate = FormatDatesTimes(lsDate, 1, 0, 0)
	End If
End Function

%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<script language="JavaScript" src="../js/func_common.js"></script>
	<script language="JavaScript">
		function showSection(SecNum) {
			for (i = 0; i < tblSec.length; i++) {
				
				if(i==SecNum) {
					tblSec[SecNum].style.display='';
					tdButton[SecNum].style.backgroundColor ='silver';
				}
				else {
					tblSec[i].style.display='none';
					tdButton[i].style.backgroundColor ='#eeeeee';
				}
			}
		}
		function toggleEditing(objId) {
			var imgPlus = document.getElementById("oListImg" + objId);
			imgPlus.src = (imgPlus.src.indexOf("tree_expand.gif") > 0 ? "../images/tree_collapse.gif" : "../images/tree_expand.gif");
			objRef = document.getElementById("Row" + objId )
			if (objRef.innerHTML == '') {
				document.body.style.cursor="wait";
				//document.getElementById("oListImg" + objId).style.cursor = "wait";
				objRef.innerHTML = "<iframe src=\"merchant_listExtra.asp?company_id=" + objId + "\" width=\"100%\" height=\"0\" framespacing=\"0\" marginwidth=\"0\" frameborder=\"0\" border=\"0\" onload=\"this.height=this.contentWindow.document.body.scrollHeight;document.body.style.cursor='default';document.getElementById('oListImg" + objId + "').style.cursor='pointer';document.getElementById('oListImg" + objId + "').style.display='none';document.getElementById('oListImg" + objId + "').style.display='';\"></iframe>";
				objRef.style.borderTop = '1px dashed silver';
			}
			else {
				objRef.innerHTML = '';
				objRef.style.borderTop = '';
			}
		}
	</script>
</head>
<body class="itextm3" bgcolor="#eeeeee">
<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
	<tr>
		<td id="pageMainHeadingTd"><span id="pageMainHeading">MERCHANTS</span></td>
		<td align="right"><a class="faq" href="?AddNew=1">Add a new merchant</a></td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td colspan="2" align="right">
			<%
			pageSecurityLevel = 0
			pageSecurityLang = "ENG"
			pageClosingTags = "</td></tr></table>"
			Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
			%>
		</td>
	</tr>
	<tr>
	    <td colspan="2" valign="middle">
	        <%
            For i = 0 to UBound(MStatusText)
                If MStatusColor(i)<>"" Then Response.Write("<span style=""vertical-align:middle; width:8px; background-color:" & MStatusColor(i) & ";""><img src=""../images/1_space.gif"" width=""2"" height=""14"" border=""0""></span> ")
                If MStatusText(i)<>"" Then Response.Write("<span style=""vertical-align:middle;"">" & MStatusText(i) & "</span> &nbsp;&nbsp;&nbsp;")
            Next
	        %>
	    </td>
	</tr>
</table>
<br />
<table style="border:1px solid gray;" align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
<tr>
	<td width="30" style="border-right:1px solid #d1d1d1;">
		<table width="100%" height="100%" align="center" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="50%" align="center" id="tdButton" onclick="showSection('0');" style="cursor:pointer; background-color:silver;">
				<img src="../images/iconAdmin_magnify.gif" alt="" width="16" height="13" border="0"><br />
			</td>
		</tr>
		<tr>
			<td align="center" id="tdButton" onclick="showSection('1');" style="cursor:pointer;">
				<img src="../images/iconAdmin_letter.gif" alt="" width="11" height="16" border="0"><br />
			</td>
		</tr>
		</table>
	</td>
	<td height="50">
		<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%" id="tblSec" style="display:block;">
		<tr>
			<td class="inputHeading">ID<br /></td>
			<td class="inputHeading">Free Text<br /></td>
			<td class="inputHeading">Group<br /></td>
			<td class="inputHeading">Debit Company<br /></td>
			<td class="inputHeading">Status<br /></td>
			<td class="inputHeading">Show<br /></td>
		</tr>
		<form action="merchant_list.asp" method="post">
		<input type="Hidden" name="Action" value="SearchFilter">
		<tr>
			<td><input type="Text" name="iMerchantID" size="3" class="input2" value="<%= sMerchantID %>"><br /></td>	
			<td><input type="Text" name="iTextSearch" size="15" value="<%= sTextSearch %>" class="input2"><br /></td>		
			<td>
				<%
				set rsData2=oledbData.Execute("SELECT * FROM tblMerchantGroup ORDER BY mg_Name")
				If not rsData2.EOF Then
					Call PutRecordsetCombodefaultNEW(rsData2, "GroupID"" style=""width:100px;"" class=""input2", "ID", "mg_Name", "", "", request("GroupID"))
				End If
				rsData2.close
				%><br />
			</td>
			<td>
				<%
				set rsData2=oledbData.Execute("SELECT debitCompany_id, dc_name FROM tblDebitCompany ORDER BY dc_name")
				If not rsData2.EOF Then
					Call PutRecordsetCombodefaultNEW(rsData2, "DebitCompanyID"" style=""width:120px;"" class=""input2", "debitCompany_id", "dc_name", "", "", sDebitCompanyID)
				End If
				rsData2.close
				%><br />
			</td>
			<td>
				<%=DrawComboConst(rsData2, GGROUP_COMPANYSTATUS, 1, False, "ActiveStatus", "class=""input2""", lActiveStatus, "", "")%>
			</td>
			<td>
				<Select name="numShow" class="input2">
					<option value="25" <%If sNumShow="25" Then%>selected<%End If%>>25</option>
					<option value="50" <%If sNumShow="50" Then%>selected<%End If%>>50</option>
					<option value="75" <%If sNumShow="75" Then%>selected<%End If%>>75</option>
					<option value="100" <%If sNumShow="100" Then%>selected<%End If%>>100</option>
				</Select><br />
			</td>	
			<td><input type="submit" value="SEARCH" class="button1"><br /></td>
		</tr>
		</form>		
		</table>
		<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%" id="tblSec" style="display:none;">
		<tr>
			<td colspan="3" class="txt11">
			<%
			for I=97 to 122
				%>
				<a class="faq" style="font-size:11px;" href="merchant_list.asp?Action=LetterFilter&NumShow=1000&sortType=compNameasc&EngShow=<%= I %>"><%= Chr(I) %></a>&nbsp; 
				<%
			next
			%>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="txt12">
			<%
			for T=224 to 250
				If T<>234 AND T<>237 AND T<>239 AND T<>243 AND T<>245 Then
				%>
				<a class="faq" style="font-size:12px;" href="merchant_list.asp?Action=LetterFilter&NumShow=1000&sortType=compNameasc&HebShow=<%= T %>"><%= Chr(T) %></a>&nbsp; 
				<%
				End If
			next
			%>
			</td>
		</tr>
		<tr><td height="6"></td></tr>
		</table>
	</td>
</tr>
</table>
<br />
<div align="center" id="waitMsg">
	<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0">
			<tr><td class="txt12" style="color: gray;">Loading merchants &nbsp;-&nbsp; Please wait<br /></td></tr>
			<tr><td height="4"></td></tr>
			<tr>
				<td><img src="../images/loading_animation_orange.gif" style="border:1px solid silver;" alt="" width="123" height="6" border="0"><br /></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
</div>
<% Response.Flush %>
<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
<%
'on error resume next
sSQL="SELECT " & sTopnum & " c.ID AS companyID, c.ActiveStatus, c.CountAdminView, c.CompanyName, c.Blocked, c.Closed, c.isNetpayTerminal, c.FirstName, c.LastName, " & _
		"CAST(ma.DateLastTransPass as datetime) as DateLastTransPass, CAST(ma.DateLastTransPending as datetime) as DateLastTransPending, CAST(ma.DateLastTransPreAuth as datetime) as DateLastTransPreAuth, CAST(ma.DateLastTransFail as datetime) as DateLastTransFail " &_
		"FROM tblCompany AS c LEFT JOIN [Track].[MerchantActivity] AS ma ON c.id = ma.Merchant_id" &_
		" " & sWhere & " " & sOrderBy
set rsData = oledbData.Execute(sSQL)
HandleErrorAfterFlush
If not rsData.EOF Then
	%>
	<script language="javascript" type="text/javascript">
		function ShowHideMenu(oDiv) {
			oDiv.style.display = (oDiv.style.display == "none" ? "" : "none");
		}
	</script>
	<style type="text/css">
		div.merchantMenu { width:100%;position:absolute;z-index:1;border:1px solid #999999;background-color:WhiteSmoke;padding:2px; }
		div.merchantMenu span { float:right;border:1px solid Black;background-color:Silver;padding:2px;line-height:1em;font-size:11px;font-weight:bold; }
		div.merchantMenu a { text-decoration:none;display:block;color:#505050; }
		div.merchantMenu a:hover { text-decoration:underline;display:block;color:Black; }
	</style>
	<form action="merchant_list.asp?<%= nQueryStr %>" method="post" name="frmCosComp" id="frmCosComp">
	<input type="Hidden" name="SortType" value="">
	<tr>
		<td class="txtHead" align="center" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;"><br /></td>
		<td colspan="2" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;"><br /></td>
		<td class="txtHead" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;">ID<br /></td>
		<td class="txtHead" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;">Merchant Name<br /></td>
		<td align="center" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;" valign="bottom">
			<img src="../images/icon_ccPayTrans.gif" title="Unsettled Transactions" width="15" height="15" border="0"><br />
		</td>
		<td align="center" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;" valign="bottom">
			<img src="../images/icon_ccPendingTrans.gif" title="Pending Transactions" width="15" height="15" border="0"><br />
		</td>
		<td align="center" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;" valign="bottom">
			<img src="../images/icon_ccOkTrans.gif" title="Pre-Authorized Transactions" width="15" height="15" border="0"><br />
		</td>
		<td align="center" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;" valign="bottom">
			<img src="../images/icon_ccFailedTrans.gif" title="Rejected Transactions" width="15" height="15" border="0"><br />
		</td>
		<td class="txtHead" align="center" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;">History<br /></td>
		<!--<td class="txtHead" align="center" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;">Info<br /></td>-->
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td>
			<a href="#" onclick="frmCosComp.SortType.value='IDdesc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeU.gif" alt="" border="0"></a>
			<a href="#" onclick="frmCosComp.SortType.value='IDasc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeD.gif" alt="" border="0"></a>
		</td>
		<td valign="middle">
			<a href="#" onclick="frmCosComp.SortType.value='compNamedesc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeU.gif" alt="" border="0"></a>
			<a href="#" onclick="frmCosComp.SortType.value='compNameasc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeD.gif" alt="" border="0"></a>
		</td>
		<td align="center" valign="middle">
 			<a href="#" onclick="frmCosComp.SortType.value='TransToPaydesc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeU.gif" alt="" border="0"></a>
			<a href="#" onclick="frmCosComp.SortType.value='TransToPayasc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeD.gif" alt="" border="0"></a>
		</td>
		<td align="center" valign="middle">
 			<a href="#" onclick="frmCosComp.SortType.value='TransPendingdesc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeU.gif" alt="" border="0"></a>
			<a href="#" onclick="frmCosComp.SortType.value='TransPendingasc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeD.gif" alt="" border="0"></a>
		</td>
		<td align="center" valign="middle">
			<a href="#" onclick="frmCosComp.SortType.value='TransApprovaldesc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeU.gif" alt="" border="0"></a>
			<a href="#" onclick="frmCosComp.SortType.value='TransApprovalasc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeD.gif" alt="" border="0"></a>
		</td>
		<td align="center" valign="middle">
 			<a href="#" onclick="frmCosComp.SortType.value='TransFaildesc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeU.gif" alt="" border="0"></a>
			<a href="#" onclick="frmCosComp.SortType.value='TransFailasc';frmCosComp.submit();" class="submenu2"><img src="../images/arrowTreeD.gif" alt="" border="0"></a>
		</td>
		<td></td>
		<!--<td></td>-->
	</tr>
	</form>
	<%
	nComCount = 0
	do until rsData.EOF
		nComCount = nComCount+1
		sCompanyID = rsData("companyID")
		If rsData("Closed") Then
			sBlocked="����"
			sColor="#ff6666"
		Elseif rsData("Blocked") Then
			sBlocked="����"
			sColor="#ff8040"
		Else
			sBlocked="����"
			sColor="#66cc66"
		End If
		%>
		<tr><td height="1" colspan="10"  bgcolor="silver"></td></tr>
		<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
			<td style="width:20px" onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';"><img onclick="toggleEditing('<%= sCompanyID %>');" style="cursor:pointer;" id="oListImg<%=sCompanyID%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" /><br /></td>
			<td width="6" align="center" valign="middle" class="txt10" bgcolor="<%=MStatusColor(rsData("ActiveStatus"))%>">
				<%
				If rsData("isNetpayTerminal") Then
					%>
					<img src="../images/1_space.gif" width="1" height="1" border="0"><br />
					<%
				Else
					%>
					<span style="background-color:#eeeeee; color:#eeeeee;"><img src="../images/1_space.gif" alt="" width="6" height="15" border="0"><br /></span>
					<%
				End If
				%>
			</td>
			<td><%=Trim(Left(MStatusText(rsData("ActiveStatus")),1))%><br /></td>
			<td class="txt10" dir="ltr" style="cursor:pointer;" onclick="toggleEditing('<%= sCompanyID %>');" onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';">
				<%= sCompanyID %><br />
			</td>
			<td class="txt11" style="cursor:pointer;" onclick="parent.frmBody.location.href='merchant_data.asp?companyID=<%= sCompanyID %>';parent.fraButton.FrmResize();" onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';" oncontextmenu="ShowHideMenu(div<%= sCompanyID %>);return false;">
				<span style="float:right;"
				 onmouseover="if (div<%= sCompanyID %>.style.display=='none') {event.cancelBubble=true;bHideMenu<%= sCompanyID %>=false;ShowHideMenu(div<%= sCompanyID %>);}return false;"
				 onmouseout="event.cancelBubble=true;bHideMenu<%= sCompanyID %>=true;return false;"><img src="/NPCommon/Images/icon_ArrowDown.gif" align="middle" /></span>
				<%
				If trim(rsData("CompanyName"))="" or trim(rsData("CompanyName"))="-" Then
					sShowName = "(" & trim(rsData("firstName")) & " " & trim(rsData("lastName")) & ")"
				Else
					sShowName = trim(rsData("CompanyName"))
				End If
				If int(len(dbtextShow(sShowName)))<22 Then
					response.write dbTextShow(sShowName) & "<br />"
				Else
					response.write "<span title="""& dbtextShow(sShowName) & """>" & left(dbtextShow(sShowName),21) & "..</span><br />"
				End If
				%>
				<div class="merchantMenu" id="div<%= sCompanyID %>" style="display:none;" onmouseover="bHideMenu<%= sCompanyID %>=false;" onmouseout="bHideMenu<%= sCompanyID %>=true;">
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_dataCompany.asp?companyID=<%= sCompanyID %>">Company Details</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_dataUser.asp?companyID=<%= sCompanyID %>">Personal & Login</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_dataGeneral.asp?companyID=<%= sCompanyID %>">General</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_dataOptions.asp?companyID=<%= sCompanyID %>">Options</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_dataSolutions.asp?companyID=<%= sCompanyID %>">Solutions</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_data_Fees.aspx?ID=<%= sCompanyID %>">Fees</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_dataPayInfo.asp?companyID=<%= sCompanyID %>">Payout</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_data_bankAccounts.aspx?ID=<%= sCompanyID %>">Bank Accounts</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_dataRiskMng.asp?companyID=<%= sCompanyID %>">Risk Management</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_dataTerminal.asp?companyID=<%= sCompanyID %>">Terminals</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_dataNotes.aspx?companyID=<%= sCompanyID %>">Files & Notes</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="Merchant_dataMail.aspx?merchantID==<%= sCompanyID %>">Mail</a>
					<a onmouseover="bHideMenu<%= sCompanyID %>=false;" target="frmBody" onclick="event.cancelBubble=true;parent.fraButton.FrmResize();ShowHideMenu(div<%= sCompanyID %>);" href="merchant_data_Devices.aspx?merchantID=<%= sCompanyID %>">Devices</a>
				</div>
				<script language="javascript" type="text/javascript">
					var bHideMenu<%= sCompanyID %>=false;
					setInterval("if (bHideMenu<%= sCompanyID %>&&(div<%= sCompanyID %>.style.display!='none')) ShowHideMenu(div<%= sCompanyID %>);", 2000);
				</script>
			</td>
			<td align="center" class="txt10" style="cursor:pointer;" 
				onclick="parent.frmBody.location.href='merchant_transPassFilter.asp?companyID=<%= sCompanyID %>&CompanyStatus=<%=rsData("ActiveStatus")%>&numTransToPay=0&ShowName=<%= Server.URLEncode(sShowName) %>';"
				onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';">
				<%= FormatLastDate(rsData("DateLastTransPass")) %><br />
			</td>
			<td align="center" class="txt10" style="cursor:pointer;"
				onclick="parent.frmBody.location.href='merchant_transPending.asp?CompanyID=<%= sCompanyID %>&isClickCount=true';parent.fraButton.FrmResize();"
				onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';">
				<%= FormatLastDate(rsData("DateLastTransPending")) %><br />
			</td>
			<td align="center" class="txt10" style="cursor:pointer;"
				onclick="parent.frmBody.location.href='merchant_transApproval.aspx?ShowCompanyID=<%= sCompanyID %>'; parent.fraButton.FrmResize();"
				onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';">
				<%= FormatLastDate(rsData("DateLastTransPreAuth")) %><br />
			</td>
			<td align="center" class="txt10" dir="ltr" style="cursor:pointer;"
				onclick="parent.frmBody.location.href='merchant_transFail.asp?CompanyID=<%= sCompanyID %>&isClickCount=true';parent.fraButton.FrmResize();"
				onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';">
				<%= FormatLastDate(rsData("DateLastTransFail")) %><br />
			</td>
			<td align="center" class="txt10" dir="ltr" style="cursor:pointer;"
				onclick="parent.frmBody.location.href='merchant_transListHistory.asp?CompanyID=<%= sCompanyID %>&isClickCount=true';"
				onmouseover="this.style.backgroundColor='#bcd5fe';" onmouseout="this.style.backgroundColor='';">
				SHOW<br />
			</td>
		</tr>
		<tr>
			<td id="Row<%= sCompanyID %>" colspan="10"></td>
		</tr>
		<%
		rsData.movenext
	loop
Else
	%>
	<tr>
		<td colspan="4" class="txt13" style="color:maroon;">
			<br />Did not find any merchants !<br />
		</td>
	</tr>
	<%
End If
	
rsData.close
Set rsData = nothing
call closeConnection()

Server.ScriptTimeout = 90
sEndTime = Timer
%>
<tr>
	<td colspan="4" class="txt11" style="color:gray;" dir="ltr">
		<br />sec <%= FormatNumber(sEndTime-sStartTime,3,-1,0,0) %><br />
	</td>
</tr>
</table>
<script language="javascript" type="text/javascript" defer="defer">
	document.getElementById("waitMsg").style.display = 'none';
	<% if nComCount=1 then %>toggleEditing('<%= sCompanyID %>');<% end if %>
</script>
</body>
</html>