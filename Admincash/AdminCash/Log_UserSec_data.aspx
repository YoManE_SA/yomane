<%If trim(request("UseSQL2"))="YES" Then Session("CurrentDSN") = "2" Else Session("CurrentDSN") = "1"%>
<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
	Dim sSQL As String, sQueryWhere As String, sQueryString As String, sDivider As String
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblheader)
		If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere
		If Request("SQL2") = "1" Then
			dbPages.CurrentDSN = 2
			Response.Write(dbPages.ShowCurrentDSN)
		Else
			dbPages.CurrentDSN = 1
		End If
		
		If Request("iInsertDate") <> "" Then
			sQueryWhere &= " AND tblSecurityLog.sl_Date >= '" & Request("iInsertDate") & " 00:00:00'"
			sQueryWhere &= " AND tblSecurityLog.sl_Date <= '" & Request("iInsertDate") & " 23:59:59'" 
		Elseif Trim(Request("toDate")) <> "" Then
			Dim dDateMax As String = Trim(Request("toDate") & " " & Request("toTime"))
			Dim dDateMin As String = Trim(Request("fromDate") & " " & Request("fromTime"))
			If IsDate(dDateMax) Then sQueryWhere &= " AND tblSecurityLog.sl_Date<='" & dDateMax & "'"
			If IsDate(dDateMin) Then sQueryWhere &= " AND tblSecurityLog.sl_Date>='" & dDateMin & "'"
		Else
			sQueryWhere &= " AND tblSecurityLog.sl_Date >= '" & Date.Now.AddDays(-1).ToShortDateString & " 00:00:00'"
			sQueryWhere &= " AND tblSecurityLog.sl_Date <= '" & Date.Today & " 23:59:59'" 
		End If
		
		If Trim(Request("ddlUser")) <> "" Then sQueryWhere &= " AND tblSecurityLog.sl_User =" & dbPages.TestVar(Request("ddlUser"), 0, -1, 0)
		If Trim(Request("ddlDocument")) <> "" Then sQueryWhere &= " AND tblSecurityLog.sl_Document =" & dbPages.TestVar(Request("ddlDocument"), 0, -1, 0)
		If Trim(Request("IPAddress")) <> "" Then sQueryWhere &= " AND tblSecurityLog.sl_IP ='" & Trim(Request("IPAddress")) & "'"
		
		If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") _
		Else PGData.PageSize = 30
		
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		sQueryString = dbPages.SetUrlValue(sQueryString, "PageID", "1")
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
</head>
<body>

	<form id="Form2" runat="server">
		<table align="center" style="width:92%">
		<tr>
			<td colspan="2" id="pageMainHeading">
				<asp:label ID="lblheader" runat="server" /> SECURITY LOG <br />
			</td>
		</tr>
		<tr>
			<td align="right" style="font-size:11px;">
				Filters:
				<%
				If Trim(Request("sl_Date"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"sl_Date",Nothing) &"';"">Insert Date</a>") : sDivider = ", "
				If Trim(Request("ddlUser"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"ddlUser",Nothing) &"';"">User</a>") : sDivider = ", "
				If Trim(Request("ddlDocument"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"ddlDocument",Nothing) &"';"">Document</a>") : sDivider = ", "
				If sDivider <>"" Then Response.Write(" | (click to remove)") _
				Else Response.Write("Add to filter by clicking on returned fields")
				%>
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%">
		<tr>
			<th>Date</th>
			<th>User</th>
			<th>Document</th>
			<th>IP</th>
			<th>Denied</th>
			<th>ReadOnly</th>
		</tr>
		<%
		sSQL = "SELECT tblSecurityLog.*, tblSecurityUser.su_Name, tblSecurityDocument.sd_URL" & _
		" FROM tblSecurityLog" & _
		" INNER JOIN tblSecurityDocument ON (tblSecurityLog.sl_Document=tblSecurityDocument.ID)" & _
		" INNER JOIN tblSecurityUser ON (tblSecurityLog.sl_User = tblSecurityUser.ID)" & sQueryWhere & _
		" ORDER BY sl_Date Desc"
		PGData.OpenDataset(sSQL)
		While PGData.Read()
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
			 <td><%=PGData("sl_Date")%></td>
			 <td><%=PGData("su_Name")%></td>
			 <td><%=PGData("sd_URL")%></td>
			 <td><%=PGData("sl_IP")%></td>
			 <td><%=PGData("sl_IsDenied")%></td>
			 <td><%=PGData("sl_IsReadOnly")%></td>
			</tr>
			<tr><td height="1" colspan="8"  bgcolor="silver"></td></tr>
			<%
		End while
		PGData.CloseDataset()
		%>
		</table>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" align="middle"> ADD TO FILTER
		</div>
	</form>
	<table align="center" style="width:92%">
	<tr>
		<td>
			<UC:Paging runat="Server" id="PGData" PageID="PageID" ShowRecordCount="true" />
		</td>
	</tr>
	</table>	
</body>
</html>