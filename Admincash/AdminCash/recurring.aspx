<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableViewStateMac="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20080616 Tamir
	'
	' Recurring transactions management
	'
	Sub AddConditionToSQL(ByRef sSQL As String, ByVal sCondition As String)
		sSQL &= IIf(sSQL.IndexOf("WHERE") > 0, " AND ", " WHERE ") & sCondition
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsSeries.ConnectionString = dbPages.DSN()
		Dim sSQL As String
		If Page.IsPostBack Then
			sSQL = Session("RecurringAdminCashSQL").ToString
		Else
			Dim sTemp As String = String.Empty, sTempFrom As String = String.Empty, sTempTo As String = String.Empty
			sSQL = "SELECT * FROM viewRecurringAdmin"
			If Request("MerchantID") <> "" Then AddConditionToSQL(sSQL, "MerchantID=" & Request("MerchantID"))
			If Request("MerchantNumber") <> "" Then AddConditionToSQL(sSQL, "MerchantID IN (SELECT ID FROM tblCompany WHERE CustomerNumber=" & Request("MerchantNumber") & ")")
			If Request("MerchantName") <> "" Then AddConditionToSQL(sSQL, "MerchantName LIKE '%" & Request("MerchantName") & "%'")

			Dim sTransTable As String = Request("TransTable"), sTransFrom As String = Request("TransFrom"), sTransTo As String = Request("TransTo")
			If Not String.IsNullOrEmpty(sTransFrom) Then
				If sTransTable.ToLower = "pass" Or String.IsNullOrEmpty(sTransTable) Then sTempFrom &= " OR ra_TransPass>=" & sTransFrom
				If sTransTable.ToLower = "fail" Or String.IsNullOrEmpty(sTransTable) Then sTempFrom &= " OR ra_TransFail>=" & sTransFrom
				If sTransTable.ToLower = "pending" Or String.IsNullOrEmpty(sTransTable) Then sTempFrom &= " OR ra_TransPending>=" & sTransFrom
			End If
			If Not String.IsNullOrEmpty(sTempFrom) Then
				sTempFrom = "(" & sTempFrom.Substring(3) & ")"
				sTemp &= " AND " & sTempFrom
			End If
			If Not String.IsNullOrEmpty(sTransTo) Then
				If sTransTable.ToLower = "pass" Or String.IsNullOrEmpty(sTransTable) Then sTemp &= " OR ra_TransPass<=" & sTransTo
				If sTransTable.ToLower = "fail" Or String.IsNullOrEmpty(sTransTable) Then sTemp &= " OR ra_TransFail<=" & sTransTo
				If sTransTable.ToLower = "pending" Or String.IsNullOrEmpty(sTransTable) Then sTemp &= " OR ra_TransPending<=" & sTransTo
			End If
			If Not String.IsNullOrEmpty(sTempTo) Then
				sTempTo = "(" & sTempTo.Substring(4) & ")"
				sTemp &= " AND " & sTempTo
			End If
			If Not String.IsNullOrEmpty(sTemp) Then
				sTemp = "ID IN (SELECT rc_Series FROM tblRecurringCharge WHERE ID IN (SELECT ra_Charge FROM tblRecurringAttempt WHERE " & sTemp.Substring(5)
			ElseIf Not String.IsNullOrEmpty(sTransTable) Then
				sTemp = "ID IN (SELECT rc_Series FROM tblRecurringCharge WHERE ID IN (SELECT ra_Charge FROM tblRecurringAttempt WHERE ra_Trans" & sTransTable & ">0"
			End If
			If Not String.IsNullOrEmpty(sTemp) Then
				If sTransTable.ToLower = "fail" And Request("ReplyCode") <> "" Then sTemp &= " AND ra_ReplyCode='" & Request("ReplyCode") & "'"
				sTemp &= "))"
				AddConditionToSQL(sSQL, sTemp)
			End If

			If Request("FromDate") <> "" Or Request("ToDate") <> "" Then
				Dim sDateFrom As String = Request("FromDate")
				Dim sDateTo As String = Request("ToDate")
				sTemp = "ID IN (SELECT rc_Series FROM tblRecurringCharge WHERE"
				If sDateFrom <> "" Then sTemp &= " rc_Date>='" & sDateFrom & "'"
				If sDateFrom <> "" And sDateTo <> "" Then sTemp &= " AND"
				If sDateTo <> "" Then sTemp &= " rc_Date<='" & sDateTo & "'"
				sTemp &= ")"
				AddConditionToSQL(sSQL, sTemp)
			End If
			If Request("ChargeAmountFrom") <> "" Or Request("ChargeAmountTo") <> "" Then
				Dim sAmountFrom As String = Request("ChargeAmountFrom")
				Dim sAmountTo As String = Request("ChargeAmountTo")
				sTemp = "ID IN (SELECT rc_Series FROM tblRecurringCharge WHERE"
				If sAmountFrom <> "" Then sTemp &= " rc_Amount>=" & sAmountFrom
				If sAmountFrom <> "" And sAmountTo <> "" Then sTemp &= " AND"
				If sAmountTo <> "" Then sTemp &= " rc_Amount<=" & sAmountTo
				sTemp &= ")"
				AddConditionToSQL(sSQL, sTemp)
			End If
			If Request("ChargeCurrency") <> "" Then AddConditionToSQL(sSQL, "rs_Currency=" & Request("ChargeCurrency"))
			If Request("CardType") <> "" Then
				Dim nCardType As Integer = Request("CardType")
				If nCardType >= ePaymentMethod.PMD_EC_MIN And nCardType <= ePaymentMethod.PMD_EC_MAX Then
					'echeck
					AddConditionToSQL(sSQL, "rs_Echeck>0")
				Else
					'credit card
					AddConditionToSQL(sSQL, "CCTypeID=" & nCardType - 20)
				End If
			End If
			If Request("Country") <> "" Then AddConditionToSQL(sSQL, "BINCountry='" & Request("Country") & "'")
			If Request("Last4") <> "" Then AddConditionToSQL(sSQL, "CardNumber LIKE '%" & Request("Last4") & "'")
			If Request("BIN") <> "" Then AddConditionToSQL(sSQL, "CardNumber LIKE '" & Request("BIN") & "%'")
			If Request("SeriesFrom") <> "" Then AddConditionToSQL(sSQL, "ID>=" & Request("SeriesFrom"))
			If Request("SeriesTo") <> "" Then AddConditionToSQL(sSQL, "ID<=" & Request("SeriesTo"))
			If Request("TransApproval") <> "" Then AddConditionToSQL(sSQL, "ID IN (SELECT ID FROM tblRecurringSeries WHERE rs_Approval=" & Request("TransApproval") & ")")
			If Security.IsLimitedMerchant Then AddConditionToSQL(sSQL, "MerchantID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))")
			If Security.IsLimitedDebitCompany Then
				AddConditionToSQL(sSQL, "((TransType='Regular' AND TransPassFirst IN (SELECT ID FROM tblCompanyTransPass WHERE DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))))" & _
				" OR (TransType='Approval' AND TransPassFirst IN (SELECT ID FROM tblCompanyTransApproval WHERE DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "')))))")
			End If
			sSQL &= " ORDER BY ID DESC"
			Session("RecurringAdminCashSQL") = sSQL
		End If
		lblSQL.Text = sSQL
		dsSeries.SelectCommand = sSQL
		dsSeries.DataBind()
		If gvSeries.Rows.Count = 20 Then
			lblMsg.Text = "More than 20 records found. Only first 20 records are shown. Please refine your search."
		ElseIf gvSeries.Rows.Count = 0 Then
			lblMsg.Text = "No records found. Try specifying less parameters for search."
		Else
			lblMsg.Text = gvSeries.Rows.Count & " records found."
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Recurring Transactions</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<form runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Recurring Transactions</h1>
		<div class="bordered">
			<asp:label ID="lblMsg" runat="server" />
		</div>
		<asp:label ID="lblSQL" runat="server" Visible="false" />
		<asp:GridView ID="gvSeries" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="dsSeries" AllowPaging="True" AllowSorting="False" CssClass="grid"
		 SelectedRowStyle-CssClass="rowSelected" PagerStyle-CssClass="gridPaging" Width="100%">
			<Columns>
				<asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" />
				<asp:BoundField DataField="MerchantName" HeaderText="Merchant" ReadOnly="true" HeaderStyle-HorizontalAlign="left"/>
				<asp:BoundField DataField="rs_ChargeCount" HeaderText="Charges" ReadOnly="true" HeaderStyle-HorizontalAlign="left" HeaderStyle-CssClass="columnTitleVertical"/>
				<asp:BoundField DataField="TransType" HeaderText="Type" ReadOnly="true" HeaderStyle-HorizontalAlign="left" />
				<asp:BoundField DataField="IntervalText" HeaderText="Interval" ReadOnly="true" HeaderStyle-HorizontalAlign="left"/>
				<asp:BoundField DataField="CreateDate" HeaderText="Created" ReadOnly="true" HeaderStyle-HorizontalAlign="left"/>
				<asp:BoundField DataField="Memo" HtmlEncode="false" HeaderText="Memo" ReadOnly="true" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="columnTitleVertical"/>
				<asp:BoundField DataField="rs_Suspended" HeaderText="Susp." ReadOnly="true" HeaderStyle-HorizontalAlign="left"/>
				<asp:BoundField DataField="rs_Paid" HeaderText="Paid" ReadOnly="true" HeaderStyle-HorizontalAlign="left"/>
				<asp:BoundField DataField="Amount" HeaderText="Amount" ReadOnly="true" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"/>
				<asp:BoundField DataField="CardNumber" HeaderText="Card Number" ReadOnly="true" HeaderStyle-HorizontalAlign="left"/>
				<asp:BoundField DataField="TransPassFirst" HeaderText="First Trans." ReadOnly="true" HeaderStyle-HorizontalAlign="left"/>
				<asp:BoundField DataField="ChargeDateFirst" HeaderText="First Charge" ReadOnly="true" HeaderStyle-HorizontalAlign="left"/>
				<asp:BoundField DataField="ChargeDateNext" HeaderText="Next Charge" ReadOnly="true" HeaderStyle-HorizontalAlign="left" NullDisplayText="----------"/>
				<asp:BoundField DataField="ChargeDateLast" HeaderText="Last Charge" ReadOnly="true" HeaderStyle-HorizontalAlign="left"/>
				<asp:CheckBoxField DataField="rs_Blocked" HeaderText="Blocked" ItemStyle-CssClass="centered" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="columnTitleVertical" />
				<asp:CheckBoxField DataField="rs_Deleted" HeaderText="Deleted" ItemStyle-CssClass="centered" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="columnTitleVertical" />
				<asp:CommandField HeaderText="Actions" ButtonType="Button" ItemStyle-CssClass="buttons" ControlStyle-CssClass="buttonWhite" ShowDeleteButton="False" ShowEditButton="True" ShowSelectButton="False" HeaderStyle-CssClass="actions" UpdateText="Save" />
			</Columns>
		</asp:GridView>
		<asp:SqlDataSource ID="dsSeries" runat="server" ProviderName="System.Data.SqlClient"
			SelectCommand=""
			UpdateCommand="UPDATE tblRecurringSeries SET rs_Blocked=IsNull(@rs_Blocked, 0), rs_Deleted=IsNull(@rs_Deleted, 0) WHERE [ID] = @ID">
			<UpdateParameters>
				<asp:Parameter Name="rs_Blocked" Type="Boolean" />
				<asp:Parameter Name="rs_Deleted" Type="Boolean" />
				<asp:Parameter Name="ID" Type="Int32" />
			</UpdateParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>