<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script runat="server">
    Sub Page_Load()
        Security.CheckPermission(lblPermissions)
        lblCurrency.Text = "<select name=""Currency"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
        Dim sSQL As String = "SELECT CUR_ID, CUR_ISOName FROM tblSystemCurrencies ORDER BY CUR_ID"
        Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
        Do While drData.Read
            lblCurrency.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
        Loop
        lblCurrency.Text &= "</select>"

        fdtrControl.FromDateTime = Date.Now.AddMonths(-1).AddDays(1).ToShortDateString
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
        drData.Close()
    End Sub
</script>
<asp:Content ContentPlaceHolderID="body" runat="server">

	<form runat="server" action="Merchant_BalanceData.aspx" method="get" target="frmBody" onsubmit="return filterForm_Submit(null)">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" name="ShowCompanyID" id="ShowCompanyID" />
	<input type="hidden" name="isAllCompanys" />
	<h1><asp:label ID="lblPermissions" runat="server" /> Account Balance<span> - Filter</span></h1>
	<table class="filterNormal">
		<tr>
			<td>
                
				<Uc1:FilterMerchants id="FMerchants" ClientField="ShowCompanyID" SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
				 INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2" class="filter indent">Activity Details</th></tr>
		<tr>
			<td colspan="2">
				<Uc1:FilterDateTimeRange Title="" ID="fdtrControl" runat="server" />
				<input name="DayMin" type="hidden" />
			</td>
		</tr>
		<tr>
			<td>Currency</td>
			<td><asp:Label ID="lblCurrency" runat="server" /></td>
		</tr>
		<tr>
			<td>Amount</td>
			<td>
				<input name="AmountFrom" /> to <input name="AmountTo" />
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top;">Status</td>
			<td>
                <asp:DropDownList runat="server" ID="ddlPending">
                    <asp:ListItem Text="All" Value="" />
                    <asp:ListItem Text="Processed" Value="false" />
                    <asp:ListItem Text="Pending" Value="true" />
			    </asp:DropDownList>
			</td>
		</tr>
        <tr>
			<td style="vertical-align:top;">Results</td>
			<td>
                <asp:DropDownList runat="server" ID="ddlResults">
                    <asp:ListItem Text="Account Status" Value="1" />
                    <asp:ListItem Text="Balance Rows" Value="0" />
			    </asp:DropDownList>
			</td>
        </tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<td class="indent">
				<select name="PageSize" class="grayBG">
					<option value="10">10 rows/page</option>
					<option value="25" selected="selected">25 rows/page</option>
					<option value="50">50 rows/page</option>
					<option value="100">100 rows/page</option>
				</select>
			</td>
			<td class="indent" align="right" nowrap="nowrap">
				<input type="submit" class="buttonFilter" value="Search" />
			</td>
		</tr>
	</table>
	</form>
</asp:Content>