<%@ Page Language="VB" AspCompat="true" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    Sub SetContinueLink()
        hlContinue.NavigateUrl = "Wire_search_data.asp?"
        Dim bFirst As Boolean = True
        For Each sItem As String In Request.QueryString.Keys
            If bFirst Then
                bFirst = False
            Else
                hlContinue.NavigateUrl &= "&"
            End If
            hlContinue.NavigateUrl &= sItem & "=" & Request.QueryString(sItem)
        Next
    End Sub

    Sub SaveFile(ByVal o As Object, ByVal e As CommandEventArgs)
        Response.AddHeader("content-disposition", "attachment;filename=" & e.CommandArgument)
        Dim srTransfers As New System.IO.StreamReader(Server.MapPath("/Data/WireTransfer/BankOfGeorgia/" & e.CommandArgument))
        Response.Write(srTransfers.ReadToEnd)
        srTransfers.Close()
        srTransfers.Dispose()
        Response.End()
    End Sub

    Sub AddWireRecord(ByVal bfRecords As BOG.File, ByVal nWire As Integer, ByVal nCurrency As Integer)
        Dim sSQL As String = "SELECT * FROM tblWireMoney WHERE WireStatus=0 AND WireProcessingCurrency IN (1, 2) AND WireMoney_ID=" & nWire
        Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
        drData.Read()
        Dim nAmount As Decimal = drData("wireAmount")
        If drData("wireCurrency") <> drData("wireProcessingCurrency") Then
            nAmount *= drData("WireExchangeRate")
        End If
        litLog.Text &= "&nbsp; &nbsp; &nbsp; Amount:" & nAmount & "<br />"
        Dim bOrder As Boolean = (drData("WireType") = 2), bProfile As Boolean = False
        If bOrder Then
            sSQL = "SELECT CompanyMakePaymentsProfiles_ID FROM tblCompanyMakePaymentsRequests WHERE CompanyMakePaymentsRequests_id=" & drData("WireSourceTbl_ID")
            bProfile = (dbPages.ExecScalar(sSQL) > 0)
        End If
        Dim drData2 As SqlDataReader, drData3 As SqlDataReader
        If bOrder Then
            litLog.Text &= "&nbsp; &nbsp; &nbsp; Payment Order"
            drData2 = dbPages.ExecReader("SELECT * FROM tblCompanyMakePaymentsRequests WHERE CompanyMakePaymentsRequests_ID=" & drData("WireSourceTbl_ID"))
        Else
            litLog.Text &= "&nbsp; &nbsp; &nbsp; Transaction Payout"
            drData2 = dbPages.ExecReader("SELECT * FROM tblTransactionPay WHERE ID=" & drData("WireSourceTbl_ID"))
        End If
        drData2.Read()
        If bProfile Then
            litLog.Text &= " - to beneficiary<br />"
            sSQL = "SELECT LTrim(RTrim(bankAbroadBankAddressCity+' '+bankAbroadBankAddressZip+' '+bankAbroadBankAddressState)) bankAbroadBankAddressSecondLine, bankAbroadIBAN," & _
            " LTrim(RTrim(bankAbroadBankAddressCity2+' '+bankAbroadBankAddressZip2+' '+bankAbroadBankAddressState2)) bankAbroadBankAddressSecondLine2, bankAbroadIBAN2, tblCompanyMakePaymentsProfiles.*," & _
            " IsNull(c1.Name, '') AS bankAbroadBankAddressCountryName, IsNull(c2.Name, '') AS bankAbroadBankAddressCountryName2 FROM tblCompanyMakePaymentsProfiles" & _
            " LEFT JOIN [List].[CountryList] AS c1 ON bankAbroadBankAddressCountry = c1.Countryid LEFT JOIN [List].[CountryList] c2 ON bankAbroadBankAddressCountry2=c2.Countryid" & _
            " WHERE CompanyMakePaymentsProfiles_ID=" & drData2("CompanyMakePaymentsProfiles_ID")
        Else
            litLog.Text &= " - self payment<br />"
            sSQL = "SELECT LTrim(RTrim(paymentAbroadBankAddressCity+' '+paymentAbroadBankAddressZip+' '+paymentAbroadBankAddressState)) paymentAbroadBankAddressSecondLine, PaymentAbroadIBAN," & _
            " LTrim(RTrim(paymentAbroadBankAddressCity+' '+paymentAbroadBankAddressZip+' '+paymentAbroadBankAddressState)) paymentAbroadBankAddressSecondLine2,PaymentAbroadIBAN2, tblCompany.*," & _
            " LTrim(RTrim(City+' '+Zip+' '+State)) Address2, IsNull(c1.Name, '') AS paymentAbroadBankAddressCountryName, IsNull(c2.Name, '') AS paymentAbroadBankAddressCountryName2" & _
            " FROM tblCompany LEFT JOIN [List].[CountryList] AS c1 ON paymentAbroadBankAddressCountry = c1.Countryid LEFT JOIN [List].[CountryList] c2 ON paymentAbroadBankAddressCountry2=c2.Countryid" & _
            " WHERE tblCompany.ID=" & drData("Company_ID")
        End If
        'Response.Write (sSQL)			
        'Response.end		
        drData3 = dbPages.ExecReader(sSQL)
        drData3.Read()
        Dim brTransfer As New BOG.Record
        brTransfer.SenderAccount = bfRecords.DebitAccount
        If nAmount = 0 Then
            litLog.Text &= "ERROR: ZERO AMOUNT !!!"
            Response.End()
        End If
        brTransfer.TransferAmount = nAmount
        Select Case drData("wireProcessingCurrency")
            Case 1 : brTransfer.TransferCurrency = BOG.Currency.USD
            Case 2 : brTransfer.TransferCurrency = BOG.Currency.EUR
        End Select
        brTransfer.DocumentDate = bfRecords.SubmissionDate
        brTransfer.PaymentDetails = "Wire ID " & nWire
        Dim sSwift_Beneficiary As String = "", sSwift_Intermediate As String = "", sIBAN_Beneficiary As String = "", sIBAN_Intermediate As String = "", sBeneficiaryName As String = ""
        sBeneficiaryName = drData("wirePaymentPayeeName")
        brTransfer.Beneficiary = sBeneficiaryName.ToUpper()
        If bProfile Then
            sSwift_Beneficiary = drData3("bankAbroadSwiftNumber")
            sSwift_Intermediate = drData3("bankAbroadSwiftNumber2")
            sIBAN_Beneficiary =  drData3("bankAbroadIBAN")
            sIBAN_Intermediate =  drData3("bankAbroadIBAN2")

            brTransfer.BeneficiaryAccount = drData3("bankAbroadAccountNumber")
            'If Not IsDBNull(sSwift_Beneficiary) and Not IsDBNull(sIBAN_Beneficiary) then
            If Not IsDBNull(sSwift_Beneficiary) then
                brTransfer.BeneficiaryBankSwiftCode = sSwift_Beneficiary
            ElseIf IsDBNull(sSwift_Beneficiary) and Not IsDBNull(sIBAN_Beneficiary) then
                brTransfer.BeneficiaryAccount = sIBAN_Beneficiary
            End If
            'If Not IsDBNull(sSwift_Intermediate) and Not IsDBNull(sIBAN_Intermediate) then
            If Not IsDBNull(sSwift_Intermediate)  then
                brTransfer.IntermediaryBankSwiftCode = sSwift_Intermediate
            ElseIf IsDBNull(sSwift_Intermediate) and Not IsDBNull(sIBAN_Intermediate) then
                brTransfer.IntermediaryBankSwiftCode = sIBAN_Intermediate
            End If
            brTransfer.IntermediaryBank = drData3("bankAbroadBankName2")
        Else
            sSwift_Beneficiary = drData3("PaymentAbroadSwiftNumber")
            sSwift_Intermediate = drData3("paymentAbroadSwiftNumber2")
            sIBAN_Beneficiary =  drData3("PaymentAbroadIBAN")
            sIBAN_Intermediate =  drData3("PaymentAbroadIBAN2")

            brTransfer.BeneficiaryAccount = drData3("paymentAbroadAccountNumber")
            'If Not IsDBNull(sSwift_Beneficiary) and Not IsDBNull(sIBAN_Beneficiary) then
            If Not IsDBNull(sSwift_Beneficiary)  then
                brTransfer.BeneficiaryBankSwiftCode = sSwift_Beneficiary
            ElseIf IsDBNull(sSwift_Beneficiary) and Not IsDBNull(sIBAN_Beneficiary) then
                brTransfer.BeneficiaryAccount = sIBAN_Beneficiary
            End If
            'If Not IsDBNull(sSwift_Intermediate) and Not IsDBNull(sIBAN_Intermediate) then
            If Not IsDBNull(sSwift_Intermediate)  then
                brTransfer.IntermediaryBankSwiftCode = sSwift_Intermediate
            ElseIf IsDBNull(sSwift_Intermediate) and Not IsDBNull(sIBAN_Intermediate) then
                brTransfer.IntermediaryBankSwiftCode = sIBAN_Intermediate
            End If
            brTransfer.IntermediaryBank = drData3("paymentAbroadBankName2")
        End If
        bfRecords.AddRecord(brTransfer)
        drData.Close()
        drData2.Close()
        drData3.Close()
    End Sub

    Sub ReadRecords(ByVal bfRecords As BOG.File)
        Dim nWire As Integer, nCurrency As Integer
        For Each sItem As String In Request.Form.Keys
            If sItem.StartsWith("wireAction") And Request.Form(sItem) = "8" Then
                nWire = dbPages.TestVar(sItem.Substring(10), 1, 0, 0)
                If nWire > 0 Then
                    litLog.Text &= "BOG: Wire " & nWire & "<br />"
                    nCurrency = dbPages.TestVar(Request.Form("currencyToUse" & nWire), 1, 0, -1)
                    If nCurrency >= 0 Then
                        litLog.Text &= "&nbsp; &nbsp; &nbsp; Currency: " & dbPages.GetCurText(nCurrency) & "<br />"
                        If nCurrency = eCurrencies.GC_USD Or nCurrency = eCurrencies.GC_EUR Then
                            AddWireRecord(bfRecords, nWire, nCurrency)
                            litLog.Text &= "&nbsp; &nbsp; &nbsp; Record has been added successfully.</span><br />"
                            dbPages.ExecSql("UPDATE tblWireMoney SET wireStatus=8, wireStatusDate=GetDate() WHERE WireMoney_ID=" & nWire)
                        Else
                            litLog.Text &= "<span class=""errorMessage"">&nbsp; &nbsp; &nbsp; This currency is invalid for BOG!</span><br />"
                        End If
                    End If
                End If
            End If
        Next
    End Sub

    Sub Page_Load()
        If Not Page.IsPostBack Then
            litLog.Text = String.Empty
            SetContinueLink()
            Dim sSQL As String = dbPages.ExecScalar("SELECT IsNull(BA_AccountNumber, '') FROM tblBankAccounts WHERE BA_ID=" & dbPages.TestVar(Request.Form("BOGAccount"), 1, 0, 0))
            'Dim sSQL As String = dbPages.ExecScalar("SELECT IsNull(AccountNumber, '') FROM [Finance].[WireAccount] WHERE WireAccount_id=" & dbPages.TestVar(Request.Form("BOGAccount"), 1, 0, 0))
            Dim sAccount As String = sSQL
            If String.IsNullOrEmpty(sAccount) Then
                litLog.Text &= "<span class=""errorMessage"">No valid BOG account selected!</span> " & Request.Form("BOGAccount") & "<br />"
                lbSave.Visible = False
            Else
                litLog.Text &= "BOG account: " & sAccount & "<br />"
                Dim nYear As Integer = dbPages.TestVar(Request.Form("BOGYear"), Date.Today.Year, Date.Today.Year + 1, 0)
                Dim nMonth As Integer = dbPages.TestVar(Request.Form("BOGMonth"), 1, 12, 0)
                Dim nDay As Integer = dbPages.TestVar(Request.Form("BOGDay"), 1, 31, 0)
                If nYear = 0 Or nMonth = 0 Or nDay = 0 Then
                    litLog.Text &= "<span class=""errorMessage"">No valid date specified!</span> " & nDay & "/" & nMonth & "/" & nYear & "<br />"
                    lbSave.Visible = False
                Else
                    Dim dSubmission As New Date(nYear, nMonth, nDay)
                    If dSubmission < Date.Today Then
                        litLog.Text &= "<span class=""errorMessage"">Past date specified!</span> " & dSubmission.ToShortDateString & "<br />"
                        lbSave.Visible = False
                    Else
                        litLog.Text &= "Submission date: " & dSubmission.ToShortDateString & "<br />"
                        Dim bfTransfers As New BOG.File(Server.MapPath("/Data/WireTransfer/BankOfGeorgia"), , sAccount, dSubmission)
                        ReadRecords(bfTransfers)
                        litLog.Text &= "All records have been read.<br />"
                        Dim sFile As String = bfTransfers.Save()
                        litLog.Text &= "File saved: " & sFile
                        lbSave.CommandArgument = sFile
                    End If
                End If
            End If
        End If
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Wire BOG</title>
	<link type="text/css" rel="stylesheet" href="../StyleSheet/StyleAdminNetEng.css" />
</head>
<body>
	<form runat="server">
		<div style="font-size:12px;">
			<asp:Literal ID="litLog" runat="server" />
			<hr />
			<asp:LinkButton ID="lbSave" OnCommand="SaveFile" CommandName="download" Text="Save File" runat="server" />
			<br /><br />
			<asp:HyperLink ID="hlContinue" Text="Continue" runat="server" />
		</div>
	</form>
</body>
</html>