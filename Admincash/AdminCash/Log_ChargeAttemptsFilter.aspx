<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		fdtrControl.FromDateTime = Date.Now.ToShortDateString
		fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
	<form runat="server" action="Log_ChargeAttempts.aspx" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />
    <input type="hidden" id="Source" name="Source" value="Filter" />
	<h1><asp:label ID="lblPermissions" runat="server" /> Charge Attempts<span> - Filter</span></h1>
	<table class="filterNormal">
		<tr>
			<td>
				<Uc1:FilterMerchants id="FMerchants" ClientField="CompanyID" runat="server" />
				<input type="hidden" id="CompanyID" name="CompanyID" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Free Text</th></tr>
		<tr><td><input type="text" size="28" dir="ltr" name="iFreeText" value=""></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Reply Code</th></tr>
		<tr><td><input type="text" size="7" dir="ltr" name="iReplyCode" value=""></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Transaction Id</th></tr>
		<tr>
			<td>
			    <select name="iTransTbl" style="width:80px;">
			        <option value="pass">Passed</option>
			        <option value="fail">Failed</option>
			    </select>
			    <input type="text" size="8" dir="ltr" name="iTransNum" value="">
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Process Duration</th></tr>
		<tr>
			<td>
			    <select name="sDurationFilterMode" style="width:50px;">
			        <option value="lowerThan">&lt;</option>
			        <option value="greaterThan">&gt;</option>
			    </select>
			    <input type="text" size="4" dir="ltr" name="iProcessDuration" value="">
			    Sec
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
	    <tr>
		    <td colspan="2">
			    <input type="checkbox" name="SQL2" value="1" checked="checked" class="option">Use SQL2 when possible
		    </td>
	    </tr>
	    <tr><td height="6"></td></tr>
	    <tr>	
		    <td>
			    <select name="iPageSize" class="grayBG">
				    <option value="10">10 rows/page</option>
				    <option value="25" selected="selected">25 rows/page</option>
				    <option value="50">50 rows/page</option>
				    <option value="100">100 rows/page</option>
			    </select>
		    </td>
		    <td style="text-align:right;">
			    <input type="submit" name="Action" value="SEARCH">
		    </td>
	    </tr>
	</table>
	</form>
</asp:Content>