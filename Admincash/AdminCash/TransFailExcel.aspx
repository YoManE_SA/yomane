<%@ Page Language="VB" ValidateRequest="false" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Sub Page_Load()
		Dim sFromDate As String = String.Empty, sToDate As String = String.Empty
		If Not String.IsNullOrEmpty(Request("fromDate")) Then
			sFromDate = Request("fromDate")
			If Not String.IsNullOrEmpty(Request("fromTime")) Then sFromDate &= " " & Request("fromTime")
		End If
		If Not String.IsNullOrEmpty(Request("toDate")) Then
			sToDate = Request("toDate")
			If Not String.IsNullOrEmpty(Request("toTime")) Then sToDate &= " " & Request("toTime")
		End If

		Dim sbSQL As New StringBuilder("EXEC ReportGetDeclinesExcel @nMaxRecords=10000")
		If Not String.IsNullOrEmpty(Request("CompanyID")) Then sbSQL.Append(", @nMerchant=" & Request("CompanyID"))
		If Not String.IsNullOrEmpty(sFromDate) Then sbSQL.Append(", @sFromDate='" & sFromDate & "'")
		If Not String.IsNullOrEmpty(sToDate) Then sbSQL.Append(", @sToDate='" & sToDate & "'")
		If Not String.IsNullOrEmpty(Request("DebitCompanyID")) Then sbSQL.Append(", @nDebitCompany=" & Request("DebitCompanyID"))
		If Not String.IsNullOrEmpty(Request("Terminal")) Then sbSQL.Append(", @sTerminalNumber='" & Request("Terminal") & "'")
		If Not String.IsNullOrEmpty(Request("TransType")) Then sbSQL.Append(", @bIsAuth=" & Request("TransType"))
		If Not String.IsNullOrEmpty(Request("AnswerNumber")) Then sbSQL.Append(", @sReplyCode='" & Request("AnswerNumber") & "'")
		If Not String.IsNullOrEmpty(Request("DebitReferenceCode")) Then sbSQL.Append(", @sDebitReferenceCode='" & Request("DebitReferenceCode") & "'")
		If Not String.IsNullOrEmpty(Request("AmountMin")) Then sbSQL.Append(", @nAmountFrom=" & Request("AmountMin"))
		If Not String.IsNullOrEmpty(Request("AmountMax")) Then sbSQL.Append(", @nAmountTo=" & Request("AmountMax"))
		If Not String.IsNullOrEmpty(Request("Currency")) Then sbSQL.Append(", @nCurrency=" & Request("Currency"))
		If Not String.IsNullOrEmpty(Request("FreeText")) Then sbSQL.Append(", @sFreeText='" & Request("FreeText") & "'")

		Response.ContentType = "application/vnd.ms-excel"
		Response.AddHeader("Content-Disposition", "attachment;filename=declines_" & Date.Now.ToString("yyyyMMddhhmmss") & ".xls")
		Dim drData As SqlDataReader = dbPages.ExecReader(sbSQL.ToString)
		While drData.Read
			Response.Write(drData(0))
		End While
	End Sub
</script>
