<%
	response.CharSet=1255
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_security.asp" -->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_htmlInputs.asp"-->
<%
sCompanyID = dbText(request("CompanyID"))
nMerchantTransID = TestNumVar(request("merchantTransID"), 0, -1, 0)
tblName = dbText(request("tblName"))

sSQL="SELECT CustomerNumber, CompanyName, IsSystemPayCVV2, IsSystemPayPhoneNumber, IsSystemPayPersonalNumber, IsSystemPayEmail, " &_
"IsRefund, IsConfirmation, isBillingAddressMust FROM tblCompany WHERE ID=" & sCompanyID
set rsData2=oledbData.execute(sSQL)
If NOT rsData2.EOF then
	sCompanyName = rsData2("CompanyName")
	nCustomerNumber = rsData2("CustomerNumber")
	if request("isUseLimitation") then
		bIsBillingAddressMust = rsData2("isBillingAddressMust")
		bCVV2 = rsData2("IsSystemPayCVV2")
		bPersonalNumber = rsData2("IsSystemPayPersonalNumber")
		bPhoneNumber = rsData2("IsSystemPayPhoneNumber")
		bEmail = rsData2("IsSystemPayEmail")
		bRefund = rsData2("IsRefund")
		bConfirmation = rsData2("IsConfirmation")
	else
		bIsBillingAddressMust = false
		bCVV2 = false
		bPersonalNumber = false
		bPhoneNumber = false
		bEmail = false
		bRefund = true
		bConfirmation = true
	end if
else
	response.write "Merchant not found"
	response.end
end if
rsData2.close
Set rsData2 = nothing


TransAmount = ""
TransCurrency = ExecScalar("Select PaymentReceiveCurrency From tblCompany Where ID = " & sCompanyID, 0)
TransPayments = 1
TransCreditType = 1
CcPersonalNumber = ""
CcCCard_number = ""
CcExpMM = ""
CcExpYY = ""
CcMember = ""
Cc_cui = ""
CcPhoneNumber = ""
CcEmail = ""
BaAddress1 = ""
BaAddress2 = ""
BaCity = ""
BaZipCode = ""
BaStateIso = ""
BaCountryIso = ""
OrderNumber = ""
nRecurringSeries = 0
nRecurringChargeNumber = 0
		
If isNumeric(nMerchantTransID) and tblName<>"" Then
	sSQL="SELECT tblCompanyTrans"&tblName&".Amount, tblCompanyTrans"&tblName&".Currency, tblCompanyTrans"&tblName&".Payments, tblCompanyTrans"&tblName&".CreditType, tblCompanyTrans"&tblName&".OrderNumber, " &_
	"tblCreditCard.id As CreditCard_id, tblCreditCard.PersonalNumber, dbo.GetDecrypted256(tblCreditCard.CCard_number256) CCard_number, tblCreditCard.ExpMM, tblCreditCard.ExpYY, tblCreditCard.Member, tblCreditCard.cc_cui, " &_
	"tblCreditCard.phoneNumber, tblCreditCard.email, tblBillingAddress.address1, tblBillingAddress.address2, tblBillingAddress.city, " &_
	"tblBillingAddress.zipCode, tblBillingAddress.stateIso, tblBillingAddress.countryIso " &_
	"FROM tblCompanyTrans"&tblName&" LEFT OUTER JOIN tblCreditCard ON tblCompanyTrans"&tblName&".CreditCardID = tblCreditCard.ID LEFT OUTER JOIN tblBillingAddress ON tblCreditCard.BillingAddressId = tblBillingAddress.id " &_
	"WHERE tblCompanyTrans"&tblName&".PaymentMethod_id = 1 AND tblCompanyTrans"&tblName&".id = " & nMerchantTransID
	set rsData2=oledbData.execute(sSQL)
	If NOT rsData2.EOF then
		CreditCard_id = rsData2("CreditCard_id")
		TransAmount = rsData2("Amount")
		TransCurrency = rsData2("Currency")
		TransPayments = rsData2("Payments")
		TransCreditType = rsData2("CreditType")
		CcCCard_number = rsData2("CCard_number")
		If trim(CcCCard_number)<>"" then
			CcCCard_Decrypt = CcCCard_number
			'CcCCard_number1 = "****" CcCCard_number2 = "****" CcCCard_number3 = "****"
			CcCCard_Decrypt = Replace(CcCCard_Decrypt, " ", "")
			CcCCard_number1 = left(replace(CStr(CcCCard_Decrypt)," ",""),4)
			'CcCCard_number2 = mid(replace(CStr(CcCCard_Decrypt)," ",""), 5, 4)
			CcCCard_number2 = mid(replace(CStr(CcCCard_Decrypt)," ",""), 5, 2)
			if Len(CcCCard_Decrypt) > 8 Then _
				CcCCard_number3 = ""
				'CcCCard_number3 = mid(replace(CStr(CcCCard_Decrypt)," ",""), 9, 4)
			if Len(CcCCard_Decrypt) > 12 Then _
				CcCCard_number4 = right(replace(CStr(CcCCard_Decrypt)," ",""), Len(CcCCard_Decrypt) - 12)
		End if
		CcExpMM = rsData2("ExpMM")
		CcExpYY = rsData2("ExpYY")
		CcMember = rsData2("Member")
		If Trim(rsData2("cc_cui")) <> "" Then Cc_cui = ""
		'If Trim(rsData2("cc_cui")) <> "" Then Cc_cui = DecCVV(rsData2("cc_cui"))
		CcPersonalNumber = rsData2("PersonalNumber")
		CcPhoneNumber = rsData2("phoneNumber")
		CcEmail = rsData2("email")
		BaAddress1 = rsData2("address1")
		BaAddress2 = rsData2("address2")
		BaCity = rsData2("city")
		BaZipCode = rsData2("zipCode")
		BaStateIso = rsData2("stateIso")
		BaCountryIso = rsData2("countryIso")
		OrderNumber = rsData2("OrderNumber")
	End if
	rsData2.Close
	'
	'
	' If resending failed recurring transaction, pass recurring series and charge number
	'
	If UCase(tblName) = "FAIL" Then
		sSQL="SELECT rc_Series, rc_ChargeNumber FROM tblRecurringAttempt a INNER JOIN tblRecurringCharge c ON ra_Charge=c.ID WHERE ra_Trans" & tblName & "=" & nMerchantTransID
		Set rsData2 = oledbData.execute(sSQL)
		If Not rsData2.EOF Then
			nRecurringSeries = rsData2(0)
			nRecurringChargeNumber = rsData2(1)
		End If
		rsData2.Close
	End If
	Set rsData2 = Nothing
End if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><%= COMPANY_NAME_1 %> - Control Panel</title>
    <meta http-equiv="Content-Type" content="text/html;charset=Windows-1255" />
    <link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css" />
    <script src="../js/formValidator2.js" type="text/javascript"></script>
    <script type="text/javascript">
        function OpenPop(sURL,sName,sWidth,sHeight,sScroll) {
			placeLeft = screen.width/2-sWidth/2
            placeTop = screen.height/2-sHeight/2
			var winPrint=window.open(sURL,sName,'TOP='+placeTop+',LEFT='+placeLeft+',WIDTH='+sWidth+',HEIGHT='+sHeight+',scrollbars='+sScroll);
        }
		
        var vm = new validationManager("hebrew");
        var vm2 = new validationManager("hebrew");
        var vm3 = new validationManager("hebrew");

        function registerValidation() {
            // register validation
            formInstance = document.frmCard;
            //vm.add(new ccValidation("document.frmCard.CCard_num", "Credit Card Number"));
            //vm.add(new textValidation(formInstance.Member, "Card holder Fullname", "string", 1, 50, true));
            vm.add(new textValidation(formInstance.Amount, "Amount", "numeric", 1, 7, true));
            //vm.add(new ccDateValidation(formInstance.ExpMonth, formInstance.ExpYear));
            //vm.add(new selectValidation(formInstance.CreditType, "Credit Type"));
            <% if bEmail then %>vm.add(new textValidation(formInstance.Email, "Email Address", "email", 1, 50, true));<% End If %>
			<% if bPhoneNumber then %>vm.add(new textValidation(formInstance.PhoneNumber, "Phone Number", "string", 1, 20, true));<% End If %>
			<% if bPersonalNumber then %>vm.add(new textValidation(formInstance.PersonalNumber, "Personal Number", "string", 1, 20, true));<% End If %>
			<%if bIsBillingAddressMust then %>
				vm.add(new textValidation(formInstance.BillingAddress1, "Address 1", "string", 1, 200, true));
            vm.add(new textValidation(formInstance.BillingCity, "City", "string", 1, 50, true));
            vm.add(new textValidation(formInstance.BillingZipCode, "ZipCode", "string", 1, 50, true));
            <%end if %>
			vm2.add(new textValidation(document.frmAdminCharge.Amount, "Amount", "numeric", 1, 7, true));
            vm3.add(new textValidation(document.frmBalance.Amount, "Amount", "numeric", 1, 7, true));
        }

        function CheckForm() {
            <%
			if bIsBillingAddressMust then
				%>
				formInstance = document.frmCard;
            if (formInstance.BillingCountry.value=='228' || formInstance.BillingCountry.value=='43') {
                if (formInstance.BillingState.value=='' || formInstance.BillingState.value=='0') {
                    alert('Please Choose Country');
                    formInstance.BillingState.focus();
                    return false;
                }
            }
            <%
        End If
			
            if bCVV2 then
				%>
				formInstance = document.frmCard;
            if (! formInstance.isCheckCVV2.checked) {
                if (formInstance.CVV2.value=='') {
                    alert('CVV2 is required');
                    formInstance.CVV2.focus();
                    return false;
                }
            }
            <%
        End If
        %>
			
        return  vm.validate();
        }
			

        function CheckFormInter() {
            return  vm2.validate();
        }
		
        function ShowPayments() {
            if (document.frmCard.CreditType.value==8 || document.frmCard.CreditType.value==6) {
                document.frmCard.Payments.disabled=false;
            } else {
                document.frmCard.Payments.disabled=true;
                document.getElementById('RefTransID').disabled = !((document.frmCard.CreditType.value == 0) || document.frmCard.TypeCharge[4].checked);
            }
        }
		
        function ShowConfirmationInput() {
            if (document.frmCard.TypeCharge[2].checked) {
                document.frmCard.ConfirmationNum.disabled=false;
                document.frmCard.ConfirmationNum.style.backgroundColor='#ffffff';
                document.frmCard.ConfirmationNum.value='';
            }
            else {
                document.frmCard.ConfirmationNum.disabled=true;
                document.frmCard.ConfirmationNum.style.backgroundColor='#f5f5f5';
                document.frmCard.ConfirmationNum.value='Enter Confirmation Number';
            }
            ShowPayments();
        }
    </script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <style>
        body {
            font-family: 'Open Sans', sans-serif;
        }

        .bg-form {background: #f5f5f5; border: 1px solid #c0c0c0;  padding: 10px;}
       
        .box-info-color {
            background: #dbe3ff;
            border: 1px solid #a2b4ee;
            padding: 10px;
        }

        h2 {
            margin: 0;
            padding: 0;
            font-size: 14px;
            padding: 10px 0;
            font-size: 14px;
            color: #2566AB;
        }

        input[type="text"], select {
            border: 1px solid #c0c0c0;
            padding: 3px 5px;
            border-radius: 3px;
        }

        .form-group {
            float: left;
            width: 33%;
            margin-bottom: 10px;
        }

        .clearfix {
            clear: both;
        }

        input[type="button"] {
            background: #2566AB;
            color: #ffffff;
            border-radius: 3px;
            border: none;
            font-weight: normal;
            font-family: 'Open Sans', sans-serif;
            padding: 5px 10px;
            cursor: pointer;
        }

            input[type="button"]:hover {
                background: #76a8dd;
            }
        input[type="radio"], input[type="checkbox"]  {position: relative; top: 2px;}
        input[type="radio"] + label {padding-right: 5px;  display:inline-block;}
        .wrap-button {
            text-align: right;
            padding-top: 10px;
        }

        hr {
            border-top: 1px solid #c2c2c2;
            border-bottom: none;
            border-left: none;
            border-right: none;
        }

        .container {
            width: 900px;
            font-size: 12px;
            margin: 0 auto;
        }
        .error {background: #FFCECE; border-radius: 3px; border: 1px solid #df8f8f; color: #665252;  padding: 10px;}
        .mb-10 {margin-bottom: 10px;}
        .pb-10 {padding-bottom: 10px;}
        .w-250 {width: 250px;}
        .w-80 {width: 80px;}
    </style>
</head>
<body onload="registerValidation();">

    <div class="container">

        <div class="mb-10">
            <span id="pageMainHeading">Virtual Terminal</span> - <%= dbtextShow(sCompanyName) %>
        </div>
        <%
        pageSecurityLevel = 0
        pageClosingTags = "</table>"
        Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
        %>

         <!-- Error Alert -->
         <% if trim(request("Error"))<>"" then %>
         <div class="error"><%= request("Error") %></div>
         <% end if %>

        <!-- 000 Preview  -->
        <article class="bg-form">
        <section class="box-info" style="border-bottom: 1px solid #c2c2c2; " >
            <form action="charge_form.asp" name="frmLimitation" method="post">
                <input type="hidden" name="CompanyID" value="<%= request("CompanyID") %>" />
                <input name="CompanyNum" type="hidden" value="<%= nCustomerNumber %>" />
                <div class="pb-10">
                    <%
				    if request("isUseLimitation") then
                    %>
					    [ <a style="text-decoration: underline;" href="#" onclick="frmLimitation.submit();">Cancel Limitation Charges</a> ]
					    <input type="Hidden" name="isUseLimitation" value="0" />
                    <%
				    else
                    %>
					    [ <a style="text-decoration: underline;" href="#" onclick="frmLimitation.submit();">Allow Limitation Charges</a> ]
					    <input type="Hidden" name="isUseLimitation" value="1" />
                    <%
				    end if
                    %>
				    [ <a href="#" onclick="OpenPop('charge_Customer_list.asp','fraCustomer',350,450,1);">Charge Customer</a> ] 
                </div>
                <form action="" method="post">
                    <div class="form-group">
                        <div>Transaction Number</div>
                        <div>
                            <input type="text" name="merchantTransID" tabindex="1" class="w-250" value="<%=nMerchantTransID%>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div>Transaction Mode</div>
                        <div>
                            <select name="tblName" tabindex="2" class="w-250">
                                <option value="pass" <%if tblName="pass" Then Response.Write("selected")%>>Approved</option>
                                <option value="fail" <%if tblName="fail" Then Response.Write("selected")%>>Decline</option>
                            </select>
                        </div>
                    </div>
                    <div class="wrap-button">
                        <input type="button" value="Show" />
                    </div>
                    <div class="clearfix"></div>
                </form>
            </form>
        </section>


        <form method="post" action="charge_form.asp" name="frmCard">
            <input type="hidden" name="compname" value="<%= sCompanyName %>" />
            <input type="hidden" name="CompanyID" value="<%= sCompanyID %>" />
            <input type="hidden" name="CompanyNum" value="<%= nCustomerNumber %>" />
            <input type="hidden" name="CreditCard_id" value="<%= CreditCard_id %>" />
            <input type="hidden" name="CustomerID" value="0" />
            <input type="hidden" name="Action" value="" />


         <!-- 001 Payment Type -->
         <h2>Payment Type</h2>
         <section class="box-info">
                <select name="PaymentType" tabindex="3" class="w-250" onchange="document.getElementById('PaymentMethodCC').style.display = (selectedIndex == 1 ? 'none' : '');document.getElementById('PaymentMethodECHECK').style.display = (selectedIndex == 0 ? 'none' : '');">
                    <option value="1">Credit Card
                    <option value="2">E-Check
                </select>
            </section>

          <!-- 002 Transaction Type -->
         <h2>Transaction type*</h2>
         <section class="box-info">
                   <div class="pb-10">
                        <input type="Radio" tabindex="4" checked="checked" value="0" name="TypeCharge" onclick="ShowConfirmationInput();" />Process
				        <input type="Radio" tabindex="5" value="-1" name="TypeCharge" onclick="ShowConfirmationInput();" />  Create Only (local)
				        <input type="Radio" tabindex="6" value="0" name="TypeCharge" onclick="ShowConfirmationInput();" > Process with an approval #
                        <input type="Radio" tabindex="7" value="1" name="TypeCharge"  onclick="ShowConfirmationInput();"> Pre-Auth
                        <input type="Radio" tabindex="8" value="2" name="TypeCharge"  onclick="ShowConfirmationInput();"> Capture Pre-Auth
                       
                   </div>
                <div>
                      <input type="Text" tabindex="9" class="w-250" disabled  name="ConfirmationNum" value="Insert Approval Number">
                </div>
                <div>
                    
                   
			      
                </div>
            </section>

         <!-- 003 Card Details -->
         <h2>Card Details</h2>
         <section class="box-info">
                <div class="form-group">
                    <div>Credit Card Number*</div>
                    <div id="PaymentMethodCC">
                        <input type="Text" name="CCard_num1" maxlength="4" style="width: 60px;" tabindex="10" value="<%=CcCCard_number1%>" onkeyup="if(this.value.length==4){frmCard.CCard_num2.focus();}" />
                        <input type="Text" name="CCard_num2" maxlength="4" style="width: 60px;" tabindex="11" value="<%=CcCCard_number2%>" onkeyup="if(this.value.length==4){frmCard.CCard_num3.focus();}" />
                        <input type="Text" name="CCard_num3" maxlength="4" style="width: 60px;" tabindex="12" value="<%=CcCCard_number3%>" onkeyup="if(this.value.length==4){frmCard.CCard_num4.focus();}" />
                        <input type="Text" name="CCard_num4" maxlength="4" style="width: 60px;" tabindex="13" value="<%=CcCCard_number4%>" />
                    </div>
                </div>
                  <div class="form-group">
                    <div>Expiration Date:</div>
                    <div>
                        <%
				        sCharExpYY = "@"
				        sListExpYY = ""
				        If trim(sExpYY)<>"" Then
					        If sExpYY<year(date()) Then sListExpYY = sListExpYY & sExpYY & sCharExpYY
				        End if
				        For i = year(date()) To year(date())+10
					        sListExpYY = sListExpYY & i & sCharExpYY
				        Next
				        response.write PutComboDefault("ExpMonth"" tabindex=""14", "01@02@03@04@05@06@07@08@09@10@11@12@", "01@02@03@04@05@06@07@08@09@10@11@12@", "@", CcExpMM)
				        response.write " / "
				        response.write PutComboDefault("ExpYear"" tabindex=""15", sListExpYY, sListExpYY, sCharExpYY, "20" & CcExpYY)
                        %>
                    </div>
                </div>
                 <div class="form-group">
                    <div>Security Code (CVV) <% If bCVV2 then %>*<% End If %></div>
                    <div>
                        <input class="w-80" tabindex="16" type="Text" name="CVV2" value="<%=Cc_cui%>" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div>Customer Full Name</div>
                    <div>
                        <input type="Text" tabindex="17" class="w-250" name="UserName" value="" />
                    </div>
                </div>
                <div class="form-group">
                    <div>Order Number</div>
                    <div>
                        <input type="Text" tabindex="18" class="w-250" name="Order" value="<%=OrderNumber%>" />
                    </div>
                </div>
                  <div class="form-group">
                    <div>
                        <br />
                    </div>
                    <div>
                        <input type="checkbox" tabindex="19" name="isCheckCVV2" value="1">
                        Credit Card without CVV
                           
                    </div>
                </div>
               
                <div class="clearfix"></div>
            </section>

         <!-- 004 Card Details -->
         <h2>Transaction Details</h2>
        <section class="box-info">
                <div class="form-group">
                    <div>Transaction Type*:</div>
                    <div>
                        <%
				    if bRefund=False then
					    sWhere = "AND CreditCode<>0"
				    end if
				    sSQL="SELECT * FROM List.TransCreditType WHERE IsShow=1 " & sWhere & " ORDER BY ShowOrder asc"
				    set rsDataCreditType=oledbData.execute(sSQL)
				    if not rsDataCreditType.EOF then
					    PutRecordsetCombo rsDataCreditType, "CreditType", "onchange=""ShowPayments();"" class=""w-250"" dir=""ltr""", "TransCreditType_id", "Name", "", "false", "false"
				    end if
				    rsDataCreditType.close
				    Set rsDataCreditType = nothing
                        %>
                    </div>
                </div>
                <div class="form-group">
                    <div>RefTransID</div>
                    <div>
                        <input type="text" class="w-250" tabindex="20" id="RefTransID" name="RefTransID" disabled value="<%=RefTransID%>" />
                    </div>
                </div>
                <div class="form-group">
                    <div>Installments*</div>
                    <div>
                        <%
				sComboTmp = "1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23;24"
				PutComboDefault "Payments"" class=""w-80", sComboTmp, sComboTmp, ";", TransPayments
                        %>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

        <!-- 005 Echeck -->
        <div id="PaymentMethodECHECK" style="display: none;">
                <h2>Bank Account</h2>
                <section class="box-info">
                    <div class="form-group">
                        <div>Personal Number*</div>
                        <div>
                            <input name="PersonalNum" class="w-250" tabindex="21" type="Text" value="<%=PersonalNum%>" /></div>
                    </div>
                    <div class="form-group">
                        <div>Account Name* </div>
                        <div>
                            <input maxlength="50" class="w-250" name="accountName" tabindex="22" type="Text" value="<%=accountName%>" /></div>
                    </div>
                    <div class="form-group">
                        <div>Routing Number*</div>
                        <div>
                            <input name="routingNumber" class="w-250" tabindex="23" type="Text" value="<%=routingNumber%>" /></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div>Account Number*</div>
                        <div>
                            <input name="accountNumber" class="w-250" type="Text" value="<%=accountNumber%>" /></div>
                    </div>
                    <div class="form-group">
                        <div>Bank Account Type*</div>
                        <div>
                            <select name="bankAccountType" tabindex="24" class="w-250">
                                <option value="1">���� ���</option>
                                <option value="2">������</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div>Bank Name*</div>
                        <div>
                            <input name="BankName" class="w-250"  tabindex="25"  type="Text" value="<%=BankName%>" /></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div>Phone Bank Number*</div>
                        <div>
                            <input name="BankPhone" class="w-250"  tabindex="26" type="Text" value="<%=BankPhone%>" /></div>
                    </div>
                    <div class="form-group">
                        <div>Country of the Bank *</div>
                        <div>
                            <%
				                sSQL="SELECT Name, stateID FROM [List].[StateList] ORDER BY Name"
				                set rsData = oledbData.execute(sSQL)
				                if not rsData.EOF then
					                PutRecordsetComboDefault rsData, "BankState", " style=""width: 250px;"" class=""w-250", "stateID", "Name", "0", ""
					                rsData.movefirst
				                end if
                            %>
                        </div>
                    </div>
                    <div class="form-group">
                        <div>City Of the bank*</div>
                        <div>
                            <input name="BankCity" class="w-250" tabindex="27"  type="Text" value="<%=BankCity%>" /></div>
                    </div>
                    <div class="clearfix"></div>
                </section>

            </div>

        <!-- 006 Personal Detalis  -->
        <h2>Personal Detalis</h2>
        <section class="box-info">
                <div class="form-group">
                    <div>Cardholder Name*</div>
                    <div>
                        <input type="Text" name="Member" class="w-250" tabindex="28" value="<%=CcMember%>" />
                    </div>
                </div>
                <div class="form-group">
                    <div>Phone Number <% If bPhoneNumber then %> * <% End If %></div>
                    <div>
                        <input type="Text" class="w-250" tabindex="29" name="PhoneNumber" value="<%= CcPhoneNumber %>" />
                    </div>
                </div>
                <div class="form-group">
                    <div>Personal Number <% If bPersonalNumber then %> * <% End If %></div>
                    <div>
                        <input type="Text" class="w-250" tabindex="30" name="PersonalNumber" value="<%=CcPersonalNumber%>" />
                    </div>
                </div>
                <div class="form-group">
                    <div>Birth Date (dd/mm/yyyy)</div>
                    <div>
                        <input name="BirthDate" class="w-250" tabindex="31" type="Text" value="<%=BirthDate%>" />
                    </div>
                </div>
                <div class="form-group">
                    <div>Email Address <% If bEmail then %> * <% End If %></div>
                    <div>
                        <input type="Text" class="w-250" name="Email" tabindex="32" maxlength="80" value="<%= CcEmail %>" />
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

        <!-- 007 Billing Address  -->
        <h2>Billing Address</h2>
        <section class="box-info">
                <div class="form-group">
                    <div>Country <%If bIsBillingAddressMust Then%>*<%End if%></div>
                    <div>
                        <%
				sSQL="SELECT Name, CountryISOCode FROM [List].[CountryList] ORDER BY Name"
				set rsData = oledbData.execute(sSQL)
				if not rsData.EOF then
					PutRecordsetCombo rsData, "BillingCountry"" onChange=""checkRequiredState();", "class=""input3"" style=""width: 250px""", "CountryISOCode", "Name", "", "", BaCountryIso
				end if
				rsData.close
				Set rsData = nothing
                        %>
                    </div>
                </div>
                <div class="form-group">
                    <div>State <%If bIsBillingAddressMust Then%>*<%End if%></div>
                    <div>

                        <%
				sSQL="SELECT Name, StateISOCode FROM [List].[StateList] ORDER BY Name"
				set rsData = oledbData.execute(sSQL)
				if not rsData.EOF then
					PutRecordsetCombo rsData, "BillingState", "dir=""ltr"" class=""input3"" style=""width: 250px""", "StateISOCode", "Name", "", "", BaStateIso
				end if
				rsData.close
				Set rsData = nothing
                        %>
                    </div>

                </div>
                <div class="form-group">
                    <div>City <%If bIsBillingAddressMust Then%> * <%End if%></div>
                    <div>
                        <input id="city" name="BillingCity" class="w-250" tabindex="33" type="text" value="<%=BaCity%>" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div>ZipCode <%If bIsBillingAddressMust Then%> * <%End if%></div>
                    <div>
                        <input id="zipCode" class="w-250" name="BillingZipCode" tabindex="34" type="text" value="<%=BaZipCode%>" />
                    </div>
                </div>
                <div class="form-group">
                    <div>Address 1 <%If bIsBillingAddressMust Then%>*<%End if%></div>
                    <div>
                        <input id="address1" class="w-250" name="BillingAddress1"  tabindex="35" type="text" value="<%=BaAddress1%>" />
                    </div>
                </div>
                <div class="form-group">
                    <div>Address 2</div>
                    <div>
                        <input id="address2" class="w-250" name="BillingAddress2" tabindex="36" type="text" value="<%=BaAddress2%>" />
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

        <!-- 008 Transaction Detalis  -->
        <h2>Transaction Detalis</h2>
        <section class="box-info">
                <div class="form-group">
                    <div>Amount*</div>
                    <div>
                        <input type="Text" class="w-250" tabindex="37" name="Amount" value="<%=TransAmount%>" />

                    </div>
                </div>
                <div class="form-group">
                    <div>Currency</div>
                    <div>
                        <select name="Currency" tabindex="38" class="w-80">
                            <%
					    For i = 0 to MAX_CURRENCY
						    If TransCurrency = i Then Response.Write("<option value=""" & i & """ selected=""selected"">" & GetCurText(i)) _
						    Else Response.Write("<option value=""" & i & """>" & GetCurText(i))
					    Next
                            %>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div>
                    <div>Comment</div>
                    <div>
                        <input type="Text" tabindex="39" style="width: 100%;" name="Comment" value="" />
                    </div>
                </div>

            </section>

        <!-- 009 Recurring Detalis  -->
        <% If nRecurringSeries > 0 Then %>
        <h2>Recurring</h2>
        <section class="box-info">
                <div class="form-group">
                    <div>Recurring Charge Number</div>
                    <div>
                      
                        <input type="Text" class="w-250" tabindex="40" name="RecurringChargeNumber" value="<%=nRecurringChargeNumber%>" />
                    </div>
                </div>
                <div class="form-group">
                    <div>Recurring Series Number</div>
                    <div>
                        <input type="Text" class="w-250" tabindex="41" name="RecurringSeries" value="<%=nRecurringSeries%>" />
                       
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>
        <% End If %>



        <!-- 010 Fees  -->
        <h2>Fees</h2>
        <section class="box-info">
               
                <div class="pb-10">
                    <input type="radio" <%= sDisabled %> name="ApplyHandlingFee" tabindex="42" value="0"  checked="checked" />Without Handling Fee
                     <input type="radio" <%= sDisabled %> name="ApplyHandlingFee" tabindex="43" value="1"  />Add Handling Fee (<%= sHandlingFee %>)
                     <input type="radio" <%= sDisabled %> name="ApplyHandlingFee" tabindex="44" value="2"   />Add HandlingFee (<%= sHandlingFee %>) Only for Approved transaction
                </div>
                <hr />
                <div>
                    <%
						nHandlingFee=ExecScalar("SELECT IsNull(HandlingFee, 0) FROM Setting.SetMerchantSettlement WHERE Currency_ID=0 And Merchant_ID=" & sCompanyID, 0)
						sHandlingFee = GetCurText(0) & FormatNumber(nHandlingFee, 2, true, false, true)
						if nHandlingFee=0 then
							sDisabled=" disabled=""disabled"" "
						else
							sDisabled=""
						end if
                %>
            
            <input type="checkbox" value="1" tabindex="45" name="UseTestServer" />
             Use Testing Server
                
                </div>
                <div class="wrap-button">
                    <input type="Button" value="Charge" tabindex="46" onclick="if(CheckForm()){document.frmCard.Action.value='charge';document.frmCard.action='charge_formAction1.asp';document.frmCard.submit();}else{return false;};" />&nbsp;&nbsp;
                </div>
            </section>

        </form>
        </article>
        <!-- 011 Fees Netpay  -->
        <h2>Admin Charge/Fees</h2>
        <section class="box-info-color">
            <form method="post" action="charge_form.asp" id="frmAdminCharge" name="frmAdminCharge">
                <input type="hidden" name="compname" value="<%= sCompanyName %>" />
                <input type="hidden" name="Customer" value="<%= sCustomer %>" />
                <input type="hidden" name="CompanyID" value="<%= sCompanyID %>" />
                <input type="hidden" name="PaymentMethod" value="2" />
                <input type="hidden" name="PaymentMethodOld" value="4" />
                <input type="hidden" name="PaymentMethodDisplay" value="---" />
                <input name="CompanyNum" type="hidden" value="<%= nCustomerNumber %>" />
                <input type="hidden" name="Action" value="" />
                <script language="javascript" type="text/javascript">
                    function SetAdminChargeFormVisibility()
                    {
                        var bFee=frmAdminCharge.HandlingFee[1].checked;
                        frmAdminCharge.Comment.value="";
                        if (bFee) frmAdminCharge.CreditType[0].checked=true;
                        frmAdminCharge.PaymentMethod.value=(bFee ? <%=PMD_ManualFee%> : <%=PMD_Admin %>);
                        frmAdminCharge.PaymentMethodOld.value=(bFee ? 5 : 4);
                        frmAdminCharge.PaymentMethodDisplay.value= (bFee ?"Manual Fee":"---");
                        for (var i=0;i<frmAdminCharge.CreditType.length;i++) frmAdminCharge.CreditType[i].disabled=bFee;
                    }
                </script>
               <div class="pb-10">Transaction Type:</div>
                    <div>
                        <input type="Radio" name="HandlingFee" value="0"  checked="checked" onclick="SetAdminChargeFormVisibility();" />
                        Admin Transaction
                   
                        <input type="Radio" name="HandlingFee" value="1"  onclick="SetAdminChargeFormVisibility();" />
                        Transaction Fee
                    </div>
                <hr />
                <div class="form-group">
                    <div>Amount</div>
                    <div>
                        <input type="Text" class="w-250" onkeydown="return IsKeyDigit();" name="Amount" value="" /></div>
                </div>
                <div class="form-group">
                   <div>Currency</div>
                    <div>
                        <select name="Currency" class="w-80">
                            <%
					For i = 0 to MAX_CURRENCY
						if TransCurrency = i Then Response.Write("<option value=""" & i & """ selected=""selected"">" & GetCurText(i)) _
						Else Response.Write("<option value=""" & i & """>" & GetCurText(i))
					Next
                            %>
                        </select>

                    </div>
                </div>
                <div>Credit Type:</div>
                <div>
                    <input type="Radio" checked="checked" value="0" name="CreditType" />
                    Credit (deduct money)
                    &nbsp;
                    <input type="Radio" value="1" name="CreditType" />Debit (add money)
                    &nbsp;
                    <input type="Radio" value="-1" name="CreditType" />Both

                </div>
              <div class="clearfix"></div>
                <div class="pb-10">
                    Comment
                     <input type="Text" name="Comment" style="width: 100%;" maxlength="200" value="" />
                </div>
                <% if nHandlingFee>0 then %>
                <script language="javascript" type="text/javascript">
                    function SetAdminChargeFormHandlingFee()
                    {
                        frmAdminCharge.HandlingFee[1].checked=true;
                        frmAdminCharge.Currency.selectedIndex=0;
                        frmAdminCharge.Amount.value="<%= FormatNumber(nHandlingFee, 2, true, false, true) %>";
                        SetAdminChargeFormVisibility();
                        frmAdminCharge.Comment.value="Handling Fee - ";
                        frmAdminCharge.Comment.focus();
                    }
                </script>
          
                [<a  onclick="SetAdminChargeFormHandlingFee();return false;">Handling Fee...</a>]
                
                <%
					end if
                %>
                [<a onclick="frmAdminCharge.Comment.value='Deduction Of 10% Rolling Reserve Funds for the period of processing transactions until �';">Deduction Of 10% ...</a>]
                [<a onclick="frmAdminCharge.Comment.value='Wire Transfer fee �';">Wire Transfer ...</a>]
   
    <div class="wrap-button">
        <input type="Button" value="Submit Transaction" onclick="if(CheckFormInter()){document.frmAdminCharge.Action.value='Submit Transaction';for (var i=0;i<frmAdminCharge.CreditType.length;i++) frmAdminCharge.CreditType[i].disabled=false;document.frmAdminCharge.action='charge_formAction3.asp';document.frmAdminCharge.submit();}else{return false;};" />&nbsp;&nbsp;
    </div>
    </form>
            
             
            </section>

        <!-- 012 Manage  -->
        <h2>Manage Balance</h2>
        <section class="box-info-color">
            <form method="post" action="charge_form.asp" name="frmBalance">
                <input type="hidden" name="compname" value="<%= sCompanyName %>" />
                <input type="hidden" name="Customer" value="<%= sCustomer %>" />
                <input type="hidden" name="CompanyID" value="<%= sCompanyID %>" />
                <input name="CompanyNum" type="hidden" value="<%= nCustomerNumber %>" />
                <input type="hidden" name="Action" value="" />
                <div class="mb-10">
                    <input type="Radio" checked="checked" value="0" name="CreditType" />
                    Credit (deduct money) &nbsp;
                            <input type="Radio" value="1" name="CreditType" />
                    Debit (add money)
                </div>
                <hr />
                <div class="form-group">
                    <div>Amount</div>
                    <div>
                        <input type="Text" onkeydown="return IsKeyDigit();" class="w-250" name="Amount" value="" /></div>
                </div>
                <div class="form-group">
                    <div>Currency</div>
                    <div>
                        <select name="Currency" class="w-80">
                            <%
					    For i = 0 to MAX_CURRENCY
						    if TransCurrency = i Then Response.Write("<option value=""" & i & """ selected=""selected"">" & GetCurText(i)) _
						    Else Response.Write("<option value=""" & i & """>" & GetCurText(i))
					    Next
                            %>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div>
                    <div>Comment</div>
                    <div>
                        <input type="Text" name="Comment" style="width: 100%;" maxlength="100" value="" /></div>
                </div>
                <div class="clearfix"></div>
                <div class="wrap-button">
                    <input type="Button" value="Submit Transaction" onclick="if(vm3.validate()){document.frmBalance.Action.value='Submit Transaction';document.frmBalance.action='charge_formAction4.asp';document.frmBalance.submit();}else{return false;};" />&nbsp;&nbsp;
                </div>
            </form>
        </section>
    </div>
    <script type="text/javascript">
        document.frmCard.Payments.disabled=true;
    </script>
</body>
</html>
<%call closeConnection()%>
