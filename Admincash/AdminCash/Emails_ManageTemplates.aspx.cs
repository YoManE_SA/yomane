﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

public partial class Emails_ManageTemplates : System.Web.UI.Page
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		Security.CheckPermission(lblPermissions);
	}

    protected void Page_Load(object sender, EventArgs e)
	{
		if (IsPostBack) return;
		rptSignatureList.DataSource = Netpay.Emails.MailBox.Load();
		rptSignatureList.DataBind();

		rptTemplateList.DataSource = Netpay.Emails.Templates.GetTemplates(WebUtils.DomainHost);
		rptTemplateList.DataBind();
	}

	protected void LoadTemplateClick(object sender, CommandEventArgs e)
	{
		txtFileName.Text = (string) e.CommandArgument;
		hf_originalFileName.Value = txtFileName.Text;
		ifrmBody.Attributes.Add("src", string.Format("Emails_TemplateBody.aspx?template={0}", txtFileName.Text));
	}

	protected void LoadSignatureClick(object sender, CommandEventArgs e)
	{
		txtFileName.Text = "";
		hf_originalFileName.Value = "User:" + (string)e.CommandArgument;
		ifrmBody.Attributes.Add("src", string.Format("Emails_TemplateBody.aspx?Signature={0}", (string)e.CommandArgument));
	}
	
	protected void SaveClick(object sender, EventArgs e)
	{
		string orgFileName = hf_originalFileName.Value;
		if (orgFileName.StartsWith("User:")) {
			var mailbox = Netpay.Emails.MailBox.Load(orgFileName.Substring(5).ToInt32(0));
			mailbox.Signature = hf_txtBody.Value;
			mailbox.Save();
		} else {
			Netpay.Emails.Templates.SaveTemplete(WebUtils.DomainHost, Replace_sp(txtFileName.Text.Trim()), hf_txtBody.Value);
		}
	}

	protected void DeleteClick(object sender, EventArgs e)
	{
		Netpay.Emails.Templates.DeleteTemplate(WebUtils.DomainHost, txtFileName.Text.Trim());
		txtFileName.Text = "";
		ifrmBody.Attributes["src"] = string.Format("Emails_TemplateBody.aspx?template={0}", txtFileName.Text);
	}

	string Replace_sp(string p)
	{
		System.Text.StringBuilder b = new System.Text.StringBuilder(p);
		b.Replace("  ", string.Empty);
		b.Replace(Environment.NewLine, string.Empty);
		b.Replace("\t", string.Empty);
		b.Replace(":", "_");
		b.Replace("\\", "_");
		b.Replace("/", "_");
		b.Replace("?", "_");
		b.Replace("'", "_");
		b.Replace("<", "_");
		b.Replace(">", "_");
		b.Replace("|", "_");
		return b.ToString();
	}
}
