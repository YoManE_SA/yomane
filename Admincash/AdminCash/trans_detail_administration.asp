<%
PendingFieldName = IIF(sTransTableName = "tblCompanyTransPass", "TransPass_id", "TransFail_id")
If(Request("NotifyMerchant") = "1") Then
    oledbData.Execute "Insert Into EventPending (" & PendingFieldName & ", EventPendingType_id, TryCount)Values(" & sTransID & "," & PET_InfoEmailSendMerchant & ",3)"
ElseIf(Request("NotifyClient") = "1") Then
    oledbData.Execute "Insert Into EventPending (" & PendingFieldName & ", EventPendingType_id, TryCount)Values(" & sTransID & "," & PET_InfoEmailSendClient & ",3)"
End If
If sIsAllowAdmin Then
	'-----------------------------------------------------------------------
	'	Show denied options
	'-----------------------------------------------------------------------
	sIsShowDeniedOptions = false
	If sTransTableName = "tblCompanyTransPass" Then
		If (rsData("IsChargeback") and rsData("OriginalTransID")=0) or rsData("deniedstatus")=DNS_SetFoundValid Or rsData("deniedstatus")=DNS_SetFoundValidRefunded Or rsData("deniedstatus")=DNS_WasWorkedOut or rsData("deniedstatus")=DNS_DupWasWorkedOut or rsData("deniedstatus")=DNS_DupWasWorkedOutRefunded then sIsShowDeniedOptions = true
	End if

	If sIsShowDeniedOptions Then

		If sPaymentMethod="1" Then
			sPageUrl = "trans_detail_cc.asp"
		ElseIf sPaymentMethod="2" Then
			sPageUrl = "trans_detail_eCheck.asp"
		End if

		bSendClarify = False
		sSQL="SELECT CompanyName, Mail, languagePreference FROM tblCompany WHERE id = " & rsData("companyID")
		set rsData3=oledbData.execute(sSQL)
		If not rsData3.EOF Then
			If CDate(rsData("DeniedSendDate"))>"01/01/2000  00:00" Then
				sMailDateText = "&nbsp;(" & FormatDatesTimes(rsData("DeniedSendDate"),1,0,0) & ")"
			End if
			If CDate(rsData("DeniedPrintDate"))>"01/01/2000  00:00" Then
				sPrintDateText = "&nbsp;(" & FormatDatesTimes(rsData("DeniedPrintDate"),1,0,0) & ")"
			End if
			If trim(rsData3("languagePreference"))="heb" Then
				sLanguage = "Hebrew"
			Elseif trim(rsData3("languagePreference"))="eng" Then
				sLanguage = "English"
			End if
			If rsData("deniedstatus") = DNS_SetFoundValid Or rsData("deniedstatus") = DNS_SetFoundValidRefunded Or rsData("deniedstatus") = DNS_DupFoundValid Or rsData("deniedstatus") = DNS_DupWasWorkedOut Then bSendClarify = True
			%>
			<tr>
				<td valign="top" style="height:22px; <%=sBtnBarStyle%>">
					<form action="<%= sPageUrl %>" name="frmUpdDeniedComment" method="post">
					<input type="Hidden" name="Action" value="updDeniedComment">
					<input type="Hidden" name="transID" value="<%= sTransID %>">
					<input type="Hidden" name="bkgColor" value="<%= sPageBkgColor %>">
					<input type="Hidden" name="isAllowAdministration" value="<%= sIsAllowAdmin %>">
					<input type="Hidden" name="PaymentMethod" value="<%= sPaymentMethod %>">
					<input type="Hidden" name="TransTableType" value="<%= sTransTableType %>">
					<table cellpadding="1" cellspacing="0" border="0" style="width:100%;">
					<tr>
						<td style="white-space:nowrap; padding-left:12px;">
							<span style="font-weight:bold; color:#505050;">CHB:</span> &nbsp;&nbsp;
							<span class="SmallHeadings">LANG:</span> <%= sLanguage %> &nbsp;&nbsp;
							<span class="SmallHeadings">MESSAGE:</span>
								<a onclick="OpenPop('System_chbMailSend.aspx?transID=<%= sTransID %>&Clarify=<%=IIF(bSendClarify, 1, 0)%>','docPrint',400,300,0);" class="faq">Email</a><%= sMailDateText %> &nbsp;|&nbsp;
								<a onclick="OpenPop('System_chbMailSend.aspx?preview=1&transID=<%= sTransID %>&Clarify=<%=IIF(bSendClarify, 1, 0)%>','docPrint',660,400,1);" class="faq">Print</a><%= sPrintDateText %> &nbsp;&nbsp;
							<span class="SmallHeadings">COMMENT:</span> 
						</td>
						<td style="white-space:nowrap; width:100%;">				        
							<input type="Text" name="deniedComment" value="<%= rsData("DeniedAdminComment") %>" class="inputData" style="width:100%">
						</td>
						<td style="white-space:nowrap; padding-right:12px;">
							 &nbsp;&nbsp;
							<span class="SmallHeadings">DATE:</span> <input type="Text" name="deniedDate" value="<%= Left(rsData("deniedDate"), 10) %>" size="10" class="inputData"> &nbsp;&nbsp;
							<input type="submit" value="UPDATE" class="button1" style="font-size:10px;">
						</td>
					</tr>
					</table>
					</form>
				</td>
			</tr>
			<tr><td height="12"></td></tr>
			<%
		End if
		rsData3.close
		set rsData3 = nothing
	End If

	'-----------------------------------------------------------------------
	'	Show refund and denied buttons
	'-----------------------------------------------------------------------
	If int(rsData("CreditType"))=0 Then
		sIsShowRefund = false
	Elseif rsData("isTestOnly") and rsData("CompanyID")<>35 Then
		sIsShowRefund = false
	Else
		sIsShowRefund = true
	End if
	If sIsShowRefund Then
		sAdminBtn = sAdminBtn & "<a style=""cursor:pointer;"" onclick=""OpenPop('Trans_RefundRequest.aspx?ID=" & rsData("ID") & "&Merchant=" & rsData("CompanyID") & "&Currency=" & rsData("Currency") & "','fraRefundAsk',600,300,0);""><img src=""../images/btn_AskTransRefundEng.gif"" align=""middle"" border=""0""></a>"
	Else
		sAdminBtn = sAdminBtn & "<img src=""../images/btn_AskTransRefundEngOff.gif"" align=""middle"" border=""0"">"
	End If

	'-----------------------------------------------------------------------
	'	Show denied buttons
	'-----------------------------------------------------------------------
	Dim sIsShowClarify, sClrfButton, sClrfStatus
	'If int(rsData("CreditType"))=8 Then 
	'	' �� ���� ��������
	'	if rsData("deniedstatus")=0 then
	'		'���� ������
	'		sIsShowDenied = true
	'		sIsShowClarify = true
	'		sClrfButton = "btn_SetClrfEng"
	'		sDeniedButton = "btn_SetDenialEng"
	'		sDeniedStatus = IIF(rsData("PrimaryPayedID") > 0, 2, 1)
	'		sClrfStatus = 3
	'	ElseIf rsData("deniedstatus") = DNS_UnSetInVar Or rsData("deniedstatus") = DNS_SetInVar then
	'		sIsShowDenied = true
	'		sIsShowClarify = false
	'		sDeniedButton = "btn_ClearDenialEng"
	'		sDeniedStatus = 0
	'	end if
	'Else

	If int(rsData("CreditType"))<>0 Then
		If rsData("deniedstatus") = 0 And int(rsData("CreditType"))=8 Then
			sIsShowDenied = true
			sIsShowClarify = true
			sClrfButton = "btn_SetClrfEng"
			sDeniedButton = "btn_SetDenialEng"
			sDeniedStatus = IIF(rsData("PrimaryPayedID") > 0, 2, 1)
			sClrfStatus = 3
		ElseIf rsData("deniedstatus") = 0 AND rsData("payID")<>";0;" then
			'���� �����
			sIsShowDenied = true
			sIsShowClarify = true
			sDeniedButton = "btn_SetDenialEng"
			sClrfButton = "btn_SetClrfEng"
			sDeniedStatus = 2
			sClrfStatus = 3
		ElseIf rsData("deniedstatus") = 0 then
			'���� ������
			sIsShowDenied = true
			sIsShowClarify = true
			sDeniedButton = "btn_SetDenialEng"
			sClrfButton = "btn_SetClrfEng"
			sDeniedStatus = 1
			sClrfStatus = 3
		ElseIf rsData("deniedstatus") = DNS_UnSetInVar then
			sIsShowDenied = true
			sDeniedButton = "btn_ClearDenialEng"
			sDeniedStatus = 0
		ElseIf rsData("deniedstatus") = DNS_SetInVar then
			sIsShowDenied = true
			sDeniedButton = "btn_ClearDenialEng"
			sDeniedStatus = 0
		ElseIf rsData("deniedstatus") = DNS_SetFoundValid Or rsData("deniedstatus") = DNS_SetFoundValidRefunded then
			sDeniedButton = "btn_SetDenialEng"
			sDeniedStatus = 1
			sIsShowDenied = true
			sIsShowClarify = true
			sClrfButton = "btn_ClearClrfEng"
			sClrfStatus = 0
		ElseIf rsData("deniedstatus") = DNS_DupFoundValid then
			sIsShowClarify = true
			sClrfButton = "btn_ClearClrfEng"
			sClrfStatus = 0
		ElseIf rsData("deniedstatus") = DNS_DupWasWorkedOut then
			sIsShowDenied = true
			sDeniedButton = "btn_SetDenialEng"
			sDeniedStatus = 2
		End if
	ElseIf rsData("deniedstatus") = DNS_DupFoundUnValid And rsData("payID") = ";0;" then
		sIsShowDenied = true
		sDeniedButton = "btn_ClearDenialEng"
		sDeniedStatus = 0
	Else
		sIsShowDenied = false
	End if
%>
<script language="javascript" type="text/javascript">
	function SetChargebackStatus(nNewStatus)
	{
		var sReasonCode = "", nReasonCode=0;
		if (nNewStatus != 0)
		{
			var ReasonCodes = new Array();
			<%
				sSQL = "SELECT ReasonCode, Brand, Title FROM tblChargebackReason ORDER BY ReasonCode, Brand"
				Set rsReasons = oledbData.Execute(sSQL)
				Do
					Response.Write "ReasonCodes[" & rsReasons("ReasonCode") & "]=""" & rsReasons("Brand") & ": " & Replace(rsReasons("Title"), """", "\""") & """;" & chr(13)
					rsReasons.MoveNext
				Loop Until rsReasons.EOF
				rsReasons.Close
				Set rsReasons = Nothing
			%>
			nReasonCode = NaN;
			while (isNaN(nReasonCode))
			{
				sReasonCode = prompt("Please type in the reason code:", "");
				if (sReasonCode == null) return;
				nReasonCode = parseInt(sReasonCode);
				if (!confirm(sReasonCode + " " + ReasonCodes[nReasonCode])) return;
			}
		}
		location.replace("<%= sPageUrl %>?Action=updDeniedStatus&status=" + nNewStatus + "&ReasonCode=" + nReasonCode + "&<%= sQuery %>");
	}
</script>
<%
	If sIsShowDenied Then
		sAdminBtn = sAdminBtn & "&nbsp;|&nbsp; <a onclick=""SetChargebackStatus(" & sDeniedStatus & ");return false;"" href=""#""><img src=""../images/" & sDeniedButton & ".gif"" align=""middle"" border=""0""></a>"
	End If

	If sIsShowClarify Then
		sAdminBtn = sAdminBtn & "&nbsp;|&nbsp; <a onclick=""SetChargebackStatus(" & sClrfStatus & ");return false;"" href=""#""><img src=""../images/" & sClrfButton & ".gif"" align=""middle"" border=""0""></a>"
	End If



    Dim fraudStatus : fraudStatus = rsData("IsFraudByAcquirer")
    If fraudStatus Then
        sAdminBtn = sAdminBtn & "&nbsp;&nbsp;| &nbsp;<a style=""cursor:pointer;"" onclick=""OpenPop('Trans_SetFraud.aspx?transactionId=" & rsData("ID") & "&merchantId=" & rsData("CompanyID") & "&Currency=" & rsData("Currency") & "','fraSetFraud',600,300,0);""><img src=""../images/btn_SetFraud_off.gif"" align=""middle"" border=""0""></a>"
    Else
       sAdminBtn = sAdminBtn & "&nbsp;&nbsp;| &nbsp;<a style=""cursor:pointer;"" onclick=""OpenPop('Trans_SetFraud.aspx?transactionId=" & rsData("ID") & "&merchantId=" & rsData("CompanyID") & "&Currency=" & rsData("Currency") & "','fraSetFraud',600,300,0);""><img src=""../images/btn_SetFraud_on.gif"" align=""middle"" border=""0""></a>"
    End If



	'sAdminBtn = sAdminBtn & "&nbsp;|&nbsp; <a href=""?" & UrlWithout(Request.QueryString, "SetFraud") & "&SetFraud=" & IIF(fraudStatus, "0", "1") & """><img src=""../images/btn_SetFraud_" & IIF(fraudStatus, "off", "on") & ".gif"" align=""middle"" border=""0""></a>"

End if

'-----------------------------------------------------------------------
'	Show block card buttons
'-----------------------------------------------------------------------
If sPaymentMethod="1" AND isCardDetails Then
    If sAdminBtn <> "" Then sAdminBtn = sAdminBtn & "&nbsp;|&nbsp; "
	If trim(request("Action"))="Block" Then
		sAdminBtn = sAdminBtn & "<span class=""txt11"">(Card is blocked)</span>"
	Else
		sSQL="SELECT fcbl_BlockLevel FROM tblFraudCcBlackList WHERE" & _
		" fcbl_ccNumber256 = dbo.GetEncrypted256('" & Replace(rsData2("CCard_number")," ","") & "')" &_
		" AND (fcbl_BlockLevel=1 Or (fcbl_BlockLevel=0 And company_id=" & rsData("CompanyID") & "))"
		set rsDataTmp = oledbData.execute(sSQL)
		If rsDataTmp.EOF Then
			sAdminBtn = sAdminBtn & "<a href=""#"" onclick=""AddToBlacklist('" & Replace(IIF(IsNull(rsData2("CCard_number")), "", rsData2("CCard_number")), " ", "") & "', " & rsData2("ExpMM") & ", " & rsData2("ExpYY") & ");return false;""><img src=""../images/btn_BlockCcEng.gif"" align=""middle"" border=""0""></a>"
		Else
			sAdminBtn = sAdminBtn & "<span class=""txt11"">(CC blacklist - " & IIf(rsDataTmp("fcbl_BlockLevel")=0,"Merchant","System") & ")</span>"
		End if
		rsDataTmp.close
		set rsDataTmp = nothing
	End if
End if

If rsData("PaymentMethod") >= PMD_CC_MIN And rsData("PaymentMethod") <= PMD_CC_MAX Then
	sAdminBtn = sAdminBtn & "&nbsp;|&nbsp; "
	sAdminBtn = sAdminBtn & "<a href=""#"" onClick=""OpenPop('BLCommonBlockAdd.aspx?TableType=" & sTransTableType & "&TransID=" &  rsData("id") & "&CCID=" & rsData("PaymentMethodid") & "','CommonBlockAdd',500,300,1);""><img src=""../images/btn_BlockItemEng.gif"" align=""middle"" border=""0""></a>"
	sAdminBtn = sAdminBtn & "&nbsp;|&nbsp; "
	sAdminBtn = sAdminBtn & "<a href=""#"" onClick=""OpenPop('Whitelist.aspx?CCID=" & rsData("PaymentMethodID") & "&FilterMerchantDefaultID=" & rsData("CompanyID") & "','CommonBlockAdd',1000,300,1);""><img src=""../images/btn_WhitelistCCEng.gif"" align=""middle"" border=""0""></a>"
End if

'Email confitmation button
If sTransTableName = "tblCompanyTransPass" Or sTransTableName = "tblCompanyTransFail" Then
    isPassNotifyMerchant = ExecScalar("Select PassNotifyEmail From tblCompany Where ID =" & rsData("CompanyID"), "") <> ""
    If isPassNotifyMerchant Then
	    sAdminBtn = sAdminBtn & "&nbsp;|&nbsp;Email Merchant: <a class=""link"" href=""?" & UrlWithout(Request.QueryString, "NotifyMerchant") & "&NotifyMerchant=1""><img src=""/NPCommon/images/icon_sendMail.png"" align=""middle"" alt=""Send transaction notificatin to merchant"" /></a>"
    Else
	    sAdminBtn = sAdminBtn & "&nbsp;|&nbsp;Email Merchant: <img src=""/NPCommon/images/icon_sendMailDim.png"" align=""middle"" alt=""To send transaction notificatin to merchant, First need to set an address for notifications in merchant settings."" />"
    End If
End if

If sIsAllowAdmin And sTransTableName = "tblCompanyTransPass" Then
	If ClientEmailAddress <> "" Then
		sAdminBtn = sAdminBtn & "&nbsp;|&nbsp; Email Client: <a class=""link"" href=""?" & UrlWithout(Request.QueryString, "NotifyClient") & "&NotifyClient=1""><img src=""/NPCommon/images/icon_sendMail.png"" align=""middle"" alt=""Send transaction notificatin to client"" /></a>"
	Else
		sAdminBtn = sAdminBtn & "&nbsp;|&nbsp; Email Client: <img src=""/NPCommon/images/icon_sendMailDim.png"" align=""middle"" alt=""To send transaction notificatin to client, no address apear in the transaction."" />"
	End If
End if

If PageSecurity.IsMemberOf("Dev. Support") Then

	Dim sWhereField
	Select Case sTransTableType
		Case "preauth": sWhereField = "TransApprovalID"
		Case "fail": sWhereField = "TransFailID"
		Case "pass": sWhereField = "TransPassID"
		Case "pending": sWhereField = "TransPendingID"
	End Select
	Set rsEvents = oleDbData.Execute("SELECT a.*, t.Name FROM tblTransactionAmount a LEFT JOIN List.TransAmountType t ON a.TypeID=t.TransAmountType_id WHERE " & sWhereField & "=" & sTransID & " ORDER BY a.ID")
	If NOT rsEvents.EOF Then
	%>
	<tr>
		<td>
			<span class="MainHeadings">AMOUNTS</span><br />
			<table cellpadding="2" cellspacing="0" class="subTable">
			<tr>
				<th>ID</th>
				<th>Date</th>
				<th>Name</th>
				<th>Amount</th>
				<th>Settle Amount</th>
				<th>Settlement</th>
				<th>SettleDate</th>
			</tr>
			<%
			Do While Not rsEvents.EOF
				Response.Write("<tr>")
				Response.Write("<td>" & rsEvents("ID") & "</td>")
				Response.Write("<td>" & rsEvents("InsertDate") & "</td>")
				Response.Write("<td>" & rsEvents("Name") & "</td>")
				Response.Write("<td>" & FormatCurr(rsEvents("Currency"), rsEvents("Amount")) & "</td>")
				Response.Write("<td>" & FormatCurr(rsEvents("Currency"), rsEvents("SettledAmount")) & "</td>") 'IIF(rsEvents("SettledCurrency") Is Nothing, 255, rsEvents("SettledCurrency"))
				Response.Write("<td>" & rsEvents("SettlementID") & "</td>")
				Response.Write("<td>" & rsEvents("SettlementDate") & "</td>")
				Response.Write("</tr>")
				rsEvents.MoveNext
			Loop
			%>
			</table>
		</td>
	</tr>
	<%
	rsEvents.Close()
	End if

If session("Identity") = "Local" Then

	'Dim sWhereField
	Select Case sTransTableType
		Case "preauth": sWhereField = "TransPreAuth_id"
		Case "fail": sWhereField = "TransFail_id"
		Case "pass": sWhereField = "TransPass_id"
		'Case "pending": sWhereField = "TransPendingID"
	End Select
	Set rsEvents = oleDbData.Execute("SELECT a.*, t.Name as atName, st.Name as stName" & _
		" FROM Trans.TransactionAmount a" & _
		" LEFT JOIN List.AmountType t ON a.AmountType_id=t.AmountType_id" & _
		" LEFT JOIN List.SettlemenType st ON a.SettlementType_id=st.SettlementType_id" & _
		" WHERE " & sWhereField & "=" & sTransID & " ORDER BY a.TransactionAmount_id")
	If NOT rsEvents.EOF Then
	%>
	<tr>
		<td>
			<span class="MainHeadings">AMOUNTS2</span><br />
			<table cellpadding="2" cellspacing="0" class="subTable">
			<tr>
				<th>ID</th>
				<th>Date</th>
				<th>SettlementType</th>
				<th>Amount Type</th>
				<th>Installmetns</th>

				<th>Percent</th>
				<th>FixedAmount</th>
				<th>PercentAmount</th>
				<th>Total</th>

				<th>Settlement</th>
				<th>SettleDate</th>
			</tr>
			<%
			Do While Not rsEvents.EOF
				curId = GetCurrIDByIso(rsEvents("CurrencyISOCode"))
				Response.Write("<tr>")
				Response.Write("<td>" & rsEvents("TransactionAmount_id") & "</td>")
				Response.Write("<td>" & rsEvents("InsertDate") & "</td>")
				Response.Write("<td>" & rsEvents("stName") & "</td>")
				Response.Write("<td>" & rsEvents("atName") & IIF(rsEvents("IsFee"), " Fee", "") & "</td>")
				Response.Write("<td>" & rsEvents("Installment") & "</td>")
				Response.Write("<td>" & rsEvents("PercentValue") & "%</td>")
				Response.Write("<td>" & FormatCurr(curId, TestNumVar(rsEvents("FixedAmount"), 0, -1, 0)) & "</td>")
				Response.Write("<td>" & FormatCurr(curId, TestNumVar(rsEvents("PercentAmount"), 0, -1, 0)) & "</td>")
				Response.Write("<td>" & FormatCurr(curId, TestNumVar(rsEvents("Total"), 0, -1, 0)) & "</td>")
				Response.Write("<td>" & rsEvents("Settlement_id") & "</td>")
				Response.Write("<td>" & rsEvents("SettlementDate") & "</td>")
				Response.Write("</tr>")
				rsEvents.MoveNext
			Loop
			%>
			</table>
			<br />
		</td>
	</tr>
	<%
	rsEvents.Close()
	End if
End if
            
	Select Case sTransTableType
		Case "preauth": WhereString = "TransPreAuth_id"
		Case "fail": WhereString = "TransFail_id"
		Case "pass": WhereString = "TransPass_id"
		Case "pending": WhereString = "TransPending_id"
	End Select
	Set rsEvents = oleDbData.Execute("SELECT th.IsSucceeded, ISNULL(CAST(th.ReferenceNumber AS VARCHAR(20)), '---') AS 'ReferenceNumber' , " & _
		"CONVERT(VARCHAR(10), th.InsertDate, 103) + ' ' + CONVERT(VARCHAR(8), th.InsertDate, 114) AS 'InsertDate', " & _
		"th.Description, ISNULL(th.ReferenceUrl, '---') AS 'ReferenceUrl', tht.Name From Trans.TransHistory AS th " & _
		"LEFT JOIN List.TransHistoryType AS tht ON (th.TransHistoryType_id = tht.TransHistoryType_id) " & _
		"WHERE " & WhereString & "=" & sTransID & " ORDER BY th.TransHistory_id ASC")
	If NOT rsEvents.EOF Then
		%>
		<tr>
			<td>
				<br />
				<span class="MainHeadings">HISTORY</span><br />
				<table cellpadding="0" cellspacing="4" class="subTable">
				<tr>
					<th>Date</th>
					<th>Type</th>
					<th>Success</th>
					<th>Description</th>
					<th>Reference</th>
				</tr>
				<%
				Do While Not rsEvents.EOF
					Response.Write("<tr>")
						Response.Write("<td nowrap=""nowrap"">" & rsEvents("InsertDate") & "</td>")
						Response.Write("<td nowrap=""nowrap"">" & rsEvents("Name") & "</td>")
						Response.Write("<td nowrap=""nowrap"">" & IIf(rsEvents("IsSucceeded"), "Yes", "No") & "</td>")
						Response.Write("<td>" & IIF(Len(rsEvents("Description")) > 45, Left(rsEvents("Description"), 42) & " ...", rsEvents("Description")) & "</td>")
						Response.Write("<td nowrap=""nowrap"">" & rsEvents("ReferenceNumber"))
							If rsEvents("ReferenceUrl") <> "---" Then Response.Write("(<a href=""" & rsEvents("ReferenceUrl") & """>Click</a>)")
						Response.Write("</td>")
					Response.Write("</tr>")
					rsEvents.MoveNext
				Loop
				%>
				</table>
				<br />
			</td>
		</tr>
		<%
	rsEvents.Close()
	End if
End If
%>
<tr>
	<td style="height:22px; <%=sBtnBarStyle%>">
		<table width="98%" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td class="txt11"><%=sAdminBtn%></td>
			<td>&nbsp;&nbsp;</td>
			<td class="txt11" align="left">
				<span style="float:right;" id="pageMainHeadingTd"></span>
				<%
				Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, False)

				If sTransTableType <> "pending" Then
					If int(rsData("PaymentMethod_id")) = 4 Then
						sType="transAdmin"
					Else
						sType="trans"
					End if
					If sTransTableType <> "preauth" Then
						%>
						<a class="faq alwaysActive" href="javascript:parent.location.href='charge_form.asp?CompanyID=<%=rsData("CompanyID")%>&tblName=<%=sTransTableType%>&MerchantTransID=<%=Trim(rsData("id"))%>';">Virtual Terminal</a> <img src="../images/iconNewWinRight.gif" align="middle" border="0"> &nbsp;|&nbsp;
						<%
					End If
					If rsData("TransSource_id")=23 Then
						response.Write "Processed Manually &nbsp;|&nbsp;"
					Else
						%>
						<a class="faq alwaysActive" onClick="OpenPop('Log_ChargeAttempts.aspx?iTransNum=<%=rsData("id")%>&iTransTbl=<%=sTransTableType%>','AttmptLog',900,400,1);"> Log Charge Attempt</a> 
						<%
					End If
				End If
				If rsData("PaymentMethod") >= PMD_CC_MIN And rsData("PaymentMethod") <= PMD_CC_MAX Then
					If sTransTableType = "pass" Then
						'20110317 Tamir - Add link for payout clarification email to BNS
						'20110524 Tamir - allow manual marking of transactions as paid/refunded by bank (EPA)
						'20110525 Tamir - allow manual marking of transactions as NOT paid/refunded by bank (EPA)
						nDebitCompanyID = rsData("DebitCompanyID")
						dtInsert = rsData("InsertDate")
						If rsData("PrimaryPayedID") = 0 Then
							sActionType=IIf(rsData("CreditType") = 0, "Refunded", "Paid")
							sSQL = "SELECT ID FROM tblLogImportEPA WITH (NOLOCK) WHERE TransID=" & sTransID & " AND Installment=1 AND Is" & sActionType & "=1"
							nEpaID = ExecScalar(sSQL, 0)
							If nEpaID = 0 Then
								%>
									<br />
								<%
								If DateDiff("d", dtInsert, Now()) > 8 And (nDebitCompanyID=18 Or nDebitCompanyID=46) Then
									sUrl = "mailto:BS-Support-Netpay@bs-card-service.com?cc=limor@netpay-intl.com&subject=Transaction Missing in EPA Statements " & sTransID & "&Body="
									sUrl = sUrl & "Hello,"
									sUrl = sUrl & "%0A "
									sUrl = sUrl & "%0ACould you please check if the following transaction is already paid?"
									sUrl = sUrl & "%0AThe transaction is missing from EPA statements."
									sUrl = sUrl & "%0A "
									sUrl = sUrl & "%0ATransaction ID: " & sTransID
									sUrl = sUrl & "%0ACredit card: " & rsData2("CardNumberProtected")
									sUrl = sUrl & "%0AProcessing date: " & FormatDateTime(dtInsert, 2)
									If DatePart("h", dtInsert) >= 22 Then sUrl = sUrl & " or " & FormatDateTime(DateAdd("d", 1, dtInsert), 2)
									sUrl = sUrl & "%0AReference Code: " & rsData("DebitReferenceCode")
									sUrl = sUrl & "%0AAuthorization Code: " & rsData("ApprovalNumber")
									sUrl = sUrl & "%0ATerminal No.: " & rsData("TerminalNumber")
									sUrl = sUrl & "%0AContract No.: " & rsData("CN")
									sUrl = sUrl & "%0AAmount: " & IIf(rsData("CreditType") = 0, "-", "") & FormatNumber(rsData("Amount"), 2, True, False, True) & " " & GetCurIso(rsData("Currency"))
									sUrl = sUrl & "%0A "
									sUrl = sUrl & "%0ASincerely,"
									sUrl = sUrl & "%0A" & PageSecurity.FullName
									%>
										<a class="faq alwaysActive" href="<%= sUrl %>">Request payment proof</a>
									<%
								Else
									%>
										Not paid by bank
									<%
								End If
								sUrl = Request.ServerVariables("PATH_INFO") & "?MarkEPA=" & sActionType & "&" & Request.QueryString
								%>
									(<a class="faq" href="<%= sUrl %>">mark as paid</a>)
								<%
							Else
								%>
									<br />
									Paid by bank
								<%
								sSQL = "SELECT IsNull(AdminUser, '" & PageSecurity.Username & "') FROM tblLogImportEPA WITH (NOLOCK) WHERE TransID=" & sTransID & " AND Installment=1 AND Is" & sActionType & "=1"
								sAdminUser = ExecScalar(sSQL, "system")
								If sAdminUser = PageSecurity.Username Then
									'only the user that marked paid/refunded can undo it
									sUrl = Request.ServerVariables("PATH_INFO") & "?MarkEPA=Not" & sActionType & "&" & Request.QueryString
									%>
										(<a class="faq" href="<%= sUrl %>"><span style="text-transform:lowercase;">cancel</span></a>)
									<%
								Else
									Response.Write "(marked by " & sAdminUser & ")"
								End If
							End If
						End If
					End If
				End If
				%>  
			</td>
		</tr>
		</table>
	</td>
</tr>