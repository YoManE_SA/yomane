<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_security.asp" -->
<%
	'sFromDate = trim(Request("iFromDate"))
'	sToDate = trim(Request("iToDate"))
	nCurrency = TestNumVar(Request("Currency"), 0, MAX_CURRENCY, -1)
	sMerchantIDs = TestNumList(Trim(Request("MerchantIDs")))
	sWireIDs = TestNumList(Trim(Request("WireIDs")))
	sSearchText = DBText(Trim(Request("SearchText")))
	dDateMax= Trim(Request("toDate") & " " & Request("toTime"))
	dDateMin= Trim(Request("fromDate") & " " & Request("fromTime"))

	sWhere=""
	if dDateMax<>"" and IsDate(dDateMax) then sWhere=sWhere & " AND insertDate<='" & dDateMax & "'"
	if dDateMax<>"" and IsDate(dDateMin) then sWhere=sWhere & " AND insertDate>='" & dDateMin & "'"
	if nCurrency>-1 then sWhere=sWhere & " AND PayCurrency=" & nCurrency
	if sMerchantIDs<>"" then sWhere=sWhere & " AND logMasavFile_id IN (SELECT logMasavFile_id FROM tblLogMasavDetails WHERE Company_id IN (" & sMerchantIDs & "))"
	if sWireIDs<>"" then sWhere=sWhere & " AND logMasavFile_id IN (SELECT logMasavFile_id FROM tblLogMasavDetails WHERE WireMoney_id IN (" & sWireIDs & "))"
	if sSearchText<>"" then sWhere=sWhere & " AND logMasavFile_id IN (SELECT logMasavFile_id FROM tblLogMasavDetails WHERE PayeeName+PayeeBankDetails LIKE N'%" & sSearchText & "%')"
	sSQL="SELECT"
	if sWhere="" then sSQL=sSQL & " TOP 20"
	sSQL=sSQL & " logMasavFile_id, insertDate, PayedBankDesc, LogonUser, DoneFlag, Flag," & _
	" PayCurrency, PayAmount, PayCount, AttachedFileExt FROM tblLogMasavFile"
	if sWhere<>"" then sSQL=sSQL & " WHERE " & mid(sWhere, 5)
	sSQL=sSQL & " ORDER BY insertDate DESC"
	Set fso = Server.CreateObject("Scripting.FileSystemObject")
	set rsData = oledbData.execute(sSQL)
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<script src="../js/func_common.js" type="text/javascript"></script> 
	<script language="javascript">
		//-------------------------------------------------------------------------
		//	Ajax for getting log details
		//-------------------------------------------------------------------------
		function creat_Object() { 
			var xmlhttp;
			if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
				try {
					xmlhttp = new XMLHttpRequest();
				} 
				catch (e) {
					alert("Your browser is not supporting XMLHTTPRequest");
					xmlhttp = false;
				}
			}
			else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			return xmlhttp;
		}
		
		var request = creat_Object();
		var containerName
		function sever_interaction() {
			//var containerName = 'tdUpdPendingTransText';
			
			if(request.readyState == 1) {
				
				waitMsg = 'Loading data - Please wait ...<br />'
				objRef.innerHTML='';
				objRef.innerHTML = waitMsg;
			}

			if(request.readyState == 4) {
				if (request.status == 200) {
					var responseTxt = request.responseText;
					objRef.innerHTML='';
					objRef.innerHTML = responseTxt + '<br />';
				}
			}
		}
		
		function call_server(fLogMasavFile_id) {
			objRef = document.getElementById('Row' + fLogMasavFile_id);
			objRef2 = document.getElementById('trRow' + fLogMasavFile_id);
			if (objRef.innerHTML == '') {
				request.open("GET", "Log_MasavFile_details.asp?logMasavFile_id="+fLogMasavFile_id + "&x=" + new Date());
				request.onreadystatechange = sever_interaction;
				request.send(null);
				event.srcElement.src = "../images/tree_collapse.gif";
				objRef.style.borderTop = "1px dashed Silver";
				objRef2.style.display = 'block';
			}
			else {
				objRef.innerHTML = '';
				event.srcElement.src = "../images/tree_expand.gif";
				objRef.style.borderTopWidth = "0px";
				objRef2.style.display = 'none';
			}
		}
	</script>
</head>
<body class="itext" style="scrollbar-track-color:WhiteSmoke;text-align:center;">

<table border="0" cellpadding="1" cellspacing="2" width="95%">
<tr>
	<td id="pageMainHeadingTd">
		<span id="pageMainHeading">WIRE CONTROL</span>
	</td>
	<td valign="top">
		<table border="0" cellspacing="0" cellpadding="1" align="right">
		<tr>
			<td width="6" bgcolor="#6699cc"></td><td width="1"></td>
			<td width="50" bgcolor="#FFFFFF">Local</td>
			<td width="10"></td>
			<td width="6" bgcolor="#E9C70A"></td><td width="1"></td>
			<td width="60" bgcolor="#FEF8C8">Abroad</td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<br />
<%
pageSecurityLevel = 0
pageClosingTags = ""
Dim nEmptySum, nFullSum
Redim nEmptySum(MAX_CURRENCY) : Redim nFullSum(MAX_CURRENCY)
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
%>
<table border="0" cellpadding="2" cellspacing="1" width="95%">
<%
	sCurrenciesShown="_"
	if not rsData.EOF then
		%>
		<tr id="listHeading">
			<td colspan="2">&nbsp;</td>
			<td>Date</td>
			<td>User</td>
			<td>Bank Name</td>
			<td align="right">Transfers</td>
			<td align="right">Amount</td>
			<td align="center">Done</td>
			<td align="center">File</td>
			<td width="20" style="background-color:White;"></td>
			<td>&nbsp;</td>
		</tr>
		<tr><td height="3"></td></tr>
		<%
		nCount = 0
		set fsoServer=Server.CreateObject("Scripting.FileSystemObject")
		Do until rsData.EOF
			nCount = nCount + 1
			sCurrenciesShown=sCurrenciesShown & trim(rsData("PayCurrency")) & "_"
			sDate=""
			if day(rsData("InsertDate"))<10 then sDate=sDate & "0" end if
			sDate=sDate & day(rsData("InsertDate")) & "/"
			if month(rsData("InsertDate"))<10 then sDate=sDate & "0" end if
			sDate=sDate & month(rsData("InsertDate")) & "/" & right(year(rsData("InsertDate")),2) & "&nbsp;&nbsp;"
			sDate=sDate & FormatDateTime(rsData("InsertDate"),4)
			nEmptySum(rsData("PayCurrency")) = nEmptySum(rsData("PayCurrency")) + rsData("PayAmount")
			if rsData("DoneFlag") > 0 Then _
				nFullSum(rsData("PayCurrency")) = nFullSum(rsData("PayCurrency")) + rsData("PayAmount")
			select case rsData("Flag")
				case "0" sFlagSrc="../images/flag_clear.gif"
				case "1" sFlagSrc="../images/flag_green.gif"
				case "2" sFlagSrc="../images/flag_red.gif"
				case else sFlagSrc="../images/flag_clear.gif"
			end select
			If rsData("PayCurrency") = 0 Then
				sTdBkgColorFirst = "style=""background-color:#6699cc;"""
				xStyle = "style=""background-color:#FFFFFF;"""
			Else
				sTdBkgColorFirst = "style=""background-color:#E9C70A;"""
				xStyle = "style=""background-color:#FEF8C8;"""
			End If
			
			If nCount<>1 Then
				%>
				<tr>
					<td height="1" colspan="9"  bgcolor="silver"></td>
					<td></td>
					<td height="1" colspan="1"  bgcolor="silver"></td>
				</tr>
				<%
			End If
			%>
			<tr id="listBody">
				<td width="3" <%=sTdBkgColorFirst%>><img src="../images/1_space.gif" width="1" height="1" border="0"><br /></td>
				<td class="Numeric" align="center" <%=xStyle%>>
					<img src="../images/tree_expand.gif" style="border:0;padding:0;margin:0;cursor:pointer;" alt="Show this transfer details" onclick="call_server('<%=trim(rsData("logMasavFile_id"))%>');" />
				</td>
				<td class="Numeric" nowrap <%=xStyle%>>
					<%
					sFileName = "/Data/WireTransfer/WireFiles/RSA" & rsData("logMasavFile_id") & ".CSV"
					if fso.FileExists(Server.MapPath(sFileName)) Then _
						Response.Write("<a href=""" & sFileName & """ class=""Numeric"" " & xStyle & ">" & sDate & "</a>") _
					Else Response.Write(sDate)
					%>
				</td>
				<td class="Numeric" <%=xStyle%>>
					<%
					sUser=rsData("LogonUser")
					if inStr(sUser, "\")>0 then sUser=mid(sUser, inStr(sUser, "\")+1)
					Response.Write(sUser)
					%>
				</td>	
				<td class="Numeric" nowrap <%=xStyle%>><%= rsData("PayedBankDesc") %></td>	
				<td align="right" class="Numeric" <%=xStyle%>><%= rsData("PayCount") %></td>
				<td align="right" class="Numeric" <%=xStyle%>><%=FormatCurr(rsData("PayCurrency"), rsData("PayAmount"))%></td>
				<td class="txt11" align="center" <%=xStyle%>>
					<%
					btnTxt = "checkbox.gif"
					If rsData("DoneFlag") = 0 Then btnTxt = "checkbox_off.gif"
					response.write "<img src=""../images/" & btnTxt & """ align=""top"" height=""16"" border=""0"">"
					%>
				</td>
				<td class="txt11" align="center" <%=xStyle%>>
					<%
						If rsData("AttachedFileExt")<>"" Then
							sFileName = "?DownloadPrivateFile=" & Server.URLEncode("Wires/WireFiles/MasavConfirm_" & rsData("logMasavFile_id") & "." & rsData("AttachedFileExt"))
							Response.Write("<a target=""_blank"" href=""" & sFileName & """>Show</a>")
						Else
							Response.Write("<a href=""javascript:window.open('UploadMerchantFile.aspx?LogMasavFileID=" & rsData("logMasavFile_id") & "', 'FileUpload','WIDTH=300,HEIGHT=100,toobar=no,scrollbars=no,status=no');void(0)"">Attach</a>")
						End if
					%>
				</td>
				<td></td>
				<td class="txt11" align="center" <%=xStyle%>>
					<a href="common_flagUpdate.asp?tblName=tblLogMasavFile&FieldName=Flag&FieldIdName=logMasavFile_id&FieldIdValue=<%= rsData("logMasavFile_id") %>" target="emptyIframe"><img src="<%= sFlagSrc %>" name="flagImage<%= rsData("logMasavFile_id") %>" id="flagImage<%= rsData("logMasavFile_id") %>" width="19" height="18" border="0"></a>
				</td>
			</tr>
			<tr id="trRow<%=rsData("logMasavFile_id") %>" style="display:none;"><td colspan="2" <%=xStyle%>></td><td id="Row<%=rsData("logMasavFile_id") %>" colspan="7" <%=xStyle%>></td></tr>
			<%
			Response.Flush()
			rsData.movenext
		loop
	end if

	Set rsData = nothing
	call closeConnection()
	if sCurrenciesShown="_" then
		%>
		<tr><td colspan="9" style="text-align: center; font: normal 12px;">No transfers found. Please refine your search.</td></tr>
		<%
	else
		if nCurrency > -1 then
			nCurrencyMin=nCurrency
			nCurrencyMax=nCurrency
		else
			nCurrencyMin=0
			nCurrencyMax=MAX_CURRENCY
		end if
		%>
		<tr><td height="1" colspan="11"  bgcolor="black"></td></tr>
		<tr>
			<td colspan="4"></td>
			<td class="txt11b" align="right" valign="top">Total:</td>
			<td class="txt11b" align="right">
				<%
				For i = nCurrencyMin To nCurrencyMax
					if instr(sCurrenciesShown, "_" & trim(i) & "_")>0 then Response.Write FormatCurr(i, nEmptySum(i)) + "<br />"
				Next
				%>
			</td>
			<td class="txt11b" align="right" valign="top">Total Done:</td>
			<td class="txt11b" align="right">
				<%
				For i = nCurrencyMin To nCurrencyMax
					if instr(sCurrenciesShown, "_" & trim(i) & "_")>0 then Response.Write FormatCurr(i, nFullSum(i)) + "<br />"
				Next
				%>
			</td>
		</tr>
		<%
	end if
%>
<tr>
	<td>
		<iframe name="emptyIframe" id="emptyIframe" width="2" height="2" marginwidth="0" marginheight="0" scrolling="no" frameborder="0"></iframe>
	</td>
</tr>
</table>
<div style="text-align:left;width:95%;">
	[ <a target="_blank" style="font-size:12px;" href="../Accounting?DOK=1&UserName=limor&Password=limor@@">Wire Interface</a> ]
</div>
</body>
</html>