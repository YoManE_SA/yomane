<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Math"%>
<%@ Import Namespace="System.Globalization"%>

<script runat="server">
	
	Dim sSQL As String, sQueryWhere As String = "", _
		sAnd As String = "", sQueryString As String, sTmpTxt As String, sDivider As String
	Dim i As Integer
	
	Dim AffiliateID As Integer, lFeeRatio As Decimal, SumNIS As Decimal, SumUSD As Decimal, sName As String
	Dim sPayDate As String = "", xFee As Decimal,xValue As Decimal
	
	Function DisplayContent(sVar As String) As String
		DisplayContent = "<span class=""key"">" & replace(replace(dbPages.dbtextShow(sVar),"=","</span> = "),"|"," <br /><span class=""key"">")
	End Function
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)		
		Security.CheckPermission(lblPermissions)		
		If Not IsNumeric(""&Trim(Request("AffiliateID"))) then
			Response.Redirect ("common_blank.htm",true)
			If cint(""&Trim(Request("AffiliateID"))) < 0 then
				Response.Redirect ("common_blank.htm",true)
			End If
		End If
		AffiliateID = dbpages.TestVar(Trim(Request("AffiliateID")), 0, -1, -1) : lFeeRatio = 0
        If Not IsPostBack Then ltAffiliateName.Text = dbPages.ExecScalar("Select name From tblAffiliates Where affiliates_id=" & AffiliateID)
		sName = ltAffiliateName.Text
		tlbTop.LoadPartnerTabs(AffiliateID)

		PGData.PageSize = IIf(Trim(Request("iPageSize")) <> "", Request("iPageSize"), 25)
		sQueryWhere = sQueryWhere & sAnd & "(sma.Affiliate_id=" & AffiliateID & ")" : sAnd = " AND "
		If Trim(Request("iMerchantID")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(tblCompany.id = " & Trim(Request("iMerchantID")) & ")" : sAnd = " AND "
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere

		If Request("Totals") = "1" Then CalcTotal()
		
		If Trim(Request("UpdateAFPay")) <> ""  Then 
			'Response.Write(Trim(Request("note")) & "<br />")
			dbPages.ExecSql("Update tblAffiliatePayments Set AFP_PaymentNote='" & dbpages.DBText(Trim(Request("note"))) & "' Where AFP_ID=" &  dbpages.TestVar(Trim(Request("UpdateAFPay")), 0, -1, -1))
		End If
		
		If Trim(Request("ClearAFPay")) <> ""  Then 
			dbPages.ExecSql("Delete From tblAffiliateFeeSteps Where AFS_AFPID=" & dbpages.TestVar(Trim(Request("ClearAFPay")), 0, -1, -1))
			dbPages.ExecSql("Delete From tblWireMoney Where WireType=3 And WireSourceTbl_id=" & dbpages.TestVar(Trim(Request("ClearAFPay")), 0, -1, -1))
			dbPages.ExecSql("Delete From tblAffiliatePayments Where AFP_ID=" & dbpages.TestVar(Trim(Request("ClearAFPay")), 0, -1, -1))
		End If
		
        If dbPages.TestVar(Request("PayID"), 0, -1, 0) > 0  Then
            Response.Redirect("../include/common_totalTransShowAffiliate.asp?AFP_ID=0&AffiliateID=" & AffiliateID & "&CompanyID=" & Request("CompanyID") & "&Currency=" & Request("Currency") & "&PayID=" & Request("PayID") & "&CreateAffPay=1&Redirect=" & Server.UrlEncode(dbPages.SetUrlValue(Request.Url.ToString(), "PayID", Nothing)))
		End If
		
		If Request("UseSQL2") = "YES" Then
			dbPages.CurrentDSN = 2
			Response.Write(dbPages.ShowCurrentDSN)
		Else
			dbPages.CurrentDSN = 1
		End If
	
		sSQL = "SELECT tblAffiliatePayments.*, sma.*, tblCompany.CompanyName, tblCompany.ID As CompanyID, tblTransactionPay.paydate, tblTransactionPay.id, tblTransactionPay.TransChargeTotal, " & _
		  " tblTransactionPay.TransPayTotal, tblTransactionPay.currency " & _
		  " FROM tblTransactionPay " & _
		  " Left Join tblCompany ON(tblTransactionPay.CompanyID = tblCompany.ID) " & _
		  " Left Join [Setting].[SetMerchantAffiliate] sma ON(sma.Merchant_id = tblCompany.ID) " & _
		  " Left Join tblAffiliatePayments ON (AFP_Affiliate_ID=sma.Affiliate_id And tblTransactionPay.id = tblAffiliatePayments.AFP_TransPaymentID) " & _
		  " " & sQueryWhere & " ORDER BY tblTransactionPay.ID DESC"
		PGData.OpenDataset(sSQL)
    End Sub

    Private Sub CalcTotal()
	    Dim naTotalFees(eCurrencies.MAX_CURRENCY) As Decimal
	    Dim naTotalPayments(eCurrencies.MAX_CURRENCY) As Decimal
	    Dim naTotalPaid(eCurrencies.MAX_CURRENCY) As Decimal
		
		Response.CharSet = "windows-1255"
		Response.Write("<table class=""formThin"" width=""100%"">")
		Response.Write("<tr>")
			Response.Write("<td>TOTALS</td>")
			Response.Write("<td style=""text-align:right;cursor:pointer;float:right;"" onclick=""document.getElementById('tblTotals').style.display='none';""><b>X</b></td>")
		Response.Write("</tr>")
		Response.Write("</table>")
		Response.Write("<table class=""formNormal"" width=""350"" style=""margin:8px 5px;"">")
		Response.Write("<tr>")
			Response.Write("<th width=""25%"">Fee Sum</th>")
			Response.Write("<th>Payments Sum</th>")
			Response.Write("<th width=""35%"">Pay</th>")
		Response.Write("</tr>")
		sSQL = "SELECT tblAffiliatePayments.*, sma.*, tblCompany.CompanyName, tblTransactionPay.paydate, tblTransactionPay.id, tblTransactionPay.TransChargeTotal, " & _
		" tblTransactionPay.TransPayTotal, tblTransactionPay.currency" & _
		" FROM tblTransactionPay " & _
		" Left Join tblCompany ON(tblTransactionPay.CompanyID = tblCompany.ID) " & _
		" Left Join [Setting].[SetMerchantAffiliate] sma ON(sma.Merchant_id = tblCompany.ID) " & _
		" Left Join tblAffiliatePayments ON (AFP_Affiliate_ID=sma.Affiliate_id And tblTransactionPay.id = tblAffiliatePayments.AFP_TransPaymentID) " & _
		" " & sQueryWhere & " ORDER BY tblTransactionPay.ID DESC"
		Dim iTotalReader As SqlDataReader = dbPages.ExecReader(sSQL)
		While iTotalReader.Read()
			xFee=0
			xValue=0

			if ""&iTotalReader("AFP_ID") <> "" Then 
				xFee = iTotalReader("AFP_FeeRatio")
				xValue = iTotalReader("AFP_PaymentAmount")
			Else
				If Not IsDBNull(iTotalReader("FeesReducedPercentage")) Then xFee = iTotalReader("FeesReducedPercentage") Else xFee = lFeeRatio
				xValue = (xFee / 100) * iTotalReader("TransPayTotal")
			End if	
			naTotalFees(iTotalReader("Currency"))=naTotalFees(iTotalReader("Currency"))+xValue
			naTotalPayments(iTotalReader("Currency"))=naTotalPayments(iTotalReader("Currency"))+iTotalReader("TransPayTotal")
			if ""&iTotalReader("AFP_ID") <> "" Then naTotalPaid(iTotalReader("Currency"))=naTotalPaid(iTotalReader("Currency"))+xValue							
		End While
		iTotalReader.Close()
		for cnt As Integer=0 to eCurrencies.MAX_CURRENCY
			Response.Write("<tr>")
			Response.Write ("<td>")
			if naTotalFees(cnt)>0 then response.Write (dbPages.FormatCurr(cnt, naTotalFees(cnt)))
			if naTotalFees(cnt)>0 or naTotalPayments(cnt)>0 or naTotalPaid(cnt)>0 then response.Write ("<br />")
			Response.Write ("</td>")
			Response.Write ("<td>")
			if naTotalPayments(cnt)>0 then response.Write (dbPages.FormatCurr(cnt, naTotalPayments(cnt)))
			if naTotalFees(cnt)>0 or naTotalPayments(cnt)>0 or naTotalPaid(cnt)>0 then response.Write ("<br />")
			Response.Write ("</td>")
			Response.Write ("<td>")
			if naTotalPaid(cnt)>0 then response.Write (dbPages.FormatCurr(cnt, naTotalPaid(cnt)))
			if naTotalFees(cnt)>0 or naTotalPayments(cnt)>0 or naTotalPaid(cnt)>0 then response.Write ("<br />")
			Response.Write ("</td>")
			Response.Write("</tr>")
		Next
		Response.Write("</table>")			
		Response.End()        
    End Sub

	Private Sub ExportToExcel(sender As Object, e As System.EventArgs)
		Dim sPayDate As String = "", xFee As Decimal, xValue As Decimal, lFeeRatio As Decimal
		PGData.CloseDataset()
		PGData.PageSize = 100000
		PGData.OpenDataset(sSQL)
		Response.ContentType = "application/vnd.ms-excel"
		Response.AddHeader("Content-Disposition", "attachment;filename=report.xls")
		Response.Write("<table border=""1"">")
		Response.Write("<tr bgcolor=""#edf662""><th>Affiliate Name</th><th>Company Name</th><th>Settlement Number</th><th>Settlement Date</th><th>Fee %</th><th>Currency</th><th>Fee Sum</th><th>Payment Sum</th><th>Paid</th><th>Date</th><th>Memo</th></tr>")
		If Not TypeOf (Application.Get("CUR_ISO")) Is Array Then dbPages.LoadCurrencies()
		
		While PGData.Read()
			sPayDate = dbPages.TestVar(PGData("PayDate"), DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue).ToString("dd/MM/yyy HH:mm")
			
			if ""&PGData("AFP_ID") <> "" Then 
				xFee = PGData("AFP_FeeRatio")
				xValue = PGData("AFP_PaymentAmount")
			Else
				If Not IsDBNull(PGData("FeesReducedPercentage")) Then xFee = PGData("FeesReducedPercentage") Else xFee = lFeeRatio
				xValue = (xFee / 100) * PGData("TransPayTotal")
			End if	
			
			Response.Write("<tr>")
			Response.Write("<td>" & sName & "</td>")
			Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("CompanyName") & "</td>")
			Response.Write("<td>" & PGData("id") & "</td>")
			Response.Write("<td bgcolor=""#f0f0e8"">" & sPayDate & "</td>")
			Response.Write("<td>" & FormatNumber(xFee, 2, True) & "</td>")
			Response.Write("<td bgcolor=""#f0f0e8"">" & CType(Application.Get("CUR_ISO"), Array)(PGData("Currency")) & "</td>")
			Response.Write("<td>" & FormatNumber(xValue, 4, True) & "</td>")
			Response.Write("<td bgcolor=""#f0f0e8"">" & FormatNumber(PGData("TransPayTotal"), 4, True) & "</td>")			
			Response.Write("<td>" & IIf(""&PGData("AFP_ID")<>"","Yes","No") & "</td>")
			Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("AFP_InsertDate") & "</td>")
			Response.Write("<td>" & dbPages.dbtextShow("" & PGData("AFP_PaymentNote")) & "</td>")
			Response.Write("</tr>")
		End While
				
		PGData.CloseDataset()
		Response.Write("</table>")
		
		Response.End()
	End Sub
	
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script type="text/javascript" src="../js/func_common.js"></script>
	<script type="text/javascript" language="JavaScript">
		function IconVisibility(fVar) {
			spanObj = document.getElementById(fVar)
			if (spanObj.style.visibility == 'hidden') {
				spanObj.style.visibility = 'visible';
			}
			else {
				spanObj.style.visibility = 'hidden';
			}
        }

        function ExpandNode(fTrNum) {
        	trObj = document.getElementById("trInfo" + fTrNum)
        	trSpacer = document.getElementById("trSpacer" + fTrNum)
			imgObj = document.getElementById("oListImg" + fTrNum)
			if (trObj) {
				if (trObj.style.display == '') {
					imgObj.src = '../images/tree_expand.gif';
					trObj.style.display = 'none';
					trSpacer.style.display = 'none';
				}
				else {
					imgObj.src = '../images/tree_collapse.gif';
					trObj.style.display = '';
					trSpacer.style.display = '';
				}
			}			
		}

		function OpenPop(sURL, sName, sWidth, sHeight, sScroll) {
			placeLeft = screen.width / 2 - sWidth / 2
			placeTop = screen.height / 2 - sHeight / 2
			var winPrint = window.open(sURL, sName, 'TOP=' + placeTop + ',LEFT=' + placeLeft + ',WIDTH=' + sWidth + ',HEIGHT=' + sHeight + ',scrollbars=' + sScroll);
		}

		function nodelete() {
			for (i = 0; i < document.getElementsByTagName("INPUT").length; i++) {
				if (document.getElementsByTagName("INPUT")[i].type == "checkbox" && !document.getElementsByTagName("INPUT")[i].disabled) {
					document.getElementsByTagName("INPUT")[i].checked = false;
				}
			}
		}
		function checkdelete() {
			var isChecked = false;
			for (i = 0; i < document.getElementsByTagName("INPUT").length; i++) {
				if (document.getElementsByTagName("INPUT")[i].type == "checkbox" && document.getElementsByTagName("INPUT")[i].name == "Paid" && document.getElementsByTagName("INPUT")[i].checked && !document.getElementsByTagName("INPUT")[i].disabled) {
					isChecked = true;
					break;
				}
			}
			if (!isChecked) {
				alert("Please choose transactions to pay!");
				return false;
			}
			else {
				return confirm('Really pay?');
			}
		}		
	</script>
</head>
<body>
    <div align="center" id="waitMsg">
	    <table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
	    <tr>
		    <td>
			    <table border="0" cellspacing="0" cellpadding="0">
			    <tr><td class="txt12" style="color: gray;">Loading data &nbsp;-&nbsp; Please wait<br /></td></tr>
			    <tr><td height="4"></td></tr>
			    <tr>
				    <td><img src="../images/loading_animation_orange.gif" style="border:1px solid silver;" alt="" width="123" height="6" border="0"><br /></td>
			    </tr>
			    </table>
		    </td>
	    </tr>
	    </table>
    </div>
	<%
	Response.Flush
	%>
<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
<tr>
	<td>
		<asp:label ID="lblPermissions" runat="server" />
		<span id="pageMainHeading">Partners Management</span> - <asp:Literal runat="server" ID="ltAffiliateName" /><br /><br />
	</td>
</tr>
<tr>
	<td>
		<custom:Toolbar ID="tlbTop" Width="100%" runat="server" />
        <form id="Form1" runat="server">
		<table align="center" style="width:100%" border="0">
		<tr><td class="MerchantSubHead" colspan="2">Settlements<br /><br /></td></tr>
		<tr>
			<td style="font-size:11px; padding-bottom:4px;">
				(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');">Collapse All</a>)
			</td>
			<td align="right" style="font-size:11px;">
				Filters:
				<%
				If Trim(Request("iMerchantID"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"iMerchantID",Nothing) &"';"">Merchant</a>") : sDivider = ", "				    
				    'If Trim(Request("iReplyCode"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"iReplyCode",Nothing) &"';"">Reply</a>") : sDivider = ", "
				If sDivider <>"" Then Response.Write(" | (click to remove)") _
					Else Response.Write("Add to filter by clicking on returned fields")
				%>
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th style="width:30px;"><br /></th>
			<th>Merchant Name</th>
			<th>Settlement<br />Number</th>
			<th>Settlement<br />Date</th>
			<th>Settlement<br />Sum</th>
			<th>Show Totals</th>
			<td></td>
			<th style="text-align:center;">Pay</th>			
		</tr>
		<%
		While PGData.Read()
			i = i + 1
			sPayDate = dbPages.TestVar(PGData("PayDate"), DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue).ToString("dd/MM/yyy HH:mm")
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
				<td>
				    <% If PGData("AFP_ID") IsNot DBNull.Value Then %>
					<img onclick="ExpandNode('<%=i%>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/<%=IIF(Request("OpenNode") = PGData("id"), "tree_collapse", "tree_expand")%>.gif" alt="" width="16" height="16" border="0" align="middle" /><br />
					<% Else %>
					<img src="../images/tree_disabled.gif" alt="" width="16" height="16" border="0" align="middle" /><br />
					<% End If %>
				</td>
				<td>
					<%
					IF NOT IsDBNull(PGData("CompanyName")) Then
						If Trim(Request("iMerchantID"))<>"" Then Response.Write(PGData("CompanyName")) _
							Else dbPages.showFilter(PGData("CompanyName"), "?" & sQueryString & "&iMerchantID=" & PGData("CompanyID"), 1)
					End If
					%><br />
				</td>
				<td><%= PGData("id") %></td>
				<td nowrap="nowrap"><%= sPayDate %></td>
				<td nowrap="nowrap"><%=dbPages.FormatCurr(PGData("Currency"), PGData("TransPayTotal"))%></td>
				<td>
				    <a onclick="OpenPop('../include/common_totalTransShow.asp?LNG=1&companyID=<%=PGData("CompanyID")%>&Currency=<%=PGData("Currency")%>&PayID=<%=PGData("id")%>&isClickCount=true&isNotCalcAll=&isIframe=0','fraOrderPrint',750,300,1);" >SHOW MERCHANT TOTALS</a> &nbsp;&nbsp;
				    <a onclick="OpenPop('../include/common_totalTransShowAffiliate.asp?LNG=1&AFP_ID=<%=PGData("AFP_ID")%>&AffiliateID=<%=AffiliateID%>&companyID=<%=PGData("CompanyID")%>&Currency=<%=PGData("Currency")%>&PayID=<%=PGData("id")%>&isClickCount=true&isNotCalcAll=&isIframe=0','fraOrderPrint',750,300,1);" >SHOW AFFILIATE TOTALS</a>
				</td>
				<td style="background-color:#ffffff;"></td>
				<td style="text-align:center;">
				    <% If ("" & PGData("AFP_ID")) <> "" Then Response.Write(dbPages.FormatCurr(PGData("Currency"), PGData("AFP_PaymentAmount"))) _
				       Else Response.Write("<input type=""button"" value="" Pay "" onclick=""document.location='?AffiliateID=" & AffiliateID & "&PayID=" & PGData("id") & "&CompanyID=" & PGData("CompanyID") & "&Currency=" & PGData("Currency") & "&PageID=" & Request("PageID") & "&name=" & Server.UrlEncode(sName) & "'"" />")
				    %>
				</td>
			</tr>
			<tr style="<%=IIF(Request("OpenNode") = PGData("id"), "", "display:none;")%>" id="trSpacer<%=i%>"><td height="3px" colspan="8" style="border-bottom: 1px dashed #E2E2E2;font-size:3px;">&nbsp;</td></tr>
			<tr style="<%=IIF(Request("OpenNode") = PGData("id"), "", "display:none;")%>" id="trInfo<%=i%>">
				<td>&nbsp;</td>
				<td colspan="7" style="padding-bottom:15px">
				    <% If PGData("AFP_ID") IsNot DBNull.Value Then 
                        Dim wireID as Long = dbPages.ExecScalar("Select WireMoney_id From tblWireMoney WHERE WireType=3 AND WireSourceTbl_id=" & PGData("AFP_ID"))
				        Dim fileName as String = dbPages.TestVar(dbPages.ExecScalar("Select wmf_FileName From tblWireMoneyFile WHERE wmf_WireMoneyID=" & wireID), -1, "")
						%>
				        <table width="100%" border="0" cellspacing="0" cellpadding="1">
				        <tr>
							<td style="white-space:nowrap; vertical-align:top; height:100%;" width="25%">
								<fieldset style="height:100%;">
									<legend>Details</legend>
									<span class="SmallValueHeadings">Create Date:</span> <%=PGData("AFP_InsertDate")%><br />
									<span class="SmallValueHeadings">Wire #:</span> <%= IIF(wireID = 0, "---", wireID) %><br />
									<span class="SmallValueHeadings">Wire File:</span> <%=IIF(fileName = String.Empty, "---", "<a href=""/GlobalData/WireFileUploads/" & fileName & """ target""_blank"">" & fileName & "</a>")%><br />
								</fieldset>
							</td>
							<td style="white-space:nowrap; vertical-align:top; height:100%;" width="30%">
								<fieldset style="height:100%;">
									<legend>Notes</legend>
									<textarea id="Note<%=PGData("id")%>" name="Note<%=PGData("id")%>" cols="40" rows="5"><%=dbPages.dbtextShow(""&PGData("AFP_PaymentNote"))%></textarea><br />
									<input type="button" value=" Update " onclick="javascript:document.location.href='?UpdateAFPay=<%=PGData("AFP_ID")%>&AffiliateID=<%=AffiliateID%>&Note='+escape(document.getElementById('Note<%=PGData("id")%>').value)" /><br />
								</fieldset>
							</td>
							<td style="white-space:nowrap; vertical-align:top; height:100%;" width="45%">
								<fieldset style="height:100%;">
									<legend>Actions</legend>
									<div id="dvPayButtons<%=PGData("id")%>">
									 <input type="button" value="Edit Settlement" style="width:100px;" onclick="document.getElementById('dvPayLines<%=PGData("id")%>').src='Partners_PaymentLines.aspx?AFP_ID=<%=PGData("AFP_ID")%>&CompanyID=<%=PGData("CompanyID")%>&Currency=<%=PGData("Currency")%>&AffiliateID=<%=AffiliateID%>&PayID=<%=PGData("id")%>';document.getElementById('dvPayLines<%=PGData("id")%>').style.display='';document.getElementById('dvPayButtons<%=PGData("id")%>').style.display='none'" /> &nbsp;
									 <input type="button" value="Cancel Settlement" class="buttonDelete" style="width:100px;" onclick="javascript:if(confirm('Are you sure you wish to delete this settlment?')) document.location.href='?ClearAFPay=<%=PGData("AFP_ID")%>&AffiliateID=<%=AffiliateID%>&PageID=<%=Request("PageID")%>'" />
									</div>
									<iframe id="dvPayLines<%=PGData("id")%>" style="height:200px; width:100%;" frameborder="0" scrolling="auto" style="display:none;" ></iframe>
								</fieldset>
							</td>
						 </tr>
				        </table>
				    <% End If %>
				</td>    
			</tr>
			<%
			Response.Write("<tr><td height=""1"" colspan=""8""  bgcolor=""#ffffff""></td></tr>")
			Response.Write("<tr><td height=""1"" colspan=""6""  bgcolor=""silver""></td><td></td><td bgcolor=""silver""></td></tr>")
			Response.Write("<tr><td height=""1"" colspan=""8""  bgcolor=""#ffffff""></td></tr>")
		End while
		PGData.CloseDataset()
		%>
		</table>
		<div id="tblTotals" onresize="PosObject(this, document.getElementById('TotalsButton'), 'RRTB', 0, -10);" style="border-color:#484848; border-style:solid; border-width:1px 3px; position:absolute;width:360px;background-color:White;display:none;"> &nbsp; LOADING DATA... </div>
		<script language="JavaScript">
			function ExpandAll(fDisplay, fImgStatus) {
				for(i = 0; i <= <%= i %>; i++) {
					trObj = document.getElementById("trInfo" + i)
					imgObj = document.getElementById("oListImg" + i)
					if (trObj) {
						imgObj.src = '../images/tree_' + fImgStatus + '.gif';
						trObj.style.display = fDisplay;
					}
				}
			}
		</script>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" align="middle"> ADD TO FILTER
		</div>
	<%If Trim(Request("iPageSize")) <> "" Then %>
	<input type="hidden" name="iPageSize" value="<%=Trim(Request("iPageSize"))%>">
	<%End If %>
	<table align="center" style="width:100%">
	<tr>
		<td>
			<UC:Paging runat="Server" id="PGData" PageID="PageID" />
		</td>
		<td style="text-align:right">
			<%Dim sString As String =(Request.QueryString.ToString() & "&" & Request.Form.ToString())%>
			<input type="button" id="TotalsButton" Class="buttonWhite" value="SHOW TOTALS &Sigma;" onclick="with(document.getElementById('tblTotals')){style.display='block';if(onresize)onresize();};setAjaxHTML('?Totals=1&<%=sString%>', document.getElementById('tblTotals'), true);"  />&nbsp;			
			<asp:Button ID="btnExcel" runat="server" CssClass="buttonWhite" OnClick="ExportToExcel" Text="View Excel" OnClientClick="nodelete()" />&nbsp;
		</td>
	</tr>
	</table>	
	</form>
	<script language="JavaScript1.2" type="text/javascript">
		waitMsg.style.display = 'none';
	</script>	
	</td>
</tr>
</table>	
</body>
</html>
