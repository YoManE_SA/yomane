﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Bll;
using Netpay.Infrastructure;

public partial class AdminCash_merchant_RegistrationFilter : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

	protected List<Netpay.Bll.Merchants.Registration> GetFilteredList()
	{
		var filters = new Netpay.Bll.Merchants.Registration.SearchFilters();
		filters.RegisteredLocally = ddlRegisteredLocally.SelectedValue.ToNullableBool();
		filters.RegisteredAtCardConnect = ddlRegisteredAtCardConnect.SelectedValue.ToNullableBool();
		//if (RegisteredLocally != null) registrations = registrations.Where(r => r.MerchantID != null).ToList();
		//if (RegisteredAtCardConnect != null) registrations = registrations.Where(r => r.IntegrationData.ContainsKey("CardConnectID") == RegisteredAtCardConnect.Value).ToList();
		return Netpay.Bll.Merchants.Registration.Search(filters).List;
	}
	
    protected void Search_Click(object sender, EventArgs e)
	{
		rptList.DataSource = GetFilteredList();
		rptList.DataBind();
	}
	
    protected void Export_Click(object sender, EventArgs e)
	{
		var list = GetFilteredList();
		var listIndustries = Netpay.Bll.ThirdParty.CardConnect.GetIndustries();
		var listBusinessTypes = Netpay.Bll.ThirdParty.CardConnect.GetBusinessTypes();
		Response.ContentType = "application/vnd.ms-excel";
		Response.AddHeader("Content-Disposition", "attachment;filename=report.xls");
		Response.Write(list.ToHtmlTable(new System.Collections.Generic.HashSet<string>(new string[] { "CanceledCheckImageContent" }), true, 
			delegate(System.Reflection.PropertyInfo p, object item, ref object value) {
				if (value == null) value = string.Empty;
				if (p.Name == "Industry" && listIndustries.ContainsKey(value.ToString())) value = listIndustries[value.ToString()];
				else if (p.Name == "TypeOfBusiness" && listBusinessTypes.ContainsKey(value.ToString())) value = listBusinessTypes[value.ToString()];
			}
		));
		Response.End();
	}
	
}