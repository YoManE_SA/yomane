<!--#include file="../include/const_globalData.asp" -->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_htmlInputs.asp"-->
<!--#include file="../include/func_security.asp" -->
<%
'
' 20080709 Tamir
'
' support for "inline" delete of Credit Card Risk Management row
'
if trim(request("ImgDeleteCCRM"))<>"" then
	oleDbData.Execute("Delete From tblCreditCardRiskManagement Where CCRM_ID=" & request("ImgDeleteCCRM"))
	response.end
end if

nCompanyID = trim(request("companyID"))
sTarget = "merchant_data.asp?CompanyID=" & nCompanyID & "&ShowTrSec=" & trim(request("ShowTrSec"))
sCompNum = trim(request("compnum"))

If trim(request("Action"))="update" Then
	nIsBillingAddressMust = cInt(request("IsBillingAddressMust"))
	nIsBillingCityOptional = cInt(request("IsBillingCityOptional"))
	nIsAllow3DTrans = cInt(request("IsAllow3DTrans"))
	nIsSkipFraudDetectionInVirtualTerminal=cInt(request("IsSkipFraudDetectionInVirtualTerminal"))
	nIsShowSensitiveData = cInt(request("IsShowSensitiveData"))
	nIsCcWhiteListEnabled = cInt(request("IsCcWhiteListEnabled"))
	nIsUseFraudDetection_Local = cInt(request("IsUseFraudDetection_Local"))
	nIsUseFraudDetection_MaxMind = cInt(request("IsUseFraudDetection_MaxMind"))
	nIsAllowFreeMail = cInt(request("isAllowFreeMail"))
	AllowedAmounts = TestNumList(Request("AllowedAmounts"))
	sDailyVolumeLimit=TestNumVar(Request("DailyVolumeLimit"), 1, 0, -1)
	if sDailyVolumeLimit<0 then sDailyVolumeLimit="NULL"
    
	sSQL="UPDATE tblCompany SET" & _
		" DailyVolumeLimit=" & sDailyVolumeLimit & "," & _
		" IsBillingAddressMust=" & nIsBillingAddressMust & "," & _
		" IsBillingCityOptional=" & nIsBillingCityOptional & "," & _
		" IsBillingAddressMustIDebit=" & cInt(request("IsBillingAddressMustIDebit")) & "," & _
		" IsAllow3DTrans=" & nIsAllow3DTrans & "," & _
		" IsSkipFraudDetectionInVirtualTerminal=" & nIsSkipFraudDetectionInVirtualTerminal & "," & _
		" IsShowSensitiveData=" & nIsShowSensitiveData & "," & _
		" IsCcWhiteListEnabled=" & nIsCcWhiteListEnabled & "," & _
		" IsAllowFreeMail=" & nIsAllowFreeMail & "," & _
		" IsUseFraudDetection_Local=" & nIsUseFraudDetection_Local & "," & _
		" IsUseFraudDetection_MaxMind=" & nIsUseFraudDetection_MaxMind & "," & _
		" MaxMind_MinScore=" & TestNumVar(request("MaxMind_MinScore"), 0, 10, -1) & "," & _
		" RiskScore=" & TestNumVar(request("RiskScore"), 0, 100, -1) & "," & _
		" dailyCcMaxFailCount=" & TestNumVar(request("dailyCcMaxFailCount"), 0, -1, 0) & "," & _
		" AllowedAmounts='" & Replace(AllowedAmounts, "'", "''") & "', " & _
		" IPWhiteList='" & Replace(DBText(Request("IPWhiteList"))," ","") & "'" & _
		" WHERE ID=" & nCompanyID
		oledbData.Execute sSQL


        ' CC Limitation For eMail
        nRiskMailCcLimit = TestNumVar(Request("RiskMailCcLimit"), 1, 5, 0)
        sRiskMailCcNotifyEmail = Trim(Request("RiskMailCcNotifyEmail"))
        sRiskMailCcNotifyEmail = Replace(Replace(sRiskMailCcNotifyEmail, " ", ""), ",", ";")
        nRiskMailCcIsTransactionDeclined=TestNumVar(Request("RiskMailCcIsTransactionDeclined"), 0, 1, 0)
        nRiskMailCcIsMailBlacklisted=TestNumVar(Request("RiskMailCcIsMailBlacklisted"), 0, 1, 0)
        nRiskMailCcIsCardBlacklisted=TestNumVar(Request("RiskMailCcIsCardBlacklisted"), 0, 1, 0)
        nRiskMailCcIsCardsBlacklisted=TestNumVar(Request("RiskMailCcIsCardsBlacklisted"), 0, 1, 0)
        nRiskMailCcIsMerchantNotified=TestNumVar(Request("RiskMailCcIsMerchantNotified"), 0, 1, 0)

        ' eMail Limitation For CC
        nRiskCcMailLimit = TestNumVar(Request("RiskCcMailLimit"), 1, 5, 0)
        sRiskCcMailNotifyEmail = Trim(Request("RiskCcMailNotifyEmail"))
        sRiskCcMailNotifyEmail = Replace(Replace(sRiskCcMailNotifyEmail, " ", ""), ",", ";")
        nRiskCcMailIsTransactionDeclined=TestNumVar(Request("RiskCcMailIsTransactionDeclined"), 0, 1, 0)
        nRiskCcMailIsCCBlacklisted=TestNumVar(Request("RiskCcMailIsCCBlacklisted"), 0, 1, 0)
        nRiskCcMailIsEmailBlacklisted=TestNumVar(Request("RiskCcMailIsEmailBlacklisted"), 0, 1, 0)
        nRiskCcMailIsEmailsBlacklisted=TestNumVar(Request("RiskCcMailIsEmailsBlacklisted"), 0, 1, 0)
        nRiskCcMailIsMerchantNotified=TestNumVar(Request("RiskCcMailIsMerchantNotified"), 0, 1, 0)

		If (TestNumVar(ExecScalar("Select Merchant_id From Setting.SetMerchantRisk Where Merchant_id=" & nCompanyID, 0), 0, -1, 0) = 0) Then _
			ExecSql("Insert Into Setting.SetMerchantRisk(Merchant_id) Values(" & nCompanyID & ")")
        sSQL = "UPDATE Setting.SetMerchantRisk SET" & _
		" LimitCcForEmailAllowedCount=" & nRiskMailCcLimit & "," & _
		" IsLimitCcForEmailDeclineTrans=" & nRiskMailCcIsTransactionDeclined & "," & _
		" IsLimitCcForEmailBlockEmail=" & nRiskMailCcIsMailBlacklisted & "," & _
		" IsLimitCcForEmailBlockNewCc=" & nRiskMailCcIsCardBlacklisted & "," & _
		" IsLimitCcForEmailBlockAllCc=" & nRiskMailCcIsCardsBlacklisted & "," & _
		" IsLimitCcForEmailNotifyByEmail=" & nRiskMailCcIsMerchantNotified & "," & _
		" LimitCcForEmailNotifyByEmailList=Left(LTrim(RTrim('" & DBText(sRiskMailCcNotifyEmail) & "')), 200), " & _
        " LimitEmailForCcAllowedCount=" & nRiskCcMailLimit & "," & _
		" IsLimitEmailForCcDeclineTrans=" & nRiskCcMailIsTransactionDeclined & "," & _
		" IsLimitEmailForCcBlockCC=" & nRiskCcMailIsCCBlacklisted & "," & _
		" IsLimitEmailForCcBlockNewEmail=" & nRiskCcMailIsEmailBlacklisted & "," & _
		" IsLimitEmailForCcBlockAllEmails=" & nRiskCcMailIsEmailsBlacklisted & "," & _
		" IsLimitEmailForCcNotifyByEmail=" & nRiskCcMailIsMerchantNotified & "," & _
		" LimitEmailForCcNotifyByEmailList=Left(LTrim(RTrim('" & DBText(sRiskCcMailNotifyEmail) & "')), 200)," & _
		" BlacklistCountry='" & dbText(Request("countryBlackList")) & "'," & _
		" WhitelistCountry='" & dbText(Request("countryWhiteList")) & "'," & _
		" BlacklistState='" & dbText(Request("stateBlackList")) & "'," & _
		" WhitelistState='" & dbText(Request("stateWhiteList")) & "'" & _
		" WHERE Merchant_id=" & nCompanyID
		oledbData.Execute sSQL
End if

if Request("UpdateCCRisk") = "1" Then
	CCRM_ID = TestNumVar(Request("CCRM_ID"), 0, -1, 0)
	CCRM_Act = TestNumVar(Request("CCRM_Act"), 1, 0, 0)
	CCRM_WhitelistLevel = TestNumVar(Request("CCRM_WhitelistLevel"), 1, 0, 0)
	CCRM_Amount = TestNumVar(Request("CCRM_Amount"), 0, -1, 0)
	CCRM_MaxTrans = TestNumVar(Request("CCRM_MaxTrans"), 0, -1, 0)
	CCRM_IsActive = TestNumVar(Request("CCRM_IsActive"), 0, 1, 0)
    CCRM_ApplyVT = TestNumVar(Request("CCRM_ApplyVT"), 0, 1, 0)
	CCRM_Currency = TestNumVar(Request("CCRM_Currency"), 0, -1, 0)
	CCRM_CreditType = TestNumVar(Request("CCRM_CreditType"), 0, -1, 0)
	CCRM_PaymentMethod = TestNumVar(Request("CCRM_PaymentMethod"), 0, -1, 0)
	CCRM_Hours = TestNumVar(Request("CCRM_HourNumber"), 0, -1, 0)*TestNumVar(Request("CCRM_HourFactor"), 0, -1, 0)
	CCRM_BlockHours = TestNumVar(Request("CCRM_BlockHourNumber"), 0, -1, 0)*TestNumVar(Request("CCRM_BlockHourFactor"), 0, -1, 0)
	CCRM_ReplySource = TestNumVar(Request("CCRM_ReplySource"), 0, -1, -1)
	CCRM_ReplyMaxTrans = DBText(Request("CCRM_ReplyMaxTrans"))
	CCRM_ReplyAmount = DBText(Request("CCRM_ReplyAmount"))

	If CCRM_ReplySource=-1 Then
		'pass - default is 585/586
		If CCRM_ReplyMaxTrans="587" Then CCRM_ReplyMaxTrans="585"
		If CCRM_ReplyAmount="587" Then CCRM_ReplyAmount="586"
	Else
		'fail - default is 587
		If CCRM_ReplyMaxTrans="585" Then CCRM_ReplyMaxTrans="587"
		If CCRM_ReplyAmount="586" Then CCRM_ReplyAmount="587"
	End If

	If CCRM_ID = 0 Then 
		sSQL="INSERT INTO tblCreditCardRiskManagement (CCRM_ReplySource, CCRM_CompanyID, CCRM_Hours," & _
		" CCRM_PaymentMethod, CCRM_Currency, CCRM_Amount, CCRM_MaxTrans, CCRM_Act, CCRM_CreditType," & _
		" CCRM_BlockHours) VALUES (" & CCRM_ReplySource & ", " & nCompanyID & ", " & CCRM_Hours & "," & _
		" " & CCRM_PaymentMethod & ", " & CCRM_Currency & ", " & CCRM_Amount & ", " & CCRM_MaxTrans & "," & _
		" " & CCRM_Act & ", " & CCRM_CreditType & ", " & CCRM_BlockHours & ")"
		oleDbData.Execute(sSQL)
		Session("CCRM_LastEditedRule")=ExecScalar("Select Max(CCRM_ID) From tblCreditCardRiskManagement", 0)
		response.Redirect "merchant_dataRiskMng.asp?CompanyID=" & nCompanyID
	End If
	if CCRM_MaxTrans = -1 Then 
		oleDbData.Execute("Delete From tblCreditCardRiskManagement Where CCRM_ID=" & CCRM_ID)
	Else
		oleDbData.Execute("Update tblCreditCardRiskManagement Set CCRM_Amount=" & CCRM_Amount & _
			",CCRM_MaxTrans=" & CCRM_MaxTrans & _
			",CCRM_Act=" & CCRM_Act & _
			",CCRM_WhitelistLevel=" & CCRM_WhitelistLevel & _
			",CCRM_IsActive=" & CCRM_IsActive & _
			",CCRM_Currency=" & CCRM_Currency & _
			",CCRM_CreditType=" & CCRM_CreditType & _
			",CCRM_PaymentMethod=" & CCRM_PaymentMethod & _
			",CCRM_Hours=" & CCRM_Hours & _
			",CCRM_BlockHours=" & CCRM_BlockHours & _
			",CCRM_ReplySource=" & CCRM_ReplySource & _
			",CCRM_ReplyMaxTrans='" & CCRM_ReplyMaxTrans & "'" & _
			",CCRM_ReplyAmount='" & CCRM_ReplyAmount & "'" & _
			",CCRM_ApplyVT='" & CCRM_ApplyVT & "'" & _
			" Where CCRM_ID=" & CCRM_ID)
		Session("CCRM_LastEditedRule")=CCRM_ID
	End if
End If
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<script type="text/javascript" src="../js/func_common.js"></script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<script type="text/javascript">
 <%
	Set iRs = Server.CreateObject("Adodb.Recordset")
	GetConstArray iRs, GGROUP_CREDITTYPE, 1, "", LocCreditType, VbNull
 %>
 
 function indexByComboValue(cmbObj, isochr){
 	for(var i = 0; i < cmbObj.options.length; i++)
		if(cmbObj.options[i].value == isochr) return i;
	return -1;
 }

 function UpdateSummaryText(cmbSrcObj, listTd, listTitle)
 {
 	listTd.innerHTML = '<span style=""color:#006699;"">' + listTitle + ':</span> '
 	for(var i = cmbSrcObj.options.length - 1; i >= 0; i--)
 		listTd.innerHTML = listTd.innerHTML + cmbSrcObj.options[i].innerText + ', ';
 }
 
 function CopyItem(cmbSrcObj, cmbTargetObj, value){
	if (indexByComboValue(cmbTargetObj, value) > -1) return;
	var itemIndex = indexByComboValue(cmbSrcObj, value);
	if (itemIndex == -1) return;

	var oOption = document.createElement("OPTION");
		oOption.innerText = cmbSrcObj.options[itemIndex].innerText;
		oOption.value = cmbSrcObj.options[itemIndex].value;

	cmbTargetObj.appendChild(oOption);
	cmbSrcObj.options.remove(itemIndex)
	return oOption;
 }
 
 function copyItems(cmbSrc, cmbDst){
 	for(var i = cmbSrc.options.length - 1; i >= 0; i--)
	 	if(cmbSrc.options[i].selected)
			CopyItem(cmbSrc, cmbDst, cmbSrc.options[i].value);
 }
 
 function copyGroups(groupCombo, cmbSrc, cmbDst) {
 	for(var i = groupCombo.options.length - 1; i >= 0; i--){
	 	if(groupCombo.options[i].selected){
		 	var arstr = groupCombo.options[i].value.split(',');
			for(var x = 0; x < arstr.length; x++) 
				CopyItem(cmbSrc, cmbDst, arstr[x]);
		}
	}
 }
 
 function doSubmit(frm){
	var xVal = '';
 	var cmbSrc = document.getElementById('listCountryCompany');
 	for(var i = 0; i < cmbSrc.options.length; i++) xVal += cmbSrc.options[i].value + ',';
	frm.countryBlackList.value = xVal.substring(0, xVal.length - 1);
	
	xVal = '';
 	var cmbSrc = document.getElementById('listWCountryCompany');
 	for(var i = 0; i < cmbSrc.options.length; i++) xVal += cmbSrc.options[i].value + ',';
	frm.countryWhiteList.value = xVal.substring(0, xVal.length - 1);

	xVal = '';
	var cmbSrc = document.getElementById('listStateCompany');
	for(var i = 0; i < cmbSrc.options.length; i++) xVal += cmbSrc.options[i].value + ',';
	frm.stateBlackList.value = xVal.substring(0, xVal.length - 1);
	
	xVal = '';
	var cmbSrc = document.getElementById('listWStateCompany');
	for(var i = 0; i < cmbSrc.options.length; i++) xVal += cmbSrc.options[i].value + ',';
	frm.stateWhiteList.value = xVal.substring(0, xVal.length - 1);

	return true;
 }
 
 window.onload = function(){
	<%
	Dim strCBNames, strCWNames, strSBNames, strSWNames
	iRs.Open "Select WhitelistCountry, WhitelistState, BlacklistCountry, BlacklistState From [Setting].[SetMerchantRisk] Where Merchant_id=" & nCompanyID, oledbData, 0, 1
	If Not iRs.EOF Then 
		strCBNames = Replace(IIF(IsNull(iRs("BlacklistCountry")), "", iRs("BlacklistCountry")) , ",", "','")
		strCWNames = Replace(IIF(IsNull(iRs("WhitelistCountry")), "", iRs("WhitelistCountry")), ",", "','")

		strSBNames = Replace(IIF(IsNull(iRs("BlacklistState")), "", iRs("BlacklistState")), ",", "','")
		strSWNames = Replace(IIF(IsNull(iRs("WhitelistState")), "", iRs("WhitelistState")), ",", "','")
	End If	
	iRs.Close
	%>
	if('<%=strCBNames%>'.length > 0){
		var CmpCountries = new Array('<%=strCBNames%>');
		for(var i = 0; i < CmpCountries.length; i++) CopyItem(document.getElementById('listCountries'), document.getElementById('listCountryCompany'), CmpCountries[i], document.getElementById('TdBCountryList'));
		UpdateSummaryText(document.getElementById('listCountryCompany'), document.getElementById('TdBCountryList'), 'Block List');
	}
	if('<%=strCWNames%>'.length > 0){
		var CmpCountries = new Array('<%=strCWNames%>');
		for(var i = 0; i < CmpCountries.length; i++) CopyItem(document.getElementById('listCountries'), document.getElementById('listWCountryCompany'), CmpCountries[i], document.getElementById('TdWCountryList'));
		UpdateSummaryText(document.getElementById('listWCountryCompany'), document.getElementById('TdWCountryList'), 'White List');
	}

	if('<%=strSBNames%>'.length > 0){
		var CmpStates = new Array('<%=strSBNames%>');
		for(var i = 0; i < CmpStates.length; i++) CopyItem(document.getElementById('listStates'), document.getElementById('listStateCompany'), CmpStates[i], document.getElementById('TdBStateList'));
		UpdateSummaryText(document.getElementById('listStateCompany'), document.getElementById('TdBStateList'), 'Block List');
	}
	if('<%=strSWNames%>'.length > 0){
		var CmpStates = new Array('<%=strSWNames%>');
		for(var i = 0; i < CmpStates.length; i++) CopyItem(document.getElementById('listStates'), document.getElementById('listWStateCompany'), CmpStates[i], document.getElementById('TdWStateList'));
		UpdateSummaryText(document.getElementById('listWStateCompany'), document.getElementById('TdWStateList'), 'White List');
	}
	//CmpCountries.sort();
 }
</script>
<table align="center" border="0" cellpadding="0" cellspacing="2" dir="rtl" width="95%">
<tr>
	<td>
		<%
        sSql = "SELECT tblCompany.*, Setting.SetMerchantRisk.LimitCcForEmailAllowedCount AS RiskMailCcLimit, Setting.SetMerchantRisk.IsLimitCcForEmailBlockNewCc AS RiskMailCcIsCardBlacklisted, Setting.SetMerchantRisk.IsLimitCcForEmailBlockAllCc AS RiskMailCcIsCardsBlacklisted, Setting.SetMerchantRisk.IsLimitCcForEmailBlockEmail AS RiskMailCcIsMailBlacklisted, Setting.SetMerchantRisk.IsLimitCcForEmailDeclineTrans AS RiskMailCcIsTransactionDeclined, Setting.SetMerchantRisk.IsLimitCcForEmailNotifyByEmail AS RiskMailCcIsMerchantNotified, Setting.SetMerchantRisk.LimitCcForEmailNotifyByEmailList AS RiskMailCcNotifyEmail, Setting.SetMerchantRisk.LimitEmailForCcAllowedCount AS RiskCcMailLimit, Setting.SetMerchantRisk.IsLimitEmailForCcBlockNewEmail AS RiskCcMailIsEmailBlacklisted, Setting.SetMerchantRisk.IsLimitEmailForCcBlockAllEmails AS RiskCcMailIsEmailsBlacklisted, Setting.SetMerchantRisk.IsLimitEmailForCcBlockCC AS RiskCcMailIsCCBlacklisted, Setting.SetMerchantRisk.IsLimitEmailForCcDeclineTrans AS RiskCcMailIsTransactionDeclined, Setting.SetMerchantRisk.IsLimitEmailForCcNotifyByEmail AS RiskCcMailIsMerchantNotified, Setting.SetMerchantRisk.LimitEmailForCcNotifyByEmailList AS RiskCcMailNotifyEmail " & _
				"FROM tblCompany LEFT OUTER JOIN Setting.SetMerchantRisk ON tblCompany.ID = Setting.SetMerchantRisk.Merchant_id WHERE tblCompany.ID = " & nCompanyID
		set rsData = oledbData.Execute(sSQL)
		if Not rsData.EOF then
			%>
			<table align="center" border="0" cellpadding="1" cellspacing="0" dir="rtl" width="100%">
				<!--#include file="Merchant_dataHeaderInc.asp"-->
				<%
				pageSecurityLevel = 0
				pageClosingTags = "</table></td></tr></table>"
				Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
				%>
				<tr><td height="20"></td></tr>
				<!--#include file="Merchant_dataMenuInc.asp"-->
			</table>
			<br/>
			<form id="frmData" action="merchant_dataRiskMng.asp" name="frmRiskMngUpd" method="post" onsubmit="doSubmit(this);">
			<input type="hidden" name="Action" value="update">
			<input type="hidden" name="companyID" value="<%= nCompanyID %>">

			<input type="hidden" name="countryWhiteList" value="">
			<input type="hidden" name="countryBlackList" value="">
			<input type="hidden" name="stateBlackList" value="">
			<input type="hidden" name="stateWhiteList" value="">

			<table align="center" border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
			<tr>
				<td>
					<table align="center" border="0" cellpadding="0" cellspacing="0" dir="ltr" width="100%">
					<tr>
						<td class="MerchantSubHead">Options<br/></td>
						<td class="MerchantSubHead">Fraud Detection<br/></td>
						<td class="MerchantSubHead">CC Limitation For eMail<br/></td>
                        <td class="MerchantSubHead">eMail Limitation For CC<br/></td>
						<td rowspan="3" width="140"></td>
					</tr>
					<tr>
						<td class="txt11" valign="top">
							<!--Daily volume limit--> 
							<%
							'DivBoxTxt = "Set amount for merchants daily charge limit.<br /><br />"
							'DivBoxTxt = DivBoxTxt & "525 - When limit is reached.<br />"			
							'Call showDivBox("help","ltr", "230",DivBoxTxt)
							%>
							<input type="Checkbox" name="IsBillingAddressMust" onclick="this.form.IsBillingCityOptional.disabled=!this.checked;" <% if rsData("IsBillingAddressMust") then %>checked<% End If %> value="1"> Billing address must be verified<br />
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="Checkbox" name="IsBillingCityOptional" <% if not rsData("IsBillingAddressMust") then %>disabled <% End If %><% if rsData("IsBillingCityOptional") then %>checked <% End If %>value="1">Only Country and Zip are required<br />
							<input type="Checkbox" name="IsBillingAddressMustIDebit" <% if rsData("IsBillingAddressMustIDebit") then %>checked<% End If %> value="1"> Billing address must be verified (Instant Debit)<br />
							<input type="Checkbox" name="IsAllow3DTrans" <% if rsData("IsAllow3DTrans") then %>checked<% End If %> value="1"> Process using 3D secure<br/>
							<input type="Checkbox" name="IsShowSensitiveData" <% if rsData("IsShowSensitiveData") then %>checked<% End If %> value="1"> Display sensitive information<br/>
							<input type="Checkbox" name="IsCcWhiteListEnabled" <% if rsData("IsCcWhiteListEnabled") then %>checked<% End If %> value="1"> Enable Credit Card Whitelist<br/>
							<!-- Max declined transactions per card daily --><input type="hidden" class="inputData" style="width:50px;" dir="ltr" name="dailyCcMaxFailCount" value="<%= rsData("dailyCcMaxFailCount") %>" /><br/>
						</td>
						<td class="txt11" valign="top">
						    <!--
							<input type="Checkbox" name="IsUseFraudDetection_Local" <% if rsData("IsUseFraudDetection_Local") then %>checked<% End If %> value="1"> Use Local I.R.A<br/>
							-->
							<input type="Checkbox" name="IsUseFraudDetection_MaxMind" <% if rsData("IsUseFraudDetection_MaxMind") then %>checked<% End If %> value="1"> Assess transactions before processing<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Max Risk to Process (OLD)
							<input type="text" class="inputData" style="width:50px;" dir="ltr" name="MaxMind_MinScore" value="<% If cdbl(rsData("MaxMind_MinScore"))>=0 Then Response.Write rsData("MaxMind_MinScore") %>" /><br />
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Max Risk to Process (NEW)
							<input type="text" class="inputData" style="width:50px;" dir="ltr" name="RiskScore" value="<% If cdbl(rsData("RiskScore"))>=0 Then Response.Write rsData("RiskScore") %>" onchange="ResetRiskScores();" onkeyup="ResetRiskScores();" /><br />
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="Checkbox" name="IsSkipFraudDetectionInVirtualTerminal" <% if rsData("IsSkipFraudDetectionInVirtualTerminal") then %>checked<% End If %> value="1">Skip fraud detection in Virtual Terminal<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="Checkbox" name="isAllowFreeMail" <% if rsData("isAllowFreeMail") then %>checked<% End If %> value="1"> Ignore public e-mail in risk scoring<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							IP Whitelist
							<%
							DivBoxTxt = "For charge request from the whitelisted IP address, "
							DivBoxTxt = DivBoxTxt & "the fraud detection is not applied.<br />"
							DivBoxTxt = DivBoxTxt & "Multiple IPs must be separated with semicolons, "
							DivBoxTxt = DivBoxTxt & "without spacing before or after the separator."
							Call showDivBox("help","ltr", "310",DivBoxTxt)
							%>&nbsp;&nbsp;
							<input type="text" class="inputData" style="width:150px;" dir="ltr" name="IPWhiteList" value="<%= rsData("IPWhiteList") %>" /><br />
						</td>
						<td class="txt11" valign="top">
							Limit
							<select name="RiskMailCcLimit" id="ddlRiskMailCcLimit" class="inputData">
								<option value="0"></option>
								<%
									for i = 1 to 5
										If i = rsData("RiskMailCcLimit") Then sSelected = "selected=""selected"" " Else sSelected = ""
										%>
											<option <%= sSelected %>value="<%= i %>"><%= i %></option>
										<%
									next
								%>
							</select>
							CCs for single email address<br />
							When the limit is exceeded:<br />
							<input type="checkbox" name="RiskMailCcIsTransactionDeclined" <% If rsData("RiskMailCcIsTransactionDeclined") Then Response.Write "checked" %> value="1" style="margin-left:20px;" /> Decline transaction
							<% showDivBox "help","ltr", "340", "This setting will affect only charge attempts with new CCs.<br />Additional charge attempts with CCs already passed (since the limitation was enabled) will not be affected.<br /><br />The reply code is 547." %><br />
							<input type="checkbox" name="RiskMailCcIsMailBlacklisted" <% If rsData("RiskMailCcIsMailBlacklisted") Then Response.Write "checked" %> value="1" style="margin-left:20px;" /> Blacklist email address<br />
							<input type="checkbox" name="RiskMailCcIsCardBlacklisted" <% If rsData("RiskMailCcIsCardBlacklisted") Then Response.Write "checked" %> value="1" style="margin-left:20px;" /> Blacklist new CC
							<% showDivBox "help","ltr", "260", "Only limit-breaking CCs will be blocked.<br />The blocking is permanent (never expires).<br />Previously charged CCs will not be blocked." %><br />
							<input type="checkbox" name="RiskMailCcIsCardsBlacklisted" <% If rsData("RiskMailCcIsCardsBlacklisted") Then Response.Write "checked" %> value="1" style="margin-left:20px;" /> Blacklist all CCs
							<% showDivBox "help","ltr", "410", "Both limit-breaking CCs and previously charged CCs will be blocked.<br />The blocking is permanent (never expires)." %><br />
							<input type="checkbox" style="vertical-align:middle;margin-left:20px;" name="RiskMailCcIsMerchantNotified" <% If rsData("RiskMailCcIsMerchantNotified") Then Response.Write "checked" %> value="1" /> Notify by email/s
							<% showDivBox "help","ltr", "323", "Multiple addresses must be separated with semicolons, without spacing before or after the separator." %><br />
							<input class="inputData" style="width:200px;vertical-align:middle;margin-left:40px;" name="RiskMailCcNotifyEmail" value="<%= rsData("RiskMailCcNotifyEmail") %>" /><br />
						</td>
                        <td class="txt11" valign="top">
	                        Limit
	                        <select name="RiskCcMailLimit" id="ddlRiskCcMailLimit" class="inputData">
		                        <option value="0"></option>
		                        <%
			                        for i = 1 to 5
				                        If i = rsData("RiskCcMailLimit") Then sSelected = "selected=""selected"" " Else sSelected = ""
				                        %>
					                        <option <%= sSelected %>value="<%= i %>"><%= i %></option>
				                        <%
			                        next
		                        %>
	                        </select>
	                        Emails for single CC<br />
	                        When the limit is exceeded:<br />
	                        <input type="checkbox" name="RiskCcMailIsTransactionDeclined" <% If rsData("RiskCcMailIsTransactionDeclined") Then Response.Write "checked" %> value="1" style="margin-left:20px;" /> Decline transaction
	                        <br />
                            <input type="checkbox" name="RiskCcMailIsCCBlacklisted" <% If rsData("RiskCcMailIsCCBlacklisted") Then Response.Write "checked" %> value="1" style="margin-left:20px;" /> Blacklist CC
	                        <br />
                            <input type="checkbox" name="RiskCcMailIsEmailBlacklisted" <% If rsData("RiskCcMailIsEmailBlacklisted") Then Response.Write "checked" %> value="1" style="margin-left:20px;" /> Blacklist new Email
	                        <br />
                            <input type="checkbox" name="RiskCcMailIsEmailsBlacklisted" <% If rsData("RiskCcMailIsEmailsBlacklisted") Then Response.Write "checked" %> value="1" style="margin-left:20px;" /> Blacklist all Emails
	                        <br />
                            <input type="checkbox" style="vertical-align:middle;margin-left:20px;" name="RiskCcMailIsMerchantNotified" <% If rsData("RiskCcMailIsMerchantNotified") Then Response.Write "checked" %> value="1" /> Notify by email/s
	                        <% showDivBox "help","ltr", "323", "Multiple addresses must be separated with semicolons, without spacing before or after the separator." %><br />
	                        <input class="inputData" style="width:200px;vertical-align:middle;margin-left:40px;" name="RiskCcMailNotifyEmail" value="<%= rsData("RiskCcMailNotifyEmail") %>" /><br />
                        </td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td><br/></td></tr>
			<tr>
				<td valign="top">
					<span class="MerchantSubHead">Predefined Transaction Amounts</span>
					(comma-separated)
					<% Call showDivBox("help","ltr", "230","Comma-delimited list of transaction amounts valid for the merchant, e.g. 1.67,2.00,5.00,1.54.<br /><br />If empty, any positive amount is valid.<br /><br />588 - when amount is not in the list.") %>
					<br /><br />
				</td>
			</tr>
			<tr><td><textarea class="inputData" style="width:100%; height:40px;" name="AllowedAmounts"><%=rsData("AllowedAmounts")%></textarea><br /><br /><br /></td></tr>
			<tr>
				<td valign="top">
					<img onclick="showSection('0',1);" style="cursor:pointer;" id="oListToggle0" src="../images/tree_expand.gif" width="16" height="16" border="0" align="middle">
					<span onclick="showSection('0',1);" class="MerchantSubHead" style="cursor:pointer; vertical-align:middle;">Geo Ip/Bin White & Black List</span>
					<%
					DivBoxTxt = "Manage a list of blocked countries. Country is taken from cc issuer or payer IP address.<br /><br />"
					DivBoxTxt = DivBoxTxt & "581 - When BIN country match.<br />"
					DivBoxTxt = DivBoxTxt & "582 - When IP country match.<br />"
					Call showDivBox("help","ltr", "230",DivBoxTxt) %><br /><br />
				</td>
			</tr>
			<tr><td id="TdBCountryList"></td></tr>
			<tr><td id="TdWCountryList"></td></tr>
			<tr><td><br /></td></tr>
			<tr id="Tbl0" style="display:none;">
				<td>
					<% sInputExtra = "multiple size=""10"" class=""inputData"" style=""width:180px; direction:ltr;""" %>
					<% sInputExtra2 = "multiple size=""10"" class=""inputData"" style=""width:180px; direction:ltr; background-color:white!important;""" %>
					<table border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
					<tr>
						<td class="txt11b" style="text-align:center; color:#454545;">
							Country White List
							<%DivBoxTxt = "The system reduce risk managment scores by 2.5 points when the ip country is considered as high risk but we have it in this list<br /><br />"
							Call showDivBox("help","ltr", "230",DivBoxTxt) %>
						</td>
						<td></td>
						<td class="txt11b" style="text-align:center; color:#454545;">Country list<br /></td>
						<td class="txt11b" style="text-align:center; color:#454545;">Country group<br /></td>
						<td></td>
						<td class="txt11b" style="text-align:center; color:#454545;">
							Country Black List
							<%DivBoxTxt = "The system blocks transactions from these countries.<br /><br />"
							Call showDivBox("help","ltr", "230",DivBoxTxt) %>
						</td>
					</tr>
					<tr>
						<td class="txt11" align="center" valign="top">
							<select name="listWCountryCompany" id="listWCountryCompany" <%=sInputExtra%>></select>
							<br /><br />
							<input type="button" name="" value="Remove Country" class="b_companyData" style="width:180px;" onclick="copyItems(document.getElementById('listWCountryCompany'), document.getElementById('listCountries')); UpdateSummaryText(document.getElementById('listWCountryCompany'), document.getElementById('TdWCountryList'), 'White List');" /><br />
						</td>
						<td>
							<input type="button" value="&lt;&lt; Add Country" class="b_companyData" style="width:120px;" onclick="copyItems(document.getElementById('listCountries'), document.getElementById('listWCountryCompany')); UpdateSummaryText(document.getElementById('listWCountryCompany'), document.getElementById('TdWCountryList'), 'White List');" /><br />
							<input type="button" value="&lt;&lt; Add Group" class="b_companyData" style="width:120px;" onclick="copyGroups(document.getElementById('listCountryRegion'), document.getElementById('listCountries'), document.getElementById('listWCountryCompany')); UpdateSummaryText(document.getElementById('listWCountryCompany'), document.getElementById('TdWCountryList'), 'White List');" /><br />
						</td>
						<td class="txt11" align="center" valign="top">
							<%
							sSQL="SELECT CountryISOCode, Name FROM [List].[CountryList] ORDER BY Name"
							set rsData2 = oledbData.execute(sSQL)
							PutRecordsetComboDefault rsData2, "listCountries", sInputExtra2, "CountryISOCode", "Name", "false", "false"
							rsData2.close
							Set rsData2 = nothing
							%>
						</td>
						<td class="txt11" align="center" valign="top">
							<%
							sInputExtra =	"multiple size=""10"" class=""inputData"" style=""width:180px; direction:ltr;"""""
							sSQL="SELECT REPLACE((Select CountryIsoCode + ',' AS 'data()'  From [List].CountryListToCountryGroup Where [List].CountryListToCountryGroup.CountryGroup_id=[List].[CountryGroup].CountryGroup_id FOR XML PATH('')), ' ', '') As isoCodes, Name AS CountryName FROM [List].[CountryGroup] ORDER BY Name"
							set rsData2 = oledbData.execute(sSQL)
							PutRecordsetComboDefault rsData2, "listCountryRegion",sInputExtra2, "isoCodes", "CountryName", "false", "false"
							rsData2.close
							Set rsData2 = nothing
							%>
						</td>
						<td class="txt11" align="center">
							<input type="button" value="Add Country &gt;&gt;" class="b_companyData" style="width:120px;" onclick="copyItems(document.getElementById('listCountries'), document.getElementById('listCountryCompany')); UpdateSummaryText(document.getElementById('listCountryCompany'), document.getElementById('TdBCountryList'), 'Block List');" /><br />
							<input type="button" value="Add Group &gt;&gt;" class="b_companyData" style="width:120px;" onclick="copyGroups(document.getElementById('listCountryRegion'), document.getElementById('listCountries'), document.getElementById('listCountryCompany')); UpdateSummaryText(document.getElementById('listCountryCompany'), document.getElementById('TdBCountryList'), 'Block List');" /><br />
						</td>
						<td class="txt11" align="center" valign="top">
							<select name="listCountryCompany" id="listCountryCompany" <%=sInputExtra%>></select>
							<br /><br />
							<input type="button" name="" value="Remove Country" class="b_companyData" style="width:180px;" onclick="copyItems(document.getElementById('listCountryCompany'), document.getElementById('listCountries')); UpdateSummaryText(document.getElementById('listCountryCompany'), document.getElementById('TdBCountryList'), 'Block List');" /><br />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td height="12"></td></tr>
			<tr><td bgcolor="#A9A7A2" height="1"></td></tr>

			<tr>
				<td valign="top">
					<img onclick="showSection('1',1);" style="cursor:pointer;" id="oListToggle1" src="../images/tree_expand.gif" width="16" height="16" border="0" align="middle">
					<span onclick="showSection('1',1);" class="MerchantSubHead" style="cursor:pointer; vertical-align:middle;">State White & Black List</span>
					<%
					DivBoxTxt = "Manage a list of blocked states. state is taken from billing address.<br /><br />"
					DivBoxTxt = DivBoxTxt & "582 - When IP country match.<br />"
					Call showDivBox("help","ltr", "230",DivBoxTxt) %><br /><br />
				</td>
			</tr>
			<tr><td id="TdBStateList"></td></tr>
			<tr><td id="TdWStateList"></td></tr>
			<tr><td><br /></td></tr>
			<tr id="Tbl1" style="display:none;">
				<td>
					<% sInputExtra = "multiple size=""10"" class=""inputData"" style=""width:180px; direction:ltr;""" %>
					<% sInputExtra2 = "multiple size=""10"" class=""inputData"" style=""width:180px; direction:ltr; background-color:white!important;""" %>
					<table border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
					<tr>
						<td class="txt11b" style="text-align:center; color:#454545;">
							Country White List
							<%DivBoxTxt = "The system reduce risk managment scores by 2.5 points when the state is considered as high risk but we have it in this list<br /><br />"
							Call showDivBox("help","ltr", "230",DivBoxTxt) %>
						</td>
						<td></td>
						<td class="txt11b" style="text-align:center; color:#454545;">State list<br /></td>
						<td class="txt11b" style="text-align:center; color:#454545;">State group<br /></td>
						<td></td>
						<td class="txt11b" style="text-align:center; color:#454545;">
							State Black List
							<%DivBoxTxt = "The system blocks transactions from these state.<br /><br />"
							Call showDivBox("help","ltr", "230",DivBoxTxt) %>
						</td>
					</tr>
					<tr>
						<td class="txt11" align="center" valign="top">
							<select name="listWStateCompany" id="listWStateCompany" <%=sInputExtra%>></select>
							<br /><br />
							<input type="button" name="" value="Remove State" class="b_companyData" style="width:180px;" onclick="copyItems(document.getElementById('listWStateCompany'), document.getElementById('listStates')); UpdateSummaryText(document.getElementById('listWStateCompany'), document.getElementById('TdWStateList'), 'White List');" /><br />
						</td>
						<td>
							<input type="button" value="&lt;&lt; Add State" class="b_companyData" style="width:120px;" onclick="copyItems(document.getElementById('listStates'), document.getElementById('listWStateCompany')); UpdateSummaryText(document.getElementById('listWStateCompany'), document.getElementById('TdWStateList'), 'White List');" /><br />
							<input type="button" value="&lt;&lt; Add Group" class="b_companyData" style="width:120px;" onclick="copyGroups(document.getElementById('listStateRegion'), document.getElementById('listStates'), document.getElementById('listWStateCompany')); UpdateSummaryText(document.getElementById('listWStateCompany'), document.getElementById('TdWStateList'), 'White List');" /><br />
						</td>
						<td class="txt11" align="center" valign="top">
							<%
							sSQL="SELECT StateISOCode, Name AS StateName FROM [List].[StateList] ORDER BY Name"
							set rsData2 = oledbData.execute(sSQL)
							PutRecordsetComboDefault rsData2, "listStates", sInputExtra2, "StateISOCode", "StateName", "false", "false"
							rsData2.close
							Set rsData2 = nothing
							%>
						</td>
						<td class="txt11" align="center" valign="top">
							<%
							sInputExtra =	"multiple size=""10"" class=""inputData"" style=""width:180px; direction:ltr;"""""
							sSQL="SELECT '' As isoCodes, Name AS StateName FROM [List].[CountryGroup] Where 1=2 ORDER BY Name"
							set rsData2 = oledbData.execute(sSQL)
							PutRecordsetComboDefault rsData2, "listStateRegion",sInputExtra2, "isoCodes", "StateName", "false", "false"
							rsData2.close
							Set rsData2 = nothing
							%>
						</td>
						<td class="txt11" align="center">
							<input type="button" value="Add State &gt;&gt;" class="b_companyData" style="width:120px;" onclick="copyItems(document.getElementById('listStates'), document.getElementById('listStateCompany')); UpdateSummaryText(document.getElementById('listStateCompany'), document.getElementById('TdBStateList'), 'Block List');" /><br />
							<input type="button" value="Add Group &gt;&gt;" class="b_companyData" style="width:120px;" onclick="copyGroups(document.getElementById('listStateRegion'), document.getElementById('listStates'), document.getElementById('listStateCompany')); UpdateSummaryText(document.getElementById('listStateCompany'), document.getElementById('TdBStateList'), 'Block List');" /><br />
						</td>
						<td class="txt11" align="center" valign="top">
							<select name="listStateCompany" id="listStateCompany" <%=sInputExtra%>></select>
							<br /><br />
							<input type="button" name="" value="Remove State" class="b_companyData" style="width:180px;" onclick="copyItems(document.getElementById('listStateCompany'), document.getElementById('listStates')); UpdateSummaryText(document.getElementById('listStateCompany'), document.getElementById('TdBStateList'), 'Block List');" /><br />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td height="12"></td></tr>
			<tr><td bgcolor="#A9A7A2" height="1"></td></tr>

			<tr><td height="6"></td></tr>
			<tr>
				<td>
					<input type="submit" value="Save Changes" style="float:right;width:110px; height:auto; background-color:#f1f1f1;">
					<a class="faq_eng" href="BLCommon.aspx?BL_CompanyID=<%=nCompanyID%>">Common Blacklist</a>
				</td>
			</tr>
			</table>
			</form>
			<br /><br /><br />
			<table class="formNormal" border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
			<tr>
				<td colspan="10" valign="top">
					<span style="float:right;">
						<span style="background-color:#66cc66;">&nbsp;&nbsp;</span>
						Passed
						&nbsp;&nbsp;
						<span style="background-color:#ff6666;">&nbsp;&nbsp;</span>
						Failed
					</span>
					<span class="MerchantSubHead">Limitation By Payment Method</span>
					<%
					DivBoxTxt = "Limit process amounts and/or number of transactions.<br />"
					DivBoxTxt = DivBoxTxt & """Time Frame"" column sets the time frame for the limit. When set to 0, the limit is per transaction.<br />"
					DivBoxTxt = DivBoxTxt & "<br />"
					DivBoxTxt = DivBoxTxt & "585 - Default reply<span style=""color:Maroon;"">*</span>, when passed transaction count is exceeded.<br />"
					DivBoxTxt = DivBoxTxt & "586 - Default reply<span style=""color:Maroon;"">*</span>, when passed amont is exceeded.<br />"
					DivBoxTxt = DivBoxTxt & "587 - Default reply<span style=""color:Maroon;"">*</span>, when temporary block is created after fail.<br />"
					DivBoxTxt = DivBoxTxt & "<br />"
					DivBoxTxt = DivBoxTxt & "<span style=""color:Maroon;"">*</span>Default replies can be overriden by selecting different codes in dropdown boxes in ""Reply"" columns."
					Call showDivBox("help","ltr", "230", DivBoxTxt) %><br />
					<br />
				</td>
			</tr>
			<tr>
				<td colspan="10" style="padding-bottom:6px;">
					<form method="post" action="?UpdateCCRisk=1&CompanyID=<%=nCompanyID%>">
						<input type="submit" value="ADD NEW RULE" class="btn" style="width:120px;">
					</form>
				</td>
			</tr>
			<tr>
				<th>ID</th>
				<!--<th>Created</th>-->
				<th>Reply Source</th>
				<th>Time Frame</th>
				<th>Payment Method</th>
				<th>Credit Type</th>
				<th>Whitelist Level</th>
				<th>Amount Processed</th>
				<th>Trans<br />Count</th>
				<th style="display:none;">Action</th>
				<th>Block Duration</th>
				<th style="text-align:center;">Reply<br />(Amount)</th>
				<th style="text-align:center;">Reply<br />(Trans Count)</th>
				<th style="text-align:center;">Apply in<br />Virtual Terminal</th>
				<th style="text-align:center;">Active</th>
				<th><br /></th>
			</tr>
			<script language="javascript" type="text/javascript">
				//
				// 20080709 Tamir
				//
				// support for "inline" delete of Credit Card Risk Management row
				//
				function ConfirmAndDelete(nCCRM_ID)
				{
					if (confirm("Are you sure ?!"))
					{
						var imgDelete=document.createElement("IMG");
						imgDelete.src="merchant_dataRiskMng.asp?ImgDeleteCCRM="+nCCRM_ID;
						document.getElementById("trCCRM"+nCCRM_ID).style.display="none";
					}
				}

				function ResetReplyFieldsVisibility(selReplySource)
				{
					selReplySource.form.CCRM_ReplyAmount.style.visibility = (selReplySource.selectedIndex == 0 ? "visible" : "hidden");
					selReplySource.form.CCRM_ReplyMaxTrans.style.visibility = selReplySource.form.CCRM_ReplyAmount.style.visibility;
				}
			</script>
			<%
			sSQL="SELECT * FROM tblCreditCardRiskManagement WHERE CCRM_CompanyID=" & nCompanyID & " ORDER BY CCRM_IsActive DESC, CCRM_InsDate DESC"
			Set iRs = oleDbData.Execute(sSQL)
			sStyleBGColor="background-color:#EEEEEE;"
			If Not iRs.EOF Then CCRM_IsActive=iRs("CCRM_IsActive")
			
			if request.QueryString("CCRMID")<>"" then
				nCurrentRuleID=request.QueryString("CCRMID")
			else
				nCurrentRuleID=Session("CCRM_LastEditedRule")
			end if
			nCurrentRuleID=TestNumVar(nCurrentRuleID, 1, 0, 0)

			Do While Not iRs.EOF
				sStyleBGColor=IIf(sStyleBGColor="", "background-color:#F0F0F0;", "")
				sStyleBGColorShow=IIf(nCurrentRuleID-iRs("CCRM_ID")=0, "background-color:#fff4b3;", sStyleBGColor)
				
				If CCRM_IsActive <> iRs("CCRM_IsActive") Then
					Response.Write("<tr><td colspan=""10""><hr width=""100%"" size=""1"" noshade style=""border:1px dotted #666666;""><b>INACTIVE</b><br /></td></tr>")
					CCRM_IsActive = iRs("CCRM_IsActive")
				End If
				%>
				<tr><td height="2"></td></tr>
				<tr id="trCCRM<%= iRs("CCRM_ID") %>" style="<%=sStyleBGColorShow%>">
					<form method="post" action="?UpdateCCRisk=1&CompanyID=<%=nCompanyID%>">
					<input type="hidden" name="CCRM_ID" value="<%=iRs("CCRM_ID")%>">
					<td class="txt10"><%=iRs("CCRM_ID")%></td>
					<!--<td class="txt10"><%'=Left(iRs("CCRM_InsDate"), 10)%></td>-->
					<td class="txt11" style="text-transform:capitalize;">
						<span style="height:16px; width:4; background-color:<%=IIf(iRs("CCRM_ReplySource")<0, "#66cc66", "#ff6666")%>;">&nbsp;</span>
						<select name="CCRM_ReplySource" class="inputData" style="text-transform:capitalize;" onclick="//ResetReplyFieldsVisibility(this);">
							<option value="-1">Passed
							<%
							Set rsReplySource = oledbData.Execute("SELECT GD_ID, Lower(GD_Text+' decline') GD_Text FROM GetGlobalData(46) UNION SELECT 999, 'all declines' ORDER BY GD_ID DESC")
							Do While Not rsReplySource.EOF
								sSelected=IIf(iRs("CCRM_ReplySource")-rsReplySource("GD_ID")=0, "selected=""selected""", "")
								Response.Write("<option " & sSelected & "style=""text-transform:capitalize;"" value=""" & rsReplySource("GD_ID") & """>" & rsReplySource("GD_Text") & "</option>")
								rsReplySource.MoveNext	
							Loop	
							rsReplySource.Close
							%>
						</select>
					</td>
					<td class="txt11">
						<%
						if iRs("CCRM_Days")*24-iRs("CCRM_Hours")=0 then
							sSelectedDays="selected=""selected"""
							sSelectedHours=""
							nNumber=iRs("CCRM_Days")
							nFactor=24
						else
							sSelectedDays=""
							sSelectedHours="selected=""selected"""
							nNumber=iRs("CCRM_Hours")
							nFactor=1
						end if
							
						%>
						<select name="CCRM_HourNumber" class="inputData">
							<%
							For i = 0 To 60
								sSelected=IIF(nNumber-i=0, "selected=""selected""", "")
								Response.Write("<option value=""" & i & """ " & sSelected & ">" & i & "</option>")
							Next	
							%>
						</select>
						<select name="CCRM_HourFactor" class="inputData">
							<option value="1" <%=sSelectedHours%>>Hrs</option>
							<option value="24" <%=sSelectedDays%>>Days</option>
						</select>
					</td>
					<td class="txt11">
						<select name="CCRM_PaymentMethod" class="inputData">
							<option value="0">[All]</option>
							<%
							Set rsPaymentMethod = oledbData.Execute("SELECT PaymentMethod_id AS ID, name as pm_Name FROM List.PaymentMethod WHERE PaymentMethod_id > " & PMD_MAXINTERNAL & " ORDER BY PaymentMethod_id")
							Do While Not rsPaymentMethod.EOF
								sSelected=IIf(iRs("CCRM_PaymentMethod")-rsPaymentMethod("ID")=0, "selected=""selected""", "")
								Response.Write("<option value=""" & rsPaymentMethod("ID") & """ " & sSelected & ">" & rsPaymentMethod("pm_Name") & "</option>")
								rsPaymentMethod.MoveNext
							Loop
							rsPaymentMethod.Close
							%>
						</select>
					</td>
					<td class="txt11">
						<select name="CCRM_CreditType" class="inputData">
							<option value="255">[All]</option>
							<option value="0" <%=IIf(iRs("CCRM_CreditType")=0, "selected=""selected""", "") %>><%=LocCreditType(0)%></option>
							<option value="1" <%=IIf(iRs("CCRM_CreditType")=1, "selected=""selected""", "") %>><%=LocCreditType(1)%></option>
						</select>
					</td>
					<td class="txt11">
						<select name="CCRM_WhitelistLevel" class="inputData">
							<option value="-1">Not Listed</option>
							<%
							Set rsWhitelistLevel = oledbData.Execute("SELECT * FROM GetGlobalData(39)")
							Do While Not rsWhitelistLevel.EOF
								sSelected=IIf(iRs("CCRM_WhitelistLevel")-rsWhitelistLevel("GD_ID")=0, "selected=""selected""", "")
								Response.Write("<option style=""background-color:" & rsWhitelistLevel("GD_Color") & """ value=""" & rsWhitelistLevel("GD_ID") & """ " & sSelected & ">" & rsWhitelistLevel("GD_Text") & "</option>")
								rsWhitelistLevel.MoveNext
							Loop	
							rsWhitelistLevel.Close
							%>
						</select>
					</td>
					<td class="txt11">
						<input name="CCRM_Amount" class="inputData" size="6" value="<%=iRs("CCRM_Amount")%>" />
						<select name="CCRM_Currency" class="inputData">
							<option value="-1"></option>
							<%
							For i = 0 To MAX_CURRENCY
								sSelected=IIf(iRs("CCRM_Currency")-i=0, "selected=""selected""", "")
								Response.Write("<option value=""" & i & """ " & sSelected & ">" & GetCurISO(i)) & "</option>"
							Next	
							%>
						</select>
					</td>
					<td class="txt11"><input name="CCRM_MaxTrans" class="inputData" size="5" value="<%=iRs("CCRM_MaxTrans")%>"/></td>
					<!--
					<td class="txt11">
						<select name="CCRM_Act" class="inputData">
							<option value="0" <%if iRs("CCRM_Act") = 0 Then Response.Write("selected")%>>Decline</option>
							<option value="1" <%if iRs("CCRM_Act") = 1 Then Response.Write("selected")%>>Phone Verify</option>
							<option value="2" <%if iRs("CCRM_Act") = 2 Then Response.Write("selected")%>>Resolve by upload</option>
						</select>
					</td>
					-->
					<td>
						<%
						if iRs("CCRM_BlockDays")*24-iRs("CCRM_BlockHours")=0 then
							sSelectedDays="selected=""selected"""
							sSelectedHours=""
							nNumber=iRs("CCRM_BlockDays")
							nFactor=24
						else
							sSelectedDays=""
							sSelectedHours="selected=""selected"""
							nNumber=iRs("CCRM_BlockHours")
							nFactor=1
						end if
							
						%>
						<select name="CCRM_BlockHourNumber" class="inputData">
							<%
							For i = 0 To 60
								sSelected=IIF(nNumber-i=0, "selected=""selected""", "")
								Response.Write("<option value=""" & i & """ " & sSelected & ">" & i & "</option>")
							Next	
							%>
						</select>
						<select name="CCRM_BlockHourFactor" class="inputData">
							<option value="1" <%=sSelectedHours%>>Hrs</option>
							<option value="24" <%=sSelectedDays%>>Days</option>
						</select>
					</td>
					<%
						sStyle=""'IIf(iRs("CCRM_ReplySource")=-1, "", "visibility:hidden;")
					%>
					<td style="text-align:center;">
						<select name="CCRM_ReplyAmount" class="inputData" style="<%= sStyle %>">
							<%
							Set rsReplyCode = oledbData.Execute("SELECT * FROM GetReplyCodes(1, DEFAULT) ORDER BY Code")
							Do Until rsReplyCode.EOF
								sSelected=IIf(trim(iRs("CCRM_ReplyAmount"))=trim(rsReplyCode("Code")), "selected=""selected""", "")
								Response.Write("<option value=""" & rsReplyCode("Code") & """ " & sSelected & " title=""" & rsReplyCode("Code") & " " & rsReplyCode("Description") & """>" & rsReplyCode("Code") & "</option>")
								rsReplyCode.MoveNext
							Loop	
							%>
						</select>
					</td>
					<td style="text-align:center;">
						<select name="CCRM_ReplyMaxTrans" class="inputData" style="<%= sStyle %>">
							<%
							rsReplyCode.MoveFirst
							Do Until rsReplyCode.EOF
								sSelected=IIf(trim(iRs("CCRM_ReplyMaxTrans"))=trim(rsReplyCode("Code")), "selected=""selected""", "")
								Response.Write("<option value=""" & rsReplyCode("Code") & """ " & sSelected & " title=""" & rsReplyCode("Code") & " " & rsReplyCode("Description") & """>" & rsReplyCode("Code") & "</option>")
								rsReplyCode.MoveNext
							Loop
							rsReplyCode.Close
							%>
						</select>
					</td>
					<td style="text-align:center;">
						<%
						sChecked=IIf(iRs("CCRM_ApplyVT"), "checked=""checked""", "")
						%>
						<input type="checkbox" style="background-color:Transparent;" name="CCRM_ApplyVT" value="1" <%=sChecked%> />
					</td>
					<td style="text-align:center;">
						<%
						sChecked=IIf(iRs("CCRM_IsActive"), "checked=""checked""", "")
						%>
						<input type="checkbox" style="background-color:Transparent;" name="CCRM_IsActive" value="1" <%=sChecked%> />
					</td>
					<td>
						<input type="submit" value="Update" class="button1" style="width:50px;">
						<input type="button" value="Delete" class="button1" style="width:50px; color:Maroon;" onclick="ConfirmAndDelete(<%=iRs("CCRM_ID")%>);">
					</td>
					</form>
				</tr>
				<%
			 iRs.MoveNext
			Loop
			iRs.Close
			%>
			</table>	
			<%
		End If
		rsData.close
		Set rsData = nothing
		
		call closeConnection()
		%>
	</td>
</tr>
</table>
</body>
</html>