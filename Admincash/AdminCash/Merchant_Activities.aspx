<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
	Dim i As Integer
	Const DEFAULT_FILTER As String = "IsInactive=1"
	Const MESSAGE_MERCHANTS_ALL As String = "All inactive merchants are displayed."
	Const MESSAGE_MERCHANTS_LISTED As String = "Only the merchants listed in the filter textbox are displayed."

	Protected Sub Delete_Click(sender As Object, e As EventArgs)
		If Request("MerchantID") <> String.Empty Then
			dbPages.ExecSql("DELETE FROM tblCompany WHERE ID IN (" & Request("MerchantID") & ")")
		End If
	End Sub

	Protected Sub UpdateStatus_Click(sender As Object, e As EventArgs)
		If ddlStatus.SelectedIndex > -1 And Request("MerchantID") <> String.Empty Then
            Dim accountDisable As Boolean
            accountDisable = ddlStatus.SelectedValue = MerchantStatus.Closed Or ddlStatus.SelectedValue = MerchantStatus.Blocked Or ddlStatus.SelectedValue = MerchantStatus.Archived
            dbPages.ExecSql("UPDATE tblCompany SET ActiveStatus=" & ddlStatus.SelectedValue & " WHERE ID IN (" & Request("MerchantID") & ")")
            dbPages.ExecSql("UPDATE Data.LoginAccount SET IsActive=" & IIf(accountDisable, 0, 1) & " Where LoginAccount_id IN (Select LoginAccount_id From Data.Account Where Merchant_ID IN(" & Request("MerchantID") & "))")
        End If
	End Sub
	
	Sub AddConditionToSQL(ByRef sSQL As String, ByVal sCondition As String)
		sSQL &= IIf(sSQL.IndexOf("WHERE") > 0, " AND ", " WHERE ") & sCondition
	End Sub

	Sub Page_Load()
		'lblMsg.Text = MESSAGE_MERCHANTS_ALL
		Security.CheckPermission(lblPermissions)
	End Sub
	
	Protected Overrides Sub OnPreRender(e As EventArgs)
		Dim sqlQuery as String = "SELECT  dbo.tblCompany.ID, dbo.tblCompany.ActiveStatus, dbo.tblCompany.CustomerNumber, dbo.tblCompany.CompanyName, " & _
			"dbo.tblGlobalData.GD_Text AS Status_Name, ISNULL(dbo.tblGlobalData.GD_Color, '') AS Status_Color," & _
			"dbo.tblCompany.InsertDate AS [Signup_Date], " & _
			"DateLastTransPass ,DateLastTransPreAuth, DateLastTransPending, DateLastTransFail, DateLastSettlement " & _
			"FROM dbo.tblCompany " & _
			"INNER JOIN Track.MerchantActivity AS ma ON dbo.tblCompany.ID = ma.Merchant_id " & _
			"LEFT JOIN dbo.tblGlobalData ON (dbo.tblGlobalData.GD_LNG = 1 And dbo.tblGlobalData.GD_Group = 44 And dbo.tblCompany.ActiveStatus = dbo.tblGlobalData.GD_ID) " 
		
		If Request("ShowInactive") = "1" Then
			Response.Write(Request("ShowInactive"))
			AddConditionToSQL(sqlQuery, "ActiveStatus Not IN (" & CType(MerchantStatus.Processing, Integer) & ")")
		Else
			If Not String.IsNullOrEmpty(Request("Merchant")) Then AddConditionToSQL(sqlQuery, " ID IN (" & Request("Merchant") & ")")
			If Not String.IsNullOrEmpty(Request("MerchantGroup")) Then AddConditionToSQL(sqlQuery, " GroupID IN (" & Request("MerchantGroup") & ")")
			If Request("FilterStatus") <> String.Empty Then AddConditionToSQL(sqlQuery, "ActiveStatus IN (" & Request("FilterStatus") & ")")
			If Security.IsLimitedMerchant Then AddConditionToSQL(sqlQuery, "ID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))")
		End If
		gvMerchants.DataSource = dbPages.ExecReader(sqlQuery)
		gvMerchants.DataBind()
		MyBase.OnPreRender(e)
	End Sub
	
	Protected Sub gvMerchants_RowCommand(sender As Object, e As GridViewCommandEventArgs)
		Response.Write("jere")
		If e.CommandName = "Delete" Then 
			Response.Write("DELETE FROM tblCompany WHERE ID IN (" & e.CommandArgument & ")")
			'dbPages.ExecSql("DELETE FROM tblCompany WHERE ID IN (" & Request("MerchantID") & ")")
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Merchant Status</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../js/func_common.js"></script>
</head>
<body>
	<form action="" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Merchant Status</h1>
		<div><asp:label ID="lblMsg" runat="server" /></div>
		<script language="javascript" type="text/javascript">
			function HasSelectedMerchant()
			{
				var sMerchants = "0";
				if (frmForm.MerchantID) {
					if (frmForm.MerchantID[0]) {
						for (var i = 0; i != frmForm.MerchantID.length; i++) {
							if (frmForm.MerchantID[i].checked) sMerchants += "," + frmForm.MerchantID[i].value.replace("	", "");
						}
					} else {
						if (frmForm.MerchantID.checked) sMerchants += "," + frmForm.MerchantID.value.replace("	", "");
					}
				}
				if (sMerchants == "0") {
					alert("No merchant selected!");
					return false;
				}
				return true;
			}

			function SetAllCheckboxes(chbThis) {
				if (chbThis.form.MerchantID) {
					if (chbThis.form.MerchantID[0]) {
						for (var i=0;i!=chbThis.form.MerchantID.length;i++)
							chbThis.form.MerchantID[i].checked=chbThis.checked;
					} else {
						chbThis.form.MerchantID.checked=chbThis.checked;
					}
				}
			}

			function ConfirmAndDeleteMerchant(nID) {
				if (!confirm("Are you sure ?!")) return false;
				if (!confirm("The merchant will be deleted from the system!\n\nThis operation cannot be undone!\n\nDo you really want to delete the merchant ?!")) return false;
				return true;
			}

			function ChangeMerchantsStatus(frmForm) {
				if (document.getElementById("NewStatus").selectedIndex==0) {
					alert("New status not specified!");
					return false;
				}
				if (!HasSelectedMerchant()) return false;
				return true;
			}

			function DeleteMerchants(frmForm) {
				if (!HasSelectedMerchant()) return false;
				if (!confirm("Are you sure ?!")) return false;
				if (!confirm("The selected merchants will be deleted from the system!\n\nThis operation cannot be undone!\n\nDo you really want to delete these merchants ?!")) return false;
				return true;
			}
		</script>
		<br />
		<div>
			<input type="checkbox" onclick="SetAllCheckboxes(this);" />Select / Unselect all
		</div>
		<asp:GridView ID="gvMerchants" runat="server" ViewStateMode="Disabled" AutoGenerateColumns="False" DataKeyNames="ID" AllowSorting="False" CssClass="grid" SelectedRowStyle-CssClass="rowSelected" Width="100%" OnRowCommand="gvMerchants_RowCommand">
			<SelectedRowStyle CssClass="rowSelected" />
			<Columns>
				<asp:TemplateField>
					<ItemTemplate><input type="checkbox" name="MerchantID" value="<%# Eval("ID") %>" /></ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField ItemStyle-HorizontalAlign="Center">
					<ItemTemplate><span style="background-color:<%# Eval("Status_Color") %>">&nbsp;&nbsp;</span></ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="ID" HeaderText="ID" HeaderStyle-HorizontalAlign="left" />
				<asp:BoundField DataField="CustomerNumber" HeaderText="Merchant Number" HeaderStyle-HorizontalAlign="left" />
				<asp:HyperLinkField DataTextField="CompanyName" HeaderText="CompanyName" HeaderStyle-HorizontalAlign="left" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="merchant_data.asp?CompanyID={0}"  />
				<asp:BoundField DataField="Signup_Date" HeaderText="Signup Date" ItemStyle-HorizontalAlign="Center" />
				<asp:BoundField DataField="Status_Name" HeaderText="Current Status" ItemStyle-HorizontalAlign="Center" />
				<asp:BoundField DataField="DateLastTransPass" NullDisplayText="---" HeaderImageUrl="~/images/icon_ccPayTrans.gif" HeaderText="Last TransPass" ItemStyle-HorizontalAlign="Center" />
				<asp:BoundField DataField="DateLastTransPending" NullDisplayText="---" HeaderImageUrl="~/images/icon_ccPendingTrans.gif" HeaderText="Last TransPending" ItemStyle-HorizontalAlign="Center" />
				<asp:BoundField DataField="DateLastTransPreAuth" NullDisplayText="---" HeaderImageUrl="~/images/icon_ccOkTrans.gif" HeaderText="Last TransPreAuth" ItemStyle-HorizontalAlign="Center" />
				<asp:BoundField DataField="DateLastTransFail" NullDisplayText="---" HeaderImageUrl="~/images/icon_ccFailedTrans.gif" HeaderText="Last TransFail" ItemStyle-HorizontalAlign="Center" />
				<asp:BoundField DataField="DateLastSettlement" NullDisplayText="---" HeaderText="Last Settlement" ItemStyle-HorizontalAlign="Center" />
				<asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="merchant_transListHistory.asp?CompanyID={0}" HeaderText="History" Text="History" ItemStyle-HorizontalAlign="Center" />
				<asp:ButtonField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" CommandName="Delete" ButtonType="Button" DataTextField="ID" DataTextFormatString="Delete" />
			</Columns>
		</asp:GridView>
		<div class="bordered workplace">
			<span style="float:right;padding-bottom:1px;">
				<asp:Button runat="server" CssClass="buttonWhite buttonDelete buttonWide" Text="Delete Merchants" OnClick="Delete_Click" OnClientClick="DeleteMerchants(this.form);" />
			</span>
			Update status
			<netpay:MerchantStatusDropDown runat="server" ID="ddlStatus" />
			&nbsp;
			<asp:Label ID="lblStatusSelect" runat="server" />
			<asp:Button runat="server" CssClass="buttonWhite" Text="Update" OnClick="UpdateStatus_Click" OnClientClick="ChangeMerchantsStatus(this.form);" />
		</div>
	</form>
</body>
</html>