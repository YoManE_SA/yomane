<%@ Page Language="VB" %>
<script runat="server">
	Dim sPageMenuWidth, sResetBodyJS As String

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		sPageMenuWidth = dbPages.TestVar(Request.QueryString("pageMenuWidth"), -1, "*")
		Dim sResetBodyURL As String = Request.QueryString("ResetBodyURL")
		sResetBodyJS = IIf(String.IsNullOrEmpty(sResetBodyURL), String.Empty, "parent.frmBody.location.href='" & sResetBodyURL & "';")
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<script language="JavaScript">
		function FrmResize()
		{
			var frsSubBrowser = top.frames['fraBrowser'].document.getElementById('fraSubBrowse');
			if (frsSubBrowser.cols=='0,33,*')
			{
				frsSubBrowser.cols='<%=sPageMenuWidth%>,33,*';
				<%= sResetBodyJS %>
				setTimeout("document.getElementById('imgTab').src = '../images/ResizeTabA_ltr.gif'", 10);
			}
			else
			{
				frsSubBrowser.cols='0,33,*';
				setTimeout("document.getElementById('imgTab').src = '../images/ResizeTabB_ltr.gif'", 10);
			}
		}
	</script>
</head>
<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">
	<table height="100%" border="0" cellspacing="0" cellpadding="0" align="left">
		<tr>
			<td width="4" bgcolor="#484848">
				<img src="../images/1_space.gif" alt="" border="0" /><br />
			</td>
			<td height="100%">
				<a href="#" onclick="FrmResize();return false;">
					<img id="imgTab" src="../images/ResizeTabA_ltr.gif" alt="" border="0"></a><br />
			</td>
		</tr>
	</table>
</body>
</html>
