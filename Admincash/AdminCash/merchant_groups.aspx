<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20100202 Tamir
	'
	' Merchant groups management
	'

	Protected Sub SetDeleteConfirmation(ByVal o As Object, ByVal e As GridViewRowEventArgs) Handles gvRecords.RowDataBound
		NPControls.SetGridViewDeleteConfirmation(e)
	End Sub

	Sub ReloadPage(ByVal o As Object, ByVal e As GridViewDeletedEventArgs) Handles gvRecords.RowDeleted
		Response.Redirect(Request.Url.PathAndQuery)
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsRecords.ConnectionString = dbPages.DSN()
	End Sub

	Sub AddRecord(ByVal sender As Object, ByVal e As System.EventArgs)
		If String.IsNullOrEmpty(txtNewRecord.Text) Then
			lblAddRecord.Text = "Specify new group name!"
		Else
			lblAddRecord.Text = String.Empty
			dbPages.ExecSql("INSERT INTO tblMerchantGroup (mg_Name) VALUES ('" & dbPages.DBText(txtNewRecord.Text) & "')")
			txtNewRecord.Text = String.Empty
			gvRecords.DataBind()
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Merchant Groups</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body class="menu">
	<form id="frmServer" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Merchant Groups<span></span></h1>
		<div class="bordered">
			Add new group
			<asp:TextBox ID="txtNewRecord" CssClass="wide" runat="server" />
			<asp:Button ID="btnAddRecord" CssClass="buttonWhite" runat="server" Text="Add" OnClick="AddRecord" UseSubmitBehavior="True" />
			<asp:Label ID="lblAddRecord" runat="server" ForeColor="red" />
		</div>
		<asp:GridView ID="gvRecords" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="dsRecords" AllowPaging="False"
			AllowSorting="False" SelectedRowStyle-CssClass="rowSelected" Width="100%"
		>
			<Columns>
				<asp:BoundField DataField="ID" HeaderText="ID" HeaderStyle-HorizontalAlign="right" ReadOnly="True" />
				<asp:BoundField DataField="mg_Name" HeaderText="Group&nbsp;Name" HeaderStyle-HorizontalAlign="left" ControlStyle-CssClass="wide" HeaderStyle-Width="100%" />
				<asp:HyperLinkField HeaderText="Merchants" ItemStyle-HorizontalAlign="Center" DataTextField="Merchants" DataTextFormatString="{0}" DataNavigateUrlFormatString="merchant_list.asp?NumShow=100&Action=SearchFilter&GroupID={0}" DataNavigateUrlFields="ID" />
				<asp:CommandField HeaderText="Actions" ButtonType="Button" ItemStyle-CssClass="centered buttons" ControlStyle-CssClass="buttonWhite"
					ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="False" HeaderStyle-CssClass="actions" UpdateText="Save"
				/>
			</Columns>
		</asp:GridView>
		<asp:SqlDataSource ID="dsRecords" runat="server" ProviderName="System.Data.SqlClient"
			SelectCommand="SELECT *, (SELECT Count(*) FROM tblCompany WHERE GroupID=g.ID) Merchants FROM tblMerchantGroup g ORDER BY mg_Name"
			UpdateCommand="UPDATE tblMerchantGroup SET mg_Name=IsNull(@mg_Name, '') WHERE ID=@ID"
			DeleteCommand="DELETE FROM tblMerchantGroup WHERE ID=@ID"
		>
			<DeleteParameters>
				<asp:Parameter Name="ID" Type="Int32" />
			</DeleteParameters>
			<UpdateParameters>
				<asp:Parameter Name="mg_Name" Type="String" />
				<asp:Parameter Name="ID" Type="Int32" />
			</UpdateParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>