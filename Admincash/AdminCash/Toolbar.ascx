<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Toolbar.ascx.vb" Inherits="Controls_Toolbar" %>
<style type="text/css">
	table.toolbarControl { width:100%;padding:2px 0;border:1px solid #A9A7A2;background-color:#F1F1F1; }
	table.toolbarControl tr.tabRow td { text-align:center;cursor:pointer;font-weight:normal;font-size:10px; }
	table.toolbarControl tr.tabRow td.toolbarSeparator { color:#A9A7A2;width:1%;cursor:default; }
	table.toolbarControl tr.tabRow td.current { font-weight:bold;background-color:#D6C8A8; }
</style>
<div style="text-align:center;" id="dvTabs" runat="server">
	<asp:Label ID="lblContainer" runat="server">
		<table border="0" cellpadding="2" cellspacing="2" class="toolbarControl">
			<tr class="tabRow">
				<asp:Literal ID="litItems" Text="" runat="server" />
			</tr>
			<tr runat="server" id="trChildObj">
			    <td runat="server" id="tdChildObj" style="padding:10px; border:solid 1px #A9A7A2; background-color:White;"></td>
			</tr>
		</table>
	</asp:Label>
</div>
<br />
