<!--#include file="../include/func_adoConnect.asp"-->
<%
sRefundAskID = dbText(request("RefundAskID"))

If trim(request("Action"))="UPDATE" then
	sRefundAskComment = dbText(request("RefundAskComment"))
	sRefundAskConfirmationNum = dbText(request("RefundAskConfirmationNum"))
	sSQL="UPDATE tblRefundAsk SET RefundAskComment='" & sRefundAskComment & "', RefundAskConfirmationNum='" & sRefundAskConfirmationNum & "' WHERE id = " & sRefundAskID
    oledbData.Execute(sSQL)
End if
%>
<html>
<head>
	<title>Refund request update</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
</head>
<body leftmargin="0" topmargin="8" rightmargin="0" bottommargin="8">

<%
sSQL="SELECT RefundAskComment, RefundAskConfirmationNum FROM tblRefundAsk WHERE id = " & sRefundAskID
set rsData = oledbData.execute(sSQL)
If NOT rsData.EOF Then
    %>
    <form action="" method="post" name="frmRefundAskUpd" id="frmRefundAskUpd">
    <table border="0" cellspacing="0" cellpadding="1">
    <tr>
	    <td class="txt13">
		    Refund comment
		    <input type="Text" name="RefundAskComment" maxlength="300" value="<%= dbTextShow(rsData("RefundAskComment")) %>" size="30" class="inputData">
	        &nbsp;&nbsp;&nbsp;
		    Case number
		    <input type="Text" name="RefundAskConfirmationNum" maxlength="30" value="<%= dbTextShow(rsData("RefundAskConfirmationNum")) %>" size="12" class="inputData">
	        &nbsp;&nbsp;&nbsp;
		    <input type="submit" name="Action" value="UPDATE" class="b_windowCharge">
	    </td>
    </tr>
	<%
	sSQL="SELECT l.*, IsNull(' - '+DescriptionMerchantEng, '') CodeText FROM tblRefundAskLog l WITH (NOLOCK)" & _
	" LEFT JOIN tblDebitCompanyCode c WITH (NOLOCK) ON ral_description='processing a refund failed ('+Code+')' AND IsNull(FailSource, 1)<>0" & _
	" WHERE refundAsk_id = " & sRefundAskID & " ORDER BY refundAskLog_id desc"
	Set rsDataTmp = oledbData.Execute(sSQL)
	If Not rsDataTmp.EOF Then
		%>
        <tr>
            <td colspan="5">
                <br /><span class="txt12b">Action Log</span><br />
                <%
                Do until rsDataTmp.EOF
                    Response.Write(rsDataTmp("ral_date") & "&nbsp;|&nbsp;")
                    Response.Write(rsDataTmp("ral_user") & "&nbsp;|&nbsp;")
                    Response.Write(rsDataTmp("ral_description") & rsDataTmp("CodeText") & "<br />")
				rsDataTmp.movenext
				Loop
                %>
            </td>
        </tr>
		<%
	End If
    rsDataTmp.close
    Set rsDataTmp = nothing
	%>
    </table>
    </form>
    <%
End if

rsData.close
Set rsData = nothing
call closeConnection()
%>

</body>
</html>
