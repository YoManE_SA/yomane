<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
	Sub UploadFile(ByVal o As Object, ByVal e As EventArgs)
		Dim sFilename, sExt, sTitle, sSql As String
        if Request.Files.Count = 0 Then Exit Sub 
		sFilename = Request.Files(0).FileName
		sExt = sFilename.Substring(sFilename.LastIndexOf(".") + 1).ToLower
		If ".gif.jpg.jpe.jpeg.png.tif.tiff.pdf.xls.xlsx.doc.docx.".Contains(sExt + ".") Then
			If dbPages.TestVar(hidWireID.Value, 1, 0, 0) > 0 Then
				sFilename = "WireConfirm_" & hidWireID.Value & "." & sExt
				sTitle = "Wire #" & hidWireID.Value
			Else
				sFilename = "MasavConfirm_" & hidLogMasavFileID.Value & "." & sExt
				sTitle = "Masav #" & hidLogMasavFileID.Value
			End If

            Request.Files(0).SaveAs(Domain.Current.MapPrivateDataPath("Wires/WireFiles/" & sFilename))
			Dim iReader As SqlDataReader = dbPages.ExecReader("Select WireMoney_id From tblLogMasavDetails Where logMasavFile_id=" & hidLogMasavFileID.Value)
			Do While iReader.Read()
                'Dim nParseLog As String = "", nParseResults As Byte = 0
                'If sExt = "pdf" Then nParseResults = dbPages.ParsePDF(Domain.Current.MapPrivateDataPath("Wires/WireFiles/" & sFilename), iReader(0), nParseLog)
                sSql = "INSERT INTO tblWireMoneyFile (wmf_WireMoneyID, wmf_FileName," & _
                    " wmf_Description, wmf_User, wmf_ParseLog, wmf_ParseResult) VALUES (" & iReader(0) & ", Left('" & dbPages.DBText(sFilename) & "'," & _
                    " 100), Left('Wire Confirm', 350), Left('" & dbPages.DBText(Security.Username) & "', 40), '', 0)"
				dbPages.ExecSql(sSql)
			Loop
			iReader.Close()
			'Dim sSQL As String = "INSERT INTO tblFile(Title, CompanyName, FileName1, FileExt1)" & " VALUES('" & sTitle & "', '', '" & sFilename & "','png')"
			'If dbPages.TestVar(hidCompanyID.Value, 1, 0, 0) > 0 Then dbPages.ExecSql(sSQL)
			sSql = "UPDATE tblLogMasavFile SET AttachedFileExt='" & sExt & "' WHERE LogMasavFile_id=" & hidLogMasavFileID.Value
			If dbPages.TestVar(hidLogMasavFileID.Value, 1, 0, 0) > 0 Then dbPages.ExecSql(sSQL)
			fuDocument.Visible = False
			btnUpload.Visible = False
			btnClose.Visible = True
		End If
	End Sub

	Sub Page_Load()
		If Not Page.IsPostBack Then
			hidCompanyID.Value = dbPages.TestVar(Request("CompanyID"), 1, 0, 0)
			hidWireID.Value = dbPages.TestVar(Request("WireID"), 1, 0, 0)
			hidLogMasavFileID.Value = dbPages.TestVar(Request("LogMasavFileID"), 1, 0, 0)
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>File Upload</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
</head>
<body>
	<form id="form1" runat="server">
		<asp:FileUpload ID="fuDocument" runat="server" />
		<asp:HiddenField ID="hidCompanyID" runat="server" />
		<asp:HiddenField ID="hidWireID" runat="server" />
		<asp:HiddenField ID="hidLogMasavFileID" runat="server" />
		<asp:Button ID="btnUpload" OnClick="UploadFile" Text="Go!" UseSubmitBehavior="false" runat="server" />
		<asp:Button ID="btnClose" OnClientClick="top.close();" Text="Close" Visible="false" UseSubmitBehavior="false" runat="server" />
	</form>
</body>
</html>
