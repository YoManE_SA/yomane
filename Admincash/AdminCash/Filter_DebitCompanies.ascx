﻿<%@ Control Language="VB" ClassName="Filter_DebitCompanies" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
	Private sClientFieldDC As String = "DebitCompanyID"
	Public Property ClientFieldDebitCompany() As String
		Get
			Return sClientFieldDC
		End Get
		Set(ByVal value As String)
			sClientFieldDC = value
		End Set
	End Property
	
	Private sClientFieldDT As String = "DebitCompanyID"
	Public Property ClientFieldDebitTerminal() As String
		Get
			Return sClientFieldDT
		End Get
		Set(ByVal value As String)
			sClientFieldDT = value
		End Set
	End Property
	
	Sub Page_Load()
		litDebitCompany.Text = "<select name=""" & ClientFieldDebitCompany & """ onchange=""FilterTerminals(this, false, true);"">"
		litDebitCompany.Text &= "<option></option>"
		Dim sSQL As String = "SELECT DebitCompany_ID, dc_Name, dc_isActive FROM tblDebitCompany"
		If Security.IsLimitedDebitCompany Then sSQL &= " WHERE DebitCompany_ID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
		sSQL &= " ORDER BY dc_isActive desc, dc_Name asc"
		Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
		While drData.Read
			litDebitCompany.Text &= "<option style=""color:" & IIf(drData("dc_isActive"),"#000000","maroon") & ";"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		End While
		drData.Close()
		litDebitCompany.Text &= "</select>"
		litTerminal.Text = "<div>"
		litTerminal.Text &= "<input name=""" & ClientFieldDebitTerminal & """ type=""hidden"" />"
		litTerminal.Text &= "<input id=""txt" & ClientFieldDebitTerminal & """ class=""medium grayBG"" onkeyup=""FilterTerminals(this, true, false);"" "
		litTerminal.Text &= "ondblclick=""FilterTerminals(this, false, false);"" />"
		litTerminal.Text &= "</div>"
		litTerminal.Text &= "<select id=""sel" & ClientFieldDebitTerminal & """ style=""position:absolute;display:none;z-index:1;border-style:none;"" "
		litTerminal.Text &= "onchange=""SelectFilterTerminal(this);"" onclick=""SelectFilterTerminal(this);"" onblur=""this.style.display='none';"" />"
		sSQL = "SELECT TerminalNumber, DebitCompany  FROM tblDebitTerminals"
		If Security.IsLimitedDebitCompany Then sSQL &= " WHERE DebitCompany IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
		sSQL &= " ORDER BY DebitCompany, TerminalNumber"
		Dim sbJS As New StringBuilder("<s" & "cript language=""javascript"" type=""text/javascript"">")
		sbJS.Append("var naDC=new Array(), saDT=new Array();")
		drData = dbPages.ExecReader(sSQL)
		Dim nCount As Integer = 0
		While drData.Read
			sbJS.Append("naDC[" & nCount & "]=" & drData(1) & "; saDT[" & nCount & "]=""" & drData(0) & """;")
			nCount += 1
			litTerminal.Text &= "<option value=""" & drData(0) & """>" & drData(0) & "</option>"
		End While
		sbJS.Append("var nTerminals=" & nCount & ";")
		sbJS.Append("</s" & "cript>")
		litJS.Text = sbJS.ToString
		drData.Close()
		litTerminal.Text &= "</select>"
	End Sub
</script>
<script language="javascript" type="text/javascript">
	function FilterTerminals(oControl, bKeyUp, bDebitCompanySelect)
	{
		var selDebitCompany=oControl.form.<%= ClientFieldDebitCompany %>;
		var inpTerminal=document.getElementById("txt<%= ClientFieldDebitTerminal %>");
		var hidTerminal=oControl.form.<%= ClientFieldDebitTerminal %>;
		var selTerminal=oControl.form.sel<%= ClientFieldDebitTerminal %>;
		hidTerminal.value=inpTerminal.value;
		if (bKeyUp && event.keyCode<27) return;
		if ((bKeyUp && event.keyCode==27)||(bDebitCompanySelect && selDebitCompany.selectedIndex==0 && inpTerminal.value==""))
		{
			selTerminal.style.display="none";
			return;
		}
		if (selTerminal.length>0)
		{
			selTerminal.selectedIndex=0;
			for (;selTerminal.length>0;) selTerminal.remove(0);
		}
		var nSize=0;
		for(var i=0;i<nTerminals;i++)
		{
			if ((inpTerminal.value.length==0)||(saDT[i].substr(0, inpTerminal.value.length)==inpTerminal.value))
			{
				if ((selDebitCompany.selectedIndex==0)||(naDC[i]==selDebitCompany.value))
				{
					selTerminal.add(new Option(saDT[i], saDT[i]));
					if (nSize<10) nSize++;
				}
			}
		}
		if (nSize>0) selTerminal.size=nSize;
		if (!bDebitCompanySelect) selTerminal.style.display=(nSize>0?"":"none");
		if (selTerminal.style.display!="none") selTerminal.focus();
		selTerminal.multiple=(nSize==1);
	}

	function SelectFilterTerminal(oControl)
	{
		var inpTerminal=document.getElementById("txt<%= ClientFieldDebitTerminal %>");
		var selTerminal=oControl.form.sel<%= ClientFieldDebitTerminal %>;
		var hidTerminal=oControl.form.<%= ClientFieldDebitTerminal %>;
		inpTerminal.value=selTerminal.value;
		selTerminal.style.display="none";
		hidTerminal.value=inpTerminal.value;
	}
</script>
<asp:Literal ID="litJS" runat="server" />
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="filterNormal">
	<tr>
		<td nowrap="nowrap">
			Debit Company
		</td>
		<td>
			<asp:Literal ID="litDebitCompany" runat="server" />
		</td>
	</tr>
	<tr>
		<td nowrap="nowrap">
			Terminal
		</td>
		<td>
			<asp:Literal ID="litTerminal" runat="server" />
		</td>
	</tr>
</table>