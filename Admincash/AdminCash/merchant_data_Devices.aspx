<%@ Page Language="VB" %>
<%@ Import Namespace="Netpay.Infrastructure" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="TabMenu.ascx" TagPrefix="UC1" TagName="TabMenu" %>

<script runat="server">
	Protected Overrides Sub OnLoad(e As System.EventArgs)
		MyBase.OnLoad(e)
        If Not IsPostBack Then
            Dim account = Netpay.Bll.Merchants.Merchant.Load(Request("merchantID").ToNullableInt().GetValueOrDefault())
            rptDevices.DataSource = Netpay.Bll.Accounts.MobileDevice.Search(account.AccountID)
            rptDevices.DataBind()
			
            Dim orangeSettings = Netpay.Bll.Affiliates.MerchantSettings.Load(Netpay.Bll.ThirdParty.Orange.PartnerId, Request("merchantID"))
            If orangeSettings IsNot Nothing Then
                chkServiceMain_Confirm.Checked = orangeSettings.ConfigurationValues(Netpay.Bll.ThirdParty.Orange.ServiceCodeMaster + Netpay.Bll.ThirdParty.Orange.ServiceConfirmPostfix).ToInt32(0) = 1
                chkServiceMain.Checked = orangeSettings.ConfigurationValues(Netpay.Bll.ThirdParty.Orange.ServiceCodeMaster).ToInt32(0) = 1
                chkServiceInv_Confirm.Checked = orangeSettings.ConfigurationValues(Netpay.Bll.ThirdParty.Orange.ServiceCodeInvoices + Netpay.Bll.ThirdParty.Orange.ServiceConfirmPostfix).ToInt32(0) = 1
                chkServiceInv.Checked = orangeSettings.ConfigurationValues(Netpay.Bll.ThirdParty.Orange.ServiceCodeInvoices).ToInt32(0) = 1
                txtServiceDevices_Confirm.Text = orangeSettings.ConfigurationValues(Netpay.Bll.ThirdParty.Orange.ServiceCodeChild + Netpay.Bll.ThirdParty.Orange.ServiceConfirmPostfix).ToInt32(0)
                txtServiceDevices.Text = orangeSettings.ConfigurationValues(Netpay.Bll.ThirdParty.Orange.ServiceCodeChild).ToInt32(0)
                txtPhoneNumber.Text = orangeSettings.UserID
                txtPhoneNumber.Enabled = Not (chkServiceMain_Confirm.Checked Or chkServiceInv_Confirm.Checked Or (txtServiceDevices_Confirm.Text.ToInt32(0) > 0))
                ltNextSync.Text = IIf(orangeSettings.SyncDate Is Nothing, "Not Needed", orangeSettings.SyncDate)
            End If
        End If
	End Sub
	
	Protected Sub OnUpdateOrangeStatus_Command(sender As Object, e As System.EventArgs)
		Dim orangeSettings As Netpay.Bll.Affiliates.MerchantSettings
		If txtPhoneNumber.Enabled Then
			orangeSettings = Netpay.Bll.Affiliates.MerchantSettings.Load(Netpay.Bll.ThirdParty.Orange.PartnerId, Request("merchantID"))
			If orangeSettings Is Nothing Then orangeSettings = New Netpay.Bll.Affiliates.MerchantSettings(Netpay.Bll.ThirdParty.Orange.PartnerId, Request("merchantID"))
			orangeSettings.UserID = txtPhoneNumber.Text
			Try
				orangeSettings.Save()
			Catch ex As Exception
				lblMessage.Text = ex.Message
				Return
			End Try
		End If
		Netpay.Bll.ThirdParty.Orange.setMerchantServices(Request("merchantID"), 
			chkServiceMain.Checked, chkServiceInv.Checked, txtServiceDevices.Text.ToInt32(0))
		orangeSettings =  Netpay.Bll.Affiliates.MerchantSettings.Load(Netpay.Bll.ThirdParty.Orange.PartnerId, Request("merchantID"))
		ltNextSync.Text = IIF(orangeSettings.SyncDate is Nothing, "Not Needed", orangeSettings.SyncDate)
	End Sub
	
	Protected Sub IsActive_Click(sender As Object, e As System.EventArgs)
		Dim chkbox as CheckBox = CType(sender, System.Web.UI.WebControls.CheckBox)
		Dim rowId As Integer = chkbox.Attributes("value").ToInt32()
		Netpay.Bll.Accounts.MobileDevice.BlockMobileDevice(rowId, chkbox.Checked)
	End Sub
	
</script>
<html>
<head id="Head1" runat="server">
    <title>Merchant Fees</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
    <link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
    <script src="../js/func_common.js" type="text/jscript"></script>
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../js/jquery-ui-1.8.10.custom/css/custom-theme/jquery-ui-1.8.10.custom.css" />
</head>
<body>
    <form id="form1" runat="server">
	<UC1:TabMenu ID="TabMenu1" Width="95%" runat="server" idFieldName="merchantID" />
	<table width="95%">
		<tr>
			<td width="80%" valign="top">
				<asp:Repeater runat="server" ID="rptDevices">
					<HeaderTemplate>
						<table rules="all" width="100%">
							<tr>
								<th>Insert Date</th>
								<th>Identity</th>
								<th>User Agent</th>
								<th>Phone</th>
								<th>Pass Code</th>
								<th>Las tLogin</th>
								<th>Is Activated</th>
								<th>Is Active</th>
								<th>App Version</th>
								<th>Sign Fail Count</th>
							</tr>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<tr>
								<td><%#Eval("InsertDate")%></td>
								<td><%#Eval("DeviceIdentity")%></td>
								<td><%#Eval("DeviceUserAgent")%></td>
								<td><%#Eval("DevicePhoneNumber")%></td>
								<td><%#Eval("PassCode")%></td>
								<td><%#Eval("LastLogin")%></td>
								<td align="center"><%#Eval("IsActivated")%></td>
								<td align="center"><asp:CheckBox runat="server" Checked='<%#Eval("IsActive")%>' OnCheckedChanged="IsActive_Click" AutoPostBack="true" value='<%#Eval("id")%>' /></td>
								<td><%#Eval("AppVersion")%></td>
								<td><%#Eval("SignatureFailCount")%></td>
							</tr>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						</table>
					</FooterTemplate>
				</asp:Repeater>
			</td><td valign="top" style="padding-left:30px;">
				<b>Orange Settings</b><br /><br />
				Phone Number: <br /><asp:TextBox runat="server" ID="txtPhoneNumber" /><br /><asp:Literal runat="server" ID="lblMessage" EnableViewState="false" /><br /><br />
				Next Sync: <asp:Literal runat="server" ID="ltNextSync" /><br /><br />
				<table>
					<tr>
						<th>Service</th>
						<th>Current</th>
						<th>Update</th>
					</tr>
					<tr>
						<td>Main</td>
						<td><asp:CheckBox ID="chkServiceMain_Confirm" runat="server" Enabled="false" /></td>
						<td><asp:CheckBox ID="chkServiceMain" runat="server" /></td>
					</tr>
					<tr>
						<td>Invoices</td>
						<td><asp:CheckBox ID="chkServiceInv_Confirm" runat="server" Enabled="false" /></td>
						<td><asp:CheckBox ID="chkServiceInv" runat="server" /></td>
					</tr>
					<tr>
						<td>Devices</td>
						<td><asp:TextBox ID="txtServiceDevices_Confirm" runat="server" Enabled="false" style="width:20px;" /></td>
						<td><asp:TextBox ID="txtServiceDevices" runat="server" style="width:20px;" /></td>
					</tr><tr>
						<td colspan="3">
							<asp:Button ID="btnUpdateOrangeStatus" runat="server" OnCommand="OnUpdateOrangeStatus_Command" Text="Update" style="float:right;" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
    </form>
</body>
</html>
