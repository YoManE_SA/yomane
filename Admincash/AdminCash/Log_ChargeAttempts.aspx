<%@ Page Language="VB" ValidateRequest="false" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    Dim sSQL, sQueryWhere, sFromDate, sToDate, sDateDiff, sStatusColor, sAnd, sQueryString, sTmpTxt, sDivider, sRC As String, i, nTmpNum As Integer

    Function DisplayContent(ByVal sVar As String) As String
        DisplayContent = "<span class=""key"">" & Replace(Replace(dbPages.DBTextShow(sVar), "=", "</span> = "), "|", " <br /><span class=""key"">")
    End Function

    Function FormatData(sVar As Object) As String
        If IsDBNull(sVar) Then Return String.Empty
        Dim strValue As String = sVar.ToString() 'CType(sVar, String)
        If String.IsNullOrEmpty(strValue) Then Return String.Empty
        Return strValue.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&", " &amp;").Replace(">", "&gt;").Replace("<", " &lt;")
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Security.CheckPermission(lblPermissions)
        sAnd = String.Empty

        sFromDate = Trim(Request("fromDate")) & " " & Trim(Request("fromTime"))
        If sFromDate <> " " Then sQueryWhere &= sAnd & "(tblLogChargeAttempts.Lca_DateStart >= '" & sFromDate.ToString() & "')" : sAnd = " AND "
        sToDate = Trim(Request("toDate")) & " " & Trim(Request("toTime"))
        If sToDate <> " " Then sQueryWhere &= sAnd & "(tblLogChargeAttempts.Lca_DateStart <= '" & sToDate.ToString() & "')" : sAnd = " AND "

        If Security.IsLimitedMerchant Then
            sQueryWhere &= sAnd & " tblCompany.ID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
            sAnd = " AND "
        End If
        If Trim(Request("iTransNum")) <> "" Then
            sQueryWhere &= sAnd & "(tblLogChargeAttempts.Lca_TransNum = " & Trim(Request("iTransNum")) & ")" : sAnd = " AND "
            Select Case Trim(Request("iTransTbl"))
                Case "pass" : sQueryWhere &= sAnd & "(tblLogChargeAttempts.Lca_ReplyCode = '000') " : sAnd = " AND "
                Case "fail" : sQueryWhere &= sAnd & "(tblLogChargeAttempts.Lca_ReplyCode <> '000') AND (tblLogChargeAttempts.Lca_ReplyCode <> '001')" : sAnd = " AND "
            End Select
        End If
        If Trim(Request("CompanyID")) <> "" Then sQueryWhere &= sAnd & "(tblCompany.id = " & Trim(Request("CompanyID")) & ")" : sAnd = " AND "
        If Trim(Request("iMerchantID")) <> "" Then sQueryWhere &= sAnd & "(tblCompany.id = " & Trim(Request("iMerchantID")) & ")" : sAnd = " AND "

        sRC = Request("iReplyCode")
        If Not String.IsNullOrEmpty(sRC) Then
            sQueryWhere &= sAnd & "tblLogChargeAttempts.Lca_ReplyCode" & IIf(sRC.ToLower = "null" Or sRC = "---", " IS NULL", "='" & sRC & "'")
            sAnd = " AND "
        End If

        If Trim(Request("iRemoteAddress")) <> "" Then sQueryWhere &= sAnd & "(tblLogChargeAttempts.Lca_RemoteAddress = '" & Trim(Request("iRemoteAddress")) & "')" : sAnd = " AND "
        If Trim(Request("iFreeText")) <> "" Then
            sQueryWhere &= sAnd & "((tblLogChargeAttempts.Lca_RequestForm LIKE '%" & Trim(Request("iFreeText")) & "%')"
            sQueryWhere &= " OR (tblLogChargeAttempts.Lca_SessionContents LIKE '%" & Trim(Request("iFreeText")) & "%')"
            sQueryWhere &= " OR (tblLogChargeAttempts.Lca_QueryString LIKE '%" & Trim(Request("iFreeText")) & "%')"
            sQueryWhere &= " OR (tblLogChargeAttempts.Lca_RequestString LIKE '%" & Trim(Request("iFreeText")) & "%')"
            sQueryWhere &= " OR (tblLogChargeAttempts.Lca_ResponseString LIKE '%" & Trim(Request("iFreeText")) & "%'))"
        End If
        If Trim(Request("iProcessDuration")) <> "" Then
            Dim mode As String = IIf(Request("sDurationFilterMode") = "greaterThan", ">", "<")
            sQueryWhere &= sAnd & " DATEDIFF(SECOND, Lca_DateStart, Lca_DateEnd) " & mode & " " & Request("iProcessDuration")
        End If
        If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") _
         Else PGData.PageSize = 15
        sQueryString = dbPages.CleanUrl(Request.QueryString)
        If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere
        If Request("SQL2") = "1" Then
            dbPages.CurrentDSN = 2
            Response.Write(dbPages.ShowCurrentDSN)
        Else
            dbPages.CurrentDSN = 1
        End If
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Charge Attempt Log</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading {text-decoration: underline;white-space: nowrap;}
	</style>
	<script language="JavaScript">
		function IconVisibility(fVar)
		{
			spanObj = document.getElementById(fVar);
			spanObj.style.visibility = (spanObj.style.visibility == "hidden" ? "visible" : "hidden");
		}

		function ExpandNode(fTrNum)
		{
			trObj = document.getElementById("trInfo" + fTrNum)
			imgObj = document.getElementById("oListImg" + fTrNum)
			if (trObj)
			{
				if (trObj.style.display == '')
				{
					imgObj.src = '../images/tree_expand.gif';
					trObj.style.display = 'none';
				}
				else
				{
					imgObj.src = '../images/tree_collapse.gif';
					trObj.style.display = '';
				}
			}
		}
	</script>
</head>
<body>
	<form id="Form1" runat="server">
	<table align="center" style="width: 92%">
		<tr>
			<td colspan="2"><asp:Label ID="lblPermissions" runat="server" /><span id="pageMainHeading">CHARGE ATTEMPT LOG</span><br /><br /></td>
		</tr>
		<tr>
			<td style="font-size: 11px; padding-bottom: 4px;">
				(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');"> Collapse All</a>)
			</td>
			<td align="right" style="font-size: 11px;">
				Filters:
				<%
					If Trim(Request("iMerchantID")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iMerchantID", Nothing) & "';"">Merchant</a>") : sDivider = ", "
					If Trim(Request("iRemoteAddress")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iRemoteAddress", Nothing) & "';"">IP Address</a>") : sDivider = ", "
					If Trim(Request("iReplyCode")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iReplyCode", Nothing) & "';"">Reply</a>") : sDivider = ", "
					Response.Write(IIf(String.IsNullOrEmpty(sDivider), "Add to filter by clicking on returned fields", " | (click to remove)"))
				%>
			</td>
		</tr>
	</table>
	<table class="formNormal" align="center" style="width: 92%">
		<tr>
			<th colspan="2" style="width: 30px;">&nbsp;</th>
			<th>ID</th>
			<th>Date (Duration)</th>
			<th>Merchant</th>
			<th>IP Address</th>
			<th>Page Source</th>
			<th>Reply (Desc)</th>
			<th>Trans Id</th>
		</tr>
		<%
		    sSQL = "SELECT LogChargeAttempts_id, Lca_RemoteAddress, Lca_RequestMethod, Lca_LocalAddr, Lca_PathTranslate, Lca_HttpHost, Lca_HttpReferer, " & _
		     "Lca_QueryString, Lca_RequestForm, Lca_SessionContents, IsNull(Lca_ReplyCode, '') Lca_ReplyCode, " & _
		     "IsNull(Lca_ReplyDesc,'') Lca_ReplyDesc, Lca_TransNum, Lca_DateStart, Lca_DateEnd, IsNull(Lca_TimeString,'') Lca_TimeString, " & _
		     "tblCompany.ID AS CompanyID, Lca_RequestString, Lca_ResponseString, Name TransSource, CompanyName " & _
		     "FROM List.TransSource RIGHT JOIN tblLogChargeAttempts ON List.TransSource.TransSource_id=TransactionType_id " & _
		     "LEFT JOIN tblCompany ON Right('0000000'+LTrim(RTrim(Str(Lca_MerchantNumber))), 7)=CustomerNumber" & _
		     " " & sQueryWhere & " ORDER BY Lca_DateStart DESC"
		PGData.OpenDataset(sSQL)
		While PGData.Read()
			i += 1
			sStatusColor = IIf(PGData("Lca_ReplyCode") = "000", "#66cc66", "#ff6666")
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
				<td>
					<img onclick="ExpandNode('<%=i%>');" style="cursor: pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" /><br />
				</td>
				<td><span style="background-color: <%= sStatusColor %>;">&nbsp;</span></td>
				<td><%=PGData("LogChargeAttempts_id")%></td>
				<td nowrap="nowrap">
					<%
					Response.Write(CType(PGData("Lca_DateStart"), DateTime).ToString("dd/MM/yy HH:mm:ss"))
					If Not IsDBNull(PGData("Lca_DateStart")) And Not IsDBNull(PGData("Lca_DateEnd")) Then
						Response.Write(" (" & DateDiff(DateInterval.Second, CType(PGData("Lca_DateStart"), DateTime), CType(PGData("Lca_DateEnd"), DateTime)) & " s)<br />")
					End If
					%>
				</td>
				<td style="white-space: nowrap;">
					<%
					If Not IsDBNull(PGData("CompanyName")) Then
						If Trim(Request("iMerchantID")) <> "" Then Response.Write(PGData("CompanyName")) _
							Else dbPages.showFilter(PGData("CompanyName"), "?" & sQueryString & "&iMerchantID=" & PGData("CompanyID"), 1)
					End If
					%><br />
				</td>
				<td>
					<%
					If Trim(Request("iRemoteAddress")) <> "" Then Response.Write(PGData("Lca_RemoteAddress")) _
						Else dbPages.showFilter(PGData("Lca_RemoteAddress"), "?" & sQueryString & "&iRemoteAddress=" & PGData("Lca_RemoteAddress"), 1)
					%>
				</td>
				<td><%=PGData("TransSource")%><br /></td>
				<td>
					<%
					If Trim(PGData("Lca_ReplyCode")) <> "" Then
						If Trim(PGData("Lca_ReplyCode")) <> "000" And Trim(PGData("Lca_ReplyDesc")) <> "" Then
							sTmpTxt = PGData("Lca_ReplyCode") & " (<a style=""cursor:help"" title=""" & PGData("Lca_ReplyDesc") & """>?</a>)" : nTmpNum = 4
						Else
							sTmpTxt = PGData("Lca_ReplyCode") : nTmpNum = 1
						End If
						If Trim(Request("iReplyCode")) <> "" Then Response.Write(sTmpTxt) _
							Else dbPages.showFilter(sTmpTxt, "?" & sQueryString & "&iReplyCode=" & PGData("Lca_ReplyCode"), nTmpNum)
					Else
						Response.Write("---")
					End If
					%>
				</td>
				<td><%=PGData("Lca_TransNum")%><br /></td>
			</tr>
			<tr id="trInfo<%=i%>" style="display: none;">
				<td colspan="2"></td>
				<td colspan="7">
					<table class="formNormal" style="width: 100%; border-top: 1px dashed #e2e2e2;">
						<tr><td height="10"></td></tr>
						<tr>
							<%
                                If Not IsDBNull(PGData("Lca_RequestForm")) Then
                                    Response.Write("<td valign=""top"">")
                                    Response.Write("<span class=""dataHeading"">Request Form</span><br />")
                                    Response.Write(DisplayContent(PGData("Lca_RequestForm")))
                                    Response.Write("</td>")
                                End If
                                If Not IsDBNull(PGData("Lca_QueryString")) Then
                                    Response.Write("<td valign=""top"">")
                                    Response.Write("<span class=""dataHeading"">Query String</span><br />")
                                    Response.Write(DisplayContent(PGData("Lca_QueryString")))
                                    Response.Write("</td>")
                                End If
                                Response.Write("<td valign=""top"">")
                                If Not IsDBNull(PGData("Lca_SessionContents")) Then
                                    Response.Write("<span class=""dataHeading"">Session Contents</span><br />")
                                    Response.Write(DisplayContent(PGData("Lca_SessionContents")) & "</span><br /><br />")
                                End If
                                If Not IsDBNull(PGData("Lca_HttpReferer")) Then
                                    Response.Write("<span class=""dataHeading"">Http Referer</span><br />")
                                    Response.Write(DisplayContent(PGData("Lca_HttpReferer")) & "</span><br /><br />")
                                End If
                                If Not IsDBNull(PGData("Lca_HttpHost")) Then
                                    Response.Write("<span class=""dataHeading"">Http Host</span><br />")
                                    Response.Write(DisplayContent(PGData("Lca_HttpHost")) & "</span><br /><br />")
                                End If
                                If Not IsDBNull(PGData("Lca_RequestMethod")) Then
                                    Response.Write("<span class=""dataHeading"">Request Method</span><br />")
                                    Response.Write(DisplayContent(PGData("Lca_RequestMethod")) & "</span><br /><br />")
                                End If
                                If Not IsDBNull(PGData("Lca_LocalAddr")) Then
                                    Response.Write("<span class=""dataHeading"">Local Address</span><br />")
                                    Response.Write(DisplayContent(PGData("Lca_LocalAddr")) & "</span><br /><br />")
                                End If
                                Response.Write("</td>")
                                Response.Write("<td valign=""top"">")
                                If Not IsDBNull(PGData("Lca_TimeString")) Then
                                    Response.Write("<span class=""dataHeading"">Time String</span><br />")
                                    Response.Write(DisplayContent(PGData("Lca_TimeString")) & "</span><br /><br />")
                                End If
                                Response.Write("</td>")
                                'Response.Write("<td valign=""top"">")
                                'If Not IsDBNull(PGData("Lca_RequestString")) Then
                                '	Response.Write("<span class=""dataHeading"">Request String</span><br />")
                                '	Response.Write("<a style=""color:#006699;"" href=""#"" title=""" & PGData("Lca_RequestString") & """>Show</a> (" & Len(PGData("Lca_RequestString")) & " Chr)<br />")
                                'End If
                                'If Not IsDBNull(PGData("Lca_ResponseString")) Then
                                '	Response.Write("<br /><span class=""dataHeading"">Response String</span><br />")
                                '	Response.Write("<a style=""color:#006699;"" href=""#"" title=""" & PGData("Lca_ResponseString") & """>Show</a> (" & Len(PGData("Lca_ResponseString")) & " Chr)<br />")
                                'End If
                                'Response.Write("</td>")
							%>
						</tr>
						<tr><td><br /></td></tr>
						<tr>
							<td colspan="5">
								<span class="dataHeading">Request String</span><br />
								<div style="border:dotted 1px gray; width:100%; overflow:auto;">
									<%= FormatData(PGData("Lca_RequestString")) %>
								</div>
							</td>
						</tr>
						<tr><td><br /></td></tr>
						<tr>
							<td colspan="5">
								<span class="dataHeading">Response String</span><br />
								<div style="border:dotted 1px gray; width:100%; overflow:auto;">
									<%= FormatData(PGData("Lca_ResponseString")) %>
								</div>
							</td>
						</tr>
						<tr><td height="10"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td height="1" colspan="9" bgcolor="silver"></td></tr>
			<%
                End While
                PGData.CloseDataset()
		%>
	</table>
	<script language="JavaScript">
			function ExpandAll(fDisplay, fImgStatus) {
				for(i = 0; i <= <%= i %>; i++) {
					trObj = document.getElementById("trInfo" + i)
					imgObj = document.getElementById("oListImg" + i)
					if (trObj) {
						imgObj.src = '../images/tree_' + fImgStatus + '.gif';
						trObj.style.display = fDisplay;
					}
				}
			}
	</script>
	<br />
	<div id="filterIcon" style="visibility: hidden; position: absolute; z-index: 1; font-size: 10px; color: Gray; background-color: White;">
		<img src="../images/icon_filterPlus.gif" align="middle">ADD TO FILTER
	</div>
	</form>
	<table align="center" style="width: 92%">
		<tr><td><UC:Paging runat="Server" ID="PGData" PageID="PageID" /></td></tr>
	</table>
</body>
</html>
