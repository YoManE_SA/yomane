<%@ Page Language="C#" %>
<script runat="server" >
	private UserType RefType;
	private int RefID;
	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);
		RefID = Request["RefID"].ToNullableInt().GetValueOrDefault();
		RefType = Request["RefType"].ToNullableEnumByValue<UserType>().GetValueOrDefault();
		if (string.IsNullOrEmpty(Request["ref"]))
		{
			if(RefType == UserType.Merchant || RefType == UserType.MerchantLimited)
				hlBack.NavigateUrl = "merchant_dataUser.asp?companyID=" + RefID;
			else if (RefType == UserType.Customer)
				hlBack.NavigateUrl = "Customers/Data.aspx?id=" + RefID;
		} else hlBack.NavigateUrl = Request["ref"];
	}
	
	protected void SetPassword_Click(object sender, EventArgs e)
	{
		if (txtPassword.Text != txtPasswordConf.Text){
			ltError.Text = "Failed - Password not verified correctly";
			return;
		}
		Netpay.Infrastructure.Security.Login login = null;
		Netpay.Bll.Accounts.Account accObj = null;
		switch(RefType){
		case UserType.Customer: 
			accObj = Netpay.Bll.Customers.Customer.Load(RefID); 
			if(accObj.Login.LoginID == 0) (accObj as Netpay.Bll.Customers.Customer).Save();
			break;
		case UserType.Merchant: 
			accObj = Netpay.Bll.Merchants.Merchant.Load(RefID);
			if (accObj.Login.LoginID == 0) (accObj as Netpay.Bll.Merchants.Merchant).Save();
			break;
		case UserType.Affiliate: 
			accObj = Netpay.Bll.Affiliates.Affiliate.Load(RefID);
			if (accObj.Login.LoginID == 0) (accObj as Netpay.Bll.Affiliates.Affiliate).Save();
			break;
		case UserType.SystemAdmin:
		case UserType.SystemUser:
			var user = Netpay.Infrastructure.Security.AdminUser.Load(RefID);
			if (user.Login.LoginID == 0) (user as Netpay.Infrastructure.Security.AdminUser).Save();
			login = user.Login;
			break;
		}
		if (accObj != null) login = accObj.Login;
		if (login != null) ltError.Text = login.SetPassword(txtPassword.Text, Request.UserHostAddress).ToString();
	}
</script>
<html>
<head>
	<title>Change Password - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
</head>
<body dir="ltr">
	<form runat="server" method="post">
	<table align="center" border="0" cellpadding="1" dir="ltr" cellspacing="2" width="95%">
		<tr>
			<td id="pageMainHeadingTd">
				<span id="pageMainHeading">Change Password</span>
			</td>
			<tr>
	</table>
	<table align="center" border="0" cellpadding="1" dir="ltr" cellspacing="2" width="90%">
		<tr>
			<td>
				<br />
				<table cellpadding="2" cellspacing="2">
					<tr>
						<td class="txt12" align="left">New password</td>
						<td><asp:TextBox runat="server" TextMode="password" ID="txtPassword" dir="ltr" CssClass="input2" Style="font-size: 11px; width: 128px;" /></td>
					</tr>
					<tr>
						<td class="txt12" align="left">Confirm new password</td>
						<td><asp:TextBox runat="server" TextMode="password" ID="txtPasswordConf" dir="ltr" CssClass="input2" Style="font-size: 11px; width: 128px;" /></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<asp:Button runat="server" Text=" SUBMIT " OnClick="SetPassword_Click" />
						</td>
					</tr>
				</table>
				<br />
				<asp:Label runat="server" ID="ltError" Style="font-size: 12px;" />
				<br />
			</td>
		</tr>
		<tr>
			<td>
				[<asp:HyperLink runat="server" ID="hlBack" CssClass="submenu2" Style="text-decoration: underline;" Text="Return to card details" />]<br />
			</td>
		</tr>
	</table>
	</form>
</body>
</html>
