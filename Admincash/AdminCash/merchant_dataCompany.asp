<%
response.buffer = true
nCompanyID = TestNumVar(Request("companyID"), 0, -1, 0)
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/func_controls.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
If trim(request("Action"))="update" Then
	sGroupID = TestNumVar(request("GroupID"), 1, 0, 0)
	If sGroupID < 1 Then sGroupID = "NULL"
	sSQL="UPDATE tblCompany SET " &_
	" ParentCompany=" & TestNumVar(request("ParentCompany"), 1, 0, "IsNull((SELECT TOP 1 ID FROM tblParentCompany ORDER BY pc_IsDefault DESC, pc_Code), 0)") & "," &_
	" CompanyName=Left('" & DBText(request("CompanyName")) & "', 200)," &_
	" CompanyLegalName=Left('" & DBText(request("CompanyLegalName")) & "', 200)," &_
	" CompanyLegalNumber=Left('" & DBText(request("CompanyLegalNumber")) & "', 15)," &_
	" CompanyStreet=Left('" & DBText(request("CompanyStreet")) & "', 500)," &_
	" CompanyCity=Left('" & DBText(request("CompanyCity")) & "', 100)," &_
	" CompanyZIP=Left('" & DBText(request("CompanyZIP")) & "', 25)," &_
	" CompanyCountry=CASE WHEN " & DBText(request("CompanyCountryID")) & "=0 THEN NULL ELSE " & DBText(request("CompanyCountryID")) & " END," &_
	" CompanyPhone=Left('" & DBText(request("CompanyPhone")) & "', 50)," &_
	" CompanyFax=Left('" & DBText(request("CompanyFax")) & "', 50)," &_
	" CompanyIndustry_id=" & DBText(request("CompanyIndustry_id")) & "," &_
	" MerchantLinkName='" & DBText(request("MerchantLinkName")) & "'," &_
	" Url=Left('" & DBText(request("Url")) & "', 500)," &_
	" BillingFor=Left('" & DBText(request("BillingFor")) & "', 200)," &_
	" comment=Left('" & DBText(request("comment")) & "', 50)," &_
	" ChargebackNotifyMail=Left('" & DBText(request("ChargebackNotifyMail")) & "', 100)," &_
	" merchantSupportEmail=Left('" & DBText(request("merchantSupportEmail")) & "', 80)," &_
	" merchantSupportPhoneNum=Left('" & DBText(request("merchantSupportPhoneNum")) & "', 20)," &_
	" merchantOpenningDate='" & DBText(request("merchantOpenningDate")) & "'," &_
	" merchantClosingDate='" & DBText(request("merchantClosingDate")) & "'," &_
	" CustomerPurchasePayerIDText=Left('" & dbText(request("CustomerPurchasePayerIDText")) & "', 150)," &_
	" descriptor=Left('" & DBText(request("descriptor")) & "', 30)," &_
	" ReferralID=" & int(request("ReferralID")) & "," &_
	" GroupID=" & sGroupID & "," &_
	" AlertEmail='" & Replace(Request("AlertEmail"), "'", "''") & "'," &_
	" ContactName='" & Replace(Request("ContactName"), "'", "''") & "'," &_
	" ContactMail='" & Replace(Request("ContactMail"), "'", "''") & "'," &_
	" careOfAdminUser=Left('" & DBText(request("careOfAdminUser")) & "', 50)," &_
	" MerchantDepartment_id=" & IIF(TestNumVar(Request("MerchantDepartment_id"), 0, -1, 0) <> 0, Request("MerchantDepartment_id"), "Null") & _
	" WHERE ID=" & nCompanyID
	oledbData.Execute sSQL

    ' update account table
    sSql = "UPDATE [Data].[Account] SET [Name] = Left('" & DBText(request("CompanyName")) & "', 200) WHERE [Merchant_id] = " & nCompanyID
    oledbData.Execute sSQL
End if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css"/>
	<link rel="stylesheet" type="text/css" media="all" href="../StyleSheet/calendar-blue.css" title="winter" />
	<script type="text/javascript" src="../js/func_common.js"></script>

    <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/jquery-ui-1.11.4.custom/jquery-ui.css" />
    <script type="text/javascript">
        $(function () {
            $("[name='merchantOpenningDate']").datepicker();
            $("[name='merchantClosingDate']").datepicker();
        });
    </script>

<!--	
    <script type="text/javascript" src="../js/calendar.js"></script>
	<script type="text/javascript" src="../js/calendar-en.js"></script>
	<script type="text/javascript" src="../js/calendar-setup.js"></script>	
-->
	<style type="text/css">
		input.inputData { width:90%; }
	</style>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<%
sSQL="SELECT tblCompany.* FROM tblCompany WHERE tblCompany.ID=" & request("companyID")
set rsData = oledbData.Execute(sSQL)
if not rsData.EOF then

	sSQL="SELECT CountryID AS ID, Name AS CountryName FROM [List].[CountryList] ORDER BY Name"
	set rsData4 = oledbData.execute(sSQL)
	sSQL="SELECT ID, pc_Code FROM tblParentCompany ORDER BY pc_IsDefault DESC, pc_Code"
	set rsData8 = oledbData.Execute(sSQL)
	
	sLoginURL = session("TempOpenURL")
	If trim(COMPANY_NAME_1) = "Netpay" then
		sLoginURL = sLoginURL & "site_heb/"
	Elseif trim(COMPANY_NAME_1) = "AsTech" then
		sLoginURL = sLoginURL & "site_eng/"
	End if
	%>
	
	<table align="center" border="0" cellpadding="0" cellspacing="2" dir="ltr" width="95%">
	<tr>
		<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" dir="ltr" width="100%">
			<!--#include file="Merchant_dataHeaderInc.asp"-->
			<%
			pageSecurityLevel = 0
			pageSecurityLang = "ENG"
			pageClosingTags = "</table></td></tr></table>"
			Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
			%>
			<tr><td height="20"></td></tr>
			<!--#include file="Merchant_dataMenuInc.asp"-->
			</table>
		</td>
	</tr>
	<tr><td><br/></td></tr>
	</table>
	<form action="merchant_dataCompany.asp" name="frmCoopDetail" method="post" onsubmit="return ValidateMerchantData();">
	<input type="Hidden" name="companyID" value="<%= request("companyID") %>">
	<input type="hidden" name="Action" value="update">
	<table align="center" border="0" cellpadding="0" cellspacing="2" dir="rtl" width="95%">
	<tr><td height="10"></td></tr>
	<script language="javascript" type="text/javascript">
		function ValidateMerchantData()
		{
			if (document.frmCoopDetail.CompanyLegalName.value=='')
			{
				document.frmCoopDetail.CompanyLegalName.parentElement.parentElement.firstChild.style.backgroundColor="#ffe6e6";
				document.frmCoopDetail.CompanyLegalName.parentElement.parentElement.firstChild.style.color="Maroon";
				document.frmCoopDetail.CompanyLegalName.parentElement.style.backgroundColor="#ffe6e6";
				alert ("Company Legal Name is mandatory field!\n\nThe changes are not saved!");
				document.frmCoopDetail.CompanyLegalName.focus();
				return false;
			}
			return true;
		}
	</script>
	<tr>
		<td>
			<table align="center" border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
			<tr>
				<td colspan="4" class="MerchantSubHead">Company Details<br/></td>
			</tr>
			<tr>
				<td class="txt11" width="20%">Company name</td>
				<td class="txt11" width="30%">
					<input type="text" class="inputData" name="CompanyName" value="<%= rsData("CompanyName") %>">
					<input type="hidden" name="OldCompanyName" value="<%= rsData("CompanyName") %>">
				</td>
				<td class="txt11" width="20%">Merchant number</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" disabled name="CustomerNumber" value="<%= rsData("CustomerNumber") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Legal name</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="CompanyLegalName" value="<%= rsData("CompanyLegalName") %>">
				</td>
				<td class="txt11">Legal number</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="CompanyLegalNumber" value="<%= rsData("CompanyLegalNumber") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Street address</td>
				<td class="txt11" dir="ltr">
					<input type="text" class="inputData" name="CompanyStreet" value="<%= rsData("CompanyStreet") %>">
				</td>
				<td class="txt11">Billing name</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="BillingFor" value="<%= rsData("BillingFor") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">City</td>
				<td class="txt11" dir="ltr">
					<input type="text" class="inputData" name="CompanyCity" value="<%= rsData("CompanyCity") %>">
				</td>
				<td class="txt11">Phone</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="CompanyPhone" value="<%= rsData("CompanyPhone") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Zipcode</td>
				<td class="txt11">
					<input type="text" class="inputData" name="CompanyZIP" value="<%= rsData("CompanyZIP") %>">
				</td>
				<td class="txt11">Fax</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="CompanyFax" value="<%= rsData("CompanyFax") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11" valign="top">Country</td>
				<td class="txt11" valign="top">
					<%
					if not rsData4.EOF then
						PutRecordsetComboDefaultNEW rsData4, "companyCountryID"" dir=""ltr"" class=""inputData", "ID", "CountryName", "0", "", trim(rsData("companyCountry"))
						rsData4.movefirst
					end if
					%>
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr><td colspan="4"><br /><hr width="100%" size="1" style="border-bottom:dotted 1px #d2d2d2;"><br /></td></tr>
			<tr>
				<td class="txt11">Parent company</td>
				<td class="txt11">
					<%
					if not rsData8.EOF then
						PutRecordsetComboDefaultNEW rsData8, "ParentCompany"" dir=""ltr"" class=""inputData", "ID", "pc_Code", "", "- default -", trim(rsData("ParentCompany"))
						rsData8.movefirst
					end if
					%>
				</td>
				<td class="txt11" valign="top">
					Chargebacks mail
					<% showDivBox "help", "ltr", "300", "<ul style=""margin-left:10px;padding-left:10px;"" type=""square""><li>This email address is used when sending the merchant notifications about chargebacks.</li></ul>" %>	
				</td>
				<td class="txt11" valign="top">
					<input type="text" class="inputData" name="ChargebackNotifyMail" value="<%= rsData("ChargebackNotifyMail") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Industry</td>
				<td class="txt11">
					<%
					GetConstArray Server.CreateObject("ADODB.Recordset"), 51, 1, "", saIndustry, saColor
					%>
					<select id="CompanyIndustry_id" name="CompanyIndustry_id" class="inputData">
						<option value="0"></option>
						<%
						for i=LBound(saIndustry) to UBound(saIndustry)
							if saIndustry(i)<>"" then
								if i-rsData("CompanyIndustry_id")=0 then
									sSelected="selected=""selected"""
								else
									sSelected=""
								end if
								%>
								<option value="<%= i %>" <%= sSelected %> style="background-color:<%= saColor(i) %>">
									<%= saIndustry(i) %>
								</option>
								<%
							end if
						next
						%>
					</select>
					<!--<% call DrawComboConst("tmpRS", 51, 1, false, "CompanyIndustry_id",  "dir=""ltr"" class=""inputData"" style=""width:150px;""", rsData("CompanyIndustry_id"), "0", "") %>-->
				</td>
				<td class="txt11">
					System alert mail
					<% showDivBox "help", "ltr", "300", "<ul style=""margin-left:10px;padding-left:10px;"" type=""square""><li>This email address is used when sending the merchant system related notifications.<br />For example: schedule maintains, important changes, server/processing downtime.</li></ul>" %>
				</td>
				<td><input type="text" class="inputData" dir="ltr" name="AlertEmail" value="<%= rsData("AlertEmail") %>"></td>
			</tr>
			<tr>
				<td class="txt11">Account manager</td>
				<td class="txt11">
					<select name="careOfAdminUser" class="inputData">
						<option value=""></option>
						<%
						sSQL="SELECT su_Username, su_Name FROM tblSecurityUser ORDER BY CASE su_Username WHEN '" & DBText(rsData("careOfAdminUser")) & "' THEN 1 ELSE 2 END, su_Name"
						Set rsDataTmp=oledbData.Execute(sSQL)
						If Not rsDataTmp.EOF Then
							If trim(rsData("careOfAdminUser"))<>"" Then
								response.Write "<option value=""" & rsData("careOfAdminUser") & """ selected=""selected"">" & rsData("careOfAdminUser")
								If rsDataTmp("su_Username")=rsData("careOfAdminUser") Then
									Response.Write " (" & rsDataTmp("su_Name") & ")"
									rsDataTmp.MoveNext
								End If
								Response.Write "</option>"
							End If
							Do Until rsDataTmp.EOF
								response.Write "<option value=""" & rsDataTmp("su_Username") & """>" & rsDataTmp("su_Username") & " (" & rsDataTmp("su_Name") & ")</option>"
								rsDataTmp.MoveNext
							Loop
						End If
						rsDataTmp.Close
						Set rsDataTmp=Nothing
						%>
					</select>
				</td>
				<td class="txt11">
					Support mail
					<% showDivBox "help", "ltr", "300", "<ul style=""margin-left:10px;padding-left:10px;"" type=""square""><li>This support email address is displayed in the transaction mail sent after a sucessful charge.</li></ul>" %>
				</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="merchantSupportEmail" maxlength="80" value="<%= rsData("merchantSupportEmail") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Group</td>
				<td class="txt11">
					<%
						set rsDataTmp=oledbData.Execute("SELECT * FROM tblMerchantGroup ORDER BY mg_Name")
						If not rsDataTmp.EOF Then
							Call PutRecordsetCombodefaultNEW(rsDataTmp, "GroupID"" class=""inputData", "ID", "mg_Name", "", "", trim(rsData("GroupID")))
						End If
						rsDataTmp.close
					%>
				</td>
				<td class="txt11">
					Support phone
					<% showDivBox "help", "ltr", "300", "<ul style=""margin-left:10px;padding-left:10px;"" type=""square""><li>This support phone number is displayed in the transaction mail sent after a sucessful charge.</li></ul>" %>
				</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="merchantSupportPhoneNum" maxlength="20" value="<%= rsData("merchantSupportPhoneNum") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Link Name</td>
				<td class="txt11"><input type="text" class="inputData" dir="ltr" name="MerchantLinkName" value="<%=rsData("MerchantLinkName") %>" style="width:150px;" /></td>
			</tr>
			<tr>
				<td class="txt11">Department</td>
				<td class="txt11">
					<%
					Dim rsDept : Set rsDept = oleDbData.Execute("Select MerchantDepartment_id, Name From MerchantDepartment")
					If Not rsDept.EOF Then
						PutRecordsetComboDefaultNEW rsDept, "MerchantDepartment_id"" dir=""ltr"" class=""inputData", "MerchantDepartment_id", "Name", "0", "", trim(rsData("MerchantDepartment_id"))
					End If
					rsDept.Close()
					 %>
				</td>
			</tr>
			<%
			merchantOpenningDate = rsData("merchantOpenningDate")
			If trim(merchantOpenningDate) = "01/01/1900" Then merchantOpenningDate = ""
			merchantClosingDate = rsData("merchantClosingDate")
			If trim(merchantClosingDate) = "01/01/1900" Then merchantClosingDate = ""
			%>
			<tr>
				<td class="txt11">Open date</td>
				<td class="txt11">
					<input type="text" class="inputData" style="width:115px;" dir="ltr" name="merchantOpenningDate" maxlength="80" value="<%= merchantOpenningDate %>">
					<!--<img alt="Date selection" src="../images/calendar_icon.gif" id="OpenningDate_trigger" width="16" height="15" border="0" style="cursor:pointer;"/>-->
				</td>
				<td class="txt11" valign="top" rowspan="2">
					Website URL
					<% showDivBox "help", "ltr", "300", "<ul style=""margin-left:10px;padding-left:10px;"" type=""square""><li>If the merchant has more than one URL, type each URL in separate row.</li><li>Remember to include 'http://' in each URL.</li></ul>" %>
				</td>
				<td class="txt11" rowspan="2"><textarea class="inputData" dir="ltr" name="url" style="width:90%; height:48px;"><%= rsData("url") %></textarea></td>
			</tr>
			<tr>
				<td class="txt11">Close date</td>
				<td class="txt11">
					<input type="text" class="inputData" style="width:115px;" dir="ltr" name="merchantClosingDate" maxlength="80" value="<%= merchantClosingDate %>">
					<!--<img alt="Date selection" src="../images/calendar_icon.gif" id="Img2" width="16" height="15" border="0" style="cursor:pointer;"/>-->
				</td>
			</tr>
			<tr>
				<td class="txt11">Description<br />(for credit companies)</td>
				<td class="txt11" dir="ltr">
					<input type="text" class="inputData" name="descriptor" maxlength="150" value="<%= rsData("descriptor") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Activities</td>
				<td class="txt11" dir="ltr" rowspan="2">
					<textarea class="inputData" dir="ltr" name="comment" style="width:90%;height:40px;"><%= rsData("comment") %></textarea>
				</td>
				<td class="txt11">Contact Name</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="ContactName" maxlength="100" value="<%= rsData("ContactName") %>">
				</td>
			</tr>
			<tr>
				<td><br /></td>
				<td class="txt11">Contact Mail</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="ContactMail" maxlength="100" value="<%= rsData("ContactMail") %>">
				</td>
			</tr>
			</table>
			<script type="text/javascript">
                /*
			    Calendar.setup({
					inputField	:	"merchantOpenningDate",	 // id of the input field
					ifFormat	:	"%d/%m/%Y",	  // format of the input field
					button		:	"OpenningDate_trigger",  // trigger for the calendar (button ID)
					align		:	"Tr",			 // alignment (defaults to "Bl")
					singleClick	:	true,
				 mondayFirst	:	false,
				 weekNumbers	:	false
				}); 
				Calendar.setup({
					inputField	 :	"merchantClosingDate",	 // id of the input field
					ifFormat	 :	"%d/%m/%Y",	  // format of the input field
					button		 :	"ClosingDate_trigger",  // trigger for the calendar (button ID)
					align		 :	"Tr",			 // alignment (defaults to "Bl")
					singleClick	:	true,
				 mondayFirst	:	false,
				 weekNumbers	:	false
				}); 
                */
			</script>
		</td>
	</tr>
	<tr><td height="12"></td></tr>
	<tr><td bgcolor="#A9A7A2" height="1"></td></tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td align="right"><input type="submit" value="Save Changes" style="width:110px; height:auto; background-color:#f1f1f1;"></td>
	</tr>
	</table>
	</form>
	<%
end if
%>
</body>
<%
rsData.close
Set rsData = nothing
rsData4.close
Set rsData4 = nothing
rsData8.close
Set rsData8 = nothing
call closeConnection()
%>
</html>