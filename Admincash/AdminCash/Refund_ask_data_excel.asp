<%
Server.ScriptTimeout = 360
response.Expires = -1
%>
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_adoPaging.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/func_security.asp" -->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
	
	response.ContentType = "application/vnd.ms-excel"
	response.AddHeader "content-disposition", "attachment;filename=report.xls"
%>

<%
	sWhere = " WHERE (tblCompanyTransPass.PaymentMethod_id<=2) "
	sAnd = " AND "
	If Trim(request("source")) = "filter" Then
		If Trim(request("ShowCompanyID"))<>"" AND Trim(request("ShowCompanyID"))<>"null" Then sWhere=sWhere & sAnd & " (tblRefundAsk.CompanyID=" & trim(request("ShowCompanyID")) & ")" : sAnd=" AND "
		if trim(request("DebitCompanyID"))<>"" then sWhere=sWhere & sAnd & " tblDebitTerminals.DebitCompany IN (" & request("DebitCompanyID") & ")" : sAnd=" AND "
		if trim(request("TerminalNumber"))<>"" then sWhere=sWhere & sAnd & " tblDebitTerminals.TerminalNumber='" & DBText(request("TerminalNumber")) & "'" : sAnd=" AND "
		dDateMax = Trim(Request("toDate") & " " & Request("toTime"))
		dDateMin = Trim(Request("fromDate") & " " & Request("fromTime"))
		if IsDate(dDateMax) then sWhere=sWhere & sAnd & " (tblRefundAsk.RefundAskDate <='" & CDate(dDateMax) & "')" : sAnd=" AND "
		if IsDate(dDateMin) then sWhere=sWhere & sAnd & " (tblRefundAsk.RefundAskDate >='" & CDate(dDateMin) & "')" : sAnd=" AND "
		if trim(request("RefundAskStatus0"))<>"1" then sWhere = sWhere & sAnd & " (tblRefundAsk.RefundAskStatus<>0)" : sAnd=" AND "
		if trim(request("RefundAskStatus1"))<>"1" then sWhere = sWhere & sAnd & " (tblRefundAsk.RefundAskStatus<>1)" : sAnd=" AND "
		if trim(request("RefundAskStatus22"))<>"1" then sWhere = sWhere & sAnd & " (tblRefundAsk.RefundAskStatus<>2)" : sAnd=" AND "
		if trim(request("RefundAskStatus24"))<>"1" then sWhere = sWhere & sAnd & " (tblRefundAsk.RefundAskStatus<>4)" : sAnd=" AND "
		if trim(request("RefundAskStatus25"))<>"1" then sWhere = sWhere & sAnd & " (tblRefundAsk.RefundAskStatus<>5)" : sAnd=" AND "
		if trim(request("RefundAskStatus3"))<>"1" then sWhere = sWhere & sAnd & " (tblRefundAsk.RefundAskStatus<>3)" : sAnd=" AND "
	end if
	If PageSecurity.IsLimitedMerchant Then
		sWhere = sWhere & sAnd & " tblRefundAsk.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
		sAnd=" AND "
	End If
	If PageSecurity.IsLimitedDebitCompany Then
		sWhere = sWhere & sAnd & " tblDebitTerminals.DebitCompany IN (SELECT ID FROM GetPermittedDebitCompanies('" & PageSecurity.Username & "'))"
		sAnd=" AND "
	End If

	Set rsData = server.createobject("adodb.recordset")
	sSQL="SELECT tblCompany.CompanyName, tblCompanyTransPass.PaymentMethodDisplay, tblCompanyTransPass.TerminalNumber, tblCompanyTransPass.PaymentMethod, " &_
		"tblDebitCompany.debitCompany_id AS DebitCompanyID, tblCompanyTransPass.insertdate AS OriginalTransDate, tblRefundAsk.id, tblRefundAsk.CompanyID, tblRefundAsk.transID, tblRefundAsk.RefundAskAmount, tblRefundAsk.RefundAskCurrency, " &_
		"tblRefundAsk.RefundAskComment, tblRefundAsk.RefundAskDate, tblRefundAsk.RefundAskStatus, tblRefundAsk.RefundAskStatusHistory, tblRefundAsk.RefundFlag, tblCreditCard.BINCountry, " &_
		"dt_IsRefundBlocked, tblCreditCard.CCard_First6, tblCreditCard.CCard_Last4, tblDebitTerminals.dt_name, tblRefundAsk.id as RefundAskID, tblCompanyTransPass.DebitReferenceCode FROM tblDebitTerminals " &_
		"LEFT OUTER JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany = tblDebitCompany.debitCompany_id " &_
		"RIGHT OUTER JOIN tblCompanyTransPass ON tblDebitTerminals.terminalNumber = tblCompanyTransPass.TerminalNumber " &_
		"LEFT OUTER JOIN tblCreditCard ON(tblCompanyTransPass.CreditCardID = tblCreditCard.ID) " &_
		"RIGHT OUTER JOIN tblRefundAsk " &_
		"LEFT OUTER JOIN tblCompany ON tblRefundAsk.companyID = tblCompany.ID ON tblCompanyTransPass.ID = tblRefundAsk.transID " &_
		" " & sWhere & " ORDER BY tblRefundAsk.RefundAskDate DESC, tblRefundAsk.ID DESC"	
		
'response.Write sSQL		
	
	set rsData=oledbData.Execute(sSQL)	
	If not rsData.EOF then
		%>
			<table border="1" bordercolor="gray" cellspacing="1" cellpadding="1">	
				<tr  bgcolor="#edf662">
					<td>CC BIN</td>
					<td>CC Last 4</td>
					<td>Currency</td>
					<td align="right">Refund Amount</td>
					<td>Terminal Name</td>
					<td>Terminal Number</td>
					<td>Original Trans</td>
					<td>Debit Reference Code</td>
					<td>Date</td>
					<td>Action Log</td>					
				</tr>
				<%
				iCounter = 0
				sColor1  = "#f0f7a2"
				sColor2  = "#ffffff"
					do until rsData.EOF						
						if iCounter mod 2 =0 then
							response.Write "<tr bgcolor=""" & sColor1 & """>"
						else
							response.Write "<tr bgcolor=""" & sColor2 & """>"
						end if
						response.Write "<td align=""left"">" & rsData("CCard_First6") & "</td>"
						response.Write "<td align=""left"">" & rsData("CCard_Last4") & "</td>"
						response.Write "<td>" & GetCurText(rsData("RefundAskCurrency")) & "</td>"
						response.Write "<td align=""right"">" & FormatNumber(rsData("RefundAskAmount"), 2, True, False, True) & "</td>"
						response.Write "<td align=""left"">" & IIf(rsData("dt_name")<>"",DBText(rsData("dt_name")) ,"") & "</td>"
						response.Write "<td align=""left"">" & rsData("terminalNumber") & "</td>"
						response.Write "<td align=""left"">" & rsData("transID") & "</td>"
						response.Write "<td align=""left"">" & rsData("DebitReferenceCode") & "</td>"
						response.Write "<td align=""left"">" & FormatDatesTimes(rsData("OriginalTransDate"),1,1,0) & "</td>"
						if not isnull(rsData("RefundAskID")) then
							sTempSQL = "SELECT ral_date,ral_description FROM tblRefundAskLog WHERE refundAsk_id = " & rsData("RefundAskID") & " ORDER BY refundAskLog_id desc"
							Set rsDataTmp = oledbData.Execute(sTempSQL)
							If Not rsDataTmp.EOF Then
								response.Write "<td align=""left"">"
								Do until rsDataTmp.EOF
									Response.Write(rsDataTmp("ral_date") & "&nbsp;|&nbsp;")
									Response.Write(rsDataTmp("ral_description") & "<br />")
									rsDataTmp.movenext
								loop
								response.Write "</td>"
							Else
								response.Write "<td></td>"
							End If
							rsDataTmp.close
						else
							response.Write "<td></td>"
						end if
						response.Write "</tr>"
						rsData.movenext
						iCounter = iCounter + 1
					loop
				%>
			</table>
		<%
	End If
	CloseConnection
	response.End
%>
