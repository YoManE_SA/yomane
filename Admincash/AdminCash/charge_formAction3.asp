<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
sCompanyID = trim(request("CompanyID"))
sCustomerID = 0
sIP = DBText(Request.ServerVariables("REMOTE_ADDR"))
nAmount = Replace(DBText(request("Amount")),",","")
nTypeCredit = DBText(request("CreditType"))
nCurrency = DBText(request("Currency"))
sComment = DBText(request("Comment"))

' if no payment method specified, the default is 'admin'
nPaymentMethod=TestNumVar(request("PaymentMethod"), 1, 0, 2)
nPaymentMethodOld=TestNumVar(request("PaymentMethodOld"), 1, 0, 4)
sPaymentMethodDisplay=trim(DBText(request("PaymentMethodDisplay")))
if sPaymentMethodDisplay="" then sPaymentMethodDisplay="---"
bIsFrame = (trim(request("IsFrame")) = "1")


if TestNumVar(request("HandlingFee"), 0, 1, 0)=1 then
	sAmountField="netpayFee_transactionCharge" '"HandlingFee"
else
	sAmountField="Amount"
end if

Function CreateTransaction()
    sSQL = ""
	If UseLocalID Then sSQL = "Declare @curID as int = 1 + IsNull((Select MAX(ID) From tblCompanyTransPass Where ID > " & APP_MINID & " And ID < " & APP_MAXID & "), " & APP_MINID & ");"
	sSQL = sSQL & "INSERT INTO tblCompanyTransPass(" & IIF(UseLocalID, "ID, ", "") & "companyID, TransSource_id, CreditType, IPAddress, " & sAmountField & ", Currency, OCurrency, Comment, paymentMethod_Id, paymentMethodDisplay, MerchantPD, paymentMethod, UnsettledAmount, UnsettledInstallments)" &_
	" VALUES(" & IIF(UseLocalID, "@curID, ", "") & sCompanyID & ", 10, " & nTypeCredit & ", '" & sIP & "', " & nAmount & "," & nCurrency & "," & nCurrency & ", '" & sComment & "', " & nPaymentMethodOld & ", '" & sPaymentMethodDisplay & "', GETDATE(), " & nPaymentMethod & "," & IIF(nTypeCredit = "0", -nAmount, nAmount) & ", 1);"
	'" Select " & IIF(UseLocalID, "@curID", "IDENT_CURRENT('tblCompanyTransPass')")
	'response.Write sSQL
	CreateTransaction = ExecSql(sSQL)
End Function

xTransID = 0
If int(nAmount)>0 then
	If Trim(nTypeCredit) = "-1" Then
		nTypeCredit = 0
		xTransID = CreateTransaction()
		nTypeCredit = 1
		xTransID = CreateTransaction()
	Else
		xTransID = CreateTransaction()
	End If
End If

%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
     <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <style>
        body {font-family: 'Open Sans', sans-serif;}
       .bg-form {background: #f5f5f5; border: 1px solid #c0c0c0; padding: 10px;}
        h2 {margin: 0; padding: 0; font-size: 14px; padding: 10px 0; font-size: 14px; color: #2566AB;}

      
        hr {
            border-top: 1px solid #c2c2c2;
            border-bottom: none;
            border-left: none;
            border-right: none;
        }

        .container {
            width: 900px;
            font-size: 12px;
            margin: 0 auto;
        }

        .noerror {
          padding: 10px; color: #556652 !important; background-color: #D5FFCE; border: 1px solid #9ADF8F;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; font-size: 14px; line-height: 24px; margin-bottom: 10px;
        }

        .mb-10 {
            margin-bottom: 10px;
        }

        .pb-10 {
            padding-bottom: 10px;
        }

        .w-250 {
            width: 250px;
        }

        .w-80 {
            width: 80px;
        }
        .details {font-size: 14px;}
    </style>
</head>
<body bgcolor="#ffFFFF" leftmargin="0" topmargin="0" rightmargin="0" marginwidth="0" class="itextM" dir="ltr">

      <div class="container">
        <div class="mb-10">
            <span id="pageMainHeading">Virtual Terminal</span>
        </div>
        <div class="bg-form">
            

           <% If int(nAmount)>0 then %>
			<div class="noerror">
                Transaction was successfully
            </div>
		    <% if bIsFrame then %><div class="details mb-10"><%= sComment %></div>
		    <% end if %>

              <% end if %>

            <div class="details ">Company Number: <b> <%= dbTextShow(request("compname")) %></b> </div>


            <div style="text-align: center;"><img src="../images/cardadmin.gif" alt="" border="0"></div>
        </div>
    </div>
<% call closeConnection() %>
</body>
</html>