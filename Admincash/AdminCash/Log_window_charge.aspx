<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
	
	Dim sSQL As String, sQueryWhere As String, nFromDate As String, nToDate As String, sDateDiff As String, _
		sStatusColor As String, sAnd As String, sQueryString As String, sTmpTxt As String, sDivider As String
	Dim i As Integer, nTmpNum As Integer

	Sub DisplayContent(sVar As String)
		Dim sVarShow As String
		If Trim(PGData(sVar).ToString) <> "" Then
			If Trim(sVar) = "RequestQueryString" Then sVarShow = Replace(Trim(PGData(sVar)),"&"," &") _
				Else sVarShow = Trim(PGData(sVar).ToString)
			Response.Write("<span class=""key"">" & Trim(sVar) & ":</span> " & sVarShow & "<br />")
		End If
	End Sub
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblheader)
		If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere
		If Request("SQL2") = "1" Then
			dbPages.CurrentDSN = 2
			Response.Write(dbPages.ShowCurrentDSN)
		Else
			dbPages.CurrentDSN = 1
		End If
		Dim dDateMax As String = Trim(Request("toDate") & " " & Request("toTime"))
		Dim dDateMin As String = Trim(Request("fromDate") & " " & Request("fromTime"))
		If IsDate(dDateMax) Then sQueryWhere &= " AND tblWindowChargeTmpData.InsertDate<='" & dDateMax & "'"
		If IsDate(dDateMin) Then sQueryWhere &= " AND tblWindowChargeTmpData.InsertDate>='" & dDateMin & "'"
		If Trim(Request("iIPAddress")) <> "" Then sQueryWhere &= " AND tblWindowChargeTmpData.IPAddress ='" & dbPages.DBText(Trim(Request("iIPAddress"))) & "'"
		If Trim(Request("ReplyCode")) <> "" Then sQueryWhere &= " AND tblWindowChargeTmpData.ReplyCode ='" & dbPages.DBText(Trim(Request("ReplyCode"))) & "'"
		If Request("ShowCompanyID") <> String.Empty Then sQueryWhere &= " AND tblWindowChargeTmpData.CompanyID=" & dbPages.TestVar(Request("ShowCompanyID"), 0, -1, 0)
		If Security.IsLimitedMerchant Then sQueryWhere &= " AND tblWindowChargeTmpData.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
		If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") Else PGData.PageSize = 20
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		sQueryString = dbPages.SetUrlValue(sQueryString, "PageID", "1")
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript">
		function IconVisibility(fVar) {
		
			spanObj = document.getElementById(fVar)
			if (spanObj.style.visibility == 'hidden') {
				spanObj.style.visibility = 'visible';
			}
			else {
				spanObj.style.visibility = 'hidden';
			}
		}
		function ExpandNode(fTrNum) {
			trObj = document.getElementById("trInfo" + fTrNum)
			imgObj = document.getElementById("oListImg" + fTrNum)
			if (trObj) {
				if (trObj.style.display == '') {
					imgObj.src = '../images/tree_expand.gif';	
					trObj.style.display = 'none';
				}
				else {
					imgObj.src = '../images/tree_collapse.gif';	
					trObj.style.display = '';
				}
			}
		}
	</script>
</head>
<body>

	<form id="Form2" runat="server">
		<table align="center" style="width:92%">
		<tr>
			<td colspan="2" id="pageMainHeading">
				<asp:label ID="lblheader" runat="server" /> HOSTED PAGE<br /><br />
			</td>
		</tr>
		<tr>
			<td style="font-size:11px; padding-bottom:4px;">
				(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');">Collapse All</a>)
			</td>
			<td align="right" style="font-size:11px;">
				Filters:
				<%
				If Trim(Request("iInsertDate"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"iInsertDate",Nothing) &"';"">Insert Date</a>") : sDivider = ", "
				If Trim(Request("iMerchantID"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"iMerchantID",Nothing) &"';"">Merchant</a>") : sDivider = ", "
					If Trim(Request("iIPAddress")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iIPAddress", Nothing) & "';"">IP Address</a>") : sDivider = ", "
					If Trim(Request("ReplyCode")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "ReplyCode", Nothing) & "';"">Reply Code</a>") : sDivider = ", "
					
				If sDivider <>"" Then Response.Write(" | (click to remove)") _
					Else Response.Write("Add to filter by clicking on returned fields")
				%>
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%">
		<tr>
			<th colspan="2"><br /></th>
			<th>Id</th>
			<th>Date</th>
			<th>Merchant</th>
			<th>IP Address</th>
			<th>ReplyCode</th>
		</tr>
		<%
			sSQL = "SELECT " & IIf(dbPages.CurrentDSN = 2, "'xxxx'", "Right(dbo.GetDecrypted256(f_CCardNum256),4)") & " f_CCardNum, tblWindowChargeTmpData.*," & _
			" CompanyName FROM tblWindowChargeTmpData INNER JOIN tblCompany ON CompanyID=tblCompany.ID " & sQueryWhere & " ORDER BY InsertDate DESC"
			PGData.OpenDataset(sSQL)
			While PGData.Read()
				i += 1
				sStatusColor = IIf(Trim(PGData("ID")) = "000", "#66cc66", "#ff6666")
				%>
				<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
					<td><img onclick="ExpandNode('<%=i%>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle"><br /></td>
					<td><span Style="background-color:<%= sStatusColor %>;">&nbsp;</span></td>
					<td><%=PGData("id")%></td>
					<td nowrap="nowrap">				
						<%
						If Trim(Request("iInsertDate"))<>"" Then Response.Write(Trim(PGData("insertDate"))) _
							Else dbPages.showFilter(CType(Trim(PGData("insertDate")), DateTime).ToString("dd/MM/yy HH:mm"), "?" & sQueryString & "&iInsertDate=" & CType(Trim(PGData("insertDate")), DateTime).ToString("dd/MM/yy"), 1)
						%>  
					</td>
					<td style="white-space:nowrap;">
						<%
						If Trim(Request("iMerchantID"))<>"" Then Response.Write(PGData("CompanyID")) _
							Else dbPages.showFilter(PGData("CompanyID") & " " & PGData("CompanyName"), "?" & sQueryString & "&iMerchantID=" & PGData("CompanyID"), 1)
						%>
					</td>
					<td>
						<%
							If Trim(Request("IPAddress")) <> "" Then Response.Write(PGData("IPAddress")) _
								Else dbPages.showFilter(PGData("IPAddress"), "?" & sQueryString & "&iIPAddress=" & PGData("IPAddress"), 1)
						%>
					</td>
					<td><%=PGData("ReplyCode")%></td>
				</tr>
				<tr id="trInfo<%=i%>" style="display:none;">
					<td colspan="7" style="border-top:1px dashed #e2e2e2; padding-left:40px;">
						<table class="formNormal" style="width:100%;">
						<tr><td height="10"></td></tr>
						<tr>
							<%
							Response.Write("<td valign=""top"" nowrap>")
								DisplayContent("Guid")
								DisplayContent("referringUrl")
								DisplayContent("f_ExpMM")
								DisplayContent("f_ExpYY")
								DisplayContent("f_Cui")
								DisplayContent("f_MemberName")
								DisplayContent("f_PersonalNumber")
								DisplayContent("f_PhoneNumber")
								DisplayContent("f_Email")
								DisplayContent("f_BillingAddress")
								DisplayContent("f_BillingCity")
								DisplayContent("f_BillingZipCode")
								DisplayContent("f_BillingStateId")
								DisplayContent("f_BillingCountryId")
								DisplayContent("f_CCardNum")
							Response.Write("</td>")
							Response.Write("<td valign=""top"" nowrap>")
								DisplayContent("x_ReplyURL")
								DisplayContent("x_ReplyType")
								DisplayContent("x_TransType")
								DisplayContent("x_CreditType")
								DisplayContent("x_Amount")
								DisplayContent("x_Currency")
								DisplayContent("x_Payments")
								DisplayContent("x_PayFor")
								DisplayContent("x_PayerID")
								DisplayContent("x_Order")
								DisplayContent("x_Comment")
								DisplayContent("x_RecurringCount")
								DisplayContent("x_RecurringCycle")
								DisplayContent("x_RecurringMode")
								DisplayContent("x_Recurring")
							Response.Write("</td>")
							%>
						</tr>
						<tr><td height="10"></td></tr>
						</table>
					</td>
				</tr>
				<tr><td height="1" colspan="7" bgcolor="silver"></td></tr>
				<%
			End While
			PGData.CloseDataset()
		%>
		</table>
		<script language="JavaScript">
			function ExpandAll(fDisplay, fImgStatus) {
				for(i = 0; i <= <%= i %>; i++) {
					trObj = document.getElementById("trInfo" + i)
					imgObj = document.getElementById("oListImg" + i)
					if (trObj) {
						imgObj.src = '../images/tree_' + fImgStatus + '.gif';
						trObj.style.display = fDisplay;
					}
				}
			}
		</script>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" align="middle"> ADD TO FILTER
		</div>
		
	</form>
	
	<table align="center" style="width:92%">
	<tr>
		<td>
			<UC:Paging runat="Server" id="PGData" PageID="PageID" />
		</td>
	</tr>
	</table>
		
		
</body>
</html>