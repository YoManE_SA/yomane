<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Dim sSQL, sQueryWhere, sFromDate, sToDate, sDateDiff, sStatusColor, sAnd, sQueryString, sTmpTxt, sDivider, sRC As String, i, nTmpNum As Integer

	Function DisplayContent(ByVal sVar As String) As String
		DisplayContent = "<span class=""key"">" & Replace(Replace(dbPages.DBTextShow(sVar), "=", "</span> = "), "&", " <br /><span class=""key"">")
	End Function
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		sAnd = String.Empty

		sFromDate = Trim(Request("fromDate")) & " " & Trim(Request("fromTime"))
		If sFromDate <> " " Then sQueryWhere &= sAnd & "(tblLogPaymentPage.Lpp_DateStart >= '" & sFromDate.ToString() & "')" : sAnd = " AND "
		sToDate = Trim(Request("toDate")) & " " & Trim(Request("toTime"))
		If sToDate <> " " Then sQueryWhere &= sAnd & "(tblLogPaymentPage.Lpp_DateStart <= '" & sToDate.ToString() & "')" : sAnd = " AND "

		If Security.IsLimitedMerchant Then
			sQueryWhere &= sAnd & " tblCompany.ID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
			sAnd = " AND "
		End If
		If Trim(Request("iTransNum")) <> "" Then
			sQueryWhere &= sAnd & "(tblLogPaymentPage.Lpp_TransNum = " & Trim(Request("iTransNum")) & ")" : sAnd = " AND "
			Select Case Trim(Request("iTransTbl"))
				Case "pass" : sQueryWhere &= sAnd & "(tblLogPaymentPage.Lpp_ReplyCode = '000') " : sAnd = " AND "
				Case "fail" : sQueryWhere &= sAnd & "(tblLogPaymentPage.Lpp_ReplyCode <> '000') AND (tblLogPaymentPage.Lpp_ReplyCode <> '001')" : sAnd = " AND "
			End Select
		End If
		If Trim(Request("CompanyID")) <> "" Then sQueryWhere &= sAnd & "(tblCompany.id = " & Trim(Request("CompanyID")) & ")" : sAnd = " AND "
		If Trim(Request("iMerchantID")) <> "" Then sQueryWhere &= sAnd & "(tblCompany.id = " & Trim(Request("iMerchantID")) & ")" : sAnd = " AND "

		sRC = Request("iReplyCode")
		If Not String.IsNullOrEmpty(sRC) Then
			sQueryWhere &= sAnd & "tblLogPaymentPage.Lpp_ReplyCode" & IIf(sRC.ToLower = "null" Or sRC = "---", " IS NULL", "='" & sRC & "'")
			sAnd = " AND "
		End If

		If Trim(Request("iRemoteAddress")) <> "" Then sQueryWhere &= sAnd & "(tblLogPaymentPage.Lpp_RemoteAddress = '" & Trim(Request("iRemoteAddress")) & "')" : sAnd = " AND "
		If Trim(Request("iFreeText")) <> "" Then
			sQueryWhere &= sAnd & "((tblLogPaymentPage.Lpp_RequestForm LIKE '%" & Trim(Request("iFreeText")) & "%')"
			sQueryWhere &= " OR (tblLogPaymentPage.Lpp_SessionContents LIKE '%" & Trim(Request("iFreeText")) & "%')"
			sQueryWhere &= " OR (tblLogPaymentPage.Lpp_QueryString LIKE '%" & Trim(Request("iFreeText")) & "%'))"
		End If
		If Trim(Request("iProcessDuration")) <> "" Then
			Dim mode As String = IIf(Request("sDurationFilterMode") = "greaterThan", ">", "<")
			sQueryWhere &= sAnd & " DATEDIFF(SECOND, Lpp_DateStart, Lpp_DateEnd) " & mode & " " & Request("iProcessDuration")
		End If
		If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") _
		 Else PGData.PageSize = 15
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere
		If Request("SQL2") = "1" Then
			dbPages.CurrentDSN = 2
			Response.Write(dbPages.ShowCurrentDSN)
		Else
			dbPages.CurrentDSN = 1
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Payment Page Log</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading {text-decoration: underline;white-space: nowrap;}
	</style>
	<script language="JavaScript">
		function IconVisibility(fVar)
		{
			spanObj = document.getElementById(fVar);
			spanObj.style.visibility = (spanObj.style.visibility == "hidden" ? "visible" : "hidden");
		}

		function ExpandNode(fTrNum)
		{
			trObj = document.getElementById("Row" + fTrNum)
			imgObj = document.getElementById("Img" + fTrNum)
			if (trObj)
			{
				if (trObj.style.display == '')
				{
					imgObj.src = '../images/tree_expand.gif';
					trObj.style.display = 'none';
				}
				else
				{
					imgObj.src = '../images/tree_collapse.gif';
					trObj.style.display = '';
				}
			}
		}
	</script>
</head>
<body>
	<form id="Form1" runat="server">
	<table align="center" style="width: 92%">
		<tr>
			<td colspan="2"><asp:Label ID="lblPermissions" runat="server" /> <span id="pageMainHeading">PAYMENT PAGE LOG</span><br /><br /></td>
		</tr>
		<tr>
			<td style="font-size: 11px; padding-bottom: 4px;">
				(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');"> Collapse All</a>)
			</td>
			<td align="right" style="font-size: 11px;">
				Filters:
				<%
					If Trim(Request("iMerchantID")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iMerchantID", Nothing) & "';"">Merchant</a>") : sDivider = ", "
					If Trim(Request("iRemoteAddress")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iRemoteAddress", Nothing) & "';"">IP Address</a>") : sDivider = ", "
					If Trim(Request("iReplyCode")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iReplyCode", Nothing) & "';"">Reply</a>") : sDivider = ", "
					Response.Write(IIf(String.IsNullOrEmpty(sDivider), "Add to filter by clicking on returned fields", " | (click to remove)"))
				%>
			</td>
		</tr>
	</table>
	<table class="formNormal" align="center" style="width: 92%">
		<tr>
			<th colspan="2" style="width: 30px;">&nbsp;</th>
			<th>ID</th>
			<th>Date (Duration)</th>
			<th>Merchant</th>
			<th>IP Address</th>
			<th>Reply (Desc)</th>
			<th>Trans Id</th>
		</tr>
		<%
	    Dim x As Integer = 0
		sSQL = "SELECT LogPaymentPage_id, Lpp_RemoteAddress, Lpp_RequestMethod, Lpp_LocalAddr, Lpp_PathTranslate, Lpp_HttpHost, Lpp_HttpReferer, " & _
		 "Lpp_QueryString, Lpp_RequestForm, Lpp_SessionContents, IsNull(Lpp_ReplyCode, '') Lpp_ReplyCode, " & _
		 "IsNull(Lpp_ReplyDesc,'') Lpp_ReplyDesc, Lpp_TransNum, Lpp_DateStart, Lpp_DateEnd," & _
		 "tblCompany.ID AS CompanyID, CompanyName " & _
		 "FROM tblLogPaymentPage " & _
		 "LEFT JOIN tblCompany ON Right('0000000'+LTrim(RTrim(Lpp_MerchantNumber)), 7)=CustomerNumber" & _
		 " " & sQueryWhere & " ORDER BY Lpp_DateStart DESC"
		PGData.OpenDataset(sSQL)
		While PGData.Read()
			i += 1
			sStatusColor = IIf(PGData("Lpp_ReplyCode") = "000", "#66cc66", "#ff6666")
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
				<td>
					<img onclick="ExpandNode('PL<%=i%>');" style="cursor: pointer;" id="ImgPL<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" /><br />
				</td>
				<td><span style="background-color: <%= sStatusColor %>;">&nbsp;</span></td>
				<td><%=PGData("LogPaymentPage_id")%></td>
				<td nowrap="nowrap">
					<%
					Response.Write(CType(PGData("Lpp_DateStart"), DateTime).ToString("dd/MM/yy HH:mm:ss"))
					If Not IsDBNull(PGData("Lpp_DateStart")) And Not IsDBNull(PGData("Lpp_DateEnd")) Then
						Response.Write(" (" & DateDiff(DateInterval.Second, CType(PGData("Lpp_DateStart"), DateTime), CType(PGData("Lpp_DateEnd"), DateTime)) & " s)<br />")
					End If
					%>
				</td>
				<td style="white-space: nowrap;">
					<%
					If Not IsDBNull(PGData("CompanyName")) Then
						If Trim(Request("iMerchantID")) <> "" Then Response.Write(PGData("CompanyName")) _
							Else dbPages.showFilter(PGData("CompanyName"), "?" & sQueryString & "&iMerchantID=" & PGData("CompanyID"), 1)
					End If
					%><br />
				</td>
				<td>
					<%
					If Trim(Request("iRemoteAddress")) <> "" Then Response.Write(PGData("Lpp_RemoteAddress")) _
						Else dbPages.showFilter(PGData("Lpp_RemoteAddress"), "?" & sQueryString & "&iRemoteAddress=" & PGData("Lpp_RemoteAddress"), 1)
					%>
				</td>
				<td>
					<%
					If Trim(PGData("Lpp_ReplyCode")) <> "" Then
						If Trim(PGData("Lpp_ReplyCode")) <> "000" And Trim(PGData("Lpp_ReplyDesc")) <> "" Then
							sTmpTxt = PGData("Lpp_ReplyCode") & " (<a style=""cursor:help"" title=""" & PGData("Lpp_ReplyDesc") & """>?</a>)" : nTmpNum = 4
						Else
							sTmpTxt = PGData("Lpp_ReplyCode") : nTmpNum = 1
						End If
						If Trim(Request("iReplyCode")) <> "" Then Response.Write(sTmpTxt) _
							Else dbPages.showFilter(sTmpTxt, "?" & sQueryString & "&iReplyCode=" & PGData("Lpp_ReplyCode"), nTmpNum)
					Else
						Response.Write("---")
					End If
					%>
				</td>
				<td><%=PGData("Lpp_TransNum")%><br /></td>
			</tr>
			<tr id="RowPL<%=i%>" style="display: none;">
				<td colspan="2"></td>
				<td colspan="7" style="padding:15px 0 25px 0; border-top: 1px dashed #e2e2e2;">
					<table class="formNormal" style="width: 100%;">
						<tr>
							<%
							Dim paramString as String = ""
							If Not IsDBNull(PGData("Lpp_RequestForm")) Then
								paramString &= PGData("Lpp_RequestForm")
								Response.Write("<td valign=""top"">")
								Response.Write("<span class=""dataHeading"">Request Form</span><br />")
								Response.Write(DisplayContent(PGData("Lpp_RequestForm")))
								Response.Write("</td>")
							End If
							If Not IsDBNull(PGData("Lpp_QueryString")) Then
								If paramString <> "" Then paramString &= "&"
								paramString &= PGData("Lpp_QueryString")
								Response.Write("<td valign=""top"">")
								Response.Write("<span class=""dataHeading"">Query String</span><br />")
								Response.Write(DisplayContent(PGData("Lpp_QueryString")))
								Response.Write("</td>")
							End If
							Response.Write("<td valign=""top"">")
							If Not IsDBNull(PGData("Lpp_SessionContents")) Then
								Response.Write("<span class=""dataHeading"">Session Contents</span><br />")
								Response.Write(DisplayContent(PGData("Lpp_SessionContents")) & "</span><br /><br />")
							End If
							If Not IsDBNull(PGData("Lpp_HttpReferer")) Then
								Response.Write("<span class=""dataHeading"">Http Referer</span><br />")
								Response.Write(DisplayContent(PGData("Lpp_HttpReferer")) & "</span><br /><br />")
							End If
							If Not IsDBNull(PGData("Lpp_HttpHost")) Then
								Response.Write("<span class=""dataHeading"">Http Host</span><br />")
								Response.Write(DisplayContent(PGData("Lpp_HttpHost")) & "</span><br /><br />")
							End If
							If Not IsDBNull(PGData("Lpp_RequestMethod")) Then
								Response.Write("<span class=""dataHeading"">Request Method</span><br />")
								Response.Write(DisplayContent(PGData("Lpp_RequestMethod")) & "</span><br /><br />")
							End If
							If Not IsDBNull(PGData("Lpp_LocalAddr")) Then
								Response.Write("<span class=""dataHeading"">Local Address</span><br />")
								Response.Write(DisplayContent(PGData("Lpp_LocalAddr")) & "</span><br /><br />")
							End If
								
							Response.Write("<a href=""" & Domain.Current.ProcessV2Url & "hosted/?" & paramString & """ target=""_blank"">Open HPP with this parameters<a/>")
							Response.Write("</td>")
							%>
						</tr>
						<tr><td height="10"></td></tr>
					</table>
					<b>Process History</b><br /><br />
					<table class="formNormal" style="width: 100%;">
					    <tr>
					        <th>Log ID</th>
					        <th>Date</th>
					        <th>Payment Method</th>
					        <th>Trans Num</th>
					    </tr>
					    <%
					    Dim pReader As System.Data.SqlClient.SqlDataReader = dbPages.ExecReader("Select lppt.*, pmt.Name as pmName " & _
					        " FROM tblLogPaymentPageTrans AS lppt LEFT JOIN [List].[PaymentMethodType] AS pmt ON pmt.PaymentMethodType_id = lppt.Lppt_PaymentMethodType" & _
					        " WHERE lppt.Lppt_LogPaymentPage_id=" & PGData("LogPaymentPage_id") & " ORDER BY lppt.LogPaymentPageTrans_id ASC")
					    If Not pReader.HasRows Then Response.Write("<tr><td colspan=""4"">No history found</td></tr>")
		                Do While pReader.Read()
							%>
							<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
								<td>
									<img onclick="ExpandNode('CL<%=x%>');" style="cursor: pointer;" id="ImgCL<%=x%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="top" /> 
									<%= pReader("LogPaymentPageTrans_id") %>
								</td>
								<td><%= pReader("Lppt_Date") %></td>
								<td><%= pReader("pmName") %></td>
								<td><%= pReader("Lppt_TransNum") %></td>
							</tr>
							<tr id="RowCL<%=x%>" style="display: none;">
								<td colspan="4">
									<table>
										<tr><td><br /></td></tr>
										<tr>
											<td colspan="5">
												<span class="dataHeading">Request String</span><br />
												<div style="border:dotted 1px gray; width:100%; overflow:auto;">
                                                    <% If NOT IsDBNull(pReader("Lppt_RequestString")) Then Response.Write(Replace(HttpUtility.HtmlEncode(Replace(pReader("Lppt_RequestString"), "&", " &")), vbCrLf, "<br />")) %>
                                                </div>
											</td>
										</tr>
										<tr><td><br /></td></tr>
										<tr>
											<td colspan="5">
												<span class="dataHeading">Response String</span><br />
												<div style="border:dotted 1px gray; width:100%; overflow:auto;">
													<% If Not IsDBNull(pReader("Lppt_ResponseString")) Then Response.Write(Replace(HttpUtility.HtmlEncode(Replace(pReader("Lppt_ResponseString"), "&", " &")), vbCrLf, "<br />"))%>
												</div>
											</td>
										</tr>
										<tr><td height="10"></td></tr>
									</table>
								</td>					        
							</tr>
            				<tr><td height="1" colspan="4" bgcolor="silver"></td></tr>
							<%
					        x += 1
					    Loop
					    pReader.Close()
					    %>
					</table>
				</td>
			</tr>
			<tr><td height="1" colspan="9" bgcolor="silver"></td></tr>
			<%
		End While
		PGData.CloseDataset()
		%>
	</table>
	<script language="JavaScript">
			function ExpandAll(fDisplay, fImgStatus) {
				for(i = 0; i <= <%= i %>; i++) {
					trObj = document.getElementById("trInfo" + i)
					imgObj = document.getElementById("oListImg" + i)
					if (trObj) {
						imgObj.src = '../images/tree_' + fImgStatus + '.gif';
						trObj.style.display = fDisplay;
					}
				}
			}
	</script>
	<br />
	<div id="filterIcon" style="visibility: hidden; position: absolute; z-index: 1; font-size: 10px; color: Gray; background-color: White;">
		<img src="../images/icon_filterPlus.gif" align="middle">ADD TO FILTER
	</div>
	</form>
	<table align="center" style="width: 92%">
		<tr><td><UC:Paging runat="Server" ID="PGData" PageID="PageID" /></td></tr>
	</table>
</body>
</html>
