<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = Date.Today
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
        If HttpContext.Current.Application.Get("CUR_CHARS") Is Nothing Then dbPages.LoadCurrencies()
        Dim sCurrenciesSelect As String = "<select name=""Currency"" style=""width:120px;"">"
        sCurrenciesSelect = sCurrenciesSelect & "<option value=""""></option>"
        For i As Integer = 0 To eCurrencies.MAX_CURRENCY
            sCurrenciesSelect = sCurrenciesSelect & "<option value=""" & i & """>" & HttpContext.Current.Application.Get("CUR_ISO")(i) & "</option>"
        Next
        sCurrenciesSelect = sCurrenciesSelect & "</select>"
        litCurrencies.Text = sCurrenciesSelect
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
	<form runat="server" action="Log_FraudDetection_data.aspx" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />
    <input type="hidden" id="Source" name="Source" value="Filter" />

	<h1><asp:label ID="lblPermissions" runat="server" /> Risk Management<span> - Filter</span></h1>
	<table class="filterNormal">
		<tr>
			<td>
				<Uc1:FilterMerchants id="FMerchants" ClientField="CompanyID" runat="server" />
				<input type="hidden" id="CompanyID" name="CompanyID" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>	
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Amount</th></tr>
		<tr><td>
			From <input type="text" dir="ltr" size="5" name="amountFrom" value="">
			&nbsp;
			To <input type="text" dir="ltr" size="5" name="amountTo" value="">
		</td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Currencies</th></tr>
		<tr><td><asp:Literal runat="server" ID="litCurrencies" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Status</th></tr>
		<tr>
		    <td>
		        <select name="transStatus" style="width:120px;">
		            <option value="" selected="selected"></option>
		            <option value="0">Failed Risk Management </option>
		            <option value="1">Process Successful</option>
		            <option value="2">Process Rejected</option>
		        </select>
		    </td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Reference Code</th></tr>
		<tr><td><input type="text" name="ReferenceCode" style="width:120px;"></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
	    <tr>
		    <td colspan="2">
			    <input type="checkbox" name="SQL2" value="1" checked="checked" class="option">Use SQL2 when possible
		    </td>
	    </tr>
	    <tr><td height="6"></td></tr>
	    <tr>	
		    <td>
			    <select name="iPageSize" class="grayBG">
				    <option value="10">10 rows/page</option>
				    <option value="25" selected="selected">25 rows/page</option>
				    <option value="50">50 rows/page</option>
				    <option value="100">100 rows/page</option>
			    </select>
		    </td>
		    <td style="text-align:right;">
			    <input type="submit" name="Action" value="SEARCH">
		    </td>
	    </tr>
	</table>
	</form>
</asp:Content>