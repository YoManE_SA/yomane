<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20080728 Tamir
	'
	' BIN numbers Management
	'
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)

		lblCountry.Text = "<select size=""5"" multiple=""multiple"" name=""Country"" class=""grayBG"" style=""width:100%;"">"
        Dim sSQL As String = "SELECT CountryISOCode, Name FROM [List].[CountryList] UNION SELECT '--', '- Unknown -' ORDER BY Name"
		Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblCountry.Text &= "<option class=""grayBG"" style=""text-transform:capitalize;"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		drData.Close()
		lblCountry.Text &= "</select>"

		lblCardType.Text &= "<select size=""5"" multiple=""multiple"" name=""CardType"" class=""grayBG"" style=""width:100%;"">"
		sSQL = "SELECT PaymentMethod_id AS 'ID', Name AS 'pm_Name' FROM List.PaymentMethod WHERE PaymentMethod_id BETWEEN " & ePaymentMethod.PMD_CC_MIN & " AND " & ePaymentMethod.PMD_CC_MAX & " ORDER BY PaymentMethod_id"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblCardType.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		drData.Close()
		lblCardType.Text &= "</select>"
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Merchant Status Filter</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminNetEng.css" />
</head>
<body class="menu">
	<br />
	<h1><asp:label ID="lblPermissions" runat="server" /> BIN Numbers <span> - Filter</span></h1>
	<br />
	<form id="frmFilter" action="System_BinData.aspx" method="get" target="frmBody">
		<table class="filterTable" cellpadding="0" cellspacing="0" border="0">
			<tr><th class="filter">Countries</th></tr>
			<tr><td><asp:Label ID="lblCountry" runat="server" /></td></tr>
			<tr><td><br /></td></tr>
			<tr><th class="filter">Card Types</th></tr>
			<tr><td><asp:Label ID="lblCardType" runat="server" /></td></tr>
			<tr><td><br /></td></tr>
			<tr><th class="filter">BIN Numbers</th></tr>
			<tr><td><textarea name="BIN" class="grayBG" style="font-size:11px;letter-spacing:-1px;width:100%;height:70px;"></textarea></td></tr>
			<tr>
				<td align="right">
					<br />
					<input type="submit" class="buttonFilter" value="Search" />
					<input type="reset" class="buttonFilter buttonDelete" value="Reset" />
				</td>
			</tr>
		</table>
	</form>
</body>
</html>