<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20080514 Tamir
	'
	' Security management - toolbar
	'
	Sub Page_Load()
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security Toolbar</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body class="menu">
	<script language="JavaScript" type="text/javascript">
		function LoadMenu(spnItem, sMenu)
		{
			spnUsers.className="toolbarItem"
			spnGroups.className="toolbarItem"
			spnDocuments.className="toolbarItem"
			parent.fraBody.location.replace("common_blank.htm");
			parent.fraMenu.location.href("security_"+sMenu+".aspx");
			spnItem.className="toolbarItemActive";
			parent.fraMenu.focus();
		}
	</script>
	<span class="toolbarLabel" id="lblToolbar">
		Security
	</span>
	<span class="toolbarItem" id="spnUsers" onclick="LoadMenu(spnUsers, 'users');">Users</span>
	<span class="toolbarItem" id="spnGroups" onclick="LoadMenu(spnGroups, 'groups');">Groups</span>
	<span class="toolbarItem" id="spnDocuments" onclick="LoadMenu(spnDocuments, 'documents');">Documents</span>
	<script language="javascript" type="text/javascript">
		LoadMenu(spnUsers, "users");
	</script>
</body>
</html>