<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/const_globalData.asp"-->
<!--include file="../include/func_controls.asp"-->
<!--#include file="../include/func_transPay.asp"-->
<%
	sCompanyID = request("companyID")
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">
<script language="javascript">
    focus();
</script>
<table cellpadding="1" cellspacing="2" width="100%">
<tr>
    <td id="pageMainHeadingTd">
        <span id="pageMainHeading">TRANSACTIONS HISTORY</span>
        <%
        If sCompanyID <> "" Then
			sCompanyName = ExecScalar("SELECT CompanyName FROM tblCompany WHERE ID=" & sCompanyID, "")
			Response.Write(" <span class=""txt14"">- " & dbTextShow(uCase(sCompanyName)) & "</span>")
        End if
        %>
    </td>
</tr>
</table>
<br />
<%
pageSecurityLevel = 0
pageSecurityLang = "ENG"
pageClosingTags = ""
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
%>
<table class="formNormal" style="width:100%; padding:0; margin:0;">
<tr>
	<th colspan="2" valign="bottom"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></th>
	<th valign="bottom">ID /<br />Date & Time<br /></th>
	<th valign="bottom">Merchant's Name /<br />Payment Method</th>
	<td width="24""></td>
	<th width="100" valign="bottom">Details<br /></th>
</tr>
<tr><td height="3"></td></tr>
    <!--getAllTrans function=
         SELECT TOP 25
  *
 FROM
  (
   SELECT
    ROW_NUMBER() OVER (ORDER BY ID DESC) Rating,
    'Pass' AS tblName, InsertDate, ID, companyID, TransactionTypeID, Amount, Currency, CreditType, Payments, PaymentMethodDisplay,
    DebitCompanyID, PaymentMethod_id, '000' AS replyCode, PaymentMethod, DeniedStatus, isTestOnly, PayID
   FROM
    dbo.tblCompanyTransPass
   WHERE
    @nMerchant IN (NULL, CompanyID)
   UNION SELECT
    ROW_NUMBER() OVER (ORDER BY ID DESC) Rating,
    'Fail' AS tblName, InsertDate, ID, companyID, TransactionTypeID, Amount, Currency, CreditType, Payments, PaymentMethodDisplay,
    DebitCompanyID, PaymentMethod_id, replyCode, PaymentMethod, 0 AS DeniedStatus, isTestOnly, '' AS PayID
   FROM
    dbo.tblCompanyTransFail
   WHERE
    @nMerchant IN (0, CompanyID)
  ) g
 WHERE
  Rating<=25
 ORDER BY
  InsertDate DESC -->
<%
    
sSQL="SELECT * FROM GetAllTrans(" & IIf(sCompanyID = "", "DEFAULT", sCompanyID) & ")"
Set rsData = oledbData.execute(sSQL)
If not rsData.EOF Then
	Set fsServer=Server.CreateObject("Scripting.FileSystemObject")
	Do until rsData.EOF
		Call Fn_TransParam(rsData, sTrBkgColor, sTdBkgColorFirst, sTdFontColor, sPayStatusText, bShowDeniedOptions, bAllowDelete)
		If Trim(rsData(0))="Fail" then
			sTdBkgColorFirst = "#ff6666"
		End if
		%>
		<tr>
			<td width="2" bgcolor="<%= sTdBkgColorFirst %>"><img src="../images/1_space.gif" alt="" width="2" height="1" border="0"><br /></td>
			<td width="2"></td>
			<td dir="ltr" nowrap valign="top" style="color:<%= sTdFontColor %>;">
			    <%= rsData("id") %><br /><%= FormatDatesTimes(rsData("InsertDate"),1,1,0) %><br />
			</td>
			<td valign="top" style="color:<%= sTdFontColor %>;">
				<% if trim(rsData("PaymentMethodDisplay"))<>"" Then response.write rsData("PaymentMethodDisplay") & "<br />" %>
			</td>
			<td bgcolor="#ffffff"></td>
			<td valign="top" nowrap style="color:<%= sTdFontColor %>;">
		        <%
		        response.write "Reply code: " & rsData("replyCode") & "<br />"
		        If trim(rsData("DebitCompanyID"))<>"" Then
			        If fsServer.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & trim(rsData("DebitCompanyID")) & ".gif")) Then
			            Response.write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(rsData("DebitCompanyID")) & ".gif"" align=""middle"" border=""0"" /> &nbsp;")
			        End if
			    End if
				response.write "<span dir=""ltr"">"
				response.write FormatCurrWCT(rsData("Currency"), rsData("Amount"), rsData("CreditType"))
				response.write "</span>"
				response.write " &nbsp;" & rsData("Payments") & " Ins. &nbsp;&nbsp;&nbsp;"
				Select case sPayStatusText
					case "Pending"
					%>
					<a href="merchant_transPass.asp?companyID=<%= rsData("companyID") %>&transID=<%= rsData("ID") %>&currency=<% if int(rsData("Currency"))=1 then %>1<% Else %>0<% End If %>&isFilter=1" style="text-decoration:underline; font-size:11px;" onclick="parent.fraBrowserButton.FrmResize();">Show &gt;&gt;</a><br />
					<%
					case "Settled"
						if nTypeCredit<>"8" then
						%>
						<a href="Merchant_transSettledDetail.asp?companyID=<%= rsData("companyID") %>&payID=<%= replace(rsData("payID"),";","") %>&transID=<%= rsData("ID") %>" style="text-decoration:underline; font-size:11px;" onclick="parent.fraBrowserButton.FrmResize();">Show &gt;&gt;</a><br />
						<%
						End If
					case "Archive"
					%>
					<a class="go" target="fraBrowser" href="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=<%=Server.UrlEncode("Trans_admin_regularFilter.aspx?FilterMerchantDefaultID=" & rsData("companyID") & "&Payout=A" )%>&pageBodyUrl=<%=Server.UrlEncode("Trans_admin_regularData.aspx?ShowCompanyID=" & rsData("companyID") & "&Payout=A&chk2=1&fromDate=" & Server.UrlEncode(DateAdd("m", -6, Now)) )%>">Show &gt;&gt;</a><br />
					<%
				End select
				%>
			</td>
		</tr>
		<tr>
			<td height="1" colspan="4" bgcolor="silver"></td>
			<td></td>
			<td height="1" colspan="1" bgcolor="silver"></td>
		</tr>
		<%
	rsData.movenext
	loop
    Set fsServer=Nothing
End if
call closeConnection()
%>
</table>
</body>
</html>