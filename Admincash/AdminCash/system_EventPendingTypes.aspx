<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
    Sub Page_Load()
        Security.CheckPermission(lblPermissions)
        dsRecords.ConnectionString = dbPages.DSN
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Global Data</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<style type="text/css">
		.centered input {width:60px !important;text-align:right !important;}
		.buttons input {width:50px !important;text-align:center !important;}
	</style>
</head>
<body>
	<form id="frmServer" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Pending Event Types</h1>
		<br /><br />
		<asp:GridView ID="gvRecords" runat="server" AutoGenerateColumns="False" DataKeyNames="EventPendingType_id" DataSourceID="dsRecords"
		 AllowPaging="False" AllowSorting="False" SelectedRowStyle-CssClass="rowSelected" Width="100%" >
			<Columns>
				<asp:BoundField DataField="EventPendingType_id" HeaderText="ID" ItemStyle-CssClass="centered" ReadOnly="true" ItemStyle-Width="50px" />
				<asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-CssClass="wide" ControlStyle-CssClass="wide" />
				<asp:BoundField DataField="MinutesForWarning" HeaderText="Minutes For Warning" ItemStyle-CssClass="centered" ItemStyle-Width="150px" />
				<asp:CommandField HeaderText="Edit" ButtonType="Button" ItemStyle-CssClass="centered buttons" ControlStyle-CssClass="buttonWhite"
				 ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="False" HeaderStyle-CssClass="actions" UpdateText="Save" ItemStyle-Width="150px" />
			</Columns>
		</asp:GridView>
		<asp:SqlDataSource ID="dsRecords" runat="server" ProviderName="System.Data.SqlClient"
		 SelectCommand="SELECT * FROM list.EventPendingType"
		  UpdateCommand="UPDATE list.EventPendingType SET Name=@Name, MinutesForWarning=@MinutesForWarning WHERE EventPendingType_id=@ID"
		   DeleteCommand="DELETE FROM list.EventPendingType WHERE EventPendingType_id=@ID">
			<UpdateParameters>
				<asp:Parameter Name="EventPendingType_id" Type="Int32" />
				<asp:Parameter Name="Name" Type="String" />
				<asp:Parameter Name="MinutesForWarning" Type="Int32" />
			</UpdateParameters>
			<DeleteParameters>
				<asp:Parameter Name="EventPendingType_id" Type="Int32" />
			</DeleteParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>