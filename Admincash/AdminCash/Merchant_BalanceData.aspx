<%@ Page Title="Account Balance" Language="C#" MasterPageFile="~/AdminCash/ContentPage.master" AutoEventWireup="true" %>
<script runat="server">
    Dictionary<int, string> accountNames;
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        var sf = new Netpay.Bll.Accounts.Balance.SearchFilters();
        //sf.AccountType = Netpay.Bll.Accounts.AccountType.Merchant;
        if (Request["ShowCompanyID"].ToNullableInt().HasValue)
            sf.AccountID = Netpay.Bll.Merchants.Merchant.LoadAccount(Netpay.Bll.Accounts.AccountType.Merchant, Request["ShowCompanyID"].ToNullableInt().GetValueOrDefault()).AccountID;
        sf.Amount = new Netpay.Infrastructure.Range<Decimal?>(Request["amountFrom"].ToNullableDecimal(), Request["amountTo"].ToNullableDecimal());
        if (Request["Currency"].ToNullableInt() != null) sf.CurrencyIso = Netpay.Bll.Currency.GetIsoCode(Request["Currency"].ToNullableEnum<Netpay.CommonTypes.Currency>().GetValueOrDefault());
        sf.InsertDate = new Netpay.Infrastructure.Range<DateTime?>((Request["fromDate"] + " " + Request["fromTime"]).ToNullableDateTime(), (Request["toDate"] + " " + Request["toTime"]).ToNullableDateTime());
        sf.IsPending = Request["ddlPending"].ToNullableBool();
        pgList.PageSize = Request["PageSize"].ToNullableInt().GetValueOrDefault(20);
        if (Request["ddlResults"].ToNullableInt().GetValueOrDefault((sf.AccountID == null ? 1 : 0)) == 1) {
            var list = Netpay.Bll.Accounts.Balance.GetStatus(sf, pgList.Info);
            rptTotals.DataSource = list;
            accountNames = Netpay.Bll.Accounts.Account.GetNames(list.Select(v => v.AccountId).ToList());
        } else {
            var list = Netpay.Bll.Accounts.Balance.Search(sf, pgList.Info);
            rptList.DataSource = list;
            accountNames = Netpay.Bll.Accounts.Account.GetNames(list.Select(v => v.AccountID).ToList());
        }
        if (accountNames.Count == 1) lblTitle.Text += " - " + accountNames.First().Value;
        if (!string.IsNullOrEmpty(sf.CurrencyIso)) lblTitle.Text += " - " + sf.CurrencyIso;
        DataBindChildren();
    }

    protected string GetAccountName(int accountId)
    {
        string ret;
        if (accountNames.TryGetValue(accountId, out ret)) return ret;
        return string.Empty;
    }

    private string formatAmount(decimal amount, string currencyIso)
    {
        string format = new Money(amount, currencyIso).ToIsoString();
        if (amount < 0) format = "<span style=\"color:red;\">" + format + "</span>";
        return format;
    }

    private string getDirImage(decimal amount)
    {
        return "../images/icon_Arrow" + (amount < 0 ? "R" : "G") + ".gif";
    }

    private string formatSource(string source) {
        var items = source.EmptyIfNull().Split(new char[] { '.' }, 2);
        if (items.Length < 2) return source;
        if (items[0].ToUpper() == "SYSTEM") return "<span style=\"color:blue;\">" + items[1] + "</span>";
        else return string.Format("{1} ({0})", items[0], items[1]);
    }

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <span class="sectionHeading">ACCOUNT BALANCE</span> <asp:Literal runat="server" id="lblTitle" Text="" EnableViewState="false" /><br /><br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="Server">
    <asp:Literal ID="lblPermissions" runat="server" />
    <asp:Repeater runat="server" ID="rptList">
        <HeaderTemplate>
            <table class="formNormal" width="100%">
                <tr>
                    <th>&nbsp;</th>
                    <th>Date</th>
                    <th>Account</th>
                    <th>Action Type</th>
                    <th align="center">Text</th>
                    <th align="right">Credit</th>
                    <th align="right">Debit</th>
                    <th align="center">Balance</th>
                    <th align="center">IsPending</th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr style="<%# (bool)Eval("IsPending") ? "background-color:#FFF4B3;" : "" %>">
                <td><img src="<%# getDirImage((decimal)Eval("Amount")) %>" /></td>
                <td><%# Eval("InsertDate") %></td>
                <td><%# GetAccountName((int)Eval("AccountID")) %></td>
                <td><%# formatSource((string)Eval("SourceType")) %></td>
                <td class="Numeric" align="center" title='<%# Eval("Text") %>'><%# Eval("Text") %></td>
                <td align="right" class="Numeric"><%# (decimal)Eval("Amount") > 0 ? Eval("Amount") : "" %></td>
                <td align="right" class="Numeric"><%# (decimal)Eval("Amount") <= 0 ? Eval("Amount") : "" %></td>
                <td align="right" class="Numeric"><%# formatAmount((decimal)Eval("Total"), (string) Eval("CurrencyIso")) %></td>
                <td><%# Eval("IsPending") %></td>
            </tr>
			<tr>
                <td height="1" colspan="10" style="background-color: #c5c5c5;"></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <asp:Repeater runat="server" ID="rptTotals">
        <HeaderTemplate>
            <table class="formNormal" width="100%">
                <tr>
                    <th>Account</th>
                    <th>Currency</th>
                    <th>Current</th>
                    <th>Pending</th>
                    <th>Expected</th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <img src="<%# getDirImage((decimal)Eval("Expected")) %>" /> &nbsp;&nbsp;
                    <%# GetAccountName((int)Eval("AccountId")) %>
                </td>
                <td><%# Eval("CurrencyIso") %></td>
                <td><%# formatAmount((decimal)Eval("Current"), (string) Eval("CurrencyIso")) %></td>
                <td><%# formatAmount((decimal)Eval("Pending"), (string) Eval("CurrencyIso")) %></td>
                <td><%# formatAmount((decimal)Eval("Expected"), (string) Eval("CurrencyIso")) %></td>
            </tr>
			<tr>
                <td height="1" colspan="5" style="background-color: #c5c5c5;"></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>

    <netpay:Pager runat="server" ID="pgList" />
</asp:Content>
