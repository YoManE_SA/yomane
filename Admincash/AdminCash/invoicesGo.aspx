<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient" %>
<script runat="server">
	Sub Page_Load()
		Security.CheckPermission("Invoices - Delete")
		Dim sSQL As String = String.Empty
		If dbPages.TestVar(Request("DeleteInvoiceDocument"), 1, 0, 0) > 0 Then
			sSQL = "DELETE FROM tblInvoiceDocument WHERE ID=" & Request("DeleteInvoiceDocument") & " AND id_IsPrinted=0 AND ID IN (SELECT ID FROM viewInvoices WHERE InvoiceOrderNumber=1)"
		End If
		If sSQL <> String.Empty Then dbPages.ExecSql(sSQL)
	End Sub
</script>
<script language="javascript" type="text/javascript">
	parent.location.reload();
</script>
