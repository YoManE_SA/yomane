﻿<%@ page language="C#" autoeventwireup="true" codefile="Merchant_dataMail.aspx.cs"	inherits="AdminCash_merchants_dataMail" %>
<%@ Register Src="TabMenu.ascx" TagPrefix="UC1" TagName="TabMenu" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css" />
	<script type="text/javascript">
		var prev = '';
		function showhistoryrow(namerow, spanrow, num) {
			document.getElementById("contentSP").display = "block";
			document.getElementById("contentSP").style.border = "1px solid #f0f0f0";
			document.getElementById("contentSP").innerHTML = document.getElementById(namerow).innerHTML;
			document.getElementById("row_" + num).style.color = "Blue";
			if (prev != ("row_" + num) && prev != "") { document.getElementById(prev).style.color = "#000"; prev = ("row_" + num); }
			else { prev = ("row_" + num); }
		}
	</script>
	<style type="text/css">
		#tbl_historymail
		{
			width: 80%;
			border: 1px solid #000;
			display: inline-block;
			margin: 0px;
			float: left;
		}
		#tbl_historymail th
		{
			background-color: rgb(216, 216, 216);
			padding: 2px 0px 2px 5px;
			text-align: left;
		}
		#tbl_historymail tr
		{
			background-color: #fff;
		}
		#tbl_historymail tr:hover
		{
			background-color: #f9f9f9;
		}
		#tbl_historymail tr td
		{
			border-bottom: 1px solid #f0f0f0;
		}
		#contentSP
		{
			width: 44%;
			display: inline-block;
			padding: 5px;
			margin: 0px;
			float: left;
			min-height: 100px;
			max-height: 300px;
			overflow-x: hidden;
			overflow-y: auto;
		}
		.imgo
		{
			padding: 0px;
			margin: 0px;
		}
		.th_date
		{
			width: 100px;
		}
		#merchantM
		{
			clear: both;
			border: 1px solid #000;
			width: 100%;
			height: auto;
		}
	</style>
</head>
<body>
	<form id="form1" runat="server">
	<UC1:TabMenu ID="TabMenu1" Width="95%" runat="server" idFieldName="merchantID" />
	<div style="padding: 0px 5%;">
		<asp:repeater id="repMerchantEmails" runat="server">
			<HeaderTemplate>
				<table cellpadding="0" cellspacing="1" id="tbl_historymail">
					<tr>
						<th class="th_date"><span>Date and Time</span></th>
						<th><span>Subject</span></th>
						<th><span>From</span></th>
						<th><span>To</span></th>
						<th><span>Status</span></th>
						<th colspan="2"><span>Replied With</span></th>
					</tr>
			</HeaderTemplate>
			<itemtemplate>
				<tr id="row_<%#Container.ItemIndex %>" style="border-bottom: 1px solid #000; padding: 1px;
					cursor: pointer;" onclick="showhistoryrow('history_row_<%#Container.ItemIndex %>','sp_<%#Container.ItemIndex %>','<%#Container.ItemIndex %>')">
					<td>
						<span><%#DateTime.Parse(Eval("Date").ToString()).ToString("MM/dd/yy HH:MM")%></span>
					</td>
					<td>
						<spa><%#Eval("Subject")%></spa>
					</td>
					<td>
						<span><%#Eval("EmailFrom")%></span>
					</td>
					<td>
						<span><%#Eval("EmailTo")%></span>
					</td>
					<td>
						<span><%#GetStatusName((int?)Eval("Status"))%></span>
					</td>
					<td>
						<span><%#Eval("AdmincashUser")%></span>
						<%#Eval("IsSent").ToString().ToLower() == "false" ? "<img alt='Msg In' title='Msg In' height='10' class='imgo' width='10' id='0' src='../images/icon_greenArrow.gif' />" : "<img alt='Msg Out' class='imgo'  dir='ltr' title='Msg Out' height='10' width='10' id='0' src='../images/icon_ArrowR2.gif' />"%>
					</td>
				</tr>
				<tr id="history_row_<%#Container.ItemIndex %>" style="display: none">
					<td id="history_row_td_<%#Container.ItemIndex %>" colspan="7" style="padding: 5px;">
						<%#Eval("Text")%>
					</td>
				</tr>
			</itemtemplate>
			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:repeater>
	</div>
	<!---
	<span class="contentSP" id="contentSP"></span>
	<div id="merchantM">
		<asp:hiddenfield id="hf_holderIdMerchant" runat="server" />
		<asp:textbox id="txt_mailsMerchant" runat="server" width="50%" rows="5" textmode="MultiLine"
			clientidmode="Static"></asp:textbox>
		<br />
		<asp:button id="btn_update" onclick="btn_update_Click" runat="server" text="Update" />
	</div>
	--->
	</form>
</body>
</html>
