<%@ Page Language="VB" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="radC" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Charting" TagPrefix="radC" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Dim sCompany_id As Integer, nCurrency As Integer
    Dim sTerminal_id As String, sPageHeading As String, sWhere As String
    Dim sTable As String = "tblCompanyTransPass"
    Dim nTotalTransCountAll As Integer, nTotalTransSumAll As Decimal
    Dim nSums(3) As Decimal
    Dim strDetails(3) As String
    
    Private Function GetCCExtract(ByVal xWhere As String, ByVal nTitle As String, ByVal xCount As Integer, ByVal xPrcCount As Decimal, ByVal xTotal As Decimal, ByVal xPrcTotal As Decimal, ByVal lDayStart As Integer, ByVal lDayEnd As Integer) As String
        Dim sSQL As String = "SELECT ccTypeID, COUNT(Amount) AS transCountAll, SUM(Amount) AS transSumAll FROM " & sTable & _
            " Left Join tblCreditCard ON(" & sTable & ".CreditCardID = tblCreditCard.ID)" & _
            " WHERE (" & Replace(xWhere, "tblCompanyTransPass", sTable) & ") " & _
            " And (InsertDate > DATEADD(day," & lDayStart & ",GETDATE())) AND (InsertDate < DATEADD(day," & lDayEnd & ",GETDATE()))" & _
            " GROUP BY ccTypeID"
        'Response.Write(sSQL)
        Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
        Dim ccSum(3) As Decimal
        Dim ccCount(3) As Decimal
        Do While iReader.Read()
            Dim lIndex As Integer
            Select Case IIf(IsDBNull(iReader(0)), 0, iReader(0))
                Case 2, 8
                    lIndex = 0
                Case 5, 11
                    lIndex = 1
                Case Else
                    lIndex = 2
            End Select
            ccCount(lIndex) = ccCount(lIndex) + iReader(1)
            'ccSum(lIndex) = ccSum(lIndex) + iReader(2)
        Loop
        iReader.Close()
        Dim CountLine As String = ccCount(0) & "+" & ccCount(1) & "+" & ccCount(2) & "="
        Dim SumLine As String = ""
        For i As Integer = 0 To 2
            If ccCount(i) <> 0 And nTotalTransCountAll <> 0 Then _
                SumLine += FormatNumber((ccCount(i) / nTotalTransCountAll) * 100, 2) & "%" _
            Else SumLine += "0.00%"
            SumLine += IIf(i < 2, "+", "=")
        Next
        nSums(0) += xPrcCount : nSums(1) += xCount : nSums(2) += xPrcTotal : nSums(3) += xTotal
        Dim pRet As String = ""
        pRet += "<tr>"
        pRet += "<th align=""left"">" & nTitle & "</th>"
        pRet += "<td>" & SumLine & FormatNumber(xPrcCount, 2) & "%</td>"
        pRet += "<td>" & CountLine & xCount & "</td>"
        pRet += "<td>" & FormatNumber(xPrcTotal, 2) & "%</td>"
        pRet += "<td>" & dbPages.FormatCurr(nCurrency, -xTotal) & "</td>"
        pRet += "</tr>"
        Return pRet
    End Function
        
    Private Function GetTotals(ByVal lDayStart As Integer, ByVal lDayEnd As Integer) As String
        Dim pRet As New System.Text.StringBuilder
        pRet.Append("<p class=""sectionHeading"" align=""left"">" & -lDayEnd & " - " & -lDayStart & " Days ago</p>")
        pRet.Append("<table align=""left"" border=""0"" cellpadding=""0"" cellspacing=""0""><tr><td bgcolor=""LightGrey"">")
        pRet.Append("<table id=""tblTotals"" border=""0"" cellpadding=""1"" cellspacing=""1"">")
        pRet.Append("<tr id=""headTotals"">")
        pRet.Append("<td></td>")
        pRet.Append("<td>% of Processed Count (V+M+O=X)</td>")
        pRet.Append("<td>Processed Count (V+M+O=X)</td>")
        pRet.Append("<td>% of Processed Sum</td>")
        pRet.Append("<td>Processed Sum</td>")
        pRet.Append("</tr>")
        
        'Regular transaction
        Dim sSQL As String = "SELECT COUNT(Amount) AS transCountAll, SUM(Amount) AS transSumAll FROM tblCompanyTransPass " & _
            "WHERE (" & sWhere & ")" & _
            "AND (CreditType > 0) AND (DeniedStatus = 0) AND (InsertDate > DATEADD(day," & lDayStart & ",GETDATE())) AND (InsertDate < DATEADD(day," & lDayEnd & ",GETDATE()))"
        Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
        If iReader.Read() Then
            nTotalTransCountAll = dbPages.TestVar(iReader("transCountAll"), 0, -1, 0)
            nTotalTransSumAll = dbPages.TestVar(iReader("transSumAll"), 0, -1, 0D)
            pRet.Append("<tr>")
            pRet.Append("<th>Total transactions: (all Cards)</th>")
            pRet.Append("<td></td>")
            pRet.Append("<td>" & nTotalTransCountAll & "</td>")
            pRet.Append("<td></td>")
            pRet.Append("<td>" & dbPages.FormatCurr(nCurrency, nTotalTransSumAll) & "</td>")
            pRet.Append("</tr>")
        End If
        iReader.Close()
        pRet.Append("<tr><td colspan=""5"" style=""background-color:LightGrey;""></td></tr>")
        Return pRet.ToString()
    End Function

    Public Function FillGraph(ByVal nTitle As String, ByVal xWhere As String, ByVal dtDay As Date, ByVal lDayStart As Integer, ByVal lDayEnd As Integer, ByVal fGraph As Telerik.Web.UI.RadChart, ByVal bCol As Byte) As String
        Dim sSQL As String = "SELECT CAST(FLOOR(CAST(InsertDate AS FLOAT)) AS DATETIME) AS transDateRefund, COUNT(ID) AS transCountRefund, SUM(Amount) AS transSumRefund " & _
            "FROM " & sTable & " WHERE (" & Replace(xWhere, "tblCompanyTransPass", sTable) & ")" & _
            "GROUP BY CAST(FLOOR(CAST(InsertDate AS FLOAT)) AS DATETIME) " & _
            "HAVING (CAST(FLOOR(CAST(InsertDate AS FLOAT)) AS DATETIME) < DATEADD(day, " & lDayEnd & ", GETDATE())) " & _
            "AND (CAST(FLOOR(CAST(InsertDate AS FLOAT)) AS DATETIME) > DATEADD(day," & lDayStart & ", GETDATE())) " & _
            "ORDER BY CAST(FLOOR(CAST(InsertDate AS FLOAT)) AS DATETIME) DESC"
        Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
        Dim TotalSum As Decimal = 0, nCountPrc As Decimal = 0, nSumPrc As Decimal = 0
        Dim TotalCount As Integer = 0
        If iReader.HasRows() Then
            iReader.Read()
            Dim dRowDate As Date = CDate(iReader(0)).Date
            For i As Integer = 0 To 29
                Dim nTransCount As Integer = 0
                Dim dProcessDate As Date = DateAdd("d", -i, dtDay)
                If dRowDate = dProcessDate Then
                    nTransCount = dbPages.TestVar(iReader(1), 0, -1, 0)
                    TotalCount = TotalCount + nTransCount
                    TotalSum = TotalSum + dbPages.TestVar(iReader(2), 0, -1, 0D)
                    If iReader.Read() Then dRowDate = CDate(iReader(0)).Date
                End If
                If bCol = 0 Then fGraph.PlotArea.XAxis.AddItem(dProcessDate.ToString("dd / MM"))
                fGraph.Series.Item(bCol).AddItem(nTransCount)
            Next
        End If
        iReader.Close()
        If TotalCount > 0 And nTotalTransCountAll > 0 Then nCountPrc = (TotalCount / nTotalTransCountAll) * 100
        If TotalSum > 0 And nTotalTransSumAll > 0 Then nSumPrc = (TotalSum / nTotalTransSumAll) * 100
        Return GetCCExtract(xWhere, nTitle, TotalCount, nCountPrc, TotalSum, nSumPrc, lDayStart, lDayEnd)
    End Function
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        nCurrency = dbPages.TestVar(Request.Form("currency"), 0, 255, 0)
        sCompany_id = dbPages.TestVar(Request.Form("company_id"), 0, -1, -1)
        sTerminal_id = Request.Form("terminalNumber")
        Dim nDate As Date = dbPages.TestVar(Request("startDate"), Date.MinValue, Date.MaxValue, Date.Now)
        Dim fGraph() As Telerik.Web.UI.RadChart = {Chart1, Chart2, Chart3}
        sWhere = IIf(sTerminal_id <> "", "tblCompanyTransPass.TerminalNumber='" & sTerminal_id & "'", "tblCompanyTransPass.CompanyID=" & sCompany_id)
        sWhere += " And (PaymentMethod_id = 1) AND (Currency = " & nCurrency & ") "
        For i As Integer = 0 To 2
            sTable = "tblCompanyTransPass"
            Dim lDayEnd As Integer = -i * 30, lDayStart As Integer = lDayEnd - 30
            Dim CurDt As Date = nDate.AddDays(lDayEnd).Date()
            fGraph(i).PlotArea.XAxis.AutoScale = False
            strDetails(i) = GetTotals(lDayStart, lDayEnd)
            strDetails(i) += FillGraph("Num. of refunds:", sWhere + " AND (CreditType = 0) AND (DeniedStatus = 0) ", CurDt, lDayStart, lDayEnd, fGraph(i), 0)
            strDetails(i) += FillGraph("Num. of CHB:", sWhere + " AND (CreditType > 0) AND (DeniedStatus IN (1, 2, 4, 6)) ", CurDt, lDayStart, lDayEnd, fGraph(i), 1)
            sTable = "tblCompanyTransRemoved"
            strDetails(i) += FillGraph("Num. of fraudulent:", sWhere + " AND (CreditType = 0) ", CurDt, lDayStart, lDayEnd, fGraph(i), 2)
            strDetails(i) += "<tr><td colspan=""5""><br/></td></tr>"
            strDetails(i) += "<tr><th>Grand total:</th>"
            strDetails(i) += "<td>" & FormatNumber(nSums(0), 2, TriState.True) & "%</td>"
            strDetails(i) += "<td>" & FormatNumber(nTotalTransCountAll - nSums(1), 0, TriState.True) & "</td>"
            strDetails(i) += "<td>" & FormatNumber(nSums(2), 2, TriState.True) & "%</td>"
            strDetails(i) += "<td>" & dbPages.FormatCurr(nCurrency, nTotalTransSumAll - nSums(3)) & "</td>"
            strDetails(i) += "</tr></table></td></tr></table>"
        Next
        Dim sSQL As String = "SELECT CompanyName FROM tblCompany WHERE ID=" & sCompany_id
        sPageHeading = IIf(String.IsNullOrEmpty(sTerminal_id), dbPages.ExecScalar(sSQL), "Terminal:" & sTerminal_id)
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
    <link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
    <style>
        #tblTotals TH {direction:ltr; font-size:11px; padding-right:2px; text-align:left; background-color:#ffffff; color:#000000;}
        #tblTotals TD {direction:ltr; font-size:11px; padding-right:2px; text-align:right; background-color:#ffffff; color:#000000;}
        #tblTotals #headTotals TD {direction:ltr; font-size:11px; text-align:left; vertical-align: bottom; background-color:#f5f5f5;}
        .sectionHeading {direction:ltr; font-size:12px; font-weight:bold; text-align:left; color:#000000;}
    </style>
</head>
<body>
    
    <br />
    <table align="center" style="direction:ltr;">
    <tr>
        <td align="left" id="pageMainHeadingTd">
            <span id="pageMainHeading">Risk Managment Graph</span>
            - <span><%=dbPages.dbtextShow(sPageHeading)%></span><br />
            <span id="pageMainSubHeading">Starting at <%=Request("startDate")%> and going 90 days back</span>
        </td>
    </tr>
    </table>
    <br />
    <table align="center" style="direction:ltr; " border="0">
       <tr><td valign="top"><%=strDetails(0)%></td></tr>
       <tr><td>
            <radC:RadChart ID="Chart1" runat="server" Palette="Pastel" ShadowColor="180, 0, 0, 0" Height="350px" Width="800px" Margins-Bottom="70" Margins-Left="3%" Margins-Right="1" AlternateText="" DataGroupColumn="" ImageQuality="HighSpeed" Margins-Top="5%" TextQuality="SystemDefault" RadControlsDir="~/Include/RadControls/">
                <PlotArea>
                    <EmptySeriesMessage TextBlock-Text="Did not find data for building graph" TextBlock-Appearance-TextProperties-Color="black" TextBlock-Appearance-TextProperties-Font="Arial, 12pt"></EmptySeriesMessage>
                    <DataTable Appearance-Position-X="1" Appearance-Position-Y="298" Appearance-Dimensions-Height="49" Appearance-Dimensions-Width="764"></DataTable>
                    <Appearance>
                        <Border Color="193, 214, 221" />
                        <FillStyle FillType="Solid" MainColor="250, 250, 250"></FillStyle>
                    </Appearance>
                    <XAxis Appearance-Color="193, 214, 221">
                        <AxisLabel TextBlock-Appearance-TextProperties-Font="Arial, 10pt" Appearance-RotationAngle="90">
                            <Marker Appearance-FillStyle-FillType="Solid" Appearance-FillStyle-MainColor="154, 153, 129"></Marker>
                        </AxisLabel>
                    </XAxis>
                   <YAxis Appearance-Color="193, 214, 221">
                        <AxisLabel TextBlock-Appearance-TextProperties-Font="Arial, 10pt" Appearance-RotationAngle="90">
                            <Marker Appearance-FillStyle-FillType="Solid" Appearance-FillStyle-MainColor="154, 153, 129"></Marker>
                        </AxisLabel>
                    </YAxis>
                </PlotArea>
                <ChartTitle Appearance-Visible="False" TextBlock-Text="������ �������" Appearance-Dimensions-Paddings="10" Appearance-Dimensions-Margins="10" TextBlock-Appearance-TextProperties-Font="Arial, 21.75pt" TextBlock-Appearance-TextProperties-Color="81, 103, 114">
                    <Appearance>
                        <FillStyle FillType="Solid" ></FillStyle>
                    </Appearance>
                </ChartTitle>
                <Legend Appearance-Location="InsidePlotArea" Appearance-ItemTextAppearance-TextProperties-Font="Arial, 8.25pt">
                    <Appearance>
                        <Border Color="193, 214, 221" />
                        <FillStyle FillType="Solid" MainColor="241, 253, 255"></FillStyle>
                    </Appearance> 
                </Legend>
                <Appearance>
                    <FillStyle FillType="Solid" MainColor="#ffffff"></FillStyle>
                    <Border Color="#ffffff" />
                </Appearance>
                <Series>
                    <radC:ChartSeries Name="Refund" Appearance-LabelAppearance-Visible="False">
                        <Appearance>
                            <Border Color="DimGray" />
                            <FillStyle FillType="Solid" MainColor="green" SecondColor="243, 244, 230"></FillStyle>
                        </Appearance>
                    </radC:ChartSeries>
                    <radC:ChartSeries Name="CHB" Appearance-LabelAppearance-Visible="False">
                         <Appearance>
                            <Border Color="DimGray" />
                            <FillStyle FillType="Solid" MainColor="red" SecondColor="248, 232, 227"></FillStyle>
                        </Appearance>                     
                    </radC:ChartSeries>
                    <radC:ChartSeries Name="Fradulant" Appearance-LabelAppearance-Visible="False">
                        <Appearance>
                            <Border Color="DimGray" />
                            <FillStyle FillType="Solid" MainColor="purple" SecondColor="248, 232, 227"></FillStyle>
                        </Appearance>
                    </radC:ChartSeries>
                </Series>
            </radC:RadChart>
        </td>
       </tr>
        <tr><td valign="top"><%=strDetails(1)%></td></tr>
        <tr><td>
            <radC:RadChart ID="Chart2" runat="server" Palette="Pastel" ShadowColor="180, 0, 0, 0" Height="350px" Width="800px" Margins-Bottom="70" Margins-Left="3%" Margins-Right="1" AlternateText="" DataGroupColumn="" ImageQuality="HighSpeed" Margins-Top="5%" TextQuality="SystemDefault" RadControlsDir="~/Include/RadControls/">
                <PlotArea>
                    <EmptySeriesMessage TextBlock-Text="Did not find data for building graph" TextBlock-Appearance-TextProperties-Color="black" TextBlock-Appearance-TextProperties-Font="Arial, 12pt"></EmptySeriesMessage>
                    <DataTable Appearance-Position-X="1" Appearance-Position-Y="298" Appearance-Dimensions-Height="49" Appearance-Dimensions-Width="764"></DataTable>
                    <Appearance>
                        <Border Color="193, 214, 221" />
                        <FillStyle FillType="Solid" MainColor="250, 250, 250"></FillStyle>
                    </Appearance>
                    <XAxis Appearance-Color="193, 214, 221">
                        <AxisLabel TextBlock-Appearance-TextProperties-Font="Arial, 10pt" Appearance-RotationAngle="90">
                            <Marker Appearance-FillStyle-FillType="Solid" Appearance-FillStyle-MainColor="154, 153, 129"></Marker>
                        </AxisLabel>
                    </XAxis>
                   <YAxis Appearance-Color="193, 214, 221">
                        <AxisLabel TextBlock-Appearance-TextProperties-Font="Arial, 10pt" Appearance-RotationAngle="90">
                            <Marker Appearance-FillStyle-FillType="Solid" Appearance-FillStyle-MainColor="154, 153, 129"></Marker>
                        </AxisLabel>
                    </YAxis>
                </PlotArea>
                <ChartTitle Appearance-Visible="False" TextBlock-Text="������ �������" Appearance-Dimensions-Paddings="10" Appearance-Dimensions-Margins="10" TextBlock-Appearance-TextProperties-Font="Arial, 21.75pt" TextBlock-Appearance-TextProperties-Color="81, 103, 114">
                    <Appearance>
                        <FillStyle FillType="Solid" ></FillStyle>
                    </Appearance>
                </ChartTitle>
                <Legend Appearance-Location="InsidePlotArea" Appearance-ItemTextAppearance-TextProperties-Font="Arial, 8.25pt">
                    <Appearance>
                        <Border Color="193, 214, 221" />
                        <FillStyle FillType="Solid" MainColor="241, 253, 255"></FillStyle>
                    </Appearance> 
                </Legend>
                <Appearance>
                    <FillStyle FillType="Solid" MainColor="#ffffff"></FillStyle>
                    <Border Color="#ffffff" />
                </Appearance>
                <Series>
                    <radC:ChartSeries Name="Refund" Appearance-LabelAppearance-Visible="False">
                        <Appearance>
                            <Border Color="DimGray" />
                            <FillStyle FillType="Solid" MainColor="green" SecondColor="243, 244, 230"></FillStyle>
                        </Appearance>
                    </radC:ChartSeries>
                    <radC:ChartSeries Name="CHB" Appearance-LabelAppearance-Visible="False">
                         <Appearance>
                            <Border Color="DimGray" />
                            <FillStyle FillType="Solid" MainColor="red" SecondColor="248, 232, 227"></FillStyle>
                        </Appearance>                     
                    </radC:ChartSeries>
                    <radC:ChartSeries Name="Fradulant" Appearance-LabelAppearance-Visible="False">
                        <Appearance>
                            <Border Color="DimGray" />
                            <FillStyle FillType="Solid" MainColor="purple" SecondColor="248, 232, 227"></FillStyle>
                        </Appearance>
                    </radC:ChartSeries>
                </Series>
            </radC:RadChart>
        </td>
       </tr>
       <tr><td valign="top"><%=strDetails(2)%></td></tr>
       <tr><td>
            <radC:RadChart ID="Chart3" runat="server" Palette="Pastel" ShadowColor="180, 0, 0, 0" Height="350px" Width="800px" Margins-Bottom="70" Margins-Left="3%" Margins-Right="1" AlternateText="" DataGroupColumn="" ImageQuality="HighSpeed" Margins-Top="5%" TextQuality="SystemDefault" RadControlsDir="~/Include/RadControls/">
                <PlotArea>
                    <EmptySeriesMessage TextBlock-Text="Did not find data for building graph" TextBlock-Appearance-TextProperties-Color="black" TextBlock-Appearance-TextProperties-Font="Arial, 12pt"></EmptySeriesMessage>
                    <DataTable Appearance-Position-X="1" Appearance-Position-Y="298" Appearance-Dimensions-Height="49" Appearance-Dimensions-Width="764"></DataTable>
                    <Appearance>
                        <Border Color="193, 214, 221" />
                        <FillStyle FillType="Solid" MainColor="250, 250, 250"></FillStyle>
                    </Appearance>
                    <XAxis Appearance-Color="193, 214, 221">
                        <AxisLabel TextBlock-Appearance-TextProperties-Font="Arial, 10pt" Appearance-RotationAngle="90">
                            <Marker Appearance-FillStyle-FillType="Solid" Appearance-FillStyle-MainColor="154, 153, 129"></Marker>
                        </AxisLabel>
                    </XAxis>
                   <YAxis Appearance-Color="193, 214, 221">
                        <AxisLabel TextBlock-Appearance-TextProperties-Font="Arial, 10pt" Appearance-RotationAngle="90">
                            <Marker Appearance-FillStyle-FillType="Solid" Appearance-FillStyle-MainColor="154, 153, 129"></Marker>
                        </AxisLabel>
                    </YAxis>
                </PlotArea>
                <ChartTitle Appearance-Visible="False" TextBlock-Text="������ �������" Appearance-Dimensions-Paddings="10" Appearance-Dimensions-Margins="10" TextBlock-Appearance-TextProperties-Font="Arial, 21.75pt" TextBlock-Appearance-TextProperties-Color="81, 103, 114">
                    <Appearance>
                        <FillStyle FillType="Solid" ></FillStyle>
                    </Appearance>
                </ChartTitle>
                <Legend Appearance-Location="InsidePlotArea" Appearance-ItemTextAppearance-TextProperties-Font="Arial, 8.25pt">
                    <Appearance>
                        <Border Color="193, 214, 221" />
                        <FillStyle FillType="Solid" MainColor="241, 253, 255"></FillStyle>
                    </Appearance> 
                </Legend>
                <Appearance>
                    <FillStyle FillType="Solid" MainColor="#ffffff"></FillStyle>
                    <Border Color="#ffffff" />
                </Appearance>
                <Series>
                    <radC:ChartSeries Name="Refund" Appearance-LabelAppearance-Visible="False">
                        <Appearance>
                            <Border Color="DimGray" />
                            <FillStyle FillType="Solid" MainColor="green" SecondColor="243, 244, 230"></FillStyle>
                        </Appearance>
                    </radC:ChartSeries>
                    <radC:ChartSeries Name="CHB" Appearance-LabelAppearance-Visible="False">
                         <Appearance>
                            <Border Color="DimGray" />
                            <FillStyle FillType="Solid" MainColor="red" SecondColor="248, 232, 227"></FillStyle>
                        </Appearance>                     
                    </radC:ChartSeries>
                    <radC:ChartSeries Name="Fradulant" Appearance-LabelAppearance-Visible="False">
                        <Appearance>
                            <Border Color="DimGray" />
                            <FillStyle FillType="Solid" MainColor="purple" SecondColor="248, 232, 227"></FillStyle>
                        </Appearance>
                    </radC:ChartSeries>
                </Series>
            </radC:RadChart>
        </td>
    </tr>
    </table>
    <br />
</body>
</html>
