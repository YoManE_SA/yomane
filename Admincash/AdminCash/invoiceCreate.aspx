<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="radC" %>
<%@ Import namespace="System.Data"%>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	Const DEFAULT_ISSUE_TO As String = "Type a recepient name here ..."

	Sub ResetVATPercent(ByVal o As Object, ByVal e As EventArgs)
		lblVATPercent.Text = dbPages.ExecScalar("SELECT VATamount*100 FROM tblBillingCompanys WHERE BillingCompanys_id=" & rblBillingCompany.SelectedValue)
	End Sub

	Sub SetIssueToMode(ByVal o As Object, ByVal e As EventArgs)
		ddlMerchant.Visible = (ddlIssueToMode.SelectedIndex = 0)
		txtOther.Visible = Not ddlMerchant.Visible
	End Sub

	Sub Recalculate(ByVal o As Object, ByVal e As EventArgs)
		Dim nBaseRate As Decimal = dbPages.GetCurrencyRate(ddlCurrency.SelectedValue)
		Dim nVATPercent As Decimal = CType(lblVATPercent.Text, Decimal)
		Dim nTotal As Decimal = 0
		Dim nAmount As Decimal
		'prices include VAT - reduce
		For i As Integer = 0 To gvLines.Rows.Count - 1
			nAmount = CType(gvLines.Rows(i).Cells(gvLines.Rows(i).Cells.Count - 2).Text, Decimal) * CType(gvLines.Rows(i).Cells(gvLines.Rows(i).Cells.Count - 6).Text, Decimal)  / nBaseRate
			If rblVATUsage.SelectedValue = 1 Then nAmount = nAmount / (100D + nVATPercent) * 100D
			gvLines.Rows(i).Cells(gvLines.Rows(i).Cells.Count - 1).Text = nAmount.ToString("0.00")
			nTotal += CType(gvLines.Rows(i).Cells(gvLines.Rows(i).Cells.Count - 1).Text, Decimal)
		Next
		lblTotal.Text = nTotal.ToString("0.00")
		Dim nVAT As Decimal = IIf(rblVATUsage.SelectedValue = 2, 0D, nTotal / 100D * nVATPercent)
		lblVAT.Text = nVAT.ToString("0.00")
		Dim nTotalVAT As Decimal = nTotal + nVAT
		lblTotalVAT.Text = nTotalVAT.ToString("0.00")
	End Sub
	
	Sub CreateDocument(ByVal o As Object, ByVal e As EventArgs)
		Recalculate(Nothing, Nothing)
		If gvLines.Rows.Count = 0 Then
			lblError.Text = "Cannot create document: no line is specified"
		ElseIf lblTotal.Text = 0 Then
			lblError.Text = "Cannot create document: no total calculated"
		Else
			Dim nDocumentType As Integer = rblInvoiceType.SelectedValue
			Dim nBillingCompany As Integer = rblBillingCompany.SelectedValue
			Dim nApplyVAT As Integer = IIf(rblVATUsage.SelectedValue = 2, 0, 1)
			Dim nLinesReduceVAT As Integer = IIf(rblVATUsage.SelectedValue = 1, 1, 0)
			Dim sBillTo As String = IIf(ddlIssueToMode.SelectedIndex = 1, dbPages.DBText(IIf(txtOther.Text = DEFAULT_ISSUE_TO, String.Empty, txtOther.Text)), String.Empty)
			Dim nMerchant As Integer = IIf(ddlIssueToMode.SelectedIndex = 0, dbPages.TestVar(ddlMerchant.SelectedValue, 1, 0, 0), 0)
			Dim nCurrency As Integer = ddlCurrency.SelectedValue
			Dim sUsername As String = dbPages.DBText(Security.Username)
			Dim dtDocument As Date = IIf(rdtpDocument.SelectedDate.HasValue, rdtpDocument.SelectedDate, Date.Now)
			Dim sSQL As String = "EXEC InvoiceDocumentCreate 0, " & nDocumentType & ", " & nBillingCompany & ", " & nApplyVAT & "," & _
			" " & nLinesReduceVAT & ", " & nMerchant & ", '" & sBillTo & "', " & nCurrency & ", " & dbPages.ConvertCurrencyRate(nCurrency, 0) & ", '" & sUsername & "'," & _
			" '" & dtDocument.ToString("yyyy-MM-dd HH:mm:ss") & "'"
			If nDocumentType = 4 Then sSQL &= ", 'eng'"
			Dim nReply As Integer = dbPages.ExecScalar(sSQL)
			If nReply > 0 Then
				sSQL = "SELECT id_InvoiceNumber FROM tblInvoiceDocument WHERE ID=" & nReply
				Dim nInvoiceNumber As Integer = dbPages.ExecScalar(sSQL)
				lblError.Text = rblInvoiceType.SelectedItem.Text & " #" & nInvoiceNumber & " has been successfully created. "
				lblError.Text &= "<input type=""button"" class=""buttonWhite"" value=""Open"" onclick=""PrintInvoice(" & nInvoiceNumber & ", " & nBillingCompany & ", " & nDocumentType & ");"" /> "
				lblError.Text &= "<input type=""button"" class=""buttonWhite buttonWide"" value=""Create Another"" onclick=""javascript:location.href='invoiceCreate.aspx';"" /> "
			Else
				lblError.Text = "Cannot create document: error " & nReply
			End If
		End If
	End Sub
	
	Sub ReloadLinesGrid()
		gvLines.DataBind()
		gvLines.Columns(gvLines.Columns.Count - 1).HeaderText = ddlCurrency.SelectedItem.Text & " Amount"
	End Sub

	Sub SetGridSource(ByVal o As Object, ByVal e As EventArgs)
		If Not IsNothing(o) Then
			ddlNewCurrency.SelectedIndex = ddlCurrency.SelectedIndex
			lblTotal.Text = "0.00"
			lblVAT.Text = "0.00"
			lblTotalVAT.Text = "0.00"
		End If
		dsLines.ConnectionString = dbPages.DSN
		dsLines.SelectCommand = "SELECT ID, LineNumber, il_TransactionPayID, il_Username, il_Text, il_Quantity, il_Price, il_Amount, il_CurrencyRate, CUR_ISOName, AmountConverted FROM InvoiceGetUnattendedLines("
		If ddlIssueToMode.SelectedIndex = 0 And ddlMerchant.SelectedValue <> String.Empty Then
			dsLines.SelectCommand &= ddlMerchant.SelectedValue & ", NULL"
		Else
			dsLines.SelectCommand &= "NULL, '" & IIf(txtOther.Text = DEFAULT_ISSUE_TO, String.Empty, dbPages.DBText(txtOther.Text)) & "'"
		End If
		dsLines.SelectCommand &= ", " & ddlCurrency.SelectedValue & ")"
		ReloadLinesGrid()
	End Sub

	Sub AddNewLine(ByVal o As Object, ByVal e As EventArgs)
		Dim nTransactionPay As Integer = 0
		Dim sBillTo As String = IIf(ddlIssueToMode.SelectedIndex = 1, dbPages.DBText(IIf(txtOther.Text = DEFAULT_ISSUE_TO, String.Empty, txtOther.Text)), String.Empty)
		Dim nMerchant As Integer = IIf(ddlIssueToMode.SelectedIndex = 0, dbPages.TestVar(ddlMerchant.SelectedValue, 1, 0, 0), 0)
		Dim sText As String = dbPages.DBText(txtNewText.Text)
		Dim nQuantity As Decimal = dbPages.TestVar(txtNewQuantity.Text, 1D, 0D, 0D)
		Dim nPrice As Decimal = dbPages.TestVar(txtNewPrice.Text, 1D, 0D, 0D)
		Dim nCurrency As Integer = dbPages.TestVar(ddlNewCurrency.SelectedValue, 1, 0, 0)
		Dim sUsername As String = dbPages.DBText(Security.Username)
		If sText = String.Empty Then
			lblError.Text = "Cannot add line: missing or invalid description"
		ElseIf nQuantity <= 0 Then
			lblError.Text = "Cannot add line: missing or invalid quantity"
		ElseIf nPrice <= 0 Then
			lblError.Text = "Cannot add line: missing or invalid price"
		Else
			Dim sSQL As String = "EXEC InvoiceLineCreate " & nTransactionPay & ", '" & sBillTo & "', " & nMerchant & "," & _
			" '" & sText & "', " & nQuantity & ", " & nPrice & ", " & nCurrency & ", " & dbPages.ConvertCurrencyRate(nCurrency, 0) & ", '" & sUsername & "'"
			Dim nReply As Integer = dbPages.ExecScalar(sSQL)
			If nReply > 0 Then
				txtNewText.Text = String.Empty
				txtNewQuantity.Text = String.Empty
				txtNewPrice.Text = String.Empty
				ddlNewCurrency.SelectedIndex = ddlCurrency.SelectedIndex
				ReloadLinesGrid()
				lblError.Text = "Line " & nReply & " has been successfully added."
			Else
				lblError.Text = "Cannot add line: error " & nReply
			End If
		End If
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		Dim sSQL As String, drData As SqlDataReader
		If Not Page.IsPostBack Then
			rdtpDocument.SelectedDate = Date.Now
			sSQL = "SELECT BillingCompanys_id, Name AS BillingCompanys_name FROM tblBillingCompanys ORDER BY BillingCompanys_name DESC"
			drData = dbPages.ExecReader(sSQL)
			Do While drData.Read
				rblBillingCompany.Items.Add(New ListItem(drData(1), drData(0)))
			Loop
			drData.Close()
			rblBillingCompany.SelectedIndex = 0
			sSQL = "SELECT GD_ID, GD_Text FROM GetGlobalData(" & GlobalDataGroup.DOCUMENT_TYPE & ")"
			drData = dbPages.ExecReader(sSQL)
			Do While drData.Read
				rblInvoiceType.Items.Add(New ListItem(drData(1), drData(0)))
			Loop
			drData.Close()
			rblInvoiceType.SelectedIndex = 0
			ResetVATPercent(Nothing, Nothing)
			sSQL = "SELECT ID, Replace(CompanyName, '`', '''')+' - '+LTrim(RTrim(Str(ID))) Name FROM tblCompany WHERE Len(LTrim(RTrim(CompanyName)))>2" & _
			IIf(Security.IsLimitedMerchant, " AND ID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))", String.Empty) & _
			" ORDER BY CompanyName"
			drData = dbPages.ExecReader(sSQL)
			ddlMerchant.Items.Add(New ListItem("< Select Merchant >", String.Empty))
			Do While drData.Read
				ddlMerchant.Items.Add(New ListItem(drData(1), drData(0)))
			Loop
			drData.Close()
			SetIssueToMode(Nothing, Nothing)
			txtOther.Text = DEFAULT_ISSUE_TO
            sSQL = "SELECT [CurrencyID], [CurrencyISOCode] FROM [List].[CurrencyList] ORDER BY [CurrencyID]"
			drData = dbPages.ExecReader(sSQL)
			Do While drData.Read
				ddlCurrency.Items.Add(New ListItem(drData(1), drData(0)))
				ddlNewCurrency.Items.Add(New ListItem(drData(1), drData(0)))
			Loop
			drData.Close()

			Dim sUsername As String = dbPages.DBText(Security.Username)
			Dim nFeeMonthly As Decimal = 0, nFeeTransaction As Decimal = 0, nFeeVolumePercent As Single = 0
			Dim nVolume As Decimal = 0, nTransactions As Integer = 0, sText As String
			drData = dbPages.ExecReader("SELECT * FROM tblGlobalValues")
			If drData.Read Then
				nFeeMonthly = drData("PSPFeeMonthly")
				nFeeTransaction = drData("PSPFeeTransaction")
				nFeeVolumePercent = drData("PSPFeeVolumePercent")
			End If
			drData.Close()
			Dim sDateFrom As String = "DEFAULT", sDateTo As String = "DEFAULT", sDateText As String = Date.Today.AddMonths(-1).ToString("MM/yyyy")
			Dim nPSP As Integer = dbPages.TestVar(Request("PSP"), 1, 0, -1)
			If nPSP >= 0 Then
				Dim sPSP As String = GlobalData.Value(GlobalDataGroup.PSP, nPSP).Text
				If Not String.IsNullOrEmpty(sPSP) Then
					txtOther.Text = sPSP
					ddlIssueToMode.SelectedIndex = 1
					txtOther.Visible = True
					ddlMerchant.Visible = False
					ddlCurrency.SelectedIndex = 1
					rblVATUsage.SelectedIndex = 2
					rblInvoiceType.SelectedIndex = 4
					SetGridSource(btnOpenLines, Nothing)
					dbPages.ExecSql("DELETE FROM tblInvoiceLine WHERE IsNull(il_DocumentID, 0)<1 AND il_Username='" & sUsername & "'")
					If Not (String.IsNullOrEmpty(Request("fdtrPSP$rdtpFrom")) Or String.IsNullOrEmpty(Request("fdtrPSP$rdtpTo"))) Then
						sDateFrom = Request("fdtrPSP$rdtpFrom")
						sDateTo = Request("fdtrPSP$rdtpTo")
						sDateFrom = sDateFrom.Substring(8, 2) & "/" & sDateFrom.Substring(5, 2) & "/" & sDateFrom.Substring(0, 4) & " " & sDateFrom.Substring(11, 2) & ":" & sDateFrom.Substring(14, 2)
						sDateTo = sDateTo.Substring(8, 2) & "/" & sDateTo.Substring(5, 2) & "/" & sDateTo.Substring(0, 4) & " " & sDateTo.Substring(11, 2) & ":" & sDateTo.Substring(14, 2)
						sDateText = sDateFrom & " - " & sDateTo
						sDateFrom = "Convert(datetime, '" & sDateFrom & "', 103)"
						sDateTo = "Convert(datetime, '" & sDateTo & "', 103)"
					End If
					sText = "PSP Fixed Fee - " & sDateText
					dbPages.ExecSql("EXEC InvoiceLineCreate 0, '" & sPSP & "', 0, '" & sText & "', 1, " & nFeeMonthly & ", 1, " & dbPages.ConvertCurrencyRate(1, 0) & ", '" & sUsername & "'")
					sText = "Authorized transaction fee"
					drData = dbPages.ExecReader("SELECT * FROM dbo.GetFeesPSP(" & nPSP & ", " & sDateFrom & ", " & sDateTo & ")")
					If drData.Read Then
						nTransactions = IIF(IsDBNull(drData("Transactions")), 0, drData("Transactions"))
						If nTransactions > 0 Then nVolume = drData("Volume")
					End If
					drData.Close()
					dbPages.ExecSql("EXEC InvoiceLineCreate 0, '" & sPSP & "', 0, '" & sText & "', " & nTransactions & ", " & nFeeTransaction & ", 1, " & dbPages.ConvertCurrencyRate(1, 0) & ", '" & sUsername & "'")
					sText = "Volume Fee - " & nVolume.ToString("#,0.00") & " USD x " & nFeeVolumePercent & "%"
					dbPages.ExecSql("EXEC InvoiceLineCreate 0, '" & sPSP & "', 0, '" & sText & "', 1, " & nVolume * (nFeeVolumePercent / 100) & ", 1, " & dbPages.ConvertCurrencyRate(1, 0) & ", '" & sUsername & "'")
					ReloadLinesGrid()
				End If
			Else
				Dim nManagingCompany As Integer = dbPages.TestVar(Request("ManagingCompany"), 1, 0, -1)
				If nManagingCompany >= 0 Then
					Dim sManagingCompany As String = GlobalData.Value(GlobalDataGroup.MANAGING_COMPANY, nManagingCompany).Text
					If Not String.IsNullOrEmpty(sManagingCompany) Then
						txtOther.Text = sManagingCompany
						ddlIssueToMode.SelectedIndex = 1
						txtOther.Visible = True
						ddlMerchant.Visible = False
						ddlCurrency.SelectedIndex = 1
						rblVATUsage.SelectedIndex = 2
						rblInvoiceType.SelectedIndex = 4
						SetGridSource(btnOpenLines, Nothing)
						dbPages.ExecSql("DELETE FROM tblInvoiceLine WHERE IsNull(il_DocumentID, 0)<1 AND il_Username='" & sUsername & "'")
						If Not (String.IsNullOrEmpty(Request("fdtrManagingCompany$rdtpFrom")) Or String.IsNullOrEmpty(Request("fdtrManagingCompany$rdtpTo"))) Then
							sDateFrom = Request("fdtrManagingCompany$rdtpFrom")
							sDateTo = Request("fdtrManagingCompany$rdtpTo")
							sDateFrom = sDateFrom.Substring(8, 2) & "/" & sDateFrom.Substring(5, 2) & "/" & sDateFrom.Substring(0, 4) & " " & sDateFrom.Substring(11, 2) & ":" & sDateFrom.Substring(14, 2)
							sDateTo = sDateTo.Substring(8, 2) & "/" & sDateTo.Substring(5, 2) & "/" & sDateTo.Substring(0, 4) & " " & sDateTo.Substring(11, 2) & ":" & sDateTo.Substring(14, 2)
							sDateText = sDateFrom & " - " & sDateTo
							sDateFrom = "Convert(datetime, '" & sDateFrom & "', 103)"
							sDateTo = "Convert(datetime, '" & sDateTo & "', 103)"
						End If
						sText = "Mng. Company Fixed Fee - " & sDateText
						dbPages.ExecSql("EXEC InvoiceLineCreate 0, '" & sManagingCompany & "', 0, '" & sText & "', 1, " & nFeeMonthly & ", 1, " & dbPages.ConvertCurrencyRate(1, 0) & ", '" & sUsername & "'")
						sText = "Authorized transaction fee"
						drData = dbPages.ExecReader("SELECT * FROM dbo.GetFeesManagingCompany(" & nManagingCompany & ", " & sDateFrom & ", " & sDateTo & ")")
						If drData.Read Then
							nTransactions = IIf(IsDBNull(drData("Transactions")), 0, drData("Transactions")) 
							If nTransactions > 0 Then nVolume = drData("Volume")
						End If
						drData.Close()
						dbPages.ExecSql("EXEC InvoiceLineCreate 0, '" & sManagingCompany & "', 0, '" & sText & "', " & nTransactions & ", " & nFeeTransaction & ", 1, " & dbPages.ConvertCurrencyRate(1, 0) & ", '" & sUsername & "'")
						sText = "Volume Fee - " & nVolume.ToString("#,0.00") & " USD x " & nFeeVolumePercent & "%"
						dbPages.ExecSql("EXEC InvoiceLineCreate 0, '" & sManagingCompany & "', 0, '" & sText & "', 1, " & nVolume * (nFeeVolumePercent / 100) & ", 1, " & dbPages.ConvertCurrencyRate(1, 0) & ", '" & sUsername & "'")
						ReloadLinesGrid()
					End If
				End If
			End If
		End If
		SetGridSource(Nothing, Nothing)
		Recalculate(Nothing, Nothing)
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Invoices - Manual Issuance</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminNetEng.css" />
</head>
<body>
	<script language="javascript" type="text/javascript">
		function OpenPop(sURL,sName,sWidth,sHeight,sScroll)
		{
			var placeLeft = screen.width/2-sWidth/2
			var placeTop = screen.height/2-sHeight/2
			var winPrint=window.open(sURL,sName,'TOP='+placeTop+',LEFT='+placeLeft+',WIDTH='+sWidth+',HEIGHT='+sHeight+',status=1,scrollbars='+sScroll);
		}	
		function PrintInvoice(nInvoiceNumber, nBillingCompany, nDocumentType)
		{
			var sURL="invoicePrint.asp?";
			//var sURL="../include/common_billingShow_frameset.asp?";
			sURL+="BillingCompanysID="+nBillingCompany;
			sURL+="&billingNumber="+nInvoiceNumber;
			sURL+="&invoiceType="+nDocumentType;
			OpenPop(sURL,'winInvoice'+nInvoiceNumber,700,500,1);
		}
	</script>
	<br />
	<h1><asp:label ID="lblPermissions" runat="server" /> Invoices <span style="letter-spacing:-1px;"> - Manual Issuance</span></h1>
	<br />
	<form id="frmMain" runat="server">
	    <asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    	<div class="bordered">
		<table cellpadding="0" cellspacing="0" border="0" class="noborder alignTop">
		<tr>
			<th>Document Type</th>
			<th>Issuer</th>
			<th>VAT - <span style="color:#2566AB;"><asp:Label ID="lblVATPercent" runat="server" Text="0" />%</span></th>
		</tr>
		<tr>
			<td><asp:RadioButtonList ID="rblInvoiceType" runat="server" CssClass="compactList" /></td>
			<td><asp:RadioButtonList ID="rblBillingCompany" runat="server" CssClass="compactList" AutoPostBack="true" OnSelectedIndexChanged="ResetVATPercent" /></td>
			<td>
				<asp:RadioButtonList ID="rblVATUsage" runat="server" CssClass="compactList" AutoPostBack="true">
					<asp:ListItem Value="1" Text="Prices include VAT" />
					<asp:ListItem Value="0" Text="Prices do not include VAT" Selected="True" />
					<asp:ListItem Value="2" Text="The document does not need VAT" />
				</asp:RadioButtonList>
			</td>
		</tr>
		</table>
		<table cellpadding="0" cellspacing="3" border="0" class="noborder" style="width:auto;">
		<tr>
			<th>Issue to</th>
			<th>Date</th>
			<th>Currency</th>
			<th></th>
		</tr>
		<tr>
			<td style="padding-bottom:1px;">
				<asp:DropDownList ID="ddlIssueToMode" AutoPostBack="true" runat="server" OnSelectedIndexChanged="SetIssueToMode">
					<asp:ListItem Text="Merchant" />
					<asp:ListItem Text="Other" />
				</asp:DropDownList>
				<asp:DropDownList ID="ddlMerchant" runat="server" />
				<script language="javascript" type="text/javascript" event="onfocus" for="txtOther">
					if (txtOther.value=="<%= DEFAULT_ISSUE_TO %>") txtOther.value="";
				</script>
				<script language="javascript" type="text/javascript" event="onblur" for="txtOther">
					if (txtOther.value=="") txtOther.value="<%= DEFAULT_ISSUE_TO %>";
				</script>
				<asp:TextBox ID="txtOther" runat="server" CssClass="wide" Width="370" />
			</td>
			<td>
				<radC:RadDateTimePicker DateInput-Width="90px" ID="rdtpDocument" RadControlsDir="~/Include/RadControls/" Culture="en-GB"
				 Calendar-SkinsPath="~/Include/RadControls/Calendar/Skins" Calendar-Skin="WebBlue" TimeView-Interval="00:30" TimeView-Columns="4"
				  TimeView-SkinsPath="~/Include/RadControls/Calendar/Skins" TimeView-Skin="WebBlue" TimeView-Width="200px" Calendar-Width="200px"
				   runat="server" />
			</td>
			<td><asp:DropDownList ID="ddlCurrency" runat="server" /></td>
			<td style="padding-top:1px;"><asp:Button ID="btnOpenLines" runat="server" Text="Continue &gt;&gt;" CssClass="buttonWhite buttonSemiWide" OnClick="SetGridSource" /></td>
		</tr>
		</table>
	</div>
	<div id="divAdd" style="display:none;">
		<table cellpadding="2" cellspacing="0" border="0" class="noborder" style="width:auto;">
		<tr>
			<th>Add New Item</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Currency</th>
		</tr>
		<tr>
			<td><asp:TextBox ID="txtNewText" runat="server" CssClass="wide" /></td>
			<td><asp:TextBox ID="txtNewQuantity" runat="server" CssClass="medium" /></td>
			<td><asp:TextBox ID="txtNewPrice" runat="server" CssClass="medium" /></td>
			<td style="padding-bottom:1px;"><asp:DropDownList ID="ddlNewCurrency" runat="server" /></td>
			<td><asp:Button ID="btnNewAdd" runat="server" Text="Add" CssClass="buttonWhite" OnClick="AddNewLine" /></td>
		</tr>
		</table>
		<br />
	</div>
	<asp:GridView ID="gvLines" runat="server" AutoGenerateColumns="False" CssClass="grid" SelectedRowStyle-CssClass="rowSelected" Width="100%" DataKeyNames="ID" DataSourceID="dsLines" OnDataBound="Recalculate">
	<Columns>
		<asp:CommandField HeaderText="Actions" ShowDeleteButton="true" ControlStyle-CssClass="buttonWhite buttonDelete" ButtonType="Button" ItemStyle-HorizontalAlign="Right" />
		<asp:BoundField DataField="LineNumber" HeaderText="#" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Right" />
		<asp:BoundField DataField="il_Text" HeaderText="Description" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100%" HeaderStyle-HorizontalAlign="Left" />
		<asp:BoundField DataField="il_CurrencyRate" HeaderText="Rate" ItemStyle-HorizontalAlign="Right" />
		<asp:BoundField DataField="il_Quantity" HeaderText="Qty" ItemStyle-HorizontalAlign="Center" />
		<asp:BoundField DataField="il_Price" HeaderText="Price" ItemStyle-HorizontalAlign="Right" />
		<asp:BoundField DataField="CUR_ISOName" HeaderText="Currency"  ItemStyle-HorizontalAlign="Center" />
		<asp:BoundField DataField="il_Amount" HeaderText="Amount" ItemStyle-HorizontalAlign="Right" ReadOnly="true" />
		<asp:BoundField DataField="AmountConverted" HeaderText="Converted Amount" ItemStyle-HorizontalAlign="Right" ReadOnly="true" HeaderStyle-Wrap="false" />
	</Columns>
	</asp:GridView>
	<asp:SqlDataSource ID="dsLines" runat="server" ProviderName="System.Data.SqlClient"
		DeleteCommand="DELETE FROM tblInvoiceLine WHERE ID=@ID AND il_DocumentID IS NULL">
		<DeleteParameters>
			<asp:Parameter Name="ID" />
		</DeleteParameters>
	</asp:SqlDataSource>
	<div style="text-align:right;display:none;" id="divTotals">
		<table cellpadding="0" cellspacing="0" border="0" class="total">
			<tr>
				<td class="title">Total</td>
				<td><asp:Label ID="lblTotal" runat="server" Text="0.00" /></td>
			</tr>
			<tr>
				<td class="title">Total VAT</td>
				<td><asp:Label ID="lblVAT" runat="server" Text="0.00" /></td>
			</tr>
			<tr>
				<td class="title">Total incuding VAT</td>
				<td><asp:Label ID="lblTotalVAT" runat="server" Text="0.00" /></td>
			</tr>
		</table>
	</div>
	<div style="text-align:right;display:none;padding:25px 0 1px;" id="divCreate">
		<asp:Button ID="btnRecalc" runat="server" Text="Recalculate Totals" CssClass="buttonWhite buttonWide" OnClick="Recalculate" />
		&nbsp;
		<asp:Button ID="btnCreate" Text="Create Document" CssClass="buttonWhite buttonWide" runat="server" OnClick="CreateDocument" />
	</div>
	<br /><br />
	<asp:Label ID="lblError" runat="server" ForeColor="red" />
	</form>
	<%
		Dim sDisplayTotals As String = IIf(gvLines.Rows.Count = 0, "none", "block")
		Dim sDisplayAdd As String
		If ddlIssueToMode.SelectedIndex = 0 And ddlMerchant.SelectedIndex > 0 Then
			sDisplayAdd = "block"
		ElseIf ddlIssueToMode.SelectedIndex = 1 And txtOther.Text <> String.Empty And txtOther.Text <> DEFAULT_ISSUE_TO Then
			sDisplayAdd = "block"
		Else
			sDisplayAdd = "none"
		End If
		Recalculate(Nothing, Nothing)
	%>
	<script language="javascript" type="text/javascript" defer="defer">
		divTotals.style.display="<%= sDisplayTotals %>";
		divAdd.style.display="<%= sDisplayAdd %>";
		divCreate.style.display="<%= sDisplayTotals %>";
	</script>
</body>
</html>