﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin
{
	public partial class ContentFrame : System.Web.UI.Page
	{
		public string UpdateID { get { return Request["UpdateID"]; } }
		public bool IsPopup { get{ return Request["IsRowDetails"] != "1"; } }
		protected override void OnPreInit(EventArgs e)
		{
			if (!IsPopup) MasterPageFile = "ContentRowFrame.master";
			base.OnPreInit(e);
		}

		protected override void OnInit(EventArgs e)
		{
			if (Request["Control"] != null){
				var ctl = LoadControl(Request["Control"]);
				phControl.Controls.Add(ctl);
				if (ctl is Netpay.Web.Controls.ILoadControlParameters)
					(ctl as Netpay.Web.Controls.ILoadControlParameters).LoadControlParameters(Request.QueryString);
			}
			base.OnInit(e);
		}

		protected void RegisterNotifyParent(string commandName)
		{
			ClientScript.RegisterStartupScript(this.GetType(), "RowDetailsFrame_Update_" + commandName, "var topContainer; for(var wnd = window; wnd != null; wnd = wnd.parent) { if (wnd.DetailsUpdated_" + UpdateID + ") topContainer = wnd; if (wnd == wnd.parent) break; } if (topContainer.DetailsUpdated_" + UpdateID + ") topContainer.DetailsUpdated_" + UpdateID + "('" + commandName + "'); \r\n", true);
		}

		protected override bool OnBubbleEvent(object source, EventArgs args)
		{
			var e = args as CommandEventArgs;
			if (e != null && !string.IsNullOrEmpty(e.CommandName)) RegisterNotifyParent(e.CommandName);
			return base.OnBubbleEvent(source, args);
		}
		
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (!IsPostBack) DataBind();
			else if (!string.IsNullOrEmpty(UpdateID)) RegisterNotifyParent("PostBack");
		}
	}
}