<%
'--------------------------------------------------------------------------------------------
'   Approval only fee
'--------------------------------------------------------------------------------------------
sSQL="SELECT COUNT(id) AS numApprovalOnly, Sum(netpayFee_transactionCharge) As SmAppv FROM tblCompanyTransApproval " &_
"WHERE tblCompanyTransApproval.PayID=0 AND tblCompanyTransApproval.CompanyID=" & nCompanyID & " AND Currency=" & nCurrency
If trim(request("isFilter"))="1" Then
	dDateTo = dbText(request("toDate"))
	dDateFrom = dbText(request("fromDate"))
	If IsDate(dDateFrom) then sSQL = sSQL & " And InsertDate>='" & DateValue(dDateFrom) & " 00:00:00'"
	If IsDate(dDateTo) then sSQL = sSQL & " And InsertDate<='" & DateValue(dDateTo) & " 23:59:59'"
End if
set rsDataTmp = oledbData.execute(sSQL)
if rsDataTmp("numApprovalOnly")>0 then
	%>
	<tr bgcolor="#f5f5f5">
		<td width="6" bgcolor="#484848"></td>
		<td width="3"></td>
		<td><input type="Checkbox" name="isApprovalOnlyFee" value="1"></td>
		<td class="txt11" colspan="7">
			Pre-Authorized transactions fee
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			(<%= rsDataTmp("numApprovalOnly") %> transactions)
		</td>
		<td class="txt11" nowrap style="color:#cc0000;"><%= FormatCurr(nCurrency, -rsDataTmp("SmAppv")) %></td>
		<td class="txt11" align="center" colspan="3">
			<!-- <a href="#" onclick="OpenPop('Admin_transDetail_browser.asp','fraOrderPrint',480,300,0);" class="submenu2" style="text-decoration:underline;">��� ������</a><br /> -->
		</td>
	</tr>
	<tr><td height="1" colspan="14"  bgcolor="#ffffff"></td></tr>
	<tr><td height="1" colspan="14"  bgcolor="gray"></td></tr>
	<tr><td height="1" colspan="14"  bgcolor="#ffffff"></td></tr>
	<%
end if
rsDataTmp.close
Set rsDataTmp = nothing

'--------------------------------------------------------------------------------------------
'   Failed transaction fees
'--------------------------------------------------------------------------------------------
sSQL="SELECT COUNT(id) As RCount, Sum(netpayFee_transactionCharge) As RSum FROM tblCompanyTransFail " &_
"WHERE PayID=0 AND CompanyID=" & nCompanyID & " AND Currency=" & nCurrency
If trim(request("isFilter"))="1" Then
	dDateTo = dbText(request("toDate"))
	dDateFrom = dbText(request("fromDate"))
	If IsDate(dDateFrom) then sSQL = sSQL & " And InsertDate>='" & DateValue(dDateFrom) & " 00:00:00'"
	If IsDate(dDateTo) then sSQL = sSQL & " And InsertDate<='" & DateValue(dDateTo) & " 23:59:59'"
End if
set rsDataTmp = oledbData.execute(sSQL)
if rsDataTmp("RSum") <> 0 then
	%>
	<tr bgcolor="#f5f5f5">
		<td width="6" bgcolor="#484848"></td>
		<td width="3"></td>
		<td><input type="Checkbox" name="isFailFee" value="1"></td>
		<td class="txt11" colspan="7">
			Failed transaction fees
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			(<%= rsDataTmp("RCount") %> transactions)
		</td>
		<td class="txt11" nowrap style="color:#cc0000;"><%= FormatCurr(nCurrency, -rsDataTmp("RSum")) %></td>
		<td class="txt11" align="center" colspan="3">
			<!-- <a href="#" onclick="OpenPop('Admin_transDetail_browser.asp','fraOrderPrint',480,300,0);" class="submenu2" style="text-decoration:underline;">��� ������</a><br /> -->
		</td>
	</tr>
	<tr><td height="1" colspan="14"  bgcolor="#ffffff"></td></tr>
	<tr><td height="1" colspan="14"  bgcolor="gray"></td></tr>
	<tr><td height="1" colspan="14"  bgcolor="#ffffff"></td></tr>
	<%
end if
rsDataTmp.close
Set rsDataTmp = nothing

'--------------------------------------------------------------------------------------------
'   Card storage fees
'--------------------------------------------------------------------------------------------
sSQL="SELECT COUNT(id) AS numCCStorage FROM tblCCStorage " &_
"WHERE tblCCStorage.PayID=0 AND tblCCStorage.CompanyID=" & nCompanyID
set rsDataTmp = oledbData.execute(sSQL)
if rsDataTmp("numCCStorage")>0 then
	%>
	<tr bgcolor="#f5f5f5">
		<td width="6" bgcolor="#484848"></td>
		<td width="3"></td>
		<td><input type="Checkbox" name="isCcStorageFee" value="1"></td>
		<td class="txt11" colspan="7">
			Credit card storage fee
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			(<span style="font-size:smaller;"><%= FormatCurr(nCurrency, transCcStorage) & " X " & rsDataTmp("numCCStorage") %></span>)
		</td>
		<td class="txt11" nowrap style="color:#cc0000;"><%= FormatCurr(nCurrency, -(rsDataTmp("numCCStorage") * transCcStorage)) %></td>
		<td class="txt11" align="center" colspan="3">
			<!-- <a href="#" onclick="OpenPop('Admin_transDetail_browser.asp','fraOrderPrint',480,300,0);" class="submenu2" style="text-decoration:underline;">��� ������</a><br /> -->
		</td>
	</tr>
	<tr><td height="1" colspan="14"  bgcolor="#ffffff"></td></tr>
	<tr><td height="1" colspan="14"  bgcolor="gray"></td></tr>
	<tr><td height="1" colspan="14"  bgcolor="#ffffff"></td></tr>
	<%
end if
rsDataTmp.close
Set rsDataTmp = nothing
'-----------------------------------------------------------------------------------------------------------------------------
%>