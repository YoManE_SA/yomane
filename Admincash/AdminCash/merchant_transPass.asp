<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_adoPaging.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_htmlInputs.asp"-->
<!--#include file="../include/func_transPay.asp"-->
<!--#include file="../include/func_security.asp" -->
<%
nCompanyID = TestNumVar(Request("companyID"), 0, -1, 0)
nCurrency = TestNumVar(Request("Currency"), 0, MAX_CURRENCY, 0)
sTransIdToShow = Request("transID")
If Trim(nCompanyID) = 0 Then Response.End
session("PageSize") = 25

'keep record of clicks for each company
If Trim(Request("isClickCount")) = "true" Then _
	oledbData.Execute "UPDATE tblCompany SET CountAdminView=CountAdminView+1 WHERE id=" & nCompanyID

If Trim(Request("type")) = "denied" Then _
	oledbData.Execute "UPDATE tblCompanyTransPass SET deniedstatus=" & trim(request("status")) & " WHERE id=" & trim(request("id"))

sSQL="SELECT CompanyName, RREnable, RRAutoRet, StorageFee FROM tblCompany" & _
     " Left Join Setting.SetMerchantSettlement mcs ON(tblCompany.ID = mcs.Merchant_ID And mcs.Currency_ID=" & nCurrency & ") " & _
     " WHERE ID=" & nCompanyID
set rsData2=oledbData.execute(sSQL)
If not rsData2.EOF then
	transCcStorage = rsData2("StorageFee")
	CompanyName = dbtextShow(rsData2("CompanyName"))
	RREnable = rsData2("RREnable")
	RRAutoRet = rsData2("RRAutoRet")
Else
	Response.End
End if
rsData2.close
Set rsData2 = Nothing

If trim(request("isFilter"))="1" Then
	dDateTo = dbText(request("toDate"))
	dDateFrom = dbText(request("fromDate"))
	If IsDate(dDateFrom) then sWhere = sWhere & " And tblCompanyTransPass.InsertDate>='" & DateValue(dDateFrom) & " 00:00:00'"
	If IsDate(dDateTo) then sWhere = sWhere & " And tblCompanyTransPass.InsertDate<='" & DateValue(dDateTo) & " 23:59:59'"
	If Request("TransID") <> "" Then sWhere = sWhere & " And tblCompanyTransPass.ID IN(" & Request("TransID") & ")"
	If Request("PaymentMethod") <> "" And Request("PaymentMethod") <> "1, 2, 3, 15, 20, 21, 22, 23, 24, 25, 26, 28, 29" Then _
		sWhere = sWhere & " And PaymentMethod IN(" & Request("PaymentMethod") & ")"
	If Request("CreditType") <> "" And Request("CreditType") <> "1, 2, 6, 8, 0" Then _
		sWhere = sWhere & " And CreditType IN(" & Request("CreditType") & ")"
	If Request("PaymentMethod_ID") <> "" Then sWhere = sWhere & " And PaymentMethod_id IN(" & Request("PaymentMethod_ID") & ")"
	If Request("TypeTerminal") <> "" Then sWhere = sWhere & " And (tblCompanyTransPass.TerminalNumber='" & Request("TypeTerminal") & "' OR tblCompanyTransPass.TerminalNumber='')"
	If Request("DeniedStatus") <> "" Then sWhere = sWhere & " And DeniedStatus " & IIF(Request("DeniedStatus") = "Y", ">0", "IN(" & Request("DeniedStatus") & ")")
End if
sQueryFilter = "&" & UrlWithout(Request.QueryString, "page")
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<script language="JavaScript" src="../js/jquery-1.3.2.min.js"></script>
	<script language="JavaScript" src="../js/jquery.shiftcheckbox.js"></script>
	<script language="JavaScript" src="../js/func_common.js"></script>
	<link rel="stylesheet" type="text/css" media="all" href="../StyleSheet/calendar-blue.css" title="winter" />
	<style type="text/css">
		.Bsum{
			background-image: url("../images/icon_sum.gif");
			background-repeat: no-repeat; background-position: 105px; 
			width: 123px; height: 24px; text-align: left;
			background-color:#e9e9e9; font-size:10px;
			padding-left: 3px; padding-right: 3px; cursor: hand;
		}
		.Barchive{
			width: 86px; height: 24px; text-align: center;
			background-color:#ffffff; font-size:10px;
			padding-left: 3px; padding-right: 3px; cursor: hand;
		}
	</style>
	<script type="text/javascript" src="../js/calendar.js"></script>
	<script type="text/javascript" src="../js/calendar-he.js"></script>
	<script type="text/javascript" src="../js/calendar-setup.js"></script>
	<script language="JavaScript">
		var checkflag = "false";
		function check() {
			if (checkflag == "false") {
				if(frmCompanyPay.ID) { // obj exist
				if(frmCompanyPay.ID.length) { // means if(obj.length > 0)
					for (i = 0; i < frmCompanyPay.ID.length; i++) 
					frmCompanyPay.ID[i].checked = true;
				}
				else { // means not an array (!obj.length)
					frmCompanyPay.ID.checked = true
				}}
				if(frmCompanyPay.IDpayments) {
				if(frmCompanyPay.IDpayments.length) {
					for (i = 0; i < frmCompanyPay.IDpayments.length; i++) 
					frmCompanyPay.IDpayments[i].checked = true;
				}
				else {
					frmCompanyPay.IDpayments.checked = true
				}}
				checkflag = "true";
				return "UNCHECK ALL";
			}
			else {
				if(frmCompanyPay.ID) {
				if(frmCompanyPay.ID.length) {
					for (i = 0; i < frmCompanyPay.ID.length; i++) 
					frmCompanyPay.ID[i].checked = false;
				}
				else {
					frmCompanyPay.ID.checked = false
				}}
				if(frmCompanyPay.IDpayments) {
				if(frmCompanyPay.IDpayments.length) {
					for (i = 0; i < frmCompanyPay.IDpayments.length; i++) 
					frmCompanyPay.IDpayments[i].checked = false;
				}
				else {
					frmCompanyPay.IDpayments.checked = false
				}}
				checkflag = "false";
				return "CHECK ALL";
			}
		}

		function CheckPaid() {
			var checkboxes = document.getElementsByTagName("INPUT");
			var chk;
			for (var i = 0; i < checkboxes.length; i++) {
				chk = checkboxes[i];
				if ((chk.name == "ID") && (chk.className.toUpperCase().indexOf("PAIDEPA") >= 0)) chk.checked = true;
			}
		}

		function UncheckUnpaid() {
			var checkboxes = document.getElementsByTagName("INPUT");
			var chk;
			for (var i = 0; i < checkboxes.length; i++) {
				chk = checkboxes[i];
				if ((chk.name == "ID") && (chk.className.toUpperCase().indexOf("PAIDEPA") < 0)) chk.checked = false;
			}
		}

		function CheckForm()
		{
			if(frmCompanyPay.ID) {
			if(frmCompanyPay.ID.length) {
				for (i = 0; i < frmCompanyPay.ID.length; i++) 
					if(frmCompanyPay.ID[i].checked == true) {
						return true;
					}
			}
			else {
				if(frmCompanyPay.ID.checked == true) {
					return true;
				}
			}}
			if(frmCompanyPay.IDpayments) {
			if(frmCompanyPay.IDpayments.length) {
				for (i = 0; i < frmCompanyPay.IDpayments.length; i++) 
					if(frmCompanyPay.IDpayments[i].checked == true) {
						return true;
					}
			}
			else {
				if(frmCompanyPay.IDpayments.checked == true) {
					return true;
				}
			}}
			if(frmCompanyPay.isApprovalOnlyFee) {
				if(frmCompanyPay.isApprovalOnlyFee.checked == true) {
					return true;
				}
			}
			if(frmCompanyPay.isCcStorageFee) {
				if(frmCompanyPay.isCcStorageFee.checked == true) {
					return true;
				}
			}
			if(frmCompanyPay.isFailFee) {
				if(frmCompanyPay.isFailFee.checked == true) {
					return true;
				}
			}
			alert('Need to choose transactions to be settled');
			return false;
		}
		
		function toggleInfo(TransId, PaymentMethod, bkgColor) {			
			objRefA = document.getElementById('Row' + TransId + 'A')
			objRefB = document.getElementById('Row' + TransId + 'B')			
			if (objRefB && objRefB.innerHTML == '') {
				if (objRefA) { objRefA.style.display = 'none'; }
				if (PaymentMethod >= <%= PMD_EC_MIN %>)
				{
					UrlAddress = 'trans_detail_eCheck.asp';
				}
				else
				{
					switch (PaymentMethod)
					{
						case '4':
							UrlAddress = 'trans_detail_RollingRes.asp';
							break;
						case '2':
							UrlAddress = 'Trans_DetailAdmin.asp';
							break;
						default:
							UrlAddress = 'trans_detail_cc.asp';
					}
				}
				if (objRefB) {
						objRefB.innerHTML = '<iframe framespacing="0" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" width="100%" height="0px" src="' + UrlAddress + '?transID=' + TransId + '&PaymentMethod=' + PaymentMethod + '&bkgColor=' + bkgColor + '&TransTableType=pass" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>';
					}
			}
			else {
				if (objRefA) { objRefA.style.display = ''; }
				if (objRefB) { objRefB.innerHTML = ''; }
			}
		}

		function showFrameTotals(fQueryFilter)
		{
			objIFrame = document.getElementById('FrameTotals')
			objIFrame.src = '../include/common_totalTransShow.asp?LNG=1' + fQueryFilter + '&isIframe=1';
			objIFrame.style.margin = '20px 0px';
			objIFrame.style.display = '';
		}

		function showFrameTotals2(fQueryFilter) {
			objIFrame = document.getElementById('FrameTotals')
			objIFrame.src = '../include/common_totalTransShowV2.aspx?LNG=1' + fQueryFilter + '&isIframe=1';
			objIFrame.style.margin = '20px 0px';
			objIFrame.style.display = '';
		}

		$(document).ready(
        function() {
        	$('.shiftCheckbox').shiftcheckbox();
        });
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="1">
		<tr>
			<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
			<td width="60" bgcolor="#edfeed">Debit</td>
			<td width="10"></td>
			<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
			<td width="60" bgcolor="#edfeed" style="color:#cc0000;">Refund</td>
			<td width="10"></td>
			<td width="6" background="../images/green_strip_sqr.gif"></td><td width="1"></td>
			<td width="90" bgcolor="#edfeed">Refund Source</td>
			<td width="10"></td>
			<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
			<td width="60" bgcolor="#ffebe1">Denied</td>
			<td width="10"></td>
			<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
			<td width="60" bgcolor="#fff3dd" style="font-size:10px">Clarify</td>
			<td width="10"></td>
			<td width="6" bgcolor="#66cc66">X</td><td width="1"></td>
			<td width="64" bgcolor="#ffffff" style="color:#6b6b6b;">Test Only</td>
			<td width="10"></td>
			<td width="6" bgcolor="#003399"></td><td width="1"></td>
			<td width="60" bgcolor="#eeeffd">Admin</td>
			<td width="10"></td>
			<td width="6" bgcolor="#484848"></td><td width="1"></td>
			<td width="60" bgcolor="#f5f5f5">System</td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<br />
<table width="95%" border="0" cellspacing="2" cellpadding="1" align="center">
<tr>
	<td id="pageMainHeadingTd">
		<span id="pageMainHeading">UNSETTLED BALANCE (<%= GetCurText(nCurrency) %>) -</span> <span class="txt13"><%= uCase(CompanyName) %></span>
	</td>
	<td align="right" class="txt13" valign="top">
		<% If IsDate(dDateTo) OR IsDate(dDateFrom) then response.write "Transactions from " & dDateFrom & " To " & dDateTo %>
	</td>			
</tr>
</table>
<br />
<%
pageSecurityLevel = 0
pageSecurityLang = "ENG"
pageClosingTags = ""
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)

Set rsData=server.createobject("adodb.recordset")
GetDescConstArray rsData, GGROUP_DeniedStatus, 1, "", DeniedStatusDesc, VbNull

sSQL="SELECT List.TransSource.Name AS TransTypeName, pm.name AS 'pm_Name', tblCreditCard.CCTypeID, IsNull(tblLogImportEPA.IsPaid, 0) IsPaidEPA," &_
	"tblCreditCard.BINCountry, List.TransCreditType.Name AS CreditName, tblDebitTerminals.dt_name, tblCompanyTransPass.* " &_
	"FROM tblCompanyTransPass " &_
	"LEFT JOIN List.PaymentMethod AS pm ON tblCompanyTransPass.PaymentMethod = pm.PaymentMethod_id " &_
	"LEFT JOIN List.TransCreditType ON(tblCompanyTransPass.CreditType = List.TransCreditType.TransCreditType_id) " &_
	"LEFT JOIN tblCreditCard ON(tblCompanyTransPass.CreditCardID = tblCreditCard.ID) " &_
	"LEFT JOIN List.TransSource ON(tblCompanyTransPass.TransSource_id = List.TransSource.TransSource_id) " &_
	"LEFT JOIN tblDebitTerminals ON(tblCompanyTransPass.terminalNumber = tblDebitTerminals.terminalNumber) " &_
	"LEFT JOIN tblLogImportEPA ON tblCompanyTransPass.ID=tblLogImportEPA.TransID AND 1=tblLogImportEPA.Installment " &_
	"WHERE tblCompanyTransPass.Currency=" & nCurrency & " AND tblCompanyTransPass.CompanyID=" & nCompanyID & " AND UnsettledInstallments <> 0 " & sWhere &_
	" ORDER BY tblCompanyTransPass.InsertDate DESC"
call openRsDataPaging(session("PageSize"))
	
If NOT rsData.EOF Then

	Set fsServer=Server.CreateObject("Scripting.FileSystemObject")
	
	If Request("isTransCheck") Then
		sFromDateSelect = IIf(Trim(Request("fromDateSelect"))<>"", Request("fromDateSelect") & " 00:00:01", "01/01/2000" & " 00:00:01")
		sToDateSelect = IIf(Trim(Request("toDateSelect"))<>"", Request("toDateSelect") & " 23:59:59", "01/01/2000" & " 23:59:59")
	End if
	%>
	
	<form action="merchant_payCreate.asp" method="post" name="frmCompanyPay">
	<table class="formNormal" width="95%" align="center" cellpadding="1" cellspacing="0">
	<tr>
		<th colspan="2"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></th>
		<th width="40">Pay<br /></th>
		<th valign="bottom">ID</th>
		<th>Date<br /></th>
		<th>Payment Method<br /></th>
		<th>Credit Type<br /></th>
		<th>Ins.<br /></th>
		<th>Amount<br /></th>
		<th>Source<br /></th>
		<th width="60" style="text-align:center;">Debit<br /></th>
		<th>Terminal<br /></th>
		<th width="60" style="text-align:center;">Info<br /></th>
	</tr>
	<tr><td height="3"></td></tr>
	<!--#include file="merchant_transPass_feesInc.asp"-->
	<%
	nCount=0
		
	rsData.MoveFirst
	Do until rsData.EOF

		nCount=nCount+1
		bShowOriginalCreditTrans = false
		bShowCheckbox = True
		bShowBorder = false

		'nPercentsSave = rsData("Percents") ' ���� ���� ������� ����� �����
		'nPercents = rsData("Percents") ' ���� ���� ������� ����� �������
		'if nPercents>0 then nPercents = rsData("Percents")+nPrimePercent '	�� ���� ����� ���� � 0 ������ �����
		
		if trim(sTransIdToShow) = trim(rsData("ID")) then bShowBorder = true '��� ���� ����� �� ����
		
		If rsData("PaymentMethod") >= 14 Then
			If rsData("CreditType") = 0 Then
				If int(rsData("originalTransID")) > 0 Then bShowOriginalCreditTrans = True
			End if
			Call Fn_chkWasRefund(rsData("ID")) ' ���� �� ���� ���� ����� ����� ���� �� ��
		End if
		
		'Get dispaly Param
		Call Fn_TransParam(rsData, sTrBkgColor, sTdBkgColorFirst, sTdFontColor, sPayStatusText, bShowDeniedOptions, bAllowDelete)
		'Mark checkbox according to form selection
		sTrStyle = ""
		sCheckboxTxt = ""
		bIsSelectCheckbox = false
		if request("isTransCheck") then
			If trim(request("isUseOnlyDay")) = "1" then
				If day(CDate(rsData("InsertDate")))>=day(CDate(sFromDateSelect)) AND day(CDate(rsData("InsertDate")))<=day(CDate(sToDateSelect)) then
					sCheckboxTxt = "checked"
					bIsSelectCheckbox = true
				End if
			Else
				If CDate(rsData("InsertDate"))>=CDate(sFromDateSelect) AND CDate(rsData("InsertDate"))<=CDate(sToDateSelect) then
					sCheckboxTxt = "checked"
					bIsSelectCheckbox = true
				End if
			End if
		end if
	    If rsData("deniedstatus") = DNS_DupFoundValid Or rsData("deniedstatus") = DNS_DupFoundUnValid Then _
	        sTrStyle = "background-image:url('\Images\img\bkg_slash.gif');"
		
		If nCount<>1 Then
			Response.Write("<tr><td height=""1"" colspan=""13""	bgcolor=""#ffffff""></td></tr>")
			Response.Write("<tr><td height=""1"" colspan=""13""	bgcolor=""silver""></td></tr>")
			Response.Write("<tr><td height=""1"" colspan=""13""	bgcolor=""#ffffff""></td></tr>")
		End If

		If bShowBorder Then
			Response.Write("<tr><td height=""2"" colspan=""13""	bgcolor=""#000000""></td></tr>")
		End If
		%>
		<tr bgcolor="<%= sTrBkgColor %>" style="<%=sTrStyle%>">
			<%
			If trim(sTdBkgColorFirst)="green_strip_sqr" Then
				Response.Write("<td width=""4"" background=""../images/green_strip_sqr.gif""></td>")
			Else
				sTdSymbolFirst = ""
				If rsData("isTestOnly") Then sTdSymbolFirst = "X"
				Response.Write("<td class=""txt12b"" width=""4"" bgcolor=""" & sTdBkgColorFirst & """>" & sTdSymbolFirst & "</td>")
			End If
			%>
			<td width="3"></td>
			<td>
				<%
				If bShowCheckbox Then
					sCssClassPaid=IIf(rsData("IsPaidEPA"), " paidEPA", "")
					%>
					<input type="Checkbox" name="ID" value="<%= rsData("ID") %>" style="background-color:<%= sTrBkgColor %>;" <%= sCheckboxTxt %> class="shiftCheckbox option<%= sCssClassPaid %>">
					<input type="Hidden" name="Percents<%= rsData("ID") %>" value="0"><%'= nPercentsSave %>
					<%
				End If
				%>
			</td>
			<td nowrap style="color:<%= sTdFontColor %>;"><%= rsData("id") %><br /></td>
			<td nowrap style="color:<%= sTdFontColor %>;"><%= FormatDatesTimes(rsData("InsertDate"),1,1,0) %><br /></td>
			<td style="color:<%= sTdFontColor %>;">
				<%
				If rsData("PaymentMethod")>=2 Then
					Response.Write("<img src=""/NPCommon/ImgPaymentMethod/23X12/" & rsData("PaymentMethod") & ".gif"" alt=""" & rsData("pm_Name") & """ align=""middle"" border=""0"" /> &nbsp;")
					If Trim("" & rsData("BinCountry")) <> "" Then
						Response.Write("<img src=""/NPCommon/ImgCountry/18X12/" & rsData("BinCountry") & ".gif"" align=""middle"" border=""0"" />")
						Response.Write(" &nbsp;" & rsData("BinCountry"))
					End If
				End If
				Response.Write(" " & Trim("" & rsData("PaymentMethodDisplay")))
				%>
			</td>
			<td style="color:<%= sTdFontColor %>;"><%= rsData("CreditName") %><br /></td>
			<td style="color:<%= sTdFontColor %>;"><%= rsData("Payments") %><br /></td>
			<td nowrap dir="ltr" style="color:<%= sTdFontColor %>;">
				<%
				If (rsData("PaymentMethod") = PMD_ManualFee Or rsData("PaymentMethod") = PMD_SystemFees) And rsData("netpayFee_transactionCharge") <> 0 Then _
					Response.Write(FormatCurrWCT(nCurrency, rsData("netpayFee_transactionCharge"), 0)) _
				Else Response.Write(FormatCurrWCT(nCurrency, rsData("Amount"), rsData("CreditType")))
				%>
			</td>
			<td style="color:<%= sTdFontColor %>;"><%= rsData("TransTypeName") %><br /></td>
			<td style="text-align:center;">
				<%
				If rsData("PaymentMethod")>14 AND trim(rsData("DebitCompanyID"))<>"" Then
					If fsServer.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & trim(rsData("DebitCompanyID")) & ".gif")) Then
						Response.write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(rsData("DebitCompanyID")) & ".gif"" align=""middle"" border=""0"" />")
					End if
				End if
				%>
			</td>
			<td>
				<%
				If rsData("PaymentMethod")>14 Then
					If Trim(rsData("dt_name")) <> "" Then
						If int(len(dbtextShow(rsData("dt_name"))))<16 Then
							response.write dbTextShow(rsData("dt_name")) & "<br />"
						Else
							response.write "<span title="""& dbtextShow(rsData("dt_name")) & """>" & left(dbtextShow(rsData("dt_name")),15) & "..</span><br />"
						End If
					Else
						response.write(rsData("terminalNumber"))
					End if
				End if
				%>
			</td>
			<td style="text-align:center;">
				<%
				If rsData("PaymentMethod")>14 Or rsData("PaymentMethod") = 4 Or rsData("PaymentMethod") = 2 Then
					%>
					<a onclick="toggleInfo('<%= rsData("ID") %>','<%= rsData("PaymentMethod") %>','<%= replace(sTrBkgColor,"#","") %>')" name="<%= rsData("ID") %>" class="go" style="cursor:pointer; font-size:11px;"><img src="../images/b_showWhiteEng.gif" align="middle" /></a><br />
					<%
				End If
				%>
			</td>
		</tr>
		<%
		If bShowBorder then
			%>
			<tr><td height="2" colspan="13"	bgcolor="#000000"></td></tr>
			<%
		End If
		
		If bShowOriginalCreditTrans Then
			sSQL="SELECT InsertDate, PayID, Amount, CreditType FROM tblCompanyTransPass WHERE id=" & rsData("originalTransID")
			set rsData5=oledbData.execute(sSQL)
			If Not rsData5.EOF Then
				sCreditTransType = "partial"
				If rsData5("Amount")=rsData("Amount") Then sCreditTransType="Full"
				sShowLink = False
				If Trim(sPayStatusText) = "Unsettled" Then sShowLink = True
				%>
				<tr>
					<td colspan="3" style="background-color:<%= sTrBkgColor %>;"></td>
					<td colspan="11" style="background-color:<%= sTrBkgColor %>;">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td style="color:<%= sTdFontColor %>;">
								<%= sCreditTransType %> refund of transaction <%= rsData("originalTransID") %>
								from <%=FormatDatesTimes(rsData5("InsertDate"),1,1,0)%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Original transaction is <%= sPayStatusText %>
								<% If sShowLink then %>
								- <a href="#<%= rsData("originalTransID") %>" style="font-size:11px; text-decoration:underline; color:<%= sTdFontColor %>;">See Transaction</a>
								<% End If %>
								<br />
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<%
			End if
			rsData5.close
			Set rsData5 = nothing
		End if
		
		If (rsData("PaymentMethod")=PMD_Admin OR rsData("PaymentMethod")=PMD_SystemFees) AND Trim(rsData("Comment"))<>"" Then
		%>
		<tr>
			<td colspan="3" bgcolor="<%= sTrBkgColor %>"></td>
			<td colspan="11" dir="ltr"	bgcolor="<%= sTrBkgColor %>">
				<%
				Response.Write(dbtextShow(rsData("Comment")))
				If rsData("PaymentMethod")=PMD_SystemFees Then Response.Write(" &nbsp;&nbsp; " & FormatCurrWCT(nCurrency, rsData("HandlingFee"), rsData("CreditType")))
				%>
			</td>
		</tr>
		<%
		End if 
		
		If bShowDeniedOptions Then
		%>
		<tr>
			<td colspan="3" bgcolor="<%= sTrBkgColor %>"></td>
			<td colspan="11" bgcolor="<%= sTrBkgColor %>">
				<table dir="ltr" align="left" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td><%=DeniedStatusDesc(rsData("deniedstatus"))%></td>
					<td width="25"></td>
					<td>
						<% If rsData("deniedstatus")=DNS_UnSetInVar then %>
						<input type="radio" name="deniedID<%= rsData("ID") %>" value="2" style="background-color:<%= sTrBkgColor %>;">
						Still waiting
						<% End If %>
						<input type="radio" checked name="deniedID<%= rsData("ID") %>" value="<% if rsData("deniedstatus")=DNS_SetInVar then %>6<% Else %>4<% End If %>" style="background-color:<%= sTrBkgColor %>;">
						Found to be a charge back
						<input type="radio" name="deniedID<%= rsData("ID") %>" value="<% if rsData("deniedstatus")=DNS_SetInVar then %>5<% Else %>3<% End If %>" style="background-color:<%= sTrBkgColor %>;">
						Found to be ok
					</td>
					<%
					If trim(rsData("DeniedAdminComment"))<>"" then
						%>
						<td width="20"></td>
						<td>(<%= dbTextShow(rsData("DeniedAdminComment")) %>)<br /></td>
						<%
					end if
					%>
				</tr>
				</table>
			</td>
		</tr>
		<%
		End If

		if rsData("CreditType")="8" then ' ���� ���� ��������
			sSQL="SELECT * FROM tblCompanyTransInstallments WHERE PayID=0 AND transAnsID=" & rsData("id") & " ORDER BY id"
			set rsDataPayments=oledbData.execute(sSQL)
			If not rsDataPayments.EOF Then
				nCountPayments=0
				sFirstPayment = false
				%>
				<tr>
					<td id="Row<%= rsData("ID") %>A" colspan="13" style="padding-left:72px;"	bgcolor="<%= sTrBkgColor %>">
						<table align="left" cellspacing="0" cellpadding="0" border="0">
						<%
						Do until rsDataPayments.EOF
							nCountPayments=nCountPayments+1
							sCheckboxTxt = ""
							if bIsSelectCheckbox then
								If nCountPayments=1 then sCheckboxTxt = "checked"
							end if
							if nCountPayments=1 AND left(rsDataPayments("comment"),2)="1/" then sFirstPayment = true
							%>
							<tr>
								<td>
									<input type="Checkbox" name="IDpayments" value="<%= trim(rsDataPayments("ID")) %>" style="background-color:<%= sTrBkgColor %>;" <%= sCheckboxTxt %> class="shiftCheckbox">
									<input type="Hidden" name="Percents<%= rsDataPayments("ID") %>" value="0"><%'= nPercentsSave %>
								</td>
								<td>Installment <%= rsDataPayments("comment") %></td>
								<td width="20"></td>
								<td>Amount <%=FormatCurr(nCurrency, rsDataPayments("Amount"))%></td>
								<td width="20"></td>
								<td>Min Pay Date <%=Left(rsDataPayments("MerchantPD"), 10)%></td>
							</tr>
							<%
							rsDataPayments.movenext
						Loop
						%>
						</table>
					</td>
				</tr>
				<%
			End if
			rsDataPayments.close
			set rsDataPayments = nothing
		End if
		
		response.write "<tr><td id=""Row" & rsData("ID") & "B"" colspan=""13""></td></tr>"

	rsData.movenext
	loop
	%>
	<tr><td height="1" colspan="13"	bgcolor="#ffffff"></td></tr>
	<tr><td height="1" colspan="13"	bgcolor="silver"></td></tr>
	<tr><td height="1" colspan="13"	bgcolor="#ffffff"></td></tr>
	</table>
	
	<iframe id="FrameTotals" align="center" frameborder="0" style="display:none; border:1px solid gray;" width="95%" height="0" scrolling="Auto" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>
	<!--<iframe align="center" frameborder="0" style="border:1px solid gray;" width="95%" height="0" scrolling="Auto" src="../include/common_totalTransShow.asp?LNG=1&<%= sQueryFilter %>&isIframe=1" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe><br />-->
		
	<br />
	<table width="95%" align="center" border="0" cellpadding="1" cellspacing="0">
	<tr>
		<td align="left" class="txt14" valign="middle">
			<!--<input type="button" class="Bsum" value="SHOW TOTALS" onclick="OpenPop('../include/common_totalTransShow.asp?LNG=1&<%= sQueryFilter %>&isNotCalcAll=<%= sIsNotCalcAll %>&isIframe=0','fraOrderPrint',750,300,0);">-->
			<input type="button" class="Bsum" value="SHOW TOTALS" onclick="showFrameTotals('<%= sQueryFilter %>'); /*this.style.display='none';*/">
			<% If PageSecurity.IsMemberOf("Dev. Support") Then %>
				<input type="button" class="Bsum" value="SHOW TOTALS 2" onclick="showFrameTotals2('<%= sQueryFilter %>'); /*this.style.display='none';*/">
			<% End If %>
		</td>
		<td align="right">
			<table align="right" border="0" cellpadding="1" cellspacing="0"> 
			<tr><td><% call showPaging("merchant_transPass.asp", "Eng", sQueryFilter) %></td></tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
	<table width="95%" style="border:1px solid silver;" border="0" cellspacing="2" cellpadding="2" align="center">
	<tr>
		<td bgcolor="#f5f5f5">
			<table border="0" cellspacing="0" cellpadding="0" dir="ltr">
			<tr>
				<td class="txt12">
					<%
					If RREnable Then
						Response.Write("<input type=""checkbox"" onclick=""return false;"" " & IIf(RREnable, "checked", "") & " name=""IncRR"" value=""1"" />Add RR ")
						Response.Write("<input type=""checkbox"" onclick=""return false;"" " & IIf(RRAutoRet, "checked", "") & " name=""RetRR"" value=""1"" />Release RR ")
					End if
					%>
				</td>
				<td style="padding-left:35px;">
					<%
					sDay=""
					for i=1 to 31
						sDay=sDay & i & ";"
					next
					sMonth=""
					for i=1 to 12
						sMonth=sMonth & i & ";"
					next
					sYear=""
					For i = year(date()) To year(date())+5
						sYear = sYear & i & ";"
					Next
					Response.Write(" " & PutComboDefault("Day", sDay, sDay, ";", Day(now())))
					Response.Write(" " & PutComboDefault("Month", sMonth, sMonth, ";", Month(now())))
					Response.Write(" " & PutComboDefault("Year", sYear, sYear, ";", Year(now())))
					%>
					<input type="button" class="buttonRed" value="NEW SETTLEMENT" onclick="frmCompanyPay.Action.value='New'; if(CheckForm()){document.frmCompanyPay.submit();}else{return false;};">
				</td>
				<%
				sSQL = "SELECT TOP 10 tblTransactionPay.PayDate, tblTransactionPay.id FROM tblTransactionPay Left JOIN tblWireMoney ON (tblTransactionPay.id = tblWireMoney.WireSourceTbl_id AND tblWireMoney.WireType = 1) " &_
				"WHERE (tblTransactionPay.CompanyID = " & nCompanyID & ") AND (tblWireMoney.WireStatus <= 2) AND (tblTransactionPay.isBillingPrintOriginal = 0) AND (tblTransactionPay.Currency = " & nCurrency & ") ORDER BY tblTransactionPay.PayDate DESC"
				set rsData5=oledbData.execute(sSQL)
				if NOT rsData5.EOF then
					%>
					<td style="padding-left:35px;">
						<select name="PayID">
							<%
							Do until rsData5.EOF
								sPayDate = day(rsData5("PayDate")) & "/" & month(rsData5("PayDate")) & "/" & right(year(rsData5("PayDate")),2) & "&nbsp;&nbsp;"
								sPayDate = sPayDate & FormatDateTime(rsData5("PayDate"),4)
								%>
								<option value="<%= rsData5("ID") %>"><%= rsData5("ID") & " " & sPayDate %>
								<%
							rsData5.movenext
							loop
							%>
						</select>
						<input type="button" class="buttonRed" value="ADD TO EXISTING" onclick="frmCompanyPay.Action.value=''; if(CheckForm()){document.frmCompanyPay.submit();}else{return false;};">
					</td>
					<%
				end if
				rsData5.close
				Set rsData5 = nothing
				%>
			</tr>
			</table>
			<input type="Hidden" value="<%= nCompanyID %>" name="companyID" />
			<input type="Hidden" value="1" name="TerminalType" /><!--rsData("TerminalType")-->
			<!--<input type="Hidden" value="< %= nPrimePercent % >" name="PrimePercent" />-->
			<input type="Hidden" value="<%= nCurrency %>" name="Currency" />
			<input type="Hidden" value="" name="Action" />
			</form>
		</td>
	</tr>
	</table>
	
	<table width="95%" style="border:1px solid silver; margin-top:8px;" border="0" cellspacing="2" cellpadding="2" align="center">
	<tr>
		<td>
			<form action="merchant_transPass.asp" method="get" name="frmTransCheck">
			<input type="hidden" name="companyID" value="<%= nCompanyID %>">
			<input type="hidden" name="Currency" value="<%= nCurrency %>">
			<input type="hidden" name="isTransCheck" value="true">
			<input type="hidden" name="fromDate" value="<%= dbText(request("fromDate")) %>">
			<input type="hidden" name="toDate" value="<%= dbText(request("toDate")) %>">
			<table border="0" cellspacing="0" cellpadding="0" dir="ltr">
			<tr>
				<td class="txt12b">Checking transactions</td>
				<td width="20"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"></td>
				<td class="txt13">
					<img src="../images/calendar_icon.gif" id="fromDateSelect_trigger_c" width="16" height="15" border="0" style="cursor:pointer;">
					<input type="text" class="inputData" tabindex="1" dir="ltr" size="10" name="fromDateSelect" value="<%= dbText(request("fromDateSelect")) %>">
					To <img src="../images/calendar_icon.gif" id="toDateSelect_trigger_c" width="16" height="15" border="0" style="cursor:pointer;">
					<input type="text" class="inputData" tabindex="2" dir="ltr" size="10" name="toDateSelect" value="<%= dbText(request("toDateSelect")) %>"><br />
				</td>
				<td></td>
				<td style="padding-left:8px;" class="txt13">
					<input type="checkbox" name="isUseOnlyDay" value="1" tabindex="3" <%If trim(request("isUseOnlyDay")) = "1" Then%>checked<%End if%>	/>
					Check only days &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td style="padding-left:8px;">
					<input type="submit" class="button1" tabindex="4" value=" CHECK " /> &nbsp;
					<input class="button1" type="button" value="CHECK ALL" onclick="this.value=check();" />
					&nbsp; | &nbsp;
					<input class="button1" type="button" value="Check Paid" onclick="CheckPaid();" />
					<input class="button1" type="button" value="Uncheck Unpaid" onclick="UncheckUnpaid();" />
				</td>
			</tr>
			</table>
			</form>
		</td>
	</tr>
	</table>
	<script type="text/javascript">
		Calendar.setup({
			inputField	 :	"fromDateSelect",	 // id of the input field
			ifFormat		:	"%d/%m/%Y",		// format of the input field
			button		 :	"fromDateSelect_trigger_c",	// trigger for the calendar (button ID)
			align			:	"Tl",			// alignment (defaults to "Bl")
			singleClick	:	true,
			mondayFirst	:	false,
			weekNumbers	:	false
		});
		Calendar.setup({
			inputField	 :	"toDateSelect",	 // id of the input field
			ifFormat		:	"%d/%m/%Y",		// format of the input field
			button		 :	"toDateSelect_trigger_c",	// trigger for the calendar (button ID)
			align			:	"Tl",			// alignment (defaults to "Bl")
			singleClick	:	true,
			mondayFirst	:	false,
			weekNumbers	:	false
		});
	</script>
	<br />
	<%
	Set fsServer=Nothing
Else
	%>
	<table width="95%" border="0" cellspacing="2" cellpadding="2" align="center">
	<tr>
		<td align="left" class="txt13">
			<br />Did not find any transactions !<br />
		</td>
	</tr>
	</table>
	<%
End if
rsData.close
Set rsData = nothing
call closeConnection()
%>
</body>
</html>