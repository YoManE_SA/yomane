﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Emails;

public partial class AdminCash_merchants_dataMail : System.Web.UI.Page
{
    private int merchantId;
	private Dictionary<int, MessageStatus> _messageStatuses;
	protected Dictionary<int, MessageStatus> MessageStatuses
	{
		get
		{
			if (_messageStatuses == null)
				_messageStatuses = Netpay.Emails.MessageStatus.Cache;
			return _messageStatuses;
		}
	}

    protected void Page_Load(object sender, EventArgs e)
	{
		merchantId = dbPages.TestVar(Request.QueryString["merchantID"], 0, -1, 0);
        if (merchantId != 0)
		{
			repMerchantEmails.DataSource = Netpay.Emails.Message.getMerchantMessages(merchantId);
			repMerchantEmails.DataBind();
			hf_holderIdMerchant.Value = merchantId.ToString();
            if (!IsPostBack)
            {
				var list = Netpay.Emails.EmailToMerchant.GetEmailsForMerchant(WebUtils.DomainHost, merchantId);
                txt_mailsMerchant.Text = string.Join("\r\n", list);
            }
		}
	}

	protected void btn_update_Click(object sender, EventArgs e)
	{
        string[] emailList = txt_mailsMerchant.Text.Replace("\r", "").Split('\n');
		Netpay.Emails.EmailToMerchant.setEmailsToMerchant(WebUtils.DomainHost, merchantId, emailList);
	}

	protected string GetStatusName(int ?status)
	{
        if (status == null) return string.Empty;
		if (!MessageStatuses.ContainsKey(status.Value)) return string.Empty;
		return MessageStatuses[status.Value].Name;
	}
}