<script runat="server">
	Sub Page_Load()
		Dim sFileName As String = "settlements"

		Dim nMerchant As Integer = dbPages.TestVar(Request("ShowCompanyID"), 1, 0, 0)
		Dim sMerchant As String = IIf(nMerchant > 0, nMerchant, "DEFAULT")
		sFileName &= "_" & sMerchant

		Dim sDateTimeFrom As String = "DEFAULT"
		Dim sDateFrom As String = Request("fromDate")
		If Not String.IsNullOrEmpty(sDateFrom) Then
			Dim sTimeFrom As String = Request("fromTime")
			sDateTimeFrom = "Convert(datetime, '" & sDateFrom & IIf(String.IsNullOrEmpty(sTimeFrom), String.Empty, " " & sTimeFrom) & "', 103)"
			sFileName &= "_" & sDateFrom.Replace("/", String.Empty)
		Else
			sFileName &= "_DEFAULT"
		End If

		Dim sDateTimeTo As String = "DEFAULT"
		Dim sDateTo As String = Request("toDate")
		If Not String.IsNullOrEmpty(sDateTo) Then
			Dim sTimeTo As String = Request("toTime")
			sDateTimeTo = "Convert(datetime, '" & sDateTo & IIf(String.IsNullOrEmpty(sTimeTo), String.Empty, " " & sTimeTo) & "', 103)"
			sFileName &= "_" & sDateTo.Replace("/", String.Empty)
		Else
			sFileName &= "_DEFAULT"
		End If
		
		Response.ContentType = "application/vnd.ms-excel"
		Response.AddHeader("Content-Disposition", "attachment;filename=" & sFileName & ".xls")

		Dim sSQL As String = "SELECT * FROM GetSettlements(" & sMerchant & ", " & sDateTimeFrom & ", " & sDateTimeTo & ")"
		Dim drData As System.Data.SqlClient.SqlDataReader = dbPages.ExecReader(sSQL)
		Response.Write("<table border=""1"">")
		Response.Write("<tr>")
		For i As Integer = 0 To drData.FieldCount - 1
			Response.Write("<th>" & drData.GetName(i) & "</th>")
		Next
		Response.Write("</tr>")
		While drData.Read
			Response.Write("<tr>")
			For i As Integer = 0 To drData.FieldCount - 1
				Response.Write("<td>" & drData(i) & "</td>")
			Next
			Response.Write("</tr>")
		End While
		Response.Write("</table>")
		drData.Close()
	End Sub
</script>