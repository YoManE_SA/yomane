<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoPaging.asp"-->
<!--#include file="../member/remoteCharge_Functions.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/MerchantTerminals.asp" -->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
Dim CompanyBatchFiles_id, trans_amount, sQueryString, sWhere, X_DebitReferenceCode, X_DebitReferenceNum, bIsNetpayTerminal
Dim lChTransID, Upload, RqForm, xAppList, xDecList, xChbList, xRetList, xStlPend, nShowCounter

If Request.QueryString("PageSize") = "" Then Session("PageSize") = 20 _
Else Session("PageSize") = int(Request.QueryString("PageSize"))

xAppList = Request("AppList")
xDecList = Request("DecList")
xRetList = Request("RetList")
xChbList = Request("ChbList")
xStlPend = Request("StlPend")

Set rsData = server.createobject("adodb.recordset")
If (Request("AfterFileUpload") = "1") And (Request("DOK") <> "1") Then
	LoadExcel("PendingProgress.xls")
	Set RqForm = Request.QueryString
	session("PageSize") = 2000
Else
	Set RqForm = Request.Form
End if

if(Request.QueryString("companyTransPending_id") <> "") Then sWhere = sWhere & "tblCompanyTransPending.companyTransPending_id = " & Replace(Request.QueryString("companyTransPending_id"), "'", "''") & " And "
if(Request.QueryString("CompanyBatchFiles_id") <> "") Then sWhere = sWhere & "tblCompanyTransPending.CompanyBatchFiles_id =" & TestNumVar(Request.QueryString("CompanyBatchFiles_id"), 0, -1, 0) & " And "
if(Request.QueryString("CompanyID") <> "") Then sWhere = sWhere & "tblCompanyTransPending.company_id IN(" & Request.QueryString("CompanyID") & ") And "
if(Request.QueryString("TypeTrans") <> "") Then sWhere = sWhere & "tblCompanyTransPending.trans_creditType IN(" & Request.QueryString("TypeTrans") & ") And "' Else sWhere = sWhere & "1=2 And "
if(Request.QueryString("PaymentMethod") <> "") Then sWhere = sWhere & "tblCompanyTransPending.PaymentMethod IN(" & Request.QueryString("PaymentMethod") & ") And "
if(Request.QueryString("AmountMax") <> "") Then sWhere = sWhere & "tblCompanyTransPending.trans_amount <= " & TestNumVar(Request.QueryString("AmountMax"), 0, -1, 0) & " And "
if(Request.QueryString("AmountMin") <> "") Then sWhere = sWhere & "tblCompanyTransPending.trans_amount >= " & TestNumVar(Request.QueryString("AmountMin"), 0, -1, 0) & " And "
if(Request.QueryString("ccHolderName") <> "") Then sWhere = sWhere & "tblCreditCard.member Like '" & Replace(Request.QueryString("ccHolderName"), "'", "''") & "' And "
if(Request.QueryString("CcLast4Num") <> "") Then sWhere = sWhere & "tblCompanyTransPending.PaymentMethodDisplay Like '" & Replace(Request.QueryString("CcLast4Num"), "'", "''") & "' And "
if(Request.QueryString("DebitReferenceCode") <> "") Then sWhere = sWhere & "tblCompanyTransPending.DebitReferenceCode = '" & Replace(Request.QueryString("DebitReferenceCode"), "'", "''") & "' And "
if(Request.QueryString("DebitApprovalNumber") <> "") Then sWhere = sWhere & "tblCompanyTransPending.DebitApprovalNumber = '" & Replace(Request.QueryString("DebitApprovalNumber"), "'", "''") & "' And "
if(Request.QueryString("rdtpFrom") <> "") Then sWhere = sWhere & "tblCompanyTransPending.insertDate >= '" & Replace(Left(Request.QueryString("rdtpFrom"), 10), "'", "''") & "' And "
if(Request.QueryString("rdtpTo") <> "") Then sWhere = sWhere & "tblCompanyTransPending.insertDate <= '" & Replace(Left(Request.QueryString("rdtpTo"), 10), "'", "''") & "' And "

If PageSecurity.IsLimitedMerchant Then
	sWhere = sWhere & " tblCompanyTransPending.company_id IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "')) AND "
End If
If PageSecurity.IsLimitedDebitCompany Then
	sWhere = sWhere & " tblCompanyTransPending.DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & PageSecurity.Username & "')) AND "
End If

if sWhere <> "" Then sWhere = "Where " & Left(sWhere, Len(sWhere) - 4)

sSQL = "SELECT tblCompanyTransPending.companyTransPending_id, tblCompanyTransPending.companyBatchFiles_id, tblCompanyTransPending.isTestOnly, " & _ 
		" tblCompanyTransPending.TransSource_id, tblCompanyTransPending.company_id, tblCompanyTransPending.CustomerID, " & _ 
		" tblCompanyTransPending.DebitCompanyID, tblCompanyTransPending.FraudDetectionLog_id, tblCompanyTransPending.insertDate, " & _ 
		" tblCompanyTransPending.TerminalNumber, tblCompanyTransPending.PaymentMethod, " & _ 
		" tblCompanyTransPending.PaymentMethodDisplay, tblCompanyTransPending.IPAddress, " & _ 
		" tblCompanyTransPending.replyCode, tblCompanyTransPending.Comment, tblCompanyTransPending.trans_amount, " & _ 
		" tblCompanyTransPending.trans_currency, tblCompanyTransPending.trans_creditType, tblCompanyTransPending.trans_payments, " & _ 
		" tblCompanyTransPending.trans_originalID, tblCompanyTransPending.trans_order, tblCompanyTransPending.DebitReferenceCode, tblCompanyTransPending.DebitReferenceNum, " & _ 
		" tblCompanyTransPending.DebitApprovalNumber, tblCompanyTransPending.Locked, tblCompanyTransPending.trans_type, " & _ 
		" tblCompanyTransPending.ID, tblCompanyTransPending.payerIdUsed, tblCompanyTransPending.OrderNumber, tblCompanyTransPending.CompanyID, " & _ 
		" tblCompanyTransPending.insertDate AS PInsertDate, tblCompanyTransPending.company_id AS PCompanyID, " & _ 
		" tblCompanyTransPending.CustomerID AS PCustomerID, tblCreditCard.ID AS Expr1, tblCreditCard.companyID AS Expr2, tblCreditCard.ccTypeID, " & _ 
		" tblCreditCard.BillingAddressId, tblCreditCard.PersonalNumber, tblCreditCard.ExpMM, tblCreditCard.ExpYY, " & _ 
		" tblCreditCard.Member, tblCreditCard.cc_cui, tblCreditCard.phoneNumber, tblCreditCard.email, " & _ 
		" tblCreditCard.BINCountry, tblCreditCard.Comment AS Expr3, tblCreditCard.CCard_First6, tblCreditCard.CCard_Last4, " & _ 
		" tblCreditCard.CCard_number256, tblCheckDetails.id AS Expr4, tblCheckDetails.insertDate AS Expr5, tblCheckDetails.companyId AS Expr6, " & _ 
		" tblCheckDetails.customerId AS Expr7, tblCheckDetails.billingAddressId AS Expr8, tblCheckDetails.accountName, tblCheckDetails.accountNumber256, " & _ 
		" tblCheckDetails.routingNumber256, tblCheckDetails.personalNumber AS Expr9, tblCheckDetails.phoneNumber AS Expr10, " & _ 
		" tblCheckDetails.email AS Expr11, tblCheckDetails.comment AS Expr12, tblCheckDetails.birthDate, tblCheckDetails.bankAccountTypeId, " & _ 
		" tblCheckDetails.bankName, tblCheckDetails.bankCity, tblCheckDetails.bankPhone, tblCheckDetails.bankState, tblBillingAddress.id AS Expr13,  " & _
		" tblBillingAddress.insertDate AS Expr14, tblBillingAddress.address1, tblBillingAddress.address2, tblBillingAddress.city, tblBillingAddress.zipCode, " & _ 
		" tblBillingAddress.stateId, tblBillingAddress.countryId, tblBillingAddress.stateIso, tblBillingAddress.countryIso, " & _ 
		" List.TransSource.Name AS PageSource, tblCompany.CompanyName, tblCompanyChargeAdmin.isPendingReply, pm.Name AS 'pm_Name', " & _
		" tblCompanyChargeAdmin.PendingReplyUrl FROM tblCompanyTransPending " & _
		" LEFT JOIN List.PaymentMethod AS pm ON tblCompanyTransPending.PaymentMethod = pm.PaymentMethod_id " &_
		" LEFT JOIN tblCreditCard ON(tblCompanyTransPending.CreditCardID=tblCreditCard.ID)" &_
		" LEFT JOIN tblCheckDetails ON(tblCompanyTransPending.CheckDetailsID=tblCheckDetails.ID)" &_
		" LEFT JOIN tblBillingAddress ON(tblBillingAddress.id=tblCreditCard.BillingAddressId And tblBillingAddress.id=tblCheckDetails.BillingAddressId)" &_
		" LEFT JOIN List.TransSource ON(tblCompanyTransPending.TransSource_id = List.TransSource.TransSource_id)" &_
		" LEFT JOIN tblCompany ON tblCompanyTransPending.company_id = tblCompany.ID" & _
		" LEFT JOIN tblCompanyChargeAdmin ON tblCompanyChargeAdmin.company_id = tblCompany.ID " & sWhere & " ORDER BY companyTransPending_id Desc"

sQueryString = "&" & UrlWithout(Request.QueryString, "page")
if Request.QueryString("exl") <> "" Then
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "content-disposition", "attachment; filename=Export.xls"
 	Response.Write("<HTML xmlns:o=""urn:schemas-microsoft-com:office:office"" " & _
		"xmlns:x=""urn:schemas-microsoft-com:office:excel"" xmlns=""http://www.w3.org/TR/REC-html40"">" & _
		"<HEAD><meta HTTP-EQUIV=""Content-Type"" CONTENT=""text/html; CHARSET=windows-1255"">" & _
		"<title>" & Title & "</title></HEAD><style>" & _
		"@page {margin:1.0in .75in 1.0in .75in; mso-header-margin:.5in; mso-footer-margin:.5in;} " & _
		".style0 {mso-number-format:General; mso-style-id:0;} " & _
		"td, .xl24 {mso-style-parent:style0; mso-number-format:0;}" & _
		"</style><body dir=""rtl""><table x:str border=""1"" style=""border-collapse:collapse;table-layout:fixed;""><tr>" & _
		"<th>trans_amount</th><th>trans_currency</th><th>trans_creditType</th><th>trans_payments</th>" & _
		"<th>cc_number</th><th>cc_expMonth</th><th>cc_expYear</th><th>cc_cvv2</th>" & _
		"<th>ch_name</th><th>ch_phoneNum</th><th>ch_personalIdNum</th><th>ch_emailAddress</th>" & _
		"<th>billing_address1</th><th>billing_address2</th><th>billing_city</th><th>billing_zipCode</th><th>billing_state</th><th>billing_country</th></tr>")
	'rsData.Open sSQL, oledbData, 0, 1
	call openRsDataPaging(session("PageSize"))
	Do While Not rsData.EOF
		Response.Write("<tr>" & _
			"<td>" & rsData("trans_amount") & "</td>" & _
			"<td>" & rsData("trans_currency") & "</td>" & _
			"<td>" & rsData("trans_creditType") & "</td>" & _
			"<td>" & rsData("trans_payments") & "</td>" & _
			"<td>" & Right(rsData("CCard_First6"), 4) & "</td>" & _

			"<td>" & rsData("ExpMM") & "</td>" & _
			"<td>" & rsData("ExpYY") & "</td>" & _
			"<td>" & DecCVV(IIF(IsNull(rsData("cc_cui")), "", rsData("cc_cui"))) & "</td>" & _
			"<td>" & rsData("Member") & "</td>" & _
			"<td>" & rsData("phoneNumber") & "</td>" & _
			"<td>" & rsData("PersonalNumber") & "</td>" & _
			"<td>" & rsData("email") & "</td>" & _
			"<td>" & rsData("address1") & "</td>" & _
			"<td>" & rsData("address2") & "</td>" & _
			"<td>" & rsData("city") & "</td>" & _
			"<td>" & rsData("zipCode") & "</td>" & _
			"<td>" & rsData("stateIso") & "</td>" & _
			"<td>" & rsData("countryIso") & "</td></tr>")
	  rsData.MoveNext
	Loop
	rsData.Close
	Response.Write(outputFile)
	Response.Write("</table></body></html>")
	call closeConnection()
	Response.End
End If

Function GetFieldValue(dRs, sField, vValue)
	On Error Resume Next
	GetFieldValue = trim(dRs(sField))
	if Err.Number <> 0 Or IsNull(GetFieldValue) Then GetFieldValue = vValue
End Function

call openRsDataPagingEx(session("PageSize"), Request.QueryString)
If (RqForm("DOK") = "1") And (Not rsData.EOF) Then
	Dim retCode
	nShowCounter = 0
	Do While Not rsData.EOF Or nShowCounter=int(session("PageSize"))
		If (rsData("PaymentMethod") >= PMD_EC_MIN) And (rsData("PaymentMethod") <= PMD_EC_MIN) Then xOldMethod = 2 Else xOldMethod = 1
		Select Case Request("Tr" & rsData("companyTransPending_id"))
		Case "1"
		    X_DebitReferenceCode = rsData("DebitReferenceCode")
		    X_DebitReferenceNum = rsData("DebitReferenceNum")
			X_OCurrency = rsData("trans_currency") : X_OAmount = rsData("trans_amount")
			lChTransID = MovePendingTransaction("companyTransPending_id=" & rsData("companyTransPending_id"), "000")
			If InStr(1, xChbList, "," & X_DebitReferenceCode & ",") > 0 Then _
				ChargeBackTrans 0, False, "DeniedStatus=0 And tblCompanyTransPass.DebitReferenceCode='" & X_DebitReferenceCode & "'"
		Case "2", "3"
		    X_DebitReferenceCode = rsData("DebitReferenceCode")
		    X_DebitReferenceNum = rsData("DebitReferenceNum")
			retCode = IIF(Request("Tr" & rsData("companyTransPending_id")) = "3", "ACRET", rsData("replyCode"))
			lChTransID = MovePendingTransaction("companyTransPending_id=" & rsData("companyTransPending_id"), retCode)
		End Select
	  rsData.MoveNext
	Loop
	rsData.Requery
End If
%>
<html>
<head>
	<title>Pending Transactions</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
    <link  href="../StyleSheet/styleAdminEng.css" rel="stylesheet" type="text/css" />
	<script language="JavaScript" src="../js/func_common.js"></script>
    <style type="text/css">
	    .Bsave{
		    background-image: url("../images/icon_xls.gif");
		    background-repeat: no-repeat; background-position: 75px; 
		    width: 100; height: 24px; text-align: left;
		    background-color:#ffffff; font-size:11px;
		    padding-left: 3px; padding-right: 3px; cursor: hand;
		    }
    </style>
	 <script language="JavaScript">
		function ExpandNode(fTrNum, TransId, PaymentMethod, bkgColor) {
			toggleInfo(TransId, PaymentMethod, bkgColor);
			var imgPlus=document.getElementById("oListImg"+fTrNum);
			imgPlus.src = (imgPlus.src.indexOf("tree_expand.gif")>0 ? "../images/tree_collapse.gif" : "../images/tree_expand.gif");
		}

	 	function toggleInfo(TransId, PaymentMethod, bkgColor) 
		{			
			objRefA = document.getElementById('Row' + TransId + 'A')
			objRefB = document.getElementById('Row' + TransId + 'B')			
			if (objRefB && objRefB.innerHTML == '') {
				if (objRefA) { objRefA.style.display = 'none'; }
				if (PaymentMethod >= <%= PMD_EC_MIN %>)
				{
					UrlAddress = 'trans_detail_eCheck.asp';
				}
				else
				{
					switch (PaymentMethod)
					{
						case '4':
							UrlAddress = 'trans_detail_RollingRes.asp';
							break;
						case '2':
							UrlAddress = 'Trans_DetailAdmin.asp';
							break;
						default:
							UrlAddress = 'trans_detail_cc.asp';
					}
				}
				if (objRefB) {
						objRefB.innerHTML = '<iframe framespacing="0" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" width="100%" height="0px" src="' + UrlAddress + '?transID=' + TransId + '&PaymentMethod=' + PaymentMethod + '&bkgColor=' + bkgColor + '&TransTableType=pending" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>';
				}
			}
			else {
				if (objRefA) { objRefA.style.display = ''; }
				if (objRefB) { objRefB.innerHTML = ''; }
			}
		}
	 </script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">
<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
<tr>
	<td id="pageMainHeadingTd"><span id="pageMainHeading">PENDING TRANSACTIONS</span></td>
	<td align="right" valign="top">
		<table border="0" cellspacing="0" cellpadding="1">
		<tr>
			<td width="6" bgcolor="silver"></td><td width="1"></td>
			<td width="60">Pending</td>
			<td width="10"></td>
			<td width="6" bgcolor="silver">X</td><td width="1"></td>
			<td width="64" style="color:black;">Test Only</td>
			<td width="10"></td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<br />
<%
pageSecurityLevel = 0
pageSecurityLang = "ENG"
pageClosingTags = ""
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)

Set fsServer=Server.CreateObject("Scripting.FileSystemObject")
If Not rsData.EOF Then
    %>
    <form method="post">
    <input type="hidden" name="DOK" value="1">
	<table class="formNormal" width="95%" align="center" cellpadding="1" cellspacing="0">	
	<tr>
		<th><br /></th>
		<th>ID<br /></th>
		<th valign="bottom">Date</th>
		<th colspan="2">Payment Method<br /></th>
		<th>Merchant Name<br /></th>
		<th>Amount<br /></th>
		<th width="60" style="text-align:center;">Debit<br /></th>
		<td width="20"></td>
		<th width="30" style="text-align:center;">Pass<br /></th>
		<th width="30" style="text-align:center;">Fail<br /></th>
		<th width="30" style="text-align:center;">Return<br /></th>
		<th width="15%" style="text-align:center;">Note<br /></th>
	</tr>
	<tr><td height="3"></td></tr>
	<%
	i = 0
	nShowCounter = 0
	Do Until rsData.EOF
		i = i + 1
		sShowSign = ""
		sShowColor = "#000000"
		If int(rsData("trans_creditType"))=0 Then
			sShowSign = "-"
			sShowColor = "red"
		End if
		%>
		<tr>
			<td style="width:18px;">
				<%If rsData("PaymentMethod") = PMD_Admin OR rsData("PaymentMethod") > 14 OR rsData("PaymentMethod") = PMD_RolRes Then %>
				<img onclick="ExpandNode('<%=i%>','<%= rsData("ID") %>','<%= rsData("PaymentMethod") %>','<%= replace(sTrBkgColor,"#","") %>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" /><br />
				<%Else %>
				<img src="../images/tree_disabled.gif" alt="" width="16" height="16" border="0" align="middle" /><br />
				<%End If %>
			</td>
			<td nowrap>
			  <span style="background-color:silver;width:12px;height:15px;text-align:center;"><%=IIF(rsData("isTestOnly"), "X", "&nbsp;")%></span>
			  <%= rsData("companyTransPending_id") %><br />
			</td>
			<td><%= FormatDatesTimes(rsData("PInsertDate"),1,1,0) %><br /></td>
			<td width="60">
				<%
				If rsData("PaymentMethod")>14 Then
					Response.Write("<img src=""/NPCommon/ImgPaymentMethod/23X12/" & rsData("PaymentMethod") & ".gif"" alt=""" & rsData("pm_Name") & """ align=""middle"" border=""0"" /> &nbsp;")
					If Trim(rsData("BinCountry")) <> "" Then Response.write("<img src=""/NPCommon/ImgCountry/18X12/" & rsData("BinCountry") & ".gif"" alt=""" & rsData("BinCountry") & """ align=""middle"" border=""0"" />")
				End if
				%>
			</td>
			<td nowrap><%=rsData("PaymentMethodDisplay")%><br /></td>
			<td>
				<%
				If NOT isNull(rsData("CompanyName")) Then
					If int(len(dbtextShow(rsData("CompanyName"))))<14 Then
						response.write dbtextShow(rsData("CompanyName"))
					Else
						response.write left(dbtextShow(rsData("CompanyName")),13) & ".."
					End if
				End if
				%><br />
			</td>
			<td style="color:<%= sShowColor %>;"><%=FormatCurrWCT(rsData("trans_currency"), rsData("trans_amount"), rsData("trans_creditType"))%><br /></td>
			<td style="text-align:center;">
				<%
				If rsData("PaymentMethod")>14 AND trim(rsData("DebitCompanyID"))<>"" Then
					If fsServer.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & trim(rsData("DebitCompanyID")) & ".gif")) Then
						Response.write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(rsData("DebitCompanyID")) & ".gif"" align=""middle"" border=""0"" />")
					End if
				End if
				%>
			</td>
			<td></td>
			<td style="text-align:center; background-color:#f0fff0;"><input type="radio" value="1" <%=IIF(InStr(1, xAppList, "," & rsData("DebitReferenceCode") & ",") > 0, " checked", "") %> style="background-color:#f0fff0;" name="Tr<%=rsData("companyTransPending_id")%>"></td>
			<td style="text-align:center; background-color:#ffe6e6;"><input type="radio" value="2" <%=IIF(InStr(1, xDecList, "," & rsData("DebitReferenceCode") & ",") > 0, " checked", "") %> style="background-color:#ffe6e6;" name="Tr<%=rsData("companyTransPending_id")%>"></td>
			<td style="text-align:center; background-color:#A0e6e6;"><input type="radio" value="3" <%=IIF(InStr(1, xRetList, "," & rsData("DebitReferenceCode") & ",") > 0, " checked", "") %> style="background-color:#A0e6e6;" name="Tr<%=rsData("companyTransPending_id")%>"></td>
			<td><input type="text" class="inputData" name="Tr<%=rsData("companyTransPending_id")%>T"></td>
		</tr>
		<tr><td id="Row<%=rsData("ID")%>B" colspan="12"></td></tr>
		<tr><td height="1" colspan="8" bgcolor="silver"></td><td></td><td height="1" colspan="4" bgcolor="silver"></td></tr>
		<%
		nShowCounter=nShowCounter+1
	  rsData.movenext
	Loop
	%>
	<tr><td height="4"></td></tr>
	<tr>
		<td colspan="8">
			<%
				'
				' 20080901 Tamir
				'
				' show totals if filtered by merchant
				'
				nCompany=TestNumVar(Request.QueryString("CompanyID"), 1, 0, 0)
				if nCompany>0 then
					response.Write "<b>Total pending for the merchant:</b> &nbsp; "
					sSQL="SELECT trans_currency, Sum(trans_amount) FROM tblCompanyTransPending" & _
					" WHERE CompanyID=" & nCompany & " GROUP BY trans_currency ORDER BY trans_currency"
					Set rsTotal=oleDBData.Execute(sSQL)
					Do until rsTotal.EOF
						response.Write FormatCurrWCT(rsTotal(0), rsTotal(1), 1) & " &nbsp; "
						rsTotal.MoveNext
					Loop
					rsTotal.Close
				end if
			%>
		</td>
		<td></td>
		<td colspan="3">
			<input type="hidden" name="AppList" value="<%=xAppList%>" >
			<input type="hidden" name="DecList" value="<%=xDecList%>" >
			<input type="hidden" name="RetList" value="<%=xRetList%>" >
			<input type="hidden" name="ChbList" value="<%=xChbList%>" >
			<input type="submit" value="UPDATE" class="button1">
			<input type="reset" value="RESET" class="button1">
		</td>
	</tr>
	</table>
    <br />
	<table width="95%" align="center" cellpadding="1" cellspacing="0">
	<tr>
		<td><% call showPaging("merchant_transPending.asp", "Eng", sQueryString)%></td>
	</tr>
	</table>
    </form>
	<%
Else
	%>
	<table width="95%" align="center" cellpadding="1" cellspacing="0">
	<tr>
		<td>
			<hr width="100%" size="1" color="#C0C0C0" noshade>
			<span class="txt15">No records found !</span><br />
			<hr width="100%" size="1" color="#C0C0C0" noshade>
		</td>
	</tr>
	</table>
	<%
End If

Set fsServer=Nothing
call rsData.close
Set rsData = nothing
call closeConnection()
Set Upload = Nothing
%>
</body>
</html>