<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20080501 Tamir
	'
	' FAQ management
	'
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsCurrencies.ConnectionString = dbPages.DSN()
		litZero.Text = String.Empty
	End Sub

	Sub ZeroAmount(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		litZero.Text = "Rows updated: " & dbPages.ExecSql("UPDATE f SET Amount=0 FROM tblSystemCurrencies c INNER JOIN tblCompanyTransFail f ON CUR_ID=Currency AND Amount>CUR_TransactionAmountMax WHERE CUR_TransactionAmountMax>0")
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Currencies</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<h1><asp:label ID="lblPermissions" runat="server" /> Currencies</h1><br />
	<form id="frmServer" runat="server">
		<asp:GridView ID="gvCurrencies" runat="server" AutoGenerateColumns="False" DataKeyNames="CUR_ID" DataSourceID="dsCurrencies" AllowPaging="False" AllowSorting="False" SelectedRowStyle-CssClass="rowSelected" Width="100%">
			<Columns>
				<asp:BoundField DataField="CUR_ID" HeaderText="ID" ReadOnly="True" SortExpression="CUR_ID" HeaderStyle-HorizontalAlign="Left" />
				<asp:BoundField DataField="CUR_FullName" HeaderText="Title" SortExpression="CUR_FullName" ControlStyle-CssClass="text" ReadOnly="True" HeaderStyle-HorizontalAlign="Left" />
				<asp:BoundField DataField="CUR_InsertDate" HeaderText="Insert Date" SortExpression="CUR_InsertDate" ControlStyle-CssClass="text" ReadOnly="True" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
				<asp:BoundField DataField="CUR_RateRequestDate" HeaderText="Request Date" SortExpression="CUR_RateRequestDate" ControlStyle-CssClass="text" ReadOnly="True" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
				<asp:BoundField DataField="CUR_RateValueDate" HeaderText="Value Date" SortExpression="CUR_RateValueDate" ControlStyle-CssClass="text" ReadOnly="True" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
				<asp:BoundField DataField="CUR_IsoName" HeaderText="Sign" SortExpression="CUR_IsoName" ControlStyle-CssClass="text" ReadOnly="True" HeaderStyle-HorizontalAlign="Left" />
				<asp:BoundField DataField="CUR_BaseRate" HeaderText="Rate" SortExpression="CUR_BaseRate" ControlStyle-CssClass="short" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ControlStyle-Width="70px" HeaderStyle-Width="7%" />
				<asp:BoundField DataField="CUR_ExchangeFeePC" HeaderText="Fee" SortExpression="CUR_ExchangeFeePC"   HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="7%" ItemStyle-HorizontalAlign="Right" ControlStyle-Width="70px" DataFormatString="{0:0.00} %" />				
				<asp:BoundField DataField="CUR_TransactionAmountMax" HeaderText="Max Trans Amount" SortExpression="CUR_TransactionAmountMax" ControlStyle-CssClass="short" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ControlStyle-Width="70px" DataFormatString="{0:#,0.00}" />
				<asp:CommandField HeaderText="Actions" ButtonType="Button" ItemStyle-CssClass="buttons" ControlStyle-CssClass="buttonWhite" ShowDeleteButton="False" ShowEditButton="True" ShowSelectButton="False" HeaderStyle-CssClass="actions" HeaderStyle-HorizontalAlign="Right" />
			</Columns>
		</asp:GridView>
		<asp:SqlDataSource ID="dsCurrencies" runat="server" ProviderName="System.Data.SqlClient"
			SelectCommand="SELECT *, CUR_ExchangeFeeInd * 100 As CUR_ExchangeFeePC FROM tblSystemCurrencies  ORDER BY CUR_ID "
			UpdateCommand="UPDATE [tblSystemCurrencies] SET [CUR_TransactionAmountMax]=@CUR_TransactionAmountMax, [CUR_BaseRate]=@CUR_BaseRate, [CUR_ExchangeFeeInd]=@CUR_ExchangeFeePC/100.0, [CUR_InsertDate]=GetDate() WHERE [CUR_ID]=@CUR_ID">
			<UpdateParameters>				
				<asp:Parameter Name="CUR_BaseRate" Type="Decimal" ConvertEmptyStringToNull="false"  />
				<asp:Parameter Name="CUR_ExchangeFeePC" Type="Decimal"  ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="CUR_TransactionAmountMax" Type="Decimal"  ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="CUR_ID" Type="Int32" />
			</UpdateParameters>
		</asp:SqlDataSource>
		<br />
		<br />
		<br />
		<br />
		<asp:LinkButton runat="server" Text="Set zero amount in declines having amount higher than currency limit" CssClass="menu"
		 OnClientClick="if (!confirm('Are you sure ?!')) return false;" OnClick="ZeroAmount" />
		<br />
		<div class="workplace" style="text-align:left;">
			<asp:Literal ID="litZero" runat="server" />
		</div>
	</form>
</body>
</html>