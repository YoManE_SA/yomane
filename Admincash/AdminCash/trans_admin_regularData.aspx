<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Drawing"%>
<%@ Import Namespace="System.Drawing.Text"%>
<%@ Import Namespace="System.Drawing.Imaging" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="System.Collections"%>
<%@ Import Namespace="System.Math"%>
<%@ Import Namespace="System.Globalization"%>
<script runat="server">
    Dim sSQL, sQueryWhere, sQueryWhereNoCHB, nFromDate, nToDate, sDateDiff, sStatusColor, sAnd, sQueryString, sTmpTxt, sDivider, nDateMin, nDateMax As String
    Dim i, nTmpNum, nShowCounter As Integer

    Dim sStatusTxtPrelim As String = String.Empty
    Dim CardBIN As String = String.Empty
    Dim CcLast4Num As String = String.Empty
    Dim TransID As Integer = -1
    'Dim DeniedStatusDesc(20) As String
    Dim QB_CcNumber As String = String.Empty
    Dim QuickBar_TransID As String = String.Empty
    Dim nGraphSideSize As Integer = 500

    Dim LeftMargin As Short = 30, BaseMargin As Short = 30, TopMargin As Short = 70, RightMargin As Short = 10, BarGap As Short = 0, BarWidth As Integer
    Dim VertLineLength, BaseLineLength As Integer, TotalTicks As Integer, TickInterval As Integer = 200, HighCHB, VertScale As Double
    Dim RedLineY, nRedLineY As Integer

    Dim MCGraphName As String, VSGraphName As String

    Structure GraphData
        Dim sMonth As String
        Dim nChargeBacks As Integer
        Dim nChargeBacksTillNow As Integer
        Dim nDealsPrevMonth As Integer
        Dim nDealsCurrentMonth As Integer
        Dim CHBValue As String
        Dim BarColor As Color
        Dim MCPerCent As String
        Dim VSPercent As String
        Sub New(ByVal mmonth As String, ByVal nchbacks As Integer, ByVal nchbackstillnow As Integer, ByVal ndealsprev As Integer, ByVal ndealscurr As Integer, ByVal chbkvalue As Decimal, ByVal barcol As Color)
            Dim nFactor As Integer = 1
            Me.sMonth = mmonth
            Me.nChargeBacks = nchbacks * nFactor
            Me.nChargeBacksTillNow = nchbackstillnow * nFactor
            Me.nDealsPrevMonth = ndealsprev * nFactor
            Me.nDealsCurrentMonth = ndealscurr * nFactor
            Me.CHBValue = "USD " & Round(chbkvalue, 2)
            Me.BarColor = barcol
            If ndealsprev > 0 Then
                Me.MCPerCent = Round(nchbacks * 100.0 / ndealsprev, 2) & "%"
            Else
                Me.MCPerCent = "N/P"
            End If
            If ndealscurr > 0 Then
                Me.VSPercent = Round(nchbacks * 100.0 / ndealscurr, 2) & "%"
            Else
                Me.VSPercent = "N/P"
            End If
        End Sub
    End Structure

    Dim MCCHBData As New ArrayList
    Dim VSCHBData As New ArrayList

    Private Sub GetMonthGraphData(ByVal pmethod As Short, dataList As ArrayList)
        Dim sgSQL As String, smSQL As String
        Dim dPrevMonth As String = "Null", dMonth As String = "Null"
        Dim nCurrCHBtillNow As Integer, nCurrCHB As Integer, nPrevMonthTotal As Integer, nCurrMonthTotal As Integer
        Dim dcCHBTotalMoney As Decimal
        Dim iCnt As Short = 0
        Dim brColor As Color
        Dim sDay As String = Date.Today.Day.ToString("00")
        Dim dMonthFull As String, dDateFull As String, dateFormat As String = "dd/MM/yyyy"
        Dim dResult As DateTime
        Dim provider As CultureInfo = CultureInfo.InvariantCulture

        Dim sJoinType As String = String.Empty
        If InStr(LCase(sQueryWhere), "tblcreditcard") > 0 Then sJoinType = "INNER" Else sJoinType = "LEFT"

        sgSQL = "SELECT tblCompanyTransPass.DeniedDate " & _
        " FROM tblCompany WITH (NOLOCK) INNER JOIN tblCompanyTransPass WITH (NOLOCK) ON tblCompany.ID = tblCompanyTransPass.companyID" & _
        " " & sJoinType & " JOIN tblCreditCard WITH (NOLOCK) ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID" & _
        " LEFT JOIN List.TransCreditType WITH (NOLOCK) ON tblCompanyTransPass.CreditType = List.TransCreditType.TransCreditType_id" & _
        " LEFT JOIN List.PaymentMethod AS pm ON tblCompanyTransPass.PaymentMethod= pm.PaymentMethod_id" & _
        " LEFT JOIN tblDebitTerminals WITH (NOLOCK) ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber" & _
        " LEFT JOIN tblBillingAddress WITH (NOLOCK) ON tblCreditCard.BillingAddressId = tblBillingAddress.id" & _
        " " & sQueryWhere & " AND tblCompanyTransPass.PaymentMethod BETWEEN " & ePaymentMethod.PMD_CC_MIN & " AND " & ePaymentMethod.PMD_CC_MAX & " AND tblCompanyTransPass.PaymentMethod =" & pmethod & " ORDER BY DeniedDate"
        Dim MonthData As SqlDataReader
        MonthData = dbPages.ExecReader(sgSQL)
        While MonthData.Read()
            nCurrCHBtillNow = 0
            nCurrCHB = 0
            nPrevMonthTotal = 0
            nCurrMonthTotal = 0
            dcCHBTotalMoney = 0
            If dMonth <> Month(MonthData("DeniedDate")) & "/" & Right(Year(MonthData("DeniedDate")), 2) Then
                brColor = IIf(iCnt Mod 2 = 0, Color.RoyalBlue, Color.CadetBlue)
                iCnt += 1

                dMonth = Month(MonthData("DeniedDate")) & "/" & Right(Year(MonthData("DeniedDate")), 2)
                dPrevMonth = Month(DateAdd(DateInterval.Month, -1, MonthData("DeniedDate"))) & "/" & Year(DateAdd(DateInterval.Month, -1, MonthData("DeniedDate")))
                dMonthFull = Month(MonthData("DeniedDate")) & "/" & Year(MonthData("DeniedDate"))
                If Len("" & Month(MonthData("DeniedDate"))) = 1 Then
                    dMonthFull = "0" & dMonthFull
                End If
                dDateFull = sDay & "/" & dMonthFull

                smSQL = "SELECT" & _
                " Sum(tblCompanyTransPass.Amount*sysCurr.CUR_BaseRate/sysCurrUSD.CUR_BaseRate) " & _
                " FROM tblCompany INNER JOIN tblCompanyTransPass ON tblCompany.ID = tblCompanyTransPass.companyID" & _
                " " & sJoinType & " JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID AND tblCompanyTransPass.PaymentMethod>=20" & _
                " INNER JOIN tblSystemCurrencies sysCurr ON sysCurr.CUR_ID = tblCompanyTransPass.Currency" & _
                " INNER JOIN tblSystemCurrencies sysCurrUSD ON sysCurrUSD.CUR_ID = 1" & _
                " LEFT JOIN List.TransCreditType ON tblCompanyTransPass.CreditType = List.TransCreditType.TransCreditType_id" & _
                " LEFT JOIN List.PaymentMethod AS pm ON tblCompanyTransPass.PaymentMethod= pm.PaymentMethod_id" & _
                " LEFT JOIN tblDebitTerminals ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber" & _
                " LEFT JOIN tblBillingAddress ON tblCreditCard.BillingAddressId = tblBillingAddress.id" & _
                " " & sQueryWhere & " AND tblCompanyTransPass.PaymentMethod =" & pmethod & _
                " AND Month(tblCompanyTransPass.DeniedDate)=" & Month(MonthData("DeniedDate")) & " AND Year(tblCompanyTransPass.DeniedDate)=" & Year(MonthData("DeniedDate"))
                dcCHBTotalMoney = dbPages.ExecScalar(smSQL)

                smSQL = "SELECT Count(tblCompanyTransPass.ID) " & _
                " FROM tblCompany INNER JOIN tblCompanyTransPass ON tblCompany.ID = tblCompanyTransPass.companyID" & _
                " " & sJoinType & " JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID" & _
                " INNER JOIN tblSystemCurrencies sysCurr ON sysCurr.CUR_ID = tblCompanyTransPass.Currency" & _
                " INNER JOIN tblSystemCurrencies sysCurrUSD ON sysCurrUSD.CUR_ID = 1" & _
                " LEFT JOIN List.TransCreditType ON tblCompanyTransPass.CreditType = List.TransCreditType.TransCreditType_id" & _
                " LEFT JOIN List.PaymentMethod AS pm ON tblCompanyTransPass.PaymentMethod= pm.PaymentMethod_id" & _
                " LEFT JOIN tblDebitTerminals ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber" & _
                " LEFT JOIN tblBillingAddress ON tblCreditCard.BillingAddressId = tblBillingAddress.id" & _
                " " & sQueryWhereNoCHB & " AND tblCompanyTransPass.PaymentMethod =" & pmethod & " AND Month(tblCompanyTransPass.InsertDate)=" & Month(DateAdd(DateInterval.Month, -1, MonthData("DeniedDate"))) & " AND Year(tblCompanyTransPass.InsertDate)=" & Year(DateAdd(DateInterval.Month, -1, MonthData("DeniedDate"))) & " AND dbo.IsCHB(DeniedStatus, IsTestOnly)=0"

                'FROM tblCompanyTransPass WHERE dbo.IsCHB(DeniedStatus, IsTestOnly)=0 AND Month(tblCompanyTransPass.DeniedDate)=" & Month(DateAdd(DateInterval.Month,-1,MonthData("DeniedDate"))) & " AND Year(tblCompanyTransPass.DeniedDate)=" & Year(DateAdd(DateInterval.Month,-1,MonthData("DeniedDate"))) & " AND tblCompanyTransPass.PaymentMethod =" & pmethod
                nPrevMonthTotal = dbPages.ExecScalar(smSQL)

                smSQL = "SELECT Count(tblCompanyTransPass.ID) " & _
                " FROM tblCompany INNER JOIN tblCompanyTransPass ON tblCompany.ID = tblCompanyTransPass.companyID" & _
                " " & sJoinType & " JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID" & _
                " LEFT JOIN List.TransCreditType ON tblCompanyTransPass.CreditType = List.TransCreditType.TransCreditType_id" & _
                " LEFT JOIN List.PaymentMethod AS pm ON tblCompanyTransPass.PaymentMethod= pm.PaymentMethod_id" & _
                " LEFT JOIN tblDebitTerminals ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber" & _
                " LEFT JOIN tblBillingAddress ON tblCreditCard.BillingAddressId = tblBillingAddress.id" & _
                " " & sQueryWhereNoCHB & " AND tblCompanyTransPass.PaymentMethod =" & pmethod & " AND Month(tblCompanyTransPass.InsertDate)=" & Month(MonthData("DeniedDate")) & " AND Year(tblCompanyTransPass.InsertDate)=" & Year(MonthData("DeniedDate")) & " AND dbo.IsCHB(DeniedStatus, IsTestOnly)=0"

                'FROM tblCompanyTransPass WHERE dbo.IsCHB(DeniedStatus, IsTestOnly)=0 AND Month(tblCompanyTransPass.DeniedDate)=" & Month(MonthData("DeniedDate")) & " AND Year(tblCompanyTransPass.DeniedDate)=" & Year(MonthData("DeniedDate")) & " AND tblCompanyTransPass.PaymentMethod =" & pmethod
                nCurrMonthTotal = dbPages.ExecScalar(smSQL)

                smSQL = "SELECT" & _
                " Count(tblCompanyTransPass.ID) " & _
                " FROM tblCompany INNER JOIN tblCompanyTransPass ON tblCompany.ID = tblCompanyTransPass.companyID" & _
                " " & sJoinType & " JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID" & _
                " LEFT JOIN List.TransCreditType ON tblCompanyTransPass.CreditType = List.TransCreditType.TransCreditType_id" & _
                " LEFT JOIN List.PaymentMethod AS pm ON tblCompanyTransPass.PaymentMethod= pm.PaymentMethod_id" & _
                " LEFT JOIN tblDebitTerminals ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber" & _
                " LEFT JOIN tblBillingAddress ON tblCreditCard.BillingAddressId = tblBillingAddress.id" & _
                " " & sQueryWhere & " AND tblCompanyTransPass.PaymentMethod =" & pmethod & " AND Month(tblCompanyTransPass.DeniedDate)=" & Month(MonthData("DeniedDate")) & " AND Year(tblCompanyTransPass.DeniedDate)=" & Year(MonthData("DeniedDate"))
                nCurrCHB = dbPages.ExecScalar(smSQL)

                Try
                    dResult = DateTime.ParseExact(dDateFull, dateFormat, provider)
                    smSQL = "SELECT" & _
                    " Count(tblCompanyTransPass.ID) " & _
                    " FROM tblCompany INNER JOIN tblCompanyTransPass ON tblCompany.ID = tblCompanyTransPass.companyID" & _
                    " " & sJoinType & " JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID" & _
                    " LEFT JOIN List.TransCreditType ON tblCompanyTransPass.CreditType = List.TransCreditType.TransCreditType_id" & _
                    " LEFT JOIN List.PaymentMethod AS pm ON tblCompanyTransPass.PaymentMethod= pm.PaymentMethod_id" & _
                    " LEFT JOIN tblDebitTerminals ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber" & _
                    " LEFT JOIN tblBillingAddress ON tblCreditCard.BillingAddressId = tblBillingAddress.id" & _
                    " " & sQueryWhere & " AND tblCompanyTransPass.PaymentMethod =" & pmethod & " AND Month(tblCompanyTransPass.DeniedDate)=" & Month(MonthData("DeniedDate")) & " AND Year(tblCompanyTransPass.DeniedDate)=" & Year(MonthData("DeniedDate")) & " AND DAY(tblCompanyTransPass.DeniedDate)<=" & sDay
                    nCurrCHBtillNow = dbPages.ExecScalar(smSQL)
                Catch ex As Exception
                    'Response.Write (dDateFull & "<br />" & ex.Message)				
                    nCurrCHBtillNow = -1
                End Try
                'Response.Write (nCurrCHBtillNow & "<br />")
                dataList.Add(New GraphData(dMonth, nCurrCHB, nCurrCHBtillNow, nPrevMonthTotal, nCurrMonthTotal, dcCHBTotalMoney, brColor))
            End If
        End While
    End Sub

    Private Sub DrawVerticalAxis(ByVal g As Graphics, dataList As ArrayList, isMasterCard As Boolean)
        nRedLineY = 0
        TickInterval = 200
        VertLineLength = 0
        HighCHB = 0
        TotalTicks = 0
        RedLineY = 0
        '	Draw a line for the Vertical Axis.
        Dim StartPoint As New Point(LeftMargin, nGraphSideSize - BaseMargin)
        Dim EndPoint As New Point(LeftMargin, TopMargin)
        '  Basic Pen
        Dim LinePen As New Pen(Color.Black, 2)
        '  Draw the vertical line (without tick marks)
        g.DrawLine(LinePen, StartPoint, EndPoint)

        '  Draw the Tickmarks and Display Numbers
        '	Calculate length of the vertical axis
        VertLineLength = nGraphSideSize - (BaseMargin + TopMargin)

        '  Identify the highest sales figure
        For Each gd As GraphData In dataList
            If gd.nChargeBacks > HighCHB Then HighCHB = gd.nChargeBacks
        Next
        ' Scaled Tick Marks code follows 
        ' Round up to next hundred above highest sales figure
        Dim NextCent As Integer = CInt(HighCHB)
        If isMasterCard Then
            nRedLineY = 50
            If NextCent < 50 Then NextCent = 50
        Else
            nRedLineY = 200
            If NextCent < 200 Then NextCent = 200
        End If
        If TickInterval > HighCHB Then TickInterval = 100
        If TickInterval > HighCHB Then TickInterval = 50
        If TickInterval > HighCHB Then TickInterval = 10

        Do While NextCent Mod TickInterval <> 0
            NextCent += 1
        Loop

        '  Identify how many TickMarks required :
        TotalTicks = CInt(NextCent / TickInterval)
        ' Calc gaps between vertical tick marks
        Dim YPos As Integer = IIf(TotalTicks > 0, CInt(VertLineLength / TotalTicks), 0)
        '  Variables for Start and End Points of Tick Marks
        Dim TickSP As New Point(LeftMargin - 5, StartPoint.Y - YPos)
        Dim TickEP As New Point(LeftMargin, StartPoint.Y - YPos)
        '  Font for values  - declared here for readability
        Dim ValueFont As New Font("Arial", 8, FontStyle.Regular)

        For i As Integer = 1 To TotalTicks
            Dim sTickerValue As String = (i * TickInterval).ToString
            If Len(sTickerValue) = 1 Then
                sTickerValue = "  " & sTickerValue
            ElseIf Len(sTickerValue) = 2 Then
                sTickerValue = " " & sTickerValue
            End If
            g.DrawLine(New Pen(Color.Black), TickSP, TickEP)    '  Tick mark
            If i * TickInterval = nRedLineY Then RedLineY = TickSP.Y
            '  Tick Values as text :
            g.DrawString(sTickerValue, ValueFont, Brushes.Black, 0, TickSP.Y - 5)
            '  Resetx, y positions, proportionately up vertical line
            TickSP.Y = CInt(TickSP.Y - (VertLineLength / TotalTicks))
            TickEP.Y -= CInt(VertLineLength / TotalTicks)
        Next
    End Sub


    Private Sub Draw3DBars(ByVal g As Graphics, dataList As ArrayList, isMasterCard As Boolean)

        BarWidth = 0
        VertScale = 0
        BaseLineLength = 0

        g.DrawLine(New Pen(Color.Black), LeftMargin, nGraphSideSize - BaseMargin, _
          nGraphSideSize - RightMargin, nGraphSideSize - BaseMargin)
        '  Calculate length of baseline drawn by the code above
        BaseLineLength = nGraphSideSize - (LeftMargin + RightMargin)

        ' Calculate width of each bar making full use of space available
        BarWidth = CInt((BaseLineLength / dataList.Count) - BarGap)         '  Set the start point for the first bar
        Dim BarStartX As Integer = LeftMargin + BarGap
        '  Set the baseline of the graph
        Dim BaseLine As Integer = nGraphSideSize - BaseMargin
        '  Calc scaling  (new in this project)
        'VertScale = VertLineLength / HighCHB
        VertScale = VertLineLength / (TotalTicks * TickInterval)
        For Each gd As GraphData In dataList
            '  Set the positions of the four points of the bottom-most
            '  parallelogram and store in an array
            Dim Corners(3) As Point
            Corners(0) = New Point(BarStartX, BaseLine - 10)
            Corners(1) = New Point(BarStartX + BarWidth - 5, BaseLine - 10)
            Corners(2) = New Point(BarStartX + BarWidth, BaseLine)
            Corners(3) = New Point(BarStartX + 5, BaseLine)

            '  Calculate the height of this bar, taking scale into account:
            Dim BarHeight As Integer = CInt(gd.nChargeBacks * VertScale)

            '  Create brushes to draw the bars
            '  Colors will change according to settings in GraphData
            Dim barmainBrush As New HatchBrush(HatchStyle.Percent50, gd.BarColor)
            Dim bartopBrush As New SolidBrush(gd.BarColor)

            '  Draw one complete bar
            For i As Integer = 0 To BarHeight - 11
                '  (BarHeight - 11 might be confusing.	This makes allowance for
                '  a.	the 10 pixels which are added to create the 3D depth effect
                '  plus
                '  b.  the final rhombus is to be drawn in a lighter color, so we
                '  need to stop drawing these hatched ones 1 pixel below the 
                '  total bar height.

                '  Fill next polygon using the hatchbrush
                g.FillPolygon(barmainBrush, Corners)
                '  Move  all the Y positions up the picture box by 1 pixel
                Corners(0).Y -= 1
                Corners(1).Y -= 1
                Corners(2).Y -= 1
                Corners(3).Y -= 1
            Next
            '  Finally, top it off with a lighter rhombus
            g.FillPolygon(bartopBrush, Corners)
            '	Draw a line on today's level			
            If gd.nChargeBacksTillNow >= 0 Then
                g.DrawLine(New Pen(Color.Goldenrod), BarStartX, BaseLine - CInt(gd.nChargeBacksTillNow * VertScale), BarStartX + BarWidth, BaseLine - CInt(gd.nChargeBacksTillNow * VertScale))
            End If

            If RedLineY = 0 Then RedLineY = BaseLine - CInt(nRedLineY * VertScale)
            g.DrawLine(New Pen(Color.Red), LeftMargin, RedLineY, nGraphSideSize - RightMargin, RedLineY)

            '  Move the startpoint for the next bar
            BarStartX += CInt(BarWidth + BarGap)

            '	Dispose of brushes
            barmainBrush.Dispose()
            bartopBrush.Dispose()
        Next
    End Sub


    Private Sub WriteTheNames(ByVal g As Graphics, dataList As ArrayList, isMasterCard As Boolean)
        '  X position for start of country name(s).  It is placed
        '  under the left edge of the bar, plus 5 pixels for better look
        Dim sTitle As String = IIf(isMasterCard, "MasterCard", "Visa")
        Dim TitleStartX As Integer = (nGraphSideSize - LeftMargin - RightMargin) / 2 - 10
        Dim TitleTextBrsh As Brush = New SolidBrush(Color.Red)
        Dim TitleTextFont As New Font("Verdana", 10, FontStyle.Bold)
        g.DrawString(sTitle, TitleTextFont, TitleTextBrsh, TitleStartX, CInt(TopMargin - 65))
        Dim TextStartX As Integer = LeftMargin + BarGap + 5

        '  Create a Brush to draw the text
        Dim TextBrsh As Brush = New SolidBrush(Color.Black)
        Dim PCTextBrsh As Brush = New SolidBrush(Color.Blue)

        '  Create a Font object instance for text display
        '  dynamically adjusted font size would be useful:
        Dim fntSize As Integer = 10
        Dim TextFont As New Font("Verdana", fntSize, FontStyle.Bold)
        Dim TextFontAnnotation As New Font("Verdana", fntSize + 3, FontStyle.Regular)
        Dim TextFontComment As New Font("Verdana", fntSize - 3, FontStyle.Regular)

        '	Write them:
        For Each gd As GraphData In dataList
            g.DrawString(IIf(isMasterCard, gd.MCPerCent, gd.VSPercent), TextFont, PCTextBrsh, TextStartX, CInt(TopMargin - 48))
            g.DrawString(gd.nChargeBacks, TextFont, TextBrsh, TextStartX, CInt(TopMargin - 35))
            g.DrawString(gd.CHBValue, TextFontComment, TextBrsh, TextStartX, CInt(TopMargin - 15))
            g.DrawString(gd.sMonth, TextFont, TextBrsh, TextStartX, CInt(nGraphSideSize - (BaseMargin - 1)))
            g.DrawString(gd.nDealsCurrentMonth, TextFontAnnotation, TextBrsh, TextStartX, CInt(nGraphSideSize - (BaseMargin - 12)))
            TextStartX += CInt(BarWidth + BarGap)
        Next
    End Sub

    Private Sub WriteNoData(ByVal g As Graphics, isMasterCard As Boolean)
        Dim sText As String = "No data to display for " & IIf(isMasterCard, "MasterCard", "VISA")
        Dim TextStartX As Integer = LeftMargin + BarGap + 5
        Dim TextBrsh As Brush = New SolidBrush(Color.Black)
        Dim fntSize As Integer = 15
        Dim TextFont As New Font("Verdana", fntSize, FontStyle.Bold)
        g.DrawString(sText, TextFont, TextBrsh, TextStartX, CInt(nGraphSideSize / 2))
    End Sub

    Function DisplayContent(sVar As String) As String
        Return "<span class=""key"">" & dbPages.dbtextShow(sVar).Replace("=", "</span> = ").Replace("|", " <br /><span class=""key"">")
    End Function

    Protected Sub GetTransParam(ByVal PGData As Paging, ByRef sTrBkgColor As String, ByRef sTdBkgColorFirst As String, ByRef sTdFontColor As String, ByRef sPayStatusText As String, ByRef bShowDeniedOptions As Boolean, ByRef bAllowDelete As Boolean)
        If PGData.HasRows Then
            bAllowDelete = False
            bShowDeniedOptions = False
            sPayStatusText = ""
            sTrBkgColor = "#F6FEF6" 'LightGreen
            sTdBkgColorFirst = "#66cc66" 'Green
            sTdFontColor = "#000000"
            Dim lPaymentMethod As Long = dbPages.TestVar(PGData("PaymentMethod"), 0, -1, 0)
            If lPaymentMethod = ePaymentMethod.PMD_ManualFee Then
                bAllowDelete = True
                sTrBkgColor = "f5f5f5" 'LightGray
                sTdBkgColorFirst = "#484848" 'DarkGray
            ElseIf lPaymentMethod = ePaymentMethod.PMD_Admin Then
                bAllowDelete = True
                sTrBkgColor = "#eeeffd" 'LightBlue
                sTdBkgColorFirst = "#003399" 'Blue
            ElseIf lPaymentMethod = ePaymentMethod.PMD_RolRes Then
                bAllowDelete = True
                sTrBkgColor = "#f5f5f5" 'LightGray
                sTdBkgColorFirst = "#399496" 'DarkGreen
            End If

            If PGData("CreditType") = 0 Then sTdFontColor = "#cc0000" 'Red

            If PGData("deniedstatus") = eDeniedStatus.DNS_DupFoundValid Or PGData("deniedstatus") = eDeniedStatus.DNS_SetFoundValid Or PGData("deniedstatus") = eDeniedStatus.DNS_SetFoundValidRefunded Or PGData("deniedstatus") = eDeniedStatus.DNS_DupWasWorkedOut Then
                sTrBkgColor = "#fff3dd" 'LightOrange
            ElseIf PGData("deniedstatus") > 0 Then
                sTrBkgColor = "#ffebe1" 'LightOrange
                If PGData("deniedstatus") = eDeniedStatus.DNS_UnSetInVar Or PGData("deniedstatus") = eDeniedStatus.DNS_SetInVar Then bShowDeniedOptions = True
            End If
            If PGData("isTestOnly") Then
                bAllowDelete = True
                sTrBkgColor = "#ffffff"
                sTdFontColor = "#6b6b6b"
            End If

            If Trim(PGData("payID")) = "" Then
                sPayStatusText = ""
            ElseIf InStr(Trim(PGData("payID")), ";X;") > 0 Then
                sPayStatusText = "Archived"
            ElseIf Replace(Trim(PGData("payID")), ";", "") = "0" Then
                sPayStatusText = "Unsettled"
            ElseIf InStr(Trim(PGData("payID")), ";0;") = 0 Then
                bAllowDelete = False
                sPayStatusText = "Settled"
            ElseIf InStr(Trim(PGData("payID")), ";0;") > 0 Then
                bAllowDelete = False
                sPayStatusText = "Partial Settled"
            End If

            If TypeOf PGData("IsFraudByAcquirer") Is Boolean Then
                If PGData("IsFraudByAcquirer") Then
                    bAllowDelete = False
                    sTrBkgColor = "#d8d8d8"
                    sTdFontColor = "#000000"
                End If
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Security.CheckPermission(lblPermissions)

        Dim sQueryWhere As String = String.Empty
        If Trim(Request("ID")) <> "" And IsPostBack Then
            Dim sIP As String = Request.ServerVariables("REMOTE_ADDR"), arrIDs() As String = Request("ID").ToString.Trim.Split(",")
            Dim sbSQL As New StringBuilder(String.Empty)
            For Each nID As String In arrIDs
                sbSQL.AppendFormat("INSERT tblCompanyTransRemoved (companyID, TransSource_id, CreditType, CustomerID," & _
                " InsertDate, DeniedDate, DeniedStatus, IPAddress, Amount, Currency, Payments, replyCode," & _
                " Interest, Comment, DateDel, IPAddressDel, TerminalNumber, " & _
                " isTestOnly, paymentMethod_id, paymentMethodId, paymentMethodDisplay)" & _
                " SELECT companyID, TransSource_id, CreditType, CustomerID, InsertDate, DeniedDate," & _
                " DeniedStatus, IPAddress, Amount, Currency, Payments, replyCode, Interest," & _
                " Comment, '{0}', '{1}', TerminalNumber, isTestOnly," & _
                " PaymentMethod_id, paymentMethodId, paymentMethodDisplay FROM tblCompanyTransPass" & _
                " WHERE ID={2};", Date.Now.ToString("dd/MM/yyyy"), sIP, nID)
            Next
            sbSQL.AppendFormat("DELETE FROM tblCompanyTransPass WHERE ID IN ({0});", Request("ID"))
            Try
                Dim nCurrDSN As Integer = dbPages.CurrentDSN
                dbPages.ExecSql(sbSQL.ToString)
                dbPages.CurrentDSN = nCurrDSN
            Catch ex As Exception
                Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
                Response.Write("alert('Delete failed!');")
                Response.Write("</s" & "cript>")
            End Try
        End If

        Dim nScriptTimeout As Integer = Server.ScriptTimeout
        Server.ScriptTimeout = 180
        CardBIN = Trim(Request("CardBIN"))
        CcLast4Num = Trim(Request("CcLast4Num"))
        QB_CcNumber = Trim(Request("QuickBar_CcNumber"))
        If QB_CcNumber <> "" Then
            If Len(QB_CcNumber) = 6 Then CardBIN = QB_CcNumber
            If Len(QB_CcNumber) = 4 Then CcLast4Num = QB_CcNumber
        End If

        TransID = dbPages.TestVar(Request("transID"), 1, 0, 0)
        QuickBar_TransID = dbPages.TestVar(Request("QuickBar_TransID"), 1, 0, 0)
        If QuickBar_TransID > 0 Then TransID = QuickBar_TransID

        If TransID > 0 Then
            sQueryWhere = sQueryWhere & sAnd & "(t.ID IN (" & dbPages.TestNumericList(TransID, Nothing) & "))" : sAnd = " AND "
        ElseIf Trim(Request("onlyTestTrans")) <> "" Then
            sQueryWhere = sQueryWhere & sAnd & " UnsettledInstallments <> 0 AND isTestOnly = 1" : sAnd = " AND "
            If Request("ShowCompanyID") <> "" Then sQueryWhere = sQueryWhere & sAnd & " t.CompanyID = " & Trim(Request("ShowCompanyID")) : sAnd = " AND "
        ElseIf Trim(Request("DebitReferenceCode")) <> "" Then
            sQueryWhere = sQueryWhere & sAnd & " DebitReferenceCode IN ('" & Trim(Request("DebitReferenceCode")) & "') And t.InsertDate > '" & DateTime.Now.AddYears(-1).ToSql() & "'" : sAnd = " AND "
        ElseIf Trim(Request("DebitReferenceNum")) <> "" Then
            sQueryWhere = sQueryWhere & sAnd & " DebitReferenceNum IN ('" & Trim(Request("DebitReferenceNum")) & "') And t.InsertDate > '" & DateTime.Now.AddYears(-1).ToSql() & "'" : sAnd = " AND "
        ElseIf Trim(Request("ApprovalNumber")) <> "" Then
            sQueryWhere = sQueryWhere & sAnd & " ApprovalNumber = '" & Trim(Request("ApprovalNumber")) & "' And t.InsertDate > '" & DateTime.Now.AddYears(-1).ToSql() & "'" : sAnd = " AND "
        Else
            If Request("Excel") = "" Then
                If Trim(Request("PageSize")) <> "" Then
                    PGData.PageSize = Request("PageSize")
                Else
                    PGData.PageSize = 25
                End If
            Else
                PGData.PageSize = 100000
            End If

            If Trim(Request("chk2")) = "1" And Not String.IsNullOrEmpty(Request("Payout")) Then
                Dim sPayout As String = Request("Payout")
                If sPayout.Length < 7 Then  'if 7 - all checked - no filtering
                    Dim sbWherePayout As New StringBuilder()
                    If sPayout.Contains("A") Then sbWherePayout.Append(" OR PayID LIKE '%;X;%'")
                    If sPayout.Contains("P") Then sbWherePayout.Append(" OR (PayID LIKE '%;0;%' AND PayID<>';0;')")
                    If sPayout.Contains("S") Then sbWherePayout.Append(" OR PrimaryPayedID <> 0")
                    If sPayout.Contains("U") Then sbWherePayout.Append(" OR PayID LIKE '%;0;%'")
                    If sbWherePayout.Length > 0 Then
                        sQueryWhere &= sAnd & "(" & sbWherePayout.Remove(0, 4).ToString & ")"
                        sAnd = " AND "
                    End If
                End If
            End If

            If Trim(Request("ShowCompanyID")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(t.CompanyID = " & Trim(Request("ShowCompanyID")) & ")" : sAnd = " AND "
            If Trim(Request("DebitCompanyID")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(t.DebitCompanyID IN (" & Trim(Request("DebitCompanyID")) & "))" : sAnd = " AND "
            If Trim(Request("paymentMethodType")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(t.PaymentMethod_id = " & dbPages.DBText(Trim(Request("paymentMethodType"))) & ")" : sAnd = " AND "
            If Trim(Request("TerminalNumber")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(t.TerminalNumber ='" & Trim(Request("TerminalNumber")) & "')" : sAnd = " AND "
            'If Trim(Request("TransactionHistory")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(t.ID IN (Select th.TransPass_id From Trans.TransHistory th Where th.TransHistoryType_id=" & Request("TransactionHistory") & "))" : sAnd = " AND "
            If Trim(Request("IsFraud")) = "1" Then sQueryWhere = sQueryWhere & sAnd & "(t.IsFraudByAcquirer=" & Request("IsFraud") & ")" : sAnd = " AND "

            If Trim(Request("chk0")) = "1" Or Len("" & Trim(Request("QuickBar_CcNumber"))) > 0 Then
                If Trim(Request("PaymentMethod")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(t.PaymentMethod IN(" & Trim(Request("PaymentMethod")) & "))" : sAnd = " AND "
                If Trim(Request("ccHolderName")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(cc.member = '" & dbPages.DBText(Trim(Request("ccHolderName"))) & "')" : sAnd = " AND "
                If Trim(Request("ccEmail")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(cc.email = '" & dbPages.DBText(Trim(Request("ccEmail"))) & "')" : sAnd = " AND "
                If dbPages.TestVar(CardBIN, 1, 999999, 0) > 0 Then sQueryWhere = sQueryWhere & sAnd & "(cc.CCard_First6 = " & dbPages.DBText(CardBIN) & ")" : sAnd = " AND "
                If dbPages.TestVar(CcLast4Num, 0, 9999, -1) > -1 Then sQueryWhere = sQueryWhere & sAnd & "(cc.CCard_Last4 = " & dbPages.DBText(CcLast4Num) & ")" : sAnd = " AND "
                If Trim(Request("Country")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(cc.BINCountry = '" & dbPages.DBText(Trim(Request("Country"))) & "')" : sAnd = " AND "
            End If
            nFromDate = Trim(Request("fromDate")) & " " & Trim(Request("fromTime"))
            nToDate = Trim(Request("toDate")) & " " & Trim(Request("toTime"))
            If nFromDate <> " " Then sQueryWhere = sQueryWhere & sAnd & "(t.insertDate >= '" & nFromDate.ToString() & "')" : sAnd = " AND "
            If nToDate <> " " Then sQueryWhere = sQueryWhere & sAnd & "(t.insertDate <= '" & nToDate.ToString() & "')" : sAnd = " AND "
            If Trim(Request("AmountMin")) <> "" And IsNumeric("" & Trim(Request("AmountMin"))) Then sQueryWhere = sQueryWhere & sAnd & "(t.Amount >= " & Trim(Request("AmountMin")) & ")" : sAnd = " AND "
            If Trim(Request("AmountMax")) <> "" And IsNumeric("" & Trim(Request("AmountMax"))) Then sQueryWhere = sQueryWhere & sAnd & "(t.Amount <= " & Trim(Request("AmountMax")) & ")" : sAnd = " AND "
            If Request("NoTestOnly") = "1" Then sQueryWhere = sQueryWhere & sAnd & " IsTestOnly=0" : sAnd = " AND "
            If Trim(Request("Currency")) <> "" And IsNumeric("" & Trim(Request("Currency"))) Then sQueryWhere = sQueryWhere & sAnd & "(t.Currency = " & Trim(Request("Currency")) & ")" : sAnd = " AND "
            If Trim(Request("CreditType")) <> "" And Trim(Request("CreditType")) <> "1, 8, 0" Then sQueryWhere = sQueryWhere & sAnd & "(t.CreditType IN (" & Trim(Request("CreditType")) & "))" : sAnd = " AND "
            If Trim(Request("iMerchantID")) <> "" Then sQueryWhere = sQueryWhere & sAnd & "(m.id = " & Trim(Request("iMerchantID")) & ")" : sAnd = " AND "
            sQueryWhereNoCHB = sQueryWhere
            If Trim(Request("chk1")) = "1" Then
                'btnGraph.Visible = True
                nDateMin = Trim(Request("DStartDate")) & " " & Trim(Request("DStartTime"))
                nDateMax = Trim(Request("DEndDate")) & " " & Trim(Request("DEndTime"))
                If nDateMin <> " " Then sQueryWhere = sQueryWhere & sAnd & "(t.DeniedDate >= '" & nDateMin.ToString() & "')" : sAnd = " AND "
                If nDateMax <> " " Then sQueryWhere = sQueryWhere & sAnd & "(t.DeniedDate <= '" & nDateMax.ToString() & "')" : sAnd = " AND "
                If Trim(Request("deniedStatus")) <> "" Then
                    If Trim(Request("deniedStatus")) = "1" Then
                        sQueryWhere = sQueryWhere & sAnd & " t.isTestOnly=0" : sAnd = " AND "
                        sQueryWhere = sQueryWhere & sAnd & " dbo.IsCHB(DeniedStatus, IsTestOnly)=1" : sAnd = " AND "
                    Else
                        sQueryWhere = sQueryWhere & sAnd & " dbo.IsCHB(DeniedStatus, IsTestOnly)=0" : sAnd = " AND "
                    End If
                End If
            End If
        End If
        If Security.IsLimitedMerchant Then
            sQueryWhere &= sAnd & " t.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
            sQueryWhereNoCHB &= sAnd & " t.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
            sAnd = " AND "
        End If
        If Security.IsLimitedDebitCompany Then
            sQueryWhere &= sAnd & " t.DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
            sQueryWhereNoCHB &= sAnd & " t.DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
            sAnd = " AND "
        End If

        'sStatusTxtPrelim = ""
        'If Trim(Request("iStatus")) <> "" Then
        '	Select Case Trim(Request("iStatus"))
        '              Case "1" : sQueryWhere = sQueryWhere & sAnd & "(Log.FraudDetection.IsProceed = 0)" : sAnd = " AND " : sStatusTxtPrelim = "Failure"
        '              Case "2" : sQueryWhere = sQueryWhere & sAnd & "(Log.FraudDetection.IsProceed = 1 AND tblCompanyTransPass.ID IS NOT NULL AND tblCompanyTransFail.ID IS NULL AND tblCompanyTransPending.companyTransPending_id IS NULL AND tblCompanyTransApproval.ID IS NULL) " : sAnd = " AND " : sStatusTxtPrelim = "Charge Authorised"
        '              Case "3" : sQueryWhere = sQueryWhere & sAnd & "(Log.FraudDetection.IsProceed = 1 AND tblCompanyTransFail.ID IS NOT NULL AND tblCompanyTransPass.ID IS NULL AND tblCompanyTransPending.companyTransPending_id IS NULL AND tblCompanyTransApproval.ID IS NULL) " : sAnd = " AND " : sStatusTxtPrelim = "Charge Rejected"
        '              Case "4" : sQueryWhere = sQueryWhere & sAnd & "(Log.FraudDetection.IsProceed = 1 AND tblCompanyTransPending.companyTransPending_id IS NOT NULL AND tblCompanyTransPass.ID IS NULL AND tblCompanyTransFail.ID IS NULL AND tblCompanyTransApproval.ID IS NULL) " : sAnd = " AND " : sStatusTxtPrelim = "Pending"
        '              Case "5" : sQueryWhere = sQueryWhere & sAnd & "(Log.FraudDetection.IsProceed = 1 AND tblCompanyTransApproval.ID IS NOT NULL AND tblCompanyTransPass.ID IS NULL AND tblCompanyTransFail.ID IS NULL AND tblCompanyTransPending.companyTransPending_id IS NULL) " : sAnd = " AND " : sStatusTxtPrelim = "Authorisation only"
        '          End Select
        'End If
        Dim PayoutStat As Integer = dbPages.TestVar(Request("PayoutStat"), 0, -1, 0)
        Select Case PayoutStat
            Case 1 'Overdue not paid transactions
                sQueryWhere &= sAnd & " isTestOnly=0 And MerchantPD <= getDate() And Not Exists (Select tblLogImportEpa.ID From tblLogImportEpa Where tblLogImportEpa.TransID=t.ID)" : sAnd = " AND "
            Case 2 'Only paid transactions
                sQueryWhere &= sAnd & " isTestOnly=0 And Exists (Select tblLogImportEpa.ID From tblLogImportEpa Where tblLogImportEpa.TransID=t.ID)" : sAnd = " AND "
            Case 3 'Only not paid transactions
                sQueryWhere &= sAnd & " isTestOnly=0 And Not Exists (Select tblLogImportEpa.ID From tblLogImportEpa Where tblLogImportEpa.TransID=t.ID)" : sAnd = " AND "
        End Select

        sQueryString = dbPages.PageCombinedUrl
        'Response.Write(sQueryString)
        If Not String.IsNullOrEmpty(sQueryWhere) Then sQueryWhere = "WHERE " & sQueryWhere
        If Not String.IsNullOrEmpty(sQueryWhereNoCHB) Then sQueryWhereNoCHB = "WHERE " & sQueryWhereNoCHB
        If Request("UseSQL2") = "YES" Then
            dbPages.CurrentDSN = 2
            Response.Write(dbPages.ShowCurrentDSN)
        Else
            dbPages.CurrentDSN = 1
        End If

        '20111107 Tamir - filter by chargeback/retrieval reason
        Dim nReasonCode = dbPages.TestVar(Request("ChbReason"), 1, 0, -1)
        If nReasonCode >= 0 Then
            sQueryWhere &= sAnd & "(t.ID IN (SELECT TransPass_id FROM Trans.TransHistory WHERE TransHistoryType_id IN (4, 6) AND ReferenceNumber=" & nReasonCode & ") OR (t.OriginalTransID>0 AND t.OriginalTransID IN (SELECT TransPass_id FROM Trans.TransHistory WHERE TransHistoryType_id IN (4, 6) AND ReferenceNumber=" & nReasonCode & ")))"
            sAnd = " AND "
        End If

        'NetpayConst.GetDescConstArray(eGroupNames.GGROUP_DeniedStatus, 1, "", DeniedStatusDesc)
        Dim sJoinType As String = IIf(sQueryWhere.ToLower().Contains("cc."), "INNER", "LEFT")

        sSQL = "SELECT m.CompanyName, IsNull(cc.BINCountry, '--') BINCountry, t.PaymentMethod, t.CreditType, t.DeniedStatus, t.isTestOnly, t.PayID,"
        sSQL &= " t.ID, t.InsertDate, t.companyID, t.PaymentMethodDisplay, t.Payments, t.Amount, t.netpayFee_transactionCharge, t.Currency, t.DebitCompanyID, t.PrimaryPayedID,"
        sSQL &= " IPAddress, t.Comment, t.IsFraudByAcquirer, cc.Member as chName, cc.email as chEmail, cc.phonenumber as chPhone"
        If Request("Excel") = "" Then
            sSQL &= ", DeniedDate, OrderNumber, TerminalNUmber, ApprovalNumber, DeniedAdminComment, ExpMM, ExpYY, DebitReferenceCode,"
            sSQL &= " ba.address1, ba.address2, ba.city, zipCode, countryIso, CCard_First6"
        End If
        sSQL &= " FROM tblCompany m WITH (NOLOCK) INNER JOIN tblCompanyTransPass t WITH (NOLOCK) ON m.ID=t.companyID"
        sSQL &= " " & sJoinType & " JOIN tblCreditCard cc WITH (NOLOCK) ON t.CreditCardID=cc.ID"
        If Request("Excel") = "" Then
            sSQL &= " LEFT JOIN tblBillingAddress ba WITH (NOLOCK) ON cc.BillingAddressID=ba.ID"
        End If
        sSQL &= " " & sQueryWhere & " ORDER BY t.InsertDate DESC"
        PGData.OpenDataset(sSQL, 60)
        Server.ScriptTimeout = nScriptTimeout
    End Sub

    Private Sub ExportToExcel(ByVal sender As Object, ByVal e As System.EventArgs)
        PGData.CloseDataset()
        PGData.PageSize = 100000
        PGData.OpenDataset(sSQL)
        If Trim(Request("chk_Chargeback")) = "1" Then
            MCCHBData.Clear()
            VSCHBData.Clear()
            GetMonthGraphData(22, VSCHBData)
            GetMonthGraphData(25, MCCHBData)

            Dim objMCBitmap As New Bitmap(nGraphSideSize, nGraphSideSize)
            Dim objVSBitmap As New Bitmap(nGraphSideSize, nGraphSideSize)
            Dim objMCGraphic As Graphics = Graphics.FromImage(objMCBitmap)
            Dim objVSGraphic As Graphics = Graphics.FromImage(objVSBitmap)

            Dim whiteBrush As New SolidBrush(Color.WhiteSmoke)

            objMCGraphic.FillRectangle(whiteBrush, 0, 0, nGraphSideSize, nGraphSideSize)
            objVSGraphic.FillRectangle(whiteBrush, 0, 0, nGraphSideSize, nGraphSideSize)

            If MCCHBData.Count > 0 Then
                DrawVerticalAxis(objMCGraphic, MCCHBData, True)
                Draw3DBars(objMCGraphic, MCCHBData, True)
                WriteTheNames(objMCGraphic, MCCHBData, True)
            Else
                WriteNoData(objMCGraphic, True)
            End If

            If VSCHBData.Count > 0 Then
                DrawVerticalAxis(objVSGraphic, VSCHBData, False)
                Draw3DBars(objVSGraphic, VSCHBData, False)
                WriteTheNames(objVSGraphic, VSCHBData, False)
            Else
                WriteNoData(objVSGraphic, False)
            End If

            Dim MyGuiD As String = Guid.NewGuid().ToString()
            MCGraphName = "MC" & Date.Today.Year.ToString("0000") & Date.Today.Month.ToString("00") & Date.Today.Day.ToString("00") & "_" & MyGuiD & ".gif"
            VSGraphName = "VS" & Date.Today.Year.ToString("0000") & Date.Today.Month.ToString("00") & Date.Today.Day.ToString("00") & "_" & MyGuiD & ".gif"

            objMCBitmap.Save(Server.MapPath("/Data/Graphs/" & MCGraphName))
            objVSBitmap.Save(Server.MapPath("/Data/Graphs/" & VSGraphName))
        End If

        Dim sDate As String = String.Empty, sDeniedDate As String = String.Empty
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=report.xls")
        Response.Write("<table border=""1""><tr bgcolor=""#edf662"">")
        Response.Write("<th>ID</th><th>Date</th><th>MerchantID</th><th>Company</th><th>Amount</th><th>CreditType</th><th>Currency</th><th>Order#</th><th>Terminal#</th>")
        Response.Write("<th>Approval#</th><th>Denied Date</th><th>DeniedAdminComment</th><th>PaymentMethodDisplay</th><th>ExpMM</th><th>ExpYY</th>")
        Response.Write("<th>DebitReferenceCode</th><th>Address1</th><th>Address2</th><th>City</th><th>Zip</th><th>CountryISO</th><th>ccard_first6</th><th>IPAddress</th><th>TransComment</th>")
        Response.Write("<th>CH Name</th><th>CH Email</th><th>CH Phone</th></tr>")
        If Not TypeOf (Application.Get("CUR_ISO")) Is Array Then dbPages.LoadCurrencies()

        While PGData.Read()
            'sDate = ""
            'If Day(PGData("InsertDate")) < 10 Then
            '	sDate = sDate & "0"
            'End If
            'sDate = sDate & Day(PGData("InsertDate")) & "/"
            'If Month(PGData("InsertDate")) < 10 Then
            '	sDate = sDate & "0"
            'End If
            'sDate = sDate & Month(PGData("InsertDate")) & "/" & Right(Year(PGData("InsertDate")), 2) & "&nbsp;&nbsp;"
            'sDate = sDate & FormatDateTime(PGData("InsertDate"), 4)
            sDate = CType(PGData("InsertDate"), Date).ToString("dd/MM/yyyy hh:mm:ss")

            'sDeniedDate = ""
            'If Day(PGData("DeniedDate")) < 10 Then
            '	sDeniedDate = sDeniedDate & "0"
            'End If
            'sDeniedDate = sDeniedDate & Day(PGData("DeniedDate")) & "/"
            'If Month(PGData("DeniedDate")) < 10 Then
            '	sDeniedDate = sDeniedDate & "0"
            'End If
            'sDeniedDate = sDate & Month(PGData("DeniedDate")) & "/" & Right(Year(PGData("DeniedDate")), 2) & "&nbsp;&nbsp;"
            'sDeniedDate = sDate & FormatDateTime(PGData("DeniedDate"), 4)
            sDeniedDate = CType(PGData("DeniedDate"), Date).ToString("dd/MM/yyyy hh:mm:ss")
            If dbPages.TestVar(PGData("DeniedStatus"), 0, -1, 0) = 0 Then sDeniedDate = ""
            Response.Write("<tr>")
            Response.Write("<td>" & PGData("id") & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & sDate & "</td>")
            Response.Write("<td>" & PGData("companyID") & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("CompanyName") & "</td>")
            Response.Write("<td>" & PGData("Amount") & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("CreditType") & "</td>")
            Response.Write("<td>" & CType(Application.Get("CUR_ISO"), Array)(PGData("Currency")) & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("OrderNumber") & "</td>")
            Response.Write("<td>" & PGData("TerminalNumber") & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("ApprovalNumber") & "</td>")
            Response.Write("<td>" & sDeniedDate & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & dbPages.DBTextShow("" & PGData("DeniedAdminComment")) & "</td>")
            Response.Write("<td>" & dbPages.DBTextShow("" & PGData("PaymentMethodDisplay")) & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & dbPages.DBTextShow("" & PGData("ExpMM")) & "</td>")
            Response.Write("<td>" & dbPages.DBTextShow("" & PGData("ExpYY")) & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & dbPages.DBTextShow("" & PGData("DebitReferenceCode")) & "</td>")
            Response.Write("<td>" & dbPages.DBTextShow("" & PGData("address1")) & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & dbPages.DBTextShow("" & PGData("address2")) & "</td>")
            Response.Write("<td>" & dbPages.DBTextShow("" & PGData("city")) & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & dbPages.DBTextShow("" & PGData("zipCode")) & "</td>")
            Response.Write("<td>" & dbPages.DBTextShow("" & PGData("countryIso")) & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("CCard_First6") & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("IPAddress") & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("Comment") & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("chName") & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("chEmail") & "</td>")
            Response.Write("<td bgcolor=""#f0f0e8"">" & PGData("chPhone") & "</td>")
            Response.Write("</tr>")
        End While
        Response.Write("</table>")
        PGData.CloseDataset()

        If Trim(Request("chk_Chargeback")) = "1" Then
            Response.Write("<table border=""0"" cellpadding=""0"" cellspacing=""0"">")
            Response.Write("<tr><td colspan=""11"">&nbsp;</td>")
            Response.Write("<tr>")
            Response.Write("<td><img border=""1"" bordercolor=""gray"" src=""http" & IIf(Request.IsSecureConnection, "s", String.Empty) & "://" & Request.ServerVariables("SERVER_NAME") & ":" & Request.ServerVariables("SERVER_PORT") & "/Data/Graphs/" & MCGraphName & """></td>")
            Response.Write("<td colspan=""9""></td>")
            Response.Write("<td><img border=""1"" bordercolor=""gray"" src=""http" & IIf(Request.IsSecureConnection, "s", String.Empty) & "://" & Request.ServerVariables("SERVER_NAME") & ":" & Request.ServerVariables("SERVER_PORT") & "/Data/Graphs/" & VSGraphName & """></td>")
            Response.Write("</tr>")
            Response.Write("</table>")
        End If
        Response.End()
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript" src="../js/jquery-1.3.2.min.js"></script>
	<script language="JavaScript" src="../js/jquery.shiftcheckbox.js"></script>
	<script language="JavaScript">
		function IconVisibility(fVar) {
			spanObj = document.getElementById(fVar)
			if (spanObj.style.visibility == 'hidden') {
				spanObj.style.visibility = 'visible';
			}
			else {
				spanObj.style.visibility = 'hidden';
			}
		}
		function toggleInfo(TransId, PaymentMethod, bkgColor) {			
			objRefA = document.getElementById('Row' + TransId + 'A')
			objRefB = document.getElementById('Row' + TransId + 'B')			
			if (objRefB && objRefB.innerHTML == '') {
				if (objRefA) { objRefA.style.display = 'none'; }
				if (PaymentMethod >= <%= ePaymentMethod.PMD_EC_MIN %>)
				{
					UrlAddress = 'trans_detail_eCheck.asp';
				}
				else
				{
					switch (PaymentMethod)
					{
						case '4':
							UrlAddress = 'trans_detail_RollingRes.asp';
							break;
						case '1':
						case '2':
							UrlAddress = 'Trans_DetailAdmin.asp';
							break;
						default:
							UrlAddress = 'trans_detail_cc.asp';
					}
				}
				if (objRefB) {
						objRefB.innerHTML = '<iframe framespacing="0" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" width="100%" height="0px" src="' + UrlAddress + '?transID=' + TransId + '&PaymentMethod=' + PaymentMethod + '&bkgColor=' + bkgColor + '&TransTableType=pass" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>';
					}
			}
			else {
				if (objRefA) { objRefA.style.display = ''; }
				if (objRefB) { objRefB.innerHTML = ''; }
			}
		}
		function ExpandNode(fTrNum, TransId, PaymentMethod, bkgColor) {
			toggleInfo(TransId, PaymentMethod, bkgColor);
			var imgPlus=document.getElementById("oListImg"+fTrNum);
			imgPlus.src = (imgPlus.src.indexOf("tree_expand.gif")>0 ? "../images/tree_collapse.gif" : "../images/tree_expand.gif");
		}
		function OpenPop(sURL, sName, sWidth, sHeight, sScroll) {
			placeLeft = screen.width / 2 - sWidth / 2
			placeTop = screen.height / 2 - sHeight / 2
			var winPrint = window.open(sURL, sName, 'TOP=' + placeTop + ',LEFT=' + placeLeft + ',WIDTH=' + sWidth + ',HEIGHT=' + sHeight + ',scrollbars=' + sScroll);
		}

		function nodelete() {
			for (i = 0; i < document.getElementsByTagName("INPUT").length; i++) {
				if (document.getElementsByTagName("INPUT")[i].type == "checkbox") {
					document.getElementsByTagName("INPUT")[i].checked = false;
				}
			}
		}
		function checkdelete() {
			var isChecked = false;
			for (i = 0; i < document.getElementsByTagName("INPUT").length; i++) {
				if (document.getElementsByTagName("INPUT")[i].type == "checkbox" && document.getElementsByTagName("INPUT")[i].name=="ID" && document.getElementsByTagName("INPUT")[i].checked) {
					isChecked = true;
					break;
				}
			}
			if (!isChecked) {
				alert("Please choose transactions to delete!");
				return false;
			}
			else {
				return confirm('Sure you want to delete?');
			}
		}

		$(document).ready(
        function() {
        	$('.shiftCheckbox').shiftcheckbox();
        });		
	</script>
</head>
<body>
	<div align="center" id="waitMsg">
		<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
				<tr><td style="color: gray;">Loading data &nbsp;-&nbsp; Please wait<br /></td></tr>
				<tr><td height="4"></td></tr>
				<tr>
					<td><img src="../images/loading_animation_orange.gif" style="border:1px solid silver;" alt="" width="123" height="6" border="0"><br /></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</div>
	<%
	Response.Flush
	%>
	<form id="Form1" runat="server">

		<table align="center" style="width:92%" border="0">
		<tr>
			<td>
				<asp:label ID="lblPermissions" runat="server" />
				<span id="pageMainHeading">TRANSACTIONS</span>
			</td>
			<td valign="top">
				<table border="0" cellspacing="0" cellpadding="1" align="right">
			<tr>
				<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
				<td width="60" bgcolor="#edfeed" style="font-size:10px">Debit</td>
				<td width="10"></td>
				<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
				<td width="60" bgcolor="#edfeed" style="color:#cc0000; font-size:10px">Refund</td>
				<td width="10"></td>
				<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
				<td width="110" bgcolor="#fff3dd" style="font-size:10px">Retrieval Request</td>
				<td width="10"></td>
				<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
				<td width="40" bgcolor="#ffebe1" style="font-size:10px">Chb</td>
                <td width="10"></td>
				<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
				<td width="40" bgcolor="#d8d8d8" style="font-size:10px">Fraud</td>
				<td width="10"></td>
				<td width="6" bgcolor="#003399"></td><td width="1"></td>
				<td width="60" bgcolor="#eeeffd" style="font-size:10px">Admin</td>
				<td width="10"></td>
				<td width="6" bgcolor="#484848"></td><td width="1"></td>
				<td width="60" bgcolor="#f5f5f5" style="font-size:10px">System</td>
			</tr>
				</table>			
			</td>			
		</tr>
		<tr><td colspan="2"><br /></td></tr>
		<tr>
			<td style="font-size:11px; padding-bottom:4px;">
				(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');">Collapse All</a>)
			</td>
			<td align="right" style="font-size:11px;">
				Filters:
				<%
				If Trim(Request("iMerchantID"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"iMerchantID",Nothing) &"';"">Merchant</a>") : sDivider = ", "
				'If Trim(Request("iStatus")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iStatus", Nothing) & "';"">" & sStatusTxtPrelim & "</a>") : sDivider = ", "
				If sDivider <>"" Then Response.Write(" | (click to remove)") _
					Else Response.Write("Add to filter by clicking on returned fields")
				%>
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th colspan="2" style="width:30px;"><br /></th>
			<th>ID</th>
			<th>Date</th>
			<th>Merchant Name</th>
			<th>Payment Method</th>
			<th>Credit Type</th>
			<th>Ins.</th>					
			<th>Amount</th>
			<th>Status</th>
			<th>Debit</th>			
			<th>Del</th>			
		</tr>
		<%
		Dim statusTxt As String = String.Empty
		Dim sTrBkgColor As String = String.Empty
		Dim sTdBkgColorFirst As String = String.Empty
		Dim sTdFontColor As String = String.Empty
		Dim sPayStatusText As String = String.Empty
		Dim bShowDeniedOptions As Boolean = False
		Dim bAllowDelete As Boolean = False
			
		Dim tableType As String = String.Empty
		Dim transid As Integer = -1
		Dim sStatParam As String = String.Empty			
		nShowCounter = 0
		Dim sRowStyle As String	

		Dim sbExpandAll As New StringBuilder(String.Empty)
			
		While PGData.Read()
			i += 1
			nShowCounter += 1			
			sRowStyle = String.Empty
			statusTxt = String.Empty
			sTrBkgColor = String.Empty
			sTdBkgColorFirst = String.Empty
			tableType = String.Empty
			transid = -1
			sStatusColor = String.Empty
			sStatParam = String.Empty				
			GetTransParam(PGData, sTrBkgColor, sStatusColor, sTdFontColor, sPayStatusText, bShowDeniedOptions, bAllowDelete)
		    Dim lPaymentMethod As Long = dbPages.TestVar(PGData("PaymentMethod"), 0, -1, 0)
		    If PGData("deniedstatus") = eDeniedStatus.DNS_DupFoundValid Or PGData("deniedstatus") = eDeniedStatus.DNS_DupFoundUnValid Then _
		        sRowStyle = "background-image:url('\Images\img\bkg_slash.gif');"
		    
			If nShowCounter<>1 Then
				Response.Write("<tr><td height=""1"" colspan=""14""  bgcolor=""#ffffff""></td></tr>")
				Response.Write("<tr><td height=""1"" colspan=""14""  bgcolor=""silver""></td></tr>")
				Response.Write("<tr><td height=""1"" colspan=""14""  bgcolor=""#ffffff""></td></tr>")
			End If			
			sbExpandAll.AppendFormat("ExpandNode('{0}','{1}','{2}','" & sTrBkgColor.Replace("#", String.Empty) & "');", i, PGData("ID"), PGData("PaymentMethod"))
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='<%=sTrBkgColor%>';" style="background-color:<%=sTrBkgColor%>;<%=sRowStyle%>">			
				<td style="width:18px;">
					<%If lPaymentMethod = ePaymentMethod.PMD_Admin Or lPaymentMethod = ePaymentMethod.PMD_ManualFee OR lPaymentMethod > 14 OR lPaymentMethod=ePaymentMethod.PMD_RolRes Then %>
					<img onclick="ExpandNode('<%=i%>','<%= PGData("ID") %>','<%= PGData("PaymentMethod") %>','<%= replace(sTrBkgColor,"#","") %>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" /><br />
					<%Else %>
					<img src="../images/tree_disabled.gif" alt="" width="16" height="16" border="0" align="middle" /><br />
					<%End If %>
				</td>
				<td><span Style="background-color:<%= sStatusColor %>;width:10px;">&nbsp;</span></td>
				<td style="color:<%= sTdFontColor %>;"><%= PGData("id") %><br /></td>
				<td style="color:<%= sTdFontColor %>;" nowrap="nowrap"><%=dbPages.FormatDateTime(PGData("InsertDate"),True,True,False,False)%></td>
				<td>
					<a href="javascript:OpenPop('Reports_CompanyDataPrintview.asp?proceed=false&CompanyID=<%= PGData("companyID") %>','fraCompanyData',650,300,1);" title="<%= dbPages.dbtextShow(""&PGData("CompanyName")) %>">
					<%
						If Not IsDBNull(PGData("CompanyName")) Then
							If Trim(Request("iMerchantID")) <> "" Then Response.Write(PGData("CompanyName")) _
							 Else dbPages.showFilter(PGData("CompanyName"), "?" & sQueryString & "&iMerchantID=" & PGData("CompanyID"), 1)
						End If
					%></a><br />
				</td>
				<td style="color:<%= sTdFontColor %>;">
					<%
						If lPaymentMethod >= ePaymentMethod.PMD_CC_UNKNOWN Then
					        Response.Write("<img src=""/NPCommon/ImgPaymentMethod/23X12/" & PGData("PaymentMethod") & ".gif"" alt="""" align=""middle"" border=""0"" /> &nbsp;")
							If Trim("" & PGData("BinCountry")) <> "" Then
					            Response.Write("<img src=""/NPCommon/ImgCountry/18X12/" & PGData("BinCountry") & ".gif"" align=""middle"" border=""0"" />")
								Response.Write(" &nbsp;" & IIf(PGData("BinCountry")="--", String.Empty, PGData("BinCountry")))
							End If
						End If
						Response.Write(" " & Trim("" & PGData("PaymentMethodDisplay")))
					%>
				</td>
				<td style="color:<%= sTdFontColor %>;">
					<%
						Select Case PGData("CreditType")
							Case 0 : Response.Write("Refund")
							Case 8 : Response.Write("Installments")
							Case Else : Response.Write("Regular")
						End Select
					%>
				</td>
				<td style="color:<%= sTdFontColor %>;"><%= PGData("Payments") %><br /></td>
				<td style="color:<%= sTdFontColor %>;" nowrap>
					<%
					If (PGData("PaymentMethod") = ePaymentMethod.PMD_ManualFee Or PGData("PaymentMethod") = ePaymentMethod.PMD_SystemFees) And PGData("netpayFee_transactionCharge") <> 0 Then _
						Response.Write(dbPages.FormatCurrWCT(PGData("Currency"), PGData("netpayFee_transactionCharge"), 0)) _
					Else Response.Write(dbPages.FormatCurrWCT(PGData("Currency"), PGData("Amount"), PGData("CreditType")))
					%>
				</td>
				<td style="color:<%= sTdFontColor %>;">
					<%= sPayStatusText %>
					<%
					If PGData("PrimaryPayedID") > 0 Then
						%>
						(<a href="Merchant_transSettledListData.asp?TransactionPayID=<%= PGData("PrimaryPayedID") %>" style="color:<%= sTdFontColor %>;"><%= PGData("PrimaryPayedID") %></a>)
						<%
					End If
					If PGData("deniedstatus")>0 Then Response.Write("[" & PGData("deniedstatus") & "]")
					%>
				</td>
				<td style="color:<%= sTdFontColor %>;">
					<%
					If lPaymentMethod > 14 And Not Convert.IsDBNull(PGData("DebitCompanyID")) Then
						If My.Computer.FileSystem.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & Trim(PGData("DebitCompanyID")) & ".gif")) Then
					            Response.Write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(PGData("DebitCompanyID")) & ".gif"" align=""middle"" border=""0"" />")
						End If
					End If
					%>
				</td>
				<td>
					<input type="Checkbox" name="ID" style="background-color:<%= sTrBkgColor %>;" <% If NOT bAllowDelete then %>disabled<% End If %> value="<%= PGData("ID") %>" class="shiftCheckbox">
				</td>
			</tr>
			<%
			If False or PGData("deniedstatus")>0 Then
				%>
				<tr bgcolor="<%= sTrBkgColor %>">
					<td colspan="3"></td>
					<td colspan="11"><%'DeniedStatusDesc(PGData("deniedstatus")) %></td>
				</tr>				
				<%				
			End if
			response.write ("<tr><td colspan=""2""></td><td id=""Row" & PGData("ID") & "B"" colspan=""14""></td></tr>")

			If i mod PGData.PageSize = 0 then
				Response.Write("<tr><td height=""1"" colspan=""14""  bgcolor=""#ffffff""></td></tr>")
				Response.Write("<tr><td height=""1"" colspan=""14""  bgcolor=""silver""></td></tr>")
				Response.Write("<tr><td height=""1"" colspan=""14""  bgcolor=""#ffffff""></td></tr>")					
			End If				
		End while
		PGData.CloseDataset()
		%>
		</table>
		<script language="JavaScript" type="text/javascript">
			function ExpandAll(fDisplay, fImgStatus) {
				<%= sbExpandAll.ToString %>;
			}
		</script>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" align="middle"> ADD TO FILTER
		</div>
		<%
		If Trim(Request("iPageSize")) <> "" Then
		 Response.Write("<input type=""hidden"" name=""iPageSize"" value=""" & Trim(Request("iPageSize")) & """>")
		End if
		%>
		<table align="center" style="width:92%">
		<tr>
			<td><UC:Paging runat="Server" id="PGData" PageID="PageID" /></td>
			<td style="text-align:right">
				<%--<asp:Button ID="btnGraph" runat="server" CssClass="buttonWhite" Text="View CHB Graphs" OnClick="btnGraph_Click" Visible="false" OnClientClick="nodelete()" />&nbsp;--%>
				<asp:Button ID="btnExcel" runat="server" CssClass="buttonWhite" OnClick="ExportToExcel" Text="View Excel" OnClientClick="nodelete()" />&nbsp;
				<asp:Button ID="btnDelete" CssClass="buttonWhite" runat="server" Text="Delete Selected" OnClientClick="return checkdelete()" />
			</td>
		</tr>
		</table>	
	</form>
	<script language="JavaScript1.2" type="text/javascript">
		waitMsg.style.display = 'none';
	</script>
		
</body>
</html>