<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" MasterPageFile="~/AdminCash/FiltersMaster.master" Title="Invoices - Filter" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<script runat="server">
	Sub Page_Load()
		fdtrControl.FromDateTime = Date.Now.AddMonths(-1).AddDays(1).ToShortDateString
		fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
	End Sub
</script>
<asp:Content ContentPlaceHolderID="body" runat="server">
	<form runat="server" action="Log_window_charge.aspx" method="get" target="frmBody" onsubmit="return filterForm_Submit(null)">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
	<input type="hidden" name="from" value="menu" />
	<input type="hidden" name="ShowCompanyID" id="ShowCompanyID" />

	<h1><asp:label ID="lblPermissions" runat="server" /> Hosted Page<span> - Filter</span></h1>
	<table class="filterNormal">
		<tr>
			<td colspan="2">
				<Uc1:FilterMerchants id="FMerchants" ClientField="ShowCompanyID" SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
				 INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Request Details</th></tr>
		<tr>
			<td colspan="2">
				<Uc1:FilterDateTimeRange Title="" ID="fdtrControl" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Misc</th></tr>
		<tr>
			<td>IP Address</td>
			<td><input type="text" style="background-color:#f5f5f5;" name="iIPAddress" /></td>
		</tr>
		<tr>
			<td>Reply Code</td>
			<td><input type="text" style="background-color:#f5f5f5;" name="ReplyCode" /></td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			 <td colspan="2"><input type="checkbox" name="SQL2" value="1" checked="checked" class="option" />Use SQL2 when possible</td>
		</tr>
		<tr>
			<td class="indent">
				<select name="iPageSize">
				<option value="20" selected="selected">20 Rows/page</option>
				<option value="50">50 Rows/page</option>
				<option value="75">75 Rows/page</option>
				<option value="100">100 Rows/page</option>
	    		</select>
			</td>
			<td class="indent" align="right" nowrap="nowrap">
				<input type="submit" class="buttonFilter" value="Search" />
			</td>
		</tr>
	</table>
	</form>
</asp:Content>
