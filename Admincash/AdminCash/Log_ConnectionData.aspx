<%@ Page Language="VB" EnableViewStateMac="false" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>

<script runat="server">
	Dim sSQL As String, sSQL2 As String, sQueryWhere As String, sDateDiff As String, sStatusColor As String, sAnd As String, sQueryString As String, sTmpTxt As String, sDivider As String
	Dim nFromDate As String, nToDate As String
	Dim i As Integer, nTmpNum As Integer, sStandardDecline As String

	Sub DisplayContent(sVar As String)
		Dim sVarShow As String
		If Trim(PGData(sVar).ToString) <> "" Then
			If Trim(sVar) = "RequestQueryString" Then sVarShow = Replace(Trim(PGData(sVar)),"&"," &") Else sVarShow = Trim(PGData(sVar).ToString)
			Response.Write("<span class=""key"">" & Trim(sVar) & ":</span> " & sVarShow & "<br />")
		End If
	End Sub
	
	Public Function FormatTotalValue(ByVal nCurrency As Byte, ByVal nCount As Integer, ByVal nSum As Decimal, ByVal nTax As Decimal) As String
		If nCount = 0 Then Return String.Empty
		Return "(" & nCount & ")" & dbPages.FormatCurr(nCurrency, nSum) & " + " & dbPages.FormatCurr(nCurrency, nTax) & "=" & dbPages.FormatCurr(nCurrency, nSum + nTax)
	End Function
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		nFromDate = Trim(Request("fromDate")) & " " & Trim(Request("fromTime"))
		nToDate = Trim(request("toDate")) & " " & Trim(request("toTime"))
		If Trim(nFromDate) <> "" Then sQueryWhere &= " AND tblLog_NoConnection.lnc_InsertDate >= '" & CType(nFromDate, DateTime).ToString("dd/MM/yy HH:mm") & "'"
		If Trim(nToDate) <> "" Then sQueryWhere &= " AND tblLog_NoConnection.lnc_InsertDate <= '" & CType(nToDate, DateTime).ToString("dd/MM/yy  HH:mm") & "'"
		If Not String.IsNullOrEmpty(Request("TerminalNumber")) Then sQueryWhere &= " AND lnc_TerminalNumber='" & dbPages.DBText(Request("TerminalNumber")) & "'"
		If Not String.IsNullOrEmpty(Request("Status")) Then sQueryWhere &= " AND lnc_AutoRefundStatus IN (" & Request("Status") & ")"
		If Not String.IsNullOrEmpty(Request("DebitCompanyID")) Then sQueryWhere &= " AND tblDebitCompany.debitCompany_id=" & dbPages.TestVar(Request("DebitcompanyID"), 1, 0, 0)
		If Security.IsLimitedMerchant Then sQueryWhere &= " AND lnc_CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
		If Security.IsLimitedDebitCompany Then sQueryWhere &= " AND tblDebitCompany.debitCompany_id IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
		If Trim(Request("iInsertDate")) <> "" Then
			Dim dtInsert As Date = CType(Request("iInsertDate"), Date)
			sQueryWhere &= " AND tblLog_NoConnection.lnc_InsertDate BETWEEN '" & dtInsert.ToString("dd/MM/yy") & "' AND '" & dtInsert.AddDays(1).ToString("dd/MM/yy") & "'"
		End If
		If Trim(Request("iMerchantID")) <> "" Then sQueryWhere &= " AND tblLog_NoConnection.lnc_CompanyID = '" & Trim(Request("iMerchantID")) & "'"
		If Trim(Request("iIPAddress")) <> "" Then sQueryWhere &= " AND tblLog_NoConnection.lnc_IPAddress = '" & Trim(Request("iIPAddress")) & "'"
		If Trim(Request("iReplyCode")) <> "" Then sQueryWhere &= " AND tblLog_NoConnection.lnc_DebitReturnCode = '" & Trim(Request("iReplyCode")) & "'"
		If Not String.IsNullOrEmpty(Request("TransFail")) Then
			If dbPages.TestVar(Request("TransFail"), 1, 0, 0) > 0 Then
				sQueryWhere &= " AND lnc_TransactionFailID = " & Request("TransFail")
			Else
				Dim nTransFailCount As Integer = 0
				Dim sTransList As String = dbPages.TestNumericList(Request("TransFail"), nTransFailCount)
				If nTransFailCount > 0 Then sQueryWhere &= " AND lnc_TransactionFailID IN (" & sTransList & ")"
			End If
		End If
		Select Case dbPages.TestVar(Request("RefundAttempt"), 0, 1, 2)
			Case 0
				sQueryWhere &= " AND lnc_AutoRefundStatus=0"
			Case 1
				sQueryWhere &= " AND lnc_AutoRefundStatus>0"
		End Select
		If Trim(Request("iReplyCode")) <> "" Then sQueryWhere &= " AND tblLog_NoConnection.lnc_DebitReturnCode = '" & Trim(Request("iReplyCode")) & "'"
		'If Trim(Request("iTerminalNumber")) <> "" Then sQueryWhere &= " AND (tblLog_NoConnection.lnc_TerminalNumber = '" & Trim(Request("iTerminalNumber")) & "')"
		If Trim(Request("HTTP_Error")) <> "" Then sQueryWhere &= " AND tblLog_NoConnection.lnc_HTTP_Error LIKE '%" & Trim(Request("HTTP_Error")) & "%'"
		If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere.Substring(5)
		If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") Else PGData.PageSize = 25
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		sQueryString = dbPages.SetUrlValue(sQueryString, "PageID", "1")
		
		If Request("Totals") = "1" Then
			Response.Write("<table class=""formThin"" width=""100%""><tr>")
			Response.Write("<td>TOTALS</td><td style=""text-align:right;cursor:pointer;float:right;"" onclick=""document.getElementById('tblTotals').style.display='none';""><b>X</b></td>")
			Response.Write("</tr></table>")
			Response.Write("<table class=""formNormal"" width=""250"" style=""margin:8px 5px;"">")
			Response.Write("<tr><th style=""text-align:right"" width=""50%"">Count</th><th style=""text-align:right"">Amount</th></tr>")
			sSQL2 = "Select lnc_Currency, Count(*) as counter, Sum(lnc_Amount) as Amount FROM tblLog_NoConnection" & _
			" LEFT JOIN tblGlobalData ON tblLog_NoConnection.lnc_Currency = tblGlobalData.GD_ID AND GD_Group=48 AND GD_Lng=1" & _
			" INNER JOIN tblDebitTerminals ON tblLog_NoConnection.lnc_TerminalNumber = tblDebitTerminals.TerminalNumber" & _
			" INNER JOIN tblCompanyTransFail ON tblCompanyTransFail.ID = tblLog_NoConnection.lnc_TransactionFailID" & _
			" INNER JOIN tblDebitCompany ON tblCompanyTransFail.DebitCompanyID=tblDebitCompany.debitCompany_id" & _
			" " & sQueryWhere & "Group by lnc_Currency"
				'Response.Write(sSQL2) : Response.End()
			Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL2)
			While iReader.Read()
				Response.Write("<tr><td style=""text-align:right"">" & iReader("counter") & "</td><td style=""text-align:right"">" & dbPages.FormatCurr(iReader(0), iReader("Amount")) & "</td></tr>")
			End While
			iReader.Close()
			Response.Write("</table>")
			Response.End() 
		End If
	End Sub
	
	Private Sub ExportToExcel(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim sSQL As String = ""
		Dim iReader As SqlDataReader
        sSQL = "SELECT tblLog_NoConnection.*, List.TransSource.Name AS TransTypeName, tblDebitTerminals.dt_name, " & _
        " tblDebitCompany.dc_name, tblDebitCompany.debitCompany_id,tblCompanyTransFail.PaymentMethodDisplay,tblCompanyTransFail.PaymentMethod," & _
        " GD_Text, GD_Color, tblCreditCard.CCard_First6, tblCreditCard.CCard_Last4, tblCompanyTransFail.DebitReferenceCode " & _
        " FROM tblLog_NoConnection INNER JOIN List.TransSource ON tblLog_NoConnection.lnc_TransactionTypeID = List.TransSource.TransSource_id" & _
        " INNER JOIN tblDebitTerminals ON tblLog_NoConnection.lnc_TerminalNumber = tblDebitTerminals.TerminalNumber" & _
        " INNER JOIN tblCompanyTransFail ON tblCompanyTransFail.ID = tblLog_NoConnection.lnc_TransactionFailID" & _
        " INNER JOIN tblDebitCompany ON tblCompanyTransFail.DebitCompanyID=tblDebitCompany.debitCompany_id" & _
        " LEFT JOIN tblCreditCard ON tblCreditCard.ID = tblCompanyTransFail.CreditCardID" & _
        " LEFT JOIN tblGlobalData ON lnc_AutoRefundStatus=GD_ID AND GD_Group=65 AND GD_Lng=1" & _
        " " & sQueryWhere & " ORDER BY tblLog_NoConnection.lnc_InsertDate Desc"
		iReader = dbPages.ExecReader(sSQL)
		If iReader.HasRows then
			Response.ContentType = "application/vnd.ms-excel"
			Response.AddHeader("Content-Disposition", "attachment;filename=report.xls")
			Response.Write("<table border=""1""><tr bgcolor=""#edf662"">")
			Response.Write("<th>Date</th><th>Debit Company</th><th>Debit Reference</th><th>CC BIN</th><th>CC Last 4</th><th>Terminal name</th><th>Terminal number</th><th>Currency</th><th align=""right"">Amount</th><th>HTTP Error</th>")
			Response.Write("</tr>")
			While iReader.Read()
				Response.Write("<tr>")
				Response.Write("<td>" & CType(Trim(iReader("lnc_InsertDate")), DateTime).ToString("dd/MM/yy HH:mm") & "</td>")
				Response.Write("<td>" & dbPages.dbtextShow(iReader("dc_Name")) & "</td>")
				Response.Write("<td align=""left"">" & iReader("DebitReferenceCode") & "</td>")
				If iReader("PaymentMethod") > 14 Then
					Response.Write("<td>" & iReader("CCard_First6") & "</td>")
					Response.Write("<td>" & iReader("CCard_Last4") & "</td>")
				Else
					Response.Write("<td></td><td></td>")
				End If								
				Response.Write("<td align=""left"">" & IIf(iReader("dt_name")<>"",dbPages.DBText(iReader("dt_name")) ,"") & "</td>")
				Response.Write("<td align=""left"">" & iReader("lnc_TerminalNumber") & "</td>")
				Response.Write("<td>" & dbPages.GetCurISO(iReader("lnc_Currency")) & "</td>")
				Response.Write("<td align=""right"">" & iReader("lnc_Amount") & "</td>")
				Response.Write("<td>" & iReader("lnc_HTTP_Error") & "</td>")
				Response.Write("</tr>")
			End While
			Response.Write("</table>")
		End If
		iReader.Close
		Response.End()
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript" src="../js/func_common.js" type="text/javascript"></script>
	<script language="JavaScript" type="text/javascript">
		function IconVisibility(fVar){
			spanObj = document.getElementById(fVar)
			if (spanObj.style.visibility == 'hidden') {
				spanObj.style.visibility = 'visible';
			}
			else {
				spanObj.style.visibility = 'hidden';
			}
		}
		function ExpandNode(fTrNum,fTransId) {
			trObj = document.getElementById("trInfo" + fTrNum)
			divInfo = document.getElementById("divInfo" + fTrNum)
			divRefund = document.getElementById("divRefund" + fTrNum)
			imgObj = document.getElementById("oListImg" + fTrNum)
			if (trObj) {
				if (trObj.style.display == '') {
					imgObj.src = '../images/tree_expand.gif';
					divInfo.innerHTML = '';
					divRefund.innerHTML = '';
					trObj.style.display = 'none';
				}
				else {
					PaymentMethod = '1';
					if (PaymentMethod=='1'){ UrlAddress = 'trans_detail_cc.asp'; } else { UrlAddress = 'trans_detail_eCheck.asp'; }
					divInfo.innerHTML='<iframe framespacing="0" scrolling="no" marginwidth="0" frameborder="0" width="100%" height="0px" src="'+UrlAddress+'?transID='+fTransId+'&PaymentMethod='+PaymentMethod+'&bkgColor=ffffff&TransTableType=fail" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>';
					divRefund.innerHTML='<iframe framespacing="0" scrolling="no" marginwidth="0" frameborder="0" width="100%" height="0px" src="Log_ConnectionData_RefundLog.aspx?TransID='+fTransId+'" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>';
					imgObj.src = '../images/tree_collapse.gif';
					trObj.style.display = '';
				}
			}
		}
	</script>
</head>
<body>
	<form id="Form1" runat="server">
	<table align="center" style="width:95%;">
		<tr>
			<td id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> LOG CONNECTION ERRORS<br />
			</td>
			<td style="font-size:11px;text-align:right;">
				<%
					Dim drColors As SqlDataReader = dbPages.ExecReader("SELECT GD_Text, GD_Color FROM tblGlobalData WHERE GD_Group=65 AND GD_Lng=1")
					While drColors.Read
						Response.Write(" &nbsp; &nbsp; <span style=""background-color:" & drColors(1) & ";"">&nbsp;&nbsp;</span> " & drColors(0))
					End While
					drColors.Close()
				%><br />
			</td>
		</tr>
		<tr><td id="pageMainSubHeading">520 - Reply with wrong format, 521 - No reply</td></tr>
		<tr>
			<td colspan="2" style="font-size:11px;text-align:right;">
				Filters:
				<%
				If Trim(Request("iInsertDate"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"iInsertDate",Nothing) &"';"">Insert Date</a>") : sDivider = ", "
				If Trim(Request("iMerchantID"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"iMerchantID",Nothing) &"';"">Merchant</a>") : sDivider = ", "
				If Trim(Request("iIPAddress"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"iIPAddress",Nothing) &"';"">IP Address</a>") : sDivider = ", "
				If sDivider <> "" Then Response.Write(" | (click to remove)") Else Response.Write("Add to filter by clicking on returned fields")
				%>
			</td>
		</tr>
	</table>
	<%
		sSQL = "SELECT tblLog_NoConnection.*, List.TransSource.Name AS TransTypeName, tblDebitTerminals.dt_name, " & _
		" tblDebitCompany.dc_name, tblDebitCompany.debitCompany_id,tblCompanyTransFail.PaymentMethodDisplay,tblCompanyTransFail.PaymentMethod," & _
		" GD_Text, GD_Color" & _
		" FROM tblLog_NoConnection INNER JOIN List.TransSource ON tblLog_NoConnection.lnc_TransactionTypeID = List.TransSource.TransSource_id" & _
		" INNER JOIN tblDebitTerminals ON tblLog_NoConnection.lnc_TerminalNumber = tblDebitTerminals.TerminalNumber" & _
		" INNER JOIN tblCompanyTransFail ON tblCompanyTransFail.ID = tblLog_NoConnection.lnc_TransactionFailID" & _
		" INNER JOIN tblDebitCompany ON tblCompanyTransFail.DebitCompanyID=tblDebitCompany.debitCompany_id" & _
		" LEFT JOIN tblGlobalData ON lnc_AutoRefundStatus=GD_ID AND GD_Group=65 AND GD_Lng=1" & _
		" " & sQueryWhere & " ORDER BY tblLog_NoConnection.lnc_InsertDate Desc"
		PGData.OpenDataset(sSQL)
	If PGData.HasRows Then
		%>
		<table class="formNormal" align="center" style="width:95%">
		<tr>
			<th colspan="2" style="width:30px;"><br /></th>
			<th>Reply</th>
			<th>Trans</th>
			<th>Date</th>
			<th>Merchant</th>
			<th>Debit</th>
			<th>PM</th>
			<%--<th>Terminal</th>--%>
			<th>IP Address</th>
			<th style="text-align:right;">Amount</th>
			<th>HTTP Error</th>
			<td width="20"></td>
			<th>Refund Status</th>
			<th>Last Attempt</th>
			<td width="20"></td>
			<th style="text-align:center;">Actions</th>
		</tr>
		<%
		While PGData.Read()
			i = i + 1
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='#ffffff';">
				<td><img onclick="ExpandNode('<%=i%>','<%=PGData("lnc_TransactionFailID")%>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" /><br /></td>
				<td><span style="background-color:<%= PGData("GD_Color") %>;" title="<%=PGData("GD_Text")%>">&nbsp;&nbsp;</span></td>
				<td><%=PGData("lnc_DebitReturnCode")%></td>
				<td style="white-space:nowrap;"><%=PGData("lnc_TransactionFailID")%></td>
				<td style="white-space:nowrap;">				
					<%
					If Trim(Request("iInsertDate"))<>"" Then Response.Write(Trim(PGData("lnc_InsertDate"))) _
						Else dbPages.showFilter(CType(Trim(PGData("lnc_InsertDate")), DateTime).ToString("dd/MM/yy HH:mm"), "?" & sQueryString & "&iInsertDate=" & CType(Trim(PGData("lnc_InsertDate")), DateTime).ToString("dd/MM/yy"), 1)
					%>  
				</td>
				<td style="white-space:nowrap;">
					<%
					If Trim(Request("iMerchantID"))<>"" Then Response.Write(dbPages.dbtextShow(PGData("lnc_CompanyName"))) _
					Else dbPages.showFilter(dbPages.dbtextShow(PGData("lnc_CompanyName")), "?" & sQueryString & "&iMerchantID=" & PGData("lnc_CompanyID"), 1)
					%>
				</td>
				<td style="white-space:nowrap;">
					<%
					If PGData("debitCompany_id") > 0 Then
					        If IO.File.Exists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & Trim(PGData("debitCompany_id")) & ".gif")) Then
					            Response.Write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(PGData("debitCompany_id")) & ".gif"" align=""middle"" border=""0"" /> ")
					        End If
					End If
					%>
				</td>
				<td style="white-space:nowrap;">
					<%
					If PGData("PaymentMethod") > 14 Then
						Response.Write("<img src=""/NPCommon/ImgPaymentMethod/23X12/" & PGData("PaymentMethod") & ".gif"" align=""middle"" border=""0"" /> &nbsp;")
					End If
					%>
				</td>
				<%--<td style="white-space:nowrap;">
					<%
					sTmpTxt = IIf(PGData("dt_name")<>"",dbPages.DBText(PGData("dt_name")),PGData("lnc_TerminalNumber"))
					If Trim(Request("iTerminalNumber"))<>"" Then Response.Write(sTmpTxt) _
						Else dbPages.showFilter(sTmpTxt, "?" & sQueryString & "&iTerminalNumber=" & PGData("lnc_TerminalNumber"), 1)
					%>
				</td>--%>
				<td style="white-space:nowrap;">
					<%
					If Trim(Request("iIPAddress")) <> "" Then
						Response.Write(PGData("lnc_IPAddress"))
					Else
						dbPages.showFilter(PGData("lnc_IPAddress"), "?" & sQueryString & "&iIPAddress=" & PGData("lnc_IPAddress"), 1)
					End If
					%>
				</td>
				<td style="text-align:right;"><%=dbPages.FormatCurr(PGData("lnc_Currency"),PGData("lnc_Amount"))%></td>
				<td><%=IIf(Len(PGData("lnc_HTTP_Error")) > 17, "<span title=""" & PGData("lnc_HTTP_Error") & """>" & Left(PGData("lnc_HTTP_Error"), 14) & "</span>" & "...", PGData("lnc_HTTP_Error"))%></td>
				<td></td>
				<td><%=PGData("GD_Text")%></td>
				<td><%=IIf(PGData("lnc_AutoRefundStatus") = 0, "", dbPages.FormatDateTime(PGData("lnc_AutoRefundDate")))%></td>
				<td></td>
				<td style="text-align:center;">
					<%
						If PGData("lnc_AutoRefundStatus") = 0 Or PGData("lnc_AutoRefundStatus") = 3 Then
						%>
						<a target="ifrHidden" onclick="return confirm('Do you really want to REFUND that transaction ?!');" href="<%= dbPages.getConfig("ProcessFolderURL") %>DebitRefund.asp?TransID=<%=PGData("lnc_TransactionFailID")%>">[Refund]</a>
						<a target="ifrHidden" onclick="return confirm('Do you really want to VOID that transaction ?!');" href="Log_ConnectionData_SetStatus.aspx?Status=4&ID=<%=PGData("LogNoConnection_id")%>">[Void]</a>
						<%
						Else
						%>
						<span style="color:Silver;text-decoration:underline;">[Refund]</span>
						<span style="color:Silver;text-decoration:underline;">[Void]</span>
						<%
					End If
						If PGData("lnc_AutoRefundStatus") = 2 Or PGData("lnc_AutoRefundStatus") = 4 Then
						%>
						<a target="ifrHidden" onclick="return confirm('Do you really want to RENEW that transaction ?!');" href="Log_ConnectionData_SetStatus.aspx?Status=0&ID=<%=PGData("LogNoConnection_id")%>">[Renew]</a>
						<%
						Else
						%>
						<span style="color:Silver;text-decoration:underline;">[Renew]</span>
						<%
					End If
					%>
				</td>
			</tr>
			<tr id="trInfo<%=i%>" style="display:none;">
				<td colspan="2"></td>
				<td colspan="13">
					<%
					If Len(PGData("lnc_HTTP_Error")) > 17 Then
						%>
						<div style="margin-top:8px;border-top:1px dashed silver;border-bottom:1px dashed silver;padding-top:8px;"><b>HTTP Error</b>: <%= PGData("lnc_HTTP_Error") %></div>
						<%
					End If
					%>
					<div id="divRefund<%=i%>"></div>
					<div id="divInfo<%=i%>"></div>
				</td>
			</tr>
			<tr>
				<td height="1" colspan="11" bgcolor="silver"></td>
				<td height="1" colspan="1"></td>
				<td height="1" colspan="2" bgcolor="silver"></td>
				<td height="1" colspan="1"></td>
				<td height="1" colspan="1" bgcolor="silver"></td>
			</tr>
			<%
		End while
		%>
		</table>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" alt="ADD TO FILTER" align="middle"/> ADD TO FILTER
		</div>
		<table align="center" style="width:95%">
		<tr>
			<td><UC:Paging runat="Server" id="PGData" PageID="PageID" ShowRecordCount="true" /></td>
			<td align="right">
				<asp:Button ID="btnExcel" runat="server" OnClick="ExportToExcel" Text="View Excel" style="font-size:11px;cursor:pointer;height:21px;" CssClass="bordered" />&nbsp;
				<input type="button" id="TotalsButton" style="font-size:11px;cursor:pointer;" onclick="with(document.getElementById('tblTotals')){style.display='block';if(onresize)onresize();};setAjaxHTML('<%="?Totals=1&" + Request.QueryString.ToString() %>', document.getElementById('tblTotals'), true);" value="SHOW TOTALS &Sigma;"/>
				<div id="tblTotals" onresize="PosObject(this, document.getElementById('TotalsButton'), 'RRTB', 0, -10);" style="padding:3px; border-color:#484848; border-style:solid; border-width:1px 3px; position:absolute;width:260px;background-color:White;display:none; font-size:11px; text-align:left;"> &nbsp; LOADING DATA... </div>
			</td>
		</tr>
		</table>
		<%
	Else
		Response.Write("<table class=""formNormal"" align=""center"" style=""width:95%"">")
		Response.Write("<tr><td style=""border:1px solid silver; padding:5px;"">No records found</td></tr></table>")
	End if
	PGData.CloseDataset()
	%>
	</form>
	<br />
	<center>
		<iframe id="ifrHidden" name="ifrHidden" src="common_blank.htm" frameborder="0" width="95%" height="50"></iframe>
	</center>
</body>
</html>