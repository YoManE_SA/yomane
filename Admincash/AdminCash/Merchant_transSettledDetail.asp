<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_adoPaging.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_controls.asp"-->
<!--#include file="../include/func_transPay.asp"-->
<!--#include file="../include/func_security.asp" -->
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<script language="JavaScript" src="../js/jquery-1.3.2.min.js"></script>
	<script language="JavaScript" src="../js/jquery.shiftcheckbox.js"></script>
	<script language="JavaScript" src="../js/func_common.js"></script>
	<style type="text/css">
		.BselectAll{
			width: 75px; height: 24px; text-align: center;
			background-color:#ffffff; font-size:11px; color:navy;
			padding-left: 3px; padding-right: 3px; cursor: hand;
		}
		.BtransReturn{
			background-image: url("../images/icon_switch.gif");
			background-repeat: no-repeat; background-position: 50px; 
			width: 75px; height: 24px; text-align: left;
			background-color:#ffffff; font-size:11px; color:navy;
			padding-left: 3px; padding-right: 3px; cursor: hand;
		}
		.Bsum{
			background-image: url("../images/icon_sum.gif");
			background-repeat: no-repeat; background-position: 90px; 
			width: 110px; height: 24px; text-align: left;
			background-color:#e9e9e9; font-size:11px;
			padding-left: 3px; padding-right: 3px; cursor: hand;
		}
	</style>
	<script language="JavaScript">
		var checkflag = "false";
		function check() {
			if (checkflag == "false") {
				if(frmPaymentsReturn.transIdRegular) { // obj exist
				if(frmPaymentsReturn.transIdRegular.length) { // means if(obj.length > 0)
					for (i = 0; i < frmPaymentsReturn.transIdRegular.length; i++) 
					frmPaymentsReturn.transIdRegular[i].checked = true;
				}
				else { // means not an array (!obj.length)
					frmPaymentsReturn.transIdRegular.checked = true
				}}
				if(frmPaymentsReturn.transIdPayments) {
				if(frmPaymentsReturn.transIdPayments.length) {
					for (i = 0; i < frmPaymentsReturn.transIdPayments.length; i++) 
					frmPaymentsReturn.transIdPayments[i].checked = true;
				}
				else {
					frmPaymentsReturn.transIdPayments.checked = true
				}}
				checkflag = "true";
				return "CLEAR CHECKS";
			}
			else
			{
				if(frmPaymentsReturn.transIdRegular) {
				if(frmPaymentsReturn.transIdRegular.length) {
					for (i = 0; i < frmPaymentsReturn.transIdRegular.length; i++) 
					frmPaymentsReturn.transIdRegular[i].checked = false;
				}
				else {
					frmPaymentsReturn.transIdRegular.checked = false
				}}
				if(frmPaymentsReturn.transIdPayments) {
				if(frmPaymentsReturn.transIdPayments.length) {
					for (i = 0; i < frmPaymentsReturn.transIdPayments.length; i++) 
					frmPaymentsReturn.transIdPayments[i].checked = false;
				}
				else {
					frmPaymentsReturn.transIdPayments.checked = false
				}}
				checkflag = "false";
				return "CHECK ALL";
			}
		}

		function CheckUnpaid() {
			var checkboxes = document.getElementsByTagName("INPUT");
			var chk;
			for (var i = 0; i < checkboxes.length; i++) {
				chk = checkboxes[i];
				if (((chk.name == "transIdPayments") || (chk.name == "transIdRegular")) && (chk.className.toUpperCase().indexOf("PAIDEPA") < 0)) chk.checked = true;
			}
		}

		function UncheckPaid() {
			var checkboxes = document.getElementsByTagName("INPUT");
			var chk;
			for (var i = 0; i < checkboxes.length; i++) {
				chk = checkboxes[i];
				if (((chk.name == "transIdPayments") || (chk.name == "transIdRegular")) && (chk.className.toUpperCase().indexOf("PAIDEPA") >= 0)) chk.checked = false;
			}
		}
		
		function CheckForm()
		{
			if(frmPaymentsReturn.transIdRegular) {
			if(frmPaymentsReturn.transIdRegular.length) {
				for (i = 0; i < frmPaymentsReturn.transIdRegular.length; i++) 
					if(frmPaymentsReturn.transIdRegular[i].checked == true) {
						return true;
					}
			}
			else {
				if(frmPaymentsReturn.transIdRegular.checked == true) {
					return true;
				}
			}}
			if(frmPaymentsReturn.transIdPayments) {
			if(frmPaymentsReturn.transIdPayments.length) {
				for (i = 0; i < frmPaymentsReturn.transIdPayments.length; i++) 
					if(frmPaymentsReturn.transIdPayments[i].checked == true) {
						return true;
					}
			}
			else {
				if(frmPaymentsReturn.transIdPayments.checked == true) {
					return true;
				}
			}}
		alert('Need to choose transactions to return');
		return false;
		}
		
		function toggleInfo(TransId,PaymentMethod,bkgColor)
		{	
			objRefA = document.getElementById('Row' + TransId + 'A')
			objRefB = document.getElementById('Row' + TransId + 'B')
			if (objRefB.innerHTML == '') {
				if (objRefA) { objRefA.style.display = 'none'; }
				switch (PaymentMethod) {
					case '15':
						UrlAddress = 'trans_detail_eCheck.asp';
						break;
					case '4':
						UrlAddress = 'trans_detail_RollingRes.asp';
						break;
					case '2':
						UrlAddress = 'Trans_DetailAdmin.asp';
						break;
					default:
						UrlAddress = 'trans_detail_cc.asp';
				}
				objRefB.innerHTML='<iframe framespacing="0" scrolling="auto" align="right" marginwidth="0" frameborder="0" width="100%" height="0px" src="'+UrlAddress+'?transID='+TransId+'&PaymentMethod='+PaymentMethod+'&bkgColor='+bkgColor+'&TransTableType=pass" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>';
			}
			else {
				if (objRefA) { objRefA.style.display = ''; }
				objRefB.innerHTML='';
			}
		}

		function showFrameTotals(fQuery) {
			objIFrame = document.getElementById('FrameTotals')
			objIFrame.src = '../include/common_totalTransShow.asp?LNG=1&' + fQuery + '&isIframe=1';
			objIFrame.style.margin = '20px 0px';
			objIFrame.style.display = '';
		}

		function showFrameTotals2(fQuery) {
			objIFrame = document.getElementById('FrameTotals')
			objIFrame.src = '../include/common_totalTransShowV2.aspx?LNG=1&' + fQuery + '&isIframe=1';
			objIFrame.style.margin = '20px 0px';
			objIFrame.style.display = '';
		}

		$(document).ready(
		function() {
			$('.shiftCheckbox').shiftcheckbox();
		});
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">
<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="1">
		<tr>
			<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
			<td width="60" bgcolor="#edfeed">Debit</td>
			<td width="10"></td>
			<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
			<td width="60" bgcolor="#edfeed" style="color:#cc0000;">Refund</td>
			<td width="10"></td>
			<td width="6" background="../images/green_strip_sqr.gif"></td><td width="1"></td>
			<td width="90" bgcolor="#edfeed">Refund Source</td>
			<td width="10"></td>
			<td width="6" bgcolor="#66cc66"></td><td width="1"></td>
			<td width="60" bgcolor="#fff3dd">Denied</td>
			<td width="10"></td>
			<td width="6" bgcolor="#66cc66">X</td><td width="1"></td>
			<td width="64" bgcolor="#ffffff" style="color:#6b6b6b;">Test Only</td>
			<td width="10"></td>
			<td width="6" bgcolor="#003399"></td><td width="1"></td>
			<td width="60" bgcolor="#eeeffd">Admin</td>
			<td width="10"></td>
			<td width="6" bgcolor="#484848"></td><td width="1"></td>
			<td width="60" bgcolor="#f5f5f5">System</td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<br />
<%
nPayID = dbText(request("payID"))
If Trim(nPayID) = "" Then Response.End
nCompanyID = dbText(request("companyID"))
bAllowTransReturn = False
session("PageSize") = 100

Set rsData = server.createobject("adodb.recordset")
GetConstArray rsData, GGROUP_WireStatus, 1, "", WireStatusText, Empty
GetDescConstArray rsData, GGROUP_DeniedStatus, 1, "", DeniedStatusDesc, VbNull

sSQL="SELECT tblCompany.CompanyName, tblTransactionPay.isBillingPrintOriginal, tblTransactionPay.payDate, tblTransactionPay.currency, " & _
	" tblTransactionPay.id, tblTransactionPay.InvoiceNumber, tblWireMoney.WireStatus, tblWireMoney.WireMoney_id, WireStatusDate " & _
	" FROM tblTransactionPay " & _
	" LEFT JOIN tblWireMoney ON (tblTransactionPay.id = tblWireMoney.WireSourceTbl_id AND tblWireMoney.WireType=1) " & _
	" LEFT JOIN tblCompany ON (tblTransactionPay.companyID = tblCompany.id) WHERE tblTransactionPay.id=" & nPayID
set rsData5 = oledbData.execute(sSQL)
if rsData5.EOF then
	response.write "Settlement was not found<br />"
	rsData5.close :	Set rsData5 = nothing
	response.end
End if
nCurrency = rsData5("currency")
nPaydate = rsData5("payDate")

If (rsData5("InvoiceNumber") = 0) AND (IsNull(rsData5("WireStatus")) OR rsData5("WireStatus")<=2) then
	bAllowTransReturn = true
End if

sQueryFilter = "&" & UrlWithout(Request.QueryString, "page")
%>
<table width="95%" border="0" cellspacing="2" cellpadding="1" align="center">
<tr>
	<td id="pageMainHeadingTd">
		<span id="pageMainHeading">SETTLEMENT NUM. <%= rsData5("id") %> (<%= GetCurText(nCurrency) %>) -</span> <span class="txt13"><%= uCase(dbtextShow(rsData5("CompanyName"))) %></span>
	</td>
	<td align="right" class="txt13" valign="top">
		<a class="faq" href="Merchant_transSettledListData.asp?Source=filter&ShowCompanyID=<%= nCompanyID %>">Back To Settlement List</a><br />
		<% If IsDate(dDateTo) OR IsDate(dDateFrom) then response.write "Transactions from " & dDateFrom & " To " & dDateTo %><br />
	</td>			
</tr>
</table>
<br />
<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%" style="border:1px solid silver;">
<tr>
	<td>Settlement: <span class="txt11"><%=nPayID%></span></td>
	<td>
		Invoice:
		<%
		If int(rsData5("InvoiceNumber"))>0 then
			response.write "Num. " & rsData5("InvoiceNumber") & ""
		Else
			response.write "None"
		end if
		%>
	</td>
	<td rowspan="2" nowrap align="right">
		<%if bAllowTransReturn Then %>
		<input type="button" onclick="if(confirm('This actions is not reversable!\r\nAre you sure?')) document.location='merchant_payCreate.asp?companyID=<%=nCompanyID%>&currency=<%=nCurrency%>&PayID=<%=nPayID%>&Type=Cancel'" style="width:120px; height:35px; font-size:7pt;" value="DELETE&#10;SETTLEMENT" />
		<%End if%>
	</td>
</tr>
<tr>
	<td>Date: <%=FormatDatesTimes(rsData5("payDate"),1,1,0)%></td>
	<td>
		<%
		response.write "Wire Transfer: "
		If IsNull(rsData5("WireStatus")) then
			response.write "None"
		Elseif trim(rsData5("WireStatus"))="0" Then
			response.write rsData5("WireMoney_id") & " (Pending)"
		Elseif rsData5("WireStatus") <= 6 And rsData5("WireStatus") >=1 then
			Response.Write(rsData5("WireMoney_id") & "-")
			Response.Write(WireStatusText(rsData5("WireStatus")) & " &nbsp")
			Response.Write(FormatDatesTimes(rsData5("WireStatusDate"),1,0,0))
		End if
		%>
	</td>
</tr>
</table>
<br /><br />
<%
sSQL="SELECT List.TransSource.Name TransTypeName, pm.name as pm_Name, List.TransCreditType.Name CreditName," & _
" tblCreditCard.CCTypeID, tblCreditCard.BINCountry, tblDebitTerminals.dt_name, tblCompanyTransPass.*, " &_
" IsNull(tblLogImportEPA.IsPaid, 0) IsPaidEPA " & _
" FROM tblCompanyTransPass " & _
" LEFT JOIN List.PaymentMethod AS pm ON tblCompanyTransPass.PaymentMethod= pm.PaymentMethod_id" & _
" INNER JOIN List.TransCreditType ON tblCompanyTransPass.CreditType=List.TransCreditType.TransCreditType_id" & _
" LEFT JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID" & _
" LEFT JOIN List.TransSource ON tblCompanyTransPass.TransactionTypeID=List.TransSource.TransSource_id" & _
" LEFT JOIN tblDebitTerminals ON tblCompanyTransPass.terminalNumber=tblDebitTerminals.terminalNumber" & _
" LEFT JOIN tblLogImportEPA ON tblCompanyTransPass.ID=tblLogImportEPA.TransID AND 1=tblLogImportEPA.Installment" & _
" WHERE (tblCompanyTransPass.PayID LIKE ('%;" & nPayID & ";%')) AND tblCompanyTransPass.CompanyID=" & nCompanyID & _
" ORDER BY tblCompanyTransPass.InsertDate DESC"
call openRsDataPaging(session("PageSize"))

If NOT rsData.EOF Then
	Set fsServer=Server.CreateObject("Scripting.FileSystemObject")
	%>
	<form action="merchant_payCancel.asp" method="post" name="frmPaymentsReturn">
	<table class="formNormal" width="95%" align="center" cellpadding="1" cellspacing="0">
	<tr>
		<th colspan="2"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></th>
		<th valign="bottom"><% If bAllowTransReturn Then Response.Write("Rtn / ") %> ID</th>
		<th>Date<br /></th>
		<th>Payment Method<br /></th>
		<th>Credit Type<br /></th>
		<th>Ins.<br /></th>
		<th>Amount<br /></th>
		<th>Source<br /></th>
		<th width="60" style="text-align:center;">Debit<br /></th>
		<th>Terminal<br /></th>
		<th width="60" style="text-align:center;">Info<br /></th>
	</tr>
	<tr><td height="3"></td></tr>
	<%
	nCount = 0
	rsData.MoveFirst
	Do until rsData.EOF
	
		sTrStyle = ""
		nCount = nCount + 1
		bShowCheckbox = True
		'bIsFirstPayment = false
 
		nPercents = rsData("Interest") ' ���� ����
		if nPercents>0 then nPercents = nPercents+nPrimePercent '	�� ���� ����� ���� � 0 ������ �����
		
		'If int(rsData("CreditType")) = 8 Then
		'	sSQL="SELECT insID FROM tblCompanyTransInstallments WHERE PayID='" & nPayID & "' AND transAnsID=" & rsData("id") & " ORDER BY id"
		'	Set rsDataPayments = oledbData.execute(sSQL)
		'	If not rsDataPayments.EOF Then
		'		If rsDataPayments("insID") = 1 Then bIsFirstPayment = True
		'	End if
		'End if
		
		'Get dispaly Param
		Call Fn_TransParam(rsData, sTrBkgColor, sTdBkgColorFirst, sTdFontColor, sPayStatusText, bShowDeniedOptions, bAllowDelete)
	    If rsData("deniedstatus") = DNS_DupFoundValid Or rsData("deniedstatus") = DNS_DupFoundUnValid Then _
	        sTrStyle = "background-image:url('\Images\img\bkg_slash.gif');"
		Call Fn_chkWasRefund(rsData("ID"))
		
		'If rsData("PaymentMethod") = PMD_RolRes Then bShowCheckbox = True
		
		sShowBorder = False
		If int(request("transID")) = int(rsData("ID")) Then sShowBorder = True

		If nCount<>1 Then
			Response.Write("<tr><td height=""1"" colspan=""12""	bgcolor=""#ffffff""></td></tr>")
			Response.Write("<tr><td height=""1"" colspan=""12""	bgcolor=""silver""></td></tr>")
			Response.Write("<tr><td height=""1"" colspan=""12""	bgcolor=""#ffffff""></td></tr>")
		End If
		If bShowBorder Then Response.Write("<tr><td height=""2"" colspan=""12""	bgcolor=""#000000""></td></tr>")
		%>
		<tr bgcolor="<%= sTrBkgColor %>" style="<%=sTrStyle%>">
			<%
			If trim(sTdBkgColorFirst)="green_strip_sqr" Then
				Response.Write("<td width=""4"" background=""../images/green_strip_sqr.gif""></td>")
			Else
				sTdSymbolFirst = ""
				If rsData("isTestOnly") Then sTdSymbolFirst = "X"
				Response.Write("<td class=""txt12b"" width=""4"" bgcolor=""" & sTdBkgColorFirst & """>" & sTdSymbolFirst & "</td>")
			End If
			%>
			<td width="3"></td>
			<td class="txt10" align="right" nowrap style="color:<%= sTdFontColor %>;">
				<%
				If bAllowTransReturn Then
					'If bShowCheckbox AND rsData("deniedstatus")=0 Then
						sCssClassPaid=IIf(rsData("PaymentMethod") <= PMD_MAXINTERNAL Or rsData("IsPaidEPA"), " paidEPA", "")
						If int(rsData("CreditType")) = 8 Then
							response.write "<input type=""Checkbox"" name=""transIdPayments"" value=""" & rsData("ID") & """ style=""background-color:" & sTrBkgColor & ";"" class=""shiftCheckbox" & sCssClassPaid & """> &nbsp;"
						Else
							response.write "<input type=""Checkbox"" name=""transIdRegular"" value=""" & rsData("ID") & """ style=""background-color:" & sTrBkgColor & ";"" class=""shiftCheckbox" & sCssClassPaid & """> &nbsp;"
						End if
					'Else
						'response.write "<input type=""Checkbox"" name="""" value="""" disabled=""disabled"" style=""background-color:" & sTrBkgColor & ";""> &nbsp;"
					'End If
				End If
				Response.Write(rsData("id"))
				%>
			</td>
			<td class="txt10" align="right" nowrap style="color:<%= sTdFontColor %>;"><%=FormatDatesTimes(rsData("InsertDate"),1,1,0) %><br /></td>
			<td style="color:<%= sTdFontColor %>;">
				<%
				If rsData("PaymentMethod")>=2 Then
					Response.Write("<img src=""/NPCommon/ImgPaymentMethod/23X12/" & rsData("PaymentMethod") & ".gif"" alt=""" & rsData("pm_Name") & """ align=""middle"" border=""0"" /> &nbsp;")
					If Trim("" & rsData("BinCountry")) <> "" Then
						Response.Write("<img src=""/NPCommon/ImgCountry/18X12/" & rsData("BinCountry") & ".gif"" align=""middle"" border=""0"" />")
						Response.Write(" &nbsp;" & rsData("BinCountry"))
					End If
				End If
				Response.Write(" " & Trim("" & rsData("PaymentMethodDisplay")))
				%>
			</td>
			<td class="txt11" align="right" style="color:<%= sTdFontColor %>;"><%= rsData("CreditName") %><br /></td>
			<td class="txt11" align="right" style="color:<%= sTdFontColor %>;"><%= rsData("Payments") %><br /></td>
			<td class="txt11" align="right" nowrap dir="ltr" style="color:<%= sTdFontColor %>;">
				<%
				If (rsData("PaymentMethod") = PMD_ManualFee Or rsData("PaymentMethod") = PMD_SystemFees) And rsData("netpayFee_transactionCharge") <> 0 Then _
					Response.Write(FormatCurrWCT(nCurrency, rsData("netpayFee_transactionCharge"), 0)) _
				Else Response.Write(FormatCurrWCT(nCurrency, rsData("Amount"), rsData("CreditType")))
				%>
			</td>
			<td class="txt11" align="right" style="color:<%= sTdFontColor %>;"><%= rsData("TransTypeName") %><br /></td>
			<td style="text-align:center;">
				<%
				If rsData("PaymentMethod")>14 AND trim(rsData("DebitCompanyID"))<>"" Then
					If fsServer.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & trim(rsData("DebitCompanyID")) & ".gif")) Then
						Response.write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(rsData("DebitCompanyID")) & ".gif"" align=""middle"" border=""0"" />")
					End if
				End if
				%>
			</td>
			<td>
				<%
				If rsData("PaymentMethod")>14 AND Trim(rsData("dt_name"))<>"" Then
					If int(len(dbtextShow(rsData("dt_name"))))<16 Then
						response.write dbTextShow(rsData("dt_name")) & "<br />"
					Else
						response.write "<span title="""& dbtextShow(rsData("dt_name")) & """>" & left(dbtextShow(rsData("dt_name")),15) & "..</span><br />"
					End If
				End if
				%>
			</td>
			<td style="text-align:center;">
				<%
				If rsData("PaymentMethod")>14 Or rsData("PaymentMethod") = 4 Or rsData("PaymentMethod") = 2 Then
					%>
					<a onclick="toggleInfo('<%= rsData("ID") %>','<%= rsData("PaymentMethod") %>','<%= replace(sTrBkgColor,"#","") %>')" name="<%= rsData("ID") %>" class="go" style="cursor:pointer; font-size:11px;"><img src="../images/b_showWhiteEng.gif" align="middle" /></a><br />
					<%
				End If
				%>
			</td>
		</tr>
		<%
		If bShowBorder Then
			Response.Write("<tr><td height=""2"" colspan=""12""	bgcolor=""#000000""></td></tr>")
		End If
		If int(rsData("originalTransID"))>0 Then
			sSQL="SELECT InsertDate, PayID, Amount, CreditType FROM tblCompanyTransPass WHERE ID=" & rsData("originalTransID")
			set rsDataTmp = oledbData.execute(sSQL)
			If Not rsDataTmp.EOF Then
				sCreditTransType = "partial"
				If rsDataTmp("Amount")=rsData("Amount") Then sCreditTransType="Full"
				%>
				<tr>
					<td colspan="2" bgcolor="<%= sTrBkgColor %>"></td>
					<td colspan="11" bgcolor="<%= sTrBkgColor %>">
						<%= sCreditTransType %> refund of transaction <span class="txt10"><%= rsData("originalTransID") %></span>
						from <%= FormatDatesTimes(rsDataTmp("InsertDate"),1,0,0) %> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Original transaction is <%= sPayStatusText %><br />
					</td>
				</tr>
				<%
			End if
			rsDataTmp.close
			Set rsDataTmp = nothing
		End if
		If (rsData("PaymentMethod")=PMD_Admin OR rsData("PaymentMethod")=PMD_SystemFees) AND Trim(rsData("Comment"))<>"" Then
			%>
			<tr>
				<td colspan="3" bgcolor="<%= sTrBkgColor %>"></td>
				<td colspan="10" bgcolor="<%= sTrBkgColor %>" style="color:<%= sTdFontColor %>;"><%= dbtextShow("Comment: " & rsData("Comment")) %></td>
			</tr>
			<%
		End if
		If rsData("deniedstatus")>0 AND rsData("deniedstatus")<DNS_WasWorkedOut Then
			%>
			<tr>
				<td colspan="3" bgcolor="<%= sTrBkgColor %>"></td>
				<td colspan="10"	bgcolor="<%= sTrBkgColor %>" style="color:<%= sTdFontColor %>;"><%=DeniedStatusDesc(rsData("deniedstatus"))%></td>
			</tr>
			<%
		End If
		If int(rsData("CreditType"))=8 Then 
			If rsData("deniedstatus") <> DNS_SetFoundUnValid Then
				sSQL="SELECT Amount,comment FROM tblCompanyTransInstallments WHERE PayID='" & nPayID & "' AND transAnsID=" & rsData("id") & " ORDER BY id"
				Set rsDataPayments = oledbData.execute(sSQL)
				If not rsDataPayments.EOF Then
					Do until rsDataPayments.EOF
						%>
						<tr>
							<td colspan="3" bgcolor="<%= sTrBkgColor %>"></td>
							<td colspan="10"	bgcolor="<%= sTrBkgColor %>" style="color:<%= sTdFontColor %>;">
								Installment <%= rsDataPayments("comment") %> &nbsp;&nbsp;&nbsp;
								Amount <%=FormatCurr(nCurrency, rsDataPayments("Amount"))%><br />
							</td>
						</tr>
						<%
					rsDataPayments.movenext
					loop
				End if
			End if
			rsDataPayments.close
			set rsDataPayments = nothing
		End if
		response.write "<tr><td colspan=""12"" id=""Row" & rsData("ID") & "B""></td></tr>"
		
	rsData.movenext
	Loop
	%>
	<tr><td height="1" colspan="12"	bgcolor="#ffffff"></td></tr>
	<tr><td height="1" colspan="12"	bgcolor="silver"></td></tr>
	<tr><td height="1" colspan="12"	bgcolor="#ffffff"></td></tr>
	</table>
	
	<iframe id="FrameTotals" align="center" frameborder="0" style="display:none; border:1px solid gray;" width="95%" height="0" scrolling="Auto" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>
	<!--<iframe align="center" frameborder="0" style="border:1px solid gray;" width="95%" height="300" scrolling="Auto" src="../include/common_totalTransShow.asp?CompanyID=<%= nCompanyID %>&PayID=<%= nPayID %>&Currency=<%=nCurrency%>&LNG=1&isIframe=1" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>-->

	<br />
	<table width="95%" align="center" border="0" cellpadding="1" cellspacing="0">
	<tr>
		<td class="txt14" valign="middle">
			<%
			if bAllowTransReturn then
				%>
				<input type="hidden" name="IncRR" value="1" /> 
				<input type="hidden" name="RetRR" value="1" /> 
				<input type="button" class="BselectAll" value="CHECK ALL" onClick="this.value=check();"> &nbsp;
				<input class="BselectAll" type="button" value="Check Unpaid" onclick="CheckUnpaid();" /> &nbsp;
				<input class="BselectAll" type="button" value="Uncheck Paid" onclick="UncheckPaid();" /> &nbsp;
				<input type="button" class="BtransReturn" value="RETURN" onclick="if(CheckForm()){document.frmPaymentsReturn.submit();}else{return false;};"> &nbsp;				
				<%
			end if
			%>
			<input type="button" class="Bsum" value="SHOW TOTALS" onclick="showFrameTotals('CompanyID=<%= nCompanyID %>&payID=<%= nPayID %>&Currency=<%= nCurrency %>&isNotCalcAll=<%= sIsNotCalcAll %>'); /*this.style.display='none';*/">
			<% If PageSecurity.IsMemberOf("Dev. Support") Then %>
				<input type="button" class="Bsum" value="SHOW TOTALS 2" onclick="showFrameTotals2('CompanyID=<%= nCompanyID %>&payID=<%= nPayID %>&Currency=<%= nCurrency %>&isNotCalcAll=<%= sIsNotCalcAll %>'); /*this.style.display='none';*/">	
			<% End If %>
			<!--<input type="button" class="Bsum" value="SHOW TOTALS" onclick="OpenPop('../include/common_totalTransShow.asp?LNG=1&CompanyID=<%= nCompanyID %>&payID=<%= nPayID %>&Currency=<%= nCurrency %>&<%= sQueryFilter %>&isNotCalcAll=<%= sIsNotCalcAll %>&isIframe=0','fraOrderPrint1',750,310,0);">-->
		</td>
		<td align="right">
			<table align="right" border="0" cellpadding="1" cellspacing="0"> 
			<tr><td><% call showPaging("Merchant_transSettledDetail.asp", "Eng", sQueryFilter) %></td></tr>
			</table>
		</td>
	</tr>
	</table>
	<input type="Hidden" value="<%= nCompanyID %>" name="companyID">
	<input type="Hidden" value="<%= nPayID %>" name="PayID">
	<input type="Hidden" value="<%= nCurrency %>" name="Currency">
	</form>
	<%
	Set fsServer=Nothing
Else
	%>
	<table width="95%" border="0" cellspacing="2" cellpadding="2" align="center">
	<tr>
		<td align="left" class="txt13">
			<br />Did not find any transactions<br />
		</td>
	</tr>
	</table>
	<%
End if
rsData.close
Set rsData = nothing
rsData5.close
Set rsData5 = nothing
call closeConnection()
%>
</body>
</html>
