﻿<%@ Control Language="VB" ClassName="Password" EnableViewState="true" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
    Public Property RefID() As Integer
        Get
            Return ViewState("RefID")
        End Get
        Set(ByVal value As Integer)
            ViewState("RefID") = value
        End Set
    End Property

    Public Property RefType() As UserType
        Get
            Return CType(ViewState("RefType"), UserType)
        End Get
        Set(ByVal value As UserType)
            ViewState("RefType") = CType(value, Integer)
        End Set
    End Property

    Protected Overrides Sub OnPreRender(e As EventArgs)
        hlChangePassword.NavigateUrl = String.Format("~/admincash/ChangePassword.aspx?RefType={0}&RefID={1}", CInt(RefType), RefID)
        MyBase.OnPreRender(e)
    End Sub

    'Sub ChangePassword(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
    '	lblPassResult.ForeColor = Drawing.Color.DarkRed
    '	If NPControls.IsEmptyField(lblPassResult, lblPassResult, "Password") Then Exit Sub
    '	If NPControls.IsMismatch(txtPassword, txtPassword2, lblPassword, "passwords") Then Exit Sub
    '	Dim sSQL As String
    '	sSQL = "DECLARE @nRC int; EXEC @nRC=SetPassword " & RefType & ", " & RefID & ", '" & Request.UserHostAddress & "', '" & txtPassword.Text & "';SELECT -@nRC;"
    '	Dim gdvResult As GlobalDataValue = GlobalData.Value(GlobalDataGroup.PASSWORD_CHANGE_ATTEMPT_RESULT, dbPages.ExecScalar(sSQL))
    '	lblPassResult.Text = gdvResult.Text
    '	lblPassResult.ForeColor = Drawing.ColorTranslator.FromHtml(gdvResult.Color)
    'End Sub

    Private Function GetLogin() As Netpay.Infrastructure.Security.Login
        Dim login As Netpay.Infrastructure.Security.Login = Nothing
        Dim accObj As Netpay.Bll.Accounts.Account = Nothing
        Select Case RefType
            Case UserType.Customer
                accObj = Netpay.Bll.Customers.Customer.Load(RefID)
                If accObj.Login.LoginID = 0 Then CType(accObj, Netpay.Bll.Customers.Customer).Save()
            Case UserType.Merchant
                accObj = Netpay.Bll.Merchants.Merchant.Load(RefID)
                If accObj.Login.LoginID = 0 Then CType(accObj, Netpay.Bll.Merchants.Merchant).Save()
            Case UserType.Affiliate
                accObj = Netpay.Bll.Affiliates.Affiliate.Load(RefID)
                If accObj.Login.LoginID = 0 Then CType(accObj, Netpay.Bll.Affiliates.Affiliate).Save()
            Case UserType.SystemAdmin
            Case UserType.SystemUser
                Dim user = Netpay.Infrastructure.Security.AdminUser.Load(RefID)
                If user.Login Is Nothing Or user.Login.LoginID = 0 Then CType(user, Netpay.Infrastructure.Security.AdminUser).Save()
                login = user.Login
        End Select
        If accObj IsNot Nothing Then login = accObj.Login
        Return login
    End Function

    Protected Sub Reset_Click(ByVal o As Object, ByVal e As EventArgs)
        Netpay.Infrastructure.Security.Login.ResetLock(GetLogin().LoginID, Request.UserHostAddress)
        Dim nRC = True
        lblPassResult.Text = "<br/>Password reset " & IIf(nRC = 0, "failed (RC=" & nRC & ").", "succeeded.")
        lblPassResult.Visible = True
    End Sub

    Protected Sub Show_Click(ByVal o As Object, ByVal e As EventArgs)
        lblPassResult.Text = "<br/>Password: " & Netpay.Infrastructure.Security.Login.GetPassword(GetLogin().LoginID, Request.UserHostAddress)
        lblPassResult.Visible = True
    End Sub
</script>

<asp:HyperLink runat="server" ID="hlChangePassword" Text="Change Password" /> | 
<asp:LinkButton runat="server" OnClick="Reset_Click" Text="Reset Counter" /> | 
<asp:LinkButton runat="server" OnClick="Show_Click" Text="Show Password" />
<asp:Label runat="server" ID="lblPassResult" Visible="false" ViewStateMode="Disabled" />


