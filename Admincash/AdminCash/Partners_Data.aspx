<%@ Page Language="VB" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<script runat="server">
    Dim sSQL, StyleBkgColor As String
    Dim req_ID As Integer
    Dim sFileName, sSmallFileName, sRefCode As String

    Sub Page_Load()
        Security.CheckPermission(lblPermissions)
        req_ID = Request("id")
        If Not IsPostBack Then
            'If (Not IsNumeric("" & req_ID) Or Not IsNumeric("" & req_CompaniesCount)) And Not IsPostBack Then Response.Redirect("common_blank.htm", True)
            sSQL = "SELECT * FROM tblAffiliates WHERE Affiliates_id=" & req_ID
            Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
            If Not iReader.Read Then
                iReader.Close()
                Response.Redirect("common_blank.htm", True)
            End If
            hidAffID.Value = req_ID
            ltAffiliateName.Text = iReader("name")
            sFileName = IIf(IsDBNull(iReader("HeaderImageFileName")), "", iReader("HeaderImageFileName"))
            hidImageFileName.Value = sFileName
            sSmallFileName = IIf(IsDBNull(iReader("HeaderSmallImageFileName")), "", iReader("HeaderSmallImageFileName"))
            hidSmallImageFileName.Value = sSmallFileName
            txtTitle.Text = dbPages.DBTextShow("" & iReader("name"))
            sRefCode = dbPages.DBTextShow("" & iReader("AFLinkRefID"))
            txtLinkRefID.Text = sRefCode
            txtLegalName.Text = "" & iReader("LegalName")
            txtLegalNumber.Text = "" & iReader("LegalNumber")
            txtIDnumber.Text = "" & iReader("IDnumber")
            txtPaymentPayeeName.Text = "" & iReader("PaymentPayeeName")
            txtUserName.Text = "" & iReader("OutboundUsername")
            txtPassword.Text = "" & iReader("OutboundPassword")

            If iReader("IsActive") Then chkIsActive.Checked = True
            txtRemark.Text = dbPages.DBTextShow("" & iReader("comments"))
            If iReader("ShowSystemPay") Then chkShowSystemPay.Checked = True
            If iReader("ShowRemoteCharge") Then chkShowRemoteCharge.Checked = True
            If iReader("ShowCustomerPurchase") Then chkShowCustomerPurchase.Checked = True
            If iReader("ShowPublicPay") Then chkShowPublicPay.Checked = True
            If iReader("ShowDataUpdate") Then chkShowDataUpdate.Checked = True
            If iReader("ShowFees") Then chkShowFees.Checked = True
            If iReader("ShowDownloads") Then chkShowDownloads.Checked = True
            If iReader("ShowPayments") Then chkShowPayments.Checked = True
            If iReader("AFShowMerchants") Then chkAFShowMerchants.Checked = True
            If iReader("AFShowStats") Then chkAFShowStats.Checked = True
            If iReader("AFTransactionApplication") Then chkAFTransactionApplication.Checked = True
            If iReader("AFSettlements") Then chkAFSettlements.Checked = True
            dbPages.LoadListBox(ddPaymentCurrency, "Select CUR_ISOName, CUR_ID From tblSystemCurrencies", IIF(iReader("PaymentReceiveCurrency") Is DBNull.Value, 0, iReader("PaymentReceiveCurrency")))
            'If sRefCode.ToString = "" Then
            '    litRefLink.Text = "<br /><--- Enter code to enable a refrence link for signup"
            'Else
            '    litRefLink.Text = "Homepage: <a class=""strong"" href=""https://www.netpay-intl.com/?RefCode=" & sRefCode & """ target=""_blank"">https://www.netpay-intl.com/?RefCode=" & sRefCode & "</a>"
            '    litRefLink.Text &= "<br />Signup page: <a class=""strong"" href=""https://www.netpay-intl.com/Website/SiteSignup.aspx?RefCode=" & sRefCode & """ target=""_blank"">https://www.netpay-intl.com/Website/SiteSignup.aspx?RefCode=" & sRefCode & "</a>"
            '    litRefLink.Text &= "<br />Campaign page: <a class=""strong"" href=""https://www.netpay-intl.com/campaigns/MerchantProcessing.aspx?RefCode=" & sRefCode & """ target=""_blank"">https://www.netpay-intl.com/campaigns/MerchantProcessing.aspx?RefCode=" & sRefCode & "</a>"
            'End If
            iReader.Close()

            Dim chlAllowPages As CheckBox() = {CapturedTransactions, DeclinedTransactions, Payments, Reports, Customers }
            Dim affliate = Netpay.Bll.Affiliates.Affiliate.Load(req_ID)
            If affliate IsNot Nothing Then
                If affliate.LoginID Is Nothing Then
                    affliate.Login.UserName = ltAffiliateName.Text
                    affliate.Login.EmailAddress = ltAffiliateName.Text
                    affliate.Save()
                End If
                Dim values = Netpay.Infrastructure.Security.Permission.LoadForLogin(affliate.LoginID.GetValueOrDefault())
                For Each l As CheckBox In chlAllowPages
                    Dim permission = values.Where(Function(v) v.ObjectID = Netpay.Infrastructure.Security.SecuredObject.Get("PCP.Security", (l.ID & ".aspx").ToLower()).ID).SingleOrDefault()
                    If permission Is Nothing Then l.Checked = False _
                    Else l.Checked = (permission.Value = Netpay.Infrastructure.Security.PermissionValue.Execute)
                Next
            End If
            tlbTop.LoadPartnerTabs(req_ID)
            hlOuterLogin.NavigateUrl = "outer_login.aspx?AffiliateID=" & req_ID
        End If
    End Sub

    Sub btnDeleteFile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not String.IsNullOrEmpty(hidImageFileName.Value) Then
            dbPages.ExecSql("Update tblAffiliates Set HeaderImageFileName = '' Where Affiliates_id=" & hidAffID.Value)
            Dim sFilePAth As String = Domain.Current.MapPublicDataPath(hidImageFileName.Value)
            If My.Computer.FileSystem.FileExists(sFilePAth) Then
                Try
                    My.Computer.FileSystem.DeleteFile(sFilePAth)
                Catch ex As Exception
                End Try
            End If
        End If
        fuDocument.Visible = True
        btnUpload.Visible = True
    End Sub

    Sub UploadFile(ByVal o As Object, ByVal e As EventArgs)
        Dim sExt = System.IO.Path.GetExtension(fuDocument.FileName).ToLower()
        If ".gif;.jpg;.jpe;.jpeg;.png".Contains(sExt) Then
            Dim fileName = "CP_Logo" & sExt
            Dim aff = Affiliates.Affiliate.Load(hidAffID.Value)
            If Not System.IO.Directory.Exists(aff.MapPublicPath(Nothing)) Then System.IO.Directory.CreateDirectory(aff.MapPublicPath(Nothing))
            fuDocument.SaveAs(aff.MapPublicPath(fileName))
            sFileName = aff.MapPublicOffset(fileName)
            Dim sSql = "Update tblAffiliates Set HeaderImageFileName = '" & sFileName & "' Where Affiliates_id=" & hidAffID.Value
            dbPages.ExecSql(sSql)
            fuDocument.Visible = False
            btnUpload.Visible = False
            hidImageFileName.Value = sFileName
        End If
    End Sub

    Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not cfvLinkRefId.IsValid Then
            Exit Sub
        End If
        Dim nIsActive As Short = 0, nShowSystemPay As Short = 0, nShowRemoteCharge As Short = 0, _
        nShowCustomerPurchase As Short = 0, nShowPublicPay As Short = 0, nShowDataUpdate As Short = 0, nShowFees As Short = 0, nShowDownloads As Short = 0, _
        nShowPayments As Short = 0, nAFShowMerchants As Short = 0, nAFShowStats As Short = 0, nAFTransactionApplication As Short = 0, nAFSettlements As Short = 0
        Dim sSQL As String

        If chkIsActive.Checked Then nIsActive = 1
        If chkShowSystemPay.Checked Then nShowSystemPay = 1
        If chkShowRemoteCharge.Checked Then nShowRemoteCharge = 1
        If chkShowCustomerPurchase.Checked Then nShowCustomerPurchase = 1
        If chkShowPublicPay.Checked Then nShowPublicPay = 1
        If chkShowDataUpdate.Checked Then nShowDataUpdate = 1
        If chkShowFees.Checked Then nShowFees = 1
        If chkShowDownloads.Checked Then nShowDownloads = 1
        If chkShowPayments.Checked Then nShowPayments = 1

        'If chkAFShowMerchants.Checked Then nAFShowMerchants = 1
        'If chkAFShowStats.Checked Then nAFShowStats = 1
        'If chkAFTransactionApplication.Checked Then nAFTransactionApplication = 1
        'If chkAFSettlements.Checked Then nAFSettlements = 1

        sSQL = "UPDATE tblAffiliates SET " & _
          "Name='" & dbPages.DBText(txtTitle.Text) & "'," & _
          "IsActive=" & nIsActive & "," & _
          "comments='" & dbPages.DBText(txtRemark.Text) & "'," & _
          "ShowSystemPay=" & nShowSystemPay & "," & _
          "ShowRemoteCharge=" & nShowRemoteCharge & "," & _
          "ShowCustomerPurchase=" & nShowCustomerPurchase & "," & _
          "ShowPublicPay=" & nShowPublicPay & "," & _
          "ShowDataUpdate=" & nShowDataUpdate & "," & _
          "ShowFees=" & nShowFees & "," & _
          "ShowDownloads=" & nShowDownloads & "," & _
          "ShowPayments=" & nShowPayments & "," & _
          "AFShowMerchants=" & nAFShowMerchants & "," & _
          "AFShowStats=" & nAFShowStats & "," & _
          "AFTransactionApplication=" & nAFTransactionApplication & "," & _
          "AFSettlements=" & nAFSettlements & "," & _
          "AFLinkRefID='" & dbPages.DBText(txtLinkRefID.Text) & "'," & _
          "PaymentReceiveCurrency=" & dbPages.TestVar(ddPaymentCurrency.SelectedValue, 0, -1, 0) & "," & _
          "LegalName='" & dbPages.DBText(txtLegalName.Text) & "'," & _
          "LegalNumber='" & dbPages.DBText(txtLegalNumber.Text) & "'," & _
          "IDnumber='" & dbPages.DBText(txtIDnumber.Text) & "'," & _
          "OutboundUsername='" & txtUserName.Text & "'," & _
          "OutboundPassword='" & txtPassword.Text & "'," & _
          "PaymentPayeeName='" & dbPages.DBText(txtPaymentPayeeName.Text) & "'" & _
          " WHERE Affiliates_id=" & hidAffID.Value
        dbPages.ExecSql(sSQL)

        Dim affliate = Netpay.Bll.Affiliates.Affiliate.Load(hidAffID.Value)
        Dim values = New Dictionary(Of Integer, Netpay.Infrastructure.Security.PermissionValue?)
        Dim chlAllowPages As CheckBox() = { CapturedTransactions, DeclinedTransactions, Payments, Reports, Customers }
        For Each t As CheckBox In chlAllowPages
            Dim valueName As String = (t.ID & ".aspx").ToLower()
            If Netpay.Infrastructure.Security.SecuredObject.Get("PCP.Security", valueName) Is Nothing Then _
                Netpay.Infrastructure.Security.SecuredObject.Create(Netpay.Infrastructure.Module.Get("PCP.Security"), valueName, Netpay.Infrastructure.Security.PermissionGroup.Execute)
            values.Add(Netpay.Infrastructure.Security.SecuredObject.Get("PCP.Security", valueName).ID, IIf(t.Checked, Netpay.Infrastructure.Security.PermissionValue.Execute, Nothing))
            'dbPages.ExecSql("Insert Into tblUserPermissions(UserTypeID, UserID, PermissionTypeID, PermissionValue)Values(" & UserType.Affiliate & ", " & hidAffID.Value & ", " & PermissionObjectType.AffiliateControlPanelWebPage & ", '" & t.ID & ".aspx')")
        Next
        Netpay.Infrastructure.Security.Permission.SetToLogin(affliate.LoginID.GetValueOrDefault(), values)

        Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
        Response.Write("parent.fraMenu.location='Partners_List.aspx';")
        Response.Write("document.location.href='Partners_data.aspx?id=" & hidAffID.Value & "&CountCompNum=" & Request("CountCompNum") & "';")
        Response.Write("</s" & "cript>")
    End Sub

    Sub CheckRefCode(ByVal source As Object, ByVal args As ServerValidateEventArgs)
        If Len(args.Value.Trim) = 0 Then
            args.IsValid = True
        Else
            If dbPages.ExecScalar("Select Count(*) From tblAffiliates Where Lower(AFLinkRefID)='" & args.Value.Trim.ToLower & "' And Affiliates_id <>" & hidAffID.Value) > 0 Then
                args.IsValid = False
            Else
                args.IsValid = True
            End If
        End If
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Partners</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body>

<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
<tr>
	<td>
		<asp:label ID="lblPermissions" runat="server" />
		<span id="pageMainHeading">Partners Management</span> - <asp:Literal runat="server" ID="ltAffiliateName" /><br /><br />
	</td>
</tr>
<tr>
	<td>
		<custom:Toolbar ID="tlbTop" Width="100%" runat="server" />
		<form id="frmServer" runat="server">
			<asp:HiddenField ID="hidAffID" runat="server" />
			<asp:HiddenField ID="hidImageFileName" runat="server" />
			<asp:HiddenField ID="hidSmallImageFileName" runat="server" />
			<table class="formNormal" align="left" border="0">
			<tr>
				<td colspan="4" class="MerchantSubHead">General Data<br /><br /></td>
			</tr>
			<tr>
				<td colspan="2">
					<asp:CheckBox ID="chkIsActive" runat="server" CssClass="option" /> Account is active
				</td>
			</tr>	
			<tr><td height="20" colspan="2"></td></tr>
			<tr>
	            <td>
		            <asp:HyperLink ID="hlOuterLogin" Text="PARTNERS CONTROL PANEL" Target="_blank" runat="server"/>
					<asp:Image ID="Image1" runat="server" CssClass="strong" ImageUrl="/NPCommon/images/iconNewWinRight.gif" />
	            </td>
			</tr>
			<tr><td height="20" colspan="2"></td></tr>
            <tr>
				<td width="300" valign="top">
					Affiliate Name
				    <AC:DivBox ID="DivBox1" runat="server" Text="Affiliate name as it apear on the system" /><br />
					<asp:TextBox ID="txtTitle" Width="200" runat="server" />
					<asp:requiredfieldvalidator id="rfvTitle" controltovalidate="txtTitle" display="Dynamic" errormessage="Merchant title cannot be blank!" SetFocusOnError="true" runat="server"/>
				</td>
            </tr>
			<tr>
				<td>
					Reference Code<br />
					<asp:TextBox ID="txtLinkRefID" Width="200" runat="server" /><br />
					<asp:CustomValidator ID="cfvLinkRefId" ControlToValidate="txtLinkRefID"  display="Dynamic" errormessage="Reference Code is already in use,<br />Plese enter a new one." SetFocusOnError="true" runat="server" OnServerValidate="CheckRefCode"/>
				</td>
				<td>
					<asp:Literal ID="litRefLink" runat="server" />
				</td>
			</tr>
			<tr><td height="20" colspan="2"></td></tr>
			<tr>
			    <td>
			        <b>Payout information</b> <AC:DivBox ID="DivBox3" runat="server" Text="Legal Name for payment and wires" />
			    </td>
			</tr>
			<tr>
			    <td>
				    Legal Name<br />
				    <asp:TextBox ID="txtLegalName" Width="200" runat="server" />
				</td>
				<td valign="top" rowspan="2">
					Comment<br />
					<asp:TextBox ID="txtRemark" Width="500" runat="server" Height="56" TextMode="MultiLine" />
				</td>
			</tr>
			<tr>
			    <td> Registration Numberr<br />
				    <asp:TextBox ID="txtLegalNumber" Width="200" runat="server" />
				</td>
			</tr>
			<tr>
			    <td>
				    Payee Name<br />
				    <asp:TextBox ID="txtPaymentPayeeName" Width="200" runat="server" />
				</td>
			</tr>
			<tr>
			    <td>
				    ID Number<br />
				    <asp:TextBox ID="txtIDnumber" Width="200" runat="server" />
				</td>
			</tr>
			<tr>
				<td>
				</td>
			</tr>
			<tr>
			    <td>
				    Outbound UserName<br />
				    <asp:TextBox ID="txtUserName" Width="200" runat="server" />
				</td>
			</tr>
			<tr>
			    <td>
				    Outbound Password<br />
				    <asp:TextBox ID="txtPassword" Width="200" runat="server" />
				</td>
			</tr>
            <tr>
			    <td>
				    Default Payment Currency:
				    <AC:DivBox ID="DivBox2" runat="server" Text="Both default bank account and bank accounts for currencies can be managed in 'BANK ACCOUNTS' page." />
				    <br />
			        <asp:ListBox Rows="1" runat="server" ID="ddPaymentCurrency" />
			    </td>
            </tr>
			<tr><td height="20" colspan="2"></td></tr>
			<tr><td colspan="2"><b>Allow Pages:</b></td></tr>
			<tr>
			</tr>
				<td valign="top">
				    <asp:CheckBox runat="server" ID="CapturedTransactions" Text="Captured Transactions" /> <AC:DivBox runat="server" Text="Enable Captured Transactions menu in affiliate control panel" /><br />
				    <asp:CheckBox runat="server" ID="DeclinedTransactions" Text="Declined Transactions" /> <AC:DivBox runat="server" Text="Enable Declined Transactions menu in affiliate control panel" /><br />
				    <asp:CheckBox runat="server" ID="Payments" Text="Payments" /> <AC:DivBox runat="server" Text="Enable Payments menu in affiliate control panel" /><br />
				    <asp:CheckBox runat="server" ID="Reports" Text="Reports" /> <AC:DivBox runat="server" Text="Enable Reports menu in affiliate control panel" /><br />
				    <asp:CheckBox runat="server" ID="Customers" Text="Customers" /> <AC:DivBox runat="server" Text="Enable Customers menu in affiliate control panel" /><br />
				</td>
			</tr>	
			<tr>
				<!--
				<td valign="top">
					<asp:CheckBox ID="chkAFShowMerchants" runat="server" CssClass="option" /> Show merchant details<br />
					<asp:CheckBox ID="chkAFSettlements" runat="server" CssClass="option" /> Show Settlements<br />
					<asp:CheckBox ID="chkAFShowStats" runat="server" CssClass="option" /> Show statistics<br />
					<asp:CheckBox ID="chkAFTransactionApplication" runat="server" CssClass="option" /> Show Transaction Application<br />
				</td>
				-->
			</tr>
			<!--
			<tr><td height="20" colspan="2"></td></tr>
			<tr><td colspan="2"><b>Merchant Control Panel</b></td></tr>
			<tr>
				<td>
					<asp:CheckBox ID="chkShowSystemPay" runat="server" CssClass="option" /> Charging system<br />
					<asp:CheckBox ID="chkShowRemoteCharge" runat="server" CssClass="option" /> Remote charge<br />
					<asp:CheckBox ID="chkShowCustomerPurchase" runat="server" CssClass="option" /> Purchases charge<br />
					<asp:CheckBox ID="chkShowPublicPay" runat="server" CssClass="option" /> Public payment page<br />
				</td>
				<td>
					<asp:CheckBox ID="chkShowDataUpdate" runat="server" CssClass="option" /> Update Details<br />
					<asp:CheckBox ID="chkShowFees" runat="server" CssClass="option" /> Show Fees<br />
					<asp:CheckBox ID="chkShowDownloads" runat="server" CssClass="option" /> Downloads<br />
					<asp:CheckBox ID="chkShowPayments" runat="server" CssClass="option" /> Payments<br />
				</td>
			</tr>
			-->
			<tr><td height="20" colspan="2"></td></tr>
			<tr>
				<td colspan="2">
					<br /><asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" Text=" UPDATE " />
					<asp:ValidationSummary ShowMessageBox="false" ShowSummary="false" runat="server" ID="vsMerchant" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><hr size="1" color="silver" noshade /></td></tr>
	<tr>
		<td>
			<table align="left" border="0" class="formNormal">
			<%
                    If Len("" & sFileName) > 0 Then
                        Dim sFilePAth As String = Domain.Current.MapPublicDataPath(sFileName)
                        If System.IO.File.Exists(sFilePAth) Then
                            Dim fiFile As System.IO.FileInfo = New System.IO.FileInfo(sFilePAth)
                            Dim imgFile As New System.Drawing.Bitmap(sFilePAth)
                            Dim sHeight As String = ""
                            If imgFile.Height > 80 Then
                                sHeight = " height=""80px"""
                            End If
					%>
					<tr>
						<td>
							<b>Top Logo</b><br />
							<%=fiFile.Name%><br />
							<%=fiFile.Length%> bytes
							<%=imgFile.Width%> w X <%=imgFile.Height%> h<br />
							<% imgFile.Dispose  %>
							<br /><img src="<%= Domain.Current.MapPublicDataVirtualPath(sFileName) %>" alt="" <%=sHeight%> border="0"><br />					
						</td>
					</tr>
					<%	
                    Else
					%>
					<tr><td>File not found. Please delete and upload again.</td></tr>
					<%
				End If
				%>
				<tr><td><br /><asp:Button ID="btnDeleteFile" CssClass="buttonWhite" runat="server" Text="Delete File" OnClick="btnDeleteFile_Click" /></td></tr>		
				<%
			Else
				%>		
				<tr>
					<td>
						<b>Top Logo</b><br />
						<asp:FileUpload ID="fuDocument" runat="server" /><asp:Button ID="btnUpload" OnClick="UploadFile" Text="Upload File" UseSubmitBehavior="false" runat="server" />
					</td>
				</tr>		
				<%
			End If
			%>
			</table>
		</form>	
	</td>
</tr>
</table>	
</body>
</html>