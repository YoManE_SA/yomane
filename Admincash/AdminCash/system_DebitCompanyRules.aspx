﻿<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Sub ResetTerminalAlert(ByVal o As Object, ByVal e As CommandEventArgs)
		dbPages.ExecSql("UPDATE tblDebitTerminals SET dt_Monthly" & IIf(e.CommandName = "MC", "MC", String.Empty) & "CHBWasSent=0 WHERE ID=" & e.CommandArgument)
		rptTerminals.DataBind()
	End Sub
	
	Sub DisableUpDownButtons(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		If rptRules.Items.Count > 0 Then
			CType(rptRules.Items(0).FindControl("btnUp"), Button).Enabled = False
			CType(rptRules.Items(rptRules.Items.Count - 1).FindControl("btnDown"), Button).Enabled = False
		End If
	End Sub

	Sub SetTerminalsHeaderVisibility(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		rptTerminals.Visible = (rptTerminals.Items.Count > 0)
		litAlerts.Visible = rptTerminals.Visible
	End Sub

	Sub RefreshScreen()
		rptRules.DataBind()
		rptRules.Visible = (rptRules.Items.Count > 0)
	End Sub

	Sub AddRecord(ByVal o As Object, ByVal e As EventArgs)
		Dim nDebitCompany As Integer = dbPages.TestVar(Request("DebitCompany"), 1, 0, 0)
		dbPages.ExecSql("INSERT INTO tblDebitRule(dr_DebitCompany) VALUES (" & nDebitCompany & ")")
		SaveRecords()
	End Sub

	Sub RemoveRecord(ByVal o As Object, ByVal e As EventArgs)
		dbPages.ExecSql("DELETE FROM tblDebitRule WHERE ID=" & CType(CType(o, Control).NamingContainer.FindControl("litID"), Literal).Text)
		SaveRecords()
	End Sub
	
	Sub SaveRecords(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		Dim sbSQL As New StringBuilder(), nAttempts, nFails As Integer
		If rptRules.Items.Count > 0 Then
			For Each riRecord As RepeaterItem In rptRules.Items
				If CType(riRecord.FindControl("chkDelete"), CheckBox).Checked Then
					dbPages.ExecSql("DELETE FROM tblDebitRule WHERE ID=" & CType(riRecord.FindControl("litID"), Literal).Text)
				Else
					nAttempts = dbPages.TestVar(CType(riRecord.FindControl("txtAttemptCount"), TextBox).Text, 1, 0, 0)
					nFails = dbPages.TestVar(CType(riRecord.FindControl("txtFailCount"), TextBox).Text, 1, 0, 0)
					If nAttempts < nFails Then
						CType(riRecord.FindControl("txtAttemptCount"), TextBox).Text = nFails
						CType(riRecord.FindControl("txtFailCount"), TextBox).Text = nAttempts
						nFails = nAttempts
						nAttempts = dbPages.TestVar(CType(riRecord.FindControl("txtAttemptCount"), TextBox).Text, 1, 0, 0)
					End If
					sbSQL.Remove(0, sbSQL.Length)
					sbSQL.Append("UPDATE tblDebitRule SET dr_ReplyCodes='" & dbPages.DBText(CType(riRecord.FindControl("txtReplyCodes"), TextBox).Text) & "'")
					sbSQL.Append(", dr_NotifyUsers='")
					Dim bFirstUser As Boolean = True
					For Each liCheckbox As ListItem In CType(riRecord.FindControl("cblUsers"), CheckBoxList).Items
						If liCheckbox.Selected Then
							If bFirstUser Then bFirstUser = False Else sbSQL.Append(";")
							sbSQL.Append(liCheckbox.Value)
						End If
					Next
					sbSQL.Append("', dr_NotifyUsersSMS='")
					bFirstUser = True
					For Each liCheckbox As ListItem In CType(riRecord.FindControl("cblUsersSMS"), CheckBoxList).Items
						If liCheckbox.Selected Then
							If bFirstUser Then bFirstUser = False Else sbSQL.Append(";")
							sbSQL.Append(liCheckbox.Value)
						End If
					Next
					sbSQL.AppendFormat("', dr_AttemptCount={0}, dr_FailCount={1}", nAttempts, nFails)
					sbSQL.Append(", dr_AutoEnableMinutes=" & dbPages.TestVar(CType(riRecord.FindControl("txtAutoEnableMinutes"), TextBox).Text, 1, 0, 0))
					sbSQL.Append(", dr_AutoEnableAttempts=" & dbPages.TestVar(CType(riRecord.FindControl("txtAutoEnableAttempts"), TextBox).Text, 1, 0, 0))
					sbSQL.Append(", dr_IsAutoDisable=" & IIf(CType(riRecord.FindControl("chkIsAutoDisable"), CheckBox).Checked, 1, 0))
					sbSQL.Append(", dr_IsAutoEnable=" & IIf(CType(riRecord.FindControl("chkIsAutoEnable"), CheckBox).Checked, 1, 0))
					sbSQL.Append(", dr_IsActive=" & IIf(CType(riRecord.FindControl("chkIsActive"), CheckBox).Checked, 1, 0))
					dbPages.ExecSql(sbSQL.Append(" WHERE ID=" & CType(riRecord.FindControl("litID"), Literal).Text).ToString)
				End If
			Next
			If Not o Is Nothing Then
				Dim sControl As String = CType(o, Control).ID
				If sControl = "btnUp" Or sControl = "btnDown" Then
					Dim nRuleID As Integer = CType(CType(o, Control).NamingContainer.FindControl("litID"), Literal).Text
					dbPages.ExecSql("EXEC DebitRuleMove " & nRuleID & ", " & IIf(sControl = "btnUp", 1, 0))
				End If
			End If
		End If
		RefreshScreen()
		If pnlCHB.Visible Then
			Dim sMonthlyLimitCHBNotifyUsers As String = NPControls.GetSelectedItems(cblMonthlyLimitCHBNotifyUsers)
			Dim sMonthlyLimitCHBNotifyUsersSMS As String = NPControls.GetSelectedItems(cblMonthlyLimitCHBNotifyUsersSMS)
			Dim sMonthlyMCLimitCHBNotifyUsers As String = NPControls.GetSelectedItems(cblMonthlyMCLimitCHBNotifyUsers)
			Dim sMonthlyMCLimitCHBNotifyUsersSMS As String = NPControls.GetSelectedItems(cblMonthlyMCLimitCHBNotifyUsersSMS)
			Dim sAutoRefundNotifyUsers As String = NPControls.GetSelectedItems(cblAutoRefundNotifyUsers)
			sbSQL.Remove(0, sbSQL.Length).AppendFormat("UPDATE tblDebitCompany SET dc_MonthlyLimitCHB={0}", dbPages.TestVar(txtMonthlyLimitCHB.Text, 1, 999999, 0))
			sbSQL.AppendFormat(", dc_MonthlyLimitCHBNotifyUsers='{0}'", dbPages.DBText(sMonthlyLimitCHBNotifyUsers))
			sbSQL.AppendFormat(", dc_MonthlyLimitCHBNotifyUsersSMS='{0}'", dbPages.DBText(sMonthlyLimitCHBNotifyUsersSMS))
			sbSQL.AppendFormat(", dc_MonthlyMCLimitCHB={0}", dbPages.TestVar(txtMonthlyMCLimitCHB.Text, 1, 999999, 0))
			sbSQL.AppendFormat(", dc_MonthlyMCLimitCHBNotifyUsers='{0}'", dbPages.DBText(sMonthlyMCLimitCHBNotifyUsers))
			sbSQL.AppendFormat(", dc_MonthlyMCLimitCHBNotifyUsersSMS='{0}'", dbPages.DBText(sMonthlyMCLimitCHBNotifyUsersSMS))
			sbSQL.AppendFormat(", dc_AutoRefundNotifyUsers='{0}'", dbPages.DBText(sAutoRefundNotifyUsers))
			sbSQL.AppendFormat(" WHERE DebitCompany_ID={0}", dbPages.TestVar(Request("DebitCompany"), 1, 0, 0))
			litMonthlyLimitCHBNotifyUsers.Text = sMonthlyLimitCHBNotifyUsers
			litMonthlyLimitCHBNotifyUsersSMS.Text = sMonthlyLimitCHBNotifyUsersSMS
			litMonthlyMCLimitCHBNotifyUsers.Text = sMonthlyMCLimitCHBNotifyUsers
			litMonthlyMCLimitCHBNotifyUsersSMS.Text = sMonthlyMCLimitCHBNotifyUsersSMS
			dbPages.ExecSql(sbSQL.ToString)
		End If
	End Sub

	Sub RecheckUsers(ByVal o As Object, ByVal e As RepeaterItemEventArgs)
		Dim cblRuleUsers As CheckBoxList = e.Item.FindControl("cblUsers")
		Dim litUsers As Literal = e.Item.FindControl("litNotifyUsers")
		If Not cblRuleUsers Is Nothing Then
			For Each liCheckbox As ListItem In cblRuleUsers.Items
				liCheckbox.Selected = (";" & litUsers.Text & ";").Contains(";" & liCheckbox.Value & ";")
			Next
		End If
		cblRuleUsers = e.Item.FindControl("cblUsersSMS")
		litUsers = e.Item.FindControl("litNotifyUsersSMS")
		If Not cblRuleUsers Is Nothing Then
			For Each liCheckbox As ListItem In cblRuleUsers.Items
				liCheckbox.Selected = (";" & litUsers.Text & ";").Contains(";" & liCheckbox.Value & ";")
			Next
		End If
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		If dbPages.TestVar(Request("DebitCompany"), 1, 0, 0) < 1 Then pnlCHB.Visible = False
		dsTerminals.ConnectionString = dbPages.DSN
		dsRules.ConnectionString = dbPages.DSN
		dsRules.SelectCommand = "SELECT tblDebitRule.*, CASE dr_IsActive WHEN 1 THEN '#66cc66' ELSE '#ff6666' END StatusColor," & _
		" Cast(Abs(Sign(DateDiff(y, dr_LastFailDate, 0))) AS bit) WasFail," & _
		" Cast(Abs(Sign(DateDiff(y, dr_LastUnblockDate, 0))) AS bit) WasUnblock," & _
		" CASE @nDebitCompany WHEN 0 THEN dc_name ELSE '' END DebitCompanyName" & _
		" FROM tblDebitRule INNER JOIN tblDebitCompany ON dr_DebitCompany=DebitCompany_ID" & _
		" WHERE @nDebitCompany IN (0, DebitCompany_ID)" & _
		IIf(Security.IsLimitedDebitCompany, " AND DebitCompany_ID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))", String.Empty) & _
		" ORDER BY DebitCompany_ID, dr_Rating, dr_IsActive DESC, dr_FailCount/(dr_AttemptCount+.1), ID DESC"
		dsUsers.ConnectionString = dbPages.DSN
		dsUsersSMS.ConnectionString = dbPages.DSN
		If Not Page.IsPostBack Then
			Dim nDebitCompany As Integer = dbPages.TestVar(Request("DebitCompany"), 1, 0, 0)
			If nDebitCompany > 0 Then
				litDebitCompanyName.Text = dbPages.ExecScalar("SELECT dc_Name FROM tblDebitCompany WHERE DebitCompany_ID=" & nDebitCompany) & " (#" & nDebitCompany & ")"
				tlbTop.LoadDebitComapnyTabs(nDebitCompany)
			Else
				litDebitCompanyName.Text = "All Debit Companies"
			End If
			RefreshScreen()
			Dim drMonthly, drUsers As SqlDataReader
			drMonthly = dbPages.ExecReader("SELECT * FROM tblDebitCompany WHERE DebitCompany_ID=" & nDebitCompany)
			If drMonthly.Read() Then
				litMonthlyLimitCHBNotifyUsers.Text = drMonthly("dc_MonthlyLimitCHBNotifyUsers")
				litMonthlyLimitCHBNotifyUsersSMS.Text = drMonthly("dc_MonthlyLimitCHBNotifyUsersSMS")
				litMonthlyMCLimitCHBNotifyUsers.Text = drMonthly("dc_MonthlyMCLimitCHBNotifyUsers")
				litMonthlyMCLimitCHBNotifyUsersSMS.Text = drMonthly("dc_MonthlyMCLimitCHBNotifyUsersSMS")
				drUsers = dbPages.ExecReader("SELECT su_Username ID, su_Username FROM tblSecurityUser WHERE su_MailEmergency<>'' AND su_IsActive=1 ORDER BY su_Username")
				NPControls.FillList(cblMonthlyLimitCHBNotifyUsers, drUsers, , , , Nothing)
				For Each liUser As ListItem In cblMonthlyLimitCHBNotifyUsers.Items
					liUser.Selected = (";" & litMonthlyLimitCHBNotifyUsers.Text & ";").Contains(";" & liUser.Value & ";")
				Next
				drUsers.Close()
				drUsers = dbPages.ExecReader("SELECT su_Username ID, su_Username FROM tblSecurityUser WHERE su_SMS<>'' AND su_IsActive=1 ORDER BY su_Username")
				NPControls.FillList(cblMonthlyLimitCHBNotifyUsersSMS, drUsers, , , , Nothing)
				For Each liUser As ListItem In cblMonthlyLimitCHBNotifyUsersSMS.Items
					liUser.Selected = (";" & litMonthlyLimitCHBNotifyUsersSMS.Text & ";").Contains(";" & liUser.Value & ";")
				Next
				drUsers.Close()
				drUsers = dbPages.ExecReader("SELECT su_Username ID, su_Username FROM tblSecurityUser WHERE su_MailEmergency<>'' AND su_IsActive=1 ORDER BY su_Username")
				NPControls.FillList(cblMonthlyMCLimitCHBNotifyUsers, drUsers, , , , Nothing)
				For Each liUser As ListItem In cblMonthlyMCLimitCHBNotifyUsers.Items
					liUser.Selected = (";" & litMonthlyMCLimitCHBNotifyUsers.Text & ";").Contains(";" & liUser.Value & ";")
				Next
				drUsers.Close()
				drUsers = dbPages.ExecReader("SELECT su_Username ID, su_Username FROM tblSecurityUser WHERE su_SMS<>'' AND su_IsActive=1 ORDER BY su_Username")
				NPControls.FillList(cblMonthlyMCLimitCHBNotifyUsersSMS, drUsers, , , , Nothing)
				For Each liUser As ListItem In cblMonthlyMCLimitCHBNotifyUsersSMS.Items
					liUser.Selected = (";" & litMonthlyMCLimitCHBNotifyUsersSMS.Text & ";").Contains(";" & liUser.Value & ";")
				Next
				drUsers.Close()
				txtMonthlyLimitCHB.Text = IIf(drMonthly("dc_MonthlyLimitCHB") > 0, drMonthly("dc_MonthlyLimitCHB"), String.Empty)
				txtMonthlyMCLimitCHB.Text = IIf(drMonthly("dc_MonthlyMCLimitCHB") > 0, drMonthly("dc_MonthlyMCLimitCHB"), String.Empty)
				drUsers = dbPages.ExecReader("SELECT su_Username ID, su_Username FROM tblSecurityUser WHERE su_MailEmergency<>'' AND su_IsActive=1 ORDER BY su_Username")
				NPControls.FillList(cblAutoRefundNotifyUsers, drUsers, , , , Nothing)
				For Each liUser As ListItem In cblAutoRefundNotifyUsers.Items
					liUser.Selected = (";" & drMonthly("dc_AutoRefundNotifyUsers") & ";").Contains(";" & liUser.Value & ";")
				Next
				drUsers.Close()
			End If
			drMonthly.Close()
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Debit Company - Notifications</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading{text-decoration:underline;white-space:nowrap;}
		table.formNormal td, table.formNormal th {text-align:center;}
		.btnSave { width:58px; height:52px; }
	</style>
	<script language="javascript" type="text/javascript" src="../js/func_common.js"></script>
</head>
<body onload="focus();">
	<iframe id="ifrDelete" style="display:none;"></iframe>
	<form id="frmMain" runat="server">
	<table align="center" style="width:95%;">
	<tr>
		<td id="pageMainHeadingTd">
			<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">Notifications</span><span style="letter-spacing:-1px;"> - <asp:Literal ID="litDebitCompanyName" runat="server" /></span>
		</td>
	</tr>
	</table>
	<br />
	<custom:Toolbar ID="tlbTop" Width="95%" runat="server" />
	<script language="javascript" type="text/javascript">
		function NextSibling(oNode)	{
			for(oNode=oNode.nextSibling;oNode && oNode.nodeType==3;oNode=oNode.nextSibling);
			return oNode;
		}
		function PreviousSibling(oNode)	{
			for(oNode=oNode.previousSibling;oNode && oNode.nodeType==3;oNode=oNode.previousSibling);
			return oNode;
		}
		
		function ExpandRecord()	{
			event.srcElement.style.display="none";
			NextSibling(event.srcElement).style.display="";
			NextSibling(event.srcElement.parentNode.parentNode).style.display="";
		}
		function CollapseRecord() {
			event.srcElement.style.display="none";
			PreviousSibling(event.srcElement).style.display="";
			NextSibling(event.srcElement.parentNode.parentNode).style.display="none";
		}
		function ConfirmDeleteRecord(nRuleID) {
			return confirm("Rule #"+nRuleID+" will be deleted from the system.\n\nDo you really want to delete this rule?");
		}
	</script>
	<asp:Panel ID="pnlCHB" runat="server">
	<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
		<tr>
			<td valign="top">
				<span class="MerchantSubHead">Automatic Refund Notification</span>
				<AC:DivBox Width="375px" runat="server">
					Used for sending a notification when automatic refund occurs.<br />
					When no user is set to receive the notification, it is never sent.
				</AC:DivBox>
			</td>
		</tr>
		<tr>
			<td>
				<table class="formNormal">
					<tr>
						<td>
							<b>Notify by mail:</b>
							<asp:CheckBoxList ID="cblAutoRefundNotifyUsers" CssClass="option" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server" />
						</td>
					</tr>
				</table>
				<asp:Button Text="Save Changes" OnClick="SaveRecords" UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" />
			</td>
		</tr>
	</table>
	<br /><hr style="width:95%;" /><br />
	<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
		<tr>
			<td valign="top">
				<span class="MerchantSubHead">CHB Alert</span>
				<AC:DivBox Width="500px" runat="server">
					Used for sending an alert when chb count for a single terminal in this debit company exceeds the number specified in "Monthly limit" field.<br />
					When "Monthly limit" is not set, the alert is never sent.
				</AC:DivBox>
			</td>
		</tr>
		<tr><td height="5"></td></tr>
		<tr>
			<td>
				<table class="formNormal">
				<tr>
					<th colspan="2">Payment method</th>
					<th>Monthly limit</th>
					<th style="text-align:left;">Notify By Mail</th>
					<th style="text-align:left;">Notify By SMS</th>
				</tr>
				<tr>
					<td style="text-align:right;">
						<img onclick="ExpandRecord();" src="../images/tree_expand.gif" style="border:0;padding:0;margin:0 0 1px;vertical-align:middle;" alt="Click to expand" />
						<img onclick="CollapseRecord();" src="../images/tree_collapse.gif" style="display:none;border:0;padding:0;margin:0 0 1px;vertical-align:middle;" alt="Click to collapse" />
					</td>
					<td style="text-align:left;"><img align="middle" src="/NPCommon/ImgPaymentMethod/23X12/22.gif"> VISA</td>
					<td>
						<asp:TextBox ID="txtMonthlyLimitCHB" CssClass="medium" runat="server" />
						<asp:RangeValidator ControlToValidate="txtMonthlyLimitCHB" MinimumValue="0" MaximumValue="999999"
						 ErrorMessage="Monthly limit is invalid!" Display="none" SetFocusOnError="true" Type="Integer" runat="server" /> &nbsp;
					</td>
					<td style="text-align:left;">
						<asp:Literal ID="litMonthlyLimitCHBNotifyUsers" runat="server" />
					</td>
					<td style="text-align:left;">
						<asp:Literal ID="litMonthlyLimitCHBNotifyUsersSMS" runat="server" />
					</td>
				</tr>
				<tr id="trVISA" style="display:none;">
					<td></td>
					<td colspan="4" style="padding:5px 0 5px 10px;border-top:1px dashed Silver;text-align:left;">
						<div style="text-align:left;">
							<b>Notify by mail:</b>
							<asp:CheckBoxList ID="cblMonthlyLimitCHBNotifyUsers" CssClass="option" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server" />
						</div>
						<div style="text-align:left;">
							<b>Notify by SMS:</b>
							<asp:CheckBoxList ID="cblMonthlyLimitCHBNotifyUsersSMS" CssClass="option" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server" />
						</div>
					</td>
				</tr>
				<tr><td height="1" colspan="16" bgcolor="silver"></td></tr>
				<tr>
					<td style="text-align:right;">
						<img onclick="ExpandRecord();" src="../images/tree_expand.gif" style="border:0;padding:0;margin:0 0 1px;vertical-align:middle;" alt="Click to expand" />
						<img onclick="CollapseRecord();" src="../images/tree_collapse.gif" style="display:none;border:0;padding:0;margin:0 0 1px;vertical-align:middle;" alt="Click to collapse" />
					</td>
					<td style="text-align:left;"><img align="middle" src="/NPCommon/ImgPaymentMethod/23X12/25.gif"> MC</td>
					<td>
						<asp:TextBox ID="txtMonthlyMCLimitCHB" CssClass="medium" runat="server" />
						<asp:RangeValidator ControlToValidate="txtMonthlyMCLimitCHB" MinimumValue="0" MaximumValue="999999"
						 ErrorMessage="MonthlyMC limit is invalid!" Display="none" SetFocusOnError="true" Type="Integer" runat="server" /> &nbsp;
					</td>
					<td style="text-align:left;">
						<asp:Literal ID="litMonthlyMCLimitCHBNotifyUsers" runat="server" />
					</td>
					<td style="text-align:left;">
						<asp:Literal ID="litMonthlyMCLimitCHBNotifyUsersSMS" runat="server" />
					</td>
				</tr>
				<tr id="tr1" style="display:none;">
					<td></td>
					<td colspan="4" style="padding:5px 0 5px 10px;border-top:1px dashed Silver;text-align:left;">
						<div style="text-align:left;">
							<b>Notify by mail:</b>
							<asp:CheckBoxList ID="cblMonthlyMCLimitCHBNotifyUsers" CssClass="option" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server" />
						</div>
						<div style="text-align:left;">
							<b>Notify by SMS:</b>
							<asp:CheckBoxList ID="cblMonthlyMCLimitCHBNotifyUsersSMS" CssClass="option" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server" />
						</div>
					</td>
				</tr>
				</table>
				<asp:Button Text="Save Changes" OnClick="SaveRecords" UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" />
				<asp:ValidationSummary ShowMessageBox="true" ShowSummary="false" runat="server" />
			</td>
		</tr>
	</table>
	<br />
	<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
		<tr>
			<td valign="top" style="font-size:12px;"><asp:Literal ID="litAlerts" Text="Alert Log" runat="server" /></td>
		</tr>
		<tr><td height="5"></td></tr>
		<tr>
			<td>
				<table class="formNormal">
					<asp:Repeater ID="rptTerminals" DataSourceID="dsTerminals" runat="server" OnPreRender="SetTerminalsHeaderVisibility">
						<HeaderTemplate>
							<tr>
								<th class="light">Terminal No.</th>
								<th class="light">Name</th>
								<th class="light">Payment Method</th>
								<th class="light">CHB Alert</th>
								<th class="light">Reset</th>
							</tr>
						</HeaderTemplate>
						<ItemTemplate>
							<tr>
								<td style="text-align:left;"><asp:Literal Text='<%# Eval("TerminalNumber") %>' runat="server" /></td>
								<td style="text-align:left;"><asp:Literal Text='<%# Eval("dt_Name") %>' runat="server" /></td>
								<td style="text-align:left;">
									<img align="middle" src="/NPCommon/ImgPaymentMethod/23X12/<%# Eval("PaymentMethodType") %>.gif">
									<asp:Literal Text='<%# Eval("PaymentMethod") %>' runat="server" />
								</td>
								<td style="text-align:left;">
									<asp:Literal runat="server"
									 Text='<%# IIf(Eval("dt_MonthlyCHBWasSent"), "Sent on " & dbpages.FormatDateTime(Eval("dt_MonthlyCHBSendDate")), "Not sent this month") & ". " %>' />
								</td>
								<td>
									<asp:LinkButton Text='<%# IIf(Eval("dt_MonthlyCHBWasSent"), "Reset", String.Empty) %>' runat="server"
									 OnCommand="ResetTerminalAlert" CommandName='<%# Eval("PaymentMethod") %>' CommandArgument='<%# Eval("ID") %>' />
								</td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</table>
				<asp:SqlDataSource ID="dsTerminals" runat="server"
				 SelectCommand="SELECT ID, TerminalNumber, dt_Name, dt_MonthlyCHBWasSent, dt_MonthlyCHBSendDate, 'VISA' PaymentMethod, 22 PaymentMethodType FROM tblDebitTerminals WHERE DebitCompany=@DebitCompany AND dt_MonthlyCHBWasSent=1
				  UNION SELECT ID, TerminalNumber, dt_Name, dt_MonthlyMCCHBWasSent, dt_MonthlyMCCHBSendDate, 'MC', 25 FROM tblDebitTerminals WHERE DebitCompany=@DebitCompany AND dt_MonthlyMCCHBWasSent=1">
					<SelectParameters>
						<asp:QueryStringParameter Name="DebitCompany" QueryStringField="DebitCompany" Type="Int32" DefaultValue="0" ConvertEmptyStringToNull="true" />
					</SelectParameters>
				</asp:SqlDataSource>
			</td>
		</tr>
	</table>
	<br /><hr style="width:95%;" /><br />
	</asp:Panel>
	<table align="center" width="95%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td style="vertical-align:top;"><span class="MerchantSubHead">Notifications</span></td>
			<td style="text-align:right;padding-bottom:10px;"><asp:Button Text="Add New Rule" OnClick="AddRecord" CssClass="buttonWhite" UseSubmitBehavior="false" runat="server" /></td>
		</tr>
	</table>
	<table class="formNormal" align="center" style="width:95%;">
		<asp:Repeater ID="rptRules" DataSourceID="dsRules" runat="server" OnPreRender="DisableUpDownButtons" OnItemDataBound="RecheckUsers">
			<HeaderTemplate>
				<tr>
					<th colspan="3" rowspan="2" style="text-align:right;">ID</th>
					<asp:Literal Text='<%# IIf(dbPages.TestVar(Request("DebitCompany"), 1, 0, 0)=0, "<th rowspan=""2"">Debit Company</th>", String.Empty) %>' runat="server" />
					<th rowspan="2">Reply Codes*</th>
					<th colspan="2" style="border-bottom:0px;">Fail Condition</th>
					<th colspan="2" style="border-bottom:0px;">Notify Users</th>
					<th colspan="2" style="border-bottom:0px;">Automation</th>
					<th colspan="2" style="border-bottom:0px;">AutoUnblock After</th>
					<th rowspan="2">Active</th>
					<th rowspan="2">Remove</th>
					<th rowspan="2">Change Order</th>
				</tr>
				<tr>
					<th>Fails</th>
					<th>Attempts</th>
					<th>Mail</th>
					<th>SMS</th>
					<th>Block</th>
					<th>Unblock</th>
					<th>Minutes</th>
					<th>Attempts</th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr onmouseover="this.style.backgroundColor='#f5f5f5';" onmouseout="this.style.backgroundColor='';">
					<td>
						<img onclick="ExpandRecord();" src="../images/tree_expand.gif" style="border:0;padding:0;margin:0 0 1px;vertical-align:middle;" alt="Click to expand" />
						<img onclick="CollapseRecord();" src="../images/tree_collapse.gif" style="display:none;border:0;padding:0;margin:0 0 1px;vertical-align:middle;" alt="Click to collapse" />
					</td>
					<td>
						<asp:Literal Text='<%# "<span style=""background-color:" & Eval("StatusColor") & ";"">&nbsp;</span>" %>' runat="server" />
					</td>
					<td style="text-align:right;"><asp:Literal ID="litID" Text='<%# Eval("ID") %>' runat="server" /></td>
					<asp:Literal ID="litDC" Text='<%# IIf(dbPages.TestVar(Request("DebitCompany"), 1, 0, 0)=0, "<td>" & Eval("DebitCompanyName") & "</td>", String.Empty) %>' runat="server" />
					<td><asp:TextBox ID="txtReplyCodes" Text='<%# Eval("dr_ReplyCodes") %>' CssClass="medium" runat="server" /></td>
					<td><asp:TextBox ID="txtFailCount" Text='<%# Eval("dr_FailCount") %>' CssClass="short" runat="server" /></td>
					<td><asp:TextBox ID="txtAttemptCount" Text='<%# Eval("dr_AttemptCount") %>' CssClass="short" runat="server" /></td>
					<td>
						<asp:ImageButton ToolTip='<%# Eval("dr_NotifyUsers") %>'
							ImageUrl='<%# "../images/checkbox" & IIf(String.IsNullOrEmpty(Eval("dr_NotifyUsers")), "_off", "") & ".gif" %>'
							OnClientClick="return false;//" style="cursor:default;" runat="server" />
						<asp:ImageButton ToolTip='<%# Eval("dr_NotifyUsers") %>'
							ImageUrl='<%# "../images/checkbox" & IIf(String.IsNullOrEmpty(Eval("dr_NotifyUsers")), "_off", "") & ".gif" %>'
							OnClientClick="return false;//" style="cursor:default;display:none;" runat="server" />
						<asp:Literal ID="litNotifyUsers" Text='<%# Eval("dr_NotifyUsers") %>' Visible="false" runat="server" />
					</td>
					<td>
						<asp:ImageButton ToolTip='<%# Eval("dr_NotifyUsersSMS") %>'
							ImageUrl='<%# "../images/checkbox" & IIf(String.IsNullOrEmpty(Eval("dr_NotifyUsersSMS")), "_off", "") & ".gif" %>'
							OnClientClick="return false;//" style="cursor:default;" runat="server" />
						<asp:ImageButton ToolTip='<%# Eval("dr_NotifyUsersSMS") %>'
							ImageUrl='<%# "../images/checkbox" & IIf(String.IsNullOrEmpty(Eval("dr_NotifyUsersSMS")), "_off", "") & ".gif" %>'
							OnClientClick="return false;//" style="cursor:default;display:none;" runat="server" />
						<asp:Literal ID="litNotifyUsersSMS" Text='<%# Eval("dr_NotifyUsersSMS") %>' Visible="false" runat="server" />
					</td>
					<td><asp:CheckBox ID="chkIsAutoDisable" Checked='<%# Eval("dr_IsAutoDisable") %>' CssClass="option" runat="server" /></td>
					<td><asp:CheckBox ID="chkIsAutoEnable" Checked='<%# Eval("dr_IsAutoEnable") %>' CssClass="option" runat="server" /></td>
					<td><asp:TextBox ID="txtAutoEnableMinutes" Text='<%# Eval("dr_AutoEnableMinutes") %>' CssClass="short" runat="server" /></td>
					<td><asp:TextBox ID="txtAutoEnableAttempts" Text='<%# Eval("dr_AutoEnableAttempts") %>' CssClass="short" runat="server" /></td>
					<td><asp:CheckBox ID="chkIsActive" Checked='<%# Eval("dr_IsActive") %>' CssClass="option" runat="server" /></td>
					<td>
						<asp:CheckBox ID="chkDelete" Checked="false" CssClass="option" runat="server"
							onclick='<%# "if (checked) checked=ConfirmDeleteRecord(" & Eval("ID") & ");" %>' />
					</td>
					<td>
						<asp:Button ID="btnUp" ToolTip="Move Up" OnClick="SaveRecords" runat="server" CssClass="wingdings" Text="á" />
						<asp:Button ID="btnDown" ToolTip="Move Down" OnClick="SaveRecords" runat="server" CssClass="wingdings" Text="â" />
					</td>
				</tr>
				<tr style="display:none;">
					<td colspan="3">&nbsp;</td>
					<td colspan="13" style="padding:5px 0 5px 10px;border-top:1px dashed Silver;text-align:left;">
						<div>
							<b>Rule history:</b>
							&nbsp; &nbsp; Last Fail:
							<asp:Literal runat="server"
								Text='<%# IIf(Eval("WasFail"), dbPages.FormatDateTime(Eval("dr_LastFailDate")), "never occured") %>' />
							&nbsp; &nbsp; Last Unblock:
							<asp:Literal runat="server"
								Text='<%# IIf(Eval("WasUnblock"), dbPages.FormatDateTime(Eval("dr_LastUnblockDate")), "never occured") %>' />
						</div>
						<div>
							<b>Notify by mail:</b>
							<asp:CheckBoxList ID="cblUsers" DataSourceID="dsUsers" CssClass="option" runat="server"
								DataTextField="su_Username" DataTextFormatString="{0}" RepeatLayout="Flow"
								DataValueField="su_Username" RepeatDirection="Horizontal" />
						</div>
						<div>
							<b>Notify by SMS:</b>
							<asp:CheckBoxList ID="cblUsersSMS" DataSourceID="dsUsersSMS" CssClass="option" runat="server"
								DataTextField="su_Username" DataTextFormatString="{0}" RepeatLayout="Flow"
								DataValueField="su_Username" RepeatDirection="Horizontal" />
						</div>
					</td>
				</tr>
				<tr><td height="1" colspan="16" bgcolor="silver"></td></tr>
			</ItemTemplate>
			<FooterTemplate>
				<tr>
					<td colspan="16" style="text-align:left;padding:10px 0 0;">
						<span style="float:right;">
							<asp:Button Text="Save Changes" OnClick="SaveRecords" UseSubmitBehavior="false" runat="server" />
						</span>
						* Multiple values allowed: separated with semicolons, with no spacing
					</td>
				</tr>
			</FooterTemplate>
		</asp:Repeater>
	</table>
	<asp:SqlDataSource ID="dsRules" runat="server">
		<SelectParameters>
			<asp:QueryStringParameter DefaultValue="0" Name="nDebitCompany" QueryStringField="DebitCompany" Type="Int32" />
		</SelectParameters>
	</asp:SqlDataSource>
	<asp:SqlDataSource ID="dsUsers" runat="server"
		SelectCommand="SELECT su_Username FROM tblSecurityUser WHERE su_MailEmergency<>'' AND su_IsActive=1 ORDER BY su_Username">
	</asp:SqlDataSource>
	<asp:SqlDataSource ID="dsUsersSMS" runat="server"
		SelectCommand="SELECT su_Username FROM tblSecurityUser WHERE su_SMS<>'' AND su_IsActive=1 ORDER BY su_Username">
	</asp:SqlDataSource>
	</form>
</body>
</html>