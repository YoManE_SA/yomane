<%@ Page Language="VB" %>
<%
	Response.ContentType = "text/plain;charset=Windows-1255"
	Response.ContentEncoding = System.Text.Encoding.ASCII
	Response.AddHeader("Content-Disposition", "attachment;filename=irb" & Date.Now.ToString("yyyyMMddhhmmss") & ".txt")

	Dim sCardNumber, sMerchantReference, sMerchantName As String, nExpYear, nExpMonth, nTransaction, nTransRefunded, nCurrency, nMCC As Integer, nAmount As Decimal
	Dim sMerchantReferencePrev, sMerchantNamePrev As String, nCurrencyPrev, nMCCPrev As Integer, sMerchantCity As String, sDescription As String
	
	Dim drData As System.Data.SqlClient.SqlDataReader = dbPages.ExecReader("EXEC GetInvikRefundBatch")
	If drData.HasRows Then
		Response.Write(Invik.FileHeaderRecord())
		Dim nRecord As Integer = 1, nBatch As Integer = 0, nCountTrans As Integer = 0, nAmountTrans As Decimal = 0, nRecordBatch As Integer, nAmountBatch As Decimal
		sMerchantReferencePrev = String.Empty
		sMerchantNamePrev = String.Empty
		nCurrencyPrev = -1
		nMCCPrev = -1
		While drData.Read
			nCurrency = drData("Currency")
			sMerchantReference = drData("MerchantReference")
			sMerchantName = drData("MerchantName")
			nMCC = dbPages.TestVar(drData("MCC"), 1, 0, 0)
			If nCurrency <> nCurrencyPrev Or nMCC <> nMCCPrev Or sMerchantReference = sMerchantReferencePrev Or sMerchantName = sMerchantNamePrev Then
				If nCurrencyPrev <> -1 Then
					nRecord += 1
					Response.Write(Invik.BatchTrailerRecord(nRecord, nBatch, nRecord, 0, 0, nRecordBatch, nAmountBatch))
				End If
				nRecordBatch = 0
				nAmountBatch = 0
				sMerchantReferencePrev = sMerchantReference
				sMerchantNamePrev = sMerchantName
				nCurrencyPrev = nCurrency
				nMCCPrev = nMCC
				nRecord += 1
				nBatch += 1
				Response.Write(Invik.BatchHeaderRecord(nRecord, nBatch, InvikBatchType.CreditBatch, nCurrency, sMerchantReference, sMerchantName, nMCC))
			End If
			sCardNumber = drData("CardNumber")
			nExpYear = drData("ExpYear")
			nExpMonth = drData("ExpMonth")
			nTransaction = dbPages.TestVar(drData("Transaction"), 1, 0, 0)
			nAmount = drData("Amount")
			nTransRefunded = dbPages.TestVar(drData("TransRefunded"), 1, 0, 0)
			nRecord += 1
			nRecordBatch += 1
			nAmountBatch += nAmount
			nCountTrans += 1
			nAmountTrans += nAmount
			Response.Write(Invik.TransactionRecord(nRecord, InvikRecordType.TransactionCredit, sCardNumber, nExpYear, nExpMonth, nTransaction, nAmount, , , nTransRefunded))
			sMerchantCity = drData("MerchantCity")
			sDescription = drData("Description")
			nRecord += 1
			nRecordBatch += 1
			Response.Write(Invik.InfoRecord(nRecord, sCardNumber, sMerchantCity, "", sDescription))
			dbPages.ExecSql("UPDATE tblInvikRefundBatch SET irb_IsDownloaded=1, irb_DownloadFileNumber=" & Invik.FileSequenceNumber & " WHERE ID=" & drData("ID"))
			sMerchantReferencePrev = sMerchantReference
			sMerchantNamePrev = sMerchantName
			nCurrencyPrev = nCurrency
			nMCCPrev = nMCC
		End While
		If nCurrencyPrev <> -1 Then
			nRecord += 1
			Response.Write(Invik.BatchTrailerRecord(nRecord, nBatch, nRecordBatch, 0, 0, nRecordBatch, nAmountBatch))
		End If
		nRecord += 1
		Response.Write(Invik.FileTrailerRecord(nRecord, nBatch, nRecord, nCountTrans, 0, 0, nCountTrans, nAmountTrans))
	Else
		Response.Write("The batch file is empty - no record found!")
	End If
	drData.Close()
%>