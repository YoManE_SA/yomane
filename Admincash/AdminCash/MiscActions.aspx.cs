﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

public partial class AdminCash_MiscActions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		string action = Request["action"];
		string p1 = Request["p1"];
		string p2 = Request["P2"];
		string redirectUrl = Request["Ref"];
		string result = null; 
		switch(action) {
			case "createMerchant":
				var obj = new Netpay.Bll.Merchants.Merchant();
				obj.Name = "New Company " + DateTime.Now.Ticks;
				obj.Save();
				result = obj.ID.ToString();
				break;
			case "resetPasswordLock":
				Netpay.Infrastructure.Security.Login.ResetLock(p1.ToNullableInt().GetValueOrDefault(), Request.UserHostAddress);
				result = "Success";
				break;
			default: 
				result = "not supported action"; 
				break;
		}
		if (!string.IsNullOrEmpty(redirectUrl))
			Response.Redirect(redirectUrl + result, true);
		Response.Write(result);
    }
}