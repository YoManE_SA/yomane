<%@ Page Language="VB" EnableEventValidation="false" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<script runat="server" type="text/VB">
    Dim sSQL As String, AffiliateID As Integer
    Dim nOldViewPM As Integer, nOldCur As Integer, nOldTransType As Integer
    Private Sub Page_Load()
        DivBox1.Text = DivBox1.Text.Replace("netpay", dbPages.TestVar(HttpContext.Current.Session("Identity"), -1, ""))
        AffiliateID = dbPages.TestVar(Request("id"), 0, -1, 0)
        nOldViewPM = nOldCur = nOldTransType = -1
        Security.CheckPermission(lblPermissions)
        If Not IsPostBack Then
            ltAffiliateName.Text = dbPages.ExecScalar("Select name From tblAffiliates Where affiliates_id=" & AffiliateID)
            dbPages.LoadListBox(ddPaymentMethod, "Select Name AS pm_Name, PaymentMethod_id AS ID From List.PaymentMethod Where PaymentMethod_id > " & ePaymentMethod.PMD_MAXINTERNAL, Nothing)
            dbPages.LoadListBox(ddCurrency, "Select CUR_ISOName, CUR_ID From tblSystemCurrencies", Nothing)
            dbPages.LoadListBox(ddCompanies, "Select left(CompanyName,25), ID From tblCompany Left Join [Setting].[SetMerchantAffiliate] sma ON(sma.Merchant_id = tblCompany.ID) Where sma.Affiliate_id=" & AffiliateID, Nothing)
            ddCompanies.Items.Insert(0, New ListItem("[All]", 0))
            ddPaymentMethod.Items.Insert(0, New ListItem("[All]", 0))
            ddTransType.Items.Add(New ListItem("Debit", CInt(eAFTransType.eAF_Debit))) : ddTransType.Items.Add(New ListItem("Authorize", CInt(eAFTransType.eAF_Authorize))) : ddTransType.Items.Add(New ListItem("Refund", CInt(eAFTransType.eAF_Refund)))
            If ddCompanies.Items.Count > 0 Then ddCompanies.SelectedIndex = 0
            tlbTop.LoadPartnerTabs(AffiliateID)
        End If
        Dim companyId = dbPages.TestVar(ddCompanies.SelectedValue, 0, -1, 0)
        sSQL = "Select tblAffiliateFeeSteps.*, Name AS pm_Name, CUR_ISOName, CUR_Symbol" & _
          " From tblAffiliateFeeSteps" & _
          " Left Join List.PaymentMethod AS pm ON (pm.PaymentMethod_id = tblAffiliateFeeSteps.AFS_PaymentMethod)" & _
          " Left Join tblSystemCurrencies ON(tblSystemCurrencies.CUR_ID = tblAffiliateFeeSteps.AFS_Currency)" & _
          " Where AFS_AffiliateID=" & AffiliateID & " And AFS_CompanyID" & IIf(companyId <> 0, "=" & companyId, " Is Null") & " And AFS_AFPID Is Null Order By AFS_PaymentMethod Asc, AFS_TransType Asc, AFS_Currency Asc, AFS_UpToAmount Asc"
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If dbPages.TestVar(ddCompanies.SelectedValue, 0, -1, -1) = -1 Then
            lblError.Text = "Error - you must first select an attached company."
            Exit Sub
        End If
        Dim companyId = dbPages.TestVar(ddCompanies.SelectedValue, 0, -1, 0)
        dbPages.ExecSql("Insert Into tblAffiliateFeeSteps(AFS_AffiliateID, AFS_CompanyID, AFS_PaymentMethod, AFS_Currency, AFS_UpToAmount, AFS_SlicePercent, AFS_TransType, AFS_PercentFee)Values(" & _
          AffiliateID & _
          "," & IIF(companyId <> 0, companyId, "Null") & _
          "," & dbPages.TestVar(ddPaymentMethod.SelectedValue, 0, -1, 0) & _
          "," & dbPages.TestVar(ddCurrency.SelectedValue, 0, -1, 0) & _
          "," & dbPages.TestVar(txAmount.Text, 0D, -1D, 0D) & _
          "," & dbPages.TestVar(txtSlicePercent.Text, 0D, -1D, 0D) & _
          "," & dbPages.TestVar(ddTransType.SelectedValue, 0, -1, 0) & _
          "," & 0 & ")")
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim nCalcMethod As Integer = 0
        Dim nSlicePercent As Decimal = 0
        For Each Item As RepeaterItem In rList.Items
            nCalcMethod = dbPages.TestVar(CType(Item.FindControl("ddlCalcMethod"), DropDownList).SelectedValue, 0, -1, nCalcMethod)
            nSlicePercent = dbPages.TestVar(CType(Item.FindControl("txtSlicePercent"), TextBox).Text, 0D, -1D, nSlicePercent)
            dbPages.ExecSql("Update tblAffiliateFeeSteps Set" & _
              " AFS_PercentFee=" & dbPages.TestVar(CType(Item.FindControl("txFee"), TextBox).Text, 0D, -1D, 0D) & _
              ", AFS_SlicePercent=" & nSlicePercent & _
              ", AFS_CalcMethod=" & nCalcMethod & _
              " Where AFS_ID=" & dbPages.TestVar(CType(Item.FindControl("AFS_ID"), HiddenField).Value, 0, -1, 0))
        Next
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        dbPages.ExecSql("Delete From tblAffiliateFeeSteps Where AFS_ID=" & dbPages.TestVar(e.CommandArgument, 0, -1, 0))
    End Sub

    Private Sub rList_OnItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim nPMValue As Integer = dbPages.TestVar(CType(e.Item.FindControl("ltPaymentMethod"), Literal).Text, 0, -1, 0)
        Dim nCurValue As Integer = dbPages.TestVar(CType(e.Item.FindControl("ltCurrency"), Literal).Text, 0, -1, 0)
        Dim nTransType As Integer = dbPages.TestVar(CType(e.Item.FindControl("ltTransType"), Literal).Text, 0, -1, 0)

        e.Item.FindControl("trPMSeperator").Visible = (nOldViewPM <> nPMValue) Or (nOldCur <> nCurValue) Or (nOldTransType <> nTransType)
        e.Item.FindControl("trPMSeperatorBR").Visible = e.Item.FindControl("trPMSeperator").Visible
        CType(e.Item.FindControl("lbPaymentMethodName"), Label).Text = IIf(nPMValue <> 0, CType(e.Item.FindControl("ltPaymentMethodName"), Literal).Text, "[All]")
        CType(e.Item.FindControl("ddlCalcMethod"), DropDownList).SelectedValue = CType(e.Item.FindControl("ltCalcMethod"), Literal).Text
        CType(e.Item.FindControl("ltTransTypeName"), Literal).Text = CType(dbPages.TestVar(CType(e.Item.FindControl("ltTransType"), Literal).Text, 0, -1, 0), eAFTransType).ToString().Substring(4)

        If Not e.Item.FindControl("trPMSeperatorBR").Visible Then
            CType(e.Item.FindControl("ddlCalcMethod"), DropDownList).Items.Clear()
            'CType(e.Item.FindControl("txtSlicePercent"), TextBox).Text = ""
        End If
        nOldViewPM = nPMValue
        nOldCur = nCurValue
        nOldTransType = nTransType
    End Sub

    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        rList.DataSource = dbPages.ExecReader(sSQL)
        rList.DataBind()
        CType(rList.DataSource, System.Data.IDataReader).Close()
        MyBase.OnPreRender(e)
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Partners</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body>

<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
<tr>
	<td>
		<asp:label ID="lblPermissions" runat="server" />
		<span id="pageMainHeading">Partners Management</span> - <asp:Literal runat="server" ID="ltAffiliateName" /><br /><br />
	</td>
</tr>
<tr>
	<td>
		<custom:Toolbar ID="tlbTop" Width="100%" runat="server" />
		<form id="frmServer" runat="server">
		<table cellpadding="0" cellspacing="0">
			<tr><td class="MerchantSubHead" colspan="2">Fee Settings<br /><br /></td></tr>
			<tr>
				<td style="width:230px;" valign="top">
					 <table class="formNormal" width="100%" style="background-color:#f5f5f5; padding:10px;">
						<tr>
						 <td>
							Select Company<br />
							<asp:RadioButtonList runat="server" ID="ddCompanies" AutoPostBack="true" Width="200" Rows="1" />
						 </td>
						</tr>
					 </table>
				</td>
				<td style="width:30px;"></td>
				<td valign="top">
					 <asp:Label runat="server" id="lblError" />
					 <table class="formNormal" width="100%" border="0">
						<tr>
							<td>
								PaymentMethod<br />
								<asp:ListBox Rows="1" runat="server" ID="ddPaymentMethod" />
							</td>
							<td>
								Trans Type<br />
								<asp:ListBox Rows="1" runat="server" ID="ddTransType" Enabled="false" />
							</td>
							<td>
								Currency<br />
								<asp:ListBox Rows="1" runat="server" ID="ddCurrency" />
							</td>
							<td>
								Slice Total<br />
								<asp:TextBox ID="txtSlicePercent" runat="server" Width="60px" />
								<AC:DivBox ID="DivBox2" runat="server" Text="Affiliate Sale Price (%)" />
							</td>
							<td>
								Step Amount<br />
								<asp:TextBox ID="txAmount" runat="server" Width="60px" />
								<AC:DivBox ID="DivBox1" runat="server" Text="this is the upto amount for the step" />
							</td>
							<td></td>
							<td valign="bottom"><asp:Button runat="server" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" Width="60px" /></td>
							<td></td>
						</tr>
						<tr>
						 <td colspan="6"><hr /></td>
						</tr>
						<asp:Repeater runat="server" id="rList" OnItemDataBound="rList_OnItemDataBound">
							<ItemTemplate>
								<asp:HiddenField runat="server" ID="AFS_ID" Value='<%# Eval("AFS_ID") %>' />
								<tr runat="server" id="trPMSeperatorBR">
									<td colspan="6"><br /></td>
								</tr>
								<tr runat="server" id="trPMSeperator" style="background-color:#F0F0F0;" height="25">
									<td>
										<asp:Literal runat="server" ID="ltCalcMethod" Text='<%# Eval("AFS_CalcMethod") %>' Visible="false" />
										<asp:Literal runat="server" ID="ltPaymentMethod" Text='<%# Eval("AFS_PaymentMethod") %>' Visible="false" />
										<asp:Literal runat="server" ID="ltPaymentMethodName" Text='<%# Eval("pm_Name") %>' Visible="false" />
										<img width="23" height="15" align="middle" border="0" src="/NPCommon/ImgPaymentMethod/23X12/<%# Eval("AFS_PaymentMethod")%>.gif" />
										<asp:Label runat="server" ID="lbPaymentMethodName" />
									</td>
							        <td>
										<asp:Literal runat="server" ID="ltTransType" Text='<%# Eval("AFS_TransType") %>' Visible="false" />
										<asp:Literal runat="server" ID="ltTransTypeName" />
							        </td>
									<td>
										<asp:Literal runat="server" ID="ltCurrency" Text='<%# Eval("AFS_Currency") %>' Visible="false" />
										<asp:Label runat="server" ID="lbCurrencyName" Text='<%# Eval("CUR_ISOName") %>' />
										<asp:Label runat="server" ID="Label1" Text='<%# Eval("CUR_Symbol") %>' />
									</td>
									<td></td>
									<td colspan="2">
										<asp:DropDownList runat="server" ID="ddlCalcMethod" >
											<asp:ListItem Text="From Volume" Value="0" />
											<asp:ListItem Text="From Fees" Value="1" />
											<asp:ListItem Text="MerchantSale - TerminalBuy" Value="2" />
											<asp:ListItem Text="MerchantSale - AffiliateBuy" Value="3" />
											<asp:ListItem Text="MerchantSale - AffiliateBuy (Step %)" Value="5" />
										</asp:DropDownList>
										<AC:DivBox ID="DivBox1" runat="server" >
											1. From Volume : Calculate affiliate fees from the total volume minus refunds & CHB's. <br /><br />
											2. From Fees : Calculate affiliate fees from the fees netpay took from the merchant. <br /><br />
											3. (MerchantSale - TerminalBuy) : Calculate affiliate fees from the fees netpay took from the merchant minus the fees the bank otook from us.<br /><br />
											4. (MerchantSale - AffiliateBuy) : Calculate affiliate fees from the fees netpay took from the merchant minus affiliate buy price (in the row).<br /><br />
										</AC:DivBox>
									</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<asp:TextBox runat="server" ID="txtSlicePercent" Text='<%# dbPages.TestVar(Eval("AFS_SlicePercent"), 0d, -1d, 0d).ToString("0.00") %>' Width="60px" style="dir:ltr;text-align:right;" />
										<AC:DivBox ID="DivBox2" runat="server" Text="Split Percent (%)" />
									</td>
									<td><asp:Label runat="server" ID="lbAmount" Text='<%# CType(Eval("AFS_UpToAmount"), Decimal).ToString("0.000") %>' /></td>
									<td><asp:TextBox runat="server" ID="txFee" Text='<%# CType(Eval("AFS_PercentFee"), Decimal).ToString("0.000") %>' Width="60px" style="text-align:right;" />%</td>
									<td><asp:Button runat="server" ID="btnDelete" Text="Delete" OnCommand="btnDelete_Click" CommandArgument='<%# Eval("AFS_ID") %>' Width="60px" /></td>
								</tr>
							</ItemTemplate>
						</asp:Repeater>
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td colspan="6"></td>
							<td><asp:Button runat="server" ID="btnUpdate" Text="Update" OnClick="btnUpdate_Click" Width="60px" /></td>
						</tr>
					 </table>
				</td>
			</tr>
		</table>
		</form>
	</td>
</tr>
</table>	
</body>
</html>