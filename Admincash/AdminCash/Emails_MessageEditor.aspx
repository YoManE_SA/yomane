﻿<%@ Page Language="C#" AutoEventWireup="true" validaterequest="false" CodeFile="Emails_MessageEditor.aspx.cs" Inherits="Emails_MessageEditor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reply</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../js/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js" type="text/javascript"></script>

<script type="text/javascript">
	function formSubmit() {
		var src = $('#ifrmBody').contents().find("body").html();
		$('#hf_txtBody').val(src);
	}
</script>
<style type="text/css">
.sreg{background-color:#fafafa;}
.error{color:Red;}
</style>
</head>
<body style="padding: 0px 30px 0px 18px;">
    <form id="form_" runat="server" onsubmit="formSubmit()">
        <asp:hiddenfield id="hf_txtBody" clientidmode="Static" runat="server" value="" />
		<div>
			<h1 style="background-color: rgb(238,238,238); padding: 5px 0px;">
					<span style="color: #2566ab; font-weight: bold;">Emails - </span>
					<span style="color: #000000;">MESSAGE EDITOR</span>
					<span style="float:right;">
						<asp:button id="btn_backToPost" cssclass="buttonWhite" style="width:120px;" tooltip="" text="Back To The Post" runat="server" OnClick="BackToPost_Click" />
					</span>
			</h1>
		    <asp:panel ID="panReply" runat="server"  CssClass="panel">
			    <table cellpadding="0" cellspacing="0" border="0" style="text-align:left;" width="100%">
				    <tr>
					    <td><b>From</b></td>
					    <td style="padding:2px 0px;">
						    <asp:RadioButtonList ID="rblFrom" RepeatColumns="3" RepeatLayout="Flow" runat="server" />
						    <asp:TextBox ID="txtFrom" CssClass="text" runat="server" />
					    </td>
				    </tr>
				    <tr>
					    <td><b>To</b></td>
					    <td style="padding:2px 0px;">
						    <asp:RadioButtonList ID="rblTo" RepeatColumns="3" RepeatLayout="Flow" runat="server" />
						    <asp:TextBox ID="txtTo" CssClass="text" runat="server" />
					    </td>
				    </tr>
				    <tr>
					    <td><b>CC</b></td>
					    <td style="padding:2px 0px;">
						    <asp:RadioButtonList ID="rblCC" RepeatColumns="3" RepeatLayout="Flow" runat="server" />
						    <asp:TextBox ID="txtCC" CssClass="text" runat="server" />
					    </td>
				    </tr>
				    <tr>
					    <td><b>Subject</b></td>
					    <td style="padding:2px 0px;"><asp:TextBox ID="txtSubject" CssClass="wide" style='width:550px;' runat="server" /></td>
				    </tr>
				    <tr>
					    <td valign="top"><b>Message</b></td>
					    <td>
                            <%--<iframe runat="server" id="ifrmBody" style="width:600px; overflow:scroll; height:250px; margin:5px 0px 5px 0px; padding:10px; border:1px solid #000;" />--%>
							<iframe frameborder="0" runat="server" id="ifrmBody" style="height: 450px; width: 100%; margin: 0; border: solid 1px #cccccc;" contentEditable="true" clientidmode="Static"></iframe>
                        </td>
				    </tr>
					<tr>
						<td>
							<span>Attachment</span>
							<asp:regularexpressionvalidator 
							id="REV_fu_atachment" 
							runat="server" 
							errormessage="*" 
							validationgroup="file_Upload"
							validationexpression ="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.PNG|.png|.doc|.DOC|.docx|.DOCX|.pdf|.PDF|.txt|.TXT|.bmp|.BMP|.jpg|.JPG|.zip|.ZIP|.rar|.RAR|.xlsx|.XLSX|.xls|.XLS|.ppt|.PPT|.pptx|.PPTX)$" 
							controltovalidate="fu_atachment"> 
							</asp:regularexpressionvalidator> 
						</td>
						<td style="padding:2px 0px;">
							<asp:fileupload id="fu_atachment" clientidmode="Static" runat="server" width="250" />
						</td>
					</tr>
                    <tr>
                        <td>Answer from template</td>
                        <td style="padding:2px 0px;">
							<asp:DropDownList id="lb_ReplyTemplete" style="width:400px" clientidmode="Static" runat="server" OnSelectedIndexChanged="TemplateChanged" AutoPostBack="true" />
						</td>
                    </tr>
					<tr><td><br /></td></tr>
				    <tr>
					    <td></td>
					    <td style="padding:2px 0px;">
						    <asp:Button id="btnSend" OnClick="SendClick" validationgroup="file_Upload" Text=" SEND " runat="server" Font-Size="12px" BackColor="#CFE7E2" BorderWidth="1px" BorderColor="Black" />
							<br />
						    <asp:Label ID="lblError" CssClass="error" Visible="false" runat="server" />
					    </td>
				    </tr>
			    </table>
			    <script language="javascript" type="text/javascript">
			    	for (var i = 0; i < document.all.length; i++) if (document.all[i].innerHTML == "Other:") document.all[i].style.width = "auto";
			    </script>
		    </asp:panel>
		</div>

	</form>
</body>
</html>
