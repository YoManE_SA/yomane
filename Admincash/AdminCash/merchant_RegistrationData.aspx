﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminCash/ContentPage.master" AutoEventWireup="true" CodeFile="merchant_RegistrationData.aspx.cs" Inherits="AdminCash_merchant_RegistrationData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" Runat="Server">
	<style>		
		input, select { width:98%; border:1px solid silver; padding:2px; }
		table.formNormal td { padding:4px;  }
		table.formNormal th { vertical-align:middle; }
	</style>
	<asp:Label ID="lblPermissions" runat="server" /> <span id="pageMainHeading">Merchant Registration</span> - DATA<br /><br />
	<div runat="server" id="divResultMessage"></div>
	<table width="100%" class="formNormal" >
		<tr>
			<td colspan="2">
				<table width="100%" border="1" cellspacing="0" cellpadding="5" style="border-collapse:collapse">
					<tr>
						<th width="25%">ID</th>
						<td width="25%"><asp:Literal runat="server" Text="<%# Item.ID %>" /></td>

						<th width="25%">Merchant ID</th>
						<td width="25%"><asp:Literal runat="server" ID="ltMerchantID" Text="<%# Item.MerchantID %>" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="50%" valign="top">
				<table width="100%" border="1" cellspacing="0" cellpadding="5" style="border-collapse:collapse">
					<tr>
						<th width="40%">First name</th>
						<td><asp:TextBox runat="server" ID="txtFirstName" Text="<%# Item.FirstName %>" /></td>
					</tr><tr>
						<th>Last name</th>
						<td><asp:TextBox runat="server" ID="txtLastName" Text="<%# Item.LastName %>" /></td>
					</tr><tr>
						<th>Email</th>
						<td><asp:TextBox runat="server" ID="txtEmail" Text="<%# Item.Email %>" /></td>
					</tr><tr>
						<th>Address</th>
						<td><asp:TextBox runat="server" ID="txtAddress" Text="<%# Item.Address %>" /></td>
					</tr><tr>
						<th>City</th>
						<td><asp:TextBox runat="server" ID="txtCity" Text="<%# Item.City %>" /></td>
					</tr><tr>
						<th>Province</th>
						<td><netpay:StateDropDown runat="server" ID="ddlState" EnableBlankSelection="false" CountryID="228" /></td>
					</tr><tr>
						<th>Zipcode</th>
						<td><asp:TextBox runat="server" ID="txtZipcode" Text="<%# Item.Zipcode %>" /></td>
					</tr><tr>
						<th>Owner dob</th>
						<td><netpay:DatePicker runat="server" ID="txtOwnerDob" Date="<%# Item.OwnerDob %>" /></td>
					</tr><tr>
						<th>Owner ssn</th>
						<td><asp:TextBox runat="server" ID="txtOwnerSsn" Text="<%# Item.OwnerSsn %>" /></td>
					</tr><tr>
						<th>Phone</th>
						<td><asp:TextBox runat="server" ID="txtPhone" Text="<%# Item.Phone %>" /></td>
					</tr><tr>
						<th>Fax</th>
						<td><asp:TextBox runat="server" ID="txtFax" Text="<%# Item.Fax %>" /></td>
					</tr><tr>
						<th>Url</th>
						<td><asp:TextBox runat="server" ID="txtUrl" Text="<%# Item.Url %>" /></td>
					</tr><tr>
						<td colspan="2"><br /></td>
					</tr><tr>
						<th>Phisical address</th>
						<td><asp:TextBox runat="server" ID="txtPhisicalAddress" Text="<%# Item.PhisicalAddress %>" /></td>
					</tr><tr>
						<th>Phisical city</th>
						<td><asp:TextBox runat="server" ID="txtPhisicalCity" Text="<%# Item.PhisicalCity %>" /></td>
					</tr><tr>
						<th>Phisical state</th>
						<td><netpay:StateDropDown runat="server" ID="ddlPhisicalState" EnableBlankSelection="false" CountryID="228" /></td>
					</tr><tr>
						<th>Phisical zip</th>
						<td><asp:TextBox runat="server" ID="txtPhisicalZip" Text="<%# Item.PhisicalZip %>" /></td>
					</tr>
				</table>
			</td>
			<td width="50%" valign="top">
				<table width="100%"  border="1" cellspacing="0" cellpadding="5" style="border-collapse:collapse">
					<tr>
						<th width="40%">Dba name</th>
						<td><asp:TextBox runat="server" ID="txtDbaName" Text="<%# Item.DbaName %>" /></td>
					</tr><tr>
						<th>Legal business name</th>
						<td><asp:TextBox runat="server" ID="txtLegalBusinessName" Text="<%# Item.LegalBusinessName %>" /></td>
					</tr><tr>
						<th>Legal business number</th>
						<td><asp:TextBox runat="server" ID="txtLegalBusinessNumber" Text="<%# Item.LegalBusinessNumber %>" /></td>
					</tr><tr>
						<th>State of incorporation</th>
						<td><netpay:StateDropDown runat="server" ID="ddlStateOfIncorporation" EnableBlankSelection="false" CountryID="228" /></td>
					</tr><tr>
						<th>Type of business</th>
						<td><asp:DropDownList ID="ddlTypeOfBusiness" runat="server" DataValueField="key" DataTextField="value" /></td>
					</tr><tr>
						<th>Business start date</th>
						<td><netpay:DatePicker runat="server" ID="txtBusinessStartDate" Date="<%# Item.BusinessStartDate %>" /></td>
					</tr><tr>
						<th>Industry</th>
						<td><asp:DropDownList ID="ddlIndustry" runat="server" DataValueField="key" DataTextField="value" /></td>
					</tr><tr>
						<th>Business description</th>
						<td><asp:TextBox runat="server" ID="txtBusinessDescription" Text="<%# Item.BusinessDescription %>" /></td>
					</tr><tr>
						<th>Anticipated monthly volume</th>
						<td><asp:TextBox runat="server" ID="txtAnticipatedMonthlyVolume" Text="<%# Item.AnticipatedMonthlyVolume %>" /></td>
					</tr><tr>
						<th>Anticipated average transaction amount</th>
						<td><asp:TextBox runat="server" ID="txtAnticipatedAverageTransactionAmount" Text="<%# Item.AnticipatedAverageTransactionAmount %>" /></td>
					</tr><tr>
						<th>Anticipated largest transaction amount</th>
						<td><asp:TextBox runat="server" ID="txtAnticipatedLargestTransactionAmount" Text="<%# Item.AnticipatedLargestTransactionAmount %>" /></td>
					</tr><tr>
						<th>% of Goods Delivered in 0 to 7 days</th>
						<td><asp:TextBox runat="server" ID="txtPercentDelivery0to7" Text="<%# Item.PercentDelivery0to7 %>" /></td>
					</tr><tr>
						<th>% of Goods Delivered in 15 to 30 days</th>
						<td><asp:TextBox runat="server" ID="txtPercentDelivery15to30" Text="<%# Item.PercentDelivery15to30 %>" /></td>
					</tr><tr>
						<th>% of Goods Delivered in 8 to 14 days</th>
						<td><asp:TextBox runat="server" ID="txtPercentDelivery8to14" Text="<%# Item.PercentDelivery8to14 %>" /></td>
					</tr><tr>
						<th>% of Goods Delivered over 30 days</th>
						<td><asp:TextBox runat="server" ID="txtPercentDeliveryOver30" Text="<%# Item.PercentDeliveryOver30 %>" /></td>
					</tr><tr>
						<td colspan="2"><br /></td>
					</tr><tr>
						<th>Bank routing number</th>
						<td><asp:TextBox runat="server" ID="txtBankRoutingNumber" Text="<%# Item.BankRoutingNumber %>" /></td>
					</tr><tr>
						<th>Bank account number</th>
						<td><asp:TextBox runat="server" ID="txtBankAccountNumber" Text="<%# Item.BankAccountNumber %>" /></td>
					</tr>
				</table>
			</td>
		</tr><tr>
			<td colspan="2">
				<table border="1" cellspacing="0" cellpadding="5" style="border-collapse:collapse">
					<asp:Repeater runat="server" ID="rptIntegrationData" DataSource='<%# Item.IntegrationData %>'>
						<ItemTemplate>
							<tr><th width="50%"><%# Eval("Key") %></th><td><%# Eval("Value") %></td></tr>
						</ItemTemplate>
					</asp:Repeater>
				</table>
			</td>
		</tr>
	</table>
	<hr />
	<asp:Button ID="btnSave" OnCommand="Save_Command" Text="Save" runat="server" style="width:180px" />
	<asp:Button ID="btnRegisterAtNetpay" OnCommand="RegisterLocally_Command" Text="Register at Netpay" runat="server" style="width:180px" Enabled="<%# Item.MerchantID == null %>" />
	<asp:Button ID="btnRegisterAtCardConnect" OnCommand="RegisterAtCardConnect_Command" Text="Register at CardConnect" runat="server" style="width:180px" Enabled='<%# !Item.IntegrationData.ContainsKey("CardConnectID") %>' />
	<asp:Button ID="btnRegisterAtZoho" OnCommand="RegisterAtZoho_Command" Text="Register at Zoho" runat="server" style="width:180px" />
	<asp:Button ID="btnDelete" OnCommand="Delete_Command" Text="Delete" runat="server" style="width:180px" OnClientClick="return confirm('Are you sure?')" />
</asp:Content>

