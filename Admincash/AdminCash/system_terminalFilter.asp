<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_htmlInputs.asp"-->
<!--#include file="../include/func_security.asp" -->
<%
sFrom = trim(request("From"))
sTextSearch = trim(request("iTextSearch"))
sDebitCompanyID = trim(request("iDebitCompanyID"))
sStatus = trim(request("iStatus"))
sSortType = trim(request("iSortType"))

Response.Cookies("DebitTerminalSearch")("textValue")=sTextSearch
Response.Cookies("DebitTerminalSearch")("statusValue")=sStatus
Response.Cookies("DebitTerminalSearch")("sortTypeValue")=sSortType

if trim(request("Action"))="addNew" then	
	sSQL="INSERT INTO tblDebitTerminals(DebitCompany, dt_name,terminalNumber, isActive) Values " &_
	"(" & sDebitCompanyID & ", '" & date() & "', '1', 0)"	
oledbData.execute sSQL
end if

If sFrom = "Filter" Then
	sWhere = ""
	if sTextSearch<>"" then
		sWhere = sWhere & " AND (tblDebitTerminals.dt_name LIKE '" & sTextSearch & "%'"
		sWhere = sWhere & " OR tblDebitTerminals.terminalNumber LIKE '" & sTextSearch & "%'"
		sWhere = sWhere & " OR tblDebitTerminals.dt_ContractNumber LIKE '" & sTextSearch & "%')"
	end if
	if sDebitCompanyID<>"" then sWhere = sWhere & " AND tblDebitTerminals.DebitCompany=" & sDebitCompanyID
	if sStatus<>"" then sWhere = sWhere & " AND tblDebitTerminals.isActive=" & sStatus
	If PageSecurity.IsLimitedMerchant Then sWhere = sWhere & " AND tblDebitTerminals.DebitCompany IN (SELECT ID FROM GetPermittedDebitCompanies('" & PageSecurity.Username & "'))"

	sOrderBy = ""
	select case sSortType
		case "TerminalNumberdesc"
			sOrderBy = "tblDebitTerminals.terminalNumber desc"
		case "TerminalNumberasc"
			sOrderBy = "tblDebitTerminals.terminalNumber asc"
		case "TerminalNamedesc"
			sOrderBy = "tblDebitTerminals.dt_name desc"
		case "TerminalNameasc"
			sOrderBy = "tblDebitTerminals.dt_name asc"
		case "DebitCompanyIDdesc"
			sOrderBy = "tblDebitTerminals.DebitCompany desc, tblDebitTerminals.dt_name asc"
		case "DebitCompanyIDasc"
			sOrderBy = "tblDebitTerminals.DebitCompany asc, tblDebitTerminals.dt_name asc"
		case else
			sOrderBy = "tblDebitTerminals.dt_name asc"
	end select

	If sWhere<>"" Then sWhere = " WHERE " & mid(sWhere, 5)
End if
%>
<html>
<head>
	<title>Terminal List</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/styleAdminEng.css" rel="stylesheet" type="text/css" />
	<script language="JavaScript" src="../js/func_common.js"></script>
	<script language="JavaScript1.2">
		function showSection(SecNum) {
			for (i = 0; i < tblSec.length; i++) {
				if(i==SecNum) {
					tblSec[SecNum].style.display='';
					tdButton[SecNum].style.backgroundColor ='silver';
				}
				else {
					tblSec[i].style.display='none';
					tdButton[i].style.backgroundColor ='#eeeeee';
				}
			}
		}
	</script>
	<script language="javascript" type="text/javascript">
		function ShowHideMenu(oDiv) {
			oDiv.style.display = (oDiv.style.display == "none" ? "" : "none");
		}
	</script>
	<style type="text/css">
		div.merchantMenu { width:120px;position:absolute;z-index:1;border:1px solid #999999;background-color:WhiteSmoke;padding:2px; }
		div.merchantMenu span { float:right;border:1px solid Black;background-color:Silver;padding:2px;line-height:1em;font-size:11px;font-weight:bold; }
		div.merchantMenu a { text-decoration:none;display:block;color:#505050; }
		div.merchantMenu a:hover { text-decoration:underline;display:block;color:Black; }
	</style>
</head>
<body class="itextm3" bgcolor="#eeeeee">
	
	<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr>
		<td id="pageMainHeadingTd"><span id="pageMainHeading">TERMINALS</span></td>
		<td></td>
		<td>
			<table border="0" cellspacing="1" cellpadding="1" align="right">
			<tr>
				<td width="6" bgcolor="#66cc66"></td>
				<td width="38">Active</td>
				<td width="6" bgcolor="#ff6666"></td>
				<td width="38">Inactive</td>
			</tr>
			</table>	
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<br />
			<%
			pageSecurityLevel = 0
			pageSecurityLang = ""
			pageClosingTags = "</td></tr></table>"
			Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
			%>
		</td>
	</tr>
	</table>
	<form action="system_terminalFilter.asp" method="post" name="frmTerminalOrder" id="frmTerminalOrder">
	<input type="Hidden" name="from" value="Filter">
	<input type="Hidden" name="iSortType" value="<%= sSortType %>">
	<table class="filterNormal" width="95%" align="center" style="border:1px solid gray;">
	<tr>
		<th>Debit Company<br /></th>
		<th>Free Text<br /></th>
		<th>Status<br /></th>
		<td></td>
	</tr>
	<tr>	
		<td>
			<%
			sSQL = "SELECT DebitCompany_ID, dc_Name FROM tblDebitCompany"
			If PageSecurity.IsLimitedMerchant Then sSQL = sSQL & " WHERE DebitCompany_ID IN (SELECT ID FROM GetPermittedDebitCompanies('" & PageSecurity.Username & "'))"
			sSQL = sSQL & " ORDER BY DebitCompany_ID"
			set rsDataTmp = oledbData.Execute(sSQL)
			If not rsDataTmp.EOF then PutRecordsetCombo rsDataTmp,"iDebitCompanyID","style=""width:110px;font-size:11px;""","debitCompany_id","dc_name","","", sDebitCompanyID
			rsDataTmp.close
			set rsDataTmp = nothing
			%><br />
		</td>
		<td><input type="Text" name="iTextSearch" size="12" value="<%= sTextSearch %>"><br /></td>
		<td>
			<select name="iStatus" style="width:70px;">
				<option value="" <%if sStatus="" then%>selected<%End If%>></option>
				<option value="1" <%if sStatus="1" then%>selected<%End If%>>Active</option>
				<option value="0" <%if sStatus="0" then%>selected<%End If%>>Inactive</option>
			</select>
		</td>
		<td align="left"><input type="submit" name="Action" value=" GO "></td>
	</tr>
	</table>
	</form>
	<%
	If sFrom = "Filter" Then
		%>
		<br />
		<form action="system_terminalFilter.asp" method="post">
		<input type="Hidden" name="Action" value="addNew">
		<input type="Hidden" name="from" value="Filter">
		<input type="Hidden" name="iSortType" value="<%= trim(sSortType) %>">
		<!--<input type="Hidden" name="iStatus" value="0">-->
		<input type="Hidden" name="iDebitCompanyID" value="<%= trim(sDebitCompanyID) %>">
		<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
		<%if isnumeric(""& sDebitCompanyID) then%>
		<tr>
			<td><input type="submit" style="width:100px;" value="ADD NEW"><br /></td>
		</tr>
		<%end if %>
		<tr><td colspan="4"></td></tr>
		</table>
		</form>
		<div align="center" id="waitMsg">
			<br />
			<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="0">
					<tr><td class="txt12" style="color: gray;">Loading terminals &nbsp;-&nbsp; Please wait<br /></td></tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td><img src="../images/loading_animation_orange.gif" style="border:1px solid silver;" alt="" width="123" height="6" border="0"><br /></td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</div>
		<% Response.Flush %>
		<table class="formNormal" align="center" width="95%" border="0">
		<tr>
			<th class="dark" colspan="2"><br /></th>
			<th class="dark">Name<br /></th>
			<th class="dark">Number<br /></th>
			<th class="dark">Debit Company<br /></th>
		</tr>
		<%	
		Set fsServer=Server.CreateObject("Scripting.FileSystemObject")
		sSQL="SELECT tblDebitTerminals.id, tblDebitTerminals.dt_name, tblDebitTerminals.DebitCompany, tblDebitTerminals.terminalNumber, tblDebitTerminals.isActive, tblDebitCompany.dc_Name AS DebitCompanyName " &_
		"FROM tblDebitTerminals INNER JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany = tblDebitCompany.debitCompany_id " & sWhere & " ORDER BY " & sOrderBy
		set rsData = oledbData.execute(sSQL)
		If NOT rsData.EOF Then
			%>
			<tr>
				<td></td>
				<td></td>
				<td valign="middle">
					<a href="#" onclick="frmTerminalOrder.iSortType.value='TerminalNamedesc';frmTerminalOrder.submit();" class="submenu2"><img src="../images/arrowTreeU.gif" alt="" border="0"></a>
					<a href="#" onclick="frmTerminalOrder.iSortType.value='TerminalNameasc';frmTerminalOrder.submit();" class="submenu2"><img src="../images/arrowTreeD.gif" alt="" border="0"></a>
				</td>
				<td valign="middle">
					<a href="#" onclick="frmTerminalOrder.iSortType.value='TerminalNumberdesc';frmTerminalOrder.submit();" class="submenu2"><img src="../images/arrowTreeU.gif" alt="" border="0"></a>
					<a href="#" onclick="frmTerminalOrder.iSortType.value='TerminalNumberasc';frmTerminalOrder.submit();" class="submenu2"><img src="../images/arrowTreeD.gif" alt="" border="0"></a>
				</td>
				<td valign="middle">
					<a href="#" onclick="frmTerminalOrder.iSortType.value='DebitCompanyIDdesc';frmTerminalOrder.submit();" class="submenu2"><img src="../images/arrowTreeU.gif" alt="" border="0"></a>
					<a href="#" onclick="frmTerminalOrder.iSortType.value='DebitCompanyIDasc';frmTerminalOrder.submit();" class="submenu2"><img src="../images/arrowTreeD.gif" alt="" border="0"></a>
				</td>
			</tr>
			<tr><td height="6"></td></tr>			
			<%
			Do until rsData.EOF
				sColor="#66cc66"
				If NOT rsData("isActive") Then sColor="#ff6666"
				%>
				<tr><td height="1" colspan="10"  bgcolor="silver"></td></tr>
				<tr>
					<td width="2" bgcolor="<%= sColor %>"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td>
					<td width="2"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td>
					<td valign="top">
						<span style="float:right;"
						onmouseover="if (div<%= rsData("ID") %>.style.display=='none') {event.cancelBubble=true;bHideMenu<%= rsData("ID") %>=false;ShowHideMenu(div<%= rsData("ID") %>);}return false;"
						onmouseout="event.cancelBubble=true;bHideMenu<%= rsData("ID") %>=true;return false;"><img src="/NPCommon/Images/icon_ArrowDown.gif" align="middle" /></span>
						<a href="system_terminalTermDetail.aspx?terminalID=<%= rsData("id") %>" title="<%= dbtextShow(rsData("dt_name")) %>" target="frmBody">
						<%
						If trim(rsData("dt_name"))<>"" Then
							If int(len(dbtextShow(rsData("dt_name"))))<17 Then
								Response.Write(dbtextShow(rsData("dt_name")))
							Else
								Response.Write(left(dbtextShow(rsData("dt_name")),16) & "..")
							End if
						Else
							Response.Write("[ empty ]")
						End if
						%></a><br />											
						<div class="merchantMenu" id="div<%= rsData("ID") %>" style="display:none;" onmouseover="bHideMenu<%= rsData("ID") %>=false;" onmouseout="bHideMenu<%= rsData("ID") %>=true;">
							<a onmouseover="bHideMenu<%= rsData("ID") %>=false;" target="frmBody" onclick="event.cancelBubble=true;ShowHideMenu(div<%= rsData("ID") %>);" href="system_terminalTermDetail.aspx?terminalID=<%= rsData("ID") %>">Terminal Details</a>
							<a onmouseover="bHideMenu<%= rsData("ID") %>=false;" target="frmBody" onclick="event.cancelBubble=true;ShowHideMenu(div<%= rsData("ID") %>);" href="system_terminalCardDetail.aspx?terminalID=<%= rsData("ID") %>">Card Company Details</a>
							<a onmouseover="bHideMenu<%= rsData("ID") %>=false;" target="frmBody" onclick="event.cancelBubble=true;ShowHideMenu(div<%= rsData("ID") %>);" href="system_terminalFees.aspx?terminalID=<%= rsData("ID") %>">Fees & Payouts</a>
							<a onmouseover="bHideMenu<%= rsData("ID") %>=false;" target="frmBody" onclick="event.cancelBubble=true;ShowHideMenu(div<%= rsData("ID") %>);" href="system_terminalRules.aspx?TerminalID=<%= rsData("ID") %>">Notifications</a>
							<a onmouseover="bHideMenu<%= rsData("ID") %>=false;" target="frmBody" onclick="event.cancelBubble=true;ShowHideMenu(div<%= rsData("ID") %>);" href="system_terminalMisc.aspx?terminalID=<%= rsData("ID") %>">Miscellaneous</a>
						</div>
						<script language="javascript" type="text/javascript">
							var bHideMenu<%= rsData("ID") %>=false;
							setInterval("if (bHideMenu<%= rsData("ID") %>&&(div<%= rsData("ID") %>.style.display!='none')) ShowHideMenu(div<%= rsData("ID") %>);", 1000);
						</script>						
					</td>
					<td valign="top" id="TerminalNameCheck_<%= rsData("id") %>"><%= rsData("terminalNumber") %><br /></td>
					<td valign="top">
						<%
						If trim(rsData("DebitCompany"))<>"" Then
							If fsServer.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & trim(rsData("DebitCompany")) & ".gif")) Then
								Response.write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(rsData("DebitCompany")) & ".gif"" align=""middle"" border=""0"" />")
							End if
						End if
						Response.Write(" " & rsData("DebitCompanyName"))
						%><br />
					</td>
				</tr> 
				<%
			rsData.movenext
			Loop
			%>
			<tr><td><br /></td></tr>
			<%
		Else
			%>
			<tr><td height="5"></td></tr>
			<tr><td colspan="5" class="txt12">No records found !<br /></td></tr>
			<tr><td><br /></td></tr>
			<%
		End If
		%>
		</table>
		<script language="JavaScript">
			waitMsg.style.display = 'none';
		</script>
		<%
		rsData.close
		set rsData = nothing
		call closeConnection()
		Set fsServer=Nothing
	End if
	%>
</body>
</html>
