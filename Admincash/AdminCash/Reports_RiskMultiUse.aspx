<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
	Dim GField As String
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		GField = dbPages.TestVar(Request("GField"), -1, "email")
		PGData.PageSize = IIf(Trim(Request("iPageSize"))<>"",Request("iPageSize"),25)
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<table class="formNormal" align="center" style="width:95%;">
		<tr>
			<td colspan="2" id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> Usage Risk Report
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>
				<fieldset class="formNormal" style="font-size:12px; width:600px;">
					<legend style="color:gray;">Field to report of</legend>
					<form action="Reports_RiskMultiUse.aspx" method="get">
						<table class="formNormal" width="100%">
							<tr>
								<td><input type="radio" class="option" name="GField" value="email" <%=IIF(GField = "email", " checked", "")%> />Email</td>
								<td><input type="radio" class="option" name="GField" value="member" <%=IIF(GField = "member", " checked", "")%> />Full Name</td>
								<td><input type="radio" class="option" name="GField" value="phoneNumber" <%=IIF(GField = "phoneNumber", " checked", "")%> />Phone Number</td>
								<td><input type="radio" class="option" name="GField" value="PersonalNumber" <%=IIF(GField = "PersonalNumber", " checked", "")%> />Personal Number</td>
 								<td><input type="submit" value="SHOW" /></td>
							</tr>
						</table>
					</form>
				</fieldset>			
			</td>
		</tr>
	</table>
	<br />
	<table class="formNormal" align="center" style="width:95%;">
		<tr>
			<th>Value</th>
			<th>Count</th>
			<th>Common Blacklist</th>
		</tr> 
		<%
			Dim lIndex As Integer = 0
			Select Case GField
				Case "email" : lIndex = 1
				Case "member" : lIndex = 2
				Case "phoneNumber" : lIndex = 3
				Case "PersonalNumber" : lIndex = 4
			End Select
			Dim sSQL As String = "SELECT " & GField & ", Count(DISTINCT CCard_number256) AS eCount FROM tblCreditCard WHERE LTrim(RTrim(" & GField & "))<>''" & _
			IIf(Security.IsLimitedMerchant, " AND CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))", String.Empty) & _
			" GROUP BY " & GField & " HAVING Count(Distinct CCard_number256) > 1 ORDER BY eCount DESC"
			PGData.OpenDataset(sSQL)
			While PGData.Read()
				Response.Write("<tr onmouseover=""this.style.backgroundColor='#f0f0f0';"" onmouseout=""this.style.backgroundColor='';"">")
				Response.Write("<td>" & PGData(0) & "</td>")
				Response.Write("<td>" & PGData(1) & "</td>")
				Response.Write("<td><a class=""risk"" href=""BLCommon.aspx?New=1&BL_Type=" & lIndex & "&BL_Value=" & Server.UrlDecode(PGData(0)) & """>Add</a></td>")
				Response.Write("</tr><tr><td height=""1"" colspan=""3"" bgcolor=""silver""></td></tr>")
			End While
			PGData.CloseDataset()
		%>
	</table>
	<br />
	<table class="formNormal" align="center" style="width:95%;">
	<tr>
		<td><UC:Paging runat="Server" id="PGData" PageID="PageID" /></td>
	</tr>
	</table>

</body>
</html>

