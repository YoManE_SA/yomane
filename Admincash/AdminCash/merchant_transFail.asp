<%If trim(request("UseSQL2"))="YES" Then Session("CurrentDSN") = "2" Else Session("CurrentDSN") = "1"%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_adoPaging.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_controls.asp"-->
<!--#include file="../include/func_transPay.asp"-->
<!--#include file="../include/func_security.asp" -->
<%
nCompanyID = request("CompanyID")
If trim(nCompanyID)<>"" Then
	'keep record of clicks for each company
	If trim(request("isClickCount"))="true" Then 
		sSQL="UPDATE tblCompany SET CountAdminView=CountAdminView+1 WHERE id=" & nCompanyID
		oledbData.Execute sSQL
	End if
	sCompanyName = dbtextShow(ExecScalar("SELECT CompanyName FROM tblCompany WHERE ID=" & nCompanyID, ""))
End if
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<script language="JavaScript" src="../js/func_common.js"></script>
	<script language="JavaScript">
		function toggleInfo(TransId,PaymentMethod,bkgColor)
		{
			objRef = document.getElementById('Row' + TransId)
			if ((objRef.innerHTML == ""))
			{
				UrlAddress=(((PaymentMethod >= <%= PMD_EC_MIN %>) && (PaymentMethod <= <%= PMD_EC_MAX %>)) ? "trans_detail_eCheck.asp" : "trans_detail_cc.asp");
				objRef.innerHTML='<iframe framespacing="0" scrolling="no" marginwidth="0" frameborder="0" width="100%" height="0px" src="'+UrlAddress+'?transID='+TransId+'&PaymentMethod='+PaymentMethod+'&bkgColor='+bkgColor+'&TransTableType=fail" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>';
				objRef.style.display="";
			}
			else
			{
				objRef.style.display=(objRef.style.display=="none" ? "" : "none");
			}
		}
		function ExpandNode(fTrNum, TransId, PaymentMethod, bkgColor)
		{
			toggleInfo(TransId, PaymentMethod, bkgColor);
			var imgPlus = document.getElementById("oListImg" + fTrNum);
			imgPlus.src = (imgPlus.src.indexOf("tree_expand.gif") > 0 ? "../images/tree_collapse.gif" : "../images/tree_expand.gif");
		}
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
<tr>
	<td id="pageMainHeadingTd">
		<span id="pageMainHeading">REJECTED TRANSACTIONS</span>
		<%If sCompanyName<>"" Then Response.Write("- <span class=""txt14"">" & dbTextShow(uCase(sCompanyName)) & "</span>")%> 
	</td>
	<td align="right">
		<table border="0" cellspacing="0" cellpadding="1" align="right">
		<tr>
			<td width="6" bgcolor="#ff6666" align="center"></td>
			<td width="2"></td>
			<td width="45">Sale</td>
			<td width="13"></td>
			<td width="6" bgcolor="#ff6666" align="center">
				<span style="background-color:#c5c5c5;"><img src="../images/1_space.gif" alt="" width="6" height="15" border="0"></span><br />
			</td>
			<td width="2"></td>
			<td width="50">Capture</td>
			<td width="13"></td>
			<td width="6" bgcolor="#ff6666" align="center">
				<span style="background-color:#ffffff;"><img src="../images/1_space.gif" alt="" width="6" height="15" border="0"></span><br />
			</td>
			<td width="2"></td>
			<td width="80">Authorization</td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<br />
<%
pageSecurityLevel = 0
pageSecurityLang = "ENG"
pageClosingTags = ""
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)

'20100627 Tamir - join charge attempt log only when filtering by autorefund status
bJoinChargeAttemptLog = false

sWhere = ""
sAnd = ""
session("PageSize") = 25
TransID = request("TransID")
If trim(request("QuickBar_TransFailID"))<>"" Then
	TransID = request("QuickBar_TransFailID")
Else
	TransID = request("TransID")
End If
If instr(TransID, ",")>0 Then
	sWhere = sWhere & " f.id IN (" & TransID & ")"
	sAnd = " AND "
ElseIf TestNumVar(TransID, 1, 0, 0)>0 Then
	sWhere = sWhere & " f.id=" & TransID
	sAnd = " AND "
Else
	If Trim(request("PageSize"))<>"" Then session("PageSize") = request("PageSize")
	bCreditCard=(Trim(Request("CardNumberFirst6")) <> "" Or Trim(Request("CardNumberLast4")) <> "")
	If trim(request("DebitCompanyID"))<>"" Then	sWhere = sWhere & sAnd & " f.DebitCompanyID=" & trim(request("DebitCompanyID")) : sAnd=" AND "
	If trim(request("CompanyID"))<>"" Then	sWhere = sWhere & sAnd & " m.ID=" & trim(request("CompanyID")) : sAnd=" AND "
	If trim(request("DebitReferenceCode"))<> "" Then sWhere = sWhere & sAnd & " DebitReferenceCode LIKE '%" & trim(request("DebitReferenceCode")) & "%'" : sAnd=" AND "
	'If trim(request("CardNumber"))<> "" Then sWhere = sWhere & sAnd & " Replace(dbo.GetDecrypted256(tblCreditCard.CCard_Number256), ' ', '') LIKE Replace('%" & request("CardNumber") & "%', ' ', '')" : sAnd=" AND "
	If trim(request("CardNumberFirst6"))<> "" Then sWhere = sWhere & sAnd & " Right('00000'+Cast(CCard_First6 as varchar(6)), 6) LIKE '%" & request("CardNumberFirst6") & "%'" : sAnd=" AND "
	If trim(request("CardNumberLast4"))<> "" Then sWhere = sWhere & sAnd & " Right('000'+Cast(CCard_Last4 as varchar(4)), 4) LIKE '%" & request("CardNumberLast4") & "%'" : sAnd=" AND "
	If trim(request("BINCountry"))<> "" Then sWhere = sWhere & sAnd & " BINCountry='" & request("BINCountry") & "'" : sAnd=" AND "
	If trim(request("Terminal"))<> "" Then sWhere = sWhere & sAnd & " TerminalNumber='" & DBText(request("Terminal")) & "'" : sAnd=" AND "
	If trim(request("Currency"))<> "" Then sWhere = sWhere & sAnd & " Currency=" & DBText(request("Currency")) : sAnd=" AND "
	If trim(request("AmountMin"))<> "" Then sWhere = sWhere & sAnd & " Amount>=" & TestNumVar(request("AmountMin"), 1, 0, 0) : sAnd=" AND "
	If trim(request("AmountMax"))<> "" Then sWhere = sWhere & sAnd & " Amount<=" & TestNumVar(request("AmountMax"), 1, 0, 0) : sAnd=" AND "
	If trim(request("FreeText"))<> "" Then sWhere = sWhere & sAnd & "IPAddress+OrderNumber+f.Comment+PaymentMethodDisplay+PersonalNumber+Member+phoneNumber+email+BINCountry+cc.Comment LIKE '%" & request("FreeText") & "%'" : sAnd=" AND "
	If trim(request("TransType"))="0" or trim(request("TransType"))="1" Then sWhere = sWhere & sAnd & " f.TransType=" & request("TransType") : sAnd=" AND "
	If Request("ARF_VALUE") <> "" Then
		sWhere = sWhere & sAnd & " replyCode='521' And "
		Select Case Request("ARF_VALUE")
			Case "ARF_WAIT"
				sWhere = sWhere & " (IsNull(Lca_ResponseString, '') IN ('', 'NP-INT'))"
				sAnd=" AND "
				bJoinChargeAttemptLog = true
			Case "ARF_DONE"
				sWhere = sWhere & " (Lca_ResponseString LIKE 'Refunded %')"
				sAnd=" AND "
				bJoinChargeAttemptLog = true
			Case "ARF_FAIL"
				sWhere = sWhere & " (Lca_ResponseString LIKE 'Try Refund %')"
				sAnd=" AND "
				bJoinChargeAttemptLog = true
		End Select
	Else	
		If trim(request("AnswerNumber"))<>"" Then sWhere = sWhere & sAnd & " f.replyCode = '" & trim(request("AnswerNumber")) & "'" : sAnd=" AND "
	End If
	
	sWhereDate = ""
	
	dDateMax= Trim(Request("toDate") & " " & Request("toTime"))
	if IsDate(dDateMax) then sWhereDate=sWhereDate & " f.insertDate<='" & CDate(dDateMax) & "'"
	dDateMin= Trim(Request("fromDate") & " " & Request("fromTime"))
	if IsDate(dDateMin) then sWhereDate=sWhereDate & " AND f.insertDate>='" & CDate(dDateMin) & "'"
	
	sQueryString = UrlWithout(Request.QueryString, "page")
	sQueryString = sQueryString & "&" & UrlWithout(Request.Form, "page")
End if
	
If PageSecurity.IsLimitedMerchant Then
	sWhere = sWhere & sAnd & " f.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
	sAnd = " AND "
End If
If PageSecurity.IsLimitedDebitCompany Then
	sWhere = sWhere & sAnd & " f.DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & PageSecurity.Username & "'))"
	sAnd = " AND "
End If

Set rsData = Server.CreateObject("ADODB.Recordset")
sSQL = "SELECT CompanyName, pm.Name AS 'pm_Name', ts.Name AS TransTypeName," & _
" CASE Len(cl.CountryISOCode) WHEN 2 THEN cl.CountryISOCode ELSE '--' END AS PaymentMethodCountryIso, IsNull(dc_Name, '') DebitCompanyName," & _
" ct.Name AS CreditName, BINCountry, f.*" & _
" FROM tblCompany m INNER JOIN tblCompanyTransFail f ON m.ID = f.CompanyID"
If bJoinChargeAttemptLog Then
	sSQL = sSQL & " LEFT JOIN tblLogChargeAttempts WITH (NOLOCK) ON f.ID=Lca_TransNum AND Abs(DateDiff(hour, Lca_DateStart, f.InsertDate))<=1"
End If
sSQL = sSQL & " INNER JOIN List.TransCreditType ct ON f.CreditType = ct.TransCreditType_id" & _
" INNER JOIN List.TransSource ts ON f.TransSource_id = ts.TransSource_id" & _
" INNER JOIN List.PaymentMethod AS pm ON f.PaymentMethod = pm.PaymentMethod_id" & _
" LEFT JOIN List.PaymentMethodToCountryCurrency ON pm.PaymentMethod_id = List.PaymentMethodToCountryCurrency.PaymentMethod_id" & _
" AND List.PaymentMethodToCountryCurrency.Country_id=(SELECT Min(List.PaymentMethodToCountryCurrency.Country_id) FROM List.PaymentMethodToCountryCurrency WITH (NOLOCK) WHERE List.PaymentMethodToCountryCurrency.PaymentMethod_id=pm.PaymentMethod_id)" & _
" LEFT JOIN [List].[CountryList] cl ON List.PaymentMethodToCountryCurrency.Country_id = cl.CountryID" & _
" LEFT JOIN tblDebitCompany dc ON f.DebitCompanyID=dc.Debitcompany_ID" & _
" " & IIf(bCreditCard, "INNER", "LEFT") & " JOIN tblCreditCard cc ON f.PaymentMethodID = cc.ID"
If sWhere <> "" OR sWhereDate <> "" Then
	sSQL = sSQL & " WHERE " & sWhere
	If sWhere <> "" And sWhereDate <> "" then sSQL=sSQL & " AND "
	sSQL = sSQL & sWhereDate
End If
sSQL = sSQL & " ORDER BY f.InsertDate DESC"
call openRsDataPaging(session("PageSize"))
If not rsData.EOF Then
	Set fsServer=Server.CreateObject("Scripting.FileSystemObject")
	%>
	<table class="formNormal" align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr>
		<th colspan="2"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></th>
		<th valign="bottom">ID</th>
		<th>Date<br /></th>
		<%If nCompanyID="" Then%><th>Merchant<br /></th><%End if%>
		<th colspan="2">Payment Method<br /></th>
		<th>Credit&nbsp;Type<br /></th>
		<th>Ins.<br /></th>
		<th>Amount<br /></th>
		<th>Source<br /></th>
		<th>Reply<br /></th>
		<th width="60" style="text-align:center;">Debit<br /></th>
	</tr>
	<tr><td height="3"></td></tr>
	<%
	nShowCounter = 0
	rsData.MoveFirst
	Do until rsData.EOF
		sFontColor = "black"
		If int(rsData("CreditType"))=0 then sFontColor="red"
		
		If nShowCounter > 0 Then
			%>
			<tr><td height="1" colspan="14" bgcolor="#ffffff"></td></tr>
			<tr><td height="1" colspan="14" bgcolor="silver"></td></tr>
			<tr><td height="1" colspan="14" bgcolor="#ffffff"></td></tr>
			<%
		End If
		%>
		<tr>
			<td width="20">
				<%
					If rsData("PaymentMethodID") > 0 Or (rsData("PaymentMethod") >= PMD_CC_MIN And rsData("PaymentMethod") <= PMD_CC_MAX) Or (rsData("PaymentMethod") >= PMD_EC_MIN And rsData("PaymentMethod") <= PMD_EC_MAX) Or (rsData("PaymentMethod") >= PMD_PP_MIN And rsData("PaymentMethod") <= PMD_PP_MAX) Then
						%>
						<img onclick="ExpandNode('<%=nShowCounter%>','<%= rsData("ID") %>','<%= rsData("PaymentMethod") %>','');" style="cursor:pointer;" id="oListImg<%=nShowCounter%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" /><br />
						<%
					Else
						%>
						<img src="../images/tree_disabled.gif" alt="" width="16" height="16" border="0" align="middle" /><br />
						<%
					End If
				%>
			</td>
			<td width="4" align="center">
				<div style="background-color:#ff6666; padding:1px;">
				<%
				If int(rsData("TransType"))=1 then
					Response.Write("<span style=""background-color:#ffffff;""><img src=""../images/1_space.gif"" width=""6"" height=""15"" border=""0""><br /></span>")
				ElseIf int(rsData("TransType"))=2 then
					Response.Write("<span style=""background-color:#c5c5c5;""><img src=""../images/1_space.gif"" width=""6"" height=""15"" border=""0""><br /></span>")
				Else
					Response.Write("<img src=""../images/1_space.gif"" width=""6"" height=""15"" border=""0""><br />")
				End If
				%>
				</div>
			</td>
			<td><%=rsData("id")%><br /></td>
			<td nowrap><%= FormatDatesTimes(rsData("InsertDate"),1,1,0) %><br /></td>
			<%
			If nCompanyID="" Then
				sShowName = trim(rsData("CompanyName"))
				If int(len(dbtextShow(sShowName)))<22 Then
					response.write "<td>" & dbTextShow(sShowName) & "<br /></td>"
				Else
					response.write "<td><span title="""& dbtextShow(sShowName) & """>" & left(dbtextShow(sShowName),21) & "..</span><br /></td>"
				End If
			End if
			%>
			<td width="60">
				<%
				Response.Write("<img src=""/NPCommon/ImgPaymentMethod/23X12/" & rsData("PaymentMethod") & ".gif"" alt=""" & rsData("pm_Name") & """ align=""middle"" border=""0"" /> &nbsp;")
				If Trim(rsData("BinCountry")) <> "" Then
					Response.write("<img src=""/NPCommon/ImgCountry/18X12/" & rsData("BinCountry") & ".gif"" alt=""" & rsData("BinCountry") & """ align=""middle"" border=""0"" />")
				Else
					Response.write("<img src=""/NPCommon/ImgCountry/18X12/" & rsData("PaymentMethodCountryIso") & ".gif"" alt=""" & rsData("PaymentMethodCountryIso") & """ align=""middle"" border=""0"" />")
				End If
				%>
			</td>
			<td>
				<%= Trim(rsData("PaymentMethodDisplay")) %>
				<%
					If rsData("PaymentMethod") - PMD_EC_MIN >= 0 And rsData("PaymentMethod") - PMD_EC_MAX <= 0 Then
						Response.Write " - " & rsData("pm_Name")
					End If
				%>
			</td>
			<td><%= rsData("CreditName") %></td>
			<td><%= rsData("Payments") %><br /></td>
			<td nowrap style="color:<%= sFontColor %>;"><%=FormatCurrWCT(rsData("Currency"), rsData("Amount"), rsData("CreditType"))%></td>
			<td style="white-space:nowrap;"><%= rsData("TransTypeName") %></td>
			<td align="center"><%= rsData("replyCode") %></td>
			<td style="text-align:center;">
				<%
				If trim(rsData("DebitCompanyID"))<>"" Then
					If fsServer.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & trim(rsData("DebitCompanyID")) & ".gif")) Then
						Response.write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(rsData("DebitCompanyID")) & ".gif"" alt=""" & rsData("DebitCompanyName") & """ align=""middle"" border=""0"" />")
					End if
				End if
				%>
			</td>
		</tr>
		<%
		response.write "<tr><td id=""Row" & rsData("ID") & """ colspan=""14""></td></tr>"

		nShowCounter=nShowCounter+1
	rsData.movenext
	loop
	%>
	<tr><td height="1" colspan="14" bgcolor="#ffffff"></td></tr>
	<tr><td height="1" colspan="14" bgcolor="silver"></td></tr>
	<tr><td height="1" colspan="14" bgcolor="#ffffff"></td></tr>
	</table>
	<br />
	<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%"> 
	<tr>
		<td>
			<%
			'Show paging controls
			call showPaging("merchant_transFail.asp", "Eng", sQueryString)
			%>
		</td>
	</tr>
	</table>
	<%
Else
	%>
	<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%"><tr><td>No transaction found!</td></tr></table>
	<%
End if

rsData.close
Set rsData = nothing
Set fsServer=Nothing
call closeConnection()
%>
</body>
</html>