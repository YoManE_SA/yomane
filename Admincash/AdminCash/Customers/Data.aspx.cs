﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

public partial class AdminCash_Customers_Data : Netpay.Web.Admin_AccountPage
{
	protected Netpay.Bll.Customers.Customer ItemData;
    protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack) {
			ItemData = Account as Netpay.Bll.Customers.Customer;
			if (ItemData != null) DataBind();
		}
    }

	public override void DataBind()
	{
		base.DataBind();
		if (ItemData.RefIdentityID.HasValue) ddlAppIdentity.SelectedValue = ItemData.RefIdentityID.Value.ToString();
	}

	protected void Save_Click(object sender, EventArgs e)
	{
		ItemData = Account as Netpay.Bll.Customers.Customer;
		if (ItemData == null) ItemData = new Netpay.Bll.Customers.Customer();
		ItemData.FirstName = txtFirstName.Text;
		ItemData.LastName = txtLastName.Text;
		ItemData.PersonalNumber = txtPersonalID.Text;
		ItemData.PhoneNumber = txtPhone.Text;
		ItemData.CellNumber = txtCellular.Text;
		ItemData.EmailAddress = txtEmailAddress.Text;
		ItemData.RefIdentityID = ddlAppIdentity.SelectedValue.ToNullableInt();
		//ItemData.PinCode = txtPinCode.Text;
		ItemData.PersonalAddress = caAddress.Save(ItemData.PersonalAddress);
        if (!string.IsNullOrEmpty(txtPinCode.Text))
        {
            ItemData.SetPinCode(txtPinCode.Text);
            txtPinCode.Text = "";
        }
		ItemData.Save();
	}

}