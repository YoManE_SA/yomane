﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

public partial class AdminCash_Customers_ShippingAddresses : Netpay.Web.Admin_AccountPage
{
	protected Netpay.Bll.Customers.ShippingAddress ItemData;
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack) RefreshList();
	}

	private void RefreshList()
	{
		rptList.DataSource = Netpay.Bll.Customers.ShippingAddress.LoadForCustomer(Account.CustomerID);
		rptList.DataBind();
	}

	protected void List_Command(object sender, RepeaterCommandEventArgs e)
	{
		if (e.CommandName == "Select")
		{
			ItemData = Netpay.Bll.Customers.ShippingAddress.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
			phEditForm.Visible = (ItemData != null);
			if (phEditForm.Visible) phEditForm.DataBind();
		}
	}

	protected void New_Command(object sender, CommandEventArgs e)
	{
		ItemData = new Netpay.Bll.Customers.ShippingAddress();
		phEditForm.Visible = (ItemData != null);
		if (phEditForm.Visible) phEditForm.DataBind();
	}

	protected void Save_Command(object sender, CommandEventArgs e)
	{
		ItemData = Netpay.Bll.Customers.ShippingAddress.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
		if (ItemData == null) {
			ItemData = new Netpay.Bll.Customers.ShippingAddress();
			ItemData.Customer_id = Account.CustomerID.Value;
		}
		ItemData.Title = txtTitle.Text;
		ItemData.Comment = txtComment.Text;
		caAddress.Save(ItemData);
		ItemData.Save();
		RefreshList();
	}
}