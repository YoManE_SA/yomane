﻿<%@ Page Language="C#" MasterPageFile="../Accounts/ContentPage.master" AutoEventWireup="true" CodeFile="Data.aspx.cs" Inherits="AdminCash_Customers_Data" %>
<%@ Register Src="../Accounts/Address.ascx" TagPrefix="CUC" TagName="AccountAddress" %>
<%@ Register Src="../Password.ascx" TagPrefix="AUC" TagName="Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" Runat="Server">
	<table width="100%">
		<tr>
			<td valign="top" width="50%" style="padding-right:60px;">
				<h4 class="dataSection">Personal information</h4>
				<table width="100%" class="formData">
					<tr>
						<th width="30%">Customer Number</th>
						<td><asp:Literal runat="server" ID="ltNumber" Text='<%# ItemData.AccountNumber %>' /></td>
					</tr>
					<tr>
						<th>First Name</th>
						<td><asp:TextBox runat="server" ID="txtFirstName" Text='<%# ItemData.FirstName %>' /></td>
					</tr>
					<tr>
						<th>Last Name</th>
						<td><asp:TextBox runat="server" ID="txtLastName" Text='<%# ItemData.LastName %>' /></td>
					</tr>
					<tr>
						<th>Personal ID</th>
						<td><asp:TextBox runat="server" ID="txtPersonalID" Text='<%# ItemData.PersonalNumber %>' /></td>
					</tr>
					<tr>
						<th>Phone</th>
						<td><asp:TextBox runat="server" ID="txtPhone" Text='<%# ItemData.PhoneNumber %>' /></td>
					</tr>
					<tr>
						<th>Cellular</th>
						<td><asp:TextBox runat="server" ID="txtCellular" Text='<%# ItemData.CellNumber %>' /></td>
					</tr>
				</table>
				<CUC:AccountAddress runat="server" ID="caAddress" AddressData='<%# ItemData.PersonalAddress %>' />
			</td>
			<td width="60">&nbsp;</td>
			<td valign="top">
				<h4 class="dataSection">Security Settings (Integration)</h4>
				<table width="100%" class="formData">
					<tr>
						<th>Identity</th>
						<td><netpay:ApplicationIdentityDropDown runat="server" ID="ddlAppIdentity" /></td>
					</tr><tr>
						<th>PinCode</th>
						<td><asp:TextBox runat="server" ID="txtPinCode" placeholder="[secret]" /></td>
					</tr><tr>
						<th>Email</th>
						<td><asp:TextBox runat="server" ID="txtEmailAddress" Text='<%# ItemData.EmailAddress %>' /></td>
					</tr><tr>
						<th>Password</th>
						<td><AUC:Password ID="ucPassword" RefType="Customer" Layout="SingleLineTitlesOnTop" RefID='<%# ItemData.ID %>' runat="server" /></td>
					</tr>
				</table>
				<br />
			</td>
		</tr>
	</table>
	<hr />
	<asp:Button runat="server" ID="btnSave" OnClick="Save_Click" Text="Save" style="float:right;" />
</asp:Content>

