﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

public partial class AdminCash_Customers_TabBar : System.Web.UI.UserControl
{
	protected Netpay.Web.Admin_AccountPage AccountPage { get { return Page as Netpay.Web.Admin_AccountPage; } }
	protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack) {
			var ItemData = AccountPage.Account as Netpay.Bll.Customers.Customer;
			tbTop.LoadCustomerTabs(ItemData.AccountID);
			if (ItemData != null) {
				ltCustomerName.Text = string.Format("{0} {1}", ItemData.FirstName, ItemData.LastName);
				hlEmail.NavigateUrl = string.Format("mailto:{0}", ItemData.EmailAddress);
				hlDeposits.NavigateUrl = string.Format("../customer_dataDeposit.aspx?Customer={0}", ItemData.ID);
				hlCustomerUI.NavigateUrl = string.Format("javascript:window.open('outer_login.aspx?CustomerID={0}');void(0)", ItemData.ID);
				ddlStatus.SelectedValue = ((int)ItemData.ActiveStatus).ToString();
			}
		}
    }

	protected void Status_Changes(object sender, EventArgs e)
	{
		var ItemData = AccountPage.Account as Netpay.Bll.Customers.Customer;
		ItemData.ActiveStatus = ddlStatus.SelectedValue.ToNullableEnumByValue<Netpay.Bll.Customers.ActiveStatus>().GetValueOrDefault();
		ItemData.Save();
	}
}