﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

public partial class AdminCash_Customers_List : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack) Search_Click(sender, e);
    }

    protected void Search_Click(object sender, EventArgs e)
	{
		var sf = new Netpay.Bll.Customers.Customer.SearchFilters();
		sf.ID = new Range<int?>(txtIDSearch.Text.ToNullableInt());
		sf.Name = txtNameSearch.Text;
		if (ddlAppIdentitySearch.Value.ToNullableInt() != null) sf.RefIdentityId = new List<int>() { ddlAppIdentitySearch.SelectedValue.ToNullableInt().Value };
		sf.Status = ddlStatus.SelectedValue.ToNullableEnumByValue<Netpay.Bll.Customers.ActiveStatus>();
		var pi = new Netpay.Infrastructure.SortAndPage(0, ddlPageSize.Text.ToNullableInt().GetValueOrDefault(25));
		rptList.DataSource = Netpay.Bll.Customers.Customer.Search(sf, pi);
		rptList.DataBind();
	}

    public void ExportDeviceList_Click(object sender, EventArgs e)
    {
        var sb = new System.Text.StringBuilder();
        sb.AppendFormat("<html><head></head><body>");
        sb.AppendFormat("<table><tr><th>DeviceIdentity</th><th>PushToken</th><th>PhoneNumber</th><th>AccountName</th><th>AccountNumber</th></tr>");
        var data = Netpay.Bll.Accounts.MobileDevice.Search(new Netpay.Bll.Accounts.MobileDevice.SearchFilters() { AccountType = Netpay.Bll.Accounts.AccountType.Customer }, null);
        var accounts = Netpay.Bll.Accounts.Account.LoadAccount(data.Select(v => v.Account_id).ToList());
        foreach (var d in data) {
            var a = accounts.Where(v => v.AccountID == d.Account_id).SingleOrDefault();
            if (a == null) a = Netpay.Bll.Accounts.Account.Empty;
            sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>", d.DeviceIdentity, d.PushToken, d.DevicePhoneNumber, a.AccountName, a.AccountNumber);
        }
        sb.AppendFormat("</table>");
        sb.AppendFormat("<body></html>");
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-disposition", "attachment; filename=Export.xls");
        Response.Write(sb.ToString());
        Response.End();
    }

}