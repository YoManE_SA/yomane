﻿<%@ Page Title="" Language="C#" MasterPageFile="../Accounts/ContentPage.master" AutoEventWireup="true" CodeFile="ShippingAddresses.aspx.cs" Inherits="AdminCash_Customers_ShippingAddresses" %>
<%@ Register Src="../Accounts/Address.ascx" TagPrefix="CUC" TagName="AccountAddress" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" Runat="Server">
	<table width="100%">
		<tr>
			<td valign="top" width="50%">
				<h4 class="dataSection">Shipping Addresses</h4>
				<table width="100%" class="formNormal">
					<tr>
						<th>ID</th>
						<th>Title</th>
						<th>CountryISO</th>
						<th>StateISO</th>
					</tr>
					<asp:Repeater runat="server" ID="rptList" OnItemCommand="List_Command">
						<ItemTemplate>
							<tr>
								<td><asp:LinkButton runat="server" Text='<%# Eval("ID") %>' CommandName="Select" CommandArgument='<%# Eval("ID") %>' /></td>
								<td><%# Eval("Title") %></td>
								<td><%# Eval("CountryISOCode") %></td>
								<td><%# Eval("StateISOCode") %></td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</table>
				<br />
				<asp:Button runat="server" ID="btnAdd" Text="Add Item" OnCommand="New_Command" />
			</td>
			<td width="60">&nbsp;</td>
			<td valign="top">
				<asp:PlaceHolder runat="server" ID="phEditForm" Visible="false">
					<h4 class="dataSection">Edit Shipping Address</h4>
					<table width="100%" class="formData">
						<tr>
							<th width="30%">Title</th>
							<td><asp:TextBox runat="server" ID="txtTitle" Text='<%# ItemData.Title %>' /></td>
						</tr>
						<tr>
							<th>Comment</th>
							<td><asp:TextBox runat="server" ID="txtComment" Text='<%# ItemData.Comment %>' /></td>
						</tr>
					</table>
					<CUC:AccountAddress runat="server" ID="caAddress" AddressData='<%# ItemData %>' />
					<asp:Button runat="server" ID="btnSave" OnCommand="Save_Command" CommandArgument='<%# ItemData.ID %>' Text="Save" />
				</asp:PlaceHolder>
			</td>
		</tr>
	</table>
</asp:Content>

