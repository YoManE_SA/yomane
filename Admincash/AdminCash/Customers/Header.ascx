﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="AdminCash_Customers_TabBar" %>
<%@ Register Src="../Toolbar.ascx" TagPrefix="AUC" TagName="Toolbar" %>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td id="pageMainHeadingTd">
			<span id="pageMainHeading">CUSTOMER MANAGEMENT</span> -
			<span><asp:Literal runat="server" ID="ltCustomerName" /></span>
		</td>
		<td align="right">
			Status: <netpay:EnumDropDown runat="server" ID="ddlStatus" ViewEnumName="Netpay.Bll.Customers.ActiveStatus" AutoPostBack="true" OnSelectedIndexChanged="Status_Changes" EnableBlankSelection="false" />
		</td>
		<td align="right">
			<asp:HyperLink runat="server" ID="hlEmail" NavigateUrl="mailto:udi@netpay-intl.com"><asp:Image runat="server" ImageUrl="~/images/post_button_2.gif" title="send e-mail" width="25" height="21" border="0" /></asp:HyperLink>
			<asp:HyperLink runat="server" ID="hlDeposits" NavigateUrl="../customer_dataDeposit.aspx?Customer="><asp:Image runat="server" ImageUrl="~/images/post_button_4.gif" title="deposit" width="25" height="21" border="0" /></asp:HyperLink>
			<asp:HyperLink runat="server" ID="hlCustomerUI" NavigateUrl="javascript:window.open('outer_login.aspx?CustomerID=2750');void(0)"><asp:Image runat="server" ImageUrl="~/images/post_button_1.gif" title="to client interface" width="40" height="21" border="0" /></asp:HyperLink>
		</td>
	</tr>
</table>
<br />
<AUC:Toolbar runat="server" ID="tbTop" />
