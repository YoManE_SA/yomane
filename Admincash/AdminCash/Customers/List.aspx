﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminCash/FiltersMaster.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="AdminCash_Customers_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
	<form runat="server">
		<h1><asp:Label ID="lblPermissions" runat="server" /> Customers<span> - Filter</span></h1>
		<table width="100%" class="filterNormal" style="border:1px solid silver">
			<tr>
				<th>ID<br /><asp:TextBox runat="server" ID="txtIDSearch" Width="35" /></th>
				<th>Name<br /><asp:TextBox runat="server" ID="txtNameSearch" /></th>
				<th>Status<br /><netpay:EnumDropDown runat="server" ViewEnumName="Netpay.Bll.Customers.ActiveStatus" ID="ddlStatus" /></th>
				<th>Identity<br /><netpay:ApplicationIdentityDropDown runat="server" ID="ddlAppIdentitySearch" /></th>
				<th>Show<br /><asp:DropDownList runat="server" ID="ddlPageSize" ><asp:listitem Text="25" Value="25" /><asp:listitem Text="50" Value="50" /><asp:listitem Text="75" Value="75" /><asp:listitem Text="100" Value="100" /> </asp:DropDownList></th>
				<td><br /><asp:Button runat="server" UseSubmitBehavior="true" OnClick="Search_Click" Text="GO" /></td>
			</tr>
		</table>
		<a href="Data.aspx" target="frmBody" style="float:right" >Add new customer</a>
		<br />
	    <table class="formNormal" width="100%">
		    <tr>
			    <th>ID</th>
			    <th>Name</th>
			    <th>Registration</th>
			    <th>Identity</th>
			    <th>Pass</th>
			    <th>Fail</th>
		    </tr>
		    <asp:Repeater runat="server" ID="rptList">
			    <ItemTemplate>
				    <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
					    <td><%# Eval("ID") %></td>
					    <td><asp:HyperLink runat="server" Target="frmBody" Text='<%# ((string)Eval("FullName")).Truncate(30) %>' NavigateUrl='<%# "Data.aspx?id=" + Eval("AccountID") %>' /></td>
					    <td><%# Eval("RegistrationDate", "{0:d}") %></td>
					    <td><%# Eval("RefIdentityID") %></td>
					    <td><%# Eval("ProcessStatus.PassCount") %></td>
					    <td><%# Eval("ProcessStatus.FailCount") %></td>
				    </tr>
			    </ItemTemplate>
		    </asp:Repeater>
	    </table>
        <br />
        <asp:LinkButton runat="server" OnClick="ExportDeviceList_Click" Text="Export Device List" />
	</form>
</asp:Content>

