<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DebitCompanies.ascx" TagName="FilterDebitCompanies" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_ComboBox.ascx" TagName="FilterComboBox" TagPrefix="Uc1" %>

<script runat="server">
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)

		Dim sSQL As String, drData As SqlDataReader

		litCountry.Text = "<select style=""width:165px;font:normal 11px Monospace;"" name=""BINCountry"" class=""grayBG"">"
		litCountry.Text &= "<option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT CountryISOCode, CountryISOCode+' '+Name FROM List.CountryList ORDER BY CountryISOCode, Name"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			litCountry.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		drData.Close()
		litCountry.Text &= "</select>"

		fdtrControl.FromDateTime = Date.Now.AddDays(-7).ToShortDateString()
		fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)

		If HttpContext.Current.Application.Get("CUR_CHARS") Is Nothing Then dbPages.LoadCurrencies()
		Dim sCurrenciesSelect As String = "<select name=""Currency"" class=""grayBG"">"
		sCurrenciesSelect = sCurrenciesSelect & "<option value=""""></option>"
		For i As Integer = 0 To eCurrencies.MAX_CURRENCY
			sCurrenciesSelect = sCurrenciesSelect & "<option value=""" & i & """>" & HttpContext.Current.Application.Get("CUR_ISO")(i) & "</option>"
		Next
		sCurrenciesSelect = sCurrenciesSelect & "</select>"
		litCurrencies.Text = sCurrenciesSelect
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script language="JavaScript">
	    function ExpandNode(fTrNum) {
	        trObj = document.getElementById("tr" + fTrNum)
	        imgObj = document.getElementById("oListImg" + fTrNum)
	        if (trObj) {
	            if (trObj.style.display == '') {
	                imgObj.src = '../images/tree_expand.gif';
	                trObj.style.display = 'none';
	            }
	            else {
	                imgObj.src = '../images/tree_collapse.gif';
	                trObj.style.display = '';
	            }
	        }
	    }
	</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<form runat="server" action="merchant_transFail.asp" method="get" target="frmBody" onsubmit="return filterForm_Submit(null, document.getElementById('ActionURL').value);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" id="CompanyID" name="CompanyID" />
	<input type="hidden" name="ActionURL" value="merchant_transFail.asp" id="ActionURL" />

	<h1><asp:Label ID="lblPermissions" runat="server" /> Rejected<span> - Filter</span></h1>

	<table class="filterNormal">
	<tr><td colspan="2"><Uc1:FilterMerchants id="FMerchants" ClientField="CompanyID" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
	<tr>
		<td colspan="2">
			<Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" />
		</td>
	</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
	<tr><th colspan="2">Reply code</th></tr>
	<tr>
		<td colspan="2">
			<Uc1:FilterComboBox ID="FilterComboBox1" runat="server" ClientField="AnswerNumber" Width="155px" RecordsVisible="10"
				ValueList="123|456|789" TextList="First code|Second code|Third code" ColorList="Red|Green|Blue" ListSeparator="|"
				ValueColumn="Code" TextColumn="DescriptionOriginal" ColorColumn="Color"
				SqlQuery="SELECT CASE FailSource WHEN 0 THEN 'Red' WHEN 1 THEN 'Blue' ELSE 'Green' END Color, Code, DescriptionOriginal
				FROM tblDebitCompanyCode WHERE DebitCompanyID=1 AND FailSource=1 AND Code LIKE '5__' ORDER BY Code"
				/>
		</td>
	</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
	<tr><th colspan="2">Auto Refund</th></tr>
	<tr>
		<td>Phase</td>
		<td>
			<select name="ARF_VALUE" style="width:110px;" onchange="AnswerNumber.value=(selectedIndex > 0 ? '521' : '');" >
				<option value="">
				<option value="ARF_WAIT">Waiting Refund
				<option value="ARF_DONE">Refund Succeeded
				<option value="ARF_FAIL">Refund Not Needed
			</select>
		</td>
	</tr>
	<tr><td height="4"></td></tr>
	<tr>
		<th colspan="2">
			<img onclick="ExpandNode('0');" style="cursor:pointer; margin-right:4px;" id="oListImg0" src="../images/tree_expand.gif" width="16"
			 border="0" align="top"><span style="cursor:pointer;" onclick="ExpandNode('0');">Debit company</span>
		</th>
	</tr>
	<tr id="tr0" style="display:none;">
		<td colspan="2">
			<Uc1:FilterDebitCompanies ID="FilterDebitCompanies1" ClientFieldDebitCompany="DebitCompanyID" ClientFieldDebitTerminal="Terminal" runat="server" />
		</td>
	</tr>
	<tr><td height="4"></td></tr>
	<tr>
		<th colspan="2">
			<img onclick="ExpandNode('1');" style="cursor:pointer; margin-right:4px;" id="oListImg1" src="../images/tree_expand.gif" width="16"
			 border="0" align="top"><span style="cursor:pointer;" onclick="ExpandNode('1');">Credit Card</span>
		</th>
	</tr>
	<tr id="tr1" style="display:none;">
		<td colspan="2">
			<table class="filterNormal">
			<tr>
				<td>BIN #</td>
				<td><input name="CardNumberFirst6" style="width:90px;" /></td>
			</tr>
			<tr>
				<td>Country</td>
				<td>
					<asp:literal ID="litCountry" runat="server" />
				</td>
			</tr>
			<tr>
				<td>Last 4</td>
				<td><input name="CardNumberLast4" style="width:90px;" /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td height="4"></td></tr>
	<tr>
		<th colspan="2">
			<img onclick="ExpandNode('2');" style="cursor:pointer; margin-right:4px;" id="oListImg2" src="../images/tree_expand.gif" width="16"
			 border="0" align="top"><span style="cursor:pointer;" onclick="ExpandNode('2');">Transaction Details</span>
		</th>
	</tr>
	<tr id="tr2" style="display:none;">
		<td colspan="2">
			<table class="filterNormal">
			<tr>
				<td>Type</td>
				<td style="white-space:nowrap;">
					<input type="Checkbox" name="TransType" value="0" class="option" />Debit
					<input type="Checkbox" name="TransType" value="1" class="option" />Pre-Auth
				</td>
			</tr>
			<tr>
				<td>Debit ref.</td>
				<td><input name="DebitReferenceCode" style="width:100px;" /></td>
			</tr>
			<tr>
				<td>Amount</td>
				<td>
					<input name="AmountMin" class="medium grayBG" style="width:50px;" />
					to <input name="AmountMax" class="medium grayBG" style="width:50px;" />
				</td>
			</tr>
			<tr>
				<td>Currency</td>
				<td colspan="2"><asp:Literal runat="server" ID="litCurrencies" /></td>
			</tr>		
			<tr>
				<td>Free text</td>
				<td><input name="FreeText" style="width:100px;" /></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td colspan="2">
			<input type="checkbox" name="UseSQL2" value="YES" checked="checked" class="option">Use SQL2 when possible
		</td>
	</tr>
	<tr><td height="6"></td></tr>
	<tr>	
		<td>
			<select name="PageSize">
				<option value="10">10 rows/page</option>
				<option value="25" selected="selected">25 rows/page</option>
				<option value="50">50 rows/page</option>
				<option value="100">100 rows/page</option>
			</select>
		</td>
		<td style="text-align:right;">
			<input type="submit" value="SEARCH" onclick="this.form.ActionURL.value='merchant_transFail.asp';" /> &nbsp;&nbsp;
			<input type="submit" value="XSL" onclick="this.form.ActionURL.value='TransFailExcel.aspx';" />
		</td>
	</tr>
	</table>
	</form>
	<hr class="filter" />
	<form id="frmTransID" action="merchant_transFail.asp" method="post" target="frmBody">
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<td>Trans ID</td>
			<td><input size="12" name="TransID" /></td>
			<td style="text-align:center;"><input type="submit" value="SHOW" /></td>
		</tr>
	</table>
	</form>
</asp:Content>