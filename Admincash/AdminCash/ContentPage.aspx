﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" %>
<%@ Register TagPrefix="Rad" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="Netpay" Namespace="Netpay.Web.Controls" Assembly="Netpay.Web" %>
<script runat="server" type="text/C#">
	private string _fileName;
	protected string FileName { 
		get { return _fileName; }
		set { foreach (var c in System.IO.Path.GetInvalidFileNameChars()) value = value.Replace(c, '_'); _fileName = value; }
	}

    public static string BaseDataDir { get { return Domain.Current.MapPrivateDataPath(@"Site Content\MCP\"); } }
	public static string ImagePath { get { return BaseDataDir + "Images/"; } }
	public static string LanguageContentDir { get { return BaseDataDir + "Content/"; } }
	public static string LanguageArchiveContentDir { get { return BaseDataDir + "ContentArchive/"; } }
	public string PhysicalPath { get { return LanguageContentDir + FileName + ".xml";  } }
	public string ResolveToLocal(string value) { return value.Replace("~/", ResolveUrl("~/")); }
	public string ResolveToGlobal(string value) { return value.Replace("=\"" + ResolveUrl("~/"), "=\"~/"); }
	public static string GetXmlNodeText(System.Xml.XmlNode pNode, string sChildElement)
	{
		System.Xml.XmlNode ppNode = pNode.SelectSingleNode(sChildElement);
		if (ppNode == null) return null;
		return ppNode.InnerText;
	}

	public static void SetXmlNodeText(System.Xml.XmlNode pNode, string sChildElement, string sData)
	{
		System.Xml.XmlNode ppNode = pNode.SelectSingleNode(sChildElement);
		if (ppNode == null) pNode.AppendChild(ppNode = pNode.OwnerDocument.CreateElement(sChildElement));
		ppNode.InnerText = sData;
	}

	public static void SetXmlNodeAttrib(System.Xml.XmlNode pNode, string sAttrib, string sData)
	{
		System.Xml.XmlAttribute pAttrib = pNode.Attributes[sAttrib];
		if (pAttrib == null) pNode.Attributes.Append(pAttrib = pNode.OwnerDocument.CreateAttribute(sAttrib));
		pAttrib.InnerText = sData;
	}

	private void LoadContent(string fileName, bool contentOnly)
    {
        if (string.IsNullOrEmpty(fileName)) fileName = PhysicalPath;
        if (!System.IO.File.Exists(fileName)) return;
        System.Xml.XmlDocument pDoc = new System.Xml.XmlDocument();
        pDoc.Load(fileName);
        RadEditor1.Content = ResolveToLocal(dbPages.TestVar(GetXmlNodeText(pDoc.DocumentElement, "Content"), -1, ""));
        if (contentOnly) return;
        PageTitle.Text = GetXmlNodeText(pDoc.DocumentElement, "PageTitle");
        MetaKeywords.Text = GetXmlNodeText(pDoc.DocumentElement, "MetaKeywords");
        MetaDescription.Text = GetXmlNodeText(pDoc.DocumentElement, "MetaDescription");
		PageURL.Text = Domain.Current.MerchantUrl + @"Website/ContentPage.aspx?f=" + Server.UrlEncode(FileName);
    }

    private void SaveContent_Click(object sender, EventArgs e)
    {
        System.Xml.XmlDocument pDoc = new System.Xml.XmlDocument();
		FileName = PageTitle.Text;
        if (!System.IO.File.Exists(PhysicalPath))
            pDoc.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n<PageData>\r\n</PageData>\r\n");
        else pDoc.Load(PhysicalPath);
        SetXmlNodeText(pDoc.DocumentElement, "Update", DateTime.Now.ToString("g"));
        SetXmlNodeText(pDoc.DocumentElement, "User", Request.LogonUserIdentity.Name);
        SetXmlNodeText(pDoc.DocumentElement, "PageTitle", PageTitle.Text);
        SetXmlNodeText(pDoc.DocumentElement, "MetaKeywords", MetaKeywords.Text);
        SetXmlNodeText(pDoc.DocumentElement, "MetaDescription", MetaDescription.Text);
        
        System.Xml.XmlNode pNode = pDoc.DocumentElement.SelectSingleNode("Content");
        if (pNode != null) pNode.ParentNode.RemoveChild(pNode);
        pDoc.DocumentElement.AppendChild(pNode = pDoc.CreateElement("Content"));
        pNode.AppendChild(pDoc.CreateCDataSection(ResolveToGlobal(RadEditor1.Content)));
        if (System.IO.File.Exists(PhysicalPath)) System.IO.File.Move(PhysicalPath, GetArchiveFileName());
        pDoc.Save(PhysicalPath);
		EnumContentList();
		EnumFileArchive();
	}

	private void DeleteContent_Click(object sender, EventArgs e)
	{
		FileName = PageTitle.Text;
		if (System.IO.File.Exists(PhysicalPath)) System.IO.File.Move(PhysicalPath, LanguageArchiveContentDir + FileName + ".xml");
		EnumContentList();
	}

	private void OpenFile_Click(object sender, CommandEventArgs e)
	{
		FileName = (sender as LinkButton).Text;
		LoadContent(null, false);
		btnDelete.Visible = phPageUrlContainer.Visible = true;
		EnumFileArchive();
	}

	private void EnumContentList()
	{
        if (!System.IO.Directory.Exists(LanguageContentDir)) System.IO.Directory.CreateDirectory(LanguageContentDir);
		string[] files = System.IO.Directory.GetFiles(LanguageContentDir);
		for (int i = 0; i < files.Length; i++)
			files[i] = System.IO.Path.GetFileNameWithoutExtension(files[i]);
		rptContentList.DataSource = files;
		rptContentList.DataBind();
	}

	protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!IsPostBack) {
			string[] imagePaths = new string[] { ImagePath };
			RadEditor1.ImageManager.UploadPaths = imagePaths;
			RadEditor1.ImageManager.ViewPaths = imagePaths;
			RadEditor1.ImageManager.DeletePaths = imagePaths;
			EnumContentList();
        }
		//if (ElementDir == "rtl") RadEditor1.ContentAreaCssFile = "~/stylesRtl.css";
    }

    //archive managment
    private void FileArchive_OnSelectedIndexChanged(object sender, EventArgs e) 
    {
        string fileName = ddlFileArchive.SelectedItem.Value;
        if (!string.IsNullOrEmpty(fileName)) fileName = LanguageArchiveContentDir + fileName;
        LoadContent(fileName, true);
    }

    private void EnumFileArchive()
    {
        ddlFileArchive.Items.Clear();
        bool isExist = System.IO.File.Exists(PhysicalPath);
        ddlFileArchive.Items.Add(new ListItem("Current - " + (isExist ? System.IO.File.GetLastWriteTime(PhysicalPath) : DateTime.Now).ToShortDateString(), ""));
		if (!isExist) return;
        if (!System.IO.Directory.Exists(LanguageArchiveContentDir)) System.IO.Directory.CreateDirectory(LanguageArchiveContentDir);
		string[] fileNames = System.IO.Directory.GetFiles(LanguageArchiveContentDir, FileName + "_*.xml");
        foreach (string s in fileNames)
            ddlFileArchive.Items.Add(new ListItem(System.IO.Path.GetFileName(s) + " - " + System.IO.File.GetLastWriteTime(s).ToShortDateString(), System.IO.Path.GetFileName(s)));
    }

    private string GetArchiveFileName()
    {
        int fileCounter = 1;
        while (true)
        {
			string fileName = LanguageArchiveContentDir + FileName + "_" + fileCounter + ".xml";
            if (!System.IO.File.Exists(fileName)) return fileName;
            fileCounter++;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		Security.CheckPermission(lblPermissions);
	}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Content Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="../StyleSheet/StyleAdminNetEng.css" />
</head>
<body style="padding: 0px 30px 0px 18px;">
    <form runat="server">
		<asp:ScriptManager runat="server" />
		<h1><asp:label ID="lblPermissions" runat="server" /> CONTENT PAGES<span> - Merchant CP</span></h1>
		<table width="100%">
			<tr>
				<td valign="top" style="width:170px; background-color:#f5f5f5; padding:8px;">
					<h5 style="margin:0;">FILE LIST</h5><br />
					<a href="?">[ NEW PAGE ]</a><br />
					<br />
					<asp:Repeater runat="server" ID="rptContentList">
						<ItemTemplate>
							<img src="\NPCommon\Images\Arrow01Right.gif" align="middle" />
							<asp:LinkButton ID="LinkButton1" runat="server" Text="<%# Container.DataItem %>" OnCommand="OpenFile_Click" Font-Size="12px" /><br />
						</ItemTemplate>
					</asp:Repeater>
				</td>
				<td valign="top" style="padding-left:25px;">
					<script type="text/jscript">
        				function selectTab(tabId) {
        					document.getElementById('tabSettings').style.display = (tabId == 'tabSettings' ? '' : 'none');
        					document.getElementById('tabContent').style.display = (tabId == 'tabContent' ? '' : 'none');
        					document.getElementById('tabSettingsTab').className = (tabId == 'tabSettings' ? 'SelectedTab' : '');
        					document.getElementById('tabContentTab').className = (tabId == 'tabContent' ? 'SelectedTab' : '');
        				}
					</script>
					<table border="0" class="formThin">
						<tr><th>SEO - Page Title</th></tr>
						<tr><td><asp:TextBox runat="server" ID="PageTitle" style="width:100%" /></td></tr>
						<tr><th style="padding-top:10px;">SEO - Meta Keywords</th></tr>
						<tr><td><asp:TextBox runat="server" ID="MetaKeywords" TextMode="MultiLine" Rows="2" style="width:100%;height:40px;" /></td></tr>
						<tr><th style="padding-top:10px;">SEO - Meta Description</th></tr>
						<tr><td><asp:TextBox runat="server" ID="MetaDescription" TextMode="MultiLine" Rows="2" style="width:100%;height:40px;" /></td></tr>
						<tr><td><br /></td></tr>
						<asp:PlaceHolder runat="server" ID="phPageUrlContainer" Visible="false">
							<tr><th></th></tr>
							<tr>
								<td>
									<span>File Version: <asp:DropDownList runat="server" ID="ddlFileArchive" style="width:400px;" AutoPostBack="true" OnSelectedIndexChanged="FileArchive_OnSelectedIndexChanged" /><br /></span>
									<br />
                                    <span>Page URL: <asp:Label runat="server" ID="PageURL" style="direction:ltr;" /></span>
								</td>
							</tr>
						</asp:PlaceHolder>
						<tr>
							<td>
								<Rad:RadEditor ID="RadEditor1" runat="server" ContentAreaCssFile="~/styles.css" Width="100%" Skin="Windows7" ImageManager-UploadPaths="~/Data/Editor/" ImageManager-ViewPaths="~/Data/Editor/" ImageManager-DeletePaths="~/Data/Editor/" />
							</td>
						</tr>
					</table>
					<br />
					<div class="Buttons">
						<asp:Button runat="server" ID="btnSave" Text="Save" OnClick="SaveContent_Click" />
						<asp:Button runat="server" ID="btnDelete" Visible="false" Text="Delete" OnClick="DeleteContent_Click" OnClientClick="return confirm('Delete the page?');" /> &nbsp;&nbsp;&nbsp;&nbsp;
					</div>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
