<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<script runat="server">
    Sub ClearBody()
        Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
        Response.Write("parent.frmBody.location.replace('common_blank.htm');")
        Response.Write("</s" & "cript>")
    End Sub

    Sub ReloadPage()
        Response.Redirect("security_documents.aspx")
        Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
        Response.Write("setTimeout(""location.replace('security_documents.aspx');"", 100);")
        Response.Write("</s" & "cript>")
    End Sub

    Sub Page_Load()
        Security.CheckPermission(lblPermissions)
        Dim nDeleteDocument As Integer = dbPages.TestVar(Request("DeleteDocument"), 1, 0, 0)
        If nDeleteDocument > 0 Then
            dbPages.ExecSql("DELETE FROM tblSecurityDocument WHERE ID=" & nDeleteDocument)
            ClearBody()
        End If
        dsDocuments.ConnectionString = dbPages.DSN()
    End Sub

    Sub btnAddDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If String.IsNullOrEmpty(txtNewDocument.Text) Then
            lblAddDocument.Text = "Specify new document filename!"
        Else
            lblAddDocument.Text = String.Empty
            dbPages.ExecSql("INSERT INTO tblSecurityDocument (sd_Title, sd_URL) VALUES ('" & dbPages.DBText(txtNewDocument.Text) & "', '" & dbPages.DBText(txtNewDocument.Text) & "')")
            txtNewDocument.Text = String.Empty
            gvDocuments.DataBind()
        End If
    End Sub

    Sub gvDocuments_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If gvDocuments.SelectedIndex >= 0 Then
            Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"" src=""../js/security.js""></s" & "cript>")
            Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
            Response.Write("OpenSecurityManagerFrameURL(""" & CType(gvDocuments.SelectedRow.Cells(2).Controls(0), HyperLink).Text.Replace("""", String.Empty) & """);")
            Response.Write("</s" & "cript>")
        End If
    End Sub

    Sub gvDocuments_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs)
        ClearBody()
        ReloadPage()
    End Sub

    Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("security_documents.aspx")
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security - Documents</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body class="menu">
	<span class="toolbar">
		<a class="toolbar AlwaysActive" href="security_users.aspx" onclick="parent.frmBody.location.replace('common_blank.htm');">Users</a>
		<span class="toolbarSeparator">|</span>
		<a class="toolbar AlwaysActive" href="security_groups.aspx" onclick="parent.frmBody.location.replace('common_blank.htm');">Groups</a>
		<span class="toolbarSeparator">|</span>
		<a class="toolbarCurrent AlwaysActive" href="security_documents.aspx" onclick="parent.frmBody.location.replace('common_blank.htm');">Documents</a>
	</span>
	<form id="frmServer" runat="server">
		<script language="javascript" type="text/javascript">
			function DeleteRecord(nID, sTitle, sDescription)
			{
				if (confirm("The following document is to be unmanaged:\n\n"+sTitle+" ("+sDescription+")\n\nUnmanaging document removes all access limitations.\n\nAny authenticated user will be allowed to see this document.\n\nDo you really want to unmanage this document?"))
					document.location.replace("security_documents.aspx?DeleteDocument="+nID);
			}
		</script>
		<h1><asp:label ID="lblPermissions" runat="server" /> SECURITY<span> - DOCUMENTS</span></h1>
		<div class="bordered">
			<span style="float:right;padding-bottom:1px;">
				<asp:Button ID="btnRefresh" CssClass="buttonWhite AlwaysActive" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
			</span>
			Add new document
			<asp:TextBox ID="txtNewDocument" CssClass="text" runat="server" />
			<asp:Button ID="btnAddDocument" CssClass="buttonWhite" runat="server" Text="Add" OnClick="btnAddDocument_Click" />
			<asp:Label ID="lblAddDocument" runat="server" ForeColor="red" />
		</div>
		<asp:GridView ID="gvDocuments" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="dsDocuments" AllowPaging="False" AllowSorting="False" SelectedRowStyle-CssClass="rowSelected" OnSelectedIndexChanged="gvDocuments_SelectedIndexChanged" OnRowDeleted="gvDocuments_RowDeleted" Width="100%" CssClass="grid">
			<Columns>
				<asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" Visible="false" />
				<asp:BoundField DataField="sd_Title" HeaderText="Title" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100%" ControlStyle-CssClass="text"/>
				<asp:HyperLinkField HeaderText="File Name" HeaderStyle-HorizontalAlign="Left" Target="frmBody" DataTextField="sd_URL" DataNavigateUrlFields="sd_URL" />
				<asp:CheckBoxField DataField="sd_IsLogged" HeaderText="Logged" ItemStyle-CssClass="centered" HeaderStyle-CssClass="columnTitleVertical" />
				<asp:BoundField DataField="GroupCount" HeaderText="Groups" ItemStyle-CssClass="centered" ReadOnly="True" />
				<asp:TemplateField HeaderText="Delete" >
					<ItemTemplate>
						<input type="button" onclick="DeleteRecord(<%# Eval("ID") %>, '<%# Eval("sd_Title") %>', '<%# Eval("sd_URL") %>');" value="Delete" class="buttonWhite buttonDelete">
					</ItemTemplate>
				</asp:TemplateField>
				<asp:CommandField HeaderText="Actions" ButtonType="Button" ItemStyle-CssClass="buttons" ControlStyle-CssClass="buttonWhite" ShowDeleteButton="False" ShowEditButton="True" ShowSelectButton="True" HeaderStyle-CssClass="actions" SelectText="Manage" UpdateText="Save" />
			</Columns>
			<SelectedRowStyle CssClass="rowSelected" />
		</asp:GridView>
		<asp:SqlDataSource ID="dsDocuments" runat="server" ProviderName="System.Data.SqlClient"
			SelectCommand="SELECT ID, sd_Title, sd_URL, sd_IsLogged,
			 (SELECT Count(*) FROM tblSecurityDocumentGroup WHERE sdg_Document=tblSecurityDocument.ID AND sdg_IsVisible=1) GroupCount
			   FROM tblSecurityDocument ORDER BY sd_Title, sd_URL"
			UpdateCommand="UPDATE tblSecurityDocument SET sd_Title = IsNull(@sd_Title, ''), sd_IsLogged = @sd_IsLogged WHERE ID = @ID">
			<DeleteParameters>
				<asp:Parameter Name="ID" Type="Int32" />
			</DeleteParameters>
			<UpdateParameters>
				<asp:Parameter Name="sd_Title" Type="String" />
				<asp:Parameter Name="sd_IsLogged" Type="Boolean" />
				<asp:Parameter Name="ID" Type="Int32" />
			</UpdateParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>