<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="rtl">
	<%
	sSendFaxID = trim(request("SendFaxID"))
	sSendFaxArray = Split(sSendFaxID, ",", -1, 1)
	sSendFaxArraySize = int(UBound(sSendFaxArray))+1
	
	sSQL="SELECT * FROM tblWireMoney WHERE tblWireMoney.WireMoney_id IN (" & sSendFaxID & ")"
	set rsData = oledbData.execute(sSQL)
	nCount=0
	Do until rsData.EOF
		If request("type")="preview" Then
			sPaymentBank = trim(rsData("wirePaymentBank"))
			sPaymentBranch = trim(rsData("wirePaymentBranch"))
			sPaymentAccount = trim(rsData("wirePaymentAccount"))
			sPaymentPayeeName = trim(rsData("wirePaymentPayeeName"))
			sIDnumber = trim(rsData("wireIDnumber"))
			sWirePaymentAbroadAccountName = trim(rsData("WirePaymentAbroadAccountName"))
			sWirePaymentAbroadAccountNumber = trim(rsData("WirePaymentAbroadAccountNumber"))
			sWirePaymentAbroadBankName = trim(rsData("WirePaymentAbroadBankName"))
			sWirePaymentAbroadBankAddress = trim(rsData("WirePaymentAbroadBankAddress"))
			sWirePaymentAbroadSwiftNumber = trim(rsData("WirePaymentAbroadSwiftNumber"))
		
			sPaymentAbroadOK = false
			if sWirePaymentAbroadAccountName<>"" AND sWirePaymentAbroadAccountNumber<>"" AND sWirePaymentAbroadBankName<>"" AND sWirePaymentAbroadBankAddress<>"" AND sWirePaymentAbroadSwiftNumber<>"" then
				if IsNumeric(sWirePaymentAbroadAccountNumber) AND IsNumeric(sWirePaymentAbroadSwiftNumber) then
					sPaymentAbroadOK = true
				end if
			end if
			%>
			<table align="center" border="0" dir="rtl" cellpadding="2" cellspacing="0" width="650">
			<tr>
				<td align="right" dir="rtl" class="txt14" style="color:#00254a;">
					<li type="square">���� ������ ����� ������</li><br />
				</td>
			</tr>
			</table>
			<table align="center" border="0" style="background-color:white; border:1px solid black; filter:progid:DXImageTransform.Microsoft.Shadow(color='gray', Direction=135, Strength=5)" dir="rtl" cellpadding="1" cellspacing="2" width="650">
			<tr>
				<td style="padding-top:10px;">
					<div id="divWirePreview<%= rsData("WireMoney_id") %>">
						<%
						If sPaymentAbroadOK then
							%>
							<!--#include file="Wire_fax_AbroadDoc.asp"-->
							<%
						else
							%>
							<!--#include file="Wire_fax_LocalDoc.asp"-->
							<%
						End If
						%>
					</div>
				</td>
			</tr>
			</table>
			<br /><br />
			<%
		Else
			nCount=nCount+1
			if int(nCount)<int(sSendFaxArraySize) then ' check to see if need to page break
				sStyle="page-break-after:always;"
			else
				sStyle="page-break-after:auto;"
			end if
			%>
			<div id="divWirePrint<%= rsData("id") %>" style="<%= sStyle %>">
				<script language="JavaScript">
					divWirePrint<%= rsData("id") %>.innerHTML=top.fraWirePreview.divWirePreview<%= rsData("id") %>.innerHTML;
				</script>
			</div>
			<%
		End If
	rsData.movenext
	loop
	rsData.close
	Set rsData = nothing
	%>
	<script language="JavaScript">
		<%
		If request("type")="preview" then
			%>
			top.fraWireprint.location.href='Wire_fax_pages.asp?type=print&SendFaxID=<%= sSendFaxID %>';
			<%
		Else
			%>
			top.fraWireMenu.frmWire.bPrint.style.cursor = 'hand';
			top.fraWireMenu.frmWire.bPrint.disabled = false;
			<%
		End If
		%>
	</script>
</body>
</html>
<% call closeConnection() %>