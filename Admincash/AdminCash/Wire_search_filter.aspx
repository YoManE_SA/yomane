<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = DateAdd("d", -1, Date.Now).ToShortDateString
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

    <form runat="server" action="wire_search_data.asp" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />
    <input type="hidden" id="Source" name="Source" value="filter" />
    <input type="hidden" id="ShowCompanyID" name="ShowCompanyID" />

	<h1><asp:label ID="lblPermissions" runat="server" /> Wire Transfer<span> - Filter</span></h1>

	<table class="filterNormal">
    <tr>
		<td><Uc1:FilterMerchants id="FMerchants" SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
			 INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" runat="server" />
		 </td>
	</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="3">Wire Status</th></tr>
		<tr>
			<td><input type="Checkbox" name="WireFlag" value="0" checked> <span style="background-color: #7d7d7d; color: #7d7d7d;">-</span> Waiting </td>
			<td width="5%"></td>
			<td><input type="Checkbox" name="WireFlag" value="1" checked> <span style="background-color: #66cc66; color: #66cc66;">-</span> Approved </td>
		</tr>
		<tr>
			<td colspan="3"><input type="Checkbox" name="WireFlag" value="4" checked> <span style="background-color:#FFD400; color: #FFD400;">-</span> Partially Approved </td>
		</tr>
		<tr>
			<td><input type="Checkbox" name="WireFlag" value="2" checked> <span style="background-color: #ff6666; color: #ff6666;">-</span> Rejected </td>
			<td></td>
			<td><input type="Checkbox" name="WireFlag" value="3" checked> <span style="background-color: #6699cc; color: #6699cc;">-</span> Done </td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="3">Wire Action</th></tr>
		<tr>
			<td>
				<input type="Checkbox" name="wireStatus" value="0">None
			</td>
			<td>
				<input type="Checkbox" name="wireStatus" value="2">On hold
			</td>
			<td>
				<input type="Checkbox" name="wireStatus" value="6">Manual
			</td>
		</tr>
		<tr>
			<td>
				<input type="Checkbox" name="wireStatus" value="4">Fax
			</td>
			<td>
				<input type="Checkbox" name="wireStatus" value="3">Masav
			</td>
			<td>
				<input type="Checkbox" name="wireStatus" value="5">Balance
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="Checkbox" name="wireStatus" value="1">Canceled
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Request Details</th></tr>
		<tr>
			<td>Type</td>
			<td><% PutComboFromList("wireType", "class=""input2", ";", ";1;2;3", ";Settlement;Payment Request;Affiliate", "", "") %></td>
		</tr>
		<tr>
			<td>Settlement #</td>
			<td><input type="text" class="input1" size="5" dir="ltr" name="PayID" value="" /></td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Amount</th></tr>
		<tr>
			<td>
				<select name="iCurrency">
					<option value=""></option>
					<%
						Dim i As Integer
						For i = 0 To CType(eCurrencies.MAX_CURRENCY, Integer)
						%>
						<option value="<%=i%>"><%=dbPages.GetCurText(i)%></option>
						<%
					Next
					%>
				</select>
				<input type="text" class="input1" size="5" dir="ltr" name="amountFrom" value="" />
				To <input type="text" class="input1" size="5" dir="ltr" name="amountTo" value="" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Parse Results</th></tr>
		<tr>
			<td>
				From <input type="text" class="input1" size="5" dir="ltr" name="ParseFrom" value="" />
				To <input type="text" class="input1" size="5" dir="ltr" name="ParseTo" value="" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl"  runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td>
				<select name="PageSize">
					<option value="20" selected="selected">20 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td style="text-align:right;">
				<input type="reset" value="RESET" />
				<input type="submit" name="Action" value="SEARCH"><br />
			</td>
		</tr>
	</table>
    </form>
	<hr class="filter" />
    <form action="wire_search_data.asp" target="frmBody" method="get">
	<input type="hidden" name="Source" value="filter" />
	<input type="hidden" name="PageSize" value="20" />
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Wire #</th></tr>
		<tr>
			<td>
				<input type="text" class="input1" size="5" dir="ltr" name="IdFrom" value="">
				To <input type="text" class="input1" size="5" dir="ltr" name="IdTo" value="">
			</td>
			<td style="text-align:right;">
				<input type="submit" name="Action" value="SEARCH"><br />
			</td>
		</tr>
	</table>
    </form>
</asp:Content>