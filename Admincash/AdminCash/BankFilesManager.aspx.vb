﻿Partial Class EpaUpload
	Inherits System.Web.UI.Page
	Implements System.Collections.IComparer
    Private currentFolder As Netpay.Bll.BankHandler.EpaFolder
    Private manager As Netpay.Bll.BankHandler.BankHandlerManagerBase
    Private handler As Netpay.Bll.BankHandler.BankHandlerBase

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        Server.ScriptTimeout = 5000
        MyBase.OnPreInit(e)
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        'Security.CheckPermission(lblPermissions)
        Dim currentRootFolder As String = Request("root")
        Select Case currentRootFolder
            Case "Auth" : manager = Netpay.Bll.BankHandler.AuthHandlerManager.Current
            Case "EPA" : manager = Netpay.Bll.BankHandler.EpaHandlerManager.Current
            Case "CHB" : manager = Netpay.Bll.BankHandler.ChbHandlerManager.Current
            Case "Fraud" : manager = Netpay.Bll.BankHandler.FraudHandlerManager.Current
        End Select
        currentFolder = [Enum].Parse(GetType(Netpay.Bll.BankHandler.EpaFolder), dbPages.TestVar(Request("f"), -1, "New"))
        If Not IsPostBack Then
            ddlEpaType.DataSource = manager.Handlers.Select(Function(x) x.BankDirName) : ddlEpaType.DataBind()
            ddlEpaType.SelectedIndex = 0
            ddlMoveTo.DataSource = manager.Handlers.Select(Function(x) x.BankDirName) : ddlMoveTo.DataBind()
            ddlMoveTo.Items.Insert(0, New ListItem("Move to Folder:", ""))
            ddlMoveTo.SelectedIndex = 0
            If Request("bank") <> "" Then ddlEpaType.Text = Request("bank")
            If Request("download").EmptyIfNull() <> "" Then
                Dim destFileName = Request("download")
                If (currentFolder <> BankHandler.EpaFolder.New) Then
                    destFileName = Domain.Current.MapTempPath(destFileName)
                    Netpay.Crypt.SymEncryption.GetKey(Domain.Current.EncryptionKeyNumber).DecryptFile(System.IO.Path.Combine(RootFolder, Request("download")), destFileName)
                End If
                WebUtils.SendFile(destFileName)
                Response.End()
            End If
        End If
        pageMainHeading.InnerHtml = "Bank " & currentRootFolder & " File Manager"
        If Request("Delete") <> "" Then
            Try
                System.IO.File.Delete(Request("Delete"))
            Catch ex As Exception
                txtError.Text = "Error - " & ex.Message & "<br/>"
            End Try
        End If
        MyBase.OnLoad(e)
    End Sub

    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        tlbTop.Clear()
        AddDir(Netpay.Bll.BankHandler.EpaFolder.New, True)
        AddDir(Netpay.Bll.BankHandler.EpaFolder.Pending, True)
        AddDir(Netpay.Bll.BankHandler.EpaFolder.Error, True)
        AddDir(Netpay.Bll.BankHandler.EpaFolder.Archive, False)

        Dim strOut As New StringBuilder
        strOut.Append("<table width=""100%"" class=""tableThin"" border=""0"" cellpadding=""1"" cellspacing=""0"">")
        strOut.Append("<tr><th width=""70%"">File Name</th><th>Last Update</th><th>Delete</th></tr>")
        RecDrawFiles(New System.IO.DirectoryInfo(RootFolder), "", strOut, IIf(currentFolder = Netpay.Bll.BankHandler.EpaFolder.Archive, 60, Integer.MaxValue))
        strOut.Append("</table>")
        txtFiles.Text = strOut.ToString()
        tdFileMove.Visible = True '(currentFolder <> Netpay.Bll.BankHandler.EpaFolder.New And currentFolder <> Netpay.Bll.BankHandler.EpaFolder.Pending)
    End Sub

    Protected ReadOnly Property RootFolder As String
        Get
            handler = manager.Handlers.Where(Function(x) x.BankDirName = ddlEpaType.Text).FirstOrDefault()
            Return handler.GetPath(currentFolder)
        End Get
    End Property

    Protected Sub Upload_Click(sender As Object, e As EventArgs)
        Dim fileName As String
        If ddlEpaType.Text = "" Then Exit Sub
        Dim folderName = System.IO.Path.GetDirectoryName(RootFolder.TrimEnd("\", "/")) & "\New"
        For Each fs As String In Request.Files
            Dim f As HttpPostedFile = Request.Files(fs)
            If f.ContentLength > 0 Then
                fileName = folderName & "\" & System.IO.Path.GetFileName(f.FileName)
                dbPages.ExecSql("Insert Into tblEpaFileLog(nul, StoredFileName, OriginalFileName) Values(" & eLogAction.eLA_Uploaded & ", '" & fileName.Replace("'", "''") & "', '" & f.FileName.Replace("'", "''") & "')")
                f.SaveAs(fileName)
            End If
        Next
    End Sub

    Private Function Compare(o1 As Object, o2 As Object) As Integer Implements IComparer.Compare
        Return System.Math.Sign((CType(o2, System.IO.FileInfo).LastWriteTime - CType(o1, System.IO.FileInfo).LastWriteTime).TotalMilliseconds)
    End Function

    Protected Sub RecDrawFiles(dir As System.IO.DirectoryInfo, padd As String, strOut As StringBuilder, maxItems As Integer)
        Dim directories() As System.IO.DirectoryInfo = dir.GetDirectories()
        For Each d As System.IO.DirectoryInfo In directories
            strOut.Append("<tr><td><br/></td></tr><tr><td><img src=""/NPCommon/Images/icon_folder.gif"" align=""top""> " & padd & d.Name & "<br/></td><td></td><td></td></tr>")
            RecDrawFiles(d, padd & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", strOut, maxItems)
        Next
        Dim files() As System.IO.FileInfo = dir.GetFiles()
        If (files Is Nothing Or files.Length = 0) Then
            If padd <> "" Then strOut.Append("<tr><td style=""color:#cccccc!important;"">" & padd & "- Empty" & "</td></tr>")
            Exit Sub
        End If
        Array.Sort(files, Me)
        Dim nItem As Integer = 0
        For Each f As System.IO.FileInfo In files
            nItem += 1
            If nItem > maxItems Then Exit For
            strOut.Append("<tr><td>" & padd & "<a target=""_blank"" href=""?root=" & Request("root") & "&f=" & Server.UrlEncode(dir.Name) & "&bank=" & Server.UrlEncode(handler.BankDirName) & "&download=" & Server.UrlEncode(f.Name) & """>" + f.Name & "</td><td>" & f.LastWriteTime.ToString("dd/MM/yyy HH:mm") & "</td><td><input type=""checkbox"" name=""selectedFile"" value=""" & f.FullName & """/></td></tr>")
        Next
        If nItem < files.Length Then strOut.Append("<tr><td style=""color:#cccccc!important;"">" & padd & "- More files exist" & "</td></tr>")
    End Sub

    Private Function DirFileCount(dir As String) As Integer
        Dim nRet As Integer = System.IO.Directory.GetFiles(dir).Length
        Dim nDirs() As String = System.IO.Directory.GetDirectories(dir)
        For Each d As String In nDirs
            nRet += DirFileCount(d)
        Next
        Return nRet
    End Function

    Public Sub btnDelete_Click(sender As Object, e As EventArgs)
        Dim fileNames() As String = Request.Form.GetValues("selectedFile")
        If fileNames Is Nothing Then Return
        For Each f As String In fileNames
            Try
                System.IO.File.Delete(f)
            Catch ex As Exception
                txtError.Text = "Error - " & ex.Message & "<br/>"
            End Try
        Next
    End Sub

    Public Sub btnUpdate_Click(sender As Object, e As EventArgs)
        Dim fileNames() As String = Request.Form.GetValues("selectedFile")
        If fileNames Is Nothing Then Return
        ddlEpaType.Text = ddlMoveTo.Text
        currentFolder = BankHandler.EpaFolder.Pending
        For Each f As String In fileNames
            Try
                System.IO.File.Move(f, RootFolder & "\" & System.IO.Path.GetFileName(f))
            Catch ex As Exception
                txtError.Text = "Error - " & ex.Message & "<br/>"
            End Try
        Next
    End Sub

    Private Sub AddDir(dir As Netpay.Bll.BankHandler.EpaFolder, bCount As Boolean)
        Dim tabTitle As String = dir.ToString()
        If bCount Then tabTitle = tabTitle & " (" & DirFileCount(System.IO.Path.GetDirectoryName(RootFolder.TrimEnd("\", "/")) & "\" & dir.ToString()) & ")"
        tlbTop.AddItem(tabTitle, "?f=" & Server.UrlEncode(dir) & "&root=" & Server.UrlEncode(manager.BasePath) & "&bank=" & Server.UrlEncode(ddlEpaType.Text), (currentFolder = dir))
    End Sub

End Class
