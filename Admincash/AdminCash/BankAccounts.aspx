<%@ Page Language="VB" Inherits="htmlInputs" %>
<%@ Register Src="Filter_DateRange.ascx" TagName="FilterDateRange" TagPrefix="Uc1" %>
<script language="vbscript" runat="server">
    Protected Overrides Sub OnLoad(e As EventArgs)
        If Not IsPostBack Then
            ddlProvider.DataSource = Netpay.Bll.Wires.Provider.Cache
            ddlProvider.DataBind()
        End If
        MyBase.OnLoad(e)
    End Sub

    Protected Sub OnSave(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ba = New Netpay.Bll.Wires.WireBankAccount()
        ba.ProviderID = ddlProvider.Value
        ba.AccountBranch = txtAccountBranch.Text
        ba.AccountName = txtAccountName.Text
        ba.AccountNumber = txtAccountNumber.Text
        ba.CurrencyISOCode = ddlCurrency.SelectedCurrencyIso
        ba.Save()
    End Sub

    Protected Sub OnDelete(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim ba = Netpay.Bll.Wires.WireBankAccount.Load(e.CommandArgument)
        If ba IsNot Nothing Then ba.Delete()
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        Dim sf = New Netpay.Bll.Wires.WireBankAccount.SearchFilters()
        If ddlProvider.IsSelected Then sf.ProviderID = New List(Of String) From { ddlProvider.Value }
        rptList.DataSource = Netpay.Bll.Wires.WireBankAccount.Search(sf, Nothing)
        rptList.DataBind()
        MyBase.OnPreRender(e)
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Bank Accounts</title>
    <link rel="stylesheet" type="text/css" media="all" href="../StyleSheet/calendar-blue.css" title="winter" />
    <link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/Analytics.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <table align="center" class="formThin" width="95%">
            <tr>
                <td>
                    <asp:Label ID="lblPermissions" runat="server" />
                    <span id="pageMainHeading">BANK ACCOUNTS</span>
                </td>
                <td>
                    Provider ID:
                    <netpay:DropDownBase runat="server" ID="ddlProvider" DataTextField="FriendlyName" DataValueField="Name" AutoPostBack="true" />
                </td>
            </tr>
        </table>
        <br />
        <table width="95%" class="formNormal" align="center">
            <tr>
                <th>ID</th>
                <th>Branch Number</th>
                <th>Account Name</th>
                <th>Account Number</th>
                <th>Currency</th>
                <th>Action</th>
            </tr>
            <tr runat="server" id="trEdit" Visible='<%# Not String.IsNullOrEmpty(ddlProvider.Value) %>'>
                <td>--</td>
                <td><asp:TextBox runat="server" ID="txtAccountBranch" /></td>
                <td><asp:TextBox runat="server" ID="txtAccountName" /></td>
                <td><asp:TextBox runat="server" ID="txtAccountNumber" /></td>
                <td><netpay:CurrencyDropDown runat="server" ID="ddlCurrency" /></td>
                <td><asp:Button runat="server" Text="Save" OnClick="OnSave" /></td>
            </tr>
            <asp:Repeater runat="server" id="rptList">
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("ID")%></td>
                        <td><%# Eval("AccountBranch")%></td>
                        <td><%# Eval("AccountName")%></td>
                        <td><%# Eval("AccountNumber")%></td>
                        <td><%# Eval("CurrencyISOCode")%></td>
                        <td><asp:Button runat="server" OnCommand="OnDelete" CommandArgument='<%# Eval("ID")%>' Text="Delete" /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </form>
</body>
</html>
