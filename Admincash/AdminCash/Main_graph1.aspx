<%@ Page Language="VB" Buffer="false" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="radC" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Charting" TagPrefix="radC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

	Dim sSQL, sPageHeading, sGraphLabelDate As String
	Dim iReader, iReader2 As SqlDataReader
	Dim dDate As Date = date.Now.Date()
	Dim i, nCurrency As Integer
	 
    Public Sub FillGraph(sSQL As String, dDate As Date, fGraph As Telerik.Web.UI.RadChart, bCol As Byte)
        Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
        If iReader.HasRows() Then
            iReader.Read()
            Dim dRowDate As Date = CDate(iReader(0)).Date
            For i As Integer = 0 To 9
                Dim NumberUnits As Integer = 0
                Dim dProcessDate As Date = DateAdd("d", -i, dDate)
                If dRowDate = dProcessDate Then
                    NumberUnits = dbPages.TestVar(iReader(1), 0, -1, 0)
                    If iReader.Read() Then dRowDate = CDate(iReader(0)).Date
                End If
                If bCol = 0 Then
                    If i = 0 Then
                        fGraph.PlotArea.XAxis.AddItem("Today")
                    Else
                        fGraph.PlotArea.XAxis.AddItem(dProcessDate.ToString("ddd"))
                    End If
                End If
                fGraph.Series.Item(bCol).AddItem(NumberUnits)
            Next
        End If
        iReader.Close()
    End Sub
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Page.Culture = "en-us"
        Dim fGraph() As Telerik.Web.UI.RadChart = {Chart1}
        Chart1.PlotArea.XAxis.AutoScale = False
	
		Dim sWhereLimit As String = String.Empty
		If Security.IsLimitedMerchant Then sWhereLimit &= " AND Company_ID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
		'Get number of credits used from maxmind
        sSQL = "SELECT CAST(InsertDate AS date) dDate, COUNT(IsDuplicateAnswer) NumberUnits FROM [Log].FraudDetection WITH (NOLOCK)" & _
  " WHERE IsDuplicateAnswer=0 AND CAST(InsertDate AS date)>DATEADD(day, - 10, GETDATE())" & sWhereLimit & _
  " GROUP BY CAST(InsertDate AS date) ORDER BY CAST(InsertDate AS date) DESC"
		FillGraph(sSQL, dDate, fGraph(0), 0)
		
		'Get number of duplicate credits used
        sSQL = "SELECT CAST(InsertDate AS date) dDate, COUNT(IsDuplicateAnswer) NumberUnits FROM [Log].FraudDetection WITH (NOLOCK)" & _
  " WHERE IsDuplicateAnswer=1 AND CAST(InsertDate AS date)>DATEADD(day, - 10, GETDATE())" & sWhereLimit & _
  " GROUP BY CAST(InsertDate AS date) ORDER BY CAST(InsertDate AS date) DESC"
		FillGraph(sSQL, dDate, fGraph(0), 1)
	End Sub
	
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin:0;padding:0;text-align:center;">

	<table width="330" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<%
			'-------------------------------------------------------------------------------------------------
			'	Maxmind credits
			'-------------------------------------------------------------------------------------------------
			    sPageHeading = dbPages.ExecScalar("SELECT TOP 1 ReturnQueriesRemaining FROM [Log].FraudDetection WHERE (IsDuplicateAnswer = 0) AND LTrim(RTrim(ReturnQueriesRemaining))<>'' ORDER BY InsertDate DESC")
				If Trim(sPageHeading) <> "" Then
					sPageHeading = "(" & FormatNumber(sPageHeading, 0, 0, 0, -1) & " credits)"
				Else
					sPageHeading = "(error)"
				End If
			%>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
			<tr>
				<td class="subSectionHeading" style="color:#484848;font-size:11px;">Risk Management<br /></td>
				<td align="right" style="font-size:11px;"><%= sPageHeading %><br /></td>
			</tr>
			</table>
			<hr style="width:98%;height:1px;color:#C0C0C0;" />
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
			<tr>
				<td valign="top" align="center">
					<radC:RadChart ID="Chart1" runat="server" Palette="Pastel" ShadowColor="180, 0, 0, 0" Height="200px" Width="320px" Margins-Bottom="50" Margins-Left="40" Margins-Right="5" Margins-Top="5" ImageQuality="HighQuality" TextQuality="AntiAlias" DefaultType="StackedBar" RadControlsDir="~/Include/RadControls/">
                        <PlotArea>
                            <EmptySeriesMessage TextBlock-Text="Did not find data for building graph" TextBlock-Appearance-TextProperties-Color="black" TextBlock-Appearance-TextProperties-Font="Arial, 12pt"></EmptySeriesMessage>
                            <DataTable Appearance-Position-X="1" Appearance-Position-Y="298" Appearance-Dimensions-Height="49" Appearance-Dimensions-Width="764"></DataTable>
                            <Appearance>
                                <Border Color="193, 214, 221" />
                                <FillStyle FillType="Solid" MainColor="250, 250, 250"></FillStyle>
                            </Appearance>
                            <XAxis Appearance-Color="193, 214, 221">
                                <AxisLabel TextBlock-Appearance-TextProperties-Font="Arial, 10pt" Appearance-RotationAngle="90">
                                    <Marker Appearance-FillStyle-FillType="Solid" Appearance-FillStyle-MainColor="154, 153, 129"></Marker>
                                </AxisLabel>
                            </XAxis>
                            <YAxis Appearance-Color="193, 214, 221">
                                <AxisLabel TextBlock-Appearance-TextProperties-Font="Arial, 10pt" Appearance-RotationAngle="90">
                                    <Marker Appearance-FillStyle-FillType="Solid" Appearance-FillStyle-MainColor="154, 153, 129"></Marker>
                                </AxisLabel>
                            </YAxis>
                        </PlotArea>
                        <ChartTitle Appearance-Visible="False" TextBlock-Text="������ �������" Appearance-Dimensions-Paddings="10" Appearance-Dimensions-Margins="10" TextBlock-Appearance-TextProperties-Font="Arial, 21.75pt" TextBlock-Appearance-TextProperties-Color="81, 103, 114">
                            <Appearance>
                                <FillStyle FillType="Solid" ></FillStyle>
                            </Appearance>
                        </ChartTitle>
                        <Legend Appearance-Location="InsidePlotArea" Appearance-ItemTextAppearance-TextProperties-Font="Arial, 8.25pt">
                            <Appearance>
                                <Border Color="193, 214, 221" />
                                <FillStyle FillType="Solid" MainColor="241, 253, 255"></FillStyle>
                            </Appearance> 
                        </Legend>
                        <Appearance>
                            <FillStyle FillType="Solid" MainColor="#ffffff"></FillStyle>
                            <Border Color="#ffffff" />
                        </Appearance>
                        <Series>
                            <radC:ChartSeries Name="Credits" Appearance-LabelAppearance-Visible="False">
                                <Appearance>
                                    <Border Color="DimGray" />
                                    <FillStyle FillType="Solid" MainColor="#ddb77b" SecondColor="#f9f3e7"></FillStyle>
                                </Appearance>
                            </radC:ChartSeries>
                            <radC:ChartSeries Name="Recycled" Appearance-LabelAppearance-Visible="False">
                                    <Appearance>
                                    <Border Color="DimGray" />
                                    <FillStyle FillType="Solid" MainColor="#83b4c7" SecondColor="#eaf2f5"></FillStyle>
                                </Appearance>                     
                            </radC:ChartSeries>
                        </Series>
					</radC:RadChart>
				</td>
			</tr>
			</table>
			<br />
		</td>
	</tr>
	</table>

</body>
</html>