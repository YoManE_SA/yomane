﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Xml.Linq;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using System.IO;
using System.Linq;
using Netpay.Emails.DAL;
using Netpay.Web;

public partial class Emails_MessageEditor : System.Web.UI.Page
{
	private int messageId = 0;
	protected override void OnInit(EventArgs e)
	{
		//Security.CheckPermission(lblPermissions);
		base.OnInit(e);
	}

	protected override void OnLoad(EventArgs e)
	{
        base.OnLoad(e);
		messageId = dbPages.TestVar(Request.QueryString["MessageId"], 1, 0, 0);
		if (IsPostBack) return;
		var msg = Netpay.Emails.Message.Load(messageId);
		btn_backToPost.Visible = (msg != null);
		if (msg != null)
		{
			txtSubject.Text = "RE: " + msg.Subject;
			if (!string.IsNullOrEmpty(Request["ReplyToAddress"]))
				rblTo.Items.Add(new ListItem(Request["ReplyToAddress"]));
			else rblTo.Items.Add(new ListItem(msg.EmailFrom));
			var mb = Netpay.Emails.MailBox.Load(msg.MailBoxID);
			rblFrom.Items.Add(new ListItem(mb.EmailAddress, mb.ID.ToString()));
		}
		else {
			rblFrom.DataSource = Netpay.Emails.MailBox.Load();
			rblFrom.DataTextField = "EmailAddress";
			rblFrom.DataValueField = "ID";
			rblFrom.DataBind();
		}
		txtFrom.Visible = false;
		//rblFrom.Items.Add(new ListItem("Other:"));

		rblTo.Items.Add(new ListItem("Other:", string.Empty));
		rblCC.Items.Add(new ListItem(Netpay.Web.WebUtils.LoggedUser.EmailAddress, Netpay.Web.WebUtils.LoggedUser.EmailAddress));
		rblCC.Items.Add(new ListItem("Other:", string.Empty));

		rblFrom.SelectedIndex = 0; rblTo.SelectedIndex = 0;
		ifrmBody.Attributes.Add("src", string.Format("Emails_MessageBody.aspx?replyToMessageId={0}&template={1}", messageId, lb_ReplyTemplete.SelectedValue));
		LoadTemplateList();
	}

    public void BackToPost_Click(object sender, EventArgs e)
    {
        Response.Redirect("Emails_MessageView.aspx?MessageId=" + Request.QueryString["MessageId"]);
    }

	protected void SendClick(object sender, EventArgs e)
	{
        string allowedExtention = ",png,doc,docx,txt,jpeg,pdf,bmp,jpg,zip,rar,xlsx,xls,ppt,pptx,";
		System.Collections.Generic.Dictionary<string, System.IO.Stream> attachments = null;
        try
        {
            if (fu_atachment.HasFile)
            {
				string fileExt = System.IO.Path.GetExtension(fu_atachment.FileName);
				attachments = new Dictionary<string, Stream>();
				if (allowedExtention.IndexOf("," + fileExt + ",") > -1)
					attachments.Add(fu_atachment.FileName, new System.IO.MemoryStream(fu_atachment.FileBytes));
            }
			int replyToMessageId;
			short mailboxId = 0;
			string mailForm = null;
			if (string.IsNullOrEmpty(rblFrom.SelectedValue)) throw new Exception("From field not filled");
			mailboxId = short.Parse(rblFrom.SelectedValue);
			mailForm = Netpay.Emails.MailBox.Load(mailboxId).EmailAddress;
			if (!int.TryParse(Request.QueryString["MessageId"], out replyToMessageId)) replyToMessageId = 0;
			Netpay.Emails.Execute.SendMessage(WebUtils.DomainHost, mailboxId, (replyToMessageId == 0 ? null : (int?)replyToMessageId), mailForm, Security.Username,
				(rblTo.SelectedValue != string.Empty ? rblTo.SelectedValue : txtTo.Text),
				(rblCC.SelectedValue != string.Empty ? rblCC.SelectedValue : txtCC.Text), 
				txtSubject.Text.Trim(), HttpUtility.UrlDecode(hf_txtBody.Value), string.Empty, attachments);
			Response.Redirect("Emails_MessageView.aspx?MessageId=" + Request.QueryString["MessageId"]);
        }
        catch (Exception ex)
        {
            Netpay.Infrastructure.Logger.Log(ex);
            lblError.Text = ex.Message;
			lblError.Visible = true;
        }
	}

	protected void LoadTemplateList()
	{
		lb_ReplyTemplete.Items.Clear();
        string[] filePaths = Netpay.Emails.Templates.GetTemplates(WebUtils.DomainHost);
		lb_ReplyTemplete.Items.Add(new ListItem("Select a templete...", string.Empty));
		foreach (string filename in filePaths)
			lb_ReplyTemplete.Items.Add(new ListItem(System.IO.Path.GetFileNameWithoutExtension(filename), filename));
	}

	protected void TemplateChanged(object sender, EventArgs e)
	{
		ifrmBody.Attributes["src"] = string.Format("Emails_MessageBody.aspx?replyToMessageId={0}&template={1}", messageId, lb_ReplyTemplete.SelectedValue);
	}

}