<!--#include file="../include/const_globalData.asp" -->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/func_controls.asp"-->
<!--#include file="../include/func_transCharge.asp"-->
<!--#include file="../include/CalcTransPayment.asp"-->
<%
	Function DrawArrCombo(sName, parr, def)
		dim sFound, i
		sFound = false
		DrawArrCombo = "<select class=""inputData"" name=""" & sName & """>"
		DrawArrCombo = DrawArrCombo & "<option value=""0"">"
		For i = LBound(parr) to UBound(parr)
			if parr(i) = def Then 
				sFound = True
				DrawArrCombo = DrawArrCombo & "<option selected value=""" & parr(i) & """>" & parr(i)
			Else 
				DrawArrCombo = DrawArrCombo & "<option value=""" & parr(i) & """>" & parr(i)
			End if
		Next
		if (Not sFound) And (def <> 0) Then DrawArrCombo = DrawArrCombo & "<option selected value=""" & def & """>" & def
		DrawArrCombo = DrawArrCombo & "</select>"
	End Function

	nCompanyID = dbText(request("companyID"))
	sTarget = "merchant_data.asp?CompanyID=" & nCompanyID & "&ShowTrSec=" & dbText(request("ShowTrSec"))
	sCompNum = dbText(request("compnum"))

	If trim(request("Action"))="update" Then
		Dim RRState, bChange, RRPayID, RRKCur : bChange = False
		RRState = TestNumVar(Request("RRState"), 0, -1, 0)
		Set rsData = oledbData.Execute("Select RRState, RRKeepAmount, RRKeepCurrency, RRPayID From tblCompany WHERE ID=" & nCompanyID)
		If Not rsData.EOF Then
			RRPayID = TestNumVar(rsData("RRPayID"), 0, -1, 0)
			RRKCur = rsData("RRKeepCurrency")
			bChange = (RRState <> rsData("RRState")) Or (TestNumVar(Request("RRKeepAmount"), 0, -1, 0) <> rsData("RRKeepAmount")) Or (TestNumVar(Request("RRKeepCurrency"), 0, -1, 0) <> rsData("RRKeepCurrency"))
		End If
		rsData.Close

		sPayDate1 = dbText(request("fromDay1") & request("toDay1") & request("payDay1"))
		sPayDate2 = dbText(request("fromDay2") & request("toDay2") & request("payDay2"))
		sPayDate3 = dbText(request("fromDay3") & request("toDay3") & request("payDay3"))
		
		sSQL="UPDATE tblCompany SET " &_
		"PreferredWireType=" & TestNumVar(request("PreferredWireType"), 0, -1, 0) & "," &_
		"PayingDaysMargin=" & TestNumVar(request("PayingDaysMargin"), 0, 127, 0) & "," &_
		"PayingDaysMarginInitial=" & TestNumVar(request("PayingDaysMarginInitial"), 0, 127, 0) & "," &_
		"payingDates1='" & sPayDate1 & "'," &_
		"payingDates2='" & sPayDate2 & "'," &_
		"payingDates3='" & sPayDate3 & "'," &_
		"SecurityDeposit=" & TestNumVar(Request("SecurityDeposit"), 0, -1, 0) & "," &_
		"SecurityPeriod=" & TestNumVar(Request("SecurityPeriod"), 0, 9999, 0) & "," &_
		"RREnable=" & TestNumVar(Request("RREnable"), 0, 1, 0) & "," &_
		"RRAutoRet=" & TestNumVar(Request("RRAutoRet"), 0, 1, 0) & "," &_
		"RRState=" & TestNumVar(Request("RRState"), 0, -1, 0) & "," &_
		"RRKeepAmount=" & TestNumVar(Request("RRKeepAmount"), 0, -1, 0) & "," &_
		"RRKeepCurrency=" & TestNumVar(Request("RRKeepCurrency"), 0, -1, 0) & "," &_
		"RRComment='" & DBText(Request("RRComment")) & "'," &_
		"PayPercent=" & TestNumVar(Request("PayPercent"), 0, 100, 100) & "," &_
		"PaymentReceiveCurrency=" & int(request("PaymentReceiveCurrency")) & "," &_
		"IsChargeVAT=" & TestNumVar(request("isChargeVAT"),1,1,0) & "," &_
		"BillingCompanys_id=" & IIF(request("BillingCompanys_id") = "", "Null", request("BillingCompanys_id")) & _
		" WHERE ID=" & nCompanyID

		For i = 0 To MAX_CURRENCY
            If Request("CurrencySettings_" & i & "_Exist") = "1" Then
                Dim updateSql 
                updateSql = "Update Setting.SetMerchantSettlement Set" & _
                    "  IsShowToSettle=" & TestNumVar(Request("IsShowToSettle" & i), 0, 1, 0) & _
                    ", IsAutoInvoice=" & TestNumVar(Request("IsAutoInvoice" & i), 0, 1, 0) & _
                    ", MinSettlementAmount=" & Request("MinSettlementAmount" & i) & _
                    ", IsWireExcludeDebit=" & TestNumVar(Request("IsWireExcludeDebit" & i), 0, 1, 0) & _
                    ", IsWireExcludeRefund=" & TestNumVar(Request("IsWireExcludeRefund" & i), 0, 1, 0) & _
                    ", IsWireExcludeFee=" & TestNumVar(Request("IsWireExcludeFee" & i), 0, 1, 0) & _
                    ", IsWireExcludeChb=" & TestNumVar(Request("IsWireExcludeChb" & i), 0, 1, 0) & _
                    ", IsWireExcludeCashback=" & TestNumVar(Request("IsWireExcludeCashback" & i), 0, 1, 0) & _
                    "  Where Merchant_ID=" & nCompanyID & " And Currency_ID=" & i 
                If ExecSql(updateSql) = 0 Then
                    ExecSql("Insert Into Setting.SetMerchantSettlement(Merchant_ID, Currency_ID) Values(" & nCompanyID & ", " & i & ")")
                    ExecSql(updateSql)
                End If
            End If
		Next

		'response.write Replace(sSQL, ",", ", ")
		oledbData.Execute sSQL
		If bChange Then
			If RRPayID <> 0 Then
				Set xPayment = New CPayment
				xPayment.InitByPayID 0, nCompanyID, RRKCur
				xPayment.CreatePayment(Now)
				xPayment.AddRetReserve(RRPayID)
				xPayment.UpdatePayment()
				oledbData.Execute "UPDATE tblCompany SET RRPayID=0 WHERE ID=" & nCompanyID
				RRPayID = 0
			End If	
			If RRState = 4 And RRPayID = 0 Then
				Set xPayment = New CPayment
				xPayment.InitByPayID 0, nCompanyID, TestNumVar(Request("RRKeepCurrency"), 0, -1, 0)
				xPayment.CreatePayment(Now)
				xPayment.RollingReserve = TestNumVar(Request("RRKeepAmount"), 0, -1, 0)
				xPayment.UpdatePayment()
				RRPayID = xPayment.RRID
				oledbData.Execute "UPDATE tblCompany SET RRPayID=" & RRPayID & " WHERE ID=" & nCompanyID
			End If	
		End If
	ElseIf Request("RecalcDates") = "1" Then
		RecalcMerchantPO nCompanyID, 0
	ElseIf Request("UpdateCCFF") = "1" Then
		CCFF_ID = TestNumVar(Request("CCFF_ID"), 0, -1, 0)
		CCFF_PaymentMethod = TestNumVar(Request("CCFF_PaymentMethod"), 0, -1, 0)
		CCFF_Currency = TestNumVar(Request("CCFF_Currency"), 0, -1, 0)
		CCFF_AboveAmount = TestNumVar(Request("CCFF_AboveAmount"), 0, -1, 0)
		CCFF_FixedFee = TestNumVar(Request("CCFF_FixedFee"), 0, -1, 0)
		CCFF_PrecentFee = TestNumVar(Request("CCFF_PrecentFee"), 0, -1, 0)
		If CCFF_ID = 0 Then 
			oleDbData.Execute("Insert Into tblCompanyCreditFeesFloor(CCFF_CompanyID, CCFF_PaymentMethod, CCFF_Currency, CCFF_AboveAmount, CCFF_FixedFee, CCFF_PrecentFee)Values(" & _
				nCompanyID & "," & CCFF_PaymentMethod & "," & CCFF_Currency & "," & CCFF_AboveAmount & "," & CCFF_FixedFee & "," & CCFF_PrecentFee & ")")
			CCRM_ID = ExecScalar("Select Max(CCFF_ID) From tblCompanyCreditFeesFloor", 0)
		ElseIf CCFF_ID < 0 Then
			oleDbData.Execute("Delete From tblCompanyCreditFeesFloor Where CCFF_ID=" & -CCFF_ID)
		Else
			oleDbData.Execute("Update tblCompanyCreditFeesFloor Set" & _
				" CCFF_PaymentMethod=" & CCFF_PaymentMethod & _
				",CCFF_Currency=" & CCFF_Currency & _
				",CCFF_AboveAmount=" & CCFF_AboveAmount & _
				",CCFF_FixedFee=" & CCFF_FixedFee & _
				",CCFF_PrecentFee=" & CCFF_PrecentFee & _
				" Where CCFF_ID=" & CCFF_ID)
		End If
	End If
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<script type="text/javascript" language="javascript" src="../js/func_common.js"></script>
	<script type="text/javascript" language="javascript">
		function checkFields() {
			if (document.getElementById('RRState1').checked && !(document.getElementById('RREnable').checked || document.getElementById('RRAutoRet').checked)) {
				if (confirm('Rolling Reserve - Reserve and/or Release must be enabled when choosing to hold dynamically\nWould you like to enable them now?'))
					document.getElementById('RREnable').checked = document.getElementById('RRAutoRet').checked = true;
			}
			if (document.getElementById('RRState3').checked && isNaN(parseInt(document.getElementById('RRKeepAmount1').value))) {
				alert('Rolling Reserve - Please set amount when choosing to hold a fixed amount'); return false;
			} else {
				document.getElementById('RRKeepAmount1').name = 'RRKeepAmount';
				document.getElementById('RRKeepCurrency1').name = 'RRKeepCurrency';
			}

			if (document.getElementById('RRState4').checked && isNaN(parseInt(document.getElementById('RRKeepAmount2').value))) {
				alert('Rolling Reserve - please set amount when choosing to record an external hold'); return false;
			} else {
				document.getElementById('RRKeepAmount2').name = 'RRKeepAmount';
				document.getElementById('RRKeepCurrency2').name = 'RRKeepCurrency';
			}

			if (document.getElementById('RRState1').checked) {
				var SecurityDeposit = parseInt(document.getElementById('SecurityDeposit').value);
				var SecurityPeriod = parseInt(document.getElementById('SecurityPeriod').value);
				if (isNaN(SecurityDeposit) || isNaN(SecurityPeriod) || (SecurityDeposit <= 0) || (SecurityPeriod <= 0)) {
					alert('Rolling Reserve - Hold Percentage and/or Hold Period fields are incorrect');
					return false;
				}
				return true;
			}
		}

		function CheckRR() {
			document.getElementById('lblRREnable').disabled = document.getElementById('lblRRAutoRet').disabled =
				document.getElementById('RREnable').disabled = document.getElementById('RRAutoRet').disabled = !document.getElementById('RRState1').checked;
			document.getElementById('lblRRAmount1').disabled = document.getElementById('RRKeepCurrency1').disabled = document.getElementById('RRKeepAmount1').disabled = !document.getElementById('RRState3').checked;
			document.getElementById('lblRRAmount2').disabled = document.getElementById('RRKeepCurrency2').disabled = document.getElementById('RRKeepAmount2').disabled = !document.getElementById('RRState4').checked;
		}

		function showSection(SecNum) {
			for (i = 0; i <= 7; i++) {
				if (document.getElementById("Tbl" + i)) {
					if (i == SecNum) {
						trSecObj = document.getElementById("Tbl" + SecNum)
						oListToggleObj = document.getElementById("oListToggle" + SecNum)
						if (trSecObj.style.display == "none") {
							oListToggleObj.src = "../images/tree_collapse.gif";
							trSecObj.style.display = "";
						}
						else {
							oListToggleObj.src = "../images/tree_expand.gif";
							trSecObj.style.display = "none";
						}
					}
					//else {
					//	document.getElementById("oListToggle" + i).src = "../images/tree_expand.gif";
					//	document.getElementById("Tbl" + i).style.display = "none";
					//}
				}
			}
        }

        function ShowCurrencyFeeRow() 
        {
            var currencyDropDown = document.getElementById('MerchantCurrencyList');
            if (currencyDropDown.selectedIndex < 0) return;
            var currencyID = currencyDropDown.options[currencyDropDown.selectedIndex].value;
            currencyDropDown.options.removeChild(currencyDropDown.options[currencyDropDown.selectedIndex]);
            document.getElementById('CurrencyFee' + currencyID).style.display = '';
            document.getElementById('CurrencySettings_' + currencyID + '_Exist').value = 1;
        }

	</script>
	<style type="text/css">
		input.inputData { width:90%; }
	</style>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">
<form action="merchant_dataPayInfo.asp" name="frmPayInfoUpd" method="get" onsubmit="return checkFields()">
<input type="hidden" name="Action" value="update">
<input type="hidden" name="companyID" value="<%= nCompanyID %>">
<table align="center" border="0" cellpadding="0" cellspacing="2" width="95%">
	<tr>
		<td>
			<%
				sSQL="SELECT IsNull(dbo.GetMerchantBankAccountCurrencyList(ID), '') CurrencyList," & _
				" Convert(char(10), IsNull(MonthlyFeeEuroDate, GetDate()), 103) MonthlyFeeEuroDateDisplay," & _
				" Convert(char(10), IsNull(MonthlyFeeDollarDate, GetDate()), 103) MonthlyFeeDollarDateDisplay," & _
				" Convert(char(10), IsNull(MonthlyFeeBankHighDate, GetDate()), 103) MonthlyFeeBankHighDateDisplay," & _
				" Convert(char(10), IsNull(MonthlyFeeBankLowDate, GetDate()), 103) MonthlyFeeBankLowDateDisplay," & _
				" Convert(char(10), IsNull(AnnualFeeDate, GetDate()), 103) AnnualFeeDateDisplay," & _
				" Convert(char(10), IsNull(AnnualFeeLowRiskDate, GetDate()), 103) AnnualFeeLowRiskDateDisplay," & _
				" Convert(char(10), IsNull(AnnualFeeHighRiskDate, GetDate()), 103) AnnualFeeHighRiskDateDisplay," & _
				" Convert(char(10), IsNull(AnnualFee3dSecureDate, GetDate()), 103) AnnualFee3dSecureDateDisplay," & _
				" Convert(char(10), IsNull(AnnualFeeRegistrationDate, GetDate()), 103) AnnualFeeRegistrationDateDisplay," & _
				" * FROM tblCompany WHERE ID=" & nCompanyID
				set rsData = oledbData.Execute(sSQL)
				if Not rsData.EOF then
					%>
					<table align="center" border="0" cellpadding="1" cellspacing="0" width="100%">
						<!--#include file="Merchant_dataHeaderInc.asp"-->
						<%
							pageSecurityLevel = 0
							pageClosingTags = "</table></td></tr></table>"
							Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
						%>
						<tr><td height="20"></td></tr>
						<!--#include file="Merchant_dataMenuInc.asp"-->
						<tr><td><br/></td></tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td valign="top">
											<table border="0" cellpadding="1" cellspacing="0">
												<tr><td class="MerchantSubHead">Settlement Settings</td></tr>
												<tr>
													<td colspan="2" style="padding-bottom:12px;">
                                                        Add Currency Setting <select id="MerchantCurrencyList">
															<%
															Set iRs = oleDbData.Execute("Select CUR_ID, CUR_ISOName From tblSystemCurrencies Where CUR_ID Not IN(Select Currency_ID From Setting.SetMerchantSettlement Where Merchant_ID=" & nCompanyID & ") Order By CUR_ISOName Asc")
															Do While Not iRs.EOF
                                                                Response.Write("<option value=" & iRs("CUR_ID") & ">" & iRs("CUR_ISOName") & "</option>")
                                                                iRs.MoveNext
                                                            Loop
                                                            iRs.Close
                                                            %>
                                                        </select>
                                                        <input type="button" value="Add" onclick="ShowCurrencyFeeRow();return;" />
														<table class="formNormal">
                                                            <tr>
                                                                <th>Currency</th><th>Show in<br />Settle Payouts</th><th>Minimum<br />Settled Payouts</th><th>Auto Create<br />Invoice</th>
                                                                <th>Exclude Debit</th><th>Exclude Refund</th><th>Exclude Fees</th><th>Exclude Chb</th><th>Exclude Cashback</th>
                                                            </tr>
															<%
															Set iRs = oleDbData.Execute("SELECT * FROM tblSystemCurrencies Left Join Setting.SetMerchantSettlement mcs ON(tblSystemCurrencies.CUR_ID = mcs.Currency_ID And mcs.Merchant_ID=" & nCompanyID & ")")
															Do While Not iRs.EOF
																%>
																<tr id="CurrencyFee<%=iRs("CUR_ID")%>" style="<%=IIF(iRs("CUR_ID") = iRs("Currency_ID"), "", "display:none;") %>">
																	<td>
																		<input type="hidden" id="CurrencySettings_<%=iRs("CUR_ID")%>_Exist" name="CurrencySettings_<%=iRs("CUR_ID")%>_Exist" value="<%=IIF(iRs("CUR_ID") = iRs("Currency_ID"), "1", "0") %>" />
																		<%=GetCurIso(iRs("CUR_ID"))%>
																	</td>
																	<td><input type="checkbox" name="IsShowToSettle<%=iRs("CUR_ID")%>" value="1" <%=IIF(iRs("IsShowToSettle"), " checked", "")%> /></td>
																	<td><input type="text" name="MinSettlementAmount<%=iRs("CUR_ID")%>" value="<%=FormatNumber(TestNumVar(iRs("MinSettlementAmount"), 0, -1, 0), 2, true, false, false) %>" style="width:100%;" /></td>
																	<td><input type="checkbox" name="IsAutoInvoice<%=iRs("CUR_ID")%>" value="1" <%=IIF(iRs("IsAutoInvoice"), " checked", "")%> /></td>
																	<td><input type="checkbox" name="IsWireExcludeDebit<%=iRs("CUR_ID")%>" value="1" <%=IIF(iRs("IsWireExcludeDebit"), " checked", "")%> /></td>
																	<td><input type="checkbox" name="IsWireExcludeRefund<%=iRs("CUR_ID")%>" value="1" <%=IIF(iRs("IsWireExcludeRefund"), " checked", "")%> /></td>
																	<td><input type="checkbox" name="IsWireExcludeFee<%=iRs("CUR_ID")%>" value="1" <%=IIF(iRs("IsWireExcludeFee"), " checked", "")%> /></td>
																	<td><input type="checkbox" name="IsWireExcludeChb<%=iRs("CUR_ID")%>" value="1" <%=IIF(iRs("IsWireExcludeChb"), " checked", "")%> /></td>
																	<td><input type="checkbox" name="IsWireExcludeCashback<%=iRs("CUR_ID")%>" value="1" <%=IIF(iRs("IsWireExcludeCashback"), " checked", "")%> /></td>
																</tr>
																<%
																iRs.MoveNext
															Loop
															iRs.Close
															%>
														</table>
													</td>
												</tr>
											</table>
											<br />
											<table border="0" cellpadding="1" cellspacing="0" width="100%">
												<tr><td class="MerchantSubHead">Payout Settings</td></tr>
												<tr>
													<td>Preferred Wire Type</td>
													<td>
														<select name="PreferredWireType" class="inputData" style="width:200px;">
															<option value="0"></option>
															<option value="6" <%=IIF(rsData("PreferredWireType") = 6, " selected", "")%> >Manual</option>
															<option value="4" <%=IIF(rsData("PreferredWireType") = 4, " selected", "")%> >Fax</option>
															<option value="3" <%=IIF(rsData("PreferredWireType") = 3, " selected", "")%> >Masav</option>
															<option value="7" <%=IIF(rsData("PreferredWireType") = 7, " selected", "")%> >Hellenic</option>
															<option value="8" <%=IIF(rsData("PreferredWireType") = 8, " selected", "")%> >BOG</option>
															<option value="5" <%=IIF(rsData("PreferredWireType") = 5, " selected", "")%> >E-Bank</option>
														</select>
													</td>
												</tr>
												<tr>
													<td>Charge VAT</td>
													<td>
														<select name="isChargeVAT" class="inputData" style="width:200px;">
															<option value="1" <% if rsData("isChargeVAT") then %>selected<% End If %>>Yes</option>
															<option value="0" <% if not rsData("isChargeVAT") then %>selected<% End If %>>No</option>
														</select>
													</td>
												</tr>
												<tr>
 													<td>Invoice Provider</td>
													<td>
														<%
														sSQL="SELECT BillingCompanys_id, name FROM tblBillingCompanys ORDER BY BillingCompanys_id"
														set rsData3 = oledbData.Execute(sSQL)
														if not rsData3.EOF then
															PutRecordsetComboNew rsData3, "BillingCompanys_id", "BillingCompanys_id", "Name", rsData("BillingCompanys_id"), "class=""inputData"" style=""width:200px;""", false
														end if
														rsData3.close
														Set rsData3 = nothing
														%>
													</td>
												</tr>
												<tr>
													<td>Settlement Percentage</td>
													<td>
														<input type="text" class="inputData" style="width:50px;" name="PayPercent" value="<%= rsData("PayPercent") %>"> %
													</td>
												</tr>
												<tr><td style="height:12px;"></td></tr>
												<tr>
													<td>
														Default Currency *
														<% showDivBox "help", "ltr", "355", "Both default bank account and bank accounts for currencies can be managed in ""BANK ACCOUNTS"" page." %>
													</td>
													<td>
														<select name="PaymentReceiveCurrency" class="inputData" style="width:50px;">
															<%
																For i = 0 To MAX_CURRENCY
																	Response.Write("<option value=""" & i & """" & IIF(rsData("PaymentReceiveCurrency") = i, " selected", "") & ">" & GetCurIso(i) & "</option>")
																Next   
															%>
														</select>
													</td>
												</tr>
												<tr>
													<td colspan="2">
														<%
														sCurrencyList = Trim(rsData("CurrencyList"))
														If sCurrencyList = "" Then
															response.Write "All settlements will be paid out in the default currency."
														Else
															%>
															* Settlements in <b><%= sCurrencyList %></b> will be paid out in the settlement's currency.
															<br />
															* Settlements in all other currencies will be paid out in the above default currency.
															<%
														End If
														%>
													</td>
												</tr>
											</table>
										</td>
										<td style="border-right:1px solid #d2d2d2;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td valign="top" style="text-align:left; padding-left:50px;">
											<table border="0" cellpadding="1" cellspacing="0" width="100%">
												<tr><td class="MerchantSubHead">Payout Schedule</td></tr>
												<tr>
													<td>
														<table border="0" cellpadding="1" cellspacing="0" width="50%">
															<tr>
																<td>Initial holding margin</td>
																<td>
																	<input type="text" class="inputData" style="width:40px;" name="PayingDaysMarginInitial" value="<%= rsData("PayingDaysMarginInitial") %>" /> Days
																</td>
															</tr>
															<tr>
																<td>Holding margin</td>
																<td>
																	<input type="text" class="inputData" style="width:40px;" name="PayingDaysMargin" value="<%= rsData("PayingDaysMargin") %>" /> Days
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr><td style="padding-bottom:8px; padding-top:8px; font-size:14px;">Or<br /></td></tr>
												<%
													if len(trim(rsData("payingDates1")))=6 then
														sfromDay1 = mid(trim(rsData("payingDates1")),1,2)
														stoDay1 = mid(trim(rsData("payingDates1")),3,2)
														spayday1 = mid(trim(rsData("payingDates1")),5,2)
													else
														sfromDay1=00
														stoDay1=00
														spayday1=00
													End if
													if len(trim(rsData("payingDates2")))=6 then
														sfromDay2 = mid(trim(rsData("payingDates2")),1,2)
														stoDay2 = mid(trim(rsData("payingDates2")),3,2)
														spayday2 = mid(trim(rsData("payingDates2")),5,2)
													else
														sfromDay2=00
														stoDay2=00
														spayday2=00
													end if
													if len(trim(rsData("payingDates3")))=6 then
														sfromDay3 = mid(trim(rsData("payingDates3")),1,2)
														stoDay3 = mid(trim(rsData("payingDates3")),3,2)
														spayday3 = mid(trim(rsData("payingDates3")),5,2)
													else
														sfromDay3=00
														stoDay3=00
														spayday3=00
													end if
												%>
												<tr>
													<td>
														Transactions between
														<%=DrawArrCombo("fromDay1", Split("01,11,16,21", ","), sfromDay1)%>
														and
														<%=DrawArrCombo("toDay1", Split("10,15,20,31", ","), stoDay1)%>
														credit on
														<%=DrawArrCombo("payday1", Split("05,10,11,15,20,25,01", ","), spayday1)%>
													</td>
												</tr>
												<tr><td height="4"></td></tr>
												<tr>
													<td>
														Transactions between
														<%=DrawArrCombo("fromDay2", Split("01,11,16,21", ","), sfromDay2)%>
														and
														<%=DrawArrCombo("toDay2", Split("10,15,20,31", ","), stoDay2)%>
														credit on
														<%=DrawArrCombo("payday2", Split("05,10,11,15,20,25,01", ","), spayday2)%>
													</td>
												</tr>
												<tr><td height="4"></td></tr>
												<tr>
													<td>
														Transactions between
														<%=DrawArrCombo("fromDay3", Split("01,11,16,21", ","), sfromDay3)%>
														and
														<%=DrawArrCombo("toDay3", Split("10,15,20,31", ","), stoDay3)%>
														credit on
														<%=DrawArrCombo("payday3", Split("05,10,11,15,20,25,01", ","), spayday3)%>
													</td>
												</tr>
											</table>
											<br />
											<div>
												<a class="faq" href="?RecalcDates=1&CompanyID=<%=nCompanyID%>">Update Transactions</a> <span class="txt11">(Unsettled transactions only!)</span>
											</div>
											<br /><br />
											<table border="0" cellpadding="1" cellspacing="0">
												<tr><td class="MerchantSubHead">Rolling Reserve<br /></td></tr>
												<tr>
													<td valign="top">Comment</td>
													<td><textarea class="inputData" name="RRComment" style="width:300px; height:34px;"><%= rsData("RRComment") %></textarea></td>
												</tr>
												<tr>
													<td width="130">Hold Percentage</td>
													<td><input type="text" class="inputData" style="width:40px;" id="SecurityDeposit" name="SecurityDeposit" value="<%= FormatNumber(rsData("SecurityDeposit"), 2, true, false, false) %>"> %</td>
												</tr>
												<tr>
													<td>Hold Period</td>
													<td><input type="text" class="inputData" style="width:40px;" id="SecurityPeriod" name="SecurityPeriod" value="<%= rsData("SecurityPeriod") %>"> Months</td>
												</tr>
												<tr><td><br /></td></tr>
												<tr>
													<td rowspan="8" valign="top">Action to Take</td>
													<td><input type="radio" name="RRState" id="RRState0" value="0" class="option" <%= IIF(rsData("RRState") = 0, " checked", "")%> onclick="CheckRR()" />None</td>
												</tr>
												<tr>
													<td>
														<%bSOValid = TestNumVar(ExecScalar("Select TOP 1 ID From tblCompanyTransPass Where CompanyID=" & nCompanyID & " And PaymentMethod=" & PMD_RolRes & " And CreditType=0 And InsertDate < '" & DateAdd("m", -rsData("SecurityPeriod"), Now) & "'", 0), 0, -1, 0) > 0 %>
														<input type="radio" name="RRState" id="RRState2" value="2" class="option" <%=IIF(bSOValid, " disabled", "") %> <%= IIF(rsData("RRState") = 2, " checked", "")%> onclick="CheckRR()" >Hold a Single Occurrence
														<% showDivBox "help", "ltr", "300", "<ul><li>Take RR only one time and release once when period reached.</li><li>Checkbox might be disabled if there was already reserve/release for this merchant</li></ul>" %><br />
													</td>
												</tr>
												<tr>
													<td>
														<input type="radio" name="RRState" id="RRState1" value="1" class="option" <%= IIF(rsData("RRState") = 1, " checked", "")%> onclick="CheckRR()" />Hold Dynamically
														<% showDivBox "help", "ltr", "300", "<ul><li>Take RR with every settlement and release when period is reached for each one.</li><li>Check ""Enable Reserve"" to allow rolling reserve transaction to be created.</li><li>Check ""Enable Release"" to allow rolling release transaction to be created.</li></ul>" %>
													</td>
												</tr>
												<tr>
													<td style="padding-left:30px;">
														<input type="checkbox" class="option" name="RREnable" id="RREnable" value="1" <%= IIF(rsData("RREnable"), " checked", "")%> ><span id="lblRREnable">Enable Reserve</span> &nbsp;
														<input type="checkbox" class="option" name="RRAutoRet" id="RRAutoRet" value="1" <%= IIF(rsData("RRAutoRet"), " checked", "")%> ><span id="lblRRAutoRet">Enable Release</span>
													</td>
												</tr>
												<tr>
													<td>
														<input type="radio" name="RRState" id="RRState3" value="3" class="option" <%= IIF(rsData("RRState") = 3, " checked", "")%> onclick="CheckRR()" >Hold a Fixed Amount 
														<% showDivBox "help", "ltr", "300", "<ul><li>System will make sure to reserve this amount regardless of the time the amount is kept, This will be done by slowly reserving or releasing with every settlemnt made until the amount is reached.</li><li>In currency box choose ""each"" if you wish to keep RR for every currency settled</li></ul>" %>
													</td>
												</tr>
												<tr>
													<td style="padding-left:30px;">
														<table cellpadding="1" cellspacing="0">
														<tr>
															<td width="80" id="lblRRAmount1">Amount</td>
															<td>
																<select name="RRKeepCurrency1" id="RRKeepCurrency1" class="inputData">
																	<option value="255">Each</option>
																	<%
																	For i = 0 To MAX_CURRENCY
																		Response.Write("<option value=""" & i & """" & IIF(rsData("RRKeepCurrency") = i, " selected", "") & ">" & GetCurIso(i) & "</option>")
																	Next   
																	%>
																</select>
																<input type="text" class="inputData" style="width:70px;" id="RRKeepAmount1" name="RRKeepAmount1" value="<%=IIF(rsData("RRState") = 3, rsData("RRKeepAmount"), "") %>">
															</td>
														</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td>
														<input type="radio" name="RRState" id="RRState4" value="4" class="option" <%= IIF(rsData("RRState") = 4, " checked", "")%> onclick="CheckRR()" >Record an External Hold 
														<% showDivBox "help", "ltr", "300", "<ul><li>Insert a rolling reserve transaction for the amount specified regardless of any processing/settlments made, This is done only once right after choosing this option and clicking ""Save"", Use this when there is an hold taken outside of this system.</li></ul>" %>
													</td>
												</tr>
												<tr>
													<td style="padding-left:30px;">
														<table cellpadding="1" cellspacing="0">
														<tr>
															<td width="80" id="lblRRAmount2">Amount</td>
															<td>
																<select name="RRKeepCurrency2" id="RRKeepCurrency2" class="inputData">
																	<%
																	For i = 0 To MAX_CURRENCY
																		Response.Write("<option value=""" & i & """" & IIF(rsData("RRKeepCurrency") = i, " selected", "") & ">" & GetCurIso(i) & "</option>")
																	Next   
																	%>
																</select>
																<input type="text" class="inputData" style="width:70px;" id="RRKeepAmount2" name="RRKeepAmount2" value="<%=IIF(rsData("RRState") = 4, rsData("RRKeepAmount"), "") %>">
															</td>
														</tr>
														</table>
													</td>
												</tr>
											</table>
											<script language="javascript" type="text/javascript">
												CheckRR();
											</script>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="right">
								<br /><hr class="separator" />
								<input type="submit" value="Save Changes" style="width:110px; height:auto; background-color:#f1f1f1;">
							</td>
						</tr>
					</table>
				<%
			End If
			rsData.close
			Set rsData = nothing
			%>
		</td>
	</tr>
</table>
</form>
<%
CloseConnection
%>
</body>
</html>