<%@ Control Language="VB" ClassName="CardHeaders" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css"/>

<script runat="server">
	Dim bFirstMenuItem As Boolean = True
	
	Protected uWidth As Unit = Unit.Parse("90%")
	Public idFieldName As String = "companyID"
	Public Property Width() As Unit
		Get
			Return tblMenu.Width
		End Get
		Set(ByVal value As Unit)
			If Not value.IsEmpty Then
				uWidth = value
				tblMenu.Width = uWidth
			End If
		End Set
	End Property

	Private Function GetMerchantMenuDataItem(ByVal sText As String, ByVal sURL As String, Optional ByVal sOnClick As String = Nothing) As String
		Dim sSeparator As String = IIf(bFirstMenuItem, String.Empty, "<td class=txt11 style=""color:#A9A7A2; padding-left:2px;padding-right:2px;"">|<br/></td>")
		Dim sStyle As String = "cursor:pointer;text-transform:uppercase;font-size:10px;padding:0px 2px;"
		If sURL.ToLower.Contains(Request.ServerVariables("SCRIPT_NAME").ToLower.Substring(Request.ServerVariables("SCRIPT_NAME").LastIndexOf("/") + 1)) Then
			sStyle &= "font-weight:bold;background-color:#D6C8A8;padding-left:2px;padding-right:2px;"
		Else
			sStyle &= "text-decoration:none;font-size:10px;padding-left:2px;padding-right:2px;"
		End If
		If String.IsNullOrEmpty(sOnClick) Then sOnClick = "location.href='" & sURL & "';"
		bFirstMenuItem = False
		Return sSeparator & "<td align=""center""  title=""" & sURL & """ onclick=""" & sOnClick & """ style=""" & sStyle & """>" & sText & "</td>"
	End Function
	
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		Dim nMerchant As Integer = dbPages.TestVar(Request(idFieldName), 1, 0, 0)
		If Not Page.IsPostBack Then
			litMenu.Text &= GetMerchantMenuDataItem("Home", "merchant_data.asp?companyID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("Company Details", "merchant_dataCompany.asp?companyID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("Personal & Login Details", "merchant_dataUser.asp?companyID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("General", "merchant_dataGeneral.asp?companyID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("Options", "merchant_dataOptions.asp?companyID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("Solutions", "merchant_dataSolutions.asp?companyID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("Fees", "merchant_data_Fees.aspx?ID=" & nMerchant, String.Empty)
			If Session("Local") Then litMenu.Text &= GetMerchantMenuDataItem("Fees2", "merchant_data_Fees2.aspx?ID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("Payout", "merchant_dataPayInfo.asp?companyID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("Bank Accounts", "merchant_data_bankAccounts.aspx?ID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("Risk Management", "merchant_dataRiskMng.asp?companyID=" & nMerchant, String.Empty)
            litMenu.Text &= GetMerchantMenuDataItem("Terminals", "merchant_dataTerminal.asp?companyID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("Files & Notes", "merchant_dataNotes.aspx?companyID=" & nMerchant, String.Empty)
            litMenu.Text &= GetMerchantMenuDataItem("Mail", "Merchant_dataMail.aspx?merchantID=" & nMerchant, String.Empty)
			litMenu.Text &= GetMerchantMenuDataItem("Devices", "merchant_data_Devices.aspx?merchantID=" & nMerchant, String.Empty)
			'litMenu.Text &= GetMerchantMenuDataItem("T", "merchant_data_terminal.aspx?ID=" & nMerchant, String.Empty)
			
			Dim sCompanyName As String = ""
			Dim drData As SqlDataReader = dbPages.ExecReader("SELECT CustomerNumber, CompanyName, CompanyLegalNumber, IDNumber, FirstName, LastName, Phone," & _
			" CompanyFax, Cellular, Mail, URL, ActiveStatus FROM tblCompany WHERE ID=" & nMerchant)
			If drData.Read() Then
				hlBlacklist.NavigateUrl = "merchant_blacklist.aspx?FilterSource=Merchant&FilterType=OR"
				hlBlacklist.NavigateUrl &= "&MerchantID=" & nMerchant
				hlBlacklist.NavigateUrl &= "&MerchantNumber=" & drData("CustomerNumber")
				hlBlacklist.NavigateUrl &= "&MerchantName=" & drData("CompanyName")
				hlBlacklist.NavigateUrl &= "&LegalNumber=" & drData("CompanyLegalNumber")
				hlBlacklist.NavigateUrl &= "&IDNumber=" & drData("IDNumber")
				hlBlacklist.NavigateUrl &= "&FirstName=" & drData("FirstName")
				hlBlacklist.NavigateUrl &= "&LastName=" & drData("LastName")
				hlBlacklist.NavigateUrl &= "&Phone=" & drData("Phone")
				hlBlacklist.NavigateUrl &= "&Fax=" & drData("CompanyFax")
				hlBlacklist.NavigateUrl &= "&Cellular=" & drData("Cellular")
				hlBlacklist.NavigateUrl &= "&Mail=" & drData("Mail")
				hlBlacklist.NavigateUrl &= "&URL=" & drData("URL").ToString.Replace(vbLf, "").Replace(vbCr, " ")
				litTitle.Text = nMerchant & " " & drData("CompanyName")
				hlStatus.NavigateUrl = "Merchant_Activities.aspx?Merchant=" & nMerchant
				lbStatus.Text = GlobalData.Value(GlobalDataGroup.MERCHANT_STATUS, drData("ActiveStatus")).Text
				hlCharges.NavigateUrl = "charge_browser.asp?companyID=" & nMerchant
				hlMerchantAdmin.NavigateUrl = "outer_login.aspx?CompanyID=" & nMerchant
			End If
			drData.Close()
		End If
	End Sub
</script>
<%--<div style="text-align:left; width:95%">--%>
	<asp:Table ID="tblMenu" Width="90%"  runat="server">
		<asp:TableRow>
			<asp:TableCell VerticalAlign="Top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td id="pageMainHeadingTd">
							<asp:label ID="lblPermissions" runat="server" />
							<span id="pageMainHeading">MERCHANT MANAGEMENT</span> -
							<span style="font-size:14px;">
								<asp:Literal ID="litTitle" runat="server" />
								| <asp:Label ID="lbStatus" runat="server" /> (<asp:HyperLink ID="hlStatus" Text="change" runat="server" ForeColor="#000050"  Font-Size="14px" />)
							</span>
						</td>
						<td align="right">
							<asp:HyperLink ID="hlBlacklist" runat="server" CssClass="alwaysactive" ImageUrl="../images/post_button_blacklist.gif" ToolTip="Search in Blacklist" />&nbsp;
							<asp:HyperLink ID="hlArchive" runat="server" CssClass="alwaysactive" ImageUrl="../images/post_button_documents.gif" ToolTip="File Archive" NavigateUrl="common_framesetB.aspx?pageMenuWidth=250&pageMenuUrl=FileArchiveFilter.aspx" />&nbsp;
							<asp:HyperLink ID="hlCharges" runat="server" CssClass="alwaysactive" ImageUrl="../images/post_button_4.gif" ToolTip="Make Transactions" />&nbsp;
							<asp:HyperLink ID="hlMerchantAdmin" runat="server" CssClass="alwaysactive" Target="_blank" ImageUrl="../images/post_button_1.gif" ToolTip="Merchant Control Panel" />
						</td>
					</tr>
				</table>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow  >
			<asp:TableCell cssclass="txt10">
				<%--<br />--%>
				<div style="height:10px; width:100%"></div>
				<table align="center" width="100%"  cellpadding="2" cellspacing="2" style="margin:0px; border:1px solid #a9a7a2; background-color:#f1f1f1">
					<tr >
						<asp:Literal ID="litMenu" runat="server" />
					</tr>
				</table>
			
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<div style="height:10px; width:100%"></div>
<%--</div>
<br />--%>