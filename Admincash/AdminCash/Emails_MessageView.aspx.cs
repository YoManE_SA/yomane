﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Netpay.Web;
using Netpay.Emails;
//using Netpay.Emails.DAL;

public partial class Emails_MessageView : System.Web.UI.Page
{
    public int MessageId { get; private set; }
    public int? MerchantId { get; private set; }
    public string UserMailForAllHistory { get; private set; }
	public string ReplyToAddress { get; private set; }
    public bool IsSent { get; private set; }
    private Netpay.Emails.Email.Message mimeMsg;
	private Dictionary<int, MessageStatus> _messageStatuses;
	protected Dictionary<int, MessageStatus> MessageStatuses
	{
		get
		{
			if (_messageStatuses == null)
				_messageStatuses = Netpay.Emails.MessageStatus.Cache;
			return _messageStatuses;
		}
	}
    
    protected override void OnInit(EventArgs e)
	{
        Security.CheckPermission(lblPermissions);
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ddlStatus.DataValueField = "ID"; ddlStatus.DataTextField = "Name";
		ddlStatus.DataSource = MessageStatuses.Values; 
        MessageId = dbPages.TestVar(Request["MessageId"], 1, 0, 0);
        if (MessageId > 0) LoadMessage();
    }


	protected void btnaddFilesToCurentMerchant_Command(object sender, CommandEventArgs e)
	{
		int numberAttach = dbPages.TestVar(e.CommandArgument, 0, -1, 0);
		var att = mimeMsg.Attachments[numberAttach];
		var merchant = Netpay.Bll.Merchants.Merchant.Load(MerchantId.Value);
		if (merchant != null)
		{
			Netpay.Bll.Accounts.File fvo = new Netpay.Bll.Accounts.File(merchant.AccountID);
		    fvo.FileTitle = ((sender as Control).Parent.FindControl("txtComment") as TextBox).Text;
			fvo.Save("merchant_" + MerchantId.ToString() + "_" + att.FileName, mimeMsg.Attachments[numberAttach].DecodedData);
		    Button btn =(Button) sender;
		    btn.Attributes.Add("style","display:none;");//hiderptAttachments);		
	    }
		if (ddlStatus.SelectedIndex == 0) ddlStatus.SelectedIndex = 3;
	}

	protected string getStatusName(int? status)
    {
        if (status == null) return string.Empty;
		if (!MessageStatuses.ContainsKey(status.Value)) return string.Empty;
		return MessageStatuses[status.Value].Name;
    }

    protected void ReplyToMerchant_Click(object sender, EventArgs e)
    {
		Response.Redirect(string.Format("Emails_MessageEditor.aspx?MessageId={0}&ReplyToAddress={1}", MessageId, Server.UrlEncode(ReplyToAddress)));
    }

    protected void LoadMessage()
    {
		var msg = Netpay.Emails.Message.Load(MessageId);
        if (msg != null)
        {
			mimeMsg = msg.MimeMessage;
            hf_msgID.Value = msg.ID.ToString();
            hf_threadID.Value = msg.ThreadID.ToString();
            MerchantId = msg.MerchantID;
            IsSent = msg.IsSent;
            lblTo.Text = msg.EmailTo;
            lblForm.Text = msg.EmailFrom;
            lblDateTime.Text = msg.Date.ToString();
            lbl_CC.Text = msg.CC;
            lbl_CcTitle.Visible = !(string.IsNullOrEmpty(lbl_CC.Text));
            ddlStatus.SelectedValue = msg.Status.GetValueOrDefault(1).ToString();
            lblSubject.Text = msg.Subject;
			UserMailForAllHistory = msg.IsSent ? msg.EmailTo : msg.EmailFrom;
			if (mimeMsg != null) {
				if (!string.IsNullOrEmpty(mimeMsg["Reply-To"]))
					ReplyToAddress = new System.Net.Mail.MailAddress(mimeMsg["Reply-To"]).Address;
				else if (!string.IsNullOrEmpty(mimeMsg["Return-Path"])) 
					ReplyToAddress = new System.Net.Mail.MailAddress(mimeMsg["Return-Path"]).Address;
			}
			lbt_deleteAssignedMail.Visible = MerchantId != null;
            contentFrame.Attributes.Add("src", "Emails_MessageBody.aspx?MessageId=" + MessageId);
			if (mimeMsg != null && mimeMsg.Attachments != null)
            {
				if (Request.QueryString.AllKeys.Contains("DownloadAtt"))
				{
					var attachment = mimeMsg.Attachments[int.Parse(Request.QueryString["DownloadAtt"])];
					//Response.Clear();
					if(!string.IsNullOrEmpty(attachment.ContentType))
						Response.ContentType = attachment.ContentType;
					if(!string.IsNullOrEmpty(attachment.ContentDisposition)) 
						Response.AddHeader("ContentDisposition", attachment.ContentDisposition);
					Response.BinaryWrite(attachment.DecodedData);
					Response.End();
				}

				if (mimeMsg.Attachments.Count > 0)
				{
					rptAttachments.DataSource = mimeMsg.Attachments;
					rptAttachments.DataBind();
					foreach (var att in mimeMsg.Attachments) {
                        if (att.ContentType != null && ((att.FileName == "NetpayAppData.xml") && (att.ContentType.Contains("application/xml;") || att.ContentType.Contains("text/xml;")))) {
                            //btn_addNewMarchant.Visible = true;
                            //XmlDocument doc = new XmlDocument();
							//doc.LoadXml(System.Text.Encoding.UTF8.GetString(att.DecodedData));
							//var emailNodes = doc.GetElementsByTagName("Email");
							//if (emailNodes.Count > 0) ReplyToAddress = emailNodes[0].InnerText;
                            break;
                        }
                    }
				}
            }
			rep_History.DataSource = Netpay.Emails.LogStatus.Search(new LogStatus.SearchFilters() { MessageID = MessageId });
            if (!string.IsNullOrEmpty(hf_threadID.Value))
				rep_Conversation.DataSource = Netpay.Emails.Message.getThreadMessages(Guid.Parse(hf_threadID.Value));
            DataBind();
            rep_History.Visible = rep_History.Items.Count > 0;
            rep_Conversation.Visible = rep_Conversation.Items.Count  > 0;
            LoadAssignedMerchantList(msg);
        }
        else Response.Redirect("common_blank.htm");
    }

    protected void LoadAssignedMerchantList(Netpay.Emails.Message msg)
    {
        ddl_merchant.Items.Clear();
        Dictionary<int, string> merchantsByMail = Netpay.Emails.EmailToMerchant.GetMerchantsByMail(WebUtils.DomainHost, msg.IsSent ? msg.EmailTo : msg.EmailFrom);
        ddl_merchant.DataValueField = "Key";
        ddl_merchant.DataTextField = "Value";
        ddl_merchant.DataSource = merchantsByMail;
        ddl_merchant.DataBind();
        //ddl_merchant.Items.Insert(0, new ListItem() { Text = string.Empty, Value = string.Empty });
        if (ddl_merchant.Items.Count > 0) ddl_merchant.Items.Add(new ListItem() { Text = "Other", Value = "Other" });
        ddl_merchant.Visible = (ddl_merchant.Items.Count > 0);
        txt_merchant.Attributes.CssStyle.Add("display", (ddl_merchant.Items.Count > 0 ? "none;" : ""));
    }
    
    protected string GetMerchantActiveStatusCompanyName
    {
        get
        {
            if (MerchantId != null)
            {
                int activeStatus;
                string _companyName;
                Netpay.Emails.EmailToMerchant.GetMerchantActiveStatusCompanyName(Netpay.Web.WebUtils.DomainHost, MerchantId.GetValueOrDefault(), out activeStatus, out _companyName);
                string statusText =  Netpay.Infrastructure.GlobalData.GetText(Netpay.Infrastructure.GlobalDataGroup.MerchantStatus, Netpay.CommonTypes.Language.English, activeStatus);
                return string.Format("{0} ({1})", _companyName, statusText);
            }
            return "[No Merchant Assigned]";
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
		Netpay.Emails.Message.Load(MessageId).IsDeleted = true;
        Response.Redirect("common_blank.htm");
    }

	protected void btnUpdate_Click(object sender, EventArgs e)
	{
		//Netpay.Emails.Message.Load(0).Create(WebUtils.CredentialsToken, MessageId, (byte)dbPages.TestVar(hf_selectedstatus.Value, 0, -1, 0), Security.Username, txt_userMessage.Text);
        Response.Redirect(Request.RawUrl);
    }

    protected void ddl_merchant_Changed(object sender, EventArgs e)
    {
		Netpay.Emails.Message.Load(int.Parse(hf_msgID.Value)).setMerchant(int.Parse(hf_rbl_merchant.Value), chbAssignAddEmail.Checked, chbAssignUpdateHistory.Checked);
        Response.Redirect(Request.RawUrl);
    }

    protected void lbt_deleteAssignedMail_Click(object sender, EventArgs e)
    {
		Netpay.Emails.Message.Load(int.Parse(hf_msgID.Value)).setMerchant(null, false, false);
        Response.Redirect(Request.RawUrl);
    }
}