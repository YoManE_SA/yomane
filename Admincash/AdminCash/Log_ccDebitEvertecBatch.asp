<html>
<head>	
	<title><%= COMPANY_NAME_1 %></title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
</head>
<body bgcolor="white" topmargin="3" leftmargin="3" rightmargin="3" marginheight="3" marginwidth="3">
<%
 Dim nTerminalID, fso, nFileID, lCount, sOptions
 nFileID = Request("FileID")
 nTerminalID = Request("TerminalID")
 lCount = 1
 Set fso = CreateObject("Scripting.FileSystemObject")
 while(fso.FileExists("C:\Evertec\Trans" & nTerminalID & "N" & lCount & ".dat"))
	if nFileID = cstr(lCount) Then sOptions = sOptions & "<option selected=""selected"">" & lCount _
	Else sOptions = sOptions & "<option>" & lCount
	lCount = lCount + 1
 Wend
 Set fso = Nothing
%>
<table>
<form>
 <tr>
  <th>Terminal</th><td><input type="Text" name="TerminalID" value="<%=nTerminalID%>"></td>
  <td>&nbsp;&nbsp;&nbsp;</td>
  <th>Batch Number</th><td><select name="FileID" onchange="form.submit()"><option><%=sOptions%></select></td>
  <td><input type="submit" value="Load"></td>
 </tr>
</form>
</table>
<%if nTerminalID <> "" Then%>
<%
   Dim pEvertec, nData, nRowPos, nRowLength, nTrailer, nResult
   lCount = 1
   nRowLength = Array(3, 4, 1, 19, 16, 12, 12, 6, 12, 1, 7, 7)
   Redim nRowPos(Ubound(nRowLength))
   For i = Lbound(nRowLength) to Ubound(nRowLength)
   	 nRowPos(i) = lCount
	 lCount = lCount + nRowLength(i)
   Next

   Function GetField(lRow, fID)
	 GetField = Mid(lRow, nRowPos(fID), nRowLength(fID))
	 on error resume next
   	 if IsNumeric(GetField) Then GetField = Clng(GetField)
   End Function
   
   On Error Resume Next
   Set pEvertec = Server.CreateObject("Compunet.Evertec")
   lCount = pEvertec.ReadBatch("C:\Evertec\Trans" & nTerminalID & "N" & nFileID & ".dat", nData, nTrailer, nResult)
   Set pEvertec = Nothing
   if Err.Number <> 0 Then 
   	Response.Write("Error (" & Err.Number & "):" & Err.Description)
	Response.End
   End if
   On Error Goto 0
   Response.Write("<table border=""1""><tr><th>Index</th><th>RecordType</th><th>SequenceNumber</th><th>Type</th><th>AccountNumber</th><th>TerminalNumber</th><th>Amount</th><th>SettleAmount</th><th>AuthorizationCode</th><th>Reference</th><th>ECICAVV</th><th>Sequencekey</th><th>Reserved</th></tr>")
   nData = Split(nData, vbCrLf)
   For i = Lbound(nData) to Ubound(nData)
      Response.Write("<tr><td>" & (i + 1) & "</td>")
	   For x = Lbound(nRowLength) to Ubound(nRowLength)
	      Response.Write("<td>" & GetField(nData(i), x) & "</td>")
	   Next
   	  Response.Write("</tr>")
   Next
   Response.Write("</table>")
   if Len(nTrailer) = 100 Then
     Response.Write("Trailer:<br />")
   	 Response.Write("Batch RecordCount : " & CLng(Mid(nTrailer, 4, 5)) & "<br />")
   	 Response.Write("Batch Total Amount : " & CLng(Mid(nTrailer, 9, 16)) & "<br />")
   	 Response.Write("Internal Merchant Batch Key : " & Mid(nTrailer, 25, 13) & "<br />")
   End if
   if Len(nResult) > 0 Then Response.Write("<br />Result:" & nResult)
End if
%>
</body>
</html>
