<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
    Dim sDocument As String, nDocument As Integer = 0, sDescription As String = ""
    Sub CloseWindow()
        Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
        Response.Write("setTimeout(""opener.focus();top.close();"", 100);")
        Response.Write("</s" & "cript>")
    End Sub
	
    Sub Page_Load()
        Security.CheckPermission(lblPermissions)
        If Request.QueryString("Frame") <> String.Empty Then btnDelete.Visible = False
        sDocument = dbPages.DBText(Request("PageURL"))
        Dim iReader As SqlDataReader = dbPages.ExecReader("SELECT TOP 1 * FROM tblSecurityDocument WHERE sd_URL='" & sDocument & "'")
        If iReader.Read() Then
            nDocument = iReader("ID")
            sDescription = iReader("sd_Title")
        End If
        iReader.Close()
        If nDocument = 0 Then
            dbPages.ExecSql("INSERT INTO tblSecurityDocument(sd_URL) VALUES ('" & sDocument & "')")
            nDocument = dbPages.TestVar(dbPages.ExecScalar("SELECT ID FROM tblSecurityDocument WHERE sd_URL='" & sDocument & "'"), 1, 0, 0)
        End If
        'If Not Page.IsPostBack Then LoadCheckboxes()
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        Dim iReader As SqlDataReader = dbPages.ExecReader("SELECT tblSecurityGroup.ID as ID, sg_Name, sg_Description, sdg_IsVisible, sdg_IsActive FROM tblSecurityDocumentGroup INNER JOIN tblSecurityGroup ON tblSecurityDocumentGroup.sdg_Group=tblSecurityGroup.ID WHERE sdg_Document=" & nDocument & " ORDER BY sg_Name")
        rptGroupList.DataSource = iReader
        rptGroupList.DataBind()
        iReader.Close()
        
        iReader = dbPages.ExecReader("SELECT -tblSecurityUser.ID as ID, su_Username, su_Name, sdg_IsVisible, sdg_IsActive FROM tblSecurityDocumentGroup INNER JOIN tblSecurityUser ON tblSecurityDocumentGroup.sdg_Group=-tblSecurityUser.ID WHERE sdg_Document=" & nDocument & " ORDER BY su_Username")
        rptUserList.DataSource = iReader
        rptUserList.DataBind()
        iReader.Close()
        MyBase.OnPreRender(e)
    End Sub
    
    Sub UpdateCheckboxes(idList As String, ByVal sField As String)
        Dim sSQL As String
        If String.IsNullOrEmpty(idList) Then idList = "0"
        sSQL = "UPDATE tblSecurityDocumentGroup SET sdg_" & sField & "=0 WHERE sdg_Group NOT IN (" & idList & ") AND sdg_Document=" & nDocument
        dbPages.ExecSql(sSQL)
        sSQL = "UPDATE tblSecurityDocumentGroup SET sdg_" & sField & "=1 WHERE sdg_Group IN (" & idList & ") AND sdg_Document=" & nDocument
        dbPages.ExecSql(sSQL)
    End Sub

	Sub btnUpdate_Click(ByVal o As Object, ByVal e As EventArgs)
        dbPages.ExecSql("Insert Into tblSecurityDocumentGroup(sdg_Document,sdg_Group) Select " & nDocument & ",tblSecurityGroup.ID From tblSecurityGroup Where Not Exists(Select ID From tblSecurityDocumentGroup Where sdg_Document=" & nDocument & " And sdg_Group=tblSecurityGroup.ID)")
        dbPages.ExecSql("Insert Into tblSecurityDocumentGroup(sdg_Document,sdg_Group) Select " & nDocument & ",-tblSecurityUser.ID From tblSecurityUser Where Not Exists(Select ID From tblSecurityDocumentGroup Where sdg_Document=" & nDocument & " And sdg_Group=-tblSecurityUser.ID)")
        UpdateCheckboxes(Request("chkIsVisible"), "IsVisible")
        UpdateCheckboxes(Request("chkIsActive"), "IsActive")
        If Request.QueryString("Frame") = String.Empty Then CloseWindow()
    End Sub

    Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        dbPages.ExecSql("DELETE FROM tblSecurityDocument WHERE ID=" & nDocument)
        CloseWindow()
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body onload="this.focus();">
	<form id="frmServer" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> SECURITY<span> - Document: <span class="item"><%=sDocument%></span></span></h1>
		<%
			If sDescription.Trim() <> String.Empty And sDescription.Trim() <> "Untitled" Then
				%>
					<h2>
						Title: "<%=sDescription%>"
					</h2>
				<%
			else
				Response.Write("<br />")
			End If
		%>
		<div class="bordered">
			You can grant to security groups permission to access this document by checking respective checkboxes.
		</div>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<th style="width:50px;">Visible</th>
				<th style="width:50px;">Active</th>
				<th style="width:150px; text-align:left;">Group Name</th>
				<th style="text-align:left;">Group Description</th>
			</tr>
            <asp:Repeater runat="server" ID="rptGroupList">
             <ItemTemplate>
                <tr bgcolor="#f5f5f5">
                    <td align="center">
                      <input type="checkbox" name="chkIsVisible" value='<%#Container.DataItem("ID")%>' <%#IIF(Container.DataItem("sdg_IsVisible"), " checked", "") %> />
                    </td>
                    <td align="center">
                      <input type="checkbox" name="chkIsActive" value='<%#Container.DataItem("ID")%>' <%#IIF(Container.DataItem("sdg_IsActive"), " checked", "") %> />
                    </td>
                    <td>
                      <asp:Literal runat="server" ID="ltGroupName" Text='<%#Container.DataItem("sg_Name")%>' />
                    </td>
                    <td>
                      <asp:Literal runat="server" ID="ltGroupDescription" Text='<%#Container.DataItem("sg_Description")%>' />
                    </td>
                 </tr>
             </ItemTemplate>
            </asp:Repeater>
			<tr><td style="border-bottom:0px"><br /><br /></td></tr>
			<tr>
				<th style="width:50px;">Visible</th>
				<th style="width:50px;">Active</th>
				<th style="width:150px; text-align:left;">User Name</th>
				<th style="text-align:left;">User Description</th>
			</tr>
            <asp:Repeater runat="server" ID="rptUserList">
             <ItemTemplate>
                <tr>
                    <td align="center">
                      <input type="checkbox" name="chkIsVisible" value='<%#Container.DataItem("ID")%>' <%#IIF(Container.DataItem("sdg_IsVisible"), " checked", "") %> />
                    </td>
                    <td align="center">
                      <input type="checkbox" name="chkIsActive" value='<%#Container.DataItem("ID")%>' <%#IIF(Container.DataItem("sdg_IsActive"), " checked", "") %> />
                    </td>
                    <td>
                      <asp:Literal runat="server" ID="ltGroupName" Text='<%#Container.DataItem("su_Username")%>' />
                    </td>
                    <td>
                      <asp:Literal runat="server" ID="ltGroupDescription" Text='<%#Container.DataItem("su_Name")%>' />
                    </td>
                 </tr>
             </ItemTemplate>
            </asp:Repeater>
		</table>
		<div class="lastButton">
			<span style="float:right;padding-bottom:1px;">
				<asp:Button ID="btnDelete" runat="server" CssClass="buttonWhite buttonDelete buttonWide" Text="Unmanage Document" OnClick="btnDelete_Click"
					OnClientClick="return confirm('Unmanaging document removes all access limitations.\n\nAny authenticated user will be allowed to see this document.\n\nDo you really want to unmanage this document?');" />
			</span>
			<asp:Button ID="btnUpdate" CssClass="buttonWhite" OnClick="btnUpdate_Click" Text="Save" runat="server" />
		</div>
	</form>
</body>
</html>