<%@ Page Language="VB" Inherits="htmlInputs" EnableViewState="true" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import namespace="System.Data.OleDb"%>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="radCln" %>

<script language="vbscript" runat="server">
	Dim npt As NPTotals 
	Dim strWhere As String = ""
	Dim BI_Amount As Decimal
	Dim bIsoWeeks As Boolean = True
	Private Function UpdateAmountsForDate(nFrom As DateTime, nTo As DateTime, nCurrency As Integer, nBankAccountID As Integer) As Decimal
		If npt Is Nothing Then npt = New NPTotals Else npt.Clear()
		npt.AddTransactions(nFrom, nTo, eSumType.ePS_NotArchive Or eSumType.ePS_ByCHBDate, nCurrency.ToString(), Nothing, Nothing, 0, Nothing, Nothing, nBankAccountID, Nothing, Nothing, Nothing, Nothing, Nothing, "BankAccount", DateTime.MinValue, DateTime.MaxValue)
		Return npt.Total.SubTotal - npt.Total.RunningCosts
	End Function

	Private Sub AddWeekLines(nFrom As DateTime, nTo As DateTime, nBankAccountID As Integer, nCurrency As Byte)
		If npt Is Nothing Then npt = New NPTotals Else npt.Clear()
		npt.AddTransactions(nFrom, nTo, eSumType.ePS_NotArchive Or eSumType.ePS_ByCHBDate, nCurrency, Nothing, Nothing, 0, Nothing, Nothing, nBankAccountID.ToString(), Nothing, Nothing, Nothing, Nothing, Nothing, IIf(bIsoWeeks, "InsWeek1Jun", "InsWeek"), DateTime.MinValue, DateTime.MaxValue)
		For Each r As RptData In npt.Values
			nFrom = NPTotals.GetDateByWeek(r.ItemID.Substring(0, 4), r.ItemID.Substring(4, 2), nTo, IIF(bIsoWeeks, -1, DayOfWeek.Monday))
			If (dbPages.ExecSql("Update tblBankIncomes Set BI_Amount=" & (r.SubTotal - r.RunningCosts).ToString() & _
				" Where BI_Currency=" & r.Currency & " And BI_BankAccountID=" & nBankAccountID & " And BI_FromDate='" & nFrom.ToString() & "' And BI_ToDate='" & nTo.ToString() & "'") = 0) Then
				dbPages.ExecSql("Insert Into tblBankIncomes(BI_InsDate, BI_FromDate, BI_ToDate, BI_BankAccountID, BI_Currency, BI_Amount, BI_Deposited, BI_AuthCode)Values(" & _
				 "'" & DateTime.Now.ToString() & "','" & nFrom.ToString() & "','" & nTo.ToString() & "'," & nBankAccountID & "," & r.Currency & "," & (r.SubTotal - r.RunningCosts).ToString() & "," & 0 & ",'')")
			End If	
		Next
	End Sub

	Private Sub ParseExcel(sFileName As String)
		Dim nBankAccountID As Integer = 0
		Dim nLinesAdded as Boolean = False
		Dim nCurMap As New System.Collections.Hashtable()
		Server.ScriptTimeout = 6000
		nCurMap.Add("ILS", 0) : nCurMap.Add("USD", 1) : nCurMap.Add("EUR", 2) : nCurMap.Add("GBP", 3)
		nCurMap.Add("AUD", 4) : nCurMap.Add("CAD", 5) : nCurMap.Add("JPY", 6) : nCurMap.Add("NOK", 7)
		'Dim sConString As String = "Provider=Microsoft Office 12.0 Access Database Engine OLE DB Provider;Data Source=" & sFileName & ";Extended Properties=""Excel 12.0;HDR=YES;"""
		Dim sConString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sFileName & ";Extended Properties=""Excel 8.0;"""
		Dim iReader As OleDbDataReader = Nothing
		Dim iDBCon As New OleDbConnection(sConString)
		iDBCon.Open()
		Dim iDBCom As New OleDbCommand("Select * From [Sheet1$]", iDBCon)
		Try
			iReader = iDBCom.ExecuteReader(System.Data.CommandBehavior.CloseConnection)
			While iReader.Read()
				If IsDate(iReader("ValueDate")) Then
					Dim nCur As Integer = nCurMap(iReader("PaymentCurrency"))
					If Not nLinesAdded Then 
						nBankAccountID = dbPages.TestVar(dbPages.ExecScalar("Select BA_ID From tblBankAccounts Where BA_AccountNumber='" & iReader("Account") & "'"), 0, -1, 0)
						AddWeekLines(New DateTime(2000, 1, 1), New DateTime(2009, 1, 1), nBankAccountID, nCur)
						nLinesAdded = True
					End If
					Dim nDateTo As Date, nDateFrom As Date = dbPages.TestVar(iReader("ValueDate"), DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue).Date
					Dim nWeekNo As Integer = ((nDateFrom.DayOfYear - 1) \ 7) + 1
					If Not bIsoWeeks Then nWeekNo = DatePart(DateInterval.WeekOfYear, nDateFrom, Microsoft.VisualBasic.FirstDayOfWeek.Monday, Microsoft.VisualBasic.FirstWeekOfYear.Jan1)
					nDateFrom = NPTotals.GetDateByWeek(nDateFrom.Year, nWeekNo, nDateTo, IIF(bIsoWeeks, -1, DayOfWeek.Monday))
					If (dbPages.ExecSql("Update tblBankIncomes Set BI_Deposited=BI_Deposited +" & dbPages.TestVar(iReader("Amount"), 0D, -1D, 0D) & _
						" Where BI_Currency=" & nCur & " And BI_BankAccountID=" & nBankAccountID & " And BI_FromDate='" & nDateFrom.ToString() & "' And BI_ToDate='" & nDateTo.ToString() & "'") = 0) Then
						BI_Amount = UpdateAmountsForDate(nDateFrom, nDateTo, nCur, nBankAccountID)
						dbPages.ExecSql("Insert Into tblBankIncomes(BI_InsDate, BI_FromDate, BI_ToDate, BI_BankAccountID, BI_Currency, BI_Amount, BI_Deposited, BI_AuthCode)Values(" & _
						 "'" & DateTime.Now.ToString() & "','" & nDateFrom.ToString() & "','" & nDateTo.ToString() & "'," & nBankAccountID & "," & nCur & "," & BI_Amount & "," & dbPages.TestVar(iReader("Amount"), 0D, -1D, 0D).ToString() & ",'')")
					End If
				End If
			End While
		Catch e As Exception
			lblError.CssClass = "errorMessage"
			lblError.Text = "���� ������ ����� " & e.Message
		Finally
			iDBCom.Dispose()
			iDBCon.Close()
			If Not iReader.IsClosed <> 0 Then iReader.Close()
		End Try
		Server.ScriptTimeout = 90
	End Sub
 
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		lblError.Text = ""
		If file1.HasFile Then
			Dim nFileName As String = Server.MapPath("/Data/IncomeFiles/" & System.IO.Path.GetFileName(Request.Files(0).FileName))
			Request.Files(0).SaveAs(nFileName)
			ParseExcel(nFileName)
		End If
		
		If Trim(Request("PageSize")) <> "" Then PGData.PageSize = Request("PageSize") Else PGData.PageSize = 25
		If IsPostBack Then
			If BI_BankAccountID.SelectedIndex <> -1 And BI_Currency.SelectedIndex <> -1 Then
				BI_Amount = UpdateAmountsForDate(BI_FromDate.SelectedDate.GetValueOrDefault(DateTime.Now), BI_ToDate.SelectedDate.GetValueOrDefault(DateTime.Now), BI_Currency.SelectedValue, BI_BankAccountID.SelectedValue)
				If Request("DOK") <> "" Then
					dbPages.ExecSql("Insert Into tblBankIncomes(BI_InsDate, BI_FromDate, BI_ToDate, BI_BankAccountID, BI_Currency, BI_Amount, BI_Deposited, BI_AuthCode)Values(" & _
					 "'" & DateTime.Now.ToString() & "','" & BI_FromDate.SelectedDate.GetValueOrDefault(DateTime.Now).ToString() & "','" & BI_ToDate.SelectedDate.GetValueOrDefault(DateTime.Now).ToString() & "'," & BI_BankAccountID.SelectedValue & "," & BI_Currency.SelectedValue & "," & BI_Amount & "," & dbPages.TestVar(BI_Deposited.Text, 0D, -1D, 0D).ToString() & ",'" & Replace(BI_AuthCode.Text, "'", "''") & "')")
					BI_Deposited.Text = "" : BI_AuthCode.Text = ""
				End If
			End If
		Else
			If Request.Form("DELBI_ID") <> "" Then dbPages.ExecSql("Delete From tblBankIncomes Where BI_ID IN(" & Request("DELBI_ID") & ")")
			Dim iReader As SqlDataReader = dbPages.ExecReader("Select * From tblBankAccounts Order By BA_BankName, BA_AccountNumber")
			While iReader.Read()
				BI_BankAccountID.Items.Add(New ListItem(iReader("BA_BankName") & ": " & iReader("BA_AccountNumber"), iReader("BA_ID")))
			End While
			iReader.Close()
			For i As Integer = 0 To eCurrencies.MAX_CURRENCY
				BI_Currency.Items.Add(New ListItem(dbPages.GetCurText(i), i))
			Next
			If dbPages.TestVar(Request("BankAccountID"), 0, -1, 0) <> 0 Then strWhere &= " And tblBankIncomes.BI_BankAccountID=" & dbPages.TestVar(Request("BankAccountID"), 0, -1, 0)
			If dbPages.TestVar(Request("iCurrency"), 0, -1, 0) <> 0 Then strWhere &= " And tblBankIncomes.BI_Currency=" & dbPages.TestVar(Request("iCurrency"), 0, -1, 0)
			If dbPages.TestVar(Request("amountFrom"), 0, -1, 0) <> 0 Then strWhere &= " And tblBankIncomes.BI_Amount >=" & dbPages.TestVar(Request("amountFrom"), 0D, -1D, 0D)
			If dbPages.TestVar(Request("amountTo"), 0, -1, 0) <> 0 Then strWhere &= " And tblBankIncomes.BI_Amount <=" & dbPages.TestVar(Request("amountTo"), 0D, -1D, 0D)
			If dbPages.TestVar(Request("DiffAmountFrom"), 0, -1, 0) <> 0 Then strWhere &= " And (BI_Deposited - BI_Amount) >=" & dbPages.TestVar(Request("DiffAmountFrom"), 0D, -1D, 0D)
			If dbPages.TestVar(Request("DiffAmountTo"), 0, -1, 0) <> 0 Then strWhere &= " And (BI_Deposited - BI_Amount) <=" & dbPages.TestVar(Request("DiffAmountTo"), 0D, -1D, 0D)
			If IsDate(Request("fromDate")) Then strWhere &= " And tblBankIncomes.BI_InsDate >='" & Request("fromDate") & " " & Request("fromTime") & "'"
			If IsDate(Request("toDate")) Then strWhere &= " And tblBankIncomes.BI_InsDate <='" & Request("toDate") & " " & Request("toTime") & "'"
			If IsDate(Request("BI_FromDateFrom")) Then strWhere &= " And tblBankIncomes.BI_FromDate >='" & Request("BI_FromDateFrom") & " " & Request("BI_FromDateFromTime") & "'"
			If IsDate(Request("BI_FromDateTo")) Then strWhere &= " And tblBankIncomes.BI_FromDate <='" & Request("BI_FromDateTo") & " " & Request("BI_FromDateToTime") & "'"
			If strWhere <> "" Then strWhere = " Where " & strWhere.Substring(5)
			If Request("Excel") <> "" Then PGData.PageSize = 10000
		End If
		PGData.OpenDataset("Select tblBankIncomes.*, (tblBankAccounts.BA_BankName + ': ' + tblBankAccounts.BA_AccountNumber) As BA_Title From tblBankIncomes Left Join tblBankAccounts ON(tblBankIncomes.BI_BankAccountID = tblBankAccounts.BA_ID) " & strWhere & " Order By BI_ID Desc")
		If Request("Excel") <> "" Then ExportToExcel()
    End Sub

	Private Sub ExportToExcel()
		Response.ContentType = "application/vnd.ms-excel"
		Response.AddHeader("Content-Disposition", "attachment;filename=report.xls")
		Response.Write("<table>")
		Response.Write("<tr><th>ID</th><th>Ins Date</th><th>From Day</th><th>To Day</th><th>Bank Account</th><th>Currency</th><th>Amount</th><th>Deposited</th><th>Diff</th><th>Auth Code</th></tr>")
		
		While PGData.Read()
			Dim nDiff As Decimal = PGData("BI_Deposited") - PGData("BI_Amount")
			Response.Write("<tr>")
			Response.Write("<td>" & PGData("BI_ID") & "</td>")
			Response.Write("<td>" & CType(PGData("BI_InsDate"), DateTime).ToString("d") & "</td>")
			Response.Write("<td>" & CType(PGData("BI_FromDate"), DateTime).ToString("d") & "</td>")
			Response.Write("<td>" & CType(PGData("BI_ToDate"), DateTime).ToString("d") & "</td>")
			Response.Write("<td>" & PGData("BA_Title") & "</td>")
			Response.Write("<td>" & dbPages.GetCurText(PGData("BI_Currency")) & "</td>")
			Response.Write("<td>" & PGData("BI_Amount") & "</td>")
			Response.Write("<td>" & PGData("BI_Deposited") & "</td>")
			Response.Write("<td style=""color:" & IIF(nDiff > 0, "blue", IIF(nDiff < 0, "red", "")) & ";"">" & nDiff & "</td>")
			Response.Write("<td>" & PGData("BI_AuthCode") & "</td>")
			Response.Write("</tr>")
		End While
		PGData.CloseDataset()
		Response.Write("</table>")
		Response.End()
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>BANK INCOMES</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<script language="JavaScript" src="../js/func_common.js"></script>
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" media="all" href="../StyleSheet/calendar-blue.css" title="winter" />
	<style type="text/css">
	    .dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript">
		function ExpandNode(fTrNum) {
			trObj = document.getElementById("tr" + fTrNum)
			imgObj = document.getElementById("oListImg" + fTrNum)
			if (trObj) {
				if (trObj.style.display == '') {
					imgObj.src = '../images/tree_expand.gif';	
					trObj.style.display = 'none';
				}
				else {
					imgObj.src = '../images/tree_collapse.gif';	
					trObj.style.display = '';
				}
			}
		}
	</script>
</head>
<body>
<%If Request("Totals") = "1" Then %>
    <table class="formThin" width="100%">
		<tr>
			<td>TOTALS</td>
			<td style="text-align:right;cursor:pointer;float:right;" onclick="document.getElementById('tblTotals').style.display='none';"><b>X</b></td>
		</tr>
    </table>
    <table class="formNormal" width="280" style="margin:8px 5px;">
		<tr>
			<th style="text-align:right">Estimated</th>
			<th style="text-align:right">Deposited</th>
			<th style="text-align:right">Diff</th>
		</tr>
      <%
      Dim sSQL As String = "SELECT BI_Currency, Sum(BI_Amount) As sTotal, Sum(BI_Deposited) As sDeposit FROM tblBankIncomes " & strWhere & " Group By BI_Currency"
      Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
      While iReader.Read()
      	  Response.Write("<tr>")
   		  Response.Write("<td>" & dbPages.FormatCurr(iReader("BI_Currency"), iReader("sTotal")) & "</td>")
   		  Response.Write("<td>" & dbPages.FormatCurr(iReader("BI_Currency"), iReader("sDeposit")) & "</td>")
   		  Response.Write("<td>" & dbPages.FormatCurr(iReader("BI_Currency"), iReader("sDeposit") - iReader("sTotal")) & "</td>")
      	  Response.Write("</tr>")
      End While  
	  iReader.Close()
      %>
    </table>
    <%Response.End()%>
<%End If%>   
<table align="center" class="formThin" width="95%">
	<tr>
		<td>
			<asp:label ID="lblPermissions" runat="server" /> 
			<span id="pageMainHeading">BANK INCOMES</span>
		</td>
	</tr>
</table>
<br />
<form id="Form1" runat="server" method="post">
<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
<table align="center" class="formThin" width="95%">
<tr>
	<td>
		<img onclick="ExpandNode('0');" style="cursor:pointer; margin-right:4px;" id="oListImg0" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="top"><span style="cursor:pointer;" onclick="ExpandNode('0');">Add Manually / Import From File</span>
	</td>
</tr>
<tr id="tr0" style="display:none;">
	<td style="padding-left:20px;">
		<table cellpadding="0" cellspacing="0">
		<tr>
			<td valign="top">
				<fieldset>
					<legend>Add Manually</legend>
					<table class="FormThin" width="600">
						<tr>
							<th>Transactions</th>
							<td>
								<asp:RadioButton runat="server" ID="UseTransDate" GroupName="TransDate" Checked="true" Enabled="false" />By Trans Date
								<asp:RadioButton runat="server" ID="UsePDate" GroupName="TransDate" Enabled="false" />By Credit Date<br />
							</td>
							<td nowrap rowspan="3">&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<th>Deposited</th><td><asp:TextBox runat="server" id="BI_Deposited" Width="70" /> <asp:DropDownList runat="server" ID="BI_Currency" AutoPostBack="true" /></td>
						</tr><tr>
							<th nowrap>Dates</th><td><radCln:RadDatePicker id="BI_FromDate" Runat="server" Width="100" /> To <radCln:RadDatePicker id="BI_ToDate" Runat="server" Width="100" /></td>
							<th>Auth Code</th><td><asp:TextBox runat="server" id="BI_AuthCode" Width="70" /> <input type="submit" name="DOK" value="  ADD  " /></td>
						</tr><tr>
							<th>Account</th><td><asp:DropDownList runat="Server" id="BI_BankAccountID" AutoPostBack="true" style="width:100%" /></td>
							<th nowrap>Estimated Total</th><td><%=dbPages.FormatCurr(BI_Currency.SelectedValue, BI_Amount)%></td>
						</tr>
					</table>
				</fieldset>
			</td>
			<td nowrap>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td valign="top">
				<fieldset>
					<legend>Import From File</legend>
					<table class="FormThin" height="78px">
					  <tr><th>Upload File<br /><asp:FileUpload ID="file1" runat="server" /></th></tr>
					  <tr>
						<td style="text-align:right">
							<asp:Label ID="lblError" runat="server"></asp:Label>
							<input type="submit" value=" Upload " />
						</td>
					  </tr>
					</table>  
				</fieldset>
			</td>
		  </tr>
		</table><br />
	</td>
</tr>
</table>
</form>
<form method="post">
 <table class="formNormal" width="95%" align="center">
	<tr>
		<th>ID</th>
		<th>Ins Date</th>
		<th>From Day</th>
		<th>To Day</th>
		<th>Bank Account</th>
		<th>Estimated</th>
		<th>Deposited</th>
		<th>Diff</th>
		<th>Auth Code</th>
		<th>Del</th>
	</tr>
 <%
 While PGData.Read()
   Dim nDiff As Decimal = PGData("BI_Deposited") - PGData("BI_Amount")
  %>
	<tr>
		<td><%=PGData("BI_ID")%></td>
		<td><%=CType(PGData("BI_InsDate"), DateTime).ToString("d")%></td>
		<td><%=CType(PGData("BI_FromDate"), DateTime).ToString("d")%></td>
		<td><%=CType(PGData("BI_ToDate"), DateTime).ToString("d")%></td>
		<td><%=PGData("BA_Title")%></td>
		<td nowrap><%=dbPages.FormatCurr(PGData("BI_Currency"), PGData("BI_Amount"))%></td>
		<td nowrap><%=dbPages.FormatCurr(PGData("BI_Currency"), PGData("BI_Deposited"))%></td>
		<td nowrap style="color:<%=IIF(nDiff > 0, "blue", IIF(nDiff < 0, "red", ""))%>;" ><%=dbPages.FormatCurr(PGData("BI_Currency"), nDiff)%></td>
		<td><%=PGData("BI_AuthCode")%></td>
		<td><input type="checkbox" value="<%=PGData("BI_ID")%>" name="DELBI_ID" /></td>
	</tr>
 <%
 End While
 PGData.CloseDataset()
 %>
 </table>
 <br />
 <table align="center" style="width:92%">
	<tr>
		<td><UC:Paging runat="Server" id="PGData" PageID="PageID"  /></td>
		<td><input type="submit" value="Delete Selected" /></td>
		<td align="right">
			<input type="button" id="TotalsButton" style="font-size:11px;cursor:pointer;" onclick="with(document.getElementById('tblTotals')){style.display='block';if(onresize)onresize();};setAjaxHTML('?<%=Request.QueryString.ToString()%>&Totals=1', document.getElementById('tblTotals'), true);" value="SHOW TOTALS &Sigma;">
		</td>
	</tr>
 </table>
</form>
<div id="tblTotals" onresize="PosObject(this, document.getElementById('TotalsButton'), 'RRTB', 0, -10);" style="border-color:#484848; border-style:solid; border-width:1px 3px; position:absolute;width:290px;background-color:White;display:none;"> &nbsp; LOADING DATA... </div>
</body>
</html>
