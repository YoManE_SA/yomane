<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<script runat="server">
	Dim sSQL As String, sStatusColor As String, sQueryWhere As String, sAnd As String, sQueryString As String, btnTxt As String, sTitle As String, failSourceArr() As String, sDivider As String
	Dim i As Integer, debitCompanyId As Integer, failSource As Integer
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		debitCompanyId = Convert.ToInt32(Request.QueryString("debitCompany_id"))
		If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") _
			Else PGData.PageSize = 30
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		Dim sPageID As String = Request("PageID")
		sQueryString = dbPages.SetUrlValue(sQueryString, "PageID", IIf(String.IsNullOrEmpty(sPageID), "1", sPageID))
		sQueryWhere = " DebitCompanyID = " & debitCompanyId & ""
		If Trim(Request("FailSource")) <> "" Then sQueryWhere = sQueryWhere & sAnd & " AND (tblDebitCompanyCode.FailSource = " & Trim(Request("FailSource")) & ")" : sAnd = " AND "
		If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere
		If Request.Form("debitCompanyCode_id") <> "" Then
			dbPages.ExecSql("UPDATE tblDebitCompanyCode SET DescriptionMerchantHeb = N'" & dbPages.DBText(Request("iDescriptionMerchantHeb")) & _
			"', DescriptionMerchantEng = N'" & dbPages.DBText(Request("iDescriptionMerchantEng")) & _
			"', DescriptionCustomerEng = N'" & dbPages.DBText(Request("iDescriptionCustomerEng")) & _
			"', DescriptionCustomerHeb = N'" & dbPages.DBText(Request("iDescriptionCustomerHeb")) & _
			"', ChargeFail=" & dbPages.TestVar(Request("iChargeFail"), 1, 0, 0) & _
			", BlockCC=" & dbPages.TestVar(Request("iBlockCC"), 1, 0, 0) & _
			", BlockMail=" & dbPages.TestVar(Request("iBlockMail"), 1, 0, 0) & _
			", RecurringAttemptsSeries=" & dbPages.TestVar(Request("iRecurringAttemptsSeries"), 1, 100, 0) & _
			", RecurringAttemptsCharge=" & dbPages.TestVar(Request("iRecurringAttemptsCharge"), 1, 100, 0) & _
			", FailSource=" & dbPages.TestVar(Request("iFailSource"), 0, 2, 0) & _
			", LocalError =" & "'" & dbPages.DBText(Request("iLocalError")) & "'" & _
			", IsCascade =" & dbPages.TestVar(Request("iIsCascade"), 0, 1, 0) & _
			" Where ID =" & dbPages.TestVar(Request("debitCompanyCode_id"), 1, 0, 0))
		End If
		NetpayConst.GetConstArray(46, 1, sTitle, failSourceArr)
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Admincash</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript" type="text/javascript">
		var ResArray = new Array();
		<%
			Dim iReader As SqlDataReader = dbPages.ExecReader("Select Code, DescriptionOriginal From tblDebitCompanyCode Where DebitCompanyID=1 Order By Code")
			While iReader.Read()
				Response.Write("ResArray['" & iReader("Code") & "']='" & Replace(iReader("DescriptionOriginal"), "'", "\'") & "';" & VbCrLf)
			End While
			iReader.Close()
		%>
	
		function IconVisibility(fVar) {
			spanObj = document.getElementById(fVar)
			if (spanObj.style.visibility == 'hidden') {
				spanObj.style.visibility = 'visible';
			}else {
				spanObj.style.visibility = 'hidden';
			}
		}
		function ExpandNode(fTrNum, cellID) {
			trObj = document.getElementById("trInfo" + fTrNum)
			imgObj = document.getElementById("oListImg" + fTrNum)
			if (trObj) {
				if (trObj.style.display == '') {
					imgObj.src = '../images/tree_expand.gif';	
					trObj.style.display = 'none';
				}else {
					imgObj.src = '../images/tree_collapse.gif';	
					trObj.style.display = '';
					var CmbCodes = document.getElementById("CmbCodes" + fTrNum);
				   
					if(CmbCodes.options.length > 0) return;
						var opt = document.createElement("OPTION");
						opt.innerHTML = "";
						opt.value = "";
						CmbCodes.appendChild(opt)
					for(key in ResArray){
						var opt = document.createElement("OPTION");
						opt.innerHTML = key + " - " + ResArray[key] ;
						opt.value = key;
						CmbCodes.appendChild(opt);
						if(key == cellID) CmbCodes.selectedIndex = CmbCodes.options.length-1;
					}
				}
			}
		}
		
		function ExpandAll(fDisplay, fImgStatus) {
			for(i = 0; i <= <%= i %>; i++) {
			trObj = document.getElementById("trInfo" + i)
				imgObj = document.getElementById("oListImg" + i)
				if (trObj) {
					imgObj.src = '../images/tree_' + fImgStatus + '.gif';
					trObj.style.display = fDisplay;
				}
			}
		}
	</script>
</head>

<body>
	<form id="form1" runat="server">
	<table align="center" style="width:92%">
	<tr>
		<td colspan="2" id="pageMainHeadingTd">
			<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">REPLY CODES</span>
			- <%=UCase(request("dc_name")) %> &nbsp;
			<%
			If request("debitCompany_id") > 0 Then
			        If IO.File.Exists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & Trim(Request("debitCompany_id")) & ".gif")) Then
			            Response.Write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(Request("debitCompany_id")) & ".gif"" align=""middle"" border=""0"" />")
			        End If
			End If
			%>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td style="font-size:11px; padding-bottom:4px;">
		(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');">Collapse All</a>)
		</td>
		<td colspan="2" align="right" style="font-size:11px;">
		Filters:
		<%
		If Trim(Request("FailSource")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "FailSource", Nothing) & "';"">Fail Source</a>") : sDivider = ", "
		If sDivider <> "" Then Response.Write(" | (click to remove)") _
			Else Response.Write("Add to filter by clicking on returned fields")
		%>
		</td>
	</tr>
	</table>
	</form>
	<table class="formNormal" align="center" style="width:92%">
	<tr>
		<th rowspan="2"></th>
		<th rowspan="2"></th>
		<th rowspan="2">Code</th>
		<th rowspan="2">Original Merchant Description</th>
		<th rowspan="2" style="text-align:center;">Block CC</th>
		<th rowspan="2" style="text-align:center;">Block Email</th>
		<th rowspan="2" style="text-align:center;">Apply Fee</th>
		<th colspan="2" style="border-bottom-style:none;">Recurring Attempts</th>
		<th rowspan="2" align="right">Reject Type</th>
		<th rowspan="2" align="right">Cascaded</th>
	</tr>	
	<tr>
		<th align="right">Charge</th>
		<th align="right">Series</th>
	</tr>
	<%
	sSQL = "SELECT tblDebitCompanyCode.* FROM tblDebitCompanyCode " & sQueryWhere & " ORDER BY Code ASC"
	PGData.OpenDataset(sSQL)
	Dim nTrInfo As Integer = dbPages.TestVar(Request("trInfo"), 1, 0, 0)
	While PGData.Read()
		i = i + 1
		sStatusColor = "#ff6666"	
		%>
		<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='#ffffff';">
			<td><img onclick="ExpandNode('<%=i%>', '<%=PGData("LocalError")%>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" /><br /></td>
			<td><span style="background-color:<%= sStatusColor %>;">&nbsp;</span></td>
			<td><%=PGData("Code")%></td>
			<td><%=PGData("DescriptionOriginal")%></td>
			<td style="text-align:center;"><img src="../images/<%=IIf(PGData("BlockCC"),"checkbox.gif","checkbox_off.gif")%>" align="top" height="16" border="0"></td>
			<td style="text-align:center;"><img src="../images/<%=IIf(PGData("BlockMail"),"checkbox.gif","checkbox_off.gif")%>" align="top" height="16" border="0"></td>
			<td style="text-align:center;"><img src="../images/<%=IIf(PGData("ChargeFail"),"checkbox.gif","checkbox_off.gif")%>" align="top" height="16" border="0"></td>
			<td><%=PGData("RecurringAttemptsCharge")%></td>
			<td><%=PGData("RecurringAttemptsSeries")%></td>
			<td style="white-space:nowrap;">
				<%
				If Trim(Request("FailSource")) <> "" Then Response.Write(failSourceArr(PGData("FailSource"))) _
				   Else dbPages.showFilter(failSourceArr(PGData("FailSource")), "?" & sQueryString & "&FailSource=" & PGData("FailSource"), 1)
				%>		
		   </td>
			<td><%=PGData("IsCascade")%></td>
		</tr> 
		<tr id="trInfo<%=i%>" style="display:<%=IIf(i=ntrinfo, "block", "none")%>;">
			<td colspan="2"></td>
			<td colspan="9" style="border-top:1px dashed #e2e2e2;">
				<form method="post" action="?<%=sQueryString%>">
				<input type="hidden" name="debitCompanyCode_id" value="<%=PGData("id")%>" />
				<input type="hidden" name="trInfo" value="<%=i%>" />
					<table border="0" class="formNormal">
					<tr><td height="10"></td></tr>
					<tr>
						<td>Local Error</td>
						<td><select style="width:300px;" name="iLocalError" id="CmbCodes<%=i%>" /></td>
						<td>Reject Type</td>
						<td>
							<select name="iFailSource">
							<% 
							For i As Integer = 0 To 2
								Response.Write("<option " & IIf(PGData("FailSource") = i, "selected", "") & " value=""" & i & """>" & failSourceArr(i) & "</option>")
							Next
							%>
							</select>
						</td>
						<td>
							<input type="checkbox" class="option" name="iChargeFail" value="1" <%=IIf(PGData("ChargeFail"), "checked=""checked""", String.Empty)%> />Apply Fee
						</td>
					</tr>	
					<tr>
						<td>Merchant Desc Heb</td>
						<td><input style="width:300px;direction:rtl;" name="iDescriptionMerchantHeb" value="<%=PGData("DescriptionMerchantHeb")%>" /></td>
						<td>Recurring Attempts Series</td>
						<td><input style="width:30px;" name="iRecurringAttemptsSeries" value="<%=PGData("RecurringAttemptsSeries")%>" /></td>
						<td>
							<input type="checkbox" class="option" name="iBlockCC" value="1" <%=IIf(PGData("BlockCC"), "checked=""checked""", String.Empty)%> />Block Credit Card
						</td>
					</tr>	
					<tr>
						<td>Merchant Desc Eng</td>
						<td><input style="width:300px;" name="iDescriptionMerchantEng" value="<%=PGData("DescriptionMerchantEng")%>" /></td>
						<td>Recurring Attempts Charge</td>
						<td><input style="width:30px;" name="iRecurringAttemptsCharge" value="<%=PGData("RecurringAttemptsCharge")%>" /></td>
						<td>
							<input type="checkbox" class="option" name="iBlockMail" value="1" <%=IIf(PGData("BlockMail"), "checked=""checked""", String.Empty)%> />Block Email Address
						</td>
					</tr>
					<tr>
						<td>Customer Desc Heb</td>
						<td><input style="width:300px;direction:rtl;" name="iDescriptionCustomerHeb" value="<%=PGData("DescriptionCustomerHeb")%>" /></td>
						<td></td>
						<td></td>
						<td>
							<input type="checkbox" class="option" name="iIsCascade" value="1" <%=IIf(PGData("IsCascade"), "checked=""checked""", String.Empty)%> />Continue next cascaded termianl
						</td>
					</tr>
					<tr>
						<td>Customer Desc Eng</td>
						<td><input style="width:300px;" name="iDescriptionCustomerEng" value="<%=PGData("DescriptionCustomerEng")%>" /></td>
						<td colspan="3" style="text-align:right;"><input value=" Update " type="submit" /></td>
					</tr>
					<tr><td height="10"></td></tr>
					</table>
				</form>
			</td>
		</tr>
		<tr><td height="1" colspan="11" bgcolor="silver"></td></tr>
		<%
	End while
	PGData.CloseDataset()
	%>
	</table>
	 <script language="JavaScript" type="text/javascript">
			function ExpandAll(fDisplay, fImgStatus) {
				for(i = 0; i <= <%= i %>; i++) {
					trObj = document.getElementById("trInfo" + i)
					imgObj = document.getElementById("oListImg" + i)
					if (trObj) {
						imgObj.src = '../images/tree_' + fImgStatus + '.gif';
						trObj.style.display = fDisplay;
					}
				}
			}
	 </script>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" align="middle"> ADD TO FILTER
		</div>
   
	<table align="center" style="width:92%">
	<tr>
		<td>
			<UC:Paging runat="Server" id="PGData" PageID="PageID" />
		</td>
	</tr>
	</table>
	
</body>
</html>

