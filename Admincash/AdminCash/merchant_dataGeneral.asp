<%
response.buffer = true
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/func_htmlInputs.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
nCompanyID = TestNumVar(Request("CompanyID"), 0, -1, 0)

If trim(request("Action"))="update" Then
	
	'"AffiliateID=" & int(Request("AffiliateID")) & "," &
	'"SubAffiliateID=" & int(Request("SubAffiliateID")) & "," &

	sSQL = "UPDATE tblCompany SET " & _
		" languagePreference='" & dbText(Request("languagePreference")) & "'," & _
		" isNetpayTerminal=" & TestNumVar(Request("isNetpayTerminal"),1,1,0) & "," & _
		" ReportMailOptions=" & TestNumVar(Request("ReportMailOptions"), 0, -1, 0) & "," & _
		" DebitCompanyExID='" & Replace(Request("DebitCompanyExID"), "'", "''") & "'," & _
		" ReportMail='" & Replace(Request("ReportMail"), "'", "''") & "'" & _
		" WHERE ID=" & nCompanyID
    '" IsUsingNewTerminal=" & TestNumVar(Request("IsUsingNewTerminal"), 0, -1, 0) & "," & _
	oledbData.Execute sSQL
End if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css"/>
	<style type="text/css">
	    input.inputData { width:90%; }
	</style>
	<script type="text/javascript">
		function ConfirmClone(frmClone) {
			if (frmClone.CurrentCompanyName.value==frmClone.CloneCompanyName.value)
			{
				alert("Please change a new merchant company name!");
				frmClone.CloneCompanyName.select();
				frmClone.CloneCompanyName.focus();
				return false;
			}
			if (frmClone.CurrentUsername.value+frmClone.CurrentMail.value==frmClone.CloneUsername.value+frmClone.CloneMail.value) {
				alert("Please change either a new merchant username or its mail!");
				frmClone.CloneUsername.select();
				frmClone.CloneUsername.focus();
				return false;
			}
			return confirm("Do you really want to clone '"+frmClone.CurrentCompanyName.value+"' as '"+frmClone.CloneCompanyName.value+"' ?!");
		}
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<%
sSQL="SELECT tblCompany.* FROM tblCompany WHERE tblCompany.ID=" & nCompanyID
set rsData = oledbData.Execute(sSQL)
if not rsData.EOF then
	sCloneCompanyName=rsData("CompanyName")
	sCloneUsername=rsData("Username")
	sCloneMail=rsData("Mail")
	%>
	<table align="center" border="0" cellpadding="0" cellspacing="2" dir="ltr" width="95%">
	<tr>
		<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" dir="ltr" width="100%">
			<!--#include file="Merchant_dataHeaderInc.asp"-->
			<%
			pageSecurityLevel = 0
			pageSecurityLang = "ENG"
			pageClosingTags = "</table></td></tr></table>"
			Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
			%>
			<tr><td height="20"></td></tr>
			<!--#include file="Merchant_dataMenuInc.asp"-->
			</table>
		</td>
	</tr>
	<tr><td><br/></td></tr>
	</table>
	
	<form action="merchant_dataGeneral.asp" name="frmCoopDetail" method="post">
	<input type="hidden" name="CompanyID" value="<%= nCompanyID %>">
	<input type="hidden" name="Action" value="update">
	<table align="center" border="0" cellpadding="0" cellspacing="2" dir="ltr" width="95%">
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<table align="center" border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
			<tr>
				<td colspan="4" class="MerchantSubHead">General<br /></td>
			</tr>
			<tr>
				<td class="txt11">Preferred language </td>
				<td class="txt11">
					<select name="languagePreference" class="inputData" style="width:150px;">
						<option value="eng" <% if trim(rsData("languagePreference"))="eng" then %>selected<% End If %>>English</option>
						<option value="heb" <% if trim(rsData("languagePreference"))="heb" then %>selected<% End If %>>Hebrew</option>
					</select>
				</td>
				<td class="txt11" valign="top">affiliates </td>
				<td class="txt11">
					<%
					sSQL="SELECT a.affiliates_id, a.name" & _
						" FROM [Setting].[SetMerchantAffiliate] mf" & _
						" Left Join tblAffiliates a ON(a.affiliates_id = mf.Affiliate_id)" & _
						" where mf.Merchant_id = " & nCompanyID & _
						" ORDER BY Name"
					set rsData3 = oledbData.Execute(sSQL)
					Do While Not rsData3.EOF
						Response.Write(rsData3(0) & ": " & rsData3(1) & "<Br/>")
					  rsData3.MoveNext
					Loop
					rsData3.close
					Set rsData3 = nothing
					%>
				</td>
			</tr>
			<!--<tr>
				<td class="txt11">Terminal to use</td>
				<td class="txt11">
					<select name="IsUsingNewTerminal" class="inputData" style="width:150px;">
						<option value="0" <% if NOT rsData("IsUsingNewTerminal") then %>selected<% End If %>>Old Terminal
						<option value="1" <% if rsData("IsUsingNewTerminal") then %>selected<% End If %>>New Terminal
					</select>
				</td>
			</tr>-->
			<tr>
				<td class="txt11">Terminal owned by</td>
				<td class="txt11">
					<select name="isNetpayTerminal" class="inputData" style="width:150px;">
						<option value="1" <% if rsData("isNetpayTerminal") then %>selected<% End If %>><%= COMPANY_NAME_1 %>
						<option value="0" <% if NOT rsData("isNetpayTerminal") then %>selected<% End If %>>Merchant
					</select>
				</td>
				<td class="txt11">Merchant visit counter</td>
				<td>
					<input type="text" class="inputData" dir="ltr" name="CountAdminView" value="<%= rsData("CountAdminView") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Identifier (for credit companies)</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="DebitCompanyExID" value="<%= rsData("DebitCompanyExID") %>">
				</td>
				<td class="txt11">Identifier (for internal use)</td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" disabled="disabled" value="<%= rsData("SecurityKey") %>">
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td class="txt11" valign="top"><input type="checkbox" name="ReportMailOptions" value="1" <%=IIF(rsData("ReportMailOptions") > 0, "checked", "") %> />Enable Report Sending</td>
				<td>Email list:<br /><textarea name="ReportMail" class="inputData" rows="5" cols="30"><%=rsData("ReportMail")%></textarea></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td height="12"></td></tr>
	<tr><td bgcolor="#A9A7A2" height="1"></td></tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td align="right"><input type="submit" value="Save Changes" style="width:110px; height:auto; background-color:#f1f1f1;"></td>
	</tr>
	</table>
	</form>
	<br /><br />
	<table align="center" border="0" cellpadding="0" cellspacing="2" dir="ltr" width="95%">
	<tr>
		<td valign="top">
			<table align="center" border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
			<tr><td class="MerchantSubHead">Links<br /></td></tr>
			<tr>
				<td dir="ltr" class="txt11">
					<!--Application for opening hosted payment page: <a href="company_windowChargeOpen.asp?companyID=<%= request("companyID") %>&companyNum=<%= rsData("CustomerNumber") %>">Continue &gt;&gt;</a><br/>-->
					Signup in "Payment Systems" for electronic wallet account: <a href="Merchant_pslRegister.asp?companyID=<%= request("companyID") %>&companyNum=<%= rsData("CustomerNumber") %>">Continue &gt;&gt;</a><br/>
				</td>
			</tr>
			</table>
		</td>
		<td valign="top">
			<table align="center" border="0" cellpadding="0" cellspacing="2" width="100%">
			<tr>
				<td>
					<form method="post"><!-- action="merchant_clone.aspx" onsubmit="return ConfirmClone(this);"-->
					<input type="hidden" name="Merchant" value="<%= nCompanyID %>" />
						<table border="0" cellpadding="2" cellspacing="2" class="formNormal">
						<tr>
							<td class="MerchantSubHead" colspan="3">Clone Merchant<br /></td>
						</tr>
						<tr><td height="5"><br /></td></tr>
						<tr>
							<td><br /></td>
							<td>Current Merchant<br /></td>
							<td>New Merchant<br /></td>
						</tr>
						<tr>
							<td>Merchant Name<br /></td>
							<td><input name="CurrentCompanyName" value="<%= sCloneCompanyName %>" readonly="readonly" disabled="disabled" /><br /></td>
							<td><input name="CloneCompanyName" value="<%= sCloneCompanyName %>" disabled="disabled" /><br /></td>
						</tr>
						<tr>
							<td style="white-space:nowrap;">Login - Username<br /></td>
							<td><input name="CurrentUsername" value="<%= sCloneUsername %>" readonly="readonly" disabled="disabled" /><br /></td>
							<td><input name="CloneUsername" value="<%= sCloneUsername %>" disabled="disabled" /><br /></td>
						</tr>
						<tr>
							<td>Login - Email<br /></td>
							<td><input name="CurrentMail" value="<%= sCloneMail %>" readonly="readonly" disabled="disabled" /><br /></td>
							<td><input name="CloneMail" value="<%= sCloneMail %>" disabled="disabled" /><br /></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>
								<input type="checkbox" name="CopyTerminals" value="1" disabled="disabled" style="vertical-align:middle;margin-left:-3px;padding-left:0;" />
								Copy terminals<br /><br />
								<input type="submit" value=" CLONE " disabled="disabled" class="button1" style="color:Maroon;" /><br />
							</td>
						</tr>
						</table>
					</form>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<%
end if
%>
</body>
<%
rsData.close
Set rsData = nothing
call closeConnection()
%>
</html>