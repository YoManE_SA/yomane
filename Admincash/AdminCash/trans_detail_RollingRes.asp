<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<%
sTransID = trim(request("transID"))
sPageBkgColor = trim(request("bkgColor"))
If sPageBkgColor = "" Then sPageBkgColor = "ffffff"
sPaymentMethod = trim(request("PaymentMethod"))
If sPaymentMethod - PMD_RolRes <> 0 Then Response.End

sQuery = "transID=" & sTransID & "&bkgColor=" & sPageBkgColor & "&PaymentMethod=" & sPaymentMethod

Msg = ""
If Trim(request("Action"))="UpdTransData" Then
	sSQL="UPDATE tblCompanyTransPass SET Comment='" & DBText(request("transComment")) & "' WHERE id=" & sTransID
	oledbData.Execute sSQL
	Msg = "<b>&nbsp; Update successfully " & FormatDatesTimes(Now(),1,1,0) & "</b>"
End if
Dim sCompanyID, sCurrency, sComment, sOriginalTransID

sSQL = "SELECT OriginalTransID, Comment, CompanyID, Currency, CreditType From tblCompanyTransPass WHERE id=" & sTransID
Set rsData = oledbData.execute(sSQL)
	sComment = ""
	If NOT rsData.EOF Then 
		sOriginalTransID = rsData("OriginalTransID")
		sCreditType = rsData("CreditType")
		sComment = rsData("Comment")
		sCompanyID = rsData("CompanyID")
		sCurrency = rsData("Currency")
	End if	
rsData.close
set rsData = nothing
call closeConnection()
%>
<html>
<head>
    <title>Admin Details</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
</head>
<body leftmargin="5" topmargin="0" rightmargin="15" class="itext" bgcolor="#<%= sPageBkgColor %>">
	<table border="0" cellspacing="0" cellpadding="1" width="100%" height="100%" style="border-top:1px dashed #e2e2e2;">
	<tr><td height="15"></td></tr>
	<tr>
		<td>
			<form action="?<%=sQuery%>" method="Post">
			<input type="hidden" name="Action" value="UpdTransData" />
				Comment &nbsp;
				<input type="text" name="transComment" value="<%=dbText(sComment)%>" style="width:550px; background-color:#FFFFEC;" /> &nbsp;&nbsp;
				<input type="submit" value="SAVE" class="button2" /> &nbsp;&nbsp;&nbsp;
				<%If sCreditType = 0 Then%>
				  <%If sOriginalTransID = "0" Then %>
					<a target="_parent" href="Merchant_PayCreate.asp?Type=RetReserve&companyID=<%=sCompanyID%>&Currency=<%=sCurrency%>&ResID=<%=sTransID%>" onclick="if(!confirm('This action will create a new settlement to return this reserve, Sure to process?')) return false;">Release Reserve</a>
				  <%Else%>
					This reserve already released
				  <%End If%>
				<%End if%>  
				<br />
				<%If Trim(Msg)<>"" Then Response.Write(Msg) %><br />
			</form>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	</table>
</body>
</html>
