<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Import Namespace="System.Data.SqlClient" %>


<script runat="server">
	Dim iReader As SqlDataReader
	Dim ChargeMade As Boolean = False
	Dim bIsChargeMade As Boolean = False
	Dim sTerminalNumber As String = ""
	Dim sDebitCompany As String = ""
	Dim DCF_TerminalNumber As String = ""
	Dim DCF_DebitCompanyID As String = ""
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		Dim sQueryWhere As String
		If Not Page.IsPostBack Then
			Dim nTerminalID As Integer = dbPages.TestVar(Request("terminalID"), 1, 0, 0)
			If nTerminalID > 0 Then
				litTerminalName.Text = dbPages.ExecScalar("SELECT dc_Name+' | '+TerminalNumber+' | '+dt_Name FROM tblDebitCompany" & _
				" INNER JOIN tblDebitTerminals ON DebitCompany_ID=DebitCompany WHERE ID=" & nTerminalID)
				tlbTop.AddItem("Terminal Details", "system_terminalTermDetail.aspx?terminalID=" & nTerminalID)
				tlbTop.AddItem("Card Company Details", "system_terminalCardDetail.aspx?terminalID=" & nTerminalID,True)
				tlbTop.AddItem("Fees & Payouts", "system_terminalFees.aspx?terminalID=" & nTerminalID)
				tlbTop.AddItem("Notifications", "system_terminalRules.aspx?TerminalID=" & nTerminalID)
				tlbTop.AddItem("Miscellaneous", "system_terminalMisc.aspx?terminalID=" & nTerminalID)
				sQueryWhere = "tblDebitTerminals.id=" & nTerminalID.ToString
				terminalID.Value = nTerminalID
			Else
				sQueryWhere = "tblDebitTerminals.terminalNumber='" & request("terminalNumber") & "'"
				litTerminalName.Text = "All Debit Companies"
			End If
			Dim sSQL As New StringBuilder(String.Empty)
			sSQL.Append("SELECT tblDebitTerminals.*, dbo.GetDecrypted256(accountPassword256) accountPassword, dbo.GetDecrypted256(accountPassword3D256) accountPassword3D")
			sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransPass WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransAnswer")
			sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransFail WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransFail")
			sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransApproval WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransApprovalOnly")
			sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransRemoved WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransDel")
			sSQL.Append(" FROM tblDebitTerminals WHERE " & sQueryWhere)
			iReader = dbPages.ExecReader(sSQL.ToString)
			If iReader.HasRows then				
				While iReader.Read
					If iReader("isActive") then
						ltIsActiveTerm.Text = "<span><li type=square style=color:#66cc66;> <span class=txt11>Active</span></li></span>"
					Else
						ltIsActiveTerm.Text = "<span><li type=square style=color:#ff6666;> <span class=txt11>Blocked</span></li></span>"
					End If
					txCompanyNum_visa.Text = iReader("dt_CompanyNum_visa")
					txComments_visa.Text = iReader("dt_Comments_visa")
					txCompanyNum_mastercard.Text = iReader("dt_CompanyNum_mastercard")
					txComments_mastercard.Text = iReader("dt_Comments_mastercard")
					txCompanyNum_diners.Text = iReader("dt_CompanyNum_diners")
					txComments_diners.Text = iReader("dt_Comments_diners")
					txCompanyNum_americanexp.Text = iReader("dt_CompanyNum_americanexp")
					txComments_americanexp.Text = iReader("dt_Comments_americanexp")
					txCompanyNum_isracard.Text = iReader("dt_CompanyNum_isracard")
					txComments_isracard.Text = iReader("dt_Comments_isracard")
					txCompanyNum_direct.Text = iReader("dt_CompanyNum_direct")
					txComments_direct.Text = iReader("dt_Comments_direct")
				End While
			Else
				pnlTRM.Visible = False
			End If
			iReader.Close
		End If
	End Sub
	
	Private Sub UpdateDetails(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim sSQL As New StringBuilder(String.Empty)
		sSQL.Append("UPDATE tblDebitTerminals SET ")
		sSQL.Append("dt_CompanyNum_visa='" & dbPages.DBText((""&txCompanyNum_visa.Text).Trim) & "',")
		sSQL.Append("dt_Comments_visa='" & dbPages.DBText((""&txComments_visa.Text).Trim) & "',")
		sSQL.Append("dt_CompanyNum_isracard='" & dbPages.DBText((""&txCompanyNum_isracard.Text).Trim) & "',")
		sSQL.Append("dt_Comments_isracard='" & dbPages.DBText((""&txComments_isracard.Text).Trim) & "',")
		sSQL.Append("dt_CompanyNum_direct='" & dbPages.DBText((""&txCompanyNum_direct.Text).Trim) & "',")
		sSQL.Append("dt_Comments_direct='" & dbPages.DBText((""&txComments_direct.Text).Trim) & "',")
		sSQL.Append("dt_CompanyNum_mastercard='" & dbPages.DBText((""&txCompanyNum_mastercard.Text).Trim) & "',")
		sSQL.Append("dt_Comments_mastercard='" & dbPages.DBText((""&txComments_mastercard.Text).Trim) & "',")
		sSQL.Append("dt_CompanyNum_diners='" & dbPages.DBText((""&txCompanyNum_diners.Text).Trim) & "',")
		sSQL.Append("dt_Comments_diners='" & dbPages.DBText((""&txComments_diners.Text).Trim) & "',")
		sSQL.Append("dt_CompanyNum_americanexp='" & dbPages.DBText((""&txCompanyNum_americanexp.Text).Trim) & "',")
		sSQL.Append("dt_Comments_americanexp='" & dbPages.DBText((""&txComments_americanexp.Text).Trim) & "' ")
		sSQL.Append("Where id=" & terminalID.Value)
		dbPages.ExecSql(sSQL.ToString)
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Terminals - Card Company Details</title>
    <meta http-equiv="Content-Type" content="text/html;charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading{text-decoration:underline;white-space:nowrap;}
		table.formNormal td, table.formNormal th {text-align:center;}
		.btnSave { width:58px; height:52px; }
	</style>
	<script language="javascript" type="text/javascript" src="../js/func_common.js"></script>
</head>
<body>
    <form id="form1" runat="server" class="formNormal">
    <table align="center" style="width:95%;">
	<tr>
		<td id="pageMainHeadingTd">
			<asp:label ID="lblPermissions" runat="server" /><span id="pageMainHeading"> TERMINALS</span><span style="letter-spacing:-1px;"> - <asp:Literal ID="litTerminalName" runat="server" /></span>
		</td>
		<td>
			<table width="80" border="0" cellspacing="0" cellpadding="2" align="right" style="border:1px solid #c0c0c0;">
				<tr><td align="center" id="IsActiveTd"><asp:Literal ID="ltIsActiveTerm" runat="server" /><br /></td></tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
	<custom:Toolbar ID="tlbTop" Width="95%" runat="server" />
	<asp:Panel ID="pnlTRM" runat="server">
		<asp:HiddenField ID="terminalID" runat="server" />
		<asp:HiddenField ID="isChargeMade" runat="server" />
		<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
			<tr>
			<td valign="top">
				<span class="MerchantSubHead">Card Company Details</span>				
				<br />
			</td>
		</tr>
		</table>
		<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
			<tr><td height="5"></td></tr>
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td style="text-decoration: underline;" valign="top">Card<br /></td>
							<td style="text-decoration: underline;" valign="top">Supplier Num.<br /></td>
							<td style="text-decoration: underline;" valign="top" width="65%">Comment<br /></td>
						</tr>
						<tr><td height="5" colspan="3"></td></tr>
						
						<tr>
							<td><img width="23" height="15" align="middle" src="../images/icon_card_visa.gif"> Visa<br/></td>
							<td>								
								<asp:TextBox ID="txCompanyNum_visa" runat="server" Width="100px" MaxLength="50" />
							</td>
							<td>								
								<asp:TextBox ID="txComments_visa" runat="server" Width="350px" MaxLength="50" />
							</td>
						</tr>
						<tr>
							<td><img width="23" height="15" align="middle" src="../images/icon_card_mastercard.gif"> Mastercard<br/></td>
							<td>								
								<asp:TextBox ID="txCompanyNum_mastercard" runat="server" Width="100px" MaxLength="50" />
							</td>
							<td>								
								<asp:TextBox ID="txComments_mastercard" runat="server" Width="350px" MaxLength="50" />
							</td>
						</tr>
						<tr>
							<td><img width="23" height="15" align="middle" src="../images/icon_card_diners.gif"> Diners<br/></td>
							<td>								
								<asp:TextBox ID="txCompanyNum_diners" runat="server" Width="100px" MaxLength="50" />
							</td>
							<td>								
								<asp:TextBox ID="txComments_diners" runat="server" Width="350px" MaxLength="50" />
							</td>
						</tr>
						<tr>
							<td><img width="23" height="15" align="middle" src="../images/icon_card_americanx.gif"> Amex<br/></td>
							<td>								
								<asp:TextBox ID="txCompanyNum_americanexp" runat="server" Width="100px" MaxLength="50" />
							</td>
							<td>								
								<asp:TextBox ID="txComments_americanexp" runat="server" Width="350px" MaxLength="50" />
							</td>
						</tr>
						<tr>
							<td><img width="23" height="15" align="middle" src="../images/icon_card_isracard.gif"> Isracard<br/></td>
							<td>								
								<asp:TextBox ID="txCompanyNum_isracard" runat="server" Width="100px" MaxLength="50" />
							</td>
							<td>								
								<asp:TextBox ID="txComments_isracard" runat="server" Width="350px" MaxLength="50" />
							</td>
						</tr>
						<tr>
							<td><img width="23" height="15" align="middle" src="../images/icon_card_direct.gif"> Direct<br/></td>
							<td>								
								<asp:TextBox ID="txCompanyNum_direct" runat="server" Width="100px" MaxLength="50" />
							</td>
							<td>								
								<asp:TextBox ID="txComments_direct" runat="server" Width="350px" MaxLength="50" />
							</td>
						</tr>
						<tr>
							<td colspan="3"><br /><asp:Button Text="UPDATE" runat="server" OnClick="UpdateDetails" /></td>
						</tr>					
					</table>
				</td>
			</tr>
		</table>
	</asp:Panel>
    </form>
</body>
</html>
