<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<script runat="server">
	Sub HideNotFound(ByVal o As Object, ByVal e As RepeaterItemEventArgs)
		If e.Item.ItemType = ListItemType.Item Then
			litNotFound.Text = String.Empty
			litHeader.Text = "<tr><th class=""light"">ID</th><th class=""light"">Date &amp; Time</th><th class=""light"">Reply</th><th class=""light"">Description</th><th class=""light"">Answer</th></tr>"
		End If
	End Sub

	Protected Sub Page_Load()
		dsLog.ConnectionString = dbPages.DSN
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Failed Transaction - Connection Error - Refund Log</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript" type="text/javascript" src="../js/func_common.js"></script>
</head>
<body>

	<div style="font-size:11px; font-weight:bold; color:#505050; padding-left:6px;">Log Refund attempt</div>
	<form id="Form1" runat="server">
	<asp:SqlDataSource ID="dsLog" runat="server" SelectCommand="SELECT tblLogDebitRefund.*, DescriptionOriginal FROM tblLogDebitRefund
	INNER JOIN tblLog_NoConnection ON ldr_LogNoConnection=LogNoConnection_ID
	LEFT JOIN tblDebitCompanyCode ON ldr_ReplyCode=Code AND lnc_DebitCompany=DebitCompanyID
	WHERE ldr_TransFail=@TransID ORDER BY ID DESC">
		<SelectParameters>
			<asp:QueryStringParameter Name="TransID" QueryStringField="TransID" Type="int32" ConvertEmptyStringToNull="true" DefaultValue="0" />
		</SelectParameters>
	</asp:SqlDataSource>
	<table class="formNormal">
		<asp:Literal ID="litHeader" runat="server" />
		<asp:Repeater DataSourceID="dsLog" OnItemDataBound="HideNotFound" runat="server">
			<ItemTemplate>
				<tr>
					<td><asp:Literal ID="litID" Text='<%# Eval("ID") %>' runat="server" /></td>
					<td><asp:Literal ID="litDate" Text='<%# dbPages.FormatDateTime(Eval("ldr_InsertDate"), , , True, True) %>' runat="server" /></td>
					<td><asp:Literal ID="litCode" Text='<%# Eval("ldr_ReplyCode") %>' runat="server" /></td>
					<td><asp:Literal ID="litText" Text='<%# Eval("DescriptionOriginal") %>' runat="server" /></td>
					<td><asp:Literal Text='<%# Server.HtmlEncode(Eval("ldr_Answer")) %>' runat="server" /></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
		<asp:Literal ID="litNotFound" runat="server"><tr><td colspan="4">No refund attempt found for this transaction.</td></tr></asp:Literal>
	</table>
	</form>
</body>
</html>