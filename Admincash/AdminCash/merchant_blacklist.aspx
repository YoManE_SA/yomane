<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20080629 Tamir
	'
	' Merchant blacklist management
	'
	Sub AddConditionToSQL(ByRef sSQL As String, ByVal sCondition As String)
		sSQL &= IIf(sSQL.IndexOf("WHERE") > 0, IIf(Trim(Request("FilterType")) = "OR", " OR ", " AND "), " WHERE ") & sCondition
	End Sub
	
	Sub SetRowsCountMessage()
		If gvBlacklist.Rows.Count = 15 Then
			lblMsg.Text = "More than 15 matching records found in the blacklist. Only first 15 records are shown. Please refine your search."
		ElseIf gvBlacklist.Rows.Count = 0 Then
			lblMsg.Text = "No matching records found in the blacklist. Try specifying less parameters for search."
		ElseIf gvBlacklist.Rows.Count = 1 Then
			lblMsg.Text = "One matching record found in the blacklist."
		Else
			lblMsg.Text = gvBlacklist.Rows.Count.ToString & " matching records found in the blacklist."
		End If
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		lblMsg.Text = String.Empty
		dsBlacklist.ConnectionString = dbPages.DSN
		dsRecord.ConnectionString = dbPages.DSN
		Dim sSQL As String
		If Page.IsPostBack And Session("RecurringAdminCashSQL") <> String.Empty Then
			sSQL = Session("RecurringAdminCashSQL").ToString
		Else
			sSQL = "SELECT TOP 15 * FROM [Risk].[BlacklistMerchant]"
			If Request("MerchantID") <> "" Then AddConditionToSQL(sSQL, "Merchant_id=" & Request("MerchantID").ToString)
			If Request("MerchantNumber") <> "" Then AddConditionToSQL(sSQL, "Merchant_id IN (SELECT ID FROM tblCompany WHERE CustomerNumber=" & Request("MerchantNumber").ToString & ")")
			If Request("MerchantName") <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(CompanyName) LIKE dbo.GetCleanText('%" & Request("MerchantName").ToString & "%') OR dbo.GetCleanText(CompanyLegalName) LIKE dbo.GetCleanText('%" & Request("MerchantName").ToString & "%'))")
			If Request("LegalNumber") <> "" Then AddConditionToSQL(sSQL, "dbo.GetCleanText(CompanyLegalNumber) LIKE dbo.GetCleanText('%" & Request("LegalNumber").ToString & "%')")
			If Request("IDNumber") <> "" Then AddConditionToSQL(sSQL, "dbo.GetCleanText(IDNumber) LIKE dbo.GetCleanText('%" & Request("IDNumber").ToString & "%')")
			If Request("FirstName") <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(FirstName) LIKE dbo.GetCleanText('%" & Request("FirstName").ToString & "%') OR dbo.GetCleanText(PaymentPayeeName) LIKE dbo.GetCleanText('%" & Request("FirstName").ToString & "%'))")
			If Request("LastName") <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(LastName) LIKE dbo.GetCleanText('%" & Request("LastName").ToString & "%') OR dbo.GetCleanText(PaymentPayeeName) LIKE dbo.GetCleanText('%" & Request("LastName").ToString & "%'))")
			If Request("Phone") <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(Phone) LIKE dbo.GetCleanText('%" & Request("Phone").ToString & "%') OR dbo.GetCleanText(Cellular) LIKE dbo.GetCleanText('%" & Request("Phone").ToString & "%') OR dbo.GetCleanText(CompanyPhone) LIKE dbo.GetCleanText('%" & Request("Phone").ToString & "%') OR dbo.GetCleanText(CompanyFax) LIKE dbo.GetCleanText('%" & Request("Phone").ToString & "%') OR dbo.GetCleanText(MerchantSupportPhoneNum) LIKE dbo.GetCleanText('%" & Request("Phone").ToString & "%'))")
			If Request("Fax") <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(Phone) LIKE dbo.GetCleanText('%" & Request("Fax").ToString & "%') OR dbo.GetCleanText(Cellular) LIKE dbo.GetCleanText('%" & Request("Fax").ToString & "%') OR dbo.GetCleanText(CompanyPhone) LIKE dbo.GetCleanText('%" & Request("Fax").ToString & "%') OR dbo.GetCleanText(CompanyFax) LIKE dbo.GetCleanText('%" & Request("Fax").ToString & "%') OR dbo.GetCleanText(MerchantSupportPhoneNum) LIKE dbo.GetCleanText('%" & Request("Fax").ToString & "%'))")
			If Request("Cellular") <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(Phone) LIKE dbo.GetCleanText('%" & Request("Cellular").ToString & "%') OR dbo.GetCleanText(Cellular) LIKE dbo.GetCleanText('%" & Request("Cellular").ToString & "%') OR dbo.GetCleanText(CompanyPhone) LIKE dbo.GetCleanText('%" & Request("Cellular").ToString & "%') OR dbo.GetCleanText(CompanyFax) LIKE dbo.GetCleanText('%" & Request("Cellular").ToString & "%') OR dbo.GetCleanText(MerchantSupportPhoneNum) LIKE dbo.GetCleanText('%" & Request("Cellular").ToString & "%'))")
			If Request("Mail") <> "" Then AddConditionToSQL(sSQL, "(dbo.GetCleanText(Mail) LIKE dbo.GetCleanText('%" & Request("Mail").ToString & "%') OR dbo.GetCleanText(MerchantSupportEmail) LIKE dbo.GetCleanText('%" & Request("Mail").ToString & "%'))")
			If Request("URL") <> "" Then
				Dim sCondition As String = String.Empty
				For Each sURL As String In Request("URL").Split(" ")
					If sURL <> String.Empty Then
						If sCondition <> String.Empty Then sCondition &= " OR "
						sCondition &= "dbo.GetCleanText('http://'+URL) LIKE dbo.GetCleanText('%" & sURL & "%')"
					End If
				Next
				If sCondition <> String.Empty Then AddConditionToSQL(sSQL, "(" + sCondition + ")")
			End If
			sSQL &= " ORDER BY BlacklistMerchant_id DESC"
			Session("RecurringAdminCashSQL") = sSQL
		End If
		lblSQL.Text = sSQL
		dsBlacklist.SelectCommand = sSQL
		Session("RecurringAdminCashSQL") = sSQL
		dsBlacklist.DataBind()
		SetRowsCountMessage()
		If Request("FilterSource") = "Merchant" Then
			btnAddNew.Visible = False
			gvBlacklist.Columns(gvBlacklist.Columns.Count - 1).Visible = False
			If gvBlacklist.Rows.Count = 0 Then
				lblMsg.Text = "None of this merchant's details appear in the blacklist."
			Else
				lblMsg.Text = "One or more values in this merchant's details appear in the blacklist: "
				If gvBlacklist.Rows.Count = 1 Then
					lblMsg.Text &= " 1 matching record found."
				Else
					lblMsg.Text &= gvBlacklist.Rows.Count.ToString & " matching records found."
				End If
			End If
		End If
	End Sub

	Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim sSQL As String
		dbPages.ExecSql("BlackListAddMerchant 0, '" & Security.Username & "'")
		sSQL = "SELECT * FROM [Risk].[BlacklistMerchant] WHERE BlacklistMerchant_id = (SELECT Max(BlacklistMerchant_id) FROM [Risk].[BlacklistMerchant])"
		'dsBlacklist.SelectCommand = sSQL
		'Session("RecurringAdminCashSQL") = sSQL
		gvBlacklist.DataBind()
		lblMsg.Text = "The record has been successfully added."
		dsRecord.SelectCommand = sSQL
		dvRecord.DataBind()
		dvRecord.Visible = True
		dvRecord.ChangeMode(DetailsViewMode.Edit)
	End Sub

	Sub btnShowAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim sSQL As String
		sSQL = "SELECT TOP 15 * FROM [Risk].[BlacklistMerchant] ORDER BY BlacklistMerchant_id DESC"
		dsBlacklist.SelectCommand = sSQL
		Session("RecurringAdminCashSQL") = sSQL
		gvBlacklist.DataBind()
		SetRowsCountMessage()
		dvRecord.Visible = False
	End Sub

	Sub gvBlacklist_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		dsRecord.FilterExpression = "BlacklistMerchant_id=" & gvBlacklist.SelectedDataKey.Item(0).ToString()
		dvRecord.DataBind()
		dvRecord.ChangeMode(DetailsViewMode.Edit)
		dvRecord.Visible = True
	End Sub

	Sub gvBlacklist_OnRowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs)
		dsBlacklist.SelectCommand = Session("RecurringAdminCashSQL")
		gvBlacklist.DataBind()
		SetRowsCountMessage()
		lblMsg.Text = "The record has been successfully removed. " & lblMsg.Text
		dvRecord.Visible = False
	End Sub

	Sub dvRecord_OnItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdatedEventArgs)
		gvBlacklist.DataBind()
		dvRecord.Visible = False
	End Sub

	Sub dvRecord_OnModeChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		If dvRecord.CurrentMode = DetailsViewMode.ReadOnly Then dvRecord.Visible = False
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Merchant Blacklist</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<style type="text/css" media="screen">
		table.grid td
		{
			text-align:left;
		}
		table.grid th
		{
			text-align:left;
		}
	</style>
</head>
<body>
	<iframe id="ifrDelete" src="common_blank.htm" style="display:none;"></iframe>
	<script language="javascript" type="text/javascript">
		function OpenMerchantSearch(nID)
		{
			var sOpenURL = "merchant_blacklist_search.aspx?BlacklistMerchant_id=" + nID;
			var winBlacklistSearch=window.open(sOpenURL, "fraBlacklistSearch", "top=120,left=50,width="+(screen.width-100)+",height="+(screen.height-240)+",scrollbars=auto,resizable");
		}
		function ConfirmAndDeleteRecord(nID)
		{
			if (!confirm("Do you really want to remove this record from the blacklist?")) return;
			document.getElementById("ifrDelete").contentWindow.location.replace("merchant_blacklist_go.aspx?DeleteRecord="+nID);
		}
	</script>
	<form action="" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Merchant Blacklist</h1>
		<div class="bordered">
			<span style="float:right;padding-bottom:1px;">
				<asp:Button ID="btnAddNew" runat="server" CssClass="buttonWhite buttonWide" Text="Add New Record" OnClick="btnAddNew_Click" />
				<asp:Button ID="btnShowAll" runat="server" CssClass="buttonWhite buttonWide" Text="Show All Records" OnClick="btnShowAll_Click" />
			</span>
			<asp:label ID="lblMsg" runat="server" />
		</div>
		<asp:label ID="lblSQL" runat="server" Visible="false" />
		<asp:GridView ID="gvBlacklist" runat="server" AutoGenerateColumns="False" DataKeyNames="BlacklistMerchant_id" DataSourceID="dsBlacklist" CssClass="grid" SelectedRowStyle-CssClass="rowSelected" Width="100%" OnSelectedIndexChanged="gvBlacklist_OnSelectedIndexChanged" OnRowDeleted="gvBlacklist_OnRowDeleted">
			<Columns>
				<asp:BoundField DataField="BlacklistMerchant_id" HeaderText="ID" InsertVisible="False" ReadOnly="true" Visible="false" />
				<asp:BoundField DataField="Merchant_id" NullDisplayText="---" HeaderText="Company" ControlStyle-CssClass="short" Visible="false" />
				<asp:BoundField DataField="FirstName" NullDisplayText="---" HeaderText="First Name" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="LastName" NullDisplayText="---" HeaderText="Last Name" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="Phone" NullDisplayText="---" HeaderText="Contact Phone" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="Cellular" NullDisplayText="---" HeaderText="Contact Mobile" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="IDNumber" NullDisplayText="---" HeaderText="ID Number" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="CompanyName" NullDisplayText="---" HeaderText="Name" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="CompanyLegalNumber" NullDisplayText="---" HeaderText="Legal Number" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="CompanyPhone" NullDisplayText="---" HeaderText="Phone" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="CompanyFax" NullDisplayText="---" HeaderText="Fax" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="Mail" NullDisplayText="---" HeaderText="Mail" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="URL" NullDisplayText="---" HeaderText="URL" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="MerchantSupportEmail" NullDisplayText="---" HeaderText="Support Mail" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="MerchantSupportPhoneNum" NullDisplayText="---" HeaderText="Support Phone" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="SearchLink" HeaderText="Search" HtmlEncode="false" ReadOnly="true" />
				<asp:BoundField DataField="DeleteButton" HeaderText="Delete" HtmlEncode="false" ReadOnly="true" />
				<asp:CommandField HeaderText="Edit" ButtonType="Button" ItemStyle-CssClass="buttons" ControlStyle-CssClass="buttonWhite" ShowDeleteButton="False" ShowInsertButton="False" ShowEditButton="False" ShowSelectButton="True" HeaderStyle-CssClass="actions" HeaderStyle-HorizontalAlign="Center" SelectText="Edit" />
			</Columns>
			<SelectedRowStyle CssClass="rowSelected" />
		</asp:GridView>
		<asp:SqlDataSource ID="dsBlacklist" runat="server" ProviderName="System.Data.SqlClient" ConnectionString=""
			SelectCommand="SELECT * FROM [Risk].[BlacklistMerchant]" DeleteCommand="DELETE FROM [Risk].[BlacklistMerchant] WHERE BlacklistMerchant_id=@BlacklistMerchant_id">
			<UpdateParameters>
				<asp:Parameter Name="Merchant_id" Type="Int32" />
				<asp:Parameter Name="FirstName" Type="String" />
				<asp:Parameter Name="LastName" Type="String" />
				<asp:Parameter Name="Phone" Type="String" />
				<asp:Parameter Name="Cellular" Type="String" />
				<asp:Parameter Name="IDNumber" Type="String" />
				<asp:Parameter Name="CompanyName" Type="String" />
				<asp:Parameter Name="CompanyLegalName" Type="String" />
				<asp:Parameter Name="CompanyLegalNumber" Type="String" />
				<asp:Parameter Name="CompanyPhone" Type="String" />
				<asp:Parameter Name="CompanyFax" Type="String" />
				<asp:Parameter Name="Mail" Type="String" />
				<asp:Parameter Name="URL" Type="String" />
				<asp:Parameter Name="MerchantSupportEmail" Type="String" />
				<asp:Parameter Name="MerchantSupportPhoneNum" Type="String" />
				<asp:Parameter Name="BlacklistMerchant_id" Type="Int32" />
			</UpdateParameters>
			<DeleteParameters>
				<asp:Parameter Name="BlacklistMerchant_id" Type="Int32" />
			</DeleteParameters>
			<InsertParameters>
				<asp:Parameter Name="Merchant_id" Type="Int32" />
				<asp:Parameter Name="FirstName" Type="String" />
				<asp:Parameter Name="LastName" Type="String" />
				<asp:Parameter Name="Street" Type="String" />
				<asp:Parameter Name="City" Type="String" />
				<asp:Parameter Name="Phone" Type="String" />
				<asp:Parameter Name="Cellular" Type="String" />
				<asp:Parameter Name="IDNumber" Type="String" />
				<asp:Parameter Name="CompanyName" Type="String" />
				<asp:Parameter Name="CompanyLegalName" Type="String" />
				<asp:Parameter Name="CompanyLegalNumber" Type="String" />
				<asp:Parameter Name="CompanyStreet" Type="String" />
				<asp:Parameter Name="CompanyCity" Type="String" />
				<asp:Parameter Name="CompanyPhone" Type="String" />
				<asp:Parameter Name="CompanyFax" Type="String" />
				<asp:Parameter Name="Mail" Type="String" />
				<asp:Parameter Name="URL" Type="String" />
				<asp:Parameter Name="MerchantSupportEmail" Type="String" />
				<asp:Parameter Name="MerchantSupportPhoneNum" Type="String" />
				<asp:Parameter Name="IpOnReg" Type="String" />
				<asp:Parameter Name="PaymentPayeeName" Type="String" />
				<asp:Parameter Name="PaymentBank" Type="String" />
				<asp:Parameter Name="PaymentBranch" Type="String" />
				<asp:Parameter Name="PaymentAccount" Type="String" />
			</InsertParameters>
		</asp:SqlDataSource>
		<br />
		<asp:DetailsView ID="dvRecord" DataSourceID="dsRecord" runat="server" AutoGenerateRows="False" DataKeyNames="BlacklistMerchant_id" CssClass="details" Visible="false" OnItemUpdated="dvRecord_OnItemUpdated" OnModeChanged="dvRecord_OnModeChanged">
			<Fields>
				<asp:CommandField ButtonType="Button" ControlStyle-CssClass="buttonWhite" ShowCancelButton="true" ShowEditButton="true" UpdateText="Save" EditText="Edit" ItemStyle-HorizontalAlign="right" />
				<asp:BoundField DataField="BlacklistMerchant_id" HeaderText="ID" InsertVisible="False" Visible="false" ReadOnly="True" />
				<asp:BoundField DataField="InsertDate" HeaderText="InsertDate" ReadOnly="True" />
				<asp:BoundField DataField="InsertedBy" HeaderText="InsertedBy" ReadOnly="True" />
				<asp:BoundField DataField="Merchant_id" HeaderText="Company" Visible="false" />
				<asp:BoundField DataField="FirstName" HeaderText="First Name" />
				<asp:BoundField DataField="LastName" HeaderText="Last Name" />
				<asp:BoundField DataField="Street" HeaderText="Contact Street" />
				<asp:BoundField DataField="City" HeaderText="Contact City" />
				<asp:BoundField DataField="Phone" HeaderText="Contact Phone" />
				<asp:BoundField DataField="Cellular" HeaderText="Mobile" />
				<asp:BoundField DataField="IDNumber" HeaderText="ID Number" />
				<asp:BoundField DataField="CompanyName" HeaderText="Name" />
				<asp:BoundField DataField="CompanyLegalName" HeaderText="Legal Name" />
				<asp:BoundField DataField="CompanyLegalNumber" HeaderText="Legal Number" />
				<asp:BoundField DataField="CompanyStreet" HeaderText="Street" />
				<asp:BoundField DataField="CompanyCity" HeaderText="City" />
				<asp:BoundField DataField="CompanyPhone" HeaderText="Phone" />
				<asp:BoundField DataField="CompanyFax" HeaderText="Fax" />
				<asp:BoundField DataField="Mail" HeaderText="Mail" />
				<asp:BoundField DataField="URL" HeaderText="URL" />
				<asp:BoundField DataField="MerchantSupportEmail" HeaderText="Support Email" />
				<asp:BoundField DataField="MerchantSupportPhoneNum" HeaderText="Support Phone" />
				<asp:BoundField DataField="IpOnReg" HeaderText="IpOnReg" SortExpression="IP address during signup" />
				<asp:BoundField DataField="PaymentPayeeName" HeaderText="Beneficiary" />
				<asp:BoundField DataField="PaymentBank" HeaderText="Bank" />
				<asp:BoundField DataField="PaymentBranch" HeaderText="Branch" />
				<asp:BoundField DataField="PaymentAccount" HeaderText="Account" />
				<asp:CommandField ButtonType="Button" ControlStyle-CssClass="buttonWhite" ShowCancelButton="true" ShowEditButton="true" UpdateText="Save" EditText="Edit" ItemStyle-HorizontalAlign="right" />
			</Fields>
		</asp:DetailsView>
		<asp:SqlDataSource ID="dsRecord" runat="server" ConnectionString=""
			ProviderName="System.Data.SqlClient"
			SelectCommand="SELECT * FROM [Risk].[BlacklistMerchant]"
			UpdateCommand="UPDATE [Risk].[BlacklistMerchant] SET Merchant_id=IsNull(@CompanyID, 0),
				FirstName=IsNull(@FirstName, ''), LastName=IsNull(@LastName, ''),
				Street=IsNull(@Street, ''), City=IsNull(@City, ''), Phone=IsNull(@Phone, ''),
				Cellular=IsNull(@Cellular, ''), IDNumber=IsNull(@IDNumber, ''),
				CompanyName=IsNull(@CompanyName, ''), CompanyLegalName=IsNull(@CompanyLegalName, ''),
				CompanyLegalNumber=IsNull(@CompanyLegalNumber, ''), CompanyStreet=IsNull(@CompanyStreet, ''),
				CompanyCity=IsNull(@CompanyCity, ''), CompanyPhone=IsNull(@CompanyPhone, ''),
				CompanyFax=IsNull(@CompanyFax, ''), Mail=IsNull(@Mail, ''),
				URL=IsNull(@URL, ''), MerchantSupportEmail=IsNull(@MerchantSupportEmail, ''),
				MerchantSupportPhoneNum=IsNull(@MerchantSupportPhoneNum, ''),
				IpOnReg=IsNull(@IpOnReg, ''), PaymentPayeeName=IsNull(@PaymentPayeeName, ''),
				PaymentBank=IsNull(@PaymentBank, 0), PaymentBranch=IsNull(@PaymentBranch, ''),
				PaymentAccount=IsNull(@PaymentAccount, '') WHERE BlacklistMerchant_id=@BlacklistMerchant_id">
			<DeleteParameters>
				<asp:Parameter Name="BlacklistMerchant_id" Type="Int32" />
			</DeleteParameters>
			<UpdateParameters>
				<asp:Parameter Name="Merchant_id" Type="Int32"/>
				<asp:Parameter Name="FirstName" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="LastName" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="Street" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="City" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="Phone" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="Cellular" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="IDNumber" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="CompanyName" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="CompanyLegalName" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="CompanyLegalNumber" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="CompanyStreet" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="CompanyCity" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="CompanyPhone" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="CompanyFax" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="Mail" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="URL" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="MerchantSupportEmail" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="MerchantSupportPhoneNum" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="IpOnReg" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="PaymentPayeeName" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="PaymentBank" Type="String" />
				<asp:Parameter Name="PaymentBranch" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="PaymentAccount" Type="String" ConvertEmptyStringToNull="false" />
				<asp:Parameter Name="BlacklistMerchant_id" Type="Int32" />
			</UpdateParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>