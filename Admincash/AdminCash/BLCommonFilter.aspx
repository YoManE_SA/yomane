<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>

<script runat="server">
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)

		litBL_Type.Text = "<select style=""width:130px;"" name=""BL_Type"" class=""grayBG"">"
		litBL_Type.Text &= "<option value=""""></option>"
		Dim BLCommonType() As String = Nothing
		NetpayConst.GetConstArray(47, 0, Nothing, BLCommonType)
		'BLCommonType(0) = "[All]"
		If Not IsPostBack Then
			For i As Integer = 1 To BLCommonType.Length - 1
				litBL_Type.Text &= "<option value=""" & i & """>" & BLCommonType(i) & "</option>"
			Next
		End If
		litBL_Type.Text &= "</select>"
	End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
	<form runat="server" action="BLCommon.aspx" method="get" target="frmBody" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />
	<input type="hidden" id="BL_CompanyID" name="BL_CompanyID" value="" />
	<h1><asp:label ID="lblPermissions" runat="server" /> Common Blacklist<span> - Filter</span></h1>

	<table class="filterNormal">
		<tr><td>Insert a new record - <a target="frmBody" href="BLCommon.aspx?New=1">click here</a><br /></td></tr>	
		<tr><td>Show Usage Risk Report - <a target="frmBody" href="Reports_RiskMultiUse.aspx">click here</a><br /></td></tr>
	</table>
	<hr class="filter" />
	<table class="filterNormal">
		<tr><td colspan="2"><Uc1:FilterMerchants ID="FilterMerchants1" ClientField="BL_CompanyID" runat="server" /></td></tr>
		<tr><td height="12"></td></tr>
	</table>

	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<th colspan="2" class="filter indent">Block Type</th>
		</tr>
		<tr>
			<td colspan="2">
				<input type="radio" name="BlockType" class="option" value="1" />Merchant
				<input type="radio" name="BlockType" class="option" value="0" />Global
				<input type="radio" name="BlockType" class="option" value="" checked="checked" />Both
			</td>
		</tr>
		<tr><td height="12"></td></tr>
		<tr>
			<th colspan="2" class="filter indent">Search Details</th>
		</tr>
		<tr>
			<td>Type</td>
			<td><asp:Literal ID="litBL_Type" runat="server" /></td>
		</tr>
		<tr>
			<td>Value</td>
			<td><input Name="BL_Value"></td>
		</tr>
		<tr><td height="12"></td></tr>
		<tr><th colspan="2" class="filter indent">Paging</th></tr>
		<tr>
			<td>
				<select name="PageSize">
					<option value="10">10 rows</option>
					<option value="25" selected="selected">25 rows</option>
					<option value="50">50 rows</option>
					<option value="100">100 rows</option>
				</select>
			</td>
			<td align="right" nowrap="nowrap">
				<input type="submit" value=" SEARCH " /><br />
			</td>
		</tr>
	</table>
	</form>
</asp:Content>