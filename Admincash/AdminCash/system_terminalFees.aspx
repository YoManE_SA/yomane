<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<%@ Import Namespace="System.Data.SqlClient" %>


<script runat="server">
	Dim iReader As SqlDataReader
	Dim ChargeMade As Boolean = False
	Dim bIsChargeMade As Boolean = False
	Dim sTerminalNumber As String = ""
	Dim sDebitCompany As String = ""
	Dim DCF_TerminalNumber As String = ""
	Dim DCF_DebitCompanyID As String = ""
	Dim sDiv As String = ""
	Dim DCF_ID As Integer
	Dim nTerminalID As Integer

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		SqlDataSource1.ConnectionString = dbPages.DSN
		SqlDataSource1.SelectCommand = "SELECT 0 ID, '[All]' AS 'Name' UNION SELECT PaymentMethod_id AS 'ID', Name FROM List.PaymentMethod WHERE PaymentMethod_id > " & ePaymentMethod.PMD_MAXINTERNAL & " ORDER BY Name"
		SqlDataSource2.ConnectionString = dbPages.DSN
		SqlDataSource2.SelectCommand = "Select 255 As CUR_ID, 'All' As CUR_Symbol, -1 As Fsort UNION Select 0 As CUR_ID, '" & dbPages.GetCurText(eCurrencies.GC_NIS) & "' As CUR_Symbol, 0 As Fsort  UNION Select CUR_ID, CUR_Symbol, 1 As Fsort From tblSystemCurrencies Where CUR_ID > 0 Order By Fsort, CUR_ID"
		SqlDataSource3.ConnectionString = dbPages.DSN
		SqlDataSource3.SelectCommand = "Select 255 As CUR_ID, '[Native]' As CUR_Symbol, -1 As Fsort UNION Select 0 As CUR_ID, '" & dbPages.GetCurText(eCurrencies.GC_NIS) & "' As CUR_Symbol, 0 As Fsort  UNION Select CUR_ID, CUR_Symbol, 1 As Fsort From tblSystemCurrencies Where CUR_ID > 0 Order By Fsort, CUR_ID"
		If Not Page.IsPostBack Then ddlDCF_CurrencyID.SelectedValue = "0"
		Dim sQueryWhere As String
		If Not Page.IsPostBack Then
			nTerminalID = dbPages.TestVar(Request("terminalID"), 1, 0, 0)
			If nTerminalID > 0 Then
				litTerminalName.Text = dbPages.ExecScalar("SELECT dc_Name+' | '+TerminalNumber+' | '+dt_Name FROM tblDebitCompany" & _
				" INNER JOIN tblDebitTerminals ON DebitCompany_ID=DebitCompany WHERE ID=" & nTerminalID)
				tlbTop.AddItem("Terminal Details", "system_terminalTermDetail.aspx?terminalID=" & nTerminalID)
				tlbTop.AddItem("Card Company Details", "system_terminalCardDetail.aspx?terminalID=" & nTerminalID)
				tlbTop.AddItem("Fees & Payouts", "system_terminalFees.aspx?terminalID=" & nTerminalID, True)
				tlbTop.AddItem("Notifications", "system_terminalRules.aspx?TerminalID=" & nTerminalID)
				tlbTop.AddItem("Miscellaneous", "system_terminalMisc.aspx?terminalID=" & nTerminalID)
				sQueryWhere = "tblDebitTerminals.id=" & nTerminalID.ToString
				terminalID.Value = nTerminalID
			Else
				sQueryWhere = "tblDebitTerminals.terminalNumber='" & Request("terminalNumber") & "'"
				litTerminalName.Text = "All Debit Companies"
			End If
			Dim sSQL As New StringBuilder(String.Empty)
			sSQL.Append("SELECT tblDebitTerminals.*, dbo.GetDecrypted256(accountPassword256) accountPassword, dbo.GetDecrypted256(accountPassword3D256) accountPassword3D")
			sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransPass WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransAnswer")
			sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransFail WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransFail")
			sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransApproval WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransApprovalOnly")
			sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransRemoved WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransDel")
			sSQL.Append(" FROM tblDebitTerminals WHERE " & sQueryWhere)
			iReader = dbPages.ExecReader(sSQL.ToString)
			If iReader.HasRows Then
				While iReader.Read
					If iReader("isActive") Then
						ltIsActiveTerm.Text = "<span><li type=square style=color:#66cc66;> <span class=txt11>Active</span></li></span>"
					Else
						ltIsActiveTerm.Text = "<span><li type=square style=color:#ff6666;> <span class=txt11>Blocked</span></li></span>"
					End If
					sTerminalNumber = Trim(iReader("terminalNumber"))
					sDebitCompany = Trim(iReader("DebitCompany"))
					DCF_TerminalNumber = Trim(iReader("terminalNumber"))
					hidDCF_TerminalNumber.Value = DCF_TerminalNumber
					DCF_DebitCompanyID = Trim(iReader("DebitCompany"))
					hidDCF_DebitCompanyID.Value = DCF_DebitCompanyID
				End While
			Else
				pnlTRM.Visible = False
			End If
			iReader.Close()
		Else
			DCF_DebitCompanyID = hidDCF_DebitCompanyID.Value
			DCF_TerminalNumber = hidDCF_TerminalNumber.Value
			nTerminalID = terminalID.Value
		End If
		dsFees.ConnectionString = dbPages.DSN
		dsFees.SelectCommand = "Select tblDebitCompanyFees.*, pm.Name AS 'pm_Name', DCF_PaymentMethod From tblDebitCompanyFees " & _
			" Left Join List.PaymentMethod AS pm ON DCF_PaymentMethod= pm.PaymentMethod_id" & _
			" Where DCF_DebitCompanyID=" & DCF_DebitCompanyID & " And DCF_TerminalNumber='" & Replace(DCF_TerminalNumber, "'", "''") & "' Order By DCF_ID"
		If Not String.IsNullOrEmpty(DCF_TerminalNumber) Then dvbFees.Text &= "<br />** If empty, data for updating is taken from the debit company configuration page"
	End Sub
	
	Private Sub AddNew(ByVal sender As Object, ByVal e As System.EventArgs)
		'Response.Write("Insert Into tblDebitCompanyFees(DCF_DebitCompanyID, DCF_CurrencyID, DCF_PaymentMethod, DCF_TerminalNumber)Values(" & DCF_DebitCompanyID & ", " & ddlDCF_CurrencyID.SelectedValue & ", " & ddlDCF_PaymentMethod.SelectedValue & ",'" & DCF_TerminalNumber & "')")
		dbPages.ExecSql("Insert Into tblDebitCompanyFees(DCF_DebitCompanyID, DCF_CurrencyID, DCF_PaymentMethod, DCF_TerminalNumber)Values(" & DCF_DebitCompanyID & ", " & ddlDCF_CurrencyID.SelectedValue & ", " & ddlDCF_PaymentMethod.SelectedValue & ",'" & DCF_TerminalNumber & "')")
		Response.Redirect("system_terminalFees.aspx?terminalID=" & nTerminalID)
	End Sub
	
	Sub SetDefaultValues(Optional o As Object=Nothing, optional e as RepeaterItemEventArgs=Nothing)
		Dim ddlCurrency As DropDownList=e.Item.FindControl("ddlFixedCurrency")
		If Not IsNothing(ddlCurrency) Then
			ddlCurrency.SelectedValue = CType(e.Item.FindControl("hidFixedCurrency"), HiddenField).Value
		End If
		ddlCurrency = e.Item.FindControl("ddlCHBCurrency")
		ddlCurrency.SelectedValue=ctype(e.Item.FindControl("hidCHBCurrency"), HiddenField).value
	End Sub
	
	Sub ProcessRecord(Optional o As Object=Nothing, optional e as CommandEventArgs=Nothing)
		Dim riItem As RepeaterItem=CType(o, Button).NamingContainer 
		Dim nID As Integer=ctype(riItem.FindControl("hidID"), HiddenField ).Value 
		Dim sSQL As String = ""
		Dim sSaveError As String=""
		Select Case e.CommandName.ToLower 
			Case "delete"
				sSQL = "Delete From tblDebitCompanyFees Where DCF_ID=" & nID
				dbPages.ExecSql(sSQL)
			Case "update"				
				Dim sTransNumbers As String=(""&ctype(riItem.FindControl("txPayTransDays"), TextBox).Text).Trim
				Dim DCF_PayTransDays() As String = sTransNumbers.Split(",")
				Dim sINNumbers As String = ctype(riItem.FindControl("txPayINDays"), TextBox).Text
				Dim DCF_PayINDays() As String = sINNumbers.Split(",")
				Dim nLastNumber As Integer = 0
				Dim DCF_PayINDaysStr As String = ""				
				If Len(""&sTransNumbers) > 0 then 
					For i As Integer = 0 To Ubound(DCF_PayTransDays)						
						If DCF_PayTransDays(i) = "" Or Not IsNumeric(DCF_PayTransDays(i)) Then
							sSaveError = "Transaction Days is in invalid format"
							Exit For
						ElseIf (nLastNumber > CLng(DCF_PayTransDays(i))) Or (CLng(DCF_PayTransDays(i)) > 31) Then
							sSaveError = "Transaction Days is in invalid format"
							Exit For
						Else
							nLastNumber = CLng(DCF_PayTransDays(i))
						End If					
					Next
				End If
				If sSaveError = "" Then
					If Len(""&sINNumbers) > 0 then
						For i As Integer = 0 To Ubound(DCF_PayINDays)
							If DCF_PayINDays(i) = "" Or Not IsNumeric(DCF_PayINDays(i)) Then
								sSaveError = "PayIn Days is in invalid format"
								Exit For
							ElseIf (CLng(DCF_PayINDays(i)) > 31) Then 
								sSaveError = "PayIn Days is in invalid format"
								Exit For
							End If
							DCF_PayINDaysStr = DCF_PayINDaysStr & DCF_PayINDays(i) & ","
						Next
					End If
					If sSaveError = "" And (UBound(DCF_PayTransDays) <> UBound(DCF_PayINDays)) Then sSaveError = "Lists doesn't match"
					If Len(DCF_PayINDaysStr) > 0 Then DCF_PayINDaysStr = Left(DCF_PayINDaysStr, Len(DCF_PayINDaysStr) - 1)
				End If
				If sSaveError <> "" Then
					ltSaveError.Text = "<span style=""color:red;padding:20px;"">" & sSaveError & "</span>"
				Else
					Dim xCount As Integer
                    Dim MaxPrec As String = ctype(riItem.FindControl("txMaxPrecFee"), TextBox).Text
					sSQL =	"Update tblDebitCompanyFees Set" & _
							" DCF_PercentFee=" & dbPages.TestVar(ctype(riItem.FindControl("txPercentFee"), TextBox).Text, 0d, -1d, 0d) & _
							",DCF_FixedFee=" & dbPages.TestVar(ctype(riItem.FindControl("txFixedFee"), TextBox).Text, 0d, -1d, 0d) & _
			                ",DCF_MinPrecFee=" & dbPages.TestVar(ctype(riItem.FindControl("txMinPrecFee"), TextBox).Text, 0d, -1d, 0d) & _
			                ",DCF_MaxPrecFee=" & IIF(MaxPrec <> "", dbPages.TestVar(MaxPrec, 0d, -1d, 0d), "null") & _
							",DCF_CBFixedFee=" & dbPages.TestVar(ctype(riItem.FindControl("txCBFixedFee"), TextBox).Text, 0d, -1d, 0d) & _
							",DCF_ClarificationFee=" & dbPages.TestVar(ctype(riItem.FindControl("txClarificationFee"), TextBox).Text, 0d, -1d, 0d) & _
							",DCF_RefundFixedFee=" & dbPages.TestVar(ctype(riItem.FindControl("txRefundFixedFee"), TextBox).Text, 0d, -1d, 0d) & _
							",DCF_ApproveFixedFee=" & dbPages.TestVar(ctype(riItem.FindControl("txApproveFixedFee"), TextBox).Text, 0d, -1d, 0d) & _
							",DCF_FailFixedFee=" & dbPages.TestVar(ctype(riItem.FindControl("txFailFixedFee"), TextBox).Text, 0d, -1d, 0d) & _							
							",DCF_FixedCurrency=" & dbPages.TestVar(ctype(riItem.FindControl("ddlFixedCurrency"), DropDownList).SelectedValue, 0, -1, 0) & _
							",DCF_CHBCurrency=" & dbPages.TestVar(ctype(riItem.FindControl("ddlCHBCurrency"), DropDownList).SelectedValue, 0, -1, 0) & _
							",DCF_PayTransDays='" & dbPages.TestNumericList(ctype(riItem.FindControl("txPayTransDays"), TextBox).Text,xCount) & "'" & _
							",DCF_PayINDays='" & DCF_PayINDaysStr & "'" & _
							" Where DCF_ID=" & nID
					dbPages.ExecSql(sSQL)
				End If
		End Select
		If sSaveError = "" then Response.Redirect("system_terminalFees.aspx?terminalID=" & nTerminalID)
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Terminals - Fees & Payouts</title>
    <meta http-equiv="Content-Type" content="text/html;charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading{text-decoration:underline;white-space:nowrap;}
		table.formNormal td, table.formNormal th {text-align:center;}
		.btnSave { width:58px; height:52px; }
	</style>
	<script language="javascript" type="text/javascript" src="../js/func_common.js"></script>
</head>
<body>
    <form id="form1" runat="server" class="formNormal">
    <table align="center" style="width:95%;">
	<tr>
		<td id="pageMainHeadingTd">
			<asp:label ID="lblPermissions" runat="server" /><span id="pageMainHeading"> TERMINALS</span><span style="letter-spacing:-1px;"> - <asp:Literal ID="litTerminalName" runat="server" /></span>
		</td>
		<td>
			<table width="80" border="0" cellspacing="0" cellpadding="2" align="right" style="border:1px solid #c0c0c0;">
				<tr><td align="center" id="IsActiveTd"><asp:Literal ID="ltIsActiveTerm" runat="server" /><br /></td></tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
	<custom:Toolbar ID="tlbTop" Width="95%" runat="server" />
	<asp:Panel ID="pnlTRM" runat="server">
		<asp:HiddenField ID="terminalID" runat="server" />
		<asp:HiddenField ID="isChargeMade" runat="server" />
		<asp:HiddenField ID="hidDCF_ID" runat="server" Value="-1" />
		<asp:HiddenField ID="hidDCF_DebitCompanyID" runat="server" />
		<asp:HiddenField ID="hidDCF_TerminalNumber" runat="server" />		
		<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
		<tr>
			<td valign="top">
				<span class="MerchantSubHead">Fees & Payouts</span>&nbsp;
				<AC:DivBox ID="dvbFees" Width="500px" runat="server">
					The data below is used to update incoming transactions with the following data:<br />
					1. The fee taken for the charge by the debit company<br />
					2. The date when debit company will pay us the amount
  				</AC:DivBox>
			</td>
		</tr>
		<tr><td height="5"></td></tr>
		<tr>
			<td style="width:45%">
				<table border="0" cellpadding="1" cellspacing="0">
				<tr>
					<td>
						<asp:DropDownList ID="ddlDCF_PaymentMethod" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="ID" />
						<asp:SqlDataSource ID="SqlDataSource1" runat="server" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient" />
					</td>
					<td>
						<asp:DropDownList ID="ddlDCF_CurrencyID" runat="server" DataSourceID="SqlDataSource2" DataTextField="CUR_Symbol" DataValueField="CUR_ID" />
						<asp:SqlDataSource ID="SqlDataSource2" runat="server" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient" />
					</td>
					<td>
						<asp:Button ID="btnAdd" runat="server" Text="ADD NEW" OnClick="AddNew" />
					</td>
				</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td><asp:Literal ID="ltSaveError" runat="server" /></td>
		</tr>
		<tr><td style="height:15px"></td></tr>
		<tr><td style="border-top:solid 1px;"><br /></td></tr>
		<asp:SqlDataSource ID="dsFees" runat="server" />
		<asp:SqlDataSource ID="SqlDataSource3" runat="server" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient" />
		<asp:Repeater ID="rptFees" runat="server" DataSourceID="dsFees" OnItemDataBound="SetDefaultValues">
			<ItemTemplate>
			<asp:HiddenField ID="hidID" runat="server" Value='<%# Eval("DCF_ID")%>' />
			<asp:HiddenField ID="hidCompany" runat="server" Value='<%#Eval("DCF_DebitCompanyID")%>' />
			<asp:HiddenField ID="hidCurrency" runat="server" Value='<%#Eval("DCF_CurrencyID")%>' />
			<asp:HiddenField ID="hidPaymentMethod" runat="server" Value='<%#Eval("DCF_PaymentMethod")%>' />			
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="8" style="padding-bottom:10px;">
							<img align="middle" src="/NPCommon/ImgPaymentMethod/23X12/<%#Eval("DCF_PaymentMethod")%>.gif">
							<asp:Literal Text='<%# IIf(Eval("DCF_PaymentMethod") > 0, Eval("pm_Name"), "[All]") & ", CURRENCY: " & IIf(Eval("DCF_CurrencyID")<>255,dbPages.GetCurText(Eval("DCF_CurrencyID")),"[All]") %>' runat="server" />
						</td>
					</tr>
					<tr>
						<td style="padding-left:26px;">
							<table border="0">
							<tr>
								<td width="80" rowspan="2">Fees</td>
								<td style="text-decoration: underline;">Currency<br /></td>
								<td style="text-decoration: underline;">Trans<br /></td>
			                    <td style="text-decoration: underline;">Inter (min)<br /></td>
			                    <td style="text-decoration: underline;">Sale (max)<br /></td>
								<td style="text-decoration: underline;">Clearing<br /></td>
								<td style="text-decoration: underline;" nowrap>Pre-Auth<br /></td>
								<td style="text-decoration: underline;">Refund<br /></td>
								<td style="text-decoration: underline;" nowrap>Copy R.<br /></td>
								<td style="text-decoration: underline;">Fail<br /></td>
								<td style="text-decoration: underline;">CHB<br /></td>
							</tr>
							<tr>
								<td>
									<asp:DropDownList ID="ddlFixedCurrency" runat="server" DataSourceID="SqlDataSource3" DataTextField="CUR_Symbol" DataValueField="CUR_ID" />
									<asp:HiddenField ID="hidFixedCurrency" runat="server" Value='<%# Eval("DCF_FixedCurrency") %>' />
								</td>
								<td>
									<asp:TextBox ID="txFixedFee" runat="server" Text='<%# FormatNumber(Eval("DCF_FixedFee"),2,True)%>' size="4" />
								</td>
								<td>
									<asp:TextBox ID="txMinPrecFee" runat="server" Text='<%# FormatNumber(Eval("DCF_MinPrecFee"),2,True)%>' size="4" />%
								</td>
								<td>
									<asp:TextBox ID="txMaxPrecFee" runat="server" Text='<%# Eval("DCF_MaxPrecFee")%>' size="4" />%
								</td>
								<td>
									<asp:TextBox ID="txPercentFee" runat="server" Text='<%# FormatNumber(Eval("DCF_PercentFee"), 2, True)%>' size="4" />%
								</td>
								<td>
									<asp:TextBox ID="txApproveFixedFee" runat="server" Text='<%# FormatNumber(Eval("DCF_ApproveFixedFee"), 2, True)%>' size="4" />
								</td>
								<td>
									<asp:TextBox ID="txRefundFixedFee" runat="server" Text='<%# FormatNumber(Eval("DCF_RefundFixedFee"), 2, True)%>' size="4" />
								</td>
								<td>
									<asp:TextBox ID="txClarificationFee" runat="server" Text='<%# FormatNumber(Eval("DCF_ClarificationFee"), 2, True)%>' size="4" />
								</td>
								<td>
									<asp:TextBox ID="txFailFixedFee" runat="server" Text='<%# FormatNumber(Eval("DCF_FailFixedFee"), 2, True)%>' size="4" />
								</td>
								<td>
									<asp:DropDownList ID="ddlCHBCurrency" runat="server" DataSourceID="SqlDataSource3" DataTextField="CUR_Symbol" DataValueField="CUR_ID" />
									<asp:HiddenField ID="hidCHBCurrency" runat="server" Value='<%# Eval("DCF_CHBCurrency") %>' />
								</td>
								<td>
									<asp:TextBox ID="txCBFixedFee" runat="server" Text='<%# FormatNumber(Eval("DCF_CBFixedFee"), 2, True)%>' size="4" />
								</td>
							</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="padding-left:26px;">
							<table border="0">
							<tr>
								<td width="80" rowspan="2">Payouts</td>
								<td style="text-decoration: underline;" align="left">Transaction Days<br /></td>
								<td style="text-decoration: underline;" align="left">PayIn Days<br /></td>
							</tr>
							<tr>
								<td>
									<asp:TextBox ID="txPayTransDays" runat="server" Text='<%# Eval("DCF_PayTransDays")%>' />
									<AC:DivBox ID="dvbPayTransDays" runat="server">
										Enter transaction dates separated with a comma.
										<br /><br />
										<b>Example:</b> 1,10,20<br />split the month into 3 parts 1-10, 11-20, 21-31
									</AC:DivBox>
								</td>
								<td>
									<asp:TextBox ID="txPayINDays" runat="server" Text='<%# Eval("DCF_PayINDays")%>' />
									<AC:DivBox ID="dvbPayINDays" runat="server">
										Enter pay-in dates separated with a comma, this field complement the dates and must use with the same month parts.
										<br /><br />
										<b>Example:</b><br /> Transaction Days: 1,10,20, PayIn Days: 15,25,5.<br />
										means transactions from 1-10 will be payed on the 15th,
										transactions from 11-20 will be payed on the 25th,
										transactions from 21-31 will be payed on the 5th.
									</AC:DivBox>
								</td>
								<td width="40"></td>
								<td>
									<asp:Button ID="btnUpdate" Text="UPDATE" runat="server" OnCommand="ProcessRecord" CommandName="UPDATE" />
									<asp:Button ID="btnDelete" Text="DELETE" style="color:Maroon;" runat="server" OnCommand="ProcessRecord" CommandName="DELETE" OnClientClick="javascript:return confirm('Sure to delete?');" />
								</td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</ItemTemplate>
		</asp:Repeater>	
		</table>
		
	</asp:Panel>
    </form>
</body>
</html>
