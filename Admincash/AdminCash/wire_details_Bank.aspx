<%@ Page Language="VB" EnableViewState="true" %>
<%@ Register Src="BankAccount.ascx" TagPrefix="UC1" TagName="MerchantBankAccount" %>
<%@ Register Src="BankAccountLocal.ascx" TagPrefix="UC1" TagName="MerchantBankAccountLocal" %>
<%@ Import namespace="System.Data.SqlClient" %>
<script runat="server">
	Private nWire, nMerchant, nAffiliate, nCurrency As Integer, sMerchant, sCurrency As String

	Sub Page_Init()
		nWire = dbPages.TestVar(Request("ID"), 1, 0, 0)
		If nWire < 1 Then Throw New Exception("Invalid Wire specified: " & Request("ID").ToString)
        Dim iReader As System.Data.IDataReader = dbPages.ExecReader("Select * FROM tblWireMoney WHERE WireMoney_id=" & nWire)
        If iReader.Read() Then 
		    nMerchant = dbPages.TestVar(iReader("Company_id"), 0, -1, 0)
            nAffiliate = dbPages.TestVar(iReader("AffiliateID"), 0, -1, 0)
            nCurrency = IIF(iReader("WireProcessingCurrency") Is Nothing, iReader("WireCurrency"), iReader("WireProcessingCurrency"))
        End If
        iReader.Close()
		sCurrency = dbPages.GetCurISO(nCurrency)
        If nMerchant > 0 Then sMerchant = dbPages.ExecScalar("SELECT CompanyName FROM tblCompany WHERE ID=" & nMerchant)
        If nAffiliate > 0 Then sMerchant = dbPages.ExecScalar("SELECT name FROM tblAffiliates WHERE affiliates_id=" & nAffiliate)
	End Sub

	Protected Sub Page_Load()
		If Not Page.IsPostBack Then
			SelectCurrency()
            Dim nText As String = IIf(nMerchant > 0, " merchant's ", " affiliate's ")
			Page.Title &= " Wire " & nWire & ", " & nMerchant & nAffiliate & " """ & sMerchant & """"
			rblSave.Items(1).Text &= nText & sCurrency & " account"
			rblSave.Items(2).Text &= nText & sCurrency & " and default accounts"
		End If
	End Sub

	Sub ProcessData(Optional ByVal o As Object = Nothing, Optional ByVal e As CommandEventArgs = Nothing)
		lblError.Text = String.Empty
		lblNoError.Text = String.Empty
		mba1.Currency = nCurrency
		if nMerchant > 0 Then mba1.Merchant = nMerchant Else mba1.Affiliate = nAffiliate
		mba1.IsCorrespondent = False
		mba2.Currency = nCurrency
		if nMerchant > 0 Then mba2.Merchant = nMerchant Else mba2.Affiliate = nAffiliate
		mba2.IsCorrespondent = True
		If nCurrency = 0 Then
			if nMerchant > 0 Then mbaL.Merchant = nMerchant Else mbaL.Affiliate = nAffiliate
			mbaL.SaveToWire(nWire)
			lblNoError.Text &= "Local bank account has been saved to Wire " & nWire & ".<br />"
			If Not String.IsNullOrEmpty(rblSave.SelectedValue) Then
				mbaL.SaveToMerchant()
				lblNoError.Text &= "Local bank account has been saved to Merchant " & nMerchant & ".<br />"
			End If
		End If
		If String.IsNullOrEmpty(mba1.AccountName) Then
			lblError.Text &= "Primary bank account (" & sCurrency & ") cannot be saved: missing Account Name!<br />"
		Else
			mba1.SaveToWire(nWire)
			lblNoError.Text &= "Primary bank account (" & sCurrency & ") has been saved to wire " & nWire & ".<br />"
			If Not String.IsNullOrEmpty(rblSave.SelectedValue) Then
				mba1.SaveToMerchant()
				lblNoError.Text &= "Primary bank account has been saved to merchant " & nMerchant & " (" & sCurrency & ").<br />"
				If rblSave.SelectedValue.ToUpper = "DEFAULT" Then
					mba1.SaveToDefault()
					lblNoError.Text &= "Primary bank account has been saved to merchant " & nMerchant & " (DEFAULT).<br />"
				End If
			End If
			mba1.LoadFromWire(nWire)
		End If
		If String.IsNullOrEmpty(mba2.AccountName) And Not mba2.IsEmpty Then
			lblError.Text &= "Correspondent bank account (" & sCurrency & ") cannot be saved: missing Account Name!<br />"
		ElseIf mba1.IsEmpty And mba2.IsEmpty Then
		Else
			mba2.SaveToWire(nWire)
			lblNoError.Text &= "Correspondent bank account (" & sCurrency & ") has been saved to Wire " & nWire & ".<br />"
			If Not String.IsNullOrEmpty(rblSave.SelectedValue) Then
				mba2.SaveToMerchant()
				lblNoError.Text &= "Correspondent bank account has been saved to merchant " & nMerchant & " (" & sCurrency & ").<br />"
				If rblSave.SelectedValue.ToUpper = "DEFAULT" Then
					mba2.SaveToDefault()
					lblNoError.Text &= "Correspondent bank account has been saved to merchant " & nMerchant & " (DEFAULT).<br />"
				End If
			End If
			mba2.LoadFromWire(nWire)
		End If
	End Sub
	
	Sub SelectCurrency()
		mba1.Currency = nCurrency
		if nMerchant > 0 Then mba1.Merchant = nMerchant Else mba1.Affiliate = nAffiliate
		mba1.IsCorrespondent = False
		mba1.LoadFromWire(nWire)
		mba2.Currency = nCurrency
		if nMerchant > 0 Then mba2.Merchant = nMerchant Else mba2.Affiliate = nAffiliate
		mba2.IsCorrespondent = True
		mba2.LoadFromWire(nWire)
		If nCurrency = 0 Then
    		if nMerchant > 0 Then mbal.Merchant = nMerchant Else mbal.Affiliate = nAffiliate
			mbal.LoadFromWire(nWire)
			mbal.Visible = True
		End If
	End Sub
</script>
<html>
<head runat="server">
	<title>Bank Account - </title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<script src="../js/func_common.js" type="text/jscript"></script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr" onload="focus();">
<form runat="server">
	<div style="text-align:center;">
		<div style="width:90%;text-align:left;">
			<UC1:MerchantBankAccountLocal ID="mbaL" Visible="false" runat="server" />
			<br />
			<UC1:MerchantBankAccount ID="mba1" runat="server" />
			<br />
			<UC1:MerchantBankAccount ID="mba2" runat="server" />
			<hr />
		</div>
		<div style="width:90%;text-align:left;">
			<span style="float:right;">
				<asp:Button Text="SAVE" OnCommand="ProcessData" CommandName="WIRE" CssClass="buttonWhite" runat="server" />
			</span>
			<asp:RadioButtonList ID="rblSave" CssClass="option" runat="server" RepeatLayout="Flow">
				<asp:ListItem Value="" Text="Save to the current wire only" Selected="True" />
				<asp:ListItem Value="MERCHANT" Text="Save to the current wire and to " />
				<asp:ListItem Value="DEFAULT" Text="Save to the current wire and to the " />
			</asp:RadioButtonList>
		</div>
		<div style="width:90%;text-align:left;">
			<asp:Label ID="lblNoError" CssClass="noErrorMessage" runat="server" />
			&nbsp;
			<asp:Label ID="lblError" CssClass="errorMessage" runat="server" />
		</div>
	</div>
</form>
</body>
</html>