<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20080629 Tamir
	'
	' Merchant blacklist management
	'
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Merchant Blacklist Filter</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminNetEng.css" />
</head>
<body class="menu">
	<br />
	<h1><asp:label ID="lblPermissions" runat="server" /> Blacklist <span> - Filter</span></h1>
	<br />
	<form id="frmFilter" action="merchant_blacklist.aspx" method="get" target="frmBody" onsubmit="parent.fraButton.FrmResize();">
		<table class="filterTable" cellpadding="0" cellspacing="0" border="0">
		<tr><th class="filter" colspan="2">Merchant </th></tr>
		<tr>
			<td>ID </td>
			<td><input name="MerchantID" class="short grayBG" /></td>
		</tr>
		<tr>
			<td>Merchant No. </td>
			<td><input name="MerchantNumber" class="medium grayBG" /></td>
		</tr>
		<tr>
			<td>Company&nbsp;name&nbsp; </td>
			<td><input name="MerchantName" class="medium2 grayBG" /></td>
		</tr>
		<tr>
			<td>Legal number </td>
			<td><input name="LegalNumber" class="medium2 grayBG" /> </td>
		</tr>
		<tr><th class="filter" colspan="2"><br />Personal</th></tr>
		<tr>
			<td>ID number </td>
			<td><input name="IDNumber" class="medium2 grayBG" /></td>
		</tr>
		<tr>
			<td>First name </td>
			<td><input name="FirstName" class="medium2 grayBG" /></td>
		</tr>
		<tr>
			<td>Last name </td>
			<td><input name="LastName" class="medium2 grayBG" /></td>
		</tr>
		<tr><th class="filter" colspan="2"><br />Contact </th></tr>
		<tr>
			<td>Phone </td>
			<td><input name="Phone" class="medium2 grayBG" /></td>
		</tr>
		<tr>
			<td>Fax </td>
			<td><input name="Fax" class="medium2 grayBG" /></td>
		</tr>
		<tr>
			<td>Cellular </td>
            <td><input name="Cellular" class="medium2 grayBG" /></td>
		</tr>
		<tr>
			<td>Mail </td>
			<td><input name="Mail" class="medium2 grayBG" /></td>
		</tr>
		<tr>
			<td>URL </td>
			<td><input name="URL" class="medium2 grayBG" /></td>
		</tr>
		<tr><th class="filter" colspan="2"><br />Filter Settings</th></tr>
		<tr>
			<td>Filter type </td>
			<td>
				<input type="radio" name="FilterType" value="OR" checked="checked" /> OR
				&nbsp;
				<input type="radio" name="FilterType" value="AND" /> AND
			</td>
		</tr>
		<tr>
			<td>&nbsp; </td>
			<td>
				<br />
				<input type="submit" class="buttonFilter" value="Search" />
				<input type="reset" class="buttonFilter buttonDelete" value="Reset" />
			</td>
		</tr>
		</table>
	</form>
</body>
</html>