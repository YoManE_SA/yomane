<%@ Page Language="VB" ResponseEncoding="windows-1255" %>
<%@ Import namespace="System.Data"%>
<%@ Import namespace="System.Data.SqlClient"%>

<script runat="server">
	Dim sTriggerName As String = String.Empty
	
	Function IsRegexMatch(ByVal sText As String, ByVal sPattern As String) As Boolean
		Return Regex.IsMatch(sText, sPattern)
	End Function
	
	Sub CheckUncheckFields(ByVal o As Object, ByVal e As RepeaterItemEventArgs)
		Try
			Dim sColumn As String = CType(e.Item.FindControl("hidColumn"), HiddenField).Value
			CType(e.Item.FindControl("chkLogA"), CheckBox).Checked = IsRegexMatch(sTriggerName, "_" & sColumn & "i{0,1}a")
			CType(e.Item.FindControl("chkLogR"), CheckBox).Checked = IsRegexMatch(sTriggerName, "_" & sColumn & "i{0,1}a{0,1}r")
			CType(e.Item.FindControl("chkLogU"), CheckBox).Checked = IsRegexMatch(sTriggerName, "_" & sColumn & "i{0,1}a{0,1}r{0,1}u")
			CType(e.Item.FindControl("chkImportant"), CheckBox).Checked = sTriggerName.Contains("_" & sColumn & "i")
		Catch
		End Try
	End Sub
	
	Sub ReloadFields(ByVal o As Object, ByVal e As EventArgs)
		txtTrigger.Text = String.Empty
		txtTrigger.Visible = False
		repField.Visible = False
		sTriggerName = String.Empty
		chkAllLogA.Checked = False
		chkAllLogR.Checked = False
		chkAllLogU.Checked = False
		pnlSelection.Visible = False
		If dbPages.TestVar(ddlTable.SelectedValue, 1, 0, 0) > 0 Then
			pnlSelection.Visible = True
			repField.Visible = True
			Dim sSQL As String = "SELECT TOP 1 name FROM sysobjects WHERE XType='TR' AND name LIKE 'trgDCL" & ddlTable.SelectedValue & "@_%' ESCAPE '@'"
			sTriggerName = dbPages.ExecScalar(sSQL)
			sSQL = "SELECT colorder, name, type FROM syscolumns WHERE id=" & ddlTable.SelectedValue & " ORDER BY name"
			If String.IsNullOrEmpty(sTriggerName) Then
				litNoTrigger.Visible = True
				lbTrigger.Text = String.Empty
				btnDrop.Visible = False
				For Each liAction As ListItem In cblAction.Items
					liAction.Selected = False
				Next
				txtTrigger.Text = String.Empty
			Else
				litNoTrigger.Visible = False
				lbTrigger.Text = sTriggerName
				btnDrop.Visible = True
				For Each liAction As ListItem In cblAction.Items
					liAction.Selected = sTriggerName.Substring(sTriggerName.Length - 3, 3).Contains(liAction.Value)
				Next
				txtTrigger.Text = String.Empty
				sSQL = "sp_helptext " & sTriggerName
				Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
				While drData.Read
					txtTrigger.Text &= Replace(drData(0), "	", "   ")
				End While
				drData.Close()
				txtTrigger.Text.Replace(vbTab, Space(3))
				dsField.ConnectionString = dbPages.DSN
				repField.DataBind()
			End If
			sSQL = "SELECT name nameID, name FROM syscolumns WHERE id=" & ddlTable.SelectedValue & " ORDER BY name"
			NPControls.FillList(ddlIdentityField, dbPages.ExecReader(sSQL), True, , , "< Select Identity Field >")
			For Each liField As ListItem In ddlIdentityField.Items
				If liField.Text.ToUpper = "ID" Then liField.Selected = True
			Next
			sSQL = "SELECT name FROM syscolumns WHERE id=" & ddlTable.SelectedValue & " AND colstat=1"
			Dim sIdentityFieldName As String = dbPages.ExecScalar(sSQL)
			If Not String.IsNullOrEmpty(sIdentityFieldName) Then
				ddlIdentityField.SelectedValue = sIdentityFieldName
				ddlIdentityField.SelectedItem.Attributes.CssStyle.Add("color", "Maroon")
				ddlIdentityField.SelectedItem.Attributes.CssStyle.Add("background-color", "Beige")
			End If
		End If
		lblError.Text = String.Empty
	End Sub

	Sub RecheckFields(ByVal o As Object, ByVal e As EventArgs)
		Dim bCheck As Boolean = CType(o, CheckBox).Checked
		Dim sCheckboxID As String = CType(o, CheckBox).ID.Remove(3, 3)
		For Each riField As RepeaterItem In repField.Items
			CType(riField.FindControl(sCheckboxID), CheckBox).Checked = bCheck And CType(riField.FindControl("litColumn"), Literal).Text <> ddlIdentityField.SelectedItem.Text
		Next
		lblError.Text = String.Empty
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		If Not Page.IsPostBack Then NPControls.FillList(ddlTable, dbPages.ExecReader("SELECT * FROM viewTriggersDCL ORDER BY Name"), , , , "< Select Table >")
		dsField.ConnectionString = dbPages.DSN
	End Sub

	Sub DropTrigger(ByVal o As Object, ByVal e As EventArgs)
		If lbTrigger.Text <> String.Empty Then
			Dim sSQL As String = "IF EXISTS (SELECT name FROM sysobjects WHERE name='" & lbTrigger.Text & "') DROP TRIGGER " & lbTrigger.Text
			dbPages.ExecSql(sSQL)
		End If
		lbTrigger.Text = String.Empty
		txtTrigger.Text = String.Empty
		litNoTrigger.Visible = True
		If Not o Is Nothing Then ReloadFields(Nothing, Nothing)
		lblError.Text = String.Empty
		txtTrigger.Visible = False
		sTriggerName = String.Empty
		repField.DataBind()
	End Sub

	Sub ShowHideTriggerText(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		txtTrigger.Visible = Not txtTrigger.Visible
	End Sub
	
	Sub CreateTrigger(ByVal o As Object, ByVal e As EventArgs)
		lblError.Text = "<br />Cannot create trigger: please specify the table, the identity field, the action(s) and the field(s) to monitor"
		If ddlTable.SelectedIndex = 0 Then
			lblError.Text = "<br />Cannot create trigger - Please choose table"
			Exit Sub
		End If
		If cblAction.SelectedIndex < 0 Then
			lblError.Text = "<br />Cannot create trigger - Please choose action"
			Exit Sub
		End If
		If ddlIdentityField.SelectedIndex <= 0 Then
			lblError.Text = "<br />Cannot create trigger - Please choose identity field"
			Exit Sub
		End If
		Dim sbTriggerName As StringBuilder = New StringBuilder("trgDCL" & ddlTable.SelectedValue & "_")
		Dim bCanCreate As Boolean = False, sImportant As String, sActions As String
		For Each riField As RepeaterItem In repField.Items
			If CType(riField.FindControl("chkLogA"), CheckBox).Checked Or CType(riField.FindControl("chkLogR"), CheckBox).Checked Or CType(riField.FindControl("chkLogU"), CheckBox).Checked Then
				bCanCreate = True
				sbTriggerName.Append(CType(riField.FindControl("hidColumn"), HiddenField).Value)
				If CType(riField.FindControl("chkImportant"), CheckBox).Checked Then sbTriggerName.Append("i")
				If CType(riField.FindControl("chkLogA"), CheckBox).Checked Then sbTriggerName.Append("a")
				If CType(riField.FindControl("chkLogR"), CheckBox).Checked Then sbTriggerName.Append("r")
				If CType(riField.FindControl("chkLogU"), CheckBox).Checked Then sbTriggerName.Append("u")
				sbTriggerName.Append("_")
			End If
		Next
		If Not bCanCreate Then
			lblError.Text = "<br />Cannot create trigger - Please select fields to monitor"
			Exit Sub
		End If
		For Each liAction As ListItem In cblAction.Items
			If liAction.Selected Then sbTriggerName.Append(liAction.Value)
		Next
		Dim sTriggerNameTemp As String = sbTriggerName.ToString
		If sTriggerNameTemp.Length > 128 Then
			lblError.Text = "<br />Cannot create trigger - too many fields specified to monitor, please uncheck some."
			Exit Sub
		End If

		sTriggerName = sTriggerNameTemp

		Dim sTable As String = ddlTable.SelectedItem.Text
		If sTable.Contains(" ") Then sTable = sTable.Remove(sTable.IndexOf(" "))
		Dim sField As String, bFirst As Boolean, sIdentityField As String = ddlIdentityField.SelectedValue

		Dim sbFetch As StringBuilder = New StringBuilder
		sbFetch.AppendLine("FETCH NEXT FROM curFields INTO")
		sbFetch.Append("			@nID")
		For Each riField As RepeaterItem In repField.Items
			If CType(riField.FindControl("chkLogA"), CheckBox).Checked Or CType(riField.FindControl("chkLogR"), CheckBox).Checked Or CType(riField.FindControl("chkLogU"), CheckBox).Checked Then
				sbFetch.AppendLine(",")
				sbFetch.AppendFormat("			@sDeleted_{0}, @sInserted_{0}", CType(riField.FindControl("litColumn"), Literal).Text)
			End If
		Next
		Dim sFetch As String = sbFetch.ToString
			
		Dim sbSQL As StringBuilder = New StringBuilder
		sbSQL.Append("CREATE TRIGGER " & sTriggerName)
		sbSQL.Append(" ON " & sTable & " AFTER ")
		bFirst = True
		For Each liAction As ListItem In cblAction.Items
			If liAction.Selected Then
				If bFirst Then bFirst = False Else sbSQL.Append(", ")
				sbSQL.Append(liAction.Text.ToUpper)
			End If
		Next
		sbSQL.AppendLine()
		sbSQL.AppendLine("AS")
		sbSQL.AppendLine("BEGIN")
		sbSQL.AppendLine("	DECLARE @nID int, @nDeleted int, @nInserted int, @sAction char(6)")
		sbSQL.AppendLine("	SET @nDeleted=IsNull((SELECT Count(*) FROM Deleted), 0)")
		sbSQL.AppendLine("	SET @nInserted=IsNull((SELECT Count(*) FROM Inserted), 0)")
		sbSQL.AppendLine("	SET @sAction=CASE WHEN @nInserted*@nDeleted>0 THEN 'UPDATE' WHEN @nInserted>0 THEN 'INSERT' ELSE 'DELETE' END")
		For Each riField As RepeaterItem In repField.Items
			If CType(riField.FindControl("chkLogA"), CheckBox).Checked Or CType(riField.FindControl("chkLogR"), CheckBox).Checked Or CType(riField.FindControl("chkLogU"), CheckBox).Checked Then
				sbSQL.AppendFormat("	DECLARE @sDeleted_{0} nvarchar(900), @sInserted_{0} nvarchar(900)", CType(riField.FindControl("litColumn"), Literal).Text)
				sbSQL.AppendLine()
			End If
		Next
		sbSQL.AppendLine("	IF @sAction='DELETE'")
		sbSQL.AppendLine("		DECLARE curFields CURSOR FOR")
		sbSQL.AppendLine("			SELECT")
		sbSQL.Append("				" & sIdentityField)
		For Each riField As RepeaterItem In repField.Items
			If CType(riField.FindControl("chkLogR"), CheckBox).Checked Then
				sbSQL.AppendLine(",")
				sbSQL.AppendFormat("				Left(Cast({0} AS nvarchar(MAX)), 900), NULL", CType(riField.FindControl("litColumn"), Literal).Text)
			End If
		Next
		sbSQL.AppendLine()
		sbSQL.AppendLine("		FROM")
		sbSQL.AppendLine("			Deleted")
		sbSQL.AppendLine("	IF @sAction='INSERT'")
		sbSQL.AppendLine("		DECLARE curFields CURSOR FOR")
		sbSQL.AppendLine("			SELECT")
		sbSQL.Append("				" & sIdentityField)
		For Each riField As RepeaterItem In repField.Items
			If CType(riField.FindControl("chkLogA"), CheckBox).Checked Then
				sbSQL.AppendLine(",")
				sbSQL.AppendFormat("				NULL, Left(Cast({0} AS nvarchar(MAX)), 900)", CType(riField.FindControl("litColumn"), Literal).Text)
			End If
		Next
		sbSQL.AppendLine()
		sbSQL.AppendLine("		FROM")
		sbSQL.AppendLine("			Inserted")
		sbSQL.AppendLine("	IF @sAction='UPDATE'")
		sbSQL.AppendLine("		DECLARE curFields CURSOR FOR")
		sbSQL.AppendLine("			SELECT")
		sbSQL.Append("				Inserted." & sIdentityField)
		For Each riField As RepeaterItem In repField.Items
			If CType(riField.FindControl("chkLogU"), CheckBox).Checked Then
				sbSQL.AppendLine(",")
				sbSQL.AppendFormat("				Left(Cast(Deleted.{0} AS nvarchar(MAX)), 900), Left(Cast(Inserted.{0} AS nvarchar(MAX)), 900)", CType(riField.FindControl("litColumn"), Literal).Text)
			End If
		Next
		sbSQL.AppendLine()
		sbSQL.AppendLine("			FROM")
		sbSQL.AppendLine("				Inserted")
		sbSQL.AppendLine("				INNER JOIN Deleted ON Inserted." & sIdentityField & "=Deleted." & sIdentityField)
		sbSQL.AppendLine("	OPEN curFields")
		sbSQL.AppendLine("	" & sFetch)
		sbSQL.AppendLine("	WHILE @@FETCH_STATUS=0")
		sbSQL.AppendLine("	BEGIN")
		For Each riField As RepeaterItem In repField.Items
			If CType(riField.FindControl("chkLogA"), CheckBox).Checked Or CType(riField.FindControl("chkLogR"), CheckBox).Checked Or CType(riField.FindControl("chkLogU"), CheckBox).Checked Then
				sField = CType(riField.FindControl("litColumn"), Literal).Text
				sImportant = IIf(CType(riField.FindControl("chkImportant"), CheckBox).Checked, "1", "0")
				sActions = String.Empty
				If CType(riField.FindControl("chkLogA"), CheckBox).Checked Then sActions &= ", 'INSERT'"
				If CType(riField.FindControl("chkLogR"), CheckBox).Checked Then sActions &= ", 'DELETE'"
				If CType(riField.FindControl("chkLogU"), CheckBox).Checked Then sActions &= ", 'UPDATE'"
				sbSQL.AppendLine("	IF @sAction IN (" & sActions.Substring(2) & ") AND (@sAction<>'UPDATE' OR @sInserted_" & sField & "<>@sDeleted_" & sField & ")")
				sbSQL.AppendLine("		INSERT INTO tblDataChangeLog(DCL_Table, DCL_ID, DCL_Field, DCL_Old, DCL_New, DCL_Action, DCL_IsImportant)")
				sbSQL.AppendLine("			VALUES ('" & sTable & "', @nID, '" & sField & "', @sDeleted_" & sField & ", @sInserted_" & sField & ", @sAction, " & sImportant & ")")
			End If
		Next
		sbSQL.AppendLine("		" & sFetch)
		sbSQL.AppendLine("	END")
		sbSQL.AppendLine("	CLOSE curFields")
		sbSQL.AppendLine("	DEALLOCATE curFields")
		sbSQL.AppendLine("END")
		txtTrigger.Text = "<br />" & sbSQL.ToString.Replace(vbTab, Space(3))
		DropTrigger(Nothing, Nothing)
		dbPages.ExecSql(sbSQL.ToString)
		btnDrop.Visible = True
		ReloadFields(Nothing, Nothing)
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>DATA CHANGES - Configuration</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		table table td{vertical-align:top;}
	</style>
</head>
<body onload="focus();" style="text-align:center;">

<iframe id="ifrDelete" style="display:none;"></iframe>
<form id="frmMain" runat="server">
	<table width="95%">
	<tr>
		<td id="pageMainHeadingTd">
			<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">DATA CHANGES</span> - Configuration
		</td>
	</tr>
	</table>
	<br /><br />
	<table width="95%" border="0" style="border:1px solid gray; background-color:#f5f5f5;">
	<tr>
		<td>
			<table cellpadding="1" cellspacing="1" border="0" class="formThin">
			<tr>
				<td style="padding-right:10px;">Table<br /><asp:DropDownList ID="ddlTable" runat="server" Width="200" AutoPostBack="true" OnSelectedIndexChanged="ReloadFields" /></td>
				<td style="padding-right:10px;">Identity<br /><asp:DropDownList ID="ddlIdentityField" Width="200" runat="server" /></td>
				<td style="padding-right:10px;">
					Actions<br />
					<asp:CheckBoxList ID="cblAction" CssClass="option" RepeatDirection="Horizontal" runat="server">
						<asp:ListItem Text="Delete" Value="D" />
						<asp:ListItem Text="Insert" Value="I" />
						<asp:ListItem Text="Update" Value="U" />
					</asp:CheckBoxList>
				</td>
				<td valign="bottom">
					<br /><asp:button ID="btnCreate" UseSubmitBehavior="false" runat="server" Width="60" Text="Create" OnClick="CreateTrigger" />
					<asp:button ID="btnDrop" UseSubmitBehavior="false" runat="server" Width="60" Text="Drop" OnClick="DropTrigger" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
	<asp:Panel ID="pnlSelection" Visible="false" runat="server">
		<table width="95%" border="0">
		<tr>
			<td style="font-size:11px;">
				<span style="font-weight:bold;">MONITORING:</span>
				<asp:LinkButton ID="lbTrigger" Text="" OnClick="ShowHideTriggerText" runat="server" />
				<asp:Literal ID="litNoTrigger" Text="Not set" runat="server" />
			</td>
		</tr>
		</table>
		<br />
		<table width="95%" border="0">
		<tr>
			<td>
				<asp:Label ID="lblError" runat="server" ForeColor="red" />
				<asp:TextBox ID="txtTrigger" TextMode="MultiLine" Height="200px" Width="100%" Font-Size="11px" Visible="false" BorderStyle="dotted" BorderWidth="1" runat="server" />
				<script language="javascript" type="text/javascript">
					if (document.getElementById("txtTrigger")) document.getElementById("txtTrigger").style.width = document.body.clientWidth - 100;
				</script>
			</td>
		</tr>
		</table>
		<br />
		<table width="95%" border="0">
		<tr>
			<td style="font-weight:bold; font-size:11px;">FIELD SELECTION FOR MONITOR</td>
			<td align="right" style="font-size:11px;">
				<asp:CheckBox ID="chkAllLogA" OnCheckedChanged="RecheckFields" AutoPostBack="true" runat="server" />
				Select all for insert&nbsp;
				<asp:CheckBox ID="chkAllLogR" OnCheckedChanged="RecheckFields" AutoPostBack="true" runat="server" />
				Select all for delete&nbsp;
				<asp:CheckBox ID="chkAllLogU" OnCheckedChanged="RecheckFields" AutoPostBack="true" runat="server" />
				Select all for update&nbsp;
				<asp:CheckBox ID="chkAllImportant" OnCheckedChanged="RecheckFields" AutoPostBack="true" runat="server" />
				Mark all as important
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<asp:Repeater ID="repField" DataSourceID="dsField" Visible="false" runat="server" OnItemDataBound="CheckUncheckFields">
					<HeaderTemplate>
						<table class="formNormal option" width="100%" border="0">
						<tr>
							<th class="Vertical">Insert</th><th class="Vertical">Delete</th><th class="Vertical">Update</th><th class="Vertical">Important</th><th>Column Name</th><td width="10"></td>
							<th class="Vertical">Insert</th><th class="Vertical">Delete</th><th class="Vertical">Update</th><th class="Vertical">Important</th><th>Column Name</th><td width="10"></td>
							<th class="Vertical">Insert</th><th class="Vertical">Delete</th><th class="Vertical">Update</th><th class="Vertical">Important</th><th>Column Name</th>
					</HeaderTemplate>
					<ItemTemplate>
						<asp:HiddenField ID="hidColumn" Value='<%#Eval("colorder")%>' runat="server" />
						<asp:Literal Text='<%# IIf(Eval("CellIndex")=1, "</tr><tr style=""border-bottom:1px solid silver;"">", "<td></td>") %>' runat="server" />
						<td style="text-align:center;"><asp:CheckBox ID="chkLogA" CssClass="option" Enabled='<%#Eval("type")<>35%>' runat="server" /></td>
						<td style="text-align:center;"><asp:CheckBox ID="chkLogR" CssClass="option" Enabled='<%#Eval("type")<>35%>' runat="server" /></td>
						<td style="text-align:center;"><asp:CheckBox ID="chkLogU" CssClass="option" Enabled='<%#Eval("type")<>35%>' runat="server" /></td>
						<td style="text-align:center;"><asp:CheckBox ID="chkImportant" CssClass="option" Enabled='<%#Eval("type")<>35%>' runat="server" /></td>
						<td style="vertical-align:middle;"><asp:Literal ID="litColumn" Text='<%#Eval("name")%>' runat="server" /></td>
					</ItemTemplate>
					<FooterTemplate></tr></table></FooterTemplate>
				</asp:Repeater>
				<asp:SqlDataSource ID="dsField" runat="server" SelectCommand="SELECT ROW_NUMBER() OVER(ORDER BY name)%3 CellIndex, colorder, name, type FROM syscolumns WHERE id=IsNull(@TableID, 0) ORDER BY name">
					<SelectParameters>
						<asp:ControlParameter Name="TableID" ControlID="ddlTable" PropertyName="SelectedValue" DefaultValue="0" ConvertEmptyStringToNull="true" />
					</SelectParameters>
				</asp:SqlDataSource>
			</td>
		</tr>
		</table>
	</asp:Panel>
	<br />
</form>

</body>
</html>