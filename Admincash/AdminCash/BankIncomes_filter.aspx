<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = DateAdd("m", -1, Date.Now).ToShortDateString
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
        If Not IsPostBack Then
            BankAccountID.Items.Add("")
            Dim iReader As SqlDataReader = dbPages.ExecReader("Select * From tblBankAccounts Where BA_ID IN(Select Distinct BI_BankAccountID From tblBankIncomes) Order By BA_BankName, BA_AccountNumber")
            While iReader.Read()
                BankAccountID.Items.Add(New ListItem(iReader("BA_BankName") & ": " & iReader("BA_AccountNumber"), iReader("BA_ID")))
            End While
            iReader.Close()
        End If
    End Sub
</script>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<title>Control Panel</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="body" runat="server">
    <form runat="server" action="BankIncomes_data.aspx" target="frmBody" method="post" onsubmit="ConvertFormToClient(this, 'BankIncomes_data.aspx');">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />
    <input type="hidden" id="Source" name="Source" value="filter" />
	<h1><asp:label ID="lblPermissions" runat="server" /> Bank Incomes<span> - Filter</span></h1>
	
	<table class="filterNormal">
		<tr><th>Bank Account</th></tr>
		<tr><td><asp:DropDownList runat="server" ID="BankAccountID" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Amount</th></tr>
		<tr>
			<td>
				<select name="iCurrency">
					<option value=""></option>
					<%
						Dim i As Integer
						For i = 0 To CType(eCurrencies.MAX_CURRENCY, Integer)
							%>
								<option value="<%=i%>"><%=dbPages.GetCurText(i)%></option>
							<%
						Next
					%>
				</select>
			</td>
			<td>
				<input type="text" class="input1" size="5" dir="ltr" name="amountFrom" value="" />
				To <input type="text" class="input1" size="5" dir="ltr" name="amountTo" value="" />
			</td>
		</tr>
		<tr>
			<td>Diff Amount</td>
			<td>
				<input type="text" class="input1" size="5" dir="ltr" name="DiffAmountFrom" value="" />
				To <input type="text" class="input1" size="5" dir="ltr" name="DiffAmountTo" value="" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
	    <tr><td height="6"></td></tr>
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" Title="Ins Date" /></td></tr>
		<tr><td height="6"></td></tr>
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl2" runat="server" Title="From Date" FromDateFieldName="BI_FromDateFrom" ToDateFieldName="BI_FromDateTo" FromTimeFieldName="BI_FromDateFromTime" ToTimeFieldName="BI_FromDateToTime" /></td></tr>
		<tr><td height="6"></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td>
				<select name="PageSize">
					<option value="20" selected="selected">20 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td style="text-align:right;">
				<input type="reset" value="RESET" />
				<input type="submit" name="Action" value="SEARCH" /><br />
			</td>
		</tr>
		<tr>
		    <th></th>
		    <td style="text-align:right;">
			<input type="submit" name="Excel" value="   EXPORT XLS  " /><br />
		    </td>
		</tr>
	</table>
    </form>
</asp:Content>