<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_transPay.asp"-->
<%
nPayID = TestNumVar(request("PayDateID"),0,-1,0)
nCompanyID = dbText(request("companyID"))
'-----------------------------------------------------------------------------------
'	Get fees and calculation data for this payment
'-----------------------------------------------------------------------------------
sSQL="SELECT currency, IsChargeVAT, VATamount, Paydate, PrimePercent FROM tblTransactionPay WHERE id=" & nPayID
set rsDataPaydate = oledbData.execute(sSQL)
If Not rsDataPaydate.EOF Then
	nCurrency = rsDataPaydate("currency")
	sIsChargeVAT = rsDataPaydate("IsChargeVAT")
	sVATamount = rsDataPaydate("VATamount")
	if sIsChargeVAT then sVAT = (sVATamount+1) else sVAT = 1 end if
	nPaydate = rsDataPaydate("Paydate")
	nPrimePercent = rsDataPaydate("PrimePercent")
End if
rsDataPaydate.Close
If int(nPayID) <= 4447 Then nRoundPrecision = 5 Else nRoundPrecision = 2

'-----------------------------------------------------------------------------------
'	Get all transaction from this payment for calculation
'-----------------------------------------------------------------------------------
sSQL="SELECT tblCreditCard.CCTypeID, tblCompanyTransPass.* " &_
"FROM tblCompanyTransPass LEFT OUTER JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID " &_
"WHERE (tblCompanyTransPass.PayID LIKE ('%;" & nPayID & ";%')) AND tblCompanyTransPass.CompanyID=" & nCompanyID & " ORDER BY tblCompanyTransPass.InsertDate DESC"
set rsData=oledbData.execute(sSQL)
if not rsData.EOF then
	%>
	<!--#include file="../Include/common_totalTransPayed_inc.asp"-->
	<%
	call rsData.close
	Set rsData = nothing

	nTotalCharge = 0
	nTotalCharge = formatnumber(nAmount+nAmountTransAdmin+nAmounCredit+nAmountTransSystem-nPayDenied,2,0,0,0)
	'nTotalCharge = formatnumber(nAmount+nAmountTransAdmin+nAmounCredit+nAmountTransSystem-nPayDenied,2,0,0,0)

    nTotalFees = 0
    nTotalFees = nNetpayFee_TransCharge_Chb + nNetpayFee_TransCharge_Clrf + nAmountTransSystemFee
    nTotalFees = nTotalFees + nNetpayFee_TransCharge_Debit + nNetpayFee_TransCharge_Credit + nNetpayFee_RatioCharge + nPercentsCompanyCcPay2 + nPercentsCompanyCcPay1

	nTotalFees = formatnumber(nTotalFees * sVAT,2,0,0,0)

    nTotalPay = 0
    nTotalPay = nTotalCharge - nTotalFees
	'nTotalPay = formatnumber(nAmount+nAmountTransAdmin+nAmounCredit+nAmountTransSystem-nPayDenied-nTotalFees,2,0,0,0)
end if

'-----------------------------------------------------------------------------------
'	Update totals in payment table
'-----------------------------------------------------------------------------------
sSQL="UPDATE tblTransactionPay SET transTotal=" & nTotalCharge & ", TransChargeTotal=" & nTotalFees & ", TransPayTotal=" & nTotalPay & ", transRollingReserve=" & nAmountTransAdmin & " WHERE ID=" & nPayID
oledbData.execute sSQL


'-----------------------------------------------------------------------------------
'	Update totals in wire table
'-----------------------------------------------------------------------------------
sSQL="UPDATE tblWireMoney SET WireAmount=" & nTotalPay & " WHERE WireType=1 AND WireSourceTbl_id=" & nPayID
oledbData.execute sSQL

call closeConnection()

'-----------------------------------------------------------------------------------
'	Get all transaction from this payment for calculation
'-----------------------------------------------------------------------------------
If trim(request("GoToUrl"))<>"" then
	response.redirect request("GoToUrl") & "?Action=" & request("Action") & "&CompanyID=" & nCompanyID & "&payID=" & nPayID & "&payDate=" & nPaydate
Else
	response.write "<strong>Update done !</strong><br />"
	response.write "Total Charge: " & nTotalCharge & "<br />"
	response.write "Total Fees: " & nTotalFees & "<br />"
	response.write "Total Pay: " & nTotalPay & "<br />"
	response.write "Total Trans Admin: " & nAmountTransAdmin & "<br />"
	response.end
End if
%>