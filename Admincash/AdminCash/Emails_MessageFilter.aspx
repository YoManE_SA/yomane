﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Emails_MessageFilter.aspx.cs" Inherits="Emails_MessageFilter" %>
<%@ Register Assembly="Netpay.Web" Namespace="Netpay.Web.Controls" TagPrefix="Netpay" %>
<%@ Import Namespace="Netpay.Infrastructure" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forms - Posts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../js/jquery-ui-1.8.10.custom/css/custom-theme/jquery-ui-1.8.10.custom.css" />
    <script type="text/javascript">
        var prevrow = "";
        $(document).ready(function ()
        {
        	$('#emailsList').parent().css("width", "100%");
        	$('.EmailRow').hover(
				function () {
    				$(this).css('background-color', '#c0c0c0');
				},
				function () {
    				$(this).css('background-color', '');
				}
			);

        	$('.EmailRow').each(function (index)
            {
                $(this).css("cursor", "pointer");
                $(this).bind('click', function ()
                {
                    if (prevrow == "")
                    {
                        $(this).addClass("gridSelectedRow");
                        prevrow = $(this);
                    }
                    else
                    {
                        prevrow.removeClass("gridSelectedRow");
                        $(this).addClass("gridSelectedRow");
                        prevrow = $(this);
                    }
                    window.parent.frames["frmBody"].location = "Emails_MessageView.aspx?MessageId=" + $(this).find('.tt').attr('id');
                });
            });
        });

        function checkDateFields() { if ($('#fromDate').val() == '' || $('#toDate').val() == '') { alert("Enter Correctly Date"); return false; } }
        function reloadByQuery()
        {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var txt_search_words = $('#txt_search_words').val();
            var ddlStatus = $('#ddlStatus').val();
            var ddlUser = $('#ddlUser').val();
            var url = "Emails_MessageFilter.aspx?fromDate=" + fromDate + "&toDate=" + toDate + "&txt_search_words=" + txt_search_words + "&ddlStatus=" + ddlStatus + "&ddlUser=" + ddlUser;
           }
    </script>
	<style type="text/css">
		table.emailsList td
		{
			text-align:left;
		}
	
		table.emailsList th
		{
			background-color:#e0e0e0;
			text-align:left;
		}
		table.emailsList tr.emailGroup td
		{
			background-color:#e1e1e1;
			font-weight:bold;
			text-align:left;
			text-transform:uppercase;
			padding-left:5px;
		}
		
		.pagerMsg { background-color:rgb(238, 238, 238)!important;}
	</style>
</head>
<body class="menu" style="padding: 0px 18px;">
    <form id="frmPostsMenu" runat="server" style="display: block;">
        <asp:HiddenField ID="hf_nameSort" runat="server" />
        <asp:HiddenField ID="hf_holderforpaging" runat="server" />
        <Netpay:ScriptManager runat="server" />
        <table class="cleartbl" style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
            <tr style="height: 20px;">
                <td style="height: 15px;">
                    <h1><asp:Label ID="lblPermissions" runat="server" /> Emails</h1>
                </td>
                <td valign="bottom" align="right" style="font-size:12px;">
                    <Netpay:PopupButton ID="wcEmailSelector" IconSrc="/NPCommon/Images/icon_ShowMailbox.gif" CssClass="uppercase" Text="" runat="server">
                        <table class="cleartbl" border="0" cellpadding="0" cellspacing="0" align="right">
                            <asp:Repeater ID="repeaterEmails" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td style="padding: 4px;">
                                            <a href="?mailboxId=<%# Eval("ID") %>"><%# Eval("EmailAddress")%></a>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </Netpay:PopupButton>
					&nbsp; | &nbsp;
                    <Netpay:PopupButton ID="PopupButton1" IconSrc="/NPCommon/Images/Icon_colors.gif" Text="LEGEND" CssClass="uppercase" runat="server">
                        <table class="cleartbl" border="0" cellpadding="0" cellspacing="0" align="right">
                            <asp:Repeater ID="rep_status" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td style="padding: 4px;">
                                            <span style="background-color: <%#Eval("Color") %>; border: 1px solid silver;">
                                                <img src="../images/1_space.gif" width="4" height="8" border="0" /></span> <%#Eval("Name")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </Netpay:PopupButton>
                </td>
            </tr>
        </table>
        <div style="margin-top: 20px; border:3px solid #e1e1e1;">
		<%--	<fieldset>
				<legend>SEARCH</legend>--%>
				<table class="filterBox" width="100%">
					<tr>
						<td>
							<span class="inputHeading">Folder</span><br />
							<asp:DropDownList runat="server" ID="ddlIsSent" style="Font-Size:11px;">
								<asp:ListItem Text="" Value="" />
								<asp:ListItem Text="Inbox" Value="false" Selected="True" />
								<asp:ListItem Text="Sent Items" Value="true" />
								<asp:ListItem Text="Deleted Items" Value="Deleted" />
							</asp:DropDownList>
						</td>
						<td colspan="2">
							<span class="inputHeading">Free Text </span><br />
							<asp:TextBox ID="txt_search_words" MaxLength="100" runat="server" Width="100%"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<span class="inputHeading">Status</span><br />
							<asp:DropDownList ID="ddlStatus" runat="server" />
						</td>
						<td>
							<span class="inputHeading">Date Range</span><br />
							<Netpay:DateRangePicker ID="DatePic" Font-Bold="false" Layout="Horizontal"  runat="server" />
						</td>
						<td>
							<span class="inputHeading">User</span><br />
							<asp:DropDownList ID="ddlUser" runat="server" />
						</td>
						<td valign="bottom" style="padding-bottom: 3px;">
							<asp:Button runat="server" ID="btn_search" OnClientClick="return checkDateFields();" OnClick="Search_Click" Text=" Search " CssClass="buttonWhite" />
						</td>
					</tr>
				</table>
			<%--</fieldset>--%>
        </div>
		<br />
        <div style="text-align:left; padding-left:10px; font-size:13px;"><asp:Label ID="lbl_message_For_grvPosts" runat="server" Text="No Records found !" Visible="false" /></div>
		<asp:Repeater runat="server" ID="rptEmails" OnItemDataBound="Emails_OnRowDataBound">
			<HeaderTemplate>
				<table width="100%" border="0" class="emailsList" id="emailsList" cellspacing="0" style="width:100%; border-collapse: collapse;">
					<tr style="display:none;">
						<th>
							<asp:LinkButton ID="lbt_sort_status" runat="server" ToolTip="Status" CommandName="sort_MessageStatus" CommandArgument="ACS" OnClick="lbt_Sort_Click" ClientIDMode="Static" Style='padding: 50%;' Font-Underline="false"></asp:LinkButton>
						</th>
						<th>
							<asp:LinkButton ID="lbt_sort_DateAndTime" runat="server" OnClick="lbt_Sort_Click" CommandName="sort_MessageDate" ClientIDMode="Static" Text="Date&amp;Time" Font-Underline="false" />
							|
							<asp:LinkButton runat="server" ID="lbt_sort_From" OnClick="lbt_Sort_Click" CommandName="sort_EmailFrom" ClientIDMode="Static" Font-Underline="false" Text="From" />
							<br />
							<asp:LinkButton ID="lbt_sort_Subject" runat="server" OnClick="lbt_Sort_Click" CommandName="sort_Subject" ClientIDMode="Static" Text="Subject" Font-Underline="false" />
						</th>
					</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr class="emailGroup" runat="server" id="trEmailGroup">
					<td colspan="2"><asp:Literal runat="server" ID="ltEmailGroupText" /></td>
				</tr>
				<tr class="EmailRow">
					<td style="width:16px; padding-top:3px;">
                        <span title="<%#Eval("IsSent").ToString().ToLower() == "false" ?"IN":"OUT" %>" style="display: inline-block; border: 1px solid silver; height: 8px; width: 8px; padding-left: 1px; padding-right: 1px; background-color: <%#  Eval("IsSent").ToString().ToLower() == "false" ? getStatusColor((int?)Eval("Status")):getStatusColor(3) %>;"></span>
					</td>
					<td style="padding-top:4px;">
                        <span style="text-transform:uppercase; font-size:11px; <%#(int)Eval("Status") == 0 ? "font-weight:bold;" : "" %>">
                            <%#Eval("EmailFrom")%>
						</span>
                        <span class="tt" style="float:right; font-size:12px;" id="<%#Eval("ID") %>">
                            <%# messageFormatDate((DateTime)Eval("Date"))%>
							<%#Eval("IsSent").ToString().ToLower() == "false" ? "<img alt='Msg In' title='Msg In' height='10' width='10' id='0' src='../images/icon_greenArrow.gif' />" : "<img alt='Msg Out' dir='ltr' title='Msg Out' height='10' width='10' id='0' src='../images/icon_ArrowR2.gif' />"%>
                        </span>
					</td>
				</tr>
				<tr><%--class="gridRow2"--%>
					<td></td>
					<td style="padding-bottom:10px;">
                        <span class="subject_sp" style="color:#5C5C5C;" title="<%#Eval("Subject")%>">
                            <%#(Eval("Subject") as string).Truncate(64)%>
                        </span>
                        <span style="float:right; color:#5C5C5C;">
							<%#Eval("AdmincashUser")%>
							<asp:Image ID="Image1" runat="server" ImageUrl="~/images/paperclip.gif" Visible='<%# (Container.DataItem as Netpay.Emails.Message).AttachmentCount > 0 %>' />
						</span>
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:Repeater>
		<br />
		<netpay:Pager ID="pager" runat="server" OnPageChanged="Pager_PageChanged" /><br />
        <div style="margin-top: 10px;">
            <asp:Button runat="server" ID="ExcelExport" Visible="false" Text="Export To Excel" CssClass="buttonWhite buttonSemiWide" />
			&nbsp;
            <input type="button" value="New Message" class="buttonWhite buttonSemiWide" onclick="window.parent.frames['frmBody'].location='Emails_MessageEditor.aspx'" />
            &nbsp;
            <input type="button" value="Templates" class="buttonWhite buttonSemiWide" onclick="window.parent.frames['frmBody'].location='Emails_ManageTemplates.aspx'" />
        </div>
		<br />
    </form>
</body>
</html>
