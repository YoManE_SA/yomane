<%@ Page Language="C#" %>
<script runat="server">
	protected override void OnLoad(EventArgs e)
	{
		if (Request["CompanyID"].ToNullableInt() != null){
			var merchant = Netpay.Bll.Merchants.Merchant.Load(Request["CompanyID"].ToNullableInt().GetValueOrDefault());
			string target = Request["Target"].EmptyIfNull().Trim();
			string projectUrl = null;
			switch(target){
				case "" : projectUrl = WebUtils.CurrentDomain.MerchantUrl + "website/logout.aspx"; break;
				case "test": projectUrl = WebUtils.CurrentDomain.MerchantUrl.Replace("https://merchants.", "http://merchants.test.") + "website/logout.aspx"; break;
				case "devcenter": projectUrl = WebUtils.CurrentDomain.DevCentertUrl + "website/Login.aspx"; break;
				case "devcentertest": projectUrl = WebUtils.CurrentDomain.DevCentertUrl.Replace("https://devcenter.", "http://devcenter.test.") + "website/Login.aspx"; break;
				default: return; 
			}
			Response.Redirect(projectUrl + string.Format("?login=outer&mail={0}&username={1}&userpassword={2}", merchant.Login.EmailAddress, merchant.Login.UserName, Netpay.Infrastructure.Security.Login.GetPassword(merchant.LoginID.Value, Request.UserHostAddress)), true);
		}else if (Request["CustomerID"].ToNullableInt() != null){
			var customer = Netpay.Bll.Customers.Customer.Load(Request["CustomerID"].ToNullableInt().GetValueOrDefault());
			Response.Redirect(WebUtils.CurrentDomain.WalletUrl + string.Format("?login=outer&mail={0}&password=%A1%B2%C3%D4%A1%B2%C3%D4%A1%B2%C3%D4%A1%B2%C3%D4%A1%B2%C3%D4%A1%B2%C3%D4%A1%B2%C3%D4&userpassword={2}", customer.Login.EmailAddress, customer.Login.UserName, Netpay.Infrastructure.Security.Login.GetPassword(customer.LoginID.Value, Request.UserHostAddress)), true);
		} else if (Request["AffiliateID"].ToNullableInt() != null){
			var affiliate = Netpay.Bll.Affiliates.Affiliate.Load(Request["AffiliateID"].ToNullableInt().GetValueOrDefault());
			//string projectUrl = (Netpay.Infrastructure.Application.IsProduction ? "https://partners.netpay-intl.com/Login.aspx" : "http://localhost/PartnerControlPanel/Login.aspx");
            Response.Redirect(WebUtils.CurrentDomain.PartnersUrl + string.Format("Login.aspx?login=outer&mail={0}&username={1}&userpassword={2}", affiliate.Login.EmailAddress, affiliate.Login.UserName, Netpay.Infrastructure.Security.Login.GetPassword(affiliate.LoginID.Value, Request.UserHostAddress)), true);
		}
 		base.OnLoad(e);
	}
</script>