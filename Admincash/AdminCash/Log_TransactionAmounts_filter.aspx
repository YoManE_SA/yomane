<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<script runat="server">
	Sub Page_Load()
		fdtrControl.FromDateTime = Date.Today.AddDays(-1)
		fdtrControl.ToDateTime = Date.Today.AddDays(1).AddSeconds(-1)
		ddlEventType.Items.Add("")
		dbPages.LoadListBox(ddlEventType, "SELECT Name, TransAmountType_id AS ID FROM List.TransAmountType", Nothing)
	End Sub
</script>

<asp:Content runat="server" ContentPlaceHolderID="body">
	<form runat="server" action="Log_TransactionAmounts_data.aspx" method="get" target="frmBody" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
	<input type="hidden" name="from" value="menu" />
	<input type="hidden" name="ShowCompanyID" id="ShowCompanyID" />
	<input type="hidden" name="isAllCompanys" />

	<h1><asp:label ID="Label1" runat="server" /> Amounts<span> - Filter</span></h1>
	<table class="filterNormal">
		<tr>
			<td colspan="2" style="padding-left:10px;">
				<Uc1:FilterMerchants id="FMerchants" ClientField="ShowCompanyID" SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
				 INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2" class="filter indent">Amount Details</th></tr>
		<tr>
			<td colspan="2">
				<Uc1:FilterDateTimeRange Title="" ID="fdtrControl" runat="server" />
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>ID</td>
			<td><input type="text" style="background-color:#f5f5f5;" name="ID" /></td>
		</tr>
		<tr>
			<td>TransID</td>
			<td><input type="text" style="background-color:#f5f5f5;" name="TransID" /></td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
			<td colspan="2">Event Type<br /><asp:DropDownList runat="server" ID="ddlEventType" style="width:200px;" /></td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			 <td colspan="2"><input type="checkbox" name="SQL2" value="1" checked="checked" class="option" />Use SQL2 when possible</td>
		</tr>
		<tr>
			<td class="indent">
				<select name="iPageSize">
				<option value="20" selected="selected">20 Rows/page</option>
				<option value="50">50 Rows/page</option>
				<option value="75">75 Rows/page</option>
				<option value="100">100 Rows/page</option>
				</select>
			</td>
			<td class="indent" align="right" nowrap="nowrap">
				<input type="submit" class="buttonFilter" value="Search" />
			</td>
		</tr>
	</table>
	</form>
</asp:Content>