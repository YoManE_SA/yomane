<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20080512 Tamir
	'
	' Security management
	'
	Dim nUser As Integer, sUser As String

	Sub LoadCheckboxes()
		Do While cblGroups.Items.Count > 0
			cblGroups.Items.Remove(cblGroups.Items(0))
		Loop
		Dim sSQL As String = "SELECT tblSecurityUserGroup.ID, '&nbsp; &nbsp; &nbsp; &nbsp; <span style=""font-weight:bold;width:100px;"">'+sg_Name+'</span> '+sg_Description Name, sug_IsMember FROM tblSecurityUserGroup INNER JOIN tblSecurityGroup ON tblSecurityUserGroup.sug_Group=tblSecurityGroup.ID WHERE sug_User=" & dbPages.TestVar(Request("ID"), 1, 0, 0) & " ORDER BY sug_IsMember DESC, sg_Name"
		Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
		Do While drData.Read()
			cblGroups.Items.Add(New ListItem(drData("Name"), drData("ID")))
			If drData("sug_IsMember") Then cblGroups.Items(cblGroups.Items.Count - 1).Selected = True
		Loop
		drData.Close()
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		nUser = dbPages.TestVar(Request("ID"), 1, 0, 0)
		sUser = dbPages.ExecScalar("SELECT IsNull(su_Name, '') FROM tblSecurityUser WHERE ID=" & nUser)
		If Not Page.IsPostBack Then 
            LoadCheckboxes()
		    Dim drData As SqlDataReader = dbPages.ExecReader("Select ID, su_Username From tblSecurityUser Order By su_Username")
		    Do While drData.Read()
                lstCopySrcUser.Items.Add(new System.Web.UI.WebControls.ListItem(drData("su_Username"), drData("ID")))
            Loop
            drData.Close()
        End If
	End Sub

	Sub btnUpdate_Click(ByVal o As Object, ByVal e As EventArgs)
		Dim sSQL As String, sIDs As String = "0"
		For Each liCheckbox As ListItem In cblGroups.Items
			If liCheckbox.Selected Then sIDs &= ", " & liCheckbox.Value
		Next
		sSQL = "UPDATE tblSecurityUserGroup SET sug_IsMember=0 WHERE ID NOT IN (" & sIDs & ") AND sug_User=" & nUser
		dbPages.ExecSql(sSQL)
		sSQL = "UPDATE tblSecurityUserGroup SET sug_IsMember=1 WHERE ID IN (" & sIDs & ") AND sug_User=" & nUser
		dbPages.ExecSql(sSQL)
		LoadCheckboxes()
	End Sub
    
	Sub btnCopy_Click(ByVal o As Object, ByVal e As EventArgs)
        Dim nID As Integer = dbPages.TestVar(Request("ID"), 1, 0, 0)
        'delete
		If nID > 0 Then dbPages.ExecSql("Delete From tblSecurityDocumentGroup Where sdg_Group=" & (-nID))
		dbPages.ExecSql("Delete From tblSecurityUserGroup Where sug_User=" & nID)
		dbPages.ExecSql("Delete From tblSecurityUserDebitCompany Where sudc_User=" & nID)
		dbPages.ExecSql("Delete From tblSecurityUserMerchant Where sum_User=" & nID)
        
        'copy
		If lstCopySrcUser.SelectedValue > 0 Then dbPages.ExecSql("Insert Into tblSecurityDocumentGroup(sdg_Group,sdg_Document,sdg_IsVisible,sdg_IsActive) Select '" & -nID & "',sdg_Document,sdg_IsVisible,sdg_IsActive From tblSecurityDocumentGroup Where sdg_Group=" & -lstCopySrcUser.SelectedValue)
		dbPages.ExecSql("Insert Into tblSecurityUserGroup(sug_User,sug_Group,sug_IsMember) Select '" & nID & "',sug_Group,sug_IsMember From tblSecurityUserGroup Where sug_User=" & lstCopySrcUser.SelectedValue)
		dbPages.ExecSql("Insert Into tblSecurityUserDebitCompany(sudc_User,sudc_DebitCompany) Select '" & nID & "',sudc_DebitCompany From tblSecurityUserDebitCompany Where sudc_User=" & lstCopySrcUser.SelectedValue)
		dbPages.ExecSql("Insert Into tblSecurityUserMerchant(sum_User,sum_Merchant) Select '" & nID & "',sum_Merchant From tblSecurityUserMerchant Where sum_User=" & lstCopySrcUser.SelectedValue)
        LoadCheckboxes()
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<form id="frmServer" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Security<span> - User: <span class="item"><%= sUser %></span></span></h1>
		<div class="bordered">
			You can grant to <%= sUser %> membership of security groups by checking respective checkboxes.
		</div>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<th style="width:54px;text-align:left;">
					Member
				</th>
				<th style="width:98px;text-align:left;">
					Group Name
				</th>
				<th style="text-align:left;">
					Group Description
				</th>
			</tr>
		</table>
		<asp:CheckBoxList ID="cblGroups" runat="server" CssClass="checkboxes" />
		<div class="lastButton">
			<asp:Button ID="btnUpdate" CssClass="buttonWhite" OnClick="btnUpdate_Click" Text="Save" runat="server" />
		</div>
		<br />
		<div class="bordered">
			Copy permission to this user from: 
			<asp:DropDownList ID="lstCopySrcUser" runat="server" />
			<asp:Button ID="btnCopy" CssClass="buttonWhite" OnClick="btnCopy_Click" Text="Copy" runat="server" />
		</div>
	</form>
</body>
</html>