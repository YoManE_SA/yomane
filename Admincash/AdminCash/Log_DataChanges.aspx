<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
	Sub Page_PreInit()
		dbPages.CurrentDSN = IIf(Request("DSN2") = "1", 2, 1)
	End Sub
	
	Dim sSQL As String, sQueryWhere As String, nFromDate As String, nToDate As String, sDateDiff As String, _
		sStatusColor As String, sAnd As String, sQueryString As String, sTmpTxt As String, sDivider As String
	Dim i As Integer, nTmpNum As Integer

	Function DisplayContent(sVar As String) As String
		DisplayContent = "<span class=""key"">" & replace(replace(dbPages.dbtextShow(sVar),"=","</span> = "),"|"," <br /><span class=""key"">")
	End Function
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)

		nFromDate	= Trim(Request("fromDate")) & " " & Trim(Request("fromTime"))
		nToDate = Trim(Request("toDate")) & " " & Trim(Request("toTime"))
		If nFromDate <> " " Then sQueryWhere &= sAnd & "DCL_Date>='" & nFromDate.ToString() & "'" : sAnd = " AND "
		If nToDate <> " " Then sQueryWhere &= sAnd & "DCL_Date<='" & nToDate.ToString() & "'" : sAnd = " AND "
		
		If Not String.IsNullOrEmpty(Request("Table")) Then sQueryWhere &= sAnd & "DCL_Table='" & Request("Table") & "'" : sAnd = " AND "
		If Not String.IsNullOrEmpty(Request("User")) Then sQueryWhere &= sAnd & "DCL_User='" & Request("User") & "'" : sAnd = " AND "
		If Not String.IsNullOrEmpty(Request("Action")) Then sQueryWhere &= sAnd & "DCL_Action='" & Request("Action") & "'" : sAnd = " AND "
		If Not String.IsNullOrEmpty(Request("Field")) Then sQueryWhere &= sAnd & "DCL_Field='" & Request("Field") & "'" : sAnd = " AND "
		If Not String.IsNullOrEmpty(Request("Important")) Then sQueryWhere &= sAnd & "DCL_IsImportant>=" & Request("Important") : sAnd = " AND "
		
		If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") Else PGData.PageSize = 20
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere

		If Request("SQL2") = "1" Then
			dbPages.CurrentDSN = 2
			Response.Write(dbPages.ShowCurrentDSN)
		Else
			dbPages.CurrentDSN = 1
		End If
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link	href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript">
		function IconVisibility(fVar) {
		
			spanObj = document.getElementById(fVar)
			if (spanObj.style.visibility == 'hidden') {
				spanObj.style.visibility = 'visible';
			}
			else {
				spanObj.style.visibility = 'hidden';
			}
		}
		function ExpandNode(fTrNum) {
			trObj = document.getElementById("trInfo" + fTrNum)
			imgObj = document.getElementById("oListImg" + fTrNum)
			if (trObj) {
				if (trObj.style.display == '') {
					imgObj.src = '../images/tree_expand.gif';	
					trObj.style.display = 'none';
				}
				else {
					imgObj.src = '../images/tree_collapse.gif';	
					trObj.style.display = '';
				}
			}
		}
	</script>
</head>

<body>
	<form id="Form1" runat="server">

		<table align="center" style="width:92%">
		<tr>
			<td id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> LOG DATA CHANGES<br />
			</td>
			<td align="right" style="font-size:11px;">
				&nbsp; <span style="background-color:#ffe1e1;">&nbsp;IMPORTANT&nbsp;</span>
				&nbsp; <span style="background-color:#6699cc;">&nbsp;</span> INSERT
				&nbsp; <span style="background-color:#66cc66;">&nbsp;</span> UPDATE
				&nbsp; <span style="background-color:#ff6666;">&nbsp;</span> DELETE
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
			<td colspan="2" style="font-size:11px; padding-bottom:4px;">
				(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');">Collapse All</a>)
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%;">
		<tr>
			<th colspan="2" style="width:30px;"><br /></th>
			<th>Date</th>
			<th>Action</th>
			<th>Table</th>
			<th>Field</th>
			<th>Old Value</th>
			<th>New Value</th>
			<th>Record ID</th>
			<th>DB User</th>
		</tr>
		<%
		sSQL = "SELECT *, Cast(DCL_Old AS varbinary(200)) OldBinary, Cast(DCL_New as varbinary(200)) NewBinary FROM tblDataChangeLog " & sQueryWhere & " ORDER BY ID DESC"
		PGData.OpenDataset(sSQL)
		Dim sOld, sNew, sOldShort, sNewShort, sAction, sBGColor As String
		While PGData.Read()
			i += 1
			sAction = PGData("DCL_Action")
			Select Case sAction.Trim.ToUpper
				Case "INSERT" : sStatusColor = "#6699cc"
				Case "DELETE" : sStatusColor = "#ff6666"
				Case Else : sStatusColor = "#66cc66"
			End Select
			If PGData("DCL_Field").ToString.EndsWith("256") Then
				If Convert.IsDBNull(PGData("DCL_Old")) Then
					sOld = "&nbsp;"
					sOldShort = "&lt; NULL &gt;"
				Else
					sOld = "0x"
					For i As Integer = 0 To CType(PGData("OldBinary"), Byte()).Length - 1
						sOld &= (CType(PGData("OldBinary"), Byte())(i) And &HFF).ToString("X")
					Next
					sOldShort = "&lt; BINARY DATA &gt;"
				End If
				If Convert.IsDBNull(PGData("DCL_New")) Then
					sNew = "&nbsp;"
					sNewShort = "&lt; NULL &gt;"
				Else
					sNew = "0x"
					For i As Integer = 0 To CType(PGData("NewBinary"), Byte()).Length - 1
						sNew &= (CType(PGData("NewBinary"), Byte())(i) And &HFF).ToString("X")
					Next
					sNewShort = "&lt; BINARY DATA &gt;"
				End If
			Else
				If Convert.IsDBNull(PGData("DCL_Old")) Then
					sOld = "&lt; NULL &gt;"
				Else
					sOld = Server.HtmlEncode(PGData("DCL_Old"))
				End If
				If sOld.Length > 30 Then sOldShort = sOld.Substring(0, 30) & " ..." Else sOldShort = sOld
				If Convert.IsDBNull(PGData("DCL_New")) Then
					sNew = "&lt; NULL &gt;"
				Else
					sNew = Server.HtmlEncode(PGData("DCL_New"))
				End If
				If sNew.Length > 30 Then sNewShort = sNew.Substring(0, 30) & " ..." Else sNewShort = sNew
			End If
			sBGColor = IIf(PGData("DCL_IsImportant"), "#ffe1e1", String.Empty)
			%>
			<tr style="background-color:<%=sBGColor%>;" onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='<%=sBGColor%>';">
				<td><img onclick="ExpandNode('<%=i%>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle"><br /></td>
				<td><span style="background-color:<%= sStatusColor %>;">&nbsp;</span></td>
				<td nowrap="nowrap"><%=CType(PGData("DCL_Date"), DateTime).ToString("dd/MM/yy HH:mm:ss")%></td>
				<td nowrap="nowrap"><%=sAction%></td>
				<td nowrap="nowrap"><%=PGData("DCL_Table")%></td>
				<td nowrap="nowrap"><%=PGData("DCL_Field")%></td>
				<td nowrap="nowrap"><%=sOldShort%></td>
				<td nowrap="nowrap"><%=sNewShort%></td>
				<td nowrap="nowrap"><%=PGData("DCL_ID")%></td>
				<td nowrap="nowrap"><%=PGData("DCL_User")%></td>
			</tr>
			<tr id="trInfo<%=i%>" style="display:none;background-color:<%=sBGColor%>;">
				<td colspan="2"></td>
				<td colspan="8">
					<table class="formNormal" style="width:100%; border-top:1px dashed #e2e2e2;">
					<tr><td height="10"></td></tr>
					<tr><td><span class="dataHeading">Old Value</span></td></tr>
					<tr><td><%=sOld%></td></tr>
					<tr><td><span class="dataHeading">New Value</span></td></tr>
					<tr><td><%=sNew%></td></tr>
					<tr><td height="10"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td height="1" colspan="10" bgcolor="silver"></td></tr>
			<%
		End While
		PGData.CloseDataset()
		%>
		</table>
		<script language="JavaScript">
			function ExpandAll(fDisplay, fImgStatus) {
				for(i = 0; i <= <%= i %>; i++) {
					trObj = document.getElementById("trInfo" + i)
					imgObj = document.getElementById("oListImg" + i)
					if (trObj) {
						imgObj.src = '../images/tree_' + fImgStatus + '.gif';
						trObj.style.display = fDisplay;
					}
				}
			}
			<% If i=1 Then Response.Write("ExpandNode(1);") %>
		</script>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" align="middle"> ADD TO FILTER
		</div>
		
	</form>
	
	<table align="center" style="width:92%">
	<tr>
		<td>
			<UC:Paging runat="Server" id="PGData" PageID="PageID" />
		</td>
	</tr>
	</table>
		
		
</body>
</html>
