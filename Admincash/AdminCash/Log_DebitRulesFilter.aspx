<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = Date.Now.ToShortDateString
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
	
        dsDebitCompany.ConnectionString = dbPages.DSN
        dsDebitCompany.SelectCommand = "SELECT * FROM tblDebitCompany" & _
        " WHERE" & IIf(Security.IsLimitedDebitCompany, " DebitCompany_ID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "')) AND ", String.Empty) & _
        " DebitCompany_ID IN (SELECT DISTINCT dr_DebitCompany FROM tblDebitRule) ORDER BY dc_name"
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
	<form action="Log_DebitRules.aspx" runat="server" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />
	<h1><asp:label ID="lblPermissions" runat="server" /> Rules &amp; Notifications<span> - Filter</span></h1>

	<table class="filterNormal">
		<tr><th>Debit Company</th></tr>
		<tr>
			<td>
				<asp:Repeater ID="Repeater1" DataSourceID="dsDebitCompany" runat="server">
					<HeaderTemplate><select name="DebitCompany"><option></option><option value="0">(none)</option></HeaderTemplate>
					<ItemTemplate>
						<asp:Literal ID="Literal1" Text='<%#"<option value=""" & Eval("DebitCompany_ID") & """>" & Eval("dc_name") & "</option>"%>' runat="server" />
					</ItemTemplate>
					<FooterTemplate></select></FooterTemplate>
				</asp:Repeater>
				<asp:SqlDataSource ID="dsDebitCompany" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Debit Rule</th></tr>
		<tr>
			<td>Rule No.</td>
			<td colspan="2">&nbsp;<input name="Rule" class="medium grayBG" value="" /></td>
		</tr>
		<tr>
			<td rowspan="3" style="vertical-align:top;padding-top:5px;">Rule Type</td>
			<td>
				<input type="Radio" name="RuleType" class="option" value="" checked="checked" />All
			</td>
			<td>
				<input type="Radio" name="RuleType" class="option" value="Block" />Block
			</td>
		</tr>
		<tr>
			<td>
				<input type="Radio" name="RuleType" class="option" value="---" />None
			</td>
			<td>
				<input type="Radio" name="RuleType" class="option" value="Warning" />Warning
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="Radio" name="RuleType" class="option" value="CHB" />Monthly CHB
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Record Type</th></tr>
		<tr>
			<td>
				<input type="Radio" name="ShowChecks" class="option" value="0" checked="checked" />Actions Only<br />
				<input type="Radio" name="ShowChecks" class="option" value="1" />Actions &amp; Routines
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td>
				<select name="iPageSize">
					<option value="20" selected="selected">20 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td align="right"><input type="submit" name="Action" value="SEARCH"><br /></td>
		</tr>
	</table>
	</form>
</asp:Content>