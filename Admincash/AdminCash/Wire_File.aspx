<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%
	Dim sUsername As String = dbPages.DBText(Security.Username), sFileName As String = String.Empty
	Dim sSQL As String, nID, nWireMoneyID As Integer
	nWireMoneyID = dbPages.TestVar(Request.QueryString("WireMoneyID"), 1, 0, 0)
	If Not String.IsNullOrEmpty(Request.QueryString("DeleteFileID")) Then
		nID = dbPages.TestVar(Request.QueryString("DeleteFileID"), 1, 0, 0)
		If nID > 0 Then
            sFileName = Domain.Current.MapPrivateDataPath("Wires/WireFiles/" & dbPages.ExecScalar("SELECT wmf_FileName FROM tblWireMoneyFile WHERE ID=" & nID))
			If Not String.IsNullOrEmpty(sFileName) Then
				If My.Computer.FileSystem.FileExists(sFileName) Then My.Computer.FileSystem.DeleteFile(sFileName)
			End If
			sSQL = "INSERT INTO tblWireMoneyLog(WireMoney_id, wml_description, wml_user) VALUES" & _
			" (" & nWireMoneyID & ", Left('File deleted: " & dbPages.DBText(sFileName) & "', 250)," & _
			" Left('" & sUsername & "', 50));DELETE FROM tblWireMoneyFile WHERE ID=" & nID
			dbPages.ExecSql(sSQL)
		End If
		Response.Redirect("wire_commentUpd.asp?WireMoney_id=" & nWireMoneyID)
	End If

    'Dim nParseLog As String = ""
	'Dim nParseResults As Byte = 0
    'If Request.QueryString("Reparse") = "1" Then
    '       nParseResults = dbPages.ParsePDF(Domain.Current.MapPrivateDataPath("Wires/WireFiles/" & dbPages.TestVar(dbPages.ExecScalar("Select wmf_FileName From tblWireMoneyFile Where ID=" & dbPages.TestVar(Request("ID"), 0, -1, 0)), -1, "")), nWireMoneyID, nParseLog)
    '	dbPages.ExecSql("Update tblWireMoneyFile Set wmf_ParseResult=" & nParseResults & ", wmf_ParseLog='" & nParseLog.Replace("'", "''") & "' Where ID=" & dbPages.TestVar(Request("ID"), 0, -1, 0))
    '	Response.Redirect("wire_commentUpd.asp?WireMoney_id=" & nWireMoneyID)
    'End If	

	nWireMoneyID = dbPages.TestVar(Request.Form("WireMoneyID"), 1, 0, 0)
	Dim ieFiles As IEnumerator = Request.Files.GetEnumerator()
	For i As Integer = 0 To Request.Files.Count - 1
		sFileName = Request.Files(i).FileName
		If sFileName.Contains("\") Then sFileName = sFileName.Substring(sFileName.LastIndexOf("\") + 1)
		Dim sExt As String = sFileName.Substring(sFileName.LastIndexOf(".")).ToLower
		If ".gif.jpg.jpe.jpeg.png.tif.tiff.pdf.xls.xlsx.doc.docx.csv.txt.".Contains(sExt + ".") Then
            Request.Files(i).SaveAs(Domain.Current.MapPrivateDataPath("Wires/WireFiles/" & sFileName))
		End If
        'If sExt = ".pdf" Then nParseResults = dbPages.ParsePDF(Domain.Current.MapPrivateDataPath("Wires/WireFiles/" & sFileName), nWireMoneyID, nParseLog)
		Dim sDescription As String = dbPages.DBText(Request.Form("DescriptionNew"))
        sSQL = "INSERT INTO tblWireMoneyFile (wmf_WireMoneyID, wmf_FileName," & _
        " wmf_Description, wmf_User, wmf_ParseLog, wmf_ParseResult) VALUES (" & nWireMoneyID & ", Left('" & dbPages.DBText(sFileName) & "'," & _
        " 100), Left('" & sDescription & "', 350), Left('" & sUsername & "', 40), '', 0)" & _
        ";INSERT INTO tblWireMoneyLog(WireMoney_id, wml_description, wml_user) VALUES" & _
        " (" & nWireMoneyID & ", Left('File added: " & dbPages.DBText(sFileName) & "', 250)," & _
        " Left('" & sUsername & "', 50))"
		dbPages.ExecSql(sSQL)
	Next
	Response.Redirect("wire_commentUpd.asp?WireMoney_id=" & nWireMoneyID)
%>