<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_controls.asp"-->
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
</head>
<body class="itextM2">
<table class="formNormal" style="width:100%; padding:0; margin:0;">
<tr>
	<th colspan="2"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></th>
	<th>Date<br /></th>
	<th>Amount<br /></th>
	<th>Ins<br /></th>
	<th>Payment Method<br /></th>
	<th>Location<br /></th>
</tr>
<tr><td height="3"></td></tr>
<%
nComCount = 0
sWhere = ""

sCompanyID = request("companyID")
If sCompanyID <> "" Then sWhere = "(companyID = " & sCompanyID & ")"
If sWhere <> "" Then sWhere = "WHERE " & sWhere
	
sSQL="SELECT TOP 5 TransSource_id, InsertDate, Amount, Currency, PayID, DeniedStatus, PaymentMethod_id, PaymentMethodDisplay, Payments, ID, CreditType " &_
"FROM tblCompanyTransPass " & sWhere & " ORDER BY ID DESC"
set rsData = oledbData.execute(sSQL)
If not rsData.EOF then
	
	Do until rsData.EOF OR nComCount=25
		nComCount = nComCount+1
		sPayStatusText = ""
		sTdFontColor = "#000000"
		sTdBkgColorFirst = ""
		
		If rsData("PaymentMethod_id")=1 OR rsData("PaymentMethod_id")=2 then
		    'Credit cards and eChecks
			if int(rsData("CreditType"))=0 then
				sTrBkgColor="#F6FEF6"
				sTdBkgColorFirst="#66cc66"
				sRespong="Refund"
				sTdFontColor="red"
			else
				if rsData("deniedstatus")=0 then
					if nTypeCredit="8" then
						sTdBkgColorFirst="#66cc66"
						sTrBkgColor="#F6FEF6"
						sTdFontColor="#003366"
					else
						sTdBkgColorFirst="#66cc66"
						sTrBkgColor="#F6FEF6"
					end if
				else
					sTdBkgColorFirst="#66cc66"
					sTrBkgColor="#fff5e1"
					sPayStatusText="- - -"
				end if
			end if
		Elseif rsData("PaymentMethod_id")="3" then
		    'System transactions
		    If int(rsData("TransSource_id")) = 21 OR int(rsData("TransSource_id")) = 3 Then 
				bShowCheckbox = true
				sTrBkgColor="f5f5f5"
				sTdBkgColorFirst="#484848"
				sTdFontColor="#cc0000"
			End if
		Elseif rsData("PaymentMethod_id")="4" then 
		    'Admin transactions
			sTdBkgColorFirst="#003399"
			sTrBkgColor="#eeeffd"
			if int(rsData("CreditType"))=0 then
				sTdFontColor="#cc0000"
			end if
		End if
		
		If Instr(trim(rsData("payID")),";X;")>0 Then
			sPayStatusText="Archive"
		ElseIf int(replace(trim(rsData("payID")),";",""))=0 Then
			sPayStatusText="Unsettled"
		ElseIf Instr(trim(rsData("payID")),";0;")=0 Then
			sPayStatusText="Settled"
		ElseIf Instr(trim(rsData("payID")),";0;")>0 Then
			sPayStatusText="Partial Settled"
		End If
		%>
		<tr bgcolor="<%= sTrBkgColor %>">
			<td width="6" bgcolor="<%= sTdBkgColorFirst %>"></td>
			<td width="3"></td>
			<td class="txt11" dir="ltr" align="right" nowrap style="color:<%= sTdFontColor %>;"><%= FormatDatesTimes(rsData("InsertDate"),1,1,0) %><br /></td>
			<td class="txt11" align="right" nowrap dir="ltr" style="color:<%= sTdFontColor %>;"><%=FormatCurrWCT(rsData("Currency"), rsData("Amount"), rsData("CreditType"))%></td>
			<td class="txt11" align="right" style="color:<%= sTdFontColor %>;"><%= rsData("Payments") %><br /></td>
			<td class="txt11" align="right" style="color:<%= sTdFontColor %>;"><%= rsData("PaymentMethodDisplay") %><br /></td>
			<td class="txt11" align="right" style="color:<%= sTdFontColor %>;"><%= sPayStatusText %><br /></td>
		</tr>
		<%
	rsData.movenext
	loop
end if
rsData.close
Set rsData = nothing
call closeConnection()
%>
</table>
</body>
</html>