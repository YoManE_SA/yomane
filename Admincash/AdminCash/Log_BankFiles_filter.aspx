<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = Date.Today
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
	<form runat="server" action="Log_BankFiles_data.aspx" target="frmBody" method="post" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" id="Source" name="Source" value="Filter" />
	<h1><asp:label ID="lblPermissions" runat="server" /> EPA/CHB<span> - Filter</span></h1>
	<table class="filterNormal">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>	
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><td>Type</td></tr>
		<tr>
			<td>
				<asp:DropDownList ID="ddlLogType" runat="server" Width="150">
					<asp:ListItem Text="EPA" Value="EPA" Selected="True"></asp:ListItem>
					<asp:ListItem Text="CHB" Value="CHB"></asp:ListItem>
				</asp:DropDownList>
			</td>	
		</tr>
		<tr><td>Status</td></tr>
		<tr>
			<td>
				<asp:DropDownList ID="ddActionType" runat="server" Width="150">
					<asp:ListItem Text="" Value=""></asp:ListItem>
					<asp:ListItem Text="Error" Value="0"></asp:ListItem>
					<asp:ListItem Text="File Uploaded" Value="5"></asp:ListItem>
					<asp:ListItem Text="New - Task started" Value="1"></asp:ListItem>
					<asp:ListItem Text="New - Task complete" Value="2"></asp:ListItem>
					<asp:ListItem Text="Pending - Task started" Value="3"></asp:ListItem>
					<asp:ListItem Text="Pending - Task complete" Value="4"></asp:ListItem>
				</asp:DropDownList>
			</td>	
		</tr>
		<tr><td>Original file name</td></tr>
		<tr>
			<td>
				<input type="text" name="txtOriginalFileName" class="grayBG" value="" />
			</td>	
		</tr>
		<tr><td>Stored file name</td></tr>
		<tr>
			<td>
				<input type="text" name="txtStoredFileName" class="grayBG" value="" />
			</td>	
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<td colspan="2">
				<input type="checkbox" name="SQL2" value="1" checked="checked" class="option"/>Use SQL2 when possible
			</td>
		</tr>
		<tr><td height="6"></td></tr>
		<tr>	
			<td>
				<select name="iPageSize" class="grayBG">
					<option value="10">10 rows/page</option>
					<option value="25" selected="selected">25 rows/page</option>
					<option value="50">50 rows/page</option>
					<option value="100">100 rows/page</option>
				</select>
			</td>
			<td style="text-align:right;">
				<input type="submit" name="Action" value="SEARCH"/>
			</td>
		</tr>
	</table>
	</form>
</asp:Content>