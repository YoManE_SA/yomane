<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20080512 Tamir
	'
	' Security management
	'
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		Dim nDeleteGroup As Integer = dbPages.TestVar(Request("DeleteGroup"), 1, 0, 0)
		If nDeleteGroup > 0 Then dbPages.ExecSql("DELETE FROM tblSecurityGroup WHERE ID=" & nDeleteGroup)
		dsGroups.ConnectionString = dbPages.DSN()
	End Sub

	Sub ClearBody()
		Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
		Response.Write("parent.frmBody.location.replace('common_blank.htm');")
		Response.Write("</s" & "cript>")
	End Sub

	Sub ReloadPage()
		Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
		Response.Write("location.replace('security_groups.aspx');")
		Response.Write("</s" & "cript>")
	End Sub

	Sub OpenGroup(ByVal nGroup As Integer)
		Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
		Response.Write("parent.frmBody.location.replace('security_group.aspx?ID=" & nGroup & "');")
		Response.Write("</s" & "cript>")
	End Sub

	Sub gvGroups_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		OpenGroup(gvGroups.SelectedValue)
	End Sub

	Sub gvGroups_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs)
		ClearBody()
		ReloadPage()
	End Sub

	Sub btnAddGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		If String.IsNullOrEmpty(txtNewGroup.Text) Then
			lblAddGroup.Text = "Specify new group name!"
		Else
			lblAddGroup.Text = String.Empty
			dbPages.ExecSql("INSERT INTO tblSecurityGroup (sg_Name, sg_Description) VALUES ('" & dbPages.DBText(txtNewGroup.Text) & "', '" & dbPages.DBText(txtNewGroup.Text) & "')")
			Dim nGroup As Integer = dbPages.TestVar(dbPages.ExecScalar("SELECT Max(ID) FROM tblSecurityGroup WHERE sg_Name='" & dbPages.DBText(txtNewGroup.Text) & "'"), 1, 0, 0)
			txtNewGroup.Text = String.Empty
			gvGroups.DataBind()
		End If
	End Sub

	Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Response.Redirect("security_groups.aspx")
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security - Groups</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body class="menu">
	<span class="toolbar">
		<a class="toolbar" href="security_users.aspx" onclick="parent.frmBody.location.replace('common_blank.htm');">Users</a>
		<span class="toolbarSeparator">|</span>
		<a class="toolbarCurrent" href="security_groups.aspx" onclick="parent.frmBody.location.replace('common_blank.htm');">Groups</a>
		<span class="toolbarSeparator">|</span>
		<a class="toolbar" href="security_documents.aspx" onclick="parent.frmBody.location.replace('common_blank.htm');">Documents</a>
	</span>
	<script language="javascript" type="text/javascript">
		function DeleteRecord(nID, sTitle, sDescription)
		{
			if (confirm("The following group is to be deleted from the system:\n\n"+sTitle+" ("+sDescription+")\n\nDo you really want to delete this group?"))
				document.location.replace("security_groups.aspx?DeleteGroup="+nID);
		}
	</script>
	<form id="frmServer" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> SECURITY<span> - GROUPS</span></h1>
		<div class="bordered">
			<span style="float:right;padding-bottom:1px;">
				<asp:Button ID="btnRefresh" CssClass="buttonWhite" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
			</span>
			Add new group
			<asp:TextBox ID="txtNewGroup" CssClass="text" runat="server" />
			<asp:Button ID="btnAddGroup" CssClass="buttonWhite" runat="server" Text="Add" OnClick="btnAddGroup_Click" />
			<asp:Label ID="lblAddGroup" runat="server" ForeColor="red" />
		</div>
		<asp:GridView ID="gvGroups" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="dsGroups" AllowPaging="False" AllowSorting="False" SelectedRowStyle-CssClass="rowSelected" OnSelectedIndexChanged="gvGroups_SelectedIndexChanged" OnRowDeleted="gvGroups_RowDeleted" Width="100%">
			<Columns>
				<asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" Visible="False" />
				<asp:BoundField DataField="sg_Name" HeaderText="Group&nbsp;Name" HeaderStyle-HorizontalAlign="left" ControlStyle-CssClass="medium" />
				<asp:BoundField DataField="sg_Description" HeaderText="Group Description" HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="100%" ControlStyle-CssClass="text"/>
				<asp:CheckBoxField DataField="sg_seeUnmanaged" HeaderText="Sees<br/>Unmanaged" ItemStyle-CssClass="centered" />
				<asp:CheckBoxField DataField="sg_IsActive" HeaderText="Active" ItemStyle-CssClass="centered" />
				<asp:CheckBoxField DataField="sg_IsLogged" HeaderText="Logged" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="UserCount" HeaderText="Users" ItemStyle-CssClass="centered" ReadOnly="True"/>
				
                <asp:TemplateField HeaderText="Delete" >
					<ItemTemplate>
						<input type="button" onclick="DeleteRecord(<%# Eval("ID") %>, '<%# Eval("sg_Name") %>', '<%# Eval("sg_Description") %>');" value="Delete" class="buttonWhite buttonDelete">
					</ItemTemplate>
				</asp:TemplateField>
                

                
                
                <asp:CommandField HeaderText="Actions" ButtonType="Button" ItemStyle-CssClass="buttons" ControlStyle-CssClass="buttonWhite" ShowDeleteButton="False" ShowEditButton="True" ShowSelectButton="True" HeaderStyle-CssClass="actions" SelectText="Users" UpdateText="Save" />
			</Columns>
			<SelectedRowStyle CssClass="rowSelected" />
		</asp:GridView>
		<asp:SqlDataSource ID="dsGroups" runat="server" ProviderName="System.Data.SqlClient"
		 SelectCommand="SELECT ID, sg_Name, sg_Description, sg_IsActive, sg_IsLogged, sg_seeUnmanaged,
		  (SELECT Count(*) FROM tblSecurityUserGroup WHERE sug_Group=tblSecurityGroup.ID AND sug_IsMember=1) UserCount FROM tblSecurityGroup ORDER BY sg_Name"
		    UpdateCommand="UPDATE tblSecurityGroup SET sg_Name = IsNull(@sg_Name, ''), sg_Description = IsNull(@sg_Description, ''),
		     sg_IsActive = @sg_IsActive, sg_IsLogged = @sg_IsLogged, sg_seeUnmanaged = @sg_seeUnmanaged WHERE ID = @ID">
			<DeleteParameters>
				<asp:Parameter Name="ID" Type="Int32" />
			</DeleteParameters>
			<UpdateParameters>
				<asp:Parameter Name="sg_Name" Type="String" />
				<asp:Parameter Name="sg_Description" Type="String" />
				<asp:Parameter Name="sg_IsActive" Type="Boolean" />
				<asp:Parameter Name="sg_IsLogged" Type="Boolean" />
				<asp:Parameter Name="sg_seeUnmanaged" Type="Boolean" />
				<asp:Parameter Name="ID" Type="Int32" />
			</UpdateParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>