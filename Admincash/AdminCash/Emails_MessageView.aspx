﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeFile="Emails_MessageView.aspx.cs"
    Inherits="Emails_MessageView" %>

<%@ Register Assembly="Netpay.Web" Namespace="Netpay.Web.Controls" TagPrefix="Netpay" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <title>Forms - Posts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <link href="../js/jquery-ui-1.8.10.custom/css/custom-theme/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
    <script src="../js/Emails.js" type="text/javascript"></script>
    <link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
    <style type="text/css" media="all">
    	#placeHolderMerchantChild { display: inline-block; height: 25px; margin: 0px; padding: 0px; width: auto; }
    	.chb { display: none; font-size: 11px; margin-bottom: 2px; }
    	.chb2 { display: block; font-size: 10px; margin-bottom: 2px; }
    	#updp { display: inline-block; width: auto; }
    	#placeHolder_buttons { float: right; padding-bottom: 1px; white-space: nowrap; }
    	#updp_deleteAssignedMail { display: inline-block; margin: 0px 0px 0px 0px; padding: 0px 0px; border: none; }
    	#ddl_merchant { width: 200px; margin-bottom: 2px; }
    	#txt_merchant_text { width: 220px; margin-bottom: 2px; }
    	#lbt_deleteAssignedMail { color: Red; text-decoration: none; cursor: pointer; font-size: 10px; border:none; }
    	#lbt_HistoryMerchant { width: auto; padding: 1px 2px; cursor: pointer; }
    	#display_ddl_merchant { cursor: pointer; }
    	#seperator_userInfo { }
    	#btn_userInfo { cursor: pointer; }
    	#assing_mail { display: none; }
    	.closeme { float: right; color: Red; cursor: pointer; border: 1px solid #000; background-color: #fff; font-size: 10px; margin: -3px 0px 0px 0px; padding: 0px 2px; }
    	/*assing_mailD Using in js (Dont remove)*/
    	.assing_mailD { position: absolute; border: 1px solid #000; padding: 7px; width: 250px; background-color: #fafafa; }
    	#tbl_History tr:hover { background-color: #f0f0f0; }
    	.rep_span_Conversation { padding-left: 3px; text-align: left; }
		.ui-autocomplete { text-align:left!important; }
    </style>
</head>
<body style="padding: 0px 30px 0px 18px;">
    <script type="text/javascript">
        /*Old FUNC TAMIR*/
        function OpenBlacklistURL(sMerchantName, sFirstName, sLastName, sPhone, sFax, sCellular, sMail, sURL) {
            sOpenURL = "merchant_blacklist.aspx?FilterType=OR&MerchantID=&MerchantNumber=&LegalNumber=&IDNumber=";
            sOpenURL += "&MerchantName=" + sMerchantName;
            sOpenURL += "&FirstName=" + sFirstName;
            sOpenURL += "&LastName=" + sLastName;
            sOpenURL += "&Phone=" + sPhone;
            sOpenURL += "&Fax=" + sFax;
            sOpenURL += "&Cellular=" + sCellular;
            sOpenURL += "&Mail=" + sMail;
            sOpenURL += "&URL=" + sURL;
            var winBlacklist = window.open(sOpenURL, "fraBlacklist", "top=100,left=100,width=" + (screen.width - 200) + ",height=" + (screen.height - 200) + ",scrollbars=auto,resizable");
        }
    </script>
    <form id="frmPost" runat="server" enableviewstate="false">
    <asp:HiddenField ID="hf_threadID" ClientIDMode="Static" runat="server" Value="" />
    <asp:HiddenField ID="hf_selectedstatus" ClientIDMode="Static" runat="server" Value="" />
    <asp:HiddenField ID="hf_rbl_merchant" ClientIDMode="Static" runat="server" Value="" />
    <asp:HiddenField ID="hf_msgID" ClientIDMode="Static" runat="server" Value="" />
    <asp:ScriptManager runat="server" />
    <Netpay:ScriptManager runat="server" />
    <div>
        <h1 style="background-color: rgb(238,238,238); padding: 5px 0px;">
            <span id="placeHolder_buttons">
                <!--start assing mail-->
                <asp:UpdatePanel ID="updp" ClientIDMode="Static" runat="server">
                    <ContentTemplate>
                        <span id="placeHolderMerchant" style='display: <%#MerchantId == null ? "" : "none;"%>' >
                            <span id="placeHolderMerchantChild"><a style="font-size:13px;" id="display_ddl_merchant" onclick="display_assing_merchant();">Assign Merchant</a></span>
                            <span id="assing_mail">
                                <span id="close_me" class="closeme">X</span>
								<span style="font-size:10px;">Type merchant name to assign<br />to this email message</span>
                                <asp:DropDownList ID="ddl_merchant" ClientIDMode="Static" CssClass="rbl_merchant" runat="server" />
                                <br />
                                <Netpay:AutoComplete ID="txt_merchant" IsMultiselect="false" Function="GetMerchantNameAutoComplete" ClientIDMode="Static" Width="35" class="AutoCompleteStyle" runat="server" /><br />
                                <br />
                                <asp:CheckBox ID="chbAssignAddEmail" CssClass="chb" ClientIDMode="Static" runat="server" Text="Remember for future messages" TextAlign="Right" />
                                <asp:CheckBox ID="chbAssignUpdateHistory" CssClass="chb2" ClientIDMode="Static" runat="server" Text="Include all past emails messages" TextAlign="Right" />
                                <asp:Button ID="btn_assing_merchant" runat="server" class="buttonWhite buttonSemiWide" Text="SUBMIT" OnClientClick="return checkSelectedRB();" OnClick="ddl_merchant_Changed" />
                            </span>
                            <span id="seperator_HistoryMerchant">|</span> 
                        </span>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btn_assing_merchant" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
                <!--end assing mail-->
                <a style="font-size:13px;" id="lbt_HistoryMerchant" onclick="return ShowHistoryFromCurentUser('<%=Server.UrlEncode(UserMailForAllHistory) %>','0');">Mailbox History</a>
				<span id="seperator_userInfo">|</span>
				<a style="font-size:13px;" id="btn_userInfo" maxlength="<%=MerchantId.ToString() %>" onclick="return redirectTOmarchent('<%=MerchantId.ToString() %>');">Merchant Info</a>
                <img src="/NPCommon/Images/iconNewWinRight.gif" /> &nbsp;
            </span>
            <asp:Label ID="lblPermissions" runat="server" /><span style="color: #2566ab; font-weight: bold;">
                Emails - </span><span><%=GetMerchantActiveStatusCompanyName%></span>
            <!--Start delete(button) Assigned Mail-->
            <asp:UpdatePanel ID="updp_deleteAssignedMail" runat="server" ClientIDMode="Static">
                <ContentTemplate>
                    <asp:ImageButton ID="lbt_deleteAssignedMail" ImageUrl="/NPCOmmon/Images/iconB_delete.png"
                        OnClick="lbt_deleteAssignedMail_Click" runat="server" ToolTip="Unassign Merchant"
                        OnClientClick="return (confirm('Are you sure ?!'))" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="lbt_deleteAssignedMail" EventName="click" />
                </Triggers>
            </asp:UpdatePanel>
            <!--End delete(button) Assigned Mail-->
        </h1>
        <br />
        <br />
        <table cellpadding="2" cellspacing="0" border="0" width="100%">
            <tr>
                <td valign="top" style="padding-right: 40px;">
                    <table cellpadding="1" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td valign="top">
                                <asp:Label ID="lblSubject" runat="server" Font-Size="14px" Font-Bold="True" /><br />
                                <asp:Label ID="lblForm" runat="server" Font-Size="16px" /><br />
                                <br />
                            </td>
                            <td align="right" valign="top" style="white-space:nowrap;">
                                <asp:ImageButton ID="btn_ReplyToMerchant" AlternateText="Reply" ToolTip="Reply" runat="server"
                                    ClientIDMode="Static" text="Reply" ImageUrl="/NPCommon/Images/btnReply.gif" OnClick="ReplyToMerchant_Click" />
                                <asp:ImageButton ID="btnDelete" runat="server" AlternateText="Delete" ToolTip="Delete"
                                    Text="Delete" ImageUrl="/NPCommon/Images/btnDelete.gif" OnClick="btnDelete_Click"
                                    OnClientClick="return confirm('Do you really want to delete this post ?!');" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top">
                                SENT:
                                <asp:Label ID="lblDateTime" runat="server" />
                                &nbsp;&nbsp; TO:
                                <asp:Label ID="lblTo" runat="server" />
                                &nbsp;&nbsp; <span id="lbl_CcTitle" runat="server">CC:
                                    <asp:Label ID="lbl_CC" runat="server" /></span>
                            </td>
                        </tr>
                        <tr style="display: <%=rptAttachments.Items.Count == 0 ? "none;" : ""%>">
                            <td colspan="2" valign="top">
                                <span>ATTACHMENTS</span>
                                <asp:Button ID="btn_addNewMarchant" runat="server" Visible="false" Text="Add New Merchant"
                                    Style='border: 1px solid #000; margin: 0px 5px; cursor: pointer;' />
                                <asp:Repeater runat="server" ID="rptAttachments">
                                    <HeaderTemplate>
                                        <table class="cleartbl" cellpadding="0" cellspacing="0" width="100%">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td width="80%">
                                                <asp:HyperLink ID="Hyperlink1" runat="server" NavigateUrl='<%# "?MessageId=" + MessageId + "&DownloadAtt=" + rptAttachments.Items.Count %>'
                                                    Text='<%# Eval("FileName") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" rel='<%# rptAttachments.Items.Count %>' ID="txtComment" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btn" rel='<%# rptAttachments.Items.Count %>' CssClass="buttonWhite"
                                                    ClientIDMode="Static" CommandArgument="<%# rptAttachments.Items.Count %>" runat="server"
                                                    Text="Add File To Merchant" OnCommand="btnaddFilesToCurentMerchant_Command" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <iframe frameborder="0" runat="server" id="contentFrame" style="height: 450px; width: 100%; margin: 0; border: solid 1px #cccccc;"></iframe>
                    <br />
                    <br />
                    <table cellpadding="1" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td valign="top">
                                STATUS CHANGE<br />
                                <asp:DropDownList ID="ddlStatus" runat="server" />
                                &nbsp;
                                <asp:TextBox ID="txt_userMessage" ToolTip="Some comment for the status" Style="width: 150px; margin: 0px;" TextMode="SingleLine" runat="server" MaxLength="245"></asp:TextBox>
                                &nbsp;
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="buttonWhite" OnClientClick="return leftSide();" OnClick="btnUpdate_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top" style="width: 350px; padding-left: 40px; border-left: solid 1px gray;">
                    <h2 style="margin: 0;">
                        CONVERSATION HISTORY</h2>
                    <asp:Panel ID="panConversation" runat="server" CssClass="panel">
                        <asp:Repeater ID="rep_Conversation" runat="server">
                            <HeaderTemplate>
                                <table id="tbl_convers_history" cellpadding="0" cellspacing="0" border="0" width="100%" frame="box" class="caps">
                                    <tr>
										<th></th>
										<th>Date & Time</th>
										<th>Subject</th>
										<th>Replied By</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="tbl_ch_<%#Container.ItemIndex %>" style="cursor: pointer; text-align: left;" title="ID:<%#Eval("ID")%>,Row:<%#Container.ItemIndex %>" onclick="showconversationhistory('<%#Eval("ID")%>','<%#Container.ItemIndex %>');" onmouseover="this.className='gridSelectedRow'" onmouseout="this.className=''">
                                    <td>
                                        <span class="rep_span_Conversation">
                                            <%#Eval("IsSent").ToString().ToLower() != "false" ? "<img alt='Msg In' class='' title='Msg In' style='padding:0px;' height='10' width='10' id='0' src='../images/icon_ArrowG.gif' />" : "<img alt='Msg Out' style='padding:0px;' title='Msg Out' height='10' width='10' id='0' src='../images/icon_ArrowR.gif' />"%>
                                        </span>
									</td>
									<td style="line-height:18px;">
                                        <span class="rep_span_Conversation"><%#DateTime.Parse(Eval("Date").ToString()).ToString("dd/MM HH:mm")%></span>
                                    </td>
                                    <td>
                                        <span class="rep_span_Conversation"><%#Eval("Subject")%></span>
                                    </td>
                                    <td>
                                        <span class="rep_span_Conversation"><%#Eval("AdmincashUser")%></span>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <span style="text-align:left; display: <%# rep_Conversation.Items.Count > 0 ? "none": "block" %>">- This
                            message has no conversation history.</span>
                    </asp:Panel>
                    <br />
                    <h2>
                        STATUS HISTORY</h2>
                    <asp:Panel ID="panHistory" runat="server" Style='height: auto;' CssClass="panel">
                        <asp:Repeater ID="rep_History" runat="server">
                            <HeaderTemplate>
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="caps">
                                    <tr>
                                        <th style="width: 45px;">
                                            <span>Time</span>
                                        </th>
                                        <th style="width: 45px;">
                                            <span>Old</span>
                                        </th>
                                        <th style="width: 45px;">
                                            <span>New</span>
                                        </th>
                                        <th style="width: 45px;">
                                            <span>By</span>
                                        </th>
                                        <th style="width: auto;">
                                            <span>Message</span>
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr onmouseover="this.className='gridSelectedRow'" onmouseout="this.className=''">
                                    <td style="width: 40px; padding-left: 4px;">
                                        <span>
                                            <%#Eval("Date")%></span>
                                    </td>
                                    <td style="width: 45px; padding-left: 4px;">
                                        <span>
                                            <%#getStatusName((int?)Eval("StatusOld"))%></span>
                                    </td>
                                    <td style="width: 45px; padding-left: 4px;">
                                        <span>
                                            <%#getStatusName((int?)Eval("StatusNew"))%></span>
                                    </td>
                                    <td style="width: 45px; padding: 0px 4px;">
                                        <span>
                                            <%#Eval("UserName")%></span>
                                    </td>
                                    <td style="width: auto; padding: 5px 22px 5px 10px;">
                                        <span style="">
                                            <%#Eval("Message")%></span>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <span style="text-align:left; display: <%# rep_History.Items.Count > 0 ? "none":"block"%>">- This message has no status history.</span>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="wait" style="width: auto; height: auto; padding: 46%; display: none; margin: 0px;
        background-color: White; position: absolute; top: 0px; left: 0px;">
        Please Wait...
        <br />
        Loading Data
    </div>
    </form>
</body>
</html>
