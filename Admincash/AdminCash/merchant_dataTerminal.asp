<% Response.Charset="UTF-8" : Response.CodePage=65001 %>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_controls.asp"-->
<!--#include file="../include/func_security.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_transCharge.asp"-->
<%
Dim CCF_ID, nCompanyID, IsFirstTerminal, sFirstLoad
nCompanyID = TestNumVar(Request("companyID"), 0, -1, 0)
if Request("Recalc") = "1" Then RecalcCompanyFees nCompanyID, TestNumVar(Request("NPayID"), 0, -1, 0)

If Request("TCharge") = "1" Or Request("TCharge") = "2" Then
	Dim HttpReq, sendStr, replyCode, transId
	sendStr = PROCESS_URL & "member/remote_charge.asp?"
	sendStr = sendStr & "CompanyNum=" & ExecScalar("Select CustomerNumber From tblCompany Where ID=" & nCompanyID, "")  & "&"
	sendStr = sendStr & "Amount=" & 1.00 & "&"
	sendStr = sendStr & "Payments=" & 1 & "&"
	sendStr = sendStr & "TypeCredit=" & 1 & "&"
	sendStr = sendStr & "TransType=" & 0 & "&"

	If Request("TCharge") = "2" Then
		sendStr = sendStr & "Currency=" & 1 & "&"
	'	sendStr = sendStr & "CardNum=" & Dec Data Once("SecretLocation", "CCQd9WGMje5c9NEplfKvAmutJAeyPJV8") & "&"
	'	sendStr = sendStr & "ExpMonth=" & "02" & "&"
	'	sendStr = sendStr & "ExpYear=" & "10" & "&"
	'	sendStr = sendStr & "CVV2=" & DecCVV("crkeea") & "&"
	Else
		sendStr = sendStr & "Currency=" & 0 & "&"
	End if
	'sendStr = sendStr & "CardNum=" & Dec Data Once("SecretLocation", "8SMnmUS+O4LwCOhUaEew2gPms34VuPus") & "&"
		sendStr = sendStr & "ExpMonth=" & "07" & "&"
	sendStr = sendStr & "ExpYear=" & "08" & "&"
	sendStr = sendStr & "CVV2=" & DecCVV("aecbba") & "&"

	sendStr = sendStr & "Member=" & Server.URLEncode("Alon Elbaz") & "&"
	sendStr = sendStr & "PhoneNumber=" & Server.URLEncode("036126966") & "&"
	sendStr = sendStr & "PersonalNum=" & Server.URLEncode("1234567") & "&"
	sendStr = sendStr & "Email=" & Server.URLEncode("Alon@Netpay.co.il") & "&"
	sendStr = sendStr & "Comment=" & Server.URLEncode("Test Transaction From Netpay") & "&"

	sendStr = sendStr & "BillingAddress1=" & Server.URLEncode("hahilazon 6") & "&"
	sendStr = sendStr & "BillingCity=" & Server.URLEncode("Ramat-Gan") & "&"
	sendStr = sendStr & "BillingZipCode=" & Server.URLEncode("55443") & "&"
	sendStr = sendStr & "BillingCountry=" & Server.URLEncode("IL") & "&"
	sendStr = sendStr & "BillingState=" & Server.URLEncode("") & "&"

	sendStr = sendStr & "ReplyURL=none" & "&"
	sendStr = sendStr & "referringUrl=" & Server.URLEncode(referringUrl) & "&"
	sendStr = sendStr & "ClientIP=" & Server.URLEncode(sClientIP) & "&"
	sendStr = sendStr & "requestSource=24" & "&"
	sendStr = sendStr & "isLocalTransaction=true"
	'sendStr = sendStr & "RefTransID=" & Server.URLEncode(sRefTransID) & "&"

	On Error Resume Next
		Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
		HttpReq.open "GET", sendStr, false
		HttpReq.send()
		resStr = HttpReq.responseText 'bts(HttpReq.responseBody)
	On Error Goto 0
	'Response.Write(resStr)
	replyCode = Mid(resStr, 1, 3)
	transId = Mid(resStr, 5, 7)
	Response.Redirect "?CompanyID=" & nCompanyID & "&Reply=" & replyCode & "&TransID=" & transId
End If

Function UpdateItem()
	oleDbData.Execute "Update tblCompanyCreditFees Set" & _
		" CCF_PercentFee=" & TestNumVar(Request("CCF_PercentFee"), 0, -1, 0) & _
		",CCF_FixedFee=" & TestNumVar(Request("CCF_FixedFee"), 0, -1, 0) & _
		",CCF_CBFixedFee=" & TestNumVar(Request("CCF_CBFixedFee"), 0, -1, 0) & _
		",CCF_ClarificationFee=" & TestNumVar(Request("CCF_ClarificationFee"), 0, -1, 0) & _
		",CCF_RefundFixedFee=" & TestNumVar(Request("CCF_RefundFixedFee"), 0, -1, 0) & _
		",CCF_PartialRefundFixedFee=" & TestNumVar(Request("CCF_PartialRefundFixedFee"), 0, -1, 0) & _
		",CCF_FraudRefundFixedFee=" & TestNumVar(Request("CCF_FraudRefundFixedFee"), 0, -1, 0) & _
		",CCF_ApproveFixedFee=" & TestNumVar(Request("CCF_ApproveFixedFee"), 0, -1, 0) & _
		",CCF_FailFixedFee=" & TestNumVar(Request("CCF_FailFixedFee"), 0, -1, 0) & _
		",CCF_PercentCashback=" & TestNumVar(Request("CCF_PercentCashback"), 0, -1, 0) & _
		",CCF_ListBINs='" & Replace(Request("CCF_ListBINs"), "'", "''") & "'" & _
		",CCF_ExchangeTo=" & TestNumVar(Request("CCF_ExchangeTo"), 0, -1, -1) & _
		",CCF_TSelMode=" & TestNumVar(Request("CCF_TSelMode"), 0, -1, 0) & _
		" Where CCF_ID=" & CCF_ID, UpdateItem
	For i = 1 To Request("CCFT_ID").Count
		CCFT_ID = TestNumVar(Request("CCFT_ID")(i), 0, -1, 0)
		If Request("CCFT_Ratio").Count >= i Then CCFT_Ratio = TestNumVar(Request("CCFT_Ratio")(i), 0, -1, 1) Else CCFT_Ratio = "CCFT_Ratio"
		oleDbData.Execute "Update tblCompanyCreditFeesTerminals Set CCFT_Terminal='" & Replace(Request("CCFT_Terminal")(i), "'", "''") & _
		"', CCFT_Ratio=" & CCFT_Ratio & " Where CCFT_ID=" & CCFT_ID
		oleDbData.Execute "Update tblCompanyCreditFeesTerminals Set CCFT_UseCount=0 Where CCFT_CCF_ID=" & CCF_ID
	Next
End Function

CCF_ID = TestNumVar(Request("CCF_ID"), 0, -1, -2)
CCF_CurrencyID = TestNumVar(Request("CCF_CurrencyID"), 0, -1, 0)
CCF_PaymentMethod = TestNumVar(Request("CCF_PaymentMethod"), 0, -1, 0)
If CCF_ID = -1 Then
	oleDbData.Execute "Insert Into tblCompanyCreditFees(CCF_CompanyID, CCF_CurrencyID, CCF_PaymentMethod, CCF_PartialRefundFixedFee, CCF_FraudRefundFixedFee)Values(" & _
		nCompanyID & ", " & CCF_CurrencyID & ", " & CCF_PaymentMethod & ", 0, 0)"
	CCF_ID = ExecScalar("Select Max(CCF_ID) From tblCompanyCreditFees", 0)
ElseIf Trim(Request("DoAction")) = "AddTerminal" Then
	UpdateItem
	oleDbData.Execute "Insert Into tblCompanyCreditFeesTerminals(CCFT_CCF_ID, CCFT_CompanyID, CCFT_Ratio, CCFT_UseCount)Values(" & CCF_ID & ", " & nCompanyID & ", 1, 0)"
	CCFT_ID = ExecScalar("Select Max(CCFT_ID) From tblCompanyCreditFeesTerminals Where CCFT_CCF_ID=" & CCF_ID, 0)
ElseIf Trim(Request("DoAction")) = "Update" Then
	UpdateItem
ElseIf Trim(Request("DoAction")) = "StatusChange" Then
	UpdateItem
	oleDbData.Execute "Update tblCompanyCreditFees Set CCF_IsDisabled=1-CCF_IsDisabled WHERE CCF_ID=" & CCF_ID
ElseIf Trim(Request("DoAction")) = "UpdateFee" Then
	sSQL="UPDATE tblCompany SET" & _
		" IsUsePPWList=" & TestNumVar(Request("IsUsePPWList"), 0, -1, 0) & _
		" WHERE ID=" & nCompanyID
	oledbData.Execute sSQL
ElseIf TestNumVar(Request("DelFT_ID"), 0, -1, 0) <> 0 Then
	CCFT_ID = TestNumVar(Request("DelFT_ID"), 0, -1, 0)
	oleDbData.Execute "Delete From tblCompanyCreditFeesTerminals Where CCFT_ID=" & CCFT_ID
End If

Sub fnc_showSelectBox(varName, vID, lDef, nID)
	Response.Write "<select onmousedown=""LoadTerminalCombo(this, '" & lDef & "')"" id=""" & varName & "_" & vID & """ name=""" & varName & """ class=""inputDataWhite"" style=""width:300px;"" onchange=""HighlightUpdate(" & nID & ");""><option value=""-""></select>"
	sFirstLoad = sFirstLoad & "LoadStateTerm(document.getElementById('" & varName & "_" & vID & "'), '" & lDef & "');" & vbCrLf
End Sub
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<style>
		Input {background-color:#F5F5F5;color:#000000;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:11px;z-index:-1;}
		.btn { font-size:10px; color:#000000; background-color:#F5F5F5; cursor:pointer; }
		.btnW { font-size:10px; color:#000000; background-color:#FFFFFF; cursor:pointer; }
	</style>
	<script language="javascript">
		function ShowInactiveTerm(fCount) {
			for(var i = 1; i <= fCount; i++) {
				for(var t = 1; t <= 4; t++) {
					if(document.getElementById("TrTerminal_" + i + "_" + t))
						document.getElementById("TrTerminal_" + i + "_" + t).style.display = '';
				}
			}
			document.getElementById("TrShowInactiveBtn").style.display = 'none';
		}
		function HighlightUpdate(nID){
			document.getElementById('CCF_MESSAGE_' + nID).style.display='';
			document.getElementById('CCF_BUTTON_' + nID).style.backgroundColor='red';
		}
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<table align="center" border="0" cellpadding="0" cellspacing="2" dir="ltr" width="90%">
<tr>
	<td>
	<%
	sSQL="SELECT tblCompany.CompanyName, IsUsePPWList FROM tblCompany WHERE tblCompany.ID=" & nCompanyID
	set rsData = oledbData.Execute(sSQL)
	if not rsData.EOF then
		Dim sSepaCountries : sSepaCountries = ""
		Set rsSepa = oledbData.Execute("SELECT CountryISOCode FROM [List].[CountryList] WHERE IsSepa=1 ORDER BY CountryISOCode")
		Do Until rsSepa.EOF
			sSepaCountries = sSepaCountries & rsSepa(0) & ","
			rsSepa.MoveNext
		Loop
		rsSepa.Close
		If sSepaCountries <> "" Then sSepaCountries = Left(sSepaCountries, Len(sSepaCountries)-1)
		%>
		<script language="javascript" type="text/javascript">
			function SetSepaCountries(btnThis)
			{
				if (confirm("Do you really want to limit that terminal to the Single Euro Payment Area countries ?!"))
				{
					for (var txtThis = btnThis.previousSibling; txtThis.name != "CCF_ListBINs"; txtThis = txtThis.previousSibling);
					txtThis.value = "<%= sSepaCountries %>";
				}
			}
		</script>
		<table align="center" border="0" cellpadding="1" cellspacing="0" dir="rtl" width="100%">
		<!--#include file="Merchant_dataHeaderInc.asp"-->
		<%
		pageSecurityLevel = 0
		pageClosingTags = "</table></td></tr></table>"
		Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
		%>
		<tr><td height="20"></td></tr>
		<!--#include file="Merchant_dataMenuInc.asp"-->
		</table>
		<br />
		<table align="center" border="0" cellpadding="1" cellspacing="0" width="100%">
		<!--
		<tr>
			<td class="MerchantSubHead">Fees<br /></td>
		</tr>
		-->
		<tr>
			<td>
				<form name="frmTerminal" method="post">
					<input type="Hidden" name="DoAction" value="UpdateFee">
					<table border="0" cellspacing="0" cellpadding="0" dir="ltr" width="100%">
					<tr>
						<td class="txt11">
							<input class="inputData" type="checkbox" style="width:35px;" dir="ltr" name="IsUsePPWList" value="1" <%=IIF(rsData("IsUsePPWList"), "checked", "") %>>
							Use PP White List:
						</td>
						<td align="right"><input type="submit" value="UPDATE" /></td>
					</tr>
					</table>
				</form>
			</td>
		</tr>
		<tr><td><br /><hr width="100%" size="1" style="border-bottom:solid 1px #d2d2d2;"><br /></td></tr>
		<tr>
			<td class="MerchantSubHead">Terminals<br /></td>
		</tr>
		<tr>
			<td>
				<%
				Dim CmpRs, bDisablePrecent
				Set CmpRs = oleDbData.Execute("Select IsUsingNewTerminal, ActiveStatus, CFF_Currency From tblCompany Where ID=" & nCompanyID)
				If Not CmpRs.EOF Then
					If CmpRs("ActiveStatus") = CMPS_INTEGRATION Then
					%>
					<br />
					<div style="color:maroon; font-size:12px; text-align:left;">
						** THIS MERCHANT IS UNDER INTEGRATION PROCESS, THIS SCREEN IS NOT AFFECTIVE UNDER THIS STATE
					</div>
					<%
					End if
					If CmpRs("IsUsingNewTerminal") = True Then
					%>
					<br />
					<table border="0" cellspacing="1" align="center" width="100%" class="formNormal" style="border:1px solid silver;">
					<tr>
						<td style="color:#bc3a4f; font-size:14px; text-align:left;">
							<img src="/NPCommon/Images/alert_small.png"  align="middle" border="0">
							&nbsp; THIS MERCHANT IS USING NEW TERMINAL CONFIGURATION<br />
						</td>
					</tr>
					</table>
					<br />
					<%
					End if
					If CmpRs("CFF_Currency") <> 255 Then
					    bDisablePrecent = True
					%>
					<br />
					<table border="0" cellspacing="1" align="center" width="100%" class="formNormal" style="border:1px solid silver;">
					<tr>
						<td style="color:#bc3a4f; font-size:14px; text-align:left;">
							<img src="/NPCommon/Images/alert_small.png"  align="middle" border="0">
							&nbsp; THIS MERCHANT IS USING GRADED CLEARING FEES<br />
						</td>
					</tr>
					</table>
					<br />
					<%
					End if
				End If
				CmpRs.Close()
				Set CmpRs = Nothing
				%>	
				<br/>
				<form method="post">
				<input type="hidden" name="CCF_ID" value="-1">
				<input type="hidden" name="DoAction" value="Update">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" dir="ltr">
				<tr>
					<td align="left" class="txt12">
						<select name="CCF_PaymentMethod" style="width:300px;" class="inputData" onchange="document.location='?companyID=<%=nCompanyID%>&CCF_PaymentMethod=' + options[selectedIndex].value;">
							<%
								Dim nCurrencies : nCurrencies=0
								Set rsCreditType = oledbData.Execute("SELECT p.*, g.Name AS GroupName, " & _
								    " CASE WHEN EXISTS (SELECT 1 FROM [List].[PaymentMethodToCountryCurrency] WHERE [PaymentMethod_id] = p.PaymentMethod_id AND Currency_id IS NOT NULL) THEN 1 ELSE 0 END AS CurrencyCount " & _
								    " FROM [List].[PaymentMethod] AS p LEFT JOIN [List].[PaymentMethodGroup] AS g ON (p.PaymentMethodGroup_id = g.PaymentMethodGroup_id)" & _
								    " WHERE p.PaymentMethod_id > " & PMD_MAXINTERNAL & " ORDER BY g.Name")
								Do While Not rsCreditType.EOF
									Response.Write("<option value=""" & rsCreditType("PaymentMethod_id") & """ " & IIF(CStr(rsCreditType("PaymentMethod_id")) = Request.QueryString("CCF_PaymentMethod"), " selected", "") & " style=""" & IIF(rsCreditType("PaymentMethod_id") > PMD_EC_MIN, "background-color:#FFCCCC;", "") & """>" & rsCreditType("GroupName") & " | " & rsCreditType("Name"))
									If CStr(rsCreditType("PaymentMethod_id")) = Request.QueryString("CCF_PaymentMethod") Then nCurrencies = rsCreditType("CurrencyCount")
									rsCreditType.MoveNext  
								Loop    
								rsCreditType.Close
							%>
						</select>
						<select name="CCF_CurrencyID" class="inputData">
							<%
								If nCurrencies>0 Then
									Set rsCreditType = oledbData.Execute("SELECT Currency_id FROM [List].[PaymentMethodToCountryCurrency] WHERE [PaymentMethod_id] = " & TestNumVar(Request.QueryString("CCF_PaymentMethod"), 0, -1, 0))
									Do While Not rsCreditType.EOF
										i = rsCreditType(0)
										sSelected = ""
										If i = 1 Then sSelected = "Selected"
										Response.Write("<option value=""" & i & """ " & sSelected & ">" & GetCurIso(i) & "</option>")
										rsCreditType.MoveNext
									Loop
									rsCreditType.Close
								Else
									For i = 0 To MAX_CURRENCY
										sSelected = ""
										If i = 1 Then sSelected = "Selected"
										Response.Write("<option value=""" & i & """ " & sSelected & ">" & GetCurIso(i) & "</option>")
									Next
								End If
							%>
						</select>
						<input type="submit" value="ADD CARD" class="btn" style="width:70px;" /><br />
					</td>
				</tr>
				</table>
				</form>
			</td>
		</tr>
		</table>
		<br /><br />
		<table align="center" border="0" cellpadding="1" cellspacing="0" dir="rtl" width="100%">
		<tr>
			<td colspan="4">
				<table align="center" border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
				<tr>
					<td colspan="2" style="text-decoration: underline;"><br /></td>
					<td class="txt11b" style="text-decoration:underline;">Credit card<br /></td>
					<td class="txt11b" style="text-decoration: underline;">Countries (ISO)<br /></td>
					<td colspan="3" class="txt11b" style="text-decoration: underline;" align="center">Currency<br /></td>
					<td colspan="2" class="txt11b" style="text-decoration: underline;" align="center">Trans Fees<br /></td>
					<td width="10" nowrap><br /></td>
					<td colspan="4" class="txt11b" style="text-decoration: underline;" align="center">Misc Fees<br /></td>
					<td colspan="3"><br /></td>
				</tr>
				<tr>
					<td colspan="2" style="text-decoration: underline;"><br /></td>
					<td colspan="2" style="text-decoration: underline;"><br /></td>
					<td class="txt11" style="text-decoration:underline;" align="center">Set</td>
					<td width="14"><br /></td>
					<td class="txt11" style="text-decoration:underline;" align="center">Process</td>
					<td class="txt11" style="text-decoration: underline;" align="center">Trans<br /></td>
					<td class="txt11" style="text-decoration: underline;" align="center">Clearing<br /></td>
					<td><br /></td>
					<td class="txt11" style="text-decoration: underline;" align="center">Pre-Auth<br /></td>
					<td class="txt11" style="text-decoration: underline;" align="center">Refund<br /></td>
					<td class="txt11" style="text-decoration: underline;" align="center">R.Partial<br /></td>
					<td class="txt11" style="text-decoration: underline;" align="center">R.Fraud<br /></td>
					<td class="txt11" style="text-decoration: underline;" align="center">Copy R.<br /></td>
					<td class="txt11" style="text-decoration: underline;" align="center">CHB<br /></td>
					<td class="txt11" style="text-decoration: underline;" align="center">Fail<br /></td>
					<td class="txt11" style="text-decoration: underline;" align="center">Cashback<br /></td>
					<td colspan="2"><br /></td>
				</tr>
				<tr><td><br /></td></tr>
				<%
				nCount = 0 : nTermCount = 0
				nInactiveCount = 0
				sSQL = "SELECT tblCompanyCreditFees.*, tblCompanyCreditFeesTerminals.*, pm.name AS pm_Name, tblDebitTerminals.ID DTID, isNetpayTerminal" & _
				" FROM tblCompanyCreditFees LEFT JOIN List.PaymentMethod AS pm ON CCF_PaymentMethod= pm.PaymentMethod_id" & _
				" LEFT JOIN tblCompanyCreditFeesTerminals ON CCFT_CCF_ID=CCF_ID LEFT JOIN tblDebitTerminals ON CCFT_Terminal=terminalNumber" & _
				" WHERE CCF_CompanyID=" & nCompanyID & " ORDER BY CCF_IsDisabled, CCF_PaymentMethod DESC, CCF_CurrencyID, CCF_ListBINs, CCF_ID, CCFT_ID"
				Set rsData3 = Server.CreateObject("Adodb.Recordset")
				rsData3.Open sSQL, oledbData, 3, 1
				Do While Not rsData3.EOF
					nCount = nCount + 1
					sTrStyle = "display:;"
					sTdBkgColorFirst = "#66cc66"
					sBtnTxt = "DISABLE"
					If rsData3("CCF_IsDisabled") Then
						nInactiveCount = nInactiveCount + 1
						sTrStyle = "display:none;"
						sTdBkgColorFirst = "#ff6666"
						sBtnTxt = "ACTIVATE"
					End if
					%>
					<tr bgcolor="#EEEEEE" id="TrTerminal_<%=nInactiveCount%>_1" style="<%=sTrStyle%>">
						<form method="post">
						<input type="hidden" name="DoAction" value="Update">
						<input type="hidden" name="CCF_ID" value="<%=rsData3("CCF_ID")%>">
						<input type="hidden" name="CCF_CurrencyID" value="<%=rsData3("CCF_CurrencyID")%>">
						<input type="hidden" name="CCF_PaymentMethod" value="<%=rsData3("CCF_PaymentMethod")%>">
						<td width="4" bgcolor="<%=sTdBkgColorFirst%>"></td>
						<td width="2"></td>
						<td class="txt12"><img width="23" height="15" align="middle" src="/NPCommon/ImgPaymentMethod/23X12/<%=rsData3("CCF_PaymentMethod")%>.gif"> <%=rsData3("pm_Name")%></td>
						<td class="txt12" style="white-space:nowrap;">
							<input name="CCF_ListBINs" value="<%=rsData3("CCF_ListBINs")%>" style="width:120px" />
							<img src="/NPCommon/images/sepa-logo.gif" onclick="SetSepaCountries(this)" title="Limit that terminal to SEPA countries" style="border-width:0;cursor:pointer;height:12px;margin:0;padding:0;" alt="SEPA" />
						</td>
						<td class="txt12" align="center"><%=GetCurIso(rsData3("CCF_CurrencyID"))%></td>
						<td class="txt12" align="center" nowrap>--></td>
						<td align="center">
							<select name="CCF_ExchangeTo" class="inputData">
								<option value="-1">None</option>
								<%
								For i = 0 To MAX_CURRENCY
									Response.Write("<option value=""" & i & """")
									If i = rsData3("CCF_ExchangeTo") Then Response.Write(" selected=""selected""")
									Response.Write(">" & GetCurIso(i))
								Next	
								%>
							</select>
						</td>
						<td align="center" class="txt12"><input name="CCF_FixedFee" value="<%=FormatNumber(rsData3("CCF_FixedFee"), 2, True)%>" size="4"></td>
						<td align="center" class="txt12"><input name="CCF_PercentFee" value="<%=FormatNumber(rsData3("CCF_PercentFee"), 2, True)%>" <%=IIF(bDisablePrecent, " disabled", "")%> size="4">%</td>
						<td></td>
						<td align="center" class="txt12"><input name="CCF_ApproveFixedFee" value="<%=FormatNumber(rsData3("CCF_ApproveFixedFee"), 2, True, False, False)%>" size="4"></td>
						<td align="center" class="txt12"><input name="CCF_RefundFixedFee" value="<%=FormatNumber(rsData3("CCF_RefundFixedFee"), 2, True, False, False)%>" size="4"></td>
						<td align="center" class="txt12"><input name="CCF_PartialRefundFixedFee" value="<%=FormatNumber(rsData3("CCF_PartialRefundFixedFee"), 2, True, False, False)%>" size="4"></td>
						<td align="center" class="txt12"><input name="CCF_FraudRefundFixedFee" value="<%=FormatNumber(rsData3("CCF_FraudRefundFixedFee"), 2, True, False, False)%>" size="4"></td>
						<td align="center" class="txt12"><input name="CCF_ClarificationFee" value="<%=FormatNumber(rsData3("CCF_ClarificationFee"), 2, True, False, False)%>" size="4"></td>
						<td align="center" class="txt12"><input name="CCF_CBFixedFee" value="<%=FormatNumber(rsData3("CCF_CBFixedFee"), 2, True, False, False)%>" size="4"></td>
						<td align="center" class="txt12"><input name="CCF_FailFixedFee" value="<%=FormatNumber(rsData3("CCF_FailFixedFee"), 2, True, False, False)%>" size="4"></td>
						<td align="center" class="txt12"><input name="CCF_PercentCashback" value="<%=FormatNumber(rsData3("CCF_PercentCashback"), 2, True, False, False)%>" size="4"></td>
						<td align="center" class="txt12">
							<input type="submit" value="UPD" class="btn" id="CCF_BUTTON_<%=rsData3("CCF_ID")%>" style="width:35px;padding-left:0;padding-right:0;" onclick="if(parseFloat(form.elements('CCF_FixedFee').value) < 2 || parseFloat(form.elements('CCF_PercentFee').value) < 0.25 || parseInt(form.elements('CCF_RefundFixedFee').value) == '0' || parseInt(form.elements('CCF_ApproveFixedFee').value) == '0' || parseInt(form.elements('CCF_ClarificationFee').value) == '0' || parseInt(form.elements('CCF_CBFixedFee').value) == '0' || parseInt(form.elements('CCF_FailFixedFee').value) == '0') return confirm('There are some unset fees in this terminal!\n\nDo you really want to continue?');">
							<input type="button" value="<%=sBtnTxt%>" class="btn" style="width:66px; color:Maroon;" onclick="if(confirm('Sure to change status ?')) { form.elements['DoAction'].value='StatusChange'; form.submit(); }">
						</td>
						<td align="right" nowrap style="border-right:solid 1px gray;">
							<input type="button" class="btn" onclick="form.elements['DoAction'].value='AddTerminal';form.submit();" style="width:100px;" value="ADD TERMINAL" /><br />
						</td>
					</tr>
					<%
					bAmountError = (rsData3("CCF_FixedFee") = 0) Or (rsData3("CCF_PercentFee") = 0) Or (rsData3("CCF_ApproveFixedFee") = 0) Or (rsData3("CCF_RefundFixedFee") = 0) Or (rsData3("CCF_ClarificationFee") = 0) Or (rsData3("CCF_CBFixedFee") = 0)
					nLastID = rsData3("CCF_ID")
					nTermCount = 0
					Do While Not rsData3.EOF
						If IsNull(rsData3("CCFT_ID")) Then Exit Do
						If nLastID <> rsData3("CCF_ID") Then
							rsData3.MovePrevious
							Exit Do
						End if
						nTermCount = nTermCount + 1
						%>
						<tr id="TrTerminal_<%=nInactiveCount%>_2" style="<%=sTrStyle%>">
							<td colspan="3" <%=IIF(nTermCount = 1, "style=""border-top:solid 1px gray;""", "")%> ><img width="1" height="1" /></td>
							<td colspan="17" style="border:solid 1px gray;border-top:none;border-bottom:none;padding-left:10px;">
								<input type="hidden" name="CCFT_ID" value="<%=rsData3("CCFT_ID")%>">
								<table cellpadding="0" cellspacing="2">
								<tr>
									<td class="txt12" nowrap>
										<%
										fnc_showSelectBox "CCFT_Terminal", rsData3("CCFT_ID"), rsData3("CCFT_Terminal"), rsData3("CCF_ID")
										If rsData3("isNetpayTerminal") And bAmountError Then
											Response.Write(" <img src=""/NPCommon/Images/alert_small.png"" title=""Not all fees are set"" align=""middle"" border=""0""> ")
										Else
											'Response.Write(" <img width=""20"" height=""1"" align=""middle"" border=""0""> ")
										End if
										%>&nbsp;
									</td>
									<td class="txt12" align="right" nowrap>
										<input type="button" value="DEL" class="btn" onclick="document.location='?companyID=<%=nCompanyID%>&DelFT_ID=<%=rsData3("CCFT_ID")%>'" />&nbsp&nbsp
									</td>
									<td class="txt12" nowrap>
									 <%
									 Select Case rsData3("CCF_TSelMode")
									 Case 0 ' Ratio
									    Response.Write("Ratio: <input name=""CCFT_Ratio"" value=""" & rsData3("CCFT_Ratio") & """ class=""inputDataWhite"" onkeyup=""HighlightUpdate(" & rsData3("CCF_ID") & ");"" size=""4"">&nbsp&nbsp")
									 Case 1 ' WL
									    Response.Write("Check: <select name=""CCFT_Ratio"" class=""inputDataWhite"" onchange=""HighlightUpdate(" & rsData3("CCF_ID") & ");"">")
									    Response.Write("<option value=""1""" & IIF(rsData3("CCFT_Ratio") = 1, " selected", "") & ">IN White List")
									    Response.Write("<option value=""2""" & IIF(rsData3("CCFT_Ratio") = 2, " selected", "") & ">NOT IN Black List")
									    Response.Write("<option value=""0""" & IIF(rsData3("CCFT_Ratio") = 0, " selected", "") & ">None")
									    Response.Write("</select>")
									 Case 2 ' Priority
										Response.Write(nTermCount)
										Select Case (nTermCount Mod 10)
										Case 1 : Response.Write("<sup>st</sup>")
										Case 2 : Response.Write("<sup>nd</sup>")
										Case 3 : Response.Write("<sup>rd</sup>")
										Case Else : Response.Write("<sup>th</sup>")
										End Select
									 Case 3 'Terminal Mapping
										Response.Write(rsData3("DTID"))
									 End Select
									 %>
									</td>
									<td class="txt12">&nbsp;(<%=rsData3("CCFT_UseCount")%>)</td>
									<td align="right" width="100%">
										<%If nTermCount = 1 Then%>
										 <span style="display:none;" id="CCF_MESSAGE_<%=rsData3("CCF_ID")%>">Click UPD to apply your chages</span>
										<%ElseIf nTermCount = 2 Then%>
											<select name="CCF_TSelMode" onchange="form.submit()" > 
												<option value="0" <%=IIF(rsData3("CCF_TSelMode") = 0, " selected", "") %>>Ratio Selection
												<option value="1" <%=IIF(rsData3("CCF_TSelMode") = 1, " selected", "") %>>B/W List Selection
												<option value="2" <%=IIF(rsData3("CCF_TSelMode") = 2, " selected", "") %>>Priority List
												<option value="3" <%=IIF(rsData3("CCF_TSelMode") = 3, " selected", "") %>>Merchant Control
											</select>
										<%End If %>
									</td>
								</tr>
								</table>
							</td>
						</tr>
						<%
						rsData3.MoveNext
						If IsNull(rsData3("CCFT_ID")) Then 
							rsData3.MovePrevious
							Exit Do
						End if
					Loop
					%>
					</form>
					<tr id="TrTerminal_<%=nInactiveCount%>_3" style="<%=sTrStyle%>">
					 <%
					 If nTermCount > 0 Then Response.Write("<td colspan=""3""></td><td colspan=""17"" bgColor=""gray"" height=""1""></td>") _
					 Else Response.Write("<td colspan=""20"" bgColor=""gray"" height=""1""></td>")
					 %>
					</tr>
					<tr id="TrTerminal_<%=nInactiveCount%>_4" style="<%=sTrStyle%>"><td colspan="18" height="25"><br /></td></tr>
					<%
				  If Not rsData3.EOF Then rsData3.MoveNext
				Loop
				rsData3.Close
				If nInactiveCount > 0 Then
					%>
					<tr id="TrShowInactiveBtn"><td colspan="18"><a href="javascript:ShowInactiveTerm('<%=nInactiveCount%>');">Show Incative terminals (<%=nInactiveCount%>)</a><br /></td></tr>
					<%
				End if	
				%>
				<tr><td id="tdUpdPendingTransText" colspan="18"><br /><a href="?CompanyID=<%=nCompanyID%>&Recalc=1&NPayID=0">Update unsettled transaction Fees</a></td></tr>
				</table>
			</td>
		</tr>
		<tr><td height="40"><br/></td></tr>
		<tr>
			<td align="right">
				<%if Request("Reply") <> "" Then Response.Write("<br />����� ������ �����:" & Request("Reply") & ", ����:" & Request("TransID"))%>
			</td>
		</tr>
		</table>
		<%
	End If
	rsData.close
	set rsData = nothing
	%>
	</td>
</tr>
</table>
<script language="javascript" type="text/javascript" defer="defer">
	for (var i=0;i<document.all.length;i++)
	{
		if (document.all[i].tagName.toUpperCase()=="INPUT")
		{
			if (parseFloat(document.all[i].value)==0) document.all[i].style.color="red";
		}
	}
</script>
</body>
</html>
<script language="javascript">
	var vTerminalList = new Array();
	//Set rsData2 = oledbData.execute("spSelect_TerminalSelectList")
	<%
	sSQL = "SELECT dt.terminalNumber, dt.processingMethod, dt.dt_name, dt.dt_name AS terminalName_heb, dt.isActive, dt.isNetpayTerminal, dc.dc_name, dt.id " & _
			"FROM tblDebitTerminals AS dt INNER JOIN tblDebitCompany AS dc ON dt.DebitCompany=dc.debitCompany_id " & _
			"ORDER BY dt.DebitCompany, dt.dt_name"
	Set rsData2 = oledbData.execute(sSQL)
	Do While Not rsData2.EOF
		Dim sStyle, nStrValues, bIsShow
		sStyle= "" : nStrValues = "" : bIsShow = True
		If Not rsData2("isActive") Then
			sStyle = "maroon"
		ElseIf rsData2("isNetpayTerminal") Then
			sStyle = "#315AEA"
		End If
		If rsData2("isActive") Then
			nStrValues = IIF(rsData2("isNetpayTerminal"), 2, IIF(rsData2("isActive"), 1, 0)) & ","
			nStrValues = "'" & sStyle & "','" & Replace(rsData2("dc_name"), "'", "\'") & " | " & Replace(rsData2("dt_name"), "'", "\'") & " | " & Replace(rsData2("terminalNumber"), "'", "\'")
			If CInt(rsData2("processingMethod")) = 2 Then nStrValues = nStrValues & " (���� ������)"
			nStrValues = nStrValues & "'"
			Response.Write("vTerminalList['" & Replace(rsData2("terminalNumber"), "'", "\'") & "']= new Array(" & nStrValues & ");" & vbCrLf)
		End If
	  rsData2.MoveNext
	Loop
	rsData2.Close
	%>
	
	function LoadTerminalCombo(cmb, nSel){
		if(cmb.options.length > 1) return;
		cmb.innerHTML = '';
		for (key in vTerminalList){
			var oOption = document.createElement("OPTION");
			cmb.options.add(oOption);
			oOption.style.color = vTerminalList[key][0];
			oOption.innerHTML = vTerminalList[key][1];
			oOption.value = key;
			if(key == nSel) oOption.selected = true;
		}
	}

	function LoadStateTerm(cmb, nSel){
		if(!vTerminalList[nSel]) return;
		cmb.innerHTML = '';
		var oOption = document.createElement("OPTION");
		cmb.options.add(oOption);
		oOption.style.color = vTerminalList[nSel][0];
		oOption.innerHTML = vTerminalList[nSel][1];
		oOption.value = nSel;
		oOption.selected = true;
	}
	<%=sFirstLoad%>
</script>
