<%@ Page Language="VB" %>
<%@ Register Src="Password.ascx" TagPrefix="custom" TagName="Password" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<script runat="server">
    Dim req_ID As Integer
    Sub Page_Load()
        lblPassword.Text = String.Empty
        Security.CheckPermission(lblPermissions)
        req_ID = Request("user")
        ucPassword.RefID = req_ID
        If Not IsPostBack Then
            Dim iReader As SqlDataReader = dbPages.ExecReader("SELECT * FROM tblSecurityUser WHERE ID=" & req_ID)
            If Not iReader.Read Then
                iReader.Close()
                Response.Redirect("common_blank.htm", True)
            End If
            ltName.Text = iReader("su_Name")
            txtUserName.Text = dbPages.DBTextShow(iReader("su_Username"))
            txtEmail.Text = dbPages.DBTextShow(iReader("su_Mail"))
            If iReader("su_IsActive") Then chkIsActive.Checked = True
            iReader.Close()
            'tlbTop.AddItem("Login Details", "security_user.aspx?id=" & req_ID, True)
        End If

    End Sub
	
    Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sSQL As String = "UPDATE tblSecurityUser SET " & _
          "su_Username='" & dbPages.DBText(txtUserName.Text) & "'," & _
          "su_Mail='" & dbPages.DBText(txtEmail.Text) & "'," & _
          "su_IsActive=" & IIf(chkIsActive.Checked, 1, 0) & _
          " WHERE ID=" & req_ID
        dbPages.ExecSql(sSQL)
		Dim user = Netpay.Infrastructure.Security.AdminUser.Load(req_ID)
		if (user.Login.LoginID = 0) Then CType(user, Netpay.Infrastructure.Security.AdminUser).Save()
        user.FullName = txtUserName.Text
        user.Login.UserName = txtUserName.Text
        user.Login.EmailAddress = txtEmail.Text
        user.Login.IsActive = chkIsActive.Checked
        user.Login.Save()
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Security - User</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
<tr>
	<td>
		<asp:label ID="lblPermissions" runat="server" />
		<span id="pageMainHeading">User Management</span> - <asp:Literal runat="server" ID="ltName" /><br /><br />
	</td>
</tr>
<tr>
	<td>
		<form id="frmServer" runat="server">
		<table width="200" class="formNormal" border="0">
			<asp:HiddenField ID="hidAffID" runat="server" />
			<tr><td class="MerchantSubHead" colspan="2">Login Details<br /><br /></td></tr>
			<tr>
	            <td>
		            <asp:CheckBox ID="chkIsActive" runat="server" CssClass="option" /> Allow Login<br /><br />
	            </td>
            </tr>
			<tr>
	            <td>
		            Email<br />
		            <asp:TextBox ID="txtEmail" runat="server" /><br />
	            </td>
	            <td>
		            User Name<br />
		            <asp:TextBox ID="txtUserName" runat="server" />
		            <asp:Label ID="lblPassword" runat="server" />
	            </td>
            </tr>
			<tr><td><br /><br /></td></tr>
            <tr>
	            <td colspan="2" style="padding-left:0;">
		            <custom:Password ID="ucPassword" RefType="SystemUser" Title="" TextBoxWidth="130px" Layout="SingleLineTitlesOnTop" runat="server" />
	            </td>
            </tr>
			<tr>
                <td><br /></td>
            </tr>	
			<tr><td height="20" colspan="2"></td></tr>
			<tr>
				<td colspan="2">
					<br /><asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" Text=" UPDATE " />
					<asp:ValidationSummary ShowMessageBox="false" ShowSummary="false" runat="server" ID="vsMerchant" />
				</td>
			</tr>
		</table>
        </form>	
	</td>
</tr>
</table>	
</body>
</html>
