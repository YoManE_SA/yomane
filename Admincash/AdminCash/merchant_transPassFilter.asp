<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_controls.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/const_globalData.asp"-->
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<link rel="stylesheet" type="text/css" href="../StyleSheet/calendar-blue.css" title="winter" />
	<style type="text/css">
		.Bsum{
			background-image: url("../images/icon_sum.gif");
			background-repeat: no-repeat; background-position: 85px; 
			width: 105px; height: 20px; text-align: left;
			background-color:#ffffff; font-size:10px; border:1px solid black;
			padding-left: 3px; padding-right: 3px; cursor: hand;
		}		
		.BReg{
			height: 20px; text-align: center;
			background-color:#ffffff; font-size:10px; border:1px solid black;
			padding-left: 0px; padding-right: 0px; cursor: hand;
		}
	</style>
	<script type="text/javascript" src="../js/func_common.js"></script>
	<script type="text/javascript" src="../js/calendar.js"></script>
	<script type="text/javascript" src="../js/calendar-en.js"></script>
	<script type="text/javascript" src="../js/calendar-setup.js"></script>
	<script language="javascript">
		function ShowAdvFilter() {
			for(i = 0; i <= 2; i++) {
				document.getElementById("TrFiltering" + i).style.display = "";
			}
			document.getElementById("FilteringLink").style.display = "none";
		}
	</script>
</head>
<%
sShowName = uCase(Request("ShowName"))
%>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<table class="SectionsTbl">
<tr>
 	<td nowrap id="pageMainHeadingTd">
		<span id="pageMainHeading">UNSETTLED BALANCE</span>
		<span class="txt14">- <%= dbTextShow(sShowName) %></span><br />
	</td>
</tr>
</table>
<br />
<%
Dim CurrArray()
Redim CurrArray(MAX_CURRENCY)

pageSecurityLevel = 0
pageSecurityLang = "ENG"
pageClosingTags = ""
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)

nCompanyID = TestNumVar(Request("companyID"), 0, -1, 0)
nCurrency = TestNumVar(Request("Currency"), 0, MAX_CURRENCY, -1)
nCompanyStatus = TestNumVar(Request("CompanyStatus"), 0, -1, 0)
Set rsData=server.createobject("adodb.recordset")

sSQL = "SELECT [CurrencyID], [CurrencyISOCode] FROM [List].[CurrencyList] ORDER BY [CurrencyID]"
Set rsData = oledbData.Execute(sSQL)
Do Until rsData.EOF
	If rsData(0) > UBound(CurrArray) Then Redim Preserve CurrArray(rsData(0))
	CurrArray(rsData(0)) = rsData(1)
	rsData.MoveNext
Loop
rsData.Close
%>
<fieldset>
	<legend>WAITING TO BE SETTLED</legend>
	<table class="SectionsTbl">
	<%
	sSQL = "SELECT COUNT(id) AS numOfTrans, Currency FROM tblCompanyTransPass "
	If  nCompanyStatus = CMPS_INTEGRATION Then
		sSQL = sSQL & "WHERE ((UnsettledInstallments > 0) OR (isTestOnly = 1)) AND (payID<>';X;') "
	Else
		sSQL = sSQL & "WHERE (UnsettledInstallments > 0) "
	End if
	sSQL = sSQL & " AND (CompanyID = " & nCompanyID & ") GROUP BY Currency"
	rsData.Open sSQL, oledbData
	If NOT rsData.EOF Then
		If nCurrency = -1 Then nCurrency = rsData("Currency")
		Do Until rsData.EOF
			%>
			<tr>
				<td class="txt14" style="padding-right:6px;"><%=CurrArray(rsData("Currency")) & " (" & GetCurText(rsData("Currency")) & ")"%></td>
				<td class="txt14" style="padding-right:6px;"><%=FormatNumber(rsData("numOfTrans"),0,0,0,-1)%> transactions</td>
				<td nowrap>
					<input type="Button" class="Bsum" value="SHOW TOTALS" onclick="OpenPop('../include/common_totalTransShow.asp?LNG=1&companyID=<%= nCompanyID %>&Currency=<%= rsData("Currency") %>&isClickCount=true&isNotCalcAll=&isIframe=0','fraOrderPrint',750,300,1);"> 
					<% If PageSecurity.IsMemberOf("Dev. Support") Then %>
					<input type="Button" class="Bsum" value="SHOW TOTALS 2" onclick="OpenPop('../include/common_totalTransShowV2.aspx?LNG=1&companyID=<%= nCompanyID %>&CurrencyID=<%= rsData("Currency") %>&isClickCount=true&isNotCalcAll=&isIframe=0','fraOrderPrint',750,300,1);">
					<% End If %>
				</td>
				<td><input type="Button" class="BReg" value="SHOW LIST &gt;&gt;" onclick="location.href='merchant_transPass.asp?companyID=<%= nCompanyID %>&Currency=<%= rsData("Currency") %>&isClickCount=true' ;parent.fraButton.FrmResize();"><br /></td>
			</tr>
			<%
			'CurrArray(rsData("Currency")) = 1
			'If nCurrency = -1 Then nCurrency = TestNumVar(rsData("Currency"), 0, MAX_CURRENCY, 0)
			rsData.MoveNext
		Loop
	Else
		Response.Write("<tr><td>Merchant has no unsettled transactions!</td></tr>")
	End if
	rsData.close
	%>
	</table>
</fieldset>
<%
If nCurrency <> -1 Then
	sSQL="SELECT Min(InsertDate), Max(InsertDate), Min(MerchantPD), Max(MerchantPD) FROM tblCompanyTransPass WHERE CompanyID=" & nCompanyID & " AND UnsettledInstallments>0 AND Currency=" & nCurrency
	set rsData=oledbData.Execute(sSQL)
	sFromDate="" : sToDate="" : sFromPD="" : sToPD=""
	if Not rsData.EOF then
		sFromDate=rsData(0)
		sToDate=rsData(1)
		sFromPD=rsData(2)
		sToPD=rsData(3)
	end if
	rsData.Close()
	%>
	<br />
	<fieldset>
		<legend>TRANSACTIONS FILTERING</legend>
		<form action="merchant_transPass.asp" method="get" name="frmDateFilter" onsubmit="parent.fraButton.FrmResize();">
		<input type="hidden" name="companyID" value="<%= nCompanyID %>">
		<input type="hidden" name="ShowName" value="<%= sShowName %>">
		<input type="hidden" name="Currency" value="<%= nCurrency %>">
		<input type="hidden" name="isClickCount" value="true">
		<input type="hidden" name="isFilter" value="1">
		<table class="SectionsTbl">
		<tr>
			<td style="background-color:#f5f5f5;">
				<%
				isAddDivider = False
				For i = 0 to MAX_CURRENCY
					If isAddDivider Then Response.Write(" | ")
					If nCurrency = i Then
						Response.Write("<span style=""font-size:11px; font-weight:bold; color:#555555;"">" & uCase(CurrArray(i)) & "</span>")
					
					Else
						Response.Write("<a href=""?CompanyID=" & nCompanyID & "&ShowName=" & sShowName & "&Currency=" & i & """ style=""font-size:11px; cursor:pointer;"">" & uCase(CurrArray(i)) & "</a>")
					End if
					isAddDivider = True
				Next
				%>
			</td>
		</tr>
		<tr><td height="6"></td></tr>
		<tr id="TrFiltering0" style="display:none;">
			<td>
				<table border="0" cellspacing="0" cellpadding="1" width="100%">
				<tr><td><span class="inputHeading">Payment Method</span></td></tr>
				<tr>
					<td>
						<%
							Dim nCount
							Set rsData=oledbData.Execute("SELECT PaymentMethod_id, Name FROM List.PaymentMethod ORDER BY PaymentMethod_id")
							Do While Not rsData.EOF
								%>
									<span style="float:left;width:150px;">
										<input type="checkbox" name="PaymentMethod" value="<%= rsData("PaymentMethod_id") %>" checked="checked" /><%= rsData("Name") %><br />
									</span>
								<%
								rsData.MoveNext
							Loop
							rsData.Close
						%>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr id="TrFiltering1" style="display:none;">
			<td>
				<table border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td colspan="3"><span class="inputHeading">Credit Type</span></td>
					<td width="30"></td>
					<td colspan="3"><span class="inputHeading">Chb Only</span></td>
				</tr>
				<tr>
					<td><input type="Checkbox" name="CreditType" value="1, 2, 6, 8" checked> Charge</td>
					<td width="4"></td>
					<td><input type="Checkbox" name="CreditType" value="0" checked> Refund</td>
					<td></td>
					<td><input type="radio" name="DeniedStatus" value="" checked> No</td>
					<td width="4"></td>
					<td><input type="radio" name="DeniedStatus" value="Y"> Yes, Only chb</td>
				</tr>
				<tr><td height="10"></td></tr>
				</table>
			</td>
		</tr>
		<tr id="TrFiltering2" style="display:none;">
			<td>
				<table border="0" cellspacing="0" cellpadding="1">
				<tr><td colspan="20"><span class="inputHeading">Terminals</span> <span class="footnote">*</span></td></tr>
				<tr>
					<td>
						<input type="radio" name="TypeTerminal" value="" checked> ���
					</td>
					<td width="4"></td>
					<%
					sSQL="SELECT DISTINCT TerminalNumber FROM tblCompanyTransPass WHERE Currency=" & nCurrency & _
					" AND TerminalNumber<>'' AND CompanyID=" & nCompanyID & " AND (PrimaryPayedID=0 OR DeniedStatus=" & DNS_SetInVar & ")"
					rsData.Open sSQL,oledbData, 1, 3
					sRecordCount = rsData.RecordCount
					nCount = 0
					Do Until rsData.EOF
						nCount = nCount + 1
						if nCount mod 2 = 0 then Response.Write("</tr><tr>")
						%>
						<td>
							<input type="radio" name="TypeTerminal" value="<%= rsData("TerminalNumber") %>"> <% Call displayTerminal(trim(rsData("TerminalNumber"))) %>
						</td>
						<td width="4"></td>
						<%
					  rsData.MoveNext
					Loop	
					Response.Write("</tr>")
					rsData.Close
					Set rsData = nothing			
					%>
				<tr><td height="10"></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><span class="inputHeading">Trans Date</span> <span class="footnote">*</span></td></tr>
		<tr>
			<td class="txt14">
				<input type="text" class="inputData" tabindex="1" dir="ltr" size="10" name="fromDate" value="<%= sFromDate %>">
				<img src="../images/calendar_icon.gif" id="fromDate_trigger_c" width="16" height="15" border="0" style="cursor:pointer;"> To
				<input type="text" class="inputData" tabindex="2" dir="ltr" size="10" name="toDate" value="<%= sToDate %>">
				<img src="../images/calendar_icon.gif" id="toDate_trigger_c" width="16" height="15" border="0" style="cursor:pointer;">&nbsp;&nbsp;&nbsp;&nbsp;
				<input class="button1" type="submit" value="SEARCH"><br />		
			</td>
		</tr>
		<script type="text/javascript">
			Calendar.setup({
				inputField	 :	"fromDate",	 // id of the input field
				ifFormat	   :	"%d/%m/%Y",	  // format of the input field
				button		 :	"fromDate_trigger_c",  // trigger for the calendar (button ID)
				align		  :	"Tc",		   // alignment (defaults to "Bl")
				singleClick	:	true,
				mondayFirst	:	false,
				weekNumbers	:	false
			});
			Calendar.setup({
				inputField	 :	"toDate",	 // id of the input field
				ifFormat	   :	"%d/%m/%Y",	  // format of the input field
				button		 :	"toDate_trigger_c",  // trigger for the calendar (button ID)
				align		  :	"Tc",		   // alignment (defaults to "Bl")
				singleClick	:	true,
				mondayFirst	:	false,
				weekNumbers	:	false
			});
		</script>
		<tr>
			<td class="txt10" style="padding-top:6px;">
				* Displays according to existing transactions <%=replace(space(26)," ","&nbsp;")%>
				<a id="FilteringLink" style="cursor:pointer;" onclick="ShowAdvFilter();">Show advanced filters</a><br />
			</td>
		</tr>
		</table>
		</form>
	</fieldset>
	<br />
	<fieldset>
		<legend>CREATE/UPDATE SETTLEMENT</legend>
		<form action="merchant_payCreate.asp" method="post" name="frmAddToPayment">
		<input type="Hidden" name="companyID" value="<%= nCompanyID %>">
		<input type="Hidden" name="Currency" value="<%= nCurrency %>">
		<input type="Hidden" name="Action" value="Update">
		<input type="Hidden" name="type" value="ExecAll">
		<table class="SectionsTbl">
			<tr>
				<td style="background-color:#f5f5f5;">
					<%
					isAddDivider = False
					For i = 0 to MAX_CURRENCY
						If isAddDivider Then Response.Write(" | ") : isAddDivider = False
						If nCurrency = i Then
							Response.Write("<span style=""font-size:11px; font-weight:bold; color:#555555;"">" & uCase(CurrArray(i)) & "</span>")
						Else
							Response.Write("<a href=""?CompanyID=" & nCompanyID & "&ShowName=" & sShowName & "&Currency=" & i & """ style=""font-size:11px; cursor:pointer;"">" & uCase(CurrArray(i)) & "</a>")
						End if
						isAddDivider = True
					Next
					%>
				</td>
			</tr>
			<tr><td height="6"></td></tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="1">
					<tr>
						<td>
							<input type="radio" name="DateUsage" value="1" onclick="document.getElementById('PayFromPD').disabled=document.getElementById('payToPD').disabled=false;document.getElementById('PayFromDate').disabled=document.getElementById('payToDate').disabled=true;" checked />
							<span class="inputHeading">Merchant PD</span> &nbsp;
						</td>
						<td>
							<input type="text" class="inputData" tabindex="1" dir="ltr" size="10" name="PayFromPD" id="PayFromPD" value="<%= sFromPD %>">
							<img src="../images/calendar_icon.gif" id="PayFromPDTrigger" width="16" height="15" border="0" style="cursor:pointer;">
							TO <input type="text" class="inputData" tabindex="2" dir="ltr" size="10" name="payToPD" id="payToPD" value="<%= sToPD %>">
							<img src="../images/calendar_icon.gif" id="payToPDTrigger" width="16" height="15" border="0" style="cursor:pointer;">
						</td>
					</tr>
					<tr>
						<td>
							<input type="radio" name="DateUsage" value="0" onclick="document.getElementById('PayFromPD').disabled=document.getElementById('payToPD').disabled=true;document.getElementById('PayFromDate').disabled=document.getElementById('payToDate').disabled=false;" />
							<span class="inputHeading">Trans Date</span> &nbsp;
						</td>
						<td>
							<input type="text" class="inputData" tabindex="1" dir="ltr" size="10" name="PayFromDate" id="PayFromDate" value="<%= sFromDate %>" disabled>
							<img src="../images/calendar_icon.gif" id="PayFromDateTrigger" width="16" height="15" border="0" style="cursor:pointer;">
							TO <input type="text" class="inputData" tabindex="2" dir="ltr" size="10" name="payToDate" id="payToDate" value="<%= sToDate %>" disabled>
							<img src="../images/calendar_icon.gif" id="PayToDateTrigger" width="16" height="15" border="0" style="cursor:pointer;">
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			<%
			Dim RREnable, RRAutoRet
			Set rsData5 = oledbData.execute("SELECT RREnable, RRAutoRet, PayingDaysMarginInitial, PayingDaysMargin, payingDates1, payingDates2, payingDates3 From tblCompany Where ID=" & nCompanyID)
			If Not rsData5.EOF Then
                RREnable = rsData5("RREnable")
                RRAutoRet = rsData5("RRAutoRet")
            End If 'do not close here    
			%>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="1">
					<tr>
						<td><span class="inputHeading">Payment Method</span></td>
						<td width="30"></td>
						<td><span class="inputHeading">Credit Type</span></td>
						<td width="30"></td>
						<td><span class="inputHeading">Rolling Reserve</span></td>
					</tr>
					<tr>
						<td>
							<input type="Checkbox" name="TypePaymentMethodCc" value="1" checked> Credit cards<br />
							<input type="Checkbox" name="TypePaymentMethodAdmin" value="1"> Admin trans<br />
						</td>
						<td></td>
						<td>
							<input type="Checkbox" name="TypeTransCharge" value="1" checked> Charge<br />
							<input type="Checkbox" name="TypeTransRefund" value="1" checked> Refund<br />
						</td>
						<td></td>
						<td>
							<input type="Checkbox" name="IncRR" value="1" onclick="return false;" <%=IIf(RREnable, "checked", "")%>> Reserve Hold<br />
							<input type="Checkbox" name="RetRR" value="1" onclick="return false;" <%=IIf(RRAutoRet, "checked", "")%> > Release<br />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="1">
					<tr>
						<td colspan="3"><span class="inputHeading">Include in pay</span></td>
					</tr>
					<tr>
						<td valign="top">
							<input type="Checkbox" name="isApprovalOnlyFee" value="1" checked> Approval Fee<br />
							<input type="Checkbox" name="AutoCHB" value="1" checked> Auto Debit CHB<br />
						</td>
						<td width="20"></td>
						<td valign="top">
							<input type="Checkbox" name="isFailFee" value="1" checked> Fail Fee<br />
							<input type="Checkbox" name="IncInstallemts" value="1" checked> Next Installemts<br />
						</td>
						<td width="20"></td>
						<td valign="top">
							<input type="Checkbox" name="isCcStorageFee" value="1" checked> Storage Fee<br />
							<input type="Checkbox" name="IncTestTrans" value="1"> Test Transactions<br />
						</td>
					</tr><tr>
						<td valign="top" colspan="5">
						    <input type="Checkbox" style="vertical-align:middle;" name="ForceEpaPaid" value="0" > Include overdue bank payments<br />
						</td>
					</tr>
					</table>
				    
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			<tr>
				<td>
					<%
					Dim strOldPaymentsList, lPayDate : lPayDate = ""
					sSQL = "SELECT TOP 10 tblTransactionPay.PayDate, tblTransactionPay.id FROM tblTransactionPay INNER JOIN tblWireMoney ON tblTransactionPay.id = tblWireMoney.WireSourceTbl_id AND tblWireMoney.WireType = 1 " &_
						"WHERE (tblTransactionPay.CompanyID = " & nCompanyID & ") AND (tblTransactionPay.isBillingPrintOriginal = 0) AND (tblTransactionPay.Currency = " & nCurrency & ") AND (tblWireMoney.WireStatus <= 2) ORDER BY tblTransactionPay.PayDate DESC"
					Set rsData6=oledbData.execute(sSQL)
					If Not rsData6.EOF Then lPayDate = Left(rsData6("PayDate"), 10)
					Do While Not rsData6.EOF
						sPayDate = day(rsData6("PayDate")) & "/" & month(rsData6("PayDate")) & "/" & right(year(rsData6("PayDate")),2) & "&nbsp;&nbsp;"
						sPayDate = sPayDate & FormatDateTime(rsData6("PayDate"),4)
						strOldPaymentsList = strOldPaymentsList & "<option value=""" & rsData6("ID") & """>" & rsData6("ID") & " - " & sPayDate & "</option>"
					  rsData6.movenext
					loop
					rsData6.Close
					%>
					<table border="0" cellspacing="0" cellpadding="1">
					<tr>
						<td>
							<input type="radio" name="PayUsage" value="0" onclick="document.getElementById('PayID').disabled=true;document.getElementById('PaymentDate').disabled=false;" checked />
							<span class="inputHeading">New Payment</span> &nbsp;
						</td>
						<td>
							<select name="PaymentDate" id="PaymentDate" class="inputData" onchange="options[selectedIndex].onclick();">
								<option value="<%=Date%>" onclick=""> <%=Date%> - NOW
								<%
								If Not rsData5.EOF Then
									If rsData5("PayingDaysMargin") > 0 Then 
										nChecked = " selected "
										If lPayDate <> "" Then 
											lPayDate = CDate(lPayDate)
										Else 
											lPayDate = DateAdd("d", rsData5("PayingDaysMarginInitial"), sFromPD)
											Response.Write("<option value=""" & lPayDate & """ selected onclick=""document.getElementById('payToPD').value='" & DateAdd("d", -rsData5("PayingDaysMarginInitial"), lPayDate) & "'"">" & lPayDate)
											nChecked = ""
										End If	
										For i = 1 To 10
											lPayDate = DateAdd("d", rsData5("PayingDaysMargin"), lPayDate)
											Response.Write("<option " & nChecked & " value=""" & lPayDate & """ onclick=""document.getElementById('payToPD').value='" & DateAdd("d", -rsData5("PayingDaysMargin"), lPayDate) & "'"">" & lPayDate)
											nChecked = ""
										Next	
									Else
										lPayDate = Date
										For x = 1 To 3
											For i = 1 To 3
												IsDayInRange = TestNumVar(Mid(rsData5("payingDates" & i), 5, 2), 0, -1, 0)
												nDate = DateSerial(Year(lPayDate), Month(lPayDate), IsDayInRange)
												If nDate < lPayDate Then nDate = DateAdd("m", 1, nDate)
												If IsDayInRange <> 0 Then Response.Write("<option value=""" & lPayDate & """ " & nChecked & " onclick=""document.getElementById('payToPD').value='" & DateSerial(Year(nDate), Month(nDate) - 1, TestNumVar(Mid(rsData5("payingDates" & i), 3, 2), 0, -1, 0)) & "'"">" & nDate)
											Next
											lPayDate = DateAdd("m", 1, lPayDate)
										Next	
									End if
								End If
								rsData5.Close()
								%>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<input type="radio" name="PayUsage" value="1" onclick="document.getElementById('PayID').disabled=false;document.getElementById('PaymentDate').disabled=true;" />
							<span class="inputHeading">Existing Payment</span> &nbsp;
						</td>
						<td>
							<select name="PayID" id="PayID" class="inputData" disabled >
								<%=strOldPaymentsList%>
							</select>
						</td>
						<%
							if nCurrency=0 then
								%>
									<td> &nbsp; <input type="Checkbox" style="vertical-align:middle;" name="CreateInvoice" value="1">Create invoice</td>
								<%
							end if
						%>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="1">
					<tr>
						<td valign="top">
							<span class="inputHeading">Max Amount</span><br />
							<select name="selectTop" class="inputData" style="width:80px;">
								<option value="">All</option>
								<option value="500">500</option>
								<option value="1000">1000</option>
								<option value="2000" selected="selected">2000</option>
								<option value="3000">3000</option>
								<option value="4000">4000</option>
								<option value="5000">5000</option>
							</select>
						</td>
						<td width="30"></td>
						<td><br /><input type="submit" class="buttonRed" value="PAY <%=uCase(CurrArray(nCurrency))%>"></td>	
					</tr>
					</table>
				</td>
			</tr>
		</table>
		<script language="javascript">
			Calendar.setup({
				inputField	 :	"payFromDate",	 // id of the input field
				ifFormat	   :	"%d/%m/%Y",	  // format of the input field
				button		 :	"PayFromDateTrigger",  // trigger for the calendar (button ID)
				align		  :	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true,
				mondayFirst	:	false,
				weekNumbers	:	false
			});
			Calendar.setup({
				inputField	 :	"payToDate",	 // id of the input field
				ifFormat	   :	"%d/%m/%Y",	  // format of the input field
				button		 :	"PayToDateTrigger",  // trigger for the calendar (button ID)
				align		  :	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true,
				mondayFirst	:	false,
				weekNumbers	:	false
			});
			Calendar.setup({
				inputField	 :	"payFromPD",	 // id of the input field
				ifFormat	   :	"%d/%m/%Y",	  // format of the input field
				button		 :	"payFromPDTrigger",  // trigger for the calendar (button ID)
				align		  :	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true,
				mondayFirst	:	false,
				weekNumbers	:	false
			});
			Calendar.setup({
				inputField	 :	"payToPD",	 // id of the input field
				ifFormat	   :	"%d/%m/%Y",	  // format of the input field
				button		 :	"payToPDTrigger",  // trigger for the calendar (button ID)
				align		  :	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true,
				mondayFirst	:	false,
				weekNumbers	:	false
			});
		</script>
		</form>
	</fieldset>
	<br />
	<%
	function displayTerminal(terminalNum)
		sSQL="SELECT dt_name FROM tblDebitTerminals WHERE terminalNumber='" & trim(terminalNum) & "'"
		set rsDataTmp = oledbData.execute(sSQL)
		if not rsDataTmp.EOF then
			response.write rsDataTmp("dt_name") & " <span class=txt11>(" & terminalNum & ")</span>"
		else
			response.write "<span class=txt11>" & trim(terminalNum) & " (" & terminalNum & ")</span>"
		end if
		rsDataTmp.close
		Set rsDataTmp = nothing	
	end function
End if

call closeConnection()
%>
</body>
</html>
