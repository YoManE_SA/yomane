<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_controls.asp"-->
<!--#include file="../include/security_class.asp"-->
<!--#include file="../include/const_globalData.asp" -->
<!--#include file="../include/func_balance.asp"-->

<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<style type="text/css">
		legend { color:gray; font-size:11px; padding:6px; }
	</style>
</head>
<body style="margin:0; background-color:#eeeeee;" class="itextm3">
<%

company_id = dbText(request("company_id"))

sSQL="SELECT tblParentCompany.pc_Code, tblCompany.ID AS companyID, tblCompany.CustomerNumber, tblCompany.CompanyName, tblCompany.comment, tblCompany.url, " &_
"PayingDaysMarginInitial, PayingDaysMargin, payingDates1, payingDates2, payingDates3, NoteText, AccountNote.InsertDate, InsertUser, IsNull(mg_Name, 'none') GroupName, " &_
"GD_Text IndustryName, careOfAdminUser," &_
"(SELECT COUNT(tblTransactionPay.id) FROM tblTransactionPay " &_
"WHERE tblTransactionPay.CompanyID = tblCompany.ID) AS numTimesPay, " &_
"" &_
"(SELECT COUNT(tblCompanyTransPass.id) FROM tblCompanyTransPass " &_
"WHERE (tblCompanyTransPass.CompanyID=tblCompany.ID) AND (tblCompanyTransPass.PayID=';X;')) AS numTransArchive, " &_
"" &_
"(SELECT COUNT(AccountNote_id) FROM Data.AccountNote WHERE AccountNote.Account_id = Account.Account_id) AS NoteCount " &_
"" &_
"FROM tblCompany " & _
"LEFT JOIN Data.Account ON tblCompany.ID = Account.Merchant_id " &_
"LEFT JOIN tblParentCompany ON tblCompany.ParentCompany = tblParentCompany.ID " &_
"LEFT JOIN tblMerchantGroup ON tblCompany.GroupID = tblMerchantGroup.ID " &_
"LEFT JOIN GetGlobalData(51) ON CompanyIndustry_id = GD_ID " &_
"LEFT JOIN Data.AccountNote ON AccountNote.Account_id = Account.Account_id " &_
"WHERE tblCompany.id = " & company_id
set rsData = oledbData.Execute(sSQL)
If not rsData.EOF Then
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top">
			<fieldset>
				<legend>LINKS</legend>
				<table class="formThin" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td class="txt10">
						<li type="circle" style="padding-bottom:2px;">
							<a href="outer_login.aspx?CompanyID=<%=company_id%>" target="_blank" class="go">MERCHANT CONTROL PANEL</a>
							<a href="outer_login.aspx?target=test&CompanyID=<%=company_id%>" target="_blank" class="go">(TEST)</a>
						</li>
						<br />
						<li type="circle" style="padding-bottom:2px;">
							<a href="outer_login.aspx?target=devcenter&CompanyID=<%=company_id%>" target="_blank" class="go">MERCHANT DEVELOPER CENTER</a>
							<a href="outer_login.aspx?target=devcentertest&CompanyID=<%=company_id%>" target="_blank" class="go">(TEST)</a>
						</li>
						<br />
						<li type="circle" style="padding-bottom:2px;"><a class="go" href="#" onclick="top.fraBrowser.frmBody.location.href='Merchant_BalanceData.aspx?ShowCompanyID=<%=company_id%>&isAllCompanys=0&PageSize=25';parent.parent.fraButton.FrmResize();return false;">Account Balance</a>
						<%
    					sTmp=""
	    				For Each t In fn_GetBalances(company_id).Items
							'sSQL="IF EXISTS (SELECT * FROM tblCompanyBalance WHERE currency=" & t & " AND company_id=" & company_id & ")" & _
							'" SELECT balanceExpected FROM tblCompanyBalance WHERE currency=" & t & " AND company_id=" & company_id & " AND" & _
							'" insertDate=(SELECT Max(InsertDate) FROM tblCompanyBalance WHERE currency=" & t & " AND company_id=" & company_id & ")" & _
							'" ELSE SELECT '-'"
							'nValue=trim(ExecScalar(sSQL, 0))
							sTmp=sTmp & ", <a class=""go"" href=""#"" onclick=""top.fraBrowser.frmBody.location.href='Merchant_BalanceData.aspx?ShowCompanyID=" & company_id & "&isAllCompanys=0&PageSize=25&Currency=" & t.CurrencyId & "';parent.parent.fraButton.FrmResize();return false;"">" & FormatCurr(t.CurrencyId, t.Expected) & "</a>"
						Next
						if sTmp<>"" then response.Write("<br />&nbsp;&nbsp;&nbsp;&nbsp;" & trim(mid(sTmp, 2)))
                        %>
                        </li><br />
                        <%
						Response.Write("<li type=""circle"" style=""padding-bottom:2px;"">")
						if int(rsData("numTransArchive"))>0 then
							%>
							<a class="go" target="fraBrowser" href="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=<%=Server.UrlEncode("Trans_admin_regularFilter.aspx?FilterMerchantDefaultID=" & rsData("companyID") & "&Payout=A" )%>&pageBodyUrl=<%=Server.UrlEncode("Trans_admin_regularData.aspx?ShowCompanyID=" & rsData("companyID") & "&Payout=A&chk2=1&fromDate=" & Server.UrlEncode(DateAdd("m", -6, Now)) )%>">
							    Archive</a> (<%= int(rsData("numTransArchive")) %>)
							<%
						else
							Response.Write("<span class=""txt11"" style=""color:#a1a1a1;"">Archive (0)</span>")
						End If
						Response.Write("</li><br />")
						response.write "<li type=""circle"" style=""padding-bottom:2px;""><a href=""charge_browser.asp?companyID="& company_id &""" target=""fraBrowser"" class=""go"">Virtual terminal</a></li><br />"
						response.write "<li type=""circle"" style=""padding-bottom:2px;""><a href=""javascript:top.fraBrowser.frmBody.location.href='Report_Merchant.aspx?companyID=" & company_id & "'; top.fraBrowser.fraButton.FrmResize(); undefined;"" class=""go"">Monthly Report</a></li><br />"
						response.write "<li type=""circle"" style=""padding-bottom:2px;""><a href=""MerchantChbHistory.aspx?ID=" & company_id & """ target=""frmBody"" class=""go"">Chargeback History</a></li><br />"
						%>
					</td>
				</tr>
				</table>
			</fieldset>
			<fieldset>
				<legend>INFO</legend>
				<table class="formThin" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td class="txt11">  
						<%
						response.write "<span style=""color:#006699;"">Parent company:</span> " & Trim(rsData("pc_Code")) & "<br />"
						response.write "<span style=""color:#006699;"">Merchant group:</span> " & Trim(rsData("GroupName")) & "<br />"
						response.write "<span style=""color:#006699;"">Merchant number:</span> " & Trim(rsData("CustomerNumber")) & "<br />"
						response.write "<span style=""color:#006699;"">Industry:</span> " & Trim(rsData("IndustryName")) & "<br />"
						response.write "<span style=""color:#006699;"">Account manager:</span> " & Trim(rsData("careOfAdminUser")) & "<br />"
						If trim(rsData("url"))<>"" then
							response.write "<span style=""color:#006699;"">Website:</span> "
							UrlArray = Split(trim(rsData("url")),chr(13) & chr(10))
							For i = Lbound(UrlArray) to Ubound(UrlArray)
								sUrlLink = Trim(UrlArray(i))
								If sUrlLink <> "" Then
									If InStr(sUrlLink,"://")=0 Then sUrlLink = "http://" & sUrlLink
									response.write("<a href="""& sUrlLink & """ target=""_blank"" class=""go"">" & dbTextShow(sUrlLink) & "</a><br />")
								End if
							Next
						End if
						If trim(rsData("comment"))<>"" then response.write "<span style=""color:#006699;"">Activities:</span> " & dbTextShow(rsData("comment")) & "<br />"
						if trim(rsData("NoteText"))<>"" then
							if DateDiff("d", rsData("InsertDate"), "08/08/08")<0 then
								sDate=FormatDateTime(rsData("InsertDate"), 2)
							else
								sDate=""
							end if
							sUser=trim(rsData("InsertUser"))
							response.write "<div style=""color:#006699;"">Last note:</div>"
							response.Write "<div style=""font-size:11px;"">" & dbTextShow(rsData("NoteText")) & "</span></div>"
							if sDate<>"" or sUser<>"" then response.Write "<div style=""font-size:10px;"">(" & trim(sUser & " " & sDate) & ")</div>"
							if rsData("NoteCount")>1 then response.Write "<a class=""go"" href=""#"" onclick=""parent.parent.frmBody.location.href='merchant_dataNotes.aspx?companyID=" & company_id & "';parent.parent.fraButton.FrmResize();return false;"">See all notes</a>"
						end if
						%>
					</td>
				</tr>
				</table>
			</fieldset>
		</td>
	    <td valign="top" style="padding-left:15px;">
			<fieldset>
				<legend>SETTLEMENTS</legend>
				<table class="formThin" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td class="txt11">  
						<%
						If int(rsData("numTimesPay"))>0 Then
							%>
							<li type="circle" style="padding-bottom:2px;">
							<a class="go" href="javascript:top.fraBrowser.frmBody.location.href='Merchant_transSettledListData.asp?Source=filter&ShowCompanyID=<%= rsData("companyID") %>'; top.fraBrowser.fraButton.FrmResize(); undefined;">Settlement Reports</a>
							(<%= int(rsData("numTimesPay")) %>)</li><br /><br />
							Last Settlement:<br />
							<%
							Set rsDataTmp = server.createobject("adodb.recordset")
							GetConstArray rsDataTmp, GGROUP_WireStatus, 1, "", WireStatusText, VbNull
							
							sSQL="SELECT TOP 1 tblTransactionPay.id AS TransactionPay_id, tblTransactionPay.PayDate, tblTransactionPay.TransPayTotal, " & _
								"tblTransactionPay.Currency, tblTransactionPay.transTotal, tblTransactionPay.TransChargeTotal, tblWireMoney.WireStatus " & _
								"FROM tblTransactionPay LEFT JOIN tblWireMoney ON (tblTransactionPay.ID=tblWireMoney.WireSourceTbl_id And tblWireMoney.WireType = 1) " & _
								"WHERE tblTransactionPay.CompanyID=" & company_id & " AND tblTransactionPay.PayDate=" & _
								"(SELECT Max(PayDate) FROM tblTransactionPay WHERE CompanyID=" & company_id & ")" 
							set rsDataTmp = oledbData.Execute(sSQL)
							If not rsDataTmp.EOF Then
								Response.Write("<span style=""color:#006699;"">Date:</span> " & FormatDatesTimes(rsDataTmp("PayDate"),1,1,0) & "<br />")
								Response.Write("<span style=""color:#006699;"">Number:</span> " & rsDataTmp("TransactionPay_id") & "<br />")
								Response.Write("<span style=""color:#006699;"">Amount:</span> " & FormatCurr(rsDataTmp("Currency"), rsDataTmp("transPayTotal")) & "<br />")
								Response.Write("<span style=""color:#006699;"">Wire Status:</span> ")
								If IsNull(rsDataTmp("WireStatus")) Then
									Response.Write("---")
								ElseIf rsDataTmp("WireStatus") < LBound(WireStatusText) Or rsDataTmp("WireStatus") > UBound(WireStatusText) Then
									Response.Write("Status: " & rsDataTmp("WireStatus"))
								Else
									Response.Write(WireStatusText(rsDataTmp("WireStatus")))
								End If
								Response.Write("<br />")
							End if
							rsDataTmp.close
							Set rsDataTmp = nothing
						Else
							Response.Write("<span class=""txt11"" style=""color:#a1a1a1;"">Settlement Reports (0)</span></li><br />")
						End If
						%>
					</td>
				</tr>
				</table>
			</fieldset>
			<fieldset>
				<legend>PAYOUTS</legend>
				<table class="formThin" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td colspan="2" nowrap="nowrap">
						<span style="color:#006699;">Initial holding margin:</span> <%= rsData("PayingDaysMarginInitial") %> Days<br />
						<span style="color:#006699;">Holding margin:</span> <%= rsData("PayingDaysMargin") %> Days<br />
					</td>
				</tr>
				<%
				if len(trim(rsData("payingDates1")))=6 then
					sfromDay1 = mid(trim(rsData("payingDates1")),1,2)
					stoDay1 = mid(trim(rsData("payingDates1")),3,2)
					spayday1 = mid(trim(rsData("payingDates1")),5,2)
				else
					sfromDay1=00 : stoDay1=00 : spayday1=00
				End if
				if len(trim(rsData("payingDates2")))=6 then
					sfromDay2 = mid(trim(rsData("payingDates2")),1,2)
					stoDay2 = mid(trim(rsData("payingDates2")),3,2)
					spayday2 = mid(trim(rsData("payingDates2")),5,2)
				else
					sfromDay2=00 : stoDay2=00 : spayday2=00
				end if
				if len(trim(rsData("payingDates3")))=6 then
					sfromDay3 = mid(trim(rsData("payingDates3")),1,2)
					stoDay3 = mid(trim(rsData("payingDates3")),3,2)
					spayday3 = mid(trim(rsData("payingDates3")),5,2)
				else
					sfromDay3=00 : stoDay3=00 : spayday3=00
				end if
				%>
				<tr><td colspan="2" style="color:#006699;">Transactions Period:</td></tr>
				<tr>
					<th style="font-size:10px; font-weight:normal; color:#000000;">Between</th>
					<th style="font-size:10px; font-weight:normal; color:#000000;">Credit On</th>
				</tr>
				<tr><td><%= sfromDay1 %> and <%= stoDay1 %></td><td><%= spayday1 %></td></tr>
				<tr><td><%= sfromDay2 %> and <%= stoDay2 %></td><td><%= spayday2 %></td></tr>
				<tr><td><%= sfromDay3 %> and <%= stoDay3 %></td><td><%= spayday3 %></td></tr>
				</table>
			</fieldset>
			<br /><br />
			<%If Request("Currency") <> "" And Request("PayToPD") <> "" Then %>
				<input type="submit" onclick="if (!this.disabled) window.parent.parent.frmBody.document.location = 'merchant_PayCreate.asp?companyID=<%=company_id%>&currency=<%=Request("Currency")%>&PaymentDate=<%=Request("PaymentDate")%>&PayToPD=<%=Request("PayToPD")%>&MaxTransAmount=<%=Request("MaxTransAmount")%>&Type=ExecAll&AutoCHB=1&IncInstallemts=1&IncRR=1&RetRR=1&isApprovalOnlyFee=1&isFailFee=1&isCcStorageFee=1&TypeTransCharge=1&TypeTransRefund=1&TypePaymentMethodCc=1&TypePaymentMethodAdmin=1&CreateInvoice=0&ForceEpaPaid=0'; this.disabled=true;" value="Create New Payment (<%=GetCurISO(Request("Currency"))%>)" /><br />
				<input type="submit" onclick="if (!this.disabled) window.parent.parent.frmBody.document.location = 'merchant_PayCreate.asp?companyID=<%=company_id%>&currency=<%=Request("Currency")%>&PaymentDate=<%=Request("PaymentDate")%>&PayToPD=<%=Request("PayToPD")%>&MaxTransAmount=<%=Request("MaxTransAmount")%>&Type=ExecPerPM&AutoCHB=1&IncInstallemts=1&IncRR=1&RetRR=1&isApprovalOnlyFee=1&isFailFee=1&isCcStorageFee=1&TypeTransCharge=1&TypeTransRefund=1&TypePaymentMethodCc=1&TypePaymentMethodAdmin=1&CreateInvoice=0&ForceEpaPaid=0'; this.disabled = true;" value="Create New Payment (<%=GetCurISO(Request("Currency"))%>) Per Method" /><br />
				<% If PageSecurity.IsMemberOf("Dev. Support") Then %>
					<input type="submit" onclick="if(!this.disabled) window.parent.parent.frmBody.document.location='merchant_PayCreateV2.aspx?companyID=<%=company_id%>&currency=<%=Request("Currency")%>&PaymentDate=<%=Request("PaymentDate")%>&PayToPD=<%=Request("PayToPD")%>&MaxTransAmount=<%=Request("MaxTransAmount")%>&Type=ExecAll&AutoCHB=1&IncInstallemts=1&IncRR=1&RetRR=1&isApprovalOnlyFee=1&isFailFee=1&isCcStorageFee=1&TypeTransCharge=1&TypeTransRefund=1&TypePaymentMethodCc=1&TypePaymentMethodAdmin=1&CreateInvoice=0&ForceEpaPaid=0'; this.disabled=true;" value="Create New Payment (<%=GetCurISO(Request("Currency"))%>) V2" /><br />
					<input type="submit" onclick="if(!this.disabled) window.parent.parent.frmBody.document.location='merchant_PayCreateV3.aspx?companyID=<%=company_id%>&currency=<%=Request("Currency")%>&PaymentDate=<%=Request("PaymentDate")%>&PayToPD=<%=Request("PayToPD")%>&MaxTransAmount=<%=Request("MaxTransAmount")%>&Type=ExecAll&AutoCHB=1&IncInstallemts=1&IncRR=1&RetRR=1&isApprovalOnlyFee=1&isFailFee=1&isCcStorageFee=1&TypeTransCharge=1&TypeTransRefund=1&TypePaymentMethodCc=1&TypePaymentMethodAdmin=1&CreateInvoice=0&ForceEpaPaid=0'; this.disabled=true;" value="Create New Payment (<%=GetCurISO(Request("Currency"))%>) V3" />
				<% End If %>
			<%End if%>
	    </td>
	</tr>
	</table>
	<br />
	<%
End If

rsData.close
Set rsData = nothing
call closeConnection()
%>
</body>
</html>