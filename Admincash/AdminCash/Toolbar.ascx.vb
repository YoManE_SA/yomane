﻿< ParseChildren(true, "ItemControls") > _
Partial Class Controls_Toolbar
    Inherits System.Web.UI.UserControl
    Private _activeItem As Integer, sWidth As String = "100%"
	
    Public Function AddItem(ByVal sTitle As String, ByVal sLink As String, ByVal bIsCurrent As Boolean) As Integer
        Dim nItemCount As Integer = ItemCount
	    If ItemCount > 0 Then litItems.Text &= "<td class=""toolbarSeparator"">|</td>"
	    litItems.Text &= "<td title=""" & sLink & """ onclick=""location.href='" & sLink & "';"""
	    If bIsCurrent Then litItems.Text &= " class=""current"""
	    litItems.Text &= ">" & sTitle.ToUpper & "</td>"
	    tdChildObj.ColSpan = ((nItemCount + 1) * 2)
	    If bIsCurrent Then _activeItem = nItemCount
	    Return nItemCount
    End Function
	
    Public Function AddItem(ByVal sTitle As String, ByVal sLink As String) As Integer
		Dim bSelected As Boolean = sLink.ToLower.Contains(System.IO.Path.GetFileName(Request.ServerVariables("SCRIPT_NAME").ToLower))
		Return AddItem(sTitle, sLink, bSelected)
	End Function

    Public ReadOnly Property ItemControls() As System.Web.UI.Control
        Get
            Return tdChildObj
        End Get
    End Property

    Public ReadOnly Property ActiveItem() As Integer
	    Get
	        Return _activeItem
	    End Get
    End Property
    
    Public Sub Clear()
        _activeItem = -1
        litItems.Text = ""
        tdChildObj.ColSpan = -1
    End Sub

    Public Property Width() As String
	    Get
		    Return sWidth
	    End Get
	    Set(ByVal value As String)
		    sWidth = value
		    If sWidth.EndsWith("%") And sWidth.Length > 1 Then
			    lblContainer.Width = System.Web.UI.WebControls.Unit.Percentage(sWidth.Substring(0, sWidth.Length - 1))
		    ElseIf dbPages.TestVar(sWidth, 1, 0, 0) > 0 Then
			    lblContainer.Width = System.Web.UI.WebControls.Unit.Pixel(sWidth)
		    End If
	    End Set
    End Property

    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        trChildObj.Visible = (tdChildObj.Controls.Count > 0)
        MyBase.OnPreRender(e)
    End Sub
    
    Public ReadOnly Property ItemCount() As Integer
	    Get
	        Return (dbPages.TestVar(tdChildObj.ColSpan, 0, 1000, 0) / 2)
	    End Get
    End Property

    Public Property Align() As String 
        Get
            return dvTabs.Style.Item("text-align")
        End Get
        Set
            dvTabs.Style.Item("text-align") = Value
        End Set
    End Property
	
    Sub Page_Load()
	    lblContainer.Visible = Not String.IsNullOrEmpty(litItems.Text)
    End Sub


	'Public Sub LoadAccountTabs(accountID As Integer)
	'	AddItem("Stored Payment Methods", "../Accounts/StoredPaymentMethods.aspx?accountId=" & accountId)
	'	AddItem("Files And Notes", "../Accounts/FilesAndNotes.aspx?accountId=" & accountId)
	'	AddItem("Fees", "../Accounts/Fees.aspx?accountId=" & accountId)
	'End Sub

	'Public Sub LoadMerchantTabs(accountId As Integer)
	'	AddItem("Summary", "../Merchants/Summary.aspx?accountId=" & accountId)
	'	AddItem("Details", "../Merchants/Data.aspx?accountId=" & accountId)
	'	LoadAccountTabs(accountId)
	'	AddItem("Process Settings", "../Merchants/ProcessSettings.aspx?accountId=" & accountId)
	'	AddItem("Hosted Settings", "../Merchants/HostedSettings.aspx?accountId=" & accountId)
	'	AddItem("Risk Settings", "../Merchants/RiskSettings.aspx?accountId=" & accountId)
	'	AddItem("Terminals", "../Merchants/TerminalSettings.aspx?accountId=" & accountId)
	'	AddItem("Mobile Settings", "../Merchants/MobileSettings.aspx?accountId=" & accountId)
	'End Sub


	Public Sub LoadCustomerTabs(customer As Integer)
		AddItem("General Data", "../Customers/Data.aspx?id=" & customer)
		AddItem("Shipping Address", "../Customers/ShippingAddresses.aspx?id=" & customer)
		'AddItem("Files And Notes", "../Accounts/FilesAndNotes.aspx?accountId=" & accountId)
        AddItem("Balance", "../Accounts/Balance.aspx?id=" & customer)
        AddItem("Stored Payment Methods", "../Accounts/StoredPaymentMethods.aspx?id=" & customer)
    End Sub

	Public Sub LoadPartnerTabs(affiliateId As Integer)
		AddItem("General Data", "Partners_data.aspx?id=" & affiliateId)
		AddItem("Login Details", "Partners_Login.aspx?id=" & affiliateId)
		AddItem("Bank Accounts", "Partners_BankAccounts.aspx?id=" & affiliateId)
		AddItem("Merchants", "Partners_Companies.aspx?id=" & affiliateId)
		AddItem("Fees", "Partners_fees.aspx?id=" & affiliateId)
        'AddItem("Fees2", "Partners_Fees2.aspx?id=" & affiliateId)
        AddItem("Settlements", "Partners_Payments.aspx?AffiliateID=" & affiliateId)
        AddItem("Log Refferals", "Partners_Refferals.aspx?AffiliateID=" & affiliateId)
    End Sub

    Public Sub LoadDebitComapnyTabs(debitCompanyId As Integer)
        AddItem("Debit Company Details", "system_debitCompanyData.asp?debitCompany_id=" & debitCompanyId & "&ShowSec=1")
        AddItem("Fees & Payouts", "system_debitCompanyData.asp?debitCompany_id=" & debitCompanyId & "&ShowSec=2")
        'AddItem("Fees2", "system_debitCompany_Fees2.aspx?debitCompany_id=" & debitCompanyId)
        AddItem("Refunds Ask Log", "system_debitCompanyData.asp?debitCompany_id=" & debitCompanyId & "&ShowSec=3")
        AddItem("Users", "system_debitCompanyData.asp?debitCompany_id=" & debitCompanyId & "&ShowSec=4")
        AddItem("Notifications", "system_DebitCompanyRules.aspx?DebitCompany=" & debitCompanyId)
    End Sub


End Class
