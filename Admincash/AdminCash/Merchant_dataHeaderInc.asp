<%
Set rsDataTmp=server.createobject("adodb.recordset")
GetConstArray rsDataTmp, GGROUP_COMPANYSTATUS, 1, "", MerchantStatus, MStatusColor
                    
sSQL = "SELECT CustomerNumber, CompanyName, CompanyLegalNumber, IDNumber," & _
	" FirstName, LastName, Phone, CompanyFax, Cellular, Mail, URL, ActiveStatus" & _
	" FROM tblCompany WHERE ID=" & nCompanyID
set rsDataTmp=oledbData.Execute(sSQL)
if not rsDataTmp.EOF then
	nStatus = rsDataTmp("ActiveStatus")
	sCompanyName = rsDataTmp("CompanyName")
	%>
	<script language="javascript" type="text/javascript">
		function OpenBlacklistSearch() {
			var sOpenURL="merchant_blacklist.aspx?FilterSource=Merchant&FilterType=OR";
			sOpenURL+="&MerchantID=<%= nCompanyID %>";
			sOpenURL+="&MerchantNumber=<%= rsDataTmp("CustomerNumber") %>"
			sOpenURL+="&MerchantName=<%= rsDataTmp("CompanyName") %>"
			sOpenURL+="&LegalNumber=<%= rsDataTmp("CompanyLegalNumber") %>"
			sOpenURL+="&IDNumber=<%= rsDataTmp("IDNumber") %>"
			sOpenURL+="&FirstName=<%= rsDataTmp("FirstName") %>"
			sOpenURL+="&LastName=<%= rsDataTmp("LastName") %>"
			sOpenURL+="&Phone=<%= rsDataTmp("Phone") %>"
			sOpenURL+="&Fax=<%= rsDataTmp("CompanyFax") %>"
			sOpenURL+="&Cellular=<%= rsDataTmp("Cellular") %>"
			sOpenURL+="&Mail=<%= rsDataTmp("Mail") %>"
			sOpenURL+="&URL=<%= Replace(Replace(rsDataTmp("URL"), chr(10), ""), chr(13), " ") %>"
			var winBlacklistSearch=window.open(sOpenURL, "fraBlacklistSearch", "top=120,left=50,width="+(screen.width-100)+",height="+(screen.height-240)+",scrollbars=auto,resizable");
		}
	</script>
	<%
end if
rsDataTmp.Close
%>
<tr>
	<td bgcolor="white" valign="top" class="txt12" style="color:black;">
		<table width="100%" border="0" cellspacing="0" cellpadding="2" dir="ltr">
			<tr>
				<td id="pageMainHeadingTd">
					<span id="pageMainHeading">MERCHANT MANAGEMENT</span> -
					<span class="txt14">
						<%= nCompanyID & " " & dbtextShow(sCompanyName) %>
						| <%=MerchantStatus(nStatus)%> (<a href="Merchant_Activities.aspx?Merchant=<%= nCompanyID %>" style="font-size:14px;">change</a>)</span>
				</td>
				<td align="right">
					<a href="javascript:OpenBlacklistSearch();" class="alwaysactive"><img src="../images/post_button_blacklist.gif"  border="0" title="Check data in blacklist" align="middle" /></a>
					<a href="FileArchiveData.aspx?MerchantID=<%= nCompanyID %>" class="alwaysactive"><img src="../images/post_button_documents.gif" width="25" height="21" border="0" title="Save Documents" align="middle"></a>
					<a href="charge_browser.asp?companyID=<%= nCompanyID %>" target="fraBrowser" class="alwaysactive"><img src="../images/post_button_4.gif" title="Make Transactions" width="25" height="21" border="0" align="middle"></a>
					<a href="outer_login.aspx?CompanyID=<%= nCompanyID %>" target="_blank" class="alwaysactive"><img src="../images/post_button_1.gif" title="Merchant Area" width="40" height="21" border="0" align="middle"></a><br/>
				</td>
			</tr>
		</table>
	</td>
</tr>