<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
    '
    ' 20080513 Tamir
    '
    ' Security management
    '
    Sub Page_Load()
        If Security.IsAdmin Then
            lblPermission.Text = "<s" & "cript language=""Javascript"" type=""text/javascript"" src=""../js/security.js"">"
            lblPermission.Text &= "</s" & "cript>"
            lblPermission.Text &= "<span class=""security securityManaged"" title=""Click to manage permissions"" onclick=""OpenSecurityManagerURL('" & Request("PageURL") & "');"">"
        Else
            lblPermission.Text = "<span class=""security"">"
        End If
        lblPermission.Text &= "- </span>&nbsp;" & dbPages.ExecScalar("SELECT IsNull(IsNull(sd_Title, sd_URL), 'Untitled') FROM tblSecurityDocument WHERE sd_URL='" & dbPages.DBText(Request("PageURL")) & "'") & " [" & Request("PageURL") & "]"
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security Message: Access Denied</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<form id="frmServer" runat="server">
		<h1><asp:Label ID="lblPermission" runat="server" /></h1>
		<div class="bordered">
			<%=Security.Username%>, you are not authorized to view this page.
		</div>
		<script language="javascript" type="text/javascript">
			function BackOrClose()
			{
				if (this!=top) history.back(); else close();
			}
			function Retry()
			{
				location.replace("<%= request("PageURL") %>");
			}
		</script>
		<input type="button" class="buttonWhite" value="Back" onclick="BackOrClose();" />
		<input type="button" class="buttonWhite" value="Retry" onclick="Retry();" />
		<%
			If Security.IsAdmin Then
				%>
					<input type="button" class="buttonWhite" value="Manage" onclick="OpenSecurityManagerURL('<%= request("PageURL") %>');" />
				<%
			End If
		%>
	</form>
</body>
</html>