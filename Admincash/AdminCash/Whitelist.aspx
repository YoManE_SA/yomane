<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Import namespace="System.Data"%>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<script runat="server">
	Sub AddRecord(ByVal o As Object, ByVal e As EventArgs)
		lblError.Text = String.Empty
		lblNoError.Text = String.Empty
		Dim nMerchant As Integer = dbPages.TestVar(ucMerchant.SelectedValue, 1, 0, 0)
		If nMerchant = 0 Then lblError.Text &= "Merchant is not specified. "
		Dim sNumber As String = dbPages.DBText(txtNumber.Text, 25)
		If String.IsNullOrEmpty(sNumber) Then lblError.Text &= "Card Number is not specified. "
		Dim sHolder As String = dbPages.DBText(txtHolder.Text, 100)
		If String.IsNullOrEmpty(sHolder) Then lblError.Text &= "Card Holder is not specified. "
		Dim nMonth As Integer = dbPages.TestVar(ddlMonth.SelectedValue, 1, 0, 0)
		Dim nYear As Integer = dbPages.TestVar(ddlYear.SelectedValue, 1, 0, 0)
		If nYear < Date.Now.Year Then lblError.Text &= "Card is no more valid. "
		If nYear = Date.Now.Year And nMonth < Date.Now.Month Then lblError.Text &= "Card is no more valid. "
		Dim nLevel As Integer = dbPages.TestVar(ddlLevel.SelectedValue, 1, 0, 0)
		Dim sComment As String = dbPages.DBText(txtComment.Text, 200)
		If String.IsNullOrEmpty(lblError.Text) Then
			Dim sbSQL As New StringBuilder("DECLARE @nRC int;EXEC @nRC=AddCreditCardWhiteList " & nMerchant)
			sbSQL.AppendFormat(", '{0}', {1}, {2}", sNumber, nMonth, nYear)
			sbSQL.AppendFormat(", '{0}', {1}, '{2}'", sHolder, nLevel, sComment)
			sbSQL.AppendFormat(", '{0}'", Request.UserHostAddress)
			Dim nRC As Integer = dbPages.ExecScalar(sbSQL.Append(";SELECT @nRC;").ToString)
			If nRC < 0 Then
				lblError.Text &= "Failed to add the credit card to the whitelist. Error " & nRC
				lblError.Text &= ": " & GlobalData.Value(GlobalDataGroup.WHITE_LIST_REPLY, -nRC).Text
			Else
				lblNoError.Text = "The credit card has been successfuly added to the whitelist. Record ID: "
				lblNoError.Text &= "<a href=""Whitelist.aspx?PageSize=1&ID=" & nRC & """>" & nRC & "</a>"
				txtNumber.Text = String.Empty
				txtHolder.Text = String.Empty
				ddlMonth.SelectedIndex = 0
				ddlYear.SelectedIndex = 0
				ddlLevel.SelectedIndex = 0
				txtComment.Text = String.Empty
				dsData.DataBind()
			End If
		End If
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsData.ConnectionString = dbPages.DSN
		If Not Page.IsPostBack Then
			litLegend.Text = "<b>Level:&nbsp;</b>"
			For Each gdvTemp As GlobalDataValue In GlobalData.GroupValues(GlobalDataGroup.WHITE_LIST_LEVEL)
				litLegend.Text &= " <span style=""background-color:" & gdvTemp.Color & ";"">&nbsp;&nbsp;</span> " & gdvTemp.Text
				ddlLevel.Items.Add(New ListItem(gdvTemp.Text, gdvTemp.ID))
				ddlLevel.Items(ddlLevel.Items.Count - 1).Attributes.Add("style", "background-color:" & gdvTemp.Color & ";")
			Next
			litLegend.Text &= " &nbsp;&nbsp; &nbsp;&nbsp; <b>Status:&nbsp;</b>"
			litLegend.Text &= " <span style=""background-color:#66CC66;"">&nbsp;&nbsp;</span> Active"
			litLegend.Text &= " <span style=""background-color:#FF6666;"">&nbsp;&nbsp;</span> Burnt"
			For i As Integer = 1 To 12
				ddlMonth.Items.Add(New ListItem(i.ToString("00"), i))
			Next
			For i As Integer = 0 To 8
				ddlYear.Items.Add(Today.Year + i)
			Next
			Dim nCcID As Integer = dbPages.TestVar(Request("CCID"), 1, 0, 0)
			If nCcID > 0 Then
				Dim sSQL As String = "SELECT * FROM GetCcDetailsForWhitelist(" & nCcID & ")"
				Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
				If drData.Read Then
					txtNumber.Text = drData("CardNumber")
					txtHolder.Text = drData("CardHolder")
					ddlMonth.SelectedValue = drData("ExpMonth")
					ddlYear.SelectedValue = drData("ExpYear")
					Dim nMerchant As Integer = drData("Merchant")
					drData.Close()
					sSQL = "SELECT * FROM CheckCreditCardWhitelist(" & nMerchant & ", '" & txtNumber.Text & "', " & ddlMonth.SelectedValue & ", " & ddlYear.SelectedValue & ")"
					drData = dbPages.ExecReader(sSQL)
					If drData.Read Then
						Dim NameMatch As Boolean = (Replace(drData("HolderName"), " ", "").ToLower() = Replace(txtHolder.Text, " ", "").ToLower())
						lblError.Text = "That credit card is already whitelisted for that merchant. Record ID: "
						lblError.Text &= "<a href=""Whitelist.aspx?PageSize=1&ID=" & drData("ID") & """>" & drData("ID") & "</a>"
						If Not NameMatch Then lblError.Text &= ", please Note that the name you entered does not match to the name in the record"
					End If
					repData.DataBind()
				End If
				drData.Close()
			End If
		End If
		If String.IsNullOrEmpty(Request.Form("PageSize")) And String.IsNullOrEmpty(Request.QueryString("PageSize")) And String.IsNullOrEmpty(Request.QueryString("CCID")) Then
			repData.Visible = False
		Else
			repData.Visible = True
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Credit Card Whitelist</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
</head>
<body onload="focus();">
<form id="frmMain" runat="server">
	<table style="width:100%;">
	<tr>
		<td id="pageMainHeadingTd"><asp:label ID="lblPermissions" runat="server" /> <span id="pageMainHeading">CREDIT CARD WHITELIST</span></td>
		<td style="font-size:11px;text-align:right;"><asp:Literal ID="litLegend" runat="server" /></td>
	</tr>
	</table>
	<br />
	<fieldset>
		<legend>Add new credit card to whitelist</legend>
		<table class="formNormal" cellpadding="1" cellspacing="0" border="0">
		<tr style="font:normal 11px;">
			<td> &nbsp; Merchant</td>
			<td>Card Number</td>
			<td>Card Holder</td>
			<td>Card Validity</td>
			<td>Level</td>
			<td>Comment</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><Uc1:FilterMerchants ID="ucMerchant" IsSingleLine="true" ShowTitle="false" runat="server" Width="120px" /></td>
			<td><asp:TextBox ID="txtNumber" runat="server" /></td>
			<td><asp:TextBox ID="txtHolder" runat="server" /></td>
			<td>
				<asp:DropDownList ID="ddlMonth" runat="server" />/<asp:DropDownList ID="ddlYear" runat="server" />
			</td>
			<td><asp:DropDownList ID="ddlLevel" runat="server" /></td>
			<td><asp:TextBox ID="txtComment" runat="server" /></td>
			<td>
				<asp:Button ID="btnAdd" runat="server" Text=" Add " OnClick="AddRecord" UseSubmitBehavior="false" />
			</td>
		</tr>	
		</table>
		<div style="text-align:center;">
			<asp:Label ID="lblError" runat="server" CssClass="errorMessage" />
			<asp:Label ID="lblNoError" runat="server" CssClass="noErrorMessage" />
		</div>
	</fieldset>
	<script language="javascript" type="text/javascript">
		function ShowDetails(imgThis, bShow)
		{
			for (var trMain = imgThis; trMain.nodeName.toLowerCase() != "tr"; trMain = trMain.parentNode);
			for (var trDetails = trMain.nextSibling; trDetails.nodeName.toLowerCase() != "tr"; trDetails = trDetails.nextSibling);
			trDetails.style.display = (bShow ? "block" : "none");
			if (bShow)
			{
				var nTransID = "";
				if (imgThis.parentNode.nextSibling.firstChild)
				{
					if (imgThis.parentNode.nextSibling.firstChild.nodeName.toLowerCase() == "select")
						nTransID = imgThis.parentNode.nextSibling.firstChild.value;
					else
						if (imgThis.parentNode.nextSibling.firstChild.nodeName.toLowerCase() == "#text") nTransID = imgThis.parentNode.nextSibling.innerText;
					if (nTransID != "") trDetails.firstChild.nextSibling.innerHTML = "<iframe frameborder=\"0\" border=\"0\" src=\"trans_detail_cc.asp?isAllowAdministration=false&TransTableType=pass&PaymentMethod=1&transID=" + nTransID + "\" width=\"100%\" height=\"100\" onload=\"height=contentWindow.document.body.scrollHeight;\"></iframe>";
				}
			}
			for (var imgOther = (bShow ? imgThis.nextSibling : imgThis.previousSibling); imgOther.nodeName.toLowerCase() != "img"; )
				imgOther = (bShow ? imgOther.nextSibling : imgOther.previousSibling);
			imgThis.style.display = "none";
			imgOther.style.display = "";
			if (bShow)
			{
				if (trDetails.firstChild.nextSibling.innerHTML == "")
				{
					var nID = imgThis.parentNode.nextSibling.nextSibling.innerText;
					trDetails.firstChild.nextSibling.innerHTML = "<iframe frameborder=\"0\" border=\"0\" src=\"WhitelistDetails.aspx?ID=" + nID + "\" width=\"100%\" height=\"100\" onload=\"height=contentWindow.document.body.scrollHeight;\"></iframe>";
				}
			}
		}
	</script>
	<br />
	<asp:SqlDataSource ID="dsData" runat="server" SelectCommandType="Text"
	 SelectCommand="SELECT TOP (CASE IsNull(@nRows, 0) WHEN 0 THEN @nRows2 ELSE @nRows END) * FROM dbo.GetCreditCardWhiteList(@nMerchant,
	  @sFromDate+' '+@sFromTime, @sToDate+' '+@sToTime, @nIsMerchant, @nIsBurnt, CASE WHEN IsNull(@nID, 0)=0 THEN @nID2 ELSE @nID END,
	   @nLevel, @sUsername, @nPaymentMethod, @nBin, @nLast4, @sCardHolder, @sCardNumber, @nMonthFrom, @nYearFrom, @nMonthTo, @nYearTo)">
		<SelectParameters>
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="PageSize" Name="nRows" Type="Int16" DefaultValue="0" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="MerchantID" Name="nMerchant" Type="Int32" DefaultValue="0" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="RecordType" Name="nIsMerchant" Type="Int16" DefaultValue="-1" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="Burnt" Name="nIsBurnt" Type="Int16" DefaultValue="-1" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="ID" Name="nID" Type="Int32" DefaultValue="0" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="Level" Name="nLevel" Type="Int16" DefaultValue="-1" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="PaymentMethod" Name="nPaymentMethod" Type="Int32" DefaultValue="-1" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="Bin" Name="nBin" Type="Int32" DefaultValue="0" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="Last4" Name="nLast4" Type="Int16" DefaultValue="-1" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="MonthFrom" Name="nMonthFrom" Type="Int16" DefaultValue="0" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="YearFrom" Name="nYearFrom" Type="Int16" DefaultValue="0" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="MonthTo" Name="nMonthTo" Type="Int16" DefaultValue="0" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="YearTo" Name="nYearTo" Type="Int16" DefaultValue="0" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="false" QueryStringField="fromDate" Name="sFromDate" Type="String" DefaultValue="" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="false" QueryStringField="fromTime" Name="sFromTime" Type="String" DefaultValue="" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="false" QueryStringField="toDate" Name="sToDate" Type="String" DefaultValue="" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="false" QueryStringField="toTime" Name="sToTime" Type="String" DefaultValue="" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="false" QueryStringField="UserName" Name="sUsername" Type="String" DefaultValue="" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="false" QueryStringField="CardHolder" Name="sCardHolder" Type="String" DefaultValue="" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="false" QueryStringField="CardNumber" Name="sCardNumber" Type="String" DefaultValue="" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="PageSize" Name="nRows2" Type="Int16" DefaultValue="0" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="true" QueryStringField="ID" Name="nID2" Type="Int32" DefaultValue="0" />
		</SelectParameters>
	</asp:SqlDataSource>
	<asp:Repeater ID="repData" DataSourceID="dsData" runat="server">
		<HeaderTemplate>
			<table class="formNormal" width="100%">
			<tr>
				<th colspan="2">&nbsp;</th>
				<th>ID</th>
				<th>Merchant</th>
				<th>Credit Card</th>
				<th>Card Holder</th>
				<th>Validity</th>
				<th>RecordLevel</th>
				<th>Comment</th>
				<th>Added On</th>
				<th>Added By</th>
				<th>IP</th>
				<th>Burn Date</th>
			</tr>
		</HeaderTemplate>
		<ItemTemplate>
			<tr onmouseover="this.style.backgroundColor='Gainsboro';" onmouseout="if (this.style.backgroundColor.toLowerCase()=='gainsboro') this.style.backgroundColor='';">
				<td>
					<img onclick="ShowDetails(this,true);" style="cursor:pointer;" src="../images/tree_expand.gif" alt="Expand" width="16" height="16" border="0" />
					<img onclick="ShowDetails(this,false);" style="cursor:pointer;display:none;" src="../images/tree_collapse.gif" alt="Collapse" width="16" height="16" border="0" />
				</td>
				<td>
					<asp:Label Text="&nbsp;&nbsp;" BackColor='<%# Drawing.ColorTranslator.FromHtml(Eval("RecordLevelColor")) %>' runat="server" /><asp:Label
					 Text="&nbsp;&nbsp;" BackColor='<%# Drawing.ColorTranslator.FromHtml(IIf(Eval("IsBurnt"), "#FF6666", "#66CC66")) %>' runat="server" />
				</td>
				<td><asp:Literal Text='<%# Eval("ID") %>' runat="server" /></td>
				<td><asp:Label Text='<%# Eval("MerchantName") %>' ToolTip='<%# Eval("MerchantName") %>' runat="server" /></td>
				<td>
					<asp:Image ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/" & Eval("PaymentMethod") & ".gif"%>' ToolTip='<%# Eval("PaymentMethodName") %>' runat="server" />
					<asp:Image ImageUrl='<%# "/NPCommon/ImgCountry/18X12/" & Eval("BinCountry") & ".gif"%>' ToolTip='<%# Eval("BinCountryName") %>' runat="server" />
					<asp:Literal Text='<%# Eval("Bin").ToString.Substring(0, 4) & " " & Eval("Bin").ToString.Substring(4, 2) & "xx xxxx " & Eval("Last4") %>' runat="server" />
				</td>
				<td><asp:Literal Text='<%# Eval("CardHolder") %>' runat="server" /></td>
				<td><asp:Literal Text='<%# Eval("ExpMonth") & "/" & Eval("ExpYear") %>' runat="server" /></td>
				<td><asp:Literal Text='<%# Eval("RecordLevelName") %>' runat="server" /></td>
				<td><asp:Literal Text='<%# Eval("Comment") %>' runat="server" /></td>
				<td><asp:Literal Text='<%# dbPages.FormatDateTime(Eval("InsertDate")) %>' runat="server" /></td>
				<td><asp:Literal Text='<%# Eval("Username") %>' runat="server" /></td>
				<td><asp:Literal Text='<%# Eval("IP") %>' runat="server" /></td>
				<td><asp:Literal Text='<%# IIf(Eval("IsBurnt"), dbPages.FormatDateTime(Eval("BurnDate")), "---") %>' runat="server" /></td>
			</tr><tr style="display:none;">
				<td colspan="2"></td>
				<td colspan="11" style="border-top:dashed 1px Silver;"></td>
			</tr>
			<tr>
				<td colspan="13" style="height:1;background-color:Silver;"></td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</table>
		</FooterTemplate>
	</asp:Repeater>
</form>
</body>
</html>