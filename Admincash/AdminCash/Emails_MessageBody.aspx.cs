﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

public partial class Emails_MessageBody : System.Web.UI.Page
{
	protected string getUserSignature(int? mailboxId)
	{
		System.Text.StringBuilder sb = new System.Text.StringBuilder();
		if (mailboxId == null) {
			var mailboxes = Netpay.Emails.MailBox.Load();
			if (mailboxes.Count > 0) mailboxId = mailboxes[0].ID;
		}
		var mailbox = Netpay.Emails.MailBox.Load(mailboxId.GetValueOrDefault());
		if (mailbox != null)
		{
			sb.Append("<div id=\"signature\">" + mailbox.Signature + "<hr/></div>");
		}
		return sb.ToString();
	}

	public static string Replace(string source, string oldString, string newString, StringComparison comp)
	{
		int index = source.IndexOf(oldString, comp);
		if (index >= 0)
		{
			source = source.Remove(index, oldString.Length);
			source = source.Insert(index, newString);
		}
		return source;
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        bool isReply = false, isEditMode = false;
        int msgId = dbPages.TestVar(Request["MessageId"], 0, -1, 0);
        if (msgId == 0)
        {
            msgId = dbPages.TestVar(Request["replyToMessageId"], 0, -1, 0);
			isEditMode = isReply = (msgId != 0);
        }
		Netpay.Emails.Email.Message mimeMessage = null;
		var msg = Netpay.Emails.Message.Load(msgId);
		if (msg == null) isEditMode = true;
		else mimeMessage = msg.MimeMessage;
		string msgText = mimeMessage != null && !string.IsNullOrEmpty(mimeMessage.HTMLText) ? mimeMessage.HTMLText : (msg != null ? msg.Text : "");
		if (isEditMode)
		{
			string injectString = "";
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			if (!string.IsNullOrEmpty(Request["template"])) sb.Append(Netpay.Emails.Templates.GetTemplate(WebUtils.DomainHost, Request["template"]));
			sb.Append("<br/>");
			sb.Append(getUserSignature(msg != null ? (int?)msg.MailBoxID : null));
			if (isReply)
			{
				sb.Append("<div style=\"background-color:#fefefe;padding:2px;\" >");
		        sb.Append("<b>From: </b>" + msg.EmailFrom + "</br>");
			    sb.Append("<b>Sent: </b>" + msg.Date.ToString("MM/dd/yy") + "</br>");
				sb.Append("<b>To: </b>" + msg.EmailTo + "</br>");
				sb.Append("<b>Subject: </b>" + msg.Subject + "</br></div>");
				injectString = msgText != null ? msgText : "";
			}
			int lIndex = injectString.IndexOf("<body", StringComparison.InvariantCultureIgnoreCase);
			if (lIndex > -1) lIndex = injectString.IndexOf(">", lIndex);
            if (lIndex > -1) {
				string ourString = Replace(injectString.Substring(0, lIndex + 1), "<body", "<body contentEditable=\"true\"", StringComparison.InvariantCultureIgnoreCase);
                ourString += sb.ToString();
				ourString += injectString.Substring(lIndex + 1);
                Response.Write(ourString);
            } else {
                Response.Write("<html><body contentEditable=\"true\">");
                Response.Write(sb.ToString());
				Response.Write(injectString);
				Response.Write("</body></html>");
            }
		}
		else Response.Write(msgText);
    }
}