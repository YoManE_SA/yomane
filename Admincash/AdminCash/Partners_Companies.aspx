<%@ Page Language="VB" Inherits="htmlInputs" %>
<%@ Import namespace="Netpay.Infrastructure"%>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Register Src="Filter_DateRange.ascx" TagName="FilterDateRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>

<script language="vbscript" runat="server">
	Sub SetAffiliateID(o As Object, e As RepeaterItemEventArgs)
		If e.Item.ItemType=ListItemType.Item or e.Item.ItemType=ListItemType.AlternatingItem  then
			lblEmpty.Visible=False
			btnSave.Visible = True
		End If
	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
        Dim AffID as Integer = dbPages.TestVar(Request("ID"), 0, -1, 0)
		dsAff.ConnectionString = dbPages.DSN
		If IsPostBack Then Return
		ltAffiliateName.Text = dbPages.ExecScalar("Select name From tblAffiliates Where affiliates_id=" & AffID)
		tlbTop.LoadPartnerTabs(AffID)
	End Sub

	Sub DeleteAffMerchant_Click(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs)
		dbPages.ExecSql("Delete From [Setting].[SetMerchantAffiliate] Where SetMerchantAffiliate_id=" & e.CommandArgument)
		rptAff.DataBind()
	End Sub
	
	Sub SaveData(sender As Object, e As System.EventArgs)
		Dim nID As Integer
		For Each riLine As RepeaterItem In rptAff.Items
			If riLine.ItemType = ListItemType.Item Or riLine.ItemType = ListItemType.AlternatingItem Then
				nID = CType(riLine.FindControl("SetMerchantAffiliate_id"), HiddenField).Value
				dbPages.ExecSql("Update [Setting].[SetMerchantAffiliate] Set " & _
					"  FeesReducedPercentage=" & CType(riLine.FindControl("txtAffiliateFeeView"), TextBox).Text.ToDecimal(0) & _
					", UserID='" & CType(riLine.FindControl("txtUserID"), TextBox).Text.Replace("'", "''") & "'" & _
					" Where SetMerchantAffiliate_id=" & nID)
				LabelUpdated.Visible = True
			End If
		Next
	End Sub
	
    Sub btnAddMerchant_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsNumeric(Request("FilterMerchantID")) Then
            Response.Write("Please provide a merchant id")
            Exit Sub
        End If
  
        dbPages.ExecSql("Insert Into [Setting].[SetMerchantAffiliate] (Affiliate_id, Merchant_id) Values(" & Request("ID") & "," & Request("FilterMerchantID") & ")")
        rptAff.DataBind()
    End Sub
	
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
 <title>Partners Management</title>
 <link rel="stylesheet" type="text/css" media="all" href="../StyleSheet/calendar-blue.css" title="winter" />
 <link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr>
		<td>
    		<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">Partners Management</span> - <asp:Literal runat="server" ID="ltAffiliateName" /><br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<custom:Toolbar ID="tlbTop" Width="100%" runat="server" />
			<form id="form1" runat="server">
				<div style="width:300px;font-size:12px;">
					Add Merchant: 
					<Uc1:FilterMerchants runat="server" id="FMerchants" ClientField="ShowCompanyID" IsSingleLine="true" ShowTitle="false" /> 
					<asp:Button runat="server" Text="Add" ID="btnAddMerchant" OnClick="btnAddMerchant_Click" style="float:right;" />
				</div>
				<asp:SqlDataSource ID="dsAff" runat="server" SelectCommand="
					 SELECT a.SetMerchantAffiliate_id, a.UserID, c.id as CompanyID, c.CompanyName, a.FeesReducedPercentage
					 FROM [Setting].[SetMerchantAffiliate] a
					 LEFT JOIN tblCompany c ON (a.Merchant_id = c.ID)
					 WHERE a.Affiliate_id = @adIf ORDER BY CompanyName">
					<SelectParameters>
						<asp:QueryStringParameter QueryStringField="ID" Name="adIf" DefaultValue="-1" ConvertEmptyStringToNull="true" Type="Int32" />
					</SelectParameters>
				</asp:SqlDataSource>
				<table width="100%" class="formNormal" align="center">
				<tr><td class="MerchantSubHead">Merchants<br /><br /></td></tr>
				<tr>
					<th>ID</th>
					<th>Merchant</th>
					<th>UserID</th>
					<td></td>
					<th>Display</th>
					<td></td>
					<th>Delete</th>
				</tr>	
				<asp:Repeater ID="rptAff" runat="server" DataSourceID="dsAff" OnItemDataBound="SetAffiliateID">
					<ItemTemplate>
						<asp:HiddenField runat="server" ID="SetMerchantAffiliate_id" Value='<%#Eval("SetMerchantAffiliate_id")%>' />
						<tr onmouseover="this.style.backgroundColor='#f0f0f0';" onmouseout="this.style.backgroundColor='';">
							<td><asp:Literal ID="litID" runat="server" Text='<%#Eval("CompanyID")%>' /></td>
							<td><asp:Literal runat="server" Text='<%#Eval("CompanyName")%>' /></td>
							<td><asp:TextBox ID="txtUserID" runat="server" Text='<%#Eval("UserID")%>' /></td>
							<td style="background-color:Transparent;">&nbsp;</td>
							<td>
								<asp:TextBox ID="txtAffiliateFeeView" runat="server" text='<%#FormatNumber(IIF(IsDbNull(Eval("FeesReducedPercentage")), 0, Eval("FeesReducedPercentage")), 2, True)%>' Width="50px" />
								<asp:RangeValidator ControlToValidate="txtAffiliateFeeView" MinimumValue="0" MaximumValue="999999"
								ErrorMessage="The number is invalid!" Display="Dynamic" SetFocusOnError="true" Type="Double" runat="server" />
								<asp:RequiredFieldValidator  runat="server" Display="Dynamic" ControlToValidate="txtAffiliateFeeView" SetFocusOnError="true" ErrorMessage="The number must be filled!" ValidationGroup="4" />					
							</td>
							<td style="background-color:Transparent;">&nbsp;</td>
							<td><asp:Button runat="server" ID="btnDeleteAffMerchant" Text="Delete" OnCommand="DeleteAffMerchant_Click" CommandArgument='<%#Eval("SetMerchantAffiliate_id")%>' OnClientClick="if(!confirm('delete reference, are you sure?')) return false;" /></td>
						</tr>
					</ItemTemplate>
					<SeparatorTemplate>
						<tr>
							<td height="1" style="background-color:Silver;" colspan="3"></td>
							<td height="1" style="background-color:Transparent;"></td>
							<td height="1" style="background-color:Silver;" colspan="1"></td>
							<td height="1" style="background-color:Transparent;"></td>
							<td height="1" style="background-color:Silver;" colspan="1"></td>
						</tr>
					</SeparatorTemplate>
				</asp:Repeater>
				<tr>
					<td colspan="10" style="text-align:right;">
					<br />
						<asp:Button ID="btnSave" OnClick="SaveData" Text="Update" CssClass="buttonWhite" runat="server" Visible="false" />
						<asp:ValidationSummary ShowMessageBox="false" ShowSummary="false" runat="server" />
						<asp:Label ID="lblEmpty" CssClass="errorMessage" Text="No record found! Assignment is done form the company update page." runat="server" />			
					</td>
				</tr>
				<tr><td colspan="10" style="text-align:center;"><asp:Label ID="LabelUpdated" CssClass="details" Text="Data updated" runat="server" Visible="false" /></td></tr>
				</table>	
			</form>
		</td>
	</tr>
</table>	
</body>
</html>
