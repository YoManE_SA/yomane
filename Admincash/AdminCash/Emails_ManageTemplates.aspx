﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Emails_ManageTemplates.aspx.cs" Inherits="Emails_ManageTemplates" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Emails - Templates</title>
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <link href="../js/jquery-ui-1.8.10.custom/css/custom-theme/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<link href="../StyleSheet/../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<script language="javascript">
		function formSubmit() {
			var src = $('#ifrmBody').contents().find("body").html();
			$('#hf_txtBody').val(src);
		}
	</script>
    <form id="form1" runat="server" onsubmit="formSubmit()">
        <asp:hiddenfield id="hf_txtBody" clientidmode="Static" runat="server" />
        <asp:hiddenfield id="hf_originalFileName" runat="server" />
		<h1><asp:label id="lblPermissions" runat="server" /> Manage Tempates</h1>
		<br />
		<table cellpadding="2" cellspacing="0" border="0" width="100%">
			<tr>
				<td valign="top" style="padding-right: 40px; text-align:left;">
					Template Name: <asp:TextBox runat="server" ID="txtFileName" CssClass="wide" style='width:550px;' /><br />
					<br />
					<iframe frameborder="0" runat="server" id="ifrmBody" style="height: 450px; width: 100%; margin: 0; border: solid 1px #cccccc;" clientidmode="Static"></iframe>
					<br />
					<asp:Button runat="server" ID="btnSave" Text="Save" OnClick="SaveClick" />
					<asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="DeleteClick" />
				</td>
				<td valign="top" style="width: 350px; padding-left: 40px; text-align:left; border-left: solid 1px gray;">
                    <h2 style="margin: 0;">SIGNATURE</h2>
					<asp:Repeater runat="server" ID="rptSignatureList">
						<ItemTemplate>
							<asp:LinkButton runat="server" Text='<%# Eval("EmailAddress") %>' OnCommand="LoadSignatureClick" CommandArgument='<%# Eval("ID") %>' /><br />
						</ItemTemplate>
					</asp:Repeater>
					<br /><br />
                    <h2 style="margin: 0;">TEMPLATES LIST</h2>
					<a href="?">[New Template]</a><br />
					<asp:Repeater runat="server" ID="rptTemplateList">
						<ItemTemplate>
							<asp:LinkButton runat="server" Text="<%# Container.DataItem %>" OnCommand="LoadTemplateClick" CommandArgument="<%# Container.DataItem %>" /><br />
						</ItemTemplate>
					</asp:Repeater>
				</td>
			</tr>
		</table>
    </form>
</body>
</html>
