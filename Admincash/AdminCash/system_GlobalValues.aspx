<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20100210 Tamir
	'
	' Global values' management
	'

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsRecords.ConnectionString = dbPages.DSN()
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Global Data</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<style type="text/css">
		.centered input {width:60px !important;text-align:right !important;}
		.buttons input {width:50px !important;text-align:center !important;}
	</style>
</head>
<body>
	<form id="frmServer" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Global Values</h1>
		<br /><br />
		<asp:GridView ID="gvRecords" runat="server" AutoGenerateColumns="False" DataKeyNames="" DataSourceID="dsRecords"
		 AllowPaging="False" AllowSorting="False" SelectedRowStyle-CssClass="rowSelected" Width="100%">
			<Columns>
				<asp:BoundField DataField="Prime" HeaderText="Prime" DataFormatString="{0:#,0.00}%" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="PSPFeeMonthly" DataFormatString="%{0:#,0.00}" HeaderText="PSP&nbsp;Fee (Fixed)" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="PSPFeeVolumePercent" DataFormatString="{0:#,0.00}%" HeaderText="PSP&nbsp;Fee (Volume&nbsp;Percent)" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="PSPFeeTransaction" DataFormatString="${0:#,0.00}" HeaderText="PSP&nbsp;Fee (Transaction)" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="DeclineCountHours" DataFormatString="{0}" HeaderText="Hours For Decline Counters" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="AnnualFeeAmount" DataFormatString="&euro;{0:#,0.00}" HeaderText="Annual&nbsp;Fee (General)" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="AnnualFeeLowRiskAmount" DataFormatString="&euro;{0:#,0.00}" HeaderText="Annual&nbsp;Fee (Low&nbsp;Risk)" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="AnnualFeeHighRiskAmount" DataFormatString="&euro;{0:#,0.00}" HeaderText="Annual&nbsp;Fee (High&nbsp;Risk)" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="AnnualFee3dSecureAmount" DataFormatString="&euro;{0:#,0.00}" HeaderText="Annual&nbsp;Fee (3D&nbsp;Secure)" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="MonthlyFeeEuro" DataFormatString="&euro;{0:#,0.00}" HeaderText="Monthly&nbsp;Fee (EUR)" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="MonthlyFeeDollar" DataFormatString="${0:#,0.00}" HeaderText="Monthly&nbsp;Fee (USD)" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="MonthlyFeeBankHigh" DataFormatString="&euro;{0:#,0.00}" HeaderText="Monthly&nbsp;Fee (Bank High)" ItemStyle-CssClass="centered" />
				<asp:BoundField DataField="MonthlyFeeBankLow" DataFormatString="&euro;{0:#,0.00}" HeaderText="Monthly&nbsp;Fee (Bank Low)" ItemStyle-CssClass="centered" />
				<asp:CommandField HeaderText="Edit" ButtonType="Button" ItemStyle-CssClass="centered buttons" ControlStyle-CssClass="buttonWhite"
				 ShowDeleteButton="False" ShowEditButton="True" ShowSelectButton="False" HeaderStyle-CssClass="actions" UpdateText="Save" />
			</Columns>
		</asp:GridView>
		<asp:SqlDataSource ID="dsRecords" runat="server" ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM tblGlobalValues"
		 UpdateCommand="UPDATE tblGlobalValues SET DeclineCountHours=@DeclineCountHours, PSPFeeVolumePercent=@PSPFeeVolumePercent, Prime=@Prime,
		  PSPFeeMonthly=@PSPFeeMonthly, PSPFeeTransaction=@PSPFeeTransaction, AnnualFeeAmount=@AnnualFeeAmount,
		   AnnualFeeLowRiskAmount=@AnnualFeeLowRiskAmount, AnnualFeeHighRiskAmount=@AnnualFeeHighRiskAmount, AnnualFee3dSecureAmount=@AnnualFee3dSecureAmount,
		    MonthlyFeeEuro=@MonthlyFeeEuro, MonthlyFeeDollar=@MonthlyFeeDollar, MonthlyFeeBankHigh=@MonthlyFeeBankHigh, MonthlyFeeBankLow=@MonthlyFeeBankLow">
			<UpdateParameters>
				<asp:Parameter Name="Prime" Type="Single" />
				<asp:Parameter Name="PSPFeeMonthly" Type="Single" />
				<asp:Parameter Name="PSPFeeTransaction" Type="Single" />
				<asp:Parameter Name="PSPFeeVolumePercent" Type="Single" />
				<asp:Parameter Name="DeclineCountHours" Type="Int32" />
				<asp:Parameter Name="AnnualFeeAmount" Type="Single" />
				<asp:Parameter Name="AnnualFeeLowRiskAmount" Type="Single" />
				<asp:Parameter Name="AnnualFeeHighRiskAmount" Type="Single" />
				<asp:Parameter Name="AnnualFee3dSecureAmount" Type="Single" />
				<asp:Parameter Name="MonthlyFeeEuro" Type="Single" />
				<asp:Parameter Name="MonthlyFeeDollar" Type="Single" />
				<asp:Parameter Name="MonthlyFeeBankHigh" Type="Single" />
				<asp:Parameter Name="MonthlyFeeBankLow" Type="Single" />
			</UpdateParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>