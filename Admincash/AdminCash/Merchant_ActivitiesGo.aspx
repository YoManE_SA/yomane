<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient" %>
<script runat="server">
	Sub Page_Load()
		Security.CheckPermission("Inactive Merchants - Delete")
		Dim sSQL As String = String.Empty
		If dbPages.TestVar(Request("DeleteMerchant"), 1, 0, 0) > 0 Then
			sSQL = "DELETE FROM tblCompany WHERE ID=" & Request("DeleteMerchant") & " AND ID IN (SELECT ID FROM viewMerchantsInactive WHERE DeleteButton<>'')"
		ElseIf dbPages.TestVar(Request("DeleteMerchants"), 0, 1, 0) = 1 And Request("Merchant") <> String.Empty Then
			sSQL = "DELETE FROM tblCompany WHERE ID IN (" & Request("Merchant") & ") AND ID IN (SELECT ID FROM viewMerchantsInactive WHERE DeleteButton<>'')"
		ElseIf dbPages.TestVar(Request("NewStatus"), 1, 0, -1) > -1 And Request("Merchant") <> String.Empty Then
			sSQL = "UPDATE tblCompany SET ActiveStatus=" & Request("NewStatus") & " WHERE ID IN (" & Request("Merchant") & ")"
		End If
		If sSQL <> String.Empty Then dbPages.ExecSql(sSQL)
	End Sub
</script>
<script language="javascript" type="text/javascript">
	parent.location.replace("Merchant_Activities.aspx?Merchant="+parent.document.getElementById("txaMerchants").value+"&FilterStatus="+parent.document.getElementById("txaFilterStatus").value);
</script>
