<%
Response.CharSet="Windows-1255"
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_balance.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_mail.asp" -->
<!--#include file="../include/func_security.asp" -->
<%
Response.CharSet="Windows-1255"
frameTestSrc = ""
sShowMessage2 = ""
CurResult = ""
If request("Action") = "Exchange" Then
    CurResult = ConvertCurrency(TestNumVar(Request("ExchangeFrom"), 0, -1, 0), _
        TestNumVar(Request("ExchangeTo"), 0, -1, 0), Request("Amount"))
ElseIf request("Action") = "SendEmail" Then
    Dim sqlStr, sTransID, toEmail : toEmail = "'Email=" & Server.URLEncode(Request("iEmailAddress")) &  "'"
    sTransID = TestNumVar(Request("iTransId"), 0, -1, 0)
    Select Case Request("iTableType")
    case "passToClient": 
        sqlStr = "Insert Into EventPending(TransPass_id, EventPendingType_id, TryCount, Parameters)Values(" & sTransID & "," & PET_InfoEmailSendClient & ",3, " & toEmail & ")"
    case "passToMerchant":
        sqlStr = "Insert Into EventPending(TransPass_id, EventPendingType_id, TryCount, Parameters)Values(" & sTransID & "," & PET_InfoEmailSendMerchant & ",3, " & toEmail & ")"
    case "failToClient": 
        sqlStr = "Insert Into EventPending(TransFail_ID, EventPendingType_id, TryCount, Parameters)Values(" & sTransID & "," & PET_InfoEmailSendClient & ",3, " & toEmail & ")"
    case "failToMerchant": 
        sqlStr = "Insert Into EventPending(TransFail_ID, EventPendingType_id, TryCount, Parameters)Values(" & sTransID & "," & PET_InfoEmailSendMerchant & ",3, " & toEmail & ")"
    case "passToAffiliate": 
        sqlStr = "Insert Into EventPending(TransFail_ID, EventPendingType_id, TryCount, Parameters)Values(" & sTransID & "," & PET_InfoEmailSendAffiliate & ",3, " & toEmail & ")"
    case "ClarifyToMerchant": 
        sqlStr = "Insert Into EventPending(TransFail_ID, EventPendingType_id, TryCount, Parameters)Values(" & sTransID & "," & PET_EmailPhotoCopy & ",3, " & toEmail & ")"
    case "CHBToMerchant": 
        sqlStr = "Insert Into EventPending(TransFail_ID, EventPendingType_id, TryCount, Parameters)Values(" & sTransID & "," & PET_EmailChargeBack & ",3, " & toEmail & ")"
    End Select
    oledbData.Execute sqlStr 
    Response.Write("Added to sending queue " & now())
	Response.end
ElseIf Request("Action") = "EncTest" Then 
    sShowMessage3 = ExecScalar("Select dbo.GetEncrypted256('" & Request("EncValue") & "')", "")
    'sShowMessage3 = DecDataOnce("SecretLocation", Request("EncValue")) ' 20080225 Tamir: testing feature - old decryption is left unchanged
ElseIf Request("Action") = "HashTest" Then 
	sShowMessage4 = CalcHash(Request("HashType"), Request("HashValue"), Request("HashRawBinary") = "1")
ElseIf Request("Action")="chkCcNumber" then
	%>
	<!--#include file="../include/func_transCharge.asp" -->
	<%
	sCcNumber = trim(request("ccNumber"))
	sCcNumber = Fn_CleanCCNumber(sCcNumber) '���� �� ���� ����� ������
	isTestOnly = Fn_IsCreditCardTestOnly(sCcNumber) '����� ��� ����� ����� ������ ����
	ccTypeID = left(Fn_ccTypeAndRegion(sCcNumber),1) '����� ��� ��� �����
	ccTypeEngName = Fn_ccNameEnglish(ccTypeID) '����� �� ����� �������
	
    If NOT fn_IsValidCCNum(sCcNumber, false, 0, 0, 0, 0) then
        sShowMessage2 = sShowMessage2 & "Valid Card: No<br />"
    Else
        sShowMessage2 = sShowMessage2 & "Valid Card: Yes<br />"
    End if
    
	if isTestOnly then 
		sShowMessage2 = sShowMessage2 & "Test Card: Yes<br />"
	else
		sShowMessage2 = sShowMessage2 & "Test Card: No<br />"
	end if
	sShowMessage2 = sShowMessage2 & "Type: " & ccTypeEngName
    sShowMessage2 = sShowMessage2 & "BIN : " & CCNumToCountry(request("ccNumber"), 0, "") & "<br />"
	'sShowMessage2 = sShowMessage2 & "����� : " & ExecScalar("SELECT dbo.GetEncrypted256('" & Replace(request("ccNumber"), "'", "''") & "')", "- ��� -")
ElseIf request("Action")= "FixBalance" Then
	fn_BalanceTotalUpdate Request("CompanyID"), Request("CurType"), 0
end if
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<br />
<table width="98%" align="center" border="0" cellpadding="1" cellspacing="0">
<tr>
	<td id="pageMainHeadingTd"><span id="pageMainHeading">Miscellaneous</span><br /><br /><br /></td>
</tr>
<tr>
	<td valign="top">
		<%
		pageSecurityLevel = 0
		pageSecurityLang = ""
		pageClosingTags = "</td></tr></table>"
		Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
		%>
		<table border="0" cellspacing="1" cellpadding="1">
		<form target="frameEmailTest" method="post">
            <input type="hidden" name="Action" value="SendEmail" />
		<tr>
			<td class="txt12b" colspan="7">
				Send client an approved transaction email<br />
			</td>
		</tr>
		<tr>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">Status</span><br />
				<select name="iTableType">
				    <option value="passToClient" <%if sTableType="passToClient" Then%>selected<%End if%>>Pass To Client</option>
				    <option value="passToMerchant" <%if sTableType="passToMerchant" Then%>selected<%End if%>>Pass To Merchant</option>
				    <!--<option value="failToClient" <%if sTableType="failToClient" Then%>selected<%End if%>>Fail To Client</option>-->
				    <option value="failToMerchant" <%if sTableType="failToMerchant" Then%>selected<%End if%>>Fail To Merchant</option>
				    <option value="passToAffiliate" <%if sTableType="passToAffiliate" Then%>selected<%End if%>>Pass To Affiliate</option>
				    <option value="ClarifyToMerchant" <%if sTableType="ClarifyToMerchant" Then%>selected<%End if%>>Clarify To Merchant</option>
				    <option value="CHBToMerchant" <%if sTableType="CHBToMerchant" Then%>selected<%End if%>>CHB To Merchant</option>
				</select><br />
			</td>
			<td width="12"></td>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">Transaction ID</span><br />
				<input type="Text" dir="ltr" name="iTransId" value="<%= sTransId %>" size="12"><br />
			</td>
			<td width="6"></td>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">Email Address</span><br />
				<input type="Text" dir="ltr" name="iEmailAddress" value="<%= sEmailAddress %>" size="18"><br />
			</td>
			<td width="6"></td>
			<td><br /><input type="submit" value=" Send "><br /></td>
		</tr>
		<tr>
			<td colspan="7">
				<iframe name="frameEmailTest" frameborder="0" style="border:1px dotted gray; width:400px; height:50px;"></iframe>
			</td>
		</tr>
		</form>
		</table>
		<br /><br />
		<table border="0" cellspacing="1" cellpadding="1">
		<form method="post">
		<input type="Hidden" name="Action" value="SendEmail">
		<tr>
			<td class="txt12b" colspan="3">
				Test Outgoing Email<br />
			</td>
		</tr>
		<tr>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">Email</span><br />
				<input type="Text" dir="ltr" name="Email" value="" size="18"><br />
			</td>
			<td width="6"></td>
			<td><br /><input type="submit" value=" Send "><br /></td>
		</tr>
		</form>
		</table>
		<br /><br />
		<table border="0" cellspacing="1" cellpadding="1">
		<form action="system_testingPage.asp" method="post">
		<input type="Hidden" name="Action" value="chkCcNumber">
		<tr>
			<td class="txt12b" colspan="5">
				Validate credit card<br />
			</td>
		</tr>
		<tr>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">Card Number</span><br />
				<input type="Text" dir="ltr" name="ccNumber" value="<%= sCcNumber %>" size="18"><br />
			</td>
			<td width="6"></td>
			<td><br /><input type="submit" value=" Send "><br /></td>
			<%
			If sShowMessage2<>"" Then
				%>
				<td width="12"></td>
				<td class="txt12" valign="bottom" style="color:green;"><%= sShowMessage2 %></td>
				<%
			End If
			%>
		</tr>
		</form>
		</table>
	</td>
	<td style="border-left:1px solid #d2d2d2;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign="top">

		<table border="0" cellspacing="1" cellpadding="1">
		<form method="post">
		<input type="Hidden" name="Action" value="EncTest">
		<tr>
			<td class="txt12b" colspan="3">
				Encryption Test<br />
			</td>
		</tr>
		<tr>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">Password</span><br />
				<input type="Text" dir="ltr" name="EncValue" value="<%=Request("EncValue")%>" size="18">
			</td>
			<td width="6"></td>
			<td><br /><input type="submit" value=" Send "><br /></td>
			<td width="12"></td>
			<td><br /><%=sShowMessage3%></td>
		</tr>
		</form>
		</table>
		<br /><br />

		<table border="0" cellspacing="1" cellpadding="1">
		<form method="post">
		<input type="Hidden" name="Action" value="HashTest">
		<tr>
			<td class="txt12b" colspan="3">
				Hash Test<br />
			</td>
		</tr>
		<tr>
			<td class="txt12" nowrap>
				<span style="color:#003366;text-decoration:underline;">Value</span> <input name="HashRawBinary" value="1" type="checkbox" <%=IIF(Request("HashRawBinary") = "1", " checked", "") %> /> raw binary<br />
				<input type="Text" dir="ltr" name="HashValue" value="<%=Request("HashValue")%>" size="14">
				<select name="HashType"><option>MD5<option<%=IIF(Request("HashType") = "SHA", " selected", "") %>>SHA</select>
			</td>
			<td width="6"></td>
			<td><br /><input type="submit" value=" Send "><br /></td>
		</tr>
		<tr><td colspan="3"><%=sShowMessage4%></td></tr>
		</form>
		</table>
		<br /><br />


		<table border="0" cellspacing="1" cellpadding="1">
		<form method="post">
		<input type="Hidden" name="Action" value="Exchange">
		<tr>
			<td class="txt12b" colspan="3">
				Test Currency Conversion<br />
			</td>
		</tr>
		<tr>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">Amount</span><br />
				<input type="Text" name="Amount" value="1" size="4"> &nbsp;&nbsp;
                <select name="ExchangeFrom" class="inputData">
                <%For i = 0 To MAX_CURRENCY
                    Response.Write("<option value=""" & i & """>" & GetCurText(i))
                Next%>
                </select>
				&nbsp;&nbsp;To 
                <select name="ExchangeTo" class="inputData">
                <%For i = 0 To MAX_CURRENCY
                    Response.Write("<option value=""" & i & """>" & GetCurText(i))
                Next%>
                </select>
			</td>
			<td width="6"></td>
			<td><br /><input type="submit" value=" Convert "><br /></td>
			<td width="12"></td>
			<td><br /><%=CurResult%></td>
		</tr>
		</form>
		</table>
		<br /><br />
		<table border="0" cellspacing="1" cellpadding="1">
		<form method="post">
		<input type="Hidden" name="Action" value="IPAddressTest">
		<tr>
			<td class="txt12b" colspan="3">
				Test IP GEO location<br />
			</td>
		</tr>
		<tr>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">IP Address</span><br />
				<input type="Text" dir="ltr" name="IPAddress" value="<%=Request("IPAddress")%>" size="15" maxlength="15">
				<%If Request("Action") = "IPAddressTest" Then Response.Write("GEO Result:" & GetIPGeoCountry(Request("IPAddress")))%>
			</td>
			<td width="6"></td>
			<td><br /><input type="submit" value=" Test "><br /></td>
		</tr>
		</form>
		</table>
	</td>
	<td style="border-left:1px solid #d2d2d2;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign="top">
<!--		<table border="0" cellspacing="1" cellpadding="1">
		<form action="https://www.netpay.co.il/member/DebitCUpdateMDPR2.asp" method="post">
		<input type="Hidden" name="Action">
		<tr>
			<td class="txt12b" colspan="3">
				Send Invik batch file<br />
			</td>
		</tr>
		<tr>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">Terminal number</span><br />
				<input type="Text" dir="ltr" name="Terminal" value="99994567" size="18"><br />
			</td>
			<td width="6"></td>
			<td><br /><input type="submit" value=" Send "><br /></td>
		</tr>
		</form>
		</table>
		<br /><br />-->
		<table width="300">
		<tr>
			<td class="txt12">ERROR SQL DATA File</td>
		</tr>
		<tr>
			<td align="left" class="txt12"><a href="<%=PROCESS_URL%>Data/Uploads/ERROR_PASS.txt">Trans SQL Data</a> | <a href="<%=PROCESS_URL%>Member/ExecErrorPass.aspx">Execute</a></td>
		</tr>
		</table> 
		<br /><br />
		<table width="300">
		<tr>
			<td class="txt12">.NET ASPX Testing</td>
		</tr>
		<tr>
			<td align="left" class="txt12"><a href="system_testingPage.aspx">Click Here</td>
		</tr>
		</table> 
		<br /><br />
		<table width="300">
		<tr>
			<td class="txt12">SQL Management</td>
		</tr>
		<tr>
			<td align="left" class="txt12"><a href="sql.aspx">Click Here</td>
		</tr>
		</table> 

	</td>
</tr>
</table>
</body>
</html>

<!--
<tr><td><br /></td></tr>
<tr>
	<td>
		<table border="0" cellspacing="1" cellpadding="1">
		<form method="post">
		<input type="Hidden" name="Action" value="FixBalance">
		<tr>
			<td class="txt12">
				Fixed Company Balance<br />
			</td>
			<td width="12"></td>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">Company ID</span><br />
				<input type="Text" dir="ltr" name="CompanyID" value="35" size="18"><br />
				<input type="Radio" name="CurType" value="0">ILS&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="Radio" name="CurType" value="1">USD
			</td>
			<td width="6"></td>
			<td><br /><input type="submit" value=" Send "><br /></td>
		</tr>
		</form>
		</table>
	</td>
</tr>
-->
<!--
<tr><td><br /></td></tr>
<tr>
	<td>
		<table border="0" cellspacing="1" cellpadding="1">
		<form method="post">
		<input type="Hidden" name="Action" value="CheckSID">
		<tr>
			<td class="txt12">
				Test Company SID<br />
			</td>
			<td width="12"></td>
			<td class="txt12">
				<span style="color:#003366;text-decoration:underline;">SID Number</span><br />
				<input type="Text" dir="ltr" name="SID" value="<%=Request("SID")%>" size="5"><br />
			</td>
			<td width="6"></td>
			<td><br /><input type="submit" value=" Test "><br /></td>
			<td width="6"></td>
		</tr><tr>
			<td class="txt12" colspan="6">
			<%
			 if Request("Action") = "CheckSID" And Request("SID") <> "" Then 
				 Set iRs = oleDbData.Execute("Select ID, CompanyName From tblCompany Where debitCompanyExID='" & Request("SID") & "'")
				 Do while not iRs.EOF
				 	Response.Write(iRs(0) & " - " & iRs(1) & "<br />")
				  iRs.MoveNext
				 Loop
				 iRs.Close
			  End if
			%>
			</td>
		</tr>
		</form>
		</table>
	</td>
</tr>
-->
<!--
<tr><td><br /></td></tr>
<tr>
	<td>
		<table border="0" cellspacing="1" cellpadding="1">
		<input type="Hidden" name="Action" value="SendEmail">
		<tr>
			<td class="txt12">
				Sync CHB with PAGO<br />
			</td>
			<td width="12"></td>
			<td><input type="button" value=" Start "  onclick="document.location='https://www.netpay.co.il/member/PagoRefund.asp?DoRequest=1'" ><br /></td>
		</tr>
		</table>
	</td>
</tr>
-->