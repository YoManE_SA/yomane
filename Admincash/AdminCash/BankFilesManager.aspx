<%@ Page Language="VB" CodeFile="BankFilesManager.aspx.vb" Inherits="EpaUpload" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminNetEng.css" />
</head>
<body>
    <form id="form1" runat="server">
	<table border="0" align="center" style="width:92%; height:100%;">
		<tr>
			<td>
                <asp:Label ID="lblPermissions" runat="server" /> <span id="pageMainHeading" runat="server">Bank File Manager</span><br /><br />
            </td>
            <td style="text-align:right;">
                Debit Company: <asp:DropDownList runat="server" ID="ddlEpaType" Width="100" AutoPostBack="true" />
            </td>
		</tr>
		<tr>
			<td>Browse and manage files on server<br /></td>
			<td style="padding-left:40px;">Upload new files to server<br /></td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
		    <td valign="top" style="width:660px; height:100%;">
		        <asp:Literal runat="server" ID="txtError" ViewStateMode="Disabled" />
		        <custom:Toolbar ID="tlbTop" Width="600" runat="server" Align="">
                    <asp:Literal runat="server" ID="txtFiles" />
		        </custom:Toolbar>
		        <table border="0" style="width:600px;">
		            <tr>
		                <td runat="server" id="tdFileMove" align="right">
                            <asp:DropDownList runat="server" ID="ddlMoveTo" Width="130px" />
                            <asp:Button runat="server" ID="btnMove" Text="Move" OnClick="btnUpdate_Click" OnClientClick="return confirm('Continue with update files?');" />
							&nbsp;
							<asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="btnDelete_Click" OnClientClick="return confirm('Continue with delete files?');" />
						</td>
		            </tr>
		        </table>
		    </td>
		    <td valign="top" style="padding-left:40px; border-left:solid gray 1px;">
                <table class="formNormal" border="0">
                    <tr><td colspan="2"><asp:FileUpload runat="server" ID="FileUpload1" Width="300" /></td></tr>
                    <tr><td colspan="2"><asp:FileUpload runat="server" ID="FileUpload2" Width="300" /></td></tr>
                    <tr><td colspan="2"><asp:FileUpload runat="server" ID="FileUpload3" Width="300" /></td></tr>
                    <tr><td colspan="2"><asp:FileUpload runat="server" ID="FileUpload4" Width="300" /></td></tr>
                    <tr><td colspan="2"><asp:FileUpload runat="server" ID="FileUpload5" Width="300" /></td></tr>
					<tr><td><br /></td></tr>
                    <tr>
                        <td></td>
						<td style="text-align:right;"><asp:Button runat="server" ID="btnUpload" OnClick="Upload_Click" Text=" Upload " OnClientClick="if(ddlEpaType.selectedIndex < 0){alert('you must select file type.'); return false; }" /></td>
					</tr>
                </table>
		    </td>
		</tr>
	</table>
    </form>
</body>
</html>
