<%
'DCF_TerminalNumber = Request("DCF_TerminalNumber")
'DCF_DebitCompanyID = TestNumVar(Request("DCF_DebitCompanyID"), 0, -1, 0)
DCF_ID = TestNumVar(Request("DCF_ID"), 0, -1, 0)
DCF_CurrencyID = TestNumVar(Request("DCF_CurrencyID"), 0, -1, 0)
DCF_PaymentMethod = TestNumVar(Request("DCF_PaymentMethod"), 0, -1, 0)
DCF_PayINDaysStr = ""
If DCF_ID <> 0 Then
	If Request("ACT") = "DELETE" Then 
		OleDbData.Execute("Delete From tblDebitCompanyFees Where DCF_ID=" & DCF_ID)
	Else
		'Validation
		Dim DCF_PayTransDays, DCF_PayINDays, nLastNumber, sSaveError
		DCF_PayTransDays = Split(Request("DCF_PayTransDays"), ",")
		nLastNumber = 0
		For i = 0 To Ubound(DCF_PayTransDays)
			If DCF_PayTransDays(i) = "" Or Not IsNumeric(DCF_PayTransDays(i)) Then
				sSaveError = "Transaction Days is in invalid format"
				Exit For
			ElseIf (nLastNumber > CLng(DCF_PayTransDays(i))) Or (CLng(DCF_PayTransDays(i)) > 31) Then
				sSaveError = "Transaction Days is in invalid format"
				Exit For
			Else
				nLastNumber = CLng(DCF_PayTransDays(i))
			End if
		Next
		If sSaveError = "" Then
			DCF_PayINDays = Split(Request("DCF_PayINDays"), ",")
			For i = 0 To Ubound(DCF_PayINDays)
				If DCF_PayINDays(i) = "" Or Not IsNumeric(DCF_PayINDays(i)) Then
					sSaveError = "PayIn Days is in invalid format"
					Exit For
				ElseIf (CLng(DCF_PayINDays(i)) > 31) Then 
					sSaveError = "PayIn Days is in invalid format"
					Exit For
				End If
				DCF_PayINDaysStr = DCF_PayINDaysStr & DCF_PayINDays(i) & ","
			Next
			If sSaveError = "" And (UBound(DCF_PayTransDays) <> UBound(DCF_PayINDays)) Then sSaveError = "Lists doesn't match"
			If Len(DCF_PayINDaysStr) > 0 Then DCF_PayINDaysStr = Left(DCF_PayINDaysStr, Len(DCF_PayINDaysStr) - 1)
		End If
		If sSaveError = "" Then
			If DCF_ID = -1 Then
				oleDbData.Execute "Insert Into tblDebitCompanyFees(DCF_DebitCompanyID, DCF_CurrencyID, DCF_PaymentMethod, DCF_TerminalNumber)Values(" & _
					DCF_DebitCompanyID & ", " & DCF_CurrencyID & ", " & DCF_PaymentMethod & ",'" & DCF_TerminalNumber & "')"
				DCF_ID = ExecScalar("Select Max(DCF_ID) From tblDebitCompanyFees", 0)
			End If
			oleDbData.Execute "Update tblDebitCompanyFees Set" & _
				" DCF_PercentFee=" & TestNumVar(Request("DCF_PercentFee"), 0, -1, 0) & _
				",DCF_FixedFee=" & TestNumVar(Request("DCF_FixedFee"), 0, -1, 0) & _
				",DCF_MinPrecFee=" & TestNumVar(Request("DCF_MinPrecFee"), 0, -1, 0) & _
				",DCF_MaxPrecFee=" & IIF(Request("DCF_MaxPrecFee") <> "", TestNumVar(Request("DCF_MaxPrecFee"), 0, -1, 0), "null") & _
				",DCF_CBFixedFee=" & TestNumVar(Request("DCF_CBFixedFee"), 0, -1, 0) & _
				",DCF_ClarificationFee=" & TestNumVar(Request("DCF_ClarificationFee"), 0, -1, 0) & _
				",DCF_RefundFixedFee=" & TestNumVar(Request("DCF_RefundFixedFee"), 0, -1, 0) & _
				",DCF_ApproveFixedFee=" & TestNumVar(Request("DCF_ApproveFixedFee"), 0, -1, 0) & _
				",DCF_FailFixedFee=" & TestNumVar(Request("DCF_FailFixedFee"), 0, -1, 0) & _
				",DCF_StatIncDays=" & TestNumVar(Request("DCF_StatIncDays"), 0, -1, 0) & _
				",DCF_FixedCurrency=" & TestNumVar(Request("DCF_FixedCurrency"), 0, -1, 0) & _
				",DCF_CHBCurrency=" & TestNumVar(Request("DCF_CHBCurrency"), 0, -1, 0) & _
				",DCF_PayTransDays='" & TestNumList(Request("DCF_PayTransDays")) & "'" & _
				",DCF_PayINDays='" & DCF_PayINDaysStr & "'" & _
				" Where DCF_ID=" & DCF_ID
		End if
	End if		
End if
Sub drawCurrency(nStr, nCrSel, nNoneText)
	Response.Write("<select name=""" & nStr & """ class=""inputData"">")
	Response.Write("<option value=""255"">[" & nNoneText & "]</option>")
	For i = 0 To MAX_CURRENCY
		sSelected = IIF(i = nCrSel, "Selected", "")
		Response.Write("<option value=""" & i & """ " & sSelected & ">" & GetCurText(i) & "</option>")
	Next
	Response.Write("</select>")
End Sub
%>
<form method="post">
<table border="0" width="95%" align="center">
<tr>
	<td>
		<span class="MerchantSubHead">Fees & Payouts</span> &nbsp;
		<% sDiv = "The data below is used for updateing incoming transactions with the following data:<br />" & _
		"1. The fee taken for the charge by the debit company<br />" & _
		"2. The date when debit company will pay us the amount<br /><br />"
		If DCF_TerminalNumber<>"" Then sDiv = sDiv & "** If empty, data for updating is taken from the debit company configuration page"
		showDivBox "help", "ltr", "500", sDiv %><br />
	</td>
</tr>
<tr><td height="5"></td></tr>
<tr>
	<input type="hidden" name="DCF_ID" value="-1">
	<input type="hidden" name="DCF_DebitCompanyID" value="<%=nDebitCompany%>">
	<td>
	<%
	set rsPaymentMethod = oledbData.Execute("SELECT PaymentMethod_id AS ID, Name AS pm_Name FROM List.PaymentMethod WHERE PaymentMethod_id > " & PMD_MAXINTERNAL)
	PutRecordsetComboDefault rsPaymentMethod,"DCF_PaymentMethod","class=""inputData""", "ID","pm_Name","0","[All]"
	rsPaymentMethod.Close
	drawCurrency "DCF_CurrencyID", 0, "All"
	%>
	<input type="submit" value="ADD NEW" class="btn" style="width:70px;"><br />
	</td>
 </tr>
 </table>
 </form>
 <br /><br />
 <%If sSaveError <> "" Then Response.Write("<span style=""color:red;padding:20px;"">" & sSaveError & "</span>")%>
 <table width="95%" align="center" border="0">
 <%
	Set rsData3 = Server.CreateObject("Adodb.Recordset")
	sSQL = "SELECT tblDebitCompanyFees.*, pm.Name AS pm_Name FROM tblDebitCompanyFees " & _
		" LEFT JOIN List.PaymentMethod AS pm ON DCF_PaymentMethod= pm.PaymentMethod_id" & _
		" WHERE DCF_DebitCompanyID=" & DCF_DebitCompanyID & " AND DCF_TerminalNumber='" & Replace(DCF_TerminalNumber, "'", "''") & "' ORDER BY DCF_ID"
	'Response.Write(sSQL)
	rsData3.Open sSQL, oledbData, 3, 1
	Do While Not rsData3.EOF
	%>
	 <tr><td style="border-top:solid 1px;"><br /></td></tr>	
	 <tr>
		<form method="post">
		<input type="hidden" name="DCF_ID" value="<%=rsData3("DCF_ID")%>">
		<input type="hidden" name="DCF_DebitCompanyID" value="<%=rsData3("DCF_DebitCompanyID")%>">
		<input type="hidden" name="DCF_CurrencyID" value="<%=rsData3("DCF_CurrencyID")%>">
		<input type="hidden" name="DCF_PaymentMethod" value="<%=rsData3("DCF_PaymentMethod")%>">
		<td class="txt12" colspan="8" style="padding-bottom:10px;">
			<img align="middle" src="/NPCommon/ImgPaymentMethod/23X12/<%=rsData3("DCF_PaymentMethod")%>.gif">
		 <% 
		 If rsData3("DCF_PaymentMethod") > 0	Then
			strGDText = UCase(rsData3("pm_Name"))
		 Else
			strGDText = "[All]"
		 End if
		 %>
		 <%= strGDText%>, CURRENCY: <%=GetCurText(rsData3("DCF_CurrencyID"))%><br />
		</td>
		</tr>
		<tr>
		<td style="padding-left:26px;">
			<table border="0" class="tblInputs">
			<tr>
			<td class="txt11b" width="80" rowspan="2">Fees</td>
			<td class="txt11" style="text-decoration: underline;">Currency<br /></td>
			<td class="txt11" style="text-decoration: underline;">Trans<br /></td>
			<td class="txt11" style="text-decoration: underline;">Inter (min)<br /></td>
			<td class="txt11" style="text-decoration: underline;">Sale (max)<br /></td>
			<td class="txt11" style="text-decoration: underline;">Clearing<br /></td>
			<td class="txt11" style="text-decoration: underline;" nowrap>Pre-Auth<br /></td>
			<td class="txt11" style="text-decoration: underline;">Refund<br /></td>
			<td class="txt11" style="text-decoration: underline;" nowrap>Copy R.<br /></td>
			<td class="txt11" style="text-decoration: underline;">Fail<br /></td>
			<td class="txt11" style="text-decoration: underline;">CHB<br /></td>
			</tr>
			<tr>
			<td><%drawCurrency "DCF_FixedCurrency", rsData3("DCF_FixedCurrency"), "Native"%></td>
			<td><input name="DCF_FixedFee" value="<%=FormatNumber(rsData3("DCF_FixedFee"), 2, True)%>" size="4"></td>
			<td><input name="DCF_MinPrecFee" value="<%=FormatNumber(rsData3("DCF_MinPrecFee"), 2, True)%>" size="4">%</td>
			<td><input name="DCF_MaxPrecFee" value="<%If rsData3("DCF_MaxPrecFee") <> "" Then Response.Write(FormatNumber(rsData3("DCF_MaxPrecFee"), 2, True))%>" size="4">%</td>
			<td><input name="DCF_PercentFee" value="<%=FormatNumber(rsData3("DCF_PercentFee"), 2, True)%>" size="4">%</td>
			<td><input name="DCF_ApproveFixedFee" value="<%=FormatNumber(rsData3("DCF_ApproveFixedFee"), 2, True)%>" size="4"></td>
			<td><input name="DCF_RefundFixedFee" value="<%=FormatNumber(rsData3("DCF_RefundFixedFee"), 2, True)%>" size="4"></td>
			<td><input name="DCF_ClarificationFee" value="<%=FormatNumber(rsData3("DCF_ClarificationFee"), 2, True)%>" size="4"></td>
			<td><input name="DCF_FailFixedFee" value="<%=FormatNumber(rsData3("DCF_FailFixedFee"), 2, True)%>" size="4"></td>
			<td><%drawCurrency "DCF_CHBCurrency", rsData3("DCF_CHBCurrency"), "Native"%></td>
			<td><input name="DCF_CBFixedFee" value="<%=FormatNumber(rsData3("DCF_CBFixedFee"), 2, True)%>" size="4"></td>
			</tr>	
			</table>
		</td>
		</tr>
		<tr>
		<td style="padding-left:26px;">
			<table border="0" class="tblInputs">
			<tr>
			<td class="txt11b" width="80" rowspan="2">Payouts</td>
			<td class="txt11" style="text-decoration: underline;" align="left">Transaction Days<br /></td>
			<td class="txt11" style="text-decoration: underline;" align="left">PayIn Days<br /></td>
			</tr>
			<tr>
			<td>
			 <input name="DCF_PayTransDays" value="<%=rsData3("DCF_PayTransDays")%>" size="20">
			 <% showDivBox "help", "ltr", "300", "Enter transaction dates seperated with a comma.<br /><br /><b>Example:</b> 1,10,20<br />split the month into 3 parts 1-10, 11-20, 21-31" %>
			</td>
			<td>
			 <input name="DCF_PayINDays" value="<%=rsData3("DCF_PayINDays")%>" size="20">
			 <% showDivBox "help", "ltr", "300", "Enter pay-in dates seperated with a comma, this field complement the dates and must use with the same month parts.<br /><br /><b>Example:</b><br /> Transaction Days: 1,10,20, PayIn Days: 15,25,5.<br />means transactions from 1-10 will be payed on the 15th, transactions from 11-20 will be payed on the 25th, transactions from 21-31 will be payed on the 5th" %>
			</td>
			<td width="40"></td>
			<td>
				<input type="submit" name="ACT" value="UPDATE" class="btn">
				<input type="submit" name="ACT" value="DELETE" class="btn" style="color:Maroon;" onclick="return confirm('Sure to delete ?');">
			</td>
			</tr>	
			</table>
		</td>
		</form>
	 </tr>
	<%	
		rsData3.MoveNext()
	Loop
	rsData3.Clone()
%>
</table>
