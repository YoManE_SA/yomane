<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<script runat="server">
	'
	' 20110922 Tamir
	'
	' Types' management for pending events
	'
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsRecords.ConnectionString = dbPages.DSN
		dsCurrencies.ConnectionString = dbPages.DSN
		dsBehaviors.ConnectionString = dbPages.DSN

		Dim drData As SqlDataReader = dbPages.ExecReader(dsBehaviors.SelectCommand)
		divbOptions.Text = "<ul>"
		While drData.Read
			divbOptions.Text &= "<li>" & drData("FullText") & "</li>"
		End While
		divbOptions.Text &= "</ul>"
	End Sub
	
	Sub UpdateRecord(o As Object, e As GridViewUpdateEventArgs)
		dsRecords.UpdateParameters("CurrencyID").DefaultValue = CType(CType(o, GridView).Rows(e.RowIndex).FindControl("ddlCurrency"), DropDownList).SelectedValue
		dsRecords.UpdateParameters("Behavior").DefaultValue = CType(CType(o, GridView).Rows(e.RowIndex).FindControl("ddlBehavior"), DropDownList).SelectedValue
		dsRecords.UpdateParameters("IsAnnual").DefaultValue = IIf(CType(CType(o, GridView).Rows(e.RowIndex).FindControl("ddlPeriodicity"), DropDownList).SelectedValue = 1, True, False)
	End Sub

	Sub AddRecord(o As Object, e As EventArgs)
		lblError.Text = String.Empty
		lblNoError.Text = String.Empty
		If NPControls.IsNotNumeric(txtAmount, lblError, "New fee amount", AllowedNumericValues.NonNegative) Then Exit Sub
		If NPControls.IsEmptyField(txtName, lblError, "New fee name") Then Exit Sub
		If NPControls.IsEmptyField(txtAmount, lblError, "New fee amount") Then Exit Sub
		If NPControls.IsCustomError(ddlBehavior.SelectedIndex = 0 And dbPages.TestVar(txtFeeLimit.Text, 1D, 0D, 0D) > 0D, lblError, "Minimum monthly fee is specified but type is not selected") Then Exit Sub
		If NPControls.IsCustomError(ddlBehavior.SelectedIndex > 0 And dbPages.TestVar(txtFeeLimit.Text, 1D, 0D, 0D) = 0D, lblError, "Type is selected and minimum monthly fee is not specified") Then Exit Sub
		dsRecords.InsertParameters("Name").DefaultValue = txtName.Text
		dsRecords.InsertParameters("Amount").DefaultValue = dbPages.TestVar(txtAmount.Text, 1D, 0D, 0D)
		dsRecords.InsertParameters("IsAnnual").DefaultValue = IIf(ddlPeriodicity.SelectedValue = 0, False, True)
		dsRecords.InsertParameters("IsActive").DefaultValue = chkIsActive.Checked()
		dsRecords.InsertParameters("CurrencyID").DefaultValue = ddlCurrency.SelectedValue
		dsRecords.InsertParameters("FeeLimit").DefaultValue = txtFeeLimit.Text 'dbPages.TestVar(txtFeeLimit.Text, 1D, 0D, 0D)
		dsRecords.InsertParameters("Behavior").DefaultValue = IIf(ddlBehavior.SelectedValue = "-1", 0, ddlBehavior.SelectedValue)
		dsRecords.Insert()
		lblNoError.Text = "New record has been added successfully!"
	End Sub
	
	Sub SetDefault(o As Object, e As EventArgs)
		Dim ddl As DropDownList = CType(o, DropDownList)
		Dim nCurrency As Integer = dbPages.TestVar(CType(ddl.NamingContainer.FindControl("hidCurrencyID"), HiddenField).Value, 1, 0, -1)
		If nCurrency >= 0 Then ddl.SelectedValue = nCurrency
	End Sub
	
	Sub SetDefaultBehavior(o As Object, e As EventArgs)
		Dim ddl As DropDownList = CType(o, DropDownList)
		Dim nLimit As Decimal = dbPages.TestVar(CType(ddl.NamingContainer.FindControl("hidLimit"), HiddenField).Value, 1D, 0D, 0D)
		If nLimit > 0 Then
			Dim nBehavior As Integer = dbPages.TestVar(CType(ddl.NamingContainer.FindControl("hidBehavior"), HiddenField).Value, 1, 0, -1)
			If nBehavior >= 0 Then ddl.SelectedValue = nBehavior
		End If
	End Sub

	Sub SetDefaultPeriodicity(o As Object, e As EventArgs)
		Dim ddl As DropDownList = CType(o, DropDownList)
		Dim bIsAnnual As Boolean = dbPages.TestVar(CType(ddl.NamingContainer.FindControl("hidIsAnnual"), HiddenField).Value, False)
		ddl.SelectedIndex = IIf(bIsAnnual, 1, 0)
	End Sub
	</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Global Data</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" media="screen" />
	<style type="text/css">
		.centered input {width:60px !important;text-align:right !important;}
		.buttons input {width:50px !important;text-align:center !important;}
	</style>
</head>
<body>
	<form id="frmServer" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Periodic Fee Types</h1>
		<br /><br />
		<fieldset style="width:90%;">
			<legend>Add new periodic fee type</legend>
			<table cellpadding="1" cellspacing="0" border="0" class="noborder" style="width:100%;">
			<tr style="font:normal 11px;">
				<td>Periodicity</td>
				<td>Name</td>
				<td>Currency</td>
				<td>Amount</td>
				<td>Type <AC:DivBox ID="divbOptions" runat="server" /></td>
				<td>Activity Fees <AC:DivBox text="Merchant overall fees sum for the previous month/year" Width="170px" runat="server" /></td>
				<td>Active</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
					<asp:DropDownList ID="ddlPeriodicity" CssClass="option" RepeatLayout="Flow" runat="server">
						<asp:ListItem Text="Monthly" Value="0" />
						<asp:ListItem Text="Annual" Value="1" />
					</asp:DropDownList>
				</td>
				<td><asp:TextBox ID="txtName" Width="260px" runat="server" /></td>
				<td><asp:DropDownList ID="ddlCurrency" DataSourceID="dsCurrencies" DataTextField="Title" DataValueField="ID" runat="server" /></td>
				<td><asp:TextBox ID="txtAmount" CssClass="medium" runat="server" /></td>
				<td><asp:DropDownList ID="ddlBehavior" DataSourceID="dsBehaviors" DataTextField="Title" DataValueField="ID" runat="server" /></td>
				<td><asp:TextBox ID="txtFeeLimit" Width="170px" runat="server" /></td>
				<td><asp:CheckBox ID="chkIsActive" runat="server" /></td>
				<td>
					<asp:Button ID="btnAdd" CssClass="buttonWhite" runat="server" Text=" Add " OnClick="AddRecord" UseSubmitBehavior="false" />
				</td>
			</tr>	
			</table>
		<div style="text-align:center;">
			<asp:Label ID="lblError" runat="server" CssClass="errorMessage" />
			<asp:Label ID="lblNoError" runat="server" CssClass="noErrorMessage" />
		</div>
		</fieldset>
		<br /><br /><br />
		<asp:GridView ID="gvRecords" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="dsRecords"
		 AllowPaging="False" AllowSorting="False" SelectedRowStyle-CssClass="rowSelected" Width="90%" HorizontalAlign="Center" OnRowUpdating="UpdateRecord" HeaderStyle-CssClass="gridTitleleft">
			<Columns>
				<asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="true" ItemStyle-Width="50px" />
				<asp:TemplateField HeaderText="Periodicity" ItemStyle-Width="150px">
					<ItemTemplate>
						<asp:Literal runat="server" Text='<%# IIf(Eval("IsAnnual"), "Annual", "Monthly") %>' />
					</ItemTemplate>
					<EditItemTemplate>
						<asp:HiddenField ID="hidIsAnnual" runat="server" Value='<%# Eval("IsAnnual") %>' />
						<asp:DropDownList OnDataBound="SetDefaultPeriodicity" ID="ddlPeriodicity" CssClass="option" RepeatLayout="Flow" runat="server">
							<asp:ListItem Text="Monthly" Value="0" />
							<asp:ListItem Text="Annual" Value="1" />
						</asp:DropDownList>
					</EditItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-CssClass="wide" ControlStyle-CssClass="medium2" />
				<asp:TemplateField HeaderText="Currency" ItemStyle-Width="150px">
					<ItemTemplate>
						<asp:Literal runat="server" Text='<%# Eval("CUR_IsoName") %>' />
					</ItemTemplate>
					<EditItemTemplate>
						<asp:HiddenField ID="hidCurrencyID" runat="server" Value='<%# dbPages.TestVar(Eval("CurrencyID"), 1, 0, -1) %>' />
						<asp:DropDownList OnDataBound="SetDefault" ID="ddlCurrency" DataSourceID="dsCurrencies" DataTextField="Title" DataValueField="ID" CssClass="inputdata" runat="server" />
					</EditItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="Amount" HeaderText="Amount" ItemStyle-Width="150px" DataFormatString="{0:#,0.00}" />
				<asp:TemplateField HeaderText="Type" ItemStyle-Width="150px">
					<ItemTemplate>
						<asp:MultiView runat="server" ActiveViewIndex='<%#IIf(Convert.IsDbNull(Eval("FeeLimit")), 0, 1)%>' >
							<asp:View runat="server">
								Full charge - No conditions
							</asp:View>
							<asp:View runat="server">
								<asp:Literal runat="server" Text='<%#Eval("GD_Text")%>' />
							</asp:View>
						</asp:MultiView>
					</ItemTemplate>
					<EditItemTemplate>
						<asp:HiddenField ID="hidBehavior" runat="server" Value='<%# dbPages.TestVar(Eval("Behavior"), 1, 0, -1) %>' />
						<asp:HiddenField ID="hidLimit" runat="server" Value='<%# Eval("FeeLimit") %>' />
						<asp:DropDownList OnDataBound="SetDefaultBehavior" ID="ddlBehavior" DataSourceID="dsBehaviors" DataTextField="Title" DataValueField="ID" CssClass="inputdata" runat="server" />
					</EditItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="FeeLimit" HeaderText="Activity Fees" ItemStyle-Width="120px" DataFormatString="{0:#,0.00}" />
				<asp:CheckBoxField DataField="IsActive" HeaderText="Active" ItemStyle-CssClass="centered" HeaderStyle-CssClass="centered" />
				<asp:CommandField ButtonType="Button" ItemStyle-CssClass="centered buttons" ControlStyle-CssClass="buttonWhite"
				 ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="False" HeaderStyle-CssClass="actions" UpdateText="Save" ItemStyle-Width="150px" />
			</Columns>
		</asp:GridView>
		<asp:SqlDataSource ID="dsRecords" runat="server" ProviderName="System.Data.SqlClient"
		 SelectCommand="SELECT t.*, CUR_ISOName, GD_Text, GD_Description FROM tblPeriodicFeeType t INNER JOIN tblSystemCurrencies ON CurrencyID=CUR_ID INNER JOIN GetGlobalData(33) ON Behavior=GD_ID"
		  InsertCommand="INSERT INTO tblPeriodicFeeType(Name, Amount, IsAnnual, IsActive, CurrencyID, FeeLimit, Behavior) VALUES (@Name, @Amount, @IsAnnual, @IsActive, @CurrencyID, @FeeLimit, @Behavior)"
		   UpdateCommand="UPDATE tblPeriodicFeeType SET Name=@Name, Amount=@Amount, FeeLimit=@FeeLimit, IsAnnual=@IsAnnual, IsActive=@IsActive, CurrencyID=@CurrencyID, Behavior=CASE WHEN @Behavior<0 THEN 0 ELSE @Behavior END WHERE ID=@ID"
		    DeleteCommand="DELETE FROM tblPeriodicFeeType WHERE ID=@ID" >
			<UpdateParameters>
				<asp:Parameter Name="ID" Type="Int32" />
				<asp:Parameter Name="Name" Type="String" />
				<asp:Parameter Name="Amount" Type="Decimal" />
				<asp:Parameter Name="Behavior" Type="Int32" />
				<asp:Parameter Name="FeeLimit" Type="Decimal" ConvertEmptyStringToNull="true" />
				<asp:Parameter Name="IsAnnual" Type="Boolean" />
				<asp:Parameter Name="IsActive" Type="Boolean" />
				<asp:Parameter Name="CurrencyID" Type="Int32" />
			</UpdateParameters>
			<DeleteParameters>
				<asp:Parameter Name="ID" Type="Int32" />
			</DeleteParameters>
			<InsertParameters>
				<asp:Parameter Name="Name" Type="String" DefaultValue="NEW PERIODIC FEE" />
				<asp:Parameter Name="Amount" Type="Decimal" DefaultValue="0" />
				<asp:Parameter Name="IsAnnual" Type="Boolean" DefaultValue="False" />
				<asp:Parameter Name="IsActive" Type="Boolean" DefaultValue="False" />
				<asp:Parameter Name="CurrencyID" Type="Int32" DefaultValue="0" />
				<asp:Parameter Name="Behavior" Type="Int32" DefaultValue="0" />
				<asp:Parameter Name="FeeLimit" Type="Decimal" ConvertEmptyStringToNull="true" />
			</InsertParameters>
		</asp:SqlDataSource>
		<asp:SqlDataSource ID="dsCurrencies" runat="server" SelectCommand="SELECT CUR_ID ID, CUR_ISOName Title FROM tblSystemCurrencies ORDER BY ID" />
		<asp:SqlDataSource ID="dsBehaviors" runat="server" SelectCommand="SELECT -1 ID, 'Full charge - No conditions' Title, 'Full charge - No conditions: Full periodic fee is allways charged' FullText UNION SELECT GD_ID, GD_Text, GD_Text+': '+GD_Description FROM GetGlobalData(33) ORDER BY ID" />
	</form>
</body>
</html>