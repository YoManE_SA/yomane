<%
Server.ScriptTimeout = 360
response.Expires = -1
%>
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_adoPaging.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/func_security.asp" -->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
Function WriteToLog(fHeading, fStatus, fId)
	Dim sSQL
	sSQL="INSERT tblRefundAskLog (refundAsk_id, ral_description, ral_user) " &_
	"VALUES (" & TestNumVar(fId, 0, -1, 0) & ", '" & DBText(fHeading) & "', '" & Request.ServerVariables("LOGON_USER") & "')"
	oledbData.Execute sSQL
	sSQL="UPDATE tblRefundAsk SET RefundAskStatus=" & TestNumVar(fStatus, 0, 100, 0) & " WHERE ID=" & TestNumVar(fId, 0, -1, 0)
	oledbData.Execute sSQL
End Function

Sub ShowAlert(sText)
	Response.Write "<script language=""javascript"" type=""text/javascript"">alert(""" & Replace(sText, """", "\""") & """);</script>"
End Sub

Function AddToInvikRefundBatch(nID)
	Dim sSQL : sSQL="SELECT DebitCompanyID FROM tblCompanyTransPass WHERE ID=(SELECT TransID FROM tblRefundAsk WHERE ID=" & nID & ")"
	Dim nDebitCompany : nDebitCompany=ExecScalar(sSQL, 0)
	If nDebitCompany=21 Then
		sSQL="IF NOT EXISTS(SELECT 1 FROM tblInvikRefundBatch WHERE irb_IsDownloaded=0 AND irb_RefundRequest=" & nID & ") INSERT INTO tblInvikRefundBatch(irb_RefundRequest) VALUES (" & nID & ")"
		oledbData.Execute(sSQL)
		AddToInvikRefundBatch=True
	Else
		ShowAlert "Refund request " & nID & " cannot be added to Invik batch: not Invik transaction!"
		AddToInvikRefundBatch=False
	End If
End Function

Function RemoveFromInvikRefundBatch(nID)
	Dim sSQL : sSQL="SELECT ID FROM tblInvikRefundBatch WHERE irb_IsDownloaded=1 AND irb_RefundRequest=" & nID
	If ExecScalar(sSQL, 0)>0 Then
		RemoveFromInvikRefundBatch=False
	Else
		oledbData.Execute("DELETE FROM tblInvikRefundBatch WHERE irb_RefundRequest=" & nID)
		RemoveFromInvikRefundBatch=True
	End If
End Function

Function GetListWhere(nVarName, sFieldName)
    Dim sWhereIn, sWhereBetween, retWhere
	sWhereIn="" : sWhereBetween="" : retWhere = ""
	sIDs = Trim(Request(nVarName)) : saIDs = Split(sIDs, ",")
	For i = LBound(saIDs) To UBound(saIDs)
		nDashPos=InStr(saIDs(i), "-")
		If nDashPos > 1 and right(saIDs(i), 1) <> "-" Then
			sWhereBetween = sWhereBetween & " OR " & sFieldName & " BETWEEN " & Left(saIDs(i), nDashPos-1) & " AND " & Mid(saIDs(i), nDashPos+1)
		Elseif isNumeric(saIDs(i)) Then
			sWhereIn = sWhereIn & ", " & saIDs(i)
		End if
	Next
	If sWhereIn <> "" or sWhereBetween <> "" Then
		retWhere = retWhere & " AND ("
		If sWhereIn <> "" Then
			retWhere = retWhere & sFieldName & " IN (" & Mid(sWhereIn, 2) & ")"
		Else
			sWhereBetween = mid(sWhereBetween, 4)
		End if
		If sWhereBetween<>"" Then retWhere = retWhere & sWhereBetween
		retWhere = retWhere & ")"
	End if
	GetListWhere = retWhere
End Function

If trim(request("Action"))="SUBMIT" Then
	'20110109 Tamir - allow modifying refund amount
	bAmountUpdated=false
	For Each fldItem In Request.Form
		nID = 0
		nAmountNew = 0
		nAmountOld = 0
		If Left(fldItem, 18) = "RefundAskAmountNew" Then
			nID = TestNumVar(Mid(fldItem, 19), 1, 0, 0)
			nAmountNew = TestNumVar(Request.Form(fldItem), 1, 0, 0)
			nAmountOld = TestNumVar(Request.Form("RefundAskAmount" & Trim(nID)), 1, 0, 0)
			If nID > 0 And nAmountNew > 0 And nAmountOld > 0 And nAmountOld - nAmountNew > 0 Then
				sSQL = "UPDATE tblRefundAsk SET RefundAskAmount=" & nAmountNew & " WHERE RefundAskStatus IN (0, 1) AND ID=" & nID & " AND RefundAskAmount=" & nAmountOld
				oledbData.Execute sSQL
				bAmountUpdated=true
			End If
		End If
	Next
    Dim nmerchantId, refundComment, nRefundType
	nCount = 0
	sSendFaxID = ""	
	sRefundAskID = trim(request("RefundAskID"))
	sRefundAskArray = Split(sRefundAskID, ",", -1, 1)
	For each nID in sRefundAskArray
        nID = Trim(nID)
		sRefundAskStatus = trim(request("RefundAskAction" & nID))
		Select case sRefundAskStatus
			Case "1"
				nCount = nCount + 1
				if nCount <> 1 then sSendFaxID = sSendFaxID & ","
				sSendFaxID = sSendFaxID & trim(nID)
				WriteToLog "Request was printed", 1, nID
			Case "2"
                nRefundType = 0
				nTransID = TestNumVar(request("RefundAskTransID" & trim(nID)), 1, 0, 0)
				sIP = Request.ServerVariables("REMOTE_ADDR")
				nAmount = TestNumVar(request("RefundAskAmount" & trim(nID)), 1, 0, 0)
                Set nRs = oledbData.Execute("Select CompanyID, RefundAskComment, RefundType From tblRefundAsk Where id=" & nID)
                If Not nRs.EOF Then
				    refundComment = nRs("RefundAskComment")
                    nmerchantId = nRs("CompanyID")
                    nRefundType = IIF(IsNull(nRs("RefundType")), 0, nRs("RefundType"))
                End If
                nRs.Close()
				set rsData = oledbData.Execute("EXEC CreateRefund " & nTransID & ", '" & sIP & "', " & nAmount & ", '" & Replace(refundComment, "'", "''") & "', " & nRefundType)
				if rsData.EOF then
					WriteToLog "Failed to create refund (unexpected error)", 0, nID
				Else
					nRefundTransID=rsData(0)
					If nRefundTransID>0 Then
                        ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nRefundTransID & "," & PET_RefundRequestSendNotify & ",'', 3)")
				        Set cmpRs = oledbData.Execute("Select IsSendUserConfirmationEmail, IsMerchantNotifiedOnPass From tblCompany Where ID=" & nmerchantId)
				        If Not cmpRs.EOF Then
					        If IIF(IsNull(cmpRs("IsSendUserConfirmationEmail")), False, cmpRs("IsSendUserConfirmationEmail")) Then _
						        ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nRefundTransID & "," & PET_InfoEmailSendClient & ",'', 3)")
					        If IIF(IsNull(cmpRs("IsMerchantNotifiedOnPass")), False, cmpRs("IsMerchantNotifiedOnPass")) Then _
						        ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nRefundTransID & "," & PET_InfoEmailSendMerchant & ",'', 3)")
				        End If
				        cmpRs.Close()
						WriteToLog "Refund was created (transaction " & nRefundTransID & ")", 2, nID
					Else
						WriteToLog "Failed to create refund (error code " & nRefundTransID & ")", 0, nID
					End If
				End If
			Case "3"
				WriteToLog "Refund request was canceled", 3, nID
			Case "4"
                If TestNumVar(ExecScalar("Select RefundAskStatus From tblRefundAsk Where id=" & nID, 0), 0, -1, 0) <> 4 Then
				    sStatusHistory = ""
				    %>
				    <!--#include file="Refund_ask_transCharge.asp"-->
				    <%
                End If
			Case "5" '20090726 Tamir - Preparing batch for Invik
				'AddToInvikRefundBatch(nID)
				'Call WriteToLog("Refund request added to Invik batch", 5, nID)
                nRefundType = 0
				nTransID = TestNumVar(request("RefundAskTransID" & trim(nID)), 1, 0, 0)
				sIP = Request.ServerVariables("REMOTE_ADDR")
				nAmount = TestNumVar(request("RefundAskAmount" & trim(nID)), 1, 0, 0)
                Set nRs = oledbData.Execute("Select CompanyID, RefundAskComment, RefundType From tblRefundAsk Where id=" & nID)
                If Not nRs.EOF Then
				    refundComment = nRs("RefundAskComment")
                    nmerchantId = nRs("CompanyID")
                    nRefundType = IIF(IsNull(nRs("RefundType")), 0, nRs("RefundType"))
                End If
                nRs.Close()
				set rsData = oledbData.Execute("EXEC CreateRefund " & nTransID & ", '" & sIP & "', " & nAmount & ", '" & Replace(refundComment, "'", "''") & "', " & nRefundType)
				if rsData.EOF then
					WriteToLog "Failed to create refund (unexpected error)", 0, nID
				Else
					nRefundTransID=rsData(0)
					If nRefundTransID>0 Then
						oledbData.Execute "UPDATE tblCompanyTransPass SET DebitCompanyID=21, BTFileName=NULL, CTP_Status=1 WHERE ID=" & nRefundTransID
						WriteToLog "Refund was created (transaction " & nRefundTransID & ") and scheduled for Invik batch", 5, nID
					Else
						WriteToLog "Failed to create refund (error code " & nRefundTransID & ")", 0, nID
					End If
				End If
		End Select
	Next
	If sSendFaxID <> "" Then
		%>
			<script language="javascript" type="text/javascript">
				var winRefundRequestFax = window.open('Refund_ask_fax_browser.asp?SendFaxID=<%= sSendFaxID %>', 'SendFaxWin', 'width=700,height=500,scrollbars=1');
			</script>
		<%
	End If
	%>
		<script language="javascript" type="text/javascript">
		    parent.frames[0].document.getElementById("aspnetForm").submit();
		</script>
	<%
	Response.End 
Elseif trim(request("Action"))="clean" Then
	sCleanRefundAskID = trim(request("CleanRefundAskID"))
	If RemoveFromInvikRefundBatch(sCleanRefundAskID) Then
		WriteToLog "Selection was reset", 0, sCleanRefundAskID
	Else
		ShowAlert "Reset failed for refund request " & sCleanRefundAskID & ": Invik batch already downloaded!"
	End If
	%>
		<script language="javascript" type="text/javascript">
			parent.frames[0].document.getElementById("formFilter").submit();
		</script>
	<%
	Response.End 
End if

session("PageSize") = 20
If Trim(request("source")) = "filter" Then
	If TestNumVar(request("PageSize"), 1, 100, 0)>0 Then
		session("PageSize") = int(request("PageSize"))
	End if
End if

sWhere = " WHERE tblRefundAsk.ID IS NOT NULL "
If Trim(request("Source")) = "filter" Then
	dDateMax = Trim(Request("toDate") & " " & Request("toTime"))
	dDateMin = Trim(Request("fromDate") & " " & Request("fromTime"))
	If Trim(request("ShowCompanyID"))<>"" AND Trim(request("ShowCompanyID"))<>"null" Then sWhere=sWhere & " AND (tblRefundAsk.CompanyID=" & trim(request("ShowCompanyID")) & ")"
	if trim(request("DebitCompanyID"))<>"" then sWhere=sWhere & " AND tblDebitTerminals.DebitCompany IN (" & request("DebitCompanyID") & ")"
	if trim(request("TerminalNumber"))<>"" then sWhere=sWhere & " AND tblDebitTerminals.TerminalNumber='" & DBText(request("TerminalNumber")) & "'"
	if IsDate(dDateMax) then sWhere=sWhere & " AND (tblRefundAsk.RefundAskDate <='" & CDate(dDateMax) & "')"
	if IsDate(dDateMin) then sWhere=sWhere & " AND (tblRefundAsk.RefundAskDate >='" & CDate(dDateMin) & "')"
	if trim(request("RefundAskStatus"))<>"" then sWhere = sWhere & " AND tblRefundAsk.RefundAskStatus IN (" & request("RefundAskStatus") & ")"
	sWhere = sWhere & GetListWhere("RequestNumber", "tblRefundAsk.ID") 'search by Request ID - allows ranges and CSV, e.g. "1,7,3-5,9"
	sWhere = sWhere & GetListWhere("OriginalTransID", "tblCompanyTransPass.ID") 'search by Transaction ID - allows ranges and CSV, e.g. "1,7,3-5,9"
	sRC = Trim(Request("FailReplyCode"))
	If sRC <> "" Then
		sWhere = sWhere & " AND tblRefundAsk.ID IN (SELECT RefundAsk_ID FROM tblRefundAskLog r1 WHERE ral_Description='processing a refund failed (" & sRC & ")'"
		If Trim(Request("LastFailOnly"))="1" Then sWhere = sWhere & " AND RefundAskLog_ID=(SELECT Max(RefundAskLog_ID) FROM tblRefundAskLog r2 WHERE RefundAsk_ID=r1.RefundAsk_ID)"
		sWhere = sWhere & ")"
	End If
end if

If PageSecurity.IsLimitedMerchant Then
	sWhere = sWhere & " AND tblRefundAsk.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
End If
If PageSecurity.IsLimitedDebitCompany Then
	sWhere = sWhere & "AND tblDebitTerminals.DebitCompany IN (SELECT ID FROM GetPermittedDebitCompanies('" & PageSecurity.Username & "'))"
End If

Function Make2DigitNum(value)
	Make2DigitNum = value
	If Len(value) < 2 Then Make2DigitNum = "0" & value
End Function

If TestNumVar(Request("ExcelSend"), 0, -1, 0) > 0 Then 'do send to excel
    Dim exWhere : exWhere = " WHERE RefundRequestID IS NOT NULL "
	If Trim(request("ShowCompanyID"))<>"" AND Trim(request("ShowCompanyID"))<>"null" Then exWhere=exWhere & " AND (InternalMID=" & trim(request("ShowCompanyID")) & ")"
	If trim(request("DebitCompanyID"))<>"" Then exWhere=exWhere & " AND DebitCompany IN (" & request("DebitCompanyID") & ")"
	If trim(request("TerminalNumber"))<>"" Then exWhere=exWhere & " AND TerminalNumber='" & DBText(request("TerminalNumber")) & "'"
	dDateMax = Trim(Request("toDate") & " " & Request("toTime"))
	dDateMin = Trim(Request("fromDate") & " " & Request("fromTime"))
	If IsDate(dDateMax) Then exWhere=exWhere & " AND (RequestDate <='" & CDate(dDateMax) & "')"
	If IsDate(dDateMin) Then exWhere=exWhere & " AND (RequestDate >='" & CDate(dDateMin) & "')"
	If trim(request("RefundAskStatus"))<>"" Then exWhere = exWhere & " AND RefundAskStatus IN (" & request("RefundAskStatus") & ")"
	exWhere = exWhere & GetListWhere("RequestNumber", "RefundRequest")
	exWhere = exWhere & GetListWhere("OriginalTransID", "TID")
    sSQL = "SELECT * From GetUnprocessedRefundRequestsWithCardNumber(0) " & exWhere
    'Response.Write(sSQL) : Response.End()
	Set rsData = oleDbData.Execute(sSQL)
	If TestNumVar(Request("ExcelSend"), 0, -1, 2) = 2 Then
		Response.ContentType = "application/csv"
		Response.AddHeader "content-disposition", "attachment;filename=report.csv"
		Response.Write("MID;PAN / CC;TXN Amount;TXN Currency;Approval Code;MRF;TXN Date" & vbCrLf)
		Do While Not rsData.EOF
			Response.Write(Right(rsData("ContractNumber"), Len(rsData("ContractNumber")) - 4)  & ";")
			Response.Write(LCase(Replace(rsData("CardNumber"), " " , "")) & ";")
			Response.Write(rsData("RequestAmount") & ";")
			Response.Write(rsData("RequestCurrency") & ";")
			Response.Write(rsData("ApprovalCode") & ";")
			Response.Write(Left(rsData("MRF"), Len(rsData("MRF")) - 3) & ";")
			Response.Write(Make2DigitNum(Day(rsData("TransDate"))) & "." & Make2DigitNum(Month(rsData("TransDate"))) & "." & Year(rsData("TransDate")))
			Response.Write(vbCrLf)
		  rsData.MoveNext
		Loop
	Else
		Response.ContentType = "application/vnd.ms-excel"
		Response.AddHeader "content-disposition", "attachment;filename=report.xls"
		Response.Write("<table border=""1""><tr>")
		For i = 0 To rsData.Fields.Count - 1
			Response.Write("<th>" & rsData.Fields(i).Name & "</th>")
		Next
		Response.Write("</tr>")
		Do While Not rsData.EOF
			Response.Write("<tr>")
			For i = 0 To rsData.Fields.Count - 1
    			Response.Write("<td>" & rsData(i) & "</td>")
			Next
			Response.Write("</tr>")
		  rsData.MoveNext  
		Loop
		Response.Write("</table>")
	End If
	rsData.Close
    Response.End()
End If

Set rsData = server.createobject("adodb.recordset")
sSQL="SELECT tblCompany.CompanyName, tblCompanyTransPass.CreditCardID, tblCompanyTransPass.PaymentMethodDisplay, tblCompanyTransPass.TerminalNumber, tblCompanyTransPass.PaymentMethod, " &_
	"tblDebitCompany.debitCompany_id AS DebitCompanyID, tblDebitCompany.dc_isSupportPartialRefund, tblCompanyTransPass.insertdate AS OriginalTransDate, tblCompanyTransPass.Amount as FullAmount, tblRefundAsk.id, tblRefundAsk.CompanyID, tblRefundAsk.transID, tblRefundAsk.RefundAskAmount, tblRefundAsk.RefundAskCurrency, " &_
	"tblRefundAsk.RefundAskComment, tblRefundAsk.RefundAskDate, tblRefundAsk.RefundAskStatus, tblRefundAsk.RefundAskStatusHistory, tblRefundAsk.RefundFlag, tblRefundAsk.RefundType, tblCreditCard.BINCountry, " &_
	"dt_IsRefundBlocked, dc_isAllowPartialRefund ,tblChargebackReason.IsPendingChargeback " &_
    "FROM tblRefundAsk " &_
    "LEFT JOIN tblCompany ON tblRefundAsk.companyID = tblCompany.ID " &_
    "LEFT JOIN tblCompanyTransPass ON tblCompanyTransPass.ID = tblRefundAsk.transID " &_
    "LEFT JOIN tblCreditCard ON(tblCompanyTransPass.CreditCardID = tblCreditCard.ID) " &_
    "LEFT JOIN tblDebitTerminals ON tblDebitTerminals.terminalNumber = tblCompanyTransPass.TerminalNumber " &_
    "LEFT JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany = tblDebitCompany.debitCompany_id " &_
    "LEFT JOIN tblRetrivalRequests ON(tblRetrivalRequests.TransPass_ID = tblRefundAsk.transID) " &_
    "LEFT JOIN tblChargebackReason ON(tblChargebackReason.ReasonCode = tblRetrivalRequests.RR_ReasonCode) " &_
	sWhere & " ORDER BY tblRefundAsk.RefundAskDate DESC, tblRefundAsk.ID DESC"
'Response.Write(sSQL) : Response.End()

call openRsDataPaging(session("PageSize"))

'sQueryString="PageSize=" & request("PageSize") & "&source=" & request("source") & "&ShowCompanyID=" & request("ShowCompanyID") & "&fromDate=" & trim(request("fromDate")) & "&toDate=" & trim(request("toDate")) & "&fromTime=" & trim(request("fromTime")) & "&toTime=" & trim(request("toTime")) & "&RefundAskStatus0=" & trim(request("RefundAskStatus0")) & "&RefundAskStatus1=" & trim(request("RefundAskStatus1")) & "&RefundAskStatus22=" & trim(request("RefundAskStatus22")) & "&RefundAskStatus24=" & trim(request("RefundAskStatus24")) & "&RefundAskStatus25=" & trim(request("RefundAskStatus25")) & "&RefundAskStatus3=" & trim(request("RefundAskStatus3")) & "&DebitCompanyID=" & trim(""&request("DebitCompanyID")) & "&TerminalNumber=" & trim(""&request("TerminalNumber")) & "&fdtrControl%24rdtpFrom=" & server.URLEncode(trim(""&request("fdtrControl%24rdtpFrom"))) & "&fdtrControl_rdtpFrom_dateInput_text=" & server.URLEncode(trim(""&request("fdtrControl_rdtpFrom_dateInput_text"))) 
'sQueryString = sQueryString & "&fdtrControl%24rdtpFrom%24dateInput=" & server.URLEncode(trim(""&request("fdtrControl%24rdtpFrom%24dateInput"))) & "&fdtrControl_rdtpFrom_calendar_SD=" & server.URLEncode(trim(""&request("fdtrControl_rdtpFrom_calendar_SD"))) & "&fdtrControl_rdtpFrom_calendar_AD=" & server.URLEncode(trim(""&request("fdtrControl_rdtpFrom_calendar_AD"))) & "&fdtrControl%24rdtpTo=" & server.URLEncode(trim(""&request("fdtrControl%24rdtpTo"))) & "&fdtrControl_rdtpTo_dateInput_text=" & server.URLEncode(trim(""&request("fdtrControl_rdtpTo_dateInput_text"))) & "&fdtrControl%24rdtpTo%24dateInput=" & server.URLEncode(trim(""&request("fdtrControl%24rdtpTo%24dateInput"))) & "&fdtrControl_rdtpTo_calendar_SD=" & server.URLEncode(trim(""&request("fdtrControl_rdtpTo_calendar_SD"))) & "&fdtrControl_rdtpTo_calendar_AD=" & server.URLEncode(trim(""&request("fdtrControl_rdtpTo_calendar_AD")))
sQueryString=""
for each fldItem in Request.QueryString
	if trim(Request.QueryString(fldItem))<>"" and UCase(fldItem)<>"ACTION" and UCase(fldItem)<>"PAGE" and UCase(fldItem)<>"PAGESIZE" and UCase(fldItem)<>"CLEANREFUNDASKID" and UCase(fldItem)<>"REFUNDASKID" and Left(Ucase(fldItem), 15)<>"REFUNDASKACTION" then
		sQueryString=sQueryString & "&" & fldItem & "=" & request.QueryString(fldItem)
	end if
next
for each fldItem in Request.Form
	if trim(Request.QueryString(fldItem))="" and trim(Request.Form(fldItem))<>"" and UCase(fldItem)<>"ACTION" and UCase(fldItem)<>"PAGE" and UCase(fldItem)<>"PAGESIZE" and UCase(fldItem)<>"CLEANREFUNDASKID" and UCase(fldItem)<>"REFUNDASKID" and Left(Ucase(fldItem), 15)<>"REFUNDASKACTION" then
		sQueryString=sQueryString & "&" & fldItem & "=" & request.Form(fldItem)
	end if
next
sQueryString = UrlWithout(UrlWithout(sQueryString, "__VIEWSTATE"), "__EVENTVALIDATION")
If Request("Totals") = "1" Then
	Response.CharSet = "windows-1255"
	Response.Write("<table class=""formThin"" width=""100%"">")
	Response.Write("<tr>")
		Response.Write("<td>TOTALS</td>")
		Response.Write("<td style=""text-align:right;cursor:pointer;float:right;"" onclick=""document.getElementById('tblTotals').style.display='none';""><b>X</b></td>")
	Response.Write("</tr>")
	Response.Write("</table>")
	Response.Write("<table class=""formNormal"" width=""250"" style=""margin:8px 5px;"">")
	Response.Write("<tr>")
		Response.Write("<th style=""text-align:right"" width=""50%"">Count</th>")
		Response.Write("<th style=""text-align:right"">Amount</th>")
	Response.Write("</tr>")
	sSQL="SELECT RefundAskCurrency, Count(*) Quantity, Sum(RefundAskAmount) Total FROM tblDebitTerminals " &_
	"LEFT OUTER JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany = tblDebitCompany.debitCompany_id " &_
	"RIGHT OUTER JOIN tblCompanyTransPass ON tblDebitTerminals.terminalNumber = tblCompanyTransPass.TerminalNumber " &_
	"LEFT OUTER JOIN tblCreditCard ON(tblCompanyTransPass.CreditCardID = tblCreditCard.ID) " &_
	"RIGHT OUTER JOIN tblRefundAsk " &_
	"LEFT OUTER JOIN tblCompany ON tblRefundAsk.companyID = tblCompany.ID ON tblCompanyTransPass.ID = tblRefundAsk.transID " &_
	" " & sWhere & " GROUP BY RefundAskCurrency"
	set rsData=oleDbData.Execute(sSQL)
	Do While Not rsData.EOF
		Response.Write("<tr><td style=""text-align:right"">" & rsData(1) & "</td><td style=""text-align:right"">" & FormatCurr(rsData(0), rsData(2)) & "</td></tr>")
	  rsData.MoveNext
	Loop
	rsData.Close
	Response.Write("</table>")
	Set rsData = nothing
	call closeConnection()
	Response.End()
End If
%>
<html>
<head>
	<title>Refund request data</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css"/>
	<script type="text/javascript" src="../js/func_common.js"></script>
	<script type="text/javascript">
		function ExpandNode(objId) {
			event.srcElement.src = (event.srcElement.src.indexOf("expand") > 0 ? "../images/tree_collapse.gif" : "../images/tree_expand.gif");
			objRef = document.getElementById("Row" + objId)
			objRef2 = document.getElementById("trRow" + objId)
			if (objRef.innerHTML == '') {
				objRef.innerHTML = '<iframe framespacing="0" scrolling="No" marginwidth="0" frameborder="0" border="0" width="100%" height="0px" src="Refund_ask_update.asp?RefundAskID=' + objId + '" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>';
				objRef.style.borderTop = '1px dashed silver';
				objRef2.style.display = 'block';
			}
			else {
				objRef.innerHTML = '';
				objRef.style.borderTop = '';
				objRef2.style.display = 'none';
			}
		}
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">
<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
<tr>
	<td id="pageMainHeadingTd">
		<span id="pageMainHeading">REFUND REQUESTS</span>
		<span class="txt14">- SEARCH RESULTS</span>
	</td>
	<td></td>
	<td>
		<table border="0" cellspacing="0" cellpadding="1" align="right">
		<tr>
			<td width="6" bgcolor="#7d7d7d"></td>
			<td width="40">New</td>
			<td width="6" bgcolor="#6699cc"></td>
			<td width="80">In Progress</td>
			<td width="6" bgcolor="#66cc66"></td>
			<td width="65">Refunded</td>
			<td width="6" bgcolor="#ff6666"></td>
			<td width="60">Canceled</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />
<%
pageSecurityLevel = 0
pageClosingTags = ""
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
%>
<table width="95%" border="0" cellspacing="0" cellpadding="1" align="center">
<%
if not rsData.EOF then
	%>
	<tr>
		<td>
			<form action="refund_ask_data.asp?<%= sQueryString %>" name="frmRefundAsk" id="frmRefundAsk" target="frmBody" method="post">
			<table class="formNormal" width="100%">	
			<tr>
				<th colspan="2"><br /></th>
				<th>Request<br /></th>
				<th>Merchant<br /></th>
				<th>Payment Method<br /></th>
				<th>Refund<br />Amount<br /></th>
				<th>Refund<br />Fee<br /></th>
				<th>Terminal *</th>
				<th>Original Trans.<br /></th>
				<th><br/></th>
				<td width="20"></td>
				<th class="Vertical" width="20">Cancel<br/></th>
				<th class="Vertical" width="20">Print<br/></th>
				<th class="Vertical" width="30">Create<br />Refund<br/></th>
				<th class="Vertical" width="30">Process<br />Refund<br/></th>
				<th class="Vertical" width="30">Invik<br />Batch<br/></th>
				<th style="text-align:center;">Reset<br/></th>
			</tr>
			<tr><td height="3"></td></tr>
			<%
			bBkgChange = False
			Set fsServer=Server.CreateObject("Scripting.FileSystemObject")
			
			rsData.MoveFirst
			Do until rsData.EOF

				bBkgChange = NOT bBkgChange
				sTrStyle = IIF(bBkgChange,"background-color:#ffffff;", "background-color:#f0f0f0;")
				sInputStyle = IIF(bBkgChange,"background-color:#ffffff;", "background-color:#f0f0f0;")
				
				sStatusColor = ""
				If rsData("RefundAskStatus")="0" then 'New
					sStatusColor = "#7d7d7d"
				ElseIf rsData("RefundAskStatus")="1" then 'In Progress
					sStatusColor ="#6699cc"
				ElseIf rsData("RefundAskStatus")="2" OR rsData("RefundAskStatus")="4" OR rsData("RefundAskStatus")="5" Then 'Was refund
					sStatusColor = "#66cc66"
				ElseIf rsData("RefundAskStatus")="3" OR rsData("RefundAskStatus")="6" then ' Canceled
					sStatusColor = "#ff6666"
				End If

				Select case rsData("RefundFlag")
					Case "0"	sFlagSrc="../images/flag_clear.gif"
					Case "1"	sFlagSrc="../images/flag_green.gif"
					Case "2"	sFlagSrc="../images/flag_red.gif"
					Case else	sFlagSrc="../images/flag_clear.gif"
				End select
				
				sCompanyName = trim(rsData("CompanyName"))

				sRButtonShow1 = false
				sRButtonShow2 = false
				sRButtonShow3 = false
				sRButtonShow4 = false
				sRButtonShow5 = false
				If rsData("RefundAskStatus")="0" Then
					sRButtonShow1 = true
					sRButtonShow2 = true
					sRButtonShow3 = true
					sRButtonShow4 = true
					sRButtonShow5 = true
				ElseIf rsData("RefundAskStatus")="1" Then
					sRButtonWhereV = "1"
					sRButtonShow2 = true
					sRButtonShow3 = true
					sRButtonShow4 = true
					sRButtonShow5 = true
				ElseIf rsData("RefundAskStatus")="2" Then
					sRButtonWhereV = "2"
				ElseIf rsData("RefundAskStatus")="3" Then
					sRButtonWhereV = "3"
				ElseIf rsData("RefundAskStatus")="4" Then
					sRButtonWhereV = "4"
				ElseIf rsData("RefundAskStatus")="5" Then
					sRButtonWhereV = "5"
				ElseIf rsData("RefundAskStatus")="6" Then
					sRButtonWhereV = "0"
				End If
                If TestNumVar(rsData("IsPendingChargeback"), 0, -1, 0) <> 0 Then
				    sRButtonShow1 = false
				    sRButtonShow3 = false
				    sRButtonShow4 = false
				    sRButtonShow5 = false
                End If
				'20110109 Tamir - allow modifying refund amount
				nRefundAskAmount = rsData("RefundAskAmount")
				%>
				<input type="hidden" name="RefundAskID" value="<%= rsData("id") %>">
				<input type="hidden" name="RefundAskTransID<%= rsData("id") %>" value="<%= rsData("TransID") %>">
				<input type="hidden" name="RefundAskPaymentMethodId<%= rsData("id") %>" value="<%= rsData("CreditCardID") %>">
				<input type="hidden" name="RefundAskAmount<%= rsData("id") %>" value="<%= nRefundAskAmount %>">
				<input type="hidden" name="RefundAskCurrency<%= rsData("id") %>" value="<%= rsData("RefundAskCurrency") %>">
				<tr style="<%=sTrStyle%>">
					<td style="width:10px;"><img onclick="ExpandNode('<%= rsData("ID") %>');" style="cursor:pointer;" id="oListImg<%= rsData("ID") %>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle"><br /></td>
					<td style="width:10px;"><span Style="background-color:<%= sStatusColor %>;">&nbsp;</span></td>
					<td>
						<%= rsData("ID") %><br />
						<%= FormatDatesTimes(rsData("RefundAskDate"),1,1,0) %><br />
					</td>
					<td valign="middle">
                        <a href="#" onclick="OpenPop('../include/common_totalTransShow.asp?LNG=1&companyID=<%= rsData("CompanyID") %>&Currency=<%=rsData("RefundAskCurrency") %>&isNotCalcAll=&isIframe=0', 'fraOrderPrint', 750, 300, 1);">
						<%= left(dbtextShow(sCompanyName),19) %>
						<%
						If int(len(dbtextShow(sCompanyName)))>19 then
							response.write ("...")
						end if
						%>
                        </a><br />
					</td>
					<td valign="middle">
						<%
						If rsData("PaymentMethod")>=2 Then
							Response.Write("<img src=""/NPCommon/ImgPaymentMethod/23X12/" & rsData("PaymentMethod") & ".gif"" align=""middle"" border=""0"" /> &nbsp;")
							If Trim("" & rsData("BinCountry")) <> "" Then
								Response.Write("<img src=""/NPCommon/ImgCountry/18X12/" & rsData("BinCountry") & ".gif"" align=""middle"" border=""0"" />")
								Response.Write(" &nbsp;" & rsData("BinCountry"))
							End If
						End If
						Response.Write(" " & Trim("" & rsData("PaymentMethodDisplay")))
						%>
					</td>
					<td>
						<span style="font-family:Monospace;"><%=GetCurISO(rsData("RefundAskCurrency"))%></span>
						<%
							If (rsData("RefundAskStatus") = 0 Or rsData("RefundAskStatus") = 1) And rsData("dc_isAllowPartialRefund") Then
								sReadOnly=""
								sStyle=""
							Else
								sStyle="border-width:0 !important;"
								sReadOnly=" readonly=""readonly"" "
							End if
						%>
						<input name="RefundAskAmountNew<%= rsData("id") %>" value="<%= FormatNumber(nRefundAskAmount, 2, True, False, True) %>" class="inputprice" <%= sReadOnly %> style="width:50px;text-align:right;<%= sStyle %>" />
					</td>
                    <td>
                        <select name="RefundAskFeeNew<%= rsData("id") %>" class="inputprice" <%= sReadOnly %> style="width:70px;<%= sStyle %>" >
                            <option value="0" <%= IIF(rsData("RefundType") = 0, " selected", "") %>>Normal</option>
                            <option value="1" <%= IIF(rsData("RefundType") = 1, " selected", "") %>>Partial</option>
                            <option value="2" <%= IIF(rsData("RefundType") = 2, " selected", "") %>>Fraud</option>
                        </select>
                    </td>
					<td valign="middle">
						<%
						If rsData("PaymentMethod")>14 AND trim(rsData("DebitCompanyID"))<>"" Then
							If fsServer.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & trim(rsData("DebitCompanyID")) & ".gif")) Then
								Response.write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(rsData("DebitCompanyID")) & ".gif"" align=""middle"" border=""0"" /> ")
							End if
						End if
						Response.Write(rsData("TerminalNumber"))
						If rsData("dt_IsRefundBlocked") Then
							Response.Write(" <img src=""\NPCommon\images\alert_small.png"" align=""middle"" title=""Special terminal - Refunds cannot be processed"">")
						End if
						%>
					</td>
					<td>
						<a class="submenu2" style="font-size:11px; text-decoration:underline;" target="_blank" href="trans_detail_cc.asp?transID=<%= int(rsData("transID")) %>&CompanyID=<%= rsData("CompanyID") %>&PaymentMethod=<%= rsData("PaymentMethod") %>&TransTableType=pass"><%= rsData("transID") %></a>
                        <%=IIF(TestNumVar(rsData("IsPendingChargeback"), 0, -1, 0) <> 0, "(Pending CHB)", "")%>
                        <br />
						<%= FormatDatesTimes(rsData("OriginalTransDate"),1,1,0) %><br />
					</td>
					<td style="text-align:center;">
						<a href="common_flagUpdate.asp?tblName=tblRefundAsk&FieldName=RefundFlag&FieldIdName=id&FieldIdValue=<%= rsData("id") %>" target="emptyIframe"><img src="<%= sFlagSrc %>" name="flagImage<%= rsData("id") %>" id="flagImage<%= rsData("id") %>" width="19" height="18" border="0"></a><br />
					</td>
					<td style="background-color:#ffffff;"></td>
					<td style="text-align:center;">
						<%
						If sRButtonShow2 or sRButtonWhereV="5" then
							Response.Write("<input type=""Radio"" name=""RefundAskAction" & rsData("id") & """ value=""3"" style=""" & sInputStyle & """><br />")
						Elseif sRButtonWhereV="3" then
							Response.Write("<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />")
						Else
							Response.Write("<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />")
						End If
						%>
					</td>
					<td style="text-align:center;">
						<%
						If sRButtonShow1 then
							Response.Write("<input type=""Radio"" name=""RefundAskAction" & rsData("id") & """ value=""1"" style=""" & sInputStyle & """><br />")
						Elseif sRButtonWhereV="1" then
							Response.Write("<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />")
						Else
							Response.Write("<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />")
						End If
						%>
					</td>
					<td style="text-align:center;">
						<%
						If sRButtonShow3 or sRButtonWhereV="5" then
							Response.Write("<input type=""Radio"" name=""RefundAskAction" & rsData("id") & """ value=""2"" style=""" & sInputStyle & """><br />")
						Elseif sRButtonWhereV="2" then
							Response.Write("<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />")
						Else
							Response.Write("<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />")
						End If
						%>
					</td>
					<td style="text-align:center;">
						<%
						If sRButtonShow4 then
							Dim nDisabled : nDisabled = ""
							IF (rsData("RefundAskAmount") < rsData("FullAmount")) Then _
								If Not rsData("dc_isSupportPartialRefund") Then nDisabled = " disabled "
							Response.Write("<input type=""Radio"" name=""RefundAskAction" & rsData("id") & """ value=""4"" style=""" & sInputStyle & """ " & nDisabled & "><br />")
						Elseif sRButtonWhereV="4" then
							Response.Write("<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />")
						Else
							Response.Write("<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />")
						End If
						%>
					</td>
					<td style="text-align:center;">
						<%
						If sRButtonShow5 then
							If rsData("DebitCompanyID")=21 Then
								sValue="5"
								sDisabled=""
							Else
								sValue=""
								sDisabled=" disabled=""disabled"""
							End If
							Response.Write("<input type=""Radio"" name=""RefundAskAction" & rsData("id") & """ value=""" & sValue & """ style=""" & sInputStyle & """" & sDisabled & "><br />")
						Elseif sRButtonWhereV="5" then
							Response.Write("<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />")
						Else
							Response.Write("<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />")
						End If
						%>
					</td>
					<td style="text-align:center;">
						<%
						If rsData("RefundAskStatus")<>"0" AND rsData("RefundAskStatus")<>"4" AND rsData("RefundAskStatus")<>"6" then
							%>
							<a href="refund_ask_data.asp?Action=clean&CleanRefundAskID=<%=rsData("id")%>&page=<%=session("ActualPage")%><%=sQueryString%>" onclick="return confirm('Sure to reset and allow new selections?');">RESET</a><br />
							<%
						else
							%>
							<span style="color:#d5d5d5; font-size:10px;">RESET</span><br />
							<%
						End If
						%>
					</td>
				</tr>
				<tr id="trRow<%=rsData("ID")%>" style="display:none;"><td id="Row<%=rsData("ID")%>" colspan="17"></td></tr>
				<tr>
					<td height="1" colspan="10" bgcolor="#c5c5c5"></td>
					<td></td>
					<td height="1" colspan="6" bgcolor="#c5c5c5"></td>
				</tr>
				<%
				nShowCounter=nShowCounter+1
				rsData.movenext
			Loop
			Set fsServer=Nothing
			%>
			<tr><td height="15"></td></tr>
			</table>
			<table width="100%">	
			<tr>
				<td align="left">
					<%
						showPaging "refund_ask_data.asp", "Eng", sQueryString
					%>
				</td>
				<td align="center">
					<input type="button" value="View Excel" onclick="document.location.href='Refund_ask_data_excel.asp?<%=sQueryString%>'" />
					<input type="button" value="View Excel To Send" onclick="document.location.href='Refund_ask_data.asp?ExcelSend=1&<%=sQueryString%>'" />
					<input type="button" value="View CSV To Send" onclick="document.location.href='Refund_ask_data.asp?ExcelSend=2&<%=sQueryString%>'" />
					<input type="button" id="TotalsButton" style="font-size:11px;cursor:pointer;" value="SHOW TOTALS &Sigma;"
					 onclick="with(document.getElementById('tblTotals')){style.display='block';if(onresize)onresize();};setAjaxHTML('?Totals=1<%=sQueryString%>', document.getElementById('tblTotals'), true);"  />
				</td>
				<td align="right" valign="top">
					<input type="reset" name="Action" value="RESET" class="button1"> &nbsp;
					<input type="hidden" name="Action" value="SUBMIT" />
					<%
						for each fldItem in Request.QueryString
							if trim(Request.QueryString(fldItem))<>"" and UCASE(fldItem)<>"ACTION" and UCase(fldItem)<>"PAGE" and UCase(fldItem)<>"PAGESIZE" and UCase(fldItem)<>"CLEANREFUNDASKID" and UCase(fldItem)<>"REFUNDASKID" and Left(Ucase(fldItem), 15)<>"REFUNDASKACTION" then
								%>
                                <input type="hidden" name="<%= fldItem %>" value="<%= Server.HTMLEncode(request.QueryString(fldItem)) %>" />
                                <%
							end if
						next
						for each fldItem in Request.Form
							if trim(Request.QueryString(fldItem))="" and trim(Request.Form(fldItem))<>"" and UCase(fldItem)<>"ACTION" and UCase(fldItem)<>"PAGE" and UCase(fldItem)<>"PAGESIZE" and UCase(fldItem)<>"CLEANREFUNDASKID" and UCase(fldItem)<>"REFUNDASKID" and Left(Ucase(fldItem), 15)<>"REFUNDASKACTION" then
								%>
                                <input type="hidden" name="<%= fldItem %>" value="<%= Server.HTMLEncode(request.Form(fldItem)) %>" />
                                <%
							end if
						next
					%>
					<input type="submit" class="button1" value="SUBMIT" onclick="disabled=true;form.submit();">
					<iframe name="emptyIframe" id="emptyIframe" width="10" height="5" marginwidth="0" marginheight="0" scrolling="no" frameborder="0"></iframe>
				</td>
			</tr>
			</table>
			</form>
		</td>
	</tr>
	<%
Else
	Response.Write("<tr><td style=""border:1px solid silver; padding:5px;"">No records found</td></tr>")
End If
If 1 = 0 Then
	nInvik = ExecScalar("SELECT Count(*) FROM tblInvikRefundBatch WHERE irb_IsDownloaded=0", -1)
	%>
		<tr>
			<td style="text-align:right;">
				<br />
				Invik refund batch contains <%= IIf(nInvik>0, nInvik, "no") %> records.
				<%
					If nInvik > 0 Then
						%>
							<a href="Refund_Ask_DownloadInvikBatch.aspx" target="ifrInvik">Download</a>
							<span style="visibility:hidden;"><iframe id="ifrInvik" name="ifrInvik" src="common_blank.htm" width="1" height="1"></iframe></span>
						<%
					End If
				%>
			</td>
		</tr>
	<%
End If
rsData.close
Set rsData = nothing
Call closeConnection()
%>
</table>
<div id="tblTotals" onresize="PosObject(this, document.getElementById('TotalsButton'), 'RRTB', 0, -10);" style="border-color:#484848; border-style:solid; border-width:1px 3px; position:absolute;width:260px;background-color:White;display:none;"> &nbsp; LOADING DATA... </div>
</body>
</html>