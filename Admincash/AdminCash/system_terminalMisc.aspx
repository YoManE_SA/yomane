<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    Dim iReader As SqlDataReader
    Dim ChargeMade As Boolean = False
    Dim bIsChargeMade As Boolean = False
    Dim sTerminalNumber As String = ""
    Dim sDebitCompany As String = ""
    Dim DCF_TerminalNumber As String = ""
    Dim DCF_DebitCompanyID As String = ""
    Dim nTerminalID As Integer
    Dim isUsedByCompany As Boolean = False
    Sub Page_Load()
        Security.CheckPermission(lblPermissions)
        Dim sQueryWhere As String
        If Not Page.IsPostBack Then
            nTerminalID = dbPages.TestVar(Request("terminalID"), 1, 0, 0)
            If nTerminalID > 0 Then
                litTerminalName.Text = dbPages.ExecScalar("SELECT dc_Name+' | '+TerminalNumber+' | '+dt_Name FROM tblDebitCompany" & _
                " INNER JOIN tblDebitTerminals ON DebitCompany_ID=DebitCompany WHERE ID=" & nTerminalID)
                tlbTop.AddItem("Terminal Details", "system_terminalTermDetail.aspx?terminalID=" & nTerminalID)
                tlbTop.AddItem("Card Company Details", "system_terminalCardDetail.aspx?terminalID=" & nTerminalID)
                tlbTop.AddItem("Fees & Payouts", "system_terminalFees.aspx?terminalID=" & nTerminalID)
                tlbTop.AddItem("Notifications", "system_terminalRules.aspx?TerminalID=" & nTerminalID)
                tlbTop.AddItem("Miscellaneous", "system_terminalMisc.aspx?terminalID=" & nTerminalID, True)
                sQueryWhere = "tblDebitTerminals.id=" & nTerminalID.ToString
                terminalID.Value = nTerminalID
            Else
                sQueryWhere = "tblDebitTerminals.terminalNumber='" & request("terminalNumber") & "'"
                litTerminalName.Text = "All Debit Companies"
            End If
            Dim sSQL As New StringBuilder(String.Empty)
            sSQL.Append("SELECT tblDebitTerminals.*, dbo.GetDecrypted256(accountPassword256) accountPassword, dbo.GetDecrypted256(accountPassword3D256) accountPassword3D")
            sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransPass WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransAnswer")
            sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransFail WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransFail")
            sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransApproval WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransApprovalOnly")
            sSQL.Append(", (SELECT Count(ID) FROM tblCompanyTransRemoved WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransDel")
            sSQL.Append(" FROM tblDebitTerminals WHERE " & sQueryWhere)
            iReader = dbPages.ExecReader(sSQL.ToString)
            If iReader.HasRows Then
                While iReader.Read
                    If iReader("isActive") Then
                        ltIsActiveTerm.Text = "<span><li type=square style=color:#66cc66;> <span class=txt11>Active</span></li></span>"
                        btnOpenClose.Text = "CLOSE"
                        btnOpenClose.OnClientClick = "javascript:return confirm('Sure to close?')"
                        btnOpenClose.CommandName = "close"
                    Else
                        ltIsActiveTerm.Text = "<span><li type=square style=color:#ff6666;> <span class=txt11>Blocked</span></li></span>"
                        btnOpenClose.Text = "OPEN"
                        btnOpenClose.OnClientClick = "javascript:return confirm('Sure to open?')"
                        btnOpenClose.CommandName = "open"
                    End If
                    sTerminalNumber = trim(iReader("terminalNumber"))
                    hidTerminalNumber.Value = sTerminalNumber
                    sDebitCompany = trim(iReader("DebitCompany"))
                    hidDebitCompany.Value = sDebitCompany
                    DCF_TerminalNumber = trim(iReader("terminalNumber"))
                    DCF_DebitCompanyID = trim(iReader("DebitCompany"))
                    ChargeMade = Not (iReader("numTransAnswer") = 0 And iReader("numTransFail") = 0 And iReader("numTransApprovalOnly") = 0 And iReader("numTransDel") = 0)
                    isChargeMade.Value = ChargeMade
                    If ChargeMade Then
                        chkDel.Enabled = False
                    End If
                    ltTransDel.Text = iReader("numTransDel")
                    ltTransApprovalOnly.Text = iReader("numTransApprovalOnly")
                    ltTransFail.Text = iReader("numTransFail")
                    ltTransAnswer.Text = iReader("numTransAnswer")
                End While
            Else
                pnlTRM.Visible = False
            End If
            iReader.Close()
        Else
            sTerminalNumber = hidTerminalNumber.Value
            sDebitCompany = hidDebitCompany.Value
        End If
        If Not Page.IsPostBack Then
            dsCompany.ConnectionString = dbPages.DSN
            dsCompany.SelectCommand = "SELECT tblCompany.ID, tblCompany.CompanyName FROM tblCompanyCreditFeesTerminals INNER JOIN tblCompany ON tblCompanyCreditFeesTerminals.CCFT_CompanyID = tblCompany.ID WHERE (tblCompanyCreditFeesTerminals.CCFT_Terminal = '" & sTerminalNumber & "') GROUP BY tblCompany.ID, tblCompany.CompanyName"
            ddlCompanyID.DataBind()
        End If
    End Sub

    Private Sub CheckCompany(sender As Object, e As System.EventArgs)
        If ddlCompanyID.Items.Count > 0 then
            ddlCompanyID.Visible = True
            ltMerchantList.Visible = True
            btnUpdateCompany.Visible = True
            isUsedByCompany = True
            chkDeleteFlag.Enabled = False
        Else
            chkDeleteFlag.Enabled = True
            ltNoCompany.Visible = True
        End If
    End Sub
    Private Sub UpdateCompany(sender As Object, e As System.EventArgs)
        Response.Redirect("merchant_dataTerminal.asp?ShowSec=4&companyID=" & ddlCompanyID.SelectedValue)
    End Sub

    Private Sub OpenClose_Status(sender As Object, e As System.EventArgs)
        If chkDeleteFlag.Checked then
            btnOpenClose.Enabled = True
        Else
            btnOpenClose.Enabled = False
        End If
    End Sub

    Private Sub TerminalStatus(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs)
        Select Case e.CommandName.ToLower
            Case "close"
                dbPages.ExecSql("UPDATE tblDebitTerminals SET isActive = 0 WHERE id = " & terminalID.Value)
                btnOpenClose.Text="OPEN"
                btnOpenClose.OnClientClick="javascript:return confirm('Sure to open?')"
                btnOpenClose.CommandName="open"
            Case "open"
                dbPages.ExecSql("UPDATE tblDebitTerminals SET isActive = 1 WHERE id = " & terminalID.Value)
                btnOpenClose.Text="CLOSE"
                btnOpenClose.OnClientClick="javascript:return confirm('Sure to close?')"
                btnOpenClose.CommandName="close"
        End Select
        chkDeleteFlag.Checked = False
        btnOpenClose.Enabled = False
        Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
        Response.Write("parent.fraMenu.location.href='system_terminalFilter.asp?iDebitCompanyID=" & sDebitCompany & "&iTextSearch=" & Request.Cookies("DebitTerminalSearch")("textValue") & "&iStatus=" & Request.Cookies("DebitTerminalSearch")("statusValue") & "&iSortType=" & Request.Cookies("DebitTerminalSearch")("sortTypeValue") & "&from=Filter';")
        Response.Write("</s" & "cript>")
        Response.Flush
    End Sub

    Private Sub Delete_Status(sender As Object, e As System.EventArgs)
        If chkDel.Checked then
            btnDelete.Enabled=True
        Else
            btnDelete.Enabled=False
        End If
    End Sub

    Private Sub TerminalDelete(sender As Object, e As System.EventArgs)
        dbPages.ExecSql("DELETE FROM tblDebitTerminals WHERE ID=" & terminalID.Value)
        chkDel.Checked = False
        btnDelete.Enabled=False
        Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
        Response.Write("parent.fraMenu.location.href='system_terminalFilter.asp?iDebitCompanyID=" & sDebitCompany & "&iTextSearch=" & Session("DebitTerminalTextSearchValue") & "&iStatus=" & Session("DebitTerminalSearchStatusValue") & "&from=Filter';")
        Response.Write("</s" & "cript>")
        Response.Flush
        pnlTRM.Visible = False
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Terminals - Miscelaneous</title>
    <meta http-equiv="Content-Type" content="text/html;charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading{text-decoration:underline;white-space:nowrap;}
		table.formNormal td, table.formNormal th {text-align:center;}
		.btnSave { width:58px; height:52px; }
	</style>
	<script language="javascript" type="text/javascript" src="../js/func_common.js"></script>
</head>
<body>
    <form id="form1" runat="server" class="formNormal">
    <table align="center" style="width:95%;">
	<tr>
		<td id="pageMainHeadingTd">
			<asp:label ID="lblPermissions" runat="server" /><span id="pageMainHeading"> TERMINALS</span><span style="letter-spacing:-1px;"> - <asp:Literal ID="litTerminalName" runat="server" /></span>
		</td>
		<td>
			<table width="80" border="0" cellspacing="0" cellpadding="2" align="right" style="border:1px solid #c0c0c0;">
				<tr><td align="center" id="IsActiveTd"><asp:Literal ID="ltIsActiveTerm" runat="server" /><br /></td></tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
	<custom:Toolbar ID="tlbTop" Width="95%" runat="server" />
	<asp:Panel ID="pnlTRM" runat="server">
	<asp:HiddenField ID="terminalID" runat="server" />
	<asp:HiddenField ID="isChargeMade" runat="server" />
	<asp:HiddenField ID="hidTerminalNumber" runat="server" />
	<asp:HiddenField ID="hidDebitCompany" runat="server" />
	<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">		
		<tr>
			<td valign="top">
				<span class="MerchantSubHead">Terminal open/close</span>				
				<br />
			</td>
		</tr>
		<tr><td height="5"></td></tr>
		<tr>
			<td>
			<table border="0" cellpadding="1" cellspacing="1">
			<tr>
				<td>
					<asp:Literal ID="ltMerchantList" runat="server" Text="Merchant list &nbsp;" Visible="false" />
					<asp:DropDownList ID="ddlCompanyID" runat="server" DataSourceID="dsCompany" DataTextField="CompanyName" DataValueField="ID" Visible="false" OnDataBound="CheckCompany" />
					<asp:SqlDataSource ID="dsCompany" runat="server" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient" />
					<asp:Button ID="btnUpdateCompany" runat="server" OnClick="UpdateCompany" Text="UPDATE &gt;&gt;" Visible="false" style="background-color:#FFFFFF;border:1px #000000 solid;color:#000000;cursor:pointer;" />
					<asp:Literal ID="ltNoCompany" runat="server" Text="(There are no merchants with this terminal)<br />" Visible="false" />
				</td>
			</tr>
			<tr>
				<td align="left" valign="top" style="padding-top:5px; padding-bottom:5px;">
					<asp:Button ID="btnOpenClose" runat="server" Enabled="false" OnCommand="TerminalStatus" style="background-color:#FFFFFF;border:1px #000000 solid;color:#000000;cursor:pointer;" />
					<asp:CheckBox ID="chkDeleteFlag" runat="server" Enabled="false" OnCheckedChanged="OpenClose_Status" EnableViewState="false" AutoPostBack="true" />
				</td>
			</tr>
			<tr><td>* Closing terminal is not allowed while the terminal is used by merchants<br /></td></tr>
			</table>
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr><td class="MerchantSubHead" valign="top">Terminal delete<br /></td></tr>
		<tr><td height="5"></td></tr>
		<tr>
			<td>
				<table border="0" cellpadding="1" cellspacing="1" width="40%">
				<tr>
					<td style="text-decoration: underline;" valign="top">Pass<br /></td>
					<td style="text-decoration: underline;" valign="top">Fail<br /></td>
					<td style="text-decoration: underline;" valign="top">Pre-Auth<br /></td>
					<td style="text-decoration: underline;" valign="top">Del<br /></td>
				</tr>
				<tr>
					<td valign="top"><img src="../images/icon_ccOkTransShekel2.gif" alt="" width="15" height="15" border="0" align="middle"> (<asp:Literal ID="ltTransAnswer" runat="server" />)<br /></td>
					<td valign="top"><img src="../images/icon_ccFailedTrans.gif" alt="" width="15" height="15" border="0" align="middle"> (<asp:Literal ID="ltTransFail" runat="server" />)<br /></td>
					<td valign="top"><img src="../images/icon_ccOkTrans.gif" alt="" width="15" height="15" border="0" align="middle"> (<asp:Literal ID="ltTransApprovalOnly" runat="server" />)<br /></td>
					<td valign="top">Del (<asp:Literal ID="ltTransDel" runat="server" />)<br /></td>
				</tr>
				<tr>
					<td colspan="4" style="padding-top:5px; padding-bottom:5px;">
						<asp:Button ID="btnDelete" runat="server" Enabled="false" Text="DELETE" OnClientClick="javascript: return confirm('Sure to delete?')" OnClick="TerminalDelete" style="background-color:#FFFFFF;border:1px #000000 solid;color:#000000;cursor:pointer;" />
						<asp:CheckBox ID="chkDel" runat="server"  EnableViewState="true" AutoPostBack="true" OnCheckedChanged="Delete_Status" />
					</td>
				</tr>
				<tr>
					<td colspan="4">* Delete/Change terminal is not allowed since the terminal was used by merchants<br /></td>
				</tr>
				</table>
			</td>
		</tr>
	</table>
	</asp:Panel>
    </form>
</body>
</html>
