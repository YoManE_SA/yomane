<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script runat="server">
		Dim sQueryString, sDivider As String
		Const RGB_PASS As String = "#66CC66", RGB_FAIL As String = "#ff6666"
		
		Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
			Security.CheckPermission(lblPermissions)
			If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") Else PGData.PageSize = 25
			
			Dim sWhere As String = ""
			If Request("transID") <> "" Then
				sWhere &= " And ltj_TransID=" & dbPages.TestVar(Request("transID"), 0, -1, 0)
			Else
				If Request("fromDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And ltj_InsertDate >= '" & Request("fromDate") & " " & Request("fromTime") & "'"
				If Request("toDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And ltj_InsertDate <= '" & Request("toDate") & " " & Request("toTime") & "'"
				If Request("ltj_CompanyID") <> "" Then sWhere &= " And ltj_CompanyID=" & dbPages.TestVar(Request("ltj_CompanyID"), 0, -1, 0)
				If Request("ltj_DebitCompanyID") <> "" Then sWhere &= " And ltj_DebitCompanyID=" & dbPages.TestVar(Request("ltj_DebitCompanyID"), 0, -1, 0)
				If Request("ltj_TransReply") <> "" Then sWhere &= " And ltj_TransReply='" & Replace(Request("ltj_TransReply"), "'", "''") & "'"
				If Request("ltj_TerminalNumber") <> "" Then sWhere &= " And ltj_TerminalNumber='" & Replace(Request("ltj_TerminalNumber"), "'", "''") & "'"
			End If
			If Security.IsLimitedMerchant Then sWhere &= " AND tblCompany.ID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
			If Security.IsLimitedDebitCompany Then sWhere &= " AND tblDebitCompany.DebitCompany_ID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
			If sWhere.Length > 0 Then sWhere = " Where " & sWhere.Substring(5)
			If Request("Export") <> "" Then PGData.PageSize = 10000
	   		PGData.OpenDataset("Select tblLog_TerminalJump.*, tblCompany.CompanyName, tblDebitCompany.dc_Name " & _
	   			"From tblLog_TerminalJump " & _
	   			"Left Join tblDebitCompany ON(tblLog_TerminalJump.ltj_DebitCompanyID = tblDebitCompany.debitCompany_id) " & _
	   			"Left Join tblCompany ON(tblLog_TerminalJump.ltj_CompanyID = tblCompany.ID) " & _
	   			sWhere & " Order By ltj_GroupID Desc, ltj_ID Desc")
			If Request("Export") <> "" Then
				Response.ContentType = "application/vnd.ms-excel"
				Response.AddHeader("Content-Disposition", "attachment;filename=report.xls")
				Response.Write("<table><tr><th>ID</th><th>Group</th><th>Insert Date</th><th>Merchant</th><th>Debit</th><th>Terminal</th><th>Trans ID</th><th>Reply</th></tr>")
				While PGData.Read()
					Response.Write("<tr>")
					Response.Write("<td>" & PGData("ltj_ID") & "</td>")
					Response.Write("<td>" & PGData("ltj_GroupID") & "</td>")
					Response.Write("<td>" & PGData("ltj_InsertDate") & "</td>")
					Response.Write("<td>" & PGData("CompanyName") & "</td>")
					Response.Write("<td>" & PGData("dc_Name") & "</td>")
					Response.Write("<td>" & PGData("ltj_TerminalNumber") & "</td>")
					Response.Write("<td>" & PGData("ltj_TransID") & "</td>")
					Response.Write("<td>" & PGData("ltj_TransReply") & "</td>")
					Response.Write("</tr>")
				End While
				Response.Write("</table>")
				PGData.CloseDataset()
				Response.End()
			End If
			sQueryString = dbPages.CleanUrl(Request.QueryString)
		End Sub
	</script>
</head>
<body>
	<form id="Form2" runat="server">
		<table align="center" style="width:92%">
		<tr>
			<td id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> TERMINAL SWITCH LOG 
				<span style="color:Black;font-weight:normal;"><%=IIf(Request("From") = "Top", " - Last Actions", String.Empty)%></span>
				<br /><br />
			</td>
			<td style="font-size:11px;text-align:right;vertical-align:top;">
				&nbsp; &nbsp; <span style="background-color:<%= RGB_PASS %>;">&nbsp;&nbsp;</span> Pass
				&nbsp; &nbsp; <span style="background-color:<%= RGB_FAIL %>;">&nbsp;&nbsp;</span> Fail
			</td>
		</tr>
		<tr>
			<td style="font-size:11px; padding-bottom:4px;">
				<%
				Dim culCurrent As System.Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture
				Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
				Response.Write(Date.Now.ToString("dddd, dd/MM/yy HH:mm:ss"))
				%>
			</td>
			<td align="right" style="font-size:11px;">
				Filters:
				<%
				If Trim(Request("ltj_DebitCompanyID")) <> "" Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "ltj_DebitCompanyID", Nothing) & "';"">Debit Company</a>")
					sDivider = ", "
				End If
				If Trim(Request("ltj_CompanyID")) <> "" Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "ltj_CompanyID", Nothing) & "';"">Merchant ID</a>")
					sDivider = ", "
				End If
				If Trim(Request("ltj_TransReply")) <> "" Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "ltj_TransReply", Nothing) & "';"">Reply Code.</a>")
					sDivider = ", "
				End If
				If Trim(Request("ltj_TerminalNumber")) <> "" Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "ltj_TerminalNumber", Nothing) & "';"">Terminal Number.</a>")
					sDivider = ", "
				End If
				
				Response.Write(IIf(sDivider = String.Empty, "Add to filter by clicking on returned fields", " | (click to remove)"))
				%>
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%" cellpadding="1" cellspacing="0">
		<tr>
			<th style="width:30px;"><br /></th>
			<th>Group</th>
			<th>Trans ID</th>
			<th>Insert Date</th>
			<th>Merchant</th>
			<th>Debit</th>
			<th>Terminal</th>
			<th>Reply</th>
			<th>Log ID</th>
		</tr>
		<%
		While PGData.Read()
			Dim sTrBkgColor As String = IIF(PGData("ltj_TransReply") = "000", RGB_PASS, RGB_FAIL)
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';" height="16">
				<td style="text-align:left;"><span style="background-color:<%= sTrBkgColor %>;">&nbsp;</span></td>
				<td><%=PGData("ltj_GroupID")%></td>
				<td><%=PGData("ltj_TransID")%></td>
				<td><%=PGData("ltj_InsertDate")%></td>
				<td>
					<%
					If Trim(Request("ltj_TransReply")) <> "" Then
						Response.Write(PGData("CompanyName"))
					Else
						dbPages.showFilter(PGData("CompanyName"), "?" & sQueryString & "&ltj_CompanyID=" & PGData("ltj_CompanyID"), 1)
					End If
					%>
				</td>
				<td>
					<%
					If Trim(Request("ltj_DebitCompanyID")) <> "" Then
						Response.Write(PGData("dc_Name"))
					Else
						dbPages.showFilter(PGData("dc_Name"), "?" & sQueryString & "&ltj_DebitCompanyID=" & PGData("ltj_DebitCompanyID"), 1)
					End If
					%>
				</td>
				<td>
					<%
					If Trim(Request("ltj_TerminalNumber")) <> "" Then
						Response.Write(PGData("ltj_TerminalNumber"))
					Else
						dbPages.showFilter(PGData("ltj_TerminalNumber"), "?" & sQueryString & "&ltj_TerminalNumber=" & PGData("ltj_TerminalNumber"), 1)
					End If
					%>
				</td>
				<td>
					<%
					If Trim(Request("ltj_TransReply")) <> "" Then
						Response.Write(PGData("ltj_TransReply"))
					Else
						dbPages.showFilter(PGData("ltj_TransReply"), "?" & sQueryString & "&ltj_TransReply=" & PGData("ltj_TransReply"), 1)
					End If
					%>
				</td>
				<td><%=PGData("ltj_ID")%></td>
			</tr>
			<tr><td height="1" colspan="9"  bgcolor="#ffffff"></td></tr>
			<tr><td height="1" colspan="9"  bgcolor="silver"></td></tr>
			<tr><td height="1" colspan="9"  bgcolor="#ffffff"></td></tr>
			<%
		End While
		PGData.CloseDataset()%>
		</table>
	</form>
	<br />
	<table align="center" style="width:92%">
	 <tr>
	  <td><UC:Paging runat="Server" id="PGData" PageID="PageID"  /></td>
	 </tr>
	</table>
</body>
</html>