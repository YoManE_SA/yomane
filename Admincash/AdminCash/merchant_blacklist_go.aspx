<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
	Sub Page_Load()
		Security.CheckPermission("Inactive Merchants - Delete")
		Dim sSQL As String = String.Empty
		If dbPages.TestVar(Request("DeleteRecord"), 1, 0, 0) > 0 Then
			sSQL = "DELETE FROM Risk.BlacklistMerchant WHERE BlacklistMerchant_id=" & Request("DeleteRecord")
		End If
		If sSQL <> String.Empty Then dbPages.ExecSql(sSQL)
	End Sub
</script>
<script language="javascript" type="text/javascript">
	parent.location.reload();
</script>
