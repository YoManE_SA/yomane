<%response.buffer = true%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/func_balance.asp"-->

<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
nCompanyID = TestNumVar(Request("companyID"), 0, -1, 0)

Function ShowStatus(fIsAllow)
	If CByte(fIsAllow) Then
		sTxt = "YES"
		sBkgColor = "66cc66"
	Else
		sTxt = "NO"
		sBkgColor = "ff6666"
	End if
	Response.Write("<Span style=""width:26px; border:1px solid #" & sBkgColor & "; font-size:8px; margin:2px; padding:1px; text-align:center; background-color:#ffffff;"">" & sTxt& "</span>")
End function

If Trim(Request("Action")) = "DelTestOnly" Then
	sSQL = "DELETE FROM [Data].[Cart] WHERE [Data].[Cart].[TransPass_id] IN (SELECT ID FROM tblCompanyTransPass WITH (NOLOCK) WHERE companyID=" & nCompanyID & " AND isTestOnly=1 AND UnsettledInstallments>0)"
	oledbData.Execute sSQL
	sSQL = "DELETE FROM tblRefundAsk WHERE TransID IN (SELECT ID FROM tblCompanyTransPass WITH (NOLOCK) WHERE companyID=" & nCompanyID & " AND isTestOnly=1 AND UnsettledInstallments>0)"
	oledbData.Execute sSQL
	sSQL = "DELETE FROM tblCompanyTransPass WHERE ID IN (SELECT ID FROM tblCompanyTransPass WITH (NOLOCK) WHERE companyID=" & nCompanyID & " AND isTestOnly=1 AND UnsettledInstallments>0)"
	oledbData.Execute sSQL
End if
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css"/>
	<style type="text/css">
		input.inputData { width:90%; }
		legend { color:gray; font-size:11px; padding:6px; }
		.tblNote {
			border-left:1px solid #a1a1a1;
			border-bottom:1px solid #a1a1a1;
			background-color:#eeeeee;
			filter:progid:DXImageTransform.Microsoft.Shadow(color='#b0b0b0', Direction=225, Strength=	);
			FILTER: progid:DXImageTransform.Microsoft.Alpha(style=1,opacity=100,finishOpacity=40,startX=0,finishX=0,startY=40,finishY=100);
		}
	</style>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<%
sSQL="SELECT tblCompany.*, tblParentCompany.pc_Code, IsNull(mg_Name, 'none') GroupName, " & _
"" &_
"(SELECT COUNT(tblTransactionPay.id) FROM tblTransactionPay " &_
"WHERE tblTransactionPay.CompanyID = tblCompany.ID) AS numTimesPay, " &_
"" &_
"(SELECT MAX(tblTransactionPay.PayDate) FROM tblTransactionPay " &_
"WHERE tblTransactionPay.CompanyID = tblCompany.ID) AS lastPaid, " &_
"" &_
"(SELECT COUNT(tblCompanyTransPass.id) FROM tblCompanyTransPass " &_
"WHERE (tblCompanyTransPass.CompanyID=tblCompany.ID) AND (tblCompanyTransPass.PayID=';X;')) AS numTransArchive " &_
"" &_
"FROM tblCompany LEFT JOIN tblParentCompany ON tblCompany.ParentCompany=tblParentCompany.ID " & _
"LEFT JOIN tblMerchantGroup ON tblCompany.GroupID=tblMerchantGroup.ID " &_
"WHERE tblCompany.ID=" & nCompanyID
set rsData = oledbData.Execute(sSQL)
if not rsData.EOF then
	%>
	<table align="center" border="0" cellpadding="0" cellspacing="2" dir="ltr" width="95%">
	<tr>
		<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" dir="ltr" width="100%">
			<!--#include file="Merchant_dataHeaderInc.asp"-->
			<%
			pageSecurityLevel = 0
			pageSecurityLang = "ENG"
			pageClosingTags = "</table></td></tr></table>"
			Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
			%>
			<tr><td height="20"></td></tr>
			<!--#include file="Merchant_dataMenuInc.asp"-->
			</table>
		</td>
	</tr>
	<tr><td><br/></td></tr>
	</table>
	<table align="center" border="0" cellpadding="0" cellspacing="2" width="95%">
	<tr><td height="10"></td></tr>
	<tr>
		<td width="25%" valign="top">
			<table border="0" cellspacing="0" cellpadding="1">
			<%
			sSQL = "SELECT COUNT(id) AS numTestTrans FROM tblCompanyTransPass WHERE companyID=" & nCompanyID & " AND isTestOnly=1 And UnsettledInstallments <> 0"
			numTestTrans = ExecScalar(sSQL, 0)
			If numTestTrans > 0 Then
				%>
				<tr>
					<td>
						<table cellpadding="4" cellspacing="0" align="left" style="border:1px solid silver;">
						<tr>
							<td><img src="/NPCommon/images/alert_medium.png" alt="" border="0"><br /></td>
							<td>
								<span class="txt13" style="color:#bc3a4f;"><%=numTestTrans%> Test transactions made</span><br />
								<a href="Trans_admin_regularData.aspx?onlyTestTrans=1&ShowCompanyID=<%=nCompanyID%>" class="largeB">View & Delete</a> &nbsp;|&nbsp;
								<a href="?Action=DelTestOnly&companyID=<%=nCompanyID%>" class="largeB">Delete All</a><br />
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr><td><br /></td></tr>
				<%
			End if
			%>
			<tr>
				<td class="txt11b">
					<span style="background-color:<%=MStatusColor(nStatus)%>;">&nbsp;&nbsp;&nbsp;</span> <%=UCase(MerchantStatus(nStatus))%><br />
				</td>
			</tr>
			<tr><td height="6"></td></tr>
			<tr>
				<td class="txt11">  
					<%
					Response.Write("<span class=""txt14"">" & dbtextShow(rsData("CompanyName")) & "<span><br />")
					response.write("<span style=""color:#006699;"">Parent company:</span> " & Trim(rsData("pc_Code")) & "<br />")
					response.write "<span style=""color:#006699;"">Merchant group:</span> " & Trim(rsData("GroupName")) & "<br />"
					response.write("<span style=""color:#006699;"">Merchant number:</span> " & Trim(rsData("CustomerNumber")) & "<br />")
					GetConstArray Server.CreateObject("ADODB.Recordset"), 51, 1, "", saIndustry, saColor
					response.write("<span style=""color:#006699;"">Industry:</span> " & saIndustry(rsData("CompanyIndustry_id")) & "<br />")
					response.write("<span style=""color:#006699;"">Account manager:</span> " & Trim(rsData("careOfAdminUser")) & "<br />")
					response.write("<span style=""color:#006699;"">Signup:</span> " & FormatDatesTimes( rsData("InsertDate"),1,0,0) & "<br />")

					If trim(rsData("url"))<>"" Then
						response.write("<span style=""color:#006699;"">Website:</span> ")
						UrlArray = Split(trim(rsData("url")),chr(13) & chr(10))
						For i = Lbound(UrlArray) to Ubound(UrlArray)
							sUrlLink = Trim(UrlArray(i))
							If sUrlLink <> "" Then
								If InStr(sUrlLink,"://")=0 Then sUrlLink = "http://" & sUrlLink
								response.write("<a href="""& sUrlLink & """ target=""_blank"" class=""go"">" & dbTextShow(sUrlLink) & "</a><br />")
							End if
						Next
					End if
					'If trim(rsData("comment"))<>"" then response.write "<span style=""color:#006699;"">Industry:</span> " & dbTextShow(rsData("comment")) & "<br />"
					%>
				</td>
			</tr>
			</table>
			<br /><br />
			<%
			If len(trim(rsData("payingDates1")))=6 Then
				sfromDay1 = mid(trim(rsData("payingDates1")),1,2)
				stoDay1 = mid(trim(rsData("payingDates1")),3,2)
				spayday1 = mid(trim(rsData("payingDates1")),5,2)
			Else
				sfromDay1=00 : stoDay1=00 : spayday1=00
			End if
			If len(trim(rsData("payingDates2")))=6 Then
				sfromDay2 = mid(trim(rsData("payingDates2")),1,2)
				stoDay2 = mid(trim(rsData("payingDates2")),3,2)
				spayday2 = mid(trim(rsData("payingDates2")),5,2)
			Else
				sfromDay2=00 : stoDay2=00 : spayday2=00
			End if
			If len(trim(rsData("payingDates3")))=6 Then
				sfromDay3 = mid(trim(rsData("payingDates3")),1,2)
				stoDay3 = mid(trim(rsData("payingDates3")),3,2)
				spayday3 = mid(trim(rsData("payingDates3")),5,2)
			Else
				sfromDay3=00 : stoDay3=00 : spayday3=00
			End if
			%>
			<table class="formThin" border="0" cellspacing="0" cellpadding="1">
			<tr><td class="MerchantSubHead" colspan="3">PAYOUTS<br /><br /></td></tr>
			<tr>
				<td colspan="2" nowrap="nowrap">
					<span style="color:#006699;">Initial holding margin:</span> <%= rsData("PayingDaysMarginInitial") %> Days<br />
					<span style="color:#006699;">Holding margin:</span> <%= rsData("PayingDaysMargin") %> Days<br />
					<span style="color:#006699;">Transactions Period:</span><br />
					Between <%= sfromDay1 %> and <%= stoDay1 %> Pay on <%= spayday1 %><br />
					Between <%= sfromDay2 %> and <%= stoDay2 %> Pay on <%= spayday2 %><br />
					Between <%= sfromDay3 %> and <%= stoDay3 %> Pay on <%= spayday3 %><br />
					<br />
					<div style="width:250px;white-space:normal;">
						<%
							sSQL = "SELECT CUR_IsoName FROM tblSystemCurrencies INNER JOIN Setting.SetMerchantSettlement ON (CUR_ID=Currency_ID) WHERE Merchant_ID=" & nCompanyID & " And IsShowToSettle<>1 ORDER BY CUR_ID"
							Dim rsHiddenCurrencies : Set rsHiddenCurrencies = oleDbData.Execute(sSQL)
							If rsHiddenCurrencies.EOF Then
								Response.Write "All currencies are shown in payout list."
							Else
								Response.Write "Currencies hidden from payout list: "
								Do Until rsHiddenCurrencies.EOF
									Response.Write rsHiddenCurrencies(0)
									rsHiddenCurrencies.MoveNext
									Response.Write IIf(rsHiddenCurrencies.EOF, ".", ", ")
								Loop
							End If
							rsHiddenCurrencies.Close
							Set rsHiddenCurrencies = Nothing
						%>
					</div>
				</td>
			</tr>
			</table>
			<br /><br />
			<table class="formThin" border="0" cellspacing="0" cellpadding="1">
			<tr><td class="MerchantSubHead" colspan="3">LINKS<br /><br /></td></tr>
			<tr>
				<td class="txt10">
					<li type="circle" style="padding-bottom:2px;">
						<a href="outer_login.aspx?CompanyID=<%=nCompanyID%>" target="_blank" class="go">MERCHANT CONTROL PANEL</a>
						<a href="outer_login.aspx?target=test&CompanyID=<%=nCompanyID%>" target="_blank" class="go">(TEST)</a>
					</li>
					<br />
					<li type="circle" style="padding-bottom:2px;">
						<a href="outer_login.aspx?target=devcenter&CompanyID=<%=nCompanyID%>" target="_blank" class="go">MERCHANT DEVELOPER CENTER</a>
						<a href="outer_login.aspx?target=devcentertest&CompanyID=<%=nCompanyID%>" target="_blank" class="go">(TEST)</a>
					</li>
					<br />
					<li type="circle" style="padding-bottom:2px;"><a class="go" href="#" onclick="top.fraBrowser.frmBody.location.href='merchant_transPassFilter.asp?companyID=<%=nCompanyID%>&CompanyStatus=<%=rsData("ActiveStatus")%>&numTransToPay=0&ShowName=<%=rsData("CompanyName")%>'; parent.fraButton.FrmResize(); return false;">Unsettled Balance</a></li><br />
					<li type="circle" style="padding-bottom:2px;"><a class="go" href="#" onclick="top.fraBrowser.frmBody.location.href='Merchant_BalanceData.aspx?ShowCompanyID=<%=nCompanyID%>&isAllCompanys=0&PageSize=25'; return false;">Account Balance</a>
					<%
					sTmp=""
					For Each t In fn_GetBalances(nCompanyID).Items
                        'nValue = TestNumVar(ExecScalar("SELECT BalanceExpected FROM tblCompanyBalance WHERE currency=" & t & " AND company_id=" & nCompanyID & " AND insertDate=(SELECT Max(insertDate) FROM tblCompanyBalance WHERE currency=" & t & " AND company_id=" & nCompanyID & ")", 0), 0, -1, 0)
						sTmp=sTmp & ", <a class=""go"" href=""#"" onclick=""top.fraBrowser.frmBody.location.href='Merchant_BalanceData.aspx?ShowCompanyID=" & nCompanyID & "&isAllCompanys=0&PageSize=25&Currency=" & t.CurrencyId & "';return false;"">" & FormatCurr(t.CurrencyId, t.Expected) & "</a>"
					Next
					if sTmp<>"" then response.Write("<br />&nbsp;&nbsp;&nbsp;&nbsp;" & trim(mid(sTmp, 2)))
                    %>
					</li><br />
                    <%
					Response.Write("<li type=""circle"" style=""padding-bottom:2px;"">")
					If int(rsData("numTransArchive"))>0 Then
						%>
						<a class="go" target="fraBrowser" href="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=<%=Server.UrlEncode("Trans_admin_regularFilter.aspx?FilterMerchantDefaultID=" & nCompanyID & "&Payout=A" )%>&pageBodyUrl=<%=Server.UrlEncode("Trans_admin_regularData.aspx?ShowCompanyID=" & nCompanyID & "&Payout=A&chk2=1&fromDate=" & Server.UrlEncode(DateAdd("m", -6, Now)) )%>">
						Archive</a> (<%= int(rsData("numTransArchive")) %>)
						<%
					Else
						Response.Write("<span class=""txt11"" style=""color:#a1a1a1;"">Archive (0)</span>")
					End If
					Response.Write("</li><br /><li type=""circle"" style=""padding-bottom:2px;"">")
					If int(rsData("numTimesPay"))>0 Then
						%>
						<a class="go" href="javascript:top.fraBrowser.frmBody.location.href='Merchant_transSettledListData.asp?Source=filter&ShowCompanyID=<%=nCompanyID%>'; undefined;">Settlement Reports</a></li><br />
						&nbsp;&nbsp;&nbsp;&nbsp;Count: <%= int(rsData("numTimesPay")) %>, Last: <%=Left(rsData("lastPaid"), 10)%>
						<%
					Else
						Response.Write("<span class=""txt11"" style=""color:#a1a1a1;"">Settlement Reports (0)</span></li><br />")
					End If
					response.write "<li type=""circle"" style=""padding-bottom:2px;""><a href=""charge_browser.asp?companyID="& nCompanyID &""" target=""fraBrowser"" class=""go"">Virtual terminal</a></li><br />"
					response.write "<li type=""circle"" style=""padding-bottom:2px;""><a href=""javascript:top.fraBrowser.frmBody.location.href='Report_Merchant.aspx?companyID=" & nCompanyID & "'; undefined;"" class=""go"">Monthly Report</a></li><br />"
					response.write "<li type=""circle"" style=""padding-bottom:2px;""><a href=""MerchantChbHistory.aspx?ID=" & nCompanyID & """ target=""frmBody"" class=""go"">Chargeback History</a></li><br />"
					%>
				</td>
			</tr>
			</table>
		</td>
		<td width="1" bgcolor="#484848"></td>
		<td width="35%" valign="top">
			<table class="formThin" align="center" dir="ltr" width="96%">
			<tr><td class="MerchantSubHead" colspan="3">ACTIVE TERMINALS<br /><br /></td></tr>
			<%
			Set fsServer=Server.CreateObject("Scripting.FileSystemObject")
			Dim sSQL
			If ExecScalar("Select IsUsingNewTerminal From tblCompany Where ID=" & nCompanyID, 0) = True Then 
				sSQL = "Select tblDebitCompany.debitCompany_id, tblDebitCompany.dc_Name, CPM_PaymentMethod As PaymentMethod, tblCompanyPaymentMethods.CPM_CurrencyID As CurrencyID, pm.name AS pm_Name, tblDebitTerminals.dt_name, tblDebitTerminals.isNetpayTerminal " & _
					" From tblCompanyCreditRestrictions " & _
					" Left Join tblCompanyPaymentMethods ON (tblCompanyCreditRestrictions.CCR_CPM_ID = tblCompanyPaymentMethods.CPM_ID)" & _
					" Left Join List.PaymentMethod AS pm ON CPM_PaymentMethod= pm.PaymentMethodGroup_id" & _
					" Left Join dbo.tblDebitTerminals ON(tblCompanyCreditRestrictions.CCR_TerminalNumber = tblDebitTerminals.terminalNumber)" & _
					" Left Join dbo.tblDebitCompany ON(tblDebitTerminals.DebitCompany = tblDebitCompany.debitCompany_id)" & _
					" Where CPM_CompanyID=" & nCompanyID & " And CPM_IsDisabled=0 AND tblCompanyCreditRestrictions.CCR_TerminalNumber<>'' AND tblCompanyCreditRestrictions.CCR_Ratio>0"
			Else
				sSQL = "Select tblDebitCompany.debitCompany_id, tblDebitCompany.dc_Name, CCF_PaymentMethod As PaymentMethod, tblCompanyCreditFees.CCF_CurrencyID As CurrencyID, pm.name AS pm_Name, tblDebitTerminals.dt_name, tblDebitTerminals.isNetpayTerminal " &_
					" FROM tblCompanyCreditFees " &_
					" Left Join List.PaymentMethod AS pm ON CCF_PaymentMethod= pm.PaymentMethod_id" & _
					" Left Join tblCompanyCreditFeesTerminals ON(tblCompanyCreditFeesTerminals.CCFT_CCF_ID = tblCompanyCreditFees.CCF_ID) " &_
					" Left Join dbo.tblDebitTerminals ON(tblCompanyCreditFeesTerminals.CCFT_Terminal = tblDebitTerminals.terminalNumber) " &_
					" Left Join dbo.tblDebitCompany ON(tblDebitTerminals.DebitCompany = tblDebitCompany.debitCompany_id) " &_
					" Where CCF_CompanyID=" & nCompanyID & " AND CCF_IsDisabled=0 AND tblCompanyCreditFeesTerminals.CCFT_Terminal<>'' AND tblCompanyCreditFeesTerminals.CCFT_Ratio>0 ORDER BY CCF_ID Asc"
			End If
			Set rsDataTmp = oledbData.Execute(sSQL)
			If Not rsDataTmp.EOF Then
				%>
				<tr>
					<th style="font-size:10px; font-weight:normal; color:#000000;">Payment Method</th>
					<th style="font-size:10px; font-weight:normal; color:#000000;">Debit Company</th>
					<th style="font-size:10px; font-weight:normal; color:#000000;">Terminal Name</th>
				</tr>
				<%
				Do until rsDataTmp.EOF
					%>
					<tr>
						<td style="font-size:10px;" nowrap><img align="middle" src="/NPCommon/ImgPaymentMethod/23X12/<%=rsDataTmp("PaymentMethod")%>.gif"> <%=rsDataTmp("pm_Name")%> (<%=GetCurText(rsDataTmp("CurrencyID"))%>)<br /></td>
						<td style="font-size:10px;" nowrap>
							<%
							If trim(rsDataTmp("debitCompany_id"))<>"" Then
								If fsServer.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & trim(rsDataTmp("debitCompany_id")) & ".gif")) Then
									Response.write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(rsDataTmp("debitCompany_id")) & ".gif"" align=""middle"" border=""0"" /> &nbsp;")
								End if
							End if
							Response.write(dbTextShow(rsDataTmp("dc_name")) & "<br/>")
							%>
						</td>
						<td style="font-size:10px;">
							<%
							If int(len(dbtextShow(rsDataTmp("dt_name"))))<16 Then
								response.write dbTextShow(rsDataTmp("dt_name"))
							Else
								response.write "<span title="""& dbtextShow(rsDataTmp("dt_name")) & """>" & left(dbtextShow(rsDataTmp("dt_name")),15) & "..</span>"
							End If
							If NOT rsDataTmp("isNetpayTerminal") Then response.write(" (GW)")
							%><br />
						</td>
					</tr>
					<%
				rsDataTmp.movenext
				Loop
			Else
				Response.Write("<tr><td colspan=""3"" nowrap>None configured!</td></tr>")
			End if
			rsDataTmp.close
			Set rsDataTmp = nothing
			Set fsServer=Nothing
			%>
			</table>
			<br /><br />
			<table class="formThin" align="center" dir="ltr" width="96%">
			<tr><td class="MerchantSubHead" colspan="3">PROCESSING METHODS<br /><br /></td></tr>
			<tr>
				<td>
					<%ShowStatus(rsData("IsSystemPay"))%> <span style="vertical-align:middle;">Virtual terminal - Credit card</span><br/>
					<%ShowStatus(rsData("IsSystemPayEcheck"))%> <span style="vertical-align:middle;">Virtual terminal - E-Check</span><br/>
					<%ShowStatus(rsData("IsRemoteCharge"))%> <span style="vertical-align:middle;">Silent post transactions - Credit card</span><br />
					<%ShowStatus(rsData("IsRemoteChargeEcheck"))%> <span style="vertical-align:middle;">Silent post transactions - E-Check</span><br />
					<%ShowStatus(rsData("IsPublicPay"))%> <span style="vertical-align:middle;">Public payment page</span><br />
				</td>
			</tr>
			</table>
		</td>
		<td width="1" bgcolor="#484848"></td>
		<td valign="top">
			<table align="center" dir="ltr" width="96%">
			<tr><td class="MerchantSubHead" colspan="3">NOTES<br /><br /></td></tr>
			<%
			sSQL="SELECT TOP 3 InsertDate, InsertUser, NoteText FROM Data.AccountNote WHERE Account_ID IN (Select Account_ID From Data.Account Where Merchant_id=" & nCompanyID & ") ORDER BY InsertDate DESC"
			Set rsDataTmp = oledbData.Execute(sSQL)
			nNoteCount=0
			Do until rsDataTmp.EOF OR nNoteCount = 4
				nNoteCount = nNoteCount+1
				%>
				<tr>
					<td>
						<table align="center" width="100%" cellspacing="0" cellpadding="0" class="tblNote">
						<tr>
							<td style="color:#484848; padding:6px;">
								<span class="txt11b"><%= FormatDatesTimes(rsDataTmp("InsertDate"),1,1,0) %></span>
								<%If Trim(rsDataTmp("InsertUser"))<>"" Then Response.Write("&nbsp; (" & rsDataTmp("InsertUser") & ")") %><br />
								<%= Replace(rsDataTmp("NoteText"), chr(13), "<br />") %><br /><br />
							</td>
							<td style="text-align:right;" valign="top"><img src="../images/pageCurl_gray.gif"><br /></td>
						</tr>
						</table>
					</td>
				</tr>
				<tr><td height="6"></td></tr>
				<%
				rsDataTmp.MoveNext
			Loop
			rsDataTmp.close
			%>
			<tr>
				<td>
					<a href="merchant_dataNotes.aspx?CompanyID=<%= nCompanyID %>" style="font-weight:bold;">See All / Add New ...</a><br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<%
end if
%>
</body>
<%
rsData.close
Set rsData = nothing
call closeConnection()
%>
</html>