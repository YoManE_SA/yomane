<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = Date.Now.ToShortDateString
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
	
        Security.CheckPermission(lblPermissions)
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
	<form runat="server" action="Log_TerminalJump_Data.aspx" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager2" AjaxFrameworkMode="Enabled" runat="server" />
	<input type="hidden" id="iMerchantID" name="ltj_CompanyID" />

	<h1><asp:label ID="lblPermissions" runat="server" /> Terminal Switch Log<span> - Filter</span></h1>
	<table class="filterNormal">
		<tr><td><Uc1:FilterMerchants id="FMerchants" ClientField="iMerchantID" Width="150px" runat="server" /></td></tr>
		<tr><td height="12"></td></tr>
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td>
				<select name="iPageSize">
					<option value="20" selected="selected">25 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td align="right" style="text-align:right;"><input type="submit" name="Action" value="  SEARCH  " /><br /></td>
		</tr>
		<tr>
			<td align="right" style="text-align:right;" colspan="2"><input type="submit" name="Export" value="SAVE XLS " /><br /></td>
		</tr>
	</table>
	<hr class="filter" />
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<td>Transaction ID</td>
			<td align="center"><input class="medium grayBG" size="5" name="transID" /></td>
			<td align="right"><input type="submit" class="buttonFilter" value="Show" /></td>
		</tr>
	</table>
	</form>
</asp:Content>