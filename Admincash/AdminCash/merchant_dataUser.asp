<%
response.buffer = true
nCompanyID = TestNumVar(Request("companyID"), 0, -1, 0)
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/func_controls.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
bDuplicate=false
If trim(request("Action"))="update" Then
		If trim(request("Mail"))<>"" or trim(request("UserName"))<>"" Then
			sSQL="IF EXISTS(SELECT ID FROM tblCompany WHERE Mail='" & DBText(request("Mail")) & "' AND" & _
			" UserName='" & DBText(request("UserName")) & "' AND ID<>" & nCompanyID & ")" & _
			" SELECT CAST(1 as bit) ELSE SELECT CAST(0 as bit)"
			set rsData=oledbData.Execute(sSQL)
			bDuplicate=rsData(0)
			If bDuplicate Then
				%>
				<script language="javascript" type="text/javascript">
					alert("The username and the email were not updated, since it would cause duplication!");
				</script>
				<%
			End if
		End if

		sCard = DBText(request("CCard_num1") & request("CCard_num2") & request("CCard_num3") & request("CCard_num4"))
		sCard = trim(replace(sCard," ",""))

		sSQL="UPDATE tblCompany SET " &_
		" FirstName=Left('" & DBText(request("FirstName")) & "', 150)," &_
		" LastName=Left('" & DBText(request("LastName")) & "', 150)," &_
		" Street=Left('" & DBText(request("Street")) & "', 100)," &_
		" City=Left('" & DBText(request("City")) & "', 50)," &_
		" ZIP=Left('" & DBText(request("ZIP")) & "', 25)," &_
		" Country=CASE WHEN " & DBText(request("CountryID")) & "=0 THEN NULL ELSE " & DBText(request("CountryID")) & " END," &_
		" Phone=Left('" & DBText(request("Phone")) & "', 50)," &_
		" Cellular=Left('" & DBText(request("Cellular")) & "', 50)," &_
		" IDNumber=Left('" & DBText(request("IDNumber")) & "', 15)," &_
		" CCardNumber256=dbo.GetEncrypted256(dbo.fnFormatCcNumToGroups('" & sCard & "'))," &_
		" CCardExpMM=Left('" & dbText(request("CCardExpMM")) & "', 50)," &_
		" CCardExpYY=Left('" & dbText(request("CCardExpYY")) & "', 50)," &_
		" CCardHolderName=Left('" & DBText(request("CCardHolderName")) & "', 100)," &_
		" CCardCUI=dbo.SetCUI(Left('" & request("CardCUI") & "', 7))"
		If not bDuplicate Then
			sSQL=sSQL & ", Mail=Left('" & DBText(request("Mail")) & "', 200)," &_
			" UserName=Left('" & DBText(request("UserName")) & "', 50) "
		End if
		sSQL = sSQL & " WHERE ID=" & nCompanyID
		oledbData.Execute sSQL
		
		
		Dim accountId, loginAccountId ,acReader
		Set acReader = oledbData.Execute("Select Account_id, LoginAccount_id From Data.Account Where Merchant_Id=" & nCompanyID)
		If Not acReader.EOF Then 
			accountId = TestNumVar(acReader("Account_id"), 0, -1, 0)
			loginAccountId = TestNumVar(acReader("LoginAccount_id"), 0, -1, 0)
		End If
		acReader.Close()
		if (loginAccountId = 0) Then
			loginAccountId = ExecScalar("SET NOCOUNT ON;Insert Into Data.LoginAccount(LoginRole_id, IsActive) Values(20, 1); SELECT SCOPE_IDENTITY();SET NOCOUNT OFF;", 0)
			oledbData.Execute "Update Data.Account Set LoginAccount_id=" & loginAccountId & " Where Merchant_Id=" & nCompanyID
		End If
		sSQL = "Update Data.LoginAccount Set " & _
			"  LoginEmail = Left('" & DBText(request("Mail")) & "', 200)" &_
			", LoginUser = Left('" & DBText(request("UserName")) & "', 50) " & _
			" Where LoginAccount_id=" & loginAccountId & " Or LoginAccount_id IN(Select LoginAccount_id From Data.AccountSubUser Where Account_id=" & accountId & ")"
		oledbData.Execute sSQL

End if

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css"/>
	<style type="text/css">
		input.inputData { width:90%; }
	</style>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<%
sSQL="SELECT dbo.GetCUI(tblCompany.CCardCUI) CardCUI, dbo.GetDecrypted256(tblCompany.CCardNumber256) CCardNumber, tblCompany.*, l.* " & _
" FROM tblCompany" & _
" LEFT JOIN Data.Account a ON (tblCompany.ID = a.Merchant_id)" & _
" LEFT JOIN Data.LoginAccount l ON (a.LoginAccount_id = l.LoginAccount_id)" & _
" WHERE tblCompany.ID=" & request("companyID")
set rsData = oledbData.Execute(sSQL)
if not rsData.EOF then
	sLoginURL = session("TempOpenURL")
	If trim(COMPANY_NAME_1) = "Netpay" then
		sLoginURL = sLoginURL & "site_heb/"
	Elseif trim(COMPANY_NAME_1) = "AsTech" then
		sLoginURL = sLoginURL & "site_eng/"
	End if
	%>
	
	<table align="center" border="0" cellpadding="0" cellspacing="2" dir="ltr" width="95%">
	<tr>
		<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" dir="ltr" width="100%">
			<!--#include file="Merchant_dataHeaderInc.asp"-->
			<%
			pageSecurityLevel = 0
			pageSecurityLang = "ENG"
			pageClosingTags = "</table></td></tr></table>"
			Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
			%>
			<tr><td height="20"></td></tr>
			<!--#include file="Merchant_dataMenuInc.asp"-->
			</table>
		</td>
	</tr>
	<tr><td><br/></td></tr>
	</table>
	<form action="merchant_dataUser.asp" name="frmCoopDetail" method="post">
	<input type="Hidden" name="companyID" value="<%= request("companyID") %>">
	<input type="hidden" name="Action" value="update">
	<table align="center" border="0" cellpadding="0" cellspacing="2" dir="rtl" width="95%">
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<table align="center" border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
			<tr>
				<td colspan="4" class="MerchantSubHead">
					Personal Details<br/>
				</td>
			</tr>
			<tr>
				<td class="txt11" width="20%">First name<br /></td>
				<td class="txt11" width="30%">
					<input type="text" class="inputData" dir="ltr" name="FirstName" value="<%= rsData("FirstName") %>">
				</td>
				<td class="txt11" width="20%">Last name<br /></td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="LastName" value="<%= rsData("LastName") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Street address<br /></td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="Street" value="<%= rsData("Street") %>">
				</td>
				<td class="txt11">City<br /></td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="City" value="<%= rsData("City") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Country<br /></td>
				<td class="txt11">
					<%
	                sSQL="SELECT CountryID, Name FROM [List].[CountryList] ORDER BY Name"
	                set rsData4 = oledbData.execute(sSQL)
					if not rsData4.EOF then
						PutRecordsetCombodefaultNEW rsData4, "CountryID"" dir=""ltr"" class=""inputData"" style=""width:150px;", "CountryID", "Name", "0", "", trim(rsData("Country"))
						rsData4.movefirst
					end if
                    rsData4.close
					%>
				</td>
				<td class="txt11">Zip<br /></td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="ZIP" value="<%= rsData("ZIP") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Phone<br /></td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="Phone" value="<%= rsData("Phone") %>">
				</td>
				<td class="txt11">Mobile<br /></td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="cellular" value="<%= rsData("cellular") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">ID<br /></td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="IDNumber" value="<%= rsData("IDNumber") %>">
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr><td height="10"><br /></td></tr>
			<tr>
				<td colspan="4" class="MerchantSubHead">
					Credit Card Details<br/>
				</td>
			</tr>
			<tr>
				<td class="txt11">Card number<br /></td>
				<%
				sCCardExpMM = trim(rsData("CCardExpMM"))
				sCCardExpYY = trim(rsData("CCardExpYY"))
				sCCard = trim(replace("" & rsData("CCardNumber")," ",""))
				sCardNumShow1 = mid(sCCard,1,4)
				sCardNumShow2 = mid(sCCard,5,4)
				sCardNumShow3 = mid(sCCard,9,4)
				sCardNumShow4 = mid(sCCard,13,4)
				%>
				<td class="txt11" dir="ltr">
					<input type="Text" name="CCard_num1" class="inputData" style="width:35px;" dir="ltr" maxlength="4" tabindex="1" value="<%= sCardNumShow1 %>" onkeyup="if(this.value.length==4){frmCoopDetail.CCard_num2.focus();}">
					<input type="Text" name="CCard_num2" class="inputData" style="width:35px;" dir="ltr" maxlength="4" tabindex="2" value="<%= sCardNumShow2 %>" onkeyup="if(this.value.length==4){frmCoopDetail.CCard_num3.focus();}">
					<input type="Text" name="CCard_num3" class="inputData" style="width:35px;" dir="ltr" maxlength="4" tabindex="3" value="<%= sCardNumShow3 %>" onkeyup="if(this.value.length==4){frmCoopDetail.CCard_num4.focus();}">
					<input type="Text" name="CCard_num4" class="inputData" style="width:35px;" dir="ltr" maxlength="4" tabindex="4" value="<%= sCardNumShow4 %>">
				</td>
				<td class="txt11">Card validity<br /></td>
				<td class="txt11">
					<%
					sCharExpYY = "@"
					sListExpYY = ""
					If trim(sExpYY)<>"" Then
						If sExpYY<year(date()) Then sListExpYY = sListExpYY & sExpYY & sCharExpYY
					End if
					For i = year(date()) To year(date())+10
						sListExpYY = sListExpYY & i & sCharExpYY
					Next
					response.write PutComboDefault("CCardExpMM"" class=""inputData", "01@02@03@04@05@06@07@08@09@10@11@12@", "@", sCCardExpMM)
					response.write "/"
					response.write PutComboDefault("CCardExpYY"" class=""inputData", sListExpYY, sCharExpYY, sCCardExpYY)
					%>
				</td>
			</tr>
			<tr>
				<td class="txt11">Card holder name<br /></td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="CCardHolderName" value="<%= rsData("CCardHolderName") %>">
				</td>
				<td class="txt11">CVV2/CVC2/CID<br /></td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="CardCUI" value="<%= rsData("CardCUI") %>">
				</td>
			</tr>
			<tr><td height="10"><br /></td></tr>
			<tr>
				<td colspan="2" class="MerchantSubHead">Login Details<br /></td>
			</tr>
			<%
			if not bDuplicate then
				isExist = TestNumVar(ExecScalar("SELECT LoginAccount_ID FROM Data.LoginAccount WHERE LoginEmail='" & rsData("LoginEmail") & "' AND LoginUser='" & rsData("LoginUser") & "' AND LoginRole_id=" & 20 & " AND LoginAccount_id <> " & TestNumVar(rsData("LoginAccount_id"), 0, -1, 0) & "", 0), 0, -1, 0)
				if isExist > 0 then
					sDuplicateStyle="font-weight:bold;background-color:red;color:white;"
					sDuplicateText="<tr><td colspan=""2"" style=""" & sDuplicateStyle & """>* There is another merchant with the same username and email address!</td><td colspan=""2""></td></tr>"
				else
					sDuplicateStyle=""
					sDuplicateText=""
				end if
			end if
			%>
			<tr>
				<td class="txt11" style="<%= sDuplicateStyle %>">Email</td>
				<td class="txt11" style="<%= sDuplicateStyle %>">
					<input type="text" class="inputData" name="Mail" value="<%= rsData("LoginEmail") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11" style="<%= sDuplicateStyle %>">Username</td>
				<td class="txt11" style="<%= sDuplicateStyle %>">
					<input type="text" class="inputData" name="UserName" value="<%= rsData("LoginUser") %>">
				</td>
			</tr>
			<%= sDuplicateText %>
			<tr>
				<td class="txt11">Password</td>
				<td class="txt11">
					<a href="ChangePassword.aspx?RefType=2&RefID=<%= request("companyID") %>">Add / Change password</a>
					&nbsp;|&nbsp;
					<a href="MiscActions.aspx?action=resetPasswordLock&p1=<%=rsData("LoginAccount_id")%>&Ref=<%= Server.UrlEncode("merchant_dataUser.asp?companyID=" & request("companyID") & "&PassReset=2") %>" class="alwaysactive">Reset counter</a>
					<%
					If Request("PassReset") = "2" Then Response.Write " Counter reset succeeded. "
                    If trim(request("Action")) = "showPassword" Then
                        response.Write("&nbsp;|<br/>Password: " & ExecScalar("SELECT Top 1 dbo.GetDecrypted256(PasswordEncrypted) From Data.LoginPassword Where LoginAccount_id = " & rsData("LoginAccount_id") & " Order By LoginPassword_id Desc", ""))
                        response.Write(", Fail Count:" & rsData("FailCount") & ", Last Fail:" & rsData("LastFailTime"))
                    Else
						response.Write("&nbsp;|&nbsp;<a href=""?action=showPassword&companyID=" & request("companyID")& """>Show Password</a>")
					End If
                    %>
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			</table>
		</td>
	</tr>
	<tr><td height="12"></td></tr>
	<tr><td bgcolor="#A9A7A2" height="1"></td></tr>
	<tr><td height="6"></td></tr>
	<tr>
		<td align="right"><input type="submit" value="Save Changes" style="width:110px; height:auto; background-color:#f1f1f1;"></td>
	</tr>
	</table>
	</form>
	<%
end if
%>
</body>
<%
rsData.close
Set rsData = nothing
Set rsData4 = nothing
call closeConnection()
%>
</html>