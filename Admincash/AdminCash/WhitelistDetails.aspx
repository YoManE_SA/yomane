<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Import namespace="System.Data"%>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<script runat="server">
	Public nID As Integer = 0

	Sub Page_PreInit()
		nID = dbPages.TestVar(Request.QueryString("ID"), 1, 0, 0)
	End Sub
	
	Sub ReloadData()
		Dim drData As SqlDataReader = dbPages.ExecReader("SELECT * FROM GetCreditCardWhiteListDetails(" & nID & ")")
		If drData.Read Then
			litID.Text = nID
			If drData("IsBurnt") Then litBurnt.Text = dbPages.FormatDateTime(drData("BurnDate")) Else litBurnt.Text = "Not burnt"
			lblLevel.BackColor = Drawing.ColorTranslator.FromHtml(drData("RecordLevelColor"))
			litLevel.Text = drData("RecordLevelName")
			litInsertDate.Text = dbPages.FormatDateTime(drData("InsertDate"))
			litUser.Text = drData("Username")
			litIP.Text = drData("IP")
			litHolder.Text = drData("CardHolder")
			litBin.Text = drData("Bin")
			litLast4.Text = drData("Last4")
			litNumber.Text = drData("CardNumber")
			litValidity.Text = drData("ExpMonth") & "/" & drData("ExpYear")
			litCardType.Text = drData("PaymentMethodName")
			imgCardType.ImageUrl = "/NPCommon/ImgPaymentMethod/23X12/" & drData("PaymentMethod") & ".gif"
			imgCardType.ToolTip = litCardType.Text
			litCountry.Text = drData("BinCountryName")
            imgCountry.ImageUrl = "/NPCommon/ImgCountry/18X12/" & drData("BinCountry") & ".gif"
			imgCountry.ToolTip = litCountry.Text
			litMerchant.Text = drData("Merchant")
			litName.Text = drData("MerchantName")
			litMID.Text = drData("MID")
			lblStatus.BackColor = Drawing.ColorTranslator.FromHtml(drData("MerchantStatusColor"))
			litStatus.Text = drData("MerchantStatusName")
			lblIndustry.BackColor = Drawing.ColorTranslator.FromHtml(drData("IndustryColor"))
			litIndustry.Text = drData("IndustryName")
			hlMail.Text = drData("MerchantMail")
			hlMail.NavigateUrl = "mailto:" & drData("MerchantMail") & "?Subject=Netpay - CC Whitelist"
			litPhone.Text = drData("MerchantPhone")
			litComment.Text = drData("Comment")

			ddlLevel.SelectedValue = drData("RecordLevel")
			ddlMonth.SelectedValue = drData("ExpMonth").ToString.Replace("0", String.Empty)
			ddlYear.SelectedValue = drData("ExpYear")
			rblBurnt.SelectedValue = IIf(drData("IsBurnt"), 1, 0)
			txtComment.Text = drData("Comment")
			mvData.ActiveViewIndex = 0
			pnlToolbar.Visible = True
		Else
			mvData.ActiveViewIndex = 1
			pnlToolbar.Visible = False
		End If
		drData.Close()
	End Sub

	Sub DeleteRecord(ByVal o As Object, ByVal e As EventArgs)
		dbPages.ExecSql("EXEC RemoveCreditCardWhiteList " & nID)
		ReloadData()
	End Sub

	Sub SaveRecord(ByVal o As Object, ByVal e As EventArgs)
		Dim nMerchant As Integer = litMerchant.Text
		Dim nLevel As Integer = ddlLevel.SelectedValue
		Dim nMonth As Integer = ddlMonth.SelectedValue
		Dim nYear As Integer = ddlYear.SelectedValue
		Dim nBurnt As Integer = rblBurnt.SelectedValue
		Dim sComment As String = dbPages.DBText(txtComment.Text, 200)
		
		dbPages.ExecSql("EXEC UpdateCreditCardWhiteList " & nID & ", " & nMerchant & ", " & nLevel & ", " & nMonth & ", " & nYear & ", " & nBurnt & ", '" & sComment & "'")
		ReloadData()
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		If Not Page.IsPostBack Then
			For Each gdvLevel As GlobalDataValue In GlobalData.GroupValues(GlobalDataGroup.WHITE_LIST_LEVEL)
				ddlLevel.Items.Add(New ListItem(gdvLevel.Text, gdvLevel.ID))
			Next
			For i As Integer = 1 To 12
				ddlMonth.Items.Add(New ListItem(i.ToString("00"), i))
			Next
			For i As Integer = 0 To 3
				ddlYear.Items.Add(Today.Year + i)
			Next
			ReloadData()
		End If
		For Each gdvLevel As GlobalDataValue In GlobalData.GroupValues(GlobalDataGroup.WHITE_LIST_LEVEL)
			ddlLevel.Items(gdvLevel.ID).Attributes.Add("style", "background-color:" & gdvLevel.Color)
		Next
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Credit Card Whitelist Details</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
</head>
<body onload="focus();" style="margin:0;padding:10px;background-color:White;overflow:auto;">
<form id="frmMain" runat="server">
	<asp:MultiView ID="mvData" runat="server">
		<asp:View runat="server">
			<table cellpadding="0" cellspacing="0" border="0" class="DetailSections">
				<tr>
					<td>
						<div class="SectionTitle">Record Details</div>
						<div><span class="ItemTitle">ID:</span> <asp:Literal ID="litID" runat="server" /></div>
						<div>
							<span class="ItemTitle">Level:</span>
							<asp:Label ID="lblLevel" Text="&nbsp;&nbsp;" runat="server" /> <asp:Literal ID="litLevel" runat="server" />
						</div>
						<div><span class="ItemTitle">Added on:</span> <asp:Literal ID="litInsertDate" runat="server" /></div>
						<div><span class="ItemTitle">Added by:</span> <asp:Literal ID="litUser" runat="server" /></div>
						<div><span class="ItemTitle">Added at IP:</span> <asp:Literal ID="litIP" runat="server" /></div>
						<div><span class="ItemTitle">Burnt:</span> <asp:Literal ID="litBurnt" runat="server" /></div>
						<div><span class="ItemTitle">Comment:</span> <asp:Literal ID="litComment" runat="server" /></div>
					</td>
					<td>
						<div class="SectionTitle">Credit Card Details</div>
						<div>
							<span class="ItemTitle">Card Type:</span>
							<asp:Image ID="imgCardType" runat="server" />
							<asp:Literal ID="litCardType" runat="server" />
						</div>
						<div>
							<span class="ItemTitle">Country:</span>
							<asp:Image ID="imgCountry" runat="server" />
							<asp:Literal ID="litCountry" runat="server" />
						</div>
						<div><span class="ItemTitle">BIN:</span> <asp:Literal ID="litBin" runat="server" /></div>
						<div><span class="ItemTitle">Last 4 digits:</span> <asp:Literal ID="litLast4" runat="server" /></div>
						<div><span class="ItemTitle">Number:</span> <asp:Literal ID="litNumber" runat="server" /></div>
						<div><span class="ItemTitle">Validity:</span> <asp:Literal ID="litValidity" runat="server" /></div>
						<div><span class="ItemTitle">Card Holder:</span> <asp:Literal ID="litHolder" runat="server" /></div>
					</td>
					<td>
						<div class="SectionTitle">Merchant Details</div>
						<div><span class="ItemTitle">ID:</span> <asp:Literal ID="litMerchant" runat="server" /></div>
						<div><span class="ItemTitle">Name:</span> <asp:Literal ID="litName" runat="server" /></div>
						<div><span class="ItemTitle">MID:</span> <asp:Literal ID="litMID" runat="server" /></div>
						<div>
							<span class="ItemTitle">Status:</span>
							<asp:Label ID="lblStatus" Text="&nbsp;&nbsp;" runat="server" /> <asp:Literal ID="litStatus" runat="server" />
						</div>
						<div>
							<span class="ItemTitle">Industry:</span>
							<asp:Label ID="lblIndustry" Text="&nbsp;&nbsp;" runat="server" /> <asp:Literal ID="litIndustry" runat="server" />
						</div>
						<div><span class="ItemTitle">Mail:</span> <asp:HyperLink ID="hlMail" runat="server" /></div>
						<div><span class="ItemTitle">Phone:</span> <asp:Literal ID="litPhone" runat="server" /></div>
					</td>
				</tr>
			</table>
		</asp:View>
		<asp:View runat="server">
			<div class="errorMessage">
				The requested record not found in the credit card whitelist. Probably, the record has been removed from the list.
			</div>
		</asp:View>
	</asp:MultiView>
	<br />
	<table cellpadding="0" cellspacing="0" border="0" style="padding:3px 5px;width:100%;background-color:#f1efe2;font-size:11px;">
		<tr>
			<td style="vertical-align:middle;">
				<asp:Panel ID="pnlToolbar" runat="server">
					Set level: <asp:DropDownList ID="ddlLevel" runat="server" CssClass="option inputdata" />
					&nbsp;
					Validity:
					<asp:DropDownList ID="ddlMonth" runat="server" CssClass="option inputdata" />/<asp:DropDownList ID="ddlYear" runat="server" CssClass="option inputdata" />
					&nbsp;
					Comment: <asp:TextBox ID="txtComment" runat="server" CssClass="option inputdata" Width="300px" />
					&nbsp;
					Burnt:
					<asp:RadioButtonList ID="rblBurnt" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" CssClass="option">
						<asp:ListItem Value="1" Text="Yes" />
						<asp:ListItem Value="0" Text="No" />
					</asp:RadioButtonList>
					&nbsp; &nbsp;
					<asp:Button runat="server" OnClick="SaveRecord" Text=" Save " CssClass="button1 option" UseSubmitBehavior="false" />
					<asp:Button runat="server" OnClick="DeleteRecord" Text=" Delete " CssClass="button1 option buttonDelete" UseSubmitBehavior="false"
					 OnClientClick="if (!confirm('Are you sure ?!')) return false;" />
					<input type="button" onclick="parent.parent.frames[0].document.forms[0].submit();" value=" Close " class="button1 option" />
				</asp:Panel>
			</td>
			<td style="text-align:right;vertical-align:middle;"><asp:label ID="lblPermissions" runat="server" /></td>
		</tr>
	</table>
</form>
</body>
</html>