<%@ Page Language="VB" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = Date.Now.AddMonths(-1).AddDays(1).ToShortDateString
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

	<form runat="server" action="Trans_RollingData.aspx" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" id="Source" name="Source" value="filter" />
	<input type="hidden" id="ShowCompanyID" name="ShowCompanyID" />

	<h1><asp:Label ID="lblPermissions" runat="server" /> Rolling Reserve<span> - Filter</span></h1>

	<table class="filterNormal">
		<tr><td><Uc1:FilterMerchants id="FMerchants" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" Title="Create Date" /></td></tr>
		<tr><td><Uc1:FilterDateTimeRange ID="rdtrControl" runat="server" Title="Release Date" FromDateFieldName="PDfromDate" FromTimeFieldName="PDfromTime" ToDateFieldName="PDtoDate" ToTimeFieldName="PDtoTime" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="3">Type</th></tr>
		<tr>
			<td><input type="Checkbox" name="iCreditType0" value="1" onclick="document.getElementById('chkNotRelease').disabled=!this.checked;" checked> Reserve Hold &nbsp; <img src="../images/icon_ArrowR.gif" align="middle" /><br /></td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="Checkbox" name="iNotRelease" id="chkNotRelease" value="1"> Only overdue holdings &nbsp;</td>
		</tr>
		<tr>
			<td><input type="Checkbox" name="iCreditType1" value="1" checked> Release &nbsp; <img src="../images/icon_ArrowG.gif" align="middle" /><br /></td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Amount</th></tr>
		<tr>
			<td>
				<input type="text" class="input1" size="5" dir="ltr" name="iAmountFrom" value="">
				&nbsp;
				To <input type="text" class="input1" size="5" dir="ltr" name="iAmountTo" value="">
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td>
				<select name="iPageSize">
					<option value="20" selected="selected">20 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td style="text-align:right;">
				<input type="submit" name="Action" value="SEARCH"> &nbsp;&nbsp;
				<input type="submit" name="Excel" value="XSL">
			</td>
		</tr>
	</table>
	</form>
</asp:Content>