<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/security_class.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_balance.asp"-->
<%
Dim FinanceLevel1Group, FinanceLevel2Group
Dim wireFlag, nWireMoney_id, sBkgColor, sUsername
nWireMoney_id = Trim(request("WireMoney_id"))
sBkgColor = Trim(request("bkgColor"))
sUsername = PageSecurity.Username
FinanceLevel1Group = GetDomainConfig(Empty, "FinanceLevel1Group", "Financial Manager")
FinanceLevel2Group = GetDomainConfig(Empty, "FinanceLevel2Group", "High Managment")

if trim(request("Action"))="UPDATE" then
	sWireCommentOld=request("wireCommentOld")
	sWireConfirmationNumOld=request("wireConfirmationNumOld")
	nWireFlagOld=request("WireFlagOld")
	sWireComment=request("wireComment")
	sWireConfirmationNum=request("wireConfirmationNum")
	wireFlag=request("WireFlag")
	bUpdate=false
	if sWireCommentOld<>sWireComment then
		bUpdate=true
		ExecSQL "INSERT INTO tblWireMoneyLog(WireMoney_id, wml_description, wml_user) VALUES" & _
		" (" & nWireMoney_id & ", Left('Wire comment changed: " & dbText(sWireComment) & "', 250)," & _
		" Left('" & dbText(sUsername) & "', 50))"
	end if
	if sWireConfirmationNumOld<>sWireConfirmationNum then
		bUpdate=true
		ExecSQL "INSERT INTO tblWireMoneyLog(WireMoney_id, wml_description, wml_user) VALUES" & _
		" (" & nWireMoney_id & ", Left('Confirmation number changed: " & dbText(sWireConfirmationNum) & "', 250)," & _
		" Left('" & dbText(sUsername) & "', 50))"
	end if
	if bUpdate then
		ExecSQL "UPDATE tblWireMoney SET wireComment='" & DBText(sWireComment) & "'," & _
		" wireConfirmationNum='" & DBText(sWireConfirmationNum) & "'" & _
		" WHERE WireMoney_id=" & nWireMoney_id
		
		logMasavFile_id = ExecScalar("Select logMasavFile_id From tblLogMasavDetails Where WireMoney_id=" & nWireMoney_id, 0)
		xVal = ExecScalar("Select Count(WireMoney_id) From tblWireMoney Where WireFlag <> 3 And WireMoney_id IN(Select WireMoney_id From tblLogMasavDetails Where logMasavFile_id=" & logMasavFile_id & ")", 0)
		oledbData.execute("Update tblLogMasavFile Set DoneFlag=" & IIF(xVal > 0, 0, 1) & " Where logMasavFile_id=" & logMasavFile_id)
		%>
		<script language="JavaScript">
			parent.document.flagImage<%= nWireMoney_id %>.src="<%= sSrc %>";
			parent.document.flagImage<%= nWireMoney_id %>.alt="<%= sAlt %>";
			if(parent.FlagChanged) parent.FlagChanged(<%= nWireMoney_id %>, <%=wireFlag %>);
		</script>
		<%
	end if
elseif Trim(Request("Action")) = "UPDATELEVEL" then
	Dim setValue
	wireFlag = ExecScalar("Select wireStatus From tblWireMoney Where WireMoney_id=" & nWireMoney_id, 0)
	If wireFlag <> WRF_Done Then
		If trim(request("UpdateLevel1")) <>"" Then
			setValue = IIF(Request("UpdateLevel1Value") = "1",  "1", IIF(Request("UpdateLevel1Value") = "0", "0", "NULL"))
			oledbData.Execute "Update tblWireMoney Set wireApproveLevel1=" & setValue & " Where WireMoney_id=" & nWireMoney_id
		ElseIf trim(request("UpdateLevel2")) <>"" Then
			setValue = IIF(Request("UpdateLevel2Value") = "1", "1", IIF(Request("UpdateLevel2Value") = "0", "0", "NULL"))
			oledbData.Execute "Update tblWireMoney Set wireApproveLevel2=" & setValue & " Where WireMoney_id=" & nWireMoney_id
		End If
		If setValue = "1" Then 
			setValue = "Approved" 
		ElseIf setValue = "0" Then 
			setValue = "Hold"
		Else
			setValue = "Cleared"
		End If
		ExecSQL "INSERT INTO tblWireMoneyLog(WireMoney_id, wml_description, wml_user) VALUES" & _
			" (" & nWireMoney_id & ", Left('Approval changed: " & dbText(setValue) & "', 250)," & _
			" Left('" & dbText(sUsername) & "', 50))"

		Dim wireRs : 
		Set wireRs = oleDbData.Execute("Select wireApproveLevel1, wireApproveLevel2 From tblWireMoney Where WireMoney_id=" & nWireMoney_id)
		If (Not wireRs("wireApproveLevel1")) Or (Not wireRs("wireApproveLevel2")) Then
			ExecSQL "UPDATE tblWireMoney SET wireFlag=" & WRF_Rejected & " WHERE WireMoney_id=" & nWireMoney_id
		ElseIf wireRs("wireApproveLevel1") And wireRs("wireApproveLevel2") Then
			ExecSQL "UPDATE tblWireMoney SET wireFlag=" & WRF_Approved & " WHERE WireMoney_id=" & nWireMoney_id
		ElseIf IsNull(wireRs("wireApproveLevel1")) Or IsNull(wireRs("wireApproveLevel2")) Then
			ExecSQL "UPDATE tblWireMoney SET wireFlag=" & WRF_PartiallyApproved & " WHERE WireMoney_id=" & nWireMoney_id
		Else
			ExecSQL "UPDATE tblWireMoney SET wireFlag=" & WRF_Waiting & " WHERE WireMoney_id=" & nWireMoney_id
		End If
		wireRs.Close()
	End If
End If
Dim wireApproveLevel1, wireApproveLevel2, wireFlagText
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<script language="javascript">
		function fixFrameHeight() {
			ObjTmp = parent.document.getElementById('iFrame<%=nWireMoney_id%>');
			ObjTmp.height = ObjTmp.contentWindow.document.body.scrollHeight;
		}
	</script>
</head>
<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="8" style="<%=sBkgColor%>color:#000000!important;">

	<%
	sSQL="SELECT tblWireMoney.Company_id, tblWireMoney.wireType, tblWireMoney.WireSourceTbl_id, tblWireMoney.wireComment, tblWireMoney.wireConfirmationNum, wireApproveLevel1, wireApproveLevel2,GD_Text as wireFlagText," & _
	" tblWireMoney.wireCurrency, tblWireMoney.wireFlag, tblCompany.CompanyIndustry_id, tblWireMoney.ConfirmationFileName " & _
	" FROM tblWireMoney " & _
	" LEFT JOIN tblCompany ON (tblWireMoney.Company_id = tblCompany.ID)" & _
	" LEFT JOIN tblGlobalData ON (tblGlobalData.GD_Group=48 AND tblGlobalData.GD_Lng=1 And WireFlag=tblGlobalData.GD_ID)" & _
	" WHERE WireMoney_id=" & nWireMoney_id
	set rsData = oledbData.execute(sSQL)
	if NOT rsData.EOF then
		sCompanyID = trim(rsData("Company_id"))
		nWireType = trim(rsData("wireType"))
		nWireSourceTbl_id = trim(rsData("WireSourceTbl_id"))
		sWireComment = DBTextShow(rsData("wireComment"))
		sWireConfirmationNum = DBTextShow(rsData("wireConfirmationNum"))
		wireApproveLevel1 = rsData("wireApproveLevel1")
		wireApproveLevel2 = rsData("wireApproveLevel2")
		wireFlag = rsData("wireFlag")
		wireFlagText = rsData("wireFlagText")
		wireConfirmationFileName = rsData("ConfirmationFileName")
		%>
		<table border="0" cellspacing="0" cellpadding="1">
		<tr>
			<td style="white-space:nowrap; vertical-align:top; padding-right:20px; height:100%">
				<fieldset style="height:100%;">
					<legend>Balance</legend>
					<%
					'sSQL = "SELECT CUR_ISOName, Currency, BalanceCurrent, BalanceExpected FROM tblCompanyBalance INNER JOIN tblSystemCurrencies ON tblCompanyBalance.currency=tblSystemCurrencies.CUR_ID WHERE company_id=" & rsData("Company_ID") & " AND companyBalance_ID IN (SELECT Max(companyBalance_ID) FROM tblCompanyBalance t WHERE t.currency=tblCompanyBalance.currency AND t.company_id=tblCompanyBalance.company_id) ORDER BY Currency"
					Set rsDataTmp = fn_GetBalances(sCompanyID)
					if rsDataTmp.Count = 0 then
						response.Write "No balance history."
					else
						%>
						<table border="0" cellpadding="2" cellspacing="0" class="secondary">
						<tr>
							<th>Currency</th>
							<th style="text-align:right;">Expected</th>
							<th style="text-align:right;">Current</th>
						</tr>
						<%
    	    			For Each t In rsDataTmp.Items
							%>
							<tr style="<% If t.CurrencyId <> rsData("wireCurrency") Then response.write "font-weight:bold; color:black;" %>">
								<td><%= GetCurISO(t.CurrencyId) %></td>
								<td style="text-align:right;"><%= FormatCurr(t.CurrencyId, t.Expected) %></td>
								<td style="text-align:right;"><%= FormatCurr(t.CurrencyId, t.Current) %></td>
							</tr>
							<%
						Next
						%>
						</table>
						<%
					end if
					Set rsDataTmp = nothing
					%>
				</fieldset>
			</td>
			<td style="white-space:nowrap; vertical-align:top; padding-right:20px; height:100%;">
				<fieldset style="height:100%;">
					<legend>Unsettled Transactions</legend>
					<%
					sSQL = "SELECT CUR_ISOName, Currency, Count(*) TransCount, Sum(UnsettledAmount) TransAmount FROM tblSystemCurrencies INNER JOIN tblCompanyTransPass ON tblSystemCurrencies.CUR_ID=tblCompanyTransPass.Currency WHERE UnsettledInstallments>0 AND CompanyID=" & rsData("Company_ID") & " GROUP BY Currency, CUR_ISOName ORDER BY Currency"
					set rsDataTmp=oledbData.Execute(sSQL)
					if rsDataTmp.EOF then
						response.Write "No unsettled transaction."
					else
						%>
						<table border="0" cellpadding="2" cellspacing="0" class="secondary">
						<tr>
							<th>Currency</th>
							<th style="text-align:right;">Count</th>
							<th style="text-align:right;">Amount</th>
						</tr>
						<%
						do until rsDataTmp.EOF
							%>
							<tr style="<% if rsDataTmp("Currency")-rsData("wireCurrency")=0 then response.write "font-weight:bold; color:black;" %>">
								<td><%= rsDataTmp("CUR_ISOName") %></td>
								<td style="text-align:right;"><%= rsDataTmp("TransCount") %></td>
								<td style="text-align:right;"><%= FormatCurr(rsDataTmp("Currency"), rsDataTmp("TransAmount")) %></td>
							</tr>
							<%
							rsDataTmp.MoveNext
						loop
						%>
						</table>
						<%
					end if
					rsDataTmp.close
					Set rsDataTmp = nothing
					%>
				</fieldset>
			</td>
			<td style="white-space:nowrap; vertical-align:top; height:100%;">
				<fieldset style="height:100%;">
					<legend>Rolling Reserves</legend>
					<%
					sSQL = "SELECT CUR_ISOName,Currency, Sum(Amount*CASE CreditType WHEN 0 THEN 1 ELSE -1 END) AmountReserved FROM tblCompanyTransPass "
					sSQL = sSQL & " Inner Join tblSystemCurrencies On tblCompanyTransPass.Currency = tblSystemCurrencies.CUR_ID "
					sSQL = sSQL & "WHERE tblCompanyTransPass.PaymentMethod=" &  PMD_RolRes & " And companyID=" & sCompanyID
					sSQL = sSQL & " Group By CUR_ISOName,Currency"
					sSQL = sSQL & " ORDER BY Currency Asc"
					Set rsRollRes=oledbData.Execute(sSQL)
					
					sSQL = "SELECT tblGlobalData.GD_Text FROM  tblCompany LEFT OUTER JOIN tblGlobalData ON tblCompany.RRState = tblGlobalData.GD_ID WHERE (tblCompany.ID = 35) AND (tblGlobalData.GD_LNG = 1) AND (tblGlobalData.GD_Group = 55)"
					Set rsRollResState=oledbData.Execute(sSQL)
					
					if rsRollRes.EOF then
						response.Write "No Rolling Reserve data for this merchant."
					else
					%>
					<table border="0" cellpadding="2" cellspacing="0" class="secondary">
					<tr>
						<th>Currency</th>
						<th style="text-align:right">Balance</th>
					</tr>
					<%
					Do until rsRollRes.EOF
						response.Write "<tr "
						if rsRollRes("Currency")-rsData("wireCurrency")=0 then response.write "style=""font-weight:bold; color:black;"""
						response.Write " >"
						response.write "<td>" & rsRollRes("CUR_ISOName") & "</td>"
						response.Write "<td style=""text-align:right"">" & FormatCurr(rsRollRes("Currency"), rsRollRes("AmountReserved")) & "</td>"
						response.Write "</tr>"
						rsRollRes.movenext
					Loop
					rsRollRes.Close()
					set rsRollRes = nothing
					%>
					</table>
					<hr noshade="noshade" size="1" width="96%" style="border:dotted silver 1px;" />
					<table border="0" cellpadding="2" cellspacing="0" class="secondary">
						<%
						response.Write "<td style=""font-weight:normal;"">Hold Settings:</td><td>" & rsRollResState("GD_Text") & "</td>"
						%>					
					</table>
					<%
					end if
					%>
				</fieldset>
			</td>		
		</tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="1">
		<tr>
			<td style="vertical-align:top; height:100%;">
				<fieldset style="height:100%;">
					<legend>Action Log</legend>
					<%
					sSQL = "SELECT wml_date, wml_description, wml_user FROM tblWireMoneyLog WHERE WireMoney_id =" & nWireMoney_id & " ORDER BY wireMoneyLog_id DESC"
					Set rsDataTmp=oledbData.Execute(sSQL)
					if rsDataTmp.EOF then
						response.Write "No action was taken on this transfer."
					else
						%>
						<table border="0" cellpadding="2" cellspacing="0" class="secondary">
						<tr>
							<th>Date &amp; Time</th>
							<th>User</th>
							<th>Description</th>
						</tr>
						<%
						nCount = 0
						Do until rsDataTmp.EOF
							nCount = nCount + 1
							If nCount = 5 Then Response.Write("<tbody id=""tbLog"" style=""display:none;"">")
							%>
							<tr id="trLog" style="vertical-align:top;">
								<td style="white-space:nowrap;"><%= Left(rsDataTmp("wml_date"), 16) %></td>
								<td><%= rsDataTmp("wml_user") %></td>
								<td><%= rsDataTmp("wml_description") %></td>
							</tr>
							<%
							rsDataTmp.movenext
						Loop
						If nCount > 4 Then
							%>
							</tbody>
							<tr id="trLogLink"><td><a href="#" onclick="trLogLink.style.display='none'; tbLog.style.display='';fixFrameHeight();">Show All</a></td></tr>
							<%
						End if
						%>
						</table>
						<%
					end if
					rsDataTmp.close
					Set rsDataTmp = nothing
					%>
				</fieldset>
			</td>
			<td style="vertical-align:top; height:100%;">
				<fieldset style="height:100%;">
					<legend>Info</legend>
					<%
					GetConstArray Server.CreateObject("ADODB.Recordset"), 51, 1, "", saIndustry, saColor
					If TestNumVar(rsData("CompanyIndustry_id"), 0, -1, 0) > 0 Then CompanyIndustry = saIndustry(rsData("CompanyIndustry_id")) _
					Else CompanyIndustry = "---"
					%>
					Industry: <%= CompanyIndustry %><br /><br />
					<%
					If nWireType=1 Then
						%>
						Settlement: <%= nWireSourceTbl_id %>
						[<a href="Merchant_transSettledDetail.asp?companyID=<%= sCompanyID %>&payID=<%= nWireSourceTbl_id %>" target="_blank" style="text-decoration:underline;">SHOW &gt;&gt;</a>]<br />
						<%
					End If
					%>
				</fieldset>
			</td>	
			<td style="vertical-align:top; height:100%;">
				<form action="Wire_commentUpd.asp" method="post">
				<input type="Hidden" name="WireMoney_id" value="<%= nWireMoney_id %>">
				<input type="Hidden" name="Action" value="UPDATELEVEL">
				<fieldset style="height:100%;" <%= IIf(wireFlag = WRF_Done, "disabled", "") %> >
					<legend>Wire Approval</legend>
						<table border="0" cellpadding="2" cellspacing="0" class="secondary">
						<tr>
							<td style="color:<% If NOT PageSecurity.IsMemberOf(FinanceLevel1Group) Then Response.Write("#d0d0d0") %>;"><%=FinanceLevel1Group%></td>
							<td>
								<%disabledText = IIF(PageSecurity.IsMemberOf(FinanceLevel1Group), "", " disabled") %>
								<select name="UpdateLevel1Value" <%=disabledText%>><option></option><option value="0"<%=IIF(wireApproveLevel1 = False And Not IsNull(wireApproveLevel1), " selected", "") %>>Reject</option><option value="1"<%=IIF(wireApproveLevel1 And Not IsNull(wireApproveLevel1), " selected", "") %>>Approve</option></select>
							</td>
							<td><input type="submit" name="UpdateLevel1" value="UPDATE" class="b_windowCharge" <%=disabledText%>></td>
						</tr>
						
						<tr>
							<td style="color:<% If NOT PageSecurity.IsMemberOf(FinanceLevel2Group) Then Response.Write("#d0d0d0") %>;"><%=FinanceLevel2Group%></td>
							<td>
								<%disabledText = IIF(PageSecurity.IsMemberOf(FinanceLevel2Group), "", " disabled") %>
								<select name="UpdateLevel2Value" <%=disabledText%>><option></option><option value="0"<%=IIF(wireApproveLevel2 = False And Not IsNull(wireApproveLevel2), " selected", "") %>>Reject</option><option value="1"<%=IIF(wireApproveLevel2 And Not IsNull(wireApproveLevel2), " selected", "") %>>Approve</option></select>
							</td>
							<td><input type="submit" name="UpdateLevel2" value="UPDATE" class="b_windowCharge" <%=disabledText%>></td>
						</tr>
						</table>
				</fieldset>
				</form>
			</td>		
		</tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="1">
		<tr>
			<td class="txt12">
				<fieldset style="height:100%;">
					<legend>Details</legend>
					<form action="Wire_commentUpd.asp" method="post" name="frmwireUpd" id="frmwireUpd">
					<input type="Hidden" name="WireMoney_id" value="<%= nWireMoney_id %>">
					<table border="0" cellspacing="0" cellpadding="1">
					<tr>
						<td>
							Wire Comment
							<input type="Text" name="wireComment" maxlength="500" value="<%= sWireComment %>" size="40" class="inputData">
							<input type="Hidden" name="wireCommentOld" value="<%= sWireComment %>">
							&nbsp;Confirmation
							<input type="Text" name="wireConfirmationNum" maxlength="30" value="<%= sWireConfirmationNum %>" size="12" class="inputData">
							<input type="Hidden" name="wireConfirmationNumOld" value="<%= sWireConfirmationNum %>">
							&nbsp;Status:&nbsp;&nbsp;<%=wireFlagText %>
							<input type="Hidden" name="wireFlagOld" value="<%= wireFlag %>">
							&nbsp;<input type="submit" name="Action" value="UPDATE" class="b_windowCharge">
						</td>		
					</tr>
					<%
					If nWireType = "2" Then
						sMerchantComment = ""
						sMerchantComment = ExecScalar("SELECT paymentMerchantComment FROM tblCompanyMakePaymentsRequests WHERE CompanyMakePaymentsRequests_id = " & nWireSourceTbl_id, "")
						If sMerchantComment<>"" Then
							%>
							<tr><td height="6"></td></tr>
							<tr>
								<td colspan="3">
									Merchant Comment: <%= sMerchantComment %>
								</td>
							</tr>
							<%
						End If
					End if
					%>
					</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td>
				<form action="Wire_File.aspx" method="post" enctype="multipart/form-data" style="display:inline;">
					<input type="hidden" name="WireMoneyID" value="<%= nWireMoney_id %>" />
					<fieldset>
						<legend>Files Upload</legend>
						<table border="0" cellpadding="1" cellspacing="1" class="secondary">
						<%
						nCount = 0
						Set rsDataTmp=oledbData.Execute("SELECT * FROM tblWireMoneyFile WHERE wmf_WireMoneyID=" & nWireMoney_id)
						If NOT rsDataTmp.EOF Then
							%>
							<tr>
								<th>Date &amp Time</th>
								<th>User</th>
								<th>Description</th>
								<th>File</th>
								<th></th>
							</tr>
							<%
							Dim sBGColor
							Do Until rsDataTmp.EOF
								nCount = nCount + 1
								sFileName = rsDataTmp("wmf_FileName")
								sFilePath = MapWirePath("/WireFiles/" & rsDataTmp("wmf_FileName"))
								sDeleteDisplay = ""
								sIcon = GetFileIcon(sFileName)
								%>
								<tr>
									<td><%= Left(rsDataTmp("wmf_Date"), 16) %></td>
									<td><%= rsDataTmp("wmf_User") %></td>
									<td><%= rsDataTmp("wmf_Description") %></td>
									<td><a target="_blank" href="?DownloadPrivateFile=<%= Server.UrlEncode("Wires\WireFiles\" & sFileName) %>"><%= sIcon & sFileName %></a></td>
									<td style="text-align:center;"><img src="../images/b_deleteEng.gif" style="cursor:pointer;display:<%= sDeleteDisplay %>;" onclick="location.href='Wire_File.aspx?DeleteFileID=<%= rsDataTmp("ID") %>&WireMoneyID=<%= nWireMoney_id %>';" /></td>
								</tr>
								<%
								rsDataTmp.MoveNext
							Loop
						End if
						rsDataTmp.close
						Set rsDataTmp = Nothing
						If nCount > 0 Then
							%>
							<tr id="trUploadLink"><td>[<a href="#" onclick="trUploadLink.style.display='none'; trUpload.style.display=''; fixFrameHeight();">Upload More</a>]</td></tr>
							<%
						End if
						sStyle = IIf(nCount = 0, "block", "none")
						%>
						<tr id="trUpload" style="display:<%=sStyle%>;">
							<td colspan="5" style="padding-top:6px;">
								Description: <input name="DescriptionNew" size="24" class="inputData" /> &nbsp;
								Choose File: <input type="file" name="FileNew" size="14" class="inputData" accept="*.doc;*.pdf;*.tif;" /> &nbsp;
								<input type="submit" value="UPLOAD" class="b_windowCharge" />
							</td>
						</tr>
						</table>
					</fieldset>
				</form>
			</td>
		</tr>
		</table>
		<%
	End if

rsData.close
Set rsData = nothing
call closeConnection()
%>

</body>
</html>