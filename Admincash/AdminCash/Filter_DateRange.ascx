﻿<%@ Control Language="VB" ClassName="Filter_DateRange" %>
<script runat="server">
    Public AutoPostBack As Boolean 
	Private mDateFrom As String = String.Empty
	Public Property DateFrom() As String
		Get
			Return mDateFrom
		End Get
		Set(ByVal value As String)
			mDateFrom = value
		End Set
	End Property

	Private mDateTo As String = String.Empty
	Public Property DateTo() As String
		Get
			Return mDateTo
		End Get
		Set(ByVal value As String)
			mDateTo = value
		End Set
	End Property

	Private mTimeFrom As String = String.Empty
	Public Property TimeFrom() As String
		Get
			Return mTimeFrom
		End Get
		Set(ByVal value As String)
			mTimeFrom = value
		End Set
	End Property

	Private mTimeTo As String = String.Empty
	Public Property TimeTo() As String
		Get
			Return mTimeTo
		End Get
		Set(ByVal value As String)
			mTimeTo = value
		End Set
	End Property

	Private mTitle As String = "Date Range"
	Public Property Title() As String
		Get
			Return mTitle
		End Get
		Set(ByVal value As String)
			mTitle = value
		End Set
	End Property

	Private mTitleFrom As String = "From"
	Public Property TitleFrom() As String
		Get
			Return mTitleFrom
		End Get
		Set(ByVal value As String)
			mTitleFrom = value
		End Set
	End Property

	Private mTitleTo As String = "To"
	Public Property TitleTo() As String
		Get
			Return mTitleTo
		End Get
		Set(ByVal value As String)
			mTitleTo = value
		End Set
	End Property

	Private mFieldFrom As String = "fromDate"
	Public Property FieldFrom() As String
		Get
			Return mFieldFrom
		End Get
		Set(ByVal value As String)
			mFieldFrom = value
		End Set
	End Property

	Private mFieldTo As String = "toDate"
	Public Property FieldTo() As String
		Get
			Return mFieldTo
		End Get
		Set(ByVal value As String)
			mFieldTo = value
		End Set
	End Property

	Private mFieldFromTime As String = "fromTime"
	Public Property FieldFromTime() As String
		Get
			Return mFieldFromTime
		End Get
		Set(ByVal value As String)
			mFieldFromTime = value
		End Set
	End Property

	Private mFieldToTime As String = "toTime"
	Public Property FieldToTime() As String
		Get
			Return mFieldToTime
		End Get
		Set(ByVal value As String)
			mFieldToTime = value
		End Set
	End Property

	Private mIsSingleRow As Boolean = False
	Public Property IsSingleRow() As Boolean
		Get
			Return mIsSingleRow
		End Get
		Set(ByVal value As Boolean)
			mIsSingleRow = value
		End Set
	End Property

	Private mFieldClass As String = String.Empty
	Public Property FieldClass() As String
		Get
			Return mFieldClass
		End Get
		Set(ByVal value As String)
			mFieldClass = value
		End Set
	End Property

	Private mFieldTimeClass As String = String.Empty
	Public Property FieldTimeClass() As String
		Get
			Return mFieldTimeClass
		End Get
		Set(ByVal value As String)
			mFieldTimeClass = value
		End Set
	End Property

	Private mIsCalendarBelow As Boolean = False
	Public Property IsCalendarBelow() As Boolean
		Get
			Return mIsCalendarBelow
		End Get
		Set(ByVal value As Boolean)
			mIsCalendarBelow = value
		End Set
	End Property
</script>
<link rel="stylesheet" type="text/css" media="all" href="../StyleSheet/calendar-blue.css" title="winter" />
<script type="text/javascript" src="../js/calendar.js"></script>
<script type="text/javascript" src="../js/calendar-en.js"></script>
<script type="text/javascript" src="../js/calendar-setup.js"></script>
<table class="filterNormal" <%= IIf(IsSingleRow, "cellspacing=""0""", String.Empty) %>>
<%=IIf(Title = String.Empty, String.Empty, "<tr><th colspan=""2"">" & Title & "<br /></th></tr>")%>
<tr>
	<td <%= IIf(IsSingleRow, String.Empty, "width=""50""") %>><%= TitleFrom %>&nbsp;</td>
	<td style="white-space:nowrap;margin-left:0;padding-left:0;">
		<img src="../images/calendar_icon.gif" id="<%= FieldFrom %>_trigger_c" width="16" height="15" alt="<%= Title & " " & TitleFrom %>" style="cursor:pointer;vertical-align:middle;border-width:0;" />
		<input dir="ltr" size="8" name="<%= FieldFrom %>" id="fromDate" value="<%=DateFrom%>" class="<%= FieldClass %>" />
		<%=IIf(TimeFrom = String.Empty, String.Empty, "<input dir=""ltr"" size=""3"" name=""" & FieldFromTime & """ value=""" & TimeFrom & """ class=""" & FieldTimeClass & """ />")%>
	</td>
	<%=IIf(IsSingleRow, String.Empty, "</tr><tr>")%>
	<td><%=IIf(IsSingleRow, "&nbsp;", String.Empty)%><%= TitleTo %>&nbsp;<br /></td>
	<td style="white-space:nowrap;margin-left:0;padding-left:0;">
		<img src="../images/calendar_icon.gif" id="<%= FieldTo %>_trigger_c" width="16" height="15" alt="<%= Title & " " & TitleTo %>" style="cursor:pointer;vertical-align:middle;border-width:0;" />
		<input type="text" dir="ltr" size="8" name="<%= FieldTo %>" id="toDate" value="<%=DateTo%>" class="<%= FieldClass %>" />
		<%=IIf(TimeTo = String.Empty, String.Empty, "<input dir=""ltr"" size=""3"" name=""" & FieldToTime & """ value=""" & TimeTo & """ class=""" & FieldTimeClass & """ />")%>
	</td>
</tr>	
</table>
<script type="text/javascript">
	Calendar.setup({
		inputField	 :	"<%= FieldFrom %>",	// id of the input field
		ifFormat		:	"%d/%m/%Y",	// format of the input field
		button		 :	"<%= FieldFrom %>_trigger_c",	// trigger for the calendar (button ID)
		align		  :	"<%= IIf(IsCalendarBelow, "BC", "TC") %>",	// alignment (defaults to "Bl")
		singleClick	:	true,
		mondayFirst	:	false,
		weekNumbers	:	false
	});
	Calendar.setup({
		inputField	 :	"<%= FieldTo %>",	// id of the input field
		ifFormat		:	"%d/%m/%Y",	// format of the input field
		button		 :	"<%= FieldTo %>_trigger_c",	// trigger for the calendar (button ID)
		align		  :	"<%= IIf(IsCalendarBelow, "BC", "TC") %>",	// alignment (defaults to "Bl")
		singleClick	:	true,
		mondayFirst	:	false,
		weekNumbers	:	false
	});
</script>