﻿<%@ Control Language="VB" ClassName="MerchantBankAccountLocal" EnableViewState="true" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
	Private nRefID As Integer = 0
    Private bIsMerchant As Boolean
	Private bIsCorrespondent As Boolean = False

	Public Property Merchant() As Integer
		Get
			Return nRefID
		End Get
		Set(ByVal value As Integer)
            bIsMerchant = True
			nRefID = value
		End Set
	End Property

	Public Property Affiliate() As Integer
		Get
			Return nRefID
		End Get
		Set(ByVal value As Integer)
            bIsMerchant = False
			nRefID = value
		End Set
	End Property

	Public Property PaymentType() As String
		Get
			Return txtPaymentType.Text
		End Get
		Set(ByVal value As String)
			txtPaymentType.Text = value
		End Set
	End Property

	Public Property Beneficiary() As String
		Get
			Return txtBeneficiary.Text
		End Get
		Set(ByVal value As String)
			txtBeneficiary.Text = value
		End Set
	End Property

	Public Property BranchNumber() As String
		Get
			Return txtBranchNumber.Text
		End Get
		Set(ByVal value As String)
			txtBranchNumber.Text = value
		End Set
	End Property

	Public Property AccountNumber() As String
		Get
			Return txtAccountNumber.Text
		End Get
		Set(ByVal value As String)
			txtAccountNumber.Text = value
		End Set
	End Property

	Public Property Bank() As Integer
		Get
			Return ddlBank.SelectedValue
		End Get
		Set(ByVal value As Integer)
			ddlBank.SelectedValue = value
		End Set
	End Property

	Public ReadOnly Property BankName() As String
		Get
			Return ddlBank.selectedItem.Text
		End Get
	End Property

	Public Sub ClearValues(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		txtPaymentType.Text = String.Empty
		txtBeneficiary.Text = String.Empty
		txtBranchNumber.Text = String.Empty
		txtAccountNumber.Text = String.Empty
		txtIdNumber.Text = String.Empty
		ddlBank.SelectedIndex = 0
	End Sub

	Public ReadOnly Property IsEmpty() As Boolean
		Get
			If Not String.IsNullOrEmpty(txtPaymentType.Text) Then Return False
			If Not String.IsNullOrEmpty(txtBeneficiary.Text) Then Return False
			If Not String.IsNullOrEmpty(txtBranchNumber.Text) Then Return False
			If Not String.IsNullOrEmpty(txtAccountNumber.Text) Then Return False
			If Not String.IsNullOrEmpty(txtIdNumber.Text) Then Return False
			If ddlBank.SelectedIndex > 0 Then Return False
			Return True
		End Get
	End Property

	Public Sub SaveToMerchant(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		If nRefID > 0 Then
			Dim sbSQL As New StringBuilder("EXEC " & IIF(bIsMerchant, "SaveMerchantBankAccountLocal ", "SaveAffiliateBankAccountLocal ").ToString())
			sbSQL.AppendFormat("{0}, '{1}', '{2}',", nRefID, dbPages.DBText(PaymentType), dbPages.DBText(Beneficiary))
			sbSQL.AppendFormat("'{0}', '{1}', {2}", dbPages.DBText(BranchNumber), dbPages.DBText(AccountNumber), ddlBank.SelectedValue)
			dbPages.ExecSql(sbSQL.ToString)
		End If
	End Sub

	Public Sub SaveToWire(ByVal nWire As Integer)
		Dim sbSQL As New StringBuilder("EXEC SaveWireBankAccountLocal ")
		sbSQL.AppendFormat("{0}, '{1}', '{2}',", nWire, dbPages.DBText(PaymentType), dbPages.DBText(Beneficiary))
		sbSQL.AppendFormat("'{0}', '{1}', '{2}', '{3}'", dbPages.DBText(BranchNumber), dbPages.DBText(AccountNumber), dbPages.DBText(txtIdNumber.Text), ddlBank.SelectedValue)
		dbPages.ExecSql(sbSQL.ToString)
	End Sub

	Public Sub LoadFromMerchant(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		dsBank.ConnectionString = dbPages.DSN
		ddlBank.DataBind()
		ClearValues()
		If nRefID > 0 Then
			Dim drData As SqlDataReader = dbPages.ExecReader("EXEC " & IIF(bIsMerchant, "LoadMerchantBankAccountLocal ", "LoadAffiliateBankAccountLocal ") & nRefID)
			If drData.Read Then
				txtPaymentType.Text = IIF(drData("PaymentType") Is DBNull.Value, "", drData("PaymentType"))
				txtBeneficiary.Text = IIF(drData("Beneficiary") Is DBNull.Value, "", drData("Beneficiary"))
				txtBranchNumber.Text = IIF(drData("BranchNumber") Is DBNull.Value, "", drData("BranchNumber"))
				txtAccountNumber.Text = IIF(drData("AccountNumber") Is DBNull.Value, "", drData("AccountNumber"))
				If drData("Bank") IsNot DBNull.Value Then ddlBank.SelectedValue = drData("Bank")
			End If
			drData.Close()
		End If
	End Sub
	
	Public Sub LoadFromWire(ByVal nWire As Integer)
		dsBank.ConnectionString = dbPages.DSN
		ddlBank.DataBind()
		ClearValues()
		If nRefID > 0 Then
			Dim drData As SqlDataReader = dbPages.ExecReader("EXEC LoadWireBankAccountLocal " & nWire & ", NULL")
			If drData.Read Then
				txtPaymentType.Text = IIF(drData("PaymentType") Is DBNull.Value, "", drData("PaymentType"))
				txtBeneficiary.Text = IIF(drData("Beneficiary") Is DBNull.Value, "", drData("Beneficiary"))
				txtBranchNumber.Text = IIF(drData("BranchNumber") Is DBNull.Value, "", drData("BranchNumber"))
				txtAccountNumber.Text = IIF(drData("AccountNumber") Is DBNull.Value, "", drData("AccountNumber"))
				txtIdNumber.Text = IIF(drData("WireIdNumber") Is DBNull.Value, "", drData("WireIdNumber"))
				ddlBank.SelectedValue = drData("Bank")
			End If
			drData.Close()
		End If
	End Sub
</script>
<table border="0" cellpadding="1" cellspacing="0" class="formNormal">
	<tr>
		<td colspan="2">
			<div class="MerchantSubHead">Local Bank Account</div><br />
		</td>
	</tr>
	<tr>
		<td width="40%">Payment Type</td>
		<td><asp:TextBox ID="txtPaymentType" Width="230px" runat="server" /></td>
	</tr>
	<tr>
		<td>Beneficiary</td>
		<td><asp:TextBox ID="txtBeneficiary" Width="230px" runat="server" /></td>
	</tr>
	<tr>
		<td>Branch Number</td>
		<td><asp:TextBox ID="txtBranchNumber" Width="230px" runat="server" /></td>
	</tr>
	<tr>
		<td>Account Number</td>
		<td><asp:TextBox ID="txtAccountNumber" Width="230px" runat="server" /></td>
	</tr>
	<tr>
		<td>ID Number</td>
		<td><asp:TextBox ID="txtIdNumber" Width="230px" runat="server" /></td>
	</tr>
	<tr>
		<td valign="top">Bank</td>
		<td>
			<asp:DropDownList ID="ddlBank" DataSourceID="dsBank" DataTextField="Name" DataValueField="ID" Width="230px" runat="server" />
			<asp:SqlDataSource ID="dsBank" SelectCommand="SELECT '&lt; Select Bank &gt;' Name, 0 ID UNION SELECT Right('0'+convert(varchar(2), bankCode), 2)+' - '+bankName, ID FROM tblSystemBankList ORDER BY Name" runat="server" />
		</td>
	</tr>
</table>