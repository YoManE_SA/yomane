<%@ Page Language="VB" MasterPageFile="~/AdminCash/FiltersMaster.master" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DebitCompanies.ascx" TagName="FilterDebitCompanies" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = DateAdd("m", -1, Date.Now).ToShortDateString
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
        dsStatus.ConnectionString = dbPages.DSN
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <form runat="server" action="Refund_ask_data.asp" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
        <asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
        <input type="hidden" id="Source" name="Source" value="filter" />
        <input type="hidden" id="ShowCompanyID" name="ShowCompanyID" />

        <h1>
            <asp:Label ID="lblPermissions" runat="server" />
            Refund Requests<span> - Filter</span></h1>
        <table class="filterNormal">
            <tr>
                <td>
                    <Uc1:FilterMerchants ID="FMerchants" SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
				 INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1"
                        runat="server" />
                </td>
            </tr>
        </table>
        <table class="filterNormal" style="padding-top: 5px;">
            <tr>
                <th>Request Status
                </th>
            </tr>
            <tr>
                <td>
                    <asp:SqlDataSource runat="server" ID="dsStatus"
                        SelectCommand="SELECT * FROM GetGlobalData(63) ORDER BY Sign(GD_ID), Abs(Cast(CheckSum('#ff6666') AS bigint)-CheckSum(GD_Color)) DESC, GD_ID" />
                    <asp:Repeater runat="server" ID="repStatus" DataSourceID="dsStatus">
                        <ItemTemplate>
                            <span style="float: left; width: 109px; white-space: nowrap;">
                                <asp:Label runat="server" ID="lblColor" Text="&nbsp;&nbsp;" BackColor='<%#Drawing.ColorTranslator.FromHtml(Eval("GD_Color"))%>' ToolTip='<%# Eval("GD_Description") %>' />
                                <asp:Literal runat="server" ID="litCheckbox" Text='<%#"<input type=""checkbox"" name=""RefundAskStatus"" value=""" & Eval("GD_ID") & """" & IIf(Eval("GD_ID")=0, " checked=""checked""", String.Empty) & " /> " & Eval("GD_Text")%>' />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
        <table class="filterNormal" style="padding-top: 5px;">
            <tr>
                <th>Debit Terminal
                </th>
            </tr>
            <tr>
                <td>
                    <Uc1:FilterDebitCompanies ID="FilterDebitCompanies1" ClientFieldDebitCompany="DebitCompanyID"
                        ClientFieldDebitTerminal="TerminalNumber" runat="server" />
                </td>
            </tr>
        </table>
        <table class="filterNormal" style="padding-top: 5px;">
            <tr>
                <th colspan="2">Request Details
                </th>
            </tr>
            <tr>
                <td>Request Number
                </td>
                <td>
                    <input name="RequestNumber" style="width: 100px;" class="input1" />
                </td>
            </tr>
            <tr>
                <td>Original Trans. ID
                </td>
                <td>
                    <input name="OriginalTransID" style="width: 100px;" class="input1" />
                </td>
            </tr>
        </table>
        <table class="filterNormal" style="padding-top: 5px;">




            <tr>
                <td>
                    <Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" />
                </td>
            </tr>
        </table>
        <table class="filterNormal" style="padding-top: 5px;">
            <tr>
                <th colspan="2">Failed Attempts
                </th>
            </tr>
            <tr>
                <td>Reply Code
                </td>
                <td>
                    <input name="FailReplyCode" style="width: 100px;" class="input1" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
                <td>
                    <input type="checkbox" name="LastFailOnly" class="option" checked="checked" value="1" />
                    Last attempt only
                </td>
            </tr>
        </table>
        <table class="filterNormal" style="padding-top: 5px;">
            <tr>
                <th>Paging</th>
            </tr>
            <tr>
                <td align="right">
                    <select name="PageSize">
                        <option value="20" selected="selected">20 Rows</option>
                        <option value="50">50 Rows</option>
                        <option value="75">75 Rows</option>
                        <option value="100">100 Rows</option>
                    </select>
                </td>
                <td align="right">
                    <input type="submit" name="Action" value="SEARCH"><br />
                </td>
            </tr>
        </table>
    </form>

</asp:Content>
