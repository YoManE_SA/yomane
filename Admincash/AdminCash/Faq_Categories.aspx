<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsCategories.ConnectionString = dbPages.DSN
		dsLanguages.ConnectionString = dbPages.DSN
	End Sub

	Sub HideQuestions(Optional ByVal o As Object = Nothing, Optional ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs = Nothing)
		gvCategories.SelectedIndex = -1
		Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">parent.frmBody.location.href='common_blank.htm';</s" & "cript>")
	End Sub

	Sub SetDefault(o As Object, e As EventArgs)
		Dim ddl As DropDownList = CType(o, DropDownList)
		Dim nLanguage As Integer = dbPages.TestVar(CType(ddl.NamingContainer.FindControl("hidLanguageID"), HiddenField).Value, 1, 0, -1)
		If nLanguage >= 0 Then ddl.SelectedValue = nLanguage
	End Sub
	
	Sub SetDefaultTemplate(o As Object, e As EventArgs)
		Dim ddl As DropDownList = CType(o, DropDownList)
		Dim templateRestriction As String = CType(ddl.NamingContainer.FindControl("hidTemplateRestriction"), HiddenField).Value
		If Not string.IsNullOrEmpty(templateRestriction) Then ddl.SelectedValue = templateRestriction
	End Sub

	Sub UpdateCategory(o As Object, e As GridViewUpdateEventArgs)
		dsCategories.UpdateParameters("Language_id").DefaultValue = CType(CType(o, GridView).Rows(e.RowIndex).FindControl("ddlLanguage"), DropDownList).SelectedValue
		dsCategories.UpdateParameters("TemplateRestriction").DefaultValue = CType(CType(o, GridView).Rows(e.RowIndex).FindControl("ddlTemplate"), DropDownList).SelectedValue
	End Sub

	Sub CreateCategory(Optional ByVal o As Object = Nothing, Optional ByVal e As System.EventArgs = Nothing)
		If String.IsNullOrEmpty(txtNewCategory.Text) Then
			lblAddCategory.Text = "Please specify group name!"
			lblAddCategory.Visible = True
		Else
			lblAddCategory.Text = String.Empty
			lblAddCategory.Visible = False
			Dim nID As Integer = dbPages.ExecScalar("DECLARE @nID int;EXEC @nID=spAddQNAGroup " & ddlNewLanguage.SelectedValue & ", '" & dbPages.DBText(txtNewCategory.Text) & "';SELECT @nID;")
			txtNewCategory.Text = String.Empty
			gvCategories.DataBind()
            gvCategories.Sort("QNAGroup_id", System.Web.UI.WebControls.SortDirection.Descending)
			gvCategories.SelectedIndex = 0
			Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">parent.frmBody.location.href='faq_questions.aspx?CategoryID=" & nID & "';</s" & "cript>")
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>FAQ - Categories</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body class="menu" style="padding: 0px 18px;">

	<form id="frmServer" runat="server">
		<h1><asp:label id="lblPermissions" runat="server" /> Q&A <span>- Groups</span></h1><br />
		<fieldset style="text-align:left; padding:4px 8px;">
			<legend>ADD NEW GROUP</legend>
			<span style="float:right;">
				<input type="button" class="buttonWhite" value="Refresh" onclick="location.href='faq_categories.aspx';" />
			</span>
			<asp:TextBox ID="txtNewCategory" CssClass="text" runat="server" /> &nbsp;
			<asp:DropDownList ID="ddlNewLanguage" DataSourceID="dsLanguages" DataTextField="Name" DataValueField="Language_id" runat="server" />
			<asp:Button ID="btnAddCategory" CssClass="buttonWhite" runat="server" Text="Add" OnClick="CreateCategory" UseSubmitBehavior="false" />
			<asp:Label ID="lblAddCategory" runat="server" Visible="false" ForeColor="red" />
		</fieldset>
		<br />
		<asp:GridView ID="gvCategories" runat="server" AutoGenerateColumns="False" DataKeyNames="QNAGroup_id" DataSourceID="dsCategories" AllowPaging="False" AllowSorting="False"
		 SelectedRowStyle-CssClass="rowSelected" OnRowDeleted="HideQuestions" OnRowUpdating="UpdateCategory" Width="100%" CellSpacing="1" CellPadding="1"
			CssClass="gridforms" alternatingrowstyle-cssclass="gridRow2" rowstyle-cssclass="gridRow2" pagerstyle-cssclass="gridPaging" GridLines="None">
			<Columns>
				<%--<asp:BoundField DataField="QNAGroup_id" HeaderText="ID" ReadOnly="True" SortExpression="QNAGroup_id" />--%>

                <asp:TemplateField HeaderText="Group" HeaderStyle-CssClass="gridTitleLeft">
                     <ItemTemplate>
						<asp:HyperLink runat="server" Target="frmBody" Text='<%# Eval("Name") %>'  NavigateUrl='<%# String.Format("faq_questions.aspx?CategoryID={0}", Eval("QNAGroup_id")) %>' />
                     </ItemTemplate>
                     <EditItemTemplate>
						<asp:TextBox runat="server" Text='<%# Bind("Name") %>' ID="txtGroupName" />
                     </EditItemTemplate>
                </asp:TemplateField>
				<asp:TemplateField HeaderText="Language" ItemStyle-Width="150px" HeaderStyle-CssClass="gridTitleLeft">
					<ItemTemplate>
						<asp:Literal runat="server" Text='<%# Eval("LanguageName") %>' />
					</ItemTemplate>
					<EditItemTemplate>
						<asp:HiddenField ID="hidLanguageID" runat="server" Value='<%# dbPages.TestVar(Eval("Language_id"), 1, 0, -1) %>' />
						<asp:DropDownList OnDataBound="SetDefault" ID="ddlLanguage" DataSourceID="dsLanguages" DataTextField="Name" DataValueField="Language_id" runat="server" />
					</EditItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Template" ItemStyle-Width="50px" HeaderStyle-CssClass="gridTitleLeft">
					<ItemTemplate>
						<asp:Literal runat="server" Text='<%# Eval("TemplateRestriction") %>' />
					</ItemTemplate>
					<EditItemTemplate>
						<asp:HiddenField ID="hidTemplateRestriction" runat="server" Value='<%# Eval("TemplateRestriction") %>' />
						<asp:DropDownList OnDataBound="SetDefaultTemplate" ID="ddlTemplate" runat="server" >
							<asp:ListItem Value="" Text="All" />
							<asp:ListItem Value="Tmp_netpayintl" Text="Tmp_netpayintl" />
							<asp:ListItem Value="Tmp_paybytech" Text="Tmp_paybytech" />
							<asp:ListItem Value="Tmp_quicksell" Text="Tmp_quicksell" />
						</asp:DropDownList>
					</EditItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="Questions" HeaderText="Questions" ReadOnly="True" ItemStyle-CssClass="centered" HeaderStyle-CssClass="columnTitleVertical" />
				<asp:CheckBoxField DataField="IsVisible" HeaderText="Visible" SortExpression="IsVisible" HeaderStyle-CssClass="columnTitleVertical" />
				<asp:CheckBoxField DataField="IsMerchantCP" HeaderText="Merchant" SortExpression="IsMerchant" HeaderStyle-CssClass="columnTitleVertical" />
				<asp:CheckBoxField DataField="IsWallet" HeaderText="Wallet" SortExpression="IsCustomer" HeaderStyle-CssClass="columnTitleVertical" />
				<asp:CheckBoxField DataField="IsDevCenter" HeaderText="DevCenter" SortExpression="IsDeveloper" HeaderStyle-CssClass="columnTitleVertical" />
				<asp:CheckBoxField DataField="IsWebsite" HeaderText="Website" SortExpression="IsContent" HeaderStyle-CssClass="columnTitleVertical" />
				<asp:CommandField HeaderText="Edit" ButtonType="Button" ItemStyle-CssClass="buttons centered" ControlStyle-CssClass="buttonWhite" ShowDeleteButton="false" ShowEditButton="true" ShowSelectButton="false" HeaderStyle-CssClass="actions" />
				<asp:TemplateField HeaderText="Delete" ItemStyle-CssClass="buttons centered">
					<ItemTemplate>
						<asp:Button runat="server" ID="btnDelete" CommandName="Delete" CommandArgument='<%# Eval("QNAGroup_id") %>' CssClass="button buttonWhite buttonDelete" Text="Delete" OnClientClick="if (!confirm('Do you really want to delete that category ?!')) return false;" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</asp:GridView>
		<asp:SqlDataSource ID="dsCategories" runat="server" ProviderName="System.Data.SqlClient"
			SelectCommand="SELECT * FROM vwQNAGroup ORDER BY QNAGroup_id DESC" DeleteCommand="DELETE FROM dbo.QNAGroup WHERE QNAGroup_id = @QNAGroup_id"
			UpdateCommand="UPDATE dbo.QNAGroup SET Name=@Name, Language_id=@Language_id, TemplateRestriction= @TemplateRestriction, IsVisible=@IsVisible, IsMerchantCP=@IsMerchantCP, IsWallet=@IsWallet, IsDevCenter=@IsDevCenter, IsWebsite=@IsWebsite WHERE QNAGroup_id=@QNAGroup_id">
			<DeleteParameters>
				<asp:Parameter Name="QNAGroup_id" Type="Int32" />
			</DeleteParameters>
			<UpdateParameters>
				<asp:Parameter Name="Name" Type="String" ConvertEmptyStringToNull="false" DefaultValue="" />
				<asp:Parameter Name="Language_id" Type="Int32" />
				<asp:Parameter Name="TemplateRestriction" Type="String" ConvertEmptyStringToNull="true" />
				<asp:Parameter Name="IsVisible" Type="Boolean" />
				<asp:Parameter Name="IsMerchant" Type="Boolean" />
				<asp:Parameter Name="IsCustomer" Type="Boolean" />
				<asp:Parameter Name="IsDeveloper" Type="Boolean" />
				<asp:Parameter Name="IsContent" Type="Boolean" />
				<asp:Parameter Name="QNAGroup_id" Type="Int32" />
			</UpdateParameters>
		</asp:SqlDataSource>
		<asp:SqlDataSource ID="dsLanguages" runat="server" SelectCommand="SELECT Language_id, Name FROM [List].[LanguageList] ORDER BY Language_id" />
	</form>

</body>
</html>