<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Import namespace="System.Data"%>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>

<script runat="server">

	Const COLOR_BLOCK_MERCHANT As String = "<span style=""background-color:#6699cc;"">&nbsp;&nbsp;&nbsp;&nbsp;</span>"
	Const COLOR_BLOCK_SYSTEM As String = "<span style=""background-color:#cc99cc;"">&nbsp;&nbsp;&nbsp;&nbsp;</span>"
	Const COLOR_BLOCK_DOUBLE As String = "<span style=""background-color:#cc99cc;"">&nbsp;&nbsp;</span><span style=""background-color:#6699cc;"">&nbsp;&nbsp;</span>"
	Const COLOR_BLOCK_TEMPORARY As String = "<span style=""background-color:Green;"">&nbsp;&nbsp;&nbsp;&nbsp;</span>"
	Const COLOR_BLOCK_CCRM_TEMPORARY As String = "<span style=""background-color:#cc99cc;"">&nbsp;&nbsp;</span><span style=""background-color:Green;"">&nbsp;&nbsp;</span>"
	
	Sub RemoveRecords(ByVal o As Object, ByVal e As EventArgs)
		dbPages.ExecSql("DELETE FROM tblFraudCcBlackList WHERE fraudCcBlackList_id IN (" & txtRecords.Text & ")", dbPages.DSN)
		Response.Redirect("blacklist.aspx?" & Request.QueryString.ToString)
	End Sub

	Sub AddRecord(ByVal o As Object, ByVal e As EventArgs)
		Dim sNumber As String = dbPages.DBText(txtNewNumber.Text)
		Dim sComment As String = dbPages.DBText(txtNewComment.Text)
        Dim sSQL As String = _
        "INSERT INTO tblFraudCcBlackList (fcbl_ccDisplay," & _
        " fcbl_comment, fcbl_ccNumber256, PaymentMethod_id, fcbl_BlockLevel) VALUES (" & _
        " dbo.fnFormatCcNumToHideDigits('" & sNumber & "')," & _
        " '" & sComment & "'," & _
        " dbo.GetEncrypted256('" & sNumber & "')," & _
        " dbo.GetPaymentMethodCC('" & sNumber & "'), 1)"
		dbPages.ExecSql(sSQL)
		Dim sQueryString As String = Request.QueryString.ToString
		If sQueryString.Contains("id=0") Then
			sSQL = "SELECT Max(fraudCcBlackList_id) FROM tblFraudCcBlackList"
			sQueryString = "ID=" & dbPages.ExecScalar(sSQL)
		End If
		Response.Redirect("blacklist.aspx?" & sQueryString)
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)

		'if block/unblock button clicked, toggle the flag
		Dim nAddRemoveSystemBlock As Integer = dbPages.TestVar(Request("AddRemoveSystemBlock"), 1, 0, 0)
		If nAddRemoveSystemBlock > 0 Then
			Dim nBlock As Integer = dbPages.TestVar(dbPages.ExecScalar("Select fcbl_BlockLevel From tblFraudCcBlackList WHERE fraudCcBlackList_id=" & nAddRemoveSystemBlock), 0, -1, 256)
			If nBlock <= 1 Then _
			 dbPages.ExecSql("UPDATE tblFraudCcBlackList SET fcbl_BlockLevel=" & 1 - nBlock & " WHERE fraudCcBlackList_id=" & nAddRemoveSystemBlock)
		End If

		ddlNewMonth.Items.Add(String.Empty)
		For i As Integer = 1 To 12
			ddlNewMonth.Items.Add(i.ToString("00"))
		Next
		ddlNewYear.Items.Add(String.Empty)
		For i As Integer = Date.Now.Year To Date.Now.Year + 11
			ddlNewYear.Items.Add(i.ToString("0000"))
		Next
		If Not Page.IsPostBack Then
			txtNewNumber.Text = Request("CardNumber")
			Dim nNewMonth As Integer = dbPages.TestVar(Request("CardMonth"), 1, 12, 0)
			ddlNewMonth.SelectedIndex = nNewMonth
			Dim nNewYear As Integer = dbPages.TestVar(Request("CardYear"), 1, 0, 0)
			If nNewYear < 100 Then nNewYear += 2000
			For i As Integer = 0 To ddlNewYear.Items.Count - 1
				If dbPages.TestVar(ddlNewYear.Items(i).Value, 1, 0, -1) = nNewYear Then ddlNewYear.SelectedIndex = i
			Next
			CType(IIf(String.IsNullOrEmpty(txtNewNumber.Text), txtNewNumber, txtNewComment), TextBox).Focus()
		End If
		If Request("PageSize") <> String.Empty Then PGData.PageSize = Request("PageSize")
		If dbPages.TestVar(Request("ID"), 1, 0, -1) > -1 Then PGData.NoRecordsText = String.Empty
	End Sub
	
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Credit Card Blacklist</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	    .dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
</head>
<body onload="focus();">

	<iframe id="ifrDelete" style="display:none;"></iframe>
	<form id="frmMain" runat="server">
	<table style="width:95%;">
	<tr>
		<td id="pageMainHeadingTd">
			<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">CREDIT CARD BLACKLIST</span>
		</td>
		<td style="font-size:11px;text-align:right;">
			<%=COLOR_BLOCK_SYSTEM%> System
			<%=COLOR_BLOCK_MERCHANT%> Merchant
			<%=COLOR_BLOCK_DOUBLE%> System &amp; Merchant
			<%=COLOR_BLOCK_TEMPORARY%> Temporary
			<%=COLOR_BLOCK_CCRM_TEMPORARY%> Temporary CCRM
		</td>
	</tr>
	</table>
	<br />
	<script language="javascript" type="text/javascript">
		function AddRemoveRecord() {
			var nID=event.srcElement.value;
			if (event.srcElement.checked) {
				document.getElementById("txtRecords").value+=","+nID+",0";
			}
			else {
				var sRecords=document.getElementById("txtRecords").value;
				document.getElementById("txtRecords").value=sRecords.replace(","+nID+",0", "");
			}
		}
	</script>
	<span style="border:1px solid silver;padding:5px 3px;">
		<table class="formNormal" cellpadding="1" cellspacing="0" border="0" style="display:inline;">
		<tr>
			<td style="font:normal 11px;">Card Number</td>
			<td style="font:normal 11px;">Card Validity</td>
			<td style="font:normal 11px;">Comment</td>
			<td style="font:normal 11px;">&nbsp;</td>
		</tr>
		<tr>
			<td><asp:TextBox ID="txtNewNumber" runat="server" /></td>
			<td>
				<asp:DropDownList ID="ddlNewMonth" runat="server" />
				/
				<asp:DropDownList ID="ddlNewYear" runat="server" />
			</td>
			<td><asp:TextBox ID="txtNewComment" runat="server" Width="300" /></td>
			<td>
				<asp:Button ID="btnAdd" runat="server" Text=" ADD " OnClick="AddRecord" OnClientClick="if (document.getElementById('txtNewNumber').value==''||document.getElementById('ddlNewMonth').value==''||document.getElementById('ddlNewYear').value==''||document.getElementById('txtNewComment').value=='') {alert('Not all fields are filled!');return false;};return confirm('Do you really want to add this credit card to the blacklist ?!');" />
			</td>
		</tr>	
		</table>
	</span>
	<br />
	<br />
	<%
		Dim sWhere As String = String.Empty
	    Dim sSQL As String = "SELECT  fraudCcBlackList_id ,company_id ,tblFraudCcBlackList.PaymentMethod_id ,fcbl_ccDisplay, " & _
	 	"fcbl_comment , ISNULL(Name, '') AS Name ,ISNULL(CompanyName, '- SYSTEM -') AS CompanyName , " & _
		"fcbl_BlockLevel ,fcbl_InsertDate ,fcbl_UnblockDate ,fcbl_CCRMID ,fcbl_ReplyCode, fcbl_ccNumber256 " & _
		"FROM    tblFraudCcBlackList WITH ( NOLOCK ) " & _
	 	"LEFT JOIN List.PaymentMethod WITH ( NOLOCK ) ON tblFraudCcBlackList.PaymentMethod_id = List.PaymentMethod.PaymentMethod_id " & _
	 	"LEFT JOIN tblCompany WITH ( NOLOCK ) ON company_id = tblCompany.ID"
		If Request("BlockedBy") = "0" Then
			sWhere &= " AND company_id<=0"
		ElseIf Request("BlockedBy") = "1" Then
			If dbPages.TestVar(Request("MerchantID"), 1, 0, 0) > 0 Then
				sWhere &= " AND company_id=" & Request("MerchantID")
			Else
				sWhere &= " AND company_id>0"
			End If
		Else
			If dbPages.TestVar(Request("MerchantID"), 1, 0, 0) > 0 Then
				sWhere &= " AND company_id IN (0, " & Request("MerchantID") & ")"
			End If
		End If
		If Request("BlockType") = "1" Then
			sWhere &= " AND fcbl_BlockLevel<2"
		ElseIf Request("BlockType") = "2" Then
			sWhere &= " AND fcbl_BlockLevel=2"
		End If
		If dbPages.TestVar(Request("ID"), 1, 0, -1) > -1 Then sWhere &= " AND fraudCcBlackList_id=" & Request("ID")
	    If dbPages.TestVar(Request("CardPaymentMethod"), 1, 0, -1) > -1 Then sWhere &= " AND tblFraudCcBlackList.PaymentMethod_id=" & Request("CardPaymentMethod")
		If dbPages.TestVar(Request("Last4"), 1, 0, -1) > -1 Then sWhere &= " AND Replace(fcbl_ccDisplay,' ','') LIKE '%" & Request("Last4") & "'"
	    'If Request("SearchText") <> String.Empty Then sWhere &= " AND (fcbl_comment+' '+CompanyName+' '+Name LIKE '%" & dbPages.DBText(Request("SearchText")) & "%' OR fcbl_ccNumber256=dbo.GetEncrypted256('" & dbPages.DBText(Request("SearchText")) & "'))"
		If Request("ReplyCode") <> String.Empty Then sWhere &= " AND fcbl_ReplyCode LIKE '%" & dbPages.DBText(Request("ReplyCode")) & "%'"
		If Request("ShowActiveOnly") = "1" Then
			Dim sActiveOnDate As String = IIf(String.IsNullOrEmpty(Request("ActiveOnDate")), "GetDate()", "'" & dbPages.DBText(Request("ActiveOnDate")) & "'")
			sWhere &= " AND (fcbl_BlockLevel<2 OR " & sActiveOnDate & " BETWEEN fcbl_InsertDate AND fcbl_UnblockDate)"
		End If
		If Security.IsLimitedMerchant Then sWhere &= " AND (company_id=0 OR company_id IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "')))"
		If sWhere <> String.Empty Then sSQL &= " WHERE " & sWhere.Substring(4)
		sSQL &= " ORDER BY fraudCcBlackList_id DESC"
		PGData.OpenDataset(sSQL)
		Dim sCompanyName As String, sComment As String
		If PGData.iTable.Rows.Count > 0 Then
			%>
			<script language="javascript" type="text/javascript">
				function ExpandNode(nID)
				{
					var oImg = event.srcElement;
					oImg.src = (oImg.src.toLowerCase().indexOf("expand") > 0 ? "../images/tree_collapse.gif" : "../images/tree_expand.gif")
					var oIFrame = document.getElementById("ifrDetails" + nID);
					if (oIFrame.src == "common_blank.htm") oIFrame.src = "BlacklistDetails.aspx?color=ffffff&ID=" + nID;
					var oDetails = document.getElementById("trDetails" + nID)
					oDetails.style.display = (oDetails.style.display == "none" ? "" : "none");
				}
			</script>
			<table class="formNormal" style="width:95%;">
			<tr>
				<th style="padding-left:46px;">ID</th>
				<th>Merchant</th>
				<th>Credit Card</th>
				<th>Comment</th>
				<th style="text-align:center;">System Block</th>
				<th style="text-align:center;">Inserted On</th>
				<th style="text-align:center;">Expires On</th>
				<th style="text-align:right;">Reply</th>
				<th style="text-align:center;">Remove</th>
			</tr>
			<%
		End If
		Dim sColorBlock As String, bTemporary As Boolean
		While PGData.Read()
			sCompanyName = dbPages.dbtextShow(PGData("CompanyName"))
			If sCompanyName.Length > 20 Then sCompanyName = sCompanyName.Substring(0, 18) & "..."
			sComment = dbPages.dbtextShow(PGData("fcbl_comment"))
			If PGData("fcbl_BlockLevel") = 2 And String.IsNullOrEmpty(sComment) Then sComment = "Temporary block: debit terminal - 12 hrs"
			If sComment.Length > 30 Then sComment = sComment.Substring(0, 28) & "..."
			If PGData("fcbl_blockLevel") > 1 Then
				If PGData("fcbl_CCRMID") = 0 Then
					sColorBlock = COLOR_BLOCK_TEMPORARY
				Else
					sColorBlock = COLOR_BLOCK_CCRM_TEMPORARY
					sComment = "<a target=""_blank"" href=""merchant_dataRiskMng.asp?companyID=" & PGData("Company_ID") & "&CCRMID=" & PGData("fcbl_CCRMID") & """>" & sComment & "</a>"
				End If
			ElseIf PGData("company_id") <= 0 Then
				sColorBlock = COLOR_BLOCK_SYSTEM
			ElseIf PGData("fcbl_BlockLevel") = 1 Then
				sColorBlock = COLOR_BLOCK_DOUBLE
			Else
				sColorBlock = COLOR_BLOCK_MERCHANT
			End If
				bTemporary = (PGData("fcbl_blockLevel") > 1)
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
				<td style="white-space:nowrap;white-space:nowrap;">
					<img onclick="ExpandNode(<%=PGData("fraudCcBlackList_id")%>);" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" style="vertical-align:middle;" />
					<%=sColorBlock%>
					<%=PGData("fraudCcBlackList_id")%>
				</td>
				<td style="white-space:nowrap;" title="<%=dbPages.dbtextShow(PGData("CompanyName"))%>">
					<%=sCompanyName%>
				</td>
				<td style="white-space:nowrap;">
					<img src="/NPCommon/ImgPaymentMethod/23X12/<%=PGData("PaymentMethod_id")%>.gif" alt="<%=PGData("Name")%>" align="middle" />
					<%=PGData("fcbl_ccDisplay")%>
				</td>
				<td style="white-space:nowrap;" title="<%=dbPages.dbtextShow(PGData("fcbl_comment"))%>">
					<%=sComment%>
				</td>
				<td style="text-align:center;">
					<%
					If PGData("company_id") > 0 And PGData("fcbl_BlockLevel") <= 1 Then
						%>
						<img style="cursor:pointer;border:0;padding:0;" src="../images/<%= iif(PGData("fcbl_BlockLevel") = 1, "checkbox.gif", "checkbox_off.gif") %>" onclick="location.href='blacklist.aspx?<%= dbPages.SetUrlValue(Request.QueryString.Tostring, "AddRemoveSystemBlock", PGData("fraudCcBlackList_id")) %>';" alt="<%= iif(PGData("fcbl_BlockLevel") = 1, "Blocked, click to unblock", "Not blocked, click to block") %>" />
						<%
					End If
					%>
				</td>
				<td style="text-align:center;white-space:nowrap;">
					<%=dbPages.FormatDateTime(PGData("fcbl_InsertDate"))%>
				</td>
				<td style="text-align:center;white-space:nowrap;">
					<%=IIf(bTemporary, dbPages.FormatDateTime(PGData("fcbl_UnblockDate")), "---")%>
				</td>
				<td style="text-align:center;">
					<%=PGData("fcbl_ReplyCode")%>
				</td>
				<td style="text-align:center;">
					<input type="checkbox" onclick="AddRemoveRecord();" style="background-color:transparent;" value="<%=PGData("fraudCcBlackList_id")%>" />
				</td>
			</tr>
			<tr style="display:none;" id="trDetails<%=PGData("fraudCcBlackList_id")%>">
				<td></td>
				<td colspan="9" style="border-top:1px dashed silver;">
					<iframe id="ifrDetails<%=PGData("fraudCcBlackList_id")%>" border="0" frameborder="0" src="common_blank.htm" width="100%" height="20" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>
				</td>
			</tr>
			<tr><td height="1" colspan="10"  bgcolor="silver"></td></tr>
			<%
		End While
		If PGData.iTable.Rows.Count > 0 Then
			%>
			<tr>
				<td colspan="9"></td>
				<td style="text-align:center;">
					<span style="display:none;">
						<asp:TextBox ID="txtRecords" runat="server" Text="0" />
					</span>
					<asp:button ID="btnRemove" runat="server" Text="REMOVE" OnClick="RemoveRecords" OnClientClick="if (document.getElementById('txtRecords').value=='0') {alert('No record selected!');return false;};return confirm('Do you really want to remove the selected record(s) from the blacklist ?!');" />
				</td>
			</tr>
			</table>
			<%
		End If
		PGData.CloseDataset()
	%>
	</form>
	<table style="width:95%;">
	<tr>
		<td>
			<UC:Paging id="PGData" runat="Server" 
				ShowPageNumber="true" 
				ShowPageCount="true" 
				ShowRecordNumbers="true" 
				ShowRecordCount="true" 
				ShowPageFirst="true" 
				ShowPageLast="true" 
				NoRecordsText="No record found in blacklist. Refine the search." 
			/>
		</td>
	</tr>
	</table>
</body>
</html>