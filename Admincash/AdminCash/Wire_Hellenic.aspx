<%@ Page Language="VB" AspCompat="true" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    Sub SetContinueLink()
        hlContinue.NavigateUrl = "Wire_search_data.asp?"
        Dim bFirst As Boolean = True
        For Each sItem As String In Request.QueryString.Keys
            If bFirst Then
                bFirst = False
            Else
                hlContinue.NavigateUrl &= "&"
            End If
            hlContinue.NavigateUrl &= sItem & "=" & Request.QueryString(sItem)
        Next
    End Sub

    Sub SaveFile(ByVal o As Object, ByVal e As CommandEventArgs)
        Response.AddHeader("content-disposition", "attachment;filename=" & e.CommandArgument)
        Dim srTransfers As New System.IO.StreamReader(Server.MapPath("/Data/WireTransfer/Hellenic/" & e.CommandArgument))
        Response.Write(srTransfers.ReadToEnd)
        srTransfers.Close()
        srTransfers.Dispose()
        Response.End()
    End Sub

    Sub AddWireRecord(ByVal hfRecords As Hellenic.File, ByVal nWire As Integer, ByVal nCurrency As Integer)
        Dim sSQL As String = "SELECT * FROM tblWireMoney WHERE WireStatus=0 AND WireProcessingCurrency IN (1, 2, 3) AND WireMoney_ID=" & nWire
        Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
        drData.Read()
        Dim nAmount As Decimal = drData("wireAmount")
        If drData("wireCurrency") <> drData("wireProcessingCurrency") Then
            'sSQL = "SELECT CAST(c1.CUR_BaseRate AS money)/CAST(c2.CUR_BaseRate AS money) FROM tblSystemCurrencies c1, tblSystemCurrencies c2" & _
            '" WHERE c1.CUR_ID=" & drData("wireCurrency") & " AND c2.CUR_ID=" & drData("wireProcessingCurrency")
            'nAmount *= CType(dbPages.ExecScalar(sSQL), Decimal)
            nAmount *= drData("WireExchangeRate")
        End If
        litLog.Text &= "&nbsp; &nbsp; &nbsp; Amount:" & nAmount & "<br />"
        Dim bOrder As Boolean = (drData("WireType") = 2), bProfile As Boolean = False
        If bOrder Then
            sSQL = "SELECT CompanyMakePaymentsProfiles_ID FROM tblCompanyMakePaymentsRequests WHERE CompanyMakePaymentsRequests_id=" & drData("WireSourceTbl_ID")
            bProfile = (dbPages.ExecScalar(sSQL) > 0)
        End If
        Dim drData2 As SqlDataReader, drData3 As SqlDataReader
        If bOrder Then
            litLog.Text &= "&nbsp; &nbsp; &nbsp; Payment Order"
            drData2 = dbPages.ExecReader("SELECT * FROM tblCompanyMakePaymentsRequests WHERE CompanyMakePaymentsRequests_ID=" & drData("WireSourceTbl_ID"))
        Else
            litLog.Text &= "&nbsp; &nbsp; &nbsp; Transaction Payout"
            drData2 = dbPages.ExecReader("SELECT * FROM tblTransactionPay WHERE ID=" & drData("WireSourceTbl_ID"))
        End If
        drData2.Read()
        If bProfile Then
            litLog.Text &= " - to beneficiary<br />"
            sSQL = "SELECT LTrim(RTrim(bankAbroadBankAddressCity+' '+bankAbroadBankAddressZip+' '+bankAbroadBankAddressState)) bankAbroadBankAddressSecondLine," &
            " LTrim(RTrim(bankAbroadBankAddressCity2+' '+bankAbroadBankAddressZip2+' '+bankAbroadBankAddressState2)) bankAbroadBankAddressSecondLine2, tblCompanyMakePaymentsProfiles.*," &
            " IsNull(c1.Name, '') bankAbroadBankAddressCountryName, IsNull(c2.Name, '') bankAbroadBankAddressCountryName2 FROM tblCompanyMakePaymentsProfiles" &
            " LEFT JOIN [List].[CountryList] c1 ON bankAbroadBankAddressCountry=c1.CountryID LEFT JOIN [List].[CountryList] c2 ON bankAbroadBankAddressCountry2=c2.CountryID" &
            " WHERE CompanyMakePaymentsProfiles_ID=" & drData2("CompanyMakePaymentsProfiles_ID")
        Else
            litLog.Text &= " - self payment<br />"
            sSQL = "SELECT LTrim(RTrim(paymentAbroadBankAddressCity+' '+paymentAbroadBankAddressZip+' '+paymentAbroadBankAddressState)) paymentAbroadBankAddressSecondLine," &
            " LTrim(RTrim(paymentAbroadBankAddressCity+' '+paymentAbroadBankAddressZip+' '+paymentAbroadBankAddressState)) paymentAbroadBankAddressSecondLine2, tblCompany.*," &
            " LTrim(RTrim(City+' '+Zip+' '+State)) Address2, IsNull(c1.Name, '') paymentAbroadBankAddressCountryName, IsNull(c2.Name, '') paymentAbroadBankAddressCountryName2" &
            " FROM tblCompany LEFT JOIN [List].[CountryList] c1 ON paymentAbroadBankAddressCountry=c1.CountryID LEFT JOIN [List].[CountryList] c2 ON paymentAbroadBankAddressCountry2=c2.CountryID" &
            " WHERE tblCompany.ID=" & drData("Company_ID")
        End If
        drData3 = dbPages.ExecReader(sSQL)
        drData3.Read()
        Dim hrTransfer As New Hellenic.WorldwideRecord
        hrTransfer.DebitAccount = hfRecords.DebitAccount
        If nAmount = 0 Then
            litLog.Text &= "ERROR: ZERO AMOUNT !!!"
            Response.End()
        End If
        hrTransfer.TransferAmount = nAmount
        Select Case drData("wireProcessingCurrency")
            Case 1 : hrTransfer.TransferCurrency = Hellenic.Currency.USD
            Case 2 : hrTransfer.TransferCurrency = Hellenic.Currency.EUR
            Case 3 : hrTransfer.TransferCurrency = Hellenic.Currency.GBP
        End Select
        hrTransfer.SubmissionDate = hfRecords.SubmissionDate
        hrTransfer.Notes1 = "Wire ID " & nWire
        If bProfile Then
            hrTransfer.CreditAccount = drData3("bankAbroadAccountNumber")
            hrTransfer.BeneficiaryName = drData3("bankAbroadAccountName")
            hrTransfer.BeneficiaryAddress1 = drData3("basicInfo_address")
            hrTransfer.BeneficiaryAddress2 = String.Empty
            hrTransfer.BeneficiaryBankName = drData3("bankAbroadBankName")
            hrTransfer.BeneficiaryBankAddress1 = drData3("bankAbroadBankAddress")
            hrTransfer.BeneficiaryBankAddress3 = drData3("bankAbroadBankAddressSecondLine")
            hrTransfer.BeneficiaryBankAddress3 = drData3("bankAbroadBankAddressCountryName")
            hrTransfer.BeneficiaryBankSwiftCode = drData3("bankAbroadSwiftNumber")
            hrTransfer.SortingCode = drData3("bankAbroadSortCode")
            hrTransfer.ReceiverCorrespondentSwiftCode = drData3("bankAbroadSwiftNumber2")
            hrTransfer.ReceiverCorrespondentBankName = drData3("bankAbroadBankName2")
            hrTransfer.ReceiverCorrespondentAddress1 = drData3("bankAbroadBankAddress2")
            hrTransfer.ReceiverCorrespondentAddress2 = drData3("bankAbroadBankAddressSecondLine2")
            hrTransfer.ReceiverCorrespondentAddress3 = drData3("bankAbroadBankAddressCountryName2")
        Else
            hrTransfer.CreditAccount = drData3("paymentAbroadAccountNumber")
            hrTransfer.BeneficiaryName = drData3("paymentAbroadAccountName")
            hrTransfer.BeneficiaryAddress1 = drData3("Street")
            hrTransfer.BeneficiaryAddress2 = drData3("Address2")
            hrTransfer.BeneficiaryBankName = drData3("paymentAbroadBankName")
            hrTransfer.BeneficiaryBankAddress1 = drData3("paymentAbroadBankAddress")
            hrTransfer.BeneficiaryBankAddress3 = drData3("paymentAbroadBankAddressSecondLine")
            hrTransfer.BeneficiaryBankAddress3 = drData3("paymentAbroadBankAddressCountryName")
            hrTransfer.BeneficiaryBankSwiftCode = drData3("PaymentAbroadSwiftNumber")
            hrTransfer.SortingCode = drData3("paymentAbroadSortCode")
            hrTransfer.ReceiverCorrespondentSwiftCode = drData3("paymentAbroadSwiftNumber2")
            hrTransfer.ReceiverCorrespondentBankName = drData3("paymentAbroadBankName2")
            hrTransfer.ReceiverCorrespondentAddress1 = drData3("paymentAbroadBankAddress2")
            hrTransfer.ReceiverCorrespondentAddress2 = drData3("paymentAbroadBankAddressSecondLine2")
            hrTransfer.ReceiverCorrespondentAddress3 = drData3("paymentAbroadBankAddressCountryName2")
        End If
        hfRecords.AddRecord(hrTransfer)
        drData.Close()
        drData2.Close()
        drData3.Close()
    End Sub

    Sub ReadRecords(ByVal hfRecords As Hellenic.File)
        Dim nWire As Integer, nCurrency As Integer
        For Each sItem As String In Request.Form.Keys
            If sItem.StartsWith("wireAction") And Request.Form(sItem) = "7" Then
                nWire = dbPages.TestVar(sItem.Substring(10), 1, 0, 0)
                If nWire > 0 Then
                    litLog.Text &= "Hellenic: Wire " & nWire & "<br />"
                    nCurrency = dbPages.TestVar(Request.Form("currencyToUse" & nWire), 1, 0, -1)
                    If nCurrency >= 0 Then
                        litLog.Text &= "&nbsp; &nbsp; &nbsp; Currency: " & dbPages.GetCurText(nCurrency) & "<br />"
                        If nCurrency = eCurrencies.GC_USD Or nCurrency = eCurrencies.GC_EUR Or nCurrency = eCurrencies.GC_GBP Then
                            AddWireRecord(hfRecords, nWire, nCurrency)
                            litLog.Text &= "&nbsp; &nbsp; &nbsp; Record has been added successfully.</span><br />"
                            dbPages.ExecSql("UPDATE tblWireMoney SET wireStatus=7, wireStatusDate=GetDate() WHERE WireMoney_ID=" & nWire)
                        Else
                            litLog.Text &= "<span class=""errorMessage"">&nbsp; &nbsp; &nbsp; This currency is invalid for Hellenic!</span><br />"
                        End If
                    End If
                End If
            End If
        Next
    End Sub

    Sub Page_Load()
        If Not Page.IsPostBack Then
            litLog.Text = String.Empty
            SetContinueLink()
            Dim sSQL As String = dbPages.ExecScalar("SELECT IsNull(BA_AccountNumber, '') FROM tblBankAccounts WHERE BA_ID=" & dbPages.TestVar(Request.Form("HellenicAccount"), 1, 0, 0))
            'Dim sSQL As String = dbPages.ExecScalar("SELECT IsNull(AccountNumber, '') FROM [Finance].[WireAccount] WHERE WireAccount_id=" & dbPages.TestVar(Request.Form("HellenicAccount"), 1, 0, 0))
            Dim sAccount As String = sSQL
            If String.IsNullOrEmpty(sAccount) Then
                litLog.Text &= "<span class=""errorMessage"">No valid Hellenic account selected!</span> " & Request.Form("HellenicAccount") & "<br />"
                lbSave.Visible = False
            Else
                litLog.Text &= "Hellenic account: " & sAccount & "<br />"
                Dim nYear As Integer = dbPages.TestVar(Request.Form("HellenicYear"), Date.Today.Year, Date.Today.Year + 1, 0)
                Dim nMonth As Integer = dbPages.TestVar(Request.Form("HellenicMonth"), 1, 12, 0)
                Dim nDay As Integer = dbPages.TestVar(Request.Form("HellenicDay"), 1, 31, 0)
                If nYear = 0 Or nMonth = 0 Or nDay = 0 Then
                    litLog.Text &= "<span class=""errorMessage"">No valid date specified!</span> " & nDay & "/" & nMonth & "/" & nYear & "<br />"
                    lbSave.Visible = False
                Else
                    Dim dSubmission As New Date(nYear, nMonth, nDay)
                    If dSubmission < Date.Today Then
                        litLog.Text &= "<span class=""errorMessage"">Past date specified!</span> " & dSubmission.ToShortDateString & "<br />"
                        lbSave.Visible = False
                    Else
                        litLog.Text &= "Submission date: " & dSubmission.ToShortDateString & "<br />"
                        Dim hfTransfers As New Hellenic.File(Server.MapPath("/Data/WireTransfer/Hellenic"), , , sAccount, dSubmission)
                        ReadRecords(hfTransfers)
                        litLog.Text &= "All records have been read.<br />"
                        Dim sFile As String = hfTransfers.Save()
                        litLog.Text &= "File saved: " & sFile
                        lbSave.CommandArgument = sFile
                    End If
                End If
            End If
        End If
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Wire Hellenic</title>
	<link type="text/css" rel="stylesheet" href="../StyleSheet/StyleAdminNetEng.css" />
</head>
<body>
	<form runat="server">
		<div style="font-size:12px;">
			<asp:Literal ID="litLog" runat="server" />
			<hr />
			<asp:LinkButton ID="lbSave" OnCommand="SaveFile" CommandName="download" Text="Save File" runat="server" />
			<br /><br />
			<asp:HyperLink ID="hlContinue" Text="Continue" runat="server" />
		</div>
	</form>
</body>
</html>