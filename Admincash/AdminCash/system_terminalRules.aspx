<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">

	Sub CheckWasSent(ByVal nTerminal As Integer)
		Dim drData As SqlDataReader = dbPages.ExecReader("SELECT * FROM tblDebitTerminals WHERE ID=" & nTerminal)
		If drData.Read Then
            litSent.Text = "<img align=""middle"" src=""/NPCommon/ImgPaymentMethod/23X12/22.gif""> VISA: "
			If drData("dt_MonthlyCHBWasSent") Then
				litSent.Text &= "Last alert sent on " & dbPages.FormatDateTime(drData("dt_MonthlyCHBSendDate")) & " - No additional alerts will be sent this month"
				lbReset.Text = "Reset"
			Else
				litSent.Text &= "No alert was sent this month"
				lbReset.Text = String.Empty
			End If
            litSentMC.Text = "<img align=""middle"" src=""/NPCommon/ImgPaymentMethod/23X12/25.gif""> MC: "
			If drData("dt_MonthlyMCCHBWasSent") Then
				litSentMC.Text &= "Last alert sent on " & dbPages.FormatDateTime(drData("dt_MonthlyMCCHBSendDate")) & " - No additional alerts will be sent in this month"
				lbResetMC.Text = "Reset"
			Else
				litSentMC.Text &= "No alert was sent this month"
				lbResetMC.Text = String.Empty
			End If
		End If
		drData.Close()
	End Sub
	
	Sub SaveRecords(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		Dim sbSQL As New StringBuilder()
		Dim sMonthlyLimitCHBNotifyUsers As String = NPControls.GetSelectedItems(cblMonthlyLimitCHBNotifyUsers)
		Dim sMonthlyLimitCHBNotifyUsersSMS As String = NPControls.GetSelectedItems(cblMonthlyLimitCHBNotifyUsersSMS)
		Dim sMonthlyMCLimitCHBNotifyUsers As String = NPControls.GetSelectedItems(cblMonthlyMCLimitCHBNotifyUsers)
		Dim sMonthlyMCLimitCHBNotifyUsersSMS As String = NPControls.GetSelectedItems(cblMonthlyMCLimitCHBNotifyUsersSMS)
		sbSQL.Remove(0, sbSQL.Length).AppendFormat("UPDATE tblDebitTerminals SET dt_MonthlyLimitCHB={0}", dbPages.TestVar(txtMonthlyLimitCHB.Text, 1, 999999, 0))
		sbSQL.AppendFormat(", dt_MonthlyLimitCHBNotifyUsers='{0}'", dbPages.DBText(sMonthlyLimitCHBNotifyUsers))
		sbSQL.AppendFormat(", dt_MonthlyLimitCHBNotifyUsersSMS='{0}'", dbPages.DBText(sMonthlyLimitCHBNotifyUsersSMS))
		sbSQL.AppendFormat(", dt_MonthlyMCLimitCHB={0}", dbPages.TestVar(txtMonthlyMCLimitCHB.Text, 1, 999999, 0))
		sbSQL.AppendFormat(", dt_MonthlyMCLimitCHBNotifyUsers='{0}'", dbPages.DBText(sMonthlyMCLimitCHBNotifyUsers))
		sbSQL.AppendFormat(", dt_MonthlyMCLimitCHBNotifyUsersSMS='{0}'", dbPages.DBText(sMonthlyMCLimitCHBNotifyUsersSMS))
		sbSQL.AppendFormat(" WHERE ID={0}", dbPages.TestVar(Request("TerminalID"), 1, 0, 0))
		litMonthlyLimitCHBNotifyUsers.Text = sMonthlyLimitCHBNotifyUsers
		litMonthlyLimitCHBNotifyUsersSMS.Text = sMonthlyLimitCHBNotifyUsersSMS
		litMonthlyMCLimitCHBNotifyUsers.Text = sMonthlyMCLimitCHBNotifyUsers
		litMonthlyMCLimitCHBNotifyUsersSMS.Text = sMonthlyMCLimitCHBNotifyUsersSMS
		dbPages.ExecSql(sbSQL.ToString)
	End Sub

	Sub ResetNotification(ByVal o As Object, ByVal e As EventArgs)
		Dim nTerminal As Integer = dbPages.TestVar(Request("TerminalID"), 1, 0, 0)
		dbPages.ExecSql("UPDATE tblDebitTerminals SET dt_Monthly" & IIf(CType(o, Control).ID.EndsWith("MC"), "MC", String.Empty) & "CHBWasSent=0 WHERE ID=" & nTerminal)
		CheckWasSent(nTerminal)
	End Sub
	
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		If Not Page.IsPostBack Then
			Dim nTerminal As Integer = dbPages.TestVar(Request("TerminalID"), 1, 0, 0)
			If nTerminal > 0 Then
				litTerminalName.Text = dbPages.ExecScalar("SELECT dc_Name+' | '+TerminalNumber+' | '+dt_Name FROM tblDebitCompany" & _
				" INNER JOIN tblDebitTerminals ON DebitCompany_ID=DebitCompany WHERE ID=" & nTerminal)
				tlbTop.AddItem("Terminal Details", "system_terminalTermDetail.aspx?terminalID=" & nTerminal)
				tlbTop.AddItem("Card Company Details", "system_terminalCardDetail.aspx?terminalID=" & nTerminal)
				tlbTop.AddItem("Fees & Payouts", "system_terminalFees.aspx?terminalID=" & nTerminal)
				tlbTop.AddItem("Notifications", "system_terminalRules.aspx?TerminalID=" & nTerminal, True)
				tlbTop.AddItem("Miscellaneous", "system_terminalMisc.aspx?terminalID=" & nTerminal)
			Else
				litTerminalName.Text = "All Debit Companies"
			End If
			Dim drMonthly, drUsers As SqlDataReader
			drMonthly = dbPages.ExecReader("SELECT * FROM tblDebitTerminals WHERE ID=" & nTerminal)
			If drMonthly.HasRows then
				drMonthly.Read()
				If drMonthly("isActive") then
						ltIsActiveTerm.Text = "<span><li type=square style=color:#66cc66;> <span class=txt11>Active</span></li></span>"
				Else
						ltIsActiveTerm.Text = "<span><li type=square style=color:#ff6666;> <span class=txt11>Blocked</span></li></span>"
				End If
				CheckWasSent(nTerminal)
				litMonthlyLimitCHBNotifyUsers.Text = drMonthly("dt_MonthlyLimitCHBNotifyUsers")
				litMonthlyLimitCHBNotifyUsersSMS.Text = drMonthly("dt_MonthlyLimitCHBNotifyUsersSMS")
				litMonthlyMCLimitCHBNotifyUsers.Text = drMonthly("dt_MonthlyMCLimitCHBNotifyUsers")
				litMonthlyMCLimitCHBNotifyUsersSMS.Text = drMonthly("dt_MonthlyMCLimitCHBNotifyUsersSMS")
				drUsers = dbPages.ExecReader("SELECT su_Username ID, su_Username FROM tblSecurityUser WHERE su_MailEmergency<>'' AND su_IsActive=1 ORDER BY su_Username")
				NPControls.FillList(cblMonthlyLimitCHBNotifyUsers, drUsers, , , , Nothing)
				For Each liUser As ListItem In cblMonthlyLimitCHBNotifyUsers.Items
					liUser.Selected = (";" & litMonthlyLimitCHBNotifyUsers.Text & ";").Contains(";" & liUser.Value & ";")
				Next
				drUsers.Close()
				drUsers = dbPages.ExecReader("SELECT su_Username ID, su_Username FROM tblSecurityUser WHERE su_SMS<>'' AND su_IsActive=1 ORDER BY su_Username")
				NPControls.FillList(cblMonthlyLimitCHBNotifyUsersSMS, drUsers, , , , Nothing)
				For Each liUser As ListItem In cblMonthlyLimitCHBNotifyUsersSMS.Items
					liUser.Selected = (";" & litMonthlyLimitCHBNotifyUsersSMS.Text & ";").Contains(";" & liUser.Value & ";")
				Next
				drUsers.Close()
				drUsers = dbPages.ExecReader("SELECT su_Username ID, su_Username FROM tblSecurityUser WHERE su_MailEmergency<>'' AND su_IsActive=1 ORDER BY su_Username")
				NPControls.FillList(cblMonthlyMCLimitCHBNotifyUsers, drUsers, , , , Nothing)
				For Each liUser As ListItem In cblMonthlyMCLimitCHBNotifyUsers.Items
					liUser.Selected = (";" & litMonthlyMCLimitCHBNotifyUsers.Text & ";").Contains(";" & liUser.Value & ";")
				Next
				drUsers.Close()
				drUsers = dbPages.ExecReader("SELECT su_Username ID, su_Username FROM tblSecurityUser WHERE su_SMS<>'' AND su_IsActive=1 ORDER BY su_Username")
				NPControls.FillList(cblMonthlyMCLimitCHBNotifyUsersSMS, drUsers, , , , Nothing)
				For Each liUser As ListItem In cblMonthlyMCLimitCHBNotifyUsersSMS.Items
					liUser.Selected = (";" & litMonthlyMCLimitCHBNotifyUsersSMS.Text & ";").Contains(";" & liUser.Value & ";")
				Next
				drUsers.Close()
				txtMonthlyLimitCHB.Text = IIf(drMonthly("dt_MonthlyLimitCHB") > 0, drMonthly("dt_MonthlyLimitCHB"), String.Empty)
				txtMonthlyMCLimitCHB.Text = IIf(drMonthly("dt_MonthlyMCLimitCHB") > 0, drMonthly("dt_MonthlyMCLimitCHB"), String.Empty)
				litMonthlyCHB.Text = drMonthly("dt_MonthlyCHB")
				litMonthlyMCCHB.Text = drMonthly("dt_MonthlyMCCHB")
			End If
		End If
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Debit Terminal - Notifications</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="../js/func_common.js"></script>
	<style type="text/css">
		.dataHeading{text-decoration:underline;white-space:nowrap;}
		table.formNormal td, table.formNormal th {text-align:center;}
		.btnSave { width:58px; height:52px; }
	</style>
</head>
<body onload="focus();">

	<iframe id="ifrDelete" style="display:none;"></iframe>
	<form id="frmMain" runat="server" class="formNormal">
	
	<table align="center" width="95%">
	<tr>
		<td id="pageMainHeadingTd">
			<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">Notifications</span><span style="letter-spacing:-1px;"> - <asp:Literal ID="litTerminalName" runat="server" /></span>
		</td>
		<td>
			<table width="80" border="0" cellspacing="0" cellpadding="2" align="right" style="border:1px solid #c0c0c0;">
				<tr><td align="center" id="IsActiveTd"><asp:Literal ID="ltIsActiveTerm" runat="server" /><br /></td></tr>
			</table>
		</td>
	</tr>	
	</table>
	<br />
	<custom:Toolbar ID="tlbTop" Width="95%" runat="server" />
	<script language="javascript" type="text/javascript">
		function NextSibling(oNode)	{
			for(oNode=oNode.nextSibling;oNode && oNode.nodeType==3;oNode=oNode.nextSibling);
			return oNode;
		}
		function PreviousSibling(oNode)	{
			for(oNode=oNode.previousSibling;oNode && oNode.nodeType==3;oNode=oNode.previousSibling);
			return oNode;
		}
		
		function ExpandRecord()	{
			event.srcElement.style.display="none";
			NextSibling(event.srcElement).style.display="";
			NextSibling(event.srcElement.parentNode.parentNode).style.display="";
		}
		function CollapseRecord() {
			event.srcElement.style.display="none";
			PreviousSibling(event.srcElement).style.display="";
			NextSibling(event.srcElement.parentNode.parentNode).style.display="none";
		}
		function ConfirmDeleteRecord(nRuleID) {
			return confirm("Rule #"+nRuleID+" will be deleted from the system.\n\nDo you really want to delete this rule?");
		}
	</script>
	<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
	<tr>
		<td valign="top">
			<span class="MerchantSubHead">CHB Alert</span>
			<AC:DivBox Width="420px" runat="server">
				<ul>
					<li>Used for sending an alert when chb count for this terminal exceeds the number set in field "Monthly limit".</li>
					<li>When "Monthly limit" is not set, company level setting is used.</li>
					<li>If neither terminal level nor company level "Monthtly limit" is set, the alert is never sent.</li>
				</ul>
			</AC:DivBox>
	</tr>
	<tr><td height="5"></td></tr>
	<tr>
		<td>
			<table class="formNormal">
				<tr>
					<th colspan="2">Payment Method</th>
					<th>Monthly Limit</th>
					<th>Count</th>
					<th style="text-align:left;">Notify By Mail</th>
					<th style="text-align:left;">Notify By SMS</th>
				</tr>
				<tr>
					<td style="text-align:right;">
						<img onclick="ExpandRecord();" src="../images/tree_expand.gif" style="border:0;padding:0;margin:0 0 1px;vertical-align:middle;" alt="Click to expand" />
						<img onclick="CollapseRecord();" src="../images/tree_collapse.gif" style="display:none;border:0;padding:0;margin:0 0 1px;vertical-align:middle;" alt="Click to collapse" />
					</td>
					<td style="text-align:left;"><img align="middle" src="/NPCommon/ImgPaymentMethod/23X12/22.gif"> VISA</td>
					<td>
						<asp:TextBox ID="txtMonthlyLimitCHB" CssClass="medium" runat="server" />
						<asp:RangeValidator ControlToValidate="txtMonthlyLimitCHB" MinimumValue="0" MaximumValue="999999"
						 ErrorMessage="Monthly limit is invalid!" Display="none" SetFocusOnError="true" Type="Integer" runat="server" /> &nbsp;
					</td>
					<td><asp:Literal ID="litMonthlyCHB" runat="server" /></td>
					<td style="text-align:left;"><asp:Literal ID="litMonthlyLimitCHBNotifyUsers" runat="server" /></td>
					<td style="text-align:left;"><asp:Literal ID="litMonthlyLimitCHBNotifyUsersSMS" runat="server" /></td>
				</tr>
				<tr style="display:none;">
					<td></td>
					<td colspan="4" style="padding:5px 0 5px 10px;border-top:1px dashed Silver;text-align:left;">
						<div style="text-align:left;">
							<b>Notify by mail:</b>
							<asp:CheckBoxList ID="cblMonthlyLimitCHBNotifyUsers" CssClass="option" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server" />
						</div>
						<div style="text-align:left;">
							<b>Notify by SMS:</b>
							<asp:CheckBoxList ID="cblMonthlyLimitCHBNotifyUsersSMS" CssClass="option" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server" />
						</div>
					</td>
				</tr>
				<tr><td height="1" colspan="16" bgcolor="silver"></td></tr>
				<tr>
					<td style="text-align:right;">
						<img onclick="ExpandRecord();" src="../images/tree_expand.gif" style="border:0;padding:0;margin:0 0 1px;vertical-align:middle;" alt="Click to expand" />
						<img onclick="CollapseRecord();" src="../images/tree_collapse.gif" style="display:none;border:0;padding:0;margin:0 0 1px;vertical-align:middle;" alt="Click to collapse" />
					</td>
					<td style="text-align:left;"><img align="middle" src="/NPCommon/ImgPaymentMethod/23X12/25.gif"> MC</td>
					<td>
						<asp:TextBox ID="txtMonthlyMCLimitCHB" CssClass="medium" runat="server" />
						<asp:RangeValidator ControlToValidate="txtMonthlyMCLimitCHB" MinimumValue="0" MaximumValue="999999"
						 ErrorMessage="MonthlyMC limit is invalid!" Display="none" SetFocusOnError="true" Type="Integer" runat="server" /> &nbsp;
					</td>
					<td><asp:Literal ID="litMonthlyMCCHB" runat="server" /></td>
					<td style="text-align:left;"><asp:Literal ID="litMonthlyMCLimitCHBNotifyUsers" runat="server" /></td>
					<td style="text-align:left;"><asp:Literal ID="litMonthlyMCLimitCHBNotifyUsersSMS" runat="server" /></td>
				</tr>
				<tr style="display:none;">
					<td></td>
					<td colspan="4" style="padding:5px 0 5px 10px;border-top:1px dashed Silver;text-align:left;">
						<b>Notify by mail:</b>
						<asp:CheckBoxList ID="cblMonthlyMCLimitCHBNotifyUsers" CssClass="option" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server" />
						<br /><b>Notify by SMS:</b>
						<asp:CheckBoxList ID="cblMonthlyMCLimitCHBNotifyUsersSMS" CssClass="option" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server" />
					</td>
				</tr>
			</table>
			<asp:Button Text="Save Changes" OnClick="SaveRecords" UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" />
			<asp:ValidationSummary ShowMessageBox="true" ShowSummary="false" runat="server" />
		</td>
	</tr>
	</table>
	<br />
	<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
	<tr>
		<td valign="top" style="font-size:12px;">Alert status</td>
	</tr>
	<tr><td height="5"></td></tr>
	<tr>
		<td>
			<table class="formThin" width="100%">
			<tr>
				<td>
					<asp:Literal ID="litSent" runat="server" />
					<asp:LinkButton ID="lbReset" OnClick="ResetNotification" Font-Size="100%" runat="server" />
					<br />
					<asp:Literal ID="litSentMC" runat="server" />
					<asp:LinkButton ID="lbResetMC" OnClick="ResetNotification" Font-Size="100%" runat="server" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	</form>
	
</body>
</html>