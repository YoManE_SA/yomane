<%@ Page Language="VB" EnableEventValidation="false" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script runat="server">
		Dim sQueryString, sDivider As String
		Const RGB_PASS As String = "#66CC66", RGB_FAIL As String = "#ff6666"
		
		Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
			Security.CheckPermission(lblPermissions)
			If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") Else PGData.PageSize = 25

		End Sub
		
		Protected Sub Delete_Click(ByVal s As object, ByVal e As EventArgs)
			If Request("SelectedEvent") = "" Then Exit Sub
	        dbPages.ExecSql("Delete From EventPending Where EventPending_id IN(" & Request("SelectedEvent") & ")")
		End Sub

		Protected Sub Retry_Click(ByVal s As object, ByVal e As EventArgs)
			If Request("SelectedEvent") = "" Then Exit Sub
	        dbPages.ExecSql("Update EventPending Set TryCount=3 Where EventPending_id IN(" & Request("SelectedEvent") & ")")
		End Sub
		
		Protected Overrides Sub OnPreRender(e As System.EventArgs)
			MyBase.OnPreRender(e)
	        Dim sWhere As String = ""
			If Request("ID") <> "" Then
				sWhere &= " And EventPending.EventPending_id=" & dbPages.TestVar(Request("ID"), 0, -1, 0)
			ElseIf Request("TransID") <> "" Then
				sWhere &= " And "
				sWhere &= "(TransPass_id=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & _
				 " Or TransPreAuth_id=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & _
				 " Or TransPending_id=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & _
				 " Or TransFail_id=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & ")"
			Else
				If Request("fromDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And InsertDate >= '" & Request("fromDate") & " " & Request("fromTime") & "'"
				If Request("toDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And InsertDate <= '" & Request("toDate") & " " & Request("toTime") & "'"
				If Request("ddlEventType") <> "" Then sWhere &= " And EventPending.EventPendingType_id = " & Request("ddlEventType")
			End If
			'If Security.IsLimitedMerchant Then sWhere &= " AND tblCompany.ID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
			'If Security.IsLimitedDebitCompany Then sWhere &= " AND tblDebitCompany.DebitCompany_ID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
			If sWhere.Length > 0 Then sWhere = " Where " & sWhere.Substring(5)
			dbPages.CurrentDSN = IIf(Request("SQL2") = "1", 2, 1)
			PGData.OpenDataset("Select EventPending.*, EventPendingType.Name as EvName From EventPending " & _
			  "Left Join List.EventPendingType ON(EventPending.EventPendingType_id = EventPendingType.EventPendingType_id) " & _
			  sWhere & " Order By EventPending.EventPending_id Desc")
			
			sQueryString = dbPages.CleanUrl(Request.QueryString)
		End Sub
    </script>
</head>
<body>
	<form id="Form2" runat="server">
		<table align="center" style="width:92%">
		<tr>
			<td id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> PENDING EVENTS
				<span style="color:Black;font-weight:normal;"><%=IIf(Request("From") = "Top", " - Last Actions", String.Empty)%></span>
				<br /><br />
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%" cellpadding="1" cellspacing="0">
		<tr>
			<th>Event ID</th>
			<th>Event Date</th>
			<th>Trans ID</th>
			<th>Trans Source</th>
			<th>Merchant ID</th>
			<th>Customer ID</th>
			<th>Type</th>
			<th>Parameters</th>
			<th>Retries left</th>
		</tr>
		<%
		While PGData.Read()
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';" height="16">
				<td><%= PGData("EventPending_id")%></td>
				<td><%= PGData("InsertDate")%></td>
				<%	
					If PGData("TransPass_id") IsNot DBNull.Value Then
						Response.Write("<td>" & PGData("TransPass_id") & "</td><td>Pass</td>")
					ElseIf PGData("TransPreAuth_id") IsNot DBNull.Value Then
						Response.Write("<td>" & PGData("TransPreAuth_id") & "</td><td>Approval</td>")
					ElseIf PGData("TransPending_id") IsNot DBNull.Value Then
						Response.Write("<td>" & PGData("TransPending_id") & "</td><td>Pending</td>")
					ElseIf PGData("TransFail_id") IsNot DBNull.Value Then
						Response.Write("<td>" & PGData("TransFail_id") & "</td><td>Fail</td>")
					End If
				%>
				<td><%= PGData("Merchant_id")%></td>
				<td><%= PGData("Customer_id")%></td>
				<td><%= PGData("EvName")%></td>
				<td><%= PGData("Parameters")%></td>
				<td><%= IIf(PGData("TryCount") = 0, "<input type=""checkbox"" name=""SelectedEvent"" value=""" & PGData("EventPending_id") & """ />", PGData("TryCount"))%></td>
			</tr>
			<tr><td height="1" colspan="9"  bgcolor="#ffffff"></td></tr>
			<tr><td height="1" colspan="9"  bgcolor="silver"></td></tr>
			<tr><td height="1" colspan="9"  bgcolor="#ffffff"></td></tr>
			<%
		End While
		PGData.CloseDataset()%>
		</table>
		<br />
		<table align="center" style="width:92%">
		 <tr>
		  <td><UC:Paging runat="Server" id="PGData" PageID="PageID"  /></td>
		  <td align="right">
			<asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="Delete_Click" OnClientClick="return confirm('Are you sure ?!')" /> 
			&nbsp;&nbsp; 
			<asp:Button runat="server" ID="btnRetry" Text="Retry" OnClick="Retry_Click" />
		  </td>
		 </tr>
		</table>
	</form>
</body>
</html>