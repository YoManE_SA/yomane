<%@ Page Language="VB" %>
<script runat="server">
	Dim MerchantID as Integer
	Dim pTotals As New NPTotals	
	Dim EndDate As DateTime 
	Dim StartDate As DateTime 
	Dim DisplayMonth As String
	Dim nMailAddress As String, nMerchantName As String
	Dim bShowPaymentMethod As Boolean = True, bShowDaily As Boolean = True, bShowLast12 As Boolean = True
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Server.ScriptTimeout = 3000		
		If IsPostBack then			
			StartDate = New DateTime(RepYear.SelectedValue, RepMonth.SelectedValue, 1)
			EndDate = StartDate.AddMonths(1).AddDays(-1)			

			System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-us")
			DisplayMonth = StartDate.ToString("MMMM") & " " & StartDate.Year.ToString
			System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("he-il")
			bShowPaymentMethod = IIf(RepPaymentMethod.Checked,True,False)
			bShowDaily = IIf(RepDaily.Checked,True,False)
			bShowLast12 = IIf(RepLast12.Checked,True,False)
		End If
		
		If Request("SendMail") = "1" Then			
			StartDate = New DateTime(Request("RepYearHid"), Request("RepMonthHid"), 1)
			EndDate = StartDate.AddMonths(1).AddDays(-1)			

			System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-us")
			DisplayMonth = StartDate.ToString("MMMM") & " " & StartDate.Year.ToString
			System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("he-il")
			bShowPaymentMethod = IIf(Request("RepPaymentMethodHid")="1",True,False)
			bShowDaily = IIf(Request("RepDailyHid")="1",True,False)
			bShowLast12 = IIf(Request("RepLast12Hid")="1",True,False)
		End If

		MerchantID = dbPages.TestVar(Request("CompanyID"), 0, -1, 0)
		Dim iReader As System.Data.SqlClient.SqlDataReader = dbPages.ExecReader("Select CompanyName, ReportMail From tblCompany Where ID=" & MerchantID)
		If iReader.Read() Then
			nMerchantName = dbPages.TestVar(iReader("CompanyName"), -1, "")
			nMailAddress = dbPages.TestVar(iReader("ReportMail"), -1, "")
		End if
		iReader.Close()

		If Request("SendMail") = "1" Then 			
						
			SendEmail()
			Response.Write("SENT") : Response.End()
		End If
		If Not IsPostBack Then
			For i As Integer = 1 To 12
				RepMonth.Items.Add(i.ToString())
			Next
			For i As Integer = DateTime.Now.Year To 2002 Step -1 
				RepYear.Items.Add(i.ToString())
			Next
			RepYear.SelectedValue = StartDate.Year
			RepMonth.SelectedValue = StartDate.Month
		End if
	End Sub
	
	Private Sub SendEmail()
		Dim nBuffer As String = ""
		Dim txWriter As New System.IO.StringWriter()
				
		
		RenderData(txWriter)
		nBuffer = txWriter.GetStringBuilder.ToString()
		txWriter.Close()
		
		Dim nKeys as New System.Collections.Specialized.NameValueCollection()
		Dim sList() As String
		If Request("EmailTo") <> "" Then sList = New String() { Request("EmailTo") } _
		Else sList = nMerchantName.Split(vbLf)
		For i as Integer = 0 To sList.Length - 1
			sList(i) = sList(i).Replace(vbCr, "")
			If sList(i) <> "" Then 
				nKeys.Set("MailTo", sList(i))
				dbPages.SendEmail("Payment Report", nBuffer, Nothing, nKeys)
			End if	
		Next
	End Sub
	
	Private Sub RenderData(R As System.IO.TextWriter)				
		
   		Dim CurrencyText() As String = Nothing
		Dim nCurAvail(eCurrencies.MAX_CURRENCY) as Boolean 
		Dim nFlags As eSumType = eSumType.ePS_ShowFailed Or eSumType.ePS_ShowFees Or eSumType.ePS_ShowRolling Or eSumType.ePS_ByCHBDate
   		NetpayConst.GetCurrencyArray(CurrencyText)
		R.Write("<h2>" & nMerchantName & "</h2>")
		'R.Write("<tr><td colspan=""13"" style=""padding-left:5px;text-align:left;font-size:13px;""><br />Total Transactions By Currency</td></tr>")		
		pTotals.AddTransactions(StartDate, EndDate, nFlags, "", "", MerchantID, -1, "", "", "", Nothing, Nothing, Nothing, Nothing, Nothing, "Currency", Date.MinValue, Date.MaxValue)
		'pTotals.DrawData(R, nFlags, "", "", "", 0)
		For i As Integer = 0 To pTotals.Count - 1
			nCurAvail(pTotals(i).ItemID) = True 
		Next
		pTotals.Clear

		nFlags = nFlags Or eSumType.ePS_ShowTotal Or eSumType.ePS_DrawCols
		For i As Integer = 0 To 3
			If nCurAvail(i) Then
				If bShowPaymentMethod then
					R.Write("<table width=""100%"" border=""0"" class=""formNormal"" margin=""0"">")
					R.Write("<tr><td colspan=""13"" style=""padding-left:2px;text-align:left;font-size:15px;font-weight:bold;"">" & CurrencyText(i) & " (" & dbpages.GetCurText(i) & ")</td></tr>")				
					pTotals.AddTransactions(StartDate, EndDate, nFlags And Not eSumType.ePS_ShowRolling, i, "", MerchantID, -1, "", "", "", Nothing, Nothing, Nothing, Nothing, Nothing, "PaymentMethod", Date.MinValue, Date.MaxValue)
					if pTotals.Count > 0 Then
    					R.Write("<tr><td colspan=""16"" class=""SubTitle"" style=""text-align:left"">REPORT " & uCase(DisplayMonth) & " - BY PAYMENT METHOD</td></tr>")
						pTotals.DrawData(R, nFlags And Not eSumType.ePS_ShowRolling, "", "", "", 0)
					End if	
					pTotals.Clear
					R.Write("<tr><td><br /><br /></td></tr></table>")
				End If
				If bShowDaily then
					R.Write("<table width=""100%"" border=""0"" class=""formNormal"" margin=""0"">")
					nFlags = nFlags Or eSumType.ePS_ShowRolling
					pTotals.AddTransactions(StartDate, EndDate, nFlags, i, "", MerchantID, -1, "", "", "", Nothing, Nothing, Nothing, Nothing, Nothing, "InsDay", Date.MinValue, Date.MaxValue)
					if pTotals.Count > 0 Then
						R.Write("<tr><td colspan=""16"" class=""SubTitle"" style=""text-align:left"">DAILY REPORT OF " & uCase(DisplayMonth) &"</td></tr>")
						pTotals.DrawData(R, nFlags, "", "", "", 0)
					End if
					pTotals.Clear
					R.Write("<tr><td><br /><br /></td></tr>")
				End If
				If bShowLast12 then
					pTotals.AddTransactions(New DateTime(StartDate.Year, StartDate.Month, 1).AddYears(-1), New DateTime(StartDate.Year, StartDate.Month, 1).AddMonths(1).AddDays(-1), nFlags, i, "", MerchantID, -1, "", "", "", Nothing, Nothing, Nothing, Nothing, Nothing, "InsMonth", Date.MinValue, Date.MaxValue)
					if pTotals.Count > 0 Then
						R.Write("<tr><td colspan=""16"" class=""SubTitle"" style=""text-align:left"">LAST 12 MONTHS</td></tr>")
						pTotals.DrawData(R, nFlags, "", "", "", 0)
					End if
					pTotals.Clear
					R.Write("<tr><td><br /><br /><br /><br /></td></tr>")
				End If
			End if
		Next
		R.Write("</table><br />")		
	End Sub
	
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Merchant Report</title>
	<%--<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />--%>
	<style type="text/css">
		*{ font-family:Verdana,Arial,Helvetica,sans-serif; font-size:11px; }
		body { direction:ltr; margin-top:10px; }
		tr.totals { background-color:#E0E0E0; }
		tr.totals td {font-weight:bold; text-align:right; background-color:#E8E8E8; border-top:solid 2px; }
		span.security { font-weight:bold; font-size:12px; color:rgb(245, 245, 245); background-color:#2566AB; padding-left:3px; padding-right:3px; vertical-align:middle; }
		span.securityManaged{ cursor:pointer; }
		.dim {color:gray;}
		
		.formNormal Table { padding:0px; margin:0px; border:0px; direction:ltr; }
		.formNormal Th { background-color:#5B5B5B; padding-left:5px; font-size:11px; color:#ffffff; text-align:right; padding:0px 4px 0px 4px; border-bottom:1px solid gray; font-weight:bold; vertical-align:bottom; }
		.formNormal Td { font-size:11px; color:#000000; height:18px; text-align:right; padding:0px 4px 0px 4px; white-space:nowrap;}
		.formNormal Td.Numeric { font-size:11px; direction:ltr; white-space:nowrap; }
		.formNormal Td A{ color:#000000; font-size:11px; text-decoration:underline; }
		.formNormal Input { font-size:11px; color:#000000; background-color:#f5f5f5; }
		.formNormal Input[type='submit'] { font-size:11px; color:#000000; background-color:#e5e5e5; cursor:pointer; }
		.formNormal Input[type='button'] { font-size:11px; color:#000000; background-color:#e5e5e5; cursor:pointer; }
		.formNormal Select { font-size:11px; color:#000000; background-color:#f5f5f5; }
		
		.SubTitle { padding-left:0px!important; text-align:left!important; font-size:14px!important; }
		.RowZebra { background-color:#F3F3F3; }
	</style>
	<script type="text/javascript" language="javascript">
		function setDateValues() {
			document.getElementById("RepYearHid").value = document.getElementById("RepYear")[document.getElementById("RepYear").selectedIndex].value;
			document.getElementById("RepMonthHid").value = document.getElementById("RepMonth")[document.getElementById("RepMonth").selectedIndex].value;
			if (document.getElementById("RepPaymentMethod").checked) {
				document.getElementById("RepPaymentMethodHid").value = "1";
			}
			else {
				document.getElementById("RepPaymentMethodHid").value = "0";
			}

			if (document.getElementById("RepDaily").checked) {
				document.getElementById("RepDailyHid").value = "1";
			}
			else {
				document.getElementById("RepDailyHid").value = "0";
			}

			if (document.getElementById("RepLast12").checked) {
				document.getElementById("RepLast12Hid").value = "1";
			}
			else {
				document.getElementById("RepLast12Hid").value = "0";
			}
		}
	</script>
</head>
<body>
	<table class="formThin" width="100%" border="0" bgcolor="#F0F0F0">
	 <tr>
       <form id="Form1" name="Form1" runat="server" method="post">
	   <td nowrap valign="top">
	     Date
	     <asp:ListBox ID="RepMonth" runat="server" Rows="1" />
	     <asp:ListBox ID="RepYear" runat="server" Rows="1" />
		 <asp:CheckBox ID="RepPaymentMethod" Text="Show Payment Method" runat="server" />
		 <asp:CheckBox ID="RepDaily" Text="Show Daily" runat="server" />
		 <asp:CheckBox ID="RepLast12" Text="Show Last 12 Months" runat="server" />
	     <asp:button runat="server" Text="SHOW" UseSubmitBehavior="true" />
	   </td>
       </form> 
	   <td width="100%"></td>
       <form method="post" name="mailForm" onsubmit="setDateValues(this.form)">
	   <td nowrap valign="top">
	     Send to		 
         <input type="hidden" name="SendMail" value="1" />
		 <input type="hidden" name="RepYearHid" id="RepYearHid"  />
		 <input type="hidden" name="RepMonthHid" id="RepMonthHid" />
		 <input type="hidden" name="RepPaymentMethodHid" id="RepPaymentMethodHid" />
		 <input type="hidden" name="RepDailyHid" id="RepDailyHid" />
		 <input type="hidden" name="RepLast12Hid" id="RepLast12Hid" />
	     <input name="EmailTo" value="<%=dbPages.ExecScalar("Select su_Mail From tblSecurityUser Where su_Username='" & Security.Username & "'") %>">
	     <input type="submit" value="Send" />
	     (leave blank to send to merchant list)
	   </td>
       </form>
	 </tr>
	</table><br />
	<%If IsPostBack() Then RenderData(Response.Output)%>
</body>
</html>
