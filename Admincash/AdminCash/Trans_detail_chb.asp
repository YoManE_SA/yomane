<%
'20111106 Tamir - show chargeback/retrieval reason
If rsData("DeniedStatus") > 0 Then
	%>
	<tr>
		<td>
			<table>
			<tr>
				<td>
					<table>
					<%
					sSQL = "SELECT * FROM GetChargebackHistory(" & sTransID & ") ORDER BY TypeID DESC, InsertDate;"
					Set rsChb = oledbData.Execute(sSQL)
					Do until rsChb.EOF
						bExpandable = IIF(rsChb("Title") = "", False, True)
						%>
						<tr>
							<td style="padding-bottom:5px;">
								<table cellpadding="0" cellspacing="0">
								<tr>
									<td style="width:25px;">
										<%
										If bExpandable Then
											%>
											<script language="javascript" type="text/javascript">
												function ExpandNode(nID) {
													var bShow = (document.getElementById("tblDescription" + nID).style.display == "none");
													document.getElementById("tblDescription" + nID).style.display = (bShow ? "" : "none");
													document.getElementById("imgExpand" + nID).src = "../images/tree_" + (bShow ? "collapse" : "expand") + ".gif";
												}
											</script>
											<img onclick="ExpandNode(<%= rsChb("ID") %>);" style="cursor:pointer;" id="imgExpand<%= rsChb("ID") %>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" />
											<%
										Else
											%>
											<img src="../images/tree_disabled.gif" alt="" width="16" height="16" border="0" align="middle" />
											<%
										End If
										%>
									</td>
									<td>
										<b><%= IIf(rsChb("TypeID") = 4, "Chargeback", "Retrieval Request") %></b> | <%= Left(rsChb("InsertDate"), 10) %> | <%= rsChb("ReasonCode") %>
										<% If rsChb("Title") <> "" Then Response.Write(" " & rsChb("Title")) %>
									</td>
								</tr>
								<tr>
									<td></td>
									<td>
										<%
										If rsChb("Title") <> "" Then
											%>
											<table id="tblDescription<%= rsChb("ID") %>" style="display:none;">
												<tr>
													<td class="SmallHeadings" style="vertical-align:top;">Description: </td>
													<td><%= rsChb("Description") %></td>
												</tr>
												<tr>
													<td class="SmallHeadings" style="vertical-align:top;">Required Media: </td>
													<td><%= rsChb("RequiredMedia") %></td>
												</tr>
												<tr>
													<td class="SmallHeadings" style="vertical-align:top;">Refundability: </td>
													<td><%= rsChb("RefundInfo") %></td>
												</tr>
											</table>
											<%
										End If
										%>
									</td>
								</tr>
								</table>
							</td>
						</tr>
						<%
						rsChb.MoveNext
					Loop
					rsChb.Close
					set rsChb = Nothing

					'20100214 Tamir - show link to original and/or referred transaction
					If rsData("OriginalTransID") > 0 Or Len(rsData("RefTrans")) > 1 Then
						%>
						<tr>
							<td class="txt13" style="color:#505050; padding-bottom:5px;">
								<%
								If rsData("OriginalTransID")>0 Then
									%>
									Trans Original: 
									<a target="_parent" class="link" href="Trans_admin_regularData.aspx?transID=<%=rsData("OriginalTransID")%>"><%=rsData("OriginalTransID")%></a>
									<%
								End If
								If Len(rsData("RefTrans"))>1 Then
									nRefTrans=TestNumVar(Mid(rsData("RefTrans"), 2), 1, 0, 0)
									If nRefTrans>0 And nRefTrans-rsData("OriginalTransID")<>0 Then
										%>
										Referred Transaction:
										<a target="_parent" class="link" href="Trans_admin_regularData.aspx?transID=<%=nRefTrans%>"><%=rsData("RefTrans")%></a>
										<%
									End if
								End If 
								%>
							</td>
						</tr>
						<%
					End if	
					%>
					</table>

				</td>
			</tr>
			</table>
		</td>
	</tr>
	<%
End If
%>