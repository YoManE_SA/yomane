﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

public partial class Emails_TemplateBody : System.Web.UI.Page
{
	protected string getUserSignature(int mailboxId)
	{
		var mailbox = Netpay.Emails.MailBox.Load(mailboxId);
		if (mailbox != null) return mailbox.Signature;
		return null;
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		string templateValue = "";
		if (!string.IsNullOrEmpty(Request["template"])) templateValue = Netpay.Emails.Templates.GetTemplate(WebUtils.DomainHost, Request["template"]);
		else if (!string.IsNullOrEmpty(Request["signature"])) templateValue = getUserSignature(Request["signature"].ToInt32());
        Response.Write("<html><body contentEditable=\"true\">");
		Response.Write(templateValue);
		Response.Write("</body></html>");
    }
}