<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<%
if request("from")="menu" then
	ssTop="TOP 15"
else
	ssTop=""
end if

sWhere = " WHERE (tblWireMoney.isShow=1) "
sAnd = " AND "
if request("from")<>"menu" then
	if Trim(request("ShowCompanyID")) <> "" then
		sWhere=sWhere & sAnd & " (tblWireMoney.Company_id=" & trim(request("ShowCompanyID")) & ")" : sAnd=" AND "
	end if
	
	dDateMax = trim(request("toDate")) & " 23:59:59"
	dDateMin = trim(request("fromDate")) & " 00:00:00"
	if IsDate(dDateMax) then sWhere=sWhere & sAnd & " (tblWireMoney.WireDate<='" & CDate(dDateMax) & "')" : sAnd=" AND "
	if IsDate(dDateMin) then sWhere=sWhere & sAnd & " (tblWireMoney.WireDate>='" & CDate(dDateMin) & "')" : sAnd=" AND "

	if Trim(request("WireType"))<>"" then
		sWhere=sWhere & sAnd & " (tblWireMoney.WireType=" & trim(request("WireType")) & ")" : sAnd=" AND "
	end if
	
	if trim(request("wireStatus0"))<>"1" then
		sWhere = sWhere & sAnd & " (tblWireMoney.wireStatus<>0)" : sAnd=" AND "
	end if
	if trim(request("wireStatus1"))<>"1" then
		sWhere = sWhere & sAnd & " (tblWireMoney.wireStatus<>1)" : sAnd=" AND "
	end if	
	if trim(request("wireStatus2"))<>"1" then
		sWhere = sWhere & sAnd & " (tblWireMoney.wireStatus<>2)" : sAnd=" AND "
	end if	
	if trim(request("wireStatus3"))<>"1" then
		sWhere = sWhere & sAnd & " (tblWireMoney.wireStatus<3)" : sAnd=" AND "
	end if
	
	if trim(request("IdFrom"))<>"" then
		sWhere=sWhere & sAnd & " (tblWireMoney.WireMoney_id>=" & request("IdFrom") & ")" : sAnd=" AND "
	end if
	if trim(request("IdTo"))<>"" then
		sWhere=sWhere & sAnd & " (tblWireMoney.WireMoney_id<=" & request("IdTo") & ")" : sAnd=" AND "
	end if
	If trim(request("amountFrom"))<>"" Then sWhere=sWhere & sAnd & " (tblWireMoney.wireAmount>=" & request("amountFrom") & ")" : sAnd=" AND "
	If trim(request("amountTo"))<>"" Then sWhere=sWhere & sAnd & " (tblWireMoney.wireAmount<=" & request("amountTo") & ")" : sAnd=" AND "
end if

sSQL = "SELECT " & ssTop & " WireCurrency, WireAmount, WireExchangeRate FROM tblWireMoney " & sWhere
set rsData = oledbData.execute(sSQL)
sAmountTotal = 0
if not rsData.EOF then
	do until rsData.EOF
	
		If int(rsData("WireCurrency")) = 0 then
			sAmountTotal = sAmountTotal + rsData("WireAmount")
		Elseif int(rsData("WireCurrency")) = 1 then
			sAmountTotal = sAmountTotal + (rsData("WireAmount")*rsData("WireExchangeRate"))
		End if

	rsData.movenext
	loop					
end if

call rsData.close
Set rsData = nothing
call closeConnection()
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
</head>
<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<div align="center" class="txt14"><%= FormatCurrency(sAmountTotal,2,-1,0,-1) %><br /></div>
</body>
</html>