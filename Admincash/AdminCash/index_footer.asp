<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/security_class.asp"-->
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminIE.css" type="text/css" rel="stylesheet" />
	<style>.heading{color:#9BC1E9; margin:0px; padding:0px;}</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#484848">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="left" class="txt11" style="color:white;padding-top:2px;">
		<%
		sUsername = PageSecurity.Username
		If trim(Session("SecurityUserVerified"))<>"1" Then
            
			Set rsData=oledbData.Execute("SELECT * FROM tblSecurityUser WHERE su_Username='" & sUsername & "'")
			If rsData.EOF Then
				ExecSql("INSERT INTO tblSecurityUser(su_Username, su_IsActive) VALUES ('" & sUsername & "', 1)")
			    nSecUsrID = TestNumVar(ExecScalar("SELECT Max(ID) FROM tblSecurityUser WHERE su_Username='" & sUsername & "'", 0), 1, 0, 0)
                'ExecSql("INSERT INTO tblPasswordHistory(LPH_ID, LPH_RefID, LPH_RefType, LPH_Password256)Values(1, " & nSecUsrID & ",6, dbo.GetEncrypted256(newid()))")
			End if
			'sSQL="UPDATE p SET LPH_LastSuccess=GetDate() FROM tblSecurityUser u INNER JOIN tblPasswordHistory p ON u.ID=LPH_RefID WHERE LPH_RefType>=5 AND LPH_ID=1 AND su_Username='" & sUsername & "'"
			'oledbData.Execute sSQL
			rsData.Close
			Session("SecurityUserVerified")="1"
		End if
		sSQL = "GetSecurityGroups " & sUsername
		set rsData=oledbData.Execute(sSQL)
		sGroups = ""
		Do until rsData.EOF
			sGroups = sGroups & ", " & rsData(0)
			rsData.MoveNext
		Loop
		rsData.Close
		sGroups = IIf(Len(sGroups)>2,mid(sGroups, 3),"none")
		If PageSecurity.IsAdmin Then
			sAdminLink=" [ <a href=""common_framesetB.aspx?pageMenuWidth=50%&pageMenuUrl=security_users.aspx&pageBodyUrl="" target=""fraBrowser"" class=""left"">Manage Security</a> ]"
		Else
			sAdminLink=""
		End if
		sLimit = ""
		sLimitAfter = ""
		sLimitBefore = ""
		sLimitSeparator = ""
		If PageSecurity.IsLimitedMerchant Then
			sSQL = "SELECT CompanyName FROM tblCompany WHERE ID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
			set rsData=oledbData.Execute(sSQL)
			sMerchants = ""
			Do until rsData.EOF
				sMerchants = sMerchants & ", " & rsData(0)
				rsData.MoveNext
			Loop
			rsData.Close
			sMerchants = Replace(Mid(sMerchants, 3), """", "''")
			sLimit = sLimit & sLimitSeparator & "by <span class=""heading"" title=""" & sMerchants & """>merchant</span>"
			sLimitAfter = "."
			sLimitBefore = " &nbsp;&nbsp; Limited: "
			sLimitSeparator = ", "
		End If
		If PageSecurity.IsLimitedMerchant Then
			sSQL = "SELECT dc_Name FROM tblDebitCompany WHERE DebitCompany_ID IN (SELECT ID FROM GetPermittedDebitCompanies('" & PageSecurity.Username & "'))"
			set rsData=oledbData.Execute(sSQL)
			sDCs = ""
			Do until rsData.EOF
				sDCs = sDCs & ", " & rsData(0)
				rsData.MoveNext
			Loop
			rsData.Close
			sDCs = Replace(Mid(sDCs, 3), """", "''")
			sLimit = sLimit & sLimitSeparator & "by <span class=""heading"" title=""" & sDCs & """>debit company</span>"
			sLimitAfter = "."
			sLimitBefore = " &nbsp;&nbsp; Limited: "
			sLimitSeparator = ", "
		End If
		%>
		&nbsp;&nbsp;
		<span class="heading">IP:</span> <%= request.servervariables("REMOTE_ADDR") %> &nbsp;&nbsp;
		<span class="heading">User:</span> <%= sUsername %> &nbsp;&nbsp;
		<span class="heading">Groups:</span> <%= sGroups %> &nbsp;&nbsp;
		<%= sAdminLink %>
		<%= sLimitBefore & sLimit & sLimitAfter %>
	</td>
	<td align="right" class="txt11" style="color:white;padding-top:2px;padding-right:10px;">
		Admin Cash
	</td>
</tr>
</table>
</body>
</html>