<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
	'
	' 20080728 Tamir
	'
	' BIN numbers Management
	'
	Const NEW_BIN_TEXT As String = "< New BIN Number >"
	
	Sub AddConditionToSQL(ByRef sSQL As String, ByVal sCondition As String)
		sSQL &= IIf(sSQL.IndexOf("WHERE") > 0, " AND ", " WHERE ") & sCondition
	End Sub

	Function GetBINsFROMRequest() As Integer
		txaBIN.Text = Request("BIN")
		txaBIN.Text = txaBIN.Text.Replace("	", ",").Replace(vbCrLf, ",").Replace(";", ",")
		Do While txaBIN.Text.Contains(",,")
			txaBIN.Text = txaBIN.Text.Replace(",,", ",")
		Loop
		txaBIN.Text = txaBIN.Text.Replace(" ", String.Empty)
		Dim nCount As Integer
		txaBIN.Text = dbPages.TestNumericList(txaBIN.Text, nCount)
		Return nCount
	End Function
	
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsBINs.ConnectionString = dbPages.DSN
		Dim sSQL As String = "SELECT * FROM viewBIN"
		If Request("CardType") <> "" Then
			AddConditionToSQL(sSQL, "PaymentMethod IN (" & Request("CardType") & ")")
		End If
		If Request("Country") <> "" Then
			Dim sCountries As String = "'" & Request("Country").Replace(",", "','") & "'"
			AddConditionToSQL(sSQL, "isoCode IN (" & sCountries & ")")
		End If
		If Request("BIN") <> "" Then
			Dim nBINs As Integer = GetBINsFROMRequest()
			If nBINs > 0 Then
				If nBINs = 1 Then
					AddConditionToSQL(sSQL, "BIN LIKE '" & txaBIN.Text & "%'")
				Else
					AddConditionToSQL(sSQL, "BIN IN ('" & txaBIN.Text.Replace(",", "','") & "')")
				End If
			End If
		End If
		If IsPostBack Then Exit Sub
		dsBINs.SelectCommand = sSQL
		dsBINs.DataBind()
        Dim drData As SqlDataReader = dbPages.ExecReader("SELECT CountryISOCode, Name FROM [List].[CountryList] ORDER BY Name")
		ddlCountry.Items.Add(New ListItem("< Select Country >", String.Empty))
		Do While drData.Read()
			ddlCountry.Items.Add(New ListItem(drData(1), drData(0)))
		Loop
		drData.Close()
		
		drData = dbPages.ExecReader("SELECT Abbreviation , Name FROM List.PaymentMethod AS pm WHERE PaymentMethod_id BETWEEN " & ePaymentMethod.PMD_CC_MIN & " AND " & ePaymentMethod.PMD_CC_MAX & " AND PaymentMethod_id<>" & ePaymentMethod.PMD_CC_UNKNOWN & " ORDER BY Name")
		ddlCardType.Items.Add(New ListItem("< Card Type >", String.Empty))
		Do While drData.Read()
			ddlCardType.Items.Add(New ListItem(drData(1), drData(0)))
		Loop
		drData.Close()
		
		If Not Page.IsPostBack Then txtNewBIN.Text = NEW_BIN_TEXT
	End Sub

	Sub btnNewBIN_OnClick(ByVal sender As Object, ByVal e As EventArgs)
		If dbPages.TestVar(txtNewBIN.Text, 1000, 99999999999999, 0) = 0 Then
			lblNewBIN.Text = "Invalid BIN specified!"
		ElseIf ddlCountry.SelectedIndex = 0 Then
			lblNewBIN.Text = "Country not specified!"
		ElseIf ddlCardType.SelectedIndex = 0 Then
			lblNewBIN.Text = "Card type not specified!"
		Else
			'Response.Write(ddlCardType.SelectedValue)
			Dim nRC As Integer = dbPages.ExecScalar("DECLARE @nRC int;EXEC @nRC=InsertBIN '" & txtNewBIN.Text & "', '" & ddlCountry.SelectedValue & "', '" & ddlCardType.SelectedValue & "';SELECT @nRC;")
			'Response.Write(nRC)
			Select Case nRC
				Case 0
					lblNewBIN.Text = "BIN number " & txtNewBIN.Text & " successfully added."
					dbPages.ExecSql("UPDATE tblCreditCardBIN SET ImportDate=NULL WHERE BIN='" & txtNewBIN.Text & "'")
					gvBINs.DataBind()
				Case -1 : lblNewBIN.Text = "Failed - null BIN"
				Case -2 : lblNewBIN.Text = "Failed - empty BIN"
				Case -3 : lblNewBIN.Text = "Failed - null country"
				Case -4 : lblNewBIN.Text = "Failed - invalid country: " & ddlCountry.SelectedValue
				Case -5 : lblNewBIN.Text = "Failed - null card type"
				Case -6 : lblNewBIN.Text = "Failed - invalid card type: " & ddlCardType.SelectedValue
				Case -7 : lblNewBIN.Text = "Failed - country not found: " & ddlCountry.SelectedValue
				Case -8 : lblNewBIN.Text = "Failed - card type not found: " & ddlCardType.SelectedValue
				Case -9 : lblNewBIN.Text = "Failed - already exists: " & txtNewBIN.Text
				Case Else : lblNewBIN.Text = "Failed - unspecified error"
			End Select
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Merchant Status</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<form action="" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> BIN Numbers</h1><br />
		<br />
		<div class="bordered">
			Add BIN &nbsp;
			<asp:DropDownList ID="ddlCountry" runat="server" />
			<asp:DropDownList ID="ddlCardType" runat="server" />
			<asp:TextBox ID="txtNewBIN" runat="server" />
			<asp:Button ID="btnNewBIN" runat="server" Text=" Add " CssClass="buttonWhite" Enabled="false" OnClick="btnNewBIN_OnClick" />
			<asp:Label ID="lblNewBIN" runat="server" />
			<script language="javascript" type="text/javascript">
				function EnableDisableNewBIN()
				{
					document.getElementById("btnNewBIN").disabled=true;
					if (document.getElementById("txtNewBIN").value=="<%= NEW_BIN_TEXT %>") return;
					if (document.getElementById("txtNewBIN").value.length<4) return;
					if (document.getElementById("ddlCountry").selectedIndex==0) return;
					if (document.getElementById("ddlCardType").selectedIndex==0) return;
					document.getElementById("btnNewBIN").disabled=false;
				}
				function NewBINFocus()
				{
					if (event.srcElement.value=="<%= NEW_BIN_TEXT %>") event.srcElement.value="";
				}
				function NewBINBlur()
				{
					if (event.srcElement.value=="") event.srcElement.value="<%= NEW_BIN_TEXT %>";
					EnableDisableNewBIN();
				}
				function addEvent(obj, strEvent, objFunction)
				{ 
					if (obj.addEventListener)
					{ 
						obj.addEventListener(strEvent, objFunction, true); 
						return true; 
					}
					else if (obj.attachEvent)
					{ 
						var returnValue = obj.attachEvent("on"+strEvent, objFunction); 
						return returnValue; 
					} 
					else return false; 
				}
				addEvent(document.getElementById("txtNewBIN"), 'focus', NewBINFocus);
				addEvent(document.getElementById("txtNewBIN"), 'blur', NewBINBlur);
				addEvent(document.getElementById("txtNewBIN"), 'keyup', EnableDisableNewBIN);
				addEvent(document.getElementById("ddlCountry"), 'blur', EnableDisableNewBIN);
				addEvent(document.getElementById("ddlCountry"), 'change', EnableDisableNewBIN);
				addEvent(document.getElementById("ddlCardType"), 'blur', EnableDisableNewBIN);
				addEvent(document.getElementById("ddlCardType"), 'change', EnableDisableNewBIN);
			</script>
		</div>
		<div style="display:none;">
			<asp:TextBox ID="txaBIN" runat="server" TextMode="MultiLine" CssClass="filter" />
		</div>
		<iframe id="ifrDelete" src="common_blank.htm" style="display:none;"></iframe>
		<script language="javascript" type="text/javascript">
			function ConfirmAndDeleteBIN(nID)
			{
				if (!confirm("Are you sure ?!")) return;
				if (!confirm("The BIN number will be deleted from the system!\n\nThis operation cannot be undone!\n\nDo you really want to delete the BIN number ?!")) return;
				ifrDelete.location.replace("System_BinGo.aspx?BIN=<%= txaBin.Text %>&Country=<%= Request("Country") %>&CardType=<%= Request("CardType") %>&DeleteBIN="+nID);
			}
		</script>
		<asp:GridView ID="gvBINs" runat="server" PagerStyle-CssClass="gridPaging" PagerSettings-PageButtonCount="50"
			PagerSettings-Mode="NumericFirstLast" PageSize="15" AutoGenerateColumns="False" DataKeyNames="BIN" DataSourceID="dsBINs"
			AllowPaging="True" CssClass="grid" SelectedRowStyle-CssClass="rowSelected" Width="100%">
			<SelectedRowStyle CssClass="rowSelected" />
			<PagerSettings Mode="NumericFirstLast" PageButtonCount="20" />
			<PagerStyle HorizontalAlign="Center" CssClass="gridPaging" />
			<Columns>
				<asp:BoundField DataField="BIN" HeaderText="BIN" ItemStyle-HorizontalAlign="Center" ReadOnly="true" />
				<asp:TemplateField HeaderText="Country" ItemStyle-HorizontalAlign="Center">
					<ItemTemplate>
						<asp:Literal Text='<%# Eval("isoCode") %>' runat="server" />
					</ItemTemplate>
					<EditItemTemplate>
						<asp:TextBox ID="txtCountry" Text='<%# Bind("isoCode") %>' CssClass="grayBG short" runat="server" />
					</EditItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="CCName" ItemStyle-HorizontalAlign="Center" HeaderText="Card Type" ReadOnly="true" />
				<asp:BoundField DataField="ImportDate" ItemStyle-HorizontalAlign="Center" HeaderText="Import Date" ReadOnly="true" />
				<asp:BoundField DataField="InsertDate" ItemStyle-HorizontalAlign="Center" HeaderText="Insert Date" ReadOnly="true" />
				<asp:BoundField DataField="DeleteButton" HeaderText="Delete" HtmlEncode="false" ItemStyle-HorizontalAlign="Center" ReadOnly="true" />
				<asp:CommandField HeaderText="Edit" ShowEditButton="true" ButtonType="Button" ControlStyle-CssClass="buttonWhite" ItemStyle-HorizontalAlign="Center" />
			</Columns>
		</asp:GridView>
		<asp:SqlDataSource ID="dsBINs" runat="server" ProviderName="System.Data.SqlClient"
			SelectCommand="SELECT * FROM viewBIN ORDER BY BIN"
			UpdateCommand="UPDATE tblCreditCardBIN SET isoCode=IsNull(@isoCode, '--') WHERE BIN=@BIN"
			DeleteCommand="DELETE FROM tblCreditCardBIN WHERE BIN=@BIN">
			<UpdateParameters>
				<asp:Parameter Name="isoCode" Type="String" ConvertEmptyStringToNull="true" />
				<asp:Parameter Name="BIN" Type="String" />
			</UpdateParameters>
			<DeleteParameters>
				<asp:Parameter Name="BIN" Type="String" />
			</DeleteParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>