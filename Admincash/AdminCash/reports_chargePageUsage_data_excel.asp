<%
Server.ScriptTimeout = 6000
response.buffer = true
response.ContentType = "application/vnd.ms-excel"
response.AddHeader "content-disposition", "inline; filename=netpay_report.xls"
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_controls.asp"-->
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
	<style type="text/css">
		table {mso-displayed-decimal-separator:"\."; mso-displayed-thousand-separator:"\,";}
		@page {margin:1.0in .75in 1.0in .75in; mso-header-margin:.5in;	mso-footer-margin:.5in;}
		tr {mso-height-source:auto;}
		col {mso-width-source:auto;}
		br {mso-data-placement:same-cell;}
		.txt11 { font-size:11px; font-family: Verdana,Arial, Helvetica, sans-serif; color:black; }
		.txt11b { font-size:11px; font-family: Arial,Verdana, Helvetica, sans-serif; color:black; font-weight:bold; }
		.txt12 { font-size:12px; font-family: Verdana,Arial, Helvetica, sans-serif; color:black; }
		.txt12b { font-size:12px; font-family: Verdana,Arial, Helvetica, sans-serif; color:black; font-weight:bold; }
		.txt13 { font-size:13px; font-family: Verdana,Arial, Helvetica, sans-serif; color:black; }
		.txt13b { font-size:13px; font-family: Verdana,Arial, Helvetica, sans-serif; color:black; font-weight:bold; }
		.txt16b { font-size:16px; font-family: Verdana,Arial, Helvetica, sans-serif; color:black; font-weight:bold; }
	</style>
</head>
<body>
<%
sPageType = dbText(request("iPageType"))
sCurrency = dbText(request("iCurrency"))
sCcTypeID = dbText(request("iCcTypeID"))

sFromDate = dbText(request("iFromDate"))
sFromDateShow=""
if day(sFromDate)<10 then sFromDateShow=sFromDateShow & "0" end if
sFromDateShow=sFromDateShow & day(sFromDate) & "/"
if month(sFromDate)<10 then sFromDateShow=sFromDateShow & "0" end if
sFromDateShow=sFromDateShow & month(sFromDate) & "/" & right(year(sFromDate),2)

sToDate = dbText(request("iToDate"))
sToDateShow=""
if day(sToDate)<10 then sToDateShow=sToDateShow & "0" end if
sToDateShow=sToDateShow & day(sToDate) & "/"
if month(sToDate)<10 then sToDateShow=sToDateShow & "0" end if
sToDateShow=sToDateShow & month(sToDate) & "/" & right(year(sToDate),2)	
%>
<table border="0" dir="ltr" cellpadding="1" cellspacing="0">
<tr>
	<td colspan="11" nowrap="nowrap" align="left" dir="ltr">
		<font size="+1" color="#FF8000"><strong>Charge page usage report</strong></font>
	</td>
</tr>
<tr>
	<td colspan="9" nowrap="nowrap" align="left" dir="ltr">
		<%
		response.write "Charge page: "
		
		response.write "From- " & sFromDateShow
		response.write "To- " & sToDateShow
		%>
	</td>
</tr>
<tr>
	<td colspan="11" nowrap="nowrap" align="left" dir="ltr">
		<%
		response.write "Charge page: "
		
		sSQL="SELECT Name From List.TransSource WHERE List.TransSource.TransSource_id IN (" & sPageType & ")"
		set rsData2 = oledbData.execute(sSQL)
		sCount = 0
		do until rsData2.EOF
			If sCount>0 Then response.write " ,"
			response.write dbTextShow(rsData2("Name"))
			sCount = sCount+1
		rsData2.movenext
		loop
		%>
	</td>
</tr>
<tr>
	<td colspan="11" nowrap="nowrap" align="left" dir="ltr">
		<%
		response.write "Card Type: "
		
		sSQL="SELECT PaymentMethod_id AS ID, Name AS pm_Name FROM List.PaymentMethod AS pm WHERE PaymentMethod_id IN (" & sCcTypeID & ")"
		set rsData3 = oledbData.execute(sSQL)
		sCount = 0
		do until rsData3.EOF
			If sCount>0 Then response.write " ,"
			response.write dbTextShow(rsData3(1))
			sCount = sCount+1
		rsData3.movenext
		loop
		%>
	</td>
</tr>
<tr>
	<td colspan="11" nowrap="nowrap" align="left" dir="ltr">
		<%
		Select Case sCurrency
			Case "0" response.write "Currency: ILS"
			Case "1" response.write "Currency: USD"
			Case "0,1" response.write "Currency: ILS, USD"
		End Select
		%>
	</td>
</tr>
<tr><td></td></tr>
<tr><td></td></tr>
<%
sSQL="SELECT COUNT(tblCompanyTransPass.companyID) AS NumOfTrans, tblCompanyTransPass.companyID, tblCompany.CompanyName, tblCompany.Url, tblCompany.CustomerNumber, tblCompany.FirstName, tblCompany.LastName, tblCompany.Phone, tblCompany.cellular, tblCompany.mail, tblCompany.ActiveStatus, tblCompany.Country, tblCompany.CompanyPhone, tblCompany.CompanyFax, tblCompany.CompanyCountry, tblCompany.CompanyLegalNumber, tblCompany.comment " &_
"FROM tblCompanyTransPass INNER JOIN tblCompany ON tblCompanyTransPass.companyID = tblCompany.ID LEFT OUTER JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID " &_
"WHERE tblCreditCard.ccTypeID IN (" & sCcTypeID & ") " &_
"AND tblCompanyTransPass.Currency IN (" & sCurrency & ") " &_
"AND tblCompanyTransPass.InsertDate >= '" & CDate(sFromDate) & "' " &_
"AND tblCompanyTransPass.InsertDate <= '" & CDate(sToDate) & "' " &_
"AND tblCompanyTransPass.TransSource_id IN (" & sPageType & ") " &_
"GROUP BY tblCompanyTransPass.companyID, tblCompany.CompanyName, tblCompany.Url, tblCompany.CustomerNumber, tblCompany.FirstName, tblCompany.LastName, tblCompany.Phone, tblCompany.cellular, tblCompany.mail, tblCompany.ActiveStatus, tblCompany.Country, tblCompany.CompanyPhone, tblCompany.CompanyFax, tblCompany.CompanyCountry, tblCompany.CompanyLegalNumber, tblCompany.comment " &_
"ORDER BY COUNT(tblCompanyTransPass.companyID) Desc"
set rsData = oledbData.execute(sSQL)
		
if not rsData.EOF then
	%>
	<tr>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;">Merchant</td>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;">Charges</td>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;"></td>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;">Merchant Phone</td>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;">Merchant Fax</td>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;">Private Name</td>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;">Phone</td>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;">Mobile</td>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;">Email</td>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;"></td>
		<td style="background-color:#e9e9e9; border-bottom:1px solid gray;">Blocked/Closed Merchant</td>
	</tr>
	<tr><td></td></tr>
	<%
	do until rsData.EOF
		
		nCompanyID = rsData("companyID")
		sIsBlocked=IIF(rsData("ActiveStatus") > CMPS_MAXCLOSED, "Yes", "No")
		%>
		<tr bgcolor="<%= sColorBkg %>">
			<td align="left"><%= dbtextShow(rsData("CompanyName")) %></td>
			<td align="left"><%= trim(rsData("NumOfTrans")) %></td>
			<td></td>
			<td align="left"><%= dbtextShow(rsData("CompanyPhone")) %></td>
			<td align="left"><%= dbtextShow(rsData("CompanyFax")) %></td>
			<td align="left"><%= dbTextShow(trim(rsData("FirstName")) & " " & trim(rsData("LastName"))) %></td>
			<td align="left" dir="ltr"><%= dbtextShow(rsData("Phone")) %></td>
			<td dir="ltr" align="left"><%= dbtextShow(rsData("cellular")) %></td>
			<td dir="ltr" align="left"><%= dbtextShow(rsData("mail")) %></td>
			<td></td>
			<td align="left"><%= sIsBlocked %></td>
		</tr>
		<%
		nShowCounter=nShowCounter+1
	rsData.movenext
	loop
	%>
	</table>
	<%
end if
Server.ScriptTimeout = 90
%>
<% call closeConnection() %>
</body>
</html>