<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_controls.asp"-->
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="rtl">
<%
sSendFaxID = trim(request("SendFaxID"))
sSendFaxArray = Split(sSendFaxID, ",", -1, 1)
sSendFaxArraySize = int(UBound(sSendFaxArray))+1

sSQL="SELECT tblCompany.CompanyName, tblRefundAsk.id, tblRefundAsk.RefundAskAmount, tblRefundAsk.RefundAskCurrency," &_
"tblRefundAsk.RefundAskComment, tblCompanyTransPass.Amount, tblCompanyTransPass.Currency, tblCompanyTransPass.TerminalNumber," &_
"tblCompanyTransPass.InsertDate, dbo.GetDecrypted256(tblCreditCard.CCard_number256) CCard_number, tblCreditCard.Member, tblCompanyTransPass.PaymentMethod," &_
"tblCreditCard.ExpMM, tblCreditCard.ExpYY, tblCreditCard.cc_cui " &_
"FROM tblRefundAsk " &_
"INNER JOIN tblCompany ON tblRefundAsk.CompanyID = tblCompany.ID " &_
"INNER JOIN tblCompanyTransPass ON tblRefundAsk.transID = tblCompanyTransPass.ID " &_
"LEFT OUTER JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID " &_
"WHERE tblRefundAsk.id IN (" & sSendFaxID & ") ORDER BY tblRefundAsk.RefundAskDate DESC"
set rsData = oledbData.execute(sSQL)
nCount=0
do until rsData.EOF
	If request("type")="preview" then
		%>
		<table align="center" border="0" dir="rtl" cellpadding="2" cellspacing="0" width="650">
		<tr>
			<td align="right" dir="rtl" class="txt14" style="color:#00254a;">
				<li type="square"><%= dbtextShow(rsData("CompanyName")) %> - ���� �����</li><br />
			</td>
		</tr>
		</table>
		<table align="center" border="0" style="background-color:white; border:1px solid black; filter:progid:DXImageTransform.Microsoft.Shadow(color='gray', Direction=135, Strength=5)" dir="rtl" cellpadding="1" cellspacing="2" width="650">
		<tr>
			<td style="padding-top:10px;">
				<div id="divRefundAskPreview<%= rsData("id") %>">
					<!--#include file="Refund_ask_fax_doc.asp"-->
				</div>
			</td>
		</tr>
		</table>
		<br /><br />
		<%
	Else
		nCount=nCount+1
		if int(nCount)<int(sSendFaxArraySize) then ' check to see if need to page break
			sStyle="page-break-after:always;"
		else
			sStyle="page-break-after:auto;"
		end if
		%>
		<div id="divRefundAskPrint<%= rsData("id") %>" style="<%= sStyle %>">
			<script language="JavaScript">
				divRefundAskPrint<%= rsData("id") %>.innerHTML=top.fraRefundPreview.divRefundAskPreview<%= rsData("id") %>.innerHTML;
			</script>
		</div>
		<%
	End If
rsData.movenext
loop
%>
<script language="JavaScript">
	<%
	If request("type")="preview" then
		%>
		top.fraRefundprint.location.href='Refund_ask_fax_pages.asp?type=print&SendFaxID=<%= sSendFaxID %>';
		<%
	Else
		%>
		top.fraRefundMenu.frmRefundAsk.bPrint.style.cursor = 'hand';
		top.fraRefundMenu.frmRefundAsk.bPrint.disabled = false;
		<%
	End If
	%>
</script>
</body>
</html>
<% call closeConnection() %>
