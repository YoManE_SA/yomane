<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Sub ReloadParent()
		Response.Write("<s" & "cript type=""text/javascript"" language=""javascript"">parent.location.reload();</s" & "cript>")		
		Response.End()
	End Sub

    Sub DeleteFile(id As Integer)
        Dim reader = dbPages.ExecReader("Select Account_ID, FileName From Data.AccountFile Where AccountFile_id=" & id)
        If reader.Read() Then
            Dim path As String = Netpay.Bll.Accounts.Account.MapPrivatePath(reader("Account_ID"), "Files/" & reader("FileName"))
            If System.IO.File.Exists(path) Then System.IO.File.Delete(path)
        End If
        reader.Close()
    End Sub

    Sub DeleteFile(ByVal o As Object, ByVal e As EventArgs)
        DeleteFile(dbPages.TestVar(Request("ID"), 1, 0, 0))
        ReloadParent()
    End Sub

	Sub DeleteRecord(ByVal o As Object, ByVal e As EventArgs)
        DeleteFile(dbPages.TestVar(Request("ID"), 1, 0, 0))
        dbPages.ExecSql("DELETE FROM Data.AccountFile WHERE AccountFile_id=" & dbPages.TestVar(Request("ID"), 1, 0, 0))
		ReloadParent()
	End Sub

	Sub SaveRecord(ByVal o As Object, ByVal e As EventArgs)
		If fuFile.HasFile Then
            If System.Math.Round(fuFile.PostedFile.ContentLength / 1048576) > 3 Then 'check for files larger than 3mb
                ltError.Text = "<span style='color:red'>File is too large. Must be less than 3MB.</span>"
                ltError.Visible = True
                Exit Sub
            End If
            DeleteFile(dbPages.TestVar(Request("ID"), 1, 0, 0))
            Dim sFileName As String = Server.UrlEncode(fuFile.FileName).Replace("+", "_")
            Dim accountId = dbPages.TestVar(dbPages.ExecScalar("SELECT Account_ID FROM Data.AccountFile WHERE AccountFile_id=" & dbPages.TestVar(Request("ID"), 1, 0, 0)), 0, -1, 0)
            Dim path As String = Netpay.Bll.Accounts.Account.MapPrivatePath(accountId, "Files/" & sFileName)
            If Not System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)) Then System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path))
            fuFile.SaveAs(path)
			Dim sExt As String = String.Empty
			Try
				sExt = sFileName.Substring(sFileName.LastIndexOf(".") + 1)
			Catch ex As Exception
			End Try
			dbPages.ExecSql("UPDATE Data.AccountFile SET InsertDate=GetDate(), FileName='" & sFileName & "', FileExt='" & sExt & "' WHERE  AccountFile_id=" & Request("ID"), dbPages.DSN)
		End If
		If Not String.IsNullOrEmpty(txtTitle.Text) Then dbPages.ExecSql("UPDATE Data.AccountFile SET Title='" & dbPages.DBText(txtTitle.Text) & "' WHERE AccountFile_id=" & Request("ID"), dbPages.DSN)
		ReloadParent()
	End Sub

	Sub Page_Load()
		If Session("FileTooLarge") then
			ltError.Text="<span style='color:red'>File is too large. Must be less than 3MB.</span>"
			ltError.Visible=True
			Session("FileTooLarge") = False
		End If
		If Not Page.IsPostBack Then
            Dim drData As SqlDataReader = dbPages.ExecReader("SELECT * FROM Data.AccountFile WHERE AccountFile_id=" & dbPages.TestVar(Request("ID"), 1, 0, 0))
			btnDeleteFile.Enabled = False
			litUploadTitle.Text = "Upload"
			If drData.Read Then
                txtTitle.Text = drData("FileTitle")
                Dim sPath As String = Netpay.Bll.Accounts.Account.MapPrivatePath(drData("Account_ID"), "Files/" & drData("FileName"))
                If My.Computer.FileSystem.FileExists(sPath) Then
					btnDeleteFile.Enabled = True
					litUploadTitle.Text = "Replace"
				End If
			End If
			drData.Close()
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		td{vertical-align:top;}
	</style>
	<script language="JavaScript" type="text/javascript">
		function checkValidFile(filename) {
			if (filename.length == 0) return true;
			var dot = filename.lastIndexOf(".");
			var extension = filename.substr(dot, filename.length).toLowerCase();
			if (extension == ".exe" || extension == ".com" || extension == ".bat" || extension == ".js") {
				alert("This file type is not allowed!");
				return false;
			}
			return true;
		}	
	</script>
	<title></title>
</head>
<body style="padding:10px 0px 0px 30px;">
	<form runat="server">
		<table class="formNormal" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td style="vertical-align:middle;">Description</td>
				<td><asp:TextBox ID="txtTitle" Width="200px" runat="server" MaxLength="1800" /></td>
				<td style="vertical-align:middle;"><asp:Literal ID="litUploadTitle" runat="server" /> File</td>
				<td><asp:FileUpload ID="fuFile" Width="200px" runat="server" /></td>
				<td><asp:Button Text="Update" OnClick="SaveRecord" UseSubmitBehavior="true" CssClass="button" runat="server" OnClientClick="return checkValidFile(document.getElementById('fuFile').value);" /></td>
				<td width="40"></td>
				<td>
					&nbsp;<asp:Button Text="Delete Record" OnClick="DeleteRecord" UseSubmitBehavior="false" CssClass="button" ForeColor="maroon" OnClientClick="if (!confirm('Are you sure ?!')) return false;" runat="server" />
				</td>
				<td>
					<asp:Button ID="btnDeleteFile" Text="Delete File" OnClick="DeleteFile" UseSubmitBehavior="false" CssClass="button" ForeColor="maroon" OnClientClick="if (!confirm('Are you sure ?!')) return false;" runat="server" />
				</td>
				<td><asp:Literal ID = "ltError" runat="server" Visible="false" /></td>
			</tr>
		</table>
	</form>
</body>
</html>
