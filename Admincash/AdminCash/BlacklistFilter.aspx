<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" MasterPageFile="~/AdminCash/FiltersMaster.master" Title="Credit Card Blacklist - Filter" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<script runat="server">
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)

		lblCardType.Text = "<select name=""CardPaymentMethod"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
        Dim sSQL As String = "SELECT PaymentMethod_id, Name FROM List.PaymentMethod WHERE PaymentMethod_id IN (SELECT DISTINCT PaymentMethod_id FROM tblFraudCcBlackList) ORDER BY PaymentMethod_id"
		Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblCardType.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		lblCardType.Text &= "</select>"
		drData.Close()
	End Sub
</script>
<asp:Content runat="server" ContentPlaceHolderID="body">
	<form runat="server" action="blacklist.aspx" method="get" target="frmBody" onsubmit="return filterForm_Submit(null)">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />
	<input type="hidden" name="MerchantID" id="MerchantID" value="" />
	<h1><asp:label ID="lblPermissions" runat="server" /> CC Blacklist<span> - Filter</span></h1>
	<div style="text-align:left;font-size:80%;">
		For adding credit card to the blacklist <a target="frmBody" href="blacklist.aspx?id=0">click here</a>
	</div>
	<hr class="filter" />
	<table class="filterNormal">
		<tr><th class="filter">Blocked By</th></tr>
		<tr>
			<td>
				<input type="radio" name="BlockedBy" value="" checked="checked" onclick="frmFilter.BlockedBy.value=this.value;" />All
				<input type="radio" name="BlockedBy" value="0" onclick="frmFilter.BlockedBy.value=this.value;" />System
				<input type="radio" name="BlockedBy" value="1" onclick="frmFilter.BlockedBy.value=this.value;" />Merchant
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<td>
				<Uc1:FilterMerchants id="FMerchants" ClientField="MerchantID" 
					SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
					INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th class="filter">Block Type</th></tr>
		<tr>
			<td colspan="2">
				<input type="radio" name="BlockType" value="" checked="checked" onclick="frmFilter.BlockType.value=this.value;" />All
				<input type="radio" name="BlockType" value="1" onclick="frmFilter.BlockType.value=this.value;" />Permanent
				<input type="radio" name="BlockType" value="2" onclick="frmFilter.BlockType.value=this.value;" />Temporary
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="checkbox" name="ShowActiveOnly" class="option" value="1" checked="checked" onclick="frmFilter.ShowActiveOnly.value=(this.checked?1:0);" />Show only blocks active on
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<input name="ActiveOnDate" class="medium2 grayBG" value="<%= dbPages.FormatDateTime(Now(), , , true, ) %>" />
			</td>
		</tr>
		<tr>
			<td>
				Reply Code
			</td>
			<td>
				<input name="ReplyCode" class="medium grayBG" />
			</td>
		</tr>
		<tr><th colspan="2" class="filter indent">Card Details</th></tr>
		<tr>
			<td>Card Type</td>
			<td><asp:Label ID="lblCardType" runat="server" /></td>
		</tr>
		<tr>
			<td>Last 4 Digits</td>
			<td>
				<input name="Last4" class="medium grayBG" />
			</td>
		</tr>
        <!--
		<tr><th colspan="2" class="filter indent">Text To Search</th></tr>
        <tr>
			<td colspan="2"><input name="SearchText" class="long grayBG" /></td>
		</tr>
        -->
		<tr><th colspan="2" class="filter indent">Paging</th></tr>
		<tr>
			<td>
				<select name="PageSize" class="grayBG">
					<option value="10">10 rows</option>
					<option value="25" selected="selected">25 rows</option>
					<option value="50">50 rows</option>
					<option value="100">100 rows</option>
				</select>
			</td>
			<td align="right" nowrap="nowrap">
				<input type="submit" class="buttonFilter" value="Search" />
				<input type="reset" class="buttonFilter buttonDelete" value="Reset" />
			</td>
		</tr>
	</table>
	</form>
</asp:Content>