<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableViewState="false" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DebitCompanies.ascx" TagName="FilterDebitCompanies" TagPrefix="Uc1" %>

<script runat="server">
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		Dim sSQL As String, drData As SqlDataReader
		lblPaymentMethod.Text = "<select style=""width:120px; height:50px;"" multiple=""multiple"" name=""PaymentMethod"" class=""grayBG"">"
		sSQL = "SELECT PaymentMethod_id AS 'ID', Name AS 'pm_Name' FROM List.PaymentMethod ORDER BY ID"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblPaymentMethod.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		drData.Close()
		lblPaymentMethod.Text &= "</select>"
		lblCountry.Text = "<select style=""width:135px;font:normal 11px Monospace;"" name=""Country"" class=""grayBG"">"
		lblCountry.Text &= "<option class=""grayBG"" value=""""></option>"
        sSQL = "SELECT CountryISOCode, CountryISOCode+' '+Name FROM [List].[CountryList] ORDER BY CountryISOCode, Name"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblCountry.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		drData.Close()
		lblCountry.Text &= "</select>"
		fdtrControl.FromDateTime = Date.Now.AddMonths(-6).AddDays(1).ToShortDateString
		fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
		DDateRange.FromDateTime = Date.Now.AddMonths(-6).AddDays(1).ToShortDateString
		DDateRange.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
		If HttpContext.Current.Application.Get("CUR_CHARS") Is Nothing Then dbPages.LoadCurrencies()
		Dim sCurrenciesSelect As String = "<select name=""Currency"" class=""grayBG"">"
		sCurrenciesSelect &= "<option value=""""></option>"
		For i As Integer = 0 To eCurrencies.MAX_CURRENCY
			sCurrenciesSelect &= "<option value=""" & i & """>" & HttpContext.Current.Application.Get("CUR_ISO")(i) & "</option>"
		Next
		sCurrenciesSelect &= "</select>"
		litCurrencies.Text = sCurrenciesSelect

		Dim sChbReasonsSelect As String = "<select name=""ChbReason"" class=""grayBG"">"
		sChbReasonsSelect &= "<option value=""""></option>"
		sSQL = "SELECT ReasonCode, Brand FROM tblChargebackReason ORDER BY Brand DESC, ReasonCode"
		Dim drReasons As SqlDataReader = dbPages.ExecReader(sSQL)
		While drReasons.Read()
			sChbReasonsSelect &= "<option value=""" & drReasons("ReasonCode") & """>" & drReasons("Brand") & " " & drReasons("ReasonCode") & "</option>"
		End While
		sChbReasonsSelect &= "</select>"
		litChbReasons.Text = sChbReasonsSelect
        
	End Sub
</script>
<asp:Content runat="server" ContentPlaceHolderID="body" >
	<script language="JavaScript">
		function ExpandNode(fTrNum) {
			trObj = document.getElementById("tr" + fTrNum)
			var chkbox = document.getElementById("chk" + fTrNum);
			if (trObj) {
				if (trObj.style.display == '') {
					trObj.style.display = 'none';
					chkbox.checked = false;
				}
				else {
					trObj.style.display = '';
					chkbox.checked = true;
				}

			}
		}

		function checkTransIDValue() {			
			var val = document.getElementById("passValue").value;
			if (val != "") {
				if (document.getElementById('filter_select').selectedIndex == 0) {
					if (isNaN(val)) {
						alert("Transaction number must be numeric!");
						return false;
					}
					else {
						parent.fraButton.FrmResize();
						return true;
					}
				}
				else {
					if (document.getElementById('filter_select').selectedIndex > 0) {
						parent.fraButton.FrmResize();
						return true;
					}
				}
			}
			else {
				alert("Please fill out the value field!");
				document.getElementById("passValue").focus();
				return false;
			}
		}
	</script>
	<form runat="server" action="Trans_admin_regularData.aspx" method="get" target="frmBody" onsubmit="return filterForm_Submit(null)">	
    <asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />
    <input type="hidden" name="ShowCompanyID" id="ShowCompanyID" />
	
	<h1><asp:label ID="lblPermissions" runat="server" /> Transactions <span> - Filter</span></h1>
	<table class="filterNormal">
		<tr>
			<td>
				<Uc1:FilterMerchants id="FMerchants" ClientField="ShowCompanyID" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Transaction</th></tr>
		<tr>
			<td colspan="2">
				<Uc1:FilterDateTimeRange Title="" ID="fdtrControl" FromTimeFieldName="fromTime" ToTimeFieldName="toTime" FromDateFieldName="fromDate" ToDateFieldName="toDate" runat="server" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<Uc1:FilterDebitCompanies ClientFieldDebitCompany="DebitCompanyID" ClientFieldDebitTerminal="TerminalNumber" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<td>Amount</td>
			<td style="text-align:right;">
				<input name="AmountMin" class="medium grayBG" style="width:50px;" />
				to <input name="AmountMax" class="medium grayBG" style="width:50px;" />
			</td>
		</tr>
		<tr>
			<td>Currency</td>
			<td colspan="2"><asp:Literal runat="server" ID="litCurrencies" /></td>
		</tr>
		<tr>
			<td colspan="2" style="letter-spacing:-1px;">
				<input type="checkbox" name="CreditType" value="1" checked="checked" />Regular
				<input type="checkbox" name="CreditType" value="8" checked="checked" />Installments
				<input type="checkbox" name="CreditType" value="0" checked="checked" />Refund
			</td>
		</tr>
		<tr>
			<td colspan="3"><input type="checkbox" name="IsFraud" value="1" />Is Fraud</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<th colspan="2" class="filter indent">				
				<input type="checkbox" name="chk0" id="chk0" value="1" onclick="ExpandNode('0')" /><span style="cursor:pointer;" onclick="ExpandNode('0');">Payment</span>
			</th>
		</tr>
		<tr id="tr0" style="display:none;">
			<td colspan="2">
				<table class="filterNormal">
				<tr>
					<td valign="top">Method</td>
					<td><asp:Label ID="lblPaymentMethod" runat="server" /></td>
				</tr>
				<tr>
					<td>Last 4 digits</td>
					<td><input name="CcLast4Num" class="short grayBG" /> </td>
				</tr>
				<tr>
					<td>Holder name</td>
					<td><input name="ccHolderName" class="medium2 grayBG" /> </td>
				</tr>
				<tr>
					<td>Email</td>
					<td><input name="ccEmail" class="medium2 grayBG" /> </td>
				</tr>
				<tr>
					<td>BIN number</td>
					<td><input name="CardBIN" class="medium grayBG" /> </td>
				</tr>
				<tr>
					<td valign="top">Country</td>
					<td><asp:Label ID="lblCountry" runat="server" /></td>
				</tr>
				<tr><td height="4"></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<th colspan="2" class="filter">				
				<input type="checkbox" name="chk1" id="chk1" value="1" onclick="ExpandNode('1');" /><span style="cursor:pointer;" onclick="ExpandNode('1');">Chargeback</span>
			</th>
		</tr>
		<tr id="tr1" style="display:none;">
			<td colspan="2">
				<table class="filterNormal" style="padding-top:5px;">
				<tr>
					<td>Filter setting</td>
					<td>
						<select name="deniedStatus" class="grayBG">
							<option value=""></option>
							<option value="0">Exclude Chargebacks</option>
							<!--<option value="1,2,3,4,5,6,7,8,9" selected="selected">Chargebacks Only</option>-->
							<option value="1" selected="selected">Chargebacks Only</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<Uc1:FilterDateTimeRange Title="" ID="DDateRange" FromDateFieldName = "DStartDate" FromTimeFieldName = "DStartTime"	ToDateFieldName = "DEndDate"	ToTimeFieldName = "DEndTime" runat="server" />
					</td>
				</tr>
				<tr>
					<td style="white-space:nowrap;">Chb/RR Reason &nbsp;</td>
					<td colspan="2"><asp:Literal runat="server" ID="litChbReasons" /></td>
				</tr>		
				<tr><td height="4"></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<th colspan="2" class="filter">				
				<input type="checkbox" name="chk2" id="chk2" value="1" onclick="ExpandNode('2');" <%=IIF(Request("Payout") <> "", "checked", "")%> /><span style="cursor:pointer;" onclick="ExpandNode('2');">Payout</span>
			</th>
		</tr>
	</table>
	<table>
		<tr id="tr2" style="display:none;">
			<td colspan="2">
				<table class="filterNormal" style="padding-top:5px;">
				<tr>
					<td rowspan="4" width="16"></td>
					<td><input type="Checkbox" name="Payout" value="S" />Settled</td>
					<td><input type="Checkbox" name="Payout" value="U" />Unsettled</td>
				</tr>
				<tr>
					<td><input type="Checkbox" name="Payout" value="A" <%=IIF(Request("Payout") = "A", "checked", "")%> />Archived</td>
					<td><input type="Checkbox" name="Payout" value="P" />Partially Settled</td>
				</tr>
				<tr><td height="10"></td></tr>
				<tr>
					<td colspan="2">
					    Bank 
					    <select name="PayoutStat">
					        <option>
					        <option value="1">Payments overdue
					        <option value="2">Payments received
					        <option value="3">Payments not received
					    </select>
                    </td>
				</tr>
				</table>
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:15px;">
		<tr>
			<td class="indent" colspan="2">
				<input type="checkbox" name="UseSQL2" value="YES" checked="checked" class="option" />Use SQL2 when possible
			</td>
		</tr>
		<tr>
			<td class="indent">
				<select name="PageSize" class="grayBG">
					<option value="10">10 rows/page</option>
					<option value="25" selected="selected">25 rows/page</option>
					<option value="50">50 rows/page</option>
					<option value="100">100 rows/page</option>
				</select>
			</td>
			<td class="indent" align="right">
				<input type="submit" class="buttonFilter" value="Search" />
				<input type="reset" class="buttonFilter buttonDelete" value="Reset" />
			</td>
		</tr>
		</table>
	</form>
	<hr class="filter" />
	<form id="Form2" action="Trans_admin_regularData.aspx" method="post" target="frmBody" onsubmit="return checkTransIDValue();parent.fraButton.FrmResize();">
	<input type="hidden" name="isAllCompanys" value="1" />
		<table class="filterNormal">
		<tr>
			<td align="center"><input class="medium grayBG" id="passValue" style="width:90px;" name="transID" /></td>
			<td>
				<select id="filter_select" onchange="document.getElementById('passValue').name=options[selectedIndex].value;">
					<option value="transID">Trans ID</option>
					<option value="DebitReferenceCode">Debit Ref.</option>
					<option value="DebitReferenceNum">Debit Num.</option>
					<option value="ApprovalNumber">Approval #</option>
				</select>
			</td>
			<td align="right"><input type="submit" class="buttonFilter" value="Show" id="Show" /></td>
		</tr>
		</table>
	</form>
</asp:Content>