﻿<%@ Page Title="Application Identity" Language="C#" MasterPageFile="~/AdminCash/ContentPage.master" AutoEventWireup="true" CodeFile="ApplicationIdentity_data.aspx.cs" Inherits="AdminCash_ApplicationIdentity_data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" Runat="Server">
	<asp:Label ID="lblPermissions" runat="server" /> <span id="pageMainHeading">Application Identity</span> - <%# Item.Name %><br /><br />
	<netpay:TabControl runat="server" UseJQuery="true" >
		<netpay:TabView runat="server" Text="Details">
			<table class="formData" width="80%">
				<tr>
					<th width="20%">ID</th>
					<th><asp:Literal runat="server" ID="ltID" Text='<%# Item.ID %>' /></th>
				</tr><tr>
					<th>IsActive</th>
					<th><asp:CheckBox runat="server" ID="chkIsActive" Width="25px" Checked='<%# Item.IsActive %>'  /></th>
				</tr><tr>
					<th>Name</th>
					<th><asp:TextBox runat="server" ID="txtName" Text='<%# Item.Name %>' /></th>
				</tr><tr>
					<th>BrandName</th>
					<th><asp:TextBox runat="server" ID="txtBrandName" Text='<%# Item.BrandName %>' /></th>
				</tr><tr>
					<th>CompanyName</th>
					<th><asp:TextBox runat="server" ID="txtCompanyName" Text='<%# Item.CompanyName %>' /></th>
				</tr><tr>
					<th>CopyRightText</th>
					<th><asp:TextBox runat="server" ID="txtCopyRightText" Text='<%# Item.CopyRightText %>' /></th>
				</tr><tr>
					<td><br /></td>
				</tr><tr>
					<th>DomainName</th>
					<th><asp:TextBox runat="server" ID="txtDomainName" Text='<%# Item.DomainName %>' /></th>
				</tr><tr>
					<th>ThemeName</th>
					<th><asp:TextBox runat="server" ID="txtThemeName" Text='<%# Item.ThemeName %>' /></th>
				</tr><tr>
					<th>ContentFolder</th>
					<th><asp:TextBox runat="server" ID="txtContentFolder" Text='<%# Item.ContentFolder %>' /></th>
				</tr><tr>
					<th>Token Global Hash</th>
					<th><asp:CheckBox runat="server" ID="chkEnableTokenHash" Checked='<%# Item.EnableTokenHash %>' Width="25" /> <asp:Label runat="server" ID="lblTokenHash" Text='<%# Item.HashKey %>' /></th>
				</tr><tr>
					<td><br /></td>
				</tr><tr>
					<th>Wire Account ID</th>
					<th><netpay:WireAccountDropDown runat="server" ID="ddlWireAccountID" EnableBlankSelection="true" Text='<%# Item.WireAccountID %>' /></th>
				</tr><tr>
					<th>Process Merchant Number</th>
					<th><asp:TextBox runat="server" ID="txtProcessMerchantNumber" Text='<%# Item.ProcessMerchantNumber %>' /></th>
				</tr>
			</table>
		</netpay:TabView>
		<netpay:TabView ID="tbLinks" runat="server" Text="Links">
			<table class="formData" width="80%">
				<tr>
					<th width="20%">URLDevCenter</th>
					<th><asp:TextBox runat="server" ID="txtURLDevCenter" Text='<%# Item.URLDevCenter %>' /></th>
				</tr><tr>
					<th>URLProcess</th>
					<th><asp:TextBox runat="server" ID="txtURLProcess" Text='<%# Item.URLProcess %>' /></th>
				</tr><tr>
					<th>URLMerchantCP</th>
					<th><asp:TextBox runat="server" ID="txtURLMerchantCP" Text='<%# Item.URLMerchantCP %>' /></th>
				</tr><tr>
					<th>URLWallet</th>
					<th><asp:TextBox runat="server" ID="txtURLWallet" Text='<%# Item.URLWallet %>' /></th>
				</tr><tr>
					<th>URLWebsite</th>
					<th><asp:TextBox runat="server" ID="txtURLWebsite" Text='<%# Item.URLWebsite %>' /></th>
				</tr>
			</table>
		</netpay:TabView>
		<netpay:TabView ID="TabView1" runat="server" Text="Email Settings">
			<table class="formData" width="80%">
				<tr>
					<th width="20%">SmtpServer</th>
					<th><asp:TextBox runat="server" ID="txtSmtpServer" Text='<%# Item.SmtpServer %>' /></th>
				</tr><tr>
					<th>SmtpUsername</th>
					<th><asp:TextBox runat="server" ID="txtSmtpUsername" Text='<%# Item.SmtpUsername %>' /></th>
				</tr><tr>
					<th>SmtpPassword</th>
					<th><asp:TextBox runat="server" ID="txtSmtpPassword" Text='<%# Item.SmtpPassword %>' /></th>
				</tr><tr>
					<th>EmailFrom</th>
					<th><asp:TextBox runat="server" ID="txtEmailFrom" Text='<%# Item.EmailFrom %>' /></th>
				</tr><tr>
					<th>EmailContactTo</th>
					<th><asp:TextBox runat="server" ID="txtEmailContactTo" Text='<%# Item.EmailContactTo %>' /></th>
				</tr>
			</table>
		</netpay:TabView>
		<netpay:TabView ID="tbCurrencies" runat="server" Text="Currencies">
			<asp:CheckBoxList runat="server" ID="cblCurrencies"  DataTextField="Name" DataValueField="IsoCode" RepeatColumns="3" />
		</netpay:TabView>
		<netpay:TabView ID="tbPaymentMethods" runat="server" Text="PaymentMethods">
			<asp:CheckBoxList runat="server" ID="cblPaymentMethods"  DataTextField="Name" DataValueField="ID" RepeatColumns="5" />
		</netpay:TabView>
		<netpay:TabView ID="tbOwnPaymentMethods" runat="server" Text="Owned PaymentMethods">
			<asp:CheckBoxList runat="server" ID="cblOwnPaymentMethods"  DataTextField="Name" DataValueField="ID" RepeatColumns="5" />
		</netpay:TabView>
		<netpay:TabView ID="tbMerchantGroups" runat="server" Text="Merchant Groups">
			<asp:CheckBoxList runat="server" ID="cblMerchantGroups" DataTextField="Name" DataValueField="ID" RepeatColumns="5" />
		</netpay:TabView>
		<netpay:TabView ID="TabView2" runat="server" Text="Affiliates">
			<asp:CheckBoxList runat="server" ID="cblAffiliates" DataTextField="Name" DataValueField="ID" RepeatColumns="5" />
		</netpay:TabView>

		<netpay:TabView ID="tbTokens" runat="server" Text="Tokens">
			<div style="padding:8px;">
				Add new token with name
				<asp:TextBox runat="server" ID="newTokenName" />
                GUID (can leave empty or auto generate)
                <asp:TextBox runat="server" id="newToekn" />
				<asp:Button ID="Button1" runat="server" Text="Add" OnClick="AddToken_Click" />
			</div>
			<table width="100%" class="filterNormal">
				<tr>
					<th>Title</th>
					<th>Token</th>
					<th>Delete</th>
				</tr>
				<asp:Repeater runat="server" ID="rptTokens">
					<ItemTemplate>
						<tr>
							<td><%# Eval("Value") %></td>
							<td><%# Eval("Key") %></td>
							<td><asp:LinkButton runat="server" Text="Remove" CommandArgument='<%# Eval("Key") %>' OnCommand="RemoveToken_Click" /></td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</table>
		</netpay:TabView>
        <netpay:TabView runat="server" ID="tbLamda" Text="Lamda">
            <div style="border:1px solid #c0c0c0;padding:16px;">
                Current Balance:
                <b>
				    <asp:Label runat="server" ID="ltLamdaBalance"  />
				    <asp:Label runat="server" ID="ltLamdaBalanceIso"  />
                </b>
                <asp:Label runat ="server" ID="ltLamdaRequestError" EnableViewState="false" />
            </div>
            <table class="formData" width="80%">
				<tr>
					<th width="20%">User Name</th>
					<th><asp:TextBox runat="server" ID="txtLamdaUserName" /></th>
				</tr><tr>
					<th width="20%">User ID</th>
					<th><asp:TextBox runat="server" ID="txtLamdaUserID" /></th>
				</tr><tr>
					<th width="20%">Password</th>
					<th><asp:TextBox runat="server" ID="txtLamdaPassword" /></th>
                </tr>
            </table>
        </netpay:TabView>
	</netpay:TabControl>
	<hr />
	<asp:Button runat="server" ID="btnSave" OnClick="Save_Click" Text="Save" style="float:right;" />
	<asp:Button runat="server" ID="btnDelete" OnClick="Delete_Click" Text="Delete" OnClientClick="return confirm('Are you sure');" />
</asp:Content>

