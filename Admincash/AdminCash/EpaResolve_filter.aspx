<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		fdtrControl1.FromDateTime = DateAdd("d", -1, Date.Now).ToShortDateString
		fdtrControl1.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
	End Sub
</script>

<asp:Content ContentPlaceHolderID="body" runat="server">
    <form runat="server" action="EpaResolve_data.aspx" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
    <input type="hidden" id="Source" name="Source" value="filter" />
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />

	<h1><asp:label ID="lblPermissions" runat="server" /> Epa Resolve<span> - Filter</span></h1>

	<table class="filterNormal">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl1" runat="server" Title="Import Date" FromDateFieldName="ImpDateFrom" FromTimeFieldName="ImpDateFromTime" ToDateFieldName="ImpDateTo" ToTimeFieldName="ImpDateToTime" /></td></tr>
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl2" runat="server" Title="Transaction Date" FromDateFieldName="TransDateFrom" FromTimeFieldName="TransDateFromTime" ToDateFieldName="TransDateTo" ToTimeFieldName="TransDateToTime" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="3">Status</th></tr>
		<tr>
			<td>
				<select name="recType">
					<option value="0"></option>
					<option value="1">No matching found</option>
					<option value="2" selected="selected">Multiple matching found</option>
				</select>
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Amount</th></tr>
		<tr>
			<td>
				<select name="iCurrency">
					<option value=""></option>
					<%
						Dim i As Integer
						For i = 0 To CType(eCurrencies.MAX_CURRENCY, Integer)
							%>
								<option value="<%=i%>"><%=dbPages.GetCurText(i)%></option>
							<%
						Next
					%>
				</select>
				<input type="text" class="input1" size="5" dir="ltr" name="amountFrom" value="" />
				To <input type="text" class="input1" size="5" dir="ltr" name="amountTo" value="" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Terminal</th><th>Approval</th></tr>
		<tr>
			<td>
				<input type="text" class="input1" size="8" dir="ltr" name="TerminalNo" value="" />
			</td>
			<td>
				<input type="text" class="input1" size="8" dir="ltr" name="ApprovalNo" value="" />
			</td>
		</tr>    
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td>
				<select name="PageSize">
					<option value="20" selected="selected">20 Rows</option>
					<option value="40">40 Rows</option>
					<option value="60">60 Rows</option>
					<option value="80">80 Rows</option>
				</select>
			</td>
			<td style="text-align:right;">
				<input type="reset" value="RESET" />
				<input type="submit" name="Action" value="SEARCH"><br />
			</td>
		</tr>
	</table>
    </form>
	<hr class="filter" />
    <form action="EpaResolve_data.aspx" target="frmBody" method="post">
	<input type="hidden" name="Source" value="filter" />
	<input type="hidden" name="PageSize" value="20" />
	<table class="filterNormal">
		<tr><th>ID #</th></tr>
		<tr>
			<td>
				<input type="text" class="input1" size="3" dir="ltr" name="IdFrom" value="">
				To <input type="text" class="input1" size="3" dir="ltr" name="IdTo" value="">
			</td>
			<td style="text-align:right;">
				<input type="submit" name="Action" value="SEARCH"><br />
			</td>
		</tr>
	</table>
    </form>
</asp:Content>