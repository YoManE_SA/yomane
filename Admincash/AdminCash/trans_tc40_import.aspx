<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<script runat="server">
    Private Function MapFileName(fileName As String) As String
        Return Domain.Current.MapPrivateDataPath("Account Files/DebitCompany/BnS/Uploads/" & fileName)
    End Function
    
	Sub UploadFile(ByVal o As Object, ByVal e As EventArgs)
		litError.Text = String.Empty
		litFileInfo.Text = String.Empty
		If Not fuTC40.HasFile Then
			litError.Text = "No file is uploaded!"
			Exit Sub
		End If
		Dim sFileName As String = fuTC40.FileName.ToLower
		fuTC40.SaveAs(MapFileName(sFileName))
		litFileInfo.Text = "File uploaded: <b>" & sFileName & "</b>"
		If sFileName.StartsWith("estmt_bo_tc") And sFileName.EndsWith(".xml") Then
			litFileInfo.Text = "TC40 from JCC, XML file (Excel Workbook), " & Date.Today.ToString("yyyy-MM-dd")
			Dim xdTC40 As New XmlDocument
			xdTC40.Load(MapFileName(sFileName))
			Dim xnTable As XmlNode = xdTC40.DocumentElement.LastChild.FirstChild
			Dim nColumns As Integer = 0, nColumnSID As Integer = 0, nCol As Integer = 0, nColumnTransDate As Integer = 0, nRows As Integer = 0
			Dim sData, sValues As String, sColumns As String = "", sTransDate As String = "", sTrnID As String = ""
			For Each xnRecord As XmlNode In xnTable.ChildNodes
				Select Case xnRecord.Name
					Case "Row"
						If nRows = 0 Then
							For Each xnCell As XmlNode In xnRecord.ChildNodes
								sData = xnCell.InnerText
								If Not String.IsNullOrEmpty(sData) Then sColumns &= sData & ", "
								If sData.ToUpper = "S_ID" Then nColumnSID = nCol
								If sData.ToUpper = "T_DATE" Then nColumnTransDate = nCol
								nCol += 1
							Next
							sColumns &= "itc_FileName, itc_Date"
						Else
							sValues = ""
							sTransDate = ""
							sTrnID = ""
							nCol = 0
							For Each xnCell As XmlNode In xnRecord.ChildNodes
								sData = xnCell.InnerText
								sValues &= "'" & dbPages.DBText(sData) & "', "
								If nCol = nColumnSID Then sTrnID = sData
								If nCol = nColumnTransDate Then sTransDate = sData
								nCol += 1
							Next
							sValues &= "'" & sFileName & "', Convert(datetime, '" & sTransDate & "', 1)"
							Try
								dbPages.ExecSql("IF NOT EXISTS (SELECT 1 FROM tblImportTC40JCC WITH (NOLOCK) WHERE S_ID='" & sTrnID & "') INSERT INTO tblImportTC40JCC(" & sColumns & ") VALUES (" & sValues & ");")
							Catch ex As Exception
								litError.Text &= "TC40 with S_ID " & sTrnID & " failed, the row is ignored. "
							End Try
						End If
						nRows += 1
					Case "Column"
						nColumns += 1
				End Select
			Next
			litFileInfo.Text &= " (" & nRows & " rows)"
			repTC40.DataBind()
		Else
			litError.Text = "Unrecognizable file format -  <b>" & sFileName & "</b> cannot be imported."
		End If
	End Sub
	
	'Sub SetCheckbox(ByVal chkBox As CheckBox, ByVal bEnabled As Boolean, Optional ByVal bChecked As Boolean = False)
	'	chkBox.Enabled = bEnabled
	'	chkBox.Checked = bChecked
	'End Sub

	Sub SetTransList(ByVal o As Object, ByVal e As RepeaterItemEventArgs)
		If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
			CType(e.Item.FindControl("chkDelete"), CheckBox).Attributes.Add("onclick", "ConfirmWithHighlight(this);")
			CType(e.Item.FindControl("chkCreate"), CheckBox).Attributes.Add("onclick", "ReenableOtherCheckboxes(this);")
		End If
	End Sub

	Sub ProcessRecords(ByVal o As Object, ByVal e As EventArgs)
		Dim nID, nTrans, nTransCHB As Integer, litID, litIDDC, litDateTC40, litReason As Literal, chkBlock, chkSend As CheckBox, dtTrans As Date
		Dim nDC As Integer, hidDC As HiddenField
		For Each riTC40 As RepeaterItem In repTC40.Items
			litID = riTC40.FindControl("litID")
			If Not litID Is Nothing Then
				nID = CType(litID, Literal).Text
				hidDC = riTC40.FindControl("hidDebitCompany")
				nDC = hidDC.Value
				If CType(riTC40.FindControl("chkDelete"), CheckBox).Checked Then
					Select Case nDC
						Case 19 : dbPages.ExecSql("DELETE FROM tblImportTC40JCC WHERE ID=" & nID)
					End Select
				Else
					nTrans = dbPages.TestVar(CType(riTC40.FindControl("ddlTrans"), DropDownList).SelectedValue, 1, 0, 0)
					If nTrans > 0 And CType(riTC40.FindControl("chkCreate"), CheckBox).Checked Then
						chkBlock = riTC40.FindControl("chkBlock")
						chkSend = riTC40.FindControl("chkSend")
						litDateTC40 = riTC40.FindControl("litDateTC40")
						litReason = riTC40.FindControl("litReason")
						Select Case nDC
							Case 19
								dtTrans = litDateTC40.Text
								litIDDC = riTC40.FindControl("litSID")
								'nTransCHB = CHB.Create(nTrans, dtTrans, chkBlock.Checked, chkSend.Checked, "AutoCHB (JCC) TrnID=" & litIDDC.Text & ", Reason=" & litReason.Text)
								If nTransCHB > 0 Then dbPages.ExecSql("UPDATE tblImportChargebackJCC SET icb_IsProcessed=1 WHERE ID=" & nID)
						End Select
					End If
				End If
			End If
		Next
		repTC40.DataBind()
	End Sub
	
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsTC40.ConnectionString = dbPages.DSN
		If Security.IsLimitedDebitCompany Then
			dsTC40.SelectCommand = "SELECT * FROM viewImportChargeback WITH (NOLOCK) WHERE FileDebitCompany IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script type="text/javascript" language="JavaScript" src="../js/func_common.js"></script>
	<script type="text/javascript" language="JavaScript">
		function ResetCheckboxes(oSelect) {
			var bChecked = (oSelect.selectedIndex != 0);
			for (var tdCell = oSelect; tdCell.nodeName.toLowerCase() != 'td'; tdCell = tdCell.parentNode);
			for (; tdCell.nextSibling; tdCell = tdCell.nextSibling);
			tdCell.firstChild.firstChild.checked = bChecked;
			tdCell.previousSibling.firstChild.firstChild.checked = bChecked;
			tdCell.previousSibling.previousSibling.firstChild.firstChild.checked = bChecked;
		}

		function ReenableOtherCheckboxes(oCheckbox) {
			var bActive = oCheckbox.checked;
			for (var tdCell = oCheckbox; tdCell.nodeName.toLowerCase() != 'td'; tdCell = tdCell.parentNode);
			tdCell.nextSibling.firstChild.firstChild.checked = bActive;
			tdCell.nextSibling.firstChild.firstChild.disabled = !bActive;
			tdCell.nextSibling.nextSibling.firstChild.firstChild.checked = bActive;
			tdCell.nextSibling.nextSibling.firstChild.firstChild.disabled = !bActive;
			tdCell.nextSibling.nextSibling.nextSibling.firstChild.firstChild.checked = bActive;
			tdCell.nextSibling.nextSibling.nextSibling.firstChild.firstChild.disabled = !bActive;
		}

		function ConfirmWithHighlight(oCheckbox)
		{
			for (var trRow = oCheckbox; trRow.nodeName.toLowerCase() != 'tr'; trRow = trRow.parentNode);
			trRow.style.backgroundColor = 'Pink';
			if (oCheckbox.checked) oCheckbox.checked = confirm('Do you really want to delete this item ?!');
			trRow.style.backgroundColor = '';
		}

		function ConfirmProcess()
		{
			return confirm('Do you really want to process all the records above ?!');
		}

		function ShowDetails(imgThis, bShow)
		{
			for (var trMain = imgThis; trMain.nodeName.toLowerCase() != "tr"; trMain = trMain.parentNode);
			for (var trDetails = trMain.nextSibling; trDetails.nodeName.toLowerCase() != "tr"; trDetails = trDetails.nextSibling);
			trDetails.style.display = (bShow ? "block" : "none");
			for (trDetails = trDetails.nextSibling; trDetails.nodeName.toLowerCase() != "tr"; trDetails = trDetails.nextSibling);
			trDetails.style.display = (bShow ? "block" : "none");
			if (bShow)
			{
				var nTransID = "";
				if (imgThis.parentNode.nextSibling.firstChild)
				{
					if (imgThis.parentNode.nextSibling.firstChild.nodeName.toLowerCase() == "select")
						nTransID = imgThis.parentNode.nextSibling.firstChild.value;
					else
						if (imgThis.parentNode.nextSibling.firstChild.nodeName.toLowerCase() == "#text") nTransID = imgThis.parentNode.nextSibling.innerText;
						if (nTransID != "") trDetails.firstChild.nextSibling.innerHTML = "<iframe frameborder=\"0\" border=\"0\" src=\"trans_detail_cc.asp?isAllowAdministration=false&TransTableType=pass&PaymentMethod=1&transID=" + nTransID + "\" width=\"100%\" height=\"100\" onload=\"height=contentWindow.document.body.scrollHeight;\"></iframe>";
				}
			}
			for (var imgOther = (bShow ? imgThis.nextSibling : imgThis.previousSibling); imgOther.nodeName.toLowerCase() != "img";)
				imgOther = (bShow ? imgOther.nextSibling : imgOther.previousSibling);
			imgThis.style.display = "none";
			imgOther.style.display = "";
		}
	</script>
</head>
<body>
	<form id="Form1" runat="server">
	<table align="center" style="width:95%;">
	<tr>
		<td id="pageMainHeading">
			<asp:label ID="lblPermissions" runat="server" /> TC40 File Import <br />
		</td>
	</tr>
	</table>
	<br />
	<div style="text-align:center;">
		<fieldset style="width:95%;padding:10px;text-align:left;">
			<legend>Upload New File</legend>
			<asp:FileUpload ID="fuTC40" ToolTip="Upload chargeback file here" runat="server" />
			<asp:Button ID="Button1" CssClass="buttonWhite" OnClick="UploadFile" Text="Upload!" runat="server" />
			<span style="font-size:11px;"><asp:literal ID="litFileInfo" runat="server" /></span>
			<div class="errorMessage"><asp:literal ID="litError" runat="server" /></div>
		</fieldset>
		<div style="text-align:center;">
			<table class="formNormal" width="95%">
				<asp:SqlDataSource ID="dsTC40" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient" runat="server"
				 SelectCommand="SELECT * FROM viewImportTC40JCC" />
				<asp:Repeater ID="repTC40" DataSourceID="dsTC40" OnItemDataBound="SetTransList" runat="server">
					<HeaderTemplate>
						<tr>
							<th>&nbsp;</th>
							<th>Trans. No.</th>
							<th>Trans. Date</th>
							<th>Merchant</th>
							<th style="text-align:right;">Amount</th>
							<th>Currency</th>
							<th>Debit Company</th>
							<th>Payment Method</th>
							<th>Transaction Date</th>
							<th>Fraud Type</th>
							<th class="Vertical">Delete</th>
							<td>&nbsp;</td>
							<th class="Vertical">Create CHB</th>
							<th class="Vertical">Block Card</th>
							<th class="Vertical">Refund All</th>
							<th class="Vertical">Send Mail</th>
						</tr>
					</HeaderTemplate>
					<ItemTemplate>
						<tr onmouseover="this.style.backgroundColor='Gainsboro';" onmouseout="if (this.style.backgroundColor.toLowerCase()=='gainsboro') this.style.backgroundColor='';">
							<td>
								<img onclick="ShowDetails(this,true);" style="cursor:pointer;" src="../images/tree_expand.gif" alt="Expand" width="16" height="16" border="0" />
								<img onclick="ShowDetails(this,false);" style="cursor:pointer;display:none;" src="../images/tree_collapse.gif" alt="Collapse" width="16" height="16" border="0" />
							</td>
							<td>
								<asp:Literal ID="litTransID" Text='<%# Eval("TransID") %>' runat="server" />
								<asp:Literal ID="litID" Text='<%# Eval("ID") %>' Visible="false" runat="server" />
							</td>
							<td><asp:Literal Text='<%# dbPages.FormatDateTime(Eval("TransactionDate"), , False) %>' runat="server" /></td>
							<td><asp:Literal Text='<%# Eval("MerchantList") %>' runat="server" /></td>
							<td style="text-align:right;"><asp:Literal Text='<%# IIf(Eval("FileDebitCompany")=19, CType("0" & Eval("FRAUD_AMT"), Decimal).ToString("0.00"), String.Empty) %>' runat="server" /></td>
							<td><asp:Literal Text='<%# Eval("CurrencyIsoName") %>' runat="server" /></td>
							<td style="vertical-align:middle;">
								<asp:Image ImageUrl='<%# "/NPCommon/ImgDebitCompanys/23X12/" & Eval("FileDebitCompany") & ".gif"%>' AlternateText='<%# Eval("FileDebitCompanyName") %>'
								 ImageAlign="Middle" runat="server" />
								<asp:Literal Text='<%# Eval("FileDebitCompanyName") %>' runat="server" />
								<asp:HiddenField ID="hidDebitCompany" Value='<%# Eval("FileDebitCompany") %>' runat="server" />
							</td>
							<td style="vertical-align:middle;">
								<asp:Image ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/" & Eval("PaymentMethod") & ".gif"%>' AlternateText='<%# Eval("PaymentMethodName") %>'
								 ImageAlign="Middle" runat="server" />
								<asp:Image ImageUrl='<%# "/NPCommon/ImgCountry/18X12/" & Eval("CountryISO") & ".gif"%>' AlternateText='<%# Eval("CountryName") %>'
								 ImageAlign="Middle" runat="server" />
								<asp:Literal Text='<%# Eval("CardNumberPartial") %>' runat="server" />
							</td>
							<td><asp:Literal ID="litDateTC40" Text='<%# dbPages.FormatDateTime(Eval("itc_Date"), , True, False) %>' runat="server" /></td>
							<td><asp:Literal ID="litReason" Text='<%# Eval("FRAUD_TYPE") %>' runat="server" /></td>
							<td style="text-align:center;"><asp:CheckBox ID="chkDelete" CssClass="option" runat="server" /></td>
							<td>&nbsp;</td>
							<td style="text-align:center;"><asp:CheckBox ID="chkCreate" CssClass="option" Checked='<%# not Eval("IsChargeback") %>' Enabled='<%# Eval("IsChargeback") %>' runat="server" /></td>
							<td style="text-align:center;"><asp:CheckBox ID="chkBlock" CssClass="option" runat="server" /></td>
							<td style="text-align:center;"><asp:CheckBox ID="chkRefund" CssClass="option" runat="server" /></td>
							<td style="text-align:center;"><asp:CheckBox ID="chkSend" CssClass="option" runat="server" /></td>
						</tr>
						<tr style="display:none;">
							<td></td>
							<td colspan="15" class="details">
								<asp:Label Visible='<%# Eval("FileDebitCompany")=19 %>' runat="server">
									<table class="formNormal">
										<tr style="text-transform:capitalize;">
											<th>merch name</th>
											<th>merch city</th>
											<th>mcc</th>
											<th>fraud type</th>
											<th>fraud amt</th>
											<th>bin</th>
											<th>t date</th>
											<th>acc no</th>
											<th>seq no</th>
											<th>microfilm</th>
											<th>entry mode</th>
											<th>s id</th>
										</tr>
										<tr style="background-color:rgb(255,220,220);">
											<td><asp:Literal Text='<%# Eval("MERCH_NAME") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("MERCH_CITY") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("MCC") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("FRAUD_TYPE") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("FRAUD_AMT") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("BIN") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("T_DATE") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("ACC_NO") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("SEQ_NO") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("MICROFILM") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("ENTRY_MODE") %>' runat="server" /></td>
											<td><asp:Literal Text='<%# Eval("S_ID") %>' runat="server" /></td>
										</tr>
									</table>
								</asp:Label>
							</td>
						</tr>
						<tr style="display:none;">
							<td></td>
							<td colspan="15"></td>
						</tr>
						<tr>
							<td colspan="11" style="height:1;background-color:Silver;"></td>
							<td style="height:1;"></td>
							<td colspan="4" style="height:1;background-color:Silver;"></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						<tr>
							<td colspan="16" style="text-align:right;">
								<asp:Button Text="Process!" OnClick="ProcessRecords" OnClientClick="if (!ConfirmProcess()) return false;" CssClass="buttonWhite" runat="server" />
							</td>
						</tr>
					</FooterTemplate>
				</asp:Repeater>
			</table>
		</div>
	</div>
	</form>
</body>
</html>