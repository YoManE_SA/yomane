﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminCash/FiltersMaster.master" AutoEventWireup="true" CodeFile="merchant_RegistrationFilter.aspx.cs" Inherits="AdminCash_merchant_RegistrationFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
	<form id="Form1" runat="server">
		<h1><asp:Label ID="lblPermissions" runat="server" /> Merchant Registration<span> - Filter</span></h1>
		<table class="filterNormal" style="padding-top:5px;" width="100%">
			<tr>
				<th>Registered Localy</th>
				<td><asp:DropDownList runat="server" ID="ddlRegisteredLocally" ><asp:ListItem Text="" Value="" /><asp:ListItem Text="Yes" Value="true" /><asp:ListItem Text="No" Value="false" /></asp:DropDownList></td>
			</tr>
			<tr>
				<th>Registered At CardConnect</th>
				<td><asp:DropDownList runat="server" ID="ddlRegisteredAtCardConnect" ><asp:ListItem Text="" Value="" /><asp:ListItem Text="Yes" Value="true" /><asp:ListItem Text="No" Value="false" Selected="True" /></asp:DropDownList></td>
			</tr>
		</table>
		<hr />
		<asp:Button runat="server" OnClick="Search_Click" Text="Search" />
		<asp:Button runat="server" OnClick="Export_Click" Text="Export" />
		<hr />
	</form>
	<asp:Repeater ID="rptList" runat="server">
		<HeaderTemplate>
			<table class="formNormal" style="padding-top:5px;">
				<tr>
					<th>Business name</th>
					<th>Status</th>
					<th>Full name</th>
				</tr>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td><asp:HyperLink runat="server" Target="frmBody" NavigateUrl='<%# "merchant_RegistrationData.aspx?ID=" + ((Netpay.Bll.Merchants.Registration)Container.DataItem).ID %>' Text="<%# ((Netpay.Bll.Merchants.Registration)Container.DataItem).LegalBusinessName %>" /></td>
				<td><%# ((Netpay.Bll.Merchants.Registration)Container.DataItem).IntegrationData.ContainsKey("CardConnectID") ? "CC" : "" %> | <%# ((Netpay.Bll.Merchants.Registration)Container.DataItem).MerchantID != null ? "Loc" : "" %></td>
				<td><%# ((Netpay.Bll.Merchants.Registration)Container.DataItem).FirstName %> <%# ((Netpay.Bll.Merchants.Registration)Container.DataItem).LastName %></td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</table>
		</FooterTemplate>
	</asp:Repeater>
</asp:Content>

