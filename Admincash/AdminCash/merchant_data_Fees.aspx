<%@ Page Language="VB" EnableViewState="true" %>

<%@ Register Src="TabMenu.ascx" TagPrefix="UC1" TagName="TabMenu" %>
<%@ Register Src="PeriodicFees.ascx" TagPrefix="UC1" TagName="PeriodicFees" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<%@ Register Namespace="Netpay.Web.Controls" Assembly="Netpay.Web" TagPrefix="netpay" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Netpay.Web" %>
<script runat="server">
    Private nMerchant As Integer, bErrorsCleared As Boolean = False

    Sub Page_Init()
        nMerchant = dbPages.TestVar(Request("ID"), 1, 0, 0)
        If nMerchant < 1 Then Throw New Exception("Invalid merchant ID specified: " & Request("ID").ToString)
    End Sub

    Sub SaveData(Optional o As Object = Nothing, Optional e As EventArgs = Nothing)
        pfAnnual.Save()
        pfMonthly.Save()
        'save currency settings
        For Each riGrade As RepeaterItem In rptMerchantCurrencyFees.Items
            If riGrade.ItemType <> ListItemType.Header Then
                Dim curID As Integer = CType(riGrade.FindControl("hdCurrency"), HiddenField).Value
                Dim wireFee As Decimal = dbPages.TestVar(CType(riGrade.FindControl("txtWireFee"), TextBox).Text, 0D, -1D, 0D)
                Dim WireFeePercent As Decimal = dbPages.TestVar(CType(riGrade.FindControl("txtWireFeePercent"), TextBox).Text, 0D, -1D, 0D)
                Dim StorageFee As Decimal = dbPages.TestVar(CType(riGrade.FindControl("txtStorageFee"), TextBox).Text, 0D, -1D, 0D)
                Dim Handling As Decimal = dbPages.TestVar(CType(riGrade.FindControl("txtHandling"), TextBox).Text, 0D, -1D, 0D)
                Dim MinPayout As Decimal = dbPages.TestVar(CType(riGrade.FindControl("txtMinPayout"), TextBox).Text, 0D, -1D, 0D)
                Dim params As Object() = New Object() {nMerchant, curID, wireFee, WireFeePercent, StorageFee, Handling, MinPayout}
                If Request("CurrencySettings_" & curID & "_Exist") = "1" Then
                    If (dbPages.ExecSql(String.Format("Update Setting.SetMerchantSettlement Set WireFee={2}, WireFeePercent={3}, StorageFee={4}, HandlingFee={5}, MinPayoutAmount={6} Where Merchant_ID={0} And Currency_ID={1}", params)) = 0) Then
                        dbPages.ExecSql(String.Format("Insert Into Setting.SetMerchantSettlement(Merchant_ID, Currency_ID, WireFee, WireFeePercent, StorageFee, HandlingFee, MinPayoutAmount) Values({0}, {1}, {2}, {3}, {4}, {5}, {6})", params))
                    End If
                End If
            End If
        Next
        'save graded fees
        Dim sbSQL As New StringBuilder("UPDATE tblCompany SET ")
        sbSQL.AppendFormat(" CFF_Currency={0}", IIf(rbGradedFeesOff.Checked, "NULL", ddlGradedCurrency.SelectedValue))
        sbSQL.AppendFormat(" WHERE ID=" & nMerchant)
        dbPages.ExecSql(sbSQL.ToString())
        Dim nID As Integer, nTotalMin, nFeePercent As Decimal
        For Each riGrade As RepeaterItem In rptGradedTotals.Items
            If riGrade.ItemType <> ListItemType.Header Then
                nID = CType(riGrade.FindControl("hidID"), HiddenField).Value
                If CType(riGrade.FindControl("chkDelete"), CheckBox).Checked Then
                    dbPages.ExecSql("EXEC DeleteMerchantFeeGrade " & nMerchant & ", " & nID)
                Else
                    If Decimal.TryParse(CType(riGrade.FindControl("txtTotalMin"), TextBox).Text, System.Globalization.NumberStyles.AllowThousands Or System.Globalization.NumberStyles.AllowDecimalPoint, Nothing, nTotalMin) Then
                        If Decimal.TryParse(CType(riGrade.FindControl("txtFeePercent"), TextBox).Text, System.Globalization.NumberStyles.AllowThousands Or System.Globalization.NumberStyles.AllowDecimalPoint, Nothing, nFeePercent) Then
                            dbPages.ExecSql("EXEC SaveMerchantFeeGrade " & nMerchant & ", " & nTotalMin.ToString("0.00") & ", " & nFeePercent.ToString("0.00") & ", " & nID)
                        End If
                    End If
                End If
            End If
        Next
        If Decimal.TryParse(txtTotalMin.Text, System.Globalization.NumberStyles.AllowThousands Or System.Globalization.NumberStyles.AllowDecimalPoint, Nothing, nTotalMin) Then
            If Decimal.TryParse(txtFeePercent.Text, System.Globalization.NumberStyles.AllowThousands Or System.Globalization.NumberStyles.AllowDecimalPoint, Nothing, nFeePercent) Then
                nID = 0
                dbPages.ExecSql("EXEC SaveMerchantFeeGrade " & nMerchant & ", " & nTotalMin.ToString("0.00") & ", " & nFeePercent.ToString("0.00") & ", " & nID)
            End If
        End If
        txtTotalMin.Text = "< new >"
        txtFeePercent.Text = "new"
        rptGradedTotals.DataBind()
        LoadValues()
    End Sub

    Sub LoadValues()
        Dim drData As SqlDataReader = dbPages.ExecReader("SELECT * FROM tblCompany WHERE ID=" & nMerchant)
        If drData.Read Then
            'txttransCcStorage.Text = CType(drData("transCcStorage"), Decimal).ToString("#,0.00")
            'txtHandlingFee.Text = CType(drData("HandlingFee"), Decimal).ToString("#,0.00")
            If Convert.IsDBNull(drData("CFF_Currency")) Then
                rbGradedFeesOff.Checked = True
                rbGradedFeesOn.Checked = False
            Else
                rbGradedFeesOff.Checked = False
                rbGradedFeesOn.Checked = True
                ddlGradedCurrency.SelectedValue = drData("CFF_Currency")
            End If
        End If
        drData.Close()
        drData = dbPages.ExecReader("SELECT * FROM tblSystemCurrencies Left Join Setting.SetMerchantSettlement mcs ON(tblSystemCurrencies.CUR_ID = mcs.Currency_id And mcs.Merchant_id=" & nMerchant & ")")
        rptMerchantCurrencyFees.DataSource = drData
        rptMerchantCurrencyFees.DataBind()
        drData.Close()
    End Sub

    Protected Sub Page_Load()
        ddlCurrencyFee.Items.Clear()
        dsGradedCurrency.ConnectionString = dbPages.DSN
        dsGradedTotals.ConnectionString = dbPages.DSN
        If Not Page.IsPostBack Then
            LoadValues()
            rbGradedFeesOn.Attributes.Add("onclick", "document.getElementById(""" & rbGradedFeesOff.ClientID & """).checked=!this.checked;")
            rbGradedFeesOff.Attributes.Add("onclick", "document.getElementById(""" & rbGradedFeesOn.ClientID & """).checked=!this.checked;")
        End If
    End Sub

    Sub MerchantCurrencyFees_OnItemDataBound(ByVal sender As Object, e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            If e.Item.DataItem("CUR_ID") <> dbPages.TestVar(e.Item.DataItem("Currency_ID"), 0, -1, -1) Then
                Dim currencyID = e.Item.DataItem("CUR_ID")
                ddlCurrencyFee.Items.Add(New ListItem(Netpay.Bll.Currency.Get(currencyID).IsoCode, currencyID))
            End If
        End If
    End Sub
</script>
<html>
<head runat="server">
    <title>Merchant Fees</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
    <link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
    <script src="../js/func_common.js" type="text/jscript"></script>
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../js/jquery-ui-1.8.10.custom/css/custom-theme/jquery-ui-1.8.10.custom.css" />
    <script type="text/javascript" language="javascript">
        $(function ()
        {
            $("#rbGradedFeesOn").change(function () { toggleGradedFees(); });
            $("#rbGradedFeesOff").change(function () { toggleGradedFees(); });
            toggleGradedFees();
        });

        function toggleGradedFees()
        {
            if ($("#rbGradedFeesOn").attr("checked"))
                $("[id^='gradedFeesData']").css("visibility", "visible");
            else
                $("[id^='gradedFeesData']").css("visibility", "hidden");
        }

        function ResetNextDate(chk)
        {
            var txt = chk;
            for (; txt.nodeName != "TD"; txt = txt.parentNode);
            for (txt = txt.nextSibling; txt.nodeName != "INPUT"; txt = txt.firstChild);
            txt.disabled = !chk.checked;
        }

        function ShowCurrencyFeeRow()
        {
            var currencyDropDown = document.getElementById('<%= ddlCurrencyFee.ClientID %>');
            if (currencyDropDown.selectedIndex < 0) return;
            var currencyID = currencyDropDown.options[currencyDropDown.selectedIndex].value;
            $(currencyDropDown).children("option:selected").remove();
            //currencyDropDown.options.removeChild(currencyDropDown.options[currencyDropDown.selectedIndex]);
            document.getElementById('CurrencyFee' + currencyID).style.display = '';
            document.getElementById('CurrencySettings_' + currencyID + '_Exist').value = 1;
        }
    </script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
    <form runat="server">
        <UC1:TabMenu ID="TabMenu1" Width="95%" runat="server" idFieldName="ID" />
        <table border="0" class="formNormal" cellspacing="0" align="center" width="95%">
            <tr>
                <td style="vertical-align: top; width: 45%;">
                    <div class="MerchantSubHead">
                        Fees By Currency :
                        <asp:DropDownList ID="ddlCurrencyFee" runat="server" />
                        <asp:Button ID="btnCurrencyAddRow" runat="server" Text="Show" UseSubmitBehavior="false" OnClientClick="ShowCurrencyFeeRow();return;" />
                    </div>
                    <br />
                    <asp:Repeater runat="server" ID="rptMerchantCurrencyFees" OnItemDataBound="MerchantCurrencyFees_OnItemDataBound">
                        <HeaderTemplate>
                            <table border="0" cellspacing="2" cellpadding="0">
                                <tr>
                        <th>Currency</th>
                        <th>Wire Fee</th>
                        <th>Storage<br />Fee</th>
                        <th>Handling<br />Fee</th>
                        <th>Min. Merchant<br />Payment Req.</th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr style="display: <%#IIF(Eval("CUR_ID")  = dbPages.TestVar(Eval("Currency_ID"), 0, -1, -1), "", "none")%>;" id="CurrencyFee<%#Eval("CUR_ID")%>">
                                <td>
                                    <asp:Literal runat="server" Text='<%#Eval("CUR_IsoName")%>' />
                                    <asp:HiddenField runat="server" ID="hdCurrency" Value='<%#Eval("CUR_ID")%>' />
                                    <input type="hidden" id="CurrencySettings_<%#Eval("CUR_ID")%>_Exist" name="CurrencySettings_<%#Eval("CUR_ID")%>_Exist" value="<%#IIF(Eval("CUR_ID")  = dbPages.TestVar(Eval("Currency_ID"), 0, -1, -1), "1", "0")%>" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="short2" ID="txtWireFee" Text='<%#CType(IIF(DbNull.Value.Equals(Eval("WireFee")), 0, Eval("WireFee")), Decimal).ToString("#,0.00")%>' />
                                    +
                                    <asp:TextBox runat="server" CssClass="short" ID="txtWireFeePercent" Text='<%#CType(IIF(DbNull.Value.Equals(Eval("WireFeePercent")), 0, Eval("WireFeePercent")), Decimal).ToString("#,0.00")%>' />
                                    %
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="short2" ID="txtStorageFee" Text='<%#CType(IIF(DbNull.Value.Equals(Eval("StorageFee")), 0, Eval("StorageFee")), Decimal).ToString("#,0.00")%>' />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="short2" ID="txtHandling" Text='<%#CType(IIF(DbNull.Value.Equals(Eval("HandlingFee")), 0, Eval("HandlingFee")), Decimal).ToString("#,0.00")%>' />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="short2" ID="txtMinPayout" Text='<%#CType(IIF(DbNull.Value.Equals(Eval("MinPayoutAmount")), 0, Eval("MinPayoutAmount")), Decimal).ToString("#,0.00")%>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
                    <br />
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="2">
                                <div class="MerchantSubHead">
                                    Graded Clearing Fees</div>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rbGradedFeesOff" runat="server" CssClass="option" />
                            </td>
                            <td>
                                Use settings in terminals
                                <AC:DivBox runat="server" Text="Transaction fee will is calculated in accordance with the settings in the FEES &amp; TERMINALS page" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rbGradedFeesOn" runat="server" CssClass="option" />
                            </td>
                            <td>
                                Use the following graded method
                                <AC:DivBox runat="server" Text="Transaction fee is derived from the total processing amount, according the grades specified below." />
                            </td>
                        </tr>
                        <tr id="gradedFeesData1">
                            <td>
                            </td>
                            <td>
                                Currency to use:
                                <asp:DropDownList ID="ddlGradedCurrency" DataSourceID="dsGradedCurrency" DataValueField="CUR_ID" DataTextField="CUR_ISOName" runat="server" />
                                <asp:SqlDataSource ID="dsGradedCurrency" SelectCommand="SELECT * FROM tblSystemCurrencies ORDER BY CUR_ID" runat="server" />
                            </td>
                        </tr>
                        <tr id="gradedFeesData2">
                            <td>
                            </td>
                            <td style="padding-top: 5px;">
                                <table border="0" cellpadding="1" cellspacing="0" class="formNormal" style="width:320px;">
                                    <asp:Repeater ID="rptGradedTotals" DataSourceID="dsGradedTotals" runat="server">
                                        <HeaderTemplate>
                                            <tr>
                                                <th>Minimum Total</th>
                                                <th>Fee Percent</th>
                                                <th style="text-align: center;">Delete</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="hidID" Value='<%#Eval("ID")%>' runat="server" />
                                                    <asp:TextBox ID="txtTotalMin" Text='<%#CType(Eval("TotalMin"), Decimal).ToString("#,0.00")%>' CssClass="medium" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFeePercent" Text='<%#CType(Eval("FeePercent"), Decimal).ToString("#,0.00")%>' CssClass="short" runat="server" />%
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox ID="chkDelete" runat="server" Checked="false" CssClass="option" onclick="if (this.checked) return confirm('Do you really want to delete that grade?');" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:SqlDataSource ID="dsGradedTotals" SelectCommand="SELECT * FROM GetMerchantFeeGrades(@nMerchantID) ORDER BY TotalMin" runat="server">
                                        <SelectParameters>
                                            <asp:QueryStringParameter Name="nMerchantID" QueryStringField="ID" Type="Int32" DefaultValue="0" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                    <tr>
                                        <td>
                                            <asp:HiddenField ID="hidID" Value="0" runat="server" />
                                            <asp:TextBox ID="txtTotalMin" CssClass="medium" runat="server" Text="&lt; new &gt;" onfocus="if (this.value=='&lt; new &gt;') this.value='';" onblur="if (this.value=='') this.value='&lt; new &gt;';" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFeePercent" CssClass="short" runat="server" Text="new" onfocus="if (this.value=='new') this.value='';" onblur="if (this.value=='') this.value='new';" />%
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top; width: 55%; border-left: 1px solid #d2d2d2; padding-left: 50px;">
                    <UC1:PeriodicFees ID="pfAnnual" runat="server" Title="Annual Fees" IsAnnual="true" />
                    <br />
                    <br />
                    <UC1:PeriodicFees ID="pfMonthly" runat="server" Title="Monthly Fees" IsAnnual="false" />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right;">
                    <br />
                    <hr class="separator" />
                    <asp:Button ID="btnSave" Text="Save Changes" OnCommand="SaveData" CommandName="UPDATE" CssClass="buttonWhite" UseSubmitBehavior="true" runat="server" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
