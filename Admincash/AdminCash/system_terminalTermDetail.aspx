<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
	Dim iReader As SqlDataReader
	Dim ChargeMade As Boolean = False
	Dim bIsChargeMade As Boolean = False
	Dim sTerminalNumber As String = ""
	Dim sDebitCompany As String = ""
	Dim DCF_TerminalNumber As String = ""
	Dim DCF_DebitCompanyID As String = ""
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		'If dbPages.TestVar(Request("terminalID"), 1, 0, 0) < 1 Then pnlTRM.Visible = False
		Dim sQueryWhere As String
		SqlDataSource1.ConnectionString = dbPages.DSN
		SqlDataSource2.ConnectionString = dbPages.DSN
		dsManagingCompany.ConnectionString = dbPages.DSN
		dsManagingPSP.ConnectionString = dbPages.DSN
        dsMobileCat.ConnectionString = dbPages.DSN
		If Not Page.IsPostBack Then
			Dim nTerminalID As Integer = dbPages.TestVar(Request("terminalID"), 1, 0, 0)
			If nTerminalID > 0 Then
				litTerminalName.Text = dbPages.ExecScalar("SELECT dc_Name+' | '+TerminalNumber+' | '+dt_Name FROM tblDebitCompany" & _
				" INNER JOIN tblDebitTerminals ON DebitCompany_ID=DebitCompany WHERE ID=" & nTerminalID)
				tlbTop.AddItem("Terminal Details", "system_terminalTermDetail.aspx?terminalID=" & nTerminalID, True)
				tlbTop.AddItem("Card Company Details", "system_terminalCardDetail.aspx?terminalID=" & nTerminalID)
				tlbTop.AddItem("Fees & Payouts", "system_terminalFees.aspx?terminalID=" & nTerminalID)
				tlbTop.AddItem("Notifications", "system_terminalRules.aspx?TerminalID=" & nTerminalID)
				tlbTop.AddItem("Miscellaneous", "system_terminalMisc.aspx?terminalID=" & nTerminalID)
				sQueryWhere = "tblDebitTerminals.id=" & nTerminalID.ToString
				terminalID.Value = nTerminalID
			Else
				sQueryWhere = "tblDebitTerminals.terminalNumber='" & Request("terminalNumber") & "'"
				litTerminalName.Text = "All Debit Companies"
			End If
			Dim sbSQL As New StringBuilder(String.Empty)
			sbSQL.Append("SELECT dbo.GetDecrypted256(accountPassword256) accountPassword, dbo.GetDecrypted256(accountPassword3D256) accountPassword3D,")
			sbSQL.Append(" (SELECT Count(ID) FROM tblCompanyTransPass WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransAnswer,")
			sbSQL.Append(" (SELECT Count(ID) FROM tblCompanyTransFail WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransFail,")
			sbSQL.Append(" (SELECT Count(ID) FROM tblCompanyTransApproval WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransApprovalOnly,")
			sbSQL.Append(" (SELECT Count(ID) FROM tblCompanyTransRemoved WHERE terminalNumber=tblDebitTerminals.terminalNumber) numTransDel,")
			sbSQL.Append(" *, IsNull(dt_ManagingCompany, -1) MngCompany, IsNull(dt_ManagingPSP, -1) MngPSP FROM tblDebitTerminals WHERE " & sQueryWhere)
			iReader = dbPages.ExecReader(sbSQL.ToString)
			If iReader.HasRows Then
				While iReader.Read
					If iReader("isActive") Then
						ltIsActiveTerm.Text = "<span><li type=square style=color:#66cc66;> <span class=txt11>Active</span></li></span>"
					Else
						ltIsActiveTerm.Text = "<span><li type=square style=color:#ff6666;> <span class=txt11>Blocked</span></li></span>"
					End If
					txTerminalNumber.Text = Trim(iReader("terminalNumber"))
					sTerminalNumber = Trim(iReader("terminalNumber"))
					sDebitCompany = Trim(iReader("DebitCompany"))
					DCF_TerminalNumber = Trim(iReader("terminalNumber"))
					DCF_DebitCompanyID = Trim(iReader("DebitCompany"))
					DebitCompany.SelectedValue = sDebitCompany
					txTerminalNumber3D.Text = iReader("terminalNumber3D")
					If CType("" & iReader("processingMethod"), Integer) = 1 Then
						processingMethod.SelectedValue = "1"
					ElseIf CType("" & iReader("processingMethod"), Integer) = 2 Then
						processingMethod.SelectedValue = "2"
					End If
					If iReader("isNetpayTerminal") Then
						isNetpayTerminal.SelectedValue = "1"
					Else
						isNetpayTerminal.SelectedValue = "0"
					End If
					If iReader("dt_mobileCat") IsNot DBNull.Value Then ddlMobileCat.SelectedValue = Trim(iReader("dt_mobileCat"))
					If iReader("dt_SMSShortCode") IsNot DBNull.Value Then txSMSShortCode.Text = Trim(iReader("dt_SMSShortCode"))
					txDt_name.Text = Trim(iReader("dt_name"))
					txDt_mcc.Text = Trim(iReader("dt_mcc"))
					txTerminalMonthlyCost.Text = Trim(iReader("terminalMonthlyCost"))
                    txDt_Descriptor.Text = Trim(iReader("dt_Descriptor"))
                    txdt_BankExternalID.Text = IIf(DBNull.Value.Equals(iReader("dt_BankExternalID")), "", iReader("dt_BankExternalID"))
					txDt_MerchantID.Text = Trim(iReader("dt_ContractNumber"))
					dt_BankAccountID.SelectedValue = Trim(iReader("dt_BankAccountID"))
					txTerminalNotes.Text = Trim(iReader("terminalNotes"))
					If iReader("dt_isManipulateAmount") Then dt_isManipulateAmount.Checked = True
					If iReader("dt_IsRefundBlocked") Then dt_IsRefundBlocked.Checked = True
					If iReader("dt_EnableRecurringBank") Then dt_EnableRecurringBank.Checked = True
					If iReader("dt_EnableAuthorization") Then dt_EnableAuthorization.Checked = True
					If iReader("dt_Enable3dsecure") Then dt_Enable3dsecure.Checked = True
					If iReader("dt_IsPersonalNumberRequired") Then dt_IsPersonalNumberRequired.Checked = True
					If iReader("dt_IsTestTerminal") Then dt_IsTestTerminal.Checked = True
					txAccountId.Text = Trim(iReader("accountId"))
					txAccountId3D.Text = Trim(iReader("accountId3D"))
					txAccountSubId.Text = Trim(iReader("accountSubId"))
					txAccountSubId3D.Text = Trim(iReader("accountSubId3D"))
					txAccountPassword.Text = Trim(iReader("accountPassword"))
					txAccountPassword3D.Text = Trim(iReader("accountPassword3D"))
                    If Not iReader("AuthenticationCode1") Is DBNull.Value Then txtAuthenticationCode1.Text = Trim(iReader("AuthenticationCode1"))
                    If Not iReader("AuthenticationCode3D") Is DBNull.Value Then txtAuthenticationCode13D.Text = Trim(iReader("AuthenticationCode3D"))
					If iReader("isShvaMasterTerminal") Then
						isShvaMasterTerminal.SelectedValue = "1"
					Else
						isShvaMasterTerminal.SelectedValue = "0"
					End If
					'ChargeMade=Not(iReader("numTransAnswer")=0 And iReader("numTransFail")=0 And iReader("numTransApprovalOnly")=0 and iReader("numTransDel")=0)					
					'isChargeMade.Value = ChargeMade
					ddlManagingCompany.SelectedValue = iReader("MngCompany")
					ddlManagingPSP.SelectedValue = iReader("MngPSP")
				End While
			Else
				pnlTRM.Visible = False
			End If
			iReader.Close()
		Else
			sDebitCompany = DebitCompany.SelectedValue
		End If
	End Sub
	
	Private Sub UpdateDetails(ByVal sender As Object, ByVal e As System.EventArgs)
		If len(""&isChargeMade.Value) = 0 Then
			bIsChargeMade = False
		Else
			bIsChargeMade = isChargeMade.Value
		End If
		If Not bIsChargeMade then			
			Dim nOtherTerminal As integer = dbPages.ExecScalar("IF EXISTS (SELECT ID FROM tblDebitTerminals WHERE ID<>" & terminalID.Value & " AND terminalNumber<>'' AND terminalNumber='" & dbpages.DBText(txTerminalNumber.Text.Trim) & "') SELECT 1 ELSE SELECT 0;")
			If nOtherTerminal=1 then
				ltError.Text = "Terminal number is already used, Update was made without it<br />"
			Else
				dbPages.ExecSql("UPDATE tblDebitTerminals SET terminalNumber='" & txTerminalNumber.Text.Trim & "' WHERE id=" & terminalID.Value)
			End If
		End If
		Dim sSQL As New StringBuilder(String.Empty)
		sSQL.Append("UPDATE tblDebitTerminals SET ")
		If NOT bIsChargeMade Then
			sSQL.Append("DebitCompany=" & dbPages.DBText(DebitCompany.SelectedValue) & ",")
		End If
		If Not isShvaMasterTerminal is Nothing then
			If isShvaMasterTerminal.SelectedValue <> "" Then
				sSQL.Append("isShvaMasterTerminal=" & isShvaMasterTerminal.SelectedValue & ",")	
			End If
		End If
		sSQL.Append("dt_name='" & dbPages.DBText((""&txDt_name.Text).Trim) & "',")
		sSQL.Append("dt_mcc='" & dbPages.DBText((""&txDt_mcc.Text).Trim) & "',")
		sSQL.Append("dt_SMSShortCode='" & dbPages.DBText(txSMSShortCode.Text) & "',")
        sSQL.Append("dt_mobileCat=" & dbPages.TestVar(ddlMobileCat.SelectedValue, 0, -1, 0) & ",")
		sSQL.Append("accountId='" & dbPages.DBText((""&txAccountId.Text).Trim) & "',")
        sSQL.Append("accountSubId='" & txAccountSubId.Text.Trim.Replace("'", "''") & "',")
        sSQL.Append("AuthenticationCode1='" & txtAuthenticationCode1.Text.Trim.Replace("'", "''") & "',")
		sSQL.Append("accountPassword256=dbo.GetEncrypted256('" & Replace((""&txAccountPassword.Text).Trim, "'", "''") & "'),")
		sSQL.Append("terminalNumber3D='" & dbPages.DBText((""&txTerminalNumber3D.Text).Trim) & "',")
		sSQL.Append("accountId3D='" & dbPages.DBText((""&txAccountId3D.Text).Trim) & "',")
        sSQL.Append("accountSubId3D='" & txAccountSubId3D.Text.Trim.Replace("'", "''") & "',")
        sSQL.Append("AuthenticationCode3D='" & txtAuthenticationCode13D.Text.Trim.Replace("'", "''") & "',")
        sSQL.Append("accountPassword3D256=dbo.GetEncrypted256('" & Replace(("" & txAccountPassword3D.Text).Trim, "'", "''") & "'),")
		sSQL.Append("isNetpayTerminal=" & isNetpayTerminal.SelectedValue & ",")
		sSQL.Append("processingMethod=" & processingMethod.SelectedValue & ",")
		sSQL.Append("terminalMonthlyCost=" & (""&txTerminalMonthlyCost.Text).Trim & ",")
		sSQL.Append("terminalNotes='" & dbPages.DBText((""&txTerminalNotes.Text).Trim) & "', ")
        sSQL.Append("dt_Descriptor='" & dbPages.DBText(("" & txDt_Descriptor.Text).Trim) & "', ")
        sSQL.Append("dt_ContractNumber='" & dbPages.DBText(("" & txDt_MerchantID.Text).Trim) & "', ")
        sSQL.Append("dt_BankExternalID='" & dbPages.DBText(("" & txdt_BankExternalID.Text).Trim) & "', ")
        sSQL.Append("dt_BankAccountID=" & dbPages.TestVar(dt_BankAccountID.SelectedValue, 0, -1, 0) & ", ")
		sSQL.Append("dt_ManagingCompany=CASE " & dbPages.TestVar(ddlManagingCompany.SelectedValue, -1, 99, -1) & " WHEN -1 THEN NULL ELSE " & ddlManagingCompany.SelectedValue & " END, ")
		sSQL.Append("dt_ManagingPSP=CASE " & dbPages.TestVar(ddlManagingPSP.SelectedValue, -1, 99, -1) & " WHEN -1 THEN NULL ELSE " & ddlManagingPSP.SelectedValue & " END, ")
		sSQL.Append("dt_isManipulateAmount=" & IIf(dt_isManipulateAmount.Checked, 1, 0) & ",")
		sSQL.Append("dt_IsRefundBlocked=" & IIf(dt_IsRefundBlocked.Checked, 1, 0) & ",")
		sSQL.Append("dt_EnableRecurringBank=" & IIf(dt_EnableRecurringBank.Checked, 1, 0) & ",")
		sSQL.Append("dt_EnableAuthorization=" & IIf(dt_EnableAuthorization.Checked, 1, 0) & ",")
		sSQL.Append("dt_Enable3dsecure=" & IIf(dt_Enable3dsecure.Checked, 1, 0) & ",")
		sSQL.Append("dt_IsPersonalNumberRequired=" & IIf(dt_IsPersonalNumberRequired.Checked, 1, 0) & ",")
		sSQL.Append("dt_IsTestTerminal=" & IIf(dt_IsTestTerminal.Checked, 1, 0))
		sSQL.Append("Where id=" & terminalID.Value)
		dbPages.ExecSql(sSQL.ToString)
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Terminals - Terminal Details</title>
    <meta http-equiv="Content-Type" content="text/html;charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading{text-decoration:underline;white-space:nowrap;}
		table.formNormal td, table.formNormal th {text-align:center;}
		.btnSave { width:58px; height:52px; }
	</style>
	<script language="javascript" type="text/javascript" src="../js/func_common.js"></script>	
</head>
<body>
    <form id="form1" runat="server" class="formNormal">
	<table align="center" style="width:95%;">
	<tr>
		<td id="pageMainHeadingTd">
			<asp:label ID="lblPermissions" runat="server" /><span id="pageMainHeading"> TERMINALS</span><span style="letter-spacing:-1px;"> - <asp:Literal ID="litTerminalName" runat="server" /></span>
		</td>
		<td>
			<table width="80" border="0" cellspacing="0" cellpadding="2" align="right" style="border:1px solid #c0c0c0;">
				<tr><td align="center" id="IsActiveTd"><asp:Literal ID="ltIsActiveTerm" runat="server" /><br /></td></tr>
			</table>
		</td>
	</tr>
	</table>
	<br />
	<custom:Toolbar ID="tlbTop" Width="95%" runat="server" />
	<asp:Panel ID="pnlTRM" runat="server">
	<asp:HiddenField ID="terminalID" runat="server" />
	<asp:HiddenField ID="isChargeMade" runat="server" />
	<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
		<tr>
			<td valign="top" style="color:maroon;font-size:12px">
				<asp:Literal ID="ltError" runat="server" />
			</td>
		</tr>
		<tr>
			<td valign="top">
				<span class="MerchantSubHead">Terminal Details</span>				
				<br />
			</td>
		</tr>
	</table>
	<table align="center" border="0" cellpadding="1" cellspacing="0" width="95%">
		<tr><td height="5"></td></tr>
		<tr>
			<td colspan="4>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td>Terminal num.<br /></td>
						<td><asp:TextBox ID="txTerminalNumber" runat="server" style="width:120px" /></td>
						<td width="21%">Debit company<br /></td>
						<td style="font-weight:bold;" height="18px">
							<asp:DropDownList ID="DebitCompany" runat="server" DataSourceID="SqlDataSource1" DataTextField="dc_name" DataValueField="debitCompany_id" />
							<asp:SqlDataSource ID="SqlDataSource1" runat="server" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient"
							 SelectCommand="SELECT debitCompany_id, dc_name FROM tblDebitCompany ORDER BY debitCompany_id" />
						</td>
					</tr>
					<tr>
						<td>3D Terminal num.</td>
						<td><asp:TextBox ID="txTerminalNumber3D" runat="server" style="width:120px" /></td>
						<td>Type<br /></td>
						<td>
							<asp:DropDownList ID="processingMethod" runat="server">
								<asp:ListItem Value="1" Text="To debit company"/>
								<asp:ListItem Value="2" Text="Insert to pending"/>
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td>Name<br /></td>
						<td><asp:TextBox ID="txDt_name" runat="server" style="width:120px" /></td>
						<td>Belongs to <%=dbPages.TestVar(HttpContext.Current.Session("Identity"), -1, "")%><br /></td>
						<td>
							<asp:DropDownList ID="isNetpayTerminal" runat="server">
								<asp:ListItem Value="0" Text="No"/>
								<asp:ListItem Value="1" Text="Yes"/>
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td width="21%">Contract Num.<br /></td>
						<td><asp:TextBox ID="txDt_MerchantID" runat="server" style="width:120px" /></td>
						<td>Monthly pay<br /></td>
						<td><asp:TextBox ID="txTerminalMonthlyCost" runat="server" style="width:120px" /><%=dbPages.GetCurText(eCurrencies.GC_NIS)%></td>
					</tr>
					<tr>
						<td width="21%">Descriptor<br /></td>
						<td><asp:TextBox ID="txDt_Descriptor" runat="server" style="width:120px" /></td>
						<td>Managing PSP<br /></td>
						<td style="font-weight:bold;" height="18px">
							<asp:DropDownList ID="ddlManagingPSP" runat="server" DataSourceID="dsManagingPSP" DataTextField="GD_Text" DataValueField="GD_ID" />
							<asp:SqlDataSource ID="dsManagingPSP" runat="server" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient"
							 SelectCommand="SELECT -1 GD_ID, '' GD_Text UNION SELECT GD_ID, GD_Text FROM GetGlobalData(70)" />
						</td>
					</tr>
					<tr>
						<td width="21%">Mcc<br /></td>
						<td><asp:TextBox ID="txDt_mcc" runat="server" style="width:120px" /></td>
						<td>Managing Company<br /></td>
						<td style="font-weight:bold;" height="18px">
							<asp:DropDownList ID="ddlManagingCompany" runat="server" DataSourceID="dsManagingCompany" DataTextField="GD_Text" DataValueField="GD_ID" />
							<asp:SqlDataSource ID="dsManagingCompany" runat="server" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient"
							 SelectCommand="SELECT -1 GD_ID, '' GD_Text UNION SELECT GD_ID, GD_Text FROM GetGlobalData(69)" />
						</td>
					</tr>
					<tr>
						<td width="21%">Bank Ext ID<br /></td>
						<td><asp:TextBox ID="txdt_BankExternalID" runat="server" style="width:120px" /></td>
					</tr>
					<tr>
						<td>Mobile category<br /></td>
						<td style="font-weight:bold;" height="18px"><%--DataSourceID="dsMobileCat"--%>
							<asp:DropDownList ID="ddlMobileCat" runat="server" DataTextField="Name" DataValueField="PhoneCarrier_id"/>
							<asp:SqlDataSource ID="dsMobileCat" runat="server" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient"
							 SelectCommand="SELECT [PhoneCarrier_id], [Name] FROM [List].[PhoneCarrier] ORDER BY [Name] ASC" />
						</td>
						<td>SMS ShortCode<br /></td>
						<td style="font-weight:bold;" height="18px">
							<asp:TextBox ID="txSMSShortCode" runat="server"  />
						</td>
					</tr>
					<tr>
						<td>Bank Account<br /></td>
						<td colspan="3">
							<asp:DropDownList ID="dt_BankAccountID" runat="server" DataSourceID="SqlDataSource2" DataTextField="BA_Title" DataValueField="BA_ID" />
							<asp:SqlDataSource ID="SqlDataSource2" runat="server" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient"
							 SelectCommand="SELECT 0 As BA_ID, 'None' As BA_Title, 0 As SortOrder, 'None' As BA_BankName, 'None' As BA_AccountName  UNION SELECT BA_ID, (BA_BankName + ': ' + BA_AccountName + '- ' + BA_AccountNumber) As BA_Title, 1 As SortOrder, BA_BankName, BA_AccountName FROM tblBankAccounts ORDER BY SortOrder, BA_BankName, BA_AccountName" />
						</td>
					</tr>
					<tr>
						<td valign="top">Notes<br /></td>
						<td colspan="3">
							<asp:TextBox ID="txTerminalNotes" runat="server" Rows="3" Columns="68" TextMode="MultiLine" />
						</td>
					</tr>
					<tr><td colspan="4" height="24"><hr width="100%" size="1" noshade style="border:1px dashed silver;"></td></tr>
					<tr>
						<td colspan="4" style="text-decoration: underline;">Options<br /></td>
					</tr>
					<tr>
						<td colspan="4">
							<asp:CheckBox ID="dt_isManipulateAmount" runat="server" Text="Manipulate charge amount" CssClass="option" />
							<AC:DivBox Width="335px" runat="server">
								When "Manipulate charge amount" checkbox is checked, transaction amount is reduced by random amount between 0.01 and 0.05
							</AC:DivBox>
							<br />
							<asp:CheckBox ID="dt_IsRefundBlocked" runat="server" Text="Refund is prohibited" CssClass="option" />
							<AC:DivBox runat="server">
								When "Refund is prohibited" checkbox is checked, refunds cannot be processed in this terminal
							</AC:DivBox> 
							<br />
							<asp:CheckBox ID="dt_EnableRecurringBank" runat="server" Text="Enable Bank Recurring" CssClass="option" />
							<AC:DivBox ID="DivBox1" runat="server">
								this flag select the recurring method to use if checked and bank integration supprts recurring the system will use bank's specific recurring feature otherwise use netpay's recurring system.
							</AC:DivBox>
							<br />
							<asp:CheckBox ID="dt_EnableAuthorization" runat="server" Text="Enable Authorization only transaction" CssClass="option" />
							<AC:DivBox ID="DivBox2" runat="server">
								this flag makes authorization only transaction available for the terminal, check only if this feature supported by the bank
							</AC:DivBox>
							<br />
							<asp:CheckBox ID="dt_Enable3dsecure" runat="server" Text="Enable 3D Secure" CssClass="option" />
							<AC:DivBox ID="DivBox3" runat="server">
								this flag makes 3d secure transaction available for the terminal, check only if this feature supported by the bank
							</AC:DivBox>
							<br />
							<asp:CheckBox ID="dt_IsPersonalNumberRequired" runat="server" Text="Requires personal number" CssClass="option" />
							<AC:DivBox ID="DivBox4" runat="server">
								this flag force clients to send their personal id numbers for each transaction
							</AC:DivBox>
							<br />
							<asp:CheckBox ID="dt_IsTestTerminal" runat="server" Text="This is a Test Terminal" CssClass="option" />
							<AC:DivBox ID="DivBox5" runat="server">
								if this terminal is on debit company's test servers (and not in production server) then this box be checked.
							</AC:DivBox>
						</td>
					</tr>
					<tr><td colspan="4" height="10"></td></tr>
					<tr>
						<td colspan="2" style="text-decoration: underline;">Data for debit company<br /></td>
						<td colspan="2" style="text-decoration: underline;">Data for debit company (3D)<br /></td>
					</tr>
					<tr>
						<td>Account name<br /></td>
						<td><asp:TextBox ID="txAccountId" runat="server" style="width:120px" /></td>
						<td>Account name<br /></td>
						<td><asp:TextBox ID="txAccountId3D" runat="server" style="width:120px" /></td>
					</tr>
					<tr>
						<td>User Name<br /></td>
						<td><asp:TextBox ID="txAccountSubId" runat="server" style="width:120px" /></td>
						<td>User Name<br /></td>
						<td><asp:TextBox ID="txAccountSubId3D" runat="server" style="width:120px" /></td>
					</tr>
					<tr>
						<td>Password<br /></td>
						<td><asp:TextBox ID="txAccountPassword" runat="server" style="width:120px" /></td>
						<td>Password<br /></td>
						<td><asp:TextBox ID="txAccountPassword3D" runat="server" style="width:120px" /></td>
					</tr>
					<tr>
						<td>Auth Code 1<br /></td>
						<td><asp:TextBox ID="txtAuthenticationCode1" runat="server" style="width:120px" /></td>
						<td>Auth Code 1<br /></td>
						<td><asp:TextBox ID="txtAuthenticationCode13D" runat="server" style="width:120px" /></td>
					</tr>
					<%
					If IsNumeric(""&sDebitCompany) then
						If int(sDebitCompany)=3 OR int(sDebitCompany)=8 OR int(sDebitCompany)=9 OR int(sDebitCompany)=11 Then
						%>
						<tr><td colspan="4" height="24"><hr width="100%" size="1" noshade style="border:1px dashed silver;"></td></tr>
						<tr>
							<td>Shva terminal type<br /></td>
							<td colspan="3">
								<asp:DropDownList ID="isShvaMasterTerminal" runat="server" Width="80px">
									<asp:ListItem Value="0" Text="Regular"/>
									<asp:ListItem Value="1" Text="Master"/>
								</asp:DropDownList>
							</td>						
						</tr>
						<tr><td colspan="4" height="24"><hr width="100%" size="1" noshade style="border:1px dashed silver;"></td></tr>
						<%
						End If
					End If
					%>
					<tr><td colspan="4"><br /></td></tr>
					<tr><td colspan="4"><asp:Button Text="UPDATE" runat="server" OnClick="UpdateDetails" /></td></tr>
				</table>
			</td>
		</tr>				
	</table>
	</asp:Panel>
    </form>
</body>
</html>
