<%@ Page Language="VB" %>

<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
	
	Dim i, nAmountTotal, nTmpNum As Integer
	Dim sSQL, sQueryWhere, nFromDate, nToDate, sDateDiff, sIconName, sFontColor, sAnd, sQueryString, sTmpTxt, sDivider As String

	Function DisplayContent(ByVal sVar As String) As String
		DisplayContent = "<span class=""key"">" & Replace(Replace(dbPages.dbtextShow(sVar), "=", "</span> = "), "|", " <br /><span class=""key"">")
	End Function
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		nFromDate = Trim(Request("fromDate")) & " " & Trim(Request("fromTime"))
		nToDate = Trim(Request("toDate")) & " " & Trim(Request("toTime"))
		If nFromDate <> " " Then sQueryWhere &= sAnd & "(t.InsertDate >= '" & nFromDate.ToString() & "')" : sAnd = " AND "
		If nToDate <> " " Then sQueryWhere &= sAnd & "(t.InsertDate <= '" & nToDate.ToString() & "')" : sAnd = " AND "

		nFromDate = Trim(Request("PDfromDate")) & " " & Trim(Request("PDfromTime"))
		nToDate = Trim(Request("PDtoDate")) & " " & Trim(Request("PDtoTime"))
		If nFromDate <> " " Then sQueryWhere &= sAnd & "(t.PD >= '" & nFromDate.ToString() & "')" : sAnd = " AND "
		If nToDate <> " " Then sQueryWhere &= sAnd & "(t.PD <= '" & nToDate.ToString() & "')" : sAnd = " AND "
        
        
		If Trim(Request("ShowCompanyID")) <> "" Then sQueryWhere &= sAnd & "(t.companyID = " & Trim(Request("ShowCompanyID")) & ")" : sAnd = " AND "
		If Trim(Request("iCreditType0")) = "" Then sQueryWhere &= sAnd & "(t.CreditType <> 0)" : sAnd = " AND "
		If Trim(Request("iCreditType1")) = "" Then sQueryWhere &= sAnd & "(t.CreditType <> 1)" : sAnd = " AND "
		If Trim(Request("iNotRelease")) <> "" Then sQueryWhere &= sAnd & "(t.OriginalTransID = 0)" : sAnd = " AND "
		If Trim(Request("iAmountFrom")) <> "" Then sQueryWhere &= sAnd & "(t.amount >= " & Trim(Request("iAmountFrom")) & ")" : sAnd = " AND "
		If Trim(Request("iAmountTo")) <> "" Then sQueryWhere &= sAnd & "(t.amount <= " & Trim(Request("iAmountTo")) & ")" : sAnd = " AND "

		If Security.IsLimitedMerchant Then
			sQueryWhere &= sAnd & " t.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
			sAnd = " AND "
		End If
		If Security.IsLimitedDebitCompany Then
			sQueryWhere &= sAnd & " t.DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
			sAnd = " AND "
		End If

		PGData.PageSize = IIf(String.IsNullOrEmpty(Request("iPageSize")), 15, Request("iPageSize"))
        
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		If sQueryWhere <> "" Then sQueryWhere = " AND " & sQueryWhere

		sSQL = "SELECT t.id, t.InsertDate, CompanyID, PrimaryPayedID, t.Currency, Amount, CreditType, PaymentMethodDisplay," & _
		" CompanyName, CUR_ISOName, OriginalTransID, CASE WHEN t.ID>OriginalTransID THEN t.ID ELSE OriginalTransID END BaseTransID," & _
		" CASE WHEN t.CreditType=0 AND OriginalTransID=0 THEN 1 ELSE 0 END IsUnreleased FROM tblCompanyTransPass t" & _
		" INNER JOIN tblCompany m ON CompanyID=m.ID INNER JOIN tblSystemCurrencies ON t.Currency=CUR_ID" & _
		" WHERE t.PaymentMethod=" & ePaymentMethod.PMD_RolRes & sQueryWhere & " ORDER BY IsUnreleased DESC, BaseTransID DESC, t.ID DESC, InsertDate desc"
        If Request("Excel") <> "" Then PGData.PageSize = 1000000
        PGData.OpenDataset(sSQL)
        If Request("Excel") <> "" Then ExportToExcel()
    End Sub
	
	Private Sub ExportToExcel()
		Response.ContentType = "application/vnd.ms-excel"
		Response.AddHeader("Content-Disposition", "attachment;filename=report.xls")
		Response.Write("<table>")
		Response.Write("<tr><th>Date</th><th>Merchant</th><th>Trans</th><th>Settlement</th><th colspan=""2"">Amount</th><th>Comment</th></tr>")
		While PGData.Read()
			Response.Write("<tr>")
			Response.Write("<td>" & CType(PGData("InsertDate"), DateTime).ToString("dd/MM/yy") & "</td>")
			Response.Write("<td>" & PGData("CompanyName") & "</td>")
			Response.Write("<td>" & PGData("id") & "</td>")
			Response.Write("<td>" & PGData("PrimaryPayedID") & "</td>")
			Response.Write("<td>" & dbPages.FormatAmountWCT(PGData("Amount"), PGData("CreditType")) & "</td>")
			Response.Write("<td>" & PGData("CUR_ISOName") & "</td>")
			Response.Write("<td>" & PGData("PaymentMethodDisplay") & "</td>")
			Response.Write("</tr>")
		End While
		PGData.CloseDataset()
		Response.Write("</table>")
		Response.End()
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />

	<script language="JavaScript" src="../js/func_common.js"></script>

	<style type="text/css">
		.dataHeading
		{
			text-decoration: underline;
			white-space: nowrap;
		}
	</style>
</head>
<body>
	<%	If Request("Totals") = "1" Then%>
	<table class="formThin" width="100%">
		<tr>
			<td>
				TOTALS
			</td>
			<td style="text-align: right; cursor: pointer; float: right;" onclick="document.getElementById('tblTotals').style.display='none';">
				<b>X</b>
			</td>
		</tr>
	</table>
	<table class="formNormal" width="250" style="margin: 8px 5px;">
		<tr>
			<th style="text-align: right" width="50%">
				Reserve
				<img src="../images/icon_ArrowR.gif" align="middle" />
			</th>
			<th style="text-align: right">
				Return
				<img src="../images/icon_ArrowG.gif" align="middle" />
			</th>
		</tr>
		<%
			sSQL = "SELECT Currency, CreditType, Sum(Amount) FROM tblCompanyTransPass t" & _
			" WHERE PaymentMethod=" & ePaymentMethod.PMD_RolRes & sQueryWhere & " GROUP BY Currency, CreditType ORDER BY Currency, CreditType"
			Dim nLastCur As Byte = 0
			Dim nAmounts(1) As Decimal
			Dim bHasData As Boolean = False
			Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
			While iReader.Read()
				If nLastCur <> iReader("Currency") Then
					Response.Write("<tr><td style=""text-align:right"">" & dbPages.FormatCurr(nLastCur, nAmounts(0)) & "</td><td style=""text-align:right"">" & dbPages.FormatCurr(nLastCur, nAmounts(1)) & "</td></tr>")
					nAmounts(0) = 0 : nAmounts(1) = 0
					nLastCur = iReader("Currency")
				End If
				nAmounts(iReader("CreditType")) = iReader(2)
				bHasData = True
			End While
			iReader.Close()
			If bHasData Then Response.Write("<tr><td style=""text-align:right"">" & dbPages.FormatCurr(nLastCur, nAmounts(0)) & "</td><td style=""text-align:right"">" & dbPages.FormatCurr(nLastCur, nAmounts(1)) & "</td></tr>")
		%>
	</table>
	<%	Response.End()%>
	<%	End If%>
	<form id="Form1" runat="server">
	<table align="center" style="width: 92%">
		<tr>
			<td id="pageMainHeading">
				<asp:Label ID="lblPermissions" runat="server" />
				ROLLING RESERVE<br />
				<br />
			</td>
		</tr>
		<tr>
			<td style="font-size: 11px;">
				Filters:
				<%
					If Trim(Request("ShowCompanyID")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "ShowCompanyID", Nothing) & "';"">Merchant</a>") : sDivider = ", "
					If sDivider <> "" Then Response.Write(" | (click to remove)") _
					 Else Response.Write("Add to filter by clicking on returned fields")
				%>
			</td>
		</tr>
	</table>
	<table class="formNormal" align="center" style="width: 92%">
		<tr>
			<th style="width: 10px;">
				<br />
			</th>
			<th>
				Date
			</th>
			<th>
				Merchant
			</th>
			<th>
				Trans #
			</th>
			<th>
				Settlement #
			</th>
			<th>
				Amount
			</th>
			<th>
				Comment
			</th>
			<th style="width: 10px;">
				<br />
			</th>
		</tr>
		<%
			
			
			While PGData.Read()
				i = i + 1
				If Trim(PGData("CreditType")) = "0" Then
					sIconName = "icon_ArrowR.gif"
					sFontColor = "Red"
				Else
					sIconName = "icon_ArrowG.gif"
					sFontColor = "#000000"
				End If
		%>
		<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
			<td>
				<%
					If PGData("OriginalTransID") > 0 Then Response.Write(" &nbsp; ")
				%>
				<img src="../images/<%= sIconName %>" align="middle" />
			</td>
			<td nowrap="nowrap">
				<%=CType(PGData("InsertDate"), DateTime).ToString("dd/MM/yy HH:mm") %>
			</td>
			<td style="white-space: nowrap;">
				<%
					If Not IsDBNull(PGData("CompanyName")) Then
						If Trim(Request("ShowCompanyID")) <> "" Then Response.Write(PGData("CompanyName")) _
						 Else dbPages.showFilter(PGData("CompanyName"), "?" & sQueryString & "&ShowCompanyID=" & PGData("CompanyID"), 1)
					End If
				%><br />
			</td>
			<td>
				<%= PGData("id") %>
			</td>
			<td>
				<%= PGData("PrimaryPayedID") %>
			</td>
			<td style="color: <%=sFontColor%>;">
				<%= dbPages.FormatCurrWCT(PGData("Currency"), PGData("Amount"), PGData("CreditType")) %>
			</td>
			<td>
				<%=PGData("PaymentMethodDisplay")%>
			</td>
			<td>
				<a onclick="with(document.getElementById('TrgRR<%=PGData("id")%>')){ if(innerHTML != ''){ innerHTML=''; return; }innerHTML='<iframe framespacing=\'0\' scrolling=\'auto\' marginwidth=\'0\' frameborder=\'0\' src=\'trans_detail_RollingRes.asp?transID=<%=PGData("id")%>&PaymentMethod=4\' height=\'70\' width=\'100%\'></iframe>';}"
					style="cursor: pointer; font-size: 11px;">
					<img src="../images/b_showWhiteEng.gif" align="middle" border="0" /></a>
			</td>
		</tr>
		<tr>
			<td colspan="8" id="TrgRR<%=PGData("id")%>">
			</td>
		</tr>
		<tr>
			<td height="1" colspan="9" bgcolor="silver">
			</td>
		</tr>
		<%
		End While
		PGData.CloseDataset()
		%>
	</table>
	<br />
	<div id="filterIcon" style="visibility: hidden; position: absolute; z-index: 1; font-size: 10px;
		color: Gray; background-color: White;">
		<img src="../images/icon_filterPlus.gif" align="middle">
		ADD TO FILTER
	</div>
	</form>
	<table align="center" style="width: 92%">
		<tr>
			<td>
				<UC:Paging runat="Server" ID="PGData" PageID="PageID" />
			</td>
			<td align="right">
				<input type="button" id="TotalsButton" style="font-size: 11px; cursor: pointer;"
					onclick="with(document.getElementById('tblTotals')){style.display='block';if(onresize)onresize();};setAjaxHTML('?<%=Request.QueryString.ToString()%>&Totals=1', document.getElementById('tblTotals'), true);"
					value="SHOW TOTALS &Sigma;">
			</td>
		</tr>
	</table>
	<div id="tblTotals" onresize="PosObject(this, document.getElementById('TotalsButton'), 'RRTB', 0, -10);"
		style="border-color: #484848; border-style: solid; border-width: 1px 3px; position: absolute;
		width: 260px; background-color: White; display: none;">
		&nbsp; LOADING DATA...
	</div>
</body>
</html>
