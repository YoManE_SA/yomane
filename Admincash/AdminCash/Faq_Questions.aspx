<%@ Page Language="VB" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script language="vbscript" runat="server">
	Dim EditItem As Netpay.Bll.Faqs.Faq
	Dim categoryId As Integer, categoryName As String

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		categoryId = dbPages.TestVar(Request("CategoryID"), 1, 0, 0)
		categoryName = Netpay.Bll.Faqs.Group.Load(categoryId).Name
		LoadQuestions()
	End Sub

	Sub SetFormVisibile(bVisible As Boolean)
		dvEditForm.Visible = bVisible
		If bVisible Then
			ddlCategory.DataSource = Netpay.Bll.Faqs.Group.Search(Nothing, Nothing)
			ddlCategory.DataBind()
		End If
		gvQuestions.Visible = Not dvEditForm.Visible
	End Sub
	
	Sub LoadQuestions()
		gvQuestions.DataSource = Netpay.Bll.Faqs.Faq.Search(New Netpay.Bll.Faqs.Faq.SearchFilters() With { .GroupId = categoryId}, Nothing)
		gvQuestions.DataBind()
		SetFormVisibile(False)
	End Sub

	Sub SelectQuestion(ByVal sender As Object, ByVal e As EventArgs)
		EditItem = Netpay.Bll.Faqs.Faq.Load(gvQuestions.SelectedValue)
		dvEditForm.DataBind()
		ddlCategory.SelectedValue = EditItem.GroupID
		hQstName.Visible = True
		SetFormVisibile(True)
	End Sub

	Sub Create_Click(ByVal src As Object, ByVal e As System.EventArgs)
		EditItem = New Netpay.Bll.Faqs.Faq()
		dvEditForm.DataBind()
		ddlCategory.SelectedValue = categoryId
		hQstName.Visible = False
		SetFormVisibile(True)
	End Sub
	
	Sub Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
		SetFormVisibile(False)
	End Sub
	
	Sub Update_Click(ByVal sender As Object, ByVal e As EventArgs)
		EditItem = Netpay.Bll.Faqs.Faq.Load( hfID.Value)
		If EditItem Is Nothing Then EditItem = new Netpay.Bll.Faqs.Faq()
		EditItem = New Netpay.Bll.Faqs.Faq()
		EditItem.IsVisible = chkVisible.Checked
		EditItem.Answer = txtAnswer.Text
		EditItem.Question = txtQuestion.Text
		EditItem.Rating = txtRating.Text
		EditItem.GroupID = ddlCategory.SelectedValue
		EditItem.Save()
		LoadQuestions()
		ReloadCategories()
	End Sub
	
	Sub ReloadCategories()
		Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">parent.fraMenu.location.href='faq_categories.aspx';</s" & "cript>")
	End Sub
	
	Sub Delete_Command(ByVal src As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
		Netpay.Bll.Faqs.Faq.Load(e.CommandArgument).Delete()
		LoadQuestions()
		ReloadCategories()
	End Sub
	
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Q&A - <%= categoryName %> - Questions</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>

<body style="padding: 10px 40px 0px 18px;">
	<h1><asp:label ID="lblPermissions" runat="server" /> Q&A <span>- <%= categoryName %></span></h1><br />
	<br />
	<form id="frmData" runat="server">
		<div>
			<asp:Button ID="btnAddQuestion" CssClass="buttonWhite buttonSemiWide" runat="server" Text="Add New" OnClick="Create_Click" UseSubmitBehavior="false" /><br />
			<asp:Label ID="lblAddQuestion" runat="server" ForeColor="maroon" Visible="False" /><br />
		</div>
		<asp:GridView ID="gvQuestions" runat="server" AllowPaging="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="ID"
		 SelectedRowStyle-CssClass="rowSelected" OnSelectedIndexChanged="SelectQuestion" BorderWidth="0" Width="100%" CellPadding="2" CellSpacing="2" CssClass="formNormal">
			<Columns>
				<asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" HeaderStyle-CssClass="gridTitleLeft" />
				<asp:BoundField DataField="Question" HeaderText="Question" SortExpression="Question" HeaderStyle-CssClass="gridTitleLeft" />
				<asp:BoundField DataField="Answer" HeaderText="Answer" SortExpression="Answer" HtmlEncode="false" Visible="false" />
				<asp:BoundField DataField="Rating" HeaderText="Position" SortExpression="Rating" ItemStyle-CssClass="centered" ItemStyle-Width="50px" />
				<asp:CheckBoxField DataField="IsVisible" HeaderText="Visible" SortExpression="IsVisible" ItemStyle-CssClass="centered" ItemStyle-Width="50px" />
				<asp:CommandField ButtonType="Button" HeaderText="Edit" ItemStyle-CssClass="buttons centered" ControlStyle-CssClass="buttonWhite" ShowDeleteButton="False" ShowEditButton="False" ShowSelectButton="True" SelectText="Edit" ItemStyle-Width="50px" />
				<asp:TemplateField HeaderText="Delete" ItemStyle-CssClass="buttons centered" ItemStyle-Width="50px">
					<ItemTemplate>
						<asp:Button runat="server" ID="btnDelete" CommandArgument='<%# Eval("ID") %>' OnCommand="Delete_Command" CssClass="button buttonWhite buttonDelete" Text="Delete" OnClientClick="if (!confirm('Do you really want to delete that question ?!')) return false;" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<EmptyDataTemplate>
				<div class="error">There are no questions in this group !</div>
			</EmptyDataTemplate>
		</asp:GridView>

		<div id="dvEditForm" runat="server">
			<br />
			<asp:HiddenField runat="server" ID="hfID" Value='<%# EditItem.ID %>' />
			<h2 runat="server" id="hQstName">EDIT QUESTION: <asp:Literal runat="server" Text='<%# EditItem.Question %>' /></h2><br />
			<br />
			<table style="width:100%;">
				<tr>
					<td style="width:100px;">Question:</td>
					<td><asp:TextBox Width="70%" ID="txtQuestion" runat="server" Text='<%# EditItem.Question %>' /></td>
				</tr>
				<tr>
					<td valign="top">Answer:</td>
					<td><asp:TextBox TextMode="MultiLine" ID="txtAnswer" runat="server" Text='<%# EditItem.Answer %>' Width="70%" /></td>
				</tr>
				<tr>
					<td>IsVisible:</td>
					<td><asp:CheckBox ID="chkVisible" runat="server" Checked='<%# EditItem.IsVisible %>' /></td>
				</tr>
				<tr>
					<td>Position:</td>
					<td><asp:TextBox CssClass="short" ID="txtRating" runat="server" Text='<%# EditItem.Rating %>' /></td>
				</tr>
				<tr>
					<td>Group:</td>
					<td>
						<asp:DropDownList ID="ddlCategory" runat="server" DataTextField="Value" DataValueField="Key" />
					</td>
				</tr>
				<tr>
					<td style="border-bottom-width:0;">&nbsp;</td>
					<td style="border-bottom-width:0;">
						<br />
						<asp:button CssClass="buttonWhite" ID="btnUpdate" runat="server" CausesValidation="True" OnClick="Update_Click" Text="Save" />
						&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:button CssClass="buttonWhite" ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" OnClick="Cancel_Click" />
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>