<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = Date.Today.AddMonths(-6)
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
        
        Security.CheckPermission(lblPermissions)
        If Not Page.IsPostBack Then dsFileExt.ConnectionString = dbPages.DSN
    End Sub
</script>

<asp:Content ContentPlaceHolderID="body" runat="server">
	<form runat="server" action="FileArchiveData.aspx" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" id="MerchantID" name="MerchantID" />
	<h1><asp:label ID="lblPermissions" runat="server" /> FILE ARCHIVE<span> - Filter</span></h1>


	<table class="filterNormal">
		<tr><td><Uc1:FilterMerchants ID="FilterMerchants1" ClientField="MerchantID" Width="150px" runat="server" /></td></tr>
		<tr><td height="6"></td></tr>
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">File Type</th></tr>
		<tr>
			<td colspan="2">
				<asp:Repeater ID="Repeater1" DataSourceID="dsFileExt" runat="server">
					<ItemTemplate>
						<span style="width:65px;float:left;">
							<asp:Literal ID="Literal1" Text='<%# "<input type=""checkbox"" class=""option"" name=""FileExt"" value=""" & Eval("Ext") & """ />" & Eval("Ext") %>' runat="server" />
						</span>
					</ItemTemplate>
				</asp:Repeater>
				<asp:SqlDataSource ID="dsFileExt" SelectCommand="SELECT DISTINCT CASE FileExt WHEN '' THEN '(none)' ELSE Upper(FileExt) END Ext FROM Data.AccountFile" runat="server" />
			</td>
		</tr>
		<tr><th colspan="2"><br />Text Search</th></tr>
		<tr><td colspan="2"><input name="SearchText" class="grayBG" /></td></tr>
		<tr><th colspan="2"><br />Paging</th></tr>
		<tr>
			<td>
				<select name="iPageSize">
					<option value="20" selected="selected">25 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td align="right" style="text-align:right;"><input type="submit" name="Action" value="  SEARCH  "><br /></td>
		</tr>
	</table>
	</form>
</asp:Content>