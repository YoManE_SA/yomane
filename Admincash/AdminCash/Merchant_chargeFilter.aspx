<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>

<script runat="server">
    Sub Page_Load()

    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>CHARGES</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminNetEng.css" />
</head>
<body class="menu">

	<br />
	<h1>CHARGES<span style="letter-spacing:-1px;"> - Filter</span></h1>
	<br />
	
	<table class="filterTable" cellpadding="1" cellspacing="1">
	<tr>
		<td>
			<Uc1:FilterMerchants id="FMerchants" Width="150" ClientField="CompanyID" SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
		         INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" runat="server" />
		</td>
	</tr>
	</table>
		
	<form id="frmFilter" action="Merchant_ChargeForm.aspx" method="get" target="frmBody">
	<input type="hidden" name="CompanyID" />
	    <table class="filterTable" cellpadding="1" cellspacing="1">
        <tr><td height="6"></td></tr>
		<tr><th class="filter">Type</th></tr>
        <tr>
			<td>
				<input type="radio" name="ChargeType" value="1" checked> Credit Card<br />
				<input type="radio" name="ChargeType" value="2"> ECheck<br />
				<input type="radio" name="ChargeType" value="3"> Admin / Fee<br />
				<input type="radio" name="ChargeType" value="4"> Balance<br />		
			</td>
        </tr>
        <tr>
	        <td class="indent" align="right" nowrap="nowrap">
		        <input type="submit" class="buttonFilter" value="PROCEED &gt;" />
	        </td>
        </tr>
	    </table>
	</form>
	
</body>
</html>