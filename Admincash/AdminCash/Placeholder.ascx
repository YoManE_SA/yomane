﻿<%@ Control Language="VB" ClassName="Placeholder" %>
<script runat="server">
	Public Property Text() As String
		Get
			Return txt.Text
		End Get
		Set(ByVal value As String)
			txt.Text = IIf(value.StartsWith("["), String.Empty, "[") & value & IIf(value.EndsWith("]"), String.Empty, "]")
		End Set
	End Property

	Public Property Tooltip() As String
		Get
			Return txt.ToolTip
		End Get
		Set(ByVal value As String)
			txt.ToolTip = value
		End Set
	End Property
</script>
<style type="text/css">
	input.Placeholder
	{
		border:1px dotted Navy;
		color:Navy;
		text-align:center;
		vertical-align:middle;
		cursor:move;
		background-color:transparent;
	}
</style>
<asp:TextBox ID="txt" runat="server" CssClass="Placeholder" ReadOnly="true" onmouseover="focus();select();" />