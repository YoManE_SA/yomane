<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_vheb.asp" -->
<!--#include file="../include/func_CurrencyToWorld.asp" -->
<%
sWireID = trim(request("WireMoney_id"))
sPayIDArray = Split(sWireID, ",", -1, 1)

nCount = 0
sWireID = ""
for each nID in sPayIDArray
	sWireStatus = trim(request("wireAction" & trim(nID)))
	if sWireStatus = "6" then
		nCount = nCount + 1
		if nCount <> 1 then sWireID	= sWireID & ","
		sWireID=sWireID & trim(nID)
	end if
next

'sWireID = "198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,216,217,218"
if sWireID<>"" then
	sSQL="SELECT tblWireMoney.*, IsNull(tblCompany.CustomerNumber, '0000000') CustomerNumber " &_
		"FROM tblWireMoney LEFT JOIN tblCompany ON tblWireMoney.Company_id = tblCompany.ID WHERE tblWireMoney.WireMoney_id IN(" & sWireID & ")"
	set rsData = oledbData.Execute(sSQL)
	if not rsData.EOF then
	
		sDatePay = ""
		sDatePay = sDatePay & right(year(now()),2)
		if month(now())<10 then sDatePay = sDatePay & "0"
		sDatePay = sDatePay & month(now())
		if day(now())<10 then sDatePay = sDatePay & "0"
		sDatePay = sDatePay & day(now())
		
		sLogDate = now()
		
		'xxxxxxxxxxxxxxxxxxx ����� xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		sWireString = "<table border=1><tr>"
		sWireString = sWireString & "<th align=""left"">Amount</th>"
		sWireString = sWireString & "<th align=""left"">Amount in Words</th>"
		sWireString = sWireString & "<th align=""left"">Swift Code</th>"
		sWireString = sWireString & "<th align=""left"">Account Name</th>"
		sWireString = sWireString & "<th align=""left"">Account Number</th>"
		sWireString = sWireString & "<th align=""left"">Bank Name</th>"
		sWireString = sWireString & "<th align=""left"">Bank Address</th>"
		sWireString = sWireString & "<th align=""left"">IBAN</th>"
		sWireString = sWireString & "<th align=""left"">NetPay Unique Payment</th>"
		sWireString = sWireString & "</tr>"
		sWireString = sWireString & VbCrLf
		'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		
		'xxxxxxxxxxxxxxxxxxx ������ xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		sTransPayTotal = 0
		sPayCount = 0
		
		Do Until rsData.EOF
			if rsData("wireAmount") > 0 Then
			  xTrCurrency = TestNumVar(Request("currencyToUse" & rsData("WireMoney_id")), 0, -1, rsData("wireCurrency"))
			  sTransPay = CCur(rsData("wireAmount") * 100)
			  if xTrCurrency <> rsData("wireCurrency") then			  
				sTransPay = ConvertCurrencyWRate(rsData("wireCurrency"), xTrCurrency, rsData("wireExchangeRate"), sTransPay)	  	  
			  end if
			  sWireString = sWireString & "<tr>"
			  sWireString = sWireString & "<td>" & FormatNumber(sTransPay / 100, 0, True, False, True) & "</td>"
			  sWireString = sWireString & "<td>" & num2words(FormatNumber(sTransPay / 100, 0, True, False, False)) & "</td>"
			  sWireString = sWireString & "<td>" & rsData("wirePaymentAbroadSwiftNumber") & "</td>"
			  sWireString = sWireString & "<td>" & rsData("wirePaymentAbroadAccountName") & "</td>"
			  sWireString = sWireString & "<td>" & rsData("wirePaymentAbroadAccountNumber") & "</td>"
			  sWireString = sWireString & "<td>" & rsData("wirePaymentAbroadBankName") & "</td>"
			  sWireString = sWireString & "<td>" & rsData("wirePaymentAbroadBankAddress") & "</td>"
			  sWireString = sWireString & "<td>" & rsData("wirePaymentAbroadIBAN") & "</td>"
			  sWireString = sWireString & "<td>" & rsData("wireCompanyLegalName") & " - " & rsData("WireMoney_id") & "</td>"
			  sWireString = sWireString & "</tr>"
			  sWireString = sWireString & VbCrLf
			  'Add to masav log
			  
			  sPayeeBankDetails = trim(rsData("wirePaymentAbroadAccountName")) & ", " & trim(rsData("wirePaymentAbroadSwiftNumber")) & ", " & trim(rsData("wirePaymentAbroadBankName"))
			  sSQL = "INSERT INTO tblLogMasavDetails (Company_id, WireMoney_id, PayeeName, PayeeBankDetails, Amount, Currency, logMasavFile_id) " &_
	          	"VALUES(" & rsData("Company_id") & "," & rsData("WireMoney_id") & ", '" & rsData("wirePaymentPayeeName") & "', '" & sPayeeBankDetails & "', " & CCur(sTransPay / 100) & ", " & xTrCurrency & "," & Session("WireAvailCur")(xTrCurrency) & ")"
	          oledbData.Execute sSQL
	          oledbData.Execute "Update tblLogMasavFile Set PayCount=PayCount+1, PayAmount=PayAmount+" & CCur(sTransPay / 100) & " Where logMasavFile_id=" & Session("WireAvailCur")(xTrCurrency)
			End if
		  rsData.movenext
		Loop
		'xxxxxxxxxxxxxxxxxxx ��"� xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		'sWireString = sWireString & "<tr>"
		'sWireString = sWireString & "<td>" & sTransPay & "</td>"
		'sWireString = sWireString & "<td>" & sPayCount & "</td>"
		'sWireString = sWireString & "</tr>"
		'sWireString = sWireString & VbCrLf
		'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		sWireString = sWireString & "</table>"
				
		Dim FSO, TSO
		Set FSO = CreateObject("Scripting.FileSystemObject")	
		'Set TSO = FSO.CreateTextFile(MapWirePath("WREXP.HTM"), True)
		Set TSO = FSO.CreateTextFile(MapWirePath("BatchFiles/WIRE_MANUAL.HTM"), True)
			TSO.write sWireString
		TSO.close
		Set TSO = Nothing
		Set FSO = Nothing
	end if

	call rsData.close
	Set rsData = nothing
end if
'response.write "<pre><div align=left dir=ltr><font size=-1>" & sWireString & "</font></div><br /><br /><br /></pre>"
%>