<%@ Page Language="VB" %>
<%@ Import namespace="System.Data.SqlClient"%>

<script runat="server">
	Dim sSQL, sDivStyle, merchantOpenningDate, CountryBlackList, CCF_ListBINs, CCardNumber As String
	Dim IsSystemPay As String = "Closed"
	Dim IsRemoteCharge As String = "Closed"
	Dim IsCustomerPurchase As String = "Closed"
	Dim IsPublicPay As String = "Closed"
	Dim IsBillingAddressMust As String = "No"
	
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Report</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<%
	Dim nCount As Integer = 0, nIndex As Integer = 0, SecurityDeposit As Integer = 0
	Dim LocCreditType() As String = Nothing
	NetpayConst.GetConstArray(43, 1, Nothing, LocCreditType)
	    sSQL = "SELECT dbo.GetCUI(tblCompany.CCardCUI) CardCUI, dbo.GetDecrypted256(tblCompany.CCardNumber256) CCardNumber, tblCompany.*, cl2.Name AS countryPersonal, cl1.Name AS countryCompany" & _
  " FROM tblCompany LEFT OUTER JOIN [List].[CountryList] AS cl1 ON tblCompany.CompanyCountry = cl1.CountryID LEFT OUTER JOIN" & _
  " [List].[CountryList] AS cl2 ON tblCompany.Country = cl2.CountryID WHERE tblCompany.ID IN(" & dbPages.TestNumericList(Request("CompanyID"), nCount) & ")"
	'System.Web.HttpContext.Current.Response.Write(sSQL)
	Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
	While iReader.Read()
		nIndex += 1
		sDivStyle = IIF(nCount = nIndex, "page-break-after:auto;", "page-break-after:always;")
		merchantOpenningDate = iReader("merchantOpenningDate")
		If Trim(merchantOpenningDate) = "01/01/1900" Then merchantOpenningDate = ""
		If iReader("IsSystemPay") Then IsSystemPay = "Open"
		If iReader("IsRemoteCharge") Then IsRemoteCharge = "Open"
		If iReader("IsCustomerPurchase") Then IsCustomerPurchase = "Open"
		If iReader("IsPublicPay") Then IsPublicPay = "Open"
		If iReader("IsBillingAddressMust") Then IsBillingAddressMust = "Yes"
			
		If Trim(iReader("CCardNumber")) <> "" Then CCardNumber = (new String("X",len(iReader("CCardNumber"))-4) & right(iReader("CCardNumber"),4))
		%>
		<div id="div<%=iReader("id")%>" style="<%=sDivStyle%>">
			<table align="center" border="0">
			<tr>
				<td colspan="2" style="font-size:13px;">RISK AND COMMERCIAL CONDITIONS: <%=dbPages.dbTextShow(uCase(iReader("CompanyName")))%></td>
				<td style="font-size:13px;" align="right"><%=now().Date.ToString("dd/MM/yyyy")%></td>
			</tr>
			<tr><td colspan="3"><hr size="1" style="border:1px dotted silver;" /></td></tr>
			<tr>
				<td valign="top">
					<table class="dataList">
					<tr><td colspan="2" style="font-weight:bold;">Company Details</td></tr>
					<tr><th>Opening date:</th><td><%=merchantOpenningDate%></td></tr>
					<tr><th>Company name:</th><td><%=dbPages.dbTextShow(iReader("CompanyName"))%></td></tr>
					<tr><th>Merchant number:</th><td><%=iReader("CustomerNumber")%></td></tr>
					<tr><th>Address:</th><td><%=dbPages.dbTextShow(iReader("CompanyStreet"))%></td></tr>
					<tr><th>City:</th><td><%=dbPages.dbTextShow(iReader("CompanyCity"))%></td></tr>
					<tr><th>Zip code:</th><td><%=iReader("CompanyZIP")%></td></tr>
					<tr><th>Country:</th><td><%=iReader("countryCompany")%></td></tr>
					<tr><th>Phone number:</th><td><%=iReader("CompanyPhone")%></td></tr>
					<tr><th>Fax number:</th><td><%=iReader("CompanyFax")%></td></tr>
					<tr><th>Website url:</th><td><%=dbPages.dbTextShow(iReader("url"))%></td></tr>
					<tr><th>Support mail:</th><td><%=iReader("merchantSupportEmail")%></td></tr>
					<tr><th>Support phone:</th><td><%=iReader("merchantSupportPhoneNum")%></td></tr>
					</table>
					<br />
					<table class="dataList">
					<tr><td colspan="2" style="font-weight:bold;">Personal Details</td></tr>
					<tr><th>Full Name:</th><td><%=dbPages.dbTextShow(iReader("FirstName"))%> <%=dbPages.dbTextShow(iReader("LastName"))%></td></tr>
					<tr><th>ID number:</th><td><%=iReader("IDNumber")%></td></tr>
					<tr><th>Phone number:</th><td><%=iReader("Phone")%></td></tr>
					<tr><th>Mobile number:</th><td><%=iReader("cellular")%></td></tr>
					<tr><th>Address:</th><td><%=dbPages.dbTextShow(iReader("Street"))%></td></tr>
					<tr><th>City:</th><td><%=dbPages.dbTextShow(iReader("City"))%></td></tr>
					<tr><th>zip code:</th><td><%=iReader("ZIP")%></td></tr>
					<tr><th>Country:</th><td><%=iReader("countryPersonal")%></td></tr>
					</table>
				</td>
				<td width="30"></td>
				<td valign="top">
					<table class="dataList">
					<tr><td colspan="2" style="font-weight:bold;">Credit Card Details</td></tr>
					<tr><th>Cardholder's name:</th><td><%=dbPages.dbTextShow(iReader("CCardHolderName"))%></td></tr>
					<tr><th>Card number:</th><td><%=CCardNumber%></td></tr>
					<tr><th>Card validity:</th><td><%=iReader("CCardExpMM")%>/<%=iReader("CCardExpYY")%></td></tr>
					<tr><th>Card CUI:</th><td><%=iReader("CardCUI")%></td></tr>
					</table>
					<br />
					<table class="dataList">
					<tr><td colspan="2" style="font-weight:bold;">Payment Options</td></tr>
					<tr><th>Virtual terminal:</th><td><%=IsSystemPay%></td></tr>
					<tr><th>Silent post:</th><td><%=IsRemoteCharge%></td></tr>
					<tr><th>Hosted payment page:</th><td><%=IsCustomerPurchase%></td></tr>
					<tr><th>Public payment page:</th><td><%=IsPublicPay%></td></tr>
					</table>
					<br />
					<table class="dataList">
					<tr><td colspan="2" style="font-weight:bold;">Risk Management</td></tr>
					<tr><th>Billing address is required:</th><td><%=IsBillingAddressMust%></td></tr>
					<tr><th>Max daily declines per card/24h:</th><td><%=iReader("dailyCcMaxFailCount")%></td></tr>
					<tr><th>Rolling Reserve:</th><td><%If iReader("SecurityDeposit") > 0 Then Response.Write(FormatNumber(iReader("SecurityDeposit"), 2, -1)&" %")%></td></tr>
					<tr><th>Reserve Period:</th><td><%If iReader("SecurityPeriod") > 0 Then Response.Write(FormatNumber(iReader("SecurityPeriod"), 2, -1)&" Months")%></td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<br />
					<table class="dataList">
					<tr><td style="font-weight:bold;">Blocked Countries</td></tr>
					<tr><td>
					<%
					CountryBlackList = dbPages.ExecScalar("SELECT countryBlackList FROM tblCompany WHERE ID =" & iReader("id"))
					If Trim(Replace(CountryBlackList,",","")) = "" Then
						Response.Write("None")
					Else
						Dim strCountryList As String() = CountryBlackList.Split(",")
						For i As Integer = 0 To strCountryList.Length - 1
							Response.Write(NetpayConst.GetCountryName(strCountryList(i)) & ", ")
						Next
					End if
					%>
					</td></tr>
					</table>
					<br />
					<table class="dataList">
					<tr><td colspan="6" style="font-weight:bold;">Restriction Limitation by Credit Card</td></tr>
					<%
					sSQL = "SELECT r.*, Name AS pm_Name FROM tblCreditCardRiskManagement AS r LEFT JOIN List.PaymentMethod AS pm ON CCRM_PaymentMethod=pm.PaymentMethod_id WHERE CCRM_CompanyID=" & iReader("id") & " ORDER BY CCRM_Hours"
					Dim iReader2 As SqlDataReader = dbPages.ExecReader(sSQL)
					If iReader2.HasRows Then
						Response.Write("<tr class=""fieldHeadings"">")
							Response.Write("<td>Payment Method</td>") 
							Response.Write("<td>Credit Type</td>")
							Response.Write("<td>Currency</td>")
							Response.Write("<td style=""text-align:right;"">Days</td>") 
							Response.Write("<td style=""text-align:right;"">Max Trans</td>")
							Response.Write("<td style=""text-align:right;"">Amount</td>")
						Response.Write("</tr>")
						While iReader2.Read()
							Response.Write("<tr>")
								Response.Write("<td>" & iReader2("pm_Name") & "</td>")
								Response.Write("<td>")
								if iReader2("CCRM_CreditType") = 255 Then Response.Write("[All]") Else Response.Write(LocCreditType(iReader2("CCRM_CreditType")))
								Response.Write("</td>")
								If iReader2("CCRM_Currency") >= 0 Then
									Response.Write("<td>" & dbPages.GetCurISO(iReader2("CCRM_Currency")) & "</td>")
								Else
									Response.Write("<td>[All]</td>")
								End If
								Response.Write("<td style=""text-align:right;"">" & iReader2("CCRM_Days") & "</td>")
								Response.Write("<td style=""text-align:right;"">" & iReader2("CCRM_MaxTrans") & "</td>")
								Response.Write("<td style=""text-align:right;"">&nbsp;" & FormatNumber(iReader2("CCRM_Amount"), 2, -1) & "</td>")
								Response.Write("</tr>")
							End While
					Else
						Response.Write("<tr><td>None</td></tr>")		
					End If
					iReader2.Close()
					%>
					</table>
					<br />
					<table class="dataList">
					<tr><td colspan="6" style="font-weight:bold;">Fees</td></tr>
					<%
					sSQL = "SELECT f.*,  Name AS pm_Name FROM tblCompanyCreditFees f LEFT JOIN  List.PaymentMethod AS pm ON CCF_PaymentMethod = pm.PaymentMethod_id WHERE CCF_CompanyID=" & iReader("id") & " AND CCF_IsDisabled=0 ORDER BY CCF_IsDisabled, CCF_ID DESC"
					Dim iReader3 As SqlDataReader = dbPages.ExecReader(sSQL)
					If iReader3.HasRows Then
						Response.Write("<tr class=""fieldHeadings"">")
							Response.Write("<td>Payment Method</td>")
							Response.Write("<td>Countries</td>")
							Response.Write("<td>Currency</td>")
							Response.Write("<td>Trans</td>")
							Response.Write("<td>Clearing</td>")
							Response.Write("<td>Pre-Auth</td>")
							Response.Write("<td>Refund</td>")
							Response.Write("<td>Copy R.</td>")
							Response.Write("<td>Chb</td>")
							Response.Write("<td>Fail</td>")
						Response.Write("</tr>")
						While iReader3.Read()
							If iReader3("CCF_ListBINs") = "" Then CCF_ListBINs = "All"
							Response.Write("<tr>")
								Response.Write("<td>" & iReader3("pm_Name") & "</td>")
								Response.Write("<td>" & CCF_ListBINs & "</td>")
								Response.Write("<td>" & dbPages.GetCurText(iReader3("CCF_CurrencyID")) & "</td>")
								Response.Write("<td>" & FormatNumber(iReader3("CCF_FixedFee"), 2, -1) & "</td>")
								Response.Write("<td>" & FormatNumber(iReader3("CCF_PercentFee"), 2, -1) & "%</td>")
								Response.Write("<td>" & FormatNumber(iReader3("CCF_ApproveFixedFee"), 2, -1) & "</td>")
								Response.Write("<td>" & FormatNumber(iReader3("CCF_RefundFixedFee"), 2, -1) & "</td>")
								Response.Write("<td>" & FormatNumber(iReader3("CCF_ClarificationFee"), 2, -1) & "</td>")
								Response.Write("<td>" & FormatNumber(iReader3("CCF_CBFixedFee"), 2, -1) & "</td>")
								Response.Write("<td>" & FormatNumber(iReader3("CCF_FailFixedFee"), 2, -1) & "</td>")
							Response.Write("</tr>")
						End while
					Else
						Response.Write("<tr><td>None</td></tr>")		
					End If
					iReader3.Close()
					%>
					</table>
				</td>
			</tr>
			<tr><td colspan="3"><hr size="1" style="border:1px dotted silver;" /></td></tr>
			<tr>
				<td colspan="3" style="font-size:12px; padding-top:14px; padding-bottom:42px;">
					I/We <%=dbPages.dbTextShow(iReader("FirstName"))%> <%=dbPages.dbTextShow(iReader("LastName"))%>
					on behalf of the company <%=dbPages.dbTextShow(iReader("CompanyName"))%> acknowledge and agree
					to the above risk and commercial conditions of <%=dbPages.TestVar(HttpContext.Current.Session("Identity"), -1, "")%> Processing System.
				</td>
			</tr>
			<tr>
				<td style="font-size:12px; text-decoration:overline;">Signature of company's authorized signator</td>
				<td></td>
				<td style="font-size:12px; text-align:right; text-decoration:overline;">&nbsp; Place and Date &nbsp;</td>
			</tr>
											  
			</table>
		</div>
		<%
            End while
            iReader.Close()
	%>
	<script language="JavaScript" type="text/javascript">
		window.parent.fraMenu.document.getElementById("bPrint").style.cursor = 'pointer';
		window.parent.fraMenu.document.getElementById("bPrint").disabled = false;
	</script>
</body>
</html>