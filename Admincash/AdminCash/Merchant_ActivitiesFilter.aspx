<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" MasterPageFile="~/AdminCash/FiltersMaster.master" Title="Merchant Status Filter" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
	End Sub
</script>
<asp:Content ContentPlaceHolderID="body" runat="server" >
	<form runat="server" action="Merchant_Activities.aspx" method="get" target="frmBody" onsubmit="return filterForm_Submit(null)">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>

	<h1><asp:label ID="lblPermissions" runat="server" /> Status<span> - Filter</span></h1>
	<table class="filterNormal">
		<tr>
			<td>All Inactive Merchants</td>
			<td align="right">
				<input type="button" class="buttonFilter" value="Show" onclick="parent.frmBody.location.href='Merchant_Activities.aspx?ShowInactive=1';" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Merchants</th></tr>
		<tr>
			<td colspan="2"><textarea name="Merchant" class="grayBG" style="font-size:11px;letter-spacing:-1px;width:100%;height:135px;"></textarea></td>
		</tr>
		<%
			Dim sTitle As String = String.Empty, saNames() As String = Nothing, saColors() As String = Nothing
			Dim nOptions As Integer = 0, sOptions As String = String.Empty, sColors As String = String.Empty
			NetpayConst.GetConstArrayEx(44, "Color", 1, sTitle, saColors, saColors)
			NetpayConst.GetConstArray(44, 1, sTitle, saNames)
			For i As Integer = 0 To saNames.Length - 1
				If saNames(i) <> String.Empty Then
					nOptions += 1
					sOptions &= "<option value=""" & i & """>" & saNames(i) & "</option>"
					sColors &= "<option value="""""" style=""background-color:" & saColors(i) & """>&nbsp;</option>"
				End If
			Next
		%>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2"><%= sTitle %></th></tr>
		<tr>
			<td colspan="2">
				<select name="FilterStatusColor" multiple="multiple" size="<%= nOptions %>" class="grayBG" style="width:12px;">
					<%=sColors%>
				</select><select name="FilterStatus" multiple="multiple" size="<%= nOptions %>" class="grayBG" style="width:200px;">
					<%= sOptions %>
				</select>
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Merchant Groups</th></tr>
		<tr>
			<td colspan="2">
				<select name="MerchantGroup" class="grayBG" multiple="multiple" style="width:100%;height:135px;">
					<%
						Dim drData As SqlDataReader = dbPages.ExecReader("SELECT * FROM tblMerchantGroup ORDER BY mg_Name")
						Do While drData.Read
							Response.Write("<option value=""" & drData("ID") & """>" & drData("mg_Name") & "</option>")
						Loop
					%>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<br />
				<input type="submit" class="buttonFilter" value="Search" />
				<input type="reset" class="buttonFilter buttonDelete" value="Reset" />
			</td>
		</tr>
		</table>
	</form>
</asp:Content>