<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_security.asp"-->
<%
Response.Charset = "windows-1255"
Set fso = Server.CreateObject("Scripting.FileSystemObject")
sSQL = "SELECT tblCompany.CompanyLegalName, tblLogMasavDetails.Company_id, tblWireMoney.WireSourceTbl_id, tblWireMoney.WireMoney_id, tblWireMoney.WireFlag, tblLogMasavDetails.* FROM tblLogMasavDetails" &_
" LEFT JOIN tblCompany ON(tblLogMasavDetails.Company_id = tblCompany.ID)" &_
" LEFT JOIN tblWireMoney ON(tblLogMasavDetails.WireMoney_id = tblWireMoney.WireMoney_id)" &_
" WHERE (tblLogMasavDetails.logMasavFile_id = " & trim(request("logMasavFile_id")) & ")"
If PageSecurity.IsLimitedMerchant Then sSQL = sSQL & " AND tblCompany.ID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
sSQL = sSQL & " ORDER BY tblLogMasavDetails.logMasavDetails_id DESC"
set rsData = oledbData.execute(sSQL)
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">
<%
If not rsData.EOF Then
	%>
	<br />
	<table class="filterNormal" align="left" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<th valign="bottom">Wire ID/ Src ID<br /></th>
		<th valign="bottom">Company Name<br /></th>
		<th valign="bottom">Beneficiary Name<br /></th>
		<th valign="bottom">Bank, Branch, Account<br /></th>
		<th align="right" valign="bottom">Amount<br /></th>
		<th valign="bottom">File<br /></th>
		<th align="center" valign="bottom">Done<br /></th>
	</tr>
	<tr><td height="3"></td></tr>
	<%
	nCount = 0
	Do until rsData.EOF
		nCount = nCount + 1
		%>
		<tr>
			<td><%= rsData("WireMoney_id") %>/ <%= rsData("WireSourceTbl_id") %></td>
			<td><%= dbTextShow(rsData("CompanyLegalName")) %></td>	
			<td><%= dbTextShow(rsData("PayeeName")) %></td>
			<td><%= IIF(Len(rsData("PayeeBankDetails")) > 45, Left(rsData("PayeeBankDetails"), 42) & "...", rsData("PayeeBankDetails"))    %></td>
			<td align="right"><%=FormatCurr(rsData("Currency"), rsData("Amount"))%></td>
			<td>
				<%
				sFileName = ExecScalar("Select Top 1 wmf_FileName From tblWireMoneyFile Where wmf_WireMoneyID=" & rsData("WireMoney_id") & " AND wmf_FileName NOT LIKE '%[?]%' Order By ID Desc", "")
				If sFileName <> "" Then If Not fso.FileExists(MapWirePath("WireFiles/" & sFileName)) Then sFileName = "" 
				If sFileName <> "" Then Response.Write("<a target=""_blank"" href=""?DownloadPrivateFile=" & Server.URLEncode("Wires/WireFiles/" & sFileName) & """>" & GetFileIcon(sFileName) & IIF(Len(sFileName) > 15, Left(sFileName, 15) & "...", sFileName) & "</a>") _
				Else Response.Write("No File")
				%>
			</td>
			<td align="center">
				<img src="../images/checkbox<%If rsData("WireFlag") <> 3 Then Response.Write("_off")%>.gif" align="top" />
				<%
				if rsData("WireFlag") = 3 Then
					%>
					<div id="LMIDInfo<%=rsData("logMasavDetails_id")%>" style="visibility:hidden; left:2px; top:2px; width:210px; background-color:#ffffe6; border:1px solid #353535; position:absolute; z-index: 1; padding:2px 2px 2px 2px;text-align:left;font-size:12px;">
						<%= ExecScalar("Select Top 1 '<b>' + wml_user + '</b><br />' + wml_description From tblWireMoneyLog Where WireMoney_id=" & rsData("WireMoney_id") & " Order By WireMoneyLog_id Desc", "") %>
					</div>
					[<a href="#" onmouseover="showInfo2('LMIDInfo<%=rsData("logMasavDetails_id")%>', -220, 2);" onmouseout="hideInfo('LMIDInfo<%=rsData("logMasavDetails_id")%>');" style="text-decoration:underline;">comment</a>]
					<%
				End if
				%>
			</td>
		</tr>
		<tr><td height="2"></td></tr>
		<%
	rsData.movenext
	Loop
	rsData.Close
	%>
	</table>
	<%
Else
	%>
	No wire transfers found !
	<%
End if

Set rsData = Nothing
Call closeConnection()
%>
</body>
</html>