<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_adoPaging.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_balance.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/CalcTransPayment.asp"-->
<!--#include file="../include/func_invoice.asp"-->
<!--#include file="../include/func_CurrencyToWorld.asp" -->
<%
'---------------------------------------------------------------------------------
'	tblWireMoney
'	wireType: 1="payment to merchants"  2="merchant make payments"
'---------------------------------------------------------------------------------
'20090820 Alex - use exchange rate from tblWireMoney
sLOGON_USER = Request.ServerVariables("LOGON_USER")
bBkgChange = False

Sub DeleteMasav(wrID)
	Dim msvID, lmdid
	msvID = 0 : lmdid = 0
	Set iRs = oledbData.Execute("Select Top 1 * From tblLogMasavDetails Where WireMoney_id=" & wrID & " Order By logMasavDetails_id Desc")
	If Not iRs.EOF Then
		lmdid = iRs("logMasavDetails_id")
		msvID = iRs("logMasavFile_id")
	End if
	iRs.Close
	If lmdid = 0 Then Exit Sub
	oleDbData.Execute "Delete From tblLogMasavDetails Where logMasavDetails_id=" & lmdid
	
	Dim xVal, btCurrency, PayAmount, PayCount
	btCurrency = 0 : PayAmount = 0 : PayCount = 0
	btCurrency = TestNumVar(ExecScalar("Select PayCurrency From tblLogMasavFile Where logMasavFile_id=" & msvID, 0), 0, -1, 0)
	iRs.Open "Select * From tblLogMasavDetails Where logMasavFile_id=" & msvID, oleDbData, 0, 1
	Do While Not iRs.EOF
		PayCount = PayCount + 1
		PayAmount = PayAmount + ConvertCurrency(iRs("Currency"), btCurrency, iRs("Amount"))
	  iRs.MoveNext
	Loop
	iRs.Close
	If PayCount > 0 Then
		xVal = ExecScalar("Select Count(WireMoney_id) From tblWireMoney Where WireFlag <> " & WRF_Done & " And WireMoney_id IN(Select WireMoney_id From tblLogMasavDetails Where logMasavFile_id=" & msvID & ")", 0)
		oledbData.execute("Update tblLogMasavFile Set DoneFlag=" & IIF(xVal > 0, 0, 1) & ", PayCount=" & PayCount & ", PayAmount=" & PayAmount & " Where logMasavFile_id=" & msvID)
	Else
		oledbData.execute("Delete From tblLogMasavFile Where logMasavFile_id=" & msvID)
	End If	
End Sub

Function GetNewestFile(ByVal sPath, bFolder)
    Dim dPrevDate, sNewestFile, oFiles
    dPrevDate = DateSerial(1901, 1, 1)
    sNewestFile = Empty   ' init value
    Set oFSO = CreateObject("Scripting.FileSystemObject")
    If oFSO.FolderExists(sPath) Then
        If bFolder Then 
            Set oFiles = oFSO.GetFolder(sPath).SubFolders
            For Each oFile In oFiles
                If dPrevDate < oFile.DateLastModified Then
                    sNewestFile = oFile.Path
                    dPrevDate = oFile.DateLastModified
                End If
            Next
        Else
            Set oFiles = oFSO.GetFolder(sPath).Files
            For Each oFile In oFiles
                If dPrevDate < oFile.DateLastModified Then
                    sNewestFile = oFile.Path
                    dPrevDate = oFile.DateLastModified
                End If
            Next
        End If
    End If
    Set oFSO = Nothing
    GetNewestFile = sNewestFile
End Function
					
If Trim(request("ActionType")) = "SUBMIT" Then

	Dim nCurrencyAvail
	sSendFaxID = ""
	sCreateTextFileCount = 0
	sExportTextFileCount = 0
	sWireMoney_id = trim(request("WireMoney_id"))
	sPayIDArray = Split(sWireMoney_id, ",", -1, 1)
	Redim nCurrencyFileID(MAX_CURRENCY)
	for each nID in sPayIDArray
		sLogDesc = ""
		sWireStatus = trim(request("wireAction" & trim(nID)))
		if sWireStatus<>"" then
			isContinueUpdate = true
			nInvoice = 0
			sSQL="SELECT wireStatus, wireType, WireSourceTbl_id, company_id, wireAmount, wireFee, wireCurrency, wireProcessingCurrency, WireExchangeRate FROM tblWireMoney WHERE WireMoney_id=" & nID
			set rsData = oledbData.execute(sSQL)
			If Not rsData.EOF Then
				Select Case int(sWireStatus)
					case 1 'Cancel
						If int(rsData("wireType")) = 2 AND int(rsData("wireStatus")) = 0 Then
							call UpdateBalanceStatus(rsData("company_id"), rsData("WireSourceTbl_id"), 2, CBS_Voided)
						Elseif int(rsData("wireStatus")) >= 3 Then
							nWireAmount = IIf(int(rsData("wireType"))=1, -rsData("wireAmount"), rsData("wireAmount"))
							nWireAmount = ConvertCurrencyWRate(rsData("WireCurrency"), rsData("WireProcessingCurrency"),rsData("WireExchangeRate"), nWireAmount)
							call fn_BalanceInsert(rsData("company_id"), rsData("WireSourceTbl_id"), int(rsData("wireType")), "Cancel Payment " & rsData("WireSourceTbl_id"), nWireAmount, rsData("wireProcessingCurrency"), 0, "", False)
							oleDbData.execute "Update tblWireMoney Set WireAmount = (WireAmount + WireFee), WireFee=0 Where WireMoney_id=" & nID
						End if						
					case 3 'Create masav file
						nCurrencyFileID(0) = -1
						sCreateTextFileCount = sCreateTextFileCount+1
						If int(rsData("wireType"))=2 Then call UpdateBalanceStatus(rsData("company_id"), rsData("WireSourceTbl_id"), 2, CBS_Cleared)
					case 4 'Create fax page
						if sSendFaxID<>"" then sSendFaxID = sSendFaxID & ","
						sSendFaxID = sSendFaxID & trim(nID)
						If int(rsData("wireType"))=2 Then call UpdateBalanceStatus(rsData("company_id"), rsData("WireSourceTbl_id"), 2, CBS_Cleared)
					case 5 'Add to balance table (status cleared)
						nWireAmount = ConvertCurrencyWRate(rsData("WireCurrency"), rsData("WireProcessingCurrency"), rsData("WireExchangeRate"), rsData("wireAmount"))
						call fn_BalanceInsert(rsData("company_id"), rsData("WireSourceTbl_id"), int(rsData("wireType")), "Settlement #" & rsData("WireSourceTbl_id"), nWireAmount, rsData("wireProcessingCurrency"), 0, "", False)
						oledbData.Execute "UPDATE tblWireMoney SET WireFlag=" & WRF_Done & " WHERE WireMoney_id=" & nID
					case 6 'Excel
						xTrCurrency = rsData("wireProcessingCurrency")
						nCurrencyFileID(xTrCurrency) = -1
						sExportTextFileCount = sExportTextFileCount+1
						If int(rsData("wireType"))=2 Then 
							call UpdateBalanceStatus(rsData("company_id"), rsData("WireSourceTbl_id"), 2, CBS_Cleared)
						ElseIf int(rsData("wireType"))=1 Then
							nWireFee = TestNumVar(ExecScalar("Select WireFee From Setting.SetMerchantSettlement Where Merchant_ID=" & rsData("company_id") & " And Currency_ID=" & rsData("WireCurrency"), 0), 0, -1, 0)
							oleDbData.execute "Update tblWireMoney Set WireAmount=" & (rsData("WireAmount") - nWireFee) & ", WireFee=" & nWireFee & " Where WireMoney_id=" & nID
						End if	
						'oledbData.Execute "UPDATE tblWireMoney SET WireFlag=" & WRF_Done & " WHERE WireMoney_id=" & nID
						'Update balance table (status cleared)
						'fn_BalanceInsert(rsData("company_id"), rsData("WireSourceTbl_id"), 2, "����� " & rsData("WireSourceTbl_id"), -rsData("wireAmount"), rsData("wireCurrency"), "", True)
				End select

                If rsData("WireType") = 1 Then
                    If TestNumVar(ExecScalar("Select IsAutoInvoice From Setting.SetMerchantSettlement Where Merchant_ID=" & rsData("company_id") & " And Currency_ID=" & rsData("WireCurrency"), 0), 0, -1, 0) <> 0 Then
                        nInvoice = CreateInvoiceForSettlement(rsData("WireSourceTbl_id"))
                    End If
                End If
			Else
				isContinueUpdate = false
			End if
			rsData.close
			Set rsData = nothing
			
			If isContinueUpdate Then
				'Update WireMoney status
				sSQL="UPDATE tblWireMoney SET WireStatus=" & sWireStatus & ", WireStatusDate=getdate(), WireStatusUser='" & sLOGON_USER & "' WHERE WireMoney_id=" & nID
				oledbData.Execute sSQL
				'Save info to log table
				Select Case int(sWireStatus)
					Case 6 sLogDesc = sLogDesc & "Excel was created"
					Case 5 sLogDesc = sLogDesc & "Send to balance"
					Case 4 sLogDesc = sLogDesc & "Wire request was printed"
					Case 3 sLogDesc = sLogDesc & "Sent to Masav"
					Case 2 sLogDesc = sLogDesc & "Set as pending"
					Case 1 sLogDesc = sLogDesc & "Set as cancel"
				End Select
                If nInvoice <> 0 Then sLogDesc = sLogDesc & "  with Invoice #" & nInvoice
				sSQL="INSERT INTO tblWireMoneyLog(WireMoney_id, wml_description, wml_user) " &_
					"VALUES(" & nID & ", '" & sLogDesc & "', '" & sLOGON_USER & "') "
					oledbData.Execute sSQL
			End if
		End if
	Next
	
	sMasavCode = trim(request("MasavCode"))
	Select Case trim(sMasavCode)
		Case "87153201" PayedBankDesc = "��� ����� ���� 405 ����� 382378"
		Case "83663203" PayedBankDesc = "��� ����� ���� 064 ����� 390053"
		Case "MSGPOL528" PayedBankDesc = "MSGPOL528 DB-Bank"
		Case "PBNLAIKI" PayedBankDesc = "Laiki Bank"
		Case "INVIKBANK" PayedBankDesc = "Invik Bank"
		Case "HELNICBNK" PayedBankDesc = "Hellenic Bank"
		Case "INVESTBNK" PayedBankDesc = "Invest Bank"
		Case "BANKOFGEO" PayedBankDesc = "Bank Of Georgia"
		Case "ATLASBNK" PayedBankDesc = "Atlas Bank"
		Case Else PayedBankDesc = sMasavCode
	End Select
	For i = 0 To MAX_CURRENCY
		If nCurrencyFileID(i) = -1 Then
			sSQL = "Set Nocount on INSERT INTO tblLogMasavFile(LogonUser, insertDate, PayedBankCode, PayedBankDesc, PayCurrency) " &_
				"VALUES('" & Request.ServerVariables("LOGON_USER") & "', '" & Now() & "', '" & sMasavCode & "', '" & PayedBankDesc & "'," & i & ")" & _
				"SELECT @@IDENTITY AS logMasavFile_id set nocount off"
			set rsDataTmp = oledbData.execute(sSQL)
			If NOT rsDataTmp.EOF Then nCurrencyFileID(i) = TestNumVar(Cstr(rsDataTmp("logMasavFile_id")), 0, -1, 0)
			rsDataTmp.Close
			'Response.Write(nCurrencyFileID(i) & " " & i & "<br />")
		End if
	Next
	Session("WireAvailCur") = nCurrencyFileID
	if sCreateTextFileCount>0 then Server.Execute("Wire_create_textfile.asp")
	if sExportTextFileCount>0 then Server.Execute("Wire_excell_export.asp")
	For i = 0 To MAX_CURRENCY
		If nCurrencyFileID(i) > 0 Then
			xVal = ExecScalar("Select Count(WireMoney_id) From tblWireMoney Where WireFlag <> " & WRF_Done & " And WireMoney_id IN(Select WireMoney_id From tblLogMasavDetails Where logMasavFile_id=" & nCurrencyFileID(i) & ")", 0)
			oledbData.execute("Update tblLogMasavFile Set DoneFlag=" & IIF(xVal > 0, 0, 1) & " Where logMasavFile_id=" & nCurrencyFileID(i))
		End If	
	Next
Elseif Trim(request("ActionType"))="CLEAN" Then
	sWireMoney_id = trim(request("WireMoney_id"))
	'20090120 Tamir - when resetting wire from balance, generate balance operation "cancel"
	if ExecScalar("SELECT WireStatus FROM tblWireMoney WHERE WireMoney_id=" & sWireMoney_id, -1)=5 then
		sSQL="SELECT WireMoney_id, Company_id, WireCurrency, WireAmount, WireProcessingCurrency, FLOOR(10000 * WireAmount * WireExchangeRate)/10000 AS WireProcessingAmount, " & _
				"WireType, WireSourceTbl_id, WireExchangeRate FROM dbo.tblWireMoney WHERE WireMoney_id=" & sWireMoney_id
		set rsTemp=oledbData.Execute(sSQL)
		nMerchant=rsTemp("Company_id")
		nCurrency=rsTemp("WireProcessingCurrency")
		nAmount=rsTemp("WireProcessingAmount")
		if rsTemp("WireType")=1 then
			nBalanceSourceType=1
			nSettlement=rsTemp("WireSourceTbl_id")
		else
			nBalanceSourceType=3
			nSettlement=0
		end if
		rsTemp.Close
        Call fn_BalanceInsert(nMerchant, nSettlement, nBalanceSourceType, "", Replace(-nAmount, ",", ""), nCurrency, CBS_Cleared, "", False)
		'sSQL="INSERT INTO tblCompanyBalance (company_id, sourceTbl_id, sourceType, amount, currency," & _
		'" status, comment) VALUES (" & nMerchant & ", " & nSettlement & ", " & nBalanceSourceType & "," & _
		'" " & Replace(-nAmount, ",", "") & ", " & nCurrency & ", 0, 'E-bank Transfer #" & sWireMoney_id & " was cancelled.')"
		'oledbData.Execute sSQL
	end if
	'Reset status
	sSQL="UPDATE tblWireMoney SET WireStatus=0, WireFlag=" & WRF_Waiting & " WHERE WireMoney_id=" & sWireMoney_id
	oledbData.Execute sSQL
	'Save info to log table
	sLogDesc = "Reseting selection"
	sSQL="INSERT INTO tblWireMoneyLog(WireMoney_id, wml_description, wml_user) " & _
	"VALUES(" & sWireMoney_id & ", '" & sLogDesc & "', '" & sLOGON_USER & "') "
	oleDbData.execute "Update tblWireMoney Set WireAmount = (WireAmount + WireFee), WireFee=0 Where WireMoney_id=" & sWireMoney_id
	oledbData.Execute sSQL
	DeleteMasav(sWireMoney_id)
Elseif Trim(request("ActionType"))="ChangeCurrency" Then
	nID = TestNumVar(request("ChangeCurrencyWireID"), 1, 0, 0)
	nCurrency = TestNumVar(request("ChangeCurrencyCurrencyID"), 0, MAX_CURRENCY, -1)
	nWreCurrency = TestNumVar(request("WireCurrencyID"), 0, MAX_CURRENCY, -1)
	if nID>0 and nCurrency>=0 then
	    if nWreCurrency > 0 then
	        Dim nExchangeRate
	        nExchangeRate = Round(""&ConvertCurrencyRateWFee(nWreCurrency, nCurrency),5)
	        sSQL="UPDATE tblWireMoney SET WireProcessingCurrency=" & nCurrency & ",WireExchangeRate=" & nExchangeRate & " WHERE WireMoney_id=" & nID
	        oledbData.Execute(sSQL)
	    else
		    sSQL="UPDATE tblWireMoney SET WireProcessingCurrency=" & nCurrency & " WHERE WireMoney_id=" & nID
		    oledbData.Execute(sSQL)
		end if
	end if
End if

If Trim(request("Source")) = "filter" Then
	if request("PageSize") = "" then
		session("PageSize") = 20
	else
		session("PageSize") = int(request("PageSize"))
	end if
Else
	session("PageSize") = 20
End if

sWhere = " WHERE (tblWireMoney.isShow=1) "
sAnd = " AND "

If TestNumVar(request("WireMoneyId"), 1, 0, 0) > 0 Then
	sWhere = sWhere & sAnd
	sWhere= sWhere & " WireMoney_id = " & request("WireMoneyId")
	sAnd=" AND "
Elseif NOT Trim(request("source")) = "filter" Then
	sWhere = sWhere & sAnd
	sWhere= sWhere & " (tblWireMoney.wireStatus=0) AND (tblWireMoney.WireFlag IN(0,1,2,4))"
	sAnd=" AND "
ElseIf TestNumVar(request("IdFrom"), 1, 0, 0)>0 Then
	sWhere = sWhere & sAnd & " tblWireMoney.WireMoney_id" & IIf(Trim(Request("IdTo"))<>"",">=","=") & Request("IdFrom") : sAnd=" AND "
	If TestNumVar(request("IdTo"), 1, 0, 0)>0 Then sWhere = sWhere & sAnd & " (tblWireMoney.WireMoney_id<=" & Request("IdTo") & ")" : sAnd=" AND "
ElseIf TestNumVar(request("IdTo"), 1, 0, 0)>0 Then
	sWhere = sWhere & sAnd & " tblWireMoney.WireMoney_id=" & Request("IdTo") : sAnd=" AND "
Else
	dDateMax= Trim(Request("toDate") & " " & Request("toTime"))
	dDateMin= Trim(Request("fromDate") & " " & Request("fromTime"))
		
	If Trim(request("ShowCompanyID"))<>"" AND Trim(request("ShowCompanyID"))<>"null" then sWhere=sWhere & sAnd & " (tblWireMoney.Company_id=" & trim(request("ShowCompanyID")) & ")" : sAnd=" AND "
	If IsDate(dDateMax) then sWhere=sWhere & sAnd & " (tblWireMoney.WireDate<='" & dDateMax & "')" : sAnd=" AND "
	If IsDate(dDateMin) then sWhere=sWhere & sAnd & " (tblWireMoney.WireDate>='" & dDateMin & "')" : sAnd=" AND "
	If Trim(request("WireType"))<>"" then sWhere=sWhere & sAnd & " (tblWireMoney.WireType=" & trim(request("WireType")) & ")" : sAnd=" AND "

	dim nPayID : nPayID=TestNumVar(request("PayID"), 1, 0, 0)
	If nPayID>0 then sWhere=sWhere & sAnd & " (tblWireMoney.WireSourceTbl_id=" & nPayID & ")" : sAnd=" AND "
	If trim(request("wireStatus"))<>"" Then sWhere = sWhere & sAnd & " (tblWireMoney.wireStatus IN(" & request("wireStatus") & "))" : sAnd=" AND "
	If trim(request("WireFlag"))<>"" Then sWhere = sWhere & sAnd & " (tblWireMoney.WireFlag IN(" & request("WireFlag") & "))" : sAnd=" AND "
	If trim(request("iCurrency"))<>"" Then sWhere = sWhere & sAnd & " (tblWireMoney.WireCurrency=" & trim(request("iCurrency")) & ")" : sAnd=" AND "
	If trim(request("amountFrom"))<>"" Then sWhere=sWhere & sAnd & " (tblWireMoney.wireAmount>=" & request("amountFrom") & ")" : sAnd=" AND "
	If trim(request("amountTo"))<>"" Then sWhere=sWhere & sAnd & " (tblWireMoney.wireAmount<=" & request("amountTo") & ")" : sAnd=" AND "
	If TestNumVar(Request("ParseFrom"), 0, -1, 0) <> 0 Or TestNumVar(Request("ParseTo"), 0, -1, 0) <> 0 Then 
		sWhere=sWhere & sAnd & " tblWireMoney.WireMoney_id IN(Select wmf_WireMoneyID From tblWireMoneyFile Where "
		If TestNumVar(Request("ParseFrom"), 0, -1, 0) <> 0 Then sWhere=sWhere & " wmf_ParseResult>=" & TestNumVar(Request("ParseFrom"), 0, -1, 0)
		If TestNumVar(Request("ParseTo"), 0, -1, 0) <> 0 Then 
			If TestNumVar(Request("ParseFrom"), 0, -1, 0) <> 0 Then sWhere=sWhere & " And"
			sWhere=sWhere & " wmf_ParseResult<=" & TestNumVar(Request("ParseTo"), 0, -1, 0)
		End If
		sWhere=sWhere & ")"
		sAnd=" AND "
	End If
End if
If PageSecurity.IsLimitedMerchant Then
	sWhere = sWhere & sAnd & " tblWireMoney.Company_id IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
End If
Set rsData = server.createobject("adodb.recordset")
If Request.QueryString("Totals") = "1" Then
	Response.CharSet = "windows-1255"
	%>
	<table class="formThin" width="100%">
	<tr>
		<td>TOTALS</td>
		<td style="text-align:right;cursor:pointer;float:right;" onclick="document.getElementById('tblTotals').style.display='none';"><b>X</b></td>
	</tr>
	</table>
	<table class="formNormal" width="250" style="margin:8px 5px;">
	<tr>
		<th style="text-align:right" width="50%">Count</th>
		<th style="text-align:right">Amount</th>
	</tr>
	<%
	rsData.Open	"Select WireCurrency, Count(*), Sum(WireAmount) FROM tblWireMoney WHERE WireMoney_id IN (SELECT WireMoney_id FROM tblWireMoney " & _
		" LEFT JOIN tblCompany ON tblWireMoney.Company_id = tblCompany.ID" & _
		sWhere & ") Group By WireCurrency ORDER BY WireCurrency", oleDbData, 0, 1
	Do While Not rsData.EOF
		Response.Write("<tr><td style=""text-align:right"">" & rsData(1) & "</td><td style=""text-align:right"">" & FormatCurr(rsData(0), rsData(2)) & "</td></tr>")
	  rsData.MoveNext
	Loop
	rsData.Close
	%>
	</table>
	<table class="formNormal" width="250" style="margin:8px 5px;">
	<tr>
		<th>Status</th>
		<th style="text-align:right">Count</th>
		<th style="text-align:right">Amount</th>
	</tr>
	<%
	rsData.Open	"Select WireCurrency, Count(*) Quantity, Sum(WireAmount) Amount, WireFlag, GD_Text, GD_Description" & _
	" FROM tblWireMoney LEFT JOIN tblGlobalData ON tblWireMoney.WireFlag = tblGlobalData.GD_ID AND GD_Group=48 AND GD_Lng=1" & _
	" WHERE WireMoney_id IN (SELECT WireMoney_id FROM tblWireMoney " & _
	sWhere & ") Group By WireFlag, GD_Text, GD_Description, WireCurrency ORDER BY WireFlag, WireCurrency", oleDbData, 0, 1
	Do While Not rsData.EOF
		%>
			<tr>
				<td>
					<img src="../images/<%= rsData("GD_Description") %>" alt="<%= rsData("GD_Text") %>" title="<%= rsData("GD_Text") %>" /><%= rsData("GD_Text") %>
				</td>
				<td style="text-align:right"><%= rsData("Quantity") %></td>
				<td style="text-align:right"><%= FormatCurr(rsData("WireCurrency"), rsData("Amount")) %></td>
			</tr>
		<%
	  rsData.MoveNext
	Loop
	rsData.Close
	%>
	</table>
	<%
	Set rsData = nothing
	call closeConnection()
	Response.End()
End If

sSQL="SELECT tblWireMoney.*, tblCompany.CompanyName, tblAffiliates.name As AffiliateName, " & _
    " tblCompany.PreferredWireType, tblGlobalData.*, mcs.WireFee as ExpectedWireFee, lpm.Name as settlementMethod" & _
    " FROM tblWireMoney " & _
    " LEFT JOIN tblCompany ON tblWireMoney.Company_id = tblCompany.ID" & _
    " LEFT JOIN Setting.SetMerchantSettlement mcs ON mcs.Merchant_ID = tblWireMoney.Company_id And mcs.Currency_ID = tblWireMoney.WireCurrency" & _
    " LEFT JOIN tblAffiliates ON tblWireMoney.AffiliateID = tblAffiliates.affiliates_id" & _
    " LEFT JOIN tblGlobalData ON(GD_Group=48 AND GD_Lng=1 AND tblWireMoney.WireFlag=tblGlobalData.GD_ID) " & _
    " LEFT JOIN tblTransactionPay tp ON tblWireMoney.SettlementID = tp.id" & _
    " LEFT JOIN List.PaymentMethod lpm ON tp.PaymentMethod_id = lpm.PaymentMethod_id" & _
    sWhere & " ORDER BY tblWireMoney.WireDate DESC"
'Response.Write(sSQL)
call openRsDataPaging(session("PageSize"))
sQueryString = UrlWithout(UrlWithout(UrlWithout(UrlWithout(Cstr(Request.QueryString), "ActionType"), "ChangeCurrencyWireID"), "ChangeCurrencyCurrencyID"), "page")
%>
<!DOCTYPE html>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<script type="text/javascript" src="../js/func_common.js"></script>
	<script type="text/javascript">
	    <%
		If sSendFaxID<>"" then
			%>
			function openFaxWin() {
			    OpenPop('Wire_fax_browser.asp?SendFaxID=<%= sSendFaxID %>','SendFaxWin',700,500,1);
			}
			<%
		End If
		%>
		function toggleEditing(objId, bkgColor) {	
		    objRef = document.getElementById('tdRow' + objId)
		    objRef2 = document.getElementById('trRow' + objId)
	
		    if (objRef.innerHTML == '') {
		        objRef.innerHTML='<iframe id=iFrame'+objId+' framespacing="0" scrolling="No" marginwidth="0" frameborder="0" border="0" width="100%" height="0px" src="Wire_commentUpd.asp?WireMoney_id='+objId+'&bkgColor='+escape(bkgColor)+'" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>'
		        //objRef.style.borderTop = '1px dashed silver';
		        objRef2.style.display = 'table-row';
		    }
		    else {
		        objRef.innerHTML=''
		        //objRef.style.borderTop = '';
		        objRef2.style.display = 'none';
		    }
		}
	    function ChangeCurrency(nID, nCurrency, nWireCurrency) {
	        location.replace("wire_search_data.asp?ActionType=ChangeCurrency&ChangeCurrencyWireID="+nID+"&WireCurrencyID=" +nWireCurrency+ "&ChangeCurrencyCurrencyID="+nCurrency+"&<%= UrlWithout(UrlWithout(UrlWithout(UrlWithout(sQueryString, "ActionType"), "ChangeCurrencyWireID"), "ChangeCurrencyCurrencyID"), "WireCurrencyID") %>");
	    }
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" <% If trim(sSendFaxID)<>"" then %>onload="openFaxWin()"<% End If %>>
<table align="center" border="0" cellpadding="1" cellspacing="2" width="95%">
<tr>
	<td id="pageMainHeadingTd">
		<span id="pageMainHeading">Wire Transfer</span>
		<span class="txt14">
		<%
		If Trim(request("source")) = "filter" Then
			Response.Write("- SEARCH RESULTS")
		Elseif Trim(request("WireMoneyId")) <> "" Then
			Response.Write("- " & Trim(request("WireMoneyId")))
		Else
			Response.Write("- NEED ATTENTION")
		End If
		%></span><br />
	</td>
	<td></td>
	<td style="text-align:right;">
		<table border="0" cellspacing="0" cellpadding="1">
		<tr>
			<td width="6" bgcolor="#7d7d7d"></td>
			<td style="padding-right:10px;">Waiting</td>
			<td width="6" bgcolor="#FFD400"></td>
			<td style="padding-right:10px;">Partially Approved</td>
			<td width="6" bgcolor="#66cc66"></td>
			<td style="padding-right:10px;">Approved</td>
			<td width="6" bgcolor="#ff6666"></td>
			<td style="padding-right:10px;">Rejected</td>
			<td width="6" bgcolor="#6699cc"></td>
			<td style="padding-right:10px;">Done</td>
			<td width="30"></td>
			<td>
				<span style="float:right;white-space:nowrap;" />
					<%
					sPaymentsBGColor="#FFF4B3"
					sLegendStyle="border-top:1px solid silver;border-bottom:1px solid silver;font:normal 11px;height:1.5em;"
					%>
					<span style="<%= sLegendStyle %>">&nbsp;Settlements&nbsp;</span>
					<span style="background-color:<%= sPaymentsBGColor %>;<%= sLegendStyle %>">&nbsp;Payments&nbsp;</span>
				</span>
			</td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<br />
<%
pageSecurityLevel = 0
pageClosingTags = ""
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
%>
<form action="wire_search_data.asp?<%= sQueryString %>" name="frmSearchWire" id="frmSearchWire" target="frmBody" method="post">
<table width="95%" border="0" cellspacing="0" cellpadding="1" align="center">
<%
if not rsData.EOF then
	%>
	<tr>
		<td>
			<table class="formNormal" width="100%" cellspacing="1" cellpadding="0">	
			<tr>
				<th colspan="2">Wire Num. /<br />Create Date</th>
				<th>Due Date /<br />Last Action</th>
				<th>Wire Amount<br /></th>
				<th>Fee Taken /<br />Fee Expected<br /></th>
				<th>Merchant Name /<br />Payee Name<br /></th>
				<th>Merchant<br />Balance<br /></th>
				<th style="text-align:center;">Currency<br />To Use<br /></th>
				<th style="text-align:center;">Update<br/>& Info<br/></th>
				<th class="Vertical" width="20">Status</th>
				<td width="25"><br /></td>
				<th class="Vertical" width="20">Hold<br /></th>
				<th class="Vertical" width="20">Manual<br /></th>
				<th class="Vertical" width="20">Fax<br /></th>
				<th class="Vertical" width="20">Masav<br /></th>
				<th class="Vertical" width="20">Hellenic<br /></th>
				<th class="Vertical" width="20">BOG<br /></th>
				<th class="Vertical" width="20">E-Bank<br /></th>
				<th class="Vertical" width="20">FNB<br /></th>
				<th style="text-align:center;">Reset<br /></th>
				<th class="Vertical" width="20">Cancel<br /></th>
			</tr>
			<tr><td height="3"></td></tr>
			<%
			nShowCounter = 0
			rsData.MoveFirst
			Do until rsData.EOF
				
				sCompanyID = trim(rsData("Company_id"))
				sCompanyName = Trim(IIF(rsData("Company_id") <> 0, rsData("CompanyName"), rsData("AffiliateName")))
				nWireMoneyID = trim(rsData("WireMoney_id"))
				sWireCompanyName = trim(rsData("wireCompanyName"))
				sWireCompanyLegalNumber = trim(rsData("wireCompanyLegalNumber"))
				sPaymentBank = trim(rsData("wirePaymentBank"))
				sPaymentBranch = trim(rsData("wirePaymentBranch"))
				sPaymentAccount = trim(rsData("wirePaymentAccount"))
				sPaymentPayeeName = trim(rsData("wirePaymentPayeeName"))
				sIDnumber = trim(rsData("WireIDnumber"))
				sWirePaymentAbroadAccountName = trim(rsData("WirePaymentAbroadAccountName"))
				sWirePaymentAbroadAccountNumber = trim(rsData("WirePaymentAbroadAccountNumber"))
				sWirePaymentAbroadBankName = trim(rsData("WirePaymentAbroadBankName"))
				sWirePaymentAbroadBankAddress = trim(rsData("WirePaymentAbroadBankAddress"))
				sWirePaymentAbroadSwiftNumber = trim(rsData("WirePaymentAbroadSwiftNumber"))
				
				isPaymentLocalOK = false
				If sPaymentPayeeName<>"" AND sPaymentBank>0 AND sPaymentBranch<>"" AND sPaymentAccount<>"" AND (sWireCompanyLegalNumber<>"" OR sIDnumber<>"") then
					If IsNumeric(sPaymentBranch) AND IsNumeric(sPaymentAccount) AND (IsNumeric(sWireCompanyLegalNumber) OR IsNumeric(sIDnumber)) then
						If len(sPaymentBranch)<=3 And rsData("wireProcessingCurrency")=0 Then
							isPaymentLocalOK = true
						end if
					End if
				End if
				isPaymentAbroadOK = false
				If sWirePaymentAbroadAccountName<>"" AND sWirePaymentAbroadAccountNumber<>"" AND sWirePaymentAbroadBankName<>"" AND ((sWirePaymentAbroadBankAddress<>"" AND sWirePaymentAbroadSwiftNumber<>"") Or rsData("WirePaymentAbroadBankAddressCountry") = 2) then
					isPaymentAbroadOK = true
				End if
				
				bBkgChange = NOT bBkgChange
				sTdStyle = IIF(bBkgChange,"background-color:#ffffff;", "background-color:#f0f0f0;")
				sInputStyle = IIF(bBkgChange,"background-color:#ffffff;", "background-color:#f0f0f0;")
				sTdFirstStyle = IIF(rsData("WireType") = 2,"background-color:#FFFCCA;","")
				
				sRButton1 = "" : sRButton2 = "" : sRButton3 = "" : sRButton4 = "" : sRButton5 = "" :  sRButton6 = "" : sRButton7="" : sRButton8="" : sRButton9=""
				Select Case int(rsData("WireStatus"))
					Case 0
						sRButtonShow = true
                        sRButtonWhereV = "0"
						if (rsData("WireFlag") <> WRF_Approved) Then 
							sRButton1 = "disabled" : sRButton2 = "disabled" : sRButton3 = "disabled" : sRButton4 = "disabled" : sRButton5 = "disabled" :  sRButton6 = "disabled" : sRButton7 = "disabled" : sRButton8 = "disabled"
						Else
							If NOT isPaymentLocalOK Then
								sRButton3 = "disabled"
							End If
							If NOT isPaymentAbroadOK Then
								sRButton7 = "disabled"
								sRButton8 = "disabled"
                                sRButton9 = "disabled"
							End If
							If NOT isPaymentLocalOK AND NOT isPaymentAbroadOK then sRButton4 = "disabled"
									If int(rsData("WireType")) = 2 Then sRButton5 = "disabled" 'from merchant make payments - no insert to balance
							If rsData("WireAmount") <= 0 then sRButton6 = "disabled" 'wire amount is minus
						End if
					Case Else
						sRButtonShow = false
						sRButtonWhereV = Cstr(rsData("WireStatus"))
				End Select
				If rsData("wireProcessingCurrency")<>1 And rsData("wireProcessingCurrency")<>2 And rsData("wireProcessingCurrency")<>3 Then 
					sRButton7="disabled"
					sRButton8="disabled"
				End If
				isAllowAction = true
				sLinkColor=""
				If int(rsData("WireStatus")) >= 3 then
					sImageName = "icon_edit_none.gif"
					sShowLink = false
				ElseIf (Not (isPaymentLocalOK Or isPaymentAbroadOK)) AND int(rsData("WireStatus")) = 0 then
					sImageName = "icon_edit.gif"
					sTdStyle = sTdStyle & "color:#9b2424;"
					sShowLink = true
					sLinkColor="#9b2424"
					isAllowAction = false
				Else	
					sImageName = "icon_edit_gray.gif"
					sShowLink = true
				End if
				%>
				<tr>
					<td width="3" style="background-color:<%= rsData("GD_Color") %>">
                        <input type="hidden" name="WireMoney_id" value="<%= rsData("WireMoney_id") %>"/>
					</td>
					<td style="<%= sTdStyle & sTdFirstStyle %>">
                        <%= nWireMoneyID %> <%=IIF(rsData("settlementMethod") <> "", "(" & rsData("settlementMethod") & ")", "") %><br />
                        <%= FormatDatesTimes(rsData("WireDate"),1,1,0) %><br />
					</td>
					<td style="<%= sTdStyle %>">
						<div><%= FormatDatesTimes(rsData("WireInsertDate"),1,0,0) %></div>
						<div><%= iif (isnull(rsData("LastLogDate")), "---", FormatDatesTimes(rsData("LastLogDate"),1,0,0)) %></div>
					</td>
					<td style="<% If rsData("WireAmount")<0 Then response.Write "color:Maroon;" %><%= sTdStyle %>">
						<%= FormatCurr(rsData("WireCurrency"), rsData("WireAmount")) %><br />
						<%
							If rsData("WireProcessingCurrency")-rsData("WireCurrency")<>0 Then
								'Response.Write "(" & FormatCurr(rsData("WireProcessingCurrency"), ConvertCurrency(rsData("WireCurrency"), rsData("WireProcessingCurrency"), rsData("WireAmount"))) & ")"
								Response.Write "(" & FormatCurr(rsData("WireProcessingCurrency"), ConvertCurrencyWRate(rsData("WireCurrency"), rsData("WireProcessingCurrency"),rsData("WireExchangeRate"), rsData("WireAmount"))) & ")"
							Else
								Response.Write "<span style=""color:Gray;"">---</span>"
							End if
						%>
					</td>
					<td style="<%= sTdStyle %>">
						<%=response.write(FormatCurr(rsData("WireCurrency"), rsData("WireFee")))%><br />
						<%If rsData("Company_id") > 0 Then %>
						(<%=response.write(FormatCurr(rsData("WireCurrency"), rsData("ExpectedWireFee")))%>)
					    <%End If%>	
					</td>
					<td style="<%= sTdStyle %>">
						<table cellpadding="0" cellspacing="0">
						<tr>
							<td><a href="#" onclick="OpenPop('wire_details_Bank.aspx?ID=<%= nWireMoneyID %>','fraCompanyUpdate<%= nWireMoneyID %>',650,650,1);"><img src="../images/<%= sImageName %>" alt="" width="13" height="14" border="0" align="middle"></a></td>
							<td>
								<%
								response.write("<a href=""" & IIF(rsData("Company_id") > 0, "merchant_data.asp?companyID=" & sCompanyID, "Partners_data.aspx?id=" & rsData("AffiliateID")) & """ class=""go"">")
								If int(len(dbtextShow(sCompanyName)))<16 then
									response.write(dbtextShow(sCompanyName) & "</a><br />")
								else
									response.write("<span title=""" & sPaymentPayeeName & """>" & left(dbtextShow(sCompanyName),15) & "..</span></a><br />")
								end if
								If int(len(dbtextShow(sPaymentPayeeName)))<16 then
									response.write(dbtextShow(sPaymentPayeeName) & "<br />")
								else
									response.write("<span title=""" & sPaymentPayeeName & """>" & left(dbtextShow(sPaymentPayeeName),15) & "..</span><br />")
								end if
								%>							
							</td>
						</tr>
						</table>
					</td>
					<td valign="middle" style="<%= sTdStyle %>">
						<%
                        Dim wireCur : wireCur = CByte(rsData("wireCurrency"))
                        Set balances = fn_GetBalances(rsData("Company_id"))
                        If balances.Exists(wireCur) Then Response.Write("<div style=""color:gray;""><span style=""color:" & IIF(balances.Item(wireCur).Expected <= 0, "#9b2424", "#000000") & ";"">" & FormatCurr(wireCur, balances.Item(wireCur).Expected) & "</span></div>")
						If rsData("WireProcessingCurrency")-rsData("WireCurrency")<>0 Then
                            wireCur = CByte(rsData("WireProcessingCurrency"))
                            If balances.Exists(wireCur) Then Response.Write("<div style=""color:gray;""><span style=""color:" & IIF(balances.Item(wireCur).Expected <= 0, "#9b2424", "#000000") & ";"">" & FormatCurr(wireCur, balances.Item(wireCur).Expected) & "</span></div>")
						End if
						%>
					</td>
					<%
					sTdStyle2 = ""
					sCurrencyToUseTxt = ""
					If (CCur(rsData("WireExchangeRate"))<=0) OR (int(rsData("WireStatus"))>0) Then
						sTdStyle2 = "color:gray;"
						sCurrencyToUseTxt = "disabled"
					End if
					%>
					<td style="text-align:center;<%= sTdStyle & sTdStyle2 %>">
						<select style="font-size:11px;" name="currencyToUse<%= nWireMoneyID %>" <%= sCurrencyToUseTxt %> onchange="ChangeCurrency(<%= nWireMoneyID %>, this.value, <%=rsData("WireCurrency") %>);">
						<%For i = 0 To MAX_CURRENCY%>
							<option value="<%=i%>" <%If int(rsData("WireProcessingCurrency"))=i Then%>selected<%End if%> ><%=GetCurText(i)%>
						<%Next%>
						</select>
					</td>
					<td style="text-align:center; <%= sTdStyle %>">
						<a onclick="toggleEditing('<%=nWireMoneyID%>','<%=sTdStyle%>')" name="<%= nWireMoneyID %>" style="cursor:pointer;<%= sTdStyle %>"><img src="../images/b_showWhiteEng.gif" align="middle" /></a><br />
					</td>
					<td style="text-align:center; <%= sTdStyle %>">
						<%If rsData("WireFlag") <> WRF_Done Then %>
						<!--<a href="common_flagUpdate.asp?tblName=tblWireMoney&FieldName=WireFlag&FieldIdName=WireMoney_id&FieldIdValue=<%= nWireMoneyID %>" target="emptyIframe"></a>-->
						   <img src="../images/<%= rsData("GD_Description") %>" name="flagImage<%= nWireMoneyID %>" id="flagImage<%= nWireMoneyID %>" border="0" alt="<%= rsData("GD_Text") %>">
						<%Else%>
							<img src="../images/<%= rsData("GD_Description") %>" name="flagImage<%= nWireMoneyID %>" id="Img1" border="0" alt="<%= rsData("GD_Text") %>">
						<%End If%>
					</td>
					<td></td>
					<td style="text-align:center; <%= sTdStyle %>">
						<%
						If sRButtonShow then
							'20090129 Tamir - "Hold" is always enabled
							sRButton2 = ""
							If rsData("PreferredWireType") = 2 Then sRButton2 = sRButton2 & " style=""background-color:gray;"""
							response.write "<input type=""Radio"" " & sRButton2 & " style=""" & sInputStyle & """ name=""wireAction" & nWireMoneyID & """ value=""2"" ondblclick=""this.checked=false;""><br />"
						Elseif trim(sRButtonWhereV) = "2" then
							response.write "<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />"
						Else
							response.write "<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />"
						End If
						%>
					</td>
					<td style="text-align:center; <%= sTdStyle %>">
						<%
						If sRButtonShow then
							If rsData("PreferredWireType") = 6 Then sRButton6 = sRButton6 & " style=""background-color:gray;"""
							response.write "<input type=""Radio"" " & sRButton6 & " style=""" & sInputStyle & """ name=""wireAction" & nWireMoneyID & """ value=""6"" ondblclick=""this.checked=false;""><br />"
						Elseif trim(sRButtonWhereV) = "6" then
							response.write "<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />"
						Else
							response.write "<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />"
						End If
						%>
					</td>
					<td style="text-align:center; <%= sTdStyle %>">
						<%
						If sRButtonShow then
							If rsData("PreferredWireType") = 4 Then sRButton4 = sRButton4 & " style=""background-color:gray;"""
							response.write "<input type=""Radio"" " & sRButton4 & " style=""" & sInputStyle & """ name=""wireAction" & nWireMoneyID & """ value=""4"" ondblclick=""this.checked=false;""><br />"
						Elseif trim(sRButtonWhereV) = "4" then
							response.write "<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />"
						Else
							response.write "<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />"
						End If
						%>
					</td>
					<td style="text-align:center; <%= sTdStyle %>">
						<%
						If sRButtonShow then
							If rsData("PreferredWireType") = 3 Then sRButton3 = sRButton3 & " style=""background-color:gray;"""
							response.write "<input type=""Radio"" " & sRButton3 & " style=""" & sInputStyle & """ name=""wireAction" & nWireMoneyID & """ value=""3"" ondblclick=""this.checked=false;""><br />"
						Elseif trim(sRButtonWhereV) = "3" then
							response.write "<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />"
						Else
							response.write "<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />"
						End If
						%>
					</td>
					<td style="text-align:center; <%= sTdStyle %>">
						<%
						If sRButtonShow then
							If rsData("PreferredWireType") = 7 Then sRButton7 = sRButton7 & " style=""background-color:gray;"""
							response.write "<input type=""Radio"" " & sRButton7 & " style=""" & sInputStyle & """ name=""wireAction" & nWireMoneyID & """ value=""7"" ondblclick=""this.checked=false;""><br />"
						Elseif trim(sRButtonWhereV) = "7" then
							response.write "<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />"
						Else
							response.write "<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />"
						End If
						%>
					</td>
					<td style="text-align:center; <%= sTdStyle %>">
						<%
						If sRButtonShow then
							If PreferredWireType = 8 Then sRButton8 = sRButton8 & " style=""background-color:gray;"""
							response.write "<input type=""Radio"" " & sRButton8 & " style=""" & sInputStyle & """ name=""wireAction" & nWireMoneyID & """ value=""8"" ondblclick=""this.checked=false;""><br />"
						Elseif trim(sRButtonWhereV) = "8" then
							response.write "<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />"
						Else
							response.write "<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />"
						End If
						%>
					</td>					
					<td style="text-align:center; <%= sTdStyle %>">
						<%
						If sRButtonShow then
							If rsData("PreferredWireType") = 5 Then sRButton5 = sRButton5 & " style=""background-color:gray;"""
							response.write "<input type=""Radio"" " & sRButton5 & " style=""" & sInputStyle & """ name=""wireAction" & nWireMoneyID & """ value=""5"" ondblclick=""this.checked=false;""><br />"
						Elseif trim(sRButtonWhereV) = "5" then
							response.write "<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />"
						Else
							response.write "<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />"
						End If
						%>
					</td>
					<td style="text-align:center; <%= sTdStyle %>">
						<%
						If sRButtonShow then
							If rsData("PreferredWireType") = 9 Then sRButton9 = sRButton9 & " style=""background-color:gray;"""
							response.write "<input type=""Radio"" " & sRButton9 & " style=""" & sInputStyle & """ name=""wireAction" & nWireMoneyID & """ value=""9"" ondblclick=""this.checked=false;""><br />"
						Elseif trim(sRButtonWhereV) = "9" then
							response.write "<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />"
						Else
							response.write "<img src=""../images/checkbox_off.gif"" width=""15"" height=""16"" border=""0""><br />"
						End If
						%>
					</td>
					<td style="text-align:center; <%= sTdStyle %>">
						<%
						If int(rsData("WireStatus")) >= 2 Or trim(sRButtonWhereV) = "1" Then
							%>
							<a href="wire_search_data.asp?ActionType=CLEAN&WireMoney_id=<%= nWireMoneyID %>&<%= UrlWithout(UrlWithout(sQueryString, "ActionType"), "WireMoney_id") %>" onclick="return confirm('Sure to reset and allow new selections?');">RESET</a><br />
							<%
						else
							%>
							<span style="color:#d5d5d5; text-decoration:underline; font-family:Arial;">RESET</span><br />
							<%
						End If
						%>
					</td>
					<td style="text-align:center; <%= sTdStyle %>">
						<%
						If trim(sRButtonWhereV) = "1" then
							response.write "<img src=""../images/checkbox.gif"" width=""15"" height=""16"" border=""0""><br />"
						Else
							'20090129 Tamir - "Cancel" is always enabled
							sRButton1=""
							response.write "<input type=""Radio"" " & sRButton1 & " style=""" & sInputStyle & """ name=""wireAction" & nWireMoneyID & """ value=""1"" ondblclick=""this.checked=false;""><br />"
						End If
						%>
					</td>
				</tr>
				<%
				If NOT isAllowAction Then
					%>
					<tr>
						<td></td>
						<td colspan="16">
							There are missing or invalid merchant data, Fax and Masav options are disabled.
							<%= IIf(isPaymentLocalOK, "", " For local payments, beneficiary's Company Legal Number must be specified.") %>
						</td>
					</tr>
					<%
				End if	
				%>
				<tr id="trRow<%= nWireMoneyID %>" style="display:none;">
					<td></td><td id="tdRow<%= nWireMoneyID %>" colspan="19" style="<%= sTdStyle %>"></td>
				</tr>
				<tr>
					<td height="1" colspan="10" bgcolor="#c5c5c5"></td>
					<td></td>
					<td height="1" colspan="9" bgcolor="#c5c5c5"></td>
				</tr>
				<%
				nShowCounter=nShowCounter+1		
				rsData.movenext
			loop
			%>
			<tr><td height="16"></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" align="center" border="0" cellpadding="1" cellspacing="0">
			<tr>
				<td>
					<%
					'Show paging controls
					call showPaging("Wire_search_data.asp", "Eng", "&" & sQueryString)
					
					'set query string
					totalsQueryString = "?Totals=1&" & Request.QueryString
					%>
				</td><td align="right">
					<input type="button" id="TotalsButton" onclick="with(document.getElementById('tblTotals')){style.display='block';if(onresize)onresize();};setAjaxHTML('<%= totalsQueryString%>', document.getElementById('tblTotals'), true);" value="SHOW TOTALS &Sigma;">
					<div id="tblTotals" onresize="PosObject(this, document.getElementById('TotalsButton'), 'RRTB', 0, -10);" style="border-color:#484848; border-style:solid; border-width:1px 3px; position:absolute;width:260px;background-color:White;display:none;"> &nbsp; LOADING DATA... </div>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td>
			<table width="100%" style="border:1px solid gray;" border="0" cellspacing="2" cellpadding="2" align="center">
			<tr>
				<td>
					<%
					dim fs,f
					set fs=Server.CreateObject("Scripting.FileSystemObject")
					if fs.FileExists(MapWirePath("BatchFiles/Masav/Masav.CSV")) then
						Set f = fs.GetFile(MapWirePath("BatchFiles/Masav/Masav.CSV"))
						%>File: <a href="?DownloadPrivateFile=<%= Server.UrlEncode("Wires\BatchFiles\Masav\Masav.CSV") %>" class="faq" target="emptyIframe"><em>Masav.CSV</em></a> &nbsp;&nbsp; Last created on: <span><em><%= f.DateLastModified %></em></span> &nbsp;&nbsp;<%
					end if
					'if fs.FileExists(Server.MapPath("/Data/Uploads/WREXP.XLS")) then
						'Set f = fs.GetFile(Server.MapPath("/Data/Uploads/WREXP.XLS"))
						%><!--<br />File: <a href="?DownloadPrivateFile=<%= Server.UrlEncode("Wires\BatchFiles\WireFiles\WREXP.XLS") %>" class="faq" target="emptyIframe"><em>WREXP.XLS</em></a> &nbsp;&nbsp; Last created on: <span><em><%'= f.DateLastModified %></em></span> &nbsp;&nbsp;--><%
					'end if
					if fs.FileExists(MapWirePath("BatchFiles/Manual/WIRE_MANUAL.HTM")) then
						Set f = fs.GetFile(MapWirePath("BatchFiles/Manual/WIRE_MANUAL.HTM"))						
						%><br />File: <a href="?DownloadPrivateFile=<%= Server.UrlEncode("Wires\BatchFiles\Manual\WIRE_MANUAL.HTM") %>" class="faq" target="_blank"><em>WIRE_MANUAL.HTM</em></a> &nbsp;&nbsp; Last created on: <span><em><%= f.DateLastModified %></em></span> &nbsp;&nbsp;<%
					end if
					sFNB_File = GetNewestFile(MapWirePath("BatchFiles/System.FNB/"), True)
					if Len(Cstr(sFNB_File)) > 0 And fs.FileExists(sFNB_File + "/FTPTCPIP.ACBFILE1") then
						Response.Write("<br />File: ")
                        For Each fnbFile in fs.GetFolder(sFNB_File).Files
                            ext = Right(fnbFile.Name, Len(fnbFile.Name) - InStrRev(fnbFile.Name, "."))
                            Response.Write("<a href=""?DownloadPrivateFile=" & Server.UrlEncode("Wires\BatchFiles\System.FNB\" & fnbFile.ParentFolder.Name & "\" & fnbFile.Name) & """ class=""faq"" target=""_blank""><em>" & ext & "</em></a> | ")
                        Next
						Set f = fs.GetFile(sFNB_File + "/FTPTCPIP.ACBFILE1")
                        Response.Write("date: <span><em>" & f.DateLastModified & "</em></span> FNB &nbsp;&nbsp;")
					End if
					sBOG_Dir_Path = MapWirePath("BatchFiles/BankOfGeorgia/")
					sBOG_File = GetNewestFile(sBOG_Dir_Path, False)
					if len(""&sBOG_File) > 0 then
						Set f = fs.GetFile(sBOG_File)
						sBOGFileName = f.name
						%>
						<br />File: <a href="?DownloadPrivateFile=<%= Server.UrlEncode("Wires\BatchFiles\BankOfGeorgia\" & sBOGFileName) %>" class="faq" target="_blank"><em><%=f.name%></em></a> &nbsp;&nbsp; Created on: <span><em><%= f.DateLastModified %></em></span> &nbsp;&nbsp;
						<%
					end if
					Set f = Nothing
					set fs=nothing
					%>
				</td>
				<td align="right">
					<b>Hellenic</b>:
					<select name="HellenicDay" class="txt11">
						<option>DD</option>
						<%
							for i=1 to 31
								response.Write "<option value=""" & i & """>" & Right("0" & i, 2) & "</option>"
							next
						%>
					</select>/<select name="HellenicMonth" class="txt11">
						<option>MM</option>
						<%
							for i=1 to 12
								response.Write "<option value=""" & i & """>" & Right("0" & i, 2) & "</option>"
							next
						%>
					</select>/<select name="HellenicYear" class="txt11">
						<option>YYYY</option>
						<%
							for i=Year(Now()) to Year(Now())+1
								response.Write "<option value=""" & i & """>" & i & "</option>"
							next
						%>
					</select>
					<select name="HellenicAccount" class="txt11" style="text-transform:capitalize;width:250px">
						<option>&lt; Hellenic - Select Account &gt;</option>
						<%
							sSQL="SELECT BA_ID, cur.CurrencyISOCode+' '+BA_AccountNumber+' '+Lower(BA_AccountName) FROM tblBankAccounts" & _
							" INNER JOIN [List].[CurrencyList] AS cur ON BA_Currency = cur.CurrencyID WHERE BA_BankName='HELLENIC' ORDER BY BA_AccountNumber"
							Set rsData2=oledbData.Execute(sSQL)
							Do Until rsData2.EOF
								%>
									<option value="<%= rsData2(0) %>"><%= rsData2(1) %></option>
								<%
								rsData2.MoveNext
							Loop
							rsData2.Close 
						%>
					</select>
					<br />
					<b>BOG</b>:
					<select name="BOGDay" class="txt11">
						<option>DD</option>
						<%
							for i=1 to 31
								response.Write "<option value=""" & i & """>" & Right("0" & i, 2) & "</option>"
							next
						%>
					</select>/<select name="BOGMonth" class="txt11">
						<option>MM</option>
						<%
							for i=1 to 12
								response.Write "<option value=""" & i & """>" & Right("0" & i, 2) & "</option>"
							next
						%>
					</select>/<select name="BOGYear" class="txt11">
						<option>YYYY</option>
						<%
							for i=Year(Now()) to Year(Now())+1
								response.Write "<option value=""" & i & """>" & i & "</option>"
							next
						%>
					</select>
					<select name="BOGAccount" class="txt11" style="text-transform:capitalize; width:250px">
						<option>&lt; BOG - Select Account &gt;</option>
						<%
							sSQL="SELECT BA_ID, CUR_ISOName+' '+BA_AccountNumber+' '+Lower(BA_AccountName) FROM tblBankAccounts" & _
							" INNER JOIN tblSystemCurrencies ON BA_Currency=CUR_ID WHERE BA_BankName='BOG' ORDER BY BA_AccountNumber"
							Set rsData2=oledbData.Execute(sSQL)
							Do Until rsData2.EOF
								%>
									<option value="<%= rsData2(0) %>"><%= rsData2(1) %></option>
								<%
								rsData2.MoveNext
							Loop
							rsData2.Close 
						%>
					</select>
					<br />
					<b>FNB</b>:
					<select name="FNBDay" class="txt11">
						<option>DD</option>
						<%
							for i=1 to 31
								response.Write "<option value=""" & i & """>" & Right("0" & i, 2) & "</option>"
							next
						%>
					</select>/<select name="FNBMonth" class="txt11">
						<option>MM</option>
						<%
							for i=1 to 12
								response.Write "<option value=""" & i & """>" & Right("0" & i, 2) & "</option>"
							next
						%>
					</select>/<select name="FNBYear" class="txt11">
						<option>YYYY</option>
						<%
							for i=Year(Now()) to Year(Now())+1
								response.Write "<option value=""" & i & """>" & i & "</option>"
							next
						%>
					</select>
					<select name="FNBAccount" class="txt11" style="text-transform:capitalize; width:250px">
						<option>&lt; FNB - Select Account &gt;</option>
						<%
							sSQL="SELECT AccountNumber, CurrencyISOCode +' '+ AccountNumber + ' '+ Lower(AccountName) FROM Finance.WireAccount WHERE WireProvider_id='System.FNB' ORDER BY AccountNumber"
							Set rsData2=oledbData.Execute(sSQL)
							Do Until rsData2.EOF
								%>
									<option value="<%= rsData2(0) %>"><%= rsData2(1) %></option>
								<%
								rsData2.MoveNext
							Loop
							rsData2.Close 
						%>
					</select>
					<script language="javascript" type="text/javascript">
					    function ValidateFNBExclusivity(frmWire)
					    {
					        var bFNB=false;
					        var bNotFNB=false;
					        for(var i=0;i<frmWire.length;i++)
					        {
					            if (frmWire.item(i).name.substring(0, 10)=="wireAction")
					            {
					                if (frmWire.item(i).checked)
					                {
					                    if (frmWire.item(i).value=="9") bFNB=true; else bNotFNB=true;
					                }
					            }
					            if (bFNB && bNotFNB)
					            {
					                alert("Transfers via FNB Bank must be set exclusively!\n\nRemove either all FNB transfers or all other transfers from the current selection.");
					                return false;
					            }
					        }
					        if ((!bFNB)&&(!bNotFNB))
					        {
					            alert("No transfer is set!\n\nPlease set one or more transfer.");
					            return false;
					        }
					        if (bFNB)
					        {
					            if (frmWire.FNBDay.selectedIndex==0)
					            {
					                alert("FNB transfer date is not set!\n\nPlease select FNB transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                frmWire.FNBDay.focus();
					                return false;
					            }
					            if (frmWire.FNBMonth.selectedIndex==0)
					            {
					                alert("FNB transfer date is not set!\n\nPlease select FNB transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                frmWire.HellenicMonth.focus();
					                return false;
					            }
					            if (frmWire.FNBYear.selectedIndex==0)
					            {
					                alert("FNB transfer date is not set!\n\nPlease select FNB transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                frmWire.HellenicYear.focus();
					                return false;
					            }
					            if ((frmWire.FNBYear.value*13-(-1)*frmWire.FNBMonth.value)*32-(-1)*frmWire.FNBDay.value-<%= (Year(Now())*13+Month(Now()))*32+Day(Now()) %><0)
					            {
					                alert("FNB transfer date is past!\n\nPlease select valid FNB transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                if (frmWire.FNBMonth.value-<%= Month(Now()) %><0) frmWire.FNBMonth.focus();else frmWire.FNBDay.focus();
					                return false;
					            }
					            if (frmWire.FNBAccount.selectedIndex==0)
					            {
					                alert("FNB account is not set!\n\nPlease select FNB account.");
					                frmWire.HellenicAccount.focus();
					                return false;
					            }
					            frmWire.action="Wire_FNB.aspx?<%= sQueryString %>";
					            return true;
					        }
					        frmWire.action="wire_search_data.asp?<%= sQueryString %>";
					        return true;
					    }

					    function ValidateHellenicExclusivity(frmWire)
					    {
					        var bHellenic=false;
					        var bNotHellenic=false;
					        for(var i=0;i<frmWire.length;i++)
					        {
					            if (frmWire.item(i).name.substring(0, 10)=="wireAction")
					            {
					                if (frmWire.item(i).checked)
					                {
					                    if (frmWire.item(i).value=="7") bHellenic=true; else bNotHellenic=true;
					                }
					            }
					            if (bHellenic&&bNotHellenic)
					            {
					                alert("Transfers via Hellenic Bank must be set exclusively!\n\nRemove either all Hellenic transfers or all other transfers from the current selection.");
					                return false;
					            }
					        }
					        if ((!bHellenic)&&(!bNotHellenic))
					        {
					            alert("No transfer is set!\n\nPlease set one or more transfer.");
					            return false;
					        }
					        if (bHellenic)
					        {
					            if (frmWire.HellenicDay.selectedIndex==0)
					            {
					                alert("Hellenic transfer date is not set!\n\nPlease select Hellenic transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                frmWire.HellenicDay.focus();
					                return false;
					            }
					            if (frmWire.HellenicMonth.selectedIndex==0)
					            {
					                alert("Hellenic transfer date is not set!\n\nPlease select Hellenic transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                frmWire.HellenicMonth.focus();
					                return false;
					            }
					            if (frmWire.HellenicYear.selectedIndex==0)
					            {
					                alert("Hellenic transfer date is not set!\n\nPlease select Hellenic transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                frmWire.HellenicYear.focus();
					                return false;
					            }
					            if ((frmWire.HellenicYear.value*13-(-1)*frmWire.HellenicMonth.value)*32-(-1)*frmWire.HellenicDay.value-<%= (Year(Now())*13+Month(Now()))*32+Day(Now()) %><0)
					            {
					                alert("Hellenic transfer date is past!\n\nPlease select valid Hellenic transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                if (frmWire.HellenicMonth.value-<%= Month(Now()) %><0) frmWire.HellenicMonth.focus();else frmWire.HellenicDay.focus();
					                return false;
					            }
					            if (frmWire.HellenicAccount.selectedIndex==0)
					            {
					                alert("Hellenic account is not set!\n\nPlease select Hellenic account.");
					                frmWire.HellenicAccount.focus();
					                return false;
					            }
					            frmWire.action="Wire_Hellenic.aspx?<%= sQueryString %>";
					            return true;
					        }
					        frmWire.action="wire_search_data.asp?<%= sQueryString %>";
					        return true;
					    }
						
					    function ValidateBOGExclusivity(frmWire)
					    {
					        var bBOG=false;
					        var bNotBOG=false;
					        for(var i=0;i<frmWire.length;i++)
					        {
					            if (frmWire.item(i).name.substring(0, 10)=="wireAction")
					            {
					                if (frmWire.item(i).checked)
					                {
					                    if (frmWire.item(i).value=="8") bBOG=true; else bNotBOG=true;
					                }
					            }
					            if (bBOG&&bNotBOG)
					            {
					                alert("Transfers via Bank of Georgia must be set exclusively!\n\nRemove either all BOG transfers or all other transfers from the current selection.");
					                return false;
					            }
					        }
					        if ((!bBOG)&&(!bNotBOG))
					        {
					            alert("No transfer is set!\n\nPlease set one or more transfer.");
					            return false;
					        }
					        if (bBOG)
					        {
					            if (frmWire.BOGDay.selectedIndex==0)
					            {
					                alert("BOG transfer date is not set!\n\nPlease select BOG transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                frmWire.BOGDay.focus();
					                return false;
					            }
					            if (frmWire.BOGMonth.selectedIndex==0)
					            {
					                alert("BOG transfer date is not set!\n\nPlease select BOG transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                frmWire.BOGMonth.focus();
					                return false;
					            }
					            if (frmWire.BOGYear.selectedIndex==0)
					            {
					                alert("BOG transfer date is not set!\n\nPlease select BOG transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                frmWire.BOGYear.focus();
					                return false;
					            }
					            if ((frmWire.BOGYear.value*13-(-1)*frmWire.BOGMonth.value)*32-(-1)*frmWire.BOGDay.value-<%= (Year(Now())*13+Month(Now()))*32+Day(Now()) %><0)
					            {
					                alert("BOG transfer date is past!\n\nPlease select valid BOG transfer date.\n\nToday is <%= FormatDateTime(Now(), 2) %>");
					                if (frmWire.BOGMonth.value-<%= Month(Now()) %><0) frmWire.BOGMonth.focus();else frmWire.BOGDay.focus();
					                return false;
					            }
					            if (frmWire.BOGAccount.selectedIndex==0)
					            {
					                alert("BOG account is not set!\n\nPlease select BOG account.");
					                frmWire.BOGAccount.focus();
					                return false;
					            }
					            frmWire.action="Wire_BOG.aspx?<%= sQueryString %>";
					            return true;
					        }
					        if(frmWire.action.indexOf("_Hellenic")<0){
					            frmWire.action="wire_search_data.asp?<%= sQueryString %>";
					        }
					        return true;
					    }
						
					    function setAction(frmWire){
					        return ValidateHellenicExclusivity(frmWire) && ValidateBOGExclusivity(frmWire) && ValidateFNBExclusivity(frmWire);
					    }
					</script>
					<br />
					<select name="MasavCode">
						<% If LCase(session("Identity")) <> "demoobl" Then %>
							<option value="87153201" selected="selected">��� ����� ���� 405 ����� 382378 - 1-20-87153</option>
							<option value="83663203">��� ����� ���� 064 ����� 390053 - 3-20-83663</option>
						<% End If%>
						<option value="86304201">86304-20-1</option>
						<option value="67339010">67339-01-0</option>
						<option value="83285205">83285-20-5</option>
						<option value="83286286">83286-28-6</option>
						<option value="MSGPOL528">MSGPOL528 DB-Bank</option>
						<option value="PBNLAIKI">Laiki Bank</option>
						<option value="INVIKBANK">Invik Bank</option>
						<option value="HELNICBNK">Hellenic Bank</option>
						<option value="INVESTBNK">Invest Bank</option>
						<option value="BANKOFGEO">Bank Of Georgia</option>
						<option value="ATLASBNK">Atlas Bank</option>
					</select>
                    <br />
					<input type="hidden" name="ActionType" value="SUBMIT" />
					<input type="button" class="button1" name="ActionType" value=" SUBMIT " onclick="if (!(setAction(this.form))) return false;disabled=true;this.form.submit()"> &nbsp;&nbsp;
					<input type="reset" value="RESET" class="button1"> 
					<br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<%
Else
	%>
	<tr>
		<td>
			<br /><br />
			<hr width="100%" size="1" color="#C0C0C0" noshade>
			<span class="txt15">No records found !</span><br />
			<hr width="100%" size="1" color="#C0C0C0" noshade>
		</td>
	</tr>
	<%
End If

rsData.close
Set rsData = nothing
call closeConnection()
%>
</table>
</form>
<iframe id="emptyIframe" name="emptyIframe" width="0" height="0" frameborder="0"></iframe>
<script language="javascript" type="text/javascript">
    function FlagChanged(nID, nStat){
        var xValues = document.getElementById('frmSearchWire').elements.item('wireAction' + nID);
        if (xValues)
        {
            for(var i = 0; i < xValues.length; i++) xValues[i].disabled = (nStat != 1);
            if (xValues[6]) xValues[6].disabled=false;
            xValues[0].disabled=false;
            if (nStat==1 && false) {
                if ("<%= sPayToBalanceOnlyIDs %>".indexOf("_"+nID+"_")>=0) {
                    alert("This transfer cannot be wired manually:\n\n\"Settle payouts to <%= COMPANY_NAME_1 %> E-banking System only\" option is checked for the merchant.");
                    xValues[1].disabled=true;
                    xValues[5].disabled=false;
                }
            }
        }
    }
</script>
</body>
</html>