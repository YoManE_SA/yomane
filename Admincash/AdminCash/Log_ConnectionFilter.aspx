<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DebitCompanies.ascx" TagName="FilterDebitCompanies" TagPrefix="Uc1" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = Date.Now.ToShortDateString
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
        
        litStatus.Text = "<select style=""width:120px; height:80px;"" multiple=""multiple"" name=""Status"" class=""grayBG"">"
        Dim sSQL As String = "SELECT GD_ID, GD_Text FROM tblGlobalData WHERE GD_Group=65 AND GD_LNG=1 ORDER BY GD_ID"
        Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
        Do While drData.Read
            litStatus.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
        Loop
        drData.Close()
        litStatus.Text &= "</select>"
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
	<form runat="server" action="Log_ConnectionData.aspx" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" id="iMerchantID" name="iMerchantID" />
	<h1><asp:label ID="lblPermissions" runat="server" /> Connection Errors<span> - Filter</span></h1>

	<table class="filterNormal">
		<tr><td><Uc1:FilterMerchants id="FMerchants" SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
			INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" ClientField="iMerchantID" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Initial Transaction</th></tr>
		<tr>
			<td>Trans. ID</td>
			<td>
				<input name="TransFail" class="grayBG" title="Type here single transaction ID or multiple comma-separated IDs" />
			</td>
		</tr>
		<tr>
			<td>Reply Code</td>
			<td>
				<input type="radio" name="iReplyCode" class="option" value="521" />521
				<input type="radio" name="iReplyCode" class="option" value="520" />520
				<input type="radio" name="iReplyCode" class="option" value="" checked="checked" />Both
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>HTTP Status</th></tr>
		<tr>
			<td>
				<input name="HTTP_Error" class="input1 grayBG" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<th>
				DebitCompany
			</th>
		</tr>
		<tr>
			<td>
				<Uc1:FilterDebitCompanies ID="FilterDebitCompanies1" ClientFieldDebitCompany="DebitCompanyID" ClientFieldDebitTerminal="TerminalNumber" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Refund Attempt</th></tr>
		<tr>
			<td>Attempted</td>
			<td>
				<input type="radio" name="RefundAttempt" class="option" value="1" />Yes
				<input type="radio" name="RefundAttempt" class="option" value="0" />No
				<input type="radio" name="RefundAttempt" class="option" value="" checked="checked" />All
			</td>
		</tr>
		<tr>
			<td>Reply Code</td>
			<td><input name="RefundReply" class="grayBG short2" /></td>
		</tr>
		<tr>
			<td style="vertical-align:top;">Status</td>
			<td>
				<asp:Literal ID="litStatus" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td>
				<select name="iPageSize">
					<option value="25" selected="selected">25 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td align="right"><input type="submit" name="Action" value="SEARCH"><br /></td>
		</tr>
	</table>
	</form>
</asp:Content>