<%@ Page Language="VB" %>
<%@ Register Src="Password.ascx" TagPrefix="custom" TagName="Password" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>
<script runat="server">
	Dim sSQL, StyleBkgColor As String
	Dim req_ID As Integer
	
	Sub Page_Load()
		lblPassword.Text = String.Empty
		Security.CheckPermission(lblPermissions)
		req_ID = Request("id")
        ucPassword.RefID = req_ID
        If Not IsPostBack Then
			Dim affiliate = Netpay.Bll.Affiliates.Affiliate.Load(req_ID)
            If affiliate Is Nothing Then Response.End()
            ltAffiliateName.Text = affiliate.Name
            txtControlPanelUserName.Text = affiliate.Login.UserName
            txtControlPanelEmail.Text = affiliate.Login.EmailAddress
            If affiliate.IsAllowControlPanel Then chkAllowControlPanel.Checked = True
			tlbTop.LoadPartnerTabs(req_ID)
        End If
	End Sub
	
	Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim affiliate = Netpay.Bll.Affiliates.Affiliate.Load(req_ID)
        affiliate.Login.UserName = txtControlPanelUserName.Text
        affiliate.Login.EmailAddress = txtControlPanelEmail.Text
        affiliate.IsAllowControlPanel = chkAllowControlPanel.Checked
		affiliate.Login.Save()
		affiliate.Save()
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Partners</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body>

<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
<tr>
	<td>
		<asp:label ID="lblPermissions" runat="server" />
		<span id="pageMainHeading">Partners Management</span> - <asp:Literal runat="server" ID="ltAffiliateName" /><br /><br />
	</td>
</tr>
<tr>
	<td>
		<custom:Toolbar ID="tlbTop" Width="100%" runat="server" />
		<form id="frmServer" runat="server">
		<table width="200" class="formNormal" border="0">
			<tr><td class="MerchantSubHead" colspan="2">Login Details<br /><br /></td></tr>
			<tr>
	            <td>
		            <asp:CheckBox ID="chkAllowControlPanel" runat="server" CssClass="option" /> Allow Login<br /><br />
	            </td>
            </tr>
			<tr>
	            <td>
		            Email<br />
		            <asp:TextBox ID="txtControlPanelEmail" runat="server" /><br />
	            </td>
	            <td>
		            User Name<br />
		            <asp:TextBox ID="txtControlPanelUserName" runat="server" />
		            <asp:Label ID="lblPassword" runat="server" />
	            </td>
            </tr>
			<tr><td><br /><br /></td></tr>
            <tr>
	            <td colspan="2" style="padding-left:0;">
		            <custom:Password ID="ucPassword" RefType="Affiliate" Title="" TextBoxWidth="130px" Layout="SingleLineTitlesOnTop" runat="server" />
	            </td>
            </tr>
			<tr>
                <td><br /></td>
            </tr>	
			<tr><td height="20" colspan="2"></td></tr>
			<tr>
				<td colspan="2">
					<br /><asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" Text=" UPDATE " />
					<asp:ValidationSummary ShowMessageBox="false" ShowSummary="false" runat="server" ID="vsMerchant" />
				</td>
			</tr>
		</table>
        </form>	
	</td>
</tr>
</table>	
</body>
</html>