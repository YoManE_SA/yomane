<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>

<script runat="server">
	Dim sSQL, sWhere, sAnd, StyleBkgColor As String
	
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
	
		If Trim(Request("showAll")) = "" Then sWhere &= "dc_isActive = 1" : sAnd = " AND "
		If Security.IsLimitedDebitCompany() Then sWhere &= sAnd & "debitCompany_id IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
		If sWhere <> "" Then sWhere = "WHERE " & sWhere
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Debit Companys</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<script language="javascript">
		var vOldTr = null;
		function setStyle(trSec){
			if(vOldTr) vOldTr.style.backgroundColor='';
			(vOldTr = trSec).style.backgroundColor='#bcd5fe';
		}
	</script>
</head>
<body bgcolor="#eeeeee" class="itextm3">
	<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr>
		<td>
			<style type="text/css" media="all">
				span.security
				{
					font-weight:bold;
					font-size:12px;
					color:rgb(245, 245, 245);
					background-color:#2566AB;
					padding-left:3px; padding-right:3px;
					vertical-align:middle;
				}
				span.securityManaged
				{
					cursor:pointer;
				}
			</style>
			<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">Debit Companies</span><br />
			<%If Trim(Request("showAll"))="" Then%><a href="?showAll=1">Show full list</a><%End if%>
		</td>
	</tr>
	</table>
	<br />
	<table class="formNormal" align="center" width="95%" border="0">
	<tr>
		<th class="dark"><br /></th>
		<th class="dark" style="text-align:right;">No</th>
		<th class="dark">Company Name</th>
		<th class="dark" style="text-align:center;">Reply Codes</th>
		<th class="dark" style="text-align:center;">Rules (<a target="frmBody" href="system_debitCompanyRules.aspx">all</a>)</th>
	</tr>
	<tr><td height="6"></td></tr>
	<%
		sSQL = "SELECT debitCompany_id, dc_name, CASE dc_isActive WHEN 0 THEN '#ff6666' ELSE '#66cc66' END StatusColor," & _
		" IsNull(Count(ID), 0) RulesAll, IsNull(Sum(CAST(dr_IsActive AS int)), 0) RulesActive FROM tblDebitCompany" & _
		" LEFT JOIN tblDebitRule ON DebitCompany_ID=dr_DebitCompany " & sWhere & _
		" GROUP BY debitCompany_id, dc_name, dc_isActive ORDER BY dc_name"
		Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
		If iReader.HasRows Then
			While iReader.Read
				%>
				
				<tr id="trLines" onclick="setStyle(this);" style="height:18px;">
					<td><span style="background-color:<%= iReader("StatusColor") %>;">&nbsp;&nbsp;</span></td>
					<td style="text-align:right;"><%= iReader("debitCompany_id") %></td>
					<td>
						<img src="/NPCommon/ImgDebitCompanys/23X12/<%=Trim(iReader("debitCompany_id"))%>.gif" align="middle" border="0" />
						<a target="frmBody" href="system_debitCompanyData.asp?debitCompany_id=<%= iReader("debitCompany_id") %>"><%= iReader("dc_name") %></a>
					</td>
					<td style="text-align:center;">
						<a target="frmBody" href="system_debitCompanyCode.aspx?debitCompany_id=<%=iReader("debitCompany_id")%>&dc_name=<%=Server.UrlEncode(iReader("dc_name"))%>">Edit</a>
					</td>
					<td style="text-align:center;">
						<a target="frmBody" href="system_debitCompanyRules.aspx?DebitCompany=<%=iReader("debitCompany_id")%>&dc_name=<%=Server.UrlEncode(iReader("dc_name"))%>">
							<span title="<%= iReader("dc_name") %>: <%= iReader("RulesAll") %> rules, <%= iReader("RulesActive") %> active.">
								<%= iReader("RulesActive") %>/<%=iReader("RulesAll")%>
							</span>
						</a>
					</td>
				</tr> 
				<tr><td height="1" colspan="5"  bgcolor="silver"></td></tr>
				<%
			End While
		Else
			%>
			<tr><td height="5"></td></tr>
			<tr><td colspan="4" class="txt12">No records found !<br /></td></tr>
			<tr><td><br /></td></tr>
			<%
		End if
		iReader.Close()
	%>
	</table>
</body>
</html>