<%
Server.ScriptTimeout = 6000
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_GlobalData.asp"-->
<!--#include file="../include/func_controls.asp"-->
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
	<link  href="../StyleSheet/styleAdminEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.Bsave{
			background-image: url("../images/icon_xls.gif");
			background-repeat: no-repeat; background-position: 75px; 
			width: 100; height: 24px; text-align: left;
			background-color:#ffffff; font-size:11px;
			padding-left: 3px; padding-right: 3px; cursor: hand;
		}
	</style>
	<script language="JavaScript" type="text/javascript">
	    function OpenPop(sURL, sName, sWidth, sHeight, sScroll)
	    {
	        placeLeft = screen.width / 2 - sWidth / 2
	        placeTop = screen.height / 2 - sHeight / 2
	        var winPrint = window.open(sURL, sName, 'TOP=' + placeTop + ',LEFT=' + placeLeft + ',WIDTH=' + sWidth + ',HEIGHT=' + sHeight + ',scrollbars=' + sScroll);
	    }
	</script>
</head>
<body class="itext" dir="ltr">
<%
sPageType = dbText(request("iPageType"))
sCurrency = dbText(request("iCurrency"))
sCcTypeID = dbText(request("iCcTypeID"))

sFromDate = dbText(request("iFromDate"))
sFromDateShow=""
if day(sFromDate)<10 then sFromDateShow=sFromDateShow & "0" end if
sFromDateShow=sFromDateShow & day(sFromDate) & "/"
if month(sFromDate)<10 then sFromDateShow=sFromDateShow & "0" end if
sFromDateShow=sFromDateShow & month(sFromDate) & "/" & right(year(sFromDate),2)

sToDate = dbText(request("iToDate"))
sToDateShow=""
if day(sToDate)<10 then sToDateShow=sToDateShow & "0" end if
sToDateShow=sToDateShow & day(sToDate) & "/"
if month(sToDate)<10 then sToDateShow=sToDateShow & "0" end if
sToDateShow=sToDateShow & month(sToDate) & "/" & right(year(sToDate),2)	


sQueryFilter = "iFromDate=" & sFromDate & "&iToDate=" & sToDate & "&iPageType=" & sPageType & "&iCurrency=" & sCurrency & "&iCcTypeID=" & sCcTypeID
%>
<br />
<table align="center" border="0" dir="ltr" cellpadding="1" cellspacing="2" width="95%">
	<tr>
		<td id="pageMainHeadingTd"><span id="pageMainHeading">Usage By Page</span></td>
		<form>
			<td align="right" valign="top">
				<input type="button" class="Bsave" value="Export" onclick="location.href='reports_chargePageUsage_data_excel.asp?<%= sQueryFilter %>&ShowCCN=1';">
			</td>
		</form>
	</tr>
	<tr>
		<td>
			<%
			response.write "<span class=""txt12"">Period: "
			response.write "From- <span class=""txt11"">" & sFromDateShow & "</span> "
			response.write "To- <span class=""txt11"">" & sToDateShow & "</span>"

			
			response.write "<br/>Charge page: "
			sSQL="SELECT Name From List.TransSource WHERE List.TransSource.TransSource_id IN (" & sPageType & ")"
			set rsData2 = oledbData.execute(sSQL)
			sCount = 0
			do until rsData2.EOF
				If sCount>0 Then response.write " ,"
				response.write dbTextShow(rsData2("Name"))
				sCount = sCount+1
			rsData2.movenext
			loop
			
			response.write "<br/>Card Type: "
			sSQL="SELECT PaymentMethod_id AS ID, Name AS pm_Name FROM List.PaymentMethod AS pm WHERE PaymentMethod_id IN (" & sCcTypeID & ")"
			set rsData3 = oledbData.execute(sSQL)
			sCount = 0
			do until rsData3.EOF
				If sCount>0 Then response.write " ,"
				response.write dbTextShow(rsData3(1))
				sCount = sCount+1
			rsData3.movenext
			loop
			
			Select Case sCurrency
				Case "0" response.write "<br/>Currency: ILS"
				Case "1" response.write "<br/>Currency: USD"
				Case "0,1" response.write "<br/>Currency: ILS, USD"
			End Select
					
			response.write "</span><br/>"
			%>
		</td>
	</tr>
</table>
<br/>
<%
Set rsData=server.createobject("adodb.recordset")
sSQL="SELECT COUNT(tblCompanyTransPass.companyID) AS NumOfTrans, tblCompanyTransPass.companyID, tblCompany.CompanyName, tblCompany.CompanyLegalName, tblCompany.CustomerNumber, tblCompany.FirstName, tblCompany.LastName, tblCompany.Phone, tblCompany.cellular, tblCompany.Country, tblCompany.CompanyPhone, tblCompany.CompanyFax, tblCompany.CompanyCountry, tblCompany.CompanyLegalNumber " &_
"FROM tblCompanyTransPass INNER JOIN tblCompany ON tblCompanyTransPass.companyID = tblCompany.ID " &_
"WHERE tblCompanyTransPass.PaymentMethod IN (" & sCcTypeID & ") " &_
"AND tblCompanyTransPass.Currency IN (" & sCurrency & ") " &_
"AND tblCompanyTransPass.InsertDate >= '" & CDate(sFromDate) & "' " &_
"AND tblCompanyTransPass.InsertDate <= '" & CDate(sToDate) & "' " &_
"AND tblCompanyTransPass.TransSource_id IN (" & sPageType & ") " &_
"GROUP BY tblCompanyTransPass.companyID, tblCompany.CompanyName, tblCompany.CompanyLegalName, tblCompany.CustomerNumber, tblCompany.FirstName, tblCompany.LastName, tblCompany.Phone, tblCompany.cellular, tblCompany.Country, tblCompany.CompanyPhone, tblCompany.CompanyFax, tblCompany.CompanyCountry, tblCompany.CompanyLegalNumber " &_
"ORDER BY COUNT(tblCompanyTransPass.companyID) Desc"
rsData.Open sSQL,oledbData, 1, 3

session("PageSize")=25
rsData.PageSize=session("PageSize")
session("ActualPage")=1
if trim(request("page"))<>"" then
	if isNumeric(trim(request("page"))) then
		if int(trim(request("page")))<=rsData.PageCount then session("ActualPage")=trim(request("page"))
	end if
end if
		
if not rsData.EOF then
	%>
	<table width="95%" align="center" border="0" cellpadding="1" cellspacing="1" dir="ltr">
		<tr>
			<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Merchant<br/></td>
			<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Charges<br/></td>
			<td width="20"></td>
			<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Merchant Phone<br/></td>
			<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Merchant Fax<br/></td>
			<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Private Name<br/></td>
			<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Phone<br/></td>
			<td class="txt12b" bgcolor="#e9e9e9" style="border-bottom:1px solid gray;">Mobile<br/></td>
		</tr>
		<tr><td height="3"></td></tr>
	<%
	rsData.MoveFirst
	rsData.Move (session("ActualPage")-1)*session("PageSize")
	do until rsData.EOF or nShowCounter=int(session("PageSize"))

		if sSwitchColor then
			sColorBkg = ""
		else
			sColorBkg = "#eeeeee"
		end if
		sSwitchColor = not(sSwitchColor)
		
		nCompanyID = rsData("companyID")
		%>
		<tr bgcolor="<%= sColorBkg %>">
			<td class="txt11" align="left">
				<a href="#" onclick="OpenPop('Reports_CompanyDataPrintview.asp?CompanyID=<%= nCompanyID %>&proceed=false','fraCompanyData',640,350,1);" class="submenu2" style="font-size:11px; text-decoration:underline;">
				<%
				If int(len(dbtextShow(rsData("CompanyName"))))<26 then
					%>
					<%= dbtextShow(rsData("CompanyName")) %></a>
					<%
				else
					%>
					<span title="<%= dbtextShow(rsData("CompanyName")) %>"><%= left(dbtextShow(rsData("CompanyName")),25) & ".." %></span></a><br/>
					<%
				end if
				%>
			</td>
			<td class="txt10" align="right">
				<%= rsData("NumOfTrans") %><br/>
			</td>
			<td bgcolor="#ffffff"></td>
			<td class="txt11" dir="ltr" align="right">
				<%= trim(rsData("CompanyPhone")) %><br/>
			</td>
			<td class="txt11" align="right">
				<%= trim(rsData("CompanyFax")) %><br/>
			</td>
			<td class="txt11" align="left">
				<%= dbTextShow(trim(rsData("FirstName")) & " " & trim(rsData("LastName"))) %><br/>
			</td>
			<td class="txt11" align="right" nowrap="nowrap" dir="ltr"style="color:<%= sFontColor %>;">
				<%= trim(rsData("Phone")) %>
			</td>
			<td class="txt11" align="right">
				<%= trim(rsData("cellular")) %>
			</td>
		</tr>
		<%
		nShowCounter=nShowCounter+1
	rsData.movenext
	loop
	%>
	</table>
	
	
	<br/>
	<table width="90%" align="center" border="0" cellpadding="1" cellspacing="0" dir="ltr">
	<tr>
		<td align="right" class="txt12b" dir="ltr">
			Page <%= session("ActualPage") %> Of <%= rsData.PageCount %><br/>
		</td>
		<%
		if rsData.PageCount>1 then
			%>
			<td align="left" dir="ltr" class="txt13">
				<table border="0" cellspacing="1" cellpadding="0">
				<tr>
					<%
					if session("ActualPage")>1 then
						%>
						<td class="paging_PageNevigation" onmouseout="this.style.border='1px solid #d7d7d7'"
						 	onmouseover="this.style.border='1px solid black'"
						 	onclick="location.href='reports_chargePageUsage_data.asp?page=1&<%= sQueryFilter %>';">&lt;&lt;</td>
						<td class="paging_PageNevigation" onmouseout="this.style.border='1px solid #d7d7d7'"
							onmouseover="this.style.border='1px solid black'"
							onclick="location.href='reports_chargePageUsage_data.asp?&page=<%= session("ActualPage")-1 %>&<%= sQueryFilter %>';">&lt;</td>
						<%
					end if
					for nPage=1 to rsData.PageCount
						if int(nPage)>=int(session("ActualPage"))-5 AND int(nPage)<=int(session("ActualPage"))+5 then
							if trim(nPage)=trim(session("ActualPage")) then
								response.write "<td class=""paging_currentPage"">" & nPage & "</td>"
							Else
								response.write "<td class=""paging_otherPages"" onmouseout=""this.style.border='1px solid #d7d7d7'"" onmouseover=""this.style.border='1px solid black'""  onclick=""location.href='reports_chargePageUsage_data.asp?page=" & nPage & "&" & sQueryFilter & "';"">" & nPage & "</td>"
							end if
						end if
					next
					if int(session("ActualPage"))<rsData.PageCount then
						%>
						<td class="paging_PageNevigation" onmouseout="this.style.border='1px solid #d7d7d7'" 
							onmouseover="this.style.border='1px solid black'"
							onclick="location.href='reports_chargePageUsage_data.asp?page=<%= session("ActualPage")+1 %>&<%= sQueryFilter %>';">&gt;</td>
						<td class="paging_PageNevigation" onmouseout="this.style.border='1px solid #d7d7d7'" 
							onmouseover="this.style.border='1px solid black'"
							onclick="location.href='reports_chargePageUsage_data.asp?page=<%= rsData.PageCount %>&<%= sQueryFilter %>';">&gt;&gt;</td>
						<%
					end if
					%>
				</tr>
				</table>
			</td>
			<%
		End If
		%>
	</tr>
	</table>
	<%
end if

Server.ScriptTimeout = 90
%>
<% call closeConnection() %>
</body>
</html>
