<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.sqlClient" %>
<%@ Import Namespace="System.Data.SqlTypes" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="Netpay.Process" %>
<%@ Import Namespace="Netpay.CommonTypes" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<style>
    .text
    {    
        width: 100px;
        text-align: right;
    }
</style>

<script runat="server">
    Sub Page_Load()
        Security.CheckPermission(lblPermissions)
        If Not Page.IsPostBack Then
            Dim transactionId As Integer = dbPages.TestVar(Request("transactionId"), 1, 0, 0)
            Dim merchantId As Integer = dbPages.TestVar(Request("merchantId"), 1, 0, 0)
            Page.Title &= transactionId

            Dim data As SqlDataReader = dbPages.ExecReader("SELECT CompanyID, Currency, PaymentMethod, IPCountry, IsFraudByAcquirer FROM tblCompanyTransPass WHERE ID = " & transactionId)

            If data.Read() Then
                Dim colIndex = data.GetOrdinal("IsFraudByAcquirer")
                Dim isFraud As SqlBoolean = data.GetSqlBoolean(colIndex)
                mvFraudStatus.ActiveViewIndex = IIf(isFraud.IsNull Or isFraud.IsFalse, 0, 1)

                Dim terminalItem As Netpay.Bll.Merchants.ProcessTerminals = Netpay.Bll.Merchants.ProcessTerminals.GetTerminalForTransaction(data("CompanyID"), data("Currency"), data("PaymentMethod"), data("IPCountry"))
                If terminalItem IsNot Nothing Then
                    txtTerminalFee.Text = terminalItem.FraudRefundFixedFee.ToAmountFormat()
                End If
            End If

            data.Close()

            data = dbPages.ExecReader("SELECT PayID FROM tblCompanyTransPass WHERE OriginalTransID = " & transactionId & " AND TransSource_id = 50")
            If data.Read() Then
                If data("PayID") <> ";0;" Then
                    btnUnsetFraud.Enabled = False
                    divAlert.Visible = True
                End If
            End If
            data.Close()
        End If
    End Sub

    Private Sub SetAsFraud(sender As Object, e As EventArgs)
        Dim fee As Decimal = 0
        If rbFeeTerminal.Checked Then
            fee = txtTerminalFee.Text.ToDecimal(0)
        ElseIf rbFeeCustom.Checked Then
            fee = txtCustomFee.Text.ToDecimal(0)
        End If

        Dim transactionId As Integer = dbPages.TestVar(Request("transactionId"), 1, 0, 0)
        Dim merchantId As Integer = dbPages.TestVar(Request("merchantId"), 1, 0, 0)

        Dim transaction = Netpay.Bll.Transactions.Transaction.GetTransaction(merchantId, transactionId, TransactionStatus.Captured)
        transaction.SetFraud(True, fee)

        mvFraudStatus.ActiveViewIndex = 1
    End Sub

    Private Sub UnsetAsFraud(sender As Object, e As EventArgs)
        Dim transactionId As Integer = dbPages.TestVar(Request("transactionId"), 1, 0, 0)
        Dim merchantId As Integer = dbPages.TestVar(Request("merchantId"), 1, 0, 0)

        Dim transaction = Netpay.Bll.Transactions.Transaction.GetTransaction(merchantId, transactionId, TransactionStatus.Captured)
        transaction.SetFraud(False)

        mvFraudStatus.ActiveViewIndex = 0
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Fraud status For Transaction </title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body onload="focus();" style="padding:0 20px;">
	<form id="Form1" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Transaction Management<span> - Fraud status</span></h1><br />
		<div style="width:100%;">
			<asp:MultiView ID="mvFraudStatus" ActiveViewIndex="0" runat="server">
				<asp:View runat="server">
					<fieldset>
						<legend>SET AS FRAUD</legend>
                            <div>
                                &nbsp; Fee:
                                <br />
                                <asp:RadioButton id="rbFeeTerminal" GroupName="fraudFeeType" Checked="True" runat="server"/> Terminal: <asp:TextBox ID="txtTerminalFee" CssClass="text" Enabled="False" runat="server" /> 
                                <br />
                                <asp:RadioButton id="rbFeeCustom" GroupName="fraudFeeType" runat="server"/> Custom: <asp:TextBox ID="txtCustomFee" Text="0.00" CssClass="text" runat="server" />
                            </div>
                            <br /><br />
                            <div>
							    <asp:Button ID="btnSetFraud" UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" Text="Set" OnClick="SetAsFraud" />
                                <br /><br />
                                <div id="divAlert" visible="false" style="color:red; font-weight:bold;" runat="server">Transaction cannot be unset because the fraud fee has been settled.</div>
                            </div>
					</fieldset>
				</asp:View>
				<asp:View runat="server">
					<fieldset>
						<legend>UNSET AS FRAUD</legend>
							<asp:Button ID="btnUnsetFraud" UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" Text="Unset" OnClick="UnsetAsFraud" />
					</fieldset>
				</asp:View>
			</asp:MultiView>
		</div>
		<br />
	</form>
</body>
</html>
