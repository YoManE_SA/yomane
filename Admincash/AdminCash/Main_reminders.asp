<!--#include file="../include/func_security.asp"-->
<table border="0" cellspacing="1" width="92%" cellpadding="1" id="tblWarnings">
	<tr>
		<td class="txt14b" style="color:#2566AB;">
			WARNINGS
			<br /><br />
		</td>
	</tr>
</table>
<%
bHideWarnings = true

' 20110918 Tamir - Pending Events that are not processed too long
sSQL = "SELECT COUNT(ep.EventPending_id) " &_
       "FROM  [List].[EventPendingType] AS ept INNER JOIN [dbo].[EventPending] AS ep ON ept.EventPendingType_id = ep.EventPendingType_id " &_
       "WHERE ep.InsertDate < DATEADD(Minute, -ept.MinutesForWarning, GETDATE()) AND ept.MinutesForWarning > 0"
Set rsData = oledbData.Execute(sSQL)
nTemp=rsData(0)
If nTemp>0 Then
	sMsgBody="Pending Events table contains " & nTemp & " events, that should already be processed."
	Call DrawNote("NoteAlert", nTemp, "Unprocessed Pending Events", sMsgBody, "")
	bHideWarnings=False
	Response.Write("<br />")
End If

' 20100907 Tamir - Passed transactions waiting to be inserted into DB
Set fsoServer = Server.CreateObject("Scripting.FileSystemObject")
sPath = Server.MapPath("../../Netpay/Data/Uploads/ERROR_PASS.txt")
If fsoServer.FileExists(sPath) Then
	nTemp = 0
	Set tfError = fsoServer.OpenTextFile(sPath)
	Do Until tfError.AtEndOfStream
		sTemp = UCase(tfError.ReadLine)
		If InStr(sTemp, "INSERT INTO TBLCOMPANYTRANSPASS") > 0 Then nTemp = nTemp + 1
	Loop
	If nTemp > 0 Then
		If nTemp=1 Then
			sMsgBody = "There is one unregistered passed transaction.<br />The transaction must be manualy inserted to database."
		Else
			sMsgBody = "There are " & nTemp & " unregistered passed transactions.<br />The transactions must be manualy inserted to database."
		End If
		sMsgBody = sMsgBody & "<br /><a target=""_blank"" href=""" & PROCESS_URL & "/member/ExecErrorPass.aspx"">insert</a> &nbsp; <a href=""main_lite.asp"">refresh</a>"
		Call DrawNote("NoteAlert", nTemp, "Unregistered Passed Transactions", sMsgBody, "")
		bHideWarnings=False
		Response.Write("<br />")
	End If
	tfError.Close
	Set tfError = Nothing
	Set fsoServer = Nothing
End If

' 20101008 Tamir - Charge attempts without transactions
sSQL = "SELECT * FROM GetChargeAttemptsWithoutTransaction()"
Set rsData = oledbData.Execute(sSQL)
If rsData("Attempts") > 0 Then
	sToYear=Trim(Year(Now()))
	sToMonth=Right("0" & Trim(Month(Now())), 2)
	sToDay=Right("0" & Trim(Day(Now())), 2)
	sFromYear=Trim(Year(DateAdd("d", -3, Now())))
	sFromMonth=Right("0" & Trim(Month(DateAdd("d", -3, Now()))), 2)
	sFromDay=Right("0" & Trim(Day(DateAdd("d", -3, Now()))), 2)
	sMsgBody = rsData("Attempts") & " charge attempts were not completed in the last 3 days." & _
	"<br />First attempt: " & rsData("FirstID") & " (" & FormatDatesTimes(rsData("FirstDate"), 1, 1, 0) & ")." & _
	"<br />Last attempt: " & rsData("LastID") & " (" & FormatDatesTimes(rsData("LastDate"), 1, 1, 0) & ")." & _
	" &nbsp; <a href=""common_framesetB.aspx?pageMenuWidth=250&pageMenuUrl=Log_ChargeAttemptsFilter.aspx&pageBodyUrl=" & _
	Server.URLEncode("Log_ChargeAttempts.aspx?Source=Filter&iReplyCode=---&fdtrControl%24rdtpFrom=" & sFromYear & "-" & sFromMonth & "-" & sFromDay & "-00-00-00&fdtrControl_rdtpFrom_dateInput_text=" & sFromDay & "%2F" & sFromMonth & "%2F" & sFromYear & "+00%3A00&fdtrControl%24rdtpFrom%24dateInput=" & sFromYear & "-" & sFromMonth & "-" & sFromDay & "-00-00-00&fdtrControl_rdtpFrom_calendar_SD=%5B%5D&fdtrControl_rdtpFrom_calendar_AD=%5B%5B1980%2C1%2C1%5D%2C%5B2099%2C12%2C30%5D%2C%5B" & sFromYear & "%2C" & sFromMonth & "%2C8%5D%5D&fromDate=" & sFromDay & "%2F" & sFromMonth & "%2F" & sFromYear & "&fromTime=00%3A00%3A00&fdtrControl%24rdtpTo=" & sToYear & "-" & sToMonth & "-" & sToDay & "-23-59-59&fdtrControl_rdtpTo_dateInput_text=" & sToDay & "%2F" & sToMonth & "%2F" & sToYear & "+23%3A59&fdtrControl%24rdtpTo%24dateInput=" & sToYear & "-" & sToMonth & "-" & sToDay & "-23-59-59&fdtrControl_rdtpTo_calendar_SD=%5B%5D&fdtrControl_rdtpTo_calendar_AD=%5B%5B1980%2C1%2C1%5D%2C%5B2099%2C12%2C30%5D%2C%5B" & sToYear & "%2C" & sToMonth & "%2C8%5D%5D&toDate=" & sToDay & "%2F" & sToMonth & "%2F" & sToYear & "&toTime=23%3A59%3A59&sDurationFilterMode=lowerThan&iPageSize=100&Action=SEARCH") & """>see log</a>"
	Call DrawNote("NoteAlert", rsData("Attempts"), "Uncompleted Charge Attempts", sMsgBody, "")
	bHideWarnings=False
	Response.Write("<br />")
End If
rsData.close
Set rsData=Nothing
	
' 20090224 Tamir - Temporary Blocks on Debit companies
sSQL="SELECT dc_Name, DebitCompany_ID, dc_TempBlocks FROM tblDebitCompany WHERE dc_TempBlocks>0"
If PageSecurity.IsLimitedDebitCompany Then sSQL = sSQL & " AND DebitCompany_ID IN (SELECT ID FROM GetPermittedDebitCompanies('" & PageSecurity.Username & "'))"
Set rsData=oledbData.Execute(sSQL)
Do Until rsData.EOF
	sMsgBody=rsData("dc_Name") & " has been<br />temporarily blocked"
	Set rsData2=oledbData.Execute("SELECT * FROM tblDebitBlock WHERE db_IsUnblocked=0 AND db_DebitCompany=" & rsData("DebitCompany_ID"))
	If Not rsData2.EOF Then sMsgBody=sMsgBody & " on " & FormatDatesTimes(rsData2("db_InsertDate"), 1, 1, 0)
	rsData2.Close
	Call DrawNote("NoteAlert", rsData("dc_TempBlocks"), "Debit Company Temporarily Blocked", sMsgBody, "")
	bHideWarnings=False
	Response.Write("<br />")
	rsData.MoveNext
Loop
rsData.close
Set rsData=Nothing
Set rsData2=Nothing
	
'connection errors
sSQL = "SELECT Count(*) FROM tblLog_NoConnection WHERE lnc_InsertDate>DateAdd(hh, -6, GetDate()) "
nCount1 = ExecScalar(sSQL, 0)
If nCount1 > 0 Then
	sHeading = "Connection Failures " & "&nbsp;&nbsp;&nbsp;<a href=""common_framesetTabB.aspx?pageMenuWidth=260&pageMenuUrl=Log_ConnectionFilter.aspx&pageBodyUrl=Log_ConnectionData.aspx"" style=""white-space:nowrap;"">See log &gt;&gt;</a><br />"
	sBody = "There are " & nCount1 & " connection failures in last 6 hours.<br />"
	sSQL = "SELECT Count(*) FROM tblLog_NoConnection WHERE lnc_InsertDate>DateAdd(hh, -3, GetDate()) "
	nCount2 = ExecScalar(sSQL, 0)
	sBody = sBody & "There are " & nCount2 & " connection failures in last 3 hours.<br />"
	If nCount2 > 0 Then
		sSQL = "SELECT Count(*) FROM tblLog_NoConnection WHERE lnc_InsertDate>DateAdd(hh, -1, GetDate()) "
		nCount3 = ExecScalar(sSQL, 0)
		sBody = sBody & "There are " & nCount3 & " connection failures in the last hour.<br />"
		If nCount3 > 0 Then
			sSQL = "SELECT Max(lnc_InsertDate) FROM tblLog_NoConnection"
			dtLast = ExecScalar(sSQL, 0)
			sBody = sBody & "The last connection failure occured at " & FormatDateTime(dtLast, 3) & ".<br />"
		End If
	End If
	Call DrawNote("NoteAlert", nCount1, sHeading, sBody, "")
	bHideWarnings=False
	Response.Write("<br />")
End If

'connection errors without refund attempt
sSQL = "SELECT Count(*) FROM tblLog_NoConnection WHERE lnc_InsertDate BETWEEN DateAdd(d, -3, GetDate()) AND DateAdd(hh, -8, GetDate()) AND lnc_AutoRefundStatus=0"
nCount=ExecScalar(sSQL, 0)
If nCount>0 Then
	sBody="There are " & nCount & " connection failure without a refund attempt. " & _
	"<a href=""common_framesetTabB.aspx?pageMenuWidth=260&pageMenuUrl=Log_ConnectionFilter.aspx&pageBodyUrl=Log_ConnectionData.aspx""" & _
	" style=""white-space:nowrap;"">See log &gt;&gt;</a>"
	Call DrawNote("NoteAlert", nCount, "Unhandle refunds for connection failures ", sBody, "")
	bHideWarnings=False
	Response.Write("<br />")
End If

'-------------------------------------------------------------------------------------------------
'	Exchange rate
'-------------------------------------------------------------------------------------------------
sSQL="SELECT CUR_RateRequestDate, CUR_RateValueDate, CUR_BaseRate FROM tblSystemCurrencies WHERE CUR_ID=1"
set rsData=oledbData.Execute(sSQL)
if NOT rsData.EOF then
	nDateDiff = dateDiff("d",rsData("CUR_RateRequestDate"),now())
	nBankDateDiff = dateDiff("d",rsData("CUR_RateValueDate"),now())
	if nDateDiff>1 OR nBankDateDiff > 3  then
		bHideWarnings=false
		sMsgBody = "Exchange rates were updated " & nDateDiff & " days ago, "		
		sMsgBody = sMsgBody & "on " & formatDatesTimes(rsData("CUR_RateRequestDate"), 1, 0, 0) & ".<br />"
		sMsgBody = sMsgBody & "Bank last update was on " & formatDatesTimes(rsData("CUR_RateValueDate"), 1, 0, 0) & ".<br />"
		If SHOW_HEBREW Then sMsgBody = sMsgBody & " USD was equal to " & rsData("CUR_BaseRate") & " NIS.<br />"		
		Call drawNote("NoteAlert", nDateDiff, "Currency Exchange Rates", sMsgBody, "")
		Response.Write("<br />")
	End if
else
	bHideWarnings=false
	sMsgBody = "No data on currency exchange rates could be retrieved.<br />"
	Call drawNote("NoteAlert", nDateDiff, "Currency Exchange Rates", sMsgBody, "")
	Response.Write("<br />")
end if
rsData.close
Set rsData = nothing

'-------------------------------------------------------------------------------------------------
'	Failed reccuring charges
'-------------------------------------------------------------------------------------------------
sSQL="SELECT Count(*) FROM viewRecurringUnprocessed"
If PageSecurity.IsLimitedMerchant Then sSQL = sSQL & " WHERE rs_Company IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
nCount=ExecScalar(sSQL, 0)
If nCount>0 then
	bHideWarnings=false	
	sMsgBody = "There are " & nCount & " unprocessed charges.<br />"
	Call drawNote("NoteAlert", nCount, "Recurring Transactions", sMsgBody, "")
	Response.Write("<br />")
End if
Set rsData = nothing

if bHideWarnings then
	%>
	<script language="javascript" type="text/javascript">
		document.getElementById("tblWarnings").style.display="none";
	</script>
	<%
else
	Response.Write "<br /><br />"
end if
%>
<table border="0" cellspacing="1" width="92%" cellpadding="1" id="tblReminders">
<tr>
	<td class="txt14b" style="color:#2566AB;">
		REMINDERS
		<br /><br />
	</td>
</tr>
</table>
<%
bHideReminders = true
'-------------------------------------------------------------------------------------------------
'	Merchants - Transaction Files
'-------------------------------------------------------------------------------------------------
sSQL="SELECT COUNT(CompanyBatchFiles_id) AS numRecords FROM tblCompanyBatchFiles WHERE CBFStatus=" & BFS_New
If PageSecurity.IsLimitedMerchant Then sSQL = sSQL & " AND CBFCompany_id IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
nCount=ExecScalar(sSQL, 0)
if nCount>0 then
	bHideReminders=false
	sMsgBody = "There are " & nCount & " new transaction files.<br />"
	Call drawNote("Note", nCount, "Merchants - Transaction Files", sMsgBody, "")
	Response.Write("<br />")
end if
Set rsData = nothing
	
'-------------------------------------------------------------------------------------------------
'	Merchants - Bank Transfers
'-------------------------------------------------------------------------------------------------
Dim wireCount : wireCount = 0 : sMsgBody = ""
sSQL="SELECT count(WireMoney_id) AS numMsgs, wireFlag FROM tblWireMoney WHERE IsShow=1 And wireStatus IN(0, 2) And wireFlag IN(0, 1)"
If PageSecurity.IsLimitedMerchant Then sSQL = sSQL & " AND Company_id IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
sSQL = sSQL & " Group By wireFlag Order By wireFlag Asc"
set rsData = oledbData.Execute(sSQL)
Do While Not rsData.EOF
	wireCount = wireCount + rsData("numMsgs")
	If rsData("numMsgs") > 0 And rsData("wireFlag") = 0 then
		sMsgBody = sMsgBody & "There are " & rsData("numMsgs") & " transfers waiting/on-hold."
		sMsgBody = sMsgBody & " &nbsp;<a href=""common_framesetTabB.aspx?pageMenuWidth=260&pageMenuUrl=Wire_search_filter.aspx&pageBodyUrl=Wire_search_data.asp?Source=filter%26WireFlag=0%26wireStatus=0%26wireStatus=2%26wireType="" style=""white-space:nowrap;"">see list &gt;&gt;</a><br />"
	ElseIf rsData("numMsgs") > 0 And rsData("wireFlag") = 1 then
		sMsgBody = sMsgBody & "There are " & rsData("numMsgs") & " transfers approved and waiting for transfer."
		sMsgBody = sMsgBody & " &nbsp;<a href=""common_framesetTabB.aspx?pageMenuWidth=260&pageMenuUrl=Wire_search_filter.aspx&pageBodyUrl=Wire_search_data.asp?Source=filter%26WireFlag=1%26wireStatus=0%26wireStatus=2%26wireType="" style=""white-space:nowrap;"">see list &gt;&gt;</a><br />"
	End if
  rsData.MoveNext	
Loop
rsData.close
Set rsData = nothing
If wireCount > 0 Then 
	bHideReminders=false
	Call drawNote("Note", wireCount, "Merchants - Bank Transfers", sMsgBody, "")
	Response.Write("<br />")
End if

'-------------------------------------------------------------------------------------------------
'	Merchants - Refund Requests
'-------------------------------------------------------------------------------------------------
sSQL="SELECT count(ID) AS numMsgs FROM tblRefundAsk WHERE RefundAskStatus=0"
If PageSecurity.IsLimitedMerchant Then sSQL = sSQL & " AND CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
nCount = ExecScalar(sSQL, 0)
if nCount>0 then
	bHideReminders=false
	sSQL="SELECT TOP 1 ID, RefundAskDate FROM tblRefundAsk ORDER BY RefundAskDate DESC"
	set rsData2=oledbData.Execute(sSQL)
	sDate=formatDatesTimes(rsData2("RefundAskDate"), 1, 0, 0)
	nDiff = datediff("d",rsData2("RefundAskDate"),now())
	if nDiff<>0 then
		sMsg = nDiff & " days ago"
	else
		sMsg = "today"
	end if
	rsData2.close
	Set rsData2 = nothing
	
	sMsgBody = "There are " & nCount & " unhandled refund requests.<br />"
	sMsgBody = sMsgBody & "The last was on " & sDate & " (" & sMsg & ")"
	'sMsgBody = sMsgBody & " &nbsp;<a href=""common_framesetTabB.aspx?pageMenuWidth=260&pageMenuUrl=RefundRequest_filter.aspx&pageBodyUrl=Refund_ask_data.asp?RefundAskStatus=0"">see log &gt;&gt;</a><br />"
	
	Call drawNote("Note", nCount, "Merchants - Refund Requests", sMsgBody, "")
	Response.Write("<br />")
end if

'-------------------------------------------------------------------------------------------------
'	Forms Posts
'-------------------------------------------------------------------------------------------------
'Opens connection to FormPost database
'Dim oledbData_Form
'Set oledbData_Form = Server.CreateObject("ADODB.Connection")
'oledbData_Form.Open Application("FormsDSN")
'sSQL="SELECT count(ID) AS numMsgs FROM tblPost WHERE Pst_Status = 1"
'Set rsData = oledbData_Form.Execute(sSQL)
'If rsData("numMsgs")>0 Then
	'bHideReminders = false
	'sSQL="SELECT TOP 1 ID, Pst_Time FROM tblPost WHERE Pst_Status = 1 ORDER BY Pst_Time DESC"
	'Set rsData2=oledbData_Form.Execute(sSQL)
	'sDate = formatDatesTimes(rsData2("Pst_Time"), 1, 0, 0)
	'nDiff = datediff("d",rsData2("Pst_Time"),now())
	'If nDiff <> 0 then
		'sMsg = nDiff & " days ago"
	'Else
		'sMsg = "today"
	'End if
	'rsData2.close
	'Set rsData2 = Nothing
'	
	'sMsgBody = "There are " & rsData("numMsgs") & " unhandled messages.<br />"
	'sMsgBody = sMsgBody & "The last was on " & sDate & " (" & sMsg & ")"
	'sMsgBody = sMsgBody & " &nbsp;<a href=""common_framesetB.aspx?pageMenuWidth=500&pageMenuUrl=forms_posts.aspx?Status=1"" style=""white-space:nowrap;"">see list &gt;&gt;</a><br />"
	'
	'Call drawNote("Note", rsData("numMsgs"), "Forms Posts", sMsgBody, "")
	'Response.Write("<br />")
'End if
'rsData.close
'Set rsData = Nothing
'oledbData_Form.close
'Set oledbData_Form = Nothing

'-------------------------------------------------------------------------------------------------

if bHideReminders then
	%>
	<script language="javascript" type="text/javascript">
		document.getElementById("tblReminders").style.display="none";
	</script>
	<%
end if
%>