<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/func_htmlInputs.asp"-->
<%
Dim DCF_DebitCompanyID, DCF_Terminal
nDebitCompany_id = TestNumVar(request("debitCompany_id"), 0, -1, 0)
nShowSec = TestNumVar(Request("ShowSec"), 0, -1, 0)
If nShowSec = 0 Then nShowSec = 1

'---------------------------------------------------------------
'   Draw Menu
'---------------------------------------------------------------
bFirstMerchantMenuDataItem = True
sub ShowMerchantMenuDataItem(sText, sURL, sID)
	If bFirstMerchantMenuDataItem Then bFirstMerchantMenuDataItem = False _
	Else Response.Write("<td class=""txt11"" style=""color:#A9A7A2;"">|<br/></td>")
	If nShowSec = sID Then sStyle="font-weight:bold; background-color:#D6C8A8;" _
	Else sStyle="text-decoration:underline; background-color:;"
	if sOnClick="" then sOnclick="location.href='" & sURL & "';"
	%>
	<td align="center" class="txt10" title="<%= sURL %>" onclick="location.href='<%=sURL%>'" style="cursor:pointer;<%= sStyle %>"<%= sID %>>
		<%= uCase(sText) %>
	</td>
	<%
End sub

If nShowSec = 1 Then
	if trim(request("Action")) <> "" then
		sSQL = "UPDATE tblDebitCompany SET dc_name='" & TestStrVar(DBText(request("CompanyName")),1,50,"") & "', " &_
			"dc_EmergencyContact='" & TestStrVar(DBText(request("EmergencyContact")),0,50,"") & "', " &_
			"dc_EmergencyPhone='" & TestStrVar(DBText(request("EmergencyPhone")),0,50,"") & "', " &_
			"dc_description='" & TestStrVar(DBText(request("description")),0,20,"") & "', " &_
			"dc_isAllowRefund=" & TestNumVar(request("isAllowRefund"),1,1,0) & ", " &_
			"dc_isAllowPartialRefund=" & TestNumVar(request("isAllowPartialRefund"),1,1,0) & ", " &_
			"dc_isSupportPartialRefund=" & TestNumVar(request("isSupportPartialRefund"),1,1,0) & ", " &_
			"dc_IsNotifyRetReqAfterRefund=" & TestNumVar(request("isNotifyRetReqAfterRefund"),1,1,0) & ", " &_
			"dc_isActive=" & TestNumVar(request("isActive"),1,1,0) & ", " &_
			"dc_IsAutoRefund=" & TestNumVar(request("isAutoRefund"),1,1,0) & "," &_
			"IsDynamicDescriptor=" & TestNumVar(request("IsDynamicDescriptor"),1,1,0) & "," &_
			"RegistrationUsrname='" & Request("RegistrationUserName") & "'," &_
			"RegistrationPassword256=dbo.GetEncrypted256('" & Request("RegistrationPassword") & "') " &_
			"WHERE debitCompany_id=" & nDebitCompany_id
		oledbData.execute sSQL
	%>
	<script language="JavaScript1.2"> //window.parent.frmBody.location.href='system_debitCompanyList.asp'; </script>
	<%
	End if
ElseIf nShowSec = 2 Then
	DCF_DebitCompanyID = nDebitCompany_id
ElseIf nShowSec = 4 Then
  Dim Rs, N_ID, N_Username, N_Password
  if Request("DOK") <> "" Then
  	N_ID = TestNumVar(Request("ID"), 0, -1, 0)
	N_Username = Request("Userame") : N_Password = Request("Password")
	If N_ID > 0 Then
		ls_sql = "Update tblDebitCompanyLoginData Set username='" & dbText(N_Username) & "', Password='" & dbText(N_Password) & "' Where debitCompanyLoginData_id=" & N_ID
	Else 
		ls_sql = "Insert Into tblDebitCompanyLoginData (debitCompany_id, username, Password) Values (" & _
			nDebitCompany_id & ",'" & dbText(N_Username) & "','" & dbText(N_Password) & "')"
		N_ID = ExecScalar("Select Max(debitCompanyLoginData_id) From tblDebitCompanyLoginData", 0)
	End if
	oleDbData.Execute ls_sql
	if N_Password <> "" Then UpdatePasswordChange 3, N_ID, trim(N_Password)
	Response.Redirect "system_debitCompanyData.asp?debitCompany_id=" & nDebitCompany_id & "&ShowSec=" & nShowSec
  Elseif Request("ID") <> "" Then
  	N_ID = TestNumVar(Request("ID"), 0, -1, 0)
	If N_ID < 0 Then
		oleDbData.Execute "Delete From tblDebitCompanyLoginData Where debitCompanyLoginData_id=" & -N_ID
		DeletePassword 3, -N_ID
		Response.Redirect "system_debitCompanyData.asp?debitCompany_id=" & nDebitCompany_id & "&ShowSec=" & nShowSec
	End if
  End if
End if  
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<script language="JavaScript" src="../js/func_common.js"></script>
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css"/>
	<style>
		.tblInputs td { padding-right:8px; }
	</style>
	<script language="JavaScript">
		function IsKeyDigit() {
			var nKey=event.keyCode;
			if ((nKey==8)|(nKey==9)|(nKey==13)|(nKey==35)|(nKey==36)|(nKey==37)|(nKey==39)|(nKey==46)|(nKey==48)|(nKey==49)|(nKey==50)|(nKey==51)|(nKey==52)|(nKey==53)|(nKey==54)|(nKey==55)|(nKey==56)|(nKey==57)|(nKey==96)|(nKey==97)|(nKey==98)|(nKey==99)|(nKey==100)|(nKey==101)|(nKey==102)|(nKey==103)|(nKey==104)|(nKey==105)|(nKey==110)|(nKey==190)) { return true; };
			return false;
		}
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">
<%
sSQL="SELECT *, dbo.GetDecrypted256(RegistrationPassword256) as RegistrationPassword FROM tblDebitCompany Where DebitCompany_id=" & nDebitCompany_id
set rsData=oledbData.execute(sSQL)
If not rsData.EOF Then
	Set fsServer=Server.CreateObject("Scripting.FileSystemObject")
	
	if rsData("dc_isActive") then
		bIsActiveHtml="<span><li type=square style=color:#66cc66;></li> <span class=txt12>Active</span></span>"
	else
		bIsActiveHtml="<span><li type=square style=color:#ff6666;></li> <span class=txt12>Closed</span></span>"
	end if
	%>
	<table width="95%" align="right" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td id="pageMainHeadingTd">
			<span id="pageMainHeading">Debit Companies</span> <span id="pageMainSubHeading">- <%=UCase(rsData("dc_name")) %></span> &nbsp;
			<%
			If Trim(rsData("debitCompany_id")) <> "" Then
				If fsServer.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & nDebitCompany_id & ".gif")) Then
					Response.write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & nDebitCompany_id & ".gif"" align=""middle"" border=""0"" />")
				End if
			End If
			%>
		</td>
		<td align="right">
			<table width="70" border="0" cellspacing="0" cellpadding="2" style="border:1px solid #c0c0c0;">
			 <tr><td align="center" class="txt12"> <%= bIsActiveHtml %><br /> </td></tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="2"><br />
	 <table bgcolor="#f1f1f1" align="center" border="0" cellpadding="2" cellspacing="2" style=" width:100%; padding-top:2px;padding-bottom:2px; border:1px solid #A9A7A2;">
	  <tr>
		<%
		ShowMerchantMenuDataItem "Debit Company Details", "system_debitCompanyData.asp?debitCompany_id=" & nDebitCompany_id & "&ShowSec=1", 1
		ShowMerchantMenuDataItem "Fees & Payouts", "system_debitCompanyData.asp?debitCompany_id=" & nDebitCompany_id & "&ShowSec=2", 2
		'ShowMerchantMenuDataItem "Fees2", "system_debitCompany_Fees2.aspx?debitCompany_id=" & nDebitCompany_id, -1
		ShowMerchantMenuDataItem "Refunds Ask Log", "system_debitCompanyData.asp?debitCompany_id=" & nDebitCompany_id & "&ShowSec=3", 3
		ShowMerchantMenuDataItem "Users", "system_debitCompanyData.asp?debitCompany_id=" & nDebitCompany_id & "&ShowSec=4", 4
		'ShowMerchantMenuDataItem "Messages", "system_debitCompanyData.asp?debitCompany_id=" & nDebitCompany_id & "&ShowSec=5", 5
		ShowMerchantMenuDataItem "Notifications", "system_DebitCompanyRules.aspx?DebitCompany=" & nDebitCompany_id, -1
		%>
	  </tr>
	 </table>
	<br /></td></tr>
	<%
	pageSecurityLevel = 0
	pageClosingTags = "</table></td></tr></table>"
	Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
	%>
	<tr>
		<td colspan="2">
		<%If nShowSec = 1 Then%>
			<form method="post">
			<input type="Hidden" name="debitCompany_id" value="<%= nDebitCompany_id %>">
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
			<tr>
				<td class="MerchantSubHead">Details<br /></td>
				<td class="MerchantSubHead">Emergency<br /></td>
				<td class="MerchantSubHead">Settings<br /></td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td valign="top">
					Debit Name<br />
					<input type="text" size="28" maxlength="50" name="CompanyName" value="<%= rsData("dc_name") %>" class="inputData"><br />
					<br />
					Description<br />
					<textarea style="width:300px;" rows="5" class="inputData" name="description"><%= rsData("dc_description") %></textarea><br />
				</td>
				<td valign="top">
					Contact Name<br />
					<input type="text" size="28" maxlength="50" name="EmergencyContact" value="<%= rsData("dc_EmergencyContact") %>" class="inputData">
					<br /><br />
					Contact Phone<br />
					<input type="text" size="28" maxlength="50" name="EmergencyPhone" value="<%= rsData("dc_EmergencyPhone") %>" class="inputData">

					<br /><br /><br />
					Registration UserName<br />
					<input type="text" size="28" maxlength="50" name="RegistrationUserName" value="<%= rsData("RegistrationUsrname") %>" class="inputData">
					<br /><br />
					Registration Password<br />
					<input type="text" size="28" maxlength="50" name="RegistrationPassword" value="<%= rsData("RegistrationPassword") %>" class="inputData">

				</td>
				<td valign="top">
					<input type=checkbox name="isActive" value="1" <%If rsData("dc_isActive") Then%>checked<%End if%> />Enabled<br />
					<input type=checkbox name="isAllowRefund" value="1" <%If rsData("dc_isAllowRefund") Then%>checked<%End if%> />Allow refunds<br />	   
					&nbsp;&nbsp;&nbsp;&nbsp;<input type=checkbox name="isAllowPartialRefund" value="1" <%If rsData("dc_isAllowPartialRefund") Then%>checked<%End if%> />Allow partial refunds<br /> 
					&nbsp;&nbsp;&nbsp;&nbsp;<input type=checkbox name="isSupportPartialRefund" value="1" <%If rsData("dc_isSupportPartialRefund") Then%>checked<%End if%> />Support partial refunds via API<br /> 
					<input type=checkbox name="isNotifyRetReqAfterRefund" value="1" <%If rsData("dc_IsNotifyRetReqAfterRefund") Then%>checked<%End if%> />Notify on retrival requests after refund<br />	   
					<input type=checkbox name="isAutoRefund" value="1" <%If rsData("dc_IsAutoRefund") Then%>checked<%End if%> />Try AutoRefund on connection rrrors<br />
					<input type=checkbox name="IsDynamicDescriptor" value="1" <%If rsData("IsDynamicDescriptor") Then%>checked<%End if%> />Support dynamic descriptor<br />
					<br /><br />
					<b>On payment exclude the following fees:</b><br />
					<% 
						Set rsList = oledbData.Execute("Select tat.Name From DebitCompanyFeeExcludeInvoice dcfei " & _
							" Left Join List.TransAmountType tat ON (tat.TransAmountType_id = dcfei.TransAmountType_id)" & _
							" Where dcfei.DebitCompany_id=" & nDebitCompany_id)
						If rsList.EOF Then Response.Write("None.")
						Do While Not rsList.EOF
							Response.Write(rsList(0) & "<br/>")
						  rsList.MoveNext
						Loop
						rsList.Close()
					%>
					<br /><br />
					<%
						If nDebitCompany_id = 19 Then
							%>
								&nbsp;|&nbsp;
								<a href="<%=PROCESS_URL%>member/DebitRefund.asp?DebitID=19">Refund Timeouts in last 48 hours</a>
							<%
						ElseIf nDebitCompany_id = 21 Then
							%>
								&nbsp;|&nbsp;
								<a href="<%=PROCESS_URL%>member/DebitRefund.asp?DebitID=21">Refund Timeouts in last 48 hours</a>
							<%
						ElseIf nDebitCompany_id = 18 Or nDebitCompany_id = 46  Then
							%>
								&nbsp;|&nbsp;
								<a href="<%=PROCESS_URL%>member/DebitRefund.asp?DebitID=18">Refund Timeouts in last 48 hours</a>
							<%
						End If
					%>
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td colspan="4" align="left"><input type="submit" name="Action" value=" UPDATE " class="button1"></td>
			</tr>
			</table>
			</form>
		</td>
	</tr>
	</table>
	<%End If
	If nShowSec = 2 Then%>
		<!--#include file="IncDebitFees.asp"-->
	<%
	End if
	If nShowSec = 3 Then
	%>
	<table width="100%" border="0" cellspacing="2" cellpadding="1" align="center" bordercolor="#E8E8E8" style="table-layout:fixed;">
	  <tr>
		<td bgcolor="#f1f1f1" style="border-bottom:1px solid #A9A7A2;" class="txt11"><span class="txt11b">Date</span></td>
		<td bgcolor="#f1f1f1" style="border-bottom:1px solid #A9A7A2;" class="txt11" align="left"><span class="txt11b">DebitCompanyID</span></td>
		<td bgcolor="#f1f1f1" style="border-bottom:1px solid #A9A7A2;" class="txt11"><span class="txt11b">OrderID</span></td>
		<td bgcolor="#f1f1f1" style="border-bottom:1px solid #A9A7A2;" class="txt11"><span class="txt11b">Authorization</span></td>
		<td width="15">&nbsp;&nbsp;</td>
		<td bgcolor="#f1f1f1" style="border-bottom:1px solid #A9A7A2;" class="txt11"><span class="txt11b">Action</span></td>
	  </tr>		
		<%
		Dim xDoc, xChild, i
		Set xDoc = Server.CreateObject("Msxml2.DOMDocument")
		xDoc.Load Session("DataPath") & "Private\Logs\ccNotify.xml"
		If Not xDoc.documentElement Is Nothing Then
			Set xChild = xDoc.documentElement.ChildNodes
			For i = 0 To xChild.Length - 1
				if (nDebitCompany_id = "") Or (nDebitCompany_id = xChild(i).ChildNodes(1).text) Then
			%>
			<tr>
				<td class="txt11" valign="top" style="overflow: auto;"><%=xChild(i).ChildNodes(0).text%></td>
				<td class="txt11" valign="top" style="overflow: auto;"><%=xChild(i).ChildNodes(4).text%></td>
				<td class="txt11" valign="top" style="overflow: auto;"><%=xChild(i).ChildNodes(2).text%></td>
				<td class="txt11" valign="top" style="overflow: auto;"><%=xChild(i).ChildNodes(3).text%></td>
				<td></td>
				<td class="txt11" valign="top" style="overflow: auto;"><a href="">Refund</a></td>
			</tr>	
				<%
				End if
			Next
		End if
		Set xDoc = Nothing
		%>
		</table>
		<%
	End if
	If nShowSec = 4 Then
	%>
	<form method="post" id="AddFrm">
	 <input type="hidden" name="ID" value="0">
	 <table width="100%" style="padding:4px; border:1px solid silver">
	  <tr>
	   <td class="txt12">
		UserName: <input name="Userame" class="inputData" style="width:115px;" size="20" maxlength="20">&nbsp;&nbsp;
		Password: <input name="Password" type="Password" class="inputData" style="width:115px;" size="20" maxlength="20">&nbsp;&nbsp;
		<input type="submit" value=" ADD " name="DOK" class="button1" tabindex="16">&nbsp;&nbsp;
		<a href="javascript:undefined;" onclick="with(document.all.AddFrm){ Userame.value='';Password.value='';ID.value='0';DOK.value='ADD';}">Add</a>
	   </td>
	  </tr>
	 </table>
	</form>
	<br />
	 <table width="100%">
	  <tr>
	   <td bgcolor="#f1f1f1" style="border-bottom:1px solid #A9A7A2;" class="txt11"><span class="txt11b">UserName</span></td>
	   <td bgcolor="#f1f1f1" style="border-bottom:1px solid #A9A7A2;" class="txt11"><span class="txt11b">Password</span></td>
	   <td bgcolor="#f1f1f1" style="border-bottom:1px solid #A9A7A2;" class="txt11"><span class="txt11b">Delete</span></td>
	  </tr>
	<% 
	  Set Rs = oleDbData.Execute("Select * From tblDebitCompanyLoginData Where debitCompany_id=" & nDebitCompany_id & " Order By debitCompanyLoginData_id Desc")
	  Do While Not Rs.EOF
	%>
	  <tr>
	   <td><a href="javascript:undefined;" onclick="with(document.all.AddFrm){ Userame.value='<%=dbTextShow(Rs("username"))%>';Password.value='<%=dbTextShow(Rs("password"))%>';ID.value='<%=Rs("debitCompanyLoginData_id")%>';DOK.value='����';}"><%=dbTextShow(Rs("username"))%></a></td>
	   <td class="txt11"><%=dbTextShow(Rs("password"))%></td>
	   <td align="right" class="txt11"><a href="system_debitCompanyData.asp?ID=-<%=Rs("debitCompanyLoginData_id")%>&debitCompany_id=<%=nDebitCompany_id%>&ShowSec=<%=nShowSec%>">Delete</a></td>
	  </tr>
	<%
	   Rs.MoveNext
	  Loop
	  Rs.Close
	%>
	</table>
<%	
	End if
end if
rsData.close
set rsData = nothing
oledbData.close
set oledbData = nothing
%>
<br />
</body>
</html>