<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/MerchantTerminals.asp" -->
<!--#include file="../member/remoteCharge_Functions.asp" -->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
Dim ClientEmailAddress
sTransID = trim(request("transID"))
sPageBkgColor = trim(request("bkgColor"))
If sPageBkgColor = "" Then sPageBkgColor = "ffffff"
sIsShowBasicTransData = trim(request("isShowBasicTransData"))
If sIsShowBasicTransData = "" Then sIsShowBasicTransData = false
sPaymentMethod = trim(request("PaymentMethod"))
If sPaymentMethod - PMD_EC_MIN >=0 And sPaymentMethod - PMD_EC_MAX <= 0 Then sPaymentMethod = 2
If sPaymentMethod<>2 Then response.end

sTransTableType = trim(request("TransTableType"))
sBtnBarStyle = IIf(sPageBkgColor<>"ffffff","background-color:#ffffff; border:solid 1px #d2d2d2;","background-color:#f1efe2;")
If sTransTableType = "PreAuth" Then '31/03/2010 Udi added support to approved echeck
	sIsAllowAdmin = false
	sTransTableName = "tblCompanyTransApproval"
ElseIf sTransTableType = "fail" Then
	sIsAllowAdmin = false
	sTransTableName = "tblCompanyTransFail"
Elseif sTransTableType = "pass" Then
	sTransTableName = "tblCompanyTransPass"
    sIsAllowAdmin = trim(request("isAllowAdministration"))
    If sIsAllowAdmin = "" Then sIsAllowAdmin = true
Elseif sTransTableType = "pending" Then
	sTransTableName = "tblCompanyTransPending"
	sIsAllowAdmin = False
Else
    Response.end
End if

sQuery = "transID=" & sTransID & "&bkgColor=" & sPageBkgColor & "&isAllowAdministration=" & sIsAllowAdmin & "&PaymentMethod=" & sPaymentMethod & "&TransTableType=" & sTransTableType

'-----------------------------------------------------------------------
'	Execute admin actions if allowed
'-----------------------------------------------------------------------
If sIsAllowAdmin Then
	If trim(request("Action"))="updDeniedComment" Then
		sSQL="UPDATE " & sTransTableName & " SET DeniedAdminComment='" & DBText(request("deniedComment")) & "' WHERE id=" & sTransID
		oledbData.Execute sSQL
	Elseif trim(request("Action")) = "updDeniedStatus" Then
	    Dim bFoundFee, netpayFee_chbCharge, netpayFee_ClrfCharge
		If trim(request("status"))<>"0" Then
	        Set iRs = Server.CreateObject("Adodb.Recordset")
	        iRs.Open "Select tblCompanyTransPass.companyID, PaymentMethod, OCurrency, TerminalNumber From tblCompanyTransPass Where tblCompanyTransPass.ID=" & request("transID"), oleDbData, 0, 1
		        'bFoundFee = GetChbFees(iRs("companyID"), iRs("PaymentMethod"), iRs("OCurrency"), "", netpayFee_chbCharge, netpayFee_ClrfCharge)
		        bFoundFee = GetChbFeesEx(iRs("companyID"), iRs("PaymentMethod"), iRs("OCurrency"), "", iRs("TerminalNumber"), netpayFee_chbCharge, netpayFee_ClrfCharge)
		    iRs.Close
		Else
		    bFoundFee = True
		    netpayFee_chbCharge = 0: netpayFee_ClrfCharge = 0
		End if
		If bFoundFee Then 
		    sSQL="UPDATE tblCompanyTransPass SET DeniedDate=GetDate(), deniedstatus=" & trim(request("status")) & ", netpayFee_chbCharge=" & netpayFee_chbCharge & ", netpayFee_ClrfCharge=" & netpayFee_ClrfCharge & " WHERE id=" & trim(request("transID"))
		    oledbData.Execute sSQL
		Else
		    sMsgTxt = "Transaction denial was failed - Did not find terminal"
		End if
	end if
End if

'-----------------------------------------------------------------------
'	Retrieve transaction data
'-----------------------------------------------------------------------
sSQL="SELECT " & sTransTableName & ".*, List.TransSource.Name AS TransTypeName, tblDebitCompany.dc_name AS DebitCompanyName, tblDebitTerminals.dt_name, tblDebitTerminals.id AS terminalID, "
If sTransTableName = "tblCompanyTransPass" then
	sSQL = sSQL & " dbo.IsCHB(DeniedStatus, 0) IsChargeback "
Else
	sSQL = sSQL & " 0 IsChargeback "
End If
sSQL = sSQL & " FROM " & sTransTableName & " " & _
"LEFT OUTER JOIN tblDebitCompany ON " & sTransTableName & ".debitCompanyId = tblDebitCompany.debitCompany_id " &_
"LEFT OUTER JOIN tblDebitCompanyCode ON " & IIF(sTransTableName = "tblCompanyTransPass", "'000'", sTransTableName & ".replyCode") & "=tblDebitCompanyCode.Code " &_
"LEFT OUTER JOIN tblDebitTerminals ON " & sTransTableName & ".TerminalNumber = tblDebitTerminals.terminalNumber " &_
"LEFT OUTER JOIN List.TransSource ON " & sTransTableName & ".TransSource_id = List.TransSource.TransSource_id " &_
"WHERE " & sTransTableName & ".ID=" & sTransID
'response.Write ssql
set rsData=oledbData.execute(sSQL)
If rsData.EOF Then
    Response.Write("<br />Missing data<br />")
	response.end
End if
CR_Symbol = GetCurText(rsData("Currency"))
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<style type="text/css">
		.MainHeadings { font-weight:bold; font-size:10px; color:#505050; }
		.SmallHeadings { font-size:11px; color:#006699; }
		.Element { white-space: nowrap; }

		.TransData Table { padding:1px; margin:0px; border:0px; }
		.TransData th { padding-right:10px; font-weight:normal; text-align:left; font-size:11px; color:#006699; }
		.TransData td { padding-right:20px; font-weight:normal; font-size:11px; }

		.subTable Table { padding:0px; margin:0px; border:0px; }
		.subTable th { padding-right:16px; font-weight:normal; text-align:left; border-bottom:solid 1px silver; }
		.subTable td { padding-right:16px; font-weight:normal; }
	</style>
	<script language="JavaScript">
	    function OpenPop(sURL, sName, sWidth, sHeight, sScroll)
	    {
	        placeLeft = screen.width / 2 - sWidth / 2
	        placeTop = screen.height / 2 - sHeight / 2
	        var winPrint = window.open(sURL, sName, 'TOP=' + placeTop + ',LEFT=' + placeLeft + ',WIDTH=' + sWidth + ',HEIGHT=' + sHeight + ',scrollbars=' + sScroll);
	    }
	</script>
</head>
<body leftmargin="5" topmargin="0" rightmargin="15" class="itext" bgcolor="#<%= sPageBkgColor %>">
<table border="0" cellspacing="0" cellpadding="1" width="100%" height="100%" style="border-top:1px dashed #e2e2e2;">
<tr><td height="15"></td></tr>
<%
If Trim(sMsgTxt)<>"" Then Response.Write("<tr><td colspan=""2"" style=""color:red; padding-bottom:6px;"">" & sMsgTxt & "</td></tr>")

'-----------------------------------------------------------------------
'	Show fail code and description if needed
'-----------------------------------------------------------------------
If sTransTableType = "fail" Then
	If rsData("debitCompanyId")<>"" AND rsData("replyCode")<>"" Then

	    nDebitCompanyId = rsData("debitCompanyId")
		sDescription = "&nbsp;- No Details"

		sSQL="SELECT TOP 1 tblDebitCompanyCode.DescriptionOriginal FROM tblDebitCompanyCode WHERE" & _
		" tblDebitCompanyCode.DebitCompanyID=" & nDebitCompanyId & " AND tblDebitCompanyCode.Code='" & rsData("replyCode") & "'"
		sDescription =  "&nbsp;-&nbsp;" & ExecScalar(sSQL, "")
		%>
		<tr>
            <td>
                <table cellpadding="0" cellspacing="0">
				<tr>
					<td class="txt11b" style="color:#505050;" valign="top" width="100">
				        Failed reason<br />
					</td>
					<td valign="top" style="color:maroon;">
						<%
				        response.write "<span class=""txt11"" style=""color:maroon;"">" & rsData("replyCode") & "</span>" & dbTextShow(sDescription)
						%>
					</td>
				</tr>
		        <tr><td height="12"></td></tr>
				</table>
			</td>
		</tr>
		<%
	End If
End If

'-----------------------------------------------------------------------
'	Show administration options if allowed by refering page
'-----------------------------------------------------------------------
%>
<tr>
    <td height="100%" valign="top">
        <table cellpadding="0" cellspacing="0">
        <tr>
            <%
            If sIsShowBasicTransData Then
                %>
	            <td valign="top" style="padding-right:12px;">
		            <span class="MainHeadings">Transaction Info</span><br />
		            <%
		            If trim(rsData("id"))<>"" then response.write "<span class=""SmallHeadings"">Trans #:</span> " & rsData("id") & "</span><br/>"
		            If trim(rsData("InsertDate"))<>"" then response.write "<span class=""SmallHeadings"">Date:</span> " & rsData("InsertDate") & "</span><br/>"
		            If trim(rsData("PaymentMethodDisplay"))<>"" then response.write "<span class=""SmallHeadings"">Payment Method:</span> " & rsData("PaymentMethodDisplay") & "</span><br/>"
		            Response.write("<span class=""SmallHeadings"">Amount:</span> " & FormatCurrWCT(rsData("Currency"), rsData("Amount"), rsData("CreditType")))
		            If trim(rsData("Payments"))<>"" then response.write "<span class=""SmallHeadings"">Installments:</span> " & rsData("Payments") & "</span><br/>"
		            If trim(rsData("TransTypeName"))<>"" then response.write "<span class=""SmallHeadings"">Referring Page:</span> " & rsData("TransTypeName") & "</span><br/>"
		            %>
                </td>
                <%
            End if
            %>
	        <td valign="top" style="padding-right:12px;">
				<span class="MainHeadings">TRANSACTION INFO</span><br />
		        <%
		        If sTransTableType = "pass" Then
			        if trim(rsData("ApprovalNumber"))<>"" AND trim(rsData("ApprovalNumber"))<>"0000000" then response.write "<span class=""SmallHeadings"">Authorization Code:</span> " & rsData("ApprovalNumber") & "</span><br/>"
		        End if
		        If trim(rsData("comment"))<>"" then
                    response.write "<span class=""SmallHeadings"">Payer Comment:</span> "
				    If int(len(dbtextShow(trim(rsData("comment")))))<15 Then
					    response.write dbTextShow(trim(rsData("comment"))) & "<br />"
				    Else
					    response.write "<span style=""text-decoration:underline;"" title="""& dbtextShow(trim(rsData("comment"))) & """>" & left(dbtextShow(trim(rsData("comment"))),14) & "..</span><br />"
				    End If
		        End if
		        If trim(rsData("IPAddress"))<>"" then response.write "<span class=""SmallHeadings"">IP Address:</span> " & rsData("IPAddress") & "</span><br/>"
		        if (sTransTableName <> "tblCompanyTransApproval") Then _
		            If trim(rsData("payerIdUsed"))<>"" then response.write "<span class=""SmallHeadings"">Payer ID:</span> " & rsData("payerIdUsed") & "</span><br/>"
				If Trim(rsData("DebitReferenceCode"))<>"" then response.write "<span class=""SmallHeadings"">Debit Reference:</span> " & rsData("DebitReferenceCode") & "</span><br />"
				If Trim(rsData("DebitReferenceNum"))<>"" then response.write "<span class=""SmallHeadings"">Debit Reference #:</span> " & rsData("DebitReferenceNum") & "</span><br />"
		        if trim(rsData("OrderNumber"))<>"" then response.write "<span class=""Element""><span class=""SmallHeadings"">Order Number:</span> " & rsData("OrderNumber") & "</span></span><br/>"
		        if trim(rsData("DebitCompanyName"))<>"" then response.write "<span class=""SmallHeadings"">Debit Company:</span> " & dbTextShow(rsData("DebitCompanyName")) & "</span><br/>"
		        if trim(rsData("TerminalNumber"))<>"" then
			        response.write "<span class=""SmallHeadings"">Terminal:</span> <a href=""#"" onclick=""OpenPop('system_terminalTermDetail.aspx?terminalID=" & rsData("terminalID") & "','fraOrderPrint',750,600,1);"" class=""submenu2"" style=""font-size:10px; text-decoration:underline;"">" & dbTextShow(rsData("dt_name")) & "</a><br/>"
		        End If
		        if sTransTableType = "pass" Then
			        If trim(rsData("PD"))<>"01/01/1900" Then response.write "<span class=""SmallHeadings"">PD Date:</span> " & rsData("PD") & "</span><br/>"
					Response.write "<span class=""SmallHeadings"">Costs:</span> <span class=""txt11"" dir=""ltr"">" & FormatCurr(rsData("Currency"), rsData("DebitFee")) & "</span><br/>"
		        end if
		        %>
	        </td>
            <%
            If trim(sTransTableType) = "pass" Then
	            %>
	            <td valign="top" style="padding-right:12px;">
					<span class="MainHeadings">FEE INFO</span><br />
					<table class="TransData">
		            <%
		            response.write "<span class=""SmallHeadings"">Trans: <span class=""txt11"" dir=""ltr"">" & FormatCurr(rsData("Currency"), rsData("netpayFee_transactionCharge")) & "</span></span><br/>"
		            nRatioChargePercentage = FormatNumber( (rsData("NetpayFee_RatioCharge")/rsData("amount"))*100 ,1,-1)
		            response.write "<span class=""SmallHeadings"">Clearing: <span class=""txt11"" dir=""ltr"">(&plusmn;" & nRatioChargePercentage & "%) <span class=""txt11"" dir=""ltr"">" & FormatCurr(rsData("Currency"), rsData("NetpayFee_RatioCharge")) & "</span></span><br/>"
		            response.write "<span class=""SmallHeadings"">Copy R: <span class=""txt11"" dir=""rtl"">" & FormatCurr(rsData("Currency"), rsData("netpayFee_ClrfCharge")) & "</span></span><br/>"
		            response.write "<span class=""SmallHeadings"">Chb: <span class=""txt11"" dir=""rtl"">" & FormatCurr(rsData("Currency"), rsData("netpayFee_chbCharge")) & "</span></span>"
		            %>
	            </td>
	            <%
            End if

            '-----------------------------------------------------------------------
            '	eCheck data
            '-----------------------------------------------------------------------
            sSQL="SELECT id, CompanyId, CustomerId, AccountName, BankAccountTypeId, insertDate, PersonalNumber, PhoneNumber, Email, Comment, BankName," & _
            " BankCity, BankCountry, BankPhone, IsNull(sl.name, '') AS BankStateName, BirthDate, BillingAddressId, dbo.GetDecrypted256(AccountNumber256) AccountNumber," & _
            " dbo.GetDecrypted256(RoutingNumber256) RoutingNumber FROM tblCheckDetails LEFT JOIN [List].[StateList] AS sl ON tblCheckDetails.BankState=sl.stateid WHERE tblCheckDetails.ID=" & rsData("PaymentMethodID")
            set rsData3=oledbData.execute(sSQL)
            If not rsData3.EOF Then
                ClientEmailAddress = rsData3("Email")
                BillingAddressId = rsData3("BillingAddressId")
	            On Error Resume Next
		            sRoutingNumber = rsData3("routingNumber")
                    sAccountName = rsData3("accountNumber")
	            On Error GoTo 0
	            %>
                <td valign="top" style="padding-right:12px;">
                    <span class="MainHeadings">Account Info</span><br />
		            <%
		            If trim(rsData3("accountName"))<>"" then response.write "<span class=""SmallHeadings"">Name:</span> " & rsData3("accountName") & "<br/>"
		            if trim(rsData3("routingNumber"))<>"" then response.write "<span class=""SmallHeadings"">Routing No.</span> " & CStr(sRoutingNumber) & "<br/>"
		            if trim(rsData3("accountNumber"))<>"" then response.write "<span class=""Element""><span class=""SmallHeadings"">Account No.:</span> " & CStr(sAccountName) & "</span><br/>"
		            If trim(rsData3("bankAccountTypeId"))<>"" then
			            if rsData3("bankAccountTypeId")=1 then sAccountType="Checking" else sAccountType="Savings" end if
			            response.write "<span class=""Element""><span class=""SmallHeadings"">Type:</span> " & sAccountType & "</span><br/>"
		            end if
		            %>
	            </td>
                <td valign="top" style="padding-right:12px;">
                    <span class="MainHeadings">Bank Info</span><br />
		            <%
		            If trim(rsData3("BankName"))<>"" then response.write "<span class=""SmallHeadings"">Name:</span> " & rsData3("BankName") & "<br/>"
		            If trim(rsData3("BankPhone"))<>"" then response.write "<span class=""SmallHeadings"">Phone:</span> " & rsData3("BankPhone") & "<br/>"
		            If trim(rsData3("BankCity"))<>"" then response.write "<span class=""SmallHeadings"">City:</span> " & rsData3("BankCity") & "<br/>"
		            If trim(rsData3("BankStateName"))<>"" then response.write "<span class=""SmallHeadings"">State:</span> " & rsData3("BankStateName") & "<br/>"
		            If trim(rsData3("bankCountry"))<>"" then response.write "<span class=""SmallHeadings"">Country:</span> " & rsData3("bankCountry") & "<br/>"
		            %>
	            </td>
                <td valign="top" style="padding-right:12px;">
                    <span class="MainHeadings">Payer Info</span><br />
		            <%
		            If trim(rsData3("PersonalNumber"))<>"" then response.write "<span class=""SmallHeadings"">PIN Number:</span> " & rsData3("PersonalNumber") & "<br/>"
		            If trim(rsData3("PhoneNumber"))<>"" then response.write "<span class=""SmallHeadings"">Phone:</span> " & rsData3("PhoneNumber") & "<br/>"
		            If trim(rsData3("Email"))<>"" then response.write "<span class=""Element""><span class=""SmallHeadings"">Email:</span> " & rsData3("Email") & "</span><br/>"
		            %>
	            </td>
	            <%
            End if
            rsData3.close
            set rsData3 = nothing
            
            '-----------------------------------------------------------------------
            '	Data from Billing
            '-----------------------------------------------------------------------
            If BillingAddressId>0 Then
	            sSQL="SELECT tblBillingAddress.address1, tblBillingAddress.address2, tblBillingAddress.city, tblBillingAddress.zipCode, sl.name AS stateName, cl.Name AS countryName " &_
	            "FROM tblBillingAddress LEFT OUTER JOIN List.CountryList AS cl ON tblBillingAddress.countryId = cl.CountryID LEFT OUTER JOIN [List].[StateList] AS sl ON tblBillingAddress.stateId = sl.stateid WHERE tblBillingAddress.id=" & BillingAddressId
	            set rsData3=oledbData.execute(sSQL)
	            If not rsData3.EOF then
		            %>
		            <td valign="top" style="padding-right:12px;">
	                    <span class="MainHeadings">Billing Address</span><br />
			            <%
			            If trim(rsData3("address1"))<>"" then response.write "<span class=""SmallHeadings"">Address 1:</span> " & dbTextShow(rsData3("address1")) & "<br/>"
			            if trim(rsData3("address2"))<>"" then response.write "<span class=""SmallHeadings"">Address 2:</span> " & dbTextShow(rsData3("address2")) & "<br/>"
			            If trim(rsData3("city"))<>"" then response.write "<span class=""SmallHeadings"">City:</span> " & rsData3("city") & "<br/>"
			            If trim(rsData3("zipCode"))<>"" then response.write "<span class=""Element""><span class=""SmallHeadings"">Zip Code:</span> " & rsData3("zipCode") & "</span><br/>"
			            If trim(rsData3("stateName"))<>"" then response.write "<span class=""Element""><span class=""SmallHeadings"">State:</span> " & dbTextShow(rsData3("stateName")) & "</span><br/>"
			            If trim(rsData3("countryName"))<>"" then response.write "<span class=""Element""><span class=""SmallHeadings"">Country:</span> " & dbTextShow(rsData3("countryName")) & "</span><br/>"
			            %>
		            </td>
		            <%
	            end if
	            rsData3.close
	            set rsData3 = nothing
            End if
            %>
        </tr>
        </table>
    </td>
</tr>
<tr><td><br /></td></tr>
<% If sIsAllowAdmin Then %>
	<!--#include file="trans_detail_administration.asp"-->
<% End if %>
<tr><td height="10"></td></tr>
</table>
</body>
</html>	
<%
rsData.close
set rsData = nothing
call closeConnection()
%>					