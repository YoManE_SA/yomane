<!--#include file="../globalConst.asp"-->
<%
Function GetDaysInMonth(iMonth, iYear)
	Dim dTemp
	dTemp = DateAdd("d", -1, DateSerial(iYear, iMonth + 1, 1))
	GetDaysInMonth = Day(dTemp)
End Function

Function GetWeekdayMonthStartsOn(dAnyDayInTheMonth)
	Dim dTemp
	dTemp = DateAdd("d", -(Day(dAnyDayInTheMonth) - 1), dAnyDayInTheMonth)
	GetWeekdayMonthStartsOn = WeekDay(dTemp)
End Function

Function SubtractOneMonth(dDate)
	SubtractOneMonth = DateAdd("m", -1, dDate)
End Function

Function AddOneMonth(dDate)
	AddOneMonth = DateAdd("m", 1, dDate)
End Function

Dim dDate     ' Date we're displaying calendar for
Dim iDIM      ' Days In Month
Dim iDOW      ' Day Of Week that month starts on
Dim iCurrent  ' Variable we use to hold current day of month as we write table
Dim iPosition ' Variable we use to hold current position in table

' Get selected date.  There are two ways to do this.
' First check if we were passed a full date in RQS("date").
' If so use it, if not look for seperate variables, putting them togeter into a date.
' Lastly check if the date is valid...if not use today
If IsDate(Request.QueryString("date")) Then
	dDate = CDate(Request.QueryString("date"))
Else
	If IsDate(Request.QueryString("day") & "/" & Request.QueryString("month") & "/" & Request.QueryString("year")) Then
		dDate = Request.QueryString("day") & "/" & CDate(Request.QueryString("month") & "/" & Request.QueryString("year"))
	Else
		dDate = Date()
		' The annoyingly bad solution for those of you running IIS3
		If Len(Request.QueryString("month")) <> 0 Or Len(Request.QueryString("day")) <> 0 Or Len(Request.QueryString("year")) <> 0 Or Len(Request.QueryString("date")) <> 0 Then
			Response.Write "The date you picked was not a valid date.  The calendar was set to today's date.<br /><br />"
		End If
		' The elegant solution for those of you running IIS4
		'If Request.QueryString.Count <> 0 Then Response.Write "The date you picked was not a valid date.  The calendar was set to today's date.<br /><br />"
	End If
End If

'Now we've got the date.  Now get Days in the choosen month and the day of the week it starts on.
iDIM = GetDaysInMonth(Month(dDate), Year(dDate))
iDOW = GetWeekdayMonthStartsOn(dDate)
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminIE.css" type="text/css" rel="stylesheet" />
	<style type="text/css">
		<!--
		A { color:black; font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; text-align: center; }
		.tdDays { background-color: #003366; color:white; font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; text-align: center; }
		.tdYear { background-color: #F5F5F5; font-family: Arial, Helvetica, sans-serif; font-size: 10px; text-align: center; }
		.tdArrow { background-color: #F5F5F5; font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; text-align: center; }
		.tdToday { background-color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 9px; font-weight: bold; text-align: center; width: 14; height: 11; cursor: hand; }
		.tdOther { background-color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 9px; font-weight: normal; text-align: center; width: 16; height: 13; cursor: hand; }
		A.Arrow {font-family: Arial; color: black;	text-decoration : none;	font-size : 10px;}
		A.Arrow:HOVER {font-family: Arial; color :navy ;	text-decoration : none;	font-size : 10px;}
		-->
	</style>
	<script language="JavaScript">
		function placeDates(sDate)
		{
			if (event.shiftKey) {
				parent.frmMenu.iToDate.value=sDate;
			}
			else {
				parent.frmMenu.iFromDate.value=sDate;
			}
		}
		
		// disble selecting text in document xxxxxxxxxxx
		function disableselect(e){
			return false
		}
		function reEnable(){
			return true
		}
		document.onselectstart=new Function ("return false")
		// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
</script>
</head>
<body bgcolor="#eeeeee" leftmargin="0" topmargin="0" rightmargin="0" marginwidth="0" class="itextM" dir="ltr">
<table border="0" cellspacing="0" cellpadding="0" align="right" bgcolor="#004080">
<tr><td>
<table border="0" cellspacing="1" cellpadding="1" dir="rtl">
<TR>
	<td colspan="7" align="center" bgcolor="#004080">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD ALIGN="right" class="tdArrow"><A class="Arrow" HREF="./reports_calendar_iframe.asp?date=<%= SubtractOneMonth(dDate) %>">&lt;&lt;</TD>
				<TD ALIGN="center" class="tdYear"><span style="font-size:11px;"><%= MonthName(Month(dDate)) %></span> <%= Year(dDate) %></TD>
				<TD ALIGN="left" class="tdArrow"><A class="Arrow" HREF="./reports_calendar_iframe.asp?date=<%= AddOneMonth(dDate) %>">&gt;&gt;</TD>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="tddays">S<br /></td>
	<td class="tddays">M<br /></td>
	<td class="tddays">T<br /></td>
	<td class="tddays">W<br /></td>
	<td class="tddays">T<br /></td>
	<td class="tddays">F<br /></td>
	<td class="tddays">S<br /></td>
</tr>
<%
' Write spacer cells at beginning of first row if month doesn't start on a Sunday.
If iDOW <> 1 Then
	Response.Write vbTab & "<TR>" & vbCrLf
	iPosition = 1
	Do While iPosition < iDOW
		Response.Write vbTab & vbTab & "<TD bgcolor=#e9e9e9><img src=../images/1_space.gif  width=1 height=1 border=0></TD>" & vbCrLf
		iPosition = iPosition + 1
	Loop
End If

' Write days of month in proper day slots
iCurrent = 1
iPosition = iDOW
Do While iCurrent <= iDIM
	' If we're at the begginning of a row then write TR
	If iPosition = 1 Then
		Response.Write vbTab & "<TR>" & vbCrLf
	End If
	
	' If the day we're writing is the selected day then highlight it somehow.
	If iCurrent = Day(dDate) and Month(dDate) = Month(now) Then
		%>
		<td class="tdToday"  onclick="placeDates('<%= iCurrent %>/<%= Month(dDate) %>/<%= Year(dDate) %>');" style="border:1px solid black;"><%= iCurrent %></td>
		<%
	Else
		%>
		<TD class="tdOther" onclick="placeDates('<%= iCurrent %>/<%= Month(dDate) %>/<%= Year(dDate) %>');">
			<%= iCurrent %><br />
		</TD>
		<%
	End If
	
	' If we're at the endof a row then write /TR
	If iPosition = 7 Then
		Response.Write vbTab & "</TR>" & vbCrLf
		iPosition = 0
	End If
	
	' Increment variables
	iCurrent = iCurrent + 1
	iPosition = iPosition + 1
Loop

' Write spacer cells at end of last row if month doesn't end on a Saturday.
If iPosition <> 1 Then
	Do While iPosition <= 7
		Response.Write vbTab & vbTab & "<TD bgcolor=#e9e9e9><img src=../images/1_space.gif  width=1 height=1 border=0></TD>" & vbCrLf
		iPosition = iPosition + 1
	Loop
	Response.Write vbTab & "</TR>" & vbCrLf
End If
%>
</TABLE>
</td></tr>
</table>
</body>
</html>

