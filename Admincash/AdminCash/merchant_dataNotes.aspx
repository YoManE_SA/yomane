<%@ Page Language="C#" %>
<%@ Import Namespace="Netpay.Infrastructure" %>
<%@ Register TagPrefix="Rad" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="TabMenu.ascx" TagPrefix="UC1" TagName="TabMenu" %>
<script runat="server" type="text/C#">
    protected Netpay.Bll.Merchants.Merchant merchant;
    protected override void OnLoad(EventArgs e)
    {
        int merchantId = Request["companyID"].ToNullableInt32().GetValueOrDefault();
		merchant = Netpay.Bll.Merchants.Merchant.Load(merchantId);
        Response.Buffer = true;
        if (!IsPostBack) {
            rptFiles.DataSource = Netpay.Bll.Accounts.File.LoadForAccount(merchant.AccountID);
            rptFiles.DataBind();

            rptNotes.DataSource = Netpay.Bll.Accounts.Note.LoadForAccount(merchant.AccountID);
            rptNotes.DataBind();

            RadEditor1.Content = Netpay.Bll.Merchants.Merchant.GetContent(merchant.AccountID, Netpay.CommonTypes.Language.English, "RulesAndRegulations.html");
        }
        base.OnLoad(e);
    }

    protected void RulesSave_Click(object sender, EventArgs e)
    {
        Netpay.Bll.Merchants.Merchant.SaveContent(merchant.AccountID, Netpay.CommonTypes.Language.English, "RulesAndRegulations.html", RadEditor1.Content);
    }

    protected void NoteAdd_Click(object sender, EventArgs e)
    {
        Netpay.Bll.Accounts.Note.AddNote(merchant.AccountID, null, txtNote.Text);
		txtNote.Text = "";
        rptNotes.DataSource = Netpay.Bll.Accounts.Note.LoadForAccount(merchant.AccountID);
        rptNotes.DataBind();
    }

    protected void Files_Command(object sender, RepeaterCommandEventArgs e)
    {
        var file = Netpay.Bll.Accounts.File.Load(e.CommandArgument.ToString().ToNullableInt().GetValueOrDefault());
        switch (e.CommandName){
            case "Approve":
				file.SetApprove(true);
                break;
            case "Decline":
				file.SetApprove(false);
                break;
            case "Update":
                file.AdminComment = (e.Item.FindControl("txtComment") as TextBox).Text; 
                break;
            case "Delete":
				file.Delete();
                break;
            case "Download":
                WebUtils.SendFile(file.FilePath, null, "attachment");
                break;
        }
		if (e.CommandName != "Delete") file.Save();
        rptFiles.DataSource = Netpay.Bll.Accounts.File.LoadForAccount(merchant.AccountID);
        rptFiles.DataBind();
    }
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Merchant Fees</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
    <link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
    <script src="../js/func_common.js" type="text/jscript"></script>
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../js/jquery-ui-1.8.10.custom/css/custom-theme/jquery-ui-1.8.10.custom.css" />
</head>
<body>
    <script type="text/javascript" language="javascript">
        function showFileRow(imgObj, rowIndex) {
            var rowObj = document.getElementById('tblFileList').rows[rowIndex + 1];
            rowObj.style.display = rowObj.style.display != '' ? '' : 'none';
            imgObj.src = rowObj.style.display != '' ? '../images/tree_expand.gif' : '../images/tree_collapse.gif';
        }
    </script>
    <form runat="server" id="form1">
        <asp:ScriptManager runat="server" />
	    <UC1:TabMenu ID="TabMenu1" Width="95%" runat="server" idFieldName="companyID" />
        <table width="95%">
            <tr>
                <td valign="top" style="padding-right:20px;">
                    <span class="MerchantSubHead">Note</span><br /><br />
                    <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="3" Width="100%" />
                    <br />
                    <asp:Button runat="server" Text="Add New" OnClick="NoteAdd_Click" CssClass="button1" style="width:110px;" />
                    <br /><br />
                    <asp:Repeater ID="rptNotes" runat="server">
                        <HeaderTemplate>
                            <table width="100%" class="formNormal">
                                <tr>
                                    <th>Date</th>
                                    <th>Added by</th>
                                    <th>Text</th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><asp:Literal runat="server" Text='<%# Eval("InsertDate") %>' /></td>
                                <td><asp:Literal runat="server" Text='<%# Eval("UserName") %>' /></td>
                                <td><asp:Literal runat="server" Text='<%# Eval("Text") %>' /></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br /><br />
                    <hr />
                    <span class="MerchantSubHead">Rules And Regulations</span><br /><br />
                    <Rad:RadEditor ID="RadEditor1" runat="server" ContentAreaCssFile="~/styles.css" Width="100%" Height="200" Skin="Windows7" ToolbarMode="ShowOnFocus" >
                        <Modules>
                            <Rad:EditorModule Name="RadEditorHtmlInspector" Visible="false" />
                            <Rad:EditorModule Name="RadEditorNodeInspector" Visible="false" />
                            <Rad:EditorModule Name="RadEditorDomInspector" Visible="false" />
                            <Rad:EditorModule Name="RadEditorStatistics" Visible="false" />
                        </Modules>
                    </Rad:RadEditor>
                    <br />
                    <asp:Button runat="server" Text="Save" OnClick="RulesSave_Click" />
                </td>
                <td width="50%" valign="top" style="padding-left:20px;">
                    <span class="MerchantSubHead" style="float:left;">Files</span> <span style="float:right;">[ <a href="FileArchiveData.aspx?MerchantID=<%= merchant.MerchantID %>">UPLOAD NEW</a> ]</span>
                    <br />
                    <asp:Repeater ID="rptFiles" runat="server" OnItemCommand="Files_Command">
                        <HeaderTemplate>
                            <table class="formNormal" width="100%" id="tblFileList">
                                <tr>
                                    <th width="20"></th>
                                    <th>File Title</th>
                                    <th>Type</th>
                                    <th>Insert Date</th>
                                    <th>Approved</th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><img src="../images/tree_expand.gif" onclick="showFileRow(this, this.parentNode.parentNode.rowIndex)" style="cursor:pointer;" /></td>
                                <td>
									<asp:Image runat="server" ImageUrl='<%# string.Format("/NPCommon/ImgFileExt/{0}.gif", (string)Eval("FileExt")) %>' width="14" height="14" />
									<asp:LinkButton ID="Literal1" runat="server" Text='<%# Eval("FileTitle") %>' CommandName="Download" CommandArgument='<%# Eval("ID") %>' />
                                </td>
                                <td><asp:Literal runat="server" Text='<%# Netpay.Bll.Accounts.File.GetFileTypeText((byte?)Eval("FileItemType_id")) %>' /></td>
                                <td><asp:Literal runat="server" Text='<%# Eval("InsertDate") %>' /></td>
                                <td><asp:Literal runat="server" Text='<%# Eval("AdminApprovalDate") == null ? "" : "<img src=\"/NPCommon/Images/icon_v.gif\" align=\"top\" /> [" + Eval("AdminApprovalUser") + "]" %>' /></td>
                            </tr><tr style="display:none;">
                                <td colspan="5">
                                    <table width="100%">
                                        <tr>
                                            <td>Comment</td>
                                            <td width="80%"><asp:TextBox runat="server" id="txtComment" style="width:100%" TextMode="MultiLine" Rows="2" Text='<%# Eval("AdminComment") %>' /></td>
                                        </tr>
                                        <asp:PlaceHolder runat="server" Visible='<%# Eval("AdminApprovalDate") != null %>' >
                                            <tr>
                                                <td>Approval Date</td>
                                                <td><%# Eval("AdminApprovalDate") %></td>
                                            </tr>
                                            <tr>
                                                <td>Approval User</td>
                                                <td><%# Eval("AdminApprovalUser") %></td>
                                            </tr>
                                        </asp:PlaceHolder> 
                                        <tr>
                                            <td colspan="2" style="text-align:right;">
                                                <asp:Button runat="server" CommandArgument='<%# Eval("ID") %>' Text="Approve" CommandName="Approve" Visible='<%# Eval("AdminApprovalDate") == null %>' />
                                                <asp:Button runat="server" CommandArgument='<%# Eval("ID") %>' Text="Decline" CommandName="Decline" Visible='<%# Eval("AdminApprovalDate") != null %>' />
                                                <asp:Button runat="server" CommandArgument='<%# Eval("ID") %>' Text="Delete" CommandName="Delete" OnClientClick="if (!confirm('Delete file?')) return false;"  />
                                                <asp:Button runat="server" CommandArgument='<%# Eval("ID") %>' Text="Update" CommandName="Update" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>