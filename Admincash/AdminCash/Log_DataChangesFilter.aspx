<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        dsTable.ConnectionString = dbPages.DSN
        dsField.ConnectionString = dbPages.DSN
        dsUser.ConnectionString = dbPages.DSN
        dsAction.ConnectionString = dbPages.DSN
        
        If Not Page.IsPostBack Then
            fdtrControl.FromDateTime = Date.Now.AddDays(-7).AddDays(1).ToShortDateString
            fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
        End If
    End Sub
</script>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		table table td{vertical-align:top;}
	</style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
	<form runat="server" target="frmBody" action="log_DataChanges.aspx" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />
	<h1><asp:label ID="lblPermissions" runat="server" /> Data Changes<span> - Filter</span></h1>

	<table class="filterNormal">
		<tr><th colspan="2">Data Change</th></tr>
		<tr>
			<td>Table</td>
			<td>
				<asp:Repeater ID="Repeater1" DataSourceID="dsTable" runat="server">
					<HeaderTemplate><select name="Table"><option value=""></option></HeaderTemplate>
					<ItemTemplate>
						<asp:Literal ID="Literal1" Text='<%#"<option value=""" & Eval("DCL_Table") & """>" & Eval("DCL_Table") & "</option>"%>' runat="server" />
					</ItemTemplate>
					<FooterTemplate></select></FooterTemplate>
				</asp:Repeater>
				<asp:SqlDataSource ID="dsTable" runat="server" SelectCommand="SELECT DISTINCT DCL_Table FROM tblDataChangeLog ORDER BY DCL_Table" />
			</td>
		</tr>
		<tr>
			<td>Field</td>
			<td>
				<asp:Repeater ID="Repeater2" DataSourceID="dsField" runat="server">
					<HeaderTemplate><select name="Field"><option value=""></option></HeaderTemplate>
					<ItemTemplate>
						<asp:Literal ID="Literal2" Text='<%#"<option value=""" & Eval("DCL_Field") & """>" & Eval("DCL_Field") & "</option>"%>' runat="server" />
					</ItemTemplate>
					<FooterTemplate></select></FooterTemplate>
				</asp:Repeater>
				<asp:SqlDataSource ID="dsField" runat="server" SelectCommand="SELECT DISTINCT DCL_Field FROM tblDataChangeLog ORDER BY DCL_Field" />
			</td>
		</tr>
		<tr>
			<td>User</td>
			<td>
				<asp:Repeater ID="Repeater3" DataSourceID="dsUser" runat="server">
					<HeaderTemplate><select name="User"><option value=""></option></HeaderTemplate>
					<ItemTemplate>
						<asp:Literal ID="Literal3" Text='<%#"<option value=""" & Eval("DCL_User") & """>" & Eval("DCL_User") & "</option>"%>' runat="server" />
					</ItemTemplate>
					<FooterTemplate></select></FooterTemplate>
				</asp:Repeater>
				<asp:SqlDataSource ID="dsUser" runat="server" SelectCommand="SELECT DISTINCT DCL_User FROM tblDataChangeLog ORDER BY DCL_User" />
			</td>
		</tr>
		<tr>
			<td>Action</td>
			<td>
				<asp:Repeater ID="Repeater4" DataSourceID="dsAction" runat="server">
					<HeaderTemplate><select name="Action"><option value=""></option></HeaderTemplate>
					<ItemTemplate>
						<asp:Literal ID="Literal1" Text='<%#"<option value=""" & Eval("DCL_Action") & """>" & Eval("DCL_Action") & "</option>"%>' runat="server" />
					</ItemTemplate>
					<FooterTemplate></select></FooterTemplate>
				</asp:Repeater>
				<asp:SqlDataSource ID="dsAction" runat="server" SelectCommand="SELECT DISTINCT DCL_Action FROM tblDataChangeLog ORDER BY DCL_Action" />
			</td>
		</tr>
		<tr>
			<td>Type</td>
			<td>
				<input type="radio" name="Important" value="0" id="inpImportantAll" class="option" checked="checked" /><label for="inpImportantAll">All</label>
				<input type="radio" name="Important" value="1" id="inpImportantOnly" class="option" /><label for="inpImportantOnly">Important only</label>
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td>
				<select name="iPageSize">
					<option value="20" selected="selected">20 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td><input type="checkbox" name="SQL2" value="1" checked="checked" class="option" />SQL2</td>
			<td align="right"><input type="submit" value="SEARCH"><br /></td>
		</tr>
	</table>
	</form>
</asp:Content>