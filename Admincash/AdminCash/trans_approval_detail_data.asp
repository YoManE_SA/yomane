<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
sTransID = trim(request("transID"))
sCompanyID = trim(request("CompanyID"))
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminIE.css" type="text/css" rel="stylesheet" />
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" class="itext" dir="rtl" onLoad="focus();">
<table border="0" width="100%" cellspacing="0" cellpadding="0" dir="rtl" align="center">
<tr>
	<td style="padding:15px" valign="top">
		<%
		sSQL="SELECT CompanyName FROM tblCompany WHERE ID=" & sCompanyID
		set rsData = oledbData.execute(sSQL)
		if not rsData.EOF then
			sSQL="SELECT tblCreditCard.BillingAddressId, " &_
			"dbo.GetDecrypted256(tblCreditCard.CCard_number256) CCard_number, tblCreditCard.ExpYY, tblCreditCard.ExpMM, tblCreditCard.cc_cui, " &_
			"tblCreditCard.Member, tblCreditCard.PersonalNumber, tblCreditCard.PhoneNumber, tblCreditCard.Email, tblCompanyTransApproval.*, pm.name AS 'ccTypeHebName' " &_
			"FROM tblCompanyTransApproval INNER JOIN tblCreditCard ON tblCompanyTransApproval.CreditCardID = tblCreditCard.ID " &_
			"INNER JOIN List.PaymentMethod AS pm ON tblCreditCard.ccTypeID = pm.PaymentMethod_id WHERE tblCompanyTransApproval.ID=" & sTransID
			'response.write sSQL & "<br />"
			set rsData = oledbData.execute(sSQL)
			if not rsData.EOF then
				sAmount = rsData("Amount")
				%>
				<table align="right" border="0" cellpadding="1" dir="ltr" cellspacing="2" width="100%">
				<tr>
					<td colspan="2" align="right" class="txt12" dir="ltr"><%= rsData("InsertDate") %><br /></td>
				</tr>
				<tr><td height="8"></td></tr>
				<tr>
					<td width="45%" valign="top">
						<table align="right" border="0" cellpadding="1" dir="ltr" cellspacing="2">
						<%
						If rsData("IPAddress")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("IPAddress") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">����� �����<br /></td>
							</tr>
							<%
						end if
						if rsData("TerminalNumber")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("TerminalNumber") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� ����<br /></td>
							</tr>
							<%
						End If
						%>
						</table>
					</td>
					<td valign="top">
						<table align="right" border="0" cellpadding="1" dir="ltr" cellspacing="2">
						<%
						If rsData("OrderNumber")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("OrderNumber") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� �����<br /></td>
							</tr>
							<%
						end if
						%>
						<tr>
							<td align="right" class="txt12" dir="ltr">
								<%= sTransID %><br />
							</td>
							<td width="8"></td>
							<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� ����<br /></td>
						</tr>
						<tr>
							<td align="right" nowrap class="txt12" dir="ltr">
								<%
								if trim(rsData("CCard_number"))<>"" then
									sCCardNum = rsData("CCard_number")
									response.write "<span dir=ltr>" & right(sCCardNum, 4) & "</span><br />"
								end if
								%>
							</td>
							<td width="8"></td>
							<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� �����<br /></td>
						</tr>
						<tr>
							<td align="right" class="txt12" dir="ltr">
								<%= trim(rsData("ExpMM")) %>/<%= trim(rsData("ExpYY")) %><br />
							</td>
							<td width="8"></td>
							<td align="right" class="txt12b" bgcolor="#e9e9e9" dir="rtl">���� �����<br /></td>
						</tr>
						<tr>
							<td align="right" class="txt12" dir="rtl"><%= rsData("ccTypeHebName") %><br /></td>
							<td width="8"></td>
							<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">��� �����<br /></td>
						</tr>
						<%
						if rsData("cc_cui")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= DecCVV(rsData("cc_cui")) %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">����� ������<br /></td>
							</tr>
							<%
						End If
						if rsData("Member")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("Member") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">�� ��� ������<br /></td>
							</tr>
							<%
						end if
						if rsData("PersonalNumber")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("PersonalNumber") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� �.� �� ��� ������<br /></td>
							</tr>
							<%
						End If
						if rsData("PhoneNumber")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("PhoneNumber") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">����� �� ��� ������<br /></td>
							</tr>
							<%
						End If
						if rsData("Email")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("Email") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">������ �� ��� ������<br /></td>
							</tr>
							<%
						End If
						%>
						<tr>
							<td align="right" class="txt12" dir="ltr">
								<%
								if int(rsData("CreditType"))=0 then response.write "<span style=""color:red;"">" & FormatCurr(rsData("Currency"), -rsData("Amount")) & "</span>" _
								else response.write FormatCurr(rsData("Currency"), rsData("Amount"))
								%>
							</td>
							<td width="8"></td>
							<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">����<br /></td>
						</tr>
						<%
						if int(rsData("CreditType"))=8 then
							nPay=int(sAmount/rsData("Payments"))
							nPayFirst=sAmount-nPay*(rsData("Payments")-1)
							if int(rsData("Payments"))>1 then
							%>		
							<tr>
								<td align="right" dir="rtl" class="txt12"><%= rsData("Payments") %><br /></td>
								<td width="8"></td>
								<td align="right" dir="rtl" class="txt12b" bgcolor="#e9e9e9">���� �������<br /></td>
							</tr>
							<tr>
								<td align="right" dir="ltr" class="txt12">
									<%=FormatCurr(rsData("Currency"), nPayFirst)%>
								</td>
								<td width="8"></td>
								<td align="right" dir="rtl" class="txt12b" bgcolor="#e9e9e9">����� �����<br /></td>
							</tr>
							<tr>
								<td align="right" dir="ltr" class="txt12">
									<%=FormatCurr(rsData("Currency"), nPay)%>
								</td>
								<td width="8"></td>
								<td align="right" dir="rtl" class="txt12b" bgcolor="#e9e9e9">��� �������<br /></td>
							</tr>
							<%
							end if
						end if
						if trim(rsData("ApprovalNumber"))<>"" AND trim(rsData("ApprovalNumber"))<>"0000000" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("ApprovalNumber") %></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� �����<br /></td>
							</tr>
							<%
						End If
                        '-----------------------------------------------------------------------
                        '	Data from Billing
                        '-----------------------------------------------------------------------
                        If int(rsData("BillingAddressId"))>0 Then
	                        sSQL="SELECT ba.address1, ba.address2, ba.city, ba.zipCode, sl.name AS stateName, cl.Name AS countryName " &_
	                        "FROM tblBillingAddress AS ba LEFT OUTER JOIN [List].[CountryList] AS cl ON ba.countryId = cl.countryId LEFT OUTER JOIN [List].[StateList] AS sl ON ba.stateId = sl.stateid WHERE ba.id=" & rsData("BillingAddressId")
	                        set rsData2=oledbData.execute(sSQL)
	                        If not rsData2.EOF then
		                        %>
		                        <tr>
			                        <td align="right" class="txt12" dir="rtl"><%= rsData2("address1") %></td>
			                        <td width="8"></td>
			                        <td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">����� 1<br /></td>
		                        </tr>
		                        <tr>
			                        <td align="right" class="txt12" dir="rtl"><%= rsData2("address2") %></td>
			                        <td width="8"></td>
			                        <td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">����� 2<br /></td>
		                        </tr>
		                        <tr>
			                        <td align="right" class="txt12" dir="rtl"><%= rsData2("city") %></td>
			                        <td width="8"></td>
			                        <td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���<br /></td>
		                        </tr>
		                        <tr>
			                        <td align="right" class="txt12" dir="rtl"><%= rsData2("zipCode") %></td>
			                        <td width="8"></td>
			                        <td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">�����<br /></td>
		                        </tr>
		                        <tr>
			                        <td align="right" class="txt12" dir="rtl"><%= rsData2("stateName") %></td>
			                        <td width="8"></td>
			                        <td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">�����<br /></td>
		                        </tr>
		                        <tr>
			                        <td align="right" class="txt12" dir="rtl"><%= rsData2("countryName") %></td>
			                        <td width="8"></td>
			                        <td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���<br /></td>
		                        </tr>
		                        <%
	                        end if
	                        rsData2.close
	                        set rsData2 = nothing
                        End if
						%>
						</table>
					</td>
				</tr>
				</table>
				<br />
				<%
			end if
		end if
		%>
	</td>
</tr>
</table><br />
<% call closeConnection() %>
</body>
</html>
