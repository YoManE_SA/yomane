<%@ Page Language="VB" EnableEventValidation="false" %>

<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="UC" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
	Dim sWhere As String = ""
	Dim BLCommonType() As String
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)

		NetpayConst.GetConstArray(47, 0, Nothing, BLCommonType)
		If Not IsPostBack Then
			For i As Integer = 0 To BLCommonType.Length - 1
				If BLCommonType(i) <> Nothing Then BL_Type.Items.Add(New ListItem(BLCommonType(i), i))
			Next
			BL_Value.Text = Request("BL_Value")
			BL_Type.SelectedValue = Request("BL_Type")
		End If
		
		PGData.PageSize = IIf(Trim(Request("PageSize")) <> "", Request("PageSize"), 25)
		If Not String.IsNullOrEmpty(Request("BL_Value")) Then sWhere &= " And BL_Value LIKE '%" & dbPages.DBText(Request("BL_Value")) & "%'"
		If Not String.IsNullOrEmpty(Request("BL_Type")) Then sWhere &= " And BL_Type=" & dbPages.TestVar(Request("BL_Type"), 0, -1, 0)
		If Not String.IsNullOrEmpty(Request("BlockType")) Then sWhere &= " And Sign(BL_CompanyID)=" & dbPages.TestVar(Request("BlockType"), 0, 1, 0)
		If Not String.IsNullOrEmpty(Request("BL_CompanyID")) Then sWhere &= " And BL_CompanyID IN (0, " & dbPages.TestVar(Request("BL_CompanyID"), 1, 0, 0) & ")"
		If Security.IsLimitedMerchant Then sWhere &= " AND (BL_CompanyID=0 OR BL_CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "')))"
		If sWhere.Length > 0 Then sWhere = "Where " & sWhere.Substring(5)
		BL_CompanyID.Value = fmMerchant.SelectedValue
	End Sub
	
	Sub AddRecord(ByVal o As Object, ByVal e As EventArgs)
		Dim sSQL As String = "SELECT BL_ID From tblBLCommon WHERE BL_Type=" & dbPages.TestVar(BL_Type.SelectedValue, 1, 0, 0) & _
		" AND BL_Value='" & dbPages.DBText(BL_Value.Text) & "' AND BL_CompanyID=" & dbPages.TestVar(BL_CompanyID.Value, 1, 0, 0)
		Dim BL_ID As Integer = dbPages.TestVar(dbPages.ExecScalar(sSQL), 1, 0, 0)
		If (BL_ID > 0) Then
			sSQL = "UPDATE tblBLCommon SET BL_Comment='" & dbPages.DBText(BL_Comment.Text) & "' Where BL_ID=" & BL_ID
		Else
			sSQL = "EXEC Risk.spInsertBlacklistItem " & dbPages.TestVar(fmMerchant.SelectedValue, 1, 0, 0) & ", " & dbPages.TestVar(BL_Type.SelectedValue, 1, 0, 0) & "," & _
			" '" & dbPages.DBText(BL_Value.Text) & "', '" & dbPages.DBText(BL_Comment.Text) & "'"
		End If
		dbPages.ExecSql(sSQL)
		If Request("New") = "1" Then Response.Redirect("BLCommon.aspx?BL_Value=" & Server.UrlEncode(BL_Value.Text))
	End Sub

	Sub RemoveRecords(ByVal o As Object, ByVal e As EventArgs)
		dbPages.ExecSql("DELETE FROM tblBLCommon WHERE BL_ID IN (" & dbPages.TestNumericList(Request("DBL_ID"), 0) & ")")
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<script src="../js/func_common.js" type="text/javascript" language="javascript"></script>
</head>
<body>
	<form id="Form1" runat="server">
	<table style="width: 95%;" align="center">
		<tr>
			<td id="pageMainHeading">
				<asp:Label ID="lblPermissions" runat="server" />
				Common Blacklist
			</td>
		</tr>
		<tr>
			<td>
				<br />
			</td>
		</tr>
		<tr>
			<td>
				<fieldset>
					<legend style="color: gray;">ADD NEW BLOCK</legend>
					<table class="formNormal" cellpadding="1" cellspacing="0" border="0" style="width: 100%;">
						<tr>
							<td style="font: normal 11px;">
								Type
							</td>
							<td style="font: normal 11px;">
								Merchant
							</td>
							<td style="font: normal 11px;">
								Value
							</td>
							<td style="font: normal 11px;">
								Comment
							</td>
							<td style="font: normal 11px;">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td>
								<asp:DropDownList ID="BL_Type" runat="server">
									<asp:ListItem Value="" />
								</asp:DropDownList>
							</td>
							<td>
								<UC:FilterMerchants ID="fmMerchant" runat="server" ClientField="BL_CompanyID" IsSingleLine="true"
									ShowTitle="false" />
							</td>
							<td>
								<asp:TextBox ID="BL_Value" runat="server" />
							</td>
							<td>
								<asp:TextBox ID="BL_Comment" runat="server" Width="300" />
							</td>
							<td style="text-align: right;">

								<script language="javascript" type="text/javascript">
									function IsFilledBL() {
										if (document.getElementById('BL_Value').value == '') {
											alert("Value is not specified!");
											document.getElementById('BL_Value').focus();
											return false;
										}
										if (document.getElementById('BL_Type').value == '') {
											alert("Type is not specified!");
											document.getElementById('BL_Type').focus();
											return false;
										}
										return true;
									}
								</script>

								<asp:Button ID="btnAdd" runat="server" Text=" ADD " OnClick="AddRecord" OnClientClick="if (!IsFilledBL()) return false;" />
								<asp:HiddenField ID="BL_CompanyID" runat="server" />
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>
	<br />
	<%
		If Request("New") <> "1" Then
	%>
	<table class="formNormal" align="center" style="width: 95%;">
		<tr>
			<th>
				ID
			</th>
			<th>
				Type
			</th>
			<th>
				Merchant
			</th>
			<th>
				Value
			</th>
			<th>
				Comment
			</th>
			<th>
				Created
			</th>
			<th>
				Created by
			</th>
			<th style="text-align: center;">
				Remove
			</th>
		</tr>
		<%
			Dim sSQL As String = "SELECT tblBLCommon.BL_ID RowOrderKey, tblBLCommon.*, CompanyName" & _
			" FROM tblBLCommon LEFT JOIN tblCompany ON tblCompany.ID=BL_CompanyID " & sWhere & " ORDER BY BL_InsertDate desc"
			PGData.OpenDataset(sSQL)
			Dim sUser As String
			While PGData.Read()
				sUser = "\" & PGData("BL_User") & " "
				sUser = sUser.Substring(sUser.LastIndexOf("\") + 1).Trim
				Response.Write("<tr onmouseover=""this.style.backgroundColor='#f0f0f0';"" onmouseout=""this.style.backgroundColor='';"">")
				Response.Write("<td>" & PGData("BL_ID") & "</td>")
				Response.Write("<td>" & BLCommonType(PGData("BL_Type")) & "</td>")
				If dbPages.TestVar(PGData("BL_CompanyID"), 1, 0, 0) > 0 Then
					Response.Write("<td><a href=""merchant_data.asp?CompanyID=" & PGData("BL_CompanyID") & """ target=""_blank"">" & PGData("CompanyName") & "</a></td>")
				Else
					Response.Write("<td>----------</td>")
				End If
				Response.Write("<td>" & PGData("BL_Value") & "</td>")
				Response.Write("<td>" & PGData("BL_Comment") & "</td>")
				Response.Write("<td>" & dbPages.FormatDateTime(PGData("BL_InsertDate")) & "</td>")
				Response.Write("<td>" & sUser & "</td>")
				Response.Write("<td style=""text-align:center;""><input type=""checkbox"" class=""option"" name=""DBL_ID"" value=""" & PGData("BL_ID") & """ /></td>")
				Response.Write("</tr><tr><td height=""1"" colspan=""8"" bgcolor=""silver""></td></tr>")
			End While
			PGData.CloseDataset()
		%>
	</table>
	<br />
	<table class="formNormal" align="center" width="95%" border="0">
		<tr>
			<td>
				<UC:Paging runat="Server" ID="PGData" PageID="PageID" />
			</td>
			<td style="text-align: right;">
				<%
					If PGData.iTable.Rows.Count > 0 Then
				%>
				<span style="display: none;">
					<asp:TextBox ID="txtRecords" runat="server" Text="0" /></span>
				<asp:Button ID="btnRemove" runat="server" Text="REMOVE" OnClick="RemoveRecords" OnClientClick="if(checkCount('DBL_ID') == 0){ alert('No record selected!'); return false; }; return confirm('Do you really want to remove the selected record(s)?');" />
				<%
				End If
				%>
			</td>
		</tr>
	</table>
	<%
	End If
	%>
	</form>
</body>
</html>
