<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" MasterPageFile="~/AdminCash/FiltersMaster.master" Title="SECURITY LOG - Filter" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script runat="server">
	Sub Page_Load()
        ddlDocument.Items.Add("")
		Dim drData1 As SqlDataReader = dbPages.ExecReader("SELECT ID, sd_URL FROM tblSecurityDocument ORDER BY sd_Title")
		Do While drData1.Read()
			ddlDocument.Items.Add(New ListItem(drData1(1), drData1(0)))
		Loop
		drData1.Close()

        ddlUser.Items.Add("")
		Dim drData2 As SqlDataReader = dbPages.ExecReader("SELECT ID, su_Name FROM tblSecurityUser ORDER BY ID")
		Do While drData2.Read()
			ddlUser.Items.Add(New ListItem(drData2(1), drData2(0)))
		Loop
		drData2.Close()
		
		fdtrControl.FromDateTime = Date.Now.AddMonths(-1).AddDays(1).ToShortDateString
		fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
	End Sub
</script>
<asp:Content runat="server" ContentPlaceHolderID="body" >
	<form runat="server" action="Log_UserSec_data.aspx" method="get" target="frmBody" onsubmit="return filterForm_Submit(null)">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
	<input type="hidden" name="from" value="menu" />

	<h1><asp:label ID="lblPermissions" runat="server" /> Security Log<span> - Filter</span></h1>
	<table class="filterNormal">
		<tr>
			<td colspan="2">
				<Uc1:FilterDateTimeRange Title="" ID="fdtrControl" runat="server" />
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
			<td>Document</td>
			<td><asp:DropDownList CssClass="grayBG" ID="ddlDocument" Width="130px" runat="server" /></td>
		</tr>
		<tr>
			<td>User</td>
			<td><asp:DropDownList CssClass="grayBG" ID="ddlUser" Width="130px" runat="server" /></td>
		</tr>
		<tr>
			<td>IP</td>
			<td><input type="text" style="background-color:#f5f5f5;" name="IPAddress" /></td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			 <td colspan="2"><input type="checkbox" name="SQL2" value="1" checked="checked" class="option" />Use SQL2 when possible</td>
		</tr>
		<tr>
			<td class="indent">
				<select name="iPageSize">
				<option value="20" selected="selected">20 Rows/page</option>
				<option value="50">50 Rows/page</option>
				<option value="75">75 Rows/page</option>
				<option value="100">100 Rows/page</option>
				</select>
			</td>
			<td class="indent" align="right" nowrap="nowrap">
				<input type="submit" class="buttonFilter" value="Search" />
			</td>
		</tr>
	</table>
	</form>
</asp:Content>