<%@ Page Language="VB" EnableViewState="false" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml"%>
<%@	Import Namespace="System.Text.RegularExpressions"%>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<script runat="server">
	Sub Page_PreInit()
		If Security.IsLimitedDebitCompany Then
			If dbPages.TestVar(dbPages.ExecScalar("SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "') WHERE ID=18"), 1, 0, 0) <> 18 Then Response.Redirect("security_message.aspx")
		End If
	End Sub
	
    Private Function MapFileName(fileName As String) As String
        Return Domain.Current.MapPrivateDataPath("Account Files/DebitCompany/BnS/PDF/" & fileName)
    End Function
    
	Sub UploadFile(ByVal o As Object, ByVal e As EventArgs)
		litError.Text = String.Empty
		litFileInfo.Text = String.Empty
		If Not fuPDF.HasFile Then
			litError.Text = "No file is uploaded!"
			Exit Sub
		End If
		Dim sFileName As String = fuPDF.FileName
		fuPDF.SaveAs(MapFileName(sFileName))
		litFileInfo.Text = "File uploaded: " & sFileName
		Dim nFileData As New XmlDocument
		Dim nPgfObg As New WebSupergoo.ABCpdf6.Doc()
		Dim sSQL As New StringBuilder(String.Empty)
		Dim UserID As Integer
		Dim objUserID As Object
		objUserID = dbPages.ExecScalar("SELECT ID FROM tblSecurityUser WHERE su_IsActive=1 AND su_Username='" & dbPages.DBText(Security.Username) & "'")
		UserID = IIf(IsDBNull(objUserID),-1,CType(objUserID,Integer))
		Dim sCurrency As String = ""	
		Dim sComment As String = ""
		Dim paymentDate As Date	
		Dim isCHB As Boolean
		Dim arrCHB() As String = Nothing
		Dim arrData() As String = Nothing
		If sFileName.ToLower.EndsWith(".pdf") Then
			nPgfObg.Read(MapFileName(sFileName))
			Dim theCount As Integer = nPgfObg.PageCount
			Dim i As Integer
			For i = 1 To theCount
				nPgfObg.PageNumber = i
				nFileData.LoadXml(nPgfObg.GetText("SVG"))
				ParsePage(nFileData, i, sSQL, UserID, sCurrency, sComment, paymentDate, isCHB, arrCHB, arrData)
			Next
			nPgfObg.Dispose()
			dbPages.ExecSql(sSQL.ToString)
		Else
			litFileInfo.Text = sFileName & " is not valid PDF file."
		End If
	End Sub
	
	Private Function clearSpaces(workStr As String) As String
		If Len(""&trim(""&workStr)) = 0 then
			clearSpaces=""
			Exit Function
		End If
		Dim startLen As Integer, endLen As Integer
		Dim sResult As String
		sResult = trim(""&workStr)
		startLen = Len(sResult)
		endLen = 0
		While startLen > endLen
			startLen = len(sResult)
			sResult = sResult.Replace("  "," ")
			endLen = Len(sResult)
		End While
		clearSpaces = sResult
	End Function
	
	Sub ParsePage(ByRef pageXML As XmlDocument, ByVal nPage As Integer, ByRef sSQL As StringBuilder, ByVal UserID As Integer, ByRef sCurrency As String, ByRef sComment As String, ByRef paymentDate As Date, ByRef isCHB As Boolean, ByRef arrCHB As String(), ByRef arrData As String())
		Dim _text As XmlNodeList = pageXML.GetElementsByTagName("text")		
		Dim i As Integer								
		Response.Write("<br/><br/><br/>Page :" & nPage & "<br/>")
		For i = 0 To _text.Count			
			If Not _text(i) is Nothing then
				If Not _text(i).InnerText is Nothing then
					If _text(i).InnerText.ToString.ToLower.IndexOf("chargeback fee") > 0 then
						Continue For
					End If
					If _text(i).InnerText.ToString.ToLower.IndexOf("credit fee") > 0 then						
						Continue For
					End If					
					If Trim(_text(i).InnerText.ToLower).IndexOf("payment date") > 0 then
						paymentDate = date.Parse(right(Trim(_text(i).InnerText.ToLower),10))
						Response.Write("Payment Date: " & paymentDate.ToString)
						If nPage=1 then
							sSQL.Append("Delete From tblCompanyBankTransFeedback Where CBTF_DebitCompanyID=18 And CBTF_BankPayDate='" & paymentDate.ToString("dd/MM/yyyy") & "';")
						End If
					End If
					If Left(trim(_text(i).InnerText.ToLower),7)="paid-no" then
						Response.Write("<br/><br/>" & _text(i).InnerText.ToString & "<br/>")
						sComment = _text(i).InnerText.ToString
					End If
					Dim chbMatch As Match = System.Text.RegularExpressions.Regex.Match(Left(trim(""&_text(i).InnerText.ToString),8), "^\d{2}.00.00$")
					If chbMatch.Success then
					'If Left(trim(_text(i).InnerText),8)="81.00.00" OR Left(trim(_text(i).InnerText),8)="83.00.00" then
						isCHB = True
						sCurrency=left((right(trim(""&_text(i).InnerText.ToString),12)),3)
						arrCHB = clearSpaces(trim(""&_text(i).InnerText.ToString)).Split(" ")
						Continue For
					End If
					Dim dateMatch As Match = System.Text.RegularExpressions.Regex.Match(Left(trim(""&_text(i).InnerText.ToString),8), "^[0-3][0-9].\d{2}.\d{2}$")
					If dateMatch.Success then
						Dim dateMatchRev As Match = System.Text.RegularExpressions.Regex.Match(Right(trim(""&_text(i).InnerText.ToString),8), "^[0-3][0-9].\d{2}.\d{2}$")
						If dateMatchRev.Success then							
							sCurrency=left((right(trim(""&_text(i).InnerText.ToString),12)),3)
						Else
							If Not (isCHB) then
								arrData = clearSpaces(trim(""&_text(i).InnerText.ToString)).Split(" ")
								If ctype(arrData(2),Decimal) > 0 then
									sSQL.Append("Insert Into tblCompanyBankTransFeedback (CBTF_DebitCompanyID,CBTF_BankPayDate,CBTF_InsertDate,CBTF_GrossAmount,CBTF_Fee,CBTF_VAT,CBTF_NetAmount,CBTF_Currency_IZO,CBTF_ApprovalNumber,CBTF_IsCHB,CBTF_IsCredit,CBTF_UserID,CBTF_Comment) ")
									sSQL.Append("values(18,'" & paymentDate & "','" & date.Parse(arrData(0)).ToString("dd/MM/yyyy") & "'," & arrData(2) & "," & arrData(3) & "," & arrData(4) & "," & arrData(5) & ",'" & sCurrency & "','" & arrData(6) & "',0,0," & iif(UserID>0,UserID,"NULL") & ",'" & dbpages.DBText(clearSpaces(trim(""&sComment))) & "');")
									Response.Write(_text(i).InnerText.ToString & " " & sCurrency & "<br/>")
									Continue For
								Else	'credit, need to parse credit fee
									If i+1 < _text.Count then
										Dim arrCredit() As String
										arrCredit = clearSpaces(trim(""&_text(i+1).InnerText.ToString)).Split(" ")
										sSQL.Append("Insert Into tblCompanyBankTransFeedback (CBTF_DebitCompanyID,CBTF_BankPayDate,CBTF_InsertDate,CBTF_GrossAmount,CBTF_Fee,CBTF_VAT,CBTF_NetAmount,CBTF_Currency_IZO,CBTF_ApprovalNumber,CBTF_IsCHB,CBTF_IsCredit,CBTF_UserID,CBTF_Comment) ")
										sSQL.Append("values(18,'" & paymentDate & "','" & date.Parse(arrData(0)).ToString("dd/MM/yyyy") & "'," & arrData(2) & "," & arrCredit(4) & "," & arrData(4) & "," & ctype(arrData(5),Decimal)+ctype(arrCredit(4),Decimal) & ",'" & sCurrency & "','" & arrData(6) & "',0,1," & iif(UserID>0,UserID,"NULL") & ",'" & dbpages.DBText(clearSpaces(trim(""&sComment))) & "');")
										Response.Write (arrData(0) & " " & arrData(1) & " " & arrData(2) & " " & arrCredit(4) & " " & arrData(4) & " " & ctype(arrData(5),Decimal)+ctype(arrCredit(4),Decimal) & " " & arrData(6) & " " & arrData(7) & " " & sCurrency & " -Credit<br/>")
										Continue For
									End If
								End If
									
							Else
								isCHB = False								
								arrData = clearSpaces(trim(""&_text(i).InnerText.ToString)).Split(" ")
								sSQL.Append("Insert Into tblCompanyBankTransFeedback (CBTF_DebitCompanyID,CBTF_BankPayDate,CBTF_InsertDate,CBTF_GrossAmount,CBTF_Fee,CBTF_VAT,CBTF_NetAmount,CBTF_Currency_IZO,CBTF_ApprovalNumber,CBTF_IsCHB,CBTF_IsCredit,CBTF_UserID,CBTF_Comment) ")
								sSQL.Append("values(18,'" & paymentDate & "','" & date.Parse(arrData(0)).ToString("dd/MM/yyyy") & "'," & arrData(2) & "," & arrCHB(5) & "," & arrData(4) & "," & arrCHB(7) & ",'" & sCurrency & "','" & arrData(6) & "',1,0," & iif(UserID>0,UserID,"NULL") & ",'" & dbpages.DBText(clearSpaces(trim(""&sComment))) & "');")
								Response.Write (arrData(0) & " " & arrData(1) & " " & arrData(2) & " " & arrCHB(5) & " " & arrData(4) & " " & arrCHB(7) & " " & arrData(6) & " " & arrData(7) & " " & sCurrency & " -Chargeback<br/>")
								Continue For
							End If
						End If
					End If
				End If
			End If
		Next
	End Sub
	

	
	Sub Page_Load()		
		Security.CheckPermission(lblPermissions)
		'dsEPA.ConnectionString = dbPages.DSN
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script type="text/javascript" language="JavaScript" src="../js/func_common.js"></script>

</head>
<body>
	<form id="Form1" runat="server">
	<table align="center" style="width:95%;">
	<tr>
		<td id="pageMainHeading">
			<asp:label ID="lblPermissions" runat="server" /> B+S PDF File Import <br />
		</td>
	</tr>
	</table>
	<br />
	<div style="text-align:center;">
		<fieldset style="width:95%;padding:10px;text-align:left;">
			<legend>Upload New File</legend>
			<asp:FileUpload ID="fuPDF" ToolTip="Upload PDF file here" runat="server" />
			<asp:Button ID="Button1" CssClass="buttonWhite" OnClick="UploadFile" Text="Upload!" runat="server" />
			<span style="font-size:11px;"><asp:literal ID="litFileInfo" runat="server" /></span>
			<div class="errorMessage"><asp:literal ID="litError" runat="server" /></div>
		</fieldset>

	</div>
	</form>
</body>
</html>