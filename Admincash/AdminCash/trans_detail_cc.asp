<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/MerchantTerminals.asp" -->
<!--#include file="../member/remoteCharge_Functions.asp" -->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
    'response.Redirect("TransactionDetails.aspx?transactionStatus=Captured&transactionID=" & Request("transID"))
	'response.End
	
	Dim ClientEmailAddress
	Set rsData=server.createobject("adodb.recordset")
	GetDescConstArray rsData, GGROUP_DeniedStatus, 1, "", DeniedStatusDesc, VbNull
	sTransID = Trim(Request("transID"))

	'20110524 Tamir - allow manual marking of transactions as paid/refunded by bank (EPA)
	'20110525 Tamir - allow manual marking of transactions as NOT paid/refunded by bank (EPA)
	sMarkEpa = Trim(Request("MarkEPA"))
	If sMarkEpa = "Paid" Or sMarkEpa = "Refunded" Or sMarkEpa = "NotPaid" Or sMarkEpa = "NotRefunded" Then
		If Left(sMarkEpa, 3) = "Not" Then
			sMarkEpa = Mid(sMarkEpa, 4)
			sSQL = "EXEC LogClearTransactionEPA " & sTransID & ", 1, " & IIf(sMarkEpa="Paid", "1, 0", "0, 1") & ", '" & PageSecurity.Username & "';"
		Else
			sSQL = "EXEC LogWriteTransactionEPA " & sTransID & ", 1, " & IIf(sMarkEpa="Paid", "1, 0", "0, 1") & ", '" & PageSecurity.Username & "';"
		End If
		oledbData.Execute sSQL
		response.redirect Request.ServerVariables("PATH_INFO") & "?" & Replace(Request.QueryString, "MarkEPA=" & Request("MarkEPA") & "&", "")
	End If

	sPageBkgColor = Trim(Request("bkgColor"))
	If sPageBkgColor = "" Then sPageBkgColor = "ffffff"
	sIsShowBasicTransData = Trim(Request("isShowBasicTransData"))
	If sIsShowBasicTransData = "" Then sIsShowBasicTransData = False
	bIsShowRiskMngData = Trim(Request("IsShowRiskMngData"))
	If bIsShowRiskMngData = "" Then bIsShowRiskMngData = True

	sPaymentMethod = Trim(Request("PaymentMethod"))
	If sPaymentMethod - PMD_CC_MIN >=0 And sPaymentMethod - PMD_CC_MAX <= 0 Then sPaymentMethod = 1
	'If sPaymentMethod <> 1 Then Response.End
	sBtnBarStyle = IIf(sPageBkgColor<>"ffffff","background-color:#ffffff; border:solid 1px #d2d2d2;","background-color:#f1efe2;")
	sTransTableType = LCase(Trim(Request("TransTableType")))
	If sTransTableType = "preauth" Then
		sIsAllowAdmin = False
		sTransTableName = "tblCompanyTransApproval"
	Elseif sTransTableType = "fail" Then
		sIsAllowAdmin = False
		sTransTableName = "tblCompanyTransFail"
	Elseif sTransTableType = "pass" Then
		sTransTableName = "tblCompanyTransPass"
		sIsAllowAdmin = Trim(Request("isAllowAdministration"))
		If sIsAllowAdmin = "" Then sIsAllowAdmin = True
	Elseif sTransTableType = "pending" Then
		sTransTableName = "tblCompanyTransPending"
		sIsAllowAdmin = False
	Else
		Response.end
	End if
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<style type="text/css">
		.MainHeadings { font-weight:bold; font-size:10px; color:#505050; }
		.SmallHeadings { font-size:11px; color:#006699; }
		.Element { white-space: nowrap; }

		.TransData Table { padding:1px; margin:0px; border:0px; }
		.TransData th { padding-right:10px; font-weight:normal; text-align:left; font-size:11px; color:#006699; }
		.TransData td { padding-right:20px; font-weight:normal; font-size:11px; }

		.subTable Table { padding:0px; margin:0px; border:0px; }
		.subTable th { padding-right:16px; font-weight:normal; text-align:left; border-bottom:solid 1px silver; }
		.subTable td { padding-right:16px; font-weight:normal; }
	</style>
	<script language="JavaScript" src="../js/func_common.js"></script>
</head>
<body leftmargin="5" topmargin="0" rightmargin="15" class="itext" bgcolor="#<%= sPageBkgColor %>">
<script language="javascript" type="text/javascript">
	function OpenPop(sURL,sName,sWidth,sHeight,sScroll)
	{
		var placeLeft = screen.width/2-sWidth/2
		var placeTop = screen.height/2-sHeight/2
		var winPrint=window.open(sURL,sName,'TOP='+placeTop+',LEFT='+placeLeft+',WIDTH='+sWidth+',HEIGHT='+sHeight+',status=1,resizable=1,scrollbars='+sScroll);
	}	
	function AddToBlacklist(sCardNumber, nCardMonth, nCardYear)
	{
		var sURL="blacklist.aspx?id=0";
		sURL+="&CardNumber="+sCardNumber;
		sURL+="&CardMonth="+nCardMonth;
		sURL+="&CardYear="+nCardYear;
		OpenPop(sURL,'winBlacklist'+<%= sTransID %>,700,400,1);
	}
</script>
<%
sQuery = "transID=" & sTransID & "&bkgColor=" & sPageBkgColor & "&isAllowAdministration=" & sIsAllowAdmin & "&PaymentMethod=" & sPaymentMethod & "&TransTableType=" & sTransTableType

'--------------------------------------------	---------------------------
'	Execute admin actions if allowed
'-----------------------------------------------------------------------
If sIsAllowAdmin Then
	If Trim(Request("Action"))="updDeniedComment" Then
		Set iRs = Server.CreateObject("Adodb.Recordset")
		iRs.Open "Select DeniedDate, PaymentMethod From tblCompanyTransPass Where ID=" & sTransID, oleDbData, 0, 1
		If Not iRs.EOF Then
			xValue = iRs("DeniedDate")
			nPaymentMethod = iRs("PaymentMethod")
		Else
			nPaymentMethod = 0	
		End If
		iRs.Close()
		Set iRs = Nothing
		If IsNull(xValue) Then xValue = Now
		If IsDate(Request("DeniedDate")) Then 
		'	'If xValue > CDate(Request("DeniedDate")) Then xValue = Request("DeniedDate")
			xValue = CDate(Request("DeniedDate"))
		End If	
		sSQL="UPDATE " & sTransTableName & " SET DeniedAdminComment='" & DBText(Request("deniedComment")) & "'" & _
			",DeniedDate='" & xValue & "', DeniedValDate='" & IIF(nPaymentMethod=25, DateAdd("m", 1, xValue), xValue) & "' WHERE id=" & sTransID
		oledbData.Execute sSQL
	Elseif Trim(Request("Action")) = "updDeniedStatus" Then
		nTransID=TestNumVar(Request("transID"), 1, 0, 0)
		If Request("status") = 0 Then 
			If ClearChargeBack(nTransID) Then
				Response.Write("The chargeback has been cancelled successfully.")
				ExecSQL "DELETE FROM Trans.TransHistory WHERE TransHistoryType_id IN (4, 6) AND TransPass_id=" & nTransID
			Else
				Response.Write("The chargeback cannot be cancelled!")
			End If
		Else
			nReasonCode=TestNumVar(Request("ReasonCode"), 1, 0, 0)
			sSQL="SELECT Brand+': '+Title FROM tblChargebackReason WHERE ReasonCode=" & nReasonCode
			sDescription=ExecScalar(sSQL, "")
			If Request("status") = 3 Then
				If PhotocopyTransDB(nTransID, sDescription, nReasonCode) > 0 Then 
				'If ChargeBackTrans(0, True, "tblCompanyTransPass.ID=" & nTransID & " And DeniedStatus=0") Then
				'	ExecSQL "INSERT INTO Trans.TransHistory (Merchant_id, TransHistoryType_id, TransPass_id, ReferenceNumber, Description) SELECT CompanyID, 6, ID, " & nReasonCode & ", '" & Replace(sDescription, "'", "''") & "' FROM tblCompanyTransPass WHERE ID=" & nTransID & ";"
					Response.Write("The photocopy has been created successfully.")
				Else
					Response.Write("The photocopy cannot be created - the transaction is already charged back!")
				End If
			Else
				nTransID = TestNumVar(Request("transID"), 0, -1, 0)
				'20100401 Tamir - protect from double chargeback on page refreshing
				sSQL = "IF EXISTS (SELECT ID FROM tblCompanyTransPass WITH (NOLOCK) WHERE dbo.IsCHB(DeniedStatus, 0)=1 AND " & nTransID & " IN (ID, OriginalTransID)) SELECT 1 ELSE SELECT 0"
				If ExecScalar(sSQL, 0) = 0 Then
					If ChargeBackTransDB(nTransID, sDescription, nReasonCode) > 0 Then 
					'If ChargeBackTrans(0, False, "tblCompanyTransPass.ID=" & nTransID) Then
					'	ExecSQL "INSERT INTO Trans.TransHistory (Merchant_id, TransHistoryType_id, TransPass_id, ReferenceNumber, Description) SELECT CompanyID, 4, ID, " & nReasonCode & ", '" & Replace(sDescription, "'", "''") & "' FROM tblCompanyTransPass WHERE ID=" & nTransID & ";"
						Response.Write("The chargeback has been created successfully.")
					Else
						Response.Write("The chargeback cannot be created - the transaction is already charged back!")
					End If
				Else
					Response.Write("The chargeback cannot be created - the transaction is already charged back!")
				End If
			End if		
		End if		
    ElseIf(Request("SetFraud") <> "") Then 
        SetTransFraud sTransID, Request("SetFraud") = "1"
	End If
End If
'-----------------------------------------------------------------------
'	Retrieve transaction data
'-----------------------------------------------------------------------
sSQL="SELECT " & sTransTableName & ".*, pm.Name AS 'ccTypeName', List.TransSource.Name TransTypeName, dc_name DebitCompanyName," & _
" dt_name, tblDebitTerminals.id terminalID, IsNull(dt_ContractNumber, '') CN,"
If sTransTableName = "tblCompanyTransPass" then
	sSQL = sSQL & " dbo.IsCHB(DeniedStatus, 0) IsChargeback "
Else
	sSQL = sSQL & " 0 IsChargeback "
End If
sSQL = sSQL & " FROM " & sTransTableName & _
" LEFT JOIN tblDebitCompany ON " & sTransTableName & ".debitCompanyId=tblDebitCompany.debitCompany_id" & _
" LEFT JOIN tblDebitCompanyCode ON " & IIF(sTransTableName = "tblCompanyTransPass", "'000'", sTransTableName & ".replyCode") & "=tblDebitCompanyCode.Code" & _
" LEFT JOIN tblDebitTerminals ON " & sTransTableName & ".TerminalNumber = tblDebitTerminals.terminalNumber" & _
" LEFT JOIN List.PaymentMethod AS pm ON " & sTransTableName & ".PaymentMethod = pm.PaymentMethod_id" & _
" LEFT JOIN List.TransSource ON " & sTransTableName & ".TransSource_id=List.TransSource.TransSource_id" & _
" WHERE " & sTransTableName & ".ID=" & sTransID
set rsData=oledbData.execute(sSQL)
If rsData.EOF Then
	rsData.Close() : closeConnection()
	Response.Write("<div style=""width:100%;height:20px;text-align:center;font-weight:bold;"">Requested transaction no longer exist</div></body></html>")
	response.End
End if
CR_Symbol = GetCurText(GC_NIS)
sSQL="SELECT dbo.GetDecrypted256(tblCreditCard.CCard_number256) CCard_number, dbo.fnFormatCcNumToHideDigits(dbo.GetDecrypted256(tblCreditCard.CCard_number256)) CardNumberProtected, tblCreditCard.ExpYY, tblCreditCard.ExpMM, tblCreditCard.BillingAddressId, " &_
"dbo.GetCUI(tblCreditCard.cc_cui) CUI, tblCreditCard.ccTypeID, tblCreditCard.Member, tblCreditCard.PersonalNumber, tblCreditCard.phoneNumber, tblCreditCard.email, cl.Name AS BinCountryName " &_
"FROM tblCreditCard WITH (NOLOCK) LEFT OUTER JOIN List.CountryList AS cl WITH (NOLOCK) ON tblCreditCard.BINCountry = cl.CountryISOCode " &_
"WHERE tblCreditCard.ID=" & rsData("PaymentMethodID")
set rsData2 = oledbData.execute(sSQL)
isCardDetails = False
If NOT rsData2.EOF Then 
    isCardDetails = True
	BillingAddressId = rsData2("BillingAddressId")
    ClientEmailAddress = rsData2("email")
End If

%>
<table border="0" cellspacing="0" cellpadding="1" width="100%" height="100%" style="border-top:1px dashed #e2e2e2;">
<tr><td height="15"></td></tr>
<%
If Trim(sMsgTxt)<>"" Then Response.Write("<tr><td colspan=""2"" style=""color:red; padding-bottom:6px;"">" & sMsgTxt & "</td></tr>")

'-----------------------------------------------------------------------
'	Show fail code and description if needed
'-----------------------------------------------------------------------
If sTransTableType = "fail" Then
	If rsData("debitCompanyId")<>"" AND rsData("replyCode")<>"" Then
		Dim errorDescription
		If rsData("DebitDeclineReason") <> "" Then
			sDescription = "&nbsp;-&nbsp;" & rsData("DebitDeclineReason")
		Else
			nDebitCompanyId = rsData("debitCompanyId")
			sDescription = "&nbsp;- No Details"
			sSQL="SELECT TOP 1 tblDebitCompanyCode.DescriptionOriginal FROM tblDebitCompanyCode WHERE" & _
			" tblDebitCompanyCode.DebitCompanyID=" & nDebitCompanyId & " AND tblDebitCompanyCode.Code='" & rsData("replyCode") & "'"
			sDescription =  "&nbsp;-&nbsp;" & ExecScalar(sSQL, "")
		End If
		%>
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td class="txt11b" style="color:#505050;" valign="top" width="100">
						Failed reason<br />
					</td>
					<td valign="top" style="color:maroon;">
						<%
						response.write "<span class=""txt11"" style=""color:maroon;"">" & rsData("replyCode") & "</span>" & dbTextShow(sDescription)
						%>
					</td>
				</tr>
				<tr><td height="12"></td></tr>
				</table>
			</td>
		</tr>
		<%
	End If
End If
%>
<tr>
	<td height="100%" valign="top">
		<table cellpadding="0" cellspacing="0">
		<tr>
			<%
			If sIsShowBasicTransData Then
				%>
				<td valign="top" style="padding-right:12px;">
					<span class="MainHeadings">Transaction Info</span><br />
					<%
					CR_Symbol = GetCurText(nCurrency)
					If Trim(rsData("id"))<>"" then response.write "<span class=""SmallHeadings"">Trans #:</span> " & rsData("id") & "<br />"
					If Trim(rsData("InsertDate"))<>"" then response.write "<span class=""SmallHeadings"">Date:</span> " & rsData("InsertDate") & "<br />"
					If Trim(rsData("PaymentMethodDisplay"))<>"" then response.write "<span class=""SmallHeadings"">Payment Method:</span> " & rsData("PaymentMethodDisplay") & "<br />"
					Response.write("<span class=""SmallHeadings"">Amount:</span>  <span class=""txt11"" dir=""ltr"">" & FormatCurrWCT(rsData("Currency"), rsData("Amount"), rsData("CreditType"))) & "</span><br />"
					If Trim(rsData("Payments"))<>"" then response.write "<span class=""SmallHeadings"">Installments:</span> " & rsData("Payments") & "<br />"
					If Trim(rsData("TransTypeName"))<>"" then response.write "<span class=""SmallHeadings"">Referring Page:</span> " & rsData("TransTypeName") & "<br />"
					%>
				</td>
				<%
			End if
			%>
			<td valign="top" style="padding-right:12px;">
				<span class="MainHeadings">TRANSACTION INFO</span><br />
				<table class="TransData">
				<%
					If sTransTableType = "pass" Then
						if Trim(rsData("ApprovalNumber"))<>"" AND Trim(rsData("ApprovalNumber"))<>"0000000" then
							response.write "<tr><th>Authorization Code:</th><td>" & rsData("ApprovalNumber") & "</td></tr>"
						end if
					End if
					If Trim(rsData("comment"))<>"" then
						response.write "<tr><th>Payer Comment:</th><td>"
						If int(len(dbtextShow(Trim(rsData("comment")))))<20 Then
							response.write dbTextShow(Trim(rsData("comment"))) & "<br />"
						Else
							response.write "<span style=""text-decoration:underline;"" title="""& dbtextShow(Trim(rsData("comment"))) & """>" & left(dbtextShow(Trim(rsData("comment"))),20) & "..</span><br />"
						End If
					End if
					If Trim(rsData("IPAddress"))<>"" then response.write "<tr><th>IP Address:</th><td>" & rsData("IPAddress") & "</td></tr>"
					if sTransTableType <> "preauth" then
						If Trim(rsData("payerIdUsed"))<>"" then response.write "<tr><th>Payer ID:</th><td>" & rsData("payerIdUsed") & "</td></tr>"
					End if
					If Trim(rsData("DebitReferenceCode"))<>"" then response.write "<tr><th>Debit Reference:</th><td>" & rsData("DebitReferenceCode") & "</td></tr>"
					If Trim(rsData("DebitReferenceNum"))<>"" then response.write "<tr><th>Debit Reference #:</th><td>" & rsData("DebitReferenceNum") & "</td></tr>"
					If sTransTableName = "tblCompanyTransPass" Then
                        If Trim(rsData("AcquirerReferenceNum"))<>"" then response.write "<tr><th>Acquirer Reference #:</th><td>" & rsData("AcquirerReferenceNum") & "</td></tr>"
                    End If
					if Trim(rsData("OrderNumber"))<>"" then response.write "<tr><th>Order Number:</th><td>" & rsData("OrderNumber") & "</span></span><br />"
					if Trim(rsData("DebitCompanyName"))<>"" then response.write "<tr><th>Debit Company:</th><td>" & dbTextShow(rsData("DebitCompanyName")) & "</td></tr>"
					if Trim(rsData("TerminalNumber"))<>"" then
						response.write "<tr><th>Terminal:</th><td><a href=""#"" onclick=""OpenPop('system_terminalTermDetail.aspx?terminalID=" & rsData("terminalID") & "','fraOrderPrint',document.body.clientWidth,600,1);"" class=""submenu2"" style=""font-size:10px; text-decoration:underline;"">" & dbTextShow(rsData("dt_name")) & "</a><br />"
					End If
					If sTransTableType = "pass" Then
						If Trim(rsData("PD"))<>"01/01/1900" Then Response.write "<tr><th>PD Date:</th><td>" & rsData("PD") & "<br />"
						Response.write "<tr><th>Costs:</th><td>" & FormatCurr(rsData("Currency"), rsData("DebitFee")) & "</span>"
						If rsData("DebitFeeChb") <> 0 Then Response.write "&nbsp;&nbsp;&nbsp;<tr><th>Chb Costs:</th><td>" & FormatCurr(rsData("Currency"), rsData("DebitFeeChb")) & "</td></tr>"
						Response.write "<tr><th>Min Pay Date:</th><td>" & Left(rsData("MerchantPD"), 10) & "</td></tr>"
					end if
				%>
				</table>
			</td>
			<%
			If Trim(sTransTableType) <> "pending" Then
				%>
				<td valign="top" style="padding-right:12px;">
					<span class="MainHeadings">FEE INFO</span><br />
					<table class="TransData">
					<%
						If Trim(sTransTableType) = "pass" Then
							If rsData("amount") <> 0 And Not IsNull(rsData("NetpayFee_RatioCharge")) Then nRatioChargePercentage = FormatNumber((rsData("NetpayFee_RatioCharge")/rsData("amount"))*100 ,1,-1)
							response.write "<tr><th>Trans:</th><td>" & FormatCurr(rsData("Currency"), rsData("netpayFee_transactionCharge")) & "</td></tr>"
							response.write "<tr><th>Clearing:</th><td>(&plusmn;" & nRatioChargePercentage & "%) " & FormatCurr(rsData("Currency"), rsData("NetpayFee_RatioCharge")) & "</td></tr>"
							Response.write "<tr><th>Copy R:</th><td>" & FormatCurr(rsData("Currency"), rsData("netpayFee_ClrfCharge")) & "</td></tr>"
							Response.write "<tr><th>Chb:</th><td>" & FormatCurr(rsData("Currency"), rsData("netpayFee_chbCharge")) & "</td></tr>"
							Response.write "<tr><th>Handling:</th><td>" & FormatCurr(rsData("Currency"), rsData("HandlingFee")) & "</td></tr>"
						ElseIf Trim(sTransTableType) = "preauth" Then
							response.write "<tr><th>Trans:</th><td>" & FormatCurr(rsData("Currency"), rsData("netpayFee_transactionCharge")) & "</td></tr>"
						ElseIf Trim(sTransTableType) = "fail" Then
							response.write "<tr><th>Trans:</th><td>" & FormatCurr(rsData("Currency"), rsData("netpayFee_transactionCharge")) & "</td></tr>"
						End If
					%>
					</table>
				</td>
				<%
			End if

			'-----------------------------------------------------------------------
			'	Credit card data
			'-----------------------------------------------------------------------
			If isCardDetails Then
				if Trim(Request("Action"))="Block" then
					sSQL="INSERT INTO tblFraudCcBlackList(company_id, PaymentMethod_id, fcbl_ccNumber256, fcbl_ccDisplay, fcbl_comment) VALUES " &_
						"(" & rsData("companyID") & ", " & rsData2("ccTypeID") + 20 & ", dbo.GetEncrypted256('" & Replace(rsData2("CCard_number")," " ,"") & "'), '" & rsData2("CardNumberProtected") & "', '')"
					oledbData.execute(sSQL)
				End if
				%>
				<td valign="top" style="padding-right:12px;">
					<span class="MainHeadings">CREDIT CARD INFO</span><br />
					<table class="TransData">
					<%
						If Trim(rsData2("CardNumberProtected"))<>"" then
							sCCardNum = Replace(rsData2("CardNumberProtected")," ","")
							response.write "<tr><th>Number:</th><td>" & CStr(sCCardNum) & "</td></tr>"
						End if
						If Trim(rsData2("ExpMM"))<>"" then response.write "<tr><th>Exp Date:</th><td>" & Trim(rsData2("ExpMM")) & "/" & Trim(rsData2("ExpYY")) & "</td></tr>"
						If Trim(rsData("ccTypeName"))<>"" then response.write "<tr><th>Type:</th><td>" & dbTextShow(rsData("ccTypeName")) & "</td></tr>"
						If False then response.write "<span class=""Element""><tr><th>Cvv:</th><td>" & DecCVV(rsData2("cc_cui")) & "</span></span><br />" '
						If Trim(rsData2("BinCountryName"))<>"" then response.write "<tr><th>Country:</th><td>" & dbTextShow(rsData2("BinCountryName")) & "</td></tr>"
						if PageSecurity.IsAdmin then response.write "<tr><th>CUI:</th><td>" & dbTextShow(rsData2("CUI")) & "</td></tr>"
						If rsData("DebitCompanyID") = 20 Then
							nSite = ExecScalar("Select accountSubId From tblDebitTerminals Where DebitCompany=20 And terminalNumber='" & rsData("TerminalNumber") & "'", "")
							Response.Write "<tr><th>PPC Admin:</th><td><a href=""" & nSite & "/AdminPay/CardBalance.aspx?CrdNum=" & CStr(sCCardNum) & """ target=""PPC"">GO</a></td></tr>"
						End if
					%>
					</table>
				</td>
				<td valign="top" style="padding-right:12px;">
					<span class="MainHeadings">CARD HOLDER'S INFO</span><br />
					<table class="TransData">
					<%
						If Trim(rsData2("Member"))<>"" then response.write "<tr><th>Name:</th><td>" & dbTextShow(rsData2("Member")) & "</td></tr>"
						If Trim(rsData2("PersonalNumber"))<>"" then response.write "<tr><th>ID:</th><td>" & rsData2("PersonalNumber") & "</td></tr>"
						If Trim(rsData2("PhoneNumber"))<>"" then response.write "<tr><th>Phone:</th><td>" & rsData2("PhoneNumber") & "</td></tr>"
						If Trim(rsData2("Email"))<>"" then response.write "<tr><th>Email:</th><td>" & rsData2("Email") & "</td></tr>"
					%>
					</table>
				</td>
				<%
			End if

			'-----------------------------------------------------------------------
			'	Data from Billing
			'-----------------------------------------------------------------------
			If BillingAddressId > 0 Then
				sSQL="SELECT ba.address1, ba.address2, ba.city, ba.zipCode, sl.Name AS stateName, cl.Name AS countryName " &_
				"FROM tblBillingAddress AS ba LEFT OUTER JOIN [List].[CountryList] AS cl ON ba.countryId = cl.CountryID LEFT OUTER JOIN [List].[StateList] AS sl ON ba.stateId = sl.stateID WHERE ba.id=" & BillingAddressId
				set rsData3=oledbData.execute(sSQL)
				If not rsData3.EOF then
					%>
					<td valign="top" style="padding-right:12px;">
						<span class="MainHeadings">BILLING ADDRESS</span><br />
						<table class="TransData">
						<%
							If Trim(rsData3("address1"))<>"" then response.write "<tr><th>Address 1:</th><td>" & dbTextShow(rsData3("address1")) & "</td></tr>"
							if Trim(rsData3("address2"))<>"" then response.write "<tr><th>Address 2:</th><td>" & dbTextShow(rsData3("address2")) & "</td></tr>"
							If Trim(rsData3("city"))<>"" then response.write "<tr><th>City:</th><td>" & rsData3("city") & "</td></tr>"
							If Trim(rsData3("zipCode"))<>"" then response.write "<tr><th>Zip Code:</th><td>" & rsData3("zipCode") & "</td></tr>"
							If Trim(rsData3("stateName"))<>"" then response.write "<tr><th>State:</th><td>" & dbTextShow(rsData3("stateName")) & "</td></tr>"
							If Trim(rsData3("countryName"))<>"" then response.write "<tr><th>Country:</th><td>" & dbTextShow(rsData3("countryName")) & "</td></tr>"
						%>
						</table>
					</td>
					<%
				end if
				rsData3.close
				set rsData3 = nothing
			End if
			
			'-----------------------------------------------------------------------
			'	Data from fraud Detection
			'-----------------------------------------------------------------------
			If bIsShowRiskMngData AND rsData("FraudDetectionLog_id") > 0 Then
				sSQL="SELECT ReturnScore, ReturnRiskScore, IsTemperScore, allowedScore, allowedRiskScore, ReferenceCode " &_
				"FROM Log.FraudDetection WHERE FraudDetection_id = " & rsData("FraudDetectionLog_id")
				set rsData3 = oledbData.execute(sSQL)
				If not rsData3.EOF then
					%>
					<td valign="top" style="padding-right:12px;">
						<span class="MainHeadings">Fraud Mng</span><br />
						<%
						response.write "<tr><th>Score Received:</th><td>"
						response.write FormatNumber(rsData3("ReturnScore"),2,-1,0,0)
						If rsData3("IsTemperScore") Then Response.Write("*")
						response.write " / " & FormatNumber(rsData3("ReturnRiskScore"),2,-1,0,0) & "</td></tr>"
						response.write "<tr><th>Score Allowed:</th><td><a href=""#"" onclick=""OpenPop('merchant_dataRiskMng.asp?CompanyID=" & rsData("CompanyID") & "','fraMerchantRisk" & rsData("CompanyID") & "',document.body.clientWidth,500,1);"" class=""submenu2"" style=""font-size:10px; text-decoration:underline;"">(" & FormatNumber(TestNumVar(rsData3("allowedScore"), 0, -1, 0), 2, -1, 0, 0) & " / " & FormatNumber(TestNumVar(rsData3("allowedRiskScore"), 0, -1, 0), 2, -1, 0, 0) & ")</a></td></tr>"
						response.write "<tr><th>Reference Code:</th><td><a href=""#"" onclick=""OpenPop('Log_FraudDetection_data.aspx?ID=" & rsData("FraudDetectionLog_id") & "','fraFraud" & rsData("FraudDetectionLog_id") & "',document.body.clientWidth,500,1);"" class=""submenu2"" style=""font-size:10px; text-decoration:underline;"">" & rsData3("ReferenceCode") & "</a></td></tr>"
						%>
					</td>
					<%
				end if
				rsData3.close
				set rsData3 = nothing
			End if

			'-----------------------------------------------------------------------
			'	Data from public pay page
			'-----------------------------------------------------------------------
			'sSQL="SELECT * FROM tblPublicPayCostumerData WHERE transLocation='" & sTransTableType & "' AND transID=" & sTransID
			'set rsData3=oledbData.execute(sSQL)
			'If not rsData3.EOF Then
				%>
				<td valign="top" style="padding-right:12px;">
					<span class="MainHeadings">Payer Info</span><br />
					<%
			'		If Trim(rsData3("fullName"))<>"" then response.write "<tr><th>Full Name:</th><td>" & dbTextShow(rsData3("fullName")) & "</td></tr>"
			'		if Trim(rsData3("address"))<>"" then response.write "<tr><th>Address:</th><td>" & dbTextShow(rsData3("address")) & "</td></tr>"
			'		If Trim(rsData3("phone"))<>"" then response.write "<tr><th>Phone:</th><td>" & rsData3("phone") & "</td></tr>"
			'		If Trim(rsData3("cellular"))<>"" then response.write "<tr><th>Cellular:</th><td>" & rsData3("cellular") & "</td></tr>"
			'		If Trim(rsData3("email"))<>"" then response.write "<tr><th>Email:</th><td>" & dbTextShow(rsData3("email")) & "</td></tr>"
			'		If Trim(rsData3("HowGetHere"))<>"" then response.write "<tr><th>How Arrived:</th><td>" & dbTextShow(rsData3("HowGetHere")) & "</td></tr>"
			'		for i=0 to 3
			'			if Trim(rsData3("Var" & i+1 & "_data"))<>"" then response.write "<tr><th>" & dbTextShow(rsData3("Var" & i+1 & "_name")) & ":</th><td>" & dbTextShow(rsData3("Var" & i+1 & "_data")) & "</td></tr>"
			'		next
					%>
				</td>
				<%
			'End if
			'rsData3.close
			'Set rsData3 = Nothing
			%>
		</tr>
		</table>
		<br />
	</td>
</tr>
<%
If sTransTableName = "tblCompanyTransPass" Then
	If rsData("CreditType") > 0 Then
		set rsData3 = oledbData.Execute("SELECT ID FROM tblCompanyTransPass WHERE CreditType=0 AND IsChargeback=0 AND DeniedStatus<>5 AND OriginalTransID=" & rsData("ID"))
		if not rsData3.EOF then
			%>
			<tr>
				<td class="txt13" style="color:#bc3a4f;">
					Transaction was refunded - ID:
					<%
					do until rsData3.EOF
						response.Write "<a href=""Trans_admin_regularData.aspx?transID=" & rsData3("ID") & """ target=""_parent"" class=""link"">" & rsData3("ID") & "</a>"
						rsData3.MoveNext
						if not rsData3.EOF then response.Write ", "
					loop
					%>
				</td>
			</tr>
			<%
		end if
		rsData3.Close
	ElseIf TestNumVar(rsData("OriginalTransID"), 1, rsData("ID"), 0) > 0 And Not rsData("IsChargeback") Then
		%>
			<tr>
				<td class="txt13" style="color:#bc3a4f;">
					This is a refund of transaction
					<a href="Trans_admin_regularData.aspx?transID=<%= rsData("OriginalTransID") %>" target="_parent" class="link"><%= rsData("OriginalTransID") %></a>
				</td>
			</tr>
		<%
	End if
	If rsData("DeniedStatus") = DNS_SetFoundValid Or rsData("deniedstatus") = DNS_SetFoundValidRefunded Then
		%>
		<tr>
			<td class="txt13" style="color:#505050; padding-bottom:5px;">
				<%
				Response.write("<b>" & DeniedStatusDesc(rsData("deniedstatus")) & "</b>")
				%>
			</td>
		</tr>
		<%
	ElseIf rsData("DeniedStatus") = DNS_DupFoundValid Or rsData("DeniedStatus") = DNS_DupFoundUnValid Then
		%>
		<tr>
			<td class="txt13" style="color:#505050; padding-bottom:5px;">
				<%
				Response.write("<b>" & DeniedStatusDesc(rsData("deniedstatus")) & "</b>")
				if rsData("DeniedSendDate")<rsData("DeniedDate") then response.Write " - mail not sent"
				if rsData("DeniedAdminComment")="" then response.Write " - comment not set"
				%>
			</td>
		</tr>
		<%
	ElseIf rsData("IsChargeback") And rsData("OriginalTransID") = 0 Then
		%>
		<tr>
			<td class="txt13" style="color:#505050; padding-bottom:5px;">
				<%
				Response.write("<b>" & DeniedStatusDesc(rsData("deniedstatus")) & "</b>")
				%>
			</td>
		</tr>
		<%
	ElseIf Int(rsData("CreditType")) <> 0 And rsData("DeniedStatus") >=10 Then
		set rsData3 = oledbData.Execute("Select ID From tblCompanyTransPass Where DeniedStatus <> 0 And OriginalTransID=" & rsData("ID"))
		if not rsData3.EOF then
			%>
			<tr>
				<td class="txt13" style="color:#505050; padding-bottom:5px;">
					<%
					Response.write("<b>" & DeniedStatusDesc(rsData("DeniedStatus")) & "</b>")
					Response.Write(" &nbsp; Trans Duplicated: ")
					do until rsData3.EOF
						response.Write "<a href=""Trans_admin_regularData.aspx?transID=" & rsData3("ID") & """ target=""_parent"" class=""link"">" & rsData3("ID") & "</a>"
						rsData3.MoveNext
						if not rsData3.EOF then response.Write ", "
					loop
					%>
				</td>
			</tr>
			<%
		end if
		rsData3.Close
	End if

	If rsData("CreditType") = 8 then ' ���� ���� ��������
		sSQL="SELECT tblCompanyTransInstallments.*, tblLogImportEpa.IsPaid FROM tblCompanyTransInstallments " & _
		    " Left Join tblLogImportEpa ON (tblCompanyTransInstallments.transAnsID = tblLogImportEpa.TransID And tblCompanyTransInstallments.InsID = tblLogImportEpa.Installment) " & _
		    " WHERE transAnsID=" & rsData("id") & " ORDER BY id"
		set rsDataPayments=oledbData.execute(sSQL)
		If not rsDataPayments.EOF Then
			nCountPayments=0
			sFirstPayment = false
			%>
			<tr>
				<td id="Row<%= rsData("ID") %>A" bgcolor="<%= sTrBkgColor %>">
					<table cellspacing="0" cellpadding="0" border="0">
					<%
					Do until rsDataPayments.EOF
						nCountPayments=nCountPayments+1
						sCheckboxTxt = ""
						if bIsSelectCheckbox then
							If nCountPayments=1 then sCheckboxTxt = "checked"
						end if
						if nCountPayments=1 AND left(rsDataPayments("comment"),2)="1/" then sFirstPayment = true
						%>
						<tr>
							<td>Installment <%= rsDataPayments("comment") %></td>
							<td width="20"></td>
							<td>Amount: <%=FormatCurr(nCurrency, rsDataPayments("Amount"))%></td>
							<td width="20"></td>
							<td>Min Pay Date: <%=Left(rsDataPayments("MerchantPD"), 10)%></td>
							<td width="20"></td>
							<td>Bank Payed: <%=IIF(rsDataPayments("IsPaid"), "Yes", "No")%></td>
							<td width="20"></td>
							<td>Settlement: <%=rsDataPayments("PayId")%></td>
						</tr>
						<%
						rsDataPayments.movenext
					Loop
					%>
					</table>
					<br />
				</td>
			</tr>
			<%
		End if
		rsDataPayments.close
		Set rsDataPayments = nothing
	End If
	%>
	<!--#include file="trans_detail_chb.asp"-->
	<%
End If
%>
<tr><td height="10"></td></tr>
<!--#include file="trans_detail_administration.asp"-->
<tr><td height="10"></td></tr>
</table>
</body>
</html>
<%
rsData.close
set rsData = nothing
rsData2.close
set rsData2 = nothing
call closeConnection()
%>