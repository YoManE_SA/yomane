<%@ Page Language="VB" EnableEventValidation="false" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script runat="server">
		Dim sQueryString, sDivider As String
		Const RGB_PASS As String = "#66CC66", RGB_FAIL As String = "#ff6666"
		
		Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
			Security.CheckPermission(lblPermissions)
			If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") Else PGData.PageSize = 25
			Dim sWhere As String = ""
			If Request("ID") <> "" Then
				sWhere &= " And h.History_id=" & dbPages.TestVar(Request("ID"), 0, -1, 0)
			Else
				If Request("FilterMerchantID") <> "" Then sWhere &= " And h.Merchant_id = " & Request("FilterMerchantID")
				If Request("fromDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And h.InsertDate >= '" & Request("fromDate") & " " & Request("fromTime") & "'"
				If Request("toDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And h.InsertDate <= '" & Request("toDate") & " " & Request("toTime") & "'"
				If Request("SourceIdentity") <> "" Then sWhere &= " And h.SourceIdentity = " & Request("SourceIdentity")
				If Request("ddlHistoryType") <> "" Then sWhere &= " And h.HistoryType_id = " & Request("ddlHistoryType")
			End If
			If sWhere.Length > 0 Then sWhere = " Where " & sWhere.Substring(5)
			dbPages.CurrentDSN = IIf(Request("SQL2") = "1", 2, 1)
			PGData.OpenDataset("Select h.*, ht.Name as EvName, c.CompanyName as mrName " & _
			  "From Log.History as h " & _
			  "Left Join List.HistoryType as ht ON(h.HistoryType_id = ht.HistoryType_id) " & _
			  "Left Join tblCompany as c ON(h.Merchant_id = c.ID) " & _
			  sWhere & " Order By h.History_id Desc")
			sQueryString = dbPages.CleanUrl(Request.QueryString)
		End Sub
    </script>
	<script language="JavaScript">
		function IconVisibility(fVar) {
			spanObj = document.getElementById(fVar);
			spanObj.style.visibility = (spanObj.style.visibility == "hidden" ? "visible" : "hidden");
		}

		function ExpandNode(fTrNum) {
			trObj = document.getElementById("Row" + fTrNum)
			imgObj = document.getElementById("Img" + fTrNum)
			if (trObj) {
				if (trObj.style.display == '') {
					imgObj.src = '../images/tree_expand.gif';
					trObj.style.display = 'none';
				}
				else {
					imgObj.src = '../images/tree_collapse.gif';
					trObj.style.display = '';
				}
			}
		}
	</script>
</head>
<body>
	<form id="Form2" runat="server">
		<table align="center" style="width:92%">
		<tr>
			<td id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> HISTORY LOG
				<span style="color:Black;font-weight:normal;"><%=IIf(Request("From") = "Top", " - Last Actions", String.Empty)%></span>
				<br /><br />
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%" cellpadding="1" cellspacing="0">
		<tr>
			<th>History ID</th>
			<th>History Date</th>
			<th>Type</th>
			<th>Merchant</th>
			<th>Reference Number</th>
			<th>IP Address</th>
		</tr>
		<%
		Dim i As Integer
		While PGData.Read()
				i += 1
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';" height="16">
				<td nowrap="nowrap">
					<img onclick="ExpandNode('PL<%=i%>');" style="cursor: pointer;" id="ImgPL<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" />
					<%= PGData("History_id")%>
				</td>
				<td><%= PGData("InsertDate")%></td>
				<td><%= PGData("EvName")%></td>
				<td><%= PGData("mrName")%></td>
				<td><%= PGData("SourceIdentity")%></td>
				<td><%= PGData("InsertIPAddress")%></td>
			</tr>
			<tr id="RowPL<%=i%>" style="display: none;">
				<td>&nbsp;</td>
				<td colspan="4">
					<div><b>Data:</b><br /> <% If Not IsDBNull(PGData("VariableChar")) Then Response.Write(PGData("VariableChar").ToString().Replace("<", "&lt;").Replace(">", "&gt;"))%></div><br /><br />
					<div><b>XML:</b><br /> <% If Not IsDBNull(PGData("VariableXML")) Then Response.Write(PGData("VariableXML").ToString().Replace("<", "&lt;").Replace(">", "&gt;"))%></div><br />
				</td>
			</tr>
			<tr><td height="1" colspan="9"  bgcolor="#ffffff"></td></tr>
			<tr><td height="1" colspan="9"  bgcolor="silver"></td></tr>
			<tr><td height="1" colspan="9"  bgcolor="#ffffff"></td></tr>
			<%
		End While
		PGData.CloseDataset()%>
		</table>
	</form>
	<br />
	<table align="center" style="width:92%">
	 <tr>
	  <td><UC:Paging runat="Server" id="PGData" PageID="PageID"  /></td>
	 </tr>
	</table>
</body>
</html>