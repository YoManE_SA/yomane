<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script runat="server">
		Dim sQueryString, sDivider As String
		Const RGB_PASS As String = "#66CC66", RGB_FAIL As String = "#ff6666"
		
		Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
			Security.CheckPermission(lblPermissions)
			If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") Else PGData.PageSize = 25
			
			Dim sWhere As String = ""
			If Request("ID") <> "" Then
				sWhere &= " And tblTransactionAmount.ID=" & dbPages.TestVar(Request("ID"), 0, -1, 0)
			ElseIf Request("TransID") <> "" Then
				sWhere &= " And "
				sWhere &= "(TransPassID=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & _
				 " Or TransApprovalID=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & _
				 " Or TransPendingID=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & _
				 " Or TransFailID=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & ")"
			Else
				If Request("fromDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And tblTransactionAmount.InsertDate >= '" & Request("fromDate") & " " & Request("fromTime") & "'"
				If Request("toDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And tblTransactionAmount.InsertDate <= '" & Request("toDate") & " " & Request("toTime") & "'"
				If Request("ShowCompanyID") <> "" Then sWhere &= " And MerchantID = " & Request("ShowCompanyID")
				If Request("ddlEventType") <> "" Then sWhere &= " And TypeID = " & Request("ddlEventType")
			End If
			If Security.IsLimitedMerchant Then sWhere &= " AND CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
			If sWhere.Length > 0 Then sWhere = " Where " & sWhere.Substring(5)
			dbPages.CurrentDSN = IIf(Request("SQL2") = "1", 2, 1)
	        PGData.OpenDataset("Select tblTransactionAmount.*, List.TransAmountType.Name as EvName, tblCompany.CompanyName From tblTransactionAmount " & _
			"Left Join List.TransAmountType ON(List.TransAmountType.TransAmountType_id = tblTransactionAmount.TypeID) " & _
			"Left Join tblCompany ON(tblCompany.ID = tblTransactionAmount.MerchantID) " & _
			sWhere & " Order By tblTransactionAmount.ID Desc")
			sQueryString = dbPages.CleanUrl(Request.QueryString)
		End Sub
	</script>
</head>
<body>
	<form id="Form2" runat="server">
		<table align="center" style="width:92%">
		<tr>
			<td id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> AMOUNTS
				<span style="color:Black;font-weight:normal;"><%=IIf(Request("From") = "Top", " - Last Actions", String.Empty)%></span>
				<br /><br />
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%" cellpadding="1" cellspacing="0">
		<tr>
			<th>ID</th>
			<th>Date</th>
			<th>Trans ID</th>
			<th>Trans Source</th>
			<th>Merchant</th>
			<th>Type</th>
			<th style="text-align:right!important;">Amount</th>
			<td style="width:25px;"></td>
			<th>Settlement</th>
			<th>Settle Date</th>
			<th style="text-align:right!important;">Settle Amount</th>
		</tr>
		<%
		While PGData.Read()
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';" height="16">
				<td><%= PGData("ID")%></td>
				<td><%= PGData("InsertDate")%></td>
				<%	
				If PGData("TransPassID") IsNot DBNull.Value Then
					Response.Write("<td>" & PGData("TransPassID") & "</td><td>Pass</td>")
				ElseIf PGData("TransApprovalID") IsNot DBNull.Value Then
					Response.Write("<td>" & PGData("TransApprovalID") & "</td><td>Approval</td>")
				ElseIf PGData("TransPendingID") IsNot DBNull.Value Then
					Response.Write("<td>" & PGData("TransPendingID") & "</td><td>Pending</td>")
				ElseIf PGData("TransFailID") IsNot DBNull.Value Then
					Response.Write("<td>" & PGData("TransFailID") & "</td><td>Fail</td>")
				End If
				%>
				<td><%= PGData("CompanyName")%></td>
				<td><%= PGData("EvName")%></td>
				<td style="text-align:right!important;"><%= dbPages.FormatCurr(PGData("Currency"), PGData("Amount"))%></td>
				<td></td>
				<td><%= IIf(PGData("SettlementID") Is DBNull.Value, "---", PGData("SettlementID"))%></td>
				<td><%= IIf(PGData("SettlementDate") Is DBNull.Value, "---", DBPages.TestVar(PGData("SettlementDate"), DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue).ToShortDateString())%></td>
				<td style="text-align:right!important;"><%= dbPages.FormatCurr(IIf(PGData("SettledCurrency") Is DBNull.Value, 255, PGData("SettledCurrency")), IIf(PGData("SettledAmount") Is DBNull.Value, 0, PGData("SettledAmount")))%></td>
			</tr>
			<tr><td height="1" colspan="7"  bgcolor="#ffffff"></td><td></td><td height="1" colspan="3"  bgcolor="#ffffff"></td></tr>
			<tr><td height="1" colspan="7"  bgcolor="silver"></td><td></td><td height="1" colspan="3"  bgcolor="#silver"></td></tr>
			<tr><td height="1" colspan="7"  bgcolor="#ffffff"></td><td></td><td height="1" colspan="3"  bgcolor="#ffffff"></td></tr>
			<%
		End While
		PGData.CloseDataset()%>
		</table>
	</form>
	<br />
	<table align="center" style="width:92%">
	 <tr>
	  <td><UC:Paging runat="Server" id="PGData" PageID="PageID"  /></td>
	 </tr>
	</table>
</body>
</html>