<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20090922 Tamir
	'
	' Security management
	'
	Dim nUser As Integer = 0
	
	Sub Page_PreInit()
		If dbPages.TestVar(Request.QueryString("User"), 1, 0, 0) = 0 Then Response.Redirect("common_blank.htm")
		nUser = Request.QueryString("User")
		If dbPages.ExecScalar("IF EXISTS (SELECT ID FROM tblSecurityUser WHERE ID=" & nUser & " AND su_IsActive=1) SELECT 1 ELSE SELECT 0") = 0 Then Response.Redirect("common_blank.htm")
	End Sub
	
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsData.ConnectionString = dbPages.DSN
		dsData.SelectCommand = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY DebitCompany_ID)%2 AS bit) IsOdd, DebitCompany_ID, dc_Name," & _
		" Cast(CASE WHEN sudc_User IS NULL THEN 0 ELSE 1 END AS bit) IsChecked FROM tblDebitCompany" & _
		" LEFT JOIN tblSecurityUserDebitCompany ON DebitCompany_ID=sudc_DebitCompany AND sudc_User=@nUser" & _
		IIf(Security.IsLimitedDebitCompany, " WHERE DebitCompany_ID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))", String.Empty) & _
		" ORDER BY DebitCompany_ID"
		If Not Page.IsPostBack Then
			litUser.Text = dbPages.ExecScalar("SELECT su_Name FROM tblSecurityUser WHERE ID=" & nUser)
		End If
	End Sub

	Sub SavePermissions(ByVal o As Object, ByVal e As EventArgs)
		dbPages.ExecSql("DELETE FROM tblSecurityUserDebitCompany WHERE sudc_User=" & nUser)
		Dim nDC As Integer
		For Each riLine As RepeaterItem In repData.Items
			If riLine.ItemType = ListItemType.Item Or riLine.ItemType = ListItemType.AlternatingItem Then
				If CType(riLine.FindControl("chkPermitted"), CheckBox).Checked Then
					nDC = CType(riLine.FindControl("litDebitCompany"), Literal).Text
					dbPages.ExecSql("INSERT INTO tblSecurityUserDebitCompany(sudc_User, sudc_DebitCompany) VALUES (" & nUser & ", " & nDC & ")")
				End If
			End If
		Next
		repData.DataBind()
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<form id="frmServer" runat="server">
	<table width="95%" align="center">
	<tr>
		<td>
			<table>	
			<tr>
				<td id="pageMainHeading">
					<asp:label ID="lblPermissions" runat="server" /> Security<span id="pageMainSubHeading"> - Debit Companies</span>
				</td>
			</tr>
			</table>
			<div class="bordered normal">
				You can limit <b><asp:Literal ID="litUser" runat="server" /></b> to specific debit companies.<br />When none selected all are allowed<br />
			</div>
			<asp:SqlDataSource ID="dsData" runat="server">
				<SelectParameters>
					<asp:QueryStringParameter Name="nUser" QueryStringField="User" Type="Int32" />
				</SelectParameters>
			</asp:SqlDataSource>

			<table class="formNormal">
			<tr>
				<th>ID</th>
				<th colspan="2">Debit Company</th>
				<th>Permitted</th>
				<td style="width:50px;"></td>
				<th>ID</th>
				<th colspan="2">Debit Company</th>
				<th>Permitted</th>
			</tr>
			<asp:Repeater ID="repData" DataSourceID="dsData" runat="server">
				<ItemTemplate>
					<asp:Literal ID="Literal1" Text='<%# IIf(Eval("IsOdd"), "<tr>", String.Empty) %>' runat="server" />
						<td class="underlined" style="text-align:right;"><asp:Literal ID="litDebitCompany" Text='<%#Eval("DebitCompany_ID")%>' runat="server" /></td>
						<td class="underlined"><asp:Image ID="Image1" ImageUrl='<%# "/NPCommon/ImgDebitCompanys/23X12/" & Eval("DebitCompany_ID") & ".gif"%>' Width="23" Height="12" runat="server" /></td>
						<td class="underlined"><asp:Literal ID="Literal2" Text='<%#Eval("dc_Name")%>' runat="server" /></td>
						<td class="underlined" style="text-align:center;"><asp:CheckBox ID="chkPermitted" Checked='<%#Eval("IsChecked")%>' CssClass="option" runat="server" /></td>
					<asp:Literal ID="Literal3" Text='<%# IIf(Eval("IsOdd"), "<td></td>", "</tr>") %>' runat="server" />
				</ItemTemplate>
			</asp:Repeater>
			</table>
			<br />
			<asp:Button CssClass="buttonWhite" Text=" Save " OnClick="SavePermissions" runat="server" />
		</td>
	</tr>
	</table>
	</form>
</body>
</html>