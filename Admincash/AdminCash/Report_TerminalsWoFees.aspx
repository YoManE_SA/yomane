<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Import namespace="System.Data"%>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<script runat="server">
	Dim sSQL As String

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		Dim sbSQL As New StringBuilder(String.Empty)
		For Each liField As ListItem In cblFields.Items
			If liField.Selected Then sbSQL.Append(" OR " & liField.Value & "='0.00'")
		Next
		If Not String.IsNullOrEmpty(sbSQL.ToString) Then sbSQL.Remove(0, 4).Insert(0, "(").Append(")")
		If Security.IsLimitedMerchant Then sbSQL.Append(" AND MerchantID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))")
		If Security.IsLimitedDebitCompany Then sbSQL.Append(" AND DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))")
		If sbSQL.ToString <> String.Empty Then sbSQL.Insert(0, " WHERE")
		sbSQL.Insert(0, "SELECT * FROM viewTerminalsWithZeroFee").Append(" ORDER BY [Merchant], [Payment Method]")
		sSQL = sbSQL.ToString
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Terminals Without Fees</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	    .dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
</head>
<body style="text-align:center;">
	<form id="frmMain" runat="server">
		<table width="95%">
		<tr>
			<td id="pageMainHeadingTd">
				<asp:label ID="lblPermissions" runat="server" />
				<span id="pageMainHeading">Terminals Without Fees</span>
			</td>
		</tr>
		</table>
		<br />
		<table class="formNormal" width="95%" style="border:solid 1px gray;">
		<tr>
			<td style="vertical-align:middle;">
				<table>
					<tr>
						<td>Check for zero on the following fees: &nbsp;</td>
						<td>
							<asp:CheckBoxList ID="cblFields" runat="server" RepeatColumns="7">
								<asp:ListItem Text="Trans." Value="[Trans. Fee]" Selected="True" />
								<asp:ListItem Text="Clearing" Value="[Clearing Fee]" Selected="True" />
								<asp:ListItem Text="Pre-Auth" Value="[Pre-Auth Fee]" Selected="True" />
								<asp:ListItem Text="Refund" Value="[Refund Fee]" Selected="True" />
								<asp:ListItem Text="Copy R." Value="[Copy R. Fee]" Selected="True" />
								<asp:ListItem Text="CHB" Value="[CHB Fee]" Selected="True" />
								<asp:ListItem Text="Fail" Value="[Fail Fee]" Selected="False" />
							</asp:CheckBoxList>
						</td>
						<td>&nbsp; <asp:Button ID="btnFilter" runat="server" Text="FILTER" /></td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
		<br />
		<%
			PGData.Form = "frmMain"
			PGData.OpenDataset(sSQL)
		%>
		<table class="formNormal" style="width:95%;">
		<tr>
			<td>
				<UC:Paging runat="Server" id="PGData" 
					PageID="PageNo" 
					GenerateFieldForID="true" 
					PageSize="20" 
					ShowPageCount="true" 
					ShowPageFirst="true" 
					ShowPageLast="true" 
					ShowPageNumber="false" 
					ShowRecordCount="true" 
					ShowRecordNumbers="true" 
					ShowPageNumbers="true" 
					ShowPageNumbersRange="2" 
					ShowPageNumbersRecords="true"
					ShowTable="true"
					TableCssClass="formNormal"
					TableCssStyle="width:100%;"
					TableCustomTitles="|||Terminal<br />Name|Terminal<br />Number|Trans.<br />Fee|Clearing<br />Fee|Pre-Auth<br />Fee|Refund<br />Fee|Copy R.<br />Fee|CHB<br />Fee|Fail<br />Fee"
					TableWrapCells="true"
					TableWrapTitles="true"
					ZeroColor="red"
					CustomFieldCount="13"
				/>
			</td>
		</tr>
		</table>
	</form>
</body>
</html>