﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminCash_Accounts_ContentPage : System.Web.UI.MasterPage
{
	protected override void OnInit(EventArgs e)
	{
		(Page as Netpay.Web.Admin_AccountPage).AccountChanged += ContentPage_AccountChanged;
		base.OnInit(e);
	}

	private void ContentPage_AccountChanged(object sender, EventArgs e)
	{
		var account = (sender as Netpay.Web.Admin_AccountPage).Account;
		phAccountHeader.Controls.Clear();
		if (account == null) return;
		switch (account.AccountType)
		{
			case Netpay.Bll.Accounts.AccountType.Customer:
				phAccountHeader.Controls.Add(LoadControl("~/AdminCash/Customers/Header.ascx"));
				break;
		}
	}


}
