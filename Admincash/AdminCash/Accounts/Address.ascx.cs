﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminCash_Accounts_Address : System.Web.UI.UserControl
{
	private Netpay.Bll.Accounts.AccountAddress _addressData;
	public Netpay.Bll.Accounts.AccountAddress AddressData { get { return _addressData; } set { if (value == null) value = new Netpay.Bll.Accounts.AccountAddress(); _addressData = value; } }

	public override void DataBind()
	{
		base.DataBind();
		if (!string.IsNullOrEmpty(AddressData.StateISOCode)) ddlState.ISOCode = AddressData.StateISOCode;
		if (!string.IsNullOrEmpty(AddressData.CountryISOCode)) ddlCountry.ISOCode = AddressData.CountryISOCode;
	}

	public Netpay.Bll.Accounts.AccountAddress Save(Netpay.Bll.Accounts.AccountAddress setData)
	{
		AddressData = setData;
		AddressData.AddressLine1 = txtAddress1.Text;
		AddressData.AddressLine2 = txtAddress2.Text;
		AddressData.City = txtCity.Text;
		AddressData.PostalCode = txtPostal.Text;
		AddressData.StateISOCode = ddlState.ISOCode;
		AddressData.CountryISOCode = ddlCountry.ISOCode;
		return AddressData;
	}

}