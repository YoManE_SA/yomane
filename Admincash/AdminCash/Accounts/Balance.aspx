﻿<%@ Page Title="" Language="C#" MasterPageFile="../Accounts/ContentPage.master" AutoEventWireup="true" CodeFile="Balance.aspx.cs" Inherits="AdminCash_Accounts_Balance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" Runat="Server">
	<h4 class="dataSection">Current Balance</h4>
    <table width="100%" class="formNormal">
        <tr>
            <th>Currency</th>
            <th>Balance</th>
            <th>Pending</th>
            <th>Expected</th>
        </tr>
        <asp:Repeater runat="server" ID="rptStatus">
            <ItemTemplate>
                <tr>
                    <td><asp:LinkButton runat="server" Text='<%# Eval("CurrencyIso") %>' OnCommand="Show_Click" CommandArgument='<%# Eval("CurrencyIso") %>' /></td>
                    <td><%# Eval("Current")%></td>
                    <td><%# Eval("Pending")%></td>
                    <td><%# Eval("Expected") %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <br />
    <asp:PlaceHolder runat="server" ID="phHistory" Visible="false">
    	<h4 class="dataSection">Balance History for <%# SelectedCurrencyIso %></h4>
        <table width="100%" class="formNormal">
            <tr>
                <th>InsertDate</th>
                <th>Amount</th>
                <th>Total</th>
                <th>Text</th>
                <th>IsPending</th>
            </tr>
            <asp:Repeater runat="server" ID="rptHistory">
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("InsertDate") %></td>
                        <td><%# Eval("Amount") %></td>
                        <td><%# Eval("Total") %></td>
                        <td><%# Eval("Text") %></td>
                        <td><%# Eval("IsPending") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </asp:PlaceHolder>
</asp:Content>

