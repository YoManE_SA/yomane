﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminCash_Accounts_Balance : Netpay.Web.Admin_AccountPage
{
    protected string SelectedCurrencyIso = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) RefreshList();
    }

    private void RefreshList()
    {
        rptStatus.DataSource = Netpay.Bll.Accounts.Balance.GetStatus(AccountID.Value);
        rptStatus.DataBind();
    }

    protected void Show_Click(object sender, CommandEventArgs e)
    {
        SelectedCurrencyIso = (string) e.CommandArgument;
        rptHistory.DataSource = Netpay.Bll.Accounts.Balance.Search(new Netpay.Bll.Accounts.Balance.SearchFilters() { AccountID = AccountID.Value, CurrencyIso = SelectedCurrencyIso }, new Netpay.Infrastructure.SortAndPage(0, 20, "InsertDate", true));
        phHistory.DataBind();
        phHistory.Visible = true;
    }
}