﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

public partial class AdminCash_Accounts_StoredPaymentMethods : Netpay.Web.Admin_AccountPage
{
	protected Netpay.Bll.Accounts.StoredPaymentMethod ItemData;
	protected void Page_Load(object sender, EventArgs e)
    {
		ddlExpYear.YearFrom = DateTime.Now.Year - 5;
		ddlExpYear.YearTo = DateTime.Now.Year + 10;
		if(!IsPostBack) RefreshList();
    }

	private void RefreshList()
	{
		rptList.DataSource = Netpay.Bll.Accounts.StoredPaymentMethod.LoadForAccount(AccountID.Value);
		rptList.DataBind();
	}

	protected void New_Command(object sender, CommandEventArgs e)
	{
		ItemData = new Netpay.Bll.Accounts.StoredPaymentMethod();
		ItemData.MethodInstance.PaymentMethodId = ddlPaymentMethod.SelectedValue.ToNullableEnumByValue<Netpay.CommonTypes.PaymentMethodEnum>().GetValueOrDefault();
		phEditForm.Visible = (ItemData != null);
        phCardBalance.Visible = false;
        if (phEditForm.Visible) phEditForm.DataBind();
	}

	protected void List_Command(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            ItemData = Netpay.Bll.Accounts.StoredPaymentMethod.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
            phEditForm.Visible = (ItemData != null);
            phCardBalance.Visible = false;
            if (phEditForm.Visible) phEditForm.DataBind();
        } else {
            ItemData = Netpay.Bll.Accounts.StoredPaymentMethod.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
            rptCardBalance.DataSource = ItemData.Provider.GetBalace(ItemData, new Range<DateTime>(DateTime.Now.AddYears(-1), DateTime.Now));
            rptCardBalance.DataBind();
            phCardBalance.Visible = true;
            phEditForm.Visible = false;
        }
	}

	protected void Save_Command(object sender, CommandEventArgs e)
	{
		ItemData = Netpay.Bll.Accounts.StoredPaymentMethod.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
		if (ItemData == null) {
			ItemData = new Netpay.Bll.Accounts.StoredPaymentMethod();
			ItemData.Account_id = AccountID.Value;
            ItemData.MethodInstance.PaymentMethodId = hfPaymentMethod.Value.ToNullableEnumByName<Netpay.CommonTypes.PaymentMethodEnum>().GetValueOrDefault();
		}
		ItemData.OwnerName = txtOwnerName.Text;
		ItemData.MethodInstance.SetExpirationMonth(ddlExpYear.SelectedYear.GetValueOrDefault(1900), ddlExpMonth.SelectedMonth.GetValueOrDefault(1));
		ItemData.MethodInstance.SetEncryptedData(txtEncValue1.Text, txtEncValue2.Text);
		ItemData.BillingAddress = caAddress.Save(ItemData.BillingAddress);
		ItemData.Save();
		RefreshList();
	}
}