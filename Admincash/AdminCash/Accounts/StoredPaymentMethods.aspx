﻿<%@ Page Title="" Language="C#" MasterPageFile="../Accounts/ContentPage.master" AutoEventWireup="true" CodeFile="StoredPaymentMethods.aspx.cs" Inherits="AdminCash_Accounts_StoredPaymentMethods" %>
<%@ Register Src="Address.ascx" TagPrefix="CUC" TagName="AccountAddress" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" Runat="Server">
	<table width="100%">
		<tr>
			<td valign="top" width="50%">
				<h4 class="dataSection">Stored Payment Methods</h4>
				<table width="100%" class="formNormal">
					<tr>
						<th>ID</th>
						<th>PaymentMethod</th>
						<th>OwnerName</th>
						<th>Account Data</th>
						<th>Exp.Date</th>
						<th>Default</th>
						<th>Balance</th>
					</tr>
					<asp:Repeater runat="server" ID="rptList" OnItemCommand="List_Command">
						<ItemTemplate>
							<tr>
								<td><%# Eval("ID") %></td>
								<td><asp:LinkButton runat="server" Text='<%# Eval("MethodInstance.PaymentMethodId") %>' CommandName="Select" CommandArgument='<%# Eval("ID") %>' /></td>
								<td><%# Eval("OwnerName") %></td>
								<td><%# Eval("MethodInstance.Value1First6") %>...<%# Eval("MethodInstance.Value1Last4") %></td>
								<td><%# Eval("MethodInstance.ExpirationDate", "{0:MM-yyy}") %></td>
								<td><%# Eval("IsDefault") %></td>
								<td><asp:LinkButton runat="server" Text="Show" CommandName="ShowBalance" CommandArgument='<%# Eval("ID") %>' Enabled='<%# Eval("HasBalance") %>' /></td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</table>
				<br />
				<netpay:PaymentMethodDropDown runat="server" ID="ddlPaymentMethod" />
				<asp:Button runat="server" ID="btnAdd" Text="Add Item" OnCommand="New_Command" />
			</td>
			<td width="60">&nbsp;</td>
			<td valign="top">
				<asp:PlaceHolder runat="server" ID="phCardBalance" Visible="false">
					<h4 class="dataSection">Card Balance</h4>
                    <table width="100%" class="formNormal">
                        <tr>
                            <th>Date</th>
                            <th>Text</th>
                            <th>Amount</th>
                        </tr>
                        <asp:Repeater runat="server" ID="rptCardBalance">
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("InsertDate") %></td>
                                    <td><%# Eval("Text") %></td>
                                    <td><%# Eval("Amount") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
				</asp:PlaceHolder>
				<asp:PlaceHolder runat="server" ID="phEditForm" Visible="false">
					<h4 class="dataSection">Edit Payment Method</h4>
					<table width="100%" class="formData">
						<tr>
							<th width="30%">ID</th>
							<td><asp:Literal runat="server" ID="ltID" Text='<%# ItemData.ID %>' /></td>
						</tr>
						<tr>
							<th>PaymentMethod</th>
							<td>
                                <asp:Literal runat="server" ID="ltPaymentMethod" Text='<%# ItemData.MethodInstance.PaymentMethodId %>' />
                                <asp:HiddenField runat="server" ID="hfPaymentMethod" Value='<%# ItemData.MethodInstance.PaymentMethodId %>' />
							</td>
						</tr>
						<tr>
							<th>EncryptionKey</th>
							<td><asp:Literal runat="server" ID="ltEncryptionKey" Text='<%# ItemData.MethodInstance.EncryptionKey %>' /></td>
						</tr>
						<tr>
							<th>Owner Name</th>
							<td><asp:TextBox runat="server" ID="txtOwnerName" Text='<%# ItemData.OwnerName %>' /></td>
						</tr>
						<tr>
							<th><asp:Literal runat="server" Text="Account Value 1" ID="ltEncValue1" /></th>
							<td><asp:TextBox runat="server" ID="txtEncValue1" /></td>
						</tr>
						<tr>
							<th><asp:Literal runat="server" Text="Account Value 2" ID="ltEncValue2" /></th>
							<td><asp:TextBox runat="server" ID="txtEncValue2" /></td>
						</tr>
						<tr>
							<th>Exp. Date</th>
							<td>
								<netpay:MonthDropDown runat="server" ID="ddlExpMonth" Width="80px" Value='<%# ItemData.MethodInstance.ExpirationDate.GetValueOrDefault().Month %>' /> -
								<netpay:YearDropDown runat="server" ID="ddlExpYear" Width="60px" Value='<%# ItemData.MethodInstance.ExpirationDate.GetValueOrDefault(DateTime.Now).Year %>' />
							</td>
						</tr>
						<tr>
							<th>Issuer Country Iso</th>
							<td><asp:Literal runat="server" ID="ltIssuerCountryIsoCode" Text='<%# ItemData.IssuerCountryIsoCode %>' /></td>
						</tr>
						<tr>
							<th>Is Default</th>
							<td><asp:CheckBox runat="server" ID="chkIsDefault" Checked='<%# ItemData.IsDefault %>' /></td>
						</tr>
					</table>
					<br />
					<h4 class="dataSection">Billing Address</h4>
					<CUC:AccountAddress runat="server" ID="caAddress" AddressData='<%# ItemData.BillingAddress %>' />
					<asp:Button runat="server" ID="btnSave" OnCommand="Save_Command" CommandArgument='<%# ItemData.ID %>' Text="Save" />
				</asp:PlaceHolder>
			</td>
		</tr>
	</table>
</asp:Content>

