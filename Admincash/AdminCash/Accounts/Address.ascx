﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Address.ascx.cs" Inherits="AdminCash_Accounts_Address" %>
<table width="100%" class="formData">
	<tr>
		<th width="30%">Address Line 1</th>
		<td><asp:TextBox runat="server" ID="txtAddress1" Text='<%# AddressData.AddressLine1 %>' /></td>
	</tr>
	<tr>
		<th>Address Line 2</th>
		<td><asp:TextBox runat="server" ID="txtAddress2" Text='<%# AddressData.AddressLine2 %>' /></td>
	</tr>
	<tr>
		<th>City</th>
		<td><asp:TextBox runat="server" ID="txtCity" Text='<%# AddressData.City %>' /></td>
	</tr>
	<tr>
		<th>Postal Code</th>
		<td><asp:TextBox runat="server" ID="txtPostal" Text='<%# AddressData.PostalCode %>' /></td>
	</tr>
	<tr>
		<th>Province</th>
		<td><netpay:StateDropDown runat="server" ID="ddlState" /></td>
	</tr>
	<tr>
		<th>Country</th>
		<td><netpay:CountryDropDown runat="server" ID="ddlCountry" /></td>
	</tr>
</table>
