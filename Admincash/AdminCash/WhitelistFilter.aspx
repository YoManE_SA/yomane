<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<script runat="server">
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)

		Dim sSQL As String, drData As SqlDataReader
		litCardType.Text = "<select name=""PaymentMethod"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT PaymentMethod_id AS 'ID', Name AS 'pm_Name' FROM List.PaymentMethod WHERE PaymentMethod_id IN (SELECT DISTINCT ccwl_PaymentMethod FROM tblCreditCardWhiteList)"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			litCardType.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		litCardType.Text &= "</select>"
		drData.Close()
		litLevel.Text = "<select name=""Level"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT GD_ID, GD_Text, GD_Color FROM GetGlobalData(" & GlobalDataGroup.WHITE_LIST_LEVEL & ") WHERE GD_ID IN (SELECT DISTINCT ccwl_Level FROM tblCreditCardWhiteList)"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			litLevel.Text &= "<option class=""grayBG"" style=""background-color:" & drData(2) & """ value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		litLevel.Text &= "</select>"
		drData.Close()
		litUser.Text = "<select name=""UserName"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT DISTINCT dbo.RemoveDomainFromUsername(ccwl_UserName) Username FROM tblCreditCardWhiteList ORDER BY Username"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			litUser.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(0) & "</option>"
		Loop
		litUser.Text &= "</select>"
		drData.Close()
		litMonth.Text = "<select name=""MonthFrom"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		litMonthTo.Text = "<select name=""MonthTo"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT DISTINCT ccwl_ExpMonth, Right('0'+LTrim(RTrim(Str(ccwl_ExpMonth))), 2) FROM tblCreditCardWhiteList ORDER BY ccwl_ExpMonth"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			litMonth.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
			litMonthTo.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		litMonth.Text &= "</select>"
		litMonthTo.Text &= "</select>"
		drData.Close()
		litYear.Text = "<select name=""YearFrom"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		litYearTo.Text = "<select name=""YearTo"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT DISTINCT ccwl_ExpYear FROM tblCreditCardWhiteList ORDER BY ccwl_ExpYear"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			litYear.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(0) & "</option>"
			litYearTo.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(0) & "</option>"
		Loop
		litYear.Text &= "</select>"
		litYearTo.Text &= "</select>"
		drData.Close()
	End Sub
</script>
<asp:Content runat="server" ContentPlaceHolderID="body">
	<form runat="server" action="Whitelist.aspx" method="get" target="frmBody" onsubmit="return filterForm_Submit(null)">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" name="MerchantID" id="MerchantID" value="" />

	<h1><asp:Label ID="lblPermissions" runat="server" /> CC Whitelist <span> - Filter</span></h1>

	<table class="filterNormal">
		<tr>
			<td colspan="2">
				For adding credit card to the whitelist <a target="frmBody" href="whitelist.aspx">click here</a>
			</td>
		</tr>
	</table>

	<hr class="filter" />
	
	<table class="filterNormal">
	<tr>
		<td>
			<Uc1:FilterMerchants id="FMerchants" ClientField="MerchantID"
			 SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
			  INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" runat="server" />
		</td>
	</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<td colspan="2">
				<Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2" class="filter indent">Record Details</th></tr>
		<tr>
			<td>Type</td>
			<td>
				<input type="radio" class="option" name="RecordType" value="" checked="checked" />All
				<input type="radio" class="option" name="RecordType" value="0" />Global
				<input type="radio" class="option" name="RecordType" value="1" />Merchant
			</td>
		</tr>
		<tr>
			<td>Status</td>
			<td>
				<input type="radio" name="Burnt" value="" checked="checked" />All
				<input type="radio" name="Burnt" value="0" />Active
				<input type="radio" name="Burnt" value="1" />Burnt
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<td>Record ID</td>
			<td><input name="ID" /></td>
		</tr>
		<tr>
			<td>Level</td>
			<td><asp:Literal ID="litLevel" runat="server" /></td>
		</tr>
		<tr>
			<td>Added by</td>
			<td><asp:Literal ID="litUser" runat="server" /></td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Card Details</th></tr>
		<tr>
			<td>Card Type</td>
			<td><asp:Literal ID="litCardType" runat="server" /></td>
		</tr>
		<tr>
			<td>BIN</td>
			<td><input name="Bin" /></td>
		</tr>
		<tr>
			<td>Last 4 Digits</td>
			<td><input name="Last4" /></td>
		</tr>
		<tr>
			<td>Card Holder</td>
			<td><input name="CardHolder" /></td>
		</tr>
		<tr>
			<td>Card Number</td>
			<td><input name="CardNumber" /></td>
		</tr>
		<tr>
			<td>Validity From</td>
			<td><asp:Literal ID="litMonth" runat="server" /> / <asp:Literal ID="litYear" runat="server" />
			</td>
		</tr>
		<tr>
			<td>Validity To</td>
			<td><asp:Literal ID="litMonthTo" runat="server" /> / <asp:Literal ID="litYearTo" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2" class="filter indent">Paging</th></tr>
		<tr>
			<td>
				<select name="PageSize" class="grayBG">
					<option value="10">10 rows</option>
					<option value="25" selected="selected">25 rows</option>
					<option value="50">50 rows</option>
					<option value="100">100 rows</option>
				</select>
			</td>
			<td align="right" nowrap="nowrap">
				<input type="submit" class="buttonFilter" value="Search" />
				<input type="reset" class="buttonFilter buttonDelete" value="Reset" />
			</td>
		</tr>
	</table>
	</form>
</asp:Content>