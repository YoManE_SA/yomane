<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<script runat="server">
    Dim sSubtitle As String
    Dim sWhere As String = String.Empty
    Sub UpdateBillTo(ByVal o As Object, ByVal e As EventArgs)
        dbPages.ExecSql("UPDATE tblInvoiceDocument SET id_BillToName='" & dbPages.DBText(txaBillTo.Text).Replace(Chr(13), "<br />") & "' WHERE ID=" & hidBillToID.Text & " AND id_IsPrinted=0")
        Response.Redirect("invoices.aspx?" & Request.QueryString.ToString)
    End Sub

    Sub Page_Load()
        Security.CheckPermission(lblPermissions)
        If Request("from") = "menu" Then
            sSubtitle = " - Search Results"
        Else
            sSubtitle = IIf(Request("TransactionPay") = String.Empty, " - Search Results", " - Settlement #" + Request("TransactionPay"))
        End If
        Dim nPagesize As Int16 = dbPages.TestVar(Request("PageSize"), 1, 0, 0)
        If nPagesize > 0 Then PGData.PageSize = nPagesize

        Dim dDateMin As String = Trim(Request("ctl00_body_fdtrControl_rdtpFrom_dateInput_text"))
        Dim dDateMax As String = Trim(Request("ctl00_body_fdtrControl_rdtpTo_dateInput_text"))
        If Request("ID") <> String.Empty Then sWhere &= " AND ID=" & Request("ID")
        If Request("ShowCompanyID") <> String.Empty Then sWhere &= " AND id_MerchantID=" & Request("ShowCompanyID")
        If IsDate(dDateMin) Then sWhere &= " AND id_InsertDate>=Convert(datetime, '" & dDateMin & "', 103)"
        If IsDate(dDateMax) Then sWhere &= " AND id_InsertDate<=Convert(datetime, '" & dDateMax & "', 103)"
        If Request("invoiceType") <> String.Empty Then sWhere &= " AND id_Type=" & Request("invoiceType")
        If Request("payType") <> String.Empty Then sWhere &= " AND id_IsManual=" & Request("payType")
        If Request("BillingCompanys_id") <> String.Empty Then sWhere &= " AND id_BillingCompanyID=" & Request("BillingCompanys_id")
        If Request("billNumFrom") <> String.Empty Then sWhere &= " AND id_InvoiceNumber>=" & Request("billNumFrom")
        If Request("billNumTo") <> String.Empty Then sWhere &= " AND id_InvoiceNumber<=" & Request("billNumTo")
        If Security.IsLimitedMerchant Then sWhere &= " AND id_MerchantID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
        If sWhere <> String.Empty Then sWhere = " WHERE " & sWhere.Substring(4)

        Dim sSQL As String = "SELECT * FROM viewInvoices " & sWhere & " ORDER BY id_InsertDate DESC, id_InvoiceNumber DESC"

        If Request("Export") <> "" Then
            Dim nTotal(eCurrencies.MAX_CURRENCY) As Decimal, i As Long
            Dim nsWriter As New StringBuilder

            PGData.PageSize = 10000
            PGData.OpenDataset(sSQL)
            nsWriter.Append("<html><head><title>Invoices</title><meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1255"" /><style>")
            nsWriter.Append(dbPages.GetFileText(Request.MapPath("../StyleSheet/stylePdfEng.css")))
            nsWriter.Append("</style></head><body>")
            nsWriter.Append("<span id=""pageMainHeading"">INVOICES</span><br /><br /><table width=""100%"" class=""formNormal"" border=""1"" bordercolor=""silver"">")
            nsWriter.Append("<tr><th>No.</th><th>Date</th><th>Type</th><th>Bill To</th><th>VAT%</th>")
            nsWriter.Append("<th style=""text-align:center;"">Currency</th>")
            nsWriter.Append("<th style=""text-align:right;"">Amount</th>")
            nsWriter.Append("<th style=""text-align:right;"">VAT</th>")
            nsWriter.Append("<th style=""text-align:right;"">Total</th></tr>")
            While PGData.Read()
                nTotal(PGData("id_currency")) += dbPages.TestVar(PGData("id_TotalDocument"), 0D, -1D, 0D)
                nsWriter.Append("<tr>")
                nsWriter.Append("<td>" & PGData("id_InvoiceNumber") & "</td>")
                nsWriter.Append("<td>" & PGData("id_InsertDate") & "</td>")
                nsWriter.Append("<td>" & PGData("GD_Text") & "</td>")
                Dim nIdx As Integer = CType(PGData("id_BillToName"), String).IndexOf("<br />")
                nsWriter.Append("<td>" & PGData("id_MerchantID") & " - " & Left(PGData("id_BillToName"), IIf(nIdx > 0, nIdx, CType(PGData("id_BillToName"), String).Length)) & "</td>")
                nsWriter.Append("<td>" & (PGData("id_VATPercent") * 100) & "%</td>")
                nsWriter.Append("<td style=""text-align:center;"">" & PGData("Symbol") & "</td>")
                nsWriter.Append("<td style=""text-align:right;"">" & System.Math.Round(PGData("id_TotalLines"), 2) & "</td>")
                nsWriter.Append("<td style=""text-align:right;"">" & System.Math.Round(PGData("id_TotalVAT"), 2) & "</td>")
                nsWriter.Append("<td style=""text-align:right;"">" & System.Math.Round(PGData("id_TotalDocument"), 2) & "</td>")
                nsWriter.Append("</tr>")
            End While
            nsWriter.Append("<tr><td colspan=""9"" style=""text-align:right;font-weight:bold;"">Total ")
            For i = 0 To nTotal.Length - 1
                If nTotal(i) <> 0 Then nsWriter.Append(dbPages.FormatCurr(i, nTotal(i)) & "<br />")
            Next
            nsWriter.Append("</td></tr>")
            nsWriter.Append("</table>")
            nsWriter.Append("</body></html>")
            PGData.CloseDataset()
            If Request("Export").Trim() = "EXCEL" Then
                Response.ContentEncoding = Text.Encoding.GetEncoding("windows-1255")
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=report.xls")
                Response.Write(nsWriter.ToString())
                Response.End()
            End If
        Else
            PGData.OpenDataset(sSQL)
        End If
    End Sub

    Public Function FormatTotalValue(ByVal nCurrency As Integer, ByVal nCount As Integer, ByVal nSum As Decimal, ByVal nTax As Decimal) As String
        If nCount = 0 Then Return ""
        'Return "(" & nCount & ")" & dbPages.FormatCurr(nCurrency, nSum) & " + " & dbPages.FormatCurr(nCurrency, nTax) & "=" & dbPages.FormatCurr(nCurrency, nSum + nTax)
        Return "<table cellpadding=""0"" cellspacing=""0"" border=""0"">" & _
        "<tr style=""font-weight:bold;""><td>Documents:</td><td style=""text-align:right;"">" & nCount & "</td></tr>" & _
        "<tr><td>Amount:</td><td style=""text-align:right;"">" & dbPages.FormatCurr(nCurrency, nSum) & "</td></tr>" & _
        "<tr><td>VAT:</td><td style=""text-align:right;"">" & dbPages.FormatCurr(nCurrency, nTax) & "</td></tr>" & _
        "<tr><td>Total:</td><td style=""text-align:right;"">" & dbPages.FormatCurr(nCurrency, nSum + nTax) & "</td></tr>" & _
        "</table>"
    End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Invoices<%= sSubtitle %></title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />

	<script language="JavaScript" src="../js/func_common.js"></script>

	<script language="javascript" type="text/javascript">
		function OpenPop(sURL, sName, sWidth, sHeight, sScroll)
		{
			var placeLeft = screen.width / 2 - sWidth / 2
			var placeTop = screen.height / 2 - sHeight / 2
			var winPrint = window.open(sURL, sName, 'TOP=' + placeTop + ',LEFT=' + placeLeft + ',WIDTH=' + sWidth + ',HEIGHT=' + sHeight + ',status=1,scrollbars=' + sScroll);
		}
		function PrintInvoice(nInvoiceNumber, nBillingCompany, nDocumentType)
		{
			var sURL = "invoicePrint.asp?";
			//var sURL="../include/common_billingShow_frameset.asp?";
			sURL += "BillingCompanysID=" + nBillingCompany;
			sURL += "&billingNumber=" + nInvoiceNumber;
			sURL += "&invoiceType=" + nDocumentType;
			OpenPop(sURL, 'winInvoice' + nInvoiceNumber, 700, 500, 1);
		}
		function ShowTransList(nMerchant, nTransactionPay)
		{
			var sURL = "Merchant_transSettledDetail.asp?";
			sURL += "companyID=" + nMerchant;
			sURL += "&payID=" + nTransactionPay;
			OpenPop(sURL, 'winTransList' + nTransactionPay, 900, 750, 1);
		}
		function EditBillTo(nID, sBillTo)
		{
			divBillTo.style.top = event.clientY;
			divBillTo.style.left = event.clientX - 390;
			divBillTo.style.display = "block";
			document.getElementById("hidBillToID").value = nID;
			document.getElementById("txaBillTo").value = sBillTo.replace("<br />", String.fromCharCode(13)).replace("`", "'");
			document.getElementById("txaBillTo").focus();
			event.srcElement.parentElement.style.backgroundColor = "pink";
		}
		function CancelBillTo()
		{
			divBillTo.style.display = "none";
			document.getElementById("tdBillTo" + document.getElementById("hidBillToID").value).style.backgroundColor = "";
			document.getElementById("hidBillToID").value = "";
			document.getElementById("txaBillTo").value = "";
		}
	</script>

	<style type="text/css">
		.dataHeading
		{
			text-decoration: underline;
			white-space: nowrap;
		}
	</style>
</head>
<body>
	<%		If Request("Totals") = "1" Then%>
	<table class="formThin" width="600">
		<tr>
			<td>
				TOTALS
			</td>
			<td style="text-align: right; cursor: pointer; float: right;" onclick="document.getElementById('tblTotals').style.display='none';">
				<b>X</b>
			</td>
		</tr>
	</table>
	<table class="formNormal" width="600" style="margin: 8px 5px;">
		<tr>
			<%
				Dim sSQL1 As String = "SELECT * FROM GetGlobalData(60)", nTypes As Integer = 0
				For Each gdv As GlobalDataValue In GlobalData.GroupValues(GlobalDataGroup.DOCUMENT_TYPE)
					Response.Write("<th style=""white-space:nowrap;"">" & gdv.Text & "</th>")
					nTypes += 1
				Next
			%>
		</tr>
		<%
			Dim sSQL2 As String = "SELECT id_Currency, id_Type, Count(*), Sum(id_TotalLines), sum(id_TotalVAT)" & _
			" FROM tblInvoiceDocument Where ID IN(Select " & _
			" ID From tblInvoiceDocument " & sWhere & " )" & _
			" Group By id_Currency, id_Type ORDER BY id_Currency Asc, id_Type Asc"
			Dim nLastCur As Byte = 0
			Dim nAmounts(nTypes) As Decimal
			Dim nTax(nTypes) As Decimal
			Dim nCount(nTypes) As Integer
			Dim bHasData As Boolean = False
			Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL2)
			While iReader.Read()
				If nLastCur <> iReader(0) Then
					Response.Write("<tr>")
					For i As Integer = 0 To nTypes - 1
						Response.Write("<td>" & FormatTotalValue(nLastCur, nCount(i), nAmounts(i), nTax(i)) & "</td>")
						nAmounts(i) = 0
						nTax(i) = 0
						nCount(i) = 0
					Next
					Response.Write("</tr>")
					nLastCur = iReader(0)
				End If
				nCount(iReader(1)) = iReader(2)
				nAmounts(iReader(1)) = iReader(3)
				nTax(iReader(1)) = iReader(4)
				bHasData = True
			End While
			iReader.Close()
			If bHasData Then
				Response.Write("<tr>")
				For i As Integer = 0 To nTypes - 1
					Response.Write("<td>" & FormatTotalValue(nLastCur, nCount(i), nAmounts(i), nTax(i)) & "</td>")
				Next
				Response.Write("</tr>")
			End If
		%>
	</table>
	<%		Response.End()%>
	<%		End If%>
	<iframe id="ifrDelete" style="display: none;"></iframe>
	<form id="frmMain" runat="server">
	<div id="divBillTo" style="display: none; position: absolute; border: 1px solid gray;
		padding: 5px; width: 400px; background-color: rgb(238, 238, 238);" class="formThin">
		<span style="float: left;">
			<asp:TextBox TextMode="MultiLine" ID="txaBillTo" runat="Server" Width="300" Height="50" />
			<span style="display: none;">
				<asp:TextBox ID="hidBillToID" runat="server" Text="" />
			</span></span><span style="float: right;">
				<asp:Button ID="btnBillToUpdate" runat="server" Text="Update" OnClick="UpdateBillTo"
					OnClientClick="if (confirm('Are you sure ?!')) {divBillTo.style.display='none';return true}; else return false;" />
				<input type="button" value="Cancel" style="margin-top: 3px;" onclick="CancelBillTo();" />
			</span>
	</div>
	<table style="width: 95%;">
		<tr>
			<td id="pageMainHeadingTd">
				<asp:Label ID="lblPermissions" runat="server" />
				<span id="pageMainHeading">INVOICES</span> <span class="txt14">
					<%= sSubtitle %></span>
			</td>
			<td style="font-size: 11px; text-align: right;">
				<span style="background-color: #cc99cc;">&nbsp;&nbsp;</span> Manual Issuance <span
					style="background-color: #6699cc;">&nbsp;&nbsp;</span> Transaction Fees
			</td>
		</tr>
	</table>
	<br />
	<%
		If PGData.HasRows Then
	%>
	<table style="width: 95%;">
		<tr>
			<td align="right">
				<table align="right" style="margin: 0; padding: 0;">
					<tr>
						<td>
							<a style="font-size: 9px;" href="?<%=dbPages.SetUrlValue(Request.QueryString.ToString(), "Export", "EXCEL")%>">
								<img src="/NPCommon/Images/save_as_xls.gif" align="middle" /></a>
						</td>
						<td>
							<a style="font-size: 9px;" href="?<%=dbPages.SetUrlValue(Request.QueryString.ToString(), "Export", "EXCEL")%>">
								Excel<br />
								Export</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<%
	End If
	%>
	<table class="formNormal" style="width: 95%;">
		<%
			If PGData.HasRows Then
		%>
		<tr>
			<th>
				&nbsp;&nbsp; Number
			</th>
			<th>
				Date
			</th>
			<th>
				Type
			</th>
			<th style="text-align:right;padding-right:20px;">
				Amount
			</th>
			<th style="text-align:right;padding-right:20px;">
				VAT
			</th>
			<th style="text-align:right;padding-right:20px;">
				Total
			</th>
			<th>
				Merchant
			</th>
			<th>
				Bill To
			</th>
			<th style="text-align: center">
				Original<br />
				Printed
			</th>
			<th style="text-align: center">
				Print
			</th>
			<th style="text-align: center">
				Trans.<br />
				List
			</th>
		</tr>
		<%
			While PGData.Read()
		%>
		<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
			<td style="white-space: nowrap;">
				<span style="background-color: <%= IIf(PGData("id_IsManual"), "cc99cc", "6699cc") %>;">
					&nbsp;&nbsp;</span>
				<%=PGData("id_InvoiceNumber")%>
				<span title="<%=PGData("id_BillingCompanyName").Replace("""", "''") %>">(<%=PGData("BillingCompanyFirstLetter")%>)</span>
			</td>
			<td>
				<%=CType(PGData("id_InsertDate"), DateTime).ToString("dd/MM/yy HH:mm") %>
			</td>
			<td>
				<%=PGData("GD_Text")%>
			</td>
			<td style="text-align:right;padding-right:20px;">
				<%=PGData("Symbol")%><%=CType(PGData("id_TotalLines"), Single).ToString("#,0.00")%>
			</td>
			<td style="text-align:right;padding-right:20px;">
				<%=PGData("Symbol")%><%=CType(PGData("id_TotalVAT"), Single).ToString("#,0.00")%>
			</td>
			<td style="text-align:right;padding-right:20px;">
				<%=PGData("Symbol")%><%=CType(PGData("id_TotalDocument"), Single).ToString("#,0.00")%>
			</td>
			<td title="<%=dbPages.DBTextShow(PGData("MerchantName")).Replace("""", "''")%>">
				<%
					If PGData("MerchantName").ToString.Length < 20 Then
						Response.Write(dbPages.dbtextShow(PGData("MerchantName")))
					Else
						Response.Write(dbPages.dbtextShow(PGData("MerchantName")).ToString.Substring(0, 18) & "...")
					End If
				%>
			</td>
			<%
				Dim sBillTo As String = PGData("id_BillToName").ToString.Replace(Chr(13), " - ").Replace("<br />", " - ").Replace("<br />", " - ")
				Dim sSrc As String = IIf(PGData("id_IsPrinted"), "icon_edit_gray.gif", "icon_edit.gif")
				Dim sOnClick As String = IIf(PGData("id_IsPrinted"), String.Empty, "EditBillTo(" & PGData("ID") & ", '" & sBillTo.Replace("""", "\'\'").Replace(Chr(10), String.Empty) & "');")
				Dim sStyle As String = IIf(PGData("id_IsPrinted"), String.Empty, "cursor:pointer;")
			%>
			<td id="tdBillTo<%= PGData("ID") %>" title="<%=dbPages.DBTextShow(sBillTo).Replace("""", "''").Replace("<br />", Chr(13)) %>">
				<img src="../images/<%= sSrc %>" alt="" style="<%= sStyle %>" onclick="<%= sOnClick %>" />
				<%	
					If sBillTo.Length < 22 Then
						Response.Write(dbPages.dbtextShow(sBillTo))
					Else
						Response.Write(dbPages.dbtextShow(sBillTo).Substring(0, 19) & "...")
					End If
				%>
			</td>
			<td style="text-align: center">
				<img src="/images/checkbox<%=IIf(PGData("id_IsPrinted"), String.Empty, "_off")%>.gif"
					alt="" />
			</td>
			<td style="text-align: center">
				<a href="#" onclick="PrintInvoice(<%= PGData("id_InvoiceNumber") %>, <%= PGData("id_BillingCompanyID") %>, <%= PGData("id_Type") %>);return false;">
					Print</a>
			</td>
			<td style="text-align: center">
				<%
					If PGData("id_TransactionPayID") > 0 Then
				%>
				<a href="#" onclick="ShowTransList(<%= PGData("id_MerchantID") %>, <%= PGData("id_TransactionPayID") %>);return false;">
					<%=PGData("id_TransactionPayID")%></a>
				<%
				End If
				%>
			</td>
		</tr>
		<%
			If PGData("InvoiceOrderNumber") = 1 And Not PGData("id_IsPrinted") Then
		%>
		<tr>
			<td colspan="11" style="font-size: 10px; padding: 2px 0;">
				&nbsp;&nbsp; This is the last
				<%=PGData("GD_Text")%>
				issued by
				<%=PGData("id_BillingCompanyName")%>
				and it never was printed. <a href="#" onclick="if (confirm('Are you sure?!')&&confirm('This operation cannot be undone!!!\n\nDo you really want to delete this document?!')) ifrDelete.location.replace('invoicesGo.aspx?DeleteInvoiceDocument=<%= PGData("ID") %>');return false;">
					<img style="border-width: 0; margin: 0; padding: 0; vertical-align: middle;" src="../images/b_deleteEng.gif"
						alt="Delete <%=PGData("GD_Text")%> #<%=PGData("id_InvoiceNumber")%>" /></a>
			</td>
		</tr>
		<%
		End If
		%>
		<tr>
			<td height="1" colspan="11" bgcolor="silver">
			</td>
		</tr>
		<%
		End While
	Else
		Response.Write("<tr><td style=""border:1px solid silver; padding:5px;"">No records found</td></tr>")
	End If
	PGData.CloseDataset()
		%>
	</table>
	</form>
	<%
		If PGData.HasRows Then
	%>
	<table style="width: 95%;">
		<tr>
			<td>
				<UC:Paging runat="Server" ID="PGData" PageID="PageID" PageSize="20" ShowPageCount="true"
					ShowPageFirst="true" ShowPageLast="true" ShowPageNumber="false" ShowRecordCount="true"
					ShowRecordNumbers="true" />
			</td>
			<td align="right">
				<input type="button" id="TotalsButton" style="font-size: 11px; cursor: pointer;"
					onclick="with(document.getElementById('tblTotals')){style.display='block';if(onresize)onresize();};setAjaxHTML('?<%=Request.QueryString.ToString()%>&Totals=1', document.getElementById('tblTotals'), true);"
					value="SHOW TOTALS &Sigma;">
				<div id="tblTotals" onresize="PosObject(this, document.getElementById('TotalsButton'), 'RRTB', 0, -10);"
					style="padding: 3px; border-color: #484848; border-style: solid; border-width: 1px 3px;
					position: absolute; width: 630px; background-color: White; display: none; font-size: 11px;
					text-align: left;">
					&nbsp; LOADING DATA...
				</div>
			</td>
		</tr>
	</table>
	<%
	End If
	%>
</body>
</html>