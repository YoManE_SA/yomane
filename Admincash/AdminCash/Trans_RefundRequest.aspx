<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.sqlClient" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
	Sub ResetView(ByVal o As Object, ByVal e As EventArgs)
		mvNewRequest.ActiveViewIndex = 0
	End Sub

	Sub AddNewRequest(ByVal o As Object, ByVal e As EventArgs)
		Dim nID, nMerchant, nCurrency As Integer
        nID = dbPages.TestVar(Request("ID"), 1, 0, 0)
        
		nMerchant = dbPages.ExecScalar("SELECT CompanyID FROM tblCompanyTransPass WHERE ID=" & nID)
        nCurrency = dbPages.ExecScalar("SELECT Currency FROM tblCompanyTransPass WHERE ID=" & nID)
		Dim nAmount As Decimal = txtNewAmount.Text
        Dim nAmountMax As Decimal = dbPages.ExecScalar("SELECT dbo.GetRefundAmountAvailable(" & nID & ")")
        If dbPages.TestVar(nAmount, 0D, nAmountMax, 0D) > 0D Then
            Dim sComment As String = dbPages.DBText(txtNewComment.Text)
            Dim nRefund As Integer = dbPages.ExecScalar("EXEC CreateRefundRequest " & nMerchant & ", " & nID & ", " & nAmount & ", " & nCurrency & ", '" & sComment & "', " & rblFeeType.SelectedValue.ToNullableInt().GetValueOrDefault())
            mvNewRequest.ActiveViewIndex = IIf(nRefund > 0, 1, 2)
            gvData.DataBind()
        Else
            txtNewAmount.Text = Decimal.Round(nAmountMax, 2)
            mvNewRequest.ActiveViewIndex = 2
        End If
    End Sub
	
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsData.ConnectionString = dbPages.DSN()
		If Not Page.IsPostBack Then
			Dim nID As Integer = dbPages.TestVar(Request("ID"), 1, 0, 0)
			Page.Title &= nID
			Dim nAmount As Decimal = dbPages.ExecScalar("SELECT dbo.GetRefundAmountAvailable(" & nID & ")")
			txtNewAmount.Text = Decimal.Round(nAmount, 2)
			If nAmount <= 0D Then mvNewRequest.ActiveViewIndex = 3
            txtNewAmount.ReadOnly = Not dbPages.ExecScalar("SELECT dc_isAllowPartialRefund FROM tblDebitCompany WHERE DebitCompany_ID=(SELECT DebitCompanyID FROM tblCompanyTransPass WHERE ID=" & nID & ")")
            Dim terminalItem As Netpay.Bll.Merchants.ProcessTerminals = Nothing
            Dim iRs = dbPages.ExecReader("SELECT CompanyID, Currency, PaymentMethod, IPCountry FROM tblCompanyTransPass WHERE ID=" & nID)
            If iRs.Read() Then terminalItem = Netpay.Bll.Merchants.ProcessTerminals.GetTerminalForTransaction(iRs("CompanyID"), iRs("Currency"), iRs("PaymentMethod"), iRs("IPCountry"))
            iRs.Close()
            If terminalItem IsNot Nothing Then
                rblFeeType.Items.Add(New ListItem(String.Format("Normal ({0})", terminalItem.RefundFixedFee.ToIsoAmountFormat(terminalItem.CurrencyID)), 0))
                rblFeeType.Items.Add(New ListItem(String.Format("Partial ({0})", terminalItem.PartialRefundFixedFee.ToIsoAmountFormat(terminalItem.CurrencyID)), 1))
                rblFeeType.Items.Add(New ListItem(String.Format("Fraud ({0})", terminalItem.FraudRefundFixedFee.ToIsoAmountFormat(terminalItem.CurrencyID)), 2))
            End If
            If rblFeeType.Items.Count > 0 Then rblFeeType.SelectedIndex = 0
        End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Refund Request For Transaction </title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body onload="focus();" style="padding:0 20px;">
	<form id="Form1" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Transaction Management<span> - Refund Requests</span></h1><br />
		<div style="width:100%;">
			<asp:MultiView ID="mvNewRequest" ActiveViewIndex="0" runat="server">
				<asp:View runat="server">
					<fieldset>
						<legend>CREATE NEW REFUND REQUEST</legend>
                            <div style="float:left;padding-right:80px;">
                                &nbsp; Fee:
                                <asp:RadioButtonList runat="server" ID="rblFeeType" />
                            </div>
                            <div style="float:left;">
							    Amount <br /><asp:TextBox ID="txtNewAmount" CssClass="text short" runat="server" />
                                <br />
							    Comment <br /><asp:TextBox ID="txtNewComment" CssClass="text" runat="server" />
							    <asp:Button UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" Text="Add" OnClick="AddNewRequest" />
                            </div>
					</fieldset>
				</asp:View>
				<asp:View runat="server">
					<fieldset>
						<legend>CREATE NEW REFUND REQUEST</legend>
							The refund request has been created successfully. &nbsp; 
							<input type="button" class="buttonWhite" value="Close" onclick="top.close();" />
					</fieldset>
				</asp:View>
				<asp:View runat="server">
					<fieldset>
						<legend>CREATE NEW REFUND REQUEST</legend>
							The refund request was not created! &nbsp; 
							<asp:Button UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" Text="Back" OnClick="ResetView" />
					</fieldset>
				</asp:View>
				<asp:View runat="server">
					<fieldset>
						<legend>CREATE NEW REFUND REQUEST</legend>
							Full refund has already requested.
					</fieldset>
				</asp:View>
			</asp:MultiView>
		</div>
		<br />
		<asp:GridView ID="gvData" CssClass="grid" DataSourceID="dsData" AutoGenerateColumns="true" runat="server" Width="100%" />
		<asp:SqlDataSource ID="dsData" runat="server" SelectCommand="SELECT * FROM GetRefundRequests(@nTransID) ORDER BY ID DESC">
			<SelectParameters>
				<asp:QueryStringParameter QueryStringField="ID" Name="nTransID" Type="Int32" ConvertEmptyStringToNull="true" />
			</SelectParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>
