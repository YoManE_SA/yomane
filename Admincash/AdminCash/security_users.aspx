<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
    '
    ' 20080512 Tamir
    '
    ' Security management
    '
    Sub Page_Load()
        Security.CheckPermission(lblPermissions)
        dsUsers.ConnectionString = dbPages.DSN()
        gvUsers.SelectedIndex=-1
    End Sub

    Sub ClearBody()
        Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
        Response.Write("parent.frmBody.location.replace('common_blank.htm');")
        Response.Write("</s" & "cript>")
    End Sub

    Sub ReloadPage()
        Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
        Response.Write("location.replace('security_users.aspx');")
        Response.Write("</s" & "cript>")
    End Sub

    Sub SelectUser(ByVal nUser As Integer)
        For i As Integer = 0 To gvUsers.Rows.Count - 1
            If gvUsers.Rows(i).Cells(0).Text = nUser Then gvUsers.SelectedIndex = i
        Next
    End Sub

    Sub gvUsers_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs)
        dbPages.ExecSql("Delete From tblPasswordHistory Where LPH_RefID=" & e.Keys(0) & " And LPH_RefType IN(" & eUserType.eUT_NetpayUser & "," & eUserType.eUT_NetpayAdmin & ")")
        ClearBody()
        ReloadPage()
    End Sub

    Sub btnAddUser_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If String.IsNullOrEmpty(txtNewUser.Text) Then
            lblAddUser.Text = "Specify new username!"
        Else
            Dim secUser = new Netpay.Infrastructure.Security.AdminUser()
            lblAddUser.Text = String.Empty
            secUser.FullName = txtNewUser.Text
            secUser.NotifyEmail = txtNewUser.Text
            secUser.Login.UserName = txtNewUser.Text
            secUser.Login.EmailAddress = txtNewUser.Text
            secUser.Save()
            dbPages.ExecSql("INSERT INTO tblSecurityUser(ID, su_Username, su_Name) VALUES (" &  secUser.ID & ", '" & dbPages.DBText(txtNewUser.Text) & "', '" & dbPages.DBText(txtNewUser.Text) & "');")
            txtNewUser.Text = String.Empty
            gvUsers.DataBind()
        End If
    End Sub

    Sub gvUsers_RowUpdated(ByVal sender As Object, ByVal e As GridViewUpdatedEventArgs)
        'If (e.NewValues("su_IsAdmin") <> e.OldValues("su_IsAdmin")) Then _
        '    dbPages.ExecSql("Update tblPasswordHistory Set LPH_RefType=" & IIf(e.NewValues("su_IsAdmin"), eUserType.eUT_NetpayAdmin, eUserType.eUT_NetpayUser) & " Where LPH_RefID=" & e.Keys(0) & " And LPH_RefType IN(" & eUserType.eUT_NetpayUser & "," & eUserType.eUT_NetpayAdmin & ")")
    End Sub

    Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gvUsers.DataBind()
        txtNewUser.Text = String.Empty
    End Sub

    Protected Sub SetDeleteConfirmation(ByVal o As Object, ByVal e As GridViewRowEventArgs) Handles gvUsers.RowDataBound
        NPControls.SetGridViewDeleteConfirmation(e)
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security - Users</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<style type="text/css">.RowGrid { border-bottom:solid 1px rgb(192,192,192); }</style>
</head>
<body class="menu">
	<table width="95%" align="center">
	<tr>
		<td>
			<span class="toolbar">
				<a class="toolbarCurrent" href="security_users.aspx" onclick="parent.frmBody.location.replace('common_blank.htm');">Users</a>
				<span class="toolbarSeparator">|</span>
				<a class="toolbar" href="security_groups.aspx" onclick="parent.frmBody.location.replace('common_blank.htm');">Groups</a>
				<span class="toolbarSeparator">|</span>
				<a class="toolbar" href="security_documents.aspx" onclick="parent.frmBody.location.replace('common_blank.htm');">Documents</a>
			</span>
		</td>
	</tr>
	</table>
	<form id="frmServer" runat="server">
	<table width="95%" align="center">
	<tr>
		<td>
			<h1><asp:label ID="lblPermissions" runat="server" /> SECURITY<span> - USERS</span></h1>
			<br />
			<div>
				<span style="float:right;padding-bottom:1px;">
					<asp:Button ID="btnRefresh" UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
				</span>
				Add new user
				<asp:TextBox ID="txtNewUser" CssClass="text" runat="server" />
				<asp:Button ID="btnAddUser" UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" Text="Add" OnClick="btnAddUser_Click" />
				<asp:Label ID="lblAddUser" runat="server" ForeColor="red" />
			</div>
		</td>
	</tr>
	</table>
	<table width="95%" align="center">
	<tr>
		<td>
			<asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="dsUsers" AllowPaging="False" AllowSorting="False" SelectedRowStyle-CssClass="rowSelected" OnRowUpdated="gvUsers_RowUpdated" OnRowDeleted="gvUsers_RowDeleted" Width="100%" BorderWidth="0" GridLines="None">
				<Columns>
					<asp:HyperLinkField HeaderText="ID" DataTextField="ID" DataNavigateUrlFormatString="security_user.aspx?User={0}" DataNavigateUrlFields="ID" Target="frmBody" runat="server" />
					<asp:BoundField DataField="su_Username" HeaderText="User&nbsp;Name" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="medium"/>
					<asp:BoundField DataField="su_Name" HeaderText="Full Name" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="medium" />
					<asp:BoundField DataField="su_Mail" HeaderText="Mail Regular" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="medium2" />
					<asp:BoundField DataField="su_MailEmergency" HeaderText="Mail Notifications" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="medium2" />
					<asp:BoundField DataField="su_SMS" HeaderText="Cell Num." HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="medium" />
					<asp:CheckBoxField DataField="su_IsActive" HeaderText="Active" ItemStyle-CssClass="centered" HeaderStyle-CssClass="columnTitleVertical" />
					<asp:CheckBoxField DataField="su_IsLogged" HeaderText="Logged" ItemStyle-CssClass="centered" HeaderStyle-CssClass="columnTitleVertical" />
					<asp:CheckBoxField DataField="su_IsAdmin" HeaderText="Admin" ItemStyle-CssClass="centered" HeaderStyle-CssClass="columnTitleVertical" />
					<asp:HyperLinkField HeaderText="Groups" HeaderStyle-CssClass="columnTitleVertical" DataTextFormatString="{0}" DataTextField="GroupCount"
					 DataNavigateUrlFormatString="security_userGroups.aspx?ID={0}" DataNavigateUrlFields="ID" Target="frmBody"
					  ItemStyle-CssClass="gridEditLink" runat="server" />
					<asp:HyperLinkField HeaderText="Merchants" HeaderStyle-CssClass="columnTitleVertical" DataTextFormatString="{0}" DataTextField="MerchantCount"
					 DataNavigateUrlFormatString="security_merchants.aspx?User={0}" DataNavigateUrlFields="ID" Target="frmBody"
					  ItemStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center" runat="server" />
					<asp:HyperLinkField HeaderText="Debit Companies" HeaderStyle-CssClass="columnTitleVertical" DataTextFormatString="{0}" DataTextField="DebitCompanyCount"
					 DataNavigateUrlFormatString="security_debitCompanies.aspx?User={0}" DataNavigateUrlFields="ID" Target="frmBody"
					  ItemStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center" runat="server" />
					<asp:CommandField HeaderText="Actions" ButtonType="Button" ItemStyle-CssClass="buttons" ControlStyle-CssClass="buttonWhite" ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="False" HeaderStyle-CssClass="actions" UpdateText="Save" />
				</Columns>
				<SelectedRowStyle CssClass="rowSelected" />
			</asp:GridView>
			<asp:SqlDataSource ID="dsUsers" runat="server" ProviderName="System.Data.SqlClient"
			 SelectCommand="SELECT (SELECT Count(ID) FROM tblSecurityUserGroup WHERE sug_IsMember=1 AND sug_User=tblSecurityUser.ID) GroupCount,
							(SELECT Count(ID) FROM tblSecurityUserMerchant WHERE sum_User=tblSecurityUser.ID) MerchantCount,
							(SELECT Count(ID) FROM tblSecurityUserDebitCompany WHERE sudc_User=tblSecurityUser.ID) DebitCompanyCount,
							 * FROM tblSecurityUser ORDER BY su_Username"
			   DeleteCommand="DELETE FROM tblSecurityUser WHERE ID=@ID;DELETE FROM [System].[AdminUser] WHERE AdminUser_id=@ID;"
			   UpdateCommand="UPDATE tblSecurityUser SET su_Name = IsNull(@su_Name, ''), su_Username = IsNull(@su_Username, ''),
				su_Mail = IsNull(@su_Mail, ''), su_MailEmergency = IsNull(@su_MailEmergency, ''), su_SMS = IsNull(@su_SMS, ''), su_IsActive = @su_IsActive,
				 su_IsAdmin = @su_IsAdmin, su_IsLogged = @su_IsLogged WHERE ID = @ID">
				<DeleteParameters>
					<asp:Parameter Name="ID" Type="Int32" />
				</DeleteParameters>
				<UpdateParameters>
					<asp:Parameter Name="su_Name" Type="String" />
					<asp:Parameter Name="su_Username" Type="String" />
					<asp:Parameter Name="su_Mail" Type="String" />
					<asp:Parameter Name="su_MailEmergency" Type="String" />
					<asp:Parameter Name="su_SMS" Type="String" />
					<asp:Parameter Name="su_IsActive" Type="Boolean" />
					<asp:Parameter Name="su_IsAdmin" Type="Boolean" />
					<asp:Parameter Name="su_IsLogged" Type="Boolean" />
					<asp:Parameter Name="ID" Type="Int32" />
				</UpdateParameters>
			</asp:SqlDataSource>
		</td>
	</tr>
	</table>
	</form>
</body>
</html>