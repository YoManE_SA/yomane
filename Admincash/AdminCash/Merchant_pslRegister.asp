<!--#include file="../include/func_adoConnect.asp"-->
<%
If trim(request("Action"))="Register" then

	XmlStr = replace(trim(request("XmlString")),vbCrLf,"")

	'--------------------------------------------------------------------
	'	Send data
	'--------------------------------------------------------------------
	UrlAddress = "https://pslinterface.int.waltonmobile.com:444/pslinterface/sentry" 'TEST
	'UrlAddress = "https://pslinterface.waltonmobile.com:443/pslinterface/sentry" 'PRODUCTION
	
	Set SrvHTTPS = Server.CreateObject("MSXML2.ServerXMLHTTP")
	SrvHTTPS.open "POST",UrlAddress,false
	SrvHTTPS.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
	SrvHTTPS.send "PAYLOAD=" & XmlStr
	
	responseStr = SrvHTTPS.responseText
	response.write responseStr
	response.end
	
Elseif trim(request("Action"))="Update" then

	sSQL="UPDATE tblCompany SET PslWalletId='" & trim(request("PslWalletId")) & "', PslWalletCode='" & trim(request("PslWalletCode")) & "' WHERE ID=" & trim(request("companyID"))
	oledbData.execute sSQL
	
End if

sSQL="SELECT dbo.GetDecrypted256(LPH_Password256) Password256, tblCompany.* FROM tblCompany " & _
	" INNER JOIN tblPasswordHistory ON tblCompany.ID=tblPasswordHistory.LPH_RefID " & _
	" WHERE LPH_ID=1 AND LPH_RefType=2 AND tblCompany.ID=" & request("companyID")
set rsData = oledbData.execute(sSQL)
sIsoCode = ExecScalar("SELECT CountryISOCode FROM [List].[CountryList] WHERE CountryID = " & trim(rsData("companyCountry")), "")

'--------------------------------------------------------------------
'	Building string to send
'--------------------------------------------------------------------
sAccountId = "NetPay001"
sAccountSubId = "netpay001"
sAccountPassword = "Bu51n355C4Rd"
		
Dim XmlStr
XmlStr = "<?xml version=""1.0""?>" & vbCrLf
XmlStr = XmlStr & "<psldoc>" & vbCrLf
XmlStr = XmlStr & "  <merchant>" & vbCrLf
XmlStr = XmlStr & "    <merchant-id>" & sAccountId & "</merchant-id>" & vbCrLf
XmlStr = XmlStr & "    <merchant-account>" & sAccountSubId & "</merchant-account>" & vbCrLf
XmlStr = XmlStr & "    <merchant-passcode>" & sAccountPassword & "</merchant-passcode>" & vbCrLf
XmlStr = XmlStr & "  </merchant>" & vbCrLf
XmlStr = XmlStr & "  <request type=""register"">" & vbCrLf
XmlStr = XmlStr & "    <merchant-ref>2006</merchant-ref>" & vbCrLf
XmlStr = XmlStr & "    <remote-address>80.179.39.10</remote-address>" & vbCrLf
XmlStr = XmlStr & "    <client>" & vbCrLf
XmlStr = XmlStr & "      <account>" & vbCrLf
XmlStr = XmlStr & "        <name>" & trim(rsData("Mail")) & "</name>" & vbCrLf
XmlStr = XmlStr & "        <password>" & trim(rsData("Password256")) & "</password>" & vbCrLf
XmlStr = XmlStr & "        <challenge>favorite payment system</challenge>" & vbCrLf
XmlStr = XmlStr & "        <response>Netpay</response>" & vbCrLf
XmlStr = XmlStr & "      </account>" & vbCrLf
XmlStr = XmlStr & "      <identity>" & vbCrLf
XmlStr = XmlStr & "        <first-name>" & trim(rsData("FirstName")) & "</first-name>" & vbCrLf
XmlStr = XmlStr & "        <last-name>" & trim(rsData("LastName")) & "</last-name>" & vbCrLf
XmlStr = XmlStr & "        <gender>Male</gender>" & vbCrLf
XmlStr = XmlStr & "        <address>" & vbCrLf
XmlStr = XmlStr & "          <street-name>" & trim(rsData("CompanyStreet")) & "</street-name>" & vbCrLf
XmlStr = XmlStr & "          <postal-town>" & trim(rsData("CompanyCity")) & "</postal-town>" & vbCrLf
XmlStr = XmlStr & "          <postal-code>" & trim(rsData("CompanyZIP")) & "</postal-code>" & vbCrLf
XmlStr = XmlStr & "          <country>" & sIsoCode & "</country>" & vbCrLf
XmlStr = XmlStr & "        </address>" & vbCrLf
XmlStr = XmlStr & "        <nationality>" & vbCrLf
XmlStr = XmlStr & "          <country-of-birth>IL</country-of-birth>" & vbCrLf
XmlStr = XmlStr & "        </nationality>" & vbCrLf
XmlStr = XmlStr & "        <birth-details>" & vbCrLf
XmlStr = XmlStr & "          <date>1975-04-03</date>" & vbCrLf
XmlStr = XmlStr & "        </birth-details>" & vbCrLf
XmlStr = XmlStr & "        <contact-details>" & vbCrLf
XmlStr = XmlStr & "          <email>" & vbCrLf
XmlStr = XmlStr & "          <type>Business</type>" & vbCrLf
XmlStr = XmlStr & "          <address>" & trim(rsData("Mail")) & "</address>" & vbCrLf
XmlStr = XmlStr & "          </email>" & vbCrLf
XmlStr = XmlStr & "        </contact-details>" & vbCrLf
XmlStr = XmlStr & "      </identity>" & vbCrLf
XmlStr = XmlStr & "    </client>" & vbCrLf
XmlStr = XmlStr & "  </request>" & vbCrLf
XmlStr = XmlStr & "</psldoc>" & vbCrLf
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
	<link href="../StyleSheet/StyleAdminIE.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<table align="center" border="0" cellpadding="1" cellspacing="0" width="600">
<tr>
	<td align="left" valign="top" class="txt14">
		<span class="txt16b" style="color:#FF8000;">Merchant - Wallet Signup</span><br />
	</td>
	<td align="right" valign="top">
		[ <a class="submenu2" style="text-decoration:underline;" href="merchant_data.asp?ShowTRSec=4&companyID=<%= request("companyID") %>">Back to merchant data</a> ]<br />
	</td>
</tr>
<tr>
	<td colspan="2">
		<br />
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" dir="ltr">
		<form name="frmCodePay" method="post" action="<%= session("TempOpenURL") %>AdminCash/Merchant_pslRegister.asp" target="fraShoppingPurchase" onsubmit="window.open('', this.target, 'scrollbars=1, width=490, height=600, resizable=0, Status=1, top=200, left=250');">
		<input type="Hidden" name="companyID" value="<%= request("companyID") %>">
		<tr>
			<td align="right">
				<%
				sSQL="SELECT * FROM tblDebitTerminals WHERE DebitCompany=7 AND isActive=1 ORDER BY dt_name asc"
				set rsData3 = oledbData.execute(sSQL)
				If NOT rsData3.EOF Then
					response.write "<select name="""">"
					do until rsData3.EOF
					%>
					<option><%= dbtextShow(rsData3("dt_name")) & "  " & rsData3("TerminalNumber") %>
					<%
					rsData3.movenext
					loop
					response.write "</select>"
				End If
				rsData3.close
				Set rsData3 = nothing
				%>
			</td>
		</tr>
		<tr>
		    <td align="left" class="txt12" valign="top">
				<textarea name="XmlString" dir="ltr" style="width:100%; height:300px; font-size:12px;"><%= XmlStr %></textarea><br />
			</td>
		</tr>
		<tr>
			<td align="right">
				<input type="submit" name="Action" value=" Register " style="background-color:white;font-size:11px;width:90px;cursor:pointer;"><br />
			</td>
		</tr>
		</form>
		<tr><td><br /></td></tr>
		<tr><td height="1" bgcolor="#C0C0C0"></td></tr>
		<tr><td height="12"></td></tr>
		<tr>
			<form name="frmCodePay" method="post" action="<%= session("TempOpenURL") %>AdminCash/Merchant_pslRegister.asp">
			<input type="Hidden" name="companyID" value="<%= request("companyID") %>">
			<td align="left">
				wallet-id
				<input name="PslWalletId" value="<%= trim(rsData("PslWalletId")) %>" size="22"> &nbsp;&nbsp;
				wallet-code
				<input name="PslWalletCode" value="<%= trim(rsData("PslWalletCode")) %>" size="13"> &nbsp;&nbsp;
				<input type="submit" name="Action" value="Update" style="background-color:white;font-size:11px;width:40px;cursor:pointer;">
			</td>
			</form>
		</tr>
		<tr><td height="12"></td></tr>
		<tr><td height="1" bgcolor="#C0C0C0"></td></tr>
		</table>
		<br />
	</td>
</tr>
</table>
<% call closeConnection() %>
</body>
</html>