<%@ Page Language="VB" EnableViewState="true" %>
<%@ Register Src="TabMenu.ascx" TagPrefix="UC1" TagName="TabMenu" %>
<%@ Register Src="BankAccount.ascx" TagPrefix="UC1" TagName="MerchantBankAccount" %>
<%@ Register Src="BankAccountLocal.ascx" TagPrefix="UC1" TagName="MerchantBankAccountLocal" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<%@ Import namespace="System.Data.SqlClient" %>
<script runat="server">
	Private nMerchant As Integer, bErrorsCleared As Boolean = False

	Sub Page_Init()
		nMerchant = dbPages.TestVar(Request("ID"), 1, 0, 0)
		If nMerchant < 1 Then Throw New Exception("Invalid merchant ID specified: " & Request("ID").ToString)
	End Sub
	
	Sub ClearErrors()
		If Not bErrorsCleared Then
			lblError.Text = String.Empty
			lblNoError.Text = String.Empty
		End If
		bErrorsCleared = True
	End Sub

	Sub ProcessData(Optional ByVal o As Object = Nothing, Optional ByVal e As CommandEventArgs = Nothing)
		ClearErrors()
		mba1.Currency = rblCurrency.SelectedItem.Value
		mba1.Merchant = nMerchant
		mba1.IsCorrespondent = False
		mba2.Currency = rblCurrency.SelectedItem.Value
		mba2.Merchant = nMerchant
		mba2.IsCorrespondent = True
		mbaL.Merchant = nMerchant
		If e.CommandName.ToUpper = "DELETE" Then
			mba1.RemoveFromDB()
			lblNoError.Text &= "Primary bank account (" & rblCurrency.SelectedItem.Text & ") has been deleted.<br />"
			mba2.RemoveFromDB()
			lblNoError.Text &= "Correspondent bank account (" & rblCurrency.SelectedItem.Text & ") has been deleted.<br />"
			SelectCurrency() 'goes to the first non-empty account
		Else
			If String.IsNullOrEmpty(mba1.AccountName) Then
				lblError.Text &= "Primary bank account (" & rblCurrency.SelectedItem.Text & ") cannot be saved: missing Account Name!<br />"
			Else
				If rblCurrency.SelectedItem.Value >= 0 Then
					mba1.SaveToMerchant()
					lblNoError.Text &= "Primary bank account (" & rblCurrency.SelectedItem.Text & ") has been saved.<br />"
				End If
				If e.CommandArgument.ToString.ToUpper = "DEFAULT" Then
					mba1.SaveToDefault()
					lblNoError.Text &= "Primary bank account (DEFAULT) has been saved.<br />"
				End If
				If rblCurrency.SelectedItem.Value < 0 Then mba1.LoadFromDefault() Else mba1.LoadFromMerchant()
			End If
			If String.IsNullOrEmpty(mba2.SwiftNumber) And Not mba2.IsEmpty Then
				lblError.Text &= "Correspondent bank account (" & rblCurrency.SelectedItem.Text & ") cannot be saved: missing Swift Number!<br />"
			ElseIf mba1.IsEmpty And mba2.IsEmpty Then
			Else
				If rblCurrency.SelectedItem.Value >= 0 Then
					mba2.SaveToMerchant()
					lblNoError.Text &= "Correspondent bank account (" & rblCurrency.SelectedItem.Text & ") has been saved.<br />"
				End If
				If e.CommandArgument.ToString.ToUpper = "DEFAULT" Then
					mba2.SaveToDefault()
					lblNoError.Text &= "Correspondent bank account (DEFAULT) has been saved.<br />"
				End If
				If rblCurrency.SelectedItem.Value < 0 Then mba2.LoadFromDefault() Else mba2.LoadFromMerchant()
			End If
			If mbaL.Visible Then
				mbaL.SaveToMerchant()
				lblNoError.Text &= "Local bank account has been saved.<br />"
			End If
		End If
		Dim nCurrency As Integer = rblCurrency.SelectedValue
		dsCurrency.ConnectionString = dbPages.DSN
		rblCurrency.DataBind()
		rblCurrency.SelectedValue = nCurrency
	End Sub

	Sub LoadBankAccountLocal()
		mbaL.Visible = False
		If rblCurrency.SelectedItem.Value <= 0 Then
			mbaL.Merchant = nMerchant
			mbaL.Visible = True
			mbaL.LoadFromMerchant()
		End If
	End Sub
	
	Sub LoadBankAccount(ByVal mba As MerchantBankAccount, ByVal bIsCorrespondent As Boolean)
		rblCurrency.SelectedIndex = dbPages.TestVar(rblCurrency.SelectedIndex, 0, rblCurrency.Items.Count - 1, 0)
		mba.Currency = rblCurrency.SelectedItem.Value
		mba.Merchant = nMerchant
		mba.IsCorrespondent = bIsCorrespondent
		If mba.Currency < 0 Then mba.LoadFromDefault() Else mba.LoadFromMerchant()
	End Sub
	
	Sub SelectCurrency(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		LoadBankAccount(mba1, False)
		LoadBankAccount(mba2, True)
		LoadBankAccountLocal()
		btnDelete.Visible = (rblCurrency.SelectedItem.Value >= 0)
		btnSave.Visible = btnDelete.Visible
		btnSaveDefault.Visible = Not btnSave.Visible
	End Sub

	Protected Sub Page_Load()
		If Not Page.IsPostBack Then
			dsCurrency.ConnectionString = dbPages.DSN
			rblCurrency.DataBind()
			SelectCurrency()
		End If
		btnSave.OnClientClick = "if (!CheckFormBankAccount" & mba1.ClientID & "()) return false;if (!CheckFormBankAccount" & mba2.ClientID & "()) return false;"
		btnSaveDefault.OnClientClick = btnSave.OnClientClick
	End Sub
</script>
<html>
<head runat="server">
	<title>Merchant Bank Accounts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<script src="../js/func_common.js" type="text/jscript"></script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<form runat="server">
	<UC1:TabMenu Width="95%" runat="server" idFieldName="ID" />
	<table border="0" class="formNormal" cellspacing="0" align="center" width="95%">
	<tr>
		<td width="170" bgcolor="#f5f5f5" valign="top">
			<br />
			<table border="0" cellpadding="1" cellspacing="0" class="formNormal">
				<tr>
					<td>
						Select account to edit:
						<AC:DivBox ID="divbHelp" Width="475px" runat="server">
							<ul>
								<li>For settlement in currency having NO separate bank account specified for it:
									<ul>
										<li>
											Wire is generated in the default currency<br />
											(set it in the "FEES &amp; PAYOUT" page).
										</li>
										<li>Money is transferred to the default account.</li>
									</ul>
								</li>
								<li>For settlement in currency having separate bank account specified for it:
									<ul>
										<li>Wire is generated in that currency.</li>
										<li>Money is transferred to its separate account.</li>
									</ul>
								</li>
							</ul>
						</AC:DivBox>
					</td>
				</tr>
				<tr>
					<td><br />
						<asp:SqlDataSource ID="dsCurrency" SelectCommand="SELECT * FROM GetMerchantBankAccountList(@nMerchant) ORDER BY ID" runat="server">
							<SelectParameters>
								<asp:QueryStringParameter QueryStringField="ID" Name="nMerchant" />
							</SelectParameters>
						</asp:SqlDataSource>
						<asp:RadioButtonList ID="rblCurrency" DataSourceID="dsCurrency" DataValueField="ID" DataTextField="Name"
							OnSelectedIndexChanged="SelectCurrency" AutoPostBack="true" RepeatLayout="Flow" RepeatDirection="Vertical"
							CssClass="option" runat="server" />
					</td>
				</tr>
			</table>
			<br />
		</td>
		<td style="padding-left:30px;">
			<div class="normal" style="width:95%;">
				<span style="float:left;">
					<UC1:MerchantBankAccount ID="mba1" runat="server" />
					<br />
					<br />
					<UC1:MerchantBankAccountLocal ID="mbaL" Visible="false" runat="server" />
				</span>
				<span style="float:right;">
					<UC1:MerchantBankAccount ID="mba2" runat="server" />
				</span>
			</div>
			<br />
			<div class="normal" style="width:95%;">
				<asp:Label ID="lblNoError" CssClass="noErrorMessage" runat="server" />
				&nbsp;
				<asp:Label ID="lblError" CssClass="errorMessage" runat="server" />
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="text-align:right;">
			<hr class="separator" />
			<asp:Button ID="btnDelete" Text="Delete" OnCommand="ProcessData" CommandName="DELETE" CssClass="buttonWhite buttonDelete"
				OnClientClick="if (!confirm('ARE YOU SURE ?!')) return false;" UseSubmitBehavior="false" runat="server" />
			<asp:Button ID="btnSave" Text="Save" OnCommand="ProcessData" CommandName="UPDATE"
				CssClass="buttonWhite" UseSubmitBehavior="false" runat="server" />
			<asp:Button ID="btnSaveDefault" Text="Save Default" OnCommand="ProcessData" CommandName="UPDATE" CommandArgument="DEFAULT"
				CssClass="buttonWhite" UseSubmitBehavior="false" runat="server" />
		</td>
	</tr>
	</table>
</form>
</body>
</html>