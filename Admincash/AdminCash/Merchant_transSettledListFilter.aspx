<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = DateAdd("m", -1, Date.Now).ToShortDateString
        fdtrControl.ToDateTime = DateAdd("d", 7, Date.Now).ToShortDateString
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
	<form runat="server" action="Merchant_transSettledListData.asp" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" id="Source" name="Source" value="filter" />
	<input type="hidden" id="ShowCompanyID" name="ShowCompanyID" />

	<h1><asp:Label ID="lblPermissions" runat="server" /> Settlements<span> - Filter</span></h1>

	<table class="filterNormal">
	<tr><td><Uc1:FilterMerchants id="FMerchants" SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
			 INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Settlement #</th></tr>
		<tr>
			<td>
				<input type="text" class="input1" size="5" dir="ltr" name="IdFrom" value=""> &nbsp;
				To <input type="text" class="input1" size="5" dir="ltr" name="IdTo" value="">
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Amount</th></tr>
		<tr>
			<td>
				<input type="text" class="input1" size="5" dir="ltr" name="amountFrom" value=""> <%'= GetCurText(GC_NIS) %> &nbsp;
				To <input type="text" class="input1" size="5" dir="ltr" name="amountTo" value=""> <%'= GetCurText(GC_NIS) %>
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Shown To Merchant</th></tr>
		<tr>
			<td>
				<input type="radio" name="ShowToMerchant" value="" checked="checked" />All
				<input type="radio" name="ShowToMerchant" value="1" />Yes
				<input type="radio" name="ShowToMerchant" value="0" />No
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Bank Transfer</th></tr>
		<tr>
			<td>
				<input type="radio" name="WireFlag" value="" checked="checked" />All
				<input type="radio" name="WireFlag" value="1" />Yes
				<input type="radio" name="WireFlag" value="0" />No
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td>
				<select name="PageSize">
					<option value="20" selected="selected">20 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td align="right">
				<input type="submit" name="Action" onclick="this.form.action='Merchant_transSettledListData.asp'" value="SEARCH"> &nbsp;&nbsp;
				<input type="submit" name="Action" onclick="this.form.action='Merchant_transSettledListExcel.aspx'" value="XSL">
			</td>
		</tr>
	</table>
	</form>
</asp:Content>