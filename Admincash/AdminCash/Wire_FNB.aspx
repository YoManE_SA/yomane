<%@ Page Language="VB" AspCompat="true" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    Sub SetContinueLink()
        hlContinue.NavigateUrl = "Wire_search_data.asp?"
        Dim bFirst As Boolean = True
        For Each sItem As String In Request.QueryString.Keys
            If bFirst Then
                bFirst = False
            Else
                hlContinue.NavigateUrl &= "&"
            End If
            hlContinue.NavigateUrl &= sItem & "=" & Request.QueryString(sItem)
        Next
    End Sub

    Function SubmitFNBData(settlementDate As DateTime, sourceAccount As String) As String
        Dim wireIds As New System.Collections.Generic.List(Of Integer), nCurrency As Integer
        For Each sItem As String In Request.Form.Keys
            If sItem.StartsWith("wireAction") And Request.Form(sItem) = "9" Then
                Dim nWire = dbPages.TestVar(sItem.Substring(10), 1, 0, 0)
                If nWire > 0 Then
                    litLog.Text &= "FNB: Wire " & nWire & "<br />"
                    nCurrency = dbPages.TestVar(Request.Form("currencyToUse" & nWire), 1, 0, -1)
                    If nCurrency >= 0 Then
                        litLog.Text &= "&nbsp; &nbsp; &nbsp; Currency: " & dbPages.GetCurText(nCurrency) & "<br />"
                        If nCurrency = eCurrencies.GC_ZAR Then
                            wireIds.Add(nWire)
                            litLog.Text &= "&nbsp; &nbsp; &nbsp; Record has been added successfully.</span><br />"
                        Else
                            litLog.Text &= "<span class=""errorMessage"">&nbsp; &nbsp; &nbsp; This currency is invalid for FNB!</span><br />"
                        End If
                    End If
                End If
            End If
        Next
        Server.ScriptTimeout = 60 * 10
        Dim wires = Netpay.Bll.Wires.Wire.Load(wireIds)
        Netpay.Bll.Wires.Wire.Commit(wires, settlementDate, "System.FNB", sourceAccount)
        Return Netpay.Bll.Wires.Provider.Get("System.FNB").LastRelatedBatchFiles(0)
    End Function

    Sub Page_Load()
        If Not Page.IsPostBack Then
            litLog.Text = String.Empty
            SetContinueLink()
            Dim sSQL As String = dbPages.ExecScalar("SELECT IsNull(AccountNumber, '') FROM [Finance].[WireAccount] WHERE AccountNumber='" & dbPages.TestVar(Request.Form("FNBAccount"), 25, "") & "'")
            Dim sAccount As String = sSQL
            If String.IsNullOrEmpty(sAccount) Then
                litLog.Text &= "<span class=""errorMessage"">No valid FNB account selected!</span> " & Request.Form("FNBAccount") & "<br />"
            Else
                litLog.Text &= "FNB account: " & sAccount & "<br />"
                Dim nYear As Integer = dbPages.TestVar(Request.Form("FNBYear"), Date.Today.Year, Date.Today.Year + 1, 0)
                Dim nMonth As Integer = dbPages.TestVar(Request.Form("FNBMonth"), 1, 12, 0)
                Dim nDay As Integer = dbPages.TestVar(Request.Form("FNBDay"), 1, 31, 0)
                If nYear = 0 Or nMonth = 0 Or nDay = 0 Then
                    litLog.Text &= "<span class=""errorMessage"">No valid date specified!</span> " & nDay & "/" & nMonth & "/" & nYear & "<br />"
                Else
                    Dim dSubmission As New Date(nYear, nMonth, nDay)
                    If dSubmission < Date.Today Then
                        litLog.Text &= "<span class=""errorMessage"">Past date specified!</span> " & dSubmission.ToShortDateString & "<br />"
                    Else
                        Dim sFile = SubmitFNBData(dSubmission, Request.Form("FNBAccount"))
                        litLog.Text &= "File saved: " & sFile
                    End If
                End If
            End If
        End If
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Wire FNB</title>
	<link type="text/css" rel="stylesheet" href="../StyleSheet/StyleAdminNetEng.css" />
</head>
<body>
	<form runat="server">
		<div style="font-size:12px;">
			<asp:Literal ID="litLog" runat="server" />
			<hr />
			<asp:HyperLink ID="hlContinue" Text="Continue" runat="server" />
		</div>
	</form>
</body>
</html>