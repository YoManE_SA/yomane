﻿<%@ Page Language="C#" ResponseEncoding="windows-1255" %>
<script runat="server">  
    private Netpay.Infrastructure.Security.Login.TokenStatus credentialsStatus;
    protected override void OnPreRender(EventArgs e)
    {
        Netpay.Infrastructure.Security.Login.Get(ObjectContext.Current.CredentialsToken, out credentialsStatus);
        if (!IsPostBack) DataBind();
        base.OnPreRender(e);
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head serverip="<%# Request.ServerVariables["LOCAL_ADDR"] %>" userInfo="<%# ObjectContext.Current.CredentialsToken %>: <%# credentialsStatus %>, HTTP: <%# Request.ServerVariables["LOGON_USER"] %>">
	<title><%# Domain.Current.BrandName %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminIE.css" type="text/css" rel="stylesheet" />
	<style type="text/css">
		body { margin:0; }
		select { background-color:#d5d5d5; width:118px; font-family:Arial;}
	</style>
	<script language="JavaScript" type="text/javascript">
		function selectAction(sName) {
			selectName = document.getElementById(sName)
			if (selectName.value != '') { top.fraBrowser.location.href = (selectName.value); }
			selectName.options[0].selected = true;
		}
		function selectActionDefault(selectName, indexNum) {
			if (selectName.options[indexNum].value != '') { top.fraBrowser.location.href = (selectName.options[indexNum].value); }
			selectName.options[0].selected = true;
		}
		function toggleToolBar() {
			if (top.document.getElementById('fraOuter').rows == '68,2,*,2,20') {
				top.document.getElementById('fraOuter').rows = '100,2,*,2,20';
				document.getElementById('QuickBar_CcNumber').value = '';
				document.getElementById('QuickBar_TransID').value = '';
				document.getElementById('iTextSearch').value = '';
			}
			else {
				top.document.getElementById('fraOuter').rows = '68,2,*,2,20';
			}
		}
		function HideToolBar() {
			top.document.getElementById('fraOuter').rows = '68,2,*,2,20';
		}
		function ProceedCompanyForm() {
			stringUrl = 'merchant_list.asp{}Action=SearchFilter{{}}NumShow=25';
			stringUrl += '{{}}iTextSearch=' + document.getElementById('iTextSearch').value;
			document.getElementById('formMerchant').action = 'Merchant_frameset.asp?url=' + stringUrl;
			document.getElementById('formMerchant').submit();
			return false;
		}
	</script>
</head>
<body bgcolor="#484848" leftmargin="0" topmargin="0" id="siteHeaderPage">
<table border="0" cellspacing="0" cellpadding="0" dir="rtl" width="100%">
<tr><td height="2" bgcolor="#4a4a4a" colspan="4"></td></tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background-repeat:repeat-x; background-image:url('../Templates/<%# Domain.Current.ThemeFolder %>/background.gif');">
<tr>
	<td onclick="top.frames['fraBrowser'].location.href='main_lite.asp'; HideToolBar();" oncontextmenu="toggleToolBar(); return false;" style="cursor:pointer; width:394px; height:62px; padding-left:6px; background-image:url('../Templates/<%# Domain.Current.ThemeFolder %>/Logo.jpg'); background-repeat: no-repeat;"><br /></td>
	<td align="right" style="padding-right:5px;">
		<table align="right" border="0" cellspacing="0" cellpadding="3">
		<tr>
			<td align="center">
				<select style="width:128px; font-size:11px; direction:ltr; height:21px;" name="selectcompanyCustomer" id="selectcompanyCustomer"
				onchange="selectAction('selectcompanyCustomer')" oncontextmenu="selectActionDefault(selectcompanyCustomer,2)"
				 onfocusout="this.style.backgroundColor='';" onfocusin="this.style.backgroundColor='#ffffff'; HideToolBar();">
					<option value="">MERCHANTS</option>
					<option value="">----------------------</option>
					<option value="Merchant_frameset.asp?url=merchant_list.asp" style="color:#006699;">Merchants</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=52%&pageMenuUrl=merchant_groups.aspx">Merchant Groups</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=Merchant_BalanceFilter.aspx&pageBodyUrl=Merchant_BalanceData.aspx%3Ffrom=menu">Account Balance</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Merchant_ActivitiesFilter.aspx">Status</option>
					<!-- <option value="common_framesetA.asp?url=reports_transPayArchive_filter">àøëéåï úùìåîéí -->
					<!--<option value="FileArchive_browser.asp">ùîéøú ÷áöéí</option>-->
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=FileArchiveFilter.aspx&pageBodyUrl=FileArchiveData.aspx?From=top">File Archive</option>
					<!--<option value="merchant_batchFilesFrameset.asp">Batch File</option>-->
					<option value="common_framesetB.aspx?pageMenuWidth=300&pageMenuUrl=reports_companyDataFilter.aspx">Info Report</option>
                    <% if(Session["Identity"] == "Local") { %><option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=MerchantRevaluationCodes_filter.aspx&pageBodyUrl=MerchantRevaluationCodes_data.aspx%3Ffrom=menu">Revaluation Codes</option><% } %>
                    <% if(Session["Identity"] == "Local") { %><option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=MerchantTransRevaluation_filter.aspx&pageBodyUrl=MerchantTransRevaluation_data.aspx%3Ffrom=menu">Revaluated Transactions</option><% } %>
					<option value="common_framesetTabB.aspx?pageMenuWidth=300&pageMenuUrl=merchant_RegistrationFilter.aspx">Registration</option>
				</select>
			</td>
			<td align="center">
				<select style="font-size:11px; direction:ltr; height:21px;" name="selectCustomer" id="selectCustomer"
				onchange="selectAction('selectCustomer')" oncontextmenu="selectActionDefault(selectCustomer,2)"
				 onfocusout="this.style.backgroundColor='';" onfocusin="this.style.backgroundColor='#ffffff'; HideToolBar();">
					<option value="">WALLET</option>
					<option value="">----------------------</option>
					<option value="common_framesetB.aspx?pageMenuWidth=500&pageMenuUrl=Customers/List.aspx" style="color:#006699;">Customers</option>
				</select>
			</td>
			<td align="center">
				<select style="font-size:11px; direction:ltr; height:21px;" name="selectRisk" id="selectRisk"
				onchange="selectAction('selectRisk')" oncontextmenu="selectActionDefault(selectRisk,2)"
				 onfocusout="this.style.backgroundColor='';" onfocusin="this.style.backgroundColor='#ffffff'; HideToolBar();">
					<option value="">RISK</option>
					<option value="">----------------------</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Log_FraudDetection_filter.aspx" style="color:#006699;">Transaction Risk</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=BlacklistFilter.aspx">CC Blacklist</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=WhitelistFilter.aspx">CC Whitelist</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=260&pageMenuUrl=BLCommonFilter.aspx">Item Blacklist</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=merchant_blacklist_filter.aspx">Merchant Blacklist</option>
					<option value="BankFilesManager.aspx?root=Fraud">Fraud File Manager</option>
					<option value="BankFilesManager.aspx?root=CHB">CHB File Manager</option>
					<!--<option value="trans_chb_import.aspx">CHB Conflict Resolver</option>-->
					<!--<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Log_CHBNotify_filter.aspx&pageBodyUrl=Log_CHBNotify_Data.aspx">CHB Notification</option>-->
					<!--<option value="Report_Extended.aspx" style="color:#006699;">Summary</option>-->
					<!--<option value="MerchantActivityGraph.aspx">Activity Graph</option>-->
					<!--<option value="Report_System_CHB.aspx">Chb Trans</option>-->
				</select>
			</td>
			<td align="center">
				<select style="font-size:11px; direction:ltr; height:21px;"  name="selectLogs" id="selectLogs"
				onchange="selectAction('selectLogs')" oncontextmenu="selectActionDefault(selectLogs,2)"
				 onfocusout="this.style.backgroundColor='';" onfocusin="this.style.backgroundColor='#ffffff'; HideToolBar();"> 
					<option value="">LOGS</option>
					<option value="">----------------------</option>
					<option value="common_framesetB.aspx?pageMenuWidth=250&pageMenuUrl=Log_ChargeAttemptsFilter.aspx" style="color:#006699;">Charge Attempts</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=260&pageMenuUrl=Log_ConnectionFilter.aspx&pageBodyUrl=Log_ConnectionData.aspx">Connection Errors</option>
					<option value="common_framesetB.aspx?pageMenuWidth=250&pageMenuUrl=Log_DebitRulesFilter.aspx&pageBodyUrl=Log_DebitRules.aspx%3FFrom%3DTop">Debit Rules</option>
					<!--<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Log_window_charge_filter.aspx&pageBodyUrl=Log_window_charge.aspx">Hosted Page</option>-->
					<option value="common_framesetB.aspx?pageMenuWidth=250&pageMenuUrl=Log_PaymentPageFilter.aspx&pageBodyUrl=Log_PaymentPage.aspx">Hosted Page V2</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Log_TerminalJump_filter.aspx&pageBodyUrl=Log_TerminalJump_Data.aspx">Log Terminal Switch</option>
					<option value="common_framesetB.aspx?pageMenuWidth=240&pageMenuUrl=reports_chargePageUsage_filter.asp">Usage By Page</option>
					<% if(Session["Identity"] == "Local") { %><option value="common_framesetB.aspx?pageMenuWidth=250&pageMenuUrl=Log_DataChangesFilter.aspx">Data Changes</option><% } %>
					<option value="Report_TerminalsWoFees.aspx">Terminals w/o Fees</option>
					<option value="common_framesetB.aspx?pageMenuWidth=250&pageMenuUrl=Log_UserSec_filter.aspx&pageBodyUrl=<%# Server.UrlEncode("Log_UserSec_data.aspx?iDate=" + DateTime.Now.ToDateFormat()) %>">User Security Log</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Log_BankFiles_filter.aspx&pageBodyUrl=<%# Server.UrlEncode("about:blank") %>">EPA/CHB Log</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Log_PendingEvents_filter.aspx&pageBodyUrl=<%# Server.UrlEncode("about:blank") %>">Pending Events</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Log_TransactionAmounts_filter.aspx&pageBodyUrl=<%# Server.UrlEncode("about:blank") %>">Transaction Amounts</option>
					<% if(Session["Identity"] == "Local") { %><option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Log_TransactionAmounts2_filter.aspx&pageBodyUrl=<%# Server.UrlEncode("about:blank") %>">Transaction Amounts 2</option><% } %>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Log_TransactionHistory_filter.aspx&pageBodyUrl=<%# Server.UrlEncode("about:blank") %>">Transaction History</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=Log_History_filter.aspx&pageBodyUrl=<%# Server.UrlEncode("about:blank") %>">History</option>
				</select>
			</td>
			<td align="center">
				<select style="font-size:11px; direction:ltr; height:21px;" name="selectSystemAdmin" id="selectSystemAdmin"
				onchange="selectAction('selectSystemAdmin')"  oncontextmenu="selectActionDefault(selectSystemAdmin,2)"
				 onfocusout="this.style.backgroundColor='';" onfocusin="this.style.backgroundColor='#ffffff'; HideToolBar();">
					<option value="">MISC</option>
					<option value="">----------------------</option>
                    <option value="common_framesetTabB.aspx?pageMenuWidth=500&pageMenuUrl=Emails_MessageFilter.aspx" style="color:#006699;">Emails</option>
					<% if(Session["Identity"] == "Local") { %><option value="common_framesetB.aspx?pageMenuWidth=400&pageMenuUrl=DebitCompanies/List.aspx">New Debit Companies</option><% } %>				
					<option value="common_framesetB.aspx?pageMenuWidth=400&pageMenuUrl=system_debitCompanyList.aspx">Debit Companies</option>					
					<option value="common_framesetB.aspx?pageMenuWidth=400&pageMenuUrl=system_terminalFilter.asp">Debit Terminals</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=System_BinFilter.aspx">BIN Numbers</option>
					<option value="BankAccounts.aspx">Bank Accounts</option>
					<option value="common_framesetB.aspx?pageMenuWidth=650&pageMenuUrl=faq_categories.aspx">Q&A</option>
					<% if(Session["Identity"] == "Local") { %><option value="common_framesetB.aspx?pageMenuWidth=380&pageMenuUrl=Affiliates/List.aspx">New Partners</option><% } %>
					<option value="common_framesetB.aspx?pageMenuWidth=380&pageMenuUrl=Partners_List.aspx">Partners</option>
					<option value="system_testingPage.asp">Action Page</option>
					<option value="common_framesetB.aspx?pageMenuWidth=300&pageMenuUrl=ApplicationIdentity_list.aspx">Identity Setup</option>
					<option value="ContentPage.aspx">Content Page</option>
				</select>
			</td>
			<td align="center">
				<select style="font-size:11px; direction:ltr; height:21px;" name="selectSettings" id="selectSettings"
				onchange="selectAction('selectSettings')" oncontextmenu="selectActionDefault(selectSettings,2)"
				 onfocusout="this.style.backgroundColor='';" onfocusin="this.style.backgroundColor='#ffffff'; HideToolBar();">
					<option value="">SETTINGS</option>
					<option value="">----------------------</option>
					<option value="common_framesetB.aspx?pageMenuWidth=50%&pageMenuUrl=security_users.aspx">Security</option>
					<option value="system_currencies.aspx">Currencies</option>
					<option value="system_EventPendingTypes.aspx">Pending Event Types</option>
					<option value="system_PeriodicFees.aspx">Periodic Fee Types</option>
					<% if(Session["Identity"] == "Local") { %><option value="PeriodicFees/PeriodicFeeTypes.aspx">Periodic Fee Types New</option><% } %>
					<option value="system_dataChangeLog.aspx">Data Change Log</option>
					<option value="common_framesetB.aspx?pageMenuWidth=320&pageMenuUrl=system_GlobalDataFilter.aspx">Global Data Mng</option>
					<option value="system_GlobalValues.aspx">Global Values Mng</option>
				</select>
			</td>
		</tr>
		<tr>
			<td align="center">
				<select style="width:128px; font-size:11px; direction:ltr; height:21px;" name="selectWire" id="selectWire"
				onchange="selectAction('selectWire')" oncontextmenu="selectActionDefault(selectWire,2)"
				 onfocusout="this.style.backgroundColor='';" onfocusin="this.style.backgroundColor='#ffffff'; HideToolBar();">
					<option value="">WIRE TRANSFER</option>
					<option value="">----------------------</option>
					<% if(Session["Identity"] == "Local") { %><option value="Wire/" style="color:#006699;">Wire Transfer New</option><% } %>
					<% if(Session["Identity"] == "Local") { %><option value="Wire/WiresBatch/" style="color:#006699;">Wire Batch New</option><% } %>
					<option value="common_framesetTabB.aspx?pageMenuWidth=260&pageMenuUrl=Wire_search_filter.aspx&pageBodyUrl=Wire_search_data.asp" style="color:#006699;">Wire Transfer</option>
					<option value="common_framesetB.aspx?pageMenuWidth=250&pageMenuUrl=Log_MasavFile_filter.aspx&pageBodyUrl=Log_MasavFile_data.asp">Wire Control</option>
					<!--<option value="common_framesetB.aspx?pageMenuWidth=250&pageMenuUrl=BankIncomes_filter.aspx&pageBodyUrl=BankIncomes_data.aspx">Bank Incomes</option>-->
					<!--<option value="trans_bns_import.aspx">Import B+S PDF File</option>-->
					<option value="BankFilesManager.aspx?root=EPA">EPA File Manager</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=250&pageMenuUrl=EpaResolve_filter.aspx&pageBodyUrl=EpaResolve_data.aspx">EPA Conflict Resolver</option>
					<option value="BankFilesManager.aspx?root=Auth">AuthBatch File Manager</option>
				</select>
			</td>
			<td align="center">
				<select style="font-size:11px; direction:ltr; height:21px;" name="selectSettle" id="selectSettle"
				onchange="selectAction('selectSettle')" oncontextmenu="selectActionDefault(selectSettle,2)"
				 onfocusout="this.style.backgroundColor='';" onfocusin="this.style.backgroundColor='#ffffff'; HideToolBar();">
					<option value="">SETTLEMENTS</option>
					<option value="">----------------------</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=260&pageMenuUrl=Merchant_transSettledListFilter.aspx&pageBodyUrl=Merchant_transSettledListData.asp" style="color:#006699;">Settlements</option>
					<% if(Session["Identity"] == "Local") { %><option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=Settlements_Filter.aspx">Settlements 2</option><% } %>
					<option value="common_framesetTabB.aspx?pageMenuWidth=50%&pageMenuUrl=merchant_payingTree.asp">Settle Payouts</option>
					<% if(Session["Identity"] == "Local") { %><option value="common_framesetTabB.aspx?pageMenuWidth=40%&pageMenuUrl=SettlementCreateSelect.aspx">Settle Payouts 2</option><% } %>
					<option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=invoicesFilter.aspx">Invoices</option>
					
				</select>
			</td>
			<td align="center">
				<select style="font-size:11px; direction:ltr; height:21px;" name="selectTransAdmin" id="selectTransAdmin"
				onchange="selectAction('selectTransAdmin')" oncontextmenu="selectActionDefault(selectTransAdmin,2)"
				 onfocusout="this.style.backgroundColor='';" onfocusin="this.style.backgroundColor='#ffffff'; HideToolBar();">
					<option value="">TRANS ADMIN</option>
					<option value="">----------------------</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=Trans_admin_regularFilter.aspx" style="color:#006699;">Regular</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=Merchant_transPendingFilter.aspx&pageBodyUrl=Merchant_transPending.asp">Pending</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=recurring_filter.aspx">Recurring</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=TransFailFilter.aspx">Rejected</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=Trans_RollingFilter.aspx">Rolling Reserve</option>
					<option value="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=merchant_transApproval_filter.aspx&pageBodyUrl=merchant_transApproval.aspx">Pre Authorized</option>
					<!--<option value="trans_tc40_import.aspx">TC40 File Import</option>-->
				</select>
			</td>
			<td align="center">
				<table border="0" height="21" cellspacing="0" cellpadding="0" width="100%" style="border:1px solid #000000;">
					<tr>
						<td align="center" class="txt10"
							style="color:black; background-color:#d5d5d5; cursor:pointer; text-transform:uppercase; white-space:nowrap;"
							onclick="top.fraBrowser.location.href=('common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=RefundRequest_filter.aspx');HideToolBar();"
							onmouseout="style.backgroundColor='#d5d5d5';"
							onmouseover="style.backgroundColor='#ffffff';">
							Refund Requests
						</td>
					</tr>
				</table>
			</td>
			<td align="center">
				<table border="0" height="21" cellspacing="0" cellpadding="0" width="100%" style="border:1px solid #000000;">
					<tr>
						<td align="center" class="txt10"
							style="color:black; background-color:#d5d5d5; cursor:pointer; text-transform:uppercase; white-space:nowrap;"
							onclick="top.fraBrowser.location.href=('common_framesetTabB.aspx?pageMenuWidth=300&pageMenuUrl=Trans_admin_regularFilter.aspx');HideToolBar();"
							onmouseout="style.backgroundColor='#d5d5d5';"
							onmouseover="style.backgroundColor='#ffffff';">
							Transactions
						</td>
					</tr>
				</table>
			</td>
			<td align="center">
				<table border="0" height="21" cellspacing="0" cellpadding="0" width="100%" style="border:1px solid #000000;">
				<tr>
					<td align="center" class="txt10"
						style="color:black; background-color:#ffffff; padding:0 3px; text-align:left; cursor:pointer; text-transform:uppercase;"
						onclick="window.open('<%# Domain.Current.ReportsUrl %>','_blank');">
						<img src="../images/icon_graph2.gif" align="absmiddle" /> &nbsp; Reports</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr><td height="2" bgcolor="#818181" colspan="2"></td></tr>
<tr><td height="2" bgcolor="#333333" colspan="1"></td></tr>
</table>
<table bgcolor="silver" border="0" cellspacing="0" cellpadding="0" height="32" width="100%">
<tr>
	<td class="txt12" style="padding-left:18px;">
		<table cellpadding="0" cellspacing="0">
		<tr>
			<td><img src="../images/b_close.gif" title="Hide quick link" align="middle" style="cursor:pointer;" onclick="HideToolBar();" /></td>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<form action="Merchant_frameset.asp" target="fraBrowser" method="post" name="formMerchant" id="formMerchant" onsubmit=" HideToolBar(); return ProceedCompanyForm();">
			<td>
				<span style="font-size:13px;">MERCHANT</span>
				<input type="text" name="iTextSearch" id="iTextSearch" style="font-size:11; width:150px; background-color:#f5f5f5;" />
				<input type="submit" value="GO" style="font-size:12; width:40px;" />
			</td>
			</form>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<form action="Trans_admin_regularData.aspx" target="fraBrowser" name="formTrans" id="formTrans" onsubmit="HideToolBar();">
			<td>
				<span style="font-size:13px;">TRANS #</span>
				<input type="text" name="QuickBar_TransID" id="QuickBar_TransID" style="font-size:11; width:60px; background-color:#f5f5f5;" />
				<input type="submit" value="GO" style="font-size:12; width:40px;" />
			</td>
			</form>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<form action="Merchant_transFail.asp" target="fraBrowser" name="formRejected" id="form1" onsubmit="HideToolBar();">
			<td>
				<span style="font-size:13px;">FAILED #</span>
				<input type="text" name="QuickBar_TransFailID" id="QuickBar_TransFailID" style="font-size:11; width:60px; background-color:#f5f5f5;" />
				<input type="submit" value="GO" style="font-size:12; width:40px;" />
			</td>
			</form>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<form action="Trans_admin_regularData.aspx" target="fraBrowser" name="formTrans2" id="formTrans2" onsubmit="HideToolBar();">
			<input type="hidden" name="UseSQL2" value="YES" />
			<td>
				<span style="font-size:13px;">CC #</span>
				<input type="text" name="QuickBar_CcNumber" id="QuickBar_CcNumber" style="font-size:11; width:60px; background-color:#f5f5f5;" />
				<input type="submit" value="GO" style="font-size:12; width:40px;" />
			</td>
			</form>
		</tr>
		</table>
	</td>
</tr>
<tr><td height="1" bgcolor="#818181" colspan="2"></td></tr>
<tr><td height="1" bgcolor="#333333" colspan="1"></td></tr>
</table>
</body>
</html>
