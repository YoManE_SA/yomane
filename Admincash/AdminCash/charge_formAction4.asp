<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_balance.asp"-->
<%
sCompanyID=trim(request("CompanyID"))
sCustomerID=0
sIP=Request.ServerVariables("REMOTE_ADDR")
nAmount=trim(request("Amount"))
nTypeCredit=trim(request("CreditType"))
nCurrency=trim(request("Currency"))
sComment=DBText(request("Comment"))
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
         <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <style>
        body {
            font-family: 'Open Sans', sans-serif;
        }

        .bg-form {
            background: #f5f5f5;
            border: 1px solid #c0c0c0;
            padding: 10px;
        }

       
        h2 {
            margin: 0;
            padding: 0;
            font-size: 14px;
            padding: 10px 0;
            font-size: 14px;
            color: #2566AB;
        }

      
        hr {
            border-top: 1px solid #c2c2c2;
            border-bottom: none;
            border-left: none;
            border-right: none;
        }

        .container {
            width: 900px;
            font-size: 12px;
            margin: 0 auto;
        }

        .noerror {
          padding: 10px; color: #556652 !important; background-color: #D5FFCE; border: 1px solid #9ADF8F;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; font-size: 14px; line-height: 24px; margin-bottom: 10px;
        }

        .mb-10 {
            margin-bottom: 10px;
        }

        .pb-10 {
            padding-bottom: 10px;
        }

        .w-250 {
            width: 250px;
        }

        .w-80 {
            width: 80px;
        }
        .details {font-size: 14px;}
    </style>
</head>
<body bgcolor="#ffFFFF" leftmargin="0" topmargin="0" rightmargin="0" marginwidth="0" class="itextM" dir="ltr">

    <div class="container">
        <div class="mb-10">
            <span id="pageMainHeading">Virtual Terminal</span>
        </div>
        <div class="bg-form">
            

         	<%
		        if cdbl(nAmount)>0 then
			    if nTypeCredit = 0 Then nAmount = -nAmount
			    call fn_BalanceInsert(sCompanyID, 0, 3, "", nAmount, nCurrency, 0, sComment, False)
			%>

			<div class="noerror">
                Transaction was successfully
            </div>
		  
              <% end if %>

            <div class="details ">Company Number: <b> <%= dbTextShow(request("compname")) %></b> </div>


            <div style="text-align: center;"><img src="../images/cardadmin.gif" alt="" border="0"></div>
        </div>
    </div>
</body>
</html>