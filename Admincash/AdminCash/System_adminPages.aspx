<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    
    Dim sSQL, sPageUrl, sExtraTxt As String
    Dim i As Integer
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        
        sPageUrl = Request("PageUrl")
        
        If Request.Form("action") = "addNewPage" Then
            sSQL = "INSERT INTO tblAdminPages (ap_pageDescription, ap_pageUrl, ap_pageSecurityLevel) " & _
            "VALUES ('" & Request.Form("PageDescription") & "', '" & sPageUrl & "', " & Request.Form("pageLevel") & ")"
            Dim iRowEffected As Integer = dbPages.ExecSql(sSQL)
        Else if Request.Form("action") = "UpdatePage" Then
            sSQL = "UPDATE tblAdminPages SET " & _
            "ap_pageDescription = '" & Request.Form("PageDescription") & "', " & _
            "ap_pageUrl = '" & sPageUrl & "', " & _
            "ap_pageSecurityLevel = " & Request.Form("pageLevel") & _
            "WHERE (ap_pageUrl = '" & sPageUrl & "')"
            Dim iRowEffected As Integer = dbPages.ExecSql(sSQL)
        End If

    End Sub
    
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Admin Pages</title>
    <link  href="../StyleSheet/StyleAdminNet.css" rel="stylesheet" type="text/css" />
</head>
<body>
        <%
        	  sSQL = "SELECT ap_pageDescription, ap_pageSecurityLevel FROM tblAdminPages WHERE (ap_pageUrl = '" & sPageUrl & "')"
        	  Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
        	  Dim i As Integer
        	  If iReader.HasRows Then
        		  iReader.Read()
            
        		  Response.Write("<div style=""top:10px; left:10px; padding:5px; position:absolute;"">")
        		  For i = 1 To 10
        			  sExtraTxt = "Off"
        			  If i = iReader("ap_pageSecurityLevel") Then sExtraTxt = "On"
        			  Response.Write("<span class=""pageSecurityNum" & sExtraTxt & """>" & i & "</span>")
        		  Next
        		  Response.Write("</div>")
            %>
            �� ������ - ���� ����� �����<br /><br />
            <form method="post" action=".">
                <input type="hidden" name="action" value="UpdatePage" />
                <table class="formThin">
                <tr>
                    <th>����� ���</th>
                    <th>���� ���</th>
                    <th>�����</th>
                    <th></th>
                </tr>
                <tr>
                    <td><input type="text" name="PageUrl" size="32" value="<%=sPageUrl%>" maxlength="50" disabled="disabled" /></td>
                    <td><input type="text" name="PageDescription" size="28" maxlength="100" value="<%=iReader("ap_pageDescription")%>" /></td>
                    <td>
                        <select name="pageLevel">
                            <%
                            For i = 1 to 10
                                sExtraTxt = ""
                                If i = iReader("ap_pageSecurityLevel") Then sExtraTxt = "Selected"
                                Response.Write("<option value=""" & i & """ " & sExtraTxt & ">" & i & "</option>")
                            Next
                            %>
                        </select>
                    
                    </td>
                    <td><input type="submit" name="btnSubmit" size="20" value="����" /></td>
                </tr>
                </table>
            </form>
            <%
        Else
            %>
            �� �� ������ �����<br /><br />
            <form method="post" action=".">
                <input type="hidden" name="action" value="addNewPage" />
                <table class="formThin">
                <tr>
                    <th>����� ���</th>
                    <th>���� ���</th>
                    <th>�����</th>
                    <th></th>
                </tr>
                <tr>
                    <td><input type="text" name="PageUrl" size="32" value="<%=sPageUrl%>" maxlength="50" disabled="disabled" /></td>
                    <td><input type="text" name="PageDescription" size="28" maxlength="100" value="" /></td>
                    <td>
                        <select name="pageLevel">
                            <%
                            For i = 1 to 10
                                Response.Write("<option value=""" & i & """>" & i & "</option>")
                            Next
                            %>
                        </select>
                    
                    </td>
                    <td><input type="submit" name="btnSubmit" size="20" value="����" /></td>
                </tr>
                </table>
            </form>
            <%
        End if
        iReader.Close()
        %>
</body>
</html>
