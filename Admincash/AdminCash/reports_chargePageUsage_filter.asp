<!--#include file="../include/const_GlobalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_htmlInputs.asp"-->
<!--#include file="../include/func_security.asp"-->

<html>
	<head>
		<title><%= COMPANY_NAME_1 %> - Control Panel</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
		<link  href="../StyleSheet/styleAdminEng.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			function fCheckForm() {
				if (document.frmMenu.iFromDate.value=='') {
					alert('Please select start date');
					document.frmMenu.iFromDate.focus();
					return false;
				}
				if (document.frmMenu.iToDate.value=='') {
					alert('Please select end date');
					document.frmMenu.iToDate.focus();
					return false;
				}
				return true;
			}	
		</script>
	</head>
	<body class="itextm3" bgcolor="#eeeeee"><br />
		<table align="center" border="0" cellpadding="1" cellspacing="1" width="90%">
		<tr>
			<td id="pageMainHeadingTd"><span id="pageMainHeading">Usage By Page</span></td>
		</tr>
		<tr>
			<td align="left" colspan="2">
				<%
				pageSecurityLevel = 0
				pageSecurityLang = ""
				pageClosingTags = "</td></tr></table></td></tr></table>"
				Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
				%>
				<iframe src="reports_calendar_iframe.asp" name="calender" id="calender" width="140" height="130" marginwidth="0" marginheight="0" align="left" scrolling="no" frameborder="0"></iframe>
			</td>
		</tr>
		</table>
		<br />
		<table align="center" border="0" cellpadding="1" cellspacing="1" width="90%">
		<form action="reports_chargePageUsage_data.asp" target="frmBody" name="frmMenu" method="post">
			<tr>
				<td align="left" dir="ltr" class="txt12">* From<br/></td>
				<td align="left"><input type="text" name="iFromDate" value="<%=dateAdd("M",-1,date())%>" size="15" style="font-size: 12px;"/><br/></td>
			</tr>
			<tr>
				<td align="left" dir="ltr" class="txt12">** To<br/></td>
				<td align="left"><input type="text" name="iToDate" value="<%=date()%>" size="15" style="font-size: 12px;"/><br/></td>
			</tr>
			<tr><td height="8"></td></tr>
			<tr>
				<td colspan="2" align="left" dir="ltr" class="txt12" style="color:#003366;text-decoration:underline;">
					Charge page<br/>
				</td>		
			</tr>
			<tr>
				<td colspan="2" align="left" dir="ltr" class="txt11">
					<select name="iPageType" size="5" multiple="multiple" class="input2" style="width:180px;">
						<option value="8" selected="selected">Remote Charge</option>
						<option value="12">Public Charge Hebrew</option>
						<option value="7">Manual Charge Hebrew</option>
						<option value="11">Popup Charge Hebrew</option>
						<option value="9">Popup Charge</option>
					</select>
				</td>
			</tr>
			<tr><td height="5"></td></tr>
			<tr>
				<td colspan="2" align="left" dir="ltr" class="txt12" style="color:#003366;text-decoration:underline;">
					Card Type<br/>
				</td>		
			</tr>
			<tr>
				<td colspan="2" align="left" dir="ltr" class="txt11">
					<%
					sSQL="SELECT PaymentMethod_id AS ID, Name AS pm_Name FROM List.PaymentMethod"
					set rsData = oledbData.execute(sSQL)
					Call PutRecordsetCombo (rsData, "iCcTypeID", "multiple size=""5"" dir=""ltr"" class=""inputData"" style=""width:180px;""", "ID", "pm_Name", "false", "false", "1")
					%>
				</td>
			</tr>
			<tr><td height="5"></td></tr>
			<tr>
				<td colspan="2" align="left" dir="ltr" class="txt12" style="color:#003366;text-decoration:underline;">
					Currency<br/>
				</td>		
			</tr>
			<tr>
				<td colspan="2" align="left" dir="ltr" class="txt11">
					<select name="iCurrency" class="input2" style="width:180px;">
						<option value="0,1" selected="selected"></option>
						<option value="0">ILS</option>
						<option value="1">USD</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="left">
					<br/><input type="submit" name="" value=" Show "  class="button1" style="font-size:12px;" onclick="if(fCheckForm()){document.frmMenu.submit();}else{return false;};"/>
				</td>
			</tr>
		</form>
		<tr><td height="12"></td></tr>
		<tr>
			<td align="left" dir="ltr" class="txt11" colspan="2">
				* Calendar click<br/>
			</td>
		</tr>
		<tr><td height="2"></td></tr>
		<tr>
			<td align="left" dir="ltr" class="txt11" colspan="2">
				** Calendar click + shift<br/>
			</td>
		</tr>
		</table>
	</body>
</html>
<% call closeConnection() %>
