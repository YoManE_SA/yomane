<%@ Page Language="VB" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
	Sub Page_Load()
		Dim colSend As New NameValueCollection
		Security.CheckPermission(lblPermissions)
		If String.IsNullOrEmpty(Request("CompanyID")) Then
			litEmpty.Text = "No merchant selected!"
			mvJS.ActiveViewIndex = 0
		Else
			Dim nCount As Integer, sSQL As String = "SELECT * FROM viewMerchantsMailing WHERE MerchantID IN (" & dbPages.TestNumericList(Request("CompanyID"), nCount) & ")"
			If nCount < 1 Then
				litEmpty.Text = "No merchant found!"
				mvJS.ActiveViewIndex = 0
			Else
				Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
				While drData.Read()
					Dim trData As New TableRow
					For i As Integer = 0 To drData.FieldCount - 1
						Dim tdData As New TableCell
						tdData.Text = drData(i).ToString().Replace("`", "'")
						trData.Cells.Add(tdData)
						If drData.GetName(i).EndsWith(" Mail") Then
							If Not String.IsNullOrEmpty(drData(i)) Then
								colSend(drData.GetName(i)) &= tdData.Text & ";"
							ElseIf Not String.IsNullOrEmpty(drData("To Mail")) Then
								colSend(drData.GetName(i)) &= drData("To Mail") & ";"
							End If
						End If
						tdData.Style.Add("white-space", "nowrap")
					Next
					tblData.Rows.Add(trData)
				End While
				litEmpty.Text = String.Empty
				Dim trSend As New TableHeaderRow
				For i As Integer = 0 To drData.FieldCount - 1
					Dim tdSend As New TableHeaderCell
					If drData.GetName(i).EndsWith(" Mail") Then
						Dim hlSend As New HyperLink
						hlSend.NavigateUrl = "mailto:?BCC=" & colSend(drData.GetName(i))
						hlSend.Text = drData.GetName(i)
						hlSend.ForeColor = Drawing.Color.Maroon
						hlSend.ToolTip = "Send message to '" & drData.GetName(i) & "' of each merchant."
						If drData.GetName(i) <> "To Mail" Then hlSend.ToolTip &= Chr(13) & "Empty address will be substituted by address taken from the 'To Mail' column."
						tdSend.Controls.Add(hlSend)
					Else
						tdSend.Text = drData.GetName(i)
					End If
				tdSend.Style.Add("padding-bottom", "2px")
				tdSend.Style.Add("text-align", "left")
				tdSend.Style.Add("border-bottom", "solid 1px Silver")
				tdSend.Style.Add("white-space", "nowrap")
				trSend.Cells.Add(tdSend)
				Next
				tblData.Rows.AddAt(0,trSend)
				mvJS.ActiveViewIndex = 1
			End If
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Merchants Mailing List</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<form runat="server">
	<table style="width: 95%;" align="center" class="formNormal">
		<tr>
			<td id="pageMainHeading">
				<asp:Label ID="lblPermissions" runat="server" />
				Merchants' Mailing List
			</td>
		</tr>
		<tr>
			<td>
				<br />
				<asp:Table ID="tblData" CssClass="formNormal" runat="server" />
				<asp:Literal ID="litEmpty" runat="server"/>
			</td>
		</tr>
	</table>
	<asp:MultiView ID="mvJS" runat="server">
		<asp:View runat="server">
			<script language="JavaScript" type="text/javascript" defer="defer">
				window.parent.fraMenu.document.getElementById("bPrint").style.cursor = "not-allowed";
				window.parent.fraMenu.document.getElementById("bPrint").disabled = true;
			</script>
		</asp:View>
		<asp:View runat="server">
			<script language="JavaScript" type="text/javascript" defer="defer">
				window.parent.fraMenu.document.getElementById("bPrint").style.cursor = "pointer";
				window.parent.fraMenu.document.getElementById("bPrint").disabled = false;
			</script>
		</asp:View>
	</asp:MultiView>
	</form>
</body>
</html>