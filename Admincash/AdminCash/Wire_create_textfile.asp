<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_vheb.asp" -->
<%
sMasavCode = trim(request("MasavCode"))

sWireID = trim(request("WireMoney_id"))
sPayIDArray = Split(sWireID, ",", -1, 1)

nCount = 0
sWireID = ""
for each nID in sPayIDArray
	sWireStatus = trim(request("wireAction" & trim(nID)))
	if sWireStatus = "3" then
		nCount = nCount + 1
		if nCount <> 1 then sWireID	= sWireID & ","
		sWireID=sWireID & trim(nID)
	end if
next

'sWireID = "198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,216,217,218"
if sWireID<>"" then
	sSQL="SELECT tblWireMoney.WireMoney_id, tblWireMoney.Company_id, tblWireMoney.wireAmount, tblWireMoney.wireCurrency, tblWireMoney.wireExchangeRate, " &_
	"tblWireMoney.wirePaymentPayeeName, tblWireMoney.wirePaymentAccount, tblWireMoney.wirePaymentBranch, " &_
	"tblWireMoney.wireIDnumber, tblWireMoney.wireCompanyLegalNumber, bankCode, IsNull(tblCompany.CustomerNumber, '0000000') CustomerNumber " &_
	"FROM tblWireMoney INNER JOIN tblSystemBankList b ON WirePaymentBank=b.ID LEFT JOIN tblCompany ON tblWireMoney.Company_id = tblCompany.ID WHERE (tblWireMoney.WireMoney_id IN(" & sWireID & "))"
	'response.Write(ssql)
    set rsData = oledbData.Execute(sSQL)
	if not rsData.EOF then
	
		sDatePay = ""
		sDatePay = sDatePay & right(year(now()),2)
		if month(now())<10 then sDatePay = sDatePay & "0"
		sDatePay = sDatePay & month(now())
		if day(now())<10 then sDatePay = sDatePay & "0"
		sDatePay = sDatePay & day(now())
		
		sLogDate = now()
		
		'xxxxxxxxxxxxxxxxxxx header xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		sWireString = ""
		sWireString = sWireString & "K" & sMasavCode & "00"
		sWireString = sWireString & sDatePay 'payment date
		sWireString = sWireString & "00010"
		sWireString = sWireString & sDatePay '"seret" creation date
		sWireString = sWireString & "65829" & String(6,"0")
		sWireString = sWireString & right(Space(30) & "Netpay" ,30) 'company name
		sWireString = sWireString & Space(56) & "KOT"
		sWireString = sWireString & VbCr & VbLf
		'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		
		'xxxxxxxxxxxxxxxxxxx transactions xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		sTransPayTotal = 0
		sPayCount = 0
		
		Do until rsData.EOF
			sTransPay = rsData("wireAmount")
			If rsData("wireCurrency") <> 0 Then sTransPay = Round(ConvertCurrencyWRate(rsData("wireCurrency"), 0, rsData("wireExchangeRate"), sTransPay))

			sPayeeID = trim(rsData("wireCompanyLegalNumber"))
			if sPayeeID = "" then sPayeeID = trim(rsData("wireIDnumber"))
					
			sWireString = sWireString & "1" & sMasavCode
			sWireString = sWireString & "00" & String(6,"0")
			sWireString = sWireString & right(String(2,"0") & trim(rsData("bankCode")) ,2) 'bank code
			sWireString = sWireString & right(String(3,"0") & trim(rsData("wirePaymentBranch")) ,3) 'branch code         
			sWireString = sWireString & String(4,"0") 'account type
			sWireString = sWireString & right(String(9,"0") & trim(rsData("wirePaymentAccount")) ,9) 'account number
			sWireString = sWireString & "0"
			sWireString = sWireString & right(String(9,"0") & sPayeeID ,9) 'payee id
			sWireString = sWireString & right(Space(16) & dbtextShow(tovheb(rsData("wirePaymentPayeeName"))) ,16) 'payee name
			sWireString = sWireString & right(String(11,"0") & int(sTransPay) ,11) 'amount shkalim
			sWireString = sWireString & right(formatnumber(sTransPay,2,0,0,0),2) 'amount agorot
			sWireString = sWireString & right(String(20,"0") & rsData("CustomerNumber") ,20) 'payee id in company   
			sWireString = sWireString & "03090308" 'payment period
			sWireString = sWireString & "000006" & String(18, "0") & Space(2)
			sWireString = sWireString & VbCr & VbLf
			
			'Add to masav log
			sPayeeBankDetails = trim(rsData("wirePaymentAccount")) & ", " & trim(rsData("wirePaymentBranch")) & ", " & trim(rsData("bankCode"))
			sSQL="INSERT INTO tblLogMasavDetails (Company_id, WireMoney_id, PayeeName, PayeeBankDetails, logMasavFile_id, Amount) " &_
	        "VALUES(" & rsData("Company_id") & "," & rsData("WireMoney_id") & ", '" & rsData("wirePaymentPayeeName") & "', '" & sPayeeBankDetails & "', " & Session("WireAvailCur")(0) & " ," & sTransPay & ")"
	        oledbData.Execute sSQL
	        oledbData.Execute "Update tblLogMasavFile Set PayCount=PayCount+1, PayAmount=PayAmount+" & sTransPay & " Where logMasavFile_id=" & Session("WireAvailCur")(0)
			oledbData.Execute "UPDATE tblWireMoney SET WireFlag=3 WHERE WireMoney_id=" & rsData("WireMoney_id")
			sTransPayTotal = sTransPayTotal + formatnumber(sTransPay,2,0,0,0)
			sPayCount = sPayCount + 1
		rsData.movenext
		loop
		'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		
		'xxxxxxxxxxxxxxxxxxx total xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		sWireString = sWireString & "5" & sMasavCode & "00"
		sWireString = sWireString & sDatePay
		sWireString = sWireString & "0001"
		sWireString = sWireString & right(String(13,"0") & int(sTransPayTotal) ,13) 'amount shkalim
		sWireString = sWireString & right(formatnumber(sTransPayTotal,2,0,0,0),2) 'amount - agorot
		sWireString = sWireString & String(15,"0")
		sWireString = sWireString & right(String(7,"0") & sPayCount ,7) 'transaction count
		sWireString = sWireString & String(7,"0") & Space(63)
		sWireString = sWireString & VbCr & VbLf
		'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		sWireString = sWireString & String(128,"9") 'end marker
		
		Dim TSO
		Dim MyFile
		Dim FSO, MyDelFolder, MyDelFile, MyFolderCon, s
		Set FSO = CreateObject("Scripting.FileSystemObject")	
		Const sTextType = 2 'ForWriting
        MyFile = MapWirePath("BatchFiles/Masav/Masav.CSV")
		Set TSO = FSO.OpenTextFile(MyFile, sTextType, True)
		TSO.write sWireString
		TSO.close
		FSO.CopyFile MyFile, MapWirePath("BatchFiles/Masav/RSA" & logMasavFile_id & ".CSV")
		Set TSO = Nothing
	end if

	call rsData.close
	Set rsData = nothing
end if
'response.write "<pre><div align=left dir=ltr><font size=-1>" & sWireString & "</font></div><br /><br /><br /></pre>"
%>