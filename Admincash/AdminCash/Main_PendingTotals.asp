<% Server.ScriptTimeout = 9000 %>
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Untitled Page</title>
</head>
<body>

		<table>
		<tr><td colspan="7"><br /><hr width="100%" size="3" color="#484848" noshade><br /></td></tr>
		<tr>
			<td align="right" dir="rtl" colspan="7" class="txt16b" style="color:#2566AB;">
				������ ������<br /><br />
			</td>
		</tr>
		<tr>
			<td align="right" class="txtHead" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;">������<br /></td>
			<td align="right" class="txtHead" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;">����<br /></td>
			<td align="right" class="txtHead" bgcolor="#d8d8d8" style="border-right:1px solid white; border-bottom:1px solid gray; padding-right:10px;">'��<br /></td>
			<td align="right" class="txtHead" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;">������<br /></td>
			<td align="right" class="txtHead" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;">����<br /></td>
			<td align="right" class="txtHead" bgcolor="#d8d8d8" style="border-right:1px solid white; border-bottom:1px solid gray; padding-right:10px;">'��<br /></td>
			<td align="right" class="txtHead" bgcolor="#d8d8d8" style="border-bottom:1px solid gray;">����<br /></td>
		</tr>
		<tr>
		<%
        Set rsData = server.createobject("adodb.recordset")

		dim sCountAll(1)
		dim sAmountRegular(1)
		dim sAmountPayments(1)
		dim nCountDenied(1)
		dim nPercentDenied(1)
		i = 1
			
		if i = 0 then '������ ������
			CR_Code = 0
			CR_Name = "Shekel"
			CR_DisplayName = "�����"
			CR_Symbol = GetCurText(GC_NIS)
		elseif i = 1 then ' ������ �������
			CR_Code = 1
			CR_Name = "Dollar"
			CR_DisplayName = "������"
			CR_Symbol = "$"
		end if
				
		sSqlWhere  = "AND (tblCompanyTransPass.Currency = " & CR_Code & ") AND (tblCompanyTransPass.isTestOnly = 0) AND (tblDebitTerminals.isNetpayTerminal = 1)"
		
		sSQL="SELECT SUM(tblCompanyTransPass.Amount) AS amountRegular " &_
		"FROM tblCompanyTransPass INNER JOIN tblDebitTerminals ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber " &_
		"WHERE (tblCompanyTransPass.CreditType <> 0) AND (tblCompanyTransPass.PrimaryPayedID = 0) AND (tblCompanyTransPass.DeniedStatus = 0)" & sSqlWhere
		rsData.Open sSQL, oledbData
		if not rsData.EOF then
			if rsData("amountRegular")>0 then sAmountRegular(i) =  rsData("amountRegular")
		end if
		
		rsData.close
		sSQL="SELECT SUM(tblCompanyTransInstallments.amount) AS amountPayments " &_
		"FROM tblCompanyTransPass INNER JOIN tblDebitTerminals ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber INNER JOIN tblCompanyTransInstallments ON tblCompanyTransPass.ID = tblCompanyTransInstallments.transAnsID " &_
		"WHERE (tblCompanyTransInstallments.payID = 0) AND (tblCompanyTransPass.CreditType <> 0) AND (tblCompanyTransPass.DeniedStatus = 0) " & sSqlWhere
		rsData.Open sSQL, oledbData
		if not rsData.EOF then
			if rsData("amountPayments")>0 then sAmountPayments(i) =  rsData("amountPayments")
		end if
		
		rsData.close
		sSQL="SELECT COUNT(tblCompanyTransPass.Amount) AS countAll " &_
		"FROM tblCompanyTransPass INNER JOIN tblDebitTerminals ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber " &_
		"WHERE (tblCompanyTransPass.CreditType <> 0) AND (tblCompanyTransPass.PrimaryPayedID = 0) AND (tblCompanyTransPass.DeniedStatus = 0) " & sSqlWhere
		rsData.Open sSQL, oledbData
		if not rsData.EOF then sCountAll(i) = rsData("countAll")
		
		rsData.close
		sSQL="SELECT COUNT(tblCompanyTransPass.ID) AS nCountDenied " &_
		"FROM tblCompanyTransPass INNER JOIN tblDebitTerminals ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber " &_
		"WHERE (tblCompanyTransPass.CreditType <> 0) AND (tblCompanyTransPass.PrimaryPayedID = 0) AND (tblCompanyTransPass.DeniedStatus >= 1) AND (tblCompanyTransPass.DeniedStatus <= 6) " & sSqlWhere
		rsData.Open sSQL, oledbData
		if not rsData.EOF then
			nCountDenied(i) = rsData("nCountDenied")
			if nCountDenied(i)>0 then nPercentDenied(i) = formatnumber((nCountDenied(i)/sCountAll(i))*100,2,-1)
		end if
		%>
		<td align="right" dir="ltr" class="txt11">
			<%= nPercentDenied(i) %>% &nbsp; <%= nCountDenied(i) %><br />
		</td>
		<td align="right" dir="ltr" class="txt11">
			<%= CR_Symbol %> <%= formatnumber(sAmountPayments(i)+sAmountRegular(i),2) %><br />
		</td>
		<td align="right" dir="rtl" class="txt11" style="padding-right:10px; border-right:1px dashed gray;">
			<%= sCountAll(i) %><br />
		</td>

			<td align="right" dir="rtl" class="txt12">
				<li type="disk" style="color:gray;"><span class="txt12">����</span></li>
			</td>
		</tr>
		<tr>
		<%
		dim sAmountRefunds(1)
		dim sCountRefunds(1)
			
		sSqlWhere  = "AND (tblCompanyTransPass.Currency = " & CR_Code & ") AND (tblCompanyTransPass.isTestOnly = 0) AND (tblCompany.Closed = 0) AND (tblCompany.Blocked = 0) AND (tblDebitTerminals.isNetpayTerminal = 1)"

		rsData.close
		sSQL="SELECT SUM(tblCompanyTransPass.Amount) AS amountRefunds, COUNT(tblCompanyTransPass.Amount) AS countRefunds " &_
		"FROM tblCompanyTransPass INNER JOIN tblDebitTerminals ON tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber " &_
		"WHERE (tblCompanyTransPass.CreditType = 0) AND (tblCompanyTransPass.PrimaryPayedID = 0) AND (tblCompanyTransPass.DeniedStatus = 0)" & sSqlWhere
		rsData.Open sSQL, oledbData
		if not rsData.EOF then
			sAmountRefunds(i) = rsData("amountRefunds")
			If isNull(sAmountRefunds(i)) Then sAmountRefunds(i) = 0
			sCountRefunds(i) = rsData("countRefunds")
			If isNull(sCountRefunds(i)) Then sCountRefunds(i) = 0
		end if
		%>
		<td align="right" dir="ltr" class="txt11">
			- &nbsp; -<br />
		</td>
		<td align="right" dir="ltr" class="txt11">
			<%= CR_Symbol & formatnumber(-sAmountRefunds(i),2) %><br />
		</td>
		<td align="right" dir="rtl" class="txt11" style="padding-right:10px; border-right:1px dashed gray;">
			<%= sCountRefunds(i) %><br />
		</td>
		<td align="right" dir="rtl" class="txt12">
			<li type="disk" style="color:gray;"><span class="txt12">�����</span></li>
		</td>
	</tr>
	<tr><td height="1" colspan="7" style="border-bottom:1px dashed silver"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></td></tr>
	<tr>
		<%
		dim nPercentDenied2(1)
		
		if nCountDenied(i)>0 then nPercentDenied2(i) = formatnumber((nCountDenied(i)/(sCountAll(i)+sCountRefunds(i)))*100,2,-1)
		%>
		<td align="right" dir="ltr" class="txt11">
			<%= nPercentDenied(i) %>% &nbsp; <%= nCountDenied(i) %><br />
		</td>
		<td align="right" dir="ltr" class="txt11">
			<%= CR_Symbol & formatnumber((sAmountPayments(i)+sAmountRegular(i))-sAmountRefunds(i),2) %><br />
		</td>
		<td align="right" dir="rtl" class="txt11" style="padding-right:10px; border-right:1px dashed gray;">
			<%= sCountAll(i)+sCountRefunds(i) %><br />
		</td>
		<td align="right" dir="rtl" class="txt12">
			<li type="disk" style="color:gray;"><span class="txt12">�����</span></li>
		</td>
	</tr>
	</table>

</body>
</html>
<%
call rsData.close
Set rsData = nothing
call closeConnection()
Server.ScriptTimeout = 90
%>
