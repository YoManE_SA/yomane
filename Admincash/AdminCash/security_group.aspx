<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20080512 Tamir
	'
	' Security management
	'
	Dim nGroup As Integer, sGroup As String

	Sub LoadCheckboxes()
		Do While cblUsers.Items.Count > 0
			cblUsers.Items.Remove(cblUsers.Items(0))
		Loop
		Dim sSQL As String = "SELECT tblSecurityUserGroup.ID, '&nbsp; &nbsp; &nbsp; &nbsp; <span style=""font-weight:bold;width:100px;"">'+su_Username+'</span> '+su_Name Name, sug_IsMember FROM tblSecurityUserGroup INNER JOIN tblSecurityUser ON tblSecurityUserGroup.sug_User=tblSecurityUser.ID WHERE sug_Group=" & dbPages.TestVar(Request("ID"), 1, 0, 0) & " ORDER BY sug_IsMember DESC, su_Username"
		Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
		Do While drData.Read()
			cblUsers.Items.Add(New ListItem(drData("Name"), drData("ID")))
			If drData("sug_IsMember") Then cblUsers.Items(cblUsers.Items.Count - 1).Selected = True
		Loop
		drData.Close()
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		nGroup = dbPages.TestVar(Request("ID"), 1, 0, 0)
		sGroup = dbPages.ExecScalar("SELECT IsNull(sg_Name, '') FROM tblSecurityGroup WHERE ID=" & nGroup)
		If Not Page.IsPostBack Then LoadCheckboxes()
	End Sub

	Sub btnUpdate_Click(ByVal o As Object, ByVal e As EventArgs)
		Dim sSQL As String, sIDs As String = "0"
		For Each liCheckbox As ListItem In cblUsers.Items
			If liCheckbox.Selected Then sIDs &= ", " & liCheckbox.Value
		Next
		sSQL = "UPDATE tblSecurityUserGroup SET sug_IsMember=0 WHERE ID NOT IN (" & sIDs & ") AND sug_Group=" & nGroup
		dbPages.ExecSql(sSQL)
		sSQL = "UPDATE tblSecurityUserGroup SET sug_IsMember=1 WHERE ID IN (" & sIDs & ") AND sug_Group=" & nGroup
		dbPages.ExecSql(sSQL)
		LoadCheckboxes()
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<form id="frmServer" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Security<span> - Group: <span class="item"><%= sGroup %></span></span></h1>
		<div class="bordered">
			You can grant to users their membership of "<%= sGroup %>" group by checking respective checkboxes.
		</div>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<th style="width:54px;text-align:left;">
					Member
				</th>
				<th style="width:98px;text-align:left;">
					User Name
				</th>
				<th style="text-align:left;">
					Description
				</th>
			</tr>
		</table>
		<asp:CheckBoxList ID="cblUsers" runat="server" CssClass="checkboxes" />
		<div class="lastButton">
			<asp:Button ID="btnUpdate" CssClass="buttonWhite" OnClick="btnUpdate_Click" Text="Save" runat="server" />
		</div>
	</form>
</body>
</html>