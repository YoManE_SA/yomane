<%@ Page Language="VB" ResponseEncoding="windows-1255" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="radCln" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Dim sqlQuery As String
 	Dim CurrencyText() As String = Nothing
	Dim FailSourceText() As String = Nothing
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		If dbPages.IsActiveDSN2 Then
			dbPages.CurrentDSN = 2
			Response.Write(dbPages.ShowCurrentDSN)
		End If
		Security.CheckPermission(lblPermissions)
		Server.ScriptTimeout = 3000
		NetpayConst.GetCurrencyArray(CurrencyText)
		NetpayConst.GetConstArray(46, 1, Nothing, FailSourceText)
		If Not IsPostBack Then
			Terminal.Items.Add("") : DebitCompany.Items.Add("") : Currency.Items.Add("") : PaymentMethod.Items.Add("")
			dbPages.LoadListBox(Terminal, "Select dc_name + ' | ' + terminalNumber + ' | ' + dt_name, terminalNumber From tblDebitTerminals INNER JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany=tblDebitCompany.debitCompany_id" & IIf(Security.IsLimitedDebitCompany, " WHERE debitCompany_id IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))", String.Empty) & " ORDER BY dc_name, terminalNumber", Request("Terminal"))
			dbPages.LoadListBox(PaymentMethod, "SELECT Name, PaymentMethod_id FROM PaymentMethod ORDER BY PaymentMethod_id", Request("PaymentMethod"))
			dbPages.LoadListBox(DebitCompany, "Select dc_Name, debitCompany_id From tblDebitCompany" & IIf(Security.IsLimitedDebitCompany, " WHERE debitCompany_id IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))", String.Empty) & " ORDER BY dc_Name", Request("DebitCompany"))
			DateFrom.SelectedDate = dbPages.TestVar(Request("DateFrom"), DateTime.MinValue, DateTime.MaxValue, DateTime.Now.AddDays(-DateTime.Now.Day + 1))
			DateTo.SelectedDate = dbPages.TestVar(Request("DateTo"), DateTime.MinValue, DateTime.MaxValue, DateTime.Now)
			For i As Integer = 0 To UBound(CurrencyText)
				Currency.Items.Add(New ListItem(dbPages.GetCurText(i) & " " & CurrencyText(i), i))
				If Request("Currency") = i.ToString() Then Currency.Items(Currency.Items.Count - 1).Selected = True
			Next
		Else
			'If DateTo.SelectedDate.Value.Hour = 0 And DateTo.SelectedDate.Value.Minute = 0 Then DateTo.SelectedDate = DateTo.SelectedDate.Value.AddSeconds(86399)
			Dim sWhere As String = "(InsertDate >= '" & DateFrom.SelectedDate.Value.ToString() & "') AND (InsertDate <= '" & DateTo.SelectedDate.Value.ToString("d") & " 23:59:59')"
			If Company.SelectedValue <> "" Then sWhere = sWhere & " And tblCompanyTransFail.CompanyID IN(" & Company.SelectedValue & ")"
			If DebitCompany.SelectedValue <> "" Then sWhere = sWhere & " And tblCompanyTransFail.DebitCompanyID IN(" & DebitCompany.SelectedValue & ")"
			If PaymentMethod.SelectedValue <> "" Then sWhere = sWhere & " And PaymentMethod IN(" & PaymentMethod.SelectedValue & ")"
			If Terminal.SelectedValue <> "" Then sWhere = sWhere & " And TerminalNumber ='" & Terminal.SelectedValue & "'"
			If Currency.SelectedValue <> "" Then sWhere = sWhere & " And tblCompanyTransFail.Currency=" & Currency.SelectedValue
			If Security.IsLimitedMerchant Then sWhere &= " AND tblCompanyTransFail.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
			If Security.IsLimitedDebitCompany Then sWhere &= " AND tblCompanyTransFail.DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
			Dim strCnt As String = IIf(SingleCards.Checked, "Distinct isNull(tblCreditCard.CCard_number256, cast('x' as varbinary))", "*")
            sqlQuery = "Select replyCode, Count(" & strCnt & ") As Cnt, DescriptionOriginal, FailSource From tblCompanyTransFail " & _
             "Left Join tblDebitCompanyCode ON(tblCompanyTransFail.replyCode = tblDebitCompanyCode.Code And tblCompanyTransFail.DebitCompanyID = tblDebitCompanyCode.DebitCompanyID) " & _
             "Left Join tblCreditCard ON tblCompanyTransFail.CreditCardID = tblCreditCard.ID " & _
             "Where " & sWhere & " Group By replyCode, FailSource, DescriptionOriginal Order By Cnt Desc"
			'Response.Write(sqlQuery)
			If Request("EXOK") <> "" Then
				Response.ContentType = "application/vnd.ms-excel"
				Response.AddHeader("Content-Disposition", "attachment;filename=report.xls")
				Response.Write("<html><head>")
				Response.Write("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1255"">")
				Response.Write("</head><body>")
				Response.Write("<table>")
				Response.Write("<tr><th colspan=""4"">Rejected Report</th></tr>")
				Response.Write("<tr>")
				Response.Write("<th>Count</th>")
				Response.Write("<th>Source</th>")
				Response.Write("<th>Code</th>")
				Response.Write("<th>Description</th>")
				Response.Write("</tr>")
				Dim iReader As SqlDataReader = dbPages.ExecReader(sqlQuery, Nothing, 90)
				While iReader.Read()
					Response.Write("<tr>")
					Response.Write("<td>" & iReader("Cnt") & "</td>")
					Response.Write("<td>" & FailSourceText(IIf(IsDBNull(iReader("FailSource")), 0, iReader("FailSource"))) & "</td>")
					Response.Write("<td>" & iReader("replyCode") & "</td>")
					Response.Write("<td align=""right"">" & iReader("DescriptionOriginal") & "</td>")
					Response.Write("</tr>")
				End While
				iReader.Close()
				Response.Write("</table>")
				Response.Write("</body></html>")
				Response.End()
			End If
		End If
	End Sub

	Protected Function GenTitle(ByRef DataLink As String) As String
		GenTitle = ""
		If Company.SelectedValue <> "" Then GenTitle &= ", <span style=""color: #356691;"">Merchant:</span>" & Company.SelectedText : DataLink &= "&ShowCompanyID=" & Company.SelectedValue
		If Currency.SelectedValue <> "" Then GenTitle &= ", <span style=""color: #356691;"">Currency:</span>" & Currency.SelectedItem.Text
		If DebitCompany.SelectedValue <> "" Then GenTitle &= ", <span style=""color: #356691;"">Debit Company:</span>" & DebitCompany.SelectedItem.Text : DataLink &= "&DebitCompanyID=" & DebitCompany.SelectedValue
		If Terminal.SelectedValue <> "" Then GenTitle &= ", <span style=""color: #356691;"">Terminal:</span>" & Terminal.SelectedItem.Text : DataLink &= "&TerminalNumber=" & Terminal.SelectedValue
		If PaymentMethod.SelectedValue <> "" Then GenTitle &= ", <span style=""color: #356691;"">Payment Method:</span>" & PaymentMethod.SelectedItem.Text
		GenTitle &= ", <span style=""color: #356691;"">Dates:</span>" & DateFrom.SelectedDate.Value.Date.ToString("dd/MM/yyyy") & " - " & DateTo.SelectedDate.Value.Date.ToString("dd/MM/yyyy")
		GenTitle = GenTitle.Substring(2)
	End Function
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>FAILED STATISTICS</title>
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dim {color:gray;}
		tr.totals td {font-weight:bold; text-align:right;}
		.formNormal td {padding-left:5px; white-space:nowrap; text-align:left;}
		.formNormal th {text-align:left;}
		.formThin th { white-space:nowrap; padding-left:8px; padding-right:8px; }
		span.security
		{
			font-weight:bold;
			font-size:12px;
			color:rgb(245, 245, 245);
			background-color:#2566AB;
			padding-left:3px; padding-right:3px;
			vertical-align:middle;
		}
		span.securityManaged {cursor:pointer; }

		.BXls{
			background-image: url("../images/icon_xls.gif");
			background-repeat: no-repeat;
			height: 24px; text-align: right;
			background-color:#ffffff; font-size:11px;
			padding-left: 3px; padding-right: 3px; cursor: hand;
		}
	</style>
</head>
<body>
	<br />
	<table align="center" class="formThin" width="90%">
	<tr>
		<td colspan="4">
			<asp:label ID="lblPermissions" runat="server" /> 
			<span id="pageMainHeading">FAILED STATISTICS</span>
		</td>
	</tr>
	</table>
	<br />
	<form id="Form1" runat="server" method="post">
	<table align="center" width="90%" style="border:1px solid gray;">
	<tr>
		<td>
			<table border="0" align="center" class="formThin" width="100%">
			<tr>
				<th nowrap="nowrap">From Date<br /><radCln:RadDatePicker id="DateFrom" Runat="server" /></th>
				<th nowrap="nowrap">Terminal<br /><asp:ListBox ID="Terminal" Runat="server" Rows="1"/></th>
				<th nowrap="nowrap">Debit Company<br /><asp:ListBox ID="DebitCompany" Runat="server" Rows="1"/></th>
				<th nowrap="nowrap">Payment Method<br /><asp:ListBox ID="PaymentMethod" Runat="server" Rows="1"/></th>
				<td width="100%">
					<asp:Checkbox ID="SingleCards" Runat="server" /> Exclude multiple tries
				</td>
			</tr>
			<tr>
				<th nowrap="nowrap">To Date<br /><radCln:RadDatePicker ID="DateTo" runat="server" /></th>
				<th colspan="2">Company<br />
					<input type="hidden" id="CompanyID" name="CompanyID" value="<%=Company.SelectedValue%>" />
					<Uc1:FilterMerchants id="Company" ShowTitle="False" IsSingleLine="True" ClientField="CompanyID" runat="server" />
				</th>
				<th nowrap="nowrap">Currency<br /><asp:ListBox ID="Currency" runat="server" Rows="1"/></th>
				<th nowrap="nowrap" align="right">
					<input type="submit" name="DOK" style="height: 24px;" value="SHOW" />
					<input type="submit" name="EXOK" class="BXls" value="EXPORT" /><br />
				</th>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	</form>
	<br />
	<%If IsPostBack Or Request("DOK") = "1" Then%>
		 <table align="center" class="formThin" width="90%">
		  <tr><td colspan="4">
		   <%=GenTitle("")%><br />
		  </td></tr>
		 </table>
		 <br />
		 <table width="90%" border="0" class="formNormal" align="center">
		  <tr>
		 	<th style="text-align:right;padding-right:10px;" width="10%">Count</th>
		 	<th width="10%">Source</th>
		 	<th width="10%">Code</th>
		 	<th width="70%">Description</th>
		  </tr>
		 <%
		 	 Dim rowIndex As Integer = 0, CountSum As Long = 0
		 	 Dim iReader As SqlDataReader = dbPages.ExecReader(sqlQuery)
		 	 While iReader.Read()
		 		 Dim strColor As String = IIf(rowIndex Mod 2 > 0, "F5F5F5", "")
		 		 Response.Write("<tr style=""background-color:#" & strColor & """" & _
		 		 "onmouseover=""style.backgroundColor='#d8d8d8'"" onmouseout=""style.backgroundColor='" & strColor & "'"">")
		 		 Response.Write("<td style=""text-align:right;padding-right:10px;"">" & iReader("Cnt") & "</td>")
		 		 Response.Write("<td>" & FailSourceText(IIf(IsDBNull(iReader("FailSource")), 0, iReader("FailSource"))) & "</td>")
		 		 Response.Write("<td>" & iReader("replyCode") & "</td>")
		 		 Response.Write("<td>" & iReader("DescriptionOriginal") & "</td>")
		 		 Response.Write("</tr>")
		 		 rowIndex += 1
		 		 CountSum += iReader("Cnt")
		 	 End While
	   	 iReader.Close()
		 %>
		 <tr>
		  <th style="text-align:right;padding-right:10px;">Total: <%=CountSum%></th>
		 </tr>
		</table>
	<%End if%>	
 </body>
</html>
