<%@ Page Language="VB" %>
<%@ Import Namespace="System.IO" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
	Dim sSQL As String, sQueryWhere As String, nFromDate As String, nToDate As String, _
		sStatusColor As String, sAnd As String, sQueryString As String, sDivider As String
	Dim i As Integer

	Function DisplayContent(sVar As String) As String
		DisplayContent = "<span class=""key"">" & replace(replace(dbPages.dbtextShow(sVar),"=","</span> = "),"|"," <br /><span class=""key"">")
	End Function
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)

		If Trim(Request("ShowCompanyID")) <> "" Then sQueryWhere &= sAnd & "(tblCompanyTransApproval.companyID = " & Trim(Request("ShowCompanyID")) & ")" : sAnd = " AND "
		
		nFromDate = Trim(Request("fromDate")) & " " & Trim(Request("fromTime"))
		nToDate = Trim(Request("toDate")) & " " & Trim(Request("toTime"))
		If nFromDate <> " " Then sQueryWhere &= sAnd & "(tblCompanyTransApproval.InsertDate >= '" & nFromDate.ToString() & "')" : sAnd = " AND "
		If nToDate <> " " Then sQueryWhere &= sAnd & "(tblCompanyTransApproval.InsertDate <= '" & nToDate.ToString() & "')" : sAnd = " AND "
		
		If Trim(Request("CompanyName")) <> "" Then sQueryWhere &= sAnd & " tblCompany.CompanyName='" & Trim(Request("CompanyName")) + "'" : sAnd = " AND "
		If Request("DebitCompanyID") <> "" Then sQueryWhere &= sAnd & " DebitCompanyID IN(" & Trim(Request("DebitCompanyID")) & ")" : sAnd = " AND "
		If Request("AmountMin") <> "" And IsNumeric(Request("AmountMin")) Then sQueryWhere &= sAnd & " Amount>=" & Request("AmountMin") : sAnd = " AND "
		If Request("AmountMax") <> "" And IsNumeric(Request("AmountMax")) Then sQueryWhere &= sAnd & " Amount<=" & Request("AmountMax") : sAnd = " AND "
		If Trim(Request("ccEmail")) <> "" Then sQueryWhere &= sAnd & " tblCreditCard.email = '" & dbPages.DBText(Trim(Request("ccEmail"))) & "'" : sAnd = " AND "
		If Trim(Request("ccHolderName")) <> "" Then
            sQueryWhere &= sAnd & " '" & dbPages.DBText(Trim(Request("ccHolderName"))) & "' = tblCreditCard.member" : sAnd = " AND "
		End If
		If dbPages.TestVar(Request("CcLast4Num"), 1, 0, 0) > 0 Then
			sQueryWhere &= sAnd & " tblCreditCard.CCard_Last4=" & Request("CcLast4Num") : sAnd = " AND "
		End If
		If dbPages.TestVar(Request("BIN"), 1, 0, 0) > 0 Then
			sQueryWhere &= sAnd & " tblCreditCard.CCard_First6=" & Request("BIN") : sAnd = " AND "
		End If
		If dbPages.TestVar(Request("TransactionID"), 1, 0, 0) > 0 Then
			sQueryWhere &= sAnd & " tblCompanyTransApproval.ID=" & dbPages.DBText(Request("TransactionID")) : sAnd = " AND "
		End If
		If dbPages.TestVar(Request("Currency"), 1, 0, -1) > -1 Then
			sQueryWhere &= sAnd & " tblCompanyTransApproval.Currency=" & dbPages.DBText(Request("Currency")) : sAnd = " AND "
		End If
		
		'for filter in the page
		If Trim(Request("companyID")) <> "" Then sQueryWhere = sQueryWhere & sAnd & " tblCompanyTransApproval.companyID=" & Trim(Request("companyID")) : sAnd = " AND "

		If Security.IsLimitedMerchant Then
			sQueryWhere &= sAnd & " tblCompanyTransApproval.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))" : sAnd = " AND "
		End If
		If Security.IsLimitedDebitCompany Then
			sQueryWhere &= sAnd & " tblCompanyTransApproval.DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))" : sAnd = " AND "
		End If
		
		If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") _
		  Else PGData.PageSize = 25
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere

		If Request("SQL2") = "1" Then
			dbPages.CurrentDSN = 2
			Response.Write(dbPages.ShowCurrentDSN)
		Else
			dbPages.CurrentDSN = 1
		End If
		
		If Request("Totals") = "1" Then
			Response.Charset = "utf-8"
			Response.Write("<table class=""formThin"" width=""100%"">")
			Response.Write("<tr>")
			Response.Write("<td>TOTALS</td>")
			Response.Write("<td style=""text-align:right;cursor:pointer;float:right;"" onclick=""document.getElementById('tblTotals').style.display='none';""><b>X</b></td>")
			Response.Write("</tr>")
			Response.Write("</table>")
			Response.Write("<table class=""formNormal"" width=""170"" style=""margin:8px 5px;"">")
			Response.Write("<tr>")
			Response.Write("<th width=""25%"" style=""text-align:right"">Amount</th>")
			Response.Write("</tr>")
			'31/03/2010 Udi added support to approved echeck
            sSQL = "SELECT tblCompanyTransApproval.* FROM tblCompanyTransApproval" & _
            " LEFT JOIN tblCompany ON tblCompany.ID = tblCompanyTransApproval.companyID" & _
            " LEFT JOIN tblCreditCard ON tblCompanyTransApproval.CreditCardID = tblCreditCard.ID" & _
            " LEFT JOIN tblCheckDetails ON tblCompanyTransApproval.CheckDetailsID=tblCheckDetails.ID" & _
            " LEFT JOIN List.TransCreditType ON tblCompanyTransApproval.CreditType = List.TransCreditType.TransCreditType_id" & _
            " LEFT JOIN List.TransSource ON tblCompanyTransApproval.TransSource_id = List.TransSource.TransSource_id " & _
            " " & sQueryWhere & " ORDER BY tblCompanyTransApproval.InsertDate DESC"
			
            Dim naTotalPayments() As Decimal : ReDim naTotalPayments(eCurrencies.MAX_CURRENCY)
			
            Dim iTotalReader As SqlDataReader = dbPages.ExecReader(sSQL)
            While iTotalReader.Read()
                If iTotalReader("CreditType") = 0 Then
                    naTotalPayments(iTotalReader("Currency")) = naTotalPayments(iTotalReader("Currency")) - iTotalReader("Amount")
                Else
                    naTotalPayments(iTotalReader("Currency")) = naTotalPayments(iTotalReader("Currency")) + iTotalReader("Amount")
                End If
				
            End While
            iTotalReader.Close()
            For cnt As Integer = 0 To eCurrencies.MAX_CURRENCY
                Response.Write("<tr>")
                Response.Write("<td style=""text-align:right"">")
                If naTotalPayments(cnt) > 0 Then Response.Write(dbPages.FormatCurr(cnt, naTotalPayments(cnt)))
                Response.Write("</td>")
                Response.Write("</tr>")
            Next
            Response.Write("</table>")
            Response.End()
        End If
        '31/03/2010 Udi added support to approved echeck
        sSQL = "SELECT List.TransSource.Name TransTypeName, CompanyName, List.TransCreditType.Name, CCard_Last4, tblCreditCard.email, tblCompanyTransApproval.* " & _
        " FROM tblCompanyTransApproval LEFT JOIN tblCompany ON tblCompany.ID = tblCompanyTransApproval.companyID" & _
        " LEFT JOIN tblCreditCard ON tblCompanyTransApproval.CreditCardID = tblCreditCard.ID" & _
        " LEFT JOIN tblCheckDetails ON tblCompanyTransApproval.CheckDetailsID = tblCheckDetails.ID" & _
        " LEFT JOIN List.TransCreditType ON tblCompanyTransApproval.CreditType = List.TransCreditType.TransCreditType_id" & _
        " LEFT JOIN List.TransSource ON tblCompanyTransApproval.TransSource_id = List.TransSource.TransSource_id" & _
        " " & sQueryWhere & " ORDER BY tblCompanyTransApproval.InsertDate DESC"
		PGData.OpenDataset(sSQL)
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script type="text/javascript" src="../js/func_common.js"></script>
	<script language="JavaScript">
		function IconVisibility(fVar) {
			spanObj = document.getElementById(fVar)
			if (spanObj.style.visibility == 'hidden') {
				spanObj.style.visibility = 'visible';
			}
			else {
				spanObj.style.visibility = 'hidden';
			}
		}

		function toggleInfo(id,TransId,PaymentMethod)
		{
			objRef = document.getElementById('td' + TransId)
			objRefTr = document.getElementById('tr' + TransId)
			imgObj = document.getElementById("oListImg" + id)
			if (objRef.innerHTML == '') {
				if (objRefTr) { objRefTr.style.display = ''; }
				imgObj.src = '../images/tree_collapse.gif';	
				UrlAddress = (PaymentMethod == '15' ?  'trans_detail_eCheck.asp' : 'trans_detail_cc.asp');
				objRef.innerHTML='<iframe framespacing="0" scrolling="no" align="right" marginwidth="0" frameborder="0" width="100%" height="0px" src="'+UrlAddress+'?transID='+TransId+'&PaymentMethod='+PaymentMethod+'&bkgColor=ffffff&TransTableType=PreAuth" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>';
			}
			else {
				imgObj.src = '../images/tree_expand.gif';	
				if (objRefTr) { objRefTr.style.display = 'none'; }
				objRef.innerHTML='';
			}
		}
	</script>
</head>
<body>
	<form id="Form1" runat="server">
		<table align="center" style="width:92%" border="0">
		<tr>
			<td colspan="2" id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> Pre-Authorized
			</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
			<td></td>
			<td align="right" style="font-size:11px;" colspan="2">
				Filters:
				<%
				If Trim(Request("companyID")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "companyID", Nothing) & "';"">Merchant Name</a>") : sDivider = ", "
				If Trim(Request("iReplyCode")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iReplyCode", Nothing) & "';"">Reply</a>") : sDivider = ", "
				If sDivider <> "" Then Response.Write(" | (click to remove)") _
				  Else Response.Write("Add to filter by clicking on returned fields")
				%>		
			</td>
			<td></td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%">
		<tr>
			<th style="width:20px;"><br /></th>
			<th colspan="2">Trans</th>
			<th>CompanyName</th>
			<th>Date</th>
			<th>Payments Method</th>
			<th>Amount</th>
			<th>Confirmation</th>
			<th>Debit</th>
			<th>Fee</th>
			<th>Charge</th>
		</tr>
		<%
		While PGData.Read()
			i = i + 1
			
			sStatusColor = "#ffffff"
			Dim sDate As String = ""
			If Day(PGData("InsertDate")) < 10 Then
				sDate = sDate & "0"
			End If
			sDate = sDate & Day(PGData("InsertDate")) & "/"
			If Month(PGData("InsertDate")) < 10 Then
				sDate = sDate & "0"
			End If
			sDate = sDate & Month(PGData("InsertDate")) & "/" & Right(Year(PGData("InsertDate")), 2) & "&nbsp;&nbsp;"
			sDate = sDate & FormatDateTime(PGData("InsertDate"), 4)
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
			<%--<td><a onclick="toggleInfo('<%= PGData("id") %>','<%= PGData("PaymentMethod") %>')" name="<%= PGData("id") %>"  style="cursor:pointer; font-size:11px;"><img  src="../images/tree_expand.gif" align="middle" /></a></td>--%>
			<td><img onclick="toggleInfo(<%=i%>,'<%= PGData("id") %>','<%= PGData("PaymentMethod") %>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle"><br /></td>

				<td width="6" height="11" align="center">
					<div style="background-color:#66cc66; padding:1px;">
						<span style="background-color:white;"><img src="../images/1_space.gif" width="6" height="11" border="0"><br /></span>
					</div>
				</td>
				<td  align="right"><%= PGData("id") %><br /></td>
				<td>
					<%		
					If Not IsDBNull(PGData("CompanyName")) Then
					If Trim(Request("companyID")) <> "" Then Response.Write(PGData("CompanyName")) _
						Else dbPages.showFilter(PGData("CompanyName"), "?" & sQueryString & "&companyID=" & PGData("companyID"), 1)
					End If
					%>
				</td>
				<td style="white-space:nowrap;"><% =sDate%><br /></td>
				<td style="white-space:nowrap;">
					<img src="/NPCommon/ImgPaymentMethod/23X12/<%= Int(PGData("PaymentMethod"))%>.gif" alt="" />
					<% =PGData("PaymentMethodDisplay")%>
				</td>
				<td><%=dbPages.FormatCurrWCT(PGData("Currency"), PGData("Amount"), PGData("CreditType"))%></td>
				<td  align="right"><%= PGData("ApprovalNumber") %></td>
				<td>
					<%
					    Dim sPath As String = Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & PGData("DebitCompanyID") & ".gif")
					If PGData("PaymentMethod")>14 And PGData("DebitCompanyID") IsNot DBNull.Value Then
						If My.Computer.FileSystem.FileExists(sPath) Then
					            Response.Write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(PGData("DebitCompanyID")) & ".gif"" align=""middle"" border=""0"" />")
						End if
					End if
					%>
				</td>
				<td  align="center">
					<%
					If PGData("PayID") = 0 Then
						Response.Write("---")
					Else
						Response.Write("Payed")
					End If
					%>
				</td>
				<td  align="center">
					<%=IIf(Convert.IsDBNull(PGData("TransAnswerID")), "---", PGData("TransAnswerID"))%>
				</td>
			</tr>
			<tr id="tr<%= PGData("id") %>" style=" display:none;"><td></td><td id="td<%= PGData("id") %>" colspan="10" style="height:1px;"></td></tr>
			<tr><td height="1" colspan="12"  bgcolor="silver"></td></tr>
			<%
		End while
		PGData.CloseDataset()
		%>
		</table>
		<div id="tblTotals" onresize="PosObject(this, document.getElementById('TotalsButton'), 'RRTB', 0, -10);" style="border-color:#484848; border-style:solid; border-width:1px 3px; position:absolute;width:180px;background-color:White;display:none;"> &nbsp; LOADING DATA... </div>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" align="middle"> ADD TO FILTER
		</div>
	</form>
	<table align="center" style="width:92%">
	<tr>
		<td>
			<UC:Paging runat="Server" id="PGData" PageID="PageID" />
		</td>
		<td>
			<%Dim sString As String =(Request.QueryString.ToString() & "&" & Request.Form.ToString())%>
			<input type="button" id="TotalsButton" style="font-size:11px;cursor:pointer;" value="SHOW TOTALS &Sigma;" onclick="with(document.getElementById('tblTotals')){style.display='block';if(onresize)onresize();};setAjaxHTML('?Totals=1&<%=sString%>', document.getElementById('tblTotals'), true);"  />&nbsp;		
		</td>
	</tr>
	</table>
</body>
</html>
