<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Math"%>
<%@ Import Namespace="System.Globalization"%>
<script runat="server">
    Dim AffiliateID As Integer 
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)		
		AffiliateID = dbpages.TestVar(Trim(Request("AffiliateID")), 0, -1, -1)
        If Not IsPostBack Then ltAffiliateName.Text = dbPages.ExecScalar("Select name From tblAffiliates Where affiliates_id=" & AffiliateID)
		Security.CheckPermission(lblPermissions)	
	    PGData.OpenDataset("Select tblAffiliatesCount.* From tblAffiliatesCount Where AFCAFFID=" & AffiliateID & " Order By AFCDate")																						
		tlbTop.LoadPartnerTabs(AffiliateID)
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Partners</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script type="text/javascript" src="../js/func_common.js"></script>
</head>
<body>
	<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
	 <tr>
		<td>
			<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">Partners Management</span> - <asp:Literal runat="server" ID="ltAffiliateName" /><br /><br />
		</td>
	 <tr>
	    <td>
		<custom:Toolbar ID="tlbTop" Width="100%" runat="server" />
		<table class="formNormal" style="width:100%" border="0" cellpadding="0" cellspacing="0">
		<tr><td class="MerchantSubHead">Log Refferals<br /><br /></td></tr>
		<tr>
			<th>Type</th>
			<th>IP Address</th>
			<th>Date</th>
			<th>Referal</th>	
		</tr>
		<%While PGData.Read()%>
		<tr>
		    <td><%=PGData("AFCType")%></td>
		    <td><%=PGData("AFCIPAddress")%></td>
		    <td><%=PGData("AFCDate")%></td>
		    <td><%=PGData("AFCReferal")%></td>
		</tr>
		<%
		End while
		PGData.CloseDataset()
		%>
		</table>
		<br />
		<table align="center" style="width:92%" border="0">
		<tr>
			<td>
				<UC:Paging runat="Server" id="PGData" PageID="PageID" PageSize="30" />	
			</td>
		</tr>
		</table>
	    </td></tr>
    </table>
</body>
</html>
