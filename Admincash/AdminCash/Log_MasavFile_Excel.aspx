<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%
	
    Dim sWhere As String = String.Empty
    
    Dim dDateMax As String = Trim(Request("toDate") & " " & Request("toTime"))
    Dim dDateMin As String = Trim(Request("fromDate") & " " & Request("fromTime"))
    
    If IsDate(dDateMax) Then sWhere = sWhere & " AND tblLogMasavFile.insertDate<='" & dDateMax & "'"
    If IsDate(dDateMin) Then sWhere = sWhere & " AND tblLogMasavFile.insertDate>='" & dDateMin & "'"
  
	If dbPages.TestVar(Request("Currency"), 0, eCurrencies.MAX_CURRENCY, -1) >= 0 Then sWhere &= " AND PayCurrency=" & Request("Currency")
	If Not String.IsNullOrEmpty(sWhere) Then sWhere = " WHERE" & sWhere.Substring(4)
	Dim sSQL As String = "SELECT" & IIf(String.IsNullOrEmpty(sWhere), " TOP 20", String.Empty) & _
	" tblLogMasavFile.logMasavFile_id [Transfer No.], tblLogMasavFile.insertDate [Insert Date], PayedBankDesc Bank," & _
	" tblWireMoney.WireMoney_id [Wire ID], tblCompany.CompanyLegalName [Merchant Name]," & _
	" tblLogMasavDetails.Company_id [Merchant ID], tblSystemCurrencies.CUR_ISOName [Currency], Amount [Wire Amount]" & _
	" FROM tblLogMasavFile" & _
	" INNER JOIN tblLogMasavDetails ON tblLogMasavFile.logMasavFile_id=tblLogMasavDetails.logMasavFile_id" & _
	" LEFT JOIN tblCompany ON tblLogMasavDetails.Company_id = tblCompany.ID" & _
	" LEFT JOIN tblWireMoney ON tblLogMasavDetails.WireMoney_id = tblWireMoney.WireMoney_id" & _
	" LEFT JOIN tblSystemCurrencies ON tblWireMoney.WireCurrency = tblSystemCurrencies.CUR_ID" & sWhere & _
	" ORDER BY tblLogMasavFile.insertDate DESC, tblLogMasavDetails.logMasavDetails_id DESC"
	Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
	Response.ContentType = "application/vnd.ms-excel;charset=Windows-1255"
	Response.Charset = "Windows-1255"
	Response.ContentEncoding = Text.Encoding.GetEncoding("Windows-1255")
	Response.Write("<meta http-equiv=""content-type"" content=""application/vnd.ms-excel;charset=Windows-1255"" />")
	Response.Write("<table border=""1"" bordercolor=""gray""><tr>")
	For i As Integer = 0 To drData.FieldCount - 1
		Response.Write("<th nowrap=""nowrap"" bgcolor=""Gainsboro"">" & drData.GetName(i) & "</th>")
	Next
	Do While drData.Read
		Response.Write("</tr><tr>")
		For i As Integer = 0 To drData.FieldCount - 1
			Response.Write("<td nowrap=""nowrap"">" & drData(i) & "</td>")
		Next
	Loop
	Response.Write("</tr></table>")
%>