﻿<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    Dim mAFP_ID As Integer
    Dim mCurrency As Integer
    Dim mCompanyID As Integer
    Dim mAffiliateID As Integer
    Dim mPayID As Integer
    
    Protected Sub UpdatePayment()
        Response.Redirect("../include/common_totalTransShowAffiliate.asp?AFP_ID=" & mAFP_ID & "&AffiliateID=" & mAffiliateID & "&CompanyID=" & mCompanyID & "&Currency=" & mCurrency & "&PayID=" & mPayID & "&CreateAffPay=1&Redirect=" & Server.UrlEncode(dbPages.SetUrlValue(Request.Url.ToString(), "Delete", Nothing)))
    End Sub
    
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        mAFP_ID = dbPages.TestVar(Request("AFP_ID"), 0, -1, 0)
        mCurrency = dbPages.TestVar(Request("Currency"), 0, -1, 0)
        mCompanyID = dbPages.TestVar(Request("CompanyID"), 0, -1, 0)
        mAffiliateID = dbPages.TestVar(Request("AffiliateID"), 0, -1, 0)
        mPayID = dbPages.TestVar(Request("PayID"), 0, -1, 0)
        If dbPages.TestVar(Request("Delete"), 0, -1, 0) > 0 Then 
            dbPages.ExecSql("Delete tblAffiliatePaymentsLines Where AFPL_ID=" & dbPages.TestVar(Request("Delete"), 0, -1, 0))
            UpdatePayment()
        End If
    End Sub

	Protected Sub btnAdd_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        dbPages.ExecSql("Insert Into tblAffiliatePaymentsLines (AFPL_AFP_ID,AFPL_Text,AFPL_Quantity,AFPL_Amount,AFPL_Total) Values(" & _
            mAFP_ID & ",'" & _
            txtText.Text.Replace("'", "''") & "'," & _
            dbPages.TestVar(txtAmount.Text, 0d, -1d, 0d) & "," & _
            dbPages.TestVar(txtQuantity.Text, 0, -1, 0) & "," & _
            (dbPages.TestVar(txtAmount.Text, 0d, -1d, 0d) * dbPages.TestVar(txtQuantity.Text, 0, -1, 0)) & ")")
        UpdatePayment()
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>

    <form id="Form1" runat="server">
	<table class="formNormal" align="center" style="width:100%" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<th>Text</th>
		<th>Amount</th>
		<th>Quantity</th>
		<th>Total</th>
		<th style="text-align:center;">Action</th>
	</tr>
	<tr><td style="height:4px;"></td></tr>
    <tr>
     <td><asp:TextBox runat="server" ID="txtText" style="width:150px;" /></td>
     <td><asp:TextBox runat="server" ID="txtAmount" style="width:50px;" /></td>
     <td><asp:TextBox runat="server" ID="txtQuantity" style="width:50px;" /></td>
     <td></td>
     <td style="text-align:center;"><asp:Button runat="server" ID="btnAdd" Text=" Add " OnClick="btnAdd_onClick" /></td>
    </tr>
    <tr><td colspan="5"><hr style="border:1px dotted silver;" size="1"  /></td></tr>
	<% 
	    Dim lineTotal As Decimal = 0
        PGData.OpenDataset("Select AFPL_ID,AFPL_AFP_ID,AFPL_Text,AFPL_Quantity,AFPL_Amount,AFPL_Total From tblAffiliatePaymentsLines Where AFPL_AFP_ID=" & mAFP_ID)
	    While PGData.Read() 
	        lineTotal += dbPages.TestVar(PGData("AFPL_Total"), 0d, -1d, 0d)
		%>
	    <tr>
	     <td><%=PGData("AFPL_Text")%></td>
	     <td><%=dbPages.FormatCurr(mCurrency, dbPages.TestVar(PGData("AFPL_Amount"), 0d, -1d, 0d))%></td>
	     <td><%=PGData("AFPL_Quantity")%></td>
	     <td><%=dbPages.FormatCurr(mCurrency, dbPages.TestVar(PGData("AFPL_Total"), 0d, -1d, 0d))%></td>
	     <td style="text-align:center;"><a href="<%=dbPages.SetUrlValue(Request.Url.ToString(), "Delete", PGData("AFPL_ID")) %>">Delete</a></td>
	    </tr>
		<% 
	   End while
	   PGData.CloseDataset()
    %>
    <tr><td colspan="5"><hr style="border:1px solid black;" size="1"  /></td></tr>
    <tr>
     <td colspan="3" style="text-align:right;"><b>Subtotal:</b> <br /></td>
     <td><%=dbPages.FormatCurr(mCurrency, lineTotal)%></td>
    </tr>
    <tr>
     <td><input type="button" value="Finish Editing" onclick="parent.location.href='Partners_Payments.aspx?AffiliateID=<%=mAffiliateID%>&OpenNode=<%=mPayID%>'" /><br /></td>
    </tr>
    </table>
    
    <UC:Paging runat="Server" id="PGData" PageID="PageID" Visible="false" PageSize="100" />
    </form>
</body>
</html>
