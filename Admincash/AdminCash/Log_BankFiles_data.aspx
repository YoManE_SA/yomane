<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    Dim sSQL As String, sQueryString As String, sQueryWhere As String, nFromDate As String, nToDate As String, sDateDiff As String, sAnd As String, sDivider As String
    Dim tableName As String

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Security.CheckPermission(lblPermissions)
        tableName = IIf(Request("ddlLogType") = "CHB", "tblChbFileLog", "tblEpaFileLog")
        nFromDate = Trim(Request("fromDate")) & " " & Trim(Request("fromTime"))
		nToDate = Trim(Request("toDate")) & " " & Trim(Request("toTime"))
        If nFromDate <> " " Then sQueryWhere &= sAnd & "(" & tableName & ".ActionDate >= '" & nFromDate.ToString() & "')" : sAnd = " AND "
        If nToDate <> " " Then sQueryWhere &= sAnd & "(" & tableName & ".ActionDate <= '" & nToDate.ToString() & "')" : sAnd = " AND "
        If Request("ddActionType") <> "" Then sQueryWhere &= sAnd & "(" & tableName & ".ActionType = " & dbPages.TestVar(Request("ddActionType"), 0, -1, 0) & ")" : sAnd = " AND "
        If Request("txtOriginalFileName") <> "" Then sQueryWhere &= sAnd & "(" & tableName & ".OriginalFileNAme LIKE '%" & Request("txtOriginalFileName") & "%')" : sAnd = " AND "
        If Request("txtStoredFileName") <> "" Then sQueryWhere &= sAnd & "(" & tableName & ".StoredFileName LIKE '%" & Request("txtStoredFileName") & "%')" : sAnd = " AND "
        
		If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") Else PGData.PageSize = 50
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere
		If Request("SQL2") = "1" Then
			dbPages.CurrentDSN = 2
			Response.Write(dbPages.ShowCurrentDSN)
		Else
			dbPages.CurrentDSN = 1
		End If
	End Sub
	
	Protected Function GetLegendColor(ByRef actionType As Integer) As String
		Select Case actionType
			Case 0
				Return "#ff6666"
			Case 1
				Return "#6699cc"
			Case 2
				Return "#6699cc"
			Case 3
				Return "#9466CC"
			Case 4
				Return "#9466CC"
			Case 5
				Return "#7d7d7d"
			Case Else
				Return "#ffffff"
		End Select
	End Function	
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
	<head runat="server">
		<title>Admin Pages</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
		<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			.dataHeading { text-decoration:underline; white-space:nowrap; }
		</style>
	</head>
	<body>
		<form id="Form1" runat="server">
			<table align="center" style="width:92%" border="0">
				<tr>
					<td>
						<asp:label ID="lblPermissions" runat="server" />
						<span id="pageMainHeading">EPA LOG</span>
					</td>
					<td align="right">
						<table border="0" cellspacing="0" cellpadding="1">
							<tr>
								<td width="6" bgcolor="#7d7d7d"></td><td width="1"></td>
								<td style="font-size:10px;">Uploaded</td>
								<td width="10"></td>
								<td width="6" bgcolor="#6699cc"></td><td width="1"></td>
								<td style="font-size:10px;">New - Task started</td>
								<td width="10"></td>
								<td width="6" bgcolor="#6699cc"></td><td width="1"></td>
								<td style="font-size:10px;">New - Task complete</td>
								<td width="10"></td>
								<td width="6" bgcolor="#9466CC"></td><td width="1"></td>
								<td style="font-size:10px;">Pending - Task started</td>
								<td width="10"></td>
								<td width="6" bgcolor="#9466CC"></td><td width="1"></td>
								<td style="font-size:10px;">Pending - Task complete</td>
								<td width="10"></td>
								<td width="6" bgcolor="#ff6666"></td><td width="1"></td>
								<td style="font-size:10px;">Error</td>
							</tr>
						</table>	
					</td>			
				</tr>
				<tr><td><br /></td></tr>
				<tr>
					<td colspan="2" style="font-size:11px;text-align:right;">
						Filters:
						<%
						If Trim(Request("storedFileName"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"storedFileName",Nothing) &"';"">Stored File</a>") : sDivider = ", "
						If sDivider <> "" Then Response.Write(" | (click to remove)") Else Response.Write("Add to filter by clicking on returned fields")
						%>
					</td>
				</tr>
			</table>
			<table class="formNormal" align="center" style="width:92%" border="0">
				<tr>
					<th>&nbsp;</th>
					<th>Date</th>
					<th>Action</th>	
					<th>Original File</th>	
					<th>Stored File</th>	
					<th>User</th>
				</tr>
				<%
				sSQL = "SELECT " & tableName & ".*, tblEpaActionType.Name AS ActionName FROM " & tableName & " LEFT OUTER JOIN tblEpaActionType ON " & tableName & ".ActionType = tblEpaActionType.ID " + sQueryWhere + " ORDER BY " & tableName & ".ActionDate DESC"
				PGData.OpenDataset(sSQL)
				While PGData.Read()
					%>
					<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
						<td style="width:2px; background-color:<%=GetLegendColor(PGData("ActionType"))%>;"><img src="../images/1_space.gif" width="1" height="1" border="0"><br/></td>
						<td><%=PGData("ActionName")%></td>
						<td><%=PGData("ActionDate")%></td>
						<td><%=PGData("OriginalFileName")%></td>
						<td>	
							<%
							If PGData("StoredFileName") IsNot DBNull.Value Then
							    If Trim(Request("storedFileName")) <> "" Then
								    Response.Write(PGData("StoredFileName"))
							    Else
								    dbPages.showFilter(PGData("StoredFileName"), "?" & sQueryString & "&storedFileName=" & PGData("StoredFileName"), 1)
							    End If
							End If        
							%>
						</td>
						<td><%=PGData("Username")%></td>
					</tr>
					<%
				End while
				PGData.CloseDataset()
				%>
			</table>
			<hr size="1" noshade="noshade" width="92%" style="border:1px gray dotted;" />
			<table align="center" style="width:92%">
				<tr>
					<td><UC:Paging runat="Server" id="PGData" PageID="PageID"  /></td>
				</tr>
			 </table>
		</form>
	</body>
</html>
