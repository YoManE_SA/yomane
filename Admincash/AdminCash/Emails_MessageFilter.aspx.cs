﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.Emails;


public partial class Emails_MessageFilter : System.Web.UI.Page
{
	private int mailboxId;
	private string lastGroupText;
	private Dictionary<int, MessageStatus> _messageStatuses;
	protected Dictionary<int, MessageStatus> MessageStatuses 
	{ 
		get {
			if (_messageStatuses == null) 
				_messageStatuses = Netpay.Emails.MessageStatus.Cache;
			return _messageStatuses;
		} 
	}
	
	protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ExcelExport.Click += new EventHandler(ExcelExport_Click);
        base.OnInit(e);
        Security.CheckPermission(lblPermissions);
    }

	protected override void InitializeCulture()
	{
		base.InitializeCulture();
		this.Culture = this.UICulture = "en-us";
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Netpay.Web.WebUtils.LoggedUser != null)
        {
			int defMailbox = 0;
			var data = Netpay.Emails.MailBox.Load();
            repeaterEmails.DataSource = data;
            repeaterEmails.DataBind();
			if (data.Count > 0) defMailbox = data[0].ID;
			mailboxId = Request["mailboxId"].ToInt32(defMailbox);
			var mb = data.Where(d => d.ID == mailboxId).SingleOrDefault();
			if (mb != null) wcEmailSelector.Text = mb.EmailAddress;
        }
        
        if (!IsPostBack)
        {
            var statuses = MessageStatuses.Values;
            rep_status.DataSource = statuses;
            rep_status.DataBind();

			ddlUser.DataValueField = "ID"; ddlUser.DataTextField = "UserName";
			//ddlUser.DataSource = Netpay.Infrastructure.Security.SecurityManager.GetAllowedMerchants(WebUtils.DomainHost);
			ddlUser.DataBind();
			ddlUser.Items.Insert(0, ""); ddlUser.SelectedIndex = 0;


            ddlStatus.DataValueField = "ID"; ddlStatus.DataTextField = "Name";
            ddlStatus.DataSource = statuses; ddlStatus.DataBind();
            ddlStatus.Items.Insert(0, ""); ddlStatus.SelectedIndex = 0;

            DatePic.FromDate = DateTime.Now.Date.AddDays(-3);
            DatePic.ToDate = DateTime.Now.Date;
			Search_Click(sender, e);
		}
	}

    public string getStatusColor(int? status)
    {
        if (status == null) return string.Empty;
        if (!MessageStatuses.ContainsKey(status.Value)) return string.Empty;
        return MessageStatuses[status.Value].Color;
    }

    protected void Emails_OnRowDataBound(object sender, RepeaterItemEventArgs e)
    {
		var msg = (e.Item.DataItem as Netpay.Emails.Message);
		if (msg == null) return;
		string groupText = "";
		//int weekDiff = DateTime.Now.WeekOfYear() - msg.Date.WeekOfYear();
		TimeSpan ts = -(DateTime.Now - msg.Date);
		if (ts.Days == 0) groupText = "Today";
		else if (ts.Days == -1) groupText = "Yesterday";
		else groupText = "Earlier";

		if (groupText != lastGroupText)	{
			(e.Item.FindControl("ltEmailGroupText") as Literal).Text = groupText;
			e.Item.FindControl("trEmailGroup").Visible = true;
			lastGroupText = groupText;
		} else e.Item.FindControl("trEmailGroup").Visible = false;
    }

	protected string messageFormatDate(DateTime date)
	{
		if (date.Date == DateTime.Now.Date) return date.ToString("HH:mm");
		if (date.Date.Year < DateTime.Now.Date.Year) return date.ToString("dd/MM/yyyy HH:mm");
		return date.ToString("MMM dd");
	}

    public List<Netpay.Emails.Message> getDataList(string sortKey, bool sortDesc)
    {
		bool? isDeleted = false, isSent = null;
        int? status = ddlStatus.SelectedValue != "" ? (int?)int.Parse(ddlStatus.SelectedValue) : null;
        string text = string.IsNullOrEmpty(txt_search_words.Text) ? null : txt_search_words.Text;
		if (ddlIsSent.SelectedValue != "") {
			if (ddlIsSent.SelectedValue == "Deleted") isDeleted = true;
			else isSent = (bool?)bool.Parse(ddlIsSent.SelectedValue);
		}
		// sortKey, sortDesc
		var res = Netpay.Emails.Message.Search(new Message.SearchFilters() { mailboxId = mailboxId, status = status, sent = isSent, deleted = isDeleted, Date = new Range<DateTime?>(DatePic.FromDate.Value, DatePic.ToDate.Value.AddDays(1).AddMilliseconds(-1)), user = ddlUser.SelectedValue.NullIfEmpty(), Text = text }, pager.Info);
		ExcelExport.Visible = (res.Count > 0);
		lbl_message_For_grvPosts.Visible = (res.Count == 0);
		return res;
    }

    protected void lbt_Sort_Click(object sender, EventArgs e)
    {
        LinkButton button = (LinkButton)sender;
        if (button.CommandName.StartsWith("sort_"))
        {
			pager.Info.PageCurrent = 0;
            bool sortDesc = !hf_nameSort.Value.Equals("desc");
            var res = getDataList(button.CommandName.Substring(5), sortDesc);
			rptEmails.DataSource = res;
			rptEmails.DataBind();
            hf_nameSort.Value = sortDesc ? "desc" : "asc";
        }
    }

    protected void Pager_PageChanged(object sender, EventArgs e)
    {
		var data = getDataList("MessageDate", true);
		rptEmails.DataSource = data;
		rptEmails.DataBind();
	}

	protected void Search_Click(object sender, EventArgs e)
    {
		pager.Info.PageCurrent = 0;
		var data = getDataList("MessageDate", true);
		rptEmails.DataSource = data;
		rptEmails.DataBind();
    }

    private void ExcelExport_Click(object sender, EventArgs e)
    {

        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=report.xls");
        Response.Write("<html><head><meta http-equiv=Content-Type content=text/html; charset=UTF-8 /></head><body><table>");
        Response.Write("<tr><th>" + "ID,Time,Email From,Email To,Subject,CC,Text,Status,User".Replace(",", "</th><th>") + "</th>");
        foreach (var item in getDataList("MessageDate", true))
        {
            Response.Write("<tr>");
            Response.Write("<td>" + item.ID + "</td>");
            Response.Write("<td>" + item.Date.ToString("MM/dd/yyyy HH:mm:ss") + "</td>");
            Response.Write("<td>" + item.EmailFrom + "</td>");
            Response.Write("<td>" + item.EmailTo + "</td>");
            Response.Write("<td>" + item.Subject + "</td>");
            Response.Write("<td>" + item.CC + "</td>");
            Response.Write("<td>" + item.Text + "</td>");
            Response.Write("<td>" + item.Status + "</td>");
            Response.Write("<td>" + item.AdmincashUser + "</td>");
            Response.Write("</tr>");
        }
        Response.Write("</table></body></html>");
        Response.End();
    }
}