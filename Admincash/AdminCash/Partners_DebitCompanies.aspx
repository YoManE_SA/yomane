<%@ Page Language="VB" Inherits="htmlInputs" %>
<%@ Import namespace="Netpay.Infrastructure"%>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Register Src="Filter_DateRange.ascx" TagName="FilterDateRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Toolbar.ascx" TagPrefix="custom" TagName="Toolbar" %>

<script language="vbscript" runat="server">
	Sub SetAffiliateID(o As Object, e As RepeaterItemEventArgs)
		If e.Item.ItemType=ListItemType.Item Or e.Item.ItemType=ListItemType.AlternatingItem Then
			lblEmpty.Visible=False
			btnSave.Visible = True
		End If
	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
        Dim AffID as Integer = dbPages.TestVar(Request("ID"), 0, -1, 0)
		dsAff.ConnectionString = dbPages.DSN
		If IsPostBack Then Return
		ltAffiliateName.Text = dbPages.ExecScalar("Select name From tblAffiliates Where affiliates_id=" & AffID)
		'tlbTop.LoadAffiliateTabs(AffID)
		dbPages.LoadListBox(ddlDebitCompanyId, "Select dc_Name, DebitCompany_ID From tblDebitCompany Order by dc_name", "")
	End Sub

	Sub DeleteAffMerchant_Click(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs)
		dbPages.ExecSql("Delete From [Setting].[SetAffiliateDebitCompany] Where SetAffiliateDebitCompany_id=" & e.CommandArgument)
		rptAff.DataBind()
	End Sub
	
	Sub SaveData(sender As Object, e As System.EventArgs)
		Dim nID As Integer
		For Each riLine As RepeaterItem In rptAff.Items
			If riLine.ItemType = ListItemType.Item Or riLine.ItemType = ListItemType.AlternatingItem Then
				nID = CType(riLine.FindControl("SetMerchantAffiliate_id"), HiddenField).Value
				dbPages.ExecSql("Update [Setting].[SetAffiliateDebitCompany] Set " & _
					" FeesReducedPercentage=" & CType(riLine.FindControl("txtAffiliateFeeView"), TextBox).Text.ToDecimal(0) & _
					" Where SetAffiliateDebitCompany_id=" & nID)
				LabelUpdated.Visible = True
			End If
		Next
	End Sub
	
    Sub btnAddDebitCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsNumeric(ddlDebitCompanyId.SelectedValue) Then
            Response.Write("Please provide a debit company id")
            Exit Sub
        End If
  
        dbPages.ExecSql("Insert Into [Setting].[SetAffiliateDebitCompany] (Affiliate_id, DebitCompany_id) Values(" & Request("ID") & "," & ddlDebitCompanyId.SelectedValue & ")")
        rptAff.DataBind()
    End Sub
	
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
 <title>Partners Management</title>
 <link rel="stylesheet" type="text/css" media="all" href="../StyleSheet/calendar-blue.css" title="winter" />
 <link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr>
		<td>
    		<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">Partners Management</span> - <asp:Literal runat="server" ID="ltAffiliateName" /><br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<custom:Toolbar ID="tlbTop" Width="100%" runat="server" />
			<form id="form1" runat="server">
				<div style="font-size:12px;">
					Add Debit Company: 
					<asp:DropDownList runat="server" ID="ddlDebitCompanyId" /> 
					<asp:Button runat="server" Text="Add" ID="btnAddDebitCompany" OnClick="btnAddDebitCompany_Click" />
				</div>
				<br />
				<asp:SqlDataSource ID="dsAff" runat="server" SelectCommand="
					 SELECT a.SetAffiliateDebitCompany_id, d.DebitCompany_ID, d.dc_name, a.FeesReducedPercentage
					 FROM [Setting].[SetAffiliateDebitCompany] a
					 LEFT JOIN tblDebitCompany d ON (a.DebitCompany_id = d.DebitCompany_ID)
					 WHERE a.Affiliate_id = @adIf ORDER BY dc_name">
					<SelectParameters>
						<asp:QueryStringParameter QueryStringField="ID" Name="adIf" DefaultValue="-1" ConvertEmptyStringToNull="true" Type="Int32" />
					</SelectParameters>
				</asp:SqlDataSource>
				<table width="100%" class="formNormal" align="center">
				<tr><td class="MerchantSubHead">Debit Companies<br /><br /></td></tr>
				<tr>
					<th>ID</th>
					<th>Debit Company</th>
					<td></td>
					<th>Display</th>
					<td></td>
					<th>Delete</th>
				</tr>	
				<asp:Repeater ID="rptAff" runat="server" DataSourceID="dsAff" OnItemDataBound="SetAffiliateID">
					<ItemTemplate>
						<asp:HiddenField runat="server" ID="SetAffiliateDebitCompany_id" Value='<%#Eval("SetAffiliateDebitCompany_id")%>' />
						<tr onmouseover="this.style.backgroundColor='#f0f0f0';" onmouseout="this.style.backgroundColor='';">
							<td width="10%"><asp:Literal ID="litID" runat="server" Text='<%#Eval("DebitCompany_ID")%>' /></td>
							<td><asp:Literal runat="server" Text='<%#Eval("dc_Name")%>' /></td>
							<td style="background-color:Transparent;">&nbsp;</td>
							<td>
								<asp:TextBox ID="txtAffiliateFeeView" runat="server" text='<%#FormatNumber(IIF(IsDbNull(Eval("FeesReducedPercentage")), 0, Eval("FeesReducedPercentage")), 2, True)%>' Width="50px" />
								<asp:RangeValidator ControlToValidate="txtAffiliateFeeView" MinimumValue="0" MaximumValue="999999"
								ErrorMessage="The number is invalid!" Display="Dynamic" SetFocusOnError="true" Type="Double" runat="server" />
								<asp:RequiredFieldValidator  runat="server" Display="Dynamic" ControlToValidate="txtAffiliateFeeView" SetFocusOnError="true" ErrorMessage="The number must be filled!" ValidationGroup="4" />					
							</td>
							<td style="background-color:Transparent;">&nbsp;</td>
							<td><asp:Button runat="server" ID="btnDeleteAffMerchant" Text="Delete" OnCommand="DeleteAffMerchant_Click" CommandArgument='<%#Eval("SetAffiliateDebitCompany_id")%>' OnClientClick="if(!confirm('delete reference, are you sure?')) return false;" /></td>
						</tr>
					</ItemTemplate>
					<SeparatorTemplate>
						<tr>
							<td height="1" style="background-color:Silver;" colspan="2"></td>
							<td height="1" style="background-color:Transparent;"></td>
							<td height="1" style="background-color:Silver;" colspan="1"></td>
							<td height="1" style="background-color:Transparent;"></td>
							<td height="1" style="background-color:Silver;" colspan="1"></td>
						</tr>
					</SeparatorTemplate>
				</asp:Repeater>
				<tr>
					<td colspan="10" style="text-align:right;">
					<br />
						<asp:Button ID="btnSave" OnClick="SaveData" Text="Update" CssClass="buttonWhite" runat="server" Visible="false" />
						<asp:ValidationSummary ShowMessageBox="false" ShowSummary="false" runat="server" />
						<asp:Label ID="lblEmpty" CssClass="errorMessage" Text="No record found! Assignment is done form the company update page." runat="server" />			
					</td>
				</tr>
				<tr><td colspan="10" style="text-align:center;"><asp:Label ID="LabelUpdated" CssClass="details" Text="Data updated" runat="server" Visible="false" /></td></tr>
				</table>	
			</form>
		</td>
	</tr>
</table>	
</body>
</html>
