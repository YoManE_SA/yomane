<%
sShowTrSec = TestNumVar(Request("ShowTrSec"), 0, -1, 0)

bFirstMerchantMenuDataItem = True

sub ShowMerchantMenuDataItem(sText, sURL, sOnClick)
	If bFirstMerchantMenuDataItem Then
		bFirstMerchantMenuDataItem = False
	Else
		%>
		<td class="txt11" style="color:#A9A7A2;">|<br/></td>
		<%
	End if
	dim sTemp : sTemp=LCase(request("SCRIPT_NAME"))
	sTemp=mid(sTemp, inStrRev(sTemp, "/")+1)
	If inStr(LCase(sURL), sTemp)>0 Then
		sStyle="font-weight:bold; background-color:#D6C8A8;"
	Else
		sStyle="text-decoration:underline; background-color:;"
	End if
	if sOnClick="" then sOnclick="location.href='" & sURL & "';"
	%>
	<td align="center" class="txt10" title="<%= sURL %>" onclick="<%= sOnClick %>" style="cursor:pointer;<%= sStyle %>"<%= sID %>>
		<%= uCase(sText) %>
	</td>
	<%
End sub
%>
<tr>
	<td>
		<table dir="ltr" bgcolor="#f1f1f1" align="center" border="0" cellpadding="2" cellspacing="2" style=" width:100%; padding-top:2px;padding-bottom:2px; border:1px solid #A9A7A2;">
		<tr>
			<%
				ShowMerchantMenuDataItem "Home", "merchant_data.asp?companyID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Company Details", "merchant_dataCompany.asp?companyID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Personal & Login", "merchant_dataUser.asp?companyID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "General", "merchant_dataGeneral.asp?companyID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Options", "merchant_dataOptions.asp?companyID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Solutions", "merchant_dataSolutions.asp?companyID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Fees", "merchant_data_Fees.aspx?ID=" & nCompanyID, ""
				'ShowMerchantMenuDataItem "Fees2", "merchant_data_Fees2.aspx?ID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Payout", "merchant_dataPayInfo.asp?companyID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Bank Accounts", "merchant_data_bankAccounts.aspx?ID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Risk Management", "merchant_dataRiskMng.asp?companyID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Terminals", "merchant_dataTerminal.asp?companyID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Files & Notes", "merchant_dataNotes.aspx?companyID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Mail", "Merchant_dataMail.aspx?merchantID=" & nCompanyID, ""
				ShowMerchantMenuDataItem "Devices", "merchant_data_Devices.aspx?merchantID=" & nCompanyID, ""
				'ShowMerchantMenuDataItem "T", "merchant_data_terminal.aspx?ID=" & nCompanyID, ""
			%>
		</tr>
		</table>
	</td>
</tr>