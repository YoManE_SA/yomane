<%@ Page Language="VB" %>

<%@ Import Namespace="Netpay.CommonTypes" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Netpay.Process.NetpayConnector" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Sub Page_Load()
        Dim nTransID As Integer = dbPages.TestVar(Request("TransID"), 1, 0, 0)
        Dim bDebug As Boolean = IIf(Request("Debug") = "1", True, False)
        Dim bPreview As Boolean = IIf(Request("Preview") = "1", True, False)
        Dim sSQL As String = "SELECT CASE IsNull(ChargebackNotifyMail, '') WHEN '' THEN tblCompany.Mail ELSE ChargebackNotifyMail END ChargebackNotifyMail," & _
        " languagePreference, tblCompanyTransPass.insertDate" & _
        " FROM tblCompany INNER JOIN tblCompanyTransPass ON tblCompany.ID=companyID AND tblCompanyTransPass.ID=" & nTransID
        Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
        If drData.Read Then
            litMail.Text = dbPages.dbtextShow(drData("ChargebackNotifyMail"))
            litLanguage.Text = dbPages.dbtextShow(drData("languagePreference"))
            If bPreview Then
                Dim emailType As EmailTransaction.EmailType = IIf(Request("Clarify") = "1", EmailTransaction.EmailType.MerchantNotify_Clarify, EmailTransaction.EmailType.MerchantNotify_Denied)
                ltEmailText.Text = CType(Netpay.Process.Application.Modules("EmailTransaction"), EmailTransaction).getEmailText(Netpay.Process.Application.ProcessEngine, Netpay.Web.WebUtils.CurrentDomain.Host, nTransID, emailType, Nothing)
                litTitle.Text = "No email sent"
            Else
                Dim emailType As PendingEventType = IIf(Request("Clarify") = "1", PendingEventType.EmailPhotoCopy, PendingEventType.EmailChargeBack)
                dbPages.ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransID & "," & CType(emailType, Integer) & ",'', 3)")
                dbPages.ExecSql("UPDATE tblCompanyTransPass SET DeniedSendDate=GetDate() WHERE id=" & nTransID)
                litTitle.Text = "Email send scheduled."
            End If
        Else
            litTitle.Text = " Transaction not found"
            litError.Text = Date.Now.ToString("HH:mm:ss") & " Transaction not found"
        End If
        drData.Close()
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Mail Sent</title>
</head>
<body>
<asp:Literal runat="server" ID="ltEmailText" Mode="PassThrough" />
<div style="text-align:center;padding:20px 0 10px;">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td style="font-size:13px;text-align:center;">
				<b><asp:Literal ID="litTitle" runat="server" /></b><br />
				Mail: <asp:Literal ID="litMail" runat="server" /><br />
				Language: <asp:Literal ID="litLanguage" runat="server" /><br />
				<asp:Literal ID="litError" runat="server" /><br />
			</td>
		</tr>
	</table>
</div>
</body>
</html>