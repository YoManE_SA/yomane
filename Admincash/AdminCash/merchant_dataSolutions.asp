<%
response.buffer = true
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/func_controls.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
Sub SqlMerchantUpdateAppendBoolean(sSQL, sBitColumnName)
	sSQL=sSQL & IIf(sSQL="", "", ", ") & sBitColumnName & "=" & TestNumVar(Request.Form(sBitColumnName), 0, 1, 0)
End Sub

nCompanyID = TestNumVar(Request("CompanyID"), 1, 0, 0)
If trim(request("Action"))="update" Then
	sSQL=""
	SqlMerchantUpdateAppendBoolean sSQL, "IsSystemPay"
	SqlMerchantUpdateAppendBoolean sSQL, "IsSystemPayEcheck"
	SqlMerchantUpdateAppendBoolean sSQL, "IsSystemPayPersonalNumber"
	SqlMerchantUpdateAppendBoolean sSQL, "IsSystemPayCVV2"
	SqlMerchantUpdateAppendBoolean sSQL, "IsSystemPayPhoneNumber"
	SqlMerchantUpdateAppendBoolean sSQL, "IsSystemPayEmail"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRemoteCharge"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRemoteChargePersonalNumber"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRemoteChargeCVV2"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRemoteChargePhoneNumber"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRemoteChargeEmail"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRemoteChargeEcheck"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRemoteChargeEchPersonalNumber"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRemoteChargeEchCVV2"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRemoteChargeEchPhoneNumber"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRemoteChargeEchEmail"
	SqlMerchantUpdateAppendBoolean sSQL, "IsCustomerPurchase"
	SqlMerchantUpdateAppendBoolean sSQL, "IsCustomerPurchasePersonalNumber"
	SqlMerchantUpdateAppendBoolean sSQL, "IsCustomerPurchaseCVV2"
	SqlMerchantUpdateAppendBoolean sSQL, "IsCustomerPurchasePhoneNumber"
	SqlMerchantUpdateAppendBoolean sSQL, "IsCustomerPurchaseEmail"
	SqlMerchantUpdateAppendBoolean sSQL, "IsCustomerPurchasePayerID"
	SqlMerchantUpdateAppendBoolean sSQL, "IsWindowChargeEwallet"
	SqlMerchantUpdateAppendBoolean sSQL, "IsWindowChargeEwalletMust"
	SqlMerchantUpdateAppendBoolean sSQL, "IsPublicPay"
	SqlMerchantUpdateAppendBoolean sSQL, "IsPublicPayPersonalNumber"
	SqlMerchantUpdateAppendBoolean sSQL, "IsPublicPayCVV2"
	SqlMerchantUpdateAppendBoolean sSQL, "IsPublicPayPhoneNumber"
	SqlMerchantUpdateAppendBoolean sSQL, "IsPublicPayEmail"
	SqlMerchantUpdateAppendBoolean sSQL, "IsAllowSilentPostCcDetails"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRequiredClientIP"
	sSQL="UPDATE tblCompany SET " & sSQL & " WHERE ID=" & nCompanyID
	oledbData.Execute sSQL

	bIsHPPEnabled = TestNumVar(Request("IsCreditCard"), 0, 1, 0) Or TestNumVar(Request("IsEcheck"), 0, 1, 0) Or TestNumVar(Request("IsDirectDebit"), 0, 1, 0) Or TestNumVar(Request("IsPhoneDebit"), 0, 1, 0) Or TestNumVar(Request("IsCustomer"), 0, 1, 0) Or TestNumVar(Request("IsWebMoney"), 0, 1, 0) Or TestNumVar(Request("IsPayPal"), 0, 1, 0) Or TestNumVar(Request("IsGoogleCheckout"), 0, 1, 0) Or TestNumVar(Request("IsMoneyBookers"), 0, 1, 0)
	If bIsHPPEnabled = 1 Then
		sSQL="IF NOT EXISTS (SELECT 1 FROM tblCompanySettingsHosted WHERE CompanyID=" & nCompanyID & ")" & _
		"	INSERT INTO tblCompanySettingsHosted(CompanyID) VALUES (" & nCompanyID & ");" & _
		"UPDATE tblCompanySettingsHosted SET IsEnabled=1" & _
		", IsSignatureOptional=" & TestNumVar(Request("IsSignatureOptional"), 0, 1, 0) & _
		", IsCreditCard=" & TestNumVar(Request("IsCreditCard"), 0, 1, 0) & _
		", IsCreditCardRequiredEmail=" & TestNumVar(Request("IsCreditCardRequiredEmail"), 0, 1, 0) & _
		", IsCreditCardRequiredPhone=" & TestNumVar(Request("IsCreditCardRequiredPhone"), 0, 1, 0) & _
		", IsCreditCardRequiredID=" & TestNumVar(Request("IsCreditCardRequiredID"), 0, 1, 0) & _
		", IsCreditCardRequiredCVV=" & TestNumVar(Request("IsCreditCardRequiredCVV"), 0, 1, 0) & _
		", IsEcheck=" & TestNumVar(Request("IsEcheck"), 0, 1, 0) & _
		", IsEcheckRequiredEmail=" & TestNumVar(Request("IsEcheckRequiredEmail"), 0, 1, 0) & _
		", IsEcheckRequiredPhone=" & TestNumVar(Request("IsEcheckRequiredPhone"), 0, 1, 0) & _
		", IsEcheckRequiredID=" & TestNumVar(Request("IsEcheckRequiredID"), 0, 1, 0) & _
		", IsDirectDebit=" & TestNumVar(Request("IsDirectDebit"), 0, 1, 0) & _
		", IsDirectDebitRequiredEmail=" & TestNumVar(Request("IsDirectDebitRequiredEmail"), 0, 1, 0) & _
		", IsDirectDebitRequiredPhone=" & TestNumVar(Request("IsDirectDebitRequiredPhone"), 0, 1, 0) & _
		", IsDirectDebitRequiredID=" & TestNumVar(Request("IsDirectDebitRequiredID"), 0, 1, 0) & _
		", IsPhoneDebit=" & TestNumVar(Request("IsPhoneDebit"), 0, 1, 0) & _
		", IsPhoneDebitRequiredPhone=" & TestNumVar(Request("IsPhoneDebitRequiredPhone"), 0, 1, 0) & _
		", IsPhoneDebitRequiredEmail=" & TestNumVar(Request("IsPhoneDebitRequiredEmail"), 0, 1, 0) & _
		", IsCustomer=" & TestNumVar(Request("IsCustomer"), 0, 1, 0) & _
		", IsWebMoney=" & TestNumVar(Request("IsWebMoney"), 0, 1, 0) & _
		", WebMoneyMerchantPurse='" & DBText(Request("WebMoneyMerchantPurse")) & "'" & _
		", IsPayPal=" & TestNumVar(Request("IsPayPal"), 0, 1, 0) & _
		", PayPalMerchantID='" & DBText(Request("PayPalMerchantID")) & "'" & _
		", IsGoogleCheckout=" & TestNumVar(Request("IsGoogleCheckout"), 0, 1, 0) & _
		", GoogleCheckoutMerchantID='" & DBText(Request("GoogleCheckoutMerchantID")) & "'" & _
		", GoogleCheckoutMerchantKey='" & DBText(Request("GoogleCheckoutMerchantKey")) & "'" & _
		", IsMoneyBookers=" & TestNumVar(Request("IsMoneyBookers"), 0, 1, 0) & _
		", MoneyBookersAccount='" & DBText(Request("MoneyBookersAccount")) & "'" & _
		", IsWhiteLabel=" & TestNumVar(Request("IsWhiteLabel"), 0, 1, 0) & _
		" Where CompanyID=" & nCompanyID
	Else
		sSQL="UPDATE tblCompanySettingsHosted SET IsEnabled=0, IsCreditCard=0, IsEcheck=0, IsDirectDebit=0, IsPhoneDebit=0, IsCustomer=0, IsWebMoney=0, IsPayPal=0, IsGoogleCheckout=0, IsMoneyBookers=0 WHERE CompanyID=" & nCompanyID
	End If
	oledbData.Execute sSQL

	If TestNumVar(Request("IsPublicPayV2"), 0, 1, 0) = 1 Then
		sSQL = "IF NOT EXISTS (SELECT 1 FROM Setting.SetMerchantCart WHERE Merchant_id=" & nCompanyID & ") " & _
			   "INSERT INTO Setting.SetMerchantCart(Merchant_id, IsEnabled, IsAllowPreAuth, IsAllowDynamicProduct, IsKeepCart)" & _
			   "VALUES (" & nCompanyID & ", 0, 0, 0, 0);"
        oledbData.Execute sSQL
		sSQL = "UPDATE Setting.SetMerchantCart SET " & _
		" IsEnabled=1" & _
		",IsAllowDynamicProduct= " & TestNumVar(Request("PublicPay2_ChargeOptions"), 0, 1, 0) & _
		" WHERE Merchant_ID=" & nCompanyID
	Else
	    sSQL = "UPDATE Setting.SetMerchantCart SET IsEnabled=0 WHERE Merchant_id=" & nCompanyID
	End If
    oledbData.Execute sSQL

	If TestNumVar(Request("IsCartEnabled"), 0, 1, 0) = 1 Then
		sSQL="IF NOT EXISTS (SELECT 1 FROM Setting.SetMerchantCart WHERE Merchant_id=" & nCompanyID & ")" & _
		"	INSERT INTO Setting.SetMerchantCart(Merchant_id) VALUES (" & nCompanyID & ");" & _
	   "UPDATE Setting.SetMerchantCart SET IsEnabled=1 WHERE Merchant_id=" & nCompanyID
	Else
	    sSQL="UPDATE Setting.SetMerchantCart SET IsEnabled=0 WHERE Merchant_id=" & nCompanyID
	End If
	oledbData.Execute sSQL

	If TestNumVar(Request("IsEnableMobileApp"), 0, 1, 0) = 1 Then
		sSQL="IF NOT EXISTS (SELECT 1 FROM Setting.SetMerchantMobileApp WHERE Merchant_id=" & nCompanyID & ")" & _
		"	INSERT INTO Setting.SetMerchantMobileApp(Merchant_id, MaxDeviceCount) VALUES (" & nCompanyID & ", 0);"
        oledbData.Execute sSQL
	    sSQL = "UPDATE Setting.SetMerchantMobileApp SET " & _
		    " IsEnableMobileApp = " & TestNumVar(Request("IsEnableMobileApp"), 0, 1, 0) & _
		    " ,IsRequireEmail = " & TestNumVar(Request("MobileAppIsRequireEmail"), 0, 1, 0) & _
		    " ,IsRequirePersonalNumber = " & TestNumVar(Request("MobileAppIsRequirePersonalNumber"), 0, 1, 0) & _
		    " ,IsRequirePhoneNumber = " & TestNumVar(Request("MobileAppIsRequirePhoneNumber"), 0, 1, 0) & _
		    " ,IsRequireFullName = " & TestNumVar(Request("MobileAppIsRequireFullName"), 0, 1, 0) & _
		    " ,IsRequireCardholderSignature = " & TestNumVar(Request("MobileAppIsRequireCardholderSignature"), 0, 1, 0) & _
		    " ,IsAllowCardNotPresent = " & TestNumVar(Request("MobileAppIsAllowCardNotPresent"), 0, 1, 0) & _
		    " ,IsAllowInstallments = " & TestNumVar(Request("MobileAppIsAllowInstallments"), 0, 1, 0) & _
		    " ,IsAllowRefund = " & TestNumVar(Request("MobileAppIsAllowRefund"), 0, 1, 0) & _
		    " ,IsAllowAuthorization = " & TestNumVar(Request("MobileAppIsAllowAuthorization"), 0, 1, 0) & _
		    " ,IsRequireCVV = " & TestNumVar(Request("MobileAppIsRequireCVV"), 0, 1, 0) & _
		    " ,IsAllowTaxRateChange = " & TestNumVar(Request("MobileAppIsAllowTaxRateChange"), 0, 1, 0) & _
		    " ,ValueAddedTax = " & IIF(Request("MobileAppValueAddedTax") <> "", TestNumVar(Request("MobileAppValueAddedTax"), 0, 100, 0), "Null") & _
		    " ,MaxDeviceCount = " & IIF(Request("MobileAppMaxDeviceCount") <> "", TestNumVar(Request("MobileAppMaxDeviceCount"), 0, 100, 0), "0") & _
    	    " WHERE Merchant_id = " & nCompanyID
    Else
	    sSQL = "UPDATE Setting.SetMerchantMobileApp SET IsEnableMobileApp=0 WHERE Merchant_id=" & nCompanyID
    End If
	oledbData.Execute sSQL
End if

If TestNumVar(Request("MobileApp_ResetSignCount"), 0, 1, 0) = 1 Then
	oledbData.Execute "Update MobileDevice Set SignatureFailCount = 0 Where Account_id = (SELECT [Account_id] FROM [Data].[Account] WHERE [Merchant_id] = " & nCompanyID & ")" 
End If

Set HppData = oledbData.Execute("SELECT * FROM tblCompanySettingsHosted WHERE CompanyID=" & nCompanyID)
If HppData.EOF Then CloseHostedPageSettings()
Function GetHostedPageSettings(sValue)
    If HppData Is Nothing Then Exit Function
    GetHostedPageSettings = HppData(sValue)
End Function

Sub CloseHostedPageSettings()
    If HppData Is Nothing Then Exit Sub
    HppData.Close()
    Set HppData = Nothing
End Sub

Set mobileData = oledbData.Execute("SELECT * FROM Setting.SetMerchantMobileApp WHERE Merchant_id=" & nCompanyID)
If mobileData.EOF Then CloseMobileAppSettings()
Function GetMobileAppSettings(sValue)
    If mobileData Is Nothing Then Exit Function
    GetMobileAppSettings = mobileData(sValue)
End Function

Sub CloseMobileAppSettings()
    If mobileData Is Nothing Then Exit Sub
    mobileData.Close()
    Set mobileData = Nothing
End Sub

Set CartData = oledbData.Execute("SELECT * FROM Setting.SetMerchantCart WHERE Merchant_id=" & nCompanyID)
If CartData.EOF Then CloseCartSettings()
Function GetCartSettings(sValue)
    If CartData Is Nothing Then Exit Function
    GetCartSettings = CartData(sValue)
End Function

Sub CloseCartSettings()
    If CartData Is Nothing Then Exit Sub
    CartData.Close()
    Set CartData = Nothing
End Sub


Set PublicData = oledbData.Execute("SELECT * FROM Setting.SetMerchantCart Where Merchant_ID=" & nCompanyID)
If PublicData.EOF Then ClosePublicPageSettings()
Function GetPublicPageSettings(sValue)
    If PublicData Is Nothing Then Exit Function
    GetPublicPageSettings = PublicData(sValue)
End Function

Sub ClosePublicPageSettings()
    If PublicData Is Nothing Then Exit Sub
    PublicData.Close()
    Set PublicData = Nothing
End Sub

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css"/>
	<script type="text/javascript" language="javascript" src="../js/func_common.js"></script>
	<style type="text/css">
	    input { vertical-align:middle; }
	    input.inputData { width:90%; }
	</style>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<table align="center" border="0" cellpadding="0" cellspacing="2" dir="rtl" width="95%">
<tr>
	<td>
<%
sSQL="SELECT * FROM tblCompany WHERE ID=" & request("companyID")
set rsData = oledbData.Execute(sSQL)
if not rsData.EOF then
	%>
	<table border="0" cellpadding="0" cellspacing="0" dir="ltr" width="100%">
	<!--#include file="Merchant_dataHeaderInc.asp"-->
	<%
	pageSecurityLevel = 0
	pageSecurityLang = "ENG"
	pageClosingTags = "</table></td></tr></table>"
	Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
	%>
	<tr><td height="20"></td></tr>
	<!--#include file="Merchant_dataMenuInc.asp"-->
	</table>
	<form action="merchant_dataSolutions.asp" name="frmCoopDetail" method="post">
	<input type="hidden" name="CompanyID" value="<%= nCompanyID %>"/>
	<input type="hidden" name="Action" value="update"/>
	<table align="left" border="0" cellpadding="0" cellspacing="2" dir="ltr" width="100%">
	<tr><td height="10"></td></tr>
	<tr>
		<td valign="top">
			<table align="left" border="0" cellpadding="1" cellspacing="0" dir="ltr">
			<tr>
				<td></td>
				<td style="text-align:center;width:70px;">Require ID/SSN<br /></td>
				<td style="text-align:center;width:70px;">Require CVV2/CID<br /></td>
				<td style="text-align:center;width:70px;">Require Phone<br /></td>
				<td style="text-align:center;width:70px;">Require eMail<br /></td>
			</tr>
			<tr><td colspan="6"><hr width="100%" size="1" style="border-bottom:dotted 1px #d2d2d2;"></td></tr>
			<tr><td class="MerchantSubHead">VIRTUAL TERMINAL</td></tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsSystemPay" <% if rsData("IsSystemPay") then %>checked<% End If %> value="1"/>
					Allow Credit Card<br/>
					<input type="Checkbox" name="IsSystemPayEcheck" <% if rsData("IsSystemPayEcheck") then %>checked<% End If %> value="1"/>
					Allow eCheck<br/>
				</td>
				<td align="center"><input type="Checkbox" name="IsSystemPayPersonalNumber" <% if rsData("IsSystemPayPersonalNumber") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsSystemPayCVV2" <% if rsData("IsSystemPayCVV2") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsSystemPayPhoneNumber" <% if rsData("IsSystemPayPhoneNumber") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsSystemPayEmail" <% if rsData("IsSystemPayEmail") then %>checked<% End If %> value="1"/></td>
			</tr>
			<tr><td colspan="6"><hr width="100%" size="1" style="border-bottom:dotted 1px #d2d2d2;"></td></tr>
			<tr><td class="MerchantSubHead">SILENT POST</td></tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsRemoteCharge" <% if rsData("IsRemoteCharge") then %>checked<% End If %> value="1"/>
					Allow Credit card<br/>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="Checkbox" name="IsAllowSilentPostCcDetails" <% if rsData("IsAllowSilentPostCcDetails") then %>checked<% End If %> value="1"/>
					Allow to send full credit card details<br/>
				</td>
				<td align="center" valign="top"><input type="Checkbox" name="IsRemoteChargePersonalNumber" <% if rsData("IsRemoteChargePersonalNumber") then %>checked<% End If %> value="1"/></td>
				<td align="center" valign="top"><input type="Checkbox" name="IsRemoteChargeCVV2" <% if rsData("IsRemoteChargeCVV2") then %>checked<% End If %> value="1"/></td>
				<td align="center" valign="top"><input type="Checkbox" name="IsRemoteChargePhoneNumber" <% if rsData("IsRemoteChargePhoneNumber") then %>checked<% End If %> value="1"/></td>
				<td align="center" valign="top"><input type="Checkbox" name="IsRemoteChargeEmail" <% if rsData("IsRemoteChargeEmail") then %>checked<% End If %> value="1"/></td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsRemoteChargeEcheck" <% if rsData("IsRemoteChargeEcheck") then %>checked<% End If %> value="1"/>
					Allow eCheck/direct debit<br/>
				</td>
				<td align="center"><input type="Checkbox" name="IsRemoteChargeEchPersonalNumber" <% if rsData("IsRemoteChargeEchPersonalNumber") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsRemoteChargeEchCVV2" <% if rsData("IsRemoteChargeEchCVV2") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsRemoteChargeEchPhoneNumber" <% if rsData("IsRemoteChargeEchPhoneNumber") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsRemoteChargeEchEmail" <% if rsData("IsRemoteChargeEchEmail") then %>checked<% End If %> value="1"/></td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsRequiredClientIP" <% if rsData("IsRequiredClientIP") then %>checked<% End If %> value="1"/>
					Required ClientIP parameter<br/>
				</td>
			</tr>

			<tr><td colspan="6"><hr width="100%" size="1" style="border-bottom:dotted 1px #d2d2d2;"/></td></tr>
			<tr><td class="MerchantSubHead">MOBILE APP</td></tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsEnableMobileApp" <% if GetMobileAppSettings("IsEnableMobileApp") then %>checked<% End If %> value="1"/>
					Allow Mobile App<br/>
				</td>
				<td align="center" valign="top">
					<input type="Checkbox" name="MobileAppIsRequirePersonalNumber" <% if GetMobileAppSettings("IsRequirePersonalNumber") then %>checked<% End If %> value="1"/>
				</td>
                <td align="center" valign="top">
					<input type="Checkbox" name="MobileAppIsRequireCVV" <% if GetMobileAppSettings("IsRequireCVV") then %>checked<% End If %> value="1"/>
                </td>
				<td align="center" valign="top">
					<input type="Checkbox" name="MobileAppIsRequirePhoneNumber" <% if GetMobileAppSettings("IsRequirePhoneNumber") then %>checked<% End If %> value="1"/>
				</td>
				<td align="center" valign="top">
					<input type="Checkbox" name="MobileAppIsRequireEmail" <% if GetMobileAppSettings("IsRequireEmail") then %>checked<% End If %> value="1"/>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="Checkbox" name="MobileAppIsRequireFullName" <% if GetMobileAppSettings("IsRequireFullName") then %>checked<% End If %> value="1"/>
					Require Full Name<br/>
					&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="Checkbox" name="MobileAppIsRequireCardholderSignature" <% if GetMobileAppSettings("IsRequireCardholderSignature") then %>checked<% End If %> value="1"/>
					Require Cardholder Signature<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp; 
					<input type="Checkbox" name="MobileAppIsAllowCardNotPresent" <% if GetMobileAppSettings("IsAllowCardNotPresent") then %>checked<% End If %> value="1"/>
					Allow Transactions where card not present<br/>
 					&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="Checkbox" name="MobileAppIsAllowInstallments" <% if GetMobileAppSettings("IsAllowInstallments") then %>checked<% End If %> value="1"/>
					Allow Installments<br/>
					&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="Checkbox" name="MobileAppIsAllowAuthorization" <% if GetMobileAppSettings("IsAllowAuthorization") then %>checked<% End If %> value="1"/>
					Allow Authorization<br/>
					&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="Checkbox" name="MobileAppIsAllowRefund" <% if GetMobileAppSettings("IsAllowRefund") then %>checked<% End If %> value="1"/>
					Allow Refunds<br/>
					&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="Checkbox" name="MobileAppIsAllowTaxRateChange" <% if GetMobileAppSettings("IsAllowTaxRateChange") then %>checked<% End If %> value="1"/>
					Allow Tax Rate Change<br/>

					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Tax Rate
                    <input name="MobileAppValueAddedTax" value="<%= GetMobileAppSettings("ValueAddedTax") %>" style="width:40px;"/>%
					<br/>
				</td>
				<td valign="bottom" align="right" colspan="4">
					Maximum active devices:
                    <input name="MobileAppMaxDeviceCount" value="<%= GetMobileAppSettings("MaxDeviceCount") %>" style="width:40px;"/>
					<br /><br />
					<a href="?companyID=<%=Request("companyID")%>&MobileApp_ResetSignCount=1">reset signature lock count</a>
				</td>
			</tr>
			<%CloseMobileAppSettings()%>

			<tr><td colspan="6"><hr width="100%" size="1" style="border-bottom:dotted 1px #d2d2d2;"/></td></tr>
			<tr><td class="MerchantSubHead">PUBLIC PAYMENT PAGE</td></tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsPublicPay" <% if rsData("IsPublicPay") then %>checked<% End If %> value="1"/>
					Allow Credit card & eCheck<br/>
				</td>
				<td align="center"><input type="Checkbox" name="IsPublicPayPersonalNumber" <% if rsData("IsPublicPayPersonalNumber") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsPublicPayCVV2" <% if rsData("IsPublicPayCVV2") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsPublicPayPhoneNumber" <% if rsData("IsPublicPayPhoneNumber") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsPublicPayEmail" <% if rsData("IsPublicPayEmail") then %>checked<% End If %> value="1"/></td>
			</tr>
			<tr><td colspan="6"><hr width="100%" size="1" style="border-bottom:dotted 1px #d2d2d2;"/></td></tr>
			<tr><td class="MerchantSubHead">HOSTED PAYMENT PAGE (OLD!)</td></tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="checkbox" disabled=disabled /><input type="hidden" name="IsCustomerPurchase" <% IF rsData("IsCustomerPurchase") THEN %>value="1"<%ELSE%>value="0"<%END IF%> />
					Allow Credit card<br/>
				</td>
				<td align="center"><input type="hidden" name="IsCustomerPurchasePersonalNumber" <% if rsData("IsCustomerPurchasePersonalNumber") then %>value="1"<%ELSE%>value="0"<%END IF%> /></td>
				<td align="center"><input type="hidden" name="IsCustomerPurchaseCVV2" <% if rsData("IsCustomerPurchaseCVV2") then %>value="1"<%ELSE%>value="0"<%END IF%> /></td>
				<td align="center"><input type="hidden" name="IsCustomerPurchasePhoneNumber" <% if rsData("IsCustomerPurchasePhoneNumber") then %>value="1"<%ELSE%>value="0"<%END IF%> /></td>
				<td align="center"><input type="hidden" name="IsCustomerPurchaseEmail" <% if rsData("IsCustomerPurchaseEmail") then %>value="1"<%ELSE%>value="0"<%END IF%> /></td>
			</tr>
			<tr>
				<td style="padding-left:20px;" colspan="2" nowrap="nowrap">
					<input type="checkbox" disabled=disabled /><input type="hidden" name="IsCustomerPurchasePayerID" <% if rsData("IsCustomerPurchasePayerID") then %>value="1"<%ELSE%>value="0"<%END IF%> />
					Data storing allowed
					<!--[<a class="faq" href="#" onclick="javascript:var winFromURL=window.open('Merchant_hppURLAllowed.asp?companyID=<%= rsData("ID") %>&CustomerNumber=<%= rsData("CustomerNumber") %>','fraFromURL','scrollbars=1, width=580, height=240, resizable=0, Status=1, top=200, left=250');">IP's allowed</a>]-->
                    <br/>
					<input type="checkbox" disabled=disabled /><input type="hidden" name="IsWindowChargeEwallet" <% if rsData("IsWindowChargeEwallet") then %>value="1"<%ELSE%>value="0"<%END IF%> />
					Show electronic wallet
					<span style="color:gray;">(shows signup page)</span><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" disabled=disabled /><input type="hidden" name="IsWindowChargeEwalletMust" <% if rsData("IsWindowChargeEwalletMust") then %>value="1"<%ELSE%>value="0"<%END IF%> />
					Signup is required<br/>
				</td>
			</tr>
			<tr><td colspan="6"><hr width="100%" size="1" style="border-bottom:dotted 1px #d2d2d2;"/></td></tr>
			<tr><td class="MerchantSubHead">Cart Service</td></tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsCartEnabled" <% if GetCartSettings("IsEnabled") then %>checked<% End If %> value="1">
					Allow Cart Service<br/>
				</td>
				<td></td>
			</tr>
			<%Call CloseCartSettings() %>
			</table>
		</td>
		<td style="border-left:1px solid #d2d2d2;padding-left:50px;vertical-align:top;width:50%;">
			<table align="left" border="0" cellpadding="1" cellspacing="0" dir="ltr">
			<!-- New Hosted Page -->
			<tr>
				<td></td>
				<td style="text-align:center;width:70px;">Require ID/SSN<br /></td>
				<td style="text-align:center;width:70px;">Require CVV2/CID<br /></td>
				<td style="text-align:center;width:70px;">Require Phone<br /></td>
				<td style="text-align:center;width:70px;">Require eMail<br /></td>
			</tr>
			<tr><td colspan="6"><hr width="100%" size="1" style="border-bottom:dotted 1px #d2d2d2;"></td></tr>
			<tr><td class="MerchantSubHead">HOSTED PAYMENT PAGE V2</td></tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsCreditCard" <% if GetHostedPageSettings("IsCreditCard") then %>checked<% End If %> value="1"/> Allow credit card<br/>
				</td>
				<td align="center"><input type="Checkbox" name="IsCreditCardRequiredID" <% if GetHostedPageSettings("IsCreditCardRequiredID") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsCreditCardRequiredCVV" <% if GetHostedPageSettings("IsCreditCardRequiredCVV") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsCreditCardRequiredPhone" <% if GetHostedPageSettings("IsCreditCardRequiredPhone") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsCreditCardRequiredEmail" <% if GetHostedPageSettings("IsCreditCardRequiredEmail") then %>checked<% End If %> value="1"/></td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsEcheck" <% if GetHostedPageSettings("IsEcheck") then %>checked<% End If %> value="1"> Allow eCheck<br/>
				</td>
				<td align="center"><input type="Checkbox" name="IsEcheckRequiredID" <% if GetHostedPageSettings("IsEcheckRequiredID") then %>checked<% End If %> value="1"></td>
				<td align="center"><input type="Checkbox" disabled /></td>
				<td align="center"><input type="Checkbox" name="IsEcheckRequiredPhone" <% if GetHostedPageSettings("IsEcheckRequiredPhone") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsEcheckRequiredEmail" <% if GetHostedPageSettings("IsEcheckRequiredEmail") then %>checked<% End If %> value="1"/></td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsDirectDebit" <% if GetHostedPageSettings("IsDirectDebit") then %>checked<% End If %> value="1"/> Allow direct debit<br/>
				</td>
				<td align="center"><input type="Checkbox" disabled /></td>
				<td align="center"><input type="Checkbox" disabled /></td>
				<td align="center"><input type="Checkbox" name="IsDirectDebitRequiredPhone" <% if GetHostedPageSettings("IsDirectDebitRequiredPhone") then %>checked<% End If %> value="1"/></td>
				<td align="center"><input type="Checkbox" name="IsDirectDebitRequiredEmail" <% if GetHostedPageSettings("IsDirectDebitRequiredEmail") then %>checked<% End If %> value="1"/></td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsPhoneDebit" <% if GetHostedPageSettings("IsPhoneDebit") then %>checked<% End If %> value="1"/> Allow phone debit<br/>
				</td>
				<td align="center"><input type="Checkbox" disabled /></td>
				<td align="center"><input type="Checkbox" disabled /></td>
				<td align="center"><input type="Checkbox" name="IsPhoneDebitRequiredPhone" <% if GetHostedPageSettings("IsPhoneDebitRequiredPhone") then %>checked<% End If %> value="1"></td>
				<td align="center"><input type="Checkbox" name="IsPhoneDebitRequiredEmail" <% if GetHostedPageSettings("IsPhoneDebitRequiredEmail") then %>checked<% End If %> value="1"></td>
			</tr>
			<tr>
				<td nowrap="nowrap" colspan="5" style="padding-left:20px;">
					<input type="Checkbox" name="IsCustomer" <% if GetHostedPageSettings("IsCustomer") then %>checked<% End If %> value="1"/> Allow Wallet login<br/>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap" colspan="5" style="padding-left:20px;">
					<input type="Checkbox" name="IsWebMoney" <% if GetHostedPageSettings("IsWebMoney") then %>checked<% End If %> value="1"/> Allow WebMoney
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:44px;">Merchant Purse</td>
				<td colspan="4"><input type="text" name="WebMoneyMerchantPurse" value="<%= GetHostedPageSettings("WebMoneyMerchantPurse") %>"/></td>
			</tr>
			<tr>
				<td nowrap="nowrap" colspan="5" style="padding-left:20px;">
					<input type="checkbox" name="IsPayPal" <% if GetHostedPageSettings("IsPayPal") then %>checked<% End If %> value="1"/> Allow PayPal
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:44px;">Merchant ID</td>
				<td colspan="4"><input type="text" name="PayPalMerchantID" value="<%= GetHostedPageSettings("PayPalMerchantID") %>"/></td>
			</tr>
			<tr>
				<td nowrap="nowrap" colspan="5" style="padding-left:20px;">
					<input type="checkbox" name="IsGoogleCheckout" <% if GetHostedPageSettings("IsGoogleCheckout") then %>checked<% End If %> value="1"/> Allow Google Checkout
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:44px;">Merchant ID</td>
				<td colspan="4"><input type="text" name="GoogleCheckoutMerchantID" value="<%= GetHostedPageSettings("GoogleCheckoutMerchantID") %>"/></td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:44px;">Merchant Key</td>
				<td colspan="4"><input type="text" name="GoogleCheckoutMerchantKey" value="<%= GetHostedPageSettings("GoogleCheckoutMerchantKey") %>"/></td>
			</tr>
			<tr>
				<td nowrap="nowrap" colspan="5" style="padding-left:20px;">
					<input type="checkbox" name="IsMoneyBookers" <% if GetHostedPageSettings("IsMoneyBookers") then %>checked<% End If %> value="1"/> Allow Money Bookers
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:44px;">Account</td>
				<td colspan="4"><input type="text" name="MoneyBookersAccount" value="<%= GetHostedPageSettings("MoneyBookersAccount") %>"/></td>
			</tr>
			<tr>
				<td nowrap="nowrap" colspan="5" style="padding-left:20px;">
					<input type="Checkbox" name="IsSignatureOptional" <% if GetHostedPageSettings("IsSignatureOptional") then %>checked<% End If %> value="1"/>
					Don't require MD5 signature<br/>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap" colspan="5" style="padding-left:20px;">
					<input type="Checkbox" name="IsWhiteLabel" <%=IIf(GetHostedPageSettings("IsWhiteLabel"), "checked", "")%> value="1"/>
					Use White-Label view<br/>
				</td>
			</tr>
			<%Call CloseHostedPageSettings()%>
			</tr>
			<tr><td colspan="6"><hr width="100%" size="1" style="border-bottom:dotted 1px #d2d2d2;"></td></tr>
			<tr><td class="MerchantSubHead">PUBLIC PAYMENT PAGE V2</td></tr>
			<tr>
				<td nowrap="nowrap" style="padding-left:20px;">
					<input type="Checkbox" name="IsPublicPayV2" <% If GetPublicPageSettings("IsEnabled") Then %>checked<% End If %> value="1"/> Allow public page<br/>
					<input type="radio" name="PublicPay2_ChargeOptions" <% If GetPublicPageSettings("IsAllowDynamicProduct") = 0 Then %>checked<% End If %> value="0" /> Enable Only Fix Price Items<br />
					<input type="radio" name="PublicPay2_ChargeOptions" <% If GetPublicPageSettings("IsAllowDynamicProduct") Then %>checked<% End If %> value="1" /> Enable Both Fix Price items and open charge items<br />
				</td>
			</tr>
			<%Call ClosePublicPageSettings()%>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td colspan="3" bgcolor="#A9A7A2" height="1"></td></tr>
	<tr><td colspan="3" height="6"></td></tr>
	<tr><td colspan="3" align="right"><input type="submit" value="Save Changes" style="width:110px; height:auto; background-color:#f1f1f1;"></td></tr>
	</table>
	</form>
	<%
end if
%>
	</td>
</tr>
</table>
</body>
<%
rsData.close
Set rsData = nothing
call closeConnection()
%>
</html>