<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoPaging.asp"-->
<!--#include file="../include/func_security.asp" -->
<%
'=======================================================================================================================
'Update show ststus for a payment
If Trim(Request("Action")) = "UpdPayment" Then
	bIsShow = NOT request("isShow")
	sSQL = "UPDATE tblTransactionPay SET isShow = " & CInt(bIsShow) & " WHERE id=" & trim(request("payID"))
	oledbData.Execute sSQL
End if

If Trim(request("Source")) = "filter" Then
	if request("PageSize") = "" then
		session("PageSize") = 20
	else
		session("PageSize") = int(request("PageSize"))
	end if
Elseif Trim(request("TransactionPayID")) <> "" Then
	session("PageSize") = 1
Else
	session("PageSize") = 20
End if

sWhere="WHERE"
sAnd=""
If Trim(request("TransactionPayID")) <> "" Then
	sWhere = sWhere & " (tblTransactionPay.id = " & request("TransactionPayID") & ")"
	sAnd = " AND "
Elseif Trim(request("Source")) = "filter" Then
	sWhere = "WHERE"
	nShowToMerchant=TestNumVar(request("ShowToMerchant"), 0, 1, -1)
	if nShowToMerchant>-1 then
		sWhere=sWhere & sAnd & " tblTransactionPay.isShow=" & nShowToMerchant : sAnd=" AND "
	end if
	WireFlag = TestNumVar(request("WireFlag"), 0, -1, -1)
	if WireFlag > -1 then sWhere = sWhere & sAnd & " WireFlag" & IIF(WireFlag > 0, "=3", "<3") : sAnd=" AND "
	if Trim(request("ShowCompanyID"))<>"" AND Trim(request("ShowCompanyID"))<>"null" then sWhere=sWhere & sAnd & " (tblTransactionPay.CompanyID=" & trim(request("ShowCompanyID")) & ")" : sAnd=" AND "
	if trim(request("toDate"))<>"" then
		dDateMax = Trim(Request("toDate") & " " & Request("toTime"))
		if IsDate(dDateMax) then sWhere=sWhere & sAnd & " (tblTransactionPay.PayDate<='" & CDate(dDateMax) & "')" : sAnd=" AND "
	end if
	if trim(request("fromDate"))<>"" then
		dDateMin = Trim(Request("fromDate") & " " & Request("fromTime"))
		if IsDate(dDateMin) then sWhere=sWhere & sAnd & " (tblTransactionPay.PayDate>='" & CDate(dDateMin) & "')" : sAnd=" AND "
	end if
	if trim(request("IdFrom"))<>"" then sWhere=sWhere & sAnd & " (tblTransactionPay.id>=" & request("IdFrom") & ")" : sAnd=" AND "
	if trim(request("IdTo"))<>"" then sWhere=sWhere & sAnd & " (tblTransactionPay.id<=" & request("IdTo") & ")" : sAnd=" AND "
	if trim(request("amountFrom"))<>"" then
		sWhere=sWhere & sAnd & " ((tblTransactionPay.transPayTotal>=" & request("amountFrom") & " AND tblTransactionPay.Currency=0)"
		sWhere=sWhere & " OR (tblTransactionPay.transPayTotal*tblTransactionPay.ExchangeRate>=" & request("amountFrom") & " AND tblTransactionPay.Currency=1))" : sAnd=" AND "
	end if
	if trim(request("amountTo"))<>"" then
		sWhere=sWhere & sAnd & " ((tblTransactionPay.transPayTotal<=" & request("amountTo") & " AND tblTransactionPay.Currency=0)"
		sWhere=sWhere & " OR (tblTransactionPay.transPayTotal*tblTransactionPay.ExchangeRate<=" & request("amountTo") & " AND tblTransactionPay.Currency=1))" : sAnd=" AND "
	end if
end if
If PageSecurity.IsLimitedMerchant Then
	sWhere = sWhere & sAnd & " tblTransactionPay.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
	sAnd = " AND "
End If
if sWhere="WHERE" then sWhere=""

sQueryString = "&PageSize=" & request("PageSize") & "&page=" & Request("Page") & "&source=" & request("source") & "&ShowCompanyID=" & request("ShowCompanyID")
sQueryString = sQueryString & "&IdFrom=" & trim(request("IdFrom")) & "&IdTo=" & trim(request("IdTo"))
sQueryString = sQueryString & "&amountFrom=" & trim(request("amountFrom")) & "&amountTo=" & trim(request("amountTo"))
sQueryString = sQueryString & "&toDate=" & trim(request("toDate")) & "&fromDate=" & trim(request("fromDate"))

Set rsData = server.createobject("adodb.recordset")
GetConstArray rsData, GGROUP_WireStatus, 1, "", WireStatusText, VbNull
If Request.QueryString("Totals") = "1" Then
	Response.CharSet = "windows-1255"
	Response.Write("<table class=""formThin"" width=""100%"">")
	Response.Write("<tr>")
		Response.Write("<td>TOTALS</td>")
		Response.Write("<td style=""text-align:right;cursor:pointer;float:right;"" onclick=""document.getElementById('tblTotals').style.display='none';""><b>X</b></td>")
	Response.Write("</tr>")
	Response.Write("</table>")
	Response.Write("<table class=""formNormal"" width=""250"" style=""margin:8px 5px;"">")
	Response.Write("<tr>")
		Response.Write("<th style=""text-align:right"" width=""50%"">Count</th>")
		Response.Write("<th style=""text-align:right"">Amount</th>")
	Response.Write("</tr>")
	rsData.Open	"SELECT Currency, Count(*), Sum(TransPayTotal) FROM tblTransactionPay WHERE ID IN" & _
		" (SELECT tblTransactionPay.ID FROM tblTransactionPay" & _
		" LEFT JOIN tblCompany ON tblCompany.ID=tblTransactionPay.CompanyID" & _
		" LEFT JOIN tblWireMoney ON tblTransactionPay.ID=tblWireMoney.WireSourceTbl_id AND tblWireMoney.WireType=1" & _
		sWhere & ") Group By Currency", oleDbData, 0, 1
	Do While Not rsData.EOF
		Response.Write("<tr><td style=""text-align:right"">" & rsData(1) & "</td><td style=""text-align:right"">" & FormatCurr(rsData(0), rsData(2)) & "</td></tr>")
	  rsData.MoveNext
	Loop
	rsData.Close
	Response.Write("</table>")
	Set rsData = nothing
	call closeConnection()
	Response.End()
End If
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<script language="JavaScript" src="../js/func_common.js"></script>
	<script language="JavaScript">
		function toggleEditing(objId) {	
			objRef = document.getElementById("Row" + objId )
			objRef2 = document.getElementById("trRow" + objId )
			if (objRef.innerHTML == '') {
				objRef.innerHTML = '<iframe src="Merchant_transSettledListUpd.asp?TransactionPay_id='+objId+'" framespacing="0" scrolling="No" marginwidth="0" frameborder="0" border="0" width="100%" height="40px"></iframe>'
				objRef2.style.display = 'block';
			}
			else {
				objRef.innerHTML = ''
				objRef2.style.display = 'none';
			}
		}
	</script>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">
<table align="center" border="0" cellpadding="1" cellspacing="2" width="90%">
<tr>
	<td id="pageMainHeadingTd">
		<span id="pageMainHeading">SETTLEMENTS</span>
		<span class="txt14">- SEARCH RESULTS</span><br />
	</td>
	<td valign="top">
		<table border="0" cellspacing="0" cellpadding="1" align="right">
		<tr>
			<td width="6" bgcolor="#6699cc"></td><td width="1"></td>
			<td width="80" bgcolor="#FFFFFF">No Tranfer</td>
			<td width="10"></td>
			<td width="6" bgcolor="#E9C70A"></td><td width="1"></td>
			<td width="80" bgcolor="#FEF8C8">Bank Trasfer</td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<br />
<%
pageSecurityLevel = 0
pageSecurityLang = "ENG"
pageClosingTags = ""
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
%>
<table width="90%" border="0" cellspacing="0" cellpadding="1" align="center">
<%
Set rsData = server.createobject("adodb.recordset")
sSQL="SELECT tblTransactionPay.id AS TransactionPay_id, tblTransactionPay.isShow," & _
	" tblTransactionPay.PayDate, tblTransactionPay.CompanyID, tblTransactionPay.BillingCompanys_id," & _
	" tblTransactionPay.Currency, tblTransactionPay.transTotal, tblTransactionPay.ExchangeRate," & _
	" tblTransactionPay.IsChargeVAT, tblTransactionPay.VATamount, tblTransactionPay.TransChargeTotal," & _
	" tblTransactionPay.TransPayTotal, tblTransactionPay.transRollingReserve," & _
	" tblCompany.CompanyName, tblTransactionPay.InvoiceDocumentID," & _
	" tblTransactionPay.InvoiceNumber, tblWireMoney.WireFlag " & _
	" FROM tblTransactionPay " & _
	" LEFT JOIN tblCompany ON (tblCompany.ID=tblTransactionPay.CompanyID) " & _
	" LEFT JOIN tblWireMoney ON (tblTransactionPay.ID=tblWireMoney.WireSourceTbl_id And tblWireMoney.WireType = 1) " & _
	sWhere & " ORDER BY tblTransactionPay.PayDate DESC"
Call openRsDataPaging(session("PageSize"))
If NOT rsData.EOF Then
	%>
	<tr>
		<td>
			<table class="formNormal" width="100%">	
			<tr>
				<th><br /></th>
				<th>Settlement<br /></th>
				<th>Merchant<br /></th>
				<th>Settlement<br />Amount<br /></th>
				<th>Admin/<br />Fees<br /></th>
				<th>Wire Status<br /></th>
				<th style="text-align:center;">Transactions<br /></th>
				<th style="text-align:center;">Vat<br /></th>
				<th style="text-align:center;">Invoice<br /></th>
				<th style="text-align:center;">Show To<br />Merchant<br /></th>
				<td width="30"></td>
				<th style="text-align:center;" width="40">Comment<br /></th>
			</tr>
			<tr><td height="3"></td></tr>
			<%
			rsData.MoveFirst
			Do until rsData.EOF
				
				sCompanyID = trim(rsData("CompanyID"))
				sCompanyName = trim(rsData("CompanyName"))
				If rsData("WireFlag") = 3 Then
					sTdBkgColorFirst = "style=""background-color:#E9C70A;"""
					xStyle = "style=""background-color:#fff4b3;"""
				Else
					sTdBkgColorFirst = "style=""background-color:#6699cc;"""
					xStyle = "style=""background-color:#FFFFFF;"""
				End If
				%>
				<tr>
					<td width="3" <%=sTdBkgColorFirst%>><img src="../images/1_space.gif" width="1" height="1" border="0"><br /></td>
					<td <%=xStyle%>><%= rsData("TransactionPay_id") %><br /><%= FormatDatesTimes(rsData("PayDate"),1,1,0) %><br /></td>
					<td <%=xStyle%>>
						<%
						response.write(sCompanyID & "<br />")
						If int(len(dbtextShow(sCompanyName)))<25 then
							response.write(dbtextShow(sCompanyName))
						else
							%>
							<span style="cursor:help;text-decoration:underline;" title="<%= dbtextShow(sCompanyName) %>"><%= left(dbtextShow(sCompanyName),24) & ".." %></span><br />
							<%
						end if
						%>
					</td>
					<td <%=xStyle%>>
						<%
						response.write(FormatCurr(rsData("Currency"), rsData("transPayTotal")) & "<br />")
						If CCur(rsData("ExchangeRate")) > 0 Then 
							If rsData("Currency") > 0 Then
								response.write("<span style=""color:gray;"">" & FormatCurr(0, rsData("transPayTotal") * rsData("ExchangeRate")) & "*</span><br />")
							Else
								response.write("<span style=""color:gray;"">" & FormatCurr(0, rsData("transPayTotal")) & "*</span><br />")
							end if
						End if		
						%>
					</td>
					<td <%=xStyle%>>
						<%
						IF rsData("transRollingReserve")<0 Then Response.Write("<span style=""color:red;"">") Else Response.Write("<span>") End if
						Response.Write(FormatCurr(rsData("Currency"),rsData("transRollingReserve")) & "</span><br />")
						Response.Write(FormatCurr(rsData("Currency"), rsData("TransChargeTotal")))
						%>
					</td>
					<td <%=xStyle%>>
						<% 
						sSQL="SELECT WireMoney_id, WireStatus, WireStatusDate FROM tblWireMoney WHERE WireType=1 AND WireSourceTbl_id=" & rsData("TransactionPay_id")
						set rsData2 = oledbData.Execute(sSQL)
						If rsData2.EOf Then
							Response.Write("---")
						Else
							Response.Write("<a href=""Wire_search_data.asp?WireMoneyId=" & rsData2("WireMoney_id") & """>" & rsData2("WireMoney_id") & "</a> - ")
							Response.Write(WireStatusText(TestNumVar(rsData2("WireStatus"), 0, 7, 0)) & "<br />")
							If rsData2("WireStatusDate")<>"01/01/1900" Then Response.Write("(" & FormatDatesTimes(rsData2("WireStatusDate"),1,0,0) & ")<br />")
						End If
						rsData2.close
						Set rsData2 = nothing
						%>
					</td>
					<td style="text-align:center;" nowrap="nowrap" <%=xStyle%>>
						<a href="Merchant_transSettledDetail.asp?companyID=<%= sCompanyID %>&payID=<%= rsData("TransactionPay_id") %>&payDate=<%= rsData("paydate") %>" style="text-decoration:underline;">Show</a><br />
						<a href="javascript:OpenPop('invoicePrint.asp?PayID=<%= rsData("TransactionPay_id") %>','BillingWin',680,470,1);" style="text-decoration:underline;">Print</a><br />
					</td>
					<td style="text-align:center;" <%=xStyle%>>
						<%
						if rsData("IsChargeVAT") then
							response.write(rsData("VATamount")*100) & "%<br />"
						else
							response.write("---<br />")
						end if
						%>
					</td>
					<td style="text-align:center; color:gray;" <%=xStyle%>>
						<%
						If rsData("InvoiceDocumentID")>0 then
							Response.Write("<a href=""invoices.aspx?ID=" & rsData("InvoiceDocumentID") & "&TransactionPay=" & rsData("TransactionPay_id") & """ target=""fraSearchBody"" style=""text-decoration:underline;""><span style=""font-size:11px;"">" & rsData("InvoiceNumber") & "</span></a><br />")
						Else
							If int(rsData("TransChargeTotal"))>0 Then
								%>
								<a href="Billing_create_payment.asp?payID=<%= rsData("TransactionPay_id") %>&companyID=<%= sCompanyID %>&BillingCompanys_id=<%= rsData("BillingCompanys_id") %><%= sQueryString %>" onclick="return confirm('Do you really want to create an invoice for this payment?');"><img src="../images/b_create.gif" alt="" width="35" height="13" border="0"></a><br />
								<%
							Else
								Response.Write("<img src=""../images/b_create_off.gif"" width=""35"" height=""13"" border=""0"" /><br />")
							End if 
						End If
						%>
					</td>
					<td style="text-align:center;" <%=xStyle%>>
						<%
						btnTxt = "checkbox.gif"
						If NOT rsData("isShow") Then btnTxt = "x.gif"
						response.write "<img src=""../images/" & btnTxt & """ align=""top"" height=""16"" border=""0""> &nbsp;"
						%>
						[<a href="Merchant_transSettledListData.asp?Action=UpdPayment&isShow=<%=rsData("isShow")%>&payID=<%=rsData("TransactionPay_id") & "&" & sQueryString %>" style="text-decoration:underline;">Change</a>]
					</td>
					<td bgcolor="white"></td>
					<td style="text-align:center;" <%=xStyle%>>
						<a onclick="toggleEditing('<%= rsData("TransactionPay_id") %>')" name="<%=rsData("TransactionPay_id")%>" style="text-decoration:underline;cursor:pointer;"><img src="../images/b_showWhiteEng.gif"></a><br />
					</td>
				</tr>
				<tr id="trRow<%=rsData("TransactionPay_id")%>" style="display:none;"><td id="Row<%=rsData("TransactionPay_id")%>" colspan="12"></td></tr>
				<tr>
					<td height="1" colspan="10" bgcolor="#c5c5c5"></td>
					<td></td>
					<td height="1" bgcolor="#c5c5c5"></td>
				</tr>
				<%
				nShowCounter=nShowCounter+1		
				rsData.movenext
			loop
			%>
			<tr><td height="12"></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="1" cellspacing="0">
			<tr>
				<td><% call showPaging("Merchant_transSettledListData.asp", "Eng", UrlWithout(Cstr(Request.QueryString), "page")) %></td>
				<td align="right">
					<%
					totalsQueryString = "?Totals=1&" & Request.QueryString
					%>				
					<input type="button" id="TotalsButton" onclick="with(document.getElementById('tblTotals')){style.display='block';if(onresize)onresize();};setAjaxHTML('<%= totalsQueryString %>', document.getElementById('tblTotals'), true);" value="SHOW TOTALS &Sigma;">
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<%
Else
	%>
	<tr>
		<td>
			<hr width="98%" size="1" color="#C0C0C0" noshade>
			<br />Did not find any settlements !<br />
			<hr width="98%" size="1" color="#C0C0C0" noshade>
		</td>
	</tr>
	<%
End If
call rsData.close
call closeConnection()
%>
</table>
<div id="tblTotals" onresize="PosObject(this, document.getElementById('TotalsButton'), 'RRTB', 0, -10);" style="border-color:#484848; border-style:solid; border-width:1px 3px; position:absolute;width:260px;background-color:White;display:none;"> &nbsp; LOADING DATA... </div>
</body>
</html>