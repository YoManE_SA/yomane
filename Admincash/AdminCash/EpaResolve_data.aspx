﻿<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    Dim sWhere As String
    Private Sub Update_Click(sender As Object, e As EventArgs)
        Dim updateIds() as String = Request.Form.GetValues("PostTrans")
        If updateIds Is Nothing Then Exit Sub
        For i As Integer = 0 To updateIds.Length - 1
            dbPages.ExecSql("Exec ForceEpaMatch " & dbPages.TestVar(updateIds(i), 0, -1, 0) & ", " & dbPages.TestVar(Request("trans_" & updateIds(i)), 0, -1, 0))
        Next
    End Sub
    
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Server.ScriptTimeout = 5000
        If Request("ShowID") <> "" Then 
            Response.Write("<input type=""hidden"" name=""PostTrans"" value=""" & Request("ShowID") & """/>")
            Response.Write("<br /><table class=""tableThin"">")
            Response.Write("<tr><th></th><th>Trans ID</th><th>Trans Date</th><th>Merchant</th><th>Approval Num</th><th>Debit Reference</th></tr>")
            Dim pReader As SqlDataReader = dbPages.ExecReader("Select GetEpaMatchingTransactions.*, tblCompany.CompanyName From GetEpaMatchingTransactions(" & dbPages.TestVar(Request("ShowID"), 0, -1, 0) & ") " & _
                " Left Join tblCompany ON(tblCompany.ID = GetEpaMatchingTransactions.CompanyID)")
            While pReader.Read()
                Response.Write("<tr>")
                Response.Write("<td><input type=""radio"" name=""trans_" & Request("ShowID") & """ value=""" & pReader("ID") & """/></td>")
                Response.Write("<td>" & pReader("ID") & "</td>")
                Response.Write("<td>" & pReader("InsertDate") & "</td>")
                Response.Write("<td>" & pReader("CompanyName") & "</td>")
                Response.Write("<td>" & pReader("ApprovalNumber") & "</td>")
                Response.Write("<td>" & IIf(pReader("DebitReferenceCode")<>"",pReader("DebitReferenceCode"),"---") & "</td>")
                Response.Write("</tr>")
            End While
            pReader.Close()
            Response.Write("</table><br />")
            Response.End()
        End If
        
		Security.CheckPermission(lblPermissions)
        MyBase.OnLoad(e)

        Dim recType As Integer = dbPages.TestVar(Request("recType"), 0, -1, 2)
        Select Case recType
            Case 0
                sWhere = "Not (TransCount Is Null)"
            Case 1
                sWhere = "TransCount = 0"
            Case 2
                sWhere = "TransCount > 1"
        End Select
        
	    Dim dtValue as DateTime = dbPages.TestVar(Request("ImpDateFrom") & " " & Request("ImpDateFromTime"), DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue)
        If dtValue <> DateTime.MinValue Then sWhere &= " And (InsertDate >= '" & dtValue & "')"
	    dtValue = dbPages.TestVar(Request("ImpDateTo") & " " & Request("ImpDateToTime"), DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue)
        If dtValue <> DateTime.MinValue Then sWhere &= " And (InsertDate <= '" & dtValue & "')"
    		
	    dtValue = dbPages.TestVar(Request("TransDateFrom") & " " & Request("TransDateFromTime"), DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue)
        If dtValue <> DateTime.MinValue Then sWhere &= " And (TransDate >= '" & dtValue & "')"
	    dtValue = dbPages.TestVar(Request("TransDateTo") & " " & Request("TransDateToTime"), DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue)
        If dtValue <> DateTime.MinValue Then sWhere &= " And (TransDate <= '" & dtValue & "')"

        If IsNumeric(Request("amountFrom")) Then sWhere &= " And (Amount >= " & Request("amountFrom") & ")"
        If IsNumeric(Request("amountTo")) Then sWhere &= " And (Amount <= " & Request("amountTo") & ")"
        If IsNumeric(Request("iCurrency")) Then sWhere &= " And (Currency = " & Request("iCurrency") & ")"
        
        If IsNumeric(Request("IdFrom")) Then sWhere &= " And (ID >= " & Request("IdFrom") & ")"
        If IsNumeric(Request("IdTo")) Then sWhere &= " And (ID <= " & Request("IdTo") & ")"
        
        If Request("TerminalNo") <> "" Then sWhere &= " And (TerminalNumber = '" & Replace(Request("TerminalNo"), "'", "''") & "')"
        If Request("ApprovalNo") <> "" Then sWhere &= " And (ApprovalNumber = '" & Replace(Request("ApprovalNo"), "'", "''") & "')"
        
        PGData.PageSize = dbPages.TestVar(Request("PageSize"), 0, -1, PGData.PageSize)
    End Sub
    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
    <title></title>
	<script type="text/javascript" language="JavaScript" src="../js/func_common.js"></script>
	<script type="text/javascript">
		function showMatches(epaID) {
		    with (document.getElementById('EpaRow_' + epaID)) {
				if (style.display != '') style.display = ''; else style.display = 'none';
				document.getElementById('EpaRowImg_' + epaID).src = (style.display != '' ? '/NPCommon/Images/tree_expand.gif' : '/NPCommon/Images/tree_collapse.gif');
				if (!cells[1].childNodes.Length) setAjaxHTML('?ShowID=' + epaID, cells[1], true);
			}
		}
	</script>
</head>
<body class="itext">
    <form id="form1" runat="server">
	<table align="center" style="width:92%" border="0" cellpadding="0" cellspacing="1">
		<tr>
			<td><asp:Label ID="lblPermissions" runat="server" /> <span id="pageMainHeading">EPA Resolve</span><br /><br /></td>
			<td align="right">
		        <table border="0" cellspacing="0" cellpadding="1">
		        <tr>
			        <td width="6" bgcolor="#6699cc"></td><td width="1"></td>
			        <td style="font-size:10px;">Multiple Matching</td>
			        <td width="10"></td>
			        <td width="6" bgcolor="#E9C70A"></td><td width="1"></td>
			        <td style="font-size:10px;">No Matching</td>
		        </tr>
		        </table>	
			</td>
		</tr>
	</table>
	<br />
	<style> TR.IsRefund TD{ color:Maroon; } </style>
	<%If Request("Action") <> "" Then%>
    <table align="center" style="width:92%" border="0" cellpadding="0" cellspacing="1" class="formNormal">
        <tr>
            <th width="0">&nbsp;</th>
            <th width="20">&nbsp;</th>
            <th>ID</th>
            <th>Import Date</th>
            <th>Trans Date</th>
            <th>Terminal</th>
            <th>Debit Ref.</th>
            <th>Approval</th>
            <th>Amount</th>
            <th>Installment</th>
            <th>Matches</th>
            <th>File Name</th>
        </tr>
        <%
        PGData.OpenDataset("Select * From tblEpaPending Where " & sWhere & " Order By ID Desc")
        While PGData.Read() 
            If PGData("TransCount") > 0 Then btnUpdate.Visible = True : btnReset.Visible = True
        %>
        <tr class="color:<%=IIF(PGData("IsRefund"), "Marron", "")%>;">
    	    <td width="3" style="background-color:#<%=IIF(PGData("TransCount") > 0, "6699cc", "E9C70A")%>;"><img src="../images/1_space.gif" width="1" height="1" border="0"><br /></td>
            <td>
            <%If PGData("TransCount") > 0 Then %>
                <img src="/NPCommon/Images/tree_expand.gif" onclick="showMatches(<%=PGData("ID")%>)" id="EpaRowImg_<%=PGData("ID")%>" />
            <%Else%>
                <img src="/NPCommon/Images/tree_disabled.gif" id="EpaRowImg_<%=PGData("ID")%>" />
            <%End If%>
            </td>
            <td><%=PGData("ID")%></td>
            <td><%=PGData("InsertDate")%></td>
            <td><%=PGData("TransDate")%></td>
            <td><%=PGData("TerminalNumber")%></td>
            <td><%=PGData("DebitRefCode")%></td>
            <td><%=PGData("ApprovalNumber")%></td>
				<td>
					<%
						If Convert.IsDBNull(PGData("Currency")) Then Response.Write("UNKNOWN") Else Response.Write(dbPages.FormatCurr(PGData("Currency"), PGData("Amount")))
					%>
				</td>
            <td><%=PGData("Installment")%></td>
            <td><%=PGData("TransCount")%></td>
            <td><%=PGData("StoredFileName")%></td>
        </tr>
        <tr id="EpaRow_<%=PGData("ID")%>" style="display:none;">
            <td colspan="3"></td>
            <td colspan="8"></td>
        </tr>
        <tr>
            <td height="1" colspan="12" bgcolor="#C0C0C0"></td>
        </tr>
        <%
        End while
        PGData.CloseDataset()
        %>
	</table>
	<%End If %>
	<br />
    <table align="center" style="width:92%" border="0" cellpadding="0" cellspacing="1" class="formNormal">
		<tr>
			<td><UC:Paging runat="Server" id="PGData" PageID="PageID" PageSize="50" /></td>
			<td style="text-align:right;">
				<asp:Button runat="server" OnClick="Update_Click" ID="btnUpdate" Text="Update" Visible="false" />
				<input value="Reset" type="reset" runat="server" id="btnReset" Visible="false" onclick="if(!confirm('Sure to reset selection?')) return false;" />
			</td>
		</tr>
	</table>
    </form>
</body>
</html>
