<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import namespace="System.IO" %>

<script runat="server">
	Dim sSQL, StyleBkgColor As String
	
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
	End Sub
	
	Sub btnAddAffiliate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		If String.IsNullOrEmpty(txtNewAffiliate.Text) Then
			lblAddAffiliate.Text = "Please specify new affiliate name!"
		Else
			lblAddAffiliate.Text = String.Empty
			Dim aff As New Netpay.Bll.Affiliates.Affiliate()
			aff.Name = txtNewAffiliate.Text
			aff.Save()
			txtNewAffiliate.Text = String.Empty
			Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">")
			Response.Write("Reload();")
			Response.Write("</s" & "cript>")
		End If
	End Sub		
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>Partners</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<script language="javascript">
		var vOldTr = null;
		function setStyle(trSec){
			if(vOldTr) vOldTr.style.backgroundColor='';
			(vOldTr = trSec).style.backgroundColor='#bcd5fe';
		}
	</script>
</head>
<body bgcolor="#eeeeee" class="itextm3">
	<table align="center" border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr>
		<td>
			<asp:label ID="lblPermissions" runat="server" />
			<span id="pageMainHeading">Partners Management</span><br />
			<%If Trim(Request("showAll"))="" Then%><a href="?showAll=1">Show full list</a><%End if%>
		</td>
	</tr>
	</table>
	<br />
	<form id="frmServer" runat="server">
		<table class="formNormal" align="center" width="95%" border="0" style="border:1px solid gray;">
		<tr>
			<td>		
				Add New <asp:TextBox ID="txtNewAffiliate" runat="server" />
				<asp:Button ID="btnAddAffiliate" CssClass="buttonWhite" runat="server" Text="ADD" OnClick="btnAddAffiliate_Click" /><br />
				<asp:Label ID="lblAddAffiliate" runat="server" ForeColor="maroon" />
			</td>
		</tr>
		</table>
	</form>
	<br />
	<table class="formNormal" align="center" width="95%" border="0" cellspacing="1" cellpadding="0">
	<tr><td height="6" colspan="4"></td></tr>
	<tr>
		<th colspan="2" style="background-color:#d8d8d8;">ID</th>		
		<th style="background-color:#d8d8d8;">Name</th>		
		<th class="Vertical" width="20" style="background-color:#d8d8d8;">Merchants</th>
		<th class="Vertical" width="20" style="background-color:#d8d8d8;"> Leads </th>
		<th class="Vertical" width="20" style="background-color:#d8d8d8;">Settlements</th>
	</tr>
	<tr><td height="6" colspan="5"></td></tr>
	<%
		sSQL = "SELECT a.affiliates_id, a.name, a.IsActive, " & _
		" Count(distinct sa.Merchant_id) as Companies, " & _
		" (SELECT COUNT(AFCID) FROM tblAffiliatesCount WHERE AFCAFFID=a.Affiliates_id) Refs, " & _
		" Count(distinct tp.id) as AffPayments " & _
		" From tblAffiliates a" & _
		" Left Join [Setting].[SetMerchantAffiliate] sa ON(sa.Affiliate_id = a.affiliates_id)" & _
		" Left Join tblTransactionPay tp ON(sa.Merchant_id = tp.CompanyID)" & _
		" Left Join tblAffiliatePayments p ON (AFP_Affiliate_ID=a.affiliates_id And tp.id = p.AFP_TransPaymentID) " & _
		  IIf(Request("showAll") = "", " Where IsActive=1", "") & _
		" Group By a.affiliates_id, a.name, a.IsActive" & _
		" ORDER BY Name"
		Dim iReader As SqlDataReader = dbPages.ExecReader(sSQL)
		Dim sStatusColor As String
		If iReader.HasRows Then
			While iReader.Read
				sStatusColor = "#66CC66"
				If Not iReader("IsActive") Then
					sStatusColor = "#FF6666"
				End If
			%>
			<tr id="trLines" onclick="setStyle(this);" style="height:18px;">
				<td><span style="background-color:<%= sStatusColor %>;">&nbsp;&nbsp;</span></td>					
				<td><%= iReader("affiliates_id") %></td>
				<td><a target="frmBody" href="Partners_data.aspx?id=<%= iReader("affiliates_id") %>&CountCompNum=<%=Server.URLEncode(iReader("Companies"))%>"><%= iReader("name") %></a></td>
				<%If iReader("Companies")>0 then %>
					<td style="text-align:center;"><a target="frmBody" href="Partners_Companies.aspx?id=<%= iReader("affiliates_id") %>&name=<%=Server.URLEncode(iReader("name"))%>"><%=iReader("Companies")%></a></td>
				<%Else%>
					<td style="color:#888282;text-align:center;">-</td>
				<%End If %>
				<%If iReader("Refs")>0 then %>
				    <td nowrap style="text-align:center;"><a target="frmBody" href="Partners_Refferals.aspx?AffiliateID=<%= iReader("affiliates_id") %>&name=<%=Server.URLEncode(iReader("name"))%>"><%=iReader("Refs")%></a></td>
				<%Else%>
					<td nowrap style="color:#888282;text-align:center;">-</td>
				<%End If %>
				<%If iReader("AffPayments")>0 then %>
				<td style="text-align:center;"><a target="frmBody" href="Partners_Payments.aspx?AffiliateID=<%= iReader("affiliates_id") %>&name=<%=Server.URLEncode(iReader("name"))%>"><%=iReader("AffPayments")%></a></td>
				<%Else %>
				<td style="color:#888282;text-align:center;">-</td>
				<%End If %>
			</tr> 
			<td height="1" colspan="6" bgcolor="#c5c5c5"></td>
			<%
		End While
	Else
		%>
		<tr><td height="5"></td></tr>
		<tr><td colspan="5" class="txt12">No records found !<br /></td></tr>
		<tr><td><br /></td></tr>
		<%
	End if
	iReader.Close()
	%>
	</table>	
</body>
</html>