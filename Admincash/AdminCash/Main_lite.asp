<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_controls.asp"-->
<!--#include file="../include/const_globalData.asp" -->

<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
Function drawNote(fType, fMsgCount, fMsgHeading, fMsgBody, fLink)
	Select Case fType
		Case "NoteAlert" : sImgName = "pageCurl_pink"
		Case "Note" : sImgName = "pageCurl_yellow"
	End Select
	Response.Write("<table cellspacing=""0"" cellpadding=""0"" class=""tblNote"">")
	Response.Write("<tr>")
		Response.Write("<td width=""16%"" align=""center"" rowspan=""3"" class=""txt28"" style=""color:#484848; background-color:#ffffff;"">" & fMsgCount & "</td>")
		Response.Write("<td class=""tbl" & fType & "Inside"" height=""60"" valign=""top"">")
			Response.Write("<table width=""98%"" align=""right"" cellspacing=""0"" cellpadding=""0"">")
			Response.Write("<tr>")
				Response.Write("<td class=""txt12b"" style=""color:#484848;padding:4px;"">" & fMsgHeading & "</td>")
				Response.Write("<td align=""right"" rowspan=""3"" valign=""top""><img src=""../images/" & sImgName & ".gif""></td>")
			Response.Write("</tr>")
			Response.Write("<tr><td class=""txt11"" style=""padding-left:4px;"">" & fMsgBody & "</td></tr>")
			Response.Write("<tr><td height=""7""></td></tr>")
			Response.Write("</table>")
		Response.Write("</td>")
	Response.Write("</tr>")
	Response.Write("</table>")
End function
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
	<style type="text/css">
		.tblNote {
			width:92%;
			border-left:1px solid #a1a1a1;
			border-bottom:1px solid #a1a1a1;
			/*filter:progid:DXImageTransform.Microsoft.Shadow(color='#b0b0b0', Direction=225, Strength=2);*/
			}
		.tblNote a { color:black; }
		.tblNoteInside {
			background-color:#FFEEC2;
			/*FILTER: progid:DXImageTransform.Microsoft.Alpha(style=1,opacity=100,finishOpacity=50,startX=0,finishX=0,startY=60,finishY=100);*/
			}
		.tblNoteAlertInside {
			background-color:#ffe1e1;
			/*FILTER: progid:DXImageTransform.Microsoft.Alpha(style=1,opacity=100,finishOpacity=50,startX=0,finishX=0,startY=60,finishY=100);*/
			}
	</style>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">
<br />
<table border="0" width="98%" height="90%" cellspacing="0" cellpadding="1" align="center">
<tr>
	<td valign="top" align="center" width="32%">
		<!--#include file="main_fastLinks.asp" -->
	</td>
	<td width="1" bgcolor="#484848"></td>
	<td valign="top" align="center" width="33%">
<!--		<table width="330" align="center" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td id="pageMainHeading">TRACKING<br /><br /></td>
		</tr>
		</table>
		<br />
		<iframe src="main_graph1.aspx" noresize frameborder="0" style="width:340px; height:270px; padding:0; margin:0;"></iframe>
		<br />
		<iframe src="main_graph3.aspx" noresize frameborder="0" style="width:340px; height:270px; padding:0; margin:0;"></iframe>-->
	</td>
	<td width="1" bgcolor="#484848"></td>
	<td valign="top" align="center" width="35%">
		<!--#include file="main_reminders.asp" -->
	</td>
</tr>
</table>
</body>
</html>