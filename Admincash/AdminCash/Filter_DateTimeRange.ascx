<%@ Control Language="VB" ClassName="Filter_DateTimeRange" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="radC" %>

<script runat="server">
	Private sFromDateFieldName As String = "fromDate"
	Private sTitleFieldName As String = "DateRange"
	
	Public Property FromDateFieldName() As String
		Get
			Return sFromDateFieldName
		End Get
		Set(ByVal value As String)
			sFromDateFieldName = value
		End Set
	End Property
	
	Private mTitle As String = "Date Range"
	Public Property Title() As String
		Get
			Return mTitle
		End Get
		Set(ByVal value As String)
			mTitle = value
			LitTitle.Text = value
		End Set
	End Property

	
	Private sFromTimeFieldName As String = "fromTime"
	Public Property FromTimeFieldName() As String
		Get
			Return sFromTimeFieldName
		End Get
		Set(ByVal value As String)
			sFromTimeFieldName = value
		End Set
	End Property
		
	Private sToDateFieldName As String = "toDate"
	Public Property ToDateFieldName() As String
		Get
			Return sToDateFieldName
		End Get
		Set(ByVal value As String)
			sToDateFieldName = value
		End Set
	End Property
		
	Private sToTimeFieldName As String = "toTime"
	Public Property ToTimeFieldName() As String
		Get
			Return sToTimeFieldName
		End Get
		Set(ByVal value As String)
			sToTimeFieldName = value
		End Set
	End Property

	Public Property FromDateTime() As Date
		Get
			Return rdtpFrom.SelectedDate
		End Get
		Set(ByVal value As Date)
			rdtpFrom.SelectedDate = value
		End Set
	End Property
		
	Public Property ToDateTime() As Date
		Get
			Return rdtpTo.SelectedDate
		End Get
		Set(ByVal value As Date)
			rdtpTo.SelectedDate = value
		End Set
	End Property
		
	Sub Page_Load()
		Dim sbJS As New StringBuilder
		sbJS.AppendLine("<s" & "cript language=""JavaScript"" type=""text/javascript"">")
		sbJS.AppendLine("	function ConvertFormToClient(oForm, sAction)")
		sbJS.AppendLine("	{")
		sbJS.AppendLine("		for(var i=0;i<oForm.elements.length;i++) if ((oForm.elements[i].name.substring(0, 2)==""__"")||(oForm.elements[i].id.substring(0, 2)==""__"")) oForm.elements[i].disabled=true;")
		sbJS.AppendLine("		if(sAction) oForm.action=sAction;")
		sbJS.AppendLine("	}")

		sbJS.AppendLine("	function Synchronize_DateTimeRange(sControlField, sDateField, sTimeField)")
		sbJS.AppendLine("	{")
		sbJS.AppendLine("		var sDateTime=document.getElementById(sControlField).value;")
		sbJS.AppendLine("		document.getElementById(sDateField).value=sDateTime.substr(8,2)+""/""+sDateTime.substr(5,2)+""/""+sDateTime.substr(0,4);")
		sbJS.AppendLine("		document.getElementById(sTimeField).value=sDateTime.substr(11,2)+"":""+sDateTime.substr(14,2)+"":""+sDateTime.substr(17,2);")
		sbJS.AppendLine("		if (document.getElementById(sDateField).value.length<8) document.getElementById(sDateField).value="""";")
		sbJS.AppendLine("		if (document.getElementById(sTimeField).value.length<5) document.getElementById(sTimeField).value="""";")
		sbJS.AppendLine("	}")
		sbJS.AppendLine("</s" & "cript>")
		Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Filter_DateTimeRange", sbJS.ToString, False)
		
        Dim nHashFrom As Integer = System.Math.Abs(rdtpFrom.ClientID.GetHashCode)
        Dim nHashTo As Integer = System.Math.Abs(rdtpTo.ClientID.GetHashCode)
		litFromDate.Text = "<input type=""hidden"" name=""" & FromDateFieldName & """ id=""" & FromDateFieldName & """ />"
		litFromTime.Text = "<input type=""hidden"" name=""" & FromTimeFieldName & """ id=""" & FromTimeFieldName & """ />"
		litFromScript.Text = "<s" & "cript language=""javascript"" type=""text/javascript"">"
		litFromScript.Text &= "	setInterval(""Synchronize_DateTimeRange('" & rdtpFrom.ClientID & "', '" & FromDateFieldName & "', '" & FromTimeFieldName & "');"", 50);"
		litFromScript.Text &= "</s" & "cript>"
		litToDate.Text = "<input type=""hidden"" name=""" & ToDateFieldName & """ id=""" & ToDateFieldName & """ />"
		litToTime.Text = "<input type=""hidden"" name=""" & ToTimeFieldName & """ id=""" & ToTimeFieldName & """ />"
		litToScript.Text = "<s" & "cript language=""javascript"" type=""text/javascript"">"
		litToScript.Text &= "	setInterval(""Synchronize_DateTimeRange('" & rdtpTo.ClientID & "', '" & ToDateFieldName & "', '" & ToTimeFieldName & "');"", 50);"
		litToScript.Text &= "</s" & "cript>"
	End Sub
</script>
<table width="100%" class="filterNormal">
<tr><th colspan="2"><asp:Literal ID="LitTitle" Text="Date Range" runat="server" /></th></tr>
<tr>
	<td style="vertical-align:middle;">From</td>
	<td style="white-space:nowrap;">
		<radC:RadDateTimePicker ID="rdtpFrom" Calendar-Width="200px" RadControlsDir="~/Include/RadControls/" Calendar-SkinsPath="~/Include/RadControls/Calendar/Skins" Calendar-Skin="WebBlue" EnableViewState="false"
		 TimeView-Interval="00:30" TimeView-Columns="4" Culture="en-GB" TimeView-SkinsPath="~/Include/RadControls/Calendar/Skins" TimeView-Skin="WebBlue" runat="server" Width="150px" />
		<asp:Literal ID="litFromDate" runat="server" />
		<asp:Literal ID="litFromTime" runat="server" />
		<asp:Literal ID="litFromScript" runat="server" />
	</td>
</tr>
<tr>
	<td style="vertical-align:middle;">To</td>
	<td style="white-space:nowrap;">
		<radC:RadDateTimePicker  ID="rdtpTo" RadControlsDir="~/Include/RadControls/" Calendar-SkinsPath="~/Include/RadControls/Calendar/Skins" Calendar-Skin="WebBlue" EnableViewState="false"
		 TimeView-Interval="00:30" TimeView-Columns="4" Culture="en-GB" TimeView-SkinsPath="~/Include/RadControls/Calendar/Skins" TimeView-Skin="WebBlue" runat="server" Width="150px" />
		<asp:Literal ID="litToDate" runat="server" />
		<asp:Literal ID="litToTime" runat="server" />
		<asp:Literal ID="litToScript" runat="server" />
	</td>
</tr>
</table>
