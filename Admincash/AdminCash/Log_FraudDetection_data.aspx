<%@ Page Language="VB" EnableViewStateMac="false" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
	Dim sSQL As String, sQueryWhere As String, nFromDate As String, nToDate As String, sDateDiff As String, _
	sStatusColor As String, sAnd As String, sQueryString As String, sTmpTxt As String, sDivider As String
	Dim i As Integer = 0, nTmpNum As Integer
	Dim sStatusTxtPrelim As String = ""

	Function DisplayContent(sVar As String) As String
		DisplayContent = "<span class=""key"">" & replace(replace(dbPages.dbtextShow(sVar),"=","</span> = "),"|"," <br /><span class=""key"">")
	End Function
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		nFromDate = Trim(Request("fromDate")) & " " & Trim(Request("fromTime"))
		nToDate = Trim(Request("toDate")) & " " & Trim(Request("toTime"))
		If nFromDate <> " " Then sQueryWhere &= sAnd & "(f.insertDate >= '" & nFromDate.ToString() & "')" : sAnd = " AND "
		If nToDate <> " " Then sQueryWhere &= sAnd & "(f.insertDate <= '" & nToDate.ToString() & "')" : sAnd = " AND "
		
		If Trim(Request("transStatus")) <> "" Then
			Select Case Trim(Request("transStatus"))
				Case "0" : sQueryWhere &= sAnd & "(f.IsProceed = 0) " : sAnd = " AND "
				Case "1" : sQueryWhere &= sAnd & "(f.TransPending_id IS NOT NULL OR f.TransPreAuth_id IS NOT NULL OR TransPass_id IS NOT NULL)" : sAnd = " AND "
				Case "2" : sQueryWhere &= sAnd & "(f.TransFail_id IS NOT NULL) " : sAnd = " AND "
			End Select
		End If
		sStatusTxtPrelim = ""
		If Trim(Request("iStatus")) <> "" Then
			Select Case Trim(Request("iStatus"))
				Case "1" : sQueryWhere &= sAnd & "(f.IsProceed = 0)" : sAnd = " AND " : sStatusTxtPrelim = "Failure"
				Case "2" : sQueryWhere &= sAnd & "(f.IsProceed = 1 AND f.TransPass_id IS NOT NULL) " : sAnd = " AND " : sStatusTxtPrelim = "Charged"
				Case "3" : sQueryWhere &= sAnd & "(f.IsProceed = 1 AND f.TransFail_id IS NOT NULL) " : sAnd = " AND " : sStatusTxtPrelim = "Rejected"
				Case "4" : sQueryWhere &= sAnd & "(f.IsProceed = 1 AND f.TransPending_id IS NOT NULL) " : sAnd = " AND " : sStatusTxtPrelim = "Pending"
				Case "5" : sQueryWhere &= sAnd & "(f.IsProceed = 1 AND f.TransPreAuth_id IS NOT NULL) " : sAnd = " AND " : sStatusTxtPrelim = "Pre-Auth"
			End Select
		End If

        If dbPages.TestVar(Request("ID"), 1, 0, 0) > 0 Then sQueryWhere &= sAnd & "f.FraudDetection_id=" & Request("ID") : sAnd = " AND "
        If dbPages.TestVar(Request("CompanyID"), 1, 0, 0) > 0 Then sQueryWhere &= sAnd & "f.Merchant_id=" & Request("CompanyID") : sAnd = " AND "
        If dbPages.TestVar(Request("iMearchantID"), 1, 0, 0) > 0 Then sQueryWhere &= sAnd & "f.Merchant_id=" & Request("iMearchantID") : sAnd = " AND "
		If dbPages.TestVar(Request("amountFrom"), 1D, 0D, 0D) > 0D Then sQueryWhere &= sAnd & "f.transAmount>=" & Request("amountFrom") : sAnd = " AND "
		If dbPages.TestVar(Request("amountTo"), 1D, 0D, 0D) > 0D Then sQueryWhere &= sAnd & "f.transAmount<=" & Request("amountTo") : sAnd = " AND "
		If dbPages.TestVar(Request("Currency"), 0, eCurrencies.MAX_CURRENCY, -1) >= 0 Then sQueryWhere &= sAnd & "f.transCurrency IN (" & Request("Currency") & ")" : sAnd = " AND "
		If Trim(Request("ReferenceCode")) <> "" Then sQueryWhere &= sAnd & "f.ReferenceCode='" & dbPages.DBText(Request("ReferenceCode")) & "'" : sAnd = " AND "
		If Trim(Request("iReplyCode")) <> "" Then
			If Trim(Request("iReplyCode")).ToLower = "null" Or Trim(Request("iReplyCode")) = "---" Then
				sQueryWhere &= sAnd & "f.replyCode IS NULL"
			Else
				sQueryWhere &= sAnd & "f.replyCode = '" & Trim(Request("iReplyCode")) & "'"
			End If
			sAnd = " AND "
		End If

		If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") _
		 Else PGData.PageSize = 50
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		If sQueryWhere <> "" Then sQueryWhere = "WHERE " & sQueryWhere
		If Request("SQL2") = "1" Then
			dbPages.CurrentDSN = 2
			Response.Write(dbPages.ShowCurrentDSN)
		Else
			dbPages.CurrentDSN = 1
		End If
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript">
		function IconVisibility(fVar) {
			spanObj = document.getElementById(fVar)
			if (spanObj.style.visibility == 'hidden') {
				spanObj.style.visibility = 'visible';
			}
			else {
				spanObj.style.visibility = 'hidden';
			}
		}
		function toggleInfo(TransId, TableType) {
			objRef = document.getElementById('Row' + TransId)
			if (objRef.innerHTML == '') {
				objRef.innerHTML = '<iframe onload="this.height=this.contentWindow.document.body.scrollHeight;" framespacing="0" scrolling="auto" align="right" marginwidth="0" frameborder="0" width="100%" height="100%" src="trans_detail_cc.asp?transID=' + TransId + '&PaymentMethod=1&bkgColor=f5f5f5&isAllowAdministration=false&isShowBasicTransData=true&TransTableType=' + TableType + '"></iframe>';
			}
		}
		function ExpandNode(fTrNum, TransId, TableType) {
			trObj = document.getElementById("trInfo" + fTrNum)
			imgObj = document.getElementById("oListImg" + fTrNum)
			if (trObj) {
				if (trObj.style.display == '') {
					imgObj.src = '../images/tree_expand.gif';	
					trObj.style.display = 'none';
				}
				else {
					imgObj.src = '../images/tree_collapse.gif';
					trObj.style.display = '';
					toggleInfo(TransId,TableType); 
				}
			}
		}
	</script>
</head>
<body>
	<form id="Form1" runat="server">

		<table align="center" style="width:92%" border="0">
		<tr>
			<td>
				<asp:label ID="lblPermissions" runat="server" />
				<span id="pageMainHeading">RISK MANAGEMENT LOG</span>
			</td>
			<td valign="top">
				<table border="0" cellspacing="0" cellpadding="1" align="right">
				<tr>
					<td width="4" bgcolor="#66cc66" align="center" style="height:15px;font-size:10px;"></td>
					<td width="2" style="height:15px;font-size:10px"></td>
					<td width="60" style="height:15px;font-size:10px">Processed</td>
					<td width="13" style="height:15px;font-size:10px"></td>
					<td width="4" bgcolor="#ff6666" align="center" style="height:15px;font-size:10px;"></td>
					<td width="2" style="height:15px;font-size:10px"></td>
					<td width="60" style="height:15px;font-size:10px">Rejected</td>
					<td width="13" style="height:15px;font-size:10px"></td>
					<td width="4" bgcolor="#606060" align="center" style="height:15px;font-size:10px;"></td>
					<td width="2" style="height:15px;font-size:10px"></td>
					<td width="150" style="height:15px;font-size:10px;font-weight:bold; color:#616161;">Risk Management Failed</td>
				</tr>
				</table>			
			</td>			
		</tr>
		<tr><td><br /></td></tr>
		<tr>
			<td style="font-size:11px; padding-bottom:4px;">
				(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');">Collapse All</a>)
			</td>
			<td align="right" style="font-size:11px;">
				Filters:
				<%
				If Trim(Request("iMerchantID"))<>"" Then Response.Write(sDivider & "<a onclick=""location.href='?"& dbPages.SetUrlValue(sQueryString,"iMerchantID",Nothing) &"';"">Merchant</a>") : sDivider = ", "
					If Trim(Request("iStatus")) <> "" Then Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "iStatus", Nothing) & "';"">Status</a>") : sDivider = ", "
				If sDivider <>"" Then Response.Write(" | (click to remove)") _
					Else Response.Write("Add to filter by clicking on returned fields")
				%>
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%" border="0">
		<tr>
			<th colspan="2" style="width:30px;"><br /></th>
			<th>Date</th>
			<th>Merchant</th>
			<th>Payment Method</th>
			<th>Amount</th>	
			<th>Score Allowed<br />Old / New</th>
			<td width="10"></td>		
			<th>Return Score<br /> Old / New</th>
			<th>Return<br />Bin</th>
			<th>Reference #</th>
			<td width="10"></td>
			<th>Status</th>			
		</tr>
		<%
			Dim bLimitedMerchant As Boolean = Security.IsLimitedMerchant
			Dim bLimitedDC As Boolean = Security.IsLimitedDebitCompany
		    sSQL = "SELECT f.*, tblCompany.CompanyName " & _
		    "FROM Log.FraudDetection AS f INNER JOIN tblCompany WITH (NOLOCK) ON f.Merchant_id = tblCompany.ID " & _
		    " " & sQueryWhere & " " & _
			"ORDER BY f.insertDate DESC"
			'Response.Write(sSQL)
			'Response.End
			PGData.OpenDataset(sSQL)
			
			Dim sDate As String = ""
			Dim statusTxt As String = ""
			Dim sTdStyle As String = ""
			Dim sTdBkgColorFirst As String = ""
			Dim tableType As String = ""
			Dim transid As Integer = -1
			Dim sStatParam As String = ""
			
			Dim sExpandFirst As String = String.Empty
			
			While PGData.Read()
				i = i + 1
				sDate = ""
				statusTxt = ""
				sTdStyle = ""
				sTdBkgColorFirst = ""
				tableType = ""
				transid = -1
				sStatusColor = ""
				sStatParam = ""
				
				If Day(PGData("InsertDate")) < 10 Then
					sDate = sDate & "0"
				End If
				sDate = sDate & Day(PGData("InsertDate")) & "/"
				If Month(PGData("InsertDate")) < 10 Then
					sDate = sDate & "0"
				End If
				sDate = sDate & Month(PGData("InsertDate")) & "/" & Right(Year(PGData("InsertDate")), 2) & "&nbsp;&nbsp;"
				sDate = sDate & FormatDateTime(PGData("InsertDate"), 4)
			
				If Not PGData("IsProceed") Then
					statusTxt = "Failure"
					sStatParam = "1"
					sTdBkgColorFirst = "#606060"
					sStatusColor = "#606060"
					sTdStyle = "font-weight:bold; color:#6118749;"
					tableType = "fail"
					If Not IsDBNull(PGData("TransFail_id")) Then
						transid = PGData("TransFail_id")
					End If
				Else
					sTdBkgColorFirst = "#66cc66"
					sStatusColor = "#66cc66"
					If Not IsDBNull(PGData("TransPass_id")) Then
						statusTxt = "Charge Approved"
						sStatParam = "2"
						tableType = "pass"
						transid = PGData("TransPass_id")
					ElseIf Not IsDBNull(PGData("TransFail_id")) Then
						sTdBkgColorFirst = "#ff6666"
						sStatusColor = "#ff6666"
						statusTxt = "Charge Rejected"
						sStatParam = "3"
						tableType = "fail"
						transid = PGData("TransFail_id")
					ElseIf Not IsDBNull(PGData("TransPending_id")) Then
						statusTxt = "Pending"
						sStatParam = "4"
						tableType = "pending"
						transid = PGData("TransPending_id")
					ElseIf Not IsDBNull(PGData("TransPreAuth_id")) Then
						statusTxt = "Pre-Auth"
						sStatParam = "5"
						tableType = "approval"
						transid = PGData("TransPreAuth_id")
					End If
				End If
			
				If Trim(PGData("replyCode")) isnot nothing Then statusTxt = statusTxt & " <span style=""text-size:10px;"">(" & PGData("replyCode") & ")</span>"
				If String.IsNullOrEmpty(sExpandFirst) then sExpandFirst="ExpandNode('" & i & "','" & transid & "','" & tableType & "');"
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
				<td><img onclick="ExpandNode('<%=i%>','<%=transid%>','<%=tableType%>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle"><br /></td>
				<td><span Style="background-color:<%= sStatusColor %>;">&nbsp;</span></td>
				<td style="font-size:11px; <%=sTdStyle %>" nowrap="nowrap"><%=sDate%></td>
				<td style="white-space:nowrap; <%=sTdStyle %>">
					<%
					IF NOT IsDBNull(PGData("CompanyName")) Then
						If Trim(Request("iMerchantID"))<>"" Then Response.Write(PGData("CompanyName")) _
							Else dbPages.showFilter(PGData("CompanyName"), "?" & sQueryString & "&iMerchantID=" & PGData("Merchant_id"), 1, sTdStyle)
					End If
					%><br />
				</td>
				<td style="<%=sTdStyle %>">
					<%
					If PGData("BinCountry") isnot nothing Then
						Response.Write("<img src=""/NPCommon/ImgCountry/18X12/" & PGData("BinCountry") & ".gif"" align=""middle"" border=""0"" />")
						Response.Write(" &nbsp;" & PGData("BinCountry"))
					End If
					Response.Write(" " & Trim("" & PGData("PaymentMethodDisplay")))
					%>
				</td>
				<td style="direction:ltr; <%=sTdStyle %>"><%=dbPages.FormatCurr(PGData("transCurrency"), PGData("transAmount"))%><br /></td>
				<td style="<%=sTdStyle %>">
					<%=FormatNumber(PGData("allowedScore"), 2, -1, 0, 0)%> /
					<%
					If Convert.IsDBNull(PGData("allowedRiskScore")) Then
						Response.Write("--")
					ElseIf PGData("AllowedRiskScore")<0 Then
						Response.Write("--")
					Else 
						Response.Write(FormatNumber(PGData("allowedRiskScore"), 2, -1, 0, 0))
					End if
					%>
				</td>
				<td></td>				
				<td style="<%=sTdStyle %>">
					<%
						If PGData("ReturnScore") >= 0 Or PGData("ReturnRiskScore") >= 0 Then
							Response.Write(FormatNumber(PGData("ReturnScore"), 2, -1, 0, 0))
							If PGData("IsTemperScore") Then Response.Write("<sup>1</sup>")
							If PGData("IsTemperScore") And PGData("IsCountryWhitelisted") Then Response.Write("<sup>,</sup>")
							If PGData("IsCountryWhitelisted") Then Response.Write("<sup>2</sup>")
							Response.Write(" / " & FormatNumber(PGData("ReturnRiskScore"), 2, -1, 0, 0))
						Else
							Response.Write("Error")
						End If
					%><br />				
				</td>
				<td style="<%=sTdStyle %>"><%= IIf(PGData("ReturnBinCountry") is nothing, PGData("ReturnBinCountry"),"--")%></td>
				<td style="<%=sTdStyle %>"><%=PGData("ReferenceCode") & IIf(PGData("isDuplicateAnswer"), "<sup>3</sup>", String.Empty)%></td>	
				<td></td>				
				<td style="<%=sTdStyle %>">			
					<%
					If Trim(Request("iStatus")) <> "" Then Response.Write(statusTxt) _
					 Else dbPages.showFilter(statusTxt, "?" & sQueryString & "&iStatus=" & sStatParam, 1, sTdStyle)
					%><br />
				</td>				
			</tr>
			<tr id="trInfo<%=i%>" style="display:none;">				
				<td style="direction:ltr; padding:12px 0px; border-top:1px dashed #e2e2e2;" colspan="13">
					<table class="formNormal" style="width:100%;">
					<tr>
						<td valign="top" align="left" nowrap="nowrap">
							<span style="text-decoration:underline;">Answer:</span><br />
									<%
									    If Not IsDBNull(PGData("ReturnAnswer")) Then
									        Response.Write(Replace(Replace(Replace(Replace(PGData("ReturnAnswer"), "&lt;", "<"), "&gt;", ">"), ";", "<br />"), "``key``", """key"""))
									    Else
									        Response.Write("---")
									    End If
									%>
						</td>
						<td style="padding-right:25px;" valign="top" width="77%">
							<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td align="left">
									<span style="text-decoration:underline;">Explanation:</span><br />
									<%
									    If Not IsDBNull(PGData("ReturnExplanation")) Then
									        Response.Write(Replace(Replace(Replace(Replace(PGData("ReturnExplanation"), "&lt;", "<"), "&gt;", ">"), ";", "<br />"), "``key``", """key"""))
									    Else
									        Response.Write("---")
									    End If
									%>
								</td>
							</tr>
							<tr><td><br /></td></tr>
							<tr>
								<td align="left">
									<span style="text-decoration:underline;">Sending:</span><br />
									<%=Replace(Replace(PGData("SendingString"), "&", " &"), "%2E", ".")%>
								</td>
							</tr>
							<tr><td><br /></td></tr>
							<tr><td id="Row<%= transID %>"></td></tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="1" colspan="7" bgcolor="#c5c5c5"></td>
				<td></td>
				<td height="1" colspan="3" bgcolor="#c5c5c5"></td>
				<td></td>
				<td height="1" colspan="1" bgcolor="#c5c5c5"></td>
			</tr>
			<%
		End while
		PGData.CloseDataset()
		If i <> 1 Then sExpandFirst = String.Empty
		%>
		</table>
		<script language="JavaScript">
			function ExpandAll(fDisplay, fImgStatus) {
				for(i = 0; i <= <%= i %>; i++) {
					trObj = document.getElementById("trInfo" + i)
					imgObj = document.getElementById("oListImg" + i)
					if (trObj) {
						imgObj.src = '../images/tree_' + fImgStatus + '.gif';
						trObj.style.display = fDisplay;
					}
				}
			}
			<%= sExpandFirst %>
		</script>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" align="middle"> ADD TO FILTER
		</div>
		
	</form>
	
	<table align="center" style="width:92%">
	<tr>
		<td>
			<UC:Paging runat="Server" id="PGData" PageID="PageID" />
		</td>
		<td style="text-align:right; font-size:11px;">
			<sup>1</sup> Deducted 2.5 points for free email | <sup>2</sup> Deducted 2.5 points for whitelisted country | <sup>3</sup> Data was recycled<br />	
		</td>
	</tr>
	</table>
		
</body>
</html>
