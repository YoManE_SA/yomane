﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminCash/FiltersMaster.master" AutoEventWireup="true" CodeFile="ApplicationIdentity_list.aspx.cs" Inherits="AdminCash_ApplicationIdentity_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
	<form id="Form1" runat="server">
		<h1><asp:Label ID="lblPermissions" runat="server" /> Application Identities<span> - Filter</span></h1>
		<a href="ApplicationIdentity_data.aspx" target="frmBody" style="float:right" >Add new identity</a>
		<asp:CheckBox runat="server" ID="chkActiveOnly" Text="Show only active" AutoPostBack="true" Checked="true" CssClass="txt12" /><br />
	</form>
	<br />
	<table class="formNormal" width="100%">
		<tr>
			<th width="10">A</th>
			<th width="10%">ID</th>
			<th>Name</th>
		</tr>
		<asp:Repeater runat="server" ID="rptList">
			<ItemTemplate>
				<tr>
					<td><asp:Label runat="server" ID="lblStatus" Text="&nbsp;&nbsp;&nbsp;" BackColor='<%# (bool)Eval("IsActive") ? System.Drawing.Color.Green : System.Drawing.Color.Red %>' /></td>
					<td><asp:Literal runat="server" ID="ltID" Text='<%# Eval("ID") %>' /></td>
					<td><asp:HyperLink Target="frmBody" runat="server" NavigateUrl='<%# "ApplicationIdentity_data.aspx?ID=" + Eval("ID") %>' Text='<%# Eval("Name") %>' /></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</asp:Content>

