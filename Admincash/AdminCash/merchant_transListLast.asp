<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_transPay.asp"-->
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css">
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;">


<table cellpadding="1" cellspacing="2" width="100%">
<tr>
    <td id="pageMainHeadingTd"><span id="pageMainHeading">LAST TRANSACTIONS</span></td>
</tr>
</table>
<br />
<%
pageSecurityLevel = 0
pageSecurityLang = "ENG"
pageClosingTags = ""
Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
%>
<table class="formNormal" style="width:100%; padding:0; margin:0;">
<tr>
	<th colspan="2" valign="bottom"><img src="../images/1_space.gif" alt="" width="1" height="1" border="0"><br /></th>
	<th valign="bottom">ID /<br />Time<br /></th>
	<th valign="bottom">Merchant's Name /<br />Payment Method</th>
	<td width="24""></td>
	<th width="100" valign="bottom">Details<br /></th>
</tr>
<tr><td height="3"></td></tr>
<%
nComCount = 0
for i=0 to 3
	sCheckDate = DateAdd("d", -i, date())
	sCheckDay = day(sCheckDate)
	sDateDiff = DateDiff("d",sCheckDate,date())
	
	dDateMax = cDate(sCheckDate) & " 23:59:59"
	dDateMin = cDate(sCheckDate) & " 00:00:00"
	
	sSQL="SELECT tblCompanyTransPass.ID, tblCompanyTransPass.CompanyID, tblCompanyTransPass.TransSource_id, tblCompany.CompanyName, " & _
	" tblCompanyTransPass.InsertDate, tblCompanyTransPass.Amount, tblCompanyTransPass.Currency, tblCompanyTransPass.payID, tblCompanyTransPass.DeniedStatus, " & _
	" tblCompanyTransPass.CreditType, tblCompanyTransPass.Payments, tblCompanyTransPass.PaymentMethodDisplay, /*CASE tblCompanyTransPass.DebitCompanyID WHEN 21 THEN 21 ELSE dbo.GetRandomDC(DatePart(SECOND, tblCompanyTransPass.InsertDate)) END */ tblCompanyTransPass.DebitCompanyID, tblCompanyTransPass.PaymentMethod, " & _
	" tblCompanyTransPass.PaymentMethod_id, tblCompanyTransPass.isTestOnly, List.TransSource.Name AS TransactionTypeName, tblCreditCard.BINCountry, pm.name AS 'pm_Name' " & _
	" FROM tblCompanyTransPass LEFT JOIN List.PaymentMethod AS pm ON tblCompanyTransPass.PaymentMethod = pm.PaymentMethod_id" & _
	" LEFT JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID" & _
	" LEFT JOIN tblCompany ON(tblCompanyTransPass.CompanyID = tblCompany.ID)" & _
	" LEFT JOIN List.TransSource ON tblCompanyTransPass.TransSource_id = List.TransSource.TransSource_id" & _
	" WHERE (tblCompanyTransPass.PayID <> ';X;' And tblCompanyTransPass.paymentMethod <> " & PMD_RolRes & ")" & _
	" AND (tblCompanyTransPass.TransSource_id<>13) AND (tblCompanyTransPass.InsertDate BETWEEN '" & dDateMin & "' and '" & dDateMax & "')"
	If PageSecurity.IsLimitedMerchant Then sSQL = sSQL & " AND tblCompany.ID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))"
	sSQL=sSQL & " ORDER BY tblCompanyTransPass.InsertDate DESC"
	set rsData=oledbData.execute(sSQL)
	If not rsData.EOF Then
	    Set fsServer=Server.CreateObject("Scripting.FileSystemObject")
		If nComCount < 25 Then
			If int(nComCount)>0 Then response.write "<tr><td height=""30""></td></tr>"
			%>
			<tr>
				<td colspan="6" class="txt11b">
					<span class="txt11b"><%= sCheckDate %></span>
					<span>
					<%
					select case sDateDiff
						case "0"
							response.write "(Today)<br />"
						case "1"
							response.write "(Yesterday)<br />"
						case else
							response.write "(" & sDateDiff & " Days ago)<br />"
					end select
					%>
					</span>
				</td>
			</tr>
			<tr><td height="6"></td></tr>
			<tr>
				<td height="1" colspan="4" bgcolor="silver"></td>
				<td></td>
				<td height="1" colspan="1" bgcolor="silver"></td>
			</tr>
			<%
		end if
		
		Do until rsData.EOF OR nComCount = 25
			nComCount = nComCount+1
			'Get dispaly Param
			Call Fn_TransParam(rsData, sTrBkgColor, sTdBkgColorFirst, sTdFontColor, sPayStatusText, bShowDeniedOptions, bAllowDelete)
			%>
			<tr bgcolor="<%= sTrBkgColor %>">
				<td width="6" valign="top"><span style="height:18px; width:100%; background-color:<%= sTdBkgColorFirst %>;"></span></td>
				<td width="3"></td>
				<td dir="ltr" nowrap valign="top" style="color:<%= sTdFontColor %>;">
				    <%= rsData("id") %><br />
				    <%= FormatDatesTimes(rsData("InsertDate"),0,1,0) %><br />
				</td>
				<td valign="top" style="color:<%= sTdFontColor %>;">
					<%
					If int(len(dbtextShow(rsData("CompanyName"))))<28 then
						response.write dbtextShow(rsData("CompanyName"))
					else
						%>
						<span style="cursor:help;text-decoration:underline;" title="<%= dbtextShow(rsData("CompanyName")) %>"><%= left(dbtextShow(rsData("CompanyName")),25) & ".." %></span>
						<%
					end if
					Response.Write("<br/>")
		            If rsData("PaymentMethod")>14 Then
                        Response.Write("<img src=""/NPCommon/ImgPaymentMethod/23X12/" & rsData("PaymentMethod") & ".gif"" alt=""" & rsData("pm_Name") & """ align=""middle"" border=""0"" /> ")
		            End if
			        If NOT isNull(rsData("BinCountry")) Then
                        If Trim(rsData("BinCountry"))<>"" Then
                            Response.write("<img src=""/NPCommon/ImgCountry/18X12/" & Trim(rsData("BinCountry")) & ".gif"" title=""" & rsData("BinCountry") & """ align=""middle"" border=""0"" /> &nbsp;")
                        End if
                    End if
					if trim(rsData("PaymentMethodDisplay"))<>"" Then response.write rsData("PaymentMethodDisplay") & "<br />"
					%>
				</td>
				<td bgcolor="#ffffff"></td>
				<td valign="top" nowrap style="color:<%= sTdFontColor %>;">
			        <%
					response.write "<span dir=""ltr"">"
					response.write FormatCurrWCT(rsData("Currency"), rsData("Amount"), rsData("CreditType"))
					response.write "</span>"
					response.write " &nbsp;" & rsData("Payments") & " Ins. &nbsp;&nbsp;&nbsp;"
					Select case sPayStatusText
						case "Unsettled"
						%>
						<a href="merchant_transPass.asp?companyID=<%= rsData("companyID") %>&transID=<%= rsData("ID") %>&currency=<%=rsData("Currency")%>&isFilter=1" style="text-decoration:underline; font-size:11px;" onclick="parent.fraBrowserButton.FrmResize();">Show &gt;&gt;</a>
						<%
						case "Settled"
							if nTypeCredit<>"8" then
							%>
							<a href="Merchant_transSettledDetail.asp?companyID=<%= rsData("companyID") %>&payID=<%= replace(rsData("payID"),";","") %>&transID=<%= rsData("ID") %>" style="text-decoration:underline; font-size:11px;" onclick="parent.fraBrowserButton.FrmResize();">Show &gt;&gt;</a>
							<%
							End If
						case "Archive"
						%>
						<a class="go" target="fraBrowser" href="common_framesetTabB.aspx?pageMenuWidth=280&pageMenuUrl=<%=Server.UrlEncode("Trans_admin_regularFilter.aspx?FilterMerchantDefaultID=" & rsData("companyID") & "&Payout=A" )%>&pageBodyUrl=<%=Server.UrlEncode("Trans_admin_regularData.aspx?ShowCompanyID=" & rsData("companyID") & "&Payout=A&chk2=1&fromDate=" & Server.UrlEncode(DateAdd("m", -6, Now)) )%>">Show &gt;&gt;</a>
						<%
					End select
					Response.Write("<br/>")
			        If trim(rsData("DebitCompanyID"))<>"" Then
				        If fsServer.FileExists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" & trim(rsData("DebitCompanyID")) & ".gif")) Then
				            Response.write("<img src=""/NPCommon/ImgDebitCompanys/23X12/" & Trim(rsData("DebitCompanyID")) & ".gif"" align=""middle"" border=""0"" /> &nbsp;")
				        End if
				    End if
					If trim(rsData("TransactionTypeName"))<>"" Then response.write rsData("TransactionTypeName")
					%>
				</td>
			</tr>
			<tr>
				<td height="1" colspan="4" bgcolor="silver"></td>
				<td></td>
				<td height="1" colspan="1" bgcolor="silver"></td>
			</tr>
			<%
		rsData.movenext
		loop
        Set fsServer=Nothing
	end if
next
call closeConnection()
%>
</table>
</body>
</html>