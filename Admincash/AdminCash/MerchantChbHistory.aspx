<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import namespace="System.Data.OleDb"%>
<%@ Import Namespace="System.IO" %>
<script runat="server">
	'
	' 20100915 Tamir
	'
	' Chargeback history report by merchant & currency
	'
	Dim nMerchant As Integer

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		nMerchant = Request("ID")
		If Not Page.IsPostBack Then
			dsCurrency.ConnectionString = dbPages.DSN
			lblMerchant.Text = nMerchant & ": " & dbPages.ExecScalar("SELECT CompanyName FROM tblCompany WHERE ID=" & nMerchant)
			For i As Integer = 0 To 5
				ddlYear.Items.Add(Date.Today.Year - i)
			Next
			For i As Integer = 1 To 12
				ddlMonth.Items.Add(New ListItem(i.ToString("00"), i))
			Next
			ddlMonth.SelectedIndex = IIf(Date.Today.Month = 1, 11, Date.Today.Month - 2)
		End If
	End Sub
	
	Sub GetReport(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		Dim nCurrency As Integer = ddlCurrency.SelectedValue, nYear As Integer = ddlYear.SelectedValue, nMonth As Integer = ddlMonth.SelectedValue
		Dim sMonth As String = nMonth.ToString("00")
		Dim sSQL As String = "SELECT * FROM GetChargebackHistoryFormData(" & nMerchant & ", " & nCurrency & "," & nMonth & ", " & nYear & ") ORDER BY Year, MonthID"
		Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
		If drData.HasRows Then
			Dim sExcelFolder As String = Server.MapPath("/Data/ChbHistory")
			Dim sExcelFile As String = sExcelFolder & "\merchant_chb_history_form_" & nMerchant & "_" & nCurrency & "_" & nYear & "_" & sMonth & ".xls"
			FileSystem.FileCopy(sExcelFolder & "\merchant_chb_history_form.xls", sExcelFile)
			Dim dbExcel As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sExcelFile & ";Mode=ReadWrite;Extended Properties=""Excel 8.0;HDR=Yes;"";")
			dbExcel.Open()
			Dim cmdExcel As OleDbCommand = dbExcel.CreateCommand()
			cmdExcel.CommandType = Data.CommandType.Text
			Dim nCount As Integer = 0
			While drData.Read
				nCount += 1
				sSQL = "UPDATE [Data$] SET [Merchant]='" & drData("Merchant") & "', [Currency]='" & drData("Currency") & "'" & _
				", [Month]='" & drData("Month") & "', [Year]='" & drData("Year") & "'" & _
				", [SaleCountVisa]=" & drData("SaleCountVisa") & ", [SaleAmountVisa]=" & drData("SaleAmountVisa") & _
				", [ChbCountVisa]=" & drData("ChbCountVisa") & ", [ChbAmountVisa]=" & drData("ChbAmountVisa") & _
				", [SaleCountMC]=" & drData("SaleCountMC") & ", [SaleAmountMC]=" & drData("SaleAmountMC") & _
				", [ChbCountMC]=" & drData("ChbCountMC") & ", [ChbAmountMC]=" & drData("ChbAmountMC") & _
				" WHERE LineID=" & nCount & ";"
				cmdExcel.CommandText = sSQL
				cmdExcel.ExecuteNonQuery()
			End While
			dbExcel.Close()
			Response.Redirect("/Data/ChbHistory/merchant_chb_history_form_" & nMerchant & "_" & nCurrency & "_" & nYear & "_" & sMonth & ".xls", True)
		End If
		drData.Close()
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Security</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<form id="frmServer" runat="server">
		<h1>
			<asp:label ID="lblPermissions" runat="server" />
			Chargeback History<span> - Merchant <asp:Label ID="lblMerchant" CssClass="item" runat="server" /></span>
		</h1>
		<div class="bordered">
			<span style="float:right;">
				<asp:Button CssClass="buttonWhite buttonWide" OnClick="GetReport" Text="Get Report &nbsp; &nbsp;" style="width:auto;background-position:right;background-repeat:no-repeat;background-image:url('../images/icon_xls.gif')" runat="server" />
			</span>
			Currency
			<asp:DropDownList ID="ddlCurrency" DataSourceID="dsCurrency" DataTextField="Name" DataValueField="ID" Font-Names="Monospace" runat="server" />
			&nbsp;
			<asp:SqlDataSource ID="dsCurrency" DataSourceMode="DataReader" runat="server"
			 SelectCommand="SELECT -1 ID, '' Name UNION SELECT CUR_ID, CUR_ISOName+' '+CUR_FullName FROM tblSystemCurrencies ORDER BY ID" />
			Up to month	<asp:DropDownList ID="ddlMonth" runat="server" /> year <asp:DropDownList ID="ddlYear" runat="server" />
		</div>
	</form>
</body>
</html>