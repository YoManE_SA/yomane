<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<%
nWireMoney_id = TestNumVar(Request("WireMoney_id"), 0, -1, 0)

sQueryString = "PageSize=" & request("PageSize") & "&from=" & request("from") & "&WireType=" & request("WireType") & "&ShowCompanyID=" & request("ShowCompanyID")
sQueryString = sQueryString & "&IdFrom=" & trim(request("IdFrom")) & "&IdTo=" & trim(request("IdTo")) & "&amountFrom=" & trim(request("amountFrom")) & "&amountTo=" & trim(request("amountTo"))
sQueryString = sQueryString & "&toDate=" & trim(request("toDate")) & "&fromDate=" & trim(request("fromDate"))
sQueryString = sQueryString & "&wireStatus0=" & trim(request("wireStatus0")) & "&wireStatus1=" & trim(request("wireStatus1")) & "&wireStatus2=" & trim(request("wireStatus2")) & "&wireStatus3=" & trim(request("wireStatus3"))

if trim(request("Action"))="UPDATE" then

	if trim(request("WireStatus"))<>"4" AND trim(request("WireStatus"))<>"3" then
		if isDate(trim(request("WireDate"))) then
			sSQL="UPDATE tblWireMoney SET " &_
			"wireAmount=" & trim(request("wireAmount")) & ", " &_
			"WireExchangeRate=" & trim(request("ExchangeRate")) & " " &_
			"WHERE WireMoney_id=" & nWireMoney_id
			oledbData.Execute sSQL
		end if
	end if
		
	sSQL="UPDATE tblWireMoney SET" &_
	" WireCompanyLegalNumber=Left('" & DBText(request("CompanyLegalNumber")) & "',15)," &_
	" WirePaymentMethod=Left('" & DBText(request("PaymentMethod")) & "',80)," &_
	" WirePaymentBank=" & trim(request("PaymentBank")) &"," &_
	" WirePaymentBranch=Left('" & DBText(request("PaymentBranch")) &"',80)," &_
	" WirePaymentAccount=Left('" & DBText(request("PaymentAccount")) & "',80)," &_
	" WirePaymentPayeeName=Left('" & DBText(request("PaymentPayeeName")) & "',50)," &_
	" WireIDNumber=Left('" & trim(request("IDNumber")) & "',15)," &_
	" WirePaymentAbroadAccountName=Left('" & DBText(request("PaymentAbroadAccountName")) & "',80)," &_
	" WirePaymentAbroadAccountNumber=Left('" & request("PaymentAbroadAccountNumber") & "',80)," &_
	" WirePaymentAbroadBankName=Left('" & DBText(request("PaymentAbroadBankName")) & "',80)," &_
	" WirePaymentAbroadBankAddress=Left('" & DBText(request("PaymentAbroadBankAddress")) & "',200)," &_
	" WirePaymentAbroadBankAddressSecond=Left('" & DBText(request("PaymentAbroadBankAddressSecond")) & "',100)," &_
	" WirePaymentAbroadBankAddressCity=Left('" & DBText(request("PaymentAbroadBankAddressCity")) & "',30)," &_
	" WirePaymentAbroadBankAddressState=Left('" & DBText(request("PaymentAbroadBankAddressState")) & "',20)," &_
	" WirePaymentAbroadBankAddressZip=Left('" & DBText(request("PaymentAbroadBankAddressZip")) & "',20)," &_
	" WirePaymentAbroadBankAddressCountry=" & TestNumVar(request("PaymentAbroadBankAddressCountry"), 0, -1, 0) & "," &_
	" WirePaymentAbroadSwiftNumber=Left('" & request("PaymentAbroadSwiftNumber") & "',80), " &_
	" WirePaymentAbroadIBAN=Left('" & request("PaymentAbroadIBAN") & "',80), " & _
	" WirePaymentAbroadABA=Left('" & request("PaymentAbroadABA") & "',80)," & _
	" WirePaymentAbroadSortCode=Left('" & request("PaymentAbroadSortCode") & "',80)," & _
	" WirePaymentAbroadAccountName2=Left('" & DBText(request("PaymentAbroadAccountName2")) & "',80)," &_
	" WirePaymentAbroadAccountNumber2=Left('" & request("PaymentAbroadAccountNumber2") & "',80)," &_
	" WirePaymentAbroadBankName2=Left('" & DBText(request("PaymentAbroadBankName2")) & "',80)," &_
	" WirePaymentAbroadBankAddress2=Left('" & DBText(request("PaymentAbroadBankAddress2")) & "',200)," &_
	" WirePaymentAbroadBankAddressSecond2=Left('" & DBText(request("PaymentAbroadBankAddressSecond2")) & "',100)," &_
	" WirePaymentAbroadBankAddressCity2=Left('" & DBText(request("PaymentAbroadBankAddressCity2")) & "',30)," &_
	" WirePaymentAbroadBankAddressState2=Left('" & DBText(request("PaymentAbroadBankAddressState2")) & "',20)," &_
	" WirePaymentAbroadBankAddressZip2=Left('" & DBText(request("PaymentAbroadBankAddressZip2")) & "',20)," &_
	" WirePaymentAbroadBankAddressCountry2=" & TestNumVar(request("PaymentAbroadBankAddressCountry2"), 0, -1, 0) & "," &_
	" WirePaymentAbroadSwiftNumber2=Left('" & request("PaymentAbroadSwiftNumber2") & "',80), " &_
	" WirePaymentAbroadIBAN2=Left('" & request("PaymentAbroadIBAN2") & "',80)," & _
	" WirePaymentAbroadABA2=Left('" & request("PaymentAbroadABA2") & "',80)," & _
	" WirePaymentAbroadSortCode2=Left('" & request("PaymentAbroadSortCode2") & "',80)" & _
	" WHERE WireMoney_id=" & nWireMoney_id
	oledbData.Execute sSQL
	
	if trim(request("isUpdateMaster"))="true" then
		sSQL="UPDATE tblCompany SET" &_
		" CompanyLegalNumber='" & DBText(request("CompanyLegalNumber")) & "'," &_
		" PaymentMethod='" & DBText(request("PaymentMethod")) & "'," &_
		" PaymentBank=" & trim(request("PaymentBank")) &"," &_
		" PaymentBranch='" & DBText(request("PaymentBranch")) &"'," &_
		" PaymentAccount='" & DBText(request("PaymentAccount")) & "'," &_
		" PaymentPayeeName='" & DBText(request("PaymentPayeeName")) & "'," &_
		" IDNumber='" & trim(request("IDNumber")) & "'," &_
		" PaymentAbroadAccountName='" & DBText(request("PaymentAbroadAccountName")) & "'," &_
		" PaymentAbroadAccountNumber='" & request("PaymentAbroadAccountNumber") & "'," &_
		" PaymentAbroadBankName='" & DBText(request("PaymentAbroadBankName")) & "'," &_
		" PaymentAbroadBankAddress='" & DBText(request("PaymentAbroadBankAddress")) & "'," &_
		" PaymentAbroadBankAddressSecond='" & DBText(request("PaymentAbroadBankAddressSecond")) & "'," &_
		" PaymentAbroadBankAddressCity='" & DBText(request("PaymentAbroadBankAddressCity")) & "'," &_
		" PaymentAbroadBankAddressState='" & DBText(request("PaymentAbroadBankAddressState")) & "'," &_
		" PaymentAbroadBankAddressZip='" & DBText(request("PaymentAbroadBankAddressZip")) & "'," &_
		" PaymentAbroadBankAddressCountry=" & TestNumVar(request("PaymentAbroadBankAddressCountry"), 0, -1, 0) & "," &_
		" PaymentAbroadSwiftNumber='" & request("PaymentAbroadSwiftNumber") & "'," &_
		" PaymentAbroadIBAN='" & request("PaymentAbroadIBAN") & "'," & _
		" PaymentAbroadABA='" & request("PaymentAbroadABA") & "'," & _
		" PaymentAbroadSortCode='" & request("PaymentAbroadSortCode") & "'," & _
		" PaymentAbroadAccountName2='" & DBText(request("PaymentAbroadAccountName2")) & "'," &_
		" PaymentAbroadAccountNumber2='" & request("PaymentAbroadAccountNumber2") & "'," &_
		" PaymentAbroadBankName2='" & DBText(request("PaymentAbroadBankName2")) & "'," &_
		" PaymentAbroadBankAddress2='" & DBText(request("PaymentAbroadBankAddress2")) & "'," &_
		" PaymentAbroadBankAddressSecond2='" & DBText(request("PaymentAbroadBankAddressSecond2")) & "'," &_
		" PaymentAbroadBankAddressCity2='" & DBText(request("PaymentAbroadBankAddressCity2")) & "'," &_
		" PaymentAbroadBankAddressState2='" & DBText(request("PaymentAbroadBankAddressState2")) & "'," &_
		" PaymentAbroadBankAddressZip2='" & DBText(request("PaymentAbroadBankAddressZip2")) & "'," &_
		" PaymentAbroadBankAddressCountry2=" & TestNumVar(request("PaymentAbroadBankAddressCountry2"), 0, -1, 0) & "," &_
		" PaymentAbroadSwiftNumber2='" & request("PaymentAbroadSwiftNumber2") & "'," &_
		" PaymentAbroadIBAN2='" & request("PaymentAbroadIBAN2") & "'," & _
		" PaymentAbroadABA2='" & request("PaymentAbroadABA2") & "'," & _
		" PaymentAbroadSortCode2='" & request("PaymentAbroadSortCode2") & "'" & _
		" WHERE id=" & request("companyID")
		oledbData.Execute sSQL
	end if
	%>
	<script language="JavaScript1.2">
		window.opener.location.href='Wire_search_data.asp?<%= sQueryString %>';
		close();
	</script>
	<%
end if
%>
<html>
<head>
	<title><%= request("WireMoney_id") %></title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
</head>
<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" class="itext" dir="ltr">
<%
Set rsData = server.createobject("adodb.recordset")
sSQL="SELECT tblCompany.CompanyLegalName, tblWireMoney.* " &_
"FROM tblWireMoney INNER JOIN tblCompany ON tblWireMoney.company_id = tblCompany.ID WHERE tblWireMoney.WireMoney_id =" & nWireMoney_id
rsData.Open sSQL, oledbData
if not rsData.EOF then
	sWireDate = rsData("WireDate")
	%>
	<table align="center" border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
	<tr>
		<td bgcolor="#f1f1f1" valign="top" class="txt12" style="color:black;">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td width="33%" valign="top" class="txt13b">
					<%
						if trim(request("RO"))="0" then
							sTitle="Update Payment Details"
						else
							sTitle="View Payment Details"
						end if
					%>
					<span style="color:#FF8000;"><%= sTitle %></span>
				</td>
				<td width="33%" align="center" valign="top" class="txt13b">
					<%= dbtextShow(rsData("CompanyLegalName")) %>
				</td>
				<td width="33%" align="right" valign="top" class="txt13" nowrap>
					<%
					If sError<>"" then 
						response.write "<span style=""color:red;"">" & sError & "</span>"
					else
						response.write sWireDate
					end if
					%>
				</td>
			</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td>
			<form action="Wire_data_Update.asp?<%= UrlWithout(UrlWithout(UrlWithout(sQueryString, "WireMoney_id"), "companyID"), "WireStatus") %>" name="frmCompanyUpdate" method="post">
			<input type="hidden" name="WireMoney_id" value="<%= nWireMoney_id %>">
			<input type="hidden" name="companyID" value="<%= trim(rsData("company_id")) %>">
			<input type="hidden" name="WireStatus" value="<%= trim(rsData("WireStatus")) %>">
			<table width="96%" align="center" border="0" cellspacing="0" cellpadding="2">
			<tr><td height="6"></td></tr>
			<tr>
				<td colspan="4" class="txt12b" valign="top">
					Local Bank Account<br />
				</td>
			</tr>
			<tr><td height="5"></td></tr>
			<tr>
				<td class="txt11">Company L.N.</td>
				<td class="txt11">
					<input type="text"dir="ltr" class="inputData" name="CompanyLegalNumber" value="<%= rsData("wireCompanyLegalNumber") %>">
				</td>
				<td class="txt11">Personal ID<br /></td>
				<td class="txt11">
					<input type="text" class="inputData" dir="ltr" name="IDNumber" value="<%= rsData("wireIDNumber") %>">
				</td>
			</tr>
			<tr><td height="5"></td></tr>
			<tr>
				<td class="txt11">Payment Type</td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentMethod" value="<%= rsData("wirePaymentMethod") %>">
				</td>
				<td class="txt11">Beneficiary</td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentPayeeName" value="<%= rsData("wirePaymentPayeeName") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Branch Number</td>				
				<td class="txt11">
					<script language="javascript" type="text/javascript">
						function IsBranchDigitsOnly(sBranch)
						{
							if (sBranch=="") return true;
							for (var i=0;i<sBranch.length;i++) if ("1234567890".replace(sBranch.substr(i, 1), "")=="1234567890") return false;
							return true;
						}
						function ValidateBranch(objInput)
						{
							if (!IsBranchDigitsOnly(objInput.value))
							{
								alert('Numeric value only!');
								objInput.focus();
								return false;
							}
							return true;
						}
					</script>
					<input type="text" class="inputData" maxlength="3" name="PaymentBranch" dir="ltr" value="<%= rsData("wirePaymentBranch") %>" onblur="return ValidateBranch(this);" onchange="return ValidateBranch(this);">
				</td>
				<td class="txt11">Account Number</td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAccount" dir="ltr" value="<%= rsData("wirePaymentAccount") %>">
				</td>
			</tr>
			<tr>
				<td class="txt11">Bank Name</td>				
				<td class="txt11" colspan="3">
					<select name="PaymentBank" class="inputData" style="width:431px;">
						<option value="0">
						<%	
						Set rsData2 = server.createobject("adodb.recordset")
						sSQL="SELECT ID, bankName, bankCode FROM tblSystemBankList ORDER BY bankName"
						rsData2.Open sSQL, oledbData
						do until rsData2.EOF
							%>
							<option value="<%= rsData2("id") %>" <%if trim(rsData2("id"))=trim(rsData("wirePaymentBank")) then%>selected<%End If%>><%= rsData2("bankName") %> - <%= trim(rsData2("bankCode")) %>
							<%
						rsData2.movenext
						loop
						rsData2.close
						Set rsData2 = nothing
						%>
					</select>
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			</table>
			<table dir="ltr" width="96%" align="center" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td colspan="4" class="txt12b" valign="top">
					Bank Account
				</td>
			</tr>
			<tr><td height="5"></td></tr>
			<tr>
				<td class="txt11">Account Name<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadAccountName" value="<%= rsData("WirePaymentAbroadAccountName") %>"><br />
				</td>
				<td class="txt11">Account Number<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadAccountNumber" value="<%= rsData("WirePaymentAbroadAccountNumber") %>"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">Bank Name<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadBankName" value="<%= rsData("WirePaymentAbroadBankName") %>"><br />
				</td>
				<td class="txt11">Swift Code<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadSwiftNumber" value="<%= rsData("WirePaymentAbroadSwiftNumber") %>"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">Address Line 1<br /></td>				
				<td class="txt11" colspan="3">
					<input type="text" class="inputData" name="PaymentAbroadBankAddress" value="<%= rsData("WirePaymentAbroadBankAddress") %>" style="width:434px;"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">Address Line 2<br /></td>				
				<td class="txt11" colspan="3">
					<input type="text" class="inputData" name="PaymentAbroadBankAddressSecond" value="<%= rsData("WirePaymentAbroadBankAddressSecond") %>" style="width:434px;"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">City<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadBankAddressCity" value="<%= rsData("WirePaymentAbroadBankAddressCity") %>"><br />
				</td>
				<td class="txt11">State<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadBankAddressState" value="<%= rsData("WirePaymentAbroadBankAddressState") %>"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">Zip/Postal<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadBankAddressZip" value="<%= rsData("WirePaymentAbroadBankAddressZip") %>"><br />
				</td>
				<td class="txt11">Country<br /></td>				
				<td class="txt11">
					<select name="PaymentAbroadBankAddressCountry" class="inputData" style="width:140px;">
						<option value="0">
						<%	
							sSQL="SELECT CountryID AS ID, Name AS CountryName FROM [List].[CountryList] ORDER BY Name"
							set request6 = oledbData.execute(sSQL)
							do until request6.EOF
								%>
								<option value="<%= request6("ID") %>" <%if trim(request6("ID"))=trim(rsData("WirePaymentAbroadBankAddressCountry")) then%>selected<%End If%>><%= request6("Name") %>
								<%
								request6.movenext
							loop
						%>
					</select>
				</td>
			</tr>
			<tr>
				<td class="txt11">IBAN<br /></td>				
				<td class="txt11" colspan="3">
					<input type="text" class="inputData" name="PaymentAbroadIBAN" value="<%= rsData("WirePaymentAbroadIBAN") %>" style="width:434px;"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">ABA<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadABA" value="<%= rsData("WirePaymentAbroadABA") %>"><br />
				</td>
				<td class="txt11">Sort Code<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadSortCode" value="<%= rsData("WirePaymentAbroadSortCode") %>"><br />
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			</table>
			<table dir="ltr" width="96%" align="center" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td colspan="4" class="txt12b" valign="top">
					Correspondent Bank Account
				</td>
			</tr>
			<tr><td height="5"></td></tr>
			<tr>
				<td class="txt11">Account Name<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadAccountName2" value="<%= rsData("WirePaymentAbroadAccountName2") %>"><br />
				</td>
				<td class="txt11">Account Number<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadAccountNumber2" value="<%= rsData("WirePaymentAbroadAccountNumber2") %>"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">Bank Name<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadBankName2" value="<%= rsData("WirePaymentAbroadBankName2") %>"><br />
				</td>
				<td class="txt11">Swift Code<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadSwiftNumber2" value="<%= rsData("WirePaymentAbroadSwiftNumber2") %>"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">Address Line 1<br /></td>				
				<td class="txt11" colspan="3">
					<input type="text" class="inputData" name="PaymentAbroadBankAddress2" value="<%= rsData("WirePaymentAbroadBankAddress2") %>" style="width:434px;"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">Address Line 2<br /></td>				
				<td class="txt11" colspan="3">
					<input type="text" class="inputData" name="PaymentAbroadBankAddressSecond2" value="<%= rsData("WirePaymentAbroadBankAddressSecond2") %>" style="width:434px;"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">City<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadBankAddressCity2" value="<%= rsData("WirePaymentAbroadBankAddressCity2") %>"><br />
				</td>
				<td class="txt11">State<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadBankAddressState2" value="<%= rsData("WirePaymentAbroadBankAddressState2") %>"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">Zip/Postal<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadBankAddressZip2" value="<%= rsData("WirePaymentAbroadBankAddressZip2") %>"><br />
				</td>
				<td class="txt11">Country<br /></td>				
				<td class="txt11">
					<select name="PaymentAbroadBankAddressCountry2" class="inputData" style="width:140px;">
						<option value="0">
						<%	
							sSQL="SELECT CountryID AS ID, Name AS CountryName FROM [List].[CountryList] ORDER BY Name"
							set request6 = oledbData.execute(sSQL)
							do until request6.EOF
								%>
								<option value="<%= request6("ID") %>" <%if trim(request6("ID"))=trim(rsData("WirePaymentAbroadBankAddressCountry2")) then%>selected<%End If%>><%= request6("Name") %>
								<%
								request6.movenext
							loop
						%>
					</select>
				</td>
			</tr>
			<tr>
				<td class="txt11">IBAN<br /></td>
				<td class="txt11" colspan="3">
					<input type="text" class="inputData" name="PaymentAbroadIBAN2" value="<%= rsData("WirePaymentAbroadIBAN2") %>" style="width:434px;"><br />
				</td>
			</tr>
			<tr>
				<td class="txt11">ABA<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadABA2" value="<%= rsData("WirePaymentAbroadABA2") %>"><br />
				</td>
				<td class="txt11">Sort Code<br /></td>				
				<td class="txt11">
					<input type="text" class="inputData" name="PaymentAbroadSortCode2" value="<%= rsData("WirePaymentAbroadSortCode2") %>"><br />
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			</table>
			<table width="96%" align="center" border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td class="txt12" colspan="7">
					<%
						if rsData("WireType")=1 then
							'admin pays to company
							sChecked=" checked "
						else
							sSQL="SELECT CompanyMakePaymentsProfiles_id FROM tblCompanyMakePaymentsRequests WHERE CompanyMakePaymentsRequests_id=" & rsData("WireSourceTbl_id")
							set rsData2=oledbData.Execute(sSQL)
							if rsData2(0)=0 then
								sChecked=" checked "
							else
								sChecked=""
							end if
						end if

						if trim(request("RO"))="0" then
							%>
								<div class="txt11">
									<input type="radio" name="isUpdateMaster" value="false" checked>
									Update this payment request only
								</div>
								<div class="txt11">
									<input type="radio" name="isUpdateMaster" value="true" <%= sChecked %>>
									Update this payment request and merchant's payment details
								</div>
							<%
						end if
					%>
				</td>
			</tr>
			<%
			if trim(rsData("WireStatus"))<>"4" AND trim(rsData("WireStatus"))<>"3" then
				%>
				<tr><td colspan="7"><hr size="1" color="#C0C0C0" noshade></td></tr>
				<tr>
					<td colspan="7" class="txt12b" valign="top">
						Transaction details<br />
					</td>
				</tr>
				<tr><td height="7"></td></tr>
				<tr>
					<td class="txt12">Amount</td>				
					<td class="txt12">
						<input type="text" class="inputData" style="width:50px;" size="14" name="wireAmount" dir="ltr" value="<%= rsData("wireAmount") %>">
						<%= GetCurText(rsData("WireCurrency")) %>
					</td>
					<td class="txt12">Exchange rate</td>
					<td class="txt12">
						<input type="text" class="inputData" style="width:50px;" tabindex="1" dir="ltr" size="14" name="ExchangeRate" value="<%= rsData("WireExchangeRate") %>">
					</td>
					<td class="txt12">Date</td>
					<td class="txt12">
						<input type="text" class="inputData" tabindex="1" dir="ltr" size="20" name="WireDate" value="<%= rsData("WireDate") %>">
					</td>
					<td class="txt12" align="left">
						<%
							if trim(request("RO"))="0" then
								%>
									<input type="submit" name="Action" value=" UPDATE " class="button1">
								<%
							end if
						%>
					</td>
				</tr>
				<%
			else
				%>
				<tr>
					<td class="txt12" align="left" colspan="7">
						<%
							if trim(request("RO"))="0" then
								%>
									<input type="submit" name="Action" value=" UPDATE " class="button1">
								<%
							end if
						%>
					</td>
				</tr>
				<%
			End If
			%>
			</form>
			</table>
		</td>
	</tr>
	</table>
	<%
end if

call rsData.close
Set rsData = nothing
call closeConnection()
%>
<script language="javascript" type="text/javascript">
	focus();
</script>
</body>
</html>