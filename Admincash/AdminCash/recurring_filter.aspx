<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<script runat="server">
	'
	' 20080616 Tamir
	'
	' Recurring transactions management
	'
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)

		lblCurrency.Text = "<select name=""ChargeCurrency"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
        Dim sSQL As String = "SELECT [CurrencyID], [CurrencyISOCode] FROM [List].[CurrencyList] ORDER BY [CurrencyID]"
		Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblCurrency.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		lblCurrency.Text &= "</select>"

		lblCardType.Text &= "<select name=""CardType"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT PaymentMethod_id AS 'ID', Name AS 'pm_Name' FROM List.PaymentMethod WHERE PaymentMethod_id>" & ePaymentMethod.PMD_MAXINTERNAL & " ORDER BY PaymentMethod_id"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblCardType.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		lblCardType.Text &= "</select>"
		drData.Close()
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Recurring Transactions Filter</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminNetEng.css" />
</head>
<body class="menu">

	<br />
	<h1><asp:label ID="lblPermissions" runat="server" /> Recurring <span> - Filter</span></h1>
	<br />
	<table class="filterTable" cellpadding="1" cellspacing="1" style="width:90%;">
	<tr>
		<td style="padding-left:3px;">
			<Uc1:FilterMerchants id="FMerchants" ClientField="MerchantID" SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
			 INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" runat="server" />
		</td>
	</tr>
	</table>
	
	<form runat="server" id="frmFilter" action="recurring.aspx" method="post" target="frmBody" onsubmit="ConvertFormToClient(this, 'recurring.aspx');">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" name="MerchantID" />
	<table class="filterTable" cellpadding="1" cellspacing="1" style="width:90%;">
	<tr><th class="filter" colspan="2"><br />Transaction</th></tr>
	<tr>
		<td>Number</td>
        <td><input name="TransFrom" class="short2 grayBG" /> - <input name="TransTo" class="short2 grayBG" /></td>
	</tr>
	<tr><td height="4"></td></tr>
	<tr>
		<td colspan="2">
			<input type="radio" name="TransTable" value="" checked="checked" />All
			<input type="radio" name="TransTable" value="Pass" />Passed
			<input type="radio" name="TransTable" value="Pending" />Pending<br />
			<input type="radio" name="TransTable" value="Fail" />Failed with code <input name="ReplyCode" class="short grayBG" />
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr>
		<td colspan="2">
			<Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" />
		</td>
	</tr>
	<tr><th class="filter" colspan="2"><br />Charge Amount</th></tr>
	<tr>
		<td>Currency</td>
        <td><asp:Label ID="lblCurrency" runat="server" /></td>
	</tr>
	<tr>
		<td>Amount</td>
		<td>
			<input name="ChargeAmountFrom" class="short2 grayBG" />
			-
			<input name="ChargeAmountTo" class="short2 grayBG" />
		</td>
	</tr>
	<tr><th class="filter" colspan="2"><br />Payment Method</th></tr>
	<tr>
		<td>Type</td>
		<td><asp:Label ID="lblCardType" runat="server" /></td>
	</tr>
	<tr>
		<td>BIN</td>
		<td><input name="BIN" class="short2 grayBG" /> </td>
	</tr>
	<tr>
		<td>Last 4 digits </td>
		<td><input name="Last4" class="short grayBG" /> </td>
	</tr>
	<tr><th class="filter" colspan="2"><br />Series</th></tr>
	<tr>
		<td>Number</td>
		<td>
			<input name="SeriesFrom" class="short2 grayBG" />
			-
			<input name="SeriesTo" class="short2 grayBG" />
		</td>
	</tr>
	<tr><td height="4"></td></tr>
	<tr>
		<td colspan="2">
			<input type="radio" name="TransApproval" value="" checked="checked" />All
			<input type="radio" name="TransApproval" value="0" />Regular
			<input type="radio" name="TransApproval" value="1" />Approval
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
			<br />
			<input type="submit" class="buttonFilter" value="Search" />
			<input type="reset" class="buttonFilter buttonDelete" value="Reset" />
		</td>
	</tr>
	</table>
	</form>
	
</body>
</html>