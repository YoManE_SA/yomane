			<br />
			<table align="center" border="0" dir="rtl" cellpadding="1" cellspacing="2" width="620">
			<tr>
				<td>
					<table align="right" width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="right" valign="top" dir="rtl" class="txt14">
							<span class="txt14b" dir="rtl">����� ��"�</span><br />
							<span class="txt12" dir="rtl">������ 6 ��� �� 52522 &nbsp; ��. 03-6126966</span><br />
						</td>
						<td width="200" align="left" valign="top" dir="rtl" class="txt14">
							<img src="../images/logo_mainGray.gif" alt="" width="200" height="31" border="0"><br />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td><hr width="100%" size="1" style="border:1px dashed black" noshade></td></tr>
			<%
			sCompChargeNumber = ""
			PaymentMethod = trim(rsData("PaymentMethod"))
			%>
			<tr>
				<td height="16"></td>
			</tr>
			<tr>
				<td align="right">
					<table align="right" width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="right" valign="top" dir="rtl" class="txt14">
							<span class="txt14b">�����</span><br />
							���� ContactCompanyName, ����� ������ ������<br />
							��� <span class="txt12">ContactFaxNum</span><br />
						</td>
						<td width="25"></td>
						<td width="50" align="right" valign="top" dir="rtl" class="txt14">
							<span class="txt14b">�����</span><br />
							<span class="txt12"><%= date() %></span><br />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td align="center" valign="top" dir="rtl" class="txt14b" style="text-decoration:underline;">
					�����: ����� ���� <% If int(rsData("RefundAskAmount"))<int(rsData("Amount")) then %>����<% End If %><br />
				</td>
			</tr>
			<td><br /></td>
			<tr>
				<td>
					<table align="right" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="2" align="right" valign="top" dir="rtl" class="txt13">
							���� ����� ���� ����� ������ �����:<br /><br />
						</td>
					</tr>
					<tr>
						<td width="120" align="right" valign="top" dir="rtl" class="txt13">
							�� ��� ����<br />
						</td>
						<td align="right" valign="top" dir="rtl" class="txt13">
							����� ��"�<%'= rsData("CompanyName") %><br />
						</td>
					</tr>
					<%
					if sCompChargeNumber<>"" then
						%>
						<tr>
							<td align="right" valign="top" dir="rtl" class="txt13">
								���� ���<br />
							</td>
							<td align="right" valign="top" dir="rtl" class="txt13">
								<%= sCompChargeNumber %><br />
							</td>
						</tr>
						<%
					end if
					
					sDate = FormatDateTime(rsData("InsertDate"),4) & "&nbsp;&nbsp;"
					sDate = sDate & day(rsData("InsertDate")) & "/" & month(rsData("InsertDate")) & "/" & right(year(rsData("InsertDate")),2)
					%>
					<tr><td height="10"></td></tr>
					<tr>
						<td align="right" valign="top" dir="rtl" class="txt13">
							����� �����<br />
						</td>
						<td align="right" valign="top" dir="ltr" class="txt13">
							<%= sDate %><br />
						</td>
					</tr>
					<tr>
						<td align="right" valign="top" dir="rtl" class="txt13">
							���� �����<br />
						</td>
						<td align="right" valign="top" dir="ltr" class="txt13">
							<span dir=ltr><%= rsData("CCard_number")%></span><br />
						</td>
					</tr>
					<tr>
						<td align="right" valign="top" dir="rtl" class="txt13">
							���� �����<br />
						</td>
						<td align="right" valign="top" dir="rtl" class="txt13">
							<%= trim(rsData("ExpMM")) %>/<%= trim(rsData("ExpYY")) %><br />
						</td>
					</tr>
					<%
					if trim(rsData("cc_cui"))<>"" then
						%>
						<tr>
							<td align="right" valign="top" dir="rtl" class="txt13">
								����� ������<br />
							</td>
							<td align="right" valign="top" dir="rtl" class="txt13">
								<%= DecCVV(rsData("cc_cui")) %><br />
							</td>
						</tr>
						<%
					End If
					%>
					<tr>
						<td align="right" valign="top" dir="rtl" class="txt13">
							�� ��� ������<br />
						</td>
						<td align="right" valign="top" dir="rtl" class="txt13">
							<%= rsData("Member") %><br />
						</td>
					</tr>
					<tr>
						<td align="right" valign="top" dir="rtl" class="txt13">
							���� ����<br />
						</td>
						<td align="right" valign="top" dir="rtl" class="txt13">
							<%
							if rsData("RefundAskCurrency") then
								%>
								$<%= FormatNumber(rsData("Amount"), 2, -1, 0, -1) %><br />
								<%
							else
								%>
								<span dir="ltr"><%= FormatCurrency (rsData("Amount"), 2, -1, 0, -1) %></span><br />
								<%
							end if
							%>
						</td>
					</tr>
					<tr>
						<td align="right" valign="top" dir="rtl" class="txt13">
							���� ������<br />
						</td>
						<td align="right" valign="top" dir="rtl" class="txt13">
							<%
							if rsData("RefundAskCurrency") then
								%>
								$<%= FormatNumber(rsData("RefundAskAmount"), 2, -1, 0, -1) %><br />
								<%
							else
								%>
								<span dir="ltr"><%= FormatCurrency (rsData("RefundAskAmount"), 2, -1, 0, -1) %></span><br />
								<%
							end if
							%>
						</td>
					</tr>
					<%
					If trim(rsData("RefundAskComment"))<>"" then
						%>
						<tr><td height="20"></td></tr>
						<tr>
							<td align="right" valign="top" dir="rtl" class="txt13">
								����<br />
							</td>
							<td align="right" valign="top" dir="ltr" class="txt13">
								<%= dbtextShow(rsData("RefundAskComment")) %><br />
							</td>
						</tr>
						<%
					End If
					%>
					<tr>
						<td class="txt13" colspan="2">
							<br /><br />
							<span class="txt13b">�� ����� ����� ����� ����</span> <span class="txt11b">03-6126967</span><br />
							����� �������� ������ <span class="txt11">03-6126966</span><br />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td><br /><br /></td></tr>
			<tr>
				<td>
					<table align="left" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="right" valign="top" dir="rtl" class="txt13">
							����� ��, ���� ����<br />
							���"� ���� ����� ��"�<br />
						</td>
					</tr>
					<tr><td height="10"></td></tr>
					<tr>
						<td align="center">
							<img src="../images/logoSignature.gif" alt="" border="0"><br />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<td><br /></td>
			</table>
