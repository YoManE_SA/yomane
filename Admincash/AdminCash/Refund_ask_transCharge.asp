<%
Dim sendStr, sSQL, rsData4
sSQL = "SELECT tblCompany.CustomerNumber, tblCompany.id AS companyID, tblCreditCard.BINCountry, " &_
"tblRefundAsk.RefundAskAmount, tblRefundAsk.RefundAskCurrency, tblRefundAsk.RefundAskComment, tblRefundAsk.transID, " &_
"tblCompanyTransPass.TerminalNumber, tblCompanyTransPass.PaymentMethodDisplay, tblCompanyTransPass.IPAddress, tblCompanyTransPass.OrderNumber, tblCompanyTransPass.PaymentMethod, " &_
"tblCreditCard.ID as CreditID, dbo.GetDecrypted256(tblCreditCard.CCard_number256) CCard_number, tblCreditCard.ExpMM, tblCreditCard.ExpYY, tblCreditCard.Member, tblCreditCard.cc_cui, tblCreditCard.phoneNumber As CardPhone, tblCreditCard.email as CardEmail, tblCreditCard.PersonalNumber as CardPersonal, " &_
"tblCheckDetails.*, tblCheckDetails.ID as CheckID, dbo.GetDecrypted256(tblCheckDetails.RoutingNumber256) As routingNumber, dbo.GetDecrypted256(tblCheckDetails.AccountNumber256) As accountNumber, tblCheckDetails.email As CheckEmail, tblCheckDetails.phoneNumber As CheckPhone, tblCheckDetails.PersonalNumber as CheckPersonal, " & _
"tblBillingAddress.address1, tblBillingAddress.address2, tblBillingAddress.city, tblBillingAddress.zipCode, tblBillingAddress.stateIso, tblBillingAddress.countryIso " &_
" From tblRefundAsk " &_
" Left JOIN tblCompany ON (tblCompany.ID = tblRefundAsk.companyID) " &_
" Left JOIN tblCompanyTransPass ON (tblCompanyTransPass.ID = tblRefundAsk.transID) " &_
" Left Join tblCreditCard ON (tblCompanyTransPass.CreditCardID = tblCreditCard.id)" & _
" Left Join tblCheckDetails ON (tblCompanyTransPass.CheckDetailsID = tblCheckDetails.id)" & _
" Left Join tblBillingAddress ON (tblCheckDetails.BillingAddressId = tblBillingAddress.id Or tblCreditCard.BillingAddressId = tblBillingAddress.id)" & _
"WHERE tblRefundAsk.RefundAskStatus=0 AND tblRefundAsk.id = " & nID

'"FROM tblBillingAddress " &_
'"RIGHT OUTER JOIN tblCreditCard ON tblBillingAddress.id = tblCreditCard.BillingAddressId " &_
'"RIGHT OUTER JOIN tblRefundAsk INNER JOIN tblCompany ON tblRefundAsk.companyID = tblCompany.ID " &_
'"INNER JOIN tblCompanyTransPass ON tblRefundAsk.transID = tblCompanyTransPass.ID ON tblCreditCard.ID = tblCompanyTransPass.CreditCardID " &_
'"WHERE tblRefundAsk.RefundAskStatus=0 AND tblRefundAsk.id = " & nID


Set rsData4 = oledbData.Execute(sSQL)
If NOT rsData4.EOF Then
	' building url string to send -------------------------------------------------------
    If Not IsNull(rsData4("CreditID")) Then
	    sendStr = PROCESS_URL & "member/remote_charge.asp?"
	    'sendStr = sendStr & "CardNum=" & Fn_CleanCCNumber(rsData4("CCard_number")) & "&"
	    'sendStr = sendStr & "ExpMonth=" & right(trim(rsData4("ExpMM")),2) & "&"
	    'sendStr = sendStr & "ExpYear=" & right(trim(rsData4("ExpYY")),2) & "&"
	    'sendStr = sendStr & "CVV2=" & IIf( IsNull(rsData4("cc_cui")), "", Server.URLEncode(DecCVV(rsData4("cc_cui"))) ) & "&"
	    'sendStr = sendStr & "Member=" & Server.URLEncode(TestStrVar(trim(rsData4("Member")),1,100,"")) & "&"
	    'sendStr = sendStr & "PhoneNumber=" & Server.URLEncode(trim(rsData4("CardPhone"))) & "&"
	    'sendStr = sendStr & "PersonalNum=" & Server.URLEncode(trim(rsData4("CardPersonal"))) & "&"
	    'sendStr = sendStr & "Email=" & Server.URLEncode(trim(rsData4("CardEmail"))) & "&"
    ElseIf Not IsNull(rsData4("CheckID")) Then
	    sendStr = PROCESS_URL & "member/remotecharge_echeck.asp?"
        'sendStr = sendStr & "PaymentMethod=" & Server.URLEncode(trim(rsData4("PaymentMethod"))) & "&"
        'sendStr = sendStr & "AccountName=" & Server.URLEncode(trim(rsData4("AccountName"))) & "&"
        'sendStr = sendStr & "RoutingNumber=" & Server.URLEncode(trim(rsData4("RoutingNumber"))) & "&"
        'sendStr = sendStr & "AccountNumber=" & Server.URLEncode(trim(rsData4("AccountNumber"))) & "&"
        'sendStr = sendStr & "BankAccountType=" & Server.URLEncode(trim(rsData4("BankAccountTypeId"))) & "&"
        'sendStr = sendStr & "BankName=" & Server.URLEncode(trim(rsData4("BankName"))) & "&"
        'sendStr = sendStr & "BankCity=" & Server.URLEncode(trim(rsData4("BankCity"))) & "&"
        'sendStr = sendStr & "BankPhone=" & Server.URLEncode(trim(rsData4("BankPhone"))) & "&"
        'sendStr = sendStr & "BankState=" & Server.URLEncode(trim(rsData4("BankState"))) & "&"
        'sendStr = sendStr & "BankCountry=" & Server.URLEncode(trim(rsData4("BankCountry"))) & "&"
        'sendStr = sendStr & "PersonalNum=" & Server.URLEncode(trim(rsData4("CheckPersonal"))) & "&"
        'sendStr = sendStr & "Email=" & Server.URLEncode(trim(rsData4("CheckEmail"))) & "&"
        'sendStr = sendStr & "PhoneNumber=" & Server.URLEncode(trim(rsData4("CheckPhone"))) & "&"
    End If
	sendStr = sendStr & "CompanyNum=" & trim(rsData4("CustomerNumber")) & "&"
	sendStr = sendStr & "Amount=" & trim(rsData4("RefundAskAmount")) & "&"
	sendStr = sendStr & "Currency=" & trim(rsData4("RefundAskCurrency")) & "&"
	sendStr = sendStr & "Payments=" & 1 & "&"
	sendStr = sendStr & "TypeCredit=" & 0 & "&"
	sendStr = sendStr & "Order=" & Server.URLEncode(rsData4("OrderNumber")) & "&"
	sendStr = sendStr & "referringUrl=" & Server.URLEncode(Left(DBText(Request.ServerVariables("HTTP_REFERER")), 200)) & "&"
	sendStr = sendStr & "ClientIP=" & Server.URLEncode(trim(rsData4("IPAddress"))) & "&"
	sendStr = sendStr & "RefTransID=" & Server.URLEncode(trim(rsData4("transID"))) & "&"
	sendStr = sendStr & "requestSource=6" & "&"
	'If NOT isNull(rsData4("address1")) Then
	'	If Not IsNull(rsData4("address1")) Then sendStr = sendStr & "BillingAddress1=" & Server.URLEncode(trim(rsData4("address1"))) & "&"
	'	If Not IsNull(rsData4("address2")) Then sendStr = sendStr & "BillingAddress2=" & Server.URLEncode(trim(rsData4("address2"))) & "&"
	'	If Not IsNull(rsData4("city")) Then sendStr = sendStr & "BillingCity=" & Server.URLEncode(trim(rsData4("city"))) & "&"
	'	If Not IsNull(rsData4("zipCode")) Then sendStr = sendStr & "BillingZipCode=" & Server.URLEncode(trim(rsData4("zipCode"))) & "&"
	'	If Not IsNull(rsData4("stateIso")) Then sendStr = sendStr & "BillingState=" & Server.URLEncode(trim(rsData4("stateIso"))) & "&"
	'	sCountryISO=trim(rsData4("countryIso"))
	'	If sCountryISO="" then sCountryISO=trim(rsData4("BINCountry"))
	'	sendStr = sendStr & "BillingCountry=" & sCountryISO & "&"
	'End if
	'sendStr = sendStr & "ReplyURL=none" & "&"
	sendStr = sendStr & "isLocalTransaction=true" & "&"
	sendStr = sendStr & "Comment=" & Server.URLEncode(trim(rsData4("RefundAskComment"))) 
End If
rsData4.close
Set rsData4 = nothing

If sendStr <> "" Then
	'On Error Resume Next
		'Creating the request
		Dim HttpReq, resStr
		Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
		HttpReq.setTimeouts 20*1000, 30*1000, 40*1000, 240*1000

		HttpReq.open "GET", sendStr, false
		HttpReq.send()
		'Checking the response
		resStr = HttpReq.responseText
		'Response.Write(sendStr & vbCrLf & "<br/><br/>" & vbCrLf & resStr) : Response.End
		replyCode = GetUrlValue(resStr, "Reply")
        if replyCode = "" Then replyCode = GetUrlValue(resStr, "replyCode") 'echeck
		transId = GetUrlValue(resStr, "TransID")
		transDate = GetUrlValue(resStr, "Date")
		comment = GetUrlValue(resStr, "Comment")
		replyDescription = GetUrlValue(resStr, "ReplyDesc")
	On Error GoTo 0
	'Response.Write(sendStr): Response.End
	nRefStat = 0
	If Trim(resStr)<>"" Then
		If Trim(replyCode)="000" Or Trim(replyCode)="001" then
            If Trim(replyCode)="000" Then ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & transId & "," & PET_RefundRequestSendNotify & ",'', 3)")
			nRefStat = 4
			sHeading = "processing a refund successfully " & transId & " (" & replyCode & ")"
		Else
			sHeading = "processing a refund failed " & transId & " (" & replyCode & ")"
		End if
	Else
		sHeading = "processing a refund encounter a communication problem (" & sendStr & ")"
	End if
Else
	sHeading = "processing a refund encounter a missing data"
End if

Call WriteToLog(sHeading, nRefStat, nID)
'Response.Write(resStr):Response.End
%>