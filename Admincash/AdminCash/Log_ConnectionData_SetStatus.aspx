<%@ Page Language="VB" EnableViewStateMac="false" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		'20101102 Tamir - allow setting any status, depending of the current status
		Dim nID As Integer = dbPages.TestVar(Request("ID"), 1, 0, -1), nStatus As Integer = dbPages.TestVar(Request("Status"), 0, 4, -1)
		If nID >= 0 And nStatus >= 0 Then
			Dim sAllowedStatuses As String = "4"
			Select Case nStatus
				Case 0 : sAllowedStatuses = "2, 4"
				Case 4 : sAllowedStatuses = "0, 3"
			End Select
			Dim sSQL As String = "UPDATE tblLog_NoConnection SET lnc_AutoRefundStatus=" & nStatus
			'20101102 Tamir - when renewing, set date to current
			If nStatus = 0 Then sSQL &= " , lnc_InsertDate=GetDate()"
			sSQL &= " WHERE lnc_AutoRefundStatus IN (" & sAllowedStatuses & ") AND LogNoConnection_ID=" & nID
			Dim nRows As Integer = dbPages.ExecSql(sSQL)
			If nRows > 0 Then Response.Write("<s" & "cript language=""javascript"" type=""text/javascript"">parent.location.reload();</s" & "cript>")
		End If
	End Sub
</script>