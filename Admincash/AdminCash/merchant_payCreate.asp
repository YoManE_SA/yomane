<%
Option Explicit
Server.ScriptTimeout = 600
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/CalcTransPayment.asp"-->
<!--#include file="../include/security_class.asp"-->
<!--#include file="../include/func_invoice.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<%
Dim nPayID, nCompanyID, nCurrency, dPaymentDate, xPayment, xWhere, bCreateWire, nReceiveCurrency, iRs, outputList
nCompanyID = TestNumVar(Request("companyID"), 0, -1, 0)
nCurrency = TestNumVar(Request("currency"), 0, MAX_CURRENCY, 0)
nPayID = TestNumVar(Request("PayID"), 0, -1, 0)
If UCase(Request("Action")) = "NEW" Then nPayID = 0
If Request("PaymentDate") <> "" Then
	dPaymentDate = Request("PaymentDate") & iif(instr(trim(""&Request("PaymentDate"))," ")>0, "", " " & time())
ElseIf Request("Day") <> "" And Request("Month") <> "" And Request("Year") <> "" Then 
	dPaymentDate = dbText(Request("Day")) & "/" & dbText(Request("Month")) & "/" & dbText(Request("Year")) & " " & time()
Else
	dPaymentDate = Now()
End if
bCreateWire = (nPayID = 0)

If Request("Type") = "Cancel" Then
    If (nPayID <> 0) Then 
        ClacDeletePayment nPayID, nCompanyID, False
    End If
ElseIf Request("Type") = "RetReserve" Then
    Set xPayment = CreatePayment()
	xPayment.AddRetReserve(TestNumVar(Request("ResID"), 0, -1, 0))
    UpdatePayment xPayment, "", bCreateWire
ElseIf Request("Type") = "RolReserve" Then
    Set xPayment = CreatePayment()
	xPayment.RollingReserve = TestNumVar(Request("RelAmount"), 0, -1, 0)
    UpdatePayment xPayment, "", bCreateWire
ElseIf Request("Type") = "ExecAll" Then
    xWhere = GetWhereFilters()
    Set xPayment = CreatePayment()
	If TestNumVar(Request("MaxTransAmount"), 0, -1, 0) <> 0 Then 
		Dim nTransID : nTransID = xPayment.GetTransIDUpTo(xWhere, TestNumVar(Request("MaxTransAmount"), 0, -1, 0))
		If nTransID = "" Then nTransID = -1
		xWhere = xWhere & IIF(xWhere <> "", " And ", "") & " (tblCompanyTransPass.ID IN(" & nTransID & ") Or DeniedStatus<>0)"
	End If
    UpdatePayment xPayment, xWhere, bCreateWire
ElseIf Request("Type") = "ExecPerPM" Then
    If (nPayID <> 0) Then 
        Response.Write("PayCreate With type=ExecPerPM - can't be used to update existing payment")
        Response.End
    End If
    xWhere = GetWhereFilters()
    Set iRs = oleDbData.Execute("Select PaymentMethod From tblCompanyTransPass Where CompanyID=" & nCompanyID & " And Currency=" & nCurrency & " And tblCompanyTransPass.PayID LIKE ('%;0;%') And PaymentMethod > " & PMD_MAXINTERNAL & " Group By PaymentMethod")
    Do While Not iRs.EOF
        Set xPayment = CreatePayment()
        UpdatePayment xPayment, xWhere & " And (tblCompanyTransPass.PaymentMethod = " & iRs(0) & ")", bCreateWire
      iRs.MoveNext
    Loop
    iRs.Close
Else 'manual transaction selection
    Set xPayment = CreatePayment()
	If Request("ID") <> "" Then xWhere = xWhere & " tblCompanyTransPass.ID IN(" & Request("ID") & ")"
	If Request("IDpayments") <> "" Then
		If xWhere <> "" Then xWhere = xWhere & " OR "
		xWhere = xWhere & "tblCompanyTransPass.ID IN (Select transAnsID From tblCompanyTransInstallments Where tblCompanyTransInstallments.id IN(" & Request("IDpayments") & "))"
	End If
	xWhere = " (isTestOnly = 0) " & IIF(xWhere <> "", "And (" & xWhere & ")", "")
    UpdatePayment xPayment, xWhere, bCreateWire
End If	

Function GetWhereFilters()
	Dim dSelDate, sWhere
    'sWhere = " (DeniedStatus = 0) AND (Payments = 1) And (isTestOnly = 0)"
	If Request("AutoCHB") <> "1" Then sWhere = sWhere & " And (DeniedStatus = 0) "
	If Request("IncInstallemts") <> "1" Then sWhere = sWhere & " And (Payments = 1) "
	If Request("IncTestTrans") <> "1" Then sWhere = sWhere & " And (isTestOnly = 0) "
	If TestNumVar(Request("ForceEpaPaid"), 0, 1, 1) = 1 Then sWhere = sWhere & " And ((ep.IsPaid = 1 Or ep.IsRefunded = 1) Or PaymentMethod <= " & PMD_MAXINTERNAL & ") "
	
	If Request("DateUsage") = "0" Then
	    dSelDate = Request("PayFromDate")
		If (dSelDate <> "") And IsDate(dSelDate) Then _
			sWhere = sWhere & " And tblCompanyTransPass.InsertDate >='" & DateValue(dSelDate) & " 00:00:00" & "'"
		dSelDate = Request("PayToDate")
		If (dSelDate <> "") And IsDate(dSelDate) Then _
			sWhere = sWhere & " And tblCompanyTransPass.InsertDate <='" & DateValue(dSelDate) & " 23:59:59" & "'"
	Else
	    dSelDate = Request("PayFromPD")
		If (dSelDate <> "") And IsDate(dSelDate) Then _
			sWhere = sWhere & " And tblCompanyTransPass.MerchantPD >='" & DateValue(dSelDate) & " 00:00:00" & "'"
		dSelDate = Request("PayToPD")
		If (dSelDate <> "") And IsDate(dSelDate) Then _
			sWhere = sWhere & " And tblCompanyTransPass.MerchantPD <='" & DateValue(dSelDate) & " 23:59:59" & "'"
	End If
	If Request("TypeTransCharge") <> "1" Or Request("TypeTransRefund") <> "1" Then
		sWhere = sWhere & " And tblCompanyTransPass.CreditType IN("
		If Request("TypeTransRefund") = "1" Then
			sWhere = sWhere & "0,"
		ElseIf Request("TypeTransCharge") = "1" Then 
			sWhere = sWhere & "1, 2, 6, 8,"
		Else
			sWhere = sWhere & "255,"
		End If	
		sWhere = Left(sWhere, Len(sWhere) - 1) & ")"
	End If
	
	If Request("TypePaymentMethodCc") <> "1" Or Request("TypePaymentMethodAdmin") <> "1" Then
		If Request("TypePaymentMethodCc") = "1" Then
			sWhere = sWhere & " And (tblCompanyTransPass.PaymentMethod > " & PMD_MAXINTERNAL & ")"
		ElseIf Request("TypePaymentMethodAdmin") = "1" Then
			sWhere = sWhere & " And (tblCompanyTransPass.PaymentMethod <= " & PMD_MAXINTERNAL & ")"
		Else
			sWhere = sWhere & " And (tblCompanyTransPass.PaymentMethod < 0)"
		End If
	End If
	If sWhere <> "" Then sWhere = Right(sWhere, Len(sWhere) - 5)
	'Response.Write(sWhere)
	'Response.End()
    GetWhereFilters = sWhere
End Function 

Function CreatePayment()
    Set CreatePayment = New CPayment
    CreatePayment.InitByPayID nPayID, nCompanyID, nCurrency
    nPayID = CreatePayment.CreatePayment(dPaymentDate)
End Function

Function UpdatePayment(nPayment, sWhere, bCreateWire)
	'Response.Write(xWhere) : Response.End
    If sWhere <> "" Then 
	    nPayment.AddTransactions "(" & sWhere & ")"
	    nPayment.AddSpecialAdminTransAmount
    End If

    Dim PayDeleted : PayDeleted = True
    If nPayment.UpdatePayment() Then
        Dim currencyMinSettleAmount, nIsWireExcludeDebit, nIsWireExcludeRefund, nIsWireExcludeFee, nIsWireExcludeChb, nIsWireExcludeCashback
        Set iRs = oleDbData.Execute("Select * From Setting.SetMerchantSettlement Where Merchant_ID=" & nCompanyID & " And Currency_ID=" & nCurrency)
        If Not iRs.EOF Then
            currencyMinSettleAmount = TestNumVar(iRs("MinSettlementAmount"), 0, -1, 0)
            nIsWireExcludeDebit = iRs("IsWireExcludeDebit")
            nIsWireExcludeRefund = iRs("IsWireExcludeRefund")
            nIsWireExcludeFee = iRs("IsWireExcludeFee")
            nIsWireExcludeChb = iRs("IsWireExcludeChb")
            nIsWireExcludeCashback = iRs("IsWireExcludeCashback")
        End If
        iRs.Close

        If currencyMinSettleAmount > nPayment.Total Then 
            ClacDeletePayment nPayID, nCompanyID, False
        Else 
            PayDeleted = False
	        If bCreateWire Then
		        Dim sSQL, nExchangeRate
		        'If nCurrency <> 0 Then nExchangeRate = ConvertCurrencyRate(nCurrency, 0) Else nExchangeRate = ConvertCurrencyRate(nCurrency, 1)
		        nReceiveCurrency = ExecScalar("Select dbo.GetMerchantPayoutCurrency(" & nCompanyID & ", " & nCurrency & ")", nCurrency)
			    If IsNull(nReceiveCurrency) Then nReceiveCurrency = nCurrency
		        nExchangeRate = Round("" & ConvertCurrencyRateWFee(nCurrency, nReceiveCurrency), 5)
		        sSQL="INSERT INTO tblWireMoney (Company_id, WireSourceTbl_id, WireInsertDate, WireType, WireCurrency, WireExchangeRate, wireCompanyName," & _
		        " wireCompanyLegalName, wireIDnumber, wireCompanyLegalNumber, wirePaymentMethod, wirePaymentPayeeName, wirePaymentBank, wirePaymentBranch," & _
		        " wirePaymentAccount, wirePaymentAbroadAccountName, wirePaymentAbroadAccountNumber, wirePaymentAbroadBankName," & _
		        " wirePaymentAbroadBankAddress, wirePaymentAbroadBankAddressSecond, wirePaymentAbroadBankAddressCity, wirePaymentAbroadBankAddressState," &_
		        " wirePaymentAbroadBankAddressZip, wirePaymentAbroadBankAddressCountry, wirePaymentAbroadSwiftNumber, wirePaymentAbroadIBAN," & _
		        " wirePaymentAbroadABA, wirePaymentAbroadSortCode, wirePaymentAbroadAccountName2, wirePaymentAbroadAccountNumber2," & _
		        " wirePaymentAbroadBankName2, wirePaymentAbroadBankAddress2, wirePaymentAbroadBankAddressSecond2, wirePaymentAbroadBankAddressCity2," &_
		        " wirePaymentAbroadBankAddressState2, wirePaymentAbroadBankAddressZip2, wirePaymentAbroadBankAddressCountry2," & _
		        " wirePaymentAbroadSwiftNumber2, wirePaymentAbroadIBAN2, wirePaymentAbroadABA2, wirePaymentAbroadSortCode2)" &_
		        " SELECT " & nCompanyID & ", " & nPayID & ", '" & dPaymentDate & "', 1, " & nCurrency & "," & nExchangeRate & ", CompanyName," & _
		        " CompanyLegalName, IDnumber, CompanyLegalNumber, PaymentMethod, PaymentPayeeName, PaymentBank, PaymentBranch, PaymentAccount," & _
		        " mba_AccountName, mba_AccountNumber, mba_BankName, mba_BankAddress, mba_BankAddressSecond, mba_BankAddressCity, mba_BankAddressState," &_
		        " mba_BankAddressZip, mba_BankAddressCountry, mba_SwiftNumber, mba_IBAN, mba_ABA, mba_SortCode, mba_AccountName2, mba_AccountNumber2," &_
		        " mba_BankName2, mba_BankAddress2, mba_BankAddressSecond2, mba_BankAddressCity2, mba_BankAddressState2, mba_BankAddressZip2," & _
		        " mba_BankAddressCountry2, mba_SwiftNumber2, mba_IBAN2, mba_ABA2, mba_SortCode2" & _
		        " FROM tblCompany INNER JOIN GetMerchantBankAccount(" & nCompanyID & ", " & nReceiveCurrency & ") ON ID=mba_Merchant"
                'Response.Write(sSQL)
		        oledbData.Execute sSQL
	        End if
            Dim wireTotal : wireTotal = nPayment.Total
            If nIsWireExcludeDebit Then wireTotal = wireTotal - nPayment.Totals.NormAmount
            If nIsWireExcludeRefund Then wireTotal = wireTotal - nPayment.Totals.RefAmount
            If nIsWireExcludeCashback Then wireTotal = wireTotal - nPayment.Totals.CashbackAmount
            If nIsWireExcludeFee Then wireTotal = wireTotal + nPayment.TotalFeesAmount - nPayment.FeesAutoAndManual.TotalAmount
            If nIsWireExcludeChb Then wireTotal = wireTotal + nPayment.Totals.DenAmount
            'Response.Write(nPayment.Total & " N" & wireTotal)
	        oledbData.execute "UPDATE tblWireMoney SET WireAmount=" &  wireTotal & "-wireFee WHERE WireType=1 AND WireSourceTbl_id=" & nPayID
        End If
    End If

    If PayDeleted Then
        outputList = outputList & "<br>Settlement (" & nPayID & ") - " & GetCurIso(nCurrency) & " Deleted"
    Else
        Dim sellementUrl : sellementUrl = "Merchant_transSettledDetail.asp?companyID=" & nCompanyID & "&payID=" & nPayID & "&payDate=" & dPaymentDate
        outputList = outputList & "<br>Settlement <a href=""" & sellementUrl & """>" & nPayID & "</a> to " & nPayment.CompanyName & " (" & nCompanyID & ") " & " - " & nPayment.Total & " " & GetCurIso(nPayment.CurrencyID)  & " Created."
    End If
End Function
'Response.End()

If outputList <> "" Then 
    If Request("Type") <> "ExecPerPM" Then 
	    If Request("Ref") <> "" Then Response.Redirect Request("Ref") & "&payID=" & nPayID _
        Else Response.Redirect "Merchant_transSettledDetail.asp?companyID=" & nCompanyID & "&payID=" & nPayID & "&payDate=" & dPaymentDate
    End If
Else
    If Request("Type") = "Cancel" Then outputList = "Settlement " & nPayID & " Deleted" _
    Else outputList = "Settlement not created"
End If
%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Settlement <%= GetCurIso(nCurrency) %></title>
		<meta http-equiv="Content-Type" content="text/html;charset=Windows-1255" />
    	<link href="../StyleSheet/styleAdminEng.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
        <table align="center" style="width:100%;"><tr><td id="pageMainHeadingTd"><span id="pageMainHeading">CREATE SETTLEMENTS</span> </td></tr></table>
		<div style="padding:15px;font-size:14px;"><%= outputList %></div>
	</body>
</html>
