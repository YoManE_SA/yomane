<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<script runat="server">
    Private Function MapFileName(fileName As String) As String
        Return Domain.Current.MapPrivateDataPath("Account Files/DebitCompany/BnS/Uploads/" & fileName)
    End Function
    
	Sub UploadFile(ByVal o As Object, ByVal e As EventArgs)
		litError.Text = String.Empty
		litFileInfo.Text = String.Empty
		If Not fuCHB.HasFile Then
			litError.Text = "No file is uploaded!"
			Exit Sub
		End If
		Dim sFileName As String = fuCHB.FileName.ToLower
		fuCHB.SaveAs(MapFileName(sFileName))
		litFileInfo.Text = "File uploaded: <b>" & sFileName & "</b>"
		If (sFileName.Contains("photocopy") Or sFileName.Contains("chargeback")) And sFileName.EndsWith(".txt") Then
			Dim sDateCHB As String = sFileName.Substring(sFileName.LastIndexOf(".") - 8, 4) & "-" & sFileName.Substring(sFileName.LastIndexOf(".") - 4, 2) & _
			"-" & sFileName.Substring(sFileName.LastIndexOf(".") - 2, 2) & " 00:00:00"
			Dim srCHB As StreamReader = My.Computer.FileSystem.OpenTextFileReader(MapFileName(sFileName))
			Dim nFieldCount As Integer = 0, sSQL, saRecord() As String
			Dim nIsPhotocopy As Integer = IIf(sFileName.Contains("photocopy"), 1, 0)
			Dim sPhotocopy As String = IIf(nIsPhotocopy = 1, "Photocopy", "Chargeback")
			litFileInfo.Text = sPhotocopy & " list from BNS, plain text file, " & sDateCHB.Substring(0, 10)
			Dim sbSQL As New StringBuilder("INSERT INTO tblImportChargebackBNS(")
			If Not srCHB.EndOfStream Then
				saRecord = srCHB.ReadLine.Split(";")
				For i As Integer = 0 To saRecord.Length - 1
					saRecord(i) = saRecord(i).Trim.Replace(".", String.Empty)
					If String.IsNullOrEmpty(saRecord(i)) Then Exit For
					If saRecord(i) = "COMMENT-NO" Then saRecord(i) = "COMMENTS" 'rename field in photocopy file
					If Not String.IsNullOrEmpty(saRecord(i)) Then
						sbSQL.AppendFormat("{0}[{1}]", IIf(nFieldCount = 0, String.Empty, ", "), saRecord(i))
						nFieldCount += 1
					End If
				Next
			End If
			Dim nRows As Integer = 0
			If nFieldCount > 0 Then
				sbSQL.Append(", icb_FileName, icb_ChargebackDate, icb_IsPhotocopy) VALUES (")
				sSQL = sbSQL.ToString
				While Not srCHB.EndOfStream
					saRecord = srCHB.ReadLine.Split(";")
					If saRecord.Length >= nFieldCount Then
						For i As Integer = 0 To nFieldCount - 1
							saRecord(i) = dbPages.DBText(saRecord(i).Trim)
						Next
						sbSQL.Remove(0, sbSQL.Length).Append(sSQL)
						sbSQL.AppendFormat("'{0}'", String.Join("', '", saRecord, 0, nFieldCount))
						sbSQL.AppendFormat(", '{0}', Convert(datetime, '{1}', 120), {2})", sFileName, sDateCHB, nIsPhotocopy)
						Try
							dbPages.ExecSql("IF NOT EXISTS (SELECT 1 FROM tblImportChargebackBNS WITH (NOLOCK) WHERE icb_IsPhotocopy=" & nIsPhotocopy & " AND [CASE ID]='" & dbPages.DBText(saRecord(0)) & "') " & sbSQL.ToString)
						Catch ex As Exception
							litError.Text &= sPhotocopy & " with CASE ID " & saRecord(0) & " failed, the row is ignored.<div style=""padding-left:50px;"">" & ex.Source & "<hr>" & ex.StackTrace.Replace(" at ", "<br />at ") & "<hr>" & ex.Message & "</div>"
						End Try
					End If
					nRows += 1
				End While
				litFileInfo.Text &= " (" & nRows & " rows)"
			End If
			srCHB.Close()
			repCHB.DataBind()
		ElseIf (sFileName.StartsWith("photocopy") Or sFileName.StartsWith("char") Or sFileName.StartsWith("tspuk")) And sFileName.EndsWith(".csv") Then
			Dim sDateCHB As String
			If sFileName.StartsWith("tspuk") Then
				sDateCHB = sFileName.Substring(sFileName.LastIndexOf(".") - 15, 4) & "-" & sFileName.Substring(sFileName.LastIndexOf(".") - 11, 2) & "-" & sFileName.Substring(sFileName.LastIndexOf(".") - 9, 2) & " 00:00:00"
			Else
				sDateCHB = sFileName.Substring(sFileName.LastIndexOf(".") - 8, 4) & "-" & sFileName.Substring(sFileName.LastIndexOf(".") - 4, 2) & "-" & sFileName.Substring(sFileName.LastIndexOf(".") - 2, 2) & " 00:00:00"
			End If
			Dim srCHB As StreamReader = My.Computer.FileSystem.OpenTextFileReader(MapFileName(sFileName))
			Dim nFieldCount As Integer = 0, sSQL, saRecord() As String
			Dim nIsPhotocopy As Integer = IIf(sFileName.Contains("photocopy") Or sFileName.Contains("pcr"), 1, 0)
			Dim sPhotocopy As String = IIf(nIsPhotocopy = 1, "Photocopy", "Chargeback")
			litFileInfo.Text = sPhotocopy & " list from BNS, plain text file (CSV - semicolon separated), " & sDateCHB.Substring(0, 10)
			Dim sbSQL As New StringBuilder("INSERT INTO tblImportChargebackBNS(")
			If Not srCHB.EndOfStream Then
				saRecord = srCHB.ReadLine.Split(";")
				For i As Integer = 0 To saRecord.Length - 1
					saRecord(i) = saRecord(i).Trim.Replace(".", String.Empty)
					If String.IsNullOrEmpty(saRecord(i)) Then Exit For
					If saRecord(i) = "COMMENT-NO" Then saRecord(i) = "COMMENTS" 'rename field in photocopy file
					If Not String.IsNullOrEmpty(saRecord(i)) Then
						sbSQL.AppendFormat("{0}[{1}]", IIf(nFieldCount = 0, String.Empty, ", "), saRecord(i))
						nFieldCount += 1
					End If
				Next
			End If
			Dim nRows As Integer = 0
			If nFieldCount > 0 Then
				sbSQL.Append(", icb_FileName, icb_ChargebackDate, icb_IsPhotocopy) VALUES (")
				sSQL = sbSQL.ToString
				While Not srCHB.EndOfStream
					saRecord = srCHB.ReadLine.Split(";")
					If saRecord.Length >= nFieldCount Then
						For i As Integer = 0 To nFieldCount - 1
							saRecord(i) = dbPages.DBText(saRecord(i).Trim)
						Next
						sbSQL.Remove(0, sbSQL.Length).Append(sSQL)
						sbSQL.AppendFormat("'{0}'", String.Join("', '", saRecord, 0, nFieldCount))
						sbSQL.AppendFormat(", '{0}', Convert(datetime, '{1}', 120), {2})", sFileName, sDateCHB, nIsPhotocopy)
						Try
							dbPages.ExecSql("IF NOT EXISTS (SELECT 1 FROM tblImportChargebackBNS WITH (NOLOCK) WHERE icb_IsPhotocopy=" & nIsPhotocopy & " AND [CASE ID]='" & dbPages.DBText(saRecord(0)) & "') " & sbSQL.ToString)
						Catch ex As Exception
							litError.Text &= sPhotocopy & " with CASE ID " & saRecord(0) & " failed, the row is ignored.<div style=""padding-left:50px;"">" & ex.Source & "<hr>" & ex.StackTrace.Replace(" at ", "<br />at ") & "<hr>" & ex.Message & "</div>"
						End Try
					End If
					nRows += 1
				End While
				litFileInfo.Text &= " (" & nRows & " rows)"
			End If
			srCHB.Close()
			repCHB.DataBind()
		ElseIf sFileName.StartsWith("estmt_bo_") And sFileName.EndsWith(".xml") And Not sFileName.StartsWith("estmt_bo_tc") Then
			Dim sDateCHB As String = Date.Today.ToString("yyyy-MM-dd 00:00:00")
			litFileInfo.Text = "Chargebacks from JCC, XML file (Excel Workbook), " & sDateCHB.Substring(0, 10)
			Dim xdCHB As New XmlDocument
			xdCHB.Load(MapFileName(sFileName))
			Dim xnTable As XmlNode = xdCHB.DocumentElement.LastChild.FirstChild
			Dim nColumns As Integer = 0, nColumnTrnID As Integer = 0, nCol As Integer = 0, nColumnChbDate As Integer = 0, nRows As Integer = 0
			Dim sData As String, sColumns As String = "", sValues As String, sTrnID As String = ""
			For Each xnRecord As XmlNode In xnTable.ChildNodes
				Select Case xnRecord.Name
					Case "Row"
						If nRows = 0 Then
							For Each xnCell As XmlNode In xnRecord.ChildNodes
								sData = xnCell.InnerText
								Select Case sData	' rename fields in RR file
									Case "MERCHANT_ORDER_ID" : sData = "TRN_ID"
									Case "REQUEST_REASON_CODE" : sData = "REASON_CODE"
									Case "TRANSACTION_AMOUNT" : sData = "AMOUNT"
									Case "CURRENCY_CODE" : sData = "CURRENCY_CODE"
								End Select
								If Not String.IsNullOrEmpty(sData) Then sColumns &= sData & ", "
								If sData.ToUpper = "TRN_ID" Then nColumnTrnID = nCol
								If sData.ToUpper = "PROCESSING_DATE" Then nColumnChbDate = nCol
								nCol += 1
							Next
							sColumns &= "icb_FileName, icb_ChargebackDate"
						Else
							sValues = ""
							sDateCHB = ""
							sTrnID = ""
							nCol = 0
							For Each xnCell As XmlNode In xnRecord.ChildNodes
								sData = xnCell.InnerText
								sValues &= "'" & dbPages.DBText(sData) & "', "
								If nCol = nColumnTrnID Then sTrnID = sData
								If nCol = nColumnChbDate Then sDateCHB = sData
								nCol += 1
							Next
							sValues &= "'" & sFileName & "', Convert(datetime, '" & sDateCHB & "', 3)"
							Try
								dbPages.ExecSql("IF NOT EXISTS (SELECT 1 FROM tblImportChargebackJCC WITH (NOLOCK) WHERE TRN_ID='" & sTrnID & "') INSERT INTO tblImportChargebackJCC(" & sColumns & ") VALUES (" & sValues & ");")
							Catch ex As Exception
								litError.Text &= "Chargeback with TRN_ID " & sTrnID & " failed, the row is ignored.<div style=""padding-left:50px;"">" & ex.Source & "<hr>" & ex.StackTrace.Replace(" at ", "<br />at ") & "<hr>" & ex.Message & "<hr>INSERT INTO tblImportChargebackJCC(" & sColumns & ") VALUES (" & sValues & ")</div>"
							End Try
						End If
						nRows += 1
					Case "Column"
						nColumns += 1
				End Select
			Next
			litFileInfo.Text &= " (" & nRows & " rows)"
			repCHB.DataBind()
		Else
			litError.Text = "Unrecognizable file format -  <b>" & sFileName & "</b> cannot be imported."
		End If
	End Sub
	
	Sub SetCheckbox(ByVal chkBox As CheckBox, ByVal bEnabled As Boolean, Optional ByVal bChecked As Boolean = False)
		chkBox.Enabled = bEnabled
		chkBox.Checked = bChecked
	End Sub

	Sub SetTransList(ByVal o As Object, ByVal e As RepeaterItemEventArgs)
		Dim ddlTemp As DropDownList = e.Item.FindControl("ddlTrans")
		Dim litTemp As Literal = e.Item.FindControl("litTransList")
		If Not ddlTemp Is Nothing Then
			ddlTemp.Items.Clear()
			Dim saTransList() As String = litTemp.Text.Split(",")
			For Each sTrans As String In saTransList
				If Not String.IsNullOrEmpty(sTrans) Then ddlTemp.Items.Add(sTrans)
			Next
			If ddlTemp.Items.Count = 0 Then
				SetCheckbox(e.Item.FindControl("chkDelete"), True, True)
				ddlTemp.Visible = False
				litTemp.Visible = False
				SetCheckbox(e.Item.FindControl("chkCreate"), False)
				SetCheckbox(e.Item.FindControl("chkBlock"), False)
				SetCheckbox(e.Item.FindControl("chkSend"), False)
			Else
				SetCheckbox(e.Item.FindControl("chkDelete"), True, False)
				Dim bPhotocopy As Boolean = IIf(CType(e.Item.FindControl("litType"), Literal).Text.ToLower() = "photocopy", True, False)
				If ddlTemp.Items.Count > 1 Then
					ddlTemp.Items.Insert(0, String.Empty)
					ddlTemp.Visible = True
					litTemp.Visible = False
					ddlTemp.Attributes.Add("onchange", "ResetCheckboxes(this);")
					SetCheckbox(e.Item.FindControl("chkCreate"), True)
					SetCheckbox(e.Item.FindControl("chkBlock"), True)
					SetCheckbox(e.Item.FindControl("chkSend"), True)
				ElseIf ddlTemp.Items.Count = 1 Then
					ddlTemp.Visible = False
					litTemp.Visible = True
					SetCheckbox(e.Item.FindControl("chkCreate"), True, True)
					SetCheckbox(e.Item.FindControl("chkBlock"), True, Not bPhotocopy)
					SetCheckbox(e.Item.FindControl("chkSend"), True, True)
				End If
			End If
			CType(e.Item.FindControl("chkDelete"), CheckBox).Attributes.Add("onclick", "ConfirmWithHighlight(this);")
			CType(e.Item.FindControl("chkCreate"), CheckBox).Attributes.Add("onclick", "ReenableOtherCheckboxes(this);")
		End If
	End Sub

	Sub ProcessRecords(ByVal o As Object, ByVal e As EventArgs)
		Dim bDebug As Boolean = IIf(dbPages.TestVar(Request("Debug"), 1, 0, 0) = 1, True, False)
		Server.ScriptTimeout = 300
		Dim nID, nTrans, nTransCHB As Integer, litID, litIDDC, litDateCHB, litReason, litType As Literal, chkBlock, chkSend As CheckBox, dtCHB As Date
		Dim nDC As Integer, hidDC As HiddenField, bPhotocopy As Boolean
		For Each riCHB As RepeaterItem In repCHB.Items
			litID = riCHB.FindControl("litID")
			If Not litID Is Nothing Then
				nID = CType(litID, Literal).Text
				hidDC = riCHB.FindControl("hidDebitCompany")
				nDC = hidDC.Value
				If CType(riCHB.FindControl("chkDelete"), CheckBox).Checked Then
					Select Case nDC
						Case 18, 46 : dbPages.ExecSql("DELETE FROM tblImportChargebackBNS WHERE ID=" & nID)
						Case 19 : dbPages.ExecSql("DELETE FROM tblImportChargebackJCC WHERE ID=" & nID)
					End Select
					Response.Write("<div style=""font-size:10px;"">" & Date.Now.ToString("HH:mm:ss") & " Record " & nID & " has been deleted.</div>")
					Response.Flush()
				Else
					nTrans = dbPages.TestVar(CType(riCHB.FindControl("ddlTrans"), DropDownList).SelectedValue, 1, 0, 0)
					If nTrans > 0 And CType(riCHB.FindControl("chkCreate"), CheckBox).Checked Then
						chkBlock = riCHB.FindControl("chkBlock")
						chkSend = riCHB.FindControl("chkSend")
						litDateCHB = riCHB.FindControl("litDateCHB")
						litReason = riCHB.FindControl("litReason")
						Dim nReasonCode As Integer = dbPages.TestVar(litReason.Text, 1, 0, 0)
						Select Case nDC
							Case 18, 46
								dtCHB = Date.Now
								litIDDC = riCHB.FindControl("litCaseID")
								litType = riCHB.FindControl("litType")
								bPhotocopy = IIf(litType.Text.ToLower = "photocopy", True, False)
								If bPhotocopy Then
									nTransCHB = CHB.RequestPhotocopy(nTrans, dtCHB, chkBlock.Checked, chkSend.Checked, "AutoPhotocopy (BNS) CaseID=" & litIDDC.Text & ", Reason=" & litReason.Text, bDebug, nReasonCode)
								Else
									nTransCHB = CHB.Create(nTrans, dtCHB, chkBlock.Checked, chkSend.Checked, "AutoCHB (BNS) CaseID=" & litIDDC.Text & ", Reason=" & litReason.Text, bDebug, nReasonCode)
								End If
								If nTransCHB > 0 Then dbPages.ExecSql("UPDATE tblImportChargebackBNS SET icb_IsProcessed=1 WHERE ID=" & nID)
							Case 19
								dtCHB = litDateCHB.Text
								litIDDC = riCHB.FindControl("litTrnID")
								nTransCHB = CHB.Create(nTrans, dtCHB, chkBlock.Checked, chkSend.Checked, "AutoCHB (JCC) TrnID=" & litIDDC.Text & ", Reason=" & litReason.Text, bDebug, nReasonCode)
								If nTransCHB > 0 Then dbPages.ExecSql("UPDATE tblImportChargebackJCC SET icb_IsProcessed=1 WHERE ID=" & nID)
						End Select
						Response.Write("<div style=""font-size:10px;"">" & Date.Now.ToString("HH:mm:ss") & " Record " & nID & " has been processed on transaction " & nTrans & ".</div>")
						Response.Flush()
					End If
				End If
			End If
		Next
		repCHB.DataBind()
	End Sub
	
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsCHB.ConnectionString = dbPages.DSN
		If Security.IsLimitedDebitCompany Then
			dsCHB.SelectCommand = "SELECT * FROM viewImportChargeback WITH (NOLOCK) WHERE FileDebitCompany IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript" src="../js/jquery-1.3.2.min.js"></script>
	<script language="JavaScript" src="../js/jquery.shiftcheckbox.js"></script>
	<script type="text/javascript" language="JavaScript" src="../js/func_common.js"></script>
	<script type="text/javascript" language="JavaScript">
		function ResetCheckboxes(oSelect) {
			var bChecked = (oSelect.selectedIndex != 0);
			for (var tdCell = oSelect; tdCell.nodeName.toLowerCase() != 'td'; tdCell = tdCell.parentNode);
			for (; tdCell.nextSibling; tdCell = tdCell.nextSibling);
			tdCell.firstChild.firstChild.checked = bChecked;
			tdCell.previousSibling.firstChild.firstChild.checked = bChecked;
			tdCell.previousSibling.previousSibling.firstChild.firstChild.checked = bChecked;
		}

		function ReenableOtherCheckboxes(oCheckbox) {
			var bActive = oCheckbox.checked;
			for (var tdCell = oCheckbox; tdCell.nodeName.toLowerCase() != 'td'; tdCell = tdCell.parentNode);
			tdCell.nextSibling.firstChild.firstChild.checked = (bActive && (tdCell.previousSibling.previousSibling.previousSibling.innerText=="Chargeback"));
			tdCell.nextSibling.firstChild.firstChild.disabled = !bActive;
			tdCell.nextSibling.nextSibling.firstChild.firstChild.checked = bActive;
			tdCell.nextSibling.nextSibling.firstChild.firstChild.disabled = !bActive;
			tdCell.previousSibling.previousSibling.firstChild.firstChild.checked = false;
		}

		function ConfirmWithHighlight(oCheckbox)
		{
			if (oCheckbox.checked)
			{
				for (var trRow = oCheckbox; trRow.nodeName.toLowerCase() != 'tr'; trRow = trRow.parentNode);
				trRow.style.backgroundColor = 'Pink';
				oCheckbox.checked = true; //confirm('Do you really want to delete this item ?!');
				trRow.style.backgroundColor = '';
			}
			if (oCheckbox.checked)
			{
				for (var tdCell = oCheckbox; tdCell.nodeName.toLowerCase() != 'td'; tdCell = tdCell.parentNode);
				tdCell.parentNode.cells[tdCell.cellIndex + 2].firstChild.firstChild.checked = false;
				tdCell.parentNode.cells[tdCell.cellIndex + 3].firstChild.firstChild.checked = false;
				tdCell.parentNode.cells[tdCell.cellIndex + 4].firstChild.firstChild.checked = false;
			}
		}

		function ConfirmProcess()
		{
			var bDelete = false, bProcess = false;
			for (var i = 0; i < document.getElementsByTagName("INPUT").length-1; i++)
			{
				if ((document.getElementsByTagName("INPUT")[i].id.indexOf("chkDelete")>0)&&(document.getElementsByTagName("INPUT")[i].checked))
				{
					bDelete=true;
					if (bDelete && bProcess) break;
				}
				if ((document.getElementsByTagName("INPUT")[i].id.indexOf("chkCreate")>0)&&(document.getElementsByTagName("INPUT")[i].checked))
				{
					bProcess=true;
					if (bDelete && bProcess) break;
				}
			}
			if (bProcess && !confirm('Do you really want to process the records marked for register?')) return false;
			if (bDelete && !confirm('Some records are marked for delete! Do you really want to DELETE the marked records ?!')) return false;
			return true;
		}

		function ShowDetails(imgThis, bShow)
		{
			for (var trMain = imgThis; trMain.nodeName.toLowerCase() != "tr"; trMain = trMain.parentNode);
			for (var trDetails = trMain.nextSibling; trDetails.nodeName.toLowerCase() != "tr"; trDetails = trDetails.nextSibling);
			trDetails.style.display = (bShow ? "block" : "none");
			for (trDetails = trDetails.nextSibling; trDetails.nodeName.toLowerCase() != "tr"; trDetails = trDetails.nextSibling);
			trDetails.style.display = (bShow ? "block" : "none");
			if (bShow)
			{
				var nTransID = "";
				if (imgThis.parentNode.nextSibling.firstChild)
				{
					if (imgThis.parentNode.nextSibling.firstChild.nodeName.toLowerCase() == "select")
						nTransID = imgThis.parentNode.nextSibling.firstChild.value;
					else
						if (imgThis.parentNode.nextSibling.firstChild.nodeName.toLowerCase() == "#text") nTransID = imgThis.parentNode.nextSibling.innerText;
					if (nTransID!="") trDetails.firstChild.nextSibling.innerHTML = "<iframe frameborder=\"0\" border=\"0\" src=\"trans_detail_cc.asp?isAllowAdministration=false&TransTableType=pass&PaymentMethod=1&transID=" + nTransID + "\" width=\"100%\" height=\"100\" onload=\"height=contentWindow.document.body.scrollHeight;\"></iframe>";
				}
			}
			for (var imgOther = (bShow ? imgThis.nextSibling : imgThis.previousSibling); imgOther.nodeName.toLowerCase() != "img";)
				imgOther = (bShow ? imgOther.nextSibling : imgOther.previousSibling);
			imgThis.style.display = "none";
			imgOther.style.display = "";
		}

		$(document).ready(
        function () {
        	$("span.shiftCheckbox input[type='checkbox']").shiftcheckbox();
        });		


	</script>
</head>
<body>
	<form id="Form1" runat="server">
	<table align="center" style="width:95%;">
	<tr>
		<td id="pageMainHeading">
			<asp:label ID="lblPermissions" runat="server" /> CHB Conflict Resolver<br />
		</td>
	</tr>
	</table>
	<br />
	<div style="text-align:center;">
		<fieldset style="width:95%;padding:10px;text-align:left;" runat="server" visible="false">
			<legend>Upload New File</legend>
			<asp:FileUpload ID="fuCHB" ToolTip="Upload chargeback file here" runat="server" />
			<asp:Button ID="Button1" CssClass="buttonWhite" OnClick="UploadFile" Text="Upload!" runat="server" />
			<span style="font-size:11px;"><asp:literal ID="litFileInfo" runat="server" /></span>
			<div class="errorMessage"><asp:literal ID="litError" runat="server" /></div>
		</fieldset>
		<div style="text-align:center;">
			<table class="formNormal" width="95%">
				<asp:SqlDataSource ID="dsCHB" DataSourceMode="DataReader" ProviderName="System.Data.SqlClient" runat="server"
				 SelectCommand="SELECT * FROM viewImportChargeback" />
				<asp:Repeater ID="repCHB" DataSourceID="dsCHB" OnItemDataBound="SetTransList" runat="server">
					<HeaderTemplate>
						<tr>
							<th>&nbsp;</th>
							<th>Trans. No.</th>
							<th>Trans. Date</th>
							<th>Merchant</th>
							<th style="text-align:right;">Amount</th>
							<th>Currency</th>
							<th>Debit Company</th>
							<th>Payment Method</th>
							<th>CHB Date</th>
							<th>Reason</th>
							<th>Type</th>
							<th class="Vertical">Delete</th>
							<td>&nbsp;</td>
							<th class="Vertical">Register</th>
							<th class="Vertical">Block Card</th>
							<th class="Vertical">Send Mail</th>
						</tr>
					</HeaderTemplate>
					<ItemTemplate>
						<tr onmouseover="this.style.backgroundColor='Gainsboro';" onmouseout="if (this.style.backgroundColor.toLowerCase()=='gainsboro') this.style.backgroundColor='';">
							<td>
								<img onclick="ShowDetails(this,true);" style="cursor:pointer;" src="../images/tree_expand.gif" alt="Expand" width="16" height="16" border="0" />
								<img onclick="ShowDetails(this,false);" style="cursor:pointer;display:none;" src="../images/tree_collapse.gif" alt="Collapse" width="16" height="16" border="0" />
							</td>
							<td>
								<asp:DropDownList ID="ddlTrans" Visible="false" runat="server" />
								<asp:Literal ID="litTransList" Text='<%# Eval("TransList") %>' Visible="false" runat="server" />
								<asp:Literal ID="litID" Text='<%# Eval("ID") %>' Visible="false" runat="server" />
							</td>
							<td><asp:Literal ID="Literal1" Text='<%# dbPages.FormatDateTime(Eval("TransactionDate"), , False) %>' runat="server" /></td>
							<td><asp:Literal ID="Literal2" Text='<%# Eval("MerchantList") %>' runat="server" /></td>
							<td style="text-align:right;"><asp:Literal ID="Literal3" Text='<%# IIf(Eval("FileDebitCompany")=18 Or Eval("FileDebitCompany")=46, CHB.ReformatCurrencyBNS(Eval("AMOUNT-TXN")), CType("0" & Eval("AMOUNT"), Decimal).ToString("0.00")) %>' runat="server" /></td>
							<td><asp:Literal ID="Literal4" Text='<%# Eval("CurrencyIsoName") %>' runat="server" /></td>
							<td style="vertical-align:middle;">
								<asp:Image ID="Image1" ImageUrl='<%# "/NPCommon/ImgDebitCompanys/23X12/" & Eval("FileDebitCompany") & ".gif"%>' AlternateText='<%# Eval("FileDebitCompanyName") %>'
								 ImageAlign="Middle" runat="server" />
								<asp:Literal ID="Literal5" Text='<%# Eval("FileDebitCompanyName") %>' runat="server" />
								<asp:HiddenField ID="hidDebitCompany" Value='<%# Eval("FileDebitCompany") %>' runat="server" />
							</td>
							<td style="vertical-align:middle;">
								<asp:Image ID="Image2" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/" & Eval("PaymentMethod") & ".gif"%>' AlternateText='<%# Eval("PaymentMethodName") %>'
								 ImageAlign="Middle" runat="server" />
								<asp:Image ID="Image3" ImageUrl='<%# "/NPCommon/ImgCountry/18X12/" & Eval("CountryISO") & ".gif"%>' AlternateText='<%# Eval("CountryName") %>'
								 ImageAlign="Middle" runat="server" />
								<asp:Literal ID="Literal6" Text='<%# Eval("CardNumberPartial") %>' runat="server" />
							</td>
							<td><asp:Literal ID="litDateCHB" Text='<%# dbPages.FormatDateTime(Eval("icb_ChargebackDate"), , True, False) %>' runat="server" /></td>
							<td><asp:Literal ID="litReason" Text='<%# Eval("Reason") %>' runat="server" /></td>
							<td><asp:Literal ID="litType" Text='<%# IIf(Eval("icb_IsPhotocopy"), "Photocopy", "Chargeback") %>' runat="server" /></td>
							<td style="text-align:center;"><asp:CheckBox ID="chkDelete" CssClass="shiftCheckbox" runat="server" /></td>
							<td>&nbsp;</td>
							<td style="text-align:center;"><asp:CheckBox ID="chkCreate" CssClass="option" runat="server" /></td>
							<td style="text-align:center;"><asp:CheckBox ID="chkBlock" CssClass="option" runat="server" /></td>
							<td style="text-align:center;"><asp:CheckBox ID="chkSend" CssClass="option" runat="server" /></td>
						</tr>
						<tr style="display:none;">
							<td></td>
							<td colspan="13" class="details">
								<asp:Label ID="Label1" Visible='<%# Eval("FileDebitCompany")=18 Or Eval("FileDebitCompany")=46 %>' runat="server">
									<table class="formNormal">
										<tr style="text-transform:capitalize;">
											<th>case ID</th>
											<th>RSN</th>
											<th>mer-no</th>
											<th>cardholder-number</th>
											<th>txn-date</th>
											<th>amount-txn</th>
											<th>cur</th>
											<th>amount-euro</th>
											<th>ref-no</th>
											<th>ticket-no</th>
											<th>deadline</th>
											<th>comments</th>
											<th>MRF</th>
											<th>Uploaded By</th>
											<th>File Name</th>
										</tr>
										<tr style="background-color:rgb(255,220,220);">
											<td><asp:Literal ID="litCaseID" Text='<%# Eval("CASE ID") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal7" Text='<%# Eval("RSN") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal8" Text='<%# Eval("MER-NO") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal9" Text='<%# Eval("CARDHOLDER-NUMBER") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal10" Text='<%# Eval("TXN-DATE") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal11" Text='<%# Eval("AMOUNT-TXN") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal12" Text='<%# Eval("CUR") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal13" Text='<%# Eval("AMOUNT-EURO") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal14" Text='<%# Eval("REF-NO") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal15" Text='<%# Eval("TICKET-NO") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal16" Text='<%# Eval("DEADLINE") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal17" Text='<%# Eval("COMMENTS") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal18" Text='<%# Eval("MRF") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal19" Text='<%# Eval("icb_User") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal20" Text='<%# Eval("icb_FileName") %>' runat="server" /></td>
										</tr>
									</table>
								</asp:Label>
								<asp:Label ID="Label2" Visible='<%# Eval("FileDebitCompany")=19 %>' runat="server">
									<table class="formNormal">
										<tr style="text-transform:capitalize;">
											<th>pos brand code</th>
											<th>merchant number</th>
											<th>outlet number</th>
											<th>charging status</th>
											<th>microfilm ref number</th>
											<th>card number</th>
											<th>network code</th>
											<th>acronym</th>
											<th>amount</th>
											<th>currency code</th>
										</tr>
										<tr style="background-color:rgb(255,220,220);">
											<td><asp:Literal ID="Literal21" Text='<%# Eval("POS_BRAND_CODE") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal22" Text='<%# Eval("MERCHANT_NUMBER") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal23" Text='<%# Eval("OUTLET_NUMBER") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal24" Text='<%# Eval("CHARGING_STATUS") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal25" Text='<%# Eval("MICROFILM_REF_NUMBER") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal26" Text='<%# Eval("CARD_NUMBER") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal27" Text='<%# Eval("NETWORK_CODE") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal28" Text='<%# Eval("ACRONYM") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal29" Text='<%# Eval("AMOUNT") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal30" Text='<%# Eval("CURRENCY_CODE") %>' runat="server" /></td>
										</tr>
									</table>
									<table class="formNormal">
										<tr>
											<th>source amount</th>
											<th>source currency code</th>
											<th>reason code</th>
											<th>transaction date</th>
											<th>processing date</th>
											<th>authorization code</th>
											<th>trn id</th>
											<th>Uploaded By</th>
											<th>File Name</th>
										</tr>
										<tr style="background-color:rgb(255,220,220);">
											<td><asp:Literal ID="Literal31" Text='<%# Eval("SOURCE_AMOUNT") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal32" Text='<%# Eval("SOURCE_CURRENCY_CODE") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal33" Text='<%# Eval("REASON_CODE") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal34" Text='<%# Eval("TRANSACTION_DATE") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal35" Text='<%# Eval("PROCESSING_DATE") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal36" Text='<%# Eval("AUTHORIZATION_CODE") %>' runat="server" /></td>
											<td><asp:Literal ID="litTrnID" Text='<%# Eval("TRN_ID") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal37" Text='<%# Eval("icb_User") %>' runat="server" /></td>
											<td><asp:Literal ID="Literal38" Text='<%# Eval("icb_FileName") %>' runat="server" /></td>
										</tr>
									</table>
								</asp:Label>
							</td>
						</tr>
						<tr style="display:none;">
							<td></td>
							<td colspan="13"></td>
						</tr>
						<tr>
							<td colspan="12" style="height:1;background-color:Silver;"></td>
							<td style="height:1;"></td>
							<td colspan="3" style="height:1;background-color:Silver;"></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						<tr>
							<td colspan="16" style="text-align:right;">
								<span style="float:left;">
									<script language="javascript" type="text/javascript">
										function ExportToExcel()
										{
											location.replace("trans_chb_excel.aspx");
										}
									</script>
									<input type="button" value="Export To Excel" onclick="ExportToExcel();" class="buttonWhite" />
								</span>
								<asp:Button Text="Process!" OnClick="ProcessRecords" OnClientClick="if (!ConfirmProcess()) return false;" CssClass="buttonWhite" runat="server" />
							</td>
						</tr>
					</FooterTemplate>
				</asp:Repeater>
			</table>
		</div>
	</div>
	</form>
</body>
</html>