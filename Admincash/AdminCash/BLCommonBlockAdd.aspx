<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
	Sub LoadCBData(pCtl As CheckBox, pLbl As Label, lType As eBLCommon, sValue As String)
		pLbl.Text = IIf(String.IsNullOrEmpty(sValue), "----------", sValue)
		pCtl.Enabled = Not String.IsNullOrEmpty(sValue)
	End Sub

	Sub AddRecord(ByVal chkBox As CheckBox, ByVal lblValue As Label, ByVal nType As Integer)
		If chkBox.Checked And chkBox.Enabled Then
            dbPages.ExecScalar("EXEC Risk.spInsertBlacklistItem " & rblMerchant.SelectedValue & ", " & nType & ", '" & dbPages.DBText(lblValue.Text) & "', '" & dbPages.DBText(txtComment.Text) & "'")
			chkBox.Enabled = False
		End If
	End Sub

	Sub AddRecords(ByVal o As Object, ByVal e As EventArgs)
		AddRecord(eBL_Email, eBL_EmailTxt, 1)
		AddRecord(eBL_FullName, eBL_FullNameTxt, 2)
		AddRecord(eBL_Phone, eBL_PhoneTxt, 3)
		AddRecord(eBL_PNumber, eBL_PNumberTxt, 4)
		AddRecord(eBL_BIN, eBL_BINTxt, 5)
		AddRecord(eBL_BINCountry, eBL_BINCountryTxt, 6)
	End Sub
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		If Not Page.IsPostBack Then
			Dim iReader As SqlDataReader = dbPages.ExecReader("SELECT * FROM tblCreditCard WHERE ID=" & dbPages.TestVar(Request("CCID"), 0, -1, 0))
			If iReader.Read() Then
                If Not IsDBNull(iReader("email")) Then LoadCBData(eBL_Email, eBL_EmailTxt, eBLCommon.eBL_Email, iReader("email"))
                If Not IsDBNull(iReader("Member")) Then LoadCBData(eBL_FullName, eBL_FullNameTxt, eBLCommon.eBL_FullName, iReader("Member"))
                If Not IsDBNull(iReader("phoneNumber")) Then LoadCBData(eBL_Phone, eBL_PhoneTxt, eBLCommon.eBL_Phone, iReader("phoneNumber"))
                If Not IsDBNull(iReader("PersonalNumber")) Then LoadCBData(eBL_PNumber, eBL_PNumberTxt, eBLCommon.eBL_PNumber, iReader("PersonalNumber"))
                If Not IsDBNull(iReader("CCard_First6")) Then LoadCBData(eBL_BIN, eBL_BINTxt, eBLCommon.eBL_BIN, iReader("CCard_First6"))
				If Not IsDBNull(iReader("BINCountry")) Then LoadCBData(eBL_BINCountry, eBL_BINCountryTxt, eBLCommon.eBL_BINCountry, iReader("BINCountry"))
				rblMerchant.Items(0).Value = iReader("CompanyID")
			End If
			iReader.Close()
			txtComment.Text = "Added from trans " & Request("TransID") & " (" & Request("TableType") & ")"
		End If
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Add to Common Blacklist</title>
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<table>
		<tr>
			<td colspan="2" id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> Common Blacklist <span> - Add New</span>
			</td>
		</tr>
	</table>
	<br />
	<form id="form1" runat="server">
		<table class="formThin" width="90%">
		<tr><th colspan="2">Data to Block</th></tr>
		<tr>
			<td>
				<asp:CheckBox runat="server" id="eBL_FullName" CssClass="option" />
				Full Name - <asp:Label runat="server" id="eBL_FullNameTxt" />
			</td>
			<td>
				<asp:CheckBox runat="server" id="eBL_Email" CssClass="option" />
				Email - <asp:Label runat="server" id="eBL_EmailTxt" />
			</td>
		</tr>
		<tr>
			<td>
				<asp:CheckBox runat="server" id="eBL_Phone" CssClass="option" />
				Phone - <asp:Label runat="server" id="eBL_PhoneTxt" />
			</td>
			<td>
				<asp:CheckBox runat="server" id="eBL_PNumber" CssClass="option" />
				Personal ID - <asp:Label runat="server" id="eBL_PNumberTxt" />
			</td>
		</tr>
		<tr>
			<td>
				<asp:CheckBox runat="server" id="eBL_BIN" CssClass="option" />
				BIN - <asp:Label runat="server" id="eBL_BINTxt" />
			</td>
			<td>
				<asp:CheckBox runat="server" id="eBL_BINCountry" CssClass="option" />
				Country: <asp:Label runat="server" id="eBL_BINCountryTxt" />
			</td>
		</tr>
		<tr><th colspan="2" style="padding-top:6px;">Comment</th></tr>
		<tr><td colspan="2"><asp:TextBox ID="txtComment" Width="350" runat="server" /></td></tr>
		<tr><th colspan="2" style="padding-top:6px;">Block Type</th><tr>
		<tr>
			<td colspan="2">
				<asp:RadioButtonList ID="rblMerchant" CssClass="option" RepeatLayout="Flow" RepeatColumns="2" runat="server">
					<asp:ListItem Text="Merchant Only" Value="" Selected="True" />
					<asp:ListItem Text="Global" Value="0" />
				</asp:RadioButtonList>
			</td>
		</tr>
		<tr><td colspan="2" style="padding-top:6px;"><asp:Button ID="Button1" Text=" ADD " OnClick="AddRecords" runat="server" /></td></tr>
		</table>
	</form>
</body>
</html>