<%@ Page Language="VB" %>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Netpay.Bll" %>
<%@ Import Namespace="Netpay.Infrastructure" %>
<%@ Import Namespace="Netpay.Web" %>

<%@ Register Assembly="Netpay.Web" Namespace="Netpay.Web.Controls" TagPrefix="netpay" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript">
	function focusCCField(currentField)
	{
		var currentElement = document.getElementById("tbCCNum" + currentField.toString());
		if (currentElement.value.length == 4 && currentField < 4)
		{
			var nextElement = document.getElementById("tbCCNum" + (currentField + 1).toString());
			nextElement.focus();
		}
	}

	function OpenPop(sURL, sName, sWidth, sHeight, sScroll)
	{
		placeLeft = screen.width / 2 - sWidth / 2
		placeTop = screen.height / 2 - sHeight / 2
		var winPrint = window.open(sURL, sName, 'TOP=' + placeTop + ',LEFT=' + placeLeft + ',WIDTH=' + sWidth + ',HEIGHT=' + sHeight + ',scrollbars=' + sScroll);
	}
</script>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Security.CheckPermission(lblPermissions)
        lblError.Text = ""

        If Not IsPostBack Then
            Dim transactionID As String = Request("merchantTransID")
            Dim companyID As Integer
            If Not Integer.TryParse(Request("CompanyID"), companyID) Then
                Response.End()
                Return
            End If
            Dim company As Merchants.Merchant = Merchants.Merchant.Load(companyID)
            Dim tableName As String = Request("tblName")

            hfCompanyNumber.Value = company.Number

            If Request("isUseLimitation") = "1" Then
                IsValidated = True
                'literalIsAddress1.Visible = literalIsAddress2.Visible = literalIsCity.Visible = literalIsCountry.Visible = literalIsZipcode.Visible = company.
                literalIsCvv2.Visible = company.ProcessSettings.IsSystemPayCVV2
                literalIsIDNumber.Visible = company.ProcessSettings.IsSystemPayPersonalNumber
                literalIsPhoneNumber.Visible = company.ProcessSettings.IsSystemPayPhoneNumber
                literalIsEmail.Visible = company.ProcessSettings.IsSystemPayEmail
            Else
                IsValidated = False
                literalIsAddress1.Visible = literalIsAddress2.Visible = literalIsCity.Visible = literalIsCountry.Visible = literalIsZipcode.Visible = False
                literalIsCvv2.Visible = False
                literalIsIDNumber.Visible = False
                literalIsPhoneNumber.Visible = False
                literalIsEmail.Visible = False
                literalIsEmail.Visible = False
            End If

            If transactionID <> Nothing And tableName <> "" Then
                LoadTransaction(Integer.Parse(transactionID), tableName)
            End If

            tbOrderNumber.Text = transactionID

            For n As Integer = 1 To 12
                Dim currentMonth As String = n.ToString()
                If currentMonth.Length = 1 Then currentMonth = "0" & currentMonth
                Dim item As ListItem = New ListItem(currentMonth, currentMonth)
                ddExpMonth.Items.Add(item)
            Next

            For n As Integer = Date.Now.Year To Date.Now.Year + 10
                Dim currentYear As String = n.ToString().Remove(0, 2)
                Dim item As ListItem = New ListItem(n.ToString(), currentYear)
                ddExpYear.Items.Add(item)
            Next

            For n As Integer = 1 To 24
                Dim item As ListItem = New ListItem(n, n)
                ddPayments.Items.Add(item)
            Next

            'ddCreditType.DataSource = Collections.CreditTypes
            'ddCreditType.DataTextField = "Value"
            'ddCreditType.DataValueField = "Key"
            'ddCreditType.DataBind()

            ddBankState.DataSource = Netpay.Bll.State.Cache
            ddBankState.DataTextField = "name"
            ddBankState.DataValueField = "id"
            ddBankState.DataBind()

            ddBillingState.DataSource = Netpay.Bll.State.Cache
            ddBillingState.DataTextField = "name"
            ddBillingState.DataValueField = "id"
            ddBillingState.DataBind()

            ddBillingCountry.DataSource = Netpay.Bll.Country.Cache
            ddBillingCountry.DataTextField = "name"
            ddBillingCountry.DataValueField = "isoCode"
            ddBillingCountry.DataBind()

            ddCurrencyCharge.DataSource = Netpay.Bll.Currency.Cache
            ddCurrencyCharge.DataTextField = "isoCode"
            ddCurrencyCharge.DataValueField = "id"
            ddCurrencyCharge.DataBind()

            ddCurrencyManagerComissionTrans.DataSource = Netpay.Bll.Currency.Cache
            ddCurrencyManagerComissionTrans.DataTextField = "isoCode"
            ddCurrencyManagerComissionTrans.DataValueField = "id"
            ddCurrencyManagerComissionTrans.DataBind()

            ddBalanceManagementCurrency.DataSource = Netpay.Bll.Currency.Cache
            ddBalanceManagementCurrency.DataTextField = "isoCode"
            ddBalanceManagementCurrency.DataValueField = "id"
            ddBalanceManagementCurrency.DataBind()

            Dim currencyHosted = Settlements.MerchantSettings.LoadForMerchant(company.ID)
            Dim handlingFee As Decimal = currencyHosted(ddCurrencyCharge.SelectedValue).HandlingFee
            hfHandlingFee.Value = handlingFee
            For Each item As ListItem In rblHandlingFee.Items
                item.Text = item.Text.Replace("%%handlingFee%%", handlingFee)
            Next
            If handlingFee = "0" Then aHandlingFee.Visible = False

            tbCCNum1.Attributes.Add("onkeyup", "focusCCField(1)")
            tbCCNum2.Attributes.Add("onkeyup", "focusCCField(2)")
            tbCCNum3.Attributes.Add("onkeyup", "focusCCField(3)")

            Dim filterChargeType As Integer = Integer.Parse(Request("ChargeType"))

            Select Case filterChargeType
                Case 1
                    mvMain.ActiveViewIndex = 0
                    menuMainView.Items(0).Selected = True
                    ddPaymentMethod.ClearSelection()
                    ddPaymentMethod.SelectedIndex = 0
                    ddPaymentMethod_SelectedIndexChanged(Nothing, Nothing)
                Case 2
                    mvMain.ActiveViewIndex = 0
                    menuMainView.Items(0).Selected = True
                    ddPaymentMethod.ClearSelection()
                    ddPaymentMethod.SelectedIndex = 1
                    ddPaymentMethod_SelectedIndexChanged(Nothing, Nothing)
                Case 3
                    mvMain.ActiveViewIndex = 1
                    menuMainView.Items(1).Selected = True
                Case 4
                    mvMain.ActiveViewIndex = 2
                    menuMainView.Items(2).Selected = True
                Case Else
            End Select
        End If
    End Sub

    Protected Function CheckDBNull(ByVal value As Object) As String
        If TypeOf (value) Is DBNull Then Return ""
        Return value
    End Function

    Protected Sub LoadTransaction(ByVal transactionID As Integer, ByVal tableName As String)
        Dim transactionSql As String = "SELECT tblCompanyTrans" & tableName & ".Amount, tblCompanyTrans" & tableName & ".Currency, tblCompanyTrans" & tableName & ".Payments, tblCompanyTrans" & tableName & ".CreditType, tblCompanyTrans" & tableName & ".OrderNumber, tblCreditCard.id As CreditCard_id, tblCreditCard.PersonalNumber, dbo.GetDecrypted256(tblCreditCard.CCard_number256) CCard_number, tblCreditCard.ExpMM, tblCreditCard.ExpYY, tblCreditCard.Member, dbo.GetCUI(tblCreditCard.cc_cui) AS cc_cui, tblCreditCard.phoneNumber, tblCreditCard.email, tblBillingAddress.address1, tblBillingAddress.address2, tblBillingAddress.city, tblBillingAddress.zipCode, tblBillingAddress.stateIso, tblBillingAddress.countryIso FROM tblCompanyTrans" & tableName & " LEFT OUTER JOIN tblCreditCard ON tblCompanyTrans" & tableName & ".PaymentMethodID = tblCreditCard.ID LEFT OUTER JOIN tblBillingAddress ON tblCreditCard.BillingAddressId = tblBillingAddress.id WHERE tblCompanyTrans" & tableName & ".PaymentMethod_id = 1 AND tblCompanyTrans" & tableName & ".id = " & transactionID

        Dim drData As SqlDataReader = dbPages.ExecReader(transactionSql)
        If drData.HasRows Then
            drData.Read()

            hfCreditCardID.Value = CheckDBNull(drData("CreditCard_id"))

            tbTransactionAmount.Text = drData("Amount")

            If Not IsDBNull(drData("Currency")) Then
                ddCurrencyCharge.ClearSelection()
                ddCurrencyCharge.Items.FindByValue(drData("Currency")).Selected = True
            End If

            If Not IsDBNull(drData("Payments")) Then
                ddPayments.ClearSelection()
                ddPayments.Items.FindByValue(drData("Payments")).Selected = True
            End If

            If Not IsDBNull(drData("CreditType")) Then
                ddCreditType.ClearSelection()
                ddCreditType.Items.FindByValue(drData("CreditType")).Selected = True
                ddCreditType_OnSelectedIndexChanged(Nothing, Nothing)
            End If

            If Not IsDBNull(drData("CCard_number")) Then
                Dim ccNumber As String = drData("CCard_number")
                If ccNumber <> Nothing And ccNumber.Trim() <> "" Then
                    tbCCNum1.Text = "****" 'parsedCCNumber(0)
                    tbCCNum2.Text = "****" 'parsedCCNumber(1)
                    tbCCNum3.Text = "****" 'parsedCCNumber(2)
                    tbCCNum4.Text = ccNumber.Substring(ccNumber.Length - 4)
                End If
            End If

            If Not IsDBNull(drData("ExpMM")) Then
                ddExpMonth.ClearSelection()
                ddExpMonth.Items.FindByValue(drData("ExpMM").ToString().Trim()).Selected = True
            End If

            If Not IsDBNull(drData("ExpYY")) Then
                ddExpYear.ClearSelection()
                ddExpYear.Items.FindByValue(drData("ExpYY").ToString().Trim()).Selected = True
            End If

            tbCardHolderName.Text = CheckDBNull(drData("Member"))
            tbCVV2.Text = "****"    'CheckDBNull(drData("cc_cui"))
            tbCardHolderIDNumber.Text = CheckDBNull(drData("PersonalNumber"))
            tbCardHolderPhoneNumber.Text = CheckDBNull(drData("phoneNumber"))
            tbCardHolderEmail.Text = CheckDBNull(drData("email"))
            tbBillingAddress1.Text = CheckDBNull(drData("address1"))
            tbBillingAddress2.Text = CheckDBNull(drData("address2"))
            tbBillingCity.Text = CheckDBNull(drData("city"))
            tbBillingZipcode.Text = CheckDBNull(drData("zipCode"))

            If Not IsDBNull(drData("stateIso")) Then
                ddBillingState.ClearSelection()
                ddBillingState.Items.FindByValue("stateIso").Selected = True
            End If

            If Not IsDBNull(drData("countryIso")) Then
                ddBillingCountry.ClearSelection()
                ddBillingCountry.Items.FindByValue("countryIso").Selected = True
            End If

            drData.Close()
        Else
            lblError.Text = "Transaction not found"
        End If
    End Sub

    Protected Sub menuMainView_MenuItemClick(ByVal sender As Object, ByVal e As MenuEventArgs)
        Dim index As Integer = Int32.Parse(e.Item.Value)
        mvMain.ActiveViewIndex = index
    End Sub

    Protected Sub ddCreditType_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim selectedValue As String = ddCreditType.SelectedValue
        If selectedValue = "6" Or selectedValue = "8" Then
            ddPayments.Enabled = True
        Else
            ddPayments.SelectedIndex = 0
            ddPayments.Enabled = False
        End If
    End Sub

    Protected Sub btnCommit_click(ByVal sender As Object, ByVal e As EventArgs)
        Dim amount As Double
        If Not Double.TryParse(tbTransactionAmount.Text, amount) Then
            lblError.Text = "Invalid amount"
            Return
        End If

        If amount = 0 Or amount > 1000000 Then
            lblError.Text = "Invalid amount"
            Return
        End If

        If IsValidated Then
            Dim rx As Regex = New Regex("^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")
            If Not rx.IsMatch(tbCardHolderEmail.Text) Then
                lblError.Text = "Invalid email"
                Return
            End If

            If tbCardHolderPhoneNumber.Text.Length < 1 Or tbCardHolderPhoneNumber.Text.Length > 20 Then
                lblError.Text = "A valid phone number is required"
                Return
            End If

            If tbCardHolderIDNumber.Text.Length < 1 Or tbCardHolderIDNumber.Text.Length > 20 Then
                lblError.Text = "A valid card holder id is required"
                Return
            End If

            If tbBillingAddress1.Text.Length < 1 Or tbBillingAddress1.Text.Length > 20 Then
                lblError.Text = "A valid billing address is required"
                Return
            End If

            If tbBillingCity.Text.Length < 1 Or tbBillingCity.Text.Length > 20 Then
                lblError.Text = "A valid billing city is required"
                Return
            End If

            If tbBillingZipcode.Text.Length < 1 Or tbBillingZipcode.Text.Length > 20 Then
                lblError.Text = "A valid billing zipcode is required"
                Return
            End If

            If ddBillingCountry.SelectedIndex = 0 Then
                lblError.Text = "Billing country is required"
                Return
            End If

            If Not cbIsCheckCVV2.Checked And (tbCVV2.Text.Length < 3 Or tbCVV2.Text.Length > 4) Then
                lblError.Text = "A valid cvv2 is required"
                Return
            End If
        End If

        Dim uri As StringBuilder = New StringBuilder()
        uri.Append("charge_formAction1.asp?")
        uri.Append("CompanyNum=" & hfCompanyNumber.Value)
        uri.Append("&companyID=" & Request("CompanyID"))
        uri.Append("&CreditCard_id=" & hfCreditCardID.Value)
        uri.Append("&CreditType=" & ddCreditType.SelectedValue)
        uri.Append("&Payments=" & ddPayments.SelectedValue)
        uri.Append("&UserName=" & tbCustomerName.Text)
        uri.Append("&Amount=" & tbTransactionAmount.Text)
        uri.Append("&Currency=" & ddCurrencyCharge.SelectedValue)
        uri.Append("&PersonalNumber=" & tbCardHolderIDNumber.Text)
        uri.Append("&Email=" & tbCardHolderEmail.Text)
        uri.Append("&Comment=" & tbComment.Text)
        uri.Append("&ConfirmationNum=" & tbConfirmationNumber.Text)
        uri.Append("&Order=" & tbOrderNumber.Text)
        uri.Append("&CustomerID=0")
        uri.Append("&PhoneNumber=" & tbCardHolderPhoneNumber.Text)
        uri.Append("&CCard_num1=" & tbCCNum1.Text)
        uri.Append("&CCard_num2=" & tbCCNum2.Text)
        uri.Append("&CCard_num3=" & tbCCNum3.Text)
        uri.Append("&CCard_num4=" & tbCCNum4.Text)
        uri.Append("&ExpMonth=" & ddExpMonth.SelectedValue)
        uri.Append("&ExpYear=" & ddExpYear.SelectedValue)
        uri.Append("&Member=" & tbCardHolderName.Text)
        uri.Append("&CVV2=" & tbCVV2.Text)
        uri.Append("&BillingAddress1=" & tbBillingAddress1.Text)
        uri.Append("&BillingAddress2=" & tbBillingAddress2.Text)
        uri.Append("&BillingCity=" & tbBillingCity.Text)
        uri.Append("&BillingZipCode=" & tbBillingZipcode.Text)
        uri.Append("&BillingState=" & ddBillingState.SelectedValue)
        uri.Append("&BillingCountry=" & ddBillingCountry.SelectedValue)

        Response.Redirect(uri.ToString())
    End Sub

    Protected Sub btnShowTransaction_click(ByVal sender As Object, ByVal e As EventArgs)
        Dim transactionID As Integer
        If Not Integer.TryParse(tbTransactionNumber.Text, transactionID) Then
            lblError.Text = "Invalid transaction number"
            Return
        End If

        LoadTransaction(transactionID, ddTransactionStatus.SelectedValue)
    End Sub

    Protected Sub btnToggleChargeValidation_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        IsValidated = Not IsValidated
    End Sub

    Protected Property IsValidated() As Boolean
        Get
            Return btnToggleChargeValidation.CommandArgument = "1"
        End Get
        Set(ByVal value As Boolean)
            If value Then
                btnToggleChargeValidation.Text = "Disable Charge Validation"
                btnToggleChargeValidation.CommandArgument = "1"
                literalIsAddress1.Visible = True
                literalIsAddress2.Visible = True
                literalIsCity.Visible = True
                literalIsCountry.Visible = True
                literalIsCvv2.Visible = True
                literalIsEmail.Visible = True
                literalIsIDNumber.Visible = True
                literalIsPhoneNumber.Visible = True
                literalIsZipcode.Visible = True
            Else
                btnToggleChargeValidation.Text = "Enable Charge Validation"
                btnToggleChargeValidation.CommandArgument = "0"
                literalIsAddress1.Visible = False
                literalIsAddress2.Visible = False
                literalIsCity.Visible = False
                literalIsCountry.Visible = False
                literalIsCvv2.Visible = False
                literalIsEmail.Visible = False
                literalIsIDNumber.Visible = False
                literalIsPhoneNumber.Visible = False
                literalIsZipcode.Visible = False
            End If
        End Set
    End Property

    Protected Sub ddPaymentMethod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddPaymentMethod.SelectedValue = "1" Then
            PaymentMethodCC.Visible = True
            PaymentMethodECHECK.Visible = False
        Else
            PaymentMethodCC.Visible = False
            PaymentMethodECHECK.Visible = True
        End If
    End Sub

    Protected Sub aHandlingFee_click(ByVal sender As Object, ByVal e As EventArgs)
        rblManagerCommissionTransactionType.SelectedIndex = 1
        ddCurrencyManagerComissionTrans.SelectedIndex = 0
        tbManagerComissionTransAmount.Text = hfHandlingFee.Value
        tbCommentManagerComissionTrans.Text = "Handling fee - "
        tbCommentManagerComissionTrans.Focus()
    End Sub

    Protected Sub btnCommitManagerCommissionTransaction_click(ByVal sender As Object, ByVal e As EventArgs)
        Dim amount As Double
        If Not Double.TryParse(tbManagerComissionTransAmount.Text, amount) Then
            lblError.Text = "Invalid amount"
            Return
        End If

        If amount = 0 Or amount > 1000000 Then
            lblError.Text = "Invalid amount"
            Return
        End If

        Dim paymentMethod As String = ""
        Dim paymentMethodOld As String = ""
        Dim paymentMethodDisplay As String = ""
        If hfHandlingFee.Value <> "0" Then
            If rblManagerCommissionTransactionType.SelectedValue = "1" Then
                paymentMethod = "3"
                paymentMethodOld = "5"
                paymentMethodDisplay = "Handling fee"
            Else
                paymentMethod = "2"
                paymentMethodOld = "4"
                paymentMethodDisplay = "---"
            End If
        End If

        Dim uri As StringBuilder = New StringBuilder()
        uri.Append("charge_formAction3.asp?")
        uri.Append("companyID=" & Request("CompanyID"))
        uri.Append("&Amount=" & tbManagerComissionTransAmount.Text)
        uri.Append("&CreditType=" & rblCreditTypeManagerComissionTrans.SelectedValue)
        uri.Append("&Currency=" & ddCurrencyManagerComissionTrans.SelectedValue)
        uri.Append("&Comment=" & tbCommentManagerComissionTrans.Text)
        uri.Append("&PaymentMethod=" & paymentMethod)
        uri.Append("&PaymentMethodOld=" & paymentMethodOld)
        uri.Append("&PaymentMethodDisplay=" & paymentMethodDisplay)
        uri.Append("&HandlingFee=" & rblManagerCommissionTransactionType.SelectedValue)

        Response.Redirect(uri.ToString())
    End Sub

    Protected Sub btnBalanceManagementCommit_click(ByVal sender As Object, ByVal e As EventArgs)
        Dim amount As Double
        If Not Double.TryParse(tbBalanceManagementAmount.Text, amount) Then
            lblError.Text = "Invalid amount"
            Return
        End If

        If amount = 0 Or amount > 1000000 Then
            lblError.Text = "Invalid amount"
            Return
        End If

        Dim uri As StringBuilder = New StringBuilder()
        uri.Append("charge_formAction4.asp?")
        uri.Append("CompanyID=" & Request("CompanyID"))
        uri.Append("&Amount=" & tbBalanceManagementAmount.Text)
        uri.Append("&CreditType=" & rblBalanceManagementCreditType.SelectedValue)
        uri.Append("&Currency=" & ddBalanceManagementCurrency.SelectedValue)
        uri.Append("&Comment=" & tbBalanceManagementComment.Text)

        Response.Redirect(uri.ToString())
    End Sub

    Protected Sub rblChargeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        If rblChargeType.SelectedValue = "3" Then
            literalConfirmationNum.Visible = True
            tbConfirmationNumber.Visible = True
        Else
            literalConfirmationNum.Visible = False
            tbConfirmationNumber.Visible = False
        End If
    End Sub

    Protected Sub cbIsCheckCVV2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If cbIsCheckCVV2.Checked Then
            tbCVV2.Enabled = False
        Else
            tbCVV2.Enabled = True
        End If
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminNetEng.css" />
	<style type="text/css">
		.dataHeading
		{
			text-decoration: underline;
			white-space: nowrap;
		}
	</style>
</head>
<body>
	<form id="Form1" runat="server">
	<table align="center" style="width: 92%">
		<tr>
			<td colspan="2" id="pageMainHeading">
				<asp:Label ID="lblPermissions" runat="server" />
				CHARGES<br />
			</td>
		</tr>
	</table>
	<table class="formNormal" border="0" align="center" style="width: 92%">
		<tr>
			<td>
				<asp:Menu ID="menuMainView" Visible="false" OnMenuItemClick="menuMainView_MenuItemClick" Orientation="Horizontal" StaticSelectedStyle-Font-Bold="true" StaticHoverStyle-BackColor="#f5f5f5" runat="server">
					<Items>
						<asp:MenuItem Text="Charges" Value="0"></asp:MenuItem>
						<asp:MenuItem Text="Manager / Comission Transaction" Value="1"></asp:MenuItem>
						<asp:MenuItem Text="Balance Management" Value="2"></asp:MenuItem>
					</Items>
				</asp:Menu>
			</td>
		</tr>
		<tr>
			<td>
				<asp:Label ID="Label1" Font-Size="X-Large" ForeColor="Red" runat="server"></asp:Label>
			</td>
		</tr>
		<tr>
			<td>
				<asp:Label ID="lblError" Font-Size="Large" ForeColor="Red" runat="server"></asp:Label>
			</td>
		</tr>
		<tr>
			<td>
				<asp:MultiView ID="mvMain" runat="server">
					<asp:View ID="vCharges" runat="server">
						<asp:HiddenField ID="hfCompanyNumber" runat="server" />
						<asp:HiddenField ID="hfCreditCardID" runat="server" />
						<asp:HiddenField ID="hfHandlingFee" runat="server" />
						<table>
							<tr>
								<td valign="top" class="txt13b" align="left">
									<table align="center" border="0" cellpadding="1" cellspacing="2" style="border: 1px solid #000000;" width="900">
										<tr>
											<td align="left" bgcolor="#f5f5f5" class="txt11" dir="ltr">
												[
												<asp:LinkButton ID="btnToggleChargeValidation" runat="server" CssClass="faq" Font-Underline="true" Text="Disable Charge Validation" OnClick="btnToggleChargeValidation_Click"></asp:LinkButton>
												] [ <a class="faq" href="#" onclick="OpenPop('charge_Customer_list.asp','fraCustomer',350,450,1);">Charge
													<%=dbPages.TestVar(HttpContext.Current.Session("Identity"), -1, "")%>
													Customer</a> ]
											</td>
											<td bgcolor="#f5f5f5" class="txt12" dir="ltr">
												Transaction No.
												<asp:TextBox ID="tbTransactionNumber" runat="server" CssClass="inputdata"></asp:TextBox>
												Transaction Status
												<asp:DropDownList ID="ddTransactionStatus" runat="server">
													<asp:ListItem Text="Passed" Value="Pass"></asp:ListItem>
													<asp:ListItem Text="Failed" Value="Fail"></asp:ListItem>
												</asp:DropDownList>
												<asp:Button ID="btnShowTransaction" OnClick="btnShowTransaction_click" runat="server" CssClass="button1" Text="Show" />
											</td>
										</tr>
									</table>
									<br />
									<table align="center" border="0" cellpadding="1" cellspacing="2" style="border: 1px solid #000000;" width="100%">
										<tr>
											<!--td class="head" valign="top" bgcolor="#f5f5f5">
                                                        Charge type <span style="color:maroon;">*</span>
                                                        <br />
                                                        <asp:DropDownList ID="ddPaymentMethod" Visible="false" OnSelectedIndexChanged="ddPaymentMethod_SelectedIndexChanged" AutoPostBack="true" runat="server">
                                                            <asp:ListItem Text="Credit Card" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Check" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td-->
											<td colspan="4" align="right" bgcolor="#f5f5f5" class="txt12" colspan="3" dir="ltr" valign="top">
												<asp:RadioButtonList ID="rblChargeType" OnSelectedIndexChanged="rblChargeType_SelectedIndexChanged" AutoPostBack="true" runat="server">
													<asp:ListItem Text="Process transaction" Selected="True" Value="1"></asp:ListItem>
													<asp:ListItem Text="Insert transaction" Value="2"></asp:ListItem>
													<asp:ListItem Text="Process pre-authorized" Value="3"></asp:ListItem>
												</asp:RadioButtonList>
												<asp:Literal ID="literalConfirmationNum" Visible="false" runat="server">Confirmation number </asp:Literal>
												<asp:TextBox ID="tbConfirmationNumber" Visible="false" runat="server"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td bgcolor="#f5f5f5" colspan="4" height="8">
												<br />
												<br />
											</td>
										</tr>
										<tr>
											<td colspan="4" bgcolor="#f5f5f5">
												<table id="PaymentMethodCC" border="0" style="border: 1px solid #c0c0c0;" width="100%" visible="true" runat="server">
													<tr>
														<td class="head" width="18%" bgcolor="#f5f5f5">
															Card Expiry Date <span style="color: maroon;">*</span>
														</td>
														<td align="right" bgcolor="#f5f5f5" class="txt12" nowrap width="30%">
															<asp:DropDownList ID="ddExpMonth" runat="server" CssClass="input2">
															</asp:DropDownList>
															/
															<asp:DropDownList ID="ddExpYear" runat="server" CssClass="input2">
															</asp:DropDownList>
														</td>
														<td class="head" width="20%" bgcolor="#f5f5f5">
															Credit Card Number <span style="color: maroon;">*</span>
														</td>
														<td class="head" width="32%" bgcolor="#f5f5f5">
															<asp:TextBox ID="tbCCNum1" runat="server" CssClass="input2" MaxLength="4" Width="32px"></asp:TextBox>
															<asp:TextBox ID="tbCCNum2" runat="server" CssClass="input2" MaxLength="4" Width="32px"></asp:TextBox>
															<asp:TextBox ID="tbCCNum3" runat="server" CssClass="input2" MaxLength="4" Width="32px"></asp:TextBox>
															<asp:TextBox ID="tbCCNum4" runat="server" CssClass="input2" MaxLength="4" Width="32px"></asp:TextBox>
														</td>
													</tr>
													<tr>
														<td class="head" bgcolor="#f5f5f5">
															Card Verification Value
															<asp:Literal ID="literalIsCvv2" runat="server"><span style="color:maroon;">*</span></asp:Literal>
														</td>
														<td class="head" bgcolor="#f5f5f5">
															<asp:TextBox ID="tbCVV2" runat="server" CssClass="input2"></asp:TextBox>
															&nbsp;
															<asp:CheckBox ID="cbIsCheckCVV2" OnCheckedChanged="cbIsCheckCVV2_CheckedChanged" AutoPostBack="true" runat="server" />
															<span class="txt11">Without CVV</span>
														</td>
														<td class="head" bgcolor="#f5f5f5">
															Card Holder Name <span style="color: maroon;">*</span>
														</td>
														<td class="head" bgcolor="#f5f5f5">
															<asp:TextBox ID="tbCardHolderName" runat="server"></asp:TextBox>
														</td>
													</tr>
													<tr>
														<td class="head" bgcolor="#f5f5f5">
															Credit Type<span style="color: maroon;">*</span>
														</td>
														<td class="head" bgcolor="#f5f5f5">
															<netpay:CreditTypeDropDown ID="ddCreditType" runat="server" CssClass="input2" OnSelectedIndexChanged="ddCreditType_OnSelectedIndexChanged" AutoPostBack="true" />
														</td>
														<td class="head" bgcolor="#f5f5f5">
															Payments<span style="color: maroon;">*</span>
														</td>
														<td class="head" bgcolor="#f5f5f5">
															<asp:DropDownList ID="ddPayments" Enabled="false" runat="server" CssClass="input2">
															</asp:DropDownList>
														</td>
													</tr>
												</table>
												<!--/*************ECHECK*******************/-->
												<table id="PaymentMethodECHECK" align="center" border="0" cellpadding="1" cellspacing="2" dir="ltr" style="border: 1px solid #c0c0c0;" width="100%" visible="false" runat="server">
													<tr>
														<td class="head" rowspan="1" valign="top" width="25%" bgcolor="#f5f5f5">
															US Bank account only
														</td>
														<td align="right" class="head" colspan="2" dir="ltr" valign="bottom" bgcolor="#f5f5f5">
															Full name / Account <span style="color: maroon;">*</span><br />
															<asp:TextBox ID="tbAccountName" runat="server" MaxLength="50" Width="270px"></asp:TextBox>
														</td>
													</tr>
													<tr>
														<td align="right" class="head" dir="ltr" valign="bottom" bgcolor="#f5f5f5">
															ID Number <span style="color: maroon;">*</span><br />
															<asp:TextBox ID="tbIDNumber" runat="server" Width="120px"></asp:TextBox>
														</td>
														<td align="right" class="head" dir="ltr" valign="bottom" bgcolor="#f5f5f5">
															Bank number <span style="color: maroon;">*</span><br />
															<asp:TextBox ID="tbBankRoutingNumber" runat="server" Width="120px"></asp:TextBox>
														</td>
														<td align="right" class="head" dir="ltr" valign="bottom" bgcolor="#f5f5f5">
															Account number <span style="color: maroon;">*</span><br />
															<asp:TextBox ID="tbAccountNumber" runat="server" Width="120px"></asp:TextBox>
														</td>
														<td align="right" class="head" dir="ltr" valign="bottom" bgcolor="#f5f5f5">
															Account type <span style="color: maroon;">*</span><br />
															<select runat="server" class="input2" name="bankAccountType">
																<option value="1">Transactional</option>
																<option value="2">Saving</option>
															</select>
														</td>
													</tr>
													<tr>
														<td align="right" class="head" valign="bottom" bgcolor="#f5f5f5">
															Bank name <span style="color: maroon;">*</span><br />
															<asp:TextBox ID="tbBankName" runat="server" Width="120px"></asp:TextBox>
														</td>
														<td align="right" class="head" valign="bottom" bgcolor="#f5f5f5">
															Bank phone <span style="color: maroon;">*</span><br />
															<asp:TextBox ID="tbBankPhone" runat="server" Width="120px"></asp:TextBox>
														</td>
														<td align="right" class="head" valign="bottom" bgcolor="#f5f5f5">
															Bank state <span style="color: maroon;">*</span><br />
															<asp:DropDownList ID="ddBankState" runat="server">
															</asp:DropDownList>
														</td>
														<td align="right" class="head" dir="ltr" valign="bottom" bgcolor="#f5f5f5">
															Bank city <span style="color: maroon;">*</span><br />
															<asp:TextBox ID="tbBankCity" runat="server" Width="120px"></asp:TextBox>
														</td>
													</tr>
												</table>
												<br />
											</td>
										</tr>
										<tr>
											<td class="head" width="20%" bgcolor="#f5f5f5">
												Customer name
											</td>
											<td class="head" bgcolor="#f5f5f5">
												<asp:TextBox ID="tbCustomerName" runat="server" CssClass="input2"></asp:TextBox>
											</td>
											<td class="head" bgcolor="#f5f5f5">
												Order number
											</td>
											<td class="head" valign="top" bgcolor="#f5f5f5">
												<asp:TextBox ID="tbOrderNumber" runat="server" CssClass="input2"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="head" bgcolor="#f5f5f5">
												Phone
												<asp:Literal ID="literalIsPhoneNumber" runat="server"><span 
                                                        style="color:maroon;">*</span></asp:Literal>
											</td>
											<td class="head" bgcolor="#f5f5f5">
												<asp:TextBox ID="tbCardHolderPhoneNumber" Width="150" runat="server"></asp:TextBox>
											</td>
											<td class="head" bgcolor="#f5f5f5">
												ID number
												<asp:Literal ID="literalIsIDNumber" runat="server"><span 
                                                        style="color:maroon;">*</span></asp:Literal>
											</td>
											<td class="head" bgcolor="#f5f5f5">
												<asp:TextBox ID="tbCardHolderIDNumber" Width="150" runat="server"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td align="right" bgcolor="#f5f5f5" class="head" dir="ltr">
												Birth date (dd/mm/yyyy)<br />
											</td>
											<td align="right" bgcolor="#f5f5f5" class="txt12" dir="ltr">
												<asp:TextBox ID="tbCardHolderBirthDate" runat="server" Width="150"></asp:TextBox>
											</td>
											<td class="head" bgcolor="#f5f5f5">
												Email
												<asp:Literal ID="literalIsEmail" runat="server"><span style="color:maroon;">*</span></asp:Literal>
											</td>
											<td class="head" bgcolor="#f5f5f5">
												<asp:TextBox ID="tbCardHolderEmail" runat="server" MaxLength="80" Width="150"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td bgcolor="#f5f5f5" colspan="4" height="8">
												<br />
													<br /></br>
												</br>
											</td>
										</tr>
										<tr>
											<td class="head" width="13%" bgcolor="#f5f5f5">
												City
												<asp:Literal ID="literalIsCity" runat="server"><span 
                                                        style="color:maroon;">*</span></asp:Literal>
											</td>
											<td bgcolor="#f5f5f5" class="txt12" dir="ltr">
												<asp:TextBox ID="tbBillingCity" Width="150" runat="server"></asp:TextBox>
											</td>
											<td class="head" width="20%" bgcolor="#f5f5f5">
												Address 1
												<asp:Literal ID="literalIsAddress1" runat="server"><span 
                                                        style="color:maroon;">*</span></asp:Literal>
											</td>
											<td bgcolor="#f5f5f5" class="txt12" dir="ltr" width="32%">
												<asp:TextBox ID="tbBillingAddress1" Width="150" runat="server"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="head" bgcolor="#f5f5f5">
												State
												<asp:Literal ID="literalIsAddress2" runat="server"><span style="color:maroon;">*</span></asp:Literal>
											</td>
											<td bgcolor="#f5f5f5" class="txt12" dir="ltr">
												<asp:DropDownList ID="ddBillingState" Width="155" runat="server">
												</asp:DropDownList>
											</td>
											<td class="head" bgcolor="#f5f5f5">
												Address 2
											</td>
											<td bgcolor="#f5f5f5" class="txt12" dir="ltr">
												<asp:TextBox ID="tbBillingAddress2" Width="150" runat="server"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="head" bgcolor="#f5f5f5">
												Country
												<asp:Literal ID="literalIsCountry" runat="server"><span style="color:maroon;">*</span></asp:Literal>
											</td>
											<td bgcolor="#f5f5f5" class="txt12" dir="ltr">
												<asp:DropDownList ID="ddBillingCountry" Width="155" runat="server">
												</asp:DropDownList>
											</td>
											<td class="head" bgcolor="#f5f5f5">
												Zip code
												<asp:Literal ID="literalIsZipcode" runat="server"><span style="color:maroon;">*</span></asp:Literal>
											</td>
											<td bgcolor="#f5f5f5" class="txt12" dir="ltr">
												<asp:TextBox ID="tbBillingZipcode" Width="150" runat="server"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="head" valign="top" bgcolor="#f5f5f5">
												Comment
											</td>
											<td class="head" bgcolor="#f5f5f5">
												<asp:TextBox ID="tbComment" Width="150" runat="server"></asp:TextBox>
											</td>
											<td class="head" bgcolor="#f5f5f5">
												Amount <span style="color: maroon;">*</span>
											</td>
											<td class="head" bgcolor="#f5f5f5">
												<asp:TextBox ID="tbTransactionAmount" Width="150" runat="server"></asp:TextBox>
												<asp:DropDownList ID="ddCurrencyCharge" runat="server">
												</asp:DropDownList>
											</td>
										</tr>
									</table>
									<table border="0" cellpadding="1" cellspacing="0" style="border: 0px solid #000000;" width="900">
										<tr valign="top">
											<td class="txt11" style="padding-top: 10px; text-align: left;">
												Required fields <span style="color: maroon;">*</span><br />
											</td>
											<td style="text-align: left; vertical-align: top;">
												<asp:RadioButtonList ID="rblHandlingFee" runat="server">
													<asp:ListItem Text="Do not add handling charge" Selected="True" Value="0"></asp:ListItem>
													<asp:ListItem Text="Add handling charge (%%handlingFee%%)" Value="1"></asp:ListItem>
													<asp:ListItem Text="Add handling charge (%%handlingFee%%) only for passed transaction" Value="2"></asp:ListItem>
												</asp:RadioButtonList>
											</td>
											<td style="padding-top: 10px; text-align: right">
												<asp:Button ID="btnCommit" runat="server" CssClass="button4" OnClick="btnCommit_click" Text=" Commit " />
												&nbsp;&nbsp;
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</asp:View>
					<asp:View ID="vManagerComissionCharge" runat="server">
						<tr>
							<td valign="top" align="right" class="txt13b">
								<table align="left" border="0" cellpadding="1" cellspacing="2" width="900" style="border: 1px solid #000000;">
									<tr>
										<td class="head" valign="top" bgcolor="#f5f5f5">
											Transaction type
										</td>
										<td align="right" dir="ltr" class="txt12" bgcolor="#f5f5f5">
											<asp:RadioButtonList ID="rblManagerCommissionTransactionType" RepeatDirection="Horizontal" runat="server">
												<asp:ListItem Text="Manager transaction" Value="0" Selected="True"></asp:ListItem>
												<asp:ListItem Text="Commission transaction" Value="1"></asp:ListItem>
											</asp:RadioButtonList>
										</td>
									</tr>
									<tr>
										<td class="head" width="20%" bgcolor="#f5f5f5">
											Amount
										</td>
										<td align="right" dir="ltr" class="" bgcolor="#f5f5f5">
											<asp:TextBox ID="tbManagerComissionTransAmount" CssClass="txt12" runat="server"></asp:TextBox>
											<asp:DropDownList ID="ddCurrencyManagerComissionTrans" runat="server">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="head" width="20%" bgcolor="#f5f5f5">
											Debit / Credit
										</td>
										<td align="right" dir="ltr" class="txt12" bgcolor="#f5f5f5">
											<asp:RadioButtonList ID="rblCreditTypeManagerComissionTrans" RepeatDirection="Horizontal" runat="server">
												<asp:ListItem Text="Debit" Value="0" style="color: Red;" Selected="True"></asp:ListItem>
												<asp:ListItem Text="Credit" style="color: Green;" Value="1"></asp:ListItem>
												<asp:ListItem Text="Both" Value="-1"></asp:ListItem>
											</asp:RadioButtonList>
										</td>
									</tr>
									<tr>
										<td class="head" valign="top" bgcolor="#f5f5f5">
											Comment
										</td>
										<td class="head" bgcolor="#f5f5f5">
											<asp:TextBox ID="tbCommentManagerComissionTrans" CssClass="input2" MaxLength="200" runat="server"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td colspan="2" bgcolor="#f5f5f5">
											<a class="faq_eng" id="aHandlingFee" onserverclick="aHandlingFee_click" runat="server">Handling fee ...</a> <a class="faq_eng" onclick="document.getElementById('tbCommentManagerComissionTrans').value='Deduction Of 10% Rolling Reserve Funds for the period of processing transactions until �';">Deduction Of 10% ...</a> <a class="faq_eng" onclick="document.getElementById('tbCommentManagerComissionTrans').value='Wire Transfer fee �';">Wire Transfer ...</a>
										</td>
									</tr>
								</table>
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<table border="0" cellpadding="1" cellspacing="2" width="900">
									<tr>
										<td style="text-align: right;">
											<asp:Button ID="btnManagerCommissionTransactionCommit" Text=" Commit " OnClick="btnCommitManagerCommissionTransaction_click" CssClass="button4" runat="server" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</asp:View>
					<asp:View ID="vBalanceManagement" runat="server">
						<tr>
							<td valign="top" class="txt13b">
								<table align="left" border="0" cellpadding="1" cellspacing="2" width="900" style="border: 1px solid #000000;">
									<tr>
										<td class="head" bgcolor="#f5f5f5">
											Debit / Credit
										</td>
										<td align="right" dir="ltr" class="txt12" bgcolor="#f5f5f5">
											<asp:RadioButtonList ID="rblBalanceManagementCreditType" RepeatDirection="Horizontal" runat="server">
												<asp:ListItem Text="Debit" Value="0" style="color: Red;" Selected="True"></asp:ListItem>
												<asp:ListItem Text="Credit" Value="1" style="color: Green;"></asp:ListItem>
											</asp:RadioButtonList>
										</td>
									</tr>
									<tr>
										<td class="head" bgcolor="#f5f5f5">
											Amount
										</td>
										<td class="head" bgcolor="#f5f5f5">
											<asp:TextBox ID="tbBalanceManagementAmount" CssClass="txt12" runat="server"></asp:TextBox>
											<asp:DropDownList ID="ddBalanceManagementCurrency" runat="server">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="head" valign="top" bgcolor="#f5f5f5">
											Comment
										</td>
										<td class="head" bgcolor="#f5f5f5">
											<asp:TextBox ID="tbBalanceManagementComment" MaxLength="100" runat="server"></asp:TextBox>
										</td>
									</tr>
								</table>
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<table border="0" cellpadding="1" cellspacing="2" width="900">
									<tr>
										<td style="text-align: right;">
											<asp:Button ID="btnBalanceManagementCommit" Text=" Commit " OnClick="btnBalanceManagementCommit_click" CssClass="button4" runat="server" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</asp:View>
				</asp:MultiView>
			</td>
		</tr>
	</table>
	</form>
</body>
</html>
