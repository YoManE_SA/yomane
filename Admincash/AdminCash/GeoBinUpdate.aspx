﻿<%@ Page Language="VB" %>

<script runat="server">
	Const FileUrl As String = "http://software77.net/geo-ip/?DL=1"
	'Const FileUrl As String = "http://192.168.5.11/test/IpToCountry.csv.gz"
	Private Function DownloadFile() As Integer
		Dim nCount As Integer = 0
		Try
			Dim loHttp As System.Net.HttpWebRequest = System.Net.WebRequest.Create(FileUrl)
			Dim loWebResponse As System.Net.HttpWebResponse = loHttp.GetResponse()
			Dim gz As System.IO.Compression.GZipStream = New System.IO.Compression.GZipStream(loWebResponse.GetResponseStream(), System.IO.Compression.CompressionMode.Decompress)
			Dim loResponseStream As System.IO.StreamReader = New System.IO.StreamReader(gz)
			If Not loResponseStream.EndOfStream Then dbPages.ExecSql("Delete From tblGeoIP")
			While Not loResponseStream.EndOfStream
				Dim sLine As String = loResponseStream.ReadLine()
				If Not sLine.StartsWith("#") Then
					If ((nCount Mod 1000) = 0) Then Response.Write("*")
					ParseLine(sLine)
					nCount += 1
				End If
			End While
			loResponseStream.Close()
			gz.Close()
			loWebResponse.Close()
		Catch e As Exception
			Response.Write(e.Message)
			Return 0
		End Try
		Return nCount
	End Function
    
	Private Sub ParseLine(sLine As String)
		Dim sValues() As String = sLine.Split(",")
		Dim lMin As Long = StripQuot(sValues(0))
		Dim lMax As Long = StripQuot(sValues(1))
		dbPages.ExecSql("Insert Into tblGeoIP(GI_Start, GI_End, GI_Diff, GI_IsoCode)Values(" & lMin & "," & lMax & "," & (lMax - lMin) & ",'" & StripQuot(sValues(4)) & "')")
	End Sub

	Private Function StripQuot(sLine As String) As String
		If sLine.StartsWith("""") Then sLine = sLine.Substring(1)
		If sLine.EndsWith("""") Then sLine = sLine.Substring(0, sLine.Length - 1)
		Return sLine
	End Function

	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		MyBase.OnLoad(e)
		Response.Buffer = False
		Response.Write("<br />" & DownloadFile())
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><title></title></head>
	<body></body>
</html>