<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20100202 Tamir
	'
	' Global data management
	'

	Protected Sub SetDeleteConfirmation(ByVal o As Object, ByVal e As GridViewRowEventArgs) Handles gvRecords.RowDataBound
		NPControls.SetGridViewDeleteConfirmation(e)
	End Sub

	Sub ReloadPage(ByVal o As Object, ByVal e As GridViewDeletedEventArgs) Handles gvRecords.RowDeleted
		Response.Redirect(Request.Url.PathAndQuery)
	End Sub

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsRecords.ConnectionString = dbPages.DSN()
		If Not Page.IsPostBack Then
			Dim nGroup As Integer = dbPages.TestVar(Request("Group"), 1, 0, -1)
			litGroup.Text = dbPages.ExecScalar("SELECT IsNull(GD_Text, '') FROM GetGlobalData(0) WHERE GD_ID=" & nGroup)
			'txtGroup.Text = dbPages.ExecScalar("SELECT IsNull(GD_Text, '') FROM GetGlobalData(0) WHERE GD_ID=" & nGroup)
			'txtGroupH.Text = dbPages.ExecScalar("SELECT IsNull(GD_Text, '') FROM GetGlobalDataLang(0, 0) WHERE GD_ID=" & nGroup)
			'litGroup.Text = txtGroup.Text
		End If
	End Sub

	Sub AddRecord(ByVal sender As Object, ByVal e As System.EventArgs)
		If String.IsNullOrEmpty(txtNewRecord.Text) OR String.IsNullOrEmpty(txtNewRecordH.Text) Then
			lblAddRecord.Text = "Please specify new values !"
		Else
			lblAddRecord.Text = String.Empty
			Dim nGroup As Integer = dbPages.TestVar(Request("Group"), 1, 0, -1)
			Dim sText As String = dbPages.DBText(txtNewRecord.Text)
			Dim sTextH As String = dbPages.DBText(txtNewRecordH.Text)
			Dim nID As Integer = dbPages.ExecScalar("SELECT IsNull(Max(GD_ID)+1, 0) FROM GetGlobalData(" & nGroup & ")")
			dbPages.ExecSql("INSERT INTO tblGlobalData(GD_Lng, GD_Group, GD_ID, GD_Text) SELECT 1, " & nGroup & ", " & nID & ", '" & sText & "'")
			dbPages.ExecSql("INSERT INTO tblGlobalData(GD_Lng, GD_Group, GD_ID, GD_Text) SELECT 0, " & nGroup & ", " & nID & ", '" & sTextH & "'")
			txtNewRecord.Text = String.Empty
			txtNewRecordH.Text = String.Empty
			gvRecords.DataBind()
		End If
	End Sub

	'Sub SaveGroup(ByVal sender As Object, ByVal e As System.EventArgs)
	'	If String.IsNullOrEmpty(txtGroup.Text) Then
	'		lblSaveGroup.Text = "Specify English group name!"
	'	ElseIf String.IsNullOrEmpty(txtGroupH.Text) Then
	'		lblSaveGroup.Text = "Specify Hebrew group value!"
	'	Else
	'		lblSaveGroup.Text = String.Empty
	'		Dim nGroup As Integer = dbPages.TestVar(Request("Group"), 1, 0, -1)
	'		Dim sText As String = dbPages.DBText(txtGroup.Text)
	'		Dim sTextH As String = dbPages.DBText(txtGroupH.Text)
	'		dbPages.ExecSql("IF NOT EXISTS (SELECT 1 FROM GetGlobalData(0) WHERE GD_ID=" & nGroup & ") INSERT INTO tblGlobalData(GD_Lng, GD_Group, GD_ID) VALUES (1, 0, " & nGroup & ")")
	'		dbPages.ExecSql("IF NOT EXISTS (SELECT 1 FROM GetGlobalDataLang(0, 0) WHERE GD_ID=" & nGroup & ") INSERT INTO tblGlobalData(GD_Lng, GD_Group, GD_ID) VALUES (0, 0, " & nGroup & ")")
	'		dbPages.ExecSql("UPDATE tblGlobalData SET GD_Text='" & sText & "' WHERE GD_Lng=1 AND GD_Group=0 AND GD_ID=" & nGroup)
	'		dbPages.ExecSql("UPDATE tblGlobalData SET GD_Text='" & sTextH & "' WHERE GD_Lng=0 AND GD_Group=0 AND GD_ID=" & nGroup)
	'		litGroup.Text = txtGroup.Text
	'	End If
	'End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Global Data</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<form id="frmServer" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Global Data<span> - <asp:Literal ID="litGroup" runat="server" /></span></h1>
		<%--<div class="bordered">
			English
			<asp:TextBox ID="txtGroup" CssClass="text" runat="server" />
			Hebrew
			<asp:TextBox ID="txtGroupH" CssClass="text" runat="server" />
			<asp:Button ID="btnSaveGroup" CssClass="buttonWhite" runat="server" Text="Save" OnClick="SaveGroup" UseSubmitBehavior="True" />
			<asp:Label ID="lblSaveGroup" runat="server" ForeColor="red" />
		</div>--%>
		<div class="bordered">
			English
			<asp:TextBox ID="txtNewRecord" CssClass="text" runat="server" /> &nbsp;
			Hebrew
			<asp:TextBox ID="txtNewRecordH" CssClass="text RTL" runat="server" /> &nbsp;
			<asp:Button ID="btnAddRecord" CssClass="buttonWhite" runat="server" Text="Add" OnClick="AddRecord" UseSubmitBehavior="False" /> &nbsp;
			<asp:Label ID="lblAddRecord" runat="server" ForeColor="red" />
		</div>
		<asp:GridView ID="gvRecords" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="dsRecords" AllowPaging="False"
			AllowSorting="False" SelectedRowStyle-CssClass="rowSelected" Width="100%" OnRowDeleted="ReloadPage">
			<Columns>
				<asp:BoundField DataField="ID" HeaderText="ID" HeaderStyle-HorizontalAlign="left" ItemStyle-CssClass="wide" ReadOnly="True" />
				<asp:BoundField DataField="TextE" HeaderText="English Value" HeaderStyle-HorizontalAlign="left" ControlStyle-CssClass="wide" />
				<asp:BoundField DataField="TextH" HeaderText="Hebrew Value" ItemStyle-CssClass="RTL" HeaderStyle-HorizontalAlign="Right" ControlStyle-CssClass="wide" />
				<asp:CommandField HeaderText="Actions" ButtonType="Button" ItemStyle-CssClass="centered buttons" ControlStyle-CssClass="buttonWhite"
					ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="False" HeaderStyle-CssClass="actions" UpdateText="Save"
				/>
			</Columns>
		</asp:GridView>
		<asp:SqlDataSource ID="dsRecords" runat="server" ProviderName="System.Data.SqlClient"
			SelectCommand="SELECT e.GD_ID ID, e.GD_Text TextE, h.GD_Text TextH FROM GetGlobalData(@Group) e FULL JOIN GetGlobalDataLang(@Group, 0) h on e.GD_ID=h.GD_ID ORDER BY e.GD_ID"
			UpdateCommand="UPDATE tblGlobalData SET GD_Text=CASE GD_Lng WHEN 0 THEN IsNull(@TextH, '') ELSE IsNull(@TextE, '') END WHERE GD_ID=@ID AND GD_Group=@Group"
			DeleteCommand="DELETE FROM tblGlobalData WHERE GD_ID=@ID AND GD_Group=@Group">
			<SelectParameters>
				<asp:QueryStringParameter QueryStringField="Group" Name="Group" ConvertEmptyStringToNull="true" Type="Int32" />
			</SelectParameters>
			<DeleteParameters>
				<asp:QueryStringParameter QueryStringField="Group" Name="Group" ConvertEmptyStringToNull="true" Type="Int32" />
				<asp:Parameter Name="ID" Type="Int32" />
			</DeleteParameters>
			<UpdateParameters>
				<asp:QueryStringParameter QueryStringField="Group" Name="Group" ConvertEmptyStringToNull="true" Type="Int32" />
				<asp:Parameter Name="Text" Type="String" />
				<asp:Parameter Name="TextE" Type="String" />
				<asp:Parameter Name="ID" Type="Int32" />
			</UpdateParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>