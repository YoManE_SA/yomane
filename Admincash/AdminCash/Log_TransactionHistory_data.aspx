<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script runat="server">
		Dim sQueryString, sDivider As String
		Const RGB_PASS As String = "#66CC66", RGB_FAIL As String = "#ff6666"
		
		Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
			Security.CheckPermission(lblPermissions)
			If Trim(Request("iPageSize")) <> "" Then PGData.PageSize = Request("iPageSize") Else PGData.PageSize = 25
			
			Dim sWhere As String = ""
			If Request("ID") <> "" Then
				sWhere &= " And TransHistory.TransHistory_id=" & dbPages.TestVar(Request("ID"), 0, -1, 0)
			ElseIf Request("TransID") <> "" Then
				sWhere &= " And "
	            sWhere &= "(TransPass_id=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & _
	             " Or TransPreAuth_id=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & _
	             " Or TransPending_id=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & _
	             " Or TransFail_id=" & dbPages.TestVar(Request("TransID"), 0, -1, 0) & ")"
			Else
	            If Request("fromDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And TransHistory.InsertDate >= '" & Request("fromDate") & " " & Request("fromTime") & "'"
	            If Request("toDate") <> "" And IsDate(Request("fromDate")) Then sWhere &= " And TransHistory.InsertDate <= '" & Request("toDate") & " " & Request("toTime") & "'"
	            If Request("ShowCompanyID") <> "" Then sWhere &= " And Merchant_id = " & Request("ShowCompanyID")
	            If Request("ddlEventType") <> "" Then sWhere &= " And TransHistory.TransHistoryType_id = " & Request("ddlEventType")
			End If
			If Security.IsLimitedMerchant Then sWhere &= " AND Merchant_id IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
			If sWhere.Length > 0 Then sWhere = " Where " & sWhere.Substring(5)
			dbPages.CurrentDSN = IIf(Request("SQL2") = "1", 2, 1)
	        PGData.OpenDataset("Select TransHistory.*, TransHistoryType.Name as EvName, tblCompany.CompanyName From Trans.TransHistory " & _
	        "Left Join List.TransHistoryType ON(TransHistoryType.TransHistoryType_id = TransHistory.TransHistoryType_id) " & _
	        "Left Join tblCompany ON(tblCompany.ID = TransHistory.Merchant_id) " & _
	        sWhere & " Order By TransHistory.TransHistory_id Desc")
			sQueryString = dbPages.CleanUrl(Request.QueryString)
		End Sub
	</script>
</head>
<body>
	<form id="Form2" runat="server">
		<table align="center" style="width:92%">
		<tr>
			<td id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> TRANSACTION HISTORY
				<span style="color:Black;font-weight:normal;"><%=IIf(Request("From") = "Top", " - Last Actions", String.Empty)%></span>
				<br /><br />
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%" cellpadding="1" cellspacing="0">
		<tr>
			<th>ID</th>
			<th>Date</th>
			<th>Trans ID</th>
			<th>Trans Source</th>
			<th>Merchant</th>
			<th>Type</th>
			<th>Succeeded</th>
			<th>Reference</th>
			<th>Description</th>
		</tr>
		<%
		While PGData.Read()
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';" height="16">
				<td><%= PGData("TransHistory_id")%></td>
				<td><%= PGData("InsertDate")%></td>
				<%	
				    If PGData("TransPass_id") IsNot DBNull.Value Then
				        Response.Write("<td>" & PGData("TransPass_id") & "</td><td>Pass</td>")
				    ElseIf PGData("TransPreAuth_id") IsNot DBNull.Value Then
				        Response.Write("<td>" & PGData("TransPreAuth_id") & "</td><td>Approval</td>")
				    ElseIf PGData("TransPending_id") IsNot DBNull.Value Then
				        Response.Write("<td>" & PGData("TransPending_id") & "</td><td>Pending</td>")
				    ElseIf PGData("TransFail_id") IsNot DBNull.Value Then
				        Response.Write("<td>" & PGData("TransFail_id") & "</td><td>Fail</td>")
				    End If
				%>
				<td><%= PGData("CompanyName")%></td>
				<td><%= PGData("EvName")%></td>
				<td><%= PGData("IsSucceeded")%></td>
				<td><%= PGData("ReferenceNumber")%></td>
				<td><%= PGData("Description")%></td>
			</tr>
			<tr><td height="1" colspan="10"  bgcolor="#ffffff"></td></tr>
			<tr><td height="1" colspan="10"  bgcolor="silver"></td></tr>
			<tr><td height="1" colspan="10"  bgcolor="#ffffff"></td></tr>
			<%
		End While
		PGData.CloseDataset()%>
		</table>
	</form>
	<br />
	<table align="center" style="width:92%">
	 <tr>
	  <td><UC:Paging runat="Server" id="PGData" PageID="PageID"  /></td>
	 </tr>
	</table>
</body>
</html>