<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<script runat="server">
	Sub Page_Load()
		Security.CheckPermission(lblPermissions)

		Dim sSQL As String, drData As SqlDataReader
		lblIssuer.Text &= "<select name=""BillingCompanys_id"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT BillingCompanys_id, Name AS BillingCompanys_name FROM tblBillingCompanys ORDER BY BillingCompanys_name DESC"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblIssuer.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		lblIssuer.Text &= "</select>"

		lblInvoiceType.Text &= "<select name=""invoiceType"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT GD_ID, GD_Text FROM GetGlobalData(" & GlobalDataGroup.DOCUMENT_TYPE & ")"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblInvoiceType.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		lblInvoiceType.Text &= "</select>"

		lblPSP.Text &= "<select name=""PSP"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT GD_ID, GD_Text FROM GetGlobalData(" & GlobalDataGroup.PSP & ")"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblPSP.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		lblPSP.Text &= "</select>"

		lblManagingCompany.Text &= "<select name=""ManagingCompany"" class=""grayBG""><option class=""grayBG"" value=""""></option>"
		sSQL = "SELECT GD_ID, GD_Text FROM GetGlobalData(" & GlobalDataGroup.MANAGING_COMPANY & ")"
		drData = dbPages.ExecReader(sSQL)
		Do While drData.Read
			lblManagingCompany.Text &= "<option class=""grayBG"" value=""" & drData(0) & """>" & drData(1) & "</option>"
		Loop
		lblManagingCompany.Text &= "</select>"

		fdtrControl.FromDateTime = Date.Now.AddMonths(-1).AddDays(1).ToShortDateString
		fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
		drData.Close()
	End Sub
</script>
<asp:Content runat="server" ContentPlaceHolderID="body">
    <script language="javascript" type="text/javascript">
    	function ExpandTable() {
    		var oImage = event.srcElement;
    		var sSrc = oImage.src;
    		oImage.src = (sSrc.indexOf("expand") > 0 ? sSrc.replace("expand", "collapse") : sSrc.replace("collapse", "expand"));
    		for (var oTable = oImage.parentNode; oTable.tagName != "TABLE"; oTable = oTable.parentNode);
    		for (oTable = oTable.nextSibling; oTable.tagName != "TABLE"; oTable = oTable.nextSibling);
    		oTable.style.display = (oTable.style.display == "none" ? "" : "none");
    	}
	</script>
	<form runat="server" action="invoices.aspx" method="get" target="frmBody" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />
	<h1><asp:label ID="lblPermissions" runat="server" /> Invoices<span> - Filter</span></h1>

	<table class="filterNormal">
		<tr><th colspan="2" class="filter indent">Manual Issuance</th></tr>
		<tr><td>For <b>manual issuance</b> <a target="frmBody" href="invoiceCreate.aspx" onclick="parent.fraButton.FrmResize();">click here</a></td></tr>
	</table>
    <br />
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<th colspan="2" class="filter indent">
				<img onclick="ExpandTable();" style="cursor:pointer;vertical-align:middle;" src="../images/tree_expand.gif" />
				Invoice To Managing PSP
			</th>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;display:none;">
		<tr>
			<td colspan="2">
				<Uc1:FilterDateTimeRange Title="" ID="fdtrPSP"  runat="server" />
			</td>
		</tr>
		<tr>
			<td>PSP</td>
			<td><asp:Label ID="lblPSP" runat="server" /></td>
		</tr>
		<tr>
			<td></td>
			<td style="text-align:right;">
				<script language="javascript" type="text/javascript">
					function IssueToPSP()
					{
						if (event.srcElement.form.PSP.selectedIndex == 0)
						{
							alert("PSP not selected!");
							event.srcElement.form.PSP.focus();
							return false;
						}
						event.srcElement.form.ManagingCompany.selectedIndex = 0;
						ConvertFormToClient(event.srcElement.form, 'invoiceCreate.aspx');
						event.srcElement.form.submit();
					}
				</script>
				<input type="button" class="buttonFilter" value="Issue" onclick="IssueToPSP();" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<th colspan="2" class="filter indent">
				<img onclick="ExpandTable();" style="cursor:pointer;vertical-align:middle;" src="../images/tree_expand.gif" />
				Invoice To Managing Company
			</th>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;display:none;">
		<tr>
			<td colspan="2">
				<Uc1:FilterDateTimeRange Title="" ID="fdtrManagingCompany"  runat="server" />
			</td>
		</tr>
		<tr>
			<td>Comp.</td>
			<td><asp:Label ID="lblManagingCompany" runat="server" /></td>
		</tr>
		<tr>
			<td></td>
			<td style="text-align:right;">
				<script language="javascript" type="text/javascript">
					function IssueToManagingCompany()
					{
						if (event.srcElement.form.ManagingCompany.selectedIndex == 0)
						{
							alert("Managing Company not selected!");
							event.srcElement.form.ManagingCompany.focus();
							return false;
						}
						event.srcElement.form.PSP.selectedIndex = 0;
						ConvertFormToClient(event.srcElement.form, 'invoiceCreate.aspx');
						event.srcElement.form.submit();
					}
				</script>
				<input type="button" class="buttonFilter" value="Issue" onclick="IssueToManagingCompany();" />
			</td>
		</tr>
	</table>
	<hr class="filter" />
	<table class="filterNormal" style="padding-top:5px;display:none;">
		<tr>
			<td>
				<Uc1:FilterMerchants id="FMerchants" ClientField="ShowCompanyID" SqlString="SELECT GD_Color StatusColor, CompanyName, ID CompanyID FROM tblCompany
			  INNER JOIN tblGlobalData ON ActiveStatus=GD_ID WHERE GD_Group=44 AND GD_Lng=1" runat="server" />
			</td>
		</tr>
	</table>
	<input type="hidden" name="from" value="menu" />
	<input type="hidden" name="searchType" value="1" />
	<input type="hidden" name="ShowCompanyID" id="ShowCompanyID" />

	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2" class="filter indent">Invoice Details</th></tr>
		<tr>
			<td colspan="2">
				<Uc1:FilterDateTimeRange Title="" ID="fdtrControl"  runat="server" />
			</td>
		</tr>
		<tr>
			<td>Document type</td>
			<td><asp:Label ID="lblInvoiceType" runat="server" /></td>
		</tr>
		<tr>
			<td>Issuance</td>
			<td>
				<select name="payType" class="grayBG">
					<option value="">
					<option value="0">Required
					<option value="1">Manual
				</select>
			</td>
		</tr>
		<tr><td height="4"></td></tr>
		<tr><th colspan="4" class="filter">Invoice Issuer</th></tr>
		<tr><td colspan="4"><asp:Label ID="lblIssuer" runat="server" /></td></tr>
		<tr>
			<td class="indent">
				<select name="PageSize" class="grayBG">
					<option value="10">10 rows/page</option>
					<option value="25">25 rows/page</option>
					<option value="50">50 rows/page</option>
					<option value="100" selected="selected">100 rows/page</option>
				</select>
			</td>
			<td class="indent" align="right" nowrap="nowrap">
				<input type="submit" class="buttonFilter" value="Search" />
				<input type="reset" class="buttonFilter buttonDelete" value="Reset" />
			</td>
		</tr>
		</table>
	</form>
	<hr class="filter" />
	<form id="frmFilter2" action="invoices.aspx" method="post" target="frmBody" onsubmit="parent.fraButton.FrmResize();">
	<input type="hidden" name="from" value="menu" />
	<input type="hidden" name="searchType" value="2" />
	<table class="filterNormal">
		<tr><th colspan="4" class="filter indent">Invoice No.</th></tr>
		<tr>
			<td>From</td>
			<td><input name="billNumFrom" class="GrayBG" style="width:50px;" /></td>
			<td>to</td>
			<td><input name="billNumTo" class="GrayBG" style="width:50px;" /></td>
		</tr>
		<tr>
			<td class="indent" colspan="2">
				<select name="PageSize" class="grayBG">
					<option value="10">10 rows/page</option>
					<option value="25">25 rows/page</option>
					<option value="50">50 rows/page</option>
					<option value="100" selected="selected">100 rows/page</option>
				</select>
			</td>
			<td class="indent" colspan="2" align="right" nowrap="nowrap">
				<input type="submit" class="buttonFilter" value="  Show  " />
				<input type="reset" class="buttonFilter buttonDelete" value="Reset" />
			</td>
		</tr>
		</table>
	</form>
</asp:Content>