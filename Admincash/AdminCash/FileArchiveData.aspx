<%@ Page Language="VB" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Implements Interface="System.Web.UI.IPostBackEventHandler" %>
<script runat="server">
	Dim sSQL As String = String.Empty, sQueryWhere As String = String.Empty, sQueryString As String = String.Empty, sDivider As String = String.Empty
	Dim sURL As String = String.Empty, sQueryTop As String = String.Empty, sPath, sTitle As String, i As Integer
	
	Function GetRequestDateTimeString(ByVal sDateField As String, Optional ByVal sTimeField As String = Nothing) As String
		If String.IsNullOrEmpty(Request(sDateField)) Then Return String.Empty
		If String.IsNullOrEmpty(sTimeField) Then Return Request(sDateField)
		If String.IsNullOrEmpty(Request(sTimeField)) Then Return Request(sDateField)
		Return (Request(sDateField).ToString & " " & Request(sTimeField).ToString).Trim
	End Function

	Function DisplayContent(ByVal sVar As String) As String
		DisplayContent = "<span class=""key"">" & replace(replace(dbPages.dbtextShow(sVar), "=", "</span> = "), "|", " <br /><span class=""key"">")
	End Function

    Sub UploadNew(ByVal o As Object, ByVal e As EventArgs)
        If fuFile.HasFile And fuFile.PostedFile.ContentLength > 0 And Not String.IsNullOrEmpty(txtTitle.Text) Then
            If System.Math.Round(fuFile.PostedFile.ContentLength / 1048576) > 5 Then 'check for files larger than 3mb
                ltError.Text = "<span style='color:red'>File is too large. Must be less than 5MB.</span>"
                ltError.Visible = True
                Exit Sub
            End If
            Dim merchantId = dbPages.TestVar(Request("MerchantID"), 1, 0, 0)
            Dim merchant = Netpay.Bll.Merchants.Merchant.Load(merchantId)
            Dim fvo As New Netpay.Bll.Accounts.File(merchant.AccountID)
            fvo.FileTitle = txtTitle.Text
            fvo.Save(fuFile.FileName, fuFile.FileBytes)
            Response.Redirect(IIf(Len("" & Request.QueryString.ToString()) > 0, "FileArchiveData.aspx?" & Request.QueryString.ToString(), "FileArchive.aspx"), True)
        End If
    End Sub
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		
		If Session("FileTooLarge") then
			ltError.Text="<span style='color:red'>File is too large. Must be less than 3MB.</span>"
			ltError.Visible=True
			Session("FileTooLarge") = False
		End If
		
		If Trim(Request("From")) = "top" Then
			lblTopHeading.Text = "- New Uploads"
			sQueryTop = "TOP 25"
		End if
		
		Dim sbWhere As New StringBuilder
		Dim sDate As String = GetRequestDateTimeString("FromDate", "FromTime")
        If Not String.IsNullOrEmpty(sDate) Then sbWhere.Append(" AND af.InsertDate>='" & sDate & "'")
		sDate = GetRequestDateTimeString("ToDate", "ToTime")
        If Not String.IsNullOrEmpty(sDate) Then sbWhere.Append(" AND af.InsertDate<='" & sDate & "'")
		Dim sExt As String = IIf(String.IsNullOrEmpty(Request("Ext")), IIf(String.IsNullOrEmpty(Request("FileExt")), String.Empty, Request("FileExt")), Request("Ext"))
        If Not String.IsNullOrEmpty(sExt) Then sbWhere.Append(" AND af.FileExt IN ('" & dbPages.DBText(sExt).Replace("(none)", String.Empty).Replace(",", "','") & "')")
        If dbPages.TestVar(Request("MerchantID"), 1, 0, 0) > 0 Then sbWhere.Append(" AND a.Merchant_ID=" & Request("MerchantID"))
		If Not String.IsNullOrEmpty(Request("Title")) Then
            sbWhere.Append(" AND af.FileTitle='" & dbPages.DBText(Request("Title")) & "'")
		ElseIf Not String.IsNullOrEmpty(Request("SearchText")) Then
			sbWhere.Append(" AND af.FileTitle LIKE '%" & dbPages.DBText(Request("SearchText")) & "%'")
		End If
		If sbWhere.Length > 0 Then sQueryWhere = sbWhere.Remove(1, 3).Insert(1, "WHERE").ToString
		
		sQueryString = dbPages.CleanUrl(Request.QueryString)

		PGData.PageSize = IIf(dbPages.TestVar(Request("PageSize"), 1, 0, 0) > 0, Request("PageSize"), 20)

		pnlNew.Visible = False
		Dim nMerchant As Integer = dbPages.TestVar(Request("MerchantID"), 1, 0, 0)
		If nMerchant > 0 Then
			Dim sMerchant As String = dbPages.ExecScalar("SELECT CompanyName FROM tblCompany WHERE ID=" & nMerchant)
			If Not String.IsNullOrEmpty(sMerchant) Then
				pnlNew.Visible = True
				lblTopHeading.Text = "- " & sMerchant
			End If
		End If
    End Sub
    
    Public Shadows Sub RaisePostBackEvent(eventArgument As String) Implements IPostBackEventHandler.RaisePostBackEvent
        Dim values = eventArgument.Split("$")
        If (values.Length = 2 And values(0) = "Download") Then
            WebUtils.SendFile(Domain.Current.MapPrivateDataPath(System.IO.Path.Combine("Account Files/", values(1))), Nothing, "attachment")
        End If
    End Sub

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>File Archive</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript" type="text/javascript">
		function IconVisibility(fVar) {
		
			spanObj = document.getElementById(fVar)
			if (spanObj.style.visibility == 'hidden') {
				spanObj.style.visibility = 'visible';
			}
			else {
				spanObj.style.visibility = 'hidden';
			}
		}
		function ExpandNode(fTrNum) {
			trObj = document.getElementById("trInfo" + fTrNum)
			imgObj = document.getElementById("oListImg" + fTrNum)
			if (trObj)
			{
				if (trObj.style.display == '')
				{
					imgObj.src = '../images/tree_expand.gif';	
					trObj.style.display = 'none';
				}
				else
				{
					imgObj.src = '../images/tree_collapse.gif';	
					trObj.style.display = '';
					var ifrDetails=document.getElementById("ifr" + fTrNum);
					if (ifrDetails.contentWindow.location.href.indexOf(".htm")>0) ifrDetails.contentWindow.location.replace("FileArchiveDetails.aspx?ID="+fTrNum);
				}
			}
		}
		function checkValidFile(filename) {
			if (filename.length == 0) return false;
			var dot = filename.lastIndexOf(".");
			var extension = filename.substr(dot, filename.length).toLowerCase();			
			if (extension == ".exe" || extension == ".com" || extension == ".bat" || extension == ".js") {
				alert("This file type is not allowed!");
				return false;
			}
			return true;
		}
	</script>
</head>
<body>
	<form id="Form1" runat="server">
		<table align="center" style="width:92%" border="0">
		<tr>
			<td id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> FILE ARCHIVE
				<asp:label ID="lblTopHeading" runat="server" ForeColor="Black" />
				<br /><br />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<asp:Panel ID="pnlNew" runat="server">
					<fieldset>
						<legend>UPLOAD NEW FILE</legend>
						<table cellpadding="0" cellspacing="0" border="0" class="formNormal" style="margin-left:20px;">
							<tr>
								<td>File</td>
								<td><asp:FileUpload ID="fuFile" Width="200px" runat="server" /></td>
								<td>&nbsp;</td>
								<td>Description</td>
								<td><asp:TextBox ID="txtTitle" Width="200px" runat="server" MaxLength="1800" /></td>
								<td>&nbsp;</td>
								<td><asp:Button Text="  UPLOAD  " OnClick="UploadNew" UseSubmitBehavior="true" runat="server" OnClientClick="return checkValidFile(document.getElementById('fuFile').value);" /></td>
								<td><asp:Literal ID = "ltError" runat="server" Visible="false" /></td>
							</tr>
						</table>
					</fieldset>
					<br /><br />
				</asp:Panel>
			</td>
		</tr>
		<tr>
			<td style="font-size:11px; padding-bottom:4px;">
				(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');">Collapse All</a>)
			</td>
			<td align="right" style="font-size:11px;">
				Filters:
				<%
				If Trim(Request("MerchantID")) <> "" Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "MerchantID", Nothing) & "';"">Merchant</a>")
					sDivider = ", "
				End If
				If Not String.IsNullOrEmpty(Request("Ext")) Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "Ext", Nothing) & "';"">File Type</a>")
					sDivider = ", "
				End If
				If Trim(Request("Title")) <> "" Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "Title", Nothing) & "';"">Description</a>")
					sDivider = ", "
				End If
				Response.Write(IIf(sDivider = String.Empty, "Add to filter by clicking on returned fields", " | (click to remove)"))
				%>
			</td>
		</tr>
		</table>
		<script language="javascript" type="text/javascript">
			var nIDs=0, naIDs=new Array;
		</script>
		<table class="formNormal" align="center" style="width:92%">
		<tr>
			<th style="width:30px;"><br /></th>
			<th>ID</th>
			<th>Date &amp; Time</th>
			<th>Merchant</th>
			<th>Description</th>
			<th>File</th>
			<th>File Type</th>
		</tr>
		<%
		    
		    sSQL = "Select af.*, a.Name AccountName, a.Merchant_id, a.Customer_id From Data.AccountFile af Left Join Data.Account a ON(a.Account_ID = af.Account_ID) " & sQueryWhere & " Order By af.InsertDate"
		    'Response.Write(sSQL)
		'Response.End
		PGData.OpenDataset(sSQL)
		Dim sExt As String = String.Empty, sIcon, sFile, sPath As String
		While PGData.Read()
		        i = PGData("AccountFile_id")
		        sFile = PGData("FileName")
		        sPath = Netpay.Bll.Accounts.Account.MapPrivatePath(PGData("Account_ID"), "Files/" & PGData("FileName"))
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
				<td style="text-align:right;"><img onclick="ExpandNode('<%=i%>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle" /><br /></td>
				<td nowrap="nowrap"><%=PGData("AccountFile_id")%></td>
				<td nowrap="nowrap"><%If PGData("InsertDate") IsNot DBNull.Value Then Response.Write(dbPages.FormatDateTime(PGData("InsertDate")))%></td>
				<td>
					<%
					    dbPages.showFilter(IIf(String.IsNullOrEmpty(PGData("AccountName")), "----------", PGData("AccountName")), "?" & dbPages.SetUrlValue(sQueryString, "MerchantID", PGData("Merchant_ID")), 1)
					%>
				</td>
				<td style="white-space:nowrap;">
					<%
					sTitle = IIf(String.IsNullOrEmpty(PGData("FileTitle")), "----------", PGData("FileTitle"))
					If sTitle.Length > 30 Then sTitle = sTitle.Substring(0, 28) & "..."
					dbPages.showFilter(sTitle, "?" & dbPages.SetUrlValue(sQueryString, "FileTitle", PGData("FileTitle")), 1)
					%>
				</td>
				<td>
					<%
					    If sFile.Length > 30 Then sFile = sFile.Substring(0, 14) & "..." & sFile.Substring(sFile.Length - 14)
					%>
					<a href="<%= Page.ClientScript.GetPostBackClientHyperlink(Me, "Download$" & PGData("Account_ID") & "/Files/" & PGData("FileName")) %>" title="<%= PGData("FileName") %>"><%=sFile%></a>
					<%
					    If My.Computer.FileSystem.FileExists(sPath) Then Response.Write(" (" & System.Math.Round(My.Computer.FileSystem.GetFileInfo(sPath).Length / 1024) & "K)")
					%>
				</td>
				<td style="white-space:nowrap;">
					<span style="width:20px;">
						<%
						    If Not String.IsNullOrEmpty(PGData("FileExt") + "") Then
						        sExt = PGData("FileExt").ToString.ToUpper
						        sIcon = "/NPCommon/ImgFileExt/" & (sExt & "   ").Substring(0, 3) & ".gif"
						        If My.Computer.FileSystem.FileExists(Server.MapPath(sIcon)) Then Response.Write("<img src=""" & sIcon & """ align=""middle"" style=""border:0;margin:0;padding:0;"" />")
						    End If
						%>
					</span>
					<%
					dbPages.showFilter(sExt, "?" & dbPages.SetUrlValue(sQueryString, "Ext", sExt), 1)
					%>
				</td>
			</tr>
			<tr id="trInfo<%=i%>" style="display:none;">
				<td colspan="7" style="border-top:1px dashed silver;">
					<script language="javascript" type="text/javascript">
						naIDs[nIDs]=<%=i%>;
						nIDs++;
					</script>
					<iframe frameborder="0" src="common_blank.htm" id="ifr<%=i%>" width="100%" height="0" onload="this.height=this.contentWindow.document.body.scrollHeight;"></iframe>
				</td>
			</tr>
			<tr><td height="1" colspan="8"  bgcolor="silver"></td></tr>
			<%
		End While
		PGData.CloseDataset()
		%>
		</table>
		<script language="JavaScript" type="text/javascript">
			function ExpandAll(fDisplay, fImgStatus) {
				for(i = 0; i < nIDs; i++) {
					trObj = document.getElementById("trInfo" + naIDs[i])
					imgObj = document.getElementById("oListImg" + naIDs[i])
					if (trObj) {
						imgObj.src = '../images/tree_' + fImgStatus + '.gif';
						trObj.style.display = fDisplay;
						ifrDetails=document.getElementById("ifr" + naIDs[i]);
						if (ifrDetails.contentWindow.location.href.indexOf(".htm")>0) ifrDetails.contentWindow.location.replace("FileArchiveDetails.aspx?ID="+naIDs[i]);
					}
				}
			}
		</script>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" align="middle" alt="" /> ADD TO FILTER
		</div>
	</form>
	
	<table align="center" style="width:92%">
	<tr>
		<td>
			<UC:Paging runat="Server" id="PGData" PageID="PageID" />
		</td>
	</tr>
	</table>
		
</body>
</html>
