﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.Bll;

public partial class AdminCash_merchant_RegistrationData : System.Web.UI.Page
{
	public Netpay.Bll.Merchants.Registration Item;
    protected void Page_Load(object sender, EventArgs e)
    {
		Item = Netpay.Bll.Merchants.Registration.Load(Request["ID"].ToGuid());
		if (!IsPostBack && Item != null)
		{
			ddlIndustry.DataSource = Netpay.Bll.ThirdParty.CardConnect.GetIndustries();
			ddlTypeOfBusiness.DataSource = Netpay.Bll.ThirdParty.CardConnect.GetBusinessTypes();
			RefreshData(false);
		}
    }

	protected void RefreshData(bool reload)
	{
		if (reload) Item = Netpay.Bll.Merchants.Registration.Load(Request["ID"].ToGuid());
		DataBind();
		ddlState.ISOCode = Item.State;
		ddlPhisicalState.ISOCode = Item.PhisicalState;
		ddlStateOfIncorporation.ISOCode = Item.StateOfIncorporation;
		if (Item.Industry != null) ddlIndustry.SelectedValue = Item.Industry.Value.ToString();
		if (Item.TypeOfBusiness != null) ddlTypeOfBusiness.SelectedValue = Item.TypeOfBusiness.Value.ToString();
	}

	protected void Save_Command(object sender, CommandEventArgs e)
	{
		//Item = Netpay.Bll.Merchants.Registration.Load(Request["ID"].ToGuid());
		Item.DbaName = txtDbaName.Text;
		Item.LegalBusinessName = txtLegalBusinessName.Text;
		Item.LegalBusinessNumber = txtLegalBusinessNumber.Text;
		Item.FirstName = txtFirstName.Text;
		Item.LastName = txtLastName.Text;
		Item.Email = txtEmail.Text;
		Item.Address = txtAddress.Text;
		Item.City = txtCity.Text;
		Item.State = ddlState.ISOCode;
		Item.Zipcode = txtZipcode.Text;
		Item.OwnerDob = txtOwnerDob.Date;
		Item.OwnerSsn = txtOwnerSsn.Text;
		Item.Phone = txtPhone.Text;
		Item.Fax = txtFax.Text;
		Item.Url = txtUrl.Text;
		Item.PhisicalAddress = txtPhisicalAddress.Text;
		Item.PhisicalCity = txtPhisicalCity.Text;
		Item.PhisicalState = ddlPhisicalState.ISOCode;
		Item.PhisicalZip = txtPhisicalZip.Text;
		Item.StateOfIncorporation = ddlStateOfIncorporation.ISOCode;
		Item.BusinessStartDate = txtBusinessStartDate.Date;
		Item.TypeOfBusiness = ddlTypeOfBusiness.SelectedValue.ToNullableInt();
		Item.Industry = ddlIndustry.SelectedValue.ToNullableInt();
		Item.BusinessDescription = txtBusinessDescription.Text;
		Item.AnticipatedMonthlyVolume = txtAnticipatedMonthlyVolume.Text.ToNullableDecimal();
		Item.AnticipatedAverageTransactionAmount = txtAnticipatedAverageTransactionAmount.Text.ToNullableDecimal();
		Item.AnticipatedLargestTransactionAmount = txtAnticipatedLargestTransactionAmount.Text.ToNullableDecimal();
		Item.BankRoutingNumber = txtBankRoutingNumber.Text;
		Item.BankAccountNumber = txtBankAccountNumber.Text;
		Item.PercentDelivery0to7 = txtPercentDelivery0to7.Text.ToInt32(0);
		Item.PercentDelivery15to30 = txtPercentDelivery15to30.Text.ToInt32(0);
		Item.PercentDelivery8to14 = txtPercentDelivery8to14.Text.ToInt32(0);
		Item.PercentDeliveryOver30 = txtPercentDeliveryOver30.Text.ToInt32(0);
		
		Item.Save();
	}

	protected void RegisterLocally_Command(object sender, CommandEventArgs e)
	{
		Save_Command(sender, e);
		Item.CreateMerchantLocally();
		RefreshData(true);
	}

	protected void RegisterAtCardConnect_Command(object sender, CommandEventArgs e)
	{
		Save_Command(sender, e);
		divResultMessage.InnerHtml = "";
		string regResultMessage = null;
		bool result = Netpay.Bll.ThirdParty.CardConnect.CreateMerchant(Item.ID, out regResultMessage);

		string attachmentResultMessage = null;
		if (result)
		{
			result = Netpay.Bll.ThirdParty.CardConnect.SendAttachment(Item.ID, out attachmentResultMessage);
		}
		divResultMessage.InnerHtml = regResultMessage + "<br>" + attachmentResultMessage;
		RefreshData(true);
	}

	protected void RegisterAtZoho_Command(object sender, CommandEventArgs e)
	{
		Save_Command(sender, e);
		divResultMessage.InnerHtml = "";
		string regResultMessage = null;
		Netpay.Bll.ThirdParty.ZohoCRM.CreateLead(Item.ID, out regResultMessage);
		divResultMessage.InnerHtml = regResultMessage;
		RefreshData(true);
	}

	protected void Delete_Command(object sender, CommandEventArgs e)
	{
		Item.Delete();
	}
}