﻿<%@ Page Language="VB" %>
<%@ Register Src="~/AdminCash/TabMenu.ascx" TagPrefix="UC1" TagName="TabMenu" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
	Private Enum CCR_Types
		CCRT_TerminalGroup	= 0
		CCRT_Terminal		= 1
		CCRT_Country		= 2
		CCRT_Bin			= 3
		CCRT_BlackList		= 4
		CCRT_WhiteList		= 5
		CCRT_CPassMore		= 6
		CCRT_CPassLess		= 7
		CCRT_SelectGroup	= 8
	End Enum
	Dim strPaymentMethod() As String, strCurrencies() As String
	Dim CompanyID As Integer = 0, CPM_ID As Integer = 0
	Dim DoAction As String = "", sFirstLoad As String = ""
	
	Sub RecalcCompanyFeesEx(nCompanyID As Integer, nPayID as Integer)
		Dim iReader As SqlDataReader = dbPages.ExecReader("Select * From tblCompanyPaymentMethods Where CPM_CompanyID=" & nCompanyID)
		While iReader.Read()
			Dim sQuery As String = "Update tblCompanyTransPass Set "
            Dim sFrom As String = " From tblCompanyTransPass LEFT JOIN tblCreditCard ON((tblCompanyTransPass.PaymentMethod_id=1) And (tblCreditCard.ID=tblCompanyTransPass.CreditCardID)) " & _
                    " Left Join tblCheckDetails ON tblCheckDetails.ID = tblCompanyTransPass.CheckDetailsID"
			Dim sDefWhere As String = sFrom & " Where tblCompanyTransPass.CompanyID=" & nCompanyID & " And (tblCompanyTransPass.PaymentMethod=" & iReader("CPM_PaymentMethod") & ")"
			
			dbPages.ExecSql(sQuery & "netpayFee_chbCharge=" & iReader("CPM_CBFixedFee") & sDefWhere & " And OCurrency=" & iReader("CPM_CurrencyID") & " And PrimaryPayedID=" & nPayID & " And DeniedStatus IN(1, 2, 4, 6)")
			dbPages.ExecSql(sQuery & "netpayFee_ClrfCharge=" & iReader("CPM_ClarificationFee") & sDefWhere & " And OCurrency=" & iReader("CPM_CurrencyID") & " And PrimaryPayedID=" & nPayID & " And DeniedStatus IN(1, 2, 3, 4, 5, 6, 9)")
			dbPages.ExecSql(sQuery & "netpayFee_transactionCharge=" & iReader("CPM_FixedFee") & ", netpayFee_ratioCharge= (Amount * " & (iReader("CPM_PercentFee") / 100) & ")" & sDefWhere & " And OCurrency=" & iReader("CPM_CurrencyID") & " And PrimaryPayedID=" & nPayID & " And DeniedStatus Not IN(6, 7)")
			dbPages.ExecSql(sQuery & "netpayFee_transactionCharge=" & iReader("CPM_RefundFixedFee") & ", netpayFee_ratioCharge=0" & sDefWhere & " And OCurrency=" & iReader("CPM_CurrencyID") & " And PrimaryPayedID=" & nPayID & " And DeniedStatus=0 And CreditType=0")

			sQuery = "Update tblCompanyTransApproval Set "
			sDefWhere = Replace(sDefWhere, "tblCompanyTransPass", "tblCompanyTransApproval") & " And Currency=" & iReader("CPM_CurrencyID") & " And PayID=" & nPayID
			dbPages.ExecSql(sQuery & "netpayFee_transactionCharge=" & iReader("CPM_ApproveFixedFee") & sDefWhere)

			sQuery = "Update tblCompanyTransFail Set "
			sDefWhere = Replace(sDefWhere, "tblCompanyTransApproval", "tblCompanyTransFail") & " And DebitCompanyID > 1"
			dbPages.ExecSql(sQuery & "netpayFee_transactionCharge=" & iReader("CPM_FailFixedFee") & sDefWhere)
		End While
		iReader.Close
	End Sub

	Protected Function FormatDBCurBox(nObj As Decimal) As String
		Return nObj.ToString("0.00")
	End Function
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		NetpayConst.GetPaymentMethodArray(strPaymentMethod)
		NetpayConst.GetCurrencyArray(strCurrencies)
		CompanyID = dbPages.TestVar(Request("ID"), 0, -1, 0)
		CPM_ID = dbPages.TestVar(Request("CPM_ID"), 0, -1, 0)
		DoAction = dbPages.TestVar(Request("DoAction"), -1, "")
		Select Case DoAction
			Case "DelPM"
				dbPages.ExecSql("Delete From tblCompanyPaymentMethods Where CPM_ID=" & CPM_ID)
				dbPages.ExecSql("Delete From tblCompanyCreditRestrictions Where CCR_CPM_ID=" & CPM_ID)
			Case "AddPM"
				SavePaymentMethod()
			Case "EnablePM"
				dbPages.ExecSql("Update tblCompanyPaymentMethods Set CPM_IsDisabled=(1-CPM_IsDisabled) Where CPM_ID=" & CPM_ID)
			Case "Update"
				SavePaymentMethod()
				SaveTerminals()
			Case "DelRest"
				DeleteRst(dbPages.TestVar(Request("CCR_ParentID"), 0, -1, 0), 0)
				SaveTerminals()
			Case "AddTerm"
				SaveTerminal(0, CCR_Types.CCRT_Terminal)
				SaveTerminals()
			Case "AddRest"
				SaveTerminal(0, CCR_Types.CCRT_Bin)
				SaveTerminals()
			Case "MoveRestUp"
				OrderRst(dbPages.TestVar(Request("CCR_ParentID"), 0, -1, 0), True)
			Case "MoveRestDown"
				OrderRst(dbPages.TestVar(Request("CCR_ParentID"), 0, -1, 0), False)
		End Select
		If Request("Recalc") = "1" Then RecalcCompanyFeesEx(CompanyID, dbPages.TestVar(Request("NPayID"), 0, -1, 0))
	End Sub
	
	Private Sub OrderRst(iCCR_ID As Integer, nUp As Boolean)
		Dim oOrder As Integer, nParent As Integer
		Dim iReader As SqlDataReader = dbPages.ExecReader("Select CCR_Order, CCR_ParentID From tblCompanyCreditRestrictions Where CCR_ID=" & iCCR_ID)
		If iReader.Read() Then
			oOrder = dbPages.TestVar(iReader("CCR_Order"), 0, -1, 0)
			nParent = dbPages.TestVar(iReader("CCR_ParentID"), 0, -1, 0)
		End If
		iReader.Close
		Dim dOrder As Integer = dbPages.TestVar(dbPages.ExecScalar("Select Top 1 CCR_ID From tblCompanyCreditRestrictions Where CCR_ParentID=" & nParent & " And " & _
			IIF(nUp, " CCR_Order<" & oOrder & " Order By CCR_Order Desc", " CCR_Order>" & oOrder & " Order By CCR_Order Asc")), 0, -1, 0)
		If dOrder = 0 Then Exit Sub
		If nUp Then
			Dim tmp As Integer = iCCR_ID
			iCCR_ID = dOrder
			dOrder = tmp
			oOrder = oOrder - 1
		End If
		dbPages.ExecSql("Update tblCompanyCreditRestrictions Set CCR_Order=" & oOrder & " Where CCR_ID=" & dOrder)
		dbPages.ExecSql("Update tblCompanyCreditRestrictions Set CCR_Order=" & (oOrder + 1) & " Where CCR_ID=" & iCCR_ID)
	End Sub
		
	Private Sub DeleteRst(iCCR_ID As Integer, nLevel As Integer)
		Dim iReader As SqlDataReader = dbPages.ExecReader("Select CCR_ID From tblCompanyCreditRestrictions Where CCR_ParentID=" & iCCR_ID)
		While iReader.Read()
			DeleteRst(iReader("CCR_ID"), nLevel + 1)
		End While
		iReader.Close()
		If nLevel = 0 Then
			iReader = dbPages.ExecReader("Select CCR_Order, CCR_ParentID From tblCompanyCreditRestrictions Where CCR_ID=" & iCCR_ID)
			If iReader.Read() Then _
				dbPages.ExecSql("Update tblCompanyCreditRestrictions Set CCR_Order=CCR_Order-1 Where CCR_ParentID=" & iReader("CCR_ParentID") & " And CCR_Order >" & iReader("CCR_Order"))
			iReader.Close()
		End if
		dbPages.ExecSql("Delete From tblCompanyCreditRestrictions Where CCR_ID=" & iCCR_ID)
	End Sub

	Private Sub SavePaymentMethod()
		If CPM_ID = 0 Then
			dbPages.ExecSql("Insert Into tblCompanyPaymentMethods(CPM_CompanyID, CPM_PaymentMethod, CPM_CurrencyID)Values(" & _
				CompanyID & "," & _
				dbPages.TestVar(Request("CPM_PaymentMethod"), 0, -1, 0) & "," & _
				dbPages.TestVar(Request("CPM_Currency"), 0, -1, 0) & ")")
		End If
		dbPages.ExecSql("Update tblCompanyPaymentMethods Set " & _
			" CPM_PercentFee=" & dbPages.TestVar(Request("CPM_PercentFee"), -214748d, 214748d, 0d) & _
			",CPM_FixedFee=" & dbPages.TestVar(Request("CPM_FixedFee"), -214748d, 214748d, 0d) & _
			",CPM_ApproveFixedFee=" & dbPages.TestVar(Request("CPM_ApproveFixedFee"), -214748d, 214748d, 0d) & _
			",CPM_RefundFixedFee=" & dbPages.TestVar(Request("CPM_RefundFixedFee"), -214748d, 214748d, 0d) & _
			",CPM_ClarificationFee=" & dbPages.TestVar(Request("CPM_ClarificationFee"), -214748d, 214748d, 0d) & _
			",CPM_CBFixedFee=" & dbPages.TestVar(Request("CPM_CBFixedFee"), -214748d, 214748d, 0d) & _
			",CPM_FailFixedFee=" & dbPages.TestVar(Request("CPM_FailFixedFee"), -214748d, 214748d, 0d) & _
			" Where CPM_ID=" & CPM_ID)
	End Sub

	Private Sub SaveTerminals()
		Dim CCR_IDs() As String = Request.Form.GetValues("CCR_ID")
		If CCR_IDs Is Nothing Then Exit Sub
		For i As Integer = 0 To CCR_IDs.Length - 1
			Dim CCR_ID As Integer = dbPages.TestVar(CCR_IDs(i), 0, -1, 0)
			SaveTerminal(CCR_ID, 0)
		Next
	End Sub
	
	Private Sub SaveTerminal(CCR_ID As Integer, nType As CCR_Types)
		If CCR_ID = 0 Then
			Dim nParent As Integer = dbPages.TestVar(Request("CCR_ParentID"), 0, -1, 0)
			Dim nOrder As Integer = 1 + dbPages.TestVar(dbPages.ExecScalar("Select Max(CCR_Order) From tblCompanyCreditRestrictions Where CCR_ParentID=" & nParent), 0, -1, 0)
			dbPages.ExecSql("Insert Into tblCompanyCreditRestrictions(CCR_Type, CCR_ParentID, CCR_CPM_ID, CCR_Order, CCR_Ratio, CCR_ExchangeTo)Values(" & _
				nType & "," & nParent & "," & CPM_ID & "," & nOrder & ", 1, 255)")
		End If
		If nType = CCR_Types.CCRT_TerminalGroup Then nType = dbPages.TestVar(Request("CCR_Type_" & CCR_ID), 0, -1, 1)
		Dim UpdateStr As String = "Update tblCompanyCreditRestrictions Set " & _
			" CCR_Type=" & dbPages.TestVar(Request("CCR_Type_" & CCR_ID), 0, -1, 1) & _
			",CCR_ChildType=" & dbPages.TestVar(Request("CCR_ChildType_" & CCR_ID), 0, -1, 0)
		If nType = CCR_Types.CCRT_CPassMore Or nType = CCR_Types.CCRT_CPassLess Or nType = CCR_Types.CCRT_Bin Or nType = CCR_Types.CCRT_Country Then
			If Not Request("CCR_ListValue_" & CCR_ID) Is Nothing Then _
				UpdateStr &= ",CCR_ListValue='" & Replace(dbPages.TestVar(Request("CCR_ListValue_" & CCR_ID), -1, ""), "'", "''") & "'"
		ElseIf nType = CCR_Types.CCRT_Terminal Then
			UpdateStr &= ",CCR_UseCount=" & dbPages.TestVar(Request("CCR_UseCount_" & CCR_ID), 0, -1, 0) & _
				",CCR_Ratio=" & dbPages.TestVar(Request("CCR_Ratio_" & CCR_ID), 0, -1, 1) & _
				",CCR_ExchangeTo=" & dbPages.TestVar(Request("CCR_ExchangeTo_" & CCR_ID), 0, 255, 255) & _
				",CCR_TerminalNumber='" & Replace(dbPages.TestVar(Request("CCR_TerminalNumber_" & CCR_ID), 20, ""), "'", "''") & "'" & _
				",CCR_MaxAmount=" & dbPages.TestVar(Request("CCR_MaxAmount_" & CCR_ID), 0d, -1d, 0d) & _
				",CCR_PercentFee=" & dbPages.TestVar(Request("CCR_PercentFee_" & CCR_ID), -214748d, 214748d, 0d) & _
				",CCR_FixedFee=" & dbPages.TestVar(Request("CCR_FixedFee_" & CCR_ID), -214748d, 214748d, 0d) & _
				",CCR_ApproveFixedFee=" & dbPages.TestVar(Request("CCR_ApproveFixedFee_" & CCR_ID), -214748d, 214748d, 0d) & _
				",CCR_RefundFixedFee=" & dbPages.TestVar(Request("CCR_RefundFixedFee_" & CCR_ID), -214748d, 214748d, 0d) & _
				",CCR_ClarificationFee=" & dbPages.TestVar(Request("CCR_ClarificationFee_" & CCR_ID), -214748d, 214748d, 0d) & _
				",CCR_CBFixedFee=" & dbPages.TestVar(Request("CCR_CBFixedFee_" & CCR_ID), -214748d, 214748d, 0d) & _
				",CCR_FailFixedFee=" & dbPages.TestVar(Request("CCR_FailFixedFee_" & CCR_ID), -214748d, 214748d, 0d)
		End If
		dbPages.ExecSql(UpdateStr & " Where CCR_ID=" & CCR_ID)
	End Sub
	
	Private Sub drawPM()
		Dim iReader As SqlDataReader = dbPages.ExecReader("Select * From tblCompanyPaymentMethods Where CPM_CompanyID=" & CompanyID & " Order By CPM_ID Asc")
		While iReader.Read()
			sFirstLoad &= "UpdateValidation(document.getElementById('Form_" & iReader("CPM_ID") & "', false));" & vbCrLf
			Response.Write("<tr style=""background-color:#F0F0F0;""><form method=""post"" id=""Form_" & iReader("CPM_ID") & """>")
			Response.Write(" <input type=""hidden"" name=""DoAction"" value="""">")
			Response.Write(" <input type=""hidden"" name=""CPM_ID"" value=""" & iReader("CPM_ID") & """>")
			Response.Write(" <input type=""hidden"" name=""CCR_ParentID"" value=""0"">")
			Response.Write(" <td width=""5"" style=""background-color:#" & IIF(iReader("CPM_IsDisabled"), "ff6666", "66cc66") & ";""> </td>")
			Dim strCurText As String = "[All Currencies]"
			If iReader("CPM_CurrencyID") <> 255 Then strCurText = strCurrencies(iReader("CPM_CurrencyID"))
            Response.Write(" <td colspan=""2"" width=""70%""><img align=""top"" width=""23"" height=""15"" src=""/NPCommon/ImgPaymentMethod/23X12/" & iReader("CPM_PaymentMethod") & ".gif""> " & strPaymentMethod(iReader("CPM_PaymentMethod")) & " " & strCurText & "</td>")
			Response.Write(" <td><br /><input type=""button"" value=""Update"" id=""UPDATEBTN_" & iReader("CPM_ID") & """ onclick=""if(!UpdateValidation(form, true)) return; doSubmit(form, 'Update', null);""></td>")		
			Response.Write(" <td>Trans<br /><input name=""CPM_PercentFee"" style=""width:40px;"" onchange=""HighlightUpdate('" & iReader("CPM_ID") & "')"" value=""" & FormatDBCurBox(iReader("CPM_PercentFee")) & """>")
			Response.Write("%<input name=""CPM_FixedFee"" style=""width:40px;"" onchange=""HighlightUpdate('" & iReader("CPM_ID") & "')"" value=""" & FormatDBCurBox(iReader("CPM_FixedFee")) & """></td>")
			Response.Write(" <td>Auth<br /><input name=""CPM_ApproveFixedFee"" style=""width:40px;"" onchange=""HighlightUpdate('" & iReader("CPM_ID") & "')"" value=""" & FormatDBCurBox(iReader("CPM_ApproveFixedFee")) & """></td>")
			Response.Write(" <td>Refund<br /><input name=""CPM_RefundFixedFee"" style=""width:40px;"" onchange=""HighlightUpdate('" & iReader("CPM_ID") & "')"" value=""" & FormatDBCurBox(iReader("CPM_RefundFixedFee")) & """></td>")
			Response.Write(" <td>Clrf<br /><input name=""CPM_ClarificationFee"" style=""width:40px;"" onchange=""HighlightUpdate('" & iReader("CPM_ID") & "')"" value=""" & FormatDBCurBox(iReader("CPM_ClarificationFee")) & """></td>")
			Response.Write(" <td>CHB<br /><input name=""CPM_CBFixedFee"" style=""width:40px;"" onchange=""HighlightUpdate('" & iReader("CPM_ID") & "')"" value=""" & FormatDBCurBox(iReader("CPM_CBFixedFee")) & """></td>")
			Response.Write(" <td>Fail<br /><input name=""CPM_FailFixedFee"" style=""width:40px;"" onchange=""HighlightUpdate('" & iReader("CPM_ID") & "')"" value=""" & FormatDBCurBox(iReader("CPM_FailFixedFee")) & """></td>")
			Response.Write(" <td><br />")
			Response.Write("<input type=""button"" value=""e"" style=""font-family:Wingdings 3;"" title=""Add Terminal"" onclick=""doSubmit(form, 'AddTerm', 0)"">&nbsp;")
			Response.Write("<input type=""button"" value="">"" style=""font-family:Wingdings;"" title=""Add Restriction"" onclick=""doSubmit(form, 'AddRest', 0)"">&nbsp;")
			Response.Write("<input type=""button"" value=""" & IIF(iReader("CPM_IsDisabled"), " M ", " L ") & """ title=""Enable"" style=""font-family:Wingdings 2;"" onclick=""doSubmit(form, 'EnablePM', 0)"">&nbsp;")
			Response.Write("<input type=""button"" value=""Î"" style=""font-family:Wingdings 2; color:red;"" title=""Delete"" onclick=""if(confirm('Delete, Are you sure?')) return doSubmit(form, 'DelPM', 0)"">")
			Response.Write(" </td>")
			'"<select onchange=""switch(options[selectedIndex].value){case 'DelPM': if(!confirm('Delete, Are you sure?')) return (this.selectedIndex=0);break; case 'EnablePM': if(!confirm('Disable, Are you sure?')) return (this.selectedIndex=0); }form.elements['DoAction'].value=options[selectedIndex].value;form.submit();""><option><option value=""AddTerm"">Add Terminal<option value=""AddRest"">Add Restriction<option value=""EnablePM"">" & IIF(iReader("CPM_IsDisabled"), "Enable", "Disable") & "<option value=""DelPM"">Delete</select></td>"
			Response.Write("</tr>")
			drawRST(iReader("CPM_ID"),  0, CCR_Types.CCRT_TerminalGroup, 0)
			Response.Write("</form>")
			Response.Write("<tr><td><br /><br /></td></tr>")
		End While
		iReader.Close()
	End Sub
	
	Private Sub DrawRuleList(nCPM_ID As Integer, nCCR_ID As Integer, nType As CCR_Types, nStyle As String)
		Response.Write("<select style=""" & nStyle & """ onchange=""doSubmit(form, 'Update', " & nCCR_ID & ");"" name=""CCR_Type_" & nCCR_ID & """>")
		Response.Write("<option value=""" & CCR_Types.CCRT_TerminalGroup & """" & IIF(nType = CCR_Types.CCRT_Country, " selected", "") & ">Terminal Group")
		Response.Write("<option value=""" & CCR_Types.CCRT_Country & """" & IIF(nType = CCR_Types.CCRT_Country, " selected", "") & ">Country List")
		Response.Write("<option value=""" & CCR_Types.CCRT_Bin & """" & IIF(nType = CCR_Types.CCRT_Bin, " selected", "") & ">Bin List")
		Response.Write("<option value=""" & CCR_Types.CCRT_BlackList & """" & IIF(nType = CCR_Types.CCRT_BlackList, " selected", "") & ">US Friendly")
		Response.Write("<option value=""" & CCR_Types.CCRT_WhiteList & """" & IIF(nType = CCR_Types.CCRT_WhiteList, " selected", "") & ">US Not Friendly")
		Response.Write("<option value=""" & CCR_Types.CCRT_CPassMore & """" & IIF(nType = CCR_Types.CCRT_CPassMore, " selected", "") & ">Card passed More than")
		Response.Write("<option value=""" & CCR_Types.CCRT_CPassLess & """" & IIF(nType = CCR_Types.CCRT_CPassLess, " selected", "") & ">Card passed Less than")
		Response.Write("<option value=""" & CCR_Types.CCRT_SelectGroup & """" & IIF(nType = CCR_Types.CCRT_SelectGroup, " selected", "") & ">Merchant Select List")
		Response.Write("</select>")
	End Sub
	
	Private Sub drawRST(nCPM_ID As Integer, nParentID As Integer, nParentType As CCR_Types, lDeep As Integer)
		Dim nLastIsTerminal As Boolean = False, lIndex As Integer = 0
		Dim iReader As SqlDataReader = dbPages.ExecReader("Select tblCompanyCreditRestrictions.*, tblDebitTerminals.ID As DTID, tblDebitTerminals.isNetpayTerminal From tblCompanyCreditRestrictions " & _
			" Left Join tblDebitTerminals ON(tblCompanyCreditRestrictions.CCR_TerminalNumber = tblDebitTerminals.terminalNumber)" & _
			" Where CCR_CPM_ID=" & nCPM_ID & " And CCR_ParentID=" & nParentID & " Order By CCR_Order Asc")
		Dim bRead As Boolean = iReader.Read()
		While bRead
			Dim CCR_Type As Integer = iReader("CCR_Type")
			Dim CCR_ID As Integer = iReader("CCR_ID")
			Dim bAmountError As Boolean = (iReader("CCR_FixedFee") = 0) Or (iReader("CCR_PercentFee") = 0) Or (iReader("CCR_ApproveFixedFee") = 0) Or (iReader("CCR_RefundFixedFee") = 0) Or (iReader("CCR_ClarificationFee") = 0) Or (iReader("CCR_CBFixedFee") = 0)
			Response.Write(" <input type=""hidden"" name=""CCR_ID"" value=""" & iReader("CCR_ID") & """>")
			Response.Write("<tr " & IIF(nLastIsTerminal And iReader("CCR_Type") <> CCR_Types.CCRT_Terminal, " style=""background-color:#FFEEEE""", "") & "><td></td>")
			Response.Write("<td nowrap>")
			For i As Integer = 0 To lDeep - 1
				Response.Write("<img src=""/NPCommon/images/tree.gif"" align=""top"">")
			Next
			Response.Write("<img src=""/NPCommon/images/treeV.gif"" align=""top"">")
			If CCR_Type = CCR_Types.CCRT_Terminal Then
				nLastIsTerminal = True
				If nParentType = CCR_Types.CCRT_SelectGroup Then 
					Response.Write("<span style=""height:15px;margin-top:16px;"">" & iReader("DTID") & "</span>")
				Else
					Response.Write("<input style=""width:30px;margin-top:13px;"" onchange=""HighlightUpdate('" & nCPM_ID & "')"" name=""CCR_Ratio_" & iReader("CCR_ID") & """ value=""" & iReader("CCR_Ratio") & """>")
				End If
				Response.Write("</td>")
				Response.Write("<td nowrap>Terminal<br />")
					'iReader("isNetpayTerminal")
					drawTerminalList("CCR_TerminalNumber_" & iReader("CCR_ID"), iReader("CCR_TerminalNumber"), nCPM_ID)
					If dbPages.TestVar(iReader("CCR_Ratio"), 0, -1, 0) = 0 Then _
						Response.Write(" <img src=""/NPCommon/Images/alert_small.png"" title=""Ratio is 0"" align=""top"" height=""16"" border=""0""> ")
				Response.Write("</td>")
				Response.Write("<td>Exchange<br />" & drawComboCurrency("CCR_ExchangeTo_" & CCR_ID, iReader("CCR_ExchangeTo"), nCPM_ID) & "</td>")
				Response.Write("<td>Trans<br /><input name=""CCR_PercentFee_" & CCR_ID & """ style=""width:40px;"" onchange=""HighlightUpdate('" & nCPM_ID & "')"" value=""" & FormatDBCurBox(iReader("CCR_PercentFee")) & """>")
				Response.Write("%<input name=""CCR_FixedFee_" & CCR_ID & """ onchange=""HighlightUpdate('" & nCPM_ID & "')"" style=""width:40px;"" value=""" & FormatDBCurBox(iReader("CCR_FixedFee")) & """></td>")
				Response.Write("<td>Auth<br /><input name=""CCR_ApproveFixedFee_" & CCR_ID & """ onchange=""HighlightUpdate('" & nCPM_ID & "')"" style=""width:40px;"" value=""" & FormatDBCurBox(iReader("CCR_ApproveFixedFee")) & """></td>")
				Response.Write("<td>Refund<br /><input name=""CCR_RefundFixedFee_" & CCR_ID & """ onchange=""HighlightUpdate('" & nCPM_ID & "')"" style=""width:40px;"" value=""" & FormatDBCurBox(iReader("CCR_RefundFixedFee")) & """></td>")
                Response.Write("<td>Clrf<br /><input name=""CCR_ClarificationFee_" & CCR_ID & """ onchange=""HighlightUpdate('" & nCPM_ID & "')"" style=""width:40px;"" value=""" & FormatDBCurBox(iReader("CCR_ClarificationFee")) & """></td>")
				Response.Write("<td>CHB<br /><input name=""CCR_CBFixedFee_" & CCR_ID & """ onchange=""HighlightUpdate('" & nCPM_ID & "')"" style=""width:40px;"" value=""" & FormatDBCurBox(iReader("CCR_CBFixedFee")) & """></td>")
				Response.Write("<td>Fail<br /><input name=""CCR_FailFixedFee_" & CCR_ID & """ onchange=""HighlightUpdate('" & nCPM_ID & "')"" style=""width:40px;"" value=""" & FormatDBCurBox(iReader("CCR_FailFixedFee")) & """></td>")
			ElseIf CCR_Type = CCR_Types.CCRT_TerminalGroup Or CCR_Type = CCR_Types.CCRT_SelectGroup Or CCR_Type = CCR_Types.CCRT_BlackList Or CCR_Type = CCR_Types.CCRT_WhiteList Then
				Response.Write("<img src=""/NPCommon/images/treeE.gif""></td>")
				Response.Write("<td colspan=""1"">Type<br />")
				DrawRuleList(nCPM_ID, CCR_ID, CCR_Type, "width:300px;")
				Response.Write("</td><td colspan=""7""></td>")
			Else
				Response.Write("<img src=""/NPCommon/images/treeE.gif""></td>")
				Response.Write("<td colspan=""1"">Type<br />")
				DrawRuleList(nCPM_ID, CCR_ID, CCR_Type, "width:260px;")
				Response.Write("&nbsp;&nbsp;<select name=""CCR_ChildType_" & iReader("CCR_ID") & """ onchange=""HighlightUpdate('" & nCPM_ID & "')""><option value=""0""" & IIF(iReader("CCR_ChildType") = 0, " selected", "") & ">=<option value=""1""" & IIF(iReader("CCR_ChildType") = 1, " selected", "") & ">≠</select></td>")
				Response.Write("<td colspan=""7"">Values<br /><input name=""CCR_ListValue_" & iReader("CCR_ID") & """ onchange=""HighlightUpdate('" & nCPM_ID & "')"" value=""" & iReader("CCR_ListValue") & """ style=""width:410px;""></td>")
			End If
			bRead = iReader.Read()
			Response.Write("<td><br />")
			 Response.Write("<input type=""button"" value=""e"" style=""font-family:Wingdings 3;"" title=""Add Terminal"" onclick=""doSubmit(form, 'AddTerm', " & CCR_ID & ")"">&nbsp;")
			 Response.Write("<input type=""button"" value="">"" style=""font-family:Wingdings;"" title=""Add Restriction"" onclick=""doSubmit(form, 'AddRest', " & CCR_ID & ")"">&nbsp;")
			 Response.Write("<input type=""button"" value=""á"" style=""font-family:Wingdings;"" " & IIF(lIndex > 0, "", "DISABLED") & " title=""MoveUp"" onclick=""doSubmit(form, 'MoveRestUp', " & CCR_ID & ")"">&nbsp;")
			 Response.Write("<input type=""button"" value=""â"" style=""font-family:Wingdings;"" " & IIF(bRead, "", "DISABLED") & " title=""MoveDown"" onclick=""doSubmit(form, 'MoveRestDown', " & CCR_ID & ")"">&nbsp;")
			 Response.Write("<input type=""button"" value=""Î"" style=""font-family:Wingdings 2; color:red;"" title=""Delete"" onclick=""if(confirm('Delete, Are you sure?')) doSubmit(form, 'DelRest', " & CCR_ID & ")"">")
			Response.Write("</td>")
			'Response.Write("<td><br /><select style=""width:100px"" onchange=""if(options[selectedIndex].value == 'DelRest') if(!confirm('Delete, Are you sure?')) return (this.selectedIndex=0); form.elements['DoAction'].value=options[selectedIndex].value;form.elements['CCR_ParentID'].value=" & iReader("CCR_ID") & ";form.submit();""><option><option value=""AddTerm"">Add Terminal<option value=""AddRest"">Add Restriction<option value=""MoveRestUp"">Move Up<option value=""MoveRestDown"">Move Down<option value=""DelRest"">Delete</select></td>")
			Response.Write("</tr>")
			lIndex = lIndex + 1
			drawRST(nCPM_ID, CCR_ID, CCR_Type, lDeep + 1)
		End While
		iReader.Close()
	End Sub	

	Private Function drawComboCurrency(cmbName As String, lSelValue As Integer, sPMID As Integer) As String
		drawComboCurrency = "<select name=""" & cmbName & """ class=""inputDataWhite"" onchange=""HighlightUpdate('" & sPMID & "')"">"
		drawComboCurrency &= "<option value=""255"" " & IIf(lSelValue = 255, " selected", "") & ">" & "[All]"
		For i As Integer = 0 To eCurrencies.MAX_CURRENCY
			drawComboCurrency &= "<option value=""" & i & """" & IIf(lSelValue = i, " selected", "") & ">" & dbPages.GetCurISO(i)
		Next
		drawComboCurrency &= "</select>"
	End Function

	Private Sub drawTerminalList(cmbName As String, strValue As String, sPMID As Integer)
		Response.Write("<select onmousedown=""LoadTerminalCombo(this, '" & strValue & "')"" name=""" & cmbName & """ id=""" & cmbName & """ style=""width:300px;"" onchange=""HighlightUpdate('" & sPMID & "')"" class=""inputDataWhite""></select>")
		sFirstLoad &= "LoadStateTerm(document.getElementById('" & cmbName & "'), '" & strValue & "');" & vbCrLf
	End Sub
</script>
<html>
<head runat="server">
    <title>Merchant Company Terminal</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<script src="../js/func_common.js" type="text/jscript"></script>
</head>
<body class="itext" dir="ltr">
	<UC1:TabMenu runat="server" id="TabMenu1" />
	<%
	If dbPages.ExecScalar("Select IsUsingNewTerminal From tblCompany Where ID=" & CompanyID) <> True Then
		%>
		<table border="0" cellspacing="1" align="center" width="90%" class="formNormal" style="border:1px solid silver;">
		<tr>
			<td style="color:#bc3a4f; font-size:14px; text-align:left;">
				<img src="/NPCommon/Images/alert_small.png" alt="!" align="middle" border="0" />
				&nbsp; THIS MERCHANT IS USING OLD TERMINAL CONFIGURATION<br />
			</td>
		</tr>
		</table>
		<br />
		<%
	End if
    %>
	<table border="0" class="formNormal" cellspacing="0" align="center" width="95%">
	<tr>
		<td colspan="3">
			<form method="post" action=".">
				<input type="hidden" name="DoAction" value="AddPM" />
				<select name="CPM_PaymentMethod" onchange="HighlightUpdate('0')">
					<%
						For i As Integer = 15 To strPaymentMethod.Length - 1
							If strPaymentMethod(i) <> "" Then Response.Write("<option value=""" & i & """>" & strPaymentMethod(i))
						Next
					%>
				</select>
				<%=drawComboCurrency("CPM_Currency", 255, 0)%>
				<input type="submit" value="Add Card" id="UPDATEBTN_0" />
			</form>	
		</td>
	</tr>
	<tr>
	 <td colspan="10"><br /></td>
	 <td style="text-align:center">ACTION</td>
	</tr>
	 <%drawPM()%>
	<tr><td colspan="5"><br /><a href="?CompanyID=<%=CompanyID%>&Recalc=1&NPayID=0">Update unsettled transaction Fees</a></td></tr>
	</table>
	<script language="javascript" type="text/javascript">
		function doSubmit(pForm, nAct, nParent)
		{
			pForm.elements['DoAction'].value=nAct;
			if(nParent) pForm.elements['CCR_ParentID'].value=nParent;
			pForm.submit();		
		}

		function TestMinDecInput(pInput, nValue)
		{
			if(parseFloat(pInput.value) < nValue){
				pInput.style.color = '#ff0000';
				return true;
			} return false;
		}

		function TestMinDecInputRef(pInput, pRefInput, nValue){
			pRet = TestMinDecInput(pInput, nValue);
			if(pRet){
				pRet = TestMinDecInput(pRefInput, nValue);
				if(!pRet) pInput.style.color = '';
			}
			return pRet;
		}

		function ValidatePMFees(pForm){
			var bResults = false;
			bResults |= TestMinDecInput(pForm.elements('CPM_FixedFee'), 2);
			bResults |= TestMinDecInput(pForm.elements('CPM_PercentFee'), 0.25);
			bResults |= TestMinDecInput(pForm.elements('CPM_RefundFixedFee'), 0.01);
			//bResults |= TestMinDecInput(pForm.elements('CPM_ApproveFixedFee'), 0.01);
			//bResults |= TestMinDecInput(pForm.elements('CPM_ClarificationFee'), 0.01);
			bResults |= TestMinDecInput(pForm.elements('CPM_CBFixedFee'), 0.01);
			//bResults |= TestMinDecInput(pForm.elements('CPM_FailFixedFee'), 0.01);
			return !bResults;
		}

		function UpdateValidation(pForm, bAlert)
		{
			//if(ValidatePMFees(pForm)) return true;
			var bResults = false;
			for(var i = 0; i < pForm.elements.length; i++){
				if((pForm.elements(i).name.indexOf('CCR_FixedFee') > -1)) bResults |= TestMinDecInputRef(pForm.elements(i), pForm.elements('CPM_FixedFee'), 2);
				else if((pForm.elements(i).name.indexOf('CCR_PercentFee') > -1)) bResults |= TestMinDecInputRef(pForm.elements(i), pForm.elements('CPM_PercentFee'), 0.25);
				else if((pForm.elements(i).name.indexOf('CCR_RefundFixedFee') > -1)) bResults |= TestMinDecInputRef(pForm.elements(i), pForm.elements('CPM_RefundFixedFee'), 0.01);
				//else if((pForm.elements(i).name.indexOf('CCR_ApproveFixedFee') > -1)) bResults |= TestMinDecInputRef(pForm.elements(i), pForm.elements('CPM_ApproveFixedFee'), 0.01);
				//else if((pForm.elements(i).name.indexOf('CCR_ClarificationFee') > -1)) bResults |= TestMinDecInputRef(pForm.elements(i), pForm.elements('CPM_ClarificationFee'), 0.01);
				else if((pForm.elements(i).name.indexOf('CCR_CBFixedFee') > -1)) bResults |= TestMinDecInputRef(pForm.elements(i), pForm.elements('CPM_CBFixedFee'), 0.01);
				//else if((pForm.elements(i).name.indexOf('CCR_FailFixedFee') > -1)) bResults |= TestMinDecInputRef(pForm.elements(i), pForm.elements('CPM_FailFixedFee'), 0.01);
			}
			if(!bAlert) return !bResults;
			if(bResults) return confirm('There are some unset fees in this terminal!\n\nDo you really want to continue?'); 
			return true;
		}	

		function HighlightUpdate(nID) { document.getElementById('UPDATEBTN_' + nID).style.backgroundColor='#ff6666'; }
		
		var vTerminalList = new Array();
		<%
		Dim iReader As SqlDataReader = dbPages.ExecReader("spSelect_TerminalSelectList")
		If iReader.HasRows Then
			While iReader.Read()
				Dim sStyle As String = "", nStrValues As String = "", bIsShow As Boolean = True
				If Not iReader("isActive") Then
					sStyle = "maroon"
				ElseIf iReader("isNetpayTerminal") Then
					sStyle = "#315AEA"
				End If
				If iReader("isActive") Then
					nStrValues = IIF(iReader("isNetpayTerminal"), 2, IIF(iReader("isActive"), 1, 0)) & ","
					nStrValues = "'" & sStyle & "','" & iReader("dc_name") & " | " & iReader("dt_name") & " | " & iReader("terminalNumber")
					If CInt(iReader("processingMethod")) = 2 Then nStrValues &= " (נכנס להמתנה)"
					nStrValues &= "'"
					Response.Write("vTerminalList['" & iReader("terminalNumber") & "']= new Array(" & nStrValues & ");" & vbCrLf)
				End If
			End While
		End If
		iReader.Close
		%>
		
		function LoadTerminalCombo(cmb, nSel){
			if(cmb.options.length > 1) return;
			cmb.innerHTML = '';
			for (key in vTerminalList){
				var oOption = document.createElement("OPTION");
				cmb.options.add(oOption);
				oOption.style.color = vTerminalList[key][0];
				oOption.innerHTML = vTerminalList[key][1];
				oOption.value = key;
				if(key == nSel) oOption.selected = true;
			}
		}

		function LoadStateTerm(cmb, nSel){
			if(!vTerminalList[nSel]) return;
			var oOption = document.createElement("OPTION");
			cmb.options.add(oOption);
			oOption.style.color = vTerminalList[nSel][0];
			oOption.innerHTML = vTerminalList[nSel][1];
			oOption.value = nSel;
			oOption.selected = true;
		}
		<%=sFirstLoad%>
	</script>
</body>
</html>
