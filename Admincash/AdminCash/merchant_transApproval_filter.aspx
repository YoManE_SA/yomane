<%@ Page Language="VB" Inherits="htmlInputs" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>
<%@ Register Src="Filter_DebitCompanies.ascx" TagName="FilterDebitCompanies" TagPrefix="Uc1" %>

<script language="vbscript" runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        fdtrControl.FromDateTime = Date.Now.AddMonths(-3).AddDays(1).ToShortDateString
        fdtrControl.ToDateTime = Date.Today.AddHours(23).AddMinutes(59).AddSeconds(59)
        
        If HttpContext.Current.Application.Get("CUR_CHARS") Is Nothing Then dbPages.LoadCurrencies()
        Dim sCurrenciesSelect As String = "<select name=""Currency"" class=""grayBG"">"
        sCurrenciesSelect = sCurrenciesSelect & "<option value=""""></option>"
        For i As Integer = 0 To eCurrencies.MAX_CURRENCY
            sCurrenciesSelect = sCurrenciesSelect & "<option value=""" & i & """>" & HttpContext.Current.Application.Get("CUR_ISO")(i) & "</option>"
        Next
        sCurrenciesSelect = sCurrenciesSelect & "</select>"
        litCurrencies.Text = sCurrenciesSelect
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

	<form runat="server" action="merchant_transApproval.aspx" target="frmBody" method="get" onsubmit="return filterForm_Submit(null);">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server"></asp:ScriptManager>
    <input type="hidden" id="Source" name="Source" value="Filter" />
	<input type="hidden" id="ShowCompanyID" name="ShowCompanyID" />

	<h1><asp:Label ID="lblPermissions" runat="server" /> Pre-Authorized<span> - Filter</span></h1>

	<table class="filterNormal">
		<tr>
			<td>
				<Uc1:FilterMerchants id="FMerchants" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Debit Terminal</th></tr>
		<tr>
			<td>
				<Uc1:FilterDebitCompanies ID="FilterDebitCompanies1" ClientFieldDebitCompany="DebitCompanyID" ClientFieldDebitTerminal="TerminalNumber" runat="server" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">Transaction Details</th></tr>
		<tr>
			<td>ID</td>
			<td>
				<input name="TransactionID" class="medium grayBG" />
			</td>
		</tr>
		<tr>
			<td>Amount</td>
			<td>
				<input name="AmountMin" class="medium grayBG" style="width:50px;" />
				to <input name="AmountMax" class="medium grayBG" style="width:50px;" />
			</td>
		</tr>
		<tr>
			<td>Currency</td>
			<td colspan="2"><asp:Literal runat="server" ID="litCurrencies" /></td>
		</tr>		
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th colspan="2">CC Details</th></tr>
		<tr>
			<td>BIN</td>
			<td><input name="BIN" style="width:50px;" /> </td>
		</tr>
		<tr>
			<td>Last 4 digits</td>
			<td><input name="CcLast4Num" class="short grayBG" /> </td>
		</tr>
		<tr>
			<td>Holder name</td>
			<td><input name="ccHolderName" /> </td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input name="ccEmail" /> </td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td>
				<select name="iPageSize">
					<option value="20" selected="selected">20 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td><input type="checkbox" name="SQL2" value="1" checked="checked" class="option" />SQL2</td>
			<td align="right">&nbsp;&nbsp;<input type="submit" name="Action" value="SEARCH"><br /></td>
		</tr>
	</table>
	</form>
</asp:Content>