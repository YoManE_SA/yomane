<%@ Control Language="VB" ClassName="PeriodicFees" EnableViewState="true" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Register Namespace="Netpay.AdminControls" TagPrefix="AC" %>
<script runat="server">
	Public Property Title() As String
		Get
			Return litListTitle.Text
		End Get
		Set(ByVal value As String)
			litListTitle.Text = value
		End Set
	End Property

	Public Property IsAnnual() As Boolean
		Get
			Return dsPeriodic.SelectParameters("bAnnual").DefaultValue
		End Get
		Set(ByVal value As Boolean)
			dsPeriodic.SelectParameters("bAnnual").DefaultValue = value
		End Set
	End Property

	Public Sub Save()
		Dim nMerchant As Integer = Request.QueryString("ID")
		Dim nTypeID As Integer, bActive As Boolean, dtNext As Date, sDateNext As String
		For Each riFee As RepeaterItem In rptPeriodic.Items
			If riFee.ItemType <> ListItemType.Header Then
				nTypeID = CType(riFee.FindControl("hidTypeID"), HiddenField).Value
				bActive = CType(riFee.FindControl("chkIsActive"), CheckBox).Checked
				sDateNext = String.Empty
				If Date.TryParse(CType(riFee.FindControl("txtNextDate"), TextBox).Text, dtNext) Then sDateNext = ", '" & dtNext.ToString("yyyyMMdd") & "'"
				dbPages.ExecSql("EXEC SavePeriodicFee " & nMerchant & ", " & nTypeID & ", " & IIf(bActive, 1, 0) & sDateNext)
				CType(riFee.FindControl("txtNextDate"), TextBox).Enabled = bActive
				CType(riFee.FindControl("lblName"), Label).ForeColor = IIf(bActive, Color.Black, Color.Silver)
				CType(riFee.FindControl("lblAmount"), Label).ForeColor = IIf(bActive, Color.Black, Color.Silver)
				CType(riFee.FindControl("lblCurrency"), Label).ForeColor = IIf(bActive, Color.Black, Color.Silver)
				CType(riFee.FindControl("lblLimit"), Label).ForeColor = IIf(bActive, Color.Black, Color.Silver)
				CType(riFee.FindControl("lblBehavior"), Label).ForeColor = IIf(bActive, Color.Black, Color.Silver)
			End If
		Next
		dsPeriodic.DataBind()
	End Sub

	Sub Page_Load()
		dsPeriodic.ConnectionString = dbPages.DSN
		If Not Page.IsPostBack Then
			litListTitle.Text = Title
		End If
	End Sub
</script>
<table border="0" cellpadding="1" cellspacing="0" class="formNormal" width="100%">
	<tr>
		<td colspan="2">
			<div class="MerchantSubHead"><asp:Literal ID="litListTitle" Text="Annual Fees" runat="server" /></div><br />
		</td>
	</tr>
	<asp:Repeater ID="rptPeriodic" DataSourceID="dsPeriodic" runat="server">
		<HeaderTemplate>
			<tr>
				<th>Name</th>
				<th colspan="2">Amount</th>
				<th>Type</th>
				<th>Activity Fees</th>
				<th style="text-align:center;">Active</th>
				<th>Next Charge</th>
			</tr>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td style="white-space:nowrap;"><asp:Label ID="lblName" ForeColor='<%#IIf(Eval("IsActive"), Color.Black, Color.Silver)%>' Text='<%#Eval("Name")%>' runat="server" /><asp:HiddenField ID="hidTypeID" Value='<%#Eval("TypeID")%>' runat="server" /></td>
				<td><asp:Label ID="lblCurrency" ForeColor='<%#IIf(Eval("IsActive"), Color.Black, Color.Silver)%>' Text='<%#Eval("CurrencyISO")%>' runat="server" /></td>
				<td><asp:Label ID="lblAmount" ForeColor='<%#IIf(Eval("IsActive"), Color.Black, Color.Silver)%>' Text='<%#CType(Eval("Amount"), Decimal).ToString("#,0.00")%>' runat="server" /></td>
				<td>
					<asp:Label ID="lblBehavior" ForeColor='<%#IIf(Eval("IsActive"), Color.Black, Color.Silver)%>' Text='<%#IIf(Eval("BehaviorTitle")<>"", Eval("BehaviorTitle"), "Full charge - No conditions")%>' runat="server" />
					<AC:DivBox runat="server" Visible='<%#Not String.IsNullOrWhitespace(Eval("BehaviorDescription"))%>' Text='<%#Eval("BehaviorDescription")%>' />
				</td>
				<td><asp:Label ID="lblLimit" ForeColor='<%#IIf(Eval("IsActive"), Color.Black, Color.Silver)%>' Text='<%#IIf(CType(Eval("FeeLimit"), Decimal)=0, String.Empty, CType(Eval("FeeLimit"), Decimal).ToString("#,0.00"))%>' runat="server" /></td>
				<td style="text-align:center;"><asp:CheckBox ID="chkIsActive" Checked='<%#Eval("IsActive")%>' runat="server" CssClass="option" onclick="ResetNextDate(this);" /></td>
				<td><asp:TextBox ID="txtNextDate" Text='<%#CType(Eval("NextDate"), Date).ToString("dd/MM/yyyy")%>' Enabled='<%#Eval("IsActive")%>' runat="server" CssClass="inputdata medium" /></td>
			</tr>
		</ItemTemplate>
	</asp:Repeater>
	<asp:SqlDataSource ID="dsPeriodic" SelectCommand="SELECT * FROM GetPeriodicFees(@nMerchantID, @bAnnual)" runat="server">
		<SelectParameters>
			<asp:QueryStringParameter Name="nMerchantID" QueryStringField="ID" Type="Int32" DefaultValue="0" />
			<asp:Parameter Name="bAnnual" Type="Boolean" DefaultValue="False" />
		</SelectParameters>
	</asp:SqlDataSource>
</table>