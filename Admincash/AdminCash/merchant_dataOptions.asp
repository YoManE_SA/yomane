<%
response.buffer = true
%>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_security.asp" -->
<!--#include file="../include/func_controls.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
Sub SqlMerchantUpdateAppendInteger(sSQL, sIntegerColumnName, nMin, nMax)
	sSQL=sSQL & IIf(sSQL="", "", ", ") & sIntegerColumnName & "=" & TestNumVar(Request.Form(sIntegerColumnName), nMin, nMax, 0)
End Sub

Sub SqlMerchantUpdateAppendBoolean(sSQL, sBitColumnName)
	SqlMerchantUpdateAppendInteger sSQL, sBitColumnName, 0, 1
End Sub

Sub SqlMerchantUpdateAppendText(sSQL, sTextColumnName)
	sSQL=sSQL & IIf(sSQL="", "", ", ") & sTextColumnName & "='" & DBText(Request.Form(sTextColumnName)) & "'"
End Sub

Sub SqlMerchantUpdateAppend256(sSQL, sColumnName)
	sSQL=sSQL & IIf(sSQL="", "", ", ") & sColumnName & "256=dbo.GetEncrypted256('" & DBText(Request(sColumnName)) & "')"
End Sub

nCompanyID = TestNumVar(Request("CompanyID"), 1, 0, 0)
If trim(request("Action"))="update" Then
	sSQL=""
	SqlMerchantUpdateAppendBoolean sSQL, "IsAutoPersonalSignup"
	SqlMerchantUpdateAppendBoolean sSQL, "IsCcStorage"
	SqlMerchantUpdateAppendBoolean sSQL, "IsTerminalProbLog"
	SqlMerchantUpdateAppendBoolean sSQL, "IsConnectionProbLog"
	SqlMerchantUpdateAppendBoolean sSQL, "IsTransLookup"
	SqlMerchantUpdateAppendBoolean sSQL, "IsSendUserConfirmationEmail"
	SqlMerchantUpdateAppendBoolean sSQL, "IsAllowMakePayments"
	SqlMerchantUpdateAppendBoolean sSQL, "IsAllowBatchFiles"
	SqlMerchantUpdateAppendBoolean sSQL, "IsRefund"
	SqlMerchantUpdateAppendBoolean sSQL, "IsAskRefund"
	SqlMerchantUpdateAppendBoolean sSQL, "IsCcStorageCharge"
	SqlMerchantUpdateAppendBoolean sSQL, "IsApprovalOnly"
	SqlMerchantUpdateAppendBoolean sSQL, "IsConfirmation"
	SqlMerchantUpdateAppendBoolean sSQL, "IsAllowRemotePull"
	SqlMerchantUpdateAppendBoolean sSQL, "IsAskRefundRemote"
	SqlMerchantUpdateAppendBoolean sSQL, "IsMerchantNotifiedOnPass"
	SqlMerchantUpdateAppendBoolean sSQL, "IsMerchantNotifiedOnFail"
	SqlMerchantUpdateAppendBoolean sSQL, "IsInterestedInNewsletter"
	SqlMerchantUpdateAppendText sSQL, "PassNotifyEmail"
	SqlMerchantUpdateAppendText sSQL, "RemotePullIPs"
	SqlMerchantUpdateAppendText sSQL, "RemoteRefundRequestIPs"
	SqlMerchantUpdateAppendInteger sSQL, "AutoCaptureHours", 0, 72
	SqlMerchantUpdateAppendInteger sSQL, "MultiChargeProtectionMins", 0, 20000
	sSQL="UPDATE tblCompany SET " & sSQL & " WHERE ID=" & nCompanyID
	oledbData.Execute sSQL

	If TestNumVar(Request("IsInvoiceEnabled"), 0, 1, 0) = 1 Then
		sSQL="IF NOT EXISTS (SELECT 1 FROM Setting.SetMerchantInvoice WHERE Merchant_id=" & nCompanyID & ")" & _
		"	INSERT INTO Setting.SetMerchantInvoice(Merchant_id, IsEnable, IsAutoGenerateILS, IsAutoGenerateOther) VALUES (" & nCompanyID & ", 1, 0, 0);" & _
	   "UPDATE Setting.SetMerchantInvoice SET IsEnable=1" & _
		", ExternalProviderID=" & TestNumVar(Request("ExternalProviderID"), 1, 0, 0) & _
		", ExternalUseName='" & DBText(Request("InvoiceExternalUseName")) & "'" & _
		", ExternalUserID='" & DBText(Request("InvoiceExternalUserID")) & "'" & _
		", ExternalPassword='" & DBText(Request("InvoiceExternalPassword")) & "'" & _
		" WHERE Merchant_id=" & nCompanyID
	Else
	    sSQL="UPDATE Setting.SetMerchantInvoice SET IsEnable=0 WHERE Merchant_id=" & nCompanyID
	End If
	oledbData.Execute sSQL

	nIsEnabled=TestNumVar(request("RecurringIsEnabled"), 0, 1, 0)
	nIsEnabledFromTransPass=TestNumVar(request("RecurringIsEnabledFromTransPass"), 0, 1, 0)
	nIsEnabledModify=TestNumVar(request("RecurringIsEnabledModify"), 0, 1, 0)
	nMaxCharges=TestNumVar(request("RecurringMaxCharges"), 1, 0, 0)
	nMaxStages=TestNumVar(request("RecurringMaxStages"), 1, 0, 0)
	nMaxYears=TestNumVar(request("RecurringMaxYears"), 1, 0, 0)
	oledbData.Execute "EXEC SaveMerchantRecurringSettings " & nCompanyID & ", " & nIsEnabled & ", " & nIsEnabledFromTransPass & ", " & nIsEnabledModify & ", " & nMaxCharges & ", " & nMaxStages & ", " & nMaxYears & ";"
End if

Set InvoiceData = oledbData.Execute("SELECT * FROM Setting.SetMerchantInvoice WHERE Merchant_id=" & nCompanyID)
If InvoiceData.EOF Then CloseInvoiceSettings()

Function GetInvoiceSettings(sValue)
    If InvoiceData Is Nothing Then Exit Function
    GetInvoiceSettings = InvoiceData(sValue)
End Function

Sub CloseInvoiceSettings()
    If InvoiceData Is Nothing Then Exit Sub
    InvoiceData.Close()
    Set InvoiceData = Nothing
End Sub

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255"/>
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/styleAdminEng.css"/>
	<script type="text/javascript" language="javascript" src="../js/func_common.js"></script>
	<style type="text/css">
	    input { vertical-align:middle; }
	    input.inputData { width:90%; }
	</style>
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<%
sSQL="SELECT * FROM tblCompany WHERE ID=" & request("companyID")
set rsData = oledbData.Execute(sSQL)
if not rsData.EOF then
	sSQL="SELECT CountryID AS ID, Name AS CountryName FROM [List].[CountryList] ORDER BY Name"
	set rsData4 = oledbData.execute(sSQL)
	sSQL="SELECT ID, pc_Code FROM tblParentCompany ORDER BY pc_IsDefault DESC, pc_Code"
	set rsData8 = oledbData.Execute(sSQL)
	sLoginURL = session("TempOpenURL")
	If trim(COMPANY_NAME_1) = "Netpay" then
		sLoginURL = sLoginURL & "site_heb/"
	Elseif trim(COMPANY_NAME_1) = "AsTech" then
		sLoginURL = sLoginURL & "site_eng/"
	End if
	%>
	<table align="center" border="0" cellpadding="0" cellspacing="2" dir="ltr" width="95%">
	<tr>
		<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" dir="ltr" width="100%">
			<!--#include file="Merchant_dataHeaderInc.asp"-->
			<%
			pageSecurityLevel = 0
			pageSecurityLang = "ENG"
			pageClosingTags = "</table></td></tr></table>"
			Call AccessLevelCheck(pageSecurityLevel, pageClosingTags, false)
			%>
			<tr><td height="20"></td></tr>
			<!--#include file="Merchant_dataMenuInc.asp"-->
			</table>
		</td>
	</tr>
	</table>
	<form action="merchant_dataOptions.asp" name="frmCoopDetail" method="post">
	<input type="hidden" name="CompanyID" value="<%= nCompanyID %>"/>
	<input type="hidden" name="Action" value="update"/>
	<table align="center" border="0" cellpadding="0" cellspacing="2" dir="ltr" width="95%">
	<tr><td height="10"></td></tr>
	<tr>
		<td valign="top">
			<table align="center" border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
			<tr>
				<td class="MerchantSubHead">General<br /></td>
			</tr>
			<tr><td height="5"></td></tr>
			<tr>
				<td valign="top">
					<input type="Checkbox" name="IsAutoPersonalSignup" <% if rsData("IsAutoPersonalSignup") then %>checked<% End If %> value="1"/>
					Customers can signup remotely<br />
					<input type="Checkbox" name="IsCcStorage" <% if rsData("IsCcStorage") then %>checked<% End If %> value="1"/>
					Allow to store credit cards<br/>
					<input type="Checkbox" name="IsTerminalProbLog" <% if rsData("IsTerminalProbLog") then %>checked<% End If %> value="1"/>
					Show "Terminal Problems" report<br />
					<input type="Checkbox" name="IsConnectionProbLog" <% if rsData("IsConnectionProbLog") then %>checked<% End If %> value="1"/>
					Show "Communication Problems" report<br />
					<input type="Checkbox" name="IsTransLookup" <% if rsData("IsTransLookup") then %>checked<% End If %> value="1"/>
					Show transaction details when clarifying<br/>
					<input type="Checkbox" name="IsSendUserConfirmationEmail" <% if rsData("IsSendUserConfirmationEmail") then %>checked<% End If %> value="1"/>
					Notify customer about successful transaction<br/>
					<input type="Checkbox" name="IsAllowMakePayments" <% if rsData("IsAllowMakePayments") then %>checked<% End If %> value="1"/>
					Allow payment orders to employees/vendors<br/>
					<input type="Checkbox" name="IsAllowBatchFiles" <% if rsData("IsAllowBatchFiles") then %>checked<% End If %> value="1"/>
					Can upload Excel worksheets to transactions<br/>
					<input type="Checkbox" name="IsInterestedInNewsletter" <% if rsData("IsInterestedInNewsletter") then %>checked<% End If %> value="1"/>
					Mailing - Agree to receive newsletters<br/>
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td class="MerchantSubHead">Charging<br/></td>
			</tr>
			<tr>
				<td valign="top">
					<input type="Checkbox" name="IsCcStorageCharge" <% if rsData("IsCcStorageCharge") then %>checked<% End If %> value="1"/>
					Allow to charge a stored credit cards<br/>
					<input type="Checkbox" name="IsApprovalOnly" <% if rsData("IsApprovalOnly") then %>checked<% End If %> value="1"/>
					Allow to process an authorization only transactions<br />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="Checkbox" <% if rsData("AutoCaptureHours")>0 then %>checked<% End If %> onclick="this.form.AutoCaptureHours.disabled = !this.checked;if (!this.form.AutoCaptureHours.disabled) this.form.AutoCaptureHours.focus();"/>
					Capture automatically after <input name="AutoCaptureHours" value="<%=IIf(rsData("AutoCaptureHours")=0, "", rsData("AutoCaptureHours"))%>" <% if rsData("AutoCaptureHours") = 0 then response.write "disabled" %> class="inputData" style="width:40px;" /> hours<br />
					<input type="Checkbox" name="IsConfirmation" <% if rsData("IsConfirmation") then %>checked<% End If %> value="1" />
					Allow to charge an approved transaction<br />
					<%
						If rsData("IsAllowRecurring") Then
							sChecked="checked=""checked"""
							sDisabled=""
							sChecked2=IIf(rsData("IsAllowRecurringFromTransPass"), "checked=""checked""", "")
						Else
							sChecked=""
							sDisabled="disabled=""disabled"""
							sChecked2=""
						End If
					%>
					<script language="javascript" type="text/javascript">
						function ResetMultiChargeProtection(chkAllow)
						{
							chkAllow.form.MultiChargeProtectionMins.disabled = !chkAllow.checked;
							if (chkAllow.form.MultiChargeProtectionMins.value == "") chkAllow.form.MultiChargeProtectionMins.value = 5;
						}
					</script>
					<input type="Checkbox" id="chkMultiChargeProtection" <% if rsData("MultiChargeProtectionMins") > 0 then response.write "checked" %> value="1" onclick="ResetMultiChargeProtection(this);" />
					Multiple charge protection:
					<input name="MultiChargeProtectionMins" <%=IIf(rsData("MultiChargeProtectionMins")>0, "value=""" & rsData("MultiChargeProtectionMins") & """", "disabled")%> class="inputData" style="width:40px;" />
					minutes
					<% showDivBox "help", "ltr", "300", "Second transaction with same details will fail if sent in less then this value of minutes</li></ul>" %><br />
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td class="MerchantSubHead">Recurring<br/></td>
			</tr>
			<tr>
				<td>
					<%
						Set rsRecurring = oledbData.Execute("SELECT * FROM tblMerchantRecurringSettings WHERE MerchantID=" & nCompanyID)
						If rsRecurring.EOF Then
							sIsEnabled=""
							sIsEnabledFromTransPass=""
							sIsEnabledModify=""
							sMaxCharges=""
							sMaxStages=""
							sMaxYears=""
						Else
							sIsEnabled=IIf(rsRecurring("IsEnabled"), "checked", "")
							sIsEnabledFromTransPass=IIf(rsRecurring("IsEnabledFromTransPass"), "checked", "")
							sIsEnabledModify=IIf(rsRecurring("IsEnabledModify"), "checked", "")
							sMaxCharges=IIf(rsRecurring("MaxCharges")>0, rsRecurring("MaxCharges"), "")
							sMaxStages=IIF(rsRecurring("MaxStages")>0, rsRecurring("MaxStages"), "")
							sMaxYears=IIF(rsRecurring("MaxYears")>0, rsRecurring("MaxYears"), "")
						End If
					%>
					<input type="Checkbox" name="RecurringIsEnabled" <%=sIsEnabled%> value="1" />
					Allow recurring transactions<br/>
					&nbsp;&nbsp;&nbsp;&nbsp; Limit recurring series to
					<input name="RecurringMaxCharges" value="<%=sMaxCharges%>" class="inputData" style="width:40px;" />
					overall charges<br />
					&nbsp;&nbsp;&nbsp;&nbsp; Limit recurring series to
					<input name="RecurringMaxStages" value="<%=sMaxStages%>" class="inputData" style="width:40px;" />
					stages<br />
					&nbsp;&nbsp;&nbsp;&nbsp; Limit recurring series to
					<input name="RecurringMaxYears" value="<%=sMaxYears%>" class="inputData" style="width:40px;" />
					years<br />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="Checkbox" name="RecurringIsEnabledFromTransPass" <%=sIsEnabledFromTransPass%> value="1" />
					Allow creating recurring series from existing passed transaction<br />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="Checkbox" name="RecurringIsEnabledModify" <%=sIsEnabledModify%> value="1" />
					Allow modifying recurring series remotely
				</td>
			</tr>
			</table>
		</td>
		<td valign="top">
			<table align="center" border="0" cellpadding="1" cellspacing="0" dir="ltr" width="100%">
			<tr>
				<td class="MerchantSubHead">Notifications<br/></td>
			</tr>
			<tr>
				<td valign="top">
					Email address(es) to notify
					<% showDivBox "help","ltr", "323", "Multiple addresses must be separated with semicolons, without spacing before or after the separator." %>
					<input name="PassNotifyEmail" value="<%=rsData("PassNotifyEmail")%>" class="inputData" style="width:150px;" /><br />
					<input type="Checkbox" name="IsMerchantNotifiedOnPass" <% if rsData("IsMerchantNotifiedOnPass") then %>checked<% End If %> value="1"/>
					Notify merchant about successful transaction<br/>
					<input type="Checkbox" name="IsMerchantNotifiedOnFail" <% if rsData("IsMerchantNotifiedOnFail") then %>checked<% End If %> value="1"/>
					Notify merchant about decliend transaction<br/>
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td class="MerchantSubHead">Data Pulling<br/></td>
			</tr>
			<tr>
				<td valign="top">
					<input type="Checkbox" name="IsAllowRemotePull" <% if rsData("IsAllowRemotePull") then %>checked<% End If %> value="1"/>
					Allow remote data pulling services<br/>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					IP Address List &nbsp;
					<input name="RemotePullIPs" class="inputData" style="width:200px;" value="<%=rsData("RemotePullIPs")%>" size="30" />
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td class="MerchantSubHead">Refunding</td>
			</tr>
			<tr>
				<td valign="top">
					<input type="Checkbox" name="IsRefund" <% if rsData("IsRefund") then %>checked<% End If %> value="1"/>
					Allow to process a refund<br />
					<input type="Checkbox" name="IsAskRefund" <% if rsData("IsAskRefund") then %>checked<% End If %> value="1"/>
					Allow to request a refund<br />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="Checkbox" name="IsAskRefundRemote" <% if rsData("IsAskRefundRemote") then %>checked<% End If %> value="1"/>
					Allow to request a refund remotely<br />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					IP Address List &nbsp;
					<input class="inputData" style="width:200px;" name="RemoteRefundRequestIPs" value="<%=rsData("RemoteRefundRequestIPs")%>" size="30" />
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr><td class="MerchantSubHead">Invoice Service<br/></td></tr>
			<tr>
				<td valign="top">
					<input type="checkbox" name="IsInvoiceEnabled" value="1" <%=IIF(GetInvoiceSettings("IsEnable"), " checked", "")%> onclick="document.getElementById('tblInvoice').style.display=(this.checked?'':'');" />
					Allow Invoice Service
					<% showDivBox "help","ltr", "323", "Currently Invoice4u & Tamal is the only supported invoice provider." %>
					<table id="tblInvoice" border="0" cellpadding="1" cellspacing="0" style="display:<%= IIF(GetInvoiceSettings("IsEnable"), "", "") %>;">
						<tr>
							<td rowspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td>Service Provider</td>
							<td>
                              <select name="ExternalProviderID" class="inputData" style="width:200px;">
                                <option value="1"<%=IIF(GetInvoiceSettings("ExternalProviderID") = 1, " selected" ,"") %>>Invoice4u
                                <option value="2"<%=IIF(GetInvoiceSettings("ExternalProviderID") = 2, " selected" ,"") %>>Tamal
                                <option value="3"<%=IIF(GetInvoiceSettings("ExternalProviderID") = 3, " selected" ,"") %>>iCount
                              </select>
                            </td>
						</tr>
						<tr>
							<td>Username</td>
							<td><input name="InvoiceExternalUseName" value="<%=GetInvoiceSettings("ExternalUseName")%>" class="inputData" style="width:200px;" /></td>
						</tr>
						<tr>
							<td>User ID</td>
							<td><input name="InvoiceExternalUserID" value="<%=GetInvoiceSettings("ExternalUserID")%>" class="inputData" style="width:200px;" /></td>
						</tr>
						<tr>
							<td>Password</td>
							<td><input name="InvoiceExternalPassword" value="<%=GetInvoiceSettings("ExternalPassword")%>" class="inputData" style="width:200px;" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<%Call CloseInvoiceSettings() %>
			</table>
		</td>
	</tr>
	<tr><td><br /></td></tr>
	<tr><td colspan="2" bgcolor="#A9A7A2" height="1"></td></tr>
	<tr><td colspan="2" height="6"></td></tr>
	<tr><td colspan="2" align="right"><input type="submit" value="Save Changes" style="width:110px; height:auto; background-color:#f1f1f1;"></td></tr>
	</table>
	</form>
	<%
end if
%>
</body>
<%
rsData.close
Set rsData = nothing
call closeConnection()
%>
</html>