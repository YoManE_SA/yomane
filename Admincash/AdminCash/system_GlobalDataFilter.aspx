<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<script runat="server">
	'
	' 20100209 Tamir
	'
	' Global data management
	'

	Sub Page_Load()
		Security.CheckPermission(lblPermissions)
		dsGroups.ConnectionString = dbPages.DSN()
		'If Not Page.IsPostBack Then litGroup.Text = dbPages.ExecScalar("SELECT IsNull(GD_Text, '') FROM GetGlobalData(0) WHERE GD_ID=0")
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Global Data</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/dotnet.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body class="menu">
	<form id="frmServer" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Global Data Managment</h1>
		<br />
		<asp:Repeater DataSourceID="dsGroups" runat="server">
			<ItemTemplate>
				<div>
					<asp:HyperLink NavigateUrl='<%# "system_GlobalData.aspx?Group=" & Eval("GD_ID") %>' Text='<%# Eval("GD_Text") %>'
					 Target="frmBody" CssClass="menu" runat="server" />
				</div>
			</ItemTemplate>
		</asp:Repeater><%-- WHERE GD_Color IS NOT NULL--%>
		<asp:SqlDataSource ID="dsGroups" SelectCommand="SELECT GD_ID, GD_Text FROM GetGlobalData(0)" ProviderName="System.Data.SqlClient" runat="server" />
	</form>
</body>
</html>