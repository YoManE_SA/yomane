﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.CommonTypes;
using Netpay.Infrastructure;

public partial class AdminCash_ApplicationIdentity_data : System.Web.UI.Page
{
	protected int? Id { get { return IsPostBack ? ltID.Text.ToNullableInt() : Request["ID"].ToNullableInt(); } }
	protected Netpay.Bll.ApplicationIdentity Item;
    protected void Page_Load(object sender, EventArgs e)
    {
		if(!IsPostBack)
		{
			cblCurrencies.DataSource = Netpay.Bll.Currency.Cache;
			cblPaymentMethods.DataSource = Netpay.Bll.PaymentMethods.PaymentMethod.Cache;
            cblOwnPaymentMethods.DataSource = Netpay.Bll.PaymentMethods.PaymentMethod.Cache;
			cblMerchantGroups.DataSource = Netpay.Bll.Merchants.Group.Cache;
            //cblAffiliates.DataSource = Netpay.Bll.Affiliates.Affiliate.Search(null, null);
            cblAffiliates.DataSource = Netpay.Bll.Affiliates.Affiliate.GetAffiliatesList();


            Item = Netpay.Bll.ApplicationIdentity.Load(Id.GetValueOrDefault());
			if (Item == null) Item = new Netpay.Bll.ApplicationIdentity();
			rptTokens.DataSource = Item.GetTokens();
			DataBind();

			foreach(var c in Item.SupportedCurrencies) {
				var li = cblCurrencies.Items.FindByValue(c);
				if (li != null) li.Selected = true;
			}

			foreach (var c in Item.SupportedPaymentMethods) {
				var li = cblPaymentMethods.Items.FindByValue(((int)c).ToString());
				if (li != null) li.Selected = true;
			}

			foreach (var c in Item.OwnedPaymentMethods) {
                var li = cblOwnPaymentMethods.Items.FindByValue(((int)c).ToString());
				if (li != null) li.Selected = true;
			}

			foreach (var c in Item.RelatedMerchantGroups)
			{
				var li = cblMerchantGroups.Items.FindByValue(((int)c).ToString());
				if (li != null) li.Selected = true;
			}

			foreach (var c in Item.Affiliates)
			{
                var li = cblAffiliates.Items.FindByValue(((int)c).ToString());
				if (li != null) li.Selected = true;
			}
            LamdaLoad();
        }
    }

    protected void Delete_Click(object sender, EventArgs e)
	{
		Item = Netpay.Bll.ApplicationIdentity.Load(Id.GetValueOrDefault());
		if (Item == null) return;
		Item.Delete();
		Visible = false;
	}

    protected void Save_Click(object sender, EventArgs e)
	{
		Item = Netpay.Bll.ApplicationIdentity.Load(Id.GetValueOrDefault());
		if (Item == null) Item = new Netpay.Bll.ApplicationIdentity();

		Item.Name = txtName.Text;
		Item.BrandName = txtBrandName.Text;
		Item.CompanyName = txtCompanyName.Text;
		Item.DomainName = txtDomainName.Text;
		Item.ThemeName = txtThemeName.Text;
		Item.ContentFolder = txtContentFolder.Text;
		Item.URLDevCenter = txtURLDevCenter.Text;
		Item.URLProcess = txtURLProcess.Text;
		Item.URLMerchantCP = txtURLMerchantCP.Text;
		Item.URLWallet = txtURLWallet.Text;
		Item.URLWebsite = txtURLWebsite.Text;
		Item.SmtpServer = txtSmtpServer.Text;
		Item.SmtpUsername = txtSmtpUsername.Text;
		Item.SmtpPassword = txtSmtpPassword.Text;
		Item.EmailFrom = txtEmailFrom.Text;
		Item.EmailContactTo = txtEmailContactTo.Text;
		Item.CopyRightText = txtCopyRightText.Text;
		Item.IsActive = chkIsActive.Checked;
        Item.EnableTokenHash = chkEnableTokenHash.Checked;
        Item.WireAccountID = ddlWireAccountID.Value.ToNullableInt();
        Item.ProcessMerchantNumber = txtProcessMerchantNumber.Text;

        Item.Save();
        ltID.Text = Item.ID.ToString();
        lblTokenHash.Text = Item.HashKey;

        var curList = new List<string>();
		foreach(ListItem i in cblCurrencies.Items)
			if (i.Selected) curList.Add(i.Value);
		Item.SupportedCurrencies = curList;

		var pmList = new List<int>();
		foreach (ListItem i in cblPaymentMethods.Items)
			if (i.Selected) pmList.Add(i.Value.ToNullableInt().GetValueOrDefault());
		Item.SupportedPaymentMethods = pmList;

		pmList = new List<int>();
        foreach (ListItem i in cblOwnPaymentMethods.Items) {
            var pmId = i.Value.ToNullableInt().GetValueOrDefault();
            if (i.Selected && Netpay.Bll.ApplicationIdentity.GetPaymentMethodsOwner((int)pmId).GetValueOrDefault(Item.ID) != Item.ID)
                throw new Exception("can't set owned payment method if the payment method already attached to another app identity, selected pm: " + pmId);
            if (i.Selected) pmList.Add(pmId);
        }
        Item.OwnedPaymentMethods = pmList;

		var mgList = new List<int>();
		foreach (ListItem i in cblMerchantGroups.Items)
			if (i.Selected) mgList.Add(i.Value.ToNullableInt().GetValueOrDefault());
		Item.RelatedMerchantGroups = mgList;

        var afList = new List<int>();
        foreach (ListItem i in cblAffiliates.Items)
            if (i.Selected) afList.Add(i.Value.ToNullableInt().GetValueOrDefault());
        Item.Affiliates = afList;
        LamdaSave();
    }

    protected void AddToken_Click(object sender, EventArgs e)
	{
		if (string.IsNullOrEmpty(newTokenName.Text)) return;
		Item = Netpay.Bll.ApplicationIdentity.Load(Id.GetValueOrDefault());
		if (Item == null) return;
        Item.AddToken(newTokenName.Text, newToekn.Text.ToNullableGuid());
		rptTokens.DataSource = Item.GetTokens();
		rptTokens.DataBind();
	}

	protected void RemoveToken_Click(object sender, CommandEventArgs e)
	{
		Item = Netpay.Bll.ApplicationIdentity.Load(Id.GetValueOrDefault());
		if (Item == null) return;
		Item.RemoveToken(e.CommandArgument.ToNullableGuid().GetValueOrDefault());
		rptTokens.DataSource = Item.GetTokens();
		rptTokens.DataBind();
	}

    protected void LamdaLoad()
    {
        Netpay.Bll.Accounts.Balance.BalanceStatus MerchantBalance = null;
        Netpay.Bll.Accounts.ExternalServiceLogin ServiceLogin = null;
        var merchant = Netpay.Bll.Accounts.Account.LoadAccount(Netpay.Bll.Accounts.AccountType.Merchant, Item.ProcessMerchantNumber);
        if (merchant != null && merchant.MerchantID.HasValue)
        {
            ServiceLogin = Netpay.Bll.Accounts.ExternalServiceLogin.LoadForAccount(merchant.AccountID, Netpay.Bll.ThirdParty.Lamda.PM_ProviderName);
            if (ServiceLogin != null)
            {
                ltLamdaRequestError.Text = "";
                try { MerchantBalance = Netpay.Bll.ThirdParty.Lamda.Current.GetMerchantBalance(merchant.MerchantID.Value); }
                catch (Exception ex) { ltLamdaRequestError.Text = "Error: " + ex.Message; }
            }
        }
        if (MerchantBalance == null) MerchantBalance = new Netpay.Bll.Accounts.Balance.BalanceStatus();
        if (ServiceLogin == null) ServiceLogin = new Netpay.Bll.Accounts.ExternalServiceLogin(0, "");

        txtLamdaUserName.Text = ServiceLogin.Username;
        txtLamdaUserID.Text = ServiceLogin.Reference1;
        txtLamdaPassword.Text = ServiceLogin.Password;

        ltLamdaBalance.Text = MerchantBalance.Current.ToString();
        ltLamdaBalanceIso.Text = MerchantBalance.CurrencyIso;
    }

    protected void LamdaSave()
    {
        Netpay.Bll.Accounts.ExternalServiceLogin ServiceLogin = null;
        var merchant = Netpay.Bll.Accounts.Account.LoadAccount(Netpay.Bll.Accounts.AccountType.Merchant, Item.ProcessMerchantNumber);
        if (merchant != null && merchant.MerchantID.HasValue)
            ServiceLogin = Netpay.Bll.Accounts.ExternalServiceLogin.LoadForAccount(merchant.AccountID, Netpay.Bll.ThirdParty.Lamda.PM_ProviderName);
        if (ServiceLogin == null) {
            if (string.IsNullOrEmpty(ServiceLogin.Username)) return;
            ServiceLogin = new Netpay.Bll.Accounts.ExternalServiceLogin(0, "");
        }
        ServiceLogin.Username = txtLamdaUserName.Text;
        ServiceLogin.Reference1 = txtLamdaUserID.Text;
        ServiceLogin.Password = txtLamdaPassword.Text;
        ServiceLogin.Save();
    }

}