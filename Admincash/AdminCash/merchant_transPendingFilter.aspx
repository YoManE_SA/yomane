<%@ Page Language="VB" MasterPageFile="~/AdminCash/FiltersMaster.master" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Register Src="Filter_DateTimeRange.ascx" TagName="FilterDateTimeRange" TagPrefix="Uc1" %>
<%@ Register Src="Filter_Merchants.ascx" TagName="FilterMerchants" TagPrefix="Uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" type="text/css" media="all" href="../StyleSheet/calendar-blue.css" title="winter" />
	<script type="text/javascript" src="../js/func_common.js"></script>
	<script type="text/javascript" src="../js/calendar.js"></script>
	<script type="text/javascript" src="../js/calendar-en.js"></script>
	<script type="text/javascript" src="../js/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript">
		function OnClientSelectedIndexChangingHandler(item) {
	        frmSearchWire.CompanyID.value = item.Value;
	    }
	</script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

	<form runat="server" action="merchant_transPending.asp" name="frmSearchWire" id="frmRefundAsk" target="frmBody" method="get" onsubmit="return filterForm_Submit(document.getElementById('frmRefundAsk'))">
	<asp:ScriptManager ID="ScriptManager1" AjaxFrameworkMode="Enabled" runat="server" />

	<input type="hidden" id="Source" name="Source" value="filter" />
	<input type="hidden" id="CompanyID" name="CompanyID" />
	
	<h1><asp:Label ID="lblPermissions" runat="server" /> Pending Trans<span> - Filter</span></h1>

	<table class="filterNormal">
	<tr>
		<td>
			<Uc1:FilterMerchants id="FMerchants" ClientField="CompanyID" runat="server" />
		</td>
	</tr>
	</table>
	<table class="filterNormal">
		<tr><td><Uc1:FilterDateTimeRange ID="fdtrControl" runat="server" /></td></tr>	
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<th>Cc/Account #<br /></th>
			<td rowspan="2" width="6"></td>
			<th>Name<br /></th>
		</tr>
		<tr>
			<td><input type="Text" size="11" maxlength="4" name="CcLast4Num" value="" onkeydown="return IsKeyDigit();"></td>
			<td><input type="Text" size="14" name="ccHolderName" value=""></td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Amount</th></tr>
		<tr>
			<td>
				<input type="text" size="8" name="AmountMin" value="" onkeydown="return IsKeyDigit();" /> To
				<input type="text" size="8" name="AmountMax" value="" onkeydown="return IsKeyDigit();" />
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Payment Method</th></tr>
		<tr>
			<td>
				<select name="ccType">
					<option value="" selected="selected">
					<%
						Dim iReader2 As SqlDataReader = dbPages.ExecReader("SELECT PaymentMethod_id AS 'ID', name AS 'pm_Name' FROM List.PaymentMethod ORDER BY ID")
						While iReader2.Read()
							Response.Write("<option value=""" & iReader2(0) & """>" & iReader2(1) & "</option>")
						End While
						iReader2.Close()
					%>
				</select>
			</td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr><th>Paging</th></tr>
		<tr>
			<td align="right" class="txt12">
				<select name="PageSize">
					<option value="20" selected="selected">20 Rows</option>
					<option value="50">50 Rows</option>
					<option value="75">75 Rows</option>
					<option value="100">100 Rows</option>
				</select>
			</td>
			<td align="right"><input type="submit" name="Action" value="SEARCH"><br /></td>
		</tr>
	</table>
	<table class="filterNormal" style="padding-top:5px;">
		<tr>
			<td align="center"><input class="medium grayBG" id="passValue" style="width:60px;" name="companyTransPending_id" /></td>
			<td>
				<select id="filter_select" onchange="document.getElementById('passValue').name=options[selectedIndex].value;">
					<option value="companyTransPending_id">Trans ID</option>
					<option value="DebitReferenceCode">Debit Ref.</option>
					<option value="DebitApprovalNumber">Debit Approval #</option>
				</select>
			</td>
			<td align="right"><input type="submit" class="buttonFilter" value="Show" id="Show" /></td>
		</tr>
	</table>
	</form>
</asp:Content>