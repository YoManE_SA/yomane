<%@ Page Language="VB" EnableEventValidation="false" EnableViewStateMac="false" %>
<%@ Register Src="~/Include/Paging.ascx" TagPrefix="UC" TagName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
	Const RGB_ROUTINE As String = "#AAAAAA", RGB_NOTIFY As String = "#6699CC"
	Const RGB_BLOCK As String = "#FF6666", RGB_UNBLOCK As String = "#66CC66"
	Dim sSQL As String, sQueryWhere As String, nFromDate As String, nToDate As String, sDateDiff As String
	Dim sStatusColor As String, sAnd As String, sQueryString As String, sTmpTxt As String, sDivider As String
	Dim i As Integer, nTmpNum As Integer

	Function DisplayContent(sVar As String) As String
		DisplayContent = "<span class=""key"">" &replace(replace(dbPages.dbtextShow(sVar),"=","</span> = "),"|"," <br /><span class=""key"">")
	End Function
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)

		nFromDate = Trim(Request("fromDate")) & " " & Trim(Request("fromTime"))
		If nFromDate <> " " Then
			sQueryWhere = sQueryWhere & sAnd & "dbl_Date >= '" & nFromDate.ToString() & "'"
			sAnd = " AND "
		End If

		nToDate = Trim(Request("toDate")) & " " & Trim(Request("toTime"))
		If nToDate <> " " Then
			sQueryWhere = sQueryWhere & sAnd & "dbl_Date <= '" & nToDate.ToString() & "'"
			sAnd = " AND "
		End If

		If dbPages.TestVar(Request("ShowChecks"), 0, 1, 0) = 0 Then
			sQueryWhere = sQueryWhere & sAnd & "dbl_Type NOT IN ('BLOCK CHECK END', 'BLOCK CHECK', 'RULE CHECK END', 'RULE CHECK', 'MONTHLY MAIL UNSENT', 'MONTHLY SMS UNSENT')"
			sAnd = " AND "
		End If

		If Trim(Request("Type")) <> "" Then
			sQueryWhere = sQueryWhere & sAnd & "dbl_Type='" & dbPages.DBText(Request("Type")) & "'"
			sAnd = " AND "
		End If

		If Trim(Request("DebitCompany")) <> "" Then
			sQueryWhere = sQueryWhere & sAnd & "dbl_DebitCompany=" & Request("DebitCompany")
			sAnd = " AND "
		End If

		If Security.IsLimitedDebitCompany Then
			sQueryWhere = sQueryWhere & sAnd & "dbl_DebitCompany IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
			sAnd = " AND "
		End If

		If Trim(Request("Rule")) <> "" Then
			sQueryWhere = sQueryWhere & sAnd & "dbl_DebitRule=" & Request("Rule")
			sAnd = " AND "
		End If

		If Trim(Request("RuleType")) <> "" Then
			If Request("RuleType") = "Warning" Then
				sQueryWhere = sQueryWhere & sAnd & "dr_IsAutoDisable=0"
				sAnd = " AND "
			ElseIf Request("RuleType") = "Block" Then
				sQueryWhere = sQueryWhere & sAnd & "dr_IsAutoDisable=1"
				sAnd = " AND "
			ElseIf Request("RuleType") = "---" Then
				sQueryWhere = sQueryWhere & sAnd & "dr_IsAutoDisable IS NULL"
				sAnd = " AND "
			ElseIf Request("RuleType") = "CHB" Then
				sQueryWhere = sQueryWhere & sAnd & "dbl_Type LIKE 'MONTHLY %'"
				sAnd = " AND "
			End If
		End If

		If Trim(Request("FreeText")) <> "" Then
			sQueryWhere = sQueryWhere & sAnd & "dbl_Text LIKE '%" & dbPages.DBText(Request("FreeText")) & "%'"
			sAnd = " AND "
		End If

		PGData.PageSize = IIf(dbPages.TestVar(Request("PageSize"), 1, 0, 0) > 0, Request("PageSize"), 20)
		sQueryString = dbPages.CleanUrl(Request.QueryString)
		If sQueryWhere <> "" Then sQueryWhere = " WHERE " & sQueryWhere
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link  href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dataHeading { text-decoration:underline; white-space:nowrap; }
	</style>
	<script language="JavaScript">
		function IconVisibility(fVar) {
		
			spanObj = document.getElementById(fVar)
			if (spanObj.style.visibility == 'hidden') {
				spanObj.style.visibility = 'visible';
			}
			else {
				spanObj.style.visibility = 'hidden';
			}
		}
		function ExpandNode(fTrNum) {
			trObj = document.getElementById("trInfo" + fTrNum)
			imgObj = document.getElementById("oListImg" + fTrNum)
			if (trObj) {
				if (trObj.style.display == '') {
					imgObj.src = '../images/tree_expand.gif';	
					trObj.style.display = 'none';
				}
				else {
					imgObj.src = '../images/tree_collapse.gif';	
					trObj.style.display = '';
				}
			}
		}
	</script>
</head>
<body>

	<form id="Form1" runat="server">
		<table align="center" style="width:92%" border="0">
		<tr>
			<td id="pageMainHeading">
				<asp:label ID="lblPermissions" runat="server" /> RULES &amp; NOTIFICATIONS LOG
				<span style="color:Black;font-weight:normal;"><%=IIf(Request("From") = "Top", " - Last Actions", String.Empty)%></span>
				<br /><br />
			</td>
			<td style="font-size:11px;text-align:right;vertical-align:top;">
				&nbsp; &nbsp; <span style="background-color:<%= RGB_ROUTINE %>;">&nbsp;&nbsp;</span> Routine
				&nbsp; &nbsp; <span style="background-color:<%= RGB_BLOCK %>;">&nbsp;&nbsp;</span> Block
				&nbsp; &nbsp; <span style="background-color:<%= RGB_UNBLOCK %>;">&nbsp;&nbsp;</span> Unblock
				&nbsp; &nbsp; <span style="background-color:<%= RGB_NOTIFY %>;">&nbsp;&nbsp;</span> Notification
			</td>
		</tr>
		<tr>
			<td style="font-size:11px; padding-bottom:4px;">
				(<a onclick="ExpandAll('','collapse');">Expand All</a> / <a onclick="ExpandAll('none','expand');">Collapse All</a>)
			</td>
			<td align="right" style="font-size:11px;">
				Filters:
				<%
				If Trim(Request("Type")) <> "" Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "Type", Nothing) & "';"">Type</a>")
					sDivider = ", "
				End If
				If Trim(Request("DebitCompany")) <> "" Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "DebitCompany", Nothing) & "';"">Debit Company</a>")
					sDivider = ", "
				End If
				If Trim(Request("RuleType")) <> "" Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "RuleType", Nothing) & "';"">Rule Type</a>")
					sDivider = ", "
				End If
				If Trim(Request("Rule")) <> "" Then
					Response.Write(sDivider & "<a onclick=""location.href='?" & dbPages.SetUrlValue(sQueryString, "Rule", Nothing) & "';"">Rule No.</a>")
					sDivider = ", "
				End If
				Response.Write(IIf(sDivider = String.Empty, "Add to filter by clicking on returned fields", " | (click to remove)"))
				%>
			</td>
		</tr>
		</table>
		<table class="formNormal" align="center" style="width:92%">
		<tr>
			<th colspan="2" style="width:30px;"><br /></th>
			<th>ID</th>
			<th>Date &amp; Time</th>
			<th>Debit Company</th>
			<th>Rule Type</th>
			<th>Rule No.</th>
			<th>Record Type</th>
		</tr>
		<%
		sSQL = "SELECT" & IIf(Request("From") = "Top", " TOP 20", String.Empty) & _
		" tblDebitBlockLog.ID, dbl_Date, dbl_DebitCompany, dbl_DebitRule, dbl_Type, dbl_Text," & _
		" IsNull(dc_name, '---') DebitCompanyName," & _
		" CASE dr_IsAutoDisable WHEN 0 THEN 'Warning' WHEN 1 THEN 'Block' ELSE '---' END RuleType" & _
		" FROM tblDebitBlockLog" & _
		" LEFT JOIN tblDebitCompany ON dbl_DebitCompany=DebitCompany_ID" & _
		" LEFT JOIN tblDebitRule ON dbl_DebitRule=tblDebitRule.ID" & _
		" " & sQueryWhere & " ORDER BY tblDebitBlockLog.ID DESC"
		PGData.OpenDataset(sSQL)
		Dim sType As String
		While PGData.Read()
			i = i + 1
			sType = PGData("dbl_Type").ToString
			If sType.Contains(" CHECK") Then
				sStatusColor = RGB_ROUTINE
			ElseIf sType.Contains(" SENT") Then
				sStatusColor = RGB_NOTIFY
			ElseIf sType = "'BLOCK REMOVED'" Or sType = "UNBLOCK ATTEMPT OK" Then
				sStatusColor = RGB_UNBLOCK
			Else
				sStatusColor = RGB_BLOCK
			End If
			%>
			<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
				<td style="text-align:right;"><img onclick="ExpandNode('<%=i%>');" style="cursor:pointer;" id="oListImg<%=i%>" src="../images/tree_expand.gif" alt="" width="16" height="16" border="0" align="middle"><br /></td>
				<td style="text-align:left;"><span style="background-color:<%= sStatusColor %>;">&nbsp;</span></td>
				<td nowrap="nowrap"><%=PGData("ID")%></td>
				<td nowrap="nowrap"><%=CType(PGData("dbl_Date"), DateTime).ToString("dddd, dd/MM/yy HH:mm:ss")%></td>
				<td style="white-space:nowrap;">
					<%
					If Trim(Request("DebitCompany")) <> "" Then
						Response.Write(PGData("DebitCompanyName"))
					Else
						dbPages.showFilter(PGData("DebitCompanyName"), "?" & sQueryString & "&DebitCompany=" & PGData("dbl_DebitCompany"), 1)
					End If
					%>
				</td>
				<td style="white-space:nowrap;">
					<%
					If Trim(Request("RuleType")) <> "" Then
						Response.Write(PGData("RuleType"))
					Else
						dbPages.showFilter(PGData("RuleType"), "?" & sQueryString & "&RuleType=" & PGData("RuleType"), 1)
					End If
					%>
				</td>
				<td style="white-space:nowrap;">
					<%
					If Trim(Request("Rule")) <> "" Then
						Response.Write(IIf(PGData("dbl_DebitRule") = 0, "---", PGData("dbl_DebitRule")))
					Else
						dbPages.showFilter(IIf(PGData("dbl_DebitRule") = 0, "---", PGData("dbl_DebitRule")), "?" & sQueryString & "&Rule=" & PGData("dbl_DebitRule"), 1)
					End If
					%>
				</td>
				<td style="white-space:nowrap;">
					<%
					If Trim(Request("Type")) <> "" Then
						Response.Write(sType)
					Else
						dbPages.showFilter(sType, "?" & sQueryString & "&Type=" & sType, 1)
					End If
					%>
				</td>
			</tr>
			<tr id="trInfo<%=i%>" style="display:none;">
				<td colspan="3"></td>
				<td colspan="5">
					<table class="formNormal" style="width:100%; border-top:1px dashed #e2e2e2;">
					<tr><td height="10"></td></tr>
					<tr><td><%=PGData("dbl_Text")%></td></tr>
					<tr><td height="10"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td height="1" colspan="8"  bgcolor="silver"></td></tr>
			<%
		End While
		PGData.CloseDataset()
		%>
		</table>
		<script language="JavaScript">
			function ExpandAll(fDisplay, fImgStatus) {
				for(i = 0; i <= <%= i %>; i++) {
					trObj = document.getElementById("trInfo" + i)
					imgObj = document.getElementById("oListImg" + i)
					if (trObj) {
						imgObj.src = '../images/tree_' + fImgStatus + '.gif';
						trObj.style.display = fDisplay;
					}
				}
			}
		</script>
		<br />
		<div id="filterIcon" style="visibility:hidden; position:absolute; z-index:1; font-size:10px; color:Gray; background-color:White;">
			<img src="../images/icon_filterPlus.gif" align="middle"> ADD TO FILTER
		</div>
		
	</form>
	
	<table align="center" style="width:92%">
	<tr>
		<td>
			<UC:Paging runat="Server" id="PGData" PageID="PageID" />
		</td>
	</tr>
	</table>
		
</body>
</html>
