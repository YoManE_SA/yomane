<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
sTransID = dbText(request("transID"))
sCompanyID = dbText(request("CompanyID"))
%>
<html>
<head>
	<title><%= COMPANY_NAME_1 %> - Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminIE.css" type="text/css" rel="stylesheet" />
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" class="itext" dir="rtl" onLoad="focus();">
<table border="0" width="100%" cellspacing="0" cellpadding="0" dir="rtl" align="center">
<tr>
	<td style="padding:15px" valign="top">
		<%
		sSQL="SELECT CompanyName FROM tblCompany WHERE ID=" & sCompanyID
		set rsData = oledbData.execute(sSQL)
		if not rsData.EOF then
			sSQL="SELECT " &_
			"dbo.GetDecrypted256(tblCreditCard.CCard_number256) CCard_number, tblCreditCard.ExpYY, tblCreditCard.ExpMM, tblCreditCard.cc_cui, " &_
			"tblCreditCard.Member, tblCreditCard.PersonalNumber, tblCreditCard.PhoneNumber, tblCreditCard.Email, tblCompanyTransPass.*, tblCreditCardType.NameHeb AS ccTypeHebName " &_
			"FROM tblCompanyTransPass LEFT OUTER JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID=tblCreditCard.ID " &_
			"LEFT OUTER JOIN tblCreditCardType ON tblCreditCard.ccTypeID = tblCreditCardType.ID WHERE tblCompanyTransPass.PaymentMethod_id=1 AND tblCompanyTransPass.ID=" & sTransID
			'response.write sSQL & "<br />"
			set rsData = oledbData.execute(sSQL)
			if not rsData.EOF then
				sAmount = rsData("Amount")
				%>
				<table align="right" border="0" cellpadding="1" dir="ltr" cellspacing="2" width="100%">
				<tr>
					<td colspan="2" align="right" class="txt12" dir="ltr"><%= rsData("InsertDate") %><br /></td>
				</tr>
				<tr><td height="8"></td></tr>
				<tr>
					<td width="45%" valign="top">
						<table align="right" border="0" cellpadding="1" dir="ltr" cellspacing="2">
						<%
						If trim(rsData("comment"))<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= dbTextShow(rsData("comment")) %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� �����<br /></td>
							</tr>
							<%
						end if
						
						If rsData("IPAddress")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("IPAddress") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">����� �����<br /></td>
							</tr>
							<%
						end if
						if rsData("TerminalNumber")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("TerminalNumber") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� ����<br /></td>
							</tr>
							<%
						End If
						if rsData("PD")<>"01/01/1900" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("PD") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� �����<br /></td>
							</tr>
							<%
						End If
						%>
						 <tr>
							<td align="right" class="txt12" dir="rtl"><%=  rsData("DebitReferenceCode") %><br /></td>
							<td width="8"></td>
							<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">��� ���� �����<br /></td>
						 </tr>
						</table>
					</td>
					<td valign="top">
						<table align="right" border="0" cellpadding="1" dir="ltr" cellspacing="2">
						<%
						If rsData("OrderNumber")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("OrderNumber") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� �����<br /></td>
							</tr>
							<%
						end if
						%>
						<tr>
							<td align="right" class="txt12" dir="ltr">
								<%= sTransID %><br />
							</td>
							<td width="8"></td>
							<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� ����<br /></td>
						</tr>
						<tr>
							<td align="right" nowrap class="txt12" dir="ltr">
								<%
								sCCardNum = rsData("CCard_number")
								response.write "<span dir=ltr>" & sCCardNum & "</span><br />"
								%>
							</td>
							<td width="8"></td>
							<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� �����<br /></td>
						</tr>
						<tr>
							<td align="right" class="txt12" dir="ltr">
								<%= trim(rsData("ExpMM")) %>/<%= trim(rsData("ExpYY")) %><br />
							</td>
							<td width="8"></td>
							<td align="right" class="txt12b" bgcolor="#e9e9e9" dir="rtl">���� �����<br /></td>
						</tr>
						<tr>
							<td align="right" class="txt12" dir="rtl"><%= rsData("ccTypeHebName") %><br /></td>
							<td width="8"></td>
							<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">��� �����<br /></td>
						</tr>
						<%
						if rsData("cc_cui")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= DecCVV(rsData("cc_cui")) %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">����� ������<br /></td>
							</tr>
							<%
						End If
						if rsData("Member")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("Member") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">�� ��� ������<br /></td>
							</tr>
							<%
						end if
						if rsData("PersonalNumber")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("PersonalNumber") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� �.� �� ��� ������<br /></td>
							</tr>
							<%
						End If
						if rsData("PhoneNumber")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("PhoneNumber") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">����� �� ��� ������<br /></td>
							</tr>
							<%
						End If
						if rsData("Email")<>"" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("Email") %><br /></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">������ �� ��� ������<br /></td>
							</tr>
							<%
						End If
						%>
						<tr>
							<td align="right" class="txt12" dir="ltr">
								<%
								If int(rsData("CreditType"))=0 Then Response.write "<span style=""color:red;"">" & FormatCurr(rsData("Currency"), rsData("Amount")) & "</span>" _
								Else Response.Write(FormatCurr(rsData("Currency"), rsData("Amount")))
								%>
							</td>
							<td width="8"></td>
							<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">����<br /></td>
						</tr>
						<%
						if int(rsData("CreditType"))=8 then
							nPay=int(sAmount/rsData("Payments"))
							nPayFirst=sAmount-nPay*(rsData("Payments")-1)
							if int(rsData("Payments"))>1 then
							%>		
							<tr>
								<td align="right" dir="rtl" class="txt12"><%= rsData("Payments") %><br /></td>
								<td width="8"></td>
								<td align="right" dir="rtl" class="txt12b" bgcolor="#e9e9e9">���� �������<br /></td>
							</tr>
							<tr>
								<td align="right" dir="ltr" class="txt12">
									<%=FormatCurr(rsData("Currency"), nPayFirst)%>
								</td>
								<td width="8"></td>
								<td align="right" dir="rtl" class="txt12b" bgcolor="#e9e9e9">����� �����<br /></td>
							</tr>
							<tr>
								<td align="right" dir="ltr" class="txt12">
									<%=FormatCurr(rsData("Currency"), nPay)%>
								</td>
								<td width="8"></td>
								<td align="right" dir="rtl" class="txt12b" bgcolor="#e9e9e9">��� �������<br /></td>
							</tr>
							<%
							end if
						end if
	
						if trim(rsData("ApprovalNumber"))<>"" AND trim(rsData("ApprovalNumber"))<>"0000000" then
							%>
							<tr>
								<td align="right" class="txt12" dir="rtl"><%= rsData("ApprovalNumber") %></td>
								<td width="8"></td>
								<td align="right" class="txt12b" dir="rtl" bgcolor="#e9e9e9" width="100">���� �����<br /></td>
							</tr>
							<%
						End If
						%>
						</table>
					</td>
				</tr>
				</table>
				<br />
				<%
			end if
		end if
		%>
	</td>
</tr>
</table><br />
<% call closeConnection() %>
</body>
</html>
