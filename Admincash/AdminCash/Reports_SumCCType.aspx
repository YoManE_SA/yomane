<%@ Page Language="VB" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="radC" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Charting" TagPrefix="radC" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="radCln" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Function FormatCurr(ByVal trnCurr As Byte, ByVal trnValue As Decimal, ByVal trnCount As Integer) As String
		Dim tmpString As String = ""
		If trnValue = 0 And trnCount = 0 Then
			tmpString = ""
		ElseIf trnValue < 0 Then
			tmpString = "<span style=""color:red;"">" & dbPages.FormatCurr(trnCurr, trnValue) & "</span>"
			If trnCount <> -1 Then tmpString = tmpString & " <span class=""dim"">(" & trnCount & ")</span>"
		Else
			tmpString = dbPages.FormatCurr(trnCurr, trnValue)
			If trnCount <> -1 Then tmpString = tmpString & " <span class=""dim"">(" & trnCount & ")</span>"
		End If
		FormatCurr = tmpString
	End Function
	
	Public Class RptData
		Public ItemID As Long = -1
		Public ItemName As String
		Public Currency As Byte
		Public TransCount As Long = 0
		Public TransSum As Decimal = 0
		Public RefundCount As Long = 0
		Public RefundSum As Decimal = 0
		Public ChbCount As Long = 0
		Public ChbSum As Decimal = 0
		Public FeeChbSum As Decimal = 0
		Public FeeSum As Decimal = 0
	End Class
	
	Public Enum eSumType
		ePS_Payed = 1
		ePS_Archive = 2
		ePS_Pending = 3
		ePS_Failed = 4
	End Enum
	
	Protected Function LoadStatData(ByVal nFrom As DateTime, ByVal nToDate As DateTime, ByVal SumType As eSumType, ByVal pTotals As RptData, ByVal nCurrencyList As String, ByVal nPaymentMethodList As String, ByVal nCompanyList As String, ByVal nDebitList As String, ByVal nTerminalList As String, ByVal GroupField As String) As System.Collections.Hashtable
		Dim GroupBy As String = "", NameField As String = ""
		Dim sWhere As String = "(tblCompanyTransPass.InsertDate >= '" & nFrom.ToString() & "') AND (tblCompanyTransPass.InsertDate <= '" & nToDate.ToString() & "')"
		If nCurrencyList <> "" Then sWhere = sWhere & " And Currency IN(" & nCurrencyList & ")"
		If nCompanyList <> "" Then sWhere = sWhere & " And companyID IN(" & nCompanyList & ")"
		If nDebitList <> "" Then sWhere = sWhere & " And DebitCompanyID IN(" & nDebitList & ")"
		If nPaymentMethodList <> "" Then sWhere = sWhere & " And PaymentMethod IN(" & nPaymentMethodList & ")"
		If nTerminalList <> "" Then sWhere = sWhere & " And TerminalNumber ='" & nTerminalList & "'"
		'(tblCompany.Closed = 0) AND (tblCompany.Blocked = 0) AND (tblCompany.id NOT IN (12,35))
		Select Case GroupField
			Case "DebitCompanyID"
				NameField = "DebitCompanyID, IsNull(dc_Name, 'Other ('+LTrim(RTrim(Str(DebitCompanyID)))+')')"
				GroupBy = " LEFT JOIN tblDebitCompany ON(tblCompanyTransPass.DebitCompanyID = tblDebitCompany.debitCompany_id) "
			Case "CompanyID"
				NameField = "CompanyID, IsNull(CompanyName, 'Other ('+LTrim(RTrim(Str(CompanyID)))+')')"
				GroupBy = " LEFT JOIN tblCompany ON(tblCompanyTransPass.CompanyID = tblCompany.ID) "
			Case "CustomerID"
				NameField = "CustomerID, IsNull(FirstName + ' ' + LastName, 'Other ('+LTrim(RTrim(Str(CustomerID)))+')')"
				GroupBy = " LEFT JOIN tblCustomer ON(tblCompanyTransPass.CompanyID = tblCustomer.ID) "
			Case "Currency"
				NameField = "Currency, IsNull(CUR_ISOName, 'Other ('+LTrim(RTrim(Str(Currency)))+')')"
                GroupBy = " LEFT JOIN tblSystemCurrencies ON tblCompanyTransPass.Currency=CUR_ID "
			Case "PaymentMethod"
                'NameField = "PaymentMethod, IsNull(pm_Name, 'Other ('+LTrim(RTrim(Str(PaymentMethod)))+')')"
				'GroupBy = " LEFT JOIN tblPaymentMethod ON tblCompanyTransPass.PaymentMethod=tblPaymentMethod.ID "
			Case Else
                NameField = "IsNull(cl.CountryID, 0), IsNull(cl.Name, 'Other ('+LTrim(RTrim(IPCountry))+')')"
                GroupBy = " LEFT JOIN [List].[CountryList] AS cl ON tblCompanyTransPass.IPCountry = cl.CountryISOCode "
		End Select
		Select Case SumType
			Case eSumType.ePS_Failed
			
			Case eSumType.ePS_Archive
				sWhere = sWhere & " AND (tblCompanyTransPass.payID = ';X;') AND (tblCompanyTransPass.isTestOnly = 0) "
			Case eSumType.ePS_Payed
				sWhere = sWhere & " AND (tblCompanyTransPass.payID <> ';X;') AND (tblCompanyTransPass.isTestOnly = 0) "
				', Sum(tblCompanyTransInstallments.Amount) As iAmount
			Case eSumType.ePS_Pending
				sWhere = sWhere & " AND ((tblCompanyTransPass.PrimaryPayedID = 0) OR (tblCompanyTransPass.deniedstatus = 2)) "
		End Select
		Dim Sql As String = "SELECT " & NameField & ", DeniedStatus, CreditType, Count(*) tVal, Sum(tblCompanyTransPass.Amount) tAmount, " & _
		" Sum(netpayFee_transactionCharge + netpayFee_ratioCharge) FeeSum, Sum(netpayFee_ClrfCharge + netpayFee_chbCharge) FeeChb" & _
		" FROM tblCompanyTransPass " & GroupBy & " WHERE " & sWhere & _
		" GROUP BY " & NameField & ", DeniedStatus, CreditType, tblCompanyTransPass.PaymentMethod "
		Dim pData As RptData
		Dim pRet As System.Collections.Hashtable = New System.Collections.Hashtable
		Dim iReader As SqlDataReader = dbPages.ExecReader(Sql)
		While iReader.Read
			Dim cName As Long = IIf(iReader(0) Is DBNull.Value, "", iReader(0))
			If pRet.ContainsKey(cName) Then
				pData = pRet(cName)
			Else
				pData = New RptData
				pData.ItemID = iReader(0)
				pData.ItemName = iReader(1)
				pRet.Add(pData.ItemID, pData)
			End If
			pData.FeeSum -= iReader("FeeSum")
			pData.FeeChbSum -= iReader("FeeChb")
			Select Case iReader("DeniedStatus")
				Case 0 ', 3, 5
					If iReader("CreditType") = "0" Then
						pData.RefundCount = pData.RefundCount + iReader("tVal")
						pData.RefundSum = pData.RefundSum - iReader("tAmount")
					Else
						pData.TransCount = pData.TransCount + iReader("tVal")
						If iReader("CreditType") <> "8" Then pData.TransSum = pData.TransSum + iReader("tAmount")
					End If
				Case 1, 2 ', 4, 6
					pData.TransCount = pData.TransCount + iReader("tVal")
					pData.ChbCount = pData.ChbCount + iReader("tVal")
					pData.ChbSum = pData.ChbSum - iReader("tAmount")
			End Select
			pTotals.FeeChbSum += pData.FeeChbSum
			pTotals.RefundCount += pData.RefundCount : pTotals.RefundSum += pData.RefundSum
			pTotals.ChbCount += pData.ChbCount : pTotals.ChbSum += pData.ChbSum
			pTotals.TransCount += pData.TransCount : pTotals.FeeSum += pData.FeeSum
		End While
		iReader.Close()
		If SumType = 88 Then
			Sql = "Select , Sum(tblCompanyTransInstallments.Amount) From tblCompanyTransInstallments Left Join tblCompanyTransPass ON(tblCompanyTransPass.transAnsID = tblCompanyTransInstallments.ID) Where payID <> 0 Group By CompanyName"
			iReader = dbPages.ExecReader(Sql)
			While iReader.Read
				Dim cName As String = IIf(iReader(0) Is DBNull.Value, "", iReader(0))
				If pRet.ContainsKey(cName) Then
					pData = pRet(cName)
					pData.TransSum = pData.TransSum + iReader(1)
					pTotals.TransSum += pData.TransSum
				End If
			End While
			iReader.Close()
		End If
		Return pRet
	End Function

	Protected Sub drawTotals(ByVal RptList As System.Collections.Hashtable, ByVal pTotals As RptData, ByVal nMinTotalToShow As Decimal)
		Dim nRowCount As Int16
		Response.Write("<table border=""0"" class=""formNormal"" align=""center""><tr>")
		Response.Write("<th>Item</th>")
		Response.Write("<th>Transactions</th>")
		Response.Write("<th>Refunds</th>")
		Response.Write("<th>Chb</th>")
		Response.Write("<th>Chb fees</th>")
		Response.Write("<th>Trans Fees</th>")
		Response.Write("<th>Sub Total</th>")
		Response.Write("</tr>")
		nRowCount = 0
		For Each p As RptData In RptList.Values
			If p.TransSum >= nMinTotalToShow Then
				nRowCount = nRowCount + 1
				If bColorSwitch Then sTrColor = "#e5e5e5" Else sTrColor = ""
				bColorSwitch = Not bColorSwitch
				Dim xSubTotal As Integer = p.TransSum + (p.RefundSum + p.ChbSum + p.FeeChbSum + p.FeeSum)
				Response.Write("<tr style=""background-color:" & sTrColor & ";"" onclick=""if (this.style.backgroundColor=='yellow') {this.style.backgroundColor='" & sTrColor & "';}else {this.style.backgroundColor='yellow';}"">")
				Response.Write("<td>" & dbPages.DBTextShow(p.ItemName) & " &nbsp;</td>")
				Response.Write("<td>" & FormatCurr(p.Currency, p.TransSum, p.TransCount) & "</td>")
				Response.Write("<td>" & FormatCurr(p.Currency, p.RefundSum, p.RefundCount) & "</td>")
				Response.Write("<td>" & FormatCurr(p.Currency, p.ChbSum, p.ChbCount) & "</td>")
				Response.Write("<td>" & FormatCurr(p.Currency, p.FeeChbSum, p.ChbCount) & "</td>")
				Response.Write("<td>" & FormatCurr(p.Currency, p.FeeSum, -1) & "</td>")
				Response.Write("<td>" & FormatCurr(p.Currency, xSubTotal, -1) & "</td>")
				Response.Write("</tr>")
			End If
		Next
		Response.Write("<tr><td colspan=""7"" hight=""1"" bgcolor=""black""></tr>")
		Response.Write("<tr class=""totals""><td>Total:</td>")
		Response.Write("<td>" & dbPages.FormatCurr(pTotals.Currency, pTotals.TransSum) & "</td>")
		Response.Write("<td>" & dbPages.FormatCurr(pTotals.Currency, pTotals.RefundSum) & "</td>")
		Response.Write("<td>" & dbPages.FormatCurr(pTotals.Currency, pTotals.ChbSum) & "</td>")
		Response.Write("<td>" & dbPages.FormatCurr(pTotals.Currency, pTotals.FeeChbSum) & "</td>")
		Response.Write("<td>" & dbPages.FormatCurr(pTotals.Currency, pTotals.FeeSum) & "</td>")
		Response.Write("<td>" & dbPages.FormatCurr(pTotals.Currency, pTotals.TransSum + (pTotals.RefundSum + pTotals.ChbSum + pTotals.FeeChbSum + pTotals.FeeSum)) & "</td>")
		Response.Write("</tr>")
		Response.Write("</table>")
	End Sub
	
    Protected Sub fillGraph(ByVal fGraph As Telerik.Web.UI.RadChart, ByVal RptList As System.Collections.Hashtable, ByVal pTotals As RptData, ByVal nMinTotalToShow As Decimal)
        For Each p As RptData In RptList.Values
            If p.TransSum >= nMinTotalToShow Then
                fGraph.Series(0).AddItem(p.TransSum, p.ItemName)
            End If
        Next
    End Sub
	
	Dim sTrColor As String = ""
	Dim bColorSwitch As Boolean
	Dim RptList As System.Collections.Hashtable = Nothing
	Dim RptTotal As New RptData
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		Security.CheckPermission(lblPermissions)
		Server.ScriptTimeout = 3000
		If Not IsPostBack Then
			Company.Items.Add("") : Terminal.Items.Add("") : DebitCompany.Items.Add("") : Currency.Items.Add("")
			dbPages.LoadListBox(Company, "Select CustomerNumber + ' | ' + CompanyName, ID From tblCompany Order By CompanyName Asc", "")
			dbPages.LoadListBox(Terminal, "Select terminalNumber + ' | ' + dt_name, terminalNumber From tblDebitTerminals Order By dt_name Asc", "")
			dbPages.LoadListBox(DebitCompany, "Select dc_Name, debitCompany_id From tblDebitCompany Order By dc_Name Asc", "")
			dbPages.LoadListBox(Currency, "Select CUR_ISOName, CUR_ID From tblSystemCurrencies ORDER BY CUR_ID", "")
			GroupByLst.Items.Add("PaymentMethod") : GroupByLst.Items.Add("DebitCompanyID")
			GroupByLst.Items.Add("CustomerID") : GroupByLst.Items.Add("CompanyID")
			GroupByLst.Items.Add("Country") : GroupByLst.Items.Add("Currency")
			DateFrom.SelectedDate = Date.Today.AddMonths(-3)
			DateTo.SelectedDate = Date.Today.AddDays(-1)
		Else
			Server.ScriptTimeout = 90
		End If
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Admin Pages</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link href="../StyleSheet/StyleAdminNetEng.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.dim
		{
			color: gray;
		}
		tr.totals td
		{
			font-weight: bold;
		}
		.formNormal td
		{
			padding-right: 25px;
			white-space: nowrap;
		}
		span.security
		{
			font-weight: bold;
			font-size: 12px;
			color: rgb(245, 245, 245);
			background-color: #2566AB;
			padding-left: 3px;
			padding-right: 3px;
			vertical-align: middle;
		}
		span.securityManaged
		{
			cursor: pointer;
		}
	</style>
</head>
<body>
	<form runat="server" method="post">
	<table align="center" class="formThin">
		<tr>
			<td colspan="4">
				<asp:Label ID="lblPermissions" runat="server" />
				<span id="pageMainHeading">SUMMARY BY CREDIT CARD TYPE</span>
			</td>
		</tr>
		<tr>
			<th nowrap>
				From Date<br />
				<radCln:RadDatePicker ID="DateFrom" runat="server" />
			</th>
			<th>
				Terminal<br />
				<asp:ListBox ID="Terminal" runat="server" Rows="1" />
			</th>
			<th nowrap>
				Debit Company<br />
				<asp:ListBox ID="DebitCompany" runat="server" Rows="1" />
			</th>
			<th>
				Currency<br />
				<asp:ListBox ID="Currency" runat="server" Rows="1" />
			</th>
		</tr>
		<tr>
			<th nowrap>
				To Date<br />
				<radCln:RadDatePicker ID="DateTo" runat="server" />
			</th>
			<th colspan="2">
				Company<br />
				<asp:ListBox ID="Company" runat="server" Rows="1" />
			</th>
			<th nowrap>
				Group By<br />
				<asp:ListBox ID="GroupByLst" runat="server" Rows="1" />
			</th>
			<th>
				<br />
				<input type="submit" name="DOK" value="SHOW" />
			</th>
		</tr>
	</table>
	</form>
	<br />
	<%
		If IsPostBack Then
			%>
				<table width="100%">
					<tr>
						<td valign="top">
							<%
								RptList = LoadStatData(DateFrom.SelectedDate.GetValueOrDefault(), DateTo.SelectedDate.GetValueOrDefault(), eSumType.ePS_Payed, RptTotal, Currency.SelectedValue, "", Company.SelectedValue, DebitCompany.SelectedValue, Terminal.SelectedValue, GroupByLst.Text)
								drawTotals(RptList, RptTotal, 0)
								fillGraph(Chart1, RptList, RptTotal, 0)
							%>
						</td>
						<td valign="top">
							<radC:RadChart ID="Chart1" runat="server" Palette="Pastel" ShadowColor="180, 0, 0, 0"
								Height="200px" Width="320px" Margins-Bottom="50" Margins-Left="40" Margins-Right="5"
								Margins-Top="5" ImageQuality="HighQuality" TextQuality="AntiAlias" DefaultType="StackedBar"
								RadControlsDir="~/Include/RadControls/" ChartTitle-Visible="false">
								<Series>
									<radC:ChartSeries Type="Pie">
                                        <Appearance>
                                            <FillStyle FillType="Solid" MainColor="150, 150, 150" SecondColor="194, 194, 194"></FillStyle>
                                        </Appearance>
									</radC:ChartSeries>
								</Series>
							</radC:RadChart>
						</td>
					</tr>
				</table>
				<br />
			<%
		End If
	%>
</body>
</html>