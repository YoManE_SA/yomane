﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

public partial class AdminCash_ApplicationIdentity_list : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Netpay.Bll.ApplicationIdentity.SearchFilters sf = new Netpay.Bll.ApplicationIdentity.SearchFilters();
        sf.IsActive = chkActiveOnly.Checked;
		rptList.DataSource = Netpay.Bll.ApplicationIdentity.Search(sf, null);
		rptList.DataBind();
    }
}