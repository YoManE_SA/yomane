<%@ Page Language="VB" ValidateRequest="false" EnableEventValidation="false" %>
<script runat="server">
    Protected Overrides Sub OnLoad(e As EventArgs)
        If Request("MD5HashString") <> "" Then ltMD5Hash.Text = Md5Hash.HashASCIIBase64(Request("HashString"))
        If Request("SHAUTF8_Hash") <> "" Then SHAUTF8_HashRes.Text = System.Convert.ToBase64String(System.Security.Cryptography.SHA256.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(Request("SHAUTF8_Hash"))))
        If Not IsPostBack Then rptWireProviders.DataBind()
    End Sub

    Protected Sub SetOrangeServices_Click(source As Object, e As EventArgs)
        Dim PartnerId = 53
        'Netpay.Bll.Merchants.SavePartnerServiceConfig(Netpay.Web.WebUtils.DomainHost, PartnerId, txtOrangeMerchantID.Text, Netpay.Bll.ThirdParty.Orange.ServiceCodeMaster + Netpay.Bll.ThirdParty.Orange.ServiceConfirmPostfix, "1")
        'Netpay.Bll.Merchants.SavePartnerServiceConfig(Netpay.Web.WebUtils.DomainHost, PartnerId, txtOrangeMerchantID.Text, Netpay.Bll.ThirdParty.Orange.ServiceCodeInvoices + Netpay.Bll.ThirdParty.Orange.ServiceConfirmPostfix, "1")
        'Netpay.Bll.Merchants.SavePartnerServiceConfig(Netpay.Web.WebUtils.DomainHost, PartnerId, txtOrangeMerchantID.Text, Netpay.Bll.ThirdParty.Orange.ServiceCodeChild + Netpay.Bll.ThirdParty.Orange.ServiceConfirmPostfix, "1")
    End Sub

    Protected Sub GenerateAuth_Click(source As Object, e As EventArgs)
        Server.ScriptTimeout = 600
        Netpay.Bll.BankHandler.AuthHandlerManager.ExecGenerate()
    End Sub

    Protected Sub Update_WireProviderInc(source As Object, e As CommandEventArgs)
        Dim prv = Netpay.Bll.Wires.Provider.Get(e.CommandArgument)
        If prv Is Nothing Then Exit Sub
        Dim txt = CType(CType(source, System.Web.UI.Control).NamingContainer.FindControl("txtNextBetchNumber"), System.Web.UI.WebControls.TextBox).Text
        prv.NextBetchNumber = txt.ToNullableInt().GetValueOrDefault(prv.NextBetchNumber)
        rptWireProviders.DataBind()
    End Sub

</script>
<html>
<head>
	<title>Control Panel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<link rel="STYLESHEET" type="text/css" href="../StyleSheet/StyleAdminIE.css">
</head>
<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="ltr">
<form id="Form1" method="post" runat="server">
<table width="98%" align="center" border="0" cellpadding="1" cellspacing="0">
<tr>
	<td id="pageMainHeadingTd"><span id="pageMainHeading">Miscellaneous</span><br /><br /><br /></td>
</tr>
<tr>
	<td valign="top">
		<table border="0" cellspacing="1" cellpadding="1">
		<input type="Hidden" name="Action" value="EncTest">
		<tr>
			<td class="txt12b" colspan="3">MD5 Hash Test</td>
		</tr>
		<tr>
			<td class="txt12"><input type="Text" dir="ltr" name="MD5HashString" value="<%=Request("MD5HashString")%>" size="18"></td>
			<td width="6"></td>
			<td><input type="submit" value=" Send "></td>
			<td width="12"></td>
			<td><asp:Literal runat="server" ID="ltMD5Hash" /></td>
		</tr>

		<tr>
			<td class="txt12b" colspan="3">SHA Hash Test</td>
		</tr>
		<tr>
			<td class="txt12"><input type="Text" dir="ltr" name="SHAUTF8_Hash" value="<%=Request("SHAUTF8_Hash")%>" size="18"></td>
			<td width="6"></td>
			<td><input type="submit" value=" Send "></td>
			<td width="12"></td>
			<td><asp:Literal runat="server" ID="SHAUTF8_HashRes" /></td>
		</tr>

		</table>
	</td>
	<td valign="top">
		<table border="0" cellspacing="1" cellpadding="1">
		<tr>
			<td class="txt12b">Set Exist Orange Services</td>
		</tr>
		<tr>
			<td>
				MerchantID:
				<asp:TextBox runat="server" ID="txtOrangeMerchantID" Width="50" />
				<asp:Button runat="server" ID="btnSetOrangeServices" Text="Save" OnClick="SetOrangeServices_Click" />
			</td>
		</tr>
		</table>
	</td>
	<td valign="top">
		<table border="0" cellspacing="1" cellpadding="1">
		<tr>
			<td class="txt12b">Auth Generate</td>
		</tr>
		<tr>
			<td>
				<asp:Button runat="server" ID="Execute" Text="Execute" OnClick="GenerateAuth_Click" />
			</td>
		</tr>
		<tr>
            <td><br /><br /></td>
		</tr>
		<tr>
			<td class="txt12b" style="text-decoration:underline;">Wire Providers Next File Indexes</td>
		</tr>
		</table>

        <table width="100%">
            <asp:Repeater runat="server" ID="rptWireProviders" DataSource='<%# Netpay.Bll.Wires.Provider.Cache %>'>
                <ItemTemplate>
                    <tr>
                        <th class="txt11b" style="text-align:left;"><%# Eval("FriendlyName") %></th>
                        <td><asp:TextBox runat="server" ID="txtNextBetchNumber" Text='<%# Eval("NextBetchNumber") %>' /></td>
		                <td><asp:Button runat="server" Text="Update" CommandArgument='<%# Eval("Name") %>' OnCommand="Update_WireProviderInc" /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
	</td>
</tr>
</table>
</form>
</body>
</html>
