﻿Imports Microsoft.VisualBasic

Namespace Netpay.AdminControls
	<ParseChildren(True, "Text")> _
	Public Class DivBox
		Inherits WebControl
		Protected lblBox As New Label
		Protected imgIcon As New Image
        
        Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
            Page.ClientScript.RegisterClientScriptInclude("func_common.js", "../js/func_common.js")
            MyBase.OnInit(e)
        End Sub

		Public Property Text() As String
			Get
				Return lblBox.Text
			End Get
			Set(ByVal value As String)
				lblBox.Text = value
			End Set
		End Property

		Public Overrides Property Width() As Unit
			Get
				Return lblBox.Width
			End Get
			Set(ByVal value As Unit)
				lblBox.Width = value
			End Set
		End Property

		Public Overrides Property CssClass() As String
			Get
				If String.IsNullOrEmpty(lblBox.CssClass) Then Return String.Empty
				If lblBox.CssClass.Trim.ToLower = "box" Then Return String.Empty
				Return lblBox.CssClass.Substring(4)
			End Get
			Set(ByVal value As String)
				lblBox.CssClass = "box " & value
			End Set
		End Property

		Protected Overrides Sub Render(ByVal htw As HtmlTextWriter)
			If Width.Value = 0 Then Width = Unit.Pixel(300)
			If String.IsNullOrEmpty(CssClass) Then CssClass = "boxHelp LTR"
			lblBox.RenderControl(htw)

			Dim sbOnMouseOver As New StringBuilder("previousSibling.style.visibility='visible';")
			sbOnMouseOver.Append("previousSibling.style.left=getAbsPos(this).Left+15;")
			sbOnMouseOver.Append("previousSibling.style.top=getAbsPos(this).Top+15;")
			sbOnMouseOver.AppendFormat("if ((document.body.clientWidth-previousSibling.style.left.replace('px', ''))-{0}<0)", Width.Value)
			sbOnMouseOver.AppendFormat(" previousSibling.style.left=document.body.clientWidth-{0};", Width.Value)
			imgIcon.ImageUrl = "../images/iconQuestionGrayS.gif"
			imgIcon.CssClass = "boxIcon"
			imgIcon.Attributes.Add("onmouseover", sbOnMouseOver.ToString)
			imgIcon.Attributes.Add("onmouseout", "previousSibling.style.visibility='hidden';")
			imgIcon.RenderControl(htw)
		End Sub
	End Class
End Namespace
