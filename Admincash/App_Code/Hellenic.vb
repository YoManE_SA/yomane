Imports System.IO

Namespace Hellenic
	Public Enum FileType
		MultipleDebitAccounts
		Payroll
		SingleDebitAccount
	End Enum

	Public Enum RecordType
		OwnAccount
		Hellenic
		CyprusBank
		SEPA
		Worldwide
		UtilityPayment
	End Enum

	Public Enum TransferTransactionCosts
		Ours
		Beneficiary
		[Shared]
	End Enum

	Public Enum CyprusBank
		Bank_Of_Cyprus_Ltd = 2
		Laiki_Bank_Ltd
		National_Bank_of_Greece_Cyprus_Ltd = 6
		COOP_Central_Bank_Ltd
		Piraeus_Bank_Plc_Cyprus
		Alpha_Bank_Ltd
		Commercial_Bank_of_Greece_Cyprus_Ltd
		Universal_Bank_Ltd
		Societe_General_Cyprus_Ltd
	End Enum

	Public Enum UtilityPaymentCode
		Electricity_Authority_of_Cyprus
		Cyprus_Telecommunications_Authority
		Waterboard_of_Nicosia
		Waterboard_of_Limasol
		Sewerage_Board_of_Limasol_Amathus
		Telepassport_Telecommunications_Cyprus_Ltd
		Housing_Finance_Corporation
		Hellenic_Bank_Finance_Ltd
		Hellenic_Bank_Services
	End Enum

	Public Enum Currency
		USD = 1
		EUR
		GBP
	End Enum

	Public Class Format
		Public Shared Function FormatHellenicAccount(ByVal sAccount As String) As String
			Dim sbOutput As New StringBuilder
			For i As Integer = 0 To sAccount.Length - 1
				If "1234567890".Contains(sAccount(i)) Then sbOutput.Append(sAccount(i))
			Next
			Return sbOutput.ToString
		End Function

		Public Shared Function FormatCurrency(ByVal hcCurrency As Currency) As String
			Return hcCurrency.ToString
		End Function

		Public Shared Function FormatTransactionCosts(ByVal ttcCosts As TransferTransactionCosts) As String
			Return ttcCosts.ToString.Substring(0, 1)
		End Function

		Public Shared Function FormatCyprusBank(ByVal hcbBank As CyprusBank) As String
			Return CType(hcbBank, Integer).ToString("00")
		End Function

		Public Shared Function FormatCyprusBankName(ByVal hcbBank As CyprusBank) As String
			Return hcbBank.ToString.Replace("_", " ").Replace("Greece Cyprus", "Greece (Cyprus)")
		End Function

		Public Shared Function FormatAmount(ByVal nAmount As Decimal) As String
			If nAmount <= 0D Or nAmount >= 999999999999.995D Then Throw New OverflowException("The amount is out of range for Hellenic: " & nAmount)
			Dim sbResult As New StringBuilder(nAmount.ToString("0,000.00"))
			While "0,".Contains(sbResult(0))
				sbResult.Remove(0, 1)
			End While
			If sbResult(0) = "." Then sbResult.Insert(0, "0")
			Return sbResult.Replace(",", "`").Replace(".", ",").Replace("`", ".").ToString.Trim
		End Function

		Public Shared Function FormatFlag(ByVal bFlag As Boolean) As String
			Return IIf(bFlag, "Y", "N")
		End Function

		Public Shared Function FormatDate(ByVal dDate As Date) As String
			Return dDate.ToString("yyyyMMdd")
		End Function

		Public Shared Function FormatNumber(ByVal nNumber As Integer) As String
			If nNumber < 0 Or nNumber > 99999 Then Throw New OverflowException("The number is out of range (0..99999) for Hellenic: " & nNumber)
			Return nNumber.ToString("00000")
		End Function

		Public Shared Function FormatAlphaNumeric(ByVal sText As String, Optional ByVal nLength As Integer = 0) As String
			If String.IsNullOrEmpty(sText) Then Return String.Empty
			sText = sText.ToUpper
			Dim sbOutput As New StringBuilder
			For i As Integer = 0 To sText.Length - 1
				sbOutput.Append(IIf("1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ -.,'()".Contains(sText(i)), sText(i), " "))
			Next
			Return sbOutput.ToString.ToUpper
		End Function

		Public Shared Function FormatPaymentUtilityCode(ByVal hpuCode As UtilityPaymentCode) As String
			Select Case hpuCode
				Case UtilityPaymentCode.Electricity_Authority_of_Cyprus : Return "51"
				Case UtilityPaymentCode.Cyprus_Telecommunications_Authority : Return "52"
				Case UtilityPaymentCode.Waterboard_of_Nicosia : Return "60"
				Case UtilityPaymentCode.Waterboard_of_Limasol : Return "61"
				Case UtilityPaymentCode.Sewerage_Board_of_Limasol_Amathus : Return "AS"
				Case UtilityPaymentCode.Telepassport_Telecommunications_Cyprus_Ltd : Return "E2"
				Case UtilityPaymentCode.Housing_Finance_Corporation : Return "HF"
				Case UtilityPaymentCode.Hellenic_Bank_Finance_Ltd : Return "FI"
				Case UtilityPaymentCode.Hellenic_Bank_Services : Return "VI"
				Case Else : Throw New OverflowException("This PaymentUtipilyCode is not supported by Hellenic: " & hpuCode.ToString)
			End Select
		End Function

		Public Shared Function GetExtension(ByVal hftType As FileType) As String
			Select Case hftType
				Case FileType.MultipleDebitAccounts : Return "mda"
				Case FileType.Payroll : Return "prl"
				Case FileType.SingleDebitAccount : Return "sda"
				Case Else : Throw New OverflowException("The file type is unsupported by Hellenic: " & hftType.ToString)
			End Select
		End Function

		Public Shared Function FormatRecordType(ByVal hrtType As RecordType) As String
			Select Case hrtType
				Case RecordType.OwnAccount : Return "O"
				Case RecordType.Hellenic : Return "B"
				Case RecordType.CyprusBank : Return "C"
				Case RecordType.SEPA : Return "S"
				Case RecordType.Worldwide : Return "I"
				Case RecordType.UtilityPayment : Return "P"
				Case Else : Throw New OverflowException("The record type is unsupported by Hellenic: " & hrtType.ToString)
			End Select
		End Function
	End Class

	Public MustInherit Class Record
		Public TransactionType As RecordType = Nothing
		Public DebitAccount As String = String.Empty
		Public CreditAccount As String = String.Empty
		Public TransferAmount As Decimal = 0D
		Public TransferCurrency As Currency = Currency.USD
		Public SubmissionDate As Date = Date.Today
		Public Property ValueDate() As Date
			Get
				Return SubmissionDate
			End Get
			Set(ByVal value As Date)
				SubmissionDate = value
			End Set
		End Property
		Public MustOverride Overrides Function ToString() As String
	End Class

	Public Class OwnAccountRecord : Inherits Record
		Public Shadows TransactionType As RecordType = RecordType.OwnAccount
		Public Notes As String = String.Empty
		Public Overrides Function ToString() As String
			Dim sbRecord As New StringBuilder(Format.FormatRecordType(TransactionType))
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount))
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(CreditAccount))
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount))
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(ValueDate))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes, 20))
			Return sbRecord.ToString
		End Function
	End Class

	Public Class HellenicRecord : Inherits Record
		Public Shadows TransactionType As RecordType = RecordType.Hellenic
		Public BeneficiaryName As String = String.Empty
		Public BeneficiaryAddress1 As String = String.Empty
		Public BeneficiaryAddress2 As String = String.Empty
		Public BeneficiaryAddress3 As String = String.Empty
		Public Notes As String = String.Empty
		Public Overrides Function ToString() As String
			Dim sbRecord As New StringBuilder(Format.FormatRecordType(TransactionType))
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount))
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(CreditAccount))
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount))
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(ValueDate))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryName, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress1, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress2, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress3, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes, 20))
			Return sbRecord.ToString
		End Function
	End Class

	Public Class CyprusBankRecord : Inherits Record
		Public Shadows TransactionType As RecordType = RecordType.CyprusBank
		Public BeneficiaryName As String = String.Empty
		Public BeneficiaryAddress1 As String = String.Empty
		Public BeneficiaryAddress2 As String = String.Empty
		Public BeneficiaryBankName As String = String.Empty
		Public BeneficiaryBankAddress1 As String = String.Empty
		Public BeneficiaryBankAddress2 As String = String.Empty
		Public BeneficiaryBankCode As CyprusBank = Nothing
		Public Notes1 As String = String.Empty
		Public Notes2 As String = String.Empty
		Public Overrides Function ToString() As String
			Dim sbRecord As New StringBuilder(Format.FormatRecordType(TransactionType))
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(CreditAccount, 20))
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount))
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(ValueDate))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryName, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress1, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress2, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankName, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankAddress1, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankAddress2, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatCyprusBank(BeneficiaryBankCode))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes1, 20))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes2, 20))
			Return sbRecord.ToString
		End Function
	End Class

	Public Class SepaRecord : Inherits Record
		Public Shadows TransactionType As RecordType = RecordType.SEPA
		Public Shadows TransferCurrency As Currency = Currency.EUR
		Public BeneficiaryName As String = String.Empty
		Public BeneficiaryAddress1 As String = String.Empty
		Public BeneficiaryAddress2 As String = String.Empty
		Public BicCode As String = String.Empty
		Public ConfirmationByFax As Boolean = False
		Public OurReference As String = String.Empty
		Public Description1 As String = String.Empty
		Public Description2 As String = String.Empty
		Public Description3 As String = String.Empty
		Public Overrides Function ToString() As String
			Dim sbRecord As New StringBuilder(Format.FormatRecordType(TransactionType))
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(CreditAccount, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount))
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(ValueDate))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryName, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress1, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress2, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BicCode, 11))
			sbRecord.AppendFormat("|{0}", Format.FormatFlag(ConfirmationByFax))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(OurReference, 20))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Description1, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Description2, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Description3, 35))
			Return sbRecord.ToString
		End Function
	End Class

	Public Class WorldwideRecord : Inherits Record
		Public Shadows TransactionType As RecordType = RecordType.Worldwide
		Public BeneficiaryName As String = String.Empty
		Public BeneficiaryAddress1 As String = String.Empty
		Public BeneficiaryAddress2 As String = String.Empty
		Public BeneficiaryBankName As String = String.Empty
		Public BeneficiaryBankAddress1 As String = String.Empty
		Public BeneficiaryBankAddress2 As String = String.Empty
		Public BeneficiaryBankAddress3 As String = String.Empty
		Public BeneficiaryBankSwiftCode As String = String.Empty
		Public TransactionCosts As TransferTransactionCosts = TransferTransactionCosts.Beneficiary
		Public ConfirmationByFax As Boolean = False
		Public Notes1 As String = String.Empty
		Public Notes2 As String = String.Empty
		Public Notes3 As String = String.Empty
		Public Notes4 As String = String.Empty
		Public SortingCode As String = String.Empty
		Public ReceiverCorrespondentSwiftCode As String = String.Empty
		Public ReceiverCorrespondentBankName As String = String.Empty
		Public ReceiverCorrespondentAddress1 As String = String.Empty
		Public ReceiverCorrespondentAddress2 As String = String.Empty
		Public ReceiverCorrespondentAddress3 As String = String.Empty
		Public ThirdReimbursementInsitutionSwiftCode As String = String.Empty
		Public ThirdReimbursementInsitutionBankName As String = String.Empty
		Public ThirdReimbursementInsitutionAddress1 As String = String.Empty
		Public ThirdReimbursementInsitutionAddress2 As String = String.Empty
		Public ThirdReimbursementInsitutionAddress3 As String = String.Empty
		Public IntermediaryBankSwiftCode As String = String.Empty
		Public Overrides Function ToString() As String
			Dim sbRecord As New StringBuilder(Format.FormatRecordType(TransactionType))
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(CreditAccount, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount))
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(ValueDate))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryName, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress1, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress2, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankName, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankAddress1, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankAddress2, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankAddress3, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankSwiftCode, 11))
			sbRecord.AppendFormat("|{0}", Format.FormatTransactionCosts(TransactionCosts))
			sbRecord.AppendFormat("|{0}", Format.FormatFlag(ConfirmationByFax))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes1, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes2, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes3, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes4, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(SortingCode, 15))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ReceiverCorrespondentSwiftCode, 11))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ReceiverCorrespondentBankName, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ReceiverCorrespondentAddress1, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ReceiverCorrespondentAddress2, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ReceiverCorrespondentAddress3, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ThirdReimbursementInsitutionSwiftCode, 11))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ThirdReimbursementInsitutionBankName, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ThirdReimbursementInsitutionAddress1, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ThirdReimbursementInsitutionAddress2, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ThirdReimbursementInsitutionAddress3, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(IntermediaryBankSwiftCode, 11))
			Return sbRecord.ToString
		End Function
	End Class

	Public Class UtilityPaymentRecord : Inherits Record
		Public Shadows TransactionType As RecordType = RecordType.UtilityPayment
		Public UtilityAccount As String = String.Empty
		Public UtilityName As String = String.Empty
		Public UtilityCode As UtilityPaymentCode = Nothing
		Public Shadows TransferCurrency As Currency = Currency.EUR
		Public Overrides Function ToString() As String
			Dim sbRecord As New StringBuilder(Format.FormatRecordType(TransactionType))
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(UtilityAccount, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(UtilityName, 35))
			sbRecord.AppendFormat("|{0}", Format.FormatPaymentUtilityCode(UtilityCode))
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount))
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency))
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate))
			Return sbRecord.ToString
		End Function
	End Class

	Public Class File
		Private myType As FileType = FileType.MultipleDebitAccounts
		Public ReadOnly Property Type() As FileType
			Get
				Return myType
			End Get
		End Property

		Private myFolder As String = String.Empty
		Public Property Folder() As String
			Get
				Return myFolder
			End Get
			Set(ByVal value As String)
				If String.IsNullOrEmpty(value) Or value = "/" Then
					myFolder = "."
				Else
					If value.EndsWith("/") Then value = value.Substring(0, value.Length - 1)
					myFolder = value
				End If
			End Set
		End Property

		Private myName As String = String.Empty
		Public Property Name() As String
			Get
				Return myName
			End Get
			Set(ByVal value As String)
				If String.IsNullOrEmpty(value) Then
					value = Date.Now.ToString("yyyyMMddHHmmss") & "." & Format.GetExtension(myType)
				ElseIf Not value.Contains(".") Then
					value &= "." & Format.GetExtension(myType)
				ElseIf value.EndsWith(".") Then
					value &= Format.GetExtension(myType)
				End If
				myName = value & ".txt"
			End Set
		End Property

		Private myRecords As New Collection
		Public ReadOnly Property Records() As Collection
			Get
				Return myRecords
			End Get
		End Property

		Public ReadOnly Property RecordCount() As Integer
			Get
				Return myRecords.Count
			End Get
		End Property

		Private myDebitAccount As String
		Public Property DebitAccount() As String
			Get
				Return myDebitAccount
			End Get
			Set(ByVal value As String)
				If myType <> FileType.MultipleDebitAccounts And String.IsNullOrEmpty(value.Trim) Then Throw New OverflowException("File of this type requires explicit Account specification: " & myType.ToString)
				myDebitAccount = value
			End Set
		End Property

		Private mySubmissionDate As Date
		Public Property SubmissionDate() As Date
			Get
				Return mySubmissionDate
			End Get
			Set(ByVal value As Date)
				If value < Date.Today Then Throw New OverflowException("The date is out of range for Hellenic, cannot be past: " & value)
				mySubmissionDate = value
			End Set
		End Property

		Public ReadOnly Property Header() As String
			Get
				Dim sbHeader As New StringBuilder
				sbHeader.AppendFormat("{0}|{1}|{2}", myType.ToString.Substring(0, 1).ToUpper, RecordCount, Format.FormatAmount(0.01))	'Format.FormatNumber(myRecords.Count)
				sbHeader.AppendFormat("|{0}", IIf(myType = FileType.MultipleDebitAccounts, String.Empty, Format.FormatDate(mySubmissionDate)))
				sbHeader.AppendFormat("|{0}", IIf(myType = FileType.MultipleDebitAccounts, String.Empty, myDebitAccount))
				Return sbHeader.ToString
			End Get
		End Property

		Public Sub New(ByVal sFolder As String, Optional ByVal hftType As FileType = FileType.MultipleDebitAccounts, Optional ByVal sFileName As String = Nothing, _
			Optional ByVal sDebitAccount As String = Nothing, Optional ByVal dSubmissionDate As Date = Nothing)
			If dSubmissionDate = Nothing Then dSubmissionDate = Date.Today
			SubmissionDate = dSubmissionDate
			If hftType <> Nothing Then myType = hftType
			DebitAccount = sDebitAccount
			Name = sFileName
			Folder = sFolder
		End Sub

		Public Function AddRecord(ByVal recTransfer As Record, Optional ByVal sKey As String = Nothing) As Integer
			If myType <> FileType.MultipleDebitAccounts Or recTransfer.SubmissionDate = Nothing Or recTransfer.SubmissionDate < Date.Today Then recTransfer.SubmissionDate = SubmissionDate
			If myType <> FileType.MultipleDebitAccounts Or String.IsNullOrEmpty(recTransfer.DebitAccount) Then recTransfer.DebitAccount = DebitAccount
			If String.IsNullOrEmpty(sKey) Then Records.Add(recTransfer) Else Records.Add(recTransfer, sKey)
			Return RecordCount
		End Function

		Public Function RemoveRecord(ByVal sKey As String) As Integer
			If Not String.IsNullOrEmpty(sKey) Then Records.Remove(sKey)
			Return RecordCount
		End Function

		Public Function RemoveRecord(ByVal nIndex As Integer) As Integer
			Records.Remove(nIndex)
			Return RecordCount
		End Function

		Function Save() As String
			Dim swFile As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(myFolder & "/" & myName, False)
			swFile.WriteLine(Header)
			For Each recTransfer As Record In Records
				swFile.WriteLine(recTransfer)
			Next
			swFile.Close()
			Return myName
		End Function
	End Class
End Namespace