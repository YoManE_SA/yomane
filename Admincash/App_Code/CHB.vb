﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Net
Imports Netpay.CommonTypes

Public Enum ChbTransType
	Passed
	Rejected
	Authorized
	Pending
End Enum

Public Class CHB
	Public Shared Function GetTransTableName(ByVal cttType As ChbTransType) As String
		Select Case cttType
			Case ChbTransType.Passed : Return "tblCompanyTransPass"
			Case ChbTransType.Rejected : Return "tblCompanyTransFail"
			Case ChbTransType.Authorized : Return "tblCompanyTransApproval"
			Case ChbTransType.Pending : Return "tblCompanyTransPending"
			Case Else : Throw New OverflowException("Invalid ChbTransType specified!")
		End Select
	End Function

	Public Shared Function Create(ByVal nTransPass As Integer, Optional ByVal dtCHB As Date = Nothing, Optional ByVal bBlockCreditCard As Boolean = True, Optional ByVal bSendMail As Boolean = True, Optional ByVal sComment As String = Nothing, Optional ByVal bDebug As Boolean = False, Optional ByVal nReasonCode As Integer = 0) As Integer
		Dim nID As Integer
		If dtCHB = Nothing Then dtCHB = Date.Now
		nID = dbPages.ExecScalar("EXEC CreateCHB " & nTransPass & ", '" & dtCHB & "', '" & sComment & "', " & nReasonCode & ";")
		If nID > 0 Then
			If bDebug Then HttpContext.Current.Response.Write(Date.Now.ToString("HH:mm:ss") & " Transaction " & nTransPass & " charged back as " & nID & "<br />")
			If bBlockCreditCard Then BlockCreditCard(nTransPass, sComment, bDebug, ChbTransType.Passed, 1)
			If bSendMail Then SendMail(nTransPass, bDebug)
		Else
			If bDebug Then HttpContext.Current.Response.Write(Date.Now.ToString("HH:mm:ss") & " Transaction " & nTransPass & " cannot be charged back, error " & nID & "<br />")
		End If
		Return nID
	End Function

	Public Shared Function RequestPhotocopy(ByVal nTransPass As Integer, Optional ByVal dtCHB As Date = Nothing, Optional ByVal bBlockCreditCard As Boolean = True, Optional ByVal bSendMail As Boolean = True, Optional ByVal sComment As String = Nothing, Optional ByVal bDebug As Boolean = False, Optional ByVal nReasonCode As Integer = 0) As Integer
		Dim nID As Integer
		If dtCHB = Nothing Then dtCHB = Date.Now
		nID = dbPages.ExecScalar("EXEC RequestPhotocopyCHB " & nTransPass & ", '" & dtCHB & "', '" & sComment & "', " & nReasonCode & ";")
		If nID > 0 Then
			If bDebug Then HttpContext.Current.Response.Write(Date.Now.ToString("HH:mm:ss") & " Transaction " & nTransPass & " charged back as " & nID & "<br />")
			If bBlockCreditCard Then BlockCreditCard(nTransPass, sComment, bDebug, ChbTransType.Passed, 1)
			If bSendMail Then SendMail(nTransPass, bDebug, True)
		Else
			If bDebug Then HttpContext.Current.Response.Write(Date.Now.ToString("HH:mm:ss") & " Transaction " & nTransPass & " cannot be charged back, error " & nID & "<br />")
		End If
		Return nID
	End Function

	Public Shared Sub BlockCreditCard(ByVal nTransID As Integer, Optional ByVal sComment As String = Nothing, Optional ByVal bDebug As Boolean = False, Optional ByVal cttTransType As ChbTransType = ChbTransType.Passed, Optional ByVal blockLevel As Byte = 0)
        Dim sSQL As String = "INSERT INTO tblFraudCcBlackList(company_id, PaymentMethod_id, fcbl_ccNumber256, fcbl_ccDisplay, fcbl_comment, fcbl_BlockLevel)" & _
         " SELECT c.CompanyID, c.ccTypeID + 20, Replace(c.CCard_number256,' ',''), t.PaymentMethodDisplay, '" & IIf(String.IsNullOrEmpty(sComment), String.Empty, sComment) & "', " & blockLevel & _
         " FROM " & GetTransTableName(cttTransType) & " t INNER JOIN tblCreditCard c ON CreditCardID=c.ID WHERE t.ID=" & nTransID
		dbPages.ExecSql(sSQL)
		If bDebug Then HttpContext.Current.Response.Write(Date.Now.ToString("HH:mm:ss") & " Transaction " & nTransID & " - credit card blocked.<br />")
	End Sub

	Public Shared Sub SendMail(ByVal nTransPass As Integer, Optional ByVal bDebug As Boolean = False, Optional ByVal bPhotocopy As Boolean = False, Optional ByVal bPreviewOnly As Boolean = False)
		dbPages.ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransPass & "," & IIf(bPhotocopy, PendingEventType.EmailPhotoCopy, PendingEventType.EmailChargeBack) & ",'', 3)")
		'Dim sURL As String = dbPages.getConfig("ProcessFolderURL") & "System_chbMailSend.aspx?TransID=" & nTransPass
		'If bPhotocopy Then sURL &= "&Clarify=1"
		'If bPreviewOnly Then sURL &= "&Preview=1"
		'Dim hwrMail As HttpWebRequest = WebRequest.Create(sURL)
		'hwrMail.Method = "GET"
		'Dim sOutput As String = String.Empty
		'Try
		'Dim srResponse As New StreamReader(hwrMail.GetResponse.GetResponseStream)
		'sOutput = srResponse.ReadToEnd
		'sOutput = IIf(sOutput.Contains("Succeeded"), "sent successfully.", "not sent.")
		'Catch ex As Exception
		'sOutput = "failed: [" & sURL & "] " & ex.Message
		'End Try
		'If bDebug Then HttpContext.Current.Response.Write(Date.Now.ToString("HH:mm:ss") & " Transaction " & nTransPass & " - mail " & sOutput & "<br />")
	End Sub

	Public Shared Function ReformatDateBNS(ByVal sDateBNS As String, Optional ByVal bShow4DigitsYear As Boolean = False) As String
		Return sDateBNS.Substring(0, 2) & "/" & sDateBNS.Substring(3, 2) & "/" & IIf(bShow4DigitsYear, sDateBNS.Substring(6, 4), sDateBNS.Substring(8, 2))
	End Function

	Public Shared Function ReformatCurrencyBNS(ByVal sAmountBNS As String, Optional ByVal sCurrencyBNS As String = Nothing) As String
		Return IIf(String.IsNullOrEmpty(sCurrencyBNS), String.Empty, sCurrencyBNS & " ") & sAmountBNS.Replace(".", String.Empty).Replace(",", ".")
	End Function
End Class
