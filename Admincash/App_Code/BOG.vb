Imports System.IO

Namespace BOG
	Public Enum FileType
		MultipleDebitAccounts
		Payroll
		SingleDebitAccount
	End Enum


	Public Enum TransferTransactionCosts
		Ours
		Beneficiary
		[Shared]
	End Enum


	Public Enum Currency
		USD = 1
		EUR = 2
		GBP
	End Enum

	Public Class Format
		Public Shared Function FormatAccount(ByVal sAccount As String) As String
			Dim sbOutput As New StringBuilder
			For i As Integer = 0 To sAccount.Length - 1
				If "1234567890".Contains(sAccount(i)) Then sbOutput.Append(sAccount(i))
			Next
			Return sbOutput.ToString
		End Function

		Public Shared Function FormatCurrency(ByVal fcCurrency As Currency) As String
			Return fcCurrency.ToString
		End Function

		Public Shared Function FormatTransactionCosts(ByVal ttcCosts As TransferTransactionCosts) As String
			Return ttcCosts.ToString.Substring(0, 1)
		End Function


		Public Shared Function FormatAmount(ByVal nAmount As Decimal) As String
			If nAmount <= 0D Or nAmount >= 999999999999.995D Then Throw New OverflowException("The amount is out of range for BOG: " & nAmount)
			return nAmount.ToString("000.00")
		End Function

		Public Shared Function FormatFlag(ByVal bFlag As Boolean) As String
			Return IIf(bFlag, "Y", "N")
		End Function

		Public Shared Function FormatDate(ByVal dDate As Date) As String
			Return dDate.ToString("dd/MM/yyyy")
		End Function

		Public Shared Function FormatNumber(ByVal nNumber As Integer) As String
			If nNumber < 0 Or nNumber > 99999 Then Throw New OverflowException("The number is out of range (0..99999) for BOG: " & nNumber)
			Return nNumber.ToString("00000")
		End Function

		Public Shared Function FormatAlphaNumeric(ByVal sText As String, Optional ByVal nLength As Integer = 0) As String
			If String.IsNullOrEmpty(sText) Then Return String.Empty
			sText = sText.ToUpper
			Dim sbOutput As New StringBuilder
			For i As Integer = 0 To sText.Length - 1
				If i>0 and i mod 31 = 0 then
					sbOutput.Append("/\")
				End If
				sbOutput.Append(IIf("1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ -.,'()".Contains(sText(i)), sText(i), " "))
			Next
			Return sbOutput.ToString.ToUpper
		End Function


		Public Shared Function GetExtension(ByVal hftType As FileType) As String
			Select Case hftType
				Case FileType.MultipleDebitAccounts : Return "mda"
				Case FileType.Payroll : Return "prl"
				Case FileType.SingleDebitAccount : Return "sda"
				Case Else : Throw New OverflowException("The file type is unsupported by Hellenic: " & hftType.ToString)
			End Select
		End Function


	End Class

	Public Class Record
		Public CommandType As String = "001013"
		Public Version As String = "01"
		Public Number As String = "00"
		Public DocumentDate As Date = Date.Today
		Public DocumentType As String = "0000000101"
		Public TransferAmount As Decimal = 0D
		Public TransferCurrency As Currency = Currency.USD
		Public BeneficiaryAccount As String = String.Empty
		Public IntermediaryBank As String = String.Empty
		Public IntermediaryBankSwiftCode As String = String.Empty
		Public BeneficiaryBank As String = String.Empty
		Public BeneficiaryBankSwiftCode As String = String.Empty
		Public Beneficiary As String = String.Empty
		Public PaymentDetails As String = String.Empty
		Public LineBreak As String = "/\"
		Public AdditionalInformation As	String = String.Empty
		Public Charges As String = "OUR"
		Public SenderAccount As String = String.Empty
		Public Property ValueDate() As Date
			Get
				Return DocumentDate
			End Get
			Set(ByVal value As Date)
				DocumentDate = value
			End Set
		End Property
		Public Overrides Function ToString() As String
			Dim sbRecord As New StringBuilder(String.Empty)
			sbRecord.Append("-----" & vbCrLf)
			sbRecord.Append("COMMAND TYPE: " & CommandType & vbCrLf)
			sbRecord.Append("VERSION: " & Version & vbCrLf)
			sbRecord.Append("NUMBER: " & Number & vbCrLf)
			sbRecord.Append("DOCUMENT DATE: " & Format.FormatDate(DocumentDate) & vbCrLf)
			sbRecord.Append("DOCUMENT TYPE: " & DocumentType & vbCrlf)
			sbRecord.Append("[Foreign Currency Transfer]" & vbCrLf)
			sbRecord.Append("PARAMETER LIST" & vbCrLf)
			sbRecord.Append("PARAMETER: S001" & vbCrLf & "[amount]" & vbCrLf & Format.FormatAmount(TransferAmount) & vbCrLf)
			sbRecord.Append("PARAMETER: S006" & vbCrLf & "[currency]" & vbCrLf & Format.FormatCurrency(TransferCurrency) & vbCrLf)
			sbRecord.Append("PARAMETER: S008" & vbCrLf & "[beneficiary account]" & vbCrLf & Format.FormatAccount(BeneficiaryAccount) & vbCrLf)
			sbRecord.Append("PARAMETER: U001" & vbCrLf & "[value date]" & vbCrLf & Format.FormatDate(ValueDate) & vbCrLf)
			sbRecord.Append("PARAMETER: U002" & vbCrLf & "[intermediary bank]" & vbCrLf & IntermediaryBank & vbCrLf)
			sbRecord.Append("PARAMETER: U003" & vbCrLf & "[intermediary bank bic]" & vbCrLf & IntermediaryBankSwiftCode & vbCrLf)
			sbRecord.Append("PARAMETER: U004" & vbCrLf & "[beneficiary bank]" & vbCrLf & BeneficiaryBank & vbCrLf)
			sbRecord.Append("PARAMETER: U005" & vbCrLf & "[beneficiary bank bic]" & vbCrLf & BeneficiaryBankSwiftCode & vbCrLf)
			sbRecord.Append("PARAMETER: U006" & vbCrLf & "[beneficiary]" & vbCrLf & Format.FormatAlphaNumeric(Beneficiary) & vbCrLf)
			sbRecord.Append("PARAMETER: U007" & vbCrLf & "[payment details]" & vbCrLf & Format.FormatAlphaNumeric(PaymentDetails) & vbCrLf)
			sbRecord.Append("PARAMETER: U008" & vbCrLf & "[additional information]" & vbCrLf & Format.FormatAlphaNumeric(AdditionalInformation) & vbCrLf)
			sbRecord.Append("PARAMETER: U010" & vbCrLf & "[charges]" & vbCrLf & Charges & vbCrLf)
			sbRecord.Append("PARAMETER: S004" & vbCrLf & "[SENDER ACCOUNT]" & vbCrLf & Format.FormatAccount(SenderAccount) & iif(TransferCurrency=Currency.USD,"USD","EUR")) 
			Return sbRecord.ToString.Trim
		End Function
	End Class



	Public Class File
		Private myType As FileType = FileType.MultipleDebitAccounts
		Public ReadOnly Property Type() As FileType
			Get
				Return myType
			End Get
		End Property

		Private myFolder As String = String.Empty
		Public Property Folder() As String
			Get
				Return myFolder
			End Get
			Set(ByVal value As String)
				If String.IsNullOrEmpty(value) Or value = "/" Then
					myFolder = "."
				Else
					If value.EndsWith("/") Then value = value.Substring(0, value.Length - 1)
					myFolder = value
				End If
			End Set
		End Property

		Private myName As String = String.Empty
		Public Property Name() As String
			Get
				Return myName
			End Get
			Set(ByVal value As String)
				If String.IsNullOrEmpty(value) Then
					value = "BOG" & Date.Now.ToString("yyyyMMddHHmmss") & ".txt" 
				ElseIf Not value.Contains(".") Then
					value &= ".txt" 
				ElseIf value.EndsWith(".") Then
					value &= "txt" 
				End If
				myName = value 
			End Set
		End Property

		Private myRecords As New Collection
		Public ReadOnly Property Records() As Collection
			Get
				Return myRecords
			End Get
		End Property

		Public ReadOnly Property RecordCount() As Integer
			Get
				Return myRecords.Count
			End Get
		End Property

		Private myDebitAccount As String
		Public Property DebitAccount() As String
			Get
				Return myDebitAccount
			End Get
			Set(ByVal value As String)
				If myType <> FileType.MultipleDebitAccounts And String.IsNullOrEmpty(value.Trim) Then Throw New OverflowException("File of this type requires explicit Account specification: " & myType.ToString)
				myDebitAccount = value
			End Set
		End Property

		Private mySubmissionDate As Date
		Public Property SubmissionDate() As Date
			Get
				Return mySubmissionDate
			End Get
			Set(ByVal value As Date)
				If value < Date.Today Then Throw New OverflowException("The date is out of range for BOG, cannot be past: " & value)
				mySubmissionDate = value
			End Set
		End Property


		Public Sub New(ByVal sFolder As String, Optional ByVal sFileName As String = Nothing, _
			Optional ByVal sDebitAccount As String = Nothing, Optional ByVal dSubmissionDate As Date = Nothing)
			If dSubmissionDate = Nothing Then dSubmissionDate = Date.Today
			SubmissionDate = dSubmissionDate			
			DebitAccount = sDebitAccount
			Name = sFileName
			Folder = sFolder
		End Sub

		Public Function AddRecord(ByVal recTransfer As Record, Optional ByVal sKey As String = Nothing) As Integer
			If recTransfer.DocumentDate = Nothing Or recTransfer.DocumentDate < Date.Today Then recTransfer.DocumentDate = SubmissionDate
			If String.IsNullOrEmpty(recTransfer.SenderAccount) Then recTransfer.SenderAccount = DebitAccount
			If String.IsNullOrEmpty(sKey) Then Records.Add(recTransfer) Else Records.Add(recTransfer, sKey)
			Return RecordCount
		End Function

		Public Function RemoveRecord(ByVal sKey As String) As Integer
			If Not String.IsNullOrEmpty(sKey) Then Records.Remove(sKey)
			Return RecordCount
		End Function

		Public Function RemoveRecord(ByVal nIndex As Integer) As Integer
			Records.Remove(nIndex)
			Return RecordCount
		End Function

		Function Save() As String
			Dim swFile As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(myFolder & "/" & myName, False, System.Text.Encoding.ASCII)			
			For Each recTransfer As Record In Records
				swFile.WriteLine(recTransfer)
			Next
			swFile.Close()
			Return myName
		End Function
	End Class
End Namespace