Public Class Md5Hash
	Public Shared Function HashBinary(ByVal bytesValue As Byte()) As Byte()
		Return (New System.Security.Cryptography.MD5CryptoServiceProvider).ComputeHash(bytesValue)
	End Function

	Public Shared Function HashHexString(ByVal sValue As String) As String
		Dim bytesValue As Byte() = System.Text.Encoding.UTF8.GetBytes(sValue)
		Dim bytesHash As Byte() = HashBinary(bytesValue)
		Dim sb As New StringBuilder(String.Empty), n1, n2 As Integer
		For i As Integer = LBound(bytesHash) To UBound(bytesHash)
			n1 = bytesHash(i) Mod 16
			n2 = (bytesHash(i) - n1) / 16
			sb.AppendFormat("{0}{1}", IIf(n2 > 9, Chr(Asc("a") + n2 - 10), n2), IIf(n1 > 9, Chr(Asc("a") + n1 - 10), n1))
		Next
		Return sb.ToString()
	End Function

	Public Shared Function HashASCII(ByVal sValue As String) As String
		Dim bytesValue As Byte() = System.Text.Encoding.ASCII.GetBytes(sValue)
		Dim bytesHash As Byte() = HashBinary(bytesValue)
		Return System.Text.Encoding.ASCII.GetString(bytesHash)
	End Function

	Public Shared Function HashUTF8(ByVal sValue As String) As String
		Dim bytesValue As Byte() = System.Text.Encoding.UTF8.GetBytes(sValue)
		Dim bytesHash As Byte() = HashBinary(bytesValue)
		Return System.Text.Encoding.UTF8.GetString(bytesHash)
	End Function

	Public Shared Function HashASCIIBase64(ByVal sValue As String) As String
		Dim bytesValue As Byte() = System.Text.Encoding.ASCII.GetBytes(sValue)
		Dim bytesHash As Byte() = HashBinary(bytesValue)
		Return System.Convert.ToBase64String(bytesHash)
	End Function

	Public Shared Function HashUTF8Base64(ByVal sValue As String) As String
		Dim bytesValue As Byte() = System.Text.Encoding.UTF8.GetBytes(sValue)
		Dim bytesHash As Byte() = HashBinary(bytesValue)
		Return System.Convert.ToBase64String(bytesHash)
	End Function

	Public Shared Function HashHexStringBase64(ByVal sValue As String) As String
		Dim sHash = HashHexString(sValue)
		Dim bytesHash As Byte() = System.Text.Encoding.ASCII.GetBytes(sHash)
		Return System.Convert.ToBase64String(bytesHash)
	End Function
End Class