Imports system.Data.SqlClient
Imports System.Web
Imports Microsoft.VisualBasic

Public Class NPControls
	Public Shared Sub FillList(ByVal ddlList As ListControl, ByVal nFrom As Integer, ByVal nTo As Integer, Optional ByVal bClear As Boolean = False, Optional ByVal sDefaultValue As String = Nothing, Optional ByVal sFirstValue As String = "", Optional ByVal sFirstText As String = "")
		If bClear Then
			While ddlList.Items.Count > 0
				ddlList.Items.RemoveAt(0)
			End While
		End If
		If Not sFirstText Is Nothing Then
			ddlList.Items.Add(sFirstText)
			ddlList.Items(0).Value = IIf(sFirstValue Is Nothing, String.Empty, sFirstValue)
		End If
		For i As Integer = nFrom To nTo
			ddlList.Items.Add(New ListItem(Right("000000000" & i.ToString, nTo.ToString.Length)))
			If ddlList.Items(ddlList.Items.Count - 1).Value = sDefaultValue Then ddlList.Items(ddlList.Items.Count - 1).Selected = True
		Next
	End Sub

	Public Shared Sub FillList(ByVal ddlList As ListControl, ByVal drData As SqlDataReader, Optional ByVal bClear As Boolean = False, Optional ByVal sDefaultValue As String = Nothing, Optional ByVal sFirstValue As String = "", Optional ByVal sFirstText As String = "")
		If bClear Then
			While ddlList.Items.Count > 0
				ddlList.Items.RemoveAt(0)
			End While
		End If
		If Not sFirstText Is Nothing Then
			ddlList.Items.Add(sFirstText)
			ddlList.Items(0).Value = IIf(sFirstValue Is Nothing, String.Empty, sFirstValue)
		End If
		Do While drData.Read
			ddlList.Items.Add(New ListItem(drData(1), drData(0)))
			If ddlList.Items(ddlList.Items.Count - 1).Value = sDefaultValue Then ddlList.Items(ddlList.Items.Count - 1).Selected = True
		Loop
		drData.Close()
	End Sub

	Public Shared Sub TrimValue(ByVal txtField As TextBox)
		txtField.Text = txtField.Text.Trim
	End Sub

	Public Shared Function IsEmptyField(ByVal txtField As TextBox, Optional ByVal lblError As Label = Nothing, Optional ByVal sTitle As String = Nothing) As Boolean
		If txtField.Text.Trim = String.Empty Then
			txtField.Focus()
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: " & IIf(String.IsNullOrEmpty(sTitle), "at least one mandatory field", "<b>" & sTitle & "</b>") & " is empty.</div>"
			Return True
		End If
		Return False
	End Function

	Public Shared Function IsMismatch(ByVal txtField As TextBox, ByVal txtField2 As TextBox, Optional ByVal lblError As Label = Nothing, Optional ByVal sTitle As String = Nothing) As Boolean
		If txtField.Text <> txtField2.Text Then
			txtField.Focus()
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: " & IIf(String.IsNullOrEmpty(sTitle), "The ", "<b>" & sTitle & "</b>") & " values are not identical.</div>"
			Return True
		End If
		Return False
	End Function

	Public Shared Function IsNotSelected(ByVal ddlSelect As DropDownList, Optional ByVal lblError As Label = Nothing, Optional ByVal sTitle As String = Nothing) As Boolean
		If ddlSelect.SelectedIndex = 0 Then
			ddlSelect.Focus()
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: " & IIf(String.IsNullOrEmpty(sTitle), "at least one mandatory list", "<b>" & sTitle & "</b>") & " value is not selected.</div>"
			Return True
		End If
		Return False
	End Function

	Public Shared Function IsCustomError(bIsError As Boolean, Optional ByVal lblError As Label = Nothing, Optional ByVal sText As String = Nothing) As Boolean
		If bIsError Then
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error occured" & IIf(String.IsNullOrWhiteSpace(sText), String.Empty, ": " & sText) & "</div>"
			Return True
		End If
		Return False
	End Function

	Public Shared Function IsNotMail(ByVal txtField As TextBox, Optional ByVal lblError As Label = Nothing, Optional ByVal sTitle As String = Nothing) As Boolean
		Dim regxMail As Regex = New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\w", RegexOptions.IgnoreCase)
		If Not regxMail.IsMatch(txtField.Text) Then
			txtField.Focus()
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: " & IIf(String.IsNullOrEmpty(sTitle), "the eMail field", "<b>" & sTitle & "</b>") & " is not valid eMail address.</div>"
			Return True
		End If
		Return False
	End Function

	Public Shared Function IsNotNumeric(ByVal txtField As TextBox, Optional ByVal lblError As Label = Nothing, Optional ByVal sTitle As String = Nothing, Optional anvType As AllowedNumericValues = AllowedNumericValues.Any) As Boolean
		Dim sRegEx As String = "^"
		If anvType = AllowedNumericValues.Any Or anvType = AllowedNumericValues.Integers Then sRegEx &= "-{0,1}"
		sRegEx &= "\d+"
		If anvType = AllowedNumericValues.Any Or anvType = AllowedNumericValues.NonNegative Or anvType = AllowedNumericValues.Positive Then sRegEx &= "(\.\d+){0,1}"
		sRegEx &= "$"
		Dim regxNumeric As Regex = New Regex(sRegEx)
		If Not regxNumeric.IsMatch(txtField.Text) Then
			txtField.Focus()
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: " & IIf(String.IsNullOrEmpty(sTitle), "the numeric field", "<b>" & sTitle & "</b>") & " contains non-numeric value.</div>"
			Return True
		End If
		Return False
	End Function

	Public Shared ReadOnly Property Mandatory() As String
		Get
			Return "<span class=""mandatory"">*</span>"
		End Get
	End Property

	Public Shared Sub CheckSession()
		If String.IsNullOrEmpty(HttpContext.Current.Session("UserID")) Then HttpContext.Current.Response.Redirect("default.aspx")
	End Sub

	Public Shared Function IsSession() As Boolean
		Return Not String.IsNullOrEmpty(HttpContext.Current.Session("UserID"))
	End Function

	Public Shared Function IsNotCaptcha(ByVal txtField As TextBox, ByVal sCaptchaID As String, Optional ByVal lblError As Label = Nothing) As Boolean
		Dim sSessionCaptcha As String = HttpContext.Current.Session("Captcha_" & sCaptchaID)
		If String.IsNullOrEmpty(sSessionCaptcha) Then
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: the text appearing in the image is not typed in the text field.</div>"
			txtField.Focus()
			Return True
		End If
		If txtField.Text.ToLower.Trim <> sSessionCaptcha.ToString.ToLower.Trim Then
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: the text typed in the text field does not match the text appearing in the picture.</div>"
			txtField.Focus()
			Return True
		End If
		Return False
	End Function

	Public Shared Function GetSelectedItems(ByVal cblList As ListControl, Optional ByVal sSeparator As String = ";") As String
		Dim sbItems As New StringBuilder, bFirst As Boolean = True
		For Each liItem As ListItem In cblList.Items
			If liItem.Selected Then
				sbItems.AppendFormat("{0}{1}", IIf(bFirst, String.Empty, ";"), liItem.Value)
				bFirst = False
			End If
		Next
		Return sbItems.ToString
	End Function

	Public Shared Sub SetGridViewDeleteConfirmation(ByVal e As GridViewRowEventArgs, Optional ByVal nColumnPosition As Integer = -1, Optional ByVal sButtonText As String = Nothing, Optional ByVal nButtonNumber As Integer = 2, Optional ByVal sText As String = Nothing)
		If sText = Nothing Then sText = "Do you really want to delete this record ?!"
		If sButtonText = Nothing Then sButtonText = "Delete"
		If e.Row.RowType = DataControlRowType.DataRow Then
			If nColumnPosition < 0 Then nColumnPosition += e.Row.Cells.Count
			Try
				Dim btnDelete As Button = CType(e.Row.Cells.Item(nColumnPosition).Controls.Item(nButtonNumber), Button)
				If btnDelete.Text.ToLower = sButtonText.ToLower Then
					btnDelete.OnClientClick = "if (!confirm(""" & sText.Replace("""", "\""") & """)) return false;"
					btnDelete.ControlStyle.ForeColor = Drawing.Color.Maroon
				End If
			Catch
			End Try
		End If
	End Sub
End Class

Public Enum PasswordControlLayout
	SingleLineTitlesOnLeft
	SingleLineTitlesOnTop
	MultipleLinesTitlesOnLeft
	MultipleLinesTitlesOnTop
End Enum

Public Enum AllowedNumericValues
	Any
	Positive
	NonNegative
	Integers
	PositiveIntegers
	NonNegativeIntegers
End Enum