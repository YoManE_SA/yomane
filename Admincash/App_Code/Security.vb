Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Web
Imports System.Configuration

'
' 20080512 Tamir
'
' Security class - for page access control
'
Public Class Security
	Public Shared ReadOnly Property Username() As String
		Get
			Dim request As HttpRequest = HttpContext.Current.Request
			Dim sMyUsername As String
			sMyUsername = request.ServerVariables("REMOTE_USER").ToLower()
			If sMyUsername = "" and request.LogonUserIdentity IsNot nothing Then sMyUsername = request.LogonUserIdentity.Name.ToLower()
			If request.ServerVariables("SERVER_NAME") = "localhost" And request.Cookies.AllKeys.Contains("userName") And sMyUsername = "" Then
				If request.Cookies("userName").Value <> "" Then sMyUsername = request.Cookies("userName").Value.ToLower()
			End If
			If sMyUsername.LastIndexOf("\") > 0 Then sMyUsername = sMyUsername.Substring(sMyUsername.LastIndexOf("\") + 1)
			Return sMyUsername
		End Get
	End Property

	Public Shared ReadOnly Property IsAdmin() As Boolean
		Get
			Return dbPages.ExecScalar("IF EXISTS (SELECT ID FROM tblSecurityUser WHERE su_IsAdmin=1 AND su_Username='" & dbPages.DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property SeesUnmanaged() As Boolean
		Get
			Return dbPages.ExecScalar("IF EXISTS (SELECT tblSecurityUser.ID FROM tblSecurityUser INNER JOIN tblSecurityUserGroup ON (tblSecurityUser.ID=tblSecurityUserGroup.sug_User AND tblSecurityUserGroup.sug_IsMember=1) INNER JOIN tblSecurityGroup ON(tblSecurityUserGroup.sug_Group=tblSecurityGroup.ID AND tblSecurityGroup.sg_seeUnmanaged=1)  WHERE su_Username='" & dbPages.DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property IsLimitedMerchant() As Boolean
		Get
			Return dbPages.ExecScalar("IF EXISTS (SELECT u.ID FROM tblSecurityUser u INNER JOIN tblSecurityUserMerchant ON u.ID=sum_User AND su_Username='" & dbPages.DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property IsLimitedDebitCompany() As Boolean
		Get
			Return dbPages.ExecScalar("IF EXISTS (SELECT u.ID FROM tblSecurityUser u INNER JOIN tblSecurityUserDebitCompany ON u.ID=sudc_User AND su_Username='" & dbPages.DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property Path() As String
		Get
			Dim sMyPath As String
			sMyPath = HttpContext.Current.Request.ServerVariables("PATH_INFO").ToLower()			
			If sMyPath.IndexOf("?") > 0 Then sMyPath = sMyPath.Substring(0, sMyPath.IndexOf("?") - 1)
			If sMyPath.LastIndexOf("/") > 0 Then sMyPath = sMyPath.Substring(sMyPath.LastIndexOf("/") + 1)
			If sMyPath.IndexOf("#") > 0 Then sMyPath = sMyPath.Substring(0, sMyPath.IndexOf("#") - 1)
			Return sMyPath
		End Get
	End Property

	Public Shared ReadOnly Property FullPath() As String
		Get
			Dim sMyFullPath As String
			sMyFullPath = HttpContext.Current.Request.ServerVariables("PATH_INFO").ToLower()
			If HttpContext.Current.Request.ServerVariables("QUERY_STRING") <> String.Empty Then sMyFullPath &= "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
			If sMyFullPath.LastIndexOf("/") > 0 Then sMyFullPath = sMyFullPath.Substring(sMyFullPath.LastIndexOf("/") + 1)
			Return sMyFullPath
		End Get
	End Property

	Public Shared ReadOnly Property IsUserActive() As Boolean
		Get
			Return dbPages.ExecScalar("IF EXISTS (SELECT ID FROM tblSecurityUser WITH (NOLOCK) WHERE su_IsActive=1 AND su_Username='" & dbPages.DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property IsDocumentManaged() As Boolean
		Get
			Return dbPages.ExecScalar("IF EXISTS (SELECT ID FROM tblSecurityDocument WITH (NOLOCK) WHERE sd_URL='" & dbPages.DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property IsPermitted() As Boolean
		Get
			If Not IsUserActive Then Return False
			If Not IsDocumentManaged Then Return True
			Return dbPages.ExecScalar("IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument WITH (NOLOCK) INNER JOIN tblSecurityDocumentGroup WITH (NOLOCK) ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityGroup WITH (NOLOCK) ON tblSecurityDocumentGroup.sdg_Group=tblSecurityGroup.ID INNER JOIN tblSecurityUserGroup WITH (NOLOCK) ON tblSecurityGroup.ID=tblSecurityUserGroup.sug_Group INNER JOIN tblSecurityUser WITH (NOLOCK) ON tblSecurityUserGroup.sug_User=tblSecurityUser.ID WHERE sdg_IsVisible=1 AND sug_IsMember=1 AND su_IsActive=1 AND sg_IsActive=1 AND su_Username='" & dbPages.DBText(Username) & "' AND sd_URL='" & dbPages.DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property IsPermittedUser() As Boolean
		Get
			If Not IsUserActive Then Return False
			If Not IsDocumentManaged Then Return True
			If IsAdmin Then Return True
			Return dbPages.ExecScalar("IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument WITH (NOLOCK) INNER JOIN tblSecurityDocumentGroup WITH (NOLOCK) ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityUser WITH (NOLOCK) ON tblSecurityDocumentGroup.sdg_Group=-tblSecurityUser.ID WHERE sdg_IsVisible=1 AND su_IsActive=1 AND su_Username='" & dbPages.DBText(Username) & "' AND sd_URL='" & dbPages.DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property IsActive() As Boolean
		Get
			If Not IsUserActive Then Return False
			If Not IsDocumentManaged Then Return True
			Return dbPages.ExecScalar("IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument WITH (NOLOCK) INNER JOIN tblSecurityDocumentGroup WITH (NOLOCK) ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityGroup WITH (NOLOCK) ON tblSecurityDocumentGroup.sdg_Group=tblSecurityGroup.ID INNER JOIN tblSecurityUserGroup WITH (NOLOCK) ON tblSecurityGroup.ID=tblSecurityUserGroup.sug_Group INNER JOIN tblSecurityUser WITH (NOLOCK) ON tblSecurityUserGroup.sug_User=tblSecurityUser.ID WHERE sdg_IsVisible=1 AND sdg_IsActive=1 AND sug_IsMember=1 AND su_IsActive=1 AND sg_IsActive=1 AND su_Username='" & dbPages.DBText(Username) & "' AND sd_URL='" & dbPages.DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property IsActiveUser() As Boolean
		Get
			If Not IsUserActive Then Return False
			If Not IsDocumentManaged Then Return True
			If IsAdmin Then Return True
			Return dbPages.ExecScalar("IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument WITH (NOLOCK) INNER JOIN tblSecurityDocumentGroup WITH (NOLOCK) ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityUser WITH (NOLOCK) ON tblSecurityDocumentGroup.sdg_Group=-tblSecurityUser.ID WHERE sdg_IsVisible=1 AND sdg_IsActive=1 AND su_IsActive=1 AND su_Username='" & dbPages.DBText(Username) & "' AND sd_URL='" & dbPages.DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property IsDocumentLogged() As Boolean
		Get
			Return dbPages.ExecScalar("IF EXISTS (SELECT ID FROM tblSecurityDocument WITH (NOLOCK) WHERE sd_IsLogged=1 AND sd_URL='" & dbPages.DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)", dbPages.DSN)
		End Get
	End Property

	Public Shared ReadOnly Property IsUserLogged() As Boolean
		Get
			Dim sSQL As String = "IF EXISTS (SELECT ID FROM tblSecurityUser WITH (NOLOCK) WHERE su_IsLogged=1 AND su_Username='" & dbPages.DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
			If dbPages.ExecScalar(sSQL, dbPages.DSN) Then Return True
			sSQL = "IF EXISTS (SELECT * FROM tblSecurityGroup g INNER JOIN tblSecurityUserGroup ug ON g.ID=sug_Group INNER JOIN tblSecurityUser u ON sug_User=u.ID WHERE sg_IsLogged=1 AND su_Username='" & dbPages.DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)"
			Return dbPages.ExecScalar(sSQL, dbPages.DSN)
		End Get
	End Property

	Public Shared Sub LogPageAccessByUser(ByVal bBlocked As Boolean, ByVal bReadonly As Boolean)
		dbPages.ExecScalar("EXEC SecurityLogAdd '" & Username & "', '" & Path & "', " & IIf(bBlocked, 1, 0) & ", " & IIf(bReadonly, 1, 0) & ", '" & HttpContext.Current.Request.UserHostAddress & "'", dbPages.DSN)
	End Sub

	Private Shared Sub DisableButtons(ByVal ctlControl As Control)
		If ctlControl.HasControls Then
			For Each elem As Control In ctlControl.Controls
				DisableButtons(elem)
			Next
		ElseIf TypeOf ctlControl Is Button Then
			CType(ctlControl, Button).Enabled = False
		End If
	End Sub

	Public Shared Sub CheckPermission(ByVal lblPermission As System.Web.UI.WebControls.Label, ByVal bRedirect As Boolean, ByVal sTitle As String)		
		If TypeOf (HttpContext.Current.CurrentHandler) Is Page Then
			If Not CType(HttpContext.Current.CurrentHandler, Page).IsPostBack Then dbPages.CurrentDSN = 1
		End If
		Dim bPermitted As Boolean = False
        Dim bActive As Boolean = False
        Dim bAdmin As Boolean = IsAdmin
        If bAdmin Then
            Dim bIsUserActive As Boolean = IsUserActive
            bPermitted = bIsUserActive
            bActive = bIsUserActive
        Else
            If IsDocumentManaged Or SeesUnmanaged Then
                bPermitted = IsPermitted Or IsPermittedUser
                bActive = IsActive Or IsActiveUser
            End If
        End If
        If IsUserLogged Or IsDocumentLogged Then LogPageAccessByUser(Not bPermitted, Not bActive)
        Try
            If String.IsNullOrEmpty(sTitle) Then sTitle = CType(HttpContext.Current.Handler, Page).Header.Title
        Catch
        End Try
        If bRedirect And Not bPermitted Then
            HttpContext.Current.Response.Redirect("security_message.aspx?PageURL=" & Path & "&Title=" & sTitle, True)
        End If
        If Not IsNothing(lblPermission) Then
            If bAdmin Then
                lblPermission.Text = "<s" & "cript language=""Javascript"" type=""text/javascript"" src=""../js/security.js""></s" & "cript>"
                lblPermission.Text &= "<span class=""security securityManaged"" title=""" & IIf(bActive, "Full Access", "Read Only") & ", " & IIf(IsDocumentManaged, "Managed", "Unmanaged") & " - click to manage permissions"" onclick=""OpenSecurityManager();"">"
            Else
                lblPermission.Text = "<span class=""security"" title=""" & IIf(bActive, "Full Access", "Read Only") & """>"
            End If
			lblPermission.Text &= IIf(IsDocumentManaged, "+", "-") 
			lblPermission.Text &= "</span>" & IIf(bActive, " - RW ", " - RO ")
			If bPermitted And Not bActive Then
                DisableButtons(lblPermission.Page)
                lblPermission.Page.ClientScript.RegisterClientScriptBlock(lblPermission.GetType(), "nghk", "<script language=""Javascript"" type=""text/javascript"" src=""../js/security.js""></script><script language=""Javascript"" type=""text/javascript"" defer=""defer"">DeactivateContent();</script>")
            End If
        End If
    End Sub

	Public Shared Sub CheckPermission(ByVal lblPermissions As System.Web.UI.WebControls.Label, ByVal bRedirect As Boolean)
		CheckPermission(lblPermissions, bRedirect, String.Empty)
	End Sub

	Public Shared Sub CheckPermission(ByVal lblPermissions As System.Web.UI.WebControls.Label, ByVal sTitle As String)
		CheckPermission(lblPermissions, True, sTitle)
	End Sub

	Public Shared Sub CheckPermission(ByVal lblPermissions As System.Web.UI.WebControls.Label)
		CheckPermission(lblPermissions, True, String.Empty)
	End Sub

	Public Shared Sub CheckPermission(ByVal sTitle As String)
		CheckPermission(Nothing, True, sTitle)
	End Sub
End Class