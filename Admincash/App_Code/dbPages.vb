Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Net
Imports System.Web
Imports System.Configuration
Imports Netpay.Crypt

Public Class NoDbConnType
    Public Overrides Function ToString() As String
        Return "The specified database has been taken offline for maintainance purposes. Please try again later."
    End Function
End Class

Public Class dbPages
    Public Shared Function getConfig(ByVal sKey As String) As String
        Return ConfigurationManager.AppSettings.Item(sKey)
    End Function

    Public Shared ReadOnly Property DSN() As String
        Get
            Return Netpay.Web.WebUtils.CurrentDomain.Sql1ConnectionString
        End Get
    End Property

    Public Shared ReadOnly Property DSN1RO() As String
        Get
            Return Netpay.Web.WebUtils.CurrentDomain.Sql1ConnectionString 'GetConnectionString("DSN1RO")
        End Get
    End Property

    Public Shared ReadOnly Property DSN2() As String
        Get
            Return Netpay.Web.WebUtils.CurrentDomain.Sql2ConnectionString
        End Get
    End Property

    Private Shared ReadOnly Property HOTDB_ENABLED() As Boolean
        Get
            Return getConfig("HOTDB_ENABLED")
        End Get
    End Property
    Private Shared ReadOnly Property HOTDB_INTERVAL_MINUTES() As Integer
        Get
            Return TestVar(getConfig("HOTDB_INTERVAL_MINUTES"), 0, -1, 0)
        End Get
    End Property
    Private Shared ReadOnly Property HOTDB_DURATION_SECONDS() As Integer
        Get
            Return TestVar(getConfig("HOTDB_DURATION_SECONDS"), 0, -1, 0)
        End Get
    End Property
    Private Shared ReadOnly Property HOTDB_PREVENTION_SECONDS() As Integer
        Get
            Return TestVar(getConfig("HOTDB_PREVENTION_SECONDS"), 0, -1, 0)
        End Get
    End Property

    Public Shared Function IsActiveDSN2() As Boolean
        If Not HOTDB_ENABLED Then Return False
        Dim nMinute As Integer = Date.Now.Minute Mod HOTDB_INTERVAL_MINUTES, nSecond As Integer = Date.Now.Second
        Return nMinute * 60 + nSecond > HOTDB_DURATION_SECONDS And nMinute * 60 + nSecond < (HOTDB_INTERVAL_MINUTES - 1) * 60 + HOTDB_PREVENTION_SECONDS
    End Function

    Private Shared Property CurrentConnectionString() As String
        Get
            If String.IsNullOrEmpty(HttpContext.Current.Session("CurrentConnectionString")) Then HttpContext.Current.Session("CurrentConnectionString") = DSN
            Return HttpContext.Current.Session("CurrentConnectionString")
        End Get
        Set(ByVal value As String)
            HttpContext.Current.Session("CurrentConnectionString") = value
        End Set
    End Property

    Public Shared Property CurrentDSN() As Integer
        Get
            Return IIf(CurrentConnectionString = DSN2, 2, 1)
        End Get
        Set(ByVal value As Integer)
            CurrentConnectionString = IIf(value = 2 And IsActiveDSN2(), DSN2, DSN)
        End Set
    End Property

    Public Shared Function ShowCurrentDSN() As String
        Dim sbDSN As New StringBuilder
        sbDSN.AppendLine("<script language=""javascript"" type=""text/javascript"" defer=""defer"">")
        sbDSN.AppendLine("	function ShowSQL2()")
        sbDSN.AppendLine("	{")
        sbDSN.AppendLine("		var spnHead = document.getElementById(""pageMainHeading"");")
        sbDSN.AppendLine("		if (spnHead)")
        sbDSN.AppendLine("		{")
        sbDSN.AppendLine("			if (spnHead.innerHTML.indexOf(""SQL" & CurrentDSN & """)<0) spnHead.innerHTML=""<div class=\""dsnHead\"">SQL" & CurrentDSN & "</div>""+spnHead.innerHTML;")
        sbDSN.AppendLine("		}")
        sbDSN.AppendLine("		else")
        sbDSN.AppendLine("		{")
        sbDSN.AppendLine("			document.getElementById(""divDSN2"").style.display="""";")
        sbDSN.AppendLine("		}")
        sbDSN.AppendLine("	}")
        sbDSN.AppendLine("	setInterval(""ShowSQL2();"", 500);")
        sbDSN.AppendLine("</script>")
        sbDSN.AppendLine("<div id=""divDSN2"" class=""dsn"" style=""display:none;"">SQL" & CurrentDSN & "</div>")
        Return sbDSN.ToString
    End Function

    Public Shared Function ExecSql(ByVal sqlStr As String, Optional ByVal sDSN As String = Nothing, Optional timeout As Integer = -1) As Long
        Dim sTempDSN As String = IIf(String.IsNullOrEmpty(sDSN), CurrentConnectionString, sDSN)
        If sTempDSN = DSN2 Then sTempDSN = DSN
        Dim DBCon As New SqlConnection(sTempDSN)
        Dim DBCom As New SqlCommand(sqlStr, DBCon)
        If timeout <> -1 Then DBCom.CommandTimeout = timeout
        Try
            DBCon.Open()
            ExecSql = DBCom.ExecuteNonQuery()
        Catch
            With System.Web.HttpContext.Current
                .Response.Write(sqlStr)
                .Response.Flush()
            End With
            Throw
        Finally
            DBCon.Close()
            DBCom.Dispose()
            DBCon.Dispose()
        End Try
    End Function

    Public Shared Function ExecScalar(ByVal sqlStr As String, Optional ByVal sDSN As String = Nothing, Optional ByVal bStopIfDisabledDSN2 As Boolean = False, Optional ByVal nTimeOut As Integer = 0) As Object
        Dim sTempDSN As String = IIf(String.IsNullOrEmpty(sDSN), CurrentConnectionString, sDSN)
        If sTempDSN = DSN2 Then
            If bStopIfDisabledDSN2 Then Return NoDbConn
            If Not IsActiveDSN2() Then sTempDSN = DSN
        End If
        Dim DBCon As New SqlConnection(sTempDSN)
        Dim DBCom As New SqlCommand(sqlStr, DBCon)
        If nTimeOut > 0 Then DBCom.CommandTimeout = nTimeOut
        Dim bFailedDSN2 As Boolean = False
        Try
            DBCon.Open()
        Catch
            If sTempDSN = DSN2 Then
                bFailedDSN2 = True
                If DBCon.State <> Data.ConnectionState.Closed Then DBCon.Close()
                DBCon.ConnectionString = DSN
            End If
        End Try
        Try
            If bFailedDSN2 Then DBCon.Open()
            ExecScalar = DBCom.ExecuteScalar()
        Catch
            With System.Web.HttpContext.Current
                .Response.Write(sqlStr)
                .Response.Flush()
            End With
            Throw
        Finally
            DBCon.Close()
            DBCom.Dispose()
            DBCon.Dispose()
        End Try
    End Function
    Public Shared NoDbConn As NoDbConnType

    Public Shared Function ExecReader(ByVal sqlStr As String, Optional ByVal sDSN As String = Nothing, Optional ByVal nTimeOut As Integer = 0, Optional ByVal bStopIfDisabledDSN2 As Boolean = False) As SqlDataReader
        Dim sTempDSN As String = IIf(String.IsNullOrEmpty(sDSN), CurrentConnectionString, sDSN)
        If sTempDSN = DSN2 Then
            If bStopIfDisabledDSN2 Then Return Nothing
            If Not IsActiveDSN2() Then sTempDSN = DSN
        End If
        Dim DBCon As New SqlConnection(sTempDSN)
        Dim DBCom As New SqlCommand(sqlStr, DBCon)
        If nTimeOut > 0 Then DBCom.CommandTimeout = nTimeOut
        Dim bFailedDSN2 As Boolean = False
        Try
            DBCon.Open()
        Catch
            If sTempDSN = DSN2 Then
                bFailedDSN2 = True
                If DBCon.State <> Data.ConnectionState.Closed Then DBCon.Close()
                DBCon.ConnectionString = DSN
            End If
        End Try
        Try
            If bFailedDSN2 Then DBCon.Open()
            ExecReader = DBCom.ExecuteReader(System.Data.CommandBehavior.CloseConnection)
        Catch
            With System.Web.HttpContext.Current
                .Response.Write(sqlStr)
                .Response.Flush()
            End With
            Throw
        Finally
            DBCom.Dispose()
        End Try
    End Function

    Public Shared Function ExecDataset(ByVal sqlStr As String, ByVal lPageSize As Integer, ByVal lPage As Integer, Optional ByVal sDSN As String = Nothing, Optional ByVal bStopIfDisabledDSN2 As Boolean = False, Optional ByVal nTimeOut As Integer = 0) As System.Data.DataSet
        Dim sTempDSN As String = IIf(String.IsNullOrEmpty(sDSN), CurrentConnectionString, sDSN)
        If sTempDSN = DSN2 Then
            If bStopIfDisabledDSN2 Then Return Nothing
            If Not IsActiveDSN2() Then sTempDSN = DSN
        End If
        Dim DBCon As New SqlConnection(sTempDSN)
        Dim iAdpt As New SqlDataAdapter(sqlStr, DBCon)
        If nTimeOut > 0 Then iAdpt.SelectCommand.CommandTimeout = nTimeOut
        Dim pRet As New System.Data.DataSet()
        Dim bFailedDSN2 As Boolean = False
        Try
            DBCon.Open()
        Catch
            If sTempDSN = DSN2 Then
                bFailedDSN2 = True
                If DBCon.State <> Data.ConnectionState.Closed Then DBCon.Close()
                DBCon.ConnectionString = DSN
            End If
        End Try
        Try
            If bFailedDSN2 Then DBCon.Open()
            If lPageSize <= 0 Then iAdpt.Fill(pRet) Else iAdpt.Fill(pRet, lPageSize * lPage, lPageSize + 1, "Table1")
        Catch
            With System.Web.HttpContext.Current
                .Response.Write(sqlStr)
                .Response.Flush()
            End With
            Throw
        Finally
            iAdpt.Dispose()
            DBCon.Close()
        End Try
        Return pRet
    End Function

    Public Shared Function DBTextShowForm(ByVal sText As String) As String
        If sText Is Nothing Then Return ""
        Return sText.Trim.Replace("``", """").Replace("`", "'")
    End Function

    Public Shared Function DBTextShow(ByVal sText As String) As String
        If sText Is Nothing Then Return ""
        Return sText.Trim.Replace("``", """").Replace("`", "'").Replace(vbCr, "<br />")
    End Function

    Public Shared Function DBText(ByVal sText As String, Optional ByVal nMaxLength As Integer = Nothing) As String
        If sText Is Nothing Then Return ""
        sText = sText.Trim.Replace("'", "`").Replace("""", "``").Replace("<", "&lt;").Replace(">", "&gt;")
        If nMaxLength > 0 And sText.Length > nMaxLength Then sText = sText.Substring(0, nMaxLength)
        Return sText
    End Function

    Public Shared Sub LoadListBox(ByVal pBox As ListControl, ByVal sqlstr As String, ByVal cSelValue As Object)
        Dim iReader As SqlDataReader = dbPages.ExecReader(sqlstr)
        While iReader.Read
            Dim lsitem As ListItem = New ListItem(iReader(0), iReader(1))
            pBox.Items.Add(lsitem)
            If lsitem.Value = cSelValue Then lsitem.Selected = True
        End While
        iReader.Close()
    End Sub

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Integer, ByVal lMax As Integer, ByVal lDef As Integer) As Integer
        If (Not xval Is Nothing) And IsNumeric(xval) Then
            Try
                TestVar = Integer.Parse(xval)
            Catch
                TestVar = lDef
            End Try
            If (lMin <= lMax) And ((TestVar < lMin) Or (TestVar > lMax)) Then TestVar = lDef
        Else
            TestVar = lDef
        End If
    End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Decimal, ByVal lMax As Decimal, ByVal lDef As Decimal) As Decimal
        Dim rVal As Decimal = lDef
        Try
            rVal = Decimal.Parse(xval.ToString())
            If ((lMin <= lMax) And ((rVal < lMin) Or (rVal > lMax))) Then rVal = lDef
        Catch
        End Try
        Return rVal
    End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMax As Integer, ByVal lDef As String) As String
        Dim rVal As String = lDef
        Try
            rVal = xval.ToString()
            If (rVal.Length > lMax) Then rVal = rVal.Substring(0, lMax)
        Catch
        End Try
        Return rVal
    End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lDef As Boolean) As Boolean
        Dim rVal As Boolean = lDef
        Boolean.TryParse(xval.ToString(), rVal)
        Return rVal
    End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Date, ByVal lMax As Date, ByVal lDef As Date) As Date
        Dim rVal As Date = lDef
        Try
            rVal = Date.Parse(xval.ToString())
            If ((lMin <= lMax) And ((rVal < lMin) Or (rVal > lMax))) Then rVal = lDef
        Catch
        End Try
        Return rVal
    End Function

    Public Shared Function TestNumericList(ByVal xStr As String, ByRef xCount As Integer) As String
        Dim xStrList As String() = xStr.Split(",") : xCount = 0
        Dim strOut As String = ""
        For i As Integer = 0 To xStrList.Length - 1
            If (Not xStrList(i) Is Nothing) And IsNumeric(xStrList(i)) Then
                Try
                    Dim xVal As Integer = Integer.Parse(xStrList(i))
                    strOut &= xVal & "," : xCount += 1
                Catch
                End Try
            End If
        Next
        If strOut.Length > 1 Then strOut = strOut.Substring(0, strOut.Length - 1)
        Return strOut
    End Function

    Public Shared Function GetFileText(ByVal sFileName As String) As String
        Dim output As String = ""
        If System.IO.File.Exists(sFileName) Then
            Dim sr As New System.IO.StreamReader(sFileName)
            output = sr.ReadToEnd()
            sr.Close()
        End If
        Return output
    End Function

    Public Shared Function SendEmail(ByVal sMailSubject As String, ByVal sMailString As String, ByVal pReader As SqlDataReader, ByVal pData As NameValueCollection, Optional sPlaceholderStarter As String = "%", Optional sPlaceholderEnder As String = "%") As Boolean
        Dim xClt As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient(TestVar(getConfig("SMTPServer"), -1, ""))
        If Not pData Is Nothing Then
            For i As Integer = 0 To pData.Count - 1
                sMailString = sMailString.Replace(sPlaceholderStarter & pData.GetKey(i) & sPlaceholderEnder, pData(i))
            Next
        End If
        If Not pReader Is Nothing Then
            For i As Integer = 0 To pReader.FieldCount - 1
                sMailString = sMailString.Replace(sPlaceholderStarter & pReader.GetName(i) & sPlaceholderEnder, pReader(i).ToString())
            Next
        End If
        'sMailString = sMailString
        Dim sCss As String = "<style>" & GetFileText(HttpContext.Current.Server.MapPath("/Data/EmailStyle.css")) & "</style>"
        sMailString = "<html><head></head>" & sCss & "<body>" & sMailString & "</body></html>"
        Dim xMsg As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage(TestVar(getConfig("MailFrom"), -1, ""), pData("MailTo"))
        xMsg.IsBodyHtml = True
        xMsg.Subject = sMailSubject
        xMsg.Body = sMailString
        If dbPages.TestVar(getConfig("SMTPUser"), -1, "") <> "" Then xClt.Credentials = New NetworkCredential(getConfig("SMTPUser"), getConfig("SMTPPassword"))
        HttpContext.Current.Session("LastEmailError") = String.Empty
        Try
            xClt.Send(xMsg)
        Catch ex As Exception
            HttpContext.Current.Session("LastEmailError") = "<hr>Exception:<br />" & ex.Source & ": " & ex.Message & "[" & ex.TargetSite.ToString & ": " & ex.StackTrace & "]"
            If Not (ex.InnerException Is Nothing) Then
                ex = ex.InnerException
                HttpContext.Current.Session("LastEmailError") &= "<hr />Inner Exception:<br />" & ex.Source & ": " & ex.Message & "[" & ex.TargetSite.ToString & ": " & ex.StackTrace & "]"
            End If
            Return False
        End Try
        Return True
    End Function

    Public Shared Sub LoadCurrencies()
        Dim CUR_CHARS(), CUR_RATES(), CUR_ISO() As String
        ReDim CUR_CHARS(eCurrencies.MAX_CURRENCY)
        ReDim CUR_RATES(eCurrencies.MAX_CURRENCY)
        ReDim CUR_ISO(eCurrencies.MAX_CURRENCY)
        Dim iReader As SqlDataReader = ExecReader("Select CUR_ID, CUR_BaseRate, CUR_Symbol, CUR_ISOName From tblSystemCurrencies Order By CUR_ID Desc")
        While iReader.Read
            If iReader("CUR_ID") <= eCurrencies.MAX_CURRENCY Then
                CUR_CHARS(iReader("CUR_ID")) = System.Web.HttpContext.Current.Server.HtmlDecode(iReader("CUR_Symbol"))
                CUR_RATES(iReader("CUR_ID")) = iReader("CUR_BaseRate")
                CUR_ISO(iReader("CUR_ID")) = iReader("CUR_ISOName")
            End If
        End While
        iReader.Close()
        HttpContext.Current.Application.Set("CUR_CHARS", CUR_CHARS)
        HttpContext.Current.Application.Set("CUR_RATES", CUR_RATES)
        HttpContext.Current.Application.Set("CUR_ISO", CUR_ISO)
    End Sub

    Public Shared Function GetCurText(ByVal trnCurr As Byte) As String
        If HttpContext.Current.Application.Get("CUR_CHARS") Is Nothing Then LoadCurrencies()
        If (trnCurr < 0) Or (trnCurr > UBound(HttpContext.Current.Application.Get("CUR_CHARS"))) Then Return "" _
          Else Return HttpContext.Current.Application.Get("CUR_CHARS")(trnCurr)
    End Function

    Public Shared Function GetCurISO(ByVal trnCurr As Byte) As String
        If HttpContext.Current.Application.Get("CUR_ISO") Is Nothing Then LoadCurrencies()
        If (trnCurr < 0) Or (trnCurr > UBound(HttpContext.Current.Application.Get("CUR_ISO"))) Then Return "" _
        Else Return HttpContext.Current.Application.Get("CUR_ISO")(trnCurr)
    End Function

    Public Shared Function FormatCurrWCT(ByVal trnCurr As Byte, ByVal trnValue As Decimal, ByVal crType As Byte) As String
        If CInt(crType) = 0 Then trnValue = -trnValue
        FormatCurrWCT = FormatCurr(trnCurr, trnValue)
    End Function
    Public Shared Function FormatAmountWCT(ByVal trnValue As Decimal, ByVal crType As Byte) As Decimal
        If CInt(crType) = 0 Then trnValue = -trnValue
        FormatAmountWCT = trnValue
    End Function

    Public Shared Function FormatCurr(ByVal trnCurr As Byte, ByVal trnValue As Decimal) As String
        FormatCurr = GetCurISO(trnCurr) & "&nbsp;" & FormatNumber(trnValue, 2, TriState.True, TriState.False, TriState.True)
    End Function

    Public Shared Function ConvertCurrencyRateWFee(ByVal xCurFrom As Integer, ByVal xCurTo As Integer) As Decimal
        If xCurFrom <> xCurTo Then
            'LoadCurrencies()
            'ConvertCurrencyRate = Application("CUR_RATES")(xCurFrom) / Application("CUR_RATES")(xCurTo)		
            Return ExecScalar("Select (((SELECT BaseRate FROM [List].[CurrencyList] WHERE CurrencyID=" & xCurFrom & ") * (1-(SELECT ExchangeFeeInd FROM [List].[CurrencyList] WHERE CurrencyID=" & xCurTo & ")))/(SELECT BaseRate FROM [List].[CurrencyList] WHERE CurrencyID=" & xCurTo & ")) AS Rate", Nothing)
        Else
            Return 1
        End If
    End Function

    Public Shared Function ConvertCurrencyRate(ByVal xCurFrom As Integer, ByVal xCurTo As Integer) As Decimal
        If xCurFrom <> xCurTo Then
            Return ExecScalar("Select ((SELECT BaseRate FROM [List].[CurrencyList] WHERE CurrencyID=" & xCurFrom & ")/(Select BaseRate FROM [List].[CurrencyList] WHERE CurrencyID=" & xCurTo & ")) AS Rate", Nothing)
        Else
            Return 1
        End If
    End Function

    Public Shared Function GetCurrencyRate(ByVal xCurreny As Integer) As Decimal
        Return ExecScalar("SELECT BaseRate FROM [List].[CurrencyList] WHERE CurrencyID=" & xCurreny)
    End Function

    Public Shared Sub showFilter(ByVal fText As String, ByVal fUrl As String, ByVal fDist As Integer, Optional ByVal fStyle As String = "")
        Dim sSibling As String = "", sOnMouseOver As String, sOnMouseOut As String
        For i As Integer = 1 To fDist
            sSibling = sSibling & "nextSibling."
        Next
        sOnMouseOver = sSibling & "style.visibility='visible';"
        sOnMouseOut = sSibling & "style.visibility='hidden';"
        HttpContext.Current.Response.Write("<a href=""" & fUrl & """ onmouseover=""" & sOnMouseOver & """ onmouseout=""" & sOnMouseOut & """ style=""text-decoration:none; border-bottom:1px dotted #0CB10F;" & fStyle & """>" & fText & "</a>")
        HttpContext.Current.Response.Write("<span style=""visibility:hidden;""> <img src=""../images/icon_filterPlus.gif"" align=""middle""></span>")
    End Sub

    Public Shared Function GetUrlValue(ByVal sUrl As String, ByVal sValue As String) As String
        Dim sIndex As Integer = sUrl.IndexOf(sValue & "=", System.StringComparison.OrdinalIgnoreCase)
        If sIndex < 0 Then Return ""
        sIndex = sIndex + sValue.Length + 1
        Dim eIndex As Integer = sUrl.IndexOf("&", sIndex, System.StringComparison.OrdinalIgnoreCase)
        If eIndex < 0 Then eIndex = sUrl.Length
        Return sUrl.Substring(sIndex, eIndex - sIndex)
    End Function

    Public Shared Function SetUrlValue(ByVal qString As String, ByVal strParam As String, ByVal sValue As String) As String
        Dim idx As Integer, idxEnd As Integer
        SetUrlValue = qString
        idx = InStr(1, SetUrlValue, strParam & "=")
        If (idx < 1) Then
            If sValue = Nothing Then Return qString
            Return qString & "&" & strParam & "=" & sValue
        End If
        idxEnd = InStr(idx, SetUrlValue, "&")
        If (idxEnd < 1) Then idxEnd = Len(SetUrlValue)
        SetUrlValue = Left(SetUrlValue, idx - 1) & Right(SetUrlValue, Len(SetUrlValue) - idxEnd) & "&" & strParam & "=" & sValue
    End Function

    Public Shared Function CleanUrl(ByVal Request As NameValueCollection) As String
        Dim strOut As String = ""
        For i As Integer = 0 To Request.Count - 1
            If Request(i) <> "" Then strOut &= "&" & Request.GetKey(i) & "=" & Request(i)
        Next
        Return strOut
    End Function

    Public Shared Function CleanUrlNetKeys(url As String) As String
        Return SetUrlValue(SetUrlValue(SetUrlValue(SetUrlValue(url, "__VIEWSTATE", Nothing), "__EVENTVALIDATION", Nothing), "__EVENTARGUMENT", Nothing), "__EVENTTARGET", Nothing)
    End Function

    Public Shared ReadOnly Property PageCombinedUrl As String
        Get
            Dim ReturnValue = ""
            Dim queryString As String = CleanUrlNetKeys(HttpContext.Current.Request.QueryString.ToString())
            Dim form As String = CleanUrlNetKeys(HttpContext.Current.Request.Form.ToString())
            If queryString <> "" And form <> "" Then
                ReturnValue = queryString & "&" & form
            ElseIf queryString <> "" Then
                ReturnValue = queryString
            ElseIf form <> "" Then
                ReturnValue = form
            End If
            Return ReturnValue
        End Get
    End Property

    'Public Shared Sub SendControlPdf(ByVal ctl As Control)
    '    Dim nStrOut As New System.IO.StringWriter()
    '    nStrOut.Write("<html><style>")
    '    nStrOut.Write(dbPages.GetFileText(ctl.Page.Request.MapPath("StyleSheet.css")))
    '    nStrOut.Write("</style><body class=""pdf"">")
    '    ctl.RenderControl(New HtmlTextWriter(nStrOut))
    '    nStrOut.Write("</body></html>")
    '    SendHtmlPdf(ctl.Page.Response, nStrOut.ToString(), False)
    'End Sub

    'Public Shared Sub SendHtmlPdf(ByVal Response As HttpResponse, ByVal strHtml As String, ByVal bLandscape As Boolean)
    '    Dim nPgfObg As New WebSupergoo.ABCpdf6.Doc()
    '    nPgfObg.HtmlOptions.Paged = True
    '    nPgfObg.HtmlOptions.AddLinks = False
    '    Response.ContentType = "application/pdf"
    '    If bLandscape Then
    '        Dim n As Double = nPgfObg.MediaBox.Width
    '        nPgfObg.MediaBox.Width = nPgfObg.MediaBox.Height : nPgfObg.MediaBox.Height = n
    '        nPgfObg.Rect.Width = nPgfObg.MediaBox.Width : nPgfObg.Rect.Height = nPgfObg.MediaBox.Height
    '    End If
    '    nPgfObg.Rect.Top = nPgfObg.MediaBox.Top - 20 : nPgfObg.Rect.Bottom = nPgfObg.MediaBox.Bottom + 20
    '    Dim nPageID As Integer = nPgfObg.AddImageHtml(strHtml)
    '    While True
    '        If Not nPgfObg.Chainable(nPageID) Then Exit While
    '        nPgfObg.Page = nPgfObg.AddPage()
    '        nPageID = nPgfObg.AddImageToChain(nPageID)
    '    End While
    '    nPgfObg.Save(Response.OutputStream)
    '    nPgfObg.Clear()
    '    Response.End()
    'End Sub

    Private Shared Function IsOneOf(ByVal chrVal As Char, ByVal chrVals() As Char) As Boolean
        For i As Integer = 0 To chrVals.Length - 1
            If chrVal = chrVals(i) Then Return True
        Next
        Return False
    End Function

    Private Shared Function FindWord(ByVal nStr As String, ByVal nStrFind As String, ByVal nSepChar As Char) As Boolean
        Dim nRes As Integer = nStr.IndexOf(nStrFind, StringComparison.OrdinalIgnoreCase)
        If nRes = -1 Then Return False
        If nRes > 0 Then If Not IsOneOf(nStr(nRes - 1), New Char() {vbCr, vbLf, " ", ":", ";", vbTab, Chr(160)}) Then Return False
        nRes += nStrFind.Length
        If nRes < nStr.Length Then If Not IsOneOf(nStr(nRes), New Char() {vbCr, vbLf, " ", ";", vbTab, Chr(160), nSepChar}) Then Return False
        Return True
    End Function

    'Public Shared Function ParsePDF(ByVal sFileName As String, ByVal wmID As Integer, ByRef sResLog As String) As Byte
    '    Dim nFileData As String
    '    Dim nPgfObg As New WebSupergoo.ABCpdf6.Doc()
    '    Try
    '        nPgfObg.Read(sFileName)
    '        nFileData = nPgfObg.GetText("Text")
    '        nPgfObg.Dispose()
    '        Dim iReader As SqlDataReader = dbPages.ExecReader("Select * From tblWireMoney Where WireMoney_id=" & wmID)
    '        If iReader.Read() Then
    '            Dim bFoundAmount = False, bFoundAccount = False
    '            Dim strValue As String = FormatNumber(Fix(iReader("WireAmount")), 0, TriState.True, TriState.False, TriState.True)
    '            If Not bFoundAmount Then bFoundAmount = FindWord(nFileData, strValue, ".")
    '            If Not bFoundAmount Then bFoundAmount = FindWord(nFileData, strValue.Replace(",", ""), ".")
    '            If Not bFoundAmount Then bFoundAmount = FindWord(nFileData, strValue.Replace(",", "*").Replace(".", ",").Replace("*", "."), ",")
    '            If iReader("wirePaymentAbroadIBAN") <> "" Then
    '                If Not bFoundAccount Then bFoundAccount = FindWord(nFileData, iReader("wirePaymentAbroadIBAN"), "-")
    '                If Not bFoundAccount Then bFoundAccount = FindWord(nFileData, iReader("wirePaymentAbroadIBAN").Replace("-", ""), "-")
    '            End If
    '            If iReader("wirePaymentAccount") <> "" Then
    '                strValue = iReader("wirePaymentAccount")
    '                While Left(strValue, 1) = "0"
    '                    strValue = Right(strValue, Len(strValue) - 1)
    '                End While
    '                If Not bFoundAccount Then bFoundAccount = FindWord(nFileData, strValue, "-")
    '            End If
    '            If iReader("wirePaymentAbroadAccountNumber") <> "" Then
    '                strValue = iReader("wirePaymentAccount")
    '                If Not bFoundAccount Then bFoundAccount = FindWord(nFileData, strValue, "-")
    '                If Not bFoundAccount Then bFoundAccount = FindWord(nFileData, strValue.Replace("-", ""), "-")
    '                If Not bFoundAccount Then bFoundAccount = FindWord(nFileData, strValue.Replace(".", ""), "-")
    '                If Not bFoundAccount Then bFoundAccount = FindWord(nFileData, strValue.Replace(" ", ""), "-")
    '            End If
    '            If iReader("wirePaymentAbroadSwiftNumber") <> "" Then
    '                If Not bFoundAccount Then bFoundAccount = FindWord(nFileData, iReader("wirePaymentAbroadSwiftNumber"), "-")
    '            End If
    '            sResLog = "Amount:" & IIf(bFoundAmount, "Found", "Not Found") & vbCrLf & "Account:" & IIf(bFoundAccount, "Found", "Not Found")
    '            ParsePDF = (IIf(bFoundAmount, 1, 0) * 50) + (IIf(bFoundAccount, 1, 0) * 50)
    '        End If
    '        iReader.Close()
    '    Catch Ex As Exception
    '        System.Web.HttpContext.Current.Response.Write(Ex.Message & "<br />")
    '    End Try
    '    ParsePDF = Nothing
    'End Function

    Public Shared Function FormatDateTime(ByVal dtValue As Date, Optional ByVal bShowDate As Boolean = True, Optional ByVal bShowTime As Boolean = True, Optional ByVal bShowYYYY As Boolean = False, Optional ByVal bShowSeconds As Boolean = False) As String
        Dim sbDate As New StringBuilder
        If bShowDate Then
            sbDate.Append(dtValue.Day.ToString.PadLeft(2, "0")).Append("/")
            sbDate.Append(dtValue.Month.ToString.PadLeft(2, "0")).Append("/")
            If bShowYYYY Then
                sbDate.Append(dtValue.Year.ToString)
            Else
                If (dtValue.Year.ToString.Length > 2) Then sbDate.Append(dtValue.Year.ToString.Substring(dtValue.Year.ToString.Length - 2, 2)) _
                Else sbDate.Append(dtValue.Year.ToString)
            End If
            If bShowTime Then sbDate.Append(" ")
        End If
        If bShowTime Then
            sbDate.Append(dtValue.Hour.ToString.PadLeft(2, "0")).Append(":")
            sbDate.Append(dtValue.Minute.ToString.PadLeft(2, "0"))
            If bShowSeconds Then sbDate.Append(":").Append(dtValue.Second.ToString.PadLeft(2, "0"))
        End If
        Return sbDate.ToString
    End Function

    Public Shared Function SendTextMail(ByVal sFrom As String, ByVal sTo As String, ByVal sSubject As String, Optional ByVal sBody As String = Nothing, Optional ByVal sCC As String = Nothing) As Boolean
        If String.IsNullOrEmpty(sFrom) Or String.IsNullOrEmpty(sTo) Or String.IsNullOrEmpty(sSubject) Then Return False
        Try
            Dim mmMessage As New MailMessage(sFrom, sTo, sSubject, IIf(String.IsNullOrEmpty(sBody), String.Empty, sBody))
            mmMessage.IsBodyHtml = False
            If Not String.IsNullOrEmpty(sCC) Then mmMessage.CC.Add(sCC)
            Dim scSender As New SmtpClient(getConfig("SMTPServer"))
            scSender.Send(mmMessage)
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Shared Function SendHttpRequest(ByVal sUrl As String, Optional ByVal sData As String = Nothing, Optional ByVal bPost As Boolean = True, Optional ByRef sResponse As String = Nothing, Optional ByVal bAsync As Boolean = False) As Integer
        Try
            Dim enc As Encoding = Encoding.GetEncoding(1252) ' Windows default Code Page
            If Not bPost Then sUrl &= "?" & sData.Replace(" ", "+")
            Dim loHttp As System.Net.HttpWebRequest = System.Net.WebRequest.Create(sUrl)
            If bPost Then
                loHttp.Method = "POST"
                loHttp.ContentType = "application/x-www-form-urlencoded"
                Dim loRequestStreamWriter As New System.IO.StreamWriter(loHttp.GetRequestStream())
                loRequestStreamWriter.Write(sData.Replace(" ", "+"))
                loRequestStreamWriter.Close()
            End If
            If bAsync Then
                Dim loWebResponse As System.Net.HttpWebResponse = loHttp.BeginGetResponse(Nothing, Nothing)
                Return 0
            Else
                Dim loWebResponse As System.Net.HttpWebResponse = loHttp.GetResponse()
                Dim loResponseStream As System.IO.StreamReader = New System.IO.StreamReader(loWebResponse.GetResponseStream(), enc)
                sResponse = loResponseStream.ReadToEnd()
                loResponseStream.Close()
                loWebResponse.Close()
                Return loWebResponse.StatusCode
            End If
        Catch
            Return 0
        End Try
    End Function

End Class