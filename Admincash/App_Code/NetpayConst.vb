Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Public Enum UserType
	None
	Customer
	Merchant
	Unknown
	MerchantLimited
	SystemUser
	SystemAdmin
	Affiliate
End Enum

Public Enum eCurrencies
	GC_ILS = 0
	GC_NIS = 0
	GC_USD = 1
	GC_EUR = 2
	GC_GBP = 3
	GC_AUD = 4
	GC_CAD = 5
	GC_JPY = 6
	GC_NOK = 7
	GC_PLN = 8
	GC_MXN = 9
	GC_ZAR = 10
	GC_RUB = 11
	GC_TRY = 12
	GC_CHF = 13
	GC_INR = 14
	GC_DKK = 15
	GC_SEK = 16
	GC_CNY = 17
	GC_HUF = 18
	GC_NZD = 19
	GC_HKD = 20
	GC_KRW = 21
	MAX_CURRENCY = 21
End Enum

Public Enum eGroupNames
	GGROUP_WireStatus				= 40
	GGROUP_DeniedStatus				= 41
	GGROUP_CREDITTYPE				= 43
	GGROUP_COMPANYSTATUS			= 44
	GGROUP_MerchantBalanceStatus	= 45
	GGROUP_FAILSTATUSSOURCE			= 46
	GGROUP_BLCommon					= 47
	GGROUP_BatchFileStatus			= 50
	GGROUP_MerchantMakePayments		= 52
	GGROUP_MerchantBalanceSource	= 53
End Enum

Public Enum eCompanyStatus
	CMPS_ARCHIVED			= 0
	CMPS_NEW				= 1
	CMPS_BLOCKED			= 2
	CMPS_CLOSED				= 3
	CMPS_MAXCLOSED			= 9
	CMPS_CPONLY				= 10
	CMPS_INTEGRATION		= 20
	CMPS_MINOK				= 30
	CMPS_PROCESSING			= 30
End Enum

Public Enum ePaymentMethod
	PMD_Unknown				= 00
	PMD_ManualFee			= 01
	PMD_Admin				= 02
	PMD_SystemFees			= 03
	PMD_RolRes				= 04
	
	PMD_MAXINTERNAL			= 14
    PMD_CC_MIN              = 20
    PMD_CC_UNKNOWN          = 20
	PMD_CC_ISRACARD			= 21
	PMD_CC_VISA				= 22
	PMD_CC_DINERS			= 23
	PMD_CC_AMEX				= 24
	PMD_CC_MASTERCARD		= 25
	PMD_CC_DIRECT			= 26
	PMD_CC_OTHER			= 27 'NOT IN USE
	PMD_CC_MAESTRO			= 28
	PMD_CC_TOGGLECARD		= 29
    'Const PMD_CC_ECHECK	= 30
    PMD_CC_MAX              = 99

    PMD_EC_MIN              = 100
	PMD_EC_CHECK			= 100
    PMD_EC_GIROPAY          = 101
    PMD_EC_DIRECTPAY24      = 102
    PMD_EC_PINELV           = 103
    PMD_EC_PAYSAFECARD      = 104
    PMD_EC_CASH_TICKET      = 105
    PMD_EC_PRZELEWY24       = 106
    PMD_EC_EPS              = 107
    PMD_EC_WALLIE           = 108
    PMD_EC_IDEAL            = 109
    PMD_EC_TELEINGRESO      = 110
    PMD_EC_MONEYBOOKERS     = 111
    PMD_EC_ELV              = 112
    PMD_EC_YELLOWPAY        = 113
    PMD_EC_MAX              = 150
End Enum

Public Enum eDeniedStatus
	DNS_NoDenied			= 0
	DNS_UnSetInVar			= 1
	DNS_SetInVar			= 2
	DNS_SetFoundValid		= 3     'photocopy unsettled
	DNS_SetFoundUnValid		= 4
	DNS_DupFoundValid		= 5     'photocopy settled
	DNS_DupFoundUnValid		= 6
	DNS_ChbRefundRTrans		= 7		'should not included in the refund stat list
	DNS_ChbRefundCTrans		= 8		'should be included in the refund stat list as chb
	DNS_SetFoundValidRefunded= 9     'photocopy unsettled then refunded (instead of chargeback)
	DNS_WasWorkedOut		= 10    'the original transaction that charged back (duplicated)
	DNS_DupWasWorkedOut		= 11    'the original transaction that asked for photocopy (duplicated)
	DNS_DupWasWorkedOutRefunded = 12 'transaction clarified then refunded (instead of changedback)
End Enum

Public Enum eFailStatusSource
	FSC_Issuer				= 0
	FSC_Risk				= 1
	FSC_Gateway				= 2
End Enum

Public Enum eBLCommon
	eBL_Email		= 1
	eBL_FullName	= 2
	eBL_Phone		= 3
	eBL_PNumber		= 4
	eBL_BIN			= 5
	eBL_BINCountry	= 6
End Enum

Public Enum eUserType
	eUT_Unknown         = 0
	eUT_Customer        = 1
	eUT_MerchantPrimary = 2
	eUT_MerchantLimited = 4
	eUT_NetpayAdmin     = 5
	eUT_NetpayUser      = 6
End Enum

Public Enum PermissionObjectType
	WebPage							= 1
	WebFeature						= 2
	AffiliateControlPanelWebPage	= 3
End Enum

Public Enum eAFTransType
    eAF_Debit           = 0
    eAF_Refund          = 1
    eAF_Authorize       = 2
End Enum

Public Enum eLogAction
	eLA_Error                       = 0
	eLA_NewFileProcessStart         = 1
	eLA_NewFileProcessCompleted     = 2
	eLA_PendingFileProcessStart     = 3
	eLA_PendingFileProcessComplete  = 4
	eLA_Uploaded                    = 5
End Enum

Public Class NetpayConst
	Shared GLOBAL_COUNTRY_HASH As System.Collections.Hashtable = Nothing
	Const GGROUP_GroupNames As Integer = 0

	Public Shared Function GetConstArrayEx(ByVal nGroup As Integer, ByVal sTextField As String, ByVal nLng As Byte, ByRef sTitle As String, ByRef saRetArr() As String, ByRef saRetColor() As String) As Integer
		Return GetConstArrayEx(nGroup, sTextField, nLng, sTitle, saRetArr, saRetColor, True)
	End Function

	Private Shared Function GetConstArrayEx(ByVal nGroup As Integer, ByVal sTextField As String, ByVal nLng As Byte, ByRef sTitle As String, ByRef saRetArr() As String, ByRef saRetColor() As String, ByVal baLoadColors As Boolean) As Integer
		Dim iReader As SqlDataReader = dbPages.ExecReader("Select GD_ID, GD_Group, IsNull(GD_" & sTextField & ", ''), GD_Color From tblGlobalData Where ((GD_Group=" & nGroup & ") Or (GD_Group=" & GGROUP_GroupNames & " And GD_ID=" & nGroup & ")) And GD_LNG=" & nLng & " Order By GD_Group Asc, GD_ID Desc")
		Dim bHasValue As Boolean = iReader.Read()
		If bHasValue Then
			If (iReader(1) = GGROUP_GroupNames) And (iReader(0) = nGroup) Then
				sTitle = dbPages.TestVar(iReader(2), -1, "")
				bHasValue = iReader.Read()
			End If
			If bHasValue Then
				ReDim saRetArr(iReader(0))
				If baLoadColors Then ReDim saRetColor(iReader(0))
			End If
		End If
		GetConstArrayEx = 0
		Do While bHasValue
			saRetArr(iReader(0)) = iReader(2)
			If baLoadColors Then saRetColor(iReader(0)) = iReader(3)
			GetConstArrayEx += 1
			bHasValue = iReader.Read()
		Loop
		iReader.Close()
	End Function

	Public Shared Function GetConstArray(ByVal nGroup As Integer, ByVal nLng As Integer, ByRef sTitle As String, ByRef saRetArr() As String) As Integer
		Return GetConstArrayEx(nGroup, "Text", nLng, sTitle, saRetArr, Nothing, False)
	End Function

	Public Shared Function GetTableArray(ByVal sTable As String, ByVal sKeyField As String, ByVal sValueField As String, ByRef saRetArr() As String, Optional ByVal bOrderByValue As Boolean = False) As Integer
		Dim nCount As Integer = 0, nKey As Integer
		Dim sSQL As String = "SELECT DISTINCT " & sKeyField & ", " & sValueField & " FROM " & sTable & " WHERE " & sKeyField & " IS NOT NULL AND " & sValueField & " IS NOT NULL ORDER BY " & IIf(bOrderByValue, sValueField, sKeyField)
		Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
		ReDim Preserve saRetArr(0)
		While drData.Read
			nCount += 1
			nKey = drData(0)
			If nKey >= saRetArr.Length Then ReDim Preserve saRetArr(nKey + 1)
			saRetArr(nKey) = drData(1)
		End While
		drData.Close()
		Return nCount
	End Function

	Public Shared Function GetPaymentMethodArray(ByRef saRetArr() As String) As Integer
		Return GetTableArray("PaymentMethod", "PaymentMethod_id", "Name", saRetArr)
	End Function

	Public Shared Function GetCurrencyArray(ByRef saRetArr() As String) As Integer
		Return GetTableArray("tblSystemCurrencies", "CUR_ID", "CUR_IsoName", saRetArr)
	End Function

	Public Shared Function GetDescConstArray(ByVal sGroup As Integer, ByVal vLng As Integer, ByRef vTitle As String, ByRef sRetArr() As String) As Integer
		Return GetConstArrayEx(sGroup, "Description", vLng, vTitle, sRetArr, Nothing, False)
	End Function

	Public Shared Sub FillCombo(ByVal cbox As System.Web.UI.WebControls.ListBox, ByVal sGroup As Integer, ByVal vLng As Integer, ByVal xDef As Integer)
		Dim iReader As SqlDataReader = dbPages.ExecReader("Select GD_ID, GD_Text From tblGlobalData Where (GD_Group=" & sGroup & ") And GD_LNG=" & vLng & " Order By GD_Group Asc, GD_ID Desc")
		While iReader.Read()
			Dim nItem As System.Web.UI.WebControls.ListItem = New System.Web.UI.WebControls.ListItem(dbPages.TestVar(iReader("GD_Text"), -1, ""), dbPages.TestVar(iReader("GD_ID"), 0, -1, 0))
			If dbPages.TestVar(iReader("GD_ID"), 0, -1, 0) = xDef Then nItem.Selected = True
			cbox.Items.Add(nItem)
		End While
		iReader.Close()
	End Sub

	Public Shared Function GetCountryName(ByVal xIsoCode As String) As String
		If GLOBAL_COUNTRY_HASH Is Nothing Then
			GLOBAL_COUNTRY_HASH = New System.Collections.Hashtable
            Dim iReader As SqlDataReader = dbPages.ExecReader("Select CountryISOCode, Name From [List].[CountryList]")
			While iReader.Read()
				GLOBAL_COUNTRY_HASH.Add(iReader(0), iReader(1))
			End While
			iReader.Close()
		End If
		If Not GLOBAL_COUNTRY_HASH.ContainsKey(xIsoCode) Then Return ""
		Return GLOBAL_COUNTRY_HASH(xIsoCode)
	End Function
End Class
