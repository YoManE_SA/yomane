Imports System.Data.SqlClient
Imports Telerik.Charting

Public Enum eSumType
	ePS_All				= &H0000
	ePS_Payed			= &H0001
	ePS_Archive			= &H0002
	ePS_NotPayed		= &H0003
	ePS_NotArchive		= &H0004
	ePS_TypeMask		= &H0007
	ePS_DrawCols		= &H0008

	ePS_IncTest			= &H0010
	ePS_IncSys			= &H0020
	ePS_ByCHBDate		= &H0040
	ePS_CalcPMMCHBRatio	= &H0080
	
	ePS_ShowTotal		= &H0100
	ePS_ShowFees		= &H0200
	ePS_ShowFailed		= &H0400
	ePS_ShowRolling		= &H0800
	ePS_ShowCosts		= &H1000
	ePS_ChbRatioCount	= &H2000
	ePS_ChbCountRep		= &H4000
	ePS_WithCompare		= &H8000
End Enum

Public Enum eSortType
	eST_ItemID			= 0
	eST_Item			= 1
	eST_TransCount		= 2
	eST_TransSum		= 3
	eST_ChbCount		= 4
	eST_ChbSum			= 5
	eST_RefundCount		= 6
	eST_RefundSum		= 7
	eST_Costs			= 8
	eST_Total			= 9
End Enum

Public Class RptData
	Implements IComparable
	Public ItemID As String
	Public ItemName As String = ""
	Public Currency As Byte = 0
	Public TransCount As Long = 0
	Public TransSum As Decimal = 0
	Public RefundCount As Long = 0
	Public RefundSum As Decimal = 0
	Public ChbCount As Long = 0
	Public ChbSum As Decimal = 0
	Public FeeChbSum As Decimal = 0
	Public FeeSum As Decimal = 0
	Public RunningCosts As Decimal = 0
	
	' rejected transactions
	Public IFailCount As Long = 0		'issuer decline
	Public IFailSum As Decimal = 0
	Public NPFailCount As Long = 0		'netpay decline
	Public NPFailSum As Decimal = 0
	Public GRFailCount As Long = 0		'netpay risk decline 
	Public GRFailSum As Decimal = 0

	' rejected transactions which are pre-authorized 
	Public IFailCountPA As Long = 0		'issuer decline
	Public IFailSumPA As Decimal = 0
	Public NPFailCountPA As Long = 0		'netpay decline
	Public NPFailSumPA As Decimal = 0
	Public GRFailCountPA As Long = 0		'netpay risk decline 
	Public GRFailSumPA As Decimal = 0	

	Public RollingRes As Decimal = 0	'netpay rolling reverse
	Public RollingRet As Decimal = 0
	'Public AdminCount As Long = 0
	'Public AdminSum As Decimal = 0
	'Public SystemCount As Long = 0
	'Public SystemSum As Decimal = 0
	Public TransSumCmp1 As Decimal = 0
	Public TransCountCmp1 As Long = 0
	Public MCChbCount As Integer = 0	'to calculate MC CHB ratio
	Public MCTotalCount As Integer = 0
	Public VisaChbCount As Integer = 0	'to calculate Visa CHB ratio
	Public VisaTotalCount As Integer = 0 'to calculate Visa CHB ratio

	Public Overloads Function CompareTo(ByVal obj As Object) As Integer Implements IComparable.CompareTo
		Return String.Compare(ItemName, CType(obj, RptData).ItemName)
	End Function

	Public Overloads Shared Operator +(ByVal v1 As RptData, ByVal v2 As RptData) As RptData
		v1.TransCount += v2.TransCount : v1.TransSum += v2.TransSum
		v1.RefundCount += v2.RefundCount : v1.RefundSum += v2.RefundSum
		v1.ChbCount += v2.ChbCount : v1.ChbSum += v2.ChbSum
		v1.FeeChbSum += v2.FeeChbSum : v1.FeeSum += v2.FeeSum
		v1.RunningCosts += v2.RunningCosts : v1.IFailCount += v2.IFailCount
		
		' rejected transactions
		v1.IFailSum += v2.IFailSum : v1.NPFailCount += v2.NPFailCount
		v1.NPFailSum += v2.NPFailSum : v1.GRFailCount += v2.GRFailCount
		v1.GRFailSum += v2.GRFailSum

		' rejected transactions which are pre-authorized 		
		v1.IFailSumPA += v2.IFailSumPA : v1.NPFailCountPA += v2.NPFailCountPA
		v1.NPFailSumPA += v2.NPFailSumPA : v1.GRFailCountPA += v2.GRFailCountPA
		v1.GRFailSumPA += v2.GRFailSumPA
		Return v1
	End Operator

	Public ReadOnly Property TotalFailCount() As Long
		Get
			Return IFailCount + NPFailCount + GRFailCount
		End Get
	End Property

	Public ReadOnly Property TotalFail() As Decimal
		Get
			Return IFailSum + NPFailSum + GRFailSum
		End Get
	End Property

	Public ReadOnly Property TotalFailCountPA() As Long
		Get
			Return IFailCountPA + NPFailCountPA + GRFailCountPA
		End Get
	End Property

	Public ReadOnly Property TotalFailPA() As Decimal
		Get
			Return IFailSumPA + NPFailSumPA + GRFailSumPA
		End Get
	End Property

	Public ReadOnly Property Total() As Decimal
		Get
				Return TransSum + (RefundSum + ChbSum + FeeChbSum + FeeSum)
		End Get
	End Property

	Public ReadOnly Property SubTotal() As Decimal
		Get
				Return TransSum + (RefundSum + ChbSum)
		End Get
	End Property

	Public ReadOnly Property TotalCount() As Long
		Get
				Return TransCount + (RefundCount + ChbCount)
		End Get
	End Property

	Public ReadOnly Property TotalProcessed() As Decimal
		Get
			Return TransSum + TotalFail + TotalFailPA
		End Get
	End Property

	Public ReadOnly Property TotalProcessedCount() As Long
		Get
				Return TransCount + TotalFailCount + TotalFailCountPA
		End Get
	End Property

	Public ReadOnly Property ChbRatio() As Decimal
		Get
			If ChbSum <> 0 And TransSum <> 0 Then Return ChbSum / TransSum Else Return 0
		End Get
	End Property

	Public ReadOnly Property ChbCountRatio() As Decimal
		Get
			If ChbCount <> 0 And TransCount <> 0 Then Return ChbCount / TransCount Else Return 0
		End Get
	End Property
End Class

Public Class NPTotals 
	Implements IComparer
	Public RowEvent As String, TreeEvent As String
	Public GlobalScale As Decimal = 1
	Public mSortType As eSortType
	Dim mTotal As RptData
	Dim mItemList As New System.Collections.SortedList
	Dim mSortList As System.Collections.ArrayList	
	
	Public Sub New()
		Clear()
	End Sub

	Public Sub Clear()
		mItemList.Clear()
		mTotal = New RptData
		mTotal.ItemName = "TOTAL"
	End Sub

	Public ReadOnly Property IsExist(ByVal strKey As String) As Boolean
		Get
			Return mItemList.Contains(strKey)
		End Get
	End Property

	Public ReadOnly Property GetAddItem(ByVal lID As String, ByVal lName As String) As RptData
		Get
			If mItemList.Contains(lID) Then Return CType(mItemList(lID), RptData)
			Dim pRet As New RptData
			pRet.ItemID = lID
			pRet.ItemName = lName
			mItemList.Add(lID.ToString(), pRet)
			Return pRet
		End Get
	End Property

	Default Public ReadOnly Property Item(ByVal Index As Integer) As RptData
		Get
			Return CType(mItemList.GetByIndex(Index), RptData)
		End Get
	End Property

	Default Public ReadOnly Property Item(ByVal lKey As Object) As RptData
		Get
			Return CType(mItemList(lKey.ToString()), RptData)
		End Get
	End Property

	Public Sub Remove(ByVal Index As Integer)
		mItemList.RemoveAt(Index)
	End Sub

	Public ReadOnly Property Count() As Integer
		Get
			Return mItemList.Count
		End Get
	End Property

	Public ReadOnly Property Total() As RptData
		Get
			Return mTotal
		End Get
	End Property

	Private Function GroupingDateValue(ByRef sDate As String, ByRef vDate As Date, ByVal GroupField As String, ByVal bFromVDate As Boolean) As String
		GroupingDateValue = Nothing
		If bFromVDate Then
			Select Case GroupField
			Case "InsDay"
				sDate = vDate.ToString("yyyyMMdd")
				GroupingDateValue = vDate.ToString("d")
				vDate = vDate.AddDays(1)
			Case "InsWeek"
				sDate = vDate.ToString("yyyy") & DatePart(DateInterval.WeekOfYear, vDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday)
				GroupingDateValue = vDate.ToString("yyyy") & "W" & DatePart(DateInterval.WeekOfYear, vDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday)
				vDate = vDate.AddDays(7)
			Case "InsMonth"
				sDate = vDate.Year & vDate.Month.ToString("00")
				GroupingDateValue = vDate.Year & "/" & vDate.Month.ToString("00")
				vDate = vDate.AddMonths(1)
			Case "InsQuarter"
				sDate = vDate.Year & (((vDate.Month - 1) \ 3) + 1)
				GroupingDateValue = vDate.Year & "Q" & (((vDate.Month - 1) \ 3) + 1)
				vDate = vDate.AddMonths(3)
			End Select
		Else
			Select Case GroupField
			Case "InsDay"
				vDate = Date.Parse(sDate)
			Case "InsMonth"
				vDate = Date.Parse("1/" & sDate)
			Case "InsQuarter"
				vDate = Date.Parse("1/" & (CInt(Left(sDate, 1)) - 1 * 3) & Right(sDate, 4))
			End Select
		End If
	End Function

	Public Sub EnsureRange(ByVal nFrom As DateTime, ByVal nToDate As DateTime, ByVal GroupField As String)
		If Not (GroupField = "InsDay" Or GroupField = "InsMonth" Or GroupField = "InsQuarter") Then Exit Sub 
		Dim nCurrency As Byte = 0
		If Count > 0 Then nCurrency = Item(0).Currency
		While nFrom < nToDate
			Dim itemName As String, sItemID As String = Nothing
			itemName = GroupingDateValue(sItemID, nFrom, GroupField, true)
			GetAddItem(sItemID, itemName).Currency = nCurrency
		End While
	End Sub
	
	Private Shared Function GetFirstDateOfYear(ByVal Year As Integer, ByVal fdow As DayOfWeek) As Date
		Dim nDateFrom As New DateTime(Year, 1, 1)
		If nDateFrom.DayOfWeek = fdow Then Return nDateFrom
		If nDateFrom.DayOfWeek <> 0 Then fdow += (7 - nDateFrom.DayOfWeek)
		Return nDateFrom.AddDays(fdow)
	End Function

	Public Shared Function GetDateByWeek(ByVal Year As Integer, ByVal nWeek As Integer, ByRef lEndDate As DateTime, ByVal fdow As DayOfWeek) As Date
		If fdow = -1 Then
			GetDateByWeek = (New DateTime(Year, 1, 1)).AddDays(7 * (nWeek - 1))
			lEndDate = GetDateByWeek.AddDays(7).AddMilliseconds(-1)
			If lEndDate.Year <> Year Then lEndDate = New DateTime(Year + 1, 1, 1).AddMilliseconds(-1)
		Else
			If nWeek = 1 Then
				lEndDate = GetFirstDateOfYear(Year, fdow).AddMilliseconds(-1)
				Return New DateTime(Year, 1, 1)
			End If
			GetDateByWeek = GetFirstDateOfYear(Year, fdow).AddDays(7 * (nWeek - 2))
			lEndDate = GetDateByWeek.AddDays(7).AddMilliseconds(-1)
		End If
	End Function

	Public Sub AddTransactions(ByVal nFrom As DateTime, ByVal nToDate As DateTime, ByVal SumType As eSumType, ByVal nCurrencyList As String, ByVal nPaymentMethodList As String, ByVal nCompanyList As String, ByVal nMerchantActiveStatus As Integer, ByVal nDebitList As String, ByVal nTerminalList As String, ByVal nBankAccountList As String, ByVal nIPCountry As String, ByVal nBinCountry As String, ByVal sBin As String, ByVal sActiveStatus As String, ByVal sIndustry As String, ByVal GroupField As String, ByVal MerchantRealPDFrom As Date, ByVal MerchantRealPDTo As Date)
		Dim xCur As Byte = 0
		Dim sWhere As String = ""
		If MerchantRealPDFrom <> Date.MinValue Then sWhere &= " And (tblCompanyTransPass.MerchantRealPD >= '" & MerchantRealPDFrom.ToString() & "')"
		If MerchantRealPDTo <> Date.MaxValue Then sWhere &= " And (tblCompanyTransPass.MerchantRealPD <= '" & MerchantRealPDTo.ToString() & "')"
		If nCompanyList <> "" Then sWhere &= " And tblCompanyTransPass.CompanyID IN (" & nCompanyList & ")"
		If nMerchantActiveStatus >= 0 Then sWhere &= " AND tblCompanyTransPass.CompanyID IN (SELECT ID FROM tblCompany WHERE ActiveStatus=" & nMerchantActiveStatus & ")"
		If nDebitList <> "" Then sWhere &= " And tblCompanyTransPass.DebitCompanyID IN (" & nDebitList & ")"
		If nPaymentMethodList <> "" Then sWhere &= " And tblCompanyTransPass.PaymentMethod IN(" & nPaymentMethodList & ")"
		If nTerminalList <> "" Then
			If nTerminalList = "-2" Then sWhere &= " And tblCompanyTransPass.TerminalNumber IN (Select terminalNumber From tblDebitTerminals Where isActive=1)" _
			Else sWhere &= " And tblCompanyTransPass.TerminalNumber ='" & nTerminalList & "'"
		End If
		If nBankAccountList <> "" Then
			sWhere &= " And tblCompanyTransPass.TerminalNumber IN (Select terminalNumber From tblDebitTerminals Where dt_BankAccountID IN(" & nBankAccountList & "))"
		End If
		If Not nIPCountry Is Nothing Then sWhere &= " And IPCountry IN('" & nIPCountry.Replace(",", "','") & "')"
        If Not nBinCountry Is Nothing Then sWhere &= " And tblCompanyTransPass.CreditCardID IN(SELECT tblCreditCard.ID FROM tblCreditCard Where tblCreditCard.BINCountry IN('" & nBinCountry.Replace(",", "','") & "'))"
        If Not sBin Is Nothing Then sWhere &= " And tblCompanyTransPass.CreditCardID IN(Select tblCreditCard.ID FROM tblCreditCard Where tblCreditCard.CCard_First6 IN(" & sBin & "))"
		If Not sActiveStatus Is Nothing Then sWhere &= " AND tblCompanyTransPass.CompanyID IN (SELECT ID FROM tblCompany Where ActiveStatus IN(" & sActiveStatus & "))"
		If Not sIndustry Is Nothing Then sWhere &= " AND tblCompanyTransPass.CompanyID IN (SELECT ID FROM tblCompany Where CompanyIndustry_id IN(" & sIndustry & "))"
		If (GroupField <> "Currency") And (nCurrencyList <> "") Then sWhere &= " And Currency IN(" & nCurrencyList & ")"
		If Security.IsLimitedMerchant Then sWhere &= " AND tblCompanyTransPass.CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & Security.Username & "'))"
		If Security.IsLimitedDebitCompany Then sWhere &= " AND tblCompanyTransPass.DebitCompanyID IN (SELECT ID FROM GetPermittedDebitCompanies('" & Security.Username & "'))"
		If nCurrencyList.Length > 0 Then xCur = dbPages.TestVar(nCurrencyList.Substring(0, 1), 0, -1, 0)
		AddTransactions(nFrom, nToDate, SumType, sWhere, xCur, GroupField)
	End Sub

	Public Sub AddTransactions(ByVal nFrom As DateTime, ByVal nToDate As DateTime, ByVal SumType As eSumType, ByVal sWhere As String, ByVal nCurrency As Byte, ByVal GroupField As String)
		If nToDate.Hour = 0 And nToDate.Minute = 0 Then nToDate = nToDate.AddSeconds(86399)
		Dim GroupBy As String = "", OrderBy As String = "", NameField As String = ""
		Dim sDateWhere As String = "(tblCompanyTransPass.InsertDate >= '" & nFrom.ToString() & "') AND (tblCompanyTransPass.InsertDate <= '" & nToDate.ToString() & "')"
		Dim sFailWhere As String = sWhere
		Dim sLockOptions As String = " WITH (NOLOCK) "
		'(tblCompany.ActiveStatus=" & eCompanyStatus.CMPS_PROCESSING & " AND (tblCompany.id NOT IN (12,35))
		Select Case GroupField
			Case "DebitCompany"
				NameField = "tblDebitCompany.debitCompany_id, dc_Name"
				GroupBy = " LEFT JOIN tblDebitCompany " & sLockOptions & " ON(tblCompanyTransPass.DebitCompanyID = tblDebitCompany.debitCompany_id) "
				OrderBy = " dc_Name Asc"
			Case "CompanyID"
				NameField = "tblCompanyTransPass.CompanyID, CompanyName"
				GroupBy = " LEFT JOIN tblCompany " & sLockOptions & " ON(tblCompanyTransPass.CompanyID = tblCompany.ID) "
				'OrderBy = " CompanyName Asc"
				OrderBy = " tAmount Desc"
			Case "CustomerID"
				NameField = "CustomerID, FirstName + ' ' + LastName"
				GroupBy = " LEFT JOIN tblCustomer " & sLockOptions & " ON(tblCompanyTransPass.CompanyID = tblCustomer.ID) "
				OrderBy = " FirstName + ' ' + LastName Asc"
			Case "Currency"
				NameField = "CUR_ID, CUR_ISOName"
				GroupBy = " LEFT JOIN tblSystemCurrencies " & sLockOptions & " ON tblCompanyTransPass.Currency = CUR_ID "
				OrderBy = " CUR_ISOName Asc"
			Case "Industry"
				NameField = "GD_ID, GD_Text"
				GroupBy = " LEFT JOIN tblCompany " & sLockOptions & " ON(tblCompanyTransPass.CompanyID = tblCompany.ID) "
				GroupBy += " LEFT JOIN tblGlobalData " & sLockOptions & " ON(tblGlobalData.GD_Group=51 And tblGlobalData.GD_LNG=1 And tblCompany.CompanyIndustry_id = tblGlobalData.GD_ID) "
				OrderBy = " GD_Text Asc"
			Case "ActiveStatus"
				NameField = "GD_ID, GD_Text"
				GroupBy = " LEFT JOIN tblCompany " & sLockOptions & " ON tblCompanyTransPass.CompanyID=tblCompany.ID"
				GroupBy += " LEFT JOIN GetGlobalData(44) gd ON tblCompany.ActiveStatus=gd.GD_ID"
				OrderBy = " GD_Text Asc"
			Case "CareOfUser"
				NameField = "su_Username, su_Name"
				GroupBy = " LEFT JOIN tblCompany " & sLockOptions & " ON(tblCompanyTransPass.CompanyID = tblCompany.ID) "
				GroupBy += " LEFT JOIN tblSecurityUser " & sLockOptions & " ON(tblCompany.careOfAdminUser = tblSecurityUser.su_Username) "
				OrderBy = " su_Name Asc"
			Case "Terminal"
				NameField = "tblDebitTerminals.TerminalNumber, tblDebitTerminals.dt_name + ' (' + tblCompanyTransPass.TerminalNumber + ')'"
				NameField &= "+CASE LTrim(RTrim(dt_ContractNumber)) WHEN '' THEN '' ELSE ', CN '+dt_ContractNumber END"
				GroupBy = " LEFT JOIN tblDebitTerminals " & sLockOptions & " ON(tblDebitTerminals.terminalNumber=tblCompanyTransPass.TerminalNumber And tblDebitTerminals.DebitCompany=tblCompanyTransPass.DebitCompanyID) "
				OrderBy = " tblDebitTerminals.TerminalNumber Asc"
			Case "BankAccount"
				NameField = " tblBankAccounts.BA_ID, tblBankAccounts.BA_BankName + ': ' + tblBankAccounts.BA_AccountName + ' - ' + tblBankAccounts.BA_AccountNumber"
				GroupBy = " LEFT JOIN tblDebitTerminals " & sLockOptions & " ON(tblDebitTerminals.terminalNumber=tblCompanyTransPass.TerminalNumber And tblDebitTerminals.DebitCompany=tblCompanyTransPass.DebitCompanyID) " & _
				" LEFT JOIN tblBankAccounts " & sLockOptions & " ON(tblBankAccounts.BA_ID = tblDebitTerminals.dt_BankAccountID)"
				OrderBy = " tblBankAccounts.BA_ID Asc"
			Case "InsDay"
				'Cast(Month(GetDate()) as NVarChar(2))
				NameField = "Convert(NVarChar, InsertDate, 112), Cast(Floor(Cast(InsertDate As float)) As DateTime)"
				GroupField = "Convert(NVarChar, InsertDate, 112)"
				OrderBy = " Convert(NVarChar, InsertDate, 112) Asc"
			Case "InsMonth"
				NameField = "Cast(Year(InsertDate) as nvarchar) + Right('0' + Cast(Month(InsertDate) as nvarchar(2)), 2), Cast(Year(InsertDate) as nvarchar) + '/' + Right('0' + Cast(Month(InsertDate) as nvarchar(2)), 2)"
				GroupField = "Cast(Year(InsertDate) as nvarchar) + Right('0' + Cast(Month(InsertDate) as nvarchar(2)), 2)"
				OrderBy = " Cast(Year(InsertDate) as nvarchar) + Right('0' + Cast(Month(InsertDate) as nvarchar(2)), 2) Asc"
			Case "InsWeek"
				NameField = "Cast(Year(InsertDate) as nvarchar) + Right('0' + Cast(DATEPART(ww, InsertDate) as nvarchar(2)), 2), Cast(Year(InsertDate) as nvarchar) + 'W' + Right('0' + Cast(DATEPART(ww, InsertDate) as nvarchar(2)), 2)"
				GroupField = "Cast(Year(InsertDate) as nvarchar) + Right('0' + Cast(DATEPART(ww, InsertDate) as nvarchar(2)), 2)"
				OrderBy = " Cast(Year(InsertDate) as nvarchar) + Right('0' + Cast(DATEPART(ww, InsertDate) as nvarchar(2)), 2) Asc"
			Case "InsWeek1Jun"
				NameField = "Cast(Year(InsertDate) as nvarchar) + Right('0' + Cast((((DATEPART(dy, InsertDate) - 1) / 7) + 1) as nvarchar(2)), 2), Cast(Year(InsertDate) as nvarchar) + 'W' + Right('0' + Cast((((DATEPART(dy, InsertDate) - 1) / 7) + 1) as nvarchar(2)), 2)"
				GroupField = "Cast(Year(InsertDate) as nvarchar) + Right('0' + Cast((((DATEPART(dy, InsertDate) - 1) / 7) + 1) as nvarchar(2)), 2)"
				OrderBy = " Cast(Year(InsertDate) as nvarchar) + Right('0' + Cast((((DATEPART(dy, InsertDate) - 1) / 7) + 1) as nvarchar(2)), 2) Asc"
			Case "InsQuarter"
				NameField = "Cast(Year(InsertDate) as nvarchar) + Cast(DATEPART(qq, InsertDate) as nvarchar), Cast(Year(InsertDate) as nvarchar) + 'Q' + Cast(DATEPART(qq, InsertDate) as nvarchar)"
				GroupField = "Cast(Year(InsertDate) as nvarchar) + Cast(DATEPART(qq, InsertDate) as nvarchar)"
				OrderBy = " Cast(Year(InsertDate) as nvarchar) + Cast(DATEPART(qq, InsertDate) as nvarchar) Asc"
			Case "IPCountry"
                NameField = "[List].[CountryList].CountryISOCode AS isoCode, [List].[CountryList].Name AS NameE"
                GroupBy = " LEFT JOIN [List].[CountryList] " & sLockOptions & " ON [List].[CountryList].CountryISOCode = tblCompanyTransPass.IPCountry "
                OrderBy = " [List].[CountryList].CountryISOCode ASC"
			Case "BIN"
				NameField = "tblCreditCard.CCard_First6, tblCreditCard.CCard_First6"
                GroupBy = " LEFT JOIN tblCreditCard " & sLockOptions & " ON tblCreditCard.ID=tblCompanyTransPass.CreditCardID "
				OrderBy = " tblCreditCard.CCard_First6 Asc"
			Case "BINCountry"
				NameField = "tblCreditCard.BINCountry, tblCreditCard.BINCountry"
                GroupBy = " LEFT JOIN tblCreditCard " & sLockOptions & " ON tblCreditCard.ID=tblCompanyTransPass.CreditCardID "
				OrderBy = " tblCreditCard.BINCountry Asc"
			Case Else 'Case "PaymentMethod"
				GroupField = "PaymentMethod"
				NameField = "PaymentMethod, pm.Name AS pm_Name"
				GroupBy = " LEFT JOIN List.PaymentMethod AS pm " & sLockOptions & " ON tblCompanyTransPass.PaymentMethod = pm.PaymentMethod_id "
				OrderBy = " pm.Name ASC"
		End Select
		If (SumType And eSumType.ePS_IncTest) = 0 Then sWhere &= " AND tblCompanyTransPass.isTestOnly = 0"
		If (SumType And eSumType.ePS_IncSys) = 0 Then sWhere &= " AND (" & IIf(GroupField = "PaymentMethod", "", "(tblCompanyTransPass.PaymentMethod = 4) OR ") & " tblCompanyTransPass.PaymentMethod > " & ePaymentMethod.PMD_MAXINTERNAL & ")"
		Select Case SumType And eSumType.ePS_TypeMask
			Case eSumType.ePS_Archive
				sWhere &= " AND tblCompanyTransPass.payID = ';X;' "
			Case eSumType.ePS_NotArchive
				sWhere &= " AND tblCompanyTransPass.payID <> ';X;' "
			Case eSumType.ePS_Payed
				sWhere &= " AND tblCompanyTransPass.PrimaryPayedID > 0 "
				', Sum(tblCompanyTransInstallments.Amount) As iAmount
			Case eSumType.ePS_NotPayed
				sWhere &= " AND tblCompanyTransPass.payID <> ';X;' And (tblCompanyTransPass.PrimaryPayedID = 0 OR tblCompanyTransPass.deniedstatus = 2) "
		End Select
		Dim Sql As String = "SELECT " & NameField & ", DeniedStatus, CreditType, tblCompanyTransPass.PaymentMethod, " & _
		 " Count(*) As tVal, Sum(" & IIf(SumType And eSumType.ePS_TypeMask = eSumType.ePS_NotPayed, "Abs(tblCompanyTransPass.UnsettledAmount)", "tblCompanyTransPass.Amount") & ") As tAmount, " & _
		 " Sum(netpayFee_transactionCharge + netpayFee_ratioCharge) As FeeSum, " & _
		 " Sum(netpayFee_ClrfCharge + netpayFee_chbCharge) As FeeChb," & _
		 " Sum(DebitFee) As FeeDebit" & _
		 " FROM tblCompanyTransPass " & sLockOptions & " " & GroupBy & _
		 " WHERE " & sDateWhere & sWhere & _
		 " Group By " & NameField & ", DeniedStatus, CreditType, tblCompanyTransPass.PaymentMethod " & _
		 " Order By " & OrderBy

		 'HttpContext.Current.Response.Write(sql)
		 'HttpContext.Current.Response.Write("<br /><br />SumType.ePS_ChbCountRep: " + ((SumType And eSumType.ePS_ChbCountRep) <> 0).ToString())
		 'HttpContext.Current.Response.Write("<br /><br />SumType.ePS_ByCHBDate: " + ((SumType And eSumType.ePS_ByCHBDate) <> 0).ToString())
		 
		Dim pData As RptData
		Dim iReader As SqlDataReader = dbPages.ExecReader(Sql, Nothing, IIf(dbPages.CurrentDSN = 2, 1200, 90))
		Dim lastDate As Date = DateTime.MinValue
		While iReader.Read()
			Dim cName As String = IIf(iReader(0) Is DBNull.Value, "", iReader(0))
			pData = GetAddItem(cName, IIf(iReader(1) Is DBNull.Value, "[Unknown]", iReader(1)))
			
			If GroupField = "Currency" Then
				pData.Currency = pData.ItemID
			Else
				pData.Currency = nCurrency
				mTotal.Currency = pData.Currency
			End If
			
			If iReader("PaymentMethod") = ePaymentMethod.PMD_RolRes Then
				If iReader("CreditType") = "0" Then
					pData.RollingRes += iReader("tAmount")
					mTotal.RollingRes += iReader("tAmount")
				Else
					pData.RollingRet -= iReader("tAmount")
					mTotal.RollingRet -= iReader("tAmount")
				End If
			Else
				Dim DeniedStatus As Integer = iReader("DeniedStatus")
				If (iReader("CreditType") = "0") And ((DeniedStatus = 0) Or (DeniedStatus = eDeniedStatus.DNS_ChbRefundRTrans)) Then
					pData.RefundCount += iReader("tVal") : mTotal.RefundCount += iReader("tVal")
					pData.RefundSum -= iReader("tAmount") : mTotal.RefundSum -= iReader("tAmount")
				ElseIf (DeniedStatus <> eDeniedStatus.DNS_DupFoundValid) And (DeniedStatus <> eDeniedStatus.DNS_DupFoundUnValid) And (DeniedStatus <> eDeniedStatus.DNS_ChbRefundCTrans) Then 'Copies
					pData.TransCount += iReader("tVal") : mTotal.TransCount += iReader("tVal")
					'If iReader("CreditType") <> "8" Then
						pData.TransSum += iReader("tAmount") : mTotal.TransSum += iReader("tAmount")
					'End If
					If iReader("PaymentMethod") = 22 Then
						pData.VisaTotalCount += iReader("tVal") : mTotal.VisaTotalCount += iReader("tVal")
					End If
				End If
				
				' By Trans Date
				Dim bCountCHB As Boolean = (DeniedStatus = eDeniedStatus.DNS_UnSetInVar Or DeniedStatus = eDeniedStatus.DNS_SetInVar Or DeniedStatus = eDeniedStatus.DNS_SetFoundUnValid) _
				 Or (((SumType And eSumType.ePS_ChbCountRep) <> 0) And (DeniedStatus = eDeniedStatus.DNS_DupFoundUnValid Or DeniedStatus = eDeniedStatus.DNS_ChbRefundCTrans)) _
				 Or (((SumType And eSumType.ePS_ChbCountRep) = 0) And (DeniedStatus = eDeniedStatus.DNS_WasWorkedOut Or DeniedStatus = eDeniedStatus.DNS_DupWasWorkedOut Or DeniedStatus = eDeniedStatus.DNS_DupWasWorkedOutRefunded))
				If bCountCHB Then
					If (SumType And eSumType.ePS_ByCHBDate) = 0 Then
						pData.ChbCount += iReader("tVal") : mTotal.ChbCount += iReader("tVal")
						pData.ChbSum -= iReader("tAmount") : mTotal.ChbSum -= iReader("tAmount")
						If iReader("PaymentMethod") = 22 Then
							pData.VisaChbCount += iReader("tVal") : mTotal.VisaChbCount += iReader("tVal")
						ElseIf iReader("PaymentMethod") = 25 Then
							pData.MCChbCount += iReader("tVal") : mTotal.MCChbCount += iReader("tVal")
						End If
					End If
				End If
				pData.FeeSum -= iReader("FeeSum") : mTotal.FeeSum -= iReader("FeeSum")
				pData.RunningCosts += IIf(IsDBNull(iReader("FeeDebit")), 0, iReader("FeeDebit"))
				mTotal.RunningCosts += IIf(IsDBNull(iReader("FeeDebit")), 0, iReader("FeeDebit"))
				If (SumType And (eSumType.ePS_ByCHBDate Or eSumType.ePS_CalcPMMCHBRatio)) = 0 Then
					pData.FeeChbSum -= iReader("FeeChb") : mTotal.FeeChbSum -= iReader("FeeChb")
				End If
			End If
		End While
		iReader.Close()

		' By CHB Date
		If (SumType And eSumType.ePS_ByCHBDate) > 0 Then
			Sql = "SELECT " & NameField & ", DeniedStatus, CreditType, tblCompanyTransPass.PaymentMethod, " & _
			 " Count(*) As tVal, Sum(tblCompanyTransPass.Amount) As tAmount, " & _
			 " Sum(netpayFee_ClrfCharge + netpayFee_chbCharge) As FeeChb" & _
			 " FROM tblCompanyTransPass " & sLockOptions & " " & GroupBy & _
			 " WHERE " & sDateWhere & sWhere & _
			 " Group By " & NameField & ", DeniedStatus, CreditType, tblCompanyTransPass.PaymentMethod " & _
			 " Order By " & OrderBy
			Sql = Sql.Replace("InsertDate", "DeniedDate")
			
			'HttpContext.Current.Response.Write(sql)
			
			iReader = dbPages.ExecReader(Sql, Nothing, IIf(dbPages.CurrentDSN = 2, 1200, 90))
			While iReader.Read()
				Dim DeniedStatus As Integer = iReader("DeniedStatus")
				Dim bCountCHB As Boolean = (DeniedStatus = eDeniedStatus.DNS_UnSetInVar Or DeniedStatus = eDeniedStatus.DNS_SetInVar Or DeniedStatus = eDeniedStatus.DNS_SetFoundUnValid) _
				 Or (((SumType And eSumType.ePS_ChbCountRep) <> 0) And (DeniedStatus = eDeniedStatus.DNS_DupFoundUnValid Or DeniedStatus = eDeniedStatus.DNS_ChbRefundCTrans)) _
				 Or (((SumType And eSumType.ePS_ChbCountRep) = 0) And (DeniedStatus = eDeniedStatus.DNS_WasWorkedOut Or DeniedStatus = eDeniedStatus.DNS_DupWasWorkedOut Or DeniedStatus = eDeniedStatus.DNS_DupWasWorkedOutRefunded))
				If bCountCHB Then
					Dim cName As String = IIf(iReader(0) Is DBNull.Value, "", iReader(0))
					pData = GetAddItem(cName, IIf(iReader(1) Is DBNull.Value, "[Unknown]", iReader(1)))
					If GroupField = "Currency" Then
						pData.Currency = dbPages.TestVar(pData.ItemID, 0, -1, 0)
					Else
						pData.Currency = nCurrency
						mTotal.Currency = pData.Currency
					End If
					'pData.TransCount += iReader("tVal") : mTotal.TransCount += iReader("tVal")
					pData.ChbCount += iReader("tVal") : mTotal.ChbCount += iReader("tVal")
					pData.ChbSum -= iReader("tAmount") : mTotal.ChbSum -= iReader("tAmount")
					If Not IsDBNull(iReader("FeeChb")) then
						pData.FeeChbSum -= iReader("FeeChb") : mTotal.FeeChbSum -= iReader("FeeChb")
					End If
					If iReader("PaymentMethod") = 22 Then
						pData.VisaChbCount += iReader("tVal") : mTotal.VisaChbCount += iReader("tVal")
					ElseIf iReader("PaymentMethod") = 25 Then
						pData.MCChbCount += iReader("tVal") : mTotal.MCChbCount += iReader("tVal")
					End If
				End If
			End While
			iReader.Close()
		End If

		' failed transactions
		If (SumType And eSumType.ePS_ShowFailed) > 0 Then
			' get normal transactions
			Dim failQuery As String = ("Select " & NameField & ", FailSource, Count(*) As tVal, Sum(Abs(Amount)) As tAmount From tblCompanyTransFail " & sLockOptions & _
			 " Left Join tblDebitCompanyCode " & sLockOptions & " ON (tblCompanyTransFail.DebitCompanyID = tblDebitCompanyCode.DebitCompanyID And tblCompanyTransFail.replyCode = tblDebitCompanyCode.Code)" & _
			 GroupBy & _
			 " Where tblCompanyTransFail.TransType != 1 And " & sDateWhere & sFailWhere & _
			 " Group By " & NameField & ", FailSource" & _
			 " Order By " & OrderBy).Replace("tblCompanyTransPass", "tblCompanyTransFail")

			iReader = dbPages.ExecReader(failQuery, Nothing, IIf(dbPages.CurrentDSN = 2, 1200, 90))
			While iReader.Read()
				Dim cName As String = IIf(iReader(0) Is DBNull.Value, "", iReader(0))
				pData = GetAddItem(cName, IIf(iReader(1) Is DBNull.Value, "[Unknown]", iReader(1)))

				If GroupField = "Currency" Then
					pData.Currency = dbPages.TestVar(pData.ItemID, 0, -1, 0)
				Else
					pData.Currency = nCurrency
					mTotal.Currency = pData.Currency
				End If

				Select Case IIf(iReader("FailSource") Is DBNull.Value, eFailStatusSource.FSC_Issuer, iReader("FailSource"))
					Case eFailStatusSource.FSC_Gateway
						pData.NPFailCount += iReader("tVal") : mTotal.NPFailCount += iReader("tVal")
						pData.NPFailSum += iReader("tAmount") : mTotal.NPFailSum += iReader("tAmount")
					Case eFailStatusSource.FSC_Risk
						pData.GRFailCount += iReader("tVal") : mTotal.GRFailCount += iReader("tVal")
						pData.GRFailSum += iReader("tAmount") : mTotal.GRFailSum += iReader("tAmount")
					Case Else
						pData.IFailCount += iReader("tVal") : mTotal.IFailCount += iReader("tVal")
						pData.IFailSum += iReader("tAmount") : mTotal.IFailSum += iReader("tAmount")
				End Select
			End While
			iReader.Close()

			' get pre-authorized transactions
			failQuery = ("Select " & NameField & ", FailSource, Count(*) As tVal, Sum(Abs(Amount)) As tAmount From tblCompanyTransFail " & sLockOptions & _
			 " Left Join tblDebitCompanyCode " & sLockOptions & " ON (tblCompanyTransFail.DebitCompanyID = tblDebitCompanyCode.DebitCompanyID And tblCompanyTransFail.replyCode = tblDebitCompanyCode.Code)" & _
			 GroupBy & _
			 " Where tblCompanyTransFail.TransType = 1 And " & sDateWhere & sFailWhere & _
			 " Group By " & NameField & ", FailSource" & _
			 " Order By " & OrderBy).Replace("tblCompanyTransPass", "tblCompanyTransFail")

			iReader = dbPages.ExecReader(failQuery, Nothing, IIf(dbPages.CurrentDSN = 2, 1200, 90))
			While iReader.Read()
				Dim cName As String = IIf(iReader(0) Is DBNull.Value, "", iReader(0))
				pData = GetAddItem(cName, IIf(iReader(1) Is DBNull.Value, "[Unknown]", iReader(1)))

				If GroupField = "Currency" Then
					pData.Currency = pData.ItemID
				Else
					pData.Currency = nCurrency
					mTotal.Currency = pData.Currency
				End If

				Select Case IIf(iReader("FailSource") Is DBNull.Value, eFailStatusSource.FSC_Issuer, iReader("FailSource"))
					Case eFailStatusSource.FSC_Gateway
						pData.NPFailCountPA += iReader("tVal") : mTotal.NPFailCountPA += iReader("tVal")
						pData.NPFailSumPA += iReader("tAmount") : mTotal.NPFailSumPA += iReader("tAmount")
					Case eFailStatusSource.FSC_Risk
						pData.GRFailCountPA += iReader("tVal") : mTotal.GRFailCountPA += iReader("tVal")
						pData.GRFailSumPA += iReader("tAmount") : mTotal.GRFailSumPA += iReader("tAmount")
					Case Else
						pData.IFailCountPA += iReader("tVal") : mTotal.IFailCountPA += iReader("tVal")
						pData.IFailSumPA += iReader("tAmount") : mTotal.IFailSumPA += iReader("tAmount")
				End Select
			End While
			iReader.Close()
		End If

		If ((SumType And eSumType.ePS_WithCompare) > 0) Then
			Dim nMonthWidth As Integer = (nToDate - nFrom).Days
			If nMonthWidth > 0 Then nMonthWidth = (nMonthWidth / 30) + IIf(nMonthWidth Mod 30 > 0, 1, 0)
			sDateWhere = "(tblCompanyTransPass.InsertDate >= '" & nFrom.AddMonths(-nMonthWidth).ToString() & "') AND (tblCompanyTransPass.InsertDate <= '" & nToDate.AddMonths(-nMonthWidth).ToString() & "')"
			Sql = "SELECT " & NameField & ", Sum(Amount) As tAmount, Count(*) As tVal " & _
			" FROM tblCompanyTransPass " & sLockOptions & " " & GroupBy & _
			" WHERE " & sDateWhere & sWhere & " And DeniedStatus=0 And CreditType=1" & _
			" Group By " & NameField
			
			'HttpContext.Current.Response.Write(sql)
			
			iReader = dbPages.ExecReader(Sql, Nothing, IIf(dbPages.CurrentDSN = 2, 1200, 90))
			While iReader.Read()
				Dim cName As String = IIf(iReader(0) Is DBNull.Value, "", iReader(0))
				If mItemList.Contains(cName) Then
					pData = mItemList(cName)
					pData.TransCountCmp1 += iReader("tVal") : mTotal.TransCountCmp1 += iReader("tVal")
					pData.TransSumCmp1 += iReader("tAmount") : mTotal.TransSumCmp1 += iReader("tAmount")
				End If
			End While
			iReader.Close()
		End If

		' chb mastercard
		If ((SumType And eSumType.ePS_CalcPMMCHBRatio) > 0) Then
			sDateWhere = "(tblCompanyTransPass.InsertDate >= '" & nFrom.AddMonths(-1).ToString() & "') AND (tblCompanyTransPass.InsertDate <= '" & nToDate.AddMonths(-1).ToString() & "')"
			Sql = "SELECT " & NameField & ", Count(*) As tVal " & _
			" FROM tblCompanyTransPass " & sLockOptions & " " & GroupBy & _
			" WHERE " & sDateWhere & sWhere & " And tblCompanyTransPass.PaymentMethod=25 And DeniedStatus=0 And CreditType=1" & _
			" Group By " & NameField
			
			'HttpContext.Current.Response.Write(sql)
			
			iReader = dbPages.ExecReader(Sql, Nothing, IIf(dbPages.CurrentDSN = 2, 1200, 90))
			While iReader.Read()
				Dim cName As String = IIf(iReader(0) Is DBNull.Value, "", iReader(0))
				If mItemList.Contains(cName) Then
					Item(cName).MCTotalCount += dbPages.TestVar(iReader("tVal"), 0, -1, 0)
					mTotal.MCTotalCount += dbPages.TestVar(iReader("tVal"), 0, -1, 0)
				End If
			End While
			iReader.Close()
		End If
		If mSortType <> eSortType.eST_ItemID Then
			mSortList = New System.Collections.ArrayList(mItemList.Values)
			mSortList.Sort(Me)
		End If
	End Sub

	Public ReadOnly Property Values As ICollection 
		Get
			If mSortList Is Nothing Then Return mItemList.Values
			Return mSortList
		End Get
	End Property

	Public Function Compare (x As Object, y As Object) As Integer Implements IComparer.Compare
		Select mSortType
		Case eSortType.eST_Item
			Return String.Compare(CType(x, RptData).ItemName, CType(y, RptData).ItemName)
		Case eSortType.eST_TransCount
			Return CType(y, RptData).TransCount - CType(x, RptData).TransCount
		Case eSortType.eST_TransSum
			Return CType(y, RptData).TransSum - CType(x, RptData).TransSum
		Case eSortType.eST_ChbCount
			Return CType(y, RptData).ChbCount - CType(x, RptData).ChbCount
		Case eSortType.eST_ChbSum
			Return CType(y, RptData).ChbSum - CType(x, RptData).ChbSum
		Case eSortType.eST_RefundCount
			Return CType(y, RptData).RefundCount - CType(x, RptData).RefundCount
		Case eSortType.eST_RefundSum
			Return CType(y, RptData).RefundSum - CType(x, RptData).RefundSum
		Case eSortType.eST_Costs 
			Return CType(y, RptData).RunningCosts - CType(x, RptData).RunningCosts
		Case eSortType.eST_Total
			Return CType(y, RptData).Total - CType(x, RptData).Total
		End Select
		Return Nothing
	End Function
	
	Public Function FormatCurr(ByVal trnCurr As Byte, ByVal trnValue As Decimal, ByVal trnCount As Integer, ByVal trnPrecent As Decimal, ByVal xLink As String, ByVal xLinkAdd As String) As String
		Dim tmpString As String = ""
		If trnValue = 0 And trnCount = 0 And trnPrecent = 0 Then Return "&nbsp;"
		If xLink <> "" Then tmpString &= "<a style=""text-decoration:none;"" href=""" & xLink & xLinkAdd & """>"
		If trnCount <> -1 Then tmpString &= "<span class=""dim"">(" & CInt(trnCount * GlobalScale) & ")</span> "
		If trnPrecent <> 0 Then tmpString &= "<span class=""dim"">(" & Decimal.Round(trnPrecent * 100, 2) & "%)</span> "
		tmpString &= "<span style=""color:" & IIF(trnValue < 0, "red;", "") & """>" & dbPages.FormatCurr(trnCurr, trnValue * GlobalScale) & "</span>"
		If xLink <> "" And tmpString <> "" Then tmpString &= "</a>"
		FormatCurr = tmpString
	End Function

	Public Sub DrawHeader(ByVal Response As System.IO.TextWriter, ByVal SumType As eSumType)
		Response.Write("<th style=""text-align:left;"">Item</th>")
		If SumType And eSumType.ePS_ShowFailed Then
			Response.Write("<th>Charge Attempts</th>")
			Response.Write("<td width=""25"">&nbsp;</td>")
		End If
		Response.Write("<th>Transactions</th>")
		If (SumType And eSumType.ePS_WithCompare) > 0 Then Response.Write("<th>Comparison</th>")
		Response.Write("<th>Refunds</th>")
		If (SumType And eSumType.ePS_CalcPMMCHBRatio) > 0 Then 
			Response.Write("<th>MC Chb</th>")
			Response.Write("<th>Visa Chb</th>")
		End If
		Response.Write("<th>Chb</th>")
		If SumType And eSumType.ePS_ShowFees Then
			Response.Write("<th>Chb Fees</th>")
			Response.Write("<th>Handling Fees</th>")
		End If
		Response.Write("<th>Sub Total</th>")
		If SumType And eSumType.ePS_ShowRolling Then
			Response.Write("<th>Rolling Res</th>")
			Response.Write("<th>Rolling Ret</th>")
		End If
		If SumType And eSumType.ePS_ShowCosts Then
			Response.Write("<th>Costs</th>")
		End If
		If SumType And eSumType.ePS_ShowFailed Then
			' debit
			Response.Write("<td width=""25"">&nbsp;</td>")
			Response.Write("<th>Declines Total</th>")
			Response.Write("<th>Gateway Declines</th>")
			Response.Write("<th>Risk Declines</th>")
			Response.Write("<th>Issuer Declines</th>")
			
			' pre-authorized
			Response.Write("<td width=""25"">&nbsp;</td>")
			Response.Write("<th>Declines Total (Pre-authorized)</th>")
			Response.Write("<th>Gateway Declines (Pre-authorized)</th>")
			Response.Write("<th>Risk Declines (Pre-authorized)</th>")
			Response.Write("<th>Issuer Declines (Pre-authorized)</th>")
		End If
	End Sub

	Public Shared Function GetRatio(ByVal x1 As Decimal, ByVal x2 As Decimal) As Decimal
		If (x1 <> 0 And x2 <> 0) Then Return (x1 / x2) Else Return 0
	End Function

	Public Sub DrawRow(ByVal Response As System.IO.TextWriter, ByVal p As RptData, ByVal SumType As eSumType, ByVal ItemLink As String, ByVal DataLink As String, ByVal IconPath As String)
		Response.Write("<td style=""text-align:left; padding-left:5px;"">")
		If TreeEvent <> "" And Not (p.ItemID Is Nothing) Then Response.Write("<img border=""0"" style=""cursor:pointer;"" " & TreeEvent.Replace("$1", p.ItemID) & " src=""/NPCommon/images/tree_expand.gif"" align=""middle"">")
		If IconPath <> "" Then _
		If System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(IconPath & p.ItemID & ".gif")) Then Response.Write("<img src=""" & IconPath & p.ItemID & ".gif"" align=""middle"">")
		If ItemLink <> "" Then Response.Write("&nbsp;<a href=""" & ItemLink & p.ItemID & """>")
		Response.Write(dbPages.dbtextShow(p.ItemName) & "</a></td>")
		If SumType And eSumType.ePS_ShowFailed Then
			Response.Write("<td>" & FormatCurr(p.Currency, p.TotalProcessed, p.TotalProcessedCount, 0, "", "") & "</td>")
			Response.Write("<td style=""background-color:white;""></td>")
		End If
		Response.Write("<td>" & FormatCurr(p.Currency, p.TransSum, p.TransCount, GetRatio(p.TransSum, p.TotalProcessed), DataLink, p.ItemID & "&TypeTransNormal=1&Currency=" & p.Currency) & "</td>")
		If (SumType And eSumType.ePS_WithCompare) > 0 Then _
		 Response.Write("<td>" & FormatCurr(p.Currency, p.TransSumCmp1, p.TransCountCmp1, GetRatio(p.TransSum - p.TransSumCmp1, p.TransSumCmp1), DataLink, Nothing) & "&nbsp;<img src=""/NPCommon/images/" & IIf(p.TransSumCmp1 > p.TransSum, "icon_ArrowR2.gif", "icon_ArrowG2.gif") & """></td>")
		Response.Write("<td>" & FormatCurr(p.Currency, p.RefundSum, p.RefundCount, 0, DataLink, p.ItemID & "&TypeTransRefund=1&Currency=" & p.Currency) & "</td>")
		If (SumType And eSumType.ePS_CalcPMMCHBRatio) > 0 Then
			Response.Write("<td><span class=""dim"">(" & CInt(p.MCChbCount * GlobalScale) & " / " & (p.MCTotalCount * GlobalScale) & ")</span> " & Decimal.Round(GetRatio(p.MCChbCount, p.MCTotalCount) * 100, 2) & "%</td>")
			Response.Write("<td><span class=""dim"">(" & CInt(p.VisaChbCount * GlobalScale) & " / " & (p.VisaTotalCount * GlobalScale) & ")</span> " & Decimal.Round(GetRatio(p.VisaChbCount, p.VisaTotalCount) * 100, 2) & "%</td>")
		End If
		Response.Write("<td>" & FormatCurr(p.Currency, p.ChbSum, p.ChbCount, -IIf(SumType And eSumType.ePS_ChbRatioCount, p.ChbCountRatio, p.ChbRatio), DataLink, p.ItemID & "&deniedStatus=2&Currency=" & p.Currency) & "</td>")
		If SumType And eSumType.ePS_ShowFees Then
			Response.Write("<td>" & FormatCurr(p.Currency, p.FeeChbSum, p.ChbCount, 0, DataLink, p.ItemID & "&deniedStatus=2&Currency=" & p.Currency) & "</td>")
			Response.Write("<td>" & FormatCurr(p.Currency, p.FeeSum, -1, 0, DataLink, p.ItemID & "&Currency=" & p.Currency) & "</td>")
		End If
		Response.Write("<td>" & FormatCurr(p.Currency, IIf(SumType And eSumType.ePS_ShowFees, p.Total, p.SubTotal), -1, 0, DataLink, p.ItemID & "&Currency=" & p.Currency) & "</td>")
		If SumType And eSumType.ePS_ShowRolling Then
			Response.Write("<td>" & FormatCurr(p.Currency, p.RollingRes, -1, 0, DataLink, p.ItemID & "&PaymentMethod=4&TypeTransRefund=1&Currency=" & p.Currency) & "</td>")
			Response.Write("<td>" & FormatCurr(p.Currency, p.RollingRet, -1, 0, DataLink, p.ItemID & "&PaymentMethod=4&Currency=" & p.Currency) & "</td>")
		End If
		If SumType And eSumType.ePS_ShowCosts Then
			Response.Write("<td>" & FormatCurr(p.Currency, p.RunningCosts, 0, 0, "", "") & "</td>")
		End If
		If SumType And eSumType.ePS_ShowFailed Then
			' debit
			Response.Write("<td style=""background-color:white;""></td>")
			Response.Write("<td>" & FormatCurr(p.Currency, p.TotalFail, p.TotalFailCount, GetRatio(p.TotalFail, p.TotalProcessed), "", "") & "</td>")
			Response.Write("<td>" & FormatCurr(p.Currency, p.NPFailSum, p.NPFailCount, 0, "", "") & "</td>")
			Response.Write("<td>" & FormatCurr(p.Currency, p.GRFailSum, p.GRFailCount, 0, "", "") & "</td>")
			Response.Write("<td>" & FormatCurr(p.Currency, p.IFailSum, p.IFailCount, 0, "", "") & "</td>")

			' pre-authorized
			Response.Write("<td style=""background-color:white;""></td>")
			Response.Write("<td>" & FormatCurr(p.Currency, p.TotalFailPA, p.TotalFailCountPA, GetRatio(p.TotalFailPA, p.TotalProcessed), "", "") & "</td>")
			Response.Write("<td>" & FormatCurr(p.Currency, p.NPFailSumPA, p.NPFailCountPA, 0, "", "") & "</td>")
			Response.Write("<td>" & FormatCurr(p.Currency, p.GRFailSumPA, p.GRFailCountPA, 0, "", "") & "</td>")
			Response.Write("<td>" & FormatCurr(p.Currency, p.IFailSumPA, p.IFailCountPA, 0, "", "") & "</td>")
		End If
	End Sub

	Public Sub DrawSpacer(ByVal Response As System.IO.TextWriter, ByVal SumType As eSumType, ByVal nStyle As String)
		Exit Sub
        Response.Write("<tr><td colspan=""2"" hight=""1"" style=""border-top:1px " & nStyle & " silver""><img src=""../images/1_space.gif"" width=""1"" height=""1""></td><td></td>")
        Response.Write("<td colspan=""" & (IIf(SumType And eSumType.ePS_ShowFees, 6, 4) + IIf(SumType And eSumType.ePS_ShowRolling, 2, 0)) & """ hight=""1"" style=""border-top:1px " & nStyle & " silver""><img src=""../images/1_space.gif"" width=""1"" height=""1""></td>")
        If SumType And eSumType.ePS_ShowFailed Then Response.Write("<td></td><td colspan=""4"" hight=""1"" style=""border-top:1px " & nStyle & " silver""><img src=""../images/1_space.gif"" width=""1"" height=""1""></td>")
        Response.Write("</tr>")
	End Sub

	Public Sub DrawData(ByVal Response As System.IO.TextWriter, ByVal SumType As eSumType, ByVal ItemLink As String, ByVal DataLink As String, ByVal IconPath As String, ByVal nMinTotalToShow As Decimal)
		Dim nCount As Integer
		If SumType And eSumType.ePS_DrawCols Then
			Response.Write("<tr>")
			DrawHeader(Response, SumType)
			Response.Write("</tr>" & vbCrLf)
		End If
		For Each p As RptData In Values
			If p.TransSum >= nMinTotalToShow Then
					 nCount = nCount + 1
				If nCount > 1 Then DrawSpacer(Response, SumType, "dotted")
				Response.Write("<tr " & RowEvent & " class=""" & IIF((nCount Mod 2) = 0, "RowZebra", "") & """>")
				DrawRow(Response, p, SumType, ItemLink, DataLink, IconPath)
				Response.Write("</tr>" & vbCrLf)
			End If
		Next
		DrawSpacer(Response, SumType, "solid")
		If SumType And eSumType.ePS_ShowTotal Then
			Response.Write("<tr class=""totals"">")
			DrawRow(Response, mTotal, SumType, "", "", "")
			Response.Write("</tr>" & vbCrLf)
		End If
	End Sub

	Public Sub Export(ByVal Response As System.IO.TextWriter, ByVal SumType As eSumType, ByVal bDrawHeader As Boolean)
		If bDrawHeader Then
			Response.Write("<tr>")
			Response.Write("<th>Item</th>")
			Response.Write("<th>Text</th>")
			Response.Write("<th>Currency</th>")
			If SumType And eSumType.ePS_ShowFailed Then
				Response.Write("<th>Charge Attempt Amount</th>")
				Response.Write("<th>Charge Attempt Count</th>")
			End If
			Response.Write("<th>Transactions Amount</th>")
			Response.Write("<th>Transactions Count</th>")
			Response.Write("<th>Refund Amount</th>")
			Response.Write("<th>Refund Count</th>")
			If (SumType And eSumType.ePS_CalcPMMCHBRatio) > 0 Then
				Response.Write("<th>MC Chb</th>")
				Response.Write("<th>Visa Chb</th>")
			End If
			Response.Write("<th>Chb Amount</th>")
			Response.Write("<th>Chb Count</th>")
			Response.Write("<th>Sub Total</th>")
			If SumType And eSumType.ePS_ShowFees Then
				Response.Write("<th>Trans Fees Chb</th>")
				Response.Write("<th>Trans Fees Sum</th>")
			End If
			If SumType And eSumType.ePS_ShowFailed Then
				' debit
				Response.Write("<th>Issuer Declines Amount</th>")
				Response.Write("<th>Issuer Declines Count</th>")
				Response.Write("<th>Gateway Declines Amount</th>")
				Response.Write("<th>Gateway Declines Count</th>")
				Response.Write("<th>Risk Declines Amount</th>")
				Response.Write("<th>Risk Declines Count</th>")

				' pre-authorized				
				Response.Write("<th>Issuer Declines Amount (Pre-authorized)</th>")
				Response.Write("<th>Issuer Declines Count (Pre-authorized)</th>")
				Response.Write("<th>Gateway Declines Amount (Pre-authorized)</th>")
				Response.Write("<th>Gateway Declines Count (Pre-authorized)</th>")
				Response.Write("<th>Risk Declines Amount (Pre-authorized)</th>")
				Response.Write("<th>Risk Declines Count (Pre-authorized)</th>")
			End If
			Response.Write("</tr>")
		End If
		Dim nfi As System.Globalization.NumberFormatInfo = New System.Globalization.CultureInfo("en-US", False).NumberFormat
		nfi.NumberGroupSizes = New Integer() {3}
		nfi.NumberGroupSeparator = ","
		nfi.NumberDecimalDigits = 2
		For Each p As RptData In mItemList.Values
			Response.Write("<tr>")
			Response.Write("<td>" & p.ItemID & "</td>")
			Response.Write("<td>" & p.ItemName & "</td>")
			Response.Write("<td>" & dbPages.GetCurText(p.Currency) & "</td>")
			If SumType And eSumType.ePS_ShowFailed Then
				Response.Write("<td>" & p.TotalProcessed.ToString("N", nfi) & "</td>")
				Response.Write("<td>" & p.TotalProcessedCount & "</td>")
			End If
			Response.Write("<td>" & p.TransSum.ToString("N", nfi) & "</td>")
			Response.Write("<td>" & p.TransCount & "</td>")
			Response.Write("<td>" & p.RefundSum.ToString("N", nfi) & "</td>")
			Response.Write("<td>" & p.RefundCount & "</td>")
			If (SumType And eSumType.ePS_CalcPMMCHBRatio) > 0 Then
				Response.Write("<td><span class=""dim"">(" & (p.MCChbCount * GlobalScale) & " / " & (p.MCTotalCount * GlobalScale) & ")</span> " & Decimal.Round(GetRatio(p.MCChbCount, p.MCTotalCount) * 100, 2) & "%</td>")
				Response.Write("<td><span class=""dim"">(" & (p.VisaChbCount * GlobalScale) & " / " & (p.VisaTotalCount * GlobalScale) & ")</span> " & Decimal.Round(GetRatio(p.VisaChbCount, p.VisaTotalCount) * 100, 2) & "%</td>")
			End If
			Response.Write("<td>" & p.ChbSum.ToString("N", nfi) & "</td>")
			Response.Write("<td>" & p.ChbCount & "</td>")
			Response.Write("<td>" & IIf(SumType And eSumType.ePS_ShowFees, p.Total, p.SubTotal).ToString("0.00") & "</td>")
			If SumType And eSumType.ePS_ShowFees Then
				Response.Write("<td>" & p.FeeChbSum.ToString("N", nfi) & "</td>")
				Response.Write("<td>" & p.FeeSum.ToString("N", nfi) & "</td>")
			End If
			If SumType And eSumType.ePS_ShowFailed Then
				' debit
				Response.Write("<td>" & p.IFailSum.ToString("N", nfi) & "</td>")
				Response.Write("<td>" & p.IFailCount & "</td>")
				Response.Write("<td>" & p.NPFailSum.ToString("N", nfi) & "</td>")
				Response.Write("<td>" & p.NPFailCount & "</td>")
				Response.Write("<td>" & p.GRFailSum.ToString("N", nfi) & "</td>")
				Response.Write("<td>" & p.GRFailCount & "</td>")

				' pre-authorized
				Response.Write("<td>" & p.IFailSumPA.ToString("N", nfi) & "</td>")
				Response.Write("<td>" & p.IFailCountPA & "</td>")
				Response.Write("<td>" & p.NPFailSumPA.ToString("N", nfi) & "</td>")
				Response.Write("<td>" & p.NPFailCountPA & "</td>")
				Response.Write("<td>" & p.GRFailSumPA.ToString("N", nfi) & "</td>")
				Response.Write("<td>" & p.GRFailCountPA & "</td>")
			End If
			Response.Write("</tr>")
		Next
	End Sub

    'Public Sub fillGraph(ByVal fGraph As Telerik.Web.UI.RadChart, ByVal chartType As ChartSeriesType, bShowTitle As Boolean, ByVal sColor As System.Drawing.Color, ByVal nShowItem As Integer, ByVal nTitle As String)
    '    Dim nSeries As New Telerik.Web.UI.RadChartSeries()
    '    nSeries.Type = chartType
    '    nSeries.Appearance.FillStyle = FillStyle.Solid
    '    nSeries.Name = nTitle
    '    nSeries.MainColor = sColor
    '    nSeries.LabelAppearance.Visible = False : nSeries.ShowLabels = False
    '    If bShowTitle Then nSeries.LegendDisplayMode = ChartSeriesLegendDisplayMode.ItemLabels
    '    For Each p As RptData In Values
    '        Dim nValue As Decimal = 0
    '        Select Case nShowItem
    '            Case 0 : nValue = p.TransSum
    '            Case 1 : nValue = p.Total
    '            Case 2 : nValue = p.TransCount
    '            Case 3 : nValue = p.ChbSum
    '            Case 4 : nValue = p.ChbCount
    '        End Select
    '        If bShowTitle Then
    '            nSeries.AddItem(nValue, p.ItemName)
    '            'fGraph.YAxis.Add.AddItem(p.ItemName)
    '        Else
    '            nSeries.AddItem(nValue)
    '        End If
    '    Next
    '    fGraph.Series.Add(nSeries)
    'End Sub
End Class
