Imports Microsoft.VisualBasic

Public Enum InvikRecordType
	FileHeader
	BatchHeader
	TransactionDebit
	TransactionCredit
	TransactionReversal
	TransactionInfo
	BatchTrailer
	FileTrailer
End Enum

Public Enum InvikBatchType
	CreditBatch
	DebitBatch
	MixedBatch
End Enum

Public Class Invik
	Public Const RecordSeparator As String = vbCrLf
	Private Const NoTerminalPOS As String = "20351000061000000000000000"
	Private Const No3DSecureUCAF As String = "00"

	Private Shared Currencies(eCurrencies.MAX_CURRENCY + 1) As String

	Public Shared Function FormatDate(Optional ByVal dDate As Date = Nothing, Optional ByVal bShowTime As Boolean = False) As String
		If dDate = Nothing Then dDate = Date.Now
		Return dDate.ToString(IIf(bShowTime, "yyyyMMddhhmm", "yyyyMMdd"))
	End Function

	Public Shared Function FormatText(ByVal sText As String, Optional ByVal nLength As Integer = Nothing) As String
		sText = sText.Trim
		If nLength <> Nothing And nLength > 0 Then
			If sText.Length > nLength Then sText = sText.Substring(0, nLength)
			sText = sText.PadRight(nLength)
		End If
		Return sText
	End Function

	Public Shared Function FormatNumber(ByVal nNumber As Integer, Optional ByVal nLength As Integer = Nothing) As String
		Dim sText = nNumber.ToString
		sText = sText.Trim
		If nLength <> Nothing And nLength > 0 Then
			If sText.Length > nLength Then sText = sText.Substring(0, nLength)
			sText = sText.PadLeft(nLength, "0")
		End If
		Return sText
	End Function

	Public Shared Function FormatAmount(ByVal nAmount As Decimal, Optional ByVal nLength As Integer = 12) As String
        Return FormatNumber(System.Math.Abs(nAmount) * 100, nLength)
	End Function

	Public Shared Function FormatValidity(ByVal nYear As Integer, ByVal nMonth As Integer) As String
        Dim nYY As Integer : System.Math.DivRem(nYear, 100, nYY)
		Return FormatNumber(nYY, 2) & FormatNumber(nMonth, 2)
	End Function

	Public Shared Function Filler(Optional ByVal nLength As Integer = 1) As String
		Return String.Empty.PadRight(IIf(nLength <> Nothing And nLength > 1, nLength, 1))
	End Function

	Public Shared ReadOnly Property FileSequenceNumber() As Integer
		Get
			Return dbPages.TestVar(dbPages.ExecScalar("SELECT Max(irb_DownloadFileNumber) FROM tblInvikRefundBatch"), 1, 0, 0) + 1
		End Get
	End Property

	Public Shared ReadOnly Property SenderFileReference() As Integer
		Get
			Return dbPages.TestVar(dbPages.ExecScalar("SELECT Min(ID) FROM tblInvikRefundBatch WHERE irb_IsDownloaded=0"), 1, 0, 0)
		End Get
	End Property

	Public Shared Function Currency(ByVal nCurrency As Integer) As String
        If String.IsNullOrEmpty(Currencies(nCurrency)) Then Currencies(nCurrency) = dbPages.ExecScalar("SELECT CurrencyISOCode FROM [List].[CurrencyList] WHERE CurrencyID=" & nCurrency)
		Return Currencies(nCurrency)
	End Function

	Public Shared Function RecordType(ByVal irtType As InvikRecordType) As String
		Select Case irtType
			Case InvikRecordType.BatchHeader : Return "BH"
			Case InvikRecordType.BatchTrailer : Return "BT"
			Case InvikRecordType.FileHeader : Return "FH"
			Case InvikRecordType.FileTrailer : Return "FT"
			Case InvikRecordType.TransactionCredit : Return "TC"
			Case InvikRecordType.TransactionDebit : Return "TD"
			Case InvikRecordType.TransactionInfo : Return "TI"
			Case InvikRecordType.TransactionReversal : Return "TR"
		End Select
		Throw New OverflowException("Unsupported InvikRecordType: " & irtType.ToString)
	End Function

	Public Shared Function BatchType(ByVal ibtType As InvikBatchType) As String
		Select Case ibtType
			Case InvikBatchType.CreditBatch : Return "CR"
			Case InvikBatchType.DebitBatch : Return "DB"
			Case InvikBatchType.MixedBatch : Return "XX"
		End Select
		Throw New OverflowException("Unsupported InvikBatchType: " & ibtType.ToString)
	End Function

	Public Shared Function TransactionSign(ByVal irtType As InvikRecordType, Optional ByVal sDefault As String = "+") As String
		Select Case irtType
			Case InvikRecordType.TransactionCredit : Return "-"
			Case InvikRecordType.TransactionDebit : Return "+"
			Case InvikRecordType.TransactionReversal : Return FormatText(sDefault, 1)
		End Select
		Throw New OverflowException("InvikRecordType InvikBatchType: " & irtType.ToString)
	End Function

	Public Shared Function FileHeaderRecord( _
	) As String
		Return FormatNumber(1, 6) & RecordType(InvikRecordType.FileHeader) & Filler() & FormatDate() & FormatNumber(FileSequenceNumber, 6) _
		 & FormatNumber(SenderFileReference, 6) & Filler() & FormatText("NETPAY", 20) & FormatText("BANQUE INVIK", 20) & Filler() _
		 & FormatText("A2ZPay", 15) & FormatNumber(37, 15) & Filler() & FormatText("NPA", 3) & FormatText("BI", 2) & Filler(40) & RecordSeparator
	End Function

	Public Shared Function BatchHeaderRecord( _
		ByVal nRecordNumber As Integer, ByVal nBatchNumber As Integer, ByVal ibtBatchType As InvikBatchType, ByVal nCurrency As Integer, _
		ByVal sMerchantReference As String, ByVal sMerchantName As String, ByVal nMCC As Integer _
	) As String
		Return FormatNumber(nRecordNumber, 6) & RecordType(InvikRecordType.BatchHeader) & Filler() & FormatNumber(nBatchNumber, 6) & FormatDate() & BatchType(ibtBatchType) _
		& Currency(nCurrency) & Filler() & FormatText(sMerchantReference, 15) & FormatText(sMerchantName, 25) & FormatText(nMCC.ToString, 6) & Filler(73) & RecordSeparator
	End Function

	Public Shared Function TransactionRecord( _
		ByVal nRecordNumber As Integer, ByVal irtTransType As InvikRecordType, ByVal sCardNumber As String, ByVal nExpYear As Integer, ByVal nExpMonth As Integer, _
		ByVal nTransaction As Integer, ByVal nAmount As Decimal, Optional ByVal dTransDate As Date = Nothing, Optional ByVal sAuthorization As String = Nothing, _
		Optional ByVal nTransRefunded As Integer = Nothing, Optional ByVal nResponseCode As Integer = -1, Optional ByVal nProcessingCode As Integer = 0 _
	) As String
		Return FormatNumber(nRecordNumber, 6) & RecordType(irtTransType) & Filler() & FormatText(sCardNumber, 19) & FormatValidity(nExpYear, nExpMonth) & FormatNumber(nTransaction, 12) _
		& FormatDate(IIf(dTransDate = Nothing, Date.Now, dTransDate), True) & TransactionSign(irtTransType) & FormatAmount(nAmount) _
		& FormatText(IIf(String.IsNullOrEmpty(sAuthorization), " ", sAuthorization), 6) & FormatNumber(0, 12) & FormatNumber(nTransRefunded, 12) & Filler() _
		& IIf(nResponseCode < 0, FormatText(String.Empty, 2), FormatNumber(nResponseCode, 2)) & FormatNumber(nProcessingCode, 6) & FormatText(String.Empty, 1) & Filler(33) & RecordSeparator
	End Function

	Public Shared Function InfoRecord( _
		ByVal nRecordNumber As Integer, ByVal sCardNumber As String, ByVal sMerchantCity As String, ByVal sCardAcceptor As String, ByVal sDescription As String, _
		Optional ByVal nAltFieldsType As Integer = 22, Optional ByVal sTerminalReference As String = Nothing, Optional ByVal sRetrievalReferenceNumber As String = Nothing _
	) As String
		Return FormatNumber(nRecordNumber, 6) & RecordType(InvikRecordType.TransactionInfo) & Filler() & FormatText(sCardNumber, 19) & FormatNumber(nAltFieldsType, 4) & Filler() _
		& FormatText(sMerchantCity, 20) & FormatText(sDescription, 20) & FormatText(IIf(String.IsNullOrEmpty(sTerminalReference), String.Empty, sTerminalReference), 12) _
		& FormatText(IIf(String.IsNullOrEmpty(sRetrievalReferenceNumber), String.Empty, sRetrievalReferenceNumber), 12) & FormatText(sCardAcceptor, 15) & FormatNumber(2, 3) _
		& FormatNumber(59, 2) & NoTerminalPOS & No3DSecureUCAF & Filler(3) & RecordSeparator
	End Function

	Public Shared Function BatchTrailerRecord( _
		ByVal nRecordNumber As Integer, ByVal nBatchNumber As Integer, ByVal nCountInfo As Integer, ByVal nCountDebit As Integer, ByVal nAmountDebit As Decimal, _
		ByVal nCountCredit As Integer, ByVal nAmountCredit As Decimal _
	) As String
		Return FormatNumber(nRecordNumber, 6) & RecordType(InvikRecordType.BatchTrailer) & Filler() & FormatNumber(nBatchNumber, 6) & FormatNumber(nCountInfo, 6) & Filler() _
		& FormatNumber(nCountDebit, 6) & FormatAmount(nAmountDebit, 16) & Filler() & FormatNumber(nCountCredit, 6) & FormatAmount(nAmountCredit, 16) & Filler(81) & RecordSeparator
	End Function

	Public Shared Function FileTrailerRecord( _
		ByVal nRecordNumber As Integer, ByVal nCountBatch As Integer, ByVal nCountRecord As Integer, ByVal nCountInfo As Integer, ByVal nCountDebit As Integer, ByVal nAmountDebit As Decimal, _
		ByVal nCountCredit As Integer, ByVal nAmountCredit As Decimal _
	) As String
		Return FormatNumber(nRecordNumber, 6) & RecordType(InvikRecordType.BatchTrailer) & Filler() & FormatNumber(nCountBatch, 6) & FormatNumber(nCountRecord, 6) _
		& FormatNumber(nCountInfo, 6) & Filler() & FormatNumber(nCountDebit, 6) & FormatAmount(nAmountDebit, 16) & Filler() _
		& FormatNumber(nCountCredit, 6) & FormatAmount(nAmountCredit, 16) & Filler(75) & RecordSeparator
	End Function
End Class