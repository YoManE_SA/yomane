﻿Public Class GlobalDataValue
	Public ReadOnly Language As Integer, Group As Integer, ID As Integer, Flags As Integer
	Public ReadOnly Text As String, Color As String, Description As String, Country As String

	Public Sub New(ByVal nLanguage As Integer, ByVal nGroup As Integer, ByVal nID As Integer, ByVal nFlags As Integer, ByVal sText As String, ByVal sColor As String, ByVal sDescription As String)
		Language = nLanguage
		Group = nGroup
		ID = nID
		Flags = nFlags
		Text = sText
		Color = sColor
		Description = sDescription
	End Sub

	Public Sub New()
		Language = 1
		Group = 0
		ID = 0
		Flags = 0
		Text = String.Empty
		Color = String.Empty
		Description = String.Empty
	End Sub
End Class

Public Module GlobalData
	Private Expires As Date
	Private Values As New System.Collections.Generic.Dictionary(Of String, GlobalDataValue)

	Public Sub Load()
		Dim drData As System.Data.SqlClient.SqlDataReader = Nothing
		Try
			Dim sSQL As String = "SELECT GD_Lng, GD_Group, GD_ID, GD_Flags, IsNull(GD_Text, '') GD_Text, IsNull(GD_Color, '') GD_Color, IsNull(GD_Description, '') GD_Description FROM tblGlobalData"
			drData = dbPages.ExecReader(sSQL)
			Values.Clear()
		    While drData.Read
		        Dim sKey As String = drData(0) & "|" & drData(1) & "|" & drData(2)
			    Values.Add(sKey, New GlobalDataValue(drData(0), drData(1), drData(2), drData(3), drData(4), drData(5), drData(6)))
		    End While
		    Expires = Date.Now.AddHours(dbPages.TestVar(dbPages.getConfig("GlobalDataExpireHours"), 1, 1000, 12))
		Catch
		Finally
    		If Not drData Is Nothing Then drData.Close()
		End Try
	End Sub

	Public Function Value(ByVal nGroup As GlobalDataGroup, ByVal nID As Integer, Optional ByVal nLanguage As Integer = 1) As GlobalDataValue
		Return Value(CType(nGroup, Integer), nID, nLanguage)
	End Function

	Public Function Value(ByVal nGroup As Integer, ByVal nID As Integer, Optional ByVal nLanguage As Integer = 1) As GlobalDataValue
		If Values.Count = 0 Or Expires < Date.Now Then Load()
        Dim sKey As String = nLanguage & "|" & nGroup & "|" & nID
        If Values.ContainsKey(sKey) Then Return Values(sKey)
		Load()
        If Values.ContainsKey(sKey) Then Return Values(sKey)
        Return New GlobalDataValue()
	End Function

	Public Function GroupValues(ByVal nGroup As GlobalDataGroup, Optional ByVal nLanguage As Integer = 1) As System.Collections.Generic.List(Of GlobalDataValue)
		Return GroupValues(CType(nGroup, Integer), nLanguage)
	End Function

	Public Function GroupValues(ByVal nGroup As Integer, Optional ByVal nLanguage As Integer = 1) As System.Collections.Generic.List(Of GlobalDataValue)
		If Values.Count = 0 Or Expires < Date.Now Then Load()
		Dim lstGroupValues As New System.Collections.Generic.List(Of GlobalDataValue)
		For Each gdv As GlobalDataValue In Values.Values
			If gdv.Group = nGroup And gdv.Language = nLanguage Then lstGroupValues.Add(gdv)
		Next
		Return lstGroupValues
	End Function
End Module

Public Enum GlobalDataGroup
	GLOBAL_DATA_GROUPS = 0
	PASSWORD_CHANGE_ATTEMPT_RESULT = 35
	PAYMENT_METHOD_TYPE = 36
	TRANSACTION_TYPE = 37
	WHITE_LIST_REPLY = 38
	WHITE_LIST_LEVEL = 39
	WIRE_STATUS = 40
	DENIED_STATUS = 41
	CREDIT_TYPES = 43
	MERCHANT_STATUS = 44
	BALANCE_STATUS = 45
	REPLY_SOURCE = 46
	COMMON_BLOCK_TYPES = 47
	WIRE_TRANSFER_STATUS = 48
	TRANSPASS_ACTION = 49
	PENDING_FILES_STATUS = 50
	INDUSTRY = 51
	BENEFICIARY = 52
	BALANCE_SOURCE = 53
	RECURRING_MODIFICATION_STATUS = 54
	ROLLING_RESERVE_OPTION = 55
	DOCUMENT_TYPE = 60
	LIMITED_LOGIN_PERMISSIONS = 61
	RECURRING_CHARGE_STATUS = 62
	REFUND_REQUEST_STATUS = 63
	REFUND_REQUEST_REPLY_CODE = 64
	AUTOREFUND_STATUS = 65
	CUSTOMER_BALANCE_SOURCE = 67
	MANAGING_COMPANY = 69
	PSP = 70
End Enum