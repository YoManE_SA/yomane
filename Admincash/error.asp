<%
	Function ToDB(nLength, sString)
		If sString="" then
			ToDB=""
		Else
			for i=1 to nLength
				if Len(sString)<nLength then sString=sString & " "
			next
			ToDB=Left(Replace(Replace(sString, "'", "`"),",","|"), nLength)
		End If
	End Function

	sSQL="INSERT INTO tblErrorNet (ProjectName, RemoteIP, LocalIP, RemoteUser, ServerName," & _
	" ServerPort, ScriptName, RequestQueryString, RequestForm, VirtualPath, PhysicalPath," & _
	" ExceptionSource, ExceptionMessage, ExceptionTargetSite, InnerExceptionSource," & _
	" InnerExceptionTargetSite, ExceptionLineNumber, ExceptionStackTrace, InnerExceptionStackTrace) VALUES ("
	sSQL=sSQL & "'" & ToDB(25, Application("ProjectName")) & "'"
	sSQL=sSQL & ", '" & ToDB(25, Request.ServerVariables("REMOTE_ADDR")) & "'"
	sSQL=sSQL & ", '" & ToDB(25, Request.ServerVariables("LOCAL_ADDR")) & "'"
	sSQL=sSQL & ", '" & ToDB(50, Request.ServerVariables("REMOTE_USER")) & "'"
	sSQL=sSQL & ", '" & ToDB(50, Request.ServerVariables("SERVER_NAME")) & "'"
	sSQL=sSQL & ", '" & ToDB(5, Request.ServerVariables("SERVER_PORT")) & "'"
	sSQL=sSQL & ", '" & ToDB(50, Request.ServerVariables("SCRIPT_NAME")) & "'"
	sSQL=sSQL & ", '" & ToDB(500, Request.QueryString) & "'"
	sSQL=sSQL & ", '" & ToDB(500, Request.Form) & "'"
	sSQL=sSQL & ", '" & ToDB(50, Request.ServerVariables("PATH_INFO")) & "'"
	sSQL=sSQL & ", '" & ToDB(100, Request.ServerVariables("PATH_TRANSLATED")) & "'"
	if Session("SecurityUserVerified")="1" then
		sTable="<table cellpadding=""1"" cellspacing=""0"" border=""1"" bordercolor=""silver"" frame=""box"" style=""font-size:11px;vertical-align:top;"">"
		sTable=sTable & "<tr><th colspan=""2"" style=""color:red;"">ASP ERROR OCCURED</th></tr>"
		sTable=sTable & "<tr><th>Field</th><th>Value</th></tr>"
		sTable=sTable & "<tr><td>ProjectName</td><td>" & Application("ProjectName") & "</td></tr>"
		sTable=sTable & "<tr><td>RemoteIP</td><td>" & Request.ServerVariables("REMOTE_ADDR") & "</td></tr>"
		sTable=sTable & "<tr><td>LocalIP</td><td>" & Request.ServerVariables("LOCAL_ADDR") & "</td></tr>"
		sTable=sTable & "<tr><td>RemoteUser</td><td>" & Request.ServerVariables("REMOTE_USER") & "</td></tr>"
		sTable=sTable & "<tr><td>ServerName</td><td>" & Request.ServerVariables("SERVER_NAME") & "</td></tr>"
		sTable=sTable & "<tr><td>ServerPort</td><td>" & Request.ServerVariables("SERVER_PORT") & "</td></tr>"
		sTable=sTable & "<tr><td>ScriptName</td><td>" & Request.ServerVariables("SCRIPT_NAME") & "</td></tr>"
		sTable=sTable & "<tr><td>RequestQueryString</td><td>" & Request.QueryString & "</td></tr>"
		sTable=sTable & "<tr><td>RequestForm</td><td>" & Request.Form & "</td></tr>"
		sTable=sTable & "<tr><td>VirtualPath</td><td>" & Request.ServerVariables("PATH_INFO") & "</td></tr>"
		sTable=sTable & "<tr><td>PhysicalPath</td><td>" & Request.ServerVariables("PATH_TRANSLATED") & "</td></tr>"
	end if
	if trim(Session("ErrNumber"))<>"" then
		sSQL=sSQL & ", '" & ToDB(100, "Error number: " & Session("ErrNumber")) & "'"
		sSQL=sSQL & ", '" & ToDB(500, "") & "'"
		sSQL=sSQL & ", '" & ToDB(100, "") & "'"
		sSQL=sSQL & ", '" & ToDB(100, Session("ErrSource")) & "'"
		sSQL=sSQL & ", '" & ToDB(1000, Session("ErrDescription")) & "'"
		sSQL=sSQL & ", '" & ToDB(100, "") & "'"
		sSQL=sSQL & ", '" & ToDB(1000, Session("ErrSQL")) & "'"
		sSQL=sSQL & ", '" & ToDB(1000, SQL) & "'"
		sSQL=sSQL & ")"
		if Session("SecurityUserVerified")="1" then
			sTable=sTable & "<tr><td>ExceptionSource</td><td>" & "Error number: " & Session("ErrNumber") & "</td></tr>"
			sTable=sTable & "<tr><td>ExceptionMessage</td><td>" & "" & "</td></tr>"
			sTable=sTable & "<tr><td>ExceptionTargetSite</td><td>" & "" & "</td></tr>"
			sTable=sTable & "<tr><td>InnerExceptionSource</td><td>" & Session("ErrSource") & "</td></tr>"
			sTable=sTable & "<tr><td>InnerExceptionTargetSite</td><td>" & Session("ErrSource") & "</td></tr>"
			sTable=sTable & "<tr><td>ExceptionLineNumber</td><td>" & "" & "</td></tr>"
			sTable=sTable & "<tr><td>ExceptionStackTrace</td><td>" & Session("ErrSQL") & "</td></tr>"
		end if
		Session("ErrNumber")=""
		Session("ErrSource")=""
		Session("ErrDescription")=""
		Session("ErrSQL")=""
	else
		set errLast=Server.getLastError()
		sSQL=sSQL & ", '" & ToDB(100, "Error number: " & errLast.Number) & "'"
		sSQL=sSQL & ", '" & ToDB(500, errLast.ASPDescription) & "'"
		sSQL=sSQL & ", '" & ToDB(100, errLast.File) & "'"
		sSQL=sSQL & ", '" & ToDB(100, errLast.Category & "." & errLast.Source) & "'"
		sSQL=sSQL & ", '" & ToDB(1000, errLast.Description) & "'"
		sSQL=sSQL & ", '" & ToDB(100, errLast.Line) & "'"
		sSQL=sSQL & ", '" & ToDB(100, errLast.ASPCode) & "'"
		sSQL=sSQL & ", '" & ToDB(100, errLast.Column) & "'"
		sSQL=sSQL & ")"
		if Session("SecurityUserVerified")="1" then
			sTable=sTable & "<tr><td>ExceptionSource</td><td>" & "Error number: " & errLast.Number & "</td></tr>"
			sTable=sTable & "<tr><td>ExceptionMessage</td><td>" & errLast.ASPDescription & "</td></tr>"
			sTable=sTable & "<tr><td>ExceptionTargetSite</td><td>" & errLast.File & "</td></tr>"
			sTable=sTable & "<tr><td>InnerExceptionSource</td><td>" & errLast.Category & "." & errLast.Source & "</td></tr>"
			sTable=sTable & "<tr><td>InnerExceptionTargetSite</td><td>" & errLast.Description & "</td></tr>"
			sTable=sTable & "<tr><td>ExceptionLineNumber</td><td>" & errLast.Line & "</td></tr>"
		end if
		Set errLast = Nothing
	end if
	Set dbData=Server.CreateObject("ADODB.Connection")
	dbData.Open "Provider=SQLOLEDB ;Data Source=DevSQL;User ID=sa;Password=netpay@dev;Initial Catalog=Errors"
	dbData.Execute(sSQL)
	dbData.Close
	Set dbData = Nothing
	if Session("SecurityUserVerified")="1" then
		sTable=sTable & "</table>"
		response.Write "<div style=""direction:ltr;text-align:left;color:black;background-color:white;"">" & sTable & "</div>"
		response.End
	end if
	response.Write "Unspecified system error occured."
	response.End
%>