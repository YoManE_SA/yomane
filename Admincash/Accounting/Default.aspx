<%@ Page MasterPageFile="accounting.master" Title="Welcome" EnableEventValidation="false" %>
<script runat="server">
	Sub SetVisibility()
		lblLogin.Visible = String.IsNullOrEmpty(Session("AccountingID"))
		lblLogout.Visible = Not String.IsNullOrEmpty(Session("AccountingID"))
	End Sub

	Sub Logout(ByVal o As Object, ByVal e As EventArgs)
		Session("AccountingID") = String.Empty
		Session("DisplayName") = String.Empty
		Session("DisplayCompanyName") = String.Empty
		Session("PayedBankCode") = String.Empty
		Session("StatusUserName") = String.Empty
		If Not o Is Nothing Then Response.Redirect(".")
	End Sub
	
	Sub Login(ByVal o As Object, ByVal e As EventArgs)
		Logout(Nothing, Nothing)
		If txtUsername.Text = "limor" And txtPassword.Text = "limor@@" Then
			Session("AccountingID") = 1
			Session("DisplayName") = "limor"
			Session("DisplayCompanyName") = "PayByNet"
			Session("PayedBankCode") = "'PBNLAIKI','INVIKBANK','MSGPOL528','HELNICBNK','INVESTBNK','BANKOFGEO','ATLASBNK',DEFAULT,DEFAULT"
			Session("StatusUserName") = "limor"
			Response.Redirect("transfers.aspx", True)
		End If
		Response.Redirect(".")
	End Sub

	Sub Page_Load()
		If Not Page.IsPostBack Then
			If Request("DOK") = "1" Then
				txtUsername.Text = Request("Username")
				txtPassword.TextMode = TextBoxMode.SingleLine
				txtPassword.Text = Request("Password")
				Login(Nothing, Nothing)
			End If
			SetVisibility()
		End If
	End Sub
</script>
<asp:Content ContentPlaceHolderID="cphTitle" Runat="Server">
	Welcome
</asp:Content>
<asp:Content ContentPlaceHolderID="cphBody" Runat="Server">
	<br /><br /><br /><br /><br /><br />
	<asp:Label ID="lblLogin" runat="server">
		<h4>Please Log In</h4>
		<table>
			<tr>
				<td>Username</td>
				<td><asp:TextBox ID="txtUsername" runat="server" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><asp:TextBox ID="txtPassword" TextMode="Password" runat="server" /></td>
			</tr>
			<tr>
				<td></td>
				<td><asp:Button Text="Log In" OnClick="Login" runat="server" /></td>
			</tr>
		</table>
	</asp:Label>
	<asp:Label ID="lblLogout" runat="server">
		<table>
			<tr>
				<td><input type="button" value="Transfers" onclick="location.href='transfers.aspx';" /></td>
				<td><asp:Button Text="Log Out" OnClick="Logout" runat="server" /></td>
			</tr>
		</table>
	</asp:Label>
</asp:Content>