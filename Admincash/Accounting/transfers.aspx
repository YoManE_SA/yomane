<%@ Page MasterPageFile="accounting.master" Title="Welcome" EnableEventValidation="false" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Dim nTotals(eCurrencies.MAX_CURRENCY) As Decimal 
	
	Sub SubmitTransfer(ByVal o As Object, ByVal e As CommandEventArgs)
		Dim nLogMasavDetailsID As Integer = e.CommandArgument, sSQL As String
		Dim txtNote As TextBox = CType(o, Button).Parent.FindControl("txtNote")
		Dim fuFile As FileUpload = CType(o, Button).Parent.FindControl("fuFile")

		sSQL = "SELECT WireMoney_id FROM tblLogMasavDetails WHERE logMasavDetails_id=" & nLogMasavDetailsID
		Dim nWireMoneyID As Integer = dbPages.ExecScalar(sSQL)
		sSQL = "SELECT logMasavFile_id FROM tblLogMasavDetails WHERE logMasavDetails_id=" & nLogMasavDetailsID
		Dim nLogMasavFileID As Integer = dbPages.ExecScalar(sSQL)

		Dim sFileName As String = String.Empty
		If fuFile.HasFile Then
			sFileName = "wire_" & nWireMoneyID & "_" & fuFile.PostedFile.FileName.Substring(fuFile.PostedFile.FileName.LastIndexOf("\") + 1)
			Dim sExt As String = sFileName.Substring(sFileName.LastIndexOf(".")).ToLower
			If ".gif.jpg.jpe.jpeg.png.tif.tiff.pdf.xls.xlsx.doc.docx.csv.txt.".Contains(sExt + ".") Then
				fuFile.SaveAs(Domain.Current.MapPrivateDataPath("Wires/WireFiles/" & sFileName))
				sSQL = "INSERT INTO tblWireMoneyFile(wmf_WireMoneyID, wmf_User, wmf_FileName) VALUES (" & nWireMoneyID & ", '" & dbPages.DBText(Session("StatusUserName")) & " (accounting)', '" & sFileName & "')"
				dbPages.ExecSql(sSQL)
			End If
		End If

		Dim sNote As String = txtNote.Text
		sSQL = "UPDATE tblWireMoney SET WireFlag=3 WHERE WireFlag=1 AND WireMoney_id IN(" & nWireMoneyID & ")"
		dbPages.ExecSql(sSQL)
		sSQL = "INSERT INTO tblWireMoneyLog(WireMoney_id, wml_description, wml_user) VALUES (" & nWireMoneyID & "," & _
		" Left('Wire status changed: Done, " & dbPages.DBText(sNote) & "', 250), Left('" & dbPages.DBText(Session("StatusUserName")) & "', 50))"
		dbPages.ExecSql(sSQL)

		sSQL = "SELECT Count(WireMoney_id) FROM tblWireMoney WHERE WireFlag <> 3 AND WireMoney_id IN(SELECT WireMoney_id FROM tblLogMasavDetails WHERE logMasavFile_id=" & nLogMasavFileID & ")"
		Dim nCount As String = dbPages.ExecScalar(sSQL)
		dbPages.ExecSql("UPDATE tblLogMasavFile SET DoneFlag=" & IIf(nCount > 0, 0, 1) & " WHERE logMasavFile_id=" & nLogMasavFileID)
		Response.Redirect("transfers.aspx", True)
	End Sub

	Function FormatCurrEU(ByVal nAmount As Double) As String
		Dim sAmount As String = nAmount.ToString.Replace(",", String.Empty)
		sAmount &= IIf(sAmount.Contains("."), "00", ".00")
		sAmount = sAmount.Substring(0, sAmount.IndexOf(".") + 3).Replace(".", ",")
		Return sAmount
	End Function

	Function GetDetails(ByVal nAmount As Double, ByVal nCurrency As Integer, ByVal sWireCompanyLegalName As String, ByVal nWireMoney_id As Integer, ByVal sWirePaymentAbroadAccountName As String, ByVal sWirePaymentAbroadAccountNumber As String, ByVal sWirePaymentAbroadBankName As String, ByVal sWirePaymentAbroadBankAddress As String, ByVal sWirePaymentAbroadBankAddressSecond As String, ByVal sWirePaymentAbroadBankAddressCity As String, ByVal sWirePaymentAbroadBankAddressState As String, ByVal sWirePaymentAbroadBankAddressZip As String, ByVal sWirePaymentAbroadBankAddressCountry As String, ByVal sWirePaymentAbroadSwiftNumber As String, ByVal sWirePaymentAbroadIBAN As String, ByVal sWirePaymentAbroadABA As String, ByVal sWirePaymentAbroadSortCode As String, ByVal sWirePaymentAbroadSepaBic As String, ByVal sWirePaymentAbroadAccountName2 As String, ByVal sWirePaymentAbroadAccountNumber2 As String, ByVal sWirePaymentAbroadBankName2 As String, ByVal sWirePaymentAbroadBankAddress2 As String, ByVal sWirePaymentAbroadBankAddressSecond2 As String, ByVal sWirePaymentAbroadBankAddressCity2 As String, ByVal sWirePaymentAbroadBankAddressState2 As String, ByVal sWirePaymentAbroadBankAddressZip2 As String, ByVal sWirePaymentAbroadBankAddressCountry2 As String, ByVal sWirePaymentAbroadSwiftNumber2 As String, ByVal sWirePaymentAbroadIBAN2 As String, ByVal sWirePaymentAbroadABA2 As String, ByVal sWirePaymentAbroadSortCode2 As String, ByVal sWirePaymentAbroadSepaBic2 As String) As String
		nTotals(nCurrency) += nAmount
		Dim sbDetails As New StringBuilder(String.Empty)
		sbDetails.Append("<table border=""1"" bordercolor=""#505050"" cellspacing=""0"" cellpadding=""1"" width=""100%"" style=""text-align:left;"">")
		sbDetails.Append("<tr><th nowrap width=""25%"">Amount</th><td nowrap>" & dbPages.GetCurText(nCurrency) & " " & FormatCurrEU(nAmount) & "</td></tr>")
		sbDetails.Append("<tr><th nowrap>" & Session("DisplayCompanyName") & " Unique Payment Number</th><td nowrap>" & sWireCompanyLegalName & " - " & nWireMoney_id & "</td></tr>")
		If Trim(sWirePaymentAbroadAccountName) <> "" Then
			sbDetails.Append("<tr><td nowrap colspan=""2"" style=""background-color:LightSteelBlue;font-size:150%;"">Primary Bank</td></tr>")
			sbDetails.Append("<tr><th nowrap>Account Name</th><td nowrap>" & sWirePaymentAbroadAccountName & "</td></tr>")
			If Trim(sWirePaymentAbroadAccountNumber) <> "" Then sbDetails.Append("<tr><th nowrap>Account Number</th><td nowrap>" & sWirePaymentAbroadAccountNumber & "</td></tr>")
			If Trim(sWirePaymentAbroadBankName) <> "" Then sbDetails.Append("<tr><th nowrap>Bank Name</th><td nowrap>" & sWirePaymentAbroadBankName & "</td></tr>")
			sbDetails.Append("<tr><th nowrap>Bank Address</th><td nowrap>")
			sbDetails.Append("<div>" & sWirePaymentAbroadBankAddress & "</div>")
			sbDetails.Append("<div>" & sWirePaymentAbroadBankAddressSecond & "</div>")
			sbDetails.Append("<div>" & sWirePaymentAbroadBankAddressCity & ", " & sWirePaymentAbroadBankAddressState & ", " & sWirePaymentAbroadBankAddressZip & "</div>")
			sbDetails.Append("<div>" & sWirePaymentAbroadBankAddressCountry & "</div>")
			sbDetails.Append("</td></tr>")
			If Trim(sWirePaymentAbroadSwiftNumber) <> "" Then sbDetails.Append("<tr><th nowrap>Swift</th><td nowrap>" & sWirePaymentAbroadSwiftNumber & "</td></tr>")
			If Trim(sWirePaymentAbroadIBAN) <> "" Then sbDetails.Append("<tr><th nowrap>IBAN</th><td nowrap>" & sWirePaymentAbroadIBAN & "</td></tr>")
			If Trim(sWirePaymentAbroadABA) <> "" Then sbDetails.Append("<tr><th nowrap>ABA</th><td nowrap>" & sWirePaymentAbroadABA & "</td></tr>")
			If Trim(sWirePaymentAbroadSortCode) <> "" Then sbDetails.Append("<tr><th nowrap>Sort Code</th><td nowrap>" & sWirePaymentAbroadSortCode & "</td></tr>")
			If Trim(sWirePaymentAbroadSepaBic) <> "" Then sbDetails.Append("<tr><th nowrap>SEPA BIC</th><td nowrap>" & sWirePaymentAbroadSepaBic & "</td></tr>")
		End If
		If Trim(sWirePaymentAbroadAccountName2) <> "" Then
			sbDetails.Append("<tr><td nowrap colspan=""2"" style=""background-color:LightSteelBlue;font-size:150%;"">Correspondent Bank</td></tr>")
			sbDetails.Append("<tr><th nowrap>Account Name</th><td nowrap>" & sWirePaymentAbroadAccountName2 & "</td></tr>")
			If Trim(sWirePaymentAbroadAccountNumber2) <> "" Then sbDetails.Append("<tr><th nowrap>Account Number</th><td nowrap>" & sWirePaymentAbroadAccountNumber2 & "</td></tr>")
			If Trim(sWirePaymentAbroadBankName2) <> "" Then sbDetails.Append("<tr><th nowrap>Bank Name</th><td nowrap>" & sWirePaymentAbroadBankName2 & "</td></tr>")
			sbDetails.Append("<tr><th nowrap>Bank Address</th><td nowrap>")
			sbDetails.Append("<div>" & sWirePaymentAbroadBankAddress2 & "</div>")
			sbDetails.Append("<div>" & sWirePaymentAbroadBankAddressSecond2 & "</div>")
			sbDetails.Append("<div>" & sWirePaymentAbroadBankAddressCity2 & ", " & sWirePaymentAbroadBankAddressState2 & ", " & sWirePaymentAbroadBankAddressZip2 & "</div>")
			sbDetails.Append("<div>" & sWirePaymentAbroadBankAddressCountry2 & "</div>")
			sbDetails.Append("</td></tr>")
			If Trim(sWirePaymentAbroadSwiftNumber2) <> "" Then sbDetails.Append("<tr><th nowrap>Swift</th><td nowrap>" & sWirePaymentAbroadSwiftNumber2 & "</td></tr>")
			If Trim(sWirePaymentAbroadIBAN2) <> "" Then sbDetails.Append("<tr><th nowrap>IBAN</th><td nowrap>" & sWirePaymentAbroadIBAN2 & "</td></tr>")
			If Trim(sWirePaymentAbroadABA2) <> "" Then sbDetails.Append("<tr><th nowrap>ABA</th><td nowrap>" & sWirePaymentAbroadABA2 & "</td></tr>")
			If Trim(sWirePaymentAbroadSortCode2) <> "" Then sbDetails.Append("<tr><th nowrap>Sort Code</th><td nowrap>" & sWirePaymentAbroadSortCode2 & "</td></tr>")
			If Trim(sWirePaymentAbroadSepaBic2) <> "" Then sbDetails.Append("<tr><th nowrap>SEPA BIC</th><td nowrap>" & sWirePaymentAbroadSepaBic2 & "</td></tr>")
		End If
		sbDetails.Append("</table>")
		Return sbDetails.ToString
	End Function

	Function ColumnHeader(ByVal sDBColumn As String, ByVal sTitle As String, Optional ByVal sOrderBy As String = Nothing, Optional ByVal sAlign As String = "left") As String
		Return "<th title=""Order by '" & sTitle & IIf(sOrderBy = sDBColumn, " (descending)", String.Empty) & "'"" style=""cursor:pointer;text-align:" & sAlign & ";"" onclick=""location.replace('transfers.aspx?OrderBy=" & sDBColumn & IIf(sOrderBy = sDBColumn, " DESC", String.Empty) & "');"">" & sTitle & "</th>"
	End Function

	Sub Page_Load()
		If String.IsNullOrEmpty(Session("AccountingID")) Then Response.Redirect(".", True)
		Dim sOrderBy As String = Request("OrderBy")
		dsTransfers.ConnectionString = dbPages.DSN
		Dim sSQL As String = "SELECT * FROM GetAccountingTransfers(" & Session("PayedBankCode") & ")"
		dsTransfers.SelectCommand = sSQL & IIf(String.IsNullOrEmpty(sOrderBy), String.Empty, " ORDER BY " & sOrderBy)
		litFields.Text = String.Empty
		litFields.Text &= ColumnHeader("RowNumber", "No.", sOrderBy)
		litFields.Text &= ColumnHeader("WireMoney_id", "ID", sOrderBy)
		litFields.Text &= ColumnHeader("WireInsertDate", "Date", sOrderBy)
		litFields.Text &= ColumnHeader("wirePaymentAbroadAccountName", "Payee Name", sOrderBy)
		litFields.Text &= ColumnHeader("PayeeBankDetails", "Bank Info", sOrderBy)
		litFields.Text &= ColumnHeader("Currency", "Currency", sOrderBy, "right")
		litFields.Text &= ColumnHeader("Amount", "Amount", sOrderBy, "right")
	End Sub
</script>
<asp:Content ContentPlaceHolderID="cphTitle" Runat="Server">
	Transfers
</asp:Content>
<asp:Content ContentPlaceHolderID="cphBody" Runat="Server">
	<table width="900px">
		<tr>
			<asp:Literal ID="litFields" runat="server" />
			<th align="center">Details</th>
		</tr>
		<asp:SqlDataSource ID="dsTransfers" SelectCommandType="text" runat="server" />
		<asp:Repeater ID="repTransfers" DataSourceID="dsTransfers" runat="server">
			<ItemTemplate>
				<tr>
					<td><asp:Literal Text='<%# Eval("RowNumber") %>' runat="server" /></td>
					<td><asp:Literal Text='<%# Eval("WireMoney_id") %>' runat="server" /></td>
					<td><asp:Literal Text='<%# dbPages.FormatDateTime(Eval("WireInsertDate")) %>' runat="server" /></td>
					<td><asp:Literal Text='<%# Eval("wirePaymentAbroadAccountName") %>' runat="server" /></td>
					<td><asp:Literal Text='<%# Eval("PayeeBankDetails") %>' runat="server" /></td>
					<td align="right"><asp:Literal Text='<%# dbPages.GetCurText(Eval("Currency")) %>' runat="server" /></td>
					<td align="right"><asp:Literal Text='<%# FormatCurrEU(Eval("Amount")) %>' runat="server" /></td>
					<td align="center"><a onclick="this.innerText=(this.innerText=='Hide'?'Show':'Hide');with(parentElement.parentElement){parentElement.rows[rowIndex + 1].className=parentElement.rows[rowIndex + 1].className=='hidden' ? '' : 'hidden'; }" style="cursor:pointer;color:blue;">Show</a></td>
				</tr>
				<tr class="hidden">
					<td></td>
					<td colspan="6">
						<asp:Literal Text='<%# GetDetails(Eval("Amount"), Eval("Currency"), Eval("WireCompanyLegalName"), Eval("WireMoney_ID"), Eval("wirePaymentAbroadAccountName"), Eval("wirePaymentAbroadAccountNumber"), Eval("wirePaymentAbroadBankName"), Eval("wirePaymentAbroadBankAddress"), Eval("wirePaymentAbroadBankAddressSecond"), Eval("wirePaymentAbroadBankAddressCity"), Eval("wirePaymentAbroadBankAddressState"), Eval("wirePaymentAbroadBankAddressZip"), Eval("wirePaymentAbroadBankAddressCountry"), Eval("wirePaymentAbroadSwiftNumber"), Eval("wirePaymentAbroadIBAN"), Eval("wirePaymentAbroadABA"), Eval("wirePaymentAbroadSortCode"), Eval("wirePaymentAbroadSepaBic"), Eval("wirePaymentAbroadAccountName2"), Eval("wirePaymentAbroadAccountNumber2"), Eval("wirePaymentAbroadBankName2"), Eval("wirePaymentAbroadBankAddress2"), Eval("wirePaymentAbroadBankAddressSecond2"), Eval("wirePaymentAbroadBankAddressCity2"), Eval("wirePaymentAbroadBankAddressState2"), Eval("wirePaymentAbroadBankAddressZip2"), Eval("wirePaymentAbroadBankAddressCountry2"), Eval("wirePaymentAbroadSwiftNumber2"), Eval("wirePaymentAbroadIBAN2"), Eval("wirePaymentAbroadABA2"), Eval("wirePaymentAbroadSortCode2"), Eval("wirePaymentAbroadSepaBic2")) %>' runat="server" />
						<br /><span style="width:60px;"><b>Note</b></span>
						<asp:TextBox ID="txtNote" Width="600px" runat="server" />
						<br /><span style="width:60px;"><b>File</b></span>
						<asp:FileUpload ID="fuFile" Width="600px" runat="server" />
						<br /><span style="width:60px;">&nbsp;</span>
						<asp:Button Text="Execute!" CommandArgument='<%# Eval("logMasavDetails_id") %>' CommandName="Submit" OnCommand="SubmitTransfer" UseSubmitBehavior="false" runat="server" />
					</td>
					<td></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
		<tr><td colspan="8" style="border-top:solid 1px black;"><img width="1" height="1" /></td></tr>
		<tr>
		 <td colspan="5">TOTAL</td>
		 <td align="right">
			<%
			For i As Integer = 0 To nTotals.Length - 1
				if nTotals(i) <> 0 then Response.Write(dbPages.GetCurText(i) + "<br />")
			Next
			%>
		 </td><td align="right">
			<%
			For i As Integer = 0 To nTotals.Length - 1
				if nTotals(i) <> 0 then Response.Write(FormatCurrEU(nTotals(i)) + "<br />")
			Next
			%>
		 </td>
		</tr>
	</table>
</asp:Content>



