﻿<% 'Response.Buffer= Fasle %>
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../member/RemoteCharge_Functions.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<table border="1">
	<tr>
		<th>MerchantID</th>
		<th>FailID</th>
		<th>OriginalPassID</th>
		<th>Reply</th>
		<th>New ID</th>
		<th>Order</th>
		<th>Comment</th>
	</tr>
<%
Dim cRs, reqData, merchantId, merchantNumber, retData
Server.ScriptTimeout = 500 * 60
merchantId = TestNumVar(Request("MerchantID"), 0, -1, 0)
merchantNumber = ExecScalar("Select CustomerNumber From tblCompany Where ID=" & merchantId, "")
Set cRs = oledbData.Execute( _
		"Select Top 5 *, dbo.GetDecrypted256(c.CCard_number256) as decNumber From tblCompanyTransFail f " & _
		"Left Join tblCreditCard c ON(f.CreditCardID = c.ID) " & _
		"Left Join tblBillingAddress b ON(c.BillingAddressID = b.ID) " & _
		"Where f.CompanyID=" & merchantId & " And f.debitCompanyID=58 And f.replyCode = '99' " & _
		"And f.ID Not IN (16918329) " & _
		"Order By f.ID asc")
Do While Not cRs.EOF
	reqData = ""
	reqData = reqData & "TransType=0"
	reqData = reqData & "&TypeCredit=1"
	reqData = reqData & "&Payments=1"
	reqData = reqData & "&CompanyNum=" & merchantNumber

	reqData = reqData & "&Currency=" & cRs("Currency")
	reqData = reqData & "&Amount=" & cRs("Amount")
	reqData = reqData & "&PayFor=" & Server.UrlEncode(cRs("PayForText"))
 
	reqData = reqData & "&CardNum=" & cRs("decNumber")
	reqData = reqData & "&ExpYear=" & Trim(cRs("ExpYY"))
	reqData = reqData & "&ExpMonth=" & Trim(cRs("ExpMM")) 
	reqData = reqData & "&CVV2=" & DecCVV(cRs("cc_cui"))

	reqData = reqData & "&Member=" & Server.UrlEncode(cRs("Member"))
	reqData = reqData & "&Email=" & Server.UrlEncode(cRs("Email"))
	reqData = reqData & "&PhoneNumber=" & Server.UrlEncode(cRs("PhoneNumber"))
	reqData = reqData & "&PersonalNumber=" & Server.UrlEncode(cRs("PersonalNumber"))
	reqData = reqData & "&Order=" & Server.UrlEncode(cRs("OrderNumber"))
	reqData = reqData & "&ClientIP=" & Server.UrlEncode(cRs("IPAddress"))

	reqData = reqData & "&BillingAddress1=" & Server.UrlEncode(cRs("address1"))
	reqData = reqData & "&BillingAddress2=" & Server.UrlEncode(cRs("address2"))
	reqData = reqData & "&BillingCity=" & Server.UrlEncode(cRs("city"))
	reqData = reqData & "&BillingZipCode=" & Server.UrlEncode(cRs("zipCode")) 
	reqData = reqData & "&BillingState=" & Server.UrlEncode(cRs("stateId"))
	reqData = reqData & "&BillingCountry=" & Server.UrlEncode(cRs("countryId"))
	reqData = reqData & "&Comment=" & Server.UrlEncode(cRs("Comment") & " - ReSend")
	
	Dim retTransID, retReply, originalPassId
	originalPassId = Left(cRs("Comment"), 7)
	retData = SendRequest(reqData)
	'Response.Write(reqData & "<br>" & vbCrLf & retData)
	retReply = GetURLValue(retData, "Reply")
	retTransID = GetURLValue(retData, "TransID")
	Response.Write("<tr><td>" & merchantId & "</td><td>" & cRs("ID") & "</td><td>" & originalPassId & "</td><td>" & retReply & "</td><td>" & retTransID & "</td><td>" & cRs("OrderNumber") & "</td><td>" & cRs("Comment") & "</td></tr>")
	Response.Flush
 cRs.MoveNext
Loop
cRs.Close()

Function SendRequest(sendData)
	Dim HttpReq : Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 20*1000, 30*1000, 30*1000, 220*1000
	HttpReq.open "POST", "https://process.netpay-intl.com/member/remote_charge.asp", False
	HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	HttpReq.send sendData
	SendRequest = HttpReq.responseText
End Function

%>
</table>