﻿<%@ Page Language="C#" %>
<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        Response.Write("<PRE>RESULT</PRE>");
        Response.Buffer = false;
        base.OnLoad(e);
        string hashKey = "N86VDVHTYQ";
        string credToken = loginService("lavi@netpay-intl.com", "lavi", "sk64CsxK", hashKey);
        if (credToken == null) return;
        riskRequest(1, credToken, hashKey, "Remove");
    }

    protected string createSignature(string data, System.Text.Encoding enc = null, bool useRaw_output = true)
    {
        if (enc == null) enc = Encoding.UTF8;
        System.Security.Cryptography.SHA256 sh = System.Security.Cryptography.SHA256.Create();
        byte[] hashValue = sh.ComputeHash(enc.GetBytes(data));
        if (!useRaw_output) return System.Convert.ToBase64String(enc.GetBytes(BitConverter.ToString(hashValue)));
        return System.Convert.ToBase64String(hashValue);
    }

    protected string loginService(string email, string userName, string password, string hashKey)
    {

        var sb = new StringBuilder();
        sb.AppendFormat("email={0}&userName={1}&password={2}&signature={3}",
            System.Web.HttpUtility.UrlEncode(email),
            System.Web.HttpUtility.UrlEncode(userName),
            System.Web.HttpUtility.UrlEncode(password),
            System.Web.HttpUtility.UrlEncode(createSignature(email + userName + password + hashKey)));
        Response.Write("Send: <br/>" + sb.ToString() + "<br/><br/>");
        string textOut;
        Netpay.Infrastructure.HttpClient.SendHttpRequest("http://192.168.5.35/webservices/Merchants.asmx/Login", out textOut, sb.ToString(), null, 0, null);
        Response.Write("Return: <br/>" + textOut + "<br/><br/>");
        var doc = new System.Xml.XmlDocument();
        doc.LoadXml(textOut);

        System.Xml.XmlNamespaceManager nsmgr = new System.Xml.XmlNamespaceManager(doc.NameTable);
        nsmgr.AddNamespace("np", "http://netpay-intl.com/");
        if (doc.SelectSingleNode("//np:MerchantLoginResult/np:Result", nsmgr).InnerText != "Success") return null;
        return doc.SelectSingleNode("//np:MerchantLoginResult/np:CredentialsToken", nsmgr).InnerText;
    }

    protected string wrapSoapRequest(decimal spVersion, string reqData)
    {
        var sb = new StringBuilder();
        sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
        if (spVersion < 1.2m)
        {
            sb.Append("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n");
            sb.Append(" <soap:Body>\r\n");
            sb.Append(reqData);
            sb.Append(" </soap:Body>\r\n");
            sb.Append("</soap:Envelope>");
        } else {
            sb.Append("<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\r\n");
            sb.Append(" <soap12:Body>\r\n");
            sb.Append(reqData);
            sb.Append(" </soap12:Body>\r\n");
            sb.Append("</soap12:Envelope>");
        }
        return sb.ToString(); 
    }

    protected string sendSoapRequest(decimal spVersion, string serviceURL, string soapAction, string reqData)
    {
        string textOut;
        Dictionary<string, string> headers = new Dictionary<string, string>();
        Response.Write("\r\nSend: <br/>\r\n" + reqData + "<br/><br/>");
        if (spVersion >= 1.2m) {
            headers.Add("Content-Type", "text/xml; charset=utf-8");
            headers.Add("SOAPAction", soapAction);
        } else {
            headers.Add("Content-Type", "text/xml; charset=utf-8");
        }
        Netpay.Infrastructure.HttpClient.SendHttpRequest(serviceURL, out textOut, reqData, null, 0, headers);
        Response.Write("Return: <br/>" + textOut + "<br/><br/>");
        return textOut;
    }
                
    protected void riskRequest(decimal spVersion, string credToken, string hashKey, string action)
    {
        int count = 0;
        var sb = new StringBuilder();
        sb.Append("  <ManageBlocks xmlns=\"http://netpay-intl.com/\">\r\n");
        sb.AppendFormat("   <credentialsToken>{0}</credentialsToken>\r\n", credToken);
        sb.Append("   <items>\r\n");

        string fileName = Server.MapPath("Doughflow_09072012.xls");
        var excelConn = new System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" + fileName + "\";Extended Properties=\"Excel 8.0;\"");
        excelConn.Open();
        
        var excelComm = new System.Data.OleDb.OleDbCommand("Select * from [Sheet1$]", excelConn);
        var reader = excelComm.ExecuteReader();
        if (reader.HasRows)
        {
            count++;
            sb.Append("    <BlockItem Type=\"Email\" Action=\"" + action + "\">\r\n");
            sb.Append("    <Comment></Comment>\r\n");
            sb.Append("    <Values>\r\n");
            while (reader.Read())
                sb.AppendFormat("     <BlockItemValue Key=\"{0}\" Value=\"{0}\" />\r\n", reader["email"]);
            sb.Append("     </Values>\r\n");
            sb.Append("    </BlockItem>\r\n");
        }
        reader.Close();

        excelComm = new System.Data.OleDb.OleDbCommand("Select * from [Sheet2$]", excelConn);
        reader = excelComm.ExecuteReader();
        if (reader.HasRows)
        {
            count++;
            sb.Append("    <BlockItem Type=\"CreditCard\" Action=\"" + action + "\">\r\n");
            sb.Append("    <Comment></Comment>\r\n");
            sb.Append("    <Values>\r\n");
            while (reader.Read())
                sb.AppendFormat("     <BlockItemValue Key=\"{0}\" Value=\"{0}\" />\r\n", reader[0]);
            sb.Append("     </Values>\r\n");
            sb.Append("    </BlockItem>\r\n");
        }
        reader.Close();
        excelConn.Close();
        
        sb.Append("   </items>\r\n");
        sb.AppendFormat("   <signature>{0}</signature>\r\n", createSignature(credToken + count + hashKey));
        sb.Append("  </ManageBlocks>\r\n");
        
        string reqData = wrapSoapRequest(spVersion, sb.ToString());
        sendSoapRequest(spVersion, "http://192.168.5.35/webservices/Risk.asmx", "http://netpay-intl.com/ManageBlocks", reqData);
    }

</script> 