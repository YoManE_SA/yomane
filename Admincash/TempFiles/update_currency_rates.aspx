﻿<%@ Page Language="VB" %>
<script runat="server">
	Sub Page_Load()
		Dim sUrl As String = String.Empty
		Select Case Request("Bank")
			Case "EU" : sUrl = "http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml"
			Case "IL" : sUrl = "http://www.bankisrael.gov.il/heb.shearim/currency.php"
		End Select
		If String.IsNullOrEmpty(sUrl) Then
			Response.Write("ERROR: " & Request("Bank"))
		Else
			Dim hwrBank As System.Net.HttpWebRequest = System.Net.WebRequest.Create(sUrl)
			hwrBank.Method = "GET"
			Dim srRates As New System.IO.StreamReader(hwrBank.GetResponse.GetResponseStream)
			Dim sXml As String = srRates.ReadToEnd
			srRates.Close()
			srRates.Dispose()
			Response.Write(sXml)
		End If
	End Sub
</script>