<script runat="server">
    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        Dim user = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split("\\")(1)
        Dim IsAppPoolUser = user.StartsWith(".")
        If IsAppPoolUser Then user = Request.LogonUserIdentity.Name.Split("\\")(1)
        Dim cookie As HttpCookie = New HttpCookie("userName", user)
        Response.SetCookie(cookie)
        WebUtils.Login(IIf(IsAppPoolUser, Request.LogonUserIdentity, System.Security.Principal.WindowsIdentity.GetCurrent()))

        Dim userAgent As String = Request.ServerVariables("HTTP_USER_AGENT").ToLower()
        Dim redirectLocation As String = ""

        Response.Redirect("admincash", True)
    End Sub
</script>

<html>
    <body>
        <form runat="server" />
    </body>
</html>