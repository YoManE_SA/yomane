﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.FraudDetectionTest
{
    [TestClass]
    public class TestBase
    {
        [AssemblyInitialize()]
        public static void AssemblyInit(TestContext context)
        {
        }

        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
        }

        [TestInitialize()]
        public void Initialize()
        {
            Application.Init("FraudDetectionTest");
            Domain.Current = Domain.Get("default");
            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
        }

        [TestCleanup()]
        public void Cleanup()
        {
        }

        [ClassCleanup()]
        public static void ClassCleanup()
        {
        }

        [AssemblyCleanup()]
        public static void AssemblyCleanup()
        {
        }
    }
}
