﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Netpay.FraudDetection;
using Netpay.Infrastructure;
using System.Collections.Generic;
using Netpay.FraudDetection.Rules;
using System.Linq;
using Netpay.Bll.Accounts;
using Netpay.Bll;
using Netpay.Bll.Merchants;

namespace Netpay.FraudDetectionTest
{
    [TestClass]
    public class RulesTest : TestBase
    {
        [TestMethod]
        public void OfflineDetectionTest()
        {
            FraudDetectionManager.DetectOffline();
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Merchant duplicate value found. fraud spec 312")]
        public void MerchantSaveTest()
        {
            Merchant merchant = new Merchant();
            merchant.BusinessAddress = new AccountAddress();
            merchant.ContactFirstName = "First Name 3";
            merchant.LegalName = "aaabbcc";
            merchant.Save();
        }

        [TestMethod]
        public void DetectionReportTest()
        {
            FraudDetectionReport.Filters filters = new FraudDetectionReport.Filters();
            var logs = FraudDetectionReport.Search(filters);
            foreach (var log in logs)
                Console.WriteLine(log.ToString());
        }

        [TestMethod]
        public void DetectionLogTest()
        {
            FraudDetectionLog.Filters filters = new FraudDetectionLog.Filters();
            var logs = FraudDetectionLog.Search(filters, null);
            foreach (var log in logs)
                Console.WriteLine(log.ToString());
        }

        [TestMethod]
        public void RuleSaveTest()
        {
            var rules = DetectionRuleBase.Load(6629);
            //foreach (var rule in rules)
            //rule.Save();

            DetectionRuleBase changeRule = rules.Where(r => r.Code == "DR912").Single();
            Console.WriteLine((changeRule.Thresholds.First() as Threshold<int>).Value);
            changeRule.IsEnabled = !changeRule.IsEnabled;
            (changeRule.Thresholds.First() as Threshold<int>).Value = new Random().Next(100, 200);
            changeRule.Save();

            rules = DetectionRuleBase.Load(6629);
            changeRule = changeRule = rules.Where(r => r.Code == "DR912").Single();
            Console.WriteLine((changeRule.Thresholds.First() as Threshold<int>).Value);
        }

        [TestMethod]
        public void ChangeLogTest()
        {
            
            var oldo = new { PropA = "A", PropB = "B" };
            var newo = new { PropA = "A", PropB = "b" };
            //var res = ChangeLog.GetChanges(oldo, newo);
            //foreach (dynamic r in res)
                //Console.WriteLine(r.PropertyName +" "+ r.PropertyNewValue +""+ r.PropertyOldValue);


            
            /*
            ChangeAuditLog log = new ChangeAuditLog();
            log.Action = ChangeAuditLog.ActionType.Change;
            log.Location = "location";
            log.Name = "name";
            log.OldValue = "old val";
            log.NewValue = "new val";
            //log.User = "user";
            log.Save();

            ChangeAuditLog.Filters filters = new ChangeAuditLog.Filters();
            var logs = ChangeAuditLog.Search(filters);
            foreach (var cl in logs)
                Console.WriteLine(cl.Action);
            */
        }

        [TestMethod]
        public void MastercardChbTest()
        {
            MastercardChargebackStats rule = new MastercardChargebackStats();
            var results = rule.Detect(null);
            foreach (var result in results)
                Console.WriteLine($"merchant: {result.MerchantId}, {result.Message}");
        }

        [TestMethod]
        public void MastercardFraudTest()
        {
            MastercardFraudStats rule = new MastercardFraudStats();
            var results = rule.Detect(null);
            foreach (var result in results)
                Console.WriteLine($"merchant: {result.MerchantId}, {result.Message}");
        }

        [TestMethod]
        public void SimulationTest()
        {
            DetectionContext context = new DetectionContext();
            context.AccountId = 6629;
            context.CreditcardNumber = "4580000000000000";
            context.CreditcardName = "udi azulay";
            context.CreditcardExpiration = new DateTime(2017, 9, 1);
            context.Amount = 1000;
            context.TransCurrency = Currency.Get("ILS");

            Console.WriteLine("global rules");
            List<DetectionRuleBase> rules = DetectionRuleBase.Load(null);
            FraudDetectionManager.SimulationResult result = FraudDetectionManager.Simulate(context, rules);
            foreach (var detectionResult in result.Results)
            {
                Console.WriteLine(detectionResult.ToString());
            }

            Console.WriteLine();
            Console.WriteLine("merchant rules");
            rules = DetectionRuleBase.Load(6629);
            result = FraudDetectionManager.Simulate(context, rules);
            foreach (var detectionResult in result.Results)
            {
                Console.WriteLine(result.ToString());
            }  
        }
    } 
}
