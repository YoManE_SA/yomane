﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web
{
	public class ContextModule : System.Web.IHttpModule, System.Web.SessionState.IRequiresSessionState
	{
        private static object _lockObject = new object();
        private static bool _appLoaded = false;
        private static bool _mapLoaded = false;
        public static string AppName { get; set; }

		public void Init(System.Web.HttpApplication context)
		{
            lock (_lockObject)
            {
                if (!_appLoaded) {
                    //new Infrastructure.ObjectContext(System.Web.HttpContext.Current.Server. WebUtils.DomainHost, Guid.Empty);
                    Netpay.Infrastructure.Application.Init(AppName); /*System.Configuration.ConfigurationManager.AppSettings["Netpay.AppName"]*/
                    Netpay.Bll.Application.Init();
                    PublicDataHandler.RegisterRouter();
                    _appLoaded = true;
                }
            }
            context.BeginRequest += Application_BeginRequest;
			context.EndRequest += Application_EndRequest;
			//context.PostAuthorizeRequest += Application_PostAuthorizeRequest;
			context.PostAcquireRequestState += Application_PostAuthorizeRequest;
			//(context.Modules["Session"] as System.Web.SessionState.SessionStateModule).Start += Session_Start;

		}

		public void Dispose()
		{
		}

		private void Application_PostAuthorizeRequest(object sender, EventArgs e)
		{
			var credToken = WebUtils.CredentialsToken;
			if (credToken != Guid.Empty) 
                Infrastructure.ObjectContext.Current.CredentialsToken = credToken;
		}

		private void Application_BeginRequest(object sender, EventArgs e)
		{
            if (!_mapLoaded) {
                Netpay.Infrastructure.Application.ApplicationPath = System.Web.HttpContext.Current.Request.ApplicationPath;
                _mapLoaded = true;
            }
            Infrastructure.Domain.Current = Infrastructure.Domain.Get(WebUtils.DomainHost);
		}

		private void Application_EndRequest(object sender, EventArgs e)
		{
			Infrastructure.ObjectContext.Current.Dispose();
		}

	}
}
