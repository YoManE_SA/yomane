﻿using System;

namespace Netpay.Web.Ajax
{
	/// <summary>
	/// Flags a method for ajax invokation
    /// Without this flag an ajax method will not be found and invoked
	/// </summary>
    [AttributeUsage(AttributeTargets.Method)]
	internal class AjaxMethod : Attribute
	{
	}
}
