﻿using System.Resources;
using System.Web.UI;
using Netpay.Bll;

namespace Netpay.Web.Controls.Ajax.Controls
{
	public class AddToBlacklist : WindowControl
	{
		public AddToBlacklist() 
		{ 
			Title = rm.GetString("title"); 
		}

		ResourceManager rm = WebUtils.GetResource("Ajax.AddToBlacklist");
		private AddBlockedCardResult? _result = null;

		public AddBlockedCardResult? Result
		{
			get { return _result; }
			set { _result = value; }
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			base.RenderContents(writer);

			writer.Write("<div>");
			writer.Write(rm.GetString(Result.ToString()));
			writer.Write("</div>");
		}
	}
}
