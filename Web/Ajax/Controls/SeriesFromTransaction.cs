﻿using System;
using System.Resources;
using System.Web.UI;

namespace Netpay.Web.Controls.Ajax.Controls
{
	public class SeriesFromTransaction : WindowControl
	{
		public SeriesFromTransaction()
		{
			ResourceManager rm = WebUtils.GetResource("Ajax.SeriesFromTransaction");
			Title = rm.GetString("title");
		}
		
		private Bll.Transactions.Transaction _transaction = null;

		public Bll.Transactions.Transaction Transaction
		{
			get { return _transaction; }
			set { _transaction = value; }
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			ResourceManager rmCommon = WebUtils.GetResource("Common");
			ResourceManager rm = WebUtils.GetResource("Ajax.SeriesFromTransaction");

			// check transaction existing series
			if (Transaction.RecurringSeriesID != null)
			{
				writer.Write(rm.GetString("seriesExistsMessage"));
				return;
			}

			// script
			writer.Write("<script type=\"text/javascript\">");
			writer.Write("function createSeries()");
			writer.Write("{");
			writer.Write("var charges = document.getElementById(\"seriesFromTransactionCharges\").value; ");
			writer.Write("var intervalCount = document.getElementById(\"seriesFromTransactionIntervalCount\").value; ");
			writer.Write("var intervalUnit = document.getElementById(\"seriesFromTransactionIntervalUnit\").value; ");
			writer.Write("var amount = document.getElementById(\"seriesFromTransactionAmount\").value; ");
			writer.Write("var comment = document.getElementById(\"seriesFromTransactionComment\").value; ");
			writer.Write(string.Format("top.netpay.ajaxApi.AjaxMethods.CreateSeriesFromTransaction(\"{0}\", \"{1}\", charges, intervalCount, intervalUnit, amount, comment, function(response){{top.netpay.Common.openPopup(response.Data);}}); ", Transaction.ID, Transaction.Status));
			writer.Write("}");
			writer.Write("</script>");

			// body
			writer.Write("<table width=\"100%\" style=\"direction:" + WebUtils.CurrentDirection + ";\">");
			
			writer.Write("<tr>");
			writer.Write(string.Format("<td>{0}:</td>", rmCommon.GetString("Charges")));
			writer.Write(string.Format("<td>{0}:</td>", rmCommon.GetString("Interval")));
			writer.Write("<td></td>");
			writer.Write(string.Format("<td>{0}:</td>", rmCommon.GetString("Amount")));
			writer.Write(string.Format("<td>{0}:</td>", rmCommon.GetString("Comment")));
			writer.Write("</tr>");

			writer.Write("<tr>");
			writer.Write("<td>");
			writer.Write("<select id=\"seriesFromTransactionCharges\" style=\"width:70px;\">");
			for (byte idx = 1; idx <= 52; idx++)
				writer.Write(string.Format("<option value=\"{0}\">{0}</option>", idx));
			writer.Write("</select>");
			writer.Write("</td>");

			writer.Write("<td>");
			writer.Write("<select id=\"seriesFromTransactionIntervalCount\" style=\"width:70px;\">");
			for (byte idx = 1; idx <= 52; idx++)
				writer.Write(string.Format("<option value=\"{0}\">{0}</option>", idx));
			writer.Write("</select>");
			writer.Write("</td>");

			writer.Write("<td>");
			writer.Write("<select id=\"seriesFromTransactionIntervalUnit\" style=\"width:70px;\">");
			writer.Write(string.Format("<option value=\"3\">{0}</option>", rmCommon.GetString("Day")));
			writer.Write(string.Format("<option value=\"4\">{0}</option>", rmCommon.GetString("Week")));
			writer.Write(string.Format("<option value=\"5\">{0}</option>", rmCommon.GetString("Month")));
			writer.Write(string.Format("<option value=\"7\">{0}</option>", rmCommon.GetString("Quarter")));
			writer.Write(string.Format("<option value=\"6\">{0}</option>", rmCommon.GetString("Year")));
			writer.Write("</select>");
			writer.Write("</td>");

			writer.Write("<td>");
			writer.Write("<input type=\"text\" id=\"seriesFromTransactionAmount\" />");
			writer.Write("</td>");

			writer.Write("<td>");
			writer.Write("<input type=\"text\" id=\"seriesFromTransactionComment\" />");
			writer.Write("</td>");

			writer.Write("<td>");
			writer.Write(string.Format("<input type=\"button\" class=\"standard-button\" value=\"{0}\" onclick=\"createSeries()\" />", rmCommon.GetString("Create")));
			writer.Write("</td>");
			writer.Write("</tr>");

			writer.Write("</table>");
		}
	}
}