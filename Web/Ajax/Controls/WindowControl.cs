﻿using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls.Ajax.Controls
{
	public class WindowControl : WebControl
	{
		public string Title { get; set; }

		public override void RenderBeginTag(HtmlTextWriter writer)
		{
			ResourceManager rm = WebUtils.GetResource("Common");
			writer.Write(("<table cellspacing='8' class='popup'><tr><td class='header'><span class='title' style='float:" + WebUtils.CurrentAlignment + ";'>" + Title + "</span><span class='btn' style='float:" + WebUtils.CurrentReverseAlignment + ";'><input type='button' value='" + rm.GetString("Close") + "' onClick=\"javascript:netpay.Common.closePopup(event);\" /></span></td></tr><tr><td class='Content'>"));
		}

		public override void RenderEndTag(HtmlTextWriter writer)
		{
			writer.Write("</td></tr></table>");
		}
	}
}

