using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web.UI;
using Netpay.Bll;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls.Ajax.Controls
{
	public class RefundRequest : WindowControl
	{
		public RefundRequest()
		{
			ResourceManager rm = WebUtils.GetResource("Ajax.RefundRequest");
			Title = rm.GetString("title");
		}
	
		private Bll.Transactions.Transaction _transaction = null;

		public Bll.Transactions.Transaction Transaction
		{
			get { return _transaction; }
			set { _transaction = value; }
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			base.RenderContents(writer);

			ResourceManager rm = WebUtils.GetResource("Ajax.RefundRequest");
			ResourceManager rmCommon = WebUtils.GetResource("Common");
			var requests = Bll.Transactions.RefundRequest.RequestsForTrasaction(_transaction.ID);
			decimal totalRefundAmount = requests.Sum(rr => (rr.Status == Bll.Transactions.RefundRequest.RequestStatus.SourceCancel || rr.Status == Bll.Transactions.RefundRequest.RequestStatus.AdminCancel ? 0 : rr.RefundAmount.GetValueOrDefault(rr.RequestAmount)));
			decimal totalAvailableAmount = _transaction.Amount - totalRefundAmount;
			var merchant = Bll.Merchants.Merchant.Load();

			// script
			writer.Write("<script type=\"text/javascript\">");
			writer.Write("function requestRefund(transactionID, transactionStatus)");
			writer.Write("{");
			writer.Write("var amount = Number(document.getElementById(\"netpayWebControlsRefundRequestAmount\").value); ");
			writer.Write("var comment = document.getElementById(\"netpayWebControlsRefundRequestComment\").value; ");
			writer.Write(string.Format("if (isNaN(amount)){{netpay.Common.openPopup(\"{0}\", \"refundInvalidNumber\"); return;}}; ", rm.GetString("invalidAmount")));
			writer.Write("top.netpay.ajaxApi.AjaxMethods.RequestRefund(transactionID, transactionStatus, amount, comment, function(response){top.netpay.ajaxApi.AjaxMethods.GetRefundRequest(this.transactionID, this.transactionStatus, function(response){top.netpay.Common.openPopup(response.Data);})}, null, { 'transactionID': transactionID, 'transactionStatus': transactionStatus }); ");
			writer.Write("}");
			writer.Write("</script>");
			
			// body
			writer.Write("<div style=\"border:0px solid black; direction:" + WebUtils.CurrentDirection + "; \">");
			if (_transaction.InsertDate < DateTime.Now.AddMonths(-12))
			{
				writer.Write(rm.GetString("refundTransactionTooOld"));
			}
			else if (!merchant.ProcessSettings.IsAskRefund)
			{
				writer.Write(rm.GetString("noRefundPermission"));
			}
			else if (totalAvailableAmount > 0)
			{
				writer.Write(rm.GetString("createNewRequest"));
				writer.Write("<br><br><div>");
				if (Bll.Transactions.RefundRequest.GetRefundAbility(_transaction) == TransactionRefundAbility.FullOrPartial)
					writer.Write(string.Format("{0} &nbsp;<input id=\"netpayWebControlsRefundRequestAmount\" type=\"text\" value=\"{1:0.00}\" style=\"width:70px;\"/>", rmCommon.GetString("Amount"), totalAvailableAmount));
				else
					writer.Write(string.Format("{0} &nbsp;<input id=\"netpayWebControlsRefundRequestAmount\" type=\"hidden\" value=\"{1:0.00}\" /><b>{1:0.00}</b> &nbsp; ", rmCommon.GetString("Amount"), totalAvailableAmount));
				writer.Write(string.Format("&nbsp;&nbsp;{0} &nbsp;<input id=\"netpayWebControlsRefundRequestComment\" type=\"text\" style=\"width:175px;\" />", rmCommon.GetString("Comment")));
				writer.Write(string.Format("&nbsp;&nbsp;&nbsp;<input type=\"button\" class=\"standard-button\" value=\"{0}\" onclick=\"requestRefund('{1}', '{2}')\" id=\"netpayWebControlsRefundRequestSubmit\" />", rmCommon.GetString("Add"), _transaction.ID, _transaction.Status));
				writer.Write("</div>");
			}
			else
			{
				writer.Write(rm.GetString("fullyRefunded"));
			}
			writer.Write("</div>");

			// existing requests
			if (requests.Count > 0)
			{
				// table headers
				writer.Write(string.Format("<br><table cellspacing=\"0\" id=\"netpayWebControlsRefundRequestList\" class='grid' style=\"width:100%;\">"));
				writer.Write(string.Format("<tr>"));
				writer.Write(string.Format("<th>{0}</th>", rmCommon.GetString("RequestDate")));
				writer.Write(string.Format("<th>{0}</th>", rmCommon.GetString("Amount")));
				writer.Write(string.Format("<th>{0}</th>", rmCommon.GetString("Comment")));
				writer.Write(string.Format("<td style=\"width:10px;\"></td><th>{0}</th>", rmCommon.GetString("Status")));
				writer.Write(string.Format("</tr>"));

				// table data
				foreach (var currentRequest in requests)
				{
					writer.Write(string.Format("<tr>"));
					writer.Write(string.Format("<td>{0}</td>", currentRequest.Date.ToShortDateString()));
					writer.Write(string.Format("<td>{0}</td>", new Money(currentRequest.RequestAmount, currentRequest.CurrencyIso).ToIsoString()));
					writer.Write(string.Format("<td title=\"{0}\">&nbsp;{1}</td>", currentRequest.Comment.Replace("\"", "''"), currentRequest.Comment.Truncate(25)));
					writer.Write(string.Format("<td></td><td>{0}</td>", currentRequest.Status.EnumText("Enums")));
					writer.Write(string.Format("</tr>"));
				}

				writer.Write(string.Format("</table>")); 
			}
		}
	}
}
