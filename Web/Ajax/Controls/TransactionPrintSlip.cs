﻿using System.Resources;
using System.Web.UI;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls.Ajax.Controls
{
	public class TransactionPrintSlip : WindowControl
	{
		public TransactionPrintSlip()
		{
			ResourceManager rm = WebUtils.GetResource("Ajax.TransactionPrintSlip");
			Title = rm.GetString("title");
		}

		private Bll.Transactions.Transaction _transaction = null;

		public Bll.Transactions.Transaction Transaction
		{
			get { return _transaction; }
			set { _transaction = value; }
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			base.RenderContents(writer);

			ResourceManager rm = WebUtils.GetResource("Common");

			writer.Write("<div id=\"transactionPrintSlipContent\">");
			writer.Write(string.Format("<table align=\"center\" dir=\"{0}\">", WebUtils.CurrentDirection));
			writer.Write(string.Format("<tr><td colspan=\"2\"><a href=\"javascript:netpay.Common.print($('#transactionPrintSlipContent').html());\" printExclude='true' style=\"float:{3};font-size:12px;color:Maroon;\" class=\"noprint\">{0}</a>{1} {2}<br /></td>", rm.GetString("Print"), rm.GetString("Transaction"), Transaction.ID, WebUtils.CurrentReverseAlignment));
			
			writer.Write("<tr><td height=\"1\" colspan=\"2\" style=\"border-bottom:1px dashed gray\"><img src=\"/NPCommon/Images/1_space.gif\" alt=\"\" width=\"1\" height=\"1\" border=\"0\"><br /></td></tr>");
			writer.Write("<tr><td height=\"8\"></td></tr>");
			
			writer.Write("<tr>");
			writer.Write(string.Format("<td colspan=\"2\">{0}</td>", Transaction.Merchant.LegalName));
			writer.Write("</tr>");

			writer.Write("<tr>");
			writer.Write(string.Format("<td colspan=\"2\">{0}</td>", Transaction.Merchant.BusinessAddress.AddressLine1));
			writer.Write("</tr>");

			writer.Write("<tr><td height=\"8\"></td></tr>");

			writer.Write("<tr>");
			writer.Write(string.Format("<td>{0}:</td>", rm.GetString("Date")));
			writer.Write(string.Format("<td>{0}</td>", Transaction.InsertDate));
			writer.Write("</tr>");

			if (Transaction.PaymentData != null)
			{
				if (Transaction.PaymentData.MethodInstance.PaymentMethodType == PaymentMethodType.CreditCard)
				{
					writer.Write("<tr>");
					writer.Write(string.Format("<td>{0}:</td>", rm.GetString("CardType")));
					writer.Write(string.Format("<td>{0}</td>", Transaction.PaymentMethodName));
					writer.Write("</tr>");

					writer.Write("<tr>");
					writer.Write(string.Format("<td>{0}:</td>", rm.GetString("CardNumber")));
					writer.Write(string.Format("<td align='{1}' dir='ltr'>************{0}</td>", Transaction.PaymentData.MethodInstance.Value1Last4, WebUtils.CurrentAlignment));
					writer.Write("</tr>");

					writer.Write("<tr>");
					writer.Write(string.Format("<td>{0}:</td>", rm.GetString("CardExpirationDate")));
					writer.Write(string.Format("<td>{0}</td>", Transaction.PaymentData.MethodInstance.ExpirationDate.Value.ToString("MM/yyyy")));
					writer.Write("</tr>");

				}
				if (Transaction.PayerData != null)
				{
					writer.Write("<tr>");
					writer.Write(string.Format("<td>{0}:</td>", rm.GetString("Name")));
					writer.Write(string.Format("<td>{0}</td>", Transaction.PayerData.FullName));
					writer.Write("</tr>");

					writer.Write("<tr>");
					writer.Write(string.Format("<td>{0}:</td>", rm.GetString("Phone")));
					writer.Write(string.Format("<td>{0}</td>", Transaction.PayerData.PhoneNumber));
					writer.Write("</tr>");

					writer.Write("<tr>");
					writer.Write(string.Format("<td>{0}:</td>", rm.GetString("CardholderEmail")));
					writer.Write(string.Format("<td>{0}</td>", Transaction.PayerData.EmailAddress));
					writer.Write("</tr>");
				}

				if (Transaction.PaymentData.BillingAddress != null)
				{
					writer.Write("<tr>");
					writer.Write(string.Format("<td style=\"vertical-align:top;\">{0}:</td>", rm.GetString("BillingAddress")));
					writer.Write(string.Format("<td>{0}</td>", Format.FormatAddress(Transaction.PaymentData.BillingAddress, FormatAddressStyle.MultipleLines)));
					writer.Write("</tr>");
				}
			}

			if (!string.IsNullOrEmpty(Transaction.OrderNumber))
			{
				writer.Write("<tr>");
				writer.Write(string.Format("<td>{0}:</td>", rm.GetString("OrderNumber")));
				writer.Write(string.Format("<td>{0}</td>", rm.GetString(Transaction.OrderNumber)));
				writer.Write("</tr>");
			}

			writer.Write("<tr>");
			writer.Write(string.Format("<td>{0}:</td>", rm.GetString("CreditType")));
			writer.Write(string.Format("<td>{0}</td>", rm.GetString(Transaction.CreditTypeText)));
			writer.Write("</tr>");

			writer.Write("<tr>");
			writer.Write(string.Format("<td>{0}:</td>", rm.GetString("Amount")));
			writer.Write(string.Format("<td>{0} {1}</td>", Transaction.SignedAmount.ToString("0.00"), Transaction.CurrencyIsoCode));
			writer.Write("</tr>");

			if (Transaction.Installments > 1)
			{
				writer.Write("<tr>");
				writer.Write(string.Format("<td>{0}:</td>", rm.GetString("Installments")));
				writer.Write(string.Format("<td>{0}</td>", Transaction.Installments));
				writer.Write("</tr>");
			}

			if (Transaction.PaymentData != null && Transaction.PaymentData.MethodInstance.PaymentMethodType == PaymentMethodType.CreditCard)
			{
				writer.Write("<tr><td height=\"15\"></td></tr>");
				writer.Write(string.Format("<tr><td colspan=\"2\">{0}: _________________</td></tr>", rm.GetString("Phone")));
				writer.Write("<tr><td height=\"5\"></td></tr>");
				writer.Write(string.Format("<tr><td colspan=\"2\">{0}: ________________</td></tr>", rm.GetString("Signature")));				
			}

			writer.Write("</table>");
			writer.Write("</div>");
		}
	}
}
