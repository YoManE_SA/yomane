﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using Netpay.Infrastructure.Security;

namespace Netpay.Web.Controls.Ajax.Controls
{
    public class SiteFeedback : WindowControl
	{
        private bool _sent = false;
		
		public SiteFeedback() 
		{ 
			Title = WebUtils.GetResource("Ajax.SiteFeedback").GetString("SiteFeedback"); 
		}
        
		private string SendHttpRequest(string url, string data, bool post)
        {
			if (!post)
				url += "?" + data.Replace(" ", "+");
			HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

			if (post)
			{
				request.Method = "POST";
				request.ContentType = "application/x-www-form-urlencoded";
				StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(data.Replace(" ", "+"));
				requestWriter.Close();
			}

			HttpWebResponse response = request.GetResponse() as HttpWebResponse;
			StreamReader responseReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
			string responseData = responseReader.ReadToEnd();
			responseReader.Close();
			response.Close();

			return responseData;
        }

        public void SendMessage(Guid creds, string subject, string body) 
        {
			string userInfo = "";
            if (Infrastructure.ObjectContext.Current.User != null)
				userInfo = Infrastructure.ObjectContext.Current.User.LoginID + " " + Infrastructure.ObjectContext.Current.User.UserName;
		    
			StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            sb.Append("<tr><th>URL</th><td>" + HttpContext.Current.Request.Url.ToString() + "</td></tr>");
            sb.Append("<tr><th>IP</th><td>" + HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] + "</td></tr>");
            sb.Append("<tr><th>Merchant</th><td>" + userInfo + "</td></tr>");
            sb.Append("<tr><th>Subject</th><td>" + subject + "</td></tr>");
            sb.Append("<tr><th>Body</th><td>" + body + "</td></tr>");
            sb.Append("</table>");
			// send
			MailAddress sendAddressFrom = new MailAddress(WebUtils.CurrentDomain.MailAddressFrom, WebUtils.CurrentDomain.MailNameFrom);
			MailAddress sendAddressTo = new MailAddress(WebUtils.CurrentDomain.MailAddressTo, WebUtils.CurrentDomain.MailNameTo);
			Netpay.Infrastructure.Email.SmtpClient.Send(sendAddressFrom, sendAddressTo, "Feedback From site", sb.ToString(), (Infrastructure.ObjectContext.Current.User != null ? Infrastructure.ObjectContext.Current.User.EmailAddress : null) , null);
            
			_sent = true;
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            base.RenderContents(writer);
			ResourceManager rmCommon = WebUtils.GetResource("Common");
			ResourceManager rm = WebUtils.GetResource("Ajax.SiteFeedback");

            if (!_sent)
            {
                writer.Write("<table width=\"420\">");

				writer.Write("<tr>");
				writer.Write(string.Format("<td colspan=\"3\">{0}<br/><br/></td>", rm.GetString("Header")));
				writer.Write("</tr>");

				writer.Write("<tr>");
				writer.Write(string.Format("<td colspan=\"3\"><span id='siteFeedbackErrorLine' style='display:none; color:maroon;'>{0}</span></td>", rm.GetString("NoContent")));
				writer.Write("</tr>");
		
                writer.Write("<tr>");
				writer.Write(string.Format("<td valign=\"top\">{0}</td><td width=\"25\"><br/></td>", rmCommon.GetString("Subject")));
				writer.Write(string.Format("<td><input type=\"radio\" name=\"SiteFeedback_Type\" id=\"SiteFeedback_Page\" onclick=\"SiteFeedbackUpdateView()\" checked>{0} &nbsp;", rm.GetString("FeedBackCurrentPage")));
				writer.Write(string.Format("<input type=\"radio\" name=\"SiteFeedback_Type\" id=\"SiteFeedback_Site\" onclick=\"SiteFeedbackUpdateView()\">{0} &nbsp;", rm.GetString("FeedBackOveralSite")));
                writer.Write(string.Format("<input type=\"radio\" name=\"SiteFeedback_Type\" id=\"SiteFeedback_Other\" onclick=\"SiteFeedbackUpdateView()\">{0}</td>", rm.GetString("FeedBackOther")));
                writer.Write("</tr>");

                writer.Write("<tr>");
				writer.Write(string.Format("<td></td><td></td><td><input style=\"width:380px;\" id=\"SiteFeedback_Subject\"></td>"));
                writer.Write("</tr>");

                writer.Write("<tr>");
				writer.Write(string.Format("<td valign=\"top\">{0}</td><td></td><td><textarea style=\"width:380px; height:60px;\" id=\"SiteFeedback_Body\"></textarea></td>", rmCommon.GetString("Body")));
                writer.Write("</tr>");

                writer.Write("<tr>");
				writer.Write(string.Format("<td></td><td></td><td><input id=\"SiteFeedback_SendButton\" type=\"button\" value=\"{0}\" onclick=\"SiteFeedbackSend()\">", rmCommon.GetString("Send")));
                writer.Write("</tr>");

				writer.Write("<tr>");
				writer.Write(string.Format("<td colspan=\"3\" style=\"font-size:11px;\"><br/>{0}</td>", rm.GetString("Footer")));
				writer.Write("</tr>");

                writer.Write("</table>");

                // script
                writer.Write("<script type=\"text/javascript\">");
                writer.Write("function SiteFeedbackUpdateView(){");
                writer.Write("document.getElementById(\"SiteFeedback_Subject\").disabled = !document.getElementById(\"SiteFeedback_Other\").checked;");
                writer.Write("if(document.getElementById(\"SiteFeedback_Site\").checked) document.getElementById(\"SiteFeedback_Subject\").value = 'SITE';");
                writer.Write("else if(document.getElementById(\"SiteFeedback_Page\").checked) document.getElementById(\"SiteFeedback_Subject\").value = document.URL;");
                writer.Write("else document.getElementById(\"SiteFeedback_Subject\").value = '';");
                writer.Write("} SiteFeedbackUpdateView();");
                writer.Write("function SiteFeedbackSend()");
                writer.Write("{");
                writer.Write("var subject = document.getElementById(\"SiteFeedback_Subject\").value; ");
                writer.Write("var body = document.getElementById(\"SiteFeedback_Body\").value; ");
				writer.Write(string.Format("if (body.length < 1 || subject.length < 1){{$('#siteFeedbackErrorLine').show(); return;}}; ", rm.GetString("NoContent")));
				writer.Write("top.netpay.ajaxApi.AjaxMethods.GetSiteFeedback(subject, body, function(response){ netpay.Common.openPopup(response.Data, 'PopupSiteFeedback', 'btnFeedback', -260, 0); }); ");
                writer.Write("}");
            }
			else 
			{
				writer.Write("<table width=\"420\"><tr><td>");
                writer.Write("<br/>" + rm.GetString("SiteFeedbackDone") + "<br/>");
				writer.Write("</td></tr></table>");
            }

            writer.Write("</script>");
        }
    }
}
