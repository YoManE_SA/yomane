﻿using System;
using System.Resources;
using System.Web.UI;

namespace Netpay.Web.Controls.Ajax.Controls
{
	public class Capture : WindowControl
	{
		public Capture()
		{
			ResourceManager rm = WebUtils.GetResource("Ajax.Capture");
			Title = rm.GetString("title");
		}
	
		private Bll.Transactions.Transaction _transaction = null;

		public Bll.Transactions.Transaction Transaction
		{
			get { return _transaction; }
			set { _transaction = value; }
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			base.RenderContents(writer);

			ResourceManager rm = WebUtils.GetResource("Ajax.Capture");
			ResourceManager rmCommon = WebUtils.GetResource("Common");

			// script
			writer.Write("<script type=\"text/javascript\">");
			writer.Write("function DoCapture(transactionID)");
			writer.Write("{");
			writer.Write("var amount = Number(document.getElementById(\"netpayWebControlsCaptureAmount\").value); ");
			writer.Write("var comment = document.getElementById(\"netpayWebControlsCaptureComment\").value; ");
			writer.Write(string.Format("if (isNaN(amount)){{netpay.Common.openPopup(\"{0}\", \"captureInvalidNumber\"); return;}}; ", rm.GetString("invalidAmount")));
			writer.Write("top.netpay.ajaxApi.AjaxMethods.DoCapture(transactionID, amount, comment, function(response){top.netpay.ajaxApi.AjaxMethods.GetCapture(this.transactionID, function(response){top.netpay.Common.openPopup(response.Data);})}, null, { 'transactionID': transactionID, 'transactionStatus': 'Authorized' }); ");
			writer.Write("}");
			writer.Write("</script>");
			
			// body
			writer.Write("<div style=\"border:0px solid black;\">");
			if (_transaction.PassedTransactionID == null)
			{
				writer.Write(rm.GetString("CaptureTransaction"));
				writer.Write("<br><div>");
                writer.Write(string.Format("{0} &nbsp;<label>{1:0.00}</label>", rmCommon.GetString("Amount"), _transaction.Amount));
				writer.Write(string.Format("{0} &nbsp;<input id=\"netpayWebControlsCaptureAmount\" type=\"text\" value=\"{1:0.00}\" style=\"width:70px;\"/>", rmCommon.GetString("Amount"), _transaction.Amount));
				writer.Write(string.Format("&nbsp;&nbsp;{0} &nbsp;<input id=\"netpayWebControlsCaptureComment\" type=\"text\" style=\"width:175px;\" />", rmCommon.GetString("Comment")));
				writer.Write(string.Format("&nbsp;&nbsp;&nbsp;<input type=\"button\" value=\"{0}\" onclick=\"DoCapture('{1}')\" id=\"netpayWebControlsCaptureSubmit\" />", rm.GetString("btnCapture"), _transaction.ID));
				writer.Write("</div>");
			}
			else
			{
				writer.Write(rmCommon.GetString("TransactionAlreadyCaptured").Replace("#", _transaction.PassedTransactionID.ToString()));
			}
			writer.Write("</div>");
		}
	}
}
