﻿namespace Netpay.Web.Ajax
{
	public class AjaxResponse
	{
		private AjaxResult _result = AjaxResult.Undefined;
		private string _resultDescription = null;
		private object _data = null;

		public AjaxResponse(){}

		public AjaxResponse(AjaxResult result)
		{
			Result = result;
		}

		public AjaxResponse(AjaxResult result, string description)
		{
			Result = result;
			ResultDescription = description;
		}

		public AjaxResponse(AjaxResult result, string description, object data)
		{
			Result = result;
			ResultDescription = description;
			Data = data;
		}

		public AjaxResult Result
		{
			get { return _result; }
			set { _result = value; }
		}

		public string ResultDescription
		{
			get { return _resultDescription; }
			set { _resultDescription = value; }
		}

		public object Data
		{
			get { return _data; }
			set { _data = value; }
		}
	}
}
