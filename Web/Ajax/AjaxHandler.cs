﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.SessionState;
using Netpay.Infrastructure;

namespace Netpay.Web.Ajax
{
	/// <summary>
	/// Generic handler for ajax calls
    /// Register in site to use ajax methods
	/// </summary>
    public class AjaxHandler : IHttpHandler, IRequiresSessionState
	{
		#region IHttpHandler Members

		public bool IsReusable
		{
			get { return true; }
		}

		public void ProcessRequest(HttpContext context)
		{
			AjaxResponse response = new AjaxResponse();

			string typeName = context.Request["ajaxClass"];
			string methodName = context.Request["ajaxMethod"];
			string args = context.Request["args"];

			Dictionary<string, object> deserializedArgs = args.FromJson();
			string[] preparedArgs;
			if (deserializedArgs != null)
				preparedArgs = deserializedArgs.Select(kvp => kvp.Value.ToString()).ToArray();
			else
				preparedArgs = new string[0];

			// check class attribute
			string fullTypeName = "Netpay.Web.Ajax." + typeName;
			Type type = Type.GetType(fullTypeName);
			if (type == null || type.GetCustomAttributes(typeof(AjaxClass), false).Count() == 0)
			{
				response.Result = AjaxResult.Error;
				response.ResultDescription = "Class not found.";
				context.Response.Write(response.ToJson());
				return;
			}

			// method attribute
			MethodInfo method = type.GetMethod(methodName);
			if (method == null || method.GetCustomAttributes(typeof(AjaxMethod), false).Count() == 0)
			{
				response.Result = AjaxResult.Error;
				response.ResultDescription = "Method not found.";
				context.Response.Write(response.ToJson());
				return;
			}

			try
			{
				// invoke
				response = (AjaxResponse)type.InvokeMember(methodName, BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Static, null, null, preparedArgs.ToArray());
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
				
				if (ex.InnerException.GetType() == typeof(NotLoggedinException))
					response.Result = AjaxResult.NotLoggedIn;
				else if (ex.InnerException.GetType() == typeof(MethodAccessDeniedException))
					response.Result = AjaxResult.MethodAccessDenied;
				else
					response.Result = AjaxResult.Error;
					
				response.ResultDescription = "An error has occured, see logs.";
			}
				
			context.Response.Write(response.ToJson());
		}

		#endregion
	}
}
