﻿using System;

namespace Netpay.Web.Ajax
{
    /// <summary>
    /// Flags a class for ajax invokation
    /// Without this flag an ajax class will not be found and the called method will not be invoked
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
	internal class AjaxClass : Attribute
	{
	}
}