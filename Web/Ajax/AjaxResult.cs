﻿namespace Netpay.Web.Ajax
{
	public enum AjaxResult
	{
		Undefined = 0,
		Success = 1,
		Error = 2,
		NotLoggedIn = 3,
		MethodAccessDenied = 4,
		NoPermission = 5
	}
}
