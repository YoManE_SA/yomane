﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using Netpay.Bll;
using Netpay.Bll.Risk;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Web.Controls.Ajax.Controls;

namespace Netpay.Web.Ajax
{
    [AjaxClass]
    internal static class AjaxMethods
    {
        [AjaxMethod]
        public static AjaxResponse KeepSession()
        {
            AjaxResponse response = new AjaxResponse();
            if (!WebUtils.IsLoggedin)
            {
                response.Result = AjaxResult.NotLoggedIn;
                return response;
            }

            response.Result = AjaxResult.Success;
            response.Data = new int[] { WebUtils.AlertSessionTimeout, WebUtils.SessionTimeout };
            return response;
        }

		private static AjaxResponse ValidateUser()
		{
			var user = ObjectContext.Current.User;
            if (user == null)
                return new AjaxResponse(AjaxResult.NotLoggedIn, "Not logged in.");
			if(!ObjectContext.Current.IsUserOfType(UserRole.Merchant, SecuredObject.Get("MCP", "PrintSlip"), PermissionValue.Execute, false))
                return new AjaxResponse(AjaxResult.NoPermission, "No permission.");
			return null;
		}

        [AjaxMethod]
        public static AjaxResponse GetMerchantNameAutoComplete(string merchantNamePrefix, string additionalParams)
        {
            AjaxResponse response = new AjaxResponse();
            if (merchantNamePrefix == null || merchantNamePrefix.Trim() == "")
            {
                response.Result = AjaxResult.Error;
                response.ResultDescription = "No request supplied";
            }
            else
            {
                var merchants = Bll.Merchants.Merchant.AutoComplete(merchantNamePrefix.ToLower()).OrderBy(c => c.AccountName).Select(c => new { value = c.TargetID.ToString(), label = c.AccountName }).ToList();
                response.Result = AjaxResult.Success;
                response.Data = merchants;
            }

            return response;
        }

        [AjaxMethod]
        public static AjaxResponse GetDebitTerminalTagAutoComplete(string terminalTagPrefix, string additionalParams)
        {
            AjaxResponse response = new AjaxResponse();
            if (terminalTagPrefix == null || terminalTagPrefix == "")
            {
                response.Result = AjaxResult.Error;
                response.ResultDescription = "No request supplied";
            }
            else
            {
                var tags = Bll.DebitCompanies.Terminal.AutoCompleteTag(terminalTagPrefix.ToLower()).Select(t => new { value = t.TerminalId.ToString(), label = t.TerminalTag }).ToList();
                response.Result = AjaxResult.Success;
                response.Data = tags;
            }

            return response;
        }

        [AjaxMethod]
        public static AjaxResponse GetDebitTerminalAutoComplete(string terminalNamePrefix, string additionalParams)
        {
            AjaxResponse response = new AjaxResponse();
            if (terminalNamePrefix == null || terminalNamePrefix == "")
            {
                response.Result = AjaxResult.Error;
                response.ResultDescription = "No request supplied";
            }
            else
            {
                var terminals = Bll.DebitCompanies.Terminal.AutoComplete(terminalNamePrefix.ToLower()).OrderBy(t => t.TerminalName).Select(t => new { value = t.TerminalNumber, label = t.TerminalName + " - " + t.TerminalNumber }).ToList();
                response.Result = AjaxResult.Success;
                response.Data = terminals;
            }

            return response;
        }

        [AjaxMethod]
        public static AjaxResponse GetDenormalizedTransaction(string transactionID, string transactionStatus, string isChargeback)
        {
            AjaxResponse response = new AjaxResponse();
            int transID = int.Parse(transactionID);
            TransactionStatus status = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), transactionStatus);
            bool? isChb = isChargeback.ToNullableBool();

            var transaction = Netpay.Bll.Reports.ImmediateReports.GetDenormalizedTransaction(status, transID, isChb);
            response.Result = AjaxResult.Success;
            response.Data = transaction;

            return response;
        }

        [AjaxMethod]
        public static AjaxResponse GetTransactionPrintSlip(string transactionID, string transactionStatus)
        {
			//PermissionObjects.TransactionPrintSlip
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, null, PermissionValue.Execute);
            int transID = int.Parse(transactionID);
            TransactionStatus status = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), transactionStatus);

            var transaction = Bll.Transactions.Transaction.GetTransaction(null, transID, status);
            if (transaction == null) return new AjaxResponse(AjaxResult.Error, "Transaction not found.");

            StringBuilder stringBuilder = new StringBuilder();
            HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(stringBuilder));
            TransactionPrintSlip control = new TransactionPrintSlip();
            control.Transaction = transaction;
            control.RenderControl(writer);

            return new AjaxResponse(AjaxResult.Success, null, stringBuilder.ToString());
        }

        [AjaxMethod]
        public static AjaxResponse GetTransactionCreateInvoice(string transactionID)
        {
			//PermissionObjects.Invoice
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, null, PermissionValue.Execute);
			int transID = int.Parse(transactionID);
            string resultText = "";
            try
            {
				int invoiceID = Bll.Invoices.InvoiceCreator.CreateInvoice(transID).GetValueOrDefault();
                resultText = "Invoice created - Invoice Number: " + invoiceID;
            } catch(Exception ex) {
                Netpay.Infrastructure.Logger.Log(ex);
                resultText = "Create invoice failed ";
            }
            return new AjaxResponse(AjaxResult.Success, null, resultText);
        }

        [AjaxMethod]
        public static AjaxResponse GetSiteFeedback(string subject, string body)
        {
            AjaxResponse response = new AjaxResponse();
            SiteFeedback control = new SiteFeedback();

            if (!string.IsNullOrEmpty(subject))
            {
				control.SendMessage(WebUtils.CredentialsToken, subject, body);
                response.Result = AjaxResult.Success;
            }

            StringBuilder stringBuilder = new StringBuilder();
            HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(stringBuilder));
            control.RenderControl(writer);

            response.Result = AjaxResult.Success;
            response.Data = stringBuilder.ToString();

            return response;
        }

        [AjaxMethod]
        public static AjaxResponse GetCapture(string transactionID)
        {
			//PermissionObjects.RefundRequest
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, null, PermissionValue.Read);
            int transID = int.Parse(transactionID);
            var transaction = Bll.Transactions.Transaction.GetTransaction(null, transID, TransactionStatus.Authorized);
            if (transaction == null)
                return new AjaxResponse(AjaxResult.Error, "Transaction not found.");

            StringBuilder stringBuilder = new StringBuilder();
            HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(stringBuilder));
            Capture control = new Capture();
            control.Transaction = transaction;
            control.RenderControl(writer);

            return new AjaxResponse(AjaxResult.Success, null, stringBuilder.ToString());
        }

        [AjaxMethod]
        public static AjaxResponse DoCapture(string transactionID, string amount, string comment)
        {
			//PermissionObjects.RefundRequest
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, null, PermissionValue.Execute);
			AjaxResponse response = new AjaxResponse();
            int transID = int.Parse(transactionID);
            decimal amountParsed = decimal.Parse(amount);
            int? captureID;
			var trans = Bll.Transactions.Transaction.GetTransaction(null, transID, TransactionStatus.Authorized);
			CaptureResult result = trans.CaptureTransaction(amountParsed, comment, out captureID);

            switch (result)
            {
                case CaptureResult.OK:
                    response.Result = AjaxResult.Success;
                    break;
				case CaptureResult.AttemptDeclined:
                    response.Result = AjaxResult.Error;
                    response.ResultDescription = "Transaction Declined";
					break;
                case CaptureResult.AmountTooLarge:
                    response.Result = AjaxResult.Error;
                    response.ResultDescription = "Amount is too big";
                    break;
                default:
                    response.Result = AjaxResult.Error;
                    response.ResultDescription = "An error has occured, see logs.";
                    break;
            }
            return response;
        }

        [AjaxMethod]
        public static AjaxResponse GetRefundRequest(string transactionID, string transactionStatus)
        {
			//PermissionObjects.RefundRequest
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, null, PermissionValue.Execute);
            int transID = int.Parse(transactionID);
            TransactionStatus status = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), transactionStatus);
            var transaction = Bll.Transactions.Transaction.GetTransaction(null, transID, status);
            if (transaction == null) return new AjaxResponse(AjaxResult.Error, "Transaction not found.");

            StringBuilder stringBuilder = new StringBuilder();
            HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(stringBuilder));
            RefundRequest control = new RefundRequest();
            control.Transaction = transaction;
            if (WebUtils.IsLoggedin)
            {
                control.RenderControl(writer);
                return new AjaxResponse(AjaxResult.Success, null, stringBuilder.ToString());
            }
            else
                return new AjaxResponse(AjaxResult.NotLoggedIn, "Session has timed out");
        }

        [AjaxMethod]
        public static AjaxResponse RequestRefund(string transactionID, string transactionStatus, string amount, string comment)
        {
			//PermissionObjects.RefundRequest
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, null, PermissionValue.Execute);
			AjaxResponse response = new AjaxResponse();
            int transID = int.Parse(transactionID);
            TransactionStatus status = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), transactionStatus);
            decimal amountParsed = decimal.Parse(amount);

            response.Result = AjaxResult.Success;
            response.Data = Bll.Transactions.RefundRequest.Create(transID, status, amountParsed, comment).ToString();
            return response;
        }

        [AjaxMethod]
        public static AjaxResponse GetSeriesFromTransaction(string transactionID, string transactionStatus)
        {
			//PermissionObjects.CreateRecurringSeries
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, null, PermissionValue.Execute);
            int transID = int.Parse(transactionID);
            TransactionStatus status = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), transactionStatus);

            var transaction = Bll.Transactions.Transaction.GetTransaction(null, transID, status);
            if (transaction == null) return new AjaxResponse(AjaxResult.Error, "Transaction not found.");

            StringBuilder stringBuilder = new StringBuilder();
            HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(stringBuilder));
            SeriesFromTransaction control = new SeriesFromTransaction();
            control.Transaction = transaction;
            control.RenderControl(writer);

            return new AjaxResponse(AjaxResult.Success, null, stringBuilder.ToString());
        }

        [AjaxMethod]
        public static AjaxResponse CreateSeriesFromTransaction(string transactionID, string transactionStatus, string charges, string intervalCount, string intervalUnit, string amount, string comment)
        {
			//PermissionObjects.CreateRecurringSeries
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, null, PermissionValue.Execute);
			int transactionIDTyped = int.Parse(transactionID);
            TransactionStatus transactionStatusTyped = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), transactionStatus);
            int chargesTyped = int.Parse(charges);
            int intervaCountlTyped = int.Parse(intervalCount);
            ScheduleInterval intervalUnitTyped = (ScheduleInterval)Enum.Parse(typeof(ScheduleInterval), intervalUnit);
            decimal? amountTyped = amount.ToNullableDecimal();

            if (ObjectContext.Current.User.Role != UserRole.Admin)
                if (!Bll.Transactions.Recurring.MerchantSettings.Current.CanCreateFromExistingTransaction)
                    throw new Exception("Recurring feature disabled, contact support");

            int seriesID;
            CreateSeriesResult result = Bll.Transactions.Recurring.Series.CreateSeries(transactionIDTyped, transactionStatusTyped, chargesTyped, intervaCountlTyped, intervalUnitTyped, amountTyped, HttpContext.Current.Request.UserHostAddress, comment, out seriesID);
            if (result == CreateSeriesResult.Success) return GetSeriesFromTransaction(transactionID, transactionStatus);
            else return new AjaxResponse(AjaxResult.Error, result.ToString());
        }

        [AjaxMethod]
        public static AjaxResponse AddToBlacklist(string transactionID, string transactionStatus)
        {
			//PermissionObjects.AddTransactionCardToBacklist
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, null, PermissionValue.Execute);
            int transactionIDTyped = int.Parse(transactionID);
            TransactionStatus transactionStatusTyped = (TransactionStatus)Enum.Parse(typeof(TransactionStatus), transactionStatus);
			var transaction = Bll.Transactions.Transaction.GetTransaction(null, transactionIDTyped, transactionStatusTyped);

			AddBlockedCardResult result = AddBlockedCardResult.Success;
            RiskItem.BlockCardFromTransaction(transactionIDTyped, transactionStatusTyped);

            StringBuilder stringBuilder = new StringBuilder();
            HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(stringBuilder));
            AddToBlacklist control = new AddToBlacklist();
            control.Result = result;
            control.RenderControl(writer);

            return new AjaxResponse(AjaxResult.Success, null, stringBuilder.ToString());
        }
    }
}
