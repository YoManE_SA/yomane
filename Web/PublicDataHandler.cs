﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web
{
    public class PublicDataHandler : System.Web.Routing.IRouteHandler
    {
        public System.Web.IHttpHandler GetHttpHandler(System.Web.Routing.RequestContext requestContext)
        {
            var rawUrl = (string)requestContext.RouteData.Values["ref"];
            var path = Infrastructure.Domain.Current.MapPublicDataPath(rawUrl);
            if (!System.IO.File.Exists(path)) {
                requestContext.HttpContext.Response.StatusCode = 404;
                //requestContext.HttpContext.Response.Status = "File not found";
            } else requestContext.HttpContext.Response.WriteFile(path);
            requestContext.HttpContext.Response.End();
            return null;
        }

        public static void RegisterRouter()
        {
            System.Web.Routing.RouteTable.Routes.Add(new System.Web.Routing.Route("Data/{*ref}", new PublicDataHandler()));
        }
    }
}
