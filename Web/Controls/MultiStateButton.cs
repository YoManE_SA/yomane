﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	public class MultiStateButton : System.Web.UI.WebControls.WebControl, System.Web.UI.IPostBackDataHandler, System.Web.UI.IPostBackEventHandler, System.Web.UI.ICallbackEventHandler
	{
		public int Value { get; set; }
		public System.Collections.Generic.List<System.Drawing.Color> Values { get; set; }
		public event EventHandler ValueChanged;
		public bool AutoPostBack { get; set; }
		public bool AutoCallBack { get; set; }

		public MultiStateButton() : base(System.Web.UI.HtmlTextWriterTag.Span) { CssClass = "smallCircle"; }

		protected override void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);
			if (Values != null) writer.AddStyleAttribute("background-color", System.Drawing.ColorTranslator.ToHtml(Values[Value]));
			writer.AddStyleAttribute("cursor", "pointer");
			string clickScript = "var values = " + ID + "_Values; var newValue = (parseInt(document.getElementById('" + ClientID + "_Input').value) + 1) % values.length;";
			if (AutoCallBack)
				clickScript += Page.ClientScript.GetCallbackEventReference(this, "newValue", "function(arg, context){ document.getElementById('" + ClientID + "_Input').value = arg; context.style.backgroundColor = values[arg]; }", "this");
			else {
				clickScript += "document.getElementById('" + ClientID + "_Input').value = newValue;";
				if (AutoPostBack) clickScript += Page.ClientScript.GetPostBackEventReference(this, "Click", true);
				else clickScript += "this.style.backgroundColor = values[newValue];";
			}
			writer.AddAttribute("onClick", clickScript); 
		}

		protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
		{
			writer.Write("<input type=\"hidden\" id=\"{0}_Input\" name=\"{0}\" value=\"{1}\" />", ClientID, Value);
			base.RenderContents(writer);
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (Page.Items[ID + "_Values"] == null && Values != null)
			{
				Page.ClientScript.RegisterArrayDeclaration(ID + "_Values", string.Join(", ", Values.Select(c => "'" + System.Drawing.ColorTranslator.ToHtml(c) + "'")));
 				Page.Items[ID + "_Values"] = new object();
			}
			base.OnPreRender(e);
		}

		bool System.Web.UI.IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			Value = postCollection[postDataKey].ToInt32();
			return true;
		}

		void System.Web.UI.IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			if (ValueChanged != null) ValueChanged(this, EventArgs.Empty);
		}

		void System.Web.UI.IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			if (ValueChanged != null) ValueChanged(this, EventArgs.Empty);
		}

		public string GetCallbackResult()
		{
			return Value.ToString();
		}

		public void RaiseCallbackEvent(string eventArgument)
		{
			Value = eventArgument.ToInt32();
			if (ValueChanged != null) ValueChanged(this, EventArgs.Empty);
		}
	}
}
