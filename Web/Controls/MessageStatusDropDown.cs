﻿using System;
using System.Web.UI;

namespace Netpay.Web.Controls
{
    [ToolboxData("<{0}:MessageStatusDropDown runat=server></{0}:MessageStatusDropDown>")]
    public class MessageStatusDropDown : DropDownBase
    { 
        protected override void DataBind(bool raiseOnDataBinding)
        {
            DataSource = Emails.MessageStatus.Cache.Values;
            DataValueField = "ID";
            DataTextField = "Name";
            base.DataBind(raiseOnDataBinding);
        }
    }
}
