﻿using System;
using System.Resources;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:Login runat=server></{0}:Login>")]
	public class Login : WebControl
	{
		private Infrastructure.Security.UserRole _userType = Infrastructure.Security.UserRole.Account;
		private Orientation _layout = Orientation.Horizontal;
		private Label _message = null;
		private TextBox _email = null;
		private TextBox _userName = null;
		private TextBox _password = null;
		private Button _login = null;
		private bool _showMessageBox = false;
        private bool _enableAutocomplete = false;
        public event EventHandler LoginSuccess;

        public bool EnableAutocomplete
        {
            get { return _enableAutocomplete; }
            set { _enableAutocomplete = value; }
        }

        public bool SaveCockie { get; set; }

		public bool ShowMessageBox
		{
			get { return _showMessageBox; }
			set { _showMessageBox = value; }
		}

		public Orientation Layout
		{
			get { return _layout; }
			set { _layout = value; }
		}

		public Infrastructure.Security.UserRole UserType
		{
			get { return _userType; }
			set { _userType = value; }
		}

		protected void LoginClick(object sender, EventArgs e)
		{
			if (_userType == Infrastructure.Security.UserRole.System)
				throw new ApplicationException("Unknown user type");
			ResourceManager rm = WebUtils.GetResource("Login");

			if (_email.Text.Trim() == "") { _message.Text = rm.GetString("EmailAddressIsMissing"); return; }
			if (_userName.Text.Trim() == "") { _message.Text = rm.GetString("UsernameIsMissing"); return; }
			if (_password.Text.Trim() == "") { _message.Text = rm.GetString("PasswordIsMissing"); return; }

			if (!RegularExpressions.email.IsMatch(_email.Text)) { _message.Text = rm.GetString("InvalidEmail"); return; }

			Guid credentialsToken = Guid.Empty;
			var result = Infrastructure.Security.LoginResult.GeneralFailure;
			if (_userType == Infrastructure.Security.UserRole.Merchant)
            {
				result = WebUtils.Login(new Infrastructure.Security.UserRole[] { Infrastructure.Security.UserRole.Merchant, Infrastructure.Security.UserRole.MerchantSubUser }, _userName.Text.Trim(), _email.Text.Trim(), _password.Text.Trim());
                //if (result != LoginResult.Success)
                //    result = WebUtils.Login(UserType.MerchantLimited, _userName.Text.Trim(), _email.Text.Trim(), _password.Text.Trim()); 
            }

			if (_userType == Infrastructure.Security.UserRole.Admin)
            {
				result = WebUtils.Login(new Infrastructure.Security.UserRole[] { _userType }, _userName.Text.Trim(), _email.Text.Trim(), _password.Text.Trim());
				//if (result != Infrastructure.Security.LoginResult.Success)
				//	result = WebUtils.Login(_userType, _userName.Text.Trim(), _email.Text.Trim(), _password.Text.Trim());
            }

			if (_userType == Infrastructure.Security.UserRole.Partner)
				result = WebUtils.Login(new Infrastructure.Security.UserRole[] { _userType }, _userName.Text.Trim(), _email.Text.Trim(), _password.Text.Trim());

			if (result == Infrastructure.Security.LoginResult.Success) 
			{
				_message.Text = "";
                if (SaveCockie) 
                {
                    HttpCookie loginCockie = Page.Response.Cookies.Get("NPLogin");
                    if (loginCockie == null) loginCockie = new HttpCookie("NPLogin");
                    loginCockie.Values.Add("Email", _email.Text.Trim());
                    loginCockie.Values.Add("UserName", _userName.Text.Trim());
                    loginCockie.Expires = DateTime.Now.AddMonths(3);
                    Page.Response.Cookies.Add(loginCockie);
                }

				if (LoginSuccess != null) 
					LoginSuccess(sender, e);
			} 
			else 
				_message.Text = rm.GetString(Netpay.Infrastructure.Security.LoginResult.WrongPassword.ToString());
		}

		protected override void OnInit(EventArgs e)
		{
            _message = new Label();
			_message.Attributes.Add("style", "color:red; font-weight:bold;");
			this.Controls.Add(_message);

			_email = new TextBox();
			_email.ID = "txtEmail";
            if (!EnableAutocomplete)
            {
                _email.AutoCompleteType = AutoCompleteType.None;
                _email.Attributes.Add("autocomplete", "off"); 
            }
			this.Controls.Add(_email);

			_userName = new TextBox();
			_userName.ID = "txtUserName";
            if (!EnableAutocomplete)
            {
                _userName.AutoCompleteType = AutoCompleteType.None;
                _userName.Attributes.Add("autocomplete", "off"); 
            }
			this.Controls.Add(_userName);

			_password = new TextBox();
			_password.TextMode = TextBoxMode.Password;
			_password.ID = "txtPassword";
            if (!EnableAutocomplete)
            {
                _password.AutoCompleteType = AutoCompleteType.None;
                _password.Attributes.Add("autocomplete", "off"); 
            }
			this.Controls.Add(_password);

            if (SaveCockie)
            {
                HttpCookie loginCockie = Page.Request.Cookies.Get("NPLogin");
                if (loginCockie != null)
                {
                    _email.Text = loginCockie.Values["Email"].EmptyIfNull();
                    _userName.Text = loginCockie.Values["UserName"].EmptyIfNull();
                }
            }

            if (!Page.IsPostBack)
			{
				if (HttpContext.Current.Request["login"] == "outer")
				{
					_email.Text = System.Web.HttpContext.Current.Request["mail"];
					_userName.Text = System.Web.HttpContext.Current.Request["username"];
					_password.TextMode = TextBoxMode.SingleLine;
					_password.Text = System.Web.HttpContext.Current.Request["userpassword"];
					_password.Visible = false;
				}
			}

			_login = new Button();
			_login.ID = "btnLogin";
			_login.Attributes.Add("style", "text-align:center; width:50%;");
			_login.Attributes.Add("class", "loginbtn btn btn-primary");
			_login.Click += LoginClick;

			this.Controls.Add(_login);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			ResourceManager rm = WebUtils.GetResource("Common");
	
			writer.Write("<table class=\"webControlsLogin\">");
			if (_layout == Orientation.Horizontal)
			{
				if (!string.IsNullOrEmpty(_message.Text)) 
				{
					if (ShowMessageBox)
					{
						this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alertScriptBlock", string.Format("alert(\"{0}\")", _message.Text), true);
					}
					else
					{
						writer.Write("<tr>");
						writer.Write("<td colspan=\"2\">");
						writer.Write("<div class=\"error\">");
						_message.RenderControl(writer);
						writer.Write("</div>");
						writer.Write("</td>");
						writer.Write("</tr>");
					}
				}
				
				writer.Write("<tr>");
				writer.Write("<td align=\"" + WebUtils.CurrentAlignment + "\">");
				writer.Write(rm.GetString("EMail"));
				writer.Write("</td>");
				writer.Write("<td>");
				_email.RenderControl(writer);
				writer.Write("</td>");
				writer.Write("</tr>");

				writer.Write("<tr>");
				writer.Write("<td align=\"" + WebUtils.CurrentAlignment + "\">");
				writer.Write(rm.GetString("UserName"));
				writer.Write("</td>");
				writer.Write("<td>");
				_userName.RenderControl(writer);
				writer.Write("</td>");
				writer.Write("</tr>");

				writer.Write("<tr>");
				writer.Write("<td align=\"" + WebUtils.CurrentAlignment + "\">");
				writer.Write(rm.GetString("Password"));
				writer.Write("</td>");
				writer.Write("<td>");
				_password.RenderControl(writer);
				writer.Write("</td>");
				writer.Write("</tr>");

				writer.Write("<tr><td>&nbsp;</td></tr>");
				writer.Write("<tr>");
				writer.Write("<td>&nbsp;</td>");
				writer.Write("<td align=\"" + WebUtils.CurrentAlignment + "\">");
				_login.Text = rm.GetString("Login");
				_login.RenderControl(writer);
				writer.Write("</td>");
				writer.Write("</tr>");	
			}
			else
			{
				if (!string.IsNullOrEmpty(_message.Text))
				{
					if (ShowMessageBox)
					{
						this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alertScriptBlock", string.Format("alert(\"{0}\")", _message.Text), true);
					}
					else
					{
						writer.Write("<tr>");
						writer.Write("<td>");
						writer.Write("<div class=\"error\">");
						_message.RenderControl(writer);
						writer.Write("</div>");
						writer.Write("</td>");
						writer.Write("</tr>");
					}
				}
				
				writer.Write("<tr>");
				writer.Write("<td>");
				writer.Write(rm.GetString("EMail"));
				writer.Write("<br/>");
				_email.RenderControl(writer);
				writer.Write("</td>");
				writer.Write("</tr>");

				writer.Write("<tr>");
				writer.Write("<td>");
				writer.Write(rm.GetString("UserName"));
				writer.Write("<br/>");
				_userName.RenderControl(writer);
				writer.Write("</td>");
				writer.Write("</tr>");

				writer.Write("<tr>");
				writer.Write("<td>");
				writer.Write(rm.GetString("Password"));
				writer.Write("<br/>");
				_password.RenderControl(writer);
				writer.Write("</td>");
				writer.Write("</tr>");

				writer.Write("<tr><td>&nbsp;</td></tr>");
				writer.Write("<tr>");
				writer.Write("<td>");
				_login.Text = rm.GetString("Login");
				_login.RenderControl(writer);
				writer.Write("</td>");
				writer.Write("</tr>");	
			}
			writer.Write("</table>");	
		}
	}
}



