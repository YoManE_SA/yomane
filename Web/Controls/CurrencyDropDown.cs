using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:CurrencyDropDown runat=server></{0}:CurrencyDropDown>")]
	public class CurrencyDropDown : DropDownBase
	{
        public bool ShowSymbol { get; set; }

		private List<Bll.Currency> _IncludeCurrencies = null;
		
		public void SetCurrencyIDs(string delimitedCurrencyIDs)
		{
			if (delimitedCurrencyIDs == null || delimitedCurrencyIDs.Trim() == "")
				return;

			string[] preParse = delimitedCurrencyIDs.Split(',');
			int[] parsed = preParse.Select(id => int.Parse(id)).ToArray();
			SetCurrencyIDs(parsed);
		}

		public void SetCurrencyIDs(int[] currencyIDs)
		{
			_IncludeCurrencies = Bll.Currency.Cache.Where(c => currencyIDs.Contains(c.ID)).ToList();
		}
		
		public string IncludeCurrencyIDs
		{
			set 
			{
				SetCurrencyIDs(value);
                Value = null;
			}
		}

		protected override void DataBind(bool raiseOnDataBinding)
		{
			List<Bll.Currency> ds = null;
			if (_IncludeCurrencies == null)
				ds = Bll.Currency.Search();
			else
				ds = _IncludeCurrencies;

			// sort
			//ds = ds.OrderBy(c => c.IsoCode).ToList();

			this.DataSource = ds;
            if (ShowSymbol) this.DataTextField = "Symbol";
            else this.DataTextField = "IsoCode";

            this.DataValueField = "ID";
			base.DataBind(raiseOnDataBinding); 
		}

		public string SelectedCurrencyIso
		{
			get{
				var selValue = Value.ToNullableInt();
				if (selValue == null) return null;
				return Bll.Currency.Get(selValue.Value).IsoCode;
			}
			set{
				if (string.IsNullOrEmpty(value)) SelectedValue = null;
				else Value = Bll.Currency.Get(value).ID.ToString();
			}
		}

	}
}
