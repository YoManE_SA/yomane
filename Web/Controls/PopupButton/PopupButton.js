﻿if (netpay == null) { var netpay = new Object() }
if (netpay.webControls == null) { netpay.webControls = new Object() }

netpay.webControls.PopupButton =
{
	open: function (baseId, cssDirection, horizontalOffset, directionFlip, openEvent)
	{
		var anchorImageElement = $("#" + baseId + "AnchorImage");
		var anchorTextElement = $("#" + baseId + "AnchorText");
		var anchorElement = anchorImageElement == null ? anchorTextElement : anchorImageElement;
		var contentElement = $("#" + baseId + "Content");
		var backgroundElement = $("#" + baseId + "Background");

		var direction = "left";
		if (cssDirection == "rtl")
			direction = "right";

		if (directionFlip)
		{
			if (direction == "left")
				direction = "right";
			else
				direction = "left";
		}

		contentElement.fadeIn(0);
		contentElement.position
		({
			of: anchorElement,
			my: direction + " top",
			at: direction + " bottom",
			offset: horizontalOffset + " 5",
			collision: "fit none"
		});

		backgroundElement.css("height", "10px");
		backgroundElement.css("width", contentElement.width() + "px");
		//backgroundElement.css("backgroundColor", "yellow");
		backgroundElement.fadeIn(0);
		backgroundElement.position
		({
			of: anchorElement,
			my: direction + " top",
			at: direction + " bottom",
			offset: horizontalOffset + " 0",
			collision: "fit none"
		});

        if (openEvent == 'onmouseover') 
        {
            // subscribe events
            if (anchorImageElement != null) 
            {
                anchorImageElement.unbind();
                anchorImageElement.mouseleave(function (event) { netpay.webControls.PopupButton.close(baseId, event) });
            }

            if (anchorTextElement != null) 
            {
                anchorTextElement.unbind();
                anchorTextElement.mouseleave(function (event) { netpay.webControls.PopupButton.close(baseId, event) });
            }

            contentElement.unbind();
            contentElement.mouseleave(function (event) { netpay.webControls.PopupButton.close(baseId, event) });
            backgroundElement.unbind();
            backgroundElement.mouseleave(function (event) { netpay.webControls.PopupButton.close(baseId, event) });
        }
	},

	close: function (baseId, event)
	{
		if (baseId == null)
			return;

        if (event != null)
        {
 		    if (event.relatedTarget.id != baseId + "AnchorImage" && event.relatedTarget.id != baseId + "AnchorText" && event.relatedTarget.id != baseId + "Content" && event.relatedTarget.id != baseId + "Background")
		    {
			    $("#" + baseId + "Content").fadeOut(0);
			    $("#" + baseId + "Background").fadeOut(0);
		    }       
        }
        else
        {
			$("#" + baseId + "Content").fadeOut(0);
			$("#" + baseId + "Background").fadeOut(0);
        }
	}
}