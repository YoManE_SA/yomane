﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:PopupButton runat=server></{0}:PopupButton>")]
	[ParseChildren(false)]
	[PersistChildren(false)]
	public class PopupButton : WebControl
	{
		private string _iconSrc = null;
		private string _text = null;
		private int _horizontalOffset = 0;
		private bool _directionFlip = false;
		private PopupOpenEvent _openEvent = PopupOpenEvent.MouseOver;
		private string _cursor = "help";
		private string PopupStyle { get; set; }

		public string Cursor
		{
			get { return _cursor; }
			set { _cursor = value; }
		}

		public PopupOpenEvent OpenEvent
		{
			get { return _openEvent; }
			set { _openEvent = value; }
		}

		public bool DirectionFlip
		{
			get { return _directionFlip; }
			set { _directionFlip = value; }
		}

		public bool ImageOnLeft { get; set; }

		public int HorizontalOffset
		{
			get { return _horizontalOffset; }
			set { _horizontalOffset = value; }
		}

		public string IconSrc
		{
			get { return _iconSrc; }
			set { _iconSrc = value; }
		}

		public string Text
		{
			get { return _text; }
			set { _text = value; }
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			// embed scripts
			this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.PopupButton.PopupButton.js");
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (_iconSrc == null && _text == null)
				throw new ApplicationException("PopupButton control must have an icon image or text.");

			string openEvent = null;
			if (_openEvent == PopupOpenEvent.MouseClick)
				openEvent = "onclick";
			else
				openEvent = "onmouseover";

			// anchor
			if (_iconSrc != null && !ImageOnLeft)
				writer.Write(string.Format("<img id=\"{0}AnchorImage\" style=\"cursor:{6};\" src=\"{1}\" {5}=\"javascript:netpay.webControls.PopupButton.open('{0}', '{2}', {3}, {4}, '{5}')\" />&nbsp;", ClientID, _iconSrc, WebUtils.CurrentDirection, _horizontalOffset, _directionFlip.ToString().ToLower(), openEvent, _cursor));
			if (_text != null)
				writer.Write(string.Format("<span id=\"{0}AnchorText\" style=\"cursor:{6};\" {5}=\"javascript:netpay.webControls.PopupButton.open('{0}', '{1}', {3}, {4}, '{5}')\">{2}</span>", ClientID, WebUtils.CurrentDirection, _text, _horizontalOffset, _directionFlip.ToString().ToLower(), openEvent, _cursor));
			if (_iconSrc != null && ImageOnLeft)
				writer.Write(string.Format("&nbsp;<img id=\"{0}AnchorImage\" style=\"cursor:{6};\" src=\"{1}\" {5}=\"javascript:netpay.webControls.PopupButton.open('{0}', '{2}', {3}, {4}, '{5}')\" />", ClientID, _iconSrc, WebUtils.CurrentDirection, _horizontalOffset, _directionFlip.ToString().ToLower(), openEvent, _cursor));

			// background
			if (_openEvent == PopupOpenEvent.MouseOver)
				writer.Write(string.Format("<div id=\"{0}Background\" style=\"cursor:{1}; display:none; position:absolute; padding:6px; \"></div>", ClientID, _cursor));

			// content
			writer.Write(string.Format("<div id=\"{0}Content\" style=\"cursor:{1}; display:none; position:absolute; color: #000000; background-color:#ffffff; border:1px solid #000000; z-index: 1; padding:6px;{2}\">", ClientID, _cursor, PopupStyle));
			writer.Write("<table>");

			if (_openEvent == PopupOpenEvent.MouseClick)
			{
				writer.Write("<tr><td align='" + WebUtils.CurrentAlignment + "'>");
				writer.Write(string.Format("<a href=\"javascript:netpay.webControls.PopupButton.close('{0}', null)\">[{1}]</a>", ClientID, WebUtils.GetResourceValue("Common", "Close")));
				writer.Write("</td></tr>");
			}

			writer.Write("<tr><td>");
			foreach (Control currentControl in this.Controls)
				currentControl.RenderControl(writer);
			writer.Write("</td></tr>");
			writer.Write("</table>");
			writer.Write("</div>");

			if (System.Web.HttpContext.Current.Request.Browser.Type == "IE9")
			{
				writer.Write(string.Format("<script>$(function() {{netpay.webControls.PopupButton.open('{0}', '{1}', {2}, {3}, '{4}'); netpay.webControls.PopupButton.close('{0}', null);}})</script>", ClientID, WebUtils.CurrentDirection, _horizontalOffset, _directionFlip.ToString().ToLower(), openEvent));
			}
		}
	}
}



