﻿using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:FormTextbox runat=server></{0}:FormTextbox>")]
	public class FormTextBox : TextBox
	{
		private Orientation _layout = Orientation.Horizontal;
		private bool _requiredField = false;
		private string _title = null;

		public string Title
		{
			get { return _title; }
			set { _title = value; }
		}

		public Orientation Layout
		{
			get { return _layout; }
			set { _layout = value; }
		}

		public bool RequiredField
		{
			get { return _requiredField; }
			set { _requiredField = value; }
		}
		
		protected override void Render(HtmlTextWriter writer)
		{
			writer.Write("<table cellspacing='0' cellpadding='1'>");
			if (Layout == Orientation.Horizontal)
			{
				writer.Write("<tr>");
				writer.Write("<td>" + Title);
				if (RequiredField)
					writer.Write("<span style='color:maroon;'>&nbsp;*</span>");				
				writer.Write("</td>");
				writer.Write("<td>");
				base.Render(writer);
				writer.Write("</td>");
				writer.Write("</tr>");
			}
			else 
			{
				writer.Write("<tr>");
				writer.Write("<td>" + Title);
				if (RequiredField)
					writer.Write("<span style='color:maroon;'>&nbsp;*</span>");				
				writer.Write("</td>");
				writer.Write("</tr>");	
				writer.Write("<tr>");
				writer.Write("<td>");
				base.Render(writer);
				writer.Write("</td>");
				writer.Write("</tr>");			
			}
			writer.Write("</table>");
		}
	}
}


