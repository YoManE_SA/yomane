﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Text;

namespace Netpay.Web.Controls
{
    [System.Web.UI.ParseChildren(false), System.Web.UI.PersistChildren(false)]
    public class TabView : System.Web.UI.Control, System.Web.UI.INamingContainer
    {
		public object ItemData { get { return ViewState["ItemData"]; } set { ViewState["ItemData"] = value; } }
        public string Text { get { return (string) ViewState["Text"]; } set { ViewState["Text"] = value; } }
		public bool IsTemplate { get { return ((bool?)ViewState["IsTemplate"]).GetValueOrDefault(); } set { ViewState["IsTemplate"] = value; } }
	}

	[System.Web.UI.ParseChildren(true, "Tabs"), System.Web.UI.PersistChildren(false)]
    public class TabControl : System.Web.UI.Control, System.Web.UI.IPostBackDataHandler
    {
		public enum TabsStyle
		{
			NoJQuery = 0,
			JQuery = 1,
			OrganicTabs = 2,
		}
		public TabsStyle TabStyle { get; set; }
		public bool UseJQuery { get { return (TabStyle == TabControl.TabsStyle.JQuery); } set { TabStyle = value ? TabControl.TabsStyle.JQuery : TabControl.TabsStyle.NoJQuery; } }
        public string CssStyle { get; set; }
        public string Width { get; set; }
        public int ActiveViewIndex { get; set; }
        public TabControl() { ActiveViewIndex = -1;  }
		public System.Web.UI.ControlCollection Tabs { get { return Controls ; } }
		//public System.Web.UI.Control TabSpace { get; set; }
		public int TemplateStartIndex { get; set; }
		public int TemplateTabs { get { return ((int?) ViewState["TemplateTabs"]).GetValueOrDefault(); } set { ViewState["TemplateTabs"] = value; } }
		public System.Web.UI.ITemplate TabTemplate { get; set; }

		protected override void AddedControl(System.Web.UI.Control control, int index)
		{
			if (!(control is TabView)) throw new Exception("child controls must be of type tab");
			base.AddedControl(control, index);
		}

		protected override void RemovedControl(System.Web.UI.Control control)
		{
			if ((control is TabView) && (control as TabView).IsTemplate) TemplateTabs--;
			base.RemovedControl(control);
		}

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);
			if (Page.IsPostBack)
				for (int i = 0; i < TemplateTabs; i++)
					addTemplateTab(i);
		}

        protected override void OnLoad(EventArgs e)
        {
			//if (TabSpace != null) Controls.Add(TabSpace);
            base.OnLoad(e);
        }

		private TabView addTemplateTab(int index)
		{
			var tbView = new TabView() { IsTemplate = true };
			TabTemplate.InstantiateIn(tbView);
			Tabs.AddAt(TemplateStartIndex + index, tbView);
			return tbView;
		}

		public TabView AddTemplateTab(string title)
		{
			var tbView = addTemplateTab(TemplateTabs);
			tbView.Text = title;
			TemplateTabs++;
			return tbView;
		}

        protected override void OnPreRender(EventArgs e)
        {
            EnsureID();
			if (ActiveViewIndex >= Tabs.Count) ActiveViewIndex = Tabs.Count - 1;
			else if (ActiveViewIndex == -1 && Tabs.Count > 0) ActiveViewIndex = 0;
			if (TabStyle == TabsStyle.JQuery)
			{
                string tabScript = "$(\"#" + ClientID + "_Tabs\").tabs( { selected:" + ActiveViewIndex + ", select:function(event, ui) { document.getElementById('" + UniqueID + "').value = ui.index; } } );";
                Page.ClientScript.RegisterStartupScript(GetType(), "TabControl_JQuery", tabScript, true);
			} else if (TabStyle == TabsStyle.OrganicTabs) {
				Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.Tabs.organictabs.js");
				Page.ClientScript.RegisterStartupScript(GetType(), "TabControl_JQuery", "$(function() { $('#" + string.Format("{0}_Tabs", ClientID) + "').organicTabs({ 'speed': 200 }); });", true);
			} else {
                if (!Page.ClientScript.IsClientScriptBlockRegistered("TabControl"))
                {
                    string tabScript = "\r\n" +
                        "function TabControl_SelectTab(tbObject, dataRowIndex, stateInputId) {\r\n" +
                        "   var tableObject = tbObject.parentNode;\r\n" +
                        "   while (tableObject.nodeName != 'TABLE') tableObject = tableObject.parentNode;\r\n" +
                        "   var headRow = tbObject.parentNode;\r\n" +
                        "   for(var i = 0; i < headRow.childNodes.length; i++)" +
                        "       headRow.childNodes[i].className = (headRow.childNodes[i] == tbObject ? 'SelectedTab' : 'NonSelectedTab');\r\n" +
                        "   for(var i = 1; i < tableObject.rows.length; i++)" +
                        "       tableObject.rows[i].style.display = (i == dataRowIndex ? '' : 'none');\r\n" +
                        "   document.getElementById(stateInputId).value = dataRowIndex - 1;" +
                        "}\r\n";
                    Page.ClientScript.RegisterClientScriptBlock(GetType(), "TabControl", tabScript, true);
                }
            }
            base.OnPreRender(e);
        } 
        
        protected override void Render(System.Web.UI.HtmlTextWriter Response)
        {
            int tabIndex = 0;
            var tabs = new List<TabView>();
			foreach(var c in Controls) 
				if (c is TabView) tabs.Add(c as TabView);
			var activeView = ActiveViewIndex > -1 ? tabs[ActiveViewIndex] : null;
            Response.Write(string.Format("<input type=\"hidden\" id=\"{0}\" name=\"{0}\" value=\"{1}\" />", UniqueID, ActiveViewIndex));
			string tabsClientID = string.Format("{0}_Tabs", ClientID);
			if (TabStyle == TabsStyle.JQuery)
			{
				Response.Write(string.Format("<div id=\"{0}\"><ul>", tabsClientID));
				foreach (TabView item in tabs)
					Response.Write(string.Format("<li><a href=\"#{1}-{2}\">{0}</a></li>", item.Text, tabsClientID, ++tabIndex));
				//if (TabSpace != null) { Response.Write("<li style=\"float:right;\">"); TabSpace.RenderControl(Response); Response.Write("</li>"); }
				Response.Write("</ul>");
				tabIndex = 0;
				foreach (TabView item in tabs)
				{
					Response.Write(string.Format("<div id=\"{0}-{1}\">", tabsClientID, ++tabIndex));
					item.RenderControl(Response);
					Response.Write("</div>");
				}
				Response.Write("</div>");
			} else if (TabStyle == TabsStyle.OrganicTabs) {
				Response.Write(string.Format("<div id=\"{0}\" class=\"{1}\"><ul class=\"nav\">", tabsClientID, CssStyle));
				foreach (TabView item in tabs)
					Response.Write(string.Format("<li><a href=\"#{1}-{2}\" class=\"{4}\" onclick=\"document.getElementById('{3}').value={2}-1;\">{0}</a></li>", item.Text, tabsClientID, ++tabIndex, UniqueID, (item == activeView ? "current" : "")));
				Response.Write("</ul>");
				tabIndex = 0;
				Response.Write(" <div class=\"list-wrap\">");
				foreach (TabView item in tabs)
				{
					Response.Write(string.Format("<ul id=\"{0}-{1}\" class=\"{2}\"><li><div class=\"section-tabs\">", tabsClientID, ++tabIndex, (item == activeView ? "" : "hide")));
					item.RenderControl(Response);
					Response.Write("</div></li></ul>");
				}
				Response.Write(" </div>");
				Response.Write("</div>");
            } else {
                Response.Write(string.Format("<table class=\"{0}\" width=\"{1}\" id=\"{2}\" cellspacing=\"0\" cellpadding=\"0\">", CssStyle, Width, ClientID));
                Response.Write(string.Format("<tr class=\"TabHeadRow\" id=\"{0}_Header\"><td nowrap>", ClientID));
                foreach (TabView item in tabs)
                    Response.Write(string.Format("<span onclick=\"TabControl_SelectTab(this, {2}, '{1}')\" class=\"{3}\">{0}</span>", item.Text, UniqueID, ++tabIndex, (item == activeView ? "SelectedTab" : "NonSelectedTab")));
                Response.Write("</td>");
                Response.Write("<td class=\"TabHeadSpace\" width=\"100%\">&nbsp;");
                //if (TabSpace != null) TabSpace.RenderControl(Response);
                Response.Write("</td></tr>");
                foreach (TabView item in tabs)
                {
                    Response.Write(string.Format("<tr class=\"TabItemRow\" style=\"display:{0};\"><td class=\"TabItemCell\" colspan=\"2\">", (item == activeView ? "" : "none")));
                    item.RenderControl(Response);
                    Response.Write("</td></tr>");
                }
                Response.Write("</table>");
            }
        }

        bool System.Web.UI.IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
        {
            int _postbackViewIndex = -1;
            if (int.TryParse(postCollection[postDataKey], out _postbackViewIndex))
            {
                ActiveViewIndex = _postbackViewIndex;
                return true;
            }
            return false;
        }

        void System.Web.UI.IPostBackDataHandler.RaisePostDataChangedEvent() { }
    }
}
