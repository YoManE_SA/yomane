﻿using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:Legend runat=server></{0}:Legend>")]
	public class Legend : WebControl
	{
		private bool _decline = false;
		private bool _capture = false;
		private bool _clarification = false;
        private bool _pendingChargeback = false;
		private bool _chargeback = false;
		private bool _authorize = false;
		private bool _authorizeDecline = false;
		private bool _admin = false;
		private bool _system = false;
		private bool _bank = false;
		private bool _test = false;
		private bool _waiting = false;
		private bool _passed = false;
		private bool _blocked = false;
		private bool _canceled = false;
		private bool _active = false;
		private bool _suspended = false;
		private bool _ended = false;
		private bool _pending = false;
		private bool _merchant = false;
		private bool _precreated = false;
		private bool _approved = false;
        private bool _notApproved = false;

        public bool PendingChargeback
        {
            get { return _pendingChargeback; }
            set { _pendingChargeback = value; }
        }

		public bool NotApproved
		{
			get { return _notApproved; }
			set { _notApproved = value; }
		}

		public bool Approved
		{
			get { return _approved; }
			set { _approved = value; }
		}

		public bool Precreated
		{
			get { return _precreated; }
			set { _precreated = value; }
		}
		public bool Merchant
		{
			get { return _merchant; }
			set { _merchant = value; }
		}

		public bool Pending
		{
			get { return _pending; }
			set { _pending = value; }
		}

		public bool Active
		{
			get { return _active; }
			set { _active = value; }
		}

		public bool Ended
		{
			get { return _ended; }
			set { _ended = value; }
		}

		public bool Suspended
		{
			get { return _suspended; }
			set { _suspended = value; }
		}

		public bool Waiting
		{
			get { return _waiting; }
			set { _waiting = value; }
		}

		public bool Passed
		{
			get { return _passed; }
			set { _passed = value; }
		}

		public bool Blocked
		{
			get { return _blocked; }
			set { _blocked = value; }
		}

		public bool Canceled
		{
			get { return _canceled; }
			set { _canceled = value; }
		}

		public bool Authorize
		{
			get { return _authorize; }
			set { _authorize = value; }
		}

		public bool AuthorizeDecline
		{
			get { return _authorizeDecline; }
			set { _authorizeDecline = value; }
		}

		public bool Decline
		{
			get { return _decline; }
			set { _decline = value; }
		}

		public bool Capture
		{
			set { _capture = value; }
		}
		
		public bool Chargeback
		{
			set { _chargeback = value; }
		}

		public bool Clarification
		{
			set { _clarification = value; }
		}

		public bool Admin
		{
			set { _admin = value; }
		}

		public bool System
		{
			set { _system = value; }
		}

		public bool Bank
		{
			get { return _bank; }
			set { _bank = value; }
		}

		public bool Test
		{
			set { _test = value; }
		}

		private void WriteTr(HtmlTextWriter writer, string boxBackgroundColor, string boxStyle, string boxText, string titleBackgroundColor, string title)
		{
			writer.Write("<tr>");
			writer.Write(string.Format("<td bgcolor='{0}' style='{1}'>{2}</td>", boxBackgroundColor, boxStyle, boxText));
			writer.Write(string.Format("<td nowrap='nowrap' bgcolor='{0}'>&nbsp;{1}</td>", titleBackgroundColor, title));
			writer.Write("</tr>");
			writer.Write("<tr><td style='height:2px;'></td></tr>");
		}

		protected override void Render(HtmlTextWriter writer)
		{
			ResourceManager rm = WebUtils.GetResource("Legend");
			writer.Write("<table border='0' cellspacing='1' cellpadding='0' style='width:100%; direction:inherit; font-size: 10px;'>");
			if (_decline)
				WriteTr(writer, "#ff6666", "padding: 0px 2px 0px 2px; height: 18px;", "&nbsp;", "#ffffff", rm.GetString("decline"));
			if (_approved)
                WriteTr(writer, "#66cc66", "padding: 0px 2px 0px 2px; height: 18px;", "&nbsp;", "#ffffff", rm.GetString("approved"));
			if (_notApproved)
                WriteTr(writer, "#ff6666", "padding: 0px 2px 0px 2px; height: 18px;", "&nbsp;", "#ffffff", rm.GetString("notApproved"));
			if (_capture)
                WriteTr(writer, "#66cc66", "padding: 0px 2px 0px 2px; height: 18px;", "&nbsp;", "#edfeed", rm.GetString("capture"));
			if (_chargeback)
                WriteTr(writer, "#66cc66", "padding: 0px 2px 0px 2px; height: 18px;", "&nbsp;", "#ffebe1", rm.GetString("chargeback"));
			if (_clarification)
                WriteTr(writer, "#66cc66", "padding: 0px 2px 0px 2px; height: 18px;", "&nbsp;", "#fff3dd", rm.GetString("clarification"));
 			if (_pendingChargeback)
                WriteTr(writer, "#66cc66", "padding: 0px 2px 0px 2px; height: 18px;", "NR", "#fff3dd", rm.GetString("pendingChargeback"));           
            if (_authorize)
                WriteTr(writer, "#ffffff", "padding: 0px 2px 0px 2px; height: 18px; border:1px solid #66cc66;", "&nbsp;", "#ffffff", rm.GetString("authorize"));
			if (_authorizeDecline)
                WriteTr(writer, "#ffffff", "padding: 0px 2px 0px 2px; height: 18px; border:1px solid #ff6666;", "&nbsp;", "#ffffff", rm.GetString("authorizeDecline"));
			if (_admin)
                WriteTr(writer, "#003399", "padding: 0px 2px 0px 2px; height: 18px;", "&nbsp;", "#eeeffd", rm.GetString("admin"));
			if (_merchant)
                WriteTr(writer, "#6699CC", "padding: 0px 2px 0px 2px; height: 18px;", "&nbsp;", "#eeeffd", rm.GetString("merchant"));
			if (_system)
                WriteTr(writer, "#484848", "padding: 0px 2px 0px 2px; height: 18px;", "&nbsp;", "#f5f5f5", rm.GetString("system"));
			if (_bank)
                WriteTr(writer, "#804000", "padding: 0px 2px 0px 2px; width: 10px; height: 18px;", "&nbsp;", "#f5f5f5", rm.GetString("bankFee"));
			if (_test)
                WriteTr(writer, "#ffffff", "border: 1px dotted #FE0E36; padding: 0px; height: 18px; text-align:center;", "&nbsp;", "#ffffff", rm.GetString("test"));
			if (_waiting)
                WriteTr(writer, "#FFC62F", "padding: 0px 2px 0px 2px; height: 18px; text-align:center;", "&nbsp;", "#ffffff", rm.GetString("waiting"));
			if (_passed)
                WriteTr(writer, "#66CC66", "padding: 0px 2px 0px 2px; height: 18px; text-align:center;", "&nbsp;", "#ffffff", rm.GetString("passed"));
			if (_blocked)
                WriteTr(writer, "#FF6666", "padding: 0px 2px 0px 2px; height: 18px; text-align:center;", "&nbsp;", "#ffffff", rm.GetString("blocked"));
			if (_canceled)
                WriteTr(writer, "#808080", "padding: 0px 2px 0px 2px; height: 18px; text-align:center;", "&nbsp;", "#ffffff", rm.GetString("canceled"));
			if (_active)
                WriteTr(writer, "#FFC62F", "padding: 0px 2px 0px 2px; height: 18px; text-align:center;", "&nbsp;", "#ffffff", rm.GetString("active"));
			if (_precreated)
                WriteTr(writer, "#ffffff", "padding: 0px 2px 0px 2px; height: 18px; border:1px solid #FFC62F;", "&nbsp;", "#ffffff", rm.GetString("precreated"));
			if (_ended)
                WriteTr(writer, "#66CC66", "padding: 0px 2px 0px 2px; height: 18px; text-align:center;", "&nbsp;", "#ffffff", rm.GetString("ended"));
			if (_suspended)
                WriteTr(writer, "#808080", "padding: 0px 2px 0px 2px; height: 18px; text-align:center;", "&nbsp;", "#ffffff", rm.GetString("suspended"));
			if (_pending)
                WriteTr(writer, "#808080", "padding: 0px 2px 0px 2px; height: 18px; text-align:center;", "&nbsp;", "#ffffff", rm.GetString("pending"));
			writer.Write("</table>");
		}
	}
}
