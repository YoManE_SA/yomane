﻿using System;
using System.Web.UI;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:MerchantStatusDropDown runat=server></{0}:MerchantStatusDropDown>")]
	public class MerchantStatusDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			DataSource = GlobalData.GetGroup(GlobalDataGroup.MerchantStatus, Language.English);
			DataTextField = "Value";
			DataValueField = "ID";
			base.DataBind(raiseOnDataBinding);
		}
	}
}
