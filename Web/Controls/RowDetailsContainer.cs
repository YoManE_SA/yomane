﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
	public interface ILoadControlParameters { void LoadControlParameters(System.Collections.Specialized.NameValueCollection parameters); }
	public class RowDetailsContainer : System.Web.UI.WebControls.WebControl, System.Web.UI.ICallbackEventHandler
	{
		public static string FramePagePath = "~/AdminCash/ContentFrame.aspx";
		public string ControlPath { get; set; }
		public string RowControlID { get; set; }
		public event EventHandler DetailsUpdated;
		
		public RowDetailsContainer() : base(System.Web.UI.HtmlTextWriterTag.Iframe) 
		{
			Width = new System.Web.UI.WebControls.Unit(100, System.Web.UI.WebControls.UnitType.Percentage);
			Style.Add("display", "none");
			Style.Add("border", "none");
			Style.Add("margin", "0px");
			Attributes["onload"] = "this.height = (this.contentWindow.document.body.scrollHeight + 5) + 'px'";
		}

		public string GetShowScript(string sender, string parameters, string controlName, string notify)
		{
			if (controlName == null) controlName = ControlPath;
			string destPath = controlName.EndsWith(".ascx") ? (ResolveUrl(FramePagePath) + "?control=" + System.Web.HttpUtility.UrlEncode(ResolveUrl(controlName)) + "&") : (ResolveUrl(controlName) + "?");
			if (!string.IsNullOrEmpty(RowControlID)) destPath += "UpdateID=" + System.Web.HttpUtility.UrlEncode(ClientID) + "&";
			destPath += "IsRowDetails=1&";
			destPath += parameters;
			return string.Format("RowDetailsContainer_Show('{0}', {1}, '{2}', {3})", ClientID, sender, destPath, (string.IsNullOrEmpty(notify) ? "null" : notify));
		}
		
		public string GetShowScript(string sender, string parameters)
		{
			return GetShowScript(sender, parameters, null, null);
		}
		public string GetShowScript(string sender)
		{
			return GetShowScript(sender, null);
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "RowDetailsContainer"))
			{
				var script = 
					"function RowDetailsContainer_Show(frmId, sender, targetUrl, notifyProc, bShow){" +
					"	var ifrObj = document.getElementById(frmId);" +
					"	if (bShow == null) bShow = (ifrObj.style.display == 'none');" +
					"	if (bShow) {" +
					"		ifrObj.src = targetUrl;" +
					"		ifrObj.style.display = '';" +
					"		if (notifyProc) notifyProc(ifrObj, sender, 'open');" +
					"	} else {" +
					"		ifrObj.style.display = 'none';" +
					"		if (notifyProc) notifyProc(ifrObj, sender, 'close');" +
					"	}" +
					"}\r\n";
				Page.ClientScript.RegisterClientScriptBlock(GetType(), "RowDetailsContainer", script, true);
			}
			if (RowControlID != null) RegisterUpdateScript(NamingContainer.FindControl(RowControlID), null);
			base.OnPreRender(e);
		}

		public void RegisterUpdateScript(System.Web.UI.Control updateControl, string notify)
		{
			string script = 
				"function DetailsUpdated_" + ClientID + "(commandName) { " + 
				"	var notifyProc = " + (string.IsNullOrEmpty(notify) ? "null" : notify) + "; if (notifyProc) notifyProc(commandName); ";
			if (updateControl != null) script += Page.ClientScript.GetCallbackEventReference(this, "'" + updateControl.UniqueID + "'", "function(arg, context){ $(context).replaceWith(arg); }", "document.getElementById('" + updateControl.ClientID + "')");
			script += "; }\r\n";
			Page.ClientScript.RegisterClientScriptBlock(GetType(), ClientID, script, true);
		}

		public string GetCallbackResult()
		{
			var txr = new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter());
			//var control = NamingContainer.FindControl(RowControlID);
			var control = Page.FindControl(RowControlID);
			control.RenderControl(txr);
			return txr.InnerWriter.ToString();
		}

		public void RaiseCallbackEvent(string eventArgument)
		{
			if (!string.IsNullOrEmpty(eventArgument)) RowControlID = eventArgument;
			if (DetailsUpdated != null) DetailsUpdated(this, EventArgs.Empty);
		}
	}

	public class RowDetailsButton : System.Web.UI.WebControls.WebControl
	{
		public string OpenImage { get; set; }
		public string CloseImage { get; set; }
		public string OpenText { get; set; }
		public string CloseText { get; set; }
		public string RowDetailsContainerID { get; set; }
		public string ControlPath { get; set; }
		public string Parameters { get; set; }

		public RowDetailsButton()
		{
			Style.Add("cursor", "pointer");
			OpenText = "+"; CloseText = "-";
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			AddAttributesToRender(writer);
			var rdc = NamingContainer.FindControl(RowDetailsContainerID) as RowDetailsContainer;
			if (!string.IsNullOrEmpty(OpenImage)){
				if (rdc != null)
					writer.AddAttribute("onclick", rdc.GetShowScript("this", Parameters, ControlPath, "function(s, t, v) { t.src = (v == 'open' ? '" + ResolveUrl(string.IsNullOrEmpty(CloseImage) ? OpenImage : CloseImage) + "' : '" + ResolveUrl(OpenImage) + "'); } "));
				writer.AddAttribute("src", ResolveUrl(OpenImage));
				writer.RenderBeginTag(System.Web.UI.HtmlTextWriterTag.Img);
				writer.RenderEndTag();
			}else{
				if (rdc != null)
					writer.AddAttribute("onclick", rdc.GetShowScript("this", Parameters, ControlPath, "function(s, t, v) { t.innerHTML = (v == 'open' ? '" + CloseText + "' : '" + OpenText + "'); } "));
				AddAttributesToRender(writer);
				writer.RenderBeginTag(System.Web.UI.HtmlTextWriterTag.Span);
				writer.Write(OpenText);
				writer.RenderEndTag();
			}
		}
	}

	public class PopupContainer : System.Web.UI.WebControls.CompositeControl
	{
		public RowDetailsContainer Container { get { return FindControl("PopupContainer") as RowDetailsContainer; } }
		public System.Web.UI.WebControls.WebControl TitleContainer { get { return FindControl("TitleContainer") as System.Web.UI.WebControls.WebControl; } }
		public System.Web.UI.WebControls.Label Title { get { return TitleContainer.FindControl("Title") as System.Web.UI.WebControls.Label; } }
		public System.Web.UI.WebControls.Label CloseButton { get { return TitleContainer.FindControl("CloseButton") as System.Web.UI.WebControls.Label; } }

		protected override void OnInit(EventArgs e)
		{
			if (Page.Items["PopupContainer"] != null)
				throw new Exception("more then on PopupContainer controls on page, only one allowed.");
			Page.Items["PopupContainer"] = this;
			base.OnInit(e);
		}

		public static PopupContainer GetCurrent(System.Web.UI.Page page)
		{
			return page.Items["PopupContainer"] as PopupContainer;
		}

		protected override System.Web.UI.HtmlTextWriterTag TagKey { get { return System.Web.UI.HtmlTextWriterTag.Div; } }
		protected override void CreateChildControls()
		{
			Controls.Add(new System.Web.UI.WebControls.WebControl(System.Web.UI.HtmlTextWriterTag.Div) { ID = "TitleContainer" });
			TitleContainer.Controls.Add(new System.Web.UI.WebControls.Label() { ID = "Title" });
			TitleContainer.Controls.Add(new System.Web.UI.WebControls.Label() { ID = "CloseButton", Text = "X" });
			Controls.Add(new RowDetailsContainer() { ID = "PopupContainer", BackColor = System.Drawing.Color.White, Width = new System.Web.UI.WebControls.Unit(100, System.Web.UI.WebControls.UnitType.Percentage) });
			TitleContainer.Style.Add("padding-bottom", "5px");
			Title.Style.Add("font-size", "14px"); Title.Style.Add("font-weight", "bold"); 
			CloseButton.Style.Add("float", "right"); CloseButton.Style.Add("cursor", "pointer"); CloseButton.Attributes["onclick"] = "$('#" + ClientID + "').hide('fade')";
			base.CreateChildControls();
		}

		protected override void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
		{
			writer.AddStyleAttribute("position", "absolute");
			writer.AddStyleAttribute("left", "50%");
			writer.AddStyleAttribute("top", "50%");
			writer.AddStyleAttribute("transform", "translate(-50%, -50%)");
			writer.AddStyleAttribute("background-color", "#d0d0d0");
			writer.AddStyleAttribute("padding", "5px");
			writer.AddStyleAttribute("display", "none");

			base.AddAttributesToRender(writer);
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "PopupContainer"))
			{
				var script =
					"function PopupContainer_ShowPopup(title, width, destPath) {" + 
					"	$('#" + ClientID + "').width(width);" +
					"	$('#" + Title.ClientID + "').html(title);" +
					"	RowDetailsContainer_Show('" + Container.ClientID + "', document.getElementById('" + ClientID + "'), destPath + '&UpdateID=" + System.Web.HttpUtility.UrlEncode(Container.ClientID) + "', function(s, t, v) { if (v == 'open') $(t).show('fade'); else $(t).hide('fade'); }, true);" + 
					"}\r\n";
				Page.ClientScript.RegisterClientScriptBlock(GetType(), "PopupContainer", script, true);
				Container.RegisterUpdateScript(null, "function(commandName) { if (commandName == 'Save') $('#" + ClientID + "').hide('fade'); }");
			}
			base.OnPreRender(e);
		}

		public static string Popup(System.Web.UI.Control sender, string title, int width, string controlName, string parameters)
		{
			string destPath = controlName.EndsWith(".ascx") ? (sender.ResolveUrl(RowDetailsContainer.FramePagePath) + "?control=" + System.Web.HttpUtility.UrlEncode(sender.ResolveUrl(controlName)) + "&") : (sender.ResolveUrl(controlName) + "?");
			destPath += parameters;
			return "var topContainer; for(var wnd = window; wnd != null; wnd = wnd.parent) { if (wnd.PopupContainer_ShowPopup) topContainer = wnd; if (wnd == wnd.parent) break;} topContainer.PopupContainer_ShowPopup('" + title + "', " + width + ", '" + destPath + "');";
		}
	}
}
