﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:YearDropDown runat=server></{0}:YearDropDown>")]
	public class YearDropDown : DropDownBase
    {
        private bool _viewStateUsed = false;
        public int YearFrom { get; set; /*get { return ViewState["YearFrom"].ToNullableInt().GetValueOrDefault(0); } set { ViewState["YearFrom"] = value;  } */}
        public int YearTo { get; set; /*get { return ViewState["YearTo"].ToNullableInt().GetValueOrDefault(0); } set { ViewState["YearTo"] = value; } */}
		public bool DefaultCurrentYear { get; set; }
		
		protected override void OnInit(EventArgs e) 
		{
			if (!Page.IsPostBack && DefaultCurrentYear) Value = DateTime.Now.Year.ToString();
			base.OnInit(e);
		}

        protected override void OnDataBinding(EventArgs e)
        {
            _requiresDataBinding = true;
            base.OnDataBinding(e);
            if (YearFrom == 0) YearFrom = DateTime.Now.AddYears(-5).Year;
            if (YearTo == 0) YearTo = DateTime.Now.Year;
        }

		protected override void DataBind(bool raiseOnDataBinding)
		{
            if (raiseOnDataBinding) { ClearBoundedItems(); SelectedIndex = -1; SelectedValue = null; ClearSelection(); OnDataBinding(EventArgs.Empty); raiseOnDataBinding = false; }
            if (!_viewStateUsed && (YearFrom == 0 && YearTo == 0))
            {
                if (Page.Request.Form[UniqueID + "_From"] != null) YearFrom = Page.Request.Form[UniqueID + "_From"].ToNullableInt().GetValueOrDefault(DateTime.Now.Year);
                if (Page.Request.Form[UniqueID + "_To"] != null) YearTo = Page.Request.Form[UniqueID + "_To"].ToNullableInt().GetValueOrDefault(DateTime.Now.Year);
            }
            if (YearFrom == 0) YearFrom = DateTime.Now.AddYears(-5).Year;
            if (YearTo == 0) YearTo = DateTime.Now.Year;

            var ret = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, string>>();
			for (int currentYear = YearFrom; currentYear <= YearTo; currentYear++)
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(currentYear, currentYear.ToString()));
			
			DataSource = ret;
			DataValueField = "Key";
			DataTextField = "Value";
			base.DataBind(raiseOnDataBinding);
            if (!_viewStateUsed)
            {
                System.Web.UI.ScriptManager.RegisterHiddenField(this, UniqueID + "_From", YearFrom.ToString());
                System.Web.UI.ScriptManager.RegisterHiddenField(this, UniqueID + "_To", YearTo.ToString());
            }
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            if (savedState != null) {
                _viewStateUsed = true;
                YearFrom = ViewState["YearFrom"].ToNullableInt().GetValueOrDefault(DateTime.Now.Year);
                YearTo = ViewState["YearTo"].ToNullableInt().GetValueOrDefault(DateTime.Now.Year);
            }
        }

        protected override object SaveViewState()
        {
            ViewState["YearFrom"] = YearFrom;
            ViewState["YearTo"] = YearTo;
            _viewStateUsed = true;
            return base.SaveViewState();
        }

		public int? SelectedYear
		{
			get {
				if (string.IsNullOrEmpty(Value)) return null;
                return int.Parse(Value.Trim());
			}
			set {
				if (value == null) {
					Value = null; ClearSelection(); SelectedValue = ""; 
				} else{ 
					Value = value.ToString();
				}
			}
		}

		public string SelectedYearLastDigits
		{
			get
			{
				var selYear = SelectedYear;
				if (selYear == null) return "";
				return selYear.ToString().Substring(2, 2);
			}
		}
	}
}
