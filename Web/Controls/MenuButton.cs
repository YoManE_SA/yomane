﻿using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:DisabledButton runat=server></{0}:DisabledButton>")]
	public class MenuButton : HtmlAnchor
	{
		protected override void Render(HtmlTextWriter writer)
		{
			if (Disabled)
			{
				HRef = "javascript://";
				Attributes.Add("style", "color: #c5c5c5; cursor: default; text-decoration: none;");
				Attributes.Remove("disabled");
			}
			else
			{
			
			}

			base.Render(writer);
		}
	}
}
