﻿if (netpay == null) { var netpay = new Object() }
if (netpay.webControls == null) { netpay.webControls = new Object() }

netpay.webControls.DateRangePicker =
{
	init: function (controlName)
	{
		var minDate = new Date($("#dateRangePickerMinDate").val());
		var maxDate = new Date($("#dateRangePickerMaxDate").val());
		$("#dateRangePickerFrom").datepicker({ minDate: minDate, maxDate: maxDate, dateFormat: clientContext.currentDateFormat });
		$("#dateRangePickerTo").datepicker({ minDate: minDate, maxDate: maxDate, dateFormat: clientContext.currentDateFormat });
	},

	validate: function ()
	{
		var minDate = new Date($("#dateRangePickerMinDate").val());
		var maxDate = new Date($("#dateRangePickerMaxDate").val());
		var fromDate = $("#dateRangePickerFrom").datepicker("getDate");
		var toDate = $("#dateRangePickerTo").datepicker("getDate");
		var maxOffset = $("#dateRangePickerMaxRange").val();

		if (fromDate < minDate || toDate > maxDate)
		{
			netpay.Common.alert("No data exists for the selected date range.");
			return false;
		}

		if (maxOffset != undefined)
		{
			if (toDate - fromDate > maxOffset)
			{
				netpay.Common.alert($("#dateRangePickerOutOfRangeError").val());
				return false;
			}
		}

		return true;
	}
}

$(netpay.webControls.DateRangePicker.init);