﻿using System;
using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:DateRangePicker runat=server></{0}:DateRangePicker>")]
	public class DateRangePicker : WebControl
	{
		private DateTime _minDate = new DateTime(1900, 1, 1);
		private DateTime _maxDate = new DateTime(2100, 1, 1);
		private DateTime? _fromDate = null;
		private DateTime? _toDate = null;
		private Orientation _layout = Orientation.Vertical;
		private RepeatLayout _htmlLayout = RepeatLayout.Table;
		private bool _isToDateVisible = true;
		private TimeSpan? _maxRange = null;
		private string _outOfRangeError = null;

		public CultureInfo Culture 
		{
			get 
			{
				return new CultureInfo("en-gb");
				
				// disable dynamic formatting, see ScriptManager line 27
				//return WebUtils.CurrentCulture;
			}
		}

		public string OutOfRangeError
		{
			get { return _outOfRangeError; }
			set { _outOfRangeError = value; }
		}

		public TimeSpan? MaxRange
		{
			get { return _maxRange; }
			set { _maxRange = value; }
		}

		public bool IsToDateVisible
		{
			get { return _isToDateVisible; }
			set { _isToDateVisible = value; }
		}

		public Orientation Layout
		{
			get { return _layout; }
			set { _layout = value; }
		}

		public RepeatLayout HtmlLayout
		{
			get { return _htmlLayout; }
			set { _htmlLayout = value; }
		}

		public DateTime MinDate
		{
			get 
			{
				string posted = Context.Request["dateRangePickerMinDate"];
				if (posted != null)
					DateTime.TryParse(posted, out _minDate);
					
				return _minDate; 
			}
			set { _minDate = value; }
		}

		public DateTime MaxDate
		{
			get
			{
				string posted = Context.Request["dateRangePickerMaxDate"];
				if (posted != null)
					DateTime.TryParse(posted, out _maxDate);

				return _maxDate;
			}
			set { _maxDate = value; }
		}

		public DateTime? FromDate
		{
			get
			{
				DateTime parsed;
				if (DateTime.TryParse(Context.Request["dateRangePickerFrom"], Culture, DateTimeStyles.None, out parsed))
					_fromDate = parsed;

				if (_fromDate < _minDate)
					_fromDate = _minDate;
				if (_fromDate > _maxDate)
					_fromDate = _maxDate;

				return _fromDate;
			}
			set { _fromDate = value; }
		}

		public DateTime? ToDate
		{
			get
			{
				DateTime parsed;
				if (DateTime.TryParse(Context.Request["dateRangePickerTo"], Culture, DateTimeStyles.None, out parsed))
					_toDate = parsed;

				if (_toDate < _minDate)
					_toDate = _minDate;
				if (_toDate > _maxDate)
					_toDate = _maxDate;

				return _toDate;
			}
			set { _toDate = value; }
		}

		public bool IsFromDateSelected
		{
			get
			{
				return Context.Request["dateRangePickerFrom"] != null && Context.Request["dateRangePickerFrom"].Trim() != "";
			}
		}

		public bool IsToDateSelected
		{
			get
			{
				return Context.Request["dateRangePickerTo"] != null && Context.Request["dateRangePickerTo"].Trim() != "";
			}
		}

		public bool IsOutOfRange
		{
			get
			{
				if (FromDate == null || ToDate == null || MaxRange == null)
					return true;

				return ToDate - FromDate > MaxRange;
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnPreRender(e);
			
			// defaults
			if (!Page.IsPostBack)
			{
				if (_fromDate == null)
					_fromDate = DateTime.Now.AddDays(-7);
				if (_toDate == null)
					_toDate = DateTime.Now;
			}

			// embed scripts
			this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.DateRangePicker.DateRangePicker.js");
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			ResourceManager rm = WebUtils.GetResource("Common");
			string fromDate = null;
			if (FromDate != null)
				fromDate = FromDate.Value.ToString(Culture.DateTimeFormat.ShortDatePattern);
			else
				fromDate = "";
			
			string toDate = null;
			if (ToDate != null)
				toDate = ToDate.Value.ToString(Culture.DateTimeFormat.ShortDatePattern);
			else
				toDate = "";

			if (HtmlLayout == RepeatLayout.Table) {
				if (Layout == Orientation.Vertical)
				{
					writer.Write("<table>");
					writer.Write("<tr>");
					writer.Write("<td>");
					writer.Write(rm.GetString("From"));
					writer.Write("<br/>");
					writer.Write(string.Format("<input id=\"dateRangePickerFrom\" name=\"dateRangePickerFrom\" value=\"{0}\" size=\"10\" type=\"text\" />", fromDate));
					writer.Write("</td>");
					writer.Write("</tr>");
					if (IsToDateVisible)
					{
						writer.Write("<tr>");
						writer.Write("<td>");
						writer.Write(rm.GetString("To"));
						writer.Write("<br/>");
                        writer.Write(string.Format("<input id=\"dateRangePickerTo\" class=\"form-control\" name=\"dateRangePickerTo\" value=\"{0}\" size=\"10\" type=\"text\" />", toDate));
						writer.Write("</td>");
						writer.Write("</tr>"); 
					}
					writer.Write("</table>"); 
				}
				else if (Layout == Orientation.Horizontal)
				{
					writer.Write("<table cellpadding=\"0\" cellspacing=\"0\">");
					writer.Write("<tr>");
					writer.Write("<td>");
                    writer.Write(string.Format("<input id=\"dateRangePickerFrom\" name=\"dateRangePickerFrom\" class=\"form-control\" value=\"{0}\" size=\"10\" type=\"text\" />", fromDate));
					writer.Write("</td>");
					if (IsToDateVisible)
					{
						writer.Write("<td style=\"padding-left:5px; padding-right:5px; \">");
						writer.Write(rm.GetString("To"));
						writer.Write("</td>");
						writer.Write("<td>");
                        writer.Write(string.Format("<input id=\"dateRangePickerTo\" class=\"form-control\"  name=\"dateRangePickerTo\" value=\"{0}\" size=\"10\" type=\"text\" />", toDate));
						writer.Write("</td>"); 
					}
					writer.Write("</tr>");
					writer.Write("</table>"); 			
				}
			} else if (HtmlLayout == RepeatLayout.Flow) {
				writer.Write(string.Format("<input id=\"dateRangePickerFrom\" class=\"Field_100 form-control\" name=\"dateRangePickerFrom\" value=\"{0}\" size=\"10\" type=\"text\" />", fromDate));
				if (IsToDateVisible)
				{
					writer.Write("<span class=\"word\">" + rm.GetString("To") + "</span>");
                    writer.Write(string.Format("<input id=\"dateRangePickerTo\" class=\"Field_100 form-control\" name=\"dateRangePickerTo\" value=\"{0}\" size=\"10\" type=\"text\" />", toDate));
				}
			} else throw new Exception("The selected HtmlLayout is not supported");

			writer.Write(string.Format("<input type=\"hidden\" id=\"dateRangePickerMinDate\" name=\"dateRangePickerMinDate\" value=\"{0}\">", MinDate.ToString("yyyy/MM/dd")));
			writer.Write(string.Format("<input type=\"hidden\" id=\"dateRangePickerMaxDate\" name=\"dateRangePickerMaxDate\" value=\"{0}\">", MaxDate.ToString("yyyy/MM/dd")));
			if (MaxRange != null)
			{
				writer.Write(string.Format("<input type=\"hidden\" id=\"dateRangePickerMaxRange\" name=\"dateRangePickerMaxRange\" value=\"{0}\">", MaxRange.Value.TotalMilliseconds.ToString()));
				writer.Write(string.Format("<input type=\"hidden\" id=\"dateRangePickerOutOfRangeError\" name=\"dateRangePickerOutOfRangeError\" value=\"{0}\">", _outOfRangeError));	
			}
		}
	}
}
