﻿using System;
using System.Resources;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Bll;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:Pager runat=server></{0}:Pager>")]
	public class Pager : WebControl, IPostBackEventHandler
	{
        public bool AutoHide { get; set; }
        public event EventHandler PageChanged;
		protected int _maxButtons = 11;
		protected SortAndPage _info;

        private int GetCurrentPage()
		{
			int pageNumber = 0;
            if (!string.IsNullOrEmpty(Context.Request["__EVENTARGUMENT"]))
                pageNumber = Context.Request["__EVENTARGUMENT"].ToNullableInt().GetValueOrDefault();
			if (pageNumber < 1) pageNumber = 0;
			if (Page != null && (Page.IsCrossPagePostBack ||(!Page.IsPostBack))) pageNumber = 0;
			return pageNumber;
		}

		public SortAndPage Info
		{
			get { if (_info == null) _info = new SortAndPage(GetCurrentPage(), 20, null, false); return _info; }
			set { _info = value; }
		}

		public int PageSize { get { return Info.PageSize; } set { Info.PageSize = value; } }
		public int CurrentPage { get {  return Info.PageCurrent; } set { Info.PageCurrent = value; } }
		
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
            if (AutoHide) Visible = (_info != null);
            // embedding the css 
            string template = "<link rel='stylesheet' text='text/css' href='{0}' />";
			string location = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.Pager.Pager.css");
			LiteralControl literal = new LiteralControl(String.Format(template, location));
			Page.Header.Controls.Add(literal);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			ResourceManager rm = WebUtils.GetResource("Pager");
			if (_info == null)
			{
				writer.Write("<table cellspacing='2'><tr><td>Paging information was not provided.</td></tr></table>");
				return;
			} 
			
			if (_info.RowCount == 0)
			{
				writer.Write("<table cellspacing='2'><tr><td>" + rm.GetString("NoResults") + "</td></tr></table>");
				return;
			}

			if (_info.PageCount == 0)
			{
				return;
			}

			PlaceHolder firstPage = new PlaceHolder();
			PlaceHolder previousPage = new PlaceHolder();
			PlaceHolder pages = new PlaceHolder();
			PlaceHolder lastPage = new PlaceHolder();
			PlaceHolder nextPage = new PlaceHolder();
			Literal showingRecords = new Literal();
			showingRecords.Text = "<span>" + rm.GetString("ShowingRecords") + " " + _info.RowFrom.ToString("#,#") + " - " + _info.RowTo.ToString("#,#") + " " + rm.GetString("Of") + " " + _info.RowCount.ToString("#,#") + "</span>"; 
			
			int median = Convert.ToInt32(System.Math.Floor((double)(_maxButtons / 2)));
			int minButton = _info.PageCurrent - median;
			int maxButton = _info.PageCurrent + median;
			if (maxButton < _maxButtons) maxButton = _maxButtons;
			if (maxButton >= _info.PageCount) maxButton = _info.PageCount;
			if (minButton < 0) minButton = 0;

			// first & prev buttons
			if (_info.PageCurrent > 0)
			{
				HtmlTableCell firstButtonCell = new HtmlTableCell();
				firstPage.Controls.Add(firstButtonCell);
				firstButtonCell.ID = "btnNext";
				firstButtonCell.Align = "center";
				firstButtonCell.Attributes.Add("onmouseover", "this.className='on';");
				firstButtonCell.Attributes.Add("onmouseout", "this.className='';");
				firstButtonCell.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, "0"));
				firstButtonCell.InnerText = "|<";		
			
				HtmlTableCell prevButtonCell = new HtmlTableCell();
				previousPage.Controls.Add(prevButtonCell);
				prevButtonCell.ID = "btnNext";
				prevButtonCell.Align = "center";
				prevButtonCell.Attributes.Add("onmouseover", "this.className='on';");
				prevButtonCell.Attributes.Add("onmouseout", "this.className='';");
				prevButtonCell.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, (_info.PageCurrent - 1).ToString()));
				prevButtonCell.InnerText = "<<";				
			}
		
			// page buttons
			if (_info.PageCount > 0)
			{
				for (int i = minButton; i < maxButton; i++)
				{
					HtmlTableCell currentCell = new HtmlTableCell();
					pages.Controls.Add(currentCell);
					currentCell.ID = "btnPager";
					currentCell.Align = "center";
					currentCell.Attributes.Add("onmouseover", "this.className='on';");
					currentCell.Attributes.Add("onmouseout", "this.className='';");
					currentCell.InnerText = (i + 1).ToString("#,#");
					
					if (i == _info.PageCurrent)
					{
						currentCell.Attributes.Add("style", "background-color:LightGrey;");
					}
					else
					{
						currentCell.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, i.ToString()));
					}			
				}			
			}

			if (_info.PageCurrent < (_info.PageCount - 1))
			{
				// last & next buttons
				HtmlTableCell lastButtonCell = new HtmlTableCell();
				lastPage.Controls.Add(lastButtonCell);
				lastButtonCell.ID = "btnNext";
				lastButtonCell.Align = "center";
				lastButtonCell.Attributes.Add("onmouseover", "this.className='on';");
				lastButtonCell.Attributes.Add("onmouseout", "this.className='';");
				lastButtonCell.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, (_info.PageCount - 1).ToString()));
				lastButtonCell.InnerText = ">|";	

				HtmlTableCell nextButtonCell = new HtmlTableCell();
				nextPage.Controls.Add(nextButtonCell);
				nextButtonCell.ID = "btnNext";
				nextButtonCell.Align = "center";
				nextButtonCell.Attributes.Add("onmouseover", "this.className='on';");
				nextButtonCell.Attributes.Add("onmouseout", "this.className='';");
				nextButtonCell.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, (_info.PageCurrent + 1).ToString()));
				nextButtonCell.InnerText = ">>";			
			}
			writer.Write("<table cellpadding='0' cellspacing='0' border='0'><tr><td>");
			writer.Write("<table class='pagerWhite' cellpadding='2' cellspacing='1' border='0'><tr>");
			firstPage.RenderControl(writer);
			previousPage.RenderControl(writer);
			pages.RenderControl(writer);
			nextPage.RenderControl(writer);
			lastPage.RenderControl(writer);			
			writer.Write("</tr>");
			writer.Write("</table>");
			writer.Write("</td></tr>");
			writer.Write("<tr><td class='pagerMsg'>");
			showingRecords.RenderControl(writer);
			writer.Write("</td></tr>");
			writer.Write("</table>");
		}

		public void RaisePostBackEvent(string eventArgument)
		{
			Info.PageCurrent = eventArgument.ToInt32();
			if (PageChanged != null) PageChanged(this, EventArgs.Empty);
		}
	}
}