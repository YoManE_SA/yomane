﻿using System;
using System.Resources;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Bll;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:LinkPager runat=server></{0}:Pager>")]
	public class LinkPager : Pager
	{
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			ResourceManager rm = WebUtils.GetResource("Pager");
			if (_info == null)
			{
				writer.Write(rm.GetString("NoPagingInformation"));
				return;
			} 
			
			if (_info.RowCount == 0)
			{
				writer.Write(rm.GetString("NoResults"));
				return;
			}

			if (_info.PageCount == 0)
			{
				return;
			}

			PlaceHolder firstPage = new PlaceHolder();
			PlaceHolder previousPage = new PlaceHolder();
			PlaceHolder pages = new PlaceHolder();
			PlaceHolder lastPage = new PlaceHolder();
			PlaceHolder nextPage = new PlaceHolder();
			Literal showingRecords = new Literal();
			showingRecords.Text = "<br/><span>" + rm.GetString("ShowingRecords") + " " + _info.RowFrom.ToString("#,#") + " - " + _info.RowTo.ToString("#,#") + " " + rm.GetString("Of") + " " + _info.RowCount.ToString("#,#") + "</span>"; 
			
			int median = Convert.ToInt32(System.Math.Floor((double)(_maxButtons / 2)));
			int minButton = _info.PageCurrent - median;
			int maxButton = _info.PageCurrent + median;		
			if (maxButton < _maxButtons) maxButton = _maxButtons;
			if (maxButton >= _info.PageCount) maxButton = _info.PageCount;
			if (minButton < 0) minButton = 0;

			// first & prev buttons
			if (_info.PageCurrent > 0)
			{
                HtmlAnchor firstButtonCell = new HtmlAnchor();
				firstPage.Controls.Add(firstButtonCell);
				firstButtonCell.ID = "btnNext";
                firstButtonCell.Attributes.Add("class", "pager");
                firstButtonCell.HRef = "javascript://";
				firstButtonCell.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, "0"));
				firstButtonCell.InnerText = "|<";

                HtmlAnchor prevButtonCell = new HtmlAnchor();
				previousPage.Controls.Add(prevButtonCell);
                prevButtonCell.ID = "btnNext";
                prevButtonCell.Attributes.Add("class", "pager");
                prevButtonCell.HRef = "javascript://";
				prevButtonCell.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, (_info.PageCurrent - 1).ToString()));
				prevButtonCell.InnerText = "<<";				
			}
		
			// page buttons
			if (_info.PageCount > 0)
			{
				for (int i = minButton; i < maxButton; i++)
				{
                    HtmlAnchor currentCell = new HtmlAnchor();
					pages.Controls.Add(currentCell);
                    currentCell.ID = "btnPager";
                    currentCell.HRef = "javascript://";
					currentCell.InnerText = (i + 1).ToString("#,#");
					
					if (i == _info.PageCurrent)
					{
                        currentCell.Attributes.Add("class", "pager selected");
					}
					else
					{
                        currentCell.Attributes.Add("class", "pager");
                        currentCell.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, i.ToString()));
					}			
				}			
			}

			if (_info.PageCurrent < (_info.PageCount - 1))
			{
				// last & next buttons
                HtmlAnchor lastButtonCell = new HtmlAnchor();
				lastPage.Controls.Add(lastButtonCell);
                lastButtonCell.ID = "btnNext";
                lastButtonCell.Attributes.Add("class", "pager");
                lastButtonCell.HRef = "javascript://";
				lastButtonCell.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, (_info.PageCount - 1).ToString()));
				lastButtonCell.InnerText = ">|";

                HtmlAnchor nextButtonCell = new HtmlAnchor();
				nextPage.Controls.Add(nextButtonCell);
                nextButtonCell.ID = "btnNext";
                nextButtonCell.Attributes.Add("class", "pager");
                nextButtonCell.HRef = "javascript://";
				nextButtonCell.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, (_info.PageCurrent + 1).ToString()));
				nextButtonCell.InnerText = ">>";			
			}
           
            writer.Write("<div class=\"wrap-pager\">");
			firstPage.RenderControl(writer);
			previousPage.RenderControl(writer);
			pages.RenderControl(writer);
			nextPage.RenderControl(writer);
			lastPage.RenderControl(writer);			
			showingRecords.RenderControl(writer);
            writer.Write("</div>");
		}
	}
}
