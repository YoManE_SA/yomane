﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Linq;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:StateDropDown runat=server></{0}:StateDropDown>")]
	public class StateDropDown : DropDownBase
	{
		public bool StartEmpty { get; set; }
		private int _countryID { get { return ViewState["CountryID"].ToNullableInt().GetValueOrDefault(Domain.Current.HostCountryId); } set { ViewState["CountryID"] = value; } } // South Africa = 2

		public int CountryID
		{
			get { return _countryID; }
			set {
				Items.Clear();			//DO not remove the below lines - fix a bug in .net after postback
				SelectedIndex = -1;		//Nothing changes including SelectedIndex
				SelectedValue = null;	//Nothing changes including SelectedValue
				ClearSelection();		//DO not remove
				_countryID = value;
				_requiresDataBinding = true;
			}
		}

		public string CountryIsoCode
		{
			get { if (_countryID == 0) return null; return Bll.Country.Get(_countryID).IsoCode2; }
			set
			{
				if (string.IsNullOrEmpty(value)) {
					CountryID = 0;
				} else { 
					var country = Bll.Country.Get(value);
					CountryID = (country != null) ? country.ID : 0;
				}
			}
		}

		public int? SelectedStateID
		{
			get
			{
				if (!IsSelected) return null;
				return int.Parse(Value);
			}
			set
			{
				if (value == null)
					return;
				ClearSelection();
				base.Value = value != null ? value.Value.ToString() : null;
				ListItem item = Items.FindByValue(value.Value.ToString());
				if (item != null)
					item.Selected = true;
			}
		}

		public string ISOCode
		{
			set
			{
				var state = Bll.State.Get(value);
				if (state != null) SelectedStateID = state.ID;
			}
			get 
			{
				if (IsSelected) return Bll.State.Get(SelectedStateID.Value).IsoCode;
				else return null;
			}
		}

		protected override void DataBind(bool raiseOnDataBinding)
		{
			var states = Bll.State.GetForCountry(CountryID);
            //foreach (var state in states) state.Name = WebUtils.GetStateName(state.IsoCode);
			if(states != null) states = states.OrderBy(s => s.Name).ToList();
            DataSource = states;
			DataTextField = "Name";
			DataValueField = "ID";
			base.DataBind(raiseOnDataBinding);
		}

	}
}
