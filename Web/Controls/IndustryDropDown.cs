﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:IndustryDropDown runat=server></{0}:IndustryDropDown>")]
	public class IndustryDropDown : DropDownBase
	{
		public bool EnableNone { get; set; }
		protected override void DataBind(bool raiseOnDataBinding)
		{
            DataSource = Bll.Merchants.Industry.Cache;
			DataValueField = "ID";
			DataTextField = "Name";
			base.DataBind(raiseOnDataBinding);
			if (EnableNone) Items.Add(new System.Web.UI.WebControls.ListItem("[not set]", "0"));
		}
	}
}



