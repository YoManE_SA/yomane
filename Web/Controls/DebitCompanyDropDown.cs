﻿using System;
using System.Web.UI;
using System.Linq;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:DebitCompanyDropDown runat=server></{0}:DebitCompanyDropDown>")]
	public class DebitCompanyDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			DataSource = Bll.DebitCompanies.DebitCompany.GetCache().Values.Where(x => x.IsActive == true).OrderBy(x => x.Name);
			DataValueField = "ID";
			DataTextField = "Name";
			base.DataBind(raiseOnDataBinding);
		}
	}
}



