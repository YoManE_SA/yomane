﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
    public class ShowTotalsButton : WebControl
    {
        public string Text { get; set; }
        public int CurrencyID { get; set; }
        public int? PayID { get; set; }
        public int? AFPID { get; set; }
        public int? CompanyID { get; set; }
        public int? AffiliateID { get; set; }
        public DateTime? InsDateFrom { get; set; }
        public DateTime? InsDateTo { get; set; }

        protected override void OnInit(EventArgs e)
        {
            if (Text == null) Text = WebUtils.GetResource("Common").GetString("ShowTotals");
            base.OnInit(e);
        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            string filters = "";
            //UserType userType = WebUtils.LoggedUser.Type;
            if (PayID != null) filters += "&PayID=" + PayID;
            if (AFPID != null) filters += "&AFPID=" + AFPID;
            if (CompanyID != null) filters += "&CompanyID=" + CompanyID;
            if (AffiliateID != null) filters += "&AffiliateID=" + AffiliateID;
            if (PayID == null || PayID < 1) filters += "&IncRR=1";
            if (InsDateFrom != null) filters += "&InsDateFrom=" + HttpUtility.UrlEncode(InsDateFrom.Value.ToShortDateString());
            if (InsDateTo != null) filters += "&InsDateTo=" + HttpUtility.UrlEncode(InsDateTo.Value.ToShortDateString());
            writer.Write(string.Format("<input onclick=\"window.open('ShowTotals.aspx?currencyID={1}{2}', 'Totals', 'height=600,width=985,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes');\" class=\"btn btn-default\" type=\"button\" BorderWidth=\"0\" value=\"{0}\"/>", Text, CurrencyID, filters));
            if (!Infrastructure.Application.IsProduction) writer.Write(string.Format(" <input onclick=\"window.open('ShowTotalsV2.aspx?currencyID={1}{2}', 'Totals2', 'height=350,width=720,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes');\" class=\"search\" type=\"button\" BorderWidth=\"0\" value=\"{0}\"/>", Text, CurrencyID, filters));
        }

        public static void LoadTotalsParams(ref Totals totals)
        {
            HttpRequest Request = System.Web.HttpContext.Current.Request;
            if (Request["CurrencyID"] != null) totals.Currency = Request["CurrencyID"].ToInt32(0);
            if (Request["PayID"] != null) totals.PayID = Request["PayID"].ToInt32(0);

			Infrastructure.Security.UserRole userRole = Infrastructure.Security.UserRole.Merchant;
            var user = Infrastructure.ObjectContext.Current.User;
            if (user != null) userRole = user.Role;

			if (userRole == Infrastructure.Security.UserRole.Partner)
            {
                if (Request["CompanyID"] != null) totals.CompanyID = Request["CompanyID"].ToInt32(0);
                if (Request["AFPID"] != null)
                {
                    totals.AFP_ID = Request["AFPID"].ToInt32(0);
                    totals.AffiliateID = Bll.Affiliates.Affiliate.Current.ID;
                }
            }
			else if (userRole == Infrastructure.Security.UserRole.Merchant || userRole == Infrastructure.Security.UserRole.MerchantSubUser)
            {
				totals.CompanyID = Bll.Merchants.Merchant.Current.ID;
            }
			else if (userRole == Infrastructure.Security.UserRole.Admin)
            {
                if (Request["CompanyID"] != null) totals.CompanyID = Request["CompanyID"].ToInt32(0);
                if (Request["AFPID"] != null) totals.AFP_ID = Request["AFPID"].ToInt32(0);
                if (Request["AffiliateID"] != null) totals.AFP_ID = Request["AffiliateID"].ToInt32(0);
            }
            DateTime dt;
            string sFilters = "";
            if (Request["InsDateFrom"] != null)
            {
                if (DateTime.TryParse(Request["InsDateFrom"], out dt))
                    sFilters += " And tblCompanyTransPass.InsertDate>=Convert(datetime, '" + dt.ToString("yyyy/MM/dd") + " 00:00:00', 120)";
            }
            if (Request["InsDateTo"] != null)
            {
                if (DateTime.TryParse(Request["InsDateTo"], out dt))
                    sFilters += " And tblCompanyTransPass.InsertDate<=Convert(datetime, '" + dt.ToString("yyyy/MM/dd") + " 23:59:59', 120)";
            }
            if (totals.PayID == 0) sFilters += " And IsTestOnly=0";
            if (sFilters.Length > 1) totals.Where = sFilters.Substring(5);
        }
		/*
        public static void LoadTotalsParams(Totals3 totals)
        {
            HttpRequest Request = System.Web.HttpContext.Current.Request;
            if (Request["CurrencyID"] != null) totals.Currency = (CommonTypes.Currency)Request["CurrencyID"].ToInt32(0);
            if (Request["PayID"] != null) totals.PayID = Request["PayID"].ToInt32(0);

            UserType userType = UserType.Unknown;
            var user = Netpay.Infrastructure.Security.SecurityManager.GetUser(WebUtils.CredentialsToken);
            if (user != null) userType = user.Type;

            if (userType == UserType.Partner)
            {
                if (Request["CompanyID"] != null) totals.CompanyID = Request["CompanyID"].ToInt32(0);
                if (Request["AFPID"] != null)
                {
                    totals.AFP_ID = Request["AFPID"].ToInt32(0);
                    totals.AffiliateID = user.ID;
                }
            }
            else if (userType == UserType.MerchantLimited || userType == UserType.MerchantPrimary)
            {
                totals.CompanyID = user.ID;
            }
            else if (userType == UserType.NetpayUser || userType == UserType.NetpayAdmin)
            {
                if (Request["CompanyID"] != null) totals.CompanyID = Request["CompanyID"].ToInt32(0);
                if (Request["AFPID"] != null) totals.AFP_ID = Request["AFPID"].ToInt32(0);
                if (Request["AffiliateID"] != null) totals.AFP_ID = Request["AffiliateID"].ToInt32(0);
            }

            DateTime dt;
            if (Request["InsDateFrom"] != null)
            {
                if (DateTime.TryParse(Request["InsDateFrom"], out dt)) totals.insertDateFrom = dt;
            }
            if (Request["InsDateTo"] != null)
            {
                if (DateTime.TryParse(Request["InsDateTo"], out dt)) totals.insertDateTo = dt;
            }
            if (totals.PayID == 0) totals.IsIncludeTest = false;
        }
		*/
    }
}
