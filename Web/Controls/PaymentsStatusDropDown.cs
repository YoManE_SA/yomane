﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:PaymentsStatusDropDown runat=server></{0}:PaymentsStatusDropDown>")]
	public class PaymentsStatusDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			String[] names = PaymentsStatus.GetNames(typeof(PaymentsStatus));
			PaymentsStatus[] values = (PaymentsStatus[])Enum.GetValues(typeof(PaymentsStatus));
			var lst = new System.Collections.Generic.Dictionary<int, string>();
			for (int i = 1; i < names.Length; i++) 
				lst.Add((int)values[i], HttpContext.GetGlobalResourceObject("MultiLang", "PaymentsStatus" + names[i]).ToString());
			DataSource = lst;
			DataValueField = "Key";
			DataTextField = "Value";
			base.DataBind(raiseOnDataBinding);
		}
	}
}
