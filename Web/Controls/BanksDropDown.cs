using System;
using System.Web.UI;

namespace Netpay.Web.Controls
{
	/// <summary>
	/// Payment banks (not debit companies)
	/// </summary>
	[ToolboxData("<{0}:DebitCompanyDropDown runat=server></{0}:DebitCompanyDropDown>")]
	public class BanksDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			DataSource = Bll.Wires.Wire.GetSourceAccountList("System.Masav");
			DataValueField = "Key"; DataTextField = "Value";
			base.DataBind(raiseOnDataBinding);
		} 
	}
}

