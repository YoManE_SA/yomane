﻿using System;
using System.Resources;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	public class EnumDropDown : DropDownBase
	{
		private string _ViewEnumName;
		public string ViewEnumName { get { return _ViewEnumName; } set { _ViewEnumName = value; if (Page != null && !Page.IsPostBack) DataBind(); } }

        public string ExcludeValue { get; set; }

		protected override void DataBind(bool raiseOnDataBinding)
		{
			var type = System.Web.Compilation.BuildManager.GetType(_ViewEnumName, false);
			if (type == null) throw new Exception("type could not be found:" + _ViewEnumName + ", ClientID:" + ClientID);
            _ViewEnumName = EnumHelper.GetViewEnumName(type);
			DataSource = EnumHelper.GetEnumList(type, ExcludeValue);
			DataValueField = "Key";
			DataTextField = "Value";
			base.DataBind(raiseOnDataBinding);
		}
	}
}
