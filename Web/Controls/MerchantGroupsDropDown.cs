﻿using System;
using System.Web.UI;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:MerchantGroupsDropDown runat=server></{0}:MerchantGroupsDropDown>")]
	public class MerchantGroupsDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			DataSource = Bll.Merchants.Group.Cache;
			DataValueField = "ID";
			DataTextField = "Name";
			base.DataBind(raiseOnDataBinding);
		}
	}
}
