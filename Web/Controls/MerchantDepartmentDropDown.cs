﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
	public class MerchantDepartmentDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			DataSource = Bll.Merchants.Department.Cache;
			DataValueField = "ID";
			DataTextField = "Name";
			base.DataBind(raiseOnDataBinding);
		}
	}
}
