﻿using System;
using System.Web.UI;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:ManagingCompanyDropDown runat=server></{0}:ManagingCompanyDropDown>")]
	public class ManagingCompanyDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			DataSource = GlobalData.GetGroup(GlobalDataGroup.ManagingCompany, Language.English);
			DataValueField = "ID";
			DataTextField = "Value";
			base.DataBind(raiseOnDataBinding);
		}
	}
}
