﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
    [ToolboxData("<{0}:MerchantRiskMngReplyCodeDropDown runat=server></{0}:MerchantRiskMngReplyCodeDropDown>")]
    public class MerchantRiskMngReplyCodeDropDown : DropDownBase
    {
        protected override void DataBind(bool raiseOnDataBinding)
        {
            var sf = new Netpay.Bll.DebitCompanies.ResponseCode.SearchFilters();
            sf.DebitCompanyID = 1;
            sf.FailSource = 1;

            DataSource = Bll.DebitCompanies.ResponseCode.Search(sf, null).OrderBy(x=>x.Code).ToList();

            DataValueField = "Code";
            DataTextField = "Code";

            base.DataBind(raiseOnDataBinding);
        }
    }
}
