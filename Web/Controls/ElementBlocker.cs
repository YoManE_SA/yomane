﻿using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:ElementBlocker runat=server></{0}:ElementBlocker>")]
	public class ElementBlocker : WebControl
	{
		private string _elementClientID = null;
		private string _text = null;

		public string ElementClientID
		{
			get { return _elementClientID; }
			set { _elementClientID = value; }
		}

		public string Text
		{
			get { return _text; }
			set { _text = value; }
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			if (Visible)
				writer.Write(string.Format("<script type='text/javascript'>$(document).ready(function (){{netpay.Blocker.blockElement('{0}', '{1}');}});</script>", _elementClientID, _text));
		}
	}
}
