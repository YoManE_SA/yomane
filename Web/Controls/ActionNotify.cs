﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Web.Controls
{
    public class ActionNotify : System.Web.UI.WebControls.WebControl, System.Web.UI.IPostBackEventHandler
    {
        public event System.Web.UI.WebControls.CommandEventHandler Confirmed;
        public string LastConfirmed { get; private set; }
        public string Message { get; set; }
        public event EventHandler Fired;
        public ActionNotify()
            : base(System.Web.UI.HtmlTextWriterTag.Div)
        {
            ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(Message)) return;
            this.AddAttributesToRender(writer);
            base.RenderBeginTag(writer);
            writer.Write(Message);
            base.RenderEndTag(writer);
        }

        public void SetException(Exception ex)
        {
            string text = null;
            var rm = new System.Resources.ResourceManager("Resources.DataActions", System.Reflection.Assembly.Load("App_GlobalResources"));
            if (rm != null) text = rm.GetString(ex.Message);
            if (string.IsNullOrEmpty(text)) text = ex.Message;
            SetMessage(text, true);
            OnFire(EventArgs.Empty);
        }

        public void SetMessage(string message, bool isError = false)
        {
            if (message.Contains("Netpay"))
            {
                message = message.Replace("Netpay", "");
            }

            if (message.Contains("netpay"))
            {
                message.Replace("netpay", "");
            }

            var rm = new System.Resources.ResourceManager("Resources.DataActions", System.Reflection.Assembly.Load("App_GlobalResources"));
            if (rm != null && isError) message = string.Format("{0}: {1}", rm.GetString("Error"), message);
            Message = message;
            CssClass = isError ? "alert alert-danger" : "alert alert-info";
            OnFire(EventArgs.Empty);
        }

        public void ClearMessage()
        {
            Message = "";
        }

        public void SetSuccess(string message)
        {
            Message = message;
            CssClass = "alert alert-success";
            OnFire(EventArgs.Empty);
        }

        public void SetWorning(string name, int level, string buttonText, string message)
        {
            Message = message;
            if (string.IsNullOrEmpty(buttonText)) buttonText = "המשך";
            Message += string.Format(" <input type=\"button\" name=\"{0}_{1}\" value=\"{2}\" onclick=\"window['SaveBigData'] = true;{3}\" />", ClientID, name, buttonText, Page.ClientScript.GetPostBackEventReference(this, name));
            CssClass = (level > 5) ? "alert alert-danger" : "alert alert-warning";
            OnFire(EventArgs.Empty);
        }

        protected void OnFire(EventArgs e)
        {
            Focus();
            if (Fired != null) Fired(this, e);
        }

        public bool IsConfirmed(string name)
        {
            return LastConfirmed == name;
        }

        public void RaisePostBackEvent(string eventArgument)
        {
            if (string.IsNullOrEmpty(eventArgument)) return;
            LastConfirmed = eventArgument;
            var eventarges = new System.Web.UI.WebControls.CommandEventArgs("Confirm", LastConfirmed);
            if (Confirmed != null) Confirmed(this, eventarges);
            else RaiseBubbleEvent(this, eventarges);
        }
    }
}
