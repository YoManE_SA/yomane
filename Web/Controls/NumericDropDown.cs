﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
	public class NumericDropDown : DropDownBase
	{
		public int MinValue { get; set; }
		public int MaxValue { get; set; }
		public int ValueStep { get; set; }
		public NumericDropDown()
		{
			ValueStep = 1;
			MaxValue = 10;
		}
		public override string SelectedValue
		{
			get { return base.SelectedValue; }
			set { EnsureChildControls(); base.SelectedValue = value; }
		}
		protected override void CreateChildControls()
		{
			base.CreateChildControls();
			if (Page.IsPostBack)
				Items.Clear();
			if (EnableBlankSelection)
				Items.Add(new System.Web.UI.WebControls.ListItem(BlankSelectionText, ""));
			for (int i = MinValue; i <= MaxValue; i += ValueStep) {
				if (Items.Count > 100) break;
				Items.Add(new System.Web.UI.WebControls.ListItem(i.ToString(), i.ToString()));
			}
		}

		protected override void DataBind(bool raiseOnDataBinding)
		{
            EnsureChildControls();
            ClearSelection();
			//base.DataBind(raiseOnDataBinding);
			if (raiseOnDataBinding) OnDataBinding(EventArgs.Empty);
			try { base.SelectedValue = Value; }
			catch (System.ArgumentOutOfRangeException)
			{
				Items.Add(new System.Web.UI.WebControls.ListItem(Value, Value));
				base.SelectedValue = Value;
			}
		}
	}
}
