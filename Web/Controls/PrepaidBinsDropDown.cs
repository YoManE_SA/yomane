﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Netpay.Web.Controls
{
    [ToolboxData("<{0}:PrepaidBinsDropDown runat=server></{0}:PrepaidBinsDropDown>")]
    public class PrepaidBinsDropDown : DropDownBase
    {
        protected override void DataBind(bool raiseOnDataBinding)
        {
            DataSource = Bll.PaymentMethods.CreditCardBin.PrepaidBins
                .Select(bin => new
                {
                    BIN = bin.BIN
                    //Name = string.Format(
                    //    "{0}-{1}",
                    //    bin.BIN,
                    //    Bll.PaymentMethods.PaymentMethod.CachePrepaid.Where(x => x.ID == (short)bin.PaymentMethodID).SingleOrDefault().Name)
                })
                .ToList();
            DataTextField = "BIN";
            DataValueField = "BIN";
            base.DataBind(raiseOnDataBinding);
        }
    }
}
