﻿using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Bll;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:MerchantTransactionRowView runat=server></{0}:TransactionRowView>")]
	public sealed class MerchantTransactionRowView : Netpay.Web.Controls.TransactionRowView
    {
        protected override void Render(HtmlTextWriter writer)
        {
            if (_transactionInfo == null)
                return;

            string rowBackgroundColor = "";
            string legendCellStyle = "";
            string cellTextColor = "";
            bool showAskRefund = false;
            string currencySymbol = "";
            string legendTitle = "";

            currencySymbol = Bll.Currency.Get(_transactionInfo.CurrencyID).Symbol;

            if (_transactionInfo.PaymentMethodID > (PaymentMethodEnum) 15)
            {
                if (_transactionInfo.Status == TransactionStatus.Declined)
                {
                    if (_transactionInfo.TransType == TransactionType.Authorization || _transactionInfo.TransType == TransactionType.AuthorizationCapture)
                    {
                        legendCellStyle = "border:1px solid #ff6666; background-color:#ffffff;";
                        rowBackgroundColor = "#F6FEF6";
                        legendTitle = "authorizeDecline";
                    }
                    else
                    {
                        legendCellStyle = "background-color:#ff6666;";
                        rowBackgroundColor = "#F6FEF6";
                        legendTitle = "decline";
                    }
                }
                else if (_transactionInfo.Status == TransactionStatus.Authorized)
                {
                    legendCellStyle = "border:1px solid #66cc66; background-color:#ffffff;";
                    rowBackgroundColor = "#ffffff";
                    legendTitle = "authorize";
                }
                else
                {
                    showAskRefund = true;
                    rowBackgroundColor = "#F6FEF6";
                    legendCellStyle = "background-color:#66cc66;";
                    legendTitle = "capture";
                }

                if (_transactionInfo.CreditType == (byte)CreditType.Refund)
                {
                    cellTextColor = "#cc0000";
                    showAskRefund = false;
                }

                if (_transactionInfo.PaymentMethodID >= PaymentMethodEnum.ECCheck)
                {
                    showAskRefund = false;
                }

                if (_transactionInfo.DeniedStatusID != (byte)DeniedStatus.ValidTransaction)
                {
                    showAskRefund = true;
                    if (_transactionInfo.DeniedStatusID == (int)DeniedStatus.UnsettledBeenSettledAndValid || _transactionInfo.DeniedStatusID == (int)DeniedStatus.SetFoundValidRefunded || _transactionInfo.DeniedStatusID == (int)DeniedStatus.DuplicateValid || _transactionInfo.DeniedStatusID == (int)DeniedStatus.DuplicateTransactionWorkedOut)
                    {
                        rowBackgroundColor = "#fff3dd";
                        legendCellStyle = "background-color:#66cc66;";
                        legendTitle = "clarify";
                    }
                    else
                    {
                        rowBackgroundColor = "#ffebe1";
                        legendCellStyle = "background-color:#66cc66;";
                        legendTitle = "chargeback";
                    }
                }
            }
            else if (_transactionInfo.PaymentMethodID == PaymentMethodEnum.ManualFees)
            {
                rowBackgroundColor = "#f5f5f5";
                legendCellStyle = "background-color:#484848;";
                cellTextColor = "#cc0000";
                legendTitle = "system";
            }
            else if (_transactionInfo.PaymentMethodID == PaymentMethodEnum.Admin)
            {
                showAskRefund = true;
                rowBackgroundColor = "#eeeffd";
                legendCellStyle = "background-color:#003399;";
                legendTitle = "admin";

                if (_transactionInfo.CreditType == (byte)CreditType.Refund)
                    cellTextColor = "#cc0000";
            }
            else if (_transactionInfo.PaymentMethodID == PaymentMethodEnum.SystemFees)
            {
                showAskRefund = true;
                rowBackgroundColor = "#f5f5f5";
                legendCellStyle = "background-color:#484848;";
                cellTextColor = "#cc0000";
                legendTitle = "system";
            }
            else if (_transactionInfo.PaymentMethodID == PaymentMethodEnum.RollingReserve)
            {
                rowBackgroundColor = "#f5f5f5";
                legendCellStyle = "background-color:#484848;";
                legendTitle = "system";
            }
            else if (_transactionInfo.PaymentMethodID == PaymentMethodEnum.BankFees)
            {
                showAskRefund = false;
                rowBackgroundColor = "#eeeffd";
                legendCellStyle = "background-color:#804000;";
                cellTextColor = "#cc0000";
            }
            else if (_transactionInfo.PaymentMethodID == PaymentMethodEnum.Unknown)
            {
                if (_transactionInfo.ReplyCode != "000" && _transactionInfo.ReplyCode != "001" && ("" + _transactionInfo.ReplyCode).Trim() != "")
                {
					if (_transactionInfo.TransType == TransactionType.Authorization)
                    {
                        legendCellStyle = "border:1px solid #ff6666;";
                        rowBackgroundColor = "#F6FEF6";
                    }
                    else
                    {
                        legendCellStyle = "background-color:#ff6666;";
                        rowBackgroundColor = "#F6FEF6";
                    }
                }
                else if (_transactionInfo.ReplyCode == null)
                {
                    if (_transactionInfo.TransactionSource == TransactionSource.CcStorageFee)
                    {
                        rowBackgroundColor = "#f5f5f5";
                        legendCellStyle = "background-color:#484848;";
                        cellTextColor = "#cc0000";
                    }
                    else if (_transactionInfo.Status == TransactionStatus.Captured)
                    {
                        if (_transactionInfo.DeniedStatusID == (int)DeniedStatus.UnsettledBeenSettledAndValid || _transactionInfo.DeniedStatusID == (int)DeniedStatus.SetFoundValidRefunded || _transactionInfo.DeniedStatusID == (int)DeniedStatus.DuplicateValid || _transactionInfo.DeniedStatusID == (int)DeniedStatus.DuplicateTransactionWorkedOut)
                        {
                            rowBackgroundColor = "#fff3dd";
                            legendCellStyle = "background-color:#66cc66;";
                        }
                        else if (_transactionInfo.DeniedStatusID > 0)
                        {
                            rowBackgroundColor = "#ffebe1";
                            legendCellStyle = "background-color:#66cc66;";
                        }
                        else
                        {
                            legendCellStyle = "background-color:#66cc66;";
                        }
                    }
                }
            }

            // output
            writer.Write(string.Format("<tr onmouseover=\"this.style.backgroundColor='#d8d8d8';\" onmouseout=\"this.style.backgroundColor='{0}';\" style=\"background-color:{0};\">", rowBackgroundColor));

			// expand button
			if (_showExpandButton)
			{
                if (_transactionInfo.PaymentMethodID == PaymentMethodEnum.ManualFees || _transactionInfo.PaymentMethodID == PaymentMethodEnum.SystemFees /*|| _transactionInfo.PaymentMethodID == PaymentMethodEnum.Admin*/ || _transactionInfo.PaymentMethodID == PaymentMethodEnum.RollingReserve)
                {
                    writer.Write(string.Format("<td><img id='Row{0}A' src='/NPCommon/Images/disabled-button.png' alt='' border='0' align='middle' /></td>", _transactionInfo.ID));
                }
                else
                {
					writer.Write(string.Format("<td><img onclick=\"netpay.Common.toggleTransactionInfo('{0}','{1}','{2}','{3}','{4}', '{5}')\" style='cursor:pointer;' id='expandImage-{0}' src='{5}/images/plus.png' alt='' border='0' align='middle' /></td>", _transactionInfo.ID, _transactionInfo.PaymentMethodID, showAskRefund, rowBackgroundColor.Replace("#", ""), _transactionInfo.Status, WebUtils.MapTemplateVirPath(this, "")));
                }
			}
            else
            {
				writer.Write("<td style='height:16px;'>&nbsp;</td>");
			}
            // legend square
			if (_showLegend)
            {
                string legendText = "&nbsp;";
                if (_transactionInfo.IsTest)
                    legendCellStyle += "border: 1px dotted #FE0E36;";
                if (Transaction.IsPendingChargeback)
                    legendText = "NR";

                ResourceManager rm = WebUtils.GetResource("Legend");
                writer.Write(string.Format("<td><div title='{0}' style='text-align:center; width: 15px; height: 15px;  {1}'>{2}</div></td>", rm.GetString(legendTitle), legendCellStyle, legendText));
            }

            // merchant name 
            if (_showMerchantName)
                writer.Write(string.Format("<td style='color:{0}; text-align:{2};' nowrap='nowrap'>{1}</td>", cellTextColor, _transactionInfo.Merchant == null ? "[Hidden]" : _transactionInfo.Merchant.Name, WebUtils.CurrentAlignment));

            // id
            writer.Write(string.Format("<td style='color:{0}; text-align:{2};' nowrap='nowrap'>{1}</td>", cellTextColor, _transactionInfo.ID, WebUtils.CurrentAlignment));

            // date
            writer.Write(string.Format("<td style='color:{0}; text-align:{2}; direction:ltr;' nowrap='nowrap'>{1}</td>", cellTextColor, _showDateChargeback ? _transactionInfo.ChargebackDate.ToString("dd/MM/yyyy HH:mm") : _transactionInfo.InsertDate.ToString("dd/MM/yyyy HH:mm"), WebUtils.CurrentAlignment));

            // payment method
            if (_showPaymentMethod)
            {
                PaymentMethodView paymentMethodView = new PaymentMethodView();
                paymentMethodView.PaymentMethodID = (int)Transaction.PaymentMethodID;
                paymentMethodView.PaymentMethodDisplay = Transaction.PaymentMethodDisplay;
				paymentMethodView.BinCountryCode = Transaction.IssuerCountryIsoCode;
                writer.Write(string.Format("<td style='color:{0}; text-align:{1};' nowrap='nowrap'>", cellTextColor, WebUtils.CurrentAlignment));
                paymentMethodView.RenderControl(writer);
                writer.Write("</td>");
            }

            // credit type
            if (_showCreditType)
            {
                string creditType = GlobalData.GetValue(GlobalDataGroup.CreditType, WebUtils.CurrentLanguage, (int) _transactionInfo.CreditType).Value;
                writer.Write(string.Format("<td style='color:{0}; text-align:{2};' nowrap='nowrap'>{1}</td>", cellTextColor, creditType, WebUtils.CurrentAlignment));
            }

            // amount
            writer.Write(string.Format("<td style='color:{0}; text-align:{3}; direction:ltr;' nowrap='nowrap'>{1} {2}</td>", cellTextColor, currencySymbol, _transactionInfo.SignedAmount.ToString("0.00"), WebUtils.CurrentAlignment));

            // settlement status
            if (_showStatus)
            {
                string paymentStatusText = "";
				if (_transactionInfo.Status == TransactionStatus.Captured){
					if (_transactionInfo.PaymentsIDs != null)
					{
						PaymentsStatus paymentStatus = Enums.GetPaymentsStatus(_transactionInfo.PaymentsIDs.Trim());
						if (paymentStatus == PaymentsStatus.Archived)
							paymentStatusText = "---";
						else if (paymentStatus == PaymentsStatus.Settled && !_transactionInfo.IsInstallments)
						{
							ResourceManager rm = WebUtils.GetResource("Common");
							paymentStatusText = rm.GetString("PaymentsStatus" + paymentStatus.ToString()).ToString();
							paymentStatusText += string.Format(" (<a href='SettlementList.aspx?payID={0}&Currency={1}'>{0}</a>) ", _transactionInfo.PrimaryPaymentID, _transactionInfo.CurrencyID);
						}
					}
				} else {
					if (_transactionInfo.PrimaryPaymentID != 0) {
						ResourceManager rm = WebUtils.GetResource("Common");
						paymentStatusText = rm.GetString("PaymentsStatus" + PaymentsStatus.Settled.ToString()).ToString();
						paymentStatusText += string.Format(" (<a href='SettlementList.aspx?payID={0}&Currency={1}'>{0}</a>) ", _transactionInfo.PrimaryPaymentID, _transactionInfo.CurrencyID);
					}
				}
                writer.Write(string.Format("<td style='color:{0}; text-align:{2};' nowrap='nowrap'>{1}</td>", cellTextColor, paymentStatusText, WebUtils.CurrentAlignment));
            }

            // approval number
            if (_showApprovalNumber)
                writer.Write(string.Format("<td style='color:{0}; text-align:{2};' nowrap='nowrap'>{1}</td>", cellTextColor, _transactionInfo.ApprovalNumber, WebUtils.CurrentAlignment));

            // netpay fee
            if (_showTransactionFee)
            {
                if (_transactionInfo.TransactionFee == 0 || _transactionInfo.PaymentsIDs == "0")
                    writer.Write(string.Format("<td style='color:{0}; text-align:{1};' nowrap='nowrap'> --- </td>", cellTextColor, WebUtils.CurrentAlignment));
                else
                    writer.Write(string.Format("<td style='color:{0}; text-align:{3};' nowrap='nowrap'>{1} {2}</td>", cellTextColor, currencySymbol, _transactionInfo.TransactionFee.ToString("0.00"), WebUtils.CurrentAlignment));
            }

            // transaction id
            if (_showPassedTransactionID)
            {
                if (_transactionInfo.PassedTransactionID > 0)
                    writer.Write(string.Format("<td style='color:{0}; text-align:{2};' nowrap='nowrap'>{1}</td>", cellTextColor, _transactionInfo.PassedTransactionID, WebUtils.CurrentAlignment));
                else
                    writer.Write(string.Format("<td style='color:{0}; text-align:{2};' nowrap='nowrap'>{1}</td>", cellTextColor, "---", WebUtils.CurrentAlignment));
            }

            // reply code
            if (_showReplyCode)
                writer.Write(string.Format("<td style='color:{0}; text-align:{2};' nowrap='nowrap'>{1}</td>", cellTextColor, _transactionInfo.ReplyCode, WebUtils.CurrentAlignment));

            writer.Write("</tr>");

            // installments
            if (_transactionInfo.IsInstallments && _transactionInfo.InstallmentList != null)
            {
                ResourceManager rm = WebUtils.GetResource("Common");
                foreach (var currentInstallment in _transactionInfo.InstallmentList)
                {
                    writer.Write(string.Format("<tr onmouseover=\"this.style.backgroundColor='#d8d8d8';\" onmouseout=\"this.style.backgroundColor='';\" bgcolor=\"{0}\">", rowBackgroundColor));
                    if (_showStatus)
                        writer.Write("<td colspan='4'></td>");
                    else
                        writer.Write("<td colspan='5'></td>");
                    writer.Write("<td>");
                    writer.Write(rm.GetString("Installment"));
                    writer.Write(": ");
                    writer.Write(currentInstallment.Comment);
                    writer.Write("</td>");
                    writer.Write("<td>");
                    writer.Write(currentInstallment.Amount.ToAmountFormat(_transactionInfo.CurrencyID));
                    writer.Write("</td>");

                    if (_showStatus)
                    {
                        if (currentInstallment.SettlementID > 0)
                        {
                            writer.Write("<td>");
                            writer.Write(rm.GetString("PaymentsStatusSettled"));
                            writer.Write(string.Format(" (<a href='SettlementList.aspx?payID={0}&Currency={1}'>{0}</a>) ", currentInstallment.SettlementID, _transactionInfo.CurrencyID));
                            writer.Write("</td>");
                        }
                        else
                        {
                            writer.Write("<td>");
                            writer.Write(rm.GetString("PaymentsStatusUnsettled"));
                            writer.Write("</td>");
                        }
                    }
                    
                    writer.Write("</tr>");
                }
            }

            if (_showExpandButton)
                writer.Write(string.Format("<tr><td id='infoContailner-{0}' style='display:none; padding:0px; width: 100%' colspan='9'></td></tr>", _transactionInfo.ID));
        }
    }
}
