﻿using System;
using System.Web.UI;

namespace Netpay.Web.Controls
{
    [ToolboxData("<{0}:LanguageDropDown runat=server></{0}:LanguageDropDown>")]
    public class LanguageDropDown : DropDownBase
    {
        protected override void DataBind(bool raiseOnDataBinding)
        {
            DataSource = Bll.International.Language.Cache;
            DataValueField = "ID";
            DataTextField = "Name";
            base.DataBind(raiseOnDataBinding);
        }
    }
}
