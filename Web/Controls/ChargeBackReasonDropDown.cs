﻿using System;
using System.Web.UI;
using System.Linq;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
    public class ChargeBackReasonDropDown : DropDownBase
    {
        protected override void DataBind(bool raiseOnDataBinding)
        {
            var CBreasons = Bll.Transactions.ChargeBackReason.Cache;
            
            DataSource = Bll.Transactions.ChargeBackReason.Cache.OrderByDescending(x=>x.Brand).ThenBy(x=>x.ReasonCode).ToList();
            DataValueField = "ReasonCode";
            DataTextField = "Text";
            base.DataBind(raiseOnDataBinding);
        }
        
    }
}
