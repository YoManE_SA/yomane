﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:CreditCardExpirationDate runat=server></{0}:CreditCardExpirationDate>")]
	[ValidationPropertyAttribute("DateValue")]
	public class CreditCardExpirationDate : WebControl
	{
		private MonthDropDown _monthDropDown = null;
		private YearDropDown _yearDropDown = null;

		public string DateValue
		{
			get
			{
				//if (_monthDropDown == null || _yearDropDown == null)
					//return "";
				
				if (!_monthDropDown.IsSelected || !_yearDropDown.IsSelected)
					return "";

				return _yearDropDown.SelectedValue + " / " + _monthDropDown.SelectedValue;
			}
		}

		public int? SelectedMonth
		{
			get
			{
				if (!_monthDropDown.IsSelected)
					return null;

				return _monthDropDown.SelectedMonth;
			}
		}

		public int? SelectedYear
		{
			get
			{
				if (!_yearDropDown.IsSelected)
					return null;

				return _yearDropDown.SelectedYear;
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			_monthDropDown = new MonthDropDown();
			_yearDropDown = new YearDropDown();
			Literal seperator = new Literal();

			_yearDropDown.YearFrom = DateTime.Now.Year;
			_yearDropDown.YearTo = DateTime.Now.AddYears(10).Year;
			_yearDropDown.Width = 70;
			_yearDropDown.EnableBlankSelection = true;
			_monthDropDown.Width = 100;
			_monthDropDown.EnableBlankSelection = true;
			seperator.Text = "&nbsp;/&nbsp;";

			Controls.Add(_monthDropDown);
			Controls.Add(seperator);
			Controls.Add(_yearDropDown);

			if (Page.IsPostBack)
			{
				_monthDropDown.ClearSelection();
				ListItem selectedMonth = _monthDropDown.Items.FindByValue(Page.Request[_monthDropDown.UniqueID]);
				if (selectedMonth != null)
					selectedMonth.Selected = true;
				_yearDropDown.ClearSelection();
				ListItem selectedYear = _yearDropDown.Items.FindByValue(Page.Request[_yearDropDown.UniqueID]);
				if (selectedYear != null)
					selectedYear.Selected = true;
			}
		}

		protected override void  OnPreRender(EventArgs e)
		{

		}

		protected override void RenderContents(HtmlTextWriter output)
		{
			EnsureChildControls();

			foreach (Control currentControl in Controls)
				currentControl.RenderControl(output);
		}
	}
}
