﻿if (netpay == null) { var netpay = new Object() }
if (netpay.webControls == null) { netpay.webControls = new Object() }

netpay.webControls.PageItemHelp =
{
	show: function (anchorElementID, contentElementID)
	{
		/*
		var contentElement = $("#" + contentElementID);
		var anchorElement = $("#" + anchorElementID);
		contentElement.position
		({
			of: anchorElement,
			my: "left top",
			at: "right top",
			offset: "15 0",
			collision: "flip flip"
		});

		contentElement.fadeIn("slow");
		*/

		var contentElement = document.getElementById(contentElementID);
		var anchorElement = document.getElementById(anchorElementID);
		var anchorPosition = netpay.Common.findPosition(anchorElement);
		contentElement.style.top = anchorPosition.top;
		if (clientContext.currentDirection == "ltr")
			contentElement.style.left = anchorPosition.left + 30;
		else
			contentElement.style.left = anchorPosition.left - ($(contentElement).width() + 15);

		$("#" + contentElementID).fadeIn("slow");
	},

	hide: function (contentElementID)
	{
		$("#" + contentElementID).fadeOut("slow");
	}
}