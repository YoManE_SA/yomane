﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:PageItemHelp runat=server></{0}:PageItemHelp>")]
	[ParseChildren(true, "Text")]
	[PersistChildren(false)]
	public class PageItemHelp : CompositeControl
	{
		private string _text = null;

		private Unit _width = Unit.Empty;
		private Unit _height = Unit.Empty;

		public override Unit Height
		{
			get { return _height; }
			set { _height = value; }
		}

		public override Unit Width
		{
			get { return _width; }
			set { _width = value; }
		}

		public string Text
		{
			get { return _text; }
			set { _text = value; }
		}
		
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			// embed scripts
			this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.PageItemHelp.PageItemHelp.js");

			// embedding the css is more tricky
			string includeTemplate = "<link rel='stylesheet' text='text/css' href='{0}' />";
			string includeLocation = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.PageItemHelp.PageItemHelp.css");
			LiteralControl include = new LiteralControl(String.Format(includeTemplate, includeLocation));
			Page.Header.Controls.Add(include);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			string embeddedImageUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.PageItemHelp.IconQuestion.gif");

			writer.Write(string.Format("<img id=\"{0}_imgPageItemHelp\" class=\"pageItemHelp\" onmouseover=\"netpay.webControls.PageItemHelp.show('{0}_imgPageItemHelp', '{0}_divPageItemHelpContent')\" onmouseout=\"netpay.webControls.PageItemHelp.hide('{0}_divPageItemHelpContent')\" src=\"{1}\">", ClientID, embeddedImageUrl));

			string height = _height == Unit.Empty ? "" : string.Format("height:{0};", _height);
			string width = _width == Unit.Empty ? "" : string.Format("width:{0};", _width);
			writer.Write(string.Format("<table id=\"{0}_divPageItemHelpContent\" class=\"pageItemHelpTable\" style=\"{1}{2}\">", ClientID, height, width));
			writer.Write("<tr>");
			writer.Write("<td class=\"pageItemHelpCell\">");
			if (_text != null) writer.Write(_text);
			writer.Write("</td>");
			writer.Write("</tr>");
			writer.Write("</table>");
			
			//writer.Write(string.Format("<div id=\"{0}_divPageItemHelpContent\" class=\"pageItemHelpContent\" style=\"width:{1}; height:{2};\" onmouseout=\"netpay.webControls.PageItemHelp.hide('{0}_divPageItemHelpContent')\">", ClientID, Width, Height));
			//if (_text != null)
				//writer.Write(_text);
			//writer.Write("</div>");
		}
	}
}
