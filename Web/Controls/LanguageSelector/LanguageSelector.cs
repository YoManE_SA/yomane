﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:LanguageSelector runat=server></{0}:LanguageSelector>")]
	public class LanguageSelector : PopupButton
	{
		private List<CultureInfo> _cultures = new List<CultureInfo>();
		public string AlternateDestination { get; set; }
		public bool UsePostback { get; set; }
		public LanguageSelector() { UsePostback = false; }
		public string CulturesDelimited
		{
			set
			{
				string[] culturesSplit = value.Split(',');
				foreach (string currentCultureString in culturesSplit)
					_cultures.Add(new CultureInfo(currentCultureString.Trim()));
			}
		}

		public List<CultureInfo> Cultures
		{
			get { return _cultures; }
			set { _cultures = value; }
		}

		public Language SelectedLanguage
		{
			get
			{
				return Enums.GetLanguage(WebUtils.CurrentCulture);
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
		}

		public override void RenderControl(HtmlTextWriter writer)
		{
			string currentCountryCode = WebUtils.CurrentCultureCountryCode;
			this.Text = string.Format("&nbsp;<span class=\"langSelector\" style=\"text-transform:capitalize;\">{0}</span>&nbsp;<img src=\"/NPCommon/images/ArrowDown.gif\" />",
					(WebUtils.CurrentCulture.IsNeutralCulture ? WebUtils.CurrentCulture.NativeName : WebUtils.CurrentCulture.Parent.NativeName));
            this.IconSrc = string.Format("/NPCommon/ImgCountry/18X12/{0}.gif", currentCountryCode);

			foreach (CultureInfo currentCulture in _cultures)
			{
				if (!currentCulture.Equals(WebUtils.CurrentCulture))
				{
					StringBuilder langBuilder = new StringBuilder();
					string countryCode = null;
					if (currentCulture.ToString().IndexOf("-") > -1) countryCode = currentCulture.ToString().Split('-')[1];
					else countryCode = currentCulture.ToString();
					string locationURL = string.IsNullOrEmpty(AlternateDestination) ? "window.location.href" : "'" + AlternateDestination + "'";
					string nativeName = currentCulture.IsNeutralCulture ? currentCulture.NativeName : currentCulture.Parent.NativeName;

					if (UsePostback)
						langBuilder.Append(string.Format("<div onclick=\"{0}\" class=\"langSelector\" onmouseover=\"this.style.backgroundColor='#d8d8d8';\" onmouseout=\"this.style.backgroundColor='#ffffff';\" style=\"text-align:{1}; direction:{2}; cursor:pointer; border:0px solid #000000; width:100%; height:17px; padding-top:3px; text-transform:capitalize; background-color:#ffffff;\">", Page.ClientScript.GetPostBackClientHyperlink(this, currentCulture.ToString(), false), WebUtils.CurrentAlignment, WebUtils.CurrentDirection));
					else langBuilder.Append(string.Format("<div onclick=\"window.location = netpay.Common.setURLValue(" + locationURL + ", 'culture', '{0}');\" class=\"langSelector\" onmouseover=\"this.style.backgroundColor='#d8d8d8';\" onmouseout=\"this.style.backgroundColor='#ffffff';\" style=\"text-align:{1}; direction:{2}; cursor:pointer; border:0px solid #000000; width:100%; height:17px; padding-top:3px; text-transform:capitalize; background-color:#ffffff;\">", currentCulture.ToString(), WebUtils.CurrentAlignment, WebUtils.CurrentDirection));
                    langBuilder.Append(string.Format("<img border=\"0\" title=\"{1}\" src=\"/NPCommon/ImgCountry/18X12/{0}.gif\">", countryCode, nativeName));
					langBuilder.Append("&nbsp;");
					langBuilder.Append(nativeName);
					langBuilder.Append("</div>");

					Literal literal = new Literal();
					literal.Text = langBuilder.ToString();
					Controls.Add(literal);
				}
			}

			base.RenderControl(writer);
		}
	}
}
