﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:LanguageSelector runat=server></{0}:LanguageSelector>")]
	public class LanguageSelector2 : System.Web.UI.WebControls.WebControl
	{
		private List<CultureInfo> _cultures = new List<CultureInfo>();
		public string AlternateDestination { get; set; }
		public bool UsePostback { get; set; }
		public LanguageSelector2() : base(HtmlTextWriterTag.Div) { UsePostback = false; }

		public string CulturesDelimited
		{
			set
			{
				string[] culturesSplit = value.Split(',');
				foreach (string currentCultureString in culturesSplit)
					_cultures.Add(new CultureInfo(currentCultureString.Trim()));
			}
		}

		public List<CultureInfo> Cultures
		{
			get { return _cultures; }
			set { _cultures = value; }
		}

		public Language SelectedLanguage
		{
			get
			{
				return Enums.GetLanguage(WebUtils.CurrentCulture);
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			string script = 
				"$(function() {\r\n" +
					"$('#" + ClientID + "').click(function () {\r\n" +
						"if ($('#" + ClientID + "_Popup').css('display') == 'none') $('#" + ClientID + "_Popup').show();\r\n" +
						"else $('#" + ClientID + "_Popup').hide();\r\n" +
					"});\r\n" +
					"$(document).mouseup(function () { $('#" + ClientID + "_Popup').hide(); });\r\n" +
				"});";
            //Mouseup textarea false
            //"$('.submenu').mouseup(function () { return false });"
            //"$('.account').mouseup(function () { return false });"
			Page.ClientScript.RegisterClientScriptBlock(GetType(), ClientID, script, true);
			base.OnPreRender(e);
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			string currentCountryCode = WebUtils.CurrentCultureCountryCode;
			writer.Write(string.Format("<a class=\"SelectedText\"><span>" + Resources.LanguageSelector.Language + ":</span>&nbsp;{0}</a>",
					(WebUtils.CurrentCulture.IsNeutralCulture ? WebUtils.CurrentCulture.NativeName : WebUtils.CurrentCulture.Parent.NativeName)));
            //this.IconSrc = string.Format("/NPCommon/ImgCountry/18X12/{0}.gif", currentCountryCode);
			writer.Write("<div id=\"" + ClientID + "_Popup\" style=\"display: none;\"><ul>");
			StringBuilder langBuilder = new StringBuilder();
			foreach (CultureInfo currentCulture in _cultures)
			{
				if (!currentCulture.Equals(WebUtils.CurrentCulture))
				{
					string countryCode = null;
					if (currentCulture.ToString().IndexOf("-") > -1) countryCode = currentCulture.ToString().Split('-')[1];
					else countryCode = currentCulture.ToString();
					string locationURL = string.IsNullOrEmpty(AlternateDestination) ? "window.location.href" : "'" + AlternateDestination + "'";
					string nativeName = currentCulture.IsNeutralCulture ? currentCulture.NativeName : currentCulture.Parent.NativeName;
					if (UsePostback)
                        langBuilder.Append(string.Format("<li onclick=\"{0}\" class=\"langSelector\" onmouseover=\"this.style.backgroundColor='#d8d8d8';\" onmouseout=\"this.style.backgroundColor='#ffffff';\" style=\"text-align:{1}; direction:{2};\">", Page.ClientScript.GetPostBackClientHyperlink(this, currentCulture.ToString(), false), WebUtils.CurrentAlignment, WebUtils.CurrentDirection));
                    else langBuilder.Append(string.Format("<li onclick=\"window.location = netpay.Common.setURLValue(" + locationURL + ", 'culture', '{0}');\" class=\"langSelector\" onmouseover=\"this.style.backgroundColor='#d8d8d8';\" onmouseout=\"this.style.backgroundColor='#ffffff';\" style=\"text-align:{1}; direction:{2};\">", currentCulture.ToString(), WebUtils.CurrentAlignment, WebUtils.CurrentDirection));
                    langBuilder.Append(string.Format("<img border=\"0\" title=\"{1}\" src=\"/NPCommon/ImgCountry/18X12/{0}.gif\">", countryCode, nativeName));
					langBuilder.Append("&nbsp;");
					langBuilder.Append(nativeName);
					langBuilder.Append("</li>");

					//Literal literal = new Literal();
					//literal.Text = langBuilder.ToString();
					//Controls.Add(literal);
				}
			}
			writer.Write(langBuilder.ToString());
			writer.Write("</ul></div>");
			//base.Render(writer);
		}
	}
}
