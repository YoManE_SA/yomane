﻿using System;
using System.Web.UI;
using System.Linq;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:CountryGroupDropDown runat=server></{0}:CountryGroupDropDown>")]
	public class CountryGroupDropDown : DropDownBase
	{
		protected override void OnInit(EventArgs e)
		{
			if (Items.Count == 0)
			{
				DataSource = Bll.CountryGroup.Cache;
				DataValueField = "ID";
				DataTextField = "Name";
				DataBind();
			}

			base.OnInit(e);
		}
	}
}



