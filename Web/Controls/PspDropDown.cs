﻿using System;
using System.Web.UI;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:PspDropDown runat=server></{0}:PspDropDown>")]
	public class PspDropDown : DropDownBase
	{
		protected override void OnInit(EventArgs e)
		{
			if (Items.Count == 0)
			{
				DataSource = GlobalData.GetGroup(GlobalDataGroup.Psp, Language.English);
				DataValueField = "ID";
				DataTextField = "Value";
				DataBind();
			}

			base.OnInit(e);
		}
	}
}
