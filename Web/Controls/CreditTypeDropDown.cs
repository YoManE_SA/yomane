﻿using System;
using System.Web.UI;
using Netpay.Infrastructure;
using System.Linq;

namespace Netpay.Web.Controls
{
	//[ToolboxData("<{0}:TransactionTypeDropDown runat=server></{0}:TransactionTypeDropDown>")]
	public class CreditTypeDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
            DataSource = Bll.Merchants.CreditType.Cache.Where(ct => ct.IsShow);
            DataValueField = "ID";
            if (WebUtils.CurrentLanguage == CommonTypes.Language.Hebrew)
            {
                DataTextField = "NameHebrew";
            } 
            else
            {
                DataTextField = "NameEng";
            }
            base.DataBind(raiseOnDataBinding);
        }
	}
}
