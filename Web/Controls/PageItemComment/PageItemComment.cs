﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
    [ToolboxData("<{0}:PageItemComment runat=server></{0}:PageItemComment>")]
    [ParseChildren(true, "Text")]
    [PersistChildren(false)]
    public class PageItemComment : CompositeControl
    {
        private string _text = null;
        private Unit _width = new Unit(250);
        private Unit _height = new Unit(60);
        private int _horizontalOffset = 30;
        private bool _useNewDesign = false;

        public bool UseNewDesign
        {
            get { return _useNewDesign; }
            set { _useNewDesign = value; }
        }

        public int HorizontalOffset
        {
            get { return _horizontalOffset; }
            set { _horizontalOffset = value; }
        }

        public override Unit Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public override Unit Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // embed scripts
            this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.PageItemComment.PageItemComment.js");

            // embedding the css 
            string includeTemplate = "<link rel='stylesheet' text='text/css' href='{0}' />";
            string includeLocation = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.PageItemComment.PageItemComment.css");
            LiteralControl include = new LiteralControl(String.Format(includeTemplate, includeLocation));
            Page.Header.Controls.Add(include);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            String imageFileNameOn = null;
            String imageFileNameOff = null;
            if (UseNewDesign)
            {
                imageFileNameOn = "note-icon-online.png";
                imageFileNameOff = "note-icon-offline.png";
            }
            else
            {
                imageFileNameOn = "IconCommentOn.gif";
                imageFileNameOff = "IconCommentOff.gif";
            }

            if (Text == null || Text.Trim() == "")
            {
                string image = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.PageItemComment." + imageFileNameOff);
                writer.Write(string.Format("<img id=\"{0}_imgPageItemComment\" class=\"PageItemComment\" src=\"{1}\">", ClientID, image));
            }
            else
            {
                string image = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.PageItemComment." + imageFileNameOn);
                writer.Write(string.Format("<img id=\"{0}_imgPageItemComment\" class=\"PageItemComment\" onmouseover=\"netpay.webControls.PageItemComment.showItemHelp(this, '{0}_divPageItemCommentContent', {2}, '{3}')\" onmouseout=\"netpay.webControls.PageItemComment.hideItemHelp('{0}_divPageItemCommentContent')\" src=\"{1}\">", ClientID, image, _horizontalOffset, WebUtils.CurrentDirection));
                writer.Write(string.Format("<div id=\"{0}_divPageItemCommentContent\" class=\"PageItemCommentContent\" style=\"width:{1}; height:{2};\" onmouseout=\"netpay.webControls.PageItemComment.hideItemHelp('{0}_divPageItemCommentContent')\">{3}</div>", ClientID, Width, Height, Text));
            }
        }
    }
}
