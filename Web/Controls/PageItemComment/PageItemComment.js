﻿if (netpay == null) { var netpay = new Object() }
if (netpay.webControls == null) { netpay.webControls = new Object() }

netpay.webControls.PageItemComment =
{
	showItemHelp: function (anchorElement, contentElementID, horizontalOffset, direction)
	{
		var contentElement = document.getElementById(contentElementID);
		var position = netpay.Common.findPosition(anchorElement);
		if (direction == "rtl")
			horizontalOffset = (horizontalOffset * - 1) - $(contentElement).width();
		position.left += horizontalOffset;

		contentElement.style.top = position.top;
		contentElement.style.left = position.left;
		$("#" + contentElementID).fadeIn("fast");
	},

	hideItemHelp: function (contentElementID)
	{
		$("#" + contentElementID).fadeOut("fast");
	}
}