﻿using System;
using System.Web.UI;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:PartnerDropDown runat=server></{0}:PartnerDropDown>")]
	public class PartnerDropDown : DropDownBase
	{
		protected override void OnInit(EventArgs e)
		{
			if (Items.Count == 0)
			{
				DataSource = Netpay.Bll.Affiliates.Affiliate.Search(null, null);
				DataValueField = "ID";
				DataTextField = "Name";
				DataBind();
			}

			base.OnInit(e);
		}
	}
}



