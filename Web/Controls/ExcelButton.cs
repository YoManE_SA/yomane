﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Resources;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll.Reports;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Tasks;
using Netpay.Bll;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:ExcelButton runat=\"server\" />")]
	public class ExcelButton : LinkButton
	{
		public class ExportEventArgs : EventArgs { }
		public int ResultCount { get; set; }
		public string ReportHeader { get; private set; }
		public ReportType Type { get; private set; }
		public Func<ISortAndPage, ICollection> DataFunction { get; private set; }
		public string IconImageOn { get; set; }
		public string IconImageOff { get; set; }
		public ExcelButton()
		{
			IconImageOn = "xls.gif";
			IconImageOff = "xls_off.gif";
		}

		protected override void OnClick(EventArgs e) 
		{
			base.OnClick(new ExportEventArgs()); 
		}
		
		protected override void CreateChildControls()
		{
			Image imgExcel = new Image();
			imgExcel.ID = "imgExcel";
			imgExcel.ImageAlign = ImageAlign.TextTop;
			imgExcel.Attributes.Add("style", "margin:0px 5px;");
			imgExcel.Attributes.Add("align", "top");
			this.Controls.Add(imgExcel);

			Literal litExcel = new Literal();
			litExcel.ID = "litExcel";
			this.Controls.Add(litExcel);
		}

		public void SetExportInfo(string reportHeader, ReportType reportType, Func<ISortAndPage, ICollection> dataFunction)
		{
			ReportHeader = reportHeader;
			Type = reportType;
			DataFunction = dataFunction;
		}

		public void Export() { Export(ReportHeader, Type, DataFunction); }

		public void Export(string exportHeader, ReportType reportType, Func<ISortAndPage, ICollection> dataFunction)
		{
			var sortAndPage = new SortAndPage(0, 1, null, false) { };
			dataFunction.Invoke(sortAndPage);

			if (exportHeader == null) 
				exportHeader = reportType.ToString();

			if (sortAndPage.RowCount < Infrastructure.Application.MaxImmediateReportSize) 
			{
				Page.Response.ClearHeaders();
				Page.Response.Clear();
				Page.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
				Page.Response.AddHeader("Content-Disposition", "attachment;filename=" + reportType.ToString() + ".xlsx");
				Page.Response.Cache.SetCacheability(HttpCacheability.Private);
				sortAndPage.PageSize = sortAndPage.RowCount;
				ICollection data = dataFunction.Invoke(sortAndPage);

                var generator = new ReportGeneratorExcel();
                var result = generator.Generate(reportType, data, null, exportHeader);
                Page.Response.BinaryWrite(result);
				Page.Response.End();
			}
            else if (sortAndPage.RowCount < Infrastructure.Application.MaxFiledReportSize)
			{
				List<Object> args = new List<Object>();
				args.Add(exportHeader);
				args.Add(reportType);
				args.Add(sortAndPage.DataFunction != null ? sortAndPage.DataFunction : dataFunction);

				Task task = new Task(WebUtils.LoggedUser, Task.GetUserGroupID(WebUtils.LoggedUser), "Exported Report", LogTag.Reports, "Netpay.Bll", typeof(Netpay.Bll.Reports.FiledReports).ToString(), "ExportTask", args);
				if (!Netpay.Infrastructure.Application.Queue.IsTaskQueued(task))
					Netpay.Infrastructure.Application.Queue.Enqueue(task);
				
				Page.Response.Redirect("Reports.aspx");
			}
		}

		public override void RenderControl(HtmlTextWriter writer)
		{
			Literal litExcel = (Literal)this.FindControl("litExcel");
			litExcel.Text = Text;

			Image imgExcel = (Image)this.FindControl("imgExcel");
			imgExcel.AlternateText = Text;
			imgExcel.ToolTip = Text;
            imgExcel.ImageUrl = "/NPCommon/ImgFileExt/14X14/" + (ResultCount > 0 ? IconImageOn : IconImageOff);

			this.CssClass = "ExportExcelTxt";
			this.Attributes.Add("style", "margin-bottom:0;");

			// limits
			if (ResultCount == 0) 
			{
				this.Enabled = false;
			}
            else if (ResultCount > Infrastructure.Application.MaxFiledReportSize)
			{
				this.Enabled = true;
				this.Attributes.Remove("href");
				this.Attributes.Add("href", "javascript://");

				ResourceManager rm = WebUtils.GetResource("ExcelButton");
                string message = string.Format(rm.GetString("FiledOverLimit"), Infrastructure.Application.MaxFiledReportSize);
				this.OnClientClick = "netpay.Common.alert(\"" + message + "\");";
			}
            else if (ResultCount > Infrastructure.Application.MaxImmediateReportSize) 
			{
				this.Enabled = true;
				this.Attributes.Remove("href");
				this.Attributes.Add("href", "javascript://");

				ResourceManager rm = WebUtils.GetResource("ExcelButton");
                string alertHtml = string.Format(rm.GetString("OverLimit"), Infrastructure.Application.MaxImmediateReportSize) + 
				"<br/><br/>" +
				string.Format("<a href=\\\"" + Page.ClientScript.GetPostBackClientHyperlink(this, "") + "\\\">{0}</a>", rm.GetString("Continue"));
				this.OnClientClick = "netpay.Common.alert(\"" + alertHtml + "\");";
			} 
			else 
			{	
				this.Enabled = true;			
				this.Attributes.Remove("href");
				this.Attributes.Add("href", Page.ClientScript.GetPostBackClientHyperlink(this, ""));
				this.OnClientClick = "netpay.Blocker.enabled=false;";
			}

			base.RenderControl(writer);
		}
	}
}