﻿
namespace Netpay.Web.Controls
{

	public enum FormatAddressStyle
	{
		MultipleLines,
		SingleLine,
		Table
	}

	public static class Format
	{
		public static string FormatAddress(Bll.IAddress address, FormatAddressStyle style)
		{
			if (address == null) return string.Empty;

			string address1 = address.AddressLine1, address2 = address.AddressLine2, city = address.City, zip = address.PostalCode;
			var country = Bll.Country.Get(address.CountryISOCode);
			var state = Bll.State.Get(address.StateISOCode);

			string result = string.Empty;
			if (!string.IsNullOrEmpty(address1)) result += "<div>" + address1 + "</div>";
			if (!string.IsNullOrEmpty(address2)) result += "<div>" + address2 + "</div>";
			if (!string.IsNullOrEmpty(city))
			{
				result += "<div>" + city;
				if (state == null && !string.IsNullOrEmpty(zip)) result += " " + zip;
				result += "</div>";
			}
			if (state != null)
			{
				result += "<div>" + state.IsoCode;
				if (!string.IsNullOrEmpty(zip)) result += " " + zip;
				result += "</div>";
			}
            if (country != null) result += "<div>" + Web.WebUtils.GetCountryName(country.IsoCode2) + "</div>";

			if (style==FormatAddressStyle.SingleLine)
				result = result.Replace("</div><div>", ", ").Replace("</div>", string.Empty).Replace("<div>", string.Empty);
			else if (style == FormatAddressStyle.Table)
				result = "<table>" + result.Replace("</div>", "</td></tr>").Replace("<div>", "<tr><td>") + "</table>";
			return result;
		}

		public static string FormatBankAccount(Bll.Accounts.BankAccount account, FormatAddressStyle style)
		{
			string result = string.Empty;
			if (!string.IsNullOrEmpty(account.AccountName)) result += "<div>" + account.AccountName + "</div>";
			if (!string.IsNullOrEmpty(account.AccountNumber)) result += "<div>" + account.AccountNumber + "</div>";
			if (!string.IsNullOrEmpty(account.IBAN)) result += "<div>" + account.IBAN + "</div>";
			if (!string.IsNullOrEmpty(account.BankName)) result += "<div>" + account.BankName + "</div>";
			if (!string.IsNullOrEmpty(account.BankStreet1)) result += "<div>" + account.BankStreet1 + "</div>";
			if (!string.IsNullOrEmpty(account.BankStreet2)) result += "<div>" + account.BankStreet2 + "</div>";
			if (!string.IsNullOrEmpty(account.BankCity)) {
				result += "<div>" + account.BankCity; 
				if (!string.IsNullOrEmpty(account.BankPostalCode)) result += ", " + account.BankCity;
				result += "</div>";
			}
			var country = Bll.Country.Get(account.BankCountryISOCode);
			if (country != null)
			{
				result += "<div>";
				if (!string.IsNullOrEmpty(account.BankStateISOCode)) result += account.BankStateISOCode + ", ";
				result += country.Name + "</div>";
			}

			if (style==FormatAddressStyle.SingleLine)
				result = result.Replace("</div><div>", ", ").Replace("</div>", string.Empty).Replace("<div>", string.Empty);
			else if (style == FormatAddressStyle.Table)
				result = "<table>" + result.Replace("</div>", "</td></tr>").Replace("<div>", "<tr><td>") + "</table>";
			return result;
		}
	}
}
