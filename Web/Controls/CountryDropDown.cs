﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Linq;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:CountryDropDown runat=server></{0}:CountryDropDown>")]
	public class CountryDropDown : DropDownBase
	{
		public CountryDropDown()
		{
			DataTextField = "Name";
			DataValueField = "ID";
		}
		
		public Bll.Country SelectedCountry
		{
			get
			{
				if (!IsSelected)
					return null;

                int selectedCountryID;
                bool result = int.TryParse(SelectedValue, out selectedCountryID);
                if (!result)
                    return null;

				return Bll.Country.Get(selectedCountryID);
			}
			set 
			{
				if (value == null) 
				{ 
					SelectedCountryID = null; 
					return; 
				}
				
				SelectedCountryID = value.ID;
			}
		}

		public string ISOCode
		{
			get{
				if (SelectedCountryID == null) return null;
				return Bll.Country.Get(SelectedCountryID.Value).IsoCode2;
			}
			set 
			{
				var country = Bll.Country.Get(value);
				if(country != null) 
					SelectedCountryID = country.ID; 
			}
		}

		public int? SelectedCountryID
		{
			get
			{
				if (!IsSelected) return null;
				return Value.ToNullableInt();
			}
			set
			{
				ClearSelection();
                if (value == null) return;
                Value = value.ToString();
                ListItem item = Items.FindByValue(value.Value.ToString());
				if (item != null) item.Selected = true;
			}
		}

        public void Populate() { DataBind(false); }

		protected override void DataBind(bool raiseOnDataBinding)
        {
            var countries = Bll.Country.Cache.ToList();
            foreach (var country in countries)
                country.Name = WebUtils.GetCountryName(country.IsoCode2);
			countries = countries.OrderBy(c => c.Name).ToList();
            DataSource = countries;
			base.DataBind(raiseOnDataBinding);	
        }

 		protected override void OnInit(EventArgs e)
		{
            if (Items.Count == 0) Populate();
			base.OnInit(e);
		}
	}
}
