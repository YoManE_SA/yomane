﻿using System;
using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:DatePicker runat=server></{0}:DatePicker>")]
    public class DatePicker : WebControl
	{
		private DateTime? _date = null;
        private bool _showMonth = false;
        private bool _showYear = false;
		private CultureInfo _culture = new CultureInfo("en-gb");

		public DatePicker()
		{
            _date = null; // DateTime.Now;
		}

        public bool ShowMonth 
        {
            get { return _showMonth; }
            set { _showMonth = value; }
        }

        public bool ShowYear
        {
            get { return _showYear; }
            set { _showYear = value; }
        }

		public CultureInfo Culture 
		{
			get 
			{
				return _culture;
			}
			set 
			{
				_culture = value;
			}
		}

		public DateTime? Date
		{
			get
			{
				DateTime parsed;
                if (DateTime.TryParse(Context.Request[ClientID + "_datePicker"], Culture, DateTimeStyles.None, out parsed))
					_date = parsed;

                return _date;
			}
            set { _date = value; }
		}

		public bool IsDateSelected
		{
			get
			{
                return Context.Request[ClientID + "_datePicker"] != null && Context.Request[ClientID + "_datePicker"].Trim() != "";
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.DatePicker.DatePicker.js");
			base.OnPreRender(e);
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
            string date = null;
			if (Date != null)
				date = Date.Value.ToString(Culture.DateTimeFormat.ShortDatePattern);
			else
				date = "";

            writer.Write(string.Format("<input id=\"{0}_datePicker\" name=\"{0}_datePicker\"  value=\"{1}\" size=\"10\" type=\"text\" />", ClientID, date));
            writer.Write(string.Format("<script>netpay.webControls.DatePicker.init('{0}', {1}, {2}, '{3}')</script>", ClientID, _showMonth.ToString().ToLower(), _showYear.ToString().ToLower(), Culture.DateTimeFormat.ShortDatePattern.ToLower().Replace("yyyy", "yy")));
		}
	}
}
