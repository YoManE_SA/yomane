﻿if (netpay == null) { var netpay = new Object() }
if (netpay.webControls == null) { netpay.webControls = new Object() }

netpay.webControls.DatePicker =
{
    init: function (baseId, showMonth, showYear, format)
    {
    	$("#" + baseId + "_datePicker").datepicker({ dateFormat: format, changeMonth: showMonth, changeYear: showYear, yearRange: "-100:+0" });
	}
}