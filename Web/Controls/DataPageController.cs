﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
	public class DataPageController : System.Web.UI.WebControls.CompositeControl
	{
		public enum ViewMode { SearchMode = 0, EditMode = 1 }
		public System.Web.UI.WebControls.Unit ContentWidth { get; set; }
		public System.Web.UI.WebControls.Unit FilterWidth { get; set; }
		public ViewMode Mode
		{
			get { return (FormView.Visible) ? ViewMode.EditMode : ViewMode.SearchMode; }
			set
			{
				//FindControl("ListView").Visible = value == ViewMode.SearchMode;
				FormView.Visible = (value == ViewMode.EditMode);
			}
		}
		
		[System.Web.UI.ParseChildren(false)]
		public class ControlContainer : System.Web.UI.WebControls.WebControl, System.Web.UI.INamingContainer
		{
			public ControlContainer() : base(System.Web.UI.HtmlTextWriterTag.Div) { }
		}

		public DataPageController()
        {
            ContentWidth = System.Web.UI.WebControls.Unit.Empty; //.Percentage(80);
            Width = System.Web.UI.WebControls.Unit.Percentage(100);
            Height = System.Web.UI.WebControls.Unit.Percentage(100);
            Attributes["cellspacing"] = "0";
			Controls.Add(new ControlContainer() { ID = "FilterView" });
            Controls.Add(new ControlContainer() { ID = "ListView" });
            Controls.Add(new ControlContainer() { ID = "FormView" });
            //Controls.Add(new ControlContainer() { ID = "ReadView" });
			Mode = ViewMode.SearchMode;
        }
		
		protected override System.Web.UI.HtmlTextWriterTag TagKey
		{
			get { return System.Web.UI.HtmlTextWriterTag.Table; }
		}

		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
		public ControlContainer FilterView { get { return FindControl("FilterView") as ControlContainer; } }

		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
		public ControlContainer ListView { get { return FindControl("ListView") as ControlContainer; } }

		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
		public ControlContainer FormView { get { return FindControl("FormView") as ControlContainer; } }

		protected override void OnPreRender(EventArgs e)
		{
			if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "DataPageController"))
			{
				var script = 
					"function DataPageController_Command(objId, viewName, show){" +
					"	var filterView = document.getElementById(objId + '_FilterTd');" +
					"	var listView = document.getElementById(objId + '_ListTd');" +
					"	var contentView = document.getElementById(objId + '_ContentTd');" +
					"	var viewObj = document.getElementById(objId + '_' + viewName + 'Td');" +
					"	if (show == null) show = !$(viewObj).is(':visible');" +
					"	if (show) $(viewObj).show('fade'); else $(viewObj).hide('fade'); " + 
					"}";
					Page.ClientScript.RegisterClientScriptBlock(GetType(), "DataPageController", script, true);
			}
 			base.OnPreRender(e);
		}

		protected override void RenderContents(System.Web.UI.HtmlTextWriter response)
		{
			response.Write("<tr>");
			response.Write("  <td valign=\"top\" id=\"" + ClientID + "_FilterTd\" width=\"" + (FilterWidth != null ? FilterWidth.ToString() : "") + "\" class=\"dataPageFilter\" style=\"display:" + (Mode != ViewMode.SearchMode ? "none" : "") + ";\">");
			FilterView.RenderControl(response);
			response.Write("  </td>");
			response.Write("  <td valign=\"top\" class=\"dataPageSeperator\"><img id=\"" + ClientID + "_FilterTdSeperatorImage\" style=\"margin-top:150px;\" src=\"" + Page.ResolveUrl("~/Images/DataTableSize.jpg") + "\" onclick=\"DataPageController_Command('" + ClientID + "', 'Filter', null);\" /></td>");

			response.Write("  <td valign=\"top\" id=\"" + ClientID + "_ListTd\" width=\"" + (ContentWidth != null ? ContentWidth.ToString() : "") + "\" class=\"dataPageData\">");
			ListView.RenderControl(response);
			response.Write("  </td>");

			if (Mode != ViewMode.SearchMode) {
				response.Write("  <td valign=\"top\" class=\"dataPageSeperator\"><img id=\"" + ClientID + "_ContentTdSeperatorImage\" style=\"margin-top:100px;\" src=\"" + Page.ResolveUrl("~/Images/DataTableSize.jpg") + "\" onclick=\"DataPageController_Command('" + ClientID + "', 'Content', null);\" /></td>");
				response.Write("  <td valign=\"top\" id=\"" + ClientID + "_ContentTd\" width=\"" + (Mode != ViewMode.EditMode ? "50%" : "100%") + "\" class=\"dataPageeData\">");
				FormView.RenderControl(response);
				response.Write("  </td>");
			}            
			response.Write("</tr>");
		}
	}
}
