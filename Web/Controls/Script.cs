﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
	public class Script : System.Web.UI.Control
	{
		public string Src { get; set; }
		public string Type { get; set; }
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			writer.Write("<script src=\"{0}\" type=\"{1}\"></script>", ResolveClientUrl(Src), Type);
		}
	}
}
