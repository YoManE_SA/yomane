﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:TransactionTypeDropDown runat=server></{0}:TransactionTypeDropDown>")]
	public class RefundRequestStatusDropDown : EnumDropDown
	{
		public bool ShowClient { get; set; }

		public RefundRequestStatusDropDown()
		{
			base.ViewEnumName = typeof(Bll.Transactions.RefundRequest.RequestStatus).FullName;
		}

		protected override void DataBind(bool raiseOnDataBinding)
		{
			base.DataBind(raiseOnDataBinding);
			if (ShowClient) {
				Items.Remove(Items.FindByValue(((int)Bll.Transactions.RefundRequest.RequestStatus.SourceCancel).ToString()));
				Items.Remove(Items.FindByValue(((int)Bll.Transactions.RefundRequest.RequestStatus.Batched).ToString()));
				Items.Remove(Items.FindByValue(((int)Bll.Transactions.RefundRequest.RequestStatus.InProgress).ToString()));
			}
		}
	}
}