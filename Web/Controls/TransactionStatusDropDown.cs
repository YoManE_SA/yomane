﻿using System;
using System.Web.UI;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:TransactionStatusDropDown runat=server></{0}:TransactionStatusDropDown>")]
	public class TransactionStatusDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			String[] names = PaymentsStatus.GetNames(typeof(PaymentsStatus));
			TransactionStatus[] values = (TransactionStatus[])Enum.GetValues(typeof(TransactionStatus));
			var lst = new System.Collections.Generic.Dictionary<int, string>();
			for (int i = 1; i < names.Length; i++)
				lst.Add((int)values[i], names[i].ToString());

			DataValueField = "Key";
			DataTextField = "Value";
			base.DataBind(raiseOnDataBinding);
		}
	}
}
