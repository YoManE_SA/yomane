﻿using System;
using System.Web.UI;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
    public class BankAccountDetailsDropDown : DropDownBase
    {
        protected override void DataBind(bool raiseOnDataBinding)
        {
            var bankAccountDetails = Bll.DebitCompanies.BankAccountDetails.Cache;
            bankAccountDetails.Sort((i, j) => {
                if (i.BA_BankName == j.BA_BankName)
                    return i.BA_AccountName.CompareTo(j.BA_AccountName);
                else
                    return i.BA_BankName.CompareTo(j.BA_BankName);
            });
            DataSource = bankAccountDetails;
            DataValueField = "BA_ID";
            DataTextField = "Title";
            base.DataBind(raiseOnDataBinding);
        }
    }
}
