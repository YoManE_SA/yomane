﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
	public class TerminalDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			base.DataValueField = "TerminalNumber"; base.DataTextField = "Name";
            var sf = new Bll.DebitCompanies.Terminal.SearchFilters();
            sf.IsActive = true;
			DataSource = Bll.DebitCompanies.Terminal.Search(sf, null);
			base.DataBind(raiseOnDataBinding);
		}
	}
}
