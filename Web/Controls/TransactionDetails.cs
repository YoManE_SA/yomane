﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using System.Resources;
using Netpay.Bll;
using Netpay.Bll.Risk;
using System.Web;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:TransactionDetails runat=server></{0}:TransactionDetails>")]
	public class TransactionDetails : WebControl, IPostBackEventHandler
	{
		private Bll.Transactions.Transaction _transaction = null;
		private List<Bll.Transactions.History> _history = null;
        private string _backgroundColor = "#efefef";
        private bool _emailSent = false;
		private bool _showFees = false;
		private bool _showAmounts = false;
		private bool _showMerchantDetails = false;
		private bool _showMerchantTransactionInfo = false;
		private bool _showCustomerTransactionInfo = false;

		public bool ShowMerchantTransactionInfo
		{
			get { return _showMerchantTransactionInfo; }
			set { _showMerchantTransactionInfo = value; }
		}

		public bool ShowCustomerTransactionInfo
		{
			get { return _showCustomerTransactionInfo; }
			set { _showCustomerTransactionInfo = value; }
		}

		public bool ShowMerchantDetails
		{
			get { return _showMerchantDetails; }
			set { _showMerchantDetails = value; }
		}

		public bool ShowFees
		{
			get { return _showFees; }
			set { _showFees = value; }
		}

		public bool ShowAmounts
		{
			get { return _showAmounts; }
			set { _showAmounts = value; }
		}

		public string BackgroundColor
		{
			get { return _backgroundColor; }
			set { _backgroundColor = value; }
		}

		public Bll.Transactions.Transaction Transaction
		{
			get { return _transaction; }
			set
			{
				_transaction = value;
			}
		}

		public List<Bll.Transactions.History> History
		{
			get 
			{
				if (Transaction == null)
					return null;
				if (_history == null)
					_history = Bll.Transactions.History.GetTransactionHistory(Transaction.ID, Transaction.Status);

				return _history; 
			}
		}

		private string GetOptionalField(string title, string value)
		{
			if (value == null || value.Trim() == "")
				return "";

			return string.Format("<span class=\"SmallHeadings\">{0}:</span> {1}<br/>", title, value);
		}

        protected string GetEmailField(string title, string value)
        {
            if (value == null || value.Trim() == "") return "";
            string sendLink = null;
            if (_emailSent) sendLink = string.Format("<br/><img title=\"{0}\" src=\"/NPCommon/Images/icon_sendMaildim.png\" align=\"middle\" /> {0}", WebUtils.GetResource("Common").GetString("SendConfirmEmail"));
			else sendLink = string.Format("<br/><a href=\"{0}\"><img title=\"{1}\" src=\"/NPCommon/Images/icon_sendMail.png\" align=\"middle\" /> {1}</a>", Page.ClientScript.GetPostBackClientHyperlink(this, "SendClientEmail"), WebUtils.GetResource("Common").GetString("SendConfirmEmail"));
            return string.Format("<span class=\"SmallHeadings\">{0}:</span> {1} {2}<br/>", title, value, sendLink);
        }

        private string GetOptionalField(string title, decimal value)
		{
			if (value == 0)
				return "";

			return string.Format("<span class=\"SmallHeadings\">{0}:</span> {1}<br/>", title, value);
		}

		private string GetOptionalField(string title, string currencySymbol, decimal value)
		{
			if (value == 0)
				return "";

			return string.Format("<span class=\"SmallHeadings\">{0}:</span> {1}<br/>", title, value.ToAmountFormat(currencySymbol));
		}

		protected string GetStateName()
		{
			if (Transaction.PaymentData == null)
				return "";
			if (Transaction.PaymentData.BillingAddress == null)
				return "";
			var state = State.Get(_transaction.PaymentData.BillingAddress.StateISOCode);
			if (state == null) return "";

			return WebUtils.GetStateName(state.IsoCode);
		}

		protected string GetCountryName()
		{
			if (Transaction.PaymentData == null)
				return "";
			if (Transaction.PaymentData.BillingAddress == null) return "";
			var country = Country.Get(_transaction.PaymentData.BillingAddress.CountryISOCode);
			if (country == null)
				return "";

			return WebUtils.GetCountryName(country.IsoCode2);
		}

		protected string GetBinCountryName()
		{
			if (Transaction.PaymentData == null) return "";
			if (string.IsNullOrEmpty(Transaction.PaymentData.IssuerCountryIsoCode)) return "";
			var country = Country.Get(_transaction.PaymentData.IssuerCountryIsoCode);
			if (country == null) return "";
            return WebUtils.GetCountryName(country.IsoCode2);
		}

		public string MoneyTransferText()
		{
			ResourceManager rmCommon = WebUtils.GetResource("Common");
			string functionReturnValue = null;

			if (_transaction.Status != TransactionStatus.Captured)
				return "";
			if (WebUtils.LoggedUser.Role != Infrastructure.Security.UserRole.Merchant)
				return "";
			if (_transaction.EpaList == null)
				return rmCommon.GetString("MoneyTransferNoRecords");
			if (_transaction.EpaList.Count == 0)
				return rmCommon.GetString("MoneyTransferNoRecords");

			if (_transaction.Installments == 1)
			{
				if (_transaction.EpaList[0].IsPaid & _transaction.EpaList[0].IsRefunded)
				{
					functionReturnValue = rmCommon.GetString("MoneyTransferPaidAndRefunded") + " (" + _transaction.EpaList[0].PaidInsertDate.ToShortDateString() + ", " + _transaction.EpaList[0].RefundedInsertDate.ToShortDateString() + ")";
				}
				else if (_transaction.EpaList[0].IsPaid)
				{
					functionReturnValue = rmCommon.GetString("MoneyTransferPaid") + " (" + _transaction.EpaList[0].PaidInsertDate.ToShortDateString() + ")";
				}
				else if (_transaction.EpaList[0].IsRefunded)
				{
					functionReturnValue = rmCommon.GetString("MoneyTransferRefunded") + " (" + _transaction.EpaList[0].RefundedInsertDate.ToShortDateString() + ")";
				}
				else
				{
					functionReturnValue = "";
				}
			}
			else
			{
				functionReturnValue = "";
				string temp = null;
				foreach (var record in _transaction.EpaList)
				{
					if (record.IsPaid & record.IsRefunded)
					{
						temp = rmCommon.GetString("MoneyTransferPaidAndRefunded") + " (" + _transaction.EpaList[0].PaidInsertDate.ToShortDateString() + ", " + _transaction.EpaList[0].RefundedInsertDate.ToShortDateString() + ")";
					}
					else if (record.IsPaid)
					{
						temp = rmCommon.GetString("MoneyTransferPaid") + " (" + _transaction.EpaList[0].PaidInsertDate.ToShortDateString() + ")";
					}
					else if (record.IsRefunded)
					{
						temp = rmCommon.GetString("MoneyTransferRefunded") + " (" + _transaction.EpaList[0].RefundedInsertDate.ToShortDateString() + ")";
					}
					else
					{
						temp = "";
					}
					if (!string.IsNullOrEmpty(temp))
					{
						temp = record.Installment.ToString() + rmCommon.GetString("MoneyTransferInstallmentOf") + _transaction.Installments + ": " + temp;
						functionReturnValue += "<div> &nbsp; &nbsp; &nbsp; " + rmCommon.GetString("MoneyTransferInstallment") + " " + temp + "</div>";
					}
				}
			}

			return functionReturnValue;
		}

		private string ReplyDescription
		{
			get
			{
				if (!string.IsNullOrEmpty(Transaction.ReplyDescriptionText)) 
					return Transaction.ReplyDescriptionText;

				if (Transaction.ReplyDescription == null)
					return "";

				if (WebUtils.IsLoggedin && (WebUtils.LoggedUser.Role == Infrastructure.Security.UserRole.Merchant))
				{
					if (WebUtils.CurrentLanguage == Language.Hebrew)
						return Transaction.ReplyDescription.DescriptionMerchantHeb;
					else
						return Transaction.ReplyDescription.DescriptionMerchantEng;
				}
				else
				{
					if (WebUtils.CurrentLanguage == Language.Hebrew)
						return Transaction.ReplyDescription.DescriptionCustomerHeb;
					else
						return Transaction.ReplyDescription.DescriptionCustomerEng;
				}
			}
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (Transaction == null)
			{
				writer.Write("No transaction provided.");
				return;
			}

			ResourceManager rmCommon = WebUtils.GetResource("Common");
			ResourceManager rmTransactionDetails = WebUtils.GetResource("TransactionDetails");
			ResourceManager rmTransactionAmounts = WebUtils.GetResource("TransactionAmountType");

			writer.Write(string.Format("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" style=\"padding: 12px 10px; direction:{1}; text-align:{2}; \">", BackgroundColor, WebUtils.CurrentDirection, WebUtils.CurrentAlignment));
			writer.Write("<tbody>");

			// declined line
			if (Transaction.Status == TransactionStatus.Declined)
			{
				writer.Write("<tr>");
				writer.Write("<td>");
				writer.Write("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
				writer.Write("<tr>");
				writer.Write(string.Format("<td class=\"MainHeadings\" valign=\"top\">{0}<br /></td>", rmTransactionDetails.GetString("ReasonForDecline")));
				writer.Write("<td style=\"width: 10px;\"></td>");
				writer.Write("<td valign=\"top\" style=\"color: maroon;\">");
				writer.Write(string.Format("<span style=\"color:maroon;\">{0}</span> - {1}", Transaction.ReplyCode, ReplyDescription));
				writer.Write("</td>");
				writer.Write("</tr>");
				writer.Write("</table>");
				writer.Write("</td>");
				writer.Write("</tr>");
				writer.Write("<tr><td style=\"height: 12px;\"></td></tr>");
			}

			writer.Write("<tr>");
			writer.Write("<td valign=\"top\">");
			writer.Write("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">");
			writer.Write("<tbody>");
			writer.Write("<tr>");

			// transaction info, merchant
			if (_showMerchantTransactionInfo)
			{
				writer.Write("<td valign=\"top\">");
				writer.Write(string.Format("<div class=\"MainHeadings\">{0}</div>", rmTransactionDetails.GetString("TransactionInfo")));
				writer.Write(GetOptionalField(rmCommon.GetString("AuthorizationCode"), Transaction.ApprovalNumber));
				writer.Write(GetOptionalField(rmCommon.GetString("IPAddress"), Transaction.IP));
				writer.Write(GetOptionalField(rmCommon.GetString("CreditType"), rmCommon.GetString(Transaction.CreditTypeText)));
				writer.Write(GetOptionalField(rmCommon.GetString("Installments"), Transaction.Installments > 1 ? Transaction.Installments.ToString() : ""));
				writer.Write(GetOptionalField(rmCommon.GetString("OrderNumber"), Transaction.OrderNumber));
                writer.Write(GetOptionalField(rmCommon.GetString("BankReferenceNumber"), Transaction.AcquirerReferenceNum));
				writer.Write(GetOptionalField(rmCommon.GetString("MoneyTransfer"), MoneyTransferText()));
				writer.Write(GetOptionalField(rmCommon.GetString("PayFor"), Transaction.PayForText));
				writer.Write("</td>"); 
			}

			// transaction info, customer
			if (_showCustomerTransactionInfo)
			{
				writer.Write("<td valign=\"top\">");
				writer.Write(string.Format("<div class=\"MainHeadings\">{0}</div>", rmTransactionDetails.GetString("TransactionInfo")));
				writer.Write(GetOptionalField(rmCommon.GetString("Installments"), Transaction.Installments > 1 ? Transaction.Installments.ToString() : ""));
				writer.Write(GetOptionalField(rmCommon.GetString("OrderNumber"), Transaction.OrderNumber));
				writer.Write(GetOptionalField(rmCommon.GetString("PayFor"), Transaction.PayForText));
				writer.Write("</td>");
			}

			if (Transaction.PayerData != null)
			{
				writer.Write("<td valign=\"top\">");
				writer.Write(string.Format("<div class=\"MainHeadings\">{0}</div>", rmTransactionDetails.GetString("CardHolderDetails")));
				writer.Write(GetOptionalField(rmCommon.GetString("Name"), Transaction.PayerData.FullName));
				writer.Write(GetOptionalField(rmCommon.GetString("PersonalNum"), Transaction.PayerData.PersonalNumber));
				writer.Write(GetOptionalField(rmCommon.GetString("Phone"), Transaction.PayerData.PhoneNumber));
				writer.Write(GetEmailField(rmCommon.GetString("EMail"), Transaction.PayerData.EmailAddress));
				writer.Write("</td>");
			}
			// payment method info
			if (Transaction.PaymentData != null)
			{
				if (Transaction.PaymentData.MethodInstance.PaymentMethodType == PaymentMethodType.CreditCard)
				{
					writer.Write("<td valign=\"top\">");
					writer.Write(string.Format("<div class=\"MainHeadings\">{0}</div>", rmTransactionDetails.GetString("CCDetails")));
					writer.Write(GetOptionalField(rmCommon.GetString("Expiration"), Transaction.PaymentData.MethodInstance.ExpirationDate.Value.ToString("MM/yyyy")));
					writer.Write(GetOptionalField(rmCommon.GetString("Type"), Transaction.PaymentMethodName));
					writer.Write(GetOptionalField(rmCommon.GetString("Country"), GetBinCountryName()));
					writer.Write("</td>");
				}

				if (Transaction.PaymentData.MethodInstance.PaymentMethodType == PaymentMethodType.ECheck)
				{
					writer.Write("<td valign=\"top\">");
					writer.Write(string.Format("<div class=\"MainHeadings\">{0}</div>", rmTransactionDetails.GetString("ECDetails")));
					writer.Write(GetOptionalField(rmTransactionDetails.GetString("AccountNumber"), Transaction.PaymentData.MethodInstance.Display));
					//writer.Write(GetOptionalField(rmTransactionDetails.GetString("RoutingNumber"), Transaction.PaymentData.RoutingNumber));
					//writer.Write(GetOptionalField(rmCommon.GetString("BankName"), Transaction.PaymentData.BankName));
					writer.Write(GetOptionalField(rmCommon.GetString("BankCountry"), Transaction.PaymentData.IssuerCountryIsoCode));
					//writer.Write(GetOptionalField(rmCommon.GetString("BankCity"), Transaction.PaymentData.BankCity));
					//writer.Write(GetOptionalField(rmCommon.GetString("BankState"), Transaction.PaymentData.BankState));
					//writer.Write(GetOptionalField(rmCommon.GetString("BankPhone"), Transaction.PaymentData.BankPhone));
					writer.Write("</td>");
				}

				// billing address
				if (Transaction.PaymentData.BillingAddress != null)
				{
					writer.Write("<td valign=\"top\">");
					writer.Write(string.Format("<div class=\"MainHeadings\">{0}</div>", rmTransactionDetails.GetString("BillingAddress")));
					writer.Write(GetOptionalField(rmCommon.GetString("Country"), GetCountryName()));
					writer.Write(GetOptionalField(rmCommon.GetString("State"), GetStateName()));
					writer.Write(GetOptionalField(rmCommon.GetString("City"), Transaction.PaymentData.BillingAddress.City));
					writer.Write(GetOptionalField(rmCommon.GetString("ZipCode"), Transaction.PaymentData.BillingAddress.PostalCode));
					writer.Write(GetOptionalField(rmCommon.GetString("Address1"), Transaction.PaymentData.BillingAddress.AddressLine1));
					writer.Write(GetOptionalField(rmCommon.GetString("Address2"), Transaction.PaymentData.BillingAddress.AddressLine2));
					writer.Write("</td>");
				}
			}
			/*
			// payer info
			PublicPayCustomerVO payerInfo = PublicPayment.GetPublicPayCustomer(WebUtils.LoggedUser.CredentialsToken, Transaction.ID, Transaction.Status);
			if (!payerInfo.IsEmpty)
			{
				writer.Write("<td valign=\"top\">");
				writer.Write("<div class=\"MainHeadings\">" + rmTransactionDetails.GetString("PayerInfo") + "</div>");
				writer.Write(GetOptionalField(rmCommon.GetString("PayerInfoFullName"), payerInfo.FullName));
				writer.Write(GetOptionalField(rmCommon.GetString("PayerInfoMail"), payerInfo.Mail));
				writer.Write(GetOptionalField(rmCommon.GetString("PayerInfoAddress"), payerInfo.Address));
				writer.Write(GetOptionalField(rmCommon.GetString("PayerInfoPhone"), payerInfo.Phone));
				writer.Write(GetOptionalField(rmCommon.GetString("PayerInfoCellular"), payerInfo.Cellular));
				writer.Write(GetOptionalField(rmCommon.GetString("PayerInfoHowGetHere"), payerInfo.HowGetHere));
				writer.Write(GetOptionalField(payerInfo.CustomName1, payerInfo.CustomValue1));
				writer.Write(GetOptionalField(payerInfo.CustomName2, payerInfo.CustomValue2));
				writer.Write(GetOptionalField(payerInfo.CustomName3, payerInfo.CustomValue3));
				writer.Write(GetOptionalField(payerInfo.CustomName4, payerInfo.CustomValue4));
				writer.Write("</td>");
			}
			*/
			// merchant info
			if (_showMerchantDetails)
			{
				if (Transaction.Merchant != null)
				{
					writer.Write("<td valign=\"top\">");
					writer.Write("<div class=\"MainHeadings\">" + rmTransactionDetails.GetString("MerchantInfo") + "</div>");
					writer.Write(GetOptionalField(rmCommon.GetString("Name"), Transaction.Merchant.Name));
					writer.Write(GetOptionalField(rmCommon.GetString("Address"), Transaction.Merchant.BusinessAddress.AddressLine1));
					writer.Write("</td>");
				} 
			}

			// fees
			if (_showFees)
			{
				if (Transaction.TransactionFee > 0 || Transaction.RatioFee > 0 || Transaction.ChargebackFee > 0 || Transaction.ClarificationFee > 0 || Transaction.HandlingFee > 0)
				{
					writer.Write("<td valign=\"top\">");
					writer.Write(string.Format("<div class=\"MainHeadings\">{0}</div>", rmCommon.GetString("Fees")));
					writer.Write(GetOptionalField(rmTransactionDetails.GetString("TransFixedFee"), Transaction.CurrencySymbol, Transaction.TransactionFee));
					writer.Write(GetOptionalField(rmTransactionDetails.GetString("TransRatioFee"), Transaction.CurrencySymbol, Transaction.RatioFee));
					writer.Write(GetOptionalField(rmTransactionDetails.GetString("ChargebackFee"), Transaction.CurrencySymbol, Transaction.ChargebackFee));
					writer.Write(GetOptionalField(rmTransactionDetails.GetString("ClarificationFee"), Transaction.CurrencySymbol, Transaction.ClarificationFee));
					writer.Write(GetOptionalField(rmTransactionDetails.GetString("HandlingFee"), Transaction.CurrencySymbol, Transaction.HandlingFee));
					writer.Write("</td>");
				} 
			}

			writer.Write("<tr>");
			writer.Write("<td colspan=\"5\">");
			writer.Write(GetOptionalField(rmCommon.GetString("Comment"), Transaction.Comment));
			writer.Write("</td>");
			writer.Write("</tr>");

			writer.Write("</tr>");
			writer.Write("</tbody>");
			writer.Write("</table>");
			writer.Write("</td>");
			writer.Write("</tr>");

			writer.Write("<tr><td style=\"height: 12px;\"></td></tr>");

			// amounts
			if (!Infrastructure.Application.IsProduction && _showAmounts)
			{
				var events = Bll.Finance.TransactionAmount.LoadForTransaction(Transaction.ID, Transaction.Status);
				if (WebUtils.IsLoggedin && WebUtils.LoggedUser.Role == Infrastructure.Security.UserRole.Merchant)
					events = events.Where(e => e.SettlementType == Bll.Finance.SettlementType.Merchant).ToList();
				if (events.Count > 0)
				{
					writer.Write("<tr>");
					writer.Write("<td style=\"height: 22px;\">");
					writer.Write("<div class=\"MainHeadings\">" + rmTransactionDetails.GetString("TransactionAmounts") + " (Shown only in dev)</div>");
					writer.Write("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"50%\" class=\"transEvents\">");
					writer.Write("<tbody>");
					foreach (var currentAmount in events.OrderByDescending(e => e.ID).OrderByDescending(e => e.Date))
					{
						writer.Write("<tr>");
						writer.Write("<td>");
						if (currentAmount.AmountType == Bll.Finance.AmountType.InstallmentItem)
							writer.Write(currentAmount.SettlementDate);
						else
							writer.Write(currentAmount.Date);
						writer.Write("</td>");
						writer.Write("<td>");
						writer.Write(rmTransactionAmounts.GetString(currentAmount.AmountType.ToString()));
						writer.Write("</td>");
						writer.Write("<td>");
						writer.Write(new AmountView(currentAmount.Total, currentAmount.CurrencyISOCode).RenderNow());
						writer.Write("</td>");
						writer.Write("</tr>");
					}
					writer.Write("</tbody>");
					writer.Write("</table>");
					writer.Write("</td>");
					writer.Write("</tr>");

					writer.Write("<tr><td style=\"height: 12px;\"></td></tr>");
				}
			}

			// reasons for chargebacks and retrieval requests
			if (Transaction.Status == TransactionStatus.Captured && Transaction.DeniedStatusID > 0)
			{
				writer.Write("<tr><td></td></tr>");

				var items = Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(Transaction.ID);
				foreach (var item in items)
				{
					writer.Write("<tr><td>");
					string typeTitle = rmTransactionDetails.GetString(item.TypeID == TransactionHistoryType.Chargeback ? "Chargeback" : "RetrievalRequest");
					if (Transaction.IsPendingChargeback)
						typeTitle += " (" + rmTransactionDetails.GetString("NonRefundable") + ") ";
					writer.Write("<b>" + typeTitle + "</b> | " + item.RecordDate.ToShortDateString());
					writer.Write(" | " + rmTransactionDetails.GetString("ChargebackReasonCode") + " " + item.ReasonCode);
					if (!string.IsNullOrEmpty(item.Title))
					{
						writer.Write(" - " + item.Title);
						writer.Write("<br /><span class=\"SmallHeadings\">" + rmTransactionDetails.GetString("ChargebackReasonDescription") + "</span>: " + item.Description);
						writer.Write("<br /><span class=\"SmallHeadings\">" + rmTransactionDetails.GetString("ChargebackReasonRequiredMedia") + "</span>: " + item.RequiredMedia);
						if (!string.IsNullOrWhiteSpace(item.RefundInfo))
							writer.Write("<br /><span class=\"SmallHeadings\">" + rmTransactionDetails.GetString("ChargebackReasonRefundInfo") + "</span>: " + item.RefundInfo);
					}
					writer.Write("</td></tr><tr><td style=\"height: 12px;\"></td></tr>");
				}
			}

			// when captured display refunds/originals
			if (Transaction.Status == TransactionStatus.Captured)
			{
				var refunds = Bll.Transactions.Transaction.GetRefunds(Transaction.ID);
				if (refunds.Count > 0 || Transaction.OriginalTransactionID > 0)
				{
					writer.Write("<tr><td>");
					foreach (var refund in refunds)
						writer.Write("<div><span class=\"SmallHeadings\">" + rmTransactionDetails.GetString("RefundTransaction") + "</span>: <a href=\"?backgroundColor=F6FEF6&transactionStatus=Captured&transactionID=" + refund.ID.ToString() + "\">" + refund.ID.ToString() + "</a></div>");
					if (Transaction.OriginalTransactionID > 0)
						writer.Write("<div><span class=\"SmallHeadings\">" + rmTransactionDetails.GetString("OriginalTransaction") + "</span>: <a href=\"?backgroundColor=F6FEF6&transactionStatus=Captured&transactionID=" + Transaction.OriginalTransactionID.ToString() + "\">" + Transaction.OriginalTransactionID.ToString() + "</a></div>");
					writer.Write("<br /></td></tr>");
				}
			}

			// merchant action bar
			if (WebUtils.LoggedUser.Role == Infrastructure.Security.UserRole.Merchant)
			{
				if (Transaction.Status == TransactionStatus.Captured || Transaction.Status == TransactionStatus.Authorized || Transaction.Status == TransactionStatus.Declined)
				{
					writer.Write("<tr>");
					writer.Write("<td style=\"height: 22px;\">");
					writer.Write("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" class=\"transActionBar\">");
					writer.Write("<tbody>");
					writer.Write("<tr>");
					writer.Write("<td>");

					if (Transaction.Status == TransactionStatus.Authorized)
					{
						if (Transaction.PassedTransactionID == null)
						{
							// not captured
							writer.Write(string.Format("<a onclick=\"top.netpay.ajaxApi.AjaxMethods.GetCapture('{0}', function(response){{top.netpay.Common.openPopup(response.Data);}});return false;\"><span>{1}</span></a>", Transaction.ID, rmTransactionDetails.GetString("btnCapture")));
						}
						else
						{
							// captured
							string transactionLink = string.Format("<a target=\"_top\" href=\"TransApproved.aspx?transID={0}\">{0}</a>", Transaction.PassedTransactionID);
							writer.Write(rmTransactionDetails.GetString("TransactionAlreadyCaptured").Replace("#", transactionLink));
						}
						writer.Write("&nbsp; | &nbsp;");
					}

					if (Transaction.Status == TransactionStatus.Captured)
					{
						// print slip
						writer.Write(string.Format("<a onclick=\"top.netpay.ajaxApi.AjaxMethods.GetTransactionPrintSlip('{0}', '{1}', function(response){{top.netpay.Common.openPopup(response.Data);}})\"><span>{2}</span></a>", Transaction.ID, Transaction.StatusText, rmTransactionDetails.GetString("btnPrintSlip")));
						writer.Write("&nbsp; | &nbsp;");

						// create invoice
						if (Bll.Invoices.InvoiceCreator.IsEnabled(WebUtils.LoggedUser.LoginID))
						{
							string invoiceUrl;
							int? invID = Bll.Invoices.InvoiceCreator.InvoiceStatus(_transaction.ID, out invoiceUrl);
							if (invID != null)
							{
								if (!string.IsNullOrEmpty(invoiceUrl)) invoiceUrl = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", invoiceUrl, invID);
								writer.Write("<span>" + string.Format(rmTransactionDetails.GetString("InvoiceCreated"), (string.IsNullOrEmpty(invoiceUrl) ? invID.ToString() : invoiceUrl)) + "</span>");
							}
							else writer.Write(string.Format("<a onclick=\"top.netpay.ajaxApi.AjaxMethods.GetTransactionCreateInvoice('{0}', function(response){{top.netpay.Common.alert(response.Data);}})\"><span>{1}</span></a>", Transaction.ID, rmTransactionDetails.GetString("btnCreateInvoice")));
							writer.Write("&nbsp; | &nbsp;");
						}

						// refund request
						if (Bll.Transactions.RefundRequest.GetRefundAbility(_transaction) != TransactionRefundAbility.None)
						{
							writer.Write(string.Format("<a onclick=\"top.netpay.ajaxApi.AjaxMethods.GetRefundRequest('{0}', '{1}', function(response){{top.netpay.Common.openPopup(response.Data);}})\"><span>{2}</span></a>", Transaction.ID, Transaction.StatusText, rmTransactionDetails.GetString("btnRequestRefund")));
							writer.Write("&nbsp; | &nbsp;");
						}

						if (Transaction.PaymentData != null && Transaction.PaymentData.MethodInstance.PaymentMethodType == PaymentMethodType.CreditCard)
						{
							// recurring series
							if (Transaction.RecurringSeriesID == null)
							{
								if (Bll.Transactions.Recurring.MerchantSettings.Current.CanCreateFromExistingTransaction)
								{
									// can create series
									writer.Write(string.Format("<a onclick=\"top.netpay.ajaxApi.AjaxMethods.GetSeriesFromTransaction('{0}', '{1}', function(response){{top.netpay.Common.openPopup(response.Data);}})\" style=\"cursor: pointer;\"><span>{2}</span></a>", Transaction.ID, Transaction.StatusText, rmTransactionDetails.GetString("CreateRecurringSeries")));
								}
								else
								{
									// cannot create series
									writer.Write(rmTransactionDetails.GetString("CannotCreateSeries"));
								}
							}
							else
							{
								// series already created
								writer.Write(string.Format("<span>{0}: # <a style=\"color:#111111;\" href=\"RecurringChargesMain.aspx?seriesID={1}\" target=\"_top\">{1}</a></span>", rmTransactionDetails.GetString("RecurringNumber"), Transaction.RecurringSeriesID));
							}

							writer.Write("&nbsp; | &nbsp;");
						}
					}

					if ((Transaction.PaymentMethodID >= (PaymentMethodEnum)PaymentMethodEnum.CC_MIN && Transaction.PaymentMethodID <= (PaymentMethodEnum)PaymentMethodEnum.CC_MAX) &&
						(Transaction.Status == TransactionStatus.Authorized || Transaction.Status == TransactionStatus.Captured || Transaction.Status == TransactionStatus.Declined))
					{
						// blocked card
						var blockedCard = RiskItem.Search(true, null, new List<RiskItem.RiskItemValue>() { new RiskItem.RiskItemValue() { ValueType = RiskItem.RiskValueType.AccountValue1, Value = System.Convert.ToBase64String(Transaction.PaymentData.MethodInstance.GetEncyptedValue1()) } }).FirstOrDefault();
						if (blockedCard == null)
						{
							// card not blocked
							writer.Write(string.Format("<a onclick=\"top.netpay.ajaxApi.AjaxMethods.AddToBlacklist('{0}', '{1}', function(response){{top.netpay.Common.openPopup(response.Data);}})\"><span>{2}</span></a>", Transaction.ID, Transaction.StatusText, rmTransactionDetails.GetString("blacklistAdd")));
						}
						else
						{
							// card already blocked
							writer.Write(string.Format("<a href='CCBlackList.aspx?transactionID={0}&transactionStatus={1}' target='_top'><span>{2}</span></a>", Transaction.ID, (int)Transaction.Status, rmTransactionDetails.GetString("blacklistExists")));
						}
					}

					writer.Write("</td>");
					writer.Write("</tr>");
					writer.Write("</tbody>");
					writer.Write("</table>");
					writer.Write("</td>");
					writer.Write("</tr>");
				}
			}

			/*
			// netpay action bar
			if (WebUtils.LoggedUser.Type == Infrastructure.UserType.NetpayUser || WebUtils.LoggedUser.Type == Infrastructure.UserType.NetpayAdmin)
			{
				writer.Write("<tr>");
				writer.Write("<td style=\"height: 22px;\">");
				writer.Write("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" class=\"transActionBar\">");
				writer.Write("<tbody>");
				writer.Write("<tr>");
				writer.Write("<td>");
				
				// request refund
				if (RefundRequests.GetRefundAbility(WebUtils.LoggedUser.CredentialsToken, _transaction) != TransactionRefundAbility.None)
				{
					writer.Write("<a onclick=\"OpenPop('Trans_RefundRequest.aspx?ID=5367822&Merchant=35&Currency=2','fraRefundAsk',600,300,0);\" href=\"#\">Refund Request</a>");
					writer.Write("&nbsp;|&nbsp;");
				}

				// chargeback & clarification
				var chargebacks = History.Where(th => th.Type == TransactionHistoryType.Chargeback);
				var clarifications = History.Where(th => th.Type == TransactionHistoryType.RetrievalRequest);
				if (chargebacks.Count() > 0)
				{
					writer.Write("Chargeback");
					writer.Write("&nbsp;|&nbsp;");
					writer.Write("Clarification");
					writer.Write("&nbsp;|&nbsp;");
				}
				else if (clarifications.Count() > 0)
				{
					writer.Write("<a onclick=\"createChargeback();return false;\" href=\"#\">Chargeback</a>");
					writer.Write("&nbsp;|&nbsp;");
					writer.Write("Clarification");
					writer.Write("&nbsp;|&nbsp;");
				}
				else 
				{
					writer.Write("<a onclick=\"createChargeback();return false;\" href=\"#\">Chargeback</a>");
					writer.Write("&nbsp;|&nbsp;");
					writer.Write("<a onclick=\"createClarification();return false;\" href=\"#\">Clarification</a>");
					writer.Write("&nbsp;|&nbsp;");
				}

				// block card
				if ((Transaction.PaymentMethodID >= (short)PaymentMethodEnum.CC_MIN && Transaction.PaymentMethodID <= (short)PaymentMethodEnum.CC_MAX) &&
					(Transaction.Status == TransactionStatus.Authorized || Transaction.Status == TransactionStatus.Captured || Transaction.Status == TransactionStatus.Declined))
				{
					// blocked card
					BlockedCreditCardVO blockedCard = BlockedItems.GetBlockedCard(WebUtils.LoggedUser.CredentialsToken, Transaction.ID, Transaction.Status);
					if (blockedCard == null)
					{
						// card not blocked
						writer.Write("<a href=\"" + Page.ClientScript.GetPostBackClientHyperlink(this, "CCBlock") + "\">CC Block</a>");
					}
					else
					{
						// card already blocked
						writer.Write("Card Blocked");
					}
				}

				// block item
				writer.Write("&nbsp;|&nbsp;");
				writer.Write("<a href=\"#\" onclick=\"OpenPop('BLCommonBlockAdd.aspx?TransID=" + _transaction.ID + "&CCID=" + _transaction.PaymentMethodData.PaymentMethodRowID + "','CommonBlockAdd',500,300,1);\">Item Block</a>");
				
				// white list
				writer.Write("&nbsp;|&nbsp;");
				writer.Write("<a href=\"#\" onclick=\"OpenPop('Whitelist.aspx?CCID=" + _transaction.PaymentMethodData.PaymentMethodRowID + "&FilterMerchantDefaultID=" + _transaction.MerchantID + "','CommonBlockAdd',1000,300,1);\">CC Whitelist</a>");

				writer.Write("</td>");
				writer.Write("</tr>");
				writer.Write("</tbody>");
				writer.Write("</table>");
				writer.Write("</td>");
				writer.Write("</tr>");
			}
			*/
			
			writer.Write("</tbody>");
			writer.Write("</table>");
		}

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
			/*
			if (eventArgument == "CCBlock") 
			{
				if (WebUtils.LoggedUser.Type != Infrastructure.UserType.NetpayAdmin && WebUtils.LoggedUser.Type != Infrastructure.UserType.NetpayUser)
					return;
				
				DateTime? blockDate = DateTime.Now;
				BlockedItems.AddBlockedCard(WebUtils.CredentialsToken, Transaction.ID, Transaction.Status, "Blocked by admin", out blockDate);
			}

			if (eventArgument == "createChargeback")
			{
				if (WebUtils.LoggedUser.Type != Infrastructure.UserType.NetpayAdmin && WebUtils.LoggedUser.Type != Infrastructure.UserType.NetpayUser)
					return;

				var chargebacks = History.Where(th => th.Type == TransactionHistoryType.Chargeback);
				if (chargebacks.Count() > 0)
					return;
				
				int reasonCode = HttpContext.Current.Request["ddChargebackReasons"].ToInt32();
				Transactions.CreateChargeback(WebUtils.CredentialsToken, Transaction.ID, reasonCode, "created by admin");
				_history = null;
			}

			if (eventArgument == "createClarification")
			{
				if (WebUtils.LoggedUser.Type != Infrastructure.UserType.NetpayAdmin && WebUtils.LoggedUser.Type != Infrastructure.UserType.NetpayUser)
					return;

				var chargebacks = History.Where(th => th.Type == TransactionHistoryType.Chargeback || th.Type == TransactionHistoryType.RetrievalRequest);
				if (chargebacks.Count() > 0)
					return;

				int reasonCode = HttpContext.Current.Request["ddChargebackReasons"].ToInt32();
				Transactions.CreateRetrievalRequest(WebUtils.CredentialsToken, Transaction.ID, reasonCode, "created by admin");
				_history = null;
			}
			*/

			if (eventArgument == "SendClientEmail")
            {
				Transaction.SendClientEmail();
                _emailSent = true;
            }
        }
    }
}
