﻿namespace Netpay.Web.Controls
{
	public enum NotificationSlipType
	{
		Warning = 1,
		Reminder = 2
	}
}
