﻿using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	public class NotificationSlip : WebControl
	{
		public NotificationSlipType Type { get; set; }
		public string Title { get; set; }
		public string Number { get; set; }
		public string Text { get; set; }

		public NotificationSlip()
		{
			Type = NotificationSlipType.Reminder;
		}

		protected override void Render(HtmlTextWriter writer)
		{
			//string imageName = "Curl_" + _type.ToString() + WebUtils.CurrentDirection.ToSentence() + ".gif";
			//string embeddedImageUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.NotificationSlip." + imageName);
			//if (_type == NotificationSlipType.Warning) textBackgroundColor = "#ffe1e1";
			//else if (_type == NotificationSlipType.Reminder) textBackgroundColor = "#ffeec2";
			Text = Text.Replace("#N#", Number);
			writer.Write("<div class=\"notification-content\">");
            writer.Write(" <div class=\"notification-content-title\">");
			if (!string.IsNullOrEmpty(Number)) writer.Write("  <div class=\"notifaction-popNumber\">{0}</div>", Number);
            writer.Write("  <div class=\"notification-button\">");
            writer.Write("	 <div class=\"notification-button-text\">{0}</div>", Title);
            writer.Write("  </div>");
            writer.Write(" </div>");
            writer.Write(" <div class=\"notification-content-data\">{0}</div>", Text);
            writer.Write(" <div class=\"spacer\"></div>");
            writer.Write("</div>");
		}
	}
}
