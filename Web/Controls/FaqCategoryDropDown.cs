﻿using System;
using System.Web.UI;
using System.Collections.Generic;

namespace Netpay.Web.Controls
{
    [ToolboxData("<{0}:FaqCategoryDropDown runat=server></{0}:FaqCategoryDropDown>")]
    public class FaqCategoryDropDown : DropDownBase
	{
        private bool _isContent = false;
        private bool _isCustomer = false;
        private bool _isMerchant = false;
        private bool _isDeveloper = false;

        public bool IsContent
        {
            get { return _isContent; }
            set { _isContent = value; }
        }

        public bool IsCustomer
        {
            get { return _isCustomer; }
            set { _isCustomer = value; }
        }

        public bool IsMerchant
        {
            get { return _isMerchant; }
            set { _isMerchant = value; }
        }

        public bool IsDeveloper
        {
            get { return _isDeveloper; }
            set { _isDeveloper = value; }
        }
        
        protected override void OnInit(EventArgs e)
		{
			if (Items.Count == 0)
			{
				DataSource = Netpay.Bll.Faqs.Group.Search(new Bll.Faqs.Group.SearchFilters() { LanguageId = WebUtils.CurrentLanguage, IsMerchantCP = IsMerchant, IsWallet = IsCustomer, IsDevCenter = IsDeveloper, IsWebsite = IsContent, IsVisible = true, Theme = WebUtils.CurrentDomain.ThemeFolder, AllForTheme = true }, null);
                DataValueField = "Key";
				DataTextField = "Value";
				DataBind();
			}

			base.OnInit(e);
		}
	}
}



