﻿using System;
using System.Drawing;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	public partial class ImageUpload : System.Web.UI.Control
	{
		public event EventHandler DataUpdate;
		public int ImageWidth { get; set; }
		public int ImageHeight { get; set; }
		public int ThumbImageWidth { get; set; }
		public int ThumbImageHeight { get; set; }
		public string TargetFileName { get; set; }
		public string FileName { get; set; }
		public bool FillSpace { get; set; }
		public string CssCalss { get; set; }
		public short TabIndex { get; set; }

		public System.Drawing.Brush BackgroundBrush { get; set; }
		public ImageUpload() 
		{
			//init local vars
			ImageWidth = 80; ImageHeight = 80; 
			BackgroundBrush = System.Drawing.Brushes.White;

			//init child controls
			MultiView pMultiView = new MultiView(); pMultiView.ID = "ImageView";
			View pView = new View();
			System.Web.UI.WebControls.Image pImage = new System.Web.UI.WebControls.Image();
			pImage.ID = "Image";
			pView.Controls.Add(pImage);
			Literal lt = new Literal(); lt.Text = "<br/><br/>[ "; lt.Mode = LiteralMode.PassThrough; 
			pView.Controls.Add(lt);
			LinkButton lb = new LinkButton(); lb.Text = "Remove logo";
			lb.Click += btnRemove_OnClick;
			pView.Controls.Add(lb);
			lt = new Literal(); lt.Mode = LiteralMode.PassThrough; lt.Text = " ]<br/>";
			pView.Controls.Add(lt);
			pMultiView.Controls.Add(pView);

			pView = new View();
			FileUpload fu = new FileUpload(); fu.ID = "FileUpload";
			fu.TabIndex = TabIndex;
			pView.Controls.Add(fu);
			Button bt = new Button(); bt.Text = "Upload!"; bt.Click += btnUpload_OnClick;
			pView.Controls.Add(bt);
			lt = new Literal(); lt.ID = "ltFileError"; lt.Mode = LiteralMode.PassThrough;
			pView.Controls.Add(lt);
			pMultiView.Controls.Add(pView);

			Controls.Add(pMultiView);
		}

		protected override void OnLoad(EventArgs e)
		{
			if (FileName == null && Page.IsPostBack) FileName = (FindControl("Image") as System.Web.UI.WebControls.Image).ImageUrl;
			base.OnLoad(e);
		}

		public string SaveFileName { get { if (string.IsNullOrEmpty(FileName)) return ""; return System.IO.Path.GetFileName(FileName); } }
		private void btnRemove_OnClick(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(FileName)) return;
			(FindControl("Image") as System.Web.UI.WebControls.Image).ImageUrl = "";
			(FindControl("ImageView") as MultiView).SetActiveView((FindControl("ImageView") as MultiView).Views[1]);
			if (System.IO.File.Exists(Page.MapPath(FileName))) System.IO.File.Delete(Page.MapPath(FileName));
			FileName = "";
			if (DataUpdate != null) DataUpdate.Invoke(this, e);
		}

		private void btnUpload_OnClick(object sender, EventArgs e)
		{
			if (Save() && (DataUpdate != null)) DataUpdate.Invoke(this, e);
		}

		private System.Drawing.Image ImageResize(System.Drawing.Image pImage, bool releaseSource)
		{
			Rectangle rctDest;
			Bitmap RetImage = null;
			if (FillSpace) {
				if (((float)ImageHeight / (float)pImage.Height) > ((float)ImageWidth / (float)pImage.Width)) {
					int newSize = ImageHeight - (int)(((float)ImageWidth / (float)pImage.Width) * (float)pImage.Height);
					rctDest = new Rectangle(0, (newSize >> 1), ImageWidth, ImageHeight - newSize);
				} else {
					int newSize = ImageWidth - (int)(((float)ImageHeight / (float)pImage.Height) * (float)pImage.Width);
					rctDest = new Rectangle((newSize >> 1), 0, ImageWidth - newSize, ImageHeight);
				}
				RetImage = new Bitmap(ImageWidth, ImageHeight);
			} else {
				rctDest = new Rectangle(0, 0, pImage.Width, pImage.Height);
				if (ImageWidth < rctDest.Width) {
					float newSize = (float)ImageWidth / (float)rctDest.Width;
					rctDest = new Rectangle(0, 0, (int)(newSize * (float)rctDest.Width), (int)(newSize * (float)rctDest.Height));
				}
				if (ImageHeight < rctDest.Height) {
					float newSize = (float)ImageHeight / (float)rctDest.Height;
					rctDest = new Rectangle(0, 0, (int)(newSize * (float)rctDest.Width), (int)(newSize * (float)rctDest.Height));
				}
				RetImage = new Bitmap(rctDest.Width, rctDest.Height);
			}
			Graphics g = Graphics.FromImage(RetImage);
			if (FillSpace) g.FillRectangle(BackgroundBrush, new Rectangle(0, 0, RetImage.Width, RetImage.Height));
			g.DrawImage(pImage, rctDest, new Rectangle(0, 0, pImage.Width, pImage.Height), GraphicsUnit.Pixel);
			g.Dispose();
			if (releaseSource) pImage.Dispose();
			return RetImage;
		}

		private bool Save() 
		{
			FileUpload fu = FindControl("FileUpload") as FileUpload;
			Literal ltFileError = FindControl("ltFileError") as Literal;

			if (!fu.HasFile) return false;
			string fileExt = System.IO.Path.GetExtension(fu.FileName).ToLower();
			if (fileExt.StartsWith(".")) fileExt = fileExt.Substring(1);
			if (fileExt != "jpg" && fileExt != "jpeg" && fileExt != "gif" && fileExt != "png" && fileExt != "bmp") {
				ltFileError.Text = "Invalid file format!";
				return false;
			}
			System.Drawing.Image pImage = null;
			try {
				pImage = System.Drawing.Image.FromStream(fu.FileContent);
				pImage = ImageResize(pImage, true);
            } catch {
                ltFileError.Text = "Unable to open image file";
                FileName = null;
                if (pImage != null) pImage.Dispose();
                return false;
            }
            string sDirectoryName = Page.MapPath(System.IO.Path.GetDirectoryName(TargetFileName));
            FileName = TargetFileName + ".jpg";
            System.Drawing.Imaging.EncoderParameters Params = new System.Drawing.Imaging.EncoderParameters(1);
            Params.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            System.Drawing.Imaging.ImageCodecInfo ici = getImageEncoder(System.Drawing.Imaging.ImageFormat.Jpeg);
            if (ici == null)
            {
                ltFileError.Text = "unable to find jpeg image encoder";
                FileName = null;
                return false;
            }
            try{
                if (!System.IO.Directory.Exists(sDirectoryName)) System.IO.Directory.CreateDirectory(sDirectoryName);
                //pImage.Save(Page.MapPath(FileName), System.Drawing.Imaging.ImageFormat.Jpeg);
                pImage.Save(Page.MapPath(FileName), ici, Params); 
			} catch(Exception ex) {
                Netpay.Infrastructure.Logger.Log(ex);
                ltFileError.Text = "Unable to save image file see log for details";
				FileName = null;
				return false;
			} finally {
				if (pImage != null) pImage.Dispose();
			}
			return true;
		}

        private System.Drawing.Imaging.ImageCodecInfo getImageEncoder(System.Drawing.Imaging.ImageFormat format)
        {
            System.Drawing.Imaging.ImageCodecInfo[] Info = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
            foreach (var v in Info) if (v.FormatID == format.Guid) return v;
            return null;
        }

        protected override void OnPreRender(EventArgs e)
		{
			bool bImageExist = false;
			System.Web.UI.WebControls.Image pImage = FindControl("Image") as System.Web.UI.WebControls.Image;
			if (ThumbImageWidth != 0) pImage.Width = ThumbImageWidth;
			if (ThumbImageHeight != 0) pImage.Height = ThumbImageHeight;
			if (!string.IsNullOrEmpty(FileName)) bImageExist = System.IO.File.Exists(Page.MapPath(FileName));
			if (bImageExist) pImage.ImageUrl = FileName;
			(FindControl("ImageView") as MultiView).SetActiveView((FindControl("ImageView") as MultiView).Views[bImageExist ? 0 : 1]);
			base.OnPreRender(e);
		}
	}
}
