﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using System.Web.UI;
using System.IO;
using Netpay.Infrastructure;
using Netpay.Bll;

namespace Netpay.Web.Controls
{
	public class AmountView : System.Web.UI.Control
	{
		public int? CountValue { get; set; }
		public decimal? AmountValue { get; set; }
		public decimal? PercentValue { get; set; }
		public int? CurrencyID { get; set; }

		public AmountView() { }

		public AmountView(decimal? amountValue) 
		{
			AmountValue = amountValue;
		}

		public AmountView(decimal? amountValue, int? currency)
		{
			AmountValue = amountValue;
			CurrencyID = currency.GetValueOrDefault((int)CommonTypes.Currency.USD);
		}

		public AmountView(decimal? amountValue, string currencyIso)
		{
			AmountValue = amountValue;
			CurrencyID = Bll.Currency.Get(currencyIso).ID;
		}

		public AmountView(int? countValue, decimal? amountValue, CommonTypes.Currency? currency, decimal? percentValue)
		{
			CountValue = countValue; AmountValue = amountValue; PercentValue = percentValue;
			CurrencyID = (int)currency.GetValueOrDefault(CommonTypes.Currency.USD);
		}

		public AmountView(int? countValue, decimal? amountValue, int? currencyID, decimal? percentValue)
		{
			CountValue = countValue; AmountValue = amountValue; PercentValue = percentValue;
			CurrencyID = currencyID.GetValueOrDefault((int)CommonTypes.Currency.USD);
		}

		protected override void Render(HtmlTextWriter response)
		{
			if (CountValue != null) 
				response.Write(string.Format("<span style=\"color: #999999;\">({0}) </span>", CountValue.Value.ToString("#,0")));
			if (PercentValue != null) 
				response.Write(string.Format("<span style=\"color: #999999;\">({0}) </span>", (PercentValue.Value / 100).ToString("P")));

			if (AmountValue == null) 
				response.Write("<span>0</span>");
			else
			{
				string formattedAmount = null;
				if (CurrencyID == null)
					formattedAmount = AmountValue.Value.ToAmountFormat();
				else
					formattedAmount = AmountValue.Value.ToAmountFormat(CurrencyID.Value);
				
				if (AmountValue < 0)
					response.Write(string.Format("<span style=\"color:red;\">{0}</span>", formattedAmount));
				else
					response.Write(string.Format("<span>{0}</span>", formattedAmount));
			}

			base.Render(response);
		}

		public string RenderNow() 
		{
			StringBuilder sb = new StringBuilder();
			StringWriter sw = new StringWriter(sb);
			HtmlTextWriter htw = new HtmlTextWriter(sw);
			Render(htw);
			
			return sb.ToString();
		}
	}
}
