﻿using System;
using System.Web.UI;

namespace Netpay.Web.Controls
{
	/// <summary>
	/// Payment banks (not debit companies)
	/// </summary>
	[ToolboxData("<{0}:DebitCompanyDropDown runat=server></{0}:DebitCompanyDropDown>")]
	public class WireAccountDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			DataSource = Bll.Wires.WireBankAccount.Search(null, null);
            DataValueField = "ID"; DataTextField = "Title";
			base.DataBind(raiseOnDataBinding);
		} 
	}
}



