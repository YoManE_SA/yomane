﻿if (netpay == null) {var netpay = {}}
if (netpay.webControls == null) {netpay.webControls = {}}

netpay.webControls.AutoComplete = 
{
    tagSeperator: null,
    additionalParams: null,
    init: function (baseID, ajaxFunctionName, isMultiselect, tagSeperator, additionalParams)
	{
        netpay.Ajax.showLoaderOnAjaxCall = false;
        netpay.webControls.AutoComplete.tagSeperator = tagSeperator;
        netpay.webControls.AutoComplete.additionalParams = additionalParams;

        $("#" + baseID + "_text").keyup(function ()
		{ 
			if($(this).val() == ""){ $(this).next().val("") }
		});

		if (isMultiselect)
		{
			$("#" + baseID + "_text").autocomplete
			({ 
				source: netpay.webControls.AutoComplete.getSource, 
				functionName: ajaxFunctionName,
				focus: function(event, ui)
				{
					return false;
				},
				select: function(event, ui) 
				{
					// labels
					var element = this;
					var terms = element.value.split(netpay.webControls.AutoComplete.tagSeperator);
					terms.pop();
					terms.push(ui.item.label);
					terms.push("");
					element.value = terms.join(netpay.webControls.AutoComplete.tagSeperator);

					// values
					var element = $(this).next()[0];
					var terms = element.value.split(netpay.webControls.AutoComplete.tagSeperator);
					terms.pop();
					terms.push(ui.item.value);
					terms.push("");
					element.value = terms.join(netpay.webControls.AutoComplete.tagSeperator);

					return false;
				},
				search: function(event, ui) 
				{
				    var term = this.value.split(netpay.webControls.AutoComplete.tagSeperator).pop();
					if (term.length < 2) 
					{
						return false;
					}
				}
			});		
		}
		else
		{
			$("#" + baseID + "_text").autocomplete
			({ 
				source: netpay.webControls.AutoComplete.getSource, 
				functionName: ajaxFunctionName,
				focus: function(event, ui)
				{
					return false;
				},
				select: function(event, ui) 
				{
					// labels
					var element = this;
					element.value = ui.item.label;

					// values
					var element = $(this).next()[0];
					element.value = ui.item.value;

					return false;
				},
				search: function(event, ui) 
				{
					if (this.value < 2) 
					{
						return false;
					}
				}
			});	
		}

		$("#" + baseID + "_text").data("autocomplete")._renderItem = function(ul, item) 
		{
			return $("<li style='white-space:nowrap;'></li>").data("item.autocomplete", item).append("<a>" + item.label + "</a>").appendTo(ul);
		};
	},
	
	getSource: function (request, response)
	{
	    var term = request.term.split(netpay.webControls.AutoComplete.tagSeperator).pop();
		var context = new Object();
		context.response = response;
		netpay.ajaxApi.AjaxMethods[this.options.functionName](term, netpay.webControls.AutoComplete.additionalParams, netpay.webControls.AutoComplete.successHandler, null, context);
	},
	
	successHandler: function (result)
	{
		this.response(result.Data);
	}
}