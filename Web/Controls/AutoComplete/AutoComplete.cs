﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:AutoComplete runat=server></{0}:AutoComplete>")]
	public class AutoComplete : WebControl
	{
		private string _function = null;
		private string _selectedText = null;
		private string _selectedValue = null;
		private bool _isMultiselect = false;
        private string _tagSeperator = ", ";
        private Dictionary<string, string> _additionalParams = new Dictionary<string, string>();

        public Dictionary<string, string> AdditionalParams
        {
            get { return _additionalParams; }
            set { _additionalParams = value; }
        }

        public string TagSeperator
        {
            get { return _tagSeperator; }
            set { _tagSeperator = value; }
        }

		public bool IsMultiselect
		{
			get { return _isMultiselect; }
			set { _isMultiselect = value; }
		}

		public string Function
		{
			get { return _function; }
			set { _function = value; }
		}

		public bool IsSelected 
		{
			get 
			{
				return SelectedValue != null && SelectedValue.Trim() != "";
			}
		}

		public string SelectedText
		{
			get 
			{
				if (_selectedText == null)
					_selectedText = HttpContext.Current.Request[ClientID + "_text"];

				return _selectedText;
			}
			set 
			{
				_selectedText = value;
			}
		}

		public string SelectedValue
		{
			get
			{
				if (_selectedValue == null)
					_selectedValue = HttpContext.Current.Request[ClientID + "_value"];

				return _selectedValue;
			}
			set
			{
				_selectedValue = value;
			}
		}

		public string[] SelectedValues
		{
			get 
			{
				if (SelectedValue == null || SelectedValue.Trim() == "")
					return new string[0];

				string returnValue = SelectedValue.Trim();
				if (returnValue.EndsWith(","))
					returnValue = returnValue.Remove(SelectedValue.LastIndexOf(','));
				
				return returnValue.Split(',');
			}
		}

		public List<T> GetSelectedValues<T>() 
		{
			List<T> result = new List<T>();
			foreach (string currentValue in SelectedValues)
			{
				try
				{
					T converted = (T)Convert.ChangeType(currentValue, typeof(T));
					result.Add(converted);
				}
				catch { }
			}
			
			return result;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			// embed scripts
			this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.AutoComplete.AutoComplete.js");
		}

		protected override void Render(HtmlTextWriter writer)
		{
            writer.Write(string.Format("<input type=\"text\" id=\"{0}\" placeholder=\"Search\" name=\"{0}\" value=\"{1}\" ", ClientID + "_text", SelectedText));
			Attributes.Render(writer);
			writer.Write(" />");
            writer.Write(string.Format("<input type=\"hidden\" id=\"{0}\" placeholder=\"Search\"  name=\"{0}\" value=\"{1}\" />", ClientID + "_value", SelectedValue));
			writer.Write(string.Format("<script>netpay.webControls.AutoComplete.init('{0}', '{1}', {2}, '{3}', '{4}');</script>", ClientID, Function, IsMultiselect.ToString().ToLower(), TagSeperator, AdditionalParams.ToJson()));
		}
	}
}
