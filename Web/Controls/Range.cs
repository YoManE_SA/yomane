﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{

	public class Range<T> : System.Web.UI.WebControls.CompositeControl where T : class, new()
	{
		public T From { get { return FindControl(ID + "From") as T; } }
		public T To { get { return FindControl(ID + "To") as T; } }
		protected override void CreateChildControls()
		{
			base.CreateChildControls();
			System.Web.UI.Control ctl = new T() as System.Web.UI.Control;
			ctl.ID = ID + "From";
			if (ctl is System.Web.UI.WebControls.WebControl)
				(ctl as System.Web.UI.WebControls.WebControl).Width = new System.Web.UI.WebControls.Unit(40, System.Web.UI.WebControls.UnitType.Percentage);
			Controls.Add(ctl);

			Controls.Add(new System.Web.UI.LiteralControl(" To "));

			ctl = new T() as System.Web.UI.Control;
			ctl.ID = ID + "To";
			if(ctl is System.Web.UI.WebControls.WebControl)
				(ctl as System.Web.UI.WebControls.WebControl).Width = new System.Web.UI.WebControls.Unit(40, System.Web.UI.WebControls.UnitType.Percentage);
			Controls.Add(ctl);
		}
	}

	public class DateRange : Range<DatePicker> {
		public Infrastructure.Range<DateTime?> Value
		{ 
			get { 
				return new Infrastructure.Range<DateTime?>() { From = this.From.Date, To = this.To.Date };
			}
		}
		protected override void CreateChildControls()
		{
			base.CreateChildControls();
			From.Date = To.Date = null; 
		}
	}
	public class IntRange : Range<System.Web.UI.WebControls.TextBox> {
		public Infrastructure.Range<int?> Value
		{ 
			get {
				return new Infrastructure.Range<int?>() { From = this.From.Text.ToNullableInt(), To = this.To.Text.ToNullableInt() };
			}
		} 
	}

	public class DecimalRange : Range<System.Web.UI.WebControls.TextBox> { 		
		public Infrastructure.Range<decimal?> Value
		{ 
			get {
				return new Infrastructure.Range<decimal?>() { From = this.From.Text.ToNullableDecimal(), To = this.To.Text.ToNullableDecimal() };
			}
		} 
	}

}
