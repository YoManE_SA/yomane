﻿namespace Netpay.Web.Controls
{
	public enum SortMode
	{
		Unsorted = 1,
		Ascending = 2,
		Descending = 3
	}
}
