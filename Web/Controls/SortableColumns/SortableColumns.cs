﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Bll;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:SortableColumns runat=server></{0}:SortableColumns>")]
	[ParseChildren(true, "Columns")]
	[PersistChildren(false)]
	public class SortableColumns : CompositeControl
	{
		private List<Control> _columns = new List<Control>();
		private string _voType = null;
		public event EventHandler SortChange;

		public string VOType
		{
			get { return _voType; }
			set { _voType = string.Format("Netpay.Infrastructure.VO.{0},Netpay.Infrastructure", value); }
		}

		public List<Control> Columns
		{
			get { return _columns; }
			set { _columns = value; }
		} 

		public ISortAndPage Info
		{
			get
			{
				SortableColumn sortedCol = SortedColumn;
				if (sortedCol == null)
					return null;
				
				var si = new SortAndPage();
				//if (VOType != null) si.VOType = Type.GetType(VOType, true);
				si.SortKey = sortedCol.ColumnName;
				si.SortDesc = false;
				if (sortedCol.ColumnSortMode == Netpay.Web.Controls.SortMode.Descending)
					si.SortDesc = true; 
					
				return si;
			}		
		}
		
		public SortableColumn SortedColumn
		{
			get 
			{
				SortableColumn sortedColumn = null;
				foreach (Control currentColumn in Columns)
				{
					if (currentColumn.GetType() == typeof(SortableColumn))
					{
						if (((SortableColumn)currentColumn).ColumnSortMode != SortMode.Unsorted)
						{
							sortedColumn = (SortableColumn)currentColumn;
							break;
						}
					}
				}
				
				return sortedColumn;
			}
			
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			// embedding the css 
			string template = "<link rel='stylesheet' text='text/css' href='{0}' />";
			string location = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.SortableColumns.SortableColumns.css");
			LiteralControl literal = new LiteralControl(String.Format(template, location));
			Page.Header.Controls.Add(literal);
		}

		protected override void CreateChildControls()
		{
			foreach (Control currentColumn in _columns)
			{
				if (currentColumn.GetType() == typeof(SortableColumn))
				{
					((SortableColumn)currentColumn).SortChange += new EventHandler(ColumnSortChange);	
					Controls.Add(currentColumn);					
				}
			}
		}

		protected void ColumnSortChange(object sender, EventArgs e)
		{
			foreach (Control currentColumn in _columns)
			{
				if (currentColumn.GetType() == typeof(SortableColumn) && currentColumn != sender)
				{
					((SortableColumn)currentColumn).ColumnSortMode = SortMode.Unsorted;
				}
			}
			
			if (SortChange != null)
				SortChange(this, null);
		}

		public override void RenderControl(HtmlTextWriter writer)
		{
			writer.Write("<thead><tr>");
			foreach (Control currentColumn in _columns)
				currentColumn.RenderControl(writer);
			writer.Write("</tr></thead>");
		}
	}
}