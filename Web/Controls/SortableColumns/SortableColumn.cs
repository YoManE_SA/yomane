﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:SortableColumn runat=server></{0}:SortableColumn>")]
	public class SortableColumn : CompositeControl
	{
		private LinkButton _button = new LinkButton();
		private Image _arrowImage = new Image();
		private string _cssStyle = null;
		public event EventHandler SortChange;

		public string CssStyle
		{
			get { return _cssStyle; }
			set { _cssStyle = value; }
		}

		public string ColumnName
		{
			get
			{
				if (ViewState["columnName"] == null)
					return "";

				return ViewState["columnName"].ToString();
			}
			set
			{
				ViewState["columnName"] = value;
			}
		}

		
		public string ColumnTitle
		{
			get 
			{ 
				if (ViewState["columnTitle"] == null)
					return "";
				
				return ViewState["columnTitle"].ToString();
			}
			set 
			{
				ViewState["columnTitle"] = value;
			}
		}

		/// <summary>
		/// Enables html tagging {} in title
		/// </summary>
		public string ParsedColumnTitle
		{
			get
			{
				return ColumnTitle.Replace('{', '<').Replace('}', '>');
			}
		}

		public SortMode ColumnSortMode
		{
			get
			{
				if (ViewState["columnSortMode"] == null)
					return SortMode.Unsorted;

				return (SortMode)ViewState["columnSortMode"];
			}
			set
			{
				ViewState["columnSortMode"] = value;
			}
		}

		protected override void CreateChildControls()
		{
			_button.Text = ParsedColumnTitle;
			if (Enabled)
			{
				_button.Attributes.Add("class", "sortedColumnLink");
				_button.Click += new EventHandler(this.SortButtonClick);
			}			
			Controls.Add(_button);
			Controls.Add(_arrowImage);
		}

		protected virtual void SortButtonClick(object sender, EventArgs e)
		{
			if (ColumnSortMode == SortMode.Unsorted || ColumnSortMode == SortMode.Descending)
				ColumnSortMode = SortMode.Ascending;
			else
				ColumnSortMode = SortMode.Descending;
				
			if (SortChange != null)
				SortChange(this, e);
		} 

		public override void RenderControl(HtmlTextWriter writer)
		{
			if (!Visible)
				return;
			
			string embeddedImageUrl = null;
			switch (ColumnSortMode)
			{
				case SortMode.Unsorted:
					_arrowImage.Visible = false;
					break;
				case SortMode.Ascending:
					embeddedImageUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.SortableColumns.ArrowUp.gif");
					_arrowImage.ImageUrl = embeddedImageUrl;
					_arrowImage.Visible = true;
					break;
				case SortMode.Descending:
					embeddedImageUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.SortableColumns.ArrowDown.gif");
					_arrowImage.ImageUrl = embeddedImageUrl;
					_arrowImage.Visible = true;
					break;
				default:
					break;
			}
			
			if(_cssStyle != null)
				writer.Write("<th class='sortedColumn' style='" + _cssStyle + "'>");
			else
				writer.Write("<th class='sortedColumn'>");

			if (Enabled)
				_button.RenderControl(writer);
			else
				writer.Write("<span class='unsortedColumn'>" + ParsedColumnTitle + "</span>");

			writer.Write("&nbsp;");
			_arrowImage.RenderControl(writer);
			writer.Write("</th>");
		}
	}
}
