﻿if (netpay == null) { var netpay = new Object() }
netpay.Timezone =
{
    convert: function (rootNode) {
        var start = new Date();

        if (rootNode == null)
            rootNode = document.body
        var textNodes = getTextNodes(rootNode);
        for (var idx in textNodes) {
            var node = textNodes[idx];
            if (node.converted)
            {
                console.log("already converted: " + node.textContent);
                continue;
            }

            var dateFormat = parseDate(node.textContent);
            if (dateFormat.date == null || isNaN(dateFormat.date.getTime()))
            {
                console.log("invalid: " + node.textContent);
                continue;
            }
              
            var offset = dateFormat.date.getTimezoneOffset() * -1;
            dateFormat.date.setMinutes(dateFormat.date.getMinutes() + offset);
            node.textContent = formatDate(dateFormat);
            node.converted = true;

            console.log("converted: " + node.textContent);
        }

        var end = new Date();
        console.log("convert time: " + (end - start));
        console.log("filtered count: " + textNodes.length);

        function getTextNodes(root) {
            var node, result = [], walk = document.createTreeWalker(root, NodeFilter.SHOW_TEXT, null, false);
            while (node = walk.nextNode()) {
                var nodeText = node.textContent.trim();
                if (nodeText.length >= 8 && nodeText.length <= 22) 
                    if (!isNaN(nodeText[1]))
                        if (nodeText[2] == "/" || nodeText[2] == ":")
                            result.push(node);
            }
            return result;
        }

        function formatDate(dateFormat) {
            function pad(datePart) {
                if (datePart > 9)
                    return datePart;
                return "0" + datePart;
            }
            var format = pad(dateFormat.date.getDate()) + "/" + pad((dateFormat.date.getMonth() + 1)) + "/" + dateFormat.date.getFullYear() + " " + pad(dateFormat.date.getHours()) + ":" + pad(dateFormat.date.getMinutes());
            if (dateFormat.showSeconds)
                format += ":" + pad(dateFormat.date.getSeconds())
            return format;
        }

        function parseDate(date) {
            var dateTimeSplit = date.split(" ");
            var dateSplit = dateTimeSplit[0].split("/");
            var timeSplit = dateTimeSplit.length > 1 ? dateTimeSplit[1].split(":") : null;

            if (dateSplit.length != 3)
                return { "date": null, "showTime": false, "showSeconds": false };

            var dayOfMonth = new Number(dateSplit[0]);
            var month = new Number(dateSplit[1]) - 1;
            var year = new Number(dateSplit[2].length == 4 ? dateSplit[2] : "20" + dateSplit[2]);

            if (timeSplit == null)
                return { "date": new Date(year, month, dayOfMonth), "showTime": false, "showSeconds": false }

            var hour = new Number(timeSplit[0]);
            var minute = new Number(timeSplit[1]);
            var second = timeSplit.length == 3 ? new Number(timeSplit[2]) : 0;

            return { "date": new Date(year, month, dayOfMonth, hour, minute, second), "showTime": true, "showSeconds": timeSplit.length == 3 }
        }
    }
}

