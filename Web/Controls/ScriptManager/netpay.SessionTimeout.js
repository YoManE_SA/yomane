﻿if (netpay == null) { var netpay = new Object() }
netpay.SessionTimeout =
{
    alterTimerId: 0,
    logoutTimerId: 0,
    alertMessage: 'your session will expire in a few minutes',
    logoutMessage: 'your session expired',

    init: function (alertTimeout, logoutTimeout) {
        //alertTimeout = 1;
        //logoutTimeout = 2;
        if (netpay.SessionTimeout.alterTimerId) window.clearTimeout(netpay.SessionTimeout.alterTimerId);
        if (netpay.SessionTimeout.logoutTimerId) window.clearTimeout(netpay.SessionTimeout.logoutTimerId);
        netpay.SessionTimeout.alterTimerId = window.setTimeout('netpay.SessionTimeout.showAlert()', (alertTimeout * 1000 * 60));
        netpay.SessionTimeout.logoutTimerId = window.setTimeout('netpay.SessionTimeout.showLogout()', (logoutTimeout * 1000 * 60));
    },

    show: function (message, isSessionTimeout) {
        if ($("#netpaySessionAlert").length == 0) {
            $("body").append("<div title=\"\" id=\"netpaySessionAlert\"><p id=\"netpaySessionAlertMessage\" style=\"text-align: center;position:absolute;\">&nbsp;</p></div>");
            $("#netpaySessionAlert").dialog({
                "autoOpen": false,
                "modal": true,
                "resizable": false,
                "height": 'auto',
                "width": 335,
                "position": 'center',
                "buttons": { "Ok": function () { $("#netpaySessionAlert").dialog('close'); } },
                "show": { "effect": "drop", "direction": "up", "mode": "show", "duration": 300 },
                "hide": { "effect": "drop", "direction": "down", "mode": "hide", "duration": 300 },
                "close": function (event, ui) { if (isSessionTimeout) netpay.SessionTimeout.logoutRedirect(); else netpay.SessionTimeout.keepSession(); }
            });
        }
        $("#netpaySessionAlertMessage").html(message);
        $("#netpaySessionAlert").dialog("open");
    },

    logoutRedirect: function () {
        document.location = 'Logout.aspx';
    },

    keepSession: function () {
        top.netpay.ajaxApi.AjaxMethods.KeepSession(
            function (response) { netpay.SessionTimeout.init(response.Data[0], response.Data[1]); },
            function (response) { showLogout(); }
        );
    },

    showAlert: function () {
        netpay.SessionTimeout.show(netpay.SessionTimeout.alertMessage, false);
    },

    showLogout: function () {
        netpay.SessionTimeout.show(netpay.SessionTimeout.logoutMessage, true);
    }

}
