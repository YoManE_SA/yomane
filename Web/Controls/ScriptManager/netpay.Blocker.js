﻿if (netpay == null) { var netpay = new Object() }

netpay.Blocker =
{
	enabled: true,

	blockElement: function (blockedElement, content) 
	{
		if (typeof (blockedElement) == "string")
			blockedElement = document.getElementById(blockedElement);
		if (blockedElement == null) 
		{
			alert("Blocked element not found");
			return;
		}

		var blockerElement = document.getElementById("divElementBlocker");
		var contentElement = document.getElementById("divElementBlockerContent");
		if (blockerElement == null) 
		{
			// create block layrer
			var body = document.getElementsByTagName("body")[0];
			blockerElement = document.createElement("div");
			blockerElement.id = "divElementBlocker";
			blockerElement.style.position = "absolute";
			blockerElement.style.backgroundColor = "#ffffff";
			blockerElement.style.display = "block";
			blockerElement.style.opacity = 0.75;
			blockerElement.style.MozOpacity = 0.75;
			blockerElement.style.filter = "alpha(opacity='75')";
			body.appendChild(blockerElement);

			contentElement = document.createElement("div");
			contentElement.id = "divElementBlockerContent";
			contentElement.style.position = "absolute";
			contentElement.style.display = "block";
			body.appendChild(contentElement);
		}

		// set blocker
        var position = netpay.Common.findOffset(blockedElement);
		blockerElement.style.top = position.top + "px";
		blockerElement.style.left = position.left + "px";
		blockerElement.style.width = blockedElement.clientWidth + "px";
		blockerElement.style.height = blockedElement.clientHeight + "px";
		contentElement.style.top = position.top + "px";
		contentElement.style.left = position.left + "px";
		contentElement.style.width = blockedElement.clientWidth + "px";
		contentElement.style.height = blockedElement.clientHeight + "px";
		contentElement.innerHTML = "<table style='height:100%;width:100%;' border='0'><tr><td align='center' valign='top' style='color: #666666; font-weight: normal; font-size: 14px; font-weight:bold;'><div style='background-color: white !important; width: 400px; direction: rtl; padding: 20px; margin-top: 10%; border: 1px solid maroon;'>" + content + "</div></td></tr></table>";
	},

	show: function (textMessage) 
	{
		if (netpay.Blocker.enabled) 
		{
			document.getElementById("divMainBlockerBackground").style.display = "block";
			document.getElementById("divMainBlockerProgressContainer").style.display = "block";
			if (textMessage) 
			{
				document.getElementById("divMainBlockerProgressText").innerHTML = textMessage;
				document.getElementById("divMainBlockerProgressText").style.display = "block";
			}
			$("#divMainBlockerProgressContainer").center();
		}
	},

	hide: function () 
	{
		if (netpay.Blocker.enabled) 
		{
			document.getElementById("divMainBlockerBackground").style.display = "none";
			document.getElementById("divMainBlockerProgressContainer").style.display = "none";		
		}
	},

	init: function (isVisible, options) 
	{
		// Pass true to gray out screen, false to ungray  
		// options are optional.  This is a JSON object with the following (optional) properties  
		// opacity:0-100         
		// Lower number = less grayout higher = more of a blackout   
		// zindex: #             
		// HTML elements with a higher zindex appear on top of the gray out  
		// bgcolor: (#xxxxxx)   
		// Standard RGB Hex color code 
		// grayOut(true, {'zindex':'50', 'bgcolor':'#0000FF', 'opacity':'70'});  
		// Because options is JSON opacity/zindex/bgcolor are all optional and can appear  
		// in any order.  Pass only the properties you need to set.  
		var options = options || {};
		var zindex = options.zindex || 50;
		var opacity = options.opacity || 70;
		var opaque = (opacity / 100);
		var bgcolor = options.bgcolor || '#000000';
		var blocker = document.getElementById('divMainBlockerBackground');

		// Calculate the page width and height
		if (document.body && (document.body.scrollWidth || document.body.scrollHeight)) 
		{
			var pageWidth = document.body.scrollWidth - 20;
			var pageHeight = document.body.scrollHeight - 20;
		}
		else if (document.body.offsetWidth) 
		{
			var pageWidth = document.body.offsetWidth;
			var pageHeight = document.body.offsetHeight;
		}

		// setup blocker
		if (blocker == null) 
		{
			// The dark layer doesn't exist, it's never been created.  So we'll    
			// create it here and apply some basic styles.   
			// If you are getting errors in IE see: http://support.microsoft.com/default.aspx/kb/927917    

			// create block layrer
			var body = document.getElementsByTagName("body")[0];
			var blockerLayer = document.createElement('div'); // Create the layer.        
			blockerLayer.style.position = 'absolute'; // Position absolutely        
			blockerLayer.style.top = '0px'; // In the top        
			blockerLayer.style.left = '0px'; // Left corner of the page        
			blockerLayer.style.overflow = 'hidden'; // Try to avoid making scroll bars                    
			blockerLayer.style.display = 'none'; // Start out Hidden
			blockerLayer.id = 'divMainBlockerBackground'; // Name it so we can find it later

			// create progress animation
			var progressContainer = document.createElement('div');
			progressContainer.style.position = 'absolute';
			progressContainer.style.top = ((pageHeight / 2) - 50) + 'px';
			progressContainer.style.left = ((pageWidth / 2) - 50) + 'px';
			progressContainer.style.zIndex = zindex + 1;
			progressContainer.style.display = "none";
			progressContainer.style.border = "1px solid #FECE7A";
			progressContainer.style.backgroundColor = 'white';
			progressContainer.id = 'divMainBlockerProgressContainer';

			var progressImage = document.createElement('img');
			progressImage.src = "/NPCommon/Images/ajax-loader.gif";
			progressImage.id = 'divMainBlockerProgressImage';

			var progressMessage = document.createElement('div');
			progressMessage.style.padding = '5px;';
			progressMessage.id = 'divMainBlockerProgressText';

			// append blocker
			progressContainer.appendChild(progressImage);
			progressContainer.appendChild(progressMessage);
			body.appendChild(progressContainer);
			body.appendChild(blockerLayer);
			blocker = document.getElementById('divMainBlockerBackground'); // Get the object.
		}

		if (isVisible) 
		{
			//set the shader to cover the entire page and make it visible.
			blocker.style.opacity = opaque;
			blocker.style.MozOpacity = opaque;
			blocker.style.filter = 'alpha(opacity=' + opacity + ')';
			blocker.style.zIndex = zindex;
			blocker.style.backgroundColor = bgcolor;
			blocker.style.width = pageWidth + 'px'; ;
			blocker.style.height = pageHeight + 'px'; ;
		}
	}
}

$(document).ready
(
	function () 
	{
		netpay.Blocker.init(true, { 'opacity': '00' });
		
		var spinnerImage = document.createElement('img'); 
		spinnerImage.src = "/NPCommon/Images/ajax-loader.gif"; 
		spinnerImage.style.display = 'none';
		document.body.appendChild(spinnerImage)
	}
);