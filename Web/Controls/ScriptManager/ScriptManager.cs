﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web.Ajax;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:ScriptManager runat=server></{0}:ScriptManager>")]
	public class ScriptManager : WebControl
	{
        private static string _ajaxApi = null;

        public static string AjaxApi
        {
            get 
            {
                if (_ajaxApi == null)
                {
                    StringBuilder apiBuilder = new StringBuilder();
                    apiBuilder.AppendLine("<script type=\"text/javascript\">");
                    apiBuilder.AppendLine("if (netpay == null) { var netpay = {} };");
                    apiBuilder.AppendLine("if (netpay.ajaxApi == null) { netpay.ajaxApi = {} };");

                    Assembly assembly = Assembly.GetExecutingAssembly();
                    List<Type> ajaxClasses = assembly.GetTypes().Where(t => t.Namespace == "Netpay.Web.Ajax").Where(t => t.GetCustomAttributes(typeof(AjaxClass), false).Count() > 0).ToList();
                    foreach (Type ajaxClass in ajaxClasses)
                    {
                        apiBuilder.AppendLine("netpay.ajaxApi." + ajaxClass.Name + " = ");
                        apiBuilder.AppendLine("{");

                        List<MethodInfo> ajaxMethods = ajaxClass.GetMethods(BindingFlags.Public | BindingFlags.Static).Where(m => m.GetCustomAttributes(typeof(AjaxMethod), false).Count() > 0).ToList();
                        foreach (MethodInfo ajaxMethod in ajaxMethods)
                        {
                            StringBuilder functionParamsBuilder = new StringBuilder();
                            foreach (ParameterInfo parameter in ajaxMethod.GetParameters())
                                functionParamsBuilder.Append(parameter.Name + ", ");
                            functionParamsBuilder.Append("successHandler, errorHandler, context");

                            StringBuilder jsonParamsBuilder = new StringBuilder();
                            foreach (ParameterInfo parameter in ajaxMethod.GetParameters())
                                jsonParamsBuilder.Append("'" + parameter.Name + "':" + parameter.Name + ", ");
                            if (jsonParamsBuilder.Length > 1) jsonParamsBuilder.Remove(jsonParamsBuilder.Length - 2, 2);

                            apiBuilder.AppendLine();
                            apiBuilder.Append(ajaxMethod.Name);
                            apiBuilder.Append(":function ");
                            apiBuilder.Append("(");
                            apiBuilder.Append(functionParamsBuilder.ToString());
                            apiBuilder.Append(")");
                            apiBuilder.Append("{");
                            apiBuilder.Append("netpay.Ajax.call('" + ajaxClass.Name + "', '" + ajaxMethod.Name + "', {" + jsonParamsBuilder + "}, successHandler, errorHandler, context);");
                            apiBuilder.Append("},");
                        }

                        apiBuilder.Remove(apiBuilder.Length - 1, 1);
                        apiBuilder.AppendLine("}");
                    }
                    apiBuilder.AppendLine("</script>");

                    _ajaxApi = apiBuilder.ToString();
                }
                
                return ScriptManager._ajaxApi; 
            }
        }
        public ScriptManager() { EnableBlocker = true; }
        public bool EnableSessionAlerts { get; set; }
        public bool EnableBlocker { get; set; }
		
		public override void RenderControl(HtmlTextWriter writer)
		{
			//base.RenderControl(writer);

			writer.WriteLine("<script type=\"text/javascript\">");
			writer.WriteLine("var clientContext = new Object();");
			writer.WriteLine(string.Format("clientContext.currentDirection = \"{0}\";", WebUtils.CurrentDirection));
			writer.WriteLine(string.Format("clientContext.currentAlignment = \"{0}\";", WebUtils.CurrentAlignment));
			writer.WriteLine(string.Format("clientContext.currentReverseAlignment = \"{0}\";", WebUtils.CurrentReverseAlignment));
			writer.WriteLine(string.Format("clientContext.currentLanguageShort = \"{0}\";", WebUtils.CurrentLanguageShort));
			writer.WriteLine(string.Format("clientContext.themeFolder = \"{0}\";", WebUtils.CurrentDomain.ThemeFolder));
			//writer.WriteLine(string.Format("clientContext.currentDateFormat = \"{0}\";", WebUtils.CurrentDateFormat.ToLower().Replace("yyyy", "yy")));
			writer.WriteLine("clientContext.currentDateFormat = \"dd/mm/yy\";");
			writer.WriteLine("</script>");

            // all scripts must register in AssemplyInfo.cs
			writer.WriteLine(string.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.ScriptManager.netpay.Common.js")));
			if (EnableBlocker) writer.WriteLine(string.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.ScriptManager.netpay.Blocker.js")));
			writer.WriteLine(string.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.ScriptManager.netpay.Ajax.js")));
            writer.WriteLine(string.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.ScriptManager.netpay.Timezone.js")));
            if (EnableSessionAlerts && WebUtils.IsLoggedin)
            {
                writer.WriteLine(string.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.ScriptManager.netpay.SessionTimeout.js")));
                writer.WriteLine("<script type=\"text/javascript\">");
                writer.WriteLine("$(document).ready(function(){");
                writer.WriteLine(string.Format("netpay.SessionTimeout.alertMessage = '{0}';", Resources.Common.SessionTimeoutAlert));
                writer.WriteLine(string.Format("netpay.SessionTimeout.logoutMessage = '{0}';", Resources.Common.SessionLogoutAlert));
                writer.WriteLine(string.Format("netpay.SessionTimeout.init({0}, {1});", WebUtils.AlertSessionTimeout, WebUtils.SessionTimeout));
                //writer.WriteLine(string.Format("netpay.SessionTimeout.init({0}, {1});", 1, 2));
                writer.WriteLine("});</script>");
            }
            
            writer.WriteLine("<script type=\"text/javascript\">");
            writer.WriteLine("window.addEventListener('beforeunload', function(){");
            writer.WriteLine("netpay.SessionTimeout.keepSession();");
            writer.WriteLine("});</script>");

            // render ajax methods
            writer.Write(AjaxApi);
		}		
	}
}