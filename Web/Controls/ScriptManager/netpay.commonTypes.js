﻿if (netpay == null) { var netpay = {} }

netpay.commonTypes = 
{
	Currency:
	{
		UnsignedUnknown: 255,
		Unknown: -1,
		ILS: 0,
		USD: 1,
		EUR: 2,
		GBP: 3,
		AUD: 4,
		CAD: 5,
		JPY: 6,
		NOK: 7,
		PLN: 8,
		MXN: 9,
		ZAR: 10,
		RUB: 11,
		TRY: 12,
		CHF: 13,
		INR: 14
	},
	
	PaymentMethodType: 
	{
		Unknown: 0,
		System: 1,
		CreditCard: 2,
		ECheck: 3, 
		BankTransfer: 4, 
		PhoneDebit: 5,
		Wallet: 6,
		ExternalWallet: 7		
	}, 

	CreditType: 
	{
		Unknown: -1,
		Refund: 0,
		Regular: 1,
		DelayedCharge: 2,
		CreditCharge: 6,
		Installments: 8,
		Debit: 30,
		Credit: 31	
	},

	TransactionSource: 
	{
		Unknown: -1,
		MobileWeb: 40
	},

	TransactionType: 
	{
		Unknown: -1,
		Capture: 0,
		Authorization: 1,
		AuthorizationCapture: 2,
		StoredCreditCardCapture: 3,
		PendingCapture: 4
	}
}