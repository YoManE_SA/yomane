﻿if (netpay == null) { var netpay = {} }

netpay.Common =
{
	toggleTransactionInfo: function (transactionId, paymentMethod, isShowAskRefund, backgroundColor, transactionStatus, theme) {
		var expandImage = document.getElementById('expandImage-' + transactionId)
		var infoContainer = document.getElementById('infoContailner-' + transactionId)

		if (infoContainer.style.display == '') {
			infoContainer.style.display = 'none';
			expandImage.src = theme + '/images/plus.png';
		}
		else {
			infoContainer.style.display = '';
			expandImage.src = theme + '/images/minus.png';
		}

		if (infoContainer.innerHTML == '') {
			urlAddress = 'TransDetails.aspx';
			infoContainer.innerHTML = '<div id="transRowLoader_' + transactionId + '"><img src="images/ajax-loader.gif" style="padding:10px;" /></div><iframe framespacing="0" onload="netpay.Common.toggleTransactionInfoClearLoader(' + transactionId + '); this.height=this.contentWindow.document.body.scrollHeight;" scrolling="no" marginwidth="0" frameborder="0" width="100%"  src="' + urlAddress + '?transactionID=' + transactionId + '&PaymentMethod=' + paymentMethod + '&backgroundColor=' + escape(backgroundColor) + '&isShowAskRefund=' + isShowAskRefund + '&transactionStatus=' + transactionStatus + '"></iframe>';
		}
		else {
			infoContainer.innerHTML = '';
		}
	},

	toggleTransactionInfoClearLoader: function (transactionId) {
		var item = document.getElementById("transRowLoader_" + transactionId);
		if (item) item.style.display = "none";
	},

	// default path is the current path
	// for global cookies enter "/" for path
	createCookie: function (name, value, days, path) {
		if (path == null || path == "") {
			pathProperty = "";
		}
		else {
			pathProperty = "; path=" + path;
		}

		if (days > 0) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			var expiresProperty = "; expires=" + date.toGMTString();
		}
		else {
			var expiresProperty = "";
		}

		document.cookie = name + "=" + value + expiresProperty + pathProperty;
	},

	readCookie: function (name) {
		var nameProperty = name + "=";
		var cookieArray = document.cookie.split(';');
		for (var cookieIndex = 0; cookieIndex < cookieArray.length; cookieIndex++) {
			var currentCookie = cookieArray[cookieIndex];

			while (currentCookie.charAt(0) == ' ')
				currentCookie = currentCookie.substring(1, currentCookie.length);

			if (currentCookie.indexOf(nameProperty) == 0)
				return currentCookie.substring(nameProperty.length, currentCookie.length);
		}

		return null;
	},

	eraseCookie: function (name) {
		netpay.webControls.utils.createCookie(name, "", -1);
	},

	findPosition: function (element) {
		if (typeof (element) == "string")
			element = $("#" + element);
		else
			element = $(element);

		return element.position();
	},

	findOffset: function (element) {
		if (typeof (element) == "string")
			element = $("#" + element);
		else
			element = $(element);

		return element.offset();
	},

	emptyIfUndefined: function (source) {
		if (!netpay.Common.isDefined(source))
			return "";
		else
			return source;
	},

	valueIfUndefined: function (source, value) {
		if (!netpay.Common.isDefined(source))
			return value;
		else
			return source;
	},

	isDefined: function (source) {
		return typeof (source) !== "undefined";
	},

	// obsolete
	openPopupWindow: function (url, name, width, height, scrollBars) {
		placeLeft = screen.width / 2 - width / 2
		placeTop = screen.height / 2 - height / 2
		window.open(url, name, 'TOP=' + placeTop + ',LEFT=' + placeLeft + ',WIDTH=' + width + ',HEIGHT=' + height + ',scrollbars=' + scrollBars);
	},

	alert: function (message, title) {
		if ($("#netpayCommonAlert").length == 0) {
			$("body").append("<div title=\"\" id=\"netpayCommonAlert\"><p id=\"netpayCommonAlertMessage\">&nbsp;</p></div>");
			$("#netpayCommonAlert").dialog({ "autoOpen": false, "modal": true, "resizable": false, "height": 230, "width": 375, "buttons": { "Ok": function () { $("#netpayCommonAlert").dialog('close'); } }, "show": { "effect": "drop", "direction": "up", "mode": "show", "duration": 300 }, "hide": { "effect": "drop", "direction": "down", "mode": "hide", "duration": 300} });
		}

		$("#netpayCommonAlertMessage").html(message);
		$("#netpayCommonAlert").dialog("option", "title", title);
		$("#netpayCommonAlert").dialog("open");
	},

	openPopup: function (content, windowId, anchorElement, anchorTopOffset, anchorLeftOffset) {
		if (windowId == null)
			windowId = "netpayCommonPopup";

		if ($("#" + windowId).length == 0)
			$("body").append("<div ispopupwindow='true' id='" + windowId + "' style='display:block; position:absolute;'></div>");

		var popup = $("#" + windowId);
		popup.html(content);
		popup.fadeIn("fast");

		if (anchorElement == null)
			popup.center();
		else {
			if (typeof (anchorElement) == "string")
				anchorElement = document.getElementById(anchorElement);
			if (anchorTopOffset == null)
				anchorTopOffset = 0;
			if (anchorLeftOffset == null)
				anchorLeftOffset = 0;
			var position = netpay.Common.findPosition(anchorElement);
			popup.css("top", position.top + anchorTopOffset);
			popup.css("left", position.left + anchorLeftOffset);
		}
	},

	// reference: event obj or window id
	closePopup: function (reference) {
		var popup = null;
		if (typeof (reference) == "string")
			popup = $('#' + reference);
		else {
			var source = reference.srcElement == null ? reference.currentTarget : reference.srcElement;
			popup = $(source).closest("div[ispopupwindow='true']");
		}

		popup.fadeOut("fast");
	},

	toDelimitedString: function (collection, delimiter) {
		if (collection == null)
			return "";

		var delimited = "";
		for (var idx = 0; idx < collection.length; idx++)
			delimited += collection[idx] + delimiter;
		delimited = delimited.substring(0, delimited.length - 1)

		return delimited;
	},

	toQueryString: function (collection) {
		var prepared = new Array();
		for (var currentItem in collection)
			prepared.push(escape(currentItem + "=" + collection[currentItem]));

		return netpay.Common.toDelimitedString(prepared, "&");
	},

	fromQueryString: function (queryString) {
		var result = {};
		var temp = queryString.split("&");
		for (var idx in temp) {
			var pairSplit = temp[idx].split("=");
			var key = unescape(pairSplit[0]);
			var value = unescape(pairSplit[1]);
			result[key] = value;
		}

		return result;
	},

	addQueryParams: function (fullUrl, paramsCollecion) {
		for (var key in paramsCollecion)
			fullUrl = netpay.Common.setURLValue(fullUrl, key, paramsCollecion[key]);

		return fullUrl;
	},

	setURLValue: function (fullUrl, key, value) {
		var idx = fullUrl.indexOf(key + "=")
		if (idx < 1) {
			if (!value)
				return fullUrl;

			return fullUrl + (fullUrl.indexOf('?') > -1 ? '&' : '?') + key + "=" + value;
		}

		var idxEnd = fullUrl.indexOf("&", idx);
		if (idxEnd < 1)
			idxEnd = fullUrl.length;

		return fullUrl.substr(0, idx + key.length + 1) + value + fullUrl.substr(idxEnd);
	},

	toExtendedString: function (source) {
		var result = "";
		for (var currentProperty in source)
			result += currentProperty + " = " + source[currentProperty] + "\n";

		return result;
	},

	toJson: function (collection) {
		var prepared = new Array();
		for (var currentItem in collection) {
			var itemValue = collection[currentItem].toString();
			itemValue = itemValue.replace(/\\/g, "\\\\");
			itemValue = itemValue.replace(/'/g, "\\'");
			prepared.push("'" + currentItem + "':'" + itemValue + "'");
		}

		return "{" + netpay.Common.toDelimitedString(prepared, ",") + "}";
	},

	print: function (html) {
		// remove excluded elements
		var prepared = $("<div>" + html + "</div>");
		prepared.find("[printExclude='true']").remove();

		// popup
		var popup = window.open("", "_blank", "fullscreen=no, toolbar=no, directories=no, height=10, width=10, location=no, menubar=no, resizable=no, scrollbars=no, status=no, titlebar=no");
		popup.document.open();
		popup.document.write(prepared.html());
		popup.document.close();
		popup.print();
		popup.close();
	}
}

$.fn.center = function ()
{
	this.css("position", "absolute");
	this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
	this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
	
	return this;
}

$.fn.defined = function () 
{
    return this.length !== 0;
}


$(document).ready(function () 
{
	if (typeof(clientContext) === "undefined")
		return;

	if (typeof ($.datepicker) === "undefined")
	    return;

	if (clientContext.currentLanguageShort == "en")
		$.datepicker.setDefaults($.datepicker.regional['']);
	else
		$.datepicker.setDefaults($.datepicker.regional[clientContext.currentLanguageShort]);
});