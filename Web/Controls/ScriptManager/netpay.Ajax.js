﻿if (netpay == null) { var netpay = new Object() }

netpay.Ajax =
{
	call: function (ajaxClass, ajaxMethod, args, successHandler, errorHandler, context)
	{
		// context default
		if (context == null)
			context = new Object();

		context.successHandler = successHandler;
		context.errorHandler = errorHandler;

		// data
		var data = new Object();
		data.ajaxClass = ajaxClass;
		data.ajaxMethod = ajaxMethod;
		data.args = netpay.Common.toJson(args);

		// call
		jQuery.ajax({ async: true, cache: false, type: "POST", data: data, context: context, dataType: "json", url: "AjaxHandler.axd", success: netpay.Ajax.successHandler, error: netpay.Ajax.errorHandler })
	},

	successHandler: function (response)
	{
		if (response.Result == netpay.Ajax.result.notLoggedIn)
		{
			top.location.href = top.location.href;
			return;
		}

		if (response.Result == netpay.Ajax.result.noPermission)
		{
			netpay.Common.alert(response.ResultDescription);
			return;
		}

		if (response.Result == netpay.Ajax.result.error)
		{
			netpay.Common.alert(response.ResultDescription);
			return;
		}

		var context = this.context;
		if (context == undefined)
			context = this;

		if (context.successHandler == null)
			netpay.Ajax.defaultSuccessHandler();
		else
			context.successHandler(response);
	},

	defaultSuccessHandler: function ()
	{
		netpay.Common.alert("Operation completed successfuly");
	},

	errorHandler: function (request, textStatus, errorThrown)
	{
		if (this.errorHandler == null)
			netpay.Ajax.defaultErrorHandler();
		else
			this.errorHandler();
	},

	defaultErrorHandler: function ()
	{
		netpay.Common.alert("An error has occured during this operation.");
	},

	result: { "undefined": 0, "success": 1, "error": 2, "notLoggedIn": 3, "methodAccessDenied": 4, "noPermission": 5 },

    showLoaderOnAjaxCall: true,

    showLoader: function ()
	{
		if (netpay.Ajax.showLoaderOnAjaxCall)
		    if (netpay.Blocker) netpay.Blocker.show();
	},

    hideLoader: function ()
	{
		if (netpay.Ajax.showLoaderOnAjaxCall)
		    if (netpay.Blocker) netpay.Blocker.hide();
	}
}

$(document).ready
(
	function () {
	    $(document).bind("ajaxStart", function () { netpay.Ajax.showLoader(); })
	    $(document).bind("ajaxStop", function () { netpay.Ajax.hideLoader(); })
	}
);