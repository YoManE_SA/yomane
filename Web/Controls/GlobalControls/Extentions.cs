﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Web.Controls.GlobalControls
{
	public static class Extentions
	{
		public static string ToHtmlColor(this System.Drawing.Color color)
		{
			return string.Format("#{0:X2}{1:X2}{2:X2}", color.R, color.G, color.B);
		}

		public static string ToHtmlRGBA(this System.Drawing.Color color)
		{
			return string.Format("rgba({0},{1},{2},{3:0.0})", color.R, color.G, color.B, color.A / 255f);
		}
	}
}