﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls.GlobalControls.JQueryControls
{
    public static class JQueryControlHelper
    {
        public const string JQueryJS = "core/jquery-1.9.0.js";
        public const string JQueryUIJS = "Plugins/jquery-ui/jquery-ui-1.10.4.custom.min.js";
        public static string GetJqueryFilePath(System.Web.UI.Page page)
        {
            return page.ResolveClientUrl(@"~/Scripts/");
        }

        public static void AddAjaxUrlParam(System.Web.UI.Page page, string paramName, string paramValue)
        {
            var urlCol = page.Items["AjaxUrl"] as System.Collections.Specialized.NameValueCollection;
            if (urlCol == null) page.Items["AjaxUrl"] = urlCol = HttpUtility.ParseQueryString("");
            urlCol.Add(paramName, paramValue);
        }
        public static void AddAjaxUrlParam(System.Web.UI.Page page, System.Collections.Specialized.NameValueCollection values)
        {
            var urlCol = page.Items["AjaxUrl"] as System.Collections.Specialized.NameValueCollection;
            if (urlCol == null) page.Items["AjaxUrl"] = urlCol = HttpUtility.ParseQueryString("", System.Text.Encoding.UTF32);
            urlCol.Add(values);
        }

        public static void RenderHistory(System.Web.UI.Page page)
        {
            var urlCol = page.Items["AjaxUrl"] as System.Collections.Specialized.NameValueCollection;
            if (urlCol == null) return;
            if (System.Web.UI.ScriptManager.GetCurrent(page).IsInAsyncPostBack)
            {
                var sb = new System.Text.StringBuilder();
                for (int i = 0; i < urlCol.Count; i++)
                    sb.AppendFormat("&{0}={1}", urlCol.GetKey(i), System.Web.HttpUtility.UrlEncode(urlCol[i]));
                sb[0] = '?';
                RegisterJQueryStartUp(page, "history.replaceState(null, null, '" + page.Request.Url.LocalPath + sb.ToString() + "');");
            }
        }

        public static bool RegisterScript(System.Web.UI.Page page, string jsFile)
        {
            string scriptFile = (GetJqueryFilePath(page) + jsFile);
            if (!page.ClientScript.IsClientScriptIncludeRegistered(scriptFile))
            {
                page.ClientScript.RegisterClientScriptInclude(scriptFile, scriptFile);
                var sm = System.Web.UI.ScriptManager.GetCurrent(page);
                if (sm.IsInAsyncPostBack)
                {
                    System.Web.UI.ScriptManager.RegisterClientScriptInclude(page, page.GetType(), scriptFile, scriptFile);
                    return true;
                }
                return true;
            }
            return false;
        }

        public static bool RegisterCss(System.Web.UI.Page page, string url)
        {
            var found = page.Header.Controls.OfType<System.Web.UI.HtmlControls.HtmlLink>().Any(m => m.Href == url);
            if (!found)
            {
                var link = new System.Web.UI.HtmlControls.HtmlLink();
                link.Href = url;
                link.Attributes["type"] = "text/css";
                link.Attributes["rel"] = "stylesheet";
                page.Header.Controls.Add(link);
            }
            return !found;
        }

        //All references to that function are commented, 
        //Beacuse the projects that uses this JQueryControls
        //Class , Jquery is already registered.
        public static void RegisterJQuery(System.Web.UI.Page page)
        {
            bool registerLoc = JQueryControlHelper.RegisterScript(page, JQueryJS);
            JQueryControlHelper.RegisterScript(page, JQueryUIJS);
            string JQTheme_Css = System.Configuration.ConfigurationManager.AppSettings["JQTheme_Css"].EmptyIfNull();
            if (!string.IsNullOrEmpty(JQTheme_Css)) JQueryControlHelper.RegisterCss(page, JQTheme_Css);
            if (registerLoc)
            {
                string[] locItems = System.Configuration.ConfigurationManager.AppSettings["JQLocalization"].EmptyIfNull().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var s in locItems) JQueryControlHelper.RegisterScript(page, s);
            }
        }

        public static void RegisterJQueryStartUp(System.Web.UI.Control control, string script, string scriptName = null)
        {            
            if (scriptName == null) scriptName = control.ClientID + "_Load";
            System.Web.UI.ScriptManager.RegisterStartupScript(control, control.GetType(), scriptName, script, true);
        }
    }

    [System.Web.UI.ParseChildren(false)]
    public class Button : System.Web.UI.WebControls.WebControl
    {
        public string Text { get; set; }
        public string Icon { get; set; }
        public string GroupCssClass { get; set; }
        public Button() : base(System.Web.UI.HtmlTextWriterTag.Button) { }

        protected override void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
        {
            base.AddAttributesToRender(writer);
            //writer.AddAttribute("type", "button");
            writer.AddAttribute("value", Text);
            //if (IsEnabled) writer.AddAttribute("onclick", "$('#" + ClientID + "Menu').slideToggle('fast').position( { of: $('#" + ClientID + "'), my: 'right top', at: 'right bottom', collision: 'none' } );", false);
            //else writer.AddAttribute(System.Web.UI.HtmlTextWriterAttribute.Disabled, "disabled");
        }

        protected override void OnPreRender(EventArgs e)
        {
            EnsureID();
            JQueryControlHelper.RegisterJQueryStartUp(this, "$(document).click(function(e){ if (!$(e.target).parents().andSelf().is('#" + ClientID + ",#" + ClientID + "Menu')) $('#" + ClientID + "Menu:visible').slideToggle('fast'); });");
            base.OnPreRender(e);
        }

        protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<div id=\"" + ClientID + "Menu\" style=\"position:absolute;display:none;\">");
            base.RenderChildren(writer);
            writer.Write("</div>");
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<div class=\"btn-group " + GroupCssClass + "\">");
            if (CssClass == null || !CssClass.Contains("btn ")) CssClass = "btn " + CssClass;
            //if (CssClass == null || !CssClass.Contains(" dropdown-toggle")) CssClass = CssClass + " dropdown-toggle";
            AddAttributesToRender(writer);
            writer.AddAttribute("data-toggle", "dropdown");
            writer.RenderBeginTag(TagKey);
            writer.Write(Text);
            writer.RenderEndTag();
            writer.Write("<button data-toggle=\"dropdown\" class=\"btn " + CssClass + " dropdown-toggle\"><span class=\"caret\"></span></button>");
            this.RenderChildren(writer);
            writer.Write("</div>");
        }
    }

    [System.Web.UI.ParseChildren(true, "Items")]
    public class MenuButton : Button, System.Web.UI.IPostBackEventHandler
    {
        [System.ComponentModel.MergableProperty(false), System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerDefaultProperty), System.ComponentModel.DefaultValue(null)]
        public string CommandName { get; set; }
        public event System.Web.UI.WebControls.CommandEventHandler Command;
        public System.Web.UI.WebControls.MenuItemCollection Items { get; set; }
        public MenuButton() { Items = new System.Web.UI.WebControls.MenuItemCollection(); }

        public void AddRange(IEnumerable<System.Web.UI.WebControls.MenuItem> values)
        {
            foreach (var i in values) Items.Add(i);
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            string script = "$('#" + ClientID + "Menu').menu().hide();";
            //JQueryControlHelper.RegisterJQueryStartUp(this, script, "LoadPopupMenu");
        }

        protected virtual void OnCommand(System.Web.UI.WebControls.CommandEventArgs e)
        {
            if (Command != null) Command(this, e);
            base.RaiseBubbleEvent(this, e);
        }

        void System.Web.UI.IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            this.OnCommand(new System.Web.UI.WebControls.CommandEventArgs(this.CommandName, eventArgument));
        }

        protected override void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
        {
            if (Page != null) this.Page.ClientScript.RegisterForEventValidation(UniqueID);
            base.AddAttributesToRender(writer);
        }

        protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
        {
            if (Items != null && Items.Count > 0)
            {
                writer.Write("<ul id=\"" + ClientID + "Menu\"  class=\"dropdown-menu\">");
                foreach (System.Web.UI.WebControls.MenuItem Item in Items)
                {
                    if (Item.Text == "---")
                    {
                        writer.Write("<li><hr></li>");
                    }
                    else
                    {
                        string href = null;
                        if (!string.IsNullOrEmpty(Item.NavigateUrl)) href = ResolveClientUrl(Item.NavigateUrl);
                        else href = Page.ClientScript.GetPostBackClientHyperlink(this, Item.Value);
                        writer.Write("<li><a href=\"{1}\" onclick=\"$('#" + ClientID + "Menu').hide();\">{0}</a></li>", Item.Text, href, Item.Target);
                    }
                }
                writer.Write("</ul>");
            }
        }

    }
}