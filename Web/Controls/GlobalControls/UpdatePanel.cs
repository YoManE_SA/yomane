﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Web.Controls.GlobalControls
{
	public class UpdatePanel : System.Web.UI.UpdatePanel, System.Web.UI.IPostBackEventHandler
	{
		protected string UpdatePanelBlockJsPath = "";
		public enum JQAnimation { None, Fade, Slide, }
		public JQAnimation Animation { get; set; }
		public string CssClass { get; set; }
		public System.Web.UI.HtmlTextWriterTag HtmlTag { get; set; }
		public UpdatePanel() { Animation = JQAnimation.Fade; }

		protected System.Web.UI.Control getControlFromTrigerID(string triger, System.Web.UI.Control container)
		{
			while (container != null) {
				var ret = container.FindControl(triger);
				if (ret != null) return ret;
				container = container.NamingContainer;

			}
			return null;
		}

		protected override void OnPreRender(EventArgs e)
		{            
			if (Animation != JQAnimation.None) Attributes["Animation"] = Animation.ToString();
			Attributes["ShowShadow"] = "false";
			JQueryControls.JQueryControlHelper.RegisterScript(Page, UpdatePanelBlockJsPath + "UpdatePanelBlock.js");
			if (Visible) {
				foreach (var t in Triggers) {
					var controlID = getControlFromTrigerID((t as System.Web.UI.UpdatePanelControlTrigger).ControlID, this);
					Page.ClientScript.RegisterArrayDeclaration("PanelsUpdate_" + controlID.ClientID, "'" + ClientID + "'");
				}
			}
			base.OnPreRender(e);
		}


		void System.Web.UI.IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			int index = eventArgument.LastIndexOf('$');
			if (index > 0) this.RaiseBubbleEvent(this, new System.Web.UI.WebControls.CommandEventArgs(eventArgument.Substring(0, index), eventArgument.Substring(index + 1)));
			else this.RaiseBubbleEvent(this, new System.Web.UI.WebControls.CommandEventArgs(eventArgument, null));
		}

		protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
		{
			if (!IsInPartialRendering) {
				writer.AddAttribute(System.Web.UI.HtmlTextWriterAttribute.Id, this.ClientID);
				writer.AddAttribute(System.Web.UI.HtmlTextWriterAttribute.Class, CssClass);
				if (this.Attributes != null) this.Attributes.AddAttributes(writer);
				if (HtmlTag == System.Web.UI.HtmlTextWriterTag.Unknown) 
					HtmlTag = RenderMode == System.Web.UI.UpdatePanelRenderMode.Block ? System.Web.UI.HtmlTextWriterTag.Div : System.Web.UI.HtmlTextWriterTag.Span;
				writer.RenderBeginTag(HtmlTag);
				//base.RenderChildren(writer); //no good - also draw the div
				foreach (System.Web.UI.Control ctl in base.Controls) ctl.RenderControl(writer);
				writer.RenderEndTag();
			} else base.RenderChildren(writer);
		}
	}
}