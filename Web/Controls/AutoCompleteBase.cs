﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace Netpay.Web.Controls
{
    public abstract class AutoCompleteBase : System.Web.UI.WebControls.CompositeControl, System.Web.UI.ICallbackEventHandler
    {
        //The option to disable a button when cancelling a selection of an item from the 
        //Autocomplete list , TBD : Add the ID of the button that should be disabled.
        public bool DisableButtonOnKeyDown { get; set; }

        public virtual void RegisterJqueryStartUp()
        {
            GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(this,
                "$('#" + SearchBox.ClientID + "').autocomplete({ " +
                    "minLength: 2," +
                    "autoFocus: false," +
                    "source: function(request, response) { " + Page.ClientScript.GetCallbackEventReference(this, "request.term", "function(arg, context){ context(eval(arg)); }", "response") + "; }, " +
                    "select: function(event, ui) { $('#" + SearchBox.ClientID + "').val(ui.item ? ui.item.label : ''); $('#" + ValueField.ClientID + "').val(ui.item ? ui.item.value : ''); $('#" + IDLabel.ClientID + "').text(ui.item ? ui.item.value : ''); " + OnClientClick + "; return false; } " +
                "}).data('ui-autocomplete')._renderItem = function( ul, item ) { " +
                "	var aItem = $('<a>');" +
                "	if (item.statusColor) aItem.append($('<span style=\"float:left;margin-right:3px;margin-left:3px;background-color:' + item.statusColor + ';\">&nbsp;&nbsp;</span>'));" +
                "	if (item.itemType) aItem.append($('<span style=\"float:right;margin-right:3px;margin-left:3px;padding-right:3px;padding-left:3px;background-color:#c0c0c0;\">' + item.itemType.substr(0, 1) + '<span>'));" +
                "	if (item.imageUrl) aItem.append($('<img style=\"margin-right:3px;margin-left:3px;\" src=\"' + item.imageUrl + ' \" />'));" +
                "	aItem.append($('<span>' + item.label + '</span>'));" +
                "	return $('<li>').attr('data-value', item.value).append(aItem).appendTo(ul); };" +
                "$('#" + SearchBox.ClientID + "').keydown(function(e){ if($.inArray(e.keyCode,[9,13,16,17,18,19,20,27,35,36,37,38,39,40,91,93,224]) > -1) return; $('#" + ValueField.ClientID + "').val(''); $('#" + IDLabel.ClientID + "').text('-');" + (DisableButtonOnKeyDown ? "$('#" + ButtonClientIdToDisable + "').prop('disabled',true);" : "") + "});"
                );
        }

        public string ButtonClientIdToDisable { get; set; }

        public string SearchText { get; private set; }
        public string Text { get { return SearchBox.Text; } set { SearchBox.Text = value; } }

        public string PlaceHolder { get; set; }

        // Triger to Client side script that will run when selecting an item from the AutoComplete items.
        public string OnClientClick { get; set; }
        private System.Text.StringBuilder output;

        public AutoCompleteBase()
            : base()
        {
            CssClass = "input-group";
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            Controls.Add(new System.Web.UI.LiteralControl("<span class=\"input-group-addon\">"));
            Controls.Add(new System.Web.UI.WebControls.Label() { ClientIDMode = ClientIDMode.Predictable, ID = "lblID", CssClass = "textBox" });
            Controls.Add(new System.Web.UI.LiteralControl("</span>"));

            // Add TextBox with placeholder if necessary
            var textBox = new System.Web.UI.WebControls.TextBox() { ClientIDMode = ClientIDMode.Predictable, ID = "txtSearch", CssClass = "form-control" };
            if (PlaceHolder != null)
                textBox.Attributes.Add("placeholder", PlaceHolder);

            Controls.Add(textBox);

            Controls.Add(new System.Web.UI.WebControls.HiddenField() { ClientIDMode = ClientIDMode.Predictable, ID = "hdID" });
        }
        public System.Web.UI.WebControls.HiddenField ValueField { get { return FindControl("hdID") as System.Web.UI.WebControls.HiddenField; } }
        public System.Web.UI.WebControls.Label IDLabel { get { return FindControl("lblID") as System.Web.UI.WebControls.Label; } }
        public System.Web.UI.WebControls.TextBox SearchBox { get { return FindControl("txtSearch") as System.Web.UI.WebControls.TextBox; } }
        protected override System.Web.UI.HtmlTextWriterTag TagKey { get { return System.Web.UI.HtmlTextWriterTag.Div; } }

        protected override void OnPreRender(EventArgs e)
        {
            IDLabel.Text = ValueField.Value;
            if (IDLabel.Text == string.Empty) IDLabel.Text = "-";

            //if (SearchBox != null && !string.IsNullOrEmpty(SearchBox.Text))
            //    SearchBox.Text = "";

            if (IDLabel.Text == string.Empty && SearchBox != null && !string.IsNullOrEmpty(SearchBox.Text)) SearchBox.Text = "";

            RegisterJqueryStartUp();

            base.OnPreRender(e);
        }

        protected void appendItem(string id, string text, string category, System.Drawing.Color? statusColor, string imageUrl)
        {
            output.Append("{");
            output.AppendFormat(" \"value\":\"{0}\"", HttpUtility.JavaScriptStringEncode(id));
            output.AppendFormat(", \"label\":\"{0}\"", HttpUtility.JavaScriptStringEncode(text));
            if (category != null) output.AppendFormat(", \"itemType\":\"{0}\"", HttpUtility.JavaScriptStringEncode(category));
            if (imageUrl != null) output.AppendFormat(", \"imageUrl\":\"{0}\"", HttpUtility.JavaScriptStringEncode(imageUrl));
            if (statusColor != null) output.AppendFormat(", \"statusColor\":\"{0}\"", HttpUtility.JavaScriptStringEncode(System.Drawing.ColorTranslator.ToHtml(statusColor.Value)));
            output.Append(" },");
        }

        protected abstract void SearchItems();
        string System.Web.UI.ICallbackEventHandler.GetCallbackResult()
        {
            output = new System.Text.StringBuilder();
            output.Append("[");
            SearchItems();

            if (output.Length > 2) output.Remove(output.Length - 1, 1);
            output.Append("]");
            return output.ToString();
        }

        void System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
        {
            SearchText = eventArgument;
        }

        public void Clear()
        {
            if (ValueField != null)
                ValueField.Value = string.Empty;

            if (IDLabel != null)
                IDLabel.Text = string.Empty;

            if (SearchBox != null)
                SearchBox.Text = string.Empty;
        }
    }
}
