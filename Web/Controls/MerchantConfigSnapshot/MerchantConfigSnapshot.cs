﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Bll;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:MerchantConfigSnapshot runat=\"server\" />")]
	public class MerchantConfigSnapshot : WebControl
	{
		private Bll.Merchants.Merchant data;

		private string sTableCssClass = string.Empty;

		private List<MerchantConfigProperty> properties = new List<MerchantConfigProperty>();

		public Bll.Merchants.Merchant Merchant
		{
			get
			{
				return data;
			}
			set
			{
				data = value;
				properties.Clear();
				if (data != null)
				{
					var merchantRisk = Bll.Merchants.RiskSettings.Load(data.ID);
                    if (merchantRisk == null) merchantRisk = new Bll.Merchants.RiskSettings(data.ID);

                    ResourceManager rm = WebUtils.GetResource("MerchantConfig"); ;
					properties.Add(new MerchantConfigProperty("MerchantStatus", data.ActiveStatus.ToString(), rm, "GeneralGroup"));
					properties.Add(new MerchantConfigProperty("MerchantNumber", data.Number, rm));
					properties.Add(new MerchantConfigProperty("LegalName", data.LegalName, rm));

					//support
					properties.Add(new MerchantConfigProperty("NotifyMerchant", data.ProcessSettings.IsPassNotificationSentToMerchant, rm, "SupportGroup"));
					properties.Add(new MerchantConfigProperty("NotifyCustomer", data.ProcessSettings.IsPassNotificationSentToPayer, rm));
					properties.Add(new MerchantConfigProperty("Mail", "<a href=\"mailto:" + data.SupportEmail + "\">" + data.SupportEmail + "</a>", rm));
					properties.Add(new MerchantConfigProperty("Phone", data.SupportPhone, rm));
					string sUrls = string.Empty;
					foreach (string sUrl in data.Url.Split('\n'))
						sUrls += "<a target=\"_blank\" href=\"" + sUrl + "\">" + sUrl + "</a></br>";
					properties.Add(new MerchantConfigProperty("Website", sUrls, rm));

					//risk
					properties.Add(new MerchantConfigProperty("AllowedAmounts", (merchantRisk.AllowedAmounts != null ? merchantRisk.AllowedAmounts.ToDelimitedString(", ") : rm.GetString("AnyAmount")), rm, "RiskGroup"));
					properties.Add(new MerchantConfigProperty("IsCcWhitelistEnabled", merchantRisk.IsCreditCardWhitelistEnabled, rm));
					string sCountries = rm.GetString("None");
					if (merchantRisk.CountryBlacklist != null)
					{
						StringBuilder sbCountries = new StringBuilder();
						foreach (string sCountryIso in merchantRisk.CountryBlacklist)
						{
							var country = Country.Get(sCountryIso);
							if (country != null) sbCountries.AppendFormat(", {0}", country.Name);
						}
						if (sbCountries.Length > 2) sbCountries.Remove(0, 2);
						sCountries = sbCountries.ToString();
					}
					properties.Add(new MerchantConfigProperty("BlockedCountries", sCountries, rm));

					//recurring
					var dataRecurring = Bll.Transactions.Recurring.MerchantSettings.Load(data.ID);
					properties.Add(new MerchantConfigProperty("IsEnabled", (dataRecurring == null ? false : dataRecurring.IsEnabled), rm, "RecurringGroup"));
					if (dataRecurring != null)
					{
						properties.Add(new MerchantConfigProperty("IsEnabledFromTransPass", dataRecurring.IsEnabledFromTransPass, rm));
						properties.Add(new MerchantConfigProperty("IsEnabledModify", dataRecurring.IsEnabledModify, rm));
						properties.Add(new MerchantConfigProperty("MaxYears", dataRecurring.MaxYears, rm));
						properties.Add(new MerchantConfigProperty("MaxStages", dataRecurring.MaxStages, rm));
						properties.Add(new MerchantConfigProperty("MaxCharges", dataRecurring.MaxCharges, rm));
					}
				}
			}
		}

		public int MerchantID
		{
			get
			{
				return data == null ? 0 : data.ID;
			}
			set
			{
				Merchant = Bll.Merchants.Merchant.Load(value);
			}
		}

		public string TableCssClass
		{
			get
			{
				return sTableCssClass;
			}
			set
			{
				sTableCssClass = value;
			}
		}

		protected override void RenderContents(HtmlTextWriter output)
		{
			if (MerchantID > 0)
			{
				output.Write("<table class=\"" + sTableCssClass + "\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">");
				for (int i = 0; i < properties.Count; i++) output.Write(properties[i].ToString());
				output.Write("</table>");
			}
		}
	}
}