﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;

namespace Netpay.Web.Controls
{
	class MerchantConfigProperty
	{
		public string Name;
		public string Value;
		public string Group;

		private void FillData(string sName, string sValue, ResourceManager rm, string sGroup)
		{
			Name = (rm == null ? sName : rm.GetString(sName));
			Value = sValue;
			Group = (sGroup == null ? string.Empty : (rm == null ? sName : rm.GetString(sGroup)));
		}

		public MerchantConfigProperty(string Name, string Value, ResourceManager rm = null, string Group = null)
		{
			FillData(Name, Value, rm, Group);
		}

		public MerchantConfigProperty(string Name, bool Value, ResourceManager rm = null, string Group = null)
		{
			string sYes = (rm == null ? "Yes" : rm.GetString("Yes"));
			string sNo = (rm == null ? "No" : rm.GetString("No"));
			FillData(Name, Value ? sYes : sNo, rm, Group);
		}

		public MerchantConfigProperty(string Name, int Value, ResourceManager rm = null, string Group = null)
		{
			FillData(Name, Value.ToString(), rm, Group);
		}

		public MerchantConfigProperty(string Name, decimal Value, ResourceManager rm = null, string Group = null)
		{
			FillData(Name, Value.ToString("#,0.00"), rm, Group);
		}

		/// <summary>
		/// Returns formatted string (typically HTML) containing the property data
		/// </summary>
		/// <param name="GroupFormat">Format string with {0} as placeholder for Group</param>
		/// <param name="ValueFormat">Format string with {0} as placeholder for Name and {1} as placeholder for Value</param>
		/// <returns>Formatted string consisting of processed GroupFormat and ValueFormat</returns>
		public string ToString(string GroupFormat, string ValueFormat)
		{
			StringBuilder sb = new StringBuilder();
			if (!string.IsNullOrEmpty(Group)) sb.AppendFormat(GroupFormat, Group);
			sb.AppendFormat(ValueFormat, Name, Value);
			return sb.ToString();
		}

		/// <summary>
		/// Returns formatted string (HTML) containing the property data
		/// </summary>
		/// <returns>Formatted default string</returns>
		public override string ToString()
		{
			return ToString("<tr><th colspan=\"2\">{0}</th></tr>", "<tr><td width=\"50%\">{0}</td><td>{1}</td></tr>");
		}
	}
}
