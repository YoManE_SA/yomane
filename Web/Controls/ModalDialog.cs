﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Web.Controls
{
    public class ModalDialog : Dialog, System.Web.UI.IPostBackEventHandler
    {
        public event System.Web.UI.WebControls.CommandEventHandler DialogClose;

        public virtual Controls.GlobalControls.UpdatePanel UpdatePanel
        {
            get
            {
                return FindControl("UpdatePanel") as Controls.GlobalControls.UpdatePanel;
            }
        }

        public bool ChildrenAsTriggers { get { return UpdatePanel.ChildrenAsTriggers; } set { UpdatePanel.ChildrenAsTriggers = value; } }

        protected virtual void AddUpdatePanelToControls()
        {
            Controls.Add(new Controls.GlobalControls.UpdatePanel() { ID = "UpdatePanel", ChildrenAsTriggers = false, UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional, HtmlTag = System.Web.UI.HtmlTextWriterTag.Div, CssClass = "modal-content", Animation = GlobalControls.UpdatePanel.JQAnimation.None });
        }

        public ModalDialog()
        {
            CssClass = "modal fade";
            AddUpdatePanelToControls();
            UpdatePanel.ContentTemplateContainer.Controls.Add(new PanelContainer() { ID = "Header", CssClass = "modal-header" });
            Header.Controls.Add(new System.Web.UI.LiteralControl() { ID = "TitleHtml", ViewStateMode = System.Web.UI.ViewStateMode.Disabled });
            UpdatePanel.ContentTemplateContainer.Controls.Add(new PanelContainer() { ID = "Body", CssClass = "modal-body" });
            UpdatePanel.ContentTemplateContainer.Controls.Add(new PanelContainer() { ID = "Footer", CssClass = "modal-footer" });
        }

        protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<div class=\"modal-dialog\" id=\"{0}Modal\">", ClientID);
            base.RenderChildren(writer);
            writer.Write("</div>");
        }

        protected override void OnPreRender(EventArgs e)
        {
            TitleHtml.Text = "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>";
            if (!string.IsNullOrEmpty(Title))
                TitleHtml.Text += string.Format("<h4 class=\"modal-title\"><i class=\"fa {1} fa-2x box-size-font\"></i> {0}</h4>", Title, Icon);

            base.OnPreRender(e);
        }

        public string ShowJSCommand
        {
            get
            {
                return "$('#" + ClientID + "').off('hidden.bs.modal');$('#" + ClientID + "').on('hidden.bs.modal', function() {" + Page.ClientScript.GetPostBackClientHyperlink(this, "DialogClose") + ";}).modal('show');";
            }
        }

        public virtual string HideJSCommand { get { return "$('#" + ClientID + "').modal('hide');"; } }
        public virtual void RegisterShow() { Visible = true; UpdatePanel.Update(); GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(Page, ShowJSCommand, ClientID + "_Show"); }
        public virtual void RegisterHide() { GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(Body, HideJSCommand, ClientID + "_Hide"); /* Visible = false; */}

        public virtual void BindAndShow()
        {
            DataBind();
            RegisterShow();
        }

        public void BindAndUpdate()
        {
            DataBind();
            UpdatePanel.Update();
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            var e = args as System.Web.UI.WebControls.CommandEventArgs;
            if (e != null)
            {
                base.OnBubbleEvent(this, e);
                if (e.CommandName == "Close")
                {
                    RegisterHide();
                }
                else
                {
                    UpdatePanel.Update();
                }
                return true;
            }
            return base.OnBubbleEvent(source, args);
        }


        public void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument == "DialogClose")
            {
                if (DialogClose != null) DialogClose(this, new System.Web.UI.WebControls.CommandEventArgs("DialogClose", null));
                else base.OnBubbleEvent(this, new System.Web.UI.WebControls.CommandEventArgs("DialogClose", null));
            }
        }
    }
}
