﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:DeclinedTypeDropDown runat=server></{0}:DeclinedTypeDropDown>")]
	public class DeclinedTypeDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			var lst = new System.Collections.Generic.Dictionary<int, string>();
			lst.Add(1, "Authorization");
			lst.Add(2, "Capture");
			DataSource = lst;
			DataValueField = "Key";
			DataTextField = "Value";
			base.DataBind(raiseOnDataBinding);
		}
	}
}
