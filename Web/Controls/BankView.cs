﻿using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:BankView runat=server></{0}:BankView>")]
	public class BankView : WebControl
	{
		private int? _bankID = 0;
		private string _bankName = "";

		public int? BankID
		{
			get { return _bankID; }
			set { _bankID = value; }
		}

		public string BankName
		{
			get { return _bankName; }
			set { _bankName = value; }
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
            writer.Write(string.Format("<img src='/NPCommon/ImgDebitCompanys/23X12/{0}.gif' alt='' align='bottom' border='0' />&nbsp;{1}", _bankID, _bankName));
		}
	}
}
