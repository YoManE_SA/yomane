﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
    [ToolboxData("<{0}:InvoiceTypeDropDown runat=server></{0}:InvoiceTypeDropDown>")]
    public class InvoiceTypeDropDown : DropDownBase
    {
        
        protected override void DataBind(bool raiseOnDataBinding)
        {
            DataSource = Bll.Invoices.InvoiceType.Cache;
            DataValueField = "ID";
            DataTextField = "Text";
            base.DataBind(raiseOnDataBinding);
        }
    }
}
