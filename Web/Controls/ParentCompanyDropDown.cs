﻿using System;
using System.Web.UI;
using System.Linq;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
    [ToolboxData("<{0}:ParentCompanyDropDown runat=server></{0}:ParentCompanyDropDown>")]
    public class ParentCompanyDropDown : DropDownBase
    {
        protected override void DataBind(bool raiseOnDataBinding)
        {
            //"SELECT ID, pc_Code FROM tblParentCompany ORDER BY pc_IsDefault DESC, pc_Code"
            var source = Bll.DataManager.ParentCompany.GetAllParentCompanies().OrderByDescending(x => x.IsDefault).ThenBy(x => x.Code).ToList();
            DataSource = source;
            DataValueField = "ID";
            DataTextField = "Code";

            base.DataBind(raiseOnDataBinding);
        }
    }
}
