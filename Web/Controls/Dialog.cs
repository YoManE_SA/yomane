﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Web.Controls
{
    [System.Web.UI.ParseChildren(true)]
    public class Dialog : System.Web.UI.WebControls.CompositeControl
    {
        public event System.Web.UI.WebControls.CommandEventHandler Command;

        public string Title { get; set; }
        public string Icon { get; set; }

        [System.Web.UI.ParseChildren(false)]
        public class PanelContainer : System.Web.UI.WebControls.WebControl { public PanelContainer() : base(System.Web.UI.HtmlTextWriterTag.Div) { } }

        [System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public PanelContainer Header { get { return FindControl("Header") as PanelContainer; } }

        [System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public PanelContainer Body { get { return FindControl("Body") as PanelContainer; } }

        [System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public PanelContainer Footer { get { return FindControl("Footer") as PanelContainer; } }


        protected override System.Web.UI.HtmlTextWriterTag TagKey { get { return System.Web.UI.HtmlTextWriterTag.Div; } }
        /*
		protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
		{
			if (Header.Controls.Count > 1 || !string.IsNullOrEmpty(Title))
				Header.RenderControl(writer);
			Body.RenderControl(writer);
			Footer.RenderControl(writer);
		}
		*/
        protected System.Web.UI.LiteralControl TitleHtml { get { return (Header.FindControl("TitleHtml") as System.Web.UI.LiteralControl); } }

        protected override void OnPreRender(EventArgs e)
        {
            Header.Visible = !string.IsNullOrEmpty(Title);
            base.OnPreRender(e);
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            var e = args as System.Web.UI.WebControls.CommandEventArgs;
            if (e != null && Command != null)
                Command(this, e);

            return base.OnBubbleEvent(source, args);
        }

    }
}