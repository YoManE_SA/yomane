﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("Netpay.Web.Controls.LegendView.LegendView.css", "text/css")]

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:LegendView runat=server></{0}:LegendView>")]
	public class LegendView : WebControl
	{
		private struct LegentItem
		{
			public string Text, Class, BoxText;
			public int Value;
			public LegentItem(int nValue, string sText, string sBoxText, string sClass)
			{
				Value = nValue; Text = sText; BoxText = sBoxText;
				Class = sClass ?? sText;
			}
		}

		private Dictionary<int, LegentItem> _Items;
		private string _ViewEnumName;

		public string ViewEnumName { get { return _ViewEnumName; } set { _ViewEnumName = value; if(Page != null) ViewEnum = ViewEnum; } }

		public System.Type ViewEnum
		{
			get { return System.Web.Compilation.BuildManager.GetType(_ViewEnumName, false); }
			set
			{
				_ViewEnumName = value != null ? value.FullName : null;
				_Items = null;
				if(value == null) return;
				_Items = new Dictionary<int, LegentItem>();
				ResourceManager rm = WebUtils.GetResource("LegendView");
				string[] itemText = Enum.GetNames(value);
				Array itemValues = Enum.GetValues(value);
				for(int i = 0; i < itemText.Length; i++)
				{
					string sFullName = value.Name + "." + itemText[i];
					_Items.Add((int)itemValues.GetValue(i), new LegentItem((int)itemValues.GetValue(i), rm.GetString(sFullName) ?? itemText[i], "&nbsp;", sFullName.Replace(".", "_")));
				}
			}
		}

		public void Remove(int value)
		{
			if(_Items == null)
				return;

			_Items.Remove(value);
		}

		public void Add(int nValue, string sText, string sBoxText, string sClass)
		{
			if(_Items == null) _Items = new Dictionary<int, LegentItem>();
			_Items.Add(nValue, new LegentItem(nValue, sText, sBoxText, sClass));
		}

		public string GetItemClass(int value)
		{
			if(_Items == null || !_Items.ContainsKey(value))
				return null;

			return _Items[value].Class;
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			if(ViewEnumName != null && _Items == null) ViewEnumName = ViewEnumName; // first initialize
		}

		protected override void OnPreRender(EventArgs e)
		{
			string includeTemplate = "<link rel='stylesheet' text='text/css' href='{0}' />";
			string includeLocation = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.LegendView.LegendView.css");
			LiteralControl include = new LiteralControl(String.Format(includeTemplate, includeLocation));
			Page.Header.Controls.Add(include);
			base.OnPreRender(e);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			writer.Write("<table border='0' cellspacing='1' cellpadding='0' style='width:100%; direction:inherit;' class='LegendView'>");
			if(_Items != null)
			{
				foreach(var li in _Items)
				{
					writer.Write(string.Format("<tr class='{0}'>", li.Value.Class));
					writer.Write(string.Format("<td style='padding: 0px; width: 15px; height: 18px;'>{0}</td>", li.Value.BoxText));
                    writer.Write(string.Format("<th nowrap='nowrap' style='font-size: 0.8em;' >{0}</th>", li.Value.Text));
					writer.Write("</tr>");
					writer.Write("<tr><td colspan='2' style='height:2px;'></td></tr>");
				}
			}
			else writer.Write("<tr><td colspan='2'>Enum Not Loaded</td></tr>");
			writer.Write("</table>");
		}
	}
}
