﻿if (netpay == null) { var netpay = new Object() }
if (netpay.webControls == null) { netpay.webControls = new Object() }

netpay.webControls.DenormalizedTransactionRowView =
{
	toggleDetails: function(detailsContainerID, transactionStatus, transactionID, isChargeback)
	{
		var container = jQuery("#" + detailsContainerID);
		if (container.css("display") == "none")
		{
			//jQuery.ajax({ async: true, cache: false, context: container, dataType: "json", url: "AjaxHandler.axd?ajaxMethodName=getDenormalizedTransaction&credentialsToken=" + clientContext.credentialsToken + "&transactionStatus=" + transactionStatus + "&transactionID=" + transactionID + "&isChargeback=" + isChargeback, success: netpay.webControls.DenormalizedTransactionRowView.successHandler, error: netpay.webControls.DenormalizedTransactionRowView.errorHandler })

			container.css("display", "")
		}
		else
		{
			container.css("display", "none")
		}
	},

	successHandler: function(result)
	{
		var properties = new Object();
		properties["TerminalName"] = "Terminal Name";
		properties["TerminalNumber"] = "Terminal Number";
		properties["sep1"] = "seperator";
		properties["CompanyName"] = "Merchant Name";
		properties["CompanyNumber"] = "Merchant Number";
		properties["sep2"] = "seperator";
		properties["AcquiringBankName"] = "Bank Name";

		var content = "<br/>";
		for (var property in properties)
		{
			if (result.hasOwnProperty(property) && result[property] != null)
				content += "<b>" + properties[property] + "</b>: " + result[property] + "<br/>";
			else
				content += "<br/>";
		}
		content += "<br/>";

		this.html(content);
	},

	errorHandler: function(request, textStatus, errorThrown)
	{
		this.html("An error has occured during this ajax request.");
	}
}