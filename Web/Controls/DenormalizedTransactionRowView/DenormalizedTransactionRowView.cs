﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Bll.OldVO;
using Netpay.CommonTypes;
using Netpay.Bll.Reports.VO;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:DenormalizedTransactionRowView runat=server></{0}:DenormalizedTransactionRowView>")]
	public sealed class DenormalizedTransactionRowView : WebControl
	{
		private DenormalizedTransactionVO _transaction = new DenormalizedTransactionVO();
		private bool _showStatus = false;
		private bool _showRejectionCode = false;
		private bool _showExpandButton = true;
		private bool _showLegend = true;

		public bool ShowLegend
		{
			get { return _showLegend; }
			set { _showLegend = value; }
		}

		public bool ShowExpandButton
		{
			get { return _showExpandButton; }
			set { _showExpandButton = value; }
		}

		public bool ShowRejectionCode
		{
			get { return _showRejectionCode; }
			set { _showRejectionCode = value; }
		}

		public bool ShowStatus
		{
			get { return _showStatus; }
			set { _showStatus = value; }
		}

		public DenormalizedTransactionVO Transaction
		{
			get
			{
				return _transaction;
			}
			set
			{
				_transaction = value;		
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			// embed scripts
			this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.DenormalizedTransactionRowView.DenormalizedTransactionRowView.js");
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (_transaction == null)
				return;			
				
			String rowBackgroundColor = "";
			String legendCellStyle = "";
			String cellTextColor = "";
			String currencySymbol = "";

			currencySymbol = Bll.Currency.Get(_transaction.TransactionCurrencyID.GetValueOrDefault()).Symbol;

			if (_transaction.PaymentMethodID >= 15 && _transaction.PaymentMethodID <= 29)
			{
				if (_transaction.TransactionStatus == TransactionStatus.Declined.ToString())
				{
					if (_transaction.TransTypeID == (int)FailedTransactionType.PreAuthorized || _transaction.TransTypeID == (int)FailedTransactionType.PreAuthorizedCapture)
					{						
						legendCellStyle = "border:1px solid #ff6666; background-color:#ffffff";
						rowBackgroundColor = "#F6FEF6";
					}
					else
					{
						legendCellStyle = "background-color:#ff6666";
						rowBackgroundColor = "#F6FEF6";
					}
				}
				else if (_transaction.IsRefund != null && _transaction.IsRefund.Value)
				{
					rowBackgroundColor = "#F6FEF6";
					legendCellStyle = "background-color:#66cc66";
					cellTextColor = "#cc0000";					
				}				
				else
				{
					rowBackgroundColor = "#F6FEF6";
					legendCellStyle = "background-color:#66cc66";						
				}
			}
			else if (_transaction.PaymentMethodID == (byte)PaymentMethodEnum.ManualFees)
			{
				rowBackgroundColor = "#f5f5f5";
				legendCellStyle = "background-color:#484848";
				cellTextColor = "#cc0000";
			}
			else if (_transaction.PaymentMethodID == (byte)PaymentMethodEnum.Admin)
			{
				//firstCellTextColor = "#003399";
				rowBackgroundColor = "#eeeffd";
				legendCellStyle = "background-color:#003399";
			}
			else if (_transaction.PaymentMethodID == (byte)PaymentMethodEnum.SystemFees)
			{
				rowBackgroundColor = "#f5f5f5";
				legendCellStyle = "background-color:#003399";
				cellTextColor = "#cc0000";
			}
			else if (_transaction.PaymentMethodID == (byte)PaymentMethodEnum.Unknown)
			{
				if (_transaction.RejectionCode != "000" && _transaction.RejectionCode != "001" && ("" + _transaction.RejectionCode).Trim() != "")
				{
					if (_transaction.TransTypeID == 1)
					{
						legendCellStyle = "border:1px solid #ff6666;";
						rowBackgroundColor = "#F6FEF6";
					}
					else
					{
						legendCellStyle = "background-color:#ff6666";
						rowBackgroundColor = "#F6FEF6";
					}
				}
			}
			
			// output
			writer.Write(string.Format("<tr onmouseover=\"this.style.backgroundColor='#d8d8d8';\" onmouseout=\"this.style.backgroundColor='';\" bgcolor=\"{0}\">", rowBackgroundColor));

			if (ShowLegend)
				writer.Write(string.Format("<td width='8px' style='{0}'>&nbsp;</td>", legendCellStyle));

			if (ShowExpandButton)
				writer.Write(string.Format("<td><img onclick=\"netpay.webControls.DenormalizedTransactionRowView.toggleDetails('detailsContainer_{0}_{1}', '{0}', '{1}', {2})\" style='cursor:pointer;' src='/NPCommon/Images/tree_expand.gif' alt='' width='16' height='16' border='0' align='middle' /></td>", _transaction.TransactionStatus, _transaction.TransactionID, _transaction.IsChargeback.ToString().ToLower()));
			
			writer.Write(string.Format("<td class='txt11' style='color:{0};' nowrap='nowrap'>{1}</td>", cellTextColor, _transaction.TransactionID));
			if (_showStatus)
				writer.Write(string.Format("<td class='txt11' style='color:{0};' nowrap='nowrap'>{1}</td>", cellTextColor, _transaction.TransactionStatus.ToSentence()));
			writer.Write(string.Format("<td class='txt11' style='color:{0};' nowrap='nowrap'>{1}</td>", cellTextColor, _transaction.TransactionDate));
			
			// payment method
			PaymentMethodView paymentMethodView = new PaymentMethodView();
			paymentMethodView.PaymentMethodID = Transaction.PaymentMethodID;			
			writer.Write(string.Format("<td class='txt11' style='color:{0};' nowrap='nowrap'>", cellTextColor));
			paymentMethodView.RenderControl(writer);
			writer.Write("</td>");

			writer.Write(string.Format("<td class='txt11' style='color:{0}; text-align:" + HttpContext.Current.Session["CssAlign"] + ";' nowrap='nowrap'>{1} {2}</td>", cellTextColor, currencySymbol, _transaction.TransactionAmount.Value.ToString("0.00")));

			if (_showRejectionCode)
				writer.Write(string.Format("<td class='txt11' style='color:{0}; text-align:" + HttpContext.Current.Session["CssAlign"] + ";' nowrap='nowrap'>{1}</td>", cellTextColor, _transaction.RejectionCode));

			writer.Write(string.Format("<td class='txt11' style='color:{0}; text-align:" + HttpContext.Current.Session["CssAlign"] + ";' nowrap='nowrap'>{1}</td>", cellTextColor, _transaction.BankTransferID));

			writer.Write("</tr>");
			writer.Write(string.Format("<tr><td id='detailsContainer_{0}_{1}' style='display:none;' colspan='8'>Loading ...</td></tr>", _transaction.TransactionStatus, _transaction.TransactionID));
			writer.Write("<tr><td height='1' colspan='10' bgcolor='silver'></td></tr>");
		}
	}
}
