﻿using System;
using System.Resources;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Bll.Totals;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	public class Totals : WebControl
	{
        public CPayment Payment = null;
        public int PayID { get; set; }
		public int CompanyID { get; set; }
		public int Currency { get; set; }
		public int AffiliateID { get; set; }
		public int AFP_ID { get; set; }
		public bool IsBankPaid { get; set; }
		public bool SettlemntPreview { get; set; }
		public bool HideTitle { get; set; }
		public string Where = null;
		private int lineCount;

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			string template = "<link rel='stylesheet' text='text/css' href='{0}' />";
			string location = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.Totals.Totals.css");
			LiteralControl literal = new LiteralControl(String.Format(template, location));
			Page.Header.Controls.Add(literal);
		}

		private string drawMerchantLine(string xTitle, string xQty, decimal wDebit, decimal xDeposit, decimal xFuture, string xCssClass, bool xDrawLine)
		{
			if ((xQty == "" || xQty == "0") && (xDeposit == 0m) && (wDebit == 0m)) return "";
			lineCount++;
			System.Text.StringBuilder lineText = new StringBuilder();
            //if (xDrawLine && lineCount > 1) lineText.Append("<tr><td style='height: 16px' colspan='4'><img src='/NPCommon/Images/1_space.gif' width='1' height='1' border='0'><br /></td></tr>");
			lineText.Append("<tr bgcolor=\"white\">");
			lineText.AppendFormat("<td class=\"{0}\" valign=\"top\">{1}<br /></td>", xCssClass, xTitle);
			lineText.AppendFormat("<td class=\"{0}\" valign=\"top\">{1}<br /></td>", xCssClass, xQty);
			if (wDebit != 0) lineText.AppendFormat("<td valign=\"top\" class=\"{0}\" style=\"color:red;\">{1}<br /></td>", xCssClass, wDebit.ToAmountFormat(Currency));
			else lineText.Append("<td></td>");
			if (xDeposit != 0) lineText.AppendFormat("<td class=\"{0}\">{1}&nbsp;", xCssClass, xDeposit.ToAmountFormat(Currency));
			else lineText.Append("<td>");
			if (PayID == 0 && xFuture != 0 && !SettlemntPreview) lineText.AppendFormat("({0})", (xDeposit + xFuture).ToAmountFormat(Currency));
			lineText.Append("</td></tr>");
			return lineText.ToString();
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			DateTime settlmentDate = DateTime.Now.Date;
            //Page.Request.QueryString
            if(Payment == null) { 
                if (SettlemntPreview) {
                    var createSettings = new CPayment.CreateSettings();
				    settlmentDate = Bll.Merchants.Merchant.GetMerchantNextSettlementDate(CompanyID);
                    createSettings.TransPD = new Infrastructure.Range<DateTime?>(null, settlmentDate);
                    //if (IsBankPaid) Where = "ep.TransID Is Not Null" + (string.IsNullOrEmpty(Where) ? "" : " And " + Where);
                    createSettings.ForceEpaPaid = IsBankPaid;
                    Payment = CPayment.Create(CompanyID, Currency);
                    Payment.UpdatePayment(settlmentDate, new CPayment.CreateSettings() { }, false);
                } else {
                    Payment = CPayment.Load(PayID, CompanyID);
                }
            }
            if(Payment == null)
            {
                writer.Write("No Data Exist");
                return;
            }

            ResourceManager rm = WebUtils.GetResource("Totals");
            ResourceManager gm = WebUtils.GetResource("Common");
			CAmount gTotal = Payment.Totals;
			CAmount gPMTotal = Payment.PaymentMethodTotal(PaymentMethodEnum.CC_MIN, PaymentMethodEnum.EC_MAX);
			Currency = Payment.Currency;

			var currencyObj = Bll.Currency.Get(Currency);
			SearchFiltersView filtersView = new SearchFiltersView();
            
			filtersView.Add(gm.GetString("Currency"), currencyObj.IsoCode);
			if (HttpContext.Current.Request["InsDateFrom"] != null)
                filtersView.Add(gm.GetString("Fromdt"), HttpContext.Current.Request["InsDateFrom"]);
			if (HttpContext.Current.Request["InsDateTo"] != null)
                filtersView.Add(gm.GetString("Todate"), HttpContext.Current.Request["InsDateTo"]);

			if (!HideTitle)
			{
				//writer.Write("<h3 style=\"margin-bottom:5px;margin-top:5px;\">" + (AmountSum != null ? AmountSum.CompanyName : "") + "</h3><br/>");
				filtersView.RenderControl(writer);
			}

            writer.Write("<table width=\"100%\" class=\"exspand-table\" >");
			if (AffiliateID == 0)
			{
				writer.Write("<tr>");
				writer.Write("<th width=\"50%\">" + rm.GetString("thDescription") + "<br/></th>");
				writer.Write("<th width=\"12%\">" + rm.GetString("thQty") + "<br/></th>");
				writer.Write("<th width=\"12%\">" + rm.GetString("thFee") + "<br/></th>");
				writer.Write("<th width=\"26%\">" + rm.GetString("thDeposit") + "<br/></th>");
				

				writer.Write(drawMerchantLine(rm.GetString("dscAuthorizedTransactions"), gPMTotal.NormCount.ToString(), gPMTotal.NormLineFee, gPMTotal.NormAmount, gPMTotal.NormFutInst, "txt11", false));
				writer.Write(drawMerchantLine(rm.GetString("dscProcessingFee"), gPMTotal.NormCount.ToString(), gPMTotal.NormFees - gPMTotal.NormLineFee, 0, 0, "txt11", false));
				writer.Write(drawMerchantLine(rm.GetString("dscRefundTransactions"), gPMTotal.RefCount.ToString(), gPMTotal.RefFees, gPMTotal.RefAmount, 0, "txt11", true));
                writer.Write(drawMerchantLine(rm.GetString("dscCashback"), gPMTotal.CashbackCount.ToString(), gPMTotal.CashbackFees, gPMTotal.CashbackAmount, 0, "txt11", true));
                writer.Write(drawMerchantLine(rm.GetString("dscCustomerChargebacks"), gPMTotal.DenCount.ToString(), gPMTotal.DenFees, -gPMTotal.DenAmount, 0, "txt11", true));
				writer.Write(drawMerchantLine(rm.GetString("dscClarificationFee"), gPMTotal.ClrfCount.ToString(), gPMTotal.ClrfFees, 0, 0, "txt11", false));
				writer.Write(drawMerchantLine(rm.GetString("dscBankFees"), Payment.BankFees.TotalCount.ToString(), Payment.BankFees.TotalFees, Payment.BankFees.TotalAmount, 0, "txt11", true));
				if (Payment.NotPayPercent > 0) writer.Write(drawMerchantLine(rm.GetString("dscDirectDeposit"), "(-" + Payment.NotPayPercent.ToString("0") + "%)", 0, -Payment.TotalNotPayedAmount, 0, "txt11", false));
				writer.Write(drawMerchantLine(rm.GetString("dscAdminTransactions"), Payment.Admin.TotalCount.ToString(), Payment.Admin.TotalFees, Payment.Admin.TotalAmount, 0, "txt11", true));
				writer.Write(drawMerchantLine(rm.GetString("dscSystemTransactions"), Payment.FeesAutoAndManual.TotalCount.ToString(), Payment.FeesAutoAndManual.TotalFees, Payment.FeesAutoAndManual.TotalAmount, 0, "txt11", false));
				writer.Write(drawMerchantLine(rm.GetString("dscHandlingFee"), gTotal.HandlingCount.ToString(), gTotal.HandlingFee, 0, 0, "txt11", false));
				writer.Write(drawMerchantLine(rm.GetString("dscFinance"), gPMTotal.FinanceCount.ToString(), gPMTotal.FinanceFee, 0, 0, "txt11", true));
				writer.Write(drawMerchantLine(rm.GetString("dscRollingReserve"), (Payment.RollingReserve != 0 ? 1 : 0).ToString(), 0, Payment.RollingReserve, 0, "txt11", true));
				writer.Write(drawMerchantLine(rm.GetString("dscReserveRelease"), Payment.RReturnCount.ToString(), 0, Payment.ReserveReturn, 0, "txt11", false));

                writer.Write("<tr><td style='height: 36px; border: 0px;' colspan='4'><div style='border-bottom: 1px dashed #e8e8e8;'></div></td></tr>");
                
				writer.Write(drawMerchantLine(rm.GetString("dscTotalAmountFees"), "", Payment.TotalFeesAmount, Payment.TotalAmount, gPMTotal.NormFutInst, "txt11", false));
				if (Payment.VAT > 0) writer.Write(drawMerchantLine(rm.GetString("dscTotalFeesVAT") + "(" + (Payment.VAT * 100) + "%)", "", Payment.VATAmount, 0, 0, "txt11", false));
				writer.Write(drawMerchantLine(rm.GetString("dscTotalPaymentMerchant"), "", 0, Payment.Total, gPMTotal.NormFutInst, "grandTotal", false));
				//if (PayID <= 0) writer.Write(drawMerchantLine(rm.GetString("dscTotalPaymentMerchantNoHold"), "", 0, gPMTotal.BankPaidAmount, 0, "grandTotal", false));
			}
			else
			{
				var AffSum = AffiliateFees.CalcAffiliateFees(AFP_ID, Payment, AffiliateID);
				writer.Write("<tr>");
				writer.Write("<th width=\"14%\">" + rm.GetString("thPaymentMethod") + "<br/></th>");
				writer.Write("<th width=\"6%\">" + rm.GetString("thCount") + "<br/></th>");
				writer.Write("<th width=\"10%\">" + rm.GetString("thAmount") + "<br/></th>");
				writer.Write("<th width=\"10%\">" + rm.GetString("thRefunds") + "<br/></th>");
				writer.Write("<th width=\"10%\">" + rm.GetString("thCHB") + "<br/></th>");
				writer.Write("<th width=\"10%\">" + rm.GetString("thSubTotal") + "<br/></th>");
				writer.Write("<th width=\"10%\">" + rm.GetString("thCredit") + "<br/></th>");
				writer.Write("<th width=\"30%\">" + rm.GetString("thFee") + "<br/></th>");
				

				string Notes = ""; int NoteIndex = 1;
				var pms = Bll.PaymentMethods.PaymentMethod.Search(null, null);
				foreach (var pm in pms)
				{
					if (AffSum.HasPaymentMethodAmount((PaymentMethodEnum)pm.ID))
					{
						var AffTotal = AffSum.GetPaymentMethodAmount((PaymentMethodEnum)pm.ID);
						var lineTotal = Payment.PaymentMethodTotal((PaymentMethodEnum)pm.ID, (PaymentMethodEnum)pm.ID);
						var xSubTotal = lineTotal.NormAmount + lineTotal.RefAmount - lineTotal.DenAmount;
						writer.Write("<tr>");
						writer.Write("<td valign=\"top\">" + pm.Name + "</td>");
						writer.Write("<td valign=\"top\">" + lineTotal.TotalCount + "</td>");
						writer.Write("<td valign=\"top\">" + (lineTotal.NormAmount + lineTotal.RefAmount).ToAmountFormat(Currency) + "</td>");
						writer.Write("<td valign=\"top\">" + lineTotal.RefAmount.ToAmountFormat(Currency) + "</td>");
						writer.Write("<td valign=\"top\">" + lineTotal.DenAmount.ToAmountFormat(Currency) + "</td>");
						writer.Write("<td valign=\"top\">" + xSubTotal.ToAmountFormat(Currency) + "</td>");
						writer.Write("<td valign=\"top\">" + AffTotal.CreditAmount.ToAmountFormat(Currency) + "</td>");
						writer.Write("<td valign=\"top\">");
						if (AffTotal.Items.Count > 1)
						{
							for (int i = 0; i < AffTotal.Items.Count; i++)
							{
								if (!string.IsNullOrEmpty(AffTotal.Items[i].CreditInfo))
								{
									if (NoteIndex == 1) Notes = "<br/>Notes:<br/>";
									Notes += NoteIndex + " - " + AffTotal.Items[i].CreditInfo + "<br/>";
									NoteIndex = NoteIndex + 1;
								}
								writer.Write(AffTotal.Items[i].Amount.ToAmountFormat(Currency) + " X " + AffTotal.Items[i].FeePercent.ToString("0.00") + "% = " + AffTotal.Items[i].Total.ToAmountFormat(Currency) + (!string.IsNullOrEmpty(AffTotal.Items[i].CreditInfo) ? "<sup>(" + (NoteIndex - 1) + ")</sup>" : "") + "<br/>");
							}
							writer.Write("<div style=\"border-top:solid 1px #C0C0C0;margin-top:2px;\">" + rm.GetString("dscTotalFee") + ": " + AffTotal.Total.ToAmountFormat(Currency) + "</div>");
						}
						else writer.Write(rm.GetString("dscTotalFee") + ": " + AffTotal.Total.ToAmountFormat(Currency));
						writer.Write("</td>");
						writer.Write("</tr>");
                        writer.Write("<tr><td style='height: 16px' colspan=\"8\" ><img src=\"../images/img/1_space.gif\" width=\"1\" height=\"1\" border=\"0\"><br></td></tr>");
					}
				}
				writer.Write("<tr><td height=\"1\" bgcolor=\"gray\" colspan=\"8\"></td></tr>");
				writer.Write("<tr><td class=\"txt11B\" colspan=\"7\">Sub Total :</td><td class=\"txt11B\">" + AffSum.SubTotal.ToAmountFormat(Currency) + "</td></tr>");
				writer.Write("<tr><td height=\"1\" bgcolor=\"gray\" colspan=\"8\"></td></tr>");
				writer.Write("<tr><td class=\"txt11B\" colspan=\"8\"><br />Other services :</td></tr>");
				writer.Write("<tr><td height=\"1\" bgcolor=\"gray\" colspan=\"8\"></td></tr>");

				for (int i = 0; i < AffSum.PaymentLines.Length; i++)
				{
					writer.Write("<tr>");
					writer.Write("<td colspan=\"5\">" + AffSum.PaymentLines[i].Text + "</td>");
					writer.Write("<td colspan=\"2\">" + AffSum.PaymentLines[i].Quantity + " X " + AffSum.PaymentLines[i].Amount.ToAmountFormat(Currency) + "</td>");
					writer.Write("<td>" + AffSum.PaymentLines[i].Total.ToAmountFormat(Currency) + "</td>");
					writer.Write("</tr>");
				}

                writer.Write("<tr><td height=\"1\" bgcolor=\"gray\" colspan=\"8\"></td></tr>");
				writer.Write("<tr>");
				writer.Write("<td valign=\"top\" colspan=\"7\">" + rm.GetString("dscTotalPaymentAffiliate") + "</td>");
				writer.Write("<td class=\"txt11b\">" + AffSum.Total.ToAmountFormat(Currency) + "</td></tr>");
				writer.Write("<tr><td colspan=\"8\" class=\"txt10\">" + Notes + "</td></tr>");
			}
			writer.Write("</table>");
			base.RenderContents(writer);
		}
	}
}
