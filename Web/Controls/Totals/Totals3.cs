﻿using System;
using System.Resources;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Infrastructure.VO;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	public class Totals3 : WebControl
	{
		CreatePayments.CreatePaymentInfo _paymentInfo = new CreatePayments.CreatePaymentInfo();
		public int AffiliateID { get; set; }
		public int AFP_ID { get; set; }
		public int PayID { get { return _paymentInfo.payID; } set { _paymentInfo.payID = value; } }
		public int CompanyID { get { return _paymentInfo.merchantID; } set { _paymentInfo.merchantID = value; } }
		public Currency Currency { get { return _paymentInfo.currencyID; } set { _paymentInfo.currencyID = value; } }
		public bool IsBankPaid { get { return _paymentInfo.epaPaid.GetValueOrDefault(); } set { _paymentInfo.epaPaid = value; } }
		public bool IsIncludeTest { get { return _paymentInfo.includeTest; } set { _paymentInfo.includeTest = value; } }
		public DateTime? insertDateFrom { get { return _paymentInfo.insertDateFrom; } set { _paymentInfo.insertDateFrom = value; } }
		public DateTime? insertDateTo { get { return _paymentInfo.insertDateTo; } set { _paymentInfo.insertDateTo = value; } }
		public bool SettlemntPreview { get; set; }
		public bool HideTitle { get; set; }

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			string template = "<link rel=\"stylesheet\" text=\"text/css\" href=\"{0}\" />";
			string location = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.Totals.Totals.css");
			LiteralControl literal = new LiteralControl(String.Format(template, location));
			if (Page.Header != null) Page.Header.Controls.Add(literal);
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			_paymentInfo.apply = false;
			_paymentInfo.rollingReserve = _paymentInfo.rollingRelease = _paymentInfo.includeStorage = (_paymentInfo.payID == 0);
			if (SettlemntPreview) {
				_paymentInfo.epaPaid = false;
				_paymentInfo.payDate = Merchants.GetMerchantNextSettlementDate(WebUtils.LoggedUser.CredentialsToken);
				_paymentInfo.merchantPayDateTo = _paymentInfo.payDate;
			} else _paymentInfo.payDate = DateTime.Now.Date;

			Settlement2VO settlment = CreatePayments.CreatePayment(WebUtils.CredentialsToken, _paymentInfo);
			ResourceManager rm = WebUtils.GetResource("Totals2");
			Currency = settlment.currencyID;
			
			CurrencyVO currencyObj = WebUtils.CurrentDomain.Cache.Currencies[(int)Currency];
			SearchFiltersView filtersView = new SearchFiltersView();
			filtersView.Add("Currency", currencyObj.IsoCode);
			if (HttpContext.Current.Request["InsDateFrom"] != null)
				filtersView.Add("From Date", HttpContext.Current.Request["InsDateFrom"]);
			if (HttpContext.Current.Request["InsDateTo"] != null)
				filtersView.Add("To Date", HttpContext.Current.Request["InsDateTo"]);
			
			if (!HideTitle) {
				writer.Write("<h3 class=\"title\">" + (PayID != 0 ? rm.GetString("SettlementNumber") + " " + PayID : rm.GetString("Unsettled")) + " (" + WebUtils.DomainCache.GetCurrency((int)Currency).IsoCode + ") - <span class=\"subTitle\">" + settlment.merchantId + "</span></h3>");
				//writer.Write("<p>"); filtersView.RenderControl(writer); writer.Write("</p>");
			}

			writer.Write("<div id=\"TotalsTabs\">");
			writer.Write(" <ul>");
			writer.Write("  <li><a href=\"#tabs-1\">Totals</a></li>");
			writer.Write("  <li><a href=\"#tabs-2\">Fees</a></li>");
			writer.Write("  <li><a href=\"#tabs-3\">Other Charges details</a></li>");
			writer.Write(" </ul>");
			writer.Write(" <div id=\"tabs-2\">");
			System.Collections.Generic.List<CreditFeesVO> feesRes = Netpay.Bll.Payments.GetSettlmentFees(WebUtils.CredentialsToken, CompanyID, PayID);
			if (feesRes != null && feesRes.Count > 0) {
				writer.Write("<table width=\"95%\" class=\"totals\">");
				writer.Write("<th>Credit Card</th>");
				writer.Write("<th>Country</th>");
				writer.Write("<th>Transaction</th>");
				writer.Write("<th>Fail Fee</th>");
				writer.Write("<th>Pre-Auth</th>");
				writer.Write("<th>Refund</th>");
				writer.Write("<th>Clarification</th>");
				writer.Write("<th>Charge<br />back</th>");
				foreach (CreditFeesVO f in feesRes) {
					writer.Write("<tr>");
					writer.Write(" <td>" + WebUtils.DomainCache.GetPaymentMethod(f.PaymentMethod).Name + "</td>");
					writer.Write(" <td>" + f.ListBINs + "</td>");
					writer.Write(" <td>" + f.FixedFee.ToAmountFormat(WebUtils.DomainHost, Currency) + " / " + (settlment.isGradedFees ? rm.GetString("GradedFee") : f.PercentFee.ToString("0.00") + "%") + "</td>");
					writer.Write(" <td>" + f.FailFixedFee.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write(" <td>" + f.ApproveFixedFee.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write(" <td>" + f.RefundFixedFee.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write(" <td>" + f.ClarificationFee.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write(" <td>" + f.CBFixedFee.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write("</tr>");
				}
				writer.Write("</table>");
				feesRes.Clear();
			}
			if (settlment.isGradedFees)
			{
				System.Collections.Generic.List<MerchantFeesFloorVO> gradedFeesRes = Netpay.Bll.Payments.GetFloorFees(WebUtils.CredentialsToken, CompanyID, PayID);
				if (gradedFeesRes != null && gradedFeesRes.Count > 0)
				{
					writer.Write("<p>");
					writer.Write("<b>" + rm.GetString("GradedFeesTable") + ":</b></br>");
					for (int i = 0; i < gradedFeesRes.Count; i++)
					{
						if (i > 0) writer.Write(string.Format(rm.GetString("GradedFromTo"), gradedFeesRes[i - 1].TotalTo.ToAmountFormat(WebUtils.DomainHost, Currency), gradedFeesRes[i].TotalTo.ToAmountFormat(WebUtils.DomainHost, Currency), gradedFeesRes[i - 1].Precent.ToString("0.00") + "%") + "</br>");
						if (i == (gradedFeesRes.Count - 1)) writer.Write(string.Format(rm.GetString("GradedFrom"), gradedFeesRes[i].TotalTo.ToAmountFormat(WebUtils.DomainHost, Currency), gradedFeesRes[i].Precent.ToString("0.00") + "%") + "</br>");
					}
					gradedFeesRes.Clear();
					writer.Write("</p>");
				}
			}
			writer.Write(" </div>");

			writer.Write(" <div id=\"tabs-3\">");
				//var SettlmentAmounts = Netpay.Bll.TransactionAmounts.GetPaymentSettlmentAmounts(WebUtils.CredentialsToken, PayID);
				foreach (var amt in settlment.SettlmentAmounts)
					writer.Write("<input type=\"checkbox\" name=\"transAmountID\" value=\"" + amt.ID + "\" />" + amt.Description + ": " + amt.SettledAmount.Value.ToHtml(WebUtils.DomainHost, Currency) + "<br />");
			writer.Write(" </div>");

			writer.Write(" <div id=\"tabs-1\">");
			CountAmount grandTotal = new CountAmount(), groupTotal = new CountAmount();
			writer.Write("<table class=\"totals\" width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">");
			writer.Write("<tr><th>" + rm.GetString("thItem") + "<br/></th><th>" + rm.GetString("thQty") + "</th><th>" + rm.GetString("thAmount") + "</th></tr>");
			if (AffiliateID == 0) {
				TransactionAmountGroup prevType = TransactionAmountGroup.Unknown;
                TransactionAmountGroup[] TotalsGroups = { TransactionAmountGroup.Transactions, TransactionAmountGroup.Fees, TransactionAmountGroup.DirectBank, TransactionAmountGroup.Reserve, TransactionAmountGroup.OtherCharges, TransactionAmountGroup.Taxes };
				foreach (var tg in TotalsGroups)
				{
					var eventGroups = WebUtils.CurrentDomain.Cache.GetTransactionAmountGroup(tg);
					foreach (var v in eventGroups)
					{
						if (prevType != tg)
						{
							if (prevType != TransactionAmountGroup.Unknown)
							{
								writer.Write("<tr class=\"subTotal\" />");
								writer.Write("<td>" + rm.GetString("Total") + "</td>");
								writer.Write("<td>" + groupTotal.Count + "</td>");
								writer.Write("<td>" + (-groupTotal.Amount).ToHtml(WebUtils.DomainHost, Currency) + "</td>");
								writer.Write("</tr>");
							}
							grandTotal.Add(groupTotal);
							groupTotal.Reset();
							prevType = tg;
							writer.Write("<tr><td colspan=\"3\"><br/></td></tr>");
							writer.Write("<tr class=\"groupName\"><td colspan=\"2\">" + WebUtils.GetResource("TransactionAmountGroup").GetString(tg.ToString()) + "<br/></td><td>&nbsp;</td></tr>");
						}
						CountAmount ca;
						if (settlment.Totals.TryGetValue(v, out ca))
						{
							if (!ca.IsZero) {
								groupTotal.Add(ca);
								writer.Write("<tr>" + 
									"<td>" + WebUtils.GetResource("TransactionAmountType").GetString(v.ToString()) + "</td>" +
									"<td>" + ca.Count.ToString() + "</td><td>" + (-ca.Amount).ToHtml(WebUtils.DomainHost, Currency) + "</td>");
							}
						}
					}
				}
				if (prevType != TransactionAmountGroup.Unknown) {
					writer.Write("<tr class=\"subTotal\" />");
					writer.Write("<td>" + rm.GetString("Total") + "</td>");
					writer.Write("<td>" + groupTotal.Count + "</td>");
					writer.Write("<td>" + (-groupTotal.Amount).ToHtml(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write("</tr>");
					grandTotal.Add(groupTotal);
				}

				writer.Write("<tr><td colspan=\"3\"><br/></td></tr>");
				writer.Write("<tr class=\"subTotal\" />");
				writer.Write("<td>" + rm.GetString("GrandTotal") + "</td>");
				writer.Write("<td>&nbsp;</td>");
				writer.Write("<td>" + (-grandTotal.Amount).ToHtml(WebUtils.DomainHost, Currency) + "</td>");
				writer.Write("</tr>");
			} else {

			}
			writer.Write("</table>");
			writer.Write(" </div>");
			writer.Write("</div>");
			writer.Write("<script type=\"text/javascript\">$(function() { $('#TotalsTabs').tabs(); });<" + "/script>");
			base.RenderContents(writer);
		}
	}
}
