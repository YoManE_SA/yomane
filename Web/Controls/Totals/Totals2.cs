﻿using System;
using System.Resources;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Bll.Totals2;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	public class Totals2 : WebControl
	{

		public int PayID { get; set; }
		public int CompanyID { get; set; }
		public int Currency { get; set; }
		public int AffiliateID { get; set; }
		public int AFP_ID { get; set; }
		public bool IsBankPaid { get; set; }
		public bool SettlemntPreview { get; set; }
		public bool HideTitle { get; set; }
		public string Where = null;

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			string template = "<link rel=\"stylesheet\" text=\"text/css\" href=\"{0}\" />";
			string location = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.Totals.Totals.css");
			LiteralControl literal = new LiteralControl(String.Format(template, location));
			if (Page.Header != null) Page.Header.Controls.Add(literal);
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			DateTime settlmentDate = DateTime.Now.Date;
			if (SettlemntPreview) { 
				IsBankPaid = false;
				settlmentDate = Bll.Merchant.Merchant.GetMerchantNextSettlementDate(CompanyID);
				Where = "tblCompanyTransPass.MerchantPD <= '" + settlmentDate.ToString("yyyyMMdd") + " 00:00:00'" + (string.IsNullOrEmpty(Where) ? "" : " And " + Where);
			}
			if (IsBankPaid) Where = "ep.TransID Is Not Null" + (string.IsNullOrEmpty(Where) ? "" : " And " + Where);
			CPayment AmountSum = CPayment.GetPayment(WebUtils.DomainHost, PayID, CompanyID, Currency, settlmentDate, Where);
			ResourceManager rm = WebUtils.GetResource("Totals2");
			CAmount gTotal = AmountSum.Totals;
			Currency = AmountSum.Currency;
			bool isGradedFees = AmountSum.IsGradedFees;
			
			CurrencyVO currencyObj = WebUtils.CurrentDomain.Cache.Currencies[Currency];
			SearchFiltersView filtersView = new SearchFiltersView();
			filtersView.Add("Currency", currencyObj.IsoCode);
			if (HttpContext.Current.Request["InsDateFrom"] != null)
				filtersView.Add("From Date", HttpContext.Current.Request["InsDateFrom"]);
			if (HttpContext.Current.Request["InsDateTo"] != null)
				filtersView.Add("To Date", HttpContext.Current.Request["InsDateTo"]);
			
			if (!HideTitle) {
				writer.Write("<h3 class=\"title\">" + (PayID != 0 ? rm.GetString("SettlementNumber") + " " + PayID : rm.GetString("Unsettled")) + " (" + WebUtils.DomainCache.GetCurrency(Currency).IsoCode + ") - <span class=\"subTitle\">" + (AmountSum != null ? AmountSum.CompanyName : "") + "</span></h3>");
				//writer.Write("<p>"); filtersView.RenderControl(writer); writer.Write("</p>");
			}

			writer.Write("<div id=\"TotalsTabs\">");
			writer.Write(" <ul>");
			writer.Write("  <li><a href=\"#tabs-1\">Totals</a></li>");
			writer.Write("  <li><a href=\"#tabs-2\">Fees</a></li>");
			writer.Write("  <li><a href=\"#tabs-3\">Other Charges details</a></li>");
			writer.Write(" </ul>");
			writer.Write(" <div id=\"tabs-2\">");
			System.Collections.Generic.List<CreditFeesVO> feesRes = Netpay.Bll.Payments.GetSettlmentFees(WebUtils.CredentialsToken, CompanyID, PayID);
			if (feesRes != null && feesRes.Count > 0) {
				writer.Write("<table width=\"95%\" class=\"totals\">");
				writer.Write("<th>Credit Card</th>");
				writer.Write("<th>Country</th>");
				writer.Write("<th>Transaction</th>");
				writer.Write("<th>Fail Fee</th>");
				writer.Write("<th>Pre-Auth</th>");
				writer.Write("<th>Refund</th>");
				writer.Write("<th>Clarification</th>");
				writer.Write("<th>Charge<br />back</th>");
				foreach (CreditFeesVO f in feesRes) {
					writer.Write("<tr>");
					writer.Write(" <td>" + WebUtils.DomainCache.GetPaymentMethod(f.PaymentMethod).Name + "</td>");
					writer.Write(" <td>" + f.ListBINs + "</td>");
					writer.Write(" <td>" + f.FixedFee.ToAmountFormat(WebUtils.DomainHost, Currency) + " / " + (isGradedFees ? rm.GetString("GradedFee") : f.PercentFee.ToString("0.00") + "%") + "</td>");
					writer.Write(" <td>" + f.FailFixedFee.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write(" <td>" + f.ApproveFixedFee.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write(" <td>" + f.RefundFixedFee.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write(" <td>" + f.ClarificationFee.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write(" <td>" + f.CBFixedFee.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write("</tr>");
				}
				writer.Write("</table>");
				feesRes.Clear();
			}
			if (isGradedFees) {
				System.Collections.Generic.List<MerchantFeesFloorVO> gradedFeesRes = Netpay.Bll.Payments.GetFloorFees(WebUtils.CredentialsToken, CompanyID, PayID);
				if (gradedFeesRes != null && gradedFeesRes.Count > 0)
				{
					writer.Write("<p>");
					writer.Write("<b>" + rm.GetString("GradedFeesTable") + ":</b></br>");
					for (int i = 0; i < gradedFeesRes.Count; i++)
					{
						if (i > 0) writer.Write(string.Format(rm.GetString("GradedFromTo"), gradedFeesRes[i - 1].TotalTo.ToAmountFormat(WebUtils.DomainHost, Currency), gradedFeesRes[i].TotalTo.ToAmountFormat(WebUtils.DomainHost, Currency), gradedFeesRes[i - 1].Precent.ToString("0.00") + "%") + "</br>");
						if (i == (gradedFeesRes.Count - 1)) writer.Write(string.Format(rm.GetString("GradedFrom"), gradedFeesRes[i].TotalTo.ToAmountFormat(WebUtils.DomainHost, Currency), gradedFeesRes[i].Precent.ToString("0.00") + "%") + "</br>");
					}
					gradedFeesRes.Clear();
					writer.Write("</p>");
				}
			}
			writer.Write(" </div>");

			writer.Write(" <div id=\"tabs-3\">");
				var amounts = Netpay.Bll.TransactionAmounts.GetPaymentSettlmentAmounts(WebUtils.CredentialsToken, PayID);
				foreach (var amt in amounts)
					writer.Write("<input type=\"checkbox\" name=\"transAmountID\" value=\"" + amt.ID + "\" />" + amt.Description + ": " + amt.SettledAmount.Value.ToHtml(WebUtils.DomainHost, Currency) + "<br />");
			writer.Write(" </div>");

			writer.Write(" <div id=\"tabs-1\">");
			CountAmount grandTotal = new CountAmount(), groupTotal = new CountAmount();
			writer.Write("<table class=\"totals\" width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">");
			writer.Write("<tr><th>" + rm.GetString("thItem") + "<br/></th><th>" + rm.GetString("thQty") + "</th><th>" + rm.GetString("thAmount") + "</th></tr>");
			if (AffiliateID == 0) {
				TransactionAmountGroup prevType = TransactionAmountGroup.Unknown;
                TransactionAmountGroup[] TotalsGroups = { TransactionAmountGroup.Transactions, TransactionAmountGroup.Fees, TransactionAmountGroup.DirectBank, TransactionAmountGroup.Reserve, TransactionAmountGroup.OtherCharges };
				foreach (var tg in TotalsGroups)
				{
					var eventGroups = WebUtils.CurrentDomain.Cache.GetTransactionAmountGroup(tg);
					foreach (var v in eventGroups)
					{
						if (prevType != tg)
						{
							if (prevType != TransactionAmountGroup.Unknown)
							{
								writer.Write("<tr class=\"subTotal\" />");
								writer.Write("<td>" + rm.GetString("Total") + "</td>");
								writer.Write("<td>" + groupTotal.Count + "</td>");
								writer.Write("<td>" + (-groupTotal.Amount).ToHtml(WebUtils.DomainHost, Currency) + "</td>");
								writer.Write("</tr>");
							}
							grandTotal.Add(groupTotal);
							groupTotal.Reset();
							prevType = tg;
							writer.Write("<tr><td colspan=\"3\"><br/></td></tr>");
							writer.Write("<tr class=\"groupName\"><td colspan=\"2\">" + WebUtils.GetResource("TransactionAmountGroup").GetString(tg.ToString()) + "<br/></td><td>&nbsp;</td></tr>");
						}
						CountAmount ca = gTotal[v];
						if (!ca.IsZero){
							groupTotal.Add(ca);
							writer.Write("<tr>" + 
								"<td>" + WebUtils.GetResource("TransactionAmountType").GetString(v.ToString()) + "</td>" +
								"<td>" + ca.Count.ToString() + "</td><td>" + (-ca.Amount).ToHtml(WebUtils.DomainHost, Currency) + "</td>");
							}
					}
				}
				if (prevType != TransactionAmountGroup.Unknown) {
					writer.Write("<tr class=\"subTotal\" />");
					writer.Write("<td>" + rm.GetString("Total") + "</td>");
					writer.Write("<td>" + groupTotal.Count + "</td>");
					writer.Write("<td>" + (-groupTotal.Amount).ToHtml(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write("</tr>");
					grandTotal.Add(groupTotal);
				}

				writer.Write("<tr><td colspan=\"3\"><br/></td></tr>");
				writer.Write("<tr class=\"subTotal\" />");
				writer.Write("<td>" + rm.GetString("GrandTotal") + "</td>");
				writer.Write("<td>&nbsp;</td>");
				writer.Write("<td>" + (-grandTotal.Amount).ToHtml(WebUtils.DomainHost, Currency) + "</td>");
				writer.Write("</tr>");
			} else {
			/*
				var AffSum = AffiliateFees.CalcAffiliateFees(AFP_ID, AmountSum, AffiliateID);
				writer.Write("<tr>");
				writer.Write("<th width=\"14%\">" + rm.GetString("thPaymentMethod") + "<br/></th>");
				writer.Write("<th width=\"6%\">" + rm.GetString("thCount") + "<br/></th>");
				writer.Write("<th width=\"10%\">" + rm.GetString("thAmount") + "<br/></th>");
				writer.Write("<th width=\"10%\">" + rm.GetString("thRefunds") + "<br/></th>");
				writer.Write("<th width=\"10%\">" + rm.GetString("thCHB") + "<br/></th>");
				writer.Write("<th width=\"10%\">" + rm.GetString("thSubTotal") + "<br/></th>");
				writer.Write("<th width=\"10%\">" + rm.GetString("thCredit") + "<br/></th>");
				writer.Write("<th width=\"30%\">" + rm.GetString("thFee") + "<br/></th>");
				writer.Write("</tr><tr><td height=\"3\"></td></tr>");

				string Notes = ""; //int NoteIndex = 1;
				var pms = WebUtils.CurrentDomain.Cache.PaymentMethods;
				foreach (var pm in pms) {
					if (AffSum.HasPaymentMethodAmount((PaymentMethod)pm.ID)) {
						var AffTotal = AffSum.GetPaymentMethodAmount((PaymentMethod)pm.ID);
						var lineTotal = AmountSum.PaymentMethodTotal((PaymentMethod)pm.ID, (PaymentMethod)pm.ID);
						var xSubTotal = lineTotal.NormAmount + lineTotal.RefAmount - lineTotal.DenAmount;
						writer.Write("<tr>");
					   writer.Write("<td valign=\"top\">" + pm.Name + "</td>");
					   writer.Write("<td valign=\"top\">" + lineTotal.TotalCount + "</td>");
					   writer.Write("<td valign=\"top\">" + (lineTotal.NormAmount + lineTotal.RefAmount).ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					   writer.Write("<td valign=\"top\">" + lineTotal.RefAmount.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					   writer.Write("<td valign=\"top\">" + lineTotal.DenAmount.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					   writer.Write("<td valign=\"top\">" + xSubTotal.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
						writer.Write("<td valign=\"top\">" + AffTotal.CreditAmount.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
						writer.Write("<td valign=\"top\">");
						if(AffTotal.Items.Count > 1){
							for(int i = 0; i < AffTotal.Items.Count; i++){
								if (!string.IsNullOrEmpty(AffTotal.Items[i].CreditInfo)) {
									if (NoteIndex == 1) Notes = "<br/>Notes:<br/>";
									Notes += NoteIndex + " - " + AffTotal.Items[i].CreditInfo + "<br/>";
									NoteIndex = NoteIndex + 1;
								}
								writer.Write(AffTotal.Items[i].Amount.ToAmountFormat(WebUtils.DomainHost, Currency) + " X " + AffTotal.Items[i].FeePercent.ToString("0.00") + "% = " + AffTotal.Items[i].Total.ToAmountFormat(WebUtils.DomainHost, Currency) + (!string.IsNullOrEmpty(AffTotal.Items[i].CreditInfo) ? "<sup>(" + (NoteIndex - 1) + ")</sup>" : "") + "<br/>");
							}
							writer.Write("<div style=\"border-top:solid 1px #C0C0C0;margin-top:2px;\">" + rm.GetString("dscTotalFee") + ": " + AffTotal.Total.ToAmountFormat(WebUtils.DomainHost, Currency) + "</div>");
						} else writer.Write(rm.GetString("dscTotalFee") + ": " + AffTotal.Total.ToAmountFormat(WebUtils.DomainHost, Currency));
					   writer.Write("</td>");
				      writer.Write("</tr>");
		            writer.Write("<tr><td height=\"1\" colspan=\"8\" style=\"border-bottom:1px dashed silver\"><img src=\"../images/img/1_space.gif\" width=\"1\" height=\"1\" border=\"0\"><br></td></tr>");
    			   }
				}
				writer.Write("<tr><td height=\"1\" bgcolor=\"gray\" colspan=\"8\"></td></tr>");
    			writer.Write("<tr><td class=\"txt11B\" colspan=\"7\">Sub Total :</td><td class=\"txt11B\">" + AffSum.SubTotal.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td></tr>");
				writer.Write("<tr><td height=\"1\" bgcolor=\"gray\" colspan=\"8\"></td></tr>");
    			writer.Write("<tr><td class=\"txt11B\" colspan=\"8\"><br />Other services :</td></tr>");
				writer.Write("<tr><td height=\"1\" bgcolor=\"gray\" colspan=\"8\"></td></tr>");

				for (int i = 0; i < AffSum.PaymentLines.Length; i++){
				   writer.Write("<tr>");
				   writer.Write("<td colspan=\"5\">" + AffSum.PaymentLines[i].Text + "</td>");
					writer.Write("<td colspan=\"2\">" + AffSum.PaymentLines[i].Quantity + " X " + AffSum.PaymentLines[i].Amount.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
					writer.Write("<td>" + AffSum.PaymentLines[i].Total.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td>");
				   writer.Write("</tr>");
				}

				writer.Write("<tr><td height=\"1\" bgcolor=\"gray\" colspan=\"8\"></td></tr>");
				writer.Write("<tr>");
				writer.Write("<td valign=\"top\" colspan=\"7\">" + rm.GetString("dscTotalPaymentAffiliate") + "</td>");
				writer.Write("<td class=\"txt11b\">" + AffSum.Total.ToAmountFormat(WebUtils.DomainHost, Currency) + "</td></tr>");
				writer.Write("<tr><td colspan=\"8\" class=\"txt10\">" + Notes + "</td></tr>");
			*/
			}
			writer.Write("</table>");
			writer.Write(" </div>");
			writer.Write("</div>");
			writer.Write("<script type=\"text/javascript\">$(function() { $('#TotalsTabs').tabs(); });<" + "/script>");
			base.RenderContents(writer);
		}
	}
}
