﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web.UI;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:PaymentMethodDropDown runat=server></{0}:PaymentMethodDropDown>")]
	public class PaymentMethodDropDown : DropDownBase
	{
		public PaymentMethodType[] PaymentMethodTypes { get; set; }
		public PaymentMethodType SinglePaymentMethodType
		{
			get
			{
				return (PaymentMethodTypes == null ? PaymentMethodType.Unknown : PaymentMethodTypes[0]);
			}
			set
			{
				if (PaymentMethodTypes == null) PaymentMethodTypes = new PaymentMethodType[1];
				PaymentMethodTypes[0] = value;
			}
		}
		public bool StartEmpty { get; set; }
        public void Populate(int? countryID, int? currency)
		{
			List<Bll.PaymentMethods.PaymentMethod> paymentMethods = null;
			List<Bll.PaymentMethods.PaymentMethod> visiblePaymentMethods = new List<Bll.PaymentMethods.PaymentMethod>();
			if (countryID == null)
			{
				paymentMethods = Bll.PaymentMethods.PaymentMethod.Search(null, null);
				if (PaymentMethodTypes == null) 
					visiblePaymentMethods = paymentMethods;
				else 
					visiblePaymentMethods.AddRange(paymentMethods.Where(pm => PaymentMethodTypes.Contains(pm.Type)));
			} 
			else
                visiblePaymentMethods = Bll.PaymentMethods.PaymentMethod.GetCountryPaymentMethods(countryID.Value, currency, PaymentMethodTypes, null);

			// translate
			// if there is no entry in the resource file for a payment method, the english name will be used
			List<KeyValuePair<int, string>> translated = new List<KeyValuePair<int, string>>();
			ResourceManager rm = WebUtils.GetResource("PaymentMethods");
			foreach (var currentPaymentMethod in visiblePaymentMethods)
			{
				string name = rm.GetString(currentPaymentMethod.Name.Replace(" ", ""));
				if (name == null || name.Trim() == "")
					name = currentPaymentMethod.Name;
				translated.Add(new KeyValuePair<int, string>((int)currentPaymentMethod.ID, name));
			}

			// order
			translated = translated.OrderBy(kv => kv.Value).ToList();

			this.DataSource = translated;
			this.DataTextField = "Value";
			this.DataValueField = "Key";
			this.DataBind(false);
		}

		protected override void OnInit(EventArgs e)
		{
			if (Items.Count == 0 && !StartEmpty)
                Populate(null, null);
			
			base.OnInit(e);
		}
	}
}
