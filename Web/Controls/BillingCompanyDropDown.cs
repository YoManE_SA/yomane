﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
    [ToolboxData("<{0}:BillingCompanyDropDown runat=server></{0}:BillingCompanyDropDown>")]
    public class BillingCompanyDropDown : DropDownBase
    {
        protected override void DataBind(bool raiseOnDataBinding)
        {
            DataSource = Bll.BillingCompany.Cache;
            DataValueField = "ID";
            DataTextField = "Name";
            base.DataBind(raiseOnDataBinding);
        }

    }
}
