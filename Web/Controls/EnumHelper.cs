﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;

namespace Netpay.Web.Controls
{
    public class EnumHelper
    {
        public static string GetViewEnumName(System.Type value)
        {
            string viewEnumName = value != null ? value.FullName : null;
            return viewEnumName;
        }

        public static System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, string>> GetEnumList(System.Type value, string excludeValue)
        {
            var ret = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, string>>();
            ResourceManager rm = WebUtils.GetResource("LegendView");
            string[] pItemText = Enum.GetNames(value);
            Array pItemValues = Enum.GetValues(value);
            for (int i = 0; i < pItemText.Length; i++)
            {
                if (pItemText[i].Equals(excludeValue))
                    continue;
                string sFullName = value.Name + "." + pItemText[i];
                string enumDisplayText = EnumNameToEnumDisplayText(pItemText[i]);
                ret.Add(new System.Collections.Generic.KeyValuePair<int, string>((int)pItemValues.GetValue(i), rm.GetString(sFullName) ?? enumDisplayText));
            }
            return ret;
        }

        /// <summary>
        /// Convert enum name to enum display text by chaning '_' to ' '
        /// </summary>
        /// <param name="enumName"></param>
        /// <returns></returns>
        public static string EnumNameToEnumDisplayText(string enumName)
        {
            if (enumName == null)
                return null;

            string enumDisplayText = enumName.Replace("_", " ");
            return enumDisplayText;
        }

        /// <summary>
        /// Convert enum display text to enum name by chaning ' ' to '_'
        /// </summary>
        /// <param name="enumDisplayText"></param>
        /// <returns></returns>
        public static string EnumDisplayTextToEnumName(string enumDisplayText)
        {
            if (enumDisplayText == null)
                return null;

            string enumName = enumDisplayText.Replace(" ", "_");
            return enumName;
        }

        /// <summary>
        /// Convert enum string to enum type value 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumValueStr"></param>
        /// <returns></returns>
        public static T? EnumFromString<T>(string enumValueStr) where T: struct, IConvertible
        {
            T enumValue;
            if (Enum.TryParse(enumValueStr, out enumValue))
                return enumValue;
            else {
                // error: the string was not an enum member 
                return null;
            }
        }

    }
}
