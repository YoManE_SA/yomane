﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Web.Controls
{
    public class AppIdentityPicker : AutoCompleteBase
    {
        public string Value
        {
            get
            {
                return ValueField.Value;
            }
            set
            {
                ValueField.Value = value;
            }
        }

        protected override void SearchItems()
        {
            var items = Bll.ApplicationIdentity.AutoComplete(SearchText);
            foreach (var i in items)
            {
                appendItem(i.ID.ToString(), i.Name, null, null, i.IsImageExist ? i.VirtualImageUrl : null);
            }
        }
    }
}
