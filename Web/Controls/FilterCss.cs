﻿using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:FilterCss runat=\"server\" />")]
	public class FilterCss : WebControl
	{
		private string _direction = "ltr";
		private int _width = 70;

		public string Direction
		{
			get { return _direction; }
			set { _direction = value.ToLower(); }
		}

		public int CellWidth
		{
			get { return _width; }
			set { _width = value; }
		}

		protected override void Render(HtmlTextWriter writer)
		{
			writer.Write("<style media=\"screen\" type=\"text/css\">");
			if (_direction == "ltr")
			{
				writer.Write(".filterCell{direction:ltr !important;text-align:left !important;width:" + _width.ToString() + "px;}");
				writer.Write(".filterLink{color:black !important;text-decoration:none !important;text-align:left;}");
				writer.Write(".filterLink:hover{background-image:url('/npcommon/images/icon_filterPlus.gif');background-repeat:no-repeat;background-position:right;padding-right:20px;}");
			}
			else
			{
				writer.Write(".filterCell{direction:ltr !important;text-align:right !important;width:" + _width.ToString() + "px;}");
				writer.Write(".filterLink{color:black !important;text-decoration:none !important;text-align:right;margin-left:20px;}");
				writer.Write(".filterLink:hover{background-image:url('/npcommon/images/icon_filterPlus.gif');background-repeat:no-repeat;background-position:left;margin-left:0;padding-left:20px;}");
			}
			writer.Write("	.filterLink span{border-bottom:1px dotted Green !important;}");
			writer.Write("	.filterNoLink{color:black !important;text-decoration:none !important;cursor:default;}");
			writer.Write("</style>");
		}
	}
}
