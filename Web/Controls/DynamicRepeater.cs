using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System;

namespace Netpay.Web.Controls
{
	public class DynamicRepeater : Repeater, System.Web.UI.IPostBackDataHandler
	{
		private List<string> _hiddenColumnsIDs = new List<string>();

		public void AddHiddenColumnID(string columnID)
		{
			_hiddenColumnsIDs.Add(columnID);
		}

        [System.Web.UI.ParseChildren(false)]
        public class EmptyContainerControl : System.Web.UI.Control { public EmptyContainerControl() { } }

        [System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public EmptyContainerControl EmptyContainer { get; set; }
        
        protected override void OnItemCreated(RepeaterItemEventArgs e)
		{
			base.OnItemCreated(e);

			foreach (string currentHiddenID in _hiddenColumnsIDs) 
			{
				Control foundControl = e.Item.FindControl(currentHiddenID);
				if (foundControl == null)
					throw new ApplicationException(string.Format("Could not find '{0}' to hide.", currentHiddenID));
				else
					foundControl.Visible = false;
			}
		}

		protected System.Collections.ArrayList baseItemsArray
		{
			get { return typeof(Repeater).GetField("itemsArray", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue((this as Repeater)) as System.Collections.ArrayList; }
			set { typeof(Repeater).GetField("itemsArray", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue((this as Repeater), value); }
		}

		protected System.Web.UI.Control[] baseControlCollection
		{
			get { return typeof(System.Web.UI.ControlCollection).GetField("_controls", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue(Controls) as System.Web.UI.Control[]; }
			set { typeof(System.Web.UI.ControlCollection).GetField("_controls", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(Controls, value); }
		}

		bool System.Web.UI.IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection) { return this.OnLoadPostData(postDataKey, postCollection); }
		void System.Web.UI.IPostBackDataHandler.RaisePostDataChangedEvent() { this.OnRaisePostDataChangedEvent(); }

		protected virtual void OnRaisePostDataChangedEvent() { }
		protected virtual bool OnLoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			if (!IsViewStateEnabled){
				int tmpValue; 
				ViewState["_!ItemCount"] = int.TryParse(postCollection[UniqueID], out tmpValue) ? (int?)tmpValue : null;
			}
			return true;
		}

		private void updateCount()
		{
			ViewState["_!ItemCount"] = Items.Count;
		}

		protected override void AddedControl(System.Web.UI.Control control, int index)
		{
			if ((control is RepeaterItem) && (new ListItemType[] { ListItemType.Item, ListItemType.AlternatingItem  }.Contains((control as RepeaterItem).ItemType))) 
				(control as RepeaterItem).ID = string.Format("item{0}", Items.Count);
			base.AddedControl(control, index);
		}

		private RepeaterItem SelfCreateItem(int itemIndex, ListItemType itemType, bool dataBind, object dataItem)
		{

			RepeaterItem repeaterItem = this.CreateItem(itemIndex, itemType);
			RepeaterItemEventArgs e = new RepeaterItemEventArgs(repeaterItem);
			this.InitializeItem(repeaterItem);
			if (dataBind) repeaterItem.DataItem = dataItem;
			this.OnItemCreated(e);
			int controlIndex = Controls.Count;
			if (itemIndex < Items.Count) controlIndex = Controls.IndexOf(Items[itemIndex]);
			if (itemType != ListItemType.Footer && itemType != ListItemType.Header)
			{
				if (FooterTemplate != null) controlIndex = controlIndex - 1;
				if (itemIndex == 0) controlIndex = (HeaderTemplate != null) ? 1 : 0;
			}

			this.Controls.AddAt(controlIndex, repeaterItem);
			if (dataBind)
			{
				repeaterItem.DataBind();
				this.OnItemDataBound(e);
				repeaterItem.DataItem = null;
			}
			return repeaterItem;
		}

		protected void fixIndexes(int startIndex)
		{
			var list = baseItemsArray;
			for (int i = startIndex; i < list.Count; i++)
			{
				(list[i] as RepeaterItem).ID = string.Format("item{0}", i);
				typeof(RepeaterItem).GetField("itemIndex", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(list[i], i);
				typeof(RepeaterItem).GetField("itemType", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(list[i], ((i % 2 == 0) ? ListItemType.Item : ListItemType.AlternatingItem));
			}
			typeof(System.Web.UI.Control).GetMethod("ClearCachedUniqueIDRecursive", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).Invoke((this as System.Web.UI.Control), null);
		}

        public RepeaterItem AddItem(object dataItem)
		{
			return AddItemAt(Items.Count, dataItem);
		}

        public RepeaterItem AddItemAt(int index, object dataItem)
		{
			bool addHeaderFooter = (Controls.Count == 0 && (HeaderTemplate != null || FooterTemplate != null));
			if (addHeaderFooter && HeaderTemplate != null)
				SelfCreateItem(0, ListItemType.Header, true, null);
			if (SeparatorTemplate != null && index > 0)
				SelfCreateItem(index, ListItemType.Separator, true, null);
			ListItemType itemType = (Items.Count % 2 == 0) ? ListItemType.Item : ListItemType.AlternatingItem;
			RepeaterItem value = SelfCreateItem(index, itemType, true, dataItem);
			if (addHeaderFooter && FooterTemplate != null)
				SelfCreateItem(Controls.Count, ListItemType.Footer, true, null);
			baseItemsArray.Insert(index, value);
			fixIndexes(index);
			updateCount();
            return value;
		}

		public virtual void RemoveItem(int index)
		{
			var list = baseItemsArray;
			if (SeparatorTemplate != null){
				if (index > 0) Controls.RemoveAt(Controls.IndexOf(list[index] as RepeaterItem) - 1);
				else if(list.Count > 1)  Controls.RemoveAt(Controls.IndexOf(list[index] as RepeaterItem) + 1);
			}
			Controls.Remove((list[index] as RepeaterItem));
			list.RemoveAt(index);
			fixIndexes(index);
			updateCount();
		}

		public void setItemFocus(int index)
		{
			var focusJS = "setTimeout(\"$('" + Items[index].ClientID + "').focus(); \", 100);";
			Page.ClientScript.RegisterStartupScript(GetType(), "focusJS", focusJS, true);
		}

        public void setItemFocusNew()
        {
            var focusJS = "setTimeout(\"$('table tbody tr:last-child td:nth-child(2) input').focus(); \", 100);";
            Page.ClientScript.RegisterStartupScript(GetType(), "focusJS", focusJS, true);
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if (!IsViewStateEnabled)
			{
				writer.Write("<input type=\"hidden\" name=\"{0}\" value=\"{1}\" />", UniqueID, Items.Count.ToString());
				//System.Web.UI.ScriptManager.RegisterHiddenField(this, UniqueID, Items.Count.ToString());
			}
            if (EmptyContainer != null) EmptyContainer.Visible = (Items.Count == 0);
            if (Items.Count > 0) base.Render(writer);
            else if (EmptyContainer != null) EmptyContainer.RenderControl(writer);
        }

		protected override void OnPreRender(EventArgs e)
		{
            if (RequiresDataBinding)
            {
                if (DataSource != null) DataBind();
                RequiresDataBinding = false;
            }
            if (EmptyContainer != null) Controls.Add(EmptyContainer);
            base.OnPreRender(e);
        }
		
		protected override void OnDataPropertyChanged()
		{
			base.OnDataPropertyChanged();
		}

		public RepeaterItem FooterItem
		{
			get{
				if(FooterTemplate != null) return Controls[Controls.Count - 1] as RepeaterItem;
				return null;
			}
		}

		public RepeaterItem HeaderItem
		{
			get{
				if(HeaderTemplate != null) return Controls[0] as RepeaterItem;
				return null;
			}
		}

	}
}
