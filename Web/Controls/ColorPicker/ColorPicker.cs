﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.ColorPicker.js", "text/javascript")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.ColorPicker.css", "text/css", PerformSubstitution = true)]

[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.blank.gif", "image/gif")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_background.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_hex.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_hsb_b.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_hsb_h.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_hsb_s.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_indic.gif", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_overlay.png", "image/png")]

[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_rgb_b.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_rgb_g.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_rgb_r.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_select.gif", "image/gif")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.colorpicker_submit.png", "image/png")]

[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.custom_background.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.custom_hex.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.custom_hsb_b.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.custom_hsb_h.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.custom_hsb_s.png", "image/png")]

[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.custom_indic.gif", "image/gif")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.custom_rgb_b.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.custom_rgb_g.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.custom_rgb_r.png", "image/png")]

[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.custom_submit.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.select.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.select2.png", "image/png")]
[assembly: System.Web.UI.WebResource("Netpay.Web.Controls.ColorPicker.images.slider.png", "image/png")]

namespace Netpay.Web.Controls
{
	public class ColorPicker : System.Web.UI.WebControls.TextBox 
	{
		public bool HideTextBox { get; set; }
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (!Visible) return;
            this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.ColorPicker.ColorPicker.js");
            this.Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link type=\"text/css\" href=\"" +  Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.ColorPicker.ColorPicker.css") + "\" rel=\"stylesheet\" />"));

			string strLoad = "$('#" + ClientID + "').ColorPicker({" +
				"onChange: function (hsb, hex, rgb) { $('#" + ClientID + "').val('#' + hex); $('#" + ClientID + "Selector div').css('backgroundColor', '#' + hex); }," +
				"onSubmit: function (hsb, hex, rgb, el) { $(el).ColorPickerHide(); }," +
				"onBeforeShow: function () { $(this).ColorPickerSetColor(this.value); }" + 
				"});\r\n";
			Page.ClientScript.RegisterStartupScript(this.GetType(), ID + "PickerLoad", strLoad, true);
		}

		protected override void Render(System.Web.UI.HtmlTextWriter Response)
		{
			if (HideTextBox) {
				Style.Add("visibility", "hidden");
				Style.Add("width", "5px");
			}
			base.Render(Response);
			Response.Write("<div style=\"display:inline;position:relative;width:18px;height:18px;background:url(" + Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.ColorPicker.images.select2.png") + "\" id=\"" + ClientID + "Selector\" onclick=\"$('#" + ClientID + "').ColorPickerShow();\" ><div class=\"color-picker-mcp\" style=\"position: absolute; top: 1px; left: 1px;width:16px;height:16px;background:url(" + Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.ColorPicker.images.select2.png") + ") center;background-color: " + Text + "\">&nbsp;</div></div>");
		}
	}
}
