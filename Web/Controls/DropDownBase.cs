﻿using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	public class DropDownBase : DropDownList
	{
		private bool _enableBlankSelection = true;
        private bool _enableNaSelection;

		private Orientation _layout = Orientation.Horizontal;
		private bool _requiredField = false;
		private string _title = null;

		private string _blankSelectionText = null;
		private string _blankSelectionValue = null;

        private string _naSelectionText = null;
        private string _naSelectionValue = null;

		protected bool _requiresDataBinding = true;
		private bool _onBind;
		private string _value = null;
		public string Value
		{
			get { return _value; }
			set
			{
				_value = value;
				if (_requiresDataBinding) return;
				try { base.SelectedValue = _value; }
				catch (System.ArgumentOutOfRangeException) { }
			}
		}

		public string BlankSelectionValue
		{
			get { return _blankSelectionValue; }
			set { _blankSelectionValue = value; }
		}

		public string BlankSelectionText
		{
			get { return _blankSelectionText; }
			set { _blankSelectionText = value; }
		}

        public string NaSelectionValue
        {
            get { return _naSelectionValue; }
            set { _naSelectionValue = value; }
        }

        public string NaSelectionText
        {
            get { return _naSelectionText; }
            set { _naSelectionText = value; }
        }

        public string Title
		{
			get { return _title; }
			set { _title = value; }
		}
		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);
			if (savedState != null) _requiresDataBinding = false;
		}

		public KeyValuePair<string, string> InitAttribute
		{
			set
			{
				Attributes.Add(value.Key, value.Value);
			}
		}

		public Orientation Layout
		{
			get { return _layout; }
			set { _layout = value; }
		}

		public bool RequiredField
		{
			get { return _requiredField; }
			set { _requiredField = value; }
		}

		public bool EnableBlankSelection
		{
			get { return _enableBlankSelection; }
			set { _enableBlankSelection = value; }
		}

        public bool EnableNaSelection
        {
            get { return _enableNaSelection; }
            set { _enableNaSelection = value; }
        }

		public bool IsSelected { get { return !string.IsNullOrEmpty(Value); } }

		protected override void OnInit(System.EventArgs e)
		{
			base.OnInit(e);
		}

		protected override void OnSelectedIndexChanged(System.EventArgs e)
		{
			if (!_onBind) _value = SelectedValue;
			base.OnSelectedIndexChanged(e);
		}
		protected override bool LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			_value = postCollection[postDataKey];
			return base.LoadPostData(postDataKey, postCollection);
		}
		protected override void RaisePostDataChangedEvent()
		{
			base.RaisePostDataChangedEvent();
		}

		protected void ClearBoundedItems()
		{
			base.ClearSelection();
			if (base.AppendDataBoundItems)
			{
				while (Items.Count > 1) Items.RemoveAt(Items.Count - 1);
			}
			else Items.Clear();
		}

		public override void DataBind()
		{
			DataBind(true);
		}
        public void ListDataBind()
        {
            DataBind(false);
        }

        protected override void DataBind(bool raiseOnDataBinding)
		{
			_onBind = false;
			ClearBoundedItems();
			SelectedIndex = -1; SelectedValue = null; ClearSelection();
			try
			{
				if (raiseOnDataBinding) base.DataBind(raiseOnDataBinding);
				else base.PerformDataBinding(DataSource as System.Collections.IEnumerable);
			}
			catch { } //resove cached value bug in net
			if (EnableBlankSelection && (Items.Count == 0 || Items[0].Value != ""))
			{
				ListItem li = new ListItem();
				li.Text = BlankSelectionText.EmptyIfNull();
				li.Value = BlankSelectionValue.EmptyIfNull();
				li.Attributes.Add("class", "emptyOption");
				Items.Insert(0, li);
			}
            if (EnableNaSelection)
            {
                ListItem li = new ListItem();
                li.Text = NaSelectionText.EmptyIfNull();
                li.Value = NaSelectionValue.EmptyIfNull();
                li.Attributes.Add("class", "emptyOption");
                Items.Add(li);
            }
			if (_value != null)
			{
				try { base.SelectedValue = _value; }
				catch (System.ArgumentOutOfRangeException)
				{
					Items.Add(new System.Web.UI.WebControls.ListItem("[Deleted " + _value + "]", _value));
					SelectedValue = _value;
				}
			} else if (Items.Count > 0) _value = Items[0].Value;
			_onBind = false;
			_requiresDataBinding = false;
		}

		protected override void EnsureDataBound()
		{
			if (this._requiresDataBinding) this.DataBind(false);
		}

		public string RenderNow()
		{
			OnPreRender(null);
			var sb = new System.Text.StringBuilder();
			var sw = new System.IO.StringWriter(sb);
			HtmlTextWriter htw = new HtmlTextWriter(sw);
			Render(htw);
			return sb.ToString();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (Title != null)
			{
				writer.Write("<table cellspacing='0' cellpadding='1' width='" + this.Width + "'>");
				if (Layout == Orientation.Horizontal)
				{
					writer.Write("<tr>");
					writer.Write("<td>" + Title);
					if (RequiredField)
						writer.Write("<span style='color:maroon;'>&nbsp;*</span>");
					writer.Write("</td>");
					writer.Write("<td>");
					base.Render(writer);
					writer.Write("</td>");
					writer.Write("</tr>");
				}
				else
				{
					writer.Write("<tr>");
					writer.Write("<td>" + Title);
					if (RequiredField)
						writer.Write("<span style='color:maroon;'>&nbsp;*</span>");
					writer.Write("</td>");
					writer.Write("</tr>");
					writer.Write("<tr>");
					writer.Write("<td>");
					base.Render(writer);
					writer.Write("</td>");
					writer.Write("</tr>");
				}
				writer.Write("</table>");
			}
			else
			{
				base.Render(writer);
			}
		}
	}
}
