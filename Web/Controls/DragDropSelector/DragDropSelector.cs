﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:DragDropSelector runat=server></{0}:DragDropSelector>")]
	public class DragDropSelector : WebControl
	{
		private List<ListItem> _availableItems = new List<ListItem>();
		private List<ListItem> _selectedItems = new List<ListItem>();

		public List<ListItem> AvailableItems
		{
			get 
			{
				if (_availableItems.Count == 0)
				{
					string posted = HttpContext.Current.Request[ClientID + "dragDropSelectorAvailableVals"];
					if (posted != null && posted.Trim() != "") 
					{
						string[] items = posted.Split(',');
						foreach (string currentItem in items) 
						{
							string[] currentItemSplit = currentItem.Split(new string[] { "#%#" }, StringSplitOptions.None);
							string currentValue = currentItemSplit[0];
							string currentText = currentItemSplit[1];

							_availableItems.Add(new ListItem(currentText, currentValue));
						}
					}
				}
				
				return _availableItems; 
			}
			set { _availableItems = value; }
		}

		public List<ListItem> SelectedItems
		{
			get
			{
				if (_selectedItems.Count == 0)
				{
					string posted = HttpContext.Current.Request[ClientID + "dragDropSelectorSelectedVals"];
					if (posted != null && posted.Trim() != "")
					{
						string[] items = posted.Split(',');
						foreach (string currentItem in items)
						{
							string[] currentItemSplit = currentItem.Split(new string[] { "#%#" }, StringSplitOptions.None);
							string currentValue = currentItemSplit[0];
							string currentText = currentItemSplit[1];

							_selectedItems.Add(new ListItem(currentText, currentValue));
						}
					}
				}

				return _selectedItems;
			}
			set { _selectedItems = value; }
		}

		public bool IsSelected 
		{
			get 
			{
				return SelectedItems.Count > 0;
			}
		}

		private string GetDelimited(List<ListItem> list)
		{
			if (list == null || list.Count == 0)
				return "";
			
			StringBuilder builder = new StringBuilder();
			foreach(ListItem currentItem in list)
			{
				builder.Append(currentItem.Value + "#%#" + currentItem.Text + ",");
			}
			builder.Remove(builder.Length - 1, 1);
			
			return builder.ToString();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			// embed scripts
			this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.DragDropSelector.DragDropSelector.js");

			// embed css
			string includeTemplate = "<link rel='stylesheet' text='text/css' href='{0}' />";
			string includeLocation = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.DragDropSelector.DragDropSelector.css");
			LiteralControl include = new LiteralControl(String.Format(includeTemplate, includeLocation));
			Page.Header.Controls.Add(include);
		}
		
		protected override void Render(HtmlTextWriter writer)
		{
			writer.Write("<table cellpadding=\"0\" cellspacing=\"0\">");
			writer.Write("<tr>");
			writer.Write("<td style=\"padding:0px;\">");
			writer.Write(string.Format("<div id=\"{0}dragDropSelectorAvailable\" baseID=\"{0}\" class=\"dragDropSelectorBox\" style=\"height:{1}; width:{2};\">", ClientID, Height.ValueIfEmpty(Unit.Pixel(200)), Width.ValueIfEmpty(Unit.Pixel(200))));
			foreach (ListItem currentItem in AvailableItems)
				writer.Write(string.Format("<div key=\"{0}\" value=\"{1}\" class=\"dragDropSelectorItem\">{1}</div>", currentItem.Value, currentItem.Text));
			writer.Write("</div>");
			writer.Write("</td>");
			writer.Write("<td style=\"padding:0px;\">&nbsp;</td>");
			writer.Write("<td style=\"padding:0px;\">");
			writer.Write(string.Format("<div id=\"{0}dragDropSelectorSelected\" baseID=\"{0}\" class=\"dragDropSelectorBox\" style=\"height:{1}; width:{2};\">", ClientID, Height.ValueIfEmpty(Unit.Pixel(200)), Width.ValueIfEmpty(Unit.Pixel(200))));
			foreach (ListItem currentItem in SelectedItems)
				writer.Write(string.Format("<div key=\"{0}\" value=\"{1}\" class=\"dragDropSelectorItem\">{1}</div>", currentItem.Value, currentItem.Text));
			writer.Write("</div>");
			writer.Write("</td>");
			writer.Write("</tr>");
			writer.Write("</table>");
			writer.Write(string.Format("<input type=\"hidden\" id=\"{0}dragDropSelectorAvailableVals\" name=\"{0}dragDropSelectorAvailableVals\" value=\"{1}\" />", ClientID, GetDelimited(AvailableItems)));
			writer.Write(string.Format("<input type=\"hidden\" id=\"{0}dragDropSelectorSelectedVals\" name=\"{0}dragDropSelectorSelectedVals\" value=\"{1}\" />", ClientID, GetDelimited(SelectedItems)));
			writer.Write(string.Format("<script>netpay.webControls.DragDropSelector.init('{0}dragDropSelectorAvailable', '{0}dragDropSelectorSelected')</script>", ClientID));
		}
	}
}
