﻿if (netpay == null) { var netpay = new Object() }
if (netpay.webControls == null) { netpay.webControls = new Object() }

netpay.webControls.DragDropSelector =
{
	init: function (availableBoxID, selectedBoxID) 
	{
		$("#" + availableBoxID + ",#" + selectedBoxID).children().draggable
		({ 
			revert: "invalid",
			helper: "clone",
			cursor: 'pointer',
			scroll: false,
			appendTo: 'body' 
		});
		
		$("#" + availableBoxID).droppable
		({
			accept: "#" + selectedBoxID + " > div",
			activeClass: "dragDropSelectorBoxActive",
			hoverClass: "dragDropSelectorBoxHover",
			drop: function(event, ui) 
			{
				ui.draggable.appendTo($(this));
				netpay.webControls.DragDropSelector.persist($(this).attr("baseID"));
			}
		});

		$("#" + selectedBoxID).droppable
		({
			accept: "#" + availableBoxID + " > div",
			activeClass: "dragDropSelectorBoxActive",
			hoverClass: "dragDropSelectorBoxHover",
			drop: function(event, ui) 
			{
				ui.draggable.appendTo($(this));
				netpay.webControls.DragDropSelector.persist($(this).attr("baseID"));
			}
		});
	},

	persist: function (baseID) 
	{
		var availableItemsBox = $("#" + baseID + "dragDropSelectorAvailable");
		var selectedItemsBox = $("#" + baseID + "dragDropSelectorSelected");
		var availableItemsVals = $("#" + baseID + "dragDropSelectorAvailableVals");
		var selectedItemsVals = $("#" + baseID + "dragDropSelectorSelectedVals");
				
		availableItemsVals.val("")
		availableItemsBox.children().each(function (index, value) {availableItemsVals.val(availableItemsVals.val() + $(value).attr("key") + "#%#" + $(value).attr("value") + ",")});
		availableItemsVals.val(availableItemsVals.val().substring(0, availableItemsVals.val().length - 1));

		selectedItemsVals.val("")
		selectedItemsBox.children().each(function (index, value) {selectedItemsVals.val(selectedItemsVals.val() + $(value).attr("key") + "#%#" + $(value).attr("value") + ",")});
		selectedItemsVals.val(selectedItemsVals.val().substring(0, selectedItemsVals.val().length - 1));
	}
}

