﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll.Reports;

namespace Netpay.Web.Controls
{
	public class ReportUpdateInfo : WebControl
	{
		private ReportType _report;
		
		public ReportType Report
		{
			get { return _report; }
			set { _report = value; }
		}
		
		private DateTime? PackageUpdateTime(string packageName)
		{
            return DateTime.Now;
            /*
            var record = IntegrationService.GetPackageLastUpdateLog(packageName);
            if (record == null)
				return null;
			else 
				return record.StartDate;
            */
		}

		private string ReportUpdateTimeSinglePackage(DateTime? startDate)
		{			
			if (startDate != null)
			{
				return "LAST UPDATE " + startDate.Value.ToString("dd/MM/yyyy HH:mm");
			}
			else
			{
				return "<span style=\"color:red\">NEVER UPDATED</span>";
			}
		}

		private string ReportUpdateTimeDoublePackage(DateTime? startDate1, DateTime? startDate2)
		{
			if (startDate1 == null || startDate2 == null)
			{
				return "<span style=\"color:red\">NEVER UPDATED</span>";
			}
			else
			{
				return "LAST UPDATE " + ((DateTime.Compare(startDate1.Value, startDate2.Value) < 0) ? startDate1.Value : startDate2.Value).ToString("dd/MM/yyyy hh:mm");
			}
		}

		public override void RenderControl(HtmlTextWriter writer)
		{			
			string reportDisplay = "";
			
			
			switch (_report)
			{
				case ReportType.AdminTransactionsReport:
					reportDisplay = ReportUpdateTimeSinglePackage(PackageUpdateTime("DenormalizeTransactions.dtsx"));
					break;
				case ReportType.AdminRollingReserves:
					reportDisplay = ReportUpdateTimeSinglePackage(PackageUpdateTime("RollingReserveReport.dtsx"));
					break;
				case ReportType.AdminFailStats:
					reportDisplay = ReportUpdateTimeSinglePackage(PackageUpdateTime("DenormalizeTransactions.dtsx"));
					break;
				case ReportType.AdminDailyRiskByMerchant:
					reportDisplay = ReportUpdateTimeDoublePackage(PackageUpdateTime("DenormalizeTransactions.dtsx"), PackageUpdateTime("DailyRiskReportByCompany.dtsx"));
					break;
				case ReportType.AdminDailyRiskByTerminal:
					reportDisplay = ReportUpdateTimeDoublePackage(PackageUpdateTime("DenormalizeTransactions.dtsx"), PackageUpdateTime("DailyRiskReportByTerminal.dtsx"));					
					break;
				case ReportType.AdminDailyStatusByMerchant:
					reportDisplay = ReportUpdateTimeDoublePackage(PackageUpdateTime("DenormalizeTransactions.dtsx"), PackageUpdateTime("DailyStatusReportByMerchant.dtsx"));
					break;
				case ReportType.AdminDailyStatusByTerminal:
					reportDisplay = ReportUpdateTimeDoublePackage(PackageUpdateTime("DenormalizeTransactions.dtsx"), PackageUpdateTime("DailyStatusReportByTerminal.dtsx"));
					break;
				default:
					reportDisplay = "Report unsupported.";
					break;
			}
			
			writer.Write("<span style='font-size:12px; color:#000000; text-align:left;'>");
			writer.Write(reportDisplay);
			writer.Write("</span>");
		}
	}
}
