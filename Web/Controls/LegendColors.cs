﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
	public class LegendColors : System.Web.UI.WebControls.WebControl
	{
		public object Items { get; set; }
		public LegendColors() : base(System.Web.UI.HtmlTextWriterTag.Div) { }
		public string Title { get; set; }
		private bool _requiresDataBinding = true;
		protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
		{
			if (Items == null) return;
			if (!string.IsNullOrEmpty(Title)) writer.Write(string.Format("<span style=\"font-weight:bold;\">{0}</span>: &nbsp;&nbsp;&nbsp;&nbsp;", Title));
			var dicList = Items as System.Collections.IDictionary;
			foreach(System.Collections.DictionaryEntry i in dicList)
				writer.Write(string.Format("<span style=\"background-color:{1};\">&nbsp;&nbsp;</span> {0} &nbsp;&nbsp;&nbsp;&nbsp;", i.Key, System.Drawing.ColorTranslator.ToHtml((System.Drawing.Color)i.Value)));
			base.RenderContents(writer);
		}

		public override void DataBind()
		{
			base.DataBind();
			_requiresDataBinding = false;
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (_requiresDataBinding) DataBind();
			base.OnPreRender(e);
		}

	}
}
