﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Netpay.Web.Controls
{
    [ToolboxData("<{0}:FaqList runat=server></{0}:FaqList>")]
    public class FaqList : WebControl 
    {
        private int? _categoryID = null;

        public int? CategoryID
        {
            get { return _categoryID; }
            set { _categoryID = value; }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (CategoryID == null)
                return;

            int index = 0;
			var faqs = Netpay.Bll.Faqs.Faq.Search(new Bll.Faqs.Faq.SearchFilters() { GroupId = CategoryID.Value }, null);
            writer.Write("<table>");
            foreach (var item in faqs)
            {
                writer.Write("<tr>");
                writer.Write("<td>");
                writer.Write("<a href=\"javascript://\" onclick=\"$('#faqRow" + index + "').toggle('blind')\">" + item.Question + "</a>");
                writer.Write("</td>");
                writer.Write("</tr>");
                writer.Write("<tr id=\"faqRow" + index + "\" style=\"display:none;\">");
                writer.Write("<td>");
                writer.Write(item.Answer);
                writer.Write("</td>");
                writer.Write("</tr>");
                writer.Write("<tr><td>&nbsp;</td></tr>");

                index++;
            }
            writer.Write("</table>");
        }
    }
}
