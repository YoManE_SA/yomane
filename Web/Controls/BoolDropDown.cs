﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
	public class BoolDropDown : DropDownBase
	{
		public string TrueText { get; set; }
		public string FalseText { get; set; }
		public BoolDropDown() 
		{
			TrueText = "Yes"; 
			FalseText = "No";
		}

		protected override void DataBind(bool raiseOnDataBinding)
		{
			DataSource = new Dictionary<bool, string>() { { true, TrueText }, { false, FalseText} };
			DataTextField = "Value"; DataValueField = "Key";
			base.DataBind(raiseOnDataBinding);
		}

		public bool? BoolValue
		{
			get { if(string.IsNullOrEmpty(Value)) return null; return Value.ToLower() == "true"; }
			set { if(value == null) Value = null; else Value = value.ToString(); }
		}
	}
}
