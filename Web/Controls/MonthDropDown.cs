﻿using System;
using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:MonthDropDown runat=server></{0}:MonthDropDown>")]
	public class MonthDropDown : DropDownBase
	{
		public bool ShowNames = true;

		public int? SelectedMonth
		{
			get
			{
				if (string.IsNullOrEmpty(Value)) return null;
				return int.Parse(Value.Trim());
			}
			set
			{
                Value = value != null ? value.ToString() : null;
				ClearSelection();
				ListItem item = Items.FindByValue(value.ToString());
				if (item != null)
					item.Selected = true;
			}
		}

		protected override void DataBind(bool raiseOnDataBinding)
		{
			DataSource = GetMonthList(ShowNames);
			DataValueField = "Key";
			DataTextField = "Value";
			base.DataBind(raiseOnDataBinding);
		}

		public System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, string>> GetMonthList(bool listNames)
		{
			var ret = new System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, string>>();
			if (listNames)
			{
				ResourceManager rm = WebUtils.GetResource("Months");
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(1, rm.GetString("January")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(2, rm.GetString("February")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(3, rm.GetString("March")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(4, rm.GetString("April")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(5, rm.GetString("May")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(6, rm.GetString("June")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(7, rm.GetString("July")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(8, rm.GetString("August")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(9, rm.GetString("September")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(10, rm.GetString("October")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(11, rm.GetString("November")));
				ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(12, rm.GetString("December")));
			}
			else
			{
				for (var i = 1; i <= 12; i++)
					ret.Add(new System.Collections.Generic.KeyValuePair<int, string>(i, i.ToString("00")));
			}
			return ret;		
		}
	}
}
