using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
	public class ApplicationIdentityDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
            //Create the search filter so the drop down will show only the active
            //App indentities.
            var sf = new Bll.ApplicationIdentity.SearchFilters() { IsActive = true};
            
			DataSource = Netpay.Bll.ApplicationIdentity.Search(sf, null);
			DataTextField = "Name"; DataValueField = "ID";
			base.DataBind(raiseOnDataBinding);
		}

	}
}