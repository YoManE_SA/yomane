﻿using System;
using System.Web.UI;
using System.Linq;
using System.Collections.Specialized;

[assembly: WebResource("Netpay.Web.Controls.CreditcardInput.SwipeManager.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.CreditcardInput.SwipeCardOn_24x24.png", "image/png")]
[assembly: WebResource("Netpay.Web.Controls.CreditcardInput.SwipeCardOff_24x24.png", "image/png")]

namespace Netpay.Web.Controls
{
    public class SwipeManager : System.Web.UI.WebControls.HiddenField
    {
        public string CardNumberID { get; set; }
        public string CardholderNameID { get; set; }
        public string ExpMonthID { get; set; }
        public string ExpYearID { get; set; }
        public string CvvID { get; set; }

        public string OnImageUrl { get; set; }
        public string OffImageUrl { get; set; }
        public bool ShowImage { get; set; }

        public string TrackII { get { return Value; } set { Value = value; } }
        public bool HasTrackData { get { return !string.IsNullOrEmpty(Value); } }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (OnImageUrl == null) OnImageUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.CreditcardInput.SwipeCardOn_24x24.png");
            if (OffImageUrl == null) OffImageUrl = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.CreditcardInput.SwipeCardOff_24x24.png");
        }

        /*
        public string TrackI
        {
            get
            {
                var ret = RawData;
                if (string.IsNullOrEmpty(ret)) throw new Exception("Invalid track data");
                if (ret.StartsWith(";")) ret = ret.TrimStart(';');
                var trackIndex = ret.IndexOf('=');
                if (trackIndex == -1) throw new Exception("Invalid track data");
                return ret.Substring(0, trackIndex);
            }
        }
        */

            /*
        public string TrackII
        {
            get
            {
                if (string.IsNullOrEmpty(RawData) || !RawData.Contains("="))
                    throw new Exception("Invalid track data");

                string trackII = null;
                if (RawData.Contains("%")) // contains track 1
                    trackII = RawData.Substring(RawData.IndexOf(';'));
                else
                    trackII = RawData;
                trackII = trackII.Replace(";", "").Replace("?", "");

                return trackII;
            }
        }
        */

        //public string TrackI_II { get { return string.Format("{0}={1}", TrackI, TrackII); } }

        private string ResolveImageUrl(string imageUrl)
        {
            //if(imageUrl)
            return string.IsNullOrEmpty(imageUrl) ? null : ResolveUrl(imageUrl);
        }

        protected override void OnPreRender(EventArgs e)
        {
            CreditcardInput ccInput = null;
            string setScript = "function " + ClientID + "_setSwipeData(data){\r\n";
            if (!string.IsNullOrEmpty(ExpMonthID)) setScript += "netpay.webControls.SwipeManager.setInput('" + NamingContainer.FindControl(ExpMonthID).ClientID + "', data.expMonth);\r\n";
            if (!string.IsNullOrEmpty(ExpYearID)) setScript += "netpay.webControls.SwipeManager.setInput('" + NamingContainer.FindControl(ExpYearID).ClientID + "', data.expYear);\r\n";
            if (!string.IsNullOrEmpty(CardholderNameID)) setScript += "netpay.webControls.SwipeManager.setInput('" + NamingContainer.FindControl(CardholderNameID).ClientID + "', data.fullName);\r\n";
            if (!string.IsNullOrEmpty(CvvID)) setScript += "netpay.webControls.SwipeManager.setInput('" + NamingContainer.FindControl(CvvID).ClientID + "', (data.trackII ? '' : null));\r\n";
            if (!string.IsNullOrEmpty(CardNumberID))
            {
                ccInput = NamingContainer.FindControl(CardNumberID) as CreditcardInput;
                if (ccInput != null)
                {
                    if (ccInput.SingleFieldMode) setScript += "netpay.webControls.SwipeManager.setInput('" + NamingContainer.FindControl(CardNumberID).ClientID + "_F_1', data.cardNumber);\r\n";
                    else setScript += "netpay.webControls.SwipeManager.setCcInputNumber('" + NamingContainer.FindControl(CardNumberID).ClientID + "', data.cardNumber);\r\n";
                }
                else setScript += "netpay.webControls.SwipeManager.setInput('" + NamingContainer.FindControl(CardNumberID).ClientID + "', data.cardNumber);\r\n";
            }
            //setScript += "document.getElementById('" + ClientID + "').value = data.rawData;\r\n";
            setScript += "document.getElementById('" + ClientID + "').value = data.trackII;\r\n";
            if (ShowImage) setScript += "document.getElementById('" + ClientID + "_Image').src = data.cardNumber ? '" + ResolveImageUrl(OnImageUrl) + "' : '" + ResolveImageUrl(OffImageUrl) + "';\r\n";
            setScript += "}\r\n";
            var submitFields = new System.Collections.Generic.List<string>();
            if (!string.IsNullOrEmpty(ExpMonthID)) submitFields.Add(NamingContainer.FindControl(ExpMonthID).ClientID);
            if (!string.IsNullOrEmpty(ExpYearID)) submitFields.Add(NamingContainer.FindControl(ExpYearID).ClientID);
            if (!string.IsNullOrEmpty(CvvID)) submitFields.Add(NamingContainer.FindControl(CvvID).ClientID);
            if (ccInput != null)
            {
                if (ccInput.SingleFieldMode) submitFields.Add(ccInput.ClientID + "_F_1");
                else for (int i = 0; i < 4; i++) submitFields.Add(ccInput.ClientID + "_F_" + i);
            }
            setScript += "function " + ClientID + "_disableFields(value){\r\n";
            //setScript += string.Join("", submitFields.Select(v => string.Format("document.getElementById('{0}').disabled=value;\r\n", v)));
            setScript += "}\r\n";

            Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.CreditcardInput.SwipeManager.js");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), ClientID + "_SetProc", setScript, true);
            Page.ClientScript.RegisterStartupScript(this.GetType(), ClientID + "_Load", "netpay.webControls.SwipeManager.attach(" + ClientID + "_setSwipeData);", true);
            if (HasTrackData) Page.ClientScript.RegisterStartupScript(this.GetType(), ClientID + "_LoadData", ClientID + "_disableFields(true);", true);
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), ClientID + "_Submit", ClientID + "_disableFields(false);");
            base.OnPreRender(e);
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            base.Render(writer);
            if (ShowImage)
            {
                string imageUrl = ResolveImageUrl(HasTrackData ? OnImageUrl : OffImageUrl);
                writer.Write("<img id=\"{0}_Image\" align=\"bottom\" onclick=\"netpay.webControls.SwipeManager.swipeClear()\" title=\"Swipe card, press to clear\" src=\"{1}\"  style=\"vertical-align:bottom\" />", ClientID, imageUrl);
            }
        }

    }
}
