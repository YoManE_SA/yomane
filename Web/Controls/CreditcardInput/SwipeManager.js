﻿if (netpay == null) { var netpay = new Object() }
if (netpay.webControls == null) { netpay.webControls = new Object() }

netpay.webControls.SwipeManager =
{
    onDetected: null,
    lastSwipe: null,
    isOnCancel: false,

    attach: function (detectFunction)
	{
	    onDetected = detectFunction;
	    if (document.onkeypress != this.swipeDetect)
	        document.onkeypress = this.swipeDetect;
        //document.ondblclick = this.simulateSwipe;
	    //netpay.webControls.SwipeManager.swipeClear();
	},

    isLuhn: function (ccNumber)
    {
        var multiplyBy = 1;
        var sum = 0;
        for (var i = 0, len = ccNumber.length; i < len; i++) {
            var digit = Number(ccNumber[i]);
            var digitResult = digit * multiplyBy;
            if (digitResult >= 10) {
                var resultStr = String(digitResult);
                digitResult = Number(resultStr[0]) + Number(resultStr[1]);
            }

            sum += digitResult;
            multiplyBy = multiplyBy == 1 ? 2 : 1;
        }

        var modResult = sum % 10;
        return modResult == 0;
    },

    isIsracart: function (ccNumber)
    {
        var sum = 0;
        for (var i = 0; i <= 8; i++)
        {
            var digit = Number(ccNumber[i]);
            var multiplyBy = 9 - i;
            var digitResult = digit * multiplyBy;
            sum += digitResult;
        }

        var modResult = sum % 11;
        return modResult == 0;
    },

    simulateSwipe: function (e)
    {
        //var str = ';4580000000000000=19122010000010112000?';
        //var str = '4580000000000000=19062010000002051000';
        var str = ';5326000000000000=16092010000010112000?'; 
        //var str = "%B1234123412341234^Carduser/John^030510100000019301000000877000000?;1234123412341234=0305101193010877?";
        //var str = '%B0000000000000000^  name     /     some    ^0000201000000000000000000000000?;12000000000080000=0108305290000017120?';

        for (var i = 0; i < str.length; i++)
            netpay.webControls.SwipeManager.sendChar(window.document, str.charCodeAt(i));
    },

    completeTimer: null,

	swipeDetect: function (e)
	{ 
	    var evt = window.Event;
	    var keyCode = evt ? e.which : e.keyCode;
	    var keyChar = String.fromCharCode(keyCode);
	    var manager = netpay.webControls.SwipeManager;

	    if (manager.lastSwipe == null)
	        manager.lastSwipe = keyChar;
	    else
	        manager.lastSwipe += keyChar;

	    if (manager.completeTimer != null)
	        window.clearTimeout(manager.completeTimer);
	    manager.completeTimer = window.setTimeout(function () { manager.swipeExtract(manager.lastSwipe); }, 500);

	    return true;
	},

	swipeExtract: function (value) {
	    netpay.webControls.SwipeManager.lastSwipe = null;
	    if (value.indexOf("=") == -1)
	        return;

	    var cardData = {
	        rawData: value,
	        trackII: null,
	        cardNumber: null,
	        expMonth: null,
	        expYear: null,
	        fullName: null
	    };

	    // track 1
	    if (value.indexOf("%") != -1) {
	        var name = value.substring(value.indexOf("^") + 1, value.lastIndexOf("^"));
	        name = name.replace(/[^a-z/]/gi, '');
	        cardData.fullName = name.split("/").reverse().join(" ");
	    }

        // track 2
	    var trackII = null;
	    if (value.indexOf("%") != -1) // contains track 1
	        trackII = value.substring(value.indexOf(";"), value.indexOf("?", value.indexOf(";")) + 1);
	    else
	        trackII = value;

	    if (trackII.indexOf(";") != -1 && trackII.indexOf("?") != -1)
	        trackII = trackII.substring(trackII.indexOf(";") + 1, trackII.indexOf("?"));
	       
	    trackII = trackII.replace(/\s/g, ""); 
	    cardData.trackII = trackII;

	    if (trackII.indexOf("=") != -1) {
	        var split = trackII.split('=');
	        cardData.cardNumber = split[0];
	        cardData.expMonth = split[1].substring(2, 4);
	        if (isNaN(cardData.expMonth) || Number(cardData.expMonth) > 12)
	            cardData.expMonth = null;
	        cardData.expYear = '20' + split[1].substring(0, 2);
	        if (isNaN(cardData.expYear) || Number(cardData.expYear) < new Date().getFullYear())
	            cardData.expYear = null;
	    }

	    //if (trackII.indexOf("=") != -1)
	    //{
	    //    var split = trackII.split('=');
	    //    var ccNumber = split[0];
	    //    var ccIsracardNumber = split[1].substring(0, 9);

	    //    if (netpay.webControls.SwipeManager.isLuhn(ccNumber)) {
	    //        cardData.cardNumber = split[0];
	    //        cardData.expMonth = split[1].substring(2, 4);
	    //        if (isNaN(cardData.expMonth) || Number(cardData.expMonth) > 12)
	    //            cardData.expMonth = null;
	    //        cardData.expYear = '20' + split[1].substring(0, 2);
	    //        if (isNaN(cardData.expYear) || Number(cardData.expYear) < new Date().getFullYear())
	    //            cardData.expYear = null;
	    //    }
	    //    else if (netpay.webControls.SwipeManager.isIsracart(ccIsracardNumber)) {
	    //        cardData.cardNumber = split[1].substring(1, 9);
	    //        cardData.expMonth = split[1].substring(16, 18);
	    //        if (isNaN(cardData.expMonth) || Number(cardData.expMonth) > 12)
	    //            cardData.expMonth = null;
	    //        cardData.expYear = '20' + split[1].substring(14, 16);
	    //        if (isNaN(cardData.expYear) || Number(cardData.expYear) < new Date().getFullYear())
	    //            cardData.expYear = null;
	    //    }
	    //    else
	    //    {
	    //        alert("Invalid card number");
	    //    }
	    //}

	    //console.log(cardData.cardNumber);
	    //console.log(cardData.expMonth);
	    //console.log(cardData.expYear);

	    if (onDetected)
	        onDetected(cardData);
	},

	sendChar: function(target, charCode){
	    var evt = document.createEvent("KeyboardEvent");
	    if (evt.initKeyboardEvent) evt.initKeyboardEvent("keypress", true, true, window, charCode, null, null, false, null);
	    else if (evt.initKeyEvent) evt.initKeyEvent("keypress", true, true, window, false, false, false, false, 0, charCode);
	    Object.defineProperty(evt, 'keyCode', { get: function () { return this.keyCodeVal; } });
	    Object.defineProperty(evt, 'which', { get: function () { return this.keyCodeVal; } });
	    evt.keyCodeVal = charCode;
	    target.dispatchEvent(evt);
	},
    
	swipeClear: function ()
	{
	    var cardData = { rawData:null, cardNumber: null, trackII: null, expMonth: null, expYear: null };
	    netpay.webControls.SwipeManager.lastSwipe = null;
	    if (onDetected)
	        onDetected(cardData);
	},
    
	setInput: function (clientId, value)
	{
	    var element = document.getElementById(clientId);
	    element.value = value ? value : '';
	    //element.disabled = (value != null);
	    if (value && element.tagName.toUpperCase() == 'SELECT' && element.value != value) {
	        var option = document.createElement("option");
	        option.text = value; option.value = value;
	        element.appendChild(option);
	        element.selectedIndex = element.length - 1;
	    }
	},

	setCcInputNumber: function (ccInputId, value) {
	    var itemArray = new Array();
	    if (value) {
	        for (var i = 0; i < Math.min(16, value.length); i += 4)
	            itemArray.push(value.substring(i, Math.min(value.length, i + 4)));
	    }
	    while (itemArray.length < 4) itemArray.push(value ? '' : null);
	    for (var i = 0; i < 4; i++)
	        setInput(ccInputId + '_F_' + i, itemArray[i]);
	}

}