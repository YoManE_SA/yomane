﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Bll;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:CreditCardInput runat=server></{0}:CreditCardInput>")]
	[ValidationPropertyAttribute("CardValue")]
	public class CreditcardInput : WebControl, INamingContainer, System.Web.UI.IPostBackEventHandler
	{
		public event EventHandler Changed;
		public System.Web.UI.WebControls.Unit ElementWidth { get; set; }
		public bool SingleFieldMode { get; set; }
		public bool AutoPostBack { get; set; }
        public bool EnableSwipe { get; set; }
        public string OnClientSwipe { get; set; }
        public bool CausesValidation { get; set; }
        private bool _netpayRequired = false;

		public CreditcardInput()
			: base(HtmlTextWriterTag.Span)
		{
			//ElementWidth = new System.Web.UI.WebControls.Unit(38.0, UnitType.Pixel);
			this.Attributes.Add("style", "direction:ltr !important;");
			this.Attributes.Add("dir", "ltr");
		}

        public void RaisePostBackEvent(string eventArgument) { if (Changed != null) Changed(this, EventArgs.Empty); }

		public bool NetpayRequired
		{
			get { return _netpayRequired; }
			set { _netpayRequired = value; }
		}

		public string CardValue { get { return Card.CardNumber; } }

		public override bool Enabled
		{
			get { return base.Enabled; }
			set 
			{
				base.Enabled = value;
				foreach(var c in Controls)
					if (c is HtmlControl) (c as HtmlControl).Disabled = !Enabled;
			}
		}
	
		protected override void CreateChildControls()
		{
			for (int i = 1; i <= (SingleFieldMode ? 1 : 4); i++)
			{
				HtmlInputText currentInput = new HtmlInputText();
				currentInput.ID = "F_" + i.ToString();
				currentInput.Attributes.Add("style", String.Format("width:{0};", ElementWidth.ToString()));
				currentInput.Attributes.Add("maxlength", SingleFieldMode ? "20" : "4");
				if (TabIndex != 0) currentInput.Attributes.Add("tabindex", (TabIndex + i).ToString());
				currentInput.Attributes.Add("class", "inputData " + CssClass);
                currentInput.Attributes.Add("autofocus", "");
                currentInput.Attributes.Add("autocomplete", "off");
                //currentInput.AutoCompleteType = AutoCompleteType.None;
				//currentInput.Attributes.Add("onpaste", "netpay.webControls.CreditcardInput.paste('" + _baseID + "');");
				currentInput.Attributes.Add("onkeypress", "var charCode = (typeof event.which == 'number') ? event.which : event.keyCode; event.returnValue = (charCode >= '0'.charCodeAt(0) && charCode <= '9'.charCodeAt(0)) || (charCode == ' '.charCodeAt(0) || charCode == '-'.charCodeAt(0) || charCode == 8 || charCode == 9 || charCode == 39 || charCode == 37 || charCode == 46 || charCode == 0); return event.returnValue;");
				if (_netpayRequired)
					currentInput.Attributes.Add("netpayRequired", "true");
				if (i > 1)
				{
					Literal currentSpacer = new Literal();
					currentSpacer.Text = "&nbsp;";
					Controls.Add(currentSpacer);
				}
				Controls.Add(currentInput);
				currentInput.Disabled = !Enabled;
			}
			
			// repopulate if is postback
			if (this.Page.IsPostBack)
			{
				for (int i = 1; i <= (SingleFieldMode ? 1 : 4); i++)
				{
					HtmlInputText currentInput = (HtmlInputText)FindControl("F_" + i.ToString());
					string postedValue = Page.Request[currentInput.Name];
					if (postedValue != null) currentInput.Value = postedValue;
				}				
			}		
		}

        protected override void OnPreRender(EventArgs e)
        {
            if (AutoPostBack) { 
                for (int i = 1; i <= (SingleFieldMode ? 1 : 4); i++) { 
                    HtmlInputText currentInput = (HtmlInputText)FindControl("F_" + i.ToString());
                    currentInput.Attributes.Add("onchange", Page.ClientScript.GetPostBackEventReference(new PostBackOptions(this, "Changed"), true));
                    //CausesValidation
                }
            }
            if (!SingleFieldMode) {
                for (int i = 1; i <= 3; i++) {
                    HtmlInputText currentInput = (HtmlInputText)FindControl("F_" + i.ToString());
                    currentInput.Attributes.Add("onkeyup", string.Format("if(this.value.length == 4){{{0}.focus();}}", FindControl("F_" + (i + 1).ToString()).ClientID));
                }
            }
            base.OnPreRender(e);
        }

        protected override void Render(HtmlTextWriter writer)
		{
			CssClass = "";
			base.Render(writer);
		}

		public Boolean IsEmpty
		{
			get
			{
				for (int i = 1; i <= (SingleFieldMode ? 1 : 4); i++)
				{
					HtmlInputText currentInput = (HtmlInputText)FindControl("F_" + i.ToString());
					if (!string.IsNullOrEmpty(currentInput.Value)) return false;
				}
				return true;
			}
		}

		public Bll.PaymentMethods.CreditCard Card
		{
			get
			{
				EnsureChildControls();
				StringBuilder ccBuilder = new StringBuilder();
				for (int i = 1; i <= (SingleFieldMode ? 1 : 4); i++)
				{
					HtmlInputText currentInput = (HtmlInputText)FindControl("F_" + i.ToString());
					ccBuilder.Append(currentInput.Value);
				}
				return Bll.PaymentMethods.CreditCard.FromCardNumber(ccBuilder.Replace("-", "").Replace(" ", "").ToString());
			}
			set
			{
				string[] parsed = { };
				if (value != null && value.PaymentMethodType == CommonTypes.PaymentMethodType.CreditCard){
					string value1, value2;
					value.GetDecryptedData(out value1, out value2);
					parsed = Bll.PaymentMethods.CreditCard.ExtractData(value1);
				}
                if (SingleFieldMode) {
                    HtmlInputText currentInput = (HtmlInputText)FindControl("F_1");
                    currentInput.Value = string.Join(" ", parsed);
                }
                else { 
				    for (int i = 1; i <= 4; i++)
				    {
					    HtmlInputText currentInput = (HtmlInputText)FindControl("F_" + i.ToString());
					    currentInput.Value = parsed.Length >= i ? parsed[i - 1] : "";
				    }
                }
            }
		}
	}
}
