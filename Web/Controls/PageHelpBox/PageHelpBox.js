﻿if (netpay == null){var netpay = new Object()}
if (netpay.webControls == null){netpay.webControls = new Object()}

netpay.webControls.PageHelpBox =
{
	toggleHelpBox: function()
	{
		var contentElement = document.getElementById("divPageHelpBoxContent");
		var arrowImageElement = document.getElementById("pageHelpBoxArrowImage");
		if (contentElement.style.display == "none") 
		{
			netpay.Common.createCookie("pageHelpBoxMinimized", "false", 3600, window.location.pathname);
			arrowImageElement.src = pageHelpBoxUpArrowUrl;
			$("#divPageHelpBoxContent").show();
		}
		else 
		{
			netpay.Common.createCookie("pageHelpBoxMinimized", "true", 3600, window.location.pathname);
			arrowImageElement.src = pageHelpBoxDownArrowUrl;
			$("#divPageHelpBoxContent").hide();
		}	
	}
}