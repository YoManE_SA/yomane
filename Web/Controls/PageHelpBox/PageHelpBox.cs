﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:PageHelpBox runat=server></{0}:PageHelpBox>")]
	[ParseChildren(true, "Text")]
	[PersistChildren(false)]
	public class PageHelpBox : CompositeControl
	{
		private string _text = null;

		public string Text
		{
			get { return _text; }
			set { _text = value; }
		}

		public static bool EnabledGlobally
		{
			get
			{
				bool isEnabled = true;
				HttpCookie cookie = HttpContext.Current.Request.Cookies.Get("pageHelpBoxEnabled");
				if (cookie != null) isEnabled = bool.Parse(cookie.Value);
				return isEnabled;
			}
			set
			{
				HttpCookie cookie = new HttpCookie("pageHelpBoxEnabled", value.ToString());
				cookie.Expires = DateTime.Now.AddYears(10);
				cookie.Path = "/";
				HttpContext.Current.Response.Cookies.Add(cookie);
			}
		}

		public bool Minimized
		{
			get
			{
				bool isEnabled = false;
				HttpCookie cookie = HttpContext.Current.Request.Cookies.Get("pageHelpBoxMinimized");
				if (cookie != null)
					isEnabled = bool.Parse(cookie.Value);

				return isEnabled;
			}
			set
			{
				HttpCookie cookie = new HttpCookie("pageHelpBoxMinimized", value.ToString());
				HttpContext.Current.Response.Cookies.Add(cookie);
			}
		}

		protected override void CreateChildControls()
		{
			base.CreateChildControls();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			// embed scripts
			this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.PageHelpBox.PageHelpBox.js");

			// embed css
			string includeTemplate = "<link rel='stylesheet' text='text/css' href='{0}' />";
			string includeLocation = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.PageHelpBox.PageHelpBox.css");
			LiteralControl include = new LiteralControl(String.Format(includeTemplate, includeLocation));
			Page.Header.Controls.Add(include);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (Infrastructure.Application.IsProduction)
				return;
			
			if (!EnabledGlobally)
				return;

			// setup
			string upArrow = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.PageHelpBox.ArrowUp.png");
			string downArrow = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Netpay.Web.Controls.PageHelpBox.ArrowDown.png");
			string currentArrow = Minimized ? downArrow : upArrow;
			string displayStyle = Minimized ? "none" : "block";

			// output
			writer.Write("<script>");
			writer.Write(string.Format("var pageHelpBoxUpArrowUrl = '{0}';", upArrow));
			writer.Write(string.Format("var pageHelpBoxDownArrowUrl = '{0}';", downArrow)); 
			writer.Write("</script>");
			
			writer.Write("<div class=\"pageHelpBoxContainer\">");
			writer.Write(string.Format("<div id=\"divPageHelpBoxContent\" class=\"pageHelpBoxContent\" style=\"display:{0}; text-align:{1};\">", displayStyle, WebUtils.CurrentAlignment));
			if (_text != null)
				writer.Write(_text);
			writer.Write("</div></div>");

			writer.Write("<div class=\"pageHelpBoxMinimizeContainer\"><center>");
			writer.Write("<div class=\"pageHelpBoxMinimize\" onclick=\"netpay.webControls.PageHelpBox.toggleHelpBox()\" onmouseover=\"this.className='pageHelpBoxMinimizeHover'\" onmouseout=\"this.className='pageHelpBoxMinimize'\" onmousedown=\"this.className='pageHelpBoxMinimizeClick'\" onmouseup=\"this.className='pageHelpBoxMinimizeHover'\">");
			writer.Write(string.Format("<img id=\"pageHelpBoxArrowImage\" src=\"{0}\">", currentArrow));
			writer.Write("</div>");
			writer.Write("</center></div>");
		}
	}
}
