﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Web.UI;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	public class PeriodicFeeTypeDropDown : DropDownBase
	{
        public Bll.Accounts.AccountType? AccountTypeToShowInList { get; set; }


		protected override void DataBind(bool raiseOnDataBinding)
		{
			var sf = new Bll.PeriodicFees.PeriodicFeeType.SearchFilters();
			sf.isActive = true;

            if (AccountTypeToShowInList != null) sf.AccountTypeToSearch = AccountTypeToShowInList;

            var list = Bll.PeriodicFees.PeriodicFeeType.Search(sf, null);
            DataSource = list;
            DataValueField = "ID";
            DataTextField = "Name";

            if (list.Count == 1)
            {
                base.DataBind(false);
            }
            else
            {
                base.DataBind(raiseOnDataBinding);
            }
        }
	}
}
