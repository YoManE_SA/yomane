﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
	public class TagsInput : System.Web.UI.WebControls.TextBox 
	{

		protected override void OnPreRender(EventArgs e)
		{
            if (!Page.ClientScript.IsClientScriptIncludeRegistered("Plugins/tags/jquery.tagsinput.js"))
                Page.ClientScript.RegisterClientScriptInclude("Plugins/tags/jquery.tagsinput.js", "Plugins/tags/jquery.tagsinput.js");
			System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "TagsLoad_" + ClientID, "$(function () { $('#" + this.ClientID + "').tagsInput({ width: 'auto' }); });", true);
			base.OnPreRender(e);
		}
	}
}
