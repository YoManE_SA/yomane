﻿using System;
using System.Web.UI;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:TransactionHistoryDropDown runat=server></{0}:TransactionStatusDropDown>")]
	public class TransactionHistoryDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			String[] names = CommonTypes.TransactionHistoryType.GetNames(typeof(CommonTypes.TransactionHistoryType));
            CommonTypes.TransactionHistoryType[] values = (CommonTypes.TransactionHistoryType[])Enum.GetValues(typeof(CommonTypes.TransactionHistoryType));
			var lst = new System.Collections.Generic.Dictionary<int, string>();
			for (int i = 1; i < names.Length; i++)
				lst.Add((int)values[i], names[i].ToString());
            DataSource = lst;
			DataValueField = "Key";
			DataTextField = "Value";
			base.DataBind(raiseOnDataBinding);
		}
	}
}
