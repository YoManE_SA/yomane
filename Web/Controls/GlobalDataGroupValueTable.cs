﻿using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	public class GlobalDataGroupValueTable : WebControl
	{
		private GlobalDataGroup gdgGroup = GlobalDataGroup.Group;
		private Language lng = Language.English;
		private int digits = 2;

		public GlobalDataGroup GlobalDataGroup
		{
			get { return gdgGroup; }
			set { gdgGroup = value; }
		}

		public Language DisplayLanguage
		{
			get { return lng; }
			set { lng = value; }
		}

		public int MinimumDigits
		{
			get { return digits; }
			set { digits = value; }
		}

		protected override void Render(HtmlTextWriter writer)
		{
			var values = GlobalData.GetGroup(gdgGroup, lng);
			writer.Write("<table border=\"0\" cellspacing=\"4\" cellpadding=\"2\">");
			foreach (var value in values)
			{
				writer.Write("<tr>");
				writer.Write(string.Format("<td style=\"text-align:right;\">{0:" + "".PadLeft(digits, '0') + "}</td>", value.ID));
				writer.Write("<td>=</td>");
				writer.Write("<td>" + value.Value + "</td>");
				writer.Write("</tr>");
			}
			writer.Write("</table>");
		}
	}
}