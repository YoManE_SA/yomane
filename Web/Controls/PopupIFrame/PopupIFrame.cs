﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:PopupIFrame runat=server></{0}:PopupIFrame>")]
	public class PopupIFrame : WebControl
	{
		private string _src = null;
		private string _text = null;

		public string Text
		{
			get { return _text; }
			set { _text = value; }
		}

		public string Src
		{
			get { return _src; }
			set { _src = value; }
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			// embed scripts
			this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.PopupIFrame.PopupIFrame.js");
		}

		
		public override void RenderControl(HtmlTextWriter writer)
		{
			//anchorElementID, containerElementID, iFrameElementID, iFrameSource
			writer.Write(string.Format("<a id=\"{0}Anchor\" href=\"javascript:netpay.webControls.PopupIFrame.open('{0}Anchor', '{0}Content', '{0}Frame', '{1}')\">{2}</a>", this.ClientID, Src, Text));
			writer.Write(string.Format("<div id=\"{0}Content\" style=\"position:absolute; display:none; width:{1}; height:{2}; border:1px solid; padding:5px;\">", this.ClientID, Width, Height));
			writer.Write(string.Format("<div style=\"background-color:white; cursor:pointer; text-align:right;\" onclick=\"javascript:netpay.webControls.PopupIFrame.close('{0}Content')\">X</div>", this.ClientID));
			writer.Write(string.Format("<iframe id=\"{0}Frame\" style=\"border:0 none; width:100%;\">Loading...</iframe>", this.ClientID));
			writer.Write("</div>");
		}
	}
}
