﻿if (netpay == null) { var netpay = new Object() }
if (netpay.webControls == null) { netpay.webControls = new Object() }

netpay.webControls.PopupIFrame =
{
	open: function(anchorElementID, containerElementID, iFrameElementID, iFrameSource)
	{
		var anchorElement = document.getElementById(anchorElementID);
		var containerElement = document.getElementById(containerElementID);
		var iFrameElement = document.getElementById(iFrameElementID);
		var position = netpay.Common.findPosition(anchorElement);
		containerElement.style.top = position.top + 20;
		containerElement.style.left = position.left;
		$("#" + containerElementID).fadeIn("slow");
		iFrameElement.src = iFrameSource;
	},

	close: function(containerElementID)
	{
		$("#" + containerElementID).fadeOut("slow");
	}
}