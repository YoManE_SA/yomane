﻿if (netpay == null) { var netpay = new Object() }
if (netpay.webControls == null) { netpay.webControls = new Object() }

netpay.webControls.NumberRangeSlider =
{
	init: function ()
	{
		var minLimit = Number($("#numberRangeSliderMinLimit").val());
		var maxLimit = Number($("#numberRangeSliderMaxLimit").val());
		var minValue = Number($("#numberRangeSliderMinValue").val());
		var maxValue = Number($("#numberRangeSliderMaxValue").val());

		$( "#numberRangeSliderElement" ).slider
		({
			range: true,
			min: minLimit,
			max: maxLimit,
			values: [ minValue, maxValue ],
			slide: function( event, ui ) 
			{		
				var minValue = $("#numberRangeSliderMinValue").val(ui.values[ 0 ]);
				var maxValue = $("#numberRangeSliderMaxValue").val(ui.values[ 1 ]);
			}
		});
	},

	validate: function ()
	{
		var minLimit = Number($("#numberRangeSliderMinLimit").val());
		var maxLimit = Number($("#numberRangeSliderMaxLimit").val());
		var minValue = Number($("#numberRangeSliderMinValue").val());
		var maxValue = Number($("#numberRangeSliderMaxValue").val());

		if (minValue < minLimit || maxValue > maxLimit)
		{
			netpay.Common.alert($("#numberRangeSliderOutOfRangeError").val());
			return false;
		}

		return true;
	}
}

$(netpay.webControls.NumberRangeSlider.init);