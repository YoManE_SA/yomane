﻿using System;
using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Netpay.Infrastructure;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:NumberRangeSlider runat=server></{0}:NumberRangeSlider>")]
	public class NumberRangeSlider : WebControl
	{
		private int _minLimit = 0;
		private int _maxLimit = 100;
		private int? _minValue = null;
		private int? _maxValue = null;
		private string _outOfRangeError = null;
		private bool _showSlider = true;

		public bool ShowSlider
		{
			get { return _showSlider; }
			set { _showSlider = value; }
		}

		public string OutOfRangeError
		{
			get { return _outOfRangeError; }
			set { _outOfRangeError = value; }
		}

		public int MaxLimit
		{
			get
			{
				string posted = Context.Request["numberRangeSliderMaxLimit"];
				if (posted != null)
					_maxLimit = int.Parse(posted);

				return _maxLimit;
			}
			set { _maxLimit = value; }
		}
		 
		public int MinLimit
		{
			get 
			{
				string posted = Context.Request["numberRangeSliderMinLimit"];
				if (posted != null)
					_minLimit = int.Parse(posted);
					
				return _minLimit; 
			}
			set { _minLimit = value; }
		}

		public int? MaxValue
		{
			get
			{
				string posted = Context.Request["numberRangeSliderMaxValue"];
				if (posted != null && posted.Trim() != "")
					_maxValue = posted.ToInt32(MaxLimit);

				return _maxValue;
			}
			set { _maxValue = value; }
		}

		public int? MinValue
		{
			get
			{
				string posted = Context.Request["numberRangeSliderMinValue"];
				if (posted != null && posted.Trim() != "")
					_minValue = posted.ToInt32(MinLimit);

				return _minValue;
			}
			set { _minValue = value; }
		}

		public bool IsMinValueSelected
		{
			get
			{
				return MinValue != null;
			}
		}

		public bool IsMaxValueSelected
		{
			get
			{
				return MaxValue != null;
			}
		}

		public bool IsOutOfRange
		{
			get
			{
				if (IsMinValueSelected && MinValue < MinLimit)
					return true;
				if (IsMaxValueSelected && MaxValue > MaxLimit)
					return true;

				return false;
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnPreRender(e);

			// embed scripts
			this.Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Netpay.Web.Controls.NumberRangeSlider.NumberRangeSlider.js");
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			ResourceManager rm = WebUtils.GetResource("Common");

			writer.Write("<table cellpadding=\"0\" cellspacing=\"" + (ShowSlider ? "5" : "0") + "\">");
			if (ShowSlider)
			{
				writer.Write("<tr>");
				writer.Write("<td colspan=\"2\">");
				writer.Write("<div id=\"numberRangeSliderElement\" name=\"numberRangeSliderElement\"></div>");
				writer.Write("</td>");
				writer.Write("</tr>"); 
			}
			writer.Write("<tr>");
			writer.Write("<td>");
			writer.Write(string.Format("<input type=\"text\" id=\"numberRangeSliderMinValue\" style=\"Width:50px;\" name=\"numberRangeSliderMinValue\" value=\"{0}\">", MinValue));
			writer.Write("&nbsp;");
			writer.Write(rm.GetString("To"));
			writer.Write("&nbsp;");
			writer.Write(string.Format("<input type=\"text\" id=\"numberRangeSliderMaxValue\" style=\"Width:50px;\" name=\"numberRangeSliderMaxValue\" value=\"{0}\">", MaxValue));
			writer.Write("</td>");
			writer.Write("</tr>");
			writer.Write("</table>");

			writer.Write(string.Format("<input type=\"hidden\" id=\"numberRangeSliderMinLimit\" name=\"numberRangeSliderMinLimit\" value=\"{0}\">", MinLimit));
			writer.Write(string.Format("<input type=\"hidden\" id=\"numberRangeSliderMaxLimit\" name=\"numberRangeSliderMaxLimit\" value=\"{0}\">", MaxLimit));
			writer.Write(string.Format("<input type=\"hidden\" id=\"numberRangeSliderOutOfRangeError\" name=\"numberRangeSliderOutOfRangeError\" value=\"{0}\">", _outOfRangeError));	
		}
	}
}
