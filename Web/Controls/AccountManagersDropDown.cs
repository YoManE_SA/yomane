﻿using System;
using System.Web.UI;
using System.Linq;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:AccountManagersDropDown runat=server></{0}:AccountManagersDropDown>")]
	public class AccountManagersDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			DataSource = Infrastructure.Security.AdminUser.Cache.Where(x => x.IsActive.HasValue && x.IsActive.Value).OrderBy(x => x.FullName);
			DataValueField = "ID";
			DataTextField = "FullName";
			base.DataBind(raiseOnDataBinding);
		}
	}
}



