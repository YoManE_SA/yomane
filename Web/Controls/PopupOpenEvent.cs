﻿namespace Netpay.Web.Controls
{
    public enum PopupOpenEvent
    {
        MouseOver = 1,
        MouseClick = 2
    }
}
