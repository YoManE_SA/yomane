﻿using System;
using System.Linq;
using System.Web.UI;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:MerchantsDropDown runat=server></{0}:MerchantsDropDown>")]
	public class MerchantsDropDown : DropDownBase
	{
		protected override void DataBind(bool raiseOnDataBinding)
		{
			var items = Bll.Merchants.Merchant.CachedNamesForDomain().ToList();
            if (Bll.Accounts.AccountFilter.Current.ObjectIDs.ContainsKey(Bll.Accounts.AccountType.Merchant) && Bll.Accounts.AccountFilter.Current.ObjectIDs[Bll.Accounts.AccountType.Merchant].Count > 0)
				items = items.Where(i => Bll.Accounts.AccountFilter.Current.ObjectIDs[Bll.Accounts.AccountType.Merchant].Contains(i.Key)).ToList();
			DataSource = items;
			DataValueField = "Key";
			DataTextField = "Value";
			base.DataBind(raiseOnDataBinding);
		}
	}
}
