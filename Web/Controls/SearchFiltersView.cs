﻿using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Bll;
using System.Resources;

namespace Netpay.Web.Controls
{
	public class SearchFiltersView : WebControl
	{
		private Dictionary<string, string> _filters = new Dictionary<string,string>();

		public void LoadFilters(object filters)
        {
			Add(filters, WebUtils.GetResource("Common"));
			/*
            ResourceManager rm = WebUtils.GetResource("Common");
			if (filters.paymentMethodID != null) Add("Payment Method", Bll.PaymentMethods.PaymentMethod.Get(filters.paymentMethodID.GetValueOrDefault(0)).Name);
			if (filters.transactionID != null) Add("Transaction Num.", filters.transactionID.ToString());
			if (filters.dateFrom != null) Add("From Date", filters.dateFrom.ToString());
			if (filters.dateTo != null) Add("To Date", filters.dateTo.ToString());
			if (filters.cardHolderName != null) Add("Cardhoder Name", filters.cardHolderName);
			if (filters.creditType != null) Add("Credit Type", GlobalData.GetValue(GlobalDataGroup.CreditType, WebUtils.CurrentLanguage, filters.creditType.GetValueOrDefault(0)).Value);
			if (filters.cardHolderEmail != null) Add("Cardholder Email", filters.cardHolderEmail);
			if (filters.phoneNumber != null) Add("Cardholder Phone", filters.phoneNumber);
			if (filters.last4Digits != null) Add("Last 4 digits of CC number", filters.last4Digits.GetValueOrDefault().ToString("0000"));
			if (filters.currencyID != null) Add(rm.GetString("Currency"), Currency.Get(filters.currencyID.GetValueOrDefault(0)).IsoCode);
			if (filters.paymentsStatus != null) Add("Payout Status", filters.paymentsStatus);
			if (filters.isChargeback != null && filters.isChargeback.GetValueOrDefault(false)) Add("Chargeback Only", "");
			if (filters.seriesID != null) Add("Series", filters.seriesID.ToString());
			if (filters.reportAmount != null) Add(rm.GetString("Amount"), filters.reportAmount.ToString());
			if (filters.personalNumber != null) Add("Payer", filters.personalNumber);
			if (filters.orderID != null) Add("Order Number", filters.orderID);
			if (filters.replyCode != null) Add("Reply Code", filters.replyCode);
			if (filters.declinedType != null) Add("Declined Type", GlobalData.GetValue(GlobalDataGroup.DeniedStatus, WebUtils.CurrentLanguage, filters.declinedType.GetValueOrDefault(0)).Value);
			*/
		}

		public void LoadFilters(Bll.Transactions.Transaction.SearchFilters filters)
		{
			ResourceManager rm = WebUtils.GetResource("Common");

			if (filters.paymentMethodID != null) Add("Payment Method", Bll.PaymentMethods.PaymentMethod.Get(filters.paymentMethodID.GetValueOrDefault(0)).Name);
			if (filters.ID.From != null) Add("From Transaction Num.", filters.ID.From.ToString());
			if (filters.ID.To != null) Add("To Transaction Num.", filters.ID.To.ToString());
			if (filters.date.From != null) Add("From Date", filters.date.From.ToString());
			if (filters.date.To != null) Add("To Date", filters.date.To.ToString());

			if (filters.creditTypes != null) Add("Credit Type", string.Join(", ", filters.creditTypes.Select( i => GlobalData.GetValue(GlobalDataGroup.CreditType, WebUtils.CurrentLanguage, i).Value)));
			if (filters.currencyID != null) Add(rm.GetString("Currency"), Currency.Get(filters.currencyID.GetValueOrDefault(0)).IsoCode);
			//if (filters.paymentsStatus != null) Add("Payout Status", filters.paymentsStatus);
			if (filters.isChargeback != null && filters.isChargeback.GetValueOrDefault(false)) Add("Chargeback Only", "");
			//if (filters.seriesID != null) Add("Series", filters.seriesID.ToString());
			//if (filters.reportAmount != null) Add(rm.GetString("Amount"), filters.reportAmount.ToString());
			if (filters.orderID != null) Add("Order Number", filters.orderID);
			if (filters.replyCode != null) Add("Reply Code", filters.replyCode);
			if (filters.declinedType != null) Add("Declined Type", GlobalData.GetValue(GlobalDataGroup.DeniedStatus, WebUtils.CurrentLanguage, filters.declinedType.GetValueOrDefault(0)).Value);

			if(filters.PayerFilters != null){
				if (filters.PayerFilters.FirstName != null) Add("Payer First Name", filters.PayerFilters.FirstName);
				if (filters.PayerFilters.LastName != null) Add("Payer Last Name", filters.PayerFilters.LastName);
				if (filters.PayerFilters.EmailAddress != null) Add("Cardholder Email", filters.PayerFilters.EmailAddress);
				if (filters.PayerFilters.PhoneNumber != null) Add("Cardholder Phone", filters.PayerFilters.PhoneNumber);
				if (filters.PayerFilters.PersonalNumber != null) Add("Payer", filters.PayerFilters.PersonalNumber);
			}
			if (filters.PaymentFilters != null){
				if (filters.PaymentFilters.Last4Digits != null) Add("Last 4 digits of CC number", filters.PaymentFilters.Last4Digits);
			}

		}

		public void Add(string filterName, string filterValue)
		{
			_filters.Add(filterName, filterValue);
		}

		public void Remove(string filterName)
		{
			_filters.Remove(filterName);
		}

		public void Add(object filters, ResourceManager rm = null)
		{
			FieldInfo[] fields = filters.GetType().GetFields();
			foreach (FieldInfo currentField in fields)
			{
				string name = null;
				if (rm != null) name = rm.GetString(currentField.Name);
				if (string.IsNullOrEmpty(name)) name = currentField.Name.ToSentence();
				object value = currentField.GetValue(filters);
				if (value != null)
					_filters.Add(name, value.ToString());
			}
		}
		public string RenderText()
		{
			if (_filters.Count == 0)
				return "";
            ResourceManager rm = WebUtils.GetResource("Common");
			StringBuilder sb = new StringBuilder();
            sb.Append(rm.GetString("Filterby"));
			foreach (string currentKey in _filters.Keys)
                sb.Append(string.Format(" {0}: {1}, ", currentKey, _filters[currentKey]));
			sb.Remove(sb.Length - 7, 7);
						
			return sb.ToString();
		}

		public string RenderHtml()
		{
			if (_filters.Count == 0)
				return "";
            ResourceManager rm = WebUtils.GetResource("Common");
			StringBuilder sb = new StringBuilder();
            sb.Append("<span class=\"fvHead\">" + rm.GetString("Filterby") + "<span> ");
			foreach (string currentKey in _filters.Keys)
                sb.Append(string.Format(" <span class=\"dark-grey\">{0}<span>: <span class=\"dark-grey\">{1}<span>,&nbsp;", currentKey, _filters[currentKey]));
			sb.Remove(sb.Length - 7, 7);
						
			return sb.ToString();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (_filters.Count == 0) return;
            ResourceManager rm = WebUtils.GetResource("Common");
			StringBuilder sb = new StringBuilder();
			foreach (string currentKey in _filters.Keys)
                sb.Append(string.Format("<span class=\"dark-grey\">{0}<span>: <span class=\"dark-grey\">{1}<span>,&nbsp;", currentKey, _filters[currentKey]));
			sb.Remove(sb.Length - 7, 7);

            writer.Write("<div><span class=\"dark-grey\">" + rm.GetString("Filterby") + "<span> &nbsp;");
			writer.Write(sb.ToString());
			writer.Write("</div>");
		}
	}
}
