﻿using System;
using System.Resources;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web.Controls
{
	[ToolboxData("<{0}:PaymentMethodView runat=server></{0}:PaymentMethodView>")]
	public class PaymentMethodView : WebControl
	{
        public bool HideFlag { get; set; }
		private string _binCountryCode = null;
		private int? _paymentMethodID = 0;
		private string _paymentMethodDisplay = null;
		private string _cardLast4Digits = null;
        private Bll.PaymentMethods.MethodData _methodData;

        public string MethodImagePath { get; set; }
        public string FlagsImagePath { get; set; }

        public PaymentMethodView() {
            MethodImagePath = "/NPCommon/ImgPaymentMethod/23X12/{0}.gif";
            FlagsImagePath = "/NPCommon/ImgCountry/18X12/{0}.gif";
        }


		public string CardLast4Digits
		{
			get { return _cardLast4Digits; }
			set { _cardLast4Digits = value; }
		}

        public string CardNumber
        {
            set { if (value != null && value.Length > 4) _cardLast4Digits = value.Substring(value.Length - 4); else _cardLast4Digits = value; }
        }

		public string PaymentMethodDisplay
		{
			get { return _paymentMethodDisplay; }
			set { _paymentMethodDisplay = value; }
		}

		public string BinCountryCode
		{
			get { return _binCountryCode; }
			set { _binCountryCode = value; }
		}

		public int? PaymentMethodID
		{
			get { return _paymentMethodID; }
			set { _paymentMethodID = value; }
		}

		public Bll.PaymentMethods.IStoredMethod StoredData
		{
			set
			{
				if (value == null) { MethodData = null; return; }
				MethodData = value.MethodInstance;
			}
		}

		public Bll.PaymentMethods.MethodData MethodData
		{
			set {
                _methodData = value;

                if (value == null) {
					PaymentMethodID = null;
					BinCountryCode  = null;
					CardLast4Digits = null;
					return;
				}
				PaymentMethodID = (int) value.PaymentMethodId;
				//BinCountryCode = value.iss '<%# Eval("IssuerCountryIsoCode") %>' 
				CardLast4Digits = value.Value1Last4;
			}
		} 

		protected override void RenderContents(HtmlTextWriter output)
		{
			StringBuilder paymentMethodBuilder = new StringBuilder();
			if (PaymentMethodID >= (byte)PaymentMethodEnum.Admin)
			{
				    var paymentMethod = Bll.PaymentMethods.PaymentMethod.Get(PaymentMethodID.Value);
                    paymentMethodBuilder.Append(String.Format("<img src='" + MethodImagePath + "' alt='{1}' style='border: 1px solid #ccc; vertical-align: middle' /> &nbsp;", PaymentMethodID, paymentMethod == null ? "" : paymentMethod.Name));
                    if (!HideFlag)
                    {
                        if (BinCountryCode != null && BinCountryCode.Trim() != "" && PaymentMethodID != (byte)PaymentMethodEnum.RollingReserve && PaymentMethodID > (byte)PaymentMethodEnum.Admin && PaymentMethodID != (byte)PaymentMethodEnum.ECCheck && PaymentMethodID != (byte)PaymentMethodEnum.SystemFees)
					    paymentMethodBuilder.Append(string.Format("<img src='" + FlagsImagePath + "' alt='{1}' style='border: 1px solid #ccc; vertical-align: middle;' /> &nbsp;", BinCountryCode, WebUtils.GetCountryName(BinCountryCode)));
                }
			}

			if (PaymentMethodDisplay == null || PaymentMethodDisplay == "")
			{
				string name = "";
				if (PaymentMethodID != null)
				{
					var paymentMethod = Bll.PaymentMethods.PaymentMethod.Get(PaymentMethodID.Value);
					if (paymentMethod != null)
					{
						ResourceManager rm = WebUtils.GetResource("PaymentMethods");
						name = rm.GetString(paymentMethod.Name);
					}
				}

				paymentMethodBuilder.Append(name);
                if (CardLast4Digits != null && CardLast4Digits.Trim() != "")
                {
                    paymentMethodBuilder.Append(" .... ");
                    paymentMethodBuilder.Append(CardLast4Digits);
                }
                else if (_methodData != null)
                {
                    // in rare cases where missing last 4
                    string ccnum = Netpay.Infrastructure.Security.Encryption.Decrypt(_methodData.GetEncyptedValue1());
                    if (ccnum != null && ccnum.Length > 4)
                    {
                        string last4 = ccnum.Substring(ccnum.Length - 4);
                        paymentMethodBuilder.Append(" .... ");
                        paymentMethodBuilder.Append(last4);
                    }
                }
			}
			else
				paymentMethodBuilder.Append(PaymentMethodDisplay);

			output.Write(paymentMethodBuilder.ToString());
		}
	}
}
