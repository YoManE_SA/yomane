﻿using System;
using System.Resources;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Web.Controls
{
    public class EnumMultiSelect : System.Web.UI.WebControls.CheckBoxList
    {
        private string _ViewEnumName;
        public string ViewEnumName { get { return _ViewEnumName; } set { _ViewEnumName = value; if (Page != null && !Page.IsPostBack) DataBind(); } }

        public string ExcludeValue { get; set; }

        /// <summary>
        /// Return list of selected checkboxes' enum string value.
        /// The returned strings can be converted to the actual enums by using Enum.TryParse(value, out enumVariable)
        /// </summary>
        public List<string> SelectedItems
        {
            get
            {
                var selected = Items.Cast<System.Web.UI.WebControls.ListItem>()
                                            .Where(li => li.Selected)
                                            .Select(i => EnumHelper.EnumDisplayTextToEnumName(i.Text))
                                            .ToList();

                return selected;
            }
        }

        public List<T> SelectedToEnumValues<T>() where T : struct, IConvertible
        {
            var selectedStr = this.SelectedItems;
            List<T> selectedTypes =
                selectedStr
                .Select(i => Netpay.Web.Controls.EnumHelper.EnumFromString<T>(i))
                .Where(i => i != null)
                .Select(i => (T)i)
                .ToList();

            return selectedTypes;
        }


        public override void DataBind()
        {
            var type = System.Web.Compilation.BuildManager.GetType(_ViewEnumName, false);
            if (type == null) throw new Exception("type could not be found:" + _ViewEnumName + ", ClientID:" + ClientID);
            _ViewEnumName = EnumHelper.GetViewEnumName(type);

            var items = EnumHelper.GetEnumList(type, this.ExcludeValue);

            if (items != null && items.Count > 0)
            {
                // Add checkboxes list
                DataSource = items;
                DataValueField = "Key";
                DataTextField = "Value";
            }

            base.DataBind();
        }
    }
}
