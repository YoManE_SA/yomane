﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.SessionState;
using Netpay.Infrastructure;
using System.Drawing;

namespace Netpay.Web
{
	/// <summary>
	/// Generic handler for ajax calls
    /// Register in site to use ajax methods
	/// </summary>
    public class PlaceHolderHandler : IHttpHandler, IRequiresSessionState
	{
		#region IHttpHandler Members

		public bool IsReusable
		{
			get { return true; }
		}

		public void ProcessRequest(HttpContext context)
		{
            int width = context.Request.Params["width"].ToInt32();
            int height = context.Request.Params["height"].ToInt32();

            Bitmap bitmap = new Bitmap(width, height);
            Graphics graphics = Graphics.FromImage(bitmap);
            Rectangle rect = new Rectangle(0, 0, width, height);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            
            // bg
            SolidBrush bgBrush = new SolidBrush(Color.FromArgb(214, 214, 214));
            graphics.FillRectangle(bgBrush, rect);

            // text
            int textSize = width / 12;
            SolidBrush textBrush = new SolidBrush(Color.FromArgb(150, 150, 150));
            Font font = new Font("Arial", textSize, GraphicsUnit.Pixel);
            string text = string.Format("{0} x {1}", width, height);
            graphics.DrawString(text, font, textBrush, rect, stringFormat);

            byte[] buffer = Netpay.Infrastructure.ImageUtils.Compress(bitmap, System.Drawing.Imaging.ImageFormat.Png, 80);
            graphics.Dispose();
            context.Response.ContentType = "image/png";
            context.Response.BinaryWrite(buffer);
            context.Response.Flush();
		}

		#endregion
	}
}
