﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Web;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;
using System.Web.UI;
using System.Web.Handlers;
using System.IO;
using System.Collections;
using System.Linq;
using System.Net;

namespace Netpay.Web
{
    public static class WebUtils
    {
        private static object _syncRoot = new object();
        private static Dictionary<string, ResourceManager> _resourceManagers = new Dictionary<string, ResourceManager>();
        private static Dictionary<string, string> _resourceContents = new Dictionary<string, string>();
        public static bool SessionKeyNotUsePath { get; set; }

        /// <summary>
        /// Exports resource values as json.
        /// Mainly for clients that need to support multiple languages.
        /// </summary>
        /// <param name="resourceName"></param>
        /// <param name="keys"></param>
        /// <returns>Json key - value collection</returns>
        public static string ExportResource(string resourceName, List<string> keys)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            ResourceManager resource = GetResource(resourceName);
            foreach (string currentKey in keys)
            {
                string currentValue = resource.GetString(currentKey);
                result.Add(currentKey, currentValue);
            }

            return string.Format("if(typeof(resources) == 'undefined'){{resources = {{}}}}; resources.{0} = {1}", resourceName, result.ToJson());
        }

        /// <summary>
        /// Exports resource values as json.
        /// Mainly for clients that need to support multiple languages.
        /// </summary>
        /// <param name="resourceName"></param>
        /// <param name="keys"></param>
        /// <returns>Json key - value collection</returns>
        public static string ExportResource(string resourceName)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            ResourceManager resource = GetResource(resourceName);

            ResourceSet set = resource.GetResourceSet(CurrentCulture, true, true);
            foreach (DictionaryEntry currentEntry in set)
                result.Add(currentEntry.Key.ToString(), currentEntry.Value.ToString());

            return string.Format("if(typeof(resources) == 'undefined'){{resources = {{}}}}; resources.{0} = {1}", resourceName, result.ToJson());
        }

        public static string ExportResource(string resourceName, Assembly assembly)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            ResourceManager resource = new ResourceManager(resourceName, assembly);

            ResourceSet set = resource.GetResourceSet(CurrentCulture, true, true);
            foreach (DictionaryEntry currentEntry in set)
                result.Add(currentEntry.Key.ToString(), currentEntry.Value.ToString());

            string resourceClassName = resourceName.Contains(".") ? resourceName.Split('.').Last() : resourceName;
            return string.Format("if(typeof(resources) == 'undefined'){{resources = {{}}}}; resources.{0} = {1}", resourceClassName, result.ToJson());
        }

        public static string GetEmbeddedResource(string resourcePath)
        {
            Stream stream = null;
            if (!_resourceContents.ContainsKey(resourcePath))
            {
                lock (_resourceContents)
                {
                    if (!_resourceContents.ContainsKey(resourcePath))
                    {
                        try
                        {
                            Assembly assembly = Assembly.GetAssembly(typeof(WebUtils));
                            stream = assembly.GetManifestResourceStream(resourcePath);
                            _resourceContents[resourcePath] = new StreamReader(stream).ReadToEnd();
                        }
                        finally
                        {
                            if (stream != null)
                                stream.Close();
                        }
                    }
                }
            }

            return _resourceContents[resourcePath];
        }

        /// <summary>
        /// Gets translated name
        /// </summary>
        /// <param name="countryIsoCode2"></param>
        /// <returns></returns>
        public static string GetCountryName(int? countryID)
        {
            if (countryID == null)
                return "";

            return GetCountryName(countryID.Value);
        }

        /// <summary>
        /// Gets translated name
        /// </summary>
        /// <param name="countryIsoCode2"></param>
        /// <returns></returns>
        public static string GetStateName(int? stateID)
        {
            if (stateID == null)
                return "";

            return GetStateName(stateID.Value);
        }

        /// <summary>
        /// Gets translated name
        /// </summary>
        /// <param name="countryIsoCode2"></param>
        /// <returns></returns>
        public static string GetCountryName(int countryID)
        {
            var vo = Bll.Country.Get(countryID);
            return GetCountryName(vo.IsoCode2);
        }

        /// <summary>
        /// Gets translated name
        /// </summary>
        /// <param name="countryIsoCode2"></param>
        /// <returns></returns>
        public static string GetStateName(int stateID)
        {
            var vo = Bll.State.Get(stateID);
            return GetStateName(vo.IsoCode);
        }

        /// <summary>
        /// Gets translated name
        /// </summary>
        /// <param name="countryIsoCode2"></param>
        /// <returns></returns>
        public static string GetCountryName(string countryIsoCode2)
        {
            if (string.IsNullOrEmpty(countryIsoCode2)) return string.Empty;
            string name = Web.WebUtils.GetResourceValue("Countries", countryIsoCode2);
            return name;
        }

        /// <summary>
        /// Gets translated name
        /// </summary>
        /// <param name="countryIsoCode2"></param>
        /// <returns></returns>
        public static string GetStateName(string stateIsoCode2)
        {
            if (string.IsNullOrEmpty(stateIsoCode2)) return string.Empty;
            string name = Web.WebUtils.GetResourceValue("States", stateIsoCode2);
            return name;
        }

        public static ResourceManager GetResource(string resourceName)
        {
            if (resourceName == null || resourceName.Trim() == "")
                throw new ArgumentException("resourceName cannot be null or empty.");

            if (_resourceManagers == null)
                throw new ApplicationException("_resourceManagers is null.");

            if (!_resourceManagers.ContainsKey(resourceName))
            {
                lock (_resourceManagers)
                {
                    if (!_resourceManagers.ContainsKey(resourceName))
                    {
                        ResourceManager resourceManager = new ResourceManager("Netpay.Web.Resources." + resourceName, Assembly.GetExecutingAssembly());
                        if (resourceManager == null)
                            throw new ApplicationException("unable to create resource: Netpay.Web.Resources." + resourceName);

                        _resourceManagers.Add(resourceName, resourceManager);
                    }
                }
            }

            return _resourceManagers[resourceName];
        }

        public static string GetResourceValue(string resourceName, string resourceKey)
        {
            ResourceManager rm = GetResource(resourceName);
            return rm.GetString(resourceKey);
        }

        public static string GetResourceValue(string resourceName, string resourceKey, CultureInfo culture)
        {
            ResourceManager rm = GetResource(resourceName);
            if (culture == null)
                culture = CultureInfo.CurrentCulture;
            string value = rm.GetString(resourceKey, culture);
            if (value == null)
                value = resourceKey;

            return value;
        }

        public static string MapTemplateVirPath(System.Web.UI.Control control, string fileName)
        {
            return control.ResolveUrl(System.IO.Path.Combine("~/Templates/" + Domain.Current.ThemeFolder, fileName).Replace('\\', '/'));
        }

        public static Domain CurrentDomain { get { return Domain.Get(DomainHost); } }

        /// <summary>
        /// The current domain host (localhost, 127.0.0.1 etc)
        /// </summary>
        public static string DomainHost { get { return HttpContext.Current.Request.Url.Host; } }

        public static HttpRequest CurrentHttpRequest
        {
            get
            {
                return HttpContext.Current.Request;
            }
        }

        public static string RootUrl
        {
            get
            {
                return CurrentHttpRequest.Url.Scheme + "://" + CurrentHttpRequest.Url.Authority + CurrentHttpRequest.ApplicationPath.TrimEnd('/') + "/";
            }
        }

        public static string GetGlobalParameter(string paramName)
        {
            //debug
            //if (!Infrastructure.Application.IsProduction && HttpContext.Current != null)
            //{
            //    foreach (var key in HttpContext.Current.Request.Headers.AllKeys)
            //    {
            //        System.Diagnostics.Debug.WriteLine($"{key}: {HttpContext.Current.Request.Headers[key]}");
            //    }
            //}

            var Context = HttpContext.Current;
            if (Context == null) return null;
            if (Context.Response.Cookies.AllKeys.Contains(paramName))
                return Context.Response.Cookies.Get(paramName).Value;
            else if (Context.Request.QueryString.AllKeys.Contains(paramName))
                return Context.Request.QueryString.Get(paramName);
            else if (Context.Request.Headers.AllKeys.Contains(paramName))
                return Context.Request.Headers.Get(paramName);
            else if (Context.Request.Cookies.AllKeys.Contains(paramName))
                return Context.Request.Cookies.Get(paramName).Value;
            return null;
        }

        public static void SetGlobalParameter(string key, string value) //always on cookie
        {
            var appPath = HttpContext.Current.Request.ApplicationPath;
            //if (appPath.Length > 1 && !appPath.EndsWith("/"))
                //appPath = appPath + "/";
            var cookie = HttpContext.Current.Response.Cookies[key];
            cookie.HttpOnly = true;
            cookie.Secure = HttpContext.Current.Request.IsSecureConnection;
            if (!SessionKeyNotUsePath)
                cookie.Path = appPath;
            cookie.Value = value;
        }

        public static string CredentialsTokenName { get { return string.Format("{0}_Token", Domain.Current.Host.Replace(".", "_") + (SessionKeyNotUsePath ? "_" + Infrastructure.Application.InstanceName : "")); } }
        public static Guid CredentialsToken
        {
            get { return GetGlobalParameter(CredentialsTokenName).ToNullableGuid().GetValueOrDefault(); }
            set { SetGlobalParameter(CredentialsTokenName, value.ToString()); }
        }

        public static CultureInfo CurrentCulture { get { return Thread.CurrentThread.CurrentUICulture; } set { Thread.CurrentThread.CurrentUICulture = value; } }
        public static string CurrentCultureCountryCode
        {
            get
            {
                if (CurrentCulture.ToString().IndexOf("-") > -1) return CurrentCulture.ToString().Split('-')[1];
                else return CurrentCulture.ToString();
            }
        }

        public static string CurrentDateFormat { get { return WebUtils.CurrentCulture.DateTimeFormat.ShortDatePattern; } }
        public static Language CurrentLanguage
        {
            get
            {
                var culture = CurrentCulture;
                if (culture == null) return Language.Unknown;
                return Enums.GetLanguage(CurrentCulture);
            }
            set
            {
                System.Globalization.CultureInfo.GetCultureInfo(value.ToString());
            }
        }

        public static string CurrentLanguageShort { get { return CurrentCulture.Parent.ToString().ToLower(); } }
        public static string CurrentLanguageFull { get { return CurrentCulture.EnglishName; } }

        public static string CurrentTopLanguageName
        {
            get
            {
                if (CurrentCulture.Parent == null)
                    return CurrentCulture.EnglishName;
                return CurrentCulture.Parent.EnglishName;
            }
        }

        public static string getSupportedLanguage(string[] usrLanguages)
        {
            var lngList = System.Configuration.ConfigurationManager.AppSettings["Languages"].EmptyIfNull().Split(',');
            if (usrLanguages == null) return lngList.Length > 0 ? lngList[0] : null;
            for (int i = 0; i < usrLanguages.Length; i++)
                if (lngList.Contains(usrLanguages[i]))
                    return usrLanguages[i];
            return lngList.Length > 0 ? lngList[0] : null;
        }

        public static void SelectLanguage(string defaultLanguage)
        {
            bool updateCookie = true;
            HttpCookie requestCookie = HttpContext.Current.Request.Cookies["UICulture"];
            string culture = HttpContext.Current.Request.QueryString["culture"];
            if (string.IsNullOrEmpty(culture))
            {
                if (requestCookie != null) culture = requestCookie.Value;
            }
            if (string.IsNullOrEmpty(culture))
            {
                if (defaultLanguage == null)
                {
                    defaultLanguage = getSupportedLanguage(HttpContext.Current.Request.UserLanguages);
                    if (defaultLanguage == null) defaultLanguage = "en-us";
                }
                culture = defaultLanguage;
            }
            if (requestCookie != null)
                updateCookie = (requestCookie.Value != culture);
            try
            {
                CurrentCulture = new CultureInfo(culture);
                if (updateCookie)
                {
                    requestCookie = new HttpCookie("UICulture", culture);
                    HttpContext.Current.Response.Cookies.Set(requestCookie);
                }
            }
            catch
            {
                CurrentCulture = new CultureInfo(defaultLanguage);
            }
        }

        public static string ApplicationPath
        {
            get
            {
                string path = System.Web.HttpContext.Current.Request.ApplicationPath;
                if (path == "/")
                    path = "";

                return path;
            }
        }

        public static Language CurrentLanguageOrDefault
        {
            get
            {
                Language currentLanguage = CurrentLanguage;
                if (currentLanguage != Language.English && currentLanguage != Language.Hebrew)
                    currentLanguage = Language.English;

                return currentLanguage;
            }
        }

        public static bool IsRtl
        {
            get
            {
                if (CurrentCulture == null || CurrentCulture.TextInfo == null) return false;
                return CurrentCulture.TextInfo.IsRightToLeft;
            }
        }

        public static string CurrentDirection { get { return IsRtl ? "rtl" : "ltr"; } }
        public static string CurrentReverseDirection { get { return IsRtl ? "ltr" : "rtl"; } }
        public static string CurrentAlignment { get { return IsRtl ? "right" : "left"; } }
        public static string CurrentReverseAlignment { get { return IsRtl ? "left" : "right"; } }

        public static void Login(System.Guid credentialsToken)
        {
            //HttpContext.Current.Session.Clear();
            //if (credentialsToken == Guid.Empty) HttpContext.Current.Session["credentialsToken"] = null;
            ObjectContext.Current.CredentialsToken = credentialsToken;
            CredentialsToken = credentialsToken;
        }

        public static LoginResult Login(System.Security.Principal.WindowsIdentity identity = null)
        {
            Guid credentialsToken = Guid.Empty;
            if (identity == null)
            {
                identity = System.Web.HttpContext.Current.User.Identity as System.Security.Principal.WindowsIdentity;
                if (identity == null || !identity.IsAuthenticated) identity = System.Web.HttpContext.Current.Request.LogonUserIdentity;
                if (!(identity.IsAuthenticated || identity.ImpersonationLevel == System.Security.Principal.TokenImpersonationLevel.Impersonation)) identity = null;
            }
            LoginResult result = Infrastructure.Security.Login.DoLogin(identity, null, out credentialsToken);
            Login(credentialsToken);
            return result;
        }

        public static LoginResult Login(UserRole[] userType, string userName, string email, string password, int? appIdentityId = null)
        {
            Guid credentialsToken;
            LoginResult result = Infrastructure.Security.Login.DoLogin(userType, userName, email, password, null, out credentialsToken, appIdentityId : appIdentityId);
            Login(credentialsToken);
            return result;
        }

        public static string DecryptUrlBase64String(string urlBase64String)
        {
            if (string.IsNullOrEmpty(urlBase64String)) return null;
            if (!urlBase64String.IsBase64UrlString()) return null;
            return Encryption.Decrypt(urlBase64String.FromBase64UrlString()); 
        }

        public static void Logout(bool killSession = true)
        {
            var user = ObjectContext.Current.User;
            if (user != null) user.LogOff();
            Login(Guid.Empty);
            if (killSession && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session.Abandon();
                RegenerateId(HttpContext.Current);
            }
        }

        public static bool IsLoggedin { get { return ObjectContext.Current.User != null; } }
        public static Infrastructure.Security.Login LoggedUser
        {
            get
            {
                var ret = ObjectContext.Current.User;
                if (ret == null) throw new Infrastructure.NotLoggedinException();
                return ret;
            }
        }

        public static void SendFile(string fileName, string contentType = null, string disposition = "inline")
        {
            if (!System.IO.File.Exists(fileName))
            {
                HttpContext.Current.Response.Status = "File not found";
                HttpContext.Current.Response.StatusCode = 404;
                HttpContext.Current.Response.End();
                return;
            }
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", disposition + ";filename=" + HttpUtility.UrlEncode(System.IO.Path.GetFileName(fileName)));
            if (contentType == null)
            {
                var fileExt = System.IO.Path.GetExtension(fileName);
                switch (fileExt)
                {
                    case ".png": contentType = "image/png"; break;
                    case ".bmp": contentType = "image/bmp"; break;
                    case ".gif": contentType = "image/gif"; break;
                    case ".jpg": contentType = "image/jpg"; break;
                    default: contentType = "application/octet-stream"; break;
                }
            }
            if (contentType != null) HttpContext.Current.Response.ContentType = contentType;
            //else if (disposition == "inline") HttpContext.Current.Response.ContentType = "application/octet-stream";
            if (HttpRuntime.UsingIntegratedPipeline)
                HttpContext.Current.Response.Headers.Add("Content-Length", new System.IO.FileInfo(fileName).Length.ToString());
            HttpContext.Current.Response.WriteFile(fileName, false);
            HttpContext.Current.Response.End();
            //HttpContext.Current.Response.TransmitFile(fileName);
            //HttpContext.Current.ApplicationInstance.CompleteRequest(); 
        }


        public static int SessionTimeout { get { return Infrastructure.Application.SessionTimeout; } }
        public static int AlertSessionTimeout { get { return Infrastructure.Application.SessionTimeout - 2; } }

        private static void RegenerateId(HttpContext Context)
        {
            System.Web.SessionState.SessionIDManager manager = new System.Web.SessionState.SessionIDManager();
            string oldId = manager.GetSessionID(Context);
            string newId = manager.CreateSessionID(Context);
            bool isAdd = false, isRedir = false;
            manager.SaveSessionID(Context, newId, out isRedir, out isAdd);
            HttpApplication ctx = (HttpApplication)HttpContext.Current.ApplicationInstance;
            HttpModuleCollection mods = ctx.Modules;
            System.Web.SessionState.SessionStateModule ssm = (System.Web.SessionState.SessionStateModule)mods.Get("Session");
            System.Reflection.FieldInfo[] fields = ssm.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            System.Web.SessionState.SessionStateStoreProviderBase store = null;
            System.Reflection.FieldInfo rqIdField = null, rqLockIdField = null, rqStateNotFoundField = null;
            foreach (System.Reflection.FieldInfo field in fields)
            {
                if (field.Name.Equals("_store")) store = (System.Web.SessionState.SessionStateStoreProviderBase)field.GetValue(ssm);
                if (field.Name.Equals("_rqId")) rqIdField = field;
                if (field.Name.Equals("_rqLockId")) rqLockIdField = field;
                if (field.Name.Equals("_rqSessionStateNotFound")) rqStateNotFoundField = field;
            }
            object lockId = rqLockIdField.GetValue(ssm);
            if ((lockId != null) && (oldId != null)) store.ReleaseItemExclusive(Context, oldId, lockId);
            rqStateNotFoundField.SetValue(ssm, true);
            rqIdField.SetValue(ssm, newId);
        }
        /// <summary>
        /// method to get Client ip address
        /// </summary>
        /// <param name="GetLan"> set to true if want to get local(LAN) Connected ip address</param>
        /// <returns></returns>
        public static string GetVisitorIPAddress(bool GetLan = false)
        {
            string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (String.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
            {
                GetLan = true;
                visitorIPAddress = string.Empty;
            }

            if (GetLan && string.IsNullOrEmpty(visitorIPAddress))
            {
                //This is for Local(LAN) Connected ID Address
                string stringHostName = Dns.GetHostName();
                //Get Ip Host Entry
                IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                //Get Ip Address From The Ip Host Entry Address List
                IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                try
                {
                    visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                }
                catch
                {
                    try
                    {
                        visitorIPAddress = arrIpAddress[0].ToString();
                    }
                    catch
                    {
                        try
                        {
                            arrIpAddress = Dns.GetHostAddresses(stringHostName);
                            visitorIPAddress = arrIpAddress[0].ToString();
                        }
                        catch
                        {
                            visitorIPAddress = "127.0.0.1";
                        }
                    }
                }

            }

            return visitorIPAddress;
        }

        public static string UserName
        {
            get
            {
                var request = HttpContext.Current.Request;
                string sMyUsername;
                sMyUsername = request.ServerVariables["REMOTE_USER"].ToLower();

                if (string.IsNullOrEmpty(sMyUsername) && request.LogonUserIdentity != null)
                {
                    sMyUsername = request.LogonUserIdentity.Name.ToLower();
                }

                if (request.ServerVariables["SERVER_NAME"] == "localhost" && request.Cookies.AllKeys.Contains("userName") && sMyUsername == "")
                {
                    if (request.Cookies["userName"].Value != "") sMyUsername = request.Cookies["userName"].Value.ToLower();
                }

                if (sMyUsername.LastIndexOf(@"\") > 0)
                {
                    sMyUsername = sMyUsername.Substring(sMyUsername.LastIndexOf(@"\") + 1);
                }

                return sMyUsername;
            }
        }
    }

}

