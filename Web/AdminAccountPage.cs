﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;

namespace Netpay.Web
{
	public partial class Admin_AccountPage : System.Web.UI.Page
	{
		public AccountType? AccountType { get; set; }
		public int? AccountID { get; set; }
		public event EventHandler AccountChanged;
		public Netpay.Bll.Accounts.Account _account;
		public Netpay.Bll.Accounts.Account Account
		{
			get { return _account; }
			set
			{
				_account = value;
				if (AccountChanged != null) AccountChanged(this, new EventArgs());
			}
		}

		protected override void OnInit(EventArgs e)
		{
			AccountID = Request.QueryString["id"].ToNullableInt();
			if (AccountID != null)
			{
				Account = Account.LoadObject(AccountID.Value);
				if (Account != null) AccountType = Account.AccountType;
			}
			else
			{
				var accountType = (Netpay.Bll.Accounts.AccountType?)Request.QueryString["type"].ToNullableInt();
				//Account = Account.Create(WebUtils.CredentialsToken, accountType);
			}
			base.OnInit(e);
		}
	}
}
