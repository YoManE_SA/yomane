﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Netpay.Web")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Netpay")]
[assembly: AssemblyProduct("Netpay.Web")]
[assembly: AssemblyCopyright("Copyright © Netpay 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("cdfede2e-b64a-4769-aecd-0d723f15b73e")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]

// embedded resource files
[assembly: WebResource("Netpay.Web.Controls.ScriptManager.netpay.Common.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.ScriptManager.netpay.commonTypes.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.ScriptManager.netpay.Blocker.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.ScriptManager.netpay.SessionTimeout.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.ScriptManager.netpay.Ajax.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.ScriptManager.netpay.Timezone.js", "text/javascript")]

[assembly: WebResource("Netpay.Web.Controls.PageHelpBox.PageHelpBox.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.PageHelpBox.PageHelpBox.css", "text/css")]
[assembly: WebResource("Netpay.Web.Controls.PageHelpBox.ArrowUp.png", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.PageHelpBox.ArrowDown.png", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.PageItemHelp.PageItemHelp.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.PageItemHelp.PageItemHelp.css", "text/css")]
[assembly: WebResource("Netpay.Web.Controls.PageItemHelp.IconQuestion.gif", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.PageItemComment.PageItemComment.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.PageItemComment.PageItemComment.css", "text/css")]

[assembly: WebResource("Netpay.Web.Controls.PageItemComment.IconCommentOn.gif", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.PageItemComment.IconCommentOff.gif", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.PageItemComment.note-icon-online.png", "image/png")]
[assembly: WebResource("Netpay.Web.Controls.PageItemComment.note-icon-offline.png", "image/png")]

[assembly: WebResource("Netpay.Web.Controls.SortableColumns.ArrowDown.gif", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.SortableColumns.ArrowUp.gif", "image/gif")]

[assembly: WebResource("Netpay.Web.Controls.Totals.Totals.css", "text/css")]

[assembly: WebResource("Netpay.Web.Controls.NotificationSlip.Curl_ReminderLtr.gif", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.NotificationSlip.Curl_ReminderRtl.gif", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.NotificationSlip.Curl_WarningLtr.gif", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.NotificationSlip.Curl_WarningRtl.gif", "image/gif")]

[assembly: WebResource("Netpay.Web.Controls.AutoComplete.AutoComplete.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.Pager.Pager.css", "text/css")]
[assembly: WebResource("Netpay.Web.Controls.SortableColumns.SortableColumns.css", "text/css")]
[assembly: WebResource("Netpay.Web.Controls.DenormalizedTransactionRowView.DenormalizedTransactionRowView.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.DateRangePicker.DateRangePicker.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.DatePicker.DatePicker.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.NumberRangeSlider.NumberRangeSlider.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.PopupIFrame.PopupIFrame.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.PopupButton.PopupButton.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.DragDropSelector.DragDropSelector.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.DragDropSelector.DragDropSelector.css", "text/css")]

[assembly: WebResource("Netpay.Web.Controls.Tabs.organictabs.js", "text/javascript")]
[assembly: WebResource("Netpay.Web.Controls.NotificationSlip.Curl_ReminderLtr.gif", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.NotificationSlip.Curl_ReminderRtl.gif", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.NotificationSlip.Curl_WarningLtr.gif", "image/gif")]
[assembly: WebResource("Netpay.Web.Controls.NotificationSlip.Curl_WarningRtl.gif", "image/gif")]
