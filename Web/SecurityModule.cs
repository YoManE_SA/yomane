﻿using System;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Netpay.Infrastructure;

namespace Netpay.Web
{
	public class SecurityModule : IHttpModule
	{
		private const string securityFormId = "securityToken";
		private const string securityFormIdRequest = "ctl00$securityToken";
		
		#region IHttpModule Members

		public void Dispose()
		{

		}

		public void Init(HttpApplication context)
		{
			context.PreRequestHandlerExecute += new EventHandler(PreRequestHandler);
		}

		#endregion

		public void PreRequestHandler(object sender, EventArgs e)
		{
			HttpContext context = HttpContext.Current;
			if (context == null)
				return;

			Page page = context.Handler as Page;
			if (page == null)
				return;

			page.Load += new EventHandler(PageLoadHandler);
		}

		private void PageLoadHandler(object sender, EventArgs e)
		{
			Page page = sender as Page;
			if (page == null) 
				return;

			HtmlForm form = page.Form;
			if (form == null)
				return;

			if (!WebUtils.IsLoggedin)
				return;

			string generatedSecurityKey = (WebUtils.CredentialsToken.ToString()).ToSha256(); // WebUtils.LoggedUser.HashKey
			string requestSecurityKey = page.Request[securityFormIdRequest];
			if (page.IsPostBack) 
			{
				if (requestSecurityKey == null)
				{
					page.Response.Clear();
					page.Response.Write("Security token is missing");
					page.Response.End();
					return;
				}
				else
				{
					if (requestSecurityKey != generatedSecurityKey)
					{
						page.Response.Clear();
						page.Response.Write("Security token is invalid");
						page.Response.End();
						return;
					}
				}		
			}

			HtmlInputHidden hidden = new HtmlInputHidden();
			hidden.ID = securityFormId;
			hidden.Value = generatedSecurityKey;
			
			page.Form.Controls.Add(hidden);
		}
	}
}
