﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Netpay.Web.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Legend {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Legend() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Netpay.Web.Resources.Legend", typeof(Legend).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Active.
        /// </summary>
        internal static string active {
            get {
                return ResourceManager.GetString("active", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Admin.
        /// </summary>
        internal static string admin {
            get {
                return ResourceManager.GetString("admin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approved.
        /// </summary>
        internal static string approved {
            get {
                return ResourceManager.GetString("approved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Authorize.
        /// </summary>
        internal static string authorize {
            get {
                return ResourceManager.GetString("authorize", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Authorize Decline.
        /// </summary>
        internal static string authorizeDecline {
            get {
                return ResourceManager.GetString("authorizeDecline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bank Fee.
        /// </summary>
        internal static string bankFee {
            get {
                return ResourceManager.GetString("bankFee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Blocked.
        /// </summary>
        internal static string blocked {
            get {
                return ResourceManager.GetString("blocked", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Canceled.
        /// </summary>
        internal static string canceled {
            get {
                return ResourceManager.GetString("canceled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Capture.
        /// </summary>
        internal static string capture {
            get {
                return ResourceManager.GetString("capture", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chargeback.
        /// </summary>
        internal static string chargeback {
            get {
                return ResourceManager.GetString("chargeback", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Copy Request.
        /// </summary>
        internal static string clarification {
            get {
                return ResourceManager.GetString("clarification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Decline.
        /// </summary>
        internal static string decline {
            get {
                return ResourceManager.GetString("decline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ended.
        /// </summary>
        internal static string ended {
            get {
                return ResourceManager.GetString("ended", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Merchant.
        /// </summary>
        internal static string merchant {
            get {
                return ResourceManager.GetString("merchant", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not Approved.
        /// </summary>
        internal static string notApproved {
            get {
                return ResourceManager.GetString("notApproved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passed.
        /// </summary>
        internal static string passed {
            get {
                return ResourceManager.GetString("passed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pending.
        /// </summary>
        internal static string pending {
            get {
                return ResourceManager.GetString("pending", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Copy Request (non-refundable).
        /// </summary>
        internal static string pendingChargeback {
            get {
                return ResourceManager.GetString("pendingChargeback", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Under Construction.
        /// </summary>
        internal static string precreated {
            get {
                return ResourceManager.GetString("precreated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Suspended.
        /// </summary>
        internal static string suspended {
            get {
                return ResourceManager.GetString("suspended", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to System.
        /// </summary>
        internal static string system {
            get {
                return ResourceManager.GetString("system", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test.
        /// </summary>
        internal static string test {
            get {
                return ResourceManager.GetString("test", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Waiting.
        /// </summary>
        internal static string waiting {
            get {
                return ResourceManager.GetString("waiting", resourceCulture);
            }
        }
    }
}
