﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Netpay.Web.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class TransactionAmountType {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TransactionAmountType() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Netpay.Web.Resources.TransactionAmountType", typeof(TransactionAmountType).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Admin Charge.
        /// </summary>
        internal static string AdminCharge {
            get {
                return ResourceManager.GetString("AdminCharge", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Affiliate Chargeback Fee.
        /// </summary>
        internal static string AffiliateFeeChargeback {
            get {
                return ResourceManager.GetString("AffiliateFeeChargeback", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Affiliate Process Fee.
        /// </summary>
        internal static string AffiliateFeeProcess {
            get {
                return ResourceManager.GetString("AffiliateFeeProcess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Authorize.
        /// </summary>
        internal static string Authorize {
            get {
                return ResourceManager.GetString("Authorize", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bank Authorization Fee.
        /// </summary>
        internal static string BankFeeAuthorization {
            get {
                return ResourceManager.GetString("BankFeeAuthorization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bank Chb Fee.
        /// </summary>
        internal static string BankFeeChb {
            get {
                return ResourceManager.GetString("BankFeeChb", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bank Clarification Fee.
        /// </summary>
        internal static string BankFeeClarification {
            get {
                return ResourceManager.GetString("BankFeeClarification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bank Line Fee.
        /// </summary>
        internal static string BankFeeLine {
            get {
                return ResourceManager.GetString("BankFeeLine", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bank Refund Fee.
        /// </summary>
        internal static string BankFeeRefund {
            get {
                return ResourceManager.GetString("BankFeeRefund", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bank Transaction Fee.
        /// </summary>
        internal static string BankFeeTransaction {
            get {
                return ResourceManager.GetString("BankFeeTransaction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BankPayed.
        /// </summary>
        internal static string BankPayed {
            get {
                return ResourceManager.GetString("BankPayed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Capture.
        /// </summary>
        internal static string Capture {
            get {
                return ResourceManager.GetString("Capture", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chargeback.
        /// </summary>
        internal static string Chargeback {
            get {
                return ResourceManager.GetString("Chargeback", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Debitor Direct Fees.
        /// </summary>
        internal static string DebitorDirectFees {
            get {
                return ResourceManager.GetString("DebitorDirectFees", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Debitor Direct Payment.
        /// </summary>
        internal static string DebitorDirectPayment {
            get {
                return ResourceManager.GetString("DebitorDirectPayment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Authorization Fee.
        /// </summary>
        internal static string FeeAuthorization {
            get {
                return ResourceManager.GetString("FeeAuthorization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CC Storage Fee.
        /// </summary>
        internal static string FeeCCStorage {
            get {
                return ResourceManager.GetString("FeeCCStorage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chb Fee.
        /// </summary>
        internal static string FeeChb {
            get {
                return ResourceManager.GetString("FeeChb", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clarification Fee.
        /// </summary>
        internal static string FeeClarification {
            get {
                return ResourceManager.GetString("FeeClarification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Declined Fee.
        /// </summary>
        internal static string FeeDeclined {
            get {
                return ResourceManager.GetString("FeeDeclined", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Line Fee.
        /// </summary>
        internal static string FeeLine {
            get {
                return ResourceManager.GetString("FeeLine", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Refund Fee.
        /// </summary>
        internal static string FeeRefund {
            get {
                return ResourceManager.GetString("FeeRefund", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Transaction Fee.
        /// </summary>
        internal static string FeeTransaction {
            get {
                return ResourceManager.GetString("FeeTransaction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Generate Installments.
        /// </summary>
        internal static string GenerateInstallments {
            get {
                return ResourceManager.GetString("GenerateInstallments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Generate Old Installments.
        /// </summary>
        internal static string GenerateOldInstallments {
            get {
                return ResourceManager.GetString("GenerateOldInstallments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Handling Fee.
        /// </summary>
        internal static string HandlingFee {
            get {
                return ResourceManager.GetString("HandlingFee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Installment.
        /// </summary>
        internal static string Installment {
            get {
                return ResourceManager.GetString("Installment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Installment Capture.
        /// </summary>
        internal static string InstallmentCapture {
            get {
                return ResourceManager.GetString("InstallmentCapture", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Installment Item.
        /// </summary>
        internal static string InstallmentItem {
            get {
                return ResourceManager.GetString("InstallmentItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Refund.
        /// </summary>
        internal static string Refund {
            get {
                return ResourceManager.GetString("Refund", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rolling Release.
        /// </summary>
        internal static string RollingRelease {
            get {
                return ResourceManager.GetString("RollingRelease", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rolling Reserve.
        /// </summary>
        internal static string RollingReserve {
            get {
                return ResourceManager.GetString("RollingReserve", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to System Charge.
        /// </summary>
        internal static string SystemCharge {
            get {
                return ResourceManager.GetString("SystemCharge", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tax.
        /// </summary>
        internal static string Tax {
            get {
                return ResourceManager.GetString("Tax", resourceCulture);
            }
        }
    }
}
