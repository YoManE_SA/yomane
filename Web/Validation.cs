﻿using System;
using System.Resources;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Web
{
	public class Validation
	{
		public static bool IsInvalidIban(TextBox inputControl, int countryID, Label errorControl = null, string title = null, bool isResourceKey = false, bool focusTextControl = true)
		{
            if (inputControl.Text.Trim().Length == 0) 
            {
                errorControl.Text = "<div class=\"error\">IBAN is empty</div>";
                return true;
            }
            
            ResourceManager rm = WebUtils.GetResource("common");
			if (isResourceKey)
				title = rm.GetString(title);

			var country = Bll.Country.Get(countryID);
			if (country == null)
			{
				errorControl.Text = "<div class=\"error\">Invalid country</div>";
				return true;
			}

			if (country.IbanLength > 0)
			{
				if (inputControl.Text.Length != country.IbanLength | (inputControl.Text.Substring(0, 2) != country.IsoCode2))
				{
					if (focusTextControl)
						inputControl.Focus();
					if (errorControl != null)
						errorControl.Text = "<div class=\"error\">Must be " + country.IbanLength + " characters Starting with " + country.IsoCode2 + "," + title + "</div>";
					return true;
				}
			}

			return false;
		}

		public static bool IsInvalidAba(TextBox inputControl, int countryID, Label errorControl = null, string title = null, bool isResourceKey = false, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			if (isResourceKey)
				title = rm.GetString(title);

			var country = Bll.Country.Get(countryID);
			if (country == null)
			{
				errorControl.Text = "<div class=\"error\">Invalid country</div>";
				return true;
			}

			if (country.IsABARequired)
			{
				if (inputControl.Text.Length != 9)
				{
					if (focusTextControl)
						inputControl.Focus();
					if (errorControl != null)
						errorControl.Text = "<div class=\"error\">valid bank ABA (9 digits)" + title + "</div>";
					return true;
				}
			}

			return false;
		}

		public static bool IsEmptyField(DropDownList inputControl, Label errorControl = null, string title = null, bool isResourceKey = false, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			if (isResourceKey)
				title = rm.GetString(title);
			if (inputControl.Text.Trim() == "")
			{
				if (focusTextControl)
					inputControl.Focus();
				if ((errorControl != null))
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorEmptyField").Replace("%FIELD%", (string.IsNullOrEmpty(title) ? rm.GetString("ErrorThereAreEmptyFields") : title)) + "</div>";

				return true;
			}

			return false;
		}

		public static bool IsEmptyField(TextBox inputControl, Label errorControl = null, string title = null, bool isResourceKey = false, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			if (isResourceKey)
				title = rm.GetString(title);
			if (inputControl.Text.Trim() == "")
			{
				if (focusTextControl)
					inputControl.Focus();
				if ((errorControl != null))
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorEmptyField").Replace("%FIELD%", (string.IsNullOrEmpty(title) ? rm.GetString("ErrorThereAreEmptyFields") : title)) + "</div>";

				return true;
			}

			return false;
		}

		public static bool IsNotSelected(DropDownList inputControl, Label errorControl = null, string title = null, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			if (inputControl.SelectedIndex == 0)
			{
				if (focusTextControl)
					inputControl.Focus();
				if (errorControl != null)
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorListNotSelected").Replace("%FIELD%", (string.IsNullOrEmpty(title) ? rm.GetString("ErrorThereAreEmptyFields") : title)) + "</div>";
				
				return true;
			}

			return false;
		}

		public static bool IsNotMail(TextBox inputControl, Label errorControl = null, string title = null, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			if (IsNotRegEx(inputControl, "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\\w"))
			{
				if (focusTextControl)
					inputControl.Focus();
				if ((errorControl != null))
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorInvalidMail").Replace("%FIELD%", (string.IsNullOrEmpty(title) ? rm.GetString("MailAddress") : title)) + "</div>";
				
				return true;
			}
			
			return false;
		}

		public static bool IsNotRegEx(TextBox inputControl, string expression, Label errorControl = null, string title = null, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			Regex regExp = new Regex(expression, RegexOptions.IgnoreCase);
			if (!regExp.IsMatch(inputControl.Text))
			{
				if (focusTextControl)
					inputControl.Focus();
				if ((errorControl != null))
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorInvalidRegex").Replace("%FIELD%", (string.IsNullOrEmpty(title) ? rm.GetString("ErrorThereAreEmptyFields") : title)) + "</div>";
				
				return true;
			}

			return false;
		}

		public static bool IsNotMoney(TextBox inputControl, Label errorControl = null, string title = null, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			if (IsNotRegEx(inputControl, "^[0-9]+\\.{0,1}[0-9]{0,2}$"))
			{
				if (focusTextControl)
					inputControl.Focus();
				if ((errorControl != null))
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorInvalidMoney").Replace("%FIELD%", (string.IsNullOrEmpty(title) ? rm.GetString("MoneyField") : title)) + "</div>";
				
				return true;
			}
			
			return false;
		}

		public static bool IsInvalidPassword(TextBox inputControl, Label errorControl = null, string title = null, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			inputControl.Text = inputControl.Text.Trim();
			bool invalid = false;
			if (inputControl.Text.Length < 8)
			{
				invalid = true;
			}
			else if (Validation.IsNotRegEx(inputControl, "[0-9a-zA-Z]*[0-9][0-9a-zA-Z]*[0-9][0-9a-zA-Z]*"))
			{
				invalid = true;
			}
			else if (Validation.IsNotRegEx(inputControl, "[0-9a-zA-Z]*[a-zA-Z][0-9a-zA-Z]*[a-zA-Z][0-9a-zA-Z]*"))
			{
				invalid = true;
			}

			if (invalid)
			{
				if (focusTextControl)
					inputControl.Focus();
				if (errorControl != null)
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorPasswordNotValid").Replace("%FIELD%", (string.IsNullOrEmpty(title) ? rm.GetString("Password") : title)) + "</div>";
				
				return true;
			}

			return false;
		}

		public static bool IsMismatchFields(TextBox inputControl1, TextBox inputControl2, Label errorControl = null, string title1 = null, string title2 = null, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			if (inputControl1.Text.Trim() != inputControl2.Text.Trim())
			{
				if (focusTextControl)
					inputControl2.Focus();
				if ((errorControl != null))
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorFieldMismatch").Replace("%FIELD1%", (string.IsNullOrEmpty(title1) ? rm.GetString("EMail") : title1)).Replace("%FIELD2%", (string.IsNullOrEmpty(title2) ? rm.GetString("ValidateEMail") : title2)) + "</div>";
				
				return true;
			}

			return false;
		}

		public static bool IsNotChecked(CheckBox inputControl, Label errorControl = null, string title = null, bool focusTextControl = true, string errorMsg = null)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			if (!inputControl.Checked)
			{
				if (focusTextControl)
					inputControl.Focus();
				if ((errorControl != null))
					errorControl.Text = "<div class=\"error\">" + (errorMsg == null ? rm.GetString("ErrorRequiredCheckboxNotChecked") : errorMsg).Replace("%FIELD%", (string.IsNullOrEmpty(title) ? rm.GetString("ErrorThereAreEmptyFields") : title)) + "</div>";
				
				return true;
			}

			return false;
		}

		public static string Mandatory
		{
			get { return "<span class=\"mandatory\">*</span>"; }
		}

		public static bool IsCustomError(TextBox inputControl, bool isError, Label errorControl = null, string title = null, bool isResourceKey = false, bool focusTextControl = true, string errorMsg = null)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			if (isError)
			{
				if (focusTextControl)
					inputControl.Focus();
				if ((errorControl != null))
				{
					errorControl.Text = "<div class=\"error\">";
					if (string.IsNullOrEmpty(errorMsg))
					{
						if (isResourceKey)
						{
							if (string.IsNullOrEmpty(title))
								title = rm.GetString("ErrorValueMissingOrInvalid").Replace("%FIELD%", rm.GetString("ErrorThereAreEmptyFields"));
							errorControl.Text += rm.GetString(title);
						}
						else
						{
							if (string.IsNullOrEmpty(title))
								title = rm.GetString("ErrorThereAreEmptyFields");
							errorControl.Text += rm.GetString("ErrorInvalidRegex").Replace("%FIELD%", title);
						}
					}
					else
					{
						errorControl.Text += (isResourceKey ? rm.GetString(errorMsg) : errorMsg);
					}
					errorControl.Text += "</div>";
				}

				return true;
			}

			return false;
		}

		public static bool IsNotLimitedTo(TextBox inputControl, string chars, Label errorControl = null, string title = null, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			for (int i = 0; i <= inputControl.Text.Length - 1; i++)
			{
				if (!chars.Contains(inputControl.Text.Substring(i, 1)))
				{
					if (focusTextControl)
						inputControl.Focus();
					if ((errorControl != null))
						errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorInvalidCharacter").Replace("%CHAR%", inputControl.Text.Substring(i, 1)).Replace("%FIELD%", (string.IsNullOrEmpty(title) ? rm.GetString("ErrorThereAreEmptyFields") : title)) + "</div>";
					
					return true;
				}
			}

			return false;
		}

		public static bool IsPastDate(DropDownList inputControlYear, DropDownList inputControlMonth = null, DropDownList inputControlDay = null, Label errorControl = null, string title = null, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			int selectedYear = int.Parse(inputControlYear.SelectedValue);
			if (selectedYear < 100)
				selectedYear += 2000;
			int selectedMonth = 1;
			if (inputControlMonth != null)
				selectedMonth = int.Parse(inputControlMonth.SelectedValue);
			int selectedDay = (new DateTime(selectedYear, selectedMonth, 1).AddMonths(1).AddDays(-1).Day);
			if (inputControlDay != null && inputControlDay.SelectedValue != null)
				selectedDay = int.Parse(inputControlDay.SelectedValue);
			DateTime selectedDate = new DateTime(selectedYear, selectedMonth, selectedDay);

			if (selectedDate < DateTime.Now)
			{
				if (focusTextControl)
					inputControlYear.Focus();
				if ((errorControl != null))
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorPastDateSpecified").Replace("%FIELD%", (string.IsNullOrEmpty(title) ? rm.GetString("DateField") : title)) + "</div>";
				
				return true;			
			}

			return false;
		}

		public static bool IsNotCaptcha(TextBox inputControl, bool? testResult, Label errorControl = null, bool focusTextControl = true)
		{
			ResourceManager rm = WebUtils.GetResource("common");
			if (errorControl != null)
				errorControl.Text = "";
			if (testResult == null)
			{
				if (errorControl != null)
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorNoCaptcha") + "</div>";
				inputControl.Text = "";
				if (focusTextControl)
					inputControl.Focus();

				return true;
			}
			if (!testResult.Value)
			{
				if ((errorControl != null))
					errorControl.Text = "<div class=\"error\">" + rm.GetString("ErrorInvalidCaptcha") + "</div>";
				inputControl.Text = "";
				if (focusTextControl)
					inputControl.Focus();

				return true;
			}
			inputControl.Text = "";

			return false;
		}

		public static void HtmlDecodeStringProperties(object source)
		{
			System.Reflection.MemberInfo[] properties = source.GetType().FindMembers(System.Reflection.MemberTypes.Property, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance, null, null);
			foreach (var currentProperty in properties)
			{
				var currentPropertyInfo = source.GetType().GetProperty(currentProperty.Name);
				if (currentPropertyInfo.PropertyType == typeof(string))
				{
					object currentPropertyValue = currentPropertyInfo.GetValue(source, null);
					if (currentPropertyValue != null)
					{
						string currentPropertyCleanValue = currentPropertyValue.ToString().ToDecodedHtml();
						if (currentPropertyInfo.CanWrite)
							currentPropertyInfo.SetValue(source, currentPropertyCleanValue, null);
					}
				}
			}
		}

	}
}
