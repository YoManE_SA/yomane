﻿<%@ Page Language="VB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
	Sub Page_Load()
		litInfo.Text = "Server: " & My.Computer.Name & "<br />"
		litInfo.Text &= "Time: " & Date.Now.ToLongDateString & " " & Date.Now.ToLongTimeString & "<br />"
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
		<asp:Literal runat="server" ID="litInfo" />
	</form>
</body>
</html>