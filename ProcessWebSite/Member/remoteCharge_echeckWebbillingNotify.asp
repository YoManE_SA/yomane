<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="remoteCharge_Functions.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/MerchantTerminals.asp"-->
<!--#include file="../include/timer_class.asp"-->

<%
'---------------------------------------------------------------------
'	Debit Webbilling
'---------------------------------------------------------------------
FileAppendData "Webbilling.txt", "Request:" & Request.QueryString & vbCrLf & vbCrLf

Dim X_OCurrency, X_OAmount, X_DebitReferenceCode, nApprovalRecurringSeries, nRes, X_ReplyURL, xRequestParams
bUseOldTerminal = True
nApprovalRecurringSeries = "NULL"
If (Request("debit_Webbilling") = "ON") Then
    X_ApprovalNumber = TestNumVar(Request("master_transactionid"), 0, -1, 0)
    If (X_ApprovalNumber <> 0) Then
        If (TestNumVar(Request("reason_code"), 0, -1, 0) <> 0) Then
            Call ChargeBackTrans(0, "ApprovalNumber='" & X_ApprovalNumber & "'")
        ElseIf (TestNumVar(Request("status"), 0, -1, -1) = 0) Then
            nRes = MovePendingTransaction("DebitApprovalNumber='" & X_ApprovalNumber & "'", "000")
        Else
            nRes = MovePendingTransaction("DebitApprovalNumber='" & X_ApprovalNumber & "'", Request("status"))
        End If
        nRes = nRes & " OK"
        Response.Write("OK")
    Else    
        Response.Write("TransactionId not found")
    End if    
ElseIf (Request("debit_Webbilling") = "Asyn") And Request("action") = "chargeback" Then
    X_ReferenceCode = TestNumVar(Request("processor_id"), 0, -1, 0)
    Call ChargeBackTrans(0, "DebitReferenceCode='" & X_ReferenceCode & "'")
    Response.Write("OK")
ElseIf (Request("debit_Webbilling") = "Succ") Or (Request("debit_Webbilling") = "Fail") Then
    X_ReferenceCode = TestNumVar(Request("processor_id"), 0, -1, 0)
    If Not GetTransInfo("Lca_ReplyCode=553 And Lca_DebitCompanyID=37 And Lca_TransNum IN(Select companyTransPending_id From tblCompanyTransPending Where DebitCompanyID=37 And DebitReferenceCode='" & X_ReferenceCode & "')") Then 
        Response.Write("Transaction not found")
        Response.End
    Else
        X_ReplyURL = GetURLValue(xRequestParams, "RetURL")
        Response.Write("Process - " & X_ReplyURL)
    End If
    ExecSql("Update tblCompanyTransPending Set DebitApprovalNumber='" & TestNumVar(Request("transactionid"), 0, -1, 0) & "' Where DebitCompanyID=37 And DebitReferenceCode='" & X_ReferenceCode & "'")
    If Request("debit_Webbilling") = "Succ" Then
        nRes = MovePendingTransaction("DebitCompanyID=37 And DebitReferenceCode='" & X_ReferenceCode & "'", "000")
        ReturnResponseFromTransID nRes, "000"
    Else
        nRes = MovePendingTransaction("DebitCompanyID=37 And DebitReferenceCode='" & X_ReferenceCode & "'", Request(status))
        ReturnResponseFromTransID nRes, "002"
    End If
Else    
    Response.Write("Unknown request parameters")
End If

Response.End()
%>