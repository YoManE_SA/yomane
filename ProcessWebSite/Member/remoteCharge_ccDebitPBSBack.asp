<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="remoteCharge_Functions.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/MerchantTerminals.asp"-->
<!--#include file="../include/timer_class.asp"-->
<%
Server.ScriptTimeout = 300
Dim nData, nRes, x_TransId, xmlRet, X_ReplyURL
Dim xLogChargeAttemptID, xMerchantNum, xRequestParams, xSendParams, xRetParams, xDebitCompany, X_DebitReferenceCode, X_DebitReferenceNum, X_DebitApprovalNumber, xTransID, xLogReplyCode
nApprovalRecurringSeries = "NULL"

FileAppendData "PBS.txt", "Request:" & Now & " FORM:" & Request.Form & " QS:" & Request.QueryString & vbCrLf
sReturnCode = Request("orderStatus")
X_DebitReferenceNum = Request("tradeNo")
X_DebitReferenceCode = Request("orderNo")
X_CustomErrorDesc = Request("orderInfo")
If Trim(sReturnCode) = "1" Then 'success
	sReturnCode = "000"
ElseIf Trim(sReturnCode) = "-1" Then 'pending
	sReturnCode = "001"
ElseIf Trim(sReturnCode) = "-2" Then ' to be confirmed
	sReturnCode = "001"
Else 'If Trim(sReturnCode) = "0" Then  ' decline
    'If sPBSCode = "000" Or sPBSCode = "001" Then sPBSCode = "002"
    'If (Cint(X_TypeCredit) = 0) Then sPBSCode = sPBSCode & "-RF"
    If InStr(1, X_CustomErrorDesc, "_") > 0 Then
        Dim errInfo : errInfo = Split(X_CustomErrorDesc, "_", 2)
        sReturnCode = errInfo(0)
        X_CustomErrorDesc = errInfo(1)
    Else
    	sReturnCode = "002"
    End If
    If sReturnCode = "000" Or sReturnCode = "001" Then sReturnCode = "002"
    If sReturnCode = "" Then sReturnCode = "002"
End If
x_TransId = ExecScalar("Select ID From tblCompanyTransPending Where DebitCompanyID IN(74) And DebitReferenceCode='" & X_DebitReferenceCode & "'", -1)
If x_TransId = -1 Then 
    If sReturnCode = "000" Then x_TransId = ExecScalar("Select ID From tblCompanyTransPass Where DebitCompanyID IN(74) And DebitReferenceCode='" & X_DebitReferenceCode & "'", -1) _
    Else x_TransId = ExecScalar("Select ID From tblCompanyTransFail Where DebitCompanyID IN(74) And DebitReferenceCode='" & X_DebitReferenceCode & "'", -1)
End If
If x_TransId = -1 Or Not GetTransInfo("Lca_ReplyCode='553' And Lca_TransNum=" & x_TransId) Then
    Response.Write("Transaction Not Found")
    Response.End
End If
X_ReplyURL = GetURLValue(xRequestParams, "RetURL")

ExecSql("Update tblCompanyTransPending Set DebitApprovalNumber='" & X_DebitReferenceNum & "' Where companyTransPending_id=" & x_TransId)
nRes = MovePendingTransaction("companyTransPending_id=" & x_TransId, sReturnCode)
'Response.Write(sReturnCode & " " &  X_CustomErrorDesc & " " & X_DebitReferenceCode & " " & x_TransId & " " & nRes & "<br/>")
If sReturnCode <> "000" Then ExecSql("Update tblCompanyTransFail Set DebitDeclineReason='" & Replace(X_CustomErrorDesc, "'", "''") & "' Where ID=" & nRes)
ExecSql("Update tblLogChargeAttempts Set Lca_ReplyCode='" & sReturnCode & "', Lca_ReplyDesc='" & Replace(X_CustomErrorDesc, "'", "''") & "', Lca_TransNum=" & nRes & ", Lca_ResponseString= Lca_ResponseString + '" & DBText(Request.Form) & vbCrLf & "<br/>" & vbCrLf & "' Where LogChargeAttempts_id=" & xLogChargeAttemptID)

'If sReturnCode <> "000" Then sReturnCode = "002"
ReturnResponseFromTransID nRes, sReturnCode
Response.End
%>