<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
Dim X_HashKey, X_ForceCCStorageMD5, PaymentMethod, CardId, bSaveData, X_Action, X_Ret, X_NetpayCompanyNum

Sub saveLog(fReplyCode, responseText)
	Dim requestData
	requestData = HideUrlParam(Request.QueryString, "CardNum", 4, 10)
	If requestData <> "" Then requestData = requestData & "&"
	requestData = requestData & HideUrlParam(Request.Form, "CardNum", 4, 10)

	ExecSql "Exec [Log].[spInsertLogCardStorage] " & _
		IIF(IsEmpty(X_NetpayCompanyNum), "Null", X_NetpayCompanyNum) & ",'" & _
		X_Action & "'," & _
		IIF(IsEmpty(fReplyCode), "Null", fReplyCode) & ",'" & _
		Replace(requestData, "'", "''") & "','" & _
		Replace(responseText, "'", "''") & "'"
End Sub

Function returnResponse (fReplyCode, fReplyDesc, fCardId, fInsertDate, fComment)
	Dim nReply : nReply = TestNumVar(fReplyCode, 1, 0, -1)
	Dim sReply : sReply = fReplyDesc
	If sReply = "" Then
		Dim sSQL : sSQL = "SELECT GD_Text FROM GetGlobalData(34) WHERE GD_ID=" & nReply
		sReply = ExecScalar(sSQL, "Unexpected Error")
	End If
	replyString = "ReplyCode=" & Right("00" & trim(nReply), 2) & "&ReplyDesc=" & sReply & "&CardId=" & fCardId & "&InsertDate=" & fInsertDate & "&Comment=" & fComment
	saveLog fReplyCode, replyString

	Response.write(replyString)
	CloseConnection
	Response.end
	'if request("ReplyURL") <> "" then
	'	response.redirect(request("ReplyURL") & "?Reply=" & responseCode & "&CardId=" & cardId & "&InsertDate=" & insertDate & "&Comment=" & comment) 
	'else
	'	response.write responseCode & ";" & cardId & ";" & insertDate & ";" & replace(comment, ";", ",") 
	'end if
	'response.end
End function

X_CompanyNum = DBText(Request("CompanyNum"))
CardId = TestNumVar(Request("CardId"), 0, -1, 0)
X_Action = Request("Action")
If X_Action = "" Then X_Action = "add" Else X_Action = LCase(X_Action)
If X_Action = "add" Or X_Action = "modify" Then bSaveData = True

'Test company 
sSQL="SELECT id, IsCcStorage, ActiveStatus, HashKey, ForceCCStorageMD5 FROM tblCompany WHERE CustomerNumber='" & X_CompanyNum & "'"
set rsData = oledbData.execute(sSQL)
If rsData.EOF then
	call returnResponse("11", "Invalid Company or signature", "", "", "")
Else
	if rsData("IsCcStorage") = 0 then 
		call returnResponse("13", "Merchant is unauthorized to use Card Storage", "", "", "")
	elseif TestNumVar(rsData("ActiveStatus"), 0, -1, CMPS_NEW) <= CMPS_MAXCLOSED then 
		call returnResponse("14", "Merchant is closed or blocked", "", "", "")
	else
		X_NetpayCompanyNum = rsData("id")
		X_HashKey = rsData("HashKey")
		X_ForceCCStorageMD5 = IIF(IsNull(rsData("ForceCCStorageMD5")), False, rsData("ForceCCStorageMD5"))
	end if
End if
rsData.Close

'signature validation
If Request("signature") <> "" Or X_ForceCCStorageMD5 Then
	Dim hashValue
	hashValue = ValidateHash(TestStrVar(Request("signatureType"), 0, -1, "MD5"), X_CompanyNum + X_HashKey, Request("signature"))
	If Not hashValue Then hashValue = ValidateHash(TestStrVar(Request("signatureType"), 0, -1, "SHA"), X_CompanyNum + X_HashKey, Request("signature"))
	If hashValue = Empty Then returnResponse "16", "Invalid signature type", "", "", ""
	If Not hashValue Then returnResponse "11", "Invalid Company or signature", "", "", ""
End if

If X_Action = "modify" Then
	If CardId <= 0 Then call returnResponse("15", "Parameter CardID missing", CardId, insertDate, comment)
	sSQL = "SELECT * FROM tblCCStorage WHERE ID=" & CardId & " And companyID=" & X_NetpayCompanyNum
	Set rsData2 = oledbData.execute(sSQL)
	If Not rsData2.EOF Then
		X_CHPersonalNum = rsData2("CHPersonalNum") 
		X_ExpMonth = rsData2("ExpMM") 
		X_ExpYear = rsData2("ExpYY") 
		X_CVV2 = DecCVV(rsData2("cc_cui"))
		X_CHPhoneNumber = rsData2("CHPhoneNumber")
		X_CHEmail = rsData2("CHEmail")
		X_CHFullName = rsData2("CHFullName")
		X_Comment = rsData2("Comment")
		ccTypeHebShow = rsData2("Ccard_display")
		'sIP = rsData2("IPAddress")
		X_CountryID = TestNumVar(rsData2("countryId"), 0, -1, "null")
		X_StateID = TestNumVar(rsData2("stateId"), 0, -1, "null")
		X_Street1 = rsData2("CHStreet")
		X_Street2 = rsData2("CHStreet1")
		X_City = rsData2("CHSCity")
		X_ZipCode = rsData2("CHSZipCode")
		'Response.Write("AA" & rsData2("countryId") )
	Else
		CardId = 0	
	End If
	rsData2.Close()	
	If CardId <= 0 Then call returnResponse("15", "CardID not found", CardId, insertDate, comment)
End If

If bSaveData Then
	If NOT IsNumeric(request("CompanyNum")) Then call returnResponse("10", "One of the required fields is missing or invalid", "", "", "CompanyNum")
	If X_Action = "add" And NOT IsNumeric(request("CardNum")) OR	NOT IsNumeric(request("ExpMonth")) OR	NOT IsNumeric(request("ExpYear")) or	trim(request("CHFullName")) = "" then
		call returnResponse("10", "One of the required fields is missing or invalid", "", "", "CardNum")
	End if
	sIP = DBText(Request.ServerVariables("REMOTE_ADDR"))
	If request("CardNum") <> "" Then X_CardType = Replace(DBText(request("CardNum")), " ", "")
	If Len(X_CardType) < 6 OR Len(X_CardType) > 19 Then Call returnResponse("17", "", "", "", "")

	If request("ExpMonth") <> "" Then X_ExpMonth = DBText(request("ExpMonth"))
	If request("ExpYear") <> "" Then X_ExpYear = DBText(request("ExpYear"))
	If request("CVV2") <> "" Then X_CVV2 = DBText(request("CVV2"))
	If X_CVV2 <> "" Then If Len(X_CVV2) < 3 OR Len(X_CVV2) > 5 Then Call returnResponse("17", "", "", "", "")
	If request("CHEmail") <> "" Then X_CHEmail = DBText(request("CHEmail"))
	If request("CHPhoneNumber") <> "" Then X_CHPhoneNumber = DBText(request("CHPhoneNumber"))
	If request("CHPersonalNum") <> "" Then X_CHPersonalNum = DBText(request("CHPersonalNum"))
	If request("CHFullName") <> "" Then X_CHFullName = DBText(request("CHFullName"))
	If request("Comment") <> "" Then X_Comment = DBText(request("Comment"))

	' address
	X_CountryID="null"
	If request("billingCountry") <> "" Then X_CountryID = ExecScalar("SELECT CountryID FROM [List].[CountryList] where CountryISOCode = '" & DBText(request("billingCountry")) & "'", "null")
	X_StateID="null"
	If request("billingState") <> "" Then X_StateID = ExecScalar("SELECT stateID FROM [List].[StateList] WHERE StateISOCode = '" & DBText(request("billingState")) & "'", "null")
	If request("billingStreet1") <> "" Then X_Street1 = DBText(request("billingStreet1"))
	If request("billingStreet2") <> "" Then X_Street2 = DBText(request("billingStreet2"))
	If request("billingCity") <> "" Then X_City = DBText(request("billingCity"))
	If request("billingZipcode") <> "" Then X_ZipCode = DBText(request("billingZipcode"))

	If request("CardNum") <> "" Or X_Action = "add" Then 
		X_CardNum = Fn_CleanCCNumber(request("CardNum"))
		ccBINCountry = CCNumToCountry(X_CardNum, PaymentMethod, "")
		ccTypeHebShow = ExecScalar("Select GD_Text From tblGlobalData Where GD_LNG=1 And GD_Group=" & GGROUP_PAYMENTMETHOD & " And GD_ID=" & PaymentMethod, "UNK") & " .... " & right(X_CardNum,4)
		ccTypeID = PaymentMethod - PMD_CC_UNKNOWN
		If PaymentMethod = 0 Then Call returnResponse("12", "Cannot identify card type / invalid card number", "", "", "")
		If PaymentMethod <> PMD_CC_ISRACARD And PaymentMethod <> PMD_CC_DIRECT Then
			If NOT fn_IsValidCCNum(X_CardNum, false, sCompanyID, 0, 0, 0) then
				Call returnResponse("12", "Cannot identify card type / invalid card number", "", "", "")
			End if
		End if
	End If	
End If

'20110602 Tamir - validate address length
If Len(X_Street1) > 100 Then Call returnResponse("10", "", "", "", "billingStreet1 is too long")
If Len(X_Street2) > 100 Then Call returnResponse("10", "", "", "", "billingStreet2 is too long")
If Len(X_City) > 60 Then Call returnResponse("10", "", "", "", "billingCity is too long")
If Len(X_ZipCode) > 20 Then Call returnResponse("10", "", "", "", "billingZipcode is too long")

If X_Action = "add" Or X_Action = "modify" Then
	'Store cc number
	'If CardId <= 0 Then returnResponse "15", "CardNum already exists number.", CardId, "", ""
	sSQL = "StoreCreditCard " & X_NetpayCompanyNum & ", " & PaymentMethod & ", N'" & X_CHPersonalNum & "', '" & Replace(X_CardNum, "'", "''") & "', '" & X_ExpMonth & "', '" & X_ExpYear & "', N'" & EncCVV(X_CVV2) & "', N'" & X_CHPhoneNumber & "', N'" & X_CHEmail & "', N'" & X_CHFullName & "', N'" & X_Comment & "', N'" & ccTypeHebShow & "', N'" & sIP & "', " & X_CountryID & ", " & X_StateID & ", N'" & X_Street1 & "', N'" & X_Street2 & "', N'" & X_City & "', N'" & X_ZipCode & "';"
	set rsData2 = oledbData.Execute("DECLARE @nID int;EXEC @nID=" & sSQL & "SELECT @nID, GetDate();")
	If Not rsData2.EOF Then
		CardId = rsData2(0)
		insertDate = rsData2(1)
		comment = X_Comment
	Else
		rsData2.Close
		returnResponse "10", "One of the required fields is missing or invalid.", 0, "", ""
	End If
	rsData2.Close
ElseIf X_Action = "remove" Then
	If CardId <= 0 Then call returnResponse("15", "Parameter CardID missing", CardId, insertDate, comment)
	oledbData.execute("UPDATE tblCCStorage SET isDeleted=1 WHERE ID=" & CardId & " AND CompanyID=" & X_NetpayCompanyNum)
	call returnResponse("00", "Card Removed", CardId, insertDate, comment)
ElseIf X_Action = "view" Then
	If CardId <= 0 Then call returnResponse("15", "Parameter CardID missing", CardId, insertDate, comment)
	sSQL = "SELECT * FROM tblCCStorage WHERE ID=" & CardId & " And companyID=" & X_NetpayCompanyNum
	Set rsData2 = oledbData.execute(sSQL)
		X_Ret = ""
		X_Ret = X_Ret & "?CardID=" & rsData2("ID")
		X_Ret = X_Ret & "&ExpMonth=" & Server.URLEncode(Trim(rsData2("ExpMM")))
		X_Ret = X_Ret & "&ExpYear=" & Server.URLEncode(Trim(rsData2("ExpYY")))
		X_Ret = X_Ret & "&CHFullName=" & Server.URLEncode(Trim(rsData2("CHFullName")))
		X_Ret = X_Ret & "&CHEmail=" & Server.URLEncode(Trim(rsData2("CHEmail")))
		X_Ret = X_Ret & "&CHPersonalNum=" & Server.URLEncode(Trim(rsData2("CHPersonalNum")))
		X_Ret = X_Ret & "&CHPhoneNumber=" & Server.URLEncode(Trim(rsData2("CHPhoneNumber")))
		X_Ret = X_Ret & "&Comment=" & Server.URLEncode(Trim(rsData2("Comment")))
		X_Ret = X_Ret & "&billingStreet1=" & Server.URLEncode(Trim(rsData2("CHStreet")))
		X_Ret = X_Ret & "&billingStreet2=" & Server.URLEncode(Trim(rsData2("CHStreet1")))
		X_Ret = X_Ret & "&billingCity=" & Server.URLEncode(Trim(rsData2("CHSCity")))
		X_Ret = X_Ret & "&billingZipcode=" & Server.URLEncode(Trim(rsData2("CHSZipCode")))
		X_Ret = X_Ret & "&billingState=" & rsData2("stateId")
		X_Ret = X_Ret & "&billingCountry=" & rsData2("countryId")
	rsData2.Close
	Response.Write(X_Ret)
	saveLog 0, X_Ret
	Response.End()
End If
call returnResponse("00", "Card has been stored successfully", CardId, insertDate, comment)
%>