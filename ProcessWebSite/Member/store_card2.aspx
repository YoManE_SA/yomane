<%@ Page Language="vb" AutoEventWireup="false" %>
<script runat="server" language="vbscript">
	Private Function TransReq(rqVal As String) as String
		Dim strBytes() As Byte = Encoding.Unicode.GetBytes(rqVal)
		strBytes = Encoding.Convert(Encoding.Unicode, Encoding.GetEncoding("windows-1255"), strBytes)
		Dim asciiChars(Encoding.Default.GetCharCount(strBytes, 0, strBytes.Length)) As Char
		Encoding.Default.GetChars(strBytes, 0, strBytes.Length, asciiChars, 0)
		return New String(asciiChars)
	End Function

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim sDataUnicode As String = Request.QueryString.ToString()
		If String.IsNullOrEmpty(sDataUnicode) Then sDataUnicode = Request.Form.ToString()
		Dim sData As String = TransReq(sDataUnicode)
		Dim sURL As String = IIf(Request.ServerVariables("HTTPS").ToUpper = "ON", "https://", "http://") & Request.Url.Host & Request.Url.AbsolutePath.ToLower.Replace("store_card2.aspx", "store_card.asp")
		Dim sReply As String = String.Empty
		dbPages.SendHttpRequest(sURL, sData, , sReply)
		Response.Write(sReply)
	End Sub
</script>