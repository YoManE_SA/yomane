<%
'---------------------------------------------------------------------
'	Debit PayVision
'---------------------------------------------------------------------
Function PV_Make2DigDate(xNum)
	If Len(xNum) = 1 Then xNum = "0" & xNum
	PV_Make2DigDate = xNum
End Function

Function PV_ReplaceEscapeSequences(sText)
	dim i, j, sTemp
	If IsEmpty(sText) Then
		sTemp = ""
	ElseIf InStr(sText, "&") < 1 Then
		sTemp = sText
	Else
		sTemp = Replace(sText, "&;", " ")
		For i = 1 To Len(sTemp)
			If Mid(sTemp, i, 1) = "&" Then
				For j = i To Len(sTemp)
					If Mid(sTemp, i, 1) = ";" Then sTemp = Replace(sTemp, Mid(sTemp, i, j-i+1), Mid(sTemp, i+1, 1))
				Next
			End If
		Next
		sTemp = Replace(Replace(sTemp, "&", " "), ";", " ")
	End If
	PV_ReplaceEscapeSequences = sTemp
End Function

Function debitPayVision()
	If Trim(X_ccHolderName)<>"" Then X_ccHolderName = PV_ReplaceEscapeSequences(X_ccHolderName)
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer, soapAction', IsoCurreny
	xNames = Split(X_ccHolderName, " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	If Len(sCompanyDescriptor) > 25 Then sCompanyDescriptor = Left(sCompanyDescriptor, 25)
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0 And X_DebitReferenceCode <> "") Then
	    If CInt(X_TypeCredit) = 0 Then soapAction = "Refund" Else soapAction = "Capture"
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & vbCrLf & _
		" <soap:Body>" & vbCrLf & _
		"  <" & soapAction & " xmlns=""http://payvision.com/gateway/"">" & vbCrLf & _
		"	<memberId>" & sAccountId & "</memberId>" & vbCrLf & _
		"	<memberGuid>" & sAccountPassword & "</memberGuid>" & vbCrLf & _
		"	<transactionId>" & X_DebitReferenceCode & "</transactionId>" & vbCrLf & _
		"	<transactionGuid>" & X_ConfirmNumber & "</transactionGuid>" & vbCrLf & _
		"	<amount>" & formatNumber(X_Amount, 2) & "</amount>" & vbCrLf & _
		"	<currencyId>" & GetCurISOCode(Cint(X_Currency)) & "</currencyId>" & vbCrLf & _
		"	<trackingMemberCode>" & GetTransRefCode() & "</trackingMemberCode>" & vbCrLf & _
		"  </" & soapAction & ">" & vbCrLf & _
		" </soap:Body>" & vbCrLf & _
		"</soap:Envelope>"
	Else
	    If CInt(X_TypeCredit)=0 Then 
    		soapAction = "Credit"
	    Else 
    		If Cint(X_TransType) = 1 Then soapAction = "Authorize" _
    	    Else soapAction = "Payment"
	    End If
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & vbCrLf & _
		" <soap:Body>" & vbCrLf & _
		"  <" & soapAction & " xmlns=""http://payvision.com/gateway/"" >" & vbCrLf & _
		"	<memberId>" & sAccountId & "</memberId>" & vbCrLf & _
		"	<memberGuid>" & sAccountPassword & "</memberGuid>" & vbCrLf & _
		"	<countryId>" & ExecScalar("SELECT ISONumber FROM List.CountryList WHERE CountryID = " & TestNumVar(BACountry, 1, 0, 0), "") & "</countryId>" & vbCrLf & _
		"	<amount>" & formatNumber(X_Amount, 2) & "</amount>" & vbCrLf & _
		"	<currencyId>" & GetCurISOCode(Cint(X_Currency)) & "</currencyId>" & vbCrLf & _
		"	<trackingMemberCode>" & X_DebitReferenceCode & "</trackingMemberCode>" & vbCrLf & _
		"	<cardNumber>" & Replace(X_ccNumber, " ", "") & "</cardNumber>" & vbCrLf & _
		"	<cardHolder>" & X_ccHolderName & "</cardHolder>" & vbCrLf & _
		"	<cardExpiryMonth>" & X_ccExpMM & "</cardExpiryMonth>" & vbCrLf & _
		"	<cardExpiryYear>20" & X_ccExpYY & "</cardExpiryYear>" & vbCrLf & _
		"	<cardCvv>" & X_ccCVV2 & "</cardCvv>" & vbCrLf & _
		"	<cardType></cardType>" & vbCrLf & _
		"	<issueNumber>" & "</issueNumber>" & vbCrLf & _
		"	<merchantAccountType>" & IIF(Trim(requestSource) = "20", "4", "1") & "</merchantAccountType>" & vbCrLf & _
		"	<dynamicDescriptor>" & sCompanyDescriptor & "</dynamicDescriptor>" & vbCrLf & _
		"	<avsAddress>" & "</avsAddress>" & vbCrLf & _
		"	<avsZip>" & "</avsZip>" & vbCrLf & _
		"  </" & soapAction & ">" & vbCrLf & _
		" </soap:Body>" & vbCrLf & _
		"</soap:Envelope>"
	End if

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	UrlAddress = "https://testprocessor.payvisionservices.com/Gateway/BasicOperations.asmx"
	
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false, sAccountSubId, sAccountPassword '"paybynet", "start"
		HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		HttpReq.setRequestHeader "SOAPAction", "http://payvision.com/gateway/" & soapAction
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		'Response.Write(ParamList & "<br>" & sResDetails) : Response.End()
		HttpError = FormatHttpRequestError(HttpReq)
	
	    'Made by tamir to hide cvv and card number from log
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "<cardCvv>" & X_ccCVV2, "<cardCvv>" & GetSafePartialNumber(X_ccCVV2))
		sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

		Set xmlRet = HttpReq.responseXml
		sReturnCode = Trim(xmlRet.selectSingleNode("//Result").text)
		sApprovalNumber = xmlRet.selectSingleNode("//TransactionGuid").text
		sError = xmlRet.selectSingleNode("//Message").text
		X_DebitReferenceCode = xmlRet.selectSingleNode("//TransactionId").text
	On Error Goto 0

	If Trim(sReturnCode) = "0" Then
		sReturnCode = "000"
	ElseIf Trim(sReturnCode) = "001" Then 
		sReturnCode = "002" '001 is taken by netpay
	End if	

	Set xmlRet = Nothing
	Set HttpReq = Nothing

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>