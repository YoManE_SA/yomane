<%
'---------------------------------------------------------------------
'	Debit debitEpx
'---------------------------------------------------------------------
Function Epx_Make2DigDate(xNum)
	If Len(xNum) = 1 Then xNum = "0" & xNum
	Epx_Make2DigDate = xNum
End Function

Function ReplaceEscapeSequences(sText)
	dim i, j, sTemp
	If IsEmpty(sText) Then
		sTemp = ""
	ElseIf InStr(sText, "&") < 1 Then
		sTemp = sText
	Else
		sTemp = Replace(sText, "&;", " ")
		For i = 1 To Len(sTemp)
			If Mid(sTemp, i, 1) = "&" Then
				For j = i To Len(sTemp)
					If Mid(sTemp, i, 1) = ";" Then sTemp = Replace(sTemp, Mid(sTemp, i, j-i+1), Mid(sTemp, i+1, 1))
				Next
			End If
		Next
		sTemp = Replace(Replace(sTemp, "&", " "), ";", " ")
	End If
	ReplaceEscapeSequences = sTemp
End Function

Function debitEpx()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer, trnType, batchId
	If int(X_TypeCredit) = 8 Or int(X_Payments) > 1 Then Call throwError("508") 
	xNames = Split(ReplaceEscapeSequences(X_ccHolderName), " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = ""
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = ""
	batchId = Epx_Make2DigDate(Year(Now)) & Epx_Make2DigDate(Month(Now)) & Epx_Make2DigDate(Day(Now))
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_ConfirmNumber = "" Then Call throwError("532") 'Reftrans not Found
		If Cint(X_TypeCredit) = 0 Then trnType = "CCE9" Else trnType = "CCE4"
		ParamList = ParamList & _ 
			"CUST_NBR=" & sAccountId & _
			"&MERCH_NBR=" & sAccountSubId & _
			"&DBA_NBR=1" & _
			"&TERMINAL_NBR=1" & _
			"&TRAN_TYPE=" & trnType & _
			"&BATCH_ID=" & batchId & _
			"&TRAN_NBR=" & X_DebitReferenceCode & _
			"&ACCOUNT_NBR=" & Server.URLEncode(Replace(X_ccNumber, " ", "")) & _
			"&AMOUNT=" & formatNumber(X_Amount, 2, true, false, false) & _
			"&AUTH_CODE=" & X_ConfirmNumber & _
			"&ORIG_AUTH_GUID=" & X_DebitReferenceNum
	Else
		If Len(sCompanyDescriptor) > 24 Then sCompanyDescriptor = Left(sCompanyDescriptor, 24)
		trnType = IIF(Cint(X_TransType) = 1, "CCE2", "CCE1")
        X_DebitReferenceCode = GetTransRefCode()
		ParamList = _ 
			"CUST_NBR=" & Server.URLEncode(sAccountId) & _
			"&MERCH_NBR=" & Server.URLEncode(sAccountSubId) & _
			"&DBA_NBR=1" & _
			"&TERMINAL_NBR=1" & _
			"&TRAN_TYPE=" & Server.URLEncode(trnType) & _
			"&BATCH_ID=" & Server.URLEncode(batchId) & _
			"&TRAN_NBR=" & Server.URLEncode(X_DebitReferenceCode) & _
			"&IDENT_NBR=" & Server.URLEncode(X_DebitReferenceCode) & _
			"&ACCOUNT_NBR=" & Server.URLEncode(Replace(X_ccNumber, " ", "")) & _
			"&EXP_DATE=" & Server.URLEncode(Epx_Make2DigDate(X_ccExpYY) & Epx_Make2DigDate(X_ccExpMM)) & _
			"&CARD_ENT_METH=X" & _
			"&CVV2=" & Server.URLEncode(X_ccCVV2) & _
			"&AMOUNT=" & Server.URLEncode(formatNumber(X_Amount, 2, true, false, false)) & _
			"&CURRENCY_CODE=" & Server.URLEncode(GetCurISOCode(Cint(X_Currency))) & _
			"&FIRST_NAME=" & Server.URLEncode(fName) & _
			"&LAST_NAME=" & Server.URLEncode(sName) & _
			"&PHONE_CELL=" & Server.URLEncode(Replace(Replace(X_PhoneNumber, " ", ""), "-", "")) & _
			"&ADDRESS=" & Server.URLEncode(BACHAddr1) & _
			"&CITY=" & Server.URLEncode(BACity) & _
			"&STATE=" & Server.URLEncode(BAStateCode) & _
			"&ZIP_CODE=" & Server.URLEncode(BAZipCode) & _
			"&SPECIAL_9=" & Server.URLEncode(BACountryCode) & _
			"&SOFT_DESCRIPTOR=" & Server.URLEncode(sCompanyDescriptor)
	End if

	Dim SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS : SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS = 13056
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	Call HttpReq.setOption(2, SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS)
    Call HttpReq.setOption(3, "LOCAL_MACHINE\Root\*.epxuap.com") '"IWAM_NETPAY\Root"
	'HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    If isTestTerminal Then UrlAddress = "https://secure.epxuap.com" _
	Else UrlAddress = "https://secure.epx.com"
	'Response.Buffer=False

	On Error Resume Next
		'Response.Write(UrlAddress & "?" & ParamList & vbCrLf & "<br/>")
		'HttpReq.Option(17) = False 'WinHttpRequestOption_EnableHttp1_1
		HttpReq.open "POST", UrlAddress ', False, sAccountSubId, sAccountPassword
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		'HttpReq.setRequestHeader "Content-Length", Len(ParamList)
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		'HttpError = FormatHttpRequestError(HttpReq)
		TimeControl.Append("A_SEND") 'Time Optimization *********
		'Response.Write(ParamList & vbCrLf & "<br/>" & vbCrLf & sResDetails)

		If Len(sResDetails) = 0 Then Call throwError("521")
		sDebitReturnAnswer = Left(sResDetails, 3900)
	
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "CVV2=" & X_ccCVV2, "CVV2=" & GetSafePartialNumber(X_ccCVV2))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails
		
		Set xmlRet = HttpReq.responseXml
		'Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
		xmlRet.LoadXML HttpReq.responseText
		sReturnCode = Trim(xmlRet.selectSingleNode("//FIELD[@KEY='AUTH_RESP']").text) 'State
		sApprovalNumber = xmlRet.selectSingleNode("//FIELD[@KEY='AUTH_CODE']").text
		X_DebitReferenceNum = xmlRet.selectSingleNode("//FIELD[@KEY='AUTH_GUID']").text
		X_CustomErrorDesc = xmlRet.selectSingleNode("//FIELD[@KEY='AUTH_RESP_TEXT']").text

		'Response.Write(sReturnCode & " " & X_DebitReferenceNum & " " & sApprovalNumber & " " & X_CustomErrorDesc)
		'Response.End()
	On Error Goto 0
	If (Len(sReturnCode) < 1) Then Call throwError("520")
	
	If Trim(sReturnCode) = "00" Then
		sReturnCode = "000"
	Else
		sReturnCode = Trim(sReturnCode)
	End if	

	Set xmlRet = Nothing
	Set HttpReq = Nothing

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>