<%
	Dim pDataSend
	Function AddMDRPField(fNum, fValue)
		pDataSend = pDataSend & fNum & ":" & fValue & "/"
	End Function

	Function GetMDRPFieldValue(xtrData, xVal)
		Dim sIndex, eIndex
		sIndex = InStr(1, xtrData, xVal & ":")
		if sIndex < 1 Then Exit Function
		sIndex = sIndex + Len(xVal) + 1
		eIndex = InStr(sIndex, xtrData, "/")
		if eIndex < sIndex Then eIndex = Len(xtrData)
		GetMDRPFieldValue = Mid(xtrData, sIndex, eIndex - sIndex)
	End Function

	Sub SendRefundInvik(iRs)
		Dim pMP, lnCount, pRetData, IsoCurreny
	    Set pMP = Server.CreateObject("Compunet.CModernProcessing")
		pMP.Server = "172.17.3.51" : pMP.Port = 3002
		IsoCurreny = Array("376", "840", "978", "826")
		lnCount = TestNumVar(ExecScalar("SELECT dc_nextTransID FROM tblDebitCompany WHERE DebitCompany_id=21", 0), 0, -1, 0) + 1
		ExecSql("UPDATE tblDebitCompany SET dc_nextTransID=dc_nextTransID+1 WHERE DebitCompany_id=21")
		
		pDataSend = ""
		AddMDRPField "MSG", "0400"
		AddMDRPField "002", Replace(iRs("CCNumberX"), " ", "")
		AddMDRPField "003", "000000"
		AddMDRPField "004", pMP.PadLeft(formatNumber(iRs("Amount"), 2) * 100, 12, "0")
		AddMDRPField "007", pMP.Format(Now, "MMDDhhmmss")
		AddMDRPField "011", pMP.PadLeft(lnCount, 6, "0")
		AddMDRPField "012", pMP.Format(iRs("InsertDate"), "Hhmmss")
		AddMDRPField "013", pMP.Format(iRs("InsertDate"), "MMDD")
		AddMDRPField "014", Left(iRs("ExpYY"), 2) & Left(iRs("ExpMM"), 2)
		AddMDRPField "018", iRs("AccountPassword") 'MCC
		AddMDRPField "019", "352"
		AddMDRPField "022", "002"
		AddMDRPField "025", "59"
		AddMDRPField "037", pMP.PadLeft(iRs("DebitReferenceCode"), 12, "0")
		AddMDRPField "041", CStr(iRs("AccountId"))
		AddMDRPField "042", pMP.PadRight(Left(iRs("dt_Descriptor"), 15), 15, "0")
		AddMDRPField "049", IsoCurreny(iRs("Currency"))

		On Error Resume Next
		pMP.Connect()
		pRetData = pMP.SendTrans(pDataSend)
		pMP.Disconnect()
		On Error Goto 0
	    FileAppendData "InvikAutoRefund.txt", Now & vbCrLf & pDataSend & vbCrLf & pRetData & vbCrLf & vbCrLf
		xRetStatus = GetMDRPFieldValue(pRetData, "039")
		sApprovalNumber = GetMDRPFieldValue(pRetData, "038")
		If xRetStatus = "00" Then xRetStatus = "000"
	    Set pMP = Nothing
		Call ProcessResults(iRs, "INVIK", xRetStatus, "", sApprovalNumber)
	End Sub
%>
