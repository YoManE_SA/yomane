<%
'---------------------------------------------------------------------
'	Debit WireCard - 21042010 Udi
'---------------------------------------------------------------------
Function debitWireCard()
	Dim HttpReq, xmlRet, UrlAddress, ParamList, fName, sName, sApprovalNumber', IsoCurreny
	'IsoCurreny = Array("ILS", "USD", "EUR", "GBP", "AUD", "CAD", "JPY", "NOK")
	xNames = Split(X_ccHolderName, " ")
	if Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
	if Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))

	If (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
            "<WIRECARD_BXML xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"">" & vbCrLf & _
            " <W_REQUEST>" & vbCrLf & _
            "  <W_JOB>" & vbCrLf & _
            "   <JobID>" & X_DebitReferenceCode & "</JobID>" & vbCrLf & _
            "   <BusinessCaseSignature>" & sAccountId & "</BusinessCaseSignature>" & vbCrLf & _
            "   <" & IIF(Cint(X_TypeCredit) = 0, "FNC_CC_BOOKBACK", "FNC_CC_CAPTURE_AUTHORIZATION") & ">" & vbCrLf & _
            "    <FunctionID>" & X_DebitReferenceCode & "</FunctionID>" & vbCrLf & _
            "    <CC_TRANSACTION mode=""" & IIf(isTestOnly, "demo", "live") & """>" & vbCrLf & _
            "     <TransactionID>" & X_DebitReferenceCode & "</TransactionID>" & vbCrLf & _
            "     <GuWID>" & X_ConfirmNumber & "</GuWID>" & vbCrLf & _
            "     <Amount minorunits=""2"">" & CLng(X_Amount*100) & "</Amount>" & vbCrLf & _
            "     <Usage>" & sCompanyDescriptor & "</Usage>" & vbCrLf & _
            "    </CC_TRANSACTION>" & vbCrLf & _
            "   </" & IIF(Cint(X_TypeCredit) = 0, "FNC_CC_BOOKBACK", "FNC_CC_CAPTURE_AUTHORIZATION") & ">" & vbCrLf & _
            "  </W_JOB>" & vbCrLf & _
            " </W_REQUEST>" & vbCrLf & _
            "</WIRECARD_BXML>"
	Else
		X_DebitReferenceCode = GetTransRefCode()
	    ParamList = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCrLf & _
	        "<WIRECARD_BXML xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"">" & vbCrLf & _
	        " <W_REQUEST>" & vbCrLf & _
	        "  <W_JOB>" & vbCrLf & _
	        "   <JobID>" & X_DebitReferenceCode & "</JobID>" & vbCrLf & _
	        "   <BusinessCaseSignature>" & sAccountId & "</BusinessCaseSignature>" & vbCrLf & _
	        "   <" & IIF(Cint(X_TransType) = 0, "FNC_CC_PURCHASE", "FNC_CC_AUTHORIZATION") & ">" & vbCrLf & _
	        "    <FunctionID>" & X_DebitReferenceCode & "</FunctionID>" & vbCrLf & _
	        "    <CC_TRANSACTION mode=""" & IIf(isTestOnly, "demo", "live") & """>" & vbCrLf & _
	        "     <TransactionID>" & X_DebitReferenceCode & "</TransactionID>" & vbCrLf & _
	        "     <CommerceType>eCommerce</CommerceType>" & vbCrLf & _
	        "     <Amount minorunits=""2"">" & CLng(X_Amount*100) & "</Amount>" & vbCrLf & _
	        "     <Currency>" & GetCurISOName(X_Currency) & "</Currency>" & vbCrLf & _
	        "     <CountryCode>" & BACountryCode & "</CountryCode>" & vbCrLf & _
	        "     <Usage>" & sCompanyDescriptor & "</Usage>" & vbCrLf & _
	        "     <CREDIT_CARD_DATA>" & vbCrLf & _
	        "      <CreditCardNumber>" & Replace(X_ccNumber, " ", "") & "</CreditCardNumber>" & vbCrLf & _
	        "      <CVC2>" & X_ccCVV2 & "</CVC2>" & vbCrLf & _
	        "      <ExpirationYear>" & (X_ccExpYY mod 100) + 2000 & "</ExpirationYear>" & vbCrLf & _
	        "      <ExpirationMonth>" & X_ccExpMM & "</ExpirationMonth>" & vbCrLf & _
	        "      <CardHolderName>" & X_ccHolderName & "</CardHolderName>" & vbCrLf & _
	        "     </CREDIT_CARD_DATA>" & vbCrLf & _
	        "     <CONTACT_DATA>" & vbCrLf & _
	        "      <IPAddress>" & sIP & "</IPAddress>" & vbCrLf & _
	        "     </CONTACT_DATA>" & vbCrLf & _
	        "     <CORPTRUSTCENTER_DATA>" & vbCrLf & _
	        "      <ADDRESS>" & vbCrLf & _
	        "       <FirstName>" & fName & "</FirstName>" & vbCrLf & _
	        "       <LastName>" & sName & "</LastName>" & vbCrLf & _
	        "       <Address1>" & BACHAddr1 & "</Address1>" & vbCrLf & _
	        "       <Address2>" & BACHAddr2 & "</Address2>" & vbCrLf & _
	        "       <City>" & BACity & "</City>" & vbCrLf & _
	        "       <ZipCode>" & BAZipCode & "</ZipCode>" & vbCrLf & _
	        "       <State>" & BAStateCode & "</State>" & vbCrLf & _
	        "       <Country>" & BACountryCode & "</Country>" & vbCrLf
	        '"       <Phone>" & X_PhoneNumber & "</Phone>" & vbCrLf & _
			  ParamList = ParamList & _
	        "       <Email>" & X_Email & "</Email>" & vbCrLf & _
	        "      </ADDRESS>" & vbCrLf & _
	        "      <PERSONINFO>" & vbCrLf & _
	        "       <State>" & BAStateCode & "</State>" & vbCrLf & _
	        "      </PERSONINFO>" & vbCrLf & _
	        "     </CORPTRUSTCENTER_DATA>" & vbCrLf & _
	        "    </CC_TRANSACTION>" & vbCrLf & _
	        "   </" & IIF(Cint(X_TransType) = 0, "FNC_CC_PURCHASE", "FNC_CC_AUTHORIZATION") & ">" & vbCrLf & _
	        "  </W_JOB>" & vbCrLf & _
	        " </W_REQUEST>" & vbCrLf & _
	        "</WIRECARD_BXML>"
	End if
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	If isTestTerminal Then UrlAddress = "https://c3-test.wirecard.com/secure/ssl-gateway" _
	Else UrlAddress = "https://c3.wirecard.com/secure/ssl-gateway"
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false, sAccountSubId, sAccountPassword '"paybynet", "start"
		HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		Set xmlRet = HttpReq.responseXml

		sReturnCode = Trim(xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/FunctionResult").text)
    	sApprovalNumber = xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/GuWID").text
    	sError = xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/ERROR/Number").text
	On Error Goto 0
	ParamList = Replace(ParamList, X_ccNumber, GetSafePartialNumber(X_ccNumber))
	ParamList = Replace(ParamList, "<CVC2>" & X_ccCVV2, "<CVC2>" & GetSafePartialNumber(X_ccCVV2))
	sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
	SaveLogChargeAttemptRequestResponse ParamList, sResDetails

	'Response.Write(ParamList & vbCrLf & "<br><br>" & vbCrLf & sResDetails)
	'Response.End

	If sReturnCode = "" Then Call throwError("521")
	If sReturnCode = "ACK" Then
    	sReturnCode = "000"
	ElseIf sReturnCode = "PENDING" Then
    	sReturnCode = "000" '"001"
	Else
    	sReturnCode = sError
	End If
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>