<%@ Page Language="VB" %>
<%@ Import namespace="System.Data.SqlClient"%>
<script runat="server">
    Dim nMerchantID As Integer
    Dim dDateStart, dDateEnd As Date
    Dim nTransID As Integer = 0, sTransID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        nMerchantID = dbPages.TestVar(dbPages.ExecScalar("Select Top 1 ID From tblCompany Where CustomerNumber='" & Replace(Request("CompanyNum"), "'", "''") & "'"), 0, -1, 0)
        'Response.ContentType = "text/xml"
        Response.Write("<?xml version=""1.0"" encoding=""utf-8"" ?>" & vbCrLf)

        Dim nReqType As Integer = dbPages.TestVar(Request("ReqType"), 0, -1, 0)
        If nReqType = 0 Then
            nTransID = dbPages.TestVar(Request("TransID"), 1, 0, 0)
            sTransID = IIf(nTransID > 0, nTransID, "NULL")
        End If

        If nTransID = 0 And Not dbPages.IsActiveDSN2 Then ReturnResult(16, "The service is temporarily unavailable!")

        Dim CompanyNumber As String = dbPages.TestVar(Request("CompanyNum"), -1, "")
        If CompanyNumber = "" Then ReturnResult(12, "Merchant company number is invalid")
        Dim Signature As String = dbPages.TestVar(Request("Signature"), -1, "")

        dDateStart = dbPages.TestVar(Request("DateStart"), DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue)
        dDateEnd = dbPages.TestVar(Request("DateEnd"), DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue)
        If nTransID < 1 Then
            If dDateStart < New Date(1899, 1, 1) Then ReturnResult(11, "Missing or invalid parameter DateStart")
            If dDateEnd < New Date(1899, 1, 1) Then ReturnResult(11, "Missing or invalid parameter DateEnd")
            If Request("DateEnd").Length < 11 And (dDateEnd = dDateEnd.Date) Then dDateEnd = dDateEnd.AddSeconds(86399)
        End If

        Dim sSQL As String
        sSQL = "SELECT ID, IsAllowRemotePull, RemotePullIPs, HashKey, ActiveStatus FROM tblCompany WHERE CustomerNumber='" & dbPages.DBText(CompanyNumber) & "'"
        Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
        If drData.Read() Then
            If drData("ActiveStatus") < 10 Then ReturnResult(17, "Merchant is closed or blocked", drData)
            If Not drData("IsAllowRemotePull") Then ReturnResult(13, "Merchant is unauthorized to use this service", drData)
            Dim UserIp As String = IIf(String.IsNullOrWhiteSpace(Request.ServerVariables("HTTP_X_FORWARDED_FOR")), Request.UserHostAddress, Request.ServerVariables("HTTP_X_FORWARDED_FOR"))
            If UserIp.IndexOf(",") > -1 Then UserIp = UserIp.Split(",").First().Trim()
            If Not IPManager.ContainsIP(drData("RemotePullIPs"), UserIp) Then ReturnResult(14, "Service not allowed from your IP Address(" & UserIp & ")", drData)
            nMerchantID = dbPages.TestVar(drData("ID"), 0, -1, 0)
            If Not ((Md5Hash.HashMD5UTF8Base64(CompanyNumber & drData("HashKey")) = Signature) Or (Md5Hash.HashSHA256UTF8Base64(CompanyNumber & drData("HashKey")) = Signature)) Then _
                ReturnResult(15, "MD5 Signature not correct", drData)
            Select Case nReqType
                Case 0
                    OutputTransPass()
                Case 1
                    OutputTransFail()
                Case 5
                    OutputTransApproval()
                Case 10
                    OutputRecurringModifyLog()
                Case Else
                    ReturnResult(11, "Missing or invalid parameter ReqType", drData)
            End Select
        Else
            ReturnResult(12, "Merchant company number is invalid", drData)
        End If
        drData.Close()
    End Sub

    Protected Sub ReturnResult(ByVal nCode As Integer, ByVal nDesc As String, Optional ByVal drData As SqlDataReader = Nothing)
        If Not drData Is Nothing Then drData.Close()
        dbPages.ExecSql("Exec [Log].[spInsertHistory] " &
               "8" & "," &
               IIf(nMerchantID = 0, "NULL", nMerchantID) & "," &
               "'" & dbPages.DBText(Request("ReqType")) & "'," &
               "'" & dbPages.DBText(nCode) & "'," &
               "'" & dbPages.DBText(Request.QueryString.ToString() & "&" & Request.Form.ToString()) & "'," &
               "'" & dbPages.DBText(nDesc) & "'")
        Response.Write("<Error ServerDateTime=""" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & """>")
        Response.Write(" <Code>" & nCode & "</Code>")
        Response.Write(" <Message>" & nDesc & "</Message>")
        Response.Write("</Error>")
        Response.End()
    End Sub

    Private Sub OutputRecurringModifyLog()
        Server.ScriptTimeout = 240
        Dim sOut As New StringBuilder()
        Dim sWhere As String = " WHERE Merchant_id=" & nMerchantID & " AND InsertDate >= '" & dDateStart & "' AND InsertDate <= '" & dDateEnd & "'"
        Dim nCount As Object = dbPages.ExecScalar("SELECT COUNT(*) FROM [Log].[RecurringModify]" & sWhere, dbPages.DSN2, True)
        If nCount Is dbPages.NoDbConn Then ReturnResult(16, "The service is temporarily unavailable")
        Dim drData As SqlDataReader = dbPages.ExecReader("SELECT TOP 1000 * FROM [Log].[RecurringModify]" & sWhere & " ORDER BY RecurringModify_id ASC", dbPages.DSN2, 120, True)
        If drData Is Nothing Then ReturnResult(16, "The service is temporarily unavailable")
        sOut.Append("<RecurringModifyLog Count=""" & dbPages.TestVar(nCount, 0, -1, 0) & """ ReturnCount=""" & Math.Min(dbPages.TestVar(nCount, 0, -1, 0), 1000) & """ ServerDateTime=""" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & """>" & vbCrLf)
        While drData.Read()
            sOut.Append(" <RecurringModify>" & vbCrLf)
            sOut.Append("  <LogID>" & drData("RecurringModify_id") & "</LogID>" & vbCrLf)
            sOut.Append("  <DateTime>" & drData("InsertDate") & "</DateTime>" & vbCrLf)
            sOut.Append("  <Series>" & drData("RecurringSeries_id") & "</Series>" & vbCrLf)
            sOut.Append("  <Action>" & drData("Action") & "</Action>" & vbCrLf)
            sOut.Append("  <RequestString><![CDATA[" & drData("RequestString") & "]]></RequestString>" & vbCrLf)
            sOut.Append("  <RemoteIP>" & drData("IPAddress") & "</RemoteIP>" & vbCrLf)
            sOut.Append(" </RecurringModify>" & vbCrLf)
        End While
        sOut.Append("</RecurringModifyLog>" & vbCrLf)
        drData.Close()
        Response.Write(sOut.ToString())
    End Sub

    Private Sub OutputTransFail()
        Server.ScriptTimeout = 240
        Dim sOut As New StringBuilder()
        Dim sWhere As String = " WHERE t.CompanyID=" & nMerchantID & " AND t.InsertDate BETWEEN '" & dDateStart & "' And '" & dDateEnd & "'"
        Dim nCount As Object = dbPages.ExecScalar("SELECT Count(*) FROM tblCompanyTransFail t " & sWhere, dbPages.DSN2)
        If nCount Is dbPages.NoDbConn Then ReturnResult(16, "The service is temporarily unavailable")

        Dim drData As SqlDataReader = dbPages.ExecReader("SELECT TOP 1000 t.*, Name AS pm_Name, cc.email, cc.Member, cc.CCard_First6, cc.CCard_Last4," &
        " cc.BINCountry, cc.ExpMM, cc.ExpYY, cc.PersonalNumber, cc.phoneNumber, ba.address1, ba.address2, ba.city, zipCode, stateIso, countryIso" &
        " FROM tblCompanyTransFail t LEFT JOIN tblCreditCard cc ON t.CreditCardID=cc.ID LEFT JOIN List.PaymentMethod AS pm ON t.PaymentMethod = pm.PaymentMethod_id" &
        " LEFT JOIN tblBillingAddress ba ON cc.BillingAddressID=ba.ID " & sWhere & " ORDER BY t.ID", dbPages.DSN2, 120, True)
        If drData Is Nothing Then ReturnResult(16, "The service is temporarily unavailable")
        sOut.Append("<Transactions Count=""" & dbPages.TestVar(nCount, 0, -1, 0) & """ ReturnCount=""" & Math.Min(dbPages.TestVar(nCount, 0, -1, 0), 1000) & """ ServerDateTime=""" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & """>" & vbCrLf)
        While drData.Read()
            sOut.Append(" <Trans>" & vbCrLf)
            sOut.Append("  <ID>" & drData("ID") & "</ID>" & vbCrLf)
            sOut.Append("  <Date>" & drData("InsertDate") & "</Date>" & vbCrLf)
            sOut.Append("  <Currency>" & drData("Currency") & "</Currency>" & vbCrLf)
            sOut.Append("  <Amount>" & drData("Amount") & "</Amount>" & vbCrLf)
            sOut.Append("  <Email>" & drData("email") & "</Email>" & vbCrLf)
            sOut.Append("  <Phone>" & drData("phoneNumber") & "</Phone>" & vbCrLf)
            sOut.Append("  <PersonalNum>" & drData("PersonalNumber") & "</PersonalNum>" & vbCrLf)
            sOut.Append("  <Order>" & drData("OrderNumber") & "</Order>" & vbCrLf)
            sOut.Append("  <Comment>" & drData("Comment") & "</Comment>" & vbCrLf)
            sOut.Append("  <Reply>" & drData("replyCode") & "</Reply>" & vbCrLf)
            sOut.Append("  <CreditCard>" & vbCrLf)
            sOut.Append("   <Type>" & drData("pm_Name") & "</Type>" & vbCrLf)
            sOut.Append("   <BIN>" & drData("CCard_First6") & "</BIN>" & vbCrLf)
            sOut.Append("   <BINCountry>" & drData("BINCountry") & "</BINCountry>" & vbCrLf)
            sOut.Append("   <ExpMM>" & dbPages.TestVar(drData("ExpMM"), 0, -1, 0).ToString("00") & "</ExpMM>" & vbCrLf)
            sOut.Append("   <ExpYY>" & dbPages.TestVar(drData("ExpYY"), 0, -1, 0).ToString("00") & "</ExpYY>" & vbCrLf)
            sOut.Append("   <Last4>" & dbPages.TestVar(drData("CCard_Last4"), 0, -1, 0).ToString("0000") & "</Last4>" & vbCrLf)
            sOut.Append("   <Holder>" & drData("Member") & "</Holder>" & vbCrLf)
            sOut.Append("  </CreditCard>" & vbCrLf)
            sOut.Append("  <Address>" & vbCrLf)
            sOut.Append("   <Line1>" & drData("address1") & "</Line1>" & vbCrLf)
            sOut.Append("   <Line2>" & drData("address2") & "</Line2>" & vbCrLf)
            sOut.Append("   <City>" & drData("city") & "</City>" & vbCrLf)
            sOut.Append("   <Postal>" & drData("zipCode") & "</Postal>" & vbCrLf)
            sOut.Append("   <State>" & drData("stateIso") & "</State>" & vbCrLf)
            sOut.Append("   <Country>" & drData("countryIso") & "</Country>" & vbCrLf)
            sOut.Append("  </Address>" & vbCrLf)
            sOut.Append(" </Trans>" & vbCrLf)
        End While
        sOut.Append("</Transactions>" & vbCrLf)
        drData.Close()
        Response.Write(sOut.ToString())
    End Sub

    Private Sub OutputTransApproval()
        Server.ScriptTimeout = 240
        Dim sOut As New StringBuilder()
        Dim sWhere As String = " WHERE t.CompanyID=" & nMerchantID & " And t.InsertDate BETWEEN '" & dDateStart & "' AND '" & dDateEnd & "'"
        Dim nCount As Object = dbPages.ExecScalar("SELECT Count(*) FROM tblCompanyTransApproval t " & sWhere, dbPages.DSN2, True)
        If nCount Is dbPages.NoDbConn Then ReturnResult(16, "The service is temporarily unavailable")

        Dim drData As SqlDataReader = dbPages.ExecReader("SELECT TOP 1000 t.*, pm.Name AS pm_Name, cc.email, cc.Member, cc.CCard_First6, cc.CCard_Last4," &
        " cc.BINCountry, cc.ExpMM, cc.ExpYY, cc.PersonalNumber, cc.phoneNumber, ba.address1, ba.address2, ba.city, zipCode, stateIso, countryIso" &
        " FROM tblCompanyTransApproval t LEFT JOIN tblCreditCard cc ON t.CreditCardID=cc.ID LEFT JOIN List.PaymentMethod AS pm ON t.PaymentMethod = pm.PaymentMethod_id" &
        " LEFT JOIN tblBillingAddress ba ON cc.BillingAddressID=ba.ID " & sWhere & " ORDER BY t.ID", dbPages.DSN2, 120, True)
        If drData Is Nothing Then ReturnResult(16, "The service is temporarily unavailable")
        sOut.Append("<Transactions Count=""" & dbPages.TestVar(nCount, 0, -1, 0) & """ ReturnCount=""" & Math.Min(dbPages.TestVar(nCount, 0, -1, 0), 1000) & """ ServerDateTime=""" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & """>" & vbCrLf)
        While drData.Read()
            sOut.Append(" <Trans>" & vbCrLf)
            sOut.Append("  <ID>" & drData("ID") & "</ID>" & vbCrLf)
            sOut.Append("  <Date>" & drData("InsertDate") & "</Date>" & vbCrLf)
            sOut.Append("  <Currency>" & drData("Currency") & "</Currency>" & vbCrLf)
            sOut.Append("  <Amount>" & drData("Amount") & "</Amount>" & vbCrLf)
            sOut.Append("  <Approval>" & drData("ApprovalNumber") & "</Approval>" & vbCrLf)
            sOut.Append("  <Email>" & drData("email") & "</Email>" & vbCrLf)
            sOut.Append("  <Phone>" & drData("phoneNumber") & "</Phone>" & vbCrLf)
            sOut.Append("  <PersonalNum>" & drData("PersonalNumber") & "</PersonalNum>" & vbCrLf)
            sOut.Append("  <Type>Pre-auth</Type>" & vbCrLf)
            sOut.Append("  <Order>" & drData("OrderNumber") & "</Order>" & vbCrLf)
            sOut.Append("  <RecurringCharge>" & drData("RecurringChargeNumber") & "</RecurringCharge>" & vbCrLf)
            sOut.Append("  <RecurringSeries>" & drData("RecurringSeries") & "</RecurringSeries>" & vbCrLf)
            sOut.Append("  <Comment>" & drData("Comment") & "</Comment>" & vbCrLf)
            sOut.Append("  <Reply>000</Reply>" & vbCrLf)
            sOut.Append("  <CreditCard>" & vbCrLf)
            sOut.Append("   <Type>" & drData("pm_Name") & "</Type>" & vbCrLf)
            sOut.Append("   <BIN>" & drData("CCard_First6") & "</BIN>" & vbCrLf)
            sOut.Append("   <BINCountry>" & drData("BINCountry") & "</BINCountry>" & vbCrLf)
            sOut.Append("   <ExpMM>" & dbPages.TestVar(drData("ExpMM"), 0, -1, 0).ToString("00") & "</ExpMM>" & vbCrLf)
            sOut.Append("   <ExpYY>" & dbPages.TestVar(drData("ExpYY"), 0, -1, 0).ToString("00") & "</ExpYY>" & vbCrLf)
            sOut.Append("   <Last4>" & dbPages.TestVar(drData("CCard_Last4"), 0, -1, 0).ToString("0000") & "</Last4>" & vbCrLf)
            sOut.Append("   <Holder>" & drData("Member") & "</Holder>" & vbCrLf)
            sOut.Append("  </CreditCard>" & vbCrLf)
            sOut.Append("  <Address>" & vbCrLf)
            sOut.Append("   <Line1>" & drData("address1") & "</Line1>" & vbCrLf)
            sOut.Append("   <Line2>" & drData("address2") & "</Line2>" & vbCrLf)
            sOut.Append("   <City>" & drData("city") & "</City>" & vbCrLf)
            sOut.Append("   <Postal>" & drData("zipCode") & "</Postal>" & vbCrLf)
            sOut.Append("   <State>" & drData("stateIso") & "</State>" & vbCrLf)
            sOut.Append("   <Country>" & drData("countryIso") & "</Country>" & vbCrLf)
            sOut.Append("  </Address>" & vbCrLf)
            sOut.Append(" </Trans>" & vbCrLf)
        End While
        sOut.Append("</Transactions>" & vbCrLf)
        drData.Close()
        Response.Write(sOut.ToString())
    End Sub

    Private Sub OutputTransPass()
        Dim nTransType As Integer = dbPages.TestVar(Request("TransType"), 1, 0, 0)
        Dim sTransType As String = IIf(nTransType <= 0, "DEFAULT", nTransType)
        Server.ScriptTimeout = 120
        Dim sDateStart As String = IIf(nTransID > 0, "NULL", "'" & dDateStart & "'"), sDateEnd As String = IIf(nTransID > 0, "NULL", "'" & dDateEnd & "'")
        Dim sSQL As String = "SELECT dbo.DataPullingGetTransCount(" & nMerchantID & ", " & sDateStart & ", " & sDateEnd & ", " & sTransType & ", " & sTransID & ")"
        Dim sDSN As String = IIf(nTransID > 0, dbPages.DSN, dbPages.DSN2)
        Dim oCount As Object = dbPages.ExecScalar(sSQL, sDSN)
        If oCount Is dbPages.NoDbConn Then ReturnResult(16, "The service is temporarily unavailable")
        Dim nCount As Integer = dbPages.TestVar(oCount, 0, -1, 0)
        sSQL = "EXEC spDataPullingGetTransXML " & nMerchantID & ", " & sDateStart & ", " & sDateEnd & ", " & sTransType & ", " & nTransID
        Dim drData As SqlDataReader = dbPages.ExecReader(sSQL, sDSN, 120)
        If drData Is Nothing Then ReturnResult(16, "The service is temporarily unavailable")
        Try
            Dim sbOut As New StringBuilder()
            sbOut.Append("<Transactions Count=""" & nCount & """ ReturnCount=""" & nCount & """ ServerDateTime=""" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & """ Server=""" & My.Computer.Name & """>" & vbCrLf)
            While drData.Read()
                sbOut.Append(drData(0))
            End While
            sbOut.Append("</Transactions>" & vbCrLf)
            Response.Write(sbOut.ToString())
        Finally
            drData.Close()
        End Try
    End Sub
</script>