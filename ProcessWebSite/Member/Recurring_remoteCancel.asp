<!--#include file="../include/func_adoConnect.asp"-->
<%
Function returnResponse (fResponseCode, fTransID, insertDate, fResponseDesc)
	response.Clear
	call closeConnection()
	If request("ReplyURL") <> "" Then
		response.redirect(request("ReplyURL") & "?Reply=" & fResponseCode & "&TransID=" & fTransID & "&InsertDate=" & insertDate & "&ResponseDesc=" & fResponseDesc) 
	Else
		response.write fResponseCode & ";" & fTransID & ";" & insertDate & ";" & fResponseDesc) 
	End If
	response.End
End function

'Getting data
x_companyID = TestNumVar(Request("CompanyID"), 0, -1, -1)
x_companyNum = TestNumVar(DBText(request("CompanyNum")), 0, -1, -1)
x_securityKey = DBText(request("SecurityKey"))
x_transID = TestNumVar(DBText(request("TransID")), 0, -1, -1)	

'Checking data
If x_companyID = -1 AND x_companyNum = -1 Then Call returnResponse("1", "", "", "Missing or invalid company number")
If x_securityKey = "" Then Call returnResponse("2", "", "", "Missing identifier key")
If x_transID = -1 Then Call returnResponse("3", "", "", "Missing or invalid transaction number")
If NOT x_companyNum = -1 Then
	x_companyID = ExecScalar("SELECT id FROM tblCompany WHERE CustomerNumber = '" & x_companyNum & "' AND SecurityKey='" & x_securityKey & "'", 0)
    If x_companyID = 0 Then Call returnResponse("1", "", "", "Invalid company number or identifier key")
End If

'Check if recurring exist
Set rsData = Server.CreateObject("Adodb.Recordset")
sSQL = "SELECT tblCompanyRecurringCc.RecurringLeft FROM tblCompanyRecurringCc LEFT JOIN tblCompanyChargeAdmin ON (tblCompanyRecurringCc.CompanyID = tblCompanyChargeAdmin.company_id) " & _
       "WHERE tblCompanyRecurringCc.CompanyTransPass_ID = " & iTransID & " AND tblCompanyRecurringCc.CompanyID = " & iCompanyID
set rsData = oledbData.execute(sSQL)
If rsData.EOF Then
    rsData.Close
    Set rsData = Nothing
    Call returnResponse("1", "", "", "TransID was not found")
End if
rsData.Close
Set rsData = Nothing

'If (Not IsNull(rsData("recurringReplyUrl"))) And (rsData("recurringReplyUrl") <> "") Then
'	Set mHttpRequest = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
'	mHttpRequest.Open "GET", rsData("recurringReplyUrl") & "&Reply=" & 600 & "&TransID=" & iTransID, true
'	Set mHttpRequest = Nothing
'End If

'Cancel recurring
sSQL = "UPDATE tblCompanyRecurringCc SET RecurringCount = RecurringCount-RecurringLeft, RecurringLeft=0, NextTransTime=0 " & _
	   "WHERE CompanyTransPass_ID = " & iTransID & " AND CompanyID = " & iCompanyID
oledbData.execute(sSQL)

Call returnResponse("0", "", "", "Success")
%>
