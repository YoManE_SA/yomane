<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_mail.asp"-->
<!--#include file="../include/func_controls.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/InfrastructureErrors.inc" -->
<%
Dim counryId, stateId
Sub SendResponse(xErr, xCstNum, xMail, xDesc)
	Response.Write("ReplyCode=" & xErr & "&ReplyDesc=" & xDesc & "&CustomerNum=" & xCstNum & "&Email=" & xMail)
	Response.End
	'If Trim(Request("ReplyURL")) <> "" Then
	'	Response.Redirect Trim(Request("ReplyURL")) & "?Reply=" & xErr & "&CustomerNum=" & xCstNum & "&Email=" & xMail & "&Desc=" & xDesc
	'Else
	'	Response.Write xErr & xCstNum & xMail & xDesc
	'	Response.End
	'End if	
End Sub

CountryCode = Trim(Request("Country"))
StateCode = Trim(request("State"))

If CountryCode <> "" Then
	If IsNumeric(CountryCode) Then
		CountryCode = ExecScalar("SELECT CountryISOCode FROM [List].[CountryList] WHERE [CountryID]=" & CountryCode, "")
		If CountryCode = "" Then SendResponse 20, "0000000", "", "Invalid country"
	End If
Else
	SendResponse 19, "0000000", "", "Country is missing"
End If

If StateCode <> "" Then
	If IsNumeric(StateCode) Then
		StateCode = ExecScalar("SELECT StateISOCode FROM [List].[StateList] WHERE stateID=" & StateCode, "")
		If StateCode = "" Then SendResponse 20, "0000000", "", "Invalid State"
	End If
End If

'call web serice
	Dim ParamList, UrlAddress, expDate, responseXml
	expDate = DateSerial(TestNumVar(Request("ExpYear"), 0, -1, 1901), TestNumVar(Request("ExpMonth"), 1, 12, 1), 1)
	expDate = DateAdd("d", -1, DateAdd("m", 1, expDate))
	ParamList = _
		"<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">" & vbCrLf & _
		"  <soap12:Body>" & vbCrLf & _
		"	<RegisterCustomer xmlns=""http://netpay-intl.com/"">" & vbCrLf & _
		"	  <data>" & vbCrLf & _
		"		<info>" & vbCrLf & _
		"        <FirstName>" & Request("FirstName") & "</FirstName>" & vbCrLf & _
		"        <LastName>" & Request("LastName") & "</LastName>" & vbCrLf & _
		"        <PersonalNumber>" & Request("PersonalNum") & "</PersonalNumber>" & vbCrLf & _
		"        <PhoneNumber>" & Request("Phone") & "</PhoneNumber>" & vbCrLf & _
		"        <CellNumber>" & Request("CellPhone") & "</CellNumber>" & vbCrLf & _
		"        <EmailAddress>" & Request("Mail") & "</EmailAddress>" & vbCrLf & _
		"        <AddressLine1>" & Request("Street") & "</AddressLine1>" & vbCrLf & _
		"        <City>" & Request("City") & "</City>" & vbCrLf & _
		"        <PostalCode>" & Request("ZIP") & "</PostalCode>" & vbCrLf & _
		"        <StateIso>" & StateCode & "</StateIso>" & vbCrLf & _
		"        <CountryIso>" & CountryCode & "</CountryIso>" & vbCrLf & _
		"       </info>" & vbCrLf & _
		"       <PinCode>" & Request("PinCode") & "</PinCode>" & vbCrLf & _
		"       <Password>" & sPassword & "</Password>" & vbCrLf
	If Request("CardNum") <> "" Then
		ParamList = ParamList & _
		"       <StoredPaymentMethods>" & vbCrLf & _
        "         <WalletStoredPaymentMethod>" & vbCrLf & _
        "          <PaymentMethodKey>22</PaymentMethodKey>" & vbCrLf & _
        "          <OwnerName>" & Request("Member") & "</OwnerName>" & vbCrLf & _
        "          <ExpirationDate>" & Year(expDate) & "-" & IIF(Month(expDate) < 10, "0", "") & Month(expDate) & "-" & IIF(Day(expDate) < 10, "0", "") & Day(expDate) & "T23:59:59" & "</ExpirationDate>" & vbCrLf & _
        "          <IsDefault>true</IsDefault>" & vbCrLf & _
        "          <AccountValue1>" & Request("CardNum") & "</AccountValue1>" & vbCrLf & _
        "          <AccountValue2>" & Request("CVV2") & "</AccountValue2>" & vbCrLf & _
        "         </WalletStoredPaymentMethod>" & vbCrLf & _
		"       </StoredPaymentMethods>" & vbCrLf
	End If
	ParamList = ParamList & _
		"	  </data>" & vbCrLf & _
		"	</RegisterCustomer>" & vbCrLf & _
		"  </soap12:Body>" & vbCrLf & _
		"</soap12:Envelope>" & vbCrLf

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	'On Error Resume Next
		HttpReq.open "POST", WEBSERVICES_URL & "wallet.asmx", false
		HttpReq.setRequestHeader "Content-Type", "application/soap+xml"
		HttpReq.send ParamList
		'Response.Write(WEBSERVICES_URL & "wallet.asmx" & vbCrLf & "<br/>Params: " & vbCrLf & ParamList & vbCrLf & "<br/>Response: " & vbCrLf & HttpReq.responseText)
		Set responseXml = Server.CreateObject("Microsoft.XMLDOM")
		responseXml.LoadXML HttpReq.responseText
		Dim resCode, resNumber, resKey
		resCode = responseXml.selectSingleNode("//Code").text
		resKey = responseXml.selectSingleNode("//Key").text
		If (Not responseXml.selectSingleNode("//Number") Is Nothing) Then resNumber = responseXml.selectSingleNode("//Number").text
	On Error Goto 0
	Select Case resKey
	Case "Success" : resCode = "00"
	Case "Invalid_FirstName" : resCode = "15"
	Case "Invalid_LastName" : resCode = "16"
	Case "Invalid_Address" : resCode = "17"
	Case "Invalid_City" : resCode = "18"
	Case "Invalid_Country" : resCode = "20"
	Case "Invalid_Phone" : resCode = "22"
	Case "Invalid_Email" : resCode = "23"
	Case "EmailAlreadyExist" : resCode = "33"
	Case "PasswordWeak" : resCode = "45"
	Case "PinCodeWeak" : resCode = "46"

	Case "Invalid_AccountValue1" : resCode = "40"
	Case "Invalid_AccountValue2" : resCode = "40"
	Case "Invalid_ExpDate" : resCode = "41"
	Case "Invalid_OwnerName" : resCode = "43"
	Case "Invalid_SSN" : resCode = "44"
	End Select
	SendResponse resCode, resNumber, Request("Mail"), resKey

Response.End()

'****************************************************OLD CODE*******************************************************'
Dim CompanyID : CompanyID = 0
Dim sPassword, CstID, sCustomerNumber

If Trim(Request("CompanyNum")) = "" then
	SendResponse 10, "0000000", "", "Merchant number is missing"
Else
	If Not IsNumeric(Request("CompanyNum")) Or Len(Trim(Request("CompanyNum"))) < 7 then
		SendResponse 11, "0000000", "", "Invalid merchant number"
	Else
		Set iRs = Server.CreateObject("Adodb.Recordset")
		iRs.Open "SELECT ID, ActiveStatus, IsAutoPersonalSignup FROM tblCompany WHERE CustomerNumber='" & Replace(Request("CompanyNum"), "'", "''") & "'", oleDbData, 0, 1 
		If Not iRs.EOF Then
			CompanyID = TestNumVar(iRs("ID"), 0, -1, 0)
			ActiveStatus = TestNumVar(iRs("ActiveStatus"), 0, -1, CMPS_NEW)
			IsAutoPersonalSignup = iRs("IsAutoPersonalSignup")
		End If
		iRs.Close
		If CompanyID = 0 Then
			SendResponse 12, "0000000", "", "Merchant does not exist"
		Else
			If ActiveStatus <= CMPS_MAXCLOSED Then
				SendResponse 13, "0000000", "", "Merchant closed or inactive"
			ElseIf Not IsAutoPersonalSignup Then
				SendResponse 14, "0000000", "", "Merchant is not allowed to use remote registration"
			End If	
		End if
	End If

	If Trim(Request("FirstName")) = "" Then
		SendResponse 15, "0000000", "", "First name is missing or invalid"
	ElseIf Trim(Request("LastName")) = "" Then
		SendResponse 16, "0000000", "", "Surname is missing or invalid"
	ElseIf Trim(Request("Street")) = "" Then
		SendResponse 17, "0000000", "", "Street & house number is missing or invalid"
	ElseIf Trim(Request("City")) = "" Then
		SendResponse 18, "0000000", "", "City is missing or invalid"
	ElseIf Trim(Request("Phone")) = "" Then
		SendResponse 21, "0000000", "", "Phone is missing"
	ElseIf Len(Trim(request("Phone"))) < 9 Then
		SendResponse 22, "0000000", "", "Invalid phone"
	ElseIf Trim(Request("Mail")) = "" Then
		SendResponse 23, "0000000", "", "Email is missing or invalid"
	ElseIf Trim(Request("IsRead")) <> "1" Then
		SendResponse 32, "0000000", "", "Invalid end user license agreement"
	ElseIf ExecScalar("Select Customer_ID From Data.Customer Where EmailAddress='" & Replace(Request("Mail"), "'", "''") & "'", 0) > 0 Then
		SendResponse 33, "0000000", "", "Email already exists"
	End If
	If Request("Password") <> "" Then
		sPassword = Request("Password")
		If Not CheckPasswordPolicy(sPassword) Then _
			SendResponse 45, "0000000", "", "Password strength too low"
	Else 
		'sPassword = GetRandomNum(10)
	End If
End If

'Check credit card data ----------------------------------------------------------
bIsCustomerCC = False
If Trim(Request("CardNum")) <> "" Then
	If Not IsNumericString(Request("CardNum"), 8, 19) Then
		SendResponse 40, "0000000", "", "Credit card - invalid number"
	ElseIf Not IsNumericString(Request("ExpMonth"), 1, 2) Then
		SendResponse 41, "0000000", "", "Credit card - month expiration is missing or invalid"
	ElseIf Not IsNumericString(Request("ExpYear"), 1, 2) Then
		SendResponse 42, "0000000", "", "Credit card - year expiration is missing or invalid"
	ElseIf Trim(Request("Member")) = "" Then
		SendResponse 43, "0000000", "", "Credit card - card holder name is missing or invalid"
	ElseIf Not IsNumericString(Request("PersonalNum"), 9, 9) Then
		SendResponse 44, "0000000", "", "Credit card - invalid card holder ID"
	End If
	bIsCustomerCC = True
End if

'---------------------------------------------------------------------------------------------------------------------
	sIp = Request.ServerVariables("REMOTE_ADDR")
	Randomize
	Do
		sCustomerNumber = GetRandomNum(7)
		xID = TestNumVar(ExecScalar("SELECT ID FROM tblCustomer WHERE CustomerNumber='" & Replace(sCustomerNumber, "'", "''") & "'", 0), 0, -1, 0)
	Loop While xID > 0

	Dim pinCode : pinCode = CInt(Rnd() * 9999)
	while (Len(pinCode) < 4) 
		pinCode = "0" & pinCode
	Wend

	sSQL = "INSERT INTO tblCustomer(PinCode, FirstName, LastName, Street, City, ZIP, Country, State, Phone, CellPhone, Mail, CustomerFrom, IsRead, IpOnReg, CustomerNumber, fromCompanyID)VALUES('" & _
		pinCode & "','" & DBText(TestStrVar(Request("FirstName"), 0, 200, "")) & "', '" & DBText(TestStrVar(Request("LastName"), 0, 200, "")) & "', '" & DBText(TestStrVar(Request("Street"), 0, 500, "")) & "', '" & DBText(TestStrVar(Request("City"), 0, 50, "")) & "', '" & DBText(TestStrVar(Request("ZIP"), 0, 25, "")) & "', " & _
		counryId & ", " & stateId & ", '" & DBText(TestStrVar(Request("Phone"), 0, 50, "")) & "', '" & DBText(TestStrVar(Request("CellPhone"), 0, 50, "")) & "', '" & DBText(TestStrVar(Request("Mail"), 0, 50, "")) & "', '" & _
		DBText(TestStrVar(Request("CustomerFrom"), 0, 50, "")) & "', " & TestNumVar(Request("IsRead"), 0, 1, 0) & ", '" & Trim(sIp) & "', '" & DBText(sCustomerNumber) & "', " & CompanyID & ")"
	oledbData.Execute sSQL
	CstID = ExecScalar("SELECT ID FROM tblCustomer WHERE CustomerNumber='" & DBText(sCustomerNumber) & "'", 0)
	UpdatePasswordChange 1, CstID, sPassword
	
	If bIsCustomerCC Then 'Insert credit card data
		sCardNum = Fn_CleanCCNumber(DBText(Trim(Request("CardNum"))))
		ccTypeID = Left(Fn_ccTypeAndRegion(sCardNum), 1)
		sSQL = "INSERT INTO tblCustomerCC(CustomerID, CH_PersonalNumber, CCard_number256, CCard_TypeID, CCard_ExpMM, CCard_ExpYY, CH_Fullname, CCard_Cui) VALUES " &_
			   "(" & CstID & ", '" & DBText(TestStrVar(Request("PersonalNum"), 0, 50, "")) & "', dbo.GetEncrypted256('" & Replace(sCardNum, "'", "''") & "'), " & ccTypeID & ", '" & TestNumVar(Request("ExpMonth"), 1, 12, 1) & "', '" & TestNumVar(Request("ExpYear"), 0, -1, 0) & "', '" & DBText(TestStrVar(Request("Member"), 0, 100, "")) & "', dbo.SetCUI('" & DBText(Trim(Request("CVV2"))) & "'))"
		oledbData.Execute sSQL
	End if
    ExecSql("Insert Into EventPending(Customer_id, EventPendingType_id, Parameters, TryCount)Values(" & CstID & "," & PET_WalletRegisterEmail & ",'', 3)")

Call closeConnection()
SendResponse "00", sCustomerNumber, Request("Mail"), "Registration completed successfully"
%>