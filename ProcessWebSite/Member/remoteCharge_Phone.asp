<%
Dim nTransactionID, nInsertDate, X_DPRET, X_SMSShortCode, isTestTerminal
%>
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/timer_class.asp"-->
<!--#include file="../include/MerchantTerminals.asp" -->
<!--#include file="remoteCharge_Functions.asp"-->
<%
Dim phoneNumber, TransCat, X_RetURL, X_RetSMSInstruction
Dim sTermName, sAccountId, sAccountSubId, sAccountPassword, sAccountId3D, sAccountSubId3D, sAccountPassword3D, X_CustomErrorDesc
Dim debitCompany, terminalActive, bIsShvaMasterTerminal, nProcessingMethod, sMcc, isManipulateAmount, bIsValidateEmail, bIsValidateFullName
Dim X_RefTransID, X_DebitReferenceCode, X_DebitReferenceNum, X_ApprovalNumber, ccTypeEngShow, X_Payments, isTestOnly, ccTypeID
Dim X_TerminalHasNext, X_TransINC, X_3dRedirect, X_MessageDirection, merchantUrl, X_deviceId

'20100530 Tamir - when capturing pre-auth. transaction from recurring series, copy RecurringSeries and RecurringCharge to the passed transaction
nApprovalRecurringSeries = "NULL"
nApprovalRecurringChargeNumber = "NULL"

companyNum = DBText(request("CompanyNum"))
sIP = DBText(request("ClientIP"))
If sIP = "" Then sIP = Request.ServerVariables("REMOTE_ADDR")

X_Payments = 1
isTestOnly = 0
PaymentMethod = TestNumVar(request("PaymentMethod"), PMD_PD_MIN, PMD_PD_MAX, PMD_PD_MIN)
requestSource = DBText(request("RequestSource"))
If requestSource = "" Then requestSource = "8"
fraudDetectionLog_id = 0 : X_TransType = 0 : sTransactionTypeID = requestSource
X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, MAX_CURRENCY + 1)

Set iRs = oleDbData.Execute("Select Name, IsPMInfoMandatory From List.PaymentMethod Where PaymentMethod_id = " & PaymentMethod)
If Not iRs.EOF Then
	ccTypeEngShow = iRs("Name")
	IsCheckPMData = iRs("IsPMInfoMandatory")
End If
iRs.Close
If ccTypeEngShow = "" Then ccTypeEngShow = "PhoneDebit"
X_deviceId = TestNumVar(Request("deviceId"), 0, -1, "")

'---------------------------------------------
'   save charge attempt log
'---------------------------------------------
LogSavingChargeDataID = Sub_LogSavingChargeData(requestSource, companyNum, sIP)

'---------------------------------------------
'	Get and secure input data
'---------------------------------------------
referringUrl = DBText(request("ReferringUrl"))
If trim(referringUrl) = "" Then referringUrl = DBText(Request.ServerVariables("HTTP_REFERER"))

sCompanyID = ExecScalar("SELECT ID FROM tblCompany WHERE CustomerNumber='" & companyNum & "'", 0)
If sCompanyID = 0 Then ThrowError "500"
X_TransType = TestNumVar(request("TransType"), 0, 4, 0)
X_RefTransID = 0
ccTypeID = 10

If trim(X_TransType)="4" Then ' 07/04/2010 Udi added the echeck load from pending
	sTransactionTypeID = "17"
	X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, -1)
	X_RefTransID = TestNumVar(request("RefTransID"), 0, -1, 0)
	X_ConfirmNumber = request("TransApprovalID")
	If X_RefTransID < 1 And X_ConfirmNumber = "" Then Call throwError("536")
	X_Amount = TestNumVar(request("Amount"), 0, -1, 0)
	bIsUseMaxMind = false 'Overide merchant configuration
	bIsCheckDailyLimts = False

	sSQL="SELECT t.ID, trans_Amount, trans_Currency," & _
	" trans_Payments, trans_CreditType, OrderNumber, t.Comment, DebitReferenceCode, DebitReferenceNum, DebitApprovalNumber, TerminalNumber," & _
	" PhoneNumber, BillingAddressID, " & _
	" PersonalNumber, phoneNumber, Email, address1, address2, city," & _
	" zipCode, tblBillingAddress.stateIso, tblBillingAddress.countryIso, BillingAddressID, PaymentMethod, PaymentMethodID FROM tblCompanyTransPending t" & _
	" INNER JOIN Trans.PhoneDetail ON PhoneDetailsID=PhoneDetail.PhoneDetail_id Left JOIN tblBillingAddress ON BillingAddressId=tblBillingAddress.ID" & _
	" WHERE " & IIF(X_ConfirmNumber <> "", " t.DebitApprovalNumber='" & X_ConfirmNumber & "'", " t.ID=" & X_RefTransID) & " AND t.CompanyID=" & sCompanyID
	
	set rsData3 = oledbData.execute(sSQL)
	If rsData3.EOF Then
		rsData3.Close
		Call throwError("536")
	ElseIf X_Amount > rsData3("trans_Amount") Then
		rsData3.Close
		Call throwError("538")
	Else
		'nExistingBillingAddressID=TestNumVar(rsData3("BillingAddressID"), 1, 0, 0)
		'nExistingCreditCardID=TestNumVar(rsData3("BillingAddressID"), 1, 0, 0)
		If X_Amount = 0 Then X_Amount = rsData3("trans_Amount")
		PaymentMethod = TestNumVar(rsData3("PaymentMethod"), PMD_EC_MIN, PMD_EC_MAX, PMD_EC_CHECK)
		X_Currency = rsData3("trans_Currency")
	    X_OCurrency = X_Currency
        X_OAmount = X_Amount
    	RecurringSeries = "NULL"
    	nApprovalRecurringSeries = "NULL"
        X_HolderName = rsData3("FullName")
    	X_RefTransID = rsData3("ID")
		X_OrderNumber = rsData3("OrderNumber")
		X_PersonalNumber = trim(rsData3("PersonalNumber"))
		X_PhoneNumber = trim(rsData3("phoneNumber"))
		X_Email = trim(rsData3("Email"))
		X_Payments = TestNumVar(rsData3("trans_Payments"), 0, -1, 1)
		X_ConfirmNumber = trim(rsData3("DebitApprovalNumber"))
		X_Customer = 0
		X_TypeCredit = TestNumVar(rsData3("trans_CreditType"), 0, 8, -1)
		X_Comment = trim(rsData3("Comment"))
		X_DebitReferenceCode = trim(rsData3("DebitReferenceCode"))
		X_DebitReferenceNum = trim(rsData3("DebitReferenceNum"))
		sTerminalNumber = trim(rsData3("TerminalNumber"))
		BACHAddr1 = trim(rsData3("address1"))
		BACHAddr2 = trim(rsData3("address2"))
		BACity = trim(rsData3("city"))
		BAZipCode = trim(rsData3("zipCode"))
		BAState = trim(rsData3("stateIso"))
		BACountry = trim(rsData3("countryIso"))

		' 20090706 Tamir - when capturing pre-auth. transaction from recurring series, copy RecurringSeries and RecurringCharge to the passed transaction
		if sTerminalNumber <> "" Then
			bIsCheckTerminalUsabe = false
			sTerminalChargeOptions = rsData3("trans_Currency")
			CCF_ID = 0
		End If
	End if
	rsData3.close
	Set rsData3 = nothing
Else
    X_Amount = TestNumVar(Request("Amount"), 0, -1, 0)
    X_Currency = TestNumVar(Request("Currency"), 0, -1, 1)
    X_OCurrency = X_Currency
    X_OAmount = X_Amount

    X_HolderName = DBText(Request("FullName"))
    X_PhoneNumber = DBText(Request("PhoneNumber"))
    X_PersonalNumber = DBText(Request("PersonalNum"))
    X_Email = DBText(Request("Email"))
    X_Customer = TestNumVar(Request("CustomerId"), 0, -1, 0)
    replyUrl = DBText(Request("ReplyUrl"))
    X_Comment = DBText(Request("Comment"))
    X_OrderNumber = DBText(Request("Order"))
    X_TypeCredit = TestNumVar(Request("CreditType"), 0, 8, 1)
    X_RetURL = DBText(Request("RetURL"))
    X_BillingType = TestNumVar(Request("BillingType"), 0, -1, 0)
    BACHAddr1 = DBText(Request("BillingAddress1"))
    BACHAddr2 = DBText(Request("BillingAddress2"))
    BACity = DBText(Request("BillingCity"))
    BAZipCode = DBText(Request("BillingZipCode"))
    BAState = DBText(Request("BillingState"))
    BACountry = DBText(Request("BillingCountry"))
    If X_BankCountry = "" And BACountry <> "" Then X_BankCountry = BACountry
End If
'---------------------------------------------
'   validation
'---------------------------------------------
'empty field validation

isMissingData = (X_Amount = "") or (X_HolderName = "") or (companyNum = "") or (X_Currency = "")
If isMissingData Then call throwError("506")

'data type validation 
isTypeMismatch = (not isNumeric(X_Amount)) or (not isNumeric(X_Currency)) or (Not IsNumeric(companyNum))
If isTypeMismatch then call throwError("590")

'data length validation
isFieldTooLong = (len(X_HolderName) > 50) or (len(X_PersonalNumber) > 20) or (len(X_PhoneNumber) > 20) or (len(X_Email) > 255) or (len(X_Comment) > 500) or (len(X_Currency) > 3) or (len(companyNum) > 10) or (len(X_Amount) > 12)
If isFieldTooLong then call throwError("591")

'--------------------------------------------------------
'	Get and check company and terminal data
'--------------------------------------------------------
Dim rsCmp, sCompanyID, sCompanyName, CCF_ID, sTerminalNumber, sTerminalChargeOptions, Protect5mins
Set rsCmp = server.createobject("Adodb.Recordset")
rsCmp.Open "SELECT tblCompany.*, mrs.IsEnabled as AllowRecurring FROM tblCompany WITH (NOLOCK) LEFT JOIN tblMerchantRecurringSettings mrs WITH (NOLOCK) ON tblCompany.ID=mrs.MerchantID WHERE ID='" & sCompanyID & "'", oledbData, 0, 1
if Not rsCmp.EOF Then
	' 20080619 Tamir - implement recurring charges by echeck
	bUseOldTerminal = Not rsCmp("IsUsingNewTerminal")
	X_RecurringCount = dbText(request("RecurringCount")) 'number of charges
	X_RecurringCycle = dbText(request("RecurringCycle")) 'interval length
	X_RecurringMode = dbText(request("RecurringMode"))	 'interval unit: 1=day, 2=week, 3=month, 4=year
    merchantUrl = rsCmp("URL")
	' 20080615 Tamir - recurring mode can be also a word or its first letter (case insensitive)
	if X_RecurringMode<>"" then
		if not IsNumeric(TestNumVar(X_RecurringMode, 1, 4, "A")) then
			if StrComp(X_RecurringMode, "DAY", 1)=0 or StrComp(X_RecurringMode, "D", 1)=0 then
				X_RecurringMode=1
			elseif StrComp(X_RecurringMode, "WEEK", 1)=0 or StrComp(X_RecurringMode, "W", 1)=0 then
				X_RecurringMode=2
			elseif StrComp(X_RecurringMode, "MONTH", 1)=0 or StrComp(X_RecurringMode, "M", 1)=0 then
				X_RecurringMode=3
			elseif StrComp(X_RecurringMode, "YEAR", 1)=0 or StrComp(X_RecurringMode, "Y", 1)=0 then
				X_RecurringMode=4
			else
				Call throwError("551") 'Recurring parameters incomplete
			end if
		end if
	end if
	If trim(X_RecurringCount)<>"" OR trim(X_RecurringCycle)<>"" OR trim(X_RecurringMode)<>"" Then
		If NOT rsCmp("AllowRecurring") Then Call throwError("550") 'recurring transaction is not allowed to the merchant
		If trim(X_RecurringCount)="" OR trim(X_RecurringCycle)="" OR  trim(X_RecurringMode)="" Then Call throwError("551") 'Recurring parameters incomplete
		If NOT IsNumeric(X_RecurringCount) OR  NOT IsNumeric(X_RecurringCycle) OR  NOT IsNumeric(X_RecurringMode) Then Call throwError("551") 'Recurring parameters incomplete
	End if
	sCompanyName = rsCmp("CompanyName")
    sIsBillingAddressMust = trim(rsCmp("IsBillingAddressMust"))
    Protect5mins = rsCmp("IsMultiChargeProtection")
	if TestNumVar(rsCmp("ActiveStatus"), 0, -1, CMPS_NEW) < CMPS_INTEGRATION Then	Call throwError("501")  '��� ���� �� ����
    '--------------- check merchant permissions by request source
    select case requestSource
       case "5", "7", "18", "27"
           'manual charge / system charge (admin)
           'if not rsCmp("IsSystemPayEcheck") then call throwError("502")
           'if requestSource <> "18" then
           '    if rsCmp("IsSystemPayPersonalNumber") and X_PersonalNumber = "" then call throwError("513")
           'end if
		    'No matter from where - we dont require personal id for eCheck (us customers)
           if rsCmp("IsSystemPayPhoneNumber") and X_PhoneNumber = "" then call throwError("514")
           if rsCmp("IsSystemPayEmail") and X_Email = "" then call throwError("515")
       case "9", "11"
           'popup charge english, popup charge hebrew
		    if not rsCmp("IsCustomerPurchaseEcheck") then call throwError("502")
		    if requestSource = "11" then
			    if rsCmp("IsCustomerPurchasePersonalNumber") and X_PersonalNumber = "" then call throwError("513")
		    end if
		    if rsCmp("IsCustomerPurchasePhoneNumber") and X_PhoneNumber = "" then call throwError("514")
		    if rsCmp("IsCustomerPurchaseEmail") and X_Email = "" then call throwError("515")
       Case "32", "33", "40"
			Dim hpsMerchant : Set hpsMerchant = New HostedPageSettings
			hpsMerchant.LoadMerchant sCompanyID, "IsPhoneDebit"
			If Not hpsMerchant.IsActive Then ThrowError "502"
			If hpsMerchant.IsRequiredEmail And X_Email = "" Then ThrowError "515"
			If hpsMerchant.IsRequiredPhone And X_PhoneNumber = "" Then ThrowError "514"
			'If hpsMerchant.IsRequiredID And X_PersonalNumber = "" Then ThrowError "513"
			Set hpsMerchant = Nothing
       case "12"
           'public charge - not implemented for now
           'if Not rsCmp("IsPublicPayEcheck") then call throwError("502")
           if rsCmp("IsPublicPayPersonalNumber") and X_PersonalNumber = "" then call throwError("513")
           if rsCmp("IsPublicPayPhoneNumber") and X_PhoneNumber = "" then call throwError("514")
           if rsCmp("IsPublicPayEmail") and X_Email = "" then call throwError("515")
       case "", "8"
           'remote charge
           'if not rsCmp("IsRemoteChargeEcheck") then call throwError("502")
           'if rsCmp("IsRemoteChargeEchPersonalNumber") and X_PersonalNumber = "" then call throwError("513")
           'if rsCmp("IsRemoteChargeEchPhoneNumber") and X_PhoneNumber = "" then call throwError("514")
           'if rsCmp("IsRemoteChargeEchEmail") and X_Email = "" then call throwError("515")
		case "20"
			'recurring page
			sIsAllow3DTrans = false
       case else
           'invalid request source
           call throwError("526")
    End select
  End if
  rsCmp.Close
If sCompanyID = 0 Then	Call throwError("500") '���� ��� �� ���� - �� ���� ��� ������
sTransactionTypeID = requestSource

'Load Original Trans Details From X_RefTransID
If (X_TypeCredit = 0) And (X_RefTransID <> 0) Then
   Set rsData3 = oledbData.execute("Select DebitReferenceCode, DebitReferenceNum, TerminalNumber, OCurrency, ApprovalNumber From tblCompanyTransPass Where ID=" & X_RefTransID)
   if Not rsData3.EOF Then 
   	  X_DebitReferenceCode = rsData3("DebitReferenceCode")
      X_DebitReferenceNum = rsData3("DebitReferenceNum")
   	  X_ApprovalNumber = rsData3("ApprovalNumber")
	  sTerminalNumber = rsData3("TerminalNumber")
	  if sTerminalNumber <> "" Then
         sTerminalChargeOptions = rsData3("OCurrency")
         CCF_ID = 0
	     bIsCheckTerminalUsabe = false
	  End if  
   end if
   rsData3.Close
   Set rsData3 = Nothing
End if

'----------------------------------------------------------------------
'	Check billing address fields
'----------------------------------------------------------------------
If sIsBillingAddressMust Then
	If BACHAddr1 = "" Then Call throwError("540")
	If BACity = "" Then Call throwError("541")
	If BAZipCode = "" Then Call throwError("542")
	If BACountry = "" Then Call throwError("544")
End if

If isNumeric(X_BankCountry) Then
	'Country is in numeric format
	X_BankCountry = ExecScalar("SELECT [CountryISOCode] FROM [List].[CountryList] WHERE CountryID = " & X_BankCountry, "US")
	if X_BankCountry = "" Then throwError(543)
	BACountryId = BACountry
End if

If sIsBillingAddressMust OR trim(BACountry)<>"" Then
    If isNumeric(BACountry) Then
		'Country is in numeric format
		BACountryCode = ExecScalar("SELECT [CountryISOCode] [List].[CountryList] WHERE CountryID = " & BACountry, "US")
		if BACountryCode = "" Then throwError(543)
		BACountryId = BACountry
	Else
		'Country is in two letter iso format
		BACountryId = ExecScalar("SELECT CountryID FROM [List].[CountryList] WHERE [CountryISOCode] = '" & BACountry & "'", "544")
		if BACountryId = "" Then throwError(543)
		BACountryCode = BACountry
    End if
End if

If sIsBillingAddressMust OR trim(BAState)<>"" Then
	If trim(BACountryCode)="US" OR trim(BACountryCode)="CA" Then
	    If trim(BAState)<>"" AND isNumeric(BAState) Then
			'State is in numeric format
			BAStateCode = ExecScalar("SELECT StateISOCode FROM [List].[StateList] WHERE stateID = " & BAState, "NY")
			if BAStateCode = "" Then throwError(543)
			BAStateId = BAState
		Elseif trim(BAState)<>"" AND NOT isNumeric(BAState) Then
			'State is in two letter iso format
			BAStateId = ExecScalar("SELECT stateID FROM [List].[StateList] WHERE StateISOCode = '" & BAState & "'", "543")
			if BAStateId = "" Then throwError(543)
			BAStateCode = BAState
		Else
			call throwError("543")
	    End if
	End if
End if
If trim(BAZipCode) <> "" AND Len(BAZipCode) > 15 Then Call throwError("542")

'---------------------------------------------------------------
'	eCheck test mode
'---------------------------------------------------------------
 If ckacct = "999999999" or ckacct = "12345678" Then isTestOnly = "1" Else isTestOnly = "0"

 If Protect5mins Then 'check for last 5 minutes same transaction
	sSQL = "SELECT ID FROM tblCompanyTransPass WHERE (InsertDate >= DateAdd(mi, -5, GetDate())) And (Amount=" & X_Amount & ") AND (IPAddress = N'" & sIP & "') AND (Currency = " & X_Currency & ") AND (PaymentMethodDisplay LIKE '%" & X_PhoneNumber & "')"
	If ExecScalar(sSQL, "") <> "" Then throwError("522")
 End If
 CheckPassLimitation sCompanyID, X_Currency, PaymentMethod, X_TypeCredit, trim(ckacct), X_Amount, 0, (requestSource = "7" Or requestSource = "18")

'---------------------------------------------------------
'   Check clearing company by terminal
'---------------------------------------------------------
Function GetDebitTerminalInfo(sTermNum)
	Dim sSQL : sSQL="SELECT dbo.GetDecrypted256(accountPassword256) As accountPassword, dbo.GetDecrypted256(accountPassword3D256) As accountPassword3D," & _
		" dc_isActive, dc_TempBlocks, tblDebitTerminals.* FROM tblDebitTerminals" & _
		" LEFT JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany=tblDebitCompany.debitCompany_id WHERE TerminalNumber='" & trim(sTermNum) & "'"
	Dim rsTerminal : Set rsTerminal = OleDbData.Execute(sSQL)
	GetDebitTerminalInfo = "---"
	If Not rsTerminal.EOF Then
		If Not rsTerminal("dc_isActive") Then GetDebitTerminalInfo = "592"
		If (Trim(requestSource) <> "6" Or CInt(X_TypeCredit) <> 0) And Not rsTerminal("isActive") Then GetDebitTerminalInfo = "592"
		If rsTerminal("dc_TempBlocks") > 0 Then GetDebitTerminalInfo = "524"
		terminalActive = rsTerminal("isActive") Or (Trim(requestSource) = "6" And CInt(X_TypeCredit) = 0)
		debitCompany = rsTerminal("DebitCompany")
		bIsNetpayTerminal = rsTerminal("isNetpayTerminal")
		bIsShvaMasterTerminal = rsTerminal("isShvaMasterTerminal")
		nProcessingMethod = rsTerminal("processingMethod")
		sMcc = rsTerminal("dt_mcc")
		isManipulateAmount = rsTerminal("dt_isManipulateAmount")
		isTestTerminal = rsTerminal("dt_IsTestTerminal")
		TerminalId = rsTerminal("id")
		sTermName = Trim(rsTerminal("dt_Descriptor"))
		dt_EnableRecurringBank = IIF(IsNull(rsTerminal("dt_EnableRecurringBank")), False, rsTerminal("dt_EnableRecurringBank"))
		dt_EnableAuthorization = IIF(IsNull(rsTerminal("dt_EnableAuthorization")), False, rsTerminal("dt_EnableAuthorization"))
		dt_Enable3dsecure = IIF(IsNull(rsTerminal("dt_Enable3dsecure")), False, rsTerminal("dt_Enable3dsecure"))
		dt_IsPersonalNumberRequired = IIF(IsNull(rsTerminal("dt_IsPersonalNumberRequired")), False, rsTerminal("dt_IsPersonalNumberRequired"))
		'terminal login
		sAccountId = Trim(rsTerminal("accountId"))
		sAccountSubId = Trim(rsTerminal("accountSubId"))
		if rsTerminal("accountPassword") <> "" Then sAccountPassword = rsTerminal("accountPassword")
		'terminal 3D login
		sAccountId3D = Trim(rsTerminal("accountId3D"))
		sAccountSubId3D = Trim(rsTerminal("accountSubId3D"))
		If rsTerminal("accountPassword3D") <> "" Then sAccountPassword3D = rsTerminal("accountPassword3D")
	End If
    rsTerminal.close
	If debitCompany = 20 Then bIsUseMaxMind = False
End Function

'PriorityList Setup
X_TerminalHasNext = False
X_TransINC = "X_TransINC_" & sCompanyID & "_" & X_ccNumber & "_" & X_Amount & "_" & X_Currency
XX_Amount = X_Amount
debitCompany = 1

If isTestOnly = "1" Then
	sTerminalChargeOptions = X_Currency
	sTerminalNumber = "0000000"
ElseIf bUseOldTerminal Then
	CCF_ID = GetCompanyTerminal(sCompanyID, PaymentMethod, X_Currency, ccBINCountry, ccBIN, X_TypeCredit, sTerminalNumber, sTerminalChargeOptions)
	CCF_ID = sTerminalChargeOptions
Else
	Dim mSendTrans : Set mSendTrans = New CSendTrans
	CCF_ID = mSendTrans.GetMerchantTerminal(sCompanyID, PaymentMethod, X_Currency, X_TypeCredit, 0, sTerminalNumber, sTerminalChargeOptions)
End If

If CCF_ID = -2 Then 'not found
	call throwError("503")
Elseif CCF_ID = -1 Then 'blocked
	Call throwError("503")
Else
	If bUseOldTerminal Then
		If sTerminalChargeOptions <> X_Currency Then 'convert currency if needed
			X_Amount = ConvertCurrency(X_Currency, sTerminalChargeOptions, X_Amount)
			X_Currency = CCF_ID
		End If
		Call SendCharge()
	Else	
		Call fn_returnResponse(LogSavingChargeDataID, requestSource, debitCompany, X_TransType, X_DPRET, nTransactionID, nInsertDate, X_OrderNumber, Formatnumber(X_Amount,2,-1,0,0), X_Payments, X_Currency, dpApproval, X_Comment)
	End If
End If

Function SendCharge()
	If Not isNumeric(debitCompany) Then throwError("503")
	If sTerminalChargeOptions <> X_Currency Then 'Convert Currency if needed
		X_Amount = ConvertCurrency(X_Currency, sTerminalChargeOptions, XX_Amount)
		X_Currency = sTerminalChargeOptions
	Else	
		X_Amount = XX_Amount 'Restore Amount
	End If
	X_DPRET = GetDebitTerminalInfo(sTerminalNumber)
	If X_DPRET <> "---" Then
		If bUseOldTerminal Then throwError(X_DPRET)
		DebitIProcessResult X_DPRET, ""
		SendCharge = False
		Exit Function
	End If	
	If requestSource = "27" Then
		Call DebitIProcessResult(IIF(int(nProcessingMethod) = 2, "001", "000"), "")
	ElseIf int(nProcessingMethod) = 2 Then
		Call DebitIProcessResult("001", "")
	Else
	    ExecSql("Update tblLogChargeAttempts Set Lca_DebitCompanyID=" & debitCompany & " Where LogChargeAttempts_id=" & LogSavingChargeDataID)
		Select Case debitCompany
			Case 1 : call debitNULL()               'NULL debitCompany
			Case 43 : call debit_Atlas()            'debit Atlas
			Case 57 : Call debit_Qiwi()		        'Qiwi
			Case else : call throwError("503")      'invalid clearing company
		End Select
	End if
	SendCharge = (Trim(X_DPRET) = "000") Or (Trim(X_DPRET) = "001")
End Function

Function DebitIProcessResult(dpRet, dpApproval)
	If sIsBillingAddressMust Then _
		newBillingAddressId = insertBillingAddress(BACountryCode, BAStateCode, BACountryId, BAStateId, BACity, BACHAddr1, BACHAddr2, BAZipCode) _
	Else newBillingAddressId = 0
	sSQL = "SET NOCOUNT ON;"
    If UseLocalID Then sSQL = sSQL & "Declare @curID as int = 1 + IsNull((Select MAX(PhoneDetail_id) From Trans.PhoneDetail Where PhoneDetail_id > " & APP_MINID & " And PhoneDetail_id < " & APP_MAXID & "), " & APP_MINID & ");"
	sSQL = sSQL & "INSERT INTO Trans.PhoneDetail (" & IIF(UseLocalID, "PhoneDetail_id, ", "") & "Merchant_id, FullName, PhoneNumber, PhoneCarrier_id, BillingAddress_id, Email) VALUES (" & _
	    IIF(UseLocalID, "@curID, ", "") & sCompanyID & ", " & _
	    "Left(N'" & X_HolderName & "', 50), " & _
	    "Left(N'" & X_PhoneNumber & "', 20), " & _
	    IIF(True, "NULL", "NULL") & ", " & _
	    IIF(newBillingAddressId <> 0, newBillingAddressId, "NULL") & ", " & _
	    "Left(N'" & X_Email & "', 50) " & _
	    ")SELECT  " & IIF(UseLocalID, "@curID", "SCOPE_IDENTITY()") & " AS NewCardID;SET NOCOUNT OFF"
    xParam = ExecScalar(sSQL, 0)
	If bUseOldTerminal Then DebitIProcessResult = DebitProcessResult(dpRet, dpApproval, xParam) _
	Else DebitIProcessResult = DebitProcessResultIN(dpRet, dpApproval)
End Function

%>
<!--#include file="remoteCharge_PhoneAtlas.asp"-->
<!--#include file="remoteCharge_PhoneQiwi.asp"-->
<!--#include file="remoteCharge_ccDebitNULL.asp"-->
<%
Set rsData = nothing
call closeConnection()
%>
