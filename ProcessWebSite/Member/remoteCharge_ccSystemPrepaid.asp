<%
'---------------------------------------------------------------------
'	Debit SystemPrepaid
'---------------------------------------------------------------------
'const SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS = 13056
function debitSystemPrepaid()
   
   'Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer
   'xNames = Split(X_ccHolderName, " ")
   'if Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
   'if Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
   
  'sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
  'if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
  
  'SendAmnt = X_Amount
  
  'if (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
   '   if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
    '  sCompanyDescriptor = "Refund: " & sCompanyDescriptor
    '  SendAmnt = -X_Amount
  'Else
'	  X_DebitReferenceCode = GetTransRefCode()
  'End if
  'If Cint(X_TransType) = 1 Then mSDep = 0 Else mSDep = 1
    
    '-------------------------------------------------------
  ParamList = "Account=" & sAccountId & _
   	    "&CardNumber=" & GetEncrypted256(X_ccNumber) & _
    	"&ExpMM=" &  X_ccExpMM & "&ExpYY=20" & X_ccExpYY & _
        "&Amount=" & X_Amount & _
        "&TransType=" & mSDep & _
        "&CVV=" & X_ccCVV2 & _
   		"&CurrencyID=" & X_Currency & _
		"&CustomerNumber=" & X_CompanyNum & _
	    "&FullName=" & Server.URLEncode(X_ccHolderName) & _
	    "&ReferenceCode=" & X_DebitReferenceCode & _
        "&ReferenceNumber=" & X_DebitReferenceNum & _
    	"&Descriptor=" & Server.UrlEncode(sCompanyDescriptor)
         
    UrlAddress = WEBSERVICES_URL & "frauddetection.asmx/ProcessPrepaid?" & ParamList
    
    ' service request
    Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
    HttpReq.setTimeouts 1.5*1000, 1.5*1000, 2.5*1000, 2.5*1000 'lResolve, lConnect, lSend, lReceive
    On Error Resume Next
    HttpReq.open "POST", UrlAddress & ParamList, false
	HttpReq.send
    result = HttpReq.responseText
    on error goto 0

    '------------------------------------------------

    'If Len(HttpReq.responseText) = 0 Then Call throwError("521")
	'TxtRet = HttpReq.responseText
	'sDebitReturnAnswer = Left(TxtRet, 200)
    'sReturnCode = Left(TxtRet, 3)
	'sApprovalNumber = Mid(TxtRet, 5, 7)
	'sError = Right(TxtRet, Len(TxtRet) - 12)
	'If trim(sReturnCode) = "001" then sReturnCode = "002" '001 is taken by netpay

    Set xmlRet = Nothing
    Set HttpReq = Nothing

	'Response.Write(sReturnCode & " - " & sError & " - " & sApprovalNumber)
	'Response.Flush
    'On Error GoTo 0 
    
	'Call DebitIProcessResult(sReturnCode, 10) 'TBD: set approval number from response
End function

    function TestFraud() 
    Dim fraudDetectionHttpReq, fraudDetectionUrl, fraudDetectionParams, fraudDetectionResult  
    '& "&ccNumber=" & sCCardNum_Encrypt & "&ccName=" & X_ccHolderName & "&expMonth=" & X_ccExpMM & "&expYear=" & X_ccExpYY  _     
    fraudDetectionParams = "merchantNumber=" & X_CompanyNum & "&amount=" & X_Amount & "&currencyId=" & X_Currency & "&terminalNumber=" & sTerminalNumber _                         
                            & "&ccPhoneNumber=" & X_PhoneNumber & "&ccEmail=" & X_Email & "&ccPersonalNumber=" & X_PersonalNumber& "&ccBINCountry=" & ccBINCountry _ 
                            & "&ccTypeEngString=" & ccTypeEngShow & "&ccDateOfBirth=" & X_DateOfBirth & "&ccTypeId=" & ccTypeID _
                            & "&countryISO=" & BACountry & "&stateISO=" & BAState & "&city=" & BACity & "&address1=" & BACHAddr1 & "&address2=" & BACHAddr2 & "&zipCode=" & BAZipCode _
                            & "&transactionTypeId=" & sTransactionTypeID & "&transTypeId=" & X_TransType & "&creditTypeId=" & X_TypeCredit & "&customerIp=" & sIP & "&referenceTransactionId=" & X_RefTransID _
                            & "&payments=" & X_Payments & "&orderNumber=" & X_OrderNumber & "&payFor=" & X_PayFor & "&productId=" & X_ProductId & "&comment=" & X_Comment _
                            & "&paymentMethod=" & PaymentMethod & "&iMethodType=1" & "&isTest=" & isTestOnly & "&referringUrl=" & referringUrl & "&debitCompanyId=1" _
                            & "&transPayerInfoId=" & X_PayerInfoId & "&transPaymentMethodId=" & X_PaymentMethodId & "&payerId=" & nPayerID & "&payerName=" & X_PayerName _
                            & "&is3DSecure=" & X_Is3dSecure & "&deviceId=" & X_deviceId & "&debitReferenceCode=" & X_DebitReferenceCode & "&debitReferenceNum=" & X_DebitReferenceNum
    fraudDetectionUrl = WEBSERVICES_URL & "frauddetection.asmx/Detect?" & fraudDetectionParams

    ' service request
    Set fraudDetectionHttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
    fraudDetectionHttpReq.setTimeouts 1.5*1000, 1.5*1000, 2.5*1000, 2.5*1000 'lResolve, lConnect, lSend, lReceive
    On Error Resume Next
    fraudDetectionHttpReq.open "POST", fraudDetectionUrl, False	
    fraudDetectionHttpReq.Send
    fraudDetectionResult = fraudDetectionHttpReq.responseText
    on error goto 0

    ' get params 
	isDetected = DBText(GetURLValue(fraudDetectionResult, "isDetected"))
    resultCode = DBText(GetURLValue(fraudDetectionResult, "resultCode"))
    logIds = DBText(GetURLValue(fraudDetectionResult, "logIds"))
    if isDetected = "true" then
        isProceed = false
        nReply = "580" 'resultCode
    else
        isProceed = true
    end if

	'-------------------------------------------------------------------------------
	'   Did not pass fraud check, insert to fail table
	'-------------------------------------------------------------------------------
	If NOT isProceed Then		
		'Insert billing address
		newBillingAddressId = 0
		if TestNumVar(NewBillingAddressID, 0, -1, 0) = 0 then
			on error resume next
			NewBillingAddressID=insertBillingAddress(BACountryCode, BAStateCode, BACountryId, BAStateId, BACity, BACHAddr1, BACHAddr2, BAZipCode)
			on error goto 0
		end if
		
		'Insert credit card details
		if TestNumVar(nNewCreditCardID, 0, -1, 0) = 0 then
			on error resume next
			nNewCreditCardID=insertCard(sCCardNum_Encrypt, X_ccExpMM, X_ccExpYY, X_ccHolderName, X_PayerName, X_ccCVV2, X_PhoneNumber, X_Email, sCompanyID, X_PersonalNumber, X_DateOfBirth, ccTypeID, X_Comment, newBillingAddressId, ccBINCountry)
			on error goto 0
		end if
		newCardId=nNewCreditCardID
		
		'Insert to failed table
		transactionIdOutParam = ""
		insertDateOutParam = ""
		'SaveTransactionData PaymentMethod, ccTypeEngShow, X_ccNumber, EncCVV(X_Cvv), X_ExpDate, sBinCountry
		call insertFailedTransaction (transactionIdOutParam, insertDateOutParam, sCompanyID, fraudDetectionLog_id, sTransactionTypeID, X_TransType, X_TypeCredit, X_Customer, sIP, X_Amount, X_Currency, ccTypeEngShow, X_Payments, X_OrderNumber, nReply, "", X_PayFor, X_ProductId, X_Comment, sTerminalNumber, PaymentMethod, 1, newCardId, isTestOnly, referringUrl, "1", X_PayerInfoId, X_PaymentMethodId, nPayerID, X_Is3dSecure, X_deviceId)
		nTransactionID = transactionIdOutParam
		nInsertDate = insertDateOutParam

		CancelCartTransaction nTransactionID
		RemoveParallelChargeAttempt
		
        ' update log ids
        if not Trim(logIds) = "" then
            dim updateIdsSql
            updateIdsSql = "UPDATE [Log].[RiskRuleHistory] SET [TransFail_id] = " & nTransactionID & " WHERE [RiskRuleHistory_id] IN (" & logIds & ")" 
            ExecSql(updateIdsSql)
        end if

		Call fn_returnResponse(LogSavingChargeDataID,requestSource,"1",X_TransType,nReply,nTransactionID,nInsertDate,X_OrderNumber,formatnumber(X_Amount,2,-1,0,0),X_Payments,X_Currency,sApprovalNumber,X_Comment)
	End if
    
    End function
%>