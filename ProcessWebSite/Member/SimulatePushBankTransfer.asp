﻿<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_transCharge.asp" -->
<%
    Dim X_BankCountry, X_Currency, X_BankID, X_TransID, PaymentMethod, sCompanyID, isTestTerminal
    CompanyNum = DBText(request("CompanyNum"))
    sCompanyID = ExecScalar("SELECT ID FROM tblCompany WHERE CustomerNumber='" & CompanyNum & "'", 0)
    X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, MAX_CURRENCY + 1)
    X_TransID = TestNumVar(Request("TransID"), 0, -1, 0)
    PaymentMethod = TestNumVar(request("PaymentMethod"), PMD_EC_MIN, PMD_EC_MAX, PMD_EC_CHECK)
    If sCompanyID = 0 Then 
        Response.Write("Service Unavailable")
        Response.End
    End If    
    If (X_BankCountry <> "") And IsNumeric(X_BankCountry) Then 'Country is in numeric format
	    X_BankCountry = ExecScalar("SELECT CountryISOCode FROM [List].[CountryList] WHERE CountryID = " & X_BankCountry, "US")
	    If X_BankCountry = "" Then BankListResponse "543", Empty
    End if

    Set rsCmp = server.createobject("Adodb.Recordset")
    rsCmp.Open "SELECT * FROM tblCompany WHERE ID='" & sCompanyID & "'", oledbData, 0, 1
    If Not rsCmp.EOF Then
	    bUseOldTerminal = Not rsCmp("IsUsingNewTerminal")
	    sCompanyName = rsCmp("CompanyName")
	    if TestNumVar(rsCmp("ActiveStatus"), 0, -1, CMPS_NEW) < CMPS_INTEGRATION Then Call throwError("501")  'עסק סגור או חסום
    End If
    rsCmp.Close()
    
    If Request("DOK") <> "" Then 
        Dim sTerminalNumber
	    sTerminalNumber = ExecScalar("Select TerminalNumber From tblCompanyTransPending Where ID=" & X_TransID, 0)
	    Set rsTerminal = Server.CreateObject("Adodb.Recordset")
	    rsTerminal.Open "SELECT dc_isActive, dc_TempBlocks, dbo.GetDecrypted256(accountPassword256) As accountPassword," & _
		    " dbo.GetDecrypted256(accountPassword3D256) As accountPassword3D, tblDebitTerminals.* FROM tblDebitTerminals" & _
		    " LEFT JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany=tblDebitCompany.debitCompany_id" & _
		    " WHERE TerminalNumber='" & trim(sTerminalNumber) & "'", oledbData, 0, 1
	    If Not rsTerminal.EOF Then
		    debitCompany = rsTerminal("DebitCompany")
            isTestTerminal = rsTerminal("dt_IsTestTerminal")
		    'terminal login
		    sAccountId = Trim(rsTerminal("accountId"))
		    sAccountSubId = Trim(rsTerminal("accountSubId"))
		    if rsTerminal("accountPassword") <> "" Then sAccountPassword = rsTerminal("accountPassword")
		    'terminal 3D login
		    sAccountId3D = Trim(rsTerminal("accountId3D"))
		    sAccountSubId3D = Trim(rsTerminal("accountSubId3D"))
		    if rsTerminal("accountPassword3D") <> "" Then sAccountPassword3D = rsTerminal("accountPassword3D")
	    End If
	    rsTerminal.Close()
	    Select Case debitCompany
		    case 42
		        'InPay
		        call debit_InPaySimulateTransfer()
		    case else
			    'invalid clearing company
			    Response.Write("Not Supported")
	    End Select
	    Response.Write("Completed")
    End If
%>
<!--#include file="remoteCharge_echeckInPay.asp"-->
<html>
<head>
 <title>Push Bank Trasnfer Simulation</title>
</head>
<body>
 <h1>Push Bank Trasnfer Simulation</h1>
 <br /><br />
 <form method="post">
    <input type="submit" value="Click here to make the transfer" name="DOK" />
 </form>
</body>
</html>

