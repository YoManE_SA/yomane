<%
  Sub SendRefundJcc(iRs)
	Dim HttpReq, UrlAddress, ParamList, sReturnCode, sError
    Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
    UrlAddress = "https://www.jccsecure.com/SENTRY/PaymentGateway/Merchant/StoreFrontServices/FinancialService.asmx"
    ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
        " <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
        "  <soap:Header>" & _
        "   <AuthHeader xmlns=""http://www.ctl.com/SENTRY/PaymentGateway/Merchant/StoreFrontServices"">" & _
        "    <MerchantID>" & iRs("AccountSubId") & "</MerchantID>" & _
        "    <AcquirerID>" & iRs("AccountId") & "</AcquirerID>" & _
        "    <Password>" & iRs("AccountPassword") & "</Password>" & _
        "   </AuthHeader>" & _
        "  </soap:Header>" & _
        "  <soap:Body>" & _
        "   <RefundAmount xmlns=""http://www.ctl.com/SENTRY/PaymentGateway/Merchant/StoreFrontServices"">" & _
        "    <OrderID>" & iRs("DebitReferenceCode") & "</OrderID>" & _
        "    <Amount>" & iRs("Amount") & "</Amount>" & _
        "    <Culture>en-US</Culture>" & _
        "   </RefundAmount>" & _
        "  </soap:Body>" & _
        "</soap:Envelope>"
        
	On Error Resume Next
    HttpReq.open "POST", UrlAddress, false
	HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    HttpReq.setRequestHeader "SOAPAction", "http://www.ctl.com/SENTRY/PaymentGateway/Merchant/StoreFrontServices/RefundAmount"
    HttpReq.send ParamList
    sResDetails = HttpReq.responseText
    
    sReturnCode = ""
    If sResDetails <> "" Then
    	Set xmlRet = HttpReq.responseXml
        sReturnCode = Trim(xmlRet.selectSingleNode("//RefundAmountResult").text)
        sError = Trim(xmlRet.selectSingleNode("//sError").text)
        If sReturnCode = "true" Then sReturnCode = "000" Else sReturnCode = "2"
        Set xmlRet = Nothing
    End If
	Call ProcessResults(iRs, "JCC", sReturnCode, sError, sApprovalNumber)
  End Sub
%>
