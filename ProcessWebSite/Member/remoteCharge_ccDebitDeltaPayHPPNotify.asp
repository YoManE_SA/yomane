<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="remoteCharge_Functions.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/MerchantTerminals.asp"-->
<!--#include file="../include/timer_class.asp"-->

<%
'---------------------------------------------------------------------
'	Debit DeltaPay
'---------------------------------------------------------------------
Function stripTags(HTMLstring)
	Set RegularExpressionObject = New RegExp
	With RegularExpressionObject
	  .Pattern = "<[^>]+>"
	  .IgnoreCase = True
	  .Global = True
	End With
	stripTags = RegularExpressionObject.Replace(HTMLstring, "")
	Set RegularExpressionObject = nothing
End Function

FileAppendData "DeltaPay.txt", "Request:" & Now & " FORM:" & Request.Form & " QS:" & Request.QueryString & vbCrLf
Server.ScriptTimeout = 300

Dim X_OCurrency, X_OAmount, X_DebitReferenceCode, X_DebitReferenceNum, nApprovalRecurringSeries, nRes, X_ReplyURL, IsNotifyRequest
bUseOldTerminal = True
nApprovalRecurringSeries = "NULL"

sReturnCode = Request("status")
'sApprovalNumber = Request("customer_id")
X_DebitReferenceCode = Request("order_id")
X_DebitReferenceNum = Request("transaction_no")
X_DebitApprovalNumber = Request("approval_no")
X_CustomErrorDesc = Replace(Request("reason"), "'", "")
IsNotifyRequest = Request("IsNotify") = "1"

Dim passID, pendingID, failID, logChargeWhere
pendingID = ExecScalar("Select companyTransPending_id From tblCompanyTransPending Where DebitCompanyID=49 And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
If pendingID = 0 Then 
    passID = ExecScalar("Select ID From tblCompanyTransPass Where DebitCompanyID=49 And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
    If passID = 0 Then 
		failID = ExecScalar("Select ID From tblCompanyTransFail Where DebitCompanyID=49 And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
		If failID = 0 Then
			Response.Write("Transaction not found")
			Response.End
		End If
	End If
End If

If pendingID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=49 And Lca_ReplyCode='553' And Lca_TransNum IN(" & pendingID & ")"
ElseIf passID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=49 And Lca_ReplyCode='000' And Lca_TransNum IN(" & passID & ")"
ElseIf failID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=49 And Lca_ReplyCode Not IN('000', '001', '553') And Lca_TransNum IN(" & failID & ")"
End If

Dim xLogChargeAttemptID, xMerchantNum, xRequestParams, xSendParams, xRetParams, xDebitCompany, xLogReplyCode
If Not GetTransInfo(logChargeWhere) Then
    Response.Write("Charge Attempt not found")
    Response.End
End If
X_ReplyURL = GetURLValue(xRequestParams, "RetURL")
'Response.Write("Process - " & X_ReplyURL)
If pendingID <> 0 Then
    If Trim(sReturnCode) = "APPROVED" Or Trim(sReturnCode) = "AUTHORIZED" Or Trim(sReturnCode) = "CAPTURED" Or Trim(sReturnCode) = "REFUNDED" Or Trim(sReturnCode) = "VOIDED" Then
        sReturnCode = "000"
    ElseIf Trim(sReturnCode) = "PENDING" Then
        sReturnCode = "001"
    Else
        sReturnCode = "002"
    End If
    If sReturnCode <> "001" Then
        ExecSql("Update tblCompanyTransPending Set DebitApprovalNumber='" & X_DebitApprovalNumber & "', DebitReferenceNum='" & X_DebitReferenceNum & "' Where DebitCompanyID=49 And DebitReferenceCode='" & X_DebitReferenceCode & "'")
        nRes = MovePendingTransaction("DebitCompanyID=49 And DebitReferenceCode='" & X_DebitReferenceCode & "'", sReturnCode)
        ExecSql("Update tblLogChargeAttempts Set Lca_ReplyCode='" & sReturnCode & "', Lca_ReplyDesc='" & Replace(X_CustomErrorDesc, "'", "''") & "', Lca_TransNum=" & nRes & ", Lca_ResponseString='" & Replace(Request.Form & "&" & Request.QueryString, "'", "''") & "' Where LogChargeAttempts_id=" & xLogChargeAttemptID)
    End If
Else
	If Trim(sReturnCode) = "CHARGEDBACK" And passID <> 0 Then
		ExecSql("Exec CreateCHB " & passID & ",getDate(), 'notification from deltapay'")
	ElseIf Trim(sReturnCode) = "SETTLED" And passID <> 0 Then
		ExecSql("Insert Into tblLogImportEPA(TransID, Installment, IsPaid)Values(" & passID & ", 1, 1)")
	End If
    sReturnCode = xLogReplyCode
    nRes = IIF(passID <> 0, passID, failID)
End If

X_CustomErrorDesc = stripTags(X_CustomErrorDesc)
If IsNotifyRequest Then Response.Write("") _
Else ReturnResponseFromTransID nRes, sReturnCode
Response.End()

%>