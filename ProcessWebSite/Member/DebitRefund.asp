<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp"-->
<%
	Dim btDataRs, dtStart, dtEnd, nDebitID, nTransID, sWhere
	Server.ScriptTimeout = 900
	dtStart = DateAdd("h", -24, Now)
	If IsDate(Request("StartTime")) Then dtStart = CDate(Request("StartTime"))
	dtEnd = DateAdd("h", 24, dtStart)
	nDebitID = TestNumVar(Request("DebitID"), 1, 0, 0)
	nTransID = TestNumVar(Request("TransID"), 1, 0, 0)
	nStatus = TestNumVar(Request("Status"), 1, 0, -1)
	bAll=IIf(trim(request("All")) = "1", True, False)
	bDebug = IIf(trim(request("Debug")) = "1", True, False)
	
	Function ProcessResults(iRs, strDebitor, sRetCode, sErrorText, sApprovalNum)
		Dim sAnswer : sAnswer="Refunded " & Now() & ":" & sApprovalNum
		If Trim(sRetCode) <> "000" Then sAnswer="Try Refund " & Now() & ":" & sRetCode & "-" & Replace(Left(sErrorText, 500), "'", "''")
		Dim nAutoRefundStatus : nAutoRefundStatus = 3
		If sRetCode = "000" Then
			nAutoRefundStatus = 1
		ElseIf ((iRs("DebitCompanyID")=18 Or iRs("DebitCompanyID")=46) And sRetCode="1316") Or (iRs("DebitCompanyID")=19 And sRetCode="2") Or (iRs("DebitCompanyID")=21 And sRetCode="12") Or (iRs("DebitCompanyID")=21 And sRetCode="76") Then
			nAutoRefundStatus = 2
		End If
		If bDebug Then Response.Write("<br />" & iRs("PID") & ", " & strDebitor & ", Approval:" & sApprovalNum & ", Ret:" & sRetCode & " - " & sErrorText)
		If bDebug Then Response.Flush 
		Dim sSQL : sSQL = "UPDATE tblLog_NoConnection SET lnc_AutoRefundDate=GetDate(), lnc_AutoRefundStatus=" & nAutoRefundStatus & " WHERE LogNoConnection_id=" & iRs("LogNoConnection_id")
		If bDebug Then Response.Write("<br />" & sSQL)
		If bDebug Then Response.Flush
		oleDbData.Execute(sSQL)
		sSQL = "UPDATE tblCompanyTransFail SET AutoRefundDate=GetDate(), AutoRefundStatus=" & nAutoRefundStatus & " WHERE ID=" & iRs("PID")
		If bDebug Then Response.Write("<br />" & sSQL)
		If bDebug Then Response.Flush
		oleDbData.Execute(sSQL)
		sSQL = "INSERT INTO tblLogDebitRefund(ldr_TransFail, ldr_LogNoConnection, ldr_ReplyCode, ldr_Answer) VALUES"
		sSQL = sSQL & " (" & iRs("PID") & ", " & iRs("LogNoConnection_id") & ", '" & DBText(sRetCode) & "', '" & sAnswer & "')"
		If bDebug Then Response.Write("<br />" & sSQL)
		If bDebug Then Response.Write("<script language=""javascript"" type=""text/javascript"">scrollBy(0, 500);</script>")
		If bDebug Then Response.Flush
		oledbData.Execute(sSQL)
	End Function

	Sub SendRefundNULL(iRs)
		Randomize
		ProcessResults iRs, "Netpay Local", "000", "", Right("0000000" & Trim(Int(Rnd()*1000000)), 7)
	End Sub
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Debit Refund</title>
</head>
<body style="font-family: Arial; font-size: 12px;">
	<%
	sAndWhere = " AND DebitCompanyID " & IIf(nDebitID > 0, " = " & nDebitID, " IN (1,18,46)")
    Dim IncLogChargeAttempt : IncLogChargeAttempt = False
	If Not bAll Then
		If nTransID > 0 Then
			sAndWhere = sAndWhere & " AND F.ID=" & nTransID
		ElseIf nStatus > -1 Then
			sAndWhere = sAndWhere & " AND replyCode IN ('521', '520') AND lnc_AutoRefundStatus=" & nStatus
		Else
			sAndWhere = sAndWhere & " AND replyCode IN ('521', '520') AND lnc_InsertDate BETWEEN '" & dtStart & "' AND '" & dtEnd & "' AND (Lca_ResponseString IS NULL OR Lca_ResponseString LIKE 'NP-INT%')"
            IncLogChargeAttempt = True
		End If
	End If
	sSQL = "SELECT F.ID As PID, *, dbo.GetDecrypted256(accountPassword256) accountPassword, dbo.GetDecrypted256(CCard_number256) As CCNumberX, LogNoConnection_id" & _
	" FROM tblCompanyTransFail F WITH (NOLOCK) " & _
	" INNER JOIN tblLog_NoConnection L WITH (NOLOCK) ON F.ID=lnc_TransactionFailID " & _
    IIF(IncLogChargeAttempt, " INNER JOIN tblLogChargeAttempts A WITH (NOLOCK) ON F.ID = Lca_TransNum AND Abs(DateDiff(hour, Lca_DateStart, F.InsertDate))<=1", "") & _
	" LEFT JOIN tblCreditCard C WITH (NOLOCK) ON F.CreditCardID = C.ID" & _
	" LEFT JOIN tblDebitTerminals T WITH (NOLOCK) ON F.DebitCompanyID = T.DebitCompany AND F.TerminalNumber = T.terminalNumber" & _
	" WHERE lnc_AutoRefundStatus NOT IN (1, 2, 4) " & sAndWhere & " ORDER BY F.terminalNumber, F.InsertDate"
	If bDebug Then Response.Write(sSQL)
	'If bDebug Then Response.End
	If bDebug Then Response.Flush 
	Set btDataRs = oleDbData.Execute(sSQL)
	Do While Not btDataRs.EOF
		Select Case btDataRs("DebitCompanyID")
			Case 1 : If nDebitID = 0 Or nDebitID =  1 Then SendRefundNULL(btDataRs)
			Case 18, 46 : If nDebitID = 0 Or (nDebitID = 18 Or nDebitID = 46) Then SendRefundBNS(btDataRs)
			'Case 19 : If nDebitID = 0 Or nDebitID = 19 Then SendRefundJcc(btDataRs)
			'Case 21 : If nDebitID = 0 Or nDebitID = 21 Then SendRefundInvik(btDataRs)
		End Select
	  btDataRs.MoveNext
	Loop
	btDataRs.Close()
	%>
	<br />
	<b>DONE</b>
</body>
</html>
<!--#include file="DebitRefundBNS.asp"-->
<!--include file="DebitRefundInvik.asp"-->
<!--include file="DebitRefundJcc.asp"-->
