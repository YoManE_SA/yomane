<%
'---------------------------------------------------------------------
'	Debit eTriton
'---------------------------------------------------------------------
function set2charNum(cnm)
	if cnm < 10 then set2charNum = "0" & cnm else set2charNum = cnm
end function 
function debit_eTriton()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer

	xNames = Split(Trim(X_ccHolderName), " ")
	if Ubound(xNames) < 1 Then Call throwError("517")
	fName = xNames(0) : sName = xNames(1)
	
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
	UrlAddress = "http://www.etriton.com/API/Transaction.aspx"
	if (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	End if
	X_DebitReferenceCode = GetTransRefCode()
	'if Left(sIP, 8) = "192.168." Then sIP = Request.ServerVariables("LOCAL_ADDR")
	If Cint(X_TransType) = 1 Then Call throwError("503")
	if bankAccountType = "1" Then cAcc = "C" Else cAcc = "S"
	' & X_ApprovalNumber
	if X_Amount < 25 Then throwError("505")
	
	ParamList = "AccountId=" & sAccountId & _
	"&AccountKey=" & sAccountPassword & _
	"&ServiceCode=ACH" & _
	"&SubCode=DEB" & _
	"&CurrencyCode=USD" & _
	"&Amount=" & X_Amount & _
	"&TrnDate=" & Year(Date) & "/" & set2charNum(Month(Date)) & "/" & set2charNum(Day(Date)) & _
	"&TrnTime=" & Time & _
	"&Reference=" & X_DebitReferenceCode & _
	"&IDTRANSACTION=" & _
	"&Notes=" & "Nothing" & _
	"&Product=" & "(" & companyNum & ")Netpay LTD" & _
	"&TRNDATA1=" & cAcc & _
	"&TRNDATA2=" & ckacct & _
	"&TRNDATA3=" & ckaba & _
	"&TRNDATA4=" & X_BankName & _
	"&TRNDATA5=" & X_BankCity & _
	"&TRNDATA6=" & X_BankPhone & _
	"&TRNDATA7=" & X_BankStateCode & _
	"&TRNDATA8=" & X_DebitReferenceCode & _
	"&TRNDATA9=" & sName & _
	"&TRNDATA10=" & "" & _
	"&TRNDATA11=" & fName & _
	"&TRNDATA12=" & X_PersonalNumber & _
	"&TRNDATA13=" & X_PhoneNumber & _
	"&TRNDATA14=" & BAZipCode & _
	"&TRNDATA15=" & X_BirthDate & _
	"&TRNDATA16=" & BACity & _
	"&TRNDATA17=" & BAStateCode & _
	"&TRNDATA18=" & BACountryCode & _
	"&TRNDATA19=" & BACHAddr1 & _
	"&TRNDATA20=" & X_Email
	'Response.Write(ParamList)
	'Response.End
	On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
		HttpReq.Send ParamList
		TxtRet = HttpReq.responseText
		TxtRet = Replace(Replace(TxtRet, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba))
		SaveLogChargeAttemptRequestResponse Replace(Replace(ParamList, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba)), TxtRet
		HttpError = FormatHttpRequestError(HttpReq)
	On Error GoTo 0
	
	If Len(TxtRet) = 0 Then Call throwError("521")
	sDebitReturnAnswer = Left(TxtRet, 400)
	sReturnCode = GetURLValue(TxtRet, "ACCEPTED")
	sResult = GetURLValue(TxtRet, "RESULT")
	
	sApprovalNumber = GetURLValue(TxtRet, "IDTRANSACTION")
	'Response.Write(ParamList & "<br>" & vbCrLf & sReturnCode & "<br>" & vbCrLf & sResult & "<br>" & vbCrLf & TxtRet)
	'Response.End
	'sReturnCode = "Y" : sResult = "A"
	If sReturnCode = "Y" And (sResult = "A" Or sResult = "S" Or sResult = "P") then
		'If sResult = "P" Then sResult = "001" Else sReturnCode = "000"
		sReturnCode = "001"
	Else
		sReturnCode = GetURLValue(TxtRet, "RETURNCODE")
		If (sReturnCode = "000") Or (sReturnCode = "001") Then sReturnCode = "002"
	End If
	DebitIProcessResult sReturnCode, sApprovalNumber

	Set xmlRet = Nothing
	Set HttpReq = Nothing
End function
%>