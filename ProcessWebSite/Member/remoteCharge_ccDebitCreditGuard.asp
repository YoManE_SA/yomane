<%
'---------------------------------------------------------------------
'		debit company Shva - sending transaction
'---------------------------------------------------------------------

Function debitCreditGuard()
	Dim HttpReq, UrlAddress, ParamList, SendData, sResDetails, sSendMode, xNames, sReturnCode, sApprovalNumber

	If (Cint(X_TypeCredit) = 0) Then
		If X_DebitReferenceNum = "" Then Call throwError("532") 
	    ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		    "<ashrait>" & vbCrLf & _
		    " <request>" & vbCrLf & _
            "  <version>1000</version>" & vbCrLf & _
            "  <language>ENG</language>" & vbCrLf & _
            "  <command>refundDeal</command>" & vbCrLf & _
		    "  <refundDeal>" & vbCrLf & _
            "   <terminalNumber>" & sAccountSubId & "</terminalNumber>" & vbCrLf & _
            "   <tranId>" & X_DebitReferenceNum & "</tranId>" & vbCrLf & _
            "   <total>" & CLng(X_Amount * 100) & "</total>" & vbCrLf & _
            "   <user>" & X_DebitReferenceCode & "</user>" & vbCrLf & _
            "   <authNumber>" & X_ConfirmNumber & "</authNumber>" & vbCrLf & _
            "   <cardNo></cardNo>" & vbCrLf & _
            "   <transactionType>Debit</transactionType>" & vbCrLf & _
		    "  </refundDeal>" & vbCrLf & _
		    " </request>" & vbCrLf & _
		    "</ashrait>"
    Else
	    If Cint(X_TransType) <> 2 Then X_DebitReferenceCode = GetTransRefCode()
        xNames = Split(X_ccHolderName, " ")
        If Ubound(xNames) >= 0 Then fName = xNames(0)
        If Ubound(xNames) >= 1 Then sName = xNames(1)
        sSendMode = "autoComm"
        If Cint(X_TransType) = 1 Then sSendMode = "Verify"

	    ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		    "<ashrait>" & vbCrLf & _
		    " <request>" & vbCrLf & _
            "  <version>1000</version>" & vbCrLf & _
            "  <language>ENG</language>" & vbCrLf & _
            "  <command>doDeal</command>" & vbCrLf & _
		    "  <doDeal>" & vbCrLf & _
            "   <terminalNumber>" & sAccountSubId & "</terminalNumber>" & vbCrLf & _
            "   <cardNo>" & X_ccNumber & "</cardNo>" & vbCrLf & _
            "   <cardExpiration>" & X_ccExpMM & X_ccExpYY & "</cardExpiration>" & vbCrLf & _ 
            "   <total>" & CLng(X_Amount * 100) & "</total>" & vbCrLf & _
            "   <transactionType>Debit</transactionType>" & vbCrLf & _
            "   <creditType>RegularCredit</creditType>" & vbCrLf & _
            "   <currency>" & GetCurISOName(X_Currency) & "</currency>" & vbCrLf & _
            "   <transactionCode>Phone</transactionCode>" & vbCrLf & _
            "   <validation>" & sSendMode & "</validation>" & vbCrLf & _
            "   <cvv>" & Trim(X_ccCVV2) & "</cvv>" & vbCrLf & _
            "   <clientIp>" & sIP & "</clientIp>" & vbCrLf & _
            "   <email>" & X_Email & "</email>" & vbCrLf & _
            "   <authType></authType>" & vbCrLf & _
            "   <authNumber>" & X_ConfirmNumber & "</authNumber>" & vbCrLf & _
            "   <user>" & X_DebitReferenceCode & "</user>" & vbCrLf & _
            "   <customerData>" & vbCrLf & _
            "    <extendedTransactionType>ECOMMERCE</extendedTransactionType>" & vbCrLf & _
            "    <extendedPaymentType>SALE</extendedPaymentType>" & vbCrLf & _
            "    <transactionApplicationId></transactionApplicationId>" & vbCrLf & _
            "    <senderReferenceNumber>" & X_DebitReferenceCode & "</senderReferenceNumber>" & vbCrLf & _
            "    <senderAccountNumber></senderAccountNumber>" & vbCrLf & _
            "    <chargeDescriptor>" & sCompanyDescriptor & "</chargeDescriptor>" & vbCrLf & _
            "    <firstName>" & fName & "</firstName>" & vbCrLf & _
            "    <lastName>" & sName & "</lastName>" & vbCrLf & _
            "    <acquirerResponseId>" & X_AcquirerReferenceNum & "</acquirerResponseId>" & vbCrLf & _
            "   </customerData>" & vbCrLf & _ 
		    "  </doDeal>" & vbCrLf & _
		    " </request>" & vbCrLf & _
    	    "</ashrait>"
    End If

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    If isTestTerminal Then UrlAddress = "https://cgopt.creditguard.co.il/xpo/Relay" _
	Else UrlAddress = "https://cgopt.creditguard.co.il/xpo/Relay"

    SendData = "user=" & Server.URLEncode(sAccountId) & "&password=" & Server.URLEncode(sAccountPassword) & "&int_in=" & Server.URLEncode(ParamList)
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=utf-8"
		HttpReq.send SendData
		sResDetails = HttpReq.responseText
        
		HttpError = FormatHttpRequestError(HttpReq)
		TimeControl.Append("A_SEND") 'Time Optimization *********
		If Len(sResDetails) = 0 Then Call throwError("521")
        ParamList = Replace(ParamList, X_ccNumber, GetSafePartialNumber(X_ccNumber))
    	ParamList = Replace(ParamList, "<cvv>" & X_ccCVV2, "<cvv>" & GetSafePartialNumber(X_ccCVV2))
    	SaveLogChargeAttemptRequestResponse ParamList, sResDetails

		Set xmlData = Server.CreateObject("Microsoft.XMLDOM")
		xmlData.LoadXML HttpReq.responseText

        sReturnCode = xmlData.selectSingleNode("//result").text
		X_CustomErrorDesc = xmlData.selectSingleNode("//message").text
        X_DebitReferenceNum = xmlData.selectSingleNode("//tranId").text
        sApprovalNumber = xmlData.selectSingleNode("//authNumber").text
        X_AcquirerReferenceNum = xmlData.selectSingleNode("//acquirerResponseId").text

        'Response.Write(UrlAddress & "<br/>" & ParamList & "<br/>" & HttpReq.responseText & "<br/>" & HttpReq.status & ":" & HttpReq.statusText & vbCrLf) : Response.End()
	On Error Goto 0
	If (Len(sReturnCode) < 1) Then Call throwError("520")

	If sReturnCode = "000" Then 
        'do nothing    
	ElseIf nReply = "001" Then 
		nReply = "1001"
	End If

	Set xmlRet = Nothing
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>