<%@ Page Language="VB" Inherits="htmlInputs" ValidateRequest="false" %>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import Namespace="System.IO" %>
<script runat="server">
	Private Function TransReq(rqVal As String) as String
		Dim strBytes() As Byte = Encoding.Unicode.GetBytes(rqVal)
		strBytes = Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding(1255), strBytes)
		Dim asciiChars(Encoding.Default.GetCharCount(strBytes, 0, strBytes.Length)) As Char
		Encoding.Default.GetChars(strBytes, 0, strBytes.Length, asciiChars, 0)
		return New String(asciiChars)
	End Function

	Private Function ASCII2UTF8(nEncoding As Integer, sASCII As String) as String
		dim bytesUTF8() as Byte = Encoding.UTF8.GetBytes(sASCII)
		Dim bytesASCII() As Byte = Encoding.Convert(Encoding.GetEncoding(nEncoding), Encoding.UTF8, bytesUTF8)
		Dim sUTF8 As String = Encoding.ASCII.GetString(bytesUTF8)
		return sUTF8
	End Function

	Protected Sub Page_Load()
		Dim RefReferal As String = ""
		Dim RefCode As String = dbPages.TestVar(Request("RefCode"), -1, "")
		Dim RefType As Integer = dbPages.TestVar(Request("RefType"), 0, -1, 0)
		Dim RefIP As String = dbPages.TestVar(Request("RefIP"), -1, Request.UserHostAddress)
        If Request.UrlReferrer IsNot Nothing Then RefReferal = dbPages.TestVar(Request("RefReferal"), -1, Request.UrlReferrer.ToString())
		If String.IsNullOrEmpty(RefCode) Then
		    Response.Write("Error: RefCode not valid") : Response.End
		End If
		Dim nEncoding As Integer : nEncoding = dbPages.TestVar(request("Encoding"), 1, 0, 0)
		Dim AFID As Integer = dbPages.TestVar(dbPages.ExecScalar("Select affiliates_id From tblAffiliates Where AFLinkRefID='" & Replace(RefCode, "'", "''") & "'"), 0, -1, 0)
		If AFID = 0 Then
		    Response.Write("Error: Affiliate not found") : Response.End
		End If
        dbPages.ExecSql("Insert Into tblAffiliatesCount(AFCAFFID, AFCType, AFCIPAddress, AFCReferal)Values(" & AFID & ", " & RefType & ", '" & RefIP.Replace("'", "''") & "', '" & RefReferal.Replace("'", "''") & "')")
        Response.Write("OK:" & AFID)
	End Sub
</script>