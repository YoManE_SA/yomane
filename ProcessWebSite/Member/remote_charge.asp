<%
Server.ScriptTimeout = 700

Response.Charset = "windows-1255"
Response.CodePage = 1255

Dim X_PaymentMethodId, X_PayerInfoId
Dim nTransactionID, nInsertDate, X_ConfirmNumber, X_DPRET, nRecurringSeriesReturn, isTestTerminal, sCcStorageID, sClient_WalletID
Dim dt_EnableRecurringBank, dt_EnableAuthorization, dt_Enable3dsecure, OriginalTransID, OriginalTransDeniedStatus
%>

<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/timer_class.asp"-->
<!--#include file="../include/MerchantTerminals.asp"-->
<!--#include file="remoteCharge_Functions.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<script language="JScript" runat="server" src="../Include/json2.js"></script>
<%
   

' initial values - needed for ThrowError (20081124 Tamir)
NewBillingAddressID=0
nNewCreditCardID=0

'20110206 Tamir - needed for storing BNS card identifier in recurring series
NewRecurringSeriesStoredCardIdentifier=""

'Getting user's IP address
Dim xRequestParams, xRetParams
Dim sIP, ccBIN, CTP_Status, HttpError, rsData2, rsData3, X_3dRedirect, X_3dRedirectMethod, TerminalId, X_deviceId, X_ExpDate, X_Currency
Dim sTermName, sTerminalName, sAccountId, sAccountSubId, sAccountAuthCode1, sAccountPassword, sAccountId3D, sAccountSubId3D, sAccountPassword3D, sAccountAuthCode13D, X_CustomErrorDesc, X_DateOfBirth, X_ProductId, X_CartId
Dim debitCompany, terminalActive, bIsShvaMasterTerminal, nProcessingMethod, sMcc, isManipulateAmount, bIsValidateEmail, bIsValidateFullName, X_AcquirerReferenceNum

isLocalTransaction = cBool(request("isLocalTransaction"))
isLocalRequest = cBool(Trim(Request.ServerVariables("LOCAL_ADDR")) = Request.ServerVariables("REMOTE_ADDR") OR Left(Trim(Request.ServerVariables("REMOTE_ADDR")), 11) = "80.179.180.")
canChargeLocally = cBool(isLocalTransaction AND isLocalRequest)

ClientIP = DBText(request("ClientIP"))
sIP = trim(Request.ServerVariables( "HTTP_X_FORWARDED_FOR" ))
If Trim(ClientIP) <> "" Then 
	sIP = ClientIP
Elseif sIP = "" Then
	sIP = Request.ServerVariables( "REMOTE_ADDR" )
End if
If sIP = "::1" Then sIP = "127.0.0.1"

X_CompanyNum = right(DBText(request("CompanyNum")),7)
requestSource = DBText(request("requestSource"))
If trim(requestSource) = "" Then requestSource="8"

'Saving charge data into log
LogSavingChargeDataID = 0
LogSavingChargeDataID = Sub_LogSavingChargeData(requestSource, X_CompanyNum, sIP)

'20100815 Tamir - prohibit unsecure (HTTP) requests
If FORCESSL And UCase(Trim(Request.ServerVariables("HTTPS")))="OFF" Then ThrowError "534"

'20100713 Tamir - AutoCapture (automatically capture the pre-auth after X hours, if not cancelled)
Dim nAutoCaptureHours : nAutoCaptureHours = 0

'20101031 Tamir - check, if refund processing is allowed
Dim bIsRefund : bIsRefund = False

'20110606 Tamir - RiskMailCc fields added
Dim nRiskMailCcLimit : nRiskMailCcLimit = 3
Dim bRiskMailCcIsEnabled : bRiskMailCcIsEnabled = False

Dim nLimitCcForEmailAllowedCount : nLimitCcForEmailAllowedCount = 3
Dim bIsLimitCcForEmailDeclineTrans : bIsLimitCcForEmailDeclineTrans = False
Dim bIsLimitCcForEmailBlockEmail : bIsLimitCcForEmailBlockEmail = False
Dim bIsLimitCcForEmailBlockNewCc : bIsLimitCcForEmailBlockNewCc = False
Dim bIsLimitCcForEmailBlockAllCc : bIsLimitCcForEmailBlockAllCc = False
Dim bIsLimitCcForEmailNotifyByEmail : bIsLimitCcForEmailNotifyByEmail = False
Dim sLimitCcForEmailNotifyByEmailList : sLimitCcForEmailNotifyByEmailList = ""

Dim nLimitEmailForCcAllowedCount : nLimitEmailForCcAllowedCount = 3
Dim bIsLimitEmailForCcBlockNewEmail : bIsLimitEmailForCcBlockNewEmail = False
Dim bIsLimitEmailForCcBlockAllEmails : bIsLimitEmailForCcBlockAllEmails = False
Dim bIsLimitEmailForCcBlockCc : bIsLimitEmailForCcBlockCc = False
Dim bIsLimitEmailForCcDeclineTrans : bIsLimitEmailForCcDeclineTrans = False
Dim bIsLimitEmailForCcNotifyByEmail : bIsLimitEmailForCc1NotifyByEmail = False
Dim sLimitEmailForCcNotifyByEmailList : sLimitEmailForCcNotifyByEmailList = ""

'20111117 Tamir - limit of charges, stages and years in recurring
Dim nRecurringLimitCharges : nRecurringLimitCharges = 500
Dim nRecurringLimitStages : nRecurringLimitStages = 6
Dim nRecurringLimitYears : nRecurringLimitYears = 10

'sSQL="SELECT ID, CompanyName, ActiveStatus, IsApprovalOnly, IsCcStorageCharge, MultiChargeProtectionMins, IsSkipFraudDetectionInVirtualTerminal," & _
	'" IsCustomerPurchase, IsCustomerPurchaseCVV2, IsCustomerPurchasePersonalNumber, IsCustomerPurchasePhoneNumber, IsCustomerPurchaseEmail," & _
	'" IsRemoteCharge, IsRemoteChargeCVV2, IsRemoteChargePhoneNumber, IsRemoteChargeEmail, IsRemoteChargePersonalNumber, IsSystemPay, IsRequiredClientIP," & _
	'" IsSystemPayPersonalNumber, IsSystemPayPhoneNumber, IsSystemPayEmail, IsSystemPayCVV2, IsPublicPay, IsPublicPayPersonalNumber," & _
	'" IsPublicPayCVV2, IsPublicPayPhoneNumber, IsPublicPayEmail, IsBillingAddressMust, IsBillingCityOptional, IsSendUserConfirmationEmail, IsMerchantNotifiedOnPass, IsMerchantNotifiedOnFail, " & _
	'" ISNULL(mrs.IsEnabled, 0) AS IsAllowRecurring, IsAllow3DTrans, IsAllowFreeMail, IsUseFraudDetection_MaxMind, MaxMind_MinScore, countryBlackList, countryWhiteList, descriptor," & _
	'" dailyCcMaxFailCount, debitCompanyExID, AllowedAmounts, IsUsePPWList, IsUsingNewTerminal, RiskScore, CFF_Currency, ISNULL(GroupID,0) AS GroupID," & _
	'" RiskMailCcLimit, RiskMailCcIsEnabled, RiskMailCcIsTransactionDeclined, RiskMailCcIsMailBlacklisted," & _
	'" RiskMailCcIsCardBlacklisted, RiskMailCcIsCardsBlacklisted, RiskMailCcIsMerchantNotified, RiskMailCcNotifyEmail," & _
	'" ISNULL(mrs.MaxCharges,0) AS RecurringLimitCharges, ISNULL(mrs.MaxYears,0) AS RecurringLimitYears, ISNULL(mrs.MaxStages,0) AS RecurringLimitStages," & _
	'" ISNULL(DailyVolumeLimit,-1) AS DailyVolumeLimit, IsCcWhiteListEnabled, IPWhiteList, AutoCaptureHours, IsRefund, HashKey, IsAllowSilentPostCcDetails" & _
	'" FROM tblCompany m WITH (NOLOCK) LEFT JOIN tblMerchantRecurringSettings mrs WITH (NOLOCK) ON m.ID=mrs.MerchantID WHERE CustomerNumber='" & X_CompanyNum & "'"

	sSQL = "SELECT m.ID, m.CompanyName, m.ActiveStatus, m.IsApprovalOnly, m.IsCcStorageCharge, m.MultiChargeProtectionMins, m.IsSkipFraudDetectionInVirtualTerminal, " & _
			" m.IsCustomerPurchase, m.IsCustomerPurchaseCVV2, m.IsCustomerPurchasePersonalNumber, m.IsCustomerPurchasePhoneNumber, " & _
			" m.IsCustomerPurchaseEmail, m.IsRemoteCharge, m.IsRemoteChargeCVV2, m.IsRemoteChargePhoneNumber, m.IsRemoteChargeEmail, " & _
			" m.IsRemoteChargePersonalNumber, m.IsSystemPay, m.IsRequiredClientIP, m.IsSystemPayPersonalNumber, m.IsSystemPayPhoneNumber, " & _
			" m.IsSystemPayEmail, m.IsSystemPayCVV2, m.IsPublicPay, m.IsPublicPayPersonalNumber, m.IsPublicPayCVV2, m.IsPublicPayPhoneNumber, " & _
			" m.IsPublicPayEmail, m.IsBillingAddressMust, m.IsBillingCityOptional, m.IsSendUserConfirmationEmail, m.IsMerchantNotifiedOnPass, " & _
			" m.IsMerchantNotifiedOnFail, ISNULL(mrs.IsEnabled, 0) AS IsAllowRecurring, m.IsAllow3DTrans, m.isAllowFreeMail, m.IsUseFraudDetection_MaxMind, " & _
			" m.MaxMind_MinScore, m.descriptor, m.dailyCcMaxFailCount, m.debitCompanyExID, m.AllowedAmounts, " & _
			" m.IsUsePPWList, m.IsUsingNewTerminal, m.RiskScore, m.CFF_Currency, ISNULL(m.GroupID, 0) AS GroupID, ISNULL(mrs.MaxCharges, 0) " & _
			" AS RecurringLimitCharges, ISNULL(mrs.MaxYears, 0) AS RecurringLimitYears, ISNULL(mrs.MaxStages, 0) AS RecurringLimitStages, " & _
			" ISNULL(m.DailyVolumeLimit, - 1) AS DailyVolumeLimit, m.IsCcWhiteListEnabled, m.IPWhiteList, m.AutoCaptureHours, m.IsRefund, m.HashKey, m.IsAllowSilentPostCcDetails, " & _
			" Setting.SetMerchantRisk.LimitCcForEmailAllowedCount, " & _
			" Setting.SetMerchantRisk.IsLimitCcForEmailBlockNewCc, " & _
			" Setting.SetMerchantRisk.IsLimitCcForEmailBlockAllCc, " & _
			" Setting.SetMerchantRisk.IsLimitCcForEmailBlockEmail, " & _
			" Setting.SetMerchantRisk.IsLimitCcForEmailDeclineTrans, " & _
			" Setting.SetMerchantRisk.IsLimitCcForEmailNotifyByEmail, " & _
			" Setting.SetMerchantRisk.LimitCcForEmailNotifyByEmailList, " & _
			" Setting.SetMerchantRisk.LimitEmailForCcAllowedCount, " & _
			" Setting.SetMerchantRisk.IsLimitEmailForCcBlockNewEmail, " & _
			" Setting.SetMerchantRisk.IsLimitEmailForCcBlockAllEmails, " & _
			" Setting.SetMerchantRisk.IsLimitEmailForCcBlockCc, " & _
			" Setting.SetMerchantRisk.IsLimitEmailForCcDeclineTrans, " & _
			" Setting.SetMerchantRisk.IsLimitEmailForCcNotifyByEmail, " & _
			" Setting.SetMerchantRisk.LimitEmailForCcNotifyByEmailList, " & _
			" Setting.SetMerchantRisk.WhitelistCountry, " & _
			" Setting.SetMerchantRisk.BlacklistCountry, " & _
			" Setting.SetMerchantRisk.WhitelistState, " & _
			" Setting.SetMerchantRisk.BlacklistState " & _
			" FROM tblCompany AS m WITH (NOLOCK) LEFT OUTER JOIN " & _
			" tblMerchantRecurringSettings AS mrs WITH (NOLOCK) ON m.ID = mrs.MerchantID LEFT OUTER JOIN " & _
			" Setting.SetMerchantRisk WITH (NOLOCK) ON m.ID = Setting.SetMerchantRisk.Merchant_id " & _
			" WHERE CustomerNumber='" & X_CompanyNum & "'"

	'response.write ssql & "<br><br>"
     

Set rsData2 = oledbData.execute(sSQL)
  
If rsData2.EOF Then
	rsData2.Close(): Call throwError("500") 'Merchant not found
End if

'20120126 Tamir - preventing disallowed Silent Post with cc details
If requestSource = "8" And Not rsData2("IsAllowSilentPostCcDetails") Then
	If Trim(Request("CCard_num1")) <> "" Or Trim(Request("CCard_num2")) <> "" Or Trim(Request("CCard_num3")) <> "" Or Trim(Request("CCard_num4")) <> "" Or Trim(Request("CardNum")) <> "" Then
		rsData2.Close() : ThrowError 537
	End If
End If

X_MerchantHashKey = rsData2("HashKey")
If Request("Sign") <> "" Then
	Dim EncodedSignature
	EncodedSignature = X_CompanyNum
	If Request("TransType") <> "" Then EncodedSignature = EncodedSignature & request("TransType")
	If Request("TypeCredit") <> "" Then EncodedSignature = EncodedSignature & request("TypeCredit")
	If Request("Amount") <> "" Then EncodedSignature = EncodedSignature & request("Amount")
	If Request("Currency") <> "" Then EncodedSignature = EncodedSignature & request("Currency")
	If Request("CardNum") <> "" Then EncodedSignature = EncodedSignature & request("CardNum")
	If Request("RefTransID") <> "" Then EncodedSignature = EncodedSignature & request("RefTransID")
	EncodedSignature = EncodedSignature & X_MerchantHashKey
	If Not ValidateHash("SHA", EncodedSignature, Request("Sign")) Then
		rsData2.Close() : Call throwError("500")
	End If
End If

'20111117 Tamir - limit of charges, stages and years in recurring
nRecurringLimitCharges = rsData2("RecurringLimitCharges")
nRecurringLimitStages = rsData2("RecurringLimitStages")
nRecurringLimitYears = rsData2("RecurringLimitYears")

'20101209 Tamir - preventing unneseccary call to tblCompany in insertPassedTransaction
CFF_Currency = rsData2("CFF_Currency")

nActiveStatus = TestNumVar(rsData2("ActiveStatus"), 0, -1, CMPS_NEW)
If (nActiveStatus < CMPS_CPONLY) OR (nActiveStatus = CMPS_CPONLY And Trim(Request("requestSource")) <> "6") Then 
	rsData2.Close(): Call throwError("501")	'Merchant blocked/closed
End if

'20101031 Tamir - check, if refund processing is allowed
bIsRefund = IIF(canChargeLocally And requestSource = 5, True, rsData2("IsRefund"))

' Risk Mail Cc fields 
nLimitCcForEmailAllowedCount = TestNumVar(rsData2("LimitCcForEmailAllowedCount"), 1, 0, 0)
sLimitCcForEmailNotifyByEmailList = Trim(rsData2("LimitCcForEmailNotifyByEmailList"))
If rsData2("IsLimitCcForEmailDeclineTrans") Then bIsLimitCcForEmailDeclineTrans = True
If rsData2("IsLimitCcForEmailBlockEmail") Then bIsLimitCcForEmailBlockEmail = True
If rsData2("IsLimitCcForEmailBlockNewCc") Then bIsLimitCcForEmailBlockNewCc = True
If rsData2("IsLimitCcForEmailBlockAllCc") Then bIsLimitCcForEmailBlockAllCc = True
If rsData2("IsLimitCcForEmailNotifyByEmail") And sLimitCcForEmailNotifyByEmailList <> "" Then bIsLimitCcForEmailNotifyByEmail = True

' Risk Cc Mail fields 
nLimitEmailForCcAllowedCount = TestNumVar(rsData2("LimitEmailForCcAllowedCount"), 1, 0, 0)
sLimitEmailForCcNotifyByEmailList = Trim(rsData2("LimitEmailForCcNotifyByEmailList"))
If rsData2("IsLimitEmailForCcBlockNewEmail") Then bIsLimitEmailForCcBlockNewEmail = True
If rsData2("IsLimitEmailForCcBlockAllEmails") Then bIsLimitEmailForCcBlockAllEmails = True
If rsData2("IsLimitEmailForCcBlockCc") Then bIsLimitEmailForCcBlockCc = True
If rsData2("IsLimitEmailForCcDeclineTrans") Then bIsLimitEmailForCcDeclineTrans = True
If rsData2("IsLimitEmailForCcNotifyByEmail") And sLimitEmailForCcNotifyByEmailList <> "" Then bIsLimitEmailForCcNotifyByEmail = True

' enable email cc risk 
bRiskMailCcIsEnabled = nLimitCcForEmailAllowedCount > 0 or nLimitEmailForCcAllowedCount > 0

nAutoCaptureHours = TestNumVar(Request("AutoCaptureHours"), 0, 72, 0)
If nAutoCaptureHours = 0 Then nAutoCaptureHours = TestNumVar(rsData2("AutoCaptureHours"), 0, 72, 0)

bUseOldTerminal = Not IIF(IsNull(rsData2("IsUsingNewTerminal")), False, rsData2("IsUsingNewTerminal"))
bIsCheckTerminalUsabe = True
bIsCheckDailyLimts = True
bIsCheckBlockCCNum = True
bIsCheckCommonBlock = True
bIsCheckAllowedAmounts = True
bIsCheckCountryBlacklist = True
bIsValidateEmail = True
bIsValidateFullName = True

'20100511 Tamir - Skip parts of Risk Management for whitelisted CC
bIsCcWhiteListEnabled = rsData2("IsCcWhiteListEnabled")
nCreditCardWhitelistID = -1
nCreditCardWhitelistLevel=-1
nCreditCardWhitelistNameMatch=-1

'20100225 Tamir - check daily volume limit (regardless the currency, passed debits only)
bIsCheckDailyVolumeLimit = False
nDailyVolumeLimit = rsData2("DailyVolumeLimit")
If nDailyVolumeLimit >= 0 Then bIsCheckDailyVolumeLimit = True

sCompanyID = rsData2("id")
sCompanyName = rsData2("CompanyName")
bIsBillingAddressMust = rsData2("IsBillingAddressMust")
bIsBillingCityOptional = rsData2("IsBillingCityOptional")
nMultiChargeProtectionMins = TestNumVar(rsData2("MultiChargeProtectionMins"), 0, -1, 0)
sIsSendUserConfirmationEmail = rsData2("IsSendUserConfirmationEmail")
bIsMerchantNotifiedOnPass = rsData2("IsMerchantNotifiedOnPass")
bIsMerchantNotifiedOnFail = rsData2("IsMerchantNotifiedOnFail")
sIsAllow3DTrans = rsData2("IsAllow3DTrans")
bIsUseMaxMind = rsData2("IsUseFraudDetection_MaxMind")
nMaxMind_MinScore = rsData2("MaxMind_MinScore")
nRiskScoreAllowed = rsData2("RiskScore")
bIsAllowFreeMail = rsData2("IsAllowFreeMail")
sCountryBlackList = rsData2("BlacklistCountry")
sCountryWhiteList = rsData2("WhitelistCountry")
sStateBlackList = rsData2("BlacklistState")
sStateWhiteList = rsData2("WhitelistState")
sCompanyDescriptor = DBText(rsData2("descriptor"))
sDebitCompanyExID = DBText(rsData2("debitCompanyExID"))
'[Removed By Zeev]dailyCcMaxFailCount = TestNumVar(rsData2("dailyCcMaxFailCount"), 0, -1, 0)
AllowedAmounts = rsData2("AllowedAmounts")
IsUsePPWList = rsData2("IsUsePPWList")
MerchantGroupID = rsData2("GroupID")
Merchant_EnableAuthOnly = rsData2("IsApprovalOnly")
'bIsNetpayTerminal = rsData2("IsNetpayTerminal")

'20100602 Tamir - Skip Fraud Detection for request from whitelisted IP
If InStr(";;" & rsData2("IPWhiteList") & ";", ";" & Request.ServerVariables("REMOTE_ADDR") & ";") > 0 Then bIsUseMaxMind = False

'20110125 Tamir - when recurring, 3ds back, disable duplicate transaction checking
If requestSource = "20" Or TestNumVar(request("TransType"), 0, 999999, 0) = "4" Then nMultiChargeProtectionMins = 0

referringUrl = DBText(request("referringUrl"))
If trim(referringUrl) = "" Then referringUrl = DBText(Request.ServerVariables("HTTP_REFERER"))
nPayerID = TestNumVar(request("PayerID"), 0, -1, 0)
X_TransType = TestNumVar(request("TransType"), 0, 999999, 0)
X_PayFor = DBText(request("PayFor"))
X_ProductId = DBText(request("MerchantProductId"))
X_CartId = DBText(request("CartId"))
X_OrderNumber = DBText(request("Order"))
X_RefTransID = TestNumVar(request("RefTransID"), 0, -1, 0)
X_StoreCc = Request("StoreCc") = "1"
X_DebitApprovalNumber = ""
X_DebitReferenceCode = ""
X_DebitReferenceNum = ""
X_RefTransInsertDate = ""
X_Track2 = ""
isCardPresent = 0
if Len(Trim(DBText(request("Track2")))) > 0 then isCardPresent = 1
fraudDetectionLog_id = 0
isTestOnly = 0
sTransactionTypeID = "0"
X_deviceId = TestNumVar(Request("deviceId"), 0, -1, "")
X_SendVerifyByPhone = False
X_DateOfBirth = Request("DateOfBirth")
PaymentMethod = 0 : ccTypeID = 0
If X_CartId <> "" Then If Len(X_CartId) > 10 Then X_CartId = ExecScalar("Select Cart_id From Data.Cart Where Identifier='" & Replace(X_CartId, "'", "''") & "'", "")

If Trim(sIP) <> "" Then
	IpArray = split(sIP,".")
	for i=LBound(IpArray) to UBound(IpArray)
		IpArray(i)=TestNumVar(IpArray(i), 0, 255, 0)
	next
	if UBound(IpArray)>2 then
		 If IpArray(0)=12 AND IpArray(1)=14 Then
				'Block IP range 12.14.114.0 - 12.14.115.255
				If IpArray(2)=114 OR IpArray(2)=115	Then Call throwError("582")
		 End if
	 End if
End if

'20091203 Tamir - For merchants in Integration status - no risk checks performed
If nActiveStatus = CMPS_INTEGRATION Then
	bIsCheckDailyLimts = False
	bIsCheckBlockCCNum = False
	bIsCheckCommonBlock = False
	bIsUseMaxMind = False
	IsUsePPWList = False
	bIsCheckAllowedAmounts = False
	nMultiChargeProtectionMins = 0
End If

'20090706 Tamir - when capturing pre-auth. transaction from recurring series, copy RecurringSeries and RecurringCharge to the passed transaction
nApprovalRecurringSeries = "NULL"
nApprovalRecurringChargeNumber = "NULL"

If trim(X_TransType)="4" And X_RefTransID <> "" Then '3DSecure
	X_Is3dSecure = True
	bIsValidateEmail = False
	sTransactionTypeID = "17"
	X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, -1)
	X_Amount = TestNumVar(request("Amount"), 0, -1, 0)
	bIsUseMaxMind = false 'Overide merchant configuration
	bIsCheckDailyLimts = False

	sSQL="SELECT trans_Amount, trans_Currency, t.InsertDate," & _
	" trans_Payments, trans_CreditType, PayforText, MerchantProduct_id, trans_order, t.Comment, DebitReferenceCode, DebitApprovalNumber, DebitReferenceNum, AcquirerReferenceNum, TerminalNumber," & _
	" dbo.GetDecrypted256(CCard_number256) CCard_number, ExpMM, ExpYY, Member, dbo.GetCUI(cc_cui) cc_cui," & _
	" PersonalNumber, phoneNumber, Email, address1, address2, city," & _
	" zipCode, tblBillingAddress.stateIso, tblBillingAddress.countryIso, BillingAddressID, PaymentMethodID FROM tblCompanyTransPending t" & _
	" INNER JOIN tblCreditCard ON CreditCardID=tblCreditCard.ID Left JOIN tblBillingAddress ON BillingAddressId=tblBillingAddress.ID" & _
	" WHERE t.ID=" & X_RefTransID & " AND t.CompanyID=" & sCompanyID
	set rsData3 = oledbData.execute(sSQL)
	If rsData3.EOF Then
		rsData3.Close
		Call throwError("536")
	ElseIf X_Amount-rsData3("trans_Amount")*1.1>0 Then '20091230 Tamir - capture amount can be up to 10% higher than authorized amount
		rsData3.Close
		Call throwError("538")
	Else
		'20091230 Tamir - use existing billing address & credit card records
		'nExistingBillingAddressID=TestNumVar(rsData3("BillingAddressID"), 1, 0, 0)
		'nExistingCreditCardID=TestNumVar(rsData3("BillingAddressID"), 1, 0, 0)
		RefOAmount = rsData3("trans_Amount")
		If X_Amount = 0 Then X_Amount = rsData3("trans_Amount")
		X_OrderNumber = rsData3("trans_order")
		X_PayFor = rsData3("PayforText")
		X_ProductId = rsData3("MerchantProduct_id")
		X_Currency = TestNumVar(rsData3("trans_Currency"), 0, MAX_CURRENCY, MAX_CURRENCY+1)
		X_ccCVV2 = rsData3("cc_cui")
		X_PersonalNumber = trim(rsData3("PersonalNumber"))
		X_PhoneNumber = trim(rsData3("phoneNumber"))
		X_Email = trim(rsData3("Email"))
		X_Payments = TestNumVar(rsData3("trans_Payments"), 0, -1, 1)
		X_PayerName = "" 'trim(rsData3("UserName"))
		X_ccExpMM = DBText(rsData3("ExpMM"))
		X_ccExpYY = DBText(rsData3("ExpYY"))
		X_ccHolderName = trim(rsData3("Member"))
		X_ConfirmNumber = trim(rsData3("DebitApprovalNumber"))
		X_Customer = 0
		X_TypeCredit = TestNumVar(rsData3("trans_CreditType"), 0, 8, -1)
		X_Comment = trim(rsData3("Comment"))
		X_DebitReferenceCode = trim(rsData3("DebitReferenceCode"))
		X_DebitReferenceNum = trim(rsData3("DebitReferenceNum"))
		X_AcquirerReferenceNum = trim(rsData3("AcquirerReferenceNum"))
		X_RefTransInsertDate = rsData3("InsertDate")
		sTerminalNumber = trim(rsData3("TerminalNumber"))
		X_ccNumber = rsData3("CCard_number")
		BACHAddr1 = trim(rsData3("address1"))
		BACHAddr2 = trim(rsData3("address2"))
		BACity = trim(rsData3("city"))
		BAZipCode = trim(rsData3("zipCode"))
		BAState = trim(rsData3("stateIso"))
		BACountry = trim(rsData3("countryIso"))
		If Trim(BACountry) = "0" Then BACountry = ""
		'20090706 Tamir - when capturing pre-auth. transaction from recurring series, copy RecurringSeries and RecurringCharge to the passed transaction
		if sTerminalNumber <> "" Then
			bIsCheckTerminalUsabe = false
			sTerminalChargeOptions = rsData3("trans_Currency")
			CCF_ID = 0
		End If
	End if
	rsData3.close
	Set rsData3 = nothing
	X_CartId = ExecScalar("Select Cart_id From Data.Cart Where TransPending_id=" & X_RefTransID, "")
	If X_CartId <> "" Then CartStockChanged = True
ElseIf trim(X_TransType)="3" Then
	'----------------------------------------------------------------------
	'	Collecting data from stored credit card
	'----------------------------------------------------------------------
	bIsValidateEmail = rsData2("IsRemoteChargeEmail")

	if Not rsData2("IsCcStorageCharge") Then
		rsData2.Close()
		ThrowError 549
	End if

	sTransactionTypeID = "28"
	sCcStorageID = TestNumVar(request("ccStorageID"), 0, -1, 0)
	X_ccCVV2 = DBText(request("CVV2"))

	sSQL="SELECT CHFullName, CHPersonalNum, CHPhoneNumber, CHEmail, ExpMM, ExpYY, cc_cui, CHStreet, CHStreet1, CHSCity, CHSZipCode, stateId, countryId, dbo.GetDecrypted256(CCard_number256) AS CCard_number " & _
	"FROM tblCCStorage WHERE (id = "& sCcStorageID &") AND ((" & MerchantGroupID & " = 0 AND CompanyID = "& sCompanyID &") OR ("& MerchantGroupID &" > 0 AND CompanyID IN (SELECT ID FROM tblCompany WHERE GroupID = "& MerchantGroupID &")))"
	set rsData3 = oledbData.execute(sSQL)
	if not rsData3.EOF Then
		X_ccHolderName = trim(rsData3("CHFullName"))
		X_PersonalNumber = trim(rsData3("CHPersonalNum"))
		X_PhoneNumber = trim(rsData3("CHPhoneNumber"))
		X_Email = trim(rsData3("CHEmail"))
		X_ccExpMM = DBText(rsData3("ExpMM"))
		X_ccExpYY = DBText(rsData3("ExpYY"))
		If X_ccCVV2 = "" Then X_ccCVV2 = DecCVV(rsData3("cc_cui"))
		X_ccNumber = rsData3("CCard_number")
		BACHAddr1 = DBText(rsData3("CHStreet"))
		BACHAddr2 = DBText(rsData3("CHStreet1"))
		BACity = DBText(rsData3("CHSCity"))
		BAZipCode = DBText(rsData3("CHSZipCode"))
		BAState = DBText(rsData3("stateId"))
		BACountry = DBText(rsData3("countryId"))
		If Trim(BACountry) = "0" Then BACountry = ""
	End if
	rsData3.close
	Set rsData3 = nothing
	X_Customer = 0
	X_ConfirmNumber = ""
	X_PayerName = ""
	If X_OrderNumber = "" Then X_OrderNumber = DBText(request("OrderNumber")) 'fix old parameter name orderNumber istead of order
	
	If X_ccHolderName = "" Then X_ccHolderName = DBText(request("Member")) 
	If X_PersonalNumber = "" Then X_PersonalNumber = DBText(request("PersonalNum")) 
	If X_PhoneNumber = "" Then X_PhoneNumber = DBText(request("PhoneNumber")) 
	If X_Email = "" Then X_Email = DBText(request("Email")) 
	
	X_Amount = TestNumVar(request("Amount"), 0, -1, 0)
	X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, MAX_CURRENCY+1)
   
	If X_Currency > MAX_CURRENCY Then X_Currency = ExecScalar("SELECT [CurrencyID] FROM [List].[CurrencyList] WHERE [CurrencyISOCode]='" & DBText(request("Currency")) & "'", MAX_CURRENCY+1)
	
    X_TypeCredit = TestNumVar(Request("TypeCredit"), 0, 8, -1)
	X_Payments = TestNumVar(Request("Payments"), 0, 254, 0)
	X_Comment = ""
	if DBText(request("Comment"))<>"" Then
		X_Comment = DBText(request("Comment"))
		X_Comment = Replace(Replace(X_Comment, ">", "&gt;"), "<", "&lt;")
	End if
	If Len(X_ccNumber) < 6 Then ThrowError "506"
	'Billing data
	If request("BillingAddress1") <> "" Then BACHAddr1 = DBText(request("BillingAddress1"))
	If request("BillingAddress2") <> "" Then BACHAddr2 = DBText(request("BillingAddress2"))
	If request("BillingCity") <> "" Then BACity = DBText(request("BillingCity"))
	If request("BillingZipCode") <> "" Then BAZipCode = DBText(request("BillingZipCode"))
	If request("BillingState") <> "" Then BAState = DBText(request("BillingState"))
	If request("BillingCountry") <> "" Then BACountry = DBText(request("BillingCountry"))
ElseIf trim(X_TransType)="2" Then
	'----------------------------------------------------------------------
	'	Collecting data from pre-auth transaction
	'----------------------------------------------------------------------
	sTransactionTypeID = "17"
	X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, MAX_CURRENCY + 1)
	If X_Currency > MAX_CURRENCY Then X_Currency = ExecScalar("SELECT [CurrencyID] FROM [List].[CurrencyList] WHERE [CurrencyISOCode]='" & DBText(request("Currency")) & "'", MAX_CURRENCY+1)
	sApprovalOnlyID = TestNumVar(request("TransApprovalID"), 1, 0, 0)
	If sApprovalOnlyID < 1 Then Call throwError("536")
	X_Amount = TestNumVar(request("Amount"), 0, -1, 0)
	bIsUseMaxMind = false 'Overide merchant configuration
	bIsCheckDailyLimts = False
	bIsCheckBlockCCNum = False
	bIsValidateEmail = False
	nMultiChargeProtectionMins=0
	'20091209 Tamir - new code 538 (for capturing higher amount than authorized); old code 536 remains for invalid (not found) TransApprovalID
	sSQL="SELECT IsNull(RecurringSeries, 0) RecurringSeries, IsNull(RecurringChargeNumber, 0) RecurringChargeNumber, Amount, Currency, t.InsertDate," & _
	" Payments, CreditType, OrderNumber, PayforText, MerchantProduct_id, t.Comment, DebitReferenceCode, DebitReferenceNum, AcquirerReferenceNum, approvalNumber, TerminalNumber," & _
	" dbo.GetDecrypted256(CCard_number256) CCard_number, ExpMM, ExpYY, Member, dbo.GetCUI(cc_cui) cc_cui," & _
	" PersonalNumber, phoneNumber, Email, address1, address2, city," & _
	" zipCode, tblBillingAddress.stateIso, tblBillingAddress.countryIso, BillingAddressID, PaymentMethodID FROM tblCompanyTransApproval t" & _
	" INNER JOIN tblCreditCard ON CreditCardID=tblCreditCard.ID Left Join tblBillingAddress ON BillingAddressId=tblBillingAddress.ID" & _
	" WHERE t.ID=" & sApprovalOnlyID & " AND t.CompanyID=" & sCompanyID & " AND Currency=" & X_Currency
	set rsData3 = oledbData.execute(sSQL)
	If rsData3.EOF Then
		rsData3.Close
		Call throwError("536")
	ElseIf X_Amount-rsData3("Amount")*1.1>0 Then '20091230 Tamir - capture amount can be up to 10% higher than authorized amount
		rsData3.Close
		Call throwError("538")
	Else
		'20091230 Tamir - use existing billing address & credit card records
		'nExistingBillingAddressID=TestNumVar(rsData3("BillingAddressID"), 1, 0, 0)
		'nExistingCreditCardID=TestNumVar(rsData3("BillingAddressID"), 1, 0, 0)
		RefOAmount = rsData3("Amount")
		If X_Amount = 0 Then X_Amount = rsData3("Amount")
		X_OrderNumber = rsData3("OrderNumber")
		X_PayFor = rsData3("PayforText")
		X_ProductId = rsData3("MerchantProduct_id")
		X_ccCVV2 = rsData3("cc_cui")
		X_PersonalNumber = trim(rsData3("PersonalNumber"))
		X_PhoneNumber = trim(rsData3("phoneNumber"))
		X_Email = trim(rsData3("Email"))
		X_Payments = TestNumVar(rsData3("Payments"), 0, -1, 1)
		X_PayerName = "" 'trim(rsData3("UserName"))
		X_ccExpMM = DBText(rsData3("ExpMM"))
		X_ccExpYY = DBText(rsData3("ExpYY"))
		X_ccHolderName = trim(rsData3("Member"))
		X_ConfirmNumber = trim(rsData3("approvalNumber"))
		X_Customer = 0
		X_TypeCredit = TestNumVar(rsData3("CreditType"), 0, 8, -1)
		X_Comment = trim(rsData3("Comment"))
		X_DebitReferenceCode = trim(rsData3("DebitReferenceCode"))
		X_DebitReferenceNum = trim(rsData3("DebitReferenceNum"))
		X_AcquirerReferenceNum = trim(rsData3("AcquirerReferenceNum"))
		X_RefTransInsertDate = rsData3("InsertDate")
		sTerminalNumber = trim(rsData3("TerminalNumber"))
		X_ccNumber = rsData3("CCard_number")
		BACHAddr1 = trim(rsData3("address1"))
		BACHAddr2 = trim(rsData3("address2"))
		BACity = trim(rsData3("city"))
		BAZipCode = trim(rsData3("zipCode"))
		BAState = trim(rsData3("stateIso"))
		BACountry = trim(rsData3("countryIso"))
		If Trim(BACountry) = "0" Then BACountry = ""
		'20090706 Tamir - when capturing pre-auth. transaction from recurring series, copy RecurringSeries and RecurringCharge to the passed transaction
		If rsData3("RecurringSeries")>0 Then
			nApprovalRecurringSeries = rsData3("RecurringSeries")
			nApprovalRecurringChargeNumber = rsData3("RecurringChargeNumber")
		End If
		if sTerminalNumber <> "" Then
			bIsCheckTerminalUsabe = false
			sTerminalChargeOptions = rsData3("Currency")
			CCF_ID = 0
		End If
	End if
	rsData3.close
	Set rsData3 = nothing
Else
	'----------------------------------------------------------------------
	'	Collecting data to charge a regular or approval only transaction
	'----------------------------------------------------------------------
	If X_TransType="1" AND NOT Merchant_EnableAuthOnly Then 
		rsData2.Close(): Call throwError("504")
	End if	

	bIsValidateEmail = rsData2("IsRemoteChargeEmail")

	sTransactionTypeID = requestSource
	X_Customer = TestNumVar(request("CustomerID"), 1, 0, 0)
	X_ccCVV2 = DBText(request("CVV2"))
	X_PersonalNumber = DBText(request("PersonalNum"))
	X_PhoneNumber = DBText(request("PhoneNumber"))
	X_Email = DBText(trim(request("Email")))
	X_TypeCredit = TestNumVar(Request("TypeCredit"), 0, 8, -1)
	X_Payments = TestNumVar(Request("Payments"), 0, 254, 0)
	X_PayerName = "" 'DBText(request("UserName"))
	X_ccExpMM = DBText(request("ExpMonth"))
	X_ccExpYY = DBText(request("ExpYear"))
	X_Amount = TestNumVar(request("Amount"), 0, -1, 0)
	X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, MAX_CURRENCY+1)
	If X_Currency > MAX_CURRENCY Then X_Currency = ExecScalar("SELECT [CurrencyID] FROM [List].[CurrencyList] WHERE [CurrencyISOCode]='" & DBText(request("Currency")) & "'", MAX_CURRENCY+1)
	X_ccHolderName = DBText(Trim(request("Member")))
	X_ConfirmNumber = DBText(request("ConfirmationNum"))
	X_Comment = DBText(request("Comment"))
	X_ccNumber = DBText(request("CCard_num1") & request("CCard_num2") & request("CCard_num3") & request("CCard_num4"))
	If X_ccNumber = "" Then 
		if Not IsNumericString(request("CardNum"), 0, 50) Then throwError("507")
		X_ccNumber = DBText(request("CardNum"))
		If Len(X_ccNumber) > 20 Then throwError("507")
	End If
	X_RetURL = DBText(request("RetURL"))
	X_Track2 = DBText(request("Track2"))
	'Billing data
	BACHAddr1 = DBText(request("BillingAddress1"))
	BACHAddr2 = DBText(request("BillingAddress2"))
	BACity = DBText(request("BillingCity"))
	BAZipCode = DBText(request("BillingZipCode"))
	BAState = DBText(request("BillingState"))
	BACountry = DBText(request("BillingCountry"))
	If trim(X_TypeCredit)="0" And (X_RefTransID = 0) Then Call throwError("532") 'refund request must include original trans id
	If trim(X_TypeCredit)="0" And (X_RefTransID <> 0) Then
		'Load Original Trans Details From X_RefTransID
		bIsValidateEmail = False
		Dim RefOAmount
		If X_TransType = "1" Then
			Set rsData3 = oledbData.execute("Select t.ID, t.InsertDate, t.DebitReferenceCode, t.DebitReferenceNum, t.AcquirerReferenceNum, t.ApprovalNumber, t.TerminalNumber, t.Currency, t.Amount, '0' as deniedStatus, " & _
				" dbo.GetDecrypted256(tblCreditCard.CCard_number256) as decCard ,tblCreditCard.*, tblBillingAddress.* " & _
				" From tblCompanyTransApproval t" & _
				" Left Join tblCreditCard ON(t.CreditCardID = tblCreditCard.ID) " & _
				" Left Join tblBillingAddress ON(tblCreditCard.BillingAddressID = tblBillingAddress.ID) " & _
				" Where t.ID=" & X_RefTransID & " And t.companyID=" & sCompanyID)
		Else
			Set rsData3 = oledbData.execute("Select t.ID, t.InsertDate, t.DebitReferenceCode, t.DebitReferenceNum, t.AcquirerReferenceNum, t.ApprovalNumber, t.TerminalNumber, t.Currency, t.Amount, t.deniedStatus, " & _
				" dbo.GetDecrypted256(tblCreditCard.CCard_number256) as decCard ,tblCreditCard.*, tblBillingAddress.* " & _
				" From tblCompanyTransPass t" & _
				" Left Join tblCreditCard ON(t.CreditCardID = tblCreditCard.ID) " & _
				" Left Join tblBillingAddress ON(tblCreditCard.BillingAddressID = tblBillingAddress.ID) " & _
				" Where t.ID=" & X_RefTransID & " And t.CompanyID=" & sCompanyID & " And t.deniedStatus Not IN(5, 6) Order By t.ID Asc")
		End If
		If Not rsData3.EOF Then
			X_DebitReferenceCode = rsData3("DebitReferenceCode")
			X_DebitReferenceNum = rsData3("DebitReferenceNum")
			X_AcquirerReferenceNum = rsData3("AcquirerReferenceNum")
			X_RefTransInsertDate = rsData3("InsertDate")
			X_ConfirmNumber = rsData3("ApprovalNumber")
			sTerminalNumber = rsData3("TerminalNumber")
			RefOAmount = rsData3("Amount")
			OriginalTransID = rsData3("ID")
			OriginalTransDeniedStatus = rsData3("deniedStatus")

			If X_Amount = 0 Then X_Amount = rsData3("Amount")
			If X_Currency = MAX_CURRENCY + 1 Then X_Currency = rsData3("Currency")
			'try to complete transaction details.
			If X_ccCVV2 = "" Then X_ccCVV2 = DecCVV(Trim(rsData3("cc_cui")))
			If X_ccExpMM = "" Then X_ccExpMM = Trim(rsData3("ExpMM"))
			If X_ccExpYY = "" Then X_ccExpYY = "20" & Trim(rsData3("ExpYY"))
			If X_ccNumber = "" Then X_ccNumber = rsData3("decCard")
			If X_ccHolderName = "" Then X_ccHolderName = Trim(rsData3("Member"))
			If X_PersonalNumber = "" Then X_PersonalNumber = Trim(rsData3("PersonalNumber"))
			If X_PhoneNumber = "" Then X_PhoneNumber = Trim(rsData3("phoneNumber"))
			If X_Email = "" Then X_Email = Trim(rsData3("email"))
			If BACHAddr1 = "" Then BACHAddr1 = rsData3("address1")
			If BACHAddr2 = "" Then BACHAddr2 = rsData3("address2")
			If BACity = "" Then BACity = rsData3("city")
			If BAZipCode = "" Then BAZipCode = rsData3("zipCode")
			If BAState = "" Then BAState = rsData3("stateId")
			If BACountry = "" Then 
				BACountry = rsData3("countryId")
				If Trim(BACountry) = "0" Then BACountry = ""
			End If
			'RefOAnswer = rsData3("Answer") '(answer) no longer exist in trans tables, RefOAnswer was used only for JCC2
			if sTerminalNumber <> "" Then
				sTerminalChargeOptions = rsData3("Currency")
				CCF_ID = 0
				bIsCheckTerminalUsabe = false
				bIsCheckBlockCCNum = false
				bIsCheckCommonBlock = false
			End if	
		End if
		rsData3.Close
		Set rsData3 = Nothing
		If X_TransType = "1" Then
			'RefAmount = TestNumVar(ExecScalar("Select Sum(Amount) From tblCompanyTransApproval Where CreditType=0 And OriginalTransID=" & X_RefTransID, 0), 0, -1, 0) + X_Amount
		Else
			RefAmount = TestNumVar(ExecScalar("Select Sum(OAmount) From tblCompanyTransPass Where CreditType=0 And DeniedStatus <> 5 And OriginalTransID=" & X_RefTransID, 0), 0, -1, 0) + X_Amount
		End If
		if RefAmount-RefOAmount>0 Then throwError("533")
	ElseIf Trim(X_TypeCredit)<>"0" And (X_RefTransID <> 0) Then
		'Load first Trans Details From X_RefTransID
		Dim nRowFound : nRowFound = False
		sSQL="SELECT dbo.GetDecrypted256(CCard_number256) CNumber, dbo.GetCUI(cc_Cui) CUI FROM tblCreditCard WHERE ID IN" & _
		"(SELECT PaymentMethodID FROM tblCompanyTransPass WHERE ID=" & X_RefTransID & " AND CompanyID=" & sCompanyID & ")"
		Set rsData3 = oledbData.Execute(sSQL)
		If rsData3.EOF Then
			'20090817 Tamir - allow taking CVV and cardnumber from TransApproval, if not found in TransPass
			rsData3.Close
			sSQL="SELECT dbo.GetDecrypted256(CCard_number256) CNumber, dbo.GetCUI(cc_Cui) CUI FROM tblCreditCard WHERE ID IN" & _
			"(SELECT PaymentMethodID FROM tblCompanyTransApproval WHERE ID=" & X_RefTransID & " AND CompanyID=" & sCompanyID & ")"
			Set rsData3 = oledbData.Execute(sSQL)
		End If
		If Not rsData3.EOF Then
			If Replace(rsData3("CNumber"), " ", "") = X_ccNumber Then 
				nRowFound = True
				If X_ccCVV2 = "-1" Then X_ccCVV2 = rsData3("CUI")
			End If	
		End If
		rsData3.Close
		Set rsData3 = Nothing
		If Not nRowFound Then throwError("532")
	End If
End If

'extract trackII
If X_Track2 <> "" and InStr(X_Track2, "=") > 1 Then
	Dim parseResult, ccNum
	parseResult = ParseTrackII(X_Track2)
	ccNum = parseResult(1)
	If X_ccNumber <> "" And ccNum <> X_ccNumber Then ThrowError "506"
	X_ccNumber = ccNum
	X_ccExpYY = parseResult(2)
	X_ccExpMM = parseResult(3)

	'Dim lEqIndex, ccNum
	'lEqIndex = InStr(1, X_Track2, "=")
	'If InStr(1, X_Track2, "=") > 0 Then
		'ccNum = Left(X_Track2, lEqIndex - 1)
		'If Left(ccNum, 1) = ";" Then ccNum = Right(ccNum, Len(ccNum) - 1)
		'If X_ccNumber <> "" And ccNum <> X_ccNumber Then ThrowError "506"
		'X_ccNumber = ccNum
		'X_ccExpYY = Mid(X_Track2, lEqIndex + 1, 2)
		'X_ccExpMM = Mid(X_Track2, lEqIndex + 3, 2)
	'End If
End If
If X_Currency > MAX_CURRENCY Then ThrowError "506"

If X_StoreCc Then
	if not IsNumeric(X_ccExpMM) then Call throwError("506")
	if not IsNumeric(X_ccExpYY) then Call throwError("506")

	dim expMonth, expYear
	expMonth = cint(X_ccExpMM)
	if len(X_ccExpYY) = 2 then 
		expYear = cint("20" + X_ccExpYY)
	else
		expYear = cint(X_ccExpYY)
	end if

	if expMonth > 12 or expMonth < 1 then Call throwError("506")
	if expYear < year(date) then Call throwError("510")
End If

'Recurring data
nRecurringCount=0
if trim(request("Recurring1"))<>"" then
	do
		nRecurringCount=nRecurringCount+1
	loop until trim(request("Recurring" & trim(nRecurringCount)))=""
	nRecurringCount=nRecurringCount-1
	'20111117 Tamir - limit recurring stages
	If nRecurringLimitStages>0 And nRecurringCount-nRecurringLimitStages>0 Then ThrowError 555
end if
dim saRecurring()
if nRecurringCount>0 then
	Dim dtRecurringFinish : dtRecurringFinish = Now()
	Dim nRecurringChargeCount : nRecurringChargeCount = 0
	Dim bLastStage : bLastStage = False
	redim saRecurring(nRecurringCount)
	for i=1 to nRecurringCount
		If nRecurringCount - i = 0 Then bLastStage = True
		sRecurring=trim(request("Recurring" & trim(i)))
		if sRecurring="" or not IsValidRecurringString(sRecurring) then Call throwError("551") 'Recurring parameters incomplete
		'20111117 Tamir - limit recurring charges
		If nRecurringLimitCharges>0 Then
			'limit recurring charges
			nRecurringChargeCount=nRecurringChargeCount+RecurringGetChargeCount(sRecurring)
			If nRecurringChargeCount-nRecurringLimitCharges>0 Then ThrowError 555 '20111025 Tamir - 
		End If
		'20111117 Tamir - limit recurring years
		If nRecurringLimitYears>0 Then
			'limit recurring years
			dtRecurringFinish=DateAddRecurring(dtRecurringFinish, sRecurring, Not bLastStage)
			If DateDiff("d", dtRecurringFinish, DateAdd("yyyy", nRecurringLimitYears, Now())) < 0 Then ThrowError 555 '20111025 Tamir - limit recurring to 10 years
		End If
		saRecurring(i)=sRecurring
	next
else
	' check, if there is an old style recurring (Count+Mode+Cycle)
	X_RecurringCount = dbText(request("RecurringCount")) 'how many tims
	X_RecurringCycle = dbText(request("RecurringCycle")) 'every how many modes
	X_RecurringMode = dbText(request("RecurringMode"))	 '1=day, 2=week, 3=month, 4=year
	if trim(X_RecurringMode)="0" and trim(X_RecurringCycle)="0" and trim(X_RecurringCount)="0" then
		X_RecurringCount=dbText("")
		X_RecurringCycle=dbText("")
		X_RecurringMode=dbText("")
	end if
	'20080615 Tamir - recurring mode can be also a word or its first letter (case insensitive)
	if X_RecurringMode<>"" then
		if not IsNumeric(TestNumVar(X_RecurringMode, 1, 4, "A")) then
			if StrComp(X_RecurringMode, "DAY", 1)=0 or StrComp(X_RecurringMode, "D", 1)=0 then
				X_RecurringMode=1
			elseif StrComp(X_RecurringMode, "WEEK", 1)=0 or StrComp(X_RecurringMode, "W", 1)=0 then
				X_RecurringMode=2
			elseif StrComp(X_RecurringMode, "MONTH", 1)=0 or StrComp(X_RecurringMode, "M", 1)=0 then
				X_RecurringMode=3
			elseif StrComp(X_RecurringMode, "YEAR", 1)=0 or StrComp(X_RecurringMode, "Y", 1)=0 then
				X_RecurringMode=4
			else
				Call throwError("551") 'Recurring parameters incomplete
			end if
		end if
	end if
	If trim(X_RecurringCount)<>"" OR trim(X_RecurringCycle)<>"" OR trim(X_RecurringMode)<>"" Then
		If NOT rsData2("IsAllowRecurring") Then 
			rsData2.Close(): Call throwError("550")
		End if	
		If trim(X_RecurringCount)="" OR trim(X_RecurringCycle)="" OR	trim(X_RecurringMode)="" Then Call throwError("551") 'Recurring parameters incomplete
		If NOT IsNumeric(X_RecurringCount) OR	NOT IsNumeric(X_RecurringCycle) OR	NOT IsNumeric(X_RecurringMode) Then Call throwError("551") 'Recurring parameters incomplete
	End if
	' if there is a valid old style recurring (Count+Mode+Cycle), convert to the new one
	sRecurring=trim(X_RecurringCount)
	select case trim(X_RecurringMode)
		case "1" : sRecurring=sRecurring & "D"
		case "2" : sRecurring=sRecurring & "W"
		case "3" : sRecurring=sRecurring & "M"
		case "4" : sRecurring=sRecurring & "Y"
	end select
	sRecurring=sRecurring & trim(X_RecurringCycle)
	if sRecurring="" then
		nRecurringCount=0
	elseif not IsValidRecurringString(sRecurring) then
		Call throwError("551")
	else
		'20111117 Tamir - limit recurring charges
		If nRecurringLimitCharges>0 Then
			'limit recurring charges
			If RecurringGetChargeCount(sRecurring)-nRecurringLimitCharges>0 Then ThrowError 555
		End If
		'20111117 Tamir - limit recurring years
		If nRecurringLimitYears>0 Then
			'limit recurring years
			If DateDiff("d", DateAddRecurring(Now(), sRecurring, True), DateAdd("yyyy", nRecurringLimitYears, Now())) < 0 Then ThrowError 555 '20111025 Tamir - limit recurring to 10 years
		End If
		nRecurringCount=1
	end if
	redim saRecurring(nRecurringCount)
	if nRecurringCount>0 then saRecurring(1)=sRecurring
end if

'20110331 Tamir - prevent parallel charge attempts with same card at same merchant - decline with code 546
If IsParallelChargeAttempt() Then ThrowError 546

if Not (X_TypeCredit = 0 Or X_TypeCredit = 1 Or X_TypeCredit = 6 Or X_TypeCredit = 8) Then 
	X_TypeCredit = 1
	Call throwError("506")
End if	

If (X_Payments = 0) And (Request("Payments") <> "") Then throwError("506")
If X_Payments = 0 Then X_Payments = 1
If X_Payments > 1 And (X_TypeCredit <> 6 And X_TypeCredit <> 8) Then Call throwError("506")
'20101031 Tamir - check, if refund processing is allowed
If (X_TypeCredit = 0) And Not (bIsRefund Or requestSource = "6") Then ThrowError "508"

'20091230 Tamir - When capturing, do not apply CC validations
If X_TransType<>2 Then
	if X_ccExpMM<>"" then X_ccExpMM = right("0" & trim(X_ccExpMM),2)
	if X_ccExpYY<>"" then X_ccExpYY = right(X_ccExpYY,2)
	if X_TypeCredit = 0 Then nMultiChargeProtectionMins = 0 'Overide merchant configuration

	if X_PersonalNumber<>"" and len(X_PersonalNumber)>50 then call throwError("508")
	if X_PhoneNumber<>"" and len(X_PhoneNumber)>50 then call throwError("508")
	if X_Email<>"" and len(X_Email)>80 then call throwError("508")
	'if X_PayerName<>"" and len(X_PayerName)>100 then call throwError("508")
	if TestNumVar(X_ccExpMM, 1, 12, -1)<0 then call throwError("506")
	if TestNumVar(X_ccExpYY, 1, 0, -1)<0 then call throwError("506")
	if X_ccHolderName<>"" and len(X_ccHolderName)>100 then call throwError("508")
	if X_Comment<>"" and len(X_Comment)>500 then call throwError("508")

	'20100920 Tamir - When refunding, do not check CC expiration date
	If X_TypeCredit <> 0 Then
		if X_ccExpYY - (Year(Now()) mod 2000) > 10 then call throwError("510")
		if DateSerial(2000 + CInt(X_ccExpYY), CInt(X_ccExpMM) + 1, 1) < Date() Then call throwError("510")
	End If
	If X_DateOfBirth <> "" Then 
		If Len(X_DateOfBirth) <> 8 Then call throwError("508")
		Err.Clear()
		On Error Resume Next
		X_DateOfBirth = DateSerial(Mid(X_DateOfBirth, 1, 4), Mid(X_DateOfBirth, 5, 2), Mid(X_DateOfBirth, 7, 2))
		If Err.number <> 0 Then call throwError("508")
		On Error Goto 0
		Dim clientAge : clientAge = DateDiff("yyyy", X_DateOfBirth, Now())
		If clientAge < 18 Or clientAge > 120  Then call throwError("508")
	End If
End If

'20110324 Tamir - validate transaction amount length
If InStr(X_Amount, "E") > 0 Then ThrowError 539 '20110330 Tamir - decline on scientific notation
If IIf(InStr(X_Amount, ".") > 0, InStr(X_Amount, ".")-1, Len(X_Amount)) > 8 Then ThrowError 539
   
'20100701 Tamir - validate systems max transaction amount
sSQL = "SELECT [MaxTransactionAmount] FROM [List].[CurrencyList] WITH (NOLOCK) WHERE CurrencyID=" & TestNumVar(X_Currency, 0, MAX_CURRENCY, MAX_CURRENCY + 1)
nCurrencyAmountMax = ExecScalar(sSQL, 0)
If nCurrencyAmountMax > 0 And X_Amount - nCurrencyAmountMax > 0 Then
	X_Amount = 0
	ThrowError 530
End If

'----------------------------------------------------------------------
'	check merchant permissions by request source
'	only for non local transactions
'----------------------------------------------------------------------
If canChargeLocally Then
	Select Case Trim(requestSource)
		case "5", "24", "27"
			sIsAllow3DTrans = false
			bIsUseMaxMind = false
			bIsCheckDailyLimts = false
			bIsBillingAddressMust = false
			bIsValidateEmail = False
		Case "6", "26"
			'Refund ask admin page, Refund debit cards (toggle)
			bIsCheckDailyLimts = false 'Overide page defult
			bIsUseMaxMind = false 'Overide merchant configuration
			nMultiChargeProtectionMins = 0 'Overide merchant configuration
			bIsCheckBlockCCNum = false 'Overide page defult
			sIsAllow3DTrans = false
			bIsCheckCountryBlacklist = false
			bIsValidateEmail = False
			bIsValidateFullName = False
	End select
Else
	Select Case requestSource
		Case "7", "18"
			'manual charge hebrew, manual charge english
			If NOT rsData2("IsSystemPay") Then Call throwError("502")
			If rsData2("IsSystemPayPhoneNumber") AND X_PhoneNumber = "" Then Call throwError("514")
			If rsData2("IsSystemPayEmail") AND X_Email = "" Then Call throwError("515")
			If requestSource="7" AND rsData2("IsSystemPayPersonalNumber") AND X_PersonalNumber = "" Then Call throwError("513")
			'20100307 Tamir - Skip fraud detection for manual transactions, if the checkbox in Risk is checked
			If rsData2("IsSkipFraudDetectionInVirtualTerminal") Then bIsUseMaxMind = False
			bIsValidateEmail = rsData2("IsSystemPayEmail")
		'Case "9", "11"
		'	'popup charge english, popup charge hebrew
		'	If NOT rsData2("IsCustomerPurchase") Then Call throwError("502")
		'	If rsData2("IsCustomerPurchasePhoneNumber") AND X_PhoneNumber = "" Then Call throwError("514")
		'	If rsData2("IsCustomerPurchaseEmail") AND X_Email = "" Then Call throwError("515")
		'	If requestSource<>"9" Then
		'		If rsData2("IsCustomerPurchasePersonalNumber") AND X_PersonalNumber = "" Then Call throwError("513")
		'	End If
		'	bIsValidateEmail = rsData2("IsCustomerPurchaseEmail")
		Case "32", "33", "40"
			'Hosted Page V2
			Dim hpsMerchant : Set hpsMerchant = New HostedPageSettings
			hpsMerchant.LoadMerchant sCompanyID, "IsCreditCard"
			If Not hpsMerchant.IsActive Then ThrowError "502"
			If hpsMerchant.IsRequiredCVV And X_TypeCredit <> 0 And X_ccCVV2 = "" Then ThrowError "512"
			bIsValidateEmail = hpsMerchant.IsRequiredEmail
			If hpsMerchant.IsRequiredPhone And X_PhoneNumber = "" Then ThrowError "514"
			If hpsMerchant.IsRequiredID And X_PersonalNumber = "" Then ThrowError "513"
			Set hpsMerchant = Nothing
		'Case "12", "19"
		'	'public charge english, public charge hebrew
		'	If NOT rsData2("IsPublicPay") Then Call throwError("502")
		'    If TestStrVar(X_Track2, 0, -1, "") = "" Then
		'		If rsData2("IsPublicPayCVV2") AND X_TypeCredit <> 0 AND X_ccCVV2 = "" Then Call throwError("512")
		'	End If	
		'	If rsData2("IsPublicPayPhoneNumber") AND X_PhoneNumber = "" Then Call throwError("514")
		'	If rsData2("IsPublicPayEmail") AND X_Email = "" Then Call throwError("515")
		'	If requestSource <> "19" Then
		'		If rsData2("IsPublicPayPersonalNumber") AND X_PersonalNumber = "" Then Call throwError("513")
		'	End if
		'	bIsValidateEmail = rsData2("IsPublicPayEmail")
		Case "8", "42"
			'remote charge
			If sTransactionTypeID<>"19" AND sTransactionTypeID<>"17" Then
				If NOT rsData2("IsRemoteCharge") Then Call throwError("502")
				'20090817 Tamir - If no CVV and there is X_RefTransID - search in TransPass; if not found, search in TransApproval
				If trim(X_ccCVV2) = "" And TestNumVar(X_RefTRansID, 1, 0, 0) > 0 Then
					sSQL="SELECT dbo.GetCUI(cc_cui) FROM tblCreditCard WHERE ID=(SELECT PaymentMethodID FROM tblCompanyTransPass" & _
					" WHERE ID=" & X_RefTRansID & " AND CompanyID=" & sCompanyID & ")"
					sSQL2="SELECT dbo.GetCUI(cc_cui) FROM tblCreditCard WHERE ID=(SELECT PaymentMethodID FROM tblCompanyTransApproval" & _
					" WHERE ID=" & X_RefTRansID & " AND CompanyID=" & sCompanyID & ")"
					X_ccCVV2=ExecScalar(sSQL, ExecScalar(sSQL2, ""))
				End If
				If TestStrVar(X_Track2, 0, -1, "") = "" Then
					If rsData2("IsRemoteChargeCVV2") AND X_TypeCredit <> 0 AND X_ccCVV2 = "" Then Call throwError("512")
				End If    
				If rsData2("IsRemoteChargePersonalNumber") AND X_PersonalNumber = "" Then Call throwError("513")
				If rsData2("IsRemoteChargePhoneNumber") AND X_PhoneNumber = "" Then Call throwError("514")
				If requestSource = "42" Then sIsAllow3DTrans = false
			End if
			If rsData2("IsRequiredClientIP") And ClientIP = "" Then Call throwError("558") 'change t
			bIsValidateEmail = rsData2("IsRemoteChargeEmail")
		Case "41" ' mobile app
			Set rsData3 = oledbData.execute("Select * From Setting.SetMerchantMobileApp Where Merchant_id=" & sCompanyID)
			If rsData3.EOF Then 
				rsData3.Close()
				ThrowError "502"
			End If
			If Not rsData3.EOF Then 
				If Not rsData3("IsEnableMobileApp") Then 
					rsData3.Close()
					ThrowError "502"
				End If
				If rsData3("IsRequirePersonalNumber") AND X_PersonalNumber = "" Then Call throwError("513")
				If rsData3("IsRequirePhoneNumber") AND X_PhoneNumber = "" Then Call throwError("514")
				Merchant_EnableAuthOnly = rsData3("IsAllowAuthorization")
				bIsValidateEmail = rsData3("IsRequireEmail")
				bIsBillingAddressMust = False
				If TestStrVar(X_Track2, 0, -1, "") = "" Then
					bIsValidateFullName = True
					If rsData3("IsRequireCVV") AND X_TypeCredit <> 0 AND X_ccCVV2 = "" Then Call throwError("512")
				Else 
					bIsValidateFullName = rsData3("IsRequireFullName")
				End If    
			End If
			rsData3.Close()
		Case "20", "29"
			'recurring page, AutoCapture
			sIsAllow3DTrans = false
			bIsUseMaxMind = false	
			bIsValidateEmail = False
		Case else
			'invalid request source
			call throwError("526")
	End select
End if
rsData2.Close()

'20110915 Udi - cancel this check if from refund request
'20110904 Tamir - if email is specified and invalid - decline with code 515
If bIsValidateEmail Or Trim(X_Email) <> "" Then
	If Not IsValidEmail(X_Email) Then ThrowError "515"
End If

'20091230 Tamir - When capturing, do not validate CC fields
If X_TransType<>2 Then
	If NOT IsNumeric(X_Amount) OR X_Currency="" OR NOT IsNumericString(replace(X_ccNumber," ",""), 0, -1) Then
		Call throwError("506")
	End if
	if Not (IsNumericString(X_ccExpMM, 2, 2) And IsNumericString(X_ccExpYY, 2, 2) And IsNumericString(X_Payments, 1, 2) And IsNumericString(X_TypeCredit, 1, 1)) Then 
		Call throwError("506")
	End if
	If bIsValidateFullName And X_ccHolderName="" Then Call throwError("511")
End If
If Len(Trim(X_Amount)) > 9 Then ThrowError "505"
If CCur(X_Amount) < 0.01 Then Call throwError("505")
If Len(X_Comment) > 500 Then Call throwError("508")
If IsUseFraudDetection_MaxMind AND trim(ClientIP) = "" Then Call throwError("516")
X_OCurrency = X_Currency : X_OAmount = X_Amount

'----------------------------------------------------------------------
'	Check billing address fields
'----------------------------------------------------------------------
'20091230 Tamir - When capturing, do not validate billing address fields (20090110 Tamir - skip also when processing refund request)
' skip billing address risk for SystemRefund (6) and MobileApp (41)
If X_TransType<>2 and requestSource <> "6" and requestSource <> "41" Then
	If trim(BACHAddr1) <> "" AND Len(BACHAddr1) > 100 Then Call throwError("540")
	If trim(BACHAddr2) <> "" AND Len(BACHAddr2) > 100 Then Call throwError("540")
	If trim(BACity) <> "" AND Len(BACity) > 60 Then Call throwError("541")
	If trim(BAZipCode) <> "" AND Len(BAZipCode) > 15 Then Call throwError("542")
	If bIsBillingAddressMust And (requestSource <> "6") Then
		If trim(BACHAddr1) = ""	AND Not bIsBillingCityOptional Then Call throwError("540")
		If trim(BACity) = "" AND Not bIsBillingCityOptional Then Call throwError("541")
		If trim(BAZipCode) = "" Then Call throwError("542")
		If trim(BACountry) = "" Then Call throwError("544")
	End if
	If bIsBillingAddressMust OR trim(BACountry)<>"" Then
		If isNumeric(BACountry) Then
			'Country is in numeric format
			BACountryCode = ExecScalar("SELECT [CountryISOCode] FROM [List].[CountryList] WHERE CountryID = " & TestNumVar(BACountry, 1, 0, 0), "")
			if BACountryCode = "" Then throwError(544)
			BACountryId = BACountry
		Else
			'Country is in two letter iso format
			BACountryId = ExecScalar("SELECT CountryID FROM [List].[CountryList] WHERE [CountryISOCode] = '" & BACountry & "'", "")
			if BACountryId = "" Then throwError(544)
			BACountryCode = BACountry
		End if
	End if
	If (bIsBillingAddressMust AND Not bIsBillingCityOptional) OR trim(BAState)<>"" Then
		If trim(BACountryCode)="US" OR trim(BACountryCode)="CA" Then
			If Trim(BAState)<>"" AND isNumeric(BAState) Then
				'State is in numeric format
				BAStateCode = ExecScalar("SELECT StateISOCode FROM [List].[StateList] WHERE stateID = " & TestNumVar(BAState, 1, 0, 0), "")
				if BAStateCode = "" Then throwError(543)
				BAStateId = BAState
			Elseif Trim(BAState)<>"" AND NOT isNumeric(BAState) Then
				'State is in two letter iso format
				BAStateId = ExecScalar("SELECT stateID FROM [List].[StateList] WHERE StateISOCode = '" & BAState & "'", "")
				if BAStateId = "" Then throwError(543)
				BAStateCode = BAState
			Else
				call throwError("543")
			End if
		End if
	End if
	' 20120208 Tamir - Check if zip code is well-formed for certain country
	'If bIsBillingAddressMust And Trim(BAZipCode)<>"" And Trim(BACountryCode)<>"" Then
	'	If Not IsValidZipCode(BAZipCode, BACountryCode) Then ThrowError 557
	'End If
End If

'20081124 Tamir - insert to billing address table
'If TestNumVar(nExistingBillingAddressID, 1, 0, 0)>0 Then
'	'20091230 Tamir - for captures, reuse billing address
'	NewBillingAddressID=nExistingBillingAddressID
'Else
	on error resume next
		NewBillingAddressID=insertBillingAddress(BACountryCode, BAStateCode, BACountryId, BAStateId, BACity, BACHAddr1, BACHAddr2, BAZipCode)
	on error goto 0
	If NewBillingAddressID=0 AND bIsBillingAddressMust Then Call throwError(523)
'End If
'----------------------------------------------------------------------
'	Geting card info
'----------------------------------------------------------------------
X_ccNumber = Fn_CleanCCNumber(X_ccNumber)
isTestOnly = Fn_IsCreditCardTestOnly(X_ccNumber)
ccBINCountry = CCNumToCountry(X_ccNumber, PaymentMethod, ccBIN) 'Get iso country & cc type from bin
ccTypeEngName = ExecScalar("Select GD_Text From tblGlobalData Where GD_LNG=1 And GD_Group=" & GGROUP_PAYMENTMETHOD & " And GD_ID=" & TestNumVar(PaymentMethod, 1, 0, 0), "????")
ccTypeEngShow = ccTypeEngName & " .... " & Right(X_ccNumber, 4)
if requestSource = "24" Then isTestOnly = 1 'Admincash test transaction
ccTypeID = PaymentMethod - PMD_CC_UNKNOWN

X_ExpDate = DateSerial(TestNumVar(X_ccExpYY, 0, -1, 1901), TestNumVar(X_ccExpMM, 1, 12, 1), 1)
X_ExpDate = DateAdd("d", -1, DateAdd("m", 1, X_ExpDate))

'----------------------------------------------------------------------
'	Encrypt cc number
'----------------------------------------------------------------------
ccNumberTmp = trim(mid(x_ccNumber,1,4) & " " & mid(x_ccNumber,5,4) & " " & mid(x_ccNumber,9,4) & " " & mid(x_ccNumber,13,4) & " " & mid(x_ccNumber,17,4))
sCCardNum_Encrypt = Replace(x_ccNumber, "'", "''")

'----------------------------------------------------------------------
'	Check cc number is legal (skip check if isracard or direct)
'----------------------------------------------------------------------
If PaymentMethod <> PMD_CC_ISRACARD And PaymentMethod <> PMD_CC_DIRECT And PaymentMethod <> PMD_CC_LIVECASH Then
	If NOT fn_IsValidCCNum(X_ccNumber, true, sCompanyID, 8, X_Amount, X_Currency) Then
		Call throwError("507") 'Card number is not legal
	End if
End if

'----------------------------------------------------------------------
'	Risk Mng - block when there are too many CCs with the same email
'----------------------------------------------------------------------
If nActiveStatus <> CMPS_INTEGRATION Then
	If Len(X_Email) > 0 and nLimitCcForEmailAllowedCount > 0 Then
		Dim nRiskMailCcCount : nRiskMailCcCount = ExecScalar("SELECT Risk.fnGetCcMailUsageCount('" & X_Email & "', '" & sCCardNum_Encrypt & "', 0)", 0)	
		'response.Write(X_Email&":"&nLimitCcForEmailAllowedCount&":"&nRiskMailCcCount&"<br>")
		If nRiskMailCcCount - nLimitCcForEmailAllowedCount > 0 Then
			If bIsLimitCcForEmailBlockEmail Then oledbData.Execute "EXEC Risk.spInsertBlacklistItem " & sCompanyID & ", " & eBL_Email & ", '" & X_Email & "', 'RiskMailCc';"
			If bIsLimitCcForEmailBlockNewCc Then oledbData.Execute "EXEC Risk.spInsertBlacklistCreditCard " & sCompanyID & ", '" & sCCardNum_Encrypt & "', 'RiskMailCc';"
			If bIsLimitCcForEmailBlockAllCc Then oledbData.Execute "EXEC Risk.spInsertBlacklistCreditCardByEmail " & sCompanyID & ", '" & X_Email & ", 'RiskMailCc';"
			'If bIsLimitCcForEmailNotifyByEmail Then NotifyMerchantRiskMailCc
			If bIsLimitCcForEmailDeclineTrans Then ThrowError 547
		End If
	End If
End If

'----------------------------------------------------------------------
'	Risk Mng - block when there are too many emails with the same cc
'----------------------------------------------------------------------
If nActiveStatus <> CMPS_INTEGRATION Then
	If Len(X_Email) > 0 and nLimitEmailForCcAllowedCount > 0 Then
		Dim nRiskCcMailCount : nRiskCcMailCount = ExecScalar("SELECT Risk.fnGetCcMailUsageCount('" & X_Email & "', '" & sCCardNum_Encrypt & "', 1)", 0)
		'response.Write(X_Email&":"&nLimitEmailForCcAllowedCount&":"&nRiskCcMailCount&"<br>")
		If nRiskCcMailCount - nLimitEmailForCcAllowedCount > 0 Then
			If bIsLimitEmailForCcBlockCc Then oledbData.Execute "EXEC Risk.spInsertBlacklistCreditCard " & sCompanyID & ", '" & sCCardNum_Encrypt & "', 'RiskCcMail';"
			If bIsLimitEmailForCcBlockNewEmail Then oledbData.Execute "EXEC Risk.spInsertBlacklistItem " & sCompanyID & ", " & eBL_Email & ", '" & X_Email & "', 'RiskCcMail';"
			If bIsLimitEmailForCcBlockAllEmails Then oledbData.Execute "EXEC Risk.spInsertBlacklistItemByCc " & sCompanyID & ",'" & sCCardNum_Encrypt & "', 'RiskCcMail';"
			'If bIsLimitEmailForCcNotifyByEmail Then NotifyMerchantRiskCcMail
			If bIsLimitEmailForCcDeclineTrans Then ThrowError 547
		End If
	End If
End If

'----------------------------------------------------------------------
'	Risk Mng - block when data in common black list
'----------------------------------------------------------------------
If bIsCheckCommonBlock Then
	nReply = ExecScalar("SELECT GD_Color FROM tblBLCommon INNER JOIN tblGlobalData ON BL_Type=GD_ID AND GD_Group=47 AND GD_LNG=1 WHERE" & _
	" ((BL_Value='" & X_Email & "' And BL_Type=" & eBL_Email & ") OR (BL_Value='" & X_ccHolderName & "' And BL_Type=" & eBL_FullName & ")" & _
	" OR (BL_Value='" & X_PhoneNumber & "' And BL_Type=" & eBL_Phone & ") OR (BL_Value='" & X_PersonalNumber & "' And BL_Type=" & eBL_PNumber & ")" & _
	" OR (BL_Value='" & ccBIN & "' And BL_Type=" & eBL_BIN & ") OR (BL_Value='" & ccBINCountry & "' And BL_Type=" & eBL_BINCountry & ")" & _
	" OR (BL_Value='" & sIP & "' And BL_Type=" & eBL_IP & ")) AND ISNULL(BL_CompanyID,0) IN (0, " & sCompanyID & ")", 0)
	If TestNumVar(nReply, 1, 0, 0) > 0 Then throwError(nReply)
End If

'----------------------------------------------------------------------
'	Risk Mng - block when card number in credit card black list
'----------------------------------------------------------------------
If bIsCheckBlockCCNum Then
	sSQL="IF EXISTS (SELECT 1 FROM tblFraudCcBlackList WHERE (fcbl_BlockLevel=1 OR (fcbl_BlockLevel=0 AND company_id=" & sCompanyID & ")) AND fcbl_ccNumber256 = dbo.GetEncrypted256(Replace('" & sCCardNum_Encrypt & "', ' ', ''))" & _
	" ) SELECT 1 ELSE SELECT 0"
	If ExecScalar(sSQL, 0) = 1 Then ThrowError(509)
End if

'20100511 Tamir - Skip parts of Risk Management for whitelisted CC
If bIsCcWhiteListEnabled Then
	sSQL = "SELECT * FROM CheckCreditCardWhiteList(" & sCompanyID & ", '" & sCCardNum_Encrypt & "', " & X_ccExpMM & ", " & X_ccExpYY & ");"
	Set rsData = oledbData.Execute(sSQL)
	If Not rsData.EOF Then
		nCreditCardWhitelistNameMatch = IIF((LCase(Replace(X_ccHolderName, " ", "")) = LCase(Replace(rsData("HolderName"), " ", ""))), "1", "0")
		If nCreditCardWhitelistNameMatch Then
			nCreditCardWhitelistID = rsData("ID")
			nCreditCardWhitelistLevel = rsData("Level")
		End If
	End If
	rsData.Close
End If
'0=Bronze, 1=Silver, 2=Gold
If nCreditCardWhitelistLevel >= 0 then bIsUseMaxMind = False
'20110511 Tamir - the following two lines disabled - requested by Rotem
'If nCreditCardWhitelistLevel >= 1 then bIsCheckCountryBlacklist = False
'If nCreditCardWhitelistLevel >= 2 then bIsCheckAllowedAmounts = False

'----------------------------------------------------------------------
'	Risk Mng - Check credit card limitations
'----------------------------------------------------------------------
If bIsCheckDailyLimts Then
	'Check black list for temporary blocks (CCRM) - 20090209 Tamir
	CheckForTempBlock sCCardNum_Encrypt, X_ccExpMM, X_ccExpYY, sCompanyID, (requestSource = "7" Or requestSource = "18")
	'Check approved transactions and block current try if needed
	CheckPassLimitation sCompanyID, X_Currency, PaymentMethod, X_TypeCredit, trim(sCCardNum_Encrypt), X_Amount, nCreditCardWhitelistLevel, (requestSource = "7" Or requestSource = "18")
End if

'----------------------------------------------------------------------
'	Risk Mng - block when amount is not in merchant allowed list
'----------------------------------------------------------------------
'20091230 Tamir - When capturing, do not check if the amount is allowed
If bIsCheckAllowedAmounts and X_TransType<>2 and X_TypeCredit<>0 Then
	If Trim(AllowedAmounts) <> "" Then
		if InStr(1, "," & AllowedAmounts & ",", "," & X_Amount & ",") = 0 Then throwError("588")
	End if
End if

'----------------------------------------------------------------------
'	Make sure this transaction was not charged in the last X minuts
'----------------------------------------------------------------------
If nMultiChargeProtectionMins > 0 Then
	Dim paymentMethodStamp : paymentMethodStamp = Left(Trim(sCCardNum_Encrypt),6)
	If paymentMethodStamp = "" Then paymentMethodStamp = "NULL"
	
	sSQL="SELECT CAST(CASE WHEN EXISTS(SELECT 1 FROM [Track].[ProcessApproved] WHERE" & _
	" DATEDIFF(mi, TransDate, Convert(datetime, '" & Now & "', 103))<=" & nMultiChargeProtectionMins & " AND Merchant_id=" & sCompanyID & _
	" AND TransIpAddress='" & sIP & "' AND TransAmount=" & X_Amount & " AND CreditCardNumber256=dbo.GetEncrypted256('" & trim(sCCardNum_Encrypt) & "') AND TransCurrency=" & X_Currency & _
	" AND PaymentMethodStamp=" & paymentMethodStamp & " AND TransInstallments=" & X_Payments & ") THEN 1 ELSE 0 END AS BIT)"
	If ExecScalar(sSQL, false) Then throwError "522"
End if

'20100225 Tamir - check daily volume limit (regardless the currency, passed debits only)
'If bIsCheckDailyVolumeLimit And X_TypeCredit > 0 Then
'	sSQL = "SELECT Sum(Amount) FROM tblCompanyTransPass WITH (NOLOCK) WHERE InsertDate>DateAdd(day, -1, GetDate()) AND CreditType>0" & _
'	" AND dbo.IsChb(DeniedStatus, isTestOnly)=0 AND CompanyID=" & sCompanyID
'	If ExecScalar(sSQL, 0) - nDailyVolumeLimit > 0 Then throwError "525"
'End If

TimeControl.Append("A_RM") 'Time Optimization *********

'----------------------------------------------------------------------
'	Insert data to credit card table (20081124 Tamir)
'----------------------------------------------------------------------
'If TestNumVar(nExistingPaymentMethodID, 1, 0, 0)>0 Then
'	nNewCreditCardID=nExistingCreditCardID
'Else
	If TestNumVar(nNewCreditCardID, 0, -1, 0) = 0 Then 
		On Error Resume Next
		nNewCreditCardID = insertCard(sCCardNum_Encrypt, X_ccExpMM, X_ccExpYY, X_ccHolderName, X_PayerName, X_ccCVV2, X_PhoneNumber, X_Email, sCompanyID, X_PersonalNumber, X_DateOfBirth, ccTypeID, X_Comment, NewBillingAddressID, ccBINCountry)
		On Error Goto 0
	End If
	If clng(nNewCreditCardID)=0 Then Call throwError(523)
'End If

TimeControl.Append("A_CC") 'Time Optimization *********


'----------------------------------------------------------------------
'	Risk Mng - Use MaxMind Fraud Detection service
'----------------------------------------------------------------------
If X_TransType = 2 Then bIsUseMaxMind = false '20100126 Tamir - on capture don't use MaxMind
If bIsUseMaxMind Then
	%>
	<!--#include file="remoteCharge_ccFraudMaxMind.asp"-->
	<%
Else 
	'20100721 UDI - check IP country if not use maxmind 
	sIPCountryCode = GetIPGeoCountry(sIP)
End if

'----------------------------------------------------------------------
'	Risk Mng - block when country in black list
'----------------------------------------------------------------------
If bIsCheckCountryBlacklist And Trim(sCountryBlackList) <> "" Then
	If Trim(ccBINCountry) <> "--" And Len(ccBINCountry) = 2 Then
		If Int(inStr("," & sCountryBlackList & ",", "," & ccBINCountry & ",")) > 0 Then throwError("581")
	End If

	If Trim(BACountryCode) <> "--" And Len(BACountryCode) = 2 Then
		If Int(inStr("," & sCountryBlackList & ",", "," & BACountryCode & ",")) > 0 Then throwError("581")
	End If

	'check IP Country against black list
	If Trim(sIPCountryCode) <> "--" And Len(sIPCountryCode) = 2 Then
		If Int(inStr("," & sCountryBlackList & ",", "," & sIPCountryCode & ",")) > 0 Then throwError("582")
	End If
End if

If bIsCheckCountryBlacklist And Trim(sStateBlackList) <> "" Then
	If Trim(BAState) <> "--" And Len(BAState) = 2 Then
		If Int(inStr("," & sStateBlackList & ",", "," & BAState & ",")) > 0 Then throwError("581")
	End If
End If

'----------------------------------------------------------------------
'	Risk Mng - Netpay fraud detection service 
'----------------------------------------------------------------------
	%>
	<!--#include file="remoteCharge_ccFraudDetection.asp"-->
	<%

BeginCartTransaction

TimeControl.Append("A_MM") 'Time Optimization *********
'----------------------------------------------------------------------
'	Find out terminal number to use
'----------------------------------------------------------------------
'PriorityList Setup
X_TerminalHasNext = False
X_TransINC = "X_TransINC_" & sCompanyID & "_" & X_ccNumber & "_" & X_Amount & "_" & X_Currency
XX_Amount = X_Amount
debitCompany = 1
If bIsCheckTerminalUsabe Then
	If nActiveStatus = CMPS_INTEGRATION Then
		sTerminalChargeOptions = X_Currency
		sTerminalNumber = "0000000"
		if X_CompanyNum="4307712" then sTerminalNumber = "0000002" '20090422 Tamir - manipulate amount (for Savoy in testing mode)
		bIsCheckDailyLimts = false : bIsUseMaxMind = false : nMultiChargeProtectionMins = 0
	Else
		If bUseOldTerminal Then
			CCF_ID = GetCompanyTerminal(sCompanyID, PaymentMethod, X_Currency, ccBINCountry, ccBIN, X_TypeCredit, sTerminalNumber, sTerminalChargeOptions)
			CCF_ID = sTerminalChargeOptions
		Else
			Dim mSendTrans : Set mSendTrans = New CSendTrans
			CCF_ID = mSendTrans.GetMerchantTerminal(sCompanyID, PaymentMethod, X_Currency, X_TypeCredit, 0, sTerminalNumber, sTerminalChargeOptions)
		End If
	End If
End If

If CCF_ID = -2 Then 'not found
	call throwError("503")
ElseIf CCF_ID = -3 Then 'incorrect custom terminal
	Call throwError("503") '537
Elseif CCF_ID = -1 Then 'blocked
	Call throwError("503")
Else
	If bUseOldTerminal Then
		If sTerminalChargeOptions <> X_Currency Then 'convert currency if needed
			X_Amount = ConvertCurrency(X_Currency, sTerminalChargeOptions, X_Amount)
			X_Currency = CCF_ID
		End If
		Call SendCharge()
	Else	
		Call fn_returnResponse(LogSavingChargeDataID, requestSource, debitCompany, X_TransType, X_DPRET, nTransactionID, nInsertDate, X_OrderNumber, Formatnumber(X_Amount,2,-1,0,0), X_Payments, X_Currency, dpApproval, X_Comment)
	End If
End If

'----------------------------------------------------------------------
'	Get terminal info using terminal number (inc debiting company)
'----------------------------------------------------------------------
Function GetDebitTerminalInfo(sTermNum)
	Dim sSQL : sSQL="SELECT dbo.GetDecrypted256(accountPassword256) As accountPassword, dbo.GetDecrypted256(accountPassword3D256) As accountPassword3D," & _
		" dc_isActive, dc_TempBlocks, tblDebitTerminals.* FROM tblDebitTerminals" & _
		" LEFT JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany=tblDebitCompany.debitCompany_id WHERE TerminalNumber='" & trim(sTermNum) & "'"
	Dim rsTerminal : Set rsTerminal = OleDbData.Execute(sSQL)
	GetDebitTerminalInfo = "---"
	If Not rsTerminal.EOF Then
		If Not rsTerminal("dc_isActive") Then GetDebitTerminalInfo = "592"
		If (Trim(requestSource) <> "6" Or CInt(X_TypeCredit) <> 0) And Not rsTerminal("isActive") Then GetDebitTerminalInfo = "592"
		If rsTerminal("dc_TempBlocks") > 0 Then GetDebitTerminalInfo = "524"
		terminalActive = rsTerminal("isActive") Or (Trim(requestSource) = "6" And CInt(X_TypeCredit) = 0)
		debitCompany = rsTerminal("DebitCompany")
		bIsNetpayTerminal = rsTerminal("isNetpayTerminal")
		bIsShvaMasterTerminal = rsTerminal("isShvaMasterTerminal")
		nProcessingMethod = rsTerminal("processingMethod")
		sMcc = rsTerminal("dt_mcc")
		isManipulateAmount = rsTerminal("dt_isManipulateAmount")
		isTestTerminal = rsTerminal("dt_IsTestTerminal")
		TerminalId = rsTerminal("id")
		sTerminalName = Trim(rsTerminal("dt_name"))
		sTermName = Trim(rsTerminal("dt_Descriptor"))
		dt_EnableRecurringBank = IIF(IsNull(rsTerminal("dt_EnableRecurringBank")), False, rsTerminal("dt_EnableRecurringBank"))
		dt_EnableAuthorization = IIF(IsNull(rsTerminal("dt_EnableAuthorization")), False, rsTerminal("dt_EnableAuthorization"))
		dt_Enable3dsecure = IIF(IsNull(rsTerminal("dt_Enable3dsecure")), False, rsTerminal("dt_Enable3dsecure"))
		dt_IsPersonalNumberRequired = IIF(IsNull(rsTerminal("dt_IsPersonalNumberRequired")), False, rsTerminal("dt_IsPersonalNumberRequired"))
		'terminal login
		sAccountId = Trim(rsTerminal("accountId"))
		sAccountSubId = Trim(rsTerminal("accountSubId"))
		sAccountAuthCode1 = Trim(rsTerminal("AuthenticationCode1"))
		if rsTerminal("accountPassword") <> "" Then sAccountPassword = rsTerminal("accountPassword")
		'terminal 3D login
		sAccountId3D = Trim(rsTerminal("accountId3D"))
		sAccountSubId3D = Trim(rsTerminal("accountSubId3D"))
		sAccountAuthCode13D = Trim(rsTerminal("AuthenticationCode3D"))
		If rsTerminal("accountPassword3D") <> "" Then sAccountPassword3D = rsTerminal("accountPassword3D")
	End If
	rsTerminal.close
	If isTestTerminal Then isTestOnly = 1
	If debitCompany = 20 Then bIsUseMaxMind = False
End Function

Function SendCharge()
	TimeControl.Append("A_TRM") 'Time Optimization *********
	
	If (sTerminalChargeOptions <> X_Currency) And (sTerminalChargeOptions <> 255) Then 'Convert Currency if needed
		X_Amount = ConvertCurrency(X_Currency, sTerminalChargeOptions, XX_Amount)
		X_Currency = sTerminalChargeOptions
	Else	
		X_Amount = XX_Amount 'Restore Amount
	End If

	X_DPRET = GetDebitTerminalInfo(sTerminalNumber)

	'validate cc by card type
	If X_TransType <> "2" And Trim(X_ccCVV2) <> "" Then
		'Below commented out by Zeev because AMEX can be 3 or 5 digits
		'If PaymentMethod = PMD_CC_AMEX And (debitCompany = 3 Or debitCompany = 8 Or debitCompany = 9 Or debitCompany = 11 Or debitCompany = 55 Or debitCompany = 56) Then
		'	If NOT IsNumericString(X_ccCVV2, 3, 3) Then Call throwError("559")
		'Else
			If NOT IsNumericString(X_ccCVV2, 3, IIF(PaymentMethod = PMD_CC_LIVECASH, 8, 5)) Then Call throwError("512")
		'End If
	End if
	
	If Trim(X_TransType) = "1" And Not dt_EnableAuthorization Then Call throwError("504")
	If Trim(dt_IsPersonalNumberRequired And Trim(X_PersonalNumber) = "") Then Call throwError("513")
	
	'----------------------------------------------------------------------
	'	Risk Mng - Block mcc 7995 when Bin is Unknown and country is USA
	'----------------------------------------------------------------------
	If Trim(sMcc) = "7995" And Trim(ccBINCountry) = "--" And UCase(BACountryCode) = "US" Then throwError("509")

	If X_DPRET <> "---" Then
		If bUseOldTerminal Then throwError(X_DPRET)
		DebitIProcessResult X_DPRET, ""
		SendCharge = False
		Exit Function
	End If	

	'----------------------------------------------------------------------
	'	Manipulate amount by terminal request (Subtract 0.01-0.05)
	'----------------------------------------------------------------------
	If isManipulateAmount And Trim(X_TypeCredit) <> "0" Then
		Randomize
		SubtractTmp = Round((Rnd * 0.04) + 0.01, 2)
		X_Amount = X_Amount - SubtractTmp
	End if

	'----------------------------------------------------------------------
	'	Proceed to debit company
	'----------------------------------------------------------------------
	If requestSource = "27" Then
		Call DebitIProcessResult(IIf(nProcessingMethod = 2, "001", "000"), "")
	ElseIf int(nProcessingMethod) = 2 Then
		Call DebitIProcessResult("001", "")
	Elseif int(nProcessingMethod) = 1 Then
		ExecSql("Update tblLogChargeAttempts Set Lca_DebitCompanyID=" & debitCompany & " Where LogChargeAttempts_id=" & LogSavingChargeDataID)
		'Send to debiting company
		Select Case int(debitCompany)
			case 1 : call debitNULL() 'NULL debitCompany
			case 3,8,9 : Call debitShva(0)			'Shva pelecard
			case 11, 55, 56 : Call debitShvaWS()	'Shva (direct) 'debitShva(1)
			case 18,46 : 
				If sIsAllow3DTrans And dt_Enable3dsecure Then Call debitBNS3D() _
				Else Call debitBNS(False)			'B+S
			case 20 : Call debitToggleCard()		'PPCards
			case 21 : Call debitModernProcessing()	'Invik / Catella
			case 26, 68 : 'PayOn, payon direct
				'If sIsAllow3DTrans And dt_Enable3dsecure Then Call debitPayOnCTPE() _
				'Else Call debitPayOn()				
				Call debitPayOnCTPE()
			case 34 : Call debitWireCard() 			'WireCard
			case 38 : Call debit_PPro() 			'PPro
			case 39 : Call debitWireCard3D()		'WireCard 3D
			case 40 : Call debitAllCharge()		    'AllCharge 3D
			case 44 : Call debitPayVision()		    'PayVision
			case 45 : Call debit_CredoRax()		    'CredoRax
			case 49 :                               'DeltaPay
				If sIsAllow3DTrans And dt_Enable3dsecure Then debit_DeltaPayHpp() _
				Else Call debit_DeltaPay()
			'case 51 : Call debitGlobalTechSystems()	'GlobalTechSystems
			case 52 : Call debit_LivaCash()	        'LiveCash
			case 58, 65 :							'Alied
				If sIsAllow3DTrans And dt_Enable3dsecure Then debitAllied3d() _
				Else Call debitAllied()
			case 59 : Call debitFirstData()	        'FirstData
			case 60 : Call debitCardConnect()       'Card Connect
			case 62 : Call debitYuuPay()			'YuuPay
			case 63 : Call debitLamda()				'Lamda
			case 64 : Call debitEpx()				'Exp
			'case 66 : Call debitTSys()				'TSys
			case 69 : Call debitCreditGuard()		'CreditGuard
			case 70, 71 : 
				Call debitAtlas("")                 'Atals new integraion
			case 72 : Call debitInternal()          'Internla prepaid cards
			case 73: Call debit_RemoteNP()          'debit remote netpay
			case 74: Call debit_PBS()               'debit PBS
			case 75: Call debitAlliedNew()          'debit Allied new
			case 76: 
				debitPayTechnique()
			case 77 : 
				Call debitACS("")                 'ACS new integraion
			case else : Call throwError("503") 		'Invalid debit company
		End Select
	End if
	SendCharge = (Trim(X_DPRET) = "000") Or (Trim(X_DPRET) = "001")
End Function
%>
<!--#include file="remoteCharge_ccFunctions.asp"-->
<!--#include file="remoteCharge_ccDebitWireCard3D.asp"-->
<!--#include file="remoteCharge_ccDebitWireCard.asp"-->
<!--#include file="remoteCharge_ccDebitBNS.asp"-->
<!--#include file="remoteCharge_ccDebitBNS3D.asp"-->
<!--#include file="remoteCharge_ccDebitInvik.asp"-->
<!--#include file="remoteCharge_ccDebitNULL.asp"-->
<!--#include file="remoteCharge_ccDebitToggleCard.asp"-->
<!--#include file="remoteCharge_ccDebitShva.asp"-->
<!--#include file="remoteCharge_ccDebitShvaWS.asp"-->
<!--#include file="remoteCharge_ccDebitAllCharge.asp"-->
<!--#include file="remoteCharge_ccDebitPPro.asp"-->
<!--#include file="remoteCharge_ccDebitPayVision.asp"-->
<!--#include file="remoteCharge_ccDebitCredoRax.asp"-->
<!--#include file="remoteCharge_ccDebitDeltaPay.asp"-->
<!--#include file="remoteCharge_ccDebitDeltaPayHpp.asp"-->
<!--#include file="remoteCharge_ccDebitLiveCash.asp"-->
<!--#include file="remoteCharge_ccDebitAllied.asp"-->
<!--#include file="remoteCharge_ccDebitAllied3d.asp"-->
<!--#include file="remoteCharge_ccDebitFirstData.asp"-->
<!--#include file="remoteCharge_ccDebitPayOn.asp"-->
<!--#include file="remoteCharge_ccDebitPayOnCTPE.asp"-->
<!--#include file="remoteCharge_ccDebitYuuPay.asp"-->
<!--#include file="remoteCharge_ccDebitCardConnect.asp"-->
<!--#include file="remoteCharge_ccDebitLamda.asp"-->
<!--#include file="remoteCharge_ccDebitEpx.asp"-->
<!--#include file="remoteCharge_ccDebitCreditGuard.asp"-->
<!--#include file="remoteCharge_ccDebitAtlas.asp"-->
<!--#include file="remoteCharge_ccDebitInternal.asp"-->
<!--#include file="remoteCharge_ccDebitRemoteNP.asp"-->
<!--#include file="remoteCharge_ccDebitPBS.asp"-->
<!--#include file="remoteCharge_ccDebitAlliedNew.asp"-->
<!--#include file="remoteCharge_ccDebitPayTechnique.asp"-->
<!--#include file="remoteCharge_ccDebitACS.asp"-->
