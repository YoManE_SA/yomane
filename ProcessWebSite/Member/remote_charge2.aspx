<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" %>
<script runat="server" language="vbscript">

    Const ForAppending = 8
    Const LOG_FOLDER = "C:\temp\ProcessWebsiteLogs\"
    Const LOG_FILE = "processWebsiteTracking"
    Const LOG_FILE_EXTENSION = ".log"
    Const LOG_FILE_SEPARATOR = "_"


    'Prefix zero if day or month is in single digit
    Function PrefixZero(num)
        If (Len(num) = 1) Then
            PrefixZero = "0" & num
        Else
            PrefixZero = num
        End If
    End Function

    'To get current date in ddMMyyyy format. 
    'Ex: 09 Apr 2016 wil be returned as 09042016 and 15 Oct 2016 will be returned as 15102016
    Function GetCurrentDate()
        Dim timeStamp = Now()
        Dim d = PrefixZero(Day(timeStamp))
        Dim m = PrefixZero(Month(timeStamp))
        Dim y = Year(timeStamp)
        GetCurrentDate = d & m & y
    End Function

    'Log message with timestamp
    Sub WriteLog(message)

        'Dim objFSO = CreateObject("Scripting.FileSystemObject")
        'Get log file with current date 
        Dim logFile = LOG_FOLDER & LOG_FILE & LOG_FILE_SEPARATOR & GetCurrentDate() & LOG_FILE_EXTENSION
        'Open log file in appending mode
        ' Dim objLogger = objFSO.OpenTextFile(logFile, ForAppending, True)
        'Prefix timestamp
        message = FormatDateTime(Now(), vbLongDate) & " " & FormatDateTime(Now(), vbLongTime) & " >>> " & message

        'Write log message
        If System.IO.File.Exists(logFile) = False Then
            System.IO.File.WriteAllText(logFile, message)
        Else
            System.IO.File.AppendAllText(logFile, message)
        End If

        'objLogger.WriteLine(message)
        ''Close file
        'objLogger.Close()
    End Sub


    Private Function ConvertUTF8To1255(ByVal rqVal As String) As String
        Dim strBytes() As Byte = Encoding.Unicode.GetBytes(rqVal)
        strBytes = Encoding.Convert(Encoding.Unicode, Encoding.GetEncoding("windows-1255"), strBytes)
        Dim asciiChars(Encoding.GetEncoding("windows-1255").GetCharCount(strBytes, 0, strBytes.Length)) As Char
        Encoding.GetEncoding("windows-1255").GetChars(strBytes, 0, strBytes.Length, asciiChars, 0)
        Return New String(asciiChars)
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            WriteLog("Initialize Page_Load method on remote_charge2.aspx")
            Dim sDataUnicode As String = Request.QueryString.ToString()
            WriteLog("sDataUnicode = " & sDataUnicode)
            If String.IsNullOrEmpty(sDataUnicode) Then sDataUnicode = Request.Form.ToString()
            WriteLog("sDataUnicode = " & sDataUnicode)
            Dim sData As String = ConvertUTF8To1255(sDataUnicode)
            WriteLog("sData = " & sData)
            Dim sURL As String = Request.Url.GetLeftPart(UriPartial.Authority) & ResolveUrl("remote_charge.asp")
            WriteLog("sURL = " & sURL)
            Dim sReply As String = String.Empty
            Response.StatusCode = dbPages.SendHttpRequest(sURL, sData, , sReply, Encoding.GetEncoding("windows-1255"))
            WriteLog("Response.StatusCode = " & Response.StatusCode)
            If Response.StatusCode = System.Net.HttpStatusCode.OK Then
                Dim responseToWrite = System.Web.HttpUtility.ParseQueryString(sReply, Encoding.GetEncoding("windows-1255")).ToString()
                Response.Write(responseToWrite)

                WriteLog("responseToWrite = " & responseToWrite)
            Else
                Response.Write(sReply)
            End If
            WriteLog("sReply = " & sReply)

        Catch ex As Exception
            WriteLog("ex = " & ex.ToString())

        End Try

    End Sub



</script>