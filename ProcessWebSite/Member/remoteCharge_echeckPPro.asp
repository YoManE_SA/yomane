<%
'---------------------------------------------------------------------
'	Debit PPro
'---------------------------------------------------------------------
Function remoteCharge_echeck_PPro_BackDetectAlways()
    If Request("cs") <> "" And Request("txid") <> "" Then   'test if reply from bank
        GetTransInfo("Lca_ReplyCode='553' And Lca_TransNum IN(Select companyTransPending_id From tblCompanyTransPending Where DebitApprovalNumber='" & TestNumVar(Request("txid"), 0, -1, 0) & "')")
        sRedirect = "remoteCharge_echeck.asp" & _
            "?CompanyNum=" & GetURLValue(xRequestParams, "CompanyNum") & _
            "&TransType=4" & _
            "&requestSource=18" & _
            "&ReplyURL=" & Server.URLEncode(GetURLValue(xRequestParams, "RetURL")) & _
            "&TransApprovalID=" & Request("txid") & _
            "&PaRes=" & Request("cs")
        Response.Redirect(sRedirect)
    End If
End Function

Function remoteCharge_echeck_PPro_BackDetect()
    Response.Redirect URLDecode(GetURLValue(xRetParams, "REDIRECTURL")) & "&PaReq=" & URLDecode(GetURLValue(xRetParams, "REDIRECTSECRET"))
End Function

Function debit_PPro()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer

	xNames = Split(Trim(X_ccHolderName), " ")
	if Ubound(xNames) < 1 Then Call throwError("517")
	fName = xNames(0) : sName = xNames(1)
	
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
	call HttpReq.setOption(3, "LOCAL_MACHINE\My\Udi Azulay")
	HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
    If isTestTerminal Then UrlAddress = "https://testgw.girogate.de" Else UrlAddress = "https://gw.girogate.de"
	If Cint(X_TransType) = 1 Then Call throwError("503")

	If trim(X_TransType) = "4" Then
		ParamList = "returnmode=urlencode" & vbCrLf & _
		"&txtype=GETTXSTATUS" & vbCrLf & _
		"&login=" & Server.URLEncode(sAccountSubId) & vbCrLf & _
		"&password=" & Server.URLEncode(sAccountPassword) & vbCrLf & _
		"&contractid=" & Server.URLEncode(sAccountID) & vbCrLf & _
		"&txid=" & Server.URLEncode(X_ConfirmNumber)
	ElseIf (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	Else
		Dim nTagArray, sChannnelName
		If Session("Identity") = "Local" Then nTagArray = Array("", "Giropay", "DirectPay", "dumbdummy", "dumbdummy", "dumbdummy", "dumbdummy", "dumbdummy", "dumbdummy", "dumbdummy", "dumbdummy", "dumbdummy", "dumbdummy", "dumbdummy") _
		Else nTagArray = Array("", "Giropay", "DirectPay", "PinELV", "PaySafeCard", "Cash_Ticket", "Pezelewy24", "EPS", "YellowPay", "Wallie", "iDEAL", "TeleIngreso")
		sChannnelName = nTagArray(PaymentMethod - PMD_EC_CHECK)
    	'sOuterIP = IIF(session("Identity") = "Local", "http://80.179.39.10:8080/member/", Server.URLEncode(Replace(session("ProcessURL"), "http://", "https://")))
    	sOuterIP = Session("ProcessURL")
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = "returnmode=urlencode" & vbCrLf & _
		"&txtype=TRANSACTION" & vbCrLf & _
		"&login=" & Server.URLEncode(sAccountSubId) & vbCrLf & _
		"&password=" & Server.URLEncode(sAccountPassword) & vbCrLf & _
		"&contractid=" & Server.URLEncode(sAccountID) & vbCrLf & _
		"&channel=testchannel" & vbCrLf & _
		"&tag=" & Server.URLEncode(LCase(sChannnelName)) & vbCrLf & _
		"&currency=" & GetCurISOName(Cint(X_Currency)) & vbCrLf & _
		"&amount=" & Server.URLEncode(CInt(X_Amount * 100)) & vbCrLf & _
		"&countrycode=" & Server.URLEncode(X_BankCountry) & vbCrLf & _
		"&preferredlanguage=en" & vbCrLf & _
		"&accountholdername=" & Server.URLEncode(X_ccHolderName) & vbCrLf & _
		"&merchantxid=" & Server.URLEncode(X_DebitReferenceCode) & vbCrLf & _
		"&merchantredirecturl=" & Server.URLEncode(sOuterIP & "remoteCharge_Back.asp") & vbCrLf & _
		"&merchanterrorurl=" & Server.URLEncode(sOuterIP & "remoteCharge_Back.asp") & vbCrLf & _
		"&notificationurl=" & Server.URLEncode(sOuterIP & "remoteCharge_Back.asp") & vbCrLf & _
		"&specin.accountnumber=" & Server.URLEncode(ckacct) & vbCrLf & _
		"&specin.bankcode=" & Server.URLEncode(ckaba) & vbCrLf & _
		"&specin.paymentpurpose=" & Server.URLEncode(sCompanyDescriptor) & vbCrLf & _
		"&specin.email=" & Server.URLEncode(X_Email) & vbCrLf & _
		"&specin.address=" & Server.URLEncode(BACHAddr1) & vbCrLf & _
		"&specin.zipcode=" & Server.URLEncode(BAZipCode) & vbCrLf & _
		"&specin.city=" & Server.URLEncode(BACity) & vbCrLf
	End If	
	'Response.Write(session("ProcessURL") & "<br>" & ParamList & vbCrLf & "<br>") : Response.End()
	ParamList = Replace(ParamList, vbCrLf, "")
	'On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
		HttpReq.Send(ParamList)
		TxtRet = HttpReq.responseText
		TxtRet = Replace(Replace(TxtRet, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba))
		SaveLogChargeAttemptRequestResponse Replace(Replace(ParamList, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba)), TxtRet
		HttpError = FormatHttpRequestError(HttpReq)
	'On Error GoTo 0
	If IsNull(TxtRet) Or Len(TxtRet) = 0 Then Call throwError("521")
	sReturnCode = GetURLValue(TxtRet, "STATUS")
	sApprovalNumber = GetURLValue(TxtRet, "TXID")
	If sReturnCode = "" Then Call throwError("520") 'internal error
	'Response.Write(TxtRet & "</br>END") : Response.End
	If sReturnCode = "SUCCEEDED" Then
		sReturnCode = "000"
	ElseIf sReturnCode = "PENDING" Then
		sReturnCode = "001"
		X_3dRedirect = GetURLValue(TxtRet, "REDIRECTURL")
		If (X_3dRedirect <> "") Then 
		    X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
			sReturnCode = "553"
			sIsSendUserConfirmationEmail = False
			'X_3dRedirect = URLDecode(X_3dRedirect) & "&PaReq=" & Request("REDIRECTSECRET")
		End If	
	Else
		sReturnCode = "002"
		X_CustomErrorDesc = GetURLValue(TxtRet, "ERRMSG")
	End If
	Set HttpReq = Nothing
	DebitIProcessResult sReturnCode, sApprovalNumber
End function
%>