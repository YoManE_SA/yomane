<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="remoteCharge_ccFunctions.asp"-->
<%
'---------------------------------------------------------------------
'	Debit Ecard (polish)
'---------------------------------------------------------------------
	Dim sReturnCode, LogSavingChargeDataID, rsData6, HttpReq, ReqSource
	LogSavingChargeDataID = TestNumVar(Request("LID"), 0, -1, 0)
	if LogSavingChargeDataID = 0 Then
		Response.Write("Session Timeout")
		Response.End
	End if
    Set HttpReq = CreateObject("Msxml2.ServerXMLHTTP.3.0")
	Set rsData6 = Server.CreateObject("Adodb.Recordset")

	'oleDbData.Execute "Update tblLog_Charge Set Reply='" & Replace(sReturnCode, "'", "''") & "' Where id=" & LogSavingChargeDataID
	rsData6.Open "Select * From tblLog_Charge Where id=" & LogSavingChargeDataID, oleDbData, 0, 1
	if Not rsData6.EOF Then
		sResDetails = rsData6("RequestForm")
		If sResDetails = "" Then sResDetails = rsData6("QUERY_STRING") _
		Else sResDetails = Right(sResDetails, Len(sResDetails) - 1)
		ReqSource = rsData6("TransactionTypeID")
		rsData6.Close
	End if
    HttpReq.open "POST", session("ProcessURL") & "remote_charge.asp?D3=1&" & sResDetails, false
	HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	HttpReq.Send "3D_DebitReferenceCode=" & TestNumVar(Request("RefCode"), 0, -1, 0) & _
			"&D3Ret=" & Server.UrlEncode(Request("PaRes"))
    
	resStr = HttpReq.responseText
	if ReqSource = 9 Or ReqSource = 11 Then
	    replyCode = Mid(resStr, 1, 3)
	    transId = Mid(resStr, 5, 7)
	    transDate = Mid(resStr, 12, 19)
	    replyDescription = Mid(resStr, 314, 100)
		Response.Redirect "window_charge_eng_cc_actionAnswer.asp?" & "replyCode=" & replyCode & "&transID=" & transId & "&transDate=" & transDate & "&replyDescription=" & replyDescription
	Else
		Response.Write(resStr)
	End if
%>