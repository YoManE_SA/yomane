<%
'---------------------------------------------------------------------
'	Debit JCC ()
'---------------------------------------------------------------------
Function LimitJCC25(sText)
   LimitJCC25 = Trim(Left(Trim(sText & Space(25)), 25))
End Function

function debitJCC()
   Dim HttpReq, UrlAddress, ParamList, fName, sName, EncodedSignature, sApprovalNumber, strAct', IsoCurreny
   'IsoCurreny = Array("0", "840", "978", "826")
   If Cint(X_Currency) = 0 Then Call throwError("506")
   xNames = Split(X_ccHolderName, " ")
   if Ubound(xNames) > -1 Then fName = xNames(0)
   if Ubound(xNames) > 0 Then sName = xNames(1)
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	if (X_TypeCredit = "0") Then
 		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans Not Found
 		X_DebitReferenceCode = LimitJCC25(X_DebitReferenceCode)
		UrlAddress = "https://www.jccsecure.com/SENTRY/PaymentGateway/Merchant/StoreFrontServices/FinancialService.asmx"
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
			"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
			"	<soap:Header>" & _
			"		<AuthHeader xmlns=""http://www.ctl.com/SENTRY/PaymentGateway/Merchant/StoreFrontServices"">" & _
			"			<MerchantID>" & sAccountSubId & "</MerchantID>" & _
			"			<AcquirerID>" & sAccountId & "</AcquirerID>" & _
			"			<Password>" & sAccountPassword & "</Password>" & _
			"		</AuthHeader>" & _
			"	</soap:Header>" & _
			"  <soap:Body>" & _
			"		<RefundAmount xmlns=""http://www.ctl.com/SENTRY/PaymentGateway/Merchant/StoreFrontServices"">" & _
			"			<OrderID>" & X_DebitReferenceCode & "</OrderID>" & _
			"			<Amount>" & formatNumber(X_Amount, 2) & "</Amount>" & _
			"			<Culture>en-US</Culture>" & _
			"		</RefundAmount>" & _
			"	</soap:Body>" & _
			"</soap:Envelope>"
		On Error Resume Next
			HttpReq.open "POST", UrlAddress, false
			HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
			HttpReq.setRequestHeader "SOAPAction", "http://www.ctl.com/SENTRY/PaymentGateway/Merchant/StoreFrontServices/RefundAmount"
			HttpReq.send ParamList
			sResDetails = HttpReq.responseText
		On Error GoTo 0
		if sResDetails <> "" Then 
			Set xmlRet = HttpReq.responseXml
			If IsNull(xmlRet.selectSingleNode("//RefundAmountResult")) Then ThrowError 520
			sReturnCode = Trim(xmlRet.selectSingleNode("//RefundAmountResult").text)
			If sReturnCode = "true" Then sReturnCode = "000" Else sReturnCode = "2"
			If IsNull(xmlRet.selectSingleNode("//sError")) Then ThrowError 520
			sError = Trim(xmlRet.selectSingleNode("//sError").text)
			Set xmlRet = Nothing
		End If
			'Response.Write(ParamList & vbcrlf & HttpReq.responseText & vbcrlf & "<br>" & sReturnCode & "<Br>" & sApprovalNumber)
		'Response.End
		'FileAppendData "JCC.txt", Now & vbcrlf & ParamList & vbcrlf & sResDetails & vbcrlf & vbcrlf & vbcrlf
	Else
		UrlAddress = "https://www.jccsecure.com/sentry/paymentgateway/application/directAuthzlink.aspx" //DirectLink.aspx
		If Cint(X_TransType) = 1 Then mSDep = "M" Else mSDep = "A"
		X_DebitReferenceCode = LimitJCC25(GetTransRefCode())
		'MerchantHash = MERCHANTPWD & MERCHANTID & ACQUIRERID & ORDERID & PURCAMNT & PURCHCURRENCY
		nAmnt = (formatNumber(X_Amount, 2) * 100)
		While Len(nAmnt) < 12
			nAmnt = "0" & nAmnt
		Wend
        
		EncodedSignature = Hash("MD5", sAccountPassword & sAccountSubId & sAccountId & X_DebitReferenceCode & nAmnt & GetCurISOCode(Cint(X_Currency))).Base64
		'Response.Write(EncodedSignature & "<br>" & Hash.Value.Base64 & "<br>")
		ParamList = "Version=1.0.0" & _
			"&MerID=" & sAccountSubId & _
			"&AcqID=" & sAccountId & _
			"&PurchaseCurrency=" & GetCurISOCode(Cint(X_Currency)) & _
			"&PurchaseCurrencyExponent=2" & _
			"&OrderID=" & X_DebitReferenceCode & _
			"&SignatureMethod=MD5" & _
			"&CardNo=" & Replace(X_ccNumber, " ", "") & _
			"&CardExpDate=" & X_ccExpMM & X_ccExpYY & _
			"&CardCVV2=" & X_ccCVV2 & _
			"&PurchaseAmt=" & nAmnt & _ 
	 		"&Signature=" & EncodedSignature & _
	 		"&CaptureFlag=" & mSDep
	 		'(formatNumber(X_Amount, 2) * 100)
		Set Hash = Nothing
		Set Context = Nothing
		Set CM = Nothing
		On Error Resume Next
		HttpReq.Open "POST", UrlAddress, False
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		HttpReq.setRequestHeader "Accept-Language", "en-US"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		On Error GoTo 0
		If Len(sResDetails) = 0 Then Call throwError("521") 'communication error
		If (X_TypeCredit <> "0") Then sResDetails = Replace(sResDetails, "&amp;", "&")
		sReturnCode = GetURLValue(sResDetails, "ResponseCode")
		If sReturnCode = "" Then Call throwError("520") 'internal error
		Select Case sReturnCode
		Case "1"
			sReturnCode = "000"
			sApprovalNumber = GetURLValue(sResDetails, "AuthCode")
		Case Else
			sReturnCode = GetURLValue(sResDetails, "ReasonCode")
			sError = GetURLValue(sResDetails, "ReasonCodeDesc")
		End Select
	End If	
	Set HttpReq = Nothing
	'Response.Write(ParamList) : Response.End 
 	Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
End Function
%>