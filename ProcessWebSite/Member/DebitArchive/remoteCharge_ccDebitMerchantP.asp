<%
'---------------------------------------------------------------------
'	            	debit m. partners 
'---------------------------------------------------------------------
function debitMPartners()
    ' set authorization only flag 
    MPActionType = ""
    MPAuthorizationOnly = 0      
    MPPostOnly = ""
    
    select case cInt(X_TypeCredit)
	    case 0
		    ' credit
		    MPActionType = "ns_credit"
		    MPAuthorizationOnly = 0
	    case 1
            MPActionType = "ns_quicksale_cc"
            
            select case cInt(x_transType)
	            case 0 
	                ' debit
		            MPAuthorizationOnly = 0
		            MPPostOnly = ""
	            case 1 
	                ' pre authorization
		            MPAuthorizationOnly = 1
		            MPPostOnly = ""
	            case 2 
	                ' debit preauthorized transaction
		            MPAuthorizationOnly = 0	
		            MPPostOnly = sApprovalOnlyID	        
	            case else
		            throwError("506")
            end select		    
	    case else
		    throwError("506")
    end select
    
    ' build request
    Dim sendStr
    sendStr = "https://trans.merchantpartners.com/cgi-bin/process.cgi?"
    sendStr = sendStr & "action=" & MPActionType & "&"
    sendStr = sendStr & "acctid=" & sAccountId & "&"
    sendStr = sendStr & "subId=" & sAccountSubId & "&"
    sendStr = sendStr & "amount=" & formatNumber(X_Amount, 2)  & "&"
    sendStr = sendStr & "ccname=" & X_ccHolderName & "&"
    sendStr = sendStr & "ccnum=" & replace(X_ccNumber, " ", "") & "&"
    sendStr = sendStr & "Authonly=" & MPAuthorizationOnly & "&"
    sendStr = sendStr & "postonly=" & MPPostOnly & "&"
    sendStr = sendStr & "expmon=" & X_ccExpMM & "&" ' mm
    sendStr = sendStr & "expyear=20" & X_ccExpYY ' yyyy
    
    ' add billing address fields       
    if rsData2("IsBillingAddressMust") then
        sendStr = sendStr & "&ci_billaddr1=" & BACHAddr1 & "&"
        sendStr = sendStr & "ci_billaddr2=" & BACHAddr2 & "&"
        sendStr = sendStr & "ci_billcity=" & BACity & "&"
        sendStr = sendStr & "ci_billstate=" & BAStateCode & "&"
        sendStr = sendStr & "ci_billcountry=" & BACountryCode & "&"
        sendStr = sendStr & "ci_billzip=" & BAZipCode 
    end if 

    'response.Write sendStr : response.End
    
    ' send reqest
    On Error Resume Next

        Dim HttpReq
        Set HttpReq = CreateObject("Msxml2.XMLHTTP.3.0")
        HttpReq.open "get", sendStr, False
        HttpReq.send()
	    sDebitReturnAnswer = HttpReq.responseText	
	    'response.Write sDebitReturnAnswer : response.End		
    	
    On Error GoTo 0

    '---------------------------------------------
    '       receive and parse the response
    '---------------------------------------------

    '----- check returned string length
    if len(sDebitReturnAnswer) = 0 or sDebitReturnAnswer = null then call throwError("521")

    '----- check for duplicate transacion
    searchResult = inStr(1, sDebitReturnAnswer, "DUPLICATE")
    if searchResult > 0 then call throwError("529")

    '----- build dictionary
    dim dict
    set dict = createObject("Scripting.Dictionary")
    responseStrArr = Split(sDebitReturnAnswer, vbcr)

    for each currentItem in responseStrArr
        if inStr(currentItem, "=") then
	        currentItemStr = replace(currentItem, vbCr, "")
	        currentItemStr = replace(currentItem, vbLf, "")

	        currentItemArr = split(currentItemStr, "=")
	        currentItemName = currentItemArr(0)
	        currentItemValue = currentItemArr(1)

	        if not dict.exists(currentItemName) then dict.add currentItemName, currentItemValue
        end if
    next

    '------------------------------------------------------------------------------------
    '			check for reply status
    '			missing reply status indicates an invalid reply
    '			and a communication problem
    '------------------------------------------------------------------------------------
    if not dict.exists("Status") then call throwError("521")

    '---------------------------------------------
    '             process the response
    '---------------------------------------------
    status = dict.Item("Status")
    accNumLast4 = replace(dict.Item("ACCOUNTNUMBER"),"*","")
    approvalNumber = dict.Item("refcode") 
    responseStrSql = replace(replace(replace(sDebitReturnAnswer, vbCr, ";"), vbLf, ""), "'", "''")
    responseCode = "000"
    
    if status = "Declined" then
        declinedArr = split(dict.Item("Declined"), ":")
        declinedCode = declinedArr(1)
        responseCode = Right(declinedCode, (len(declinedCode) - 1))
        declinedMsg = declinedArr(2)
    end if
    set dict = nothing

    if status = "Accepted" then responseCode = "000"
    
	Call DebitIProcessResult(responseCode, approvalNumber)	
end function
%>