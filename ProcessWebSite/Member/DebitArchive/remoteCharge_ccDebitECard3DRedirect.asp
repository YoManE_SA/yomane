<html>
<head>
    <title>Redirect to bank</title>
    <link rel="STYLESHEET" type="text/css" href="MainIE.css"/>
</head>
<body>
<br /><br /><br />
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
    <td align="center" class="txt14">
        You are being redirected to a credit card verification page<br />
    </td>
</tr>   
<tr><td height="20"></td></tr>
<tr>
    <td align="center" class="txt12">
        PLEASE WAIT ...<br />
    </td>
</tr>  
<tr><td height="4"></td></tr>
<tr>
	<td align="center">
	    <img src="../Images/img/loading_animation_orange2.gif" style="border:1px solid silver;" alt="" width="123" height="6" border="0"><br>
    </td>
</tr>
<tr><td height="60"></td></tr>
<tr>
    <td align="center" class="txt11">
        If you are not redirected automatically, then please <a href="#" onclick="document.frmRedirect.submit();" style="font-size:11px;">click here</a><br />
        <form action="<%=Request("Url")%>" method="post" name="frmRedirect">
         <input type="hidden" name="MD" value="<%=Request("MD")%>"><br>
         <input type="hidden" name="PaReq" value="<%=Request("PAReq")%>"><br>
         <input type="hidden" name="TermUrl" value="<%=Request("TermUrl")%>"><br>
        </form>
    </td>
</tr>   
</table>
</body>
</html>
<script language="JavaScript">
	setTimeout(window.onload = document.forms[0].submit(), 3500);
</script>