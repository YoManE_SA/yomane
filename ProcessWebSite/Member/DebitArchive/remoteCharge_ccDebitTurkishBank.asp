<% 
'---------------------------------------------------------------------
'	debit Turkish Bank
'---------------------------------------------------------------------
function debitTurkishBank()
    ' set authorization only flag 
	ActionType = "Auth"
    select case cInt(X_TypeCredit)
	    case 0
		    ' credit
		    ActionType = "Credit"
	    case 1
            select case cInt(x_transType)
	            case 0 
	                ' debit
		   			 ActionType = "Auth"
	            case 1 
	                ' pre authorization
		    		ActionType = "PreAuth"
	            case 2 
	                ' debit preauthorized transaction
		    		ActionType = "PostAuth"       
	            case else
		            throwError("506")
            end select		    
	    case else
		    throwError("506")
    end select
	
	
	If sTerminalNumber="7777" Then
		'TEST
		UrlAddress = "https://cc5test.est.com.tr/servlet/cc5ApiServer"
		CurrencyType = "949"
	Else
		'PRODUCTION
		UrlAddress = "https://spos.isbank.com.tr/servlet/cc5ApiServer"
		CurrencyType = "840"
	End if
	
	
	' Building string to send
	Dim XmlStr
	XmlStr = XmlStr & "<CC5Request>"
	XmlStr = XmlStr & "<Name>" & sAccountId & "</Name>"
	XmlStr = XmlStr & "<Password>" & sAccountSubId & "</Password>"
	XmlStr = XmlStr & "<ClientId>" & sTerminalNumber & "</ClientId>"
	XmlStr = XmlStr & "<Mode>P</Mode>"
	XmlStr = XmlStr & "<Type>" & ActionType & "</Type>"
	XmlStr = XmlStr & "<OrderId>" & X_OrderNumber & "</OrderId>"
	XmlStr = XmlStr & "<Number>" & replace(X_ccNumber, " ", "") & "</Number>"
	XmlStr = XmlStr & "<Expires>" & X_ccExpMM & "/" & X_ccExpYY & "</Expires>"
	XmlStr = XmlStr & "<Cvv2Val>" & X_ccCVV2 & "</Cvv2Val>"
	XmlStr = XmlStr & "<Total>" & formatNumber(X_Amount, 2) & "</Total>"
	If X_Currency = "1" then
		XmlStr = XmlStr & "<Currency>" & CurrencyType & "</Currency>" '840
	Else
		call throwError("531")
	End if      
    if rsData2("IsBillingAddressMust") then
		XmlStr = XmlStr & "<BillTo>"
			XmlStr = XmlStr & "<Name>" & X_ccHolderName & "</Name>"
			XmlStr = XmlStr & "<Street1>" & BACHAddr1 & "</Street1>"
			XmlStr = XmlStr & "<Street2>" & BACHAddr2 & "</Street2>"
			XmlStr = XmlStr & "<Street3>" & BAStateCode & "</Street3>"
			XmlStr = XmlStr & "<City>" & BACity & "</City>"
			XmlStr = XmlStr & "<PostalCode>" & BAZipCode & "</PostalCode>"
			XmlStr = XmlStr & "<Country>" & BACountryCode & "</Country>"
		XmlStr = XmlStr & "</BillTo>"
    end if 
	XmlStr = XmlStr & "</CC5Request>"
	
	'XmlStr = XmlStr & "<UserId>havva_baykara</UserId>"
	'XmlStr = XmlStr & "<email>bkutlu@est.com.tr</email>"
	'XmlStr = XmlStr & "<BillTo>"
	'	XmlStr = XmlStr & "<Name>havva_baykara</Name>"
	'	XmlStr = XmlStr & "<Street1>deneme</Street1>"
	'	XmlStr = XmlStr & "<City>ankara</City>"
	'	XmlStr = XmlStr & "<PostalCode>06700</PostalCode>"
	'	XmlStr = XmlStr & "<Country>792</Country>"
	'	XmlStr = XmlStr & "<TelVoice>0_312_446_50_07</TelVoice>"
	'XmlStr = XmlStr & "</BillTo>"
	
	Set SrvHTTPS = Server.CreateObject("MSXML2.ServerXMLHTTP")
	
	On Error Resume Next
		' Send data
		SrvHTTPS.open "POST",UrlAddress,false
		SrvHTTPS.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
		SrvHTTPS.send "DATA=" & XmlStr
		
		' Get response
		Set XMLReceive = Server.CreateObject("MSXML2.DOMDocument")
		Set XMLReceive = SrvHTTPS.responseXML
		sResponse = XMLReceive.selectSingleNode("/CC5Response/Response").text
		sReturnCode = XMLReceive.selectSingleNode("/CC5Response/ProcReturnCode").text
	On Error GoTo 0 
	
	' throw comunication error if the response code is other then ok (http 200)
	'If SrvHTTPS.status <> "200" then
	if len(sResponse) = 0 or sResponse = null then call throwError("521")
	
	' Transaction response details
	sResDetails = sResponse & "<br>"
	sResDetails = sResDetails & "Errmsg: " & XMLReceive.selectSingleNode("/CC5Response/ErrMsg").text & "<BR>"
	sResDetails = sResDetails & "OrderId: " & XMLReceive.selectSingleNode("/CC5Response/OrderId").text & "<BR>"
	sResDetails = sResDetails & "GroupId: " & XMLReceive.selectSingleNode("/CC5Response/GroupId").text & "<BR>"
	sResDetails = sResDetails & "AuthCode: " & XMLReceive.selectSingleNode("/CC5Response/AuthCode").text & "<BR>"
	sResDetails = sResDetails & "HostRefNum: " & XMLReceive.selectSingleNode("/CC5Response/HostRefNum").text & "<BR>"
	sResDetails = sResDetails & "ProcReturnCode: " & XMLReceive.selectSingleNode("/CC5Response/ProcReturnCode").text & "<BR>"
	sResDetails = sResDetails & "TransId: " & XMLReceive.selectSingleNode("/CC5Response/TransId").text & "<BR>"
	
	sApprovalNumber = XMLReceive.selectSingleNode("/CC5Response/AuthCode").text
	
	If sApprovalNumber<>"" Then
		If X_Comment<>"" then X_Comment = X_Comment & " ; "
		X_Comment = X_Comment & "AuthCode: " & sApprovalNumber
	End if

    Call DebitIProcessResult(sResponse, sApprovalNumber)
end function
%>