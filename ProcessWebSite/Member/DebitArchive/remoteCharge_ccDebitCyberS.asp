<% 
function debitCybertSystems()

	' Set debit only flag (capture and capture)
    select case cInt(X_TypeCredit)
	    case 1
            If cInt(x_transType)>0 then Call throwError("506")
	    case else
		    Call throwError("506")
    end select
	
	' get Cardholder first and last name
    chNameArr = split(X_ccHolderName, " ")
    if ubound(chNameArr) > 0 then
        sCSFirstName = chNameArr(lbound(chNameArr))
        sCSLastName = chNameArr(ubound(chNameArr))    
    else
        throwError("530")        
    end if
	
    ' generate track id
    Randomize
    sCSTrackID = session.SessionID & round(rnd * 1000000)
	
	'-----------------------------------------------------------------------------------
	
	dim oMerchantConfig
	set oMerchantConfig = Server.CreateObject( "CyberSourceWS.MerchantConfig" )
	oMerchantConfig.MerchantID = "v6894460"
	oMerchantConfig.SendToProduction = "1"
    oMerchantConfig.KeysDirectory = Server.MapPath("include")
	oMerchantConfig.TargetAPIVersion = "1.19"
	oMerchantConfig.EnableLog = "0"
	oMerchantConfig.LogDirectory = "C:\Program Files\CyberSource Corporation\Simple Order API 2.0.1 for ASP\logs"
	
	'-----------------------------------------------------------------------------------
	
	' set up the request by creating a Hashtable and adding fields to it
	dim oRequest
	set oRequest = Server.CreateObject( "CyberSourceWS.Hashtable" )
	
	oRequest( "ccAuthService_run" ) = "true"	'run authorization Service
	oRequest( "ccCaptureService_run" ) = "true" 'run capture Service
	'oRequest( "ccCreditService_run" ) = "true"	'??? Requesting a Credit
	oRequest( "merchantReferenceCode" ) = sCSTrackID
	oRequest( "billTo_firstName" ) = sCSFirstName
	oRequest( "billTo_lastName" ) = sCSLastName
	oRequest( "billTo_street1" ) = BACHAddr1
	oRequest( "billTo_city" ) = BACity
	oRequest( "billTo_state" ) = BAStateCode
	oRequest( "billTo_postalCode" ) = BAZipCode
	oRequest( "billTo_country" ) = BACountryCode
	oRequest( "billTo_email" ) = X_Email
	If X_PhoneNumber <> "" Then
		oRequest( "billTo_phoneNumber" ) = X_PhoneNumber
	End if
	oRequest( "card_accountNumber" ) = replace(X_ccNumber, " ", "")
	oRequest( "card_expirationMonth" ) = X_ccExpMM
	oRequest( "card_expirationYear" ) = "20" & X_ccExpYY
	if isCVV2 OR X_ccCVV2 <> "" then
	    oRequest( "card_cvNumber" ) = X_ccCVV2
    end if
	If X_Currency = "1" then
		oRequest( "purchaseTotals_currency" ) = "USD"
	Else
		call throwError("531")
	End if
	oRequest( "purchaseTotals_grandTotalAmount" ) = X_Amount
	if sIP <> "" then
		oRequest( "billTo_ipAddress" ) = sIP
	end if
	'-----------------------------------------------------------------------------------
	
	' create Client object
	dim oClient
	set oClient = Server.CreateObject( "CyberSourceWS.Client" )
	
	' send request now
	dim varReply, nStatus, strErrorInfo
	nStatus = oClient.RunTransaction( oMerchantConfig, Nothing, Nothing, oRequest, varReply, strErrorInfo )

	sResDetails =  "Status=" & nStatus & "<br>"
	select case nStatus
		case 0:
			dim decision
			decision = UCase( varReply( "decision" ) )
			if decision = "ACCEPT" then 'Order has been received
				sReturnCode = "000"
			elseif decision = "REVIEW" then 'There was a problem completing order
				sReturnCode = "990"
			elseif decision = "REJECT" then 'Order could not be completed
				sReturnCode = varReply( "reasonCode" )
			else ' Error
				sReturnCode = varReply( "reasonCode" )
			end if
			sResDetails =  sResDetails & varReply.Content( "<br>" )
			sResDetails =  sResDetails & strErrorInfos
		case 1
			'The following error occurred before the request could be sent
			sResDetails =  sResDetails & strErrorInfo
			sReturnCode = "991"
		case 2
			'The following error occurred while sending the request
			sResDetails =  sResDetails & strErrorInfo
			sReturnCode = "992"
		case 3
			'The following error occurred while waiting for or retrieving the reply
			'HandleCriticalError nStatus, strErrorInfo, oRequest, varReply
			sResDetails =  sResDetails & strErrorInfo
			sReturnCode = "993"
		case 4
			'The following error occurred after receiving and during processing of the reply
			'HandleCriticalError nStatus, strErrorInfo, oRequest, varReply
			sResDetails =  sResDetails & strErrorInfo
			sReturnCode = "994"
		case 5,6,7
			sResDetails =  sResDetails & GetFaultContent( varReply )
			sReturnCode = "995"
		Case 8
        	call throwError("521")
	end select
	Set oRequest = Nothing
	Set oClient = Nothing
	Set oMerchantConfig = Nothing

  	Call DebitIProcessResult(sReturnCode, approvalNumber)	
end function


'------------------------------------------------------------------------------
' If an error occurs after the request has been sent to the server, but the
' client can't determine whether the transaction was successful, then the error
' is considered critical.  If a critical error happens, the transaction may be
' complete in the CyberSource system but not complete in your order system.
' Because the transaction may have been successfully processed by CyberSource,
' you should not resend the transaction, but instead send the error information
' and the order information (customer name, order number, etc.) to the
' appropriate personnel at your company.  They should use the information as
' search criteria within the CyberSource Transaction Search Screens to find the
' transaction and determine if it was successfully processed. If it was, you
' should update your order system with the transaction information. Note that
' this is only a recommendation; it may not apply to your business model.
'------------------------------------------------------------------------------
sub HandleCriticalError( nStatus, strErrorInfo, oRequest, varReply )
'------------------------------------------------------------------------------

	' varReply may be one of the following:
	'
	' A Fault object.
	' A raw reply string.
	' Nothing/Null.
	
	dim strReply, strReplyType
	if nStatus = 5 then
		strReply = GetFaultContent( varReply )
		strReplyType = "FAULT DETAILS: "
	elseif IsNull( varReply ) then
		strReply = ""
		strReplyType = "No Reply available."
	else
		strReply = varReply
		strReplyType = "RAW REPLY: "
	end if

	dim strMessageToSend
	strMessageToSend _
		= "Status: " & CStr( nStatus ) & vbCrLf & _
		  "Error Info: " & strErrorInfo & vbCrLf & _
		  "Request: " & vbCrLf & oRequest.Content( vbCrLf ) & _
		  vbCrLf & strReplyType & vbCrLf & strReply
		  
	' send strMessageToSend to the appropriate personnel at your company
	' using any suitable method, e.g. e-mail, multicast log, etc.

	response.write "<p>This is a critical error.  Send the following information to the appropriate personnel at your company:<br><br>"
	response.write strMessageToSend & "</p>"
		
end sub

				
'------------------------------------------------------------------------------
' If you use CyberSource Decision Manager, you may also receive the REVIEW
' value for the decision field. A REVIEW means that Decision Manager has
' flagged the order for review based on how you configured the Decision Manager
' rules.  This sample treats the REVIEW as a REJECT.  This procedure is a
' placeholder for sending the request and reply information to the appropriate
' personnel at your company.  They should then review the order.  Please
' consult the section "Handling Reviews" in the accompanying developer's guide
' for detailed information.
'------------------------------------------------------------------------------
sub HandleReview( oRequest, oReply )
'------------------------------------------------------------------------------

	dim strMessageToSend
	strMessageToSend _
		= "A decision of REVIEW was received on the following order: " & _
		  vbCrLf & "REQUEST: " & vbCrLf & oRequest.Content( vbCrLf ) & _
		  vbCrLf & "REPLY: " & vbCrLf & oRequest.Content( vbCrLf )
		  
	' send strMessageToSend to the appropriate personnel at your company
	' using any suitable method, e.g. e-mail, multicast log, etc.

	response.write "<p>Send the following information to the appropriate personnel at your company:<br><br>"
	response.write strMessageToSend & "</p>"
		
end sub


'------------------------------------------------------------------------------
function GetFaultContent( oFault )
'------------------------------------------------------------------------------

	dim strRequestID
	if (oFault.RequestID = "") then
		strRequestID = "(unavailable)"
	end if
	
	GetFaultContent = "Fault code: " & oFault.FaultCode & "<br>" & _
					  "Fault string: " & oFault.FaultString & "<br>" & _
					  "RequestID: " & strRequestID & "<br>" & _
					  "Fault document: " & "<br>" & oFault.FaultDocument
					  
end function

response.end
%>