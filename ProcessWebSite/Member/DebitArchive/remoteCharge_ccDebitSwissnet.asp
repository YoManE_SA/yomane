<%
'---------------------------------------------------------------------
'	Debit SwissNet
'---------------------------------------------------------------------
'const SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS = 13056
If Request("DEBITRES") = "1" Then
	If Application(Request("OrderID")) = "WAIT" Then
		Application(Request("OrderID")) = Request.Form
	Else
		'handle client not waiting
	End If	
	Response.End()
End if	
	
Function debitSwissNet()
	Dim HttpReq, UrlAddress, UrlAnswerRet, ParamList, sApprovalNumber, strAct
	
	IsoCurreny = Array("ILS", "USD", "EUR", "GBP", "AUD", "CAD", "JPY", "NOK")
	BrandName = Array("--", "--", "VISA", "DINERS", "AMEX", "MASTER")'JCB
   
	If IsoCurreny(Cint(X_Currency)) = "" Then Call throwError("506")
	If Cint(X_TransType) = 1 Then Call throwError("503")
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")

	UrlAnswerRet = Server.URLEncode("http" & IIF(Request.ServerVariables("HTTPS") = "ON", "s", "") & "://" & Request.ServerVariables("SERVER_NAME") & "/member/remoteCharge_ccDebitSwissnet.asp?DEBITRES=1")
	If (X_TransType <> "0") Then throwError("502")
	
	If (X_TypeCredit = "0") Then
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		UrlAddress = "https://www.swissnet.cc/solution/refund/process_refund.php"
		OrdDate = ExecScalar("Select InsertDate From tblCompanyTransPass Where ID=" & X_RefTransID, Now)
		ParamList = "merchantid=" & LCase(sAccountId) & _
					"&currency=" & IsoCurreny(Cint(X_Currency)) & _ 
					"&amount=" & formatNumber(X_Amount, 2, True) & _
					"&TransactionID=" & X_DebitReferenceCode & _
					"&TransactionYear=" & Year(OrdDate) & _
					"&TransactionMonth=" & IIF(Month(OrdDate) < 10, "0" & Month(OrdDate), Month(OrdDate)) & _
					"&TransactionDay=" & IIF(Day(OrdDate) < 10, "0" & Day(OrdDate), Day(OrdDate)) & _
  					"&ResponseURL=" & UrlAnswerRet
	Else
		UrlAddress = "https://www.swissnet.cc/solution/process.php"
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = "MerchantID=" & sAccountId & _
  					"&CardNumber=" & replace(X_ccNumber, " ", "") & _
  					"&Month=" & X_ccExpMM & "&Year=" & X_ccExpYY & _
					"&csc=" & X_ccCVV2 & _
					"&CardType=" & BrandName(ccTypeID) & _
					"&amount=" & (formatNumber(X_Amount, 2, True) * 100) & _
					"&currency=" & IsoCurreny(Cint(X_Currency)) & _ 
					"&MerchantWebsite=http://netpay-intl.com" & _
					"&AcceptedUrl=" & UrlAnswerRet & _
					"&FailedUrl=" & UrlAnswerRet & _
					"&MerchantEmail=SwissNet@netpay.co.il" & _
					"&ServiceEmail=SwissNet@netpay.co.il" & _
					"&CustomerEmail=" & Server.URLEncode(X_Email) & _
					"&CardHolder=" & Server.URLEncode(X_ccHolderName) & _
   					"&HolderStreet=" & Server.URLEncode(BACHAddr1) & _
					"&postcode=" & BAZipCode & _
					"&OrderID=" & X_DebitReferenceCode
	End If
    Application(CStr(X_DebitReferenceCode)) = "WAIT"
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
	    HttpError = FormatHttpRequestError(HttpReq)
    On Error GoTo 0
    'Response.Write(ParamList & "<br>" & sResDetails)
    If Len(sResDetails) = 0 Then Call throwError("521") 'communication error

	For i = 1 To 60
		NetpayCryptoObj.Delay 1000
		If Application(CStr(X_DebitReferenceCode)) <> "" Then Exit For
	Next	

	sResDetails = Application(CStr(X_DebitReferenceCode))
	Application.Contents.Remove CStr(X_DebitReferenceCode)
	If sResDetails <> "WAIT" Then
	    If GetURLValue(sResDetails, "PSPResponse") = "Successful" Then
			sApprovalNumber = GetURLValue(sResDetails, "AuthCode")
			X_DebitReferenceCode = GetURLValue(sResDetails, "TransactionID")
			sReturnCode = "000"
	    Else
		    'Response.Write(ParamList & "<br>" & sResDetails & "<br>" & sReturnCode) : Response.End
			X_DebitReferenceCode = GetURLValue(sResDetails, "TransactionID")
			sReturnCode = "002"
	    End if
	Else
		Call throwError("521") 'communication error
	End If
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End function
%>