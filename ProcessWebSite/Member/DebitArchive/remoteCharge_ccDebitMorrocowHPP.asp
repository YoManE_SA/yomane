<%
'---------------------------------------------------------------------
'	Debit AllCharge
'---------------------------------------------------------------------

Function remoteCharge_CC_DeltaPay_BackDetect()
	Dim outString, iRs, fullName, emailAddress, phoneNumber
	sOuterIP = IIF(session("Identity") = "Local", "http://80.179.39.10:8080/Default.aspx", Session("ProcessURL") & "remoteCharge_ccDebitMorrocowHPPNotify.asp")
	sql = "Select tp.*, cd.*, cc.*, ba.*, dt.dt_IsTestTerminal, dt.accountId, dt.accountSubId , dbo.GetDecrypted256(dt.accountPassword256) As accountPassword, " & _
         " dbo.GetDecrypted256(cd.RoutingNumber256) As routingNumber, dbo.GetDecrypted256(cd.AccountNumber256) As accountNumber, cd.email As CheckEmail, cd.phoneNumber As CheckPhone, " & _
         " dbo.GetDecrypted256(cc.CCard_number256) As CardNumber, cc.email As CardEmail, cc.phoneNumber As CardPhone, " & _
         " cl.CountryISOCode AS CountryCode, sl.StateISOCode AS StateCode " & _
		 " From tblCompanyTransPending AS tp " & _
		 " Left Join tblDebitTerminals AS dt ON(dt.TerminalNumber = tp.TerminalNumber)" & _
         " Left Join tblCheckDetails AS cd ON (tp.CheckDetailsID = cd.id)" & _
         " Left Join tblCreditCard AS cc ON (tp.CreditCardID = cc.id)" & _
         " Left Join tblBillingAddress AS ba ON (cd.BillingAddressId = ba.id Or cc.BillingAddressId = ba.id)" & _
         " Left Join [List].[CountryList] AS cl ON (cl.CountryID = ba.countryId)" & _
         " Left Join [List].[StateList] AS sl ON (sl.statesID = ba.stateId)" & _
		 " Where tp.companyTransPending_id=" & xTransID
    'Response.Write(sql)
	Set iRs = oleDbData.Execute(sql)
    If Not iRs.EOF Then
        If Not IsNull(iRs("CreditCardID")) Then 
            fullName = Trim(Replace(iRs("Member"), "  ", " ")
        	emailAddress = iRs("CardEmail")
	    	phoneNumber = iRs("CardPhone")
    	ElseIf Not IsNull(iRs("CheckDetailsID")) Then 
            fullName = Trim(Replace(iRs("AccountName"), "  ", " ")
	      	emailAddress = iRs("CheckEmail")
		    phoneNumber = iRs("CheckPhone")
        End If
		outString = _
			"<html><head></head><body>" & vbCrLf & _
            "<form id=""dataForm"" action=""https://fastecom.com/gateways/FAST/processFAST.php"" method=""post"">" & vbCrLf & _
            "<input type=""hidden"" name=""storeId"" value=""" & AccountId & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""langue"" value=""" & "EN" & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""offerURL"" value=""" & sOuterIP & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""updateURL"" value=""" & sOuterIP & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""bookURL"" value=""" & sOuterIP & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""name"" value=""" & fullName & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""address"" value=""" & iRs("address1") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""city"" value=""" & iRs("city") & """ />" & vbCrLf
    		If iRs("stateId") <> 0 Then outString = outString & "<input type=""hidden"" name=""state"" value=""" & iRs("StateCode") & """ />" & vbCrLf
            "<input type=""hidden"" name=""country"" value=""" & iRs("CountryCode") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""postCode"" value=""" & iRs("zipCode") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""tel"" value=""" & phoneNumber & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""email"" value=""" & emailAddress & """ />" & vbCrLf & _
            'checksum
            "<input type=""hidden"" name=""cartId"" value=""" & 1 & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""totalAmountTx"" value=""" & iRs("trans_amount") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""symbolCur"" value=""" & GetCurISOName(iRs("trans_currency")) & """ />" & vbCrLf
	        "<input type=""hidden"" name=""btnSubmit"" value=""submit"" />" & vbCrLf & _
            "</form>" & vbCrLf & _
            "<script type=""text/javascript"" defer=""defer"">document.getElementById('dataForm').submit();</" & "script>" & vbCrLf & _
			"</body></html>"
    End If
	iRs.Close()
    ExecSql("Update tblLogChargeAttempts Set Lca_RequestString='" & Replace(Server.HTMLEncode(outString), "'", "''") & "' Where LogChargeAttempts_id=" & xLogChargeAttemptID)
	Response.Write(outString)
End Function

Function debit_DeltaPayHpp()
	Dim HttpReq
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		Call debit_DeltaPay()
        Exit Function
	Else
		X_DebitReferenceCode = GetTransRefCode()
	    X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
        X_3dRedirectMethod = "SameWindow"
	    'Response.Write(X_3dRedirect)
		sReturnCode = "553"
	End if
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>
