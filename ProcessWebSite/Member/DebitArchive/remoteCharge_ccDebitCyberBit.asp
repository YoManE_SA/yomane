<%
'---------------------------------------------------------------------
'	Debit WireCard - 20090907 Tamir
'---------------------------------------------------------------------
Function CyberBitGenerateRequest()
	Dim sSQL : sSQL = "SELECT * FROM tblDebitTerminals WHERE TerminalNumber='" & sTerminalNumber & "' AND DebitCompany=35"
	Dim rsData : Set rsData = oledbData.Execute(sSQL)
	If rsData.EOF Then ThrowError "503"
	Dim sRequest : sRequest = "Version=2&Hardidoff=1"
	sRequest=sRequest & "&Secret=" & rsData("AccountID")
	sRequest=sRequest & "&MerchantId=" & rsData("TerminalNumber")
	If X_TransType=2 Then 'capture
		If TestNumVar(sApprovalOnlyID, 1, 0, 0) < 1 Then ThrowError 536
		If ExecScalar("SELECT ApprovalNumber FROM tblCompanyTransApproval WHERE ID=" & sApprovalOnlyID, 0)=0 Then ThrowError 536
		sRequest=sRequest & "&TransType=2"
		sRequest=sRequest & "&InternalOrderID=" & sApprovalOnlyID
	ElseIf X_TypeCredit=0 Then 'refund
		If TestNumVar(X_RefTransID, 1, 0, 0) < 1 Then ThrowError 535
		Dim nApproval : nApproval=ExecScalar("SELECT ApprovalNumber FROM tblCompanyTransPass WHERE ID=" & X_RefTransID, 0)
		If nApproval=0 Then ThrowError 532
		sRequest=sRequest & "&TransType=4"
		sRequest=sRequest & "&InternalOrderID=" & nApproval
	Else
		sRequest=sRequest & "&TransType=" & IIf(X_TransType = 1, 1, 3)
		sRequest=sRequest & "&InternalOrderID=" & LogSavingChargeDataID
		sRequest=sRequest & "&CurrencyCode=" & GetCurISOCode(X_Currency)
		sRequest=sRequest & "&CurrencyTLA=" & GetCurISOName(X_Currency)
		sRequest=sRequest & "&AmountCleared=" & CLng(X_Amount*100)
		sRequest=sRequest & "&CreditCardNumber=" & Replace(X_ccNumber, " ", "")
		sRequest=sRequest & "&ExpireMonth=" & Right("0" & X_ccExpMM, 2)
		sRequest=sRequest & "&ExpireYear=" & Right("0" & (X_ccExpYY Mod 100), 2)
		sRequest=sRequest & "&CreditCardCVC=" & X_ccCVV2
		sRequest=sRequest & "&OwnerEmail=" & X_Email
		sRequest=sRequest & "&OwnerAddress=" & Left(Trim(BACHAddr1 & " " & BACHAddr2), 50)
		sRequest=sRequest & "&OwnerCity=" & BACity
		sRequest=sRequest & "&OwnerCountry=" & BACountry
		sRequest=sRequest & "&OwnerState=" & IIf(Trim(BAState)="", "OO", BAState)
		sRequest=sRequest & "&OwnerFirstName=" & IIf(InStr(X_ccHolderName, " ")>0, Trim(Left(X_ccHolderName, InStrRev(X_ccHolderName, " "))), X_ccHolderName)
		sRequest=sRequest & "&OwnerLastName=" & IIf(InStr(X_ccHolderName, " ")>0, Trim(Right(X_ccHolderName, Len(X_ccHolderName)-InStrRev(X_ccHolderName, " "))), X_ccHolderName)
		sRequest=sRequest & "&OwnerZip=" & BAZipCode
		sRequest=sRequest & "&OwnerPhone=" & X_PhoneNumber
	End If
	sRequest=sRequest & "&Callback=http://process.netpay-intl.com/empty.htm"
	CyberBitGenerateRequest=sRequest
End Function

Function CyberBitSendRequest(sRequest)
	Dim sURL : sURL = "https://" & IIf(sTerminalNumber = "net1", "test.", "") & "cyberbit.dk/author.php"
	Dim sxhRequest : Set sxhRequest = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
	Dim bDebug : bDebug = IIf(request("Debug")="1", True, False)
	if bDebug then response.Write sURL & "<hr>"
	if bDebug then response.Write sRequest & "<hr>"
	sxhRequest.Open "POST", sURL, False
	sxhRequest.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	sxhRequest.Send sRequest
	if bDebug then response.write sxhRequest.status & "<hr>"
	Dim sResponse : sResponse = ""
	sResponse = sxhRequest.ResponseText
	if bDebug then response.Write Server.HTMLEncode(sResponse) & "<hr>"
	If sResponse = "" Then ThrowError 521
	CyberBitSendRequest = sResponse
End Function

Function debitCyberBit()
	Dim sResponseXML : sResponseXML = CyberBitSendRequest(CyberBitGenerateRequest())
	Dim xdResponse : Set xdResponse = Server.CreateObject("Microsoft.XMLDOM")
	Dim sCode : sCode = ""
	Dim sAnswer : sAnswer = ""
	Dim sApprovalNumber : sApprovalNumber = ""
	On Error Resume Next
	xdResponse.LoadXML(sResponseXML)
	sCode = xdResponse.DocumentElement.SelectSingleNode("//Response/StatusCode").Text
	sAnswer = sResponseXML
	sApprovalNumber = xdResponse.DocumentElement.SelectSingleNode("//Response/OrderID").Text
	On Error GoTo 0
	If sCode = "" Or sAnswer = "" Or sApprovalNumber = "" Then ThrowError 520
	Call DebitIProcessResult(sCode, sApprovalNumber)
End Function
%>