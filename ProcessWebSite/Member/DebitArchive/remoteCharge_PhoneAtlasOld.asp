﻿<%
Function debit_phone_Atlas()
	Dim HttpReq, UrlAddress, ParamList, sApprovalNumber, strAct, sDebitReturnAnswer
	'IsoCurreny = Array("ILS", "USD", "EUR", "GBP", "AUD", "CAD", "JPY", "NOK", "PLN")
	
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
	UrlAddress = "http://88.198.101.46:8080/smsgateway/incoming/mt/10547" 'https://gw.girogate.de
	If Cint(X_TransType) = 1 Then Call throwError("503")

	If trim(X_TransType) = "4" Then
		ParamList = "returnmode=urlencode" & vbCrLf & _
		"&txtype=GETTXSTATUS" & vbCrLf & _
		"&login=" & Server.URLEncode(sAccountSubId) & vbCrLf & _
		"&password=" & Server.URLEncode(sAccountPassword) & vbCrLf & _
		"&contractid=" & Server.URLEncode(sAccountID) & vbCrLf & _
		"&txid=" & Server.URLEncode(X_ConfirmNumber)
	ElseIf (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	Else
		Dim nTagArray, sChannnelName
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = "RequestType=PinRequest" & vbCrLf & _
		"&Username=" & Server.URLEncode(sAccountSubId) & vbCrLf & _
		"&Password=" & Server.URLEncode(sAccountPassword) & vbCrLf & _
		"&RequestID=" & Server.URLEncode(X_DebitReferenceCode) & vbCrLf & _
		"&ServiceID=" & Server.URLEncode(sAccountId) & vbCrLf & _
		"&ServiceType=web" & vbCrLf & _
		"&PaymentType=subscr" & vbCrLf & _
		"&MaskID=TAN" & vbCrLf & _
		"&Destination=" & Server.URLEncode(X_PhoneNumber) & vbCrLf & _
		"&OperatorID=" & Server.URLEncode(X_OperatorID) & vbCrLf & _
		"&BruttoAmount=" & Server.URLEncode(CInt(X_Amount * 100)) & vbCrLf & _
		"&VAT=0" & vbCrLf & _
		"&Description=" & Server.URLEncode(sCompanyDescriptor) & vbCrLf & _
		"&Currency=" & GetCurISOName(Cint(X_Currency)) & vbCrLf & _
		"&Category=" & Server.URLEncode(X_CategoryID) & vbCrLf
	End If
	ParamList = Replace(ParamList, vbCrLf, "")
	'On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
		HttpReq.Send(ParamList)
		TxtRet = HttpReq.responseText
		SaveLogChargeAttemptRequestResponse ParamList, TxtRet
		HttpError = FormatHttpRequestError(HttpReq)
	'On Error GoTo 0
	Response.Write(session("ProcessURL") & "<br>" & ParamList & vbCrLf & "<br>" & TxtRet & vbCrLf & "<br>") : Response.End()
	If IsNull(TxtRet) Or Len(TxtRet) = 0 Then Call throwError("521")
	sReturnCode = GetURLValue(TxtRet, "StatusCode")
	sApprovalNumber = GetURLValue(TxtRet, "TransactionID")
	sValidityPeriod = GetURLValue(TxtRet, "ValidityPeriod")
	
	If sReturnCode = "" Then Call throwError("520") 'internal error
	'Response.Write(TxtRet & "</br>END") : Response.End
	If sReturnCode = "0" Then
		sReturnCode = "000"
	End If	
	Set HttpReq = Nothing
	DebitIProcessResult sReturnCode, sApprovalNumber
End Function
%>