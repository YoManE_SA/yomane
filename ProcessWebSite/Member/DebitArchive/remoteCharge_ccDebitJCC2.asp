<%
'---------------------------------------------------------------------
'	Debit JCC2 ()
'---------------------------------------------------------------------
Function GetJCC2FieldValue(xtrData, xVal)
    Dim sIndex, eIndex
    sIndex = InStr(1, xtrData, xVal & ":")
    if sIndex < 1 Then Exit Function
    sIndex = sIndex + Len(xVal) + 1
    eIndex = InStr(sIndex, xtrData, "/")
    if eIndex < sIndex Then eIndex = Len(xtrData)
    GetJCC2FieldValue = Mid(xtrData, sIndex, eIndex - sIndex)
End Function

function debitJCC2()
  Dim pMP, mUnique, xRef, TempRs, xAmnt', IsoCurreny
  'IsoCurreny = Array("376", "840", "978", "826")
  Set pMP = Server.CreateObject("Compunet.CJCC2")
  lnCount = TestNumVar(ExecScalar("Select dc_nextTransID From tblDebitCompany Where debitCompany_id=32", 0), 0, -1, 0) + 1
  ExecSql("Update tblDebitCompany Set dc_nextTransID = dc_nextTransID + 1 Where debitCompany_id=32")
  sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
  If CInt(X_TypeCredit) = 0 Or Cint(X_TransType) = 2 Then
	  If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans Not Found
	  'orgStan = ExecScalar("Select Answer From tblCompanyTransPass Where ID=" & X_RefTransID, "")
	  'lnCount = GetMDRPFieldValue(orgStan, "011")
  Else 
	  X_DebitReferenceCode = GetTransRefCode()
  End If

  pMP.BeginMessage IIf(Cint(X_TypeCredit) = 0, "0440", IIf(Cint(X_TransType) = 1, "0100", IIf(Cint(X_TransType) = 2, "0141", "0140")))
  pMP.AddField 002, Replace(X_ccNumber, " ", "")
  pMP.AddField 003, "000000"
  pMP.AddField 004, pMP.PadLeft(formatNumber(X_Amount, 2) * 100, 12, "0")
  pMP.AddField 007, pMP.Format(Now, "MMDDhhmmss")
  pMP.AddField 011, Right(Cstr(lnCount), 6)
  pMP.AddField 012, pMP.Format(Now, "hhmmss")
  pMP.AddField 013, pMP.Format(Now, "MMDD")
  pMP.AddField 014, X_ccExpYY & X_ccExpMM
  pMP.AddField 018, sMcc
  pMP.AddField 019, "196"
  pMP.AddField 022, IIf(X_Track2 <> "", "02", IIf(PaymentMethod = PMD_CC_MASTERCARD, "81", "01")) & "0"
  pMP.AddField 025, IIf(PaymentMethod = PMD_CC_MASTERCARD, "00", "59")
  pMP.AddField 032, "39"
  If X_Track2 <> "" Then 
	eIndex = InStr(1, X_Track2, "?")
	If eIndex < 1 Then eIndex = Len(eIndex)
	pMP.AddField "035", Replace(X_ccNumber, " ", "") & "=" & Mid(X_Track2, 1, eIndex - 1)
  End If
  pMP.AddField "037", pMP.PadLeft(X_DebitReferenceCode, 12, "0")
  If Cint(X_TypeCredit) = 0 Or Cint(X_TransType) = 2 Then
	  pMP.AddField 038, pMP.PadLeft(X_ConfirmNumber, 6, "0")
	  pMP.AddField 039, pMP.PadLeft("17", 2, "0")
  End if
  pMP.AddField 041, CStr(sAccountId)
  pMP.AddField 042, pMP.PadRight(Left(sAccountSubId, 15), 15, " ")
  pMP.AddField 043, pMP.PadRight(Left(sCompanyDescriptor, 25), 25, " ") & "LEMESOS      CY"
  If Cint(X_TypeCredit) <> 0 Then pMP.AddField 048, "    0376U0592" & pMP.PadRight(X_ccCVV2, 3, "0") & IIf(PaymentMethod = PMD_CC_MASTERCARD, "0542910", "") '"11" & pMP.PadRight(X_ccCVV2, 4, "0") & " "
  pMP.AddField 049, GetCurISOCode(X_Currency)
  pMP.AddField 060, "00"
  If Cint(X_TypeCredit) <> 0 Then 
	pMP.AddField 061, "20251000001001960000000000" '"20351000060004420000000000"
	If PaymentMethod <> PMD_CC_MASTERCARD Then pMP.AddField 126, ChrW(&h4000) & String(3, Chr(0)) & pMP.ToEBDIC("11" & pMP.PadLeft(X_ccCVV2, 4, " "))
  Else
	pMP.AddField 090, "0100" & GetJCC2FieldValue(RefOAnswer, "011") & GetJCC2FieldValue(RefOAnswer, "007") & "00000000039" & "00000000000"
	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX above line will not work - RefOAnswer (answer) no longer exist in trans tables
  End if	
  'Response.Write(pDataSend) : Response.End()
  pMP.Server = "172.16.28.241:3038" '"192.168.0.105:3038"
  pMP.Timeout = 10 * 1000
  Err.Clear
  On Error Resume Next
  pMP.Connect()
  If Err.number <> 0 Then
	 HttpError = "Connect: " & Err.number & ": " & Err.Description
  Else 
	 pRetData = pMP.Send(False)
	 If Err.number <> 0 Then HttpError = "Send: " & Err.number & ": " & Err.Description
	 pRetData = pMP.Receive(False)
	 If Err.number <> 0 Then HttpError = "Recv: " & Err.number & ": " & Err.Description
  End If
  pMP.Disconnect()
  xRetStatus = pMP.FieldValue(039, True)
  sApprovalNumber = pMP.FieldValue(038, True)
  pRetData = pMP.FormatFields(False, True)
  On Error Goto 0
  'Response.Write(pDataSend & "<br>" & pRetData & "<br>" & Len(pRetData) & " " & xRetStatus & " " & sApprovalNumber) : Response.End
  'If CInt(X_TypeCredit) = 0 Then FileAppendData "Invik.txt", Now & vbcrlf & pDataSend & vbcrlf & pRetData & vbcrlf & vbcrlf & vbcrlf
  Set pMP = Nothing
 If xRetStatus = "00" Then 
	xRetStatus = "000"
 ElseIf CInt(X_TypeCredit) = 0 And (xRetStatus = "12" Or xRetStatus = "") Then
	'(InStr(1, LCase(Request.ServerVariables("REMOTE_USER")), "lavi") > 0)
	xRetStatus = "000"
	sApprovalNumber = "ERR12"
 End If
 If xRetStatus = "" Then Call throwError("521")
 DebitIProcessResult xRetStatus, sApprovalNumber
 end function
%>