<%
'---------------------------------------------------------------------
'	Debit Ecard (polish)
'---------------------------------------------------------------------
const SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS = 13056
function debitEcard()
   Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer
   if Cint(X_Currency) <> 1 Then Call throwError("506")
   xNames = Split(X_ccHolderName, " ")
   if Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
   if Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
	 sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	 if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
      Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP

     'call HttpReq.setOption(2, HttpReq.getOption(2) - SXH_SERVER_CERT_IGNORE_CERT_DATE_INVALID)
      call HttpReq.setOption(3, "LOCAL_MACHINE\Root\" & sAccountId)'"\10000006" ' "IWAM_NETPAY\Root"
      'HttpReq.setOption(3) = "LOCAL_MACHINE\Root\" & sAccountId '"\10000006" ' "IWAM_NETPAY\Root"
	  HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
      UrlAddress = "https://payauth.ecard.pl:444/ws/services/Payment" '"http://webservices.ecard.com/" 'https://pay.ecard.pl/payment/PS '"http://localhost:1609/Service1.asmx"
      if (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
  	      if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	  	  if X_TypeCredit="0" Then strAct = "refund" Else strAct = "deposit"
		  ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & VbCrlf & _
	  		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & VbCrlf & _
			" <soap:Body>" & VbCrlf & _
			"  <" & strAct & ">" & VbCrlf & _
	    	"   <merchantid>" & sAccountId & "</merchantid>" & VbCrlf & _ 
		    "   <password>" & sAccountPassword & "</password>" & VbCrlf & _
		    "   <ordernumber>" & X_DebitReferenceCode & "</ordernumber>" & VbCrlf & _
		    "   <amount>" & (formatNumber(X_Amount, 2) * 100) & "</amount>" & VbCrlf & _
			"  </" & strAct & ">" & VbCrlf & _
		    " </soap:Body>" & VbCrlf & _
		    "</soap:Envelope>"
	  Else
		  If Cint(X_TransType) = 1 Then mSDep = 0 Else mSDep = 1
	      X_DebitReferenceCode = GetTransRefCode()
		  ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & VbCrlf & _
	  		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & VbCrlf & _
			" <soap:Body>" & VbCrlf & _
			"  <authorize>" & VbCrlf & _
	    	"   <merchantid>" & sAccountId & "</merchantid>" & VbCrlf & _ 
		    "   <password>" & sAccountPassword & "</password>" & VbCrlf & _
		    "   <ordernumber>" & X_DebitReferenceCode & "</ordernumber>" & VbCrlf & _
		    "   <amount>" & (formatNumber(X_Amount, 2) * 100) & "</amount>" & VbCrlf & _
		    "   <currency>840</currency>" & VbCrlf & _
		    "   <country>616</country>" & VbCrlf & _
		    "   <pan>" & replace(X_ccNumber, " ", "") & "</pan>" & VbCrlf & _
		    "   <cvc>" & X_ccCVV2 & "</cvc>" & VbCrlf & _
		    "   <month>" & X_ccExpMM & "</month>" & VbCrlf & _
		    "   <year>20" & X_ccExpYY & "</year>" & VbCrlf & _
		    "   <orderdescription>" & Left(sCompanyDescriptor, 15) & sIP & "</orderdescription>" & VbCrlf & _
		    "   <name>" & fName & "</name>" & VbCrlf  & _
		    "   <surname>" & sName & "</surname>" & VbCrlf & _
		    "   <autodeposit>" & mSDep & "</autodeposit>" & VbCrlf & _
			"  </authorize>" & VbCrlf & _
		    " </soap:Body>" & VbCrlf & _
		    "</soap:Envelope>"
			'Currency 985
		End if
    On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		HttpReq.setRequestHeader "SOAPAction", "http://webservices.ecard.com/"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
	    HttpError = FormatHttpRequestError(HttpReq)
    If Len(sResDetails) = 0 Then Call throwError("521")
	set xmlRet = HttpReq.responseXml
	sDebitReturnAnswer = Left(sResDetails, 1000)
	
    sReturnCode = Trim(xmlRet.selectSingleNode("//validationcode").text)
	pmnStatus = Trim(xmlRet.selectSingleNode("//pmnStatus").text)
    if (Len(pmnStatus) < 2) And (sReturnCode = "000") then
		FileAppendData "eCard.txt", Now & vbcrlf & ParamList & vbcrlf & HttpReq.responseText & vbcrlf & vbcrlf & vbcrlf
		throwError("520")
    End if
	sApprovalNumber = xmlRet.selectSingleNode("//approvalcode").text
	sError = xmlRet.selectSingleNode("//error").text
	If trim(sReturnCode) = "001" then sReturnCode = "002" '001 is taken by netpay
	If (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		If (pmnStatus <> "") And (sReturnCode = "") Then sReturnCode = "000"
		If xmlRet.selectSingleNode("//prc").text <> "0" or xmlRet.selectSingleNode("//rc").text <> "0" Then _
			sReturnCode = xmlRet.selectSingleNode("//rc").text & "," & xmlRet.selectSingleNode("//prc").text
	End If

	'If Cint(X_TransType) = 2 Then 
    '	Response.Write(ParamList & vbcrlf & HttpReq.responseText & vbcrlf & "<br>Code:" & sReturnCode & ",sApprovalNumber:" & sApprovalNumber & ",pmnStatus:" & pmnStatus & ",Error :" & sError & "<Br>")
	'   Response.End
	'End if
    Set xmlRet = Nothing
    Set HttpReq = Nothing
    On Error GoTo 0 
    
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End function
%>