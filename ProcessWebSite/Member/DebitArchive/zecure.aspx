<%@Import Namespace="System.Net" %>
<%@ Page Language="VB" %>
<%@Import Namespace="System.IO" %>
<%@Import Namespace="System.Xml" %>
<script runat="server">
	Sub SendServerRequest(ByVal o As Object, ByVal e As EventArgs)
		Dim nID As Integer
		Math.DivRem((Date.Now.Millisecond + 1) * (Date.Now.Second + 1) * (Date.Now.Minute + 1) * (Date.Now.Hour + 1), 1000000, nID)
		Dim sRequestURL As String = "http://xml.zecure.com:8099/acquirer_score?USERNAME=netpay-intl-xml&PASSWORD=7Dt3XsI&ACCOUNT_ID=Detelco Co LTD&IP=" & Request.ServerVariables("REMOTE_ADDR") & "&TRANSACTION_ID=" & nID
		litURL.Text = sRequestURL
		Dim hwrSMS As HttpWebRequest = WebRequest.Create(sRequestURL)
		hwrSMS.Method = "GET"
		Dim srResult As New StreamReader(hwrSMS.GetResponse.GetResponseStream)
		Dim xmldOutput As New XmlDocument
		xmldOutput.LoadXml(srResult.ReadToEnd)
		litXML.Text = Server.HtmlEncode(xmldOutput.OuterXml)
		srResult.Close()
		srResult.Dispose()
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Zecure Test Page</title>
	</head>
	<body>
		<form runat="server">
			<asp:Button ID="btnSend" Text="Send Server Request" OnClick="SendServerRequest" runat="server" />
			<hr />
			<asp:Literal ID="litURL" runat="server" />
			<br />
			<asp:Literal ID="litXML" runat="server" />
		</form>
		<script language="javascript" type="text/javascript" src="https://safe.zecure.com/observer.js?uid=detelco-co-ltd-event&auto=off"></script>
		<script language="javascript" type="text/javascript">
			try
			{
			  zSafeCustom['msg']='advanced';
			  zSafeObserve();
			}
			catch(error){}
		</script>
		<noscript>
			<img src="https://safe.zecure.com/collect/nojs?uid=detelco-co-ltd-event&et=hit&msg=advanced" border="0" alt="" width="1" height="1" />
		</noscript>
	</body>
</html>