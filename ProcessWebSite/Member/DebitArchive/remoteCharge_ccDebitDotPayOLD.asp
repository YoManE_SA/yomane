<%
'---------------------------------------------------------------------
'	Debit DotPay
'---------------------------------------------------------------------
Function GetStringPart(nStr, startStr, EndStr)
	Dim nStart, nEnd
    nStart = InStr(1, nStr, startStr)
    If nStart < 1 Then Exit Function
    nStart = nStart + Len(startStr)
    nEnd = InStr(nStart, nStr, EndStr)
    GetStringPart = Mid(nStr, nStart, nEnd - nStart)
End Function 

function debitDotPay()
   Dim HttpReq, UrlAddress, ParamList, fName, sName, EncodedSignature, sApprovalNumber, strAct, IsoCurreny
   IsoCurreny = Array("ILS", "USD", "EUR", "GBP", "AUD", "CAD", "JPY", "NOK")
   If IsoCurreny(Cint(X_Currency)) = "0" Then Call throwError("506")
   xNames = Split(X_ccHolderName, " ")
   if Ubound(xNames) > -1 Then fName = xNames(0)
   if Ubound(xNames) > 0 Then sName = xNames(1)
   sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
   if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
   UrlAnswerRet = Server.URLEncode("http" & IIF(Request.ServerVariables("HTTPS") = "ON", "s", "") & "://" & Request.ServerVariables("SERVER_NAME") & "/member/remoteCharge_ccDebitDotPay.asp?DEBITRES=1")
   Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
   UrlAddress = "https://ssl.dotpay.eu/"
     if (X_TypeCredit = "0") Then
 	     if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans Not Found
            On Error Resume Next
		        HttpReq.open "POST", UrlAddress, false
		    	HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
				HttpReq.send ParamList
				sResDetails = HttpReq.responseText
            On Error GoTo 0
            
	        if sResDetails <> "" Then 
            	Set xmlRet = HttpReq.responseXml
                sReturnCode = Trim(xmlRet.selectSingleNode("//RefundAmountResult").text)
                If sReturnCode = "true" Then sReturnCode = "000" Else sReturnCode = "2"
                sError = Trim(xmlRet.selectSingleNode("//sError").text)
                Set xmlRet = Nothing
	        End If
       	    'Response.Write(ParamList & vbcrlf & HttpReq.responseText & vbcrlf & "<br>" & sReturnCode & "<Br>" & sApprovalNumber)
            'Response.End
			'FileAppendData "JCC.txt", Now & vbcrlf & ParamList & vbcrlf & sResDetails & vbcrlf & vbcrlf & vbcrlf
       Else
		    'phase II
            X_DebitReferenceCode = GetTransRefCode()       
            ParamList = _
				"id=" & sAccountId & _
				"&amount=" & formatNumber(X_Amount, 2, True) & _
				"&currency=" & IsoCurreny(Cint(X_Currency)) & _
				"&description=" & sCompanyDescriptor & _
				"&lang=en" & _
				"&channel=0" & _
				"&ch_lock=1" & _
				"&URL=" & UrlAnswerRet & _
				"&type=1" & _
				"&buttontext=BACKTOMYSITE" & _
				"&URLC=" & UrlAnswerRet & _
				"&firstname=" & fName & _
				"&lastname=" & sName & _
				"&surname=" & fName & _
				"&forename=" & sName & _
				"&email=" & X_Email & _
				"&street=" & BACHAddr1 & _
				"&addr2=" & BACHAddr2 & _
				"&state=" & BAState & _
				"&city=" & BACity & _
				"&country=" & BACountryCode & _
				"&postcode=" & BAZipCode
        On Error Resume Next
			HttpReq.open "POST", UrlAddress, false
			HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
			HttpReq.setRequestHeader "Accept-Language", "en-US"
			HttpReq.send ParamList
			sResDetails = HttpReq.responseText
		    HttpError = FormatHttpRequestError(HttpReq)
        On Error GoTo 0
	    If Len(sResDetails) = 0 Then Call throwError("521") 'communication error
		'Response.Write(UrlAddress & "/?" & ParamList & "<br>" & sResDetails & "<br><br><br>")
	    'phase II
	    UrlAddress = "https://ssl.dotpay.eu/get.php"
        ParamList = _
			"kanal=" & 240 & _
			"&k_typ=" & UCase(ccTypeEngName) & _
			"&k_num=" & Replace(X_ccNumber, " ", "") & _
			"&k_mm=" & X_ccExpMM & _
			"&k_yy=" & X_ccExpYY & _
			"&k_cv=" & X_ccCVV2 & _
			"&firstname=" & fName & _
			"&lastname=" & sName & _
			"&surname=" & fName & _
			"&forename=" & sName & _
			"&email=" & X_Email & _
			"&street=" & BACHAddr1 & _
			"&addr2=" & BACHAddr2 & _
			"&state=" & BAState & _
			"&city=" & BACity & _
			"&country=" & BACountryCode & _
			"&postcode=" & BAZipCode & _
			"&send=" & Server.URLEncode("Send your order")
        On Error Resume Next
			HttpReq.open "POST", UrlAddress, false
			HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
			HttpReq.setRequestHeader "Accept-Language", "en-US"
			HttpReq.send ParamList
			sResDetails = HttpReq.responseText
		    HttpError = FormatHttpRequestError(HttpReq)
        On Error GoTo 0
	    If Len(sResDetails) = 0 Then Call throwError("521") 'communication error
        sReturnCode = Trim(Replace(GetStringPart(sResDetails, "Your payment is:", "</div>"), "<br>", ""))
		'Response.Write(UrlAddress & "?" & ParamList & "<br>" & sReturnCode & "<br>" & sResDetails & "<br><br><br>") : Response.End
        If InStr(1, sReturnCode, "Authorized") > 0 Then 
		    sApprovalNumber = Trim(GetStringPart(sResDetails, "transaction number <b>", "</b>"))
	        sReturnCode = "000"
        Else
		    sApprovalNumber = Trim(GetStringPart(sResDetails, "The transaction number is:  <B>", "</B>"))
	        sResDetails = Trim(Replace(GetStringPart(sResDetails, "Problem:", "</div>"), "<br>", ""))
	        sReturnCode = "002"
        End If
	End If    
    Set HttpReq = Nothing
    'Response.Write(ParamList) : Response.End 
 	Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
end function
%>