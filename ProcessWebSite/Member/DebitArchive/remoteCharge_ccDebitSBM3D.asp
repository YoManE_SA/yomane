<%
'---------------------------------------------------------------------
'	debit Payment Systems
'---------------------------------------------------------------------
function debitSBM3D()
	If Request("D3Ret") <> "" Then 
		SBM3DSeconedPart()
		Exit Function
	End if
	'Check currency - only US dollar 
	if X_Currency <> 1 Then Call throwError("506")
	
	'Set transaction type 
    select case cInt(x_transType)
	    case 0
			If cInt(X_TypeCredit)=1 Then
				ActionType = 1
			ElseIf cInt(X_TypeCredit)=0 Then
				ActionType = 2
			Else
				Call throwError("506")
			End if
		case 1
			'DISABLE FOR NOW
			'If cInt(X_TypeCredit)=1 Then
			'	ActionType = 4
			'Else
				Call throwError("506")
			'End if
	    case else
			Call throwError("506")
    end select
	
	If sTerminalNumber="100001" Then
		'TEST
		UrlAddress = "https://tstegate.sbmonline.com:443/egate/servlet/MPIVerifyEnrollmentXMLServlet"
	Else
		'PRODUCTION
		'UrlAddress = ""
	End if
	
	'building url string to send
	Dim XmlStr
	XmlStr = ""
	XmlStr = XmlStr & "<id>" & sAccountId3D & "</id>"
	XmlStr = XmlStr & "<password>" & sAccountPassword3D & "</password>"
	XmlStr = XmlStr & "<card>" & replace(X_ccNumber, " ", "") & "</card>"
	XmlStr = XmlStr & "<cvv2>" & X_ccCVV2 & "</cvv2>"
	XmlStr = XmlStr & "<expYear>20" & X_ccExpYY & "</expYear>"
	XmlStr = XmlStr & "<expMonth>" & X_ccExpMM & "</expMonth>"
	XmlStr = XmlStr & "<expDay>31</expDay>"
	XmlStr = XmlStr & "<action>" & ActionType & "</action>"	'Purchase-1,Credit-2,Void Purchase-3,Authorization-4,Capture-5,Void Credit-6,Void Capture-7,Void Authorization-9
	XmlStr = XmlStr & "<type>CC</type>"		'Credit Card (�CC�) and Stored Value (�SV�) are supported
	XmlStr = XmlStr & "<zip>" & BAZipCode & "</zip>"
	XmlStr = XmlStr & "<addr>" & BACHAddr1 & "</addr>"
	XmlStr = XmlStr & "<member>" & X_ccHolderName & "</member>"
	XmlStr = XmlStr & "<amt>" & formatNumber(X_Amount, 2) & "</amt>"
	XmlStr = XmlStr & "<currencycode>840</currencycode>"
	'XmlStr = XmlStr & "<trackid>100000</trackid>"
	XmlStr = XmlStr & "<udf1>" & LogSavingChargeDataID & "</udf1>"
	XmlStr = XmlStr & "<udf2>" & sTerminalNumber & "</udf2>"
	'XmlStr = XmlStr & "<udf3>CC</udf3>"
	'XmlStr = XmlStr & "<udf4>DD</udf4>"
	'XmlStr = XmlStr & "<udf5>LANTEST2</udf5>"
	'XmlStr = XmlStr & "<eci></eci>"
	'XmlStr = XmlStr & "<xid></xid>"
	'XmlStr = XmlStr & "<cavv></cavv>"

	
	On Error Resume Next
	
		'creating the request
		Dim HttpReq, XMLReceive
		Set HttpReq = CreateObject("Microsoft.XMLHTTP")
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type","text/xml"
		HttpReq.send XmlStr
		Set XMLReceive = HttpReq.ResponseXml
		
		If XMLReceive Is Nothing then
        	call throwError("521")
		Else
		    'Response.Write(XMLReceive.xml)
			'Response.Flush
			If NOT XMLReceive.selectSingleNode("//error_code_tag") is nothing Then
				sReturnCode = XMLReceive.selectSingleNode("//error_code_tag").text
        		call throwError("520")
				'sResDetails = XMLReceive.selectSingleNode("//error_text").text
			Else
				oleDbData.Execute("Update tblLog_Charge Set Reply=590 Where id=" & LogSavingChargeDataID)
				Response.Redirect "SBM3DRedirect.asp?Url=" & Server.UrlEncode(XMLReceive.selectSingleNode("response/url").text) & _
					"&MD=" & XMLReceive.selectSingleNode("response/paymentid").text & "&PAReq=" & Server.UrlEncode(XMLReceive.selectSingleNode("response/PAReq").text)
			End if
		End if
end function

function SBM3DSeconedPart()
	sReturnCode = Request("D3Ret")
    Call DebitIProcessResult(sReturnCode, sApprovalNumber)
end function
%>