<%
Server.ScriptTimeout = 240
Response.Charset = "windows-1255"
%>
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/timer_class.asp"-->
<!--#include file="../include/MerchantTerminals.asp"-->
<!--#include file="remoteCharge_Functions.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
sDCTest = "YES"
NewBillingAddressID = 0
nNewCreditCardID = 0

sIP = Request.ServerVariables("REMOTE_ADDR")
If Left(sIP, 8) <> "192.168." Then ThrowError 500

sCompanyID = 35
sCompanyName = "DC TEST"

nPayerID = 0
X_TransType = 0
X_OrderNumber = ""
X_RefTransID = 0
X_DebitApprovalNumber = ""
X_DebitReferenceCode = ""
X_Track2 = ""
sTransactionTypeID = "0"
PaymentMethod = 0
ccTypeID = 0
X_ccHolderName = "TestCCHolder"
X_PersonalNumber = "TestCCHolderNumber"
X_PhoneNumber = "1234567"
X_Email = "techsupp@netpay-intl.com"
X_ccExpMM = "12"
X_ccExpYY = (Year(Now())+1) mod 100
X_ccCVV2 = "123"
X_ccNumber = "4387751111111111"
X_Customer = 0
X_ConfirmNumber = ""
X_PayerName = ""
X_Amount = 1
X_Currency = 1
X_TypeCredit = 1
X_Payments = 1
X_Comment = "DC TEST"
BACHAddr1 = "TestArrr1"
BACHAddr2 = "TestAddr2"
BACity = "TestCity"
BAZipCode = "12345"
BAState = "IL"
BACountry = "US"

on error resume next
NewBillingAddressID=insertBillingAddress(BACountryCode, BAStateCode, BACountryId, BAStateId, BACity, BACHAddr1, BACHAddr2, BAZipCode)
on error goto 0
If NewBillingAddressID=0 AND bIsBillingAddressMust Then Call throwError(523)
ccBINCountry = CCNumToCountry(X_ccNumber, PaymentMethod, ccBIN) 'Get iso country & cc type from bin
ccTypeEngName = ExecScalar("Select GD_Text From tblGlobalData Where GD_LNG=1 And GD_Group=" & GGROUP_PAYMENTMETHOD & " And GD_ID=" & PaymentMethod, "????")
ccTypeEngShow = ccTypeEngName & " .... " & Right(X_ccNumber, 4)
ccTypeID = PaymentMethod - PMD_CC_UNKNOWN

ccNumberTmp = trim(mid(x_ccNumber,1,4) & " " & mid(x_ccNumber,5,4) & " " & mid(x_ccNumber,9,4) & " " & mid(x_ccNumber,13,4) & " " & mid(x_ccNumber,17,4))
sCCardNum_Encrypt = Replace(ccNumberTmp, "'", "''")

If TestNumVar(nNewCreditCardID, 0, -1, 0) = 0 Then 
	On Error Resume Next
	nNewCreditCardID = insertCard(sCCardNum_Encrypt, X_ccExpMM, X_ccExpYY, X_ccHolderName, X_PayerName, X_ccCVV2, X_PhoneNumber, X_Email, sCompanyID, X_PersonalNumber, ccTypeID, X_Comment, NewBillingAddressID, ccBINCountry)
	On Error Goto 0
End If
If clng(nNewCreditCardID)=0 Then Call throwError(523)

'----------------------------------------------------------------------
'	Find out terminal number to use
'----------------------------------------------------------------------
'PriorityList Setup
X_TerminalHasNext = False
X_TransINC = "X_TransINC_" & sCompanyID & "_" & X_ccNumber & "_" & X_Amount & "_" & X_Currency
XX_Amount = X_Amount
debitCompany = 1
sTerminalNumber = DBText(request("TerminalNumber"))
Call SendCharge()

'----------------------------------------------------------------------
'	Get terminal info using terminal number (inc debiting company)
'----------------------------------------------------------------------
Function GetDebitTerminalInfo(sTermNum)
	Set rsTerminal = Server.CreateObject("Adodb.Recordset")
	rsTerminal.Open "SELECT dc_isActive, dc_TempBlocks, dbo.GetDecrypted256(accountPassword256) As accountPassword," & _
		" dbo.GetDecrypted256(accountPassword3D256) As accountPassword3D, tblDebitTerminals.* FROM tblDebitTerminals" & _
		" LEFT JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany=tblDebitCompany.debitCompany_id" & _
		" WHERE TerminalNumber='" & trim(sTermNum) & "'", oledbData, 0, 1
		GetDebitTerminalInfo = "---"
		If Not rsTerminal.EOF Then
		terminalActive = rsTerminal("isActive")
		debitCompany = rsTerminal("DebitCompany")
		bIsNetpayTerminal = rsTerminal("isNetpayTerminal")
		bIsShvaMasterTerminal = rsTerminal("isShvaMasterTerminal")
		nProcessingMethod = rsTerminal("processingMethod")
		sMcc = rsTerminal("dt_mcc")
		isManipulateAmount = false
		sTermName = Trim(rsTerminal("dt_Descriptor"))
		'terminal login
		sAccountId = Trim(rsTerminal("accountId"))
		sAccountSubId = Trim(rsTerminal("accountSubId"))
		if rsTerminal("accountPassword") <> "" Then sAccountPassword = rsTerminal("accountPassword")
		'terminal 3D login
		sAccountId3D = Trim(rsTerminal("accountId3D"))
		sAccountSubId3D = Trim(rsTerminal("accountSubId3D"))
		if rsTerminal("accountPassword3D") <> "" Then sAccountPassword3D = rsTerminal("accountPassword3D")
	End If
	rsTerminal.close
	bIsUseMaxMind = False
End Function

Function SendCharge()
	X_Amount = XX_Amount 'Restore Amount

	X_DPRET = GetDebitTerminalInfo(sTerminalNumber)
	If X_DPRET <> "---" Then
		If bUseOldTerminal Then throwError(X_DPRET)
		DebitIProcessResult X_DPRET, ""
		SendCharge = False
		Exit Function
	End If	

	'Send to debiting company
	Select Case int(debitCompany)
		case 1 : call debitNULL() 'NULL debitCompany
		case 3,8,9 : Call debitShva(0) 'shva pelecard
		case 11 : Call debitShva(1) 'shva (direct)
		case 14 : If (Not sIsAllow3DTrans) Then call debitSBM() Else call debitSBM3D() 'SBM
		case 16 : Call debitEvertec() 'Evertech systems
		case 18,46 : Call debitB_SN() ' B+S
		case 19 : Call debitJCC() 'JCC
		case 20 : Call debitToggleCard()
		case 21 : Call debitModernProcessing()
		case 25 : Call debitPago()
		case 26 : Call debitPayOn()
		case 32 : Call debitJCC2() 'JCC direct
		case 34 : Call debitWireCard()
		case 35 : Call debitCyberBit()
		case else : Call throwError("503") 'invalid debit company
	End Select
	SendCharge = (Trim(X_DPRET) = "000") Or (Trim(X_DPRET) = "001")
End Function
%>
<!--#include file="remoteCharge_ccFunctions.asp"-->
<!--#include file="remoteCharge_ccEmailSend.asp"-->
<!--#include file="remoteCharge_ccDebitNULL.asp"-->
<!--#include file="remoteCharge_ccDebitEvertec.asp"-->
<!--#include file="remoteCharge_ccDebitJCC.asp"-->
<!--#include file="remoteCharge_ccDebitJCC2.asp"-->
<!--#include file="remoteCharge_ccDebitShva.asp"-->
<!--#include file="remoteCharge_ccDebitToggleCard.asp"-->
<!--#include file="remoteCharge_ccDebitPago.asp"-->
<!--#include file="remoteCharge_ccDebitPayOn.asp"-->
<!--#include file="remoteCharge_ccDebitBNS.asp"-->
<!--#include file="remoteCharge_ccDebitInvik.asp"-->
<!--#include file="remoteCharge_ccDebitSBM.asp"-->
<!--#include file="remoteCharge_ccDebitSBM3D.asp"-->
<!--#include file="remoteCharge_ccDebitWireCard.asp"-->
<!--#include file="remoteCharge_ccDebitCyberBit.asp"-->