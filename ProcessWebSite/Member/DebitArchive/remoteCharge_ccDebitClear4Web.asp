<%
'---------------------------------------------------------------------
'	Debit Clear4Web ()
'---------------------------------------------------------------------
function debitClear4Web()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, EncodedSignature, sApprovalNumber, strAct, IsoCurreny, BrandName, nCoding
	IsoCurreny = Array(3, 1, 2)
	BrandName = Array("-", "-", "1", "-", "-", "2")
	If IsoCurreny(Cint(X_Currency)) = "0" Then Call throwError("506")
	xNames = Split(X_ccHolderName, " ")
	if Ubound(xNames) > -1 Then fName = xNames(0)
	if Ubound(xNames) > 0 Then sName = xNames(1)
	if trim(X_PersonalNumber)="" then
		sPersonalNumber="000000000"
	else
		sPersonalNumber=X_PersonalNumber
	end if
	sCountryNumber = ExecScalar("SELECT ISONumber FROM [List].[CountryList] WHERE CountryID = " & BACountry, "")
	if sCountryNumber = "" Then throwError(543)
	sStateCode = ExecScalar("SELECT StateISOCode FROM [List].[StateList] WHERE stateID = " & BAState, "")
	sCity=trim(BACity & " " & sStateCode)
	nCoding=ExecScalar("SELECT dt_mcc FROM tblDebitTerminals WHERE ID=" & sTerminalNumber, "65535")

	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	UrlAddress = "http://84.94.231.202/default.aspx"
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	X_DebitReferenceCode=GetTransRefCode()
	if (X_TypeCredit = "0") Then
 		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans Not Found
	Else
		ParamList="trans_header=" & sAccountSubId & "=" & sAccountPassword & _
		"&business_number=" & sAccountId & _
		"&trans_number=" & X_DebitReferenceCode & _
		"&trans_subacc=1&trans_type=1" & _
		"&trans_code=" & nCoding & _
		"&owner_name=" & fName & sName & _
		"&owner_id=" & X_PersonalNumber & _
		"&trans_curr=" & IsoCurreny(Cint(X_Currency)) & _
		"&trans_pay=" & X_Amount & _
		"&card_type=" & BrandName(ccTypeID) & _
		"&card_number=" & X_ccNumber & _
		"&card_cvv=" & X_ccCVV2 & _
		"&card_val=" & X_ccExpMM & "." & X_ccExpYY & _
		"&client_name=" & fName & _
		"&client_surname=" & sName & _
		"&client_ident=" & sPersonalNumber & _
		"&client_gender=1" & _
		"&client_br=00.00.0000" & _
		"&client_maiden=sekret" & _
		"&client_phone=" & X_PhoneNumber & _
		"&client_mail=" & X_Email & _
		"&client_address=" & BACHAddr1 & " " & BACHAddr2 & _
		"&client_index=" & BAZipCode & _
		"&client_city=" & sCity & _
		"&client_country=" & sCountryNumber & _
		"&client_notes=" & X_Comment

		On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		'HttpReq.setRequestHeader "Accept-Language", "en-US"
		HttpReq.send ParamList
		On Error GoTo 0
		
		sResponseText=HttpReq.responseText
		If Len(sResponseText) = 0 Then Call throwError("521")
		if left(sResponseText, 8)<>"errcode=" then Call throwError("521")
		if instr(sResponseText, "&errname=")<9 then Call throwError("521")
		sReturnCode=Split(Split(sResponseText, "&")(0))(1)
		if sReturnCode=0 then
			sReturnCode = "000"
			sResDetails=Left(sResponseText, 1000)
		Else
			if sReturnCode = "" Then sReturnCode = "520" 'internal error
			sResDetails=Left(sResponseText, 1000)
		End if
		sApprovalNumber = ""
	End if	
	'Response.Write(ParamList & VbCrLf & VbCrLf & xmlRet.Xml & VbCrLf & VbCrLf & sReturnCode)
	'Response.End
	Set HttpReq = Nothing
		Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
end function
%>