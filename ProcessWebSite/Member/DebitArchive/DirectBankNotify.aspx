<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Dim nCurrency As Integer
	Dim sSql As String
	Dim SourceType As String = 9 'This is the Deposit via DirectEBanking
    
	Sub Page_Load()
		
		nCurrency = IIf(Request.QueryString("currency_id") <> "", dbPages.ExecScalar("SELECT CUR_ID  FROM tblSystemCurrencies WHERE CUR_IsoName='" & Request.QueryString("currency_id") & "'"), 1)
		sSql = "insert into tblWalletEBankingLog (webl_response) values('" & Request.QueryString.ToString & "')"
		dbPages.ExecSql(sSql)
		Dim sUrl As String = Request.QueryString("ipaddress")
		
		sSql = "insert into tblEBanking (eb_UserID,eb_ProjectID,eb_TransID,eb_CompanyID,eb_AccountNumber,eb_Holder,eb_BankCode,eb_BankName,eb_BankBic,eb_BankIban,eb_EmailSenser,eb_CompanyNum) values('" & dbPages.TestVar(Request.QueryString("user_id"), 1, 0, 0) & "'" & _
		",'" & dbPages.TestVar(Request.QueryString("project_id"), 1, 0, 0) & "'" & _
		",'" & dbPages.DBText(Request.QueryString("transaction")) & "'" & _
		",'" & dbPages.DBText(Request.QueryString("user_variable_1")) & "'" & _
		",'" & dbPages.DBText(Request.QueryString("sender_account_number")) & "'" & _
		",'" & dbPages.DBText(Request.QueryString("sender_holder")) & "'" & _
		",'" & dbPages.DBText(Request.QueryString("sender_bank_code")) & "'" & _
		",'" & dbPages.DBText(Request.QueryString("sender_bank_name")) & "'" & _
		",'" & dbPages.DBText(Request.QueryString("sender_bank_bic")) & "'" & _
		",'" & dbPages.DBText(Request.QueryString("sender_iban")) & "'" & _
		",'" & dbPages.DBText(Request.QueryString("email_sender")) & "'" & _
		",'" & dbPages.DBText(Request.QueryString("user_variable_2")) & "'" & _
		")"
		dbPages.ExecSql(sSql)
		
		Dim companyId As String = dbPages.DBText(Request.QueryString("user_variable_1"))
		Dim customerId As String = dbPages.DBText(Request.QueryString("user_variable_2"))
		Dim sInsertDate As Date = Now()
		Dim fraudDetectionLog_id As Integer = 0
		Dim transactionTypeId As Integer = 0
		Dim paymentId As String = ";0;"
		Dim creditTypeId As Integer = 1 ' regular payment
		Dim customerIp As String = "0.0.0.0"
		Dim payments As Integer = 1
		Dim transCurrcency As Integer = nCurrency
		Dim amount As Decimal = Request.QueryString("amount")
		Dim orderNumber As String = 0
		Dim replyCode As String = 0
		Dim comment As String = ""
		Dim replyString As String = ""
		Dim terminalNumber As String = "0000000"
		Dim approvalNumber As String = ""
		Dim pDate As Date = Now()
		Dim isTestOnly As Integer = 1
		Dim MerchantPD As Date = Now()
		Dim iMethodType As Integer = 1
		Dim cardId As Integer = 1
		Dim PaymentMethod As Integer = 16	'in globaldata Db the number of EBanking
		Dim debitCompanyId As Integer = 33	'in TBlDebit Db the number of EBanking
		Dim referringUrl As String = ""
		Dim PayerID As String = ""
		Dim OriginalTransID As Integer = 0
		Dim ccTypeEngString As String = 0
		Dim NetpayFeeRatioCharge As Decimal = 0
		Dim X_OAmount As Decimal = Request.QueryString("amount")
		Dim netpayFeeTransactionCharge As Decimal = 0
		Dim xBinFraud As String = 0
		Dim X_OCurrency As Integer = 2
		Dim X_DebitReferenceCode As String = 0
		Dim CTP_Status As Integer = 0
		Dim debitFeeTransactionCharge As Decimal = 0
		Dim nUnsettledAmount As Decimal = 0
		Dim nRecurringSeries As Integer = 0
		Dim nRecurringChargeNumber As Integer = 0
		Dim nUnsettledInstallments As Integer = 0
		
	    sSQL="SET NOCOUNT ON;"
        If dbPages.UseLocalID Then sSQL = "Declare @curID as int = 1 + IsNull((Select MAX(ID) From tblCompanyTransPass Where ID > " & dbPages.APP_MINID & " And ID < " & dbPages.APP_MAXID & "), " & dbPages.APP_MINID & ");"
		sSQL = sSQL & "INSERT INTO tblCompanyTransPass (" & IIF(dbPages.UseLocalID, "ID, ", "") & "companyID, insertDate, fraudDetectionLog_id, TransSource_id, payID, CreditType, CustomerID, IPAddress," & _
		" Amount, Currency, Payments, OrderNumber, Comment, TerminalNumber, ApprovalNumber," & _
		" isTestOnly, PD, MerchantPD, PaymentMethod_id, PaymentMethodID, PaymentMethod, paymentMethodDisplay, " & _
		" referringUrl, DebitCompanyID, payerIdUsed, OriginalTransID, NetpayFee_RatioCharge, netpayFee_transactionCharge," & _
		" DebitReferenceCode, OCurrency, OAmount, IPCountry, CTP_Status, DebitFee, RecurringSeries, RecurringChargeNumber," & _
		" UnsettledAmount, UnsettledInstallments) " & _
		" VALUES(" & IIF(dbPages.UseLocalID, "@curID, ", "") & companyId & ",'" & sInsertDate & "'," & fraudDetectionLog_id & "," & transactionTypeId & "," & _
		" LEFT('" & paymentId & "', 305), " & creditTypeId & ", " & customerId & ", LEFT('" & customerIp & "', 50)," & _
		" " & FormatNumber(amount, 2, -1, 0, 0) & ", " & transCurrcency & ", " & payments & ", LEFT('" & dbPages.DBText(orderNumber) & "', 100)," & _
		" LEFT('" & dbPages.DBText(comment) & "', 3000), LEFT('" & terminalNumber & "', 10), LEFT('" & approvalNumber & "', 50)," & _
		" " & isTestOnly & ",'" & pDate & "', '" & MerchantPD & "', " & iMethodType & ", " & cardId & "," & PaymentMethod & "," & _
		" LEFT('" & ccTypeEngString & "', 50), LEFT('" & dbPages.DBText(referringUrl) & "', 500), " & debitCompanyId & "," & _
		" LEFT('" & PayerID & "', 10), " & OriginalTransID & ", " & NetpayFeeRatioCharge & ", " & netpayFeeTransactionCharge & "," & _
		" LEFT('" & X_DebitReferenceCode & "', 40), " & X_OCurrency & ", " & X_OAmount & ", LEFT('" & xBinFraud & "', 2)," & _
		" " & CTP_Status & ", " & debitFeeTransactionCharge & ", " & nRecurringSeries & ", " & nRecurringChargeNumber & "," & _
		" " & nUnsettledAmount & ", " & nUnsettledInstallments & ");SELECT " & IIF(dbPages.UseLocalID, "@curID", "SCOPE_IDENTITY()") & ";SET NOCOUNT OFF;"
		dbPages.ExecSql(sSql)
		Response.Write(sSql)
	End Sub
</script>
