<%
'---------------------------------------------------------------------
'	Debit Evertec
'---------------------------------------------------------------------
function debitEcard3d()
   Dim HttpReq, UrlAddress, ParamList, fName, sName
   if(Request("D3") = "1") Then
     debitEcard3d = debitEcard2()
   	 Exit Function
   End if
   xNames = Split(X_ccHolderName, " ")
   if Ubound(xNames) > -1 Then fName = xNames(0)
   if Ubound(xNames) > 0 Then sName = xNames(1)
	 sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	 if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	 If Cint(X_TransType) = 1 Then mSDep = 0 Else mSDep = 1
     X_DebitReferenceCode = GetTransRefCode()

      Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
     'call HttpReq.setOption(2, HttpReq.getOption(2) - SXH_SERVER_CERT_IGNORE_CERT_DATE_INVALID)
      call HttpReq.setOption(3, "LOCAL_MACHINE\Root\" & sAccountId3D)'"\10000006" ' "IWAM_NETPAY\Root"
      UrlAddress = "https://payauth.ecard.pl:444/ws/services/Payment" '"http://webservices.ecard.com/" 'https://pay.ecard.pl/payment/PS '"http://localhost:1609/Service1.asmx"
      ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & VbCrlf & _
  		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & VbCrlf & _
		" <soap:Body>" & VbCrlf & _
		"  <m:verifyEnrollmentThreeDS xmlns:m=""http://webservices.eCard.com"">" & VbCrlf & _
		"   <request xsi:type=""m:VerifyEnrollmentRequest"">" & VbCrlf & _
	    "    <amount xsi:type=""xsd:int"">" & (formatNumber(X_Amount, 2) * 100) & "</amount>" & VbCrlf & _
	    "    <clientIP xsi:type=""xsd:string"">" & sIP & "</clientIP>" & VbCrlf & _
	    "    <country xsi:type=""xsd:int"">616</country>" & VbCrlf & _
	    "    <currency xsi:type=""xsd:int"">840</currency>" & VbCrlf & _
	    "    <cvc xsi:type=""xsd:string"">" & X_ccCVV2 & "</cvc>" & VbCrlf & _
	    "    <expiryMonth xsi:type=""xsd:int"">" & X_ccExpMM & "</expiryMonth>" & VbCrlf & _
	    "    <expiryYear xsi:type=""xsd:int"">20" & X_ccExpYY & "</expiryYear>" & VbCrlf & _
    	"    <merchantid xsi:type=""xsd:int"">" & sAccountId3D & "</merchantid>" & VbCrlf & _ 
	    "    <name xsi:type=""xsd:string"">" & fName & "</name>" & VbCrlf  & _
	    "    <orderdescription xsi:type=""xsd:string"">" & Left(sCompanyDescriptor, 30) & "</orderdescription>" & VbCrlf & _
	    "    <ordernumber xsi:type=""xsd:int"">" & X_DebitReferenceCode & "</ordernumber>" & VbCrlf & _
	    "    <pan xsi:type=""xsd:string"">" & replace(X_ccNumber, " ", "") & "</pan>" & VbCrlf & _
	    "    <password xsi:type=""xsd:string"">" & sAccountPassword3D & "</password>" & VbCrlf & _
	    "    <submerchantid xsi:type=""xsd:int"">" & sDebitCompanyExID & "</submerchantid>" & VbCrlf & _
	    "    <surname xsi:type=""xsd:string"">" & sName & "</surname>" & VbCrlf & _
	    "    <httpAccept xsi:type=""xsd:string"">" & Request.ServerVariables("HTTP_ACCEPT") & "</httpAccept>" & VbCrlf & _
	    "    <httpUserAgent xsi:type=""xsd:string"">" & Request.ServerVariables("HTTP_USER_AGENT") & "</httpUserAgent>" & VbCrlf & _
	    "    <redirectURL xsi:type=""xsd:string"">" & Replace(session("ProcessURL"), "http://", "https://") & "remoteCharge_ccDebitECard3DRet.asp</redirectURL>" & VbCrlf & _
		"   </request>" & VbCrlf & _
		"  </m:verifyEnrollmentThreeDS>" & VbCrlf & _
	    " </soap:Body>" & VbCrlf & _
	    "</soap:Envelope>"
		'Currency 985
	'Response.write(ParamList)
    'Response.End
 	
   On Error Resume Next
	HttpReq.open "POST", UrlAddress, false
	HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
	HttpReq.setRequestHeader "SOAPAction", "http://webservices.ecard.com/"
	HttpReq.send ParamList
	sResDetails = HttpReq.responseText
	set xmlRet = HttpReq.responseXml
    sReturnCode = xmlRet.selectSingleNode("//errCode").text
    s3dsEnable = xmlRet.selectSingleNode("//threeDSResult").text
     if (sReturnCode = "0" And s3dsEnable = "Y") Then
		oleDbData.Execute("Update tblLog_Charge Set Reply=590 Where id=" & LogSavingChargeDataID)
		Response.Write(session("ProcessURL") & "remoteCharge_ccDebitECard3DRedirect.asp?Url=" & Server.UrlEncode(xmlRet.selectSingleNode("//redirectURL").text) & _
			"&TermUrl=" & Server.UrlEncode(Replace(session("ProcessURL"), "http://", "https://") & "remoteCharge_ccDebitECard3DRet.asp?LID=" & LogSavingChargeDataID & "&RefCode=" & X_DebitReferenceCode) & _
			"&MD=" & xmlRet.selectSingleNode("//md").text & "&PAReq=" & Server.UrlEncode(xmlRet.selectSingleNode("//paReq").text))
	    'Response.WRite("After Redirect<br>" & Err.Description & "<br>")
     ElseIf(sReturnCode = "0" And s3dsEnable = "N") Then
	    debitEcard3d = debitEcard2()
     Else
	    Call fn_returnResponse(LogSavingChargeDataID, requestSource, debitCompany, X_TransType, sReturnCode, transactionIdOutParam, insertDateOutParam, X_OrderNumber, formatnumber(X_Amount,2,-1,0,0), X_Payments, X_Currency, sApprovalNumber, X_Comment)
	 End if
	'Response.Write(ParamList & vbcrlf & sResDetails)
	'Response.End

    Set xmlRet = Nothing
    Set HttpReq = Nothing
end function
%>
