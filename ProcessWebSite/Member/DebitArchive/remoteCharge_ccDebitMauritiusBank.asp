<%
'---------------------------------------------------------------------
'	Debit Mauritius Bank
'---------------------------------------------------------------------

    Function GetUrlParam(strUrl, strParam)
		GetUrlParam = Server.HTMLEncode(GetURLValue(strUrl, strParam))
    End Function

    function debitMauritius()
	    'Check currency - only US dollar 
	    if X_Currency <> 1 Then Call throwError("506")
    	
	    'Set transaction type 
        select case cInt(x_transType)
	        case 0
			    If cInt(X_TypeCredit)=1 Then
				    ActionType = 1
			    ElseIf cInt(X_TypeCredit)=0 Then
				    ActionType = 2
			    Else
				    Call throwError("506")
			    End if
		    case 1
			    'DISABLE FOR NOW
			    'If cInt(X_TypeCredit)=1 Then
			    '	ActionType = 4
			    'Else
				    Call throwError("506")
			    'End if
	        case else
			    Call throwError("506")
        end select
    	
	    If sTerminalNumber="100022" Then
		    'TEST
		    UrlAddress = "https://migs.mastercard.com.au/vpcdps"
	    Else
		    'PRODUCTION
		    'UrlAddress = ""
	    End if
	    'building url string to send
	    Dim XmlStr
	    XmlStr = "?vpc_Version=1&vpc_Command=pay" & _
	        "&vpc_MerchTxnRef=" & Cint(Rnd * 10) & _
	        "&vpc_AccessCode=" & sAccountPassword & _
	        "&vpc_Merchant=" & sAccountId & _
	        "&vpc_OrderInfo=" & 0 & _
	        "&vpc_Amount=" & (formatNumber(X_Amount, 2) * 100) & _
	        "&vpc_CardNum=" & replace(X_ccNumber, " ", "") & _
	        "&vpc_CardExp=" & X_ccExpYY & X_ccExpMM & _
	        "&vpc_CardSecurityCode=" & X_ccCVV2
    	    'XmlStr = XmlStr & "<member>" & X_ccHolderName & "</member>"
    	
	    On Error Resume Next
            'Response.Write("Send :" & UrlAddress & XmlStr & "<Br>")
		    'creating the request
		    Dim HttpReq
		    Set HttpReq = CreateObject("Microsoft.XMLHTTP")
		    HttpReq.open "POST", UrlAddress & XmlStr, false
		    'HttpReq.setRequestHeader "Content-Type","text/xml"
		    HttpReq.send ' XmlStr
		    txtReceive = HttpReq.responseText
		    'response.write txtReceive

		    sReturnCode = GetUrlParam(txtReceive, "vpc_TxnResponseCode")
		    if sReturnCode = "0" Then
			    sReturnCode = "000"
			    sApprovalNumber = GetUrlParam(txtReceive, "vpc_ReceiptNo")
		    Else
			    If trim(sReturnCode) = "001" then sReturnCode = "002" '001 is taken by netpay
			    sError = GetUrlParam(txtReceive, "vpc_Message")
		    End if

        'Response.Write("<br>Return :" & txtReceive & sReturnCode)
        'Response.Write("<br>" & sReturnCode & " Error :" & sError & "<Br>" & vbcrlf)
        'Response.Flush
	    On Error GoTo 0 

	    'Connection problem
	    If txtReceive = "" then call throwError("521")
   		Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
    end function
%>
