<%	' Set debit only flag (capture and capture)
'---------------------------------------------------------------------
'	            	debit Albis
'---------------------------------------------------------------------
function debitAlbis()

    select case cInt(X_TypeCredit)
	    case 1
            If cInt(x_transType)>0 then
				Call throwError("506")
            End if    
	    case else
		    Call throwError("506")
    end select

	if X_Currency <> 1 Then Call throwError("506")

	' get Cardholder first and last name
    chNameArr = split(X_ccHolderName, " ")
    if ubound(chNameArr) > 0 then
        sCSFirstName = chNameArr(lbound(chNameArr))
        sCSLastName = chNameArr(ubound(chNameArr))    
    else
        throwError("530")
    end if

    sendStr = "https://www.albis-zahlungsdienste.de/karteres.acgi?"
    sendStr = sendStr & "spr_betrag=" & Replace(formatNumber(X_Amount, 2), ".", ",") & "&"
    sendStr = sendStr & "spr_waehrung=" & "USD" & "&"
    'sendStr = sendStr & "spr_referenz=" & X_OrderNumber & "&"
    sendStr = sendStr & "spr_kartennummer=" & replace(X_ccNumber, " ", "") & "&"
    sendStr = sendStr & "spr_gueltigbis=" & X_ccExpMM & X_ccExpYY & "&"
    sendStr = sendStr & "spr_kkpruef=" & X_ccCVV2 & "&"
    sendStr = sendStr & "spr_vornmae=" & sCSFirstName & "&"
    sendStr = sendStr & "spr_nachname=" & sCSLastName & "&"
    sendStr = sendStr & "spr_haendlerid=19312&spr_haendlercode=Ep2aaJ2nmT&"
    sendStr = sendStr & "spr_buchen=1&spr_warten=20"

	On Error Resume Next
	    Dim HttpReq
	    Set HttpReq = CreateObject("Microsoft.XMLHTTP")
	    HttpReq.open "get", sendStr, False
	    HttpReq.send()
  		responseStr = HttpReq.responseText
	On Error GoTo 0 
	if len(responseStr) = 0 or responseStr = null then throwError("521")

	replyCode = GetURLValue(responseStr, "status")
	if replyCode = "OK" Then
		replyCode = GetURLValue(responseStr, "spr_Ergebnis")
		if replyCode = "0" Then replyCode = "000" Else replyCode =  "999"
	ElseIf replyCode = "WAIT" Then
		replyCode = "001"
	Else
		replyCode =  "999"
	End If
    
    Call DebitIProcessResult(replyCode, sApprovalNumber)
End function
%>