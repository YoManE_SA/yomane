<%
'---------------------------------------------------------------------
'	Debit CommercialLine ()
'---------------------------------------------------------------------
function debitCommercialLine()
    Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, strAct
    if Cint(X_Currency) <> 1 Then Call throwError("506")
    xNames = Split(Trim(X_ccHolderName), " ")
    if Ubound(xNames) > -1 Then fName = xNames(0) Else Call throwError("517")
    if Ubound(xNames) > 0 Then sName = xNames(1) Else Call throwError("517")
    sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
    if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
    Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
    if (X_TypeCredit = "0") Then
        if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
    Else
        If Cint(X_TransType) = 1 Then mSDep = "auth" Else mSDep = "sale" 'sale
        ParamList = "Product=" & sAccountId & _
        "&FNAME=" & fName & _
        "&LNAME=" & sName & _
        "&ADDRESS=" & BACHAddr1 & _
        "&CITY=" & BACity & _
        "&COUNTRY=" & BACountryCode & _
        "&STATE=" & BAStateCode & _
        "&ZIP=" & BAZipCode & _
        "&PHONE=" & X_PhoneNumber & _
        "&EMAIL=" & X_Email & _
        "&IPADDR=" & sIP & _
        "&TRANSTYPE=" & mSDep & _
        "&type=cc" & _
        "&CARDNUMBER=" & Replace(X_ccNumber, " ", "") & _
        "&EXPDATE=" & X_ccExpMM & X_ccExpYY & _
        "&CVV2=" & X_ccCVV2 & _
        "&AMOUNT=" & formatNumber(X_Amount, 2) & _
        "&BankName=Unknown&BankPhone=9723333333"

        If trim(sTerminalNumber) = "900000010" Then
            UrlAddress = "https://omit1pay.com/gw/post.aspx/test"
        ElseIf trim(sTerminalNumber) = "900000020" Then
            UrlAddress = "https://omit1pay.com/gw/post.aspx/submit"
        Else
            UrlAddress = "https://commlinesrl.com/gw/post.aspx/submit"
        End if
        
        On Error Resume Next
            HttpReq.open "POST", UrlAddress, false
            HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
            HttpReq.setRequestHeader "Accept-Language", "en-US"
            HttpReq.send ParamList
            sResDetails = HttpReq.responseText
        On Error GoTo 0
        
        If Len(sResDetails) = 0 Then Call throwError("521")
        sResDetails = Replace(sResDetails, """", "")

        sReturnCode = Left(sResDetails, 1)
        Select case sReturnCode
        case "Y"
            sReturnCode = "000"
            sApprovalNumber = Right(sResDetails, Len(sResDetails) - InStr(1, sResDetails, ","))
            X_DebitReferenceCode = sApprovalNumber
        case else '"E", "N"
            sReturnCode = "590"
            'sError = GetURLValue(sResDetails, "ReasonCodeDesc")
        End Select
	End if
    'Response.Write(ParamList & "<br>" & sResDetails & "<br>" & sApprovalNumber)
    'Response.End
    Set HttpReq = Nothing
   	Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
end function
%>