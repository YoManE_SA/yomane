<%
Server.ScriptTimeout = 240
Response.Charset = "windows-1255"
%>
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/MerchantTerminals.asp" -->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
' initial values - needed for ThrowError (20081124 Tamir)
NewBillingAddressID=0
nNewCreditCardID=0

'Getting user's IP address
Dim sIP, ccBIN, CTP_Status, HttpError, debitCompany, X_DPRET 'last response code
Dim nTransactionID, nInsertDate, sTerminalChargeOptions
ClientIP = DBText(request("ClientIP"))
sIP = trim(Request.ServerVariables( "HTTP_X_FORWARDED_FOR" ))
If Trim(ClientIP) <> "" Then 
	sIP = ClientIP
Elseif sIP = "" Then 
	sIP = Request.ServerVariables( "REMOTE_ADDR" )
End if
debitCompany = 1
X_CompanyNum = right(DBText(request("CompanyNum")),7)
requestSource = DBText(request("requestSource"))
If trim(requestSource) = "" Then requestSource="8"

'Saving charge data into log
LogSavingChargeDataID = 0
LogSavingChargeDataID = Sub_LogSavingChargeData(requestSource, X_CompanyNum, sIP)

'make a delay to milgram
If Request("CompanyNum") = "6054655" Or Request("CompanyNum") = "5112829" Then 
	Call NetpayCryptoObj.Delay(2000)
End if

sSQL="SELECT ID, CompanyName, ActiveStatus, IsApprovalOnly, IsCcStorageCharge, IsMultiChargeProtection, " &_
	"IsCustomerPurchase, IsCustomerPurchaseCVV2, IsCustomerPurchasePersonalNumber, IsCustomerPurchasePhoneNumber, IsCustomerPurchaseEmail, " &_
	"IsRemoteCharge, IsRemoteChargeCVV2, IsRemoteChargePhoneNumber, IsRemoteChargeEmail, IsRemoteChargePersonalNumber, IsSystemPay, " &_
	"IsSystemPayPersonalNumber, IsSystemPayPhoneNumber, IsSystemPayEmail, IsSystemPayCVV2, IsPublicPay, IsPublicPayPersonalNumber, IsPublicPayCVV2, " &_
	"IsPublicPayPhoneNumber, IsPublicPayEmail, IsBillingAddressMust, IsSendUserConfirmationEmail, IsAllowRecurring, IsAllow3DTrans, IsAllowFreeMail, IsUseFraudDetection_MaxMind, MaxMind_MinScore, countryBlackList, descriptor, " &_
	"dailyCcMaxFailCount, debitCompanyExID, AllowedAmounts, IsUsePPWList " &_
	"FROM tblCompany WHERE CustomerNumber='" & X_CompanyNum & "'"
Set rsData2 = oledbData.execute(sSQL)
If rsData2.EOF Then
	rsData2.Close(): Call throwError("500") 'Merchant not found
End if

nActiveStatus = TestNumVar(rsData2("ActiveStatus"), 0, -1, CMPS_NEW)
If nActiveStatus < CMPS_INTEGRATION Then 
	rsData2.Close(): Call throwError("501")  'Merchant blocked/closed
End if	

bIsCheckDailyLimts = true
bIsCheckTerminalUsabe = true
bIsCheckBlockCCNum = true
bIsCheckCommonBlock = true

sCompanyID = rsData2("id")
sCompanyName = rsData2("CompanyName")
bIsBillingAddressMust = rsData2("IsBillingAddressMust")
sIsMultiChargeProtection = rsData2("IsMultiChargeProtection")
sIsSendUserConfirmationEmail = rsData2("IsSendUserConfirmationEmail")
sIsAllow3DTrans = rsData2("IsAllow3DTrans")
bIsUseMaxMind = rsData2("IsUseFraudDetection_MaxMind")
nMaxMind_MinScore = rsData2("MaxMind_MinScore")
bIsAllowFreeMail = rsData2("IsAllowFreeMail")
sCountryBlackList = rsData2("countryBlackList")
sCompanyDescriptor = DBText(rsData2("descriptor"))
sDebitCompanyExID = DBText(rsData2("debitCompanyExID"))
dailyCcMaxFailCount = TestNumVar(rsData2("dailyCcMaxFailCount"), 0, -1, 0)
AllowedAmounts = rsData2("AllowedAmounts")
IsUsePPWList = rsData2("IsUsePPWList")

referringUrl = DBText(request("referringUrl"))
If trim(referringUrl) = "" Then referringUrl = DBText(Request.ServerVariables("HTTP_REFERER"))
nPayerID = TestNumVar(request("PayerID"), 0, -1, 0)
X_TransType = TestNumVar(request("TransType"), 0, -1, 0)
X_OrderNumber = DBText(request("Order"))
X_RefTransID = TestNumVar(request("RefTransID"), 0, -1, 0)
X_DebitApprovalNumber = ""
X_DebitReferenceCode = ""
X_DebitReferenceNum = ""
fraudDetectionLog_id = 0
isTestOnly = 0
sTransactionTypeID = "0"
X_SendVerifyByPhone = False
PaymentMethod = 0 : ccTypeID = 0

If Trim(sIP) <> "" Then
	IpArray = split(sIP,".")
	for i=LBound(IpArray) to UBound(IpArray)
		IpArray(i)=TestNumVar(IpArray(i), 0, 255, 0)
	next
	if UBound(IpArray)>2 then
		 If IpArray(0)=12 AND IpArray(1)=14 Then
			  'Block IP range 12.14.114.0 - 12.14.115.255
			  If IpArray(2)=114 OR IpArray(2)=115  Then Call throwError("582")
		 End if
	 End if
End if

If trim(X_TransType)="3" Then
	'----------------------------------------------------------------------
	'	Collecting data from stored credit card
	'----------------------------------------------------------------------
	if Not rsData2("IsCcStorageCharge") Then 
		rsData2.Close() : Call throwError("xxx")
	End if	

	sTransactionTypeID = "19"
	sCcStorageID = TestNumVar(request("ccStorageID"), 0, -1, 0)

	sSQL="SELECT ID, companyID, ccTypeID, payID, InsertDate, CHPersonalNum, ExpMM, ExpYY, cc_cui, CHPhoneNumber, CHEmail, CHFullName, CCard_display, IPAddress, isDeleted, Comment, dbo.GetDecrypted256(CCard_number256) CCard_number FROM tblCCStorage WHERE id=" & sCcStorageID & " AND CompanyID=" & sCompanyID
	set rsData3 = oledbData.execute(sSQL)
	if not rsData3.EOF Then
		X_ccHolderName = trim(rsData3("CHFullName"))
		X_PersonalNumber = trim(rsData3("CHPersonalNum"))
		X_PhoneNumber = trim(rsData3("CHPhoneNumber"))
		X_Email = trim(rsData3("CHEmail"))
		X_ccExpMM = DBText(rsData3("ExpMM"))
		X_ccExpYY = DBText(rsData3("ExpYY"))
		X_ccCVV2 = DecCVV(rsData3("cc_cui"))
		X_ccNumber = rsData3("CCard_number")
	End if
	rsData3.close
	Set rsData3 = nothing
	X_Customer = 0
	X_ConfirmNumber = ""
	X_PayerName = ""
	X_OrderNumber = DBText(request("OrderNumber"))
	X_Amount = TestNumVar(request("Amount"), 0, -1, 0)
	X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, 0)
	X_TypeCredit = TestNumVar(Request("CreditType"), 0, 8, 1)
	X_Payments = DBText(request("Payments"))
	X_Comment = ""
	if DBText(request("Comment"))<>"" Then
		X_Comment = DBText(request("Comment"))
		X_Comment = Replace(Replace(X_Comment, ">", "&gt;"), "<", "&lt;")
	End if
	'Billing data
	BACHAddr1 = DBText(request("BillingAddress1"))
	BACHAddr2 = DBText(request("BillingAddress2"))
	BACity = DBText(request("BillingCity"))
	BAZipCode = DBText(request("BillingZipCode"))
	BAState = DBText(request("BillingState"))
	BACountry = DBText(request("BillingCountry"))
ElseIf trim(X_TransType)="2" Then
	'----------------------------------------------------------------------
	'	Collecting data from earlier approved transaction
	'----------------------------------------------------------------------
	sTransactionTypeID = "17"
	sApprovalOnlyID = DBText(request("TransApprovalID"))
	If sApprovalOnlyID = "" Then sApprovalOnlyID = "0"
	tmpAmount = TestNumVar(request("Amount"), 0, -1, 0)
	tmpCurrency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, 0)
	bIsUseMaxMind = false 'Overide merchant configuration

	sSQL="SELECT tblCompanyTransApproval.Amount, tblCompanyTransApproval.Currency, tblCompanyTransApproval.Payments, " &_
	"tblCompanyTransApproval.CreditType, tblCompanyTransApproval.OrderNumber, tblCompanyTransApproval.Comment, tblCompanyTransApproval.DebitReferenceCode," &_
	"tblCompanyTransApproval.approvalNumber, tblCompanyTransApproval.TerminalNumber, dbo.GetDecrypted256(tblCreditCard.CCard_number256) CCard_number, tblCreditCard.ExpMM, tblCreditCard.ExpYY, " &_
	"tblCreditCard.Member, tblCreditCard.cc_cui, tblCreditCard.PersonalNumber, tblCreditCard.phoneNumber, tblCreditCard.Email, tblCreditCard.UserName, " &_
	"tblBillingAddress.address1, tblBillingAddress.address2, tblBillingAddress.city, tblBillingAddress.zipCode, tblBillingAddress.stateIso, tblBillingAddress.countryIso " &_
	"FROM tblBillingAddress RIGHT OUTER JOIN tblCreditCard ON tblBillingAddress.id = tblCreditCard.BillingAddressId RIGHT OUTER JOIN tblCompanyTransApproval ON tblCreditCard.ID = tblCompanyTransApproval.CreditCardID " &_
	"WHERE (tblCompanyTransApproval.ID = " & sApprovalOnlyID & ") AND (tblCompanyTransApproval.companyID = " & sCompanyID & ") " &_
	"AND (tblCompanyTransApproval.Amount = " & tmpAmount & ") AND (tblCompanyTransApproval.Currency = " & tmpCurrency & ")"
	set rsData3 = oledbData.execute(sSQL)
	If not rsData3.EOF Then
		X_OrderNumber = rsData3("OrderNumber")
		X_ccCVV2 = DecCVV(rsData3("cc_cui"))
		X_PersonalNumber = trim(rsData3("PersonalNumber"))
		X_PhoneNumber = trim(rsData3("phoneNumber"))
		X_Email = trim(rsData3("Email"))
		X_Payments = trim(rsData3("Payments"))
		X_PayerName = trim(rsData3("UserName"))
		X_ccExpMM = DBText(rsData3("ExpMM"))
		X_ccExpYY = DBText(rsData3("ExpYY"))
		X_Amount = TestNumVar(rsData3("Amount"), 0, -1, 0)
		X_Currency = TestNumVar(rsData3("Currency"), 0, -1, 0)
		X_ccHolderName = trim(rsData3("Member"))
		X_ConfirmNumber = trim(rsData3("approvalNumber"))
		X_Customer = 0
		X_TypeCredit = TestNumVar(rsData3("CreditType"), 0, 8, 1)
		X_Comment = trim(rsData3("Comment"))
		X_DebitReferenceCode = trim(rsData3("DebitReferenceCode"))
		sTerminalNumber = trim(rsData3("TerminalNumber"))
		X_ccNumber = rsData3("CCard_number")

		'Billing data
		BACHAddr1 = trim(rsData3("address1"))
		BACHAddr2 = trim(rsData3("address2"))
		BACity = trim(rsData3("city"))
		BAZipCode = trim(rsData3("zipCode"))
		BAState = trim(rsData3("stateIso"))
		BACountry = trim(rsData3("countryIso"))
	End if
	rsData3.close
	Set rsData3 = nothing
Else
	'----------------------------------------------------------------------
	'	Collecting data to charge a regular or approval only transaction
	'----------------------------------------------------------------------
	If NOT rsData2("IsApprovalOnly") AND X_TransType="1" Then 
		rsData2.Close(): Call throwError("504")
	End if	
	
	sTransactionTypeID = requestSource
	X_Customer = TestNumVar(request("CustomerID"), 1, 0, 0)
	X_ccCVV2 = DBText(request("CVV2"))
	X_PersonalNumber = DBText(request("PersonalNum"))
	X_PhoneNumber = DBText(request("PhoneNumber"))
	X_Email = DBText(request("Email"))
	X_TypeCredit = TestNumVar(Request("TypeCredit"), 0, 8, 1)
	X_Payments = DBText(request("Payments"))
	X_PayerName = DBText(request("UserName"))
	X_ccExpMM = DBText(request("ExpMonth"))
	X_ccExpYY = DBText(request("ExpYear"))
	X_Amount = TestNumVar(request("Amount"), 0, -1, 0)
	X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, 0)
	X_ccHolderName = DBText(request("Member"))
	X_ConfirmNumber = DBText(request("ConfirmationNum"))
	X_Comment = DBText(request("Comment"))
	X_ccNumber = DBText(request("CCard_num1") & request("CCard_num2") & request("CCard_num3") & request("CCard_num4"))
	If X_ccNumber="" Then X_ccNumber = DBText(request("CardNum"))
	
	'Billing data
	BACHAddr1 = DBText(request("BillingAddress1"))
	BACHAddr2 = DBText(request("BillingAddress2"))
	BACity = DBText(request("BillingCity"))
	BAZipCode = DBText(request("BillingZipCode"))
	BAState = DBText(request("BillingState"))
	BACountry = DBText(request("BillingCountry"))
	
	'Recurring data
	nRecurringCount=0
	if trim(request("Recurring1"))<>"" then
		do
			nRecurringCount=nRecurringCount+1
		loop until trim(request("Recurring" & trim(nRecurringCount)))=""
		nRecurringCount=nRecurringCount-1
	end if
	dim saRecurring()
	if nRecurringCount>0 then
		redim saRecurring(nRecurringCount)
		for i=1 to nRecurringCount
			sRecurring=trim(request("Recurring" & trim(i)))
			if sRecurring="" or not IsValidRecurringString(sRecurring) then Call throwError("551") 'Recurring parameters incomplete
			saRecurring(i)=sRecurring
		next
	else
		' check, if there is an old style recurring (Count+Mode+Cycle)
		X_RecurringCount = dbText(request("RecurringCount")) 'how meny tims
		X_RecurringCycle = dbText(request("RecurringCycle")) 'every how many modes
		X_RecurringMode = dbText(request("RecurringMode"))	 '1=day, 2=week, 3=month, 4=year
		if trim(X_RecurringMode)="0" and trim(X_RecurringCycle)="0" and trim(X_RecurringCount)="0" then
			X_RecurringCount=dbText("")
			X_RecurringCycle=dbText("")
			X_RecurringMode=dbText("")
		end if
		' 20080615 Tamir - recurring mode can be also a word or its first letter (case insensitive)
		if X_RecurringMode<>"" then
			if not IsNumeric(TestNumVar(X_RecurringMode, 1, 4, "A")) then
				if StrComp(X_RecurringMode, "DAY", 1)=0 or StrComp(X_RecurringMode, "D", 1)=0 then
					X_RecurringMode=1
				elseif StrComp(X_RecurringMode, "WEEK", 1)=0 or StrComp(X_RecurringMode, "W", 1)=0 then
					X_RecurringMode=2
				elseif StrComp(X_RecurringMode, "MONTH", 1)=0 or StrComp(X_RecurringMode, "M", 1)=0 then
					X_RecurringMode=3
				elseif StrComp(X_RecurringMode, "YEAR", 1)=0 or StrComp(X_RecurringMode, "Y", 1)=0 then
					X_RecurringMode=4
				else
					Call throwError("551") 'Recurring parameters incomplete
				end if
			end if
		end if
		If trim(X_RecurringCount)<>"" OR trim(X_RecurringCycle)<>"" OR trim(X_RecurringMode)<>"" Then
			If NOT rsData2("IsAllowRecurring") Then 
				rsData2.Close(): Call throwError("550")
			End if	
			If trim(X_RecurringCount)="" OR trim(X_RecurringCycle)="" OR  trim(X_RecurringMode)="" Then Call throwError("551") 'Recurring parameters incomplete
			If NOT IsNumeric(X_RecurringCount) OR  NOT IsNumeric(X_RecurringCycle) OR  NOT IsNumeric(X_RecurringMode) Then Call throwError("551") 'Recurring parameters incomplete
		End if
		' if there is a valid old style recurring (Count+Mode+Cycle), convert to the new one
		sRecurring=trim(X_RecurringCount)
		select case trim(X_RecurringMode)
			case "1" : sRecurring=sRecurring & "D"
			case "2" : sRecurring=sRecurring & "W"
			case "3" : sRecurring=sRecurring & "M"
			case "4" : sRecurring=sRecurring & "Y"
		end select
		sRecurring=sRecurring & trim(X_RecurringCycle)
		if sRecurring="" then
			nRecurringCount=0
		elseif not IsValidRecurringString(sRecurring) then
			Call throwError("551")
		else
			nRecurringCount=1
		end if
		redim saRecurring(nRecurringCount)
		if nRecurringCount>0 then saRecurring(1)=sRecurring
	end if
	'Load Original Trans Details From X_RefTransID
	If trim(X_TypeCredit)="0" And (X_RefTransID <> 0) Then
	   Dim RefOAmount
	   Set rsData3 = oledbData.execute("Select DebitReferenceCode, DebitReferenceNum, ApprovalNumber, TerminalNumber, OCurrency, OAmount From tblCompanyTransPass Where ID=" & X_RefTransID)
	   If Not rsData3.EOF Then 
	   	  X_DebitReferenceCode = rsData3("DebitReferenceCode")
	   	  X_DebitReferenceNum = rsData3("DebitReferenceNum")
	   	  X_ConfirmNumber = rsData3("ApprovalNumber")
		  sTerminalNumber = rsData3("TerminalNumber")
		  RefOAmount = rsData3("OAmount")
		  if sTerminalNumber <> "" Then
			 sTerminalChargeOptions = rsData3("OCurrency")
			 CCF_ID = 0
			 bIsCheckTerminalUsabe = false
			 bIsCheckBlockCCNum = false
		  End if  
	   End if
	   rsData3.Close
	   Set rsData3 = Nothing
	   RefAmount = TestNumVar(ExecScalar("Select Sum(OAmount) From tblCompanyTransPass Where CreditType=0 And OriginalTransID=" & X_RefTransID, 0), 0, -1, 0) + X_Amount
	   if RefAmount > RefOAmount Then throwError("533")
	End if
End if

if X_Payments="" then X_Payments = "1"
if X_ccExpMM<>"" then X_ccExpMM = right("0" & trim(X_ccExpMM),2)
if X_ccExpYY<>"" then X_ccExpYY = right(X_ccExpYY,2)
if X_TypeCredit = 0 Then sIsMultiChargeProtection = false 'Overide merchant configuration

if X_PersonalNumber<>"" and len(X_PersonalNumber)>50 then call throwError("508")
if X_PhoneNumber<>"" and len(X_PhoneNumber)>50 then call throwError("508")
if X_Email<>"" and len(X_Email)>80 then call throwError("508")
if X_PayerName<>"" and len(X_PayerName)>100 then call throwError("508")
if X_ccExpMM<>"" and len(X_ccExpMM)>10 then call throwError("508")
if X_ccExpYY<>"" and len(X_ccExpYY)>10 then call throwError("508")
if X_ccHolderName<>"" and len(X_ccHolderName)>100 then call throwError("508")
if X_Comment<>"" and len(X_Comment)>3000 then call throwError("508")

'----------------------------------------------------------------------
'	check merchant permissions by request source
'	only for non local transactions
'----------------------------------------------------------------------
isLocalTransaction = cBool(request("isLocalTransaction"))
isLocalRequest = cBool(Request.ServerVariables("LOCAL_ADDR") = Request.ServerVariables("REMOTE_ADDR"))
canChargeLocally = cBool(isLocalTransaction AND isLocalRequest)

If canChargeLocally Then
	Select Case requestSource
		case "5", "24", "27"
			sIsAllow3DTrans = false
			bIsUseMaxMind = false
			bIsCheckDailyLimts = false
			bIsBillingAddressMust = false
		Case "6", "26"
			'Refund ask admin page, Refund debit cards (toggle)
			bIsCheckDailyLimts = false 'Overide page defult
			bIsUseMaxMind = false 'Overide merchant configuration
			sIsMultiChargeProtection = false 'Overide merchant configuration
			bIsCheckBlockCCNum = false 'Overide page defult
			sIsAllow3DTrans = false
	End select
Else
	Select Case requestSource
		Case "7", "18"
			'manual charge hebrew, manual charge english
			If NOT rsData2("IsSystemPay") Then Call throwError("502")
			If rsData2("IsSystemPayPhoneNumber") AND X_PhoneNumber = "" Then Call throwError("514")
			If rsData2("IsSystemPayEmail") AND X_Email = "" Then Call throwError("515")
			If requestSource<>"18" Then
				If rsData2("IsSystemPayPersonalNumber") AND X_PersonalNumber = "" Then Call throwError("513")
			End if
		Case "9", "11"
			'popup charge english, popup charge hebrew
			If NOT rsData2("IsCustomerPurchase") Then Call throwError("502")
			If rsData2("IsCustomerPurchasePhoneNumber") AND X_PhoneNumber = "" Then Call throwError("514")
			If rsData2("IsCustomerPurchaseEmail") AND X_Email = "" Then Call throwError("515")
			If requestSource<>"9" Then
				If rsData2("IsCustomerPurchasePersonalNumber") AND X_PersonalNumber = "" Then Call throwError("513")
			End If
		Case "12", "19"
			'public charge english, public charge hebrew
			If NOT rsData2("IsPublicPay") Then Call throwError("502")
			If rsData2("IsPublicPayCVV2") AND X_ccCVV2 = "" Then Call throwError("512")
			If rsData2("IsPublicPayPhoneNumber") AND X_PhoneNumber = "" Then Call throwError("514")
			If rsData2("IsPublicPayEmail") AND X_Email = "" Then Call throwError("515")
			If requestSource <> "19" Then
				If rsData2("IsPublicPayPersonalNumber") AND X_PersonalNumber = "" Then Call throwError("513")
			End if
		Case "8"
			'remote charge
			If sTransactionTypeID<>"19" AND sTransactionTypeID<>"17" Then
				If NOT rsData2("IsRemoteCharge") Then Call throwError("502")
				If rsData2("IsRemoteChargeCVV2") AND X_ccCVV2 = "" Then Call throwError("512")
				If rsData2("IsRemoteChargePersonalNumber") AND X_PersonalNumber = "" Then Call throwError("513")
				If rsData2("IsRemoteChargePhoneNumber") AND X_PhoneNumber = "" Then Call throwError("514")
				If rsData2("IsRemoteChargeEmail") AND X_Email = "" Then call throwError("515")
				sIsAllow3DTrans = false
			End if
		Case "20"
			'recurring page
			sIsAllow3DTrans = false		
		Case else
			'invalid request source
			call throwError("526")
	End select
End if
rsData2.Close()

If NOT IsNumeric(X_Amount) OR X_Currency="" OR NOT IsNumeric(replace(X_ccNumber," ","")) Then
	Call throwError("506")
End if

if Not (IsNumericString(X_ccExpMM, 2, 2) And IsNumericString(X_ccExpYY, 2, 2) And IsNumericString(X_Payments, 1, 2) And IsNumericString(X_TypeCredit, 1, 1)) Then 
	Call throwError("506")
End if

If trim(X_ccCVV2) <> "" Then
	If NOT IsNumericString(X_ccCVV2,3,5) Then Call throwError("512")
End if

If CCur(X_Amount) < 0.01 Then Call throwError("505")
If Len(X_Comment) > 255 Then Call throwError("508")
If X_ccHolderName="" Then Call throwError("511")
If IsUseFraudDetection_MaxMind AND trim(ClientIP) = "" Then Call throwError("516")
X_OCurrency = X_Currency : X_OAmount = X_Amount

'----------------------------------------------------------------------
'	Check billing address fields
'----------------------------------------------------------------------
If trim(BACHAddr1) <> "" AND Len(BACHAddr1) > 100 Then Call throwError("540")
If trim(BACHAddr2) <> "" AND Len(BACHAddr2) > 100 Then Call throwError("540")
If trim(BACity) <> "" AND Len(BACity) > 60 Then Call throwError("541")
If trim(BAZipCode) <> "" AND Len(BAZipCode) > 15 Then Call throwError("542")
If bIsBillingAddressMust Then
	If trim(BACHAddr1) = "" Then Call throwError("540")
	If trim(BACity) = "" Then Call throwError("541")
	If trim(BAZipCode) = "" Then Call throwError("542")
	If trim(BACountry) = "" Then Call throwError("544")
End if
If bIsBillingAddressMust OR trim(BACountry)<>"" Then
	If isNumeric(BACountry) Then
		'Country is in numeric format
		BACountryCode = ExecScalar("SELECT CountryISOCode FROM [List].[CountryList] WHERE CountryID = " & BACountry, "")
		if BACountryCode = "" Then throwError(543)
		BACountryId = BACountry
	Else
		'Country is in two letter iso format
		BACountryId = ExecScalar("SELECT CountryID FROM [List].[CountryList] WHERE CountryISOCode = '" & BACountry & "'", "")
		if BACountryId = "" Then throwError(543)
		BACountryCode = BACountry
	End if
End if
If bIsBillingAddressMust OR trim(BAState)<>"" Then
	If trim(BACountryCode)="US" OR trim(BACountryCode)="CA" Then
		If Trim(BAState)<>"" AND isNumeric(BAState) Then
			'State is in numeric format
			BAStateCode = ExecScalar("SELECT StateISOCode FROM [List].[StateList] WHERE stateID = " & BAState, "")
			if BAStateCode = "" Then throwError(543)
			BAStateId = BAState
		Elseif Trim(BAState)<>"" AND NOT isNumeric(BAState) Then
			'State is in two letter iso format
			BAStateId = ExecScalar("SELECT stateID FROM [List].[StateList] WHERE StateISOCode = '" & BAState & "'", "")
			if BAStateId = "" Then throwError(543)
			BAStateCode = BAState
		Else
			call throwError("543")
		End if
	End if
End if

' Insert to billling table (20081124 Tamir)
on error resume next
	NewBillingAddressID=insertBillingAddress(BACountryCode, BAStateCode, BACountryId, BAStateId, BACity, BACHAddr1, BACHAddr2, BAZipCode)
on error goto 0
If NewBillingAddressID=0 AND bIsBillingAddressMust Then Call throwError(523)

'----------------------------------------------------------------------
'	Doing some things
'----------------------------------------------------------------------
X_ccNumber = Fn_CleanCCNumber(X_ccNumber) '���� �� ���� ����� ������
isTestOnly = Fn_IsCreditCardTestOnly(X_ccNumber) '����� ��� ����� ����� ������ ����
ccBINCountry = CCNumToCountry(X_ccNumber, PaymentMethod, ccBIN) 'Get iso country & cc type from bin
ccTypeEngName = ExecScalar("Select GD_Text From tblGlobalData Where GD_LNG=1 And GD_Group=" & GGROUP_PAYMENTMETHOD & " And GD_ID=" & PaymentMethod, "????")
ccTypeEngShow = ccTypeEngName & " .... " & Right(X_ccNumber, 4)
if requestSource = "24" Then isTestOnly = 1 'Admincash test transaction
ccTypeID = PaymentMethod - PMD_CC_UNKNOWN

'----------------------------------------------------------------------
'	Encrypt cc number
'----------------------------------------------------------------------
ccNumberTmp = trim(mid(x_ccNumber,1,4) & " " & mid(x_ccNumber,5,4) & " " & mid(x_ccNumber,9,4) & " " & mid(x_ccNumber,13,4) & " " & mid(x_ccNumber,17,4))
sCCardNum_Encrypt = Replace(ccNumberTmp, "'", "''")

'----------------------------------------------------------------------
'	Check cc number is legal (skip check if isracard or direct)
'----------------------------------------------------------------------
If (PaymentMethod <> PMD_CC_ISRACARD) And (PaymentMethod <> PMD_CC_DIRECT) Then
	If NOT fn_IsValidCCNum(X_ccNumber, true, sCompanyID, 8, X_Amount, X_Currency) Then
		Call throwError("507") 'Card number is not legal
	End if
End if

'----------------------------------------------------------------------
'	Risk Mng - block when card number in credit card black list
'----------------------------------------------------------------------
If bIsCheckBlockCCNum Then
	ExecSql("Delete From tblFraudCcBlackList Where fcbl_InsertDate<'" & DateAdd("h", -12, Now) & "' And fcbl_BlockLevel=2")
	Dim fraudCcBlackList_id : fraudCcBlackList_id = 0
	Set iRs = oleDbData.Execute("Select fraudCcBlackList_id, fcbl_BlockLevel, fcbl_BlockCount From tblFraudCcBlackList Where fcbl_ccNumber256=dbo.GetEncrypted256('" & Replace(sCCardNum_Encrypt, " ", "") & "')"
	If Not iRs.EOF Then
		fraudCcBlackList_id = TestNumVar(iRs("fraudCcBlackList_id"), 0, -1, 0)
		nBlockType = TestNumVar(iRs("fcbl_BlockLevel"), 0, -1, 0)
'		If nBlockType = 2 And (TestNumVar(iRs("fcbl_BlockCount"), 0, -1, 0) < 3) Then fraudCcBlackList_id = 0
	End if
	iRs.Close()
	If fraudCcBlackList_id > 0 Then 
		ExecSql("Update tblFraudCcBlackList Set fcbl_BlockCount = fcbl_BlockCount + 1 Where fraudCcBlackList_id=" & fraudCcBlackList_id)
		If nBlockType = 2 Then throwError(561) Else throwError(509)
	End if	
End if

If bIsCheckCommonBlock Then
	xID = TestNumVar(ExecScalar("Select BL_ID From tblBLCommon Where" & _
		"	 (BL_Value='" & X_Email & "' And BL_Type=" & eBL_Email & ")" & _ 
		" OR (BL_Value='" & X_ccHolderName & "' And BL_Type=" & eBL_FullName & ")" & _
		" OR (BL_Value='" & X_PhoneNumber & "' And BL_Type=" & eBL_Phone & ")" & _
		" OR (BL_Value='" & X_PersonalNumber & "' And BL_Type=" & eBL_PNumber & ")"  _
		, 0), 0, -1, 0)
	If xID > 0 Then throwError(562)
End If

'----------------------------------------------------------------------
'	Risk Mng - block when country in black list
'----------------------------------------------------------------------
If trim(sCountryBlackList)<>"" AND trim(ccBINCountry) <> "--" AND Len(ccBINCountry)=2 Then
	If Int(inStr("," & sCountryBlackList & ",", "," & ccBINCountry & ",")) > 0 Then throwError("581")
	'check IP Country against black list
	If sCountryCode = "" Then sCountryCode = "--"
	If Int(inStr("," & sCountryBlackList & ",", "," & sCountryCode & ",")) > 0 Then throwError("582")
End if

'----------------------------------------------------------------------
'	Risk Mng - block when amount is not in merchant allowed list
'----------------------------------------------------------------------
If Trim(AllowedAmounts) <> "" Then
	if InStr(1, "," & AllowedAmounts & ",", "," & X_Amount & ",") = 0 Then throwError("588")
End if

'----------------------------------------------------------------------
'	Make sure this transaction was not charged in the last 5 minuts
'----------------------------------------------------------------------
If sIsMultiChargeProtection Then
	sSQL="SELECT CAST(CASE WHEN EXISTS(SELECT tblCompanyTransPass.InsertDate FROM tblCompanyTransPass" & _
	" INNER JOIN tblCreditCard ON tblCompanyTransPass.CreditCardID = tblCreditCard.ID WHERE" &_
	" DateDiff(mi, tblCompanyTransPass.InsertDate, GetDate())<=5 AND tblCompanyTransPass.companyID=" & sCompanyID & _
	" AND tblCompanyTransPass.IPAddress='" & sIP & "' AND tblCompanyTransPass.Amount=" & X_Amount & " AND" & _
	" tblCreditCard.CCard_number256=dbo.GetEncrypted256('" & trim(sCCardNum_Encrypt) & "') AND" & _
	" tblCompanyTransPass.Currency=" & X_Currency & " AND tblCompanyTransPass.Payments=" & X_Payments & ")" & _
	" THEN 1 ELSE 0 END AS bit)"
	If ExecScalar(sSQL, "") Then throwError "522"
End if

'----------------------------------------------------------------------
'	Risk Mng - Check credit card daily limits
'----------------------------------------------------------------------
If bIsCheckDailyLimts Then
	CheckPassLimitation sCompanyID, X_Currency, PaymentMethod, X_TypeCredit, trim(sCCardNum_Encrypt), X_Amount, (requestSource = "7" Or requestSource = "18")
	If dailyCcMaxFailCount > 0 Then
		nDate = DateAdd("d", -1, Now)
		vOut = ExecScalar("SELECT count(tblCompanyTransFail.amount) As countFail FROM tblCompanyTransFail LEFT OUTER JOIN tblCreditCard ON(tblCompanyTransFail.CreditCardID = tblCreditCard.ID)" &_
			"WHERE tblCompanyTransFail.companyID=" & sCompanyID & " AND tblCreditCard.CCard_number256 IS NOT NULL AND dbo.GetDecrypted256(tblCreditCard.CCard_number256)='" & trim(sCCardNum_Encrypt) & "' AND tblCompanyTransFail.insertDate>='" & trim(nDate) & "'", 0)
		If cInt(vOut) >= dailyCcMaxFailCount Then Call throwError("587") 'too many trys in fail tbl
	End if
End if


'----------------------------------------------------------------------
'	Risk Mng - Use MaxMind Fraud Detection service
'----------------------------------------------------------------------
If bIsUseMaxMind Then
	%>
	<!--#include file="remoteCharge_ccFraudMaxMind.asp"-->
	<%
End if

XX_Amount = X_Amount
'----------------------------------------------------------------------
'	Find out terminal number to use
'----------------------------------------------------------------------
'PriorityList Setup
X_TerminalHasNext = False
X_TransINC = "X_TransINC_" & sCompanyID & "_" & X_ccNumber

'----------------------------------------------------------------------
'	Insert data to credit card table (20081124 Tamir)
'----------------------------------------------------------------------
If TestNumVar(nNewCreditCardID, 0, -1, 0) = 0 Then 
	On Error Resume Next
	nNewCreditCardID = insertCard(sCCardNum_Encrypt, X_ccExpMM, X_ccExpYY, X_ccHolderName, X_PayerName, X_ccCVV2, X_PhoneNumber, X_Email, sCompanyID, X_PersonalNumber, ccTypeID, X_Comment, NewBillingAddressID, ccBINCountry)
	On Error Goto 0
End If
If clng(nNewCreditCardID)=0 Then Call throwError(523)

If bIsCheckTerminalUsabe Then 
	If nActiveStatus = CMPS_INTEGRATION Then
		If X_ccNumber <> "4580000000000000" Then Call throwError("595") '����� �� ����
		sTerminalChargeOptions = X_Currency
		sTerminalNumber = "0000000"
		bIsCheckDailyLimts = false : bIsUseMaxMind = false
	Else
		Dim mSendTrans : Set mSendTrans = New CSendTrans
		CCF_ID = mSendTrans.GetMerchantTerminal(sCompanyID, PaymentMethod, X_Currency, X_TypeCredit, 0, sTerminalNumber, sTerminalChargeOptions)
	End if
End if

If CCF_ID = -2 Then 'not found
	Call throwError("503")
Elseif CCF_ID = -1 Then 'blocked
	Call throwError("503")
Else
	Call fn_returnResponse(LogSavingChargeDataID, requestSource, debitCompany, X_TransType, dpRet, nTransactionID, nInsertDate, X_OrderNumber, Formatnumber(X_Amount,2,-1,0,0), X_Payments, X_Currency, dpApproval, X_Comment)
End If

Function SendCharge()
	If sTerminalChargeOptions <> X_Currency Then 'Convert Currency if needed
		X_Amount = ConvertCurrency(X_Currency, sTerminalChargeOptions, X_OAmount)
		X_Currency = sTerminalChargeOptions
	Else	
		X_Amount = XX_Amount 'Restore Amount
	End if	
	'----------------------------------------------------------------------
	'	Get terminal info using terminal number (inc debiting company)
	'----------------------------------------------------------------------
	Call Fn_TerminalInfo(debitCompany, bIsNetpayTerminal, bIsShvaMasterTerminal, nProcessingMethod, sMcc, isManipulateAmount, sTerminalNumber)
	If NOT isNumeric(debitCompany) Then throwError("503")
	'If debitCompany = 20 Then bIsUseMaxMind = false

	'----------------------------------------------------------------------
	'	Manipulate amount by terminal request (Subtract 0.01-0.05)
	'----------------------------------------------------------------------
	If isManipulateAmount And Trim(X_TypeCredit) <> "0" Then
		Randomize
		SubtractTmp = Round((Rnd * 0.04) + 0.01, 2)
		X_Amount = X_Amount - SubtractTmp
	End if

	'----------------------------------------------------------------------
	'	Proceed to debit company
	'----------------------------------------------------------------------
	If requestSource = "27" Then
		Call DebitIProcessResult(IIF(nProcessingMethod = 2, "001", "000"), "")
	ElseIf int(nProcessingMethod) = 2 Then
		Call DebitIProcessResult("001", "")
	Elseif int(nProcessingMethod) = 1 Then
		'Send to debiting company
		Select Case int(debitCompany)
			case 1 : call debitNULL() 'NULL debitCompany
			case 3,8,9 : Call debitShva(0) 'shva pelecard
			case 11 : Call debitShva(1) 'shva (direct)
			case 16 : Call debitEvertec() 'Evertech systems
			case 18,46 : Call debitB_SN() ' B+S
			case 19 : Call debitJCC() 'JCC
			case 20 : Call debitToggleCard()
			case 21 : Call debitModernProcessing()
			case 25 : Call debitPago()
			case 26 : Call debitPayOn()
			case 28 : Call debitInatec() 'wirecard
			case else : Call throwError("503") 'invalid debit company
		End Select
	End if
	SendCharge = (Trim(X_DPRET) = "000") Or (Trim(X_DPRET) = "001")
End Function
%>
<!--#include file="remoteCharge_Functions.asp"-->
<!--#include file="remoteCharge_ccFunctions.asp"-->
<!--#include file="remoteCharge_ccEmailSend.asp"-->
<!--#include file="remoteCharge_ccDebitNULL.asp"-->
<!--#include file="remoteCharge_ccDebitEvertec.asp"-->
<!--#include file="remoteCharge_ccDebitJCC.asp"-->
<!--#include file="remoteCharge_ccDebitShva.asp"-->
<!--#include file="remoteCharge_ccDebitToggleCard.asp"-->
<!--#include file="remoteCharge_ccDebitPago.asp"-->
<!--#include file="remoteCharge_ccDebitPayOn.asp"-->
<!--#include file="remoteCharge_ccDebitBNS.asp"-->
<!--#include file="remoteCharge_ccDebitWireCard.asp"-->
<!--#include file="remoteCharge_ccDebitInvik.asp"-->