<%
'---------------------------------------------------------------------
'	Debit Ecard (polish)
'---------------------------------------------------------------------
'const SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS = 13056
function debitEcard2()
   Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer
   if Cint(X_Currency) <> 1 Then Call throwError("506")
   xNames = Split(X_ccHolderName, " ")
   if Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
   if Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
   
 sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
 if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
  Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP

 'call HttpReq.setOption(2, HttpReq.getOption(2) - SXH_SERVER_CERT_IGNORE_CERT_DATE_INVALID)
  call HttpReq.setOption(3, "LOCAL_MACHINE\Root\" & sAccountId)'"\10000006" ' "IWAM_NETPAY\Root"
  'HttpReq.setOption(3) = "LOCAL_MACHINE\Root\" & sAccountId '"\10000006" ' "IWAM_NETPAY\Root"
  HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
  UrlAddress = "https://payauth.ecard.pl:444/ws/services/Payment" '"https://payauth.ecard.pl:444/ws2/services/Payment" "http://webservices.ecard.com/" 'https://pay.ecard.pl/payment/PS '"http://localhost:1609/Service1.asmx"
  if (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
      if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
  	  if X_TypeCredit="0" Then strAct = "refund" Else strAct = "deposit"
	  ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & VbCrlf & _
  		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & VbCrlf & _
		" <soap:Body>" & VbCrlf & _
		"  <" & strAct & ">" & VbCrlf & _
    	"   <merchantid>" & sAccountId & "</merchantid>" & VbCrlf & _ 
	    "   <password>" & sAccountPassword & "</password>" & VbCrlf & _
	    "   <ordernumber>" & X_DebitReferenceCode & "</ordernumber>" & VbCrlf & _
	    "   <amount>" & (formatNumber(X_Amount, 2) * 100) & "</amount>" & VbCrlf & _
		"  </" & strAct & ">" & VbCrlf & _
	    " </soap:Body>" & VbCrlf & _
	    "</soap:Envelope>"
  Else
	  If Cint(X_TransType) = 1 Then mSDep = 0 Else mSDep = 1
  	  If (Request("D3") = "1") And (Request("3D_DebitReferenceCode") <> "") Then 
		  X_DebitReferenceCode = Request("3D_DebitReferenceCode")
		  'Response.Write(X_DebitReferenceCode & "<br>")
		  'Response.End
	  Else
 	      X_DebitReferenceCode = GetTransRefCode()
	  End if
  	  if Request("D3") = "1" Then strAct = "authorizeThreeDS" Else strAct = "authorizeMOTO"
	  ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & VbCrlf & _
	  	"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" & vbCrlf & _
	  	" <SOAP-ENV:Body>" & vbCrlf & _
	  	" <m:" & strAct & " xmlns:m=""http://webservices.eCard.com"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">" & vbCrlf & _
		"  <request xsi:type=""m:AuthorizeRequest"">" & vbCrlf & _
	    "    <amount xsi:type=""xsd:int"">" & (formatNumber(X_Amount, 2) * 100) & "</amount>" & VbCrlf & _
	    "    <clientIP xsi:type=""xsd:string"">" & sIP & "</clientIP>" & VbCrlf & _
   		"    <country xsi:type=""xsd:int"">616</country>" & VbCrlf & _
    	"    <currency xsi:type=""xsd:int"">840</currency>" & VbCrlf & _
    	"    <cvc xsi:type=""xsd:string"">" & X_ccCVV2 & "</cvc>" & VbCrlf & _
    	"    <expiryMonth xsi:type=""xsd:int"">" & X_ccExpMM & "</expiryMonth>" & VbCrlf & _
    	"    <expiryYear xsi:type=""xsd:int"">20" & X_ccExpYY & "</expiryYear>" & VbCrlf & _
		"    <merchantid xsi:type=""xsd:int"">" & sAccountId & "</merchantid>" & VbCrlf & _ 
    	"    <name xsi:type=""xsd:string"">" & fName & "</name>" & VbCrlf  & _
    	"    <orderdescription xsi:type=""xsd:string"">" & Left(sCompanyDescriptor, 30) & "</orderdescription>" & VbCrlf & _
    	"    <ordernumber xsi:type=""xsd:int"">" & X_DebitReferenceCode & "</ordernumber>" & VbCrlf & _
    	"    <pan xsi:type=""xsd:string"">" & replace(X_ccNumber, " ", "") & "</pan>" & VbCrlf & _
    	"    <password xsi:type=""xsd:string"">" & sAccountPassword & "</password>" & VbCrlf & _
    	"    <submerchantid xsi:type=""xsd:int"">" & sDebitCompanyExID & "</submerchantid>" & VbCrlf & _
    	"    <surname xsi:type=""xsd:string"">" & sName & "</surname>" & VbCrlf & _
	    "    <autodeposit xsi:type=""xsd:int"">" & mSDep & "</autodeposit>" & VbCrlf & _
	    "    <threeDSPaRes xsi:type=""xsd:string"">" & Request("D3Ret") & "</threeDSPaRes>" & VbCrlf & _
		"   </request>" & VbCrlf & _
		"  </m:" & strAct & ">" & VbCrlf & _
		" </SOAP-ENV:Body>" & VbCrlf & _
		"</SOAP-ENV:Envelope>"
		'Currency 985
	End if
   On Error Resume Next

	HttpReq.open "POST", UrlAddress, false
	HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
	HttpReq.setRequestHeader "SOAPAction", "http://webservices.ecard.com/"
	HttpReq.send ParamList
    If Len(HttpReq.responseText) = 0 Then Call throwError("521")
	set xmlRet = HttpReq.responseXml
    
	sDebitReturnAnswer = Left(xmlRet.Text, 200)
	sError = xmlRet.selectSingleNode("//error").text
	if (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
	    sReturnCode = xmlRet.selectSingleNode("//validationcode").text
		pmnStatus = xmlRet.selectSingleNode("//pmnStatus").text
	    sApprovalNumber = xmlRet.selectSingleNode("//approvalcode").text
		if (Len(pmnStatus) > 2) And (sReturnCode = "") Then sReturnCode = "000"
		if xmlRet.selectSingleNode("//prc").text <> "0" or xmlRet.selectSingleNode("//rc").text <> "0" Then _
			sReturnCode = xmlRet.selectSingleNode("//rc").text & "," & xmlRet.selectSingleNode("//prc").text
	Else
	    sReturnCode = xmlRet.selectSingleNode("//validationCode").text
		pmnStatus = xmlRet.selectSingleNode("//paymentStatus").text
	    if Len(pmnStatus) < 2 then
			FileAppendData "eCard.txt", Now & vbcrlf & ParamList & vbcrlf & HttpReq.responseText & vbcrlf & vbcrlf & vbcrlf
			throwError("520") 'internal error
	    End if
		sApprovalNumber = xmlRet.selectSingleNode("//approvalCode").text
	End if
	If trim(sReturnCode) = "001" then sReturnCode = "002" '001 is taken by netpay

	if (X_TypeCredit = "0") Then 
    	'Response.Write(ParamList & vbcrlf & HttpReq.responseText & vbcrlf & "<br>Code:" & sReturnCode & ",sApprovalNumber:" & sApprovalNumber & ",pmnStatus:" & pmnStatus & ",Error :" & sError & "<Br>")
	    'Response.End
	End if

    Set xmlRet = Nothing
    Set HttpReq = Nothing
    On Error GoTo 0 
    
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End function
%>