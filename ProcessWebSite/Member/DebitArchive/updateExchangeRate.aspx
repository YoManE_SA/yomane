<%@ Page Language="C#" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Web.Configuration" %>
<script runat="server">
	void WriteToLog(string sText)
	{
		Response.Write(DateTime.Now.ToString()+" "+sText+"<br />");
		Response.Flush();
	}
	
	void Page_Load(object sender, EventArgs e)
	{
		WriteToLog("Starting the process ...");
		WebRequest wreq = WebRequest.Create("http://www.bankisrael.gov.il/heb.shearim/currency.php");
		WriteToLog("Request created.");
		WebResponse wres = wreq.GetResponse();
		WriteToLog("Response received.");
		StreamReader sr = new StreamReader(wres.GetResponseStream());
		WriteToLog("Stream read.");
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.LoadXml(sr.ReadToEnd());
		WriteToLog("XML document loaded.");

		string sUpdateDate = xmlDoc.SelectSingleNode("//CURRENCIES/LAST_UPDATE/text()").Value.Trim();
		if (string.IsNullOrEmpty(sUpdateDate)) throw new MissingFieldException(@"//CURRENCIES/LAST_UPDATE does not exist or contains no value!");
		WriteToLog("Update date: " + sUpdateDate);
		sUpdateDate = "Convert(datetime, '" + sUpdateDate + "', 120)";

		string sIso, sUnits, sRate;
		XmlNode xnCurrency;
		string sSQL = "SELECT CUR_IsoName FROM tblSystemCurrencies";
		SqlDataReader drData = dbPages.ExecReader(sSQL, dbPages.DSN, 30, false);
		WriteToLog("Currencies loaded from DB");
		for (;drData.Read();)
		{
			sIso = drData.GetString(0);
			WriteToLog("Updating " + sIso + " ...");
			xnCurrency = xmlDoc.SelectSingleNode("//CURRENCY[CURRENCYCODE='" + sIso + "']");
			if (xnCurrency != null)
			{
				WriteToLog("&nbsp; &nbsp; &nbsp; Record found.");
				sUnits = xnCurrency.SelectSingleNode("UNIT/text()").Value.Trim();
				WriteToLog("&nbsp; &nbsp; &nbsp; Units: " + sUnits);
				sRate = xnCurrency.SelectSingleNode("RATE/text()").Value.Trim();
				WriteToLog("&nbsp; &nbsp; &nbsp; Rate: " + sRate);
				sSQL = "UPDATE tblSystemCurrencies SET CUR_InsertDate=Getdate(), CUR_UpDate=" + sUpdateDate + ", CUR_BaseRate=" + sRate + @"/" + sUnits + " WHERE CUR_IsoName='" + sIso + "'";
				dbPages.ExecSql(sSQL);
				WriteToLog("&nbsp; &nbsp; &nbsp; Updated.");
			}
		}
		drData.Close();
		WriteToLog("Update complete.");
	}
</script>

