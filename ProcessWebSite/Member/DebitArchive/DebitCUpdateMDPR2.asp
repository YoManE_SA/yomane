<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/const_globalData.asp" -->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/func_mail.asp" -->
<!--#include file="../member/remoteCharge_Functions.asp"-->
<%
Server.ScriptTimeout = 30 * 60
Response.Buffer = False
Dim pMP, iRs, tRs, fso, strFile, sTerminalID, X_DebitReferenceCode, X_DebitReferenceNum, X_OCurrency, X_OAmount, bIsNetpayTerminal, ChbList
Dim BatchID, BatchKey, IsoCurreny, BatchResult, BHInc, RecInc, transIIdx, transDIdx, transDAmount, transCIdx, transCAmount, sOldBatchID
Dim sFileFolder

Const DEBITCOMPANY_MDPR = 21
Set tRs = Server.CreateObject("Adodb.Recordset")
Set iRs = Server.CreateObject("Adodb.Recordset")
Set pMP = Server.CreateObject("Compunet.CModernProcessing")
Set fso = Server.CreateObject("Scripting.FileSystemObject")

Const BCHST_INIT 	= 0
Const BCHST_SEND 	= 1
Const BCHST_UPDATE	= 2
Const BCHST_DONEOK	= 3
Const BCHST_ERROR 	= 4

Const TL_PASS 		= 0
Const TL_AUTH 		= 1
Const TL_CAPTURE	= 2
Const TL_PREPARE	= 4
Const TL_FAIL		= 5

sTerminalID = Request("Terminal") '"99994567"
sOldBatchID = Request("OldBatchID")
IsoCurreny = Array("ILS", "USD", "EUR", "GBP", "AUD", "CAD", "JPY", "NOK")
sFileFolder = "D:/InvikBatch/" 'Server.MapPath()
oledbData.CommandTimeout = 2 * 60

Function ServerSend(str)
	str = str & vbcrlf
 	Response.Write(str)
	strFile = strFile & str
End Function

Function PrepareBatch(xCur)
	Dim xTransID, sTranBatch, prepareIDList
	transCIdx = 0 : transCAmount = 0 : transDIdx = 0 : transDAmount = 0 : transIIdx = 0
	RecInc = RecInc + 1 : BHInc = BHInc + 1
	sTranBatch = pMP.PadLeft(RecInc, 6, "0") & "BH" & " " & pMP.PadLeft(BHInc, 6, "0") & pMP.Format(DateAdd("h", -2, Now), "yyyyMMdd") & "XX" & _
		IsoCurreny(xCur) & " " & pMP.PadRight(tRs("accountId"), 15, " ") & _
		pMP.PadRight(tRs("dt_Name"), 25, " ") & _
		pMP.PadRight(tRs("AccountPassword"), 6, " ") & Space(73)
	ServerSend(sTranBatch)
	
	sSQL="SELECT tblCompanyTransPass.*, tblCompanyTransPass.ID TID, tblCompanyTransPass.insertDate PInsertDate, tblCompanyTransPass.companyid PCompanyID," & _
		"tblCompanyTransPass.CustomerID as PCustomerID, tblCreditCard.*, tblBillingAddress.*, tblCompany.CompanyName, tblCompany.descriptor, dbo.GetDecrypted256(tblCreditCard.CCard_number256) as ccXNumber " & _
		"FROM tblCompanyTransPass " & _
		"LEFT JOIN tblCompany ON tblCompanyTransPass.companyid = tblCompany.ID " & _
		"LEFT JOIN tblCreditCard ON(tblCompanyTransPass.PaymentMethod >=" & PMD_CC_MIN & " And tblCompanyTransPass.PaymentMethod <=" & PMD_CC_MAX & " And tblCompanyTransPass.CreditCardID=tblCreditCard.ID) " & _
		"LEFT JOIN tblBillingAddress ON(tblBillingAddress.id=tblCreditCard.BillingAddressId) " & _
		"Where CTP_Status=" & TL_AUTH & " And TerminalNumber='" & sTerminalID & "' And currency=" & xCur
	'Response.Write(sSQL)
	iRs.Open sSQL, oleDbData, 0, 1
	Do While Not iRs.EOF
		cmpDesc = tRs("dt_name") & " " & iRs("descriptor")
		trAmnt = formatNumber(iRs("amount") * 100, 0, 0, 0, 0)
		if iRs("creditType") = 0 Then 
			transCAmount = transCAmount + (iRs("amount") * 100)
			sTranBatch = "TC "
			transCIdx = transCIdx + 1
		Else 
			transDAmount = transDAmount + (iRs("amount") * 100)
			sTranBatch = "TD "
			transDIdx = transDIdx + 1
		End if	
		
		RecInc = RecInc + 1
		sTranBatch = pMP.PadLeft(RecInc, 6, "0") & sTranBatch & pMP.PadRight(Replace(iRs("ccXNumber"), " ", ""), 19, " ") & _
			pMP.PadLeft(iRs("ExpYY"), 2, "0") & pMP.PadLeft(iRs("ExpMM"), 2, "0") & pMP.PadLeft(iRs("TID"), 12, "0") & _
			pMP.Format(DateAdd("h", -2, Now), "yyyyMMddHHmm") & _
			IIF(iRs("creditType") = 0, "-", "+")
		sTranBatch = sTranBatch & _	
			pMP.PadLeft(trAmnt, 12, "0") & pMP.PadLeft(iRs("ApprovalNumber"), 6, "0") & _
			pMP.PadLeft(0, 12, "0") & pMP.PadLeft("0", 12, "0") & " " & _
			pMP.PadLeft("000", 2, "0") & "000000" & Space(6) & "1" & Space(33)
		ServerSend(sTranBatch)
		
		RecInc = RecInc + 1 : transIIdx = transIIdx + 1
		sTranBatch = pMP.PadLeft(RecInc, 6, "0") & "TI " & _
			pMP.PadRight(Replace(iRs("ccXNumber"), " ", ""), 19, " ") & _
			"0022" & " " & pMP.PadRight(IIF(IsNull(iRs("city")), "", iRs("city")), 20, " ") & _
			pMP.PadRight(tRs("dt_Name"), 20, " ") & _
			pMP.PadRight(tRs("accountId"), 12, " ") & _
			pMP.PadRight(iRs("DebitReferenceCode"), 12, " ") & _
			pMP.PadRight(Left(cmpDesc, 15), 15, " ") & "012" & "59" & _
			"20351000060004420000000000" & "00" & Space(3)
		ServerSend(sTranBatch)
		prepareIDList = prepareIDList & iRs("TID") & ", "
		'oleDbData.Execute "Update tblCompanyTransPass Set CTP_Status=" & TL_PREPARE & " Where ID=" & iRs("TID")
		iRs.MoveNext
	Loop
	iRs.Close
	If prepareIDList <> "" Then prepareIDList = Left(prepareIDList, Len(prepareIDList) - 2)
	oleDbData.Execute "Update tblCompanyTransPass Set CTP_Status=" & TL_PREPARE & " Where ID IN(" & prepareIDList & ")"
	RecInc = RecInc + 1
 	strHeader = pMP.PadLeft(RecInc, 6, "0") & "BT" & " " & _
 		pMP.PadLeft(BHInc, 6, "0") & pMP.PadLeft(transIIdx, 6, "0") & " " & _
 		pMP.PadLeft(transDIdx, 6, "0") & pMP.PadLeft(transDAmount, 16, "0") & " " & _
		pMP.PadLeft(transCIdx, 6, "0") & pMP.PadLeft(transCAmount, 16, "0") & Space(81)
 	ServerSend(strHeader)
End Function

Function FtpServerConnect()
 	On Error Resume Next
		pMP.FtpConnect "ftp://netpay:NeTPay2008@172.17.3.67" '"ftp://udi:udi_netpay@84.94.231.232" '
		If Err.Number <> 0 Then Response.Write("Unable to Connect FTP Server: " & Err.Description & "<br>")
	On Error Goto 0	
End Function

Function SendBatchFile(sFileName)
 	oleDbData.Execute "Update tblLog_DebitBatch Set BatchStatus=" & BCHST_SEND & ", BatchResult='Start Sending' Where BatchTerminalNumber='DEBITOR_MDPR' And BatchID=" & BatchID
 	On Error Resume Next
 	Err.Clear
 	FtpServerConnect()
	pMP.FtpIO True, sFileFolder & sFileName, "INVOICE/" & sFileName
	pMP.FtpClose
 	If Err.Number <> 0 Then
		oleDbData.Execute "Update tblCompanyTransPass Set CTP_Status=" & TL_AUTH & " Where CTP_Status=" & TL_PREPARE & " And DebitCompanyID=" & DEBITCOMPANY_MDPR
		oleDbData.Execute "Update tblLog_DebitBatch Set BatchStatus=" & BCHST_ERROR & ", BatchResult='Send Error:" & Left(Replace(Err.Description, "'", "''"), 35) & "' Where BatchTerminalNumber='DEBITOR_MDPR' And BatchID=" & BatchID
		Response.Write("Send File - " & sFileName & " FAIL:" & Err.number & "<br>")
 	Else
		oleDbData.Execute "Update tblLog_DebitBatch Set BatchStatus=" & BCHST_UPDATE & ", BatchResult='Waiting for results' Where BatchTerminalNumber='DEBITOR_MDPR' And BatchID=" & BatchID
		oleDbData.Execute "Update tblCompanyTransPass Set CTP_Status=" & TL_CAPTURE & ", BTFileName='" & Replace(sFileName, "'", "''") & "' Where CTP_Status=" & TL_PREPARE & " And DebitCompanyID=" & DEBITCOMPANY_MDPR
		Response.Write("Send File - " & sFileName & " OK<br>")
 	End if
 	On Error Goto 0
End Function
 
Function ComapreFolders(xDate, sDir)
	Dim xFileName, xMove
	FtpServerConnect()
 	On Error Resume Next
	pMP.FtpDir = sDir
	If xDate <> "" Then 
		xFileName = "NPAY71100000_" & pMP.Format(xDate, "yy") & CInt(DateDiff("d", DateSerial(Year(Date), 1, 1), xDate)) & ".DAT"
		pMP.FtpIO False, sFileName, sFileFolder & xFileName
	Else
		Response.Write("Comapre Folders...<br>")
		xFileName = pMP.FtpEnumFiles("*.*")
		While xFileName <> ""
			If xFileName <> "." And xFileName <> ".." Then
				xMove = False
				If "E-STATEMENT" = sDir Then 
					xMove = True
					If Not (fso.FileExists(sFileFolder & xFileName) Or fso.FileExists(sFileFolder & xFileName & ".dpa")) Then
						Response.Write("Download " & xFileName & "...<br>")
						pMP.FtpIO False, sFileFolder & xFileName, xFileName
						UpdateTransStatus(xFileName)
					End If	
				ElseIf "netpay_cb_out" = sDir Then
					If Not (fso.FileExists(sFileFolder & "netpay_cb_out/" & xFileName) Or fso.FileExists(sFileFolder & "netpay_cb_out/" & xFileName & ".dpa")) Then
						pMP.FtpIO False, sFileFolder & "netpay_cb_out/" & xFileName, xFileName
						checkCHBFile(xFileName)
    					xMove = True
					End If
				End If
				If xMove Then 
					Response.Write("Moveing REM /" & sDir & "/" & xFileName & " To /DONE/" & sDir & "/" & xFileName & "<br>")
					pMP.FtpRename xFileName, "../DONE/" & sDir & "/" & xFileName
				End If	
			End if
			xFileName = pMP.FtpEnumFiles("")
		Wend
		Response.Write("Done<br>")
	End If
	pMP.FtpClose()
	If ChbList <> "" Then
		On Error Resume Next
		SendMail2 "Netpay system", "info@netpay-intl.com", "lavi@netpay-intl.com", "chargeback transactions from inviq", ChbList
		On Error Goto 0
	End If
End Function

Function checkCHBFile(sFileName)
 	On Error Goto 0
	Response.Write("Parsing CHB " & sFileName & "...<br>")
	Set obfFile = fso.OpenTextFile(sFileFolder & "netpay_cb_out/" & sFileName, 1, False, 0)
	sLine = obfFile.ReadLine() 'header line
	While Not obfFile.AtEndOfStream
		sLine = obfFile.ReadLine()
		nValues = Split(sLine, ";")
		oleDbData.Execute("Insert Into tblLog_CHBNotification(CHBNT_TransID, CHBNT_DebitCompanyID, CHBNT_TerminalNumber, CHBNT_DateRec, CHBNT_Amount)Values(" & _
				nValues(0) & "," & DEBITCOMPANY_MDPR & ",'" & nValues(5) & "', CONVERT(datetime, '" & nValues(13) & "', 120)," & Round(CLng(nValues(8)) / 100, 2) & ")")
	Wend
	obfFile.Close()
	Set obfFile = Nothing
	Response.Write("Done Parsing CHB " & sFileName & "...<br>")
End Function

Function UpdateTransStatus(sFileName)
 	On Error Goto 0
	Response.Write("Parsing " & sFileName & "...<br>")
	Dim sFailedTransList, sPassTransList
	Set obfFile = fso.OpenTextFile(sFileFolder & sFileName, 1, False, 0)
	While Not obfFile.AtEndOfStream
		sLine = obfFile.ReadLine()
		If Mid(sLine, 7, 2) = "TX" Then 
			X_TransID = Mid(sLine, 55, 12)
			TransOK = Mid(sLine, 113, 1)
			ClrType = Trim(Mid(sLine, 40, 15))
			TransDate = Mid(sLine, 68, 8)
			'Response.Write("Trans ID #" & X_TransID & "=" & TransOK & "<br>")
			oleDbData.Execute "Update tblCompanyTransPass Set CTP_Status=" & IIF(TransOK = "N", TL_PASS, TL_FAIL) & " Where ID=" & X_TransID
			If ClrType = "CHARGEBACK" Then ChbList = ChbList & TransDate & " " & X_TransID & ",<br>" & vbCrLf 'ChargeBackTrans(1, "tblCompanyTransPass.ID=" & X_TransID)
		End If
	Wend
	obfFile.Close()
	Set obfFile = Nothing
 	iRs.Open "Select tblCompanyTransPass.*, dbo.GetDecrypted256(tblCreditCard.CCard_number256) as ccXNumber From tblCompanyTransPass " & _
		" LEFT JOIN tblCreditCard ON(tblCompanyTransPass.PaymentMethod >=" & PMD_CC_MIN & " And tblCompanyTransPass.PaymentMethod <=" & PMD_CC_MAX & " And tblCompanyTransPass.CreditCardID=tblCreditCard.ID)" & _
		" Where CTP_Status=" & TL_FAIL & " And DebitCompanyID=" & DEBITCOMPANY_MDPR, oleDbData, 0, 1
	Do While Not iRs.EOF
		X_DebitReferenceCode = iRs("DebitReferenceCode")
		X_DebitReferenceNum = iRs("DebitReferenceNum")
		call insertFailedTransaction(xTransID, "", iRs("companyid"), iRs("FraudDetectionLog_id"), 23, 0, iRs("creditType"), iRs("CustomerID"), iRs("IPAddress"), iRs("amount"), iRs("currency"), iRs("PaymentMethodDisplay"), iRs("payments"), trim(Session("SharingUserName")), "999", "", iRs("PayforText"), iRs("Comment"), iRs("TerminalNumber"), _
			iRs("PaymentMethod"), iRs("PaymentMethod_ID"), iRs("PaymentMethodID"), Fn_IsCreditCardTestOnly(iRs("ccXNumber")), iRs("referringUrl"), iRs("DebitCompanyid"), iRs("transPayerInfo_id"), iRs("transPaymentMethod_id"), X_Is3dSecure, 0)
		sFailedTransList = sFailedTransList & xTransID & ","
		oledbData.Execute "Delete From tblCompanyTransPass WHERE ID=" & iRs("ID")
		iRs.MoveNext
	Loop
	iRs.Close
 	If Len(sPassTransList) > 0 Then sPassTransList = Left(sPassTransList, Len(sPassTransList) - 1)
	If Len(sFailedTransList) > 0 Then sFailedTransList = Left(sFailedTransList, Len(sFailedTransList) - 1)
	'oleDbData.Execute "Update tblLog_DebitBatch Set " & _
	'	" BatchPassTransList='" & sPassTransList & "'" & _
	'	",BatchFailTransList='" & sFailedTransList & "'" & _
	' 	",BatchStatus=" & BCHST_DONEOK & _
	'	" Where BatchTerminalNumber='DEBITOR_MDPR' And BatchID=" & BatchID
	Response.Write("Parse OK<br>")
End Function

Function LoadBatchData()
	Dim strHeader, BCountTD, BCountTC, BCountTI, BSumTD, BSumTC
	RecInc = 0 : BHInc = 0 : BCountTD=0 : BCountTC=0 : BCountTI=0 : BSumTD=0 : BSumTC=0 : BSumTI=0 : strFile = "" : BatchResult = ""
	tRs.Open "SELECT tblDebitTerminals.terminalNumber, tblDebitTerminals.accountId, tblDebitTerminals.accountSubId, tblDebitTerminals.dt_Name, " & _
		"dbo.getDecrypted256(tblDebitTerminals.accountPassword256) As accountPassword, dt_Name, tblCompanyTransPass.currency, Count(tblCompanyTransPass.ID) As PendCount " & _
		"FROM tblDebitTerminals Left Join tblCompanyTransPass ON(tblDebitTerminals.DebitCompany = tblCompanyTransPass.DebitCompanyid And tblCompanyTransPass.TerminalNumber = tblDebitTerminals.terminalNumber) " & _
		"WHERE (tblDebitTerminals.DebitCompany = " & DEBITCOMPANY_MDPR & ") And CTP_Status=" & TL_AUTH & " " & _
		"GROUP BY tblDebitTerminals.terminalNumber, tblDebitTerminals.accountId, tblDebitTerminals.accountSubId, dbo.getDecrypted256(tblDebitTerminals.accountPassword256), dt_Name, tblCompanyTransPass.currency " & _
		"HAVING Count(tblCompanyTransPass.id) > 0 ORDER BY tblDebitTerminals.terminalNumber Asc", oleDbData, 0, 1
	If Not tRs.EOF Then
		RecInc = RecInc + 1
		BatchID = TestNumVar(ExecScalar("Select Max(BatchID) + 1 From tblLog_DebitBatch Where BatchTerminalNumber='DEBITOR_MDPR'", 1), 0, -1, 1)
		BatchKey = BatchID 'BatchKey = ExecScalar("Select Top 1 (BatchInternalKey + 1) From tblLog_DebitBatch Where BatchDebitCompanyID='" & DEBITCOMPANY_MDPR & "' Order By BatchDate Desc", 1)
		oleDbData.Execute "Insert Into tblLog_DebitBatch(BatchID, BatchDebitCompanyID, BatchTerminalNumber, BatchInternalKey)Values(" & BatchID & "," & DEBITCOMPANY_MDPR & ",'DEBITOR_MDPR'," & BatchKey & ")"
		strHeader = pMP.PadLeft(RecInc, 6, "0") & "FH" & " " & pMP.Format(DateAdd("h", -2, Now), "yyyyMMdd") & _
			pMP.PadLeft(BatchKey, 6, "0") & pMP.PadLeft(BatchKey, 6, "0") & " " & _
			pMP.PadRight("NETPAY", 20, " ") & pMP.PadRight("BANQUE INVIK", 20, " ") & " " & _
			pMP.PadRight(tRs("accountSubId"), 15, " ") & "000000000000037" & " " & _
			"NPA" & "BI " & Space(40)
		ServerSend(strHeader)
	End If
	Do While Not tRs.EOF
		If (sOldBatchID <> "") Or (tRs("PendCount") > 0) Then
			sTerminalID = tRs("terminalNumber")
			Response.Write("Terminal " & sTerminalID & " - " & tRs("PendCount") & " - " & GetCurText(tRs("currency")) & " Transactions" & vbcrlf)
			Call PrepareBatch(tRs("currency"))
			BCountTD = BCountTD + transDIdx : BCountTC = BCountTC + transCIdx : BCountTI = BCountTI + transIIdx
			BSumTD = BSumTD + transDAmount : BSumTC = BSumTC + transCAmount
		End if
		tRs.MoveNext 
	Loop
	tRs.Close
	If RecInc > 0 Then
		Dim obfFile, nFileName
		RecInc = RecInc + 1
		strHeader = pMP.PadLeft(RecInc, 6, "0") & "FT" & " " & _
			pMP.PadLeft(BHInc, 6, "0") & _
			pMP.PadLeft(RecInc, 6, "0") & pMP.PadLeft(BCountTI, 6, "0") & " " & _ 
			pMP.PadLeft(BCountTD, 6, "0") & pMP.PadLeft(BSumTD, 16, "0") & " " & _ 
			pMP.PadLeft(BCountTC, 6, "0") & pMP.PadLeft(BSumTC, 16, "0") & Space(75)
 		ServerSend(strHeader)
		
		oleDbData.Execute "Update tblLog_DebitBatch Set " & _
			" BatchCount=" & (BCountTD + BCountTC) & _
			",BatchAmout=" & CCur((BSumTD - BSumTC) / 100) & _
			",BatchStatus=" & BCHST_UPDATE & _
			",BatchResult='" & Left(Replace(BatchResult, "'", "''"), 50) & "'" & _
			" Where BatchTerminalNumber='DEBITOR_MDPR' And BatchID=" & BatchID
		nFileName = "NPAY005N" & BatchID & ".txt"
		Set obfFile = fso.CreateTextFile(sFileFolder & nFileName, True)
			obfFile.Write strFile
			obfFile.Close
		Set obfFile = Nothing
		SendBatchFile(nFileName)
	End If
	ComapreFolders "", "E-STATEMENT"
End Function

If sTerminalID <> "" Then sTerminalID = " And tblDebitTerminals.terminalNumber='" & Replace(sTerminalID, "'", "''") & "'"
Response.Write("<pre>" & vbcrlf)
If Request("RecieveOnly") = "1" Then 
	ComapreFolders "", "E-STATEMENT"
Else
    On Error Goto 0
	Call LoadBatchData()
End If
ComapreFolders "", "netpay_cb_out"
	
Response.Write("</pre>")
Set tRs = Nothing
Set iRs = Nothing
Set pMP = Nothing
Set fso = Nothing
call closeConnection()
%>