<%
'---------------------------------------------------------------------
'	debit Payment Systems
'---------------------------------------------------------------------
function debitSBM()
    IsoCurreny = Array("376", "840", "978", "826", "036", "124", "392")
	'Set transaction type 
	If CInt(X_TransType) = 0 Then
		If CInt(X_TypeCredit)=1 Then
			ActionType = 1
		ElseIf cInt(X_TypeCredit)=0 Then
			ActionType = 2
		Else
			Call throwError("506")
		End If
	ElseIf CInt(X_TransType) = 1 Then 'auth only
		If CInt(X_TypeCredit) = 1 Then 
			ActionType = 4 
		ElseIf CInt(X_TypeCredit) = 0 Then
			ActionType = 9
		Else 
			Call throwError("506")
		End If
	ElseIf CInt(X_TransType) = 2 Then
		ActionType = 5
	End If
    If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_DebitReferenceCode = "" Then Call throwError("532")
	Else 
		X_DebitReferenceCode = GetTransRefCode()
	End If	

	If sTerminalNumber="100001" Then UrlAddress = "https://tstegate.sbmonline.com:443/egate/servlet/TranPortalXMLServlet" _
	Else UrlAddress = ""
	
	Dim XmlStr : XmlStr = ""
	XmlStr = XmlStr & "<id>" & sAccountId & "</id>" & vbCrLf
	XmlStr = XmlStr & "<password>" & sAccountPassword & "</password>" & vbCrLf
	XmlStr = XmlStr & "<action>" & ActionType & "</action>" & vbCrLf 'Purchase-1,Credit-2,Void Purchase-3,Authorization-4,Capture-5,Void Credit-6,Void Capture-7,Void Authorization-9
	XmlStr = XmlStr & "<type>CC</type>" & vbCrLf 'Credit Card (�CC�) and Stored Value (�SV�) are supported
	XmlStr = XmlStr & "<card>" & replace(X_ccNumber, " ", "") & "</card>" & vbCrLf
	XmlStr = XmlStr & "<cvv2>" & X_ccCVV2 & "</cvv2>" & vbCrLf
	XmlStr = XmlStr & "<expYear>20" & X_ccExpYY & "</expYear>" & vbCrLf
	XmlStr = XmlStr & "<expMonth>" & X_ccExpMM & "</expMonth>" & vbCrLf
	XmlStr = XmlStr & "<expDay>31</expDay>" & vbCrLf
	XmlStr = XmlStr & "<zip>" & BAZipCode & "</zip>" & vbCrLf
	XmlStr = XmlStr & "<addr>" & BACHAddr1 & "</addr>" & vbCrLf
	XmlStr = XmlStr & "<member>" & X_ccHolderName & "</member>" & vbCrLf
	XmlStr = XmlStr & "<amt>" & formatNumber(X_Amount, 2) & "</amt>" & vbCrLf
	XmlStr = XmlStr & "<currencycode>" & IsoCurreny(X_Currency) & "</currencycode>" & vbCrLf
	XmlStr = XmlStr & "<transid>" & X_DebitReferenceCode & "</transid>" & vbCrLf
	'XmlStr = XmlStr & "<trackid>" & X_DebitReferenceCode & "</trackid>" & vbCrLf
	'XmlStr = XmlStr & "<eci></eci>"
	'XmlStr = XmlStr & "<xid></xid>"
	'XmlStr = XmlStr & "<cavv></cavv>"
	
    Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
    HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
    On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "text/xml"
		HttpReq.send XmlStr
		sResDetails = HttpReq.responseText
	    HttpError = FormatHttpRequestError(HttpReq)
		Set xmlRet = HttpReq.responseXml
    On Error GoTo 0
	'Response.Write(UrlAddress & " - " & XmlStr & "<br>" & HttpReq.responseText & "AA") : Response.End()
	If IsNull(sResDetails) Or Len(sResDetails) = 0 Then Call throwError("521")
	
	If NOT xmlRet.selectSingleNode("//error_code_tag") Is Nothing Then
		sReturnCode = xmlRet.selectSingleNode("//error_code_tag").text
	Else
		sResultXml = xmlRet.selectSingleNode("//result").text
		sApprovalNumber = xmlRet.selectSingleNode("//auth").text
		X_DebitReferenceCode = xmlRet.selectSingleNode("//tranid").text
		'sRefXml = xmlRet.selectSingleNode("//ref").text
		'sDateXml = xmlRet.selectSingleNode("//postdate").text
		Select case trim(sResultXml)
			case "CAPTURED" sReturnCode = "000"
			case "APPROVED" sReturnCode = "000"
			case else sReturnCode = xmlRet.selectSingleNode("//responsecode").text
		End Select
	End if
    If sReturnCode = "" Then Call throwError("520") 'internal error
    Call DebitIProcessResult(sReturnCode, sApprovalNumber)		
end function
%>