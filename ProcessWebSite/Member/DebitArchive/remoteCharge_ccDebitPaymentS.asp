<%
'---------------------------------------------------------------------
'	debit Payment Systems
'---------------------------------------------------------------------
function debitPaymentSystems()
	' Get psl wallet info from company data
    sql = "SELECT PslWalletId, PslWalletCode FROM tblCompany WHERE ID = " & sCompanyID
    set rsData6 = oledbData.execute(sql)
	sPslWalletId = trim(rsData6("PslWalletId"))
	sPslWalletCode = trim(rsData6("PslWalletCode"))
	If trim(sPslWalletId)="" OR trim(sPslWalletCode)="" Then
		Call throwError("503")
	End if
	
	' Set authorization only flag 
	ActionType = "Auth"
    select case cInt(X_TypeCredit)
	    case 1
            If cInt(x_transType)>0 then
				Call throwError("506")
            End if    
	    case else
		    Call throwError("506")
    end select
	
	 ' get Cardholder first and last name
    chNameArr = split(X_ccHolderName, " ")
    if ubound(chNameArr) > 0 then
        sPslFirstName = chNameArr(lbound(chNameArr))
        sPslLastName = chNameArr(ubound(chNameArr))    
    else
        throwError("530")        
    end if
	
	If sTerminalNumber="101010" OR sTerminalNumber="202020" Then
		'TEST
		UrlAddress = "https://pslinterface.int.waltonmobile.com:444/pslinterface/sentry"
	Else
		'PRODUCTION
		UrlAddress = "https://pslinterface.waltonmobile.com:443/pslinterface/sentry"
	End if
	
	' Building string to send
	Dim XmlStr
	XmlStr = "<?xml version=""1.0""?>"
	XmlStr = XmlStr & "<psldoc>"
		XmlStr = XmlStr & "<merchant>"
			XmlStr = XmlStr & "<merchant-id>" & sAccountId & "</merchant-id>" 'Got this from Steve
			XmlStr = XmlStr & "<merchant-account>" & sAccountSubId & "</merchant-account>" 'Got this from Steve
			XmlStr = XmlStr & "<merchant-passcode>" & sAccountPassword & "</merchant-passcode>" 'Got this from Steve
		XmlStr = XmlStr & "</merchant>"
		XmlStr = XmlStr & "<request type=""purchasecc"">"
			XmlStr = XmlStr & "<merchant-ref>2006</merchant-ref>"
			XmlStr = XmlStr & "<remote-address>80.179.39.10</remote-address>"
			XmlStr = XmlStr & "<wallet-id>" & sPslWalletId & "</wallet-id>"
			XmlStr = XmlStr & "<passcode>" & sPslWalletCode & "</passcode>"
			XmlStr = XmlStr & "<credit-card>"
				XmlStr = XmlStr & "<card-type>" & ccTypeEngName & "</card-type>"
				XmlStr = XmlStr & "<card-number>" & replace(X_ccNumber, " ", "") & "</card-number>"
				XmlStr = XmlStr & "<cvv2-security-code>" & X_ccCVV2 & "</cvv2-security-code>"
				XmlStr = XmlStr & "<name-on-card>" & X_ccHolderName & "</name-on-card>"
				XmlStr = XmlStr & "<expires>"
					XmlStr = XmlStr & "<year>20" & X_ccExpYY & "</year>"
					XmlStr = XmlStr & "<month>" & X_ccExpMM & "</month>"
				XmlStr = XmlStr & "</expires>"
			XmlStr = XmlStr & "</credit-card>"
			XmlStr = XmlStr & "<client>"
				XmlStr = XmlStr & "<identity>"
					XmlStr = XmlStr & "<first-name>" & sPslFirstName & "</first-name>"
					XmlStr = XmlStr & "<last-name>" & sPslLastName & "</last-name>"
	  				if rsData2("IsBillingAddressMust") then
						XmlStr = XmlStr & "<address>"
							XmlStr = XmlStr & "<street-name>" & BACHAddr1 & "</street-name>"
							XmlStr = XmlStr & "<postal-town>" & BACity & "</postal-town>"
							XmlStr = XmlStr & "<county-state>" & BAStateCode & "</county-state>"
							XmlStr = XmlStr & "<postal-code>" & BAZipCode & "</postal-code>"
							XmlStr = XmlStr & "<country>" & BACountryCode & "</country>"
						XmlStr = XmlStr & "</address>"
					end if 
			        XmlStr = XmlStr & "<contact-details>"
						XmlStr = XmlStr & " <email>" & X_Email & "</email>"
			        XmlStr = XmlStr & "</contact-details>"
				XmlStr = XmlStr & "</identity>"
			XmlStr = XmlStr & "</client>"
			XmlStr = XmlStr & "<amount>" & formatNumber(X_Amount, 2) & "</amount>"
			If X_Currency = "1" then
				XmlStr = XmlStr & "<currency>USD</currency>"
			Else
				call throwError("531")
			End if
			XmlStr = XmlStr & "<description>" & X_Email & "</description>"
		XmlStr = XmlStr & "</request>"
	XmlStr = XmlStr & "</psldoc>"

	On Error Resume Next
	
		' Send data
		Set SrvHTTPS = Server.CreateObject("MSXML2.ServerXMLHTTP")
		SrvHTTPS.open "POST",UrlAddress,false
		SrvHTTPS.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
		SrvHTTPS.send "PAYLOAD=" & XmlStr
		
		' Get response
		Set XMLReceive = Server.CreateObject("MSXML2.DOMDocument")
		Set XMLReceive = SrvHTTPS.responseXML
		sResponse = XMLReceive.selectSingleNode("/psldoc/response/status").text
		sReturnCode = XMLReceive.selectSingleNode("/psldoc/response/code").text
		sReturnCode = replace(sReturnCode,"-",".")
	On Error GoTo 0 

	' throw comunication error if the response code is other then ok (http 200)
	'If SrvHTTPS.status <> "200" then
	if len(sResponse) = 0 or sResponse = null then 
        call throwError("521")
	End if

	sResDetails = ""
	sResDetails = sResDetails & "timestamp: " & XMLReceive.selectSingleNode("/psldoc/response/timestamp").text & "<BR>"
	sResDetails = sResDetails & "status: " & XMLReceive.selectSingleNode("/psldoc/response/status").text & "<BR>"
	'sResDetails = sResDetails & "transaction-id: " & XMLReceive.selectSingleNode("/psldoc/response/transaction-id").text & "<BR>"
	If int(sResponse) = 1 then
		'Failed
		sResDetails = sResDetails & "code: " & XMLReceive.selectSingleNode("/psldoc/response/code").text & "<BR>"
		sResDetails = sResDetails & "message: " & XMLReceive.selectSingleNode("/psldoc/response/message").text & "<BR>"
	End if
	
	if int(sResponse) = 0 then sReturnCode = "000"

	Call DebitIProcessResult(sReturnCode, approvalNumber)	
end function
%>