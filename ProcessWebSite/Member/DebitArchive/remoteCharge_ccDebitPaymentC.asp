<%
'---------------------------------------------------------------------
'	            	debit Payment connexions 
'---------------------------------------------------------------------
function debitPConn()

    PCAuthorizationType = 0
    PCCardype = 0
    PCCHFirstName = ""
    PCCHLastName = ""
    PCTrackID = ""
    PCCardNum = replace(X_ccNumber, " ", "")

    'authorization types: 1-debit, 2-preauthorization, 3-settlement, 4-credit       
    select case cInt(X_TypeCredit)
	    case 0
		    PCAuthorizationType = 4
	    case 1
            select case cInt(x_transType)
	            case 0
		            PCAuthorizationType = 1
	            case 1
		            PCAuthorizationType = 2
	            case else
		            throwError("506")
            end select		    
	    case else
		    throwError("506")
    end select
    
    ' get card type
    '1-visa, 2-master card, 22-amex
    select case cInt(ccTypeID)
	    case 2
		    PCCardype = 1
	    case 5
		    PCCardype = 2
	    case 4
		    PCCardype = 22	    
	    case else
		    throwError("506")
    end select    

    ' get Cardholder first and last name
    chNameArr = split(X_ccHolderName, " ")
    if ubound(chNameArr) > 0 then
        PCCHFirstName = chNameArr(lbound(chNameArr))
        PCCHLastName = chNameArr(ubound(chNameArr))    
    else
        throwError("530")        
    end if
    
    ' generate track id
    Randomize
    PCTrackID = session.SessionID & round(rnd * 1000000) 
    
    '---------------------------------------------
    '          build and send the request
    '---------------------------------------------
    urlStr = "https://www.paymentconnexions.com/cgi-bin/cmd.asp"
    sendStr = "Request=AUTHORIZE&"
    sendStr = sendStr & "UserName=NETPAYTEST&"
    sendStr = sendStr & "Password=TEST&"
    sendStr = sendStr & "Mode=2&" ' sync mode?
    sendStr = sendStr & "AuthType=" & PCAuthorizationType & "&" 
    sendStr = sendStr & "TrackID=" & PCTrackID & "&"
    sendStr = sendStr & "FirstName=" & PCCHFirstName & "&"	    
    sendStr = sendStr & "LastName=" & PCCHLastName & "&"
    sendStr = sendStr & "Addr1=" & BACHAddr1 & "&"
    sendStr = sendStr & "Addr2=" & BACHAddr2 & "&"
    sendStr = sendStr & "City=" & BACity & "&"
    sendStr = sendStr & "State=" & BAStateCode & "&"
    sendStr = sendStr & "Country=" & BACountryCode & "&"
    sendStr = sendStr & "ZipCode=" & BAZipCode & "&"	
    'sendStr = sendStr & "Email=" & tempVar & "&"
    'sendStr = sendStr & "IPAddress=" & tempVar & "&"
    sendStr = sendStr & "PaymentID=" & PCCardype & "&" 
    sendStr = sendStr & "AcctInfo1=" & PCCardNum & "&" 
    sendStr = sendStr & "AcctInfo2=" & X_ccCVV2 & "&" 
    sendStr = sendStr & "AcctInfo3=" & X_ccExpMM & X_ccExpYY & "&" 
    sendStr = sendStr & "Amount=" & X_Amount 		    
    'sendStr = sendStr & "CustomerRef=" & tempVar & "&"
    'sendStr = sendStr & "ReversalID=" & tempVar & "&"
    'sendStr = sendStr & "RedirectionURL=&"
    'sendStr = sendStr & "NotificationURL=" 	    	    
    
    'response.Write sendStr : response.End
	
	On Error Resume Next
	
	    Dim HttpReq
	    Set HttpReq = CreateObject("Msxml2.XMLHTTP.3.0")
	    HttpReq.open "POST", urlStr , False
	    HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	    HttpReq.send sendStr
  		responseStr = HttpReq.responseText
  				
	On Error GoTo 0 
	
	'response.Write HttpReq.status : response.End 
	
	' throw comunication error if the response code is other then ok (http 200)
	'if HttpReq.status <> "200" then throwError("521")
	if len(responseStr) = 0 or responseStr = null then throwError("521")
    ' response sample
    '<Response Request="AUTHORIZE" Time="2006-01-23 07:40:02" VID="1" ReturnCode="0"<Status TrackID="641790503922924" TransactionID="28028454" AuthTypeID="1" PaymentID="1" AccountInfo="4580****0000" AmountReq="1.50" Amountpaid="1.50" AuthState="2" AuthCode="0" AuthMsg="Success" ConfirmURL="" AVSCode="T" /></Response>
	
	' create xml document
	Dim xmlDoc
    Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument.3.0")  
    xmlDoc.loadXML(responseStr)

    PCReturnCode = xmlDoc.selectSingleNode("Response/@ReturnCode").nodeValue

    if PCReturnCode = "0" then PCReturnCode = "000"
	Call DebitIProcessResult(PCReturnCode, sApprovalNumber)	
end function
%>