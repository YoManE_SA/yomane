<%
'---------------------------------------------------------------------
'	            	debit Cyprus 
'---------------------------------------------------------------------
function debitCyprus()
    sendStr = "https://secure.yesilada-bank.com/onlinenew/remote_charge.asp?"
    sendStr = sendStr & "Amount=" & formatNumber(X_Amount, 2) & "&"
    sendStr = sendStr & "Currency=" & X_Currency & "&"
    sendStr = sendStr & "TypeCredit=" & X_TypeCredit & "&"
    sendStr = sendStr & "TransType=" & x_transType & "&"
    'sendStr = sendStr & "Order=" & X_OrderNumber & "&"
    sendStr = sendStr & "CardNum=" & replace(X_ccNumber, " ", "") & "&"
    sendStr = sendStr & "ExpMonth=" & X_ccExpMM & "&"
    sendStr = sendStr & "ExpYear=" & X_ccExpYY & "&"
    sendStr = sendStr & "CVV2=" & X_ccCVV2 & "&"
    sendStr = sendStr & "Member=" & X_ccHolderName 

	On Error Resume Next
	
	    Dim HttpReq
	    Set HttpReq = CreateObject("Msxml2.XMLHTTP.3.0")
	    HttpReq.open "get", sendStr, False
	    HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	    HttpReq.send()
  		responseStr = HttpReq.responseText
	
	On Error GoTo 0 

	' throw comunication error if the response code is other than ok (http 200)
	 if len(responseStr) = 0 or responseStr = null then throwError("521")
	
	' create xml document
	Dim xmlDoc
    Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument.3.0")  
    xmlDoc.loadXML(responseStr)

    ' get reply parameters
    replyCode = xmlDoc.selectSingleNode("//Response/ReplyCode/text()").nodeValue
   	If trim(replyCode) = "001" then replyCode = "002" '001 is taken by netpay

  	Call DebitIProcessResult(replyCode, sApprovalNumber)	
end function
%>