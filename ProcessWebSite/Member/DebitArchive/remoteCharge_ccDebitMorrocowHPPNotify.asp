<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="remoteCharge_Functions.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/MerchantTerminals.asp"-->
<!--#include file="../include/timer_class.asp"-->

<%
'---------------------------------------------------------------------
'	Debit DeltaPay
'---------------------------------------------------------------------
FileAppendData "Morrocow.txt", "Request:" & Now & " " & Request.Form & " " & Request.QueryString & vbCrLf
Server.ScriptTimeout = 300

Dim X_OCurrency, X_OAmount, X_DebitReferenceCode, nApprovalRecurringSeries, nRes, X_ReplyURL, IsNotifyRequest, FileDebitCompany
FileDebitCompany = 53

bUseOldTerminal = True
nApprovalRecurringSeries = "NULL"

sReturnCode = Request("responseCode")
sApprovalNumber = Request("pmtSeqId")
X_DebitReferenceCode = Request("orderNumber")
X_DebitApprovalNumber = Request("approvalCode")
'X_CustomErrorDesc = Replace(Request("reason"), "'", "")
'IsNotifyRequest = Request("notification_url") <> ""

Dim passID, pendingID, failID, logChargeWhere
pendingID = ExecScalar("Select companyTransPending_id From tblCompanyTransPending Where DebitCompanyID=" & FileDebitCompany & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
If pendingID = 0 Then 
    passID = ExecScalar("Select ID From tblCompanyTransPass Where DebitCompanyID=" & FileDebitCompany & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
    If passID = 0 Then failID = ExecScalar("Select ID From tblCompanyTransFail Where DebitCompanyID=" & FileDebitCompany & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
    If failID  = 0 Then
        Response.Write("Transaction not found")
        Response.End
    End If
End If

If pendingID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=" & FileDebitCompany & " And Lca_ReplyCode='553' And Lca_TransNum IN(" & pendingID & ")"
ElseIf passID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=" & FileDebitCompany & " And Lca_ReplyCode='000' And Lca_TransNum IN(" & passID & ")"
ElseIf failID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=" & FileDebitCompany & " And Lca_ReplyCode Not IN('000', '001', '553') And Lca_TransNum IN(" & failID & ")"
End If

Dim xLogChargeAttemptID, xMerchantNum, xRequestParams, xSendParams, xRetParams, xDebitCompany, xLogReplyCode
If Not GetTransInfo(logChargeWhere) Then
    Response.Write("Transaction not found")
    Response.End
End If
X_ReplyURL = GetURLValue(xRequestParams, "RetURL")
'Response.Write("Process - " & X_ReplyURL)

If pendingID <> 0 Then
    If Trim(sReturnCode) = "APPROVED" Or Trim(sReturnCode) = "AUTHORIZED" Or Trim(sReturnCode) = "CAPTURED" Then
        sReturnCode = "000"
    ElseIf Trim(sReturnCode) = "PENDING" Then
        sReturnCode = "001"
    Else
        sReturnCode = "002"
    End If
    If sReturnCode <> "001" Then
        ExecSql("Update tblCompanyTransPending Set DebitApprovalNumber='" & X_DebitApprovalNumber & "' Where DebitCompanyID=" & FileDebitCompany & " And DebitReferenceCode='" & X_DebitReferenceCode & "'")
        nRes = MovePendingTransaction("DebitCompanyID=" & FileDebitCompany & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", sReturnCode)
        ExecSql("Update tblLogChargeAttempts Set Lca_ReplyCode='" & sReturnCode & "', Lca_ReplyDesc='" & Replace(X_CustomErrorDesc, "'", "''") & "', Lca_TransNum=" & nRes & ", Lca_ResponseString='" & Replace(Request.Form & "&" & Request.QueryString, "'", "''") & "' Where LogChargeAttempts_id=" & xLogChargeAttemptID)
    End If
Else
    sReturnCode = xLogReplyCode
    nRes = IIF(passID <> 0, passID, failID)
End If
If IsNotifyRequest Then Response.Write("OK") _
Else ReturnResponseFromTransID nRes, sReturnCode
Response.End()

%>