<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="remoteCharge_ccFunctions.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<%
	Dim HttpReq, XMLReceive, XmlStr
	Set HttpReq = Server.CreateObject("Microsoft.XMLHTTP")
	UrlAddress = "https://tstegate.sbmonline.com:443/egate/servlet/MPIPayerAuthenticationXMLServlet"
	sTerminalNumber = "100001"

	Dim sAccountId, sAccountSubId, sAccountPassword
	rsData2.Open "SELECT accountId3D, accountSubId3D, dbo.GetDecrypted256(accountPassword3D256) As accountPassword3D FROM tblDebitTerminals WHERE TerminalNumber='" & trim(sTerminalNumber) & "'", oledbData, 0, 1
	If Not rsData2.EOF Then
		sAccountId3D = Trim(rsLoginInfo("accountId3D"))
		sAccountSubId3D = Trim(rsLoginInfo("accountSubId3D"))
		If rsLoginInfo("accountPassword3D") <> "" Then sAccountPassword3D = rsLoginInfo("accountPassword3D")
	End If
	rsData2.Close()
	
	XmlStr = ""
	XmlStr = XmlStr & "<id>" & sAccountId & "</id>"
	XmlStr = XmlStr & "<password>" & sAccountPassword & "</password>"
	XmlStr = XmlStr & "<paymentid>" & Request.Form(2) & "</paymentid>"
	XmlStr = XmlStr & "<PARes>" & Request.Form(1) & "</PARes>"

    Response.Write(XmlStr)
    
	On Error GoTo 0 
	HttpReq.open "POST", UrlAddress, false
	HttpReq.setRequestHeader "Content-Type","text/xml"
	HttpReq.send XmlStr
	
	Set XMLReceive = HttpReq.responseXML
	If XMLReceive is nothing then
       	call throwError("521")
	Else
		If NOT XMLReceive.selectSingleNode("//error_code_tag") is nothing Then
			sReturnCode = XMLReceive.selectSingleNode("//error_code_tag").text
			Response.Write("ERROR <br><br> " & XMLReceive.xml)
			'Response.Redirect "Error"
			'sResDetails = XMLReceive.selectSingleNode("//error_text").text
		Else
			Response.Write("ERROR <br><br> " & XMLReceive.xml)
			'Response.Flush
			sResDetails = XMLReceive.text 'tranid, paymentid, postdate, auth, ref
			sResultXml = XMLReceive.selectSingleNode("//result").text
			sApprovalNumber = XMLReceive.selectSingleNode("//auth").text
			LogSavingChargeDataID = XMLReceive.selectSingleNode("//udf1").text
			sTerminalNumber = XMLReceive.selectSingleNode("//udf2").text
			Select case trim(sResultXml)
				case "CAPTURED" sReturnCode = "000"
				case "APPROVED" sReturnCode = "000"
				case else sReturnCode = XMLReceive.selectSingleNode("//responsecode").text
			End Select
			oleDbData.Execute "Update tblLog_Charge Set Reply=" & sReturnCode & " Where id=" & LogSavingChargeDataID
			rsData6.Open "Select * From tblLog_Charge Where id=" & LogSavingChargeDataID, oleDbData, 0, 1
			if Not rsData6.EOF Then
				sResDetails = rsData6("RequestForm")
				If sResDetails = "" Then sResDetails = rsData6("QUERY_STRING") _
				Else sResDetails = Right(sResDetails, Len(sResDetails) - 1)
				Response.Write("remote_charge.asp?D3Ret=" & sReturnCode & "&" & sResDetails)
				'Response.Redirect "remote_charge.asp?D3Ret=" & sReturnCode & "&" & sResDetails
			End if
			rsData6.Close
		End if
	End if
%>