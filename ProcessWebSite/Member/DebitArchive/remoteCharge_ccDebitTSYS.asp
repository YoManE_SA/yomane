<%
'---------------------------------------------------------------------
'	Debit debitB_SN
'---------------------------------------------------------------------
Function TSys_Make2DigDate(xNum)
	If Len(xNum) = 1 Then xNum = "0" & xNum
	TSys_Make2DigDate = xNum
End Function

Function TSys_ReplaceEscapeSequences(sText)
	dim i, j, sTemp
	If IsEmpty(sText) Then
		sTemp = ""
	ElseIf InStr(sText, "&") < 1 Then
		sTemp = sText
	Else
		sTemp = Replace(sText, "&;", " ")
		For i = 1 To Len(sTemp)
			If Mid(sTemp, i, 1) = "&" Then
				For j = i To Len(sTemp)
					If Mid(sTemp, i, 1) = ";" Then sTemp = Replace(sTemp, Mid(sTemp, i, j-i+1), Mid(sTemp, i+1, 1))
				Next
			End If
		Next
		sTemp = Replace(Replace(sTemp, "&", " "), ";", " ")
	End If
	TSys_ReplaceEscapeSequences = sTemp
End Function

Function debitTSysSendRequest(data)
	Dim HttpReq
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    If isTestTerminal Then UrlAddress = "https://stagegw.transnox.com/servlets/TransNox_API_Server" _
	Else UrlAddress = "https://stagegw.transnox.com/servlets/TransNox_API_Server" 'new

	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false ', sAccountSubId, sAccountPassword '"paybynet", "start"
		HttpReq.setRequestHeader "User-Agent", "InfoNox"
		HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		HttpReq.send data
		Response.Write(data & vbCrLf & "<br/>" & vbCrLf & HttpReq.responseText)
		Response.End()
	On Error Goto 0
	Set debitTSysSendRequest = HttpReq
End Function

Function debitTSysGetTransactionKey()
	Dim ParamList
	if sAccountAuthCode1 <> "" Then 
		debitTSysGetTransactionKey = sAccountAuthCode1
		Exit Function
	End If
	ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<GenerateKey>" & _
		" <mid>" & sAccountId & "</mid>" & _
		" <userID>" & sAccountSubId & "</userID>" & _
		" <password>" & sAccountPassword & "</password>" & _
		"</GenerateKey>"
	Set HttpReq = debitTSysSendRequest(ParamList)
	On Error Resume Next
		Set xmlRet = HttpReq.responseXml
		sAccountAuthCode1 = xmlRet.selectSingleNode("//transactionKey").text
	On Error Goto 0
	If sAccountAuthCode1 <> "" Then oledbData.Execute("Update tblDebitTerminals Set AuthenticationCode1='" & Replace(sAccountAuthCode1, "'", "''") & "' Where id=" & TerminalId)
	debitTSysGetTransactionKey = sAccountAuthCode1
End Function

Function debitTSys()
	If Trim(X_ccHolderName) <>"" Then X_ccHolderName = TSys_ReplaceEscapeSequences(X_ccHolderName)
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer, debitTransKey ', IsoCurreny
	If int(X_TypeCredit) = 8 Or int(X_Payments) > 1 Then Call throwError("508") 
	'sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	'If Len(sCompanyDescriptor) > 24 Then sCompanyDescriptor = Left(sCompanyDescriptor, 24)

	debitTransKey = debitTSysGetTransactionKey()
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0 And Not debitBNSForceCredit) Then
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		Dim nSendDebitReferenceCode : nSendDebitReferenceCode = X_DebitReferenceCode
		'" <action>" & IIF(Cint(X_TypeCredit)=0, "refund", "capture") & "</action>" & vbCrLf
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<" & bsAction & ">" & vbCrLf & _
		" <deviceID>" & sAccountId & "</deviceID>" & vbCrLf & _
		" <transactionKey>" & debitTransKey & "</transactionKey>" & vbCrLf & _
		" <transactionAmount>" & sBNSFormatedAmount & "</transactionAmount>" & vbCrLf & _
		" <currencyCode>" & GetCurISOName(Cint(X_Currency)) & "</currencyCode>" & vbCrLf & _
		" <transactionID>" & nSendDebitReferenceCode & "</transactionID>" & vbCrLf & _
		"</" & bsAction & ">"
	Else
        Dim bsAction
	    If CInt(X_TypeCredit)=0 Then 
    		bsAction = "credit"
	    Else 
    		If Cint(X_TransType) = 1 Then bsAction = "Auth" Else bsAction = "Sale"
	    End If
        X_DebitReferenceCode = GetTransRefCode()
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<" & bsAction & ">" & vbCrLf & _
		" <deviceID>" & "88700000258802" & "</deviceID>" & vbCrLf & _
		" <transactionKey>" & debitTransKey & "</transactionKey>" & vbCrLf & _
		" <cardDataSource>MANUAL</cardDataSource>" & vbCrLf & _
		" <transactionAmount>" & formatNumber(X_Amount, 2, True, False, False) & "</transactionAmount>" & vbCrLf & _
		" <currencyCode>" & GetCurISOName(Cint(X_Currency)) & "</currencyCode>" & vbCrLf & _
		" <cardNumber>" & Replace(X_ccNumber, " ", "") & "</cardNumber>" & vbCrLf & _
		" <expirationDate>" & TSys_Make2DigDate(X_ccExpMM) & "/" & TSys_Make2DigDate(X_ccExpYY) & "</expirationDate>" & vbCrLf & _
		" <cvv2>" & X_ccCVV2 & "</cvv2>" & vbCrLf & _
		" <cardHolderName>" & X_ccHolderName & "</cardHolderName>" & vbCrLf & _
		" <orderNumber>" & X_DebitReferenceCode & "</orderNumber>" & vbCrLf & _
		" <notifyEmailID>" & X_Email & "</notifyEmailID>" & vbCrLf & _
		" <transDiscountDetails>" & vbCrLf & _
		"  <transTotalDiscountAmount>0</transTotalDiscountAmount>" & vbCrLf & _
		" </transDiscountDetails>" & vbCrLf & _
		"</" & bsAction & ">"
	End if
	
	Set HttpReq = debitTSysSendRequest(ParamList)
	Response.End()
	On Error Resume Next
		HttpError = FormatHttpRequestError(HttpReq)

	    'Made by tamir to hide cvv and card number from log
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "<cvv2>" & X_ccCVV2, "<cvv2>" & GetSafePartialNumber(X_ccCVV2))
		sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sResDetails = Replace(sResDetails, "<cvv2>" & X_ccCVV2, "<cvv2>" & GetSafePartialNumber(X_ccCVV2))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

		Set xmlRet = HttpReq.responseXml
		sReturnCode = Trim(xmlRet.selectSingleNode("//responseCode").text) 'status
		sApprovalNumber = xmlRet.selectSingleNode("//authCode").text
		sError = xmlRet.selectSingleNode("//responseMessage").text
		X_DebitReferenceCode = xmlRet.selectSingleNode("//transactionID").text
		TimeControl.Append("A_SEND") 'Time Optimization *********
	On Error Goto 0
	If Len(sResDetails) = 0 Then Call throwError("521")
	sDebitReturnAnswer = Left(sResDetails, 3900)
	If (Len(sReturnCode) < 4) Then Call throwError("520")
	
	If Trim(sReturnCode) = "0000" Then
		sReturnCode = "000"
	ElseIf Trim(sReturnCode) = "001" Then 
		sReturnCode = "002" '001 is taken by netpay
	End if	

	Set xmlRet = Nothing
	Set HttpReq = Nothing

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>