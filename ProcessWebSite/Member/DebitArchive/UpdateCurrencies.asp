<!--#include file="../include/func_adoConnect.asp"-->
<meta http-equiv="content-type" content="Windows-1255" />
<%
	response.CharSet="Windows-1255"
	Dim HttpReq, xmlRet, xcNodes, fileDate, eurRate, usdRate
	
	Function GetCurCode(xNodes, exRate)
		For x = 0 to xNodes.length - 1
			if xNodes(x).nodeName = "CURRENCYCODE" Then 
				GetCurCode = xNodes(x).text
			Elseif xNodes(x).nodeName = "RATE" Then 
				exRate = xNodes(x).text
			End if
		Next
	End Function

	Set xmlRet = Server.CreateObject("MSXML2.DOMDocument.3.0")
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
	HttpReq.open "GET", "http://www.bankisrael.gov.il/currency.xml", false
	HttpReq.send
%>
<style>
	div.netpayError{font:normal 12px Courier New;color:Red;background-color:White;}
	div.netpayError a {font:normal 12px Courier New;color:Blue;background-color:White;text-decoration:underline;}
</style>
<div class="netpayError">
<%
	if HttpReq.status<>200 then
		%>
			HTTP request to
			<a style="" href="http://www.bankisrael.gov.il/currency.xml" target="_blank">http://www.bankisrael.gov.il/currency.xml</a>
			failed with status <%= HttpReq.status %>
		<%
		response.end
	end if
	xmlRet.loadXML(HttpReq.responseText)
	If xmlRet.parseError<>0 or IsNull(xmlRet) or IsEmpty(xmlRet) or not IsObject(xmlRet) Then
		%>
			HTTP request to
			<a href="http://www.bankisrael.gov.il/currency.xml" target="_blank">http://www.bankisrael.gov.il/currency.xml</a>
			did not return valid XML<hr /><%= HttpReq.responseText %><hr />
		<%
		response.end
	End If
	If IsNull(xmlRet.documentElement) or IsEmpty(xmlRet.documentElement) or not IsObject(xmlRet.documentElement) Then
		%>
			HTTP request to
			<a href="http://www.bankisrael.gov.il/currency.xml" target="_blank">http://www.bankisrael.gov.il/currency.xml</a>
			did not return valid XML document<hr /><%= Server.HTMLEncode(HttpReq.responseText) %><hr />
		<%
		response.end
	End If
	If xmlRet.documentElement.tagName<>"CURRENCIES" Then
		%>
			HTTP request to
			<a href="http://www.bankisrael.gov.il/currency.xml" target="_blank">http://www.bankisrael.gov.il/currency.xml</a>
			returned valid XML document, but document element's tagName is not "CURRENCIES"<hr /><%= Server.HTMLEncode(HttpReq.responseText) %><hr />
		<%
		response.end
	End If
%>
</div>
<%
	Set xcNodes = xmlRet.documentElement.childNodes
	For i = 0 to xcNodes.length - 1
		if xcNodes(i).nodeName = "LAST_UPDATE" Then 
			fileDate = xcNodes(i).text
			fileDate = Day(fileDate) & "/" & Month(fileDate) & "/" & Year(fileDate)
		Elseif xcNodes(i).nodeName = "CURRENCY" Then
			Dim exRate, CurCode
			CurCode = GetCurCode(xcNodes(i).childNodes, exRate)
			If CurCode = "USD" Then
				usdRate = exRate
			ElseIf CurCode = "EUR" Then 
				eurRate = exRate
			End If
			oleDbData.Execute("UPDATE tblSystemCurrencies SET CUR_Update='" & fileDate & "', CUR_BaseRate=" & exRate & " , CUR_InsertDate=Getdate() WHERE CUR_ISOName='" & CurCode & "' AND CUR_Update < '" & fileDate & "'")
		End if
	Next
	
	if fileDate <> "" Then 
		'oleDbData.Execute("Insert Into tblSystemExchangeRates(updateDate,dollarRate,euroRate)Values('" & fileDate & "'," & usdRate & "," & eurRate & ")")
		Response.Write("OK")
	Else
		Response.Write("FAIL")
	End if
%>