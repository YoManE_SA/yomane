<%
'---------------------------------------------------------------------
'	Debit Evertec
'---------------------------------------------------------------------
Function debitWay4()
 Dim pWay4, xRef, f90Value, TempRs, IsoCurreny
  IsoCurreny = Array("376", "840", "978", "826", "036", "124", "392")
  'sTerminalNumber = Card Acceptor = 360130001

  Set TempRs = Server.CreateObject("Adodb.Recordset")
  if (Cint(X_TransType) = 2) Then 
	  sReturnCode = "00"
  Else
	  Set pWay4 = Server.CreateObject("Compunet.WAY4")
	  '"176.168.0.20" - debug
	  pWay4.Server = "192.168.50.60"
	  pWay4.Port = 8151 '20008

	  Response.Write("X1<br>")
	  Response.Flush()

	  'TempRs.Open "Select Max(companyTransPending_id) + 1 From tblCompanyTransPending", oleDbData, 0, 1
	  'X_DebitReferenceCode = TempRs(0) & GetRandomNum(3)
	  'TempRs.Close
	  
	  If Len(X_DebitReferenceCode) > 12 Then X_DebitReferenceCode = Left(X_DebitReferenceCode, 12) _
	  Else X_DebitReferenceCode = pWay4.PadLeft(X_DebitReferenceCode, 12, "0")
	  If X_TypeCredit = "0" Then pWay4.BeginTrans "0420" _
	  Else pWay4.BeginTrans "0100"
	  
	  Response.Write("X2<br>")
	  Response.End()
	  
	   pWay4.AddField "02", Replace(X_ccNumber," ","") 'HLVAR 7-19
	   pWay4.AddField "03", "000000" 'BCD - 6
	   pWay4.AddField "04", pWay4.PadLeft(formatNumber(X_Amount, 2, -1, 0, 0) * 100, 12, "0") 'BCD - 12
	   pWay4.AddField "07", pWay4.Format(DateAdd("h", -2, Now), "MMddhhmmss")	'10 BCD
	   pWay4.AddField "11", X_DebitReferenceCode	'6 BCD
	   pWay4.AddField "12", pWay4.Format(Now, "MMddhhmmss")
	   pWay4.AddField "13", pWay4.Format(Now, "MMddhhmmss") '4 BCD
	   pWay4.AddField "14", X_ccExpYY & X_ccExpMM '4 BCD
	   pWay4.AddField "18", sAccountSubId '4 BCD
	   pWay4.AddField "19", "972" 'Country Code 3 BCD
	   pWay4.AddField "20", "972" 'Country Code 3 BCD
	   pWay4.AddField "22", "000" 'POS EnryMode 3 BCD
	   pWay4.AddField "25", "59"  'POS Service 2 BCD
	   pWay4.AddField "26", "00"  '2 BCD
	   'pWay4.AddField "32", "00" '2 HLVAR BCD
	   pWay4.AddField "37", pWay4.Format(Now, "YDDD") & "XX" & "000000"  '2 BCD
	   pWay4.AddField "41", pWay4.PadRight(sAccountId, 8, " ") 'terminal 8 ASCII
	   pWay4.AddField "42", pWay4.PadRight("NETPAY LTD", 15, " ") 'Acceptor Identification 15 ASCII
	   pWay4.AddField "43", pWay4.PadRight("Hachilazon 6", 25, " ") & pWay4.PadRight("Ramat-Gan", 13, " ") & "IL" 'Acceptor Name Location 40 ASCII
	   pWay4.AddField "49", IsoCurreny(X_Currency) '3 BCD
	   pWay4.AddField "51", IsoCurreny(X_Currency) '3 BCD
	   pWay4.AddField "70", "051" '3 BCD
	   pWay4.AddField "70", "051" '3 BCD
	Response.End
	   
	   
	If X_TypeCredit = "0" Then
		sReturnCode = "00"
		X_DebitApprovalNumber = "000000"
	Else
		on error Resume Next
		if CInt(X_TransType) = 1 Then pWay4.SendTrans "" Else pWay4.SendTrans "C:\Evertec\Trans" & sTerminalNumber & "N.dat"
		if Err.Number <> 0 Then Response.Write(Err.Description)
		on error goto 0
		sReturnCode = pWay4.FieldValue("00039", 0)
		X_DebitApprovalNumber = pWay4.FieldValue("00038")
		'DebitReferenceCode = pWay4.FieldValue("00037")
	End If
	Set pWay4 = Nothing
  End if
  Response.End
  If sReturnCode = "" Then Call throwError("521")
  If Trim(sReturnCode) = "001" Then sReturnCode = "002"
  If CInt(x_transType) = 2 or CInt(x_transType) = 0 Then
     If Trim(sReturnCode) = "00" Then sReturnCode = "001"
  Else
     If Trim(sReturnCode) = "00" Then sReturnCode = "000"
  End if

  Call DebitIProcessResult(sReturnCode, X_DebitApprovalNumber)	
end Function
%>