<%
'---------------------------------------------------------------------
'	Debit Evertec
'---------------------------------------------------------------------
function debitEvertec()
   Dim pEvertec, xRef, f90Value, TempRs
   If X_Currency <> 1 Then Call throwError("506")
  'sTerminalNumber = Card Acceptor = 360130001

  Set TempRs = Server.CreateObject("Adodb.Recordset")
  if (Cint(X_TransType) = 2) Then 
	  sReturnCode = "00"
  Else
	  Set pEvertec = Server.CreateObject("Compunet.Evertec")
	  '"176.168.0.20" - debug
	  if (pEvertec.TickCount Mod 2) = 1 Then pEvertec.Server = "64.178.213.236" Else pEvertec.Server = "64.178.213.237"
	  pEvertec.Port = 20013 '20008
	
	  TempRs.Open "Select Max(companyTransPending_id) + 1 From tblCompanyTransPending", oleDbData, 0, 1
	  X_DebitReferenceCode = TempRs(0) & GetRandomNum(3)
	  TempRs.Close
	  if Len(X_DebitReferenceCode) > 12 Then X_DebitReferenceCode = Left(X_DebitReferenceCode, 12) _
	  Else X_DebitReferenceCode = pEvertec.PadLeft(X_DebitReferenceCode, 12, "0")
	  if X_TypeCredit = "0" Then pEvertec.BeginTrans "0420" _
	  Else pEvertec.BeginTrans "0200"
	   pEvertec.AddField "00003", "000000"
	   pEvertec.AddField "00004", pEvertec.PadLeft(formatNumber(X_Amount, 2, -1, 0, 0) * 100, 12, "0")
	   if X_TypeCredit = "1" Then pEvertec.AddField "00005", pEvertec.PadLeft(formatNumber(X_Amount, 2, -1, 0, 0) * 100, 12, "0")
	   pEvertec.AddField "00007", pEvertec.Format(DateAdd("h", -2, Now), "MMddhhmmss")
	   pEvertec.AddField "00011", pEvertec.PadLeft("15", 6, "0") 'unique
	   pEvertec.AddField "00012", pEvertec.Format(Now, "hhmmss")
	   pEvertec.AddField "00013", pEvertec.Format(Now, "MMdd")
	   pEvertec.AddField "00014", X_ccExpYY & X_ccExpMM
	   pEvertec.AddField "00017", pEvertec.Format(Now, "MMdd")
	   pEvertec.AddField "00018", sAccountSubId 'MCC 4814
	   pEvertec.AddField "00022", "810"
	   pEvertec.AddField "00025", "59"
	   pEvertec.AddField "00032", pEvertec.PadLeft("431265", 9, "0") 'Acquiring Institution Code
	   pEvertec.AddField "00035", pEvertec.PadRight(replace(X_ccNumber," ","") & "=" & X_ccExpYY & X_ccExpMM, 37, "0")
	   pEvertec.AddField "00037", X_DebitReferenceCode
	   if X_TypeCredit = "0" Then
		   'Request("OriginalTransID")
   	       if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		   TempRs.Open "Select * From tblCompanyTransPass Where ID=" & X_RefTransID, oleDbData, 0, 1
		   If Not TempRs.EOF Then
		   	  f90Value = pEvertec.PadRight("0200" & X_DebitReferenceCode & pEvertec.Format(TempRs("InsertDate"), "MMddhhmmss"), 42, "0")
			  pEvertec.AddField "00038", TempRs("ApprovalNumber")
		   End if
		   TempRs.Close
		   Set TempRs = Nothing
	  	   'pEvertec.AddField "00039", "R9"
	   End if
	   pEvertec.AddField "00041", pEvertec.PadRight("084094231238", 16, " ")
	   pEvertec.AddField "00042", pEvertec.PadRight(sTerminalNumber, 15, " ") 'Card Acceptor 360130001
	   pEvertec.AddField "00043", pEvertec.PadLeft(sTermName, 22, " ") & pEvertec.PadLeft("Panama", 13, " ") & "   IL"
	   pEvertec.AddField "00048", "4549121212         01000001"
	   pEvertec.AddField "00049", "972"
	   if X_TypeCredit = "1" Then pEvertec.AddField "00052", "39617C6F07CF48BA"
	   pEvertec.AddField "00060", "BPPRTES4+0000000"
	   pEvertec.AddField "00061", "        00000000000"
	   if X_TypeCredit = "1" Then pEvertec.AddField "00063", "& 0000300045! P100003 " & X_ccCVV2
	   if X_TypeCredit = "0" Then pEvertec.AddField "00090",  f90Value
	   if X_TypeCredit = "1" Then pEvertec.AddField "00120", pEvertec.PadRight("NETPAY", 29, " ")
	   pEvertec.AddField "00123", Left(X_DebitReferenceCode, 10) & Left(X_DebitReferenceCode, 10)
	   pEvertec.AddField "00125", pEvertec.PadRight("PGCASTECHN", 12, " ")
	   if X_TypeCredit = "0" Then
		   sReturnCode = "00"
		   X_DebitApprovalNumber = "000000"
	   Else
		  on error Resume Next
		  if CInt(X_TransType) = 1 Then pEvertec.SendTrans "" Else pEvertec.SendTrans "C:\Evertec\Trans" & sTerminalNumber & "N.dat"
		  if Err.Number <> 0 Then Response.Write(Err.Description)
		  on error goto 0
		  sReturnCode = pEvertec.FieldValue("00039", 0)
		  X_DebitApprovalNumber = pEvertec.FieldValue("00038")
		  'DebitReferenceCode = pEvertec.FieldValue("00037")
	   End if
	 Set pEvertec = Nothing
  End if
  'Response.End
  if sReturnCode = "" Then Call throwError("521")
  if Trim(sReturnCode) = "001" Then sReturnCode = "002"
  if CInt(x_transType) = 2 or CInt(x_transType) = 0 Then
     If Trim(sReturnCode) = "00" Then sReturnCode = "001"
  Else
     If Trim(sReturnCode) = "00" Then sReturnCode = "000"
				End if

  Call DebitIProcessResult(sReturnCode, X_DebitApprovalNumber)	
end function
%>