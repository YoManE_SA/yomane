<%
'---------------------------------------------------------------------
'	Debit Pago () //25
'---------------------------------------------------------------------
Function ZeroPad(bVal, bLen)
    ZeroPad = bVal
    While Len(ZeroPad) < bLen
        ZeroPad = "0" & ZeroPad
    Wend
End Function

Function createPagoHeader()
   sTimeStamp = ZeroPad(Year(Now), 4) & "-" & ZeroPad(Month(Now), 2) & "-" & ZeroPad(Day(Now), 2) & "T" & ZeroPad(Hour(Now), 2) & ":" & ZeroPad(Minute(Now), 2) & ":" & ZeroPad(Second(Now), 2) & "+03:00"
   createPagoHeader = "<?xml version=""1.0"" encoding=""ISO-8859-1""?>" & VbCrLf & _
        "<!DOCTYPE PAGOxml SYSTEM ""http://xml.pago.de/2.0/PAGOxml.dtd"">" & VbCrLf & _
        "<PAGOxml version=""2.0"" timeStamp=""" & sTimeStamp & """>" & VbCrLf & _
        " <header>" & VbCrLf & _
        "  <communicationId>" & sTimeStamp & "-0000000123-" & ZeroPad(CInt(Rnd() * 1000), 5) & "@62.128.39.4</communicationId>" & VbCrLf & _
        "  <Client>" & VbCrLf & _
        "   <ClientName>" & sAccountId & "</ClientName>" & VbCrLf & _
        "   <SalesChannel>" & sAccountPassword & "</SalesChannel>" & VbCrLf & _
        "   <Branch>" & sAccountSubId & "</Branch>" & VbCrLf & _
        "  </Client>" & VbCrLf & _
        "  <retry>false</retry>" & VbCrLf & _
        " </header>" & VbCrLf & _
        " <request>" & VbCrLf
End Function

Function SendPagoXmlRequest(postData)
    Dim HttpReq
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
    On Error Resume Next
		HttpReq.open "POST", "http://62.128.39.4:30000", false
		HttpReq.setRequestHeader "Content-Length", Len(postData)
		HttpReq.send postData
		sResDetails = HttpReq.responseText
	    HttpError = FormatHttpRequestError(HttpReq)
    On Error Goto 0
    If Len(sResDetails) = 0 Then Call throwError("521")
	Set xmlRet = HttpReq.responseXML
    If xmlRet.xml = "" Then
	    Set xmlRet = Server.CreateObject("Msxml2.DOMDocument.3.0")
        xmlRet.LoadXML(sResDetails)
    End If
    Set HttpReq = Nothing
    Set SendPagoXmlRequest = xmlRet
End Function

Function GetTransStatus(xTransID, xSequenceNumber)
    ParamList = createPagoHeader()
    ParamList = ParamList & "  <PspReturnValueRequest><CustomerReference>" & xTransID & "</CustomerReference></PspReturnValueRequest>" & VbCrLf
    ParamList = ParamList & " </request>" & VbCrLf & "</PAGOxml>"
    On Error Resume Next
        Set xmlRet = SendPagoXmlRequest(ParamList)
        GetTransStatus = TestNumVar(xmlRet.selectSingleNode("//ReferenceStatus/StatusCode").text, 0, -1, 0)
        xSequenceNumber = TestNumVar(xmlRet.selectSingleNode("//SequenceNumber").text, 0, -1, 1) + 1
    	'Response.Write(ParamList & " <br> " & xmlRet.xml & "<br>" & sReturnCode)
        'response.End
    On Error Goto 0
End Function

Function debitPago()
   Dim ParamList, xmlRet, sApprovalNumber, strAct, nSequenceNumber, IsoCurreny
   if Cint(X_Currency) <> 1 Then Call throwError("506")
   UrlAddress = "http://62.128.39.4:30000"
   xNames = Split(X_ccHolderName, " ")
   if Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
   if Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
   
   sCompanyDescriptor = sTermName & " " & sCompanyDescriptor

   Randomize
   IsoCurreny = Array("ILS", "USD", "EUR", "GBP", "AUD", "CAD", "JPY", "NOK")
   nSequenceNumber = 1 : nTranCur = IsoCurreny(CInt(X_Currency))
   ParamList = createPagoHeader()
   If Cint(X_TypeCredit) = 0 Then
      if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans Not Found
      xStatus = GetTransStatus(X_DebitReferenceCode, nSequenceNumber)
      ParamList = ParamList & _
        "  <PspRefundRequest>" & VbCrLf & _
        "   <CustomerReference>" & X_DebitReferenceCode & "</CustomerReference>" & VbCrLf & _
        "   <SequenceNumber>" & nSequenceNumber & "</SequenceNumber>" & VbCrLf & _
        "   <Amount Currency=""" & nTranCur & """>" & formatNumber(X_Amount, 2) & "</Amount>" & VbCrLf & _
        "   <NarrativeText>" & sCompanyDescriptor & "</NarrativeText>" & VbCrLf & _
        "  </PspRefundRequest>" & VbCrLf
   ElseIf Cint(X_TransType) = 2 Then
      if X_DebitReferenceCode = "" Then Call throwError("532") 'Authorize Not Found
      xStatus = GetTransStatus(X_DebitReferenceCode, nSequenceNumber)
      //X_DebitReferenceCode = GetTransRefCode()
      ParamList = ParamList & _
        "  <PspCaptureRequest>" & VbCrLf & _
        "   <CustomerReference>" & X_DebitReferenceCode & "</CustomerReference>" & VbCrLf & _
        "   <SequenceNumber>" & nSequenceNumber & "</SequenceNumber>" & VbCrLf & _
        "   <ReferenceId>" & X_ConfirmNumber & "</ReferenceId>" & VbCrLf & _
        "   <Amount Currency=""" & nTranCur & """>" & formatNumber(X_Amount, 2) & "</Amount>" & VbCrLf & _
        "   <NarrativeText>" & sCompanyDescriptor & "</NarrativeText>" & VbCrLf & _
        "  </PspCaptureRequest>" & VbCrLf
   Else
     Dim nReqName
     X_DebitReferenceCode = GetTransRefCode()
     If Cint(X_TransType) = 1 Then nReqName = "PspAuthorizeRequest" Else nReqName = "PspSalesRequest"
     'MDC : for maestro
     ParamList = ParamList & _
        "  <" & nReqName & ">" & VbCrLf & _
        "   <PaymentConnection>" & VbCrLf & _
        "    <PaymentMethod>KKE</PaymentMethod>" & VbCrLf & _
        "    <CreditCard>" & VbCrLf & _
        "     <CreditCardNumber>" & Replace(X_ccNumber, " ", "") & "</CreditCardNumber>" & VbCrLf & _
        "     <CreditCardHolder>" & X_ccHolderName & "</CreditCardHolder>" & VbCrLf & _
        "     <CreditCardExpirationDate>" & X_ccExpMM & "/" & X_ccExpYY & "</CreditCardExpirationDate>" & VbCrLf & _
        "     <CVV>" & X_ccCVV2 & "</CVV>" & VbCrLf & _
        "    </CreditCard>" & VbCrLf & _
        "   </PaymentConnection>" & VbCrLf & _
        "   <CustomerReference>" & X_DebitReferenceCode & "</CustomerReference>" & VbCrLf & _
        "   <SequenceNumber>" & nSequenceNumber & "</SequenceNumber>" & VbCrLf & _
        "   <Amount Currency=""" & nTranCur & """>" & formatNumber(X_Amount, 2) & "</Amount>" & VbCrLf
     If Cint(X_TransType) = 0 Then _
            ParamList = ParamList & "   <NarrativeText>" & sCompanyDescriptor & "</NarrativeText>" & VbCrLf
     ParamList = ParamList & _
        "   <CustomerData>" & VbCrLf & _
        "    <CustomerId>" & X_DebitReferenceCode & "</CustomerId>" & VbCrLf & _
        "    <Person>" & VbCrLf & _
        "     <FirstName>" & fName & "</FirstName>" & VbCrLf & _
        "     <LastName>" & sName & "</LastName>" & VbCrLf & _
        "     <CompanyOrPerson>P</CompanyOrPerson>" & VbCrLf & _
        "    </Person>" & VbCrLf & _
        "    <Address>" & VbCrLf & _
        "     <Street>" & BACHAddr1 & "</Street>" & VbCrLf & _
        "     <CountryCode>" & BACountryCode & "</CountryCode>" & VbCrLf & _
        "     <ZipCode>" & BAZipCode & "</ZipCode>" & VbCrLf & _
        "     <City>" & BACity & "</City>" & VbCrLf & _
        "    </Address>" & VbCrLf & _
        "   </CustomerData>" & VbCrLf & _
        "  </" & nReqName & ">" & VbCrLf
   End if 
   ParamList = ParamList & " </request>" & VbCrLf & "</PAGOxml>"
    On Error Resume Next
        Set xmlRet = SendPagoXmlRequest(ParamList)
        sResDetails = Left(xmlRet.xml, 750)
        sReturnCode = Trim(xmlRet.selectSingleNode("//StatusCode").text)
        sApprovalNumber = Trim(xmlRet.selectSingleNode("//ReferenceId").text)
        sErrorCode = Trim(xmlRet.selectSingleNode("//errorNumber").text)
    On Error GoTo 0
	'Response.Write(ParamList & " <br> " & xmlRet.xml & "<br>" & sReturnCode)
    'Response.End
    if sReturnCode = "0" Then
        sReturnCode = "000"
	Else
	    if sReturnCode = "" Then sReturnCode = sErrorCode
        'sReturnCode = xmlRet.selectSingleNode("//StatusText").text
    End If
   	Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
End Function
%>