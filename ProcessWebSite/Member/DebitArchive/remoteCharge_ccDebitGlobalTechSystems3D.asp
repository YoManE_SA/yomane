<%
'---------------------------------------------------------------------
'	Debit Global Tech Systems
'---------------------------------------------------------------------
Function GTS_Make2DigDate(xNum)
	If Len(xNum) = 1 Then xNum = "0" & xNum
	GTS_Make2DigDate = xNum
End Function

Function remoteCharge_debitGlobalTechSystems_BackDetect()
    If Request("PaRes") <> "" Then
        sRedirect = PROCESS_URL & "remote_charge.asp" & _
            "?CompanyNum=" & GetURLValue(xRequestParams, "companyNum") & _
            "&TransType=4" & _
            "&requestSource=18" & _
            "&ReplyURL=" & Server.URLEncode(GetURLValue(xRequestParams, "RetURL")) & _
            "&RefTransID=" & Request("MD") & _
            "&PaRes=" & Server.URLEncode(Request("PaRes"))
        Response.Redirect(sRedirect)
    Else
        Set xmlRet = Server.CreateObject("Msxml2.DOMDocument.3.0")
        xmlRet.loadXML(xRetParams)
		X_3dRedirect = xmlRet.selectSingleNode("//result/resulttext").text
        Set xmlRet = Nothing
	    Response.Redirect X_3dRedirect
    End If
End Function

Function debitGlobalTechSystems()
	If Trim(X_ccHolderName)<>"" Then X_ccHolderName = PV_ReplaceEscapeSequences(X_ccHolderName)
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer, requestAction', IsoCurreny
	xNames = Split(X_ccHolderName, " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	sOuterIP = IIF(session("Identity") = "Local", "http://80.179.39.10/member/", Server.URLEncode(Replace(session("ProcessURL"), "http://", "https://"))) & "remoteCharge_ccDebitGlobalTechSystemsNotify.asp"
	If Len(sCompanyDescriptor) > 25 Then sCompanyDescriptor = Left(sCompanyDescriptor, 25)
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
	    If CInt(X_TypeCredit) = 0 Then requestAction = "refund" Else requestAction = "capture"
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		ParamList = _
		"<epxml>" & vbCrLf & _
		" <header>" & vbCrLf & _
        "  <version>3</version>" & vbCrLf & _
        "  <gateway>" & IIF(sIsAllow3DTrans, "3D v3", "moto") & "</gateway>" & vbCrLf & _
        "  <responsetype>direct</responsetype>" & vbCrLf & _
        "  <mid>" & sAccountId & "</mid>" & vbCrLf & _
        "  <password>" & sAccountPassword & "</password>" & vbCrLf & _
        "  <type>" & requestAction & "</type>" & vbCrLf & _
        " </header>" & vbCrLf & _
        " <request>" & vbCrLf & _
        "  <capture>" & vbCrLf & _
        "   <etan>" & X_DebitReferenceCode & "</etan>" & vbCrLf & _
        "   <tan>" & X_ConfirmNumber & "</tan>" & vbCrLf & _
        "   <amount>" & vbCrLf & _
        "    <currency>" & GetCurISOName(Cint(X_Currency)) & "</currency>" & vbCrLf & _
        "    <value>" & CLng(X_Amount * 100) & "</value>" & vbCrLf & _
        "   </amount>" & vbCrLf & _
        "  </capture>" & vbCrLf & _
		" </request>" & vbCrLf & _
		"</epxml>"
	Else
    	If Cint(X_TransType) = 1 Then requestAction = "authorize" Else requestAction = "charge"
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = _
		"<epxml>" & vbCrLf & _
		" <header>" & vbCrLf & _
        "  <version>3</version>" & vbCrLf & _
        "  <gateway>" & IIF(sIsAllow3DTrans, "3D v3", "moto") & "</gateway>" & vbCrLf & _
        "  <responsetype>direct</responsetype>" & vbCrLf & _
        "  <mid>" & sAccountId & "</mid>" & vbCrLf & _
        "  <password>" & sAccountPassword & "</password>" & vbCrLf & _
        "  <type>" & requestAction & "</type>" & vbCrLf & _
        "  <recurring>Initial</recurring>" & vbCrLf & _
        "  <callbackurl>" & sOuterIP & "</callbackurl>" & vbCrLf & _
        "  <redirectok>" & sOuterIP & "</redirectok>" & vbCrLf & _
        "  <redirecterror>" & sOuterIP & "</redirecterror>" & vbCrLf & _
        " </header>" & vbCrLf & _
        " <request>" & vbCrLf & _
        "  <charge>" & vbCrLf & _
        "   <etan>" & X_DebitReferenceCode & "</etan>" & vbCrLf & _
        "   <card>" & vbCrLf & _
        "    <cctype>" & ccTypeEngName & "</cctype>" & vbCrLf & _
        "    <cc>" & Replace(X_ccNumber, " ", "") & "</cc>" & vbCrLf & _
        "    <expire>" & GTS_Make2DigDate(X_ccExpMM) & "/" & GTS_Make2DigDate(X_ccExpYY) & "</expire>" & vbCrLf & _
        "    <cvv>" & X_ccCVV2 & "</cvv>" & vbCrLf & _
        "   </card>" & vbCrLf & _
        "   <cardholder>" & vbCrLf & _
        "    <firstname>" & sName & "</firstname>" & vbCrLf & _
        "    <lastname>" & fName & "</lastname>" & vbCrLf & _
        "    <street>Anystreet 12</street>" & vbCrLf & _
        "    <zip>" & BAZipCode & "</zip>" & vbCrLf & _
        "    <city>" & BACity & "</city>" & vbCrLf & _
        "    <country>" & BACountryCode & "</country>" & vbCrLf & _
        "    <state>" & BAStateCode & "</state>" & vbCrLf & _
        "    <email>" & X_Email & "</email>" & vbCrLf & _
        "    <ip>" & sIP & "</ip>" & vbCrLf & _
        "   </cardholder>" & vbCrLf & _
        "   <amount>" & vbCrLf & _
        "    <currency>" & GetCurISOName(Cint(X_Currency)) & "</currency>" & vbCrLf & _
        "    <value>" & CLng(X_Amount * 100) & "</value>" & vbCrLf & _
        "   </amount>" & vbCrLf & _
        "  </charge>" & vbCrLf & _
        " </request>" & vbCrLf & _
		"</epxml>"
	End if

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	UrlAddress = " https://p-p-g.net/gateway.php"
	
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
		HttpReq.send "xml=" & ParamList
		sResDetails = HttpReq.responseText
		Response.Write(ParamList & "<br>" & sResDetails) : Response.End()
		HttpError = FormatHttpRequestError(HttpReq)
	
	    'Made by tamir to hide cvv and card number from log
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "<cvv>" & X_ccCVV2, "<cvv>" & GetSafePartialNumber(X_ccCVV2))
		sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

    	Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
		xmlRet.LoadXML HttpReq.responseText
		sReturnCode = Trim(xmlRet.selectSingleNode("//result/resultcode").text)
		sApprovalNumber = xmlRet.selectSingleNode("//result/tan").text
		sError = xmlRet.selectSingleNode("//result/resulttext").text
        Response.End
		'X_DebitReferenceCode = xmlRet.selectSingleNode("//TransactionId").text
	On Error Goto 0

	If Trim(sReturnCode) = "RDU" Then '3d secure redirect
	    X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
        sReturnCode = "553"
        'sError
	ElseIf Trim(sReturnCode) = "C00" Then
		sReturnCode = "000"
	ElseIf Trim(sReturnCode) = "001" Then 
		sReturnCode = "1" '001 is taken by netpay
	End if	

	Set xmlRet = Nothing
	Set HttpReq = Nothing

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>