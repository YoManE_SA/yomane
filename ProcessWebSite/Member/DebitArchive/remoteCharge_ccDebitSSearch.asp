<%
'---------------------------------------------------------------------
'	Debit SSearch
'---------------------------------------------------------------------
'const SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS = 13056
function getInputValue(xStrForm, vName)
    Dim xAnsStart, xAnsEnd
    xAnsStart = InStr(1, xStrForm, vName)
	if xAnsStart > 0 Then
	    xAnsStart = xAnsStart + Len(vName)
        xAnsEnd = InStr(xAnsStart, xStrForm, """")
	    if xAnsEnd < 1 then xAnsEnd = Len(xStrForm)
        getInputValue = Mid(xStrForm, xAnsStart, xAnsEnd - xAnsStart)
	End if
End Function

function debitSSearch()
   Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer

   xNames = Split(Trim(X_ccHolderName), " ")
   if Ubound(xNames) > -1 Then fName = xNames(0) Else Call throwError("517")
   if Ubound(xNames) > 0 Then sName = xNames(1) Else Call throwError("517")
   
    sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
    if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
  Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
  'HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
  UrlAddress = "http://paylet.billingpronto.com/core.php"
  if (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
      if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
  End if
  if Left(sIP, 8) = "192.168." Then sIP = Request.ServerVariables("LOCAL_ADDR")
  If Cint(X_TransType) = 1 Then Call throwError("503")
  ParamList = "login=" & sAccountId & _
        "&pass=" & sAccountPassword & _
  		"&pan=" & replace(X_ccNumber, " ", "") & _
  		"&mes_cad=" & X_ccExpMM & "&an_cad=20" & X_ccExpYY & _
		"&cvv2=" & X_ccCVV2 & _
		"&importe=" & formatNumber(X_Amount, 2) & _
		"&firstname=" & fName & _
		"&lastname=" & sName & _
		"&street=" & BACHAddr1 & _
		"&zip=" & BAZipCode & _
		"&city=" & BACity & _
		"&country=" & BACountryCode & _
		"&state=" & BAStateCode & _
        "&phone=" & X_PhoneNumber & _
   		"&email=" & X_Email & _
   		"&customer_ip=" & sIP & _
   		"&auto=2" & _
        "&USERDATA_merchantID=NETP-3001"
   On Error Resume Next

	HttpReq.open "POST", UrlAddress, false
    HttpReq.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
	HttpReq.send ParamList
	
	TxtRet = HttpReq.responseText
    If Len(TxtRet) = 0 Then Call throwError("521")
    
	sDebitReturnAnswer = Right(TxtRet, 400)
    sApprovalNumber = getInputValue(TxtRet, "id_operacion value=""")
   	X_DebitReferenceCode = sApprovalNumber
	if InStr(1, TxtRet, "http://netpay.com/OK") > 0 Then
        sReturnCode	= "000"
	Else
	    sError = getInputValue(TxtRet, "codigo_error value=""")
	    'Response.Write(sError & "<br>")
    	sReturnCode = Response.Write(Left(Right(sError, 4), 3))
	    if sReturnCode = "" Or Not IsNumeric(sReturnCode) Then sReturnCode = "590"
	End if
	
	'Response.Write(ParamList & "<br>" & sReturnCode & "<br>" & Replace(TxtRet, "script", "Ascript"))
	'Response.End

    Set xmlRet = Nothing
    Set HttpReq = Nothing
    On Error GoTo 0 
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End function
%>