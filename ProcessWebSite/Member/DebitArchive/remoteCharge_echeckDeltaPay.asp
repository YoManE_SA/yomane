<%
'---------------------------------------------------------------------
'	Debit AllCharge
'---------------------------------------------------------------------

Function debit_DeltaPayParseRes(result, itemName)
	Dim start, endPos
	start = InStr(1, result, itemName & "=>")
	If start <= 0 Then Exit Function
	start = start + Len(itemName) + 2
	endPos = InStr(start, result, "<br")
	If endPos <= 0 Then endPos = Len(result)
	debit_DeltaPayParseRes = Mid(result, start, endPos - start)
End Function

Function debit_DeltaPay()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber
	xNames = Split(X_ccHolderName, " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
    If isTestTerminal Then UrlAddress = "https://fastecom.com/gateways/FAST/processFAST.php" _
    Else UrlAddress = "https://fastecom.com/gateways/FAST/processFAST.php"
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		ParamList = _
            "affiliate=" & Server.URLEncode(sAccountId) & _
            "&paymethod=" & Server.URLEncode("Credit Card") & _
            "&post_method=" & Server.URLEncode("sync") & _
			"&processing_mode=" & IIF(Cint(X_TypeCredit) = 0, "Refund", "settlement") & _
			"&notification_url=" & Server.URLEncode(Session("ProcessURL") & "remoteCharge_ccDebitDeltaPayHPPNotify.asp") & _
            "&order_id=" & Server.URLEncode(GetTransRefCode()) & _
			"&customer_id=" & Server.URLEncode(X_ConfirmNumber) & _
            "&amount=" & X_Amount & _
            "&currency=" & GetCurISOName(Cint(X_Currency)) & _
            "&reference_transaction_no=" & Server.URLEncode(X_DebitReferenceCode)
	Else
		X_DebitReferenceCode = GetTransRefCode()
		Dim sendCardType : sendCardType = 0
		ParamList = _
            "affiliate=" & Server.URLEncode(sAccountId) & _
            "&paymethod=" & Server.URLEncode("Credit Card") & _
            "&post_method=" & Server.URLEncode("sync") & _
			"&processing_mode=" & Server.URLEncode(IIF(Cint(X_TransType) = 1, "authorize", "sale")) & _
			"&redirect=" & Server.URLEncode("RemoteCharge_GenericDataWriter.asp") & _
			"&location=AFF" & _
            "&order_id=" & Server.URLEncode(X_DebitReferenceCode) & _
			"&room_name=" & Server.URLEncode(sAccountSubId) & _
			"&agent_name=" & Server.URLEncode(sAccountPassword) & _
            "&first_name=" & Server.URLEncode(Left(fName, 20)) & _
            "&last_name=" & Server.URLEncode(Left(sName, 20)) & _
            "&address1=" & Server.URLEncode(BACHAddr1) & _
            "&address2=" & Server.URLEncode(BACHAddr2) & _
            "&city=" & Left(Server.URLEncode(BACity), 30) & _
            "&state=" & Server.URLEncode(BAStateCode) & _
            "&country=" & Server.URLEncode(BACountryCode) & _
            "&zip=" & Server.URLEncode(BAZipCode) & _
            "&telephone=" & Server.URLEncode(X_PhoneNumber) & _
            "&amount=" & X_Amount & _
            "&currency=" & GetCurISOName(Cint(X_Currency)) & _
            "&email=" & Server.URLEncode(X_Email) & _
            "&account_number=" & Server.URLEncode(ckacct) & _
            "&check_number=" & Server.URLEncode(X_DebitReferenceCode) & _
            "&routing_number=" & Server.URLEncode(ckaba) & _
            "&customer_ip=" & Server.URLEncode(sIP)
			'"&Desc=" & Server.URLEncode(sCompanyDescriptor)
            '"&CCReceipt=" & Server.URLEncode(X_ConfirmNumber)
	End if
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		
		'approval_no, gateway_descriptor
		sReturnCode = debit_DeltaPayParseRes(sResDetails, "status")
		X_DebitReferenceCode = debit_DeltaPayParseRes(sResDetails, "transaction_no")
		sApprovalNumber = debit_DeltaPayParseRes(sResDetails, "customer_id")
		X_CustomErrorDesc = Replace(debit_DeltaPayParseRes(sResDetails, "reason"), "'", "")

		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "cvv=" & X_ccCVV2, "cvv=" & GetSafePartialNumber(X_ccCVV2))
		sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

		'Response.Write(ParamList & "</br>" & vbCrLf)
		'Response.Write(sResDetails & "</br>" & vbCrLf)
		'Response.Write(sReturnCode & "," & X_DebitReferenceCode & "," & sApprovalNumber & "," & X_CustomErrorDesc & "</br>")
		'Response.End

      'transactionID
		TimeControl.Append("A_SEND") 'Time Optimization *********
	On Error Goto 0
	'Response.Write(ParamList & "<br>" & sResDetails) : Response.End()
	If Trim(sReturnCode) = "APPROVED" Or Trim(sReturnCode) = "AUTHORIZED" Or Trim(sReturnCode) = "CAPTURED" Then
		sReturnCode = "000"
	ElseIf Trim(sReturnCode) = "REFUND REQUEST" Then 
		sReturnCode = "001"
	Else
    	If Trim(sReturnCode) = "001" Then sReturnCode = "002" '001 is taken by netpay
		'If Not IsNummeric(sReturnCode) Then sReturnCode = "002"
   End If
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>