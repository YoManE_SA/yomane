<%
'---------------------------------------------------------------------
'	Debit Inatec ()
'---------------------------------------------------------------------
function debitInatec()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, EncodedSignature, sApprovalNumber, strAct, IsoCurreny
	IsoCurreny = Array("ILS", "USD", "EUR", "GBP", "AUD", "CAD", "JPY", "NOK")
	If IsoCurreny(Cint(X_Currency)) = "0" Then Call throwError("506")
	 
	xNames = Split(X_ccHolderName, " ")
	if Ubound(xNames) > -1 Then fName = xNames(0)
	if Ubound(xNames) > 0 Then sName = xNames(1)
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	If Not ((Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0)) Then X_DebitReferenceCode = GetTransRefCode()
	UrlAddress = "https://pay4.powercash21.com/powercash21-3-2/backoffice/"
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If (Cint(X_TypeCredit) = 0) Then	
			Hash1 = IsoCurreny(CInt(X_Currency)) & "en" & sAccountId & FormatNumber(X_Amount, 2, True, False, False) & X_ConfirmNumber & sAccountPassword
		Else
			Hash1 = "en" & sAccountId & X_ConfirmNumber & sAccountPassword
		End If
		'Response.Write(Hash1 & "<br>" & vbCrLf)
        Hash1 = LCase(Hash("SHA1", Hash1).Hex)
		If X_ConfirmNumber = "" Then Call throwError("532") 'Reftrans Not Found
		UrlAddress = UrlAddress & IIF(Cint(X_TypeCredit) = 0, "payment_refund", "payment_capture")
		ParamList = _
			"merchantid=" & Server.URLEncode(sAccountId) & _
			"&transactionid=" & X_ConfirmNumber & _
			"&language=en"
		If (Cint(X_TypeCredit) = 0) Then
			ParamList = ParamList & _
				"&price=" & FormatNumber(X_Amount, 2, True, False, False) & _
				"&currency=" & IsoCurreny(Cint(X_Currency))
		End If
		ParamList = ParamList & "&signature=" & Hash1
	Else
	   UrlAddress = UrlAddress & IIF(Cint(X_TransType) = 1, "payment_preauthorize", "payment_authorize")
	   CountryIso3Code = ExecScalar("SELECT ISOCode3 FROM [List].[CountryList] WHERE CountryID = " & BACountryId, "")
	   Hash1 = X_ccHolderName & Replace(X_ccNumber, " ", "") & BACity & CountryIso3Code & _
			   IsoCurreny(Cint(X_Currency)) & sIP & X_ccCVV2 & X_Email & X_ccExpMM & "20" & X_ccExpYY & _
			   fName & "en" & sName & sAccountId & X_DebitReferenceCode & "1" & X_PhoneNumber & _
			   FormatNumber(X_Amount, 2, True, False, False) & BAStateCode & BAZipCode & sAccountPassword
	   'Response.Write(Hash1 & "<br>" & vbCrLf)
       Hash1 = LCase(Hash("SHA1", Hash1).Hex)
	   ParamList = _
			"merchantid=" & Server.URLEncode(sAccountId) & _
			"&orderid=" & X_DebitReferenceCode & _
			"&price=" & FormatNumber(X_Amount, 2, True, False, False) & _
			"&currency=" & IsoCurreny(Cint(X_Currency)) & _
			"&payment_method=1" & _
			"&language=en" & _
			"&ccn=" & Replace(X_ccNumber, " ", "") & _
			"&exp_month=" & X_ccExpMM & _
			"&exp_year=20" & X_ccExpYY & _
			"&cvc_code=" & X_ccCVV2 & _
			"&cardholder_name=" & Server.URLEncode(X_ccHolderName) & _
			"&email=" & Server.URLEncode(X_Email) & _
			"&firstname=" & Server.URLEncode(fName) & _
			"&lastname=" & Server.URLEncode(sName) & _
			"&customerip=" & Server.URLEncode(sIP) & _
			"&country=" & CountryIso3Code & _
			"&state=" & BAStateCode & _
			"&phone=" & Server.URLEncode(X_PhoneNumber) & _
			"&city=" & Server.URLEncode(BACity) & _
			"&zip=" & Server.URLEncode(BAZipCode) & _
			"&signature=" & Hash1
	End If

	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
	    HttpError = FormatHttpRequestError(HttpReq)
	On Error GoTo 0
	If IsNull(sResDetails) Or Len(sResDetails) = 0 Then Call throwError("521")
	sReturnCode = GetURLValue(sResDetails, "status")
	If sReturnCode = "" Then Call throwError("520") 'internal error
	
	If sReturnCode = "0" Then
		sReturnCode = "000"
		sApprovalNumber = GetURLValue(sResDetails, "transactionid")
	Else
		sError = GetURLValue(sResDetails, "errormessage")
	End If
	'Response.Write(UrlAddress & vbcrlf & ParamList & vbcrlf & sResDetails & vbcrlf & "<br>" & sReturnCode & "<Br>" & sApprovalNumber)
	'Response.Flush
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
end function
%>