<%@ Page Language="VB" %>
<script runat="server">
	Sub ReturnReply(ByVal nReply As Integer, Optional ByVal sReply As String = Nothing, Optional ByVal sTransType As String = Nothing)
		Response.Write("Reply=" & nReply.ToString("00"))
		If Not String.IsNullOrEmpty(sReply) Then Response.Write("&ReplyDesc=" & sReply)
		If Not String.IsNullOrEmpty(sTransType) Then Response.Write("&TransType=" & sTransType)
		Response.End()
	End Sub

	' 10 = Not enough data or invalid data
	' 11 = Merchant does not exist
	' 12 = Transaction number does not exist or is not associated with merchant 

	Sub Page_Load()
		Dim sMerchantNumber As String = Request("CompanyNum")
		If Convert.IsDBNull(sMerchantNumber) Or String.IsNullOrEmpty(sMerchantNumber) Or sMerchantNumber = Nothing Then ReturnReply(10, "Missing CompanyNum")
		If sMerchantNumber.Length <> 7 Then ReturnReply(10, "Invalid CompanyNum")
		If dbPages.TestVar(sMerchantNumber, 1, 0, 0) = 0 Then ReturnReply(10, "Invalid CompanyNum")

		If Convert.IsDBNull(sMerchantNumber) Or String.IsNullOrEmpty(Request("TransID")) Then ReturnReply(10, "Missing TransID")
		Dim nTrans As Integer = dbPages.TestVar(Request("TransID"), 1, 0, 0)
		If nTrans = 0 Then ReturnReply(10, "Invalid TransID")

		Dim sSQL As String = "SELECT dbo.GetVerifyTransaction('" & sMerchantNumber & "', " & nTrans & ")"
		Dim nRC As Integer = dbPages.ExecScalar(sSQL)
		Select Case nRC
			Case -11 : ReturnReply(11, "Merchant not found")
			Case -12 : ReturnReply(12, "Transaction not found for merchant")
			Case 3 : ReturnReply(0, "Transaction found", "Pre-Auth")
			Case Else : If nRC < 0 Then ReturnReply(-nRC, "Invalid data") Else ReturnReply(0, "Transaction found")
		End Select
	End Sub
</script>