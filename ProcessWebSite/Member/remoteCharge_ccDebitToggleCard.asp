<%
'---------------------------------------------------------------------
'	Debit ToggleCard
'---------------------------------------------------------------------
'const SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS = 13056
function debitToggleCard()
   Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer
   xNames = Split(X_ccHolderName, " ")
   if Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
   if Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
   
  sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
  if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
  Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
  'HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
  SendAmnt = X_Amount
  UrlAddress = sAccountSubId & "/Website/remoteCharge.aspx?"
  if (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
      if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
      sCompanyDescriptor = "Refund: " & sCompanyDescriptor
      SendAmnt = -X_Amount
  Else
	  X_DebitReferenceCode = GetTransRefCode()
  End if
  If Cint(X_TransType) = 1 Then mSDep = 0 Else mSDep = 1
  ParamList = "Account=" & sAccountId & _
  		"&CardNumber=" & replace(X_ccNumber, " ", "") & _
  		"&ExpMM=" &  X_ccExpMM & "&ExpYY=20" & X_ccExpYY & _
		"&Amount=" & (formatNumber(SendAmnt, 2) * 100) & _
		"&TransType=" & mSDep & _
		"&CVV=" & X_ccCVV2 & _
   		"&Currency=" & X_Currency & _
		"&MerchantID=" & X_CompanyNum & _
		"&Email=" & Server.URLEncode(X_Email) & _
		"&Phone=" & Server.URLEncode(X_PhoneNumber) & _
		"&FullName=" & Server.URLEncode(X_ccHolderName) & _
		"&Order=" & X_DebitReferenceCode & _
		"&Descriptor=" & Server.UrlEncode(sCompanyDescriptor)
    On Error Resume Next

	'Response.Write(UrlAddress & ParamList & "<br>")
	'Response.End
 
	HttpReq.open "POST", UrlAddress & ParamList, false
	HttpReq.send
    If Len(HttpReq.responseText) = 0 Then Call throwError("521")
	TxtRet = HttpReq.responseText
	sDebitReturnAnswer = Left(TxtRet, 200)
    sReturnCode = Left(TxtRet, 3)
	sApprovalNumber = Mid(TxtRet, 5, 7)
	sError = Right(TxtRet, Len(TxtRet) - 12)
	If trim(sReturnCode) = "001" then sReturnCode = "002" '001 is taken by netpay

    Set xmlRet = Nothing
    Set HttpReq = Nothing

	'Response.Write(sReturnCode & " - " & sError & " - " & sApprovalNumber)
	'Response.Flush
    On Error GoTo 0 
    
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End function
%>