<%
'---------------------------------------------------------------------
'	Debit AllCharge
'---------------------------------------------------------------------

Function debit_DeltaPayParseRes(result, itemName)
	If InStr(1, result, "=>") <= 0 Then 
		debit_DeltaPayParseRes = GetURLValue(result, itemName)
		Exit Function
	End If
	Dim start, endPos
	start = InStr(1, result, itemName & "=>")
	If start <= 0 Then Exit Function
	start = start + Len(itemName) + 2
	endPos = InStr(start, result, "<br")
	If endPos <= 0 Then endPos = Len(result)
	debit_DeltaPayParseRes = Mid(result, start, endPos - start)
End Function

Function debit_DeltaPay()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber
	xNames = Split(X_ccHolderName, " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = ""
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = ""
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
    If isTestTerminal Then UrlAddress = "https://fastecom.com/gateways/FAST/processFAST.php" _
    Else UrlAddress = "https://fastecom.com/gateways/FAST/processFAST.php"
	Dim sendCardType : sendCardType = 0
	Select case PaymentMethod
	Case PMD_CC_AMEX: sendCardType = "Amex"
	Case PMD_CC_VISA: sendCardType = "Visa"
	case PMD_CC_MASTERCARD: sendCardType = "MasterCard"
	case PMD_CC_CUP, PMD_EC_CUP: sendCardType = "CUP"
	'Case PMD_CC_DISCOVER: sendCardType = "Discover"
	Case Else: throwError("503")
	End Select
	sNotificationUrl = IIF(session("Identity") = "Local", "http://80.179.39.10:8080/Default.aspx", Session("ProcessURL") & "remoteCharge_ccDebitDeltaPayHPPNotify.asp")
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_DebitReferenceNum = "" Then Call throwError("532") 'Reftrans not Found
		If Cint(X_TypeCredit) = 0 Then 'no more then one refund per transaction
			Dim refCount : refCount = 0
			refCount = refCount + TestNumVar(ExecScalar("Select Count(ID) From tblCompanyTransPass Where CreditType=0 And DeniedStatus <> 5 And OriginalTransID=" & X_RefTransID, 0), 0, -1, 0)
			refCount = refCount + TestNumVar(ExecScalar("Select Count(ID) From tblCompanyTransPending Where trans_creditType=0 And OriginalTransID=" & X_RefTransID, 0), 0, -1, 0)
			If refCount > 1 Then Call throwError("564") 'Reftrans not Found
		End If
        If X_DebitReferenceNum <> "" And InStr(1, X_DebitReferenceNum, " ") = 0 Then
            Dim curChar, lIdx : lIdx = 1
            Do While lIdx < Len(X_DebitReferenceNum)
                curChar = Mid(X_DebitReferenceNum, lIdx, 1)
                If Asc(curChar) >= Asc("0") And Asc(curChar) <= Asc("9") Then 
                    lIdx = lIdx- 1
                    Exit Do
                End if	
                lIdx = lIdx + 1
            Loop
            If lIdx > 0 And lIdx < Len(X_DebitReferenceNum) Then _
                X_DebitReferenceNum = Left(X_DebitReferenceNum, lIdx) & " " & Right(X_DebitReferenceNum, Len(X_DebitReferenceNum) - lIdx) 
        End If  
		ParamList = _
            "affiliate=" & Server.URLEncode(sAccountId) & _
            "&paymethod=" & Server.URLEncode(sendCardType) & _
            "&post_method=" & Server.URLEncode("sync") & _
			"&processing_mode=" & IIF(Cint(X_TypeCredit) = 0, "Refund", "settlement") & _
			"&room_name=" & Server.URLEncode(sAccountSubId) & _
			"&redirect=" & Server.URLEncode(sNotificationUrl & "?IsNotify=0") & _
			"&notification_url=" & Server.URLEncode(sNotificationUrl & "?IsNotify=1") & _
            "&order_id=" & Server.URLEncode(GetTransRefCode()) & _
            "&amount=" & X_Amount & _
            "&currency=" & GetCurISOName(Cint(X_Currency)) & _
            "&reference_transaction_no=" & Server.URLEncode(X_DebitReferenceNum)
			'"&customer_id=" & Server.URLEncode(X_DebitReferenceCode)
	Else
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = _
            "affiliate=" & Server.URLEncode(sAccountId) & _
            "&paymethod=" & Server.URLEncode("Credit Card") & _
            "&post_method=" & Server.URLEncode("sync") & _
			"&processing_mode=" & Server.URLEncode(IIF(Cint(X_TransType) = 1, "authorize", "sale")) & _
			"&redirect=" & Server.URLEncode(sNotificationUrl & "?IsNotify=0") & _
			"&notification_url=" & Server.URLEncode(sNotificationUrl & "?IsNotify=1") & _
			"&location=AFF" & _
            "&order_id=" & Server.URLEncode(X_DebitReferenceCode) & _
			"&room_name=" & Server.URLEncode(sAccountSubId) & _
			"&agent_name=" & Server.URLEncode(sAccountPassword) & _
            "&first_name=" & Server.URLEncode(Left(fName, 20)) & _
            "&last_name=" & Server.URLEncode(Left(sName, 20)) & _
            "&address1=" & Server.URLEncode(BACHAddr1) & _
            "&address2=" & Server.URLEncode(BACHAddr2) & _
            "&city=" & Left(Server.URLEncode(BACity), 30) & _
            "&state=" & Server.URLEncode(IIF(BAStateCode <> "", BAStateCode, "NA")) & _
            "&country=" & Server.URLEncode(BACountryCode) & _
            "&zip=" & Server.URLEncode(IIF(BAZipCode <> "", BAZipCode, "NA")) & _
            "&telephone=" & Server.URLEncode(X_PhoneNumber) & _
            "&amount=" & X_Amount & _
            "&currency=" & GetCurISOName(Cint(X_Currency)) & _
            "&email=" & Server.URLEncode(X_Email) & _
            "&card_type=" & Server.URLEncode(sendCardType) & _
            "&card_number=" & Replace(X_ccNumber, " ", "") & _
            "&cvv=" & Server.URLEncode(X_ccCVV2) & _
            "&expiry_mo=" & X_ccExpMM & _
            "&expiry_yr=20" & X_ccExpYY & _
            "&customer_ip=" & Server.URLEncode(sIP)
			'"&Desc=" & Server.URLEncode(sCompanyDescriptor)
            '"&CCReceipt=" & Server.URLEncode(X_ConfirmNumber)
	End if
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		
		'approval_no, gateway_descriptor
        sReturnCode = debit_DeltaPayParseRes(sResDetails, "status")
        sApprovalNumber = debit_DeltaPayParseRes(sResDetails, "approval_no")
        X_DebitReferenceNum = debit_DeltaPayParseRes(sResDetails, "transaction_no")
        X_CustomErrorDesc = Replace(debit_DeltaPayParseRes(sResDetails, "reason"), "'", "")

		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, "card_number=" & Replace(X_ccNumber, " ", ""), "card_number=" & GetSafePartialNumber(Replace(X_ccNumber, " ", "")))
		sDebitRequest = Replace(sDebitRequest, "cvv=" & X_ccCVV2, "cvv=" & GetSafePartialNumber(X_ccCVV2))
		sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

		'Response.Write(ParamList & "</br>" & vbCrLf)
		'Response.Write(sResDetails & "</br>" & vbCrLf)
		'Response.Write(sReturnCode & "," & X_DebitReferenceCode & "," & sApprovalNumber & "," & X_CustomErrorDesc & "</br>")
		'Response.End

        'transactionID
		TimeControl.Append("A_SEND") 'Time Optimization *********
	On Error Goto 0
	'Response.Write(ParamList & "<br>" & sResDetails) : Response.End()
	If Trim(sReturnCode) = "APPROVED" Or Trim(sReturnCode) = "AUTHORIZED" Or Trim(sReturnCode) = "CAPTURED" Then
		sReturnCode = "000"
	ElseIf Trim(sReturnCode) = "REFUND REQUEST" Then 
		sReturnCode = "001"
	Else
    	If Trim(sReturnCode) = "001" Then sReturnCode = "002" '001 is taken by netpay
        If Not IsNumeric(sReturnCode) Then sReturnCode = "002"
		'If Not IsNummeric(sReturnCode) Then sReturnCode = "002"
    End If
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>