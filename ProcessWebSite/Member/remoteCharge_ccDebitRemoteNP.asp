<%
'---------------------------------------------------------------------
'	Debit using remote netpay system
'---------------------------------------------------------------------
Function remoteCharge_cc_RemoteNP_BackDetect()
    X_3dRedirect = GetURLValue(xRetParams, "D3Redirect")
    Response.Redirect(UrlDecode(X_3dRedirect))
End Function

Function debit_RemoteNP()
	Dim HttpReq, UrlAddress, ParamList, TxtRet, backPublicAddress, sApprovalNumber, EncodedSignature
	If int(X_TypeCredit) = 8 Or int(X_Payments) > 1 Then Call throwError("508") 
	
    UrlAddress = sAccountSubId
    If UrlAddress = "" Then UrlAddress = IIF(isTestTerminal, "http://localhost:83/processWebSite", "https://process.netpay-intl.com")
    backPublicAddress = IIF(isTestTerminal, "http://localhost:83/processWebSite", Session("ProcessURL"))

    If (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		If X_DebitReferenceNum = "" Then Call throwError("532") 'Reftrans not Found
    Else 
        X_DebitReferenceNum = GetTransRefCode()
	End If
    ParamList = _
        "CompanyNum=" & Server.URLEncode(sAccountId) & _
        "&TransType=" & Server.URLEncode(X_TransType) & _
        "&CardNum=" & Server.URLEncode(Replace(X_ccNumber, " ", "")) & _
        "&ExpMonth=" & Server.URLEncode(X_ccExpMM) & _
        "&ExpYear=" & Server.URLEncode(X_ccExpYY) & _
        "&Track2=" & Server.URLEncode(X_TrackII) & _
        "&Member=" & Server.URLEncode(X_ccHolderName) & _
        "&TypeCredit=" & Server.URLEncode(X_TypeCredit) & _
        "&Payments=" & Server.URLEncode(X_Payments) & _
        "&Amount=" & Server.URLEncode(X_Amount) & _
        "&Currency=" & Server.URLEncode(X_Currency) & _
        "&CVV2=" & Server.URLEncode(X_ccCVV2) & _
        "&Email=" & Server.URLEncode(X_Email) & _
        "&PersonalNum=" & Server.URLEncode(X_PersonalNumber) & _
        "&DateOfBirth=" & Server.URLEncode(X_DateOfBirth) & _
        "&PhoneNumber=" & Server.URLEncode(X_PhoneNumber) & _
        "&ClientIP=" & Server.URLEncode(sIP) & _
        "&BillingAddress1=" & Server.URLEncode(BACHAddr1) & _
        "&BillingAddress2=" & Server.URLEncode(BACHAddr2) & _
        "&BillingCity=" & Server.URLEncode(BACity) & _
        "&BillingZipCode=" & Server.URLEncode(BAZipCode) & _
        "&BillingState=" & Server.URLEncode(BAStateCode) & _
        "&BillingCountry=" & Server.URLEncode(BACountryCode) & _
        "&Order=" & Server.URLEncode(X_DebitReferenceNum) & _
        "&Comment=" & Server.URLEncode(X_Comment) & _
        "&ConfirmationNum=" & Server.URLEncode(X_ConfirmNumber) & _
        "&RetURL=" & Server.URLEncode(backPublicAddress & "/member/remoteCharge_ccDebitRemoteNPBack.asp")

    If (Cint(X_TransType) = 2) Then ParamList = ParamList & "&TransApprovalID=" & Server.URLEncode(X_DebitReferenceCode)
    If (Cint(X_TypeCredit) = 0) Then ParamList = ParamList & "&RefTransID=" & Server.URLEncode(X_DebitReferenceCode)

    EncodedSignature = sAccountId & X_TransType & X_TypeCredit & X_Amount & X_Currency & Replace(X_ccNumber, " ", "") & IIF(Cint(X_TypeCredit) = 0, X_DebitReferenceCode, "")
    EncodedSignature = Hash("SHA256", EncodedSignature & sAccountPassword).Base64
    ParamList = ParamList & "&Signature=" & Server.URLEncode(EncodedSignature)
	'On Error Resume Next
	    Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	    HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
        'Response.Write(UrlAddress & "/member/remote_charge.asp?" & ParamList & vbCrLf & "<br/>")
		HttpReq.Open "POST", UrlAddress & "/member/remote_charge.asp", false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded;charset=windows-1255"
		HttpReq.Send(ParamList)
		TxtRet = HttpReq.responseText

		ParamList = Replace(ParamList, "CardNum=" & X_ccNumber, "CardNum=" & GetSafePartialNumber(X_ccNumber))
		ParamList = Replace(ParamList, "CVV2=" & X_ccCVV2, "CVV2=" & GetSafePartialNumber(X_ccCVV2))
		TxtRet = Replace(TxtRet, X_ccNumber, GetSafePartialNumber(X_ccNumber))

		SaveLogChargeAttemptRequestResponse ParamList, TxtRet
		HttpError = FormatHttpRequestError(HttpReq)
        Set HttpReq = Nothing
	On Error GoTo 0
	If IsNull(TxtRet) Or Len(TxtRet) = 0 Then Call throwError("521")
	sReturnCode = GetURLValue(TxtRet, "Reply")
	sApprovalNumber = GetURLValue(TxtRet, "ConfirmationNum")
    X_DebitReferenceCode = GetURLValue(TxtRet, "TransID")
	X_CustomErrorDesc = URLDecode(GetURLValue(TxtRet, "ReplyDesc"))
	If sReturnCode = "" Then Call throwError("520") 'internal error
    If sReturnCode = "553" Then
        X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
        sIsSendUserConfirmationEmail = False
    End If
	DebitIProcessResult sReturnCode, sApprovalNumber
End function
%>