<%
'---------------------------------------------------------------------
'	Debit debitB_ACS New
'---------------------------------------------------------------------
Function AT_Make2DigDate(xNum)
	If Len(xNum) = 1 Then xNum = "0" & xNum
	AT_Make2DigDate = xNum
End Function

Function remoteCharge_cc_ACS_BackDetect()
    Dim sendParams, jsonResult
	xRetParams = Replace(Replace(Replace(xRetParams, "&gt;", ">"), "&lt;", "<"), "``", """")
    Set jsonResult = JSON.parse(xRetParams)
    X_3dRedirect = jsonResult.redirect.url
	Response.Write("<html><body onload=""document.getElementById('frmMain').submit()""><form id=""frmMain"" method=""post"" action=""" & X_3dRedirect & """>" & vbCrLf)
    Set sendParams = jsonResult.redirect.parameters
    For Each p in sendParams
		Response.Write(" <input type=""hidden"" name=""" & p.name & """ value=""" & p.value & """/>" & vbCrLf)
	Next	
	Response.Write("</form></body></html>")
    Set X_3dRedirect = Nothing
    Response.End
End Function

Function debitACS(tranReference)
	Dim HttpReq, UrlAddress, ParamList, sApprovalNumber, sDebitReturnAnswer, fdAction, sTransaction_Approved, sACSHttpMethod
    Dim fName, sName, xNames, backPublicAddress, hasRedirectUrl
    sACSHttpMethod = "POST" : hasRedirectUrl = False
    backPublicAddress = IIF(isTestTerminal, "http://process.yomane.net/member/", Session("ProcessURL"))
    If isTestTerminal Then UrlAddress = "https://test.oppwa.com/v1/payments" _ 
	Else UrlAddress = "https://oppwa.com/v1/payments"
    BrandName = Array("--", "--", "VISA", "DINERS", "AMEX", "MASTER")

    xNames = Split(X_ccHolderName, " ")
    if Ubound(xNames) > -1 Then fName = xNames(0)
    if Ubound(xNames) > 0 Then sName = xNames(1)

	'sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	If Len(sCompanyDescriptor) > 24 Then sCompanyDescriptor = Left(sCompanyDescriptor, 24)
	If (Cint(X_TransType) = 4) Then
		ParamList = _
            "authentication.userId=" & sAccountId & _
		    "&authentication.password=" & sAccountPassword & _
		    "&authentication.entityId=" & sAccountSubId
        sACSHttpMethod = "GET"
        UrlAddress = UrlAddress & "/" & X_DebitReferenceCode & "?" & ParamList : ParamList = ""
	ElseIf (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_DebitReferenceNum = "" Then Call throwError("532") 'Reftrans not Found
		fdAction = IIF(Cint(X_TransType) = 2, "CP", "RF") '32:tagged Pre-Authorization Completion, 34:Refund
		ParamList = _
            "authentication.userId=" & sAccountId & _
		    "&authentication.password=" & sAccountPassword & _
		    "&authentication.entityId=" & sAccountSubId & _
		    "&paymentType=" & fdAction & _
		    "&amount=" & FormatNumber(X_Amount, 2, True, False, False) & _
		    "&currency=" & Server.URLEncode(GetCurISOName(Cint(X_Currency)))
        UrlAddress = UrlAddress & "/" & X_DebitReferenceCode
	Else
	    X_DebitReferenceCode = GetTransRefCode()
		fdAction = IIF(Cint(X_TransType) = 1, "PA", "DB") 
		ParamList = _
            "authentication.userId=" & sAccountId & _
		    "&authentication.password=" & sAccountPassword & _
		    "&authentication.entityId=" & sAccountSubId & _
		    "&amount=" & FormatNumber(X_Amount, 2, True, False, False) & _
		    "&currency=" & Server.URLEncode(GetCurISOName(Cint(X_Currency))) & _
            "&paymentBrand=" & Server.URLEncode(BrandName(ccTypeID)) & _
		    "&paymentType=" & fdAction & _
		    "&card.number=" & Replace(X_ccNumber, " ", "") & _
		    "&card.holder=" & Server.URLEncode(X_ccHolderName) & _
		    "&card.expiryMonth=" & AT_Make2DigDate(X_ccExpMM) & _
		    "&card.expiryYear=20" & AT_Make2DigDate(X_ccExpYY) & _
		    "&card.cvv=" & X_ccCVV2 & _
            "&merchantTransactionId=" & Server.URLEncode(X_DebitReferenceCode) & _
			"&customer.givenName=" & Server.URLEncode(fName) & _
			"&customer.surname=" & Server.URLEncode(sName) & _
			"&customer.ip=" & Server.URLEncode(sIP) & _
			"&customer.email=" & Server.URLEncode(X_Email) & _
            "&descriptor=" & Server.URLEncode(sCompanyDescriptor)
            If sIsAllow3DTrans And dt_Enable3dsecure Then _ 
		        ParamList = ParamList & "&shopperResultUrl=" & Server.URLEncode(backPublicAddress & "remoteCharge_ccDebitACSBack.asp")    
	End If

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000

	'On Error Resume Next
		HttpReq.open sACSHttpMethod, UrlAddress, false ', sTerminalNumber, sAccountPassword
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		HttpReq.Send ParamList
		HttpError = FormatHttpRequestError(HttpReq)
		sResDetails = HttpReq.responseText

'18.1.2018 dimka - add safe partial cc nunber and cvv in chargeattempts log
    ParamList = Replace(ParamList, "card.number=" & X_ccNumber, "card.number=" & GetSafePartialNumber(X_ccNumber))
    ParamList = Replace(ParamList, "card.cvv=" & X_ccCVV2, "card.cvv=" & GetSafePartialNumber(X_ccCVV2))
    TxtRet = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
    SaveLogChargeAttemptRequestResponse ParamList, TxtRet
    
    TimeControl.Append("A_SEND") 'Time Optimization *********
        'Response.Write(ParamList  & " <br> " & sResDetails)
        Set jsonResult = JSON.parse(sResDetails)
		X_DebitReferenceNum = jsonResult.ndc
		If jsonResult.hasOwnProperty("id") Then X_DebitReferenceCode = jsonResult.id
		If jsonResult.hasOwnProperty("result") Then
            sReturnCode = jsonResult.result.code
		    sError = jsonResult.result.description
        End If
        If jsonResult.hasOwnProperty("redirect") Then hasRedirectUrl = True 'X_3dRedirect = jsonResult.redirect.url
        'Response.End
	On Error Goto 0	

	'Response.Write(sReturnCode & "," & sApprovalNumber & "," & sError)
	If sReturnCode = "000.000.000" Or sReturnCode = "000.100.110" Or sReturnCode = "000.100.111" Or sReturnCode = "000.100.112" Or sReturnCode = "000.600.000" Then
		sReturnCode = "000"
	ElseIf sReturnCode = "000.300.000" Or sReturnCode = "000.200.000" Then
		sReturnCode = "001"
        If hasRedirectUrl Then
            X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
            sIsSendUserConfirmationEmail = False
            sReturnCode = "553"
        End If
	End If	
	Set xmlRet = Nothing
	Set HttpReq = Nothing

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>