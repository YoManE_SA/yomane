<%
'---------------------------------------------------------------------
'	Debit debitB_SN
'---------------------------------------------------------------------
Function Make2DigDate(xNum)
	If Len(xNum) = 1 Then xNum = "0" & xNum
	Make2DigDate = xNum
End Function

'20110120 Tamir - Implementing BNS CC Storage
'Returns positive ID when adding new record, negative ID when record already exists
Function GetBnsStoredCardIdentifier(sCardNumber, sCVV2, sTerminalId)
	GetBnsStoredCardIdentifier = ExecScalar("SET NOCOUNT ON;DECLARE @n int;EXEC @n=GetBnsStoredCardIdentifier '" & sCardNumber & "', '" & sCVV2 & "', " & sTerminalId & ";SELECT @n;SET NOCOUNT OFF;", 0)
End Function

Function ReplaceEscapeSequences(sText)
	dim i, j, sTemp
	If IsEmpty(sText) Then
		sTemp = ""
	ElseIf InStr(sText, "&") < 1 Then
		sTemp = sText
	Else
		sTemp = Replace(sText, "&;", " ")
		For i = 1 To Len(sTemp)
			If Mid(sTemp, i, 1) = "&" Then
				For j = i To Len(sTemp)
					If Mid(sTemp, i, 1) = ";" Then sTemp = Replace(sTemp, Mid(sTemp, i, j-i+1), Mid(sTemp, i+1, 1))
				Next
			End If
		Next
		sTemp = Replace(Replace(sTemp, "&", " "), ";", " ")
	End If
	ReplaceEscapeSequences = sTemp
End Function

Function debitBNS(debitBNSForceCredit)
	If Trim(X_ccHolderName)<>"" Then X_ccHolderName = ReplaceEscapeSequences(X_ccHolderName)
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer, UseBSCardAlias ', IsoCurreny
	If int(X_TypeCredit) = 8 Or int(X_Payments) > 1 Then Call throwError("508") 
	UseBSCardAlias = dt_EnableRecurringBank
	xNames = Split(X_ccHolderName, " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
	'sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	If Len(sCompanyDescriptor) > 24 Then sCompanyDescriptor = Left(sCompanyDescriptor, 24)
	Dim sStoredCard : sStoredCard = ""
	Dim bStoreCard : bStoreCard = False
	Dim bUseStoredCard : bUseStoredCard = False
	Dim bRetryRefund : bRetryRefund = False
    Dim sBNSFormatedAmount : sBNSFormatedAmount = CLng(IIF(Cint(X_Currency) = 6 Or Cint(X_Currency) = 21, X_Amount, formatNumber(X_Amount * 100, 2)))
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0 And Not debitBNSForceCredit) Then
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		Dim nSendDebitReferenceCode : nSendDebitReferenceCode = X_DebitReferenceCode
		'ZEEV - comment the next line if you wish to apply the change daniel sent yesterday for error 1316
		If InStr(1, nSendDebitReferenceCode, "_") > 0 Then nSendDebitReferenceCode = Left(nSendDebitReferenceCode, Len(nSendDebitReferenceCode) - 3)
		bRetryRefund = True
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & vbCrLf & _
		" <soap:Body>" & vbCrLf & _
		"  <xmlApiRequest xmlns=""http://www.voeb-zvd.de/xmlapi/1.0"" id=""a1234"" version=""1.0"">" & vbCrLf & _
		"	<paymentRequest id=""reqid1"">" & vbCrLf & _
		"	<merchantId>" & sAccountId & "</merchantId>" & vbCrLf & _
		"	<timeStamp>" & Year(Now) & "-" & Make2DigDate(Month(Now)) & "-" & Make2DigDate(Day(Now)) & "T" & Time() & "</timeStamp>" & vbCrLf & _
		"	<eventExtId>" & nSendDebitReferenceCode & "</eventExtId>" & vbCrLf & _
		"	<kind>creditcard</kind>" & vbCrLf & _
		"	<action>" & IIF(Cint(X_TypeCredit)=0, "refund", "capture") & "</action>" & vbCrLf & _
		"	<txReferenceExtId>" & IIF(Cint(X_TypeCredit)=2, nSendDebitReferenceCode, X_DebitReferenceCode) & "</txReferenceExtId>" & vbCrLf & _
		"	<changedAmount>" & sBNSFormatedAmount & "</changedAmount>" & vbCrLf & _
		"	</paymentRequest>" & vbCrLf & _
		"  </xmlApiRequest>" & vbCrLf & _
		" </soap:Body>" & vbCrLf & _
		"</soap:Envelope>"
	Else
		'20110119 Tamir - Implementing recurring at BNS (CC storage)
		If Trim(Request("Recurring1")) <> "" Or Trim(Request("RecurringMode")) <> "" Or Trim(requestSource) = "20" Then
			'first or subsequent transaction in recurring series - use BNS card storage
			Dim nStoredCard : nStoredCard = GetBnsStoredCardIdentifier(X_ccNumber, X_ccCVV2, TerminalId)
			If nStoredCard < 0 Then
				'already stored - use stored card
				bStoreCard = False
				bUseStoredCard = True
				sStoredCard = Trim(-nStoredCard)
			Else
				'not stored - update the series
				bStoreCard = True
				bUseStoredCard = False
				sStoredCard = Trim(nStoredCard)
			End If
			If TestNumVar(request("RecurringSeries"), 1, 0, 0) > 0 Then oledbData.Execute "EXEC RecurringSetSeriesIdentifier " & request("RecurringSeries") & ", '" & sStoredCard & "';"
			NewRecurringSeriesStoredCardIdentifier = sStoredCard
		End If
        
        Dim bsAction
	    If CInt(X_TypeCredit)=0 Then 
    		bsAction = "credit"
	    Else 
    		If Cint(X_TransType) = 1 Then bsAction = "preauthorization" _
    	    Else bsAction = "authorization"
	    End If

		'If Not debitBNSForceCredit Then 
        X_DebitReferenceCode = GetTransRefCode()
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & vbCrLf & _
		" <soap:Body>" & vbCrLf & _
		"  <xmlApiRequest xmlns=""http://www.voeb-zvd.de/xmlapi/1.0"" id=""a1234"" version=""1.0"">" & vbCrLf & _
		"	<paymentRequest id=""reqid1"">" & vbCrLf & _
		"	 <merchantId>" & sAccountId & "</merchantId>" & vbCrLf & _
		"	 <timeStamp>" & Year(Now) & "-" & Make2DigDate(Month(Now)) & "-" & Make2DigDate(Day(Now)) & "T" & Time() & "</timeStamp>" & vbCrLf & _
		"	 <eventExtId>" & X_DebitReferenceCode & "</eventExtId>" & vbCrLf & _
		"	 <kind>creditcard</kind>" & vbCrLf
		If (sCompanyDescriptor <> "") Then ParamList = ParamList & "	 <additionalNote>" & sCompanyDescriptor & "</additionalNote>" & vbCrLf
		ParamList = ParamList & _
		"	 <action>" & bsAction & "</action>" & vbCrLf & _
		"	 <amount>" & sBNSFormatedAmount & "</amount>" & vbCrLf & _
		"	 <currency>" & GetCurISOName(Cint(X_Currency)) & "</currency>" & vbCrLf & _
		"	 <creditCard>" & vbCrLf
		If (Not bUseStoredCard) Or (Not UseBSCardAlias) Then ParamList = ParamList & "	  <pan>" & Replace(X_ccNumber, " ", "") & "</pan>" & vbCrLf
		If UseBSCardAlias And (bStoreCard Or bUseStoredCard) Then ParamList = ParamList & "	 <panalias" & IIF(bStoreCard, " generate=""true""", "") & ">" & sStoredCard & "</panalias>" & vbCrLf
		If (Not bUseStoredCard) Or (Not UseBSCardAlias) Then 
		    ParamList = ParamList & _
		    "	 <expiryDate>" & vbCrLf & _
		    "	  <month>" & X_ccExpMM & "</month>" & vbCrLf & _
		    "	  <year>20" & X_ccExpYY & "</year>" & vbCrLf & _
		    "	 </expiryDate>" & vbCrLf & _
		    "	 <holder>" & X_ccHolderName & "</holder>" & vbCrLf
		    If Trim(Replace(X_ccCVV2," ","")) <> "" Then ParamList = ParamList & "	 <verificationCode>" & X_ccCVV2 & "</verificationCode>" & vbCrLf
		End If    
		ParamList = ParamList & _
		"	 </creditCard>" & vbCrLf & _
		"	</paymentRequest>" & vbCrLf & _
		"  </xmlApiRequest>" & vbCrLf & _
		" </soap:Body>" & vbCrLf & _
		"</soap:Envelope>"
	End if

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    If isTestTerminal Then UrlAddress = "https://test.soap.bs-card-service.com/soapapi/services/XmlApiNl" _
	Else UrlAddress = "https://soap.bs-card-service.com/soapapi/services/XmlApiNl" 'new
    'UrlAddress = "https://soap.bs-card-service.com/soap/services/XmlApiNl"   'old  
	
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false, sAccountSubId, sAccountPassword '"paybynet", "start"
		HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		HttpReq.setRequestHeader "SOAPAction", "process"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		'Response.Write(ParamList & "<br>" & sResDetails) : Response.End()
		HttpError = FormatHttpRequestError(HttpReq)
	
	    'Made by tamir to hide cvv and card number from log
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "<verificationCode>" & X_ccCVV2, "<verificationCode>" & GetSafePartialNumber(X_ccCVV2))
		sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sResDetails = Replace(sResDetails, "<verificationCode>" & X_ccCVV2, "<verificationCode>" & GetSafePartialNumber(X_ccCVV2))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

		Set xmlRet = HttpReq.responseXml
		sReturnCode = Trim(xmlRet.selectSingleNode("//rc").text)
		sApprovalNumber = xmlRet.selectSingleNode("//aid").text
		sError = xmlRet.selectSingleNode("//message").text

	   If CInt(X_TypeCredit) = 0 And (Trim(sReturnCode) = "1316" Or Trim(sReturnCode) = "1507" Or Trim(sReturnCode) = "1509" Or Trim(sReturnCode) = "1510") Then 
			'FileAppendData "BNS1316.txt", Now() & vbCrLf & "DebitReferenceCode: " & X_DebitReferenceCode & ";Action: " & bsAction & vbCrLf
			If bRetryRefund Then
				debitBNS(True)
				Exit Function
			End If
      End If

		X_DebitReferenceCode = xmlRet.selectSingleNode("//txExtId").text
		'If IsNull(sResDetails) Or IsEmpty(sResDetails) Or IsNothing(sResDetails) Then sResDetails = "=== NO RESPONSE DETAILS ==="
		'Set objErr=Server.GetLastError()
		TimeControl.Append("A_SEND") 'Time Optimization *********
	On Error Goto 0
	'If Not IsNull(objErr) And Not IsEmpty(objErr) Then
	'	With objErr
	'		FileAppendData "BNS.txt", sResDetails & vbCrLf
	'		FileAppendData "BNS.txt", "ASPCode=" & .ASPCode & vbCrLf
	'		FileAppendData "BNS.txt", "Number=" & .Number & vbCrLf
	'		FileAppendData "BNS.txt", "Source=" & .Source & vbCrLf
	'		FileAppendData "BNS.txt", "Category=" & .Category & vbCrLf
	'		FileAppendData "BNS.txt", "File=" & .File & vbCrLf
	'		FileAppendData "BNS.txt", "Line=" & .Line & vbCrLf
	'		FileAppendData "BNS.txt", "Column=" & .Column & vbCrLf
	'		FileAppendData "BNS.txt", "Description=" & .Description & vbCrLf
	'		FileAppendData "BNS.txt", "ASPDescription=" & .ASPDescription & vbCrLf
	'		FileAppendData "BNS.txt", vbCrLf
	'	End With
	'	Set objErr = Nothing
	'End If

	If Len(sResDetails) = 0 Then
		'FileAppendData "BNS521.txt", "readyState:" & HttpReq.readyState & "; Status:" & HttpReq.status & "; statusText:" & HttpReq.statusText & vbCrLf
		'FileAppendData "BNS521.txt", Now() & vbCrLf & "Username:" & sAccountSubId & "; Password:" & sAccountPassword & vbCrLf
		'FileAppendData "BNS521.txt", ParamList & vbCrLf & vbCrLf
		'FileAppendData "BNS521.txt", "ResDetails:" & sResDetails & "; ReturnCode:" & sReturnCode & vbCrLf & vbCrLf
		Call throwError("521")
	End if
	sDebitReturnAnswer = Left(sResDetails, 3900)
	If (Len(sReturnCode) < 4) Then
		'FileAppendData "BNS520.txt", "readyState: " & HttpReq.readyState & ";Status " & HttpReq.status & ": " & HttpReq.statusText & vbCrLf
		'FileAppendData "BNS520.txt", Now() & vbCrLf & "Username: " & sAccountSubId & ";Password: " & sAccountPassword & vbCrLf
		'FileAppendData "BNS520.txt", ParamList & vbCrLf
		'FileAppendData "BNS520.txt", "ResDetails:" & sResDetails & "; ReturnCode:" & sReturnCode & vbCrLf & vbCrLf
		Call throwError("520")
	End If
	
    'FileAppendData "BNS1315.txt", Now() & vbCrLf & "Username: " & sAccountSubId & ";Password: " & sAccountPassword & vbCrLf
    'FileAppendData "BNS1315.txt", ParamList & vbCrLf
    'FileAppendData "BNS1315.txt", "ResDetails:" & sResDetails & "; ReturnCode:" & sReturnCode & vbCrLf & vbCrLf
	
	If Trim(sReturnCode) = "0000" Then
		sReturnCode = "000"
	ElseIf Trim(sReturnCode) = "001" Then 
		sReturnCode = "002" '001 is taken by netpay
	ElseIf bStoreCard Then
		oledbData.Execute "DELETE FROM tblBnsStoredCard WHERE ID=" & TestNumVar(sStoredCard, 1, 0, 0)
		If TestNumVar(request("RecurringSeries"), 1, 0, 0) > 0 Then oledbData.Execute "EXEC RecurringSetSeriesIdentifier " & request("RecurringSeries") & ";"
	End if	

	Set xmlRet = Nothing
	Set HttpReq = Nothing

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>