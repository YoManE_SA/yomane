<%
'---------------------------------------------------------------------
'	Debit PPro
'---------------------------------------------------------------------
Function remoteCharge_Phone_Qiwi_BackDetectAlways()
    If Request("cs") <> "" And Request("txid") <> "" Then   'test if reply from bank
        GetTransInfo("Lca_ReplyCode='553' And Lca_TransNum IN(Select companyTransPending_id From tblCompanyTransPending Where DebitApprovalNumber='" & TestNumVar(Request("txid"), 0, -1, 0) & "')")
        sRedirect = "remoteCharge_Phone.asp" & _
            "?CompanyNum=" & GetURLValue(xRequestParams, "CompanyNum") & _
            "&TransType=4" & _
            "&requestSource=18" & _
            "&ReplyURL=" & Server.URLEncode(GetURLValue(xRequestParams, "RetURL")) & _
            "&TransApprovalID=" & Request("txid") & _
            "&PaRes=" & Request("cs")
        Response.Redirect(sRedirect)
    End If
End Function

Function remoteCharge_Phone_Qiwi_BackDetect()
    xRetParams = Replace(Replace(Replace(xRetParams, "&lt;", "<"), "&gt;", ">"), "``", """")
    Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
	xmlRet.LoadXML xRetParams
    'Response.Write(debit_QiwiMD5GetXmlValue(xmlRet, "//RedirectURL")) : Response.End
    Response.Redirect debit_QiwiMD5GetXmlValue(xmlRet, "//RedirectURL")
End Function

Function debit_Qiwi_MD5(str)
    debit_Qiwi_MD5 = UCase(Hash("MD5", str).Hex)
End Function

Function debit_QiwiMD5GetXmlValue(xmlDoc, itemValue)
    Dim xNode : Set xNode = xmlDoc.selectSingleNode(itemValue)
    If xNode Is Nothing Then debit_QiwiMD5GetXmlValue = "" _
    Else debit_QiwiMD5GetXmlValue = Trim(xNode.text)
End Function

Function debit_QiwiMD5GetXmlValueAttrib(xmlDoc, itemValue, attrib)
    Dim xNode : Set xNode = xmlDoc.selectSingleNode(itemValue)
    debit_QiwiMD5GetXmlValueAttrib = ""
    If xNode Is Nothing Then Exit Function
    Dim xAttrib : Set xAttrib = xNode.attributes.getNamedItem(attrib)
    debit_QiwiMD5GetXmlValueAttrib = Trim(xAttrib.text)
End Function

Function debit_Qiwi()
	Dim HttpReq, UrlAddress, ParamList, sApprovalNumber, targetUrl, xNames, fName, sName

	xNames = Split(Trim(X_HolderName), " ")
	if Ubound(xNames) < 1 Then Call throwError("517")
	fName = xNames(0) : sName = xNames(1)

	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
	call HttpReq.setOption(3, "LOCAL_MACHINE\My\Udi Azulay")
	HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
    UrlAddress = "https://merchant.payboutique.com/xml_service"
	If Cint(X_TransType) = 1 Then Call throwError("503")

	If trim(X_TransType) = "4" Then
		ParamList = "returnmode=urlencode" & vbCrLf & _
		"&txtype=GETTXSTATUS" & vbCrLf & _
		"&login=" & Server.URLEncode(sAccountSubId) & vbCrLf & _
		"&password=" & Server.URLEncode(sAccountPassword) & vbCrLf & _
		"&contractid=" & Server.URLEncode(sAccountID) & vbCrLf & _
		"&txid=" & Server.URLEncode(X_ConfirmNumber)
	ElseIf (Cint(X_TransType) = 2) Then
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	ElseIf (X_TypeCredit = "0") Or (X_TypeCredit = "1") Then 
    	sOuterIP = Session("ProcessURL")
		X_DebitReferenceCode = GetTransRefCode()
        ParamList = _
        "<Message version=""0.2"">" & vbCrLf & _
        " <Header>" & vbCrLf & _
        "  <Identity>" & vbCrLf & _
        "   <UserID>" & sAccountSubId & "</UserID>" & vbCrLf & _
        "   <Signature>" & debit_Qiwi_MD5(sAccountSubId & debit_Qiwi_MD5(sAccountPassword))  & "</Signature>" & vbCrLf & _
        "  </Identity>" & vbCrLf & _
        "  <Time>" & Now() & "</Time>" & vbCrLf & _
        " </Header>" & vbCrLf & _
        " <Body type=""GetInvoice"" paymentMethod=""Qiwi"" buyerCurrency=""RUB"" live=""" & IIF(isTestTerminal, "false", "true") & """>" & vbCrLf & _
        "  <Order>" & vbCrLf & _
        "   <MerchantID>" & sAccountId & "</MerchantID>" & vbCrLf & _
        "   <OrderID>" & X_DebitReferenceCode & "</OrderID>" & vbCrLf & _
        "   <AmountMerchantCurrency>" & X_Amount & "</AmountMerchantCurrency>" & vbCrLf & _
        "   <MerchantCurrency>" & GetCurISOName(Cint(X_Currency)) & "</MerchantCurrency>" & vbCrLf & _
        "   <Label></Label>" & vbCrLf & _
        "   <SiteAddress>" & merchantUrl & "</SiteAddress>" & vbCrLf & _
        "   <Description>" & X_Comment & "</Description>" & vbCrLf
        If (X_TypeCredit = "0") Then
            ParamList = ParamList & _
            "<Payee>" & vbCrLf & _
            "    <AccountID>" & X_PhoneNumber & "</AccountID>" & vbCrLf & _
            "</Payee>" & vbCrLf
        Else
            ParamList = ParamList & _
            "   <Buyer>" & vbCrLf & _
            "    <FirstName>" & fName & "</FirstName>" & vbCrLf & _
            "    <LastName>" & sName & "</LastName>" & vbCrLf & _
            "    <AccountID>" & X_PhoneNumber & "</AccountID>" & vbCrLf & _
            "   </Buyer>" & vbCrLf
        End If
        ParamList = ParamList & _
        "  </Order>" & vbCrLf & _
        " </Body>" & vbCrLf & _
        "</Message>"
	End If	
	On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
		HttpReq.Send("xml=" & Server.URLEncode(ParamList))
		TxtRet = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		SaveLogChargeAttemptRequestResponse ParamList, TxtRet
    	'Response.Write(UrlAddress & vbCrLf & "<br>" & vbCrLf & ParamList & vbCrLf & "<br>" & vbCrLf & TxtRet & vbCrLf & "<br>" & vbCrLf) ': Response.End()
	    If IsNull(TxtRet) Or Len(TxtRet) = 0 Then Call throwError("521")
        Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
	    xmlRet.LoadXML HttpReq.responseText
	On Error GoTo 0

    If debit_QiwiMD5GetXmlValueAttrib(xmlRet, "//Body", "type") = "GetInvoice" Then
	    sReturnCode = debit_QiwiMD5GetXmlValue(xmlRet, "//status")
        'If sReturnCode = "" debit_QiwiMD5GetXmlValue Call throwError("520") 'internal error
        sApprovalNumber = debit_QiwiMD5GetXmlValue(xmlRet, "//ReferenceID")
		sReturnCode = "001"
		X_3dRedirect = debit_QiwiMD5GetXmlValue(xmlRet, "//RedirectURL")
		If (X_3dRedirect <> "") Then 
		    X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
			sReturnCode = "553"
			sIsSendUserConfirmationEmail = False
			'REDIRECTURL = URLDecode(X_3dRedirect) & "&PaReq=" & Request("REDIRECTSECRET")
		End If	
    Else 'error
		sReturnCode =  debit_QiwiMD5GetXmlValue(xmlRet, "//ErrorID")
		X_CustomErrorDesc = debit_QiwiMD5GetXmlValue(xmlRet, "//ErrorMessage")
    End If
	Set HttpReq = Nothing
	DebitIProcessResult sReturnCode, sApprovalNumber
End function
%>