<%
'---------------------------------------------------------------------
'	Debit Webbilling
'---------------------------------------------------------------------
Function remoteCharge_echeck_InPay_BackDetectAlways()
    If Request("invoice_reference") <> "" Then   'test if reply from bank
        Dim nTransID
        bUseOldTerminal = True
        nApprovalRecurringSeries = "NULL"
        nRefCode = Request("order_id")
        If (nRefCode <> 0) Then
            If (TestStrVar(Request("invoice_status"), 0, -1, "") = "approved") Then
                nTransID = MovePendingTransaction("DebitReferenceCode='" & nRefCode & "'", "000")
                'Call ChargeBackTrans(0, "ApprovalNumber='" & X_ApprovalNumber & "'")
            ElseIf (TestStrVar(Request("invoice_status"), 0, -1, "") = "cancelled") Or (TestStrVar(Request("invoice_status"), 0, -1, "") = "refunded") Then
                nTransID = MovePendingTransaction("DebitReferenceCode='" & nRefCode & "'", "005")
            Else
                Response.Write("incorrect invoice_status")
                Response.End()
            End If
            If nTransID = 0 Then Response.Write("TransactionId not found") _
            Else Response.Write(nTransID & " OK")
        Else
            Response.Write("Transaction not found")
        End if    
        Response.End()
    End If
End Function

Function remoteCharge_echeck_InPay_BackDetect()
	Set xdResponse = Server.CreateObject("Microsoft.XMLDOM")
	xRetParams = Replace(Replace(Replace(xRetParams, "&lt;", "<"), "&gt;", ">"), "``", "'")
	xdResponse.loadXML(xRetParams)
	Set xmlRet = xdResponse
	'Response.Write(xmlRet.selectSingleNode("//online-bank-url").text)
    Response.Redirect xmlRet.selectSingleNode("//online-bank-url").text
End Function

Function debit_InPayGetXMLValue(xElement, xValue)
    Dim pNode
    Set pNode = xElement.selectSingleNode(xValue)
    If Not pNode Is Nothing Then debit_InPayGetXMLValue = pNode.text
End Function

Function debit_AddXmlValue(xElement, xValue, sOutField)
    debit_AddXmlValue = debit_InPayGetXMLValue(xElement, xValue)
    If sOutField <> "" Then sOutField = sOutField & ": "
    If Not IsEmpty(debit_AddXmlValue) Then debit_AddXmlValue = sOutField & Replace(debit_AddXmlValue, "<br />", VbCrLf)
End Function

Function debit_InPaySimulateTransfer()
    Dim InPayServerAddress : InPayServerAddress = "https://" & IIF(isTestTerminal, "test-", "") & "admin.inpay.com/api/v1/"
    Dim pRet, UrlAddress, X_RefTransApproval, EncodedSignature
    EncodedSignature = LCase(Hash("MD5", sAccountID & ":" & sAccountPassword).Hex)

    X_RefTransApproval = ExecScalar("Select DebitApprovalNumber From tblCompanyTransPending Where ID=" & X_TransID, "")
	Set HttpReq = Server.CreateObject("Msxml2.XMLHTTP") 'Msxml2.ServerXMLHTTP.3.0
	UrlAddress = "https://" & IIF(isTestTerminal, "test-", "") & "admin.inpay.com/test/create_transfer?invoice_ref=" & X_RefTransApproval & "&passkey=" & EncodedSignature
	Response.Write(UrlAddress & vbCrLf & "</br>")
	HttpReq.Open "POST", UrlAddress, false
	HttpReq.setRequestHeader "charset", "utf-8"
	HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded;"
	HttpReq.Send()
	pRet = HttpReq.responseText
    Response.Write(pRet & vbCrLf & "</br>")
End Function

Function debit_InPayGetBanks()
    Dim InPayServerAddress : InPayServerAddress = "https://" & IIF(isTestTerminal, "test-", "") & "admin.inpay.com/api/v1/"
    Dim pRet, UrlAddress, X_RefTransApproval, EncodedSignature
	
	If X_TransID <> 0 Then
	    X_RefTransApproval = ExecScalar("Select DebitApprovalNumber From tblCompanyTransPending Where ID=" & X_TransID, "")
	    EncodedSignature = "invoice_ref=" & X_RefTransApproval & "&merchant_id=" & sAccountID & "&processor_id=" & sAccountSubId & "&secret_key=" & sAccountPassword
	    EncodedSignature = LCase(Hash("MD5", EncodedSignature, True).Hex) 'MD5
	    ParamList = "header[processor_id]=" & Server.URLEncode(sAccountSubId) & _
            "&header[merchant_id]=" & Server.URLEncode(sAccountID) & _
            "&header[checksum]=" & Server.URLEncode(EncodedSignature) & _
            "&header[exclude_binaries]=true" & _
            "&invoice_ref=" & X_RefTransApproval
        UrlAddress = InPayServerAddress & "get_payment_instructions"
    Else
	    EncodedSignature = "merchant_id=" & sAccountID & "&processor_id=" & sAccountSubId & "&secret_key=" & sAccountPassword
	    EncodedSignature = LCase(Hash("MD5", EncodedSignature, True).Hex) 'MD5
	    ParamList = "header[processor_id]=" & Server.URLEncode(sAccountSubId) & _
            "&header[merchant_id]=" & Server.URLEncode(sAccountID) & _
            "&header[checksum]=" & Server.URLEncode(EncodedSignature) & _
            "&header[exclude_binaries]=true" & _
            "&only_relevant=true"
        If X_BankID <> 0 Then ParamList = ParamList & "&bank_id=" & X_BankID _
        Else ParamList = ParamList & "&country_id=" & Server.URLEncode(X_BankCountry)
        UrlAddress = InPayServerAddress & "get_countries_and_banks"
    End If

    Set Hash = Nothing
	Set HttpReq = Server.CreateObject("Msxml2.XMLHTTP") 'Msxml2.ServerXMLHTTP.3.0
	HttpReq.Open "POST", UrlAddress, false
	HttpReq.setRequestHeader "charset", "utf-8"
	HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded;"
	HttpReq.Send(ParamList)
	TxtRet = HttpReq.responseText
	'Response.Write(ParamList)
	Set xmlRet = HttpReq.responseXml
	If X_TransID <> 0 Then
	    EncodedSignature = LCase(Hash("MD5", sAccountID & ":" & sAccountPassword, True).Hex) 'MD5
        'Response.Write(TxtRet)
        pRet = "Reply=000"
        pRet = pRet & "&BankInfo=" & debit_AddXmlValue(xmlRet, "//bank-address", "")
        pRet = pRet & "&TransferAmount=" & debit_AddXmlValue(xmlRet, "//transfer-amount-with-currency", "TransferAmount")
        pRet = pRet & "&AccountInfo=" & debit_AddXmlValue(xmlRet, "//swift", "Swift") & vbCrLf & debit_AddXmlValue(xmlRet, "//iban", "iban") & vbCrLf & debit_AddXmlValue(xmlRet, "//registration", "bankCode") & vbCrLf & debit_AddXmlValue(xmlRet, "//account", "Account")
        'pRet = pRet & "&AccountOwner=" & debit_AddXmlValue(xmlRet, "//owner-address", "")
        pRet = pRet & "&AccountOwner=" & "PayByNet" & vbCrLf & "Mykinon Office 23" & vbCrLf & "1065 Nicosia" & vbCrLf & "Cyprus" & vbCrLf & "support@netpay-intl.com"
        pRet = pRet & "&TransferInfo=" & debit_AddXmlValue(xmlRet, "//reference", "Reference") & vbCrLf & debit_AddXmlValue(xmlRet, "//additional-field1", "Info1") & vbCrLf & debit_AddXmlValue(xmlRet, "//additional-field2", "Info2")
        pRet = pRet & "&BankURL=" & debit_InPayGetXMLValue(xmlRet, "//online-bank-url")
        pRet = pRet & "&SimulateURL=" & Server.URLEncode(Session("ProcessURL") & "SimulatePushBankTransfer.asp?CompanyNum=" & CompanyNum & "&TransID=" & X_TransID)
        Response.Write(pRet)
	Else
        Set nBanks = xmlRet.selectNodes("//bank")
        For x = 0 To nBanks.length - 1
            pRet = pRet & debit_InPayGetXMLValue(nBanks.Item(x), "id") & "," & debit_InPayGetXMLValue(nBanks.Item(x), "name") & "," & debit_InPayGetXMLValue(nBanks.Item(x), "logo/url") & ";"
        Next
    	If Len(pRet) > 0 Then pRet = Left(pRet, Len(pRet) - 1)
    	BankListResponse "000", pRet
	End If
End Function

Function debit_InPay()
	Dim HttpReq, UrlAddress, ParamList, sApprovalNumber, sDebitReturnAnswer
    Dim InPayServerAddress : InPayServerAddress = "https://" & IIF(isTestTerminal, "test-", "") & "admin.inpay.com/api/v1/"
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	Set HttpReq = Server.CreateObject("Msxml2.XMLHTTP") 'Msxml2.ServerXMLHTTP.3.0
	'HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
	UrlAddress = InPayServerAddress & "create_invoice"
	'if X_Amount < 25 Then throwError("505")
	If (Cint(X_TransType) = 4) Then
		throwError("532") 'refunds not supported
	ElseIf (Cint(X_TransType) = 0) Then
		X_DebitReferenceCode = GetTransRefCode()
		EncodedSignature = "amount=" & FormatNumber(X_Amount, 2, -1, 0) & "&currency=" & GetCurISOName(Cint(X_Currency)) & "&merchant_id=" & sAccountID & "&order_id=" & X_DebitReferenceCode & "&order_text=" & Server.URLEncode(sCompanyDescriptor) & "&processor_id=" & sAccountSubId & "&secret_key=" & sAccountPassword
		EncodedSignature :  EncodedSignature = LCase(Hash("MD5", EncodedSignature, True).Hex) 'MD5
		ParamList = "header[processor_id]=" & Server.URLEncode(sAccountSubId) & _
            "&header[merchant_id]=" & Server.URLEncode(sAccountID) & _
            "&header[checksum]=" & Server.URLEncode(EncodedSignature) & _
            "&header[exclude_binaries]=true" & _
            "&bank_id=" & TestNumVar(X_BankID, 0, -1, 0) & _
            "&order_id=" & Server.URLEncode(X_DebitReferenceCode) & _
            "&amount=" & Server.URLEncode(FormatNumber(X_Amount, 2, -1, 0)) & _
            "&currency=" & Server.URLEncode(GetCurISOName(Cint(X_Currency))) & _
            "&order_text=" & Server.URLEncode(sCompanyDescriptor) & _
            "&buyer_email=" & Server.URLEncode(X_Email) & _
            "&buyer_name=" & Server.URLEncode(X_ccHolderName) & _
            "&buyer_address=" & Server.URLEncode(BACHAddr1 & " " & BAZipCode & " " & BACity) & _
            "&buyer_ip=" & Server.URLEncode(sIP) & _
            "&country=" & Server.URLEncode(BACountryCode)
        'Response.Write("<br>" & UrlAddress & "?" & ParamList)
		'Response.End
	Else
		Call throwError("503")
	End If
	'Response.Write("<PRE><br>" & UrlAddress & "?request=" + ParamList) : Response.End
	ParamList = Replace(ParamList, vbCrLf, "")
	'On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "charset", "utf-8"
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded;"
		HttpReq.Send(ParamList)
		TxtRet = HttpReq.responseText
		TxtRet = Replace(Replace(TxtRet, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba))
		SaveLogChargeAttemptRequestResponse Replace(Replace(ParamList, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba)), TxtRet
		HttpError = FormatHttpRequestError(HttpReq)
		Set xmlRet = HttpReq.responseXml
	'On Error GoTo 0
	'Response.Write(TxtRet)
	'FileAppendData "Webbilling.txt", "ResDetails:" & TxtRet & "; ReturnCode:" & vbCrLf & vbCrLf
	'Response.End
	if (xmlRet.documentElement Is Nothing) Then
		Set xdResponse = Server.CreateObject("Microsoft.XMLDOM")
		xdResponse.loadXML(TxtRet)
		Set xmlRet = xdResponse
	End If
	'Response.Write(xmlRet.Text & "<br>")
	If IsNull(TxtRet) Or Len(TxtRet) = 0 Then Call throwError("521")
	If xmlRet.DocumentElement.NodeName = "Error" Then
    	sReturnCode = debit_InPayGetXMLValue(xmlRet, "//http-code")
		sError = debit_InPayGetXMLValue(xmlRet, "//message")
	ElseIf xmlRet.DocumentElement.NodeName = "invoice" Then
    	sApprovalNumber = debit_InPayGetXMLValue(xmlRet, "//invoice-ref")
		sReturnCode = "554"
	ElseIf xmlRet.DocumentElement.NodeName = "instructions" Then
    	sApprovalNumber = debit_InPayGetXMLValue(xmlRet, "//reference")
    	If sApprovalNumber = "" Then Call throwError("520") 'internal error
	    X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
	    'Response.Write(X_3dRedirect)
		sReturnCode = "554"
	End if
	'If (Cint(X_TransType) = 4) Then OleDbData.Execute "Delete From tblCompanyTransPending Where DebitCompanyID=42 And DebitApprovalNumber='" & X_ConfirmNumber & "'"
	DebitIProcessResult sReturnCode, sApprovalNumber
	Set xmlRet = Nothing
	Set HttpReq = Nothing
End function
%>    
