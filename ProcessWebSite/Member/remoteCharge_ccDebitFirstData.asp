<%
'---------------------------------------------------------------------
'	Debit debitB_SN
'---------------------------------------------------------------------
Function FD_Make2DigDate(xNum)
	If Len(xNum) = 1 Then xNum = "0" & xNum
	FD_Make2DigDate = xNum
End Function

Function FD_ReplaceEscapeSequences(sText)
	dim i, j, sTemp
	If IsEmpty(sText) Then
		sTemp = ""
	ElseIf InStr(sText, "&") < 1 Then
		sTemp = sText
	Else
		sTemp = Replace(sText, "&;", " ")
		For i = 1 To Len(sTemp)
			If Mid(sTemp, i, 1) = "&" Then
				For j = i To Len(sTemp)
					If Mid(sTemp, i, 1) = ";" Then sTemp = Replace(sTemp, Mid(sTemp, i, j-i+1), Mid(sTemp, i+1, 1))
				Next
			End If
		Next
		sTemp = Replace(Replace(sTemp, "&", " "), ";", " ")
	End If
	FD_ReplaceEscapeSequences = sTemp
End Function

Function debitFirstData()
	If Trim(X_ccHolderName)<>"" Then X_ccHolderName = ReplaceEscapeSequences(X_ccHolderName)
	Dim HttpReq, UrlAddress, ParamList, sApprovalNumber, sDebitReturnAnswer, fdAction, sTransaction_Approved
	'sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	If Len(sCompanyDescriptor) > 24 Then sCompanyDescriptor = Left(sCompanyDescriptor, 24)
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_DebitReferenceNum = "" Then Call throwError("532") 'Reftrans not Found
		fdAction = IIF(Cint(X_TransType) = 2, "32", "34") '32:tagged Pre-Authorization Completion, 34:Refund
		ParamList = _
			" <Transaction>" & vbCrLf  & _
			"  <ExactID>" & sTerminalNumber & "</ExactID>" & vbCrLf & _
			"  <Password>" & sAccountPassword & "</Password>" & vbCrLf & _
			"  <Transaction_Type>" & fdAction & "</Transaction_Type>" & vbCrLf & _
			"  <Transaction_Tag>" & X_DebitReferenceNum & "</Transaction_Tag>" & vbCrLf & _
			"  <Authorization_Num>" & X_ConfirmNumber & "</Authorization_Num>" & vbCrLf & _
			"  <DollarAmount>" & X_Amount & "</DollarAmount>" & vbCrLf & _
			" </Transaction>"
	Else
	    X_DebitReferenceCode = GetTransRefCode()
		fdAction = IIF(Cint(X_TransType) = 1, "01", "00") '01:Pre-Authorization, 00:Purchase
		ParamList = _
			" <Transaction>" & vbCrLf  & _
			"  <ExactID>" & sTerminalNumber & "</ExactID>" & vbCrLf & _
			"  <Password>" & sAccountPassword & "</Password>" & vbCrLf & _
			"  <Transaction_Type>" & fdAction & "</Transaction_Type>" & vbCrLf & _
			"  <Card_Number>" & Replace(X_ccNumber, " ", "") & "</Card_Number>" & vbCrLf & _
			"  <Currency>" & GetCurISOName(Cint(X_Currency)) & "</Currency>" & vbCrLf & _
			"  <DollarAmount>" & X_Amount & "</DollarAmount>" & vbCrLf
		If X_Track2 <> "" Then ParamList = ParamList & "  <Track2>" & X_Track2 & "</Track2>" & vbCrLf
		ParamList = ParamList & _
			"  <Expiry_Date>" & FD_Make2DigDate(X_ccExpMM) & FD_Make2DigDate(X_ccExpYY) & "</Expiry_Date>" & vbCrLf & _
			"  <CardHoldersName>" & X_ccHolderName & "</CardHoldersName>" & vbCrLf & _
			"  <VerificationStr2>" & X_ccCVV2 & "</VerificationStr2>" & vbCrLf & _
			"  <CVD_Presence_Ind>" & IIF(X_ccCVV2 <>"", "1", "0") & "</CVD_Presence_Ind>" & vbCrLf & _
			"  <ZipCode>" & BAZipCode & "</ZipCode>" & vbCrLf & _
			"  <Ecommerce_Flag>7</Ecommerce_Flag>" & vbCrLf & _
			"  <Reference_No>" & X_DebitReferenceCode & "</Reference_No>" & vbCrLf & _
			"  <Language>EN</Language>" & vbCrLf & _
			"  <Client_IP>" & sIP & "</Client_IP>" & vbCrLf & _
			"  <Client_Email>" & X_Email & "</Client_Email>" & vbCrLf & _
			"  <soft_descriptor>" & sCompanyDescriptor & "</soft_descriptor>" & vbCrLf & _
			" </Transaction>" & vbCrLf
			'"  <Customer_Ref>{0}</Customer_Ref>", transaction.Request.merchantRefCode);
	End If
	'HASH & HMAC
	Dim	HashValue, HMacValue, HashTime
    Set HashValue = CreateTextBlob(Empty): HashValue.UTF8 = ParamList
    HashValue = Replace(LCase(NetpayCryptoObj.Hash("SHA1", HashValue).Hex), "-", "")
	'HMAC SHA1
	sendTime = DateAdd("h", -2, Now)
	HashTime = Year(sendTime) & "-" & Make2DigDate(Month(sendTime)) & "-" & Make2DigDate(Day(sendTime)) & "T" & Make2DigDate(Hour(sendTime)) & ":" & Make2DigDate(Minute(sendTime)) & ":" & Make2DigDate(Second(sendTime)) & "Z"
	HMacValue = "POST" & vbLf & "application/xml; Charset=UTF-8" & vbLf & HashValue & vbLf & HashTime & vbLf & "/transaction/v12"

	Set KeyBlob = CreateTextBlob(Empty) : KeyBlob.UTF8 = sAccountSubId
	Set MessageBlob = CreateTextBlob(Empty) : MessageBlob.UTF8 = HMacValue
	HMacValue = HMAC("HMACSHA1", KeyBlob, MessageBlob).Base64 'sha1

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	'"http://localhost/processWebSite/member/test.asp"
    If isTestTerminal Then UrlAddress = "https://api.demo.globalgatewaye4.firstdata.com/transaction/v12" _ 
	Else UrlAddress = "https://api.globalgatewaye4.firstdata.com/transaction/v12"

	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false ', sTerminalNumber, sAccountPassword
		HttpReq.setRequestHeader "Accept", "application/xml"
		HttpReq.setRequestHeader "x-gge4-date", HashTime
        HttpReq.setRequestHeader "x-gge4-content-sha1", HashValue
		HttpReq.setRequestHeader "Expect", "100-continue"
		HttpReq.setRequestHeader "host", "Nothing"
		HttpReq.setRequestHeader "Content-Type", "application/xml; Charset=UTF-8"
        HttpReq.setRequestHeader "Authorization", "GGE4_API " & sAccountId & ":" & HMacValue

		HttpReq.Send ParamList
		HttpError = FormatHttpRequestError(HttpReq)
		sResDetails = HttpReq.responseText
		SaveLogChargeAttemptRequestResponse ParamList, sResDetails
		TimeControl.Append("A_SEND") 'Time Optimization *********

		'Response.Write(ParamList & "<br>" & sResDetails) ': Response.End()

		Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
		xmlRet.LoadXML HttpReq.responseText

		sReturnCode = Trim(xmlRet.selectSingleNode("//EXact_Resp_Code").text)
		sApprovalNumber = xmlRet.selectSingleNode("//Authorization_Num").text
		X_DebitReferenceNum = xmlRet.selectSingleNode("//Transaction_Tag").text
		sError = xmlRet.selectSingleNode("//EXact_Message").text
		sTransaction_Approved = xmlRet.selectSingleNode("//Transaction_Approved").text
	On Error Goto 0	

	'Response.Write(sReturnCode & "," & sApprovalNumber & "," & sError)

	If Trim(sReturnCode) = "00" And sTransaction_Approved = "true" Then
		sReturnCode = "000"
	ElseIf Trim(sReturnCode) = "001" Then
		sReturnCode = "002" '001 is taken by netpay
	End If	
	Set xmlRet = Nothing
	Set HttpReq = Nothing

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>