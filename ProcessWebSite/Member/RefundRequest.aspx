<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    Dim nMerchant As Integer
    Private Function GetUserIP()
        Dim UserIp As String = IIf(String.IsNullOrWhiteSpace(Request.ServerVariables("HTTP_X_FORWARDED_FOR")), Request.UserHostAddress, Request.ServerVariables("HTTP_X_FORWARDED_FOR"))
        If (UserIp Is Nothing) Then UserIp = "" Else UserIp = UserIp.Split(",").FirstOrDefault().Trim()
        Return UserIp
    End Function

    Sub ReturnReply(ByVal nCode As Integer, Optional ByVal sDetails As String = Nothing, Optional ByVal drData As SqlDataReader = Nothing)
        If Not drData Is Nothing Then drData.Close()
        Dim sText As String = GlobalData.Value(GlobalDataGroup.REFUND_REQUEST_REPLY_CODE, nCode).Text
        Dim resText As String = "Reply=" & nCode & "&ReplyDesc=" & IIf(String.IsNullOrEmpty(sText), "Unspecified Error", sText)
        If Not String.IsNullOrEmpty(sDetails) Then resText += "&" & sDetails
        Response.Write(resText)
        Dim UserIp As String = GetUserIP()
        Dim RefID As Integer = dbPages.TestVar(Request("RefTransID"), 0, -1, 0)
        'dbPages.ExecSql ("INSERT INTO tblLogRefundRequest(QueryString, RequestFrom, ReplyCode, ReplyString, ServerName) VALUES ('" & dbPages.DBText(Request.QueryString.ToString()) & "', '" & dbPages.DBText(Request.Form.ToString()) & "', " & nCode & ", '" & dbPages.DBText(sDetails) & "', '" & dbPages.DBText(My.Computer.Name) & "');")
        dbPages.ExecSql("Exec [Log].[spInsertLogRefundRequest] " &
           IIf(nMerchant = 0, "NULL", nMerchant) & "," &
           "'" & dbPages.DBText(Request("Action")) & "'," &
           "'" & dbPages.DBText(nCode) & "'," &
           "'" & dbPages.DBText(Request.QueryString.ToString() & "&" & Request.Form.ToString()) & "'," &
           "'" & dbPages.DBText(resText) & "'," &
           IIf(RefID = 0, "Null", RefID) & "," &
           "'" & UserIp & "'")
        Response.End()
    End Sub

    Function GetMerchantID(ByVal sMerchantNumber As String, ByVal sMD5 As String) As Integer
        If String.IsNullOrEmpty(sMerchantNumber) Then ReturnReply(1)
        If String.IsNullOrEmpty(sMD5) Then ReturnReply(8)
        sMerchantNumber = sMerchantNumber.Trim
        Throw New Exception("just testing here")
        If sMerchantNumber.Length <> 7 Then ReturnReply(1)
        If dbPages.TestVar(sMerchantNumber, 1, 0, -1) < 0 Then ReturnReply(1)
        Dim drData As SqlDataReader = dbPages.ExecReader("SELECT TOP 1 ID, HashKey, RemoteRefundRequestIPs FROM tblCompany WITH (NOLOCK) WHERE IsAskRefund=1 AND IsAskRefundRemote=1 AND CustomerNumber='" & dbPages.DBText(sMerchantNumber) & "'")
        If drData.Read Then
            nMerchant = drData("ID")
            If nMerchant = 0 Then ReturnReply(1, , drData)
            Dim sIPs As String = drData("RemoteRefundRequestIPs")
            If String.IsNullOrEmpty(sIPs) Then ReturnReply(7, , drData)
            Dim UserIp As String = GetUserIP()
            If Not IPManager.ContainsIP(sIPs, UserIp) Then ReturnReply(7, "IP=" & UserIp, drData)
            Dim sHashed As String = Md5Hash.HashASCIIBase64(sMerchantNumber & drData("HashKey"))
            If sHashed <> sMD5 Then ReturnReply(8, , drData)
            drData.Close()
            Return nMerchant
        End If
        drData.Close()
        ReturnReply(1, , drData)
        GetMerchantID = -1
    End Function

    Function GetTransID(ByVal nMerchant As Integer, ByVal sTransID As String, Optional ByRef nAmount As Decimal = 0D, Optional ByRef nCurrency As Integer = 0, Optional ByRef bPartialRefundAllowed As Boolean = False, Optional ByVal isCheckTransDate As Boolean = False) As Integer
        If String.IsNullOrEmpty(sTransID) Then ReturnReply(2)
        Dim insertDate As DateTime
        Dim nTransID As Integer = dbPages.TestVar(sTransID, 1, 0, 0)
        If nTransID = 0 Then ReturnReply(2)
        Dim drData As SqlDataReader = dbPages.ExecReader("SELECT Amount, Currency, DebitCompanyID, InsertDate FROM tblCompanyTransPass WITH (NOLOCK) WHERE ID='" & nTransID & "' AND CompanyID='" & nMerchant & "' AND CreditType>0")
        If drData.Read Then
            insertDate = drData("InsertDate")
            nAmount = drData("Amount")
            nCurrency = drData("Currency")
            bPartialRefundAllowed = dbPages.ExecScalar("SELECT dc_isAllowPartialRefund FROM tblDebitCompany WITH (NOLOCK) WHERE debitCompany_id=" & drData("DebitCompanyID"))
        Else
            nAmount = 0D
            nCurrency = 0
            bPartialRefundAllowed = False
        End If
        drData.Close()
        If nAmount = 0D Then ReturnReply(2)
        If isCheckTransDate Then If DateDiff("m", insertDate, DateTime.Now) > 12 Then ReturnReply(12)
        Return nTransID
    End Function

    Function GetRefundRequests(ByVal nMerchant As Integer, ByVal nTransID As Integer) As String
        Dim sbDetails As New StringBuilder, nRequests As Integer = 0, nAmount As Decimal = 0D
        Dim sSQL As String = "SELECT * FROM tblRefundAsk WITH (NOLOCK) WHERE RefundAskStatus<>6 AND TransID=" & nTransID & " AND CompanyID=" & nMerchant
        Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
        Do While drData.Read
            nRequests += 1
            nAmount += drData("RefundAskAmount")
            sbDetails.AppendFormat("&ID{0}={1}", nRequests, drData("ID"))
            sbDetails.AppendFormat("&Amount{0}={1:0.00}", nRequests, drData("RefundAskAmount"))
            sbDetails.AppendFormat("&Confirm{0}={1}", nRequests, Server.HtmlEncode(drData("RefundAskConfirmationNum")))
            sbDetails.AppendFormat("&Date{0}={1}", nRequests, drData("RefundAskDate"))
            sbDetails.AppendFormat("&Status{0}={1}", nRequests, Server.HtmlEncode(GlobalData.Value(63, drData("RefundAskStatus")).Description))
        Loop
        drData.Close()
        Return sbDetails.Insert(0, "RequestCount=" & nRequests & "&RequestedAmount=" & nAmount.ToString("0.00")).ToString
    End Function

    Sub RefundRequestCreate()
        nMerchant = GetMerchantID(Request("MerchantNumber"), Request("Signature"))
        If nMerchant <= 0 Then ReturnReply(1)
        Dim nAmountMax As Decimal = 0D, nCurrency As Integer = 0, bPartialRefundAllowed As Boolean = False
        Dim nTransID As Integer = GetTransID(nMerchant, Request("RefTransID"), nAmountMax, nCurrency, bPartialRefundAllowed, True)
        If nTransID <= 0 Then ReturnReply(2)
        Dim nAmount As Decimal = nAmountMax
        If Not String.IsNullOrEmpty(Request("Amount")) Then
            nAmount = dbPages.TestVar(Request("Amount"), 1D, 0D, -1D)
            If nAmount > nAmountMax Then ReturnReply(4)
            If nAmount <= 0D Then ReturnReply(3)
            If nAmount <> nAmountMax And Not bPartialRefundAllowed Then ReturnReply(6)
        End If
        Dim nRequests As Integer = 0, sbDetails As New StringBuilder, sSQL As String
        sSQL = "SELECT Sum(RefundAskAmount) FROM tblRefundAsk WITH (NOLOCK) WHERE RefundAskStatus<>6 AND TransID = " & nTransID & " And CompanyID = " & nMerchant & " AND RefundAskCurrency=" & nCurrency
        Dim nAmountRequested As Decimal = dbPages.TestVar(dbPages.ExecScalar(sSQL), 0D, nAmountMax, 0D)
        If nAmountRequested >= nAmountMax Then ReturnReply(4)
        nAmountMax -= nAmountRequested
        If String.IsNullOrEmpty(Request("Amount")) Then nAmount = nAmountMax
        If nAmount > nAmountMax Then ReturnReply(4)
        Dim sComment As String = IIf(String.IsNullOrEmpty(Request("Comment")), String.Empty, Request("Comment"))
        sComment = sComment.Trim.PadRight(300).Substring(0, 300).Trim
        sSQL = "INSERT INTO tblRefundAsk (CompanyID,TransID, RefundAskAmount, RefundAskCurrency, RefundAskComment) VALUES (" & nMerchant & ", " & nTransID & "," & _
        " " & nAmount & ", " & nCurrency & ", '" & dbPages.DBText(Request("Comment")) & "');SELECT @@IDENTITY;"
        Dim nRequest As Integer = dbPages.ExecScalar(sSQL)
        ReturnReply(0, "RequestID=" & nRequest & "&" & GetRefundRequests(nMerchant, nTransID))
    End Sub

    Sub RefundRequestDelete()
        Dim nMerchant As Integer = GetMerchantID(Request("MerchantNumber"), Request("Signature"))
        If nMerchant <= 0 Then ReturnReply(1)
        Dim nTransID As Integer = GetTransID(nMerchant, Request("RefTransID"))
        If nTransID <= 0 Then ReturnReply(2)
        Dim nRequestID As Integer = dbPages.TestVar(Request("RequestID"), 1, 0, 0)
        If nRequestID < 1 And Not String.IsNullOrEmpty(Request("RequestID")) Then ReturnReply(11)
        Dim sSQL As String = "SELECT COUNT(*) FROM tblRefundAsk WITH (NOLOCK) WHERE RefundAskStatus IN (0, 3) AND TransID=" & nTransID & " AND CompanyID=" & nMerchant
        If nRequestID > 0 Then sSQL &= " AND ID=" & nRequestID
        Dim nRecords As Integer = dbPages.ExecScalar(sSQL)
        If nRecords = 0 Then ReturnReply(9)
        If nRecords > 1 Then ReturnReply(10)
        sSQL = "UPDATE tblRefundAsk SET RefundAskStatus=6 WHERE RefundAskStatus IN (0, 3) AND TransID=" & nTransID
        If nRequestID > 0 Then sSQL &= " AND ID=" & nRequestID
        dbPages.ExecSql(sSQL)
        ReturnReply(0, GetRefundRequests(nMerchant, nTransID))
    End Sub

    Sub RefundRequestStatus()
        Dim nMerchant As Integer = GetMerchantID(Request("MerchantNumber"), Request("Signature"))
        If nMerchant <= 0 Then ReturnReply(1)
        Dim nTransID As Integer = GetTransID(nMerchant, Request("RefTransID"))
        If nTransID <= 0 Then ReturnReply(2)
        ReturnReply(0, GetRefundRequests(nMerchant, nTransID))
    End Sub

    Sub Page_Load()
        Dim sAction As String = String.Empty
        If Not String.IsNullOrEmpty(Request("Action")) Then sAction = Request("Action").ToString.ToUpper
        Select Case sAction.ToUpper
            Case "CREATE"
                RefundRequestCreate()
            Case "DELETE"
                RefundRequestDelete()
            Case "STATUS"
                RefundRequestStatus()
            Case Else
                ReturnReply(5, "Action=" & sAction)
        End Select
    End Sub
</script>