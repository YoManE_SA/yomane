<%
	Function Make2DigDate(xNum)
		If Len(xNum) = 1 Then xNum = "0" & xNum
		Make2DigDate = xNum
	End Function

	Sub SendRefundBNS(iRs)
		Dim sReturnCode, ParamList, IsoCurreny, UrlAddress, xmlRet, sRefCode
		sRefCode = iRs("DebitReferenceCode")
		UrlAddress = "https://soap.bs-card-service.com/soap/services/XmlApiNl"
		IsoCurreny = Array("ILS", "USD", "EUR", "GBP", "AUD", "CAD", "JPY", "NOK")
		If bDebug Then Response.Write("<br />DebitReferenceCode=" & iRs("DebitReferenceCode"))
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & VbCrlf & _
		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & VbCrlf & _
		"	<soap:Body>" & VbCrlf & _
		"		<xmlApiRequest xmlns=""http://www.voeb-zvd.de/xmlapi/1.0"" id=""a1234"" version=""1.0"">" & VbCrlf & _
		"			<paymentRequest id=""reqid1"">" & VbCrlf & _
		"				<merchantId>" & iRs("AccountId") & "</merchantId>" & VbCrlf & _
		"				<timeStamp>" & Year(Now) & "-" & Make2DigDate(Month(Now)) & "-" & Make2DigDate(Day(Now)) & "T" & Time() & "</timeStamp>" & VbCrlf & _
		"				<eventExtId>" & Left(sRefCode, Len(sRefCode) - 3) & "</eventExtId>" & VbCrlf & _
		"				<kind>creditcard</kind>" & VbCrlf & _
		"				<action>refund</action>" & VbCrlf & _
		"				<txReferenceExtId>" & sRefCode & "</txReferenceExtId>" & VbCrlf & _
		"				<changedAmount>" & (formatNumber(iRs("Amount"), 2) * 100) & "</changedAmount>" & VbCrlf & _
		"			</paymentRequest>" & VbCrlf & _
		"		</xmlApiRequest>" & VbCrlf & _
		"	</soap:Body>" & VbCrlf & _
		"</soap:Envelope>"
		If bDebug Then Response.Write("<hr />" & Server.HTMLEncode(ParamList) & "<hr />")

		sReturnCode = ""
		On Error Resume Next
		Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
		if bDebug then Response.Write("<br />" & UrlAddress & " | " & iRs("AccountSubId") & " | " & iRs("AccountPassword"))
		HttpReq.open "POST", UrlAddress, false, Trim(iRs("AccountSubId")), Trim(iRs("accountPassword"))
		HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		HttpReq.setRequestHeader "SOAPAction", "process"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		if bDebug then Response.Write("<hr />" & Server.HTMLEncode(sResDetails) & "<hr />")
		Set xmlRet = HttpReq.responseXml
		sReturnCode = Trim(xmlRet.selectSingleNode("//rc").text)
		sApprovalNumber = xmlRet.selectSingleNode("//aid").text
		sError = xmlRet.selectSingleNode("//message").text
		If Trim(sReturnCode) = "0000" Then sReturnCode = "000"
		Set HttpReq = Nothing
		If sError = "" Then sError=sResDetails
		On Error Goto 0
		If sReturnCode <> "000" And Len(sRefCode) < 4 Then sReturnCode="1316" 'when refCode is 3 digits only, set transaction to "never charged"
		Call ProcessResults(iRs, "BNS", sReturnCode, sError, sApprovalNumber)
	End Sub
%>
