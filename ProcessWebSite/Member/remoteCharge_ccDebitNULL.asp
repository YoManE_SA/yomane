<%
'---------------------------------------------------------------------
'	Debit NULL (local)
'---------------------------------------------------------------------
Function remoteCharge_debitNULL_BackDetect()
	X_ReplyURL = GetURLValue(xRequestParams, "RetURL") 
	'onload=""document.getElementById('frmMain').submit()""
	Response.Write("<html><body ><form id=""frmMain"" method=""post"" action=""" & Session("ProcessURL") & "remote_charge.asp"">")
	Response.Write(" <input type=""hidden"" name=""CompanyNum"" value=""" & GetURLValue(xRequestParams, "companyNum") & """/>")
	Response.Write(" <input type=""hidden"" name=""TransType"" value=""4""/>")
	Response.Write(" <input type=""hidden"" name=""requestSource"" value=""" & GetURLValue(xRequestParams, "requestSource") & """/>")
	Response.Write(" <input type=""hidden"" name=""ReplyURL"" value=""" & GetURLValue(xRequestParams, "RetURL")  & """/>")
	Response.Write(" <input type=""hidden"" name=""RefTransID"" value=""" & xTransID & """/>")
	Response.Write(" <input type=""hidden"" name=""ClientIP"" value=""" & Request.ServerVariables("REMOTE_ADDR") & """/>")
	Response.Write(" <br/><div style=""margin:auto;background-color:#f0f0f0;width:250px;height:300px;border:1px solid black;text-align:center;"">")
	Response.Write("  <div style=""padding:5px;;background-color:#f8f8f8;border-bottom:1px solid black;"">3D Secure Simulator</div></br>")
	Response.Write("  <div style=""padding:20px;""><p style=""margin:auto;text-align:left;"">Simulator code usage:</br>")
	Response.Write("  000: return approve</br>002: return decline</br></br></br></p>")
	Response.Write("  Enter Code: </br>")
	Response.Write("  <input type=""text"" name=""PaRes"" value=""000"" style=""width:80px;""/>")
	Response.Write("  <input type=""submit"" value=""Continue"" />")
	Response.Write(" </div></div>")
	Response.Write("</form></body></html>")
	Response.End
    'sReturnCode = "000"
    'nRes = MovePendingTransaction("DebitCompanyID=1 And companyTransPending_id=" & xTransID, sReturnCode)
	'if nRes <> 0 Then ReturnResponseFromTransID nRes, sReturnCode _
    'Else Response.Write("Pending transaction not found: " & xTransID)
End Function

Function debitNULL()
	Dim sApprovalNumber, sCardNumber
	Dim sReturnCode : sReturnCode = "595"
	Dim nTimeout : nTimeout = 0
	isTestOnly = 1
	sCardNumber = Replace(X_ccNumber, " ", "")
	If sCardNumber = "4580000000000000" Or sCardNumber = "4387751111111111" Or sCardNumber = "5442987111111111" _
	Or sCardNumber = "4111111111111111" Or sCardNumber = "371000911111111" Or sCardNumber = "36005131111111" _
	Or sCardNumber = "3535111111111111" Or sCardNumber = "5326300000000000" Or sCardNumber = "5326140000000000" _
	Or sCardNumber = "4511742700707855" Or sCardNumber = "4056850111111111" Or sCardNumber = "5130113111111111" _
	Or sCardNumber = "6221701111111111" Or sCardNumber = "91000000" Or Trim(ckacct) = "12345678" _
	Or sCardNumber = "6299711111111111" Then
		sReturnCode = "000"
		Select Case X_Amount
			Case 0.04 : sReturnCode = "1001"
			Case 0.05 : sReturnCode = "1002"
			Case 0.90 : nTimeout = 5
			Case 0.91 : nTimeout = 10
			Case 0.92 : nTimeout = 20
			Case 0.93 : nTimeout = 30
			Case 0.94 : nTimeout = 40
			Case 0.95 : nTimeout = 50
			Case 0.96 : nTimeout = 60
			Case 0.97 : nTimeout = 70
			Case 0.98 : nTimeout = 80
			Case 0.99 : nTimeout = 90
			Case Else : If Int(X_Amount) < 1 Then sReturnCode = "596"
		End Select
		If nTimeout <> 0 Then
			NetpayCryptoObj.Delay nTimeout * 1000
		End If
        If Round(X_Amount, 1) = 55.3 Or CInt(X_Amount) = 553 Then
			If Request("PaRes") = "" Then
				X_Is3dSecure = True
				sReturnCode = "553"
			Else
				sReturnCode = Request("PaRes")
			End If
		End If
	End If

	Dim sDebitRequest
	If sCardNumber <> "" Then
		sDebitRequest="CardNumber=" & GetSafePartialNumber(sCardNumber) & "&CVV2=" & GetSafePartialNumber(X_ccCVV2)
	Else
		sDebitRequest="AccountNumber=" & GetSafePartialNumber(ckacct) & "&RoutingNumber=" & GetSafePartialNumber(ckaba)
	End if
	SaveLogChargeAttemptRequestResponse sDebitRequest, "Timeout=" & nTimeout & "&ReturnCode=" & sReturnCode

	If sReturnCode = "000" Then 
		If X_TerminalHasNext And TestNumVar(Session(X_TransINC), 0, -1, 0) = 0 Then sReturnCode = "524" _
		Else sApprovalNumber = "1234567"
    ElseIf sReturnCode = "553" Then
        X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
        Select Case (X_Amount * 100) Mod 10
            Case 1: X_3dRedirectMethod = "SameWindow" 
            Case 2: X_3dRedirectMethod = "UseFrames" 
            Case 3: X_3dRedirectMethod = "UseIFrame" 
            Case 4: X_3dRedirectMethod = "NewWindow" 
        End Select
	End If
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End function
%>