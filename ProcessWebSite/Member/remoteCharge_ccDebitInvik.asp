<%
'---------------------------------------------------------------------
'	Debit ModernProcessing ()
'---------------------------------------------------------------------
Dim pDataSend, pRetData
pDataSend = "" : pRetData = ""

Function AddMDRPField(fNum, fValue)
	pDataSend = pDataSend & fNum & ":" & fValue & "/"
End Function

Function GetMDRPFieldValue(xtrData, xVal)
	Dim sIndex, eIndex
	sIndex = InStr(1, xtrData, xVal & ":")
	If sIndex < 1 Then Exit Function
	sIndex = sIndex + Len(xVal) + 1
	eIndex = InStr(sIndex, xtrData, "/")
	If eIndex < sIndex Then eIndex = Len(xtrData)
	GetMDRPFieldValue = Mid(xtrData, sIndex, eIndex - sIndex)
End Function

Function debitModernProcessing()
	Dim pMP, mUnique, xRef, TempRs, xAmnt', IsoCurreny
	'IsoCurreny = Array("376", "840", "978", "826")
	Set pMP = Server.CreateObject("Compunet.CModernProcessing")
	lnCount = TestNumVar(ExecScalar("Select dc_nextTransID%1000000 From tblDebitCompany Where debitCompany_id=21", 0), 0, -1, 0) + 1
	TimeControl.Append("A_InvikSelectTransID") 'Time Optimization *********
	ExecSql("Update tblDebitCompany Set dc_nextTransID = dc_nextTransID + 1 Where debitCompany_id=21")
	TimeControl.Append("A_InvikUpdateTransID") 'Time Optimization *********
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	If CInt(X_TypeCredit) = 0 Then
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans Not Found
		If Not terminalActive Then 
			DebitIProcessResult "000", "TRCLD"	'Internal terminal close bypass authorization on refund
			Exit Function
		End if
	Else 
		X_DebitReferenceCode = GetTransRefCode()
	End If

	AddMDRPField "MSG", IIF(Cint(X_TypeCredit) = 0, "0400", "0100")
	AddMDRPField "002", Replace(X_ccNumber, " ", "")
	AddMDRPField "003", "000000"
	AddMDRPField "004", pMP.PadLeft(formatNumber(X_Amount, 2) * 100, 12, "0")
	AddMDRPField "007", pMP.Format(Now, "MMDDhhmmss")
	AddMDRPField "011", pMP.PadLeft(lnCount, 6, "0")
	AddMDRPField "012", pMP.Format(Now, "Hhmmss")
	AddMDRPField "013", pMP.Format(Now, "MMDD")
	AddMDRPField "014", X_ccExpYY & X_ccExpMM
	AddMDRPField "018", sAccountPassword 'MCC
	AddMDRPField "019", "352"
	AddMDRPField "022", "002"
	AddMDRPField "025", "59"
	If Len(Trim(X_Track2)) > 2 Then 
		eIndex = InStr(1, X_Track2, "?")
		If eIndex < 1 Then eIndex = Len(eIndex)
		AddMDRPField "035", Replace(X_ccNumber, " ", "") & "=" & Mid(X_Track2, 1, eIndex - 1)
	End If	
	AddMDRPField "037", pMP.PadLeft(X_DebitReferenceCode, 12, "0")
	AddMDRPField "041", CStr(sAccountId)
	AddMDRPField "042", pMP.PadRight(Left(sCompanyDescriptor, 15), 15, "0")
	If Cint(X_TypeCredit) <> 0 Then AddMDRPField "048", "11" & pMP.PadRight(X_ccCVV2, 4, "0") & " "
	AddMDRPField "049", GetCurISOCode(X_Currency)
	If Cint(X_TypeCredit) <> 0 Then AddMDRPField "061", "20351000060004420000000000"
	
	'Response.Write(pDataSend) : Response.End()
	
	pMP.Server = "172.17.3.51" '"172.17.3.58"
	pMP.Port = 3002

	Err.Clear
	On Error Resume Next
	pMP.Timeout = 12 * 1000
	pMP.Connect()
	If Err.number <> 0 Then
		HttpError = "Connect: " & Err.number & ": " & Err.Description
	Else 
		pRetData = pMP.SendTrans(pDataSend)
		If Err.number <> 0 Then HttpError = "Send: " & Err.number & ": " & Err.Description
	End If
	pMP.Disconnect()
	On Error Goto 0

	Dim sDebitRequest : sDebitRequest = pDataSend
	sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
	sDebitRequest = Replace(sDebitRequest, "048:11" & pMP.PadRight(X_ccCVV2, 4, "0"), "048:11" & pMP.PadRight(GetSafePartialNumber(X_ccCVV2), 4, "0"))
	pRetData = Replace(pRetData, X_ccNumber, GetSafePartialNumber(X_ccNumber))
	SaveLogChargeAttemptRequestResponse sDebitRequest, pRetData

	TimeControl.Append("A_InvikSend") 'Time Optimization *********
	xRetStatus = GetMDRPFieldValue(pRetData, "039")
	sApprovalNumber = GetMDRPFieldValue(pRetData, "038")
	'Response.Write(pDataSend & "<br>" & pRetData & "<br>" & Len(pRetData) & " " & xRetStatus & " " & sApprovalNumber) : Response.End
	'If CInt(X_TypeCredit) = 0 Then FileAppendData "Invik.txt", Now & vbcrlf & pDataSend & vbcrlf & pRetData & vbcrlf & vbcrlf & vbcrlf
	Set pMP = Nothing
	
	CTP_Status = 1
	If xRetStatus = "00" Then 
		xRetStatus = "000"
	ElseIf CInt(X_TypeCredit) = 0 And (xRetStatus = "12" Or xRetStatus = "") Then
		'(InStr(1, LCase(Request.ServerVariables("REMOTE_USER")), "lavi") > 0)
		xRetStatus = "000"
		sApprovalNumber = "ERR12"
	End If
	If xRetStatus = "" Then Call throwError("521")

	DebitIProcessResult xRetStatus, sApprovalNumber
End Function
%>