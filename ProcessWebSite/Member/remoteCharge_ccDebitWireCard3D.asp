<%
'---------------------------------------------------------------------
'	Debit WireCard - 21042010 Udi
'---------------------------------------------------------------------
Function remoteCharge_cc_debitWireCard3D_BackDetect()
    If Request("PaRes") <> "" Then
        sRedirect = PROCESS_URL & "remote_charge.asp" & _
            "?CompanyNum=" & GetURLValue(xRequestParams, "companyNum") & _
            "&TransType=4" & _
            "&requestSource=18" & _
            "&ReplyURL=" & Server.URLEncode(GetURLValue(xRequestParams, "RetURL")) & _
            "&RefTransID=" & Request("MD") & _
            "&PaRes=" & Server.URLEncode(Request("PaRes"))
        Response.Redirect(sRedirect)
    Else
        Set xmlRet = Server.CreateObject("Msxml2.DOMDocument.3.0")
        xmlRet.loadXML(xRetParams)
        X_3dRedirect = xmlRet.selectSingleNode("//CC_TRANSACTION/THREE-D_SECURE/AcsUrl").text
        d3PaReq = Trim(xmlRet.selectSingleNode("//CC_TRANSACTION/THREE-D_SECURE/PAReq").text)
        Set xmlRet = Nothing
	    Response.Redirect X_3dRedirect & "&PaReq=" & d3PaReq & "&TermUrl=" & Server.URLEncode(Session("ProcessURL") & "remoteCharge_Back.asp")
    End If
End Function

Function debitWireCard3D()
	Dim HttpReq, xmlRet, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer', IsoCurreny
	'IsoCurreny = Array("ILS", "USD", "EUR", "GBP", "AUD", "CAD", "JPY", "NOK")
	xNames = Split(X_ccHolderName, " ")
	if Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
	if Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	If (Request("PaRes") <> "") Then
	    ParamList = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCrLf & _
	        "<WIRECARD_BXML xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"">" & vbCrLf & _
	        " <W_REQUEST>" & vbCrLf & _
	        "  <W_JOB>" & vbCrLf & _
	        "   <JobID>" & X_DebitReferenceCode & "</JobID>" & vbCrLf & _
	        "   <BusinessCaseSignature>" & sAccountId & "</BusinessCaseSignature>" & vbCrLf & _
	        "   <FNC_CC_PURCHASE>" & vbCrLf & _
	        "    <FunctionID>" & X_DebitReferenceCode & "</FunctionID>" & vbCrLf & _
	        "    <CC_TRANSACTION mode=""" & IIf(isTestOnly, "demo", "live") & """>" & vbCrLf & _
            "     <TransactionID>" & X_DebitReferenceCode & "</TransactionID>" & vbCrLf & _
            "     <GuWID>" & X_ConfirmNumber & "</GuWID>" & vbCrLf & _
	        "     <CREDIT_CARD_DATA>" & vbCrLf & _
	        "      <CVC2>" & X_ccCVV2 & "</CVC2>" & vbCrLf & _
	        "     </CREDIT_CARD_DATA>" & vbCrLf & _
	        "     <THREE-D_SECURE>" & vbCrLf & _
	        "      <PARes>" & Request("PARes") & "</PARes>" & vbCrLf & _
	        "     </THREE-D_SECURE>" & vbCrLf & _
	        "    </CC_TRANSACTION>" & vbCrLf & _
	        "   </FNC_CC_PURCHASE>" & vbCrLf & _
	        "  </W_JOB>" & vbCrLf & _
	        " </W_REQUEST>" & vbCrLf & _
	        "</WIRECARD_BXML>"
    Else
		X_DebitReferenceCode = GetTransRefCode()
	    ParamList = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCrLf & _
	        "<WIRECARD_BXML xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"">" & vbCrLf & _
	        " <W_REQUEST>" & vbCrLf & _
	        "  <W_JOB>" & vbCrLf & _
	        "   <JobID>" & X_DebitReferenceCode & "</JobID>" & vbCrLf & _
	        "   <BusinessCaseSignature>" & sAccountId & "</BusinessCaseSignature>" & vbCrLf & _
	        "   <FNC_CC_ENROLLMENT_CHECK>" & vbCrLf & _
	        "    <FunctionID>" & X_DebitReferenceCode & "</FunctionID>" & vbCrLf & _
	        "    <CC_TRANSACTION mode=""" & IIf(isTestOnly, "demo", "live") & """>" & vbCrLf & _
	        "     <TransactionID>" & X_DebitReferenceCode & "</TransactionID>" & vbCrLf & _
	        "     <CommerceType>eCommerce</CommerceType>" & vbCrLf & _
	        "     <Amount minorunits=""2"">" & CLng(X_Amount*100) & "</Amount>" & vbCrLf & _
	        "     <Currency>" & GetCurISOName(X_Currency) & "</Currency>" & vbCrLf & _
	        "     <CountryCode>" & BACountryCode & "</CountryCode>" & vbCrLf & _
	        "     <Usage>" & sCompanyDescriptor & "</Usage>" & vbCrLf & _
	        "     <CREDIT_CARD_DATA>" & vbCrLf & _
	        "      <CreditCardNumber>" & Replace(X_ccNumber, " ", "") & "</CreditCardNumber>" & vbCrLf & _
	        "      <CVC2>" & X_ccCVV2 & "</CVC2>" & vbCrLf & _
	        "      <ExpirationYear>" & (X_ccExpYY mod 100) + 2000 & "</ExpirationYear>" & vbCrLf & _
	        "      <ExpirationMonth>" & X_ccExpMM & "</ExpirationMonth>" & vbCrLf & _
	        "      <CardHolderName>" & X_ccHolderName & "</CardHolderName>" & vbCrLf & _
	        "     </CREDIT_CARD_DATA>" & vbCrLf & _
	        "     <CONTACT_DATA>" & vbCrLf & _
	        "      <IPAddress>" & sIP & "</IPAddress>" & vbCrLf & _
	        "     </CONTACT_DATA>" & vbCrLf & _
	        "     <CORPTRUSTCENTER_DATA>" & vbCrLf & _
	        "      <ADDRESS>" & vbCrLf & _
	        "       <FirstName>" & fName & "</FirstName>" & vbCrLf & _
	        "       <LastName>" & sName & "</LastName>" & vbCrLf & _
	        "       <Address1>" & BACHAddr1 & "</Address1>" & vbCrLf & _
	        "       <Address2>" & BACHAddr2 & "</Address2>" & vbCrLf & _
	        "       <City>" & BACity & "</City>" & vbCrLf & _
	        "       <ZipCode>" & BAZipCode & "</ZipCode>" & vbCrLf & _
	        "       <State>" & BAStateCode & "</State>" & vbCrLf & _
	        "       <Country>" & BACountryCode & "</Country>" & vbCrLf & _
	        "       <Phone>" & X_PhoneNumber & "</Phone>" & vbCrLf & _
	        "       <Email>" & X_Email & "</Email>" & vbCrLf & _
	        "      </ADDRESS>" & vbCrLf & _
	        "      <PERSONINFO>" & vbCrLf & _
	        "       <State>" & BAStateCode & "</State>" & vbCrLf & _
	        "      </PERSONINFO>" & vbCrLf & _
	        "     </CORPTRUSTCENTER_DATA>" & vbCrLf & _
	        "    </CC_TRANSACTION>" & vbCrLf & _
	        "   </FNC_CC_ENROLLMENT_CHECK>" & vbCrLf & _
	        "  </W_JOB>" & vbCrLf & _
	        " </W_REQUEST>" & vbCrLf & _
	        "</WIRECARD_BXML>"
	End if
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	If Session("Identity") = "Local" Then UrlAddress = "https://c3-test.wirecard.com/secure/ssl-gateway" _
	Else UrlAddress = "https://c3-test.wirecard.com/secure/ssl-gateway"
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false, sAccountSubId, sAccountPassword '"paybynet", "start"
		HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		Set xmlRet = HttpReq.responseXml
	On Error Goto 0
	If Request("PaRes") <> "" Then
	    sReturnCode = Trim(xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/FunctionResult").text)
	    If sReturnCode = "ACK" Then
    	    sApprovalNumber = xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/GuWID").text
    	    sReturnCode = "000"
	    ElseIf sReturnCode = "PENDING" Then
    	    sApprovalNumber = xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/GuWID").text
    	    sReturnCode = "000"
	    Else
	        If Not xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/ERROR/Number") Is Nothing Then _
    	        sError = xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/ERROR/Number").text
    	    sReturnCode = "521"
	    End If
        'Response.Write("<pre>" & Request("PaRes") & ParamList & vbCrLf & "<br><br>" & vbCrLf & sResDetails & "</pre>") : Response.End
	    Call DebitIProcessResult(sReturnCode, sApprovalNumber)
	Else
	    sReturnCode = Trim(xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/FunctionResult").text)
	    If sReturnCode = "ACK" Then
    	    d3Status = Trim(xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/StatusType").text)
	        If d3Status = "Y" Then
	            X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
                sApprovalNumber = xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/GuWID").text
    	        sIsSendUserConfirmationEmail = False
    	        'Response.Write(X_3dRedirect) : Response.End
        	    Call DebitIProcessResult("553", sApprovalNumber)
	        ElseIf d3Status = "N" Then
    	        sApprovalNumber = xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/GuWID").text
	            debitWireCard() : Exit Function
	        ElseIf d3Status = "U" Then
	            debitWireCard() : Exit Function
	        Else 'If d3Status = "E" Then
	            debitWireCard()
	        End If
	    Else
    	    sError = xmlRet.selectSingleNode("//CC_TRANSACTION/PROCESSING_STATUS/ERROR/Number").text
    	    sReturnCode = sError
    	    Call DebitIProcessResult(sReturnCode, sApprovalNumber)
	    End If
    End If
End Function
%>