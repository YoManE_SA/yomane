<%
'---------------------------------------------------------------------
'	Debit AllCharge
'---------------------------------------------------------------------

Function remoteCharge_CC_DeltaPay_BackDetect()
	Dim outString, fName, sName, iRs, emailAddress, phoneNumber
	sOuterIP = IIF(session("Identity") = "Local", "http://80.179.39.10:8080/Default.aspx", Session("ProcessURL") & "remoteCharge_ccDebitDeltaPayHPPNotify.asp")
	sql = "Select tblCompanyTransPending.*, tblCheckDetails.*, tblCreditCard.*, tblBillingAddress.*, tblDebitTerminals.dt_IsTestTerminal, tblDebitTerminals.accountId, tblDebitTerminals.accountSubId , dbo.GetDecrypted256(tblDebitTerminals.accountPassword256) As accountPassword, " & _
         " dbo.GetDecrypted256(tblCheckDetails.RoutingNumber256) As routingNumber, dbo.GetDecrypted256(tblCheckDetails.AccountNumber256) As accountNumber, tblCheckDetails.email As CheckEmail, tblCheckDetails.phoneNumber As CheckPhone, " & _
         " dbo.GetDecrypted256(tblCreditCard.CCard_number256) As CardNumber, tblCreditCard.email As CardEmail, tblCreditCard.phoneNumber As CardPhone, " & _
         " List.CountryList.CountryISOCode CountryCode, List.StateList.CountryISOCode StateCode " & _
		 " From tblCompanyTransPending " & _
		 " Left Join tblDebitTerminals ON(tblDebitTerminals.TerminalNumber = tblCompanyTransPending.TerminalNumber)" & _
         " Left Join tblCheckDetails ON (tblCompanyTransPending.CheckDetailsID = tblCheckDetails.id)" & _
         " Left Join tblCreditCard ON (tblCompanyTransPending.CreditCardID = tblCreditCard.id)" & _
         " Left Join tblBillingAddress ON (tblCheckDetails.BillingAddressId = tblBillingAddress.id Or tblCreditCard.BillingAddressId = tblBillingAddress.id)" & _
         " Left Join List.CountryList ON (List.CountryList.CountryID = tblBillingAddress.countryId)" & _
         " Left Join List.StateList ON (List.StateList.StateID = tblBillingAddress.stateId)" & _
		 " Where companyTransPending_id=" & xTransID
    'Response.Write(sql)
	Set iRs = oleDbData.Execute(sql)
    If Not iRs.EOF Then
	    Dim sendCardType, paymentMethod
        paymentMethod = iRs("PaymentMethod")
	    Select case paymentMethod
	        Case PMD_CC_AMEX: sendCardType = "Amex"
	        Case PMD_CC_VISA: sendCardType = "Visa"
	        case PMD_CC_MASTERCARD: sendCardType = "MasterCard"
	        case PMD_CC_CUP, PMD_EC_CUP: sendCardType = "CUP"
    		Case Else: throwError("503")
	    End Select
        If Not IsNull(iRs("CreditCardID")) Then 
            xNames = Split(Trim(Replace(iRs("Member"), "  ", " ")), " ")
        	emailAddress = iRs("CardEmail")
	    	phoneNumber = iRs("CardPhone")
    	ElseIf Not IsNull(iRs("CheckDetailsID")) Then 
            xNames = Split(Trim(Replace(iRs("AccountName"), "  ", " ")), " ")
	      	emailAddress = iRs("CheckEmail")
		    phoneNumber = iRs("CheckPhone")
        End If
	    If Ubound(xNames) >= 0 Then fName = xNames(0)
        If Ubound(xNames) >= 1 Then sName = xNames(1)
		outString = _
			"<html><head></head><body>" & vbCrLf & _
            "<form id=""dataForm"" action=""https://fastecom.com/gateways/FAST/processFAST.php"" method=""post"">" & vbCrLf & _
            "<input type=""hidden"" name=""affiliate"" value=""" & iRs("AccountId") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""paymethod"" value=""Credit Card"" />" & vbCrLf & _
            "<input type=""hidden"" name=""post_method"" value=""Async"" />" & vbCrLf & _
            "<input type=""hidden"" name=""processing_mode"" value=""" & IIF(iRs("trans_type") = 1, "authorize", "sale") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""redirect"" value=""" & sOuterIP & "?IsNotify=0"" />" & vbCrLf & _
            "<input type=""hidden"" name=""notification_url"" value=""" & sOuterIP & "?IsNotify=1"" />" & vbCrLf & _
            "<input type=""hidden"" name=""location"" value=""AFF"" />" & vbCrLf & _
            "<input type=""hidden"" name=""order_id"" value=""" & iRs("DebitReferenceCode") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""room_name"" value=""" & iRs("AccountSubId") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""agent_name"" value=""" & iRs("AccountPassword") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""first_name"" value=""" & Left(fName, 20) & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""last_name"" value=""" & Left(sName, 20) & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""address1"" value=""" & iRs("address1") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""address2"" value=""" & iRs("address2") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""city"" value=""" & iRs("city") & """ />" & vbCrLf
		If iRs("stateId") <> 0 Then outString = outString & "<input type=""hidden"" name=""state"" value=""" & iRs("StateCode") & """ />" & vbCrLf
		outString = outString & _
            "<input type=""hidden"" name=""country"" value=""" & iRs("CountryCode") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""zip"" value=""" & IIF(iRs("zipCode") <> "", iRs("zipCode"), "NA") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""telephone"" value=""" & phoneNumber & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""amount"" value=""" & iRs("trans_amount") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""currency"" value=""" & GetCurISOName(iRs("trans_currency")) & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""email"" value=""" & emailAddress & """ />" & vbCrLf
            If paymentMethod = PMD_CC_CUP Or paymentMethod = PMD_EC_CUP Then
        		outString = outString & _
                "<input type=""hidden"" name=""card_type"" value=""" & sendCardType & """ />" & vbCrLf & _
                "<input type=""hidden"" name=""card_number"" value=""" & IIF(iRs("dt_IsTestTerminal"), "4111111111111111", "4111111111111111") & """ />" & vbCrLf & _
                "<input type=""hidden"" name=""cvv"" value=""123"" />" & vbCrLf & _
                "<input type=""hidden"" name=""expiry_mo"" value=""1"" />" & vbCrLf & _
                "<input type=""hidden"" name=""expiry_yr"" value=""" & Year(Now) + 1  & """ />" & vbCrLf
            ElseIf paymentMethod >= PMD_CC_MIN And paymentMethod <= PMD_EC_CUP Then
        		outString = outString & _
                "<input type=""hidden"" name=""card_type"" value=""" & sendCardType & """ />" & vbCrLf & _
                "<input type=""hidden"" name=""card_number"" value=""" & iRs("CardNumber") & """ />" & vbCrLf & _
                "<input type=""hidden"" name=""cvv"" value=""" & DecCVV(iRs("cc_cui")) & """ />" & vbCrLf & _
                "<input type=""hidden"" name=""expiry_mo"" value=""" & Trim(iRs("ExpMM")) & """ />" & vbCrLf & _
                "<input type=""hidden"" name=""expiry_yr"" value=""" & Trim(iRs("ExpYY")) & """ />" & vbCrLf
            ElseIf paymentMethod >= PMD_EC_MIN And paymentMethod <= PMD_EC_MAX Then
        		outString = outString & _
                "<input type=""hidden"" name=""account_number"" value=""" & iRs("accountNumber") & """ />" & vbCrLf & _
                "<input type=""hidden"" name=""routing_number"" value=""" & iRs("routingNumber") & """ />" & vbCrLf
            End If
        	outString = outString & _
            "<input type=""hidden"" name=""customer_ip"" value=""" & iRs("IPAddress") & """ />" & vbCrLf & _
	        "<input type=""hidden"" name=""btnSubmit"" value=""submit"" />" & vbCrLf & _
            "</form>" & vbCrLf & _
            "<script type=""text/javascript"" defer=""defer"">document.getElementById('dataForm').submit();</" & "script>" & vbCrLf & _
			"</body></html>"
    End If
	iRs.Close()
    ExecSql("Update tblLogChargeAttempts Set Lca_RequestString='" & Replace(Server.HTMLEncode(outString), "'", "''") & "' Where LogChargeAttempts_id=" & xLogChargeAttemptID)
	Response.Write(outString)
End Function

Function debit_DeltaPayHpp()
	Dim HttpReq
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		Call debit_DeltaPay()
        Exit Function
	Else
		X_DebitReferenceCode = GetTransRefCode()
	    X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
        X_3dRedirectMethod = "SameWindow"
	    'Response.Write(X_3dRedirect)
		sReturnCode = "553"
	End if
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>
