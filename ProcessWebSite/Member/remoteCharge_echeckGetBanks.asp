﻿<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_transCharge.asp" -->
<%
    Function BankListResponse(erNumber, sData)
        Response.Write("Reply=" & erNumber)
        If Not IsEmpty(sData) Then Response.Write("&BankList=" & sData)
        Response.End()
    End Function

    Dim X_BankCountry, X_Currency, X_BankID, X_TransID, PaymentMethod, sCompanyID
    CompanyNum = DBText(request("CompanyNum"))
    sCompanyID = ExecScalar("SELECT ID FROM tblCompany WHERE CustomerNumber='" & CompanyNum & "'", 0)
    PaymentMethod = TestNumVar(request("PaymentMethod"), PMD_EC_MIN, PMD_EC_MAX, PMD_EC_CHECK)
    X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, MAX_CURRENCY + 1)
    X_BankCountry = Request("Country")
    X_BankID = TestNumVar(Request("BankID"), 0, -1, 0)
    X_TypeCredit = TestNumVar(Request("CreditType"), 0, 8, 1)
    X_TransID = TestNumVar(Request("TransID"), 0, -1, 0)
    If sCompanyID = 0 Then BankListResponse "500", Empty
    If (X_BankCountry <> "") And IsNumeric(X_BankCountry) Then 'Country is in numeric format
	    X_BankCountry = ExecScalar("SELECT CountryISOCode FROM [List].[CountryList] WHERE CountryID = " & X_BankCountry, "US")
	    If X_BankCountry = "" Then BankListResponse "543", Empty
    End if


    Set rsCmp = server.createobject("Adodb.Recordset")
    rsCmp.Open "SELECT * FROM tblCompany WHERE ID='" & sCompanyID & "'", oledbData, 0, 1
    If Not rsCmp.EOF Then
	    bUseOldTerminal = Not rsCmp("IsUsingNewTerminal")
	    sCompanyName = rsCmp("CompanyName")
	    if TestNumVar(rsCmp("ActiveStatus"), 0, -1, CMPS_NEW) < CMPS_INTEGRATION Then Call throwError("501")  'עסק סגור או חסום
    End If
    rsCmp.Close()
    
    Dim sTerminalNumber, sTerminalChargeOptions
	CCF_ID = GetCompanyTerminal(sCompanyID, PaymentMethod, X_Currency, "", "", X_TypeCredit, sTerminalNumber, sTerminalChargeOptions)
	
	Set rsTerminal = Server.CreateObject("Adodb.Recordset")
	rsTerminal.Open "SELECT dc_isActive, dc_TempBlocks, dbo.GetDecrypted256(accountPassword256) As accountPassword," & _
		" dbo.GetDecrypted256(accountPassword3D256) As accountPassword3D, tblDebitTerminals.* FROM tblDebitTerminals" & _
		" LEFT JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany=tblDebitCompany.debitCompany_id" & _
		" WHERE TerminalNumber='" & trim(sTerminalNumber) & "'", oledbData, 0, 1
	If Not rsTerminal.EOF Then
		debitCompany = rsTerminal("DebitCompany")
		isTestTerminal = rsTerminal("dt_IsTestTerminal")
		'terminal login
		sAccountId = Trim(rsTerminal("accountId"))
		sAccountSubId = Trim(rsTerminal("accountSubId"))
		if rsTerminal("accountPassword") <> "" Then sAccountPassword = rsTerminal("accountPassword")
		'terminal 3D login
		sAccountId3D = Trim(rsTerminal("accountId3D"))
		sAccountSubId3D = Trim(rsTerminal("accountSubId3D"))
		if rsTerminal("accountPassword3D") <> "" Then sAccountPassword3D = rsTerminal("accountPassword3D")
	End If
	rsTerminal.Close()
	Select Case debitCompany
		case 42
		    'InPay
		    call debit_InPayGetBanks()
		case else
			'invalid clearing company
			BankListResponse "503", Empty
	End Select
%>
<!--#include file="remoteCharge_echeckInPay.asp"-->
