<%
'---------------------------------------------------------------------
'	Debit Pay ()
'---------------------------------------------------------------------
Function PayOnGetGuid() 
    Set TypeLib = Server.CreateObject("Scriptlet.TypeLib") 
    PayOnGetGuid = LCase(Replace(Mid(CStr(TypeLib.Guid), 2, 36), "-", ""))
    Set TypeLib = Nothing 
End Function 
Function PayOn_Make2DigDate(xNum)
	If Len(xNum) = 1 Then xNum = "0" & xNum
	PayOn_Make2DigDate = xNum
End Function

Function debitPayOn()
   Dim HttpReq, UrlAddress, ParamList, merchantAccountElement, fName, sName, EncodedSignature, sApprovalNumber, strAct, IsoCurreny, BrandName, sResDetails, refFormatDate, isFromApproval
   BrandName = Array("--", "--", "VISA", "DINERS", "AMEX", "MASTER")
   xNames = Split(X_ccHolderName, " ")
   if Ubound(xNames) > -1 Then fName = xNames(0)
   if Ubound(xNames) > 0 Then sName = xNames(1)
	 sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	 If Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	 If isTestTerminal Then UrlAddress = "https://test.ppipe.net/connectors/gateway" _
	 Else UrlAddress = "https://ppipe.net/connectors/gateway" 'new
     Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	 SendMode = IIF(isTestTerminal, "INTEGRATION", "LIVE") 'TEST

	 merchantAccountElement = _
		"  <MerchantAccount type=""" & IIF(isTestTerminal, "CERTIFICATION", "ATLASBANKA") & """>" & VbCrLf & _
		"	<MerchantID>" & sAccountSubId & "</MerchantID>" & VbCrLf & _
		"	<MerchantName>" & sTerminalName & "</MerchantName>" & VbCrLf & _
		"	<Password>" & sAccountSubId & "</Password>" & VbCrLf & _
		"	<TerminalID>" & sAccountSubId & "</TerminalID>" & VbCrLf & _
		"	<Username>555444333</Username>" & VbCrLf & _
		"	<IBAN>" & sMcc & "</IBAN>" & VbCrLf & _
		"	<TransactionCategory>ECOMMERCE</TransactionCategory>" & VbCrLf & _
		"  </MerchantAccount>"
	 ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & VbCrLf & _
		"<Request version=""1.0"">" & VbCrLf
     If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
 	     If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans Not Found
		 isFromApproval = False
		 If CInt(X_TypeCredit) = 0 Then isFromApproval = ExecScalar("Select ID from tblCompanyTransApproval Where TransAnswerID=" & X_RefTransID, 0) > 0 
	     PCode = IIF((Cint(X_TypeCredit) = 0), "RF", "CP")
		 X_RefTransInsertDate = DateAdd("h", -3, X_RefTransInsertDate)
		 refFormatDate = Year(X_RefTransInsertDate) & PayOn_Make2DigDate(Month(X_RefTransInsertDate)) & PayOn_Make2DigDate(Day(X_RefTransInsertDate)) & " " & PayOn_Make2DigDate(Hour(X_RefTransInsertDate)) & ":" & PayOn_Make2DigDate(Minute(X_RefTransInsertDate)) & ":" & PayOn_Make2DigDate(Second(X_RefTransInsertDate))
	     ParamList = ParamList & _
			" <Transaction mode=""" & SendMode & """ response=""SYNC"">" & VbCrLf & _
			"  <Identification>" & VbCrLf & _
			"   <ShortID>" & GetTransRefCode() & "</ShortID>" & VbCrLf & _
			"   <UUID>" & PayOnGetGuid() & "</UUID>" & VbCrLf & _
			"  </Identification>" & VbCrLf & _
			merchantAccountElement & VbCrLf & _
			"  <Payment type=""" & PCode & """>" & VbCrLf & _
			"   <Amount>" & FormatNumber(X_Amount, 2, True, False, False) & "</Amount>" & VbCrLf & _
			"   <Currency>" & GetCurISOName(Cint(X_Currency)) & "</Currency>" & VbCrLf & _
			"   <Descriptor>" & sCompanyDescriptor & "</Descriptor>" & VbCrLf & _
			"  </Payment>" & VbCrLf & _
			"  <CreditCardAccount>" & VbCrLf & _
			"   <Holder>" & X_ccHolderName & "</Holder>" & VbCrLf & _
			"   <Verification>" & X_ccCVV2 & "</Verification>" & VbCrLf & _
			"   <Brand>" & BrandName(ccTypeID) & "</Brand>" & VbCrLf & _
			"   <Number>" & X_ccNumber & "</Number>" & VbCrLf & _
			"   <Expiry month=""" & X_ccExpMM & """ year=""20" & X_ccExpYY & """ />" & VbCrLf & _
			"  </CreditCardAccount>" & VbCrLf & _
			"  <ReferencedTransaction requestTimestamp=""" & refFormatDate & """ type=""" & IIF((Cint(X_TypeCredit) = 0), IIF(isFromApproval, "CP", "DB"), "PA") & """>" & VbCrLf & _
			"	<ConnectorTxID1>" & X_DebitReferenceCode & "</ConnectorTxID1>" & VbCrLf & _
			"	<ConnectorTxID2>" & X_ConfirmNumber & "</ConnectorTxID2>" & VbCrLf & _
			"	<ConnectorTxID3>" & X_DebitReferenceNum & "</ConnectorTxID3>" & VbCrLf & _
			"  </ReferencedTransaction>" & VbCrLf & _
			" </Transaction>" & VbCrLf & _
			"</Request>"
		'Response.Write(sTerminalChargeOptions & "<br>")
		'Response.Write(ParamList)
		'Response.End	
	 Else
		 X_DebitReferenceCode = PayOnGetGuid()
	     PCode = IIF(Cint(X_TransType) = 1, "PA", "DB")
		 ParamList = ParamList & _
			" <Transaction mode=""" & SendMode & """ response=""SYNC"">" & VbCrLf & _
			"  <Identification>" & VbCrLf & _
			"   <ShortID>" & GetTransRefCode() & "</ShortID>" & _
			"   <UUID>" & X_DebitReferenceCode & "</UUID>" & VbCrLf & _
			"  </Identification>" & VbCrLf & _
			merchantAccountElement & VbCrLf & _
			"  <Payment type=""" & PCode & """>" & VbCrLf & _
			"   <Amount>" & FormatNumber(X_Amount, 2, True, False, False) & "</Amount>" & VbCrLf & _
			"   <Currency>" & GetCurISOName(Cint(X_Currency)) & "</Currency>" & VbCrLf & _
			"   <Descriptor>" & sCompanyDescriptor & "</Descriptor>" & VbCrLf & _
			"  </Payment>" & VbCrLf & _
			"  <CreditCardAccount>" & VbCrLf & _
			"   <Holder><![CDATA[" & X_ccHolderName & "]]></Holder>" & VbCrLf & _
			"   <Verification>" & X_ccCVV2 & "</Verification>" & VbCrLf & _
			"   <Brand>" & BrandName(ccTypeID) & "</Brand>" & VbCrLf & _
			"   <Number>" & X_ccNumber & "</Number>" & VbCrLf & _
			"   <Expiry month=""" & X_ccExpMM & """ year=""20" & X_ccExpYY & """ />" & VbCrLf & _
			"  </CreditCardAccount>" & VbCrLf & _
			"  <Customer>" & VbCrLf & _
			"   <Name>" & VbCrLf & _
			"    <Given><![CDATA[" & sName & "]]></Given>" & VbCrLf & _
			"    <Family><![CDATA[" & fName & "]]></Family>" & VbCrLf & _
			"   </Name>" & VbCrLf & _
			"   <Address>" & VbCrLf & _
			"    <Street><![CDATA[" & BACHAddr1 & "]]></Street>" & VbCrLf & _
			"    <Zip>" & BAZipCode & "</Zip>" & VbCrLf & _
			"    <City>" & BACity & "</City>" & VbCrLf & _
			"    <State>" & BAStateCode & "</State>" & VbCrLf & _
			"    <Country>" & BACountryCode & "</Country>" & VbCrLf & _
			"   </Address>" & VbCrLf & _
			"   <Contact>" & VbCrLf & _
			"    <Email>" & X_Email & "</Email>" & VbCrLf & _
			"    <Ip>" & sIP & "</Ip>" & VbCrLf & _
			"   </Contact>" & VbCrLf & _
			"  </Customer>" & VbCrLf & _
			" </Transaction>" & VbCrLf & _
			"</Request>"
	End if		
    On Error Resume Next
		HttpReq.open "POST", UrlAddress, false, sAccountId, sAccountPassword
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		'HttpReq.setRequestHeader "Accept-Language", "en-US"
		HttpReq.send "requestXML=" & ParamList
		sResDetails = HttpReq.responseText
		ParamList = Replace(ParamList, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		ParamList = Replace(ParamList, "<Verification>" & X_ccCVV2, "<Verification>" & GetSafePartialNumber(X_ccCVV2))
		SaveLogChargeAttemptRequestResponse ParamList, sResDetails
	    HttpError = FormatHttpRequestError(HttpReq)
		Set xmlRet = HttpReq.responseXml

		'Response.Write(ParamList & bvCrLf & "<br>" & bvCrLf & sResDetails)
        'Response.End

		If IsNull(sResDetails) Or Len(sResDetails) = 0 Then Call throwError("521")
		sReturnCode = xmlRet.selectSingleNode("//Return").attributes.getNamedItem("code").text
		If sReturnCode = "" Then Call throwError("520") 'internal error

		If sReturnCode = "000.000.000" Then
			sReturnCode = "000"
			X_DebitReferenceCode = xmlRet.selectSingleNode("//ConnectorTxID1").text
			sApprovalNumber = xmlRet.selectSingleNode("//ConnectorTxID2").text
			X_DebitReferenceNum = xmlRet.selectSingleNode("//ConnectorTxID3").text
		ElseIf sReturnCode = "000.200.000" Then
			sReturnCode = "001"
			X_DebitReferenceCode = xmlRet.selectSingleNode("//ConnectorTxID1").text
			sApprovalNumber = xmlRet.selectSingleNode("//ConnectorTxID2").text
			X_DebitReferenceNum = xmlRet.selectSingleNode("//ConnectorTxID3").text
		Else
			sError = xmlRet.selectSingleNode("//Return").text
		End If
    On Error GoTo 0
    'If (Cint(X_TypeCredit) = 0) And (sReturnCode <> "000") Then FileAppendData "PayOn.txt", Now & ", " & sReturnCode & vbcrlf & ParamList & vbcrlf & sResDetails & vbcrlf & vbcrlf & vbcrlf
    Set HttpReq = Nothing
   	DebitIProcessResult sReturnCode, sApprovalNumber
End function
%>