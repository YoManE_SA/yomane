<%
'---------------------------------------------------------------------
'	Get Next Reccuring Charge Date
'---------------------------------------------------------------------
Private Function GetNextChargeTime(RecurringMode, RecurringCycle)
	Select Case RecurringMode
		Case 1	'1 day
			GetNextChargeTime = DateAdd("d", RecurringCycle, date)
		Case 2	'1 week
			GetNextChargeTime = DateAdd("ww", RecurringCycle, date)
		case 3	'1 month
			GetNextChargeTime = DateAdd("m", RecurringCycle, date)
		case 4	'1 year
			GetNextChargeTime = DateAdd("y", RecurringCycle, date)
	End Select
End Function

'---------------------------------------------------------------------
'	insert credit card data
'---------------------------------------------------------------------
function insertCard(cardNumber, ccExpMonth, ccExpYear, chName, payerName, cvv, chPhoneNumber, chEmail, companyId, chPersonalNumber, chDateOfBirth, ccTypeId, comment, billingAddressId, ccBINCountry)
	'
	' 20080207 Tamir
	'
	' Store first 6 and last 4 digits in the dedicated fields (without encryption)
	'
	dim sCardNumber : sCardNumber=Replace(cardNumber, " ", "")
	dim nFirst6, i : nFirst6=0
	for i=1 to Len(sCardNumber)
		if nFirst6<100000 and instr("1234567890", mid(sCardNumber, i, 1))>0 then
			nFirst6=nFirst6*10+mid(sCardNumber, i, 1)
		end if
	next
	dim nLast4 : nLast4=0
	if len(sCardNumber)>3 then nLast4=Right(sCardNumber, 4)

	Dim nBillingAddressID : nBillingAddressID = TestNumVar(billingAddressId, 1, 0, 0)
	If nBillingAddressID = 0 Then nBillingAddressID = "NULL"
	Dim sBinCountry : sBinCountry = TestStrVar(ccBINCountry, 0, 2, "")
	sBinCountry = "CASE WHEN '" & sBinCountry & "' IN (SELECT CountryISOCode FROM [List].[CountryList]) THEN '" & sBinCountry & "' ELSE NULL END"

	sSQL = "SET NOCOUNT ON;INSERT INTO tblCreditCard " &_
		"(CCard_First6, CCard_Last4, BINCountry, CCard_number256, ExpMM, ExpYY, Member," & _
		" cc_cui, phoneNumber, Email, CompanyID, PersonalNumber, CardholderDOB, ccTypeID, Comment," & _
		" BillingAddressId) VALUES (" & TestNumVar(nFirst6, 100000, 999999, 0) & "," & nLast4 & "," & sBinCountry & " ," & _
		IIF(TestStrVar(cardNumber, 0, 25, "") <> "", " dbo.GetEncrypted256('" & Replace(Replace(TestStrVar(cardNumber, 0, 25, ""), " ", ""), "'", "''") & "'),", " null,") & _
		" '" & TestStrVar(ccExpMonth, 0, 10, "") & "', '" & TestStrVar(ccExpYear, 0, 10, "") & "'," & _
		" '" & DBText(TestStrVar(chName, 0, 100, "")) & "', " & _
		" dbo.SetCUI('" & TestStrVar(cvv, 0, 5, "") & "'), '" & TestStrVar(chPhoneNumber, 0, 50, "") & "'," & _
		" '" & TestStrVar(chEmail, 0, 80, "") & "', " & companyId & "," & _
		" '" & TestStrVar(chPersonalNumber, 0, 50, "") & "'," & _
		IIF(chDateOfBirth <> "", "'" & chDateOfBirth & "'", "Null") & "," & _
		ccTypeId & "," & _
		" '" & TestStrVar(comment, 0, 3000, "") & "', " & nBillingAddressID & ") " &_
		"SELECT SCOPE_IDENTITY() AS NewCardID;SET NOCOUNT OFF"
	insertCard = CLng(ExecScalar(sSQL, 0))
end function

Function DebitIProcessResult(dpRet, dpApproval)
	If sDCTest = "YES" Then
		Response.Write "replyCode=" & dpRet
		Response.End
	End If
	TimeControl.Append("A_Process1") 'Time Optimization *********
	If (Trim(dpRet) = "000" Or Trim(dpRet) = "001" Or Trim(dpRet) = "553") Then
		If sCcStorageID = 0 And X_StoreCc Then
			sSQL = "StoreCreditCard " & sCompanyID & ", " & PaymentMethod & ", N'" & DBText(X_PersonalNumber) & "', '" & Replace(Replace(X_ccNumber, " ", ""), "'", "''") & "', '" & X_ccExpMM & "', '" & X_ccExpYY & "', N'" & EncCVV(X_ccCVV2) & "', N'" & X_PhoneNumber & "', N'" & X_Email & "', N'" & DBText(X_ccHolderName) & "', N'" & DBText(X_Comment) & "', N'" & ccTypeEngShow & "', N'" & sIP & "', " & IIF(TestNumVar(BACountryId, 0, -1, 0) > 0, BACountryId, "Null") & ", " & IIF(TestNumVar(BAStateId, 0, -1, 0) > 0, BAStateId, "Null") & ", N'" & BACHAddr1 & "', N'" & BACHAddr2 & "', N'" & BACity & "', N'" & BAZipCode & "';"
			sCcStorageID = ExecScalar("DECLARE @nID int;EXEC @nID=" & sSQL & "SELECT @nID;", 0)
		End If
	End If
	SaveTransactionData PaymentMethod, ccTypeEngShow, X_ccNumber, EncCVV(X_Cvv), X_ExpDate, ccBINCountry
	If Trim(dpRet) = "000" And Request("AutoWalletRegistration") = "1" Then sClient_WalletID = CreateCustomerFromTransaction(PaymentMethod, X_ccNumber, X_Cvv, X_ExpDate)
	If bUseOldTerminal Then DebitIProcessResult = DebitProcessResult(dpRet, dpApproval, nNewCreditCardID) _
	Else DebitIProcessResult = DebitProcessResultIN(dpRet, dpApproval)
End Function

'20110609 Tamir - when relevant, notify the merchant about RiskMailCc violation
Function NotifyMerchantRiskMailCc()
	Dim oMailer : Set oMailer = Server.CreateObject("Persits.MailSender")
	Dim sBody : sBody = "Too many credit cards were attempted to charge with the following email address: " & X_Email
	oMailer.FromName = "Risk"
	oMailer.From = "risk@" &  MAIL_DOMAIN
	oMailer.Host = SMTP_SERVER
	oMailer.Username = SMTP_USERNAME
	oMailer.Password = SMTP_PASSWORD
	oMailer.CharSet = "windows-1255"
	oMailer.IsHTML = False
	oMailer.Priority = 1
	Dim saMails : saMails = Split(sRiskMailCcNotifyEmail, ";")
	Dim i
	For i = LBound(saMails) To UBound(saMails)
		oMailer.AddAddress saMails(i)
	Next
	oMailer.Subject = "Risk Notification"
	oMailer.Body = sBody
	oMailer.Queue = True
	On Error Resume Next
	oMailer.Send
	On Error GoTo 0
	Set oMailer=Nothing
End Function

Function NotifyMerchantRiskCcMail()
	Dim oMailer : Set oMailer = Server.CreateObject("Persits.MailSender")
	Dim sBody : sBody = "Too many emails were attempted to charge with the same card. last attempt email: " & X_Email
	oMailer.FromName = "Risk"
	oMailer.From = "risk@" &  MAIL_DOMAIN
	oMailer.Host = SMTP_SERVER
	oMailer.Username = SMTP_USERNAME
	oMailer.Password = SMTP_PASSWORD
	oMailer.CharSet = "windows-1255"
	oMailer.IsHTML = False
	oMailer.Priority = 1
	Dim saMails : saMails = Split(sRiskMailCcNotifyEmail, ";")
	Dim i
	For i = LBound(saMails) To UBound(saMails)
		oMailer.AddAddress saMails(i)
	Next
	oMailer.Subject = "Risk Notification"
	oMailer.Body = sBody
	oMailer.Queue = True
	On Error Resume Next
	oMailer.Send
	On Error GoTo 0
	Set oMailer=Nothing
End Function
%>