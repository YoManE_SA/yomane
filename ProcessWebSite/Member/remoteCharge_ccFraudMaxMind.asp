<%
	Dim isDuplicateAnswer
	
	'Get correct license key
	licenseKey = "T78SAlrwAhpK"
	
	'Build required fields
	sendIp = left(sIP,15)
	sendCity = left(Server.UrlEncode(BACity & ""),20)
	sendRegion = left(Server.UrlEncode(BAStateCode & ""),2)
	sendpostal = left(Server.UrlEncode(BAZipCode & ""),10)
	sendCountry = left(Server.UrlEncode(BACountryCode & ""),2)
	QueryString = "license_key=" & licenseKey & "&i=" & sendIp & "&city=" & sendCity & "&region=" & sendRegion & "&postal=" & sendpostal & "&country=" & sendCountry
		
	'Build recommended fields
	If trim(X_Email)<>"" Then
		sLenTmp = int(Len(trim(X_Email))) - int(Instr(trim(X_Email), "@"))
		X_EmailTmp = right(trim(X_Email), sLenTmp)
		sendDomain = left(X_EmailTmp,30)
		QueryString = QueryString & "&domain=" & sendDomain
	End If
	If len(X_ccNumber)>=6 Then
		sendBin = left(replace(X_ccNumber," ",""),6)
		QueryString = QueryString & "&bin=" & sendBin
	End If

	'-------------------------------------------------------------------------------
	'   Check if system already has risk information stored
	'-------------------------------------------------------------------------------
	sSQL = "SELECT TOP 1 returnAnswer, ReturnExplanation, FraudDetection_id" &_
	" FROM [Log].[FraudDetection] WHERE [InsertDate] >= DATEADD(WEEK,-1,SYSDATETIME()) AND isDuplicateAnswer = 0 AND returnScore > -1 AND IsNull(SendingIp, '') = '" & dbText(sendIp) & "'" &_
	" AND IsNull(SendingCity, '') = '" & dbText(sendCity) & "' AND IsNull(SendingRegion, '') = '" & dbText(sendRegion) & "' AND IsNull(SendingPostal, '') = '" & dbText(sendpostal) & "'" &_
	" AND IsNull(SendingCountry, '') = '" & dbText(sendCountry) & "' AND IsNull(SendingDomain, '') = '" & dbText(sendDomain) & "' AND IsNull(SendingBin, '') = '" & dbText(sendBin) & "'"
	Set rsData5 = oledbData.execute(sSQL)

	If NOT rsData5.EOF Then
		'Get data already stored in the system
		mxmRet = Replace(Replace(rsData5("returnAnswer"), ";", "&"), "& ", "&")
		nRetAnswer = Replace(UrlWithout(mxmRet, "explanation"), "&", "; ")
		nReturnExplanation = rsData5("ReturnExplanation")
		fraudDetectionLog_id = rsData5("FraudDetection_id")
		rsData5.Close
		isDuplicateAnswer = 1
	Else
		'Send data to maxmind and retrive response
		fraudDetectionLog_id = 0
		rsData5.Close
		isDuplicateAnswer = 0
		Dim HttpReq, mxmRet
		Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
		HttpReq.setTimeouts 1.5*1000, 1.5*1000, 2.5*1000, 7.5*1000 'lResolve, lConnect, lSend, lReceive
		On Error Resume Next
			HttpReq.open "POST", "https://minfraud3.maxmind.com/app/ccv2r?" & QueryString, False	'https://www.maxmind.com/app/ccv2r
			HttpReq.Send
			mxmRet = Replace(HttpReq.responseText, ";", "&")
			nRetAnswer = Replace(UrlWithout(mxmRet, "explanation"), "&", "; ")
			nReturnExplanation = "<span class=""key"">explanation</span> = " & GetURLValue(mxmRet, "explanation") & "; "
			If Len(mxmRet) < 1 Then isProceed = True
		On Error Goto 0
	End if
	Set rsData5 = nothing
	
	'Get specific data
	sFreeMail = GetURLValue(mxmRet, "freeMail")
	sHighRiskCountry = GetURLValue(mxmRet, "highRiskCountry")
	nRetScore = GetURLValue(mxmRet, "score")
	nRetRiskScore = GetURLValue(mxmRet, "riskScore")
	sBinCountry = GetURLValue(mxmRet, "binCountry")
	nRetReference = GetURLValue(mxmRet, "maxmindID")
	nReturnQueriesRemaining = GetURLValue(mxmRet, "queriesRemaining")
	sIPCountryCode = GetURLValue(mxmRet, "countryCode")
	If nRetScore = "" Then
		nRetScore = -1
		nRetRiskScore = -1
	End if

	'-------------------------------------------------------------------------------
	'	Change return score if needed
	'-------------------------------------------------------------------------------
	IsTemperScore = false
	If bIsAllowFreeMail AND UCase(sFreeMail) = "YES" Then
		IsTemperScore = true
		nRetScore = nRetScore-2.5
	End if

	'20100602 Tamir - if the country is highrisk and its in whitelist - mark as country-whitelisted
	nIsCountryWhitelisted = 0
	If UCase(sHighRiskCountry) = "YES" Then
		If IsCountryInList(sBinCountry, sCountryWhiteList) Or IsCountryInList(sIPCountryCode, sCountryWhiteList) Then
			nRetScore = nRetScore - 2.5
			nIsCountryWhitelisted = 1
		End If
	End If

	'-------------------------------------------------------------------------------
	'   Determine if should proceed to charge
	'-------------------------------------------------------------------------------
	isProceed = true
	
	'Check risk score
	If isProceed Then
		'old score (1..10)
  		If nRetScore<>"" And CDbl(nMaxMind_MinScore) >= 0 Then
			If IsNumeric(nRetScore) Then
				If CDbl(nRetScore) > CDbl(nMaxMind_MinScore) Then
					isProceed = false
					nReply = "580"
				End if
			End if
		End if	
		'new score (1..100)
  		If nRetRiskScore<>"" And CDbl(nRiskScoreAllowed) >= 0 Then
			If IsNumeric(nRetRiskScore) Then
				If CDbl(nRetRiskScore) - CDbl(nRiskScoreAllowed) > 0 Then
					isProceed = false
					nReply = "580"
				End if
			End if
		End if	
	End if
	
	'-------------------------------------------------------------------------------
	'   Insert into fraud detection log table
	'-------------------------------------------------------------------------------
	If cInt(X_TypeCredit) = 0 Then
		X_TmpAmount = -X_Amount
	Else
		X_TmpAmount = X_Amount
	End if
	
	fraudDetectionLog_id = 0
	sSQL = "SET NOCOUNT ON;INSERT INTO [Log].[FraudDetection] " & _
	" (sendingString, SendingIp, SendingCity, " & _
	" SendingRegion, SendingPostal, SendingCountry, SendingDomain, SendingBin, returnAnswer, ReturnExplanation, " & _
	" returnBinCountry, referenceCode, ReturnQueriesRemaining, " & _
	" replyCode, PaymentMethodDisplay, BinCountry, returnScore, returnRiskScore, allowedScore, allowedRiskScore, " & _
	" IsTemperScore, isProceed, isDuplicateAnswer, transAmount, transCurrency, IsCountryWhitelisted, Merchant_id)VALUES( " & _
	" " & IIF(Trim(QueryString) <> "", "Left('" & dbText(QueryString) & "',1500)", "NULL") & " ," & _
	" " & IIF(Trim(sendIp) <> "", "Left('" & dbText(sendIp) & "',15)", "NULL") & " ," & _
	" " & IIF(Trim(sendCity) <> "", "Left('" & dbText(sendCity) & "',20)", "NULL") & " ," & _
	" " & IIF(Trim(sendRegion) <> "", "Left('" & dbText(sendRegion) & "',2)", "NULL") & " ," & _
	" " & IIF(Trim(sendpostal) <> "", "Left('" & dbText(sendpostal) & "',10)", "NULL") & " ," & _
	" " & IIF(Trim(sendCountry) <> "", "Left('" & dbText(sendCountry) & "',2)", "NULL") & " ," & _
	" " & IIF(Trim(sendDomain) <> "", "Left('" & dbText(sendDomain) & "',30)", "NULL") & " ," & _
	" " & IIF(Trim(sendBin) <> "", "Left('" & dbText(sendBin) & "',6)", "NULL") & " ," & _
	" " & IIF(Trim(nRetAnswer) <> "", "Left('" & dbText(nRetAnswer) & "',1500)", "NULL") & " ," & _
	" " & IIF(Trim(nReturnExplanation) <> "", "Left('" & dbText(nReturnExplanation) & "',1500)", "NULL") & " ," & _
	" " & IIF(Trim(sBinCountry) <> "", "Left('" & dbText(sBinCountry) & "',2)", "NULL") & " ," & _
	" " & IIF(Trim(nRetReference) <> "", "Left('" & dbText(nRetReference) & "',30)", "NULL") & " ," & _
	" " & IIF(Trim(nReturnQueriesRemaining) <> "", "Left('" & dbText(nReturnQueriesRemaining) & "',5)", "NULL") & " ," & _
	" " & IIF(Trim(nReply) <> "", "Left('" & dbText(nReply) & "',5)", "NULL") & " ," & _
	" " & IIF(Trim(ccTypeEngShow) <> "", "Left('" & dbText(ccTypeEngShow) & "',50)", "NULL") & " ," & _
	" " & IIF(Trim(ccBINCountry) <> "", "Left('" & dbText(ccBINCountry) & "',2)", "NULL") & " ," & _
	" " & nRetScore & "," & nRetRiskScore & ", " & nMaxMind_MinScore & ", " & nRiskScoreAllowed & ", " & _
	" " & CInt(IsTemperScore) & ", " & CInt(isProceed) & ", " & isDuplicateAnswer & "," & X_TmpAmount & ", " & CInt(X_Currency) & ", " & _
	" " & nIsCountryWhitelisted & ", " & sCompanyID & " ); " & _
	" SELECT @@IDENTITY;SET NOCOUNT OFF;"
	'Response.Write(sSQL) :	Response.End
	fraudDetectionLog_id = ExecScalar(sSQL, 0)

	'-------------------------------------------------------------------------------
	'   Did not pass fraud check, insert to fail table
	'-------------------------------------------------------------------------------
	If NOT isProceed Then
		
        Call throwError("580")

		'Insert billing address
		'newBillingAddressId = 0
		'if TestNumVar(NewBillingAddressID, 0, -1, 0) = 0 then
			'on error resume next
			'NewBillingAddressID=insertBillingAddress(BACountryCode, BAStateCode, BACountryId, BAStateId, BACity, BACHAddr1, BACHAddr2, BAZipCode)
			'on error goto 0
		'end if
		
		'Insert credit card details
		'if TestNumVar(nNewCreditCardID, 0, -1, 0) = 0 then
			'on error resume next
			'nNewCreditCardID=insertCard(sCCardNum_Encrypt, X_ccExpMM, X_ccExpYY, X_ccHolderName, X_PayerName, X_ccCVV2, X_PhoneNumber, X_Email, sCompanyID, X_PersonalNumber, X_DateOfBirth, ccTypeID, X_Comment, newBillingAddressId, ccBINCountry)
			'on error goto 0
		'end if
		'newCardId=nNewCreditCardID
		
		'Insert to failed table
		'transactionIdOutParam = ""
		'insertDateOutParam = ""
		'SaveTransactionData PaymentMethod, ccTypeEngShow, X_ccNumber, EncCVV(X_Cvv), X_ExpDate, sBinCountry
		'call insertFailedTransaction (transactionIdOutParam, insertDateOutParam, sCompanyID, fraudDetectionLog_id, sTransactionTypeID, X_TransType, X_TypeCredit, X_Customer, sIP, X_Amount, X_Currency, ccTypeEngShow, X_Payments, X_OrderNumber, nReply, "", X_PayFor, X_ProductId, X_Comment, sTerminalNumber, PaymentMethod, 1, newCardId, isTestOnly, referringUrl, "1", X_PayerInfoId, X_PaymentMethodId, nPayerID, X_Is3dSecure, X_deviceId)
		'nTransactionID = transactionIdOutParam
		'nInsertDate = insertDateOutParam
		'UpdateMaxMindTransID "TransFail_id", nTransactionID, nReply

		'CancelCartTransaction nTransactionID
		'RemoveParallelChargeAttempt
		
		'Call fn_returnResponse(LogSavingChargeDataID,requestSource,"1",X_TransType,nReply,nTransactionID,nInsertDate,X_OrderNumber,formatnumber(X_Amount,2,-1,0,0),X_Payments,X_Currency,sApprovalNumber,X_Comment)
	End if

	Sub UpdateMaxMindTransID(fieldName, value, replyCode) 
		ExecSql("Update [Log].[FraudDetection] Set " & fieldName & "=" & value & ", replyCode='" & replyCode & "' Where FraudDetection_id=" & fraudDetectionLog_id)
	End Sub
%>