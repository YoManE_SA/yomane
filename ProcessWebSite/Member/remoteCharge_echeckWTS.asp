<%
'---------------------------------------------------------------------
'	Debit eTriton
'---------------------------------------------------------------------
function debit_WTS()
	If Trim(X_Currency) <> 1 Then ThrowError "531"
	Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer

	xNames = Split(Trim(X_ccHolderName), " ")
	if Ubound(xNames) < 1 Then Call throwError("517")
	fName = xNames(0) : sName = xNames(1)
	
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
	UrlAddress = "https://join.achbill.com/cgi-bin/man_trans.cgi"
	if (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	End if
	X_DebitReferenceCode = GetTransRefCode()

	Dim sActionCode : sActionCode = "P"
	If X_TypeCredit = 0 Then sActionCode = "R"
	
	Dim sCreditFlag : sCreditFlag = "0"
	If X_Amount<0 Then sCreditFlag= "1"
	ParamList = "parent_id=WTS01&pmt_type=chk&currency=US&billing_cycle=-1" & _
	"&merordernumber=" & X_OrderNumber & _
	"&action_code=" & sActionCode & _
	"&ip_forward=" & sIP & _
	"&credit_flag=" & sCreditFlag & _
	"&initial_amount=" & Abs(X_Amount) & _
	"&sub_id=" & sTerminalNumber & _
	"&profile_id=" & sAccountID & _
	"&acct_name=" & X_ccHolderName & _
	"&Reference=" & X_DebitReferenceCode & _
	"&chk_acct=" & ckacct & _
	"&chk_aba=" & ckaba & _
	"&custname=" & X_ccHolderName & _
	"&custssn=" & X_PersonalNumber & _
	"&custphone=" & X_PhoneNumber & _
	"&custzip=" & BAZipCode & _
	"&birth_date=" & X_BirthDate & _
	"&custcity=" & BACity & _
	"&custstate=" & BAState & _
	"&custaddress1=" & BACHAddr1 & _
	"&custaddress2=" & BACHAddr2 & _
	"&custemail=" & X_Email
	Dim sReturn : sReturn = ""
	On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded;"
		HttpReq.Send ParamList
		sReturn = HttpReq.responseText
		sReturn = Replace(Replace(sReturn, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba))
		SaveLogChargeAttemptRequestResponse Replace(Replace(ParamList, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba)), sReturn
		HttpError = FormatHttpRequestError(HttpReq)
	On Error GoTo 0
	If Len(sReturn) =0 Then ThrowError "521"
	sReturn=Replace(sReturn, chr(10), "&")
	
	sDebitReturnAnswer = Left(sReturn, InStr(sReturn, "PostedVars=BEGIN") - 1)
	sStatus = UCase(GetURLValue(sReturn, "status"))
	If sStatus = "ACCEPTED" Then
		sReturnCode = "001"
	Else
		sReturnReason = DBText(GetURLValue(sReturn, "reason"))
		If sReturnReason = "" Then
			sReturnCode = "002"
		Else
			sReturnCode = ExecScalar("SELECT TOP 1 Code FROM tblDebitCompanyCode WHERE DebitCompanyID=36 AND DescriptionOriginal='" & sReturnReason & "'", "")
			If sReturnCode = "" Then
				sReturnCode = ExecScalar("SELECT Count(*) FROM tblDebitCompanyCode WHERE DebitCompanyID=36", 0) + 1
				sSQL = "INSERT INTO tblDebitCompanyCode(DebitCompanyID, FailSource, ChargeFail, Code, DescriptionOriginal," & _
				" DescriptionMerchantEng, DescriptionMerchantHeb, DescriptionCustomerEng, DescriptionCustomerHeb) VALUES (36, 0, 1," & _
				" '" & sReturnCode & "'," & _
				" '" & sReturnReason & "', '" & sReturnReason & "', '" & sReturnReason & "', '" & sReturnReason & "', '" & sReturnReason & "')"
				oledbData.Execute sSQL
			End If
		End If
	End If
	sApprovalNumber = GetURLValue(sReturn, "history_id")

	DebitIProcessResult sReturnCode, sApprovalNumber
	Set xmlRet = Nothing
	Set HttpReq = Nothing
End function
%>