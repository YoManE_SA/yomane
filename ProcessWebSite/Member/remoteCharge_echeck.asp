<%
Dim nTransactionID, nInsertDate, X_DPRET
%>
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/timer_class.asp"-->
<!--#include file="../include/MerchantTerminals.asp" -->
<!--#include file="remoteCharge_Functions.asp"-->
<%
Dim X_PaymentMethodId, X_PayerInfoId
Dim sTermName, sAccountId, sAccountSubId, sAccountPassword, sAccountId3D, sAccountSubId3D, sAccountPassword3D, X_RetURL
Dim debitCompany, bIsShvaMasterTerminal, nProcessingMethod, sMcc, isManipulateAmount, isTestTerminal, sClient_WalletID
Dim X_RefTransID, X_DebitReferenceCode, X_DebitReferenceNum, X_ApprovalNumber, ccTypeEngShow, X_Payments, isTestOnly, ccTypeID
Dim X_TerminalHasNext, X_TransINC, X_3dRedirect, X_3dRedirectMethod, X_CustomErrorDesc, X_SiteURL, X_deviceId

isLocalTransaction = cBool(request("isLocalTransaction"))
isLocalRequest = cBool(Trim(Request.ServerVariables("LOCAL_ADDR")) = Request.ServerVariables("REMOTE_ADDR") OR Trim(Request.ServerVariables("REMOTE_ADDR")) = "80.179.180.62" OR Trim(Request.ServerVariables("LOCAL_ADDR")) = "192.168.5.11")
canChargeLocally = cBool(isLocalTransaction AND isLocalRequest)

'20100530 Tamir - when capturing pre-auth. transaction from recurring series, copy RecurringSeries and RecurringCharge to the passed transaction
nApprovalRecurringSeries = "NULL"
nApprovalRecurringChargeNumber = "NULL"

companyNum = DBText(request("CompanyNum"))
sIP = DBText(request("ClientIP"))
If sIP = "" Then sIP = Request.ServerVariables("REMOTE_ADDR")

X_Payments = 1
isTestOnly = 0
PaymentMethod = TestNumVar(request("PaymentMethod"), PMD_EC_MIN, PMD_EC_MAX, PMD_EC_CHECK)
requestSource = DBText(request("RequestSource"))
If requestSource = "" Then requestSource = "8"
fraudDetectionLog_id = 0 : X_TransType = 0 : sTransactionTypeID = requestSource
X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, MAX_CURRENCY + 1)
X_RefTransID = TestNumVar(request("RefTransID"), 0, -1, 0)
IsCheckPMData = True
Set iRs = oleDbData.Execute("Select Name, IsPMInfoMandatory From List.PaymentMethod Where PaymentMethod_id = " & PaymentMethod)
If Not iRs.EOF Then
	ccTypeEngShow = iRs("Name")
	IsCheckPMData = iRs("IsPMInfoMandatory")
End If
iRs.Close
If ccTypeEngShow = "" Then ccTypeEngShow = "eCheck"
X_deviceId = TestNumVar(Request("deviceId"), 0, -1, "")

'---------------------------------------------
'   save charge attempt log
'---------------------------------------------
LogSavingChargeDataID = 0
LogSavingChargeDataID = Sub_LogSavingChargeData(requestSource, companyNum, sIP)

'---------------------------------------------
'	Get and secure input data
'---------------------------------------------
referringUrl = DBText(request("ReferringUrl"))
If trim(referringUrl) = "" Then referringUrl = DBText(Request.ServerVariables("HTTP_REFERER"))
X_TransType = TestNumVar(request("TransType"), 0, 4, 0)
ccTypeID = 10

Set rsData2 = oledbData.execute("SELECT ID, HashKey FROM tblCompany WHERE CustomerNumber='" & companyNum & "'")
If rsData2.EOF Then
	rsData2.Close(): Call throwError("500") 'Merchant not found
End if
sCompanyID = rsData2("ID")
X_MerchantHashKey = rsData2("HashKey")
rsData2.Close()

'If trim(X_TransType)="2" Then ' 31/03/2010 Udi added the echeck approval only
'	'----------------------------------------------------------------------
'	'	Collecting data from pre-auth transaction
'	'----------------------------------------------------------------------
'	sTransactionTypeID = "17"
'	X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, -1)
'	sApprovalOnlyID = TestNumVar(request("TransApprovalID"), 1, 0, 0)
'	If sApprovalOnlyID < 1 Then Call throwError("536")
'	X_Amount = TestNumVar(request("Amount"), 0, -1, 0)
'	bIsUseMaxMind = false 'Overide merchant configuration
'	bIsCheckDailyLimts = False
'
'	sSQL="SELECT IsNull(RecurringSeries, 0) RecurringSeries, IsNull(RecurringChargeNumber, 0) RecurringChargeNumber, Amount, Currency," & _
'	" Payments, CreditType, OrderNumber, t.Comment, DebitReferenceCode, approvalNumber, TerminalNumber," & _
'	" dbo.GetDecrypted256(accountNumber256) accountNumber, dbo.GetDecrypted256(routingNumber256) routingNumber, accountName, BirthDate, bankName, bankCity, bankPhone, bankState, bankCountry, bankAccountTypeId, " & _
'	" PersonalNumber, phoneNumber, Email, address1, address2, city," & _
'	" zipCode, tblBillingAddress.stateIso, tblBillingAddress.countryIso, BillingAddressID, PaymentMethodID FROM tblCompanyTransApproval t" & _
'	" INNER JOIN tblCheckDetails ON CheckDetailsID=tblCheckDetails.ID Left JOIN tblBillingAddress ON BillingAddressId=tblBillingAddress.ID" & _
'	" WHERE t.ID=" & sApprovalOnlyID & " AND t.CompanyID=" & sCompanyID & " AND Currency=" & X_Currency
'	set rsData3 = oledbData.execute(sSQL)
'	If rsData3.EOF Then
'		rsData3.Close
'		Call throwError("536")
'	ElseIf X_Amount > rsData3("Amount") Then
'		rsData3.Close
'		Call throwError("538")
'	Else
'		'nExistingBillingAddressID=TestNumVar(rsData3("BillingAddressID"), 1, 0, 0)
'		'nExistingCreditCardID=TestNumVar(rsData3("BillingAddressID"), 1, 0, 0)
'		If X_Amount = 0 Then X_Amount = rsData3("Amount")
'	    X_OCurrency = X_Currency
'       X_OAmount = X_Amount
'    	RecurringSeries = rsData3("RecurringSeries")
'    	RecurringChargeNumber = rsData3("RecurringChargeNumber")
'    	nApprovalRecurringSeries = "NULL"'
'		X_OrderNumber = rsData3("OrderNumber")
'		X_PersonalNumber = trim(rsData3("PersonalNumber"))
'		X_PhoneNumber = trim(rsData3("phoneNumber"))
'		X_Email = trim(rsData3("Email"))
'		X_Payments = TestNumVar(rsData3("Payments"), 0, -1, 1)
'		X_ConfirmNumber = trim(rsData3("approvalNumber"))
'		X_Customer = 0
'		X_TypeCredit = TestNumVar(rsData3("CreditType"), 0, 8, -1)
'		X_Comment = trim(rsData3("Comment"))
'		X_DebitReferenceCode = trim(rsData3("DebitReferenceCode"))
'		sTerminalNumber = trim(rsData3("TerminalNumber"))
'		BACHAddr1 = trim(rsData3("address1"))
'		BACHAddr2 = trim(rsData3("address2"))
'		BACity = trim(rsData3("city"))
'		BAZipCode = trim(rsData3("zipCode"))
'		BAState = trim(rsData3("stateIso"))
'		BACountry = trim(rsData3("countryIso"))
'
'       X_ccHolderName = trim(rsData3("AccountName"))
'       ckaba = rsData3("RoutingNumber") : ckabaEnc = ckaba
'       ckacct = rsData3("AccountNumber") : ckacctEnc = ckacct
'       ckacctLast4 = Right(ckacct, 4)
'       bankAccountType = DBText(trim(rsData3("bankAccountTypeId")))
'       X_BankName = DBText(trim(rsData3("BankName")))
'       X_BankCity = DBText(trim(rsData3("BankCity")))
'       X_BankPhone = DBText(trim(rsData3("BankPhone")))
'       X_BankState = DBText(trim(rsData3("BankState")))
'       X_BankCountry = DBText(trim(rsData3("BankCountry")))
'       X_BirthDate = DBText(trim(rsData3("BirthDate")))
'	    ' 20090706 Tamir - when capturing pre-auth. transaction from recurring series, copy RecurringSeries and RecurringCharge to the passed transaction
'		if sTerminalNumber <> "" Then
'			bIsCheckTerminalUsabe = false
'			sTerminalChargeOptions = rsData3("Currency")
'			CCF_ID = 0
'		End If
'	End if
'	rsData3.close
'	Set rsData3 = nothing
'Else

If trim(X_TransType)="4" Then ' 07/04/2010 Udi added the echeck load from pending
	sTransactionTypeID = "17"
	X_Currency = TestNumVar(request("Currency"), 0, MAX_CURRENCY, -1)
	X_RefTransID = TestNumVar(request("RefTransID"), 0, -1, 0)
	X_ConfirmNumber = request("TransApprovalID")
	If X_RefTransID < 1 And X_ConfirmNumber = "" Then Call throwError("536")
	X_Amount = TestNumVar(request("Amount"), 0, -1, 0)
	bIsUseMaxMind = false 'Overide merchant configuration
	bIsCheckDailyLimts = False

	sSQL="SELECT t.ID, trans_Amount, trans_Currency," & _
	" trans_Payments, trans_CreditType, OrderNumber, t.Comment, DebitReferenceCode, DebitApprovalNumber, TerminalNumber," & _
	" dbo.GetDecrypted256(accountNumber256) accountNumber, dbo.GetDecrypted256(routingNumber256) routingNumber, accountName, BirthDate, bankName, bankCity, bankPhone, bankState, bankCountry, bankAccountTypeId, " & _
	" PersonalNumber, phoneNumber, Email, address1, address2, city," & _
	" zipCode, tblBillingAddress.stateIso, tblBillingAddress.countryIso, BillingAddressID, PaymentMethod, PaymentMethodID FROM tblCompanyTransPending t" & _
	" INNER JOIN tblCheckDetails ON CheckDetailsID=tblCheckDetails.ID Left JOIN tblBillingAddress ON BillingAddressId=tblBillingAddress.ID" & _
	" WHERE " & IIF(X_ConfirmNumber <> "", " t.DebitApprovalNumber='" & X_ConfirmNumber & "'", " t.ID=" & X_RefTransID) & " AND t.CompanyID=" & sCompanyID
	
	set rsData3 = oledbData.execute(sSQL)
	If rsData3.EOF Then
		rsData3.Close
		Call throwError("536")
	ElseIf X_Amount > rsData3("trans_Amount") Then
		rsData3.Close
		Call throwError("538")
	Else
		'nExistingBillingAddressID=TestNumVar(rsData3("BillingAddressID"), 1, 0, 0)
		'nExistingCreditCardID=TestNumVar(rsData3("BillingAddressID"), 1, 0, 0)
		If X_Amount = 0 Then X_Amount = rsData3("trans_Amount")
		PaymentMethod = TestNumVar(rsData3("PaymentMethod"), PMD_EC_MIN, PMD_EC_MAX, PMD_EC_CHECK)
		X_Currency = rsData3("trans_Currency")
	    X_OCurrency = X_Currency
        X_OAmount = X_Amount
    	RecurringSeries = "NULL"
    	nApprovalRecurringSeries = "NULL"
    	X_RefTransID = rsData3("ID")
		X_OrderNumber = rsData3("OrderNumber")
		X_PersonalNumber = trim(rsData3("PersonalNumber"))
		X_PhoneNumber = trim(rsData3("phoneNumber"))
		X_Email = trim(rsData3("Email"))
		X_Payments = TestNumVar(rsData3("trans_Payments"), 0, -1, 1)
		X_ConfirmNumber = trim(rsData3("DebitApprovalNumber"))
		X_Customer = 0
		X_TypeCredit = TestNumVar(rsData3("trans_CreditType"), 0, 8, -1)
		X_Comment = trim(rsData3("Comment"))
		X_DebitReferenceCode = trim(rsData3("DebitReferenceCode"))
		sTerminalNumber = trim(rsData3("TerminalNumber"))
		BACHAddr1 = trim(rsData3("address1"))
		BACHAddr2 = trim(rsData3("address2"))
		BACity = trim(rsData3("city"))
		BAZipCode = trim(rsData3("zipCode"))
		BAState = trim(rsData3("stateIso"))
		BACountry = trim(rsData3("countryIso"))

        X_ccHolderName = trim(rsData3("AccountName"))
        ckaba = rsData3("RoutingNumber") : ckabaEnc = ckaba
        ckacct = rsData3("AccountNumber") : ckacctEnc = ckacct
        ckacctLast4 = Right(ckacct, 4)
        bankAccountType = DBText(trim(rsData3("bankAccountTypeId")))
        X_BankName = DBText(trim(rsData3("BankName")))
        X_BankCity = DBText(trim(rsData3("BankCity")))
        X_BankPhone = DBText(trim(rsData3("BankPhone")))
        X_BankState = DBText(trim(rsData3("BankState")))
        X_BankCountry = DBText(trim(rsData3("BankCountry")))
        X_BirthDate = DBText(trim(rsData3("BirthDate")))
		' 20090706 Tamir - when capturing pre-auth. transaction from recurring series, copy RecurringSeries and RecurringCharge to the passed transaction
		if sTerminalNumber <> "" Then
			bIsCheckTerminalUsabe = false
			sTerminalChargeOptions = rsData3("trans_Currency")
			CCF_ID = 0
		End If
	End if
	rsData3.close
	Set rsData3 = nothing
Else
    X_ccHolderName = DBText(trim(request("AccountName")))
    ckaba = DBText(request("RoutingNumber"))
    ckabaEnc = ckaba
    ckacct = DBText(request("AccountNumber"))
    ckacctEnc = ckacct
    ckacctLast4 = Right(ckacct, 4)

    bankAccountType = DBText(TestNumVar(Request("bankAccountType"), 1, 2, 1))
    X_BankID = DBText(trim(request("BankID")))
    X_BankName = DBText(trim(request("BankName")))
    X_BankCity = DBText(trim(request("BankCity")))
    X_BankPhone = DBText(trim(request("BankPhone")))
    X_BankState = DBText(trim(request("BankState")))
    X_BankCountry = DBText(trim(request("BankCountry")))
    X_BirthDate = DBText(trim(request("BirthDate")))

    X_Amount = TestNumVar(request("Amount"), 0, -1, 0)
    X_Currency = TestNumVar(request("Currency"), 0, -1, 1)
    X_OCurrency = X_Currency
    X_OAmount = X_Amount

    X_PhoneNumber = DBText(request("PhoneNumber"))
    X_PersonalNumber = DBText(request("PersonalNum"))
    X_Email = DBText(request("Email"))
    X_Customer = TestNumVar(Request("CustomerId"), 0, -1, 0)
    replyUrl = DBText(request("ReplyUrl"))
    X_Comment = DBText(request("Comment"))
    X_OrderNumber = DBText(request("Order"))
    If Request("TypeCredit") <> "" And Request("CreditType") = "" Then X_TypeCredit = TestNumVar(Request("TypeCredit"), 0, 8, 1) _
    Else X_TypeCredit = TestNumVar(Request("CreditType"), 0, 8, 1)
    X_RetURL = DBText(request("RetURL"))

    BACHAddr1 = DBText(request("BillingAddress1"))
    BACHAddr2 = DBText(request("BillingAddress2"))
    BACity = DBText(request("BillingCity"))
    BAZipCode = DBText(request("BillingZipCode"))
    BAState = DBText(request("BillingState"))
    BACountry = DBText(request("BillingCountry"))
    If X_BankCountry = "" And BACountry <> "" Then X_BankCountry = BACountry

    If trim(X_TypeCredit)="0" And (X_RefTransID = 0) Then Call throwError("532") 'refund request must include original trans id
	If trim(X_TypeCredit)="0" And (X_RefTransID <> 0) Then
		'Load Original Trans Details From X_RefTransID
		bIsValidateEmail = False
		Dim RefOAmount
		If X_TransType = "1" Then
			Set rsData3 = oledbData.execute("Select ID, DebitReferenceCode, DebitReferenceNum, ApprovalNumber, TerminalNumber, Currency, Amount, '0' as deniedStatus From tblCompanyTransApproval Where ID=" & X_RefTransID & " And companyID=" & sCompanyID)
		Else
			Set rsData3 = oledbData.execute("Select t.ID, t.InsertDate, t.DebitReferenceCode, t.DebitReferenceNum, t.AcquirerReferenceNum, t.ApprovalNumber, t.TerminalNumber, t.Currency, t.Amount, t.deniedStatus, " & _
				" dbo.GetDecrypted256(tblCheckDetails.RoutingNumber256) as RoutingNumber, dbo.GetDecrypted256(tblCheckDetails.AccountNumber256) as AccountNumber ,tblCheckDetails.*, tblBillingAddress.* " & _
				" From tblCompanyTransPass t" & _
				" Left Join tblCheckDetails ON(t.ECheckID = tblCheckDetails.ID) " & _
				" Left Join tblBillingAddress ON(tblCheckDetails.BillingAddressId = tblBillingAddress.ID) " & _
				" Where t.ID=" & X_RefTransID & " And t.CompanyID=" & sCompanyID & " And t.deniedStatus Not IN(5, 6) Order By t.ID Asc")
		End If
		If Not rsData3.EOF Then
			X_DebitReferenceCode = rsData3("DebitReferenceCode")
            X_DebitReferenceNum = rsData3("DebitReferenceNum")
            X_AcquirerReferenceNum = rsData3("AcquirerReferenceNum")
			X_RefTransInsertDate = rsData3("InsertDate")
			X_ConfirmNumber = rsData3("ApprovalNumber")
			sTerminalNumber = rsData3("TerminalNumber")
			RefOAmount = rsData3("Amount")
			OriginalTransID = rsData3("ID")
			OriginalTransDeniedStatus = rsData3("deniedStatus")

			If X_Amount = 0 Then X_Amount = rsData3("Amount")
			If X_Currency = MAX_CURRENCY + 1 Then X_Currency = rsData3("Currency")
			'try to complete transaction details.
			If ckaba = "" Then ckaba = rsData3("RoutingNumber")
			If ckacct = "" Then ckaba = rsData3("AccountNumber")

			If X_ccHolderName = "" Then X_ccHolderName = Trim(rsData3("AccountName"))
			If X_PersonalNumber = "" Then X_PersonalNumber = Trim(rsData3("PersonalNumber"))
			If X_PhoneNumber = "" Then X_PhoneNumber = Trim(rsData3("PhoneNumber"))
			If X_Email = "" Then X_Email = Trim(rsData3("Email"))

            if (Request("bankAccountType") = "") Then bankAccountType = trim(request("BankAccountTypeId"))
            If X_BankCity = "" Then X_BankCity = DBText(trim(request("BankName")))
            If X_BankCity = "" Then X_BankCity = DBText(trim(request("BankCity")))
            If X_BankPhone = "" Then X_BankPhone = DBText(trim(request("BankPhone")))
            If X_BankState = "" Then X_BankState = DBText(trim(request("BankState")))
            If X_BankCountry = "" Then X_BankCountry = DBText(trim(request("BankCountry")))
            If X_BirthDate = "" Then X_BirthDate = DBText(trim(request("BirthDate")))

			If BACHAddr1 = "" Then BACHAddr1 = rsData3("address1")
			If BACHAddr2 = "" Then BACHAddr2 = rsData3("address2")
			If BACity = "" Then BACity = rsData3("city")
			If BAZipCode = "" Then BAZipCode = rsData3("zipCode")
			If BAState = "" Then BAState = rsData3("stateId")
			If BACountry = "" Then 
				BACountry = rsData3("countryId")
				If Trim(BACountry) = "0" Then BACountry = ""
			End If

			'RefOAnswer = rsData3("Answer") '(answer) no longer exist in trans tables, RefOAnswer was used only for JCC2
			if sTerminalNumber <> "" Then
				sTerminalChargeOptions = rsData3("Currency")
				CCF_ID = 0
				bIsCheckTerminalUsabe = false
				bIsCheckBlockCCNum = false
				bIsCheckCommonBlock = false
			End if	
		End if
		rsData3.Close
		Set rsData3 = Nothing
		If X_TransType = "1" Then
			'RefAmount = TestNumVar(ExecScalar("Select Sum(Amount) From tblCompanyTransApproval Where CreditType=0 And OriginalTransID=" & X_RefTransID, 0), 0, -1, 0) + X_Amount
		Else
			RefAmount = TestNumVar(ExecScalar("Select Sum(OAmount) From tblCompanyTransPass Where CreditType=0 And DeniedStatus <> 5 And OriginalTransID=" & X_RefTransID, 0), 0, -1, 0) + X_Amount
		End If
		if RefAmount-RefOAmount>0 Then throwError("533")
    End If
End If
'---------------------------------------------
'   validation
'---------------------------------------------
'empty field validation

'(ckaba = "") or 
isMissingData = (X_Amount = "") or (companyNum = "") or (X_Currency = "")
If isMissingData Then call throwError("506")
If IsCheckPMData And (ckacct = "" Or X_ccHolderName = "") Then throwError("506")

'data type validation 
'or (not isNumeric(ckaba))
isTypeMismatch = (not isNumeric(X_Amount)) or (not isNumeric(X_Currency)) or (Not IsNumeric(companyNum))
If isTypeMismatch then call throwError("590")
If IsCheckPMData And (not isNumeric(ckacct)) Then throwError("506")


'data length validation
isFieldTooLong = (len(accountName) > 50) or (len(X_PersonalNumber) > 20) or (len(X_PhoneNumber) > 20) or (len(X_Email) > 255) or (len(X_Comment) > 500) or (len(ckaba) > 20) or (len(ckacct) > 20) or (len(companyNum) > 10) or (len(X_Amount) > 12 or (len(bankAccountType) > 1))
if isFieldTooLong then call throwError("591")

'range validation
if cInt(bankAccountType) < 1 or cInt(bankAccountType) > 2 then throwError("590")

If IsNumeric(X_BankState) Then 
	X_BankStateCode = ExecScalar("Select StateISOCode From [List].[StateList] Where stateID = " & X_BankState, "")
Else
	X_BankStateCode = X_BankState
	X_BankState = TestNumVar(ExecScalar("SELECT stateID FROM [List].[StateList] WHERE StateISOCode = '" & Replace(X_BankStateCode, "'", "''") & "'", ""), 0, -1, 0)
End if

'--------------------------------------------------------
'	Get and check company and terminal data
'--------------------------------------------------------
Dim rsCmp, sCompanyID, sCompanyName, CCF_ID, sTerminalNumber, sTerminalChargeOptions, Protect5mins
Set rsCmp = server.createobject("Adodb.Recordset")
rsCmp.Open "SELECT tblCompany.*, mrs.IsEnabled as AllowRecurring FROM tblCompany WITH (NOLOCK) LEFT JOIN tblMerchantRecurringSettings mrs WITH (NOLOCK) ON tblCompany.ID=mrs.MerchantID WHERE ID='" & sCompanyID & "'", oledbData, 0, 1
if Not rsCmp.EOF Then
	' 20080619 Tamir - implement recurring charges by echeck
	bUseOldTerminal = Not rsCmp("IsUsingNewTerminal")
	X_RecurringCount = dbText(request("RecurringCount")) 'number of charges
	X_RecurringCycle = dbText(request("RecurringCycle")) 'interval length
	X_RecurringMode = dbText(request("RecurringMode"))	 'interval unit: 1=day, 2=week, 3=month, 4=year
	' 20080615 Tamir - recurring mode can be also a word or its first letter (case insensitive)
	if X_RecurringMode<>"" then
		if not IsNumeric(TestNumVar(X_RecurringMode, 1, 4, "A")) then
			if StrComp(X_RecurringMode, "DAY", 1)=0 or StrComp(X_RecurringMode, "D", 1)=0 then
				X_RecurringMode=1
			elseif StrComp(X_RecurringMode, "WEEK", 1)=0 or StrComp(X_RecurringMode, "W", 1)=0 then
				X_RecurringMode=2
			elseif StrComp(X_RecurringMode, "MONTH", 1)=0 or StrComp(X_RecurringMode, "M", 1)=0 then
				X_RecurringMode=3
			elseif StrComp(X_RecurringMode, "YEAR", 1)=0 or StrComp(X_RecurringMode, "Y", 1)=0 then
				X_RecurringMode=4
			else
				Call throwError("551") 'Recurring parameters incomplete
			end if
		end if
	end if
	If trim(X_RecurringCount)<>"" OR trim(X_RecurringCycle)<>"" OR trim(X_RecurringMode)<>"" Then
		If NOT rsCmp("AllowRecurring") Then Call throwError("550") 'recurring transaction is not allowed to the merchant
		If trim(X_RecurringCount)="" OR trim(X_RecurringCycle)="" OR  trim(X_RecurringMode)="" Then Call throwError("551") 'Recurring parameters incomplete
		If NOT IsNumeric(X_RecurringCount) OR  NOT IsNumeric(X_RecurringCycle) OR  NOT IsNumeric(X_RecurringMode) Then Call throwError("551") 'Recurring parameters incomplete
	End if
    X_SiteURL = rsCmp("URL")
	sCompanyName = rsCmp("CompanyName")
    sIsBillingAddressMust = trim(rsCmp("IsBillingAddressMust"))
    nMultiChargeProtectionMins = TestNumVar(rsCmp("MultiChargeProtectionMins"), 0, -1, 0)
	if TestNumVar(rsCmp("ActiveStatus"), 0, -1, CMPS_NEW) < CMPS_INTEGRATION Then Call throwError("501")  '��� ���� �� ����
    '--------------- check merchant permissions by request source
    If canChargeLocally Then
	    Select Case Trim(requestSource)
		    case "5", "24", "27"
			    sIsAllow3DTrans = false
			    bIsUseMaxMind = false
			    bIsCheckDailyLimts = false
			    bIsBillingAddressMust = false
			    bIsValidateEmail = False
		    Case "6", "26"
			    'Refund ask admin page, Refund debit cards (toggle)
			    bIsCheckDailyLimts = false 'Overide page defult
			    bIsUseMaxMind = false 'Overide merchant configuration
			    nMultiChargeProtectionMins = 0 'Overide merchant configuration
			    bIsCheckBlockCCNum = false 'Overide page defult
			    sIsAllow3DTrans = false
			    bIsCheckCountryBlacklist = false
			    bIsValidateEmail = False
	    End select
    Else
        Select case requestSource
    		Case "7", "18"
               'manual charge / system charge (admin)
               'if not rsCmp("IsSystemPayEcheck") then call throwError("502")
               'if requestSource <> "18" then
               '    if rsCmp("IsSystemPayPersonalNumber") and X_PersonalNumber = "" then call throwError("513")
               'end if
		        'No matter from where - we dont require personal id for eCheck (us customers)
               if rsCmp("IsSystemPayPhoneNumber") and X_PhoneNumber = "" then call throwError("514")
               if rsCmp("IsSystemPayEmail") and X_Email = "" then call throwError("515")
           case "9", "11"
               'popup charge english, popup charge hebrew
		        if not rsCmp("IsCustomerPurchaseEcheck") then call throwError("502")
		        if requestSource = "11" then
			        if rsCmp("IsCustomerPurchasePersonalNumber") and X_PersonalNumber = "" then call throwError("513")
		        end if
		        if rsCmp("IsCustomerPurchasePhoneNumber") and X_PhoneNumber = "" then call throwError("514")
		        if rsCmp("IsCustomerPurchaseEmail") and X_Email = "" then call throwError("515")
           Case "32", "33", "40"
			    Dim hpsMerchant : Set hpsMerchant = New HostedPageSettings
			    hpsMerchant.LoadMerchant sCompanyID, "IsEcheck"
			    If Not hpsMerchant.IsActive Then ThrowError "502"
			    If hpsMerchant.IsRequiredEmail And X_Email = "" Then ThrowError "515"
			    If hpsMerchant.IsRequiredPhone And X_PhoneNumber = "" Then ThrowError "514"
			    'If hpsMerchant.IsRequiredID And X_PersonalNumber = "" Then ThrowError "513"
			    Set hpsMerchant = Nothing
           case "12", "19"
               'public charge - not implemented for now
               if Not rsCmp("IsPublicPayEcheck") then call throwError("502")
               if rsCmp("IsPublicPayPersonalNumber") and X_PersonalNumber = "" then call throwError("513")
               if rsCmp("IsPublicPayPhoneNumber") and X_PhoneNumber = "" then call throwError("514")
               if rsCmp("IsPublicPayEmail") and X_Email = "" then call throwError("515")
           case "", "8", "42"
               'remote charge
               if not rsCmp("IsRemoteChargeEcheck") then call throwError("502")
               if rsCmp("IsRemoteChargeEchPersonalNumber") and X_PersonalNumber = "" then call throwError("513")
               if rsCmp("IsRemoteChargeEchPhoneNumber") and X_PhoneNumber = "" then call throwError("514")
               if rsCmp("IsRemoteChargeEchEmail") and X_Email = "" then call throwError("515")
		    case "20", "29"
			    'recurring page
			    sIsAllow3DTrans = false
           case else
               'invalid request source
               call throwError("526")
        End select
    End If
End if
rsCmp.Close
If sCompanyID = 0 Then	Call throwError("500") '���� ��� �� ���� - �� ���� ��� ������

sTransactionTypeID = requestSource
'Load Original Trans Details From X_RefTransID
If (X_TypeCredit = 0) And (X_RefTransID <> 0) Then
   Set rsData3 = oledbData.execute("Select DebitReferenceCode, DebitReferenceNum, TerminalNumber, OCurrency, ApprovalNumber From tblCompanyTransPass Where ID=" & X_RefTransID)
   if Not rsData3.EOF Then 
   	  X_DebitReferenceCode = rsData3("DebitReferenceCode")
   	  X_DebitReferenceNum = rsData3("DebitReferenceNum")
   	  X_ApprovalNumber = rsData3("ApprovalNumber")
	  sTerminalNumber = rsData3("TerminalNumber")
	  if sTerminalNumber <> "" Then
         sTerminalChargeOptions = rsData3("OCurrency")
         CCF_ID = 0
	     bIsCheckTerminalUsabe = false
	  End if  
   end if
   rsData3.Close
   Set rsData3 = Nothing
End if

'----------------------------------------------------------------------
'	Check billing address fields
'----------------------------------------------------------------------
If sIsBillingAddressMust Then
	If BACHAddr1 = "" Then Call throwError("540")
	If BACity = "" Then Call throwError("541")
	If BAZipCode = "" Then Call throwError("542")
	If BACountry = "" Then Call throwError("544")
End if

If isNumeric(X_BankCountry) Then
	'Country is in numeric format
	X_BankCountry = ExecScalar("SELECT [CountryISOCode] FROM [List].[CountryList] WHERE CountryID = " & X_BankCountry, "US")
	if X_BankCountry = "" Then throwError(543)
	BACountryId = BACountry
End if

If sIsBillingAddressMust OR trim(BACountry)<>"" Then
    If isNumeric(BACountry) Then
		'Country is in numeric format
		BACountryCode = ExecScalar("SELECT [CountryISOCode] FROM [List].[CountryList] WHERE CountryID = " & BACountry, "US")
		if BACountryCode = "" Then throwError(543)
		BACountryId = BACountry
	Else
		'Country is in two letter iso format
		BACountryId = ExecScalar("SELECT CountryID FROM [List].[CountryList] WHERE [CountryISOCode] = '" & BACountry & "'", "544")
		if BACountryId = "" Then throwError(543)
		BACountryCode = BACountry
    End if
End if

If sIsBillingAddressMust OR trim(BAState)<>"" Then
	If trim(BACountryCode)="US" OR trim(BACountryCode)="CA" Then
	    If trim(BAState)<>"" AND isNumeric(BAState) Then
			'State is in numeric format
			BAStateCode = ExecScalar("SELECT StateISOCode FROM [List].[StateList] WHERE stateID = " & BAState, "NY")
			if BAStateCode = "" Then throwError(543)
			BAStateId = BAState
		Elseif trim(BAState)<>"" AND NOT isNumeric(BAState) Then
			'State is in two letter iso format
			BAStateId = ExecScalar("SELECT stateID FROM [List].[StateList] WHERE StateISOCode = '" & BAState & "'", "543")
			if BAStateId = "" Then throwError(543)
			BAStateCode = BAState
		Else
			call throwError("543")
	    End if
	End if
End if
If trim(BAZipCode) <> "" AND Len(BAZipCode) > 15 Then Call throwError("542")

'---------------------------------------------------------------
'	eCheck test mode
'---------------------------------------------------------------
 If ckacct = "999999999" or ckacct = "12345678" or terminalNumber = "8888888" Then isTestOnly = "1" Else isTestOnly = "0"

 'check for last x minutes same transaction
 If nMultiChargeProtectionMins > 0 And trim(ckacct) <> "" Then 
	sSQL="SELECT CAST(CASE WHEN EXISTS(SELECT 1 FROM [Track].[ProcessApproved] WHERE" &_
	" DATEDIFF(SECOND, CAST(TransDate AS DATETIME2(2)), SYSDATETIME())<=" & nMultiChargeProtectionMins*60 & " AND Merchant_id=" & sCompanyID & _
	" AND TransIpAddress='" & sIP & "' AND TransAmount=" & X_Amount & " AND CheckingAccountNumber256=dbo.GetEncrypted256('" & trim(ckacct) & "')" & _
	" AND PaymentMethodStamp=" & Left(Trim(ckacct),6) & " AND TransCurrency=" & X_Currency & " AND TransInstallments=" & 1 & ") THEN 1 ELSE 0 END AS BIT)"
	If ExecScalar(sSQL, false) Then throwError "522"
 End If
 CheckPassLimitation sCompanyID, X_Currency, PaymentMethod, X_TypeCredit, trim(ckacct), X_Amount, 0, (requestSource = "7" Or requestSource = "18")

 BeginCartTransaction
'---------------------------------------------------------
'   Check clearing company by terminal
'---------------------------------------------------------
Function GetDebitTerminalInfo(sTermNum)
	Set rsTerminal = Server.CreateObject("Adodb.Recordset")
	rsTerminal.Open "SELECT dc_isActive, dc_TempBlocks, dbo.GetDecrypted256(accountPassword256) As accountPassword," & _
		" dbo.GetDecrypted256(accountPassword3D256) As accountPassword3D, tblDebitTerminals.* FROM tblDebitTerminals" & _
		" LEFT JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany=tblDebitCompany.debitCompany_id" & _
		" WHERE TerminalNumber='" & trim(sTermNum) & "'", oledbData, 0, 1
	GetDebitTerminalInfo = "---"
	If Not rsTerminal.EOF Then
		If Not rsTerminal("isActive") Then GetDebitTerminalInfo = "592"
		If Not rsTerminal("dc_isActive") Then GetDebitTerminalInfo = "592"
		If rsTerminal("dc_TempBlocks") > 0 Then GetDebitTerminalInfo = "524"
		debitCompany = rsTerminal("DebitCompany")
		bIsNetpayTerminal = rsTerminal("isNetpayTerminal")
		bIsShvaMasterTerminal = rsTerminal("isShvaMasterTerminal")
		nProcessingMethod = rsTerminal("processingMethod")
		sMcc = rsTerminal("dt_mcc")
		isManipulateAmount = rsTerminal("dt_isManipulateAmount")
		isTestTerminal = rsTerminal("dt_IsTestTerminal")

		sTermName = Trim(rsTerminal("dt_Descriptor"))
		'terminal login
		sAccountId = Trim(rsTerminal("accountId"))
		sAccountSubId = Trim(rsTerminal("accountSubId"))
		if rsTerminal("accountPassword") <> "" Then sAccountPassword = rsTerminal("accountPassword")
		'terminal 3D login
		sAccountId3D = Trim(rsTerminal("accountId3D"))
		sAccountSubId3D = Trim(rsTerminal("accountSubId3D"))
		if rsTerminal("accountPassword3D") <> "" Then sAccountPassword3D = rsTerminal("accountPassword3D")
	End If
	rsTerminal.close
	If debitCompany = 20 Then bIsUseMaxMind = False
End Function

'PriorityList Setup
X_TerminalHasNext = False
X_TransINC = "X_TransINC_" & sCompanyID & "_" & X_ccNumber & "_" & X_Amount & "_" & X_Currency
XX_Amount = X_Amount
debitCompany = 1

If isTestOnly = "1" Then
	sTerminalChargeOptions = X_Currency
	sTerminalNumber = "0000000"
ElseIf bUseOldTerminal Then
	CCF_ID = GetCompanyTerminal(sCompanyID, PaymentMethod, X_Currency, ccBINCountry, ccBIN, X_TypeCredit, sTerminalNumber, sTerminalChargeOptions)
	CCF_ID = sTerminalChargeOptions
Else
	Dim mSendTrans : Set mSendTrans = New CSendTrans
	CCF_ID = mSendTrans.GetMerchantTerminal(sCompanyID, PaymentMethod, X_Currency, X_TypeCredit, 0, sTerminalNumber, sTerminalChargeOptions)
End If

If CCF_ID = -2 Then 'not found
	call throwError("503")
Elseif CCF_ID = -1 Then 'blocked
	Call throwError("503")
Else
	If bUseOldTerminal Then
		If sTerminalChargeOptions <> X_Currency Then 'convert currency if needed
			X_Amount = ConvertCurrency(X_Currency, sTerminalChargeOptions, X_Amount)
			X_Currency = CCF_ID
		End If
		Call SendCharge()
	Else	
		Call fn_returnResponse(LogSavingChargeDataID, requestSource, debitCompany, X_TransType, X_DPRET, nTransactionID, nInsertDate, X_OrderNumber, Formatnumber(X_Amount,2,-1,0,0), X_Payments, X_Currency, dpApproval, X_Comment)
	End If
End If

Function SendCharge()
	If Not isNumeric(debitCompany) Then throwError("503")
	If sTerminalChargeOptions <> X_Currency Then 'Convert Currency if needed
		X_Amount = ConvertCurrency(X_Currency, sTerminalChargeOptions, XX_Amount)
		X_Currency = sTerminalChargeOptions
	Else	
		X_Amount = XX_Amount 'Restore Amount
	End If
	
	X_DPRET = GetDebitTerminalInfo(sTerminalNumber)
	If X_DPRET <> "---" Then
		If bUseOldTerminal Then throwError(X_DPRET)
		DebitIProcessResult X_DPRET, ""
		SendCharge = False
		Exit Function
	End If	
    
	If requestSource = "27" Then
		Call DebitIProcessResult(IIF(int(nProcessingMethod) = 2, "001", "000"), "")
	ElseIf int(nProcessingMethod) = 2 Then
		Call DebitIProcessResult("001", "")
	Else
	   ExecSql("Update tblLogChargeAttempts Set Lca_DebitCompanyID=" & debitCompany & " Where LogChargeAttempts_id=" & LogSavingChargeDataID)
		Select Case debitCompany
			case 1 : call debitNULL()				'NULL debitCompany
			case 24 : call debit_eTriton()			'debit eTriton	
			case 36 : call debit_WTS()				'debit WTS
			case 37 : 'WebbillingHpp
                If PaymentMethod = 145 Then remoteCharge_echeck_WebbillingHpp() _
                Else call debit_Webbilling()		'debit Webbilling
			case 38 : call debit_PPro()				'debit PPro - added by udi at 23-3-10
			'case 41 : call debit_BillingPartners() 'Billing Partners
			case 42 : call debit_InPay()			'InPay
			'case 48 : call debit_AltCharge()		'AltCharge
			case 49 : Call debit_DeltaPayHpp()		'DeltaPay
			case 50 : Call debit_TrustPay()		    'TrustPay
			case 61 : Call debit_Actum()			'actum
			case else : call throwError("503")		'invalid clearing company

		End Select
	End if
	SendCharge = (Trim(X_DPRET) = "000") Or (Trim(X_DPRET) = "001")
End Function

Function DebitIProcessResult(dpRet, dpApproval)
	on error resume next
    	newBillingAddressId = insertBillingAddress(BACountryCode, BAStateCode, BACountryId, BAStateId, BACity, BACHAddr1, BACHAddr2, BAZipCode)
	on error goto 0
	If newBillingAddressId=0 AND sIsBillingAddressMust Then Call throwError(523)
	sSQL = "SET NOCOUNT ON;" & _
	"INSERT INTO tblCheckDetails (companyId, customerId, accountName, routingNumber256," & _
	" accountNumber256, bankAccountTypeId, personalNumber, phoneNumber, email, comment," & _
	" BankName, BankCity, BankPhone, BankState, BankCountry, BirthDate, BillingAddressId) VALUES (" & _
	sCompanyID & ", " & IIF(X_Customer = "0", "Null", X_Customer) & ", Left(N'" & X_ccHolderName & "', 50)," & _
	" dbo.GetEncrypted256('" & ckabaEnc & "'), dbo.GetEncrypted256('" & ckacctEnc & "')," & _
	bankAccountType & ", Left(N'" & X_PersonalNumber & "', 20), Left(N'" & X_PhoneNumber & "', 20)," & _
	" Left(N'" & X_Email & "', 50), Left(N'" & X_Comment & "', 500), Left('" & X_BankName & "', 50), Left('" & X_BankCity & "', 15)," & _
	" Left('" & X_BankPhone & "', 15), Left('" & X_BankState & "', 2), Left('" & X_BankCountry & "', 2), Left('" & X_BirthDate & "', 10)," & _
	IIF(newBillingAddressId = 0, "Null", newBillingAddressId) & ");" & _
	"SELECT SCOPE_IDENTITY() AS NewCardID;SET NOCOUNT OFF"
	xParam = ExecScalar(sSQL, 0)

	SaveTransactionData PaymentMethod, ccTypeEngShow, ckacctEnc, ckabaEnc, Empty, IIF(Len(X_BankCountry) = 2,  X_BankCountry, "")
	If Trim(dpRet) = "000" And Request("AutoWalletRegistration") = "1" Then sClient_WalletID = CreateCustomerFromTransaction(PaymentMethod, ckacctEnc, ckabaEnc, DateAdd("yyyy", 10, Now))

	If bUseOldTerminal Then DebitIProcessResult = DebitProcessResult(dpRet, dpApproval, xParam) _
	Else DebitIProcessResult = DebitProcessResultIN(dpRet, dpApproval)
End Function

%>
<!--include file="remoteCharge_echeckFunctions.asp"-->
<!--#include file="remoteCharge_ccDebitNULL.asp"-->
<!--#include file="remoteCharge_echeckETritonX.asp"-->
<!--#include file="remoteCharge_echeckWTS.asp"-->
<!--#include file="remoteCharge_echeckWebbilling.asp"-->
<!--#include file="remoteCharge_echeckPPro.asp"-->
<!--#include file="remoteCharge_echeckInPay.asp"-->
<!--#include file="remoteCharge_echeckAltCharge.asp"-->
<!--#include file="remoteCharge_ccDebitDeltaPayHPP.asp"-->
<!--#include file="remoteCharge_ccDebitDeltaPay.asp"-->
<!--#include file="remoteCharge_echeckTrustPay.asp"-->
<!--#include file="remoteCharge_echeckWebbillingHpp.asp"-->
<!--#include file="remoteCharge_echeckActum.asp"-->

<%
Set rsData = nothing
call closeConnection()
%>
