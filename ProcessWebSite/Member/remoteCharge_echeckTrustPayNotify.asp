<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="remoteCharge_Functions.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/MerchantTerminals.asp"-->
<%
'---------------------------------------------------------------------
'	Debit TrustPay
'---------------------------------------------------------------------


<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="remoteCharge_Functions.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/MerchantTerminals.asp"-->
<!--#include file="../include/timer_class.asp"-->

<%
'---------------------------------------------------------------------
'	Debit DeltaPay
'---------------------------------------------------------------------
Const DebitCompanyTrustPay = 50
FileAppendData "TrustPay.txt", "Request:" & Now & " " & Request.Form & " " & Request.QueryString & vbCrLf
Server.ScriptTimeout = 300

Dim X_OCurrency, X_OAmount, X_DebitReferenceCode, nApprovalRecurringSeries, nRes, X_ReplyURL, IsNotifyRequest
bUseOldTerminal = True
nApprovalRecurringSeries = "NULL"

sReturnCode = Request("RES")
sApprovalNumber = Request("TID")
X_DebitReferenceCode = Request("REF")
IsNotifyRequest = Request("SIG") <> ""

Dim passID, pendingID, failID, logChargeWhere
pendingID = ExecScalar("Select companyTransPending_id From tblCompanyTransPending Where DebitCompanyID=" & DebitCompanyTrustPay & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
If pendingID = 0 Then 
    passID = ExecScalar("Select ID From tblCompanyTransPass Where DebitCompanyID=" & DebitCompanyTrustPay & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
    If passID = 0 Then failID = ExecScalar("Select ID From tblCompanyTransFail Where DebitCompanyID=" & DebitCompanyTrustPay & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
    If failID  = 0 Then
        Response.Write("Transaction not found")
        Response.End
    End If
End If

If pendingID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=" & DebitCompanyTrustPay & " And Lca_ReplyCode='553' And Lca_TransNum IN(" & pendingID & ")"
ElseIf passID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=" & DebitCompanyTrustPay & " And Lca_ReplyCode='000' And Lca_TransNum IN(" & passID & ")"
ElseIf failID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=" & DebitCompanyTrustPay & " And Lca_ReplyCode Not IN('000', '001', '553') And Lca_TransNum IN(" & failID & ")"
End If

Dim xLogChargeAttemptID, xMerchantNum, xRequestParams, xSendParams, xRetParams, xDebitCompany, xLogReplyCode
If Not GetTransInfo(logChargeWhere) Then
    Response.Write("Transaction not found")
    Response.End
End If
X_ReplyURL = GetURLValue(xRequestParams, "RetURL")
'Response.Write("Process - " & X_ReplyURL)

If pendingID <> 0 Then
    If Trim(sReturnCode) = "0" Then
        sReturnCode = "000"
    Else If Trim(sReturnCode) = "1" Then
        sReturnCode = "001"
    Else
        'return return code
    End If
    If sReturnCode <> "001" Then 
        nRes = MovePendingTransaction("DebitCompanyID=" & DebitCompanyTrustPay & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", sReturnCode)
        ExecSql("Update tblLogChargeAttempts Set Lca_ReplyCode='" & sReturnCode & "', Lca_ReplyDesc='" & Replace(X_CustomErrorDesc, "'", "''") & "', Lca_TransNum=" & nRes & ", Lca_ResponseString='" & Replace(Request.Form & "&" & Request.QueryString, "'", "''") & "' Where LogChargeAttempts_id=" & xLogChargeAttemptID)
    End If
Else
    sReturnCode = xLogReplyCode
    nRes = IIF(passID <> 0, passID, failID)
    If IsNotifyRequest And Request("TYP") = "CRDT" Then _
		Call ChargeBackTrans(0, "DebitCompanyID=" & DebitCompanyTrustPay & " And DebitReferenceCode='" & X_CustomErrorDesc & "'")
End If
If IsNotifyRequest Then Response.Write("OK") _
Else ReturnResponseFromTransID nRes, sReturnCode
Response.End()
%>
