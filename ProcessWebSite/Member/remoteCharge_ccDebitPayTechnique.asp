<%
'---------------------------------------------------------------------
'	Debit debitPayTechnique
'---------------------------------------------------------------------
Function remoteCharge_PayTechnique_BackDetect()
    Set xmlRet = Server.CreateObject("Msxml2.DOMDocument.3.0")
	xRetParams = Replace(Replace(Replace(xRetParams, "&gt;", ">"), "&lt;", "<"), "``", """")
    xmlRet.loadXML(xRetParams)
    X_3dRedirect = xmlRet.selectSingleNode("//verificationUrl").text
    d3PaReq = xmlRet.selectSingleNode("//verificationParams").text
    Set xmlRet = Nothing
    If d3PaReq <> "" Then 
        Dim params, pValues : params = Split(d3PaReq, "&")
	    Response.Write("<html><body onloadX=""document.getElementById('frmMain').submit()""><form id=""frmMain"" method=""post"" action=""" & X_3dRedirect & """>" & bvCrLf)
        For Each p in params
            pValues = Split(p, "=")
	        Response.Write(" <input type=""hidden"" name=""" & pValues(0) & """ value=""" & URLDecode(pValues(1)) & """/>" & bvCrLf)
        Next
	    Response.Write(" <input type=""submit"" />" & bvCrLf)
	    Response.Write("</form></body></html>")
        Response.End
    Else
        Response.Redirect(X_3dRedirect)
    End If
End Function

Function debitPayTechniqueAppendParam(ByRef outValue, sParam)
    debitPayTechniqueAppendParam = sParam
    outValue = outValue & sParam
End Function

Function debitPayTechniqueGetCardType(pmId)
    Select Case pmId
    Case PMD_CC_VISA: debitPayTechniqueGetCardType = "VISA"
    Case PMD_CC_MASTERCARD: debitPayTechniqueGetCardType = "MASTERCARD"
    Case PMD_CC_AMEX: debitPayTechniqueGetCardType = "AMEX"
    Case PMD_CC_DISCOVER: debitPayTechniqueGetCardType = "DISCOVER"
    Case Else: debitPayTechniqueGetCardType = "---"
    End Select
End Function

Function debitPayTechnique()
	Dim HttpReq, UrlAddress, ParamList, sApprovalNumber, sDebitReturnAnswer, soapEvnelope, checkValue, sStatus
	If int(X_TypeCredit) = 8 Or int(X_Payments) > 1 Then Call throwError("508") 
    If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		soapEvnelope = IIF((Cint(X_TypeCredit) = 0), "Cancel", "Capture")
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""urn:PayTechnique"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:SOAPENC=""http://schemas.xmlsoap.org/soap/encoding/"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">" & vbCrLf & _
        " <SOAP-ENV:Body>" & vbCrLf & _
        "  <ns1:" & soapEvnelope & ">" & vbCrLf & _
        "   <params xsi:type=""ns1:PayTechniqueSoap" & soapEvnelope & "Request"">" & vbCrLf & _
		"	  <amount>" & debitPayTechniqueAppendParam(checkValue, formatNumber(X_Amount, 2, true, false, false)) & "</amount>" & vbCrLf & _
		"	  <merchantID>" & debitPayTechniqueAppendParam(checkValue, sAccountId) & "</merchantID>" & vbCrLf & _
		"	  <orderReference>" & debitPayTechniqueAppendParam(checkValue, X_DebitReferenceCode) & "</orderReference>" & vbCrLf & _
        "     <signature>" & LCase(Hash("MD5", checkValue & sAccountPassword).Hex) & "</signature>" & vbCrLf & _
        "    </params>" & vbCrLf & _
		"	</ns1:" & soapEvnelope & ">" & vbCrLf & _
		"  </SOAP-ENV:Body>" & vbCrLf & _
		"</SOAP-ENV:Envelope>"
	Else
	    soapEvnelope = IIF(Cint(X_TransType) = 1, "Auth", "Sale")
		X_DebitReferenceCode = GetTransRefCode()
		Dim countryName: countryName = ExecScalar("SELECT ISOCode3 FROM [List].[CountryList] WHERE CountryID = " & TestNumVar(BACountryId, 1, 0, 0), "")
        'If countryName = "United States" Then countryName = "USA"
        'params should be order by name asc before MD5
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""urn:PayTechnique"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:SOAPENC=""http://schemas.xmlsoap.org/soap/encoding/"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">" & vbCrLf & _
        " <SOAP-ENV:Body>" & vbCrLf & _
        "  <ns1:" & soapEvnelope & ">" & vbCrLf & _
        "   <params xsi:type=""ns1:PayTechniqueSoap" & soapEvnelope & "Request"">" & vbCrLf & _
		"	  <cardExpiration>" & debitPayTechniqueAppendParam(checkValue, X_ccExpMM & X_ccExpYY) & "</cardExpiration>" & vbCrLf & _
		"	  <cardNumber>" & debitPayTechniqueAppendParam(checkValue, Replace(X_ccNumber, " ", "")) & "</cardNumber>" & vbCrLf & _
		"	  <cardType>" & debitPayTechniqueAppendParam(checkValue, debitPayTechniqueGetCardType(PaymentMethod)) & "</cardType>" & vbCrLf & _
		"	  <cardVerificationCode>" & debitPayTechniqueAppendParam(checkValue, X_ccCVV2) & "</cardVerificationCode>" & vbCrLf & _
		"	  <customerAddress>" & debitPayTechniqueAppendParam(checkValue, BACHAddr1) & "</customerAddress>" & vbCrLf & _
		"	  <customerCity>" & debitPayTechniqueAppendParam(checkValue, BACity) & "</customerCity>" & vbCrLf & _
		"	  <customerCountry>" & debitPayTechniqueAppendParam(checkValue, countryName) & "</customerCountry>" & vbCrLf & _
		"	  <customerEmail>" & debitPayTechniqueAppendParam(checkValue, X_Email) & "</customerEmail>" & vbCrLf & _
		"	  <customerIP>" & debitPayTechniqueAppendParam(checkValue, sIP) & "</customerIP>" & vbCrLf & _
		"	  <customerName>" & debitPayTechniqueAppendParam(checkValue, X_ccHolderName) & "</customerName>" & vbCrLf & _
		"	  <customerPhoneNumber>" & debitPayTechniqueAppendParam(checkValue, Replace(Replace(X_PhoneNumber, " ", ""), "-", "")) & "</customerPhoneNumber>" & vbCrLf & _
		"	  <customerState>" & debitPayTechniqueAppendParam(checkValue, IIF(BAStateCode = "", "---", BAStateCode)) & "</customerState>" & vbCrLf & _
		"	  <customerZipCode>" & debitPayTechniqueAppendParam(checkValue, BAZipCode) & "</customerZipCode>" & vbCrLf & _
		"	  <merchantID>" & debitPayTechniqueAppendParam(checkValue, sAccountId) & "</merchantID>" & vbCrLf & _
		"	  <orderAmount>" & debitPayTechniqueAppendParam(checkValue, formatNumber(X_Amount, 2, true, false, false)) & "</orderAmount>" & vbCrLf & _
		"	  <orderCurrency>" & debitPayTechniqueAppendParam(checkValue, GetCurISOName(Cint(X_Currency))) & "</orderCurrency>" & vbCrLf & _
		"	  <orderDescription>" & debitPayTechniqueAppendParam(checkValue, X_PayFor) & "</orderDescription>" & vbCrLf & _
		"	  <orderReference>" & debitPayTechniqueAppendParam(checkValue, X_DebitReferenceCode) & "</orderReference>" & vbCrLf & _
		"	  <returnUrl>" & debitPayTechniqueAppendParam(checkValue, Session("ProcessURL") & "remoteCharge_ccDebitPayTechniqueBack.asp?IDebitRefCode=" & X_DebitReferenceCode) & "</returnUrl>" & vbCrLf & _
        "     <signature>" & LCase(Hash("MD5", checkValue & sAccountPassword).Hex) & "</signature>" & vbCrLf & _
        "    </params>" & vbCrLf & _
		"	</ns1:" & soapEvnelope & ">" & vbCrLf & _
		"  </SOAP-ENV:Body>" & vbCrLf & _
		"</SOAP-ENV:Envelope>"
	End If

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    If isTestTerminal Then UrlAddress = "https://secure.payinspect.com/paymentgw/creditcard" _
	Else UrlAddress = "https://secure.payinspect.com/paymentgw/creditcard"

	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false', sAccountSubId, sAccountPassword
		HttpReq.setRequestHeader "Content-Type", "application/soap+xml; charset=utf-8"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		TimeControl.Append("A_SEND") 'Time Optimization *********

		If Len(sResDetails) = 0 Then Call throwError("521")
		sDebitReturnAnswer = Left(sResDetails, 3900)
		'Response.Write(ParamList & vbCrLf & "<br>" & vbCrLf & "CheckValue:" & checkValue & sAccountPassword & vbCrLf & "<br>" & vbCrLf & sResDetails)' : Response.End()
	
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "<cardVerificationCode>" & X_ccCVV2, "<cardVerificationCode>" & GetSafePartialNumber(X_ccCVV2))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

		Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
		xmlRet.LoadXML HttpReq.responseText
		sReturnCode = Trim(xmlRet.selectSingleNode("//statusCode").text) 'State
		sStatus = xmlRet.selectSingleNode("//transactionStatus").text
		X_CustomErrorDesc = xmlRet.selectSingleNode("//transactionError").text
        If X_CustomErrorDesc = "" Then X_CustomErrorDesc = xmlRet.selectSingleNode("//detail").text
        If sStatus = "" Then sStatus = "FAIL"
	On Error Goto 0
	If (Len(sStatus) < 1) Then Call throwError("520")
    Select Case sStatus
        Case "SUCCESS", "ACCEPT"
            sReturnCode = "000"
        Case "ONHOLD"
            If Cint(X_TransType) = 1 Or Cint(X_TypeCredit) = 0 Then sReturnCode = "000" _
            Else sReturnCode = "001"
        Case "VERIFY"
			X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
    	    sIsSendUserConfirmationEmail = False
			Call DebitIProcessResult("553", sApprovalNumber)
        Case "FAIL"
            If sReturnCode = "000" Or sReturnCode = "001" Or sReturnCode = "" Then sReturnCode = "002"
    End Select
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>