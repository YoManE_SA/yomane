<%
'---------------------------------------------------------------------
'	Debit Actum
'---------------------------------------------------------------------
Function GetActumValue(strUrl, strValue)
	Dim lIndex, eIndex, lChar : lIndex = 0 : lChar = ""
	Do
		lIndex = InStr(lIndex + 1, strUrl, strValue & "=", 1)
		If(lIndex < 1) Then Exit Function _
		Else If(lIndex > 1) Then lChar = Mid(strUrl, lIndex - 1, 1)
	Loop While lIndex > 1 And lChar <> vbLf 
	lIndex = lIndex + Len(strValue & "=")
	eIndex = InStr(lIndex, strUrl, vbLf)
	If(eIndex < 1) Then eIndex = Len(strUrl) + 1
	GetActumValue = Mid(strUrl, lIndex, eIndex - lIndex)
End Function

function remoteCharge_echeck_Actum_BackDetect()
    Response.Redirect URLDecode(GetActumValue(xRetParams, "url"))
end function

function debit_Actum()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer
	sOuterIP = IIF(session("Identity") = "Local", "http://80.179.39.10:81/Default.aspx", Session("ProcessURL") & "remoteCharge_echeckActum_Back.asp")

	'xNames = Split(Trim(X_ccHolderName), " ")
	'if Ubound(xNames) < 1 Then Call throwError("517")
	'fName = xNames(0) : sName = xNames(1)
	
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
	UrlAddress = "https://join.achbill.com/cgi-bin/man_trans.cgi"
	If Cint(X_TransType) = 1 Then Call throwError("503")
	'if X_Amount < 25 Then throwError("505")
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		ParamList = _
			"parent_id=" & sAccountId & _
			"&sub_id=" & sAccountSubId & _
			"&pmt_type=chk" & _
			"&action_code=R" & _
			"&prev_history_id=" & X_DebitReferenceCode & _
			"&order_id=" & X_ConfirmNumber & _
			"&initial_amount=" & FormatNumber(X_Amount, 2, True, False, False)
	Else
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = _
			"parent_id=" & sAccountId & _
			"&sub_id=" & sAccountSubId & _
			"&pmt_type=chk" & _
			"&response_location=" & Server.URLEncode(sOuterIP) & _
			"&acct_name=" & Server.URLEncode(X_ccHolderName) & _
			"&chk_acct=" & Server.URLEncode(ckacct) & _ 
			"&chk_aba=" & Server.URLEncode(ckaba) & _ 
			"&chk_fract=" & Server.URLEncode(cAcc) & _ 
			"&chk_number=" & Server.URLEncode(cAcc) & _
			"&currency=US" & _

			"&custname=" & Server.URLEncode(X_ccHolderName) & _
			"&custemail=" & Server.URLEncode(X_Email) & _
			"&custaddress1=" & Server.URLEncode(BACHAddr1) & _
			"&custaddress2=" & Server.URLEncode(BACHAddr2) & _
			"&custcity=" & Server.URLEncode(BACity) & _
			"&custstate=" & Server.URLEncode(BAStateCode) & _
			"&custzip=" & Server.URLEncode(BAZipCode) & _
			"&custphone=" & Server.URLEncode(X_PhoneNumber) & _

			"&initial_amount=" & FormatNumber(X_Amount, 2, True, False, False) & _
			"&billing_cycle=-1" & _
			"&ip_forward=" & Server.URLEncode(sIP) & _
			"&merordernumber=" & X_DebitReferenceCode & _
			"&action_code=P"
	End if
	On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
		HttpReq.Send ParamList
		TxtRet = HttpReq.responseText
		TxtRet = Replace(Replace(TxtRet, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba))
		SaveLogChargeAttemptRequestResponse Replace(Replace(ParamList, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba)), TxtRet
		HttpError = FormatHttpRequestError(HttpReq)
	On Error GoTo 0
	

	'TxtRet = Replace(TxtRet, vbLf, "&")
	'Response.Write(ParamList & "<br>" & TxtRet & "<br/>")
	'Response.End

	If Len(TxtRet) = 0 Then Call throwError("521")
	sDebitReturnAnswer = Left(TxtRet, 400)

	sReturnCode = GetActumValue(TxtRet, "status")
	sApprovalNumber = GetActumValue(TxtRet, "order_id")
	sResult = GetActumValue(TxtRet, "reason")
	'X_ConfirmNumber = GetURLValue(TxtRet, "order_id")
	
	If sReturnCode = "Accepted" Then 
		sReturnCode = "000"
	ElseIf sReturnCode = "verify" Then 
		sReturnCode = "001"
		X_3dRedirect = GetActumValue(TxtRet, "url")
		If (X_3dRedirect <> "") Then 
		    X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
			sReturnCode = "553"
			sIsSendUserConfirmationEmail = False
		End If	
	Else 
		X_CustomErrorDesc = GetActumValue(TxtRet, "reason")
		sReturnCode = "002"
	End If 

	Set xmlRet = Nothing
	Set HttpReq = Nothing
	DebitIProcessResult sReturnCode, sApprovalNumber
End function
%>