<%
'---------------------------------------------------------------------
'	Debit debitB_SN
'---------------------------------------------------------------------
Function Allied_Make2DigDate(xNum)
	If Len(xNum) = 1 Then xNum = "0" & xNum
	Allied_Make2DigDate = xNum
End Function

Function ReplaceEscapeSequences(sText)
	dim i, j, sTemp
	If IsEmpty(sText) Then
		sTemp = ""
	ElseIf InStr(sText, "&") < 1 Then
		sTemp = sText
	Else
		sTemp = Replace(sText, "&;", " ")
		For i = 1 To Len(sTemp)
			If Mid(sTemp, i, 1) = "&" Then
				For j = i To Len(sTemp)
					If Mid(sTemp, i, 1) = ";" Then sTemp = Replace(sTemp, Mid(sTemp, i, j-i+1), Mid(sTemp, i+1, 1))
				Next
			End If
		Next
		sTemp = Replace(Replace(sTemp, "&", " "), ";", " ")
	End If
	ReplaceEscapeSequences = sTemp
End Function

Function debitAllied()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer, soapEvnelope
	If int(X_TypeCredit) = 8 Or int(X_Payments) > 1 Then Call throwError("508") 
	xNames = Split(ReplaceEscapeSequences(X_ccHolderName), " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = ""
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = ""
    'Dim sBNSFormatedAmount : sBNSFormatedAmount = CLng(IIF(Cint(X_Currency) = 6, X_Amount, formatNumber(X_Amount * 100, 2)))
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_ConfirmNumber = "" Then Call throwError("532") 'Reftrans not Found
		Dim refAmountNode
		If Cint(X_TypeCredit) = 0 Then
			Dim preAuthApproval : preAuthApproval = ExecScalar("Select approvalNumber From tblCompanyTransApproval Where TransAnswerID=" & X_RefTransID, "")
			If preAuthApproval <> "" Then X_ConfirmNumber = preAuthApproval
			If X_Amount = RefOAmount Then
				soapEvnelope = "Refund"
				refAmountNode = ""
			Else
				soapEvnelope = "PartialRefund"
				refAmountNode = "	  <RefundAmount>" & formatNumber(X_Amount, 2, true, false, false) & "</RefundAmount>" & vbCrLf
			End If
		Else
			If RefOAmount <> X_Amount Then Call throwError("538")
			soapEvnelope = "Capture"
		End If
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">" & vbCrLf & _
		"  <soap12:Body>" & vbCrLf & _
		"	<" & soapEvnelope & " xmlns=""http://service.381808.com/"">" & vbCrLf & _
		"	  <MerchantID>" & sAccountId & "</MerchantID>" & vbCrLf & _
		"	  <TransactionID>" & X_ConfirmNumber & "</TransactionID>" & vbCrLf & _
		refAmountNode & _
		"	</" & soapEvnelope & ">" & vbCrLf & _
		"  </soap12:Body>" & vbCrLf & _
		"</soap12:Envelope>"
	Else
		soapEvnelope = IIF(Cint(X_TransType) = 1, "PreauthorizeCreditCard2", "ExecuteCreditCard2")
        X_DebitReferenceCode = GetTransRefCode()
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">" & vbCrLf & _
		"  <soap12:Body>" & vbCrLf & _
		"	<" & soapEvnelope & " xmlns=""http://service.381808.com/"">" & vbCrLf & _
		"	  <MerchantID>" & sAccountId & "</MerchantID>" & vbCrLf & _
		"	  <SiteID>" & sAccountSubId & "</SiteID>" & vbCrLf & _
		"	  <IPAddress>" & sIP & "</IPAddress>" & vbCrLf & _
		"	  <Amount>" & formatNumber(X_Amount, 2, true, false, false) & "</Amount>" & vbCrLf & _
		"	  <CurrencyID>" & GetCurISOName(Cint(X_Currency)) & "</CurrencyID>" & vbCrLf & _
		"	  <FirstName>" & fName & "</FirstName>" & vbCrLf & _
		"	  <LastName>" & sName & "</LastName>" & vbCrLf & _
		"	  <Phone>" & Replace(Replace(X_PhoneNumber, " ", ""), "-", "") & "</Phone>" & vbCrLf & _
		"	  <Address>" & BACHAddr1 & "</Address>" & vbCrLf & _
		"	  <City>" & BACity & "</City>" & vbCrLf & _
		"	  <State>" & BAStateCode & "</State>" & vbCrLf & _
		"	  <Country>" & BACountryCode & "</Country>" & vbCrLf & _
		"	  <ZipCode>" & BAZipCode & "</ZipCode>" & vbCrLf & _
		"	  <Email>" & X_Email & "</Email>" & vbCrLf & _
		"	  <CardNumber>" & Replace(X_ccNumber, " ", "") & "</CardNumber>" & vbCrLf & _
		"	  <CardName>" & X_ccHolderName & "</CardName>" & vbCrLf & _
		"	  <ExpiryMonth>" & X_ccExpMM & "</ExpiryMonth>" & vbCrLf & _
		"	  <ExpiryYear>20" & X_ccExpYY & "</ExpiryYear>" & vbCrLf & _
		"	  <CardCVV>" & X_ccCVV2 & "</CardCVV>" & vbCrLf & _
		"	  <MerchantReference>" & X_DebitReferenceCode & "</MerchantReference>" & vbCrLf & _
		"	</" & soapEvnelope & ">" & vbCrLf & _
		"  </soap12:Body>" & vbCrLf & _
		"</soap12:Envelope>"
	End if

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    If isTestTerminal Then UrlAddress = "https://service.381808.com/Merchant.asmx" _
	Else UrlAddress = "https://service.381808.com/Merchant.asmx" 'new
	
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false', sAccountSubId, sAccountPassword
		HttpReq.setRequestHeader "Content-Type", "application/soap+xml; charset=utf-8"
		'HttpReq.setRequestHeader "SOAPAction", "process"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		TimeControl.Append("A_SEND") 'Time Optimization *********

		If Len(sResDetails) = 0 Then Call throwError("521")
		sDebitReturnAnswer = Left(sResDetails, 3900)
	
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "<CardCVV>" & X_ccCVV2, "<CardCVV>" & GetSafePartialNumber(X_ccCVV2))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

		Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
		xmlRet.LoadXML HttpReq.responseText
		sReturnCode = Trim(xmlRet.selectSingleNode("//Status").text) 'State
		sApprovalNumber = xmlRet.selectSingleNode("//TransactionID").text
		X_CustomErrorDesc = xmlRet.selectSingleNode("//Message").text

	On Error Goto 0
	If (Len(sReturnCode) < 1) Then Call throwError("520")
	
	If Trim(sReturnCode) = "1" Then
		sReturnCode = "000"
	ElseIf Trim(sReturnCode) = "3" Then 
		sReturnCode = "001"
	Else
		sReturnCode = "1" & Trim(sReturnCode)
	End if	

	Set xmlRet = Nothing
	Set HttpReq = Nothing

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>