<%
'---------------------------------------------------------------------
'	Debit WebbillingHpp
'---------------------------------------------------------------------

Function WebbillingHpp_Base64Encode(inData)
  Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
  Dim cOut, sOut, I
  For I = 1 To Len(inData) Step 6
    Dim nGroup, pOut
    nGroup = &H10000 * EVal("&h" & Mid(inData, I, 2))
	 If I + 3 <= Len(inData) Then nGroup = nGroup + &H100 * EVal("&h" & Mid(inData, I + 2, 2))
	 If I + 5 <= Len(inData) Then nGroup = nGroup + EVal("&h" & Mid(inData, I + 4, 2))
    nGroup = Oct(nGroup)
    nGroup = String(8 - Len(nGroup), "0") & nGroup
    pOut = Mid(Base64, CLng("&o" & Mid(nGroup, 1, 2)) + 1, 1) + _
      Mid(Base64, CLng("&o" & Mid(nGroup, 3, 2)) + 1, 1) + _
      Mid(Base64, CLng("&o" & Mid(nGroup, 5, 2)) + 1, 1) + _
      Mid(Base64, CLng("&o" & Mid(nGroup, 7, 2)) + 1, 1)
    sOut = sOut + pOut
  Next
  Select Case Len(inData) Mod 6
    Case 2: '8 bit final
      sOut = Left(sOut, Len(sOut) - 2) + "=="
    Case 4: '16 bit final
      sOut = Left(sOut, Len(sOut) - 1) + "="
  End Select
  WebbillingHpp_Base64Encode = sOut
End Function

Function WebbillingHpp_Encrypt(ByVal strData, strKey)
    Dim strCrypted
    While Len(strKey) < Len(strData)
        strKey = strKey & strKey
    Wend
    For i = 1 To Len(strData)
        byteHex = Hex(Asc(Mid(strData, i, 1)) Xor Asc(Mid(strKey, i, 1)))
        if Len(byteHex) < 2 Then byteHex = "0" & byteHex
        strCrypted = strCrypted & byteHex
    Next
    WebbillingHpp_Encrypt = Server.URLEncode(WebbillingHpp_Base64Encode(strCrypted))
End Function

Function remoteCharge_echeck_WebbillingHpp_BackDetect()
	Dim outString, fName, sName, iRs, signature
	sOuterIP = IIF(session("Identity") = "Local", "http://80.179.39.10:8080/Default.aspx", Server.URLEncode(Replace(session("ProcessURL"), "http://", "https://")) & "remoteCharge_echeckWebbillingNotify.asp")
	sql = "Select tblCompanyTransPending.*, tblCheckDetails.*, tblBillingAddress.*, tblDebitTerminals.accountId, tblDebitTerminals.accountSubId , dbo.GetDecrypted256(tblDebitTerminals.accountPassword256) As accountPassword" & _
		 " From tblCompanyTransPending " & _
		 " Left Join tblDebitTerminals ON(tblDebitTerminals.TerminalNumber = tblCompanyTransPending.TerminalNumber)" & _
         " Left Join tblCheckDetails ON (tblCompanyTransPending.CheckDetailsID = tblCheckDetails.id)" & _
         " Left Join tblBillingAddress ON tblCheckDetails.BillingAddressId = tblBillingAddress.id" & _
		 " Where companyTransPending_id=" & xTransID
	Set iRs = oleDbData.Execute(sql)
    If Not iRs.EOF Then
        
	    xNames = Split(Trim(iRs("AccountName")), " ")
	    If Ubound(xNames) > 0 Then fName = xNames(0)
        If Ubound(xNames) > 1 Then sName = xNames(1)
		outString = _
			"<html><head></head><body>" & vbCrLf & _
            "<form id=""dataForm"" action=""http://" & IIF(isTestTerminal, "test", "") & "joinpage.webbilling.com/"" method=""post"">" & vbCrLf & _
            "<input type=""hidden"" name=""merchantid"" value=""" & iRs("accountId") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""merchantpass"" value=""" & WebbillingHpp_Encrypt(iRs("accountPassword"), iRs("accountSubId")) & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""desc"" value=""" & sCompanyDescriptor & """ />" & vbCrLf
        'user data
		outString = outString & _
            "<input type=""hidden"" name=""email"" value=""" & iRs("Email") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""fname"" value=""" & fName & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""lname"" value=""" & sName & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""street"" value=""" & iRs("address1") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""zip"" value=""" & iRs("zipCode") & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""city"" value=""" & iRs("city") & """ />" & vbCrLf
        'subscription information
		outString = outString & _
            "<input type=""hidden"" name=""product_code"" value=""" & WebbillingHpp_Encrypt(iRs("trans_amount") & "|0", iRs("accountSubId")) & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""product_currency"" value=""" & GetCurISOName(iRs("trans_currency")) & """ />" & vbCrLf & _
            "<input type=""hidden"" name=""bnk_stmt_descr"" value=""" & sCompanyDescriptor & """ />" & vbCrLf
        'hidden data for internal use
		'outString = outString & _
        '    "<input type=""hidden"" name=""transactionid"" value=""" & iRs("DebitReferenceCode") & """ />" & vbCrLf
        'optional postback data
		outString = outString & _
            "<input type=""hidden"" name=""processor_id"" value=""" & iRs("DebitReferenceCode") & """ />" & vbCrLf
            '"<input type=""hidden"" name=""webmaster"" value=""webmasterid"" />" & vbCrLf & _
            '"<input type=""hidden"" name=""username"" value=""myusername"" />" & vbCrLf & _
            '"<input type=""hidden"" name=""password"" value=""userpassword"" />" & vbCrLf
        'optional urls if no partner specified
		outString = outString & _
            "<input type=""hidden"" name=""callback_url"" value=""" & sOuterIP & "?debit_Webbilling=Asyn&"" />" & vbCrLf & _
            "<input type=""hidden"" name=""post_back_url"" value=""" & sOuterIP & "?debit_Webbilling=Succ&"" />" & vbCrLf & _
            "<input type=""hidden"" name=""return_url"" value=""" & sOuterIP & "?debit_Webbilling=Fail&"" />" & vbCrLf
            '"<input type=""hidden"" name=""jumpback_url"" value=""" & sOuterIP & "?debit_Webbilling=ON&"" />" & vbCrLf
            '"<input type=""hidden"" name=""jumpback_select"" value="" />" & vbCrLf
        'Optional variables so set the look of the template
		outString = outString & _
            "<input type=""hidden"" name=""bgcolor"" value=""white"" />" & vbCrLf & _
            "<input type=""hidden"" name=""color"" value=""black"" />" & vbCrLf
        '    "<input type=""hidden"" name=""acolor"" value=""blue"" />" & vbCrLf & _
        '    "<input type=""hidden"" name=""tpl"" value=""1"" />" & vbCrLf & _
        '    "<input type=""hidden"" name=""param1"" value=""paramstring1"" />" & vbCrLf & _
        '    "<input type=""hidden"" name=""param2"" value=""paramstring2"" />" & vbCrLf & _
        '    "<input type=""hidden"" name=""param3"" value=""paramstring3"" />" & vbCrLf & _
        '    "<input type=""hidden"" name=""param4"" value=""paramstring4"" />" & vbCrLf & _
        '    "<input type=""hidden"" name=""param5"" value=""paramstring5"" />" & vbCrLf & _
		outString = outString & _
            "<input type=""hidden"" name=""btnSubmit"" value=""submit"" />" & vbCrLf & _
            "</form>" & vbCrLf & _
            "<script type=""text/javascript"" defer=""defer"">document.getElementById('dataForm').submit();</" & "script>" & vbCrLf & _
			"</body></html>"
	End If
	iRs.Close()
	Response.Write(outString)
End function

Function remoteCharge_echeck_WebbillingHpp()
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		Call throwError("532") 'Reftrans not Found - Not Supported
	Else
        'Dim xNames
	    'xNames = Split(Trim(X_ccHolderName), " ")
	    'if Ubound(xNames) < 1 Then Call throwError("517")
		X_DebitReferenceCode = GetTransRefCode()
	    X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
	    'Response.Write(X_3dRedirect)
		sReturnCode = "553"
	End if
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>