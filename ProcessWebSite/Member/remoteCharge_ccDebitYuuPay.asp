<%
'---------------------------------------------------------------------
'	Debit JCC ()
'---------------------------------------------------------------------
function debitYuuPay()
    Dim HttpReq, UrlAddress, ParamList, fName, sName, EncodedSignature, sApprovalNumber, strAct, BrandName, OldRefCode, sResDetails
    BrandName = Array("--", "--", "VISA", "DINERS", "AMEX", "MASTER")
    xNames = Split(X_ccHolderName, " ")
    if Ubound(xNames) > -1 Then fName = xNames(0)
    if Ubound(xNames) > 0 Then sName = xNames(1)
    sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
    if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
    UrlAddress = IIF(isTestTerminal, "https://test.ctpe.io/payment/ctpe", "https://ctpe.io/payment/ctpe")
    Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
    SendMode = IIF(isTestTerminal, "INTEGRATOR_TEST", "LIVE") 'TEST
    ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & VbCrLf & _
		"<Request version=""1.0"">" & VbCrLf & _
		" <Header>" & VbCrLf & _
		"  <Security sender=""" & sAccountSubId & """ />" & VbCrLf & _
		" </Header>" & VbCrLf
     If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
 	     If X_DebitReferenceNum = "" Then Call throwError("532") 'Reftrans Not Found
		 X_DebitReferenceCode = GetTransRefCode()
	     PCode = IIF((Cint(X_TypeCredit) = 0), "RF", "CP")
	     ParamList = ParamList & _
			" <Transaction mode=""" & SendMode & """ response=""SYNC"" channel=""" & sAccountAuthCode1 & """>" & VbCrLf & _
			"  <User login=""" & sAccountId & """ pwd=""" & sAccountPassword & """ />" & VbCrLf & _
			"  <Identification>" & VbCrLf & _
			"	<TransactionID>" & X_DebitReferenceCode & "</TransactionID>" & _
			"	<ReferenceID>" & X_DebitReferenceNum & "</ReferenceID>" & _
			"  </Identification>" & VbCrLf & _
			"  <Payment code=""CC." & PCode & """>" & VbCrLf & _
			"   <Presentation>" & _
			"    <Amount>" & X_Amount & "</Amount>" & _
			"    <Currency>" & GetCurISOName(Cint(X_Currency)) & "</Currency>" & _
			"    <Usage>" & sCompanyDescriptor & "</Usage>" & _
			"   </Presentation>" & _
			"  </Payment>" & _
			" </Transaction>" & VbCrLf & _
			"</Request>"
			
		'Response.Write(sTerminalChargeOptions & "<br>")
		'Response.Write(ParamList)
		'Response.End	
	 Else
		 X_DebitReferenceCode = GetTransRefCode()
	     PCode = IIF(Cint(X_TransType) = 1, "PA", "DB")
		 ParamList = ParamList & " <Transaction mode=""" & SendMode & """ response=""SYNC"" channel=""" & sAccountAuthCode1 & """>" & VbCrLf & _
			"  <User login=""" & sAccountId & """ pwd=""" & sAccountPassword & """ />" & VbCrLf & _
			"  <Identification>" & VbCrLf & _
			"   <TransactionID>" & X_DebitReferenceCode & "</TransactionID>" & VbCrLf & _
			"  </Identification>" & VbCrLf & _
			"  <Payment code=""CC." & PCode & """>" & VbCrLf & _
			"   <Presentation>" & VbCrLf & _
			"    <Amount>" & X_Amount & "</Amount>" & VbCrLf & _
			"    <Currency>" & GetCurISOName(Cint(X_Currency)) & "</Currency>" & VbCrLf & _
			"    <Usage>" & sCompanyDescriptor & "</Usage>" & VbCrLf & _
			"   </Presentation>" & VbCrLf & _
			"  </Payment>" & VbCrLf & _
			"  <Account>" & VbCrLf & _
			"   <Holder><![CDATA[" & X_ccHolderName & "]]></Holder>" & VbCrLf & _
			"   <Number>" & X_ccNumber & "</Number>" & VbCrLf & _
			"   <Brand>" & BrandName(ccTypeID) & "</Brand>" & VbCrLf & _
			"   <Expiry month=""" & X_ccExpMM & """ year=""20" & X_ccExpYY & """ />" & VbCrLf & _
			"   <Verification>" & X_ccCVV2 & "</Verification>" & VbCrLf & _
			"  </Account>" & VbCrLf & _
			"  <Customer>" & VbCrLf & _
			"   <Name>" & VbCrLf & _
			"    <Given><![CDATA[" & sName & "]]></Given>" & VbCrLf & _
			"    <Family><![CDATA[" & fName & "]]></Family>" & VbCrLf & _
			"   </Name>" & VbCrLf & _
			"   <Address>" & VbCrLf & _
			"    <Street><![CDATA[" & BACHAddr1 & "]]></Street>" & VbCrLf & _
			"    <Zip>" & BAZipCode & "</Zip>" & VbCrLf & _
			"    <City>" & BACity & "</City>" & VbCrLf & _
			"    <State>" & BAStateCode & "</State>" & VbCrLf & _
			"    <Country>" & BACountryCode & "</Country>" & VbCrLf & _
			"   </Address>" & VbCrLf & _
			"   <Contact>" & VbCrLf & _
			"    <Email>" & X_Email & "</Email>" & VbCrLf & _
			"    <Ip>" & sIP & "</Ip>" & VbCrLf & _
			"   </Contact>" & VbCrLf & _
			"  </Customer>" & VbCrLf & _
			" </Transaction>" & VbCrLf & _
			"</Request>"
	End if		
    On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		'HttpReq.setRequestHeader "Accept-Language", "en-US"
		HttpReq.send "load=" & ParamList
		sResDetails = HttpReq.responseText
		SaveLogChargeAttemptRequestResponse ParamList, sResDetails
	    HttpError = FormatHttpRequestError(HttpReq)
		Set xmlRet = HttpReq.responseXml
    On Error GoTo 0
	If IsNull(sResDetails) Or Len(sResDetails) = 0 Then Call throwError("521")
	sReturnCode = xmlRet.selectSingleNode("//Return").attributes.getNamedItem("code").text
    If sReturnCode = "" Then Call throwError("520") 'internal error
	'sReturnCode = xmlRet.selectSingleNode("//Result").text
	'If sReturnCode = "ACK" Then
	If sReturnCode = "000.000.000" Or sReturnCode = "000.100.110" Or sReturnCode = "000.100.111" Or sReturnCode = "000.100.112" Or sReturnCode = "000.300.000" _
		Or sReturnCode = "000.400.000" Or sReturnCode = "000.400.010" Or sReturnCode = "000.400.020" Or sReturnCode = "000.400.040" _
		Or sReturnCode = "000.400.050" Or sReturnCode = "000.400.060" Or sReturnCode = "000.400.070" Or sReturnCode = "000.500.000" _
		Or sReturnCode = "000.500.100" Or sReturnCode = "000.600.000" Then

		sReturnCode = "000"
		sApprovalNumber = xmlRet.selectSingleNode("//ShortID").text
		X_DebitReferenceNum = xmlRet.selectSingleNode("//UniqueID").text
	Else
		sReturnCode = xmlRet.selectSingleNode("//Reason").attributes.getNamedItem("code").text
	    X_CustomErrorDesc = xmlRet.selectSingleNode("//Return").text
	End If
    'If (Cint(X_TypeCredit) = 0) And (sReturnCode <> "000") Then FileAppendData "YuuPay.txt", Now & ", " & sReturnCode & vbcrlf & ParamList & vbcrlf & sResDetails & vbcrlf & vbcrlf & vbcrlf
    'Response.Write(ParamList & VbCrLf & VbCrLf & xmlRet.Xml & VbCrLf & VbCrLf & sReturnCode)
    'Response.End
    Set HttpReq = Nothing
   	Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
end function
%>