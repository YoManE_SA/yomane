<%
	If Left(Request("REMOTE_ADDR"), 8) <> "192.168." Then
		Response.Write("ACCESS DENIED")
	Else
		If LCase(Request("Action")) = "send" Then
			Dim sTo As String = Request("Number")
			If String.IsNullOrEmpty(sTo) Then
				Response.Write("Missing Number")
			ElseIf sTo.Length <> 10 Or dbPages.TestVar(sTo, 1, 0, 0) = 0 Then
				Response.Write("Invalid Number")
			Else
				Dim sText As String = Request("Text")
				If String.IsNullOrEmpty(sText) Then
					Response.Write("Missing Text")
				Else
					If LCase(SMS.Send(sTo, sText)) <> "true" Then
						Response.Write("Failed to send")
					Else
						Response.Write("OK")
					End If
				End If
			End If
			Response.Write(";")
		End If
		Response.Write(SMS.Credits)
	End If
%>