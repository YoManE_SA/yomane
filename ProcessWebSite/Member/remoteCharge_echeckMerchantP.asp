<%
   '---------------------------------------------
   '       clear through merchant partners
   '---------------------------------------------
   function clearMerchantPartners()
       '---------------------------------------------
       '          build and send the request
       '---------------------------------------------
	    Dim sendStr
	    sendStr = "https://trans.merchantpartners.com/cgi-bin/process.cgi?"
		if(creditType = "0") Then sendStr = sendStr & "action=ns_credit&" _
		else sendStr = sendStr & "action=ns_quicksale_check&"
	    sendStr = sendStr & "acctid=" & sAccountId & "&"
	    sendStr = sendStr & "subId=" & sAccountSubId & "&"
	    sendStr = sendStr & "amount=" & X_Amount & "&"
	    sendStr = sendStr & "ckname=" & X_ccHolderName & "&"
	    sendStr = sendStr & "ckaba=" & ckaba & "&"
	    sendStr = sendStr & "ckacct=" & ckacct & "&"
	
		On Error Resume Next
		
		    Dim HttpReq
			Set HttpReq = CreateObject("Microsoft.XMLHTTP")
		    HttpReq.open "get", sendStr, False
		    HttpReq.send
	  		responseStr = HttpReq.responseText	
	  		'response.Write responseStr : response.End		
	  		
		On Error GoTo 0

       '---------------------------------------------
       '       receive and parse the response
       '---------------------------------------------
       'Accepted=CHECKAUTH:TEST:::53066079::: (Accepted/Declined.. complete discription under Appendix A of the integration guide)
       'historyid=53066079  (unique id for each transaction processed)
       'orderid=42219041 (unique id for each order.  each order can have multiple historyids (recurrings, credits, voids).
       'Accepted=CHECKAUTH:TEST:::53066079:::
       'ACCOUNTNUMBER=*****9999 (last four digits of the account number)
       'authcode=TEST (authorization code)
       'AuthNo=CHECKAUTH:TEST:::53066079::: (same as Accepted variable)
       'historyid=53066079
       'orderid=42219041
       'recurid=0
       'refcode=53066079-TEST (historyid-authorization code)
       'result=1
       'Status=Accepted
       'transid=0
       '---------------------------------------------

	    '----- check returned string length
	    if len(responseStr) = 0 or responseStr = null then call throwError("521")
	    
	    '----- check for duplicate transacion
	    searchResult = inStr(1, responseStr, "DUPLICATE")
	       if searchResult > 0 then call throwError("529")
	       
	       '----- build dictionary
	    dim dict
	    set dict = createObject("Scripting.Dictionary")
	    responseStrArr = Split(responseStr, vbcr)
	
	    for each currentItem in responseStrArr
		    if inStr(currentItem, "=") then
			    currentItemStr = replace(currentItem, vbCr, "")
			    currentItemStr = replace(currentItem, vbLf, "")
	
			    currentItemArr = split(currentItemStr, "=")
			    currentItemName = currentItemArr(0)
			    currentItemValue = currentItemArr(1)
	
			    if not dict.exists(currentItemName) then dict.add currentItemName, currentItemValue
		    end if
	    next

       '------------------------------------------------------------------------------------
       '			check for reply status
       '			missing reply status indicates an invalid reply
       '			and a communication problem
       '------------------------------------------------------------------------------------
		if not dict.exists("Status") then call throwError("521")

       '---------------------------------------------
       '             process the response
       '---------------------------------------------
       status = dict.Item("Status")
       accNumLast4 = replace(dict.Item("ACCOUNTNUMBER"),"*","")
       'confirmationNumber = dict.Item("historyid")
       companyId = rsData("ID")
       responseStrSql = replace(replace(replace(responseStr, vbCr, ";"), vbLf, ""), "'", "''")
       if status = "Declined" then
           declinedArr = split(dict.Item("Declined"), ":")
           declinedCode = declinedArr(1)
           declinedMsg = declinedArr(2)
       end if
       set dict = nothing

       If status = "Accepted" then
            DebitIProcessResult "000", declinedCode
       Else
            if (declinedCode = "000") Or (declinedCode = "001") Then declinedCode = "002"
            DebitIProcessResult declinedCode, ""
       End if
   end function
%>