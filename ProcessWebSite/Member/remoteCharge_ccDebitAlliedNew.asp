<%
'---------------------------------------------------------------------
'	Debit debitAlliedNew
'---------------------------------------------------------------------

Function debitAlliedNew_Json(dic)
    Dim ret : ret = ""
    For Each v in dic
        If Len(ret) > 0 Then ret = ret & ", " & vbCrLf
        Dim value: value = dic(v)
        Select Case VarType(value)
        Case vbNull, vbEmpty: value = "null"
        Case vbString: value = """" & value & """"
        Case vbBoolean: value = IIF(value, "true", "false")
        Case vbCurrency: value = formatNumber(value, 2, true, false, false)
        Case Else 
            If Not IsNumeric(value) Then value = """" & value & """"
        End Select
        ret = ret & " """ & v & """: " & value & ""
    Next
    debitAlliedNew_Json = "{" & vbCrLf & ret & vbCrLf & "}"
End Function

Function debitAlliedNew()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer, soapEvnelope, sendData
	If int(X_TypeCredit) = 8 Or int(X_Payments) > 1 Then Call throwError("508") 
	xNames = Split(X_ccHolderName, " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = ""
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = ""
    Set sendData = Server.createObject("Scripting.Dictionary")
    sendData.Add "siteId", sAccountSubId
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		If Cint(X_TypeCredit) = 0 Then
			soapEvnelope = "refundtransactions"
            sendData.Add "referencetransactionid", X_ConfirmNumber
            sendData.Add "amount", X_Amount
		Else
			soapEvnelope = "capturetransactions"
            sendData.Add "authorizetransactionid", X_ConfirmNumber
            sendData.Add "amount", X_Amount
		End If
	Else
        soapEvnelope = IIF(Cint(X_TransType) = 1, "authorizetransactions", "saletransactions")
        X_DebitReferenceCode = GetTransRefCode()
        sendData.Add "currency", GetCurISOName(Cint(X_Currency))
        sendData.Add "firstName", fName
        sendData.Add "lastName", sName        sendData.Add "phone", Replace(Replace(X_PhoneNumber, " ", ""), "-", "")
        sendData.Add "city", BACity
        sendData.Add "state", BAStateCode
        sendData.Add "countryId", BACountryCode
        sendData.Add "postalCode", BAZipCode
        sendData.Add "addressLine1", BACHAddr1
        sendData.Add "addressLine2", BACHAddr2
        'sendData.Add "shippingFirstName", ""
        'sendData.Add "shippingLastName", ""
        'sendData.Add "shippingPhone", ""
        'sendData.Add "shippingAddressLine1", ""
        'sendData.Add "shippingAddressLine2", ""
        'sendData.Add "shippingCity", "sample string 18"
        'sendData.Add "shippingState", "sample string 19"
        'sendData.Add "shippingCountryId", ""
        'sendData.Add "shippingPostalCode", ""
        sendData.Add "email", X_Email
        sendData.Add "cardNumber", Replace(X_ccNumber, " ", "")  
        sendData.Add "nameOnCard", X_ccHolderName
        sendData.Add "expirationMonth", CInt(X_ccExpMM)
        sendData.Add "expirationYear", CInt("20" + X_ccExpYY)
        sendData.Add "cvvCode", X_ccCVV2
        sendData.Add "ipAddress", sIP
        'sendData.Add "trackingId", ""
        sendData.Add "amount", X_Amount
        'sendData.Add "isInitialForRecurring", false
        ParamList = debitAlliedNew_Json(sendData)
	End if

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    If isTestTerminal Then UrlAddress = "https://api.alliedwallet.com/merchants/" _
	Else UrlAddress = "https://api.alliedwallet.com/merchants/" 'new
    UrlAddress = UrlAddress & sAccountId & "/" & soapEvnelope
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false', sAccountSubId, sAccountPassword
		HttpReq.setRequestHeader "Content-Type", "application/json"
		HttpReq.setRequestHeader "Authorization", "Bearer " & "N3EwJiFnNWI=" 'sAccountPassword
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		TimeControl.Append("A_SEND") 'Time Optimization *********
        
        Response.Write(UrlAddress & vbCrLf & "<br>Requst:" & vbCrLf & ParamList & vbCrLf & "<br>Response:" & vbCrLf & sResDetails & vbCrLf & "<br>")
        
		If Len(sResDetails) = 0 Then Call throwError("521")
		sDebitReturnAnswer = Left(sResDetails, 3900)
	
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, """cvvCode""=""" & X_ccCVV2, """" & GetSafePartialNumber(X_ccCVV2))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

        Set jsonResult = JSON.parse(sResDetails)
		sReturnCode = Trim(jsonResult.status)
        X_DebitReferenceCode = jsonResult.trackingid
		X_CustomErrorDesc = jsonResult.message
		sApprovalNumber = jsonResult.id
	On Error Goto 0
	If (Len(sReturnCode) < 1) Then Call throwError("520")
	
	If Trim(sReturnCode) = "Successful" Then 
        sReturnCode = "000"
	ElseIf Trim(sReturnCode) = "Pending" Then 
        sReturnCode = "001"
	ElseIf Trim(sReturnCode) = "Error" Then 
        sReturnCode = "002"
	ElseIf Trim(sReturnCode) = "Declined" Then 
        sReturnCode = "003"
	ElseIf Trim(sReturnCode) = "Scrubbed" Then 
        sReturnCode = "004"
	ElseIf Trim(sReturnCode) = "Fraud" Then 
        sReturnCode = "005"
	ElseIf Trim(sReturnCode) = "Unconfirmed" Then 
        sReturnCode = "006"
	Else 
        sReturnCode = "100"
	End if	

	Set xmlRet = Nothing
	Set HttpReq = Nothing

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>