<%
Dim bIsNetpayTerminal, bUseOldTerminal : bUseOldTerminal = True
Dim RedirectUrlUseScript: RedirectUrlUseScript = False
Dim CartStockChanged : CartStockChanged = False
Dim X_DeniedAdminComment : X_DeniedAdminComment = ""
Dim X_Is3dSecure : X_Is3dSecure = False
Dim X_MerchantHashKey : X_MerchantHashKey = ""
Dim PaymentMethod, X_ccNumber, ccTypeEngName
Dim X_BatchData : X_BatchData = Empty
Dim LogSavingChargeDataID : LogSavingChargeDataID = 0
Dim CFF_Currency : CFF_Currency = vbNull
Dim isCardPresent : isCardPresent = 0

' 20120208 Tamir - Check if zip code is well-formed for certain country
Function IsValidZipCode(sZipCode, sCountryIso)
	If Trim(sZipCode) = "" Or Trim(sCountryIso) = "" Then
		IsValidZipCode = True
		Exit Function
	End If
	Dim sPattern : sPattern = ExecScalar("SELECT ZipCodeRegExPattern FROM [List].[CountryList] WITH (NOLOCK) WHERE CountryISOCode = '" & sCountryIso & "'", "")
	If sPattern = "" Then
		IsValidZipCode = True
		Exit Function
	End If
	Dim regexZip : Set regexZip = New RegExp
	regexZip.Pattern = sPattern
	regexZip.IgnoreCase = True
	IsValidZipCode = regexZip.Test(Trim(sZipCode))
	Set regexEmail = Nothing
End Function

' 20110904 Tamir - Check if email address is well-formed
Function IsValidEmail(sEmail)
	If inStr(sEmail, "..") > 0 Then
		IsValidEmail = False
		Exit Function
	End If
	Dim regexEmail : Set regexEmail = New RegExp
	regexEmail.Pattern = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\w"
	regexEmail.IgnoreCase = True
	IsValidEmail = regexEmail.Test(Replace(Replace(sEmail, "-", "_"), "~", "_"))
	Set regexEmail = nothing
End Function

Function GetTransPayDate(cmpID, xTransDate, xCreditType)
	Dim tRs, i, xRet
	If int(xCreditType) = 0 Then
		GetTransPayDate = xTransDate
		Exit Function
	End if
	GetTransPayDate = DateSerial(1901, 1, 1)
	Set tRs = oledbData.Execute("Select PayingDaysMarginInitial, PayingDaysMargin, payingDates1, payingDates2, payingDates3, Cast(ma.DateFirstTransPass as datetime) as DateFirstTransPass From tblCompany" & _
		" Left Join [Track].[MerchantActivity] ma ON (ma.Merchant_id = tblCompany.ID) Where ID=" & cmpID)
	If Not tRs.EOF Then
		If (tRs("PayingDaysMarginInitial") > 0) And (DateDiff("d", IIf(IsNull(tRs("DateFirstTransPass")), Now, tRs("DateFirstTransPass")), Now) < tRs("PayingDaysMarginInitial")) Then
			GetTransPayDate = DateAdd("d", tRs("PayingDaysMarginInitial"), xTransDate)
		ElseIf tRs("PayingDaysMargin") > 0 Then 
			GetTransPayDate = DateAdd("d", tRs("PayingDaysMargin"), xTransDate)
		Else
			On Error Resume Next
			nDay = Day(xTransDate)
			For i = 1 To 3
				xRet = IsDayInRange(tRs("payingDates" & i), nDay)
				If xRet > -1 Then
					GetTransPayDate = DateSerial(Year(xTransDate), Month(xTransDate) + 1, xRet)
					'Month(xTransDate) + IIf(xRet < nDay, 1, 0)
					Exit For
				End If	
			Next
			On Error Goto 0
		End If	
	End If
	tRs.Close
End Function 

Function GetTransInfo(sWhere)
	Dim iRs, xParams
	GetTransInfo = False
	Set iRs = oleDbData.Execute("Select * From tblLogChargeAttempts Where " & sWhere)
	If Not iRs.EOF Then
		xLogChargeAttemptID = iRs("LogChargeAttempts_id")
		xMerchantNum = iRs("Lca_MerchantNumber")
		xRequestParams = Replace(iRs("Lca_QueryString") & "|" & iRs("Lca_RequestForm"), "|", "&")
		xSendParams = iRs("Lca_RequestString")
		xRetParams = iRs("Lca_ResponseString")
		xDebitCompany = iRs("Lca_DebitCompanyID")
		xLogReplyCode = iRs("Lca_ReplyCode")
		GetTransInfo = True
	End If
	iRs.Close()
End Function

'20100516 Tamir
'Transaction type - according to GlobalData(37)
Sub LogCreditCardWhitelist(nWhitelistID, nLevel, nMerchant, nTransactionID, nTransactionType)
	sSQL = "EXEC AddLogCreditCardWhitelist " & nWhitelistID & ", " & nLevel & ", " & nMerchant & ", " & nTransactionID
	If Trim(nTransactionType) <> "" Then sSQL = sSQL & ", " & nTransactionType
	oledbData.Execute sSQL
End Sub

'20090908 Tamir
Function GetIPCountry()
	If X_IPCountry = "" Then
		If fraudDetectionLog_id <> "" And fraudDetectionLog_id <> "0" Then
			X_IPCountry = Right("--" & Trim(ExecScalar("Select ReturnBinCountry From [Log].[FraudDetection] Where FraudDetection_id=" & fraudDetectionLog_id, "--")), 2)
		Else
			X_IPCountry = "--"
		End If
	End If
	GetIPCountry = X_IPCountry
End Function

'
' 20081104 Tamir
'
' Creating recurring series.
'
' nTransType: 0=pre-auth, 1=charge
'
Function CreateRecurringSeries(nTransType, nTransID)
	If Len(nTransID) > 0 Then
		If IsNumeric(Trim(nTransID)) Then nTransID = CLng(nTransID)
	Else
		nTransID = 0
	End If
	If nRecurringCount > 0 And TestNumVar(nTransID, 1, 0, 0) > 0 Then
		'If sCompanyID="35" Then 
			sSQL = "EXEC RecurringPrecreateSeries " & TestNumVar(nTransType, 1, 0, 0) & ", " & TestNumVar(nTransID, 1, 0, 0) & ", '"
		'Else
		'	sSQL = "EXEC RecurringBatchCreateFlexibleSeriesEx " & TestNumVar(nTransType, 1, 0, 0) & ", " & TestNumVar(nTransID, 1, 0, 0) & ", '"
		'End If    
		Dim i
		For i = 1 To nRecurringCount
			sSQL = sSQL & "Recurring" & trim(i) & "=" & trim(saRecurring(i)) & "&"
		Next
		sSQL = sSQL & "', '" & DBText(X_Comment) & "', " & TestNumVar(Request("TrmCode"), 0, -1, 0) & ", '" & DBText(Request("ClientIP")) & "'"
		Dim nRecurringTransType : nRecurringTransType = TestNumVar(Request("RecurringTransType"), 0, 3, -1)
		If nRecurringTransType > 0 Then nRecurringTransType = 1
		sSQL = sSQL & ", " & nRecurringTransType
		Dim sIdentifier : sIdentifier = NewRecurringSeriesStoredCardIdentifier
		If Trim(sIdentifier) <> "" Then sSQL = sSQL & ", " & sIdentifier
		nRecurringSeriesTmp = ExecScalar(sSQL, 0)
		If TestNumVar(nRecurringSeriesTmp, 1, 0, 0) > 0 Then nRecurringSeriesReturn = nRecurringSeriesTmp
	End If
End Function

'20100713 Tamir - AutoCapture (automatically capture the pre-auth after X hours, if not cancelled)
Sub CreateAutoCapture(nHours, nID)
	Dim sSQL : sSQL = "INSERT INTO tblAutoCapture(AuthorizedTransactionID, ScheduledDate) VALUES (" & nID & ", DateAdd(hour, " & nHours & ", GetDate()));"
	oledbData.Execute sSQL
End Sub

'---------------------------------------------------------------------
'	insert pass transaction
'---------------------------------------------------------------------
' transactionTypeId = request source id

Function insertPassedTransaction(transactionIdOutParam, insertDateOutParam, companyId, fraudDetectionLog_id, transactionTypeId, iMethodType, creditTypeId, customerId, customerIp, amount, transCurrcency, ccTypeEngString, payments, orderNumber, payFor, productId, comment, terminalNumber, approvalNumber, isTestOnly, PaymentMethod, pDate, cardId, referringUrl, debitCompanyId, transPayerInfo_id, transPaymentMethod_id, PayerID, is3DSecure, deviceId, OriginalTransID)
	Dim sTempChr, xBin, paymentId, i
	If IsEmpty(X_OCurrency) Then X_OCurrency = transCurrcency
	If IsEmpty(X_OAmount) Then X_OAmount = amount
	If PaymentMethod >= PMD_CC_UNKNOWN Then xBin = ExecScalar("Select BINCountry From tblCreditCard Where ID=" & TestNumVar(cardId, 1, 0, 0), "--") Else xBin = "--"
	If bUseOldTerminal Then netpayFeeTransactionCharge = FormatNumber(GetTransactionFees(netpayFeeRatioCharge, companyId, PaymentMethod, X_OCurrency, amount, xBin, creditTypeId), 2, True, False, False) _
	Else netpayFeeTransactionCharge = FormatNumber(GetTransactionFeesEx(netpayFeeRatioCharge, companyId, PaymentMethod, X_OCurrency, amount, creditTypeId, terminalNumber), 2, True, False, False)
	debitFeeTransactionCharge = FormatNumber(GetTransactionDebitFees(debitCompanyId, terminalNumber, PaymentMethod, transCurrcency, amount, creditTypeId, pDate, 0, netpayFeeRatioCharge, 0), 2, True, False, False)
	netpayFeeRatioCharge = FormatNumber(netpayFeeRatioCharge, 2, True, False, False)

	'20101209 Tamir - CCF statistics columns are moved from tblCompany to tblMerchantProcessingData,
	'while CFF_Currency is retrieved in the general query in the beginning of the remote_charge.asp
	Dim sSqlCFF : sSqlCFF = ""
	If creditTypeId <> 0 And Not IsNull(CFF_Currency) Then
		sSQL="SELECT MPD_CffCurAmount, MPD_CffResetDate FROM tblMerchantProcessingData WITH (NOLOCK) WHERE MerchantID=" & companyId
		set rsData2 = oledbData.Execute(sSQL)
		If Not rsData2.EOF Then
			CFF_CurAmount = rsData2("MPD_CffCurAmount")
			CFF_ResetDate = rsData2("MPD_CffResetDate")
			If IsNull(CFF_ResetDate) Then CFF_ResetDate = Now
			If (Month(Now) <> Month(CFF_ResetDate)) Or (Year(Now) <> Year(CFF_ResetDate)) Then
				ExecSql "UPDATE tblMerchantProcessingData SET MPD_CffResetDate=GetDate(), MPD_CffCurAmount=0 WHERE MerchantID=" & companyId
				CFF_CurAmount = 0
			End If
			CCF_Precent = TestNumVar(ExecScalar("SELECT CFF_Precent FROM tblCompanyFeesFloor WITH (NOLOCK) WHERE CFF_CompanyID=" & companyId & " AND CFF_TotalTo BETWEEN 0 And " & CFF_CurAmount & " ORDER BY CFF_TotalTo DESC", 0), 0, -1, 0)
			If CCF_Precent > 0 Then netpayFeeRatioCharge = amount * (CCF_Precent / 100)
			sSqlCFF = "UPDATE tblMerchantProcessingData SET MPD_CffCurAmount=MPD_CffCurAmount+" & ConvertCurrency(transCurrcency, CFF_Currency, amount) & " WHERE MerchantID=" & companyId
		End If
		rsData2.Close
	End If

	sInsertDate = Now()
	sTempChr = "0"
	If Not bIsNetpayTerminal Then sTempChr = "X"
	If int(creditTypeId) = 8 AND int(payments) > 1 AND trim(sTempChr) = "0" Then
		For i = 1 To int(payments)
			paymentId = paymentId & ";" & sTempChr
		Next
		paymentId = paymentId & ";"
	Else
		paymentId = ";" & sTempChr & ";"
	End If
	If IsEmpty(CTP_Status) Then CTP_Status = 0
	MerchantPD = GetTransPayDate(companyId, Now, creditTypeId)
	' 20090224 Tamir - Save recurring series&chargenumber in transpass table (for remote data pulling)
	' 20090706 Tamir - when capturing pre-auth. transaction from recurring series, copy RecurringSeries and RecurringCharge to the passed transaction
	If nApprovalRecurringSeries = "NULL" Then
		nRecurringSeries = "NULL"
		nRecurringChargeNumber = "NULL"
		If trim(request("RequestSource"))="20" And TestNumVar(Request("RecurringCharge"), 1, 0, 0)>0 Then
			sSQL="SELECT rc_Series, rc_ChargeNumber FROM tblRecurringCharge WHERE ID=" & Request("RecurringCharge")
			Set rsData2=oledbData.Execute(sSQL)
			If Not rsData2.EOF Then
				nRecurringSeries = rsData2("rc_Series")
				nRecurringChargeNumber = rsData2("rc_ChargeNumber")
			End If
			rsData2.Close
		End If
	Else
		nRecurringSeries = nApprovalRecurringSeries
		nRecurringChargeNumber = nApprovalRecurringChargeNumber
	End If

	transactionIdOutParam = -1
	Dim nUnsettledAmount, nUnsettledInstallments
	If UCase(paymentId)=";X;" Then
		nUnsettledAmount = 0
	ElseIf creditTypeId = 0 Then
		nUnsettledAmount = -amount
	Else
		nUnsettledAmount = amount
	End If
	If UCase(paymentId) = ";X;" Then
		nUnsettledInstallments = 0
	ElseIf creditTypeId=6 Then
		nUnsettledInstallments = 1
	Else
		nUnsettledInstallments = payments
	End If
	
	Dim sRefTrans : sRefTrans = Trim(dbText(request("RefTransType")))
	If sRefTrans <> "" And X_RefTransID > 0 Then
		sRefTrans = Trim(dbText(Request("RefTransType"))) & Trim(X_RefTransID)
		If Len(sRefTrans) > 20 Then sRefTrans = Left(sRefTrans, 20)
		sRefTrans = "'" & sRefTrans & "'"
	Else
		sRefTrans = "NULL"
	End If

	sSQL="SET NOCOUNT ON;INSERT INTO tblCompanyTransPass " & _
		"(companyID, insertDate, fraudDetectionLog_id, TransSource_id, payID, CreditType, CustomerID, IPAddress," & _
		" Amount, Currency, Payments, OrderNumber, PayforText, MerchantProduct_id, Comment, TerminalNumber, ApprovalNumber," & _
		" isTestOnly, PD, MerchantPD, PaymentMethod_id, PaymentMethodID, PaymentMethod, paymentMethodDisplay, " & _
		" referringUrl, DebitCompanyID, payerIdUsed, OriginalTransID, NetpayFee_RatioCharge, netpayFee_transactionCharge," & _
		" DebitReferenceCode, DebitReferenceNum, AcquirerReferenceNum, OCurrency, OAmount, IPCountry, CTP_Status, DebitFee, RecurringSeries, RecurringChargeNumber," & _
		" UnsettledAmount, UnsettledInstallments, RefTrans, MobileDevice_id, transPayerInfo_id, transPaymentMethod_id, Is3DSecure, DeniedAdminComment, IsCardPresent) " &_
		" VALUES(" & companyId & ",Convert(datetime, '" & sInsertDate & "', 103)," & fraudDetectionLog_id & "," & transactionTypeId & "," & _
		" LEFT('" & paymentId & "', 305), " & creditTypeId & ", " & customerId & ", LEFT('" & customerIp & "', 50)," & _
		" " & FormatNumber(amount, 2, -1, 0, 0) & ", " & transCurrcency & ", " & payments & ", LEFT('" & dbText(orderNumber) & "', 100)," & _
		" LEFT('" & dbText(payFor) & "', 100), " & IIF(TestNumVar(productId, 0, -1, 0) > 0, productId, "Null") & ", LEFT('" & dbText(comment) & "', 500), LEFT('" & terminalNumber & "', 10), LEFT('" & approvalNumber & "', 50)," & _
		" " & isTestOnly & ", Convert(datetime, '" & pDate & "', 103), Convert(datetime, '" & MerchantPD & "', 103), " & iMethodType & ", " & cardId & "," & PaymentMethod & "," & _
		" LEFT('" & ccTypeEngString & "', 50), LEFT('" & dbText(referringUrl) & "', 500), " & debitCompanyId & "," & _
		" LEFT('" & PayerID & "', 10), " & OriginalTransID & ", " & NetpayFeeRatioCharge & ", " & netpayFeeTransactionCharge & "," & _
		" LEFT('" & X_DebitReferenceCode & "', 40), Left('" & X_DebitReferenceNum & "', 40), " & IIF(X_AcquirerReferenceNum<> "", "Left('" & X_AcquirerReferenceNum & "', 40)", "Null") & ", " & X_OCurrency & ", " & X_OAmount & ", LEFT('" & GetIPCountry() & "', 2)," & _
		" " & CTP_Status & ", " & debitFeeTransactionCharge & ", " & nRecurringSeries & ", " & nRecurringChargeNumber & "," & _
		" " & nUnsettledAmount & ", " & nUnsettledInstallments & ", " & sRefTrans & "," & IIF(deviceId <> "", deviceId, "Null") & _
		"," & IIF(transPayerInfo_id <> "", transPayerInfo_id, "Null") & "," & IIF(transPaymentMethod_id <> "", transPaymentMethod_id, "Null") & "," & IIF(is3DSecure, "1", "0") & ",'" & Replace(X_DeniedAdminComment, "'", "''") & "'," & TestNumVar(isCardPresent, 0, 1, 0) & ");" & _
		"SELECT SCOPE_IDENTITY();SET NOCOUNT OFF;"
	oledbData.CommandTimeout = 180
	On Error Resume Next
	Err.Clear
	Set rsData2 = oledbData.execute(sSQL)
	If Err.Number <> 0 Then 
		GlobalSaveToFile = True
		ExecSQL "-- BEGIN PASS TRANSACTION " & Now & vbCrLf & "DECLARE @TransID int;"
		ExecSQL Replace(sSQL, "SCOPE_IDENTITY", "@TransID=SCOPE_IDENTITY")
		transactionIdOutParam = "@TransID"
		insertDateOutParam = Now
	Else
		transactionIdOutParam = CLng(rsData2(0))
		insertDateOutParam = sInsertDate
	End If
	oledbData.CommandTimeout = 30
	rsData2.Close()
	Set rsData2 = nothing
	On Error Goto 0
	
	'CashBack Flow
	IF ((PaymentMethod=35 Or PaymentMethod=36 Or PaymentMethod=37) Or (PaymentMethod >= PMD_PP_MIN And PaymentMethod <= PMD_PP_MAX)) Then 
		'Get the merchant percent fee
			Dim cashBackPercent 
			cashBackPercent = GetUsageFees(companyId, PaymentMethod, transCurrcency, xBin, 3)
			IF (CDbl(cashBackPercent) <> 0 And creditTypeId=1) Then
			cashBackAmount = (CDbl(cashBackPercent) * amount) / 100

			'Get the passed transaction params
			Set rsData2 = oledbData.execute("Select * From tblCompanyTransPass Where ID="&transactionIdOutParam)
				
			'Create refund transaction
		   CreateRefundSqlCommand = "INSERT INTO tblCompanyTransPass " & _
		"(IsCashBack ,companyID, insertDate, fraudDetectionLog_id, TransSource_id, payID, CreditType, CustomerID, IPAddress," & _
		" Amount, Currency, Payments, OrderNumber, PayforText, MerchantProduct_id, Comment, TerminalNumber, ApprovalNumber," & _
		" isTestOnly, PD, MerchantPD, PaymentMethod_id, PaymentMethodID, PaymentMethod, paymentMethodDisplay, " & _
		" referringUrl, DebitCompanyID, payerIdUsed, OriginalTransID, NetpayFee_RatioCharge, netpayFee_transactionCharge," & _
		" DebitReferenceCode, DebitReferenceNum, AcquirerReferenceNum, OCurrency, OAmount, IPCountry, CTP_Status, DebitFee, RecurringSeries, RecurringChargeNumber," & _
		" UnsettledAmount, UnsettledInstallments, RefTrans, MobileDevice_id, transPayerInfo_id, transPaymentMethod_id, Is3DSecure, DeniedAdminComment) " &_
		" VALUES(1," & companyId & ",GETDATE()," & fraudDetectionLog_id & "," & transactionTypeId & "," & _
		" ';0;',0, " & customerId & ", LEFT('" & customerIp & "', 50)," & _
		" " & FormatNumber(cashBackAmount, 2, -1, 0, 0) & ", " & transCurrcency & ",1,LEFT('" & dbText(orderNumber) & "', 100)," & _
		" LEFT('" & dbText(payFor) & "', 100), " & IIF(TestNumVar(productId, 0, -1, 0) > 0, productId, "Null") & ",'Cashback from trans #" & transactionIdOutParam & "', LEFT('" & terminalNumber & "', 10), '' ," & _
		" " & isTestOnly & ", Convert(datetime, '" & pDate & "', 103), DATEADD(day,1,GETDATE()), " & iMethodType & ", " & cardId & "," & PaymentMethod & "," & _
		" LEFT('" & ccTypeEngString & "', 50), LEFT('" & dbText(referringUrl) & "', 500), " & debitCompanyId & "," & _
		" LEFT('" & PayerID & "', 10), " & transactionIdOutParam & ",0,0," & _
		" LEFT('" & X_DebitReferenceCode & "', 40), Left('" & X_DebitReferenceNum & "', 40), " & IIF(X_AcquirerReferenceNum<> "", "Left('" & X_AcquirerReferenceNum & "', 40)", "Null") & ", " & X_OCurrency & ", " & X_OAmount & ", LEFT('" & GetIPCountry() & "', 2)," & _
		" " & CTP_Status & ",0," & nRecurringSeries & ", " & nRecurringChargeNumber & "," & _
		" " & nUnsettledAmount & ", " & nUnsettledInstallments & ", " & sRefTrans & "," & IIF(deviceId <> "", deviceId, "Null") & _
		"," & IIF(transPayerInfo_id <> "", transPayerInfo_id, "Null") & "," & IIF(transPaymentMethod_id <> "", transPaymentMethod_id, "Null") & "," & IIF(is3DSecure, "1", "0") & ",'" & Replace(X_DeniedAdminComment, "'", "''") & "');"
			
		'Execute Command
		ExecSql(CreateRefundSqlCommand)
			
		'Add Balance to Account
		  AccountIdRecievesCashBack = ExecScalar("Select Account_id From Data.AccountBalance Where AccountBalance_id="&approvalNumber,0)       
		  InsertBalanceCommand = "Insert Into Data.AccountBalance(Account_id,BalanceSourceType_id,SourceID,CurrencyISOCode,Amount,TotalBalance,InsertDate,SystemText,IsPending)" & _
								 "Values("& AccountIdRecievesCashBack &",'System.Purchase',"& transactionIdOutParam &","&"'"&GetCurISOName(Cint(transCurrcency))&"'"&","& cashBackAmount &",0,GETDATE(),'Cashback transaction#"& X_DebitReferenceCode &"',0)"
		   
		 ExecSql(InsertBalanceCommand)

		 'Update the refund transaction ApprovalNumber - The ID of the refund balance row
		 RefundBalanceRowID = ExecScalar("Select AccountBalance_id From Data.AccountBalance Where SourceID="&transactionIdOutParam,0)                 
		 RefundTransactionID = ExecScalar("Select ID From dbo.tblCompanyTransPass where OriginalTransID="&transactionIdOutParam,0)
		 
		 UpdateRefundTransApprovalNumberCommand = "Update dbo.tblCompanyTransPass " &_
												  "Set ApprovalNumber="&RefundBalanceRowID &_
												  " Where ID="&RefundTransactionID

		 ExecSql(UpdateRefundTransApprovalNumberCommand)    
	 
		End If     
	End If

	'20101209 Tamir - If there is a need to update CCF columns in tblMerchantProcessingData, do it after the insert to tblCompanyTransPass
	If sSqlCFF <> "" Then oledbData.Execute sSqlCFF
	
	'20100511 Tamir - log transactions for whitelisted cards
	If nCreditCardWhitelistID > 0 And IsNumeric(transactionIdOutParam) Then
		LogCreditCardWhitelist nCreditCardWhitelistID, nCreditCardWhitelistLevel, companyId, transactionIdOutParam, 1
	End If

	If creditTypeId="8" AND payments >= 1 Then
		'ExecSQL "Exec InsertInstallments " & transactionIdOutParam & ", '" & MerchantPD & "'"
		ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_GenerateOldInstallments & ",'', 3)")
		ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_GenerateInstallments & ",'', 3)")
	End If

	'Create recurring series if needed
	Call CreateRecurringSeries(1, transactionIdOutParam)

	'20110612 Tamir - RiskMailCc fields added
	If ((PaymentMethod >= PMD_CC_MIN And PaymentMethod <= PMD_CC_MAX) Or (PaymentMethod >= PMD_PP_MIN And PaymentMethod <= PMD_PP_MAX)) Then
		If bRiskMailCcIsEnabled Then oledbData.Execute("EXEC Risk.spInsertCcMailUsage '" & X_Email & "', '" & sCCardNum_Encrypt & "';")
	End If

	'Post processing to add fees
	'ExecSQLWithFile("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & transactionIdOutParam & "," & PET_DebitFeesTransaction & ",'', 3)")
	ExecSQLWithFile("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & transactionIdOutParam & "," & PET_FeesTransaction & ",'', 3)")
	'ExecSQLWithFile("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & transactionIdOutParam & "," & PET_AffiliateFeesTransaction & ",'', 3)")

	If Not isTestOnly Then ExecSql("Update [Track].[MerchantActivity] Set DateFirstTransPass=getDate() Where DateFirstTransPass is null And Merchant_id=" & companyId)
	If GlobalSaveToFile Then 
		transactionIdOutParam = 0
		ExecSQL "GO" & vbCrLf & "--END PASS TRANSACTION" & vbCrLf
		GlobalSaveToFile = False
	End If
End Function

'---------------------------------------------------------------------
'	insert failed transaction
'---------------------------------------------------------------------
' transactionTypeId = request source id
' transTypeId = transaction type
Function insertFailedTransaction(transactionIdOutParam, insertDateOutParam, companyId, fraudDetectionLog_id, transactionTypeId, transTypeId, creditTypeId, customerId, customerIp, amount, transCurrcency, ccTypeEngString, payments, orderNumber, replyCode, debitDeclineReason, payFor, productId, comment, terminalNumber, PaymentMethod, iMethodType, cardId, isTestOnly, referringUrl, debitCompanyId, transPayerInfo_id, transPaymentMethod_id, PayerID, is3DSecure, deviceId)
	netpayFeeTransactionCharge = 0
	If ExecScalar("Select ChargeFail From tblDebitCompanyCode Where DebitCompanyID=" & debitCompanyId & " And Code='" & DBText(replyCode) & "'", False) Then
		If bUseOldTerminal Then netpayFeeTransactionCharge = GetUsageFees(companyId, PaymentMethod, transCurrcency, xBin, 1) _
		Else netpayFeeTransactionCharge = GetUsageFeesEx(companyId, PaymentMethod, transCurrcency, xBin, terminalNumber, 1)
	End If
	GetTransactionDebitUsageFees debitCompanyId, terminalNumber, PaymentMethod, transCurrcency, amount, creditTypeId, 0, debitFee
	debitFee = FormatNumber(debitFee, True, False, False)
	sInsertDate = Now()
	transactionIdOutParam = 0
	insertDateOutParam = ""

	'20110324 Tamir - validate transaction amount length
	'20110803 Tamir - if X_Amount is invalid, set it to zero
	If InStr(X_Amount, "E") > 0 Then X_Amount = 0 '20110330 Tamir - decline on scientific notation
	If IIf(InStr(X_Amount, ".") > 0, InStr(X_Amount, ".")-1, Len(X_Amount)) > 8 Then X_Amount = 0

	If TestNumVar(transactionTypeId, 1, 0, -1) >= 0 And TestNumVar(transTypeId, 1, 0, -1) >= 0 And TestNumVar(creditTypeId, 1, 0, -1) >= 0 And TestNumVar(transCurrcency, 1, 0, -1) >= 0 Then
		sSQL = "SET NOCOUNT ON;INSERT INTO tblCompanyTransFail(companyID, insertDate, fraudDetectionLog_id, TransSource_id, TransType, CreditType, CustomerID, IPAddress," & _
		" Amount, Currency, Payments, OrderNumber, replyCode, DebitDeclineReason, PayforText, MerchantProduct_id, Comment, TerminalNumber, PaymentMethod, PaymentMethod_id, PaymentMethodID, paymentMethodDisplay, isTestOnly," & _
		" referringUrl, DebitCompanyID, payerIdUsed, DebitReferenceCode, DebitReferenceNum, netpayFee_transactionCharge, debitFee, IPCountry, ctf_JumpIndex, IsGateway, MobileDevice_id, transPayerInfo_id, transPaymentMethod_id, Is3DSecure, IsCardPresent) VALUES (" & IIF(UseLocalID, "@curID, ", "") & companyId & ", CONVERT(datetime,'" & sInsertDate & "', 105)," & _
		" " & fraudDetectionLog_id & ", " & transactionTypeId & ", " & transTypeId & ", " & creditTypeId & ", " & customerId & ", Left('" & customerIp & "', 50)," & _
		" " & formatnumber(amount, 2, -1, 0, 0) & ", " & TestNumVar(transCurrcency, 0, MAX_CURRENCY, 1) & ", " & payments & ", Left('" & dbText(orderNumber) & "', 100), Left('" & dbText(replyCode) & "', 50), " & IIF(debitDeclineReason <> "",  "Left('" & dbText(debitDeclineReason) & "', 500)", "Null") & "," & _
		" LEFT('" & dbText(PayFor) & "', 100), " & IIF(TestNumVar(productId, 0, -1, 0) > 0, productId, "Null") & ", Left(N'" & DBText(comment) & "', 500), Left('" & terminalNumber & "', 10), " & PaymentMethod & ", " & iMethodType & "," & _
		" " & cardId & ", Left('" & ccTypeEngString & "', 50), " & isTestOnly & ", Left(N'" & dbText(referringUrl) & "', 500), " & debitCompanyId & ", Left('" & PayerID & "', 10)," & _
		" Left('" & X_DebitReferenceCode & "', 40), Left('" & X_DebitReferenceNum & "', 40)," & netpayFeeTransactionCharge & ", " & debitFee & ", Left('" & GetIPCountry() & "', 2), " & TestNumVar(Session(X_TransINC), 0, 255, 0) & ", " & IIF(bIsNetpayTerminal, 0, 1) & "," & IIF(deviceId <> "", deviceId, "Null") & _
		"," & IIF(transPayerInfo_id <> "", transPayerInfo_id, "Null") & "," & IIF(transPaymentMethod_id <> "", transPaymentMethod_id, "Null") & "," & IIF(is3DSecure, "1", "0") & "," & TestNumVar(isCardPresent, 0, 1, 0) & ");" &_	
		"SELECT SCOPE_IDENTITY() AS NewTransFailID;SET NOCOUNT OFF;"
		transactionIdOutParam = ExecScalar(sSQL, 0)
		insertDateOutParam = sInsertDate

		'20100511 Tamir - log transactions for whitelisted cards
		If nCreditCardWhitelistID > 0 And IsNumeric(trim(transactionIdOutParam)) Then
			LogCreditCardWhitelist nCreditCardWhitelistID, nCreditCardWhitelistLevel, companyId, clng(transactionIdOutParam), 2
		End If
		
		'Post processing to add fees
		'ExecSQLWithFile("Insert Into EventPending(TransFail_id, EventPendingType_id, Parameters, TryCount)Values(" & transactionIdOutParam & "," & PET_DebitFeesTransaction & ",'', 3)")
		If netpayFeeTransactionCharge <> 0 Then ExecSQLWithFile("Insert Into EventPending(TransFail_id, EventPendingType_id, Parameters, TryCount)Values(" & transactionIdOutParam & "," & PET_FeesTransaction & ",'', 3)")
		'ExecSQLWithFile("Insert Into EventPending(TransFail_id, EventPendingType_id, Parameters, TryCount)Values(" & transactionIdOutParam & "," & PET_AffiliateFeesTransaction & ",'', 3)")
	End if
End function

'---------------------------------------------------------------------
'	insert approval transaction
'---------------------------------------------------------------------
' transactionTypeId = request source id
Function insertApprovalTransaction(transactionIdOutParam, insertDateOutParam, companyId, fraudDetectionLog_id, transactionTypeId, creditTypeId, customerId, customerIp, amount, transCurrcency, ccTypeEngString, payments, orderNumber, replyCode, payFor, productId, comment, terminalNumber, approvalNumber, cardId, referringUrl, debitCompanyId, transPayerInfo_id, transPaymentMethod_id, is3DSecure, deviceId)
	xBin = ExecScalar("Select BINCountry From tblCreditCard Where ID=" & cardId, 0)
	If bUseOldTerminal Then netpayFeeTransactionCharge = GetUsageFees(companyId, PaymentMethod, transCurrcency, xBin, 2) _
	Else netpayFeeTransactionCharge = GetUsageFeesEx(companyId, PaymentMethod, transCurrcency, xBin, terminalNumber, 2)
	GetTransactionDebitUsageFees debitCompanyId, terminalNumber, PaymentMethod, transCurrcency, amount, creditTypeId, debitFee, 0
	debitFee = FormatNumber(debitFee, True, False, False)
	sInsertDate = now()
	' 20090706 Tamir - Save recurring series&chargenumber in transapproval table (for remote data pulling)
	nRecurringSeries = "NULL"
	nRecurringChargeNumber = "NULL"
	If trim(request("RequestSource"))="20" And TestNumVar(Request("RecurringCharge"), 1, 0, 0)>0 Then
		sSQL="SELECT rc_Series, rc_ChargeNumber FROM tblRecurringCharge WHERE ID=" & TestNumVar(Request("RecurringCharge"), 0, -1, 0)
		Set rsData2=oledbData.Execute(sSQL)
		If Not rsData2.EOF Then
			nRecurringSeries = rsData2("rc_Series")
			nRecurringChargeNumber = rsData2("rc_ChargeNumber")
		End If
		rsData2.Close
	End If
	sSQL = "SET NOCOUNT ON;INSERT INTO tblCompanyTransApproval (companyID, insertDate, fraudDetectionLog_id, TransSource_id, CreditType, CustomerID, IPAddress, Amount, Currency," &_
		" Payments, OrderNumber, replyCode, PayforText, MerchantProduct_id, Comment, TerminalNumber, ApprovalNumber, PaymentMethod, PaymentMethod_id, PaymentMethodID, paymentMethodDisplay, referringUrl," & _
		" DebitCompanyID, DebitReferenceCode, DebitReferenceNum, AcquirerReferenceNum, netpayFee_transactionCharge, debitFee, IsGateway, RecurringSeries, RecurringChargeNumber, MobileDevice_id, transPayerInfo_id, transPaymentMethod_id, Is3DSecure, IsCardPresent) VALUES (" & IIF(UseLocalID, "@curID, ", "") & companyId & ", '" & sInsertDate & "'," &_
		" " & fraudDetectionLog_id & ", " & transactionTypeId & ", " & creditTypeId & ", " & customerId & ", Left('" & customerIp & "', 50), " & formatnumber(amount, 2, -1, 0, 0) & "," & _
		" " & transCurrcency & ", " & payments & ", Left('" & orderNumber & "', 100), Left('" & dbText(replyCode) & "', 50), LEFT('" & dbText(PayFor) & "', 100), " & IIF(TestNumVar(productId, 0, -1, 0) > 0, productId, "Null") & ", Left('" & comment & "', 500)," & _
		" Left('" & terminalNumber & "', 10), Left('" & approvalNumber & "', 50)," & PaymentMethod & ", 1, " & cardId & ", Left('" & ccTypeEngString & "', 50), Left('" & dbText(referringUrl) & "', 500)," & _
		" " & debitCompanyId & ", Left('" & X_DebitReferenceCode & "', 40), Left('" & X_DebitReferenceNum & "', 40), " & IIF(X_AcquirerReferenceNum<> "", "Left('" & X_AcquirerReferenceNum & "', 40)", "Null") & ", " & netpayFeeTransactionCharge & ", " & debitFee & "," & IIF(bIsNetpayTerminal, 0, 1) & ", " & nRecurringSeries & ", " & nRecurringChargeNumber & "," & IIF(deviceId <> "", deviceId, "Null") & _
		"," & IIF(transPayerInfo_id <> "", transPayerInfo_id, "Null") & "," & IIF(transPaymentMethod_id <> "", transPaymentMethod_id, "Null") & "," & IIF(is3DSecure, "1", "0") & "," & TestNumVar(isCardPresent, 0, 1, 0) & ");" & _
		"SELECT SCOPE_IDENTITY() AS NewTransApprovalID;SET NOCOUNT OFF"
	set rsData2 = oledbData.execute(sSQL)
	If NOT rsData2.EOF Then
		transactionIdOutParam = rsData2("NewTransApprovalID")
		insertDateOutParam = sInsertDate
	Else
		transactionIdOutParam = 0
		insertDateOutParam = ""
	End if
	rsData2.close
	Set rsData2 = nothing

	'20100511 Tamir - log transactions for whitelisted cards
	If nCreditCardWhitelistID > 0 And IsNumeric(transactionIdOutParam) Then
		LogCreditCardWhitelist nCreditCardWhitelistID, nCreditCardWhitelistLevel, companyId, transactionIdOutParam, 3
	End If

	'Create recurring series if needed
	Call CreateRecurringSeries(0, transactionIdOutParam)

	'20100713 Tamir - Schedule for AutoCapture if needed
	If nAutoCaptureHours>0 And transactionIdOutParam>0 Then
		CreateAutoCapture nAutoCaptureHours, transactionIdOutParam
	End If

	'20110612 Tamir - RiskMailCc fields added
	If ((PaymentMethod >= PMD_CC_MIN And PaymentMethod <= PMD_CC_MAX) Or (PaymentMethod >= PMD_PP_MIN And PaymentMethod <= PMD_PP_MAX)) Then
		If bRiskMailCcIsEnabled Then oledbData.Execute("EXEC Risk.spInsertCcMailUsage '" & X_Email & "', '" & sCCardNum_Encrypt & "';")
	End If
	
	'Post processing to add fees
	'ExecSQLWithFile("Insert Into EventPending(TransPreAuth_id, EventPendingType_id, Parameters, TryCount)Values(" & transactionIdOutParam & "," & PET_DebitFeesTransaction & ",'', 3)")
	ExecSQLWithFile("Insert Into EventPending(TransPreAuth_id, EventPendingType_id, Parameters, TryCount)Values(" & transactionIdOutParam & "," & PET_FeesTransaction & ",'', 3)")
	'ExecSQLWithFile("Insert Into EventPending(TransPreAuth_id, EventPendingType_id, Parameters, TryCount)Values(" & transactionIdOutParam & "," & PET_AffiliateFeesTransaction & ",'', 3)")
End function

'---------------------------------------------------------------------
'	Move From Pending transaction, by Udi Azulay 07-04-2010
'---------------------------------------------------------------------
Function MovePendingTransaction(sWhere, nRes)
	Dim lChTransID, cmpRs, X_Email, lPendingID : lChTransID = 0
	Set iRs = oleDbData.Execute("SELECT tblCompanyTransPending.*, tblCompanyChargeAdmin.isPendingReply, tblCompanyChargeAdmin.PendingReplyUrl, " & _
		" tblCreditCard.email ccEmail, tblCheckDetails.Email chekEmail" & _
		" FROM tblCompanyTransPending" & _
		" LEFT Join tblCompanyChargeAdmin ON (tblCompanyChargeAdmin.company_id = tblCompanyTransPending.CompanyID)" & _
		" Left Join tblCreditCard ON(tblCreditCard.ID = tblCompanyTransPending.CreditCardID)" & _
		" Left Join tblCheckDetails ON(tblCheckDetails.ID = tblCompanyTransPending.CheckDetailsID)" & _
		" Where " & sWhere & " ORDER BY companyTransPending_id Desc")
	If Not iRs.EOF Then
		CFF_Currency = ExecScalar("SELECT CFF_Currency FROM tblCompany WHERE ID=" & iRs("CompanyID"), vbNull)
		isCardPresent = IIF(iRs("isCardPresent"), 1, 0)
		lPendingID = iRs("companyTransPending_id")
		If iRs("PaymentMethod") >= PMD_EC_MIN And iRs("PaymentMethod") <= PMD_EC_MAX Then xOldMethod = 2 Else xOldMethod = 1
		X_DebitReferenceCode = iRs("DebitReferenceCode")
		X_DebitReferenceNum = iRs("DebitReferenceNum")
		X_AcquirerReferenceNum = iRs("AcquirerReferenceNum")
		X_Email = IIF(IsNull(iRs("CreditCardID")), iRs("chekEmail"), iRs("ccEmail"))
		If (nRes = "000") Then
			X_OCurrency = iRs("trans_currency") : X_OAmount = iRs("trans_amount")
			bIsNetpayTerminal = ExecScalar("Select isNetpayTerminal From tblDebitTerminals Where terminalNumber='" & iRs("TerminalNumber") & "'", False)
			call insertPassedTransaction(lChTransID, "", iRs("CompanyID"), iRs("FraudDetectionLog_id"), iRs("transactionSource_id"), xOldMethod, _
				iRs("trans_creditType"), iRs("CustomerID"), iRs("IPAddress"), iRs("trans_amount"), iRs("trans_currency"), _
				iRs("PaymentMethodDisplay"), iRs("trans_payments"), iRs("trans_order"), _
				iRs("PayforText"), iRs("MerchantProduct_id"), iRs("Comment"), iRs("TerminalNumber"), iRs("DebitApprovalNumber"), IIF(iRs("isTestOnly"), 1, 0), iRs("PaymentMethod"), "", _
				iRs("PaymentMethodID"), "", iRs("DebitCompanyID"), iRs("transPayerInfo_id"), iRs("transPaymentMethod_id"), 0, iRs("Is3dSecure"), iRs("MobileDevice_id"), iRs("trans_originalID"))
			If trim(X_Email) <> "" Then
				Set cmpRs = oleDbData.Execute("Select IsSendUserConfirmationEmail, IsMerchantNotifiedOnPass From tblCompany Where ID=" & iRs("CompanyId"))
				If Not cmpRs.EOF Then
					If IIF(IsNull(cmpRs("IsSendUserConfirmationEmail")), False, cmpRs("IsSendUserConfirmationEmail")) Then _
						ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & lChTransID & "," & PET_InfoEmailSendClient & ",'', 3)")
					If IIF(IsNull(cmpRs("IsMerchantNotifiedOnPass")), False, cmpRs("IsMerchantNotifiedOnPass")) Then _
						ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & lChTransID & "," & PET_InfoEmailSendMerchant & ",'', 3)")
				End If
				cmpRs.Close()
			End If
		Else
			call insertFailedTransaction(lChTransID, "", iRs("CompanyID"), iRs("FraudDetectionLog_id"), iRs("transactionSource_id"), _
				iRs("trans_type"), iRs("trans_creditType"), iRs("CustomerID"), iRs("IPAddress"), iRs("trans_amount"), _
				iRs("trans_currency"), iRs("PaymentMethodDisplay"), iRs("trans_payments"), iRs("trans_order"), nRes, "", _
				iRs("PayforText"), iRs("MerchantProduct_id"), iRs("Comment"), iRs("TerminalNumber"), iRs("PaymentMethod"), xOldMethod, _
				iRs("PaymentMethodID"), IIF(iRs("isTestOnly"), 1, 0), "", iRs("DebitCompanyID"), iRs("transPayerInfo_id"), iRs("transPaymentMethod_id"), 0, iRs("Is3dSecure"), iRs("MobileDevice_id"))
			If trim(X_Email) <> "" Then
				Set cmpRs = oleDbData.Execute("Select IsMerchantNotifiedOnFail From tblCompany Where ID=" & iRs("CompanyId"))
				If Not cmpRs.EOF Then
					If IIF(IsNull(cmpRs("IsMerchantNotifiedOnFail")), False, cmpRs("IsMerchantNotifiedOnFail")) Then _
						ExecSql("Insert Into EventPending(TransFail_id, EventPendingType_id, Parameters, TryCount)Values(" & lChTransID & "," & PET_InfoEmailSendMerchant & ",'', 3)")
				End If
				cmpRs.Close()
			End If
		End If
		oleDbData.Execute "Update Data.Cart Set TransPending_id=Null" & IIF(nRes = "000", ", TransPass_id=" & lChTransID, "") & " Where TransPending_id=" & lPendingID
		oleDbData.Execute "Delete From EventPending Where TransPending_id=" & lPendingID
		oleDbData.Execute "Delete From tblCompanyTransPending Where " & sWhere
		oleDbData.Execute "Insert Into tblLogPendingFinalize (PendingID, TransPassID, TransFailID) Values(" & lPendingID & "," & IIF(nRes = "000", lChTransID, "null") & "," & IIF(nRes <> "000", lChTransID, "null") & ")"
		If (iRs("isPendingReply") And Trim(iRs("PendingReplyUrl")) <> "") Then SendTransResponse IIF(nRes = "000", "tblCompanyTransPass", "tblCompanyTransFail"), lChTransID, iRs("PendingReplyUrl"), iRs("trans_order"), iRs("companyTransPending_id")
		'oledbData.Execute "Update tblPublicPayCostumerData Set transID = " & lChTransID & ", transLocation='" & IIF(nRes = "000", "pass", "fail") & "' Where transID=" & iRs("companyTransPending_id") & " And transLocation='pending'"
	End If
	iRs.Close()		
	MovePendingTransaction = lChTransID	
End Function

'---------------------------------------------------------------------
'	insert Pending transaction
'---------------------------------------------------------------------
' transactionTypeId = request source id
Function insertPendingTransaction(transactionIdOutParam, insertDateOutParam, companyId, fraudDetectionLog_id, transTypeId, creditTypeId, customerId, customerIp, amount, transCurrcency, _
	ccTypeEngString, payments, orderNumber, replyCode, payFor, productId, comment, terminalNumber, approvalNumber, cardId, referringUrl, debitCompanyId, transPayerInfo_id, transPaymentMethod_id, is3DSecure, deviceId)
	Dim sSQL : sSQL = ""
	insertDateOutParam = Now()
	sSQL="SET NOCOUNT ON;"
	sSQL = sSQL & "INSERT INTO tblCompanyTransPending(insertDate, CompanyBatchFiles_id, company_id, CustomerID, fraudDetectionLog_id, DebitCompanyid, TerminalNumber, TransSource_id, PaymentMethodDisplay, isTestOnly, PayforText, MerchantProduct_id, Comment, trans_amount, trans_currency, " & _
		"trans_type, trans_creditType, trans_payments, trans_order, replyCode, IPAddress, PaymentMethod, PaymentMethodID, DebitApprovalNumber, debitReferenceCode, debitReferenceNum, AcquirerReferenceNum, OriginalTransID, MobileDevice_id, transPayerInfo_id, transPaymentMethod_id, Is3DSecure, IsCardPresent)Values(Convert(datetime, '" & insertDateOutParam & "', 103), "	& _
		0 & "," & companyId & "," & customerId & "," & fraudDetectionLog_id & "," & debitCompanyId & ",'" & terminalNumber & "'," & requestSource & ", '" & ccTypeEngString & "'," & isTestOnly & ", LEFT('" & dbText(PayFor) & "', 100), " & IIF(TestNumVar(productId, 0, -1, 0) > 0, productId, "Null") & ", '" & DBText(comment) & "'," & amount & "," & transCurrcency & "," & transTypeId & "," & creditTypeId & "," & _
		payments & ",'" & Replace(orderNumber, "'","''") & "','" & Replace(replyCode, "'","''") & "','" & customerIp & "'," & PaymentMethod & "," & cardId & ",'" & approvalNumber & "', LEFT('" & X_DebitReferenceCode & "', 40), Left('" & X_DebitReferenceNum & "', 40), " & IIF(X_AcquirerReferenceNum<> "", "Left('" & X_AcquirerReferenceNum & "', 50)", "Null") & "," & IIF(OriginalTransID <> "", OriginalTransID, "Null") & "," & IIF(deviceId <> "", deviceId, "Null") & _	
		"," & IIF(transPayerInfo_id <> "", transPayerInfo_id, "Null") & "," & IIF(transPaymentMethod_id <> "", transPaymentMethod_id, "Null") & "," & IIF(is3DSecure, "1", "0") & "," & TestNumVar(isCardPresent, 0, 1, 0) & ");" & _
		"SELECT SCOPE_IDENTITY(); SET NOCOUNT OFF;"
	transactionIdOutParam = ExecScalar(sSQL, 0)
	'20100511 Tamir - log transactions for whitelisted cards
	If nCreditCardWhitelistID > 0 And IsNumeric(transactionIdOutParam) Then
		LogCreditCardWhitelist nCreditCardWhitelistID, nCreditCardWhitelistLevel, companyId, transactionIdOutParam, 4
	End If

	' 20081223 Tamir - write to the tracking table
	'If PaymentMethod >= PMD_CC_MIN And PaymentMethod <= PMD_CC_MAX Then
	'	sSQL="INSERT INTO tblCompanyTransTracking(ctt_MerchantID, ctt_TransInsertDate, ctt_TransPaymentMethod," & _
	'	" ctt_TransCurrency, ctt_TransAmount, ctt_TransCreditType, ctt_TransIsPending, ctt_TransInstallments," & _
	'	" ctt_TransIP, ctt_CardCountry, ctt_CardNumber256, ctt_CardMonth, ctt_CardYear) VALUES(" & _
	'	" " & companyId & ", '" & sInsertDate & "', " & PaymentMethod & ", " & transCurrcency & "," & _
	'	" " & amount & ", " & creditTypeId & ", 1, " & payments & ", LEFT('" & customerIp & "', 20)," & _
	'	" '" & Trim(ccBINCountry) & "', dbo.GetEncrypted256('" & TestStrVar(sCCardNum_Encrypt, 0, 25, "") & "')," & _
	'	" " & TestNumVar(X_ccExpMM, 1, 12, 0) & ", " & TestNumVar(X_ccExpYY, 1, 0, 0) Mod 2000+2000 & ")"
	'ElseIf PaymentMethod >= PMD_EC_MIN And PaymentMethod <= PMD_EC_MAX Then
	'	sSQL="INSERT INTO tblCompanyTransTracking(ctt_MerchantID, ctt_TransInsertDate, ctt_TransPaymentMethod," & _
	'	" ctt_TransCurrency, ctt_TransAmount, ctt_TransCreditType, ctt_TransIsPending, ctt_TransInstallments," & _
	'	" ctt_TransIP, ctt_CheckRoutingNumber256, ctt_CheckAccountNumber256) VALUES(" & companyId & ", '" & sInsertDate & "'," & _
	'	" " & PaymentMethod & ", " & transCurrcency & ", " & FormatNumber(amount, 2, -1, 0, 0) & "," & _
	'	" " & creditTypeId & ", 1, " & payments & ", LEFT('" & customerIp & "', 20)," & _
	'	" dbo.GetEncrypted256('" & ckabaEnc & "'), dbo.GetEncrypted256('" & ckacctEnc & "'))"
	'End If

	'20110612 Tamir - RiskMailCc fields added
	If ((PaymentMethod >= PMD_CC_MIN And PaymentMethod <= PMD_CC_MAX) Or (PaymentMethod >= PMD_PP_MIN And PaymentMethod <= PMD_PP_MAX)) Then
		If bRiskMailCcIsEnabled Then oledbData.Execute("EXEC Risk.spInsertCcMailUsage '" & X_Email & "', '" & sCCardNum_Encrypt & "';")
	End If
End Function

'---------------------------------------------------------------------
'	insert billing address
'---------------------------------------------------------------------
Function insertBillingAddress(countryISO, stateISO, countryId, stateId, city, address1, address2, zipCode)
	insertBillingAddress = 0
	If Not (Trim(city)<>"" OR Trim(address1)<>"" OR Trim(address2)<>"" OR Trim(zipCode)<>"") Then Exit Function
	countryId = TestNumVar(countryId, 1, 0, 0)
	stateId = TestNumVar(stateId, 1, 0, 0)
	If IsNull(address2) Then address2 = ""
	' insert address 
	sSQL = "SET NOCOUNT ON; INSERT INTO tblBillingAddress" & _
	" (countryISO, stateISO, countryId, stateId, city, address1, address2, zipCode) VALUES" & _
	" ('" & TestStrVar(countryISO, 0, 2, "") & "', '" & TestStrVar(stateISO, 0, 2, "") & "'," & _
	" " & countryId & ", " & stateId & ", '" & TestStrVar(city, 0, 60, "") & "'," & _
	" '" & TestStrVar(address1, 0, 100, "") & "', '" & TestStrVar(address2, 0, 100, "") & "'," & _
	" '" & TestStrVar(zipCode, 0, 15, "") & "');" & _
	" SELECT Cast(SCOPE_IDENTITY() As Int) AS NewBillingID; SET NOCOUNT OFF"
	insertBillingAddress = CLng(ExecScalar(sSQL, 0))
End Function

'20110413 Tamir - prevent parallel transactions with same card at same merchant - decline with code 546
Function IsParallelChargeAttempt()
	IsParallelChargeAttempt = False
	Dim nMerchant : nMerchant = TestNumVar(sCompanyID, 1, 0, 0)
	If nMerchant > 0 And Trim(X_ccNumber) <> "" Then
			sSQL = "SET NOCOUNT ON; DECLARE @n INT; EXEC @n = [Track].[spGetRunningProcessCount] " & nMerchant & ", '" & DBText(X_ccNumber) & "'; SELECT @n;"
			IsParallelChargeAttempt = IIf(ExecScalar(sSQL, 0) > 0, True, False)
	End If
End Function

'20110413 Tamir - remove the transaction from the parallel charge attempt counter
Sub RemoveParallelChargeAttempt()
	Dim nMerchant : nMerchant = TestNumVar(sCompanyID, 1, 0, 0)
	If nMerchant > 0 And Trim(X_ccNumber) <> "" Then
		sSQL =	"DELETE FROM [Track].[RunningProcess] WHERE InsertDate = " & _
				"(SELECT MAX(InsertDate) FROM [Track].[RunningProcess] WHERE [Merchant_id] = " & nMerchant & _
				"AND [CreditCardNumber256] = dbo.GetEncrypted256('" & DBText(Replace(X_ccNumber, " ", "")) & "'))"
		oledbData.Execute sSQL
	End If
End Sub

Function CreateCustomerFromTransaction(paymentMethodId, AccountValue1, AccountValue2, expDate)
	If Trim(X_Email) = "" Then Exit Function
	If Trim(X_Email) = "" Then Exit Function
	CreateCustomerFromTransaction = ExecScalar("Select CustomerNumber From Data.Customer Where EmailAddress='" & DBText(X_Email) & "'", "")
	If CreateCustomerFromTransaction <> "" Then Exit Function
	Dim ParamList, UrlAddress, WebServiceReq, responseXml, xNames, fName, sName
	xNames = Split(X_ccHolderName, " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = ""
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = ""
	ParamList = _
		"<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">" & vbCrLf & _
		"  <soap12:Body>" & vbCrLf & _
		"	<RegisterCustomer xmlns=""http://netpay-intl.com/"">" & vbCrLf & _
		"	  <data>" & vbCrLf & _
		"		<info>" & vbCrLf & _
		"        <FirstName>" & fName & "</FirstName>" & vbCrLf & _
		"        <LastName>" & sName & "</LastName>" & vbCrLf & _
		"        <PersonalNumber>" & X_PersonalNumber & "</PersonalNumber>" & vbCrLf & _
		"        <PhoneNumber>" & X_PhoneNumber & "</PhoneNumber>" & vbCrLf & _
		"        <EmailAddress>" & X_Email & "</EmailAddress>" & vbCrLf & _
		"        <AddressLine1>" & BACHAddr1 & "</AddressLine1>" & vbCrLf & _
		"        <City>" & BACity & "</City>" & vbCrLf & _
		"        <PostalCode>" & BAZipCode & "</PostalCode>" & vbCrLf & _
		"        <StateIso>" & BAStateCode & "</StateIso>" & vbCrLf & _
		"        <CountryIso>" & BACountryCode & "</CountryIso>" & vbCrLf & _
		"       </info>" & vbCrLf & _
		"       <PinCode>" & Request("PinCode") & "</PinCode>" & vbCrLf & _
		"       <Password>" & sPassword & "</Password>" & vbCrLf
	If Request("CardNum") <> "" Then
		ParamList = ParamList & _
		"       <StoredPaymentMethods>" & vbCrLf & _
		"         <WalletStoredPaymentMethod>" & vbCrLf & _
		"          <PaymentMethodKey>" & paymentMethodId & "</PaymentMethodKey>" & vbCrLf & _
		"          <OwnerName>" & X_ccHolderName & "</OwnerName>" & vbCrLf & _
		"          <OwnerSSN>" & X_PersonalNumber & "</OwnerSSN>" & vbCrLf & _
		"          <ExpirationDate>" & Year(expDate) & "-" & IIF(Month(expDate) < 10, "0", "") & Month(expDate) & "-" & IIF(Day(expDate) < 10, "0", "") & Day(expDate) & "T23:59:59" & "</ExpirationDate>" & vbCrLf & _
		"          <IsDefault>true</IsDefault>" & vbCrLf & _
		"          <AccountValue1>" & AccountValue1 & "</AccountValue1>" & vbCrLf & _
		"          <AccountValue2>" & AccountValue2 & "</AccountValue2>" & vbCrLf & _
		"         </WalletStoredPaymentMethod>" & vbCrLf & _
		"       </StoredPaymentMethods>" & vbCrLf
	End If
	ParamList = ParamList & _
		"	  </data>" & vbCrLf & _
		"	</RegisterCustomer>" & vbCrLf & _
		"  </soap12:Body>" & vbCrLf & _
		"</soap12:Envelope>" & vbCrLf

	Set WebServiceReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	WebServiceReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	'On Error Resume Next
		WebServiceReq.open "POST", WEBSERVICES_URL & "wallet.asmx", false
		WebServiceReq.setRequestHeader "Content-Type", "application/soap+xml"
		WebServiceReq.send ParamList
		'Response.Write(WEBSERVICES_URL & "wallet.asmx" & vbCrLf & "<br/>Params: " & vbCrLf & ParamList & vbCrLf & "<br/>Response: " & vbCrLf & WebServiceReq.responseText)
		Set responseXml = Server.CreateObject("Microsoft.XMLDOM")
		'Response.Write(WebServiceReq.responseText)
		responseXml.LoadXML WebServiceReq.responseText
		Dim resCode, resNumber, resKey
		resCode = responseXml.selectSingleNode("//Code").text
		resKey = responseXml.selectSingleNode("//Key").text
		If (Not responseXml.selectSingleNode("//Number") Is Nothing) Then CreateCustomerFromTransaction = responseXml.selectSingleNode("//Number").text
	'On Error Goto 0
End Function

Function ValidateShippingAddress()
	If Request("Shipping_CountryIso") <> "" Then 
		If ExecScalar("SELECT CountryID FROM [List].[CountryList] WHERE CountryISOCode='" & Replace(Request("Shipping_CountryIso"), "'", "''") & "'", 0) = 0 Then _
			throwError(544)
	End If
	If Request("Shipping_StateIso") <> "" Then 
		If ExecScalar("SELECT StateID FROM [List].[StateList] WHERE StateISOCode='" & Replace(Request("Shipping_StateIso"), "'", "''") & "'", 0) = 0 Then _
			throwError(543)
	End If
End Function

Sub SaveTransactionData(pmId, pmText, AccountValue1, AccountValue2, expDate, binCountry)
	Dim lNameIndex, fName, sName
	Dim billingAddressId, shippingAddressId
	X_ccHolderName = Trim(X_ccHolderName)
	lNameIndex = InStr(1, X_ccHolderName, " ")
	if lNameIndex > 1 Then 
		fName = Trim(Left(X_ccHolderName, lNameIndex))
		sName = Trim(Right(X_ccHolderName, Len(X_ccHolderName) - lNameIndex))
	Else
		fName = Trim(X_ccHolderName)
	End If

	If BACountryCode <> "" Then 
		ls_sql = "SET NOCOUNT ON;Insert Into Trans.TransPaymentBillingAddress(Street1, Street2, City, PostalCode, StateISOCode, CountryISOCode) Values (" & _
			"'" & Replace(BACHAddr1, "'", "''") & "'" & _
			",'" & Replace(BACHAddr2, "'", "''") & "'" & _
			",'" & Replace(BACity, "'", "''") & "'" & _
			",'" & Replace(BAZipCode, "'", "''") & "'" & _
			"," & IIF(BAStateCode <> "", "'" & Replace(BAStateCode, "'", "''") & "'", "Null") & "" & _
			",'" & Replace(BACountryCode, "'", "''") & "');" & _
			"SELECT SCOPE_IDENTITY();"
		billingAddressId = CLng(ExecScalar(ls_sql, 0))
	End If

	Dim Value1First6, Value1Last4
	AccountValue1 = Replace(Replace(AccountValue1, " ", ""), "-", "")
	AccountValue2 = Replace(Replace(AccountValue2, " ", ""), "-", "")
	Value1First6 = Left(AccountValue1, 6)
	Value1Last4 = Right(AccountValue1, 4)

	If binCountry = "" Then binCountry = TestStrVar(ccBINCountry, 0, 2, "") ' when fail trans, binCountry is empty 
	'Dim Value1Country : Value1Country = TestStrVar(ccBINCountry, 0, 2, "")
	Value1Country = "CASE WHEN '" & binCountry & "' IN (SELECT CountryISOCode FROM [List].[CountryList]) THEN '" & binCountry & "' ELSE NULL END"

	If Len(AccountValue1) > 19 Then
		AccountValue1 = Left(AccountValue1, 16) & "..."
	End If

	ls_sql = "SET NOCOUNT ON;Insert Into Trans.TransPaymentMethod(Merchant_id, TransPaymentBillingAddress_id, PaymentMethod_id, ExpirationDate, Value1Encrypted, Value2Encrypted, PaymentMethodText, Value1Last4Text, Value1First6Text, IssuerCountryIsoCode, EncryptionKey) Values (" & _
		TestNumVar(sCompanyID, 0, -1, 0) & _
		"," & TestNumVar(billingAddressId, 0, -1, "Null") & _
		"," & pmId & _
		IIF(expDate <> Empty, ",'" & Year(expDate) & "-" & Month(expDate) & "-" & Day(expDate) & "'", ", Null") & _
		IIF(AccountValue1 <> "" , ", dbo.GetEncrypted256('" & AccountValue1 & "')", ", Null") & _
		IIF(AccountValue2 <> "", ", dbo.GetEncrypted256('" & AccountValue2 & "')", ", Null") & _
		",LEFT('" & Replace(pmText, "'", "''") & "', 20)" & _
		",'" & Replace(Value1Last4, "'", "''") & "'" & _
		",'" & Replace(Value1First6, "'", "''") & "'" & _
		"," & Value1Country & ", " & TestNumVar(Session("IdentityNumberDSN"), 0, -1, 0) & ");" & _
		"SELECT SCOPE_IDENTITY();"
	X_PaymentMethodId = CLng(ExecScalar(ls_sql, 0))

	If Request("Shipping_CountryIso") <> "" Then 
		ls_sql = "SET NOCOUNT ON;Insert Into Trans.TransPayerShippingDetail(Title, Street1, Street2, City, PostalCode, StateISOCode, CountryISOCode) Values (" & _
			" '" & Replace(Request("Shipping_Title"), "'", "''") & "'" & _
			",'" & Replace(Request("Shipping_Address1"), "'", "''") & "'" & _
			",'" & Replace(Request("Shipping_Address2"), "'", "''") & "'" & _
			",'" & Replace(Request("Shipping_City"), "'", "''") & "'" & _
			",'" & Replace(Request("Shipping_PostalCode"), "'", "''") & "'" & _
			"," & IIF(Request("Shipping_StateIso") <> "", "'" & Replace(Request("Shipping_StateIso"), "'", "''") & "'", "Null") & "" & _
			"," & IIF(Request("Shipping_CountryIso") <> "", "'" & Replace(Request("Shipping_CountryIso"), "'", "''") & "'", "Null") & ");" & _
			"SELECT SCOPE_IDENTITY();"
		shippingAddressId = CLng(ExecScalar(ls_sql, 0))
	End If

	ls_sql = "SET NOCOUNT ON;Insert Into Trans.TransPayerInfo(TransPayerShippingDetail_id, Merchant_id, FirstName, LastName, InvoiceName, PersonalNumber, PhoneNumber, EmailAddress) Values (" & _
		TestNumVar(shippingAddressId, 0, -1, "Null") & _
		"," & TestNumVar(sCompanyID, 0, -1, 0) & _
		",'" & Left(Replace(fName, "'", "''"), 50) & "'" & _
		",'" & Left(Replace(sName, "'", "''"), 50) & "'" & _
		",'" & Left(Replace(Request("InvoiceName"), "'", "''"), 50) & "'" & _
		",'" & Left(Replace(X_PersonalNumber, "'", "''"), 50) & "'" & _
		",'" & Left(Replace(X_PhoneNumber, "'", "''"), 50) & "'" & _
		",'" & Left(Replace(X_Email, "'", "''"), 80) & "');" & _
		"SELECT SCOPE_IDENTITY();"
	X_PayerInfoId = CLng(ExecScalar(ls_sql, 0))
End Sub
'--------------------------------------------------------------------
'	
'---------------------------------------------------------------------
Function DebitProcessResult(dpRet, dpApproval, xParam)
	TimeControl.Append("A_Process2") 'Time Optimization *********
	DebitProcessResult = DebitProcessResultIN(dpRet, dpApproval, xParam)
	DoTerminalJump Trim(dpRet), nTransactionID
	Call fn_returnResponse(LogSavingChargeDataID, requestSource, debitCompany, X_TransType, dpRet, nTransactionID, nInsertDate, X_OrderNumber, Formatnumber(X_Amount,2,-1,0,0), X_Payments, X_Currency, dpApproval, X_Comment)
End Function

Function DebitProcessResultIN(dpRet, dpApproval, xParam)
	Dim pMethod, dt_IsIssuerDec, UsePostProcess, addTracking, isPending, originalPendingId : addTracking = False : isPending = False
	pMethod = IIf(PaymentMethod = 15, 2, 1)
	X_DPRET = dpRet
	UsePostProcess = True '(sCompanyID = "35")
	If X_TransType = "4" Then 
		originalPendingId = X_RefTransID
		X_RefTransID = 0
	End If
	If Trim(dpRet) = "000" Then
		'Approved transaction
		If X_TransType = "1" Then
			Call insertApprovalTransaction(nTransactionID, nInsertDate, sCompanyID, fraudDetectionLog_id, sTransactionTypeID, X_TypeCredit, X_Customer, sIP, X_Amount, X_Currency, ccTypeEngShow, X_Payments, X_OrderNumber, dpRet, X_PayFor, X_ProductId, X_Comment, sTerminalNumber, dpApproval, xParam, referringUrl, debitCompany, X_PayerInfoId, X_PaymentMethodId, X_Is3dSecure, X_deviceId)
			If bIsUseMaxMind Then UpdateMaxMindTransID "TransPreAuth_id", nTransactionID, Trim(dpRet)
			If X_CartId <> "" Then ExecSql("Update Data.Cart Set TransPreAuth_id=" & nTransactionID & ", CheckoutDate=getDate() Where Cart_id=" & TestNumVar(X_CartId, 0, -1, 0))
		Else
			addTracking = true
			Call insertPassedTransaction(nTransactionID, nInsertDate, sCompanyID, fraudDetectionLog_id, sTransactionTypeID, pMethod, X_TypeCredit, X_Customer, sIP, X_Amount, X_Currency, ccTypeEngShow, X_Payments, X_OrderNumber, X_PayFor, X_ProductId, X_Comment, sTerminalNumber, dpApproval, isTestOnly, PaymentMethod, "", xParam, referringUrl, debitCompany, X_PayerInfoId, X_PaymentMethodId, nPayerID, X_Is3dSecure, X_deviceId, X_RefTransID)
			If bIsUseMaxMind Then UpdateMaxMindTransID "TransPass_id", nTransactionID, Trim(dpRet)
			If X_BatchData <> "" Then ExecSql("Insert Into Trans.AuthorizationTransData(TransPass_id, VariableChar) Values(" & nTransactionID & ", '" & Replace(X_BatchData, "'", "''") & "')")
			'send email to client when pass (transId,email)
			If OriginalTransID <> "" And trim(X_TypeCredit)="0" Then
				If TestNumVar(OriginalTransDeniedStatus, 0, -1, 0) = DNS_DupWasWorkedOut Then _
					ExecSql("Update tblCompanyTransPass Set deniedStatus=" & DNS_DupWasWorkedOutRefunded & " Where ID=" & OriginalTransID)
				If TestNumVar(OriginalTransDeniedStatus, 0, -1, 0) = DNS_SetFoundValid Then _
					ExecSql("Update tblCompanyTransPass Set deniedStatus=" & DNS_SetFoundValidRefunded & " Where ID=" & OriginalTransID)
			End If
			If sIsSendUserConfirmationEmail AND trim(X_Email)<>"" Then
				ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_InfoEmailSendClient & ",'', 3)")
			End If	
			If bIsMerchantNotifiedOnPass Then
				ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_InfoEmailSendMerchant & ",'', 3)")
			End If
			Dim bCreateInvoice, invoiceRs : bCreateInvoice = False
			Set invoiceRs = oleDbData.Execute("Select * From Setting.SetMerchantInvoice Where Merchant_id=" & sCompanyID)
			If Not invoiceRs.EOF Then
				If invoiceRs("IsEnable") Then
					bCreateInvoice = (TestNumVar(invoiceRs("IsAutoGenerate" & IIF(X_Currency = 0, "ILS", "Other")), 0, -1, 0) <> 0)
					If bCreateInvoice And (Cint(X_TypeCredit) = 0) Then _
						bCreateInvoice = (TestNumVar(invoiceRs("IsAutoGenerateRefund"), 0, -1, 0) <> 0)
				End If
			End If
			invoiceRs.Close()
			Set invoiceRs = Nothing
			If X_CartId <> "" Then ExecSql("Update Data.Cart Set TransPass_id=" & nTransactionID & ", CheckoutDate=getDate() Where Cart_id=" & TestNumVar(X_CartId, 0, -1, 0))
			
			' insert pending if not refund
			If bCreateInvoice and X_TypeCredit <> 0 Then _
				ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_CreateInvoice & ",'', 3)")
		End if
		TimeControl.Append("A_INSPASS") 'Time Optimization *********
	ElseIf Trim(dpRet) = "001" Or Trim(dpRet) = "552" Or Trim(dpRet) = "553" Or Trim(dpRet) = "554" Or Trim(dpRet) = "556" then
		'Pending transaction
		If X_TransType = "1" then
			Call insertApprovalTransaction(nTransactionID, nInsertDate, sCompanyID, fraudDetectionLog_id, sTransactionTypeID, X_TypeCredit, X_Customer, sIP, X_Amount, X_Currency, ccTypeEngShow, X_Payments, X_OrderNumber, dpRet, X_PayFor, X_ProductId, X_Comment, sTerminalNumber, dpApproval, xParam, referringUrl, debitCompany, X_PayerInfoId, X_PaymentMethodId, X_Is3dSecure, X_deviceId)
			'If X_CartId <> "" Then ExecSql("Update Data.Cart Set TransApproval_id=" & nTransactionID & " Where Cart_id=" & TestNumVar(X_CartId, 0, -1, 0))
			If bIsUseMaxMind Then UpdateMaxMindTransID "TransPreAuth_id", nTransactionID, Trim(dpRet)
		Else
			isPending = true
			addTracking = true
			call insertPendingTransaction(nTransactionID, nInsertDate, sCompanyID, fraudDetectionLog_id, X_TransType, X_TypeCredit, X_Customer, sIP, X_Amount, X_Currency, ccTypeEngShow, X_Payments, X_OrderNumber, dpRet, X_PayFor, X_ProductId, X_Comment, sTerminalNumber, dpApproval, xParam, referringUrl, debitCompany, X_PayerInfoId, X_PaymentMethodId, X_Is3dSecure, X_deviceId)
			If bIsUseMaxMind Then UpdateMaxMindTransID "TransPending_id", nTransactionID, Trim(dpRet)
			If sIsSendUserConfirmationEmail AND trim(X_Email) <> "" Then
				ExecSql("Insert Into EventPending(TransPending_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_InfoEmailSendClient & ",'', 3)")
			End If	
			If X_CartId <> "" Then ExecSql("Update Data.Cart Set TransPending_id=" & nTransactionID & ", CheckoutDate=getDate() Where Cart_id=" & TestNumVar(X_CartId, 0, -1, 0))
		End if		
		TimeControl.Append("A_PND") 'Time Optimization *********
		If Trim(dpRet) = "553" Then X_3dRedirect = X_3dRedirect & IIF(InStr(1, X_3dRedirect, "?") > 0, "&", "?") & "TransID=" & nTransactionID
	Else
		'Rejected transaction
		Call insertFailedTransaction(nTransactionID, nInsertDate, sCompanyID, fraudDetectionLog_id, sTransactionTypeID, X_TransType, X_TypeCredit, X_Customer, sIP, X_Amount, X_Currency, ccTypeEngShow, X_Payments, X_OrderNumber, dpRet, X_CustomErrorDesc, X_PayFor, X_ProductId, X_Comment, sTerminalNumber, PaymentMethod, pMethod, xParam, isTestOnly, referringUrl, debitCompany, X_PayerInfoId, X_PaymentMethodId, nPayerID, X_Is3dSecure, X_deviceId)
		If bIsUseMaxMind Then UpdateMaxMindTransID "TransFail_id", nTransactionID, Trim(dpRet)
		If trim(dpRet) = "001" then nReply = "002" '001 is taken by netpay
		
		TimeControl.Append("A_INSFAIL") 'Time Optimization *********
		' 20090118 Tamir
		' Check merchant's rules and insert to black list (temporarily) if needed (fcbl_blocklevel=2)
		dt_IsIssuerDec = TestNumVar(ExecScalar("Select FailSource From tblDebitCompanyCode Where DebitCompanyID=" & debitCompany & " And Code='" & DBText(dpRet) & "'", -1), 0, -1, -1)
		If dt_IsIssuerDec>-1 Then
			Dim sSQL, rsData, nHours, nTransactions, nCurrency, nAmount, bCreateBlock, sReplyCode
			sSQL="SELECT * FROM tblCreditCardRiskManagement WHERE CCRM_PaymentMethod IN (0, " & PaymentMethod & ") AND" & _
			" (CCRM_Currency<0 OR CCRM_Currency=" & X_Currency & ") AND CCRM_CompanyID=" & sCompanyID & " AND" & _
			" CCRM_IsActive=1" & IIF(sTransactionTypeID = "7" Or sTransactionTypeID = "18", " And CCRM_ApplyVT <> 0", "") & " And CCRM_ReplySource IN (999, " & dt_IsIssuerDec & ") AND CCRM_CreditType IN (255, Sign(" & X_TypeCredit & "))"
			Set rsData=oledbData.Execute(sSQL)
			Do until rsData.EOF
				sReplyCode = "561" 'Default temporary block
				nRule = rsData("CCRM_ID")
				nHours = rsData("CCRM_Hours")
				nTransactions = rsData("CCRM_MaxTrans")
				nCurrency = rsData("CCRM_Currency")
				nAmount = rsData("CCRM_Amount")
				nPaymentMethod = rsData("CCRM_PaymentMethod")
				bCreateBlock = False
				sPaymentMethod = IIf(nPaymentMethod<=0, "DEFAULT", nPaymentMethod)
				sCurrency = IIf(nCurrency<0, "DEFAULT", nCurrency)
				nFailCount = ExecScalar("SELECT [Risk].[fnGetRuleTransFailCount](" & sCompanyID & ", " & nHours & ", " & dt_IsIssuerDec & ", " & sPaymentMethod & ", " & sCurrency & ", " & nRule & ", '" & sCCardNum_Encrypt & "');", 0)
				If nFailCount - nTransactions >= 0 Then
					sReplyCode = rsData("CCRM_ReplyMaxTrans")
					bCreateBlock = True
				Elseif nCurrency >= 0 Then
					nFailAmount = ExecScalar("SELECT [Risk].[fnGetRuleTransFailAmount](" & sCompanyID & ", " & nHours & ", " & dt_IsIssuerDec & ", " & sPaymentMethod & ", " & sCurrency & ", " & nRule & ", '" & sCCardNum_Encrypt & "');", 0)
					If nFailAmount - nAmount >= 0 Then 'was >= need to be > 
						sReplyCode = rsData("CCRM_ReplyAmount")
						bCreateBlock = True
					End if
				End if
				If bCreateBlock Then
					If PaymentMethod >= PMD_EC_MIN And PaymentMethod <= PMD_EC_MAX Then
						ExecSQL "EXEC InsertTempBlock " & sCompanyID & ", '" & ckabaEnc & ckacctEnc & "', " & nRule & ", '" & sReplyCode & "'"
					Else
						ExecSQL "EXEC InsertTempBlock " & sCompanyID & ", '" & sCCardNum_Encrypt & "', " & nRule & ", '" & sReplyCode & "'"
					End If
				End if
				rsData.MoveNext
			Loop
			rsData.Close
			CancelCartTransaction nTransactionID
		End If
		TimeControl.Append("A_FAILRULE") 'Time Optimization *********
		If bIsMerchantNotifiedOnFail Then
			ExecSql("Insert Into EventPending(TransFail_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_InfoEmailSendMerchant & ",'', 3)")
		End If
	End If
	If X_TransType = "4" Then
		ExecSQL "Insert Into tblLogPendingFinalize (PendingID, TransPassID, TransFailID) Values(" & originalPendingId & "," & IIF(dpRet = "000", nTransactionID, "null") & "," & IIF(dpRet <> "000", nTransactionID, "null") & ")"
		If X_CartId <> "" Then ExecSql("Update Data.Cart Set TransPending_id=Null Where Cart_id=" & TestNumVar(X_CartId, 0, -1, 0))
		oleDbData.Execute "Delete From EventPending Where TransPending_id=" & originalPendingId
		OleDbData.Execute "Delete From tblCompanyTransPending Where companyTransPending_id=" & originalPendingId
		OleDbData.Execute "Delete From  [Track].[ProcessApproved] Where TransPending_id=" & originalPendingId
		Dim notifyUrl : notifyUrl = ExecScalar("Select PendingReplyUrl From tblCompanyChargeAdmin Where isPendingReply = 1 And company_id = " & sCompanyID, "")
		If notifyUrl <> "" Then SendTransResponse IIF(dpRet = "000", "tblCompanyTransPass", "tblCompanyTransFail"), nTransactionID, notifyUrl, X_OrderNumber, originalPendingId
	End If

	If addTracking Then
		If ((PaymentMethod >= PMD_CC_MIN And PaymentMethod <= PMD_CC_MAX) Or (PaymentMethod >= PMD_PP_MIN And PaymentMethod <= PMD_PP_MAX)) Then
			sSQL="INSERT INTO [Track].[ProcessApproved] (Merchant_id, PaymentMethod_id, TransPending_id, TransCurrency, TransAmount, TransCreditType_id, TransInstallments," & _
				" TransIpAddress, PaymentMethodStamp, CreditCardNumber256) VALUES(" & sCompanyID & ", " & PaymentMethod & ", " & IIF(isPending, nTransactionID, "Null") & ", " & X_Currency & "," & _
				" " & FormatNumber(X_Amount, 2, -1, 0, 0) & ", " & X_TypeCredit & ", " & X_Payments & ", LEFT('" & sIP & "',20), " & Left(Replace(X_ccNumber, " ", ""), 6) & ", dbo.GetEncrypted256('" & Replace(X_ccNumber, " ", "") & "'))"
			ExecSQL sSQL
		ElseIf PaymentMethod >= PMD_EC_MIN And PaymentMethod <= PMD_EC_MAX Then
			sSQL="INSERT INTO [Track].[ProcessApproved](Merchant_id, PaymentMethod_id, TransPending_id, TransCurrency, TransAmount, TransCreditType_id, TransInstallments," & _
				" TransIpAddress, PaymentMethodStamp, CheckingAccountNumber256) VALUES(" & sCompanyID & ", " & PaymentMethod & ", " & IIF(isPending, nTransactionID, "Null") & ", " & X_Currency & ", " & _
				" " & FormatNumber(X_Amount, 2, -1, 0, 0) & ", " & X_TypeCredit & ", " & X_Payments & ", LEFT('" & sIP & "', 20), " & TestNumVar(Left(Replace(ckabaEnc, " ", ""), 6), 0, -1, 0) & ", dbo.GetEncrypted256('" & Replace(ckabaEnc, " ", "") & "'))"
			ExecSQL sSQL
		End If
	End If
	'20110413 Tamir - remove the transaction from the parallel charge attempt counter
	RemoveParallelChargeAttempt
	'Call fn_returnResponse(LogSavingChargeDataID, requestSource, debitCompany, X_TransType, dpRet, nTransactionID, nInsertDate, X_OrderNumber, Formatnumber(X_Amount,2,-1,0,0), X_Payments, X_Currency, dpApproval, X_Comment)
End Function

Sub DoTerminalJump(replyCode, xTransID)
	If replyCode = "000" Then
		If TestNumVar(Session(X_TransINC), 0, -1, 0) > 0 Then
			oleDbData.Execute "Insert Into tblLog_TerminalJump(ltj_GroupID, ltj_CompanyID, ltj_TransID, ltj_TransReply, ltj_DebitCompanyID, ltj_TerminalNumber, ltj_JumpIndex)Values(" & _
				TestNumVar(Session(X_TransINC & "_Group"), 0, -1, 0) & "," & sCompanyID & "," & xTransID & ",'" & Replace(replyCode, "'", "''") & "'," & debitCompany & ",'" & sTerminalNumber & "'," & TestNumVar(Session(X_TransINC), 0, -1, 0) & ")"
		End If	
	ElseIf (Cstr(X_TypeCredit) <> "0") And X_TerminalHasNext And (replyCode <> "553") Then
		Dim reply_IsCascaded : reply_IsCascaded = TestNumVar(ExecScalar("Select convert(int, IsCascade) From tblDebitCompanyCode Where DebitCompanyID=" & debitCompany & " And Code='" & DBText(replyCode) & "'", -1), 0, -1, 0) = 1
		If Not reply_IsCascaded Then Exit Sub
		If TestNumVar(Session(X_TransINC & "_Group"), 0, -1, 0) <= 0 Then Session(X_TransINC & "_Group") = Cstr(xTransID)
		oleDbData.Execute "Insert Into tblLog_TerminalJump(ltj_GroupID, ltj_CompanyID, ltj_TransID, ltj_TransReply, ltj_DebitCompanyID, ltj_TerminalNumber, ltj_JumpIndex)Values(" & _
			TestNumVar(Session(X_TransINC & "_Group"), 0, -1, 0) & "," & sCompanyID & "," & xTransID & ",'" & Replace(replyCode, "'", "''") & "'," & debitCompany & ",'" & sTerminalNumber & "'," & TestNumVar(Session(X_TransINC), 0, -1, 0) & ")"
		'FileAppendData "Jump.txt", Now & " " & X_TransINC & ":" & Session(X_TransINC) & vbcrlf & vbcrlf & vbcrlf
		call Fn_UpdatingChargeDataLog(LogSavingChargeDataID, replyCode, "", xTransID, TimeControl.TimeString)		
		Session(X_TransINC) = TestNumVar(Session(X_TransINC), 0, -1, 0) + 1
		Server.Transfer("remote_charge.asp")
	End If
End Sub

'---------------------------------------------------------------------
'	20090126 Tamir
'	Risk Management - Check blacklist for active temporary blocks
'---------------------------------------------------------------------
Sub CheckForTempBlock(sCardNumber, nExpMonth, nExpYear, nMerchantID, fromVT)
	Dim sSQL : sSQL="SELECT fcbl_ReplyCode FROM tblFraudCcBlackList" & _
	" INNER JOIN tblCreditCardRiskManagement ON fcbl_CCRMID=CCRM_ID" & _
	" WHERE CCRM_companyID=" & nMerchantID & " AND CCRM_IsActive=1 " & IIF(fromVT, " And CCRM_ApplyVT<>0", "") & " AND fcbl_UnblockDate>GetDate()" & _
	" AND fcbl_ccNumber256 = dbo.GetEncrypted256(Replace('" & sCardNumber & "', ' ', ''))" & _
	" ORDER BY fcbl_UnblockDate DESC"
	Dim rsData : Set rsData=oledbData.Execute(sSQL)
	If NOT rsData.EOF Then
		Dim sCode : sCode=IIf(rsData("fcbl_ReplyCode")="", "561", rsData("fcbl_ReplyCode"))
		rsData.Close
		ThrowError sCode
	End if
	rsData.Close
End sub

'---------------------------------------------------------------------
'	20090209 Tamir
'	Risk Management - Check approved transactions and block current try if needed
'---------------------------------------------------------------------
Function CheckPassLimitation(xCompanyID, ByVal xCurrency, ByVal xPaymentMethod, ByVal xCreditType, ByVal xParam, ByVal xAmount, ByVal nWhitelistLevel, nNoVTRules)
	Dim nRetValue, bCreateBlock, nRule
	bCreateBlock=False
	If xCreditType = 8 Then xCreditType = 1
	Set iRiskRs = oleDbData.Execute("SELECT * FROM GetRulesCCRM(" & xCompanyID & ", " & xPaymentMethod & ", " & xCurrency & ", " & xCreditType & ", " & nWhitelistLevel & "," & IIF(nNoVTRules, 1, 0) & ")")
	Do While Not iRiskRs.EOF
		Dim nTransCount, nTransSum, CreditType
		nTransCount = 1 : nTransSum = xAmount
		If iRiskRs("CCRM_Hours") = 0 Then
			If (xAmount - iRiskRs("CCRM_Amount")) > 0 And iRiskRs("CCRM_Amount") > 0 Then
				nRule = iRiskRs("CCRM_ID")
				If iRiskRs("CCRM_BlockHours") > 0 Then bCreateBlock=True
				nRetValue = iRiskRs("CCRM_ReplyAmount")'"586" 'amount exceeded limit per transaction
				Exit Do
			End if
		Else	
			CreditType = IIf(iRiskRs("CCRM_CreditType") = 1, "1, 8", "0")
			sSQL="SELECT Count(*), Sum(TransAmount) FROM Track.ProcessApproved pa " & _
				" LEFT JOIN List.PaymentMethod pm ON (pa.PaymentMethod_id = pm.PaymentMethod_id)" & _
				" WHERE pa.Merchant_id=" & xCompanyID & _
				" AND pa.TransCurrency = " & xCurrency & " AND pa.TransDate >= DateAdd(hour, " & -iRiskRs("CCRM_Hours") & ", GetDate())" & _
				" AND ((pa.TransPending_id Is Null) Or (DateAdd(minute, pm.PendingKeepAliveMinutes, pa.TransDate) > getDate()) )"
			If iRiskRs("CCRM_PaymentMethod") > 0 Then
				sSQL = sSQL & " AND pa.PaymentMethod_id = " & iRiskRs("CCRM_PaymentMethod") 
			Else
				sSQL = sSQL & " AND ISNULL(pa.PaymentMethod_id,0) >= " & PMD_CC_UNKNOWN
			End If
			
			If xPaymentMethod >= PMD_EC_MIN And xPaymentMethod <= PMD_EC_MAX Then
				sSQL = sSQL & " AND pa.CheckingAccountNumber256 = dbo.GetEncrypted256('" & xParam & "')"
			Else
				sSQL = sSQL & " AND pa.CreditCardNumber256 = dbo.GetEncrypted256('" & xParam & "')"
			End If
			If xParam = "" Then xParam = "NULL"
			sSQL = sSQL & " AND pa.PaymentMethodStamp=" & Left(Trim(xParam),6)
			If iRiskRs("CCRM_CreditType") <> 255 then sSQL = sSQL & " AND pa.TransCreditType_id IN (" & CreditType & ")"

			Set rsDataTmp = OleDbData.Execute(sSQL)
			Do Until rsDataTmp.EOF
				If Not IsNull(rsDataTmp(0)) Then nTransCount = nTransCount + CLng(rsDataTmp(0))
				If Not IsNull(rsDataTmp(1)) Then nTransSum = nTransSum + CLng(rsDataTmp(1))
				rsDataTmp.MoveNext
			Loop
			rsDataTmp.close
			if iRiskRs("CCRM_Amount") > 0 Then
				if nTransSum - iRiskRs("CCRM_Amount") > 0 Then
					if iRiskRs("CCRM_Act") = 0 Then
						nRule=iRiskRs("CCRM_ID")
						If iRiskRs("CCRM_BlockHours")>0 Then bCreateBlock=True
						nRetValue = iRiskRs("CCRM_ReplyAmount")'"586" 'passed amount limit for cc reached
						Exit Do
					Else
						X_TransType = 1
						X_SendVerifyByPhone = True
					End if
				End if
			End if
			if iRiskRs("CCRM_MaxTrans") > 0 Then
				if nTransCount - iRiskRs("CCRM_MaxTrans") > 0 Then
					if iRiskRs("CCRM_Act") = 0 Then 
						nRule=iRiskRs("CCRM_ID")
						If iRiskRs("CCRM_BlockHours")>0 Then bCreateBlock=True
						nRetValue = iRiskRs("CCRM_ReplyMaxTrans")'"585" 'passed trans. count limit for cc reached
						Exit Do
					Else
						X_TransType = 1
						X_SendVerifyByPhone = True
					End if
				End if
			End if
		End if	
		iRiskRs.MoveNext
	Loop
	iRiskRs.Close
	If nRetValue <> "" Then
		If bCreateBlock Then
			If xPaymentMethod >= PMD_EC_MIN And xPaymentMethod <= PMD_EC_MAX Then
				ExecSQL "EXEC InsertTempBlock " & xCompanyID & ", '" & xParam & "', " & nRule & ", '" & nRetValue & "'"
			Else
				ExecSQL "EXEC InsertTempBlock " & xCompanyID & ", '" & xParam & "', " & nRule & ", '" & nRetValue & "'"
			End If
		End If
		X_CustomErrorDesc = fn_getReplyDescription(sRequestSource, 1, nRetValue) & " - " & nRule
		If nCreditCardWhitelistNameMatch= "0" Then X_CustomErrorDesc = X_CustomErrorDesc & " (Whitelist found but no name match)"
		Call throwError(nRetValue)
	End If
End Function

'---------------------------------------------------------------------
'	function for throwing exceptions
'---------------------------------------------------------------------
function throwError(replyCode)
	If trim(replyCode) = "521" OR trim(replyCode) = "520" Then
		'If (Len(sDebitReturnAnswer) = 0) And (Trim(debitCompany) <> 1) Then SaveLogChargeAttemptRequestResponse "", "NP-INT"
	Else
		debitCompany = 1
	End if

	'20110330 Tamir - set zero Amount on code 539
	If trim(ReplyCode) = "539" Then X_Amount = 0

	If trim(replyCode) <> "500" AND trim(replyCode) <> "501" Then
		If Trim(X_TypeCredit) = "" Then X_TypeCredit = 1
		If Trim(X_Customer) = "" Then X_Customer = 0
		If Trim(X_Amount) = "" Then X_Amount = 0
		If Trim(X_Currency) = "" Then X_Currency = 0
		If Trim(X_Payments) = "" Then X_Payments = 1
		If Trim(isTestOnly) = "" Then isTestOnly = 0
		If Trim(ccTypeEngShow) = "" Then ccTypeEngShow = "---"
		nTransactionID = "" : nInsertDate = ""
		
		' 20081124 Tamir
		' the following two conditions (if TestNumVar...) were added
		' in order to prevent double insert of address and/or CC
		' when error is thrown after the address and or CC is already inserted
		if TestNumVar(NewBillingAddressID, 1, 0, 0)=0 then
			on error resume next
			NewBillingAddressID = insertBillingAddress(BACountryCode, BAStateCode, BACountryId, BAStateId, BACity, BACHAddr1, BACHAddr2, BAZipCode)
			on error goto 0
		end if
		If TestNumVar(nNewCreditCardID, 0, -1, 0) = 0 Then
			If (PaymentMethod=0 Or (PaymentMethod >= PMD_CC_MIN And PaymentMethod <= PMD_CC_MAX) Or (PaymentMethod >= PMD_PP_MIN And PaymentMethod <= PMD_PP_MAX)) Then
				On Error Resume Next
				nNewCreditCardID = insertCard(sCCardNum_Encrypt, X_ccExpMM, X_ccExpYY, X_ccHolderName, X_PayerName, X_ccCVV2, X_PhoneNumber, X_Email, sCompanyID, X_PersonalNumber, X_DateOfBirth, ccTypeID, X_Comment, newBillingAddressId, ccBINCountry)
				On Error Goto 0
			End If
		End If
		PaymentMethodID = TestNumVar(nNewCreditCardID, 0, -1, 0)
		'If PaymentMethodID = 0 Then PaymentMethodID = "Null"
		If PaymentMethodID > 0 Then _
			SaveTransactionData PaymentMethod, ccTypeEngShow, X_ccNumber, EncCVV(X_Cvv), X_ExpDate, sBinCountry
		call insertFailedTransaction (nTransactionID, nInsertDate, sCompanyID, fraudDetectionLog_id, sTransactionTypeID, X_TransType, X_TypeCredit, X_Customer, sIP, X_Amount, X_Currency, ccTypeEngShow, X_Payments, X_OrderNumber, replyCode, "", X_PayFor, X_ProductId, X_Comment, sTerminalNumber, PaymentMethod, 1, PaymentMethodID, isTestOnly, referringUrl, debitCompany, X_PayerInfoId, X_PaymentMethodId, nPayerID, X_Is3dSecure, X_deviceId)	 
		If bIsUseMaxMind Then UpdateMaxMindTransID "TransFail_id", nTransactionID, replyCode

		' update fraud detection log ids
		if not Trim(fraudDetectionLogIds) = "" then
			dim updateIdsSql
			updateIdsSql = "UPDATE [Log].[RiskRuleHistory] SET [TransFail_id] = " & nTransactionID & " WHERE [RiskRuleHistory_id] IN (" & fraudDetectionLogIds & ")" 
			ExecSql(updateIdsSql)
		end if

		If trim(replyCode) = "521" OR trim(replyCode) = "520" Then
			Call Sub_LogConnectionProblem(sCompanyName, replyCode, requestSource, X_Amount, X_Currency, sTerminalNumber, sCompanyID, sIP, X_DebitReferenceCode, debitCompany, nTransactionID, HttpError)
		End If
		DoTerminalJump trim(replyCode), nTransactionID
		If bIsMerchantNotifiedOnFail And nTransactionID <> "0" Then
			ExecSql("Insert Into EventPending(TransFail_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_InfoEmailSendMerchant & ",'', 3)")
		End If
		CancelCartTransaction nTransactionID
		'20110413 Tamir - remove the transaction from the parallel charge attempt counter
		RemoveParallelChargeAttempt
		Call fn_returnResponse(LogSavingChargeDataID,requestSource,"1",X_TransType,replyCode,nTransactionID,nInsertDate,X_OrderNumber,X_Amount,X_Payments,X_Currency,"",X_Comment)
	Else
		CancelCartTransaction ""
		'20110413 Tamir - remove the transaction from the parallel charge attempt counter
		RemoveParallelChargeAttempt
		Call fn_returnResponse(LogSavingChargeDataID,requestSource,"1",X_TransType,replyCode,"0000000","01/01/1900 00:00:01",X_OrderNumber,X_Amount,X_Payments,X_Currency,"",X_Comment)
	End if
end function

Function fn_getReplyDescription(sRequestSource, nDebitCompanyID, qReply)
	select case cStr(sRequestSource)
		case "5", "7", "11", "12", "32"
			SelectField = "DescriptionCustomerHeb"
			retErrDesc = "��� �����"
		case else
			SelectField = "DescriptionCustomerEng"
			retErrDesc = "No details"
	end select
	'Get reply code description from database
	sSql = "SELECT TOP 1 " & SelectField & " AS description FROM tblDebitCompanyCode WHERE DebitCompanyID=" & TestNumVar(nDebitCompanyID, 0, -1, 0) & " AND Code='" & DBText(qReply) & "'"
	fn_getReplyDescription = ExecScalar(sSql, retErrDesc)
End Function

'---------------------------------------------------------------------
'	returning a response
'---------------------------------------------------------------------
Function fn_returnResponse(LogSavingChargeDataID,sRequestSource,nDebitCompanyID,qTransType,qReply,qTransID,qDate,qOrder,qAmount,qPayments,qCurrency,qApprovalNum,qComment)	
	Session.Contents.Remove(X_TransINC)
	Session.Contents.Remove(X_TransINC & "_Group")
	'Get replys from main Shva debit company
	Select case int(nDebitCompanyID)
		Case 8, 9, 11 : nDebitCompanyID = 3
		Case 46 : nDebitCompanyID = 18 
	End select
		
	If trim(qReply) = "000" Then
		'Transaction pass
		If X_SendVerifyByPhone Then 
		End if
		select case cStr(sRequestSource)
			case "5", "7", "11", "12", "32" : retErrDesc = "���"
			case else : retErrDesc = "SUCCESS"
		end select

		'20091223 Tamir: on successful capture - write passed transaction ID to auth. trasnaction
		If TestNumVar(X_TransType, 1, 0, 0) = 2 And TestNumVar(sApprovalOnlyID, 1, 0, 0) > 0 And TestNumVar(qTransID, 1, 0, 0) > 0 Then
			oledbData.Execute "UPDATE tblCompanyTransApproval SET TransAnswerID=" & qTransID & " WHERE ID=" & sApprovalOnlyID
			'20100719 Tamir: on successful manual/remote capture - delete scheduled AutoCapture
			If Trim(requestSource) <> "29" Then oledbData.Execute "EXEC AutoCaptureDelete " & sApprovalOnlyID
		End If
	Elseif trim(qReply) = "REVIEW" Then
		'Transaction wait for processing
		qReply = "001"
		select case cStr(sRequestSource)
			case "5", "7", "11", "12", "32" : retErrDesc = "������"
			case else : retErrDesc = "REVIEW"
		end select
	Else
		'Transaction fail
		If X_CustomErrorDesc <> "" Then
			retErrDesc = X_CustomErrorDesc
		Else
			retErrDesc = fn_getReplyDescription(sRequestSource, nDebitCompanyID, qReply)
		End If    
		If nDebitCompanyID = 26 And qReply <> "553" Then qReply = "002" 'payOn normalize replyCode from xxx.xxx.xxx to int
	End if
	
	'Updating charge data log with response
	If LogSavingChargeDataID <> "0" Then call Fn_UpdatingChargeDataLog(LogSavingChargeDataID, qReply, retErrDesc, qTransID, TimeControl.TimeString)

	'Close connection only if not recurring (then needed for notification) - 20090129 Tamir
	'if trim(request("RequestSource"))<>"20" then
	'	oledbData.close
	'	Set oledbData = nothing
	'end if
	
	If IsEmpty(X_ReplyURL) Then X_ReplyURL = DBText(request("ReplyURL")) _
	Else Response.Write(X_ReplyURL)
	If X_ReplyURL="none" Then 'Show string
		If qAmount="" Then qAmount=0
		sReplyQueryString=right("000" & qReply, 3) & qTransType & right("0000000" & qTransID,7) & right(Space(19) & qDate,19) & right("0000000000" & qOrder,10) & right("00000000" & qAmount,8) & right("00" & qPayments,2) & qCurrency & right("0000000" & qApprovalNum,7) & right(Space(255) & qComment,255) & right(Space(100) & retErrDesc,100)
		Response.Write sReplyQueryString
	Else
		sRecurringSeries = "&RecurringSeries=" & IIf(TestNumVar(nRecurringSeriesReturn, 1, 0, 0)>0, nRecurringSeriesReturn, "")
		If PaymentMethod >= PMD_EC_MIN And PaymentMethod <= PMD_EC_MAX Then
			SendData = "replyCode=" & qReply & "&transId=" & qTransID & "&transDate=" & qDate & "&transOrder=" & qOrder & "&transAmount=" & qAmount & "&transCurrency=" & qCurrency & "&confirmationNumber=" & qApprovalNum & "&CompanyNum=" & companyNum & "&transComment=" & qComment & "&replyMessage=" & retErrDesc & "&CCType=" & ccTypeEngShow & "&TransType=" & qTransType & "&ReplyDesc=" & retErrDesc & sRecurringSeries & "&IsTest=" & IIF(isTestTerminal, 1, 0)
			'SendData = "replyCode=" & qReply & "&replyMessage=" & retErrDesc & "&transId=" & qTransID & "&transDate=" & qDate & "&transAmount=" & qAmount & "&transCurrency=" & qCurrency & "&transOrder=" & qOrder & "&transComment=" & qComment & "&confirmationNumber=" & qApprovalNum
		ElseIf PaymentMethod >= PMD_PD_MIN And PaymentMethod <= PMD_PD_MAX Then
			SendData = "replyCode=" & qReply & "&transId=" & qTransID & "&transDate=" & qDate & "&transOrder=" & qOrder & "&transAmount=" & qAmount & "&transCurrency=" & qCurrency & "&confirmationNumber=" & qApprovalNum & "&CompanyNum=" & companyNum & "&transComment=" & qComment & "&replyMessage=" & retErrDesc & "&CCType=" & ccTypeEngShow & "&TransType=" & qTransType & "&ReplyDesc=" & retErrDesc & sRecurringSeries & "&SMSInstruction=" & Replace(X_RetSMSInstruction, "%1", qTransID)
		Else
			'20111207 Tamir - return last 4 digits of CC as a part of the reply string
			'20111211 Udi - added ccStorageID
			sLast4 = ""
			If X_ccNumber <> "" Then sLast4=Right("    " & Replace(X_ccNumber, " ", ""), 4)
			SendData = "TransType=" & qTransType & "&Reply=" & qReply & "&TransID=" & qTransID & "&Date=" & qDate & "&Order=" & qOrder & "&Amount=" & qAmount & "&Payments=" & qPayments & "&Currency=" & qCurrency & "&ConfirmationNum=" & qApprovalNum & "&Comment=" & qComment & "&ReplyDesc=" & retErrDesc & "&CCType=" & ccTypeEngName & "&Descriptor=" & Server.URLEncode(sTermName) & sRecurringSeries & "&Last4=" & sLast4 & "&ccStorageID=" & sCcStorageID
		End if
		Dim transSourceCode : transSourceCode = ExecScalar("Select TransSourceCode From List.TransSource Where TransSource_id=" & TestNumVar(sTransactionTypeID, 0, -1, 0), "")
		If IsNull(transSourceCode) Then transSourceCode = ""
		SendData = SendData & "&Source=" & Server.URLEncode(transSourceCode)
		SendData = SendData & "&WalletID=" & sClient_WalletID
		If X_3dRedirect <> "" Then SendData = SendData & "&D3Redirect=" & Server.URLEncode(X_3dRedirect) 'also in ach
		If X_3dRedirectMethod <> "" Then SendData = SendData & "&D3RedirectMethod=" & Server.URLEncode(X_3dRedirectMethod) 'also in ach
		SendData = AddResponseSignature(SendData)
		Dim notifyUrl : notifyUrl = ExecScalar("SELECT NotifyProcessURL FROM tblCompanyChargeAdmin WHERE company_id=" & TestNumVar(sCompanyID, 0, -1, 0), "")
		If notifyUrl <> "" Then
			Dim httpStatus : httpStatus = 500
			On Error Resume Next
				Set objXmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP")
				objXmlHttp.Open "POST", notifyUrl, False
				objXmlHttp.Send(SendData)
				httpStatus = objXmlHttp.status
			On Error Goto 0

			If Len(Trim(qTransID)) > 1 Then
				Dim historyFieldName
				Select Case qReply
					Case "000": If X_TransType = "1" Then historyFieldName = "TransPreAuth_id" Else historyFieldName = "TransPass_id"
					Case "001", "552", "553", "554", "556": historyFieldName = "TransPending_id"
					Case Else: historyFieldName = "TransFail_id"
				End Select

				ExecSql("Insert Into Trans.TransHistory(Merchant_id, TransHistoryType_id, " & historyFieldName & ", IsSucceeded, Description, ReferenceNumber, ReferenceUrl) Values(" & sCompanyID & "," & TH_Notify_merchant_Processing & "," & qTransID & "," & IIF(httpStatus = 200, 1, 0) & ",'" & Replace(transSourceCode, "'", "''") & "'," & httpStatus & ",'" & Replace(notifyUrl, "'", "''") & "')")
			End If

			Set objXmlHttp = Nothing
		End If
		If X_ReplyURL <> "" Then 
			SendData = X_ReplyURL & IIF(InStr(1, X_ReplyURL, "?") > 0, "&", "?") & SendData
			If RedirectUrlUseScript Then Response.Write("<script type=""text/javascript"">document.location='" + SendData + "';</script>") _
			Else Response.redirect SendData
			Response.End()
		Else 
			Response.Write(SendData)
		End If

		'Notify merchant about recurring payment (if set in devcenter) - 20090128 Tamir
		If Trim(request("RequestSource")) = "20" Then
			'Set rsRecurring = OleDbData.Execute("SELECT isRecurringReply, recurringReplyUrl FROM tblCompanyChargeAdmin WHERE Company_ID=" & sCompanyID)
			'If Not rsRecurring.EOF Then
			'	If rsRecurring(0) And rsRecurring(1) <> "" Then 'udi 17-06-2012
			'       ExecSql("Insert Into EventPending(TransPass_id, Parameters, EventPendingType_id, TryCount)Values(" & qTransID & ", '&RecurringCharge=" & TestNumVar(Request("RecurringCharge"), 1, 0, 0) & "&Url=" & Server.URLEncode(rsRecurring(1)) & "', " & PET_RecurringSendNotify & ", 3)"
			'		sRecurringReplyURL = rsRecurring(1)
			'		If trim(sRecurringReplyURL) <> "" Then
			'			Set rsRecurring = oledbData.Execute("SELECT * FROM RecurringGetNotificationInfo(" & TestNumVar(request("RecurringCharge"), 1, 0, 0) & ")")
			'			If Not rsRecurring.EOF Then
			'				nFirstTransID = rsRecurring("FirstTransID")
			'				nSeriesID = rsRecurring("SeriesID")
			'				nChargeCount = rsRecurring("ChargeCount")
			'				nChargeNumber = rsRecurring("ChargeNumber")
			'				nRecurringStatus = rsRecurring("RecurringStatus")
			'				sRecurringQueryString = "&recur_initialID=" & nFirstTransID & "&recur_seriesID=" & nSeriesID & "&recur_chargeCount=" & nChargeCount & "&recur_chargeNum=" & nChargeNumber & "&recur_status=" & nRecurringStatus
			'				On Error Resume Next
			'					Set objXmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP")
			'					objXmlHttp.Open "GET", sRecurringReplyURL & IIf(inStr(sRecurringReplyURL, "?")>0, "&", "?") & SendData & sRecurringQueryString, False
			'					objXmlHttp.Send
			'					Set objXmlHttp = nothing
			'				On Error Goto 0
			'			End If
			'		End If
			'	End If
			'End If
			'rsRecurring.Close
		End If
	End If
	oledbData.close
	Set oledbData = nothing
	Response.End
End function

Function AddResponseSignature(resParams)
	Dim i, values, signData, pair
	resParams = resParams & "&signType=SHA256"
	signData =  GetURLValue(resParams, "Reply") & GetURLValue(resParams, "TransID") & GetURLValue(resParams, "Order") & GetURLValue(resParams, "Amount") & GetURLValue(resParams, "Currency") & X_MerchantHashKey
	'response.Write(signData)
	'values = Split(resParams, "&")
	'For i = 0 To Ubound(values)
	'	pair = Split(values(i), "=")
	'	If UBound(pair) > 0 Then signData = signData & URLDecode(pair(1))
	'Next
	AddResponseSignature = resParams & "&signature=" & Server.URLEncode(CalcHash("SHA", signData, True))
End Function

Sub ReturnResponseFromTransID(transId, retCode)
	Dim X_DebitCompanyID, X_RequestSource, X_TransType, X_Reply, X_TransID, X_Date, X_Order, X_Amount, X_Payments, X_Currency, X_ApprovalNumber, X_Comment, i_mechId
	If retCode = "000" Then 
		Set iRs = oleDbData.Execute("Select t.*, dbo.GetDecrypted256(m.Value1Encrypted) Value1Decrypted From tblCompanyTransPass t Left Join Trans.TransPaymentMethod m ON(t.TransPaymentMethod_id = m.TransPaymentMethod_id) Where t.ID=" & transId)
		If Not iRs.EOF Then
			X_DebitCompanyID = iRs("DebitCompanyID")
			X_RequestSource = iRs("TransSource_id")
			X_TransType = 0
			X_Reply = "000"
			X_TransID = transId
			X_Date = iRs("InsertDate")
			X_Order = iRs("OrderNumber")
			X_Amount = iRs("Amount")
			X_Currency = iRs("Currency")
			X_ApprovalNumber = iRs("ApprovalNumber")
			X_Payments = iRs("Payments")
			X_Comment = iRs("Comment")
			PaymentMethod = iRs("PaymentMethod")
			X_ccNumber = iRs("Value1Decrypted")
			i_mechId = iRs("companyID")
		End If
		iRs.Close()
	ElseIf retCode = "001" Then 
		Set iRs = oleDbData.Execute("Select t.*, dbo.GetDecrypted256(m.Value1Encrypted) Value1Decrypted From tblCompanyTransPending t Left Join Trans.TransPaymentMethod m ON(t.TransPaymentMethod_id = m.TransPaymentMethod_id) Where t.ID=" & transId)
		If Not iRs.EOF Then
			X_DebitCompanyID = iRs("DebitCompanyID")
			X_RequestSource = iRs("TransSource_id")
			X_TransType = iRs("trans_type")
			X_Reply = iRs("replyCode")
			X_TransID = transId
			X_Date = iRs("insertDate")
			X_Order = iRs("trans_order")
			X_Amount = iRs("trans_amount")
			X_Currency = iRs("trans_currency")
			X_ApprovalNumber = iRs("DebitApprovalNumber")
			X_Payments = iRs("trans_payments")
			X_Comment = iRs("Comment")
			PaymentMethod = iRs("PaymentMethod")
			X_ccNumber = iRs("Value1Decrypted")
			i_mechId = iRs("CompanyID")
		End If
		iRs.Close()
	Else
		Set iRs = oleDbData.Execute("Select t.*, dbo.GetDecrypted256(m.Value1Encrypted) Value1Decrypted From tblCompanyTransFail t Left Join Trans.TransPaymentMethod m ON(t.TransPaymentMethod_id = m.TransPaymentMethod_id) Where t.ID=" & transId)
		If Not iRs.EOF Then
			X_DebitCompanyID = iRs("DebitCompanyID")
			X_RequestSource = iRs("TransSource_id")
			X_TransType = iRs("TransType")
			X_Reply = iRs("replyCode")
			X_TransID = transId
			X_Date = iRs("InsertDate")
			X_Order = iRs("OrderNumber")
			X_Amount = iRs("Amount")
			X_Currency = iRs("Currency")
			X_ApprovalNumber = ""
			X_Payments = iRs("Payments")
			X_Comment = iRs("Comment")
			PaymentMethod = iRs("PaymentMethod")
			X_ccNumber = iRs("Value1Decrypted")
			i_mechId = iRs("CompanyID")
		End If
		iRs.Close()
	End If
	If PaymentMethod <> "" Then ccTypeEngName = ExecScalar("Select GD_Text From tblGlobalData Where GD_LNG=1 And GD_Group=" & GGROUP_PAYMENTMETHOD & " And GD_ID=" & PaymentMethod, "????")
	if X_MerchantHashKey = "" And i_mechId <> "" Then X_MerchantHashKey = ExecScalar("Select HashKey From tblCompany Where ID=" & i_mechId, "")
	fn_returnResponse 0, X_RequestSource, X_DebitCompanyID, X_TransType, X_Reply, X_TransID, X_Date, X_Order, X_Amount, X_Payments, X_Currency, X_ApprovalNumber, X_Comment
End Sub

Function FormatHttpRequestError(sObj)
	FormatHttpRequestError = "readyState:" & sObj.readyState
	On Error Resume Next
		FormatHttpRequestError = FormatHttpRequestError & " | status: " & sObj.status
		FormatHttpRequestError = FormatHttpRequestError & " | statusText: " & sObj.statusText
	On Error Goto 0
End Function

Function CreateCartFromProduct(prdId, qnt)
	Dim sql
	sql = "SET NOCOUNT ON;Insert Into Data.Cart (Identifier, Merchant_id, Customer_id, CurrencyISOCode, TotalProducts, TotalShipping, Installments, ReferenceNumber) Values(NEWID(), " & _
		sCompanyID & "," & IIF(sClient_WalletID<> "", sClient_WalletID, "Null") & ", '" & GetCurISOName(CInt(X_Currency)) & "', " & X_Amount & ", 0, " & TestNumVar(X_Payments, 0, -1, 0) & ", '" & Replace(X_OrderNumber, "'", "''") & "');SELECT SCOPE_IDENTITY();SET NOCOUNT OFF;"
	X_CartId = CLng(ExecScalar(sql, 0))
	sql = "Insert Into Data.CartProduct(Cart_id, Merchant_id, Product_id, ProductType_id, Name, Quantity, Price, CurrencyISOCode, ShippingFee, VATPercent, CurrencyFXRate)" & _
		" Select " & X_CartId & ", " & sCompanyID & ", p.Product_id, p.ProductType_id, IsNull((Select Top 1 pt.Name From Data.ProductText AS pt Where pt.Product_id = p.Product_id), 'Untitled'), " & qnt & "," & X_Amount & ", '" & GetCurISOName(CInt(X_Currency)) & "' , 0, 0, 1" & _
		" From Data.Product AS p WHERE p.Product_id=" & prdId
	oleDbData.Execute sql
End Function

Sub CartManageStock(bCancel)
	Dim productsRs, nCurQuantity, stockExecute : stockExecute = ""
	Set productsRs = oleDbData.Execute("Select c.Merchant_id, c.Quantity, c.Product_id, c.ProductStock_id, p.ProductType_id, p.QtyAvailable ProductAvailable, s.QtyAvailable StockAvailable " & _
		" From Data.CartProduct c " & _
		" Left Join Data.Product p ON (p.Product_id = c.Product_id) " & _
		" Left Join Data.ProductStock s ON (s.ProductStock_id = c.ProductStock_id) " & _
		" Where Cart_id = " & X_CartId)
	Do While Not productsRs.EOF
		If (Not bCancel) And productsRs("Merchant_id") <> sCompanyID Then ThrowError "565"
		nCurQuantity = productsRs("Quantity") 
		If productsRs("ProductStock_id") <> "" Then
			If (Not bCancel) And productsRs("StockAvailable") < nQuantity Then ThrowError "565"
			stockExecute = stockExecute & "Update Data.ProductStock Set QtyAvailable = QtyAvailable " & IIF(bCancel, "+", "-") & " " & nCurQuantity & " Where ProductStock_id=" & productsRs("ProductStock_id") & ";"
		ElseIf productsRs("Product_id") <> "" Then
			If (Not bCancel) And productsRs("ProductAvailable") < nQuantity Then ThrowError "565"
			stockExecute = stockExecute & "Update Data.Product Set QtyAvailable = QtyAvailable " & IIF(bCancel, "+", "-") & " " & nCurQuantity & " Where Product_id=" & productsRs("Product_id") & ";"
		End If
	  productsRs.MoveNext
	Loop
	productsRs.Close()
	If stockExecute <> "" Then oleDbData.Execute stockExecute
End Sub

Sub BeginCartTransaction()
	If X_TransType <> "2" And X_TransType <> "4" And X_TypeCredit <> "0" Then 
		If X_CartId = "" And X_ProductId <> "" Then CreateCartFromProduct X_ProductId, TestNumVar(Request("MerchantProductQuantity"), 0, -1, 1)
		If X_CartId <> "" Then
			CartManageStock False
			CartStockChanged = True
		End If
	End If
End Sub

Sub CancelCartTransaction(nFailTransId)
	If X_CartId <> "" Then
		If nFailTransId <> "" Then ExecSql("Insert Into Data.CartFailLog(Merchant_id, Cart_id, TransFail_id, InsertDate) Values(" & sCompanyID & "," & X_CartId & "," & nFailTransId & ", getdate())")
		If Not CartStockChanged Then Exit Sub
		CartManageStock True
	End If
End Sub

'---------------------------------------------------------------------
'	Change Transaction Status
'---------------------------------------------------------------------

Function CopyPassTrans(iRs, xPayID, xPayDate, nStatus, nAmount, netpayFee_chbCharge, netpayFee_ClrfCharge)
	If nStatus = 5 Then netpayFee_chbCharge = 0
	Dim nUnsettledAmount, nUnsettledInstallments
	If UCase(xPayID)=";X;" Then
		nUnsettledAmount = 0
	ElseIf iRs("CreditType") = 0 Then
		nUnsettledAmount = -nAmount
	ElseIf nStatus=6 Then
		nUnsettledAmount = -nAmount
	Else
		nUnsettledAmount = nAmount
	End If
	If UCase(xPayID) = ";X;" Then
		nUnsettledInstallments = 0
	Else
		nUnsettledInstallments = 1
	End If
	Dim sSQL : sSQL = ""
	sSQL="INSERT INTO tblCompanyTransPass(CompanyID, OriginalTransID, TransSource_id, CustomerID, InsertDate, PrimaryPayedID, PayID, DeniedDate, DeniedStatus, IPAddress," &_
		" Amount, Currency, Payments, CreditType, OrderNumber, Interest, Comment, DeniedPrintDate, DeniedSendDate, TerminalNumber, paymentMethodId," & _
		" paymentMethodDisplay, PaymentMethod, OCurrency, OAmount, DebitReferenceCode, DebitReferenceNum, netpayFee_chbCharge, netpayFee_ClrfCharge, UnsettledAmount, UnsettledTransactions, TransPayerInfo_id, TransPaymentMethod_id, MobileDevice_id) VALUES (" & IIF(UseLocalID, "@curID, ", "") &_
		iRs("CompanyID") & "," & iRs("ID") & "," & iRs("TransSource_id") &_
		"," & iRs("CustomerID") & ", GetDate(), " & xPayID & ", ';" & xPayID & ";" &_
		"','" & iRs("DeniedDate") & "', " & nStatus & ", '" & iRs("IPAddress") &_
		"'," & nAmount & ", " & iRs("Currency") & ", 1, " & IIf(nStatus = 6, 0, iRs("CreditType")) &_
		",'" & Replace(iRs("OrderNumber"), "'", "''") &_
		"', " & iRs("Interest") & ", '" & Replace(iRs("Comment"), "'", "''") & "', '" & iRs("DeniedPrintDate") & "', '" & iRs("DeniedSendDate") & "', '" & Replace(iRs("TerminalNumber"), "'", "''") & "', " & iRs("PaymentMethodID") & ", '" &	Replace(iRs("paymentMethodDisplay"), "'", "''") & "'" & _
		"," & iRs("PaymentMethod") & "," & iRs("OCurrency") & "," & nAmount & ",'" & Replace(iRs("DebitReferenceCode"), "'", "''") & "','" & Replace(IIF(IsNull(iRs("DebitReferenceNum")), "", iRs("DebitReferenceNum")), "'", "''") & "'," & netpayFee_chbCharge & "," & netpayFee_ClrfCharge & _
		", " & nUnsettledAmount & ", " & nUnsettledInstallments & "," & _
		IIF(iRs("TransPayerInfo_id") <> "", iRs("TransPayerInfo_id"), "Null") & "," & IIF(iRs("TransPaymentMethod_id") <> "", iRs("TransPaymentMethod_id"), "Null") & _
		IIF(iRs("MobileDevice_id") <> "", iRs("MobileDevice_id"), "Null") & ")"
	oledbData.Execute sSQL
End Function

Function ChargeBackTransDB(transId, reason, reasonCode)
	Dim procRet
	procRet = CLng(ExecScalar("DECLARE @TodayDate AS DATETIME = GETDATE(); Exec CreateCHB " & transId & ",@TodayDate, '" & Replace(reason, "'", "''") & "'," & TestNumVar(reasonCode, 0, -1, 0), 0))
	If procRet > 0 Then ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & transId & "," & PET_EmailChargeBack & ",'', 3)")
	ChargeBackTransDB = procRet
End Function

Function PhotocopyTransDB(transId, reason, reasonCode)
	Dim procRet
	procRet = CLng(ExecScalar("DECLARE @TodayDate AS DATETIME = GETDATE(); Exec RequestPhotocopyCHB " & transId & ",@TodayDate, '" & Replace(reason, "'", "''") & "'," & TestNumVar(reasonCode, 0, -1, 0), 0))
	If procRet > 0 Then ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & transId & "," & PET_EmailPhotoCopy & ",'', 3)")
	PhotocopyTransDB = procRet
End Function

Function ChargeBackTrans(nAmount, sWhere)
	Dim bUpdate, trnasID, netpayFee_chbCharge, netpayFee_ClrfCharge, DebitFeeChb, bNewSt, nPaymentMethod
	Dim sCardNumber, nMonth, nYear '20090104 Tamir
	Set iRs = Server.CreateObject("Adodb.Recordset")
	iRs.Open "Select tblCompanyTransPass.*, dbo.GetDecrypted256(CCard_Number256) As CardNumber, ExpMM, ExpYY, BINCountry From tblCompanyTransPass Left Join tblCreditCard ON(tblCompanyTransPass.CreditCardID = tblCreditCard.ID) Where " & sWhere, oleDbData, 0, 1
	If Not iRs.EOF Then
		If nAmount = 0 Then nAmount = iRs("Amount")
		sCardNumber = iRs("CardNumber")
		nMonth = iRs("ExpMM") : nYear = iRs("ExpYY")
		trnasID = iRs("ID")
		nPaymentMethod = iRs("PaymentMethod")
		If nAmount > 0 Then
			If bUseOldTerminal Then ChargeBackTrans = GetChbFees(iRs("companyID"), iRs("PaymentMethod"), iRs("OCurrency"), iRs("BINCountry"), netpayFee_chbCharge, netpayFee_ClrfCharge) _
			Else ChargeBackTrans = GetChbFeesEx(iRs("companyID"), iRs("PaymentMethod"), iRs("OCurrency"), iRs("BINCountry"), iRs("TerminalNumber"), netpayFee_chbCharge, netpayFee_ClrfCharge)
			If ChargeBackTrans Then
				If (iRs("PrimaryPayedID") > 0) Or (nAmount <> iRs("Amount")) Then 
					bNewSt = 10
					CopyPassTrans iRs, 0, Now, 6, nAmount, netpayFee_chbCharge, netpayFee_ClrfCharge
					netpayFee_chbCharge = 0 : netpayFee_ClrfCharge = 0
				Else 
					bNewSt = 1
				End if	
				bUpdate = True
			End If
			GetTransactionDebitFees iRs("DebitCompanyID"), iRs("TerminalNumber"), iRs("PaymentMethod"), iRs("Currency"), iRs("Amount"), iRs("CreditType"), "", 1, 0, DebitFeeChb
		Else
			bUpdate = False : ChargeBackTrans = True
		End if	
	Else 
		trnasID	= 0
	End if	
	iRs.Close
	If bUpdate And ChargeBackTrans Then
		sSQL="UPDATE tblCompanyTransPass SET DeniedValDate=" & IIf(nPaymentMethod=25, "DateAdd(m, 1, GetDate())", "GetDate()") & ", DeniedDate=GetDate(), deniedstatus=" & bNewSt & ", netpayFee_chbCharge=" & netpayFee_chbCharge & ", netpayFee_ClrfCharge=" & netpayFee_ClrfCharge & ", DebitFeeChb=" & DebitFeeChb & " WHERE id=" & trnasID
		oledbData.Execute sSQL
	End if
End Function

Function ClearChargeBack(sTransID)
	ClearChargeBack = False
	Set iRs = oleDbData.Execute("Select * From tblCompanyTransPass Where ID=" & sTransID)
	If Not iRs.EOF Then
		If (iRs("DeniedStatus") = 1) And (iRs("PrimaryPayedID") <= 0) Then 
			oleDbData.Execute "Update tblCompanyTransPass Set DeniedStatus=0, netpayFee_chbCharge=0, netpayFee_ClrfCharge=0 Where ID=" & sTransID
			ClearChargeBack = True
		ElseIf iRs("DeniedStatus") = 6 And (iRs("PrimaryPayedID") <= 0) Then 
			xChbCount = TestNumVar(ExecScalar("Select Count(ID) From tblCompanyTransPass Where OriginalTransID=" & iRs("OriginalTransID") & " And DeniedStatus>0", 0), 0, -1, 0)
			If xChbCount = 0 Then oleDbData.Execute "Update tblCompanyTransPass Set DeniedStatus=0 Where ID=" & iRs("OriginalTransID")
			oleDbData.Execute "Delete From tblCompanyTransPass Where ID=" & sTransID
			ClearChargeBack = True
		End If
	End If
	iRs.Close()
End Function

'20101219 Tamir  - For Hosted Page V2
Class HostedPageSettings
	Public IsActive
	Public IsRequiredCVV
	Public IsRequiredEmail
	Public IsRequiredID
	Public IsRequiredPhone
	
	Public Sub LoadMerchant(nID, vSolution)
		Dim sSQL
		If vSolution = "IsCreditCard" Then
			sSQL = "SELECT " & _
			" Cast(CASE WHEN IsNull(IsEnabled, 0)=1 AND IsNull(IsCreditCard, 0)=1 THEN 1 ELSE 0 END AS bit) IsActive, " & _
			" IsCreditCardRequiredCVV IsRequiredCVV, IsCreditCardRequiredEmail IsRequiredEmail, IsCreditCardRequiredID IsRequiredID, IsCreditCardRequiredPhone IsRequiredPhone"
		ElseIf vSolution = "IsDirectDebit" Or vSolution = "IsEcheck" Then
			sSQL = "SELECT " & _
			" Cast(CASE WHEN IsNull(IsEnabled, 0)=1 AND (IsNull(IsEcheck, 0)=1 OR IsNull(IsDirectDebit, 0)=1) THEN 1 ELSE 0 END AS bit) IsActive, " & _
			" Cast(0 As Bit) IsRequiredCVV, " & _
			" Cast(CASE WHEN IsNull(IsEcheckRequiredEmail, 0)=1 OR IsNull(IsDirectDebitRequiredEmail, 0)=1  THEN 1 ELSE 0 END AS bit) IsRequiredEmail, " & _
			" Cast(CASE WHEN IsNull(IsEcheckRequiredID, 0)=1 OR IsNull(IsDirectDebitRequiredID, 0)=1  THEN 1 ELSE 0 END AS bit) IsRequiredID, " & _
			" Cast(CASE WHEN IsNull(IsEcheckRequiredPhone, 0)=1 OR IsNull(IsDirectDebitRequiredPhone, 0)=1  THEN 1 ELSE 0 END AS bit) IsRequiredPhone"
		ElseIf vSolution = "IsPhoneDebit" Then
			sSQL = "SELECT " & _
			" Cast(CASE WHEN IsNull(IsEnabled, 0)=1 AND IsNull(IsPhoneDebit, 0)=1 THEN 1 ELSE 0 END AS bit) IsActive, " & _
			" Cast(0 As Bit) IsRequiredCVV, Cast(0 As Bit) IsRequiredID, " & _
			" IsPhoneDebitRequiredEmail IsRequiredEmail, IsPhoneDebitRequiredPhone IsRequiredPhone"
		End If
		sSQL = sSQL & " FROM tblCompanySettingsHosted WHERE CompanyID=" & nID
		Dim rsData : Set rsData = oledbData.Execute(sSQL)
		If rsData.EOF Then
			IsActive = False
			IsRequiredCVV = False
			IsRequiredEmail = False
			IsRequiredID = False
			IsRequiredPhone = False
		Else
			IsActive = rsData("IsActive")
			IsRequiredCVV = rsData("IsRequiredCVV")
			IsRequiredEmail = rsData("IsRequiredEmail")
			IsRequiredID = rsData("IsRequiredID")
			IsRequiredPhone = rsData("IsRequiredPhone")
		End If
		rsData.Close
		Set rsData = Nothing
	End Sub

	Public Sub Class_Initialize()
		bIsActive = False
		bIsRequiredCVV = False
		bIsRequiredEmail = False
		bIsRequiredID = False
		bIsRequiredPhone = False
	End Sub
End Class
%>