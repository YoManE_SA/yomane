<%

Function DebitIProcessResult(dpRet, dpApproval)
	If sIsBillingAddressMust Then _
		newBillingAddressId = insertBillingAddress(BACountryCode, BAStateCode, BACountryId, BAStateId, BACity, BACHAddr1, BACHAddr2, BAZipCode) _
	Else newBillingAddressId = 0

	Dim nBillingAddressID : nBillingAddressID = TestNumVar(newBillingAddressId, 1, 0, 0)
	If nBillingAddressID = 0 Then nBillingAddressID = "NULL"

	sSQL = "SET NOCOUNT ON;" & _
		"INSERT INTO tblCheckDetails (companyId, customerId, accountName, routingNumber256," & _
		" accountNumber256, bankAccountTypeId, personalNumber, phoneNumber, email, comment," & _
		" BankName, BankCity, BankPhone, BankState, BirthDate, BillingAddressId) VALUES (" & _
		sCompanyID & ", " & X_Customer & ", Left(N'" & X_ccHolderName & "', 50)," & _
		" dbo.GetEncrypted256('" & ckabaEnc & "'), dbo.GetEncrypted256('" & ckacctEnc & "')," & _
		bankAccountType & ", Left(N'" & X_PersonalNumber & "', 20), Left(N'" & X_PhoneNumber & "', 20)," & _
		" Left(N'" & X_Email & "', 50), Left(N'" & X_Comment & "', 500), Left('" & X_BankName & "', 50), Left('" & X_BankCity & "', 15)," & _
		" Left('" & X_BankPhone & "', 15), Left('" & X_BankState & "', 2), Left('" & X_BirthDate & "', 10)," & nBillingAddressID & ")" & _
		" SELECT SCOPE_IDENTITY() AS NewCardID;SET NOCOUNT OFF"
	xParam = ExecScalar(sSQL, 0)

	If bUseOldTerminal Then DebitIProcessResult = DebitProcessResult(dpRet, dpApproval, xParam) _
	Else DebitIProcessResult = DebitProcessResultIN(dpRet, dpApproval)
End Function
%>