<%
'---------------------------------------------------------------------
'	Debit PPro
'---------------------------------------------------------------------

Function debit_CredoRaxAddParam(prList, ourSingValue, prName, prValue)
	debit_CredoRaxAddParam = prList & "&" & prName & "=" & Server.URLEncode(prValue)
	ourSingValue = ourSingValue & prValue 'Replace(Server.URLEncode(prValue), "+", " ")
End Function

Function debit_CredoRaxEnsure2Digits(vNum)
    If Len(vNum) < 2 Then vNum = "0" & vNum
    debit_CredoRaxEnsure2Digits = vNum
End Function

Function debit_CredoRaxMapCardType(pmType)
    Select Case pmType
	Case PMD_CC_VISA
	    debit_CredoRaxMapCardType = 1
	Case PMD_CC_DINERS
	    debit_CredoRaxMapCardType = 4
	Case PMD_CC_AMEX
	    debit_CredoRaxMapCardType = 3
	Case PMD_CC_MASTERCARD
	    debit_CredoRaxMapCardType = 2
	Case PMD_CC_MAESTRO
	    debit_CredoRaxMapCardType = 7
	Case PMD_CC_JCB
	    debit_CredoRaxMapCardType = 5
	Case Else
	    debit_CredoRaxMapCardType = 0
    End Select
End Function

Function debit_CredoRaxMapOperation()
    If Cint(X_TransType) = 0 Or Cint(X_TransType) = 3 Then
        If Cint(X_TypeCredit) = 0 Then
            debit_CredoRaxMapOperation = 5 'refnud
        Else
            debit_CredoRaxMapOperation = 1 'sale
        End If
    ElseIf Cint(X_TypeCredit) = 11 Then
        debit_CredoRaxMapOperation = 34 'payout
    ElseIf Cint(X_TransType) = 1 Then
        debit_CredoRaxMapOperation = 2 'authorization only
    ElseIf Cint(X_TransType) = 2 Then
        debit_CredoRaxMapOperation = 3 'capture
    Else
        debit_CredoRaxMapOperation = 0 'not supported
    End If
End Function

Function debit_formatPhone(xPhone)
    debit_formatPhone = Trim(Replace(Replace(Replace(xPhone, ")", " "), "(", " "), "+", " "))
    While Len(debit_formatPhone) < 5
        debit_formatPhone = "0" & debit_formatPhone
    Wend
End Function

Sub debit_extractAddress(xAddress, outStreet, outNumber)
    Dim i, ny
    outStreet = xAddress & " "
    While IsNumeric(Mid(outStreet, 1, 1))
        outNumber = outNumber & Mid(outStreet, 1, 1)
        outStreet = Right(outStreet, Len(outStreet) - 1)
    Wend
    outStreet = Trim(outStreet)
End Sub

Function debit_CredoRax()
	Dim HttpReq, UrlAddress, ParamList, sApprovalNumber, X_ConfirmNumberValues, CurX_DebitReferenceNum
	Dim BACHAddr1ST, BACHAddr1NO, IsNewRequest : IsNewRequest = True
	If int(X_TypeCredit) = 8 Or int(X_Payments) > 1 Then Call throwError("508") 
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
	If isTestTerminal Then UrlAddress = "https://base2.credorax.com/crax_gate/test3D" _
	Else UrlAddress = "https://pbnplus-eu1.gate.credorax.net/crax_gate/service/gateway" '"https://pbnplus.gate.credorax.net/crax_gate/service/gateway" '"https://mla-base2.credorax.com/crax_gate/test3D"
    CurX_DebitReferenceNum = GetTransRefCode()
	If (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		If X_DebitReferenceNum = "" Then Call throwError("532") 'Reftrans not Found
		If InStr(1, Replace(X_ConfirmNumber, "#", "-"), "-") = 0 Then Call throwError("532") 'Reftrans not Found
		IsNewRequest = False
	End If
    debit_extractAddress BACHAddr1, BACHAddr1ST, BACHAddr1NO
    Dim sCDFormatedAmount : sCDFormatedAmount = CLng(IIF(Cint(X_Currency) = 6, X_Amount, X_Amount * 100))
    
	'sOuterIP = IIF(session("Identity") = "Local", "http://80.179.39.10:8080/member/", Server.URLEncode(Replace(session("ProcessURL"), "http://", "https://")))
	sOuterIP = Session("ProcessURL")
	ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "M", sAccountId)
	ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "O", debit_CredoRaxMapOperation())
	ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "V", 413)
	ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "a1", IIF(Cint(X_TransType) = 2, X_DebitReferenceNum, CurX_DebitReferenceNum))
	ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "a4", sCDFormatedAmount)
	If IsNewRequest Then ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "a5", GetCurISOName(CInt(X_Currency)))
	ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "a6", Right(debit_CredoRaxEnsure2Digits(Year(Now)) & debit_CredoRaxEnsure2Digits(Month(Now)) & debit_CredoRaxEnsure2Digits(Day(Now)), 6))
	ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "a7", debit_CredoRaxEnsure2Digits(Hour(Now)) & debit_CredoRaxEnsure2Digits(Minute(Now)) & debit_CredoRaxEnsure2Digits(Second(Now)))
	'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "a8", CurX_DebitReferenceNum)
	If IsNewRequest Then 
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "b1", Replace(X_ccNumber, " ", ""))
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "b2", debit_CredoRaxMapCardType(PaymentMethod))
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "b3", debit_CredoRaxEnsure2Digits(X_ccExpMM))
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "b4", Right(debit_CredoRaxEnsure2Digits(X_ccExpYY), 2))
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "b5", X_ccCVV2)
	    
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "c1", X_ccHolderName)
		If bIsBillingAddressMust And BAZipCode <> "" Then ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "c10", BAZipCode)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "c2", debit_formatPhone(X_PhoneNumber))
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "c3", X_Email)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "c4", BACHAddr1NO)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "c5", BACHAddr1ST)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "c6", BACHAddr2)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "c7", BACity)
	    'If BACountryCode = "US" Or BACountryCode = "CA" Then 
        'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "c8", BAStateCode)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "c9", BACountryCode)
	End If
	ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "d1", sIP)
	ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "d2", CurX_DebitReferenceNum) 'just echo to response
	If IsNewRequest Then ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "d4", Session.SessionID)
	'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "d5", Request.ServerVariables("HTTP_USER_AGENT"))
	'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "d6", Request.ServerVariables("HTTP_ACCEPT_LANGUAGE"))
	If X_TypeCredit <> "0" Then
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "e1", X_ccHolderName)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "e10", BAZipCode)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "e2", debit_formatPhone(X_PhoneNumber))
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "e3", X_Email)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "e4", BACHAddr1NO)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "e5", BACHAddr1ST)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "e6", BACHAddr2)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "e7", BACity)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "e8", BAStateCode)
	    'ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "e9", BACountryCode)
	End If
	If (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
	    X_ConfirmNumberValues = Split(Replace(X_ConfirmNumber, "#", "-"), "-")
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "g2", X_ConfirmNumberValues(1))
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "g3", X_ConfirmNumberValues(0))
	    ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "g4", X_DebitReferenceNum)
	End If
	if X_DateOfBirth <> "" Then ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "i14", Year(X_DateOfBirth) & debit_CredoRaxEnsure2Digits(Month(X_DateOfBirth)) & debit_CredoRaxEnsure2Digits(Day(X_DateOfBirth)) )
    
	ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "h9", X_DebitReferenceNum)
	If X_PayFor <> "" Then ParamList = debit_CredoRaxAddParam(ParamList, EncodedSignature, "i1", X_PayFor)

	X_DebitReferenceNum = CurX_DebitReferenceNum

    EncodedSignature = LCase(Hash("MD5", EncodedSignature & sAccountSubId).Hex)
    ParamList = "K=" & Server.URLEncode(EncodedSignature) & ParamList

	On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
		HttpReq.Send(Replace(ParamList, vbCrLf, ""))
		TxtRet = HttpReq.responseText

		ParamList = Replace(ParamList, "b1=" & X_ccNumber, "b1=" & GetSafePartialNumber(X_ccNumber))
		ParamList = Replace(ParamList, "b5=" & X_ccCVV2, "b5=" & GetSafePartialNumber(X_ccCVV2))
		TxtRet = Replace(TxtRet, X_ccNumber, GetSafePartialNumber(X_ccNumber))

		SaveLogChargeAttemptRequestResponse ParamList, TxtRet
		HttpError = FormatHttpRequestError(HttpReq)
	On Error GoTo 0
	If IsNull(TxtRet) Or Len(TxtRet) = 0 Then Call throwError("521")
	sReturnCode = GetURLValue(TxtRet, "z2")
	sApprovalNumber = GetURLValue(TxtRet, "z4") & "-" & GetURLValue(TxtRet, "z1")
	If sReturnCode = "" Then Call throwError("520") 'internal error
	'Response.Write(TxtRet & vbCrLf & "</br>" & vbCrLf & "END") : Response.End
	FileAppendData "CredoRax.log", Now & vbCrlf & ParamList & vbCrLf & TxtRet & vbCrlf & vbCrLf
    X_DebitReferenceCode = GetURLValue(TxtRet, "z13")
	If sReturnCode = "0" Then ' Or sReturnCode = "1"
		sReturnCode = "000"
	Else
        If GetURLValue(TxtRet, "z6") <> "" Then sReturnCode = GetURLValue(TxtRet, "z6")
        If sReturnCode = "001" Then sReturnCode = "01"
		X_CustomErrorDesc = URLDecode(GetURLValue(TxtRet, "z3"))
	End If
	Set HttpReq = Nothing
	DebitIProcessResult sReturnCode, sApprovalNumber
End function
%>