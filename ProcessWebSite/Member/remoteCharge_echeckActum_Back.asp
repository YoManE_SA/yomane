<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="remoteCharge_Functions.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/MerchantTerminals.asp"-->
<!--#include file="../include/timer_class.asp"-->
<%
'---------------------------------------------------------------------
'	Debit Actum
'----------------------------------------------27-----------------------
'FROM: history_id=65632629&authcode=Test+Only%3A65632629&status=Accepted&order_id=15605718&consumername=Bob+Yakuza&state=TX&email=udi%40netpay.co.il&zip=00893&city=Austin&consumer_unique=PBiy5IjdmO0.A&PostedVars=initial_amount%3D25%252E00%26custzip%3D00893%26response_location%3Dhttp%253A%252F%252F80%252E179%252E39%252E10%253A81%252FDefault%252Easpx%26billing_cycle%3D%252D1%26custaddress1%3D893%2520Ginza%26parent_id%3DACTUM%26custaddress2%3D%26currency%3DUS%26custemail%3Dudi%2540netpay%252Eco%252Eil%26merordernumber%3D1230161537%26pmt_type%3Dchk%26custstate%3DTX%26custphone%3D893%252D555%252D0893%26sub_id%3DNETPAYTEST%26custname%3DBob%2520Yakuza%26ip_forward%3D127%252E0%252E0%252E1%26custcity%3DAustin&toastcode=1bfb68225b724945f5b23f2c4232c4f7&address1=893+Ginza
FileAppendData "Actum.txt", "Request:" & Now & " FORM:" & Request.Form & " QS:" & Request.QueryString & vbCrLf
Server.ScriptTimeout = 300
nApprovalRecurringSeries = "NULL"

Dim status, authcode, DebitCompanyId, order_id, history_id, PostedVars, DebitReferenceCode, nRes, IsNotifyRequest
FileDebitCompany = 61
IsNotifyRequest = False

status = LCase(Request("status"))
authcode = Request("authcode")
order_id = Request("order_id")
history_id = Request("history_id")
PostedVars = Request("PostedVars")
X_DebitReferenceCode = GetURLValue(PostedVars, "merordernumber")

'Response.Write(X_DebitReferenceCode & " " & FileDebitCompany)
Dim passID, pendingID, failID, logChargeWhere
pendingID = ExecScalar("Select companyTransPending_id From tblCompanyTransPending Where DebitCompanyID=" & FileDebitCompany & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
If pendingID = 0 Then 
    passID = ExecScalar("Select ID From tblCompanyTransPass Where DebitCompanyID=" & FileDebitCompany & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
    If passID = 0 Then 
		failID = ExecScalar("Select ID From tblCompanyTransFail Where DebitCompanyID=" & FileDebitCompany & " And DebitReferenceCode='" & X_DebitReferenceCode & "'", 0)
		If failID = 0 Then
			Response.Write("Transaction not found")
			Response.End
		End If
	End If
End If

If pendingID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=" & FileDebitCompany & " And Lca_ReplyCode='553' And Lca_TransNum IN(" & pendingID & ")"
ElseIf passID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=" & FileDebitCompany & " And Lca_ReplyCode='000' And Lca_TransNum IN(" & passID & ")"
ElseIf failID <> 0 Then
    logChargeWhere = "Lca_DebitCompanyID=" & FileDebitCompany & " And Lca_ReplyCode Not IN('000', '001', '553') And Lca_TransNum IN(" & failID & ")"
End If

Dim xLogChargeAttemptID, xMerchantNum, xRequestParams, xSendParams, xRetParams, xDebitCompany, xLogReplyCode
If Not GetTransInfo(logChargeWhere) Then
    Response.Write("Transaction not found in charge attempts")
    Response.End
End If
X_ReplyURL = GetURLValue(xRequestParams, "RetURL")
X_CustomErrorDesc = Request("reason")
'Response.Write("Process - " & X_ReplyURL)

If pendingID <> 0 Then
	ExecSql("Update tblCompanyTransPending Set DebitApprovalNumber='" & authcode & "', DebitReferenceNum='" & order_id & "' Where ID=" & pendingID)
	If status = "accepted" Then
		sReturnCode = "000"
	ElseIf status = "declined" Then
		sReturnCode = "002"
	Else 
		sReturnCode = "001"
		nRes = pendingID
	End if
    If sReturnCode <> "001" Then
        ExecSql("Update tblCompanyTransPending Set DebitApprovalNumber='" & X_DebitApprovalNumber & "', DebitReferenceNum='" & X_DebitReferenceNum & "' Where DebitCompanyID=49 And DebitReferenceCode='" & X_DebitReferenceCode & "'")
        nRes = MovePendingTransaction("tblCompanyTransPending.ID=" & pendingID, sReturnCode)
        ExecSql("Update tblLogChargeAttempts Set Lca_ReplyCode='" & sReturnCode & "', Lca_ReplyDesc='" & Replace(X_CustomErrorDesc, "'", "''") & "', Lca_TransNum=" & nRes & ", Lca_ResponseString='" & Replace(Request.Form & "&" & Request.QueryString, "'", "''") & "' Where LogChargeAttempts_id=" & xLogChargeAttemptID)
    End If
Else
    sReturnCode = xLogReplyCode
    nRes = IIF(passID <> 0, passID, failID)
End If

'RedirectUrlUseScript = True
If IsNotifyRequest Then Response.Write("OK") _
Else ReturnResponseFromTransID nRes, sReturnCode
Response.End()
%>