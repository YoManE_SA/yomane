<%@ Page Language="VB" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
	Function IsValidRecurringString(ByVal sRecurring As String) As Boolean
		Dim regxTest As Regex = New Regex("^[1-9][0-9]{0,1}[DWMQY][1-9][0-9]*(A[0-9]+\.*[0-9]*)*$", RegexOptions.IgnoreCase)
		Return regxTest.IsMatch(sRecurring)
	End Function
	
	Sub ReturnResult(ByVal nResult As Integer, Optional ByVal sText As String = "---", Optional ByVal nMerchant As Integer = 0, Optional ByVal nSeries As Integer = 0, Optional ByVal drData As SqlDataReader = Nothing)
		If Not drData Is Nothing Then drData.Close()
		Response.ContentType = "text/plain"
		Dim sDescription As String = GlobalData.Value(GlobalDataGroup.RECURRING_MODIFICATION_STATUS, nResult).Text
		Dim sResult As String = "REPLY=" & nResult.ToString.PadLeft(2, "0") & "&DESCRIPTION=" & sDescription
		If sText <> "---" Then sResult &= "&DETAILS=" & sText
		Response.Write(sResult)
		If nResult = 0 Then
			Dim sbSQL As New StringBuilder
			sbSQL.Append("INSERT INTO [Log].[RecurringModify](IPAddress, Merchant_id, RecurringSeries_id, Action, RequestString) VALUES(")
			sbSQL.AppendFormat("Left('{0}', 20), ", Request.ServerVariables("REMOTE_ADDR"))
			sbSQL.AppendFormat("{0}, {1}, Left('{2}', 10), ", nMerchant, nSeries, dbPages.DBText(Request("Action")))
			sbSQL.AppendFormat("Left('{0}', 950))", IIf(String.IsNullOrEmpty(Request.QueryString.ToString), Request.Form.ToString, Request.QueryString.ToString))
			dbPages.ExecSql(sbSQL.ToString)
		End If
		Dim sURL As String = dbPages.ExecScalar("SELECT TOP 1 RecurringModifyReplyURL FROM tblCompanyChargeAdmin WITH (NOLOCK) WHERE isRecurringModifyReply=1 AND Company_ID=" & nMerchant)
		If Not Convert.IsDBNull(sURL) Then
			If Not String.IsNullOrEmpty(sURL) Then dbPages.SendHttpRequest(sURL, sResult)
		End If
		Response.End()
	End Sub

	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		MyBase.OnLoad(e)
		Server.ScriptTimeout = 999
		Dim sSQL As String
		Dim sAction As String = Request("Action")
		If String.IsNullOrEmpty(sAction) Then ReturnResult(1) 'no action passed
		If sAction.ToUpper <> "MODIFY" And sAction.ToUpper <> "SUSPEND" And sAction.ToUpper <> "RESUME" And sAction.ToUpper <> "DELETE" Then ReturnResult(2, sAction) 'invalid action passed
		If sAction.ToUpper = "MODIFY" Then
			If String.IsNullOrEmpty(Request("Recurring1")) Then ReturnResult(8) 'no Recurring1 passed
			If Not IsValidRecurringString(Request("Recurring1")) Then ReturnResult(9, "Recurring1 " & Request("Recurring1")) 'invalid recurring1 string passed
		End If
		Dim nMerchantNumber As Integer = dbPages.TestVar(Request("MerchantNumber"), 1, 0, 0)
		If nMerchantNumber < 1 Then ReturnResult(3) 'no merchant number passed
		Dim nSeries As Integer = dbPages.TestVar(Request("SeriesID"), 1, 0, 0)
		Dim nTransPass As Integer = dbPages.TestVar(Request("TransID"), 1, 0, 0)
		Dim nMerchant As Integer = 0
		Dim sHashKey As String = ""
		Dim bSignatureMandatory As Boolean = False

		sSQL = "SELECT TOP 1 m.ID, m.HashKey, mrs.ForceMD5OnModify FROM tblCompany m WITH (NOLOCK) INNER JOIN tblMerchantRecurringSettings mrs WITH (NOLOCK) ON m.ID=mrs.MerchantID WHERE m.CustomerNumber='" & nMerchantNumber.ToString & "' AND mrs.IsEnabled=1 AND mrs.IsEnabledModify=1 AND m.ActiveStatus IN (20, 30)"
		Dim drData As SqlDataReader = dbPages.ExecReader(sSQL)
		If drData.Read() Then
			nMerchant = dbPages.TestVar(drData("ID"), 1, 0, 0)
			bSignatureMandatory = dbPages.TestVar(drData("ForceMD5OnModify"), False)
			sHashKey = dbPages.TestVar(drData("HashKey"), -1, "")
		End If
		drData.Close()
		If nMerchant < 1 Then ReturnResult(6, "MerchantNumber " & nMerchantNumber.ToString) 'merchant not found

		Dim sSignature As String = Request("signature")
		If bSignatureMandatory Or Not String.IsNullOrEmpty(sSignature) Then
			Dim sbSignatureCorrect As New StringBuilder
			sbSignatureCorrect.AppendFormat("{0}", nMerchantNumber)
			If nSeries > 0 Then sbSignatureCorrect.AppendFormat("{0}", nSeries)
			If nTransPass > 0 Then sbSignatureCorrect.AppendFormat("{0}", nTransPass)
			sbSignatureCorrect.AppendFormat("{0}{1}", sAction, sHashKey)
			Dim sSignatureCorrect As String = Md5Hash.HashASCIIBase64(sbSignatureCorrect.ToString())
			If sSignature <> sSignatureCorrect Then ReturnResult(14, sSignature, nMerchant, nSeries)
		End If

		If nTransPass > 0 Then
			sSQL = "SELECT dbo.GetRecurringSeriesFromTransPass(" & nTransPass & ")"
			If nSeries > 0 Then
				'both series and trans ID passed - check for mismatch
				If nSeries <> dbPages.ExecScalar(sSQL) Then ReturnResult(4, "SeriesID " & nSeries & ", TransID " & nTransPass)
			Else
				'only trans ID passed
				nSeries = dbPages.ExecScalar(sSQL)
			End If
		End If
		If nSeries < 1 Then ReturnResult(5) 'no series/trans ID passed
		sSQL = "SELECT TOP 1 rs_Company, rs_Blocked FROM tblRecurringSeries WITH (NOLOCK) WHERE ID=" & nSeries.ToString & " AND rs_Deleted=0"
		drData = dbPages.ExecReader(sSQL)
		If Not drData.Read Then ReturnResult(7, "Merchant " & nMerchantNumber.ToString & ", SeriesID " & nSeries.ToString, , , drData) 'no series or belongs to another merchant
		If nMerchant <> drData.GetInt32(0) Then ReturnResult(7, "Merchant " & nMerchantNumber.ToString & ", SeriesID " & nSeries.ToString, , , drData) 'no series or belongs to another merchant
		If drData.GetBoolean(1) Then ReturnResult(12, "Merchant " & nMerchantNumber.ToString & ", SeriesID " & nSeries.ToString, , , drData) 'series is blocked
		drData.Close()

		'series OK, can process action
		If sAction.ToUpper = "SUSPEND" Then
			dbPages.ExecSql("UPDATE tblRecurringSeries SET rs_Suspended=1 FROM tblRecurringSeries WHERE ID=" & nSeries.ToString)
			ReturnResult(0, "Series " & nSeries.ToString & " has been suspended successfully", nMerchant, nSeries) 'suspended - OK
		ElseIf sAction.ToUpper = "DELETE" Then
			dbPages.ExecSql("UPDATE tblRecurringSeries SET rs_Deleted=1 FROM tblRecurringSeries WHERE ID=" & nSeries.ToString)
			ReturnResult(0, "Series " & nSeries.ToString & " has been deleted successfully", nMerchant, nSeries) 'deleted - OK
		ElseIf sAction.ToUpper = "RESUME" Then
			Dim sUncharged As String = Request("Uncharged")
			If Not String.IsNullOrEmpty(sUncharged) Then
				If sUncharged.ToUpper = "CANCEL" Then
					dbPages.ExecSql("UPDATE tblRecurringCharge SET rc_Suspended=1 WHERE rc_Paid=0 AND rc_Date<=GetDate() AND rc_Series=" & nSeries.ToString)
				ElseIf sUncharged.ToUpper = "REINSTATE" Then
					dbPages.ExecSql("UPDATE tblRecurringCharge SET rc_Suspended=0 WHERE rc_Paid=0 AND rc_Date<=GetDate() AND rc_Series=" & nSeries.ToString)
				Else
					ReturnResult(13, "Invalid value in UNCHARGED field on RESUME request: " & sUncharged, nMerchant, nSeries)
				End If
			End If
			dbPages.ExecSql("UPDATE tblRecurringSeries SET rs_Suspended=0 FROM tblRecurringSeries WHERE ID=" & nSeries.ToString)
			ReturnResult(0, "Series " & nSeries.ToString & " has been resumed successfully", nMerchant, nSeries)	'resumed - OK
		End If

		'action=MODIFY
		Dim nRecurring As Integer = 1
		Do Until String.IsNullOrEmpty(Request("Recurring" & (nRecurring + 1)))
			nRecurring += 1
		Loop
		For i As Integer = 1 To nRecurring
			If Not IsValidRecurringString(Request("Recurring" & i)) Then ReturnResult(9, "Recurring" & i & " " & Request("Recurring" & i)) 'invalid recurring string passed
		Next
		
		Dim sDate As String
		' 20100902 Tamir - if the next charge is scheduled for today - attempt now!
		Dim bNextChargeToday As Boolean = True
		If String.IsNullOrEmpty(Request("StartDD")) And String.IsNullOrEmpty(Request("StartMM")) And String.IsNullOrEmpty(Request("StartYYYY")) Then
			sDate = "NULL"
		Else
			Dim nDay As Integer = dbPages.TestVar(Request("StartDD"), 1, 31, 0)
			Dim nMonth As Integer = dbPages.TestVar(Request("StartMM"), 1, 12, 0)
			Dim nYear As Integer = dbPages.TestVar(Request("StartYYYY"), Date.Now.Year, Date.Now.Year + 8, 0)
			If nDay < 1 Then ReturnResult(10, "StartDD " & Request("StartDD")) 'invalid start date day passed
			If nMonth < 1 Then ReturnResult(10, "StartMM " & Request("StartMM")) 'invalid start date month passed
			If nYear < 1 Then ReturnResult(10, "StartYYYY " & Request("StartYYYY")) 'invalid start date year passed
			If nDay > Date.DaysInMonth(nYear, nMonth) Then ReturnResult(10, "StartDD " & Request("StartDD") & ", StartMM " & Request("StartMM") & ", StartYYYY " & Request("StartYYYY")) 'invalid start date passed
			If (nYear * 13 + nMonth) * 33 + nDay < (Date.Today.Year * 13 + Date.Today.Month) * 33 + Date.Today.Day Then ReturnResult(11, "StartDD " & Request("StartDD") & ", StartMM " & Request("StartMM") & ", StartYYYY " & Request("StartYYYY")) 'start date passed belongs to past or today
			sDate = "'" & nDay.ToString().PadLeft(2, "0") & "/" & nMonth.ToString.PadLeft(2, "0") & "/" & nYear.ToString & "'"
			If nDay <> Date.Today.Day Or nMonth <> Date.Today.Month Or nYear <> Date.Today.Year Then bNextChargeToday = False
		End If
			
		dbPages.ExecScalar("EXEC RecurringCutSeriesEx " & nSeries.ToString)
		dbPages.ExecScalar("EXEC RecurringSetSeriesIntervalEx " & nSeries.ToString & ", " & sDate)
		For i As Integer = 1 To nRecurring
			sSQL = "EXEC RecurringExtendSeriesEx " & nSeries.ToString & ", '" & Request("Recurring" & i.ToString) & "'"
			dbPages.ExecSql(sSQL)
		Next
		
		If bNextChargeToday Then
			'if no date specified - check in database, if the next charge is scheduled for today
			sSQL = "SELECT IsNull((SELECT TOP 1 ID FROM tblRecurringCharge WHERE rc_Paid=0 AND rc_Blocked=0 AND rc_Date=cast(GetDate() AS date) AND rc_Series=" & nSeries.ToString & "), 0)"
			Dim nChargeID As Integer = dbPages.ExecScalar(sSQL)
			If nChargeID > 0 Then
				sSQL = "EXEC RecurringAttemptCharge " & nChargeID & ", NULL, NULL"
				dbPages.ExecSql(sSQL)
			End If
		End If

		ReturnResult(0, "Series " & nSeries.ToString & " has been modified successfully", nMerchant, nSeries)	'modified - OK
	End Sub
</script>