<%
'---------------------------------------------------------------------
'	Debit debitB_SN
'---------------------------------------------------------------------

Function remoteCharge_ccDebitBNS3D_BackDetect()
    On Error Goto 0
    If Request("PaRes") <> "" Then
        Response.Write("<html><body>")
        Response.Write("<form id=""form3d"" method=""post"" action=""" & (Session("ProcessURL") & "remote_charge.asp") & """>")
        Response.Write(" <input type=""hidden"" name=""CompanyNum"" value=""" & GetURLValue(xRequestParams, "companyNum") & """ />")
        Response.Write(" <input type=""hidden"" name=""TransType"" value=""" & 4 & """ />")
        Response.Write(" <input type=""hidden"" name=""requestSource"" value=""" & GetURLValue(xRequestParams, "requestSource") & """ />")
        Response.Write(" <input type=""hidden"" name=""ReplyURL"" value=""" & GetURLValue(xRequestParams, "RetURL") & """ />")
        Response.Write(" <input type=""hidden"" name=""RefTransID"" value=""" & Request("MD") & """ />")
        Response.Write(" <input type=""hidden"" name=""PaRes"" value=""" & Request("PaRes") & """ />")
		Response.Write(" <input type=""hidden"" name=""ClientIP"" value=""" & Request.ServerVariables("REMOTE_ADDR") & """/>")
        Response.Write("</form>")
        'Response.Write(Request.Form & "\r\n" & Request.QueryString)
        Response.Write("<script>document.getElementById('form3d').submit();</script>")
        Response.Write("</body></html>")
        Response.End()
        'sRedirect = Session("ProcessURL") & "remote_charge.asp" & _
        '    "?CompanyNum=" & GetURLValue(xRequestParams, "companyNum") & _
        '    "&TransType=4" & _
        '    "&requestSource=18" & _
        '    "&ReplyURL=" & Server.URLEncode(GetURLValue(xRequestParams, "RetURL")) & _
        '    "&RefTransID=" & Request("MD") & _
        '    "&PaRes=" & Server.URLEncode(Request("PaRes"))
        'Response.Write(sRedirect) : Response.End()
        'Response.Redirect(sRedirect)
    Else
        Set xmlRet = Server.CreateObject("Msxml2.DOMDocument.3.0")
        xRetParams = Replace(Replace(Replace(xRetParams, "&lt;", "<"), "&gt;", ">"), "``", """")
        xRetParams = Right(xRetParams, Len(xRetParams) - InStr(1, xRetParams, vbCrLf) - 1)
        xmlRet.loadXML(xRetParams)
        X_3dRedirect = xmlRet.selectSingleNode("//acsURL").text
        d3PaReq = Trim(xmlRet.selectSingleNode("//paReq").text)
        Set xmlRet = Nothing
        Response.Write("<html><body>")
        Response.Write("<form id=""form3d"" method=""post"" action=""" & X_3dRedirect & """>")
        Response.Write("<input type=""hidden"" name=""PaReq"" value=""" & d3PaReq & """ />")
        Response.Write("<input type=""hidden"" name=""TermUrl"" value=""" & Session("ProcessURL") & "remoteCharge_Back.asp" & """ />")
        Response.Write("<input type=""hidden"" name=""MD"" value=""" & Request("TransID") & """ />")
        Response.Write("</form>")
        Response.Write("<script>document.getElementById('form3d').submit();</script>")
        Response.Write("</body></html>")
        Response.End()
    End If
End Function


Const BS_Base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
Public Function BS_base64_encode( byVal strIn )
	Dim c1, c2, c3, w1, w2, w3, w4, n, strOut
	For n = 1 To Len( strIn ) Step 3
		c1 = Asc( Mid( strIn, n, 1 ) )
		c2 = Asc( Mid( strIn, n + 1, 1 ) + Chr(0) )
		c3 = Asc( Mid( strIn, n + 2, 1 ) + Chr(0) )
		w1 = Int( c1 / 4 ) : w2 = ( c1 And 3 ) * 16 + Int( c2 / 16 )
		If Len( strIn ) >= n + 1 Then 
			w3 = ( c2 And 15 ) * 4 + Int( c3 / 64 ) 
		Else 
			w3 = -1
		End If
		If Len( strIn ) >= n + 2 Then 
			w4 = c3 And 63 
		Else 
			w4 = -1
		End If
		strOut = strOut + BS_mimeencode( w1 ) + BS_mimeencode( w2 ) + _
				  BS_mimeencode( w3 ) + BS_mimeencode( w4 )
	Next
	BS_base64_encode = strOut
End Function

Private Function BS_mimeencode( byVal intIn )
	If intIn >= 0 Then 
		BS_mimeencode = Mid( BS_Base64Chars, intIn + 1, 1 ) 
	Else 
		BS_mimeencode = ""
	End If
End Function	

Function debitBNS3D()
	Dim HttpReq, UrlAddress, ParamList, sApprovalNumber, sCookieValue
	If Trim(X_ccHolderName)<>"" Then X_ccHolderName = ReplaceEscapeSequences(X_ccHolderName)
	If Len(sCompanyDescriptor) > 24 Then sCompanyDescriptor = Left(sCompanyDescriptor, 24)
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
	    Call debitBNS(False)
	    Exit Function
	ElseIf (Request("PaRes") <> "") Then
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & vbCrLf & _
		" <soap:Body>" & vbCrLf & _
		"  <xmlApiRequest xmlns=""http://www.voeb-zvd.de/xmlapi/1.0"" id=""a1234"" version=""1.0"">" & vbCrLf & _
		"	<tdsFinalRequest id=""reqid1"">" & vbCrLf & _
		"	 <merchantId>" & sAccountId & "</merchantId>" & vbCrLf & _
		"	 <paRes>" & Request("PaRes") & "</paRes>" & vbCrLf & _
		"	</tdsFinalRequest>" & vbCrLf & _
		"  </xmlApiRequest>" & vbCrLf & _
		" </soap:Body>" & vbCrLf & _
		"</soap:Envelope>"
	Else
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & vbCrLf & _
		" <soap:Body>" & vbCrLf & _
		"  <xmlApiRequest xmlns=""http://www.voeb-zvd.de/xmlapi/1.0"" id=""a1234"" version=""1.0"">" & vbCrLf & _
		"	<tdsInitialRequest id=""reqid1"">" & vbCrLf & _
		"	 <merchantId>" & sAccountId & "</merchantId>" & vbCrLf & _
		"	 <timeStamp>" & Year(Now) & "-" & Make2DigDate(Month(Now)) & "-" & Make2DigDate(Day(Now)) & "T" & Time() & "</timeStamp>" & vbCrLf & _
		"	 <eventExtId>" & X_DebitReferenceCode & "</eventExtId>" & vbCrLf & _
		"	 <kind>creditcard</kind>" & vbCrLf
		If (sCompanyDescriptor <> "") Then ParamList = ParamList & _
		"	 <additionalNote>" & sCompanyDescriptor & "</additionalNote>" & vbCrLf
		
		ParamList = ParamList & _
		"	 <action>" & IIF(Cint(X_TransType) = 1, "preauthorization", "authorization") & "</action>" & vbCrLf & _
		"	 <amount>" & (formatNumber(X_Amount, 2) * 100) & "</amount>" & vbCrLf & _
		"	 <currency>" & GetCurISOName(Cint(X_Currency)) & "</currency>" & vbCrLf & _
		"	 <creditCard>" & vbCrLf & _
		"	  <pan>" & Replace(X_ccNumber, " ", "") & "</pan>" & vbCrLf & _
		"	  <expiryDate>" & vbCrLf & _
		"	   <month>" & X_ccExpMM & "</month>" & vbCrLf & _
		"	   <year>20" & X_ccExpYY & "</year>" & vbCrLf & _
		"	  </expiryDate>" & vbCrLf & _
		"	  <holder>" & X_ccHolderName & "</holder>" & vbCrLf
		If Trim(Replace(X_ccCVV2," ","")) <> "" Then ParamList = ParamList & _
		"	  <verificationCode>" & X_ccCVV2 & "</verificationCode>" & vbCrLf
		
		ParamList = ParamList & _
		"	 </creditCard>" & vbCrLf & _
		"	 <fallBackOnSSL>true</fallBackOnSSL>" & vbCrLf & _
		"	</tdsInitialRequest>" & vbCrLf & _
		"  </xmlApiRequest>" & vbCrLf & _
		" </soap:Body>" & vbCrLf & _
		"</soap:Envelope>"
	End if

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    'UrlAddress = "https://soap.bs-card-service.com/soap/services/XmlApiNl"   'old  
	UrlAddress = "https://soap.bs-card-service.com/soapapi/services/XmlApiNl" 'new
    'UrlAddress = "https://test.soap.bs-card-service.com/soapapi/services/XmlApiNl"
	
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false', sAccountSubId, sAccountPassword '"paybynet", "start"
		HttpReq.setRequestHeader "Authorization", "Basic " & BS_base64_encode(sAccountSubId & ":" & sAccountPassword) & "=="
		HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
		HttpReq.setRequestHeader "SOAPAction", "process"
		HttpReq.setRequestHeader "Content-Length", Len(ParamList)
		If (Request("PaRes") <> "") Then
		    Dim prevRetData : prevRetData = ExecScalar("Select Lca_ResponseString From tblLogChargeAttempts Where Lca_ReplyCode='553' And Lca_TransNum=" & X_RefTransID, 0)
		    If Left(prevRetData, 7) = "Cookie=" Then prevRetData = Mid(prevRetData, 8, InStr(1, prevRetData, vbCrLf) - 8)
    		HttpReq.setRequestHeader "Cookie", prevRetData
		End If
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		If (Request("PaRes") = "") Then sCookieValue = "Cookie=" & HttpReq.getResponseHeader("Set-Cookie") & vbCrLf
		'Response.Write(ParamList & "<br>" & sResDetails) : Response.End()
		HttpError = FormatHttpRequestError(HttpReq)
	On Error Goto 0
	    'Made by tamir to hide cvv and card number from log
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "<verificationCode>" & X_ccCVV2, "<verificationCode>" & GetSafePartialNumber(X_ccCVV2))
    	If Len(sResDetails) = 0 Then Call throwError("521")
		Set xmlRet = HttpReq.responseXml
		sResDetails = Left(sCookieValue & sResDetails, 3900)
		sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails
		X_Is3dSecure = True
    	If (Request("PaRes") = "") Then
            If Not xmlRet.selectSingleNode("//acsURL") Is Nothing Then
                X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
                sIsSendUserConfirmationEmail = False
	            Call DebitIProcessResult("553", sApprovalNumber)
	        End If
	    Else
		    'Response.Write(ParamList & "<br>" & sResDetails & " " & X_RefTransID) : Response.End()
    	End If

        If Not xmlRet.selectSingleNode("//rc") Is Nothing Then sReturnCode = Trim(xmlRet.selectSingleNode("//rc").text)
        If Not xmlRet.selectSingleNode("//aid") Is Nothing Then sApprovalNumber = xmlRet.selectSingleNode("//aid").text
        If Not xmlRet.selectSingleNode("//message") Is Nothing Then sError = xmlRet.selectSingleNode("//message").text
        If Not xmlRet.selectSingleNode("//txExtId") Is Nothing Then X_DebitReferenceCode = xmlRet.selectSingleNode("//txExtId").text
        
        If Trim(sReturnCode) = "0000" Then
            sReturnCode = "000"
        ElseIf Trim(sReturnCode) = "001" Then 
            sReturnCode = "002"
        End if	
	    'If (Len(sReturnCode) < 4) Then Call throwError("520")
	Set xmlRet = Nothing
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>