<%
'---------------------------------------------------------------------
'	Debit debitAllied3d
'---------------------------------------------------------------------
Function remoteCharge_debitAllied3d_BackDetect()
    If Request("PaRes") <> "" Then
		Response.Write("<html><body onload=""document.getElementById('frmMain').submit()""><form id=""frmMain"" method=""post"" action=""" & Session("ProcessURL") & "remote_charge.asp"">")
		Response.Write(" <input type=""hidden"" name=""CompanyNum"" value=""" & GetURLValue(xRequestParams, "companyNum") & """/>")
		Response.Write(" <input type=""hidden"" name=""TransType"" value=""4""/>")
		Response.Write(" <input type=""hidden"" name=""requestSource"" value=""" & GetURLValue(xRequestParams, "requestSource") & """/>")
		Response.Write(" <input type=""hidden"" name=""ReplyURL"" value=""" & GetURLValue(xRequestParams, "RetURL")  & """/>")
		Response.Write(" <input type=""hidden"" name=""RefTransID"" value=""" & x_TransId & """/>")
		Response.Write(" <input type=""hidden"" name=""PaRes"" value=""" & Request("PaRes") & """/>")
		Response.Write(" <input type=""hidden"" name=""ClientIP"" value=""" & Request.ServerVariables("REMOTE_ADDR") & """/>")
		'Response.Write(" <input type=""submit"" />")
		Response.Write("</form></body></html>")
        Response.End
    Else
        Set xmlRet = Server.CreateObject("Msxml2.DOMDocument.3.0")
		xRetParams = Replace(Replace(Replace(xRetParams, "&gt;", ">"), "&lt;", "<"), "``", """")
        xmlRet.loadXML(xRetParams)
        X_3dRedirect = xmlRet.selectSingleNode("//VerifyThreeDEnrollmentResult/AcsURL").text
        d3PaReq = Trim(xmlRet.selectSingleNode("//VerifyThreeDEnrollmentResult/PaReq").text)
        d3MD = Trim(xmlRet.selectSingleNode("//VerifyThreeDEnrollmentResult/MD").text)
        Set xmlRet = Nothing
		'X_3dRedirect = X_3dRedirect & "&PaReq=" & d3PaReq & "&MD=" & d3MD & "&TermUrl=" & Server.URLEncode(Session("ProcessURL") & "remoteCharge_Back.asp")
		'Response.Write(X_3dRedirect)
		Response.Write("<html><body onload=""document.getElementById('frmMain').submit()""><form id=""frmMain"" method=""post"" action=""" & X_3dRedirect & """>")
		Response.Write(" <input type=""hidden"" name=""PaReq"" value=""" & d3PaReq & """/>")
		Response.Write(" <input type=""hidden"" name=""MD"" value=""" & d3MD & """/>")
		Response.Write(" <input type=""hidden"" name=""TermUrl"" value=""" & Session("ProcessURL") & "remoteCharge_ccDebitAlliedBack.asp" & """/>")
		'Response.Write(" <input type=""submit"" />")
		Response.Write("</form></body></html>")
        Response.End
    End If
End Function

Dim debitAllied3d_ExecuteThreeD : debitAllied3d_ExecuteThreeD = False
Function debitAllied3d()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer, soapEvnelope
	If int(X_TypeCredit) = 8 Or int(X_Payments) > 1 Then Call throwError("508") 
	xNames = Split(ReplaceEscapeSequences(X_ccHolderName), " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = ""
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = ""
	If  (Cint(X_TypeCredit) <> 1) Or (Cint(X_TransType) <> 0 And Cint(X_TransType) <> 4) Then
	    debitAllied() : Exit Function
	Else
		If Request("PaRes") <> "" Or debitAllied3d_ExecuteThreeD Then
			soapEvnelope = "ExecuteThreeD"
			'X_DebitReferenceCode = GetTransRefCode()
			'GetTransInfo("Lca_ReplyCode='553' And Lca_TransNum=" & X_RefTransID)
			'Set xmlRet = Server.CreateObject("Msxml2.DOMDocument.3.0")
			'xRetParams = Replace(Replace(Replace(xRetParams, "&gt;", ">"), "&lt;", "<"), "``", """")
			'xmlRet.loadXML(xRetParams)
			'd3PaReq = Trim(xmlRet.selectSingleNode("//VerifyThreeDEnrollmentResult/PaReq").text)
			ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
			"<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">" & vbCrLf & _
			"  <soap12:Body>" & vbCrLf & _
			"	<" & soapEvnelope & " xmlns=""http://service.381808.com/"">" & vbCrLf & _
			"	  <MerchantID>" & sAccountId & "</MerchantID>" & vbCrLf & _
			"	  <SiteID>" & sAccountSubId & "</SiteID>" & vbCrLf & _
			"	  <MD>" & X_DebitReferenceNum & "</MD>" & vbCrLf & _
			"	  <PaReq>" & Request("PaRes") & "</PaReq>" & vbCrLf & _
			"	</" & soapEvnelope & ">" & vbCrLf & _
			"  </soap12:Body>" & vbCrLf & _
			"</soap12:Envelope>"
		Else
            If X_PhoneNumber <> "" And Len(Replace(Replace(X_PhoneNumber, " ", ""), "-", "")) > 20 Then throwError("514")
            If BACity <> "" And Len(BACity & "") > 30 Then throwError("541")
            X_DebitReferenceNum = ""
			soapEvnelope = "VerifyThreeDEnrollment"
			X_DebitReferenceCode = GetTransRefCode()
			ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
			"<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">" & vbCrLf & _
			"  <soap12:Body>" & vbCrLf & _
			"	<" & soapEvnelope & " xmlns=""http://service.381808.com/"">" & vbCrLf & _
			"	  <MerchantID>" & sAccountId & "</MerchantID>" & vbCrLf & _
			"	  <SiteID>" & sAccountSubId & "</SiteID>" & vbCrLf & _
			"	  <IPAddress>" & sIP & "</IPAddress>" & vbCrLf & _
			"	  <Amount>" & formatNumber(X_Amount, 2, true, false, false) & "</Amount>" & vbCrLf & _
			"	  <CurrencyID>" & GetCurISOName(Cint(X_Currency)) & "</CurrencyID>" & vbCrLf & _
			"	  <FirstName>" & fName & "</FirstName>" & vbCrLf & _
			"	  <LastName>" & sName & "</LastName>" & vbCrLf & _
			"	  <Phone>" & Replace(Replace(X_PhoneNumber, " ", ""), "-", "") & "</Phone>" & vbCrLf & _
			"	  <Address>" & BACHAddr1 & "</Address>" & vbCrLf & _
			"	  <City>" & BACity & "</City>" & vbCrLf & _
			"	  <State>" & BAStateCode & "</State>" & vbCrLf & _
			"	  <Country>" & BACountryCode & "</Country>" & vbCrLf & _
			"	  <ZipCode>" & BAZipCode & "</ZipCode>" & vbCrLf & _
			"	  <Email>" & X_Email & "</Email>" & vbCrLf & _
			"	  <CardNumber>" & Replace(X_ccNumber, " ", "") & "</CardNumber>" & vbCrLf & _
			"	  <CardName>" & X_ccHolderName & "</CardName>" & vbCrLf & _
			"	  <ExpiryMonth>" & X_ccExpMM & "</ExpiryMonth>" & vbCrLf & _
			"	  <ExpiryYear>20" & X_ccExpYY & "</ExpiryYear>" & vbCrLf & _
			"	  <CardCVV>" & X_ccCVV2 & "</CardCVV>" & vbCrLf & _
			"	  <MerchantReference>" & X_DebitReferenceCode & "</MerchantReference>" & vbCrLf & _
			"	</" & soapEvnelope & ">" & vbCrLf & _
			"  </soap12:Body>" & vbCrLf & _
			"</soap12:Envelope>"
		End If
	End if

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    If isTestTerminal Then UrlAddress = "https://service.381808.com/Merchant.asmx" _
	Else UrlAddress = "https://service.381808.com/Merchant.asmx" 'new

	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false', sAccountSubId, sAccountPassword
		HttpReq.setRequestHeader "Content-Type", "application/soap+xml; charset=utf-8"
		'HttpReq.setRequestHeader "SOAPAction", "process"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		TimeControl.Append("A_SEND") 'Time Optimization *********

		If Len(sResDetails) = 0 Then Call throwError("521")
		sDebitReturnAnswer = Left(sResDetails, 3900)
		'Response.Write(sResDetails) : Response.End()
	
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "<CardCVV>" & X_ccCVV2, "<CardCVV>" & GetSafePartialNumber(X_ccCVV2))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

		Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
		xmlRet.LoadXML HttpReq.responseText
		sReturnCode = Trim(xmlRet.selectSingleNode("//Status").text) 'State
		sApprovalNumber = xmlRet.selectSingleNode("//TransactionID").text
		X_CustomErrorDesc = xmlRet.selectSingleNode("//Message").text
		If Request("PaRes") = "" Then X_DebitReferenceNum = Trim(xmlRet.selectSingleNode("//MD").text) 'State
		'sApprovalNumber = xmlRet.selectSingleNode("//TransactionID").text
		'X_CustomErrorDesc = xmlRet.selectSingleNode("//Message").text

	On Error Goto 0
	If (Len(sReturnCode) < 1) Then Call throwError("520")
	If Request("PaRes") <> "" Or debitAllied3d_ExecuteThreeD Then
		If Trim(sReturnCode) = "1" Then
			sReturnCode = "000"
		ElseIf Trim(sReturnCode) = "3" Then 
			sReturnCode = "001"
		Else
			sReturnCode = "1" & Trim(sReturnCode)
		End if	
	    Call DebitIProcessResult(sReturnCode, sApprovalNumber)
	Else
		If Trim(sReturnCode) = "1" And xmlRet.selectSingleNode("//State").text = "9" Then
			X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
    	    sIsSendUserConfirmationEmail = False
			Call DebitIProcessResult("553", sApprovalNumber)
		Else
			sReturnCode = "002"
            'If X_DebitReferenceNum <> "00000000-0000-0000-0000-000000000000" And Not debitAllied3d_ExecuteThreeD Then 
			If X_DebitReferenceNum <> "" And Not debitAllied3d_ExecuteThreeD Then 
				debitAllied3d_ExecuteThreeD = True
				debitAllied3d()
            Else
                'debitAllied() : Exit Function
			End If
		End if	
	End If
	Set xmlRet = Nothing
	Set HttpReq = Nothing

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>