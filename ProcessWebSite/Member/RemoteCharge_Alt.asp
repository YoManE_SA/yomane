﻿<%
Dim sTermName, sAccountId, sAccountSubId, sAccountPassword, sAccountId3D, sAccountSubId3D, sAccountPassword3D, X_RetURL
Dim debitCompany, bIsShvaMasterTerminal, nProcessingMethod, sMcc, isManipulateAmount, isTestTerminal
Dim X_RefTransID, X_DebitReferenceCode, X_DebitReferenceNum, X_ApprovalNumber, ccTypeEngShow, isTestOnly, ccTypeID
Dim X_TerminalHasNext, X_TransINC, X_3dRedirect, X_CustomErrorDesc
Dim nTransactionID, nInsertDate, X_DPRET
Dim X_Currency, X_Amount, X_Payments, X_OrderNumber, X_Comment, PaymentMethod, X_TypeCredit
%>
<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/timer_class.asp"-->
<!--#include file="../include/MerchantTerminals.asp" -->
<!--#include file="remoteCharge_Functions.asp"-->
<%
bUseOldTerminal = True
X_CompanyNum = DBText(request("CompanyNum"))
requestSource = DBText(request("RequestSource"))
sIP = DBText(request("ClientIP"))
If sIP = "" Then sIP = Request.ServerVariables("REMOTE_ADDR")

'Saving charge data into log
LogSavingChargeDataID = 0
LogSavingChargeDataID = Sub_LogSavingChargeData(requestSource, X_CompanyNum, sIP)

X_Payments = 1
isTestOnly = 0
PaymentMethod = TestNumVar(request("PaymentMethod"), PMD_CS_MIN, PMD_CS_MAX, PMD_CS_MIN)
fraudDetectionLog_id = 0 : X_TransType = 0 : sTransactionTypeID = requestSource : isTestOnly = 0

sCompanyID = ExecScalar("SELECT ID FROM tblCompany WHERE CustomerNumber='" & X_CompanyNum & "'", 0)
If sCompanyID = 0 Then ThrowError "500"
X_TransType = 0
X_RefTransID = 0
ccTypeID = 0

X_Amount = TestNumVar(Request("Amount"), 0, -1, 0)
X_Currency = TestNumVar(Request("Currency"), 0, -1, 1)
X_OCurrency = X_Currency
X_OAmount = X_Amount

nApprovalRecurringSeries = "NULL"
nApprovalRecurringChargeNumber = "NULL"

X_PhoneNumber = DBText(Request("PhoneNumber"))
X_PersonalNumber = DBText(Request("PersonalNum"))
X_Email = DBText(Request("Email"))
X_Customer = TestNumVar(Request("CustomerId"), 0, -1, 0)
replyUrl = DBText(Request("ReplyUrl"))
X_Comment = DBText(Request("Comment"))
X_OrderNumber = DBText(Request("Order"))
X_TypeCredit = TestNumVar(Request("CreditType"), 0, 8, 1)
X_RetURL = DBText(Request("RetURL"))
X_BillingType = TestNumVar(Request("BillingType"), 0, -1, 0)
BACHAddr1 = DBText(Request("BillingAddress1"))
BACHAddr2 = DBText(Request("BillingAddress2"))
BACity = DBText(Request("BillingCity"))
BAZipCode = DBText(Request("BillingZipCode"))
BAState = DBText(Request("BillingState"))
BACountry = DBText(Request("BillingCountry"))
dpApproval = DBText(Request("ApprovalNum"))
dpRet = DBText(Request("ReplyCode"))
TerminalNumber = DBText(Request("TerminalNumber"))

'---------------------------------------------------------
'   Check clearing company by terminal
'---------------------------------------------------------
Function GetDebitTerminalInfo(sTermNum)
	Set rsTerminal = Server.CreateObject("Adodb.Recordset")
	rsTerminal.Open "SELECT dc_isActive, dc_TempBlocks, dbo.GetDecrypted256(accountPassword256) As accountPassword," & _
		" dbo.GetDecrypted256(accountPassword3D256) As accountPassword3D, tblDebitTerminals.* FROM tblDebitTerminals" & _
		" LEFT JOIN tblDebitCompany ON tblDebitTerminals.DebitCompany=tblDebitCompany.debitCompany_id" & _
		" WHERE TerminalNumber='" & trim(sTermNum) & "'", oledbData, 0, 1
	GetDebitTerminalInfo = "---"
	If Not rsTerminal.EOF Then
		If Not rsTerminal("isActive") Then GetDebitTerminalInfo = "592"
		If Not rsTerminal("dc_isActive") Then GetDebitTerminalInfo = "592"
		If rsTerminal("dc_TempBlocks") > 0 Then GetDebitTerminalInfo = "524"
		debitCompany = rsTerminal("DebitCompany")
		bIsNetpayTerminal = rsTerminal("isNetpayTerminal")
		bIsShvaMasterTerminal = rsTerminal("isShvaMasterTerminal")
		nProcessingMethod = rsTerminal("processingMethod")
		sMcc = rsTerminal("dt_mcc")
		isManipulateAmount = rsTerminal("dt_isManipulateAmount")
		isTestTerminal = rsTerminal("dt_IsTestTerminal")

		sTermName = Trim(rsTerminal("dt_Descriptor"))
		'terminal login
		sAccountId = Trim(rsTerminal("accountId"))
		sAccountSubId = Trim(rsTerminal("accountSubId"))
		if rsTerminal("accountPassword") <> "" Then sAccountPassword = rsTerminal("accountPassword")
		'terminal 3D login
		sAccountId3D = Trim(rsTerminal("accountId3D"))
		sAccountSubId3D = Trim(rsTerminal("accountSubId3D"))
		if rsTerminal("accountPassword3D") <> "" Then sAccountPassword3D = rsTerminal("accountPassword3D")
	End If
	rsTerminal.close
	If debitCompany = 20 Then bIsUseMaxMind = False
End Function

GetDebitTerminalInfo(TerminalNumber)
If bUseOldTerminal Then DebitIProcessResult = DebitProcessResult(dpRet, dpApproval, 0) _
Else DebitIProcessResult = DebitProcessResultIN(dpRet, dpApproval, 0)

call closeConnection()
%>
