﻿<%
Function debit_Atlas()
    Select Case PaymentMethod
	Case PMD_PD_MICROPAYMENTIN, PMD_PD_MICROPAYMENTOUT:
	    debit_Atlas = debit_SMS_Atlas()
	Case PMD_PD_IVRPERCALL, PMD_PD_IVRPERMIN, PMD_PD_IVRFREETIME:
	    debit_Atlas = debit_Phone_Atlas()
    End Select
End Function

Function debit_Phone_Atlas()
	Dim HttpReq, UrlAddress, ParamList, sApprovalNumber, strAct, sDebitReturnAnswer
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
	UrlAddress = "http://77.246.119.8/vcharge/get_pin/"
	If Cint(X_TransType) = 1 Then Call throwError("503")
    'SIMULATION LINK: http://192.168.5.11/member/remoteCharge_PhoneAtlasIVRBack.asp?uniqueID=111&startsec=54&endsec=54&duration=3&callerID=542452&maxduration=5&progress=0.7&status=ended&target=NP&pin=924
	If trim(X_TransType) = "4" Then
		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	ElseIf (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	Else
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = "customer_id=" & Server.URLEncode(sAccountPassword) & vbCrLf & _
		"&intern_id=" & X_DebitReferenceCode & vbCrLf & _
		"&project_id=" & vbCrLf
	End If
	ParamList = Replace(ParamList, vbCrLf, "")
	'On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
		HttpReq.Send(ParamList)
		TxtRet = HttpReq.responseText
		SaveLogChargeAttemptRequestResponse ParamList, TxtRet
		HttpError = FormatHttpRequestError(HttpReq)
	'On Error GoTo 0
	If IsNull(TxtRet) Or Len(TxtRet) = 0 Then Call throwError("521")
	sApprovalNumber = TxtRet
    X_RetSMSInstruction = "Please Call To " & X_SMSShortCode & vbCrLf & "Your pin code is " & sApprovalNumber
	Set HttpReq = Nothing
    DebitIProcessResult "556", sApprovalNumber
End Function

Function debit_SMS_Atlas()
	Dim HttpReq, UrlAddress, ParamList, sApprovalNumber, strAct, sDebitReturnAnswer
	
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
    if isTestTerminal Then UrlAddress = "http://77.246.119.12:8080/SMSGatewaySimulator/incoming/mt/10547" Else UrlAddress = "http://88.198.101.46:8080/smsgateway/incoming/mt/10547" 'https://gw.girogate.de
	If Cint(X_TransType) = 1 Then Call throwError("503")

	If trim(X_TransType) = "4" Then
		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	ElseIf (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	Else
		Dim nTagArray, sChannnelName
		If X_BillingType = eMD_UserToSystem Then
		    X_RetSMSInstruction = "Send SMS with " & X_SMSShortCode & " %1"
		    DebitIProcessResult "556", ""
		    Response.End
		End If
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = "encoding=TEXT" & vbCrLf & _
		"&password=" & Server.URLEncode(sAccountPassword) & vbCrLf & _
		"&mobile_address=" & Server.URLEncode(X_PhoneNumber) & vbCrLf & _
		"&sms_address=XX" & Server.URLEncode(sAccountSubId) & vbCrLf & _
		"&operator=" & Server.URLEncode(X_OperatorID) & vbCrLf & _
		"&charge_type=PREMIUM9" & vbCrLf & _
		"&country=" & BACountryCode & vbCrLf & _
		"&message=" & Server.URLEncode(sCompanyDescriptor)
	End If
	ParamList = Replace(ParamList, vbCrLf, "")
	'On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
		HttpReq.Send(ParamList)
		TxtRet = HttpReq.responseText
		SaveLogChargeAttemptRequestResponse ParamList, TxtRet
		HttpError = FormatHttpRequestError(HttpReq)
	'On Error GoTo 0
	'Response.Write(session("ProcessURL") & "<br>" & ParamList & vbCrLf & "<br>" & TxtRet & vbCrLf & "<br>") : Response.End()
	If IsNull(TxtRet) Or Len(TxtRet) = 0 Then Call throwError("521")
	sReturnCode = GetURLValue(TxtRet, "StatusCode")
	sApprovalNumber = GetURLValue(TxtRet, "TransactionID")
	sValidityPeriod = GetURLValue(TxtRet, "ValidityPeriod")
	If sReturnCode = "" Then Call throwError("520") 'internal error
	Set HttpReq = Nothing
	DebitIProcessResult sReturnCode, sApprovalNumber
End Function
%>