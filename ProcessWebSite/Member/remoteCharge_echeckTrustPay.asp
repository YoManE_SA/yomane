<%
'---------------------------------------------------------------------
'	Debit TrustPay
'---------------------------------------------------------------------

Function CalcSign(secret, message)
	Dim KeyBlob, MessageBlob
	Set KeyBlob = CreateTextBlob() : KeyBlob.Ansi = secret
	Set MessageBlob = CreateTextBlob() : MessageBlob.Ansi = message
	CalcSign = HMAC("HMACSHA256", KeyBlob, MessageBlob).Hex
	Set MessageBlob = Nothing
	Set KeyBlob = Nothing
End Function

Function remoteCharge_echeck_TrustPay_BackDetect()
	Dim outString, iRs, signature, sOuterIP
	sOuterIP = IIF(session("Identity") = "Local", "http://80.179.39.10:8080/Default.aspx", Session("ProcessURL") & "remoteCharge_echeckTrustPayNotify.asp")
	Set iRs = oleDbData.Execute("Select tblCompanyTransPending.*, tblDebitTerminals.accountId, dbo.GetDecrypted256(tblDebitTerminals.accountPassword256) As accountPassword" & _
		 " From tblCompanyTransPending " & _
		 " Left Join tblDebitTerminals ON(tblDebitTerminals.TerminalNumber = tblCompanyTransPending.TerminalNumber)" & _
		 " Where companyTransPending_id=" & xTransID)
	If Not iRs.EOF Then
		signMessage = iRs("accountId") & iRs("trans_amount") & GetCurISOName(iRs("trans_currency")) & iRs("DebitReferenceCode")
		outString = _
			"<html><head></head><body>" & vbCrLf & _
			"<FORM name=""form1"" action=""https://" & IIF(iRs("isTestOnly"), "test", "ib") & ".trustpay.eu/mapi/paymentservice.aspx"" method=""POST"">" & vbCrLf & _
				"<INPUT type=""hidden"" name=""AID"" value=""" & iRs("accountId") & """ />" & vbCrLf & _
				"<INPUT type=""hidden"" name=""AMT"" value=""" & iRs("trans_amount") & """ />" & vbCrLf & _
				"<INPUT type=""hidden"" name=""CUR"" value=""" & GetCurISOName(iRs("trans_currency")) & """ />" & vbCrLf & _
				"<INPUT type=""hidden"" name=""REF"" value=""" & iRs("DebitReferenceCode") & """ />" & vbCrLf & _
				"<INPUT type=""hidden"" name=""SIG"" value=""" & CalcSign(iRs("accountPassword"), signMessage) & """ />" & vbCrLf & _
				"<INPUT type=""hidden"" name=""DSC"" value=""" & sCompanyDescriptor & """ />" & vbCrLf & _
				"<INPUT type=""hidden"" name=""URL"" value=""" & sOuterIP & """ />" & vbCrLf & _
				"<INPUT type=""hidden"" name=""NURL"" value=""" & sOuterIP & """ />" & vbCrLf & _
				"<INPUT type=""submit"" name=""Continute"" />" & vbCrLf & _ 
			"</FORM>" & vbCrLf & _
			"</body></html>"
	End If
	iRs.Close()
	Response.Write(outString)
End Function

Function debit_TrustPay()
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		Call throwError("532") 'Reftrans not Found - Not Supported
	Else
		X_DebitReferenceCode = GetTransRefCode()
	    X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
	    'Response.Write(X_3dRedirect)
		sReturnCode = "553"
	End if
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>