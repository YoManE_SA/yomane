<%
'---------------------------------------------------------------------
'	Debit BillingPartners
'---------------------------------------------------------------------

function debit_BillingPartners()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer
	xNames = Split(Trim(X_ccHolderName), " ")
	if Ubound(xNames) < 1 Then Call throwError("517")
	fName = xNames(0) : sName = xNames(1)
	sOuterIP = IIF(session("Identity") = "Local", "http://80.179.39.10/member/", Server.URLEncode(Replace(session("ProcessURL"), "http://", "https://"))) & "remoteCharge_echeckWebbillingNotify.asp"
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.XMLHTTP")'Msxml2.ServerXMLHTTP.3.0
	'HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
	UrlAddress = "http://testclearing.webbilling.com:8080/init"
	'if X_Amount < 25 Then throwError("505")
	If (Cint(X_TransType) = 4) Then
		if X_ConfirmNumber = "" Then Call throwError("532") 'Reftrans not Found
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<Transaction transactionID=""" + Server.URLEncode(X_ConfirmNumber) + """>" & vbCrLf & _
		" <Request>" & vbCrLf & _
		"	<Merchant>" & vbCrLf & _
		"	<MerchantID>" & Server.URLEncode(sAccountID) & "</MerchantID>" & vbCrLf & _
		"	<Password>" & Server.URLEncode(sAccountPassword) & "</Password>" & vbCrLf & _
		"	<ProfileID>" & "default" & "</ProfileID>" & vbCrLf & _
		"	</Merchant>" & vbCrLf & _
		"	<Language>en</Language>" & vbCrLf & _
		"	<PinCallData>" & vbCrLf
		If Request("PhoneNumber") <> "" Then ParamList = ParamList & "	<PhoneNumber>" & Server.URLEncode(Request("PhoneNumber")) & "</PhoneNumber>" & vbCrLf
		If Request("PINCODE") <> "" Then ParamList = ParamList & "	<Pin>" & Server.URLEncode(Request("PINCODE")) & "</Pin>" & vbCrLf
		ParamList = ParamList & _
		"	</PinCallData>" & vbCrLf & _
		" </Request>" & vbCrLf & _
		"</Transaction>" & vbCrLf
	ElseIf (Cint(X_TransType) = 0) Then
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<Transaction>" & vbCrLf & _
		" <Request>" & vbCrLf & _
		"  <Merchant>" & vbCrLf & _
		"   <MerchantID>" & Server.URLEncode(sAccountID) & "</MerchantID>" & vbCrLf & _
		"   <Password>" & Server.URLEncode(sAccountPassword) & "</Password>" & vbCrLf & _
		"   <ProfileID>" & "default" & "</ProfileID>" & vbCrLf & _
		"  </Merchant>" & vbCrLf & _
		"  <Language>en</Language>" & vbCrLf & _
		"  <AddressData>" & vbCrLf & _
		"   <FirstName>" & Server.URLEncode(sName) & "</FirstName>" & vbCrLf & _
		"   <LastName>" & Server.URLEncode(fName) & "</LastName>" & vbCrLf & _
		"   <Street>" & Server.URLEncode(BACHAddr1) & "</Street>" & vbCrLf & _
		"   <ZIP>" & Server.URLEncode(BAZipCode) & "</ZIP>" & vbCrLf & _
		"   <City>" & Server.URLEncode(BACity) & "</City>" & vbCrLf & _
		"   <CountryID>" & Server.URLEncode(BACountryCode) & "</CountryID>" & vbCrLf & _
		"  </AddressData>" & vbCrLf & _
		"  <EmailData>" & vbCrLf & _
		"   <EmailAddress>" & Server.URLEncode(X_Email) & "</EmailAddress>" & vbCrLf & _
		"  </EmailData>" & vbCrLf & _
		"  <Payment type=""single"">" & vbCrLf & _
		"   <Amount>" & Server.URLEncode(FormatNumber(X_Amount, 2, -1, 0)) & "</Amount>" & vbCrLf & _
		"	<Currency>" & Server.URLEncode(GetCurISOName(Cint(X_Currency))) & "</Currency>" & vbCrLf & _
		"	<TADescriptor>" & Server.URLEncode(sCompanyDescriptor) & "</TADescriptor>" & vbCrLf & _
		"	<NotificationURL signal=""onSuccess"">" & sOuterIP & "?debit_Webbilling=ON</NotificationURL>" & vbCrLf & _
		"	<NotificationURL signal=""onRefund"">" & sOuterIP & "?debit_Webbilling=ON</NotificationURL>" & vbCrLf & _
		"	<NotificationURL signal=""onChargeBack"">" & sOuterIP & "?debit_Webbilling=ON</NotificationURL>" & vbCrLf & _
		"	<NotificationURL signal=""onReversal"">" & sOuterIP & "?debit_Webbilling=IGN</NotificationURL>" & vbCrLf & _
		"	<NotificationURL signal=""onCredit"">" & sOuterIP & "?debit_Webbilling=IGN</NotificationURL>" & vbCrLf & _
		"  </Payment>" & vbCrLf & _
		"  <DirectDebitData>" & vbCrLf & _
		"   <BankAccountNumber>" & Server.URLEncode(ckacct) & "</BankAccountNumber>" & vbCrLf & _
		"   <BankRoutingNumber>" & Server.URLEncode(ckaba) & "</BankRoutingNumber>" & vbCrLf & _
		"   <BankAccountOwner>" & Server.URLEncode(X_ccHolderName) & "</BankAccountOwner>" & vbCrLf & _
		"   <CountryID>" & Server.URLEncode(X_BankCountry) & "</CountryID>" & vbCrLf & _
		"  </DirectDebitData>" & vbCrLf & _
		"  <PinCallData>" & vbCrLf & _
		"   <Language>" + BACountryCode + "</Language>" & vbCrLf & _
		"   <Length>6</Length>" & vbCrLf & _
		"   <PhoneNumber>" & Server.URLEncode(X_PhoneNumber) & "</PhoneNumber>" & vbCrLf & _
		"  </PinCallData>" & vbCrLf & _
		"  <Identification>" & Server.URLEncode(X_DebitReferenceCode) & "</Identification>" & vbCrLf & _
		"  <Domain>" & Server.URLEncode(session("TempOpenURL")) & "</Domain>" & vbCrLf & _
		"  <RemoteIP>" & sIP & "</RemoteIP>" & vbCrLf & _
		" </Request>" & vbCrLf & _
		"</Transaction>" & vbCrLf
	Else
		Call throwError("503")
	End If	
	'Response.Write("<PRE><br>" & UrlAddress & "?request=" + ParamList) : Response.End
	ParamList = Replace(ParamList, vbCrLf, "")
	'On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
		HttpReq.Send("request=" & ParamList)
		TxtRet = HttpReq.responseText
		TxtRet = Replace(Replace(TxtRet, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba))
		SaveLogChargeAttemptRequestResponse Replace(Replace(ParamList, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba)), TxtRet
		HttpError = FormatHttpRequestError(HttpReq)
		Set xmlRet = HttpReq.responseXml
	'On Error GoTo 0
	'Response.Write(TxtRet)
	'FileAppendData "Webbilling.txt", "ResDetails:" & TxtRet & "; ReturnCode:" & vbCrLf & vbCrLf
	'Response.End
	if (xmlRet.documentElement Is Nothing) Then
		Set xdResponse = Server.CreateObject("Microsoft.XMLDOM")
		xdResponse.loadXML(TxtRet)
		Set xmlRet = xdResponse
	End If
	'Response.Write(xmlRet.Text & "<br>")
	If IsNull(TxtRet) Or Len(TxtRet) = 0 Then Call throwError("521")
	sReturnCode = xmlRet.selectSingleNode("//Status").text
	If sReturnCode = "" Then Call throwError("520") 'internal error
	If sReturnCode = "0" Then
		sReturnCode = "000"
		sApprovalNumber = xmlRet.selectSingleNode("//TransactionID").text
	ElseIf sReturnCode = "1" Or sReturnCode = "3" Then
		sReturnCode = "001"
		sApprovalNumber = xmlRet.selectSingleNode("//TransactionID").text
	ElseIf sReturnCode = "-9" Then
		sReturnCode = "552"
		sApprovalNumber = xmlRet.selectSingleNode("//TransactionID").text
	Else
		sError = xmlRet.selectSingleNode("//ErrorText").text
	End If
	If (Cint(X_TransType) = 4) Then OleDbData.Execute "Delete From tblCompanyTransPending Where DebitCompanyID=37 And DebitApprovalNumber='" & X_ConfirmNumber & "'"
	DebitIProcessResult sReturnCode, sApprovalNumber
	Set xmlRet = Nothing
	Set HttpReq = Nothing
End function
%>