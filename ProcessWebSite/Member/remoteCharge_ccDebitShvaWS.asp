<%
'---------------------------------------------------------------------
'		debit company Shva - sending transaction
'---------------------------------------------------------------------
'CHANGE : master terminal has to appear on sAccountAuthCode1, require sAccountId, sAccountPassword

Function debitShvaWSCreateRequst()
    Dim HttpReq
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    'https://www.shva-online.co.il/ash/test/abscheck/absrequest.asmx
    If isTestTerminal Then UrlAddress =  "http://192.168.5.11:81/Shva" _ 
	Else UrlAddress =  "https://www.shva-online.co.il/ash/abscheck/absrequest.asmx"

	HttpReq.open "POST", UrlAddress, false
	HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    Set debitShvaWSCreateRequst = HttpReq
End Function

Function debitShvaWSChangePassword()
    Dim ParamList, sResDetails, newPassword, srvResponse
    newPassword = "aabb112233"'"AutoP_" & Year(Now) & "_" & Month(Now) & "_" & Day(Now)
	ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & vbCrLf & _
		" <soap:Body>" & vbCrLf & _
		"  <ChangePassword xmlns=""http://shva.co.il/xmlwebservices/"">" & vbCrLf & _
		"   <MerchantNumber>" & sAccountId & "</MerchantNumber>" & vbCrLf & _
		"   <UserName>" & sAccountSubId & "</UserName>" & vbCrLf & _
		"   <Password>" & sAccountPassword & "</Password>" & vbCrLf & _
		"   <NewPassword>" & newPassword & "</NewPassword>" & vbCrLf & _
		"  </ChangePassword>" & vbCrLf & _
		" </soap:Body>" & vbCrLf & _
		"</soap:Envelope>"
    debitShvaWSChangePassword = False
	Set HttpReq = debitShvaWSCreateRequst()
	HttpReq.setRequestHeader "SOAPAction", "http://shva.co.il/xmlwebservices/ChangePassword"
	HttpReq.send ParamList
    Set xmlData = Server.CreateObject("Microsoft.XMLDOM")
	xmlData.LoadXML HttpReq.responseText
    srvResponse = xmlData.selectSingleNode("//ChangePasswordResult").text
    If srvResponse = "0" Then
        If ExecSql("Update tblDebitTerminals Set accountPassword256=dbo.getEncrypted256('" & newPassword & "') Where id=" & TerminalId) = 1 Then sAccountPassword = newPassword
        debitShvaWSChangePassword = True
    Else
        'Response.Write(UrlAddress & "<br/>" & ParamList & "<br/>" & HttpReq.responseText & "<br/>" & vbCrLf)
        'Response.Write("SHVA WS - Error setting terminal password") : Response.End()
    	SaveLogChargeAttemptRequestResponse ParamList, HttpReq.responseText
        debitShvaWSChangePassword = False
    End If
End Function

Function debitShvaWS()
	Dim HttpReq, UrlAddress, ParamList, sResDetails, srvResponse, sExtraData
	Dim nEachPay, nFirstPay, nDollarAmount, nSendCurrency
	Dim xmlData, intOut, batchValue, IsTrack2
	If X_PersonalNumber <> "" And Not IsNumericString(X_PersonalNumber, 0, 10) Then call throwError("508")
    nSendCurrency = CInt(X_Currency) + 1
	If CInt(X_Currency) > 1 Then 'And X_TransType = "0" And X_ConfirmNumber = "" 
        If debitCompany = 11 Then
            nDollarAmount = ConvertCurrency(CInt(X_Currency), 1, CCur(X_Amount))
            nSendCurrency = 2
        Else 
            nDollarAmount = CCur(X_Amount)
            nSendCurrency = 0
        End If
    Else 
        nDollarAmount = CCur(X_Amount)
    End If
	If CInt(X_TypeCredit) = 8 And CInt(X_Payments) > 1 then
		nEachPay = CLng(CLng(nDollarAmount * 100) / X_Payments)
		nFirstPay = nEachPay + (CLng(nDollarAmount * 100) - (nEachPay * X_Payments))
	End If
	X_DebitReferenceCode = GetTransRefCode()
	If Len(X_DebitReferenceCode) Then X_DebitReferenceCode = Right(X_DebitReferenceCode, 8)
    IsTrack2 = IIF(Len(Trim(X_Track2)) > 2, true, false)

	Do
	    ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		    "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & vbCrLf & _
		    " <soap:Body>" & vbCrLf & _
		    "  <AuthCreditCardFullEx xmlns=""http://shva.co.il/xmlwebservices/"">" & vbCrLf & _
		    "   <MerchantNumber>" & sAccountId & "</MerchantNumber>" & vbCrLf & _
		    "   <UserName>" & sAccountSubId & "</UserName>" & vbCrLf & _
		    "   <Password>" & sAccountPassword & "</Password>" & vbCrLf & _
		    "   <TransactionDate_yyyyMMdd>" & Year(Now) & Right("0" & Month(Now), 2) & Right("0" & Day(Now), 2) & "</TransactionDate_yyyyMMdd>" & vbCrLf & _
		    "   <TransactionTime_HHmm>" & Right("0" & Hour(Now), 2) & Right("0" & Minute(Now), 2) & "</TransactionTime_HHmm>" & vbCrLf & _
		    "   <UniqueTransactionNumber_SixDigits></UniqueTransactionNumber_SixDigits>" & vbCrLf & _
    	    "   <Code>" & IIF(IsTrack2, "00", "50") & "</Code>" & vbCrLf & _
            "   <CardNum>" & IIF(IsTrack2, "", Replace(X_ccNumber, " ", "")) & "</CardNum>" & vbCrLf & _
            "   <ExpDate_YYMM>" & IIF(IsTrack2, "", X_ccExpYY & X_ccExpMM) & "</ExpDate_YYMM>" & vbCrLf & _
            "   <Cvv2>" & IIF(IsTrack2 OR X_ccCVV2="", "", Trim(X_ccCVV2)) & "</Cvv2>" & vbCrLf
            If IsTrack2 Then
                'Sending Track2 
                If InStr(1, X_Track2, "=") > 0 Then
                    If Left(X_Track2, 1) = ";" Then X_Track2 = Right(X_Track2, Len(X_Track2) - 1)
                    If InStrRev(X_Track2, "?") > 0 Then X_Track2 = Left(X_Track2, InStrRev(X_Track2, "?") - 1)
                    ParamList = ParamList & "   <Track2>" & X_Track2 & "</Track2>" & vbCrLf
                Else
                    ParamList = ParamList & "   <Track2>" & X_ccNumber & "=" & X_Track2 & "</Track2>" & vbCrLf
                End if
            Else
                'Sending card info
                ParamList = ParamList & "   <Track2></Track2>" & vbCrLf
            End If
            ParamList = ParamList & _
		    "   <Amount>" & CLng(nDollarAmount * 100)  & "</Amount>" & vbCrLf & _
		    "   <CochavAmount>0</CochavAmount>" & vbCrLf & _
		    "   <TransactionType>" & IIF(Cint(X_TypeCredit) = 0, "51", "01") & "</TransactionType>" & vbCrLf & _
		    "   <CreditTerms>" & IIF(CInt(X_TypeCredit) > 0, CInt(X_TypeCredit), 1) & "</CreditTerms>" & vbCrLf & _
		    "   <Currency>" & nSendCurrency & "</Currency>" & vbCrLf & _
		    "   <AuthNum>" & X_ConfirmNumber & "</AuthNum>" & vbCrLf
		    If CInt(X_Payments) > 1 Then
			    ParamList = ParamList & _
			    "   <FirstAmount>" & nFirstPay & "</FirstAmount>" & vbCrLf & _
			    "   <NonFirstAmount>" & nEachPay & "</NonFirstAmount>" & vbCrLf & _
			    "   <NumOfPayment>" & IIF(int(X_TypeCredit) = 8 And X_Payments > 1, X_Payments - 1, X_Payments) & "</NumOfPayment>" & vbCrLf
		    End If
		    If sAccountAuthCode1 <> "" Then
			    ParamList = ParamList & _
			    "   <SapakMutav>1</SapakMutav>" & vbCrLf & _
			    "   <SapakMutavNo>" & sAccountAuthCode1 & "</SapakMutavNo>" & vbCrLf
            Else
			    ParamList = ParamList & _
                "   <SapakMutav>0</SapakMutav>" & vbCrLf & _
			    "   <SapakMutavNo></SapakMutavNo>" & vbCrLf
		    End If
		    ParamList = ParamList & _
		    "   <UniqNum>" & X_DebitReferenceCode & "</UniqNum>" & vbCrLf & _
		    "   <ClubCode></ClubCode>" & vbCrLf & _
		    "   <ParamJ>" & IIF(CInt(X_TransType) = 1, "5", "4") & "</ParamJ>" & vbCrLf & _
		    "   <AddData>" & X_DebitReferenceCode & "</AddData>" & vbCrLf
		    if X_PersonalNumber <> "" Then ParamList = ParamList & "   <Id>" & Trim(X_PersonalNumber) & "</Id>" & vbCrLf
			ParamList = ParamList & "   <Last4Digits>" & Right(Replace(X_ccNumber, " ", ""), 4) & "</Last4Digits>" & vbCrLf
		    If Cint(X_Currency) > 1 Then 
			    ParamList = ParamList & _
			    "   <TransactionCurrency>" & GetCurISOCode(CInt(X_Currency)) & "</TransactionCurrency>" & vbCrLf & _
			    "   <TransactionAmount>" & CLng(X_Amount * 100) & "</TransactionAmount>" & vbCrLf
		    End If
		    ParamList = ParamList & _
		    "  </AuthCreditCardFullEx>" & vbCrLf & _
		    " </soap:Body>" & vbCrLf & _
		    "</soap:Envelope>"

    	'On Error Resume Next
    	    Set HttpReq = debitShvaWSCreateRequst()
    	    HttpReq.setRequestHeader "SOAPAction", "http://shva.co.il/xmlwebservices/AuthCreditCardFullEx"
		    HttpReq.send ParamList
		    sResDetails = HttpReq.responseText
		    HttpError = FormatHttpRequestError(HttpReq)
		    TimeControl.Append("A_SEND") 'Time Optimization *********

            'Response.Write(UrlAddress & "<br/>" & ParamList & "<br/>" & sResDetails & "<br/>" & vbCrLf) ': Response.End()
		    If Len(sResDetails) = 0 Then Call throwError("521")
		    sResDetails = Left(sResDetails, 3900)

		    Set xmlData = Server.CreateObject("Microsoft.XMLDOM")
		    xmlData.LoadXML HttpReq.responseText
            srvResponse = xmlData.selectSingleNode("//AuthCreditCardFullExResult").text
            intOut = xmlData.selectSingleNode("//ResultRecord").text
		    batchValue = xmlData.selectSingleNode("//TransactionRecord").text
            sExtraData = xmlData.selectSingleNode("//sExtraData").text
        'On Error Goto 0
        If (srvResponse <> "250") Then Exit Do
        if Trim(intOut & "") = "" Then intOut = srvResponse
        If Not debitShvaWSChangePassword() Then
            Call DebitIProcessResult(srvResponse, "")
            Exit Function
        End If
    Loop While True
	
    ParamList = Replace(ParamList, X_ccNumber, GetSafePartialNumber(X_ccNumber))
	ParamList = Replace(ParamList, "<Cvv2>" & X_ccCVV2, "<Cvv2>" & GetSafePartialNumber(X_ccCVV2))
	sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
	SaveLogChargeAttemptRequestResponse ParamList, sResDetails

	nReply = Left(intOut, 3)
	If nReply = "000" Then 
		X_ConfirmNumber = Mid(intOut, 71, 7)
		X_BatchData = batchValue
		CTP_Status = 1
		'save batch row
	ElseIf nReply = "001" Then 
		nReply = "1001"
	End If
    FileAppendData "Shva.txt", "DateTime:" & Now & vbCrLf & "Type:" & IIF(X_TypeCredit = 0, "���� �����", "���� �����") & IIF(CInt(X_TransType) = 1, " - ����� ����", "") & vbCrLf & "Installments:" & X_Payments & vbCrLf & "Currency:" & GetCurISOName(CInt(X_Currency)) & vbCrLf & "Reply:" & Left(intOut, 3) & vbCrLf & "Confirmation:" & X_ConfirmNumber & vbCrLf & "Request:" & vbCrLf & ParamList & vbCrLf & "Response:" & vbCrLf & Replace(xmlData.xml, "><", ">" & vbCrLf & "<") & vbCrLf & "Extra:" & sExtraData & vbCrLf & vbCrLf & vbCrLf & vbCrLf

    If Replace(X_ccNumber, " ", "") = "4580000000000000" Then isTestOnly = 1
	Call DebitIProcessResult(nReply, X_ConfirmNumber)
End Function
%>