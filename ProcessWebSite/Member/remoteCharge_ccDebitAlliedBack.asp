﻿<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="remoteCharge_Functions.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/MerchantTerminals.asp"-->
<!--#include file="../include/timer_class.asp"-->
<!--#include file="remoteCharge_ccDebitAllied3d.asp"-->
<%
Server.ScriptTimeout = 300
Dim xRequestParams, x_TransId

If Request.Form("MD") <> "" Then
	x_TransId = ExecScalar("Select ID From tblCompanyTransPending Where DebitCompanyID=58 And DebitReferenceNum='" & Request.Form("MD") & "'", 0)
    If x_TransId <> 0 Then 
        GetTransInfo("Lca_ReplyCode='553' And Lca_TransNum=" & x_TransId)
	    Call remoteCharge_debitAllied3d_BackDetect()
    Else 
	    FileAppendData "Allied.txt", "3DS TransNotFound:" & Now & " FORM:" & Request.Form & " QS:" & Request.QueryString & vbCrLf
        Response.Write("Transaction not found")
    End If
Else
	FileAppendData "Allied.txt", "Request:" & Now & " FORM:" & Request.Form & " QS:" & Request.QueryString & vbCrLf
	Dim X_action, X_DebitReferenceCode, X_ChargebackAmount, X_ApprovalNumber
	bUseOldTerminal = True
	nApprovalRecurringSeries = "NULL"

	X_action = LCase(Request("Action"))
	X_ApprovalNumber = Request("TransactionID")
	X_ChargebackAmount = Request("ChargebackAmount")
	X_ReasonCode = Request("ReasonCode")
	X_Reason = Request("Reason")
    If X_ApprovalNumber = "" Then X_ApprovalNumber = "!!Unknwon"

	If X_action = "chargeback" Or X_action = "retrieval" Then
		x_TransId = ExecScalar("Select ID From tblCompanyTransPass Where DebitCompanyID=58 And ApprovalNumber='" & DBText(X_ApprovalNumber) & "'", 0)
		If x_TransId <> 0 Then
			If X_action = "chargeback" Then
				Call ChargeBackTransDB(x_TransId, X_Reason, X_ReasonCode)
				'ExecSql("DECLARE @TodayDate AS DATETIME = GETDATE(); Exec CreateCHB " & x_TransId & ",@TodayDate, '" & Replace(X_Reason, "'", "''") & "'," & TestNumVar(X_ReasonCode, 0, -1, 0))
			ElseIf X_action = "retrieval" Then
				Call PhotocopyTransDB(x_TransId, X_Reason, X_ReasonCode)
				'ExecSql("DECLARE @TodayDate AS DATETIME = GETDATE(); Exec RequestPhotocopyCHB " & x_TransId & ",@TodayDate, '" & Replace(X_Reason, "'", "''") & "'," & TestNumVar(X_ReasonCode, 0, -1, 0))
			End If

            ' set transaction as fraud
            If X_ReasonCode = "82" Or X_ReasonCode = "83" Then
                ExecSql("UPDATE [tblCompanyTransPass] SET [IsFraudByAcquirer] = 1 WHERE ID = " & x_TransId)
            End If

			Response.Write("OK " & x_TransId)
		Else
			Response.Write("Transaction Not Found")
		End If
	Else
		Response.Write("Unhandled Action")
	End If
End If
%>
