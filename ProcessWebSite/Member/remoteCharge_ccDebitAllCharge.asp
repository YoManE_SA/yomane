<%
'---------------------------------------------------------------------
'	Debit AllCharge
'---------------------------------------------------------------------
Function debitAllCharge()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer
	Do While InStr(X_ccHolderName, "  ") > 0
		X_ccHolderName = Replace(X_ccHolderName, "  ", " ")
	Loop
	xNames = Split(Trim(X_ccHolderName), " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	If Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	If Len(sCompanyDescriptor) > 13 Then sCompanyDescriptor = Left(sCompanyDescriptor, 13)

	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
		ParamList = _
            "DCPId=" & Server.URLEncode(sAccountId) & _
            "&DCPPassword=" & Server.URLEncode(sAccountPassword) & _
            "&transactionID=" & Server.URLEncode(X_DebitReferenceCode) & _
            "&Amount=" & (formatNumber(X_Amount, 2) * 100) & _
            "&Currency=" & GetCurISOName(Cint(X_Currency))
        If isTestTerminal Then UrlAddress = "http://demo.allcharge.com/html/aspTransactionRefund.asp" _
        Else UrlAddress = "https://incharge.allcharge.com/html/aspTransactionRefund.asp"
	Else
		X_DebitReferenceCode = GetTransRefCode()
		Dim sendCardType : sendCardType = 0
		Select case PaymentMethod
		Case PMD_CC_AMEX: sendCardType = 1
		Case PMD_CC_VISA: sendCardType = 2
		case PMD_CC_MASTERCARD: sendCardType = 3
		End Select
		ParamList = _
            "DCPId=" & Server.URLEncode(sAccountId) & _
            "&DCPPassword=" & Server.URLEncode(sAccountPassword) & _
            "&email=" & Server.URLEncode(X_Email) & _
            "&currency=" & GetCurISOName(Cint(X_Currency)) & _
            "&cardType=" & sendCardType & _
            "&cardNum=" & Replace(X_ccNumber, " ", "") & _
            "&CVV2/PIN=" & Server.URLEncode(X_ccCVV2) & _
            "&FirstName=" & Server.URLEncode(Left(fName, 20)) & _
            "&LastName=" & Server.URLEncode(Left(sName, 20)) & _
            "&Address=" & Server.URLEncode(BACHAddr1) & _
            "&Address2=" & Server.URLEncode(BACHAddr2) & _
            "&City=" & Left(Server.URLEncode(BACity), 30) & _
            "&State=" & Server.URLEncode(BAStateCode) & _
            "&postCode=" & Server.URLEncode(BAZipCode) & _
            "&Country=" & Server.URLEncode(BACountryCode) & _
            "&Phone=" & Server.URLEncode(X_PhoneNumber) & _
            "&ExpMonth=" & X_ccExpMM & _
            "&ExpYear=20" & X_ccExpYY & _
            "&amount=" & (formatNumber(X_Amount, 2) * 100) & _
            "&transactionID=" & Server.URLEncode(X_DebitReferenceCode) & _
            "&IPaddress=" & Server.URLEncode(sIP) & _
            "&itemType=other" & _
		    "&Desc=" & Server.URLEncode(sCompanyDescriptor)
            '"&CCReceipt=" & Server.URLEncode(X_ConfirmNumber)
        If isTestTerminal Then UrlAddress = "http://demo.allcharge.com/html/aspCreditCharge.asp" _
        Else UrlAddress = "https://incharge.allcharge.com/html/aspCreditCharge.asp"
	End if
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		
        sReturnCode = GetURLValue(sResDetails, "retCode")
        sApprovalNumber = GetURLValue(sResDetails, "receipt")
	    sErrorCode = Replace(GetURLValue(sResDetails, "ErrCode"), "'", "")
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "CVV2/PIN=" & X_ccCVV2, "CVV2/PIN=" & GetSafePartialNumber(X_ccCVV2))
		sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails
        'transactionID
		TimeControl.Append("A_SEND") 'Time Optimization *********
	On Error Goto 0
	'Response.Write(ParamList & "<br>" & sResDetails) : Response.End()
	If Trim(sReturnCode) = "0" Then
		sReturnCode = "000"
	Else
	    If sErrorCode <> "" Then sReturnCode = sErrorCode
    	If Trim(sReturnCode) = "001" Then sReturnCode = "002" '001 is taken by netpay
    End If	
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>