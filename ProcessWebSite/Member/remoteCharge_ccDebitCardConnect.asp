<%
'---------------------------------------------------------------------
'	Debit CardConnect
'---------------------------------------------------------------------
Function cardConnectGetField(strData, strField) 'extract json field
	Dim sIndex, eIndex
	sIndex = InStr(1, strData, """" & strField & """:")
	If sIndex <= 0 Then Exit Function
	sIndex = sIndex + Len(strField) + 3 
	eIndex = InStr(sIndex + 1, strData, ",")
	If eIndex <= 0 Then Exit Function
	cardConnectGetField = Mid(strData, sIndex, eIndex - sIndex)
	If Left(cardConnectGetField, 1) = """" Then cardConnectGetField = Right(cardConnectGetField, Len(cardConnectGetField) - 1)
	If Right(cardConnectGetField, 1) = """" Then cardConnectGetField = Left(cardConnectGetField, Len(cardConnectGetField) - 1)
End Function

Function debitCardConnect()
	Call debitCardConnectIN(False)
End Function

Function debitCardConnectIN(useVoid)
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer
    If isTestTerminal Then UrlAddress = "https://" & sAccountSubId & ".prinpay.com:6443/cardconnect/rest" _
    Else UrlAddress = "https://" & sAccountSubId & ".prinpay.com:8443/cardconnect/rest"
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_DebitReferenceNum = "" Then Call throwError("532") 'Reftrans not Found
		ParamList = _
            "{""merchid"":""" & sAccountId & """" & _
            ",""amount"":""" & (formatNumber(X_Amount, 2, True, False, False)) & """" & _
            ",""currency"":""" & GetCurISOName(CInt(X_Currency)) & """" & _
            ",""retref"":""" & X_DebitReferenceNum & """" & _
            ",""authcode"":""" & X_ConfirmNumber & """" & _
			"}"
		If CInt(X_TypeCredit) = 0 And useVoid Then 
			UrlAddress = UrlAddress & "/void"
		Else 
			UrlAddress = UrlAddress & IIF(CInt(X_TypeCredit) = 0, "/refund", "/capture")
		End If
	Else
		X_DebitReferenceCode = GetTransRefCode()
		ParamList = _
            "{""merchid"":""" & sAccountId & """" & _
            ",""accttype"":""" & ccTypeEngName & """" & _
            ",""account"":""" & Replace(X_ccNumber, " ", "") & """" & _
            ",""expiry"":""" & X_ccExpMM & X_ccExpYY & """" & _
            ",""amount"":""" & (formatNumber(X_Amount, 2, True, False, False)) & """" & _
            ",""currency"":""" & GetCurISOName(CInt(X_Currency)) & """" & _
            ",""name"":""" & X_ccHolderName & """" & _
            ",""Address"":""" & BACHAddr & """" & _
            ",""city"":""" & BACity & """" & _
            ",""region"":""" & BAStateCode & """" & _
            ",""country"":""" & BACountryCode & """" & _
            ",""phone"":""" & X_PhoneNumber & """" & _
            ",""postal"":""" & BAZipCode & """" & _
            ",""Ecomind"":""" & "E" & """" & _
            ",""cvv2"":""" & X_ccCVV2 & """" & _
            ",""orderid"":""" & X_DebitReferenceCode & """" & _
            ",""capture"":""" & IIF(Cint(X_TransType) = 1, "N", "Y") & """}"
            '"""CCReceipt=" & Server.URLEncode(X_ConfirmNumber)
			UrlAddress = UrlAddress & "/auth"
	End if
    authToken = Hash("MD5", sAccountSubId & ":" & sAccountPassword).Base64

	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	On Error Resume Next
		HttpReq.open "PUT", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/json"
		HttpReq.setRequestHeader "Authorization", "Basic " & authToken
		HttpReq.send ParamList
		'sResDetails = Replace(sResDetails, "meta", "me-ta")
		HttpError = FormatHttpRequestError(HttpReq)
		sResDetails = HttpReq.responseText
		'Response.Write(ParamList & "<br>" & sResDetails)

		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		'sDebitRequest = Replace(sDebitRequest, "CVV2/PIN=" & X_ccCVV2, "CVV2/PIN=" & GetSafePartialNumber(X_ccCVV2))
		sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		SaveLogChargeAttemptRequestResponse UrlAddress & VbCrLf & "<br>" & VbCrLf & sDebitRequest, sResDetails
        'transactionID
		TimeControl.Append("A_SEND") 'Time Optimization *********

        sReturnCode = cardConnectGetField(sResDetails, "respcode")
        sApprovalNumber = cardConnectGetField(sResDetails, "authcode")
		X_DebitReferenceNum = cardConnectGetField(sResDetails, "retref")
	    X_CustomErrorDesc = Replace(cardConnectGetField(sResDetails, "resptext"), "'", "")
	On Error Goto 0
	If Len(sResDetails) = 0 Then Call throwError("521")
	If (Len(sReturnCode) < 1) Then Call throwError("520")

	'Response.End()
	If Trim(sReturnCode) = "00" Then
		sReturnCode = "000"
	Else
		If CInt(X_TypeCredit) = 0 And Not useVoid And sReturnCode = "25" Then
			debitCardConnectIN(True)
			Exit Function
		End if
    	If Trim(sReturnCode) = "001" Then sReturnCode = "002" '001 is taken by netpay
    End If
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>