﻿<%
'---------------------------------------------------------------------
'	Debit Stored payment method from blanace
'---------------------------------------------------------------------
Function debitInternal()
    Dim ParamList, HttpReq, responseXml, UrlAddress, sReturnCode, retPmId
    X_DebitReferenceCode = GetTransRefCode()
	If Not ((Cint(X_TypeCredit) = 1) Or (Cint(X_TypeCredit) = 0)) Then Call throwError("508") 
    If Cint(X_TransType) <> 0 Then Call throwError("503")
    'validate merchant group is in client app identity


    Dim signSend : signSend = CalcHash("SHA256", "NP_" & GetCurISOName(Cint(X_Currency)) & "_" & X_ccNumber, True) 
    ParamList = _
		"<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf & _
		"<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">" & vbCrLf & _
		"  <soap12:Body>" & vbCrLf & _
		"	<Charge xmlns=""WebServices"">" & vbCrLf & _
		"	  <data>" & vbCrLf & _
		"        <CardNumber>" & X_ccNumber & "</CardNumber>" & vbCrLf & _
		"        <ExpMonth>" & X_ccExpMM & "</ExpMonth>" & vbCrLf & _
		"        <ExpYear>20" & X_ccExpYY & "</ExpYear>" & vbCrLf & _
		"        <PinCode>" & X_ccCVV2 & "</PinCode>" & vbCrLf & _
		"        <Amount>" & FormatNumber(IIF(Cint(X_TypeCredit) = 0, -X_Amount, X_Amount), 2, True, False, False) & "</Amount>" & vbCrLf & _
		"        <CurrencyIso>" & GetCurISOName(Cint(X_Currency)) & "</CurrencyIso>" & vbCrLf & _
		"        <Text>" & X_PayFor & "</Text>" & vbCrLf & _
		"        <RefNumber>" & X_DebitReferenceCode & "</RefNumber>" & vbCrLf & _
		"        <MerchantGroupID>" & MerchantGroupID & "</MerchantGroupID>" & vbCrLf & _
    	"        <MerchantID>" & sCompanyID & "</MerchantID>" & vbCrLf & _
		"        <Signature>" & signSend & "</Signature>" & vbCrLf & _
		"	  </data>" & vbCrLf & _
		"	</Charge>" & vbCrLf & _
		"  </soap12:Body>" & vbCrLf & _
		"</soap12:Envelope>" & vbCrLf

    Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
    UrlAddress = WEBSERVICES_URL & "balance.asmx"
	'On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/soap+xml"
		HttpReq.send ParamList
		SaveLogChargeAttemptRequestResponse ParamList, HttpReq.responseText
	    HttpError = FormatHttpRequestError(HttpReq)
        'Response.Write(UrlAddress & vbCrLf & " <br> " & vbCrLf & ParamList & vbCrLf & " <br> " & vbCrLf & HttpReq.responseText)
		Set responseXml = Server.CreateObject("Microsoft.XMLDOM")
		responseXml.LoadXML HttpReq.responseText
		sReturnCode = responseXml.selectSingleNode("//Code").text
		retPmId = TestNumVar(responseXml.selectSingleNode("//PaymentMethodId").text, 0, -1, -1)
		X_CustomErrorDesc = responseXml.selectSingleNode("//Message").text
		If (Not responseXml.selectSingleNode("//Number") Is Nothing) Then sApprovalNumber = responseXml.selectSingleNode("//Number").text
	'On Error Goto 0
    if (retPmId > -1) Then PaymentMethod = retPmId
    If sReturnCode = "0" Then sReturnCode = "000"
   	Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
End Function
%>