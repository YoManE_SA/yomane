<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<script language="vbscript" runat="server" src="../include/func_encryption.asp"></script>
<!--#include file="../include/timer_class.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="RemoteCharge_Functions.asp"-->
<!--#include file="remoteCharge_echeckFunctions.asp"-->
<%
    Dim X_OCurrency, X_OAmount, nApprovalRecurringSeries, X_ReplyURL
    bUseOldTerminal = True
    nApprovalRecurringSeries = "NULL"

    Dim xLogChargeAttemptID, xMerchantNum, xRequestParams, xSendParams, xRetParams, xDebitCompany, X_DebitReferenceCode, X_DebitReferenceNum, xTransID, xLogReplyCode
    xTransID = TestNumVar(Request("TransID"), 0, -1, 0)
    If xTransID = 0 Then xTransID = TestNumVar(Request("MD"), 0, -1, 0)
    If xTransID <> 0 Then GetTransInfo("Lca_ReplyCode='553' And Lca_TransNum=" & xTransID)
    If TestNumVar(xLogChargeAttemptID, 0, -1, 0) = 0 Then xDebitCompany = -1
    Select Case xDebitCompany
     Case -1: 'transaction not detected, check manually
        Call remoteCharge_echeck_PPro_BackDetectAlways()
        Call remoteCharge_echeck_InPay_BackDetectAlways()
     Case 1: 'local netpay
		Call remoteCharge_debitNULL_BackDetect()
     case 26, 68 : 'PayOn
		Call remoteCharge_debitPayOnCTPE_BackDetect()
	 Case 37: 'TrustPay
		Call remoteCharge_echeck_WebbillingHpp_BackDetect()
     Case 38 'The PPro
        Call remoteCharge_echeck_PPro_BackDetect()
     Case 18,46 'B&S
	    Call remoteCharge_ccDebitBNS3D_BackDetect()
	 Case 42 'InPay
	    Call remoteCharge_echeck_InPay_BackDetect()
	 Case 49 'deltaPay
	    Call remoteCharge_CC_DeltaPay_BackDetect()
	 Case 50: 'TrustPay
		Call remoteCharge_echeck_TrustPay_BackDetect()	
	 'Case 51: 'Global tech
        'Call remoteCharge_debitGlobalTechSystems_BackDetect()	
	 Case 57: 'Qiwi
		Call remoteCharge_Phone_Qiwi_BackDetect()	
	 Case 58: 'Allied
		Call remoteCharge_debitAllied3d_BackDetect()	
	 Case 61: 'Actum
		Call remoteCharge_echeck_Actum_BackDetect()	
	 Case 70, 71: 'Atlas
		Call remoteCharge_cc_Atlas_BackDetect()	
     Case 73: 
        Call remoteCharge_cc_RemoteNP_BackDetect()         
     Case 77: 
        Call remoteCharge_cc_ACS_BackDetect()         
	End Select
%>
<!--#include file="remoteCharge_ccDebitNULL.asp"-->
<!--#include file="remoteCharge_echeckInPay.asp"-->
<!--#include file="remoteCharge_ccDebitBNS3D.asp"-->
<!--#include file="remoteCharge_echeckPPro.asp"-->
<!--#include file="remoteCharge_echeckTrustPay.asp"-->
<!--#include file="remoteCharge_ccDebitDeltaPayHPP.asp"-->
<!--#include file="remoteCharge_echeckWebbillingHpp.asp"-->
<!--#include file="remoteCharge_PhoneQiwi.asp"-->
<!--#include file="remoteCharge_echeckActum.asp"-->
<!--#include file="remoteCharge_ccDebitAllied3d.asp"-->
<!--#include file="remoteCharge_ccDebitPayOnCTPE.asp"-->
<!--#include file="remoteCharge_ccDebitAtlas.asp"-->
<!--#include file="remoteCharge_ccDebitRemoteNP.asp"-->
<!--#include file="remoteCharge_ccDebitACS.asp"-->
<%
'<!--include file="remoteCharge_echeckWebbilling.asp"-->
'<!--include file="remoteCharge_ccDebitGlobalTechSystems3D.asp"-->
%>
<%
Set rsData = nothing
call closeConnection()
%>
