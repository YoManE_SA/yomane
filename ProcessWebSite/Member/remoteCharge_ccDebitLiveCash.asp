<%
'---------------------------------------------------------------------
'	Debit AllCharge
'---------------------------------------------------------------------

Function debit_LivaCash_Verify2Digits(val)
    If Len(val) < 2 Then val = "0" & val
    debit_LivaCash_Verify2Digits = val
End Function

Function debit_LivaCash_GetRequestType()
    If Cint(X_TransType) = 0 Then
        If Cint(X_TypeCredit) = 0 Then
            If (RefOAmount = X_Amount) Then debit_LivaCash_GetRequestType = "VOID" _
            Else debit_LivaCash_GetRequestType = "RETURN" 'refund
        Else
            debit_LivaCash_GetRequestType = "SALE"
        End If
    ElseIf Cint(X_TransType) = 1 Then
        debit_LivaCash_GetRequestType = "PREAUTH"
    ElseIf Cint(X_TransType) = 2 Then
        debit_LivaCash_GetRequestType = "POSTAUTH" 'capture
    Else
        Call throwError("532")
    End If
End Function

Function debit_LivaCash()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber
	xNames = Split(X_ccHolderName, " ")
	If Ubound(xNames) > -1 Then fName = xNames(0) Else fName = "x"
	If Ubound(xNames) > 0 Then sName = xNames(1) Else sName = "x"
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
    If isTestTerminal Then UrlAddress = "HTTPS://www.lpg24.net/test-api/LPG_Global_Gateway.asp" _
    Else UrlAddress = "HTTPS://www.lpg24.net/api/LPG_Global_Gateway.asp"
    'livecash bin - 629971
    Dim requestType : requestType = debit_LivaCash_GetRequestType()
    reqHash = requestType & sAccountId & sTerminalNumber & sAccountPassword & sAccountSubId
    reqHash = LCase(Hash("MD5", reqHash).Hex)
    X_DebitReferenceCode = GetTransRefCode()

	ParamList = "<?xml version=""1.0"" encoding=""UTF-8"" standalone=""yes"" ?>" & vbCrLf & _
        "<GatewayRequest>" & vbCrLf & _
		" <Merchant_Request>" & requestType & "</Merchant_Request>" & vbCrLf & _
        " <Merchant_Id>" & sAccountId & "</Merchant_Id>" & vbCrLf & _
        " <Terminal_Id>" & sTerminalNumber & "</Terminal_Id>" & vbCrLf & _
        " <Terminal_Pass>" & sAccountPassword & "</Terminal_Pass>" & vbCrLf & _
        " <MD5_Hash>" & reqHash & "</MD5_Hash>" & vbCrLf & _
        " <Merchant_Reference>" & X_DebitReferenceCode & "</Merchant_Reference>" & vbCrLf & _
        " <First_Name>" & Left(fName, 20) & "</First_Name>" & vbCrLf & _
        " <Last_Name>" & Left(sName, 20) & "</Last_Name>" & vbCrLf & _
        " <Address_1>" & BACHAddr1 & "</Address_1>" & vbCrLf & _
        " <Address_2>" & BACHAddr2 & "</Address_2>" & vbCrLf & _
        " <City>" & Left(BACity, 30) & "</City>" & vbCrLf & _
        " <State>" & BAStateCode & "</State>" & vbCrLf & _
        " <Country>" & BACountryCode & "</Country>" & vbCrLf & _
        " <Zip_Code>" & BAZipCode & "</Zip_Code>" & vbCrLf & _
        " <phone>" & X_PhoneNumber & "</phone>" & vbCrLf & _
        " <totalamount>" & X_Amount & "</totalamount>" & vbCrLf & _
        " <Currency_Request>" & GetCurISOName(Cint(X_Currency)) & "</Currency_Request>" & vbCrLf & _
        " <Email>" & X_Email & "</Email>" & vbCrLf & _
        " <card_type>" & ccTypeEngName & "</card_type>" & vbCrLf & _
        " <Card_number>" & Replace(X_ccNumber, " ", "") & "</Card_number>" & vbCrLf & _
        " <securitycode>" & X_ccCVV2 & "</securitycode>" & vbCrLf & _
        " <cardexpiremonth>" & X_ccExpMM & "</cardexpiremonth>" & vbCrLf & _
        " <cardexpireyear>20" & X_ccExpYY & "</cardexpireyear>" & vbCrLf & _
        " <cardHolderIP>" & sIP & "</cardHolderIP>" & vbCrLf & _
        " <Merchant_date>" & Year(Now) & "-" & debit_LivaCash_Verify2Digits(Month(Now)) & "-" & debit_LivaCash_Verify2Digits(Day(Now)) & "</Merchant_date>" & vbCrLf & _
        " <Merchant_Time>" & Time() & "</Merchant_Time>" & vbCrLf
	If CInt(X_TransType) = 2 Or CInt(X_TypeCredit) = 0 Then _
        ParamList = ParamList & " <LPG_Approval_code>" & X_ConfirmNumber & "</LPG_Approval_code>" & vbCrLf
    ParamList = ParamList & "</GatewayRequest>" & vbCrLf
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "<securitycode>" & X_ccCVV2, "<securitycode>" & GetSafePartialNumber(X_ccCVV2))
		sResDetails = Replace(sResDetails, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

	    TimeControl.Append("A_SEND") 'Time Optimization *********
	On Error Goto 0

    Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
	xmlRet.LoadXML HttpReq.responseText
	sReturnCode = Trim(xmlRet.selectSingleNode("//Status").text)
	If Trim(sReturnCode) = "OK" Then
		sReturnCode = "000"
    	sApprovalNumber = xmlRet.selectSingleNode("//Approval_code").text
	Else
    	If Trim(sReturnCode) = "001" Then sReturnCode = "002" '001 is taken by netpay
    	X_CustomErrorDesc = xmlRet.selectSingleNode("//Fail_Reason").text
		'If Not IsNummeric(sReturnCode) Then sReturnCode = "002"
    End If
	Set HttpReq = Nothing
    'Response.Write("Result" & bvCrLf)
	'Response.Write(ParamList & "<br>" & sResDetails) : Response.End()

	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>