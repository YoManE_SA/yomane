<%
'---------------------------------------------------------------------
'		debit company Shva - sending transaction
'---------------------------------------------------------------------

Function formatShvaString(iCur, iAmout, TTP)
	'Dim IsoCurreny
	'IsoCurreny = Array("376", "840", "978", "826")
	If Len(Trim(X_Track2)) > 2 Then 
        If Left(X_Track2, 1) = ";" Then X_Track2 = Right(X_Track2, Len(X_Track2) - 1)
        If Right(X_Track2, 1) = "?" Then X_Track2 = Left(X_Track2, Len(X_Track2) - 1)
        If InStr(1, X_Track2, "=") > 0 Then
	        sString = "A" & X_Track2 & "W" & Right(X_ccNumber, 4)
        Else
	        sString = "A" & X_ccNumber & "=" & X_Track2 & "W" & Right(X_ccNumber, 4)
        End if
	Else 
	    sString = "B" & replace(X_ccNumber," ","")
	End If    
	sString = sString & "C" & FormatNumber(iAmout,2,-1,0,0) * 100
	'sString = "B" & replace(X_ccNumber," ","") & "C" & formatnumber(iAmout,2,-1,0,0)*100
	If trim(X_TypeCredit) = "0" Then sString = sString  & "D51" & "1" _
	Else sString = sString  & "D01" & X_TypeCredit
	If CInt(iCur) < 2 Then sString = sString & (CInt(iCur) + 1) & "50" _
	Else sString = sString & "050" & "$" & GetCurISOCode(CInt(iCur)) 'CHANGED BY ASHRAIT VER 5.83
	If X_ConfirmNumber <> "" Then
		'20100113 Tamir - when refunding transaction by VISA CAL gift card, approval number is not passed
		If Trim(X_TypeCredit) <> "0" Or Left(Replace(X_ccNumber," ",""), 6) <> "458041" Then sString = sString & "E" & X_ConfirmNumber
	End If
	If int(X_TypeCredit) = 8 then
		If int(X_Payments) > 1 then
			nPay = int(formatnumber(X_Amount,2,-1,0,0)/X_Payments)
			nPayFirst = formatnumber(X_Amount,2,-1,0,0)-nPay*(X_Payments-1)
			sString = sString & "F" & nPayFirst*100 & "G" & nPay*100 & "H" & X_Payments-1
		End If
	Elseif int(X_TypeCredit) = 6 then
		sString = sString & "H" & X_Payments
	Else
		X_Payments = 1
	End If
	sString = sString & "I110"
	If TTP = "1" Then 
		sString = sString & "J5" 'Authorization
	Else 
		sString = sString & "J4" 'Debit
	End If
	sString = sString & "T" & X_ccExpMM & X_ccExpYY
	If isCVV2 then
		sString = sString & "U" & X_ccCVV2
	Else
		If X_ccCVV2 <> "" then sString = sString & "U" & X_ccCVV2
	End If

	If isPersonalNumber then
		sString = sString & "Y" & X_PersonalNumber
	Else
		If X_PersonalNumber <> "" Then sString = sString & "Y" & X_PersonalNumber
	End If
	sString = sString &  "X" & X_Customer' & "'" & sIP
	formatShvaString = sString
End Function

Function SendShvaCharge(nDbtType, nSendString)
	'Response.Write(nSendString) : Response.End()
	isShvaServer = True 'IIF(Trim(X_CompanyNum)="5722306",True,False)

	If nDbtType=0 then
		Set oNETSAFE = Server.CreateObject("NETSAFE.TCPINTR32")
		oNETSAFE.HostAddr = "212.25.127.42"
		SendShvaCharge = oNETSAFE.ShvINTR99(CStr(sTerminalNumber),trim(nSendString))
	Else
		If isShvaServer Then
		    On Error Resume Next
			'Send shva from admincash server (test phase)
			If Session("Identity") = "Local" Then sSend = "http://192.168.5.11:105" _
			Else sSend = "http://shva.netpay-intl.com"
			sSend = sSend & "/shva.asp?CompanyID=" & sCompanyID & "&Terminal=" & CStr(sTerminalNumber) & "&IsMaster=" & IIf(bIsShvaMasterTerminal, 1, 0) & "&ShvaString=" & Trim(nSendString)
			'If Trim(X_CompanyNum)="5722306" Then Response.Write("Server: " & now() & "<br />" : sSend)
			Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")
			HttpReq.setTimeouts 1*1000, 2*1000, 10*1000, 600*1000
			HttpReq.open "GET", sSend, False
			HttpReq.send
			SendShvaCharge = HttpReq.responseText
    		HttpError = FormatHttpRequestError(HttpReq)
    		On Error Goto 0
		Else		
			'If Trim(X_CompanyNum)="5722306" Then Response.Write("Local: " & now() & "<br />" : sSend)
			Set oNETSAFE = Server.CreateObject("Compunet.AshAsp")
			SendShvaCharge = oNETSAFE.ShvINTR99MD(CStr(sTerminalNumber),trim(nSendString), bIsShvaMasterTerminal)
			'FileAppendData "SHVA.txt", Now & vbcrlf & "Terminal:" & CStr(sTerminalNumber) & "(" & bIsShvaMasterTerminal & ")" & vbcrlf & nSendString & vbcrlf & SendShvaCharge & vbcrlf & vbcrlf & vbcrlf
		End if
		Dim sDebitRequest : sDebitRequest = nSendString
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "U" & X_ccCVV2, "U" & GetSafePartialNumber(X_ccCVV2))
		SendShvaCharge = Replace(SendShvaCharge, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		SaveLogChargeAttemptRequestResponse sDebitRequest, SendShvaCharge
	End if

	Set oNETSAFE = Nothing
	SendShvaCharge = Trim(dbText(Replace(SendShvaCharge, chr(0), " ")))
	If Len(SendShvaCharge) >= 20 Then 
		SendShvaCharge = SetStringValue(SendShvaCharge, "xxxxxxxxxxxxxxx", 5, 15)
	Else
		Call throwError("521")
	End if	
End Function

Function debitShva(nDebitType)
	Dim sDebitReturnAnswer, ShvaTestCount
	nReply = ""

	'On Error Resume Next
	If CInt(X_Currency) > 1 And X_TransType="0" And X_ConfirmNumber = "" Then
		DollarAmount = ConvertCurrency(CInt(X_Currency), 1, X_Amount)
		sDebitReturnAnswer = SendShvaCharge(nDebitType, formatShvaString(1, DollarAmount, "1"))
		nReply = Left(sDebitReturnAnswer, 3)
		If nReply = "000" Then
			X_ConfirmNumber = Mid(sDebitReturnAnswer, 71, 7)
		Else
			if nReply = "001" Then nReply = "201"
			Call DebitIProcessResult(nReply, "")
			Exit Function
		End if
	End if
	'----------------------------------------------------------------------------------------------------
	sString = formatShvaString(X_Currency, X_Amount, X_TransType)
	'Response.Write(sString) : Response.End
	For ShvaTestCount = 0 To 3
		sDebitReturnAnswer = SendShvaCharge(nDebitType, sString)
		nReply = Left(sDebitReturnAnswer,3)
		If nReply <> "009" Then Exit For
		NetpayCryptoObj.Delay 1500
	Next	
	If Len(sDebitReturnAnswer) > 25 Then
		sDebitReturnAnswer = Left(sDebitReturnAnswer, Len(sDebitReturnAnswer)-25)
		sApprovalNumber = mid(trim(sDebitReturnAnswer),71,7)
	End if
	On Error GoTo 0
	If Replace(X_ccNumber, " ", "") = "4580000000000000" Then isTestOnly = 1
	if nReply = "001" Then nReply = "201"
	Call DebitIProcessResult(nReply, sApprovalNumber)
end function
%>