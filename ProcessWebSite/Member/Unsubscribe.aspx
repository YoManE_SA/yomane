﻿<%@ Page %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        MyBase.OnLoad(e)
        Dim CompanyID As Long = 0
        Select Case Request("linkType")
            Case "Newsletter"
                Dim subscribtionStatus As Boolean = False
                If Not String.IsNullOrEmpty(Request("Email")) Then
                    Dim iReader As SqlDataReader = dbPages.ExecReader("Select ID, IsInterestedInNewsletter From tblCompany Where Mail='" & Replace(Request("Email"), "'", "''") & "'")
                    If iReader.Read Then
                        CompanyID = dbPages.TestVar(iReader("ID"), 0, -1, 0)
                        subscribtionStatus = dbPages.TestVar(iReader("IsInterestedInNewsletter"), False)
                    End If
                    iReader.Close()
                End If
                If CompanyID = 0 Then
                    mvSubscribeStatus.SetActiveView(vwAccountNotFound)
                Else
                    If subscribtionStatus Then
                        dbPages.ExecSql("Update tblCompany Set IsInterestedInNewsletter=0 Where ID=" & CompanyID)
                        mvSubscribeStatus.SetActiveView(vwUnsubscribeOK)
                    Else
                        mvSubscribeStatus.SetActiveView(vwAlreadyUnsubscribed)
                    End If
                End If
            Case Else
                mvSubscribeStatus.SetActiveView(vwUnknownAction)
        End Select
    End Sub
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
<br/><br/><br/><br/>
<center>
<img src="/NPCommon/images/MainLogo_Netpay.gif" title="Netpay Logo" /><br /><br /><br />
<asp:MultiView runat="server" ID="mvSubscribeStatus">
    <asp:View ID="vwUnknownAction" runat="server">
        <b>UNKNOWN ACTION</b><br />
		FOR SUPPORT CONTACT NETPAY AT<br />
		<a href="mailto:SUPPORT@NETPAY-INTL.COM">SUPPORT@NETPAY-INTL.COM</a>
    </asp:View>
    <asp:View ID="vwAccountNotFound" runat="server">
        <b>ACCOUNT NOT FOUND</b><br /><br />
		FOR SUPPORT CONTACT NETPAY AT<br />
		<a href="mailto:SUPPORT@NETPAY-INTL.COM">SUPPORT@NETPAY-INTL.COM</a>
    </asp:View>
    <asp:View ID="vwUnsubscribeOK" runat="server">
		<b>SYSTEM FOUND YOUR ACCOUNT</b><br />
        <b>YOU HAVE BEEN UNSUBSCRIBED SUCCESSFULLY</b><br /><br />
		FOR SUPPORT CONTACT NETPAY AT<br />
		<a href="mailto:SUPPORT@NETPAY-INTL.COM">SUPPORT@NETPAY-INTL.COM</a>
    </asp:View>
    <asp:View ID="vwAlreadyUnsubscribed" runat="server">
		<b>SYSTEM FOUND YOUR ACCOUNT</b><br />
        <b>PLEASE NOTICE, YOU HAVE ALREADY BEEN UNSUBSCRIBED IN PAST</b><br /><br />
		FOR SUPPORT CONTACT NETPAY AT<br />
		<a href="mailto:SUPPORT@NETPAY-INTL.COM">SUPPORT@NETPAY-INTL.COM</a>
    </asp:View>
</asp:MultiView>
</center>
</body>
</html>
