<!--#include file="../include/const_globalData.asp"-->
<!--#include file="../include/func_adoConnect.asp"-->
<!--#include file="../include/func_transCharge.asp" -->
<!--#include file="../include/MerchantTerminals.asp" -->
<!--#include file="RemoteCharge_Functions.asp"-->
<script language="VBScript" runat="server" src="../include/func_encryption.asp"></script>
<%
    Dim X_CompanyNum, X_RefTransID, X_CompanyID, X_Amount, referringUrl, sendData, sWhere, bIsCHB, bIsCCRefund
    referringUrl = DBText(Request.ServerVariables("HTTP_REFERER"))
    bIsCHB = (TestNumVar(Request("TransCHB"), 0, 1, 0) = 1)
    If Request("OrderID") <> "" Then 'refund out
		bIsCCRefund = False
        sWhere = "tblCompanyTransPass.DebitCompanyID=20 And tblCompanyTransPass.DebitReferenceCode='" & Request("OrderID") & "'"
    Else							 'refund cc
		bIsCCRefund = True
        X_RefTransID = TestNumVar(Request("TransID"), 0, -1, 0)
        sWhere = "tblCompanyTransPass.ID=" & X_RefTransID
    End If

    X_Amount = TestNumVar(Request("Amount"), 0, -1, 0)
    X_Currency = TestNumVar(Request("Currency"), 0, 1, 0)
    X_Comment = Request("Comment")

    sSQL = "SELECT tblCompanyTransPass.ID, tblCompanyTransPass.TerminalNumber, tblCompanyTransPass.PaymentMethodDisplay, tblCompanyTransPass.IPAddress, tblCompanyTransPass.companyID, " &_
        " dbo.GetDecrypted256(tblCreditCard.CCard_number256) CCard_number, tblCreditCard.ExpMM, tblCreditCard.ExpYY, tblCreditCard.Member, tblCreditCard.cc_cui, tblCreditCard.phoneNumber, tblCreditCard.email, tblCreditCard.PersonalNumber, " &_
        " tblBillingAddress.address1, tblBillingAddress.address2, tblBillingAddress.city, tblBillingAddress.zipCode, tblBillingAddress.stateIso, tblBillingAddress.countryIso " &_
        " FROM tblCompanyTransPass " &_
        " LEFT JOIN tblCreditCard ON (tblCreditCard.ID = tblCompanyTransPass.CreditCardID) " &_
        " LEFT JOIN tblBillingAddress ON (tblBillingAddress.id = tblCreditCard.BillingAddressId) " & _
        " Where " & sWhere
    Set iRs = oleDbData.Execute(sSQL)
    if Not iRs.EOF Then
		X_CompanyID = iRs("CompanyID")
    	X_RefTransID = iRs("ID")
    	sClientIP = trim(iRs("IPAddress"))
		X_CompanyNum = ExecScalar("SELECT CustomerNumber FROM tblCompany WHERE ID=" & X_CompanyID, 0)
        sendData = "remote_charge.asp" & _
            "?CompanyNum=" & X_CompanyNum & _
            "&Amount=" & X_Amount & _
            "&Currency=" & X_Currency & _
            "&TypeCredit=0" & _
            "&CardNum=" & Replace(iRs("CCard_number"), " ", "") & _
            "&ExpMonth=" & Trim(iRs("ExpMM")) & _
            "&ExpYear=" & Trim(iRs("ExpYY")) & _
            "&CVV2=" & Server.URLEncode(DecCVV(iRs("cc_cui"))) & _
            "&Member=" & Server.URLEncode(iRs("Member")) & _
            "&PhoneNumber=" & Server.URLEncode(iRs("phoneNumber")) & _
            "&PersonalNum=" & Server.URLEncode(iRs("PersonalNumber")) & _
            "&Email=" & Server.URLEncode(iRs("email")) & _
            "&Comment=" & Server.URLEncode(X_Comment)
		
	    If NOT isNull(iRs("address1")) Then
            sendData = sendData & _
                "&BillingAddress1=" & Server.URLEncode(iRs("address1")) & _
                "&BillingAddress2=" & Server.URLEncode(iRs("address2")) & _
                "&BillingCity=" & Server.URLEncode(iRs("city")) & _
                "&BillingZipCode=" & Server.URLEncode(iRs("zipCode")) & _
                "&BillingState=" & Server.URLEncode(iRs("stateIso")) & _
                "&BillingCountry=" & Server.URLEncode(iRs("countryIso"))
	    End if

        sendData = sendData & _
            "&ReplyURL=" & _
            "&referringUrl=" & Server.URLEncode(referringUrl) & _
            "&ClientIP=" & Server.URLEncode(sClientIP) & _
            "&RefTransID=" & Server.URLEncode(X_RefTransID) & _
            "&isLocalTransaction=true" & _
            "&requestSource=6"
    End if
    iRs.Close
    If sendData = "" Then 
		Response.Write("Transaction not found:" & X_RefTransID) 
	Else	
   		If bIsCHB Then
			ChargeBackTrans X_Amount, sWhere
			If Not bIsCCRefund Then
                ExecSql("Insert Into EventPending(TransPass_id, EventPendingType_id, Parameters, TryCount)Values(" & nTransactionID & "," & PET_EmailChargeBack & ",'', 3)")
				'Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 
				'HttpReq.open "GET", PROCESS_URL & "member/denied_mail_send.aspx?TransID=" & X_RefTransID, false
				'HttpReq.send 
				'Set HttpReq = Nothing
			End If
			Response.Write("&Reply=000&TransID=" & X_RefTransID)
			Response.End()
		Else	
			Response.Redirect sendData
		End If
	End If

    'if IsTransID > 0 Then
    '	sSQL="INSERT INTO tblRefundAsk(CompanyID, transID, RefundAskAmount, RefundAskCurrency, RefundAskComment, RefundAskDate) " &_
	'	    "VALUES(" & X_CompanyID & "," & X_TransID & "," & X_Amount & "," & X_Currency &  ",'" & DBText(X_Comment) & "', getdate())"
	'	Response.Write("OK")
    'End if
%>