<%
'---------------------------------------------------------------------
'	Debit eTriton
'---------------------------------------------------------------------
Function debit_AltCharge_Back()

debit_AltCharge_Back = _
    "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd"">" & _
    "<head>" & _
    "   <meta http-equiv=""Content-type"" value=""text/html; charset=UTF-8"" />" & _
    "   <title>Signature</title>" & _
    "</head>" & _
    "<link rel=""stylesheet"" href=""https://altcharge.com/includes/jquery.signaturepad.css"">" & _
    "<!--[if lt IE 9]><script src=""https://altcharge.com/includes/flashcanvas.js""></script><![endif]-->" & _
    "<script type=""text/javascript"" src=""https://altcharge.com/includes/jquery-1.5.min.js""></script>" & _
    "<body>" & _
    "   <p>Preliminary testing of signature pad for online merchants to capture signatures from customers</p>" & _
    "   <form method=""post""> <!-- You'll put the below inside your form you already have -->" & _
    "   <div class=""sigPad""> <!-- overriding stylesheet for the signature pad -->" & _
    "   <p class=""sigNav"">Please sign below</p>" & _
    "   <ul class=""sigNav""><li class=""clearButton""><a href=""#clear"">Clear</a></li></ul>" & _
    "   <div class=""sig sigWrapper"">" & _
    "   <div class=""typed""></div>" & _
    "   <canvas class=""pad"" width=""400"" height=""80""></canvas>" & _
    "       <input type=""hidden"" name=""signature"" class=""output"">" & _
    "   </div>" & _
    "<button type=""submit"">I accept the terms of this agreement</button> <!-- submit button -->" & _
    "   </div> <!-- ending DIV for stylesheet override -->" & _
    "</form>" & _
    "<!-- script includes below must be after the form -->" & _
    "<script src=""https://altcharge.com/includes/jquery.signaturepad.js""></script>" & _
    "<script>" & _
    "$(document).ready(function() { $('.sigPad').signaturePad({drawOnly:true}); });" & _
    "</script>" & _
    "<script src=""https://altcharge.com/includes/json2.min.js""></script>" & _
    "</body>"
End Function

function debit_AltCharge()
	If Trim(X_Currency) <> 1 Then ThrowError "531"
	Dim HttpReq, UrlAddress, ParamList, fName, sName, mSDep, sApprovalNumber, strAct, sDebitReturnAnswer

	xNames = Split(Trim(X_ccHolderName), " ")
	if Ubound(xNames) < 1 Then Call throwError("517")
	fName = xNames(0) : sName = xNames(1)
	
	sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
	if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
	Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0")'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 15*1000, 20*1000, 120*1000
	UrlAddress = "https://altcharge.com/gateway/api.php"
	if (Cint(X_TransType) = 2) Or (X_TypeCredit = "0") Then
		if X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans not Found
	End if
	X_DebitReferenceCode = GetTransRefCode()
	
	'sIP
	ParamList = "userkey=" & sAccountID & _
	"&type=SINGLE" & _
	"&version=2.6" & _
	"&email=" & X_Email & _
	"&firstName=" & Server.URLEncode(xNames(0))  & _
	"&lastName=" & Server.URLEncode(xNames(1)) & _
	"&address1=" & Server.URLEncode(BACHAddr1) & _
	"&address2=" & Server.URLEncode(BACHAddr2) & _
	"&city=" & Server.URLEncode(BACity) & _
	"&state=" & Server.URLEncode(BAState)  & _
	"&zip=" & Server.URLEncode(BAZipCode) & _
	"&country=" & Server.URLEncode(BACountryCode) & _
	"&phone=" & Server.URLEncode(X_PhoneNumber) & _
	"&amount=" & Server.URLEncode(Abs(X_Amount)) & _
	"&ipaddress=" & Server.URLEncode("80.179.39.11") & _ 
	"&accountNumber=" & Server.URLEncode(ckacct) & _
	"&routingNumber=" & Server.URLEncode(ckaba) & _
	"&merchantMID=1" & _
	"&misc1=" & Server.URLEncode(X_DebitReferenceCode) & _
	"&misc2=" & Server.URLEncode(X_OrderNumber) & _
	"&Currency=" & Server.URLEncode(GetCurISOName(Cint(X_Currency))) & _
	"&signature=AAA"

	Dim sReturn : sReturn = ""
	On Error Resume Next
		HttpReq.Open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded;"
		HttpReq.Send ParamList
		sReturn = HttpReq.responseText
		'sReturn = Replace(Replace(sReturn, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba))
		SaveLogChargeAttemptRequestResponse Replace(Replace(ParamList, ckacct, GetSafePartialNumber(ckacct)), ckaba, GetSafePartialNumber(ckaba)), sReturn
		HttpError = FormatHttpRequestError(HttpReq)
	On Error GoTo 0
	If Len(sReturn) = 0 Then ThrowError "521"
	
	Response.Write(ParamList & "<br/>" & sReturn) : Response.End

	sDebitReturnAnswer = Left(sReturn, InStr(sReturn, "PostedVars=BEGIN") - 1)
	sStatus = UCase(GetURLValue(sReturn, "status"))
	If sStatus = "ACCEPTED" Then
		sReturnCode = "001"
	Else
		sReturnReason = DBText(GetURLValue(sReturn, "reason"))
		If sReturnReason = "" Then
			sReturnCode = "002"
		Else
			sReturnCode = ExecScalar("SELECT TOP 1 Code FROM tblDebitCompanyCode WHERE DebitCompanyID=36 AND DescriptionOriginal='" & sReturnReason & "'", "")
			If sReturnCode = "" Then
				sReturnCode = ExecScalar("SELECT Count(*) FROM tblDebitCompanyCode WHERE DebitCompanyID=36", 0) + 1
				sSQL = "INSERT INTO tblDebitCompanyCode(DebitCompanyID, FailSource, ChargeFail, Code, DescriptionOriginal," & _
				" DescriptionMerchantEng, DescriptionMerchantHeb, DescriptionCustomerEng, DescriptionCustomerHeb) VALUES (36, 0, 1," & _
				" '" & sReturnCode & "'," & _
				" '" & sReturnReason & "', '" & sReturnReason & "', '" & sReturnReason & "', '" & sReturnReason & "', '" & sReturnReason & "')"
				oledbData.Execute sSQL
			End If
		End If
	End If
	sApprovalNumber = GetURLValue(sReturn, "history_id")

	DebitIProcessResult sReturnCode, sApprovalNumber
	Set xmlRet = Nothing
	Set HttpReq = Nothing
End function
%>