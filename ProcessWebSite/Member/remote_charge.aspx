<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" %>
<script runat="server" language="vbscript">
	Private Function TransReq(rqVal As String) as String
		Dim strBytes() As Byte = Encoding.Unicode.GetBytes(rqVal)
		strBytes = Encoding.Convert(Encoding.Unicode, Encoding.GetEncoding("windows-1255"), strBytes)
		Dim asciiChars(Encoding.Default.GetCharCount(strBytes, 0, strBytes.Length)) As Char
		Encoding.Default.GetChars(strBytes, 0, strBytes.Length, asciiChars, 0)
		return New String(asciiChars)
	End Function

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim sData As String = Request.QueryString.ToString()
		If String.IsNullOrEmpty(sData) Then sData = Request.Form.ToString()
		Dim StrSend As String = TransReq(sData)
		Response.Redirect("remote_charge.asp?" & StrSend)
	End Sub
</script>