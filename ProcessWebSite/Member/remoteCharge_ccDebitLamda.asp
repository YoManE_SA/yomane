<%
'---------------------------------------------------------------------
'	Debit Lamda ()
'---------------------------------------------------------------------
function debitLamda()
On Error Goto 0
   Dim HttpReq, UrlAddress, ParamList, fName, sName, encodedPassword, sApprovalNumber, sResDetails
   xNames = Split(X_ccHolderName, " ")
   if Ubound(xNames) > -1 Then fName = xNames(0)
   if Ubound(xNames) > 0 Then sName = xNames(1)
   sCompanyDescriptor = sTermName & " " & sCompanyDescriptor
   if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
   UrlAddress = IIF(isTestTerminal, "https://test.lamdaprocessing.com/securePayments/direct/v1/processor.php", "https://www.lamdaprocessing.com/securePayments/direct/v1/processor.php")

    sAccountPassword : LCase(Hash("SHA256", sAccountPassword).Hex)

	Dim sendCardType, sOuterIP, sendData
	sOuterIP = IIF(session("Identity") = "Local", "http://80.179.39.10:8080/Default.aspx", Session("ProcessURL") & "remoteCharge_ccDebitLamdaNotify.asp")
	X_DebitReferenceCode = GetTransRefCode()
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If Cint(X_TransType) = 2 Then throwError("503")
		If X_Amount <> RefOAmount Then throwError("539")
		ParamList = "<?xml version=""1.0"" encoding=""UTF-8"" ?>" & _
		"<RefundRequest xmlns=""https://test.lamdaprocessing.com/api/direct/v1/processor"">" & _
		" <Language>ENG</Language>" & _
		" <Credentials>" & _
		"  <MerchantId>" & sAccountId & "</MerchantId>" & _
		"  <TerminalId>" & sAccountSubId & "</TerminalId>" & _
		"  <TerminalPassword>" & encodedPassword & "</TerminalPassword>" & _
		" </Credentials>" & _
		" <TransactionType>LP005</TransactionType>" & _
		" <TransactionId>" & X_DebitReferenceCode & "</TransactionId>" & _
		" <ReturnUrl page=""" & sOuterIP & """></ReturnUrl>" & _
		" <ConfirmationNumber>" & X_ConfirmNumber & "</ConfirmationNumber>" & _
		" <RefundDescription>no description</RefundDescription>" & _
		" <CurrencyCode>" & GetCurISOCode(Cint(X_Currency)) & "</CurrencyCode>" & _
		" <TotalAmount>" & CLng(X_Amount * 100) & "</TotalAmount>" & _
		" <CustomerIP>" & sIP & "</CustomerIP>" & _
		"</RefundRequest>"
	Else
		If Cint(X_TransType) <> 0 Then throwError("503")
		Select case paymentMethod
			Case PMD_CC_VISA: sendCardType = "VI"
			case PMD_CC_MASTERCARD: sendCardType = "MC"
			Case Else: throwError("503")
		End Select
		If X_PayFor = "" Then X_PayFor = "Purchase"
		ParamList = "<?xml version=""1.0"" encoding=""UTF-8"" ?>" & _
		"<TransactionRequest xmlns=""https://test.lamdaprocessing.com/api/direct/v1/processor"">" & _
		" <Language>ENG</Language>" & _
		" <Credentials>" & _
		"  <MerchantId>" & sAccountId & "</MerchantId>" & _
		"  <TerminalId>" & sAccountSubId & "</TerminalId>" & _
		"  <TerminalPassword>" & encodedPassword & "</TerminalPassword>" & _
		" </Credentials>" & _
		" <TransactionType>LP001</TransactionType>" & _
		" <TransactionId>" & X_DebitReferenceCode & "</TransactionId>" & _
		" <ReturnUrl page=""" & sOuterIP & """></ReturnUrl>" & _
		" <CurrencyCode>" & GetCurISOName(Cint(X_Currency)) & "</CurrencyCode>" & _
		" <TotalAmount>" & CLng(X_Amount * 100) & "</TotalAmount>" & _
		" <ProductDescription>" & X_PayFor & "</ProductDescription>" & _
		" <CustomerDetails>" & _
		"  <FirstName>" & fName & "</FirstName>" & _
		"  <LastName>" & sName & "</LastName>" & _
		"  <CustomerIP>" & sIP & "</CustomerIP>" & _
		"  <Phone>" & X_PhoneNumber & "</Phone>" & _
		"  <Email>" & X_Email & "</Email>" & _
		" </CustomerDetails>" & _
		" <BillingDetails>" & _
		"  <CardPayMethod>2</CardPayMethod>" & _
		"  <FirstName>" & fName & "</FirstName>" & _
		"  <LastName>" & sName & "</LastName>" & _
		"  <Street>" & BACHAddr1 & "</Street>" & _
		"  <City>" & BACity & "</City>" & _
		"  <Region>" & BAStateCode & "</Region>" & _
		"  <Country>" & BACountryCode &"</Country>" & _
		"  <Zip>" & BAZipCode & "</Zip>" & _
		" </BillingDetails>" & _
		" <CardDetails>" & _
		"  <CardHolderName>" & X_ccHolderName & "</CardHolderName>" & _
		"  <CardNumber>" & X_ccNumber & "</CardNumber>" & _
		"  <CardExpireMonth>" & X_ccExpMM & "</CardExpireMonth>" & _
		"  <CardExpireYear>" & X_ccExpYY & "</CardExpireYear>" & _
		"  <CardType>" & sendCardType & "</CardType>" & _
		"  <CardSecurityCode>" & X_ccCVV2 & "</CardSecurityCode>" & _
		"  <CardIssuingBank>UNKNOWN</CardIssuingBank>" & _
		"  <CardIssueNumber></CardIssueNumber>" & _
		" </CardDetails>" & _
		"</TransactionRequest>"
	End If
	Dim EncodedMessage, EncodedSignature, Hmac_SHA
	Set EncodedMessage = CreateTextBlob(Empty) : EncodedMessage.ASCII = ParamList
	Set EncodedSignature = CreateTextBlob(Empty) : EncodedSignature.ASCII = sAccountAuthCode1 & sAccountPassword & X_DebitReferenceCode
	Hmac_SHA = HMAC(HMACSHA256, EncodedSignature, EncodedMessage).Base64
	sendData = "version=1.0&encodedMessage=" & Server.UrlEncode(Replace(EncodedMessage.Base64, vbCrLf, "")) & "&signature=" & Server.UrlEncode(Replace(Hmac_SHA, vbCrLf, "")) & ""
	Set EncodedSignature = Nothing : Set Hmac_SHA = Nothing
	'Response.Write(UrlAddress & vbCrLf & ParamList & "<br/>" & vbCrLf & sendData & vbCrLf & "<br/>X." & vbCrLf)
	'Response.End()
    On Error Resume Next
		Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
		HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Accept", "text/html, application/xhtml+xml, */*"
		HttpReq.setRequestHeader "User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko"
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		Call HttpReq.send(sendData)
		sResDetails = Replace(HttpReq.responseText, "<form", "<formX")
		'If IsNull(sResDetails) Or Len(sResDetails) = 0 Then Call throwError("521")
		'parse response
		Dim retToekn 
		If InStr(1, sResDetails, "<body") > 0 Then
			retToekn = "name=""encodedMessage"" value=""" : 
			Dim retStart : retStart = InStr(1, sResDetails, retToekn) + Len(retToekn)
			retToekn = Mid(sResDetails, retStart, InStr(retStart, sResDetails, """") - retStart)
		Else
			retToekn = URLDecode(GetUrlValue(sResDetails, "encodedMessage"))
		End If
		EncodedMessage.Base64 = retToekn : retToekn = EncodedMessage.ASCII

		SaveLogChargeAttemptRequestResponse ParamList, retToekn
	    HttpError = FormatHttpRequestError(HttpReq)

	    Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
		xmlRet.LoadXML(retToekn)
		
		Dim paymentStatus : paymentStatus = xmlRet.selectSingleNode("//PaymentStatus").text
		sReturnCode = xmlRet.selectSingleNode("//Code").text
		sApprovalNumber = xmlRet.selectSingleNode("//ConfirmationNumber").text
		If paymentStatus = "" Then Call throwError("520") 'internal error
		paymentStatus = UCase(paymentStatus)
    On Error GoTo 0
	If paymentStatus = "APPROVED" Then
		sReturnCode = "000"
	ElseIf paymentStatus = "PENDING" Then
		sReturnCode = "001"
	Else
		If sReturnCode = "000" Or sReturnCode = "001" Then sReturnCode = "002"
		X_CustomErrorDesc = xmlRet.selectSingleNode("//Description").text
	End If
    Set HttpReq = Nothing
   	Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
end function
%>
