<%
'---------------------------------------------------------------------
'	Debit PBS
'---------------------------------------------------------------------
Function PBS_Make2DigDate(xNum)
	If Len(xNum) = 1 Then xNum = "0" & xNum
	PBS_Make2DigDate = xNum
End Function

Function remoteCharge_cc_debit_PBS_BackDetect()
    Response.Write(xRetParams)
End Function

Function debit_PBS()
	Dim HttpReq, UrlAddress, ParamList, fName, sName, sApprovalNumber, sDebitReturnAnswer, signData, sPBSCode
	If int(X_TypeCredit) = 8 Or int(X_Payments) > 1 Then Call throwError("508") 
    Call SplitFullName(RemoveHtmlEntities(X_ccHolderName), fName, sName)
	If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
		If X_ConfirmNumber = "" Then Call throwError("532") 'Reftrans not Found
		ParamList = ParamList & _ 
			"merNo=" & Server.URLEncode(sAccountId) & _
			"&gatewayNo=" & Server.URLEncode(sAccountSubId) & _
			"&orderNo=" & Server.URLEncode(X_DebitReferenceCode) & _
			"&tradeNo=" & Server.URLEncode(X_ConfirmNumber)
        If (Cint(X_TypeCredit) = 0) Then
            Dim pbsRefundType : pbsRefundType = IIF(RefOAmount = X_Amount, "1", "2")
            UrlAddress = IIF(X_Is3dSecure, "https://check.centurybill.com/servlet/ApplyRefund", "https://check.onlinesslmall.com/servlet/ApplyRefund")
            ParamList = ParamList & _ 
    		    "&refundType=" & pbsRefundType & _
			    "&tradeAmount=" & formatNumber(RefOAmount, 2, true, false, false) & _
			    "&refundAmount=" & formatNumber(X_Amount, 2, true, false, false) & _
     		    "&currency=" & Server.URLEncode(GetCurISOName(Cint(X_Currency))) & _
                "&refundReason=" & Server.URLEncode("Merchant Request")
            signData = sAccountId & sAccountSubId & X_ConfirmNumber & pbsRefundType & sAccountPassword
        Else
            UrlAddress = "https://pay.onlinesslconnect.com/AuthorizeInterface"
            ParamList = ParamList & "&authType=" & "2"
            signData = sAccountId & sAccountSubId & X_DebitReferenceNum & "2" & sAccountPassword
        End If
	Else
		If Len(sCompanyDescriptor) > 24 Then sCompanyDescriptor = Left(sCompanyDescriptor, 24)
        If isTestTerminal Then 
            If dt_Enable3dsecure Then UrlAddress = "https://co.centurybill.com/DirectInterface" _
            Else UrlAddress = "https://co.onlineb2cmall.com/TestTPInterface"
        Else 
    	    If dt_Enable3dsecure Then UrlAddress = "https://co.centurybill.com/DirectInterface" _
            Else UrlAddress = "https://co.onlineb2cmall.com/TPInterface"
        End If
        X_DebitReferenceCode = GetTransRefCode()
		ParamList = _ 
			"merNo=" & Server.URLEncode(sAccountId) & _
			"&gatewayNo=" & Server.URLEncode(sAccountSubId) & _
			"&orderNo=" & Server.URLEncode(X_DebitReferenceCode) & _
			"&orderCurrency=" & Server.URLEncode(GetCurISOName(Cint(X_Currency))) & _
			"&orderAmount=" & Server.URLEncode(formatNumber(X_Amount, 2, true, false, false)) & _
            "&paymentMethod=" & Server.URLEncode("Credit Card") & _
			"&cardNo=" & Server.URLEncode(Replace(X_ccNumber, " ", "")) & _
			"&cardExpireMonth=" & PBS_Make2DigDate(X_ccExpMM) & _
			"&cardExpireYear=20" & PBS_Make2DigDate(X_ccExpYY) & _
			"&cardSecurityCode=" & Server.URLEncode(X_ccCVV2) & _
            "&csid=" & _
            "&issuingBank=Bank" & _ 
			"&firstName=" & Server.URLEncode(fName) & _
			"&lastName=" & Server.URLEncode(sName) & _
			"&email=" & Server.URLEncode(X_Email) & _
            "&ip=" & sIP & _    
			"&phone=" & Server.URLEncode(Replace(Replace(X_PhoneNumber, " ", ""), "-", "")) & _
			"&country=" & Server.URLEncode(BACountryCode) & _
			"&state=" & Server.URLEncode(BAStateCode) & _
			"&city=" & Server.URLEncode(BACity) & _
			"&address=" & Server.URLEncode(BACHAddr1) & _
			"&zip=" & Server.URLEncode(BAZipCode) & _
            "&returnUrl=" & Server.URLEncode(Session("ProcessURL") & "remoteCharge_ccDebitPBSBack.asp") 'no need see email php@paymentbillsservice.com @November 15, 2016 3:54 AM
            signData = sAccountId & sAccountSubId & X_DebitReferenceCode & _
                GetCurISOName(Cint(X_Currency)) & FormatNumber(X_Amount, 2, true, false, false) & _
                fName & sName & Replace(X_ccNumber, " ", "") & _ 
                "20" & PBS_Make2DigDate(X_ccExpYY) & PBS_Make2DigDate(X_ccExpMM) & _
                X_ccCVV2 & X_Email & sAccountPassword
	End if
    Dim signObj : Set signObj = Hash("SHA256", signData)
	ParamList = ParamList & "&signInfo=" & signObj.Hex
    Set signObj = Nothing

    Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
	HttpReq.setTimeouts 10*1000, 20*1000, 20*1000, 120*1000
	On Error Resume Next
		'Response.Write(UrlAddress & "<br/><br/>" & ParamList & vbCrLf & "<br/><br/>" & vbCrLf)
		HttpReq.open "POST", UrlAddress
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		HttpReq.send ParamList
		sResDetails = HttpReq.responseText
		HttpError = FormatHttpRequestError(HttpReq)
		TimeControl.Append("A_SEND") 'Time Optimization *********
		'Response.Write(sResDetails)

		If Len(sResDetails) = 0 Then Call throwError("521")
		sDebitReturnAnswer = Left(sResDetails, 3900)
	
		Dim sDebitRequest : sDebitRequest = ParamList
		sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
		sDebitRequest = Replace(sDebitRequest, "cardSecurityCode=" & X_ccCVV2, "cardSecurityCode=" & GetSafePartialNumber(X_ccCVV2))
		SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails
		'Response.Write(sReturnCode & " " & X_DebitReferenceNum & " " & sApprovalNumber & " " & X_CustomErrorDesc)
		'Response.End()
	On Error Goto 0
	If (Len(sResDetails) < 1) Then Call throwError("520")
	
    Dim isForm : isForm = InStr(1, sResDetails, "<form ") > 0
	If isForm Then 
        sReturnCode = "553"
        X_Is3dSecure = True
        X_3dRedirectMethod = "SameWindow" 
	    X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
    	sIsSendUserConfirmationEmail = False
    Else 
		Dim xmlRet : Set xmlRet = Server.CreateObject("Microsoft.XMLDOM")
		xmlRet.LoadXML HttpReq.responseText
        If (Len(sResDetails) < 1) Then Call throwError("520")
        On Error Resume Next
    	    If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
	            sReturnCode = xmlRet.selectSingleNode("//code").text
                sApprovalNumber = xmlRet.selectSingleNode("//tradeNo").text
                X_CustomErrorDesc = xmlRet.selectSingleNode("//description").text
                If sReturnCode = "" Then Call throwError("520") 'internal error
                If sReturnCode <> "00" Then sReturnCode = sReturnCode & "-RF" Else sReturnCode = "000"
            Else
	            sReturnCode = xmlRet.selectSingleNode("//orderStatus").text
                sApprovalNumber = xmlRet.selectSingleNode("//tradeNo").text
                X_CustomErrorDesc = xmlRet.selectSingleNode("//orderInfo").text
                If sReturnCode = "" Then Call throwError("520") 'internal error
                Select Case sReturnCode
                    Case "-1": sReturnCode = "001"
                    Case "0": sReturnCode = "002"
                    Case "1": sReturnCode = "000"
                    Case Else: sReturnCode = "003"
                End Select
            End If
        On Error Goto 0
        Set xmlRet = Nothing
    End If
	Set HttpReq = Nothing
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)
End Function
%>