<%
'---------------------------------------------------------------------
'	Debit PayOn ctpe ()
'---------------------------------------------------------------------
Function remoteCharge_debitPayOnCTPE_BackDetect()
    Dim sendParams, xmlRet
    Set xmlRet = Server.CreateObject("Msxml2.DOMDocument.3.0")
	xRetParams = Replace(Replace(Replace(xRetParams, "&gt;", ">"), "&lt;", "<"), "``", """")
    xmlRet.loadXML(xRetParams)
    X_3dRedirect = xmlRet.selectSingleNode("//Redirect").attributes.getNamedItem("url").text
	Response.Write("<html><body onload=""document.getElementById('frmMain').submit()""><form id=""frmMain"" method=""post"" action=""" & X_3dRedirect & """>")
    Set sendParams = xmlRet.selectNodes("//Redirect/Parameter")
    For Each p in sendParams
		Response.Write(" <input type=""hidden"" name=""" & p.getAttribute("name") & """ value=""" & p.Text & """/>")
	Next	
	Response.Write("</form></body></html>")
    Set xmlRet = Nothing
    Response.End
End Function

function debitPayOnCTPE()
    Dim HttpReq, UrlAddress, ParamList, fName, sName, EncodedSignature, sApprovalNumber, strAct, BrandName, sResDetails
    BrandName = Array("--", "--", "VISA", "DINERS", "AMEX", "MASTER")
    xNames = Split(X_ccHolderName, " ")
    if Ubound(xNames) > -1 Then fName = xNames(0)
    if Ubound(xNames) > 0 Then sName = xNames(1)
    sCompanyDescriptor = Trim(Trim(sTermName) & " " & Trim(sCompanyDescriptor))
    if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
    If isTestTerminal Then UrlAddress = "https://test.ctpe.io/payment/ctpe" _
    Else UrlAddress = "https://ctpe.io/payment/ctpe" 
    Set HttpReq = Server.CreateObject("Msxml2.ServerXMLHTTP.3.0") 'Microsoft.XMLHTTP
    SendMode = IIF(isTestTerminal, "CONNECTOR_TEST", "LIVE") 'TEST

	ParamList = "<?xml version=""1.0"" encoding=""utf-8""?>" & VbCrLf & _
	    "<Request version=""1.0"">" & VbCrLf & _
	    " <Header>" & VbCrLf & _
	    "  <Security sender=""" & sAccountSubId3D & """ />" & VbCrLf & _
        "  <ShopperID>admin</ShopperID>" & VbCrLf & _
	    " </Header>" & VbCrLf
     If (Cint(X_TransType) = 2) Or (Cint(X_TypeCredit) = 0) Then
 	     If X_DebitReferenceCode = "" Then Call throwError("532") 'Reftrans Not Found
         If X_RefTransInsertDate < DateAdd("h", 13, DateSerial(2015, 5, 3)) Then
            debitPayOn()
            Exit Function
         End If
		 X_DebitReferenceCode = GetTransRefCode()
	     PCode = IIF((Cint(X_TypeCredit) = 0), "RF", "CP") 'response=""SYNC"" 
	     ParamList = ParamList & _
			" <Transaction mode=""" & SendMode & """ channel=""" & sAccountAuthCode13D & """>" & VbCrLf & _
			"  <User login=""" & sAccountId3D & """ pwd=""" & sAccountPassword3D & """ />" & VbCrLf & _
			"  <Identification>" & VbCrLf & _
			"	<TransactionID>" & X_DebitReferenceCode & "</TransactionID>" & _
			"	<ReferenceID>" & X_DebitReferenceNum & "</ReferenceID>" & _
			"  </Identification>" & VbCrLf & _
			"  <Payment code=""CC." & PCode & """>" & VbCrLf & _
			"   <Presentation>" & _
			"    <Amount>" & X_Amount & "</Amount>" & _
			"    <Currency>" & GetCurISOName(Cint(X_Currency)) & "</Currency>" & _
			"    <Usage>" & sCompanyDescriptor & "</Usage>" & _
			"   </Presentation>" & _
			"  </Payment>" & _
			" </Transaction>" & VbCrLf & _
			"</Request>"
			
		'Response.Write(sTerminalChargeOptions & "<br>")
		'Response.Write(ParamList)
		'Response.End	
	 Else
         backPublicAddress = IIF(isTestTerminal, "http://localhost:83/processWebSite/member/", Session("ProcessURL"))
		 X_DebitReferenceCode = GetTransRefCode()
	     PCode = IIF(Cint(X_TransType) = 1, "PA", "DB") 'response=""SYNC"" 
		 ParamList = ParamList & " <Transaction mode=""" & SendMode & """ channel=""" & sAccountAuthCode13D & """>" & VbCrLf & _
			"  <User login=""" & sAccountId3D & """ pwd=""" & sAccountPassword3D & """ />" & VbCrLf & _
			"  <Identification>" & VbCrLf & _
			"   <TransactionID>" & X_DebitReferenceCode & "</TransactionID>" & VbCrLf & _
			"  </Identification>" & VbCrLf & _
			"  <Payment code=""CC." & PCode & """>" & VbCrLf & _
			"   <Presentation>" & VbCrLf & _
			"    <Amount>" & X_Amount & "</Amount>" & VbCrLf & _
			"    <Currency>" & GetCurISOName(Cint(X_Currency)) & "</Currency>" & VbCrLf & _
			"    <Usage>" & sCompanyDescriptor & "</Usage>" & VbCrLf & _
			"   </Presentation>" & VbCrLf & _
			"  </Payment>" & VbCrLf & _
			"  <Account>" & VbCrLf & _
			"   <Holder><![CDATA[" & X_ccHolderName & "]]></Holder>" & VbCrLf & _
			"   <Number>" & X_ccNumber & "</Number>" & VbCrLf & _
			"   <Brand>" & BrandName(ccTypeID) & "</Brand>" & VbCrLf & _
			"   <Expiry month=""" & X_ccExpMM & """ year=""20" & X_ccExpYY & """ />" & VbCrLf & _
			"   <Verification>" & X_ccCVV2 & "</Verification>" & VbCrLf & _
			"  </Account>" & VbCrLf & _
			"  <Customer>" & VbCrLf & _
			"   <Name>" & VbCrLf & _
			"    <Given><![CDATA[" & sName & "]]></Given>" & VbCrLf & _
			"    <Family><![CDATA[" & fName & "]]></Family>" & VbCrLf & _
			"   </Name>" & VbCrLf & _
			"   <Address>" & VbCrLf & _
			"    <Street><![CDATA[" & BACHAddr1 & "]]></Street>" & VbCrLf & _
			"    <Zip>" & BAZipCode & "</Zip>" & VbCrLf & _
			"    <City>" & BACity & "</City>" & VbCrLf & _
			"    <State>" & BAStateCode & "</State>" & VbCrLf & _
			"    <Country>" & BACountryCode & "</Country>" & VbCrLf & _
			"   </Address>" & VbCrLf & _
			"   <Contact>" & VbCrLf & _
			"    <Email>" & X_Email & "</Email>" & VbCrLf & _
			"    <Ip>" & sIP & "</Ip>" & VbCrLf & _
			"   </Contact>" & VbCrLf & _
			"  </Customer>" & VbCrLf
            If sIsAllow3DTrans And dt_Enable3dsecure Then
		        ParamList = ParamList & _
                "  <Frontend>" & VbCrLf & _
                "   <ResponseUrl>" & backPublicAddress & "remoteCharge_ccDebitPayOnCTPEBack.asp" & "</ResponseUrl>" & VbCrLf & _
                "   <SessionID>" & X_DebitReferenceCode & "</SessionID>" & VbCrLf & _
                "  </Frontend>" & VbCrLf
            End If
		    ParamList = ParamList & _
			" </Transaction>" & VbCrLf & _
			"</Request>"
	End if		
    On Error Resume Next
		HttpReq.open "POST", UrlAddress, false
		HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		'HttpReq.setRequestHeader "Accept-Language", "en-US"
		HttpReq.send "load=" & ParamList
		sResDetails = HttpReq.responseText
	    HttpError = FormatHttpRequestError(HttpReq)
		SaveLogChargeAttemptRequestResponse ParamList, sResDetails

		Set xmlRet = HttpReq.responseXml
	    If IsNull(sResDetails) Or Len(sResDetails) = 0 Then Call throwError("521")
	    sReturnCode = xmlRet.selectSingleNode("//Return").attributes.getNamedItem("code").text
        X_AcquirerReferenceNum = xmlRet.selectSingleNode("//Result[@name='ConnectorTxID3']").text
        X_3dRedirect = xmlRet.selectSingleNode("//Redirect").attributes.getNamedItem("url").text
        If sReturnCode = "" Then Call throwError("520") 'internal error
    On Error GoTo 0
	If sReturnCode = "000.000.000" Or sReturnCode = "000.100.110" Or sReturnCode = "000.100.111" Then
		sReturnCode = "000"
		sApprovalNumber = xmlRet.selectSingleNode("//ShortID").text
        X_DebitReferenceNum = xmlRet.selectSingleNode("//UniqueID").text 
	ElseIf sReturnCode = "000.200.000" Then
		sReturnCode = "001"
        X_DebitReferenceNum = xmlRet.selectSingleNode("//UniqueID").text 
        If X_3dRedirect <> "" Then
            X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
            sIsSendUserConfirmationEmail = False
            X_Is3dSecure = True
            sReturnCode = "553"
        End If
	Else
	    sError = xmlRet.selectSingleNode("//Return").text
	End If
    'If (Cint(X_TypeCredit) = 0) And (sReturnCode <> "000") Then FileAppendData "PayOnCTPE.txt", Now & ", " & sReturnCode & vbcrlf & ParamList & vbcrlf & sResDetails & vbcrlf & vbcrlf & vbcrlf
    'Response.Write(ParamList & VbCrLf & VbCrLf & xmlRet.Xml & VbCrLf & VbCrLf & sReturnCode)
    'Response.End
    Set HttpReq = Nothing
    'Response.Write("HEREE " & sReturnCode & X_3dRedirect )
	Call DebitIProcessResult(sReturnCode, sApprovalNumber)	
end function
%>