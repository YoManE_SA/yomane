﻿<%
'----------------------------------------------------------------------------
'	Const
'----------------------------------------------------------------------------
Const HOTDB_ENABLED=True
Const HOTDB_INTERVAL_MINUTES=60
Const HOTDB_DURATION_SECONDS=480
Const HOTDB_PREVENTION_SECONDS=120

Const FORMS_INBOX="http://192.168.5.11/member/forms_inbox.aspx"
Const ToggleCardDomain = "http://192.168.5.7"
Const SMTP_SERVER = "192.168.0.5"
Const SMTP_USERNAME = "" '"v@servnet.co.il"
Const SMTP_PASSWORD = "" '"smtpemailout"
Dim IS_PRODUCTION, DOMAIN_TEXT_1, COMPANY_NAME_1, COMPANY_NAME_2, COMPANY_IDENTITY, STYLE_NAME, MERCHANT_URL, PROCESS_URL, _
	DEVCENTER_URL, USER_URL, FAVICON_URL, MAIL_INFO, MAIL_SALES, MAIL_SUPPORT, MAIL_DOMAIN, SHOW_HEBREW, FORCESSL, WEBSERVICES_URL

IS_PRODUCTION = True

Select case Trim(session("Identity"))
	Case "YomaneNET"
		DOMAIN_TEXT_1 = "yomane.net"
		COMPANY_NAME_1 = "yomane"
		COMPANY_NAME_2 = "נטפיי"
		COMPANY_IDENTITY = "NP"
		STYLE_NAME = "StyleNetpay.css"
		MERCHANT_URL = "http://merchants.yomane.net/"
		PROCESS_URL = "http://process.yomane.net/"
		DEVCENTER_URL = "http://devcenter.yomane.net/"
		USER_URL = "http://users.yomane.net/"
		FAVICON_URL = "http://www.netpay-intl.com/favicon.ico"
		WEBSERVICES_URL = "http://webservices.yomane.net/"
		MAIL_INFO = "support@yomane.com"
		MAIL_SALES = "support@yomane.com"
		MAIL_SUPPORT = "support@yomane.com"
		MAIL_DOMAIN = "yomane.net"
		SHOW_HEBREW = True
        FORCESSL = InStr(1, "test.", LCase(Trim(Request.ServerVariables("HTTP_HOST")))) > 0
	Case "Netpay"
		DOMAIN_TEXT_1 = "Netpay.co.il"
		COMPANY_NAME_1 = "Netpay"
		COMPANY_NAME_2 = "נטפיי"
		COMPANY_IDENTITY = "NP"
		STYLE_NAME = "StyleNetpay.css"
		MERCHANT_URL = "https://merchants.intercash.net/"
		PROCESS_URL = "https://process.intercash.net/"
		DEVCENTER_URL = "https://devcenter.intercash.net/"
		USER_URL = "https://users.intercash.net/"
		FAVICON_URL = "http://www.netpay.co.il/favicon.ico"
		WEBSERVICES_URL = "https://webservices.netpay-intl.com/"
		MAIL_INFO = "lavi@netpay-intl.com"
		MAIL_SALES = "lavi@netpay-intl.com"
		MAIL_SUPPORT = "lavi@netpay-intl.com"
		MAIL_DOMAIN = "netpay-intl.com"
		SHOW_HEBREW = True
        FORCESSL = True
	Case "Paybytech"
		DOMAIN_TEXT_1 = "Paybytech.com"
		COMPANY_NAME_1 = "Paybytech"
		COMPANY_NAME_2 = "Paybytech"
		COMPANY_IDENTITY = "PB"
		STYLE_NAME = "StyleAstech.css"
		MERCHANT_URL = "https://merchants.paybytech.com/"
		PROCESS_URL = "https://process.paybytech.com/"
		DEVCENTER_URL = "https://devcenter.paybytech.com/"
		USER_URL = "https://wallet.paybytech.com/"
		WEBSERVICES_URL = "https://webservices.paybytech.com/"
		FAVICON_URL = ""
		MAIL_INFO = "sales@paybytech.com"
		MAIL_SALES = "sales@paybytech.com"
		MAIL_SUPPORT = "sales@paybytech.com"
		MAIL_DOMAIN = "paybytech.com"
		SHOW_HEBREW = False
        FORCESSL = True
	Case "Mahala"
		DOMAIN_TEXT_1 = "Mahala.com"
		COMPANY_NAME_1 = "Mahala"
		COMPANY_NAME_2 = "Mahala"
		COMPANY_IDENTITY = "MH"
		STYLE_NAME = "StyleAstech.css"
		MERCHANT_URL = "https://merchants.Mahala.us/"
		PROCESS_URL = "https://process.Mahala.us/"
		DEVCENTER_URL = "https://devcenter.Mahala.us/"
		USER_URL = "https://wallet.Mahala.us/"
		WEBSERVICES_URL = "https://webservices.Mahala.us/"
		FAVICON_URL = ""
		MAIL_INFO = "sales@Mahala.us"
		MAIL_SALES = "sales@Mahala.us"
		MAIL_SUPPORT = "sales@Mahala.us"
		MAIL_DOMAIN = "Mahala.us"
		SHOW_HEBREW = False
        FORCESSL = True
	Case "Local"
		DOMAIN_TEXT_1 = "netpay-intl.com"
		COMPANY_NAME_1 = "Netpay"
		COMPANY_NAME_2 = "נטפיי"
		COMPANY_IDENTITY = "NP"
		STYLE_NAME = "StyleNetpay.css"
		MERCHANT_URL = "http://192.168.5.9/"
		PROCESS_URL = "http://localhost/processWebSite/"
		DEVCENTER_URL = "http://192.168.5.11:90/"
		USER_URL = "http://192.168.5.11:91/"
		WEBSERVICES_URL = "http://localhost:83/webservices/"
		FAVICON_URL = ""
		MAIL_INFO = "tamir@netpay.co.il"
		MAIL_SALES = "tamir@netpay.co.il"
		MAIL_SUPPORT = "tamir@netpay.co.il"
		MAIL_DOMAIN = "intercash.net"
		SHOW_HEBREW = false
        FORCESSL = False
        IS_PRODUCTION = False
	Case else
		DOMAIN_TEXT_1 = "intercash.net"
		COMPANY_NAME_1 = "Netpay"
		COMPANY_NAME_2 = "נטפיי"
		COMPANY_IDENTITY = "NP"
		STYLE_NAME = "StyleNetpay.css"
		MERCHANT_URL = "http://192.168.5.9/"
		PROCESS_URL = "http://192.168.5.11/"
		DEVCENTER_URL = "http://192.168.5.11:90/"
		USER_URL = "http://192.168.5.11:91/"
		WEBSERVICES_URL = "http://192.168.5.11:93/"
		FAVICON_URL = ""
		MAIL_INFO = "tamir@netpay.co.il"
		MAIL_SALES = "tamir@netpay.co.il"
		MAIL_SUPPORT = "tamir@netpay.co.il"
		MAIL_DOMAIN = "intercash.net"
		SHOW_HEBREW = false
        FORCESSL = False
        IS_PRODUCTION = False
End Select
%>