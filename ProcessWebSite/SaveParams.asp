<%
	Dim fso, a, i
	Set fso = Server.CreateObject("Scripting.FileSystemObject")
	Set a = fso.OpenTextFile(Session("DataPath") & "Private/Logs/Request.txt", 8, true)

	sub WriteData(sData)
		a.WriteLine(sData)
		response.Write(sData & chr(10) & chr(13))
	end sub

	sub WriteSeparator
		WriteData("--------------------------------------------------")
	end sub

	sub WriteEmpty
		WriteData("")
	end sub

	response.ContentType="text/plain"
	response.CharSet="Windows-1255"
	
	WriteData("=====> " & Now())
	WriteEmpty
	WriteData("GET")
	WriteSeparator
	For each fldItem in Request.QueryString
		WriteData(fldItem & "=" & Request.QueryString(fldItem))
	Next
	WriteSeparator
	WriteEmpty
	WriteData("POST")
	WriteSeparator
	For each fldItem in Request.Form
		WriteData(fldItem & "=" & Request.Form(fldItem))
	Next
	WriteSeparator
	WriteEmpty
	WriteEmpty
	a.Close
	Set a = Nothing
	Set fso = Nothing
%>