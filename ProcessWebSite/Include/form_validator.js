var saRequiredFields=new Array(50), saRequiredNames=new Array(50), saRequiredTypes=new Array(50), nRequiredCount=0;

function AddRequiredField(sFieldName, sFieldTitle)
{
	saRequiredFields[nRequiredCount]=sFieldName;
	saRequiredNames[nRequiredCount]=sFieldTitle;
	saRequiredTypes[nRequiredCount]='';
	nRequiredCount++;
	return nRequiredCount-1;
}

function AddRequiredMail(sFieldName, sFieldTitle)
{
	saRequiredFields[nRequiredCount]=sFieldName;
	saRequiredNames[nRequiredCount]=sFieldTitle;
	saRequiredTypes[nRequiredCount]='mail';
	nRequiredCount++;
	return nRequiredCount-1;
}

function AddRequiredPhone(sFieldName, sFieldTitle)
{
	saRequiredFields[nRequiredCount]=sFieldName;
	saRequiredNames[nRequiredCount]=sFieldTitle;
	saRequiredTypes[nRequiredCount]='phone';
	nRequiredCount++;
	return nRequiredCount-1;
}

function AddRequiredCellular(sFieldName, sFieldTitle)
{
	saRequiredFields[nRequiredCount]=sFieldName;
	saRequiredNames[nRequiredCount]=sFieldTitle;
	saRequiredTypes[nRequiredCount]='cellular';
	nRequiredCount++;
	return nRequiredCount-1;
}

function AddRequiredZIP(sFieldName, sFieldTitle)
{
	saRequiredFields[nRequiredCount]=sFieldName;
	saRequiredNames[nRequiredCount]=sFieldTitle;
	saRequiredTypes[nRequiredCount]='zip';
	nRequiredCount++;
	return nRequiredCount-1;
}

function AddRequiredPersonalNumber(sFieldName, sFieldTitle)
{
	saRequiredFields[nRequiredCount]=sFieldName;
	saRequiredNames[nRequiredCount]=sFieldTitle;
	saRequiredTypes[nRequiredCount]='PersonalNumber';
	nRequiredCount++;
	return nRequiredCount-1;
}

function RemoveRequiredField(sFieldName)
{
	var j=nRequiredCount;
	for (var i=0;i<nRequiredCount;i++) if (saRequiredFields[i]==sFieldName) j=i;
	if (j<nRequiredCount)
	{
		saRequiredFields[nField]=saRequiredFields[nRequiredCount-1];
		saRequiredNames[nField]=saRequiredNames[nRequiredCount-1];
		saRequiredTypes[nField]=saRequiredTypes[nRequiredCount-1];
		nRequiredCount--;
	}
	return (j<nRequiredCount+1);
}

function DeleteRequiredField(nIndex)
{
	if (nIndex>=nRequiredCount) return false;
	saRequiredFields[nField]=saRequiredFields[nRequiredCount-1];
	saRequiredNames[nField]=saRequiredNames[nRequiredCount-1];
	saRequiredTypes[nField]=saRequiredTypes[nRequiredCount-1];
	nRequiredCount--;
	return true;
}

function ValidatePassword(sFormName, sFieldName1, sFieldName2)
{
	if (document.forms[sFormName].elements[sFieldName1].value!=document.forms[sFormName].elements[sFieldName2].value)
	{
		document.forms[sFormName].elements[sFieldName1].focus();
		alert('new password fields must match');
		return false;
	}
	return true;
}

function ValidateCreditCardValidity(sFormName, sFieldNameMonth, sFieldNameYear, sLang)
{
	var dNow=new Date();
	var nMonthNow=1*dNow.getMonth()+1*1;
	var nYearNow=dNow.getYear();
	var nMonth=document.forms[sFormName].elements[sFieldNameMonth].value;
	var nYear=document.forms[sFormName].elements[sFieldNameYear].value;
	if (nYearNow<nYear) return true;
	if (nYearNow>nYear)
	{
		document.forms[sFormName].elements[sFieldNameYear].focus();
		switch (sLang){
			case 'Heb': alert('���� ������ �� ����');
			break
			case 'Eng': alert('Expiration date not valid');
		}
		return false;
	}
	if (nMonthNow>nMonth)
	{
		document.forms[sFormName].elements[sFieldNameMonth].focus();
		switch (sLang){
			case 'Heb': alert('���� ������ �� ����');
			break
			case 'Eng': alert('Expiration date not valid');
		}
		return false;
	}
	return true;
}

function ValidateCreditCardNumber(sFormName, sFieldCard, sFieldType)
{
	var sCard=document.forms[sFormName].elements[sFieldCard].value;
	if (sCard=='4444333322221111')
	{
		return true;
	}
	var nType=document.forms[sFormName].elements[sFieldType].value;
	var nTotal=0;
	var nMulti=0;
	var nDigit=0;
	var nTemp=0;
	if ((nType==1)&&((sCard.length==8)||(sCard.length==9)))
	{
		//Isracart - 8 or 9 digits
		if (sCard.length==8)
		{
			sCard='0'+sCard;
		}
		for (var i=8;i>=0;i--)
		{
			nMulti++;
			nDigit=sCard.substr(i,1);
			nTotal+=nMulti*nDigit;
		}
		return (Math.round(nTotal/11)*11==nTotal)
	}
	else
	{
		nMulti=2;
		for (var i=sCard.length-1;i>-1;i--)
		{
			nMulti=3-nMulti;
			nDigit=sCard.substr(i,1);
			nTemp=nMulti*nDigit;
			if (nTemp>9)
			{
				nTemp=nTemp-9;
			}
			nTotal+=nTemp;
		}
		return (Math.round(nTotal/10)*10==nTotal);
	}
}

function ValidateCreditCardNew(sFormName, sFieldCard)
{
	var sCard=document.forms[sFormName].elements[sFieldCard].value;
	if ((sCard.length<8)||(sCard.length>19))
	{
		return false;
	}
	if (sCard.length==9)
	{
		return true;
	}
	if (sCard.length==8)
	{
		return true;
	}
	if ((sCard.length==16)&&((sCard.substr(0,1)=='3')||(sCard.substr(0,1)=='4')||(sCard.substr(0,1)=='5')||(sCard.substr(0,1)=='6')||(sCard.substr(0,6)<352800)||(sCard.substr(0,6)>358999)||(sCard.substr(0,4)=='4580')))
	{
		return true;
	}
	if ((sCard.length==15)&&((sCard.substr(0,2)=='34')||(sCard.substr(0,2)=='37')))
	{
		return true;
	}
	if ((sCard.length==14)&&((sCard.substr(0,1)=='3')||(sCard.substr(0,1)=='6')))
	{
		return true;
	}
	if (sCard.substr(0,1)=='4')
	{
		return true;
	}
	return false;
}

function ValidateCreditCardType(sFormName, sFieldCard, sFieldType)
{
	var sCard=document.forms[sFormName].elements[sFieldCard].value;
	if (sCard=='4444333322221111')
	{
		return true;
	}
	var nType=document.forms[sFormName].elements[sFieldType].value;
	if (nType==1)
	{
		//Isracart
		if (sCard.length==9)
		{
			return true;
		}
		if (sCard.length==8)
		{
			return true;
		}
		if ((sCard.length==16)&&((sCard.substr(0,1)=='5')||(sCard.substr(0,1)=='6')))
		{
			return true;
		}
		return false;
	}
	if ((nType==2)||(nType==5))
	{
		//Visa CAAL
		if (sCard.length==16)
		{
			if (((sCard.substr(0,1)=='3')||(sCard.substr(0,1)=='4'))&&((sCard.substr(0,6)<352800)||(sCard.substr(0,6)>358999)))
			{
				return true;
			}
			return false;
		}
		if ((sCard.length<11)||(sCard.length>19))
		{
			return false;
		}
		if (sCard.substr(0,1)=='4')
		{
			return true;
		}
		return false;
	}
	if (nType==3)
	{
		//Diners
		if ((sCard.length==14)&&((sCard.substr(0,1)=='3')||(sCard.substr(0,1)=='6')))
		{
			return true;
		}
		return false;
	}
	if (nType==4)
	{
		//American Express
		if ((sCard.length==15)&&((sCard.substr(0,2)=='34')||(sCard.substr(0,2)=='37')))
		{
			return true;
		}
		return false;
	}
	if (nType==6)
	{
		//Alpha Card
		if ((sCard.length==16)&&(sCard.substr(0,1)=='4'))
		{
			if(sCard.substr(0,4)=='4580')
			{
				return true;
			}
			return true;
		}
		return false;
	}
}

function ValidateCreditCard(sFormName, sFieldCard, sFieldType)
{
	var sCard=document.forms[sFormName].elements[sFieldCard].value;
	if (sCard.length==0)
	{
		return true;
	}
	var sAsterisks="********************";
	if ((sCard.substr(0,sCard.length-4)==sAsterisks.substr(0,sCard.length-4))&&(sCard.length>7))
	{
		return true;
	}
	if (!ValidateCreditCardNumber(sFormName, sFieldCard, sFieldType))
	{
		alert('���� ����� ����� ���� ����');
		document.forms[sFormName].elements[sFieldCard].focus();
		return false;
	}
	if (!ValidateCreditCardType(sFormName, sFieldCard, sFieldType))
	{
		alert('���� ����� ����� ���� ���� �� ����');
		document.forms[sFormName].elements[sFieldCard].focus();
		return false;
	}
	return true;
}

function ValidateForm(sFormName)
{
	for (var i=0;i<nRequiredCount;i++)
	{
 		if (document.forms[sFormName].elements[saRequiredFields[i]].value=='')
		{
			alert(saRequiredNames[i]);
			document.forms[sFormName].elements[saRequiredFields[i]].focus();
			return false;
		}
		if ((saRequiredTypes[i]=='PersonalNumber')&&((document.forms[sFormName].elements[saRequiredFields[i]].value.length!=9)||(isNaN(document.forms[sFormName].elements[saRequiredFields[i]].value))))
		{
			alert('���� ����� ���� �� ���� - ���� ����� �� ��� �����');
			document.forms[sFormName].elements[saRequiredFields[i]].focus();
			return false;
		}
		if ((saRequiredTypes[i]=='phone')&&((document.forms[sFormName].elements[saRequiredFields[i]].value.length!=7)||(isNaN(document.forms[sFormName].elements[saRequiredFields[i]].value))))
		{
			alert('���� ����� �� ���� - ���� ����� �� ��� �����');
			document.forms[sFormName].elements[saRequiredFields[i]].focus();
			return false;
		}
		if ((saRequiredTypes[i]=='mobile')&&((document.forms[sFormName].elements[saRequiredFields[i]].value.length!=6)||(isNaN(document.forms[sFormName].elements[saRequiredFields[i]].value))))
		{
			alert('���� ����� ���� �� ���� - ���� ����� �� �� �����');
			document.forms[sFormName].elements[saRequiredFields[i]].focus();
			return false;
		}
		if ((saRequiredTypes[i]=='zip')&&((document.forms[sFormName].elements[saRequiredFields[i]].value.length!=5)||(isNaN(document.forms[sFormName].elements[saRequiredFields[i]].value))))
		{
			alert('����� �� ���� - ���� ����� �� ��� �����');
			document.forms[sFormName].elements[saRequiredFields[i]].focus();
			return false;
		}
		if ((saRequiredTypes[i]=='mail')&&((document.forms[sFormName].elements[saRequiredFields[i]].value.lastIndexOf('@')<1)||(document.forms[sFormName].elements[saRequiredFields[i]].value.lastIndexOf('.')<1)||(document.forms[sFormName].elements[saRequiredFields[i]].value.lastIndexOf('.')<2+document.forms[sFormName].elements[saRequiredFields[i]].value.lastIndexOf('@'))||(document.forms[sFormName].elements[saRequiredFields[i]].value.lastIndexOf('.')>document.forms[sFormName].elements[saRequiredFields[i]].value.length-3)||(document.forms[sFormName].elements[saRequiredFields[i]].value.lastIndexOf('.')<document.forms[sFormName].elements[saRequiredFields[i]].value.length-4)||(document.forms[sFormName].elements[saRequiredFields[i]].value.lastIndexOf(' ')!=-1)||(document.forms[sFormName].elements[saRequiredFields[i]].value.lastIndexOf('.@')!=-1)||(document.forms[sFormName].elements[saRequiredFields[i]].value.lastIndexOf('@.')!=-1)))
		{
			alert('invalid E-mail');
			document.forms[sFormName].elements[saRequiredFields[i]].focus();
			return false;
		}
	}
	return true;
}
