\r<!--#include file="../globalConst.asp"-->
<%

'----------------------------------------------------------------------------
'	Database connection
'----------------------------------------------------------------------------
'Opens Connection
Const APP_MINID = 000000000
Const APP_MAXID = 100000000

  

Dim oledbData, DSNReq, sUrlDsn, GlobalSaveToFile : GlobalSaveToFile = False
Set oledbData = Server.CreateObject("ADODB.Connection")

If Not IS_PRODUCTION Then Application("DBConnect")=""
If trim(Application("DBConnect"))="" then
	Set DSNReq = Server.CreateObject("Msxml2.ServerXMLHTTP")
	If request.ServerVariables("HTTPS")="on" Then sUrlDsn="https://" Else sUrlDsn="http://"
	sUrlDsn=sUrlDsn & request.ServerVariables("SERVER_NAME") & ":" & request.ServerVariables("SERVER_PORT")
	If IS_PRODUCTION Then 
		sUrlDsn=sUrlDsn & Left(request.ServerVariables("SCRIPT_NAME"), inStr(request.ServerVariables("SCRIPT_NAME"), "/")) & "dsn.aspx"
	Else
		If request.ServerVariables("SERVER_NAME") = "127.0.0.1" or request.ServerVariables("SERVER_NAME") = "localhost" Or Left(request.ServerVariables("SERVER_NAME"), 10) = "192.168.5." Then
			sUrlDsn = sUrlDsn & "/ProcessWebSite/dsn.aspx"
		Else
			sUrlDsn = sUrlDsn & "/dsn.aspx"
		End If
	End If

	DSNReq.Open "POST", sUrlDsn, false
	DSNReq.Send
	
	Application("DBConnect")=DSNReq.responseText
	'Application("DBConnect")="Server=YODEVSRV01;Database=GW_Yomane;Uid=GW;Pwd=p4s5w0rD;"
	set DSNReq = Nothing
End If 



Dim sCurrentConnectionString : sCurrentConnectionString="Provider=SQLOLEDB;" & Application("DBConnect") 
'Dim sCurrentConnectionString : sCurrentConnectionString="Provider=SQLNCLI11;" & Application("DBConnect")    
 
   
 oledbData.Open sCurrentConnectionString
 

if Application("PendingUpdated")<>"1" then
    
	oledbData.Execute "UPDATE tblCompanyTransPending SET Locked=0"
	Application("PendingUpdated")="1"
end if

Dim NetpayCryptoObj : Set NetpayCryptoObj = Server.CreateObject("Netpay.Crypt.Utility")

'Close Connection
Function closeConnection()
	If lcase(TypeName(oledbData))="connection" Then
		oledbData.close
		Set oledbData = nothing
	End If
	If Not NetpayCryptoObj Is Nothing Then Set NetpayCryptoObj = Nothing
End Function

Function ExecScalar(sql, lDef) 
	Dim iRs
	'on error resume next
	Set iRs = oledbData.Execute(sql)
	If iRs.EOF Then ExecScalar = lDef Else ExecScalar = iRs(0)
	iRs.Close 
	'if error.number <> 0 then 
		'response.write error.number & " " & error : response.end
	'end if
	'on error goto 0
    'Response.Write(iRs)
    'Response.End
End Function

Function ExecSQL(sql)
	Dim nRecords
	'Response.Write(sql & "<br>")
	If GlobalSaveToFile Then
		FileAppendData "ERROR_PASS.txt", sql & vbcrlf
		ExecSQL = -1
	Else
		oledbData.Execute sql, nRecords
		ExecSQL = nRecords
	End If	
End Function

Function ExecSQLWithFile(sql)
	Dim nRecords
	On Error Resume Next
	Err.Clear
	oledbData.Execute sql, nRecords
	If Err.Number <> 0 Then 
		FileAppendData "ERROR_PASS.txt", sql & vbcrlf
		ExecSQLWithFile = -1
	End If
	On Error Goto 0
	ExecSQLWithFile = nRecords
End Function

Function IIf(valTest, valTrue, valFalse)
	If valTest Then IIf = valTrue Else IIf = valFalse
End Function

Function URLDecode(str) 
	Dim i, sT, sR
	str = Replace(str, "+", " ") 
	For i = 1 To Len(str) 
		sT = Mid(str, i, 1) 
		If sT = "%" Then 
			If i + 2 <= Len(str) Then 
				sR = sR & _ 
					Chr(CLng("&H" & Mid(str, i + 1, 2))) 
				i = i+2 
			End If 
		Else 
			sR = sR & sT 
		End If 
	Next 
	URLDecode = sR 
End Function

'----------------------------------------------------------------------------
'	Data Validation
'----------------------------------------------------------------------------
Function TestNumVar(vrVal, vrMin, vrMax, vrDef)
	If vrVal <> "" And IsNumeric(vrVal) Then
		TestNumVar = cdbl(vrVal)
		If((vrMin < vrMax) And ((vrMin > TestNumVar) Or (vrMax < TestNumVar))) Then _
			TestNumVar = vrDef
	Else
		TestNumVar = vrDef
	End If
End Function

function TestNumOptional(vrVal)
	TestNumOptional=TestNumVar(vrVal, 1, 0, "")
end function

Function IsNumericString(xStr, nMinLen, nMaxLen)
	Dim xChar, i
	IsNumericString = False
	If (nMinLen <= nMaxLen) And ( (Len(xStr) < nMinLen) Or (Len(xStr) > nMaxLen) ) Then Exit Function
	For i = 1 To Len(xStr)
		xChar = Mid(xStr, i, 1)
		If (Asc(xChar) < 48) Or (Asc(xChar) > 57) Then Exit Function
	Next
	IsNumericString = True
End Function

Function TestStrVar(vrVal, nMinLen, nMaxLen, vrDef)
	If vrVal <> "" Then
		TestStrVar = vrVal
		If((nMinLen < nMaxLen) And ((nMinLen > len(vrVal)) Or (nMaxLen < len(vrVal)))) Then _
			TestStrVar = vrDef
	Else
		TestStrVar = vrDef
	End If
End Function

Function TestNumList(xStr)
	Dim xVars
	xVars = Split(xStr, ",")
	For i = 0 To UBound(xVars)
		xVal = TestNumVar(xVars(i), 0, -1, 0)
		if xVal <> 0 Then 
			If TestNumList <> "" Then TestNumList = TestNumList & ","
			TestNumList = TestNumList & xVal
		End if	
	Next
End Function
'
' 20080309 Tamir
'
' Function GetDomainSSL added
'
' This function returns the current domain with HTTP, if the address includes "192.", HTTPS otherwise.
'
function GetDomainSSL()
	if inStr(LCase(Session("TempOpenURL")), "192.168")>0 then
		GetDomainSSL = Session("TempOpenURL")
	else
		GetDomainSSL = Replace(LCase(Session("TempOpenURL")), "http:", "https:")
	end if
end function

'----------------------------------------------------------------------------
'	Manipulate URL
'----------------------------------------------------------------------------
Function UrlWithout(qString, strParam)
	UrlWithout = qString
	idx = InStr(1, "&" & UrlWithout, "&" & strParam & "=")
	If(idx < 1) Then Exit Function
	idxEnd = InStr(idx, UrlWithout, "&")
	If(idxEnd < 1) Then idxEnd = Len(UrlWithout)
	UrlWithout = Left(UrlWithout, idx - 1) & Right(UrlWithout, Len(UrlWithout) - idxEnd)
End Function

Function GetURLValue(strUrl, strValue)
	Dim lIndex, eIndex, lChar : lIndex = 0 : lChar = ""
	Do
		lIndex = InStr(lIndex + 1, strUrl, strValue & "=", 1)
		If(lIndex < 1) Then Exit Function _
		Else If(lIndex > 1) Then lChar = Mid(strUrl, lIndex - 1, 1)
	Loop While lIndex > 1 And lChar <> "&" And lChar <> "?"
	lIndex = lIndex + Len(strValue & "=")
	eIndex = InStr(lIndex, strUrl, "&")
	If(eIndex < 1) Then eIndex = Len(strUrl) + 1
	GetURLValue = Mid(strUrl, lIndex, eIndex - lIndex)
End Function

Function SetStringValue(str, nVal, start, slen)
	If Len(str) < start Then
		SetStringValue = str
	Else
		If Len(str) < (start + slen) Then slen = Len(str) - start+1
		SetStringValue = Left(str, start - 1) & nVal & Right(str, (Len(str) - (start + slen)) + 1)
	End if	
End Function

Function HideUrlParam(strUrl, xParam, nStart, nLen)
	Dim lIndex, eIndex, i
	lIndex = InStr(1, strUrl, xParam & "=", 1)
	If(lIndex < 1) Then
		HideUrlParam = strUrl
		Exit Function
	End If
	lIndex = lIndex + Len(xParam & "=")
	eIndex = InStr(lIndex, strUrl, "|")
	If(eIndex < 1) Then eIndex = Len(strUrl) + 1
	HideUrlParam = Mid(strUrl, lIndex, eIndex - lIndex)
	If Len(HideUrlParam) < nLen Then nLen = Len(HideUrlParam)
	HideUrlParam = SetStringValue(HideUrlParam, String(nLen, "x"), nStart, nLen)
	HideUrlParam = SetStringValue(strUrl, HideUrlParam, lIndex, eIndex - lIndex)
End Function

'----------------------------------------------------------------------------
'	Password history and policy
'----------------------------------------------------------------------------
Function CheckPasswordPolicy(pss)
	Dim i, bNumFound, bCharFound
	CheckPasswordPolicy = False
	If Len(pss) < 8 Then Exit Function
	bNumFound = 0 : bCharFound = 0
	For i = 1 To Len(pss)
		 xChar = Mid(pss, i, 1)
	 If Asc(xChar) >= Asc("0") And Asc(xChar) <= Asc("9") Then bNumFound = bNumFound + 1 _
	 Else bCharFound = bCharFound + 1
	Next
	CheckPasswordPolicy = (bNumFound > 1) And (bCharFound > 1)
End Function

Function UpdatePasswordChange(refType, refID, sPassword)
	UpdatePasswordChange = ExecScalar("UpdatePasswordChange " & refType & ", " & refID & ", '" & Request.ServerVariables("REMOTE_ADDR") & "', '" & Replace(sPassword, "'", "''") & "'", 0)
End Function

Function IsPassword(refType, refID, sPassword)
	IsPassword = (ExecScalar("Select Count(*) From tblPasswordHistory Where LPH_ID = 1 And LPH_RefType=" & refType & " And LPH_RefID=" & refID & " And LPH_Password256=dbo.GetEncrypted256('" & Replace(sPassword, "'", "''") & "')", 0) > 0)
End Function

Sub DeletePassword(refType, refID)
	oleDbData.Execute "Delete From tblPasswordHistory Where LPH_RefType=" & refType & " And LPH_RefID=" & refID
End Sub

'----------------------------------------------------------------------------
'	Manipulate text string
'----------------------------------------------------------------------------
Function DBText(sText)
	DBText = sText
	If NOT isNull(sText) Then
		If trim(sText)<>"" Then
			DBText = Replace(Replace(trim(sText),"'","`"),"""","``")
			DBText = Replace(Replace(DBText, "<", "&lt;"), ">", "&gt;")
		End If
	End If
End Function

Function dbtextShow(sText)
	dbtextShow = sText
	If NOT isNull(sText) Then
		If trim(sText)<>"" Then
			dbtextShow = replace(replace(replace(trim(sText),"``",""""),"`","'"),chr(13),"<br>")
		End If
	End If
End Function
Function dbtextShowForm(sText)
	dbtextShowForm = sText
	If NOT isNull(sText) Then
		If trim(sText)<>"" Then
			dbtextShowForm = replace(replace(trim(sText),"``",""""),"`","'")
		End If
	End If
End Function
Function dbtextShowHtml(sText)
	dbtextShowHtml = sText
	If NOT isNull(sText) Then
		If trim(sText)<>"" Then
			dbtextShowHtml = replace(replace(trim(sText),"``",""""),"`","'")
		End If
	End If
End Function


'----------------------------------------------------------------------------
'	Misc
'----------------------------------------------------------------------------
Function FormatDatesTimes(sString,bShowDate,bShowTime,bShowSec)
	sDateTime = ""
	If bShowDate = 1 Then
		If day(sString)<10 Then sDateTime = sDateTime & "0"
		sDateTime = sDateTime & day(sString) & "/"
		If month(sString)<10 Then sDateTime = sDateTime & "0"
		sDateTime = sDateTime & month(sString) & "/" & right(year(sString),2)
	End If
	If bShowDate = 1 AND	bShowTime = 1 Then sDateTime = sDateTime & "&nbsp;&nbsp;"
	If bShowTime = 1 Then
		If hour(sString)<10 Then sDateTime = sDateTime & "0"
		sDateTime = sDateTime & hour(sString) & ":"
		If minute(sString)<10 Then sDateTime = sDateTime & "0"
		sDateTime = sDateTime & minute(sString)
		If bShowSec = 1 Then sDateTime = sDateTime & ":" & second(sString)
	End If
	FormatDatesTimes = sDateTime
End Function
					
Function GetRandomNum(sLength)
	Dim i : GetRandomNum = ""
	Randomize(Timer)
	While Len(GetRandomNum) < sLength
		GetRandomNum = GetRandomNum & Left(CLng(Rnd() * 10), 1)
	Wend
End Function

Function FileAppendData(strFile, strData)
	On Error Resume Next
	Set fso = Server.CreateObject("Scripting.FileSystemObject")
	Set obfFile = fso.OpenTextFile(Session("DataPath") & "Private/Logs/" & strFile, 8, True)
	Call obfFile.Write(strData)
	obfFile.Close
	Set obfFile = Nothing
	Set fso = Nothing
	On Error Goto 0
End Function

Function GetCurText(ByVal trnCurr)
	If IsEmpty(Application("CUR_CHARS")) Then LoadCurrencies()
	trnCurr = CInt(trnCurr)
	If (trnCurr < 0) Or (trnCurr > Ubound(Application("CUR_CHARS"))) Then GetCurText = IIf(trnCurr >= 255, "[All]", "") _
	Else GetCurText = Application("CUR_CHARS")(trnCurr)
End Function

Function GetCurISOName(ByVal nID)
	If IsEmpty(Application("CUR_ISONAMES")) Then LoadCurrencies()
	If nID > UBound(Application("CUR_ISONAMES")) Then LoadCurrencies()
	GetCurISOName = Application("CUR_ISONAMES")(nID)
End Function

Function GetCurISOCode(ByVal nID)
	If IsEmpty(Application("CUR_ISOCODES")) Then LoadCurrencies()
	If nID > UBound(Application("CUR_ISONAMES")) Then LoadCurrencies()
	GetCurISOCode = Application("CUR_ISOCODES")(nID)
End Function

Function FormatCurr(nCurrency, nAmount)
	FormatCurr = GetCurText(nCurrency) & "&nbsp;" & FormatNumber(nAmount, 2, True, False, True)
End Function

Function FormatCurrWCT(trnCurr, trnValue, crType)
	If isNumeric(crType) And CInt(crType) = 0 Then trnValue = -trnValue
	FormatCurrWCT = FormatCurr(trnCurr, trnValue)
End Function

Public Function LoadCurrencies()
	Dim crRs, CUR_CHARS(), CUR_RATES(), CUR_ISONAMES(), CUR_ISOCODES()
	Set crRs = Server.CreateObject("ADODB.Recordset")
	crRs.Open "Select CurrencyID, Symbol, BaseRate, CurrencyISOCode, ISONumber From List.CurrencyList Order By CurrencyID Desc", OleDbData, 0, 1
	If Not crRs.EOF Then
		MAX_CURRENCY = crRs("CurrencyID")
		Redim CUR_CHARS(MAX_CURRENCY)
		Redim CUR_RATES(MAX_CURRENCY)
		Redim CUR_ISONAMES(MAX_CURRENCY)
		Redim CUR_ISOCODES(MAX_CURRENCY)
	End if
	Do While Not crRs.EOF
		If crRs("CurrencyID") - MAX_CURRENCY <= 0 Then
			CUR_CHARS(crRs("CurrencyID")) = crRs("Symbol")
			CUR_RATES(crRs("CurrencyID")) = CCur(crRs("BaseRate"))
			CUR_ISONAMES(crRs("CurrencyID")) = crRs("CurrencyISOCode")
			CUR_ISOCODES(crRs("CurrencyID")) = Right("000" & Trim(crRs("ISONumber")), 3)
		End If
		crRs.MoveNext
	Loop
	crRs.Close()
	Set crRs = Nothing
	Application("CUR_CHARS") = CUR_CHARS
	Application("CUR_RATES") = CUR_RATES
	Application("CUR_ISONAMES") = CUR_ISONAMES
	Application("CUR_ISOCODES") = CUR_ISOCODES
End Function

Public Function ConvertCurrencyRate(xCurFrom, xCurTo)
	If xCurFrom <> xCurTo Then
		If IsEmpty(Application("CUR_CHARS")) Then LoadCurrencies()
		ConvertCurrencyRate = Application("CUR_RATES")(xCurFrom) / Application("CUR_RATES")(xCurTo)
	Else
		ConvertCurrencyRate = 1
	End if
End Function 

Public Function ConvertCurrency(xCurFrom, xCurTo, xAmount)
	ConvertCurrency = CCur(ConvertCurrencyRate(xCurFrom, xCurTo) * xAmount)
End Function

Function showDivBox(fType, fDir, fWidth, fText) '(help;note, ltr,rtl)
	Select case Trim(fType)
		Case "help" : sStyle = "width:" & fWidth & "px; background-color:#ECEFFF; border:1px solid Navy;"
		Case "note" : sStyle = "width:" & fWidth & "px; background-color:#ffffe6; border:1px solid #353535;"
	End Select
	Select case Trim(fDir)
		Case "ltr" : sClass = "txt11"
		Case "rtl" : sClass = "txt12"
	End Select
	
	objectTmp = "previousSibling.style"
	sOnMouseOver = "previousSibling.style.visibility='visible'; previousSibling.style.left=getAbsPos(this).Left+35; previousSibling.style.top=getAbsPos(this).Top;"
	sOnMouseOut = "previousSibling.style.visibility='hidden';"
	
	Response.Write("<div class=""" & sClass & """ style=""direction:" & fDir & "; visibility:hidden; position:absolute; z-index:1; padding:3px; " & sStyle & """>" & fText & "</div>")
	Response.Write("<img src=""../images/img/iconQuestionGrayS.gif"" align=""middle"" border=""0"" style=""cursor:help;"" onmouseover=""" & sOnMouseOver & """ onmouseout=""" & sOnMouseOut & """ />")
End Function

Function CreateTextBlob(text)
	Set CreateTextBlob = NetpayCryptoObj.CreateBlob()
	if Not IsEmpty(text) Then CreateTextBlob.Text = text
End Function

Function Hash(hashType, hashString)
	If hashType = "SHA" Then hashType = "SHA256"
	Set Hash = CreateTextBlob(hashString).Hash(hashType)
End Function

Function HMAC(algType, keyBlob, hashBlob)
	Dim obj
	Set obj = NetpayCryptoObj.CreateHmac(algType) 
	Set obj.Key = keyBlob
	Set HMAC = obj.Calculate(hashBlob)
	Set obj = Nothing
End Function

Function CalcHash(hashType, hashString, useRawOutput)
	Dim obj : Set obj = Hash(hashType, hashString)
	if Not useRawOutput Then obj.ASCII = LCase(obj.Hex)
	CalcHash = obj.Base64
	Set obj = Nothing
End Function

Function ValidateHash(hashType, hashString, hashValue)
	Dim hashCalcValue
	hashCalcValue = CalcHash(hashType, hashString, True)
	ValidateHash = (hashCalcValue = hashValue)
	If ValidateHash Then Exit Function
	ValidateHash = CalcHash(hashType, hashString, False) = hashValue
End Function

'
' 20080819 Tamir
'
' Support of error handling after response.flush
'
sub HandleErrorAfterFlush
	if err.number<>0 then
		sErrNumber=err.number 
		sErrSource=err.Source
		sErrDescription=err.Description 
		Session("ErrNumber")=err.number 
		Session("ErrSource")=err.Source
		Session("ErrDescription")=err.Description 
		Session("ErrSQL")=sSQL
		if Session("ErrSQL")="" then Session("ErrSQL")=SQL
		on error goto 0
		server.Transfer "../error.asp"
	end if
end sub

'
' 20081104 Tamir
'
' Checks, if the string matches the regular expression pattern
'
function IsValidStringRegex(sString, sPattern)
	if sString="" then
		IsValidStringRegex=false
	else
		dim regxTest : set regxTest=new RegExp
		regxTest.Pattern = sPattern
		regxTest.IgnoreCase=True
		regxTest.Global=false
		IsValidStringRegex = regxTest.test(sRecurring)
		set regxTest=nothing
	end if
end function

'
' 20081104 Tamir - Validate recurring string format
'
' Recurring field format: {number of charges}{gap unit}{gap length}A{amount}
' Having:
' {number of charges} � a number of charges in this part of recurring series
' {gap unit} � one of the following values: D (day), W (week), M (month), Q (quarter), Y (year)
' {gap length} � a number of gap units
' A{amount} � an amount to charge (optional). If the amount is omitted, letter A must be omitted, too.
'
' Examples:
' 12M1A30 - twelwe monthly charges, 30 currency units each charge
' 1D3 - one charge with 3 days delay after the charge, with default amount
' 
function IsValidRecurringString(sRecurring)
	IsValidRecurringString=IsValidStringRegex(sRecurring, "^[1-9][0-9]{0,1}[DWMQY][1-9][0-9]*(A[0-9]+\.*[0-9]*)*$")
end function

'
' 20111026 Tamir - Limit recurring series to 10 years
'
' Returns the last charge date based on the initial date and the recurring string
' The boolean parameter bLastGap defines, if the recurring gap should ber taken after the last charge
'
Function DateAddRecurring(dtFirst, sRecurring, bLastGap)
	Dim dtLast : dtLast = dtFirst
	Dim sTemp : sTemp = UCase(sRecurring)
	If InStr(sTemp, "A") > 0 Then sTemp = Left(sTemp, InStr(sTemp, "A") - 1)
	Dim nInterval : nInterval = IIf(InStr("DWMQY", Mid(sRecurring, 3, 1)), 3, 2)
	Dim nCharges : nCharges = Left(sTemp, nInterval - 1)
	Dim nLength : nLength = Mid(sTemp, nInterval + 1)
	Dim sInterval, i
	Select Case Mid(sRecurring, nInterval, 1)
		Case "D"
			sInterval = "d"
		Case "W"
			sInterval = "d"
			nLength = nLength * 7
		Case "M"
			sInterval = "m"
		Case "Q"
			sInterval = "m"
			nLength = nLength * 3
		Case Else
			sInterval = "m"
			nLength = nLength * 12
	End Select
	For i = 1 to IIf(bLastGap, nCharges, nCharges - 1)
		dtLast = DateAdd(sInterval, nLength, dtLast)
	Next
	DateAddRecurring = dtLast
End Function

'
' 20111117 Tamir - Limit number of charges in recurring series
'
' Returns the number of charges in the recurring stage, based on its recurring string
'
Function RecurringGetChargeCount(sRecurring)
	RecurringGetChargeCount = TestNumVar(Left(UCase(sRecurring), IIf(InStr("DWMQY", Mid(sRecurring, 3, 1)), 2, 1)), 1, 99, 0)
End Function

'
' 20081203 Tamir
'
' Returns appropriate icon for the passed filename extension
'
function GetFileIcon(sFileName)
	dim sFileExt, sIcon
	sFileExt=LCase(mid(sFileName, inStrRev(sFileName, ".")+1))
	if len(sFileExt)>3 then sFileExt=Left(sFileExt, 3)
	select case sFileExt
		case "rtf" : sFileExt="doc"
		case "jpe" : sFileExt="jpg"
		case "mp4", "mov", "wmv", "asf" : sFileExt="avi"
		case "mp3", "mid" : sFileExt="wav"
		case "rar", "arj", "7z" : sFileExt="zip"
	end select
	if sFileExt="rtf" then sFileExt="doc"
	if sFileExt="jpe" then sFileExt="jpg"
	if instr(".avi.bmp.doc.eps.gif.jpg.mpg.pdf.png.ppt.tif.txt.wav.xls.zip.", "." & sFileExt & ".")<1 then
		GetFileIcon=""
	else
		GetFileIcon="<img src=""../Images/FileExtIcons/" & sFileExt & ".gif"" style=""border-width:0;margin:0 5px 0 0;vertical-align:middle;"" />"
	end if
end function
%>
