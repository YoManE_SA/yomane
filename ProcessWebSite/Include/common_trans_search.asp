<%
nCount = nCount+1
sPayments=rsData("Payments")
sCreditType=trim(rsData("CreditType"))
sPayStatusText=""
sTdFontColor="black"
sShowRefund=false
sShowAskRefund = false
sShowAdminCharge = false

If lang = "HE" Then
    sTxtArchive = "�������"
    sTxtPending = "����� ������"
    sTxtSettled = "����"
Else
    sTxtArchive = "Archive"
    sTxtPending = "Pending"
    sTxtSettled = "Settled"
End if

If rsData("PaymentMethod_id")=1 OR rsData("PaymentMethod_id")=2 then
'Credit cards and eChecks

	if int(rsData("CreditType"))=0 then ' ����� ����
		' xxxxxxx ��� ���� xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		if Instr(trim(rsData("payID")),";X;")>0 then
			sPayStatusText = sTxtArchive
		elseif int(replace(trim(rsData("payID")),";",""))=0 then
			sPayStatusText = sTxtPending
		else
			sPayStatusText = sTxtSettled
		end if
		' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		sTrBkgColor="#F6FEF6"
		sTdBkgColorFirst="#66cc66"
		sTdFontColor="red"
		sRespong="�����"
	elseif trim(rsData("replyCode"))="000" then  '���� ��� ������
		if sCreditType="8" then  ' �� ���� ��������
			' xxxxxxx ��� ���� xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			if Instr(trim(rsData("payID")),";X;")>0 then
				sPayStatusText=sTxtArchive
			elseif int(replace(trim(rsData("payID")),";",""))=0 then
				sPayStatusText=sTxtPending
				sShowRefund=true
			elseif Instr(trim(rsData("payID")),";0;")=0 then
				sPayStatusText=sTxtSettled
				sShowRefund=true
			elseif Instr(trim(rsData("payID")),";0;")>0 then
				sPayStatusText="���� ����"
				sShowRefund=true
			end if
			' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			if rsData("deniedstatus")=0 then '���� ������
				sTdBkgColorFirst="#66cc66"
				sTrBkgColor="#F6FEF6"
				sShowAskRefund=true
			elseif rsData("deniedstatus")=1 then ' ����� ������
				sTdBkgColorFirst="#66cc66"
				sTrBkgColor="#fff3dd"
			elseif rsData("deniedstatus")=3 then ' ����� ������ ����� ����
				sTdBkgColorFirst="#66cc66"
				sTrBkgColor="#F6FEF6"
			elseif rsData("deniedstatus")=4 then ' ����� ������ ����� ����
				sTdBkgColorFirst="#66cc66"
				sTrBkgColor="#fff3dd"
				sPayStatusText=sTxtSettled
			end if
		else  ' �� ���� �� ��������
			sShowRefund=true
			' xxxxxxx ��� ���� xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			if Instr(trim(rsData("payID")),";X;")>0 then
				sPayStatusText=sTxtArchive
			elseif int(replace(trim(rsData("payID")),";",""))=0 then
				sPayStatusText=sTxtPending
			else
				sPayStatusText=sTxtSettled
			end if
			' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
			if rsData("deniedstatus")=0 AND rsData("payID")<>";0;" then '���� �����
				sTdBkgColorFirst="#66cc66"
				sTrBkgColor="#F6FEF6"
				sShowAskRefund=true
			elseif rsData("deniedstatus")=0 then '���� ������
				sTdBkgColorFirst="#66cc66"
				sTrBkgColor="#F6FEF6"
				sShowAskRefund=true
			elseif rsData("deniedstatus")=1 then ' ����� ������
				sTdBkgColorFirst="#66cc66"
				sTrBkgColor="#fff3dd"
			elseif rsData("deniedstatus")=2 then ' ����� ���� �����
				sTdBkgColorFirst="#66cc66"
				sTrBkgColor="#fff3dd"
			elseif rsData("deniedstatus")>2 then
				sTdBkgColorFirst="#66cc66"
				sTrBkgColor="#fff3dd"
			end if
			
		end if
	end if

Elseif rsData("PaymentMethod_id")="3" then
'System transactions

	If rsData("TransSource_id") = 21 OR int(rsData("TransSource_id")) = 1 OR int(rsData("TransSource_id")) = 2 OR int(rsData("TransSource_id")) = 3 Then
		if Instr(trim(rsData("payID")),";X;")>0 then
			sPayStatusText=sTxtArchive
		elseif int(replace(trim(rsData("payID")),";",""))=0 then
			sPayStatusText=sTxtPending
		else
			sPayStatusText=sTxtSettled
		end if
		sTrBkgColor = "f5f5f5"
		sTdBkgColorFirst = "#484848"
		sTdFontColor = "#cc0000"
	End if
	
Elseif int(rsData("PaymentMethod_id")) = 4 then
'Admin transactions
		
	sShowAdminCharge=true
	' xxxxxxx ��� ���� xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	if Instr(trim(rsData("payID")),";X;")>0 then
		sPayStatusText=sTxtArchive
	elseif int(replace(trim(rsData("payID")),";",""))=0 then
		sPayStatusText=sTxtPending
	else
		sPayStatusText=sTxtSettled
	end if
	' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	sTdBkgColorFirst="#003399"
	sTrBkgColor="#eeeffd"
	If int(sCreditType)=0 then 'credit
		sTdFontColor="#cc0000"
	End if
	
Elseif int(rsData("PaymentMethod_id")) = 5 then
'Fees transactions

	If int(rsData("TransSource_id"))=1 OR int(rsData("TransSource_id"))=2 then
		sShowFeeTrans=true
		' xxxxxxx ��� ���� xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		if Instr(trim(rsData("payID")),";X;")>0 then
			sPayStatusText=sTxtArchive
		elseif int(replace(trim(rsData("payID")),";",""))=0 then
			sPayStatusText=sTxtPending
		else
			sPayStatusText=sTxtSettled
		end if
		' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		sTdBkgColorFirst="#484848"
		sTrBkgColor="#f5f5f5"
		sTdFontColor="#cc0000"
	End if
	
End if

if rsData("isTestOnly") then '����� �����
	sBgTd="#ffffff"
	sFontColor="#6b6b6b"
end if
%>