<%
	'
	' 20080513 Tamir
	'
	' New security mechanism - groups
	'
%>
	<!--#include file="security_class.asp"-->
<%

	'----------------------------------------------------------------------------
	'	Test page Permissions
	'----------------------------------------------------------------------------
	Function AccessLevelCheck(fPageLevel, sPageClosingTags, bRedirect)
		'
		' 20080514 Tamir
		'
		' The original code is placed after the "exit function" statement
		'
		dim sManage, bManaged, sActive, bActive, sText
		bManaged=PageSecurity.IsDocumentManaged
		if bManaged then
			sText="+"
		else
			sText="-"
		end if
		bActive=PageSecurity.IsActive
		if bActive then
			sActive="Full control"
		else
			sActive="Read only"
		end if
		%>
			<script language="javascript" type="text/javascript" src="security.js"></script>
		<%
		if PageSecurity.IsAdmin then
			sManage=" style=\""cursor:pointer\"" title=\""" & sActive & ", "
			if bManaged then
				sManage=sManage & "Managed"
			else
				sManage=sManage & "Unmanaged"
			end if
			sManage=sManage & " - click to manage permissions\"" class=\""alwaysactive\"" onclick=\""OpenSecurityManager();\"""
		else
			sManage=" title=\""" & sActive & "\"""
		end if
		if PageSecurity.IsPermitted then
			%>
			<script language="JavaScript" type="text/javascript" defer="defer">
				var objRef = document.getElementById("pageMainHeadingTd")
				objRef.innerHTML = "<span<%= sManage %> id=\"pageSecurityHeading\"><%= sText %></span>&nbsp; " + objRef.innerHTML
			</script>
			<%
			if not bActive then
				%>
					<script language="javascript" type="text/javascript">addEvent(window, "load", DeactivateContent);</script>
				<%
			end if
		else
			if bRedirect then
				response.Redirect "security_message.aspx?PageURL=" & PageSecurity.Path
			else
				%>
				<script language="JavaScript" type="text/javascript">
					var objRef = document.getElementById("pageMainHeadingTd")
					objRef.innerHTML = "<span<%= sManage %> id=\"pageSecurityHeading\"><%= sText %></span>&nbsp; " + objRef.innerHTML
				</script>
				<%
				if UCase(Trim(pageSecurityLang))="ENG" then
					%>
					<div align="center" style="width:100%">
					    <div align="center" style="direction:ltr;text-align:center;">
					        <br /><br /><hr color="silver" size="1" noshade  />
						    <span class="txt13b">Security message: access denied.</span><br />
						    <span class="txt11">You are not authorized to see the content of this page.</span><br />
						    <hr color="silver" size="1" noshade  />
					    </div>
					</div>
					<%
				else
					%>
					<div align="center" style="width:100%">
					    <div align="center" style="direction:rtl;text-align:center;">
					        <br /><br /><hr color="silver" size="1" noshade  />
						    <span class="txt14b">����� �����: ��� ����.</span><br />
						    <span class="txt12">��� �� ����� ����� �� ���� ����� ���.</span><br />
						    <hr color="silver" size="1" noshade  />
					    </div>
					</div>
					<%
				end if
				Response.Write(sPageClosingTags & "</body></html>")
				Response.End
			end if
		end if
	End Function
%>