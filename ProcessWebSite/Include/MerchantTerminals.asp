<%
Const CCRT_None			= 0
Const CCRT_Terminal		= 1
Const CCRT_Country		= 2
Const CCRT_Bin			= 3
Const CCRT_BlackList	= 4
Const CCRT_WhiteList	= 5
Const CCRT_CPassMore	= 6
Const CCRT_CPassLess	= 7
Const CCRT_SelectGroup	= 8

Class CSendTrans
	Dim iRs
	Dim CCNumber, CCBinNumber, CCBinCountry, CCExpMM, CCExpYY, SendCount, TransGroup, CompanyID, TerminalNumber, LastTransID
	Public Sub Class_Initialize()
		Set iRs = Server.CreateObject("Adodb.Recordset")
	End Sub
	
	Public Sub Class_Terminate()
		Set iRs = Nothing
	End Sub
	
	Public Sub LoadTransMethodInfo(iPaymentMethod, iCrdID)
		If icrdID = 0 Then	'read from local vars
			CCBinCountry = ccBINCountry
			CCBinNumber = ccBIN
			CCExpMM = X_ccExpMM
			CCExpYY = X_ccExpYY
			CCNumber = X_ccNumber
		Else				'read from database
			iRs.Open "Select * From tblCreditCard Where ID=" & iCrdID, oleDbData, 0, 1
			If Not iRs.EOF Then
				CCBinCountry = iRs("BINCountry")
				CCBinNumber = iRs("CCard_First6")
				CCExpMM = iRs("ExpMM")
				CCExpYY = iRs("ExpYY")
			End if
			iRs.Close
		End if
	End Sub
	
	Public Function GetMerchantTerminal(ixCompanyID, ixPaymentMethod, ixCurrency, creditType, icrdID, RetTerminalNumber, RetCC_Options)
		SendCount = 0 : CompanyID = ixCompanyID
		RetCC_Options = ixCurrency
		LoadTransMethodInfo ixPaymentMethod, icrdID
		iRs.Open "Select * From tblCompanyPaymentMethods Where CPM_CompanyID=" & ixCompanyID & " And CPM_PaymentMethod=" & ixPaymentMethod & " And CPM_CurrencyID IN(255, " & ixCurrency & ") Order By CPM_CurrencyID Asc", oleDbData, 0, 1
		If iRs.EOF Then
			GetMerchantTerminal = -2
		Else
			If iRs("CPM_IsDisabled") Then GetMerchantTerminal = -1 _
			Else GetMerchantTerminal = iRs("CPM_ID")
		End If
		iRs.Close()
		If GetMerchantTerminal > 0 Then GetMerchantTerminal = CalcMerchantRestrictions(GetMerchantTerminal, 0, 0, RetTerminalNumber, RetCC_Options)
	End Function

	Public Function GetNextTerminal(RetTerminalNumber, nParentType, RetCC_Options)
		GetNextTerminal = -2
		If (nParentType = CCRT_SelectGroup) Then 
			If Request("TrmCode") = "" Then 
				GetNextTerminal = -3 : Exit Function
			End If	
			Do While Not iRs.EOF
				If iRs("DTID") = Request("TrmCode") Then
					RetTerminalNumber = iRs("CCR_TerminalNumber")
					RetCC_Options = TestNumVar(iRs("CCR_ExchangeTo"), 0, -1, RetCC_Options)
					GetNextTerminal = iRs("CCR_ID")
					Exit Do
				End If	
			 iRs.MoveNext
			Loop
		Else
			Dim nMinRatio : nMinRatio = 32768
			Do While Not iRs.EOF
				If iRs("CCR_Ratio") > 0 Then
					Dim nRatio
					If iRs("CCR_UseCount") = 0 Then nRatio = 0 Else nRatio = iRs("CCR_UseCount") / iRs("CCR_Ratio")
					If nRatio < nMinRatio Then
						RetTerminalNumber = iRs("CCR_TerminalNumber")
						RetCC_Options = TestNumVar(iRs("CCR_ExchangeTo"), 0, -1, RetCC_Options)
						GetNextTerminal = iRs("CCR_ID")
						If nRatio = 0 Then Exit Do Else nMinRatio = nRatio
					End If
				End If
			 iRs.MoveNext
			Loop
		End If	
		iRs.Close()
		oleDbData.Execute "Update tblCompanyCreditRestrictions Set CCR_UseCount = CCR_UseCount + 1 Where CCR_ID=" & GetNextTerminal
	End Function

	Private Sub AddToSwitchLog(nTransID)
		oleDbData.Execute "Insert Into tblLog_TerminalJump(ltj_GroupID, ltj_CompanyID, ltj_TransID, ltj_TransReply, ltj_DebitCompanyID, ltj_TerminalNumber, ltj_JumpIndex)Values(" & _
			TransGroup & "," & CompanyID & "," & nTransID & ",'" & Replace(X_DPRET, "'", "''") & "'," & debitCompany & ",'" & TerminalNumber & "'," & TestNumVar(SendCount, 0, -1, 0) & ")"
	End Sub 

	Public Function CalcMerchantRestrictions(nPMID, nParentID, nParentType, RetTerminalNumber, RetCC_Options)
		Dim nCCR_ID, iRs
		CalcMerchantRestrictions = -2
		Set iRs = Server.CreateObject("ADODB.Recordset")
		iRs.Open "Select tblCompanyCreditRestrictions.*, tblDebitTerminals.ID As DTID From tblCompanyCreditRestrictions " & _
			" Left Join tblDebitTerminals ON(tblCompanyCreditRestrictions.CCR_TerminalNumber = tblDebitTerminals.terminalNumber)" & _
			" Where CCR_CPM_ID=" & nPMID & " And CCR_ParentID=" & nParentID & " Order By CCR_Order Asc", oleDbData, 0, 1
		Do While Not iRs.EOF
			Select Case TestNumVar(iRs("CCR_Type"), 0, -1, 0)
			Case CCRT_None
				Exit Do
			Case CCRT_Terminal
				CalcMerchantRestrictions = GetNextTerminal(RetTerminalNumber, nParentType, RetCC_Options)
				If CalcMerchantRestrictions < 0 Then Exit Function
				If SendCount = 1 Then AddToSwitchLog(LastTransID)
				bValue = SendCharge()
				TerminalNumber = RetTerminalNumber : LastTransID = nTransactionID
				If SendCount > 0 Then AddToSwitchLog(nTransactionID) Else TransGroup = nTransactionID
				SendCount = SendCount + 1
				If bValue Then Exit Function Else bValue = True 'move to inner in case of fail
			Case CCRT_SelectGroup
				bValue = True
			Case CCRT_Country
				bValue = (InStr(1, iRs("CCR_ListValue"), binsCountry) > 0)
				If iRs("CCR_ChildType") = 1 Then bValue = Not bValue
			Case CCRT_Bin
				bValue = (InStr(1, iRs("CCR_ListValue"), CCBinNumber) > 0)
				If iRs("CCR_ChildType") = 1 Then bValue = Not bValue
			Case CCRT_BlackList
				bValue = (TestNumVar(ExecScalar("Select ID From tblBlackListBIN Where BIN='" & CCBinNumber & "'", 0), 0, -1, 0) > 0)
				If iRs("CCR_ChildType") = 1 Then bValue = Not bValue
			Case CCRT_WhiteList
				bValue = (TestNumVar(ExecScalar("Select ID From tblWhiteListBIN Where BIN='" & CCBinNumber & "'", 0), 0, -1, 0) > 0)
				If iRs("CCR_ChildType") = 1 Then bValue = Not bValue
			Case CCRT_CPassMore
				bValue = true
				If iRs("CCR_ChildType") = 1 Then bValue = Not bValue
			Case CCRT_CPassLess
				bValue = true
				If iRs("CCR_ChildType") = 1 Then bValue = Not bValue
			End Select
			If bValue Then
				nParentType = iRs("CCR_Type")
				nCCR_ID = iRs("CCR_ID")
				If iRs.State <> 0 Then iRs.Close()
				CalcMerchantRestrictions = CalcMerchantRestrictions(nPMID, nCCR_ID, nParentType, RetTerminalNumber, RetCC_Options)
				Exit Function
			End If
		  iRs.MoveNext
		Loop
		If iRs.State <> 0 Then iRs.Close()
	End Function
End Class

Public Function GetTransactionFeesEx(RatioChargeParam, ixCompanyID, ixPaymentMethod, ixOCurrency, ixAmount, ixTypeCredit, ixTerminalNumber)
	Dim rsFees
	sSQL = "Select CCR_PercentFee, CCR_RefundFixedFee, CCR_FixedFee, CPM_PercentFee, CPM_RefundFixedFee, CPM_FixedFee From tblCompanyCreditRestrictions Left Join tblCompanyPaymentMethods ON(tblCompanyCreditRestrictions.CCR_CPM_ID = tblCompanyPaymentMethods.CPM_ID) " & _
			"Where CPM_CompanyID=" & ixCompanyID & " And CPM_CurrencyID=" & ixOCurrency & " And CPM_PaymentMethod=" & ixPaymentMethod & _
			"And CCR_TerminalNumber IN(null, '" & ixTerminalNumber & "')"
	set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then
		If ixTypeCredit = 0 Then
			GetTransactionFeesEx = IIF(rsFees("CCR_RefundFixedFee") = 0, rsFees("CPM_RefundFixedFee"), rsFees("CCR_RefundFixedFee"))
			RatioChargeParam = 0
		Else
			GetTransactionFeesEx = IIF(rsFees("CCR_FixedFee") = 0, rsFees("CPM_FixedFee"), rsFees("CCR_FixedFee"))
			RatioChargeParam = FormatNumber(ixAmount * (IIF(rsFees("CCR_PercentFee") = 0, rsFees("CPM_PercentFee"), rsFees("CCR_PercentFee")) / 100), 4, True)
		End if
	Else
		RatioChargeParam = 0 : GetTransactionFeesEx = 0
	End if
	rsFees.Close
End Function

Function GetUsageFeesEx(ixCompanyID, ixPaymentMethod, ixOCurrency, binsCountry, ixTerminalNumber, bGetSource) '1=Faill, 2=Approval
	Dim rsFees
	GetUsageFeesEx = 0
	sSQL = "Select CCR_ApproveFixedFee, CCR_FailFixedFee, CPM_ApproveFixedFee, CPM_FailFixedFee From tblCompanyCreditRestrictions Left Join tblCompanyPaymentMethods ON(tblCompanyCreditRestrictions.CCR_CPM_ID = tblCompanyPaymentMethods.CPM_ID) " & _
			"Where CPM_CompanyID=" & ixCompanyID & " And CPM_CurrencyID=" & ixOCurrency & " And CPM_PaymentMethod=" & ixPaymentMethod & _
			" And CCR_TerminalNumber IN(null, '" & ixTerminalNumber & "')"
	Set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then 
		if bGetSource = 1 Then
			GetUsageFeesEx = IIF(rsFees("CCR_FailFixedFee") = 0, rsFees("CPM_FailFixedFee"), rsFees("CCR_FailFixedFee"))
		Elseif bGetSource = 2 Then
			GetUsageFeesEx = IIF(rsFees("CCR_ApproveFixedFee") = 0, rsFees("CPM_ApproveFixedFee"), rsFees("CCR_ApproveFixedFee"))
		End if
	End if	
	rsFees.Close
End Function

Function GetChbFeesEx(ixCompanyID, ixPaymentMethod, ixOCurrency, ixBin, ixTerminalNumber, refChb, retClrf)
	Dim rsFees
	GetChbFeesEx = False
	sSQL = "Select CCR_ClarificationFee, CCR_CBFixedFee, CPM_ClarificationFee, CPM_CBFixedFee From tblCompanyCreditRestrictions Left Join tblCompanyPaymentMethods ON(tblCompanyCreditRestrictions.CCR_CPM_ID = tblCompanyPaymentMethods.CPM_ID)" & _
			" Where CPM_CompanyID=" & ixCompanyID & " And CPM_CurrencyID IN(255, " & ixOCurrency & ") And CPM_PaymentMethod=" & ixPaymentMethod & _
			" And CCR_TerminalNumber IN(null, '" & ixTerminalNumber & "') Order By CPM_CurrencyID Asc"
	set rsFees = oledbData.execute(sSQL)
	If Not rsFees.EOF Then 
		GetChbFeesEx = True
		retClrf = IIF(rsFees("CCR_ClarificationFee") = 0, rsFees("CPM_ClarificationFee"), rsFees("CCR_ClarificationFee"))
		refChb = IIF(rsFees("CCR_CBFixedFee") = 0, rsFees("CPM_CBFixedFee"), rsFees("CCR_CBFixedFee"))
	End if
	rsFees.Close
End Function
%>