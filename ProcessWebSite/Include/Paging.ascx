<%@ Control Language="VB" ClassName="Paging" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<script runat="server">
	Private bShowPageFirst As Boolean = False
	Public Property ShowPageFirst() As Boolean
		Get
			Return bShowPageFirst
		End Get
		Set(ByVal value As Boolean)
			bShowPageFirst = value
		End Set
	End Property

	Private bShowPageLast As Boolean = False
	Public Property ShowPageLast() As Boolean
		Get
			Return bShowPageLast
		End Get
		Set(ByVal value As Boolean)
			bShowPageLast = value
		End Set
	End Property

	Private bShowPageNumber As Boolean = False
	Public Property ShowPageNumber() As Boolean
		Get
			Return bShowPageNumber
		End Get
		Set(ByVal value As Boolean)
			bShowPageNumber = value
		End Set
	End Property

	Private bShowPageCount As Boolean = False
	Public Property ShowPageCount() As Boolean
		Get
			Return bShowPageCount
		End Get
		Set(ByVal value As Boolean)
			bShowPageCount = value
		End Set
	End Property

	Private bShowRecordNumbers As Boolean = True
	Public Property ShowRecordNumbers() As Boolean
		Get
			Return bShowRecordNumbers
		End Get
		Set(ByVal value As Boolean)
			bShowRecordNumbers = value
		End Set
	End Property

	Private bShowRecordCount As Boolean = False
	Public Property ShowRecordCount() As Boolean
		Get
			Return bShowRecordCount
		End Get
		Set(ByVal value As Boolean)
			bShowRecordCount = value
		End Set
	End Property
	
	Private bShowPageNumbers As Boolean = False
	Public Property ShowPageNumbers() As Boolean
		Get
			Return bShowPageNumbers
		End Get
		Set(ByVal value As Boolean)
			bShowPageNumbers = value
		End Set
	End Property

	Private bShowPageNumbersRecords As Boolean = False
	Public Property ShowPageNumbersRecords() As Boolean
		Get
			Return bShowPageNumbersRecords
		End Get
		Set(ByVal value As Boolean)
			bShowPageNumbersRecords = value
		End Set
	End Property

	Private nShowPageNumbersRange As Integer = 3
	Public Property ShowPageNumbersRange() As Integer
		Get
			Return nShowPageNumbersRange
		End Get
		Set(ByVal value As Integer)
			If value > 0 Then
				nShowPageNumbersRange = value
			Else
				bShowPageNumbers = False
			End If
		End Set
	End Property
	
	Private sNoRecordsText As String = "No records found"
	Public Property NoRecordsText() As String
		Get
			Return sNoRecordsText
		End Get
		Set(ByVal value As String)
			sNoRecordsText = value
		End Set
	End Property

	Private nPageSize As Integer = 10
	Public Property PageSize() As Integer
		Get
			Return nPageSize
		End Get
		Set(ByVal value As Integer)
			nPageSize = value
		End Set
	End Property

	Private sPageID As String = "PageID"
	Public Property PageID() As String
		Get
			Return sPageID
		End Get
		Set(ByVal value As String)
			sPageID = value
		End Set
	End Property

	Private bGenerateFieldForID As Boolean = False
	Public Property GenerateFieldForID() As Boolean
		Get
			Return bGenerateFieldForID
		End Get
		Set(ByVal value As Boolean)
			bGenerateFieldForID = value
		End Set
	End Property

	Private sPagerCssClass As String = "pagerWhite"
	Public Property PagerCssClass() As String
		Get
			Return sPagerCssClass
		End Get
		Set(ByVal value As String)
			sPagerCssClass = value
		End Set
	End Property

	Private sPagerCssStyle As String = ""
	Public Property PagerCssStyle() As String
		Get
			Return sPagerCssStyle
		End Get
		Set(ByVal value As String)
			sPagerCssStyle = value
		End Set
	End Property

	Private bShowTable As Boolean = False
	Public Property ShowTable() As Boolean
		Get
			Return bShowTable
		End Get
		Set(ByVal value As Boolean)
			bShowTable = value
		End Set
	End Property

	Private sTableCssClass As String = "formNormal"
	Public Property TableCssClass() As String
		Get
			Return sTableCssClass
		End Get
		Set(ByVal value As String)
			sTableCssClass = value
		End Set
	End Property

	Private sTableCssStyle As String = ""
	Public Property TableCssStyle() As String
		Get
			Return sTableCssStyle
		End Get
		Set(ByVal value As String)
			sTableCssStyle = value
		End Set
	End Property

	Private sTableCustomTitles As String = ""
	Public Property TableCustomTitles() As String
		Get
			Return sTableCustomTitles
		End Get
		Set(ByVal value As String)
			sTableCustomTitles = value
		End Set
	End Property

	Private sZeroColor As String = ""
	Public Property ZeroColor() As String
		Get
			Return sZeroColor
		End Get
		Set(ByVal value As String)
			sZeroColor = value
		End Set
	End Property
	
	Private bTableWrapCells As Boolean = True
	Public Property TableWrapCells() As Boolean
		Get
			Return bTableWrapCells
		End Get
		Set(ByVal value As Boolean)
			bTableWrapCells = value
		End Set
	End Property

	Private bTableWrapTitles As Boolean = True
	Public Property TableWrapTitles() As Boolean
		Get
			Return bTableWrapTitles
		End Get
		Set(ByVal value As Boolean)
			bTableWrapTitles = value
		End Set
	End Property

	Private sForm As String = ""
	Public Property Form() As String
		Get
			Return sForm
		End Get
		Set(ByVal value As String)
			sForm = value
		End Set
	End Property

	Public PageIndex As Integer = 1, RowIndex As Integer = 0

	Public iSet As System.Data.DataSet
	Dim Public iTable As System.Data.DataTable
	
	Dim sTdLink As String = "<td align=""center"" onmouseover=""this.className='on';"" onmouseout=""this.className='';"""
	Dim sTdLinkDisabled As String = "<td align=""center"" style=""cursor:default;"">"
	Dim sTdDisplay As String = "<td>"
	Dim sTdCurrent As String = "<th>"

	Public ReadOnly Property FieldCount() As Integer
		Get
			Return iTable.Columns.Count
		End Get
	End Property
	
	Function FieldName(ByVal i As Integer) As String
		Return iTable.Columns(i).ColumnName
	End Function
	
	Public Sub OpenDataset(ByVal sSQL As String)
		'
		' 20080828 Tamir
		'
		' Count records and pages
		'
		lblPageFirst.Text = String.Empty
		lblPageLast.Text = String.Empty
		lblPagePrev.Text = String.Empty
		lblPageNext.Text = String.Empty
		Dim nRecordCount As Integer = 0, nPageCount As Integer = 0
		Dim sOnClick As String
		If bShowPageCount Or bShowPageLast Or bShowRecordCount Or bShowPageNumbers Then
			Dim sCountSQL As String = sSQL.ToUpper
			sCountSQL = "SELECT COUNT(*) " & sCountSQL.Substring(sCountSQL.IndexOf(" FROM "))
			If sCountSQL.IndexOf(" ORDER BY ") > 0 Then sCountSQL = sCountSQL.Substring(0, sCountSQL.IndexOf(" ORDER BY "))
			nRecordCount = dbPages.ExecScalar(sCountSQL)
			nPageCount = nRecordCount / nPageSize
			If nRecordCount - nPageCount * nPageSize > 0 Then nPageCount += 1
			If PageIndex > nPageCount Then PageIndex = 1
			If nPageCount > 3 Then
				If PageIndex > 1 Then
					If Form = String.Empty Then
						sOnClick = "location.href='?" & dbPages.SetUrlValue(Request.QueryString.ToString(), sPageID, 1) & "';"
					Else
						sOnClick = "with(document.forms['" & Form & "']){elements('" & sPageID & "').value=1; submit();}"
					End If
					lblPageFirst.Text = sTdLink & " onclick=""" & sOnClick & """>l&lt; FIRST PAGE</td>"
				End If
				If PageIndex < nPageCount Then
					If Form = String.Empty Then
						sOnClick = "location.href='?" & dbPages.SetUrlValue(Request.QueryString.ToString(), sPageID, nPageCount) & "';"
					Else
						sOnClick = "with(document.forms['" & Form & "']){elements('" & sPageID & "').value=" & nPageCount & "; submit();}"
					End If
					lblPageLast.Text = sTdLink & " onclick=""" & sOnClick & """>LAST PAGE &gt;l</td>"
				End If
			End If
		End If

		RowIndex = -1
		iSet = dbPages.ExecDataset(sSQL, nPageSize, PageIndex - 1)
		iTable = iSet.Tables(0)
		If iTable.Rows.Count = 0 And PageIndex > 1 Then
			PageIndex = 1
			iSet = dbPages.ExecDataset(sSQL, nPageSize, PageIndex - 1)
			iTable = iSet.Tables(0)
		End If
		If iTable.Rows.Count = 0 Then
			lblPageDisp.Text = "<td>" & sNoRecordsText & "</td>"
		Else
			If nPageSize >= iTable.Rows.Count And PageIndex = 1 Then
				lblPageDisp.Text = "<td>Showing records 1 - " & Math.Min(iTable.Rows.Count, nPageSize) & "</td>"
			Else
				lblPageDisp.Text = sTdCurrent
				If bShowPageNumber Then
					lblPageDisp.Text &= "Page " & PageIndex
					If bShowPageCount Then lblPageDisp.Text &= " of " & nPageCount
					If bShowRecordNumbers Then lblPageDisp.Text &= ", "
				End If
				If bShowRecordNumbers Then
					If bShowPageNumber Then lblPageDisp.Text &= "records "
					lblPageDisp.Text &= ((PageIndex - 1) * nPageSize) + 1 & "-" & ((PageIndex - 1) * nPageSize) + IIf(iTable.Rows.Count < nPageSize, iTable.Rows.Count, nPageSize)
					If bShowRecordCount Then lblPageDisp.Text &= " of " & nRecordCount
				End If
				lblPageDisp.Text &= "</th>"
			End If

			If PageIndex > 1 Then
				If bShowPageNumbers Then
					For i As Integer = Math.Max(1, PageIndex - nShowPageNumbersRange) To PageIndex - 1
						If Form = String.Empty Then
							sOnClick = "location.href='?" & dbPages.SetUrlValue(Request.QueryString.ToString(), sPageID, i) & "';"
						Else
							sOnClick = "with(document.forms['" & Form & "']){elements('" & sPageID & "').value=" & i & "; submit();}"
						End If
						lblPagePrev.Text &= sTdLink & " onclick=""" & sOnClick & """>" & IIf(bShowPageNumbersRecords, ((i - 1) * nPageSize + 1).ToString & "-" & (i * nPageSize).ToString, i.ToString) & "</td>"
					Next
				Else
					If Form = String.Empty Then
						sOnClick = "location.href='?" & dbPages.SetUrlValue(Request.QueryString.ToString(), sPageID, PageIndex - 1) & "';"
					Else
						sOnClick = "with(document.forms['" & Form & "']){elements('" & sPageID & "').value=" & PageIndex - 1 & "; submit();}"
					End If
					lblPagePrev.Text = sTdLink & " onclick=""" & sOnClick & """>&lt;&lt; PREV PAGE</td>"
				End If
			End If
			If nPageSize < iTable.Rows.Count Then
				If bShowPageNumbers Then
					For i As Integer = PageIndex + 1 To Math.Min(nPageCount, PageIndex + nShowPageNumbersRange)
						If Form = String.Empty Then
							sOnClick = "location.href='?" & dbPages.SetUrlValue(Request.QueryString.ToString(), sPageID, i) & "';"
						Else
							sOnClick = "with(document.forms['" & Form & "']){elements('" & sPageID & "').value=" & i & "; submit();}"
						End If
						lblPageNext.Text &= sTdLink & " onclick=""" & sOnClick & """>" & IIf(bShowPageNumbersRecords, (i - 1) * nPageSize + 1 & "-" & Math.Min(i * nPageSize, nRecordCount), i) & "</td>"
					Next
				Else
					If Form = String.Empty Then
						sOnClick = "location.href='?" & dbPages.SetUrlValue(Request.QueryString.ToString(), sPageID, PageIndex + 1) & "';"
					Else
						sOnClick = "with(document.forms['" & Form & "']){elements('" & sPageID & "').value=" & PageIndex + 1 & "; submit();}"
					End If
					lblPageNext.Text = sTdLink & " onclick=""" & sOnClick & """>NEXT PAGE &gt;&gt;</td>"
				End If
			End If
		End If
	End Sub
	
	Public Sub CloseDataset()
		iTable.Dispose()
		iSet.Dispose()
	End Sub
	Default Public ReadOnly Property Item(ByVal Index As Integer) As Object
		Get
			Return iTable.Rows(RowIndex)(Index)
		End Get
	End Property
	Default Public ReadOnly Property Item(ByVal sKey As String) As Object
		Get
			Return iTable.Rows(RowIndex)(sKey)
		End Get
	End Property
	Public Function Read() As Boolean
		RowIndex += 1
		Return ((RowIndex < nPageSize) And (RowIndex < iTable.Rows.Count))
	End Function
		
	Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs)
		If Trim(Request("Source")) = "Filter" Then PageIndex = 1
		If Form <> "" Then
			PageIndex = dbPages.TestVar(CType(Page.Form.FindControl(sPageID), HiddenField).Value, 1, 9999, 1)
			sPageID = Page.Form.FindControl(sPageID).ClientID
		Else
			PageIndex = dbPages.TestVar(Page.Request(sPageID), 1, 9999, 1)
		End If
	End Sub
</script>
<%
	If bShowTable Then
		Dim sTitle As String
		Response.Write("<table class=""" & sTableCssClass & """ style=""" & sTableCssStyle & """>")
		Response.Write("<tr>")
		For i As Integer = 0 To FieldCount - 1
			sTitle = String.Empty
			If sTableCustomTitles.Contains("|") Then
				If sTableCustomTitles.Split("|").Length > i Then sTitle = sTableCustomTitles.Split("|")(i)
			End If
			If sTitle = String.Empty Then sTitle = FieldName(i)
			Response.Write("<th>" & sTitle & "</th>")
		Next
		Response.Write("</tr>")
		Dim sValue As String
		While Read()
			Response.Write("<tr>")
			For i As Integer = 0 To FieldCount - 1
				sValue = Item(i).ToString
				If IsNumeric(sValue) And Not sValue.Contains("000") Then
					If Convert.ToDouble(sValue) = 0 Then sValue = "<span style=""color:" & sZeroColor & ";"">" & sValue & "</span>"
				End If
				Response.Write("<td" & IIf(bTableWrapCells, String.Empty, " nowrap") & ">" & sValue & "</td>")
			Next
			Response.Write("</tr>")
			Response.Write("<tr><td height=""1"" colspan=""" & FieldCount & """  bgcolor=""silver""></td></tr>")
		End While
		Response.Write("</table>")
		Response.Write("<br />")
	End If
%>
<table cellpadding="2" cellspacing="1" border="0" class="<%= sPagerCssClass %>" style="<%= sPagerCssStyle %>">
<tr>
	<asp:Label ID="lblPageFirst" runat="server" />
	<asp:Label ID="lblPagePrev" runat="server" />
	<asp:Label ID="lblPageDisp" runat="server" />
	<asp:Label ID="lblPageNext" runat="server" />
	<asp:Label ID="lblPageLast" runat="server" />
</tr>
</table>
<%= IIf(String.IsNullOrEmpty(Form) Or String.IsNullOrEmpty(PageID) or not bGenerateFieldForID, string.Empty, "<input type=""hidden"" name=""" & PageID & """ />") %>