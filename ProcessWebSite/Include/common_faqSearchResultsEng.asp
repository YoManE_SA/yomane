<br>
<script type="text/javascript">
	function changeCat()
	{
		if(document.catForm.faqCat.value=='*')
		{
			alert('Please select a category from the list')
			return false;
		}
	}
	function checkSearch()
	{
		if(document.searchForm.searchText.value=='')
		{
			alert('Please type a word to search')
			return false;
		}
	}
	function toggleAnswer(objId)
	{	
	    objRef = document.getElementById(objId);

	    if (objRef.style.display == "none")
	    {
	        objRef.style.display = ""; // browser default
	    }
	    else
	    {
	        objRef.style.display = "none";
	    }
	}
</script>
<table width="90%" dir="ltr" align="center" border="0" cellspacing="0" cellpadding="1">
<tr>
	<td colspan="2" class="txt13b" style="color:#FF8000;">
		FREQUENTLY ASKED QUESTIONS
	</td>	
</tr>
<tr><td><br></td></tr>
<tr>
	<td align="left" class="txt12" style="color:#003366;">
		Browse<br>
	</td>
	<td align="left" class="txt12" style="color:#003366;">
		Search<br>
	</td>
</tr>
<tr>
	<form action="faq.asp" name="catForm" id="catForm" onsubmit="return changeCat();">
	<td align="left">
	    <%
        searchText = dbText(request("searchText"))
		
		if dbText(request("faqCat")) <> "" then faqCat = dbText(request("faqCat"))
		if cStr(faqCat) <> "*" then faqCatSql = "categoryID = " & faqCat & " AND"
		
		sql = "SELECT * FROM tblFaqCategory WHERE IsVisible=1 AND IsContent=1 "
		if not SHOW_HEBREW then sql = sql & " AND Lang<>'he' "
		set rsData6 = oledbData.execute(sql)     
		
		response.write "<select name=""faqCat"" class=""inputdata"">"
		response.write "<option value=""*"">Select category</option>"
		
		while not rsData6.eof
			response.write "<option value=""" & rsData6("ID") & """>" & rsData6("Name") & "</option>"
		rsData6.moveNext
		wend 
		
		response.write "</select> <input class=""button1"" name=""catGo"" type=""submit"" value=""Go"" />"
	    %>
	</td>	
	</form>
	<form action="faqSearchResults.asp" name="searchForm" id="searchForm" onSubmit="return checkSearch();">
	<td class="txt13b" valign="top" align="left" width="160">
		<input class="inputdata" style="width:100px;" type="text" name="searchText" value="<%= searchText %>" /> <input class="button1" name="searchGo" style="width:50px;" type="submit" value="Search" />
	</td>    
	</form>
</tr>	
<tr>
	<td colspan="2" align="left" class="txt12b" dir="ltr">
		<br><br>SEARCH RESULT
	</td>
</tr>
</table>
<br>
<table width="90%" dir="ltr" align="center" border="0" cellspacing="0" cellpadding="1">
<%
if searchText <> "" then searchSql = " (tblFaq.question LIKE '%" & searchText & "%' OR tblFaq.answer LIKE '%" & searchText & "%') AND "
 
sql = "SELECT tblFaq.*, tblFaqCategory.Lang, tblFaqCategory.Name FROM tblFaq LEFT OUTER JOIN tblFaqCategory ON tblFaq.categoryID = tblFaqCategory.id WHERE " & searchSql & " tblFaqCategory.IsVisible=1 AND tblFaqCategory.IsContent=1 AND tblFaq.IsVisible=1 "
if not SHOW_HEBREW then sql = sql & " AND tblFaqCategory.Lang<>'he' "
sql = sql & " ORDER BY tblFaq.rating"
set rsData5 = oledbData.execute(sql)    

if rsData5.eof then 
	response.Write "<tr><td class='txt12'>No records found</td></tr>"
else
	counter = 1
	while not rsData5.eof
		answer = rsData5("answer")
		response.write "<tr><td width=""6%"" rowspan=""3"" valign=""top"" class=""txt11"">" & counter & ".</td>"
		response.write "<td class=""txt11"" style=""color:gray;""><a href=javascript:toggleAnswer('ans" & counter & "') class=""faq_eng"">" & rsData5("question") & "</a> (" & rsData5("Name") & " )</td></tr>"
		response.write "<tr><td height=""4""></td></tr>"
		response.write "<tr style='display:none;' id='ans" & counter & "'><td class='txt11'>" & answer & "<br /><br /></td></tr>" 
		counter = counter + 1
	rsData5.moveNext
	wend                  
end if              
%>
<tr><td><br></td></tr>	
</table>