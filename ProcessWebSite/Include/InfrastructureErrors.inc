﻿<%

	Const Success = 0
	'bellow 1000 is save for legacy errors
	'************************System 1XXX **********************//

	'password & pin code 11XX
	Const PasswordWeak = 1151
	Const PasswordWrong = 1152
	Const PasswordUsedInThePast = 1153
	Const PasswordConfirmationMissmatch = 1154

	Const PinCodeWeak = 1161
	Const PinCodeWrong = 1162
	Const PinCodeConfirmationMissmatch = 1163

	'user managment 12XX
	Const Invalid_Email = 1201
	Const EmailAlreadyExist = 1202
	Const Invalid_UserName = 1203
	Const UserNameAlreadyExist = 1204
	Const Invalid_Name = 1205
	Const Invalid_FirstName = 1206
	Const Invalid_LastName = 1207
	Const Invalid_SSN = 1208
	Const Invalid_Phone = 1209

	'general 15XX
	Const Invalid_Data = 1501
	Const Invalid_ID = 1502
	Const Invalid_Date = 1503


	'************************components 2XXX **********************//

	'address 21XX
	Const Invalid_Country = 2100
	Const Invalid_State = 2101
	Const Invalid_Address = 2102
	Const Invalid_PostalCode = 2103
	Const Invalid_City = 2104

	'payment 22XX
	Const Invalid_Currency = 2201
	Const Invalid_PaymentMethod = 2202
	Const Invalid_Amount = 2203
	Const Invalid_Merchant = 2204
	Const Invalid_Customer = 2206

	'payment method 23XX
	Const Invalid_AccountValue1 = 2301 'account
	Const Invalid_AccountValue2 = 2302 'cvv, routing
	Const Invalid_ExpDate = 2303
	Const Invalid_OwnerName = 2304

%>