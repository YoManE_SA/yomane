<%
' 20090709 Tamir - Initialize the TimeString
Class TimeControlClass
	Private sMyTimeString

	Public StartTimer

	Public Timeout

	Public Function Append(sText)
		Dim nTimer : nTimer = Timer()
        If nTimer < StartTimer Then nTimer = StartTimer + nTimer 'midnight overlap
		Dim nDiff : nDiff = nTimer - StartTimer
		sMyTimeString = sMyTimeString & sText & "=" & FormatNumber(nDiff, 3, true) & "|"
		If nDiff > Timeout Then Append = False Else Append = True
	End Function
	
	Public Property Get TimeString
		TimeString=sMyTimeString
	End Property
	
	Public Sub Delay(nMilliseconds)
		Call NetpayCryptoObj.Delay(nMilliseconds)
	End Sub
End Class
Dim TimeControl : Set TimeControl=New TimeControlClass
TimeControl.StartTimer=Timer()
TimeControl.Timeout=30000
%>