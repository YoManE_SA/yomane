<%
Function PutComboDefault(sName, sValueList, sDisplayList, sSeparator, sDefault)
	
	arrayValueList = split(trim(sValueList),sSeparator)
	arrayDisplayList = split(trim(sDisplayList),sSeparator)

	response.write "<select dir=""rtl"" name=""" & sName & """>"

	for i=0 to ubound(arrayValueList)
		sSelected=""
		if trim(arrayValueList(i))=trim(sDefault) then
			sSelected=" selected"
		end if	
		response.write "<option value=""" & arrayValueList(i) & """"  & sSelected & ">" & arrayDisplayList(i) & "</option>"
	next
		
	response.write "</select>"
	
End Function

'Try to move all to the funcion below (PutRecordsetCombo)
Function PutRecordsetComboDefault(rsList,selectName,selectExtras,optionValue,optionText,defaultOptionValue,defaultOptionText)
	
	Dim sSelected
	response.write "<select name=""" & selectName & """ id=""" & selectName & """ " & selectExtras & ">"
	If defaultOptionValue<>"false" AND defaultOptionText<>"false" Then
		response.write "<option value=""" & defaultOptionValue & """>" & defaultOptionText & "</option>"
	End if
	If not (rsList.Eof and rsList.BOF) Then 
		rsList.MoveFirst
		Do until rsList.EOF
			sSelected = ""
			If trim(rsList(optionValue))=trim(Request(selectName)) Then
				sSelected = " selected"
			End if
			response.write "<option value=""" & rsList(optionValue) & """"  & sSelected & ">" & rsList(optionText) & "</option>"
		rsList.MoveNext
		Loop
	End if
	response.write "</select>"

End Function

Function PutRecordsetCombo(rsList,selectName,selectExtras,optionValue,optionText,defaultOptionValue,defaultOptionText,selectedValue)
	
	Dim sSelected
	response.write "<select name=""" & selectName & """ id=""" & selectName & """ " & selectExtras & ">"
	If defaultOptionValue <> "false" AND defaultOptionText <> "false" Then
		response.write "<option value=""" & defaultOptionValue & """>" & defaultOptionText & "</option>"
	End if
	If not (rsList.Eof and rsList.BOF) Then 
		rsList.MoveFirst
		Do until rsList.EOF
			sSelected = ""
			If selectedValue <> "false" Then
			    If trim(rsList(optionValue)) = trim(selectedValue) Then sSelected = " selected"
			End if
			response.write "<option value=""" & rsList(optionValue) & """"  & sSelected & ">" & rsList(optionText) & "</option>"
		rsList.MoveNext
		Loop
	End if
	response.write "</select>"

End Function

Function PutComboDayList(selectName,selectExtras,defaultOptionValue,defaultOptionText,selectedValue)

	response.write "<select name=""" & selectName & """ id=""" & selectName & """ " & selectExtras & ">"
	If defaultOptionValue <> "false" AND defaultOptionText <> "false" Then
		response.write "<option value=""" & defaultOptionValue & """>" & defaultOptionText & "</option>"
	End if
	For i=1 to 31
		If i<10 Then i="0" & i
	    sSelected = ""
	    If selectedValue <> "false" Then
	        If i = trim(selectedValue) Then sSelected = " selected"
	    End if
		response.write "<option value=""" & i & """" & sSelected & ">" & i & "</option>"
	Next
	response.write "</select>"
	
End Function
%>
