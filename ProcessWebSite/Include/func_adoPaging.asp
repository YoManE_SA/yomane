<%
'Open Recordset for Paging
Function openRsDataPaging(pageSizeValue)
	openRsDataPagingEx pageSizeValue, Request
End Function

Function openRsDataPagingEx(pageSizeValue, Rq)
	rsData.Open sSQL, oledbData, 3, 1
	session("PageSize") = pageSizeValue
	rsData.PageSize = session("PageSize")
	session("ActualPage") = 1
	If trim(Rq("page"))<>"" then
		If isNumeric(trim(Rq("page"))) then
			If int(trim(Rq("page"))) <= rsData.PageCount then
				session("ActualPage") = trim(Rq("page"))
			End if
		End if
	End if
End Function

Function showPaging(pageUrl, lang, queryString)
	If lang = "Eng" Then
		sStyle = "direction:ltr;"
		sMsgAlign = "left"
		sButtonsAlign = "right"
		sMsgTxt1 = "Page"
		sMsgTxt2 = "of"
	Else
		sStyle = "direction:rtl;"
		sMsgAlign = "right"
		sButtonsAlign = "left"
		sMsgTxt1 = "��"
		sMsgTxt2 = "����"
	End if
	
	if rsData.PageCount>1 then
		%>
		<table width="100%" align="center" border="0" cellpadding="1" cellspacing="0" style="<%= sStyle %>">
		<tr>
			<td align="<%= sMsgAlign %>" class="txt15" style="<%= sStyle %>">
				 <%= sMsgTxt1 & " <span class=""txt14"">" & session("ActualPage") & "</span> " & sMsgTxt2 & " <span class=""txt14"">" & rsData.PageCount & "</span><br>" %>
			</td>
			<td>
				<table border="0" align="<%= sButtonsAlign %>" cellspacing="1" cellpadding="0" style="<%= sStyle %>">
				<tr>
					<%
					if session("ActualPage") > 1 then
						%>
						<td class="paging_PageNevigation alwaysActive" onmouseout="this.style.border='1px solid #d7d7d7'"
						 	onmouseover="this.style.border='1px solid black'"
						 	onclick="location.href='<%= pageUrl %>?page=1<%= queryString %>';">&lt;&lt;</td>
						<td class="paging_PageNevigation alwaysActive" onmouseout="this.style.border='1px solid #d7d7d7'"
							onmouseover="this.style.border='1px solid black'" 
							onclick="location.href='<%= pageUrl %>?&page=<%= session("ActualPage")-1 & queryString %>';">&lt;</td>
						<%
					end if
					for nPage=1 to rsData.PageCount
						if int(nPage) >= int(session("ActualPage"))-5 AND int(nPage) <= int(session("ActualPage"))+5 then
							if trim(nPage) = trim(session("ActualPage")) then
								response.write "<td class=""paging_currentPage"">" & nPage & "</td>"
							Else
								response.write "<td class=""alwaysActive paging_otherPages"" onmouseout=""this.style.border='1px solid #d7d7d7'"" onmouseover=""this.style.border='1px solid black'""  onclick=""location.href='" & pageUrl & "?page=" & nPage & queryString & "';"">" & nPage & "</td>"
							end if
						end if
					next
					if int(session("ActualPage")) < rsData.PageCount then
						%>
						<td class="paging_PageNevigation alwaysActive" onmouseout="this.style.border='1px solid #d7d7d7'" 
							onmouseover="this.style.border='1px solid black'" 
							onclick="location.href='<%= pageUrl %>?page=<%= session("ActualPage")+1 & queryString %>';">&gt;</td>
						<td class="paging_PageNevigation alwaysActive" onmouseout="this.style.border='1px solid #d7d7d7'" 
							onmouseover="this.style.border='1px solid black'" 
							onclick="location.href='<%= pageUrl %>?page=<%= rsData.PageCount & queryString %>';">&gt;&gt;</td>
						<%
					end if
					%>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<%
	End If
End Function
%>