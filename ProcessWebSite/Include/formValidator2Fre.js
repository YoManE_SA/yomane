	// text validation object
	function textValidation(instance, friendlyName, type, minLength, maxLength, isRequired)
	{
		this.errMsg = "";

		// check if the instance exists
		if(instance == null)
		{
			alert(friendlyName + " doesn't seem to exist on this page");
			return;
		}
		
		this.getInstance = function()
		{
			return instance;
		}

		this.eventHandler = function()
		{	
			var keyCode = event.keyCode;
			if(keyCode >= 96 && keyCode <= 105)
			{
				keyCode = keyCode - 48 ;
			}			
			
			var key = String.fromCharCode(keyCode);

            // enable allowed keys 
			if(isAllowedKey(keyCode, event.ctrlKey))
			{
				return true;
			}

			// disable input that exceeds max length
			if(instance.value.length == maxLength)
			{		
				return false;
			}	
					
			// enable only numeric input
			if(type == "numeric")
			{
				// enable dot
				if(keyCode == 46 || keyCode == 110 || keyCode == 190)
				{
				    return true;
				}
				
				// enable numbers
				var reg = new RegExp("[0-9]");
				return (reg.test(key));
			}
			
			return true;		
		}
		
		instance.onkeydown = this.eventHandler;
	
		this.validate = function()
		{
			if(instance.value == "" && isRequired)
			{
				this.errMsg = friendlyName + " est obligatoire";
				
				return false;			
			}
	
			if(instance.value != "" && instance.value.length < minLength)
			{
				this.errMsg = friendlyName + " doit être au moins " + minLength + " caractères";
                
				return false;	
			}
			
			if(instance.value.length > maxLength)
			{
				this.errMsg = friendlyName + " ne peut pas dépasser " + maxLength + " caractères";

				return false;	
			}	

			if(instance.value.length > 0)
			{
				switch(type)
				{
					case "email":
						var reg = new RegExp("^[\\w-_\.]*[\\w-_\.]\@[\\w]\.+[\\w]+[\\w]$" );
						if(! reg.test(instance.value))
						{
							this.errMsg = friendlyName + " est invalide";
							
							return false
						}
						
						break;
					default:
						break;
				}
			}
		
			return true;
		}
	}
	
	// validation object (deprecated)
	function validationObj(instance, friendlyName, type, minLength, maxLength)
	{
		this.errMsg = "";
		
		// check if the instance exists
		if(instance == null)
		{
			alert(friendlyName + " doesn't seem to exist on this page");
			return;
		}
		
		this.getInstance = function()
		{
			return instance;
		}

		this.eventHandler = function()
		{
			var keyCode = event.keyCode;
			if(keyCode >= 96 && keyCode <= 105)
			{
				keyCode = keyCode - 48 ;
			}			
			
			var key = String.fromCharCode(keyCode);

            // enable allowed keys  
			if(isAllowedKey(keyCode, event.ctrlKey))
			{
				return true;
			}

			// disable input that exceeds max length
			if(instance.value.length == maxLength)
			{		
				return false;
			}	
					
			// enable only numeric input
			if(type == "numeric")
			{
				// enable dot
				if(keyCode == 46 || keyCode == 110 || keyCode == 190)
				{
				    return true;
				}
				
				// enable numbers
				var reg = new RegExp("[0-9]");
				return (reg.test(key));
			}
			
			return true;	
		}
		
		instance.onkeydown = this.eventHandler;
	
		this.validate = function()
		{
			if(instance.value == "" && minLength > 0)
			{
				this.errMsg = friendlyName + " est obligatoire";
				
				return false;			
			}
			
			if(instance.value.length > maxLength)
			{
				this.errMsg = friendlyName + " ne peut pas dépasser " + maxLength + " caractères";

				return false;	
			}	
					
			if(instance.value.length < minLength)
			{
				this.errMsg = friendlyName + " doit être au moins " + minLength + " caractères";

				return false;	
			}

			switch(type)
			{
				case "email":
					var reg = new RegExp("^[\\w-_\.]*[\\w-_\.]\@[\\w]\.+[\\w]+[\\w]$" );
					if(! reg.test(instance.value))
					{
						this.errMsg = friendlyName + " est invalide";
						
						return false
					}
					
					break;
				default:
					break;
			}
		
			return true;
		}
	}

	// check box validation object
	function checkBoxValidation(instance, errMsg)
	{
		this.errMsg = errMsg;	
		
		// check if the instance exists
		if(instance == null)
		{
			alert(errMsg + " doesn't seem to exist on this page");
			return;
		}
		
		this.getInstance = function()
		{
			return instance;
		}
		
		this.getValue = function()
		{			
			return instance.checked;
		}		

		this.validate = function()
		{
			return this.getValue();
		}
	}	
	
	// select validation object
	function selectValidation(instance, friendlyName)
	{
		this.errMsg = "";	
		
		// check if the instance exists
		if(instance == null)
		{
			alert(friendlyName + " doesn't seem to exist on this page");
			return;
		}
		
		this.getInstance = function()
		{
			return instance;
		}
		
		this.getValue = function()
		{			
			return instance.value;
		}		

		this.validate = function()
		{
			if (this.getValue() == "")
			{
				this.errMsg = friendlyName + " est obligatoire";
				
				return false;				
			}	
			
			return true;
		}
	}
	
	// cc date validation object
	function ccDateValidation(mmInstance, yyInstance)
	{
		this.errMsg = "";
		
		// check if the instance exists
		if(mmInstance == null || yyInstance == null)
		{
			alert("cc date instance doesn't seem to exist on this page");
			return;
		}
		
		this.getInstance = function()
		{
			return mmInstance;
		}
		
		this.getValue = function()
		{			
			// month set is one month back, so this month will still be valid
			return new Date(yyInstance.value, (mmInstance.value), 1);
		}		

		this.validate = function()
		{
			if (this.getValue() < new Date())
			{
				this.errMsg = "La carte est périmée";
				
				return false;				
			}		
			
			return true;
		}
	}
	
	// cc validation object
	function ccValidation(instanceBase, friendlyName)
	{
		this.errMsg = "";
		
		this.instance = new Array(4);
		for(var i = 0; i <= 3; i++)
		{
			this.instance[i] = eval(instanceBase + (i + 1))
			
			// check if the instance exists
		    if(this.instance[i] == null)
		    {
			    alert(friendlyName + " doesn't seem to exist on this page");
			    return;
		    }
		}

		this.eventHandler = function()
		{
			var keyCode = event.keyCode;
			if(keyCode >= 96 && keyCode <= 105)
			{
				keyCode = keyCode - 48 ;
			}			
			
			var key = String.fromCharCode(keyCode);

            // enable allowed keys 
			if(isAllowedKey(keyCode, event.ctrlKey))
			{
				return true;
			}
					
			// enable only numeric input
			var reg = new RegExp("[0-9]");
			return (reg.test(key));
			
			return true;
		}
		
		// bind event handler
		for(var i = 0; i <= 3; i++)
		{
			this.instance[i].onkeydown = this.eventHandler;
		}
		
		this.getInstance = function()
		{
			return this.instance[0];
		}
		
		this.getValue = function()
		{
			val = "";
			for(var i = 0; i <= 3; i++)
			{
				val += this.instance[i].value; 
			}
			
			return val;
		}		
	
		this.validate = function()
		{
			if (this.getValue() == "")
			{
				this.errMsg = friendlyName + " est obligatoire";
				
				return false;				
			}		
			
			return true;
		}
	}
	
	// validation manager object
	function validationManager()
	{
		this.container = new Array();

		this.add = function(validationObject)
		{
			if(validationObject.getInstance != null)
			{
			    this.container.push(validationObject);
			}
		}
		
		this.validate = function()
		{
			isValid = true;
			errMsg = "";
			
			for(var i = 0; i < this.container.length; i++)
			{
				currentObj = this.container[i];

				if(! currentObj.validate())
				{
					isValid = false;
					errMsg += currentObj.errMsg + "\n";
					
					// start workaround to return only the first err and focus the instance
					// remove this to return a list of errs
					currentObj.getInstance().focus();
					alert(errMsg);
					
					return false;
					// end workaround
				}
			}
			
			if(isValid)
			{
				return true;
			}
			else
			{
				alert(errMsg);
				return false;
			}
		}
	}
	
	// returns if the key is allowed
	function isAllowedKey(keyCode, isCtrl)
	{
		if (isCtrl)
		{
		    // check for ctrl keys, such as control paste
		    allowedKeyArr = new Array();
		    allowedKeyArr.push(86);	// ctrl	v	
		    allowedKeyArr.push(88);	// ctrl	x
		    allowedKeyArr.push(67);	// ctrl	c	
            allowedKeyArr.push(65);	// ctrl	a
            
		    for (i = 0; i < allowedKeyArr.length; i++)
		    {
		        if (keyCode == allowedKeyArr[i])
		        {
		            return true; 
		        }
		    }			
		}
		else
		{
		    // check for single keys
		    allowedKeyArr = new Array();
		    allowedKeyArr.push(8); // backspace
		    allowedKeyArr.push(9); // tab
		    allowedKeyArr.push(46); // delete
		    allowedKeyArr.push(27); // escape
		    allowedKeyArr.push(37); // left arrow
		    allowedKeyArr.push(39);	// right arrow			

		    for (i = 0; i < allowedKeyArr.length; i++)
		    {
		        if (keyCode == allowedKeyArr[i])
		        {
		            return true; 
		        }
		    }		
		}

		return false;
	}
	
	
	function findPosX(obj)
    {
	    var curleft = 0;
	    if (obj.offsetParent)
	    {
		    while (obj.offsetParent)
		    {
			    curleft += obj.offsetLeft
			    obj = obj.offsetParent;
		    }
	    }
	    else if (obj.x)
	    {
	        curleft += obj.x;
	    }
		    
	    return curleft;
    }

    function findPosY(obj)
    {
	    var curtop = 0;
	    if (obj.offsetParent)
	    {
		    while (obj.offsetParent)
		    {
			    curtop += obj.offsetTop
			    obj = obj.offsetParent;
		    }
	    }
	    else if (obj.y)
	    {
	        curtop += obj.y;
	    }
		
	    return curtop;
    }
    
