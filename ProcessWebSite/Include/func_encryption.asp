'Dim CM, Context, Exp1Key, Blob
'Set CM = Nothing
'Function LoadEncKey(xKeyName) 'pCreditKey = LoadEncKey("SecretLocation")
'	If CM Is Nothing Then
'		Set CM = Server.CreateObject("Persits.CryptoManager")
'		Set Context = CM.OpenContext("", True)
'		Set Exp1Key = Context.CreateExponentOneKey
'		Set Blob = CM.CreateBlob
'	End if
'	Blob.LoadFromRegistry &H80000002, "Software\Netpay\AspEncrypt", xKeyName '"SecretLocation"
'	Set LoadEncKey = Context.ImportKeyFromBlob( Exp1Key, Blob, cbtSimpleBlob )
'End Function
'
'Function EncData(pKey, xData)
'	Set Blob = pKey.EncryptText(xData)
'	EncData = Blob.Base64
'End Function
'
'Function EncDataOnce(xKeyName, xData) 'EncDataOnce("SecretLocation", card)
'	Dim pKey
'	Set pKey = LoadEncKey(xKeyName)
'	EncDataOnce = EncData(pKey, xData)
'	Set pKey = Nothing
'End Function
'
'Function DecData(pKey, xData) 'DecData(pCreditKey, card)
'	if IsNull(xData) Then Exit Function
'	Blob.Base64 = xData
'	DecData = pKey.DecryptText(Blob)
'End Function
'
'Function DecDataOnce(xKeyName, xData) 'DecDataOnce("SecretLocation", card)
'	Dim pKey
'	if IsNull(xData) Or xData = "" Then DecDataOnce = "" : Exit Function
'	Set pKey = LoadEncKey(xKeyName)
'	DecDataOnce = DecData(pKey, xData)
'	Set pKey = Nothing
'End Function

Const EncCodeWord = "blackhorse"
Function EncCVV(ByVal CvIn)
    Dim xChar, i
    CvIn = GetRandomNum(3) & trim(CvIn)
    For i = 1 To Len(CvIn)
		xChar = Mid(CvIn, i, 1)
		if (Asc(xChar) >= 48) And (Asc(xChar) <= 57) Then _
	        EncCVV = EncCVV & Mid(EncCodeWord, CInt(xChar) + 1, 1)
    Next
End Function

Function GetCodeValue(ch)
	Dim i
	For i = 1 To Len(EncCodeWord)
		if Mid(EncCodeWord, i, 1) = ch Then
			GetCodeValue = i - 1 : Exit Function
		End if
	Next
	GetCodeValue = "#"
End Function

Function DecCVV(CvIn)
    Dim i
	If CvIn = "" Then 
		DecCVV = ""
		Exit Function
	End If
	CvIn = trim(CvIn)
    For i = 4 To Len(CvIn)
        DecCVV = DecCVV & GetCodeValue(Mid(CvIn, i, 1))
	Next
End Function

Function CreateCompanySecurityKey()
	Dim sRet, strKeyChars
	strKeyChars = "ABCDEFGHIGKLMNOPQRSTUVWXYZ0123456789"
	While Len(sRet) < 12
		sRet = sRet & Mid(strKeyChars, CInt(Rnd() * Len(strKeyChars)) + 1, 1)
	Wend
	CreateCompanySecurityKey = sRet
End Function

Function GetEncrypted256(sText)
	if sText="" then
		GetEncrypted256=""
	else
		GetEncrypted256=ExecScalar("SELECT dbo.GetEncrypted256('" & Replace(trim(sText), "'", "''") & "')", "")
	end if
End Function

Function GetDecrypted256(sText)
	if sText="" then
		GetDecrypted256=""
	else
		GetDecrypted256=ExecScalar("SELECT dbo.GetDecrypted256('" & Replace(trim(sText), "'", "''") & "')", "")
	end if
End Function
