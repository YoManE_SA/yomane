﻿<%@ Page Language="VB" %>
<%@ Import Namespace="System.Net" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server" type="text/VB">
	Protected Sub Send_Click(sender As Object, e As System.EventArgs)
		Dim webRequest As WebRequest  = WebRequest.Create(txtUrl.Text)
		webRequest.Timeout = 180 * 1000
		Dim webResponse As HttpWebResponse = CType(webRequest.GetResponse(), HttpWebResponse)
		Response.Write("Ret Http Code:" & webResponse.StatusCode)
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<asp:TextBox runat="server" id="txtUrl" />
		<asp:Button runat="server" Text="Send" OnClick="Send_Click"  />
    </div>
    </form>
</body>
</html>
