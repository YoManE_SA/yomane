﻿<%@ Page Language="VB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
	Sub CheckIP(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		Dim sAddress As String = txtAddress.Text
		If Not IPManager.IsValidIP(sAddress) Then
			litResult.Text = "Invalid IP address: " & sAddress
			Return
		End If
		Dim sbList As New StringBuilder(txtList.Text)
		For i As Integer = 0 To sbList.Length - 1
			If Not IPManager.IsValidChar(sbList.Chars(i)) Then sbList.Replace(sbList.Chars(i), ";")
		Next
		Dim sList As String = sbList.ToString
		For Each sIP As String In sList.Split(";")
			If Not (IPManager.IsValidIP(sIP) Or IPManager.IsValidCIDR(sIP)) Then
				litResult.Text = "Invalid IP Address in list: " & sIP
				Return
			End If
		Next
		litResult.Text = "The address list " & IIf(IPManager.ContainsIP(sList, sAddress), "contains", "doesn't contain") & " the address."
	End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<form runat="server">
	IP List:
	<asp:TextBox ID="txtList" TextMode="MultiLine" Width="100%" Height="200px" runat="server" />
	Address: <asp:TextBox ID="txtAddress" Width="100%" runat="server" />
	<asp:Button OnClick="CheckIP" Text="Check" UseSubmitBehavior="true" Width="100%" runat="server" />
	<asp:Literal ID="litResult" runat="server" />
	</form>
</body>
</html>