﻿<%@ Page Language="VB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
	Sub Page_Load()
		Response.ExpiresAbsolute = DateTime.Now
		Response.Write(eCurrencies.MAX_CURRENCY & "<hr />")
		Try
			dbPages.LoadCurrencies()
		Catch ex As Exception
			Response.Write(ex.TargetSite.ToString & "<hr />")
			Response.Write(ex.Message & "<hr />")
			Response.Write(ex.StackTrace & "<hr />")
			If Not ex.InnerException Is Nothing Then
				Response.Write(ex.InnerException.TargetSite.ToString & "<hr />")
				Response.Write(ex.InnerException.Message & "<hr />")
				Response.Write(ex.InnerException.StackTrace & "<hr />")
			End If
		End Try
		Response.Write("Currencies reloaded.")
	End Sub
</script>