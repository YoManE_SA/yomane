Imports Microsoft.VisualBasic
Imports System.Web

Public Class Identity

    Public Shared ReadOnly Property Name() As String
        Get
            Dim sURL As String = HttpContext.Current.Request.Url.OriginalString
            sURL = sURL.Substring(sURL.IndexOf("://") + 3).Split("/")(0).ToLower.Replace(":443", String.Empty).Replace(":80", String.Empty)
            Select Case sURL
                Case "process.yomane.net"
                    Return "YomaneNET"
                Case "process.pbtpay.eu"
                    Return "PBTPay"
                Case "process.obl.me"
                    Return "OBL"
                Case "process.demoobl.me"
                    Return "DemoOBL"
                Case "stageprocess.yomane.net"
                    Return "Stage"
                Case "testprocess.netpay-intl.com"
                    Return "Test"
                Case "192.168.5.11:90", "192.168.5.11:90", "192.168.5.11", "localhost"
                    Return "Local"
                Case Else
                    Return ""
            End Select
        End Get
    End Property

    Public Shared ReadOnly Property EncryptionKeyNumber() As Integer
        Get
            If Name = "PBTPay" Then Return 4
            If Name = "Yomane" Then Return 5
            Return 5
        End Get
    End Property

    Public Shared ReadOnly Property ConnectionStringPrefix() As String
        Get
            Select Case Name
                Case "YomaneNET", "Netpay", "Local"
                    Return String.Empty
                Case Else
                    Return Name
            End Select
        End Get
    End Property

End Class