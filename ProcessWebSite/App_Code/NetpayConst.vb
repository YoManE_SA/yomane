Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Public Enum eCurrencies
	GC_ILS = 0
	GC_NIS = 0
	GC_USD = 1
	GC_EUR = 2
	GC_GBP = 3
	GC_AUD = 4
	GC_CAD = 5
	GC_JPY = 6
	GC_NOK = 7
	GC_PLN = 8
	GC_MXN = 9
	GC_ZAR = 10
	GC_RUB = 11
	GC_TRY = 12
	GC_CHF = 13
	GC_INR = 14
	GC_DKK = 15
	GC_SEK = 16
    	GC_CNY = 17
    	GC_HUF = 18
    	GC_NZD = 19
    	MAX_CURRENCY = 19
End Enum

Public Enum eGroupNames
	GGROUP_Currencies		= 1
	GGROUP_WireStatus		= 40
	GGROUP_DeniedStatus		= 41
	GGROUP_PAYMENTMETHOD	= 42
	GGROUP_CREDITTYPE		= 43
	GGROUP_COMPANYSTATUS	= 44
	GGROUP_BLCommon			= 47
	GGROUP_RRStatus			= 55
End Enum

Public Enum eCompanyStatus
	CMPS_ARCHIVED			= 0
	CMPS_NEW				= 1
	CMPS_BLOCKED			= 2
	CMPS_CLOSED				= 3
	CMPS_MAXCLOSED			= 9
	CMPS_CPONLY				= 10
	CMPS_INTEGRATION		= 20
	CMPS_MINOK				= 30
	CMPS_PROCESSING			= 30
End Enum

Public Enum ePaymentMethod
    PMD_Unknown = 00
    PMD_ManualFee = 01
    PMD_Admin = 02
    PMD_SystemFees = 03
    PMD_RolRes = 04
    PMD_MAXINTERNAL = 14

    PMD_CC_MIN = 20
    PMD_CC_UNKNOWN = 20
    PMD_CC_ISRACARD = 21
    PMD_CC_VISA = 22
    PMD_CC_DINERS = 23
    PMD_CC_AMEX = 24
    PMD_CC_MASTERCARD = 25
    PMD_CC_DIRECT = 26
    PMD_CC_OTHER = 27 'NOT IN USE
    PMD_CC_MAESTRO = 28
    PMD_CC_TOGGLECARD = 29
    'Const PMD_CC_ECHECK	= 30
    PMD_CC_MAX = 99

    PMD_EC_MIN = 100
    PMD_EC_CHECK = 100
    PMD_EC_GIROPAY = 101
    PMD_EC_DIRECTPAY24 = 102
    PMD_EC_PINELV = 103
    PMD_EC_PAYSAFECARD = 104
    PMD_EC_CASH_TICKET = 105
    PMD_EC_PRZELEWY24 = 106
    PMD_EC_EPS = 107
    PMD_EC_WALLIE = 108
    PMD_EC_IDEAL = 109
    PMD_EC_TELEINGRESO = 110
    PMD_EC_MONEYBOOKERS = 111
    PMD_EC_ELV = 112
    PMD_EC_YELLOWPAY = 113
    PMD_EC_MAX = 150

    PMD_PP_MIN = 5000
    PMD_PP_MAX = 20000
    PaymentMethodMax = 20000
End Enum

Public Enum eFailStatusSource
	FSC_Issuer				= 0
	FSC_Risk				= 1
	FSC_Gateway				= 2
End Enum

Public Enum eBLCommon
	eBL_Email		= 1
	eBL_FullName	= 2
	eBL_Phone		= 3
	eBL_PNumber		= 4
End Enum

Public Class NetpayConst
	Shared GLOBAL_COUNTRY_HASH as System.Collections.Hashtable = Nothing
	Const GGROUP_GroupNames As Integer = 0
	
    Public Shared Function GetConstArrayEx(ByVal sGroup As Integer, ByVal sTextField As String, vLng As Byte, ByRef vTitle As String, ByRef sRetArr() As String) As Integer 
		Dim drData As SqlDataReader = dbPages.ExecReader("Select GD_ID, GD_Group, GD_" & sTextField & " From tblGlobalData Where ((GD_Group=" & sGroup & ") Or (GD_Group=" & GGROUP_GroupNames & " And GD_ID=" & sGroup & ")) And GD_LNG=" & vLng & " Order By GD_Group Asc, GD_ID Desc")
		Dim bHasValue As Boolean = drData.Read()
		If bHasValue Then
			If (drData(1) = GGROUP_GroupNames) And (drData(0) = sGroup) Then
				vTitle = dbPages.TestVar(drData(2), -1, "")
				bHasValue = drData.Read()
			End If
			If bHasValue Then
				If sRetArr Is Nothing Then GetConstArrayEx = -1 Else GetConstArrayEx = UBound(sRetArr)
				If GetConstArrayEx < drData(1) Then ReDim sRetArr(drData(0))
			End If
		End If
		GetConstArrayEx = 0
		Do While bHasValue
			sRetArr(drData(0)) = drData(2)
			GetConstArrayEx += 1
			bHasValue = drData.Read()
		Loop
		drData.Close()
	End Function

	Public Shared Function GetConstArray(ByVal sGroup As Integer, ByVal vLng As Integer, ByRef vTitle As String, ByRef sRetArr() As String) As Integer
		Return GetConstArrayEx(sGroup, "Text", vLng, vTitle, sRetArr)
	End Function

	Public Shared Function GetDescConstArray(ByVal sGroup As Integer, ByVal vLng As Integer, ByRef vTitle As String, ByRef sRetArr() As String) As Integer
		Return GetConstArrayEx(sGroup, "Description", vLng, vTitle, sRetArr)
	End Function

	Public Shared Sub FillCombo(ByVal cbox As System.Web.UI.WebControls.ListBox, ByVal sGroup As Integer, ByVal vLng As Integer, ByVal xDef As Integer)
		Dim drData As SqlDataReader = dbPages.ExecReader("Select GD_ID, GD_Text From tblGlobalData Where (GD_Group=" & sGroup & ") And GD_LNG=" & vLng & " Order By GD_Group Asc, GD_ID Desc")
		While drData.Read()
			Dim nItem As System.Web.UI.WebControls.ListItem = New System.Web.UI.WebControls.ListItem(dbPages.TestVar(drData("GD_Text"), -1, ""), dbPages.TestVar(drData("GD_ID"), 0, -1, 0))
			If dbPages.TestVar(drData("GD_ID"), 0, -1, 0) = xDef Then nItem.Selected = True
			cbox.Items.Add(nItem)
		End While
		drData.Close()
	End Sub

	Public Shared Function GetCountryName(ByVal xIsoCode As String) As String
		If GLOBAL_COUNTRY_HASH Is Nothing Then
			GLOBAL_COUNTRY_HASH = New System.Collections.Hashtable
			Dim drData As SqlDataReader = dbPages.ExecReader("SELECT CountryISOCode, Name FROM [List].[CountryList]")
			While drData.Read()
				GLOBAL_COUNTRY_HASH.Add(drData(0), drData(1))
			End While
			drData.Close()
		End If
		If Not GLOBAL_COUNTRY_HASH.ContainsKey(xIsoCode) Then Return ""
		Return GLOBAL_COUNTRY_HASH(xIsoCode)
	End Function
End Class
