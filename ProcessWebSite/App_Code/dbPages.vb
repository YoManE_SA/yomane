Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Net
Imports System.Web
Imports System.Configuration
Imports Netpay.Crypt

Public Class NoDbConnType
	Public Overrides Function ToString() As String
		Return "The specified database has been taken offline for maintainance purposes. Please try again later."
	End Function
End Class

Public Class dbPages
	Public Const APP_MINID As Long = 0
	Public Const APP_MAXID As Long = 100000000
    Public Const UseLocalID As Boolean = False



    Public Const ForAppending = 8
    Public Const LOG_FOLDER = "C:\temp\ProcessWebsiteLogs\"
    Public Const LOG_FILE = "processWebsiteTracking"
    Public Const LOG_FILE_EXTENSION = ".log"
    Public Const LOG_FILE_SEPARATOR = "_"


    'Prefix zero if day or month is in single digit
    Public Shared Function PrefixZero(num)
        If (Len(num) = 1) Then
            PrefixZero = "0" & num
        Else
            PrefixZero = num
        End If
    End Function

    'To get current date in ddMMyyyy format. 
    'Ex: 09 Apr 2016 wil be returned as 09042016 and 15 Oct 2016 will be returned as 15102016
    Public Shared Function GetCurrentDate()
        Dim timeStamp = Now()
        Dim d = PrefixZero(Day(timeStamp))
        Dim m = PrefixZero(Month(timeStamp))
        Dim y = Year(timeStamp)
        GetCurrentDate = d & m & y
    End Function

    'Log message with timestamp
    Public Shared Sub WriteLog(message)

        'Dim objFSO = CreateObject("Scripting.FileSystemObject")
        'Get log file with current date 
        Dim logFile = LOG_FOLDER & LOG_FILE & LOG_FILE_SEPARATOR & GetCurrentDate() & LOG_FILE_EXTENSION
        'Open log file in appending mode
        ' Dim objLogger = objFSO.OpenTextFile(logFile, ForAppending, True)
        'Prefix timestamp
        message = FormatDateTime(Now(), vbLongDate) & " " & FormatDateTime(Now(), vbLongTime) & " >>> " & message

        'Write log message
        If System.IO.File.Exists(logFile) = False Then
            System.IO.File.WriteAllText(logFile, message)
        Else
            System.IO.File.AppendAllText(logFile, message)
        End If

        'objLogger.WriteLine(message)
        ''Close file
        'objLogger.Close()
    End Sub

    Public Shared Function LogEvent(ByVal remark As String) As Boolean

        Const ForReading = 1, ForWriting = 2, ForAppending = 8
        Dim filesys = CreateObject("Scripting.FileSystemObject")
        Dim filetxt = filesys.OpenTextFile("c:\temp\someLogfile.txt", ForAppending, True)
        filetxt.WriteLine(remark)
        filetxt.Close
        Return True
    End Function


    Public Shared Function getConfig(ByVal sKey As String) As String
        Return ConfigurationManager.AppSettings.Item(sKey)
    End Function

    Private Shared Function GetConnectionString(ByVal sName As String) As String
        WriteLog("sName = " & sName)
        If String.IsNullOrEmpty(sName) Then Return String.Empty
        If String.IsNullOrEmpty(HttpContext.Current.Session(sName)) Then
            Dim sStringName As String = IIf(sName = "Errors", "", Identity.ConnectionStringPrefix) & sName
            WriteLog("sStringName = " & sStringName)
            Dim sEncrypted As String = ConfigurationManager.ConnectionStrings(sStringName).ConnectionString
            WriteLog("sEncrypted = " & sEncrypted)

            Dim ct = Netpay.Crypt.SymEncryption.GetKey(Identity.EncryptionKeyNumber)
            Dim sDecrypted As String = ct.Decrypt(New Netpay.Crypt.Blob() With {.Hex = sEncrypted}).Text
            HttpContext.Current.Session(sName) = sDecrypted
        End If

        Return HttpContext.Current.Session(sName)
    End Function

    Public Shared ReadOnly Property DSN() As String
        Get
            Return GetConnectionString("DSN")
        End Get
    End Property

    Public Shared ReadOnly Property DSN2() As String
        Get
            Return GetConnectionString("DSN2")
        End Get
    End Property

    Private Shared ReadOnly Property HOTDB_ENABLED() As Boolean
        Get
            Return getConfig("HOTDB_ENABLED")
        End Get
    End Property
    Private Shared ReadOnly Property HOTDB_INTERVAL_MINUTES() As Integer
        Get
            Return getConfig("HOTDB_INTERVAL_MINUTES")
        End Get
    End Property
    Private Shared ReadOnly Property HOTDB_DURATION_SECONDS() As Integer
        Get
            Return getConfig("HOTDB_DURATION_SECONDS")
        End Get
    End Property
    Private Shared ReadOnly Property HOTDB_PREVENTION_SECONDS() As Integer
        Get
            Return getConfig("HOTDB_PREVENTION_SECONDS")
        End Get
    End Property

    Public Shared Function IsActiveDSN2() As Boolean
        If Not HOTDB_ENABLED Then Return False
        Dim nMinute As Integer = Date.Now.Minute Mod HOTDB_INTERVAL_MINUTES, nSecond As Integer = Date.Now.Second
        Return nMinute * 60 + nSecond > HOTDB_DURATION_SECONDS And nMinute * 60 + nSecond < (HOTDB_INTERVAL_MINUTES - 1) * 60 + HOTDB_PREVENTION_SECONDS
    End Function

    Private Shared Property CurrentConnectionString() As String
        Get
            If String.IsNullOrEmpty(HttpContext.Current.Session("CurrentConnectionString")) Then HttpContext.Current.Session("CurrentConnectionString") = DSN
            Return HttpContext.Current.Session("CurrentConnectionString")
        End Get
        Set(ByVal value As String)
            HttpContext.Current.Session("CurrentConnectionString") = value
        End Set
    End Property

    Public Shared Property CurrentDSN() As Integer
        Get
            Return IIf(CurrentConnectionString = DSN2, 2, 1)
        End Get
        Set(ByVal value As Integer)
            CurrentConnectionString = IIf(value = 2 And IsActiveDSN2(), DSN2, DSN)
        End Set
    End Property

    Public Shared Function ShowCurrentDSN() As String
        Dim sbDSN As New StringBuilder
        sbDSN.AppendLine("<script language=""javascript"" type=""text/javascript"" defer=""defer"">")
        sbDSN.AppendLine("	function ShowSQL2()")
        sbDSN.AppendLine("	{")
        sbDSN.AppendLine("		if (document.getElementById(""pageMainHeading""))")
        sbDSN.AppendLine("		{")
        sbDSN.AppendLine("			document.getElementById(""pageMainHeading"").innerHTML+=""<div class=\""dsnHead\"">SQL2</div>"";")
        sbDSN.AppendLine("			document.getElementById(""divDSN2"").style.display=""none"";")
        sbDSN.AppendLine("		}")
        sbDSN.AppendLine("	}")
        sbDSN.AppendLine("	setTimeout(""ShowSQL2();"", 1000);")
        sbDSN.AppendLine("</script>")
        sbDSN.AppendLine("<div id=""divDSN2"" class=""dsn"">SQL" & CurrentDSN & "</div>")
        Return sbDSN.ToString
    End Function

    Public Shared Function ExecSql(ByVal sqlStr As String) As Long
        Dim DBCon As New SqlConnection(DSN)
        Dim DBCom As New SqlCommand(sqlStr, DBCon)
        Try
            DBCon.Open()
            ExecSql = DBCom.ExecuteNonQuery()
        Catch
            With System.Web.HttpContext.Current
                '.Response.Write(sqlStr)
                '.Response.Flush()
            End With
            Throw
        Finally
            DBCon.Close()
            DBCom.Dispose()
            DBCon.Dispose()
        End Try
    End Function

    Public Shared Function ExecScalar(ByVal sqlStr As String, Optional ByVal sDSN As String = Nothing, Optional ByVal bStopIfDisabledDSN2 As Boolean = False) As Object
        Dim sTempDSN As String = IIf(String.IsNullOrEmpty(sDSN), CurrentConnectionString, sDSN)
        If sTempDSN <> DSN And Not IsActiveDSN2() Then
            If bStopIfDisabledDSN2 Then Return NoDbConn
            sTempDSN = DSN
        End If
        Dim DBCon As New SqlConnection(sTempDSN)
        Dim DBCom As New SqlCommand(sqlStr, DBCon)
        Dim bFailedDSN2 As Boolean = False
        Try
            DBCon.Open()
        Catch ex1 As Exception
            If sTempDSN = DSN2 Then
                bFailedDSN2 = True
                If DBCon.State <> Data.ConnectionState.Closed Then DBCon.Close()
                DBCon.ConnectionString = DSN
            Else
                Throw ex1
            End If
        End Try
        Try
            If bFailedDSN2 Then DBCon.Open()
            ExecScalar = DBCom.ExecuteScalar()
        Catch ex2 As Exception
            With System.Web.HttpContext.Current
                '.Response.Write(sqlStr)
                '.Response.Flush()
            End With
            Throw ex2
        Finally
            DBCon.Close()
            DBCom.Dispose()
            DBCon.Dispose()
        End Try
    End Function

    Public Shared NoDbConn As NoDbConnType

    Public Shared Function ExecReader(ByVal sqlStr As String, Optional ByVal sDSN As String = Nothing, Optional ByVal nTimeOut As Integer = 0, Optional ByVal bStopIfDisabledDSN2 As Boolean = False) As SqlDataReader
        Dim sTempDSN As String = IIf(String.IsNullOrEmpty(sDSN), CurrentConnectionString, sDSN)
        If sTempDSN = DSN2 Then
            If Not IsActiveDSN2() Then
                If bStopIfDisabledDSN2 Then Return Nothing
                sTempDSN = DSN
            End If
        End If
        Dim DBCon As New SqlConnection(sTempDSN)
        Dim DBCom As New SqlCommand(sqlStr, DBCon)
        If nTimeOut > 0 Then DBCom.CommandTimeout = nTimeOut
        Dim bFailedDSN2 As Boolean = False
        Try
            DBCon.Open()
        Catch ex1 As Exception
            If sTempDSN = DSN2 Then
                bFailedDSN2 = True
                If DBCon.State <> Data.ConnectionState.Closed Then DBCon.Close()
                DBCon.ConnectionString = DSN
            Else
                Throw ex1
            End If
        End Try
        Try
            If bFailedDSN2 Then DBCon.Open()
            ExecReader = DBCom.ExecuteReader(System.Data.CommandBehavior.CloseConnection)
        Catch ex2 As Exception
            With System.Web.HttpContext.Current
                '.Response.Write(sqlStr)
                '.Response.Flush()
            End With
            Throw ex2
        Finally
            DBCom.Dispose()
        End Try
    End Function

    Public Shared Function ExecDataset(ByVal sqlStr As String, ByVal lPageSize As Integer, ByVal lPage As Integer, Optional ByVal sDSN As String = Nothing, Optional ByVal bStopIfDisabledDSN2 As Boolean = False) As System.Data.DataSet
        Dim sTempDSN As String = IIf(String.IsNullOrEmpty(sDSN), CurrentConnectionString, sDSN)
        If sTempDSN <> DSN Then
            If Not IsActiveDSN2() Then
                If bStopIfDisabledDSN2 Then Return Nothing
                sTempDSN = DSN
            End If
        End If
        Dim DBCon As New SqlConnection(sTempDSN)
        Dim iAdpt As New SqlDataAdapter(sqlStr, DBCon)
        Dim pRet As New System.Data.DataSet()
        Dim bFailedDSN2 As Boolean = False
        Try
            DBCon.Open()
        Catch ex1 As Exception
            If sTempDSN = DSN2 Then
                bFailedDSN2 = True
                If DBCon.State <> Data.ConnectionState.Closed Then DBCon.Close()
                DBCon.ConnectionString = DSN
            Else
                Throw ex1
            End If
        End Try
        Try
            If bFailedDSN2 Then DBCon.Open()
            If lPageSize <= 0 Then iAdpt.Fill(pRet) Else iAdpt.Fill(pRet, lPageSize * lPage, lPageSize + 1, "Table1")
        Catch ex2 As Exception
            With System.Web.HttpContext.Current
                .Response.Write(sqlStr)
                .Response.Flush()
            End With
            Throw ex2
        Finally
            iAdpt.Dispose()
            DBCon.Close()
        End Try
        Return pRet
    End Function

    Public Shared Function dbtextShowForm(ByVal sText As String) As String
        If sText Is Nothing Then Return ""
        Return sText.Trim.Replace("``", """").Replace("`", "'")
    End Function

    Public Shared Function dbtextShow(ByVal sText As String) As String
        If sText Is Nothing Then Return ""
        Return sText.Trim.Replace("``", """").Replace("`", "'").Replace(vbCr, "<br>")
    End Function

    Public Shared Function DBText(ByVal sText As String) As String
        If sText Is Nothing Then Return ""
        Return sText.Trim.Replace("'", "`").Replace("""", "``").Replace("<", "&lt;").Replace(">", "&gt;")
    End Function

    Public Shared Sub LoadListBox(ByVal pBox As ListBox, ByVal sqlstr As String, ByVal cSelValue As Object)
        Dim iReader As SqlDataReader = dbPages.ExecReader(sqlstr)
        While iReader.Read
            Dim lsitem As ListItem = New ListItem(iReader(0), iReader(1))
            pBox.Items.Add(lsitem)
            If lsitem.Value = cSelValue Then lsitem.Selected = True
        End While
        iReader.Close()
    End Sub

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Integer, ByVal lMax As Integer, ByVal lDef As Integer) As Integer
        If (Not xval Is Nothing) And IsNumeric(xval) Then
            Try
                TestVar = Integer.Parse(xval)
            Catch
                TestVar = lDef
            End Try
            If (lMin <= lMax) And ((TestVar < lMin) Or (TestVar > lMax)) Then TestVar = lDef
        Else
            TestVar = lDef
        End If
    End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lDef As Boolean) As Boolean
        If (Not xval Is Nothing) Then
            Try
                If TypeOf (xval) Is Boolean Then
                    TestVar = xval
                ElseIf IsNumeric(xval) Then
                    TestVar = Integer.Parse(xval) <> 0
                Else
                    TestVar = Boolean.Parse(xval)
                End If
            Catch
                TestVar = lDef
            End Try
        Else
            TestVar = lDef
        End If
    End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Decimal, ByVal lMax As Decimal, ByVal lDef As Decimal) As Decimal
        Dim rVal As Decimal = lDef
        Try
            rVal = Decimal.Parse(xval.ToString())
            If ((lMin <= lMax) And ((rVal < lMin) Or (rVal > lMax))) Then rVal = lDef
        Catch
        End Try
        Return rVal
    End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMax As Integer, ByVal lDef As String) As String
        Dim rVal As String = lDef
        Try
            rVal = xval.ToString()
            If (rVal.Length > lMax) Then rVal = rVal.Substring(0, lMax)
        Catch
        End Try
        Return rVal
    End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Date, ByVal lMax As Date, ByVal lDef As Date) As Date
        Dim rVal As Date = lDef
        Try
            rVal = Date.Parse(xval.ToString())
            If rVal = lDef Then rVal = Date.Parse(xval.ToString() & "PM")
            If ((lMin <= lMax) And ((rVal < lMin) Or (rVal > lMax))) Then rVal = lDef
        Catch
        End Try
        Return rVal
    End Function

    Public Shared Function TestNumericList(ByVal xStr As String, ByRef xCount As Integer) As String
        Dim xStrList As String() = xStr.Split(",") : xCount = 0
        Dim strOut As String = ""
        For i As Integer = 0 To xStrList.Length - 1
            If (Not xStrList(i) Is Nothing) And IsNumeric(xStrList(i)) Then
                Try
                    Dim xVal As Integer = Integer.Parse(xStrList(i))
                    strOut &= xVal & "," : xCount += 1
                Catch
                End Try
            End If
        Next
        If strOut.Length > 1 Then strOut = strOut.Substring(0, strOut.Length - 1)
        Return strOut
    End Function

    Public Shared Function GetFileText(ByVal sFileName As String) As String
        Dim output As String = ""
        If System.IO.File.Exists(sFileName) Then
            Dim sr As New System.IO.StreamReader(sFileName)
            output = sr.ReadToEnd()
            sr.Close()
        End If
        Return output
    End Function

    Public Shared Function SendEmail(ByVal sMailSubject As String, ByVal sMailString As String, ByVal pReader As SqlDataReader, ByVal pData As NameValueCollection, Optional ByVal bPreviewOnly As Boolean = False) As Boolean
        If Not pData Is Nothing Then
            For i As Integer = 0 To pData.Count - 1
                sMailString = sMailString.Replace("%" & pData.GetKey(i) & "%", pData(i))
            Next
        End If
        If Not pReader Is Nothing Then
            For i As Integer = 0 To pReader.FieldCount - 1
                sMailString = sMailString.Replace("%" & pReader.GetName(i) & "%", pReader(i).ToString())
            Next
        End If
        Dim sCss As String = "<style>" & GetFileText(HttpContext.Current.Server.MapPath("../Data/EmailStyle.css")) & "</style>"
        sMailString = "<html><head></head>" & sCss & "<body>" & sMailString & "</body></html>"
        If bPreviewOnly Then
            HttpContext.Current.Response.Write(sMailString)
            Return True
        End If
        Dim xMsg As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage(TestVar(getConfig("MailFrom"), -1, ""), pData("MailTo"))
        xMsg.IsBodyHtml = True
        xMsg.Subject = sMailSubject
        xMsg.Body = sMailString
        'If dbPages.TestVar(getConfig("SMTPUser"), -1, "") <> "" Then xClt.Credentials = New NetworkCredential(getConfig("SMTPUser"), getConfig("SMTPPassword"))
        Try
            SendEmail(xMsg)
        Catch ex As Exception
            HttpContext.Current.Session("LastEmailError") = ex.Source & ": " & ex.Message
            Return False
        End Try
        Return True
    End Function

    Public Shared Sub SendEmail(message As System.Net.Mail.MailMessage)
        Dim xClt As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient(TestVar(getConfig("SMTPServer"), -1, ""))
        If dbPages.TestVar(getConfig("SMTPUser"), -1, "") <> "" Then xClt.Credentials = New NetworkCredential(getConfig("SMTPUser"), getConfig("SMTPPassword"))
        xClt.Send(message)
    End Sub

    Public Shared Sub LoadCurrencies()
        Dim CUR_CHARS(), CUR_RATES() As String
        ReDim CUR_CHARS(eCurrencies.MAX_CURRENCY)
        ReDim CUR_RATES(eCurrencies.MAX_CURRENCY)
        Dim iReader As SqlDataReader = ExecReader("Select CurrencyID, BaseRate, Symbol From List.CurrencyList Order By CurrencyID Desc")
        While iReader.Read
            If iReader("CurrencyID") <= eCurrencies.MAX_CURRENCY Then
                CUR_CHARS(iReader("CurrencyID")) = System.Web.HttpContext.Current.Server.HtmlDecode(iReader("Symbol"))
                CUR_RATES(iReader("CurrencyID")) = iReader("BaseRate")
            End If
        End While
        iReader.Close()
        HttpContext.Current.Application.Set("CUR_CHARS", CUR_CHARS)
        HttpContext.Current.Application.Set("CUR_RATES", CUR_RATES)
    End Sub

    Public Shared Function GetCurText(ByVal trnCurr As Byte) As String
        If HttpContext.Current.Application.Get("CUR_CHARS") Is Nothing Then LoadCurrencies()
        If (trnCurr < 0) Or (trnCurr > UBound(HttpContext.Current.Application.Get("CUR_CHARS"))) Then Return "" _
        Else Return HttpContext.Current.Application.Get("CUR_CHARS")(trnCurr)
    End Function

    Public Shared Function FormatCurrWCT(ByVal trnCurr As Byte, ByVal trnValue As Decimal, ByVal crType As Byte) As String
        If CInt(crType) = 0 Then trnValue = -trnValue
        FormatCurrWCT = FormatCurr(trnCurr, trnValue)
    End Function

    Public Shared Function FormatCurr(ByVal trnCurr As Byte, ByVal trnValue As Decimal) As String
        FormatCurr = GetCurText(trnCurr) & "&nbsp;" & FormatNumber(trnValue, 2, TriState.True, TriState.False, TriState.True)
    End Function

    Public Shared Sub showDivBox(ByVal fType As String, ByVal fDir As String, ByVal fWidth As String, ByVal fText As String) '(help;note, ltr,rtl)
        Dim sStyle As String, sClass As String, sOnMouseOver As String, sOnMouseOut As String
        Select Case Trim(fType)
            Case "help" : sStyle = "width:" & fWidth & "px; background-color:#ECEFFF; border:1px solid Navy;"
            Case Else : sStyle = "width:" & fWidth & "px; background-color:#ffffe6; border:1px solid #353535;"
        End Select
        Select Case Trim(fDir)
            Case "ltr" : sClass = "txt11"
            Case Else : sClass = "txt12"
        End Select
        sOnMouseOver = "previousSibling.style.visibility='visible'; previousSibling.style.left=getAbsPos(this).Left+35; previousSibling.style.top=getAbsPos(this).Top;"
        sOnMouseOut = "previousSibling.style.visibility='hidden';"
        HttpContext.Current.Response.Write("<div class=""" & sClass & """ style=""direction:" & fDir & "; visibility:hidden; position:absolute; z-index:1; padding:3px; " & sStyle & """>" & fText & "</div>")
        HttpContext.Current.Response.Write("<img src=""../images/img/iconQuestionGrayS.gif"" align=""middle"" border=""0"" style=""cursor:help;"" onmouseover=""" & sOnMouseOver & """ onmouseout=""" & sOnMouseOut & """ />")
    End Sub

    Public Shared Sub showFilter(ByVal fText As String, ByVal fUrl As String, ByVal fDist As Integer)
        Dim sSibling As String = "", sOnMouseOver As String, sOnMouseOut As String
        For i As Integer = 1 To fDist
            sSibling = sSibling & "nextSibling."
        Next
        sOnMouseOver = sSibling & "style.visibility='visible';"
        sOnMouseOut = sSibling & "style.visibility='hidden';"
        HttpContext.Current.Response.Write("<a href=""" & fUrl & """ onmouseover=""" & sOnMouseOver & """ onmouseout=""" & sOnMouseOut & """ style=""text-decoration:none; border-bottom:1px dotted #0CB10F;"">" & fText & "</a>")
        HttpContext.Current.Response.Write("<span style=""visibility:hidden;""> <img src=""../images/img/icon_filterPlus.gif"" align=""middle""></span>")
    End Sub

    Public Shared Function GetUrlValue(ByVal sUrl As String, ByVal sValue As String) As String
        Dim sIndex As Integer = sUrl.IndexOf(sValue & "=", System.StringComparison.OrdinalIgnoreCase)
        If sIndex < 0 Then Return ""
        sIndex = sIndex + sValue.Length + 1
        Dim eIndex As Integer = sUrl.IndexOf("&", sIndex, System.StringComparison.OrdinalIgnoreCase)
        If eIndex < 0 Then eIndex = sUrl.Length
        Return sUrl.Substring(sIndex, eIndex - sIndex)
    End Function

    Public Shared Function SetUrlValue(ByVal qString As String, ByVal strParam As String, ByVal sValue As String) As String
        Dim idx As Integer, idxEnd As Integer
        SetUrlValue = qString
        idx = InStr(1, SetUrlValue, strParam & "=")
        If (idx < 1) Then
            If sValue = Nothing Then Return qString
            Return qString & "&" & strParam & "=" & sValue
        End If
        idxEnd = InStr(idx, SetUrlValue, "&")
        If (idxEnd < 1) Then idxEnd = Len(SetUrlValue)
        SetUrlValue = Left(SetUrlValue, idx - 1) & Right(SetUrlValue, Len(SetUrlValue) - idxEnd) & "&" & strParam & "=" & sValue
    End Function

    Public Shared Function CleanUrl(ByVal Request As NameValueCollection) As String
        Dim strOut As String = ""
        For i As Integer = 0 To Request.Count - 1
            If Request(i) <> "" Then strOut &= "&" & Request.GetKey(i) & "=" & Request(i)
        Next
        Return strOut
    End Function

    Public Shared Function FormatDateTime(ByVal dtValue As Date, Optional ByVal bShowDate As Boolean = True, Optional ByVal bShowTime As Boolean = True, Optional ByVal bShowYYYY As Boolean = False, Optional ByVal bShowSeconds As Boolean = False) As String
        Dim sbDate As New StringBuilder
        If bShowDate Then
            sbDate.Append(dtValue.Day.ToString.PadLeft(2, "0")).Append("/")
            sbDate.Append(dtValue.Month.ToString.PadLeft(2, "0")).Append("/")
            If bShowYYYY Then
                sbDate.Append(dtValue.Year.ToString)
            Else
                sbDate.Append(dtValue.Year.ToString.Substring(2, 2))
            End If
            If bShowTime Then sbDate.Append(" ")
        End If
        If bShowTime Then
            sbDate.Append(dtValue.Hour.ToString.PadLeft(2, "0")).Append(":")
            sbDate.Append(dtValue.Minute.ToString.PadLeft(2, "0"))
            If bShowSeconds Then sbDate.Append(":").Append(dtValue.Second.ToString.PadLeft(2, "0"))
        End If
        Return sbDate.ToString
    End Function

    Public Shared Function SendHttpRequest(ByVal sUrl As String, Optional ByVal sData As String = Nothing, Optional ByVal bPost As Boolean = True, Optional ByRef sResponse As String = Nothing, Optional ByRef enc As Encoding = Nothing) As Integer
        Dim loHttp As System.Net.HttpWebRequest
        Try
            WriteLog("Initiate dbPages.SendHttpRequest")
            If enc Is Nothing Then enc = Encoding.GetEncoding(1252) ' Windows default Code Page
            If Not bPost Then sUrl &= "?" & sData.Replace(" ", "+")
            loHttp = System.Net.WebRequest.Create(sUrl)

            WriteLog("loHttp.Url = " & sUrl)
            WriteLog("bPost = " & bPost)
            If bPost Then
                loHttp.Method = "POST"
                loHttp.ContentType = "application/x-www-form-urlencoded"
                Dim loRequestStreamWriter As New System.IO.StreamWriter(loHttp.GetRequestStream())
                loRequestStreamWriter.Write(sData.Replace(" ", "+"))
                loRequestStreamWriter.Close()
            End If
            Dim loWebResponse As System.Net.HttpWebResponse
            Try
                loWebResponse = loHttp.GetResponse()
                WriteLog("loWebResponse = " & loWebResponse.ToString())
            Catch ex As WebException
                loWebResponse = ex.Response
                WriteLog("Exception = " & ex.ToString())
            Catch ex As Exception
                Return 0
            End Try
            Dim loResponseStream As System.IO.StreamReader = New System.IO.StreamReader(loWebResponse.GetResponseStream(), enc)
            sResponse = loResponseStream.ReadToEnd()

            WriteLog("sResponse = " & sResponse.ToString())
            loResponseStream.Close()
            loWebResponse.Close()
            WriteLog("sResponse = " & sResponse.ToString())
            Return loWebResponse.StatusCode
        Catch
            Return 0
        End Try
    End Function

    Public Function Hash(hashType As String, hashString As String) As Netpay.Crypt.Blob
        If hashType = "SHA" Then hashType = "SHA256"
        Return (New Netpay.Crypt.Blob() With {.UTF8 = hashString}).Hash(hashType)
    End Function

    Public Function CalcHash(hashType As String, hashString As String, useRawOutput As Boolean) As String
        Dim ret = Hash(hashType, hashString)
        If Not useRawOutput Then ret.Ascii = LCase(ret.Hex)
        CalcHash = ret.Base64
    End Function

    Public Function ValidateHash(hashType As String, hashString As String, hashValue As String) As Boolean
        Dim hashCalcValue = CalcHash(hashType, hashString, True)
        ValidateHash = (hashCalcValue = hashValue)
        If ValidateHash Then Exit Function
        ValidateHash = CalcHash(hashType, hashString, False) = hashValue
    End Function



End Class
