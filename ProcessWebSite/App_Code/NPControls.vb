Imports system.Data.SqlClient
Imports System.Web
Imports Microsoft.VisualBasic

Public Class NPControls
	Public Shared Sub FillList(ByVal ddlList As ListControl, ByVal nFrom As Integer, ByVal nTo As Integer, Optional ByVal bClear As Boolean = False, Optional ByVal sDefaultValue As String = Nothing, Optional ByVal sFirstValue As String = "", Optional ByVal sFirstText As String = "")
		If bClear Then
			While ddlList.Items.Count > 0
				ddlList.Items.RemoveAt(0)
			End While
		End If
		If Not sFirstText Is Nothing Then
			ddlList.Items.Add(sFirstText)
			ddlList.Items(0).Value = IIf(sFirstValue Is Nothing, String.Empty, sFirstValue)
		End If
		For i As Integer = nFrom To nTo
			ddlList.Items.Add(New ListItem(Right("000000000" & i.ToString, nTo.ToString.Length)))
			If ddlList.Items(ddlList.Items.Count - 1).Value = sDefaultValue Then ddlList.Items(ddlList.Items.Count - 1).Selected = True
		Next
	End Sub

	Public Shared Sub FillList(ByVal ddlList As ListControl, ByVal drData As SqlDataReader, Optional ByVal bClear As Boolean = False, Optional ByVal sDefaultValue As String = Nothing, Optional ByVal sFirstValue As String = "", Optional ByVal sFirstText As String = "")
		If bClear Then
			While ddlList.Items.Count > 0
				ddlList.Items.RemoveAt(0)
			End While
		End If
		If Not sFirstText Is Nothing Then
			ddlList.Items.Add(sFirstText)
			ddlList.Items(0).Value = IIf(sFirstValue Is Nothing, String.Empty, sFirstValue)
		End If
		Do While drData.Read
			ddlList.Items.Add(New ListItem(drData(1), drData(0)))
			If ddlList.Items(ddlList.Items.Count - 1).Value = sDefaultValue Then ddlList.Items(ddlList.Items.Count - 1).Selected = True
		Loop
		drData.Close()
	End Sub

	Public Shared Sub TrimValue(ByVal txtField As TextBox)
		txtField.Text = txtField.Text.Trim
	End Sub

	Public Shared Function IsEmptyField(ByVal txtField As TextBox, Optional ByVal lblError As Label = Nothing, Optional ByVal sTitle As String = Nothing) As Boolean
		 If txtField.Text.Trim = String.Empty Then
			txtField.Focus()
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: " & IIf(String.IsNullOrEmpty(sTitle), "at least one mandatory field", "<b>" & sTitle & "</b>") & " is empty.</div>"
			Return True
		End If
		Return False
	End Function

	Public Shared Function IsNotSelected(ByVal ddlSelect As DropDownList, Optional ByVal lblError As Label = Nothing, Optional ByVal sTitle As String = Nothing) As Boolean
		If ddlSelect.SelectedIndex = 0 Then
			ddlSelect.Focus()
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: " & IIf(String.IsNullOrEmpty(sTitle), "at least one mandatory list", "<b>" & sTitle & "</b>") & " value is not selected.</div>"
			Return True
		End If
		Return False
	End Function

	Public Shared Function IsNotMail(ByVal txtField As TextBox, Optional ByVal lblError As Label = Nothing, Optional ByVal sTitle As String = Nothing) As Boolean
		Dim regxMail As Regex = New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\w", RegexOptions.IgnoreCase)
		If Not regxMail.IsMatch(txtField.Text.Replace("-", "_").Replace("~", "_")) Then
			txtField.Focus()
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: " & IIf(String.IsNullOrEmpty(sTitle), "the eMail field", "<b>" & sTitle & "</b>") & " is not valid eMail address.</div>"
			Return True
		End If
		Return False
	End Function

	Public Shared ReadOnly Property Mandatory() As String
		Get
			Return "<span class=""mandatory"">*</span>"
		End Get
	End Property

	Public Shared Sub CheckSession()
		If String.IsNullOrEmpty(HttpContext.Current.Session("UserID")) Then HttpContext.Current.Response.Redirect("default.aspx")
	End Sub

	Public Shared Function IsSession() As Boolean
		Return Not String.IsNullOrEmpty(HttpContext.Current.Session("UserID"))
	End Function

	Public Shared Function IsNotCaptcha(ByVal txtField As TextBox, ByVal sCaptchaID As String, Optional ByVal lblError As Label = Nothing) As Boolean
		Dim sSessionCaptcha As String = HttpContext.Current.Session("Captcha_" & sCaptchaID)
		If String.IsNullOrEmpty(sSessionCaptcha) Then
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: the text appearing in the image is not typed in the text field.</div>"
			txtField.Focus()
			Return True
		End If
		If txtField.Text.ToLower.Trim <> sSessionCaptcha.ToString.ToLower.Trim Then
			If Not lblError Is Nothing Then lblError.Text = "<div class=""error"">Error: the text typed in the text field does not match the text appearing in the picture.</div>"
			txtField.Focus()
			Return True
		End If
		Return False
	End Function
End Class