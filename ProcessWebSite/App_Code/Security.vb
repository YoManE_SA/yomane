Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Web
Imports System.Configuration

'
' 20080512 Tamir
'
' Security class - for page access control
'
Public Class Security
	Public Shared ReadOnly Property Username() As String
		Get
			Dim sMyUsername As String
			sMyUsername = HttpContext.Current.Request.ServerVariables("REMOTE_USER").ToLower()
			If sMyUsername.LastIndexOf("\") > 0 Then sMyUsername = sMyUsername.Substring(sMyUsername.LastIndexOf("\") + 1)
			Return sMyUsername
		End Get
	End Property

	Public Shared ReadOnly Property IsAdmin() As Boolean
		Get
			Return dbPages.ExecScalar("IF EXISTS (SELECT ID FROM tblSecurityUser WHERE su_IsAdmin=1 AND su_Username='" & dbPages.DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)")
		End Get
	End Property

	Public Shared ReadOnly Property Path() As String
		Get
			Dim sMyPath As String
			sMyPath = HttpContext.Current.Request.ServerVariables("PATH_INFO").ToLower()
			If sMyPath.LastIndexOf("/") > 0 Then sMyPath = sMyPath.Substring(sMyPath.LastIndexOf("/") + 1)
			If sMyPath.IndexOf("?") > 0 Then sMyPath = sMyPath.Substring(0, sMyPath.IndexOf("?") - 1)
			If sMyPath.IndexOf("#") > 0 Then sMyPath = sMyPath.Substring(0, sMyPath.IndexOf("#") - 1)
			Return sMyPath
		End Get
	End Property

	Public Shared ReadOnly Property FullPath() As String
		Get
			Dim sMyFullPath As String
			sMyFullPath = HttpContext.Current.Request.ServerVariables("PATH_INFO").ToLower()
			If HttpContext.Current.Request.ServerVariables("QUERY_STRING") <> String.Empty Then sMyFullPath &= "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
			If sMyFullPath.LastIndexOf("/") > 0 Then sMyFullPath = sMyFullPath.Substring(sMyFullPath.LastIndexOf("/") + 1)
			Return sMyFullPath
		End Get
	End Property

	Public Shared ReadOnly Property IsUserActive() As Boolean
		Get
			Return dbPages.ExecScalar("IF EXISTS (SELECT ID FROM tblSecurityUser WHERE su_IsActive=1 AND su_Username='" & dbPages.DBText(Username) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)")
		End Get
	End Property

	Public Shared ReadOnly Property IsDocumentManaged() As Boolean
		Get
			Return dbPages.ExecScalar("IF EXISTS (SELECT ID FROM tblSecurityDocument WHERE sd_URL='" & dbPages.DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)")
		End Get
	End Property

	Public Shared ReadOnly Property IsPermitted() As Boolean
		Get
			If Not IsUserActive Then Return False
			If Not IsDocumentManaged Then Return True
			Return dbPages.ExecScalar("IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument INNER JOIN tblSecurityDocumentGroup ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityGroup ON tblSecurityDocumentGroup.sdg_Group=tblSecurityGroup.ID INNER JOIN tblSecurityUserGroup ON tblSecurityGroup.ID=tblSecurityUserGroup.sug_Group INNER JOIN tblSecurityUser ON tblSecurityUserGroup.sug_User=tblSecurityUser.ID WHERE sdg_IsVisible=1 AND sug_IsMember=1 AND su_IsActive=1 AND sg_IsActive=1 AND su_Username='" & dbPages.DBText(Username) & "' AND sd_URL='" & dbPages.DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)")
		End Get
	End Property

	Public Shared ReadOnly Property IsActive() As Boolean
		Get
			If Not IsUserActive Then Return False
			If Not IsDocumentManaged Then Return True
			Return dbPages.ExecScalar("IF EXISTS (SELECT tblSecurityDocument.ID FROM tblSecurityDocument INNER JOIN tblSecurityDocumentGroup ON tblSecurityDocument.ID=tblSecurityDocumentGroup.sdg_Document INNER JOIN tblSecurityGroup ON tblSecurityDocumentGroup.sdg_Group=tblSecurityGroup.ID INNER JOIN tblSecurityUserGroup ON tblSecurityGroup.ID=tblSecurityUserGroup.sug_Group INNER JOIN tblSecurityUser ON tblSecurityUserGroup.sug_User=tblSecurityUser.ID WHERE sdg_IsVisible=1 AND sdg_IsActive=1 AND sug_IsMember=1 AND su_IsActive=1 AND sg_IsActive=1 AND su_Username='" & dbPages.DBText(Username) & "' AND sd_URL='" & dbPages.DBText(Path) & "') SELECT Cast(1 AS bit) ELSE SELECT Cast(0 AS bit)")
		End Get
	End Property

	Public Shared Sub CheckPermission(ByRef lblPermission As System.Web.UI.WebControls.Label, ByVal bRedirect As Boolean, ByVal sTitle As String)
		Dim bPermitted As Boolean = IsPermitted
		Dim bActive As Boolean = IsActive
		Dim bAdmin As Boolean = IsAdmin
		If String.IsNullOrEmpty(sTitle) Then sTitle = CType(HttpContext.Current.Handler, Page).Header.Title
		If bRedirect And Not bPermitted Then HttpContext.Current.Response.Redirect("security_message.aspx?PageURL=" & Path & "&Title=" & sTitle)
		If Not IsNothing(lblPermission) Then
			If bAdmin Then
				lblPermission.Text = "<s" & "cript language=""Javascript"" type=""text/javascript"" src=""security.js"">"
				lblPermission.Text &= "</s" & "cript>"
				lblPermission.Text &= "<span class=""security securityManaged"" title=""" & IIf(bActive, "Full Control", "Read Only") & ", " & IIf(IsDocumentManaged, "Managed", "Unmanaged") & " - click to manage permissions"" onclick=""OpenSecurityManager();"">"
			Else
				lblPermission.Text = "<span class=""security"" title=""" & IIf(bActive, "Full Control", "Read Only") & """>"
			End If
			lblPermission.Text &= IIf(IsDocumentManaged, "+", "-")
			lblPermission.Text &= "</span>"
			If bPermitted And Not bActive Then
				If Not bAdmin Then lblPermission.Text &= "<s" & "cript language=""Javascript"" type=""text/javascript"" src=""security.js"">"
				lblPermission.Text &= "<s" & "cript language=""Javascript"" type=""text/javascript"">"
				lblPermission.Text &= "addEvent(window, ""load"", DeactivateContent);"
				lblPermission.Text &= "</s" & "cript>"
			End If
		End If
	End Sub

	Public Shared Sub CheckPermission(ByRef lblPermissions As System.Web.UI.WebControls.Label, ByVal bRedirect As Boolean)
		CheckPermission(lblPermissions, bRedirect, String.Empty)
	End Sub

	Public Shared Sub CheckPermission(ByRef lblPermissions As System.Web.UI.WebControls.Label, ByVal sTitle As String)
		CheckPermission(lblPermissions, True, sTitle)
	End Sub

	Public Shared Sub CheckPermission(ByRef lblPermissions As System.Web.UI.WebControls.Label)
		CheckPermission(lblPermissions, True, String.Empty)
	End Sub

	Public Shared Sub CheckPermission(ByVal sTitle As String)
		CheckPermission(Nothing, True, sTitle)
	End Sub
End Class
