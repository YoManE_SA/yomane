Imports System
Imports System.Security.Cryptography
Imports System.Text

Public Class Md5Hash
	Public Shared Function HashBinary(ByVal bytesValue As Byte()) As Byte()
		Dim md5CSP As MD5 = New MD5CryptoServiceProvider
		Return md5CSP.ComputeHash(bytesValue)
	End Function

	Public Shared Function HashBinarySha256(ByVal bytesValue As Byte()) As Byte()
		Dim shaCSP As SHA256 = New SHA256Managed
		Return shaCSP.ComputeHash(bytesValue)
	End Function

	Public Shared Function HashASCII(ByVal sValue As String) As String
		Dim bytesValue As Byte() = System.Text.Encoding.ASCII.GetBytes(sValue)
		Dim bytesHash As Byte() = HashBinary(bytesValue)
		Return System.Text.Encoding.ASCII.GetString(bytesHash)
	End Function

	Public Shared Function HashASCIIBase64(ByVal sValue As String) As String
		Dim bytesValue As Byte() = System.Text.Encoding.ASCII.GetBytes(sValue)
		Dim bytesHash As Byte() = HashBinary(bytesValue)
		Return System.Convert.ToBase64String(bytesHash)
	End Function

	Public Shared Function HashUTF8(ByVal sValue As String) As String
		Dim bytesValue As Byte() = System.Text.Encoding.UTF8.GetBytes(sValue)
		Dim bytesHash As Byte() = HashBinary(bytesValue)
		Return System.Text.Encoding.UTF8.GetString(bytesHash)
	End Function

	Public Shared Function HashMD5UTF8Base64(ByVal sValue As String) As String
		Dim bytesValue As Byte() = System.Text.Encoding.UTF8.GetBytes(sValue)
		Dim bytesHash As Byte() = HashBinary(bytesValue)
		Return System.Convert.ToBase64String(bytesHash)
	End Function

	Public Shared Function HashSHA256UTF8Base64(ByVal sValue As String) As String
		Dim bytesValue As Byte() = System.Text.Encoding.UTF8.GetBytes(sValue)
		Dim bytesHash As Byte() = HashBinarySha256(bytesValue)
		Return System.Convert.ToBase64String(bytesHash)
	End Function

End Class