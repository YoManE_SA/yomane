Imports Microsoft.VisualBasic
Public Class IPManager
	Public Shared Function IsValidIP(ByVal sIP As String) As Boolean
		If Not Regex.IsMatch(sIP, "^(\d{1,3}\.){3}\d{1,3}$") Then Return False
		For Each nNumber As Integer In sIP.Split(".")
			If nNumber > 255 Then Return False
		Next
		Return True
	End Function

	Public Shared Function IsValidChar(ByVal sDigit As String) As Boolean
		If sDigit.Length <> 1 Then Return False
		Return "1234567890./*".Contains(sDigit)
	End Function

	Public Shared Function IsValidCIDR(ByVal sCIDR As String) As Boolean
		If Not Regex.IsMatch(sCIDR, "^(\d{1,3}\.){3}\d{1,3}/[23][45678901]$") Then Return False
		For Each nNumber As Integer In sCIDR.Substring(0, sCIDR.IndexOf("/")).Split(".")
			If nNumber > 255 Then Return False
		Next
		Dim nRange As Integer = sCIDR.Substring(sCIDR.IndexOf("/") + 1)
		If nRange < 24 Or nRange > 31 Then Return False
		Dim nAddresses As Integer = Math.Pow(2, 32 - nRange)
		Dim nBase As Integer = sCIDR.Substring(sCIDR.LastIndexOf(".") + 1, sCIDR.IndexOf("/") - (sCIDR.LastIndexOf(".") + 1))
		Dim nRemainder As Integer
		Math.DivRem(nBase, nAddresses, nRemainder)
		If nRemainder > 0 Then Return False
		Return True
	End Function

	Public Shared Function IsInCIDR(ByVal sCIDR As String, ByVal sIP As String) As Boolean
		If Not IsValidIP(sIP) Then Return False
		If Not IsValidCIDR(sCIDR) Then Return False
		If sIP = sCIDR.Substring(0, sCIDR.IndexOf("/") - 1) Then Return True
		If sIP.Substring(0, sIP.LastIndexOf(".")) <> sCIDR.Substring(0, sCIDR.LastIndexOf(".")) Then Return False
		Dim nRange As Integer = sCIDR.Substring(sCIDR.IndexOf("/") + 1)
		Dim nAddresses As Integer = Math.Pow(2, 32 - nRange)
		Dim nBase As Integer = sCIDR.Substring(sCIDR.LastIndexOf(".") + 1, sCIDR.IndexOf("/") - (sCIDR.LastIndexOf(".") + 1))
		For i As Integer = nBase + 1 To nBase + nAddresses - 1
			If sIP = sCIDR.Substring(0, sCIDR.LastIndexOf(".") + 1) & i Then Return True
		Next
		Return False
	End Function

	Public Shared Function ContainsIP(ByVal sIPs As String, ByVal sIP As String) As Boolean
		sIP = sIP.Trim
		If Not IsValidIP(sIP) Then Return False
		Dim sbIPs As New StringBuilder(sIPs)
		For i As Integer = 0 To sbIPs.Length - 1
			If Not IsValidChar(sbIPs.Chars(i)) Then sbIPs.Replace(sbIPs.Chars(i), ";")
		Next
		Do While Regex.IsMatch(sbIPs.ToString, ";;")
			sbIPs.Replace(";;", ";")
		Loop
		sIPs = sbIPs.ToString
		If IsValidIP(sIPs) Then Return sIPs = sIP
		For Each sItemIP As String In sIPs.Split(";")
			If Not String.IsNullOrEmpty(sItemIP) Then
				If sItemIP.EndsWith(".*") Then
					If sItemIP.Substring(0, sItemIP.IndexOf(".*")) = sIP.Substring(0, sIP.LastIndexOf(".")) Then Return True
				Else
					If sItemIP = sIP Then Return True
					If IsInCIDR(sItemIP, sIP) Then Return True
				End If
			End If
		Next
		Return False
	End Function
End Class