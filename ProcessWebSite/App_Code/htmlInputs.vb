Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Public Class htmlInputs
 Inherits System.Web.UI.Page

    Public Function PutRecordsetComboDefault(ByVal sSqlString As String, ByVal selectName As String, ByVal selectExtras As String, _
        ByVal optionValue As String, ByVal optionText As String, ByVal optionValueDefault As String, ByVal optionTextDefault As String) As String

        Dim sSelected As String
        Dim bHasRows As Boolean
        Dim iReaderTmp As SqlDataReader = dbPages.ExecReader(sSqlString)
        bHasRows = iReaderTmp.HasRows
        If bHasRows Then
            Response.Write("<select name=""" & selectName & """ id=""" & selectName & """ " & selectExtras & ">")
            If optionValueDefault <> "false" And optionTextDefault <> "false" Then
                Response.Write("<option value=""" & optionValueDefault & """>" & optionTextDefault & "</option>")
            End If
            While iReaderTmp.Read()
                sSelected = ""
                If Trim(iReaderTmp(optionValue)) = Trim(Request(selectName)) Then sSelected = " selected"
                Response.Write("<option value=""" & iReaderTmp(optionValue) & """" & sSelected & ">" & iReaderTmp(optionText) & "</option>")
            End While
        Response.Write("</select>")
        End If
        iReaderTmp.Close()
        Return bHasRows

    End Function
    
    Public Sub PutComboFromList(ByVal selectName As String, ByVal selectExtras As String, ByVal optionDelimiter As String, _
        ByVal optionValueList As String, ByVal optionTextList As String, ByVal optionValueDefault As String, ByVal optionTextDefault As String)
        Dim sSelected As String
        Response.Write("<select name=""" & selectName & """ id=""" & selectName & """ " & selectExtras & ">")
        If optionValueDefault <> "false" And optionTextDefault <> "false" Then
            Response.Write("<option value=""" & optionValueDefault & """>" & optionTextDefault & "</option>")
        End If
        Dim xValues As String() = Split(optionValueList, optionDelimiter)
        Dim xText As String() = Split(optionTextList, optionDelimiter)
        For i As Integer = 0 To xValues.Length - 1
            sSelected = ""
            If Trim(xValues(i)) = Trim(Request(selectName)) Then sSelected = " selected"
            Response.Write("<option value=""" & xValues(i) & """" & sSelected & ">" & xText(i) & "</option>")
        Next
        Response.Write("</select>")
    End Sub

    Public Sub PutRecordsetComboDefault2()
        Response.Write("sdfsdf") 
    End Sub

End Class
