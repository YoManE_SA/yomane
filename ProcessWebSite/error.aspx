<%@ Page Language="VB" %>
<script runat="server">
    Public Function ToDB(nLength As Int16, sString As String) As String
        If sString = String.Empty Then Return String.Empty
        Return sString.Replace("'", "`").PadRight(nLength).Substring(0, nLength).Trim()
    End Function

    Public Function FormatTargetSite(t As Reflection.MethodBase) As String
        If t Is Nothing Then Return ""
        Return t.ToString()
    End Function

    Public Function FormatMultiLine(t As String) As String
        If t Is Nothing Then Return ""
        Return t.Replace(vbCrLf, "<br />")
    End Function


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim appException As System.Exception = Server.GetLastError()
        If appException Is Nothing Then appException = New Exception("Empty exception")
        Dim innerException As System.Exception = appException.InnerException
        If innerException Is Nothing Then innerException = New Exception("Empty inner exception")
        Dim sbSQL As System.Text.StringBuilder = New System.Text.StringBuilder("INSERT INTO tblErrorNet (ProjectName, RemoteIP, LocalIP, RemoteUser, ServerName, ServerPort, ScriptName, RequestQueryString, RequestForm, VirtualPath, PhysicalPath, ExceptionSource, ExceptionMessage, ExceptionTargetSite, ExceptionStackTrace, ExceptionHelpLink, InnerExceptionSource, InnerExceptionMessage, InnerExceptionTargetSite, InnerExceptionStackTrace, InnerExceptionHelpLink) VALUES (")
        sbSQL.Append("'" & ToDB(25, "Process") & "'")
        sbSQL.Append(", '" & ToDB(25, Request.ServerVariables("REMOTE_ADDR")) & "'")
        sbSQL.Append(", '" & ToDB(25, Request.ServerVariables("LOCAL_ADDR")) & "'")
        sbSQL.Append(", '" & ToDB(50, Request.ServerVariables("REMOTE_USER")) & "'")
        sbSQL.Append(", '" & ToDB(50, Request.ServerVariables("SERVER_NAME")) & "'")
        sbSQL.Append(", '" & ToDB(5, Request.ServerVariables("SERVER_PORT")) & "'")
        sbSQL.Append(", '" & ToDB(50, Request.ServerVariables("SCRIPT_NAME")) & "'")
        sbSQL.Append(", '" & ToDB(500, Request.QueryString.ToString()) & "'")
        sbSQL.Append(", '" & ToDB(500, Request.Form.ToString()) & "'")
        sbSQL.Append(", '" & ToDB(50, Request.ServerVariables("PATH_INFO")) & "'")
        sbSQL.Append(", '" & ToDB(100, Request.ServerVariables("PATH_TRANSLATED")) & "'")
        sbSQL.Append(", '" & ToDB(100, appException.Source) & "'")
        sbSQL.Append(", '" & ToDB(500, appException.Message) & "'")
        sbSQL.Append(", '" & ToDB(100, FormatTargetSite(appException.TargetSite)) & "'")
        sbSQL.Append(", '" & ToDB(1000, appException.StackTrace) & "'")
        sbSQL.Append(", '" & ToDB(100, appException.HelpLink) & "'")
        sbSQL.Append(", '" & ToDB(100, innerException.Source) & "'")
        sbSQL.Append(", '" & ToDB(500, innerException.Message) & "'")
        sbSQL.Append(", '" & ToDB(1000, FormatTargetSite(innerException.TargetSite)) & "'")
        sbSQL.Append(", '" & ToDB(1000, innerException.StackTrace) & "'")
        sbSQL.Append(", '" & ToDB(100, innerException.HelpLink) & "'")

        lblError.Text = lblError.Text & "<hr />Remote IP: " & Request.ServerVariables("REMOTE_ADDR")
        lblError.Text = lblError.Text & "<hr />Local IP: " & Request.ServerVariables("LOCAL_ADDR")
        lblError.Text = lblError.Text & "<hr />Remote User: " & Request.ServerVariables("REMOTE_USER")
        lblError.Text = lblError.Text & "<hr />Server: " & Request.ServerVariables("SERVER_NAME")
        lblError.Text = lblError.Text & "<hr />Server Port: " & Request.ServerVariables("SERVER_PORT")
        lblError.Text = lblError.Text & "<hr />Script: " & Request.ServerVariables("SCRIPT_NAME")
        lblError.Text = lblError.Text & "<hr />Query String: " & Request.QueryString.ToString()
        lblError.Text = lblError.Text & "<hr />Form: " & Request.Form.ToString()
        lblError.Text = lblError.Text & "<hr />Virtual Path: " & Request.ServerVariables("PATH_INFO")
        lblError.Text = lblError.Text & "<hr />Physical Path: " & Request.ServerVariables("PATH_TRANSLATED")
        lblError.Text = lblError.Text & "<hr />Exception Source: " & appException.Source
        lblError.Text = lblError.Text & "<hr />Exception Message: " & FormatMultiLine(appException.Message)
        lblError.Text = lblError.Text & "<hr />Exception Target Site: " & FormatTargetSite(appException.TargetSite)
        lblError.Text = lblError.Text & "<hr />Exception Stack Trace: " & FormatMultiLine(appException.StackTrace)
        lblError.Text = lblError.Text & "<hr />Exception Help Link: " & appException.HelpLink
        lblError.Text = lblError.Text & "<hr />Inner Exception Source: " & innerException.Source
        lblError.Text = lblError.Text & "<hr />Inner Exception Message: " & FormatMultiLine(innerException.Message)
        lblError.Text = lblError.Text & "<hr />Inner Exception Target Site: " & FormatTargetSite(innerException.TargetSite)
        lblError.Text = lblError.Text & "<hr />Inner Exception Stack Trace: " & FormatMultiLine(innerException.StackTrace)
        lblError.Text = lblError.Text & "<hr />Inner Exception Help Link: " & innerException.HelpLink

        sbSQL.Append(");")
        Dim dbErrors As New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ErrorsDSN").ConnectionString)
        Dim cmdErrors As New System.Data.SqlClient.SqlCommand(sbSQL.ToString(), dbErrors)

        Dim config As System.Configuration.Configuration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~")
        Dim configSection As System.Web.Configuration.CompilationSection = CType(config.GetSection("system.web/compilation"), System.Web.Configuration.CompilationSection)
        dbErrors.Open()
        cmdErrors.ExecuteNonQuery()

        Try
            If configSection.Debug Then
                lblError.Text = "Unexpected system error occurred." & lblError.Text
            Else
                lblError.Text = "Unexpected system error occurred."
            End If
        Catch ex As Exception
            Dim sPath As String : sPath = "C:\ErrorNet\" & Right(Year(Now()).ToString(), 4) & Right("0" + Month(Now()).ToString(), 2) & Right("0" + Day(Now()).ToString(), 2) & Right("0" + Hour(Now()).ToString(), 2) & Right("0" + Minute(Now()).ToString(), 2) & Right("0" + Second(Now()).ToString(), 2) & "_" & Request.ServerVariables("REMOTE_ADDR").Replace(".", "-") & ".sql"
            Dim swSQL As New System.IO.StreamWriter(sPath)
            sbSQL.Replace("tblErrorNet (", "tblErrorNet (IsFailedSQL, ").Replace(") VALUES (", ") VALUES (1, ")
            swSQL.Write(sbSQL.ToString())
            swSQL.Close()
            swSQL.Dispose()
            If configSection.Debug Then
                lblError.Text = "Unexpected system error cannot be handled." & lblError.Text
            Else
                lblError.Text = "Unexpected system error cannot be handled."
            End If
        Finally
            dbErrors.Close()
            dbErrors.Dispose()
            cmdErrors.Dispose()
        End Try
        Server.ClearError()
    End Sub
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<script runat="server">
</script>
<head runat="server">
    <title>Server Error</title>
</head>
<body>
	<asp:Label runat="server" ID="lblError"></asp:Label>
</body>
</html>