<%@ Application Language="VB" %>
<script runat="server">
    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)

        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12

        Dim app = TryCast(sender, System.Web.HttpApplication)
        If (app IsNot Nothing And app.Context IsNot Nothing) Then
            app.Context.Response.Headers.Remove("Server")
        End If
    End Sub
</script>