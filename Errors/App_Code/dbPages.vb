Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Web
Imports System.Configuration

Public Class dbPages
    'Inherits System.Web.UI.Page
    Public Shared Function getConfig(sKey as string) as String 
        return ConfigurationManager.AppSettings.Item(sKey)
    End Function  

    Public Shared ReadOnly Property DSN() As String
        Get
			return ConfigurationManager.ConnectionStrings("DSN").ConnectionString
        End Get
    End Property

    Public Shared Function ExecSql(ByVal sqlStr As String) As Long
        Dim DBCon As New System.Data.SqlClient.SqlConnection(DSN)
        Dim DBCom As New System.Data.SqlClient.SqlCommand(sqlStr, DBCon)
        Try
            DBCon.Open()
            ExecSql = DBCom.ExecuteNonQuery()
        Catch
            With System.Web.HttpContext.Current
                .Response.Write(sqlStr)
                .Response.Flush()
            End With
            Throw
        Finally
            DBCon.Close()
            DBCom.Dispose()
            DBCon.Dispose()
        End Try
    End Function

    Public Shared Function ExecScalar(ByVal sqlStr As String) As Object
        Dim DBCon As New SqlConnection(DSN)
        Dim DBCom As New SqlCommand(sqlStr, DBCon)
        Try
            DBCon.Open()
            ExecScalar = DBCom.ExecuteScalar()
        Catch
            With System.Web.HttpContext.Current
                .Response.Write(sqlStr)
                .Response.Flush()
            End With
            Throw
        Finally
            DBCon.Close()
            DBCom.Dispose()
            DBCon.Dispose()
        End Try
    End Function

    Public Shared Function ExecReader(ByVal sqlStr As String) As SqlDataReader
        Dim DBCon As New SqlConnection(DSN)
        Dim DBCom As New SqlCommand(sqlStr, DBCon)
        Try
            DBCon.Open()
            ExecReader = DBCom.ExecuteReader(System.Data.CommandBehavior.CloseConnection)
        Catch
            With System.Web.HttpContext.Current
                .Response.Write(sqlStr)
                .Response.Flush()
            End With
            Throw
        Finally
            DBCom.Dispose()
        End Try
    End Function
    
    Public Shared Function ExecDataset(ByVal sqlStr As String, lPageSize as Integer, lPage as Integer) As System.Data.DataSet
        Dim DBCon As New SqlConnection(DSN)
		Dim iAdpt As New SqlDataAdapter(sqlStr, DBCon)
		Dim pRet as New System.Data.DataSet()
        Try
            DBCon.Open()
            if lPageSize <= 0 Then iAdpt.Fill(pRet) _
            Else iAdpt.Fill(pRet, lPageSize * lPage, lPageSize + 1, "Table1")
        Catch
            With System.Web.HttpContext.Current
                .Response.Write(sqlStr)
                .Response.Flush()
            End With
            Throw
        Finally
            DBCon.Close()
        End Try
		Return pRet
	End Function
	
    Public Shared Function dbtextShowForm(ByVal sText As String) As String
        If sText Is Nothing Then Return ""
        Return sText.Trim.Replace("``", """").Replace("`", "'")
    End Function
    
    Public Shared Function dbtextShow(ByVal sText As String) As String
        If sText Is Nothing Then Return ""
        Return sText.Trim.Replace("``", """").Replace("`", "'").Replace(VbCr, "<br>")
    End Function

    Public Shared Function DBText(ByVal sText As String) As String
        If sText Is Nothing Then Return ""
        Return sText.Trim.Replace("'", "`").Replace("""", "``").Replace("<", "&lt;").Replace(">", "&gt;")
    End Function
    

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Integer, ByVal lMax As Integer, ByVal lDef As Integer) As Integer
        If (Not xval Is Nothing) And IsNumeric(xval) Then
	        Try
    	        TestVar = Integer.Parse(xval)
	        Catch
                TestVar = lDef
    	    End Try
            If (lMin <= lMax) And ((TestVar < lMin) Or (TestVar > lMax)) Then TestVar = lDef
        Else
            TestVar = lDef
        End If
    End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Decimal, ByVal lMax As Decimal, ByVal lDef As Decimal) As Decimal
        Dim rVal As Decimal = lDef
        Try
            rVal = Decimal.Parse(xval.ToString())
            If ((lMin <= lMax) And ((rVal < lMin) Or (rVal > lMax))) Then rVal = lDef
        Catch
        End Try
        Return rVal
    End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lMax As Integer, ByVal lDef As String) As String
        Dim rVal As String = lDef
        Try
            rVal = xval.ToString()
            If (rVal.Length > lMax) Then rVal = rVal.Substring(0, lMax)
        Catch
        End Try
        Return rVal
    End Function

     Public Shared Function TestVar(ByVal xval As Object, ByVal lMin As Date, ByVal lMax As Date, ByVal lDef As Date) As Date
        Dim rVal As Date = lDef
        Try
            rVal = Date.Parse(xval.ToString())
            If ((lMin <= lMax) And ((rVal < lMin) Or (rVal > lMax))) Then rVal = lDef
        Catch
        End Try
        Return rVal
    End Function

	Public Shared Function TestNumericList(xStr As String, ByRef xCount As Integer) As String 
		Dim xStrList As String() = xStr.Split(",") : xCount = 0
		Dim strOut As String  = ""
		For i As Integer = 0 To xStrList.Length - 1
			If (Not xStrList(i) Is Nothing) And IsNumeric(xStrList(i)) Then
				Try
    				Dim xVal As Integer = Integer.Parse(xStrList(i))
					strOut &= xVal & "," : xCount += 1
				Catch
    			End Try
    		End if	
		Next
		If strOut.Length > 1 Then strOut = strOut.Substring(0, strOut.Length - 1)
		Return strOut
	End Function

    Public Shared Function GetFileText(ByVal sFileName As String) As String
        Dim output As String = ""
        If System.IO.File.Exists(sFileName) Then
            Dim sr As New System.IO.StreamReader(sFileName)
            output = sr.ReadToEnd()
            sr.Close()
        End If
        Return output
    End Function
    
    Public Shared Function SendEmail(ByVal sMailSubject As String, ByVal sMailString As String, ByVal pReader As SqlDataReader, ByVal pData As System.Collections.Specialized.NameValueCollection) As Boolean
        Dim xClt As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient(TestVar(getConfig("SMTPServer"), -1, ""))
        if Not pData Is Nothing Then
			For i As Integer = 0 To pData.Count - 1
				sMailString = sMailString.Replace("%" & pData.GetKey(i) & "%", pData(i))
			Next
		End if
        if Not pReader Is Nothing Then
			For i As Integer = 0 To pReader.FieldCount - 1
				sMailString = sMailString.Replace("%" & pReader.GetName(i) & "%", pReader(i).ToString())
			Next
		End if
		'sMailString = sMailString
		Dim sCss As String = "<style>" & GetFileText(HttpContext.Current.Server.MapPath("../EmailStyle.css")) & "</style>"
		sMailString = "<html><head></head>" & sCss & "<body>" & sMailString & "</body></html>"
        Dim xMsg As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage(TestVar(getConfig("MailFrom"), -1, ""), pData("MailTo"))
        xMsg.IsBodyHtml = True
        xMsg.Subject = sMailSubject
        xMsg.Body = sMailString
        If dbPages.TestVar(getConfig("SMTPUser"), -1, "") <> "" Then _
			xClt.Credentials = New System.Net.NetworkCredential(getConfig("SMTPUser"), getConfig("SMTPPassword"))
		Try	
			xClt.Send(xMsg)
		Catch
			Return False
		End Try	
        Return True
    End Function

    Public Shared Sub showDivBox(fType as String, fDir as String, fWidth as String, fText as String) '(help;note, ltr,rtl)
        Dim sStyle As String, sClass As String, sOnMouseOver As String, sOnMouseOut As String
        Select case Trim(fType)
            Case "help" : sStyle = "width:" & fWidth & "px; background-color:#ECEFFF; border:1px solid Navy;"
            Case else : sStyle = "width:" & fWidth & "px; background-color:#ffffe6; border:1px solid #353535;"
        End Select
        Select case Trim(fDir)
            Case "ltr" : sClass = "txt11"
            Case else : sClass = "txt12"
        End Select
        sOnMouseOver = "previousSibling.style.visibility='visible'; previousSibling.style.left=this.offsetLeft+35; previousSibling.style.top=this.style.top;"
        sOnMouseOut = "previousSibling.style.visibility='hidden';"
        HttpContext.Current.Response.Write("<div class=""" & sClass & """ style=""direction:" & fDir & "; visibility:hidden; position:absolute; z-index:1; padding:3px; " & sStyle & """>" & fText & "</div>")
        HttpContext.Current.Response.Write("<img src=""../Images/iconQuestionGrayS.gif"" align=""middle"" border=""0"" style=""cursor:help;"" onmouseover=""" & sOnMouseOver & """ onmouseout=""" & sOnMouseOut & """ />")
    End Sub
    
    Public Shared Sub showFilter(fText As String, fUrl As String, fDist As Integer)
        Dim sSibling As String = "", sOnMouseOver As String, sOnMouseOut As String
        For i As Integer = 1 To fDist
            sSibling = sSibling & "nextSibling."
        Next
        sOnMouseOver = sSibling & "style.visibility='visible';"
        sOnMouseOut = sSibling & "style.visibility='hidden';"
        HttpContext.Current.Response.Write("<a href=""" & fUrl & """ onmouseover=""" & sOnMouseOver & """ onmouseout=""" & sOnMouseOut & """ style=""text-decoration:none; border-bottom:1px dotted #0CB10F;"">" & fText & "</a>")
        HttpContext.Current.Response.Write("<span style=""visibility:hidden;""> <img src=""../Images/icon_filterPlus.gif"" align=""middle""></span>")
    End Sub
    
    Public Shared Function SetUrlValue(qString As String, strParam As String, sValue As String) As String
        Dim idx As Integer, idxEnd As Integer
	    SetUrlValue = qString
	    idx = InStr(1, SetUrlValue, strParam & "=")
	    If(idx < 1) Then 
	        If sValue = Nothing Then Return qString
            Return qString & "&" & strParam & "=" & sValue
        End If
	    idxEnd = InStr(idx, SetUrlValue, "&")
	    If(idxEnd < 1) Then idxEnd = Len(SetUrlValue)
	    SetUrlValue = Left(SetUrlValue, idx - 1) & Right(SetUrlValue, Len(SetUrlValue) - idxEnd) & "&" & strParam & "=" & sValue
    End Function

    Public Shared Function CleanUrl(Request As System.Collections.Specialized.NameValueCollection) As String
        Dim strOut As String = ""
        For i as Integer = 0 To Request.Count - 1
            if Request(i) <> "" Then _
                strOut &= "&" & Request.GetKey(i) & "=" & Request(i)
        Next
        Return strOut
    End Function
End Class
