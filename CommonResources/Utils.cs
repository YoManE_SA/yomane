﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.CommonResources
{
    public class Utils
    {
        private static Dictionary<string, ResourceManager> _resources = new Dictionary<string, ResourceManager>();

        public static ResourceManager GetResource(string resourceName)
        {
            if (resourceName == null || resourceName.Trim() == "")
                throw new ArgumentException("resourceName cannot be null or empty.");

            if (_resources == null)
                throw new ApplicationException("_resourceManagers is null.");

            if (!_resources.ContainsKey(resourceName))
            {
                lock (_resources)
                {
                    if (!_resources.ContainsKey(resourceName))
                    {
                        ResourceManager resourceManager = new ResourceManager("Netpay.CommonResources.Resources." + resourceName, Assembly.GetExecutingAssembly());
                        if (resourceManager == null)
                            throw new ApplicationException("unable to create resource: Netpay.CommonResources.Resources." + resourceName);

                        _resources.Add(resourceName, resourceManager);
                    }
                }
            }

            return _resources[resourceName];
        }

        public static string GetResourceValue(string resourceName, string resourceKey)
        {
            return GetResourceValue(resourceName, resourceKey, System.Threading.Thread.CurrentThread.CurrentUICulture);
        }

        public static string GetResourceValue(string resourceName, string resourceKey, CultureInfo culture) 
        {
            ResourceManager rm = GetResource(resourceName);
            if (culture == null)
                culture = CultureInfo.CurrentCulture;
            string value = rm.GetString(resourceKey, culture);
            if (value == null)
                value = resourceKey;

            return value;
        }
    }
}
