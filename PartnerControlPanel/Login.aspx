﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Netpay.PartnerControlPanel.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:Login.aspx, Title %>" /></title>

    <!-- css -->
    <link href="Plugins/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700' rel='stylesheet' type='text/css' />
    <link href="Plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- js -->
    <script type="text/javascript" src="Plugins/FormEnterSubmit/FormEnterSubmit.js"></script>

    <!-- jQuery UI -->
    <script type="text/javascript" src="Plugins/jquery/jquery-1.11.3.js"></script>

    <!-- Bootstrap js -->
    <script src="Plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <link id="lnkIcon" runat="server" rel="icon" type="image/x-icon" />
    <script type="text/javascript">
        if (navigator.appVersion.indexOf("MSIE 8") == -1 || navigator.appVersion.indexOf("MSIE 7") == -1) {
            document.createElement('header');
            document.createElement('nav');
            document.createElement('section');
            document.createElement('footer');
        }

    </script>
    <script>
        $(document).ready(function () {
            $('#loginUsername').change(function () {
                var text_value = $('#loginUsername').val();
                if (!text_value.match(/^[A-Za-z\d_-]+$/)) {
                    $('#loginUsername').value = "";
                    alert("Characters must be in english");

                }
            });
        });
    </script>
    <netpay:ScriptManager ID="ScriptManager1" runat="server" EnableSessionAlerts="true" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager" />
        <div id="Div1" class="wrapper" runat="server">
            <div class="language-selector">
                <ul>
                    <li><a class="hyperlink" href="javascript:window.location = netpay.Common.setURLValue(window.location.href, 'culture', 'en-us')"><i class="fa fa-globe"><span class="font-arial">English</span></i></a></li>
                    <li><a class="hyperlink" href="javascript:window.location = netpay.Common.setURLValue(window.location.href, 'culture', 'he-il')"><i class="fa fa-globe"><span class="font-arial">עברית</span></i></a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="wrap-login-box">
                <div class="login-box">
                    <header class="header-top">
                        <div class="logo">
                            <img alt="Logo LTD" src="<%= WebUtils.MapTemplateVirPath(this, "images/login/logo.png")%>" />
                        </div>
                    </header>
                    <div>
                        <hr class="style-line margin-10-top" />
                    </div>
                    <section id="loginForm" onkeydown="return FormEnterSubmit(this, event, 'btnLogin')">
                        <asp:Label ID="lblError" runat="server" CssClass="padding-10" />
                        <asp:ValidationSummary ID="vsLogin" runat="server" ValidationGroup="loginControl" EnableClientScript="true" DisplayMode="BulletList" ShowMessageBox="true" ShowSummary="false" />
                        <div class="line-height-30">
                            <i class="fa fa-envelope"></i>
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:Login.aspx, Mail %>" />
                            <asp:TextBox ID="loginEmail" ClientIDMode="Static" runat="server" TabIndex="1" ValidationGroup="loginControl" AutoCompleteType="None" CssClass="width-100-precent" autocomplete="off" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="loginEmail" ValidationGroup="loginControl" ErrorMessage="<%$ Resources:Login.aspx, EmailAddressIsMissing %>" Display="None" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="loginEmail" ValidationGroup="loginControl" ErrorMessage="<%$ Resources:Login.aspx, EmailAddressIsInvalid %>" Display="None"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\w" />
                        </div>
                        <%--<div class="line-height-30 margin-10-top ">
                            <i class="fa fa-user"></i>
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:Login.aspx, Username %>" />
                            <asp:TextBox ID="loginUsername" ClientIDMode="Static" TabIndex="2" runat="server" ValidationGroup="loginControl" AutoCompleteType="None" CssClass="width-100-precent" autocomplete="off" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="loginUsername" ValidationGroup="loginControl" ErrorMessage="<%$ Resources:Login.aspx, UsernameIsMissing %>" Display="None" />
                        </div>--%>
                        <div class="line-height-30 margin-10-top">
                            <i class="fa fa-unlock-alt"></i>
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:Login.aspx, Password %>" />
                            <asp:TextBox ID="loginPassword" TextMode="Password" ClientIDMode="Static" TabIndex="3" runat="server" ValidationGroup="loginControl" CssClass="width-100-precent" AutoCompleteType="None" autocomplete="off" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="loginPassword" ValidationGroup="loginControl" ErrorMessage="<%$ Resources:Login.aspx, PasswordIsMissing %>" Display="None" />
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="line-height-30 margin-10-top align-left">
                                    <asp:LinkButton runat="server" ID="btnForgotPassword" Text="Forgot password?">
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="line-height-30 margin-10-top align-right">
                                    <asp:LinkButton ID="btnLogin" ClientIDMode="Static" TabIndex="4" OnClick="btnLogin_Click" CssClass="btn btn-primary" runat="server">
                                        <asp:Localize ID="Localize1331" runat="server" Text="<%$ Resources:Login.aspx, ButtonLogin %>" />
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
            <div>
                <img src="images/shaddow.png" />
            </div>


            <div class="push"></div>
        </div>
        <footer class="footer">
            <div class="wrap-footer">
                <div class="grid-1">
                    <img src="<%= WebUtils.MapTemplateVirPath(this, "images/login/logo-small.png")%>" />
                </div>
                <div class="grid-2">
                    <asp:Localize ID="ltCopyright" runat="server" Text="<%$ Resources:Login.aspx, copyright %>" />
                </div>
                <div class="grid-3">
                    <ul>
                        <li><a class="footer-link" href='<%# string.Format("http://www.{0}/contact.html", Domain.Current.Host) %>'>
                            <asp:Localize runat="server" Text="<%$ Resources:Login.aspx,  txtContactUs %>" /></a></li>
                        <li><a class="footer-link" href='<%# string.Format("http://www.{0}/privacy-policy.html", Domain.Current.Host) %>'>
                            <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, txtPolicyPrivacy %>" /></a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </footer>

        <%-- enforce change password modal dialog --%>
        <netpay:ModalDialog OnDataBinding="dlgChangePassword_DataBinding" OnDialogClose="dlgChangePassword_DialogClose" ActiveViewIndex='<%# WebUtils.IsLoggedin? 0 : 1 %>' runat="server" ID="dlgChangePassword" Title="Change Password">
            <Body>
                <asp:MultiView ID="mvChangePass" runat="server">
                    <asp:View runat="server">
                        <div class="alert alert-danger">
                            We recommend you to change your password after it was reset.
                        </div>
                    </asp:View>
                    <asp:View runat="server">
                        <div class="alert alert-danger">
                            Password is too old. Policy requires changing it.
                            <br />
                            Please insert a new password and than log in.
                        </div>
                    </asp:View>
                </asp:MultiView>
                <netpay:ActionNotify runat="server" ID="acnPasswordMessage" />
                <div class="form-group">
                    New Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtNewPassword" TextMode="Password" />
                </div>
                <div class="form-group">
                    Confirm Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtConfirmPassword" TextMode="Password" />
                </div>
            </Body>
            <Footer>
                <asp:Button runat="server" ID="btnClose" CssClass="btn btn-inverse" Text="Cancel" OnClientClick='<%# dlgChangePassword.HideJSCommand %>' />
                <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary" Text="Save" UseSubmitBehavior="false" OnClick="UpdatePassword_Click" />
            </Footer>
        </netpay:ModalDialog>
        <netpay:ModalDialog ID="ConfirmationModalDialog" Title="Forgot your password?" runat="server">
            <Body>
                Are you sure you want us to generate a new password,
                And send it to you via email?
            </Body>
            <Footer>
                <asp:Button UseSubmitBehavior="false" OnClick="btnForgotPassword_Click" runat="server" Text="Confirm" />
                <asp:Button ID="btnCancelReset" runat="server" Text="Cancel" />
            </Footer>
        </netpay:ModalDialog>
    </form>
</body>
</html>


