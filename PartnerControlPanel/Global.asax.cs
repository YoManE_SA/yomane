﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Threading;
using System.Globalization;

namespace Netpay.PartnerControlPanel
{
	public class Global : System.Web.HttpApplication
	{
		protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
		{
			bool updateCookie = true;
			HttpCookie requestCookie = Request.Cookies["UICulture"];
			string culture = Request.QueryString["culture"];

			if (string.IsNullOrEmpty(culture) && requestCookie != null)
				culture = requestCookie.Value;
			if (string.IsNullOrEmpty(culture))
				culture = "en-us";		
			if (requestCookie != null)
				updateCookie = requestCookie.Value != culture;

			try 
			{	        
				Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture); 
				if (updateCookie)
				{
					requestCookie = new HttpCookie("UICulture", culture);
					Response.Cookies.Set(requestCookie);
				}
			}
			catch
			{
				Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
			}
		}

        void Application_BeginRequest(object sender, EventArgs e)
        {
            var application = sender as HttpApplication;
            if (application != null && application.Context != null)
            {
                application.Context.Response.Headers.Remove("Server");
            }
        }

        protected void Application_Start(object sender, EventArgs e)
		{
			Netpay.Web.ContextModule.AppName = "Partner Control Panel";
		}

		protected void Application_Error(object sender, EventArgs e)
		{
			Exception ex = Server.GetLastError();
			if (ex is System.Web.HttpException)
			{
				if (ex.Message.StartsWith("Validation of viewstate MAC failed.")){
					Response.Redirect("~/Login.aspx");
					return;
				}
			}
			Netpay.Infrastructure.Logger.Log(ex);
		}

		void Application_AcquireRequestState(object sender, EventArgs e)
		{
			if (Request.IsSecureConnection) return;
			if (Request.ServerVariables["HTTP_HOST"].StartsWith("192.168") || Request.ServerVariables["HTTP_HOST"] == "127.0.0.1"
				|| Request.ServerVariables["HTTP_HOST"] == "localhost" || Request.ServerVariables["HTTP_HOST"].StartsWith("80.179.180.")
				|| Request.ServerVariables["HTTP_HOST"] == "merchantstest.netpay-intl.com") return;
			if (Netpay.Web.WebUtils.CurrentDomain.ForceSSL) Response.Redirect("https://" + Request.Url.ToString().Substring(7));
		}
	}
}