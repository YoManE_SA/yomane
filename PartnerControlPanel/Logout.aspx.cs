﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PartnerControlPanel.Code;

namespace Netpay.PartnerControlPanel
{
	public partial class Logout : PartnerBasePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Logout();
            AffiliateStatus.Current = null;
			Response.Redirect("Default.aspx");
		}
	}
}