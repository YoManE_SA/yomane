﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Bll;
using Netpay.Web.Controls;

namespace Netpay.PartnerControlPanel
{
	public partial class ShowTotals : System.Web.UI.Page
	{
		protected void Page_Init(object sender, EventArgs e)
		{
			string mainCss = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + WebUtils.CurrentDomain.ThemeFolder + "/Styles/style.css\" />";
			string languageCss = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + WebUtils.CurrentDomain.ThemeFolder + "/Styles/style" + WebUtils.CurrentLanguageShort + ".css\" />";
			Page.Header.Controls.Add(new LiteralControl(mainCss));
			Page.Header.Controls.Add(new LiteralControl(languageCss));
			litCloseWindow.Text = "<a href=\"javascript:window.close();\">" + GetGlobalResourceObject("Multilang", "CloseWindow") + "</a>";
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!WebUtils.IsLoggedin) { Response.Write("<script>window.close();</script>"); Response.End(); }
			ShowTotalsButton.LoadTotalsParams(ref Total1);
		}
	}
}