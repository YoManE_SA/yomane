﻿function FormEnterSubmit(container, e, buttonId) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;
    if (keycode != 13) return true;
    if (buttonId != null) {
        if ($('#' + buttonId).attr('href')) document.location.href = $('#' + buttonId).attr('href');
        else $('#' + buttonId).click();
    }
    else $(container).find(':submit').click();
    return false;
}
