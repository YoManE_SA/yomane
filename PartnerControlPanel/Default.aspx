﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Netpay.PartnerControlPanel.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
    <link href="Plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TitlePage" runat="server">
    <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <asp:HiddenField ID="hdLastSerachMode" runat="server" />
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <asp:Localize ID="Localize2" Text="<%$Resources:PageTitle%>" runat="server" />
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:RadioButtonList ID="rblSearchType" runat="server" CssClass="radio-inline" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Text="<%$Resources:FilterCaptured %>" Selected="True" Value="0" />
                                <asp:ListItem Text="<%$Resources:FilterAuthorized %>" Value="1" />
                                <asp:ListItem Text="<%$Resources:FilterChargebacks %>" Value="2" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang,InsertDates  %>" />
                            <netpay:DateRangePicker ID="wcDateRangePicker" Layout="Horizontal" runat="server" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,PaymentMethod  %>" />
                            <netpay:PaymentMethodDropDown ID="ddPaymentMethod" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Literal ID="Literal3" Text="<%$Resources:MultiLang,PayoutStatus %>" runat="server" />
                            <netpay:PaymentsStatusDropDown CssClass="form-control" ID="ddPaymentsStatus" runat="server" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$Resources:MultiLang,CreditType  %>" />
                            <netpay:CreditTypeDropDown ID="ddCreditType" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:Literal ID="Literal6" runat="server" Text="<%$Resources:MultiLang,CCHolderFirstName  %>" />
                            <asp:TextBox ID="txtPayerFirstName" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:Literal ID="Literal9" runat="server" Text="<%$Resources:MultiLang,CCHolderLastName  %>" />
                            <asp:TextBox ID="txtPayerLastName" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:MultiLang,Merchants  %>" />
                            <netpay:MerchantsDropDown ID="ddMerchants" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$Resources:MultiLang,CCHolderMail  %>" />
                            <asp:TextBox ID="txtCardHolderEmail" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:Literal ID="Literal8" runat="server" Text="<%$Resources:MultiLang,Currency  %>" />
                            <netpay:CurrencyDropDown ID="ddCurrency" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="text-right">
                        <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" Text="<%$Resources:MultiLang,Search  %>" CssClass="btn btn-primary" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,QuickSearch  %>" /></div>
                <div class="panel-body">
                    <div class="form-group">
                        <asp:Literal ID="Literal10" runat="server" Text="<%$Resources:MultiLang,TransactionNumber  %>" />
                        <asp:TextBox ID="txtTransactionNumber" runat="server" CssClass="form-control" />
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <asp:LinkButton ID="btnShowTransaction" OnClick="btnShowTransaction_Click" Text="<%$Resources:MultiLang,Show  %>" CssClass="btn btn-primary" runat="server" OnClientClick="javascript:return document.getElementById('ctl00_cphBody_txtTransactionNumber').value!=''" />
                </div>
            </div>
        </div>
    </div>
    <!-- Table List -->
    <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="pull-left">
                    <asp:Literal ID="Literal16" runat="server" Text="<%$Resources:MultiLang,SearchResults  %>" />
                </div>
                <div class="pull-right">
                    <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>" Enabled="false" runat="server" OnClick="ExportExcel_Click" />&nbsp;&nbsp;|&nbsp;&nbsp;
				    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif" Text="<%$ Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:Legend ID="ttLegend" Chargeback="true" Admin="true" System="true" Test="true" Capture="true" Authorize="true" runat="server" />
                    </netpay:PopupButton>
                </div>
                <div class="clearfix">
                </div>
            </div>

            <div class="panel-body">
                <asp:Label CssClass="alert alert-danger" ID="ltError" Style="display: block;" Visible="false" runat="server" />
                <netpay:SearchFiltersView ID="wcFiltersView" runat="server" Visible="false" />
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>
                                    <asp:Literal ID="Literal11" Text="<%$Resources:MultiLang,Merchant %>" runat="server"></asp:Literal></th>
                                <th>
                                    <asp:Literal ID="Literal12" Text="<%$Resources:MultiLang,Transaction %>" runat="server"></asp:Literal></th>
                                <th>
                                    <asp:Literal ID="Literal13" Text="<%$Resources:MultiLang,Date %>" runat="server"></asp:Literal></th>
                                <th>
                                    <asp:Literal ID="Literal14" Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server"></asp:Literal></th>
                                <th>
                                    <asp:Literal ID="Literal15" Text="<%$Resources:MultiLang,Amount %>" runat="server"></asp:Literal></th>
                                <asp:Literal ID="lblTableHeadings" runat="server" />
                            </tr>
                            <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server" OnItemDataBound="SetRowView">
                                <ItemTemplate>
                                    <netpay:MerchantTransactionRowView ID="ucTransactionView" Transaction="<%# ((Netpay.Bll.Transactions.Transaction)Container.DataItem) %>" ShowMerchantName="true" ShowStatus="false" ShowExpandButton="false" runat="server" />
                                </ItemTemplate>

                            </asp:Repeater>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <netpay:LinkPager CssClass="btn btn-primary" ID="pager" runat="server" OnPageChanged="Search" />
            </div>
        </div>
    </asp:PlaceHolder>
</asp:Content>
