﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PartnerControlPanel.Code;
using Netpay.Bll;
using Netpay.Web.Controls;
using Netpay.Bll.Reports;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.PartnerControlPanel
{
	public partial class Default : PartnerBasePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request["TransID"] != null)
			{
				txtTransactionNumber.Text = Request["TransID"].ToNullableInt32().ToString();
				btnShowTransaction_Click(sender, e);
			}
            
		}

		protected bool IsCaptured { get { return rblSearchType.SelectedValue == "0"; } }
		protected new bool IsAuthorized { get { return rblSearchType.SelectedValue == "1"; } }
		protected bool IsChargeback { get { return rblSearchType.SelectedValue == "2"; } }
		protected void btnSearch_Click(Object sender, EventArgs e)
		{
			pager.CurrentPage = 0;
			Search(sender, e);
			pager.Visible = true;
		}

		protected void btnShowTransaction_Click(Object sender, EventArgs e)
		{
			hdLastSerachMode.Value = "show";
			int transactionNumber;
			if (!int.TryParse(txtTransactionNumber.Text, out transactionNumber))
			{
				ltError.Text = GetLocalResourceObject("IllegalTransNum").ToString();
				ltError.Visible = true;
				return;			
			}

			SetHeaders();
			var transStatus = (IsCaptured ? TransactionStatus.Captured : TransactionStatus.Authorized);
			var filters = new Bll.Transactions.Transaction.SearchFilters();
			filters.transactionID = txtTransactionNumber.Text.ToNullableInt32();

			if (e is ExcelButton.ExportEventArgs) 
			{
				wcFiltersView.LoadFilters(filters);
				btnExportExcel.SetExportInfo(wcFiltersView.RenderText(), ReportType.PartnerCapturedTransactions, (pi) => Bll.Transactions.Transaction.Search(transStatus, filters, pi, true, true));
			}
			else
			{
				repeaterResults.DataSource = Bll.Transactions.Transaction.Search(transStatus, filters, pager.Info, false, true);
				repeaterResults.DataBind();
			}
			pager.Visible = false;
			phResults.Visible = repeaterResults.Items.Count > 0;
		}

		protected void SetHeaders()
		{
			if (IsAuthorized)
			{
				lblTableHeadings.Text = "<th style=\"text-align:" + WebUtils.CurrentAlignment + ";\">" + GetGlobalResourceObject("MultiLang", "Confirmation") + "</th>";
				lblTableHeadings.Text = lblTableHeadings.Text + "<th style=\"text-align:" + WebUtils.CurrentAlignment + ";\">" + GetGlobalResourceObject("MultiLang", "Fee") + "</th>";
				lblTableHeadings.Text = lblTableHeadings.Text + "<th style=\"text-align:" + WebUtils.CurrentAlignment + ";\">" + GetGlobalResourceObject("MultiLang", "Capture") + "</th>";
			}
			else
			{
				lblTableHeadings.Text = "";
				lblTableHeadings.Text = "";
				lblTableHeadings.Text = "";
			}
		}

		protected void SetRowView(object sender, RepeaterItemEventArgs e)
		{
			TransactionRowView rowView = (TransactionRowView)e.Item.FindControl("ucTransactionView");
			if (IsAuthorized)
			{
				rowView.ShowApprovalNumber = true;
				rowView.ShowTransactionFee = true;
				rowView.ShowPassedTransactionID = true;			
			}
			else
			{
				rowView.ShowApprovalNumber = false;
				rowView.ShowTransactionFee = false;
				rowView.ShowPassedTransactionID = false;				
			}
		}

		protected void Search(Object sender, EventArgs e)
		{
			hdLastSerachMode.Value = "search";
			var filters = new Bll.Transactions.Transaction.SearchFilters();
			filters.paymentMethodID = ddPaymentMethod.SelectedValue.ToNullableInt32();
			filters.date.From = wcDateRangePicker.FromDate;
			filters.date.To = wcDateRangePicker.ToDate;
			if (ddCreditType.Value.ToNullableInt32() != null) filters.creditTypes = new List<int>() { ddCreditType.Value.ToNullableInt32().GetValueOrDefault() };
			filters.currencyID = ddCurrency.SelectedValue.ToNullableByte();
			filters.paymentsStatus = (Netpay.Infrastructure.PaymentsStatus?)ddPaymentsStatus.SelectedValue.NullIfEmpty().ToNullableInt();
			filters.PayerFilters = new Bll.Transactions.Payer.SearchFilters();
			filters.PayerFilters.FirstName = txtPayerFirstName.Text.NullIfEmpty();
			filters.PayerFilters.LastName = txtPayerLastName.Text.NullIfEmpty();
			filters.PayerFilters.EmailAddress = txtCardHolderEmail.Text.NullIfEmpty();
			if (ddMerchants.IsSelected)
			{
				filters.merchantIDs = new List<int>();
				filters.merchantIDs.Add(int.Parse(ddMerchants.SelectedValue));			
			}

			SetHeaders();

			TransactionStatus transStatus = IsCaptured || IsChargeback ? TransactionStatus.Captured : TransactionStatus.Authorized;
			//TransactionStatus transStatus = IsAuthorized ? TransactionStatus.Authorized : TransactionStatus.Captured;
			if (IsCaptured || IsChargeback) filters.isChargeback = IsChargeback;
			
			if (e is ExcelButton.ExportEventArgs) 
			{
				wcFiltersView.LoadFilters(filters);
				btnExportExcel.SetExportInfo(wcFiltersView.RenderText(), ReportType.PartnerCapturedTransactions, (pi) => Bll.Transactions.Transaction.Search(transStatus, filters, pi, true, true));
			} 
			else 
			{
				repeaterResults.DataSource = Bll.Transactions.Transaction.Search(transStatus, filters, pager.Info, false, true);
				repeaterResults.DataBind();
			}
			btnExportExcel.ResultCount = pager.Info.RowCount;
			phResults.Visible = repeaterResults.Items.Count > 0;
		}

		protected void ExportExcel_Click(Object sender, EventArgs e)
		{
			if (hdLastSerachMode.Value == "show") 
				btnShowTransaction_Click(sender, e);
			else 
				Search(sender, e);

			btnExportExcel.Export();
		}
	}
}