﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using System.IO;





namespace Netpay.PartnerControlPanel
{
    public partial class Customers : Code.PartnerBasePage
    {
        public MenuItemCollection ExportButtonItems
        {
            get
            {
                return new MenuItemCollection()
                    {
                        new MenuItem("CreditCards","CreditCards"),
                        new MenuItem("Balance","Balance")
                    };
            }
        }

        public UpdateProgress Loader
        {
            get
            {
                return Page.Master.FindControl("prgLoadingStatus") as UpdateProgress;
            }
        }

        public List<int> CustomersIds { get; set; }

        public Label BalanceLabel
        {
            get
            {
                return Page.Master.FindControl("lblBalance") as Label;
            }
        }

        public Web.Controls.ModalDialog ModalDialog
        {
            get
            {
                return Page.Master.FindControl("MasterModalDialog") as Web.Controls.ModalDialog;
            }
        }

        public Web.Controls.ActionNotify ActionNotify
        {
            get
            {
                return ModalDialog.FindControl("DialogActionNotify") as Web.Controls.ActionNotify;
            }
        }

        public string GetCurrentAffiliateAppIdenCurIds
        {
            get
            {
                return AffiliateStatus.GetCurrentAffiliateAppIdenCurIds;
            }
        }

        public Bll.ApplicationIdentity CurrentAppIdentity
        {
            get
            {
                return AffiliateStatus.CurrentAppIdentity;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            System.Web.UI.ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(btnCreateCustomer);
            System.Web.UI.ScriptManager.GetCurrent(Page).RegisterPostBackControl(ExportButton);

            if (ddlCustomersCur.SelectedCurrencyIso == null)
            ddlCustomersCur.IncludeCurrencyIDs = GetCurrentAffiliateAppIdenCurIds;

            if (!IsPostBack)
            {
                txtRegDateFrom.Date = txtRegDateTo.Date = null;
                if (Request["SearchText"] != null) txtSearchText.Text = Request["SearchText"];

                //Get the supported currencies of the current affiliates - app identity
                //In the future it will have to show currencies of multiple app identities that
                //Are issued with the current affiliate
                ddlCustomersCur.IncludeCurrencyIDs = GetCurrentAffiliateAppIdenCurIds;

                //Set the selected currency of the currency filter , just the first currency.
                if (CurrentAppIdentity != null)
                    ddlCustomersCur.SelectedCurrencyIso = CurrentAppIdentity.SupportedCurrencies.First();

                pager_PageChanged(sender, e);
                ctlUserDetails.DataBind();
                ctlCardDetails.DataBind();
            }

        }

        protected void pager_PageChanged(object sender, EventArgs e)
        {
            bool returnEmptyList = false;
            var filters = new Bll.Customers.Customer.SearchFilters();
            filters.RegistrationDate = new Range<DateTime?>(txtRegDateFrom.Date, txtRegDateTo.Date);
            filters.Text = txtSearchText.Text.NullIfEmpty();
            filters.RefIdentityId = Bll.Affiliates.Affiliate.Current.AppIdentities;

            if (!string.IsNullOrEmpty(txtLast4.Text))
            {
                var storedPmSf = new Bll.Accounts.StoredPaymentMethod.SearchFilters();
                storedPmSf.Value1Last4TextFilter = txtLast4.Text;
                List<int> accountidslist = Bll.Accounts.StoredPaymentMethod.Search(storedPmSf, null).Select(x => x.Account_id).ToList();
                if (accountidslist.Count > 0)
                    filters.AccountsIdsList = accountidslist;
                else
                {
                    returnEmptyList = true;
                }
            }

            //Get the customer list for the repeater
            List<Bll.Customers.Customer> customersList = new List<Bll.Customers.Customer>();
            if (!returnEmptyList)
            {
                customersList = Bll.Customers.Customer.Search(filters, pager.Info);
                rptList.DataSource = customersList;
            }
            else
            {
                rptList.DataSource = customersList;
            }

            //Get the customer list for the Total Balance
            var totalSf = new Bll.Customers.Customer.SearchFilters();
            totalSf.RefIdentityId = Bll.Affiliates.Affiliate.Current.AppIdentities;
            var totalCustomerList = Bll.Customers.Customer.Search(totalSf, null);

            //Update the ddlCustomersCur if it's null
            if (CurrentAppIdentity != null && CurrentAppIdentity.SupportedCurrencies.Count != 0 && ddlCustomersCur.SelectedCurrencyIso == null)
            {
                ddlCustomersCur.SelectedCurrencyIso = CurrentAppIdentity.SupportedCurrencies.First();
            }

            rptList.DataBind();

            //Set the balance label value - sum of all customers
            if (totalCustomerList.Count != 0)
            {
                //decimal totalBalance = Bll.Accounts.Balance.GetStatusMultipleAccountsOneCurrency(CustomersIds, ddlCustomersCur.SelectedCurrencyIso, null).Sum(x=>x.Current);
                decimal totalBalance = Bll.Accounts.Balance.GetTotalBalanceOfAffiliateCustomersByCurrency(Bll.Affiliates.Affiliate.Current.AppIdentities, ddlCustomersCur.SelectedCurrencyIso);
                BalanceLabel.Text = totalBalance.ToString("#,0.00") + " " + ddlCustomersCur.SelectedCurrencyIso;
            }
            else
            {
                BalanceLabel.Text = "0";
            }

            //If no customers , disable the currency filter
            if (CurrentAppIdentity == null || CurrentAppIdentity.SupportedCurrencies.Count == 0 || customersList.Count == 0)
            {
                ddlCustomersCur.Enabled = false;
            }
            else
            {
                ddlCustomersCur.Enabled = true;
            }

            upCustomers.Update();
        }

        protected void lnkEdit_Command(object sender, CommandEventArgs e)
        {
            //disable the creation of a customer
            //if the current affiliate is not issued
            //with any app identity , beacause there wont
            //be connection between the affiliate and the customer
            if (CurrentAppIdentity == null)
            {
                ActionNotify.SetMessage("You can't create customer because you are not issued with any app identity.", true);
                ModalDialog.Title = "Error";
                ModalDialog.BindAndShow();
                return;
            }
            ctlUserDetails.LoadCustomer(e.CommandArgument.ToNullableInt().GetValueOrDefault());
        }

        protected void lnkCards_Command(object sender, CommandEventArgs e)
        {
            ctlCardDetails.LoadCustomer(e.CommandArgument.ToNullableInt().GetValueOrDefault());
        }

        protected void rptList_PreRender(object sender, EventArgs e)
        {
            foreach (RepeaterItem item in rptList.Items)
            {
                System.Web.UI.ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(item.FindControl("lnkEdit"));
                System.Web.UI.ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(item.FindControl("lnkCards"));
            }
        }

        protected void ddlCustomersCur_SelectedIndexChanged(object sender, EventArgs e)
        {
            pager_PageChanged(sender, e);
        }

        protected void btnExport_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandArgument.ToString())
            {
                case "CreditCards":
                    GridView CreditCardGV = new GridView();
                    List<int> accountsIdsList = Bll.Customers.Customer.Search(new Bll.Customers.Customer.SearchFilters() { RefIdentityId = Bll.Affiliates.Affiliate.Current.AppIdentities }, null).Select(x => x.AccountID).Take(2000).ToList();
                    List<Bll.Accounts.StoredPaymentMethod.CreditCardDetails> detailsList = Bll.Accounts.StoredPaymentMethod.GetAccountsCreditCardDetails(accountsIdsList);
                    if (detailsList.Count() > 0)
                    {
                        CreditCardGV.DataSource = detailsList;
                        CreditCardGV.DataBind();
                    }
                    else
                    {
                        ActionNotify.SetMessage("No cards found.", true);
                        ModalDialog.Title = "Error";
                        ModalDialog.BindAndShow();
                        return;
                    }

                    foreach(GridViewRow row in CreditCardGV.Rows)
                    {
                        foreach(TableCell cell in row.Cells)
                        {
                            cell.CssClass = "text";
                        }
                    }
                                                            
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", "attachment; filename=CreditCards.xls");
                    Response.ContentType = "application/excel";

                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htmlTextWriter = new HtmlTextWriter(sw);

                    CreditCardGV.RenderControl(htmlTextWriter);
                                        
                    string style = @"<style> .text { mso-number-format:\@; } </style> ";
                    Response.Write(style);
                    Response.Write(sw.ToString());

                    upCustomers.Update();
                    upCustomers.DataBind();
                    Response.End();
                    break;


                case "Balance":
                    break;
            }

        }

        protected void ExportButton_Load(object sender, EventArgs e)
        {
            ExportButton.Items = ExportButtonItems;
        }
    }
}