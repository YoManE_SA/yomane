﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="Netpay.PartnerControlPanel.Reports" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TitlePage" runat="server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="Server">
    <script language="javascript" type="text/javascript">
        if (netpay.Blocker) netpay.Blocker.enabled = false;		
        function deleteConfirmation()
        {
            return confirm("<asp:Literal Text="<%$Resources:DeleteConfirm %>" runat="server"></asp:Literal>");
        }  
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <asp:Localize ID="Localize5" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="panel-body">
            <div class="col-md-12">
                <div class="form-group">
                    <asp:Localize ID="Localize2" Text="<%$ Resources: ReportType %>" runat="server" />:
                <asp:RadioButtonList RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radio-inline" ID="rblReportType" runat="server" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Localize ID="Localize3" Text="<%$ Resources: TimeFrame %>" runat="server" />
                    <netpay:DateRangePicker HtmlLayout="Table" Layout="Horizontal" ID="wcReportDate" OutOfRangeError="<%$ Resources: DateOffsetError %>" MaxRange="366.00:00:00.0000000" runat="server" />
                </div>
            </div>
            <asp:PlaceHolder ID="phQueuedTasks" runat="server">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning">

                            <div class="pull-left">
                                <asp:Label ID="literalDescription" Text="<%$ Resources:MultiLang, queued %>" runat="server" />! 
                    <asp:Label ID="lblMessage" Text="<%$ Resources: RequestMsg %>" runat="server" />
                            </div>
                            <div class="pull-right">
                                <asp:Button ID="btnRefreshFiles" Text="<%$ Resources:MultiLang, Refresh %>" OnClick="btnRefresh_click" CssClass="btn btn-default" runat="server" />
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </asp:PlaceHolder>
        </div>
        <div class="panel-footer text-right">
            <asp:Button ID="btnGenerateReport" Text="<%$ Resources:MultiLang, Generate %>" CssClass="btn btn-primary" OnClick="btnGenerateReport_Click" runat="server" />
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <asp:Localize ID="Localize6" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="panel-body">
            <asp:PlaceHolder ID="phGeneratedReports" runat="server">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>
                                <asp:Literal ID="Literal1" Text="<%$ Resources: FileName %>" runat="server" /></th>
                            <th>
                                <asp:Literal ID="Literal2" Text="<%$ Resources:MultiLang, Size %>" runat="server" /></th>
                            <th>
                                <asp:Literal ID="Literal3" Text="<%$ Resources:MultiLang, Created %>" runat="server" /></th>
                            <th>&nbsp;</th>
                        </tr>
                        <asp:Repeater ID="repeaterQueuedReports" runat="server">
                            <ItemTemplate>
                                <tr style="background-color: #FFFFCC;">
                                    <td><asp:Literal ID="Literal4" Text="<%$ Resources:MultiLang, Requested %>" runat="server" />: <%# ((Netpay.Infrastructure.Tasks.TaskInfo)Container.DataItem).Description %> &nbsp;
										<%# ((Netpay.Infrastructure.Tasks.TaskInfo)Container.DataItem).InsertDate.ToString("dd/MM/yy HH:MM") %>
                                    </td>
                                    <td>--</td>
                                    <td><asp:Literal ID="Literal3" Text="<%$ Resources:MultiLang, queued %>" runat="server" /> </td>
                                    <td style="text-align: center;">
                                        <asp:LinkButton ID="btnCancelReport" CommandArgument="<%# ((Netpay.Infrastructure.Tasks.TaskInfo)Container.DataItem).TaskID.ToString() %>" OnCommand="btnCancelReport_click" runat="server" AlternateText="Cancel"><i class="fa fa-trash"></i></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:Repeater ID="repeaterGeneratedReports" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#F5F5F5';" onmouseout="this.style.backgroundColor='#ffffff';">
                                    <td>
                                        <img alt="" align="absmiddle" src="/NPCommon/ImgFileExt/14X14/<%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.Extension.Remove(0,1) %>.gif" />
                                        <asp:LinkButton ID="btnDownloadFile" CommandArgument='<%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.Name + "|" + ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFileType.ToString() %>' OnCommand="btnDownloadFile_click" runat="server" ForeColor="#808080"><%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.Name %> <i class="fa fa-download" style="color: #4A885B"></i></asp:LinkButton>
                                    </td>
                                    <td><%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.Length.ToFileSize()%>
                                    </td>
                                    <td><%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.CreationTime.ToString()%>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="btnDeleteFile" CommandArgument='<%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.Name + "|" + ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFileType.ToString() %>' OnCommand="btnDeleteFile_click" OnClientClick="return deleteConfirmation();" runat="server" AlternateText="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
