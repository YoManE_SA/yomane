﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Netpay.Bll;
using Netpay.Bll.Reports;
using System.Collections.Generic;
using Netpay.PartnerControlPanel.Code;
using Netpay.Web;
using Netpay.Web.Controls;
using Netpay.Infrastructure;

namespace Netpay.PartnerControlPanel
{
	public partial class Payments : PartnerBasePage
	{
		protected void btnSearch_Click(object sender, EventArgs e)
		{
			var filters = new Bll.Settlements.PartnerPayment.SearchFilters();
			filters.Date.From = wcDateRangePicker.FromDate;
			filters.Date.To = wcDateRangePicker.ToDate;
			if (ddMerchant.IsSelected)
			{
				filters.merchantIDs = new List<int>();
				filters.merchantIDs.Add(int.Parse(ddMerchant.SelectedValue));
			} 
			if (e is ExcelButton.ExportEventArgs) 
			{
				wcFiltersView.LoadFilters(filters);
				btnExportExcel.SetExportInfo(wcFiltersView.RenderText(), ReportType.PartnerDeclinedTransactions, (pi) => Bll.Settlements.PartnerPayment.Search(filters, pi));
			} 
			else 
			{
				repeaterSettlements.DataSource = Bll.Settlements.PartnerPayment.Search(filters, pager.Info);
				repeaterSettlements.DataBind();
				btnExportExcel.ResultCount = repeaterSettlements.Items.Count;
				phResults.Visible = repeaterSettlements.Items.Count > 0;
			}
		}

		protected void ExportExcel_Click(Object sender, EventArgs e)
		{
			btnSearch_Click(this, e);
			btnExportExcel.Export();
		}
	}
}