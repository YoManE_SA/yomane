﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PartnerControlPanel.Code;
using Netpay.Bll.Reports;
using Netpay.Bll;
using Netpay.Web;
using Netpay.Web.Controls;
using Netpay.Infrastructure;

namespace Netpay.PartnerControlPanel
{
	public partial class DeclinedTransactions : PartnerBasePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request["TransID"] != null)
			{
				txtTransactionNumber.Text = Request["TransID"].ToNullableInt32().ToString();
				btnShowTransaction_Click(sender, e);
			}		
		}

		protected void btnSearch_Click(Object sender, EventArgs e)
		{
			pager.CurrentPage = 0;
			Search(sender, e);
			pager.Visible = true;
		}

		protected void btnShowTransaction_Click(Object sender, EventArgs e)
		{
			hdLastSerachMode.Value = "show";
			int transactionNumber;
			if (!int.TryParse(txtTransactionNumber.Text, out transactionNumber))
			{
				ltError.Text = GetLocalResourceObject("IllegalTransNum").ToString();
				ltError.Visible = true;
				return;			
			}

			var filters = new Bll.Transactions.Transaction.SearchFilters();
			filters.transactionID = txtTransactionNumber.Text.ToNullableInt32();

			if (e is ExcelButton.ExportEventArgs) {
				btnExportExcel.SetExportInfo(null, ReportType.PartnerDeclinedTransactions, (pi) => Bll.Transactions.Transaction.Search(TransactionStatus.Declined, filters, pi, true, true));
			}else{
				repeaterResults.DataSource = Bll.Transactions.Transaction.Search(TransactionStatus.Declined, filters, pager.Info, false, true);
				repeaterResults.DataBind();
				btnExportExcel.ResultCount = repeaterResults.Items.Count;
			}
			pager.Visible = false;
			phResults.Visible = repeaterResults.Items.Count > 0;
		}

		protected void Search(Object sender, EventArgs e)
		{
			hdLastSerachMode.Value = "search";
			var filters = new Bll.Transactions.Transaction.SearchFilters();
			filters.paymentMethodID = ddPaymentMethod.SelectedValue.ToNullableInt32();
			filters.date.From = wcDateRangePicker.FromDate;
			filters.date.To = wcDateRangePicker.ToDate;
			if (ddCreditType.Value.ToNullableInt32() != null) filters.creditTypes = new List<int>() { ddCreditType.Value.ToNullableInt32().GetValueOrDefault() };
			filters.currencyID = ddCurrency.SelectedValue.ToNullableByte();
			filters.paymentsStatus = (Netpay.Infrastructure.PaymentsStatus?) ddPaymentsStatus.SelectedValue.NullIfEmpty().ToNullableInt();
			filters.PayerFilters = new Bll.Transactions.Payer.SearchFilters();
			filters.PayerFilters.FirstName = txtPayerFirstName.Text.NullIfEmpty();
			filters.PayerFilters.LastName = txtPayerLastName.Text.NullIfEmpty();
			filters.PayerFilters.EmailAddress = txtCardHolderEmail.Text.NullIfEmpty();
			if (ddMerchants.IsSelected)
			{
				filters.merchantIDs = new List<int>();
				filters.merchantIDs.Add(int.Parse(ddMerchants.SelectedValue));			
			}

			if (e is ExcelButton.ExportEventArgs) 
			{
				wcFiltersView.LoadFilters(filters);
				btnExportExcel.SetExportInfo(wcFiltersView.RenderText(), ReportType.PartnerDeclinedTransactions, (pi) => Bll.Transactions.Transaction.Search(TransactionStatus.Declined, filters, pi, true, true));
			}
			else
			{
				repeaterResults.DataSource = Bll.Transactions.Transaction.Search(TransactionStatus.Declined, filters, pager.Info, false, true);
				repeaterResults.DataBind();
				btnExportExcel.ResultCount = pager.Info.RowCount;
			}
			phResults.Visible = repeaterResults.Items.Count > 0;
		}

		protected void ExportExcel_Click(Object sender, EventArgs e)
		{
			if (hdLastSerachMode.Value == "show") btnShowTransaction_Click(sender, e);
			else Search(sender, e);
			btnExportExcel.Export();
		}

	}
}