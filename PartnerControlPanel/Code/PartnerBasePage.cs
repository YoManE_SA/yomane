﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Web;
using Netpay.Bll;
using System.IO;
using Netpay.Infrastructure;

namespace Netpay.PartnerControlPanel.Code
{
	public class SecurityModule : Infrastructure.Module 
	{
		public override string Name { get{ return "PCP.Security"; } }
		public static SecurityModule Current { get { return Infrastructure.Module.Get("PCP.Security") as SecurityModule; } }
		public override decimal Version { get { return 1.0m; } }

		public static Infrastructure.Security.SecuredObject GetSecuredObject(string name, bool addIfNotExist = false) 
		{
			var obj = Netpay.Infrastructure.Security.SecuredObject.Get(SecurityModule.Current, name);
			if (obj == null && addIfNotExist) obj = Netpay.Infrastructure.Security.SecuredObject.Create(SecurityModule.Current, name, Infrastructure.Security.PermissionGroup.Execute);
			return obj;
		}

		public static Infrastructure.Security.PermissionValue GetPermissionValue(Dictionary<int, Infrastructure.Security.PermissionValue?> values, string name)
		{
			var sobj = GetSecuredObject(name);
			if (sobj == null) return Infrastructure.Security.PermissionValue.None;
			if (!values.ContainsKey(sobj.ID)) return Infrastructure.Security.PermissionValue.None;
			return values[sobj.ID].GetValueOrDefault(Infrastructure.Security.PermissionValue.None);
		}
	}

	public class PartnerBasePage : BasePage
	{
		private static List<string> _freeAccessPages = null;

		private static List<string> FreeAccessPages
		{
			get
			{
				if (_freeAccessPages == null)
				{
					_freeAccessPages = new List<string>();
					_freeAccessPages.Add("Login.aspx".ToLower());
					_freeAccessPages.Add("Logout.aspx".ToLower());
					_freeAccessPages.Add("Default.aspx".ToLower());
				}

				return _freeAccessPages;
			}
		}

		protected override void OnPreInit(EventArgs e)
		{
			if (!IsLoggedin && Path.GetFileName(Request.Path).ToLower() != "Login.aspx".ToLower())
				Response.Redirect("Login.aspx", true);
			if (!IsAuthorized())
				Response.Redirect("Default.aspx", true);
		}

		public bool IsAuthorized()
		{
			string pageFilePath = Path.GetFileName(Request.Path);
			return IsAuthorized(pageFilePath);
		}

		public static bool IsAuthorized(string pageFileName)
		{
			pageFileName = pageFileName.Trim().ToLower();
			if (FreeAccessPages.Contains(pageFileName)) return true;
			if (!WebUtils.IsLoggedin) return false;
			var obj = SecurityModule.GetSecuredObject(pageFileName, true);
			//var obj = Infrastructure.Security.SecuredObject.Get(SecurityModule.Current, pageFileName);
			return obj.HasPermission(Infrastructure.Security.PermissionValue.Execute);
		}

	}
}