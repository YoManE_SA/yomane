﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.PartnerControlPanel
{
    public class AffiliateStatus
    {
        public DateTime LastRead { get; private set; }
        public int? Customers { get; private set; }
        public int? ApprovedTransactions { get; private set; }
        public int? DeclinedTransactions { get; private set; }
        public int? Reports { get; private set; }

        public decimal CurrencySum { get; set; }

        public List<int> AccountIds { get; set; }

        public AffiliateStatus() { LastRead = DateTime.Now; }
        public AffiliateStatus(int affiliateId)
        {
            LastRead = DateTime.Now;
            var sortAndPage = new SortAndPage() { RowCount = int.MinValue };
            var customersList = Bll.Customers.Customer.Search(new Bll.Customers.Customer.SearchFilters() { RefIdentityId = Bll.Affiliates.Affiliate.Current.AppIdentities }, null);

            sortAndPage.RowCount = int.MinValue;
            
            Bll.Customers.Customer.Search(new Bll.Customers.Customer.SearchFilters() { RefIdentityId = Bll.Affiliates.Affiliate.Current.AppIdentities }, sortAndPage);
            Customers = sortAndPage.RowCount;
            
            if (customersList.Count != 0 && CurrentAppIdentity != null)
            {
                if (CurrentAppIdentity.SupportedCurrencies.Count == 0) throw new Exception("Current app identity has not supported currencies.");
                CurrencySum = Bll.Accounts.Balance.GetTotalBalanceOfAffiliateCustomersByCurrency(Bll.Affiliates.Affiliate.Current.AppIdentities, CurrentAppIdentity.SupportedCurrencies.First());
            }
            else
            {
                CurrencySum = 0;
            }
            
            sortAndPage.RowCount = int.MinValue;
            Bll.Transactions.Transaction.Search(TransactionStatus.Captured, new Bll.Transactions.Transaction.SearchFilters() { }, sortAndPage, false, false);
            ApprovedTransactions = sortAndPage.RowCount;

            sortAndPage.RowCount = int.MinValue;
            Bll.Transactions.Transaction.Search(TransactionStatus.Declined, new Bll.Transactions.Transaction.SearchFilters() { }, sortAndPage, false, false);
            DeclinedTransactions = sortAndPage.RowCount;

            Reports = Bll.Reports.FiledReports.GetGeneratedReports().Count;
        }

        public static AffiliateStatus Current
        {
            get
            {
                var ret = Infrastructure.Security.Login.Current.GetItem<AffiliateStatus>("AffiliateStatus");
                if (ret != null && ret.LastRead < DateTime.Now.AddMinutes(-5)) ret = null;
                if (ret != null) return ret;
                if (Bll.Affiliates.Affiliate.Current == null) ret = new AffiliateStatus();
                else ret = new AffiliateStatus(Bll.Affiliates.Affiliate.Current.ID);
                return (Current = ret);
            }
            set
            {
                if (Infrastructure.Security.Login.Current == null) return;
                Infrastructure.Security.Login.Current.SetItem("AffiliateStatus", value);
            }
        }

        public static string GetCurrentAffiliateAppIdenCurIds
        {
            get
            {
                if (CurrentAppIdentity == null || CurrentAppIdentity.SupportedCurrencies.Count == 0) return "";
                else
                {
                    var isoCodeList = CurrentAppIdentity.SupportedCurrencies;
                    var IdsList = Bll.Currency.GetCurrenciesIdsByIsoCodes(isoCodeList);
                    return string.Join(",", IdsList);
                }
            }
        }

        public static Bll.ApplicationIdentity CurrentAppIdentity
        {
            get
            {
                if (Bll.Affiliates.Affiliate.Current == null || Bll.Affiliates.Affiliate.Current.AppIdentities.Count == 0) return null;
                return Bll.ApplicationIdentity.Load(Bll.Affiliates.Affiliate.Current.AppIdentities.First());
            }
        }
    }

    public partial class Main : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            lnkIcon.Href = WebUtils.CurrentDomain.ThemeFolder + "/favicon.ico";
            btnSearch.Attributes.Add("onclick", "document.location='" + ResolveUrl("~/Customers.aspx") + "?SearchText=' + encodeURIComponent(document.getElementById('txtSearchText').value);");
            // languages
            rptLanguages.DataSource = new CultureInfo[] { new CultureInfo("en-us"), new CultureInfo("he-il") };
            rptLanguages.DataBind();
            //Page.Header.Controls.Add(new LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + WebUtils.CurrentDomain.ThemeFolder + "Plugins/bootstrap-rtl/dist/css/bootstrap-rtl.min.css\" />"));
            if (Web.WebUtils.CurrentDirection == "rtl")
                Page.Header.Controls.Add(new LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Plugins/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") + "\" />"));

            // register the blocker to block on every postback
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "blockerPostbackScript", "netpay.Blocker.show()");
            // logon state
            phStatusBox.DataBind();
            if (WebUtils.IsLoggedin)
            {
                btnApprovedTransactions.Disabled = !Code.PartnerBasePage.IsAuthorized("Default.aspx");
                btnDeclinedTransactions.Disabled = !Code.PartnerBasePage.IsAuthorized("DeclinedTransactions.aspx");
                btnPayments.Disabled = !Code.PartnerBasePage.IsAuthorized("Payments.aspx");
                btnReports.Disabled = !Code.PartnerBasePage.IsAuthorized("Reports.aspx");
                btnCustomers.Disabled = !Code.PartnerBasePage.IsAuthorized("Customers.aspx");
                literalLastLogin.Text = WebUtils.LoggedUser.LastLogin.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm");
                literalPartnerName.Text = WebUtils.LoggedUser.UserName;
                phLogout.Visible = true;
            }
            else
            {
                btnApprovedTransactions.Disabled = btnDeclinedTransactions.Disabled = btnPayments.Disabled = btnReports.Disabled = btnCustomers.Disabled = true;
                phLogout.Visible = false;
            }
            if (!Page.IsPostBack) litCopyright.Text = GetGlobalResourceObject("MultiLang", "CopyRight").ToString().Replace("%LEGAL_NAME%", WebUtils.CurrentDomain.LegalName);
        }

        protected string GetLanguageName(CultureInfo cul)
        {
            return cul.IsNeutralCulture ? cul.NativeName : cul.Parent.NativeName;
        }
        protected AffiliateStatus AffiliateStatus { get { return AffiliateStatus.Current; } }

        protected Netpay.Bll.ApplicationIdentity CurrentAppIdentity { get { return AffiliateStatus.CurrentAppIdentity; } }

        public string MapTemplateVirPath(string fileName)
        {
            return Web.WebUtils.MapTemplateVirPath(Page, fileName);
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            AffilateLogo.DataBind();
            if (Bll.Affiliates.Affiliate.Current != null && !string.IsNullOrEmpty(Bll.Affiliates.Affiliate.Current.HeaderImageFileName))
                AffilateLogo.ImageUrl = Domain.Current.MapPublicDataVirtualPath(Bll.Affiliates.Affiliate.Current.HeaderImageFileName);

        }
    }
}