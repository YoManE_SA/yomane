﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Customers.aspx.cs" Inherits="Netpay.PartnerControlPanel.Customers" %>

<%@ Register Src="~/Controls/UserDetails.ascx" TagPrefix="uc1" TagName="UserDetails" %>
<%@ Register Src="~/Controls/CardDetails.ascx" TagPrefix="uc1" TagName="CardDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header" runat="server">
        <link href="Plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TitlePage" runat="server">
    <asp:Literal ID="Literal11" runat="server" Text="<%$Resources:MultiLang,Customers  %>" />
    <span class="pull-right">
        <asp:Literal runat="server" Text="Currency: "></asp:Literal>
        <netpay:CurrencyDropDown OnSelectedIndexChanged="ddlCustomersCur_SelectedIndexChanged" AutoPostBack="true" EnableBlankSelection="false" runat="server" ID="ddlCustomersCur" CssClass="form-control fc-inline"></netpay:CurrencyDropDown>
    </span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <script>
        $(document).ready(function () {
            $("#txtRegDateFrom_datePicker").addClass("form-control");
            $("#txtRegDateTo_datePicker").addClass("form-control");
        });
    </script>
    <div class="panel panel-default">
        <div class="panel-heading">
            <asp:Literal runat="server" Text="<%$Resources:MultiLang,Customers  %>" />
        </div>
        <div class="panel-body">
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang, FromRegistrationDate %>" />
                    <netpay:DatePicker ID="txtRegDateFrom" ClientIDMode="Static" runat="server" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,  ToRegistrationDate %>" />
                    <netpay:DatePicker ID="txtRegDateTo" ClientIDMode="Static" runat="server" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Literal runat="server" Text="<%$Resources:MultiLang, SearchBy %>" />
                    <asp:TextBox ID="txtSearchText" runat="server" CssClass="form-control" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Literal runat="server" Text="<%$Resources:MultiLang, Last4CC %>" />
                    <asp:TextBox MaxLength="4" ID="txtLast4" runat="server" CssClass="form-control" />
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <asp:LinkButton runat="server" OnClick="pager_PageChanged" CssClass="btn btn-primary">
                <asp:Literal ID="Literal4" runat="server" Text="<%$Resources:MultiLang, Search %>" />
            </asp:LinkButton>
        </div>
    </div>

    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" ID="upCustomers">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left">
                        <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:MultiLang,Customers  %>" />
                    </div>
                    <div class="pull-right">
                        <JQ:MenuButton runat="server" ID="ExportButton" CssClass="btn-success" Text="Export" OnLoad="ExportButton_Load" OnCommand="btnExport_Command"></JQ:MenuButton>
                        <asp:LinkButton runat="server" ID="btnCreateCustomer" OnCommand="lnkEdit_Command" CommandArgument="0" CssClass="btn btn-primary">
                            <i class="fa fa-plus"></i>
                            <asp:Localize ID="Localize7" Text="<%$Resources:MultiLang,Create %>" runat="server" />
                        </asp:LinkButton>
                    </div>
                    <div class="clearfix"></div>
                </div>
                
                <div class="panel-body">
                    <div class="table-responsive">
                        <netpay:DynamicRepeater runat="server" ID="rptList" OnPreRender="rptList_PreRender">
                            <HeaderTemplate>
                                <table class="table table-striped">
                                    <thead>
                                        <th>
                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang, AccountNumber %>" />
                                        </th>
                                        <th>
                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang, FullName %>" />
                                        </th>
                                        <th>
                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang, PayerInfoCellular %>" />
                                        </th>
                                        <th>
                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang, Balance %>" />
                                        </th>
                                        <th>
                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang, Currency %>" />
                                        </th>
                                        <th>
                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang, EditDetalis %>" />
                                        </th>
                                        <th>
                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang, Cards %>" />
                                        </th>
                                    </thead>
                            </HeaderTemplate>

                            <ItemTemplate>
                                <tbody>
                                    <td><%# Eval("ID") %></td>
                                    <td><%# Eval("FullName") %></td>
                                    <td><%# Eval("CellNumber") %></td>
                                    <td><i style="color: #048e42;" class="fa fa-caret-up"></i><span style="color: #048e42;">&nbsp;<%# Netpay.Bll.Accounts.Balance.GetStatus((int)Eval("AccountID"), (ddlCustomersCur.SelectedCurrencyIso)).Current %></span></td>
                                    <td><%= ddlCustomersCur.SelectedCurrencyIso %></span></td>
                                    <td>
                                        <asp:LinkButton runat="server" ID="lnkEdit" OnCommand="lnkEdit_Command" CommandArgument='<%# Eval("ID") %>' CssClass="btn btn-default" Style="padding: 0 12px;">
                                            <asp:Literal ID="Literal10" runat="server" Text="<%$Resources:MultiLang, Edit %>" />
                                        </asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton runat="server" ID="lnkCards" OnCommand="lnkCards_Command" CommandArgument='<%# Eval("AccountID") %>' CssClass="btn btn-default" Style="padding: 0 12px;">
                                            <asp:Literal ID="Literal12" runat="server" Text="<%$Resources:MultiLang, Cards %>" />
                                        </asp:LinkButton>
                                    </td>
                                </tbody>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                            <EmptyContainer>
                                <div class="alert alert-info"><strong>Info!</strong> No customers found </div>
                            </EmptyContainer>
                        </netpay:DynamicRepeater>
                    </div>
                </div>
                
                <div class="panel-footer">
                    <netpay:LinkPager CssClass="pagination" ID="pager" runat="server" AutoHide="false" OnPageChanged="pager_PageChanged" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <uc1:CardDetails runat="server" ID="ctlCardDetails" />
    <uc1:UserDetails runat="server" ID="ctlUserDetails" />
</asp:Content>

