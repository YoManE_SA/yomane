﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="~/Payments.aspx.cs" Inherits="Netpay.PartnerControlPanel.Payments" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TitlePage" runat="server">
    <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">
                <asp:Localize ID="Localize2" Text="<%$Resources:PageTitle%>" runat="server" /></h1>
        </div>
        <div class="panel-body">
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang,InsertDates  %>" />
                    <netpay:DateRangePicker ID="wcDateRangePicker" Layout="Horizontal" runat="server" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,Merchant  %>" />
                    <netpay:MerchantsDropDown CssClass="form-control" ID="ddMerchant" runat="server" />
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" Text="<%$Resources:MultiLang,Search  %>" CssClass="btn btn-primary" runat="server" />
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="pull-left">
                  <h1 class="panel-title"><asp:Literal ID="Literal8" runat="server" Text="<%$Resources:MultiLang,SearchResults  %>" /></h1>
            </div>
            <div class="pull-right">
                 <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>" Enabled="false" runat="server" OnClick="ExportExcel_Click" />
            </div>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
           <asp:Label CssClass="alert alert-danger" ID="ltError" Style="display: block;" Visible="false" runat="server" />
            <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
                <netpay:SearchFiltersView ID="wcFiltersView" runat="server" Visible="false" />
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>
                                    <asp:Literal ID="Literal3" Text="<%$Resources:Number %>" runat="server" /></th>
                                <th>
                                    <asp:Literal ID="Literal4" Text="<%$Resources:SettlDateTime %>" runat="server"></asp:Literal></th>
                                <th>
                                    <asp:Literal ID="Literal5" Text="<%$Resources:PaymentToMerchant %>" runat="server"></asp:Literal></th>
                                <th>
                                    <asp:Literal ID="Literal6" Text="<%$Resources:Payment %>" runat="server"></asp:Literal></th>
                                <th>
                                    <asp:Literal ID="Literal7" Text="<%$Resources:Totals %>" runat="server"></asp:Literal></th>
                            </tr>
                        </thead>
                        <asp:Repeater ID="repeaterSettlements" EnableViewState="false" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='#FFFFFF';" id='tr_<%#Eval("ID")%>'>
                                    <td>
                                        <asp:Literal ID="ltNumber" runat="server" Text='<%#Eval("ID")%>' />
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltDate" runat="server" Text='<%#((DateTime)Eval("InsertDate")).ToString("yyyy/MM/dd")%>' />
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltMerchantPaidAmount" runat="server" Text='<%#((decimal)Eval("MerchantPaidAmount")).ToAmountFormat((int)Eval("CurrencyID"))%>' />
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltAmount" runat="server" Text='<%#((decimal)Eval("Amount")).ToAmountFormat((int)Eval("CurrencyID"))%>' />
                                    </td>
                                    <td>
                                        <netpay:ShowTotalsButton ID="ShowTotalsButton1" runat="server" CurrencyID='<%#Eval("CurrencyID")%>' PayID='<%#Eval("TransPaymentID")%>' Text="<%$Resources:ProcessTotals%>" />
                                        <netpay:ShowTotalsButton ID="ShowTotalsButton2" runat="server" CurrencyID='<%#Eval("CurrencyID")%>' PayID='<%#Eval("TransPaymentID")%>' AFPID='<%#Eval("ID")%>' Text="<%$Resources:PartnerTotal%>" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </asp:PlaceHolder>
        </div>
        <div class="panel-footer">
             <netpay:LinkPager ID="pager" runat="server" />
        </div>
    </div>
</asp:Content>
