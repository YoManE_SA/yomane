﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Xml;
using Netpay.Web;
using Netpay.Infrastructure;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure.Security;

namespace Netpay.PartnerControlPanel
{

    partial class Login : System.Web.UI.Page
    {

        public bool ShowRegister;
        protected void Page_Load(object sender, System.EventArgs e)
        {
            lnkIcon.Href = WebUtils.MapTemplateVirPath(this, "favicon.ico?1=1");

            if (!Page.IsPostBack & Request.QueryString["login"] == "outer")
            {
                string username = WebUtils.DecryptUrlBase64String(Request["username"].Trim());
                string password = WebUtils.DecryptUrlBase64String(Request["userpassword"].Trim());
                string email = WebUtils.DecryptUrlBase64String(Request["mail"].Trim());

                //Email
                loginEmail.Visible = false;
                loginEmail.Text = email;

                ////Username
                //loginUsername.Visible = false;
                //loginUsername.Text = username;

                //Pass
                loginPassword.TextMode = TextBoxMode.SingleLine;
                loginPassword.Visible = false;
                loginPassword.Text = password;
            }
            else if (!IsPostBack)
            {
                ShowRegister = Request.QueryString["ShowRegister"].ToNullableBool().GetValueOrDefault(false);
                if (Page.Request.QueryString.ToString() == "PasswordChanged")
                {
                    ClearFields();
                    SuccessAndFocus("Password changed, login with the new one.", ref loginPassword);
                }
            }
            else if (WebUtils.IsLoggedin && (!Request.Params["__EVENTTARGET"].Contains("btnSave")))
            {
                Response.Redirect("~/Default.aspx");
            }
        }

        public void ClearFields()
        {
            loginEmail.Text = "";
            //loginUsername.Text = "";
            loginPassword.Text = "";
        }

        public void AlertAndFocus(string message, ref TextBox txtField)
        {
            lblError.Text = "<div class=\"error\">" + message + "</div>";
            lblError.Visible = true;
            txtField.Focus();
        }

        public void SuccessAndFocus(string message, ref TextBox txtField)
        {
            lblError.Text = "<div class=\"success\">" + message + "</div>";
            lblError.Visible = true;
            txtField.Focus();
        }

        protected void btnLogin_Click(object o, System.EventArgs e)
        {
            ShowRegister = false;
            lblError.Text = "";
            bool isValidData = true;
            if (Validation.IsEmptyField(loginEmail, lblError, Convert.ToString(GetGlobalResourceObject("Login.aspx", "Mail"))))
                isValidData = false;
            if (Validation.IsNotMail(loginEmail, lblError, Convert.ToString(GetGlobalResourceObject("Login.aspx", "Mail"))))
                isValidData = false;
            //if (Validation.IsEmptyField(loginUsername, lblError, Convert.ToString(GetGlobalResourceObject("Login.aspx", "Username"))))
            //    isValidData = false;
            if (Validation.IsEmptyField(loginPassword, lblError, Convert.ToString(GetGlobalResourceObject("Login.aspx", "Password"))))
                isValidData = false;

            if (!isValidData)
            {
                //dvAlert.Visible = True
                return;
            }

            string email = loginEmail.Text.ToSql(true).Trim();
            //string username = loginUsername.Text.ToSql(true).Trim();
            string password = loginPassword.Text.ToSql(true).Trim();

            var result = WebUtils.Login(new Netpay.Infrastructure.Security.UserRole[] {
            Netpay.Infrastructure.Security.UserRole.Partner
        }, null, email, password);

            if (result == Netpay.Infrastructure.Security.LoginResult.Success)
            {
                if (WebUtils.LoggedUser.IsFirstLogin)
                {
                    dlgChangePassword.BindAndShow();
                }
                else
                {
                    string url = Request.RawUrl;

                    if (url.ToLower().Contains("signup.aspx"))
                        url = ".";
                    if (url.ToLower().Contains("login.aspx"))
                        url = ".";
                    if (url.ToLower().Contains("logout.aspx"))
                        url = ".";
                    if (url.ToLower().Contains("userlockedout"))
                        url = ".";
                    if (url.ToLower().Contains("userblocked"))
                        url = ".";
                    if (url.ToLower().Contains("password"))
                        url = ".";

                    //Server.Transfer(url, False)
                    //Response.End()
                    Response.Redirect(url, true);
                }
            }
            else if (result == Infrastructure.Security.LoginResult.ForcePasswordChange)
            {
                dlgChangePassword.BindAndShow();
            }
            else
            {
                AlertAndFocus(Web.WebUtils.GetResource("Login").GetString(Infrastructure.Security.LoginResult.WrongPassword.ToString()), ref loginPassword);
            }
        }

        protected override void OnPreRender(System.EventArgs e)
        {
            ltCopyright.Text = Convert.ToString(GetGlobalResourceObject("Login.aspx", "copyright")).Replace("%BRAND%", WebUtils.CurrentDomain.BrandName);
            System.Web.UI.HtmlControls.HtmlLink link = new System.Web.UI.HtmlControls.HtmlLink();
            link.Href = WebUtils.MapTemplateVirPath(this, "style/login.css");
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            link.Attributes.Add("media", "screen");
            Page.Header.Controls.Add(link);
            if (System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft)
            {
                link = new System.Web.UI.HtmlControls.HtmlLink();
                link.Href = WebUtils.MapTemplateVirPath(this, "Style/loginRTL.css");
                link.Attributes.Add("rel", "stylesheet");
                link.Attributes.Add("type", "text/css");
                link.Attributes.Add("media", "screen");
                Page.Header.Controls.Add(link);
            }

            //Set the close js function for the close button
            btnClose.OnClientClick = dlgChangePassword.HideJSCommand;
            btnForgotPassword.OnClientClick = "event.preventDefault();" + ConfirmationModalDialog.ShowJSCommand;
            btnCancelReset.OnClientClick = ConfirmationModalDialog.HideJSCommand;

            base.OnPreRender(e);
        }

        protected void UpdatePassword_Click(object sender, EventArgs e)
        {
            string email = loginEmail.Text.ToSql(true).Trim();
            //string username = loginUsername.Text.ToSql(true).Trim();
            string oldPassword = loginPassword.Text.ToSql(true).Trim();
            string newPassword = txtNewPassword.Text.ToSql(true).Trim();
            string confirmPassword = txtConfirmPassword.Text.ToSql(true).Trim();

            //Handle the case that the user inserted the old password again.
            if (oldPassword == newPassword)
            {
                acnPasswordMessage.SetMessage("Please insert a new password.", true);
                dlgChangePassword.BindAndUpdate();
            }

            //Handle case when both passwords not equale
            if (newPassword != confirmPassword)
            {
                acnPasswordMessage.SetMessage("Passwords must be the same.", true);
                dlgChangePassword.BindAndUpdate();
            }
            else
            {
                //Set the new password
                var ret = Infrastructure.Security.Login.SetNewPasswordAfterExpiredOrReset(new UserRole[] { UserRole.Partner }, null, email, oldPassword, newPassword, Request.UserHostAddress);
                if (ret == Infrastructure.Security.Login.ChangePasswordResult.Success)
                {
                    if (WebUtils.IsLoggedin) WebUtils.Logout(false);
                    Response.Redirect("~/Login.aspx?PasswordChanged");
                }
                else
                {
                    acnPasswordMessage.SetMessage("Unable to change password: " + ret.ToString(), true);
                }
            }
        }

        protected void btnForgotPassword_Click(object sender, EventArgs e)
        {
            //Close the confirmation modal.
            ConfirmationModalDialog.RegisterHide();

            //Validations
            if (Validation.IsNotMail(loginEmail))
            {
                AlertAndFocus("Email is not valid.", ref loginEmail);
                return;
            }

            //if (string.IsNullOrEmpty(loginUsername.Text))
            //{
            //    AlertAndFocus("User name is not valid.", ref loginEmail);
            //    return;
            //}

            //Try to reset password.
            bool isReset = Infrastructure.Security.Login.ResetPassword(new UserRole[] { UserRole.Partner }, 
                null//loginUsername.Text.Trim()
                , loginEmail.Text.Trim(), Request.UserHostAddress);

            //Show the relevant message.
            if (isReset)
            {
                SuccessAndFocus("New password generated and sent via email.", ref loginEmail);
            }
            else
            {
                AlertAndFocus("User wasn't found.", ref loginEmail);
            }
        }

        protected void dlgChangePassword_DialogClose(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/", false);
        }

        protected void dlgChangePassword_DataBinding(object sender, EventArgs e)
        {
            mvChangePass.ActiveViewIndex = WebUtils.IsLoggedin ? 0 : 1;
        }
    }
}