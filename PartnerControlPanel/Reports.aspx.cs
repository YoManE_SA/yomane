﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll.Reports;
using Netpay.Bll;
using Netpay.PartnerControlPanel.Code;
using System.IO;
using Netpay.Infrastructure.Tasks;
using Netpay.Infrastructure;

namespace Netpay.PartnerControlPanel
{
	public partial class Reports : PartnerBasePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				GetGeneratedReportList();
				GetQueuedReportList();
                rblReportType.Items.Add(new ListItem(GetLocalResourceObject("ApprovedTransactions").ToString(), ReportType.PartnerCapturedTransactions.ToString()));
                rblReportType.Items.Add(new ListItem(GetLocalResourceObject("DeclinedTransactions").ToString(), ReportType.PartnerDeclinedTransactions.ToString()));
				rblReportType.SelectedIndex = 0;
			}
		}

		protected void GetGeneratedReportList()
		{
			List<GeneratedReportInfo> generatedReports = FiledReports.GetGeneratedReports();
			repeaterGeneratedReports.DataSource = generatedReports;
			repeaterGeneratedReports.DataBind();

			phGeneratedReports.Visible = generatedReports.Count > 0;
		}

		protected void GetQueuedReportList()
		{
			List<TaskInfo> queuedReports = FiledReports.GetQueuedReports();
			repeaterQueuedReports.DataSource = queuedReports;
			repeaterQueuedReports.DataBind();

			phQueuedTasks.Visible = queuedReports.Count > 0;
		}

		protected void btnGenerateReport_Click(object sender, EventArgs e)
		{
            var filters = new Bll.Transactions.Transaction.SearchFilters();
			if (wcReportDate.IsFromDateSelected)
				filters.date.From = wcReportDate.FromDate;
			if (wcReportDate.IsToDateSelected)
                filters.date.To = wcReportDate.ToDate;
			filters.isChargeback = false;

			List<object> args = new List<object>();
			args.Add(filters);

			string methodName = "Create" + rblReportType.SelectedValue + "Report";
			Task task = new Task(Web.WebUtils.LoggedUser, Task.GetUserGroupID(LoggedUser), rblReportType.SelectedValue, LogTag.Reports, "Netpay.Bll", typeof(FiledReports).ToString(), methodName, args);
			if (!Netpay.Infrastructure.Application.Queue.IsTaskQueued(task))
				Netpay.Infrastructure.Application.Queue.Enqueue(task);

			GetQueuedReportList();
		}

		protected void btnDownloadFile_click(object source, CommandEventArgs e)
		{
			string[] parameters = e.CommandArgument.ToString().Split('|');
			ReportType fileType = (ReportType)Enum.Parse(typeof(ReportType), parameters[1]);
			string fileName = parameters[0];
			
			Response.Clear();
			Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
			Response.TransmitFile(FiledReports.GetReportFullName(fileType, fileName));
			Response.End();
		}

		protected void btnDeleteFile_click(object source, CommandEventArgs e)
		{
			string[] parameters = e.CommandArgument.ToString().Split('|');
			ReportType fileType = (ReportType)Enum.Parse(typeof(ReportType), parameters[1]);
			string fileName = parameters[0];
			
			FiledReports.DeleteReport(fileType, fileName);
			GetGeneratedReportList();
		}

		protected void btnCancelReport_click(object source, CommandEventArgs e)
		{
			Guid taskID = new Guid(e.CommandArgument.ToString());
			FiledReports.CancelReport(taskID);
			GetQueuedReportList();
		}

		protected void btnRefresh_click(object source, EventArgs e)
		{
			GetGeneratedReportList();
			GetQueuedReportList();
		}
	}
}