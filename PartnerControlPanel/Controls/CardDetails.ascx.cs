﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.PartnerControlPanel.Controls
{
    public partial class CCdetalis : System.Web.UI.UserControl
    {
        public int? AccountID { get { return hfID.Value.ToNullableInt(); } set { hfID.Value = value != null ? value.ToString() : ""; } }

        private void ModalCommand(string command)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(upDetails, upDetails.GetType(), "Dialog", "$('#" + ClientID + "').modal('" + command + "');", true);
        }

        public void LoadCustomer(int accountId)
        {
            AccountID = accountId;
            DataBind();
            upDetails.Update();
            ModalCommand("show");
        }

        public override void DataBind()
        {
            //Addable pm
            var addableMethods = Bll.ApplicationIdentity.LoadForAffiliate(Bll.Affiliates.Affiliate.Current.ID).SelectMany(v => v.SupportedPaymentMethods).ToList();
            ddlBrandAdd.DataSource = Bll.PaymentMethods.PaymentMethod.Cache.Where(v => addableMethods.Contains(v.ID));

            //Creatabe pm
            var creatableMethods = Bll.ApplicationIdentity.LoadForAffiliate(Bll.Affiliates.Affiliate.Current.ID).SelectMany(v => v.OwnedPaymentMethods).ToList();
            ddlBrand.DataSource = Bll.PaymentMethods.PaymentMethod.Cache.Where(v => creatableMethods.Contains(v.ID));

            rptCards.DataSource = Bll.Accounts.StoredPaymentMethod.LoadForAccount(AccountID.GetValueOrDefault());

            base.DataBind();
        }

        protected void CreateCard_Click(object sender, EventArgs e)
        {
            string command = (e as CommandEventArgs).CommandArgument.ToString();

            try
            {
                DateTime dtExpireDate = DateTime.Now.AddYears(-1);
                if (command == "Create")
                {
                    var newCreatedCard = new Bll.Accounts.StoredPaymentMethod();
                    newCreatedCard.OwnerName = txtOwner.Text;
                    newCreatedCard.Account_id = AccountID.GetValueOrDefault();

                    if (ddlBrand.SelectedValue.ToNullableInt().GetValueOrDefault() > (int)CommonTypes.PaymentMethodEnum.PP_MIN && ddlBrand.SelectedValue.ToNullableInt().GetValueOrDefault() < (int)CommonTypes.PaymentMethodEnum.PP_MAX)
                    {
                        // only creation of prepaid is allowed if it's not oneCard
                        newCreatedCard.MethodInstance.PaymentMethodId = (CommonTypes.PaymentMethodEnum)ddlBrand.SelectedValue.ToNullableInt().GetValueOrDefault();
                    }

                    else
                    {
                        // if it's not a prepaid, only creation of CCOneCard is allowed
                        newCreatedCard.MethodInstance.PaymentMethodId = CommonTypes.PaymentMethodEnum.CCOneCard;
                    }

                    var newCreatedCc = (newCreatedCard.MethodInstance as Netpay.Bll.PaymentMethods.CreditCard);
                    Bll.PaymentMethods.PaymentMethod pm = null;

                    pm = Bll.PaymentMethods.PaymentMethod.Get(ddlBrand.SelectedValue.ToNullableInt().GetValueOrDefault());
                    if (pm == null || string.IsNullOrEmpty(pm.BaseBin)) throw new Exception("Unable to create this kind of payment method, please senet another");
                    newCreatedCc.CardNumber = Netpay.Bll.PaymentMethods.CreditCard.Generate(pm.BaseBin);

                    newCreatedCc.SetExpirationMonth(dtExpireDate.Year, dtExpireDate.Month);
                    lblCreate.Visible = true;
                    newCreatedCard.Save();

                    // Clear Textboxes
                    txtOwner.Text = "";
                }
                else if (command == "Add")
                {
                    var newAddedCard = new Bll.Accounts.StoredPaymentMethod();
                    newAddedCard.OwnerName = txtOwnerAdd.Text;
                    newAddedCard.Account_id = AccountID.GetValueOrDefault();

                    //load the selected payment method
                    newAddedCard.MethodInstance.PaymentMethodId = (CommonTypes.PaymentMethodEnum)ddlBrandAdd.SelectedValue.ToNullableInt().GetValueOrDefault();

                    var newAddedCc = (newAddedCard.MethodInstance as Netpay.Bll.PaymentMethods.CreditCard);
                    Bll.PaymentMethods.PaymentMethod pm = null;
                    pm = Bll.PaymentMethods.PaymentMethod.Get(ddlBrandAdd.SelectedValue.ToNullableInt().GetValueOrDefault());
                    if (pm == null) throw new Exception("Payment method not found");
                    var ccNumberInsertedByUser = string.Empty;
                    if (!addCreditCardNumber.Text.Trim().CardNumberCheck())
                    {
                        throw new Exception("Please enter a valid credit card number");

                    }
                    ccNumberInsertedByUser = addCreditCardNumber.Text.Trim();

                    if (string.IsNullOrEmpty(ccNumberInsertedByUser)) throw new Exception("You must insert a valid cc number");

                    // check if the instered card bin number is one of the selected payment method bin numbers
                    string bin = ccNumberInsertedByUser.TakeLeftChars(6);

                    // if the selected payment method is prepaid, search for the bin number in the List.PaymentMethod
                    // table, where each prepaid has it's bin number.
                    //if (pm.IsPrepaidMethod)
                    //{
                    //    var allowedBins = Bll.PaymentMethods.PaymentMethod.CachePrepaid
                    //                        .Where(x => x.ID == pm.ID)
                    //                        .Select(x => x.BaseBin)
                    //                        .ToList();

                    //    if (!(allowedBins.Contains(bin))) throw new Exception(string.Format("Bin number {0} doesn't exist in payment method {1}", bin, pm.Name));
                    //}
                    //else
                    //{
                    //    var allowedBins = Bll.PaymentMethods.CreditCardBin
                    //        .Search(new Bll.PaymentMethods.CreditCardBin.SearchFilters()
                    //        {
                    //            PaymentMethods = new List<CommonTypes.PaymentMethodEnum>() { (CommonTypes.PaymentMethodEnum)pm.ID }
                    //        }, null)
                    //    .Select(x => x.BIN)
                    //    .Distinct()
                    //    .ToList();
                    //    if (!(allowedBins.Contains(bin))) throw new Exception(string.Format("Bin number {0} doesn't exist in payment method {1}", bin, pm.Name));
                    //}

                    //Check if the inseted credit card already exist
                    if (Bll.Accounts.StoredPaymentMethod.IsCardNumberExist(ccNumberInsertedByUser)) throw new Exception("Card already exist.");

                    newAddedCc.CardNumber = ccNumberInsertedByUser;
                    newAddedCc.SetExpirationMonth(dtExpireDate.Year, dtExpireDate.Month);

                    //clear credit card field
                    addCreditCardNumber.Text = null;
                    txtOwnerAdd.Text = "";

                    newAddedCard.Save();
                }


                lblCreate.Visible = true;
                ltCreateError.Text = "Success";
            }
            catch (Exception ex)
            {
                lblCreate.Visible = true;
                ltCreateError.Text = ex.Message;
            }
            DataBind();
            upDetails.Update();
        }

        protected void btnDeactivate_Click(object sender, EventArgs e)
        {
            CommandEventArgs command = (e as CommandEventArgs);

            if (command.CommandName == "Deactivate")
            {
                lblCreate.Visible = true;
                ltCreateError.Text = "Success";

                try
                {
                    Netpay.Bll.Accounts.StoredPaymentMethod.ChangeCardStatus(false, Convert.ToInt32(command.CommandArgument));
                }
                catch (Exception ex)
                {
                    lblCreate.Visible = true;
                    ltCreateError.Text = ex.Message;
                }
            }
            else if (command.CommandName == "Activate")
            {
                lblCreate.Visible = true;
                ltCreateError.Text = "Success";

                try
                {
                    Netpay.Bll.Accounts.StoredPaymentMethod.ChangeCardStatus(true, Convert.ToInt32(command.CommandArgument));
                }
                catch (Exception ex)
                {
                    lblCreate.Visible = true;
                    ltCreateError.Text = ex.Message;
                }
            }
            DataBind();
            upDetails.Update();

        }


    }
}