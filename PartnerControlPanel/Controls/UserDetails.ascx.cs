﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.PartnerControlPanel.Controls
{
    public partial class UserDetails : System.Web.UI.UserControl
    {
        public Bll.Customers.Customer Customer { get; set; }

        private void ModalCommand(string command)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(upDetails, upDetails.GetType(), "Dialog", "$('#" + ClientID + "').modal('" + command + "');", true);
        }

        public void LoadCustomer(int customerId)
        {
            Customer = Bll.Customers.Customer.Load(customerId);
            DataBind();
            upDetails.Update();
            if (Customer.PinNotificationTypeId.HasValue)
                ddlPinNotificationType.SelectedValue = Convert.ToString(Customer.PinNotificationTypeId);
            ModalCommand("show");
        }

        public override void DataBind()
        {
            if (Customer == null) Customer = new Bll.Customers.Customer();
            if (Customer.PersonalAddress == null) Customer.PersonalAddress = new Bll.Accounts.AccountAddress();
            base.DataBind();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            bool isRegister = false;
            Customer = Bll.Customers.Customer.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
            if (Customer == null)
            {
                Customer = new Bll.Customers.Customer();
                Customer.ActiveStatus = Bll.Customers.ActiveStatus.FullyActive;
                if (Bll.Affiliates.Affiliate.Current.AppIdentities != null && Bll.Affiliates.Affiliate.Current.AppIdentities.Count > 0)
                    Customer.RefIdentityID = Bll.Affiliates.Affiliate.Current.AppIdentities.First();
                isRegister = true;
            }
           // Customer.Login.UserName = txtUserName.Text;
            Customer.Login.EmailAddress = txtEmailAddress.Text;
            Customer.Login.IsActive = chkIsActive.Checked;
            Customer.EmailAddress = txtEmailAddress.Text;
            Customer.FirstName = txtFirstName.Text;
            Customer.LastName = txtLastName.Text;
            Customer.PersonalNumber = txtPersonalNumber.Text;
            Customer.PhoneNumber = txtPhoneNumber.Text;
            Customer.CellNumber = txtCellNumber.Text;

            if (Customer.PersonalAddress == null) Customer.PersonalAddress = new Bll.Accounts.AccountAddress();
            Customer.PersonalAddress.AddressLine1 = txtAddressLine1.Text;
            Customer.PersonalAddress.AddressLine2 = txtAddressLine2.Text;
            Customer.PersonalAddress.City = txtCity.Text;
            Customer.PersonalAddress.PostalCode = txtPostalCode.Text;
            Customer.PersonalAddress.StateISOCode = ddlStateISOCode.ISOCode;
            Customer.PersonalAddress.CountryISOCode = ddlCountryISOCode.ISOCode;

            if (ddlPinNotificationType.SelectedItem.Value == "-1")
            {
                Customer.PinNotificationTypeId = null;
            }
            else
            {
                Customer.PinNotificationTypeId = Convert.ToInt16(ddlPinNotificationType.SelectedItem.Value);
            }
            try
            {
                if (isRegister)
                {
                    var ret = Customer.Register(System.Net.IPAddress.Parse(Request.UserHostAddress), null, null);
                    if (ret != ValidationResult.Success) throw new ValidationException(ret);
                }
                else Customer.Save();
                ModalCommand("hide");
            }

            catch (Exception ex)
            {
                lblGenerate.Visible = true;
                ltGenerateError.Text = "Error: " + ex.Message;
            }

            finally
            {
                //Clear the currenct accountFilter
                //In order to refresh the AccountIds 
                //And get the new customer.
                Bll.Accounts.AccountFilter.ClearCurrentAccountFilter();
                var pager = (this.Parent.FindControl("pager") as Netpay.Web.Controls.LinkPager);
                pager.RaisePostBackEvent(pager.Info.PageCurrent.ToString());
                
                (this.Parent.FindControl("upCustomers") as UpdatePanel).Update();
            }
        }

        protected void GeneratePassword_Click(object sender, EventArgs e)
        {
            try
            {
                if (Bll.Affiliates.Affiliate.Current.AppIdentities != null && Bll.Affiliates.Affiliate.Current.AppIdentities.Count > 0)
                {
                    if (!Bll.Customers.Customer.ResetPassword(txtEmailAddress.Text, Bll.Affiliates.Affiliate.Current.AppIdentities.First() ,Request.UserHostAddress))
                        throw new Exception("Unable to reset password");
                    ltGenerateError.Text = "Success";
                }
                else
                {
                    throw new Exception("Current affiliate is not issued with any app identity.");
                }
            }
            catch (Exception ex)
            {
                ltGenerateError.Text = "Error: " + ex.Message;
            }
            lblGenerate.Visible = true;
        }

        protected void GeneratePincode_Click(object sender, EventArgs e)
        {
            try
            {
                Customer = Bll.Customers.Customer.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
                if (Customer == null) throw new Exception("Unable to reset pincode, account not exist");
                Customer.ResetPinCode();
                ltGenerateError.Text = "Pin generated and sent successfully";
            }
            catch (Exception ex)
            {
                ltGenerateError.Text = "Error: " + ex.Message;
            }
            lblGenerate.Visible = true;
        }

    }
}