﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.ascx.cs" Inherits="Netpay.PartnerControlPanel.Controls.UserDetails" %>
<div id="<%# ClientID %>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,CustomerNumber  %>" /><asp:Literal runat="server" Text='<%# Customer.AccountNumber %>' /></h4>
            </div>
            <asp:UpdatePanel ID="upDetails" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" RenderMode="Block">
                <ContentTemplate>
                    <div class="modal-body">
                        <h4>
                            <asp:Literal ID="Literal6" runat="server" Text="<%$Resources:MultiLang,PersonalDetalis  %>" /></h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal7" runat="server" Text="<%$Resources:MultiLang,FirstName  %>" />
                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" Text='<%# Customer.FirstName %>' />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal8" runat="server" Text="<%$Resources:MultiLang,LastName  %>" />
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" Text='<%# Customer.LastName %>' />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal9" runat="server" Text="<%$Resources:MultiLang,PersonalID  %>" />
                                    <asp:TextBox ID="txtPersonalNumber" runat="server" CssClass="form-control" Text='<%# Customer.PersonalNumber %>' />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal10" runat="server" Text="<%$Resources:MultiLang,Phone  %>" />
                                    <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="form-control" Text='<%# Customer.PhoneNumber %>' />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal11" runat="server" Text="<%$Resources:MultiLang,Cellular  %>" />
                                    <asp:TextBox ID="txtCellNumber" runat="server" CssClass="form-control" Text='<%# Customer.CellNumber %>' />
                                </div>
                            </div>
                        </div>
                        <h4>
                            <asp:Literal ID="Literal12" runat="server" Text="<%$Resources:MultiLang,AddressDetalis  %>" /></h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal13" runat="server" Text="<%$Resources:MultiLang,AddressLine1  %>" />
                                    <asp:TextBox ID="txtAddressLine1" runat="server" CssClass="form-control" Text='<%# Customer.PersonalAddress.AddressLine1 %>' />
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal14" runat="server" Text="<%$Resources:MultiLang,AddressLine2  %>" />
                                    <asp:TextBox ID="txtAddressLine2" runat="server" CssClass="form-control" Text='<%# Customer.PersonalAddress.AddressLine2 %>' />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal15" runat="server" Text="<%$Resources:MultiLang,City  %>" />
                                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" Text='<%# Customer.PersonalAddress.City %>' />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal16" runat="server" Text="<%$Resources:MultiLang,State  %>" />
                                    <netpay:StateDropDown ID="ddlStateISOCode" runat="server" CssClass="form-control" ISOCode='<%# Customer.PersonalAddress.StateISOCode %>' />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal17" runat="server" Text="<%$Resources:MultiLang,PostalCode  %>" />
                                    <asp:TextBox ID="txtPostalCode" runat="server" CssClass="form-control" Text='<%# Customer.PersonalAddress.PostalCode %>' />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal18" runat="server" Text="<%$Resources:MultiLang,Country  %>" />
                                    <netpay:CountryDropDown ID="ddlCountryISOCode" runat="server" CssClass="form-control" ISOCode='<%# Customer.PersonalAddress.CountryISOCode %>' />
                                </div>
                            </div>
                            <hr width="95%" size="1" />
                        </div>
                        
                        <asp:HiddenField runat="server" ID="hfID" Value='<%# Customer.ID %>' />
                        <h4>
                            <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang,LoginDetalis  %>" /></h4>
                        <div class="row">
                            <%--<div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,Username  %>" />
                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" Text='<%# Customer.Login.UserName %>' />
                                </div>
                            </div>--%>
                          
                          
                            <div class="col-md-4">
                                <div class="form-group">
                                  Customer Status  <br />
                                    <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Customer.Login.IsActive %>' />
                                    <asp:Literal ID="Literal4" runat="server" Text="<%$Resources:MultiLang,Enabled  %>" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:MultiLang,Email  %>" />
                                    <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="form-control" Text='<%# Customer.Login.EmailAddress %>' />
                                </div>
                            </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Literal ID="Literal20" runat="server" Text="Send Pin By" />
                                    <asp:DropDownList ID="ddlPinNotificationType" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="-1">Select ...</asp:ListItem>
                                        <asp:ListItem Value="1">Email</asp:ListItem>
                                        <asp:ListItem Value="2">SMS</asp:ListItem>
                                        <asp:ListItem Value="3">Email and SMS</asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>
                            <asp:PlaceHolder runat="server" Visible='<%# Customer.ID != 0 %>'>
                                <div class="col-md-12">
                                    <div class="row">  <div class="col-md-4">
                                <div class="form-group">
                                 
                                </div>
                            </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <asp:LinkButton runat="server" ID="btnGeneratePassword" OnClick="GeneratePassword_Click" CssClass="btn btn-info btn-large">
                                                    <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:MultiLang,Genratepassword  %>" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:LinkButton runat="server" ID="btnGeneratePinCode" CssClass="btn btn-info btn-large" OnClick="GeneratePincode_Click">
                                                <asp:Literal ID="Literal19" runat="server" Text="<%$Resources:MultiLang,GeneratePincode  %>" /></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <div class="col-md-12">
                                <asp:Label runat="server" ID="lblGenerate" CssClass="alert alert-success alert-dismissable" Style="display: block;" Visible="false" EnableViewState="false">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <asp:Literal runat="server" ID="ltGenerateError"></asp:Literal>
                                </asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button runat="server" CssClass="btn btn-default" data-dismiss="modal" Text="<%$Resources:MultiLang,Close  %>" />
                        <asp:Button runat="server" CssClass="btn btn-primary" OnClick="Save_Click" Text="<%$Resources:MultiLang,Savechanges  %>" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
