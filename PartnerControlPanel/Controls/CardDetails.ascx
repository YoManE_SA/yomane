﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CardDetails.ascx.cs" Inherits="Netpay.PartnerControlPanel.Controls.CCdetalis" %>
<div id="<%# ClientID %>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <asp:Literal ID="Literal12" runat="server" Text="<%$Resources:MultiLang, Cards %>" /></h4>
            </div>
            <asp:UpdatePanel ID="upDetails" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" RenderMode="Block">
                <ContentTemplate>
                    <asp:HiddenField runat="server" ID="hfID" />
                    <div class="modal-body">
                        <h4>
                            <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang, CardList %>" /></h4>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang, OwnerName %>" /></th>
                                        <th>
                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang, CreditNumber %>" /></th>
                                        <th>Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater runat="server" ID="rptCards">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("OwnerName") %></td>
                                                <td><%# Eval("MethodInstance.Display") %></td>
                                                <td><%# Netpay.Bll.Accounts.StoredPaymentMethod.GetStatus((int)Eval("ID")) %></td>
                                                <td>
                                                    <asp:Button runat="server" ClientIDMode="Static" ID="btnDeactivate" CssClass="btn btn-success"
                                                        Text='<%# Netpay.Bll.Accounts.StoredPaymentMethod.GetStatus((int)Eval("ID")).ToUpper().Contains("Deactivated".ToUpper()) ? "Activate" : "Deactivate" %>'
                                                        CommandName='<%# Netpay.Bll.Accounts.StoredPaymentMethod.GetStatus((int)Eval("ID")).ToUpper().Contains("Deactivated".ToUpper()) ? "Activate" : "Deactivate" %>'
                                                        CommandArgument='<%# Eval("ID") %>'
                                                        OnCommand="btnDeactivate_Click" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Label runat="server" ID="lblCreate" CssClass="alert alert-success alert-dismissable" Style="display: block;" Visible="false" EnableViewState="false">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <asp:Literal runat="server" ID="ltCreateError"></asp:Literal>
                                </asp:Label>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <%--create--%>
                                    <a href="#" class="btn btn-primary" onclick="$('#dvAddCard').slideUp(); $('#dvNewCard').slideDown();">
                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang, CreateNewCard %>" />
                                    </a>

                                    <%--add--%>
                                    <a href="#" class="btn btn-primary" onclick="$('#dvNewCard').slideUp(); $('#dvAddCard').slideDown();">
                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang, AddExistingCard %>" />
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <%-- new card div --%>
                                    <div class="panel-default" style="display: none; border-top: 1px solid #eee;" id="dvNewCard">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang, Owner %>" />
                                                        <asp:TextBox runat="server" ID="txtOwner" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang, PaymentMethod %>" />
                                                        <asp:DropDownList runat="server" ID="ddlBrand" CssClass="form-control" DataTextField="Name" DataValueField="ID" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="display:none;">
                                                    <div class="form-group">
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang, ExpirationDate %>" />
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <netpay:MonthDropDown runat="server" ID="ddlExpMonth" CssClass="form-control" />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <netpay:YearDropDown runat="server" ID="ddlExpYear" YearFrom='<%# DateTime.Now.Year %>' YearTo='<%# DateTime.Now.Year + 10 %>' CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <asp:Button runat="server" ClientIDMode="Static" ID="btnCreate" CssClass="btn btn-success" Text="<%$Resources:MultiLang, Create %>" CommandArgument="Create" OnCommand="CreateCard_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Add card div --%>
                                    <div class="panel-default" style="display: none; border-top: 1px solid #eee;" id="dvAddCard">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <asp:Literal runat="server" Text="<% $Resources:MultiLang, CreditCardNumber %>"></asp:Literal>
                                                        <asp:TextBox runat="server" CssClass="form-control" ID="addCreditCardNumber"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang, Owner %>" />
                                                        <asp:TextBox runat="server" ID="txtOwnerAdd" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang, PaymentMethod %>" />
                                                        <asp:DropDownList runat="server" ID="ddlBrandAdd" CssClass="form-control" DataTextField="Name" DataValueField="ID" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="display:none;">
                                                    <div class="form-group">
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang, ExpirationDate %>" />
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <netpay:MonthDropDown runat="server" ID="ddlExpMonthAdd" CssClass="form-control" />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <netpay:YearDropDown runat="server" ID="ddlExpYearAdd" YearFrom='<%# DateTime.Now.Year %>' YearTo='<%# DateTime.Now.Year + 10 %>' CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <asp:Button runat="server" ClientIDMode="Static" ID="btnAdd" CssClass="btn btn-success" Text="<%$Resources:MultiLang, Add %>" CommandArgument="Add" OnCommand="CreateCard_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button runat="server" CssClass="btn btn-default" data-dismiss="modal" Text="<%$Resources:MultiLang, Close %>" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
