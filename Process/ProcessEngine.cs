﻿using System;
using System.Collections.Generic;
using Netpay.Process.Modules;
using Netpay.CommonTypes;

namespace Netpay.Process
{
    public class ProcessEngine : IValidator, IProcessor, IRiskDetector, IPersistor, /*IPostProcessor,*/ ILogger
	{
		public Collection<ILogger> Loggers { get; private set; }
		public Collection<IPaymentMethodDetector> MethodDetectors { get; private set; }
		public Collection<IAccountManager> AccountManagers { get; private set; }
		public Collection<IValidator> Validators { get; private set; }
		public Collection<IRiskDetector> Risks { get; private set; }
		public Collection<IPersistor> Persistors { get; private set; }
        public Collection<IPersistComplete> PersistCompletes { get; private set; }
        public Collection<IProcessCompleteHandler> ProcessCompletes { get; private set; }
		public Collection<PostProcessTask> PostProcessTasks { get; private set; }
		public Dictionary<string, IPostProcessor> PostProcessors { get; private set; }
		public Dictionary<string, IProcessor> Processors { get; private set; }
		public HttpEndPointsManager EndPointsManager { get; private set; }

        public ProcessEngine()
		{
            Loggers = new Collection<ILogger>();
			MethodDetectors = new Collection<IPaymentMethodDetector>();
			AccountManagers = new Collection<IAccountManager>();
			Validators = new Collection<IValidator>();
			Risks = new Collection<IRiskDetector>();
			Processors = new Dictionary<string, IProcessor>();
			PostProcessors = new Dictionary<string, IPostProcessor>();
			Persistors = new Collection<IPersistor>();
			PostProcessTasks = new Collection<PostProcessTask>();
			EndPointsManager = new HttpEndPointsManager();
			//EndPointsManager.RegisterEndPonit(
		}

		~ProcessEngine() {
			StopEndPoints();
			StopPostTasks();
		}

		public void StartEndPoints()
		{
			EndPointsManager.Start();
		}

		public void StopEndPoints()
		{
			EndPointsManager.Stop();
		}

		public void StartPostTasks()
		{
			foreach (var task in PostProcessTasks) 
				task.Start();
		}
		public void StopPostTasks()
		{
			foreach (var task in PostProcessTasks) 
				task.Stop();
		}

		private void ChangeTransactionStage(TransactionContext transaction, TransactionStage toStage)
		{
			LogTransaction(false, transaction, null);
			transaction.Stage = toStage;
			LogTransaction(true, transaction, null);
		}

		public void LogTransaction(bool preStage, TransactionContext trans, Exception ex)
		{
			foreach (ILogger logger in Loggers) {
				try { logger.LogTransaction(preStage, trans, ex); }
				catch { }
			}
		}

		public void LogError(CommonTypes.TransactionStage stage, Exception ex)
		{
			foreach (ILogger logger in Loggers) {
				try{ logger.LogError(stage, ex); } 
				catch{ }
			}
		}

		public bool ValidateTransaction(TransactionContext transaction)
		{
			foreach (IValidator validator in Validators)
				if (!validator.ValidateTransaction(transaction))
					return true;

			return true;
		}

		public bool DetectRisk(TransactionContext transaction)
		{
			foreach (IRiskDetector detector in Risks)
				if (!detector.DetectRisk(transaction)) 
					return false;
			
			return true;
		}

        public void AddAmount(TransactionContext transaction, SaveOptions options, TransactionAmount transactionAmount)
		{
			foreach (IPersistor persistor in Persistors)
			{
				try {
                    persistor.AddAmount(transaction, options, transactionAmount);
					options &= ~SaveOptions.GenIds;
				} catch (Exception ex) {
					LogError(CommonTypes.TransactionStage.PostProcess, ex);
				}
			}
		}

		public void AddLog(TransactionContext transaction, SaveOptions options, TransactionHistoryType historyType, string description, int? refNumber, string refUrl, bool? isSucceeded)
		{
			foreach (IPersistor persistor in Persistors)
			{
				try {
					persistor.AddLog(transaction, options, historyType, description, refNumber, refUrl, isSucceeded);
					options &= ~SaveOptions.GenIds;
				} catch (Exception ex) {
					LogError(CommonTypes.TransactionStage.PostProcess, ex);
				}
			}
		}

		public void Save(TransactionContext transaction, SaveOptions options)
		{
			foreach (IPersistor persistor in Persistors)
			{
				try
				{
					persistor.Save(transaction, options);
					options &= ~SaveOptions.GenIds;
				}
				catch (Exception ex)
				{
					LogError(CommonTypes.TransactionStage.Process, ex);
				}
			}
			foreach (IPersistComplete persistComplete in PersistCompletes)
            {
                try { persistComplete.SaveComplete(transaction, options); }
                catch (Exception ex) { LogError(CommonTypes.TransactionStage.Process, ex); }
            }
		}

        public TransactionContext Load(TransLoadInfo transLoadInfo)
		{
			TransactionContext transactionContext = null;
			foreach (IPersistor persistor in Persistors)
			{
				try
				{
					if ((transactionContext = persistor.Load(transLoadInfo)) != null)
						return transactionContext;
				}
				catch (Exception ex)
				{
					LogError(CommonTypes.TransactionStage.PostProcess, ex);
				}
			}
			if (transactionContext != null) {
				foreach (IPersistComplete persistComplete in PersistCompletes)
				{
					try { persistComplete.LoadComplete(transactionContext); }
					catch (Exception ex) { LogError(CommonTypes.TransactionStage.Process, ex); }
				}
			}
			return transactionContext;
		}

		public bool DetectMethod(DetectMethodData data)
		{
			foreach (IPaymentMethodDetector currentDetector in MethodDetectors)
			{
				try
				{
					if (currentDetector.DetectMethod(data)) 
						return true;
				}
				catch (Exception ex)
				{
					LogError(CommonTypes.TransactionStage.Process, ex);
				}
			}

			return false;
		}

		public bool Process(TransactionContext transaction)
		{
			IProcessor processor = null;
			if (!Processors.TryGetValue(transaction.Config.DebitProcessor, out processor)) return false;
			return processor.Process(transaction);
		}

		public CommonTypes.ProcessTransactionType[] SupportedTypes
		{
			get
			{
				return new ProcessTransactionType[] { 
					ProcessTransactionType.Sale, 
					ProcessTransactionType.Authorize, 
					ProcessTransactionType.Capture, 
					ProcessTransactionType.Refund, 
					ProcessTransactionType.Reverse,
					ProcessTransactionType.Credit
				};
			}
		}

		public void ProcessComplete(TransactionContext transaction)
		{
			foreach (IProcessCompleteHandler completeHandler in ProcessCompletes)
				completeHandler.ProcessComplete(transaction);
		}

		public bool LoadConfig(TransactionContext transaction)
		{
			ProcessConfig processConfig;
			foreach (IAccountManager lc in AccountManagers)
			{
				try
				{
					if (lc.GetConfig(transaction, out processConfig))
					{
						transaction.Config = processConfig;
						return true;
					}
				}
				catch (Exception ex)
				{
					LogError(CommonTypes.TransactionStage.Process, ex);
				}
			}

			return false;
		}

		public TransactionResponse Process(TransactionRequest Request)
		{
			TransactionContext oldContext = null;
			TransactionContext context = new TransactionContext(Application.DomainHosts, Request);
			try
			{
				if (!DetectMethod(context.Method)) 
					throw new TransactionException(ProcessTransactionStatus.Declined, 0, "could not detect method");
				Request.paymentMethod = context.Method.paymentMethod;
				if (!LoadConfig(context)) 
					throw new TransactionException(ProcessTransactionStatus.Declined, 0, "unable to call LoadConfig");
				ValidateTransaction(context);
				if (Request.type != ProcessTransactionType.Authorize && Request.type != ProcessTransactionType.Sale)
				{
					TransLoadInfo transLoadInfo = new TransLoadInfo();
					transLoadInfo.id = Request.refTransID.Value;
					transLoadInfo.status = ProcessTransactionStatus.Approved;
					transLoadInfo.type = ProcessTransactionType.Authorize;
					transLoadInfo.domain = context.Domain;
					oldContext = Load(transLoadInfo);
					if (oldContext == null) 
						throw new TransactionException(ProcessTransactionStatus.Declined, 0, "unable to load refTransID");
				}
				if (DetectRisk(context))
					if (!Process(context))
						throw new TransactionException(ProcessTransactionStatus.Declined, 0, string.Format("unable to to find debitor '{0}'", context.Config.DebitProcessor));
			}
			catch (TransactionException ex)
			{
				context.Response = ex.Reply;
			}
			catch (Exception ex)
			{
#if DEBUG
				context.Response = new TransactionResponse(ProcessTransactionStatus.Declined, "", ex.Message);
				//throw ex;
#else
				context.Response = new TransactionResponse(ProcessTransactionStatus.Declined, "", "Unexpected error");
#endif
			}
			finally
			{
				ProcessComplete(context);
			}
			context.Response.ID = context.ID;
			try {
				Save(context, SaveOptions.GenIds);
			} catch(Exception ex) {
				LogError(CommonTypes.TransactionStage.Process, ex);
			}
			return context.Response;
		}

		public PostProcessResult PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			IPostProcessor processor;
			PostProcessors.TryGetValue(pendingData.EventType.ToString(), out processor);
			if (processor == null)
				throw new Exception(string.Format("unable to find PostProcessor to event {0}:'{1}'", (int)pendingData.EventType, pendingData.EventType.ToString()));
			if (processor.Disabled) return PostProcessResult.IsDisabled;
			return processor.PostProcess(this, transaction, pendingData);
		}
    }
}
