﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Netpay.CommonTypes;

namespace Netpay.Process
{
    public class PostProcessTask
    {
        private volatile bool _started = false;
        private Thread _runningThread = null;
        public List<PendingEventType> FilterTypes { get; set; }
        public List<PendingEventType> ExcludeTypes { get; set; }
        public int MinuteInterval { get; set; }

        public PostProcessTask()
        {
            _runningThread = new Thread(new ThreadStart(ThreadProc));
        }

        public bool IsStarted
        {
            get
            {
                return _started;
            }
        }

        public void Start()
        {
            if (!_started)
            {
                _started = true;
                _runningThread.Start();
            }
        }

        public void Stop()
        {
            if (_started)
            {
                _started = false;
                _runningThread.Interrupt();
                while (_runningThread.IsAlive) ;
            }
        }

        protected void HandleDomain(string domainName)
        {
            Infrastructure.Domain.Current = Infrastructure.Domain.Get(domainName);
            Infrastructure.ObjectContext.Current.CredentialsToken = Infrastructure.Domain.Current.ServiceCredentials;
            TransactionContext tc = null;
            var currentPendingData = Application.PendingEventManager.GetNext(FilterTypes != null && FilterTypes.Count > 0 ? FilterTypes.ToArray() : null, ExcludeTypes != null && ExcludeTypes.Count > 0 ? ExcludeTypes.ToArray() : null);
            while (currentPendingData != null)
            {
                Infrastructure.DataContext.Writer = null;
                if (currentPendingData.Retries > 0) currentPendingData.Retries--;
                bool useChache = false;
                try
                {
                    if (currentPendingData.TransID != null) {
                        useChache = (tc != null) && (tc.ID == currentPendingData.TransID) && (tc.Request.type == currentPendingData.TransType) && (tc.Response.Status == currentPendingData.TransStatus);
                        if (!useChache) {
                            TransLoadInfo transLoadInfo = new TransLoadInfo();
                            transLoadInfo.domain = domainName;
                            transLoadInfo.type = currentPendingData.TransType;
                            transLoadInfo.status = currentPendingData.TransStatus;
                            transLoadInfo.id = currentPendingData.TransID.GetValueOrDefault();
                            //if (currentPendingData.TransID == 0) tc = null;
                            tc = Application.ProcessEngine.Load(transLoadInfo);
                            if (tc == null) throw new Exception(string.Format("Unable to load pending transaction return null"));
                            tc.Stage = CommonTypes.TransactionStage.PostProcess;
                        }
                    } else tc = null;
                    switch (Application.ProcessEngine.PostProcess(Application.ProcessEngine, tc, currentPendingData))
                    {
                        case Modules.PostProcessResult.Succseeded:
                            Application.PendingEventManager.SetPendingStatus(currentPendingData, true, null);
                            break;
                        case Modules.PostProcessResult.IsDisabled:
                            break;
                        case Modules.PostProcessResult.FailedNoRetry:
                            Application.PendingEventManager.SetPendingStatus(currentPendingData, true, null);
                            break;
                        case Modules.PostProcessResult.Failed:
                            Application.PendingEventManager.SetPendingStatus(currentPendingData, false, null);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    ex = new Exception(string.Format("Pending {0} Domain:{1}, ID:{2}, TransType:{3}, Status:{4}, Cach:{5}", currentPendingData.EventType, domainName, currentPendingData.TransID, currentPendingData.TransType, currentPendingData.TransStatus, useChache), ex);
                    if (tc != null) Application.ProcessEngine.LogTransaction(false, tc, ex);
                    else Application.ProcessEngine.LogError(CommonTypes.TransactionStage.PostProcess, ex);
                    Application.PendingEventManager.SetPendingStatus(currentPendingData, false, ex.Message);
                }
                currentPendingData = Application.PendingEventManager.GetNext(FilterTypes != null && FilterTypes.Count > 0 ? FilterTypes.ToArray() : null, ExcludeTypes != null && ExcludeTypes.Count > 0 ? ExcludeTypes.ToArray() : null);
            }
        }


        private void ThreadProc()
        {
            while (_started)
            {
                var splitItems = Application.DomainHosts.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var domainName in splitItems)
                {
                    try
                    {
                        HandleDomain(domainName);
                    }
                    catch (Exception ex)
                    {
                        Application.ProcessEngine.LogError(CommonTypes.TransactionStage.PostProcess, ex);
                    }
                }
                Thread.Sleep(MinuteInterval * (60 * 1000));
            }
        }
    }
}
