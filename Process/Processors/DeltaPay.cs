﻿using System;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Processors
{
    public class DeltaPay : Modules.IModule, Modules.IProcessor, HttpEndPointsManager.IHttpEndPoint
    {
        private string outerAddress = null;
        private const string NotifyServiceName = "DeltaPayGW/";
        public CommonTypes.ProcessTransactionType[] SupportedTypes
        {
            get { return new CommonTypes.ProcessTransactionType[] { CommonTypes.ProcessTransactionType.Sale, ProcessTransactionType.Authorize, ProcessTransactionType.Capture, CommonTypes.ProcessTransactionType.Refund }; }
        }

        void Modules.IModule.Load()
        {
            outerAddress = Application.ProcessEngine.EndPointsManager.RegisterEndPonit(NotifyServiceName, this);
        }

        void Modules.IModule.Unload()
        {
            if (outerAddress != null) Application.ProcessEngine.EndPointsManager.RemoveEndPonit(NotifyServiceName);
        }

        public void RemoteInvoke(System.Net.HttpListenerContext context)
        {
            throw new NotImplementedException();
        }

        private int mapCardType(PaymentMethodEnum pm)
        {
            switch(pm){
                case PaymentMethodEnum.CCVisa: return 1;
                case PaymentMethodEnum.CCDiners: return 4;
                case PaymentMethodEnum.CCAmex: return 3;
                case PaymentMethodEnum.CCMastercard: return 2;
                case PaymentMethodEnum.CCMaestro: return 7;
                case PaymentMethodEnum.CCJcb: return 5;
                default: return 0;
            }
        }

        private int mapOperation(ProcessTransactionType transType)
        {
            switch(transType){
                case ProcessTransactionType.Sale: return 1;
                case ProcessTransactionType.Authorize: return 2;
                case ProcessTransactionType.Capture: return 3;
                case ProcessTransactionType.Refund: return 5;
                default: return 0;
            }
        }

        private void addParam(System.Text.StringBuilder sb, System.Text.StringBuilder sbSign, string prName, string prValue)
        {
            sb.Append("&" + prName + "=" + System.Web.HttpUtility.UrlEncode(prValue));
            sbSign.Append(prValue); //Replace(Server.URLEncode(prValue), "+", " ")
        }

        public bool Process(TransactionContext transaction)
        {
			bool IsNewRequest = true;
			StringBuilder sb = new StringBuilder();
			StringBuilder sbSign = new StringBuilder();
			string UrlAddress = "https://base2.credorax.com/crax_gate/test3D";
			if ((transaction.Config.Options & ProcessOptions.IsTestTerminal) != 0) UrlAddress = "https://base2.credorax.com/intenv/service/gateway";
			if (transaction.Request.type == ProcessTransactionType.Capture || transaction.Request.type == ProcessTransactionType.Refund)
			{
				if (string.IsNullOrEmpty(transaction.Request.debitorRefCode)) throw new TransactionException(ProcessTransactionStatus.Declined, 532, null); //Reftrans not Found
				if (transaction.Response.ApprovalNumber.IndexOf('#') < 0) throw new TransactionException(ProcessTransactionStatus.Declined, 532, null); //Reftrans not Found
				IsNewRequest = false;
			}

            addParam(sb, sbSign, "affiliate", transaction.Config.AccountName);
            addParam(sb, sbSign, "paymethod", "Credit Card");
            addParam(sb, sbSign, "post_method", "sync");
            addParam(sb, sbSign, "processing_mode", mapOperation(transaction.Request.type).ToString());
            addParam(sb, sbSign, "redirect", outerAddress);
            addParam(sb, sbSign, "order_id", transaction.Request.debitorRefCode);
            addParam(sb, sbSign, "amount", transaction.Request.amount.ToString("0.00"));
            addParam(sb, sbSign, "currency", transaction.Request.currencyISO);

            if (!IsNewRequest)
            {
                addParam(sb, sbSign, "order_id", transaction.Request.debitorRefCode);
                addParam(sb, sbSign, "customer_id", transaction.Response.ApprovalNumber);
                addParam(sb, sbSign, "reference_transaction_no", transaction.Request.debitorRefCode);
            }
            else
            {
                addParam(sb, sbSign, "room_name", transaction.Config.UserName);
                addParam(sb, sbSign, "agent_name", transaction.Config.Password);

                addParam(sb, sbSign, "location", "AFF");

                addParam(sb, sbSign, "first_name", transaction.Request.customer.firstName);
                addParam(sb, sbSign, "last_name", transaction.Request.customer.lastName);
                addParam(sb, sbSign, "address1", transaction.Request.billingAddress.Address1);
                addParam(sb, sbSign, "address2", transaction.Request.billingAddress.Address2);

                addParam(sb, sbSign, "city", transaction.Request.billingAddress.City);
                addParam(sb, sbSign, "state", transaction.Request.billingAddress.StateISO);
                addParam(sb, sbSign, "country", transaction.Request.billingAddress.CountryISO);
                addParam(sb, sbSign, "zip", transaction.Request.billingAddress.Postal);
                addParam(sb, sbSign, "telephone", transaction.Request.customer.phone);

                addParam(sb, sbSign, "email", transaction.Request.customer.email);

                addParam(sb, sbSign, "card_type", mapCardType(transaction.Method.paymentMethod).ToString());

                addParam(sb, sbSign, "card_number", transaction.Request.creditCard.Pan);
                addParam(sb, sbSign, "cvv", transaction.Request.creditCard.Cvv);
                addParam(sb, sbSign, "expiry_mo", transaction.Request.creditCard.ExpMonth.ToString());
                addParam(sb, sbSign, "expiry_yr", transaction.Request.creditCard.ExpYear.ToString());
                addParam(sb, sbSign, "customer_ip", transaction.Request.clientIP);
            }

            string strOut = null, returnCode = null, approvalNumber = null;
            CommonTypes.ProcessTransactionStatus retStatus = CommonTypes.ProcessTransactionStatus.Unknown;
            System.Net.HttpStatusCode res = WebHelper.HttpRequest(UrlAddress, sb.ToString(), out strOut, null);
            if (res == System.Net.HttpStatusCode.OK)
            {
                var resCol = System.Web.HttpUtility.ParseQueryString(strOut);
                returnCode = resCol["status"];
                approvalNumber =  resCol["customer_id"];
                transaction.Request.debitorRefCode = resCol["transaction_no"];

                if (returnCode == null) returnCode = string.Empty;
                else returnCode = returnCode.Trim();

	            if (returnCode == "APPROVED" || returnCode == "AUTHORIZED" || returnCode == "CAPTURED")
		            returnCode = "000";
	            else if (returnCode == "REFUND REQUEST")
		            returnCode = "001";
	            else{
    	            if (returnCode == "001") returnCode = "002"; //001 is taken by netpay
                    int dammy;
                    if (!int.TryParse(returnCode, out dammy)) returnCode = "002";
                }

                if (returnCode == "000") retStatus = CommonTypes.ProcessTransactionStatus.Approved;
                else retStatus = CommonTypes.ProcessTransactionStatus.Declined;
            } else {
                retStatus = CommonTypes.ProcessTransactionStatus.Declined;
            }
            transaction.Response = new Netpay.Process.TransactionResponse(retStatus, returnCode, approvalNumber);
            return true;            
        }

    }
}
