﻿using System;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Processors
{
	public class Local : Modules.IProcessor
	{
		public CommonTypes.ProcessTransactionType[] SupportedTypes
		{
			get { return Application.ProcessEngine.SupportedTypes; }
		}

		bool Modules.IProcessor.Process(TransactionContext transaction)
		{
			if ((transaction.Request.amount % 2) == 0) 
				transaction.Response = new Netpay.Process.TransactionResponse(ProcessTransactionStatus.Approved, "000", null);
			else transaction.Response = new Netpay.Process.TransactionResponse(ProcessTransactionStatus.Declined, "000", null);
			return true;
		}
	}
}
