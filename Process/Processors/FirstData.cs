﻿using System;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Processors
{
	public class FirstData : Modules.IProcessor
	{
		private enum TransactionTypes
		{
			Purchase = 00,
			PreAuthorization = 01,
			PreAuthorizationCompletion = 02,
			ForcedPost = 03,
			Refund = 04,
			PreAuthorizationOnly = 05,
			PayPalOrder = 07,
			Void = 13,
			TaggedPreAuthorizationCompletion = 32,
			TaggedVoid = 33,
			TaggedRefund = 34,
			CashOut = 83, //(ValueLink, v9 or higher end point only)
			Activation = 85, //(ValueLink, v9 or higher end point only)
			BalanceInquiry = 86, //(ValueLink, v9 or higher end point only)
			Reload = 88, //(ValueLink, v9 or higher end point only)
			Deactivation = 89, //(ValueLink, v9 or higher end point only)
		}

		public CommonTypes.ProcessTransactionType[] SupportedTypes
		{
			get
			{
				return new ProcessTransactionType[] { 
					ProcessTransactionType.Sale, 
					ProcessTransactionType.Authorize, 
					ProcessTransactionType.Capture, 
					ProcessTransactionType.Refund, 
					ProcessTransactionType.Credit
				};
			}
		}

		private TransactionTypes MapTransactionType(CommonTypes.ProcessTransactionType ptp)
		{
			switch(ptp){
			case ProcessTransactionType.Authorize:
				return TransactionTypes.PreAuthorization;
			case ProcessTransactionType.Capture:
				return TransactionTypes.PreAuthorizationCompletion;
			case ProcessTransactionType.Sale:
				return TransactionTypes.Purchase;
			case ProcessTransactionType.Reverse:
				return TransactionTypes.Void;
			case ProcessTransactionType.Refund:
				return TransactionTypes.Refund;
			case ProcessTransactionType.Credit:
				return TransactionTypes.CashOut;
			}
			throw new Exception("Transaction type not supported");
		}

		public bool Process(TransactionContext transaction)
		{
			StringBuilder sb = new StringBuilder();
			string UrlAddress = "https://api.globalgatewaye4.firstdata.com/transaction/v12";
			//if ((transaction.Config.Options & ProcessOptions.IsTestTerminal) != 0) UrlAddress = "https://test.soap.bs-card-service.com/soapapi/services/XmlApiNl";

			if (transaction.Request.type == CommonTypes.ProcessTransactionType.Capture || 
				transaction.Request.type == CommonTypes.ProcessTransactionType.Refund) 
			{

			}
			else if (transaction.Request.type == CommonTypes.ProcessTransactionType.Sale ||
				transaction.Request.type == CommonTypes.ProcessTransactionType.Authorize ||
				transaction.Request.type == CommonTypes.ProcessTransactionType.Credit
				)
			{
				sb.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				sb.Append(" <types:Transaction id=\"id1\" xsi:type=\"types:Transaction\">");
				sb.AppendFormat("  <ExactID xsi:type=\"xsd:string\">{0}</ExactID>", transaction.Config.UserName);
				sb.AppendFormat("  <Password xsi:type=\"xsd:string\">{0}</Password>", transaction.Config.Password);
				sb.AppendFormat("  <Transaction_Type xsi:type=\"xsd:string\">{0}</Transaction_Type>", MapTransactionType(transaction.Request.type));
				sb.AppendFormat("  <DollarAmount xsi:type=\"xsd:string\">{0}</DollarAmount>", transaction.Request.amount.ToString("0.00"));
				sb.AppendFormat("  <Currency xsi:type=\"xsd:string\">{0}</DollarAmount>", transaction.Request.currencyISO);
				//sb.AppendFormat("  <SurchargeAmount xsi:type=\"xsd:string\">{0}</SurchargeAmount>");
				sb.AppendFormat("  <Card_Number xsi:type=\"xsd:string\">{0}</Card_Number>", transaction.Request.creditCard.Pan);
				//sb.AppendFormat("  <Transaction_Tag xsi:type=\"xsd:string\">{0}</Transaction_Tag>");
				if (!string.IsNullOrEmpty(transaction.Request.creditCard.Track1))
					sb.AppendFormat("  <Track1 xsi:type=\"xsd:string\">{0}</Track1>", transaction.Request.creditCard.Track1);
				if (!string.IsNullOrEmpty(transaction.Request.creditCard.Track2)) 
					sb.AppendFormat("  <Track2 xsi:type=\"xsd:string\">{0}</Track2>", transaction.Request.creditCard.Track2);
				//sb.AppendFormat("  <PAN xsi:type=\"xsd:string\">{0}</PAN>");
				if (!string.IsNullOrEmpty(transaction.Request.ivrAuthCode))
					sb.AppendFormat("  <Authorization_Num xsi:type=\"xsd:string\">{0}</Authorization_Num>", transaction.Request.ivrAuthCode);
				sb.AppendFormat("  <Expiry_Date xsi:type=\"xsd:string\">{0}{1}</Expiry_Date>", transaction.Request.creditCard.ExpMonth.ToString("00"), transaction.Request.creditCard.ExpYear.ToString("00"));
				sb.AppendFormat("  <CardHoldersName xsi:type=\"xsd:string\">{0}</CardHoldersName>", transaction.Request.creditCard.OwnerName);
				//sb.AppendFormat("  <VerificationStr1 xsi:type=\"xsd:string\">{0}</VerificationStr1>");
				sb.AppendFormat("  <VerificationStr2 xsi:type=\"xsd:string\">{0}</VerificationStr2>", transaction.Request.creditCard.Cvv);
				sb.AppendFormat("  <CVD_Presence_Ind xsi:type=\"xsd:string\">{0}</CVD_Presence_Ind>", (string.IsNullOrEmpty(transaction.Request.creditCard.Cvv) ? "0" : "1"));
				sb.AppendFormat("  <ZipCode xsi:type=\"xsd:string\">{0}</ZipCode>", transaction.Request.billingAddress.Postal);
				//sb.AppendFormat("  <Tax1Amount xsi:type=\"xsd:string\">{0}</Tax1Amount>");
				//sb.AppendFormat("  <Tax1Number xsi:type=\"xsd:string\">{0}</Tax1Number>");
				//sb.AppendFormat("  <Tax2Amount xsi:type=\"xsd:string\">{0}</Tax2Amount>");
				//sb.AppendFormat("  <Tax2Number xsi:type=\"xsd:string\">{0}</Tax2Number>");
				sb.AppendFormat("  <Ecommerce_Flag xsi:type=\"xsd:string\">{0}</Ecommerce_Flag>", "7"); //ECI Indicator – Channel Encrypted Transaction
				//sb.AppendFormat("  <XID xsi:type=\"xsd:string\">{0}</XID>");
				//sb.AppendFormat("  <CAVV xsi:type=\"xsd:string\">{0}</CAVV>");
				//sb.AppendFormat("  <CAVV_Algorithm xsi:type=\"xsd:string\">{0}</CAVV_Algorithm>");
				sb.AppendFormat("  <Reference_No xsi:type=\"xsd:string\">{0}</Reference_No>", transaction.Request.debitorRefCode);
				sb.AppendFormat("  <Customer_Ref xsi:type=\"xsd:string\">{0}</Customer_Ref>", transaction.Request.merchantRefCode);
				//sb.AppendFormat("  <Reference_3 xsi:type=\"xsd:string\">{0}</Reference_3>");
				sb.AppendFormat("  <Language xsi:type=\"xsd:string\">{0}</Language>", "EN");
				sb.AppendFormat("  <Client_IP xsi:type=\"xsd:string\">{0}</Client_IP>", transaction.Request.clientIP);
				sb.AppendFormat("  <Client_Email xsi:type=\"xsd:string\">{0}</Client_Email>", transaction.Request.customer.email);
				sb.AppendFormat("  <soft_descriptor xsi:type=\"xsd:string\">{0}</soft_descriptor>", transaction.Config.Descriptor);
				sb.Append(" </types:Transaction>");

			}

			string strOut = null, returnCode = null, approvalNumber = null;
			CommonTypes.ProcessTransactionStatus retStatus = CommonTypes.ProcessTransactionStatus.Unknown;
			System.Net.HttpStatusCode res = WebHelper.HttpRequest(UrlAddress, sb.ToString(), out strOut, null);
			if (res == System.Net.HttpStatusCode.OK) {
			}

			transaction.Response = new Netpay.Process.TransactionResponse(retStatus, returnCode, approvalNumber);
			return true;
		}
	}
}
