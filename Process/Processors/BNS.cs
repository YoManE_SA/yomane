﻿using System;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Processors
{
	public class BNS : Modules.IProcessor
	{
		public bool UseBSCardAlias{ get; set; }
		public CommonTypes.ProcessTransactionType[] SupportedTypes
		{
			get
			{
				return new ProcessTransactionType[] { 
					ProcessTransactionType.Sale, 
					ProcessTransactionType.Authorize, 
					ProcessTransactionType.Capture, 
					ProcessTransactionType.Refund, 
					ProcessTransactionType.Credit
				};
			}
		}

		public bool Process(TransactionContext transaction)
		{
			StringBuilder sb = new StringBuilder();
			bool bRetryRefund = true, bStoreCard = false;
			string UrlAddress = "https://soap.bs-card-service.com/soapapi/services/XmlApiNl";
			if ((transaction.Config.Options & ProcessOptions.IsTestTerminal) != 0) UrlAddress = "https://test.soap.bs-card-service.com/soapapi/services/XmlApiNl";

			if (transaction.Request.type == CommonTypes.ProcessTransactionType.Capture || 
				transaction.Request.type == CommonTypes.ProcessTransactionType.Refund) 
			{
				string sendDebitReferenceCode = null;
				if (string.IsNullOrEmpty(transaction.Request.debitorRefCode)) throw new TransactionException(CommonTypes.ProcessTransactionStatus.Declined, 532, "Reftrans not Found");
				sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
				sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n");
				sb.Append(" <soap:Body>");
				sb.Append("  <xmlApiRequest xmlns=\"http://www.voeb-zvd.de/xmlapi/1.0\" id=\"a1234\" version=\"1.0\">\r\n");
				sb.Append("	 <paymentRequest id=\"reqid1\">\r\n");
				sb.Append("	  <merchantId>" + transaction.Config.AccountName + "</merchantId>\r\n");
				sb.Append("	  <timeStamp>" + DateTime.Now.ToString("g") + "</timeStamp>\r\n");
				sb.Append("	  <eventExtId>" + sendDebitReferenceCode + "</eventExtId>\r\n");
				sb.Append("	  <kind>creditcard</kind>\r\n");
				sb.Append("	  <action>" + (transaction.Request.type == CommonTypes.ProcessTransactionType.Refund ? "refund" : "capture") + "</action>\r\n");
				sb.Append("	  <txReferenceExtId>" + (transaction.Request.type == CommonTypes.ProcessTransactionType.Capture ? sendDebitReferenceCode : transaction.Request.debitorRefCode) + "</txReferenceExtId>\r\n");
				sb.Append("	  <changedAmount>" + (transaction.Request.amount * 100).ToString("0,.00") + "</changedAmount>\r\n");
				sb.Append("	 </paymentRequest>");
				sb.Append("  </xmlApiRequest>");
				sb.Append(" </soap:Body>");
				sb.Append("</soap:Envelope>");
			}
			else if (transaction.Request.type == CommonTypes.ProcessTransactionType.Sale ||
				transaction.Request.type == CommonTypes.ProcessTransactionType.Authorize ||
				transaction.Request.type == CommonTypes.ProcessTransactionType.Credit
				)
			{
				string storedCard = null;
				bool bUseStoredCard = false;
				if (transaction.Request.recurring != null /*|| Trim(requestSource) = "20"*/){
					//first or subsequent transaction in recurring series - use BNS card storage
					int nStoredCard = 0; //GetBnsStoredCardIdentifier(X_ccNumber, X_ccCVV2, TerminalId);
					if (nStoredCard < 0){
						//already stored - use stored card
						bStoreCard = false;
						bUseStoredCard = true;
						storedCard = (-nStoredCard).ToString();
					} else {
						//not stored - update the series
						bStoreCard = true;
						bUseStoredCard = false;
						storedCard = nStoredCard.ToString();
					}
					//if(TestNumVar(request("RecurringSeries"), 1, 0, 0) > 0) oledbData.Execute "EXEC RecurringSetSeriesIdentifier " & request("RecurringSeries") & ", '" & sStoredCard & "';"
					//NewRecurringSeriesStoredCardIdentifier = sStoredCard
				}
        
				string actionString = null;
				if (transaction.Request.type == CommonTypes.ProcessTransactionType.Credit) actionString = "credit";
				else actionString = (transaction.Request.type == CommonTypes.ProcessTransactionType.Authorize) ? "preauthorization" : "authorization";

				//transaction.Request.debitorRefCode = GetTransRefCode();
				sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
				sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n");
				sb.Append(" <soap:Body>\r\n");
				sb.Append("  <xmlApiRequest xmlns=\"http://www.voeb-zvd.de/xmlapi/1.0\" id=\"a1234\" version=\"1.0\">\r\n");
				sb.Append("	<paymentRequest id=\"reqid1\">\r\n");
				sb.Append("	 <merchantId>" + transaction.Config.AccountName + "</merchantId>\r\n");
				sb.Append("	 <timeStamp>" + DateTime.Now.ToString("g") + "</timeStamp>\r\n");
				sb.Append("	 <eventExtId>" + transaction.Request.debitorRefCode + "</eventExtId>\r\n");
				sb.Append("	 <kind>creditcard</kind>\r\n");
				if (!string.IsNullOrEmpty(transaction.Config.Descriptor)) sb.Append("	 <additionalNote>" + transaction.Config.Descriptor + "</additionalNote>\r\n");
				sb.Append("	 <action>" + actionString + "</action>\r\n");
				sb.Append("	 <amount>" + (transaction.Request.amount * 100) + "</amount>\r\n");
				sb.Append("	 <currency>" + transaction.Request.currencyISO + "</currency>\r\n");
				sb.Append("	 <creditCard>\r\n");
				if ((!bUseStoredCard) || (!UseBSCardAlias)) sb.Append("	   <pan>" + transaction.Request.creditCard.Pan + "</pan>\r\n");
				if (UseBSCardAlias && (bStoreCard || bUseStoredCard)) sb.Append("	   <panalias" + (bStoreCard ? " generate=\"true\"" : "\"\"") + ">" + storedCard + "</panalias>\r\n");
				if ((!bUseStoredCard) || (!UseBSCardAlias)){ 
					 sb.Append("	 <expiryDate>\r\n");
					 sb.Append("	  <month>" + transaction.Request.creditCard.ExpMonth.ToString("00") + "</month>\r\n");
					 sb.Append("	  <year>" + transaction.Request.creditCard.ExpYear.ToString() + "</year>\r\n");
					 sb.Append("	 </expiryDate>\r\n");
					 sb.Append("	 <holder>" + transaction.Request.creditCard.OwnerName + "</holder>\r\n");
					 if (string.IsNullOrEmpty(transaction.Request.creditCard.Cvv)) sb.Append("	 <verificationCode>" + transaction.Request.creditCard.Cvv + "</verificationCode>\r\n");
				}
				sb.Append("	   </creditCard>\r\n");
				sb.Append("	  </paymentRequest>\r\n");
				sb.Append("  </xmlApiRequest>\r\n");
				sb.Append(" </soap:Body>\r\n");
				sb.Append("</soap:Envelope>\r\n");			
			}

			string strOut = null, returnCode = null, approvalNumber = null;
			CommonTypes.ProcessTransactionStatus retStatus = CommonTypes.ProcessTransactionStatus.Unknown;
			System.Net.HttpStatusCode res = WebHelper.HttpRequest(UrlAddress, sb.ToString(), out strOut, null);
			//HttpReq.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
			//HttpReq.setRequestHeader "SOAPAction", "process"
			if (res == System.Net.HttpStatusCode.OK) {
				//Set xmlRet = HttpReq.responseXml
				//sReturnCode = Trim(xmlRet.selectSingleNode("//rc").text)
				//sApprovalNumber = xmlRet.selectSingleNode("//aid").text
				//sError = xmlRet.selectSingleNode("//message").text
			}
			//HttpError = FormatHttpRequestError(HttpReq)
			//Dim sDebitRequest : sDebitRequest = ParamList
			//sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
			//sDebitRequest = Replace(sDebitRequest, "<verificationCode>" & X_ccCVV2, "<verificationCode>" & GetSafePartialNumber(X_ccCVV2))
			//SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails

			if (transaction.Request.type == CommonTypes.ProcessTransactionType.Refund && (returnCode == "1316" || returnCode == "1507" || returnCode == "1509" || returnCode == "1510")){
				if (bRetryRefund){
					transaction.Request.debitorRefCode = "";
					return Process(transaction);
				}
			}

			if (returnCode == "0000") returnCode = "000";
			else if (returnCode == "001") returnCode = "002";
			else if (bStoreCard) {
				//oledbData.Execute "DELETE FROM tblBnsStoredCard WHERE ID=" & TestNumVar(sStoredCard, 1, 0, 0)
				//if (transaction.Request.recurring != null) oledbData.Execute "EXEC RecurringSetSeriesIdentifier " & request("RecurringSeries") & ";"
			}
			transaction.Response = new Netpay.Process.TransactionResponse(retStatus, returnCode, approvalNumber);
			return true;
		}
	}
}
