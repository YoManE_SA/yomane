﻿using System;
using System.Collections.Generic;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Processors
{
	public class AllCharge : Modules.IProcessor
	{
		public CommonTypes.ProcessTransactionType[] SupportedTypes
		{
			get
			{
				return new ProcessTransactionType[] { 
					ProcessTransactionType.Sale, 
					ProcessTransactionType.Authorize, 
					ProcessTransactionType.Capture, 
					ProcessTransactionType.Refund 
				};
			}
		}

        private int mapCardType(PaymentMethodEnum pm) {
            switch (pm)
            {
                case CommonTypes.PaymentMethodEnum.CCAmex: return 1;
                case CommonTypes.PaymentMethodEnum.CCVisa: return 2; 
                case CommonTypes.PaymentMethodEnum.CCMastercard: return 3;
            }
            return 0;
        }

		bool Modules.IProcessor.Process(TransactionContext transaction)
		{
			StringBuilder sb = new StringBuilder();
            System.Text.Encoding enc = System.Text.Encoding.UTF8;
            string UrlAddress = "https://incharge.allcharge.com/html/";
			if ((transaction.Config.Options & ProcessOptions.IsTestTerminal) != 0) UrlAddress = "http://demo.allcharge.com/html/";
			if (transaction.Request.type == CommonTypes.ProcessTransactionType.Capture || 
				transaction.Request.type == CommonTypes.ProcessTransactionType.Refund) 
			{
				if (string.IsNullOrEmpty(transaction.Request.debitorRefCode)) throw new TransactionException(CommonTypes.ProcessTransactionStatus.Declined, 532, "Reftrans not Found");
                sb.Append("DCPId=" + System.Web.HttpUtility.UrlEncode(transaction.Config.UserName, enc));
                sb.Append("&DCPPassword=" + System.Web.HttpUtility.UrlEncode(transaction.Config.Password, enc));
                sb.Append("&transactionID=" + System.Web.HttpUtility.UrlEncode(transaction.Request.debitorRefCode, enc));
                sb.Append("&Amount=" + System.Web.HttpUtility.UrlEncode((transaction.Request.amount * 100).ToString("0.00"), enc));
                sb.Append("&Currency=" + System.Web.HttpUtility.UrlEncode(transaction.Request.currencyISO, enc));
				UrlAddress += "aspTransactionRefund.asp";
			}
			else if (transaction.Request.type == CommonTypes.ProcessTransactionType.Sale || 
				transaction.Request.type == CommonTypes.ProcessTransactionType.Authorize)
			{
                transaction.Request.debitorRefCode = Application.GenerateUniqueRefID();
                sb.Append("DCPId=" + System.Web.HttpUtility.UrlEncode(transaction.Config.UserName, enc));
                sb.Append("&DCPPassword=" + System.Web.HttpUtility.UrlEncode(transaction.Config.Password, enc));
                sb.Append("&email=" + System.Web.HttpUtility.UrlEncode(transaction.Request.customer.email, enc));
                sb.Append("&currency=" + System.Web.HttpUtility.UrlEncode(transaction.Request.currencyISO, enc));
                sb.Append("&cardType=" + System.Web.HttpUtility.UrlEncode(mapCardType(transaction.Method.paymentMethod).ToString(), enc));
                sb.Append("&cardNum=" + System.Web.HttpUtility.UrlEncode(transaction.Request.creditCard.Pan, enc));
                sb.Append("&CVV2/PIN=" + System.Web.HttpUtility.UrlEncode(transaction.Request.creditCard.Cvv, enc));
                sb.Append("&FirstName=" + System.Web.HttpUtility.UrlEncode(transaction.Request.customer.firstName, enc));
                sb.Append("&LastName=" + System.Web.HttpUtility.UrlEncode(transaction.Request.customer.lastName, enc));
                sb.Append("&Address=" + System.Web.HttpUtility.UrlEncode(transaction.Request.billingAddress.Address1, enc));
                if (!string.IsNullOrEmpty(transaction.Request.billingAddress.Address2))
                    sb.Append("&Address2=" + System.Web.HttpUtility.UrlEncode(transaction.Request.billingAddress.Address2, enc));
                sb.Append("&City=" + System.Web.HttpUtility.UrlEncode(transaction.Request.billingAddress.City, enc));
                sb.Append("&State=" + System.Web.HttpUtility.UrlEncode(transaction.Request.billingAddress.StateISO, enc));
                sb.Append("&postCode=" + System.Web.HttpUtility.UrlEncode(Validation.TestVar(transaction.Request.billingAddress.Postal, -1, ""), enc));
                sb.Append("&Country=" + System.Web.HttpUtility.UrlEncode(transaction.Request.billingAddress.CountryISO, enc));
                sb.Append("&Phone=" + System.Web.HttpUtility.UrlEncode(transaction.Request.customer.phone, enc));
                sb.Append("&ExpMonth=" + System.Web.HttpUtility.UrlEncode(transaction.Request.creditCard.ExpMonth.ToString("00"), enc));
                sb.Append("&ExpYear=" + System.Web.HttpUtility.UrlEncode(transaction.Request.creditCard.ExpYear.ToString("0000"), enc));
                sb.Append("&amount=" + System.Web.HttpUtility.UrlEncode(((long)(transaction.Request.amount * 100)).ToString(), enc));
                if (!string.IsNullOrEmpty(transaction.Request.debitorRefCode)) 
                    sb.Append("&transactionID=" + System.Web.HttpUtility.UrlEncode(transaction.Request.debitorRefCode, enc));
                if (transaction.IPAddress != null) sb.Append("&IPaddress=" + System.Web.HttpUtility.UrlEncode(transaction.IPAddress.ToString(), enc));
				sb.Append("&itemType=" + "other");
                sb.Append("&Desc=" + System.Web.HttpUtility.UrlEncode(transaction.Config.Descriptor, enc));
				//sb.Append("&CCReceipt=" + System.Web.HttpUtility.UrlEncode(transaction.Response.ApprovalNumber));
				UrlAddress += "aspCreditCharge.asp";
			}
			string strOut = null;
			System.Net.HttpStatusCode res = WebHelper.HttpRequest(UrlAddress, sb.ToString(), out strOut, null);
            transaction.Response = new Netpay.Process.TransactionResponse();
            if (res == System.Net.HttpStatusCode.OK) {
                var resCol = System.Web.HttpUtility.ParseQueryString(strOut, enc);
                transaction.Response.ErrorCode = resCol["retCode"];
                transaction.Response.ApprovalNumber = resCol["receipt"];
				transaction.Response.Reason = string.IsNullOrEmpty(resCol["ErrCode"]) ? null : resCol["ErrCode"].Replace("'", "");
				//HttpError = FormatHttpRequestError(HttpReq)
				//sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
				//sDebitRequest = Replace(sDebitRequest, "CVV2/PIN=" & X_ccCVV2, "CVV2/PIN=" & GetSafePartialNumber(X_ccCVV2))
				//SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails
	    		//if (returnCode == "001") returnCode = "002"; //001 is taken by netpay
                if (transaction.Response.ErrorCode == "0") transaction.Response.Status = CommonTypes.ProcessTransactionStatus.Approved;
                else transaction.Response.Status = CommonTypes.ProcessTransactionStatus.Declined;
                if (string.IsNullOrEmpty(transaction.Response.ErrorCode) && !string.IsNullOrEmpty(transaction.Response.Reason))
                    transaction.Response.ErrorCode = transaction.Response.Reason;
            } else {
                transaction.Response.Status = CommonTypes.ProcessTransactionStatus.Declined;
			}
			return true;
		}
	}
}
