﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Netpay.CommonTypes;

namespace Netpay.Process.Processors
{
	public class Shva : Modules.IProcessor
	{
		public string ServerURL { get; set; }

		[DllImport("Ash96.dll")]
		static extern Boolean intr99(string pIN, out string pOut, string[] pEnv);
		[DllImport("Ash96.dll")]
		static extern Boolean intr2000(string pIN, out string pOut, string pEnv);
		[DllImport("Ash96.dll")]
		static extern Boolean intrface();

		public CommonTypes.ProcessTransactionType[] SupportedTypes
		{
			get
			{
				return new ProcessTransactionType[] { 
					ProcessTransactionType.Sale, 
					ProcessTransactionType.Authorize, 
					ProcessTransactionType.Capture, 
					ProcessTransactionType.Refund 
				};
			}
		}

		private string ShvaString(TransactionRequest transaction)
		{
			StringBuilder shvaBuilder = new StringBuilder();
			if (!string.IsNullOrEmpty(transaction.creditCard.Track2))
				shvaBuilder.Append("A" + transaction.creditCard.Pan + "=" + transaction.creditCard.Track2 + "W" + transaction.creditCard.Pan.Substring(transaction.creditCard.Pan.Length - 4, 4));
			else shvaBuilder.Append("B" + transaction.creditCard.Pan);
			shvaBuilder.Append("C" + (int)(transaction.amount * 100));

			if (transaction.type == ProcessTransactionType.Capture) shvaBuilder.Append("D51" + "1");
			else shvaBuilder.Append("D01"/*+ creditType*/);

			if (transaction.currencyISO != "ILS" && transaction.currencyISO != "USD") shvaBuilder.Append((transaction.currencyISO + 1) + "50");
			else shvaBuilder.Append("050" + "$" + transaction.currencyISO); //CHANGED BY ASHRAIT VER 5.83

			if (transaction.ivrAuthCode != null)
			{
				//20100113 Tamir - when refunding transaction by VISA CAL gift card, approval number is not passed
				//if Trim(X_TypeCredit) <> "0" Or Left(Replace(X_ccNumber," ",""), 6) <> "458041" Then 
				shvaBuilder.Append("E" + transaction.ivrAuthCode);
			}

			if (transaction.installmets > 1)
			{
				decimal eachPay = (int)transaction.amount / transaction.installmets;
				decimal firstPay = transaction.amount - (eachPay * (transaction.installmets - 1));
				shvaBuilder.Append("F" + (int)(firstPay * 100) + "G" + (int)(eachPay * 100) + "H" + (transaction.installmets - 1));
			}
			else 
				transaction.installmets = 1;
			//else if int(X_TypeCredit) = 6 then sString = sString & "H" & X_Payments
			shvaBuilder.Append("I110");
			if (transaction.type == ProcessTransactionType.Authorize) 
				shvaBuilder.Append("J5");
			else 
				shvaBuilder.Append("J4"); //debit
			shvaBuilder.Append("T" + transaction.creditCard.ExpMonth.ToString("00") + transaction.creditCard.ExpYear.ToString("00"));
			if (transaction.creditCard.Cvv != null) 
				shvaBuilder.Append("U" + transaction.creditCard.Cvv);
			if (transaction.customer.personalID != null) 
				shvaBuilder.Append("Y" + transaction.customer.personalID);
			//sString = sString &  "X" & X_Customer' & "'" & sIP
			return shvaBuilder.ToString();
		}

		bool Modules.IProcessor.Process(TransactionContext transaction)
		{
			string strOut = "", strIn = ShvaString(transaction.Request);
			if (ServerURL != null) {
				strIn = "?CompanyID=" + transaction.Config.MerchantID + "&Terminal=" + transaction.Config.TerminalNumber + "&IsMaster=" + (true ? 1 : 0) + "&ShvaString=" + strIn;
				WebHelper.HttpRequest(ServerURL, strIn, out strOut, null);
			} else intr99(strIn, out strOut, null);
            transaction.Response = new Netpay.Process.TransactionResponse();
            transaction.Response.ErrorCode = strOut.Substring(0, 3);
            transaction.Response.Status = transaction.Response.ErrorCode == "000" ? ProcessTransactionStatus.Approved : ProcessTransactionStatus.Declined;
            if (strOut.Length >= 78) transaction.Response.ApprovalNumber = strOut.Substring(71, 7).Trim();
			return true;
		}
	}
}
