﻿using System;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Processors
{
	public class PPro : Modules.IModule, Modules.IProcessor, HttpEndPointsManager.IHttpEndPoint
	{
        private const string NotifyServiceName = "PProCCIN/";
        private string outerAddress = null; 
		public CommonTypes.ProcessTransactionType[] SupportedTypes
		{
			get
			{
				return new ProcessTransactionType[] { 
					ProcessTransactionType.Sale, 
					ProcessTransactionType.Authorize, 
					ProcessTransactionType.Capture, 
					ProcessTransactionType.Refund, 
					ProcessTransactionType.BankBack, 
			    };
		    }
		}

		public void Load()
		{
            outerAddress = Application.ProcessEngine.EndPointsManager.RegisterEndPonit(NotifyServiceName, this);
		}

		public void Unload()
		{
            if (outerAddress != null) Application.ProcessEngine.EndPointsManager.RemoveEndPonit(NotifyServiceName);
		}

		public bool Process(TransactionContext transaction)
		{
			StringBuilder sb = new StringBuilder();
			//if Len(sCompanyDescriptor) < 15 Then sCompanyDescriptor = sCompanyDescriptor & Space(15 - Len(sCompanyDescriptor))
			string UrlAddress = "https://gw.girogate.de";
			if ((transaction.Config.Options & ProcessOptions.IsTestTerminal) != 0) UrlAddress = "https://testgw.girogate.de";
			if (transaction.Request.type == ProcessTransactionType.Authorize) throw new TransactionException(ProcessTransactionStatus.Declined, 503, null);

			if (transaction.Request.type == ProcessTransactionType.BankBack) {
				//OleDbData.Execute "Delete From tblCompanyTransPending Where companyTransPending_id=" & X_RefTransID
				sb.Append("returnmode=urlencode");
				sb.Append("&txtype=GETTXSTATUS");
				sb.Append("&login=" + System.Web.HttpUtility.UrlEncode(transaction.Config.AccountName));
				sb.Append("&password=" + System.Web.HttpUtility.UrlEncode(transaction.Config.Password));
				sb.Append("&contractid=" + System.Web.HttpUtility.UrlEncode(transaction.Config.TerminalNumber));
				sb.Append("&txid=" + System.Web.HttpUtility.UrlEncode(transaction.Response.ApprovalNumber));
			} else if (transaction.Request.type == ProcessTransactionType.Capture || transaction.Request.type == ProcessTransactionType.Refund) {
				if (string.IsNullOrEmpty(transaction.Request.debitorRefCode)) throw new TransactionException(ProcessTransactionStatus.Declined, 532, null); //Reftrans not Found
			} else {
				//transaction.Request.debitorRefCode = GetTransRefCode();
				sb.Append("returnmode=urlencode");
				sb.Append("&txtype=TRANSACTION");
				sb.Append("&login=" + System.Web.HttpUtility.UrlEncode(transaction.Config.AccountName));
				sb.Append("&password=" + System.Web.HttpUtility.UrlEncode(transaction.Config.Password));
				sb.Append("&contractid=" + System.Web.HttpUtility.UrlEncode(transaction.Config.TerminalNumber));
				sb.Append("&channel=testchannel");
				sb.Append("&tag=" + System.Web.HttpUtility.UrlEncode("dumbdummy"));
				sb.Append("&currency=" + transaction.Request.currencyISO);
				sb.Append("&amount=" + (transaction.Request.amount * 100));
				sb.Append("&countrycode=" + System.Web.HttpUtility.UrlEncode(transaction.Request.billingAddress.CountryISO));
				sb.Append("&preferredlanguage=en");
				sb.Append("&accountholdername=" + System.Web.HttpUtility.UrlEncode(transaction.Request.creditCard.OwnerName));
				sb.Append("&merchantxid=" + System.Web.HttpUtility.UrlEncode(transaction.Request.debitorRefCode));
				sb.Append("&merchantredirecturl=" + System.Web.HttpUtility.UrlEncode(outerAddress + "RedirectURL"));
				sb.Append("&merchanterrorurl=" + System.Web.HttpUtility.UrlEncode(outerAddress + "ErrorURL"));
				sb.Append("&notificationurl=" + System.Web.HttpUtility.UrlEncode(outerAddress + "NptifyURL"));
				sb.Append("&cc.number=" + System.Web.HttpUtility.UrlEncode(transaction.Request.creditCard.Pan));
				sb.Append("&cc.cvv=" + System.Web.HttpUtility.UrlEncode(transaction.Request.creditCard.Cvv));
				sb.Append("&specin.expmonth=" + transaction.Request.creditCard.ExpMonth.ToString());
				sb.Append("&specin.expyear=20" + transaction.Request.creditCard.ExpYear.ToString());
			}

			//Response.Write(session("ProcessURL") & "<br>" & ParamList & vbCrLf & "<br>") : Response.End()
			string strOut = null, returnCode = null, approvalNumber = null;
			CommonTypes.ProcessTransactionStatus retStatus = CommonTypes.ProcessTransactionStatus.Unknown;

			//HttpReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-8"
			//call HttpReq.setOption(3, @"LOCAL_MACHINE\My\Udi Azulay");
			System.Net.HttpStatusCode res = WebHelper.HttpRequest(UrlAddress, sb.ToString(), out strOut, null);
			if (res == System.Net.HttpStatusCode.OK)
			{
				var resCol = System.Web.HttpUtility.ParseQueryString(strOut);
				returnCode = resCol["STATUS"];
				approvalNumber = resCol["TXID"];
				string errorCode = resCol["ERRMSG"];
				if (returnCode == "SUCCEEDED") {
					retStatus = ProcessTransactionStatus.Approved;
				} else if (returnCode == "PENDING") {
					retStatus = ProcessTransactionStatus.Pending;
					/*
					X_3dRedirect = GetURLValue(TxtRet, "REDIRECTURL")
					if (X_3dRedirect <> "") {
						 X_3dRedirect = Session("ProcessURL") & "remoteCharge_Back.asp"
						returnCode = "553"
						sIsSendUserConfirmationEmail = False
						'X_3dRedirect = URLDecode(X_3dRedirect) & "&PaReq=" & Request("REDIRECTSECRET")
					}
					*/ 
				} else {
					retStatus = ProcessTransactionStatus.Declined;
				}
			} else {
				retStatus = CommonTypes.ProcessTransactionStatus.Declined;
			}
			transaction.Response = new Netpay.Process.TransactionResponse(retStatus, returnCode, approvalNumber);
			return true;
		}

		public void RemoteInvoke(System.Net.HttpListenerContext context)
		{
			System.IO.StreamWriter Response = new System.IO.StreamWriter(context.Response.OutputStream);
			Response.Write("Out put from debitor");
			Response.Close();
		}
	}
}
