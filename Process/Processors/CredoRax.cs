﻿using System;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Processors
{
	public class CredoRax : Modules.IProcessor
	{
		public CommonTypes.ProcessTransactionType[] SupportedTypes
		{
			get
			{
				return new ProcessTransactionType[] { 
					ProcessTransactionType.Sale, 
					ProcessTransactionType.Authorize, 
					ProcessTransactionType.Capture, 
					ProcessTransactionType.Refund, 
				};
			}
		}

		private void addParam(System.Text.StringBuilder sb, System.Text.StringBuilder sbSign, string prName, string prValue)
		{
			sb.Append("&" + prName + "=" + System.Web.HttpUtility.UrlEncode(prValue));
			sbSign.Append(prValue); //Replace(Server.URLEncode(prValue), "+", " ")
		}

		private int mapCardType(PaymentMethodEnum pmType)
		{
			switch(pmType){
			case PaymentMethodEnum.CCVisa: return 1;
			case PaymentMethodEnum.CCDiners: return 4;
			case PaymentMethodEnum.CCAmex: return 3;
			case PaymentMethodEnum.CCMastercard: return 2;
			case PaymentMethodEnum.CCMaestro: return 7;
			//case PaymentMethod.CCJCB: return 5;
			default: return 0;
			}
		}

		private int mapOperation(ProcessTransactionType transType)
		{
			switch(transType){
			case ProcessTransactionType.Refund: return 5;		//refnud
			case ProcessTransactionType.Authorize: return 2;	//authorization only
			case ProcessTransactionType.Capture: return 3;		//capture
			case ProcessTransactionType.Sale: return 1;			//sale
			default: return 0;									//not supported
			}
		}

		private string formatPhone(string phone)
		{
			 return phone.Trim().Replace(")", " ").Replace("(", " ").Replace("+", " ");
		}

		public bool Process(TransactionContext transaction)
		{
			bool IsNewRequest = true;
			StringBuilder sb = new StringBuilder();
			StringBuilder sbSign = new StringBuilder();
            string UrlAddress = "https://mla-base2.credorax.com/crax_gate/test3D";
            if ((transaction.Config.Options & ProcessOptions.IsTestTerminal) != 0) UrlAddress = "https://base2.credorax.com/crax_gate/test3D";
			if (transaction.Request.type == ProcessTransactionType.Capture || transaction.Request.type == ProcessTransactionType.Refund)
			{
				if (string.IsNullOrEmpty(transaction.Request.debitorRefCode)) throw new TransactionException(ProcessTransactionStatus.Declined, 532, null); //Reftrans not Found
				if (transaction.Response.ApprovalNumber.IndexOf('#') < 0) throw new TransactionException(ProcessTransactionStatus.Declined, 532, null); //Reftrans not Found
				IsNewRequest = false;
			}
			//transaction.Request.debitorRefCode = GetTransRefCode();

			addParam(sb, sbSign, "M", transaction.Config.AccountName);
			addParam(sb, sbSign, "O", mapOperation(transaction.Request.type).ToString());
			addParam(sb, sbSign, "V", "413");
			addParam(sb, sbSign, "a1", transaction.Request.debitorRefCode);
			addParam(sb, sbSign, "a4", (transaction.Request.amount * 100).ToString());
			if (IsNewRequest) addParam(sb, sbSign, "a5", transaction.Request.currencyISO);
			addParam(sb, sbSign, "a6", DateTime.Now.ToString("yymmdd"));
			addParam(sb, sbSign, "a7", DateTime.Now.ToString("HHMMss"));
			//addParam(sb, sbSign, "a8", CurX_DebitReferenceCode);
			if (IsNewRequest) {
				 addParam(sb, sbSign, "b1", transaction.Request.creditCard.Pan);
				 addParam(sb, sbSign, "b2", mapCardType(transaction.Request.paymentMethod).ToString());
				 addParam(sb, sbSign, "b3", transaction.Request.creditCard.ExpMonth.ToString("00"));
				 addParam(sb, sbSign, "b4", transaction.Request.creditCard.ExpYear.ToString("00"));
				 addParam(sb, sbSign, "b5", transaction.Request.creditCard.Cvv);
	    
				 addParam(sb, sbSign, "c1", transaction.Request.creditCard.OwnerName);
				 addParam(sb, sbSign, "c10", transaction.Request.billingAddress.Postal);
				 addParam(sb, sbSign, "c2", formatPhone(transaction.Request.customer.phone));
				 addParam(sb, sbSign, "c3", transaction.Request.customer.email);
				 //addParam(sb, sbSign, "c4", BACHAddr1NO));
				 addParam(sb, sbSign, "c5", transaction.Request.billingAddress.Address2);
				 addParam(sb, sbSign, "c6", transaction.Request.billingAddress.Address1);
				 addParam(sb, sbSign, "c7", transaction.Request.billingAddress.City);
				 addParam(sb, sbSign, "c8", transaction.Request.billingAddress.StateISO);
				 addParam(sb, sbSign, "c9", transaction.Request.billingAddress.CountryISO);
			}
			if (transaction.IPAddress != null) addParam(sb, sbSign, "d1", transaction.IPAddress.ToString());
			addParam(sb, sbSign, "d2", transaction.Request.debitorRefCode);
			//if (IsNewRequest) addParam(sb, sbSign, "d4", Session.SessionID));
			//ParamList = addParam(sb, sbSign, "d5", Request.ServerVariables("HTTP_USER_AGENT"));
			//ParamList = addParam(sb, sbSign, "d6", Request.ServerVariables("HTTP_ACCEPT_LANGUAGE"));
			if (transaction.Request.type != ProcessTransactionType.Refund) {
				 addParam(sb, sbSign, "e1", transaction.Request.creditCard.OwnerName);
				 addParam(sb, sbSign, "e10", transaction.Request.billingAddress.Postal);
				 addParam(sb, sbSign, "e2", formatPhone(transaction.Request.customer.phone));
				 addParam(sb, sbSign, "e3", transaction.Request.customer.email);
				 //addParam(sb, sbSign, "e4", BACHAddr1NO));
				 addParam(sb, sbSign, "e5", transaction.Request.billingAddress.Address1);
				 addParam(sb, sbSign, "e6", transaction.Request.billingAddress.Address2);
				 addParam(sb, sbSign, "e7", transaction.Request.billingAddress.City);
				 addParam(sb, sbSign, "e8", transaction.Request.billingAddress.StateISO);
				 addParam(sb, sbSign, "e9", transaction.Request.billingAddress.CountryISO);
			}
			
			if (transaction.Request.type == ProcessTransactionType.Capture || (transaction.Request.type == ProcessTransactionType.Refund)) {
				 string[] X_ConfirmNumberValues = transaction.Response.ApprovalNumber.Split('#');
				 addParam(sb, sbSign, "g2", X_ConfirmNumberValues[1]);
				 addParam(sb, sbSign, "g3", X_ConfirmNumberValues[0]);
				 addParam(sb, sbSign, "g4", transaction.Request.debitorRefCode);
			}

			string strOut = null, returnCode = null, approvalNumber = null;
			CommonTypes.ProcessTransactionStatus retStatus = CommonTypes.ProcessTransactionStatus.Unknown;
			System.Net.HttpStatusCode res = WebHelper.HttpRequest(UrlAddress, sb.ToString(), out strOut, null);
			if (res == System.Net.HttpStatusCode.OK)
			{
				var resCol = System.Web.HttpUtility.ParseQueryString(strOut);
				returnCode = resCol["z2"];
				approvalNumber = resCol["z4"] + "#" + resCol["z1"];
				string errorCode = resCol["z3"];
				//HttpError = FormatHttpRequestError(HttpReq)
				//sDebitRequest = Replace(sDebitRequest, X_ccNumber, GetSafePartialNumber(X_ccNumber))
				//sDebitRequest = Replace(sDebitRequest, "CVV2/PIN=" & X_ccCVV2, "CVV2/PIN=" & GetSafePartialNumber(X_ccCVV2))
				//SaveLogChargeAttemptRequestResponse sDebitRequest, sResDetails
				//if (returnCode == "001") returnCode = "002"; //001 is taken by netpay
				if (returnCode == "0") retStatus = CommonTypes.ProcessTransactionStatus.Approved;
				else retStatus = CommonTypes.ProcessTransactionStatus.Declined;
			}
			else
			{
				retStatus = CommonTypes.ProcessTransactionStatus.Declined;
			}
			transaction.Response = new Netpay.Process.TransactionResponse(retStatus, returnCode, approvalNumber);
			return true;
		}

	}
}
