﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Netpay.Process
{
	public class DataQueue<T> : IDisposable
	{
		public delegate void ProcessItemProc(T item);
		public ProcessItemProc ProcessItem;
		private Thread Thread = null;
		private AutoResetEvent NewItem = null;
		private ManualResetEvent ExitThread = null;
		public System.Collections.Generic.Queue<T> Queue { get; private set; }
		public DataQueue(ProcessItemProc processItem) 
		{
			Queue = new Queue<T>();
			ProcessItem = processItem;
			NewItem = new System.Threading.AutoResetEvent(false);
			ExitThread = new System.Threading.ManualResetEvent(false);
		}

		void IDisposable.Dispose()
		{
			if (Thread != null && Thread.IsAlive) Stop();
			NewItem.Close();
			ExitThread.Close();
		}
		
		public void Start()
		{
			if (Thread != null && Thread.IsAlive) throw new Exception("can't start while thread already running");
			ExitThread.Reset();
			Thread = new System.Threading.Thread(ThreadProc);
			Thread.Start();
		}

		public void Stop()
		{
			if (Thread == null || !Thread.IsAlive) throw new Exception("can't stop while thread not running");
			ExitThread.Set();
			Thread.Join();
		}

		public virtual void AddItem(T item) { lock (Queue) Queue.Enqueue(item); NewItem.Set(); }
		public int Count { get{ lock(Queue) return Queue.Count; } }

		protected virtual void ThreadProc() {
			WaitHandle[] waitHandles = new System.Threading.WaitHandle[] { ExitThread, NewItem };
			while(WaitHandle.WaitAny(waitHandles) != 0){
				T value;
				lock(Queue) value = Queue.Dequeue();
				ProcessItem(value);
			}
		}

	}
}
