﻿using System;
using System.Data.OleDb;

namespace Netpay.Process
{
	public class DataHelper
	{
		public enum FieldOptions{
			PrimaryKey	= 1,
			Unique		= 2,
			Identity	= 4,
			Nullable		= 8,
		}

		public enum ForeignKeyOption
		{
			No_Action,
			Cascade,
			Set_Null,
		}
	
		public enum DatabaseType{
			SQLServer,
			Access,
			//MySql,
		}

		private DatabaseType _DataType;
		public string ConnectionString { get; set; }
		public System.Collections.Generic.Dictionary<Type, string> DataTypeMap { get; set; }
		public DatabaseType DataType { 
			get { return _DataType; }
			set {
				switch (value) {
				case DatabaseType.SQLServer:
					DataTypeMap.Clear();
					DataTypeMap.Add(typeof(int), "Int");
					DataTypeMap.Add(typeof(byte), "TinyInt");
					DataTypeMap.Add(typeof(DateTime), "DateTime");
					DataTypeMap.Add(typeof(bool), "Bit");
					DataTypeMap.Add(typeof(string), "NVarChar");
					DataTypeMap.Add(typeof(decimal), "Money");
					DataTypeMap.Add(typeof(Guid), "Guid");
					_DataType = value;
					break;
				case DatabaseType.Access:
					DataTypeMap.Clear();
					DataTypeMap.Add(typeof(int), "Int");
					DataTypeMap.Add(typeof(byte), "Byte");
					DataTypeMap.Add(typeof(DateTime), "Date");
					DataTypeMap.Add(typeof(bool), "YesNo");
					DataTypeMap.Add(typeof(string), "NVarChar");
					DataTypeMap.Add(typeof(decimal), "Money");
					DataTypeMap.Add(typeof(Guid), "Guid");
					_DataType = value;
					break;
				}
			}
		}

		public DataHelper()
		{
			DataTypeMap = new System.Collections.Generic.Dictionary<Type,string>();
			DataType = DatabaseType.SQLServer;
		}

		public long ExecSql(string sql) 
		{
			var dbCon = new OleDbConnection(ConnectionString);
			try{
				dbCon.Open();
				return ExecSql(dbCon, sql);
			}finally{
				if (dbCon.State != System.Data.ConnectionState.Closed) dbCon.Close();
				dbCon.Dispose();
			}
		}

		public long ExecSql(OleDbConnection con, string sql) 
		{
			var dbCom = new OleDbCommand(sql, con);
			try {
				return dbCom.ExecuteNonQuery();
			} finally {
				dbCom.Dispose();
			}
		}

		public long ExecCommand(OleDbCommand command)
		{
			var dbCon = new OleDbConnection(ConnectionString);
			try
			{
				command.Connection = dbCon;
				dbCon.Open();
				return ExecCommand(dbCon, command);
			}
			finally
			{
				if (dbCon.State != System.Data.ConnectionState.Closed) dbCon.Close();
				dbCon.Dispose();
			}
		}

		public long ExecCommand(OleDbConnection con, OleDbCommand command)
		{
			try
			{
				return command.ExecuteNonQuery();
			}
			finally
			{
				command.Dispose();
			}
		}


		public System.Data.IDataReader ExecReader(string sqlStr)
		{
			var dbCon = new OleDbConnection(ConnectionString);
			try{
				dbCon.Open();
				return ExecReader(dbCon, sqlStr);
			}catch{
				if (dbCon.State != System.Data.ConnectionState.Closed) dbCon.Close();
				dbCon.Dispose();
				throw;
			}
		}

		public System.Data.IDataReader ExecReader(OleDbConnection con, string sqlStr)
		{
			OleDbCommand dbCom = new OleDbCommand(sqlStr, con);
			try {
				return dbCom.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
			} finally {
				dbCom.Dispose();
			}
		}

		public object ExecScalar(string sqlStr)
		{
			var dbCon = new OleDbConnection(ConnectionString);
			try
			{
				dbCon.Open();
				return ExecScalar(dbCon, sqlStr);
			}
			finally
			{
				if (dbCon.State != System.Data.ConnectionState.Closed) dbCon.Close();
				dbCon.Dispose();
			}
		}

		public object ExecScalar(OleDbConnection con, string sqlStr)
		{
			OleDbCommand dbCom = new OleDbCommand(sqlStr, con);
			try
			{
				return dbCom.ExecuteScalar();
			}
			finally
			{
				dbCom.Dispose();
			}
		}

		public void addParameter(System.Data.IDbCommand command, string name, object value) { command.Parameters.Add(new OleDbParameter(name, (value == null ? System.DBNull.Value : value))); }
		public void addParameter(System.Data.IDbCommand command, string name, string value, int maxLen) { command.Parameters.Add(new OleDbParameter(name, (value == null ? System.DBNull.Value : (object)value))); }

		public string formatValue(int value) { return value.ToString(); }
		public string formatValue(int? value) { if(value == null) return "Null"; return value.ToString(); }
		public string formatValue(decimal value) { return value.ToString(); }
		public string formatValue(Guid value) { return "{" + value.ToString() + "}"; }
		public string formatValue(Guid? value) { if (value == null) return "Null"; return "{" + value.ToString() + "}"; }
		public string formatValue(bool value) { return value ? "1" : "0"; }
		public string formatValue(string value, int maxLen) {
			if (value == null) return "Null";
			if (value.Length > maxLen) value = value.Substring(0, maxLen);
			return "'" + value.Replace("'", "''") + "'"; 
		}
		public string formatValue(DateTime value) { return "'" + value.ToString("yyy-MM-dd HH:mm:ss") + "'"; }

		private string formatFieldOptions(FieldOptions fieldOptions) {
			string result = "";
			if ((fieldOptions & FieldOptions.Identity) != 0) result += "Identity ";
			if ((fieldOptions & FieldOptions.Nullable) == 0) result += "Not Null ";
			if (((fieldOptions & FieldOptions.Unique) != 0) && (_DataType != DatabaseType.SQLServer || (fieldOptions & FieldOptions.PrimaryKey) == 0)) result += "Unique ";
			if ((fieldOptions & FieldOptions.PrimaryKey) != 0) result += "Primary Key ";
			return result.Trim();
		}
		public string formatCreateField(string name, FieldOptions fieldOptions, int maxLen) 
		{
			return name + " " + DataTypeMap[typeof(String)] + "(" + maxLen + ") " + formatFieldOptions(fieldOptions); 
		}
		public string formatCreateField(string name, FieldOptions fieldOptions, Type t) 
		{
			return name + " " + DataTypeMap[t] + " " + formatFieldOptions(fieldOptions); 
		}

		public string formatForeignKey(string tblName1, string tblFieldName1, string tblName2, string tblFieldName2, ForeignKeyOption fko) 
		{
			return "Alter Table " + tblName1 + " Add Constraint FK_" + tblName1 + "_" + tblFieldName1 + " FOREIGN KEY (" + tblFieldName1 + ") REFERENCES " + tblName2 + "(" + tblFieldName2 + ") On Delete " + fko.ToString().Replace("_", " ");
		}

		public int getNextID(string tblName, string idRowName)
		{
			if (DataType == DataHelper.DatabaseType.SQLServer) {
				return Validation.TestVar(ExecScalar(
					string.Format("SelectIdentityValue {0}, {1}, {2}", 
					formatValue(tblName, 100), 
					formatValue(Application.MinID), 
					formatValue(Application.MaxID))), Application.MinID, Application.MaxID, Application.MinID);
			} else {
				return 1 + Validation.TestVar(ExecScalar(
					string.Format("Select Max({1}) From {0} Where {1} > " + formatValue(Application.MinID) + " And {1} < " + formatValue(Application.MaxID), tblName, idRowName)), 
					Application.MinID, Application.MaxID, Application.MinID);
			}
		}

	}

	public static class Validation
	{
		public static int TestVar(object val, int nMin, int nMax, int nDefault)
		{
			int pRet;
			if (val == null || val == DBNull.Value) return nDefault;
			else if (val is int) return pRet = (int)val;
			if (!int.TryParse(val.ToString(), out pRet)) pRet = nDefault;
			if (nMin < nMax && (pRet < nMin || pRet > nMax)) pRet = nDefault;
			return pRet;
		}

		public static decimal TestVar(object val, decimal nMin, decimal nMax, decimal nDefault)
		{
			decimal pRet;
			if (val == null || val == DBNull.Value) return nDefault;
			else if (val is int) return pRet = (decimal)val;
			if (!decimal.TryParse(val.ToString(), out pRet)) pRet = nDefault;
			if (nMin < nMax && (pRet < nMin || pRet > nMax)) pRet = nDefault;
			return pRet;
		}

		public static DateTime TestVar(object val, DateTime nMin, DateTime nMax, DateTime nDefault)
		{
			DateTime pRet;
			if (val == null || val == DBNull.Value) return nDefault;
			else if (val is DateTime) return pRet = (DateTime)val;
			if (!DateTime.TryParse(val.ToString(), out pRet)) pRet = nDefault;
			if (nMin < nMax && (pRet < nMin || pRet > nMax)) pRet = nDefault;
			return pRet;
		}

		public static string TestVar(object val, int nMax, string nDefault)
		{
			string pRet;
			if (val == null) return nDefault;
			else if (val is string) return pRet = (string)val;
			pRet = val.ToString();
			if (nMax > 0 && (pRet.Length > nMax)) pRet = pRet.Substring(0, nMax);
			return pRet;
		}

		public static bool TestVar(object val, bool nDefault)
		{
			bool pRet;
			if (val == null) return nDefault;
			else if (val is bool) return pRet = (bool)val;
			if (!bool.TryParse(val.ToString(), out pRet)) pRet = nDefault;
			return pRet;
		}

		public static Guid TestVar(object val, Guid nDefault)
		{
			Guid pRet;
			if (val == null) return nDefault;
			else if (val is Guid) return pRet = (Guid)val;
			try{ pRet = new Guid(val.ToString()); }
			catch{ pRet = nDefault; }
			return pRet;
		}

	}

	public static class WebHelper 
	{
		public static System.Net.HttpStatusCode HttpRequest(string address, string postData, out string outParams, System.Text.Encoding encoding)
		{
			if (encoding == null) encoding = System.Text.Encoding.GetEncoding(1252); //Windows default Code Page
			System.Net.HttpWebResponse webResponse;
			System.Net.WebRequest webRequest = System.Net.WebRequest.Create(address);
			if (!string.IsNullOrEmpty(postData)){
				webRequest.Method = "POST";
				webRequest.ContentType = "application/x-www-form-urlencoded";
				webRequest.ContentLength = postData.Length;
				System.IO.StreamWriter sw = new System.IO.StreamWriter(webRequest.GetRequestStream(), encoding);
				sw.Write(postData);
				sw.Close();
				sw.Dispose();
			}
			webResponse = (System.Net.HttpWebResponse)webRequest.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(webResponse.GetResponseStream(), encoding);
			outParams = sr.ReadToEnd();
            sr.Close();
            sr.Dispose();
            webResponse.Close();
            return webResponse.StatusCode;
		}
	}
}
