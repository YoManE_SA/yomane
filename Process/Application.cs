﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
using Netpay.Process.Modules;

namespace Netpay.Process
{
	public class Collection<T> : List<T> { }

	public static class Application
	{
		private static long _loaded = 0;
		private static Dictionary<string, object> _modules = new Dictionary<string, object>();
		public interface DllLoader { void Load(string configSection); }
		public static ProcessEngine ProcessEngine { get; set; }
		public static IPendingEventManager PendingEventManager { get; private set; }
		public static string DomainHosts { get; set; }
		public static bool IsLoaded { get { return System.Threading.Interlocked.Read(ref _loaded) != 0; } }
		public static IDictionary Modules { get { return _modules; } } 
		public static string InstanceName { get; set; }
		public static int MinID { get; set; }
		public static int MaxID { get; set; }
		public static int TestModeMID { get; set; }

		public static int ValidateLocalID(int id)
		{
			if (id < MinID) return MinID;
			else if(id > MaxID) return 0;
			return id;
		}

        public static string GenerateUniqueRefID() 
        {
            return Guid.NewGuid().ToString("N");
        }

		public static void Init()
		{
			if (IsLoaded) 
				return;

			_modules = new Dictionary<string, object>();
			ProcessEngine = new ProcessEngine();
			Configuration.LoadConfig();
			if (MaxID < 1) MaxID = 0xFFFFFFF;
			LoadPlugins();
			Interlocked.Increment(ref _loaded);
			foreach (DictionaryEntry obj in Modules)
				if (obj.Value is IModule) (obj.Value as IModule).Load();
			ProcessEngine.StartPostTasks();
			ProcessEngine.StartEndPoints();
		}

		private static void LoadPlugins()
		{
			//AppDomain.CurrentDomain.SetShadowCopyPath("plugins");
			//AppDomain.CurrentDomain.ShadowCopyFiles = true;
			//string files[] = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory);
			//foreach (string f in files)
			System.Reflection.Assembly[] asm = System.AppDomain.CurrentDomain.GetAssemblies();
			foreach (System.Reflection.Assembly f in asm)
			{
				try
				{
					DllLoader dle = f.CreateInstance("DllLoader") as DllLoader;
					if (dle != null) 
						dle.Load(null);
				}
				catch { }
			}
		}
	}
}
