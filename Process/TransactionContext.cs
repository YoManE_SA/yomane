﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using Netpay.CommonTypes;

namespace Netpay.Process
{
	public enum ProcessOptions
	{
		IsEmailRequired = 0x0001,
		IsPhoneRequired = 0x0002,
		IsCVVRequired = 0x0004,
		IsPIDRequired = 0x0008,

		IsGateway = 0x0010,
		IsMaster = 0x0020,
		IsManipAmount = 0x0040,
		IsForcePending = 0x0080,

		Enable3d = 0x0100,
		EnableAuth = 0x0200,
		EnableCapture = 0x0400,
		EnableSale = 0x0800,
		EnableRefund = 0x1000,
		EnablePartialRefund = 0x2000,
		EnableReversal = 0x4000,

		IsTestTerminal = 0x8000,
	}

	public class ProcessConfig
	{
		public int MerchantID { get; set; }
		public string MerchantName { get; set; }
		public string MerchantLogoURL { get; set; }
		public string MerchantWebsiteURL { get; set; }
		public string MerchantSupportEmail { get; set; }
		public string MerchantSupportPhone { get; set; }
		public string MerchantNotifyEmail { get; set; }
        public string MerchantNotifyEmailChb { get; set; }
		public string MerchantDepartment { get; set; }
        public string MerchantParentCompanyName { get; set; }

        public Language MerchantLanguage { get; set; }
		public Currency? MerchantFeeCurrency { get; set; }
		public DateTime? TransactionPayDate { get; set; }

		public string Descriptor { get; set; }
		public string Mcc { get; set; }
		public int TerminalID { get; set; }

		public string GatewayBrandName { get; set; }
		public string GatewayLegalName { get; set; }
		public string GatewayLogoPath { get; set; }
		public string GatewaySupportWebSite { get; set; }
		public string GatewaySupportEmail { get; set; }
        public string GatewaySupportPhone { get; set; }
        public string GatewaySupportFax { get; set; }
        public string GatewayComplianceEmail { get; set; }
        public string GatewayCompliancePhone { get; set; }
        public string GatewayComplianceUSPhone { get; set; }
        public string GatewayUIServicesUrl { get; set; }


        public int DebitProcessorId { get; set; }
        public string DebitProcessor { get; set; }
		public string TerminalNumber { get; set; }

		public string UserName { get; set; }
		public string AccountName { get; set; }
		public string Password { get; set; }

		public string UserName3D { get; set; }
		public string AccountName3D { get; set; }
		public string Password3D { get; set; }

		public ProcessOptions Options { get; set; }
		public string MoreInfo { get; set; }
	}

    public class ChargebackInfo 
    {
        public DateTime chargebackDate;
        public DateTime retrivalDate;
        public int? reasonCode;
        public string description;
    }
    
    public class TransactionResponse
	{
		public TransactionResponse() { TimeStamp = DateTime.Now; }
		public TransactionResponse(ProcessTransactionStatus status, string errorCode, string reason)
		{
			ID = 0; Status = status; TimeStamp = DateTime.Now; ErrorCode = errorCode; Reason = reason; ApprovalNumber = null; 
		}
		public TransactionResponse(string apprivalNumber) { ApprovalNumber = apprivalNumber; Status = ProcessTransactionStatus.Approved; TimeStamp = DateTime.Now; ErrorCode = null; Reason = null; }

		public int ID { get; set; }
		public string ApprovalNumber { get; set; }
		public DateTime TimeStamp { get; set; }
		public ProcessTransactionStatus Status { get; set; }
		public string ErrorCode { get; set; }
		public string Reason { get; set; }
        public ChargebackInfo chargeback { get; set; }

        public string ProcessInfo { get; set; }
        //public string ProductPostText { get; set; }
		//public string ProductPostLink { get; set; }
	}

	public class TransactionException : Exception
	{
		public TransactionException(ProcessTransactionStatus status, string errorCode, string reason) { Reply = new TransactionResponse(status, errorCode, reason); }
		public TransactionException(ProcessTransactionStatus status, int errorCode, string reason) { Reply = new TransactionResponse(status, errorCode.ToString(), reason); }
		//public TransactionException(string apprivalNumber) { new TransactionReply(apprivalNumber); }

		public TransactionResponse Reply { get; set; }
		public override string Message { get { return Reply.Reason; } }
	}

	public class CreditCardData
	{
		[System.Xml.Serialization.XmlIgnore()]
		public int ID { get; set; }
		public string Alias { get; set; }
		public string Pan { get; set; }
		public string Cvv { get; set; }
		public int ExpMonth { get; set; }
		public int ExpYear { get; set; }
		public string Track1 { get; set; }
		public string Track2 { get; set; }
		public string OwnerName { get; set; }
		public string PersonalID { get; set; }
	}

	public class TransferData
	{
		[System.Xml.Serialization.XmlIgnore()]
		public int ID { get; set; }
		public string AccountNumber { get; set; }
		public bool SavingAccount { get; set; }
		public string BankName { get; set; }
		public string Iban { get; set; }
		public string Aba { get; set; }
	}

	public class RecurringSeries
	{
		public int Charges { get; set; }
		public int IntervalCount { get; set; }
		public char IntervalUnit { get; set; }
		public decimal Amount { get; set; }
	}

	public class LoginInfo
	{
		public string MerchantID { get; set; }
		public string Password { get; set; }
	}

	public class RiskData
	{
		public string CountryCodes { get; set; }
		public string BinCodes { get; set; }
	}

	public class Address
	{
		[System.Xml.Serialization.XmlIgnore()]
		public int ID { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string Postal { get; set; }
		public string CountryISO { get; set; }
		public string StateISO { get; set; }
	}

	public class CustomerData
	{
		[System.Xml.Serialization.XmlIgnore()]
		public int ID { get; set; }
		public string firstName { get; set; }
		public string lastName { get; set; }
        public string fullName { get { return firstName + " " + lastName; } }

		public string personalID { get; set; }
		public string phone { get; set; }
		public string email { get; set; }
	}

	public class TransactionRequest
	{
		public LoginInfo loginInfo;
		public string languageISO;
		public decimal amount;
		public decimal changedAmount;
		public string currencyISO;
		public int? refTransID;
		public byte installmets;
		public string ivrAuthCode;
		public ProcessTransactionType type;
		public ProcessTransactionSource source;
		public string merchantRefCode;
		public string debitorRefCode;
		public string terminalCode;
		public string descriptor;
		public RiskData riskData;
		public PaymentMethodEnum paymentMethod;
		public CreditCardData creditCard;
		public TransferData transferData;
		public CustomerData customer;
		public Address billingAddress;
		public Address shippingAddress;
		public string comment;
		public string clientIP;
		public string referringUrl;
        public int? productID { get; set; }
        public string PayerIDUsed { get; set; }
		//[System.Xml.Serialization.XmlAnyElement()]
		public RecurringSeries[] recurring;
	}

	public class DetectMethodData
	{
		public string domain;	 /*in*/
		public string account;	 /*in*/
		public PaymentMethodType paymentMethodType;
		public PaymentMethodEnum paymentMethod; /*in, out*/
		public string binCountry; /*out*/
		public string binValue;   /*out*/
	}

	public class TransLoadInfo
	{
		public TransLoadInfo() { }
		public TransLoadInfo(string Domian, int ID, ProcessTransactionStatus ts, ProcessTransactionType ptp)
		{
			domain = Domian; id = ID; status = ts; type = ptp;
		}

		public string domain;
		public int id;
		public ProcessTransactionStatus status;
		public ProcessTransactionType type;
	}

	public class PendingEvent
	{
		public int ID { get; set; }
		public byte Retries { get; set; }

		public ProcessTransactionType TransType { get; set; }
		public ProcessTransactionStatus TransStatus { get; set; }
		public int? TransID { get; set; }

		public int? MerchantID { get; set; }
		public int? CustomerID { get; set; }

		public PendingEventType EventType { get; set; }
		public string Parameters { get; set; }
	}

    public class TransactionAmount 
    {
        public TransactionAmountType eventType;
        public decimal? amount;
        public decimal? settleAmount;
        public DateTime? settleDate;

        public TransactionAmount(TransactionAmountType eventType, decimal? amount, decimal? settleAmount, DateTime? settleDate) 
        {
            this.eventType = eventType;
            this.amount = amount;
            this.settleAmount = settleAmount;
            this.settleDate = settleDate;
        }
    }

	public class TransactionContext
	{
        private string _methodText { get; set; }
        private System.Text.StringBuilder _replyMessage = new StringBuilder();
        public string Today { get { return DateTime.Now.ToShortDateString(); } }
		public int ID { get; set; }
		//public bool IsTest { get; set; }
		public int CurrencyID { get; set; }
		public string Domain { get; set; }
        public IPAddress IPAddress { get; set; }
		public DetectMethodData Method { get; set; }
		public ProcessConfig Config { get; set; }
		public TransactionRequest Request { get; set; }
		public TransactionResponse Response { get; set; }
		public TransactionStage Stage { get; set; }
		public Dictionary<string, object> Values { get; set; }
        public string CartTemplate { get; set; }

		public string FormatedAmount { get { return Request.amount.ToString("0.00") + " " + Request.currencyISO; } }
		public string MethodText
		{
			get
			{
                if (_methodText != null) return _methodText;
                string ret = null;
				if (Method.paymentMethod >= PaymentMethodEnum.CC_MIN && Method.paymentMethod <= PaymentMethodEnum.CC_MAX)
				{
                    ret = Method.paymentMethod.ToString().Substring(2);
                    if (Request.creditCard != null && Request.creditCard.Pan != null)
                        ret += "..." + Request.creditCard.Pan.Substring(Request.creditCard.Pan.Length - 4);
				}
				else if (Method.paymentMethod >= PaymentMethodEnum.EC_MIN && Method.paymentMethod <= PaymentMethodEnum.EC_MAX)
				{
                    ret = Method.paymentMethod.ToString().Substring(2);
                    if (Request.transferData != null && Request.transferData.AccountNumber != null) 
                        ret += "..." + Request.transferData.AccountNumber;
				}
				return ret;
			}
            set { _methodText = value; }
		}

		public TransactionContext(string domain, TransactionRequest request)
		{
			Method = new DetectMethodData();
			Method.domain = this.Domain = domain;

			if ((Request = request) != null)
			{
				Method.paymentMethod = request.paymentMethod;
				if (request.creditCard != null) Method.account = request.creditCard.Pan;
				else if (request.transferData != null) Method.account = request.transferData.AccountNumber;
			}
		}

		public void LogReplyMessage(string objName, string message)
		{
			_replyMessage.AppendFormat("{0}-{1}\r\n", objName, message);
		}

		//public void RegisterPending(EventTypes eventID, string parameters){  }

	}
}
