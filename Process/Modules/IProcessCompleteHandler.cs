﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Process.Modules
{
	public interface IProcessCompleteHandler
	{
		void ProcessComplete(TransactionContext transaction);
	}
}
