﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Modules
{
	public class CommonValidator : IValidator
	{
		public bool ValidateTransaction(TransactionContext transaction)
		{
			if (transaction.Request.amount < 0.01m) throw new TransactionException(ProcessTransactionStatus.Declined, 505, null);
			//ValidateAddress("", true);
			//If Len(Trim(X_Amount)) > 9 Then ThrowError "505"
			//If Len(X_Comment) > 255 Then Call throwError("508")
			return true;
		}

		private bool ValidateAddress(Address address, bool required)
		{
			if (!string.IsNullOrEmpty(address.Address1) && address.Address1.Length > 100)
				throw new TransactionException(ProcessTransactionStatus.Declined, 540, null);

			if (!string.IsNullOrEmpty(address.City))
				throw new TransactionException(ProcessTransactionStatus.Declined, 541, null);

			if (string.IsNullOrEmpty(address.Postal))
				throw new TransactionException(ProcessTransactionStatus.Declined, 542, null);

			if (required) {
				if (!string.IsNullOrEmpty(address.Address2) && address.Address2.Length > 100)
					throw new TransactionException(ProcessTransactionStatus.Declined, 540, null);

				if (string.IsNullOrEmpty(address.StateISO))
					throw new TransactionException(ProcessTransactionStatus.Declined, 542, null);

				if (string.IsNullOrEmpty(address.CountryISO))
					throw new TransactionException(ProcessTransactionStatus.Declined, 544, null);
			}

			return true;
		}

	}
}
