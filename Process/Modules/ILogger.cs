﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Process.Modules
{
	public interface ILogger
	{
		void LogError(CommonTypes.TransactionStage stage, Exception ex);
		void LogTransaction(bool preStage, TransactionContext trans, Exception ex);
	}
}
