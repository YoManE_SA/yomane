﻿using System;
using System.Collections.Generic;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Modules
{
	[Flags]
	public enum SaveOptions
	{
		None = 0,
		UpdateExisting = 1,
		GenIds = 2
	}

	public interface IPersistor
	{
		void Save(TransactionContext transaction, SaveOptions options);
		TransactionContext Load(TransLoadInfo transLoadInfo);
		void AddAmount(TransactionContext transaction, SaveOptions options, TransactionAmount transactionAmount);
		void AddLog(TransactionContext transaction, SaveOptions options, TransactionHistoryType historyType, string description, int? refNumber, string refUrl, bool? isSucceeded);
	}
}
