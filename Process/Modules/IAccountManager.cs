﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Process.Modules
{
	public interface IAccountManager
	{
		bool GetConfig(TransactionContext transaction, out ProcessConfig processConfig);
		void SaveStoredMethod(string alias, CreditCardData creditcard);
		CreditCardData LoadStoredMethod(string alias);
	}
}
