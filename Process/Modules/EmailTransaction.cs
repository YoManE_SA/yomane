using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Netpay.CommonTypes;

namespace Netpay.Process.Modules
{
	/// <summary>
	/// handling email sends for transactions
	/// </summary>
	public class EmailTransaction : IPostProcessor
	{
		public enum EmailType
		{
			CustomerNotify_ApprovedTrans,
			MerchantNotify_ApprovedTrans,
            MerchantNotify_DeclinedTrans,
            PartnerNotify_ApprovedTrans,
			MerchantNotify_RiskMultipleCardsOnEmail,
            MerchantNotify_Denied,
            MerchantNotify_Clarify,
        }

		public string SmtpServer { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string EmailTemplatesPath { get; set; }
		public string Bcc { get; set; }
		public bool PreviewFile { get; set; }
		public bool Disabled { get; set; }

		public EmailTransaction() { }

		/// <summary>
		/// get email message file name
		/// </summary>
		public string GetFileName(EmailType emailType, Language mailLanguage, string parentcompany)
		{
			string ret;
			if (!string.IsNullOrEmpty(parentcompany)) {
				ret = EmailTemplatesPath + @"\" + parentcompany + "\\" + emailType.ToString() + (mailLanguage == Language.Hebrew ? "_he" : string.Empty) + ".htm";
				if (System.IO.File.Exists(ret)) return ret;
			}
			ret = EmailTemplatesPath + @"\" + emailType.ToString() + (mailLanguage == Language.Hebrew ? "_he" : string.Empty) + ".htm";
			return ret;
		}

		private void SaveEmailFile(string filePath, string message)
		{
			TextWriter txw = new StreamWriter(filePath);
			txw.Write(message);
			txw.Close();
			txw.Dispose();
		}

        public string getEmailText(ProcessEngine processEngine, string domain, int transID, EmailType emailType, Language? mailLanguage) 
        {
            TransactionContext transaction = processEngine.Load(new TransLoadInfo(domain, transID, ProcessTransactionStatus.Approved, ProcessTransactionType.Sale));
            if (transaction == null) throw new ApplicationException("Could not load transaction #" + transID);
			if (mailLanguage == null) mailLanguage = transaction.Config.MerchantLanguage;
			string subject, body = GetMessageText(GetFileName(emailType, mailLanguage.GetValueOrDefault(0), transaction.Config.MerchantParentCompanyName), out subject);
            System.Globalization.CultureInfo cultureInfo = mailLanguage == Language.Hebrew ? System.Globalization.CultureInfo.GetCultureInfo("he-il") : System.Globalization.CultureInfo.GetCultureInfo("en-us");
            return FormatMessage(body, transaction, cultureInfo);
        }

        private string GetMessageText(string fileName, out string subject)
		{
			string fileText = File.ReadAllText(fileName);
			subject = null;
			int subjectStart = fileText.IndexOf("<title>");
			if (subjectStart > -1)
			{
				subjectStart += "<title>".Length;
				int subjectEnd = fileText.IndexOf("</title>", subjectStart);
				subject = fileText.Substring(subjectStart, subjectEnd - subjectStart);
			}
			return fileText;
		}

		/// <summary>
		/// interface IPostProcess method implementation
		/// </summary>
		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
			TransactionHistoryType eventType = 0;
			string subject = null, body = null, toEmail = null;
			string fromEmail = transaction.Config.GatewaySupportEmail;
			string fromName = transaction.Config.GatewayBrandName;
            System.Collections.Specialized.NameValueCollection pendingParams = System.Web.HttpUtility.ParseQueryString(pendingData.Parameters == null ? "" : pendingData.Parameters);
            Language language = Language.English;
			try
			{
				switch (pendingData.EventType)
				{
					case PendingEventType.InfoEmailSendClient:
						eventType = TransactionHistoryType.InfoEmailSendClient;
						message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                        toEmail = transaction.Request.customer.email.Replace(';', ',').Replace(" ", "");
						if (transaction.Method != null && transaction.Method.binCountry != null) language = (transaction.Method.binCountry.ToUpper() == "IL" ? Language.Hebrew : Language.English);
						body = GetMessageText(GetFileName(EmailType.CustomerNotify_ApprovedTrans, language, transaction.Config.MerchantParentCompanyName), out subject);
						break;
					case PendingEventType.InfoEmailSendMerchant:
						eventType = TransactionHistoryType.InfoEmailSendMerchant;
						message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                        toEmail = transaction.Config.MerchantNotifyEmail.Replace(';', ',').Replace(" ", "");
						language = transaction.Config.MerchantLanguage;
						body = GetMessageText(GetFileName((transaction.Response.Status == ProcessTransactionStatus.Declined ? EmailType.MerchantNotify_DeclinedTrans : EmailType.MerchantNotify_ApprovedTrans), language, transaction.Config.MerchantParentCompanyName), out subject);
						break;
					case PendingEventType.InfoEmailSendAffiliate:
						eventType = TransactionHistoryType.InfoEmailSendAffiliate;
						message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                        toEmail = "udi@netpay.co.il";
						body = GetMessageText(GetFileName(EmailType.PartnerNotify_ApprovedTrans, language, transaction.Config.MerchantParentCompanyName), out subject);
						break;
					case PendingEventType.InfoEmailSendRiskMultipleCardsOnEmail:
						eventType = TransactionHistoryType.InfoEmailSendRiskMultipleCardsOnEmail;
						message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                        toEmail = transaction.Config.MerchantSupportEmail.Replace(';', ',').Replace(" ", "");
						body = GetMessageText(GetFileName(EmailType.MerchantNotify_RiskMultipleCardsOnEmail, language, transaction.Config.MerchantParentCompanyName), out subject);
						break;
                    case PendingEventType.EmailPhotoCopy:
                        eventType = TransactionHistoryType.InfoEmailSendPhotoCopy;
                        fromEmail = transaction.Config.GatewayComplianceEmail;
						language = transaction.Config.MerchantLanguage;
                        message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
						body = GetMessageText(GetFileName(EmailType.MerchantNotify_Clarify, language, transaction.Config.MerchantParentCompanyName), out subject);
                        if (string.IsNullOrEmpty(transaction.Config.MerchantNotifyEmailChb)) {
                            toEmail = fromEmail;
                            subject = "mailbox missing: " + subject;
                        } else toEmail = transaction.Config.MerchantNotifyEmailChb.Replace(';', ',').Replace(" ", "");
                        break;
                    case PendingEventType.EmailChargeBack:
                        eventType = TransactionHistoryType.InfoEmailSendChargeBack;
                        fromEmail = transaction.Config.GatewayComplianceEmail;
                        message.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
						language = transaction.Config.MerchantLanguage;
						body = GetMessageText(GetFileName(EmailType.MerchantNotify_Denied, language, transaction.Config.MerchantParentCompanyName), out subject);
                        if (string.IsNullOrEmpty(transaction.Config.MerchantNotifyEmailChb)) {
                            toEmail = fromEmail;
                            subject = "mailbox missing: " + subject;
                        } else toEmail = transaction.Config.MerchantNotifyEmailChb.Replace(';', ',').Replace(" ", "");
                        break;
                    default:
						throw new Exception("EmailTransaction does not support event " + pendingData.EventType.ToString());
				}
                if (!string.IsNullOrEmpty(pendingParams["Email"])) toEmail = pendingParams["Email"];
                try { message.To.Add(toEmail); }
                catch (Exception ex) { processEngine.AddLog(transaction, 0, eventType, null, null, ex.Message, false); return PostProcessResult.FailedNoRetry; }
                message.IsBodyHtml = true;
				if (string.IsNullOrEmpty(subject)) subject = "Transaction Confirmation";
				System.Globalization.CultureInfo cultureInfo = language == Language.Hebrew ? System.Globalization.CultureInfo.GetCultureInfo("he-il") : System.Globalization.CultureInfo.GetCultureInfo("en-us");
				message.Subject = FormatMessage(subject, transaction, cultureInfo);
				message.Body = FormatMessage(body, transaction, cultureInfo);
				if (Bcc != null) message.Bcc.Add(Bcc.Replace(';', ',').Replace(" ", ""));
				System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(SmtpServer);
				if (UserName != null && Password != null) smtpClient.Credentials = new System.Net.NetworkCredential(UserName, Password);
				//if (PreviewFile) SaveEmailFile(AppDomain.CurrentDomain.BaseDirectory + pendingData.EventType.ToString() + ".htm", message.Body);
				smtpClient.Send(message);
				processEngine.AddLog(transaction, 0, eventType, "Sent To: " + message.To.ToString(), null, null, true);
			}
			catch (System.Net.Mail.SmtpFailedRecipientException ex) 
			{
				processEngine.AddLog(transaction, 0, eventType, ex.Message, null, null, false);
				return PostProcessResult.FailedNoRetry;
			} 
			catch (System.Net.Mail.SmtpException ex) 
			{
				processEngine.AddLog(transaction, 0, eventType, ex.Message, null, null, false);
				return PostProcessResult.Failed;
			}
			catch (Exception ex)
			{
				processEngine.AddLog(transaction, 0, eventType, ex.Message, null, null, false);
				throw new Exception(pendingData.EventType.ToString() + ":" + ex.Message, ex);
			}
			return PostProcessResult.Succseeded;
		}

		/// <summary>
		/// get text of nested property
		/// </summary>
		private string GetNestedText(object obj, string methodName, System.Globalization.CultureInfo cultureInfo)
		{
			int start = 0, pos = 0;
			Type varType = null;
			Type objType = null;
			MemberInfo[] method = null;
			BindingFlags bndAttr = System.Reflection.BindingFlags.GetProperty | System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.IgnoreCase;
			while (true)
			{
				objType = obj.GetType();
				pos = methodName.IndexOf('.', start);
				if (pos == -1) pos = methodName.Length;
                string subMethodName = methodName.Substring(start, pos - start);
                method = obj.GetType().GetMember(subMethodName, bndAttr);
                if (method.Length < 1) throw new Exception(string.Format("Unable to call find member {0} on expression {1}", subMethodName, methodName));
                try
                {
                    if (method[0].MemberType == MemberTypes.Property)
                    {
                        PropertyInfo pi = objType.GetProperty(method[0].Name);
                        obj = pi.GetValue(obj, null);
                        varType = pi.PropertyType;
                    }
                    else if (method[0].MemberType == MemberTypes.Field)
                    {
                        FieldInfo fi = objType.GetField(method[0].Name);
                        obj = fi.GetValue(obj);
                        varType = fi.FieldType;
                    }
                    else if (method[0].MemberType == MemberTypes.Method)
                    {
                        MethodInfo mi = objType.GetMethod(method[0].Name, new Type[] { });
                        if (mi == null) throw new Exception(string.Format("Unable to call method {0}, with no params or method not found on expression {1}", subMethodName, methodName));
                        obj = mi.Invoke(obj, null);
                        varType = mi.ReturnType;
                    }
                }catch (Exception ex) {
                    throw new Exception(string.Format("Error in call to {0} / {1} Error: {2}", methodName, subMethodName, ex.Message), ex);
                }
				if (obj == null) return null;
				if (varType != null && varType.IsEnum)
				{
					System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Netpay.Process.Resources.Enums", typeof(EmailTransaction).Assembly);
					obj = rm.GetString(varType.Name + "." + obj.ToString(), cultureInfo);
				}
				start = pos + 1;
				if (start >= methodName.Length) break;
			}
			if (obj == null) return null;
			return obj.ToString().Replace("\r\n", "<br/>");
		}

		/// <summary>
		/// fast replace property expression in string
		/// </summary>
		private string FormatMessage(string source, object obj, System.Globalization.CultureInfo cultureInfo)
		{
			int start = 0, pos = 0, keywordEnd = 0;
			StringBuilder sb = new StringBuilder();
			while ((pos = source.IndexOf('[', start)) > -1)
			{
				if (pos < source.Length && source[pos + 1] == '[')
				{
					start = pos + 2;
					continue;
				}
				sb.Append(source.Substring(start, pos - start));
				keywordEnd = source.IndexOf(']', pos + 1);
				if (keywordEnd == -1) throw new ApplicationException("missing ] before end of the file");
				sb.Append(GetNestedText(obj, source.Substring(pos + 1, keywordEnd - pos - 1), cultureInfo));
				start = keywordEnd + 1;
			}
			sb.Append(source.Substring(start));
			return sb.ToString();
		}
	}
}
