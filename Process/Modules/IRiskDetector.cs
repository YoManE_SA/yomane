﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Process.Modules
{
	public interface IRiskDetector
	{
		bool DetectRisk(TransactionContext transaction); /* return true to continue */
	}
}
