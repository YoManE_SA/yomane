﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Modules
{
	public interface IPendingEventManager
	{
		void RegisterPending(TransactionContext transaction, PendingEventType eventTypeID, string parameters);
        PendingEvent GetNext(PendingEventType[] eventTypeIdFilter, PendingEventType[] eventTypeIdExcludeFilter);
		void SetPendingStatus(PendingEvent pendingData, bool deleteEvent, string logInfo);
	}
}
