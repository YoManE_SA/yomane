﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Process.Modules
{
    public interface IPersistComplete
    {
        void SaveComplete(TransactionContext transaction, SaveOptions options);
        void LoadComplete(TransactionContext transaction);
    }
}
