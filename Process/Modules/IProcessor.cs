﻿using System;

namespace Netpay.Process.Modules
{
	public interface IProcessor
	{
		bool Process(TransactionContext transaction); /* return true for approved */
		CommonTypes.ProcessTransactionType[] SupportedTypes { get; }
	}
}
