﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Process.Modules
{
	public interface IConfigurationReader
	{
		void ReadConfig(System.Xml.XmlNode node);
	}
}
