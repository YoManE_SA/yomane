﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Process.Modules
{
	public enum PostProcessResult
	{
		Succseeded = 0,
		Failed = 1,
		FailedNoRetry = 2,
		IsDisabled = 3,
	}
	public interface IPostProcessor
	{
		PostProcessResult PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData);
		bool Disabled { get; set; }
	}
}
