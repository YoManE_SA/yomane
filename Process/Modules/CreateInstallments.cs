﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Netpay.CommonTypes;

namespace Netpay.Process.Modules
{
	public class CreateInstallments : IPostProcessor
	{
		public bool Disabled { get; set; }
		
		PostProcessResult IPostProcessor.PostProcess(ProcessEngine processEngine, TransactionContext transaction, PendingEvent pendingData)
		{
			if (transaction.Request.amount == 0) throw new ApplicationException("Transaction amount is 0");
			if (transaction.Request.installmets == 0) throw new ApplicationException("Transaction installments is 0");
			decimal installmentAmount = System.Math.Floor(transaction.Request.amount / transaction.Request.installmets);
			decimal firstInstallmentAmount = transaction.Request.amount - (installmentAmount * (transaction.Request.installmets - 1));
			DateTime nextPaymentDate = transaction.Config.TransactionPayDate.GetValueOrDefault();
			for (byte installmentNumber = 1; installmentNumber <= transaction.Request.installmets; installmentNumber++)
			{
				//string comment = installmentNumber + " / " + transaction.Request.installmets;
				DateTime installmentPaymentDate = nextPaymentDate.AddMonths(installmentNumber - 1);
				decimal currentInstallmentAmount = installmentNumber == 1 ? firstInstallmentAmount : installmentAmount;
				processEngine.AddAmount(transaction, 0, new TransactionAmount(TransactionAmountType.InstallmentItem, -currentInstallmentAmount, -currentInstallmentAmount, installmentPaymentDate));
			}
			processEngine.AddLog(transaction, 0, TransactionHistoryType.Installment, null, transaction.Request.installmets, null, true);
			return PostProcessResult.Succseeded;
		}
	}
}
