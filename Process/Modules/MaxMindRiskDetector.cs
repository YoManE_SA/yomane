﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;

namespace Netpay.Process.Modules
{
	public class MaxMindRiskDetector : IRiskDetector
	{
		public string ServerAddress { get; set; }

		public MaxMindRiskDetector() { }
		public MaxMindRiskDetector(string iserverAddress) { ServerAddress = iserverAddress; }

		bool IRiskDetector.DetectRisk(TransactionContext transaction)
		{
			//bool isDuplicateAnswer;
			string licenseKey = "T78SAlrwAhpK"; //read from config
			//Build required fields
			string QueryString = "license_key=" + licenseKey + "&i=" + transaction.Request.clientIP + "&city=" + transaction.Request.billingAddress.City + "&region=" + transaction.Request.billingAddress.StateISO + "&postal=" + transaction.Request.billingAddress.Postal + "&country=" + transaction.Request.billingAddress.CountryISO;
			if (!string.IsNullOrEmpty(transaction.Request.customer.email))
			{
				int lDomain = transaction.Request.customer.email.IndexOf('@');
				if (lDomain > -1) QueryString += "&domain=" + transaction.Request.customer.email.Substring(lDomain + 1);
			}
			if (transaction.Request.creditCard.Pan.Length >= 6) QueryString += "&bin=" + transaction.Request.creditCard.Pan.Replace(" ", "").Substring(0, 6);
			//Send TO MAXMIND
			transaction.Response = new Netpay.Process.TransactionResponse(ProcessTransactionStatus.Approved, "000", null);
			return true;
			/*
			bool bFreeMail = GetURLValue(mxmRet, "freeMail");
			bool bHighRiskCountry = GetURLValue(mxmRet, "highRiskCountry");
			int nRetScore = GetURLValue(mxmRet, "score");
			int nRetRiskScore = GetURLValue(mxmRet, "riskScore");
			string sBinCountry = GetURLValue(mxmRet, "binCountry");
			int nRetReference = GetURLValue(mxmRet, "maxmindID");
			int nReturnQueriesRemaining = GetURLValue(mxmRet, "queriesRemaining");
			string sIPCountryCode = GetURLValue(mxmRet, "countryCode");
			if (nRetScore == -1) {
				nRetScore = -1; nRetRiskScore = -1;
			}

			bool IsTemperScore = false;
			if (bIsAllowFreeMail && sFreeMail == "YES"){
				IsTemperScore = true;
				nRetScore = nRetScore-2.5;
			}

			int nIsCountryWhitelisted = 0;
			if (sHighRiskCountry.ToUpper() == "YES") {
				if (IsCountryInList(sBinCountry, sCountryWhiteList) || IsCountryInList(sIPCountryCode, sCountryWhiteList)) {
					nRetScore = nRetScore - 2.5;
					nIsCountryWhitelisted = 1;
				}
			}

			bool isProceed = true;
			'Check risk score
			If isProceed Then
				'old score (1..10)
  				If nRetScore<>"" And CDbl(nMaxMind_MinScore) >= 0 Then
					If IsNumeric(nRetScore) Then
						If CDbl(nRetScore) > CDbl(nMaxMind_MinScore) Then
							isProceed = false
							nReply = "580"
						End if
					End if
				End if	
				'new score (1..100)
  				If nRetRiskScore<>"" And CDbl(nRiskScoreAllowed) >= 0 Then
					If IsNumeric(nRetRiskScore) Then
						If CDbl(nRetRiskScore) - CDbl(nRiskScoreAllowed) > 0 Then
							isProceed = false
							nReply = "580"
						End if
					End if
				End if	
			End if
			*/
		}
	}
}
