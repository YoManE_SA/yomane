﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Netpay.Process
{
	public class HttpEndPointsManager
	{
		private System.Collections.Generic.Dictionary<string, IHttpEndPoint> _endPoints = new Dictionary<string, IHttpEndPoint>();
		private System.Net.HttpListener _httpListen = new System.Net.HttpListener();
		public string defaultAddress{ get; set; }
		
		public interface IHttpEndPoint { void RemoteInvoke(System.Net.HttpListenerContext context); }

		public HttpEndPointsManager() 
		{
            System.Net.IPAddress[] address = System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName());
            for(int i = 0; i < address.Length; i++){
                if (address[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
                    defaultAddress = "http://" + address[i].ToString() + ":81/";
                    break;
                }
            }
		}

		public string RegisterEndPonit(string pointName, IHttpEndPoint endPoint)
		{
			_endPoints.Add(pointName, endPoint);
			_httpListen.Prefixes.Add(defaultAddress + pointName);
			return defaultAddress + pointName;
		}

		public void RemoveEndPonit(string pointName)
		{
			_endPoints.Remove(pointName);
			_httpListen.Prefixes.Remove(defaultAddress + pointName);
		}
		
		private void ProcessRequest(IAsyncResult result)
		{
			IHttpEndPoint endPoint = null;
			System.Net.HttpListenerContext cxt = null;
			try { cxt = _httpListen.EndGetContext(result); }
			catch (Exception ex) { Application.ProcessEngine.LogError(CommonTypes.TransactionStage.Process, new Exception("EndGetContext throw exception", ex)); }
			if (cxt != null) {
				if (_endPoints.TryGetValue(cxt.Request.Url.LocalPath.Substring(1), out endPoint)) {
					try { endPoint.RemoteInvoke(cxt); }
					catch (Exception ex) { Application.ProcessEngine.LogError(CommonTypes.TransactionStage.Process, new Exception(string.Format("IHttpEndPoint.RemoteInvoke of {0} throw exception", endPoint.ToString()), ex)); }
				}
				try { cxt.Response.Close(); } catch (Exception ex) { Application.ProcessEngine.LogError(CommonTypes.TransactionStage.Process, new Exception("Could not close HttpListenerContext", ex)); }
			}			
			_httpListen.BeginGetContext(new AsyncCallback(ProcessRequest), null);
		}

		public void Start()
		{
			_httpListen.Start();
			//for(int i = 0; i < 1; i++)
			_httpListen.BeginGetContext(new AsyncCallback(ProcessRequest), null);
		}

		public void Stop()
		{
			_httpListen.Stop();
		}

	}
}
