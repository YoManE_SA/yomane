﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections;
using System.IO;
using System.Reflection;
using Netpay.Process.Modules;

namespace Netpay.Process
{
	internal static class Configuration
	{
		public static string GetXmlAttribute(XmlNode node, string attributeName)
		{
			XmlAttribute attribute = node.Attributes[attributeName];
			if (attribute == null)
				return null;

			return attribute.Value;
		}

		public static object ParseObject(string str, Type type)
		{
			object obj = null;
			if (type.IsPrimitive)
			{
				if (type == typeof(Boolean)) {
					obj = bool.Parse(str);
				} else {
					long value = Int64.Parse(str);
					obj = ((IConvertible)value).ToType(type, null);
				}
			}
			else if (type.IsEnum)
			{
				obj = Enum.Parse(type, str);
			}
			else if (type == typeof(string))
			{
				obj = str;
			}
			else if (type == typeof(DateTime))
			{
				obj = DateTime.Parse(str);
			}
			else
			{
				System.Reflection.ConstructorInfo constructor = null;
				if (str != null)
				{
					type.GetConstructor(new Type[] { typeof(string) });
					if (constructor != null) obj = constructor.Invoke(new object[] { str });
				}
				if (obj == null)
				{
					constructor = type.GetConstructor(new Type[] { });
					if (constructor == null) throw new Exception("Unable to find suitable constructor to type " + type.FullName);
					obj = constructor.Invoke(null);
				}
			}

			return obj;
		}

		public static void SetObjectProperties(object obj, XmlNode node)
		{
			Type type;
			if (obj.GetType().Name == "RuntimeType")
			{
				type = (Type)obj;
				obj = null;
			}
			else
			{
				type = obj.GetType();
			}

			foreach (XmlNode currentNode in node.ChildNodes)
			{
				if (currentNode.NodeType != XmlNodeType.Element) continue;
				PropertyInfo propertyInfo = type.GetProperty(currentNode.LocalName);
				if (propertyInfo != null)
				{
					if (propertyInfo.PropertyType == typeof(System.Collections.IDictionary) || propertyInfo.PropertyType.GetInterface("System.Collections.IDictionary") != null)
					{
						object collection = propertyInfo.GetValue(obj, null);
						if (collection == null)
							propertyInfo.SetValue(obj, collection = ParseObject(null, propertyInfo.PropertyType), null);
						LoadDictionary(collection as System.Collections.IDictionary, currentNode);
					}
					else if (propertyInfo.PropertyType.GetInterface("System.Collections.IList") != null)
					{
						object list = propertyInfo.GetValue(obj, null);
						if (list == null)
							propertyInfo.SetValue(obj, list = ParseObject(null, propertyInfo.PropertyType), null);
						LoadCollection(list as System.Collections.IList, currentNode);
					}
					else
					{
						object newObj;
						if (propertyInfo.CanWrite)
						{
							if (propertyInfo.PropertyType.IsInterface) {
								if (currentNode.ChildNodes.Count != 1) throw new Exception(string.Format("property {0} of object {1} is not collection, only one element allowed", currentNode.LocalName, type.FullName));
								newObj = CreateInstanceFromXml(currentNode.ChildNodes[0]);
							} else newObj = ParseObject(currentNode.InnerText, propertyInfo.PropertyType);
							propertyInfo.SetValue(obj, newObj, null);
						}
						else
						{
							//if (currentNode.ChildNodes.Count == 0) throw new Exception(string.Format("property {0} is read only", propertyInfo.Name));
							newObj = propertyInfo.GetValue(obj, null);
							if (newObj == null) throw new Exception(string.Format("property {0} of object {1} is null", currentNode.LocalName, type.FullName));
						}
						if (currentNode.ChildNodes.Count > 0) SetObjectProperties(newObj, currentNode);
					}
				} else Application.ProcessEngine.LogError(CommonTypes.TransactionStage.Process, new Exception(string.Format("Object {0} does not support property '{1}'", type.FullName, currentNode.LocalName)));
			}
		}

		private static object CreateInstanceFromXml(XmlNode node)
		{
			if (node.LocalName == "Load" || node.LocalName == "Add")
			{
				string className = GetXmlAttribute(node, "class");
				if (string.IsNullOrEmpty(className))
					throw new Exception("found Load or Add with empty class attribute");
				Type type = System.Type.GetType(className, false);
				if (type == null)
					throw new Exception(string.Format("unable to resolve type name '{0}'", className));
				object result = ParseObject(node.InnerText, type);
				if (result is IConfigurationReader)
					(result as IConfigurationReader).ReadConfig(node);
				else
					SetObjectProperties(result, node);

				string id = GetXmlAttribute(node, "id");
				if (id == null) id = type.FullName + ":" + result.GetHashCode().ToString();
				Application.Modules.Add(id, result);
				return result;
			}
			else if (node.LocalName == "Use")
			{
				return Application.Modules[GetXmlAttribute(node, "id")];
			}
			else return null;
		}

		private static void LoadCollection(IList collection, XmlNode node)
		{
			if (node == null) return;
			foreach (XmlNode currentNode in node.ChildNodes) {
				if (currentNode.NodeType != XmlNodeType.Element) continue;
				collection.Add(CreateInstanceFromXml(currentNode));
			}
		}

		private static void LoadDictionary(IDictionary collection, XmlNode node)
		{
			if (node == null) return;
			foreach (XmlNode currentNode in node.ChildNodes)
			{
				if (currentNode.NodeType != XmlNodeType.Element) continue;
				object obj = CreateInstanceFromXml(currentNode);
				if (collection != Application.Modules)
					collection.Add(GetXmlAttribute(currentNode, "name"), obj);
			}
		}

		public static void LoadConfig()
		{
			XmlDocument xmlDoc = new XmlDocument();
            string fileName = Path.GetDirectoryName(Assembly.GetAssembly(typeof(Configuration)).CodeBase).Remove(0, 6);
            if (fileName[1] != ':') fileName = "\\\\" + fileName;
            fileName += @"\Netpay.Process.config.xml";
			if (!File.Exists(fileName))
				throw new ApplicationException(string.Format("Could not find configuration file '{0}'", fileName));
			xmlDoc.Load(fileName);

			XmlNode root = xmlDoc.SelectSingleNode("Configuration");
			if (root == null)
				throw new ApplicationException(string.Format("Could not find configuration element in file '{0}'", fileName));

			SetObjectProperties(typeof(Application), root.SelectSingleNode("./Application"));
		}
	}
}
