﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;

using System.Net;
using System.Threading;
using System.Xml.Serialization;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Globalization;
using System.Web;

namespace Netpay.Infrastructure
{
    /// <summary>
    /// Contains extension methods
    /// </summary>
    public static partial class Extensions
    {
        public static bool IsJpeg(this byte[] source)
        {
            if (source == null)
                return false;
            if (source.Length < 4)
                return false;
            return source[0] == 0xFF && source[1] == 0xD8 && source[2] == 0xFF && source[3] == 0xE0;
        }

        public static bool IsPng(this byte[] source)
        {
            if (source == null)
                return false;
            if (source.Length < 4)
                return false;
            return source[0] == 0x89 && source[1] == 0x50 && source[2] == 0x4E && source[3] == 0x47;
        }

        public static bool IsImage(this byte[] source)
        {
            return source.IsPng() || source.IsJpeg();
        }

        public static string RemoveUnicodeChars(this string source)
        {
            string replaced = Regex.Replace(source, @"[^\u0000-\u007F]", string.Empty);
            return replaced;
        }

        /// <summary>
        /// Cuts out HTML tags from the HTML source, replaces common escape sequences with respective plain text characters
        /// </summary>
        /// <param name="htmlText">HTML source text</param>
        /// <returns>plain text</returns>
        public static string ToPlainText(this string htmlText)
        {
            if (htmlText == null) return null;
            string s = htmlText;
            int i = 0, j = 0;

            while (j > -1)
            {
                i = s.IndexOf("<", i);
                if (i == -1) break;
                j = s.IndexOf(">", i + 1);
                if (j == -1) break;
                s = s.Remove(i, j - i + 1);
            }
            return s.Replace("<", string.Empty).Replace(">", string.Empty).Replace("&nbsp;", " ").Replace("&amp;", "&").Replace("&quot;", "\"").Replace("&copy;", "©").Replace("&euro;", "€").Replace("&pound;", "£");
        }

        public static string EmptyIfNull(this Dictionary<string, string> source, string key)
        {
            if (!source.ContainsKey(key))
                return "";

            return source[key];
        }

        public static bool IsEmail(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\\w");
            return exp.IsMatch(source);
        }

        public static bool IsPhone(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[\d-\+\(\)]+$");
            return exp.IsMatch(source);
        }

        public static bool IsName(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[\w .]+$");
            return exp.IsMatch(source);
        }

        public static bool IsCvv(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[0-9]{3,4}$");
            return exp.IsMatch(source);
        }

        public static bool IsCreditcard(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[0-9 ]{9,19}$");
            return exp.IsMatch(source);
        }

        public static bool IsPersonalNumber(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[0-9]{7,10}$");
            return exp.IsMatch(source);
        }

        public static bool IsExpMonth(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[0-9]{2}$");
            return exp.IsMatch(source);
        }

        public static bool IsCurrency(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[A-Z]{3}$");
            return exp.IsMatch(source);
        }

        public static bool IsExpYear(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[0-9]{2,4}$");
            return exp.IsMatch(source);
        }

        public static bool IsUserName(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[\w@-]{4,20}$");
            return exp.IsMatch(source);
        }

        public static bool IsPassword(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[\w]{4,20}$");
            return exp.IsMatch(source);
        }

        public static bool IsImei(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[0-9a-fA-F-]{15,40}$");
            return exp.IsMatch(source);
        }

        public static bool IsTrack2(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[0-9;=?]+$");
            return exp.IsMatch(source);
        }

        public static bool IsAppVersion(this string source)
        {
            if (source == null)
                return false;

            Regex exp = new Regex(@"^[0-9.a-z]{2,6}$");
            return exp.IsMatch(source);
        }

        public static bool IsNumeric(this string source)
        {
            if (source == null)
                return false;

            decimal temp;
            return decimal.TryParse(source, out temp);
        }

        public static bool IsAmount(this string source)
        {
            if (source == null)
                return false;

            if (!source.Contains(".")) return false;

            decimal temp;
            return decimal.TryParse(source, out temp);
        }

        public static string RemoveWhitespaces(this string source)
        {
            return Regex.Replace(source, @"\s+", "");
        }

        /// <summary>
        /// Determines whether a file is being used.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool IsLocked(this FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return false;
        }

        /// <summary>
        /// Base64 encoded md5 compute result ot the string
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
		public static string ToMD5(this string source)
        {
            byte[] hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(source));
            return hash.ToBase64();
        }

        /// <summary>
        /// Base64 encoded sha256 compute result ot the string
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
		public static string ToSha256(this string source)
        {
            byte[] hash = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(source));
            return hash.ToBase64();
        }

        public static string GetValuesString(this System.Collections.Specialized.NameValueCollection source)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < source.Count; i++)
                sb.Append(source[i]);
            return sb.ToString();
        }


        public static byte[] FromBase64(this string source)
        {
            return System.Convert.FromBase64String(source);
        }

        public static string ToBase64(this byte[] source)
        {
            return Convert.ToBase64String(source);
        }

        public static string ReplaceFileName(this Uri source, string newFileName)
        {
            string path = String.Format("{0}{1}{2}", source.Scheme, Uri.SchemeDelimiter, source.Authority);
            for (int index = 0; index < source.Segments.Length - 1; index++)
            {
                path += source.Segments[index];
            }
            path = path + newFileName;

            return path;
        }

        /// <summary>
        /// Converts a string to byte array using the provided encoder.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static byte[] ToBytes(this string source, Encoding encoding)
        {
            return encoding.GetBytes(source);
        }

        /// <summary>
        /// Converts a string to byte array using UTF8 encoding.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static byte[] ToBytes(this string source)
        {
            return source.ToBytes(Encoding.UTF8);
        }

        public static string ConvertToString(this byte[] source, Encoding encoding)
        {
            return new string(encoding.GetChars(source));
        }

        public static string ConvertToString(this byte[] source)
        {
            return source.ConvertToString(Encoding.UTF8);
        }

        public static Guid? ToNullableGuid(this object source)
        {
            if (source == null || source == DBNull.Value) return null;
            if (source is Guid) return (Guid)source;
            Guid ret;
            if (Guid.TryParse(source.ToString(), out ret)) return ret;
            return null;
        }

        /// <summary>
        /// Converts the list to a comma delimited string
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToDelimitedString<T>(this IEnumerable<T> source)
        {
            return source.ToDelimitedString(",");
        }

        public static string ToDelimitedString<T>(this IEnumerable<T> source, char delimiter)
        {
            return source.ToDelimitedString(delimiter.ToString());
        }

        /// <summary>
        /// Converts the list to a delimited string with a specified delimiter.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static string ToDelimitedString<T>(this IEnumerable<T> source, string delimiter)
        {
            if (source == null || source.Count() == 0)
                return "";

            return source.Aggregate("", (current, next) => current.ToString() + delimiter.ToString() + next.ToString()).Remove(0, delimiter.Length);
        }

        /// <summary>
        /// Performs a binary copy of the instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T DeepCopy<T>(this T source)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(memory, source);
                memory.Position = 0;

                return (T)formatter.Deserialize(memory);
            }
        }

        /// <summary>
        /// Outputs the object public properties values.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="seperator"></param>
        /// <returns></returns>
        public static string ToExtendedString(this object source, string seperator = "\r\n", bool nameToSentence = false)
        {
            if (source == null) return null;
            StringBuilder propertyBuilder = new StringBuilder();
            MemberInfo[] properties = source.GetType().GetMembers(BindingFlags.Public | BindingFlags.Instance).Where(m => (m.MemberType == MemberTypes.Property) || (m.MemberType == MemberTypes.Field)).ToArray();
            foreach (var member in properties)
            {
                object propertyValue = null;
                if (member.MemberType == MemberTypes.Property)
                {
                    var currentProperty = member as PropertyInfo;
                    if (!currentProperty.CanRead) continue;
                    if (currentProperty.GetIndexParameters().Length > 0) propertyValue = string.Format("Indexer({0})", currentProperty.GetIndexParameters().Count());
                    else propertyValue = currentProperty.GetValue(source, null);
                }
                else if (member.MemberType == MemberTypes.Field)
                {
                    var currentField = member as FieldInfo;
                    propertyValue = currentField.GetValue(source);
                }
                if (propertyValue != null)
                {
                    string name = nameToSentence ? member.Name.ToSentence() : member.Name;
                    propertyBuilder.Append(name + " = " + propertyValue);
                    propertyBuilder.Append(seperator);
                }
            }

            return propertyBuilder.ToString();
        }

        public static string ToQueryString(this NameValueCollection source, System.Text.Encoding encoding = null)
        {
            StringBuilder resultBuilder = new StringBuilder();
            foreach (string currentKey in source.Keys)
            {
                resultBuilder.Append(currentKey);
                resultBuilder.Append("=");
                var value = source[currentKey];
                if (encoding != null) value = value.ToEncodedUrl(encoding);
                resultBuilder.Append(value);
                resultBuilder.Append("&");
            }
            if (resultBuilder.Length > 0)
                resultBuilder.Remove(resultBuilder.Length - 1, 1);

            return resultBuilder.ToString();
        }

        public static string ToQueryString(this object source)
        {
            if (source == null)
                return null;

            StringBuilder resultBuilder = new StringBuilder();
            MemberInfo[] properties = source.GetType().FindMembers(MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance, null, null);
            foreach (PropertyInfo currentProperty in properties)
            {
                object propertyValue = propertyValue = currentProperty.GetValue(source, null);
                resultBuilder.Append(currentProperty.Name + "=" + propertyValue);
                resultBuilder.Append("&");
            }
            resultBuilder.Remove(resultBuilder.Length - 1, 1);

            return resultBuilder.ToString();
        }

        public delegate void GetPropertyValue(PropertyInfo p, object item, ref object value);
        public static string ToHtmlTable<T>(this System.Collections.Generic.IList<T> source, System.Collections.Generic.HashSet<string> fields = null, bool listIsExclude = false, GetPropertyValue propertyDelegate = null)
        {
            StringBuilder sb = new StringBuilder();
            var arrayType = typeof(T).GetProperties(BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public);
            sb.Append("<table border=\"1\">");
            sb.Append("<tr>");
            foreach (PropertyInfo p in arrayType)
            {
                if (fields != null)
                {
                    if (listIsExclude && fields.Contains(p.Name)) continue;
                    else if (!listIsExclude && !fields.Contains(p.Name)) continue;
                }
                sb.AppendFormat("<th>{0}</th>", p.Name);
            }
            sb.Append("</tr>");
            foreach (var item in source)
            {
                sb.Append("<tr>");
                foreach (PropertyInfo p in arrayType)
                {
                    if (fields != null)
                    {
                        if (listIsExclude && fields.Contains(p.Name)) continue;
                        else if (!listIsExclude && !fields.Contains(p.Name)) continue;
                    }
                    object value = p.GetValue(item, null);
                    if (propertyDelegate != null) propertyDelegate(p, item, ref value);
                    sb.AppendFormat("<td>{0}</td>", value);
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }

        public static string ToXml(this object source)
        {
            MemoryStream memoryStream = new MemoryStream();
            StreamReader streamReader = null;
            string result = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(source.GetType());
                serializer.Serialize(memoryStream, source);
                memoryStream.Position = 0;
                streamReader = new StreamReader(memoryStream);
                result = streamReader.ReadToEnd();
            }
            finally
            {
                if (memoryStream != null)
                    memoryStream.Close();
                if (streamReader != null)
                    streamReader.Close();
            }

            return result;
        }

        public static T FromXml<T>(this string source)
        {
            return source.FromXml<T>(UnicodeEncoding.Unicode);
        }

        public static T FromXml<T>(this string source, Encoding encoding)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            byte[] bytes = encoding.GetBytes(source);
            MemoryStream stream = new MemoryStream(bytes);
            T result = (T)serializer.Deserialize(stream);
            stream.Close();

            return result;
        }

        public static T? ToNullableEnum<T>(this object source) where T : struct
        {
            int? intValue = null;
            if (source == null || source == DBNull.Value) return null;
            else if (source is int) intValue = (int)source;
            else if (source is short) intValue = (short)source;
            else if (source is byte) intValue = (byte)source;
            else if (source is string) intValue = source.ToString().ToNullableInt();
            //else if (source is decimal || source is float || source is double) return (T)source;
            if (intValue != null) return (T)System.Enum.ToObject(typeof(T), intValue.Value);
            T ret;
            if (!System.Enum.TryParse<T>(source.ToString(), out ret)) return null;
            return ret;
        }

        public static Nullable<T> ToNullableEnumByName<T>(this string source) where T : struct
        {
            if (string.IsNullOrEmpty(source)) return null;
            if (!Enum.IsDefined(typeof(T), source))
                return null;

            return (T)Enum.Parse(typeof(T), source);
        }

        public static Nullable<T> ToNullableEnumByValue<T>(this string source) where T : struct
        {
            int parsed;
            if (string.IsNullOrEmpty(source)) return null;
            if (!int.TryParse(source, out parsed))
                return null;

            if (!Enum.IsDefined(typeof(T), parsed))
                return null;

            return (T)Enum.Parse(typeof(T), source);
        }

        public static Nullable<T> ToNullableEnum<T>(this byte? source) where T : struct
        {
            if (source == null)
                return null;

            if (!Enum.IsDefined(typeof(T), Convert.ToInt32(source.Value)))
                return null;

            string name = Enum.GetName(typeof(T), Convert.ToInt32(source.Value));
            return (T)Enum.Parse(typeof(T), name);
        }

        /// <summary>
        /// Set DateTime time value to 00:00:00.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static DateTime MinTime(this DateTime source)
        {
            return new DateTime(source.Year, source.Month, source.Day, 0, 0, 0);
        }

        /// <summary>
        /// Set DateTime time value to 23:59:59. 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static DateTime MaxTime(this DateTime source)
        {
            return new DateTime(source.Year, source.Month, source.Day, 23, 59, 59);
        }

        public static DateTime AlignToEnd(this DateTime date)
        {
            if (date.TimeOfDay.Seconds == 0) return date.Date.AddDays(1).AddMilliseconds(-1);
            return date;
        }

        /// <summary>
        /// Result string will be maxCharacters long with last 3 chars replaced to dots.
        /// Example: "Some long string".Truncate(10) = "Some lo..."
        /// </summary>
        /// <param name="source"></param>
        /// <param name="maxCharacters"></param>
        /// <returns></returns>
        public static string Truncate(this string source, int maxCharacters)
        {
            if (source == null)
                return null;

            if (source.Length > maxCharacters)
                source = source.Substring(0, maxCharacters - 3) + "...";

            return source;
        }

        public static string TruncStart(this string source, int maxCharacters)
        {
            if (source == null) return null;
            if (source.Length > maxCharacters) source = source.Substring(source.Length - maxCharacters, maxCharacters);
            return source;
        }

        public static string TruncEnd(this string source, int maxCharacters)
        {
            if (source == null) return null;
            if (source.Length > maxCharacters) source = source.Substring(0, maxCharacters);
            return source;
        }

        /// <summary>
        /// Formats cc as 458000 XX XXXX 0000
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
		public static string ToSafeCCString(this string source)
        {
            if (source == null)
                return null;

            source = source.Trim();
            if (source.Length <= 4)
                return source;
            //if (source.Length < 12)
            //    throw new ArgumentException("number too short");

            string firstDigits = source.Substring(0, 6);
            if (source.Length < 12) return firstDigits + " XX XXXX";
            string lastDigits = source.Substring(12);
            source = firstDigits + " XX XXXX " + lastDigits;

            return source;
        }

        /// <summary>
        /// Returns nullable int if the the string can be parsed.
        /// Otherwise null is returned.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static int? ToNullableInt32(this string source)
        {
            int parsed;
            if (Int32.TryParse(source, out parsed))
                return parsed;

            return null;
        }

        public static bool? ToNullableBool(this object source)
        {
            if (source == null || source == DBNull.Value) return null;
            if (source is bool) return (bool)source;
            bool parsed;
            if (bool.TryParse(source.ToString(), out parsed))
                return parsed;
            return null;
        }

        public static decimal? ToNullableDecimal(this object source)
        {
            if (source == null || source == DBNull.Value) return null;
            if (source is decimal) return (decimal)source;
            decimal parsed;
            if (decimal.TryParse(source.ToString(), out parsed))
                return parsed;
            return null;
        }

        public static DateTime? ToNullableDate(this object source)
        {
            if (source == null || source == DBNull.Value) return null;
            if (source is DateTime) return (DateTime)source;
            DateTime parsed;
            if (DateTime.TryParse(source.ToString(), out parsed))
                return parsed;
            return null;
        }

        public static int? ToNullableInt(this object source)
        {
            if (source == null || source == DBNull.Value) return null;
            if (source is int) return (int)source;
            int parsed;
            if (int.TryParse(source.ToString(), out parsed))
                return parsed;
            return null;
        }


        public static string ToString(this System.Data.DataRowView source, string columnName)
        {
            if (!source.Row.Table.Columns.Contains(columnName))
                return null;

            return source[columnName].ToString();
        }

        public static int ToInt32(this System.Data.DataRowView source, string columnName)
        {
            if (!source.Row.Table.Columns.Contains(columnName))
                return 0;

            int parsed;
            if (Int32.TryParse(source[columnName].ToString(), out parsed))
                return parsed;

            return 0;
        }

        public static int? ToNullableInt32(this System.Data.DataRowView source, string columnName)
        {
            if (!source.Row.Table.Columns.Contains(columnName))
                return null;

            int parsed;
            if (Int32.TryParse(source[columnName].ToString(), out parsed))
                return parsed;

            return null;
        }

        public static decimal? ToNullableDecimal(this System.Data.DataRowView source, string columnName)
        {
            if (!source.Row.Table.Columns.Contains(columnName))
                return null;

            decimal parsed;
            if (decimal.TryParse(source[columnName].ToString(), out parsed))
                return parsed;

            return null;
        }

        public static short? ToNullableShort(this string source)
        {
            short parsed;
            if (short.TryParse(source, out parsed))
                return parsed;

            return null;
        }

        public static byte? ToNullableByte(this string source)
        {
            byte parsed;
            if (byte.TryParse(source, out parsed))
                return parsed;

            return null;
        }

        public static bool? ToNullableBool(this string source)
        {
            bool parsed;
            if (bool.TryParse(source, out parsed))
                return parsed;

            return null;
        }

        public static DateTime? ToNullableDateTime(this string source)
        {
            DateTime parsed;
            if (DateTime.TryParse(source, out parsed))
                return parsed;
            return null;
        }

        /// <summary>
        /// Converts the string to boolean.
        /// .net boolean parse (TrueString, FalseString), also 1 = true, 0 = false
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static bool ToBoolean(this string source, bool defaultValue)
        {
            source = source.Trim();
            if (source.Length == 1)
            {
                if (source == "1")
                    return true;
                if (source == "0")
                    return false;
                return defaultValue;
            }

            bool parsed;
            if (bool.TryParse(source, out parsed))
                return parsed;

            return defaultValue;
        }

        /// <summary>
        /// Converts the string to short.
        /// When conversion fails 0 is returned.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static short ToShort(this string source)
        {
            short parsed;
            if (short.TryParse(source, out parsed))
                return parsed;

            return 0;
        }

        public static byte ToByte(this string source, byte defaultValue)
        {
            byte parsed;
            if (byte.TryParse(source, out parsed))
                return parsed;

            return defaultValue;
        }

        /// <summary>
        /// Converts the string to Int32.
        /// When conversion fails 0 is returned.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static int ToInt32(this string source)
        {
            int parsed;
            if (Int32.TryParse(source, out parsed))
                return parsed;

            return 0;
        }

        /// <summary>
        /// Converts the string to Int32.
        /// When conversion fails defaultValue is returned.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int ToInt32(this string source, int defaultValue)
        {
            int parsed;
            if (Int32.TryParse(source, out parsed))
                return parsed;

            return defaultValue;
        }

        public static long ToInt64(this string source, long defaultValue)
        {
            long parsed;
            if (Int64.TryParse(source, out parsed))
                return parsed;

            return defaultValue;
        }

        public static decimal ToDecimal(this string source, decimal defaultValue)
        {
            decimal parsed;
            if (decimal.TryParse(source, out parsed))
                return parsed;

            return defaultValue;
        }

        public static decimal ToDecimal(this decimal? source, decimal defaultValue)
        {
            return source == null ? defaultValue : source.Value;
        }

        /// <summary>
        /// Converts the string to Int32.
        /// When conversion fails or the number is not within boundaries, defaultValue is returned.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="maxInclusive"></param>
        /// <param name="minInclusive"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int ToInt32(this string source, int minInclusive, int maxInclusive, int defaultValue)
        {
            int parsed;
            if (Int32.TryParse(source, out parsed))
            {
                if (parsed >= minInclusive && parsed <= maxInclusive)
                    return parsed;
            }

            return defaultValue;
        }

        /// <summary>
        /// Returns nullable decimal if the the string can be parsed.
        /// Otherwise null is returned.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static decimal? ToNullableDecimal(this string source)
        {
            decimal parsed;
            if (decimal.TryParse(source, out parsed))
                return parsed;

            return null;
        }

        public static int? ToNullableInt(this string source)
        {
            int parsed;
            if (int.TryParse(source, out parsed))
                return parsed;
            return null;
        }

        public static long? ToNullableLong(this string source)
        {
            long parsed;
            if (long.TryParse(source, out parsed))
                return parsed;
            return null;
        }

        /// <summary>
        /// Returns sql formatted date string.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToSql(this DateTime source)
        {
            return source.ToString("yyyyMMdd");
        }

        /// <summary>
        /// Returns sql formatted date string with sql quotes.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToSqlValue(this DateTime source)
        {
            return "'" + source.ToString("yyyyMMdd") + "'";
        }


        /// <summary>
        /// Converts the string to safe sql string.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToSql(this string source)
        {
            if (source == null || source.Trim() == "")
                return source;

            return source.Replace("'", "''");
        }

        /// <summary>
        /// Converts the boolean to sql bit.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static byte ToSql(this bool source)
        {
            if (source)
                return 1;
            else
                return 0;
        }

        /// <summary>
        /// Formats the number as html.
        /// Negative number is given the css class 'negativeNumber'.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ToHtml(this decimal source, string format)
        {
            if (source < 0)
                if (format == null)
                    return string.Format("<span class=\"negativeNumber\">{0}</span>", source.ToString());
                else
                    return string.Format("<span class=\"negativeNumber\">{0}</span>", source.ToString(format));

            return string.Format("<span>{0}</span>", source.ToString(format));
        }

        /// <summary>
        /// Returns null if the string is empty
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string NullIfEmpty(this string source)
        {
            if (source == null || source.Trim().Length == 0)
                return null;

            return source;
        }

        /// <summary>
        /// Returns empty string if null.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string EmptyIfNull(this string source)
        {
            if (source == null)
                return "";

            return source;
        }

        /// <summary>
        /// Returns the value if null.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ValueIfNull(this string source, string value)
        {
            if (source == null)
                return value;

            return source;
        }

        /// <summary>
        /// Returns the value if null.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static int ValueIfNull(this int? source, int value)
        {
            if (source == null)
                return value;

            return source.Value;
        }

        /// <summary>
        /// Returns empty string if null.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string EmptyIfNull(this IPAddress source)
        {
            if (source == null)
                return "";

            return source.ToString();
        }

        /// <summary>
        /// Formats a date using the configuration file format string.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static int WeekOfYear(this DateTime source)
        {
            GregorianCalendar cal = new GregorianCalendar(GregorianCalendarTypes.Localized);
            return cal.GetWeekOfYear(source, CalendarWeekRule.FirstFullWeek, DayOfWeek.Sunday);
        }

        /// <summary>
        /// Formats a date using the configuration file format string.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToDateFormat(this DateTime source)
        {
            return source.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern);
        }

        /// <summary>
        /// Formats a date using the configuration file format string.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToDateTimeFormat(this DateTime source)
        {
            return source.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern + " " + Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern);
        }

        /// <summary>
        /// Returns valid csv field string.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToCsv(this string source)
        {
            source = source.Replace("\"", "\"\"");
            source = string.Format("\"{0}\"", source);

            return source;
        }

        public static string ToEncodedUrl(this string source)
        {
            return HttpUtility.UrlEncode(source);
        }

        public static string ToEncodedUrl(this string source, System.Text.Encoding encoding)
        {
            return HttpUtility.UrlEncode(source, encoding);
        }

        public static string ToDecodedUrl(this string source)
        {
            return HttpUtility.UrlDecode(source);
        }

        public static string GetUrlValue(this string source, string param, System.Text.Encoding encoding = null)
        {
            if (encoding == null) encoding = System.Text.Encoding.UTF8;
            int sIndex = source.IndexOf(param + "=", System.StringComparison.OrdinalIgnoreCase);
            if (sIndex < 0) return null;
            sIndex = sIndex + param.Length + 1;
            int eIndex = source.IndexOf("&", sIndex, System.StringComparison.OrdinalIgnoreCase);
            if (eIndex < 0) eIndex = source.Length;
            return source.Substring(sIndex, eIndex - sIndex);

        }

        /// <summary>
        /// Determines whether this data row contains a column.
        /// </summary>
        /// <param name="dataRecord"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static bool HasColumn(this IDataRecord dataRecord, string columnName)
        {
            for (int i = 0; i < dataRecord.FieldCount; i++)
            {
                if (dataRecord.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Converts camel case to readable sentence.
        /// Example: camelCase = Camel case
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToSentence(this string value)
        {
            if (value == null || value.Length == 0)
                return null;

            if (value.Length < 4)
                return value;

            string sentence = Regex.Replace(value, ".[A-Z]", m => m.ToString()[0] + " " + char.ToLower(m.ToString()[1]));
            sentence = sentence.ToLower();
            sentence = sentence[0].ToString().ToUpper() + sentence.Remove(0, 1);

            return sentence;
        }

        /// <summary>
        /// Returns a readable file size.
        /// Example: 1.4 MB, 2.6 KB
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ToFileSize(this long bytes)
        {
            const int scale = 1024;
            string[] orders = new string[] { "EB", "PB", "TB", "GB", "MB", "KB", "Bytes" };
            long max = (long)System.Math.Pow(scale, orders.Length - 1);

            foreach (string order in orders)
            {
                if (bytes > max)
                    return string.Format("{0:##.##} {1}", decimal.Divide(bytes, max), order);

                max /= scale;
            }

            return "0 Bytes";
        }

        public static string ToPercentFormat(this decimal source)
        {
            return (source / 100).ToString("P2");
        }

        public static string ToPercentFormat(this double source)
        {
            return ToPercentFormat(Convert.ToDecimal(source));
        }



        /// <summary>
        /// Returns boolean telling if the specified bit is 1 or 0 in the bitwise representation of the integer
        /// </summary>
        /// <param name="source">the number to evaluate</param>
        /// <param name="position">the zero-based position of the bit to evaluate</param>
        /// <returns>true, if the bit is 1; otherwise false</returns>
        public static bool IsBitOn(this int source, int position)
        {
            bool bOK = (System.Math.IEEERemainder(source, System.Math.Pow(2, position + 1)) >= System.Math.Pow(2, position));
            return bOK;
        }

        public static object NullIfDBNull(this object source)
        {
            if (source is DBNull) return null;
            return source;
        }

        public static System.Collections.Generic.Dictionary<int, string> ToEnumDictionary(this System.Type enumType, System.Resources.ResourceManager resource = null)
        {
            System.Array values = System.Enum.GetValues(enumType);
            string[] names = System.Enum.GetNames(enumType);
            var ret = new System.Collections.Generic.Dictionary<int, string>(values.Length);
            for (int i = 0; i < values.Length; i++)
            {
                string text = names[i];
                if (resource != null)
                {
                    text = resource.GetString(string.Format("{0}_{1}", enumType.Name, text));
                    if (text == null) text = names[i];
                }
                ret.Add((int)values.GetValue(i), text);
            }
            return ret;
        }

        public static System.Collections.Generic.Dictionary<int, string> ToEnumDictionary(this System.Type enumType, string resourceName)
        {
            return ToEnumDictionary(enumType, new System.Resources.ResourceManager("Resources." + resourceName, System.Reflection.Assembly.Load("App_GlobalResources")));
        }

        public static string EnumText(this object value, System.Resources.ResourceManager resource = null)
        {
            if (value == null || value is DBNull) return null;
            var strValue = value.ToString();
            if (resource != null)
            {
                string ret = resource.GetString(string.Format("{0}_{1}", value.GetType().Name, strValue));
                if (ret != null) return ret;
            }
            return strValue;
        }

        public static string EnumText(this object value, string resourceName)
        {
            return EnumText(value, new System.Resources.ResourceManager("Resources." + resourceName, System.Reflection.Assembly.Load("App_GlobalResources")));
        }




        public static T GetValue<T>(this Dictionary<string, object> list, string key)
        {
            object ret;
            if (list.TryGetValue(key, out ret)) return (T)ret;
            return default(T);
        }

        public static DateTime DayStart(this DateTime source)
        {
            DateTime dayStart = source.Date;
            return dayStart;
        }

        public static DateTime DayEnd(this DateTime source)
        {
            DateTime dayEnd = source.Date.AddDays(1).AddMilliseconds(-1);
            return dayEnd;
        }

    }
}