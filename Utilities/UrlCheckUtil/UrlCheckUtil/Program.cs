﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Netpay.Infrastructure;

namespace UrlCheckUtil
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter URL:");
            var url = Console.ReadLine();
            Console.WriteLine("Enter hash key:");
            var key = Console.ReadLine();
            Console.WriteLine();
            CheckSignature(url, key);
            Console.WriteLine();
            Console.WriteLine("Enter to exit");
            Console.ReadLine();
        }

        private static void CheckSignature(string url, string hashKey)
        {
            if (string.IsNullOrEmpty(url) || string.IsNullOrEmpty(hashKey))
                return;

            try
            {
                Uri uri = new Uri(url.Trim());
                string query = uri.GetComponents(UriComponents.Query, UriFormat.UriEscaped);
                NameValueCollection coll = HttpUtility.ParseQueryString(query);
                StringBuilder concat = new StringBuilder();
                foreach (string key in coll.AllKeys)
                {
                    if (key.ToLower() == "signature")
                        continue;
                    string val = coll.Get(key);
                    concat.Append(val);
                }
                //string merchantNumber = coll.Get("merchantID");
                //String hash = Netpay.Bll.Merchants.Merchant.Load(merchantNumber).HashKey;
                concat.Append(hashKey.Trim());

                string vals = concat.ToString();
                string calcSignature = vals.ToSha256().Trim();
                string sentSignature = coll.Get("signature").ToDecodedUrl().Trim();

                Console.WriteLine("Values: " + vals);
                Console.WriteLine("Sent signature: " + sentSignature);
                Console.WriteLine("Calc signature: " + calcSignature);

                if (calcSignature == sentSignature)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Valid");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid");
                }

                Console.ForegroundColor = ConsoleColor.Gray;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
