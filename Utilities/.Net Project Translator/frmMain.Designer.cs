﻿namespace TranslateNetProject
{
    partial class frmMain
    {
        private System.ComponentModel.IContainer components = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnToExcel = new System.Windows.Forms.Button();
            this.btnToResx = new System.Windows.Forms.Button();
            this.txtResxPath = new System.Windows.Forms.TextBox();
            this.txtExcelFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSelectResxDir = new System.Windows.Forms.Button();
            this.btnSelectExcelFile = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbLanguage = new System.Windows.Forms.ComboBox();
            this.fbdResx = new System.Windows.Forms.FolderBrowserDialog();
            this.ofdExcel = new System.Windows.Forms.OpenFileDialog();
            this.chkEncodeFileNames = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnToExcel
            // 
            this.btnToExcel.Location = new System.Drawing.Point(267, 91);
            this.btnToExcel.Name = "btnToExcel";
            this.btnToExcel.Size = new System.Drawing.Size(123, 23);
            this.btnToExcel.TabIndex = 10;
            this.btnToExcel.Text = "To Excel";
            this.btnToExcel.UseVisualStyleBackColor = true;
            this.btnToExcel.Click += new System.EventHandler(this.btnToExcel_Click);
            // 
            // btnToResx
            // 
            this.btnToResx.Location = new System.Drawing.Point(112, 91);
            this.btnToResx.Name = "btnToResx";
            this.btnToResx.Size = new System.Drawing.Size(123, 23);
            this.btnToResx.TabIndex = 9;
            this.btnToResx.Text = "To resx";
            this.btnToResx.UseVisualStyleBackColor = true;
            this.btnToResx.Click += new System.EventHandler(this.btnToResx_Click);
            // 
            // txtResxPath
            // 
            this.txtResxPath.Location = new System.Drawing.Point(112, 37);
            this.txtResxPath.Name = "txtResxPath";
            this.txtResxPath.Size = new System.Drawing.Size(278, 20);
            this.txtResxPath.TabIndex = 4;
            // 
            // txtExcelFileName
            // 
            this.txtExcelFileName.Location = new System.Drawing.Point(112, 64);
            this.txtExcelFileName.Name = "txtExcelFileName";
            this.txtExcelFileName.Size = new System.Drawing.Size(278, 20);
            this.txtExcelFileName.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Resx Directory";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Excel FileName";
            // 
            // btnSelectResxDir
            // 
            this.btnSelectResxDir.Location = new System.Drawing.Point(393, 38);
            this.btnSelectResxDir.Name = "btnSelectResxDir";
            this.btnSelectResxDir.Size = new System.Drawing.Size(37, 20);
            this.btnSelectResxDir.TabIndex = 5;
            this.btnSelectResxDir.Text = "...";
            this.btnSelectResxDir.UseVisualStyleBackColor = true;
            this.btnSelectResxDir.Click += new System.EventHandler(this.btnSelectResxDir_Click);
            // 
            // btnSelectExcelFile
            // 
            this.btnSelectExcelFile.Location = new System.Drawing.Point(393, 65);
            this.btnSelectExcelFile.Name = "btnSelectExcelFile";
            this.btnSelectExcelFile.Size = new System.Drawing.Size(37, 20);
            this.btnSelectExcelFile.TabIndex = 8;
            this.btnSelectExcelFile.Text = "...";
            this.btnSelectExcelFile.UseVisualStyleBackColor = true;
            this.btnSelectExcelFile.Click += new System.EventHandler(this.btnSelectExcelFile_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Language";
            // 
            // cmbLanguage
            // 
            this.cmbLanguage.FormattingEnabled = true;
            this.cmbLanguage.Items.AddRange(new object[] {
            ".he",
            ".de",
            ".lt"});
            this.cmbLanguage.Location = new System.Drawing.Point(112, 9);
            this.cmbLanguage.Name = "cmbLanguage";
            this.cmbLanguage.Size = new System.Drawing.Size(72, 21);
            this.cmbLanguage.TabIndex = 1;
            // 
            // ofdExcel
            // 
            this.ofdExcel.CheckFileExists = false;
            this.ofdExcel.DefaultExt = "xls";
            this.ofdExcel.ShowReadOnly = true;
            // 
            // chkEncodeFileNames
            // 
            this.chkEncodeFileNames.AutoSize = true;
            this.chkEncodeFileNames.Location = new System.Drawing.Point(276, 11);
            this.chkEncodeFileNames.Name = "chkEncodeFileNames";
            this.chkEncodeFileNames.Size = new System.Drawing.Size(119, 17);
            this.chkEncodeFileNames.TabIndex = 2;
            this.chkEncodeFileNames.Text = "Encoded File Name";
            this.chkEncodeFileNames.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 119);
            this.Controls.Add(this.chkEncodeFileNames);
            this.Controls.Add(this.cmbLanguage);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSelectExcelFile);
            this.Controls.Add(this.btnSelectResxDir);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtExcelFileName);
            this.Controls.Add(this.txtResxPath);
            this.Controls.Add(this.btnToResx);
            this.Controls.Add(this.btnToExcel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Translate .Net Project";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnToExcel;
        private System.Windows.Forms.Button btnToResx;
        private System.Windows.Forms.TextBox txtResxPath;
        private System.Windows.Forms.TextBox txtExcelFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelectResxDir;
        private System.Windows.Forms.Button btnSelectExcelFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbLanguage;
        private System.Windows.Forms.FolderBrowserDialog fbdResx;
        private System.Windows.Forms.OpenFileDialog ofdExcel;
        private System.Windows.Forms.CheckBox chkEncodeFileNames;
    }
}

