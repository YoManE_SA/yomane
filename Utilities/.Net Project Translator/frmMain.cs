﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

/******************************************************************************
 *Wrriten by Udi Azulay, Netpay, 2012
 ******************************************************************************/

namespace TranslateNetProject
{
    public partial class frmMain : Form
    {
        const string FieldNames_FileName = "fileName";
        const string FieldNames_ItemName = "itemName";
        const string FieldNames_TextValue = "textValue";
        const string FieldNames_TranslatedValue = "translatedValue";

        public frmMain() { InitializeComponent(); }
        protected string LanguageExt { get { return cmbLanguage.Text; } }
        protected string ResxPath { get { return txtResxPath.Text; } }
        protected string ExcelFileName { get { return txtExcelFileName.Text; } }
        protected bool EncodeFileNames { get { return chkEncodeFileNames.Checked; } }
        protected string ExcelConnectionString { get { return string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes;'", txtExcelFileName.Text); }  }
        private System.Data.OleDb.OleDbConnection _excelConnection;
        private System.Data.DataTable _excelDataTable;

        private void btnSelectResxDir_Click(object sender, EventArgs e)
        {
            fbdResx.ShowDialog(this);
			txtResxPath.Text = fbdResx.SelectedPath;
        }

        private void btnSelectExcelFile_Click(object sender, EventArgs e)
        {
            ofdExcel.ShowDialog(this);
			txtExcelFileName.Text = ofdExcel.FileName;
        }

        /********************************EXCEL SQL IO*********************************/
        public int execSql(string sqlString) 
        {
            using(var command = new System.Data.OleDb.OleDbCommand(sqlString, _excelConnection))
                return command.ExecuteNonQuery();
        }

        public void openConnection(bool create) 
        {
            if (create) {
                if (System.IO.File.Exists(ExcelFileName)) System.IO.File.Delete(ExcelFileName);
            }
            _excelConnection = new System.Data.OleDb.OleDbConnection(ExcelConnectionString);
            _excelConnection.Open();
            if (create)
            {
                if (EncodeFileNames) execSql(string.Format("Create Table Res({0} text, {1} text, {2} text)", FieldNames_TextValue, FieldNames_TranslatedValue, FieldNames_ItemName));
                else execSql(string.Format("Create Table Res({0} text, {1} text, {2} text, {3} text)", FieldNames_FileName, FieldNames_ItemName, FieldNames_TextValue, FieldNames_TranslatedValue));
            }
        }

        public void closeConnection() 
        {
            if (_excelConnection == null) return;
            if (_excelConnection.State != System.Data.ConnectionState.Closed) 
                _excelConnection.Close();
            _excelConnection.Dispose();
            _excelConnection = null;
        }

        /********************************ITEM ENCODEING*********************************/
        private string EncodeItemName(string fileName, string itemName) 
        {
            if (!EncodeFileNames) return itemName;
            return System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(string.Format("{0}:{1}", fileName, itemName)));
        }

        private string DecodeItemName(string itemName, out string fileName)
        {
            fileName = null;
            if (!EncodeFileNames) return itemName;
            string r = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(itemName));
            string[] vals = r.Split(':');
            fileName = vals[0];
            return vals[1];
        }

        /********************************FROM EXCEL*********************************/
        private void btnToResx_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "please note that this action miight overwrite exsiting files with empty ones, so please make sure the column translatedValue is filled", "", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK) return;
            try {
                openConnection(false);
                UpdateResxFiles();
                MessageBox.Show("Completed");
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Error");
            } finally {
                closeConnection();
            }
        }

        private string getResourceString()
        {
            using (System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("EmptyFile.xml"))
                using (System.IO.StreamReader reader = new System.IO.StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }        
        }

        private void saveXmlFile(System.Xml.XmlDocument doc, string fileName)
        {
            string ext = System.IO.Path.GetExtension(fileName);
            if (ext.Equals(".resx", StringComparison.OrdinalIgnoreCase)) {
                fileName = fileName.Remove(fileName.Length - ext.Length);
                ext = System.IO.Path.GetExtension(fileName);
            }
            if (ext.Length == 3)
                fileName = fileName.Remove(fileName.Length - ext.Length);
            if (fileName.StartsWith("\\")) fileName = fileName.Substring(1);
            fileName = System.IO.Path.Combine(ResxPath, fileName + LanguageExt + ".resx");

            string dir = System.IO.Path.GetDirectoryName(fileName);
            if (!System.IO.Directory.Exists(dir)) 
                System.IO.Directory.CreateDirectory(dir);
            doc.Save(fileName);
        }

        private void UpdateResxFiles() 
        {
            System.Xml.XmlDocument doc = null;
            var docList = new System.Collections.Generic.Dictionary<string, System.Xml.XmlDocument>();
            var command = new System.Data.OleDb.OleDbCommand("Select * From Res", _excelConnection);
            var reader = command.ExecuteReader();
            while (reader.Read()) {
                string itemName, fileName = null;
                if (EncodeFileNames) itemName = DecodeItemName((string)reader[FieldNames_ItemName], out fileName);
                else {
                    fileName = (string)reader[FieldNames_FileName];
                    itemName = (string)reader[FieldNames_ItemName];
                }
				if (fileName.StartsWith("\\")) fileName = fileName.Substring(1);
				if (!docList.ContainsKey(fileName)) 
                {
                    doc = new System.Xml.XmlDocument();
                    string fullFileName = System.IO.Path.Combine(ResxPath, fileName);
                    if (System.IO.File.Exists(fullFileName)) doc.Load(fullFileName);
                    else doc.LoadXml(TranslateNetProject.Properties.Resources.EmptyFile);
                    docList.Add(fileName, doc);
                }
                doc = docList[fileName];
                string value = (DBNull.Value == reader[FieldNames_TranslatedValue] ? string.Empty : (string)reader[FieldNames_TranslatedValue]);
                System.Xml.XmlElement item = doc.SelectSingleNode("//data[@name='" + itemName + "']") as System.Xml.XmlElement;
                if (item == null)
                {
                    var el = doc.CreateElement("data");
                    var att = doc.CreateAttribute("name");
                    att.Value = itemName;
                    el.Attributes.Append(att);
                    var valueEl = doc.CreateElement("value");
                    valueEl.InnerText = value;
                    el.AppendChild(valueEl);
                    doc.DocumentElement.AppendChild(el);
                }
                else item.GetElementsByTagName("value")[0].InnerText = value;
            }
            reader.Close(); reader.Dispose();
            command.Dispose();
            foreach(var item in docList)
                saveXmlFile(item.Value, item.Key);
        }
        
        /********************************TO EXCEL*********************************/
        private void btnToExcel_Click(object sender, EventArgs e)
        {
            try {
                _excelDataTable = new System.Data.DataTable();
                _excelDataTable.Columns.Add(FieldNames_FileName, typeof(string));
                _excelDataTable.Columns.Add(FieldNames_ItemName, typeof(string));
                _excelDataTable.Columns.Add(FieldNames_TextValue, typeof(string));
                AddDirToTable(ResxPath);
                _excelDataTable.DefaultView.Sort = FieldNames_ItemName;
                openConnection(true);
                SaveDataTableAsExcel();
                MessageBox.Show("Completed");
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Error");
            } finally {
                closeConnection();
                _excelDataTable.Dispose();
            }
        }


        private void AddDirToTable(string dirName)
        {
            var files = System.IO.Directory.GetFiles(dirName, string.Format("*{0}.resx", LanguageExt));
            foreach (var f in files)
            {
                if (string.IsNullOrEmpty(LanguageExt))
                {
                    string fileName = System.IO.Path.GetFileName(f);
                    fileName = fileName.Remove(fileName.Length - 5);
                    string ext = System.IO.Path.GetExtension(fileName);
                    if (ext.Length == 3) continue;
                }
                AddXmlFileToTable(f);
            }
            var dirs = System.IO.Directory.GetDirectories(dirName);
            foreach (var d in dirs) AddDirToTable(d);
        }

        private void AddXmlFileToTable(string fileName) 
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(fileName);
            var nodes = doc.GetElementsByTagName("data");
            foreach (System.Xml.XmlElement n in nodes)
            {
                if (n.ChildNodes.Count > 0)
                {
                    var row = _excelDataTable.NewRow();
                    row.ItemArray = new object[] { 
                        fileName.Substring(ResxPath.Length),
                        n.Attributes["name"].Value,
                        n.GetElementsByTagName("value")[0].InnerText
                    };
                    _excelDataTable.Rows.Add(row);
                }
            }
        }

        private void SaveDataTableAsExcel() 
        {
            var command = new System.Data.OleDb.OleDbCommand(null, _excelConnection);
            if (EncodeFileNames) command.CommandText = string.Format("Insert Into Res({0}, {1}) values(?, ?)", FieldNames_ItemName, FieldNames_TextValue);
            else command.CommandText = string.Format("Insert Into Res({0}, {1}, {2}) values(?, ?, ?)", FieldNames_FileName, FieldNames_ItemName, FieldNames_TextValue);
            foreach (System.Data.DataRow r in _excelDataTable.DefaultView.ToTable().Rows)
            {
                command.Parameters.Clear();
                if (EncodeFileNames) {
                    command.Parameters.Add(new System.Data.OleDb.OleDbParameter(FieldNames_ItemName, EncodeItemName((string)r[FieldNames_FileName.ToString()], (string)r[FieldNames_ItemName])));
                } else {
                    command.Parameters.Add(new System.Data.OleDb.OleDbParameter(FieldNames_FileName, r[FieldNames_FileName]));
                    command.Parameters.Add(new System.Data.OleDb.OleDbParameter(FieldNames_ItemName, r[FieldNames_ItemName]));
                }
                command.Parameters.Add(new System.Data.OleDb.OleDbParameter(FieldNames_TextValue, r[FieldNames_TextValue]));
                command.ExecuteNonQuery();
            }
            command.Dispose();
        }

    }
}
