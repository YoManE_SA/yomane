﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Netpay.Bll;
using System.Globalization;
using System.Threading;
using Netpay.Infrastructure;

namespace Netpay.PaymentPage
{
    public class RequestValidator : System.Web.Util.RequestValidator
    {
        public static System.Collections.Generic.HashSet<string> IgnoredParams = new System.Collections.Generic.HashSet<string>(new string[] { "disp_payfor" } );
        internal static bool IsDangerousString(string value, out int matchIndex)
        {
            matchIndex = 0;
            int startIndex = 0;
            while (true)
            {
                int curIndex = value.IndexOfAny(new char[] { '<', '&' }, startIndex);
                if (curIndex < 0) break;
                if (curIndex == value.Length - 1) return false;
                matchIndex = curIndex;
                char c = value[curIndex], c1 = value[curIndex + 1];
                if (c == '<' && ( ( (c1 >= 'a' && c1 <= 'z') || (c1 >= 'A' && c1 <= 'Z') ) || c1 == '!' || c1 == '/' || c1 == '?') ) {
                    string testValue = value.Substring(curIndex, value.Length - curIndex > 10 ? 10 : value.Length - curIndex).ToLower().Replace(" ", "");
                    if (!
                        (testValue.StartsWith("<br/>") || testValue.StartsWith("<br>") || testValue.StartsWith("<strong>") || testValue.StartsWith("</strong>")) 
                       ) return true;
                } else if (c == '&' && c1 == '#') return true;
                startIndex = curIndex + 1;
            }
            return false;
        }
        
        protected override bool IsValidRequestString(HttpContext context, string value, System.Web.Util.RequestValidationSource requestValidationSource, string collectionKey, out int validationFailureIndex)
        {
            if (collectionKey != null && requestValidationSource == System.Web.Util.RequestValidationSource.QueryString || requestValidationSource == System.Web.Util.RequestValidationSource.Form) 
                if (IgnoredParams.Contains(collectionKey.ToLower())) 
                    return !IsDangerousString(value, out validationFailureIndex);
            return base.IsValidRequestString(context, value, requestValidationSource, collectionKey, out validationFailureIndex);
        }
    }

    public class Global : System.Web.HttpApplication
	{
        void Application_BeginRequest(object sender, EventArgs e)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3 | System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;

            var app = (HttpApplication)sender;
            if (app.Context.Request.Url.LocalPath.EndsWith("/"))
                app.Context.RewritePath(string.Concat(app.Context.Request.Url.LocalPath, "default.aspx")); //fix default doc post on integrated iis with runallmodules

            if (app != null && app.Context != null)
            {
                app.Context.Response.Headers.Remove("Server");
            }
        }


        void Application_PreRequestHandlerExecute(object sender, EventArgs e)
		{
			//Netpay.Web.WebUtils.SelectLanguage(null);
			bool bUpdateCookie = true;
			HttpCookie requestCookie = Request.Cookies["UICulture"];
			string culture = Request.QueryString["culture"];
			if (string.IsNullOrEmpty(culture) && (Request.Form["__EVENTTARGET"].EmptyIfNull().EndsWith("LanguageSelector"))) culture = Request["__EVENTARGUMENT"];

			if (string.IsNullOrEmpty(culture)) culture = Request["disp_lng"];
			if (string.IsNullOrEmpty(culture)) {
				if (requestCookie != null) culture = requestCookie.Value;
			}
			if (string.IsNullOrEmpty(culture)) {
				string disp_lngList = Request["disp_lngList"].EmptyIfNull();
				if (disp_lngList.Length >= 5) culture = disp_lngList.Substring(0, 5);
				else culture = "en-us";
			}
			if (requestCookie != null) bUpdateCookie = requestCookie.Value != culture;
			try {
				Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
				if (bUpdateCookie){
					requestCookie = new HttpCookie("UICulture", culture);
					Response.Cookies.Set(requestCookie);
				}
			} catch {
				Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
			}
		}

		void Application_Start(object sender, EventArgs e)
		{
			Web.ContextModule.AppName = "Payment Page";
		}

		void Application_End(object sender, EventArgs e)
		{
			//  Code that runs on application shutdown
		}

		void Application_Error(object sender, EventArgs e)
		{
			Exception ex = Server.GetLastError();
            if (ex is System.Web.HttpRequestValidationException) {
                Response.Redirect("~/Error.aspx?message=" + Server.UrlEncode("one of the parameters contains HTML"), true);
                return;
            }
            Logger.Log(LogTag.PaymentPage, ex);
            System.Configuration.Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
			System.Web.Configuration.CompilationSection configSection = config.GetSection("system.web/compilation") as System.Web.Configuration.CompilationSection;
			if (!configSection.Debug){
				Response.Write("Unexpected system error occurred.");
				Response.End();
			}
		}

		void Application_AuthorizeRequest(object sender, EventArgs e)
		{
			lock (this) Infrastructure.Module.GetCache(); //just make sure it loads
		}

		void Application_AcquireRequestState(object sender, EventArgs e)
		{
			if (Request.IsSecureConnection) return;
			if (Request.ServerVariables["HTTP_HOST"].StartsWith("192.168") || Request.ServerVariables["HTTP_HOST"] == "127.0.0.1"
				|| Request.ServerVariables["HTTP_HOST"] == "localhost" || Request.ServerVariables["HTTP_HOST"].StartsWith("80.179.180.")
				|| Request.ServerVariables["HTTP_HOST"] == "merchantstest.netpay-intl.com") return;
			if (Netpay.Web.WebUtils.CurrentDomain.ForceSSL) Response.Redirect("https://" + Request.Url.ToString().Substring(7), true);
		}

		void Session_Start(object sender, EventArgs e) { }
		void Session_End(object sender, EventArgs e) { }
	}
}
