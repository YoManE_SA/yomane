//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Main {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Main() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Main", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to StyleEng.css.
        /// </summary>
        internal static string cssFile {
            get {
                return ResourceManager.GetString("cssFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to day.
        /// </summary>
        internal static string Day {
            get {
                return ResourceManager.GetString("Day", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to days.
        /// </summary>
        internal static string Days {
            get {
                return ResourceManager.GetString("Days", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error.
        /// </summary>
        internal static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A server problem occurred when trying to process the request.
        /// </summary>
        internal static string ErrorCommunication {
            get {
                return ResourceManager.GetString("ErrorCommunication", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot proceed, merchant closed or blocked. 
        ///	 Please consult the support at the referring site..
        /// </summary>
        internal static string ErrorMerchantClosed {
            get {
                return ResourceManager.GetString("ErrorMerchantClosed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot proceed, merchant is not authorized to use this service. 
        ///	 Please consult the support at the referring site..
        /// </summary>
        internal static string ErrorMerchantUnauthorized {
            get {
                return ResourceManager.GetString("ErrorMerchantUnauthorized", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot proceed, merchant is not allowed to process autorization transactions. 
        ///	 Please consult the support at the referring site..
        /// </summary>
        internal static string ErrorNoApproval {
            get {
                return ResourceManager.GetString("ErrorNoApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to At the moment we do not support the requested country.
        /// </summary>
        internal static string ErrorNoMethodForCountry {
            get {
                return ResourceManager.GetString("ErrorNoMethodForCountry", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Merchant is not authorized to set a recurring transaction. 
        ///	 Please consult the support at the referring site..
        /// </summary>
        internal static string ErrorNoRecurring {
            get {
                return ResourceManager.GetString("ErrorNoRecurring", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot proceed, missing or invalid amount. 
        ///	 Please consult the support at the referring site..
        /// </summary>
        internal static string ErrorParameterAmount {
            get {
                return ResourceManager.GetString("ErrorParameterAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot proceed, missing or invalid company number. 
        ///	 Please consult the support at the referring site..
        /// </summary>
        internal static string ErrorParameterCompanyNum {
            get {
                return ResourceManager.GetString("ErrorParameterCompanyNum", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot proceed, missing or invalid currency. 
        ///	 Please consult the support at the referring site..
        /// </summary>
        internal static string ErrorParameterCurrency {
            get {
                return ResourceManager.GetString("ErrorParameterCurrency", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot proceed, missing or invalid signature. 
        ///	 Please consult the support at the referring site..
        /// </summary>
        internal static string ErrorParameterSign {
            get {
                return ResourceManager.GetString("ErrorParameterSign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot proceed, missing or invalid trans type. 
        ///	 Please consult the support at the referring site..
        /// </summary>
        internal static string ErrorParameterTransType {
            get {
                return ResourceManager.GetString("ErrorParameterTransType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request length exceeded the limit.
        /// </summary>
        internal static string ErrorRequestlength {
            get {
                return ResourceManager.GetString("ErrorRequestlength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your session has timed out. Please open this page again..
        /// </summary>
        internal static string ErrorSessionEnded {
            get {
                return ResourceManager.GetString("ErrorSessionEnded", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This requested payment solution is not enabled. 
        ///	Please consult the support at the referring site..
        /// </summary>
        internal static string ErrorSolutionDisabled {
            get {
                return ResourceManager.GetString("ErrorSolutionDisabled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to  Cannot proceed, The following field/s are empty or invalid: .
        /// </summary>
        internal static string FieldError {
            get {
                return ResourceManager.GetString("FieldError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Installments.
        /// </summary>
        internal static string Installments {
            get {
                return ResourceManager.GetString("Installments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login.
        /// </summary>
        internal static string Login {
            get {
                return ResourceManager.GetString("Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Logout.
        /// </summary>
        internal static string Logout {
            get {
                return ResourceManager.GetString("Logout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to %LEGAL_NAME% - Complete Online Payment Solutions.
        /// </summary>
        internal static string ltBottomLine {
            get {
                return ResourceManager.GetString("ltBottomLine", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to month.
        /// </summary>
        internal static string Month {
            get {
                return ResourceManager.GetString("Month", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to months.
        /// </summary>
        internal static string Months {
            get {
                return ResourceManager.GetString("Months", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Next &gt;&gt;.
        /// </summary>
        internal static string Next {
            get {
                return ResourceManager.GetString("Next", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to is the authorized payment processor for.
        /// </summary>
        internal static string PayByMsg {
            get {
                return ResourceManager.GetString("PayByMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PAY FOR.
        /// </summary>
        internal static string payFor {
            get {
                return ResourceManager.GetString("payFor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please complete payment in the opened window.
        /// </summary>
        internal static string PayInNewWindow {
            get {
                return ResourceManager.GetString("PayInNewWindow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payment Method.
        /// </summary>
        internal static string PaymentMethod {
            get {
                return ResourceManager.GetString("PaymentMethod", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payment Solutions.
        /// </summary>
        internal static string PaymentSolutions {
            get {
                return ResourceManager.GetString("PaymentSolutions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to quarter.
        /// </summary>
        internal static string Quarter {
            get {
                return ResourceManager.GetString("Quarter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to quarters.
        /// </summary>
        internal static string Quarters {
            get {
                return ResourceManager.GetString("Quarters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to And then .
        /// </summary>
        internal static string RecurringAnd {
            get {
                return ResourceManager.GetString("RecurringAnd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to And then.
        /// </summary>
        internal static string RecurringLastAnd {
            get {
                return ResourceManager.GetString("RecurringLastAnd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to a single charge within {2} {1}, {3}.
        /// </summary>
        internal static string RecurringSingle {
            get {
                return ResourceManager.GetString("RecurringSingle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to a single charge within {2} {1}.
        /// </summary>
        internal static string RecurringSingleNoAmount {
            get {
                return ResourceManager.GetString("RecurringSingleNoAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} charges every {2} {1}, {3} each charge.
        /// </summary>
        internal static string RecurringText {
            get {
                return ResourceManager.GetString("RecurringText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} charge every {2} {1}, {3} each charge.
        /// </summary>
        internal static string RecurringTextCont {
            get {
                return ResourceManager.GetString("RecurringTextCont", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} charges every {2} {1}.
        /// </summary>
        internal static string RecurringTextNoAmount {
            get {
                return ResourceManager.GetString("RecurringTextNoAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to * Required fields.
        /// </summary>
        internal static string RequireMsg {
            get {
                return ResourceManager.GetString("RequireMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This Page is Secure.
        /// </summary>
        internal static string SecuredPage {
            get {
                return ResourceManager.GetString("SecuredPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use other payment option.
        /// </summary>
        internal static string SelectPayingMethod {
            get {
                return ResourceManager.GetString("SelectPayingMethod", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to %LEGAL_NAME% - Complete Online Payment Solutions.
        /// </summary>
        internal static string Slogan {
            get {
                return ResourceManager.GetString("Slogan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street address, P.O. box, company name, c/o.
        /// </summary>
        internal static string TipAddressLine1 {
            get {
                return ResourceManager.GetString("TipAddressLine1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Apartment, suite, unit, building, floor, etc..
        /// </summary>
        internal static string TipAddressLine2 {
            get {
                return ResourceManager.GetString("TipAddressLine2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MasterCard (CVC2) and Visa (CVV2) have a 3 digit located on the back of the card. 
        ///	 American Express (CID) has a 4 digit code, located on the front of the card..
        /// </summary>
        internal static string TipCvv {
            get {
                return ResourceManager.GetString("TipCvv", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total.
        /// </summary>
        internal static string Total {
            get {
                return ResourceManager.GetString("Total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shipping address is same as billing address.
        /// </summary>
        internal static string UseShippingSameAsBilling {
            get {
                return ResourceManager.GetString("UseShippingSameAsBilling", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wallet.
        /// </summary>
        internal static string Wallet {
            get {
                return ResourceManager.GetString("Wallet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are logged in as %FIRST%%LAST%..
        /// </summary>
        internal static string WalletUserIsLoggedIn {
            get {
                return ResourceManager.GetString("WalletUserIsLoggedIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to week.
        /// </summary>
        internal static string Week {
            get {
                return ResourceManager.GetString("Week", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to weeks.
        /// </summary>
        internal static string Weeks {
            get {
                return ResourceManager.GetString("Weeks", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to year.
        /// </summary>
        internal static string Year {
            get {
                return ResourceManager.GetString("Year", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to years.
        /// </summary>
        internal static string Years {
            get {
                return ResourceManager.GetString("Years", resourceCulture);
            }
        }
    }
}
