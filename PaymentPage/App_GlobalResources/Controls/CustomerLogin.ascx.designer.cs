//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources.CustomerLogin {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ascx {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ascx() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.CustomerLogin.ascx", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pay!.
        /// </summary>
        internal static string ButtonPay {
            get {
                return ResourceManager.GetString("ButtonPay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Card holder.
        /// </summary>
        internal static string CardHolder {
            get {
                return ResourceManager.GetString("CardHolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current balance.
        /// </summary>
        internal static string CurrentBalance {
            get {
                return ResourceManager.GetString("CurrentBalance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid email address specified.
        /// </summary>
        internal static string EmailAddressIsInvalid {
            get {
                return ResourceManager.GetString("EmailAddressIsInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter an email address.
        /// </summary>
        internal static string EmailAddressIsMissing {
            get {
                return ResourceManager.GetString("EmailAddressIsMissing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maximum login failures allowed has been exceeded, Your account is blocked for the next 30 minutes, Try again after this time period elapse..
        /// </summary>
        internal static string FailedAttemptsBlocked {
            get {
                return ResourceManager.GetString("FailedAttemptsBlocked", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login Details.
        /// </summary>
        internal static string lblLoginDetails {
            get {
                return ResourceManager.GetString("lblLoginDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account provider.
        /// </summary>
        internal static string ltAppIdentity {
            get {
                return ResourceManager.GetString("ltAppIdentity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Address.
        /// </summary>
        internal static string ltLoginName {
            get {
                return ResourceManager.GetString("ltLoginName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        internal static string ltPassword {
            get {
                return ResourceManager.GetString("ltPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter a password.
        /// </summary>
        internal static string PasswordIsMissing {
            get {
                return ResourceManager.GetString("PasswordIsMissing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select currency to pay by balance.
        /// </summary>
        internal static string SelectBalance {
            get {
                return ResourceManager.GetString("SelectBalance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select credit card to pay with.
        /// </summary>
        internal static string SelectCard {
            get {
                return ResourceManager.GetString("SelectCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter a username.
        /// </summary>
        internal static string UsernameIsMissing {
            get {
                return ResourceManager.GetString("UsernameIsMissing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wrong username or password.
        /// </summary>
        internal static string UserNotFound {
            get {
                return ResourceManager.GetString("UserNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Valid until.
        /// </summary>
        internal static string ValidUntil {
            get {
                return ResourceManager.GetString("ValidUntil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wrong username or password.
        /// </summary>
        internal static string WrongPassword {
            get {
                return ResourceManager.GetString("WrongPassword", resourceCulture);
            }
        }
    }
}
