//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources.Hosted.V3.Default {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class aspx {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal aspx() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Hosted.V3.Default.aspx", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This card has expired.
        /// </summary>
        internal static string cardHasExpired {
            get {
                return ResourceManager.GetString("cardHasExpired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid credit card.
        /// </summary>
        internal static string invalidCard {
            get {
                return ResourceManager.GetString("invalidCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Next &gt;&gt;.
        /// </summary>
        internal static string nextButton {
            get {
                return ResourceManager.GetString("nextButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address is required.
        /// </summary>
        internal static string requiredAddress {
            get {
                return ResourceManager.GetString("requiredAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cardholder name is required.
        /// </summary>
        internal static string requiredCardholderName {
            get {
                return ResourceManager.GetString("requiredCardholderName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to City is required.
        /// </summary>
        internal static string requiredCity {
            get {
                return ResourceManager.GetString("requiredCity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A valid cvv is required.
        /// </summary>
        internal static string requiredCvv {
            get {
                return ResourceManager.GetString("requiredCvv", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A valid email is required.
        /// </summary>
        internal static string requiredEmail {
            get {
                return ResourceManager.GetString("requiredEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A valid expiration month is required.
        /// </summary>
        internal static string requiredExpMonth {
            get {
                return ResourceManager.GetString("requiredExpMonth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A valid expiration year is required.
        /// </summary>
        internal static string requiredExpYear {
            get {
                return ResourceManager.GetString("requiredExpYear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zipcode is required.
        /// </summary>
        internal static string requiredZipcode {
            get {
                return ResourceManager.GetString("requiredZipcode", resourceCulture);
            }
        }
    }
}
