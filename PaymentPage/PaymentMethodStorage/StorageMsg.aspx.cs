﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using System.Text;
using Netpay.Bll;
using Netpay.Infrastructure;
using System.Net;
using System.IO;
using System.Collections.Specialized;

namespace Netpay.PaymentPage.PaymentMethodStorage
{
	public partial class StorageMsg : Code.HostedPageBase
    {
		private string replyCode, replyDescription, storage_id, storage_date;
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (Data == null) RaiseError(Resources.Main.ErrorSessionEnded);
			NameValueCollection responseValues = Request.QueryString;
			replyCode = responseValues["replyCode"];
			replyDescription = responseValues["replyDesc"];
			storage_id = responseValues["storage_ID"];
			storage_date = responseValues["storage_date"];

			TransDate.Text = storage_date;
			TransID.Text = storage_id;
			mvReply.ActiveViewIndex = 0;
			if (replyCode == "00") lbSucceededText.Text = "The storage completed successfully.";
			else {
				lbFailedText.Text = string.Format("Error ({0}): {1}", replyCode, replyDescription);
				mvReply.ActiveViewIndex = 1;
			}
		}

		protected void btnFinish_Click(object sender, EventArgs e)
		{
			RedirectClient(replyCode, replyDescription, 0, ConvertRCDateString(storage_date), null, storage_id.ToInt32(0), null, 0);
		}
		
		protected void btnTryAgain_Click(object sender, EventArgs e)
		{
			Redirect("~/hosted/default.aspx");
		}
	}
}
