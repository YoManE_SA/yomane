﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.PaymentPage.PaymentMethodStorage
{
	public partial class Default : Code.HostedPageBase
	{
        protected override void InitHostedContext()
        {
            var req = HttpContext.Current.Request;
            if (req["merchantID"] != null && !IsPostBack)
                Init("~/PaymentMethodStorage/default.aspx", false);
            else base.InitHostedContext();
        }

	}
}