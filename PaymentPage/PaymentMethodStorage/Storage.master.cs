﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Bll;
using Netpay.Infrastructure;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.PaymentPage.PaymentMethodStorage
{
	public partial class Storage : Netpay.PaymentPage.Code.MasterPageBase
	{
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (!IsPostBack)
			{
				trPayFor.Visible = (Data != null);
				if (Data != null)
				{
					ltPayForText.Text = Resources.Storage.master.StoreText.ToEncodedHtml();
				}
			}
		}
	}
}