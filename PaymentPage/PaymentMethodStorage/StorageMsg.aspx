﻿<%@ Page Title="" Language="C#" MasterPageFile="Storage.master" AutoEventWireup="true" CodeBehind="StorageMsg.aspx.cs" Inherits="Netpay.PaymentPage.PaymentMethodStorage.StorageMsg" %>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<br /><br />
	<table class="FormView">
		<tr>
			<td>
				 <asp:MultiView runat="server" ID="mvReply">
					<asp:View runat="server">
						<asp:Label runat="server" ID="lbSucceededText" Font-Size="14px" ForeColor="#009900" />
						&nbsp;<br /><br />
						<asp:Literal runat="server" Text="<%$Resources:Storage.PaymentMsg.aspx,ltTransID %>" /> : <asp:Label runat="server" ID="TransID" />
						<br />
						<asp:Literal runat="server" Text="<%$Resources:Storage.PaymentMsg.aspx,ltTransDate %>" />: <asp:Label runat="server" ID="TransDate" />
						<br /><br /><br />
						<asp:Button runat="server" ID="btnFinish" Text="<%$Resources:Storage.PaymentMsg.aspx,btnFinish%>" OnClick="btnFinish_Click" CssClass="Button" style="width:250px;" />
					</asp:View>
					<asp:View runat="server">
						<asp:Label runat="server" ID="lbFailedText" Font-Size="14px" ForeColor="#CC0000" />
						&nbsp;<br /><br />
						<asp:LinkButton runat="server" ID="btnTryAgain" Text="<%$Resources:Storage.PaymentMsg.aspx,btnTryAgain%>" OnClick="btnTryAgain_Click" />
					</asp:View>
				 </asp:MultiView>
			</td>
		</tr>
	</table>
	<br />

</asp:Content>
