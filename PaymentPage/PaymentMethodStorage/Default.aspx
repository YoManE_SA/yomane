﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaymentMethodStorage/Storage.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Netpay.PaymentPage.PaymentMethodStorage.Default" %>
<%@ Register Src="~/Designs/New/Controls/CreditCardForm.ascx" TagPrefix="netpay" TagName="CreditCardForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
				<netpay:CreditCardForm runat="server" ID="ctlCreditCardForm" StorageMode="True" />
</asp:Content>
