﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Web;
using System.Text;
using Netpay.Bll;
using Netpay.Infrastructure;

namespace Netpay.PaymentPage.Public
{
	public partial class DefaultAjax : HostedPageBase
    {
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				//wcElementBlocker.Visible = !Options.IsPublicPay;
				if (ShopSettings == null) RaiseError(Resources.Main.ErrorSolutionDisabled);
				if (ShopSettings.IsAllowDynamicProduct)
				{
					phPymentOptionCustom.Visible = true;
					//ddCurrency.IncludeCurrencyIDs = Options.;
				}
				//phPymentOptionSelection.Visible = true;
				List<Bll.Shop.Products.Product> chargeOptions = Bll.Shop.Products.Product.Search(new Bll.Shop.Products.Product.SearchFilters() { MerchantId = Merchant.ID, LanguageIso = System.Globalization.CultureInfo.CurrentUICulture.Name }, null);
				//repeaterPaymentOptions.DataSource = chargeOptions;
				//repeaterPaymentOptions.DataBind();
				if (Request["Item"] != null) // auto select and redirect
					Response.Redirect(string.Format("Product.aspx?merchantID={0}&Item={1}", Merchant.Number, Request["Item"]));
					//MakePayment(Request["Item"].ToInt32(), Request["quantity"].ToInt32(1), Request["installments"].ToInt32(1)); 
			}

			if (Request["makePayment"] != null && Request["makePayment"] == "true") 
			{
				int itemId = Request["itemId"].ToInt32();
				int quantity = Request["quantitySelect" + itemId].ToInt32(1);
				int installments = Request["installmentsSelect" + itemId].ToInt32(1);
				MakePayment(itemId, quantity, installments);
			}

            acTags.AdditionalParams.Add("merchantNumber", Merchant.Number);
		}

		protected void Pay_CommandCustom(object sender, CommandEventArgs e)
		{
			if (!IsValid) return;
			MakePayment(-1, 1, 1);
		}

		protected void MakePayment(int itemId, int quantity, int installments)
		{
			decimal chargeAmount = 0;
			string chargeCurrency;
			string itemText = "open payment";
			if (itemId == -1) {
				chargeAmount = txtAmount.Text.ToDecimal(0);
				chargeCurrency = ddCurrency.SelectedCurrencyIso;
			} else {
				var item = Bll.Shop.Products.Product.Load(itemId);
				if (item == null) return;
				chargeAmount = item.Price * quantity;
				chargeCurrency = item.CurrencyISOCode ;
				itemText = string.Format("{0} x {1}", quantity, item.GetTextForLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name).Name);
			}
			var currency = Bll.Currency.Get(chargeCurrency);
			string signature = Data.CreateSignature(SignatureType.MD5, null, Merchant.Number + chargeAmount.ToAmountFormat() + currency.IsoCode + Merchant.HashKey);
			string finishPage = "", notifyPage = "";
			Response.Redirect(
				WebUtils.CurrentDomain.ProcessV2Url +
				"Hosted/?merchantID=" + Merchant.Number +
				"&trans_amount=" + chargeAmount.ToAmountFormat() +
				"&trans_currency=" + currency.IsoCode +
				"&trans_type=" + (false ? 1 : 0) +
				"&trans_order=" + itemId +
				"&signature=" + HttpUtility.UrlEncode(signature) +
				"&trans_installments=" + installments +
				"&public_itemId=" + itemId +
				"&public_quantity=" + quantity +
				"&disp_payFor=" + HttpUtility.UrlEncode(itemText) +
				"&url_notify=" + notifyPage +
				"&url_redirect=" + finishPage 
				, true);
		}

		protected string getItemOnChangeJsEvent(Bll.Shop.Products.Product ppco)
		{
			return string.Format("onQuantityCharge(this, {0}, {1}, '{2}')", ppco.ID, ppco.Price, 0d.ToAmountFormat(ppco.CurrencyISOCode));
		}

		protected void CustomCharge_Change(object sender, EventArgs e)
		{
			dvCustomPrice.InnerHtml = txtAmount.Text.ToDecimal(0).ToAmountFormat(ddCurrency.SelectedValue.ToInt32(0));
		}

	}
}