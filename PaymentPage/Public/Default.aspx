﻿<%@ Page Language="C#" MasterPageFile="~/Public/Public.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Netpay.PaymentPage.Public.Default" Title="Shops" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <script type="text/jscript">
        function onQuantityCharge(sender, itemId, itemPrice, itemFormat) {
            var quantity = $(sender).val();
            document.getElementById('dvPrice' + itemId).innerHTML = itemFormat.replace('0', (itemPrice * quantity));
        }

        function getLinkUrl() {
            var merchantId = netpay.Common.fromQueryString(location.search)["?merchantID"];
            var tags = encodeURI($("#ctl00_ctl00_body_body_acTags_text").val().trim());
            var link = location.protocol + "//" + location.host + location.pathname + "?merchantID=" + merchantId + "&search=" + tags;
            return link;
        }
    </script>

    <div id="search" style="visibility:hidden">
        <netpay:AutoComplete ID="acTags" IsMultiselect="true" Function="GetPublicPageProductTagAutocomplete" TagSeperator=" " runat="server" />
        <asp:Button ID="btnSearch" CssClass="button-search" Text="GO" OnClick="Search" runat="server" />
    </div>

    <div class="Public-head" style="visibility: hidden">
        <img src="/NPCommon/Images/Arrow01<%=Netpay.Web.WebUtils.CurrentReverseAlignment%>.gif" alt="" />
        <asp:Localize ID="Localize1" Text="<%$ Resources: Public.Default.aspx,  paymentOptions %>" runat="server" />
    </div>
    <div class="spacer"></div>


    <div id="public-page" class="col-md-12 col-sm-12 col-xs-12">
        <asp:Repeater ID="repeaterPaymentOptions" runat="server" OnItemDataBound="PaymentOptions_ItemDataBound" OnItemCommand="RepeaterItemCommand">
            <ItemTemplate>
                <div class="col-md-3 col-sm-6 col-xs-12">

                    <div class="section-public">
                        <div class="public-left">
                            <asp:Image runat="server" ImageUrl='<%# Merchant.MapPublicVirtualPath(((Netpay.Bll.Shop.Products.Product)Container.DataItem).ImageFileName) %>' Visible='<%# !string.IsNullOrEmpty(((Netpay.Bll.Shop.Products.Product)Container.DataItem).ImageFileName) %>' class="pic-box" />

                            <div class="spacer"></div>

                        </div>
                        <div class="public-right">

                            <div class="title-public">
                                
                                    <%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).GetTextForLanguage(null).Name.ToUpper() %>

                                    <div class="spacer"></div>
                                    <span class="currency-symbol"><%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).CurrencyISOCode %></span>
                                    <span id="spanDynamicAmount" style="display: <%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).IsDynamicProduct ? "inline" : "none" %>;" class="price">
                                        <asp:TextBox ID="txtDynamicAmount" runat="server"></asp:TextBox></span>
                                    <span class="price" style="display: <%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).IsDynamicProduct ? "none" : "inline" %>;" id="dvPrice<%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).ID %>"><%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).Price.ToAmountFormat() %></span>
                                    <asp:RequiredFieldValidator ID="valdatorAmountRequired" ControlToValidate="txtDynamicAmount" ErrorMessage="<%$ Resources: Public.Default.aspx, productAmountRequiredValidation %>" runat="server"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="validatorAmountNumber" ValidationExpression="^[1-9]\d*(\.\d+)?$" ControlToValidate="txtDynamicAmount" ErrorMessage="<%$ Resources: Public.Default.aspx, productAmountValidation %>" runat="server"></asp:RegularExpressionValidator>

                                
                                <span class="read-more" onclick="$('#ItemDescription<%#Container.ItemIndex %>').slideDown('slow');"></span>



                                


                            </div>
                            <div class="Quantity" style="display: <%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).IsDynamicProduct ? "none" : "inline" %>;">

                                    <asp:Literal runat="server" ID="ltQuantity" Text="<%$ Resources: Public.Default.aspx,  quantity %>" />
                                    <netpay:NumericDropDown runat="server" ID="ddlQuantity" MinValue="<%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).QtyStart %>" MaxValue="<%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).QtyEnd %>" ValueStep="<%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).QtyStep %>" onchange="<%#getItemOnChangeJsEvent( (Netpay.Bll.Shop.Products.Product)Container.DataItem )%>" />

                                    <asp:Literal runat="server" ID="ltInstallments" Text="<%$ Resources: Public.Default.aspx,  payments %>" />
                                    <netpay:NumericDropDown runat="server" ID="ddlInstallments" MinValue="1" MaxValue="<%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).Installments %>" />
                                    <div class="spacer"></div> 
                                </div>
                            <div class="text-paragraph">
                                <p><%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).GetTextForLanguage(null).Description %></p>
                            </div>

                            <div class="sku-wrap" style="display: <%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).IsDynamicProduct ? "none" : "inline" %>;">
                                <asp:Localize ID="Localize3" Text="<%$ Resources: Public.Default.aspx,  Sku %>" runat="server" />


                                <%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).SKU %> (<%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).QtyAvailable %>
                                <asp:Localize ID="Localize4" Text="<%$ Resources: Public.Default.aspx,  Avaliable %>" runat="server" />)
                               
                                
                                  

                            </div>  <div class="spacer"></div>
                                    <div class="spacer"></div>
                                     

                                    <span class=" wrap-pursace pursace checkbox">
                                        <%--<asp:LinkButton ID="btnPurchase" CssClass="button-pay" runat="server" Text="<%$ Resources: Public.Default.aspx,  Purchase %>" CommandName="Pay" CommandArgument="<%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).ID %>" />--%>

                                        <asp:LinkButton ID="btnPurchase" CssClass="button-pay" runat="server" Text="PURCHASE" CommandName="Pay" CommandArgument="<%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).ID %>" />
                                        <asp:LinkButton ID="btnAddToCart" Visible="false" CssClass="button-pay" runat="server" Text="<%$ Resources: Public.Default.aspx,  addToCart %>" CommandName="AddToCart" CommandArgument="<%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).ID %>" />
                                    </span>

                        </div>
                        <div class="spacer"></div>
                        <!-- Push Read more -->
                        <div class="description" id="ItemDescription<%#Container.ItemIndex %>">
                            <div class="title-read-more"><%# ((Netpay.Bll.Shop.Products.Product)Container.DataItem).GetTextForLanguage(null).Name %></div>
                            <div class="close-box" onclick="$('#ItemDescription<%#Container.ItemIndex %>').slideUp('slow');">
                                <img alt="" src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/close.png" />
                            </div>
                            <div class="spacer"></div>

                            <div class="spacer"></div>
                            <div class="spacer"></div>
                        </div>
                        <!-- End Read more -->
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>

</asp:Content>
