﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Web;
using System.Text;
using Netpay.Bll;
using Netpay.Infrastructure;
using System.Web.UI.HtmlControls;

namespace Netpay.PaymentPage.Public
{
	public partial class Default : PublicBasePage
    {

        protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
                if (ShopSettings == null) RaiseError(Resources.Main.ErrorSolutionDisabled);
				if (Request["search"] != null)
					acTags.SelectedText = Request["search"];
				Search(null, null);

			}

            acTags.AdditionalParams.Add("merchantNumber", Merchant.Number);
            //TODO: PMugisha testing
            var imgProfile = Merchant.ProfileImageFileName;
            (this.Master as Main).SetMerchantLogo(@"/NPCommon/Images/merchant_logo.png");
		}

        protected void Search(object sender, EventArgs e) 
        {
			var tags = acTags.SelectedText.EmptyIfNull().Trim();
            if (tags.Length == 0)
            {
				List<Bll.Shop.Products.Product> chargeOptions = Bll.Shop.Products.Product.Search(new Bll.Shop.Products.Product.SearchFilters() { MerchantId = Merchant.ID, LanguageIso = System.Globalization.CultureInfo.CurrentUICulture.Name, PromoOnly = true }, null);
                repeaterPaymentOptions.DataSource = chargeOptions;
                repeaterPaymentOptions.DataBind();
            }
            else
            {
				var sf = new Bll.Shop.Products.Product.SearchFilters() { MerchantId = Merchant.ID };
				sf.Tags =  tags.Split(' ').ToList();
				var results = Bll.Shop.Products.Product.Search(sf, new SortAndPage(0, 10));
                repeaterPaymentOptions.DataSource = results;
                repeaterPaymentOptions.DataBind();
            }
        }

		protected void RepeaterItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			int paymentOptionID = e.CommandArgument.ToString().ToInt32(-1);
			var option = Bll.Shop.Products.Product.Load(paymentOptionID);

			int quantity = 0;
			decimal amount = 0;
			if (option.IsDynamicProduct)
			{
				quantity = 1;
				amount = (e.Item.FindControl("txtDynamicAmount") as TextBox).Text.ToDecimal(0);
			}
			else 
			{
				quantity = (e.Item.FindControl("ddlQuantity") as DropDownList).SelectedValue.ToInt32(1);
				amount = option.Price * quantity;
			}
			
			if (e.CommandName == "Pay") 
			{
				if (!IsValid) return;
				int installments = (e.Item.FindControl("ddlInstallments") as DropDownList).SelectedValue.ToInt32(1);
				string itemText = null;
				if (option.IsDynamicProduct) itemText = "open payment";
				else itemText = string.Format("{0} x {1}", quantity, option.GetTextForLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name).Name);

				MakePayment(option.ID, (byte)(option.IsAuthorize ? 1 : 0), quantity, installments, amount, itemText, option.CurrencyISOCode);			
			}
			else if (e.CommandName == "AddToCart") 
			{
				Bll.Shop.Cart.Basket cart = null;
				if (Request.Cookies["CartID"] != null) 
				{
					Guid cartId = Guid.Parse(Request.Cookies["CartID"].Value);
					cart = Bll.Shop.Cart.Basket.Load(cartId);
				}

				if (cart == null) 
				{
					cart = new Bll.Shop.Cart.Basket(Merchant.ID, null, null);
					Response.SetCookie(new HttpCookie("CartID", cart.Identifier.ToString()));
				}

				var item = new Bll.Shop.Cart.BasketItem(Merchant.ID, option.ID, null, null);
				item.Quantity = (short) quantity;
                //item.ImageUrl = Merchant.MapPublicVirtualPath(option.ImageFileName);
				cart.AddItem(item);
				Response.Redirect(ResolveUrl("~/cart/default.aspx"));
			}
		}

		protected void MakePayment(int itemId, byte transType, int quantity, int installments, decimal amount, string itemText, string currencyIso)
		{
			/*
			decimal chargeAmount = 0;
			int chargeCurrency = 0;
			string itemText = "open payment";
			if (itemId == -1) {
				chargeAmount = txtAmount.Text.ToDecimal(0);
				chargeCurrency = ddCurrency.SelectedValue.ToInt32(0);
			} else {
				var item = Bll.PublicPayment.GetChargeOption(WebUtils.DomainHost, Merchant.Number, itemId);
				if (item == null) return;
				chargeAmount = item.Amount * quantity;
				chargeCurrency = item.CurrencyID;
				itemText = string.Format("{0} x {1}", quantity, item.Text);
			}
			 * */

			var currency = Bll.Currency.Get(currencyIso);
			string signature = Data.CreateSignature(SignatureType.MD5, null, Merchant.Number + amount.ToAmountFormat() + currency.IsoCode + Merchant.HashKey);
			string finishPage = "", notifyPage = "";
			//string finishPage = ((new System.Uri(Request.Url, "Finish.aspx")).ToString()).ToEncodedUrl();
			//string notifyPage = ((new System.Uri(Request.Url, "Finish.aspx?notify=1")).ToString()).ToEncodedUrl();
			Response.Redirect(
				WebUtils.CurrentDomain.ProcessV2Url +
				"Hosted/?merchantID=" + Merchant.Number +
				"&trans_amount=" + amount.ToAmountFormat() +
				"&trans_currency=" + currency.IsoCode +
                "&trans_type=" + transType +
				"&trans_order=" + itemId +
				"&signature=" + HttpUtility.UrlEncode(signature) +
				"&trans_installments=" + installments +
				"&public_itemId=" + itemId +
				"&public_quantity=" + quantity +
				"&disp_payFor=" + HttpUtility.UrlEncode(itemText) +
				"&url_notify=" + notifyPage +
				"&url_redirect=" + finishPage 
				, true);
		}

		protected void PaymentOptions_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			e.Item.FindControl("ltQuantity").Visible = e.Item.FindControl("ddlQuantity").Visible = (e.Item.DataItem as Bll.Shop.Products.Product).IsDynamicProduct;
			e.Item.FindControl("ltInstallments").Visible = e.Item.FindControl("ddlInstallments").Visible = (e.Item.DataItem as Bll.Shop.Products.Product).Installments > 0;

			if (((Bll.Shop.Products.Product)e.Item.DataItem).IsDynamicProduct)
			{
				var textbox = (TextBox)e.Item.FindControl("txtDynamicAmount");
				var button = (LinkButton)e.Item.FindControl("btnPurchase");
				var requiredFieldValidator = (RequiredFieldValidator)e.Item.FindControl("valdatorAmountRequired");
				var numericFieldValidator = (RegularExpressionValidator)e.Item.FindControl("validatorAmountNumber");

				requiredFieldValidator.ValidationGroup = e.Item.ItemIndex.ToString();
				numericFieldValidator.ValidationGroup = e.Item.ItemIndex.ToString();
				textbox.ValidationGroup = e.Item.ItemIndex.ToString();
				button.ValidationGroup = e.Item.ItemIndex.ToString();
			}
			else 
			{
				var requiredFieldValidator = (RequiredFieldValidator)e.Item.FindControl("valdatorAmountRequired");
				var numericFieldValidator = (RegularExpressionValidator)e.Item.FindControl("validatorAmountNumber");
				requiredFieldValidator.Enabled = false;
				numericFieldValidator.Enabled = false;
			}
		}

		protected string getItemOnChangeJsEvent(Bll.Shop.Products.Product ppco)
		{
			return string.Format("onQuantityCharge(this, {0}, {1}, '{2}')", ppco.ID, ppco.Price, 0d.ToAmountFormat(ppco.CurrencyISOCode));
		}
	}
}