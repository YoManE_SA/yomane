﻿<%@ Page Language="C#" MasterPageFile="~/Public/Public.Master" AutoEventWireup="true"
    CodeBehind="Product.aspx.cs" Inherits="Netpay.PaymentPage.Public.Product" Title="Product Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<asp:PlaceHolder runat="server" ID="phHeader">
		<title><%# product.GetTextForLanguage(null).Name %></title>
		<meta name="description" content="<%# product.GetTextForLanguage(null).Description %>" />
		<meta name="keywords" content="<%# product.Tags %>" />
		<meta name="language" content="en" />
		<meta name="robots" content="index,follow" />
		<meta name="viewport" content="width=615; user-scalable=yes;" />

		<!-- Schema.org markup for Google+ -->
		<meta itemprop="name" content="<%# product.GetTextForLanguage(null).Name %>">
		<meta itemprop="description" content="<%# product.GetTextForLanguage(null).Description %>">
		<meta itemprop="image" content="<%# product.ImageFileName %>">

		<!-- Twitter Card data -->
		<meta name="twitter:card" content="<%# product.GetTextForLanguage(null).Name %>">
		<meta name="twitter:site" content="@publisher_handle">
		<meta name="twitter:title" content="<%# product.GetTextForLanguage(null).Name %>">
		<meta name="twitter:description" content="<%# product.GetTextForLanguage(null).Description %>">
		<meta name="twitter:creator" content="@author_handle">
		<meta name="twitter:image" content="<%# ImageVirPath %>">
		<meta name="twitter:data1" content="<%# product.Price.ToString("0.00") %>">
		<meta name="twitter:label1" content="Price">
        <meta name="twitter:SKU" content="<%# product.SKU %>">
    

		<!-- Open Graph data -->
        <meta property="og:type" content="og:product" />
        <meta property="og:title" content="<%# product.GetTextForLanguage(null).Name %>" />
        <meta property="og:image"content="<%# ImageVirPath %>" />
        <meta property="og:image:width" content="2016">
        <meta property="og:image:height" content="630">
        <meta property="og:description" content="<%# product.GetTextForLanguage(null).Description %>" />
        <meta property="og:url" content="<%# Request.Url %>" />
        <meta property="product:plural_title" content="<%# product.GetTextForLanguage(null).Name %>" />
        <meta property="product:price:amount" content="<%# product.Price.ToString("0.00") %>"/>
        <meta property="product:price:currency" content="<%# CurrencyIso %>"/>

	</asp:PlaceHolder>
    <script type="text/javascript">
        function share(UrlName) {
            var x = window.open(UrlName + encodeURIComponent(location.href), "_blank", "width=700,height=400");
            window.opener.location.reload(false);
            return x;
        }
        function sendmail() {
            var x = window.location.assign("mailto:your@email.com?subject=Check out this cool product I'm looking at using Shops&body=" + location.href + "%0D%0A" + "%0D%0A");
            return x;
        }
    </script>
    <script type="text/javascript">
        var minQuant = <asp:Literal ID="literalMinQuant" runat="server" Text="" />;
        var maxQuant = <asp:Literal ID="literalMaxQuant" runat="server" Text="" />;
        var intervalQuant = <asp:Literal ID="literalIntervalQuant" runat="server" Text="" />;
        var maxPayments = <asp:Literal ID="literalMaxPayments" runat="server" Text="" />;
        var price = <asp:Literal ID="literalPriceVal" runat="server" Text="" />;
        var currency = '<asp:Literal ID="literalCurrencyVal" runat="server" Text="" />';
        var isDynamicAmount = <asp:Literal ID="literalIsDynamicAmount" runat="server" Text="" />

		$(function(){
		    $("#spanTotalCurrencySymbol").html(currency);
		    $("#spanPriceTopCurrencySymbol").html(currency);

		    if (maxPayments < 2)
		        $("#divPayments").css("display", "none");
			
		    if (isDynamicAmount)
		    {
		        $("#spanPriceTop").css("display", "none");
		        $("#spanPriceTopCurrencySymbol").css("display", "none");
		        $("#divQuantity").css("display", "none"); 
		        $("#ctl00_ctl00_body_body_txtDynamicAmount").css("display", "inline"); 
		    }
		    else
		    {
		        $("[id$='txtQuantity']").val(minQuant);
		        $("#spanPriceTop").html(formatCurrency(price));
		        $("#ctl00_ctl00_body_body_txtDynamicAmount").css("display", "none");
		        calculatePrice();
		        if (maxQuant < 2)
		            $("#divQuantity").css("display", "none");  
		    }
		});

        function setQuantity(val) {
            var element = $("[id$='txtQuantity']");
            var quant = new Number(element.val());
            quant += val * intervalQuant;

            if (quant < minQuant)
                quant = minQuant;

            if (quant > maxQuant)
                quant = maxQuant;

            element.val(quant);
            calculatePrice();
        }

        function setInstallments(val) {
            var element = $("[id$='txtInstallments']");
            var inst = new Number(element.val());
            inst += val;

            if (inst < 1)
                inst = 1;

            if (inst > maxPayments)
                inst = maxPayments;

            element.val(inst);
        }

        function calculatePrice()
        {
            var quant = new Number($("[id$='txtQuantity']").val());
            $("[id$='divTotal']").html(formatCurrency(price*quant));
        }

        function formatCurrency(amount)
        {
            //return currency + amount.toFixed(2);
            return amount.toFixed(2);
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div style="padding: 6px;">
    <div style="border: 1px solid #EDEDED; background-color: #FAFAFA;">
        <div id="product">
            <img id="imgProduct" alt="product image" width="614" height="192" runat="server" />
            <!-- Product image -->
        </div>
        <h2 class="title-product">
            <asp:Literal ID="literalProductName" runat="server" Text="product name" /></h2>
        <span class="price-product" id="spanPriceTop"></span>
        <span class="price-product" id="spanPriceTopCurrencySymbol">$</span>
        <div class="spacer"></div>
        <div id="wrap-detalis">
            <div class="arrow-down"></div>
            <div class="description-product">
                <asp:Literal ID="literalProductDescription" runat="server" Text="product description" />
            </div>
            <hr class="line-space" />
            <div id="divQuantity" class="choose-quantity">
                <span class="quantity-proudct">
                    <asp:Literal runat="server" Text="<%$ Resources: Public.Product.aspx,  quantity %>" /></span>
                <img src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/minus-cart.png" style="cursor: pointer;"
                    onclick="javascript:setQuantity(-1);" />
                <input id="txtQuantity" type="text" class="input-quantity" readonly="readonly" value="1"
                    runat="server" />
                <img src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/plus-cart.png" style="cursor: pointer;"
                    onclick="javascript:setQuantity(1);" />
            </div>
            <hr class="line-space" />
            <div id="divPayments" class="choose-installments">
                <span class="installments-proudct">
                    <asp:Literal runat="server" Text="<%$ Resources: Public.Product.aspx,  installments %>" /></span>
                <img src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/minus-cart.png" style="cursor: pointer;"
                    onclick="javascript:setInstallments(-1);" />
                <input id="txtInstallments" type="text" class="input-quantity" readonly="readonly"
                    value="1" runat="server" />
                <img src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/plus-cart.png" style="cursor: pointer;"
                    onclick="javascript:setInstallments(1);" />
                <span class="alert-installments">(<asp:Literal ID="literalShowInstalments" runat="server" Text="" />
                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: Public.Product.aspx,  installments %>" />)</span>
            </div>
        </div>
        <div class="total-arrow">
        </div>
        <div id="total-purchase">
            <div class="summary">
                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: Public.Product.aspx,  Totals %>" />
                <span id="spanTotalCurrencySymbol">$</span>
                <span id="divTotal">
                    <asp:Literal ID="literalTotal" runat="server" Text="" />
                    <asp:TextBox ID="txtDynamicAmount" runat="server" CssClass="DynamicAmount" />
                    <asp:RequiredFieldValidator ID="DynamicAmountValidator1" ValidationGroup="vg1" Display="Dynamic" ControlToValidate="txtDynamicAmount" ErrorMessage="<%$ Resources: Public.Default.aspx, productAmountRequiredValidation %>" runat="server"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="DynamicAmountValidator2" ValidationGroup="vg1" Display="Dynamic" ValidationExpression="^[1-9]\d*(\.\d+)?$" ControlToValidate="txtDynamicAmount" ErrorMessage="<%$ Resources: Public.Default.aspx, productAmountValidation %>" runat="server"></asp:RegularExpressionValidator>
                </span>
            </div>
            <div class="purchase">
                <asp:LinkButton ID="btnPurchase" CssClass="submit-button" CausesValidation="true" ValidationGroup="vg1" OnClick="Purchase" runat="server"><asp:Literal runat="server" Text="<%$ Resources: Public.Product.aspx,  purchase%>" /> </asp:LinkButton>
                <%--<input type="submit" class="submit-button" value="<%$ Resources: Public.Product.aspx,  purchase %>" runat="server" />--%>
            </div>
        </div>
        <div class="social">
            <a id="UrlFacebook" href="#" onclick="share('https://www.facebook.com/sharer/sharer.php?u=');return false;" target="_blank"><span class="icon-facebook"></span></a>
            <a id="Urlgoogle" href="#" onclick="share('https://plus.google.com/share?url=');return false;" target="_blank"><span class="icon-googleplus"></span></a>
            <a id="Urlmail" href="#" onclick="sendmail()"><span class="icon-envelope"></span></a>
        </div>
        <div class="spacer"></div>
        <div class="banner" id="divPromoBanner" runat="server">
            <a id="aPromoBanner" target="_blank" runat="server"><img id="imgPromoBanner" style="display:none" runat="server" height="100" width="620" border="0" /></a>
        </div>
    </div>
        </div>
</asp:Content>
