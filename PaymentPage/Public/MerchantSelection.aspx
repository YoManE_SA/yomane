﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="~/Public/MerchantSelection.aspx.cs" Inherits="Netpay.PaymentPage.Public.MerchantSelection" EnableViewStateMac="false" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>
		<asp:Literal ID="Literal6" runat="server" Text="<%$ Resources: Public.MerchantSelection.aspx, PageTitle %>" /></title>
	<link href="../scripts/jquery-ui-1.8.10.custom/css/custom-theme/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
	<script type="text/javascript" src="../scripts/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="../scripts/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js"></script>
	<script type="text/javascript" src="../scripts/jquery-ui-1.8.10.custom/js/jquery.ui.datepicker-he.js"></script>
</head>
<body>
	<form id="form1" runat="server">
		<netpay:ScriptManager runat="server" />
		<div class="header-top">
			<div class="Language">
				<netpay:LanguageSelector2 ID="wcLanguageSelector" UsePostback="true" runat="server" CssClass="LanguageDropdown" />
			</div>
		</div>
		<div class="clip"></div>
		<asp:PlaceHolder ID="phInstall" runat="server">
			<div id="container">
				<article class="wrap-content">
					<div id="content">
						<div class="logos">
							<div class="logo"></div>
							<div class="facebook-logo"></div>
						</div>
						<div class="spacer"></div>
						<div class="line-hr"></div>
						<div class="PaymentSolutions-header">
							<div class="text">
								<asp:Literal ID="lblfacebookapp" runat="server" Text="<%$ Resources: Public.MerchantSelection.aspx,  txtInstallApp %>" />
							</div>
							<div class="arrow"></div>
							<div class="spacer"></div>
						</div>
						<div class="content-section">
							<div class="table-section">
								<div class="tr-table">
									<div class="td-cell-text">
										<asp:Literal ID="literalInstallApp" runat="server" Text="<%$ Resources: Public.MerchantSelection.aspx,  txtfacbookapps %>" />&nbsp;<a id="aInstallLink" class="install-app" target="_top" runat="server"><asp:Literal ID="lblclickhere" runat="server" Text="<%$ Resources: Public.MerchantSelection.aspx,  txtclickhere %>" /></a>
									</div>
								</div>
							</div>
							<div class="spacer"></div>
						</div>
					</div>
				</article>
			</div>
		</asp:PlaceHolder>

		<asp:PlaceHolder ID="phSelectMerchant" runat="server">
			<div id="container">
				<article class="wrap-content">
					<div id="content">
						<div class="logos">
							<div class="logo"></div>
							<div class="facebook-logo"></div>
						</div>
						<div class="spacer"></div>
						<div class="line-hr"></div>
						<div id="divMerchantTip" visible="false" runat="server" class="info-notifaction">
							<asp:Literal ID="literalMerchantTip" runat="server" Text="<%$ Resources: Public.MerchantSelection.aspx,  txtTip %>" />
						</div>
						<div id="divInactiveMessage" visible="false" runat="server" class="info-notifaction">
							<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: Public.MerchantSelection.aspx, txtpageActivated %>" />
						</div>
						<asp:PlaceHolder ID="phMerchantContent" runat="server">
							<div class="line-hr"></div>
							<div class="PaymentSolutions-header">
								<div class="text">
									<asp:Literal ID="literalMerchantInstructions" runat="server" Text="<%$ Resources: Public.MerchantSelection.aspx, txtinstruction %>" />
								</div>
								<div class="arrow"></div>
								<div class="spacer"></div>
							</div>
							<div class="content-section">
								<div id="divEmptyNumber" runat="server" class="error-notifaction">
									<asp:Literal ID="Literal7" runat="server" Text="<%$ Resources: Public.MerchantSelection.aspx, txtMissingID %>" />
								</div>
								<div id="divInvalidNumber" runat="server" class="error-notifaction">
									<asp:Literal ID="Literal5" runat="server" Text="<%$ Resources: Public.MerchantSelection.aspx, txtInvalidMerchant %>" />
								</div>
								<div class="table-section">
									<div id="divAdminForm" runat="server" visible="false" class="tr-table">
										<div class="td-cell-text">
											<asp:Literal ID="Literal4" runat="server" Text="<%$ Resources: Public.MerchantSelection.aspx, txtMerchant %>" />
										</div>
										<div class="td-cell">
											<input type="text" id="txtMerchantNumber" placeholder="Merchant number" class="field_385" runat="server" />

											<input type="hidden" name="signed_request" value="<%= Request["signed_request"] %>" />
										</div>
									</div>
								</div>
								<div class="wrap-button">
									<input id="btnSetMerchantNumber" onserverclick="OnSetMerchantNumber" type="button" value="<%$ Resources: Public.MerchantSelection.aspx, Button %>" runat="server" />
								</div>
								<div class="spacer"></div>
							</div>
						</asp:PlaceHolder>
					</div>
				</article>
			</div>
		</asp:PlaceHolder>

		<div class="footer-shadow"></div>
		<footer class="footer">
			<div class="card"></div>
			<div class="copyright">
				<asp:Literal ID="lblCompanyName" runat="server" />
			</div>
		</footer>
	</form>
</body>
</html>
