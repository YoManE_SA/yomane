﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Netpay.Bll;
using Netpay.Web;

namespace Netpay.PaymentPage.Public
{
	[WebService(Namespace = "Netpay.PaymentPage.Public")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	[ScriptService]
	public class DefaultService : System.Web.Services.WebService
	{
		[WebMethod]
		public List<Bll.Shop.Products.Product> Search(string merchantNumber, string searchTags)
		{
			if (merchantNumber == null || merchantNumber.Trim().Length == 0) return null;
			return Bll.Shop.Products.Product.Search(new Bll.Shop.Products.Product.SearchFilters() { 
					MerchantId = Bll.Merchants.Merchant.CachedNumbersForDomain()[merchantNumber], 
					LanguageIso = Bll.International.Language.Get(WebUtils.CurrentLanguage).Culture, 
					Text = searchTags 
				}, new Infrastructure.SortAndPage() { PageSize = 10 });
		}
	}
}
