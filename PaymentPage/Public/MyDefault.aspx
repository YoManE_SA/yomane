﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public/ProductMain.Master" AutoEventWireup="true" CodeBehind="MyDefault.aspx.cs" Inherits="Netpay.PaymentPage.Public.MyDefault" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content> 
<asp:Content ID="Content3" ContentPlaceHolderID="cpFooterScripts" runat="server">
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="cpContent">
    <div class="content">
        <div class="about">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="about-image">
                            <img src="img/about/about.jpg">
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="about-text">
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
									sed diam nonummy nibh euismod tincidunt ut laoreet dolore
									magna aliquam erat volutpat. Ut wisi enim ad minim veniam,
									quis nostrud exerci tation ullamcorper suscipit lobortis
									nisl ut aliquip ex ea commodo consequat. Duis autem vel eum 
									iriure dolor in hendrerit in vulputate velit esse molestie consequat,
									vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan
									et iusto odio dignissim qui blandit.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="content-title">
                        <div class="container">
                            <div class="row">
                                <h4>Authors</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="author">
                            <div class="author-img">
                                <a href="#">
                                <img src="img/author/1.jpg" /></a>
                            </div>
                            <div class="author-name">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Designer</p>
                                <div class="social-media">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="author">
                            <div class="author-img">
                                <a href="#">
                                <img src="img/author/2.jpg" /></a>
                            </div>
                            <div class="author-name">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Developer</p>
                                <div class="social-media">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="author">
                            <div class="author-img">
                                <a href="#">
                                <img src="img/author/3.jpg" /></a>
                            </div>
                            <div class="author-name">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Photographer</p>
                                <div class="social-media">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="author">
                            <div class="author-img">
                                <a href="#">
                                <img src="img/author/4.jpg" /></a>
                            </div>
                            <div class="author-name">
                                <h4><a href="#">John Doe</a></h4>
                                <p>
                                    Leader</p>
                                <div class="social-media">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <!-- .about end -->
            </div>
</asp:Content>

