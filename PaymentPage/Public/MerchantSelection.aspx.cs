﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Facebook;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.PaymentPage.Code;
using Netpay.Web;

namespace Netpay.PaymentPage.Public
{
	public partial class MerchantSelection : Netpay.Web.BasePage
	{
		private FBPageData _data = null;
		private class FBPageData 
		{
			public long pageID = 0;
			public bool isPageAdmin = false;
		}

		private FBPageData GetFBData() 
		{
			if (_data == null) 
			{
				_data = new FBPageData();
                if (Infrastructure.Application.IsProduction)
				{
					string signedRequest = Request["signed_request"];
					FacebookClient client = new FacebookClient();
					dynamic signedRequestData = client.ParseSignedRequest(WebUtils.CurrentDomain.GetConfig("FacebookShopsSecretID"), signedRequest);
					_data.pageID = long.Parse(signedRequestData.page.id);
					_data.isPageAdmin = signedRequestData.page.admin;
				}
				else
				{
					_data.pageID = 123456789;
					_data.isPageAdmin = true;
					Boolean.TryParse(Request["isPageAdmin"], out _data.isPageAdmin);
				}			
			}

			return _data;
		}

		private string GetRedirectUrl(string merchantNumber)
		{
			string redirectUrl = Bll.Shop.Module.GetUrl(merchantNumber);
			//MerchantSettingsHostedVO settings = HostedPage.GetSettings(WebUtils.DomainHost, merchantNumber);
			//if (settings.UIVersion == 2) redirectUrl = "~/Designs/New/Public/default.aspx";
			//else if (IsMobile) redirectUrl = "~/Designs/Mobile/Public/default.aspx";
			return redirectUrl;
		}
		
		protected void Page_Load(object sender, EventArgs e)
		{
			lblCompanyName.Text = GetGlobalResourceObject("Main", "Slogan").ToString().Replace("%LEGAL_NAME%", WebUtils.CurrentDomain.LegalName);

			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}/Design/New/Styles/facebook.css\" />", ResolveUrl("~/" + WebUtils.CurrentDomain.ThemeFolder))));
			if (WebUtils.CurrentDirection.ToLower() == "rtl")
				Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}/Design/New/Styles/facebookrtl.css\" />", ResolveUrl("~/" + WebUtils.CurrentDomain.ThemeFolder))));

			wcLanguageSelector.Cultures.Add(new CultureInfo("en-us"));
			wcLanguageSelector.Cultures.Add(new CultureInfo("he-il"));

			if (Request["mode"] != null && Request["mode"] == "install")
			{
				phInstall.Visible = true;
				phSelectMerchant.Visible = false;

				aInstallLink.HRef = "https://www.facebook.com/dialog/pagetab?app_id=" + WebUtils.CurrentDomain.GetConfig("FacebookShopsAppID") + "&redirect_uri=https://apps.facebook.com/" + WebUtils.CurrentDomain.GetConfig("FacebookShopsNamespace") + "/";
			}
			else 
			{
				phInstall.Visible = false;
				phSelectMerchant.Visible = true;
				
				if (!IsPostBack)
				{
					divEmptyNumber.Visible = false;
					divInvalidNumber.Visible = false;
					divMerchantTip.Visible = false;
					divInactiveMessage.Visible = false;

					FBPageData data = GetFBData();

					phMerchantContent.Visible = data.isPageAdmin;
					divMerchantTip.Visible = data.isPageAdmin;
					divAdminForm.Visible = data.isPageAdmin;

					string storedMerchantNumber = Netpay.Bll.ThirdParty.Facebook.GetMerchantNumber(data.pageID);
					if (data.isPageAdmin)
					{
						if (storedMerchantNumber != null)
							txtMerchantNumber.Value = storedMerchantNumber;
					}
					else
					{
						if (storedMerchantNumber != null)
						{
							Response.Redirect(GetRedirectUrl(storedMerchantNumber), true);
						}
						else
						{
							divInactiveMessage.Visible = true;
						}
					}
				}		
			}
		}

		protected void OnSetMerchantNumber(object sender, EventArgs e) 
		{
			string enteredMerchantNumber = txtMerchantNumber.Value.Trim();
			if (enteredMerchantNumber == "")
			{
				divEmptyNumber.Visible = true;
				return;
			}

			FBPageData data = GetFBData();
			bool result = Netpay.Bll.ThirdParty.Facebook.AddMerchantPage(enteredMerchantNumber, data.pageID);
			if (!result)
			{
				divInvalidNumber.Visible = true;
				return;
			}
			else
				Response.Redirect(GetRedirectUrl(enteredMerchantNumber));
		}
	}
}