﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using System.Globalization;

namespace Netpay.PaymentPage.Public
{
	public partial class Main : Netpay.PaymentPage.Code.MasterPageBase
	{
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (!IsPostBack)
			{
					ltPayForText.Text = ""; 
			}
		}
        public void SetMerchantLogo(string imageUrl)
        {

            (this.Master as Netpay.PaymentPage.Main).SetMerchantLogo(imageUrl);
        }
	}
}