﻿<%@ Page Language="C#" MasterPageFile="~/Public/Public.Master" AutoEventWireup="true" CodeBehind="DefaultAjax.aspx.cs" Inherits="Netpay.PaymentPage.Public.DefaultAjax" Title="Shops" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<script src="../Scripts/jquery.tmpl.js" type="text/javascript"></script>

	<script type="text/jscript">
		function onQuantityChange(itemId, amount, currency) {
			var quantity = $("#quantitySelect" + itemId).val();
			var newAmount = currency + " " + (amount * quantity).toFixed(2);
			$('#dvPrice' + itemId).html(newAmount);
		}

		function search()
		{
			$("#productList").html("Loading...");
			var tags = $("#ctl00_ctl00_body_body_acTags_text").val().trim();
			$.ajax({
				type: "POST",
				url: "DefaultService.asmx/Search",
				data: "{'merchantNumber':'<%=Merchant.Number%>', 'searchTags':'" + tags + "'}",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (products) {
					$("#productList").html("");
					products = products["d"];
					$.tmpl("productTemplate", products, { getQuantity: function (item) { var result = []; for (var i = item.data.QuantityMin; i <= item.data.QuantityMax; i += item.data.QuantityInterval) { result.push(i) }; return result; }, getPaymnts: function (item) { var result = []; for (var i = 1; i <= item.data.PaymentsMax; i++) { result.push(i) }; return result; } }).appendTo("#productList").each(function (index, element) { $(element).delay(index * 400).fadeIn(2000) });
				}
			});
		}

		function getLinkUrl()
		{
			var merchantId = netpay.Common.fromQueryString(location.search)["?merchantID"];
			var tags = encodeURI($("#ctl00_ctl00_body_body_acTags_text").val().trim());
			var link = location.protocol + "//" + location.host + location.pathname + "?merchantID=" + merchantId + "&search=" + tags;
			return link;
		}

		$(document).ready(function () {
			var searchString = netpay.Common.fromQueryString(location.search)["search"];
			if (searchString != null)
				$("#ctl00_ctl00_body_body_acTags_text").val(searchString);
			$.template("productTemplate", $("#productTemplate").html());
			search();
		});
	</script>

	<div id="search">
		<netpay:AutoComplete ID="acTags" IsMultiselect="true" Function="GetPublicPageProductTagAutocomplete" TagSeperator=" " runat="server" />
		<button ID="btnSearch" class="button-search" OnClick="javascript:search()">GO</button>
	</div>

	<div class="Public-head">
		<img src="/NPCommon/Images/Arrow01<%=Netpay.Web.WebUtils.CurrentReverseAlignment%>.gif" alt="" />
		<asp:Localize ID="Localize1" Text="<%$ Resources: Public.DefaultAjax.aspx, paymentOptions %>" runat="server" />
	</div>
	<div class="spacer"></div>

	<div id="public-page">
		<div id="productList"></div>
		<script type="text/html" id="productTemplate">
			<div class="section-public" style="display:none;">
				<div class="public-left">
					<img alt="" src="<%=Merchant.MapPublicVirtualPath("") %>${ImageFileName}" class="pic-box" />
				</div>
				<div class="public-right">
					<div class="title-public">
						<span class="read-more" onclick="$('#itemDescription${ID}').slideDown('slow');">
							<img src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/expand.png" />
							${Text} -<span class="price" id="dvPrice${ID}">${CurrencySymbol} ${Amount.toFixed(2)}</span>
						</span>
						<div class="wrap-pursace">
							<span class="pursace">
								<a href="?merchantID=<%= Request["merchantID"] %>&makePayment=true&itemId=${ID}" class="button-pay">PURCHASE</a>
							</span>
						</div>
					</div>
					<div class="Quantity">
						{{if IsDynamicQuantiy}}
        					<asp:Literal runat="server" ID="ltQuantity" Text="<%$ Resources: Public.DefaultAjax.aspx,  quantity %>" />
							<select id="quantitySelect${ID}" name="quantitySelect${ID}" onchange="javascript:onQuantityChange(${ID}, ${Amount}, '${CurrencySymbol}');">
								{{each $item.getQuantity($item)}}
									<option value="${$value}">${$value}</option>
								{{/each}}
							</select>
                        {{/if}}
						{{if IsDynamicPayments}}
							<asp:Literal runat="server" ID="ltInstallments" Text="<%$ Resources: Public.DefaultAjax.aspx,  payments %>" />
							<select id="installmentsSelect${ID}" name="installmentsSelect${ID}">
								{{each $item.getPayments($item)}}
									<option value="${$value}">${$value}</option>
								{{/each}}
							</select>
                        {{/if}}
					</div>
					<div class="sku-wrap">
						<asp:Localize ID="Localize3" Text="<%$ Resources: Public.DefaultAjax.aspx,  Sku %>" runat="server" />
						${SKU} (${QuantityAvailable}
								<asp:Localize ID="Localize4" Text="<%$ Resources: Public.DefaultAjax.aspx,  Avaliable %>" runat="server" />)
					</div>
					<div class="spacer"></div>
				</div>
				<div class="spacer"></div>

				<!-- Push Read more -->
				<div class="description" id="itemDescription${ID}">
					<div class="title-read-more">${Text}</div>
					<div class="close-box" onclick="$('#itemDescription${ID}').slideUp('slow');">
						<img alt="" src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/close.png" />
					</div>
					<div class="spacer"></div>
					<div class="text-paragraph">
						<p>
							${Description}
						</p>
					</div>
					<div class="spacer"></div>
					<div class="spacer"></div>
				</div>
				<!-- End Read more -->

			</div>
		</script>

		<asp:PlaceHolder ID="phPymentOptionCustom" Visible="false" runat="server">
			<div class="section-custom">
				<div class="public-left-custom">
					<h2>
						<asp:Localize Text="<%$ Resources: Public.DefaultAjax.aspx,  customOption %>" runat="server" /></h2>
					<div class="text-forms">
						<asp:Localize Text="<%$ Resources: Public.DefaultAjax.aspx,  amount %>" runat="server" />:
                                <asp:TextBox ID="txtAmount" runat="server" class="input-amount" AutoPostBack="true" />
						&nbsp;&nbsp;  
						<asp:Localize Text="<%$ Resources: Public.DefaultAjax.aspx,  currency %>" runat="server" />:
                                <netpay:CurrencyDropDown ID="ddCurrency" runat="server" class="input-currency" OnSelectedIndexChanged="CustomCharge_Change" AutoPostBack="true" />
					</div>
				</div>
				<div class="public-right-custom">
					<div class="title-public"></div>
					<div runat="server" id="dvCustomPrice" class="price-custom">$$$</div>
					<div class="spacer"></div>
					<div class="call-to-action">
						<span class="pursace">
							<asp:LinkButton ID="btnPayCustom" CssClass="button-pay" runat="server" Text="<%$ Resources: Public.DefaultAjax.aspx,  Purchase %>" OnCommand="Pay_CommandCustom" CommandArgument="-1" /></span>
					</div>
				</div>
				<div class="spacer"></div>
			</div>
		</asp:PlaceHolder>
	</div>

</asp:Content>
