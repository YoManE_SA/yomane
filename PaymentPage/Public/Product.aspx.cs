﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Web;
using System.Text;
using Netpay.Bll;
using Netpay.Infrastructure;
using System.IO;

namespace Netpay.PaymentPage.Public
{
    public partial class Product : PublicBasePage
    {
		public Bll.Shop.Products.Product product;
		public string ImageVirPath { get; set; }
		public string CurrencyIso { get; set; }

        protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack) {
				string productId = Request["Item"];
				product = Bll.Shop.Products.Product.Load(productId.ToInt32());
				if (product == null) base.RaiseError("product not found");

                if (!product.IsDynamicProduct)
                {
                    DynamicAmountValidator1.Enabled = false;
                    DynamicAmountValidator2.Enabled = false;
                }

                imgProduct.Src = Bll.Accounts.Account.MapPublicVirtualPath(Merchant.Number, product.ImageFileName);
				ImageVirPath = string.Format("{0}{1}", Infrastructure.Domain.Get(WebUtils.DomainHost).WebServicesUrl, imgProduct.Src);
				CurrencyIso = Bll.Currency.Get(product.CurrencyISOCode).IsoCode;
				phHeader.DataBind();
				//literalPrice.Text = literalTotal.Text = product.Amount.ToString(",0.00");
				var text = product.GetTextForLanguage(null);
				Title = text.Name;
				literalProductName.Text = text.Name;
				literalProductDescription.Text = text.Description;

                literalMinQuant.Text = product.QtyStart.ToString();
                literalMaxQuant.Text = product.QtyAvailable < product.QtyEnd ? product.QtyAvailable.ToString() : product.QtyEnd.ToString();
                literalIntervalQuant.Text = product.QtyStep.ToString();
                literalMaxPayments.Text = product.Installments.ToString();
                literalShowInstalments.Text = product.Installments.ToString();
                literalPriceVal.Text = product.Price.ToString();
                literalCurrencyVal.Text = Bll.Currency.Get(product.CurrencyISOCode).Symbol;
				literalIsDynamicAmount.Text = product.IsDynamicProduct.ToString().ToLower();

                // promo banner
				/*
				if (Options != null){
					bool isBannerImage = File.Exists(FilesManager.GetGlobalAccessPhisicalPath(Infrastructure.Security.UserRole.Merchant, Merchant.Number) + Options.BannerFileName);
					if (isBannerImage && Options.BannerLinkUrl != null)
					{
						divPromoBanner.Visible = true;
						aPromoBanner.HRef = Options.BannerLinkUrl;
						imgPromoBanner.Src = FilesManager.GetGlobalAccessVirtualPath(Infrastructure.Security.UserRole.Merchant, Merchant.Number) + Options.BannerFileName;
					}
					else divPromoBanner.Visible = false;
				} else divPromoBanner.Visible = false;
				*/
            }
            //else
                //Purchase();
		}

        protected void Purchase(object sender, EventArgs e)
        {
            int productId = Request["Item"].ToInt32();
            int quant = txtQuantity.Value.ToInt32();
            int inst = txtInstallments.Value.ToInt32();
			product = Bll.Shop.Products.Product.Load(productId);
			decimal total = 0;
			if (product.IsDynamicProduct) {
				total = txtDynamicAmount.Text.ToDecimal(0);
			} else {
				total = quant * product.Price;
			}

			var currency = Bll.Currency.Get(product.CurrencyISOCode);
            string signature = Data.CreateSignature(SignatureType.MD5, null, Merchant.Number + total.ToAmountFormat() + currency.IsoCode + Merchant.HashKey);
            
            string url = WebUtils.CurrentDomain.ProcessV2Url +
                "Hosted/?merchantID=" + Merchant.Number +
                "&trans_amount=" + total.ToAmountFormat() +
                "&trans_currency=" + currency.IsoCode +
                "&trans_type=" + (product.IsAuthorize ? "1" : "0") + 
                "&trans_order=" + product.ID +
                "&signature=" + HttpUtility.UrlEncode(signature) +
                "&trans_installments=" + inst +
				"&public_itemId=" + productId +
				"&public_quantity=" + quant +
                "&disp_payFor=" + HttpUtility.UrlEncode(product.GetTextForLanguage(null).Name) +
                "&url_notify=" +
                "&url_redirect=" +
                "&trans_comment=";
            
            Response.Redirect(url, true);
        }
    }
}