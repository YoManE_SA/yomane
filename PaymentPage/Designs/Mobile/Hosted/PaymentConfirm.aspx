﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="true" Inherits="Netpay.PaymentPage.Hosted.PaymentConfirm" %>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<asp:CustomValidator ID="vAgreement" OnServerValidate="CustomValidatorAgreement" ErrorMessage="<%$Resources:Hosted.PaymentConfirm.aspx, cvAgreementRequired %>" ValidationGroup="creditCardConfirmation" Display="None" runat="server"></asp:CustomValidator>
	<table class="FormView" width="100%">
		<tr>
			<td>
				<table width="100%">
					<tr><th><asp:Label runat="server" id="ltChargeInfoTitle" /></th></tr>
					<tr><td valign="top"><asp:Literal ID="ltChargeInfo" runat="server" /></td></tr>
					<tr><td><br /></td></tr>
					<tr><th><asp:Label runat="server" id="ltAddressInfoTitle" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, ltAddressInfoTitle.Text %>" /></th></tr>
					<tr><td valign="top"><asp:Literal ID="ltAddressInfo" runat="server" /></td></tr>
					<tr><td><br /></td></tr>
					<tr><th><asp:Label runat="server" id="ltPersonalInfoTitle" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, ltPersonalInfoTitle.Text %>" /></th></tr>
					<tr><td valign="top"><asp:Literal ID="ltPersonalInfo" runat="server" /></td></tr>
				</table>
				<br /><br />
			</td>
		</tr>
		<tr>
			<td>
				<asp:ValidationSummary ID="ValidationSummary2" EnableClientScript="false" runat="server" DisplayMode="SingleParagraph" ShowMessageBox="false" ShowSummary="true" ValidationGroup="creditCardConfirmation" CssClass="ValidSummery" HeaderText="<%$Resources:Main, FieldError%>" />
				<table>
				<tr>
					<td style="vertical-align:top; width:26px;"><asp:CheckBox ID="cbAgreement" runat="server" class="distinct" Checked="true" /></td>
					<td><asp:Literal ID="ltAgreement" runat="server" /></td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="reverse">
				<br /><br />
				<asp:HyperLink runat="server" ID="hGoBack" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, hlBackChange.Text %>" /><br />
				<br /><asp:Button runat="server" ID="btnCharge" Text="Charge" OnCommand="btnCharge_Command" CssClass="Button" />
			</td>
		</tr>
	</table>
	<br />
</asp:Content>
