<%@ Control Language="C#" AutoEventWireup="true" Inherits="Netpay.PaymentPage.Controls.PaymentSolutions" %>
<div style="text-align:center;">
<asp:Repeater ID="rptSolutionGroups" runat="server" OnItemDataBound="rptSolutionGroups_OnSolutionGroups">
	<ItemTemplate>
		<span runat="server" id="trPaymentMethod" style="cursor:pointer;padding:3px;margin:4px;" onmouseover="this.style.backgroundColor='#F5F5F5';" onmouseout="this.style.backgroundColor='';">
			<asp:Image runat="server" ID="imgPayment" Height="44" ImageUrl='<%# "/NPCommon/ImgPaymentMethodGroups/98X44/" + Eval("ID") + ".png" %>' />
		</span>
	</ItemTemplate>
</asp:Repeater>

<asp:Repeater ID="rptSolutions" runat="server" OnItemDataBound="rptSolutions_OnItemDataBound">
	<ItemTemplate>
    	<span runat="server" id="trPaymentMethod" style="cursor:pointer;padding:3px;margin:4px;" onmouseover="this.style.backgroundColor='#F5F5F5';" onmouseout="this.style.backgroundColor='';">
			<asp:Image runat="server" ID="imgPayment" Height="44" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/MIX" + Eval("ID") + ".png" %>' />
		</span>
	</ItemTemplate>
</asp:Repeater>
</div>
