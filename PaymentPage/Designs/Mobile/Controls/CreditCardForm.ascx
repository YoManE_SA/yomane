<%@ Control Language="C#" AutoEventWireup="True" Inherits="Netpay.PaymentPage.Controls.CreditCardForm" %>
<%@ Register Src="BillingAddress.ascx" TagPrefix="Netpay" TagName="BillingAddress" %>
<%@ Register Src="PersonalDetails.ascx" TagPrefix="Netpay" TagName="PersonalDetails" %>

<asp:CustomValidator ID="vCreditcardLuhn" ControlToValidate="wcCreditcard" OnServerValidate="LuhnValidation" Display="None" ValidationGroup="PaymentForm" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorCreditcardNum %>" runat="server" />
<asp:RequiredFieldValidator ID="vCreditcard" ControlToValidate="wcCreditcard" Display="None" ValidationGroup="PaymentForm" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorCreditcardNum %>" runat="server" />
<asp:RequiredFieldValidator ID="vExpirationMonth" ControlToValidate="wcExpirationMonth" Display="None" ValidationGroup="PaymentForm" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorExpirationMonth %>" runat="server" />
<asp:RequiredFieldValidator ID="vExpirationYear" ControlToValidate="wcExpirationYear" Display="None" ValidationGroup="PaymentForm" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorExpirationYear %>" runat="server" />
<asp:RequiredFieldValidator ID="vCardholderName" ControlToValidate="txtCardholderName" Display="None" ValidationGroup="PaymentForm" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorCardholderName %>" runat="server" />
<asp:CustomValidator ID="vCVVFormat" ControlToValidate="txtSecurityNumber" OnServerValidate="CvvValidation" Display="None" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorCVV %>" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vCVV" ControlToValidate="txtSecurityNumber" Display="None" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorCVV %>" ValidationGroup="PaymentForm" runat="server" />

<table class="FormView" width="100%" border="0">
	<tr>
		<th style="padding-top:0!important;">
			<asp:Label ID="lblCardDetails" runat="server" Text="<%$ Resources:CreditCardForm.ascx, lblCardDetails %>" />
		</th>
	</tr><tr>
		<td>
			<asp:Literal Text="<%$ Resources:CreditCardForm.ascx, ltCCNumber %>" runat="server" /><span class="asterisk">*</span><br />
			<netpay:CreditCardInput ID="wcCreditcard" runat="server" CssClass="InputField" ElementWidth="100%" SingleFieldMode="true" />
		</td>
	</tr><tr>
		<td>
			<asp:Literal runat="server" Text="<%$ Resources:CreditCardForm.ascx, ltExpDate %>" /><span class="asterisk">*</span><br />
			<netpay:MonthDropDown ID="wcExpirationMonth" EnableBlankSelection="true" Width="100" runat="server" CssClass="InputField"></netpay:MonthDropDown>
			&nbsp;/&nbsp;
			<netpay:YearDropDown ID="wcExpirationYear" Width="70" runat="server" CssClass="InputField"></netpay:YearDropDown>
		</td>
	</tr><tr>
		<td>
			<asp:Literal ID="ltCardholderName" runat="server" Text="<%$ Resources:CreditCardForm.ascx, ltCardholderName %>" /> <span class="asterisk">*</span><br />
			<asp:TextBox runat="server" ID="txtCardholderName" MaxLength="30" CssClass="InputField" />
		</td>
	</tr><tr>
		<td>
			<asp:Literal ID="ltSecurityNumber" runat="server" Text="<%$ Resources:CreditCardForm.ascx, ltSecurityNumber %>" /> <span id="spanCVVRequired" runat="server" class="asterisk">*</span>
			<netpay:PageItemHelp ID="CvvTip" runat="server" Text="<%$Resources:Main, TipCvv%>" Width="300" /><br />
			<asp:TextBox runat="server" ID="txtSecurityNumber" MaxLength="30" CssClass="InputField" />
		</td>
	</tr>
</table>
<Netpay:BillingAddress runat="server" id="billingAddress" />
<Netpay:PersonalDetails runat="server" id="personalDetails" />
