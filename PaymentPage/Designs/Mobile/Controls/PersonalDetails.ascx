<%@ Control Language="C#" AutoEventWireup="true" Inherits="Netpay.PaymentPage.Controls.PersonalDetails" %>
<asp:RequiredFieldValidator ID="vPhoneValidator" ControlToValidate="txtPhone" Display="None" ErrorMessage="<%$ Resources:PersonalDetails.ascx, ValidatorPhone %>" ValidationGroup="PaymentForm" runat="server" />
<asp:RegularExpressionValidator ID="vRegEmail" ControlToValidate="txtEmail" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Runat="server" ErrorMessage="<%$ Resources:PersonalDetails.ascx, Validator2Email %>" ValidationGroup="PaymentForm" />
<asp:RequiredFieldValidator ID="vEMailValidator" ControlToValidate="txtEmail" Display="None" runat="server" ErrorMessage="<%$ Resources:PersonalDetails.ascx, Validator1Email %>" ValidationGroup="PaymentForm" />
<asp:RequiredFieldValidator ID="vCardholderID" ControlToValidate="txtCardholderID" Display="None" ErrorMessage="<%$ Resources:PersonalDetails.ascx, ValidatorID %>" ValidationGroup="PaymentForm" runat="server" />
<asp:PlaceHolder runat="server" ID="tblPersonalDetails">
	<table class="FormView" width="100%" border="0">
		<tr>
			<th>
				<asp:Label ID="lblPersonalDetails" runat="server" Text="<%$ Resources:PersonalDetails.ascx, lblPersonalDetails %>" />
			</th>
		</tr><tr>
			<asp:PlaceHolder runat="server" ID="tdPhone">
				<td>
					<asp:Literal ID="ltPhone" runat="server" Text="<%$ Resources:PersonalDetails.ascx, ltPhone %>" /><span id="spanPhoneRequired" runat="server" class="asterisk">*</span><br />
					<asp:TextBox runat="server" ID="txtPhone" MaxLength="30" CssClass="InputField" />
				</td>
			</asp:PlaceHolder>	
		</tr><tr>
			<asp:PlaceHolder runat="server" ID="tdEmail">
				<td>
					<asp:Literal ID="ltEmail" runat="server" Text="<%$ Resources:PersonalDetails.ascx, ltEmail %>" /><span id="spanEmailRequired" runat="server" class="asterisk">*</span><br />
					<asp:TextBox runat="server" ID="txtEmail" MaxLength="255" CssClass="InputField" />
				</td>
			</asp:PlaceHolder>	
		</tr><tr>
			<asp:PlaceHolder runat="server" ID="tdCardholderID">
				<td>
					<asp:Literal ID="ltCardholderID" runat="server" Text="<%$ Resources:PersonalDetails.ascx, ltCardholderID %>" /> <span id="spanIDRequired" runat="server" class="asterisk">*</span><br />
					<asp:TextBox runat="server" ID="txtCardholderID" MaxLength="30" CssClass="InputField" />
				</td>
			</asp:PlaceHolder>	
		</tr><tr>
            <asp:PlaceHolder runat="server" id="tdBirthDate">
		        <td>
			        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:PersonalDetails.ascx, ltBirthDate %>" /> <span id="span1" runat="server" class="asterisk">*</span><br />
			        <netpay:DatePicker runat="server" ID="dpBirthDate" MaxLength="30" CssClass="InputField" />
		        </td>
            </asp:PlaceHolder>
		</tr>	
	</table>
</asp:PlaceHolder>	
