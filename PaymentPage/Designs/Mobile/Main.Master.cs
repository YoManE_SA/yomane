﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Web;

namespace Netpay.PaymentPage.Designs.Mobile
{
	public partial class Main : Netpay.PaymentPage.Main
	{
		protected override void RegisterCss()
		{
			base.RegisterCss();
			RegisterCssFile(ResolveUrl(Data.DesignDir + "Styles/mobile.css"));
			RegisterCssFile(ResolveUrl(Data.DesignDir + string.Format("Styles/style{0}.css", WebUtils.CurrentDirection)));
		}

		protected override void OnLoad(EventArgs e)
		{
			if (!IsPostBack)
			{
				ddlLanguageSelector.DataSource = getCultures();
				ddlLanguageSelector.DataBind();
				ddlLanguageSelector.SelectedValue = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
			}
			base.OnLoad(e);
		}

		protected void LanguageSelector_SelectedIndexChanged(object sender, EventArgs e)
		{
			System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(ddlLanguageSelector.SelectedValue);
			Response.Cookies.Set(new HttpCookie("UICulture", ddlLanguageSelector.SelectedValue));
			Response.Redirect(Request.Path);
		}


	}
}