﻿<%@ Page Title="" Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" ValidateRequest="false" Inherits="Netpay.PaymentPage.Hosted.Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="wrap-accordion">
        <div id="step-01">
            <div id="click-selesction-01" class="accordion">
                <div class="text">
                    Errors:
                </div>
                <div class="arrow-close">
                </div>
                <div class="spacer">
                </div>
            </div>
            <div id="section-01" class="content-section">
            <div style="padding: 10px;">   
            <asp:Literal ID="txtErrorMessage" runat="server">An error has occured on this page.</asp:Literal></div>
            </div>
        </div>
    </div>
</asp:Content>
