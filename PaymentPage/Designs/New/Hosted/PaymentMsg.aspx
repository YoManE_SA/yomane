﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="true" Inherits="Netpay.PaymentPage.Hosted.PaymentMsg" %>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="wrap-payment-msg">
        <asp:MultiView runat="server" ID="mvReply">
            <asp:View runat="server">
                <div class="success-notifaction">
                    <asp:Label runat="server" ID="lbSucceededText" /></div>
                <div class="MsgMerchant">
                    <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:Hosted.PaymentMsg.aspx,ltTransID %>" />: <asp:Label runat="server" ID="TransID" /><br />
                    <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Hosted.PaymentMsg.aspx,ltTransDate %>" />: <asp:Label runat="server" ID="TransDate" />
                </div>
                <div class="check-detalis">
					<asp:PlaceHolder runat="server" ID="phFinish">
	                    <asp:LinkButton runat="server" ClientIDMode="Static" ID="btnFinish" Text="<%$Resources:Hosted.PaymentMsg.aspx,btnFinish%>" OnClick="btnFinish_Click" OnClientClick="doRedirect();void(0)" class="btn-finish" />
                		<script type="text/javascript">
                			var checkClose = true;
                			var timeout = window.setInterval(updateButtonText, 1000);
                			var maxTimer = <%= Data.url_redirect_seconds %>;
                			jQuery(window).bind('beforeunload', function () {
                				if (!checkClose) return;
                				alert('<asp:Literal runat="server" Text="<%$Resources:Hosted.PaymentMsg.aspx, AlertClose%>" />');
								return false;
							});

							function doRedirect() {
								if (timeout) {
									window.clearTimeout(timeout);
									timeout = null;
								}
								checkClose = false;
								$('#btnFinish').attr('disabled', 'disabled');
								document.location = $('#btnFinish').attr('href');
							}

							function updateButtonText() {
								maxTimer--;
								$('#btnFinish').text('<asp:Literal runat="server" Text="<%$Resources:Hosted.PaymentMsg.aspx, btnFinish%>" /> ' + maxTimer);
								if (maxTimer == 0) doRedirect();
							}
                		</script> 
					</asp:PlaceHolder>
				</div>
            </asp:View>
            <asp:View runat="server">
                <div class="error-notifaction">
                    <asp:Label runat="server" ID="lbFailedText" /></div>
                <div class="check-detalis">
                    <asp:LinkButton runat="server" ID="btnTryAgain" Text="<%$Resources:Hosted.PaymentMsg.aspx,btnTryAgain%>" OnClick="btnTryAgain_Click" /></div>
            </asp:View>
        </asp:MultiView>
        <div class="MsgMerchant">
            <asp:Literal runat="server" ID="ltMerchantText" /></div>
    </div>
</asp:Content>
