﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Web;

namespace Netpay.PaymentPage.Designs.New.Hosted
{
	public partial class PaymentConfirm : Netpay.PaymentPage.Hosted.PaymentConfirm
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e); 
			btnCharge.OnClientClick = "$('#btnCharge').css('visibility', 'hidden'); $('#dvLoader').css('display', ''); $('#wrap-back-button').css('display', 'none'); $('#wrap-agreement').css('display', 'none'); $('#wrap-registration').css('display', 'none');";
			if (!Page.IsPostBack) {
				chkRegisterWallet.Text = "";
				lblRegisterWallet.Text = GetGlobalResourceObject("Hosted.PaymentConfirm.aspx", "WalletRegestration").ToString().Replace("%LEGAL_NAME%", WebUtils.CurrentDomain.LegalName);
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			lblRegisterWallet.Visible = chkRegisterWallet.Visible;
		}
	}
}