﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.PaymentPage.Designs.New.Hosted
{
    public partial class Master : Netpay.PaymentPage.Hosted.Master
    {
        protected override void OnPreRender(EventArgs e)
        {
            if (imgMerchantLogo != null)
            {
                if (string.IsNullOrEmpty(imgMerchantLogo.ImageUrl))
                {
                    if (BasePage.Merchant != null && BasePage.Data != null && BasePage.Data.PaymentSettings != null)
                    {
                        imgMerchantLogo.ImageUrl = Bll.Accounts.Account.MapPublicVirtualPath(Data.merchantID, Data.PaymentSettings.LogoImage);
                        if (!System.IO.File.Exists(Bll.Accounts.Account.MapPublicPath(Data.merchantID, Data.PaymentSettings.LogoImage)))
                            imgMerchantLogo.ImageUrl = Main.DefaultLogoFile;
                    }
                    else imgMerchantLogo.ImageUrl = Main.DefaultLogoFile;
                }
                imgMerchantLogo.Visible = (imgMerchantLogo.ImageUrl != Main.DefaultLogoFile);
                dvLogoMerchant.Visible = imgMerchantLogo.Visible;
            }
            base.OnPreRender(e);
        }
    }
}