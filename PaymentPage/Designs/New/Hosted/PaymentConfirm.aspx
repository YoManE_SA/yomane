﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="True" CodeBehind="PaymentConfirm.aspx.cs" Inherits="Netpay.PaymentPage.Designs.New.Hosted.PaymentConfirm" %>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="wrap-accordion">
        <div id="step-02">
            <div class="accordion">
                <div class="text"><asp:Label runat="server" ID="Label1" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, txtPaymentReview %>" /></div>
                <div class="arrow-close"></div>
                <div class="spacer">
                </div>
            </div>
            <div id="section-02" class="content-section">
                <div id="wrap-review">
                    <div class="wrap-billing">
                        <h2>
                            <asp:Label runat="server" ID="ltAddressInfoTitle" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, ltAddressInfoTitle.Text %>" /></h2>
                        <p>
                            <asp:Literal ID="ltAddressInfo" runat="server" />
                        </p>
                    </div>
                    <div class="wrap-personalinfo">
                        <h2>
                            <asp:Label runat="server" ID="ltPersonalInfoTitle" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, ltPersonalInfoTitle.Text %>" /></h2>
                        <p>
                            <asp:Literal ID="ltPersonalInfo" runat="server" />
                        </p>
                    </div>
                    <div class="wrap-creditCard">
                        <h2>
                            <asp:Label runat="server" ID="ltChargeInfoTitle" /></h2>
                        <p>
                            <asp:Literal ID="ltChargeInfo" runat="server" />
                        </p>
                    </div>
                    <div class="spacer">
                    </div>
                </div>
				<asp:PlaceHolder runat="server" ID="phClientAgreement">
					<div class="wrap-terms">
						<div class="terms"><asp:Literal ID="litLegalDoc" runat="server" /></div>
					</div>
				</asp:PlaceHolder>
				<div id="wrap-agreement">
					<div class="agree-check">
						<asp:CheckBox ID="cbAgreement" runat="server" />
					</div>
					<div class="agree-text">
						<asp:Literal ID="ltAgreement" runat="server" /><asp:Literal ID="ltClientAgreement" runat="server" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, txtAgreement %>" />  
					</div>
					<div class="spacer">
					</div>
				</div>
                <div id="wrap-registration">
                    <div class="registration-check">
                        <asp:CheckBox ID="chkRegisterWallet" runat="server" />
                    </div>
                     <div class="registration-text">
                   <asp:Literal runat="server" ID="lblRegisterWallet" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, WalletRegestration %>" />  
                    </div>
                    <div class="spacer">
                    </div>
                </div>
                <asp:CustomValidator ID="vAgreement" OnServerValidate="CustomValidatorAgreement"
                    ErrorMessage="<%$Resources:Hosted.PaymentConfirm.aspx, cvAgreementRequired %>" ValidationGroup="creditCardConfirmation"
                    Display="None" runat="server"></asp:CustomValidator>
                <asp:CustomValidator ID="vNoQuantity" OnServerValidate="CustomValidatorQuantity"
                    ErrorMessage="<%$Resources:Hosted.PaymentConfirm.aspx, cvNoQuantity %>" ValidationGroup="creditCardConfirmation"
                    Display="None" runat="server"></asp:CustomValidator>
                <asp:ValidationSummary ID="ValidationSummary2" EnableClientScript="false" runat="server"
                    DisplayMode="SingleParagraph" ShowMessageBox="false" ShowSummary="true" ValidationGroup="creditCardConfirmation"
                    CssClass="error-notifaction" HeaderText="<%$Resources:Main, FieldError%>" />
                <div id="wrap-back-button" class="edit-form"> 
                    <asp:HyperLink runat="server" ID="hGoBack" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, hlBackChange.Text %>" />
                </div>
                <div class="wrap-button-charge">
                    <asp:Button runat="server" CssClass="btn-Charge" ID="btnCharge" ClientIDMode="Static" Text="Charge" OnCommand="btnCharge_Command" />
                </div>
                <div class="spacer">
                </div>
            </div>
        </div>
    </div>
    <section class="loader" id="dvLoader" style="display: none;">
    <div class="loader-text">
    We are processing your transaction- please wait</div>
    <div class="loader-image"></div>
    </section>
</asp:Content>
