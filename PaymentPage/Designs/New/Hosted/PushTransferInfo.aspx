﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="true" Inherits="Netpay.PaymentPage.Hosted.PushTransferInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <div id="step-01">
        <div class="accordion">
            <div class="text">
                <asp:Literal runat="server" ID="ltTitle" Text="<%$Resources:Hosted.PushTransferInfo.aspx,ltTitle%>" /></div>
            <div class="arrow-close"></div>
            <div class="spacer">
            </div>
        </div>
    </div>
    <div id="section-01" class="content-section">
        <div id="wrap-review">
            <div class="wrap-transfer">
                <h2>
                    <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Hosted.PushTransferInfo.aspx,ltTransferInfo%>" />
                </h2>
                <p>
                    <asp:Label runat="server" ID="lbAccount" /><br />
                    <asp:Label runat="server" ID="lbTransfer" />
                </p>
            </div>
            <div class="wrap-bankinfo">
                <h2>
                    <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:Hosted.PushTransferInfo.aspx,ltBankAddress%>" />
                </h2>
                <p>
                    <asp:Label runat="server" ID="lbFrom" />
                </p>
            </div>
            <div class="wrap-to">
                <h2>
                    <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:Hosted.PushTransferInfo.aspx,ltTo%>" />
                </h2>
                <p>
                    <asp:Label runat="server" ID="lbOwnerAddress" />
                </p>
            </div>
            <div class="spacer">
            </div>
           
        </div>
    </div>
        <div id="step-02">
        <div class="accordion">
            <div class="text">Soloution</div>
            <div class="arrow-close"></div>
            <div class="spacer">
            </div>
        </div>
    </div>
      <div id="section-02" class="content-section">
        <div class="link-form"><a runat="server" id="BankURL" target="_blank">Click Here to Redirect to your bank's web site</a></div>
            <div class="link-form"><a runat="server" id="SimulateURL" target="_blank">Click Here to simulate transfer</a></div>
            <div class="link-form"><asp:LinkButton runat="server" Text="Click here to check the status of the transfer"  OnClick="CheckTransfer_OnClick" /></div>
    </div>

</asp:Content>
