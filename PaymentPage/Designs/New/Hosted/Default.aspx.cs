﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.PaymentPage.Designs.New.Hosted
{
	public partial class Default : Netpay.PaymentPage.Hosted.Default
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (!IsPostBack && Data != null)
			{
				ltMerchantTopText.Text = Data.MerchantTextData(Web.WebUtils.CurrentLanguage, Infrastructure.TranslationGroup.HostedPaymentPage, "FrontTop");
				ltMerchantBottomText.Text = Data.MerchantTextData(Web.WebUtils.CurrentLanguage, Infrastructure.TranslationGroup.HostedPaymentPage, "FrontBottom");
			}

		}

	}
}