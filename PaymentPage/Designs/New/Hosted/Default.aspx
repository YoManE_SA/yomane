﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="True" MaintainScrollPositionOnPostback="true" CodeBehind="Default.aspx.cs" Inherits="Netpay.PaymentPage.Designs.New.Hosted.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<div class="MerchantTopText">
		<asp:Literal runat="server" ID="ltMerchantTopText" />
	</div>
	<asp:PlaceHolder runat="server" ID="phControl" />
	<div class="MerchantBottomText">
		<asp:Literal runat="server" ID="ltMerchantBottomText" />
	</div>
</asp:Content>

