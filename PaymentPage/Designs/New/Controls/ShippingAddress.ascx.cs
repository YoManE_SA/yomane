﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.PaymentPage.Designs.New.Controls
{
	public partial class ShippingAddress : Netpay.PaymentPage.Controls.ShippingAddress
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Visible = tblShippingAddress.Visible;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			System.Web.UI.ScriptManager.RegisterStartupScript(vShippingAddressState, vShippingAddressState.GetType(), "EnableStateValidator", "ValidatorEnable(document.getElementById('" + vShippingAddressState.ClientID + "'), " + vShippingAddressState.Enabled.ToString().ToLower() + ");", true);
		}

	}
}
