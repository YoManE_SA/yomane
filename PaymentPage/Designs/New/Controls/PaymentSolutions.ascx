<%@ Control Language="C#" AutoEventWireup="true" Inherits="Netpay.PaymentPage.Controls.PaymentSolutions" %>
<div class="PaymentSolutions-header">
    <div class="text"><asp:Literal runat="server" Text="<%$ Resources:Main, PaymentMethod %>" /></div>
    <div class="arrow"></div>
    <div class="spacer"></div>
</div>
<asp:Repeater ID="rptSolutionGroups" runat="server" OnItemDataBound="rptSolutionGroups_OnSolutionGroups">
    <ItemTemplate>
        <div id="Payment-solutions">
            <div id="trPaymentMethod" runat="server" class="tr-payment">
                <div class="td-payment">
                    <div class="frame-image">
                        <asp:Image runat="server" ID="imgPayment" alt="Payment Method" ImageUrl='<%# "/NPCommon/ImgPaymentMethodGroups/115X82/" + Eval("ID") + ".png" %>' />
                    </div>
                </div>
                <div class="td-payment-text">
                    <h3><asp:Literal runat="server" ID="ltPMTitle" /></h3>
                    <p><asp:Literal runat="server" ID="ltPMText" /></p>
                </div>
            </div>
        </div>
        <hr class="line-hr" />
    </ItemTemplate>
</asp:Repeater>
<asp:Repeater ID="rptSolutions" runat="server" OnItemDataBound="rptSolutions_OnItemDataBound">
    <ItemTemplate>
        <div id="Payment-solutions">
            <div id="trPaymentMethod" runat="server" class="tr-payment" >
                <div class="td-payment">
                    <div class="frame-image">
                        <asp:Image runat="server" ID="imgPayment" alt="Payment Method" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/115X82/" + Eval("ID") + ".png" %>' />
                    </div>
                </div>
                <div class="td-payment-text">
                    <h3><asp:Literal runat="server" ID="ltPMTitle" /></h3>
                    <p>
                        <asp:Literal runat="server" ID="ltPMText" />
                    </p>
                </div>
            </div>
        </div>
        <hr class="line-hr" />
    </ItemTemplate>
</asp:Repeater>
