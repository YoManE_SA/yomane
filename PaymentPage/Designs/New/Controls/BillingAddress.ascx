<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="BillingAddress.ascx.cs" Inherits="Netpay.PaymentPage.Designs.New.Controls.BillingAddress" %>
<div id="dvSectionAddress" runat="server" visible="false" class="notifaction success" ><asp:Literal runat="server" ID="ltrAddressSection" Text="<%$ Resources:BillingAddress.ascx, txtAddressSection %>" /></div>
<asp:PlaceHolder runat="server" ID="tblBillingAddress">
	<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="BillingAddress" DisplayMode="SingleParagraph" CssClass="notifaction error" />
    
    <div class="table-section">
        <asp:PlaceHolder runat="server" ID="tdAddress1">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" ID="Literal1" Text="<%$ Resources:BillingAddress.ascx, ltAddress1 %>" />
                    <span id="spanAddressRequired" runat="server" class="asterisk">*</span>
                </div>
                <div class="td-cell">
                    <asp:TextBox runat="server" ID="txtBillingAddress" MaxLength="255" type="text" class="field_385" placeholder="Street address, P.O. box, company name, c/o" />
					<asp:RequiredFieldValidator ID="vBillingAddress" ControlToValidate="txtBillingAddress" Display="None" ErrorMessage="<%$ Resources:BillingAddress.ascx, ValidatorAddress %>" ValidationGroup="BillingAddress" runat="server" />
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="tdAddress2">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" Text="<%$ Resources:BillingAddress.ascx, ltAddress2 %>" ID="ltAddress2" />
                </div>
                <div class="td-cell">
	                <asp:TextBox runat="server" ID="txtBillingAddress2" MaxLength="255" type="text" class="field_385" placeholder="Apartment, suite, unit, building, floor, etc." />
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="tdCity">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" ID="ltCity" Text="<%$ Resources:BillingAddress.ascx, ltCity %>" />
					<span id="spanCityRequired" runat="server" class="asterisk"> *</span>
                </div>
                <div class="td-cell">
                    <asp:TextBox runat="server" ID="txtBillingAddressCity" type="text" class="field_385" required="required" />
					<asp:RequiredFieldValidator ID="vBillingAddressCity" ControlToValidate="txtBillingAddressCity" Display="None" ErrorMessage="<%$ Resources:BillingAddress.ascx, ValidatorCity %>" ValidationGroup="BillingAddress" runat="server" />
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="tdZipCode">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" ID="ltZipCode" Text="<%$ Resources:BillingAddress.ascx, ltZipCode %>" />
					<span id="spanZipCodeRequired" runat="server" class="asterisk"> *</span>
                </div>
                <div class="td-cell">
                    <asp:TextBox runat="server" ID="txtBillingAddressZipCode" class="field_385" required="required" />
					<asp:RequiredFieldValidator ID="vBillingAddressZipcode" ControlToValidate="txtBillingAddressZipCode" Display="None" ErrorMessage="<%$ Resources:BillingAddress.ascx, ValidatorZipcode %>" ValidationGroup="BillingAddress" runat="server" />
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="tdCountry">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" Text="<%$ Resources:BillingAddress.ascx, ltCountry %>" ID="ltCountry" />
					<span id="spanCountryRequired" runat="server" class="asterisk"> *</span>
                </div>
                <div class="td-cell">
                    <div class="country-select">
						<netpay:CountryDropDown ID="ddBillingAddressCountry" type="text" class="field_385" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="BillingCountryChange" runat="server" />
						<asp:RequiredFieldValidator ID="vBillingAddressCountry" ControlToValidate="ddBillingAddressCountry" Display="None" ErrorMessage="<%$ Resources:BillingAddress.ascx, ValidatorCountry %>" ValidationGroup="BillingAddress" runat="server" />
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="tdState">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" Text="<%$ Resources:BillingAddress.ascx, ltState %>" ID="ltState" />
                    <span id="spanStateRequired" runat="server" class="asterisk"> *</span>
                </div>
                <div class="td-cell">
                    <div class="state-select">
						<asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="false" RenderMode="Inline" UpdateMode="Conditional">
							<ContentTemplate>
									<netpay:StateDropDown ID="ddBillingAddressState" StartEmpty="true" runat="server" type="text" class="field_385" />
									<asp:RequiredFieldValidator ID="vBillingAddressState" ControlToValidate="ddBillingAddressState" Display="None" ErrorMessage="<%$ Resources:BillingAddress.ascx, ValidatorState %>" ValidationGroup="BillingAddress" runat="server" />
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="ddBillingAddressCountry" />
							</Triggers>
						</asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
    </div>
</asp:PlaceHolder>
