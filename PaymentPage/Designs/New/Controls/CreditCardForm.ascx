<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="CreditCardForm.ascx.cs" Inherits="Netpay.PaymentPage.Designs.New.Controls.CreditCardForm" %>
<%@ Register Src="BillingAddress.ascx" TagPrefix="Netpay" TagName="BillingAddress" %>
<%@ Register Src="ShippingAddress.ascx" TagPrefix="Netpay" TagName="ShippingAddress" %>
<%@ Register Src="personalDetails.ascx" TagPrefix="Netpay" TagName="PersonalDetails" %>

<script type="text/javascript">
    var currentTabIndex = 1;
    var maxStep = 0;

    function validateSection(validationGroup, sectionId)
    {
        var ret = Page_ClientValidate(validationGroup);
        if (ret) {
            $("#tick-section-0" + sectionId).fadeIn();
            openSection(sectionId + 1, true);
        } else $("#tick-section-0" + sectionId).fadeOut();
        return ret;
    }

    function openSection(sectionId, approve) {
        if (maxStep == 0)
            for (maxStep = 1; document.getElementById('section-0' + maxStep) ; maxStep++);
        if (currentTabIndex == sectionId) return;
        if (!approve && currentTabIndex < sectionId) return;
        if (sectionId >= maxStep) return;

        $("#section-0" + currentTabIndex).slideUp();
        $("#section-0" + sectionId).slideDown();

        $("[class ^= arrow]").attr("class", "arrow-close");
        $("#click-selesction-0" + sectionId + " > [class ^= arrow]").attr("class", "arrow");
        for (var i = 1; i <= maxStep; i++)
            $("#image_step" + i).attr('class', 'step-0' + i + (i <= sectionId ? '-on' : '-off'));

        currentTabIndex = sectionId
    }
</script>
<div id="step">
    <span id="image_step1" class="step-01-on"></span><span id="image_step2" clientidmode="Static" runat="server" class="step-02-off"></span><span id="image_step3" clientidmode="Static" runat="server" class="step-03-off"></span><span id="image_step4" clientidmode="Static" runat="server" class="step-04-off"></span>
</div>
<div class="wrap-accordion">
    <div id="step-0<%#++currentSectionIndex%>">
        <div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(<%#currentSectionIndex%>, false)">
            <div class="text">
                <asp:Label ID="lblCardDetails" runat="server" Text="<%$ Resources:CreditCardForm.ascx, lblCardDetails %>" />
            </div>
            <div class="arrow">
            </div>
            <div class="spacer">
            </div>
        </div>
        <div id="section-0<%#currentSectionIndex%>" class="content-section">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="CCDetails" DisplayMode="SingleParagraph" CssClass="notifaction error" />
            <asp:CustomValidator ID="vCreditcardLuhn" ControlToValidate="wcCreditcard" OnServerValidate="LuhnValidation" Display="none" ValidationGroup="CCDetails" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorCreditcardNum %>" runat="server" />
            <asp:RequiredFieldValidator ID="vCreditcard" ControlToValidate="wcCreditcard" Display="none" ValidationGroup="CCDetails" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorCreditcardNum %>" runat="server" />
            <asp:RequiredFieldValidator ID="vExpirationMonth" ControlToValidate="wcExpirationMonth" Display="none" ValidationGroup="CCDetails" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorExpirationMonth %>" runat="server" />
            <asp:RequiredFieldValidator ID="vExpirationYear" ControlToValidate="wcExpirationYear" Display="none" ValidationGroup="CCDetails" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorExpirationYear %>" runat="server" />
            <asp:RequiredFieldValidator ID="vCardholderName" ControlToValidate="txtCardholderName" Display="none" ValidationGroup="CCDetails" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorCardholderName %>" runat="server" />
            <asp:CustomValidator ID="vCVVFormat" ControlToValidate="txtSecurityNumber" OnServerValidate="CvvValidation" Display="none" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorCVV %>" ValidationGroup="CCDetails" runat="server" />
            <asp:RequiredFieldValidator ID="vCVV" ControlToValidate="txtSecurityNumber" Display="none" ErrorMessage="<%$ Resources:CreditCardForm.ascx, ValidatorCVV %>" ValidationGroup="CCDetails" runat="server" />
            <div class="table-section">
                <div class="tr-table">
                    <div class="td-cell-text">
                        <asp:Literal Text="<%$ Resources:CreditCardForm.ascx, ltCardholderName %>" runat="server" /><span class="asterisk"> *</span>
                    </div>
                    <div class="td-cell">
                        <asp:TextBox runat="server" ID="txtCardholderName" class="field_385" autofocus="autofocus" type='text' required="required" ValidationGroup="CCDetails" />
                    </div>
                </div>
 
                <div class="tr-table">
                    <div class="td-cell-text"></div>
                    <div class="td-cell">
                        <asp:UpdatePanel runat="server" ID="upImageType" RenderMode="Inline" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Image runat="server" ID="imgTypeCard" ImageUrl="images/Card.png" CssClass="imgCredit" Visible="false" />
                                <div class="padding-top-10">
                                    <asp:Repeater runat="server" ID="rptPaymentMethodIcons">
                                        <ItemTemplate>
                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# string.Format("/NPCommon/ImgPaymentMethod/50X30/{0}.png", Eval("ID")) %>' BorderStyle="solid" BorderColor="#C9C9C9" BorderWidth="1px" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="wcCreditcard" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>

                <div class="tr-table">
                    <div class="td-cell-text">
                        <asp:Literal Text="<%$ Resources:CreditCardForm.ascx, ltCCNumber %>" runat="server" /><span class="asterisk"> *</span>
                    </div>

                    <div class="tr-table">
                        <netpay:CreditcardInput ID="wcCreditcard" runat="server" CssClass="field_385" ClientIDMode="Static" SingleFieldMode="true" AutoPostBack="true" OnChanged="CreditCard_Change" />
                    </div>
                </div>
                <div class="tr-table">
                    <div class="td-cell-text">
                        <asp:Literal runat="server" Text="<%$ Resources:CreditCardForm.ascx, ltExpDate %>" /><span class="asterisk"> *</span>
                    </div>
                    <div class="td-cell">
                        <div class="month-select">
                            <netpay:MonthDropDown ID="wcExpirationMonth" EnableBlankSelection="true" ValidationGroup="CCDetails" MaxLength="2" runat="server" required="required">
                            </netpay:MonthDropDown>
                            <asp:CustomValidator runat="server" ClientValidationFunction="function ClientValidate(source, arguments) { arguments.IsValid = new Date($('#wcExpirationYear').val, $('#wcExpirationMonth').val, 1) < new Date(); }" ErrorMessage="CustomValidator" />
                        </div>
                        <div class="year-select">
                            <netpay:YearDropDown ID="wcExpirationYear" ValidationGroup="CCDetails" MaxLength="4" runat="server" required="required">
                            </netpay:YearDropDown>
                        </div>
                    </div>
                </div>
                <div class="tr-table">
                    <div class="td-cell-text">
                        <asp:Literal ID="ltSecurityNumber" runat="server" Text="<%$ Resources:CreditCardForm.ascx, ltSecurityNumber %>" /><span id="spanCVVRequired" runat="server" class="asterisk"> *</span>
                    </div>
                    <div class="td-cell">
                        <asp:TextBox runat="server" ID="txtSecurityNumber" ValidationGroup="CCDetails" class="field_40" MaxLength="5" AutoCompleteType="None" autocomplete="off" required="required" />
                    </div>
                </div>
            </div>
            <div class="wrap-button">
                <asp:Button runat="server" UseSubmitBehavior="false" Text="<%$ Resources:Main, Next %>" OnClientClick='<%# GetNextScript("CCDetails") %>' />
            </div>
            <div class="tick" id="tick-section-0<%#currentSectionIndex%>" style="display: none;"></div>
            <div class="spacer">
            </div>
        </div>
    </div>
    <asp:PlaceHolder runat="server" ID="phBillingAddress">
        <div id="step-0<%# phBillingAddress.Visible ? (currentSectionIndex += 1) : currentSectionIndex %>">
            <div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(<%#currentSectionIndex%>, false)">
                <div class="text">
                    <asp:Label ID="ltBillingAddress" runat="server" Text="<%$ Resources:CreditCardForm.ascx, ltBillingAddress %>" />
                </div>
                <div class="arrow-close">
                </div>
                <div class="spacer">
                </div>
            </div>
            <div id="section-0<%#currentSectionIndex%>" class="content-section" style="display: none;">
                <netpay:BillingAddress runat="server" ID="billingAddress" />
                <div class="wrap-button">
                    <asp:Button runat="server" UseSubmitBehavior="false" Text="<%$ Resources:Main, Next %>" OnClientClick='<%# GetNextScript("BillingAddress") %>' />
                </div>
                <div class="tick" id="tick-section-0<%#currentSectionIndex%>" style="display: none;"></div>
                <div class="spacer">
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="phShippingAddress">
        <div id="step-0<%#phShippingAddress.Visible ? (currentSectionIndex += 1) : currentSectionIndex%>">
            <div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(<%#currentSectionIndex%>, false)">
                <div class="text">
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:CreditCardForm.ascx, ltShippingAddress %>" />
                </div>
                <div class="arrow-close">
                </div>
                <div class="spacer">
                </div>
            </div>
            <div id="section-0<%#currentSectionIndex%>" class="content-section" style="display: none;">
                <netpay:ShippingAddress runat="server" ID="shippingAddress" />
                <div class="wrap-button">
                    <asp:Button ID="Button1" runat="server" UseSubmitBehavior="false" Text="<%$ Resources:Main, Next %>" OnClientClick='<%# GetNextScript("ShippingAddress") %>' />
                </div>
                <div class="tick" id="tick-section-0<%#currentSectionIndex%>" style="display: none;"></div>
                <div class="spacer">
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="phPersonalDetails">
        <div id="step-0<%#phPersonalDetails.Visible ? (currentSectionIndex += 1) : currentSectionIndex%>">
            <div id="click-selesction-<%#currentSectionIndex%>" class="accordion" onclick="openSection(<%#currentSectionIndex%>, false)">
                <div class="text">
                    <asp:Label ID="lblPersonalDetails" runat="server" Text="<%$ Resources:CreditCardForm.ascx, lblPersonalDetails %>" />
                </div>
                <div class="arrow-close">
                </div>
                <div class="spacer">
                </div>
            </div>
            <div id="section-0<%#currentSectionIndex%>" class="content-section" style="display: none;">
                <netpay:PersonalDetails runat="server" ID="personalDetails" />
                <div class="wrap-button">
                    <asp:Button runat="server" ID="btnNext" UseSubmitBehavior="false" Text="<%$Resources:Main, Next%>" OnClick="btnNext_Click" OnClientClick='<%# GetNextScript("PersonalDetails") %>' />
                </div>
                <div class="tick" id="tick-section-0<%#currentSectionIndex%>" style="display: none;"></div>
                <div class="spacer">
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
</div>

