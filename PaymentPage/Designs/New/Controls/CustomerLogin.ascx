<%@ Control Language="C#" AutoEventWireup="true" Inherits="Netpay.PaymentPage.Controls.Customer" %>

<asp:MultiView ID="mvWallet" runat="server">
    <asp:View runat="server">
        <div class="wrap-accordion">
            <div id="step-01">
                <div id="click-selesction-01" class="accordion">
                    <div class="text">
                        <asp:Label ID="lblLoginDetails" runat="server" Text="<%$ Resources:CustomerLogin.ascx, lblLoginDetails %>" />
                    </div>
                    <div class="arrow-close">
                    </div>
                    <div class="spacer">
                    </div>
                </div>
                <div id="section-01" class="content-section">
                    <div class="table-section">
                        <div class="tr-table">
                            <div class="td-cell-text">
                                <asp:Literal ID="ltLoginName" runat="server" Text="<%$ Resources:CustomerLogin.ascx, ltLoginName %>" /><span class="asterisk">*</span>
                            </div>
                            <div class="td-cell">
                                <asp:TextBox ID="loginEmail" runat="server" Width="200" ValidationGroup="loginControl" />
                            </div>
                        </div>
                        <div class="tr-table">
                            <div class="td-cell-text">
                                <asp:Literal ID="ltPassword" runat="server" Text="<%$ Resources:CustomerLogin.ascx, ltPassword %>" /><span class="asterisk">*</span>
                            </div>
                            <div class="td-cell">
                                <asp:TextBox TextMode="Password" ID="loginPassword" Width="200" runat="server" ValidationGroup="loginControl" />
                            </div>
                        </div>
                        <div class="tr-table">
                            <div class="td-cell-text">
                                <asp:Literal ID="ltAppIdentity" runat="server" Text="<%$ Resources:CustomerLogin.ascx, ltAppIdentity %>" /><span class="asterisk">*</span>
                            </div>
                            <div class="td-cell">
                                <asp:RadioButtonList CssClass="percent100" RepeatDirection="Horizontal" runat="server" ID="ddlAppIdentity">
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="tr-table">
                            <div class="td-cell-text">
                                <div class="required-wallet">
                                    <asp:Literal runat="server" ID="ltRequired" Text="<%$ Resources:Main,RequireMsg %>" />
                                </div>
                            </div>
                            <div class="td-cell">
                                <asp:Button ID="btnLogin" OnClick="Login_Click" Width="100px" Text="<%$ Resources:Main,Login %>" runat="server" CssClass="Button" />
                            </div>
                        </div>
                    </div>
                    <asp:ValidationSummary ID="ValidationSummary1" EnableClientScript="false" runat="server" DisplayMode="SingleParagraph" ShowMessageBox="false" ShowSummary="true" ValidationGroup="loginControl" CssClass="notifaction error" HeaderText="<%$Resources:Main, FieldError%>" />
                    <asp:CustomValidator runat="server" ID="ValidationWrongUser" ValidationGroup="loginControl" ErrorMessage="<%$ Resources:CustomerLogin.ascx, UserNotFound %>" Display="None" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="loginEmail" ValidationGroup="loginControl" ErrorMessage="<%$ Resources:CustomerLogin.ascx, EmailAddressIsMissing %>" Display="None" CssClass="notifaction error" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="loginEmail" ValidationGroup="loginControl" ErrorMessage="<%$ Resources:CustomerLogin.ascx, EmailAddressIsInvalid %>" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\w" CssClass="notifaction error" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="loginPassword" ValidationGroup="loginControl" ErrorMessage="<%$ Resources:CustomerLogin.ascx, PasswordIsMissing %>" Display="None" CssClass="notifaction error" />
                    <div class="spacer">
                    </div>
                </div>
            </div>
        </div>
    </asp:View>
    <asp:View runat="server">
        <div class="wrap-accordion">
            <div id="step-02">
                <div id="click-selesction-02" class="accordion">
                    <div class="text">
                        <asp:Literal ID="Wallet" runat="server" Text="<%$Resources:Main,Wallet%>" />
                    </div>
                    <div class="arrow-close">
                    </div>
                    <div class="spacer">
                    </div>
                </div>
                <div id="section-02" class="content-section">
                    <div class="wallet-user">
                        <asp:Literal ID="litWalletUser" runat="server" />
                    </div>
                    <div class="wallet-user-logout">
                        <asp:LinkButton OnClick="Logout_Click" Text="<%$ Resources:Main,Logout %>" runat="server" />
                    </div>
                    <div class="spacer"></div>
                    <asp:Label ID="txtResponse" runat="server" Visible="false" CssClass="notifaction error" />
                    <div class="wrap-selectcard">
                        <h2>
                            <asp:Literal ID="litSelectCard" runat="server" Text="<%$Resources:CustomerLogin.ascx, SelectCard%>" /></h2>
                        <div class="rptCard">
                            <asp:Repeater runat="server" ID="rptCardList">
                                <ItemTemplate>
                                    <div>
                                        <input type="radio" class="option" name="PayWith" value="<%# Eval("ID") %>" <%# ((int)Eval("ID") == Request["PayWith"].ToInt32(-1) ? "checked=\"checked\"" : string.Empty) %> />

                                        <netpay:PaymentMethodView ID="PaymentMethodView1" MethodImagePath="/NPCommon/ImgPaymentMethod/50X30/{0}.png" FlagsImagePath="/NPCommon/ImgCountry/50X30/{0}.png" runat="server" HideFlag="true" MethodData='<%# Eval("MethodInstance") %>' />

                                        |
                                        <asp:Literal runat="server" Text="<%$Resources:CustomerLogin.ascx, ValidUntil%>" />: <%# Eval("MethodInstance.ExpirationDate", "{0:MM/yyyy}")%>
                                        |
                                        <asp:Literal runat="server" Text="<%$Resources:CustomerLogin.ascx, CardHolder%>" />: <%# Eval("OwnerName")%>
                                        |
                                        <asp:Literal runat="server" Text='<%# WebUtils.GetCountryName((string)Eval("IssuerCountryIsoCode")) %>' />
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="table-section">
                            <div class="tr-table">
                                <div class="td-cell-text">
                                    <asp:Literal runat="server" Text="PinCode" /><span class="asterisk">*</span>
                                </div>
                                <div class="td-cell">
                                    <asp:TextBox runat="server" ID="txtPinCode" TextMode="Password" class="field_80" AutoCompleteType="None" autocomplete="off"/>
                                </div>
                            </div>
                        </div>
                        <netpay:ActionNotify runat="server" ID="PinCodeActionNotify" />
                        <div class="wrap-button-pay">
                            <asp:Button UseSubmitBehavior="false" runat="server" Text="<%$Resources:CustomerLogin.ascx, ButtonPay%>" OnClick="Pay" CssClass="Button" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:View>
</asp:MultiView>
