﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.PaymentPage.Designs.New.Controls
{
	public partial class BillingAddress : Netpay.PaymentPage.Controls.BillingAddress
	{

		protected void Page_Load(object sender, EventArgs e)
		{
			Visible = tblBillingAddress.Visible;
		}

		protected override void OnPreRender(EventArgs e)
		{
			System.Web.UI.ScriptManager.RegisterStartupScript(vBillingAddressState, vBillingAddressState.GetType(), "EnableStateValidator", "ValidatorEnable(document.getElementById('" + vBillingAddressState.ClientID + "'), " + vBillingAddressState.Enabled.ToString().ToLower() + ");", true);
			base.OnPreRender(e);
		}

		protected override void BillingCountryChange(object sender, EventArgs e)
        {
			base.BillingCountryChange(sender, e);
		}
	}
}
