<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="InstantDebitForm.ascx.cs" Inherits="Netpay.PaymentPage.Designs.New.Controls.InstantDebitForm" %>
<%@ Register Src="BillingAddress.ascx" TagPrefix="Netpay" TagName="BillingAddress" %>
<%@ Register Src="ShippingAddress.ascx" TagPrefix="Netpay" TagName="ShippingAddress" %>
<%@ Register Src="PersonalDetails.ascx" TagPrefix="Netpay" TagName="PersonalDetails" %>
<%@ Register TagPrefix="LC" Namespace="Netpay.PaymentPage.Controls" Assembly="Netpay.PaymentPage" %>

<script type="text/javascript">
	var currentTabIndex = 1;
	var maxStep = 0;

	function validateSection(validationGroup, sectionId) {
		var ret = Page_ClientValidate(validationGroup);
		if (ret) {
			$("#tick-section-0" + sectionId).fadeIn();
			openSection(sectionId + 1, true);
		} else $("#tick-section-0" + sectionId).fadeOut();
		return ret;
	}

	function openSection(sectionId, approve) {
		if (maxStep == 0)
			for (maxStep = 1; document.getElementById('section-0' + maxStep) ; maxStep++);
		if (currentTabIndex == sectionId) return;
		if (!approve && currentTabIndex < sectionId) return;
		if (sectionId >= maxStep) return;

		$("#section-0" + currentTabIndex).slideUp();
		$("#section-0" + sectionId).slideDown();

		$("[class ^= arrow]").attr("class", "arrow-close");
		$("#click-selesction-0" + sectionId + " > [class ^= arrow]").attr("class", "arrow");
		for (var i = 1; i <= maxStep; i++)
			$("#image_step" + i).attr('class', 'step-0' + i + (i <= sectionId ? '-on' : '-off'));

		currentTabIndex = sectionId
	}
</script>

<div id="step">
	<span id="image_step1" class="step-01-on"></span><span id="image_step2"  ClientIDMode="Static" runat="server"  class="step-02-off"></span><span id="image_step3"  ClientIDMode="Static" runat="server" class="step-03-off"></span><span id="image_step4" ClientIDMode="Static" runat="server" class="step-04-off"></span>
</span>
<div class="wrap-accordion">
    <div id="step-0<%#currentSectionIndex%>">
        <div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(1, false)">
            <div class="text">
                <asp:Label ID="lblPayMethod" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltPaymentMethod%>" />
            </div>
            <div class="arrow-close">
            </div>
            <div class="spacer">
            </div>
        </div>
        <div id="section-0<%#currentSectionIndex%>" class="content-section">
			<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="DebitDetails" DisplayMode="SingleParagraph" CssClass="notifaction error" />
			<asp:RequiredFieldValidator ID="vPaymentMethodCountry" ControlToValidate="ddPaymentMethodCountry" ErrorMessage="<%$Resources:InstantDebitForm.ascx, CountryValidator%>" Display="None" ValidationGroup="DebitDetails" runat="server" />
			<asp:RequiredFieldValidator ID="vAccountNameValidator" ControlToValidate="txtAccountName" runat="server" ErrorMessage="<%$Resources:InstantDebitForm.ascx, AccountNameValidator1%>" Display="None" ValidationGroup="DebitDetails" />
			<asp:RegularExpressionValidator ID="vAccountNameValidator2" ValidationExpression="^[A-Za-z\xBF-\xFF]+[\s][A-Za-z\xBF-\xFF,'.]+$" ControlToValidate="txtAccountName" ErrorMessage="<%$Resources:InstantDebitForm.ascx, AccountNameValidator2%>" Display="None" ValidationGroup="DebitDetails" runat="server" />
            <div class="table-section">
                <div class="tr-table">
                    <div class="td-cell-text">
                        <asp:Literal ID="ltCountry" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltSelectCountry%>" /><span id="ltCountryAsterisk" runat="server" class="asterisk">*</span>
                    </div>
                </div>
                <div class="tr-table">
                    <div class="td-cell">
                        <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltFixedBankCountry%>" />
                    </div>
                </div>
                <div class="tr-table">
                    <div class="td-cell">
                        <div id="dvBankCountrySelect" class="bank-country-select" runat="server" clientidmode="Static">
                            <netpay:CountryDropDown ID="ddPaymentMethodCountry" class="field_385" OnSelectedIndexChanged="PaymentMethodCountryChange" AutoPostBack="true" runat="server">
                            </netpay:CountryDropDown>
                        </div>
                    </div>
                </div>

                <div class="tr-table">
                    <div class="td-cell">
                        <div id="dvBankCountryFixed" runat="server" clientidmode="Static">
                            <asp:Image runat="server" ID="imgBankCountryFlag" CssClass="BankCountryFlag" />
                            <asp:Literal runat="server" ID="ltIpCountry" /><span class="IpCountry"></span>
                            <asp:Button runat="server" OnClientClick="document.getElementById('dvBankCountryFixed').style.display='none';document.getElementById('dvBankCountrySelect').style.display='';return false;" Text="Change" />
                            <asp:Image runat="server" ID="imgSelectedMethod" CssClass="SelectedMethod" Visible="false" ImageAlign="Top" />
                            <asp:Label runat="server" ID="ltSelectedMethod"/>
                        </div>
                    </div>
                </div>
                <asp:MultiView runat="server" ID="mvInsDebitForm" ActiveViewIndex="-1" Visible="false">
                    <asp:View runat="server" ID="viewSelectMethod">
                        <div class="wrapSelectMethod">
                            <asp:Label ID="ltSelectMethod" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltSelectMethod%>" />
                        </div>
                        <div class="table-section" style="width: 648px; margin: -10px;">
                            <div class="tr-table">
                                <div class="td-cell">
                                    <LC:ImageRadioList runat="server" ID="irlPaymentMethod" AutoPostBack="true" OnSelectedIndexChanged="PaymentMethodChange" RepeatLayout="table" RepeatColumns="3" CssClass="ImageRadioList" RepeatDirection="Vertical" ItemCssStyle="cursor:pointer;" />
                                </div>
                            </div>
                        </div>
                        <div>
                            <asp:Label ID="lbPMError" runat="server" Text="<%$ Resources:Main,ErrorNoMethodForCountry %>" />
                        </div>
                    </asp:View>
                    <asp:View runat="server" ID="viewSelectBank">
                        <div>
                            <asp:Label runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltSelectBank%>" />
                        </div>
                        <div class="Selected-Bank">
                            <LC:ImageRadioList runat="server" ID="irlBankIDList" AutoPostBack="true" OnSelectedIndexChanged="irlBankIDList_Changed" RepeatLayout="flow" Height="" RepeatDirection="Horizontal" HideImages="true" CssClass="ImageRadioList" ItemCssStyle="cursor:pointer;" />                            
                        </div>
                        <div class="spacer"></div>
                        <div>
                            <asp:Label ID="lbBankError" runat="server" Text="<%$ Resources:Main,ErrorNoMethodForCountry %>" />
                        </div>
                    </asp:View>
                    <asp:View runat="server" ID="viewBankInfo">
                        <div>
                            <asp:Image runat="server" ID="imgSelectedBank" Width="180" Visible="false" />
                        </div>
                        <div class="wrap-title-bank">
                           <asp:Label runat="server" ID="ltSelectedBank" CssClass="title-bankname" />
                        </div>
                        <div class="table-section">
                            <div class="tr-table">
                                <div class="td-cell-text">
                                    <asp:Literal runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltAccountName%>" /><span class="asterisk">*</span>
                                </div>
                                <div class="td-cell">
                                    <asp:TextBox runat="server" ID="txtAccountName2" MaxLength="30" AutoCompleteType="None" autocomplete="off" />
                                </div>
                            </div>
                        </div>
                    </asp:View>
                    <asp:View runat="server" ID="viewAccountDetails">
                     
                            <div class="account-detalis">
                                <asp:Label ID="lblAccountDetails" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltAccountDetails%>" /></div>
                  
                        <div class="table-section">
                            <div class="tr-table">
                                <div class="td-cell-text">
                                    <asp:Literal ID="ltAccountName" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltAccountName%>" />: <span class="asterisk">*</span>
                                </div>
                                <div class="td-cell">
                                    <asp:TextBox runat="server" ID="txtAccountName" MaxLength="30" CssClass="field_385" AutoCompleteType="None" autocomplete="off" />
                                </div>
                            </div>
                            <div class="tr-table">
                                <div class="td-cell-text">
                                    <asp:Literal ID="ltRouting" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltRouting%>" />: <span class="asterisk">*</span>
                                </div>
                                <div class="td-cell">
                                    <asp:TextBox runat="server" ID="txtRouting" MaxLength="30" CssClass="field_385" AutoCompleteType="None" autocomplete="off" />
                                </div>
                            </div>
                            <div class="tr-table">
                                <div class="td-cell-text">
                                    <asp:Literal ID="ltAccount" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltAccount%>" />: <span class="asterisk">*</span>
                                </div>
                                <div class="td-cell">
                                    <asp:TextBox runat="server" ID="txtAccountNumber" MaxLength="30" CssClass="field_385" AutoCompleteType="None" autocomplete="off" />
                                </div>
                            </div>
                        </div>
                    </asp:View>
                    <asp:View runat="server" ID="viewAccountNameOnly">
                        <div class="table-section">
                            <div class="tr-table">
                                <div class="td-cell-text">
                                    <asp:Literal runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltFullName%>" />*
                                </div>
                                <div class="td-cell">
                                    <asp:TextBox runat="server" ID="txtAccountName3" MaxLength="30" CssClass="field_385" />
                                </div>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
            <div class="wrap-button">
                  <asp:Button runat="server" ID="btnNextStep1" UseSubmitBehavior="false" Text="<%$ Resources:Main, Next %>" Visible="false" OnClientClick='<%# GetNextScript("DebitDetails") %>' />
            </div>
            <div class="tick" id="tick-section-0<%#currentSectionIndex%>" style="display: none;"></div>
            <div class="spacer">
            </div>
        </div>
    </div>
	<asp:PlaceHolder runat="server" ID="phBillingAddress">
		<div id="step-0<%#phBillingAddress.Visible ? (currentSectionIndex += 1) : currentSectionIndex%>">
			<div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(2, false)">
				<div class="text">
					<asp:Label ID="ltBillingAddress" runat="server" Text="<%$ Resources:InstantDebitForm.ascx, ltBillingAddress %>" />
				</div>
				<div class="arrow-close">
				</div>
				<div class="spacer">
				</div>
			</div>
			<div id="section-0<%#currentSectionIndex%>" class="content-section" style="display: none;">
				<netpay:BillingAddress runat="server" ID="billingAddress" />
				<div class="wrap-button">
				<asp:Button runat="server" UseSubmitBehavior="false" Text="<%$ Resources:Main, Next %>" OnClientClick='<%# GetNextScript("BillingAddress") %>' />
				</div>
				<div class="tick" id="tick-section-0<%#currentSectionIndex%>" style="display: none;"></div>
				 <div class="spacer">
				</div>
			</div>
       
		</div>
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" ID="phShippingAddress">
		<div id="step-0<%#phShippingAddress.Visible ? (currentSectionIndex += 1) : currentSectionIndex%>">
			<div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(3, false)">
				<div class="text">
					<asp:Label ID="Label1" runat="server" Text="<%$ Resources:InstantDebitForm.ascx, ltShippingAddress %>" /></div>
				<div class="arrow-close">
				</div>
				<div class="spacer">
				</div>
			</div>
			<div id="section-0<%#currentSectionIndex%>" class="content-section" style="display:none;">
				<Netpay:ShippingAddress runat="server" ID="shippingAddress" />
				<div class="wrap-button">
						<asp:Button ID="Button3" runat="server" UseSubmitBehavior="false" Text="<%$ Resources:Main, Next %>" OnClientClick='<%# GetNextScript("ShippingAddress") %>' />
				</div>
				<div class="tick" id="tick-section-03" style="display:none;"></div>
				<div class="spacer">
				</div>
			</div>
		</div>
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" ID="phPersonalDetails">
		<div id="step-0<%#phPersonalDetails.Visible ? (currentSectionIndex += 1) : currentSectionIndex%>">
			<div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(4, false)">
				<div class="text">
					<asp:Label ID="lblPersonalDetails" runat="server" Text="<%$ Resources:InstantDebitForm.ascx, lblPersonalDetails %>" /></div>
				<div class="arrow-close">
				</div>
				<div class="spacer">
				</div>
			</div>
			<div id="section-0<%#currentSectionIndex%>" class="content-section" style="display:none;">
				<Netpay:PersonalDetails runat="server" ID="personalDetails" />
				<div class="wrap-button">
				<asp:Button runat="server" ID="btnNext" UseSubmitBehavior="false" Text="<%$Resources:Main, Next%>" OnClick="btnNext_Click" OnClientClick='<%# GetNextScript("PersonalDetails") %>' /></div>
				<div class="tick" id="tick-section-0<%#currentSectionIndex%>" style="display:none;"></div>
				<div class="spacer">
				</div>
			</div>
		</div>
	</asp:PlaceHolder>
</div>
