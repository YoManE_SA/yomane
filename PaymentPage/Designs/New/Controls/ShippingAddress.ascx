<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ShippingAddress.ascx.cs" Inherits="Netpay.PaymentPage.Designs.New.Controls.ShippingAddress" %>
<div id="dvSectionAddress" runat="server" visible="false" class="notifaction success"><asp:Literal runat="server" ID="ltrAddressSection" Text="<%$ Resources:ShippingAddress.ascx, txtAddressSection %>" /></div>
<asp:PlaceHolder runat="server" ID="tblShippingAddress">
	<script type="text/javascript">
		function disableShippingFields(bDisabled) {
			document.getElementById('txtShippingAddress').value = ''; document.getElementById('txtShippingAddress').disabled = bDisabled;
			document.getElementById('txtShippingAddressCity').value = ''; document.getElementById('txtShippingAddressCity').disabled = bDisabled;
			document.getElementById('txtShippingAddressZipCode').value = ''; document.getElementById('txtShippingAddressZipCode').disabled = bDisabled;
			document.getElementById('txtShippingAddress2').value = ''; document.getElementById('txtShippingAddress2').disabled = bDisabled;
			document.getElementById('ddShippingAddressCountry').value = ''; document.getElementById('ddShippingAddressCountry').disabled = bDisabled;
			document.getElementById('ddShippingAddressState').value = ''; document.getElementById('ddShippingAddressState').disabled = bDisabled;
		}
	</script>
    <div class="ShippingAddress" runat="server">
         <asp:CheckBox ID="chkUseSameAsBilling" onclick="disableShippingFields(this.checked)" runat="server" />
         <span><asp:Literal runat="server" Text="<%$Resources:Main,UseShippingSameAsBilling %>" /></span>
    </div>
	<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ShippingAddress" DisplayMode="SingleParagraph" CssClass="notifaction error" />
    <div class="table-section">
        <asp:PlaceHolder runat="server" ID="tdAddress1">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" ID="Literal1" Text="<%$ Resources:ShippingAddress.ascx, ltAddress1 %>" />
                    <span id="spanAddressRequired" runat="server" class="asterisk">*</span>
                </div>
                <div class="td-cell">
                    <asp:TextBox runat="server" ID="txtShippingAddress" ClientIDMode="Static" MaxLength="255" type="text" class="field_385" placeholder="Street address, P.O. box, company name, c/o" />
					<asp:RequiredFieldValidator ID="vShippingAddress" ControlToValidate="txtShippingAddress" Display="None" ErrorMessage="<%$ Resources:ShippingAddress.ascx, ValidatorAddress %>" ValidationGroup="ShippingAddress" runat="server" />
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="tdAddress2">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" Text="<%$ Resources:ShippingAddress.ascx, ltAddress2 %>" ID="ltAddress2" />
                </div>
                <div class="td-cell">
	                <asp:TextBox runat="server" ID="txtShippingAddress2" ClientIDMode="Static" MaxLength="255" type="text" class="field_385" placeholder="Apartment, suite, unit, building, floor, etc." />
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="tdCity">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" ID="ltCity" Text="<%$ Resources:ShippingAddress.ascx, ltCity %>" />
					<span id="spanCityRequired" runat="server" class="asterisk"> *</span>
                </div>
                <div class="td-cell">
                    <asp:TextBox runat="server" ID="txtShippingAddressCity" ClientIDMode="Static" type="text" class="field_385" required="required" />
					<asp:RequiredFieldValidator ID="vShippingAddressCity" ControlToValidate="txtShippingAddressCity" Display="None" ErrorMessage="<%$ Resources:ShippingAddress.ascx, ValidatorCity %>" ValidationGroup="ShippingAddress" runat="server" />
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="tdZipCode">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" ID="ltZipCode" Text="<%$ Resources:ShippingAddress.ascx, ltZipCode %>" />
					<span id="spanZipCodeRequired" runat="server" class="asterisk"> *</span>
                </div>
                <div class="td-cell">
                    <asp:TextBox runat="server" ID="txtShippingAddressZipCode" ClientIDMode="Static" class="field_385" required="required" />
					<asp:RequiredFieldValidator ID="vShippingAddressZipcode" ControlToValidate="txtShippingAddressZipCode" Display="None" ErrorMessage="<%$ Resources:ShippingAddress.ascx, ValidatorZipcode %>" ValidationGroup="ShippingAddress" runat="server" />
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="tdCountry">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" Text="<%$ Resources:ShippingAddress.ascx, ltCountry %>" ID="ltCountry" />
					<span id="spanCountryRequired" runat="server" class="asterisk"> *</span>
                </div>
                <div class="td-cell">
                    <div class="country-select">
						<netpay:CountryDropDown ID="ddShippingAddressCountry" ClientIDMode="Static" type="text" class="field_385" AutoPostBack="true" OnSelectedIndexChanged="ShippingCountryChange" runat="server" />
						<asp:RequiredFieldValidator ID="vShippingAddressCountry" ControlToValidate="ddShippingAddressCountry" Display="None" ErrorMessage="<%$ Resources:ShippingAddress.ascx, ValidatorCountry %>" ValidationGroup="ShippingAddress" runat="server" />
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="tdState">
            <div class="tr-table">
                <div class="td-cell-text">
                    <asp:Literal runat="server" Text="<%$ Resources:ShippingAddress.ascx, ltState %>" ID="ltState" />
                    <span id="spanStateRequired" runat="server" class="asterisk"> *</span>
                </div>
                <div class="td-cell">
                    <div class="state-select">
						<asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="false" RenderMode="Inline" UpdateMode="Conditional">
							<ContentTemplate>
									<netpay:StateDropDown ID="ddShippingAddressState" ClientIDMode="Static" StartEmpty="true" runat="server" type="text" class="field_385" />
									<asp:RequiredFieldValidator ID="vShippingAddressState" ControlToValidate="ddShippingAddressState" Display="None" ErrorMessage="<%$ Resources:ShippingAddress.ascx, ValidatorState %>" ValidationGroup="ShippingAddress" runat="server" />
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="ddShippingAddressCountry" />
							</Triggers>
						</asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
    </div>
</asp:PlaceHolder>
