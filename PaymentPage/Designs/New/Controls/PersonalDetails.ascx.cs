﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.PaymentPage.Designs.New.Controls
{
	public partial class PersonalDetails : Netpay.PaymentPage.Controls.PersonalDetails
	{
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Visible = tblPersonalDetails.Visible;
        }
    }
}