﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using System.Text;
using Netpay.PaymentPage.Code;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Configuration;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Designs.New.Controls
{
    public partial class InstantDebitForm : Netpay.PaymentPage.Controls.InstantDebitForm, System.Web.UI.IPostBackEventHandler
    {
		public int currentSectionIndex { get; set; }
		public int MaxSectionIndex { get; set; }
		protected override void OnLoad(EventArgs e)
		{
			if (Data.IsMobile) irlPaymentMethod.RepeatColumns = 1;
			base.OnLoad(e);
		}

        public void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument == string.Empty) btnNext_Click(this, EventArgs.Empty);
        }

        protected void btnNext_Click(object sender, EventArgs e)
	    {
		    Page.Validate();
		    if (Page.IsValid)
			    RaiseBubbleEvent(this, new PaymentPage.Code.AppActionEventArgs(Code.AppActionEventArgs.AppAction.Next));
	    }

		protected override string getCountryImage(string countryIso)
		{
            if (!string.IsNullOrEmpty(countryIso)) return string.Format("/NPCommon/ImgCountry/50X30/{0}.png", countryIso);
            return "/NPCommon/ImgCountry/18X12/Unknown.gif";
		}

        protected override string getMethodImage(int pmId, bool isBig)
        {
            string imageName = string.Format("/NPCommon/ImgPaymentMethod/MIX/{0}/{1}.png", (isBig ? "big" : "small"), pmId);
            if (!System.IO.File.Exists(Server.MapPath(imageName))) imageName = string.Format("/NPCommon/ImgPaymentMethod/MIX/{0}/unknown.png", (isBig ? "big" : "small"));
            return imageName;
        }

        protected override void PaymentMethodCountryChange(object sender, EventArgs e) 
        {
            base.PaymentMethodCountryChange(sender, e);
            btnNextStep1.Visible = false;
        }

        protected override void PaymentMethodChange(object sender, EventArgs e) 
        {
            base.PaymentMethodChange(sender, e);
            btnNextStep1.Visible = (irlPaymentMethod.SelectedValue != null);
        }

		protected override void OnPreRender(EventArgs e)
		{
			MaxSectionIndex = 1;
			if (phBillingAddress.Visible = billingAddress.Visible) MaxSectionIndex++;
			if (phShippingAddress.Visible = shippingAddress.Visible) MaxSectionIndex++;
			if (phPersonalDetails.Visible = personalDetails.Visible) MaxSectionIndex++;
			image_step2.Visible = (MaxSectionIndex >= 2);
			image_step3.Visible = (MaxSectionIndex >= 3);
			image_step4.Visible = (MaxSectionIndex >= 4);
			this.DataBind();
			base.OnPreRender(e);
		}

		public string GetNextScript(string validationGroup)
		{
            if (MaxSectionIndex == currentSectionIndex)
                return string.Format("if (validateSection('{0}', {1}))" + Page.ClientScript.GetPostBackEventReference(this, string.Empty) + ";Page_BlockSubmit = false;//", validationGroup, currentSectionIndex);
            else return string.Format("validateSection('{0}', {1}); Page_BlockSubmit = false;//", validationGroup, currentSectionIndex);
		}
    
    }
}