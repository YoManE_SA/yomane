﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PersonalDetails.ascx.cs" Inherits="Netpay.PaymentPage.Designs.New.Controls.PersonalDetails" %>
<asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="PersonalDetails" DisplayMode="SingleParagraph" CssClass="error notifaction" />
<asp:RequiredFieldValidator ID="vPhoneValidator" ControlToValidate="txtPhone" Display="None" ErrorMessage="<%$ Resources:PersonalDetails.ascx, ValidatorPhone %>" ValidationGroup="PersonalDetails"  runat="server" />
<asp:RegularExpressionValidator ID="vRegEmail" ControlToValidate="txtEmail" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" runat="server" ErrorMessage="<%$ Resources:PersonalDetails.ascx, Validator2Email %>" ValidationGroup="PersonalDetails" />
<asp:RequiredFieldValidator ID="vEMailValidator" ControlToValidate="txtEmail" Display="None" runat="server" ErrorMessage="<%$ Resources:PersonalDetails.ascx, Validator1Email %>" ValidationGroup="PersonalDetails" />
<asp:RequiredFieldValidator ID="vCardholderID" ControlToValidate="txtCardholderID" Display="None" ErrorMessage="<%$ Resources:PersonalDetails.ascx, ValidatorID %>" ValidationGroup="PersonalDetails" runat="server" />
<asp:PlaceHolder runat="server" ID="tblPersonalDetails">
	<asp:PlaceHolder runat="server" ID="trPersonalDetails">
		<div class="table-section">
			<asp:PlaceHolder ID="tdPhone" runat="server">
				<div class="tr-table">
					<div class="td-cell-text">
						<asp:Literal ID="ltPhone" runat="server" Text="<%$ Resources:PersonalDetails.ascx, ltPhone %>" /><span id="spanPhoneRequired" runat="server" class="asterisk"> *</span></div>
					<div class="td-cell">
						<asp:TextBox runat="server" ID="txtPhone" MaxLength="30" class="field_385" />
					</div>
				</div>
			</asp:PlaceHolder>
			<asp:PlaceHolder runat="server" ID="tdEmail">
				<div class="tr-table">
					<div class="td-cell-text">
						<asp:Literal ID="ltEmail" runat="server" Text="<%$ Resources:PersonalDetails.ascx, ltEmail %>" /><span id="spanEmailRequired" runat="server" class="asterisk"> *</span></div>
					<div class="tr-table">
						<asp:TextBox runat="server" ID="txtEmail" MaxLength="255" type="email" placeholder="Example@example.com" class="field_385" />
					</div>
				</div>
			</asp:PlaceHolder>
			<asp:PlaceHolder runat="server" ID="tdCardholderID">
				<div class="tr-table">
					<div class="td-cell-text">
						<asp:Literal ID="ltCardholderID" runat="server" Text="<%$ Resources:PersonalDetails.ascx, ltCardholderID %>" />
						<span id="spanIDRequired" runat="server" class="asterisk">*</span></div>
					<div class="tr-table">
						<asp:TextBox runat="server" ID="txtCardholderID" MaxLength="30" class="field_385" />
					</div>
				</div>
			</asp:PlaceHolder>

			<asp:PlaceHolder runat="server" ID="tdBirthDate">
                <div class="tr-table">
                    <div class="td-cell-text"> 
                       <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:ElectronicCheckForm.ascx, ltbirthdate %>" />
                    </div>
                    <div class="td-cell">
                        <netpay:DatePicker ID="dpBirthDate" required="required" ShowMonth="true" class="field_385" ShowYear="true" TabIndex="5" runat="server" />
                    </div>
                </div>
			</asp:PlaceHolder>
		</div>
	</asp:PlaceHolder>
</asp:PlaceHolder>
