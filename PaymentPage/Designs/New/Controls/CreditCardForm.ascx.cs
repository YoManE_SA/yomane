﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.PaymentPage.Designs.New.Controls
{
	public partial class CreditCardForm : PaymentPage.Controls.CreditCardForm, System.Web.UI.IPostBackEventHandler
	{
		public int currentSectionIndex { get; set; }
		public int MaxSectionIndex { get; set; }
		protected string DefaultCardImage = "";
		protected override void OnLoad(EventArgs e)
		{
			DefaultCardImage = string.Format("~/{0}/Design/New/images/Card.png", Netpay.Web.WebUtils.CurrentDomain.ThemeFolder);
			imgTypeCard.ImageUrl = DefaultCardImage;
			if (rptPaymentMethodIcons.Items.Count == 0) {
				rptPaymentMethodIcons.DataSource = Data.AllPaymentMethods;
			}
           
			base.OnLoad(e);
		}

        public void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument == string.Empty) btnNext_Click(this, EventArgs.Empty);
        }

        protected void btnNext_Click(object sender, EventArgs e)
		{
			Page.Validate();
			if (Page.IsValid)
				RaiseBubbleEvent(this, new PaymentPage.Code.AppActionEventArgs(Code.AppActionEventArgs.AppAction.Next));
		}

		protected override void OnPreRender(EventArgs e)
		{
			MaxSectionIndex = 1;
			if (phBillingAddress.Visible = billingAddress.Visible) MaxSectionIndex++;
			if (phShippingAddress.Visible = shippingAddress.Visible) MaxSectionIndex++;
			if (phPersonalDetails.Visible = personalDetails.Visible) MaxSectionIndex++;
			image_step2.Visible = (MaxSectionIndex >= 2);
			image_step3.Visible = (MaxSectionIndex >= 3);
			image_step4.Visible = (MaxSectionIndex >= 4);
			this.DataBind();
			base.OnPreRender(e);
		}

		protected void CreditCard_Change(object sender, EventArgs e)
		{
			int value = (int)wcCreditcard.Card.PaymentMethodId;
			if (value == 0) {
				imgTypeCard.ImageUrl = DefaultCardImage;
				rptPaymentMethodIcons.DataSource = Data.AllPaymentMethods;
			} else {
				rptPaymentMethodIcons.DataSource = Data.AllPaymentMethods.Where(v => v.ID == value);
                imgTypeCard.ImageUrl = string.Format("/NPCommon/ImgPaymentMethod/50X30/{0}.png", value);
			}
			rptPaymentMethodIcons.DataBind();
		}

		public string GetNextScript(string validationGroup) 
		{
			if (MaxSectionIndex == currentSectionIndex)
				return string.Format("if (validateSection('{0}', {1}))" + Page.ClientScript.GetPostBackEventReference(this, string.Empty) + ";Page_BlockSubmit = false;//", validationGroup, currentSectionIndex);
			else return string.Format("validateSection('{0}', {1}); Page_BlockSubmit = false;//", validationGroup, currentSectionIndex);
		}

    }
}