﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.PaymentPage.Designs.New.Controls
{
	public partial class ElectronicCheckForm : Netpay.PaymentPage.Controls.ElectronicCheckForm, System.Web.UI.IPostBackEventHandler
    {
		public int currentSectionIndex { get; set; }
		public int MaxSectionIndex { get; set; }

        public void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument == string.Empty) btnNext_Click(this, EventArgs.Empty);
        }

        protected void btnNext_Click(object sender, EventArgs e)
		{
			Page.Validate();
			if (Page.IsValid)
				RaiseBubbleEvent(this, new PaymentPage.Code.AppActionEventArgs(Code.AppActionEventArgs.AppAction.Next));
		}

		protected override void OnPreRender(EventArgs e)
		{
			MaxSectionIndex = 1;
			if (phBillingAddress.Visible = billingAddress.Visible) MaxSectionIndex++;
			if (phShippingAddress.Visible = shippingAddress.Visible) MaxSectionIndex++;
			if (phPersonalDetails.Visible = personalDetails.Visible) MaxSectionIndex++;
			image_step2.Visible = (MaxSectionIndex >= 2);
			image_step3.Visible = (MaxSectionIndex >= 3);
			image_step4.Visible = (MaxSectionIndex >= 4);
			this.DataBind();
			base.OnPreRender(e);
		}

		public string GetNextScript(string validationGroup)
		{
            if (MaxSectionIndex == currentSectionIndex)
                return string.Format("if (validateSection('{0}', {1}))" + Page.ClientScript.GetPostBackEventReference(this, string.Empty) + ";Page_BlockSubmit = false;//", validationGroup, currentSectionIndex);
            else return string.Format("validateSection('{0}', {1}); Page_BlockSubmit = false;//", validationGroup, currentSectionIndex);
        }
    }
}