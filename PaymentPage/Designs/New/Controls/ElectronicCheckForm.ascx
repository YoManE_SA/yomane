﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ElectronicCheckForm.ascx.cs" Inherits="Netpay.PaymentPage.Designs.New.Controls.ElectronicCheckForm" %>
<%@ Register Src="BillingAddress.ascx" TagPrefix="Netpay" TagName="BillingAddress" %>
<%@ Register Src="ShippingAddress.ascx" TagPrefix="Netpay" TagName="ShippingAddress" %>
<%@ Register Src="personalDetails.ascx" TagPrefix="Netpay" TagName="PersonalDetails" %>

<script type="text/javascript">
	var currentTabIndex = 1;
	var maxStep = 0;

	function validateSection(validationGroup, sectionId) {
		var ret = Page_ClientValidate(validationGroup);
		if (ret) {
			$("#tick-section-0" + sectionId).fadeIn();
			openSection(sectionId + 1, true);
		} else $("#tick-section-0" + sectionId).fadeOut();
		return ret;
	}

	function openSection(sectionId, approve) {
		if (maxStep == 0)
			for (maxStep = 1; document.getElementById('section-0' + maxStep) ; maxStep++);
		if (currentTabIndex == sectionId) return;
		if (!approve && currentTabIndex < sectionId) return;
		if (sectionId >= maxStep) return;

		$("#section-0" + currentTabIndex).slideUp();
		$("#section-0" + sectionId).slideDown();

		$("[class ^= arrow]").attr("class", "arrow-close");
		$("#click-selesction-0" + sectionId + " > [class ^= arrow]").attr("class", "arrow");
		for (var i = 1; i <= maxStep; i++)
			$("#image_step" + i).attr('class', 'step-0' + i + (i <= sectionId ? '-on' : '-off'));

		currentTabIndex = sectionId
	}
</script>
<div id="step">
	<span id="image_step1" class="step-01-on"></span><span id="image_step2"  ClientIDMode="Static" runat="server"  class="step-02-off"></span><span id="image_step3"  ClientIDMode="Static" runat="server" class="step-03-off"></span><span id="image_step4" ClientIDMode="Static" runat="server" class="step-04-off"></span>
</div>
<div class="wrap-accordion">
    <div id="step-0<%#++currentSectionIndex%>">
        <div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(<%#currentSectionIndex%>, false)">
            <div class="text">
                <asp:Literal ID="Literal" runat="server" Text="<%$ Resources:ElectronicCheckForm.ascx, lblAccountDetalis %>" />
            </div>
            <div class="arrow">
            </div>
            <div class="spacer">
            </div>
        </div>
        <div id="section-0<%#currentSectionIndex%>" class="content-section">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ACHDetails" DisplayMode="SingleParagraph" CssClass="notifaction error" />
            <asp:RequiredFieldValidator ID="vAccountName" ControlToValidate="txtAccountName" Display="none" ValidationGroup="ACHDetails" ErrorMessage="<%$ Resources:ElectronicCheckForm.ascx, ValidatorAccountName %>" runat="server" />
            <asp:RequiredFieldValidator ID="vAccountNumber" ControlToValidate="txtAccountNumber" Display="none" ValidationGroup="ACHDetails" ErrorMessage="<%$ Resources:ElectronicCheckForm.ascx, ValidatorAccountNumber %>" runat="server" />
            <asp:RequiredFieldValidator ID="vRoutingNumber" ControlToValidate="txtRoutingNumber" Display="none" ValidationGroup="ACHDetails" ErrorMessage="<%$ Resources:ElectronicCheckForm.ascx, ValidatorRoutingNumber %>" runat="server" />
            
            <div class="table-section">
                <div class="tr-table">
                    <div class="td-cell-text">
                           <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:ElectronicCheckForm.ascx, lblAccountName %>" /><span class="asterisk"> *</span>
                    </div>
                    <div class="td-cell">
                        <asp:TextBox ID="txtAccountName" TabIndex="1" MaxLength="50" CssClass="field_385" autofocus="autofocus" type='text' required="required" runat="server" />
                    </div>
                </div>
                <div class="tr-table">
                    <div class="td-cell-text">
                         <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:ElectronicCheckForm.ascx, lblAcoountNumber %>" />
                    </div>
                    <div class="td-cell">
                        <asp:TextBox ID="txtAccountNumber" TabIndex="2" MaxLength="50" CssClass="field_385" type='text' required="required" runat="server" />
                    </div>
                </div>
                <div class="tr-table">
                    <div class="td-cell-text">
                       <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:ElectronicCheckForm.ascx, ltRoutingNumber %>" /><span class="asterisk"> *</span>
                    </div>
                    <div class="td-cell">
                        <asp:TextBox ID="txtRoutingNumber" TabIndex="4" MaxLength="50" CssClass="field_385" runat="server" required="required" />
                    </div>
                </div>
            </div>
            <div class="wrap-button">
                <asp:Button ID="Button1" runat="server" UseSubmitBehavior="false" Text="<%$ Resources:Main, Next %>" OnClientClick='<%# GetNextScript("ACHDetails") %>' />
            </div>
            <div class="tick" id="tick-section-01" style="display: none;"></div>
            <div class="spacer">
            </div>
        </div>
    </div>
	<asp:PlaceHolder runat="server" ID="phBillingAddress">
		<div id="step-0<%# phBillingAddress.Visible ? (currentSectionIndex += 1) : currentSectionIndex %>">
			<div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(<%#currentSectionIndex%>, false)">
				<div class="text">
					<asp:Label ID="ltBillingAddress" runat="server" Text="<%$ Resources:ElectronicCheckForm.ascx, ltBillingAddress %>" />
				</div>
				<div class="arrow-close">
				</div>
				<div class="spacer">
				</div>
			</div>
			<div id="section-0<%#currentSectionIndex%>" class="content-section" style="display: none;">
				<netpay:BillingAddress runat="server" ID="billingAddress" />
				<div class="wrap-button">
					<asp:Button ID="Button2" runat="server" UseSubmitBehavior="false" Text="<%$ Resources:Main, Next %>" OnClientClick='<%# GetNextScript("BillingAddress") %>' />
				</div>
				<div class="tick" id="tick-section-0<%#currentSectionIndex%>" style="display: none;"></div>
				<div class="spacer">
				</div>
			</div>
		</div>
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" ID="phShippingAddress">
		<div id="step-0<%#phShippingAddress.Visible ? (currentSectionIndex += 1) : currentSectionIndex%>">
			<div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(<%#currentSectionIndex%>, false)">
				<div class="text">
					<asp:Label ID="Label1" runat="server" Text="<%$ Resources:ElectronicCheckForm.ascx, ltShippingAddress %>" /></div>
				<div class="arrow-close">
				</div>
				<div class="spacer">
				</div>
			</div>
			<div id="section-0<%#currentSectionIndex%>" class="content-section" style="display:none;">
				<Netpay:ShippingAddress runat="server" ID="shippingAddress" />
				<div class="wrap-button">
					  <asp:Button ID="Button3" runat="server" UseSubmitBehavior="false" Text="<%$ Resources:Main, Next %>" OnClientClick='<%# GetNextScript("ShippingAddress") %>' />
				</div>
				<div class="tick" id="tick-section-0<%#currentSectionIndex%>" style="display:none;"></div>
				<div class="spacer">
				</div>
			</div>
		</div>
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" ID="phPersonalDetails">
		<div id="step-0<%#phPersonalDetails.Visible ? (currentSectionIndex += 1) : currentSectionIndex%>">
			<div id="click-selesction-0<%#currentSectionIndex%>" class="accordion" onclick="openSection(<%#currentSectionIndex%>, false)">
				<div class="text">
					<asp:Label ID="lblPersonalDetails" runat="server" Text="<%$ Resources:ElectronicCheckForm.ascx, lblPersonalDetails %>" /></div>
				<div class="arrow-close">
				</div>
				<div class="spacer">
				</div>
			</div>
			<div id="section-0<%#currentSectionIndex%>" class="content-section" style="display:none;">
				<Netpay:PersonalDetails runat="server" ID="personalDetails" />
				<div class="wrap-button">
				<asp:Button runat="server" ID="btnNext" UseSubmitBehavior="false" Text="<%$Resources:Main, Next%>" OnClick="btnNext_Click" OnClientClick='<%# GetNextScript("PersonalDetails") %>' /></div>
				<div class="tick" id="tick-section-04" style="display:none;"></div>
				<div class="spacer">
				</div>
			</div>
		</div>
	</asp:PlaceHolder>
</div>

