﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.PaymentPage.Designs.New.Controls
{
	public partial class PhoneForm : Netpay.PaymentPage.Controls.PhoneForm, System.Web.UI.IPostBackEventHandler
    {
        public int currentSectionIndex { get; set; }
        public int MaxSectionIndex { get; set; }
        public void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument == string.Empty) btnNext_Click(this, EventArgs.Empty);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
                RaiseBubbleEvent(this, new PaymentPage.Code.AppActionEventArgs(Code.AppActionEventArgs.AppAction.Next));
        }

        public string GetNextScript(string validationGroup)
        {
            if (MaxSectionIndex == currentSectionIndex)
                return string.Format("if (validateSection('{0}', {1}))" + Page.ClientScript.GetPostBackEventReference(this, string.Empty) + ";Page_BlockSubmit = false;//", validationGroup, currentSectionIndex);
            else return string.Format("validateSection('{0}', {1}); Page_BlockSubmit = false;//", validationGroup, currentSectionIndex);
        }
    }
}