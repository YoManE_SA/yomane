﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Web;

namespace Netpay.PaymentPage.Designs.New
{
	public partial class Main : Netpay.PaymentPage.Main
	{


		protected override void RegisterCss()
		{
            lblCompanyName.Text = GetGlobalResourceObject("Main", "Slogan").ToString().Replace("%LEGAL_NAME%", WebUtils.CurrentDomain.LegalName);
			//base.RegisterCss();
			string themeFolder = "Templates/" + WebUtils.CurrentDomain.ThemeFolder;
			if (Data != null && Data.IsMobile)
			{
				RegisterCssFile(ResolveUrl("~/" + themeFolder + "/Design/New/Styles/mobile.css"));
				RegisterCssFile(ResolveUrl("~/" + themeFolder + string.Format("/Design/New/Styles/mobile{0}.css", WebUtils.CurrentDirection)));
			} else {
				RegisterCssFile(ResolveUrl("~/" + themeFolder + "/Design/New/Styles/style.css"));
				RegisterCssFile(ResolveUrl("~/" + themeFolder + string.Format("/Design/New/Styles/style{0}.css", WebUtils.CurrentDirection)));
			}
			if (Data!= null) {
				RegisterCssFile(ResolveUrl(Data.DesignDir + string.Format("theme/{0}/Styles/Style.css", Data.CustomCss)));
	      
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			if (wc2LanguageSelector != null)
            {
                //wcLanguageSelector.DirectionFlip = WebUtils.CurrentDirection == "rtl";
                if (Request.PhysicalPath.ToLower().IndexOf("hosted\\default.aspx") > -1 && Request.QueryString["merchantID"] != null)
					wc2LanguageSelector.AlternateDestination = Request.Path + "?LoadPrev=1";
				wc2LanguageSelector.Cultures.AddRange(getCultures());
            }
			base.OnLoad(e);
		}	
	}
}