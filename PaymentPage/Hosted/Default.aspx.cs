﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Hosted
{
	public partial class Default : HostedPageBase
	{
        public bool ShowPayButton { get; set; }
        protected override void InitHostedContext()
        {
            var req = HttpContext.Current.Request;
            if (req["merchantID"] != null && req["trans_amount"] != null && !IsPostBack) //&& !IsPostBack
                Init("~/Hosted/default.aspx", true);
            else base.InitHostedContext();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ShowPayButton = true;
            if (Data != null) {
                if (PaymentSettings == null || !PaymentSettings.IsEnabled) RaiseError(Resources.Main.ErrorSolutionDisabled);
                else SetActiveView(Request.Form["PaymentMethodType"].ToNullableEnumByValue<PaymentMethodType>().GetValueOrDefault(Data.PaymentMethodType), Request.Form["PaymentMethod"].ToNullableEnumByValue<PaymentMethodEnum>().GetValueOrDefault(Data.PaymentMethod));
            } else RaiseError(Resources.Main.ErrorSessionEnded);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            OnBubbleEvent(this, new ElementVisibleEventArgs(ElementVisibleEventArgs.ElementType.BottomCustomize, true));
        }

        protected override bool OnBubbleEvent(object source, EventArgs e)
        {
            ElementVisibleEventArgs elementVisibleEventArgs = e as ElementVisibleEventArgs;
            if (elementVisibleEventArgs != null && elementVisibleEventArgs.elementType == ElementVisibleEventArgs.ElementType.NextButton) {
                ShowPayButton = elementVisibleEventArgs.Visible;
                if (btnPay != null) btnPay.Visible = ShowPayButton;
            }
			AppActionEventArgs appActionEventArgs = e as AppActionEventArgs;
			if (appActionEventArgs != null) {
				if(appActionEventArgs.Action == AppActionEventArgs.AppAction.Next)
					Pay_Click(source, EventArgs.Empty);
			}
            return base.OnBubbleEvent(source, e);
        }
        
		protected void PaymentSolutions_PaymentMethodChanged(object sender, EventArgs e)
		{
            ShowPayButton = true;
            OnBubbleEvent(this, new ElementVisibleEventArgs(ElementVisibleEventArgs.ElementType.SelectMethod, true));
            phControl.Controls.Clear();
            SetActiveView(Data.PaymentMethodType, Data.PaymentMethod);
        }

        protected void Pay_Click(object sender, EventArgs e)
        {
            //Add here check of the logged in Customer pincode.
            if (phControl.Controls.Count == 0) return;
            IFormControl control = phControl.Controls[0] as IFormControl;
            if (control != null) control.Validate();
            Page.Validate();
            if (!Page.IsValid) return;
            if (control != null) control.SaveData();
			Redirect("~/Hosted/PaymentConfirm.aspx");
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (btnPay != null) btnPay.Visible = ShowPayButton;
            if (Data != null) { 
			    Page.ClientScript.RegisterHiddenField("PaymentMethodType", ((int)Data.PaymentMethodType).ToString());
			    Page.ClientScript.RegisterHiddenField("PaymentMethod", ((int)Data.PaymentMethod).ToString());
            }
            base.OnPreRender(e);
        }

        protected void SetActiveView(PaymentMethodType pmType, PaymentMethodEnum pm)
		{
			Control viewControl = null;
            if (tblContinue != null) tblContinue.Visible = true;
            switch (pmType)
			{
				case PaymentMethodType.Unknown:
                    viewControl = LoadControl(MapDesignPage(Data, "~/Controls/PaymentSolutions.ascx"));
                    (viewControl as Controls.PaymentSolutions).PaymentMethodChanged += PaymentSolutions_PaymentMethodChanged;
                    if (tblContinue != null) tblContinue.Visible = false;
					break;
				case PaymentMethodType.CreditCard:
                    if (PaymentSettings.IsCreditCard) viewControl = LoadControl(MapDesignPage(Data, "~/Controls/CreditCardForm.ascx"));
					break;
				case PaymentMethodType.ECheck:
					if (PaymentSettings.IsEcheck) viewControl = LoadControl(MapDesignPage(Data, "~/Controls/ElectronicCheckForm.ascx"));
					break;
				case PaymentMethodType.BankTransfer:
                    if (PaymentSettings.IsDirectDebit) viewControl = LoadControl(MapDesignPage(Data, "~/Controls/InstantDebitForm.ascx"));
					break;
				case PaymentMethodType.PhoneDebit:
                    if (!PaymentSettings.IsPhoneDebit) break;
                    switch (pm) 
                    { 
                        case PaymentMethodEnum.MicroPaymentsIN:
                        case PaymentMethodEnum.MicroPaymentsOut:
                        case PaymentMethodEnum.IvrPerCall:
                        case PaymentMethodEnum.IvrPerMin:
                        case PaymentMethodEnum.IvrFreeTime:
                            viewControl = LoadControl(MapDesignPage(Data, "~/Controls/PhoneAtalsForm.ascx"));
                            break;
                        case PaymentMethodEnum.Qiwi:
                            viewControl = LoadControl(MapDesignPage(Data, "~/Controls/PhoneForm.ascx"));
                            break;
                    }
                    break;
				case PaymentMethodType.Wallet:
                    if (PaymentSettings.IsCustomer) viewControl = LoadControl(MapDesignPage(Data, "~/Controls/CustomerLogin.ascx"));
					break;
				case PaymentMethodType.ExternalWallet:
					switch (pm)
					{
						case PaymentMethodEnum.WebMoney:
                            if (PaymentSettings.IsWebMoney) viewControl = LoadControl(MapDesignPage(Data, "~/Controls/WebMoneyForm.ascx"));
							break;
						case PaymentMethodEnum.PayPal:
							externalRedirect(ResolveUrl(MapDesignPage(Data, "~/Hosted/PayPal/Frame.aspx")), RedirectWindow.SameWindow);
							break;
						case PaymentMethodEnum.GoogleCheckout:
							externalRedirect(ResolveUrl(MapDesignPage(Data, "~/Hosted/GoogleCheckout/Frame.aspx")), RedirectWindow.SameWindow);
							break;
                        case PaymentMethodEnum.MoneyBookersHpp:
							externalRedirect(ResolveUrl(MapDesignPage(Data, "~/Hosted/MoneyBookers/Frame.aspx")), RedirectWindow.UseIFrame);
                            break;
                        default:
							throw new ApplicationException("invalid external wallet");
					}
					break;
			}
            if (viewControl == null) {
                Label ltDisabled = new Label();
				ltDisabled.CssClass = "SolutionDisabled";
                ltDisabled.Text = Resources.Main.ErrorSolutionDisabled;
                phControl.Controls.Add(ltDisabled);
                if (tblContinue != null) tblContinue.Visible = false;
            } else {
                viewControl.ID = "payControl";
                phControl.Controls.Add(viewControl);
                if (viewControl is IFormControl) (viewControl as IFormControl).LoadData();
            }

            if (pm == PaymentMethodEnum.Unknown) {
				if (viewControl is Controls.PaymentSolutions) Page.Title = GetGlobalResourceObject("Hosted.Default.aspx", "MainHeadPaymentSolutions").ToString();
				else Page.Title = GetGlobalResourceObject("Hosted.Default.aspx", "PayUsing").ToString() + " " + GetGlobalResourceObject("PaymentMethods", "PMG_" + (int)Data.PaymentMethodGroup);
            } else if (pm != PaymentMethodEnum.Unknown)
                Page.Title = GetGlobalResourceObject("Hosted.Default.aspx", "PayUsing").ToString() + " " + GetGlobalResourceObject("PaymentMethods", "PM_" + (int)Data.PaymentMethod);
        }
	}
}