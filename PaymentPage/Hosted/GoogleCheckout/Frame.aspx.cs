﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Infrastructure;
using System.IO;

namespace Netpay.PaymentPage.Hosted.GoogleCheckout
{
	public partial class Frame : Netpay.PaymentPage.Code.HostedPageBase
	{
		protected string _formAction = null;
		protected string _redirectUrl = null;

        protected override void OnPreInit(EventArgs e) { }
        protected void Page_Load(object sender, EventArgs e)
		{
            if (string.IsNullOrEmpty(PaymentSettings.GoogleCheckoutMerchantID))
            {
                RaiseError("GoogleCheckout MerchantID not configured");
                return;
            }

            if (Infrastructure.Application.IsProduction)
            {
                _redirectUrl = Request.Url.ReplaceFileName("GoogleCheckout/Callback.aspx");
                _formAction = string.Format("https://checkout.google.com/api/checkout/v2/checkoutForm/Merchant/{0}", PaymentSettings.GoogleCheckoutMerchantID);
            } else {
                _redirectUrl = "http://212.150.167.145:8080/";
                _formAction = string.Format("https://sandbox.google.com/checkout/api/checkout/v2/checkoutForm/Merchant/{0}", PaymentSettings.GoogleCheckoutMerchantID);
            }
            
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine(string.Format("<form action=\"{0}\" id=\"BB_BuyButtonForm\" method=\"post\" name=\"BB_BuyButtonForm\">", _formAction));
            sb.AppendLine(string.Format("<input name=\"item_name_1\" type=\"hidden\" value=\"{0}\" />", Data.disp_payFor));
            sb.AppendLine("<input name=\"item_description_1\" type=\"hidden\" value=\"\" />");
            sb.AppendLine("<input name=\"item_quantity_1\" type=\"hidden\" value=\"1\"/>");
            sb.AppendLine(string.Format("<input name=\"item_price_1\" type=\"hidden\" value=\"{0}\" />", Data.trans_amount));
            sb.AppendLine(string.Format("<input name=\"item_currency_1\" type=\"hidden\" value=\"{0}\" />", Data.trans_currency));
            sb.AppendLine(string.Format("<input name=\"continue_url\" type=\"hidden\" value=\"{0}\" />", _redirectUrl));
            sb.AppendLine(string.Format("<input name=\"shopping-cart.items.item-1.merchant-item-id\" type=\"hidden\" value=\"{0}\" />", Data.LogPaymentPage.ID));
            sb.AppendLine("<input name=\"_charset_\" type=\"hidden\" value=\"utf-8\" />");
            sb.AppendLine("</form>");

            ltFormData.Text = sb.ToString();
            Data.SaveLogTrans(ltFormData.Text, null, CommonTypes.PaymentMethodType.ExternalWallet);
		}
	}
}