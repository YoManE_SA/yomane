﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.PaymentPage.Code;
using System.Collections.Specialized;

namespace Netpay.PaymentPage.Hosted.GoogleCheckout
{
	public partial class Callback : HostedPageBase
    {
        protected override void OnInit(EventArgs e)
        {
            //base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
		{
            int paymentLogID = 0;
            if (int.TryParse(Request["shopping-cart.items.item-1.merchant-item-id"].Trim(), out paymentLogID))
            {
                /*
                financial-order-state:
                REVIEWING - Google Checkout is reviewing the order.
                CHARGEABLE - The order is ready to be charged.
                CHARGING - The order is being charged; you may not refund or cancel an order until is the charge is completed.
                CHARGED - The order has been successfully charged; if the order was only partially charged, the buyer's account page will reflect the partial charge.
                PAYMENT_DECLINED - The charge attempt failed.
                CANCELLED - Either the buyer or the seller canceled the order. An order's financial state cannot be changed after the order is canceled. Learn more about when an order may be canceled.
                CANCELLED_BY_GOOGLE - Google canceled the order. Google may cancel orders due to a failed charge without a replacement credit card being provided within a set period of time or due to a failed risk check. If Google cancels an order, you will be notified of the reason the order was canceled in the reason parameter of an order-state-change-notification.
                */
                string notificationType = Request["_type"];
                if (notificationType == null) notificationType = string.Empty;
                else notificationType = notificationType.Trim();
                if (notificationType != "new-order-notification" && notificationType != "order-state-change-notification") return;

				var lastTrans = Data.LogPaymentPage.GetLastTransactionLog();
                if (lastTrans == null) throw new Exception("google checkout callback could not found log payment page #" + paymentLogID);
                lastTrans.ResponseString = "QueryString: " + Request.QueryString.ToString() + "\r\nForm: " + Request.Form.ToString();
				lastTrans.Save();
                Response.End();
            } else {
                try {
					var lastTrans = Data.LogPaymentPage.GetLastTransactionLog();
                    var nameValueColl = HttpUtility.ParseQueryString(lastTrans.ResponseString);
                    string replyCode = null;
                    string transID = nameValueColl["google-order-number"].Trim();
                    string orderState = nameValueColl["financial-order-state"].Trim();
                    switch (orderState)
                    {
                        case "REVIEWING":
                        case "CHARGEABLE":
                        case "CHARGING":
                            replyCode = "001"; break;
                        case "CHARGED":
                            replyCode = "000"; break;
                        case "PAYMENT_DECLINED":
                        case "CANCELLED":
                        case "CANCELLED_BY_GOOGLE":
                            replyCode = "548"; break;
                        default:
                            replyCode = "520"; break;
                    }
                    ValidateExternalReturn(transID, "Google Checkout", replyCode, orderState, true, RedirectWindow.SameWindow);
                }
                catch (Exception ex)
                {
                    Logger.Log(LogTag.PaymentPage, ex);
                }
            }
		}
	}
}