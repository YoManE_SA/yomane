﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Netpay.Bll;
using Netpay.Infrastructure;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using Netpay.PaymentPage.Code;

namespace Netpay.PaymentPage.Hosted
{
    public partial class PaymentPagePinCode : HostedPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Data == null) RaiseError(Resources.Main.ErrorSessionEnded);
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			StringBuilder requestBuilder = new StringBuilder();
			requestBuilder.Append(CurrentDomain.ProcessUrl + "remoteCharge_echeck.aspx");
			requestBuilder.Append("?CompanyNum=" + Data.merchantID);
			requestBuilder.Append("&RefTransID=" + Request["trans_id"]);
			requestBuilder.Append("&Currency=" + Data.Currency.ID);
			requestBuilder.Append("&TransType=" + (int)TransactionType.PendingCapture);
			requestBuilder.Append("&requestSource=" + (int)TransactionSource.HostedPageV2En);
			requestBuilder.Append("&PinCode=" + txtPinCode.Text);
			requestBuilder.Append("&PhoneNumber=" + txtPhone.Text);

			string responseData = null;
			try { Data.SendChargeRequest(requestBuilder.ToString(), out responseData, Data.PaymentMethodType); }
			catch (Exception ex) { txtResponse.Text = ex.Message; return; }
			txtResponse.Text = ValidateChargeReturn(HttpUtility.ParseQueryString(responseData), false);
			txtResponse.Visible = true;
		}
	}
}