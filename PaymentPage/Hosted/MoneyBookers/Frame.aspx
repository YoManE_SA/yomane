﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Frame.aspx.cs" Inherits="Netpay.PaymentPage.Hosted.MoneyBookers.Frame" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title></title>
	</head>
	<body>
		<form action="https://www.moneybookers.com/app/payment.pl" id="BB_BuyButtonForm" method="post" name="BB_BuyButtonForm">
            <input name="pay_to_email" type="hidden" value="<%= Settings.MoneyBookersAccount %>" />
			<input name="recipient_description" type="hidden" value="<%= Data.PayFor %>"/>
			<input name="amount" type="hidden" value="<%= Data.Amount %>"/>
			<input name="currency" type="hidden" value="<%= Data.Currency.IsoCode %>"/>
			<input name="return_url" type="hidden" value="<%= _redirectUrl %>"/>
			<input name="cancel_url" type="hidden" value="<%= _redirectUrl %>"/>
			<input name="transaction_id" type="hidden" value="<%= Data.LogPaymentPage.PaymentPage_id %>" />
            <input name="language" type="hidden" value="EN"/>
		</form>
        <script type="text/javascript"> document.forms[0].submit(); </script>
	</body>
</html>
