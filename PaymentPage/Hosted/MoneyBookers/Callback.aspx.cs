﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.PaymentPage.Code;
using System.Collections.Specialized;

namespace Netpay.PaymentPage.Hosted.MoneyBookers
{
	public partial class Callback : HostedPageBase
	{
        protected override void OnInit(EventArgs e)
        {
            //base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
                int paymentLogID = int.Parse(Request["merchant_id"].Trim());
                string transID = Request["mb_transaction_id"].Trim();
				string replyCode = null;
                string orderState = Request["status"].Trim();
				if (orderState == "0") replyCode = "001";
				else if (orderState == "2") replyCode = "000";

                if (LoadData(paymentLogID))
                    ValidateExternalReturn(transID, "MoneyBookers", replyCode, null, true, RedirectWindow.UseIFrame);
			}
			catch (Exception ex)
			{
				Logger.Log(LogTag.PaymentPage, ex);
			}

			//Netpay.Infrastructure.Logger.Log(LogSeverity.Error, LogTag.PaymentPage, "Google notification successful, SendClientReply result: " + sendClientReplyResult);
		}
	}
}