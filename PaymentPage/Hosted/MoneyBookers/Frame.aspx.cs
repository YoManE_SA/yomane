﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Infrastructure;
using System.IO;

namespace Netpay.PaymentPage.Hosted.MoneyBookers
{
	public partial class Frame : Netpay.PaymentPage.Code.HostedPageBase
	{
		protected string _formAction = null;
		protected string _redirectUrl = null;
        protected override void OnPreInit(EventArgs e) { }
		protected void Page_Load(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(PaymentSettings.MoneyBookersAccount))
			{
				RaiseError("MoneyBookers Account not configured");
				return;
			}
            _redirectUrl = Infrastructure.Application.IsProduction ? Request.Url.ReplaceFileName("MoneyBookers/Callback.aspx") : "http://212.150.167.145:8080/";
			_formAction = "https://www.moneybookers.com/app/payment.pl";
		}
	}
}