﻿using System;
using System.Web;
using Netpay.PaymentPage.Code;

namespace Netpay.PaymentPage.Hosted
{
    public partial class HostWindow : HostedPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterStartupScript(GetType(), "HostOpenWindow", string.Format("window.open('{0}');", Request["content"]), true);
        }
    }
}