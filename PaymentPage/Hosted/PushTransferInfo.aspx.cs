﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using Netpay.Bll;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Hosted
{
	public partial class PushTransferInfo : HostedPageBase
	{
		protected void CheckTransfer_OnClick(object sender, EventArgs e)
		{
			int transID;
			TransactionStatus transStatus;
			int pendingTransID = Request["trans_id"].ToInt32(0);
			if (Bll.Transactions.Transaction.PendingResult(pendingTransID, out transStatus, out transID))
			{
				if (transStatus == TransactionStatus.Captured) {
					ValidateChargeReturn(HttpUtility.ParseQueryString("ReplyCode=000&TransID=" + transID), true);
				} else if (transStatus == TransactionStatus.Declined){
					ValidateChargeReturn(HttpUtility.ParseQueryString("ReplyCode=002&TransID=" + transID), true);
				}
			}
		}

		private string formatInfoVars(string sValues) 
		{
			return "<span style=\"color:#006699;\">" + sValues.Replace(":", "</span>:").Replace("\r\n", "<br/><span style=\"color:#006699;\">") + "</span>";
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			string responseData;
			if (Data == null) RaiseError(Resources.Main.ErrorSessionEnded);
			int TransID = Request["trans_id"].ToInt32(0);
            bool bIsTest = Request["IsTest"].ToInt32(0) == 1;
			StringBuilder requestBuilder = new StringBuilder();
			requestBuilder.Append(WebUtils.CurrentDomain.ProcessUrl + "remoteCharge_echeckGetBanks.asp");
			requestBuilder.Append("?CompanyNum=" + Data.merchantID.ToEncodedUrl());
			requestBuilder.Append("&Currency=" + Data.Currency.ID.ToString());
			requestBuilder.Append("&PaymentMethod=" + (int) PaymentMethodEnum.PushBankTransfer);
			requestBuilder.Append("&TransID=" + TransID);
			try {
				//Response.Write(requestBuilder.ToString());
				Data.SendChargeRequest(requestBuilder.ToString(), out responseData, Data.PaymentMethodType);
				System.Collections.Specialized.NameValueCollection responseValues = HttpUtility.ParseQueryString(responseData);
				if (responseValues["Reply"] != "000") return;
				lbFrom.Text = responseValues["BankInfo"].EmptyIfNull().Replace("\r\n", "<br>");
				lbOwnerAddress.Text = responseValues["AccountOwner"].EmptyIfNull().Replace("\r\n", "<br>");
				lbAccount.Text = formatInfoVars(responseValues["AccountInfo"].EmptyIfNull());
				lbTransfer.Text = formatInfoVars(responseValues["TransferInfo"].EmptyIfNull());
				BankURL.HRef = responseValues["BankURL"].EmptyIfNull();
				SimulateURL.HRef = responseValues["SimulateURL"].EmptyIfNull();
			} catch { 
				
			}
            SimulateURL.Visible = bIsTest;
            //BankURL.Visible = !bIsTestTransaction;
			Session.Timeout = 30;
		}
	}
}