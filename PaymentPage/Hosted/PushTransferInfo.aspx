﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="true" CodeBehind="PushTransferInfo.aspx.cs" Inherits="Netpay.PaymentPage.Hosted.PushTransferInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<table width="100%" border="0" cellpadding="0" cellspacing="4" style="border:1px solid silver; background-color:#f8f8f8;">
		<tr>
			<td class="mainHeading"><asp:Literal runat="server" id="ltTitle" Text="<%$Resources:Hosted.PushTransferInfo.aspx, ltTitle%>" /></td>
		</tr>
	</table>
	<br /><br />
	<table width="100%">
		<tr>
			<td valign="top">
				<b><asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Hosted.PushTransferInfo.aspx, ltTransferInfo%>" /></b><br />
				<asp:Label runat="server" ID="lbAccount" />
				<br /><br />
				<asp:Label runat="server" ID="lbTransfer" />
			</td>
			<td valign="top">
				<b><asp:Literal runat="server" Text="<%$Resources:Hosted.PushTransferInfo.aspx, ltBankAddress%>" /></b><br />
				<asp:Label runat="server" ID="lbFrom" />
			</td>
			<td valign="top">
				<b><asp:Literal ID="Literal3" runat="server" Text="<%$Resources:Hosted.PushTransferInfo.aspx, ltTo%>" /></b><br />
				<asp:Label runat="server" ID="lbOwnerAddress" />
			</td>
		</tr>
	</table><br /><br />
	<a runat="server" id="BankURL" target="_blank">Click Here to Redirect to your bank's web site</a>
	<br />
	<a runat="server" id="SimulateURL" target="_blank" >Click Here to simulate transfer</a>
	<br /><br />
	<asp:Button runat="server" Text="Click here to check the status of the transfer" ID="btnCheckTransfer" OnClick="CheckTransfer_OnClick" CssClass="Button" />
	<br /><br />
</asp:Content>
