﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Bll;
using System.Threading;
using System.Globalization;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.PaymentPage.Hosted
{
	public partial class Master : Netpay.PaymentPage.Code.MasterPageBase
	{
        public bool ShowOtherMethods { get; set; }
        public bool ShowMerchantBack { get; set; }
        public bool ShowTopCustomize { get; set; }
        public bool ShowBottomCustomize { get; set; }

        private void ExtractRcurring()
		{
			bool bHideLast = Data.disp_recurring.GetValueOrDefault(false);
			string[] series = Data.RecurringSeries.Split(';');
			lbRecurringText.Text = string.Empty;
			for (int i = 0; i < series.Length; i++)
			{
				bool bIsLast = (i == series.Length - 1);
				try
				{
					var cs = Netpay.Bll.Transactions.Recurring.Series.Parse(series[i]);
					lbRecurringText.Text += "<li>";
					if (i > 0) lbRecurringText.Text += (bIsLast && bHideLast) ? Resources.Main.RecurringLastAnd : Resources.Main.RecurringAnd;
					if (cs.Amount == 0) cs.Amount = Data.trans_amount; /*always shows amount*/
					if (bIsLast && bHideLast)
						lbRecurringText.Text += string.Format(Resources.Main.RecurringTextCont, "", Resources.Main.ResourceManager.GetString(cs.IntervalUnit.ToString() + (cs.IntervalCount > 1 ? "s" : "")), cs.IntervalCount > 1 ? cs.IntervalCount.ToString() : "", cs.Amount.ToAmountFormat(Data.Currency.ID));
					else
						if (cs.Amount == 0)
							lbRecurringText.Text += string.Format(cs.Charges == 1 ? Resources.Main.RecurringSingleNoAmount : Resources.Main.RecurringTextNoAmount, cs.Charges.ToString(), Resources.Main.ResourceManager.GetString(cs.IntervalUnit.ToString() + (cs.IntervalCount > 1 ? "s" : "")), cs.IntervalCount > 1 ? cs.IntervalCount.ToString() : "");
						else
							lbRecurringText.Text += string.Format(cs.Charges == 1 ? Resources.Main.RecurringSingle : Resources.Main.RecurringText, cs.Charges.ToString(), Resources.Main.ResourceManager.GetString(cs.IntervalUnit.ToString() + (cs.IntervalCount > 1 ? "s" : "")), cs.IntervalCount > 1 ? cs.IntervalCount.ToString() : "", cs.Amount.ToAmountFormat(Data.Currency.ID));
					if (lbRecurringText.Text.Length > 0) lbRecurringText.Text += "</li>";
				}
				catch (Exception e)
				{
					BasePage.RaiseError(e.Message);
				}
			}
			if (!string.IsNullOrEmpty(lbRecurringText.Text))
			{
				lbRecurringText.Text = "<ul>" + lbRecurringText.Text + "</ul>";
				lbRecurringText.Visible = true;
			}
		}

		protected void MerchantBack_OnClick(object sender, EventArgs e)
		{
			(BasePage as HostedPageBase).RedirectClient("600", "client closed window before charge", 0, DateTime.Now, "", 0, "", 0);
		}

        protected void lnkPaymentMethod_Click(object sender, CommandEventArgs e)
        {
            Data.PaymentMethodType = (int)Netpay.CommonTypes.PaymentMethodType.Unknown;
			BasePage.Redirect("~/hosted/default.aspx", "LoadPrev=1", false);
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!string.IsNullOrEmpty(Data.RecurringSeries)) ExtractRcurring();
            base.OnLoad(e);
        }

        protected override void OnInit(EventArgs e)
        {
            ShowMerchantBack = ShowOtherMethods = ShowTopCustomize = true;
            BasePage.BubbleEvent += new EventHandler(BasePage_BubbleEvent);
            base.OnInit(e);
        }

        protected void BasePage_BubbleEvent(object sender, EventArgs e)
        {
            ElementVisibleEventArgs elementVisibleEventArgs = e as ElementVisibleEventArgs;
            if (elementVisibleEventArgs == null) return;
            switch(elementVisibleEventArgs.elementType){
            case ElementVisibleEventArgs.ElementType.SelectMethod: ShowOtherMethods = elementVisibleEventArgs.Visible; break;
            case ElementVisibleEventArgs.ElementType.MerchantBackButton: ShowMerchantBack = elementVisibleEventArgs.Visible; break;
            case ElementVisibleEventArgs.ElementType.BottomCustomize: ShowBottomCustomize = elementVisibleEventArgs.Visible; break;
            case ElementVisibleEventArgs.ElementType.TopCustomize: ShowTopCustomize = elementVisibleEventArgs.Visible; break;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
			if (LabelMainHeading != null) LabelMainHeading.Text = Page.Title;
			if (Data != null) {
				if (Data.MerchantCustomization != null && (Data.PaymentSettings != null && Data.PaymentSettings.UIVersion == 1)) buidSkin();
				if (lnkPaymentMethod != null) lnkPaymentMethod.Visible = ((Data.PaymentMethods != null ? Data.PaymentMethods.Length : 0) + (Data.PaymentMethodGroups != null ? Data.PaymentMethodGroups.Count : 0)) > 1 && ShowOtherMethods;
				if (btnMerchantBack != null) btnMerchantBack.Visible = ShowMerchantBack && !string.IsNullOrEmpty(!string.IsNullOrEmpty(Data.url_redirect) ? Data.url_redirect : (Data.PaymentSettings != null && !string.IsNullOrEmpty(Data.PaymentSettings.RedirectionUrl) ? Data.PaymentSettings.RedirectionUrl : null));
			}
            if (mcTop != null) mcTop.Visible = ShowTopCustomize;
            if (mcBottom != null) mcBottom.Visible = ShowBottomCustomize;
            if (!IsPostBack)
            {
                if (tblIdentity != null) {
                    bool isWhiteLabel = false;
					if (Data != null && Data.PaymentSettings != null)
						isWhiteLabel = Data.PaymentSettings.IsWhiteLabel;
                    tblIdentity.Visible = !isWhiteLabel;
                    RaiseBubbleEvent(this, new ElementVisibleEventArgs(ElementVisibleEventArgs.ElementType.NetpayFooter, !isWhiteLabel));
                }
                if (hlIdentityLogo != null)
                {
                    hlIdentityLogo.ImageUrl = "/NPCommon/images/LogoSecure_" + BasePage.CurrentDomain.BrandName.Replace(" ", "") + ".jpg";
                    hlIdentityLogo.NavigateUrl = BasePage.CurrentDomain.ContentUrl;
                }
                if (ltPayByMsg != null)
                {
                    ltPayByMsg.Text = BasePage.CurrentDomain.BrandName + " " + GetGlobalResourceObject("Main", "PayByMsg");
                    if (Data != null && Data.Merchant != null) ltPayByMsg.Text += "<br />" + Data.Merchant.LegalName;
                }

                if (ltPayForTitle != null) ltPayForTitle.Visible = (Data != null);
                if (trPayFor != null) trPayFor.Visible = (Data != null);
                if (Data != null)
                {
					if (ltPayForText != null) ltPayForText.Text = Server.UrlDecode(Data.disp_payFor.EmptyIfNull());
                    if (ltPayForTitle != null) ltPayForTitle.Visible = !string.IsNullOrEmpty(ltPayForText.Text);
                    if (lbAuthorizeOnly != null) lbAuthorizeOnly.Visible = (Data.trans_type == TransactionType.Authorization);
                    // moved to onload - if (!string.IsNullOrEmpty(Data.RecurringSeries)) ExtractRcurring();
                    if (ltAmount != null)
                    {
						if (Data.Currency != null && Data.trans_amount != 0m)
                        {
							ltAmount.Text = Data.Currency.Symbol + Data.trans_amount.ToAmountFormat();
							if (Data.trans_installments > 1)
								ltAmount.Text += "&nbsp;|&nbsp;" + Data.trans_installments + " " + GetGlobalResourceObject("Main", "Installments");
                        }
						else if (Data.Currency != null) ltAmount.Text = Data.Currency.Symbol + "---";
                        else ltAmount.Text = "---";
                    }
                }
            }
        }

		public void InitImage(Bll.Merchants.Hosted.Customization.MerchantCustomizationLogo mcl, System.Web.UI.WebControls.Image img, string imageId)
		{
			if (imageId == null) imageId = img.ID;
            string fileName = Bll.Merchants.Hosted.Customization.GetImageFileName(Data.MerchantCustomization.SkinID.Value, imageId + ".jpg");
			if (!System.IO.File.Exists(Bll.Accounts.Account.MapPublicPath(Data.merchantID, fileName)))
			{
				img.Visible = false;
				return;
			}
			img.Visible = true;
            img.ImageUrl = Bll.Accounts.Account.MapPublicVirtualPath(Data.merchantID, fileName);
            img.Attributes.Add("align", mcl.Align);
            if (mcl.Width > 0) img.Attributes.Add("Width", mcl.Width.ToString());
            if (mcl.Height > 0) img.Attributes.Add("Height", mcl.Height.ToString());
		}

		protected void buidSkin()
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("<style type=\"text/css\">");
			sb.Append("body{");
			sb.Append(" background-color:" + Data.MerchantCustomization.BackColor + " !important;");
			sb.Append(" background-repeat:" + Data.MerchantCustomization.BackImageRepeat + " !important;");
            sb.Append(" background-image:url(" + Bll.Accounts.Account.MapPublicVirtualPath(Data.merchantID, Bll.Merchants.Hosted.Customization.GetImageFileName(Data.MerchantCustomization.SkinID.Value, "BackImage.jpg")) + ");");
			sb.AppendLine("}");
			sb.Append(Bll.Merchants.Hosted.Customization.GetCss(".PayForBox, TABLE.PayMsg TD", Data.MerchantCustomization.TextTitle));
			sb.Append(Bll.Merchants.Hosted.Customization.GetCss(".mainHeading", Data.MerchantCustomization.TextFormTitle));
			sb.Append(Bll.Merchants.Hosted.Customization.GetCss(".InputField", Data.MerchantCustomization.TextField));
			sb.Append(Bll.Merchants.Hosted.Customization.GetCss(".MerchantTopMsg, Table.FormView Td", Data.MerchantCustomization.Text));
			sb.Append(Bll.Merchants.Hosted.Customization.GetCss("Table.FormView TH", Data.MerchantCustomization.TextGroupTitle));
			sb.Append(Bll.Merchants.Hosted.Customization.GetCss(".Button", Data.MerchantCustomization.TextButton));
			sb.Append("</style>");
			if (ltCustomStyle != null) ltCustomStyle.Text = sb.ToString();
			InitImage(Data.MerchantCustomization.LogoMain, (Master as Main).LogoImage, "LogoMain");
		}
	}
}