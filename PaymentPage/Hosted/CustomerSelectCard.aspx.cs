﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Infrastructure.VO;
using Netpay.Web;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Hosted
{
	public partial class CustomerSelectCard : Netpay.PaymentPage.Code.HostedPageBase
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (Data == null) Response.Redirect("Error.aspx?message=" + HttpUtility.UrlEncode(Resources.Main.ErrorSessionEnded));
			LoadCustomerPaymentOptions();
		}

		protected void LoadCustomerPaymentOptions()
		{
			rptCardList.DataSource = Netpay.Bll.Accounts.StoredPaymentMethod.LoadForAccount(Data.CustomerLogin);
			rptCardList.DataBind();
			rptBalance.DataSource = Netpay.Bll.Customer.Customer.Load(Data.CustomerLogin).GetBalance((Currency)Data.Currency.ID, Data.Amount);
			rptBalance.DataBind();
		}

		protected void Pay(object sender, EventArgs e)
		{
			if (Request["PayWith"].ToInt32(-1) > 0)
				PayByCard();
			else
				PayByBalance();
			LoadCustomerPaymentOptions();
		}

		protected void PayByBalance()
		{
			string sIso = Request["PayWith"];
			if (string.IsNullOrEmpty(sIso)) return;
			sIso = sIso.Trim();
			if (sIso.Length != 3) return;
			CurrencyVO curPay = CurrentDomain.Cache.GetCurrency(sIso);
			if (curPay == null) return;
			decimal amountPay = (Data.Currency.ID == curPay.ID ? Data.Amount : System.Math.Round(Data.Amount / curPay.BaseRate * Data.Currency.BaseRate, 2));
			txtResponse.Text = "Under construction !!!";
			txtResponse.Text += string.Format("<br />To merchant: {0} {1}", Data.Amount, Data.Currency.IsoCode);
			txtResponse.Text += string.Format("<br />From customer: {0} {1}", amountPay, curPay.IsoCode);
			int nMerchantBalanceID = Bll.Customer.Customer.PayFromBalance(Data.CustomerLogin, (CommonTypes.Currency)curPay.ID, amountPay, Data.MerchantNum, (CommonTypes.Currency)Data.Currency.ID, Data.Amount, Data.PayFor + " - " + Data.Comment);
			txtResponse.Text += string.Format((nMerchantBalanceID > 0 ? "<br />Paid successfully, Transfer No. {0}" : "<br />Failed to pay, reply code: {0}"), nMerchantBalanceID);
		}

		protected void PayByCard()
		{
			int nSelID = Request["PayWith"].ToInt32(-1);
			if (nSelID < 0) return;
			string value1, value2;
			var pInfo = Netpay.Bll.Customer.Customer.Load(Data.CustomerLogin);
			var pCard = Netpay.Bll.Accounts.StoredPaymentMethod.Load(Data.CustomerLogin, nSelID);
			pCard.GetDecryptedData(out value1, out value2);
			System.Text.StringBuilder requestBuilder = new System.Text.StringBuilder();
			string serviceBaseUrl = WebUtils.CurrentDomain.ProcessUrl;
			requestBuilder.Append(serviceBaseUrl + "remote_charge.asp");
			requestBuilder.Append("?CompanyNum=" + base.Merchant.Number.ToEncodedUrl());
			requestBuilder.Append("&TypeCredit=" + (int)CreditType.Regular);
			requestBuilder.Append("&CardNum=" + value1);
			requestBuilder.Append("&ExpMonth=" + pCard.ExpirationDate.GetValueOrDefault().ToString("DD"));
			requestBuilder.Append("&ExpYear=" + pCard.ExpirationDate.GetValueOrDefault().ToString("yyyy"));
			requestBuilder.Append("&CVV2=" + value2.ToEncodedUrl());
			requestBuilder.Append("&Currency=" + Data.Currency.ID.ToString());
			requestBuilder.Append("&Payments=" + Data.Payments);
			requestBuilder.Append("&ClientIP=" + Request.ServerVariables["REMOTE_ADDR"].ToEncodedUrl());
			requestBuilder.Append("&Order=" + Data.RefNumber.ToEncodedUrl());
			requestBuilder.Append("&Amount=" + Data.Amount.ToString("0.00"));
			requestBuilder.Append("&PhoneNumber=" + pInfo.PhoneNumber.ToEncodedUrl());
			requestBuilder.Append("&Email=" + pInfo.EmailAddress.ToEncodedUrl());
			requestBuilder.Append("&PersonalNum=" + pInfo.PersonalNumber.ToEncodedUrl());
			requestBuilder.Append("&Member=" + pCard.OwnerName.ToEncodedUrl());
			requestBuilder.Append("&BillingAddress1=" + pCard.BillingAddress.Street1.ToEncodedUrl());
			requestBuilder.Append("&BillingAddress2=" + pCard.BillingAddress.Street2.ToEncodedUrl());
			requestBuilder.Append("&BillingCity=" + pCard.BillingAddress.City.ToEncodedUrl());
			requestBuilder.Append("&BillingZipCode=" + pCard.BillingAddress.PostalCode.ToEncodedUrl());
			requestBuilder.Append("&BillingState=" + pCard.BillingAddress.StateISOCode.ToString().ToEncodedUrl());
			requestBuilder.Append("&BillingCountry=" + pCard.BillingAddress.CountryISOCode.ToString().ToEncodedUrl());
			requestBuilder.Append("&RetURL=" + (serviceBaseUrl + "PaymentPageConfirm.aspx").ToEncodedUrl());
			//Response.Write(requestBuilder.ToString());
			string responseData = null;
			try { Code.HostedPageBase.SendChargeRequest(Data, base.Merchant, requestBuilder.ToString(), out responseData, Data.PaymentMethodType); }
			catch (Exception ex) { txtResponse.Text = ex.Message; return; }
			txtResponse.Text = Code.HostedPageBase.ValidateChargeReturn(Data, base.Merchant, responseData, true);
		}
	}
}