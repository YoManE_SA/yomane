﻿<%@ Page Title="" Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="true" Inherits="Netpay.PaymentPage.Hosted.CustomerSelectCard" Codebehind="CustomerSelectCard.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<asp:Label ID="txtResponse" runat="server" ForeColor="Red" />
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td width="48%" style="vertical-align:top;">
				<div class="mainHeading"><asp:Literal ID="litSelectCard" runat="server" Text="<%$Resources:Hosted.CustomerSelectCard.aspx,SelectCard%>" /></div>
				<br />
				<asp:Repeater runat="server" ID="rptCardList">
					<ItemTemplate>
						<div>
							<input type="radio" class="option" name="PayWith" value="<%# (Container.DataItem as CreditCardStorageVO).ID%>" <%# ((Container.DataItem as CreditCardStorageVO).ID==Request["PayWith"].ToInt32(-1) ? "checked=\"checked\"" : string.Empty) %> />
							<b><netpay:PaymentMethodView runat="server" PaymentMethodID="<%# (Container.DataItem as CreditCardStorageVO).PaymentMethod%>" BinCountryCode="<%# (Container.DataItem as CreditCardStorageVO).BinCountry%>" CardLast4Digits="<%# (Container.DataItem as CreditCardStorageVO).CcLast4%>" /></b>
							<br /> &nbsp; &nbsp; &nbsp; &nbsp;
							<asp:Literal runat="server" Text="<%$Resources:Hosted.CustomerSelectCard.aspx,ValidUntil%>" />: <%# (Container.DataItem as CreditCardStorageVO).ExpirationMonth.Trim()%>/<%# (Container.DataItem as CreditCardStorageVO).ExpirationYear%>
							<br /> &nbsp; &nbsp; &nbsp; &nbsp;
							<asp:Literal runat="server" Text="<%$Resources:Hosted.CustomerSelectCard.aspx,CardHolder%>" />: <%# (Container.DataItem as CreditCardStorageVO).CardholderFullName%>
						</div>
					</ItemTemplate>
				</asp:Repeater>
			</td>
			<asp:Panel Visible="false" runat="server">
				<td width="2%">&nbsp;</td>
				<td width="2%" class="columnSeparator">&nbsp;</td>
				<td width="48%" style="vertical-align:top;">
					<div class="mainHeading"><asp:Literal ID="litSelectBalance" runat="server" Text="<%$Resources:Hosted.CustomerSelectCard.aspx,SelectBalance%>" /></div>
					<br />
					<asp:Repeater ID="rptBalance" runat="server">
						<ItemTemplate>
							<div>
								<input type="radio" class="option" name="PayWith" value="<%# (Container.DataItem as CustomerBalanceVO).Iso %>" <%# ((Container.DataItem as CustomerBalanceVO).Iso==Request["PayWith"] ? "checked=\"checked\"" : string.Empty) %> />
								<b><%# Netpay.Web.WebUtils.DomainCache.GetCurrency((Container.DataItem as CustomerBalanceVO).Iso).Name%></b>
								<br /> &nbsp; &nbsp; &nbsp; &nbsp;
								You have to pay: <%# (Data.Amount * Data.Currency.BaseRate / Netpay.Web.WebUtils.DomainCache.GetCurrency((Container.DataItem as CustomerBalanceVO).Iso).BaseRate).ToAmountFormat() + " " + (Container.DataItem as CustomerBalanceVO).Iso%>
								<br /> &nbsp; &nbsp; &nbsp; &nbsp;
								<asp:Literal runat="server" Text="<%$Resources:Hosted.CustomerSelectCard.aspx,CurrentBalance%>" />:
								<%# (Container.DataItem as CustomerBalanceVO).Balance.ToAmountFormat()%> <%# (Container.DataItem as CustomerBalanceVO).Iso%>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</td>
			</asp:Panel>
		</tr>
		<tr>
			<td colspan="4" align="center">
				<br />
				<asp:Button UseSubmitBehavior="false" runat="server" Text="<%$Resources:Hosted.CustomerSelectCard.aspx,ButtonPay%>" OnClick="Pay" CssClass="Button" />
			</td>
		</tr>
	</table>
	<br />
</asp:Content>