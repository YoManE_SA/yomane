﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="true" CodeBehind="PaymentConfirm.aspx.cs" Inherits="Netpay.PaymentPage.Hosted.PaymentConfirm" %>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<asp:CustomValidator ID="vAgreement" OnServerValidate="CustomValidatorAgreement" ErrorMessage="<%$Resources:Hosted.PaymentConfirm.aspx, cvAgreementRequired %>" ValidationGroup="creditCardConfirmation" Display="None" runat="server"></asp:CustomValidator>
	<asp:CustomValidator ID="vNoQuantity" OnServerValidate="CustomValidatorQuantity" ErrorMessage="<%$Resources:Hosted.PaymentConfirm.aspx, cvNoQuantity %>" ValidationGroup="creditCardConfirmation" Display="None" runat="server"></asp:CustomValidator>
	<table class="FormView" width="100%">
		<tr>
			<td colspan="2">
				<table width="100%">
					<tr>
						<th><asp:Label runat="server" id="ltChargeInfoTitle" /></th>
						<th><asp:Label runat="server" id="ltAddressInfoTitle" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, ltAddressInfoTitle.Text %>" /></th>
						<th><asp:Label runat="server" id="ltPersonalInfoTitle" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, ltPersonalInfoTitle.Text %>" /></th>
					</tr>
					<tr>
						<td valign="top"><asp:Literal ID="ltChargeInfo" runat="server" /></td>
						<td valign="top"><asp:Literal ID="ltAddressInfo" runat="server" /></td>
						<td valign="top"><asp:Literal ID="ltPersonalInfo" runat="server" /></td>
					</tr>
				</table>
				<br /><br />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<asp:ValidationSummary ID="ValidationSummary2" EnableClientScript="false" runat="server" DisplayMode="SingleParagraph" ShowMessageBox="false" ShowSummary="true" ValidationGroup="creditCardConfirmation" CssClass="ValidSummery" HeaderText="<%$Resources:Main, FieldError%>" />
				<table>
				<tr>
					<td style="vertical-align:top; width:26px;"><asp:CheckBox ID="cbAgreement" runat="server" class="distinct" /></td>
					<td><asp:Literal ID="ltAgreement" runat="server" />&nbsp;<asp:Literal ID="ltClientAgreement" runat="server" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, txtAgreement %>" /> </td>
				</tr>
				<asp:PlaceHolder runat="server" ID="phClientAgreement" >
					<tr>
						<td colspan="2" style="line-height:50px;">
							<a id="aLegal" onclick="$('#pLegalDoc').slideDown('slow');" href="javascript://" runat="server"><asp:Literal runat="server" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, rulesAndRegulations%>" /></a>
							<div id="pLegalDoc" style="display:none; line-height:normal; background-color:#cccccc; border:1px solid #808080; padding:3px; height:200px; overflow-y:scroll; overflow-x:auto;"><asp:Literal ID="litLegalDoc" runat="server" /></div>
						</td>
					</tr>
				</asp:PlaceHolder>
                <tr>
                    <td colspan="4"><asp:CheckBox ID="chkRegisterWallet" ClientIDMode="Static" Checked="true" runat="server" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, WalletRegestration %>" /></td>
                </tr>
				</table>
             
			</td>
		</tr>
		<tr>
			<td colspan="2" class="reverse">
				<br /><br />
				<asp:HyperLink runat="server" ID="hGoBack" Text="<%$ Resources:Hosted.PaymentConfirm.aspx, hlBackChange.Text %>" /><br />
				<br /><asp:Button runat="server" ID="btnCharge" Text="Charge" OnCommand="btnCharge_Command" CssClass="Button" />
			</td>
		</tr>
	</table>
	<br />

</asp:Content>
