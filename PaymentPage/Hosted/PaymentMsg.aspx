﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="true" CodeBehind="PaymentMsg.aspx.cs" Inherits="Netpay.PaymentPage.Hosted.PaymentMsg" %>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<br />
	<table class="FormView">
		<tr>
			<td>
				 <asp:MultiView runat="server" ID="mvReply">
					<asp:View runat="server">
						<asp:Label runat="server" ID="lbSucceededText" Font-Size="14px" ForeColor="#009900" />
						&nbsp;<br /><br />
						<asp:Literal runat="server" Text="<%$Resources:Hosted.PaymentMsg.aspx, ltTransID %>" />: <asp:Label runat="server" ID="TransID" />
						<br />
						<asp:Literal runat="server" Text="<%$Resources:Hosted.PaymentMsg.aspx, ltTransDate %>" />: <asp:Label runat="server" ID="TransDate" />
						<br /><br /><br />
						<asp:PlaceHolder runat="server" ID="phFinish">
							<asp:LinkButton runat="server" ID="btnFinish" ClientIDMode="Static" class="btn-finish" Text="<%$Resources:Hosted.PaymentMsg.aspx, btnFinish%>" OnClick="btnFinish_Click" OnClientClick="doRedirect();void(0)" />
                			<script type="text/javascript">
                				var checkClose = true;
                				var timeout = window.setInterval(updateButtonText, 1000);
                				var maxTimer = <%= Data.url_redirect_seconds %>;
                				jQuery(window).bind('beforeunload', function () {
                					if (!checkClose) return;
                					alert('<asp:Literal runat="server" Text="<%$Resources:Hosted.PaymentMsg.aspx, AlertClose%>" />');
                					return false;
                				});

								function doRedirect() {
									if (timeout) {
										window.clearTimeout(timeout);
										timeout = null;
									}
									checkClose = false;
									$('#btnFinish').attr('disabled', 'disabled');
									document.location = $('#btnFinish').attr('href');
								}

								function updateButtonText() {
									maxTimer--;
									$('#btnFinish').text('<asp:Literal runat="server" Text="<%$Resources:Hosted.PaymentMsg.aspx, btnFinish%>" /> ' + maxTimer);
									if (maxTimer == 0) doRedirect();
								}
                			</script> 
						</asp:PlaceHolder>
					</asp:View>
					<asp:View runat="server">
						<asp:Label runat="server" ID="lbFailedText" Font-Size="14px" ForeColor="#CC0000" />
						&nbsp;<br /><br />
						<asp:LinkButton runat="server" ID="btnTryAgain" Text="<%$Resources:Hosted.PaymentMsg.aspx, btnTryAgain%>" OnClick="btnTryAgain_Click" />
					</asp:View>
				 </asp:MultiView>
				 <br /><br />
				 <asp:Literal runat="server" ID="ltMerchantText" />
			</td>
		</tr>
	</table>
	<br />

</asp:Content>
