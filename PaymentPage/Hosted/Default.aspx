﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="true" Inherits="Netpay.PaymentPage.Hosted.Default" Codebehind="Default.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <asp:ValidationSummary ID="ValidationSummary1" EnableClientScript="false" runat="server" DisplayMode="SingleParagraph" ShowMessageBox="false" ShowSummary="true" CssClass="ValidSummery" ValidationGroup="PaymentForm" HeaderText="<%$Resources:Main, FieldError%>" />
	<asp:PlaceHolder runat="server" ID="phControl" />
    <table class="FormView" width="100%" border="0" id="tblContinue" runat="server" enableviewstate="false">
	    <tr><td colspan="3" style="height:35px;"><hr style="border-top: 1px dashed #c2c2c2; border-bottom: 0px; border-right: 0px; border-left: left;" /></td></tr>
	    <tr>
		    <td colspan="2" width="67%" class="Require"><asp:Literal runat="server" ID="ltRequired"  Text="<%$ Resources:Main,RequireMsg %>" /></td>
		    <td class="align-right"><asp:Button ID="btnPay" OnClick="Pay_Click" Width="110px" runat="server" Text="<%$ Resources: Hosted.Default.aspx, PayButton %>"  CssClass="Button" /></td>
	    </tr>
    </table>
</asp:Content>
