﻿using System;
using System.Web;
using Netpay.PaymentPage.Code;

namespace Netpay.PaymentPage.Hosted
{
    public partial class HostFrames : System.Web.UI.Page
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Netpay.PaymentPage.Code.HostedPageBase.Load(Request.QueryString[Netpay.PaymentPage.Code.HostedPageBase.TokenValueName]);
        }

        protected string TopContentUrl {
            get {
                var target = Netpay.PaymentPage.Code.HostedPageBase.MapDesignPage(Netpay.PaymentPage.Code.HostedContext.Current, "~/Hosted/TopFrame.aspx");
                target = ResolveClientUrl(target);
                target = Netpay.PaymentPage.Code.HostedPageBase.AppendToken(target);
                return target;
            }
        }
    }
}