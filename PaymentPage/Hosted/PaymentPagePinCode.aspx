﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="true" CodeBehind="PaymentPagePinCode.aspx.cs" Inherits="Netpay.PaymentPage.Hosted.PaymentPagePinCode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <asp:Label runat="server" ID="txtResponse" ForeColor="red" /><br /><br />
    <asp:Literal runat="server" Text="<%$Resources:Hosted.PaymentPagePinCode.aspx, ltPinCode %>" /> : <br />
	<asp:TextBox runat="server" ID="txtPinCode" MaxLength="8" /><br />
	<asp:Literal runat="server" Text="<%$Resources:Hosted.PaymentPagePinCode.aspx, ltPhone %>" /> : <br />
	<asp:TextBox runat="server" ID="txtPhone" MaxLength="20" />
	<br /><br />
    <asp:Button runat="server" ID="btnContinue" Text=" CONTINUE >> " OnClick="btnContinue_Click" CssClass="Button" />
</asp:Content>
