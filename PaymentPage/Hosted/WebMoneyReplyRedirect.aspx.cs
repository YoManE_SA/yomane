﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.PaymentPage.Code;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Hosted
{
	public partial class WebMoneyReplyRedirect : HostedPageBase
	{
		protected override void OnInit(EventArgs e)
		{
			if (Request["action"] == "notify") Response.End();
			LoadData(Request["logId"].ToNullableInt().GetValueOrDefault());
			//base.OnInit(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request["action"] == null) return;
			// check secret key
			string secretKey = Request["LMI_SECRET_KEY"];
			if (PaymentSettings.WebMoneySecretKey != Request["LMI_SECRET_KEY"]) return;

			if (Request["IsFrame"] != null)
			{
				var newCol = System.Web.HttpUtility.ParseQueryString(Request.Url.Query);
				newCol.Remove("IsFrame");
				Response.Write("<script type=\"text/javascript\">window.parent.location.href='" + Request.Path + "?" + newCol.ToString() + "'</script>");
				Response.End();
			}

			// get trans data
			/*
			string merchantsPurse = Request["LMI_PAYEE_PURSE"];
			if (string.IsNullOrEmpty(merchantsPurse)) merchantsPurse = " ";
			Currency currency = Currency.Unknown;
			string currencyCode = merchantsPurse.Substring(0, 1);
			if (currencyCode == "Z") currency = Currency.USD;
			else if (currencyCode == "E") currency = Currency.EUR;
			*/
			string replyCode = null;
			if (Request["action"] == "successRedirect") replyCode = "000";
			else if (Request["action"] == "failRedirect") replyCode = "006";

			// save transaction

			if (replyCode != null){
				string strOut = null;
				System.Text.StringBuilder requestBuilder = new System.Text.StringBuilder();
				requestBuilder.Append(CurrentDomain.ProcessUrl + "RemoteCharge_Alt.asp");
				requestBuilder.Append("?CompanyNum=" + Merchant.Number.ToEncodedUrl());
				requestBuilder.Append("&CreditType=" + (int)CreditType.Regular);
				requestBuilder.Append("&Amount=" + Data.trans_amount.ToString("0.00"));
				requestBuilder.Append("&Currency=" + (int)Data.Currency.ID);
				requestBuilder.Append("&ClientIP=" + Request.UserHostAddress.ToEncodedUrl());
				requestBuilder.Append("&Order=" + Request["LMI_PAYMENT_NO"]);
				requestBuilder.Append("&DebitRefCode=" + Request["LMI_SYS_TRANS_NO"]);
				requestBuilder.Append("&PaymentMethod=" + (int)PaymentMethodEnum.WebMoney);
				requestBuilder.Append("&terminalNumber=4700000001");
				requestBuilder.Append("&ReplyCode=" + replyCode);
				requestBuilder.Append("&requestSource=" + (int)TransactionSource.HostedPageV2En);
				requestBuilder.Append("&Comment=" + Data.trans_comment.ToEncodedUrl());
				try { Data.SendChargeRequest(requestBuilder.ToString(), out strOut, PaymentMethodType.ExternalWallet); } catch { }
				ValidateChargeReturn(HttpUtility.ParseQueryString(strOut), true);
				/*
				Netpay.Infrastructure.VO.TransactionVO transation = new TransactionVO();
				transation.InsertDate = DateTime.Now; //Request["LMI_SYS_TRANS_DATE"];
				transation.MerchantID = Merchant.ID;
				transation.Amount = Request["LMI_PAYMENT_AMOUNT"].ToDecimal(0);
				transation.CurrencyID = (int) currency;
				transation.Installments = 1;
				transation.IP = Request.UserHostAddress;
				transation.DebitCompanyID = 0;
				transation.OrderNumber = Request["LMI_PAYMENT_NO"];
				//transation.DebitReferenceCode = Request["LMI_SYS_TRANS_NO"];
				transation.PaymentMethodID = (short)PaymentMethod.WebMoney;
				transation.ReplyCode = replyCode;
				transation.TransType = (int) TransactionType.Capture;
				transation.Status = replyCode == "000" ? TransactionStatus.Captured : TransactionStatus.Declined;
				transation.IsTest = false;
				//Netpay.Bll.Transactions.Create(Web.WebUtils.CurrentDomain, transation);
				*/
			}
			//http://80.179.39.10:8080/Hosted/WebMoneyReplyRedirect.aspx?action=successRedirect&replyUrl=http%3a%2f%2f80.179.39.10%3a8080%2fhosted%2fcheckReply.aspx&merchantNum=5722306
		}
	}
}