﻿using System;
using System.Web;
using Netpay.PaymentPage.Code;

namespace Netpay.PaymentPage.Hosted
{
    public partial class HostIFrame : HostedPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            contentFrame.Attributes.Add("src", Request["content"]);
        }
    }
}