﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using System.Text;
using Netpay.Bll;
using Netpay.Infrastructure;
using System.Net;
using System.IO;
using System.Collections.Specialized;

namespace Netpay.PaymentPage.Hosted
{
	public partial class PaymentMsg : HostedPageBase
	{
		private string replyCode, replyDescription, trans_ID, trans_Date, paymentDisplay, storageID, walletID, recurringID;

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
            if (Request["frameBack"] == "1" && Request["TransID"] != null)
                ValidateChargeReturn(HttpContext.Current.Request.QueryString, true);

            //if (DataOrNull == null && Request["LogID"] != null) LoadData(Request["LogID"].ToNullableInt().GetValueOrDefault(), true);
            if (Data == null) RaiseError(Resources.Main.ErrorSessionEnded);
            //Page.ClientScript.RegisterStartupScript(GetType(), "redirectTop", "try{ if(window.parent && window.parent.location && (window.parent.location.href.indexOf('HostFrames.aspx') > -1 || window.parent.location.href.indexOf('HostIFrame.aspx') > -1)) { checkClose = false; window.parent.location.href = window.location.href; } } catch(e) {}", true);

			NameValueCollection responseValues = Request.QueryString;
			replyCode = responseValues["reply"] != null ? responseValues["reply"] : responseValues["replyCode"];
			trans_ID = responseValues["TransID"] != null ? responseValues["TransID"] : responseValues["trans_id"];
			trans_Date = responseValues["Date"] != null ? responseValues["Date"] : responseValues["trans_date"];
			replyDescription = responseValues["replyDesc"];
			paymentDisplay = responseValues["paymentDisplay"];
			storageID = responseValues["storage_id"];
			walletID = responseValues["client_Wallet_id"];
			recurringID = responseValues["recurringSeries"];

			TransDate.Text = trans_Date;
			TransID.Text = trans_ID;
			mvReply.ActiveViewIndex = 0;
			var LanguageDic = Data.MerchantTextData(Web.WebUtils.CurrentLanguage, TranslationGroup.HostedPaymentPage);
            OnBubbleEvent(this, new ElementVisibleEventArgs(ElementVisibleEventArgs.ElementType.MerchantBackButton, false));
			Title = (string)GetGlobalResourceObject("Hosted.PaymentMsg.aspx", "PaymentOutcome");
			phFinish.Visible = !string.IsNullOrEmpty(!string.IsNullOrEmpty(Data.url_redirect) ? Data.url_redirect : (PaymentSettings != null && !string.IsNullOrEmpty(PaymentSettings.RedirectionUrl) ? PaymentSettings.RedirectionUrl : null));
            if (replyCode == "000") {
                lbSucceededText.Text = (string)GetGlobalResourceObject("Hosted.PaymentMsg.aspx", "PaymentSuccess");
                OnBubbleEvent(this, new ElementVisibleEventArgs(ElementVisibleEventArgs.ElementType.SelectMethod, false));
				if (LanguageDic != null && LanguageDic.ContainsKey("TransSuccess")) ltMerchantText.Text = LanguageDic["TransSuccess"];
			}
			else if (replyCode == "001")
			{
                lbSucceededText.Text = (string)GetGlobalResourceObject("Hosted.PaymentMsg.aspx", "PaymentPending");
                OnBubbleEvent(this, new ElementVisibleEventArgs(ElementVisibleEventArgs.ElementType.SelectMethod, false));
				if (LanguageDic != null && LanguageDic.ContainsKey("TransPending")) ltMerchantText.Text = LanguageDic["TransPending"];
			}
			else
			{
                OnBubbleEvent(this, new ElementVisibleEventArgs(ElementVisibleEventArgs.ElementType.MerchantBackButton, true));
                lbFailedText.Text = string.Format("{0}- ({1}): {2}", Resources.Main.Error, replyCode, replyDescription);
                mvReply.ActiveViewIndex = 1;
				if (LanguageDic != null && LanguageDic.ContainsKey("TransDecline")) ltMerchantText.Text = LanguageDic["TransDecline"];
            }
		}

		protected void btnFinish_Click(object sender, EventArgs e)
		{
			RedirectClient(replyCode, replyDescription, trans_ID.ToInt32(0), ConvertRCDateString(trans_Date), paymentDisplay, storageID.ToInt32(0), walletID, recurringID.ToInt32(0));
		}
		
		protected void btnTryAgain_Click(object sender, EventArgs e)
		{
			Redirect("~/hosted/default.aspx", "LoadPrev=1");
		}

		protected override void OnPreRender(EventArgs e)
		{
			if(!ClientScript.IsClientScriptBlockRegistered(GetType(), "checkFrame"))
				ClientScript.RegisterClientScriptBlock(GetType(), "checkFrame", "if(top.location != window.location) { checkClose = false; top.location.href = window.location.href + '&frameBack=1'; }", true);
			base.OnPreRender(e);
		}
	}
}
