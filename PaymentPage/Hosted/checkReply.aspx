﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<script runat="server" type="text/C#">
	protected override void OnLoad(EventArgs e)
	{
		System.Text.StringBuilder sb = new StringBuilder();
		sb.Append("<u>GET DATA:</u></br>");
		foreach(string n in Request.QueryString.Keys)
			sb.Append("<strong>" + n + "</strong>: " + Request.QueryString[n] + "<br>");
		sb.Append("<hr><u>POST DATA:</u></br>");
		foreach (string n in Request.Form.Keys)
			sb.Append("<strong>" + n + "</strong>: " + Request.Form[n] + "<br>");
		ltData.Text = sb.ToString();
		base.OnLoad(e);
	}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
	<asp:Literal runat="server" ID="ltData" />
</body>
</html>
