﻿using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Hosted
{
	public partial class PaymentConfirm : HostedPageBase
	{
		private bool isQuantityValid = true;

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (Data == null) RaiseError(Resources.Main.ErrorSessionEnded);
			Page.Title = (string)GetGlobalResourceObject("Hosted.PaymentConfirm.aspx", "ltTitle.Text");
			btnCharge.Text = GetGlobalResourceObject("Hosted.PaymentConfirm.aspx", "PayButton") + " " + Data.trans_amount.ToIsoAmountFormat(Data.Currency.ID) + "  >>";
            btnCharge.OnClientClick = string.Format("netpay.Blocker.show('{0}')", GetGlobalResourceObject("Hosted.PaymentConfirm.aspx", "PleaseWait"));
			string titleRes = "";
			switch (Data.PaymentMethodType) {
				case PaymentMethodType.CreditCard: titleRes = "InfoCreditCard"; break;
				case PaymentMethodType.ECheck: titleRes = "InfoCheck"; break;
				case PaymentMethodType.BankTransfer: titleRes = "InfoAccount"; break;
				case PaymentMethodType.PhoneDebit: titleRes = "InfoCellular"; break;
			}
			ltChargeInfoTitle.Text = (string)GetGlobalResourceObject("Hosted.PaymentConfirm.aspx", titleRes);
            var pmName = Bll.PaymentMethods.PaymentMethod.Get(Data.PaymentMethod)?.Name;
            ltChargeInfo.Text = FormatValues(new string[] {
                pmName,
                Data.MaskValue(Data.pm_value1),
                Data.MaskValue(Data.pm_value2),
                (Data.pm_expDate != null ? Data.pm_expDate.Value.ToString("MM-yyyy") : ""),
                (Data.pm_IssuerName + " " + Data.pm_IssuerCountryIso)
            });

            ltPersonalInfo.Text = FormatValues(new string[] {
                Data.client_fullName.EmptyIfNull(),
                Data.client_email.EmptyIfNull(),
                Data.client_phoneNum.EmptyIfNull(),
                Data.client_dateOfBirth != null ? Data.client_dateOfBirth.Value.ToString("d") : ""
            });

            ltAddressInfo.Text = FormatValues(new string[] {
                Data.client_billaddress1,
                ((string.IsNullOrEmpty(Data.client_billaddress2) ? "" : Data.client_billaddress2 + ", ") + Data.client_billzipcode),
                ((string.IsNullOrEmpty(Data.client_billstate) ? "" : WebUtils.GetStateName(Data.client_billstate) + ", ") + (string.IsNullOrEmpty(Data.client_billcountry) ? "" : Web.WebUtils.GetCountryName(Data.client_billcountry)))
            });

            if (!Page.IsPostBack) {
				if(chkRegisterWallet != null) {
					chkRegisterWallet.Checked = Data.client_autoRegistration;
					chkRegisterWallet.Visible = Data.PaymentSettings.IsCustomer;
					chkRegisterWallet.Text = GetGlobalResourceObject("Hosted.PaymentConfirm.aspx", "WalletRegestration").ToString().Replace("%LEGAL_NAME%", WebUtils.CurrentDomain.LegalName);
				}
			}
			if (ltAgreement != null) ltAgreement.Text = GetGlobalResourceObject("Hosted.PaymentConfirm.aspx", "ltAgreement.Text").ToString().Replace("%LEGAL_NAME%", WebUtils.CurrentDomain.LegalName);
			string legalDocument = Bll.Merchants.Merchant.GetContent(Merchant.AccountID, Language.English, "RulesAndRegulations.html").EmptyIfNull().Trim();
			if (phClientAgreement != null) phClientAgreement.Visible = ltClientAgreement.Visible = (legalDocument.Length > 0);
			if (litLegalDoc != null) litLegalDoc.Text = legalDocument;
			if (hGoBack != null) hGoBack.NavigateUrl = MapDesignPage(Data, "~/hosted/default.aspx") + "?" + Token;
		}

        private string FormatValues(string[] values) {
            var sb = new StringBuilder();
            foreach (var v in values)
                if (!string.IsNullOrEmpty(v)) sb.Append(v.Trim() + "<br />");
            return sb.ToString();
        }

		protected void CustomValidatorAgreement(object source, ServerValidateEventArgs args)
		{
			args.IsValid = cbAgreement.Checked;
		}

		protected void CustomValidatorQuantity(object source, ServerValidateEventArgs args)
		{
			args.IsValid = isQuantityValid;
		}

		protected void btnBack_Command(object source, CommandEventArgs args)
		{
			Redirect("~/Hosted/default.aspx", "LoadPrev=1");
		}

		protected void btnCharge_Command(object sender, CommandEventArgs e)
		{
			Page.Validate();
			if (!Page.IsValid) return;
			string retUrl = Request.Url.GetLeftPart(UriPartial.Authority) + AppendToken(ResolveUrl(MapDesignPage(Data, "~/Hosted/PaymentMsg.aspx")));

			bool manageStock = false;
            var responseValues = new System.Collections.Specialized.NameValueCollection();
            try {
				Page.Validate();
				if (!Page.IsValid) return;
				if (chkRegisterWallet != null) Data.client_autoRegistration = chkRegisterWallet.Checked;
                responseValues = Data.Process(retUrl);
			} catch (Exception ex) {
                Logger.Log(ex);
				RaiseError(ex.Message); 
			}
			ValidateChargeReturn(responseValues, true);
		}
	}
}