﻿<%@ Page Language="C#" MasterPageFile="Hosted.Master" AutoEventWireup="true" CodeBehind="PaymentIVRCode.aspx.cs" Inherits="Netpay.PaymentPage.Hosted.PaymentIVRCode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<br />
	<div style="border:solid 1px #C0C0C0;padding:10px;">
	  <h5><asp:Literal runat="server" id="ltInstructionHeader" Text="<%$Resources:Hosted.PaymentIVRCode.aspx, ltInstructionHeader%>" /></h5><br />
	  <asp:Label runat="server" id="ltSMSMessageInstruction" />
	</div>
	<br />
	<div class="reverse">
		<asp:Button runat="server" ID="btnCharge" Text="Check Status" OnCommand="btnCharge_Command" CssClass="Button" />
	</div>
</asp:Content>