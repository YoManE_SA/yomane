﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using System.Collections.Specialized;
using Netpay.PaymentPage.Code;
using Netpay.Web;

namespace Netpay.PaymentPage.Hosted.PayPal
{
	public partial class Callback : HostedPageBase
    {
        protected override void OnInit(EventArgs e)
        {
            //base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
		{
			/*
			 * params
			mc_gross: 1.50 
			protection_eligibility: Eligible 
			address_status: confirmed 
			payer_id: JX9P3SLQHLEWL 
			tax: 0.00 
			address_street: 1 Main St 
			payment_date: 04:11:56 Feb 02, 2012 PST 
			payment_status: Completed 
			charset: windows-1252 
			address_zip: 95131 
			first_name: Sean 
			mc_fee: 0.34 
			address_country_code: US 
			address_name: Sean Moore 
			notify_version: 3.4 
			custom:  
			payer_status: verified 
			business: gctest_1328170936_biz@gmail.com 
			address_country: United States 
			address_city: San Jose 
			quantity: 1 
			verify_sign: AbSrgand.O5S2cfDHABpWxF4k6SHAejvO0zVP6PytIZ71oZ.2aDhEd8a 
			payer_email: gctest_1328170865_per@gmail.com 
			txn_id: 3WC90935HM735982R 
			payment_type: instant 
			last_name: Moore 
			address_state: CA 
			receiver_email: gctest_1328170936_biz@gmail.com 
			payment_fee: 0.34 
			receiver_id: JN252BLB5MGJ4 
			txn_type: web_accept 
			item_name: Purchase 
			mc_currency: USD 
			item_number:  
			residence_country: US 
			test_ipn: 1 
			handling_amount: 0.00 
			transaction_subject: Purchase 
			payment_gross: 1.50 
			shipping: 0.00 
			ipn_track_id: aa3769c09b6c7
			*/

			/*
			 * payment_status
			Canceled_Reversal: A reversal has been canceled. For example, you won a dispute with the customer, and the funds for the transaction that was reversed have been returned to you.
			Completed: The payment has been completed, and the funds have been added successfully to your account balance.
			Created: A German ELV payment is made using Express Checkout.
			Denied: You denied the payment. This happens only if the payment was previously pending because of possible reasons described for the pending_reason variable or the Fraud_Management_Filters_x variable.
			Expired: This authorization has expired and cannot be captured.
			Failed: The payment has failed. This happens only if the payment was made from your customer’s bank account.
			Pending: The payment is pending. See pending_reason for more information.
			Refunded: You refunded the payment.
			Reversed: A payment was reversed due to a chargeback or other type of reversal. The funds have been removed from your account balance and returned to the buyer. The reason for the reversal is specified in the ReasonCode element.
			Processed: A payment has been accepted.
			Voided: This authorization has been voided.
			 */

			try
			{
				string transID = Request["txn_id"];
				int paymentLogID = int.Parse(Request["custom"]);
				string replyCode = null;
				string orderState = Request["payment_status"];
				switch (orderState)
				{
					case "Completed":
						replyCode = "000";
						break;
					case "Pending":
					case "Processed":
						replyCode = "001";
						break;
					case "Failed":
						replyCode = "548";
						break;
					default:
						replyCode = "520";
						break;
				}

                if (LoadData(paymentLogID))
                    ValidateExternalReturn(transID, "PayPal", replyCode, orderState, true, RedirectWindow.SameWindow);
			}
			catch (Exception ex)
			{
                Response.Write("Error occured");
				Logger.Log(LogTag.PaymentPage, ex);
			}
		}
	}
}