﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Infrastructure;
using System.IO;

namespace Netpay.PaymentPage.Hosted.PayPal
{
    public partial class Frame : Netpay.PaymentPage.Code.HostedPageBase
	{
        protected override void OnPreInit(EventArgs e) { }
        protected void Page_Load(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(PaymentSettings.PayPalMerchantID))
			{
                RaiseError("PayPal MerchantID not configured");
				return;
			}
			var merchant = ((HostedPageBase)Page).Merchant;
            string payPalUrl = Infrastructure.Application.IsProduction ? "https://www.paypal.com/cgi-bin/webscr?" : "https://www.sandbox.paypal.com/webscr?";
            string redirectUrl = Infrastructure.Application.IsProduction ? Request.Url.ReplaceFileName("PayPal/Callback.aspx") : "http://212.150.167.145:8080/";
            string redirectParams = string.Format("business={0}&cmd=_xclick&item_name={1}&amount={2}&currency_code={3}&custom={4}&rm=2&return={5}", PaymentSettings.PayPalMerchantID, Data.disp_payFor, Data.trans_amount, Data.trans_currency, Data.LogPaymentPage.ID, redirectUrl);
            Response.Redirect(payPalUrl + redirectParams);
		}
	}
}