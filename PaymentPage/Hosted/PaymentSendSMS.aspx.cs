﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using Netpay.Bll;
using Netpay.Infrastructure;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using System.Net;
using System.IO;

namespace Netpay.PaymentPage.Hosted
{
	public partial class PaymentSendSMS : HostedPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            if (Data == null) RaiseError(Resources.Main.ErrorSessionEnded);
			Title = (string)GetGlobalResourceObject("Hosted.PaymentSendSMS.aspx", "ltTitle");
            if (!IsPostBack) ltSMSMessageInstruction.Text = Request["SMSInstruction"];
        }

		protected void btnCharge_Command(object sender, EventArgs e)
		{
			WebRequest webRequest;
			HttpWebResponse webResponse;
			string outParams, parameters = CurrentDomain.ProcessUrl + "verify_trans.asp?CompanyNum=" + Data.merchantID + "&TransID=" + Request["trans_id"] + "&TransType=2&TransCurrency=" + Data.Currency.ID + "&TransAmount=" + Data.trans_amount.ToAmountFormat() + "&TransDate=" + DateTime.Now;
			try {
				webRequest = WebRequest.Create(parameters);
				webResponse = (HttpWebResponse)webRequest.GetResponse();
				outParams = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
				if (outParams.ToInt32(-1) == (int) VerifyTransReturnValues.PendingTransApproved)
                    Redirect("PaymentMsg.aspx", "trans_id=" + Request["trans_id"] + "&replyCode=000&replyDesc=&trans_date=" + HttpUtility.UrlEncode(DateTime.Now.ToString()));
			} catch (Exception ex) {
				Logger.Log(LogSeverity.Error, LogTag.PaymentPage, ex.Message, ex.ToString() + "\n\n" + parameters);
				throw new Exception(Resources.Main.ErrorCommunication);
			}

		}
	}
}