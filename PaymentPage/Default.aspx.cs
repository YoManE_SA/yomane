﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.CommonTypes;
using Netpay.Bll.Merchants;

namespace Netpay.PaymentPage
{
	public partial class Default : System.Web.UI.Page
	{

		private IList<MerchantDto> MerchantList
		{
			get
			{
				IList<MerchantDto> list = new List<MerchantDto>();
				if (ViewState["merchant"] != null)
					list = ViewState["merchant"] as List<MerchantDto>;
				return list;

			}
			set
			{
				ViewState["merchant"] = value;
			}
		}
		private bool generateForm = false;
		private System.Text.StringBuilder RequestString = new System.Text.StringBuilder();
		private System.Text.StringBuilder SignatureString = new System.Text.StringBuilder();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack) 
			{
				PageURL.Text = (new System.Uri(Request.Url, "$$x")).ToString().Replace("$$x", "");
				url_redirect.Text = (new System.Uri(Request.Url, "hosted/checkReply.aspx")).ToString();
				trans_currency.SelectedCurrencyIso = "ZAR";
				SetDefaults();
			}
			form1.Visible = (Request["DebugTest"] == "1") || !Infrastructure.Application.IsProduction;
		}

		private void SetDefaults()
		{
			MerchantList = Merchant.GetMerchantList(null, null, null, null).OrderBy(x => x.CompanyName).ToList();
			merchantID_Recent.Items.Clear();
			merchantID_Recent.DataSource = MerchantList;
			merchantID_Recent.DataValueField = "CustomerNumber";
			merchantID_Recent.DataTextField = "CompanyName";
			merchantID_Recent.DataBind();
			merchantID_Recent.Items.Insert(0, new ListItem("Select...", "-1"));
			trans_currency.Items.Clear();

		}
		protected void SetText(string text)
		{
			txQuery.InnerText = text;
			txQuery.Style.Remove("display");
			btnHideQuery.Style.Remove("display");
		}

		string GetLanguageListString()
		{
			if (ddlDisp_lngList.SelectedIndex < 3) return HttpUtility.UrlEncode(ddlDisp_lngList.SelectedValue);
			string s = string.Empty;
			foreach (ListItem li in cblDisp_lngList.Items)
			{
				if (li.Selected) s += (s == string.Empty ? string.Empty : ",") + li.Value;
			}
			return s;
		}
		
		protected void beginRequestData(bool genForm) 
		{
			RequestString.Clear(); 
			SignatureString.Clear();
			generateForm = genForm;
		}

		protected void addRequestParam(string paramName, string paramValue)
		{
			if (generateForm) RequestString.AppendLine(String.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\" />", paramName, HttpUtility.HtmlEncode(paramValue)));
			else RequestString.Append(String.Format("&{0}={1}", paramName, HttpUtility.UrlEncode(paramValue)));
			SignatureString.Append(paramValue);
		}

		protected string endRequestData(string servicePath) 
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			if (generateForm) {
				sb.AppendLine("<form method=\"post\" action=\"" + servicePath + "/\">");
				sb.AppendLine(RequestString.ToString());
				sb.AppendLine("<input type=\"submit\" value=\"Send\">");
				sb.AppendLine("</form>");
			} else {
				sb.Append(servicePath + "?");
				if (RequestString.Length > 0) sb.Append(RequestString.ToString(1, RequestString.Length - 1));
			}
			return sb.ToString();
		}

		protected string GetHostedString(bool genForm)
		{
			beginRequestData(genForm);
			string strMerchantID = string.IsNullOrEmpty(merchantID_Recent.Text) ? merchantID.Text : merchantID_Recent.Text;
			addRequestParam("merchantID", strMerchantID);
			addRequestParam("url_redirect", url_redirect.Text);
			addRequestParam("url_notify", url_notify.Text);
			if (!string.IsNullOrWhiteSpace(url_redirect_seconds.Text))
				addRequestParam("url_redirect_seconds", url_redirect_seconds.Text);
			addRequestParam("trans_comment", trans_comment.Text);
			if (trans_installments.SelectedItem != null) addRequestParam("trans_installments", trans_installments.SelectedValue);
			addRequestParam("trans_amount", trans_amount.Value);
			if (trans_currency.SelectedItem != null) addRequestParam("trans_currency", Bll.Currency.Get(trans_currency.SelectedValue.ToInt32(0)).IsoCode);
			string sPaymentMethod = (PaymentMedhodCC.Checked ? ",CC" : "") + (PaymentMedhodEC.Checked ? ",EC" : "") + (PaymentMedhodID.Checked ? ",ID" : "") + (PaymentMedhodDD.Checked ? ",DD" : "") + (PaymentMedhodOB.Checked ? ",OB" : "") + (PaymentMedhodWT.Checked ? ",WT" : "") + (PaymentMedhodPD.Checked ? ",PD" : "") + (PaymentMedhodWM.Checked ? ",WM" : "") + (PaymentMedhodPP.Checked ? ",PP" : "");
			if (!string.IsNullOrEmpty(PaymentMedhodOther.Text)) sPaymentMethod += "," + PaymentMedhodOther.Text;
			if (sPaymentMethod.Length > 0) sPaymentMethod = sPaymentMethod.Substring(1);
			addRequestParam("disp_paymentType", sPaymentMethod);
			addRequestParam("disp_payFor", disp_payFor.Text);
			if (!string.IsNullOrWhiteSpace(skin_no.Text)) addRequestParam("skin_no", skin_no.Text);
			if (trans_storePm.Checked) addRequestParam("trans_storePm", "1");
			if (!string.IsNullOrEmpty(trans_recurring1.Text)) addRequestParam("trans_recurring1", trans_recurring1.Text);
			if (!string.IsNullOrEmpty(trans_recurring2.Text)) addRequestParam("trans_recurring2", trans_recurring2.Text);
			if (disp_recurring.SelectedItem != null) addRequestParam("disp_recurring", disp_recurring.SelectedValue);
			if (disp_lng.SelectedItem != null) addRequestParam("disp_lng", disp_lng.SelectedValue);
			string lngList = GetLanguageListString();
			if (!string.IsNullOrWhiteSpace(lngList)) addRequestParam("disp_lngList", lngList);

			if (!string.IsNullOrEmpty(client_fullName.Text)) addRequestParam("client_fullName", client_fullName.Text);
			if (!string.IsNullOrEmpty(client_email.Text)) addRequestParam("client_email", client_email.Text);
			if (!string.IsNullOrEmpty(client_phoneNum.Text)) addRequestParam("client_phoneNum", client_phoneNum.Text);
			if (!string.IsNullOrEmpty(client_idNum.Text)) addRequestParam("client_idNum", client_idNum.Text);

			if (!string.IsNullOrEmpty(client_billaddress1.Text)) addRequestParam("client_billaddress1", client_billaddress1.Text);
			if (!string.IsNullOrEmpty(client_billaddress2.Text)) addRequestParam("client_billaddress2", client_billaddress2.Text);
			if (!string.IsNullOrEmpty(client_billcity.Text)) addRequestParam("client_billcity", client_billcity.Text);
			if (!string.IsNullOrEmpty(client_billzipcode.Text)) addRequestParam("client_billzipcode", client_billzipcode.Text);
			if (!string.IsNullOrEmpty(client_billstate.Text)) addRequestParam("client_billstate", client_billstate.Text);
			if (!string.IsNullOrEmpty(client_billcountry.Text)) addRequestParam("client_billcountry", client_billcountry.Text);

			if (!string.IsNullOrEmpty(ddldisp_mobile.SelectedValue)) addRequestParam("disp_mobile", ddldisp_mobile.SelectedValue);
			if (client_AutoRegistration.Checked) addRequestParam("client_AutoRegistration", "1");

			if (Request["DebugTest"] == "1") addRequestParam("DebugTest", "1");

			if (!string.IsNullOrEmpty(strMerchantID) && ddlSignature.Text.EmptyIfNull() != "")
			{
				var pMerchant = Netpay.Bll.Merchants.Merchant.Load(strMerchantID);
				if (pMerchant != null) {
					string sSign;
					if (cbSignatureV2.Checked) sSign = SignatureString.ToString();
					else sSign = strMerchantID + trans_amount.Value + Bll.Currency.Get(trans_currency.SelectedValue.ToInt32(0)).IsoCode;
					sSign += pMerchant.HashKey;
					if (ddlSignature.Text == "MD5") {
						System.Security.Cryptography.MD5 md = System.Security.Cryptography.MD5.Create();
						sSign = System.Convert.ToBase64String(md.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sSign.ToString())));
					}else if(ddlSignature.Text == "SHA256") {
						System.Security.Cryptography.SHA256 sh = System.Security.Cryptography.SHA256.Create();
						sSign = System.Convert.ToBase64String(sh.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sSign.ToString())));
					}
					addRequestParam("signature", sSign);
				}
			}
			return endRequestData(PageURL.Text + "hosted");
		}

		protected void btnGenerateHosted_Click(object sender, EventArgs e)
		{
			SetText(GetHostedString(false));
		}

		protected void btnOpenPageHosted_Click(object sender, EventArgs e)
		{
			OpenScript.Text = "<script type=\"text/javascript\">window.open('" + GetHostedString(false) + "', 'fraPay', 'scrollbars=1, width=700, height=800, resizable=1, Status=1, top=200, left=250');</script>";
		}

		protected void btnOpenPublic_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(merchantID_Recent.Text) && string.IsNullOrEmpty(merchantID.Text))
			{
				lblError.Text = "Capture merchant ID or select a merchant from the drop-down menu.";
				return;
			}
			else
			{
				string strMerchantID = string.IsNullOrEmpty(merchantID_Recent.Text) ? merchantID.Text : merchantID_Recent.Text;
				OpenScript.Text = "<script type=\"text/javascript\">window.open('" + PageURL.Text + "public/?merchantID=" + strMerchantID + "', 'fraPay', 'scrollbars=1, width=700, height=800, resizable=1, Status=1, top=200, left=250');</script>";
			}
		}

		protected void btnGenerateHostedForm_Click(object sender, EventArgs e)
		{
			SetText(GetHostedString(true));
		}
		
		protected string GetStorageString(bool genForm)
		{
			beginRequestData(genForm);
			string strMerchantID = string.IsNullOrEmpty(merchantID_Recent.Text) ? merchantID.Text : merchantID_Recent.Text;
			addRequestParam("merchantID", strMerchantID);
			addRequestParam("url_redirect", url_redirect.Text);
			addRequestParam("url_notify", url_notify.Text);
			addRequestParam("trans_comment", trans_comment.Text);
			string sPaymentMethod = (PaymentMedhodCC.Checked ? ",CC" : "") + (PaymentMedhodEC.Checked ? ",EC" : "") + (PaymentMedhodID.Checked ? ",ID" : "") + (PaymentMedhodDD.Checked ? ",DD" : "") + (PaymentMedhodOB.Checked ? ",OB" : "") + (PaymentMedhodWT.Checked ? ",WT" : "") + (PaymentMedhodPD.Checked ? ",PD" : "") + (PaymentMedhodWM.Checked ? ",WM" : "") + (PaymentMedhodPP.Checked ? ",PP" : "");
			if (sPaymentMethod.Length > 0) sPaymentMethod = sPaymentMethod.Substring(1);
			addRequestParam("disp_paymentType", sPaymentMethod);
			if (!string.IsNullOrWhiteSpace(skin_no.Text)) addRequestParam("skin_no", skin_no.Text);
			if (!string.IsNullOrWhiteSpace(skin_no.Text)) addRequestParam("skin_no", skin_no.Text);
			if (disp_lng.SelectedItem != null) addRequestParam("disp_lng", disp_lng.SelectedValue);
			string lngList = GetLanguageListString();
			if (!string.IsNullOrWhiteSpace(lngList)) addRequestParam("disp_lngList", lngList);

			if (!string.IsNullOrEmpty(client_fullName.Text)) addRequestParam("client_fullName", client_fullName.Text);
			if (!string.IsNullOrEmpty(client_email.Text)) addRequestParam("client_email", client_email.Text);
			if (!string.IsNullOrEmpty(client_phoneNum.Text)) addRequestParam("client_phoneNum", client_phoneNum.Text);
			if (!string.IsNullOrEmpty(client_idNum.Text)) addRequestParam("client_idNum", client_idNum.Text);

			if (!string.IsNullOrEmpty(client_billaddress1.Text)) addRequestParam("client_billaddress1", client_billaddress1.Text);
			if (!string.IsNullOrEmpty(client_billaddress2.Text)) addRequestParam("client_billaddress2", client_billaddress2.Text);
			if (!string.IsNullOrEmpty(client_billcity.Text)) addRequestParam("client_billcity", client_billcity.Text);
			if (!string.IsNullOrEmpty(client_billzipcode.Text)) addRequestParam("client_billzipcode", client_billzipcode.Text);
			if (!string.IsNullOrEmpty(client_billstate.Text)) addRequestParam("client_billstate", client_billstate.Text);
			if (!string.IsNullOrEmpty(client_billcountry.Text)) addRequestParam("client_billcountry", client_billcountry.Text);
			if (ddldisp_mobile.SelectedItem != null) addRequestParam("mobile", ddldisp_mobile.SelectedValue);
			if (Request["DebugTest"] == "1") addRequestParam("DebugTest", "1");

			if (!string.IsNullOrEmpty(strMerchantID) && ddlSignature.Text.EmptyIfNull() != "")
			{
				string sSign = "";
				var pMerchant = Netpay.Bll.Merchants.Merchant.Load(strMerchantID);
				if (cbSignatureV2.Checked) sSign = SignatureString.ToString();
				else sSign = strMerchantID;
				sSign += pMerchant.HashKey;
				if (ddlSignature.Text == "MD5") {
					System.Security.Cryptography.MD5 md = System.Security.Cryptography.MD5.Create();
					sSign = System.Convert.ToBase64String(md.ComputeHash(System.Text.Encoding.Default.GetBytes(sSign.ToString())));
				} else if (ddlSignature.Text == "SHA256") {
					System.Security.Cryptography.SHA256 sh = System.Security.Cryptography.SHA256.Create();
					sSign = System.Convert.ToBase64String(sh.ComputeHash(System.Text.Encoding.Default.GetBytes(sSign.ToString())));
				}
				addRequestParam("signature", sSign);
			}
			return endRequestData(PageURL.Text + "PaymentMethodStorage");
		}

		protected void btnGenerateStorage_Click(object sender, EventArgs e)
		{
			SetText(GetStorageString(false));
		}

		protected void btnOpenStorage_Click(object sender, EventArgs e)
		{
			OpenScript.Text = "<script type=\"text/javascript\">window.open('" + GetStorageString(false) + "', 'fraPay', 'scrollbars=1, width=700, height=800, resizable=1, Status=1, top=200, left=250');</script>";
		}

		protected void merchantID_Recent_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{ 
				if (merchantID_Recent.SelectedValue != "-1")
				{
					var currCode = GetCurrencyByCustomerCode(merchantID_Recent.SelectedValue);
					var li = trans_currency.Items.FindByText(currCode);
					if (li != null)
					{
						trans_currency.SelectedValue = li.Value;
					}
				} 
			}catch(Exception ex)
			{
				Logger.Log(ex);
				lblError.Text = "An unexpected error occured: " + ex.Message;
			}
		}

		private string GetCurrencyByCustomerCode(string customerCode)
		{
			var result = string.Empty;
			var mdto = MerchantList.FirstOrDefault(x => x.CustomerNumber == customerCode);
			if (mdto != null)
				result = mdto.CurrencyISOCode;
			return result;
		}
	}
}