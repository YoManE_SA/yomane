﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" ValidateRequest="false" CodeBehind="Error.aspx.cs" Inherits="Netpay.PaymentPage.Hosted.Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<br /><br />
	<div style="font-size: 15px; font-weight: bold;">
		<asp:Literal ID="txtErrorMessage" runat="server">An error has occured on this page.</asp:Literal>
	</div>
</asp:Content>
