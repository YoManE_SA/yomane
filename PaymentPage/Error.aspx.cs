﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Infrastructure;

namespace Netpay.PaymentPage.Hosted
{
	public partial class Error : HostedPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string strMessage = Request["message"];
			if (!string.IsNullOrEmpty(strMessage)) strMessage = strMessage.ToEncodedHtml();
			//if (!string.IsNullOrEmpty(strMessage)) strMessage = strMessage.Replace("[", "<").Replace("]", ">");
			txtErrorMessage.Text = strMessage;
		}
	}
}