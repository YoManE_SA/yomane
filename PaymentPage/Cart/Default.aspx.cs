﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Infrastructure.VO;
using Netpay.Web;

namespace Netpay.PaymentPage.Cart
{
	public partial class Default : CartPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Options == null || !Options.IsEnabled) RaiseError(Resources.Main.ErrorSolutionDisabled);
			if (Request["price"] != null) {
				CartItemVO ci = new CartItemVO();
				ci.MerchantID = Merchant.ID;
				ci.CartID = Cart.ID;
				ci.Name = Request["itemName"];
				ci.ItemID = Request["itemID"];
				ci.Quantity = Request["quantity"].ToInt32(1);
				ci.CurrencyID = Request["currency"].ToInt32(0);
				ci.Price = Request["price"].ToNullableDecimal().GetValueOrDefault();
				ci.TaxRatio = Request["taxRate"].ToNullableDecimal().GetValueOrDefault();
				ci.ShippingFee = Request["shipping"].ToNullableDecimal().GetValueOrDefault();
				ci.ItemUrl = Request["infoUrl"];
				ci.ImageUrl = Request["imageUrl"];
				ci.QuantityMin = Request["quantityMin"].ToNullableInt();
				ci.QuantityMax = Request["quantityMax"].ToNullableInt();
				ci.QuantityStep = Request["quantityStep"].ToNullableInt();
				if (!ValidateSignature(new string[] { "itemName", "price", "shipping", "taxRate" }, Request["itemSignature"], true))
					RaiseError(Resources.Main.ErrorParameterSign);
				Netpay.Bll.Cart.AddItem(WebUtils.CurrentDomain.Host, ci, true);
			}
			btnBack.OnClientClick = Request["activationMode"] == "redirect" ? "window.history.back()" : "window.close();";
			loadItems();
		}

		protected void loadItems()
		{
			List<CartItemVO> items = Netpay.Bll.Cart.GetCartItems(WebUtils.CurrentDomain.Host, Cart.ID);
			var currencyGroups = items.GroupBy(i => i.CurrencyID);
			foreach (var group in currencyGroups) 
			{
				int currencyId = group.Key;
				Response.Write(currencyId +":"+ group.Count()+"<br>");
			}

			repeaterGroups.DataSource = currencyGroups;
			repeaterGroups.DataBind();
			
			
			
			Cart.TotalItems = 0; Cart.TotalProducts = 0; Cart.TotalTax = 0; Cart.TotalShipping = 0; Cart.Total = 0;
			//repeaterResults.DataSource = Netpay.Bll.Cart.GetCartItems(WebUtils.CurrentDomain.Host, Cart.ID);
			//repeaterResults.DataBind();
			Cart.Total = Cart.TotalProducts + Cart.TotalShipping + Cart.TotalTax;

			ltTotalQuantity.Text = Cart.TotalItems.ToString();
			ltTotalProsucts.Text = Cart.TotalProducts.ToAmountFormat(WebUtils.CurrentDomain.Host, Cart.Currency);
			ltShipping.Text = Cart.TotalShipping.ToAmountFormat(WebUtils.CurrentDomain.Host, Cart.Currency);
			ltTax.Text = Cart.TotalTax.ToAmountFormat(WebUtils.CurrentDomain.Host, Cart.Currency);
			ltTotal.Text = Cart.Total.ToAmountFormat(WebUtils.CurrentDomain.Host, Cart.Currency);
		}

		protected void repeaterResults_OnItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			DropDownList ddlQuantity = e.Item.FindControl("ddlQuantity") as DropDownList;
			CartItemVO ci = e.Item.DataItem as CartItemVO;
			ddlQuantity.Items.Add(new ListItem("Remove", "0"));

			for (decimal i = ci.QuantityMin.GetValueOrDefault(1); i <= ci.QuantityMax.GetValueOrDefault(20); i += ci.QuantityStep.GetValueOrDefault(1)) 
				ddlQuantity.Items.Add(new ListItem(i.ToString()));
			((System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("ahItemURL")).HRef = ci.ItemUrl;
			((System.Web.UI.WebControls.Image)e.Item.FindControl("imgItem")).ImageUrl = ci.ImageUrl;
			ddlQuantity.SelectedValue = ci.Quantity.ToString();
			Cart.TotalItems += ci.Quantity;
			Cart.TotalProducts += ci.Quantity * ci.Price;
			if (ci.TaxRatio > 0) Cart.TotalTax += (ci.Quantity * ci.Price) * (ci.TaxRatio / 100);
			Cart.TotalShipping += ci.Quantity * ci.ShippingFee;
		}

		protected void Quantity_Changed(object sender, EventArgs e)
		{
			int quantity = (sender as DropDownList).SelectedValue.ToInt32(0);
			int itemID = ((sender as Control).FindControl("ltCartItemID") as Literal).Text.ToInt32(0);
			Netpay.Bll.Cart.UpdateItemQuantity(WebUtils.CurrentDomain.Host, Cart.ID, itemID, quantity);
			loadItems();
		}

		protected void Checkout_Click(object sender, EventArgs e)
		{
			if (Options.IsShippingEnabled) Response.Redirect("Checkout.aspx");
			LoadMerchant();
			CurrencyVO currency = WebUtils.DomainCache.GetCurrency(Cart.Currency);
			string signature = CreateMD5(Merchant.Number + Cart.Total.ToAmountFormat() + currency.IsoCode + Merchant.HashKey, null);
			string finishPage = ((new System.Uri(Request.Url, "Finish.aspx")).ToString()).ToEncodedUrl(); 
			string notifyPage = ((new System.Uri(Request.Url, "Finish.aspx?notify=1")).ToString()).ToEncodedUrl();
			Response.Redirect(WebUtils.CurrentDomain.ProcessV2Url + "?merchantID=" + Merchant.Number + "&trans_amount=" + Cart.Total.ToAmountFormat() + "&trans_currency=" + currency.IsoCode + "&trans_type=0&signature=" + HttpUtility.UrlEncode(signature) + "&trans_installments=1&disp_payFor=" + HttpUtility.UrlEncode("Cart payment") + "&url_notify=" + notifyPage + "&url_redirect=" + finishPage + "&cartID=" + Cart.ID.ToString().ToEncodedUrl());
		}
	}
}