﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.PaymentPage.Cart
{
	public partial class Cart : Netpay.PaymentPage.Code.MasterPageBase
	{
		protected Netpay.PaymentPage.Code.CartPageBase CurrentPage { get { return (Netpay.PaymentPage.Code.CartPageBase)Page; } }
		protected override void OnPreRender(EventArgs e)
		{
			ltCartNumber.Text = CurrentPage.Cart.ReferenceNumber.ToString();
			base.OnPreRender(e);
		}
	}
}