﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Infrastructure.VO;
using Netpay.Web;

namespace Netpay.PaymentPage.Cart
{
	public partial class Checkout : CartPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) SaveData();
			else LoadData();
		}

		protected void ShippingCountry_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			ddlShippingState.CountryID = ddlShippingCountry.SelectedCountryID.GetValueOrDefault();
		}

		protected void LoadData()
		{
			CartVO cart = Cart;
			txRecepientName.Text = cart.RecepientName;
			txRecepientPhone.Text = cart.RecepientPhone;
			txRecepientMail.Text = cart.RecepientMail;
			txShippingAddress.Text = cart.ShippingAddress;
			txShippingAddress2.Text = cart.ShippingAddress2;
			txShippingCity.Text = cart.ShippingCity;
			txShippingZip.Text = cart.ShippingZip;
			if (cart.ShippingState != null) ddlShippingState.SelectedStateID = cart.ShippingState;
			if (cart.ShippingCountry != null) ddlShippingCountry.SelectedCountryID = cart.ShippingCountry;
			vRecepientName.Enabled = vRecepientPhone.Enabled = vRecepientMail.Enabled = 
			vShippingCountry.Enabled = vShippingState.Enabled = vShippingAddress.Enabled = 
			vShippingCity.Enabled = ValidationSummary1.Enabled = Options.IsShippingRequired;
		}

		protected void SaveData()
		{
			CartVO cart = Cart;
			cart.RecepientName = txRecepientName.Text;
			cart.RecepientPhone = txRecepientPhone.Text;
			cart.RecepientMail = txRecepientMail.Text;
			cart.ShippingAddress = txShippingAddress.Text;
			cart.ShippingAddress2 = txShippingAddress2.Text;
			cart.ShippingCity = txShippingCity.Text;
			cart.ShippingZip = txShippingZip.Text;
			if (cart.ShippingState != null) cart.ShippingState = ddlShippingState.SelectedStateID;
			if (cart.ShippingCountry != null) cart.ShippingCountry = ddlShippingCountry.SelectedCountryID;
			Netpay.Bll.Cart.UpdateCart(WebUtils.DomainHost, cart);
		}

		protected void Checkout_Click(object sender, EventArgs e)
		{
			LoadMerchant();
			Validate(); 
			if (!IsValid) return;
			CurrencyVO currency = WebUtils.DomainCache.GetCurrency(Cart.Currency);
			string signature = CreateMD5(Merchant.Number + Cart.Total.ToAmountFormat() + currency.IsoCode + Merchant.HashKey, null);
			string finishPage = ((new System.Uri(Request.Url, "Finish.aspx")).ToString()).ToEncodedUrl();
			string notifyPage = ((new System.Uri(Request.Url, "Finish.aspx?notify=1")).ToString()).ToEncodedUrl();
			Response.Redirect(WebUtils.CurrentDomain.ProcessV2Url + "?merchantID=" + Merchant.Number + "&trans_amount=" + Cart.Total.ToAmountFormat() + "&trans_currency=" + currency.IsoCode + "&trans_type=" + (Options.IsAuthOnly ? 1 : 0) + "&signature=" + HttpUtility.UrlEncode(signature) + "&trans_installments=1&disp_payFor=" + HttpUtility.UrlEncode("Cart payment") + "&url_notify=" + notifyPage + "&url_redirect=" + finishPage + "&cartID=" + Cart.ID.ToString().ToEncodedUrl());
		}

		protected void Back_Click(object sender, EventArgs e)
		{
			Response.Redirect("Default.aspx");
		}

	}
}