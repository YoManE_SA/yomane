﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Checkout.aspx.cs" Inherits="Netpay.PaymentPage.Cart.Checkout" MasterPageFile="~/Cart/Cart.master" %>
<%@ Import Namespace="Netpay.Infrastructure.VO" %>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<table cellspacing="2" cellpadding="2" border="0" class="Form" width="600">
		<tr>
			<td colspan="2">
				<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Cart.Checkout.aspx, FillForm %>" />:<br /><br />
				<asp:ValidationSummary ID="ValidationSummary1" EnableClientScript="true" runat="server" DisplayMode="SingleParagraph" ShowMessageBox="false" ShowSummary="true" CssClass="ValidSummery" HeaderText="<%$ Resources:Main, FieldError %>" />
				<asp:RequiredFieldValidator ID="vRecepientName" ControlToValidate="txRecepientName" Display="None" ErrorMessage="<%$ Resources:Cart.Checkout.aspx, vRecepientName_ErrorMessage %>" runat="server" />
				<asp:RequiredFieldValidator ID="vRecepientPhone" ControlToValidate="txRecepientPhone" Display="None" ErrorMessage="<%$ Resources:Cart.Checkout.aspx, vRecepientPhone_ErrorMessage %>" runat="server" />
				<asp:RequiredFieldValidator ID="vRecepientMail" ControlToValidate="txRecepientMail" Display="None" ErrorMessage="<%$ Resources:Cart.Checkout.aspx, vRecepientMail_ErrorMessage %>" runat="server" />
	
				<asp:RequiredFieldValidator ID="vShippingCountry" ControlToValidate="ddlShippingCountry" Display="None" ErrorMessage="<%$ Resources:Cart.Checkout.aspx, vShippingCountry_ErrorMessage %>" runat="server" />
				<asp:RequiredFieldValidator ID="vShippingState" ControlToValidate="ddlShippingState" Display="None" ErrorMessage="<%$ Resources:Cart.Checkout.aspx, vShippingState_ErrorMessage %>" runat="server" />
				<asp:RequiredFieldValidator ID="vShippingAddress" ControlToValidate="txShippingAddress" Display="None" ErrorMessage="<%$ Resources:Cart.Checkout.aspx, vShippingAddress_ErrorMessage %>" runat="server" />
				<asp:RequiredFieldValidator ID="vShippingCity" ControlToValidate="txShippingCity" Display="None" ErrorMessage="<%$ Resources:Cart.Checkout.aspx, vShippingCity_ErrorMessage %>" runat="server" />
			</td>
		</tr><tr>
			<th><asp:Literal runat="server" Text="<%$ Resources:Cart.Checkout.aspx, RecepientName %>" />:</th>
			<td><asp:TextBox runat="server" ID="txRecepientName" /></td>
		</tr><tr>
			<th><asp:Literal runat="server" Text="<%$ Resources:Cart.Checkout.aspx, RecepientPhone %>" />:</th>
			<td><asp:TextBox runat="server" ID="txRecepientPhone" /></td>
		</tr><tr>
			<th><asp:Literal runat="server" Text="<%$ Resources:Cart.Checkout.aspx, RecepientMail %>" />:</th>
			<td><asp:TextBox runat="server" ID="txRecepientMail" /></td>
		</tr><tr>
			<td><br /></td>
		</tr><tr>
			<th><asp:Literal runat="server" Text="<%$ Resources:Cart.Checkout.aspx, ShippingCountry %>" />:</th>
			<td><netpay:CountryDropDown runat="server" ID="ddlShippingCountry" OnSelectedIndexChanged="ShippingCountry_OnSelectedIndexChanged" /></td>
		</tr><tr>
			<th><asp:Literal runat="server" Text="<%$ Resources:Cart.Checkout.aspx, ShippingState %>" />:</th>
			<td><netpay:StateDropDown runat="server" ID="ddlShippingState" /></td>
		</tr><tr>
			<td><br /></td>
		</tr><tr>
			<th><asp:Literal runat="server" Text="<%$ Resources:Cart.Checkout.aspx, ShippingAddress %>" />:</th>
			<td><asp:TextBox runat="server" ID="txShippingAddress" /></td>
		</tr><tr>
			<th><asp:Literal runat="server" Text="<%$ Resources:Cart.Checkout.aspx, ShippingAddress2 %>" />:</th>
			<td><asp:TextBox runat="server" ID="txShippingAddress2" /></td>
		</tr><tr>
			<th><asp:Literal runat="server" Text="<%$ Resources:Cart.Checkout.aspx, ShippingCity %>" />:</th>
			<td><asp:TextBox runat="server" ID="txShippingCity" /></td>
		</tr><tr>
			<th><asp:Literal runat="server" Text="<%$ Resources:Cart.Checkout.aspx, ShippingZip %>" />:</th>
			<td><asp:TextBox runat="server" ID="txShippingZip" /></td>
		</tr>
	</table>
	<br /><br />
	<table width="600">
	 <tr>
	  <td><asp:Button ID="btnBack" runat="server" UseSubmitBehavior="false" Text="<%$ Resources:Cart.Checkout.aspx, BackToCart %>" OnClick="Back_Click" CssClass="Button" /></td>
	  <td class="reverse"><asp:Button ID="btnCheckout" runat="server" Text="<%$ Resources:Cart.Checkout.aspx, Checkout %>" OnClick="Checkout_Click" UseSubmitBehavior="true" CssClass="Button" /></td>
	 </tr>
	</table>
</asp:Content>
