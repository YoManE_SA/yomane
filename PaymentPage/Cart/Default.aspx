﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Netpay.PaymentPage.Cart.Default" MasterPageFile="~/Cart/Cart.master" %>
<%@ Import Namespace="Netpay.Infrastructure.VO" %>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<table cellspacing="2" cellpadding="2" border="0" width="600" class="FormView" >
		<col span="4" />
		<col style="text-align:right;" />
		<tr>
			<th nowrap width="15%"><asp:Literal runat="server" Text="<%$ Resources:Cart.Default.aspx, ItemNo %>" /></th>
			<th width="50%"><asp:Literal runat="server" Text="<%$ Resources:Cart.Default.aspx, Item %>" /></th>
			<th width="10%"><asp:Literal runat="server" Text="<%$ Resources:Cart.Default.aspx, Price %>" /></th>
			<th width="15%"><asp:Literal runat="server" Text="<%$ Resources:Cart.Default.aspx, Quantity %>" /></th>
			<th width="10%" style="text-align:right !important;"><asp:Literal runat="server" Text="<%$ Resources:Cart.Default.aspx, Total %>" /></th>
		</tr>





		<asp:Repeater ID="repeaterGroups" EnableViewState="false" runat="server" OnItemDataBound="repeaterResults_OnItemDataBound" >
			<ItemTemplate>
				
				<asp:Repeater ID="repeaterGroup" EnableViewState="false" runat="server" OnItemDataBound="repeaterResults_OnItemDataBound" >
					<ItemTemplate>
						<tr>
							<td>
								<asp:Image runat="server" ID="imgItem" Width="30" /><br />
								<asp:Literal runat="server" ID="ltCartItemID" Text="<%# (Container.DataItem as CartItemVO).ID %>" Visible="false" />
								<asp:Literal runat="server" Text="<%# (Container.DataItem as CartItemVO).ItemID %>" />
							</td>
							<td><a runat="server" id="ahItemURL" target="_blank"><%# (Container.DataItem as CartItemVO).Name %></a></td>
							<td nowrap><%# (Container.DataItem as CartItemVO).Price.ToAmountFormat(WebUtils.CurrentDomain.Host, (Container.DataItem as CartItemVO).CurrencyID) %></td>
							<td><asp:DropDownList runat="server" ID="ddlQuantity" AutoPostBack="true" OnSelectedIndexChanged="Quantity_Changed" Width="60" /></td>
							<td nowrap><%# ((Container.DataItem as CartItemVO).Quantity * (Container.DataItem as CartItemVO).Price).ToAmountFormat(WebUtils.CurrentDomain.Host, (Container.DataItem as CartItemVO).CurrencyID)%></td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</ItemTemplate>
		</asp:Repeater>





		<tr>
			<td colspan="5"><br /></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2"><asp:Literal runat="server" Text="<%$ Resources:Cart.Default.aspx, TotalItems %>" /> (<asp:Literal runat="server" ID="ltTotalQuantity" />)</td>
			<td><asp:Literal runat="server" ID="ltTotalProsucts" /></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2"><asp:Literal runat="server" Text="<%$ Resources:Cart.Default.aspx, Tax %>" /></td>
			<td><asp:Literal runat="server" ID="ltTax" /></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2"><asp:Literal runat="server" Text="<%$ Resources:Cart.Default.aspx, Shipping %>" /></td>
			<td><asp:Literal runat="server" ID="ltShipping" /></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2"><asp:Literal runat="server" Text="<%$ Resources:Cart.Default.aspx, Total %>" /></td>
			<td style="font-weight:bold;"><asp:Literal runat="server" ID="ltTotal" /></td>
		</tr>
	</table>
	<br /><br />
	<table width="600">
	 <tr>
	  <td><asp:Button ID="btnBack" runat="server" Text="<%$ Resources:Cart.Default.aspx, BackToSite %>" OnClientClick="document.location='default.aspx';" CssClass="Button" /></td>
	  <td class="reverse"><asp:Button ID="btnCheckout" runat="server" Text="<%$Resources:Cart.Default.aspx, Checkout %>" OnClick="Checkout_Click" CssClass="Button" /></td>
	 </tr>
	</table>
</asp:Content>
