﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Bll;
using Netpay.Infrastructure.VO;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.PaymentPage.Cart
{
	public partial class Finish : CartPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string replyCode = Request["replyCode"];
			int trans_id = Request["trans_id"].ToInt32(0);
			if (!string.IsNullOrEmpty(replyCode) && trans_id != 0)
				if (Netpay.Bll.Cart.SetCartStatus(WebUtils.DomainHost, Cart, false, replyCode, trans_id))
					ClearCart();
		}
	}
}