﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Web;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Controls
{
	public partial class PaymentSolutions : ControlBase, IPostBackEventHandler 
	{
		public event EventHandler PaymentMethodChanged;
		protected void Page_Load(object sender, EventArgs e)
		{
            RaiseBubbleEvent(this, new ElementVisibleEventArgs(ElementVisibleEventArgs.ElementType.NextButton, false));
            RaiseBubbleEvent(this, new ElementVisibleEventArgs(ElementVisibleEventArgs.ElementType.SelectMethod, false));
            rptSolutionGroups.DataSource = Data.PaymentMethodGroups;
            rptSolutions.DataSource = Data.PaymentMethods;
            DataBindChildren(); 
        }

        protected override void DataBindChildren()
        {
            base.DataBindChildren();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            rptSolutionGroups.Visible = rptSolutionGroups.Items.Count > 0;
            rptSolutions.Visible = rptSolutions.Items.Count > 0;
        }

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			if (eventArgument.StartsWith("G")) {
                Data.PaymentMethodGroup = (PaymentMethodGroupEnum)eventArgument.Substring(1).ToInt32(0);
			}else{
                Data.PaymentMethod = (PaymentMethodEnum)eventArgument.ToInt32(0);
			}
			if (PaymentMethodChanged != null) PaymentMethodChanged.Invoke(this, null);
		}

		protected void rptSolutions_OnItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			var pmItem = (e.Item.DataItem as Bll.PaymentMethods.PaymentMethod);
            Literal ltPMTitle = e.Item.FindControl("ltPMTitle") as Literal;
            Literal ltPMText = e.Item.FindControl("ltPMText") as Literal;
            System.Web.UI.HtmlControls.HtmlControl trPaymentMethod = e.Item.FindControl("trPaymentMethod") as System.Web.UI.HtmlControls.HtmlControl;
            if (ltPMTitle != null) ltPMTitle.Text = Resources.PaymentMethods.ResourceManager.GetString("PM_" + pmItem.ID);
            if (ltPMText != null) ltPMText.Text = Resources.PaymentMethods.ResourceManager.GetString("PMDesc_" + pmItem.ID);
            if (trPaymentMethod != null) {
				trPaymentMethod.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, pmItem.ID.ToString()));
				//trPaymentMethod.Attributes.Add("ontouch", Page.ClientScript.GetPostBackEventReference(this, pmItem.ID.ToString()));
			}
		}

		protected void rptSolutionGroups_OnSolutionGroups(object sender, RepeaterItemEventArgs e)
		{
			var pmItem = (e.Item.DataItem as Bll.PaymentMethods.Group);
            Literal ltPMTitle = e.Item.FindControl("ltPMTitle") as Literal;
            Literal ltPMText = e.Item.FindControl("ltPMText") as Literal;
            System.Web.UI.HtmlControls.HtmlControl trPaymentMethod = e.Item.FindControl("trPaymentMethod") as System.Web.UI.HtmlControls.HtmlControl;
            if (ltPMTitle != null) ltPMTitle.Text = Resources.PaymentMethods.ResourceManager.GetString("PMG_" + pmItem.ID);
            if (ltPMText != null) ltPMText.Text = Resources.PaymentMethods.ResourceManager.GetString("PMGDesc_" + pmItem.ID);
            if (trPaymentMethod != null) {
				trPaymentMethod.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, "G" + pmItem.ID.ToString()));
				//trPaymentMethod.Attributes.Add("ontouch", Page.ClientScript.GetPostBackEventReference(this, "G" + pmItem.ID.ToString()));
			}
		}
	}
}