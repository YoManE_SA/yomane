﻿using System;
using System.Web.UI;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.PaymentPage.Code;

namespace Netpay.PaymentPage.Controls
{
    public partial class BillingAddress : ControlBase, IFormControl
    {
        public bool IsRequired { get; set; }
		protected override void OnLoad(EventArgs e)
		{
            // set billing address requirement by merchant settings
            vBillingAddress.Enabled = vBillingAddressCity.Enabled = vBillingAddressCountry.Enabled = vBillingAddressState.Enabled = vBillingAddressZipcode.Enabled = IsRequired;
            spanAddressRequired.Visible = spanCityRequired.Visible = spanZipCodeRequired.Visible = spanCountryRequired.Visible = IsRequired;
            if (ddBillingAddressCountry.SelectedCountry != null)
                spanStateRequired.Visible = vBillingAddressState.Enabled = (ddBillingAddressCountry.SelectedCountry.IsoCode2 == "US" || ddBillingAddressCountry.SelectedCountry.IsoCode2 == "CA");

            tdAddress1.Visible = Settings.IsShowAddress1;
            tdAddress2.Visible = Settings.IsShowAddress2;
            tdCity.Visible = Settings.IsShowCity;
            tdZipCode.Visible = Settings.IsShowZipCode;
            tdCountry.Visible = Settings.IsShowCountry;
            tdState.Visible = Settings.IsShowState;
            //if (!IsPostBack) BillingCountryChange(this, EventArgs.Empty);
            tblBillingAddress.Visible = tdAddress1.Visible || tdAddress2.Visible || tdCity.Visible || tdZipCode.Visible || tdCountry.Visible || tdState.Visible;
            if (IsRequired)
            {
                if (!Data.RiskSettings.IsForceBillingAddressOnlyZipcodeAndCountryAreRequired)
                {
                    if ((Data.client_billaddress1.EmptyIfNull().Trim().Length == 0 && !Settings.IsShowAddress1) ||
                    (Data.client_billcity.EmptyIfNull().Trim().Length == 0 && !Settings.IsShowCity))
                        BasePage.RaiseError("Required billing address1 / city is missing, Please consult the support at the referring site");
                }
                if ((Data.client_billzipcode.EmptyIfNull().Trim().Length == 0 && !Settings.IsShowZipCode) ||
                    (Data.client_billcountry.EmptyIfNull().Trim().Length == 0 && !Settings.IsShowCountry))
                    BasePage.RaiseError("Required billing address postal/state/country is missing, Please consult the support at the referring site");
            }
            base.OnLoad(e);
		}

		protected virtual void BillingCountryChange(object sender, EventArgs e)
        {
            var country = ddBillingAddressCountry.SelectedCountry;
            if (country != null)
            {
                if(ddBillingAddressState.CountryID != country.ID) { 
                    ddBillingAddressState.CountryID = country.ID;
                    ddBillingAddressState.Value = null;
                }
                ddBillingAddressState.DataBind();
                ddBillingAddressState.Enabled = vBillingAddressState.Enabled = spanStateRequired.Visible = (country.IsoCode2 == "US" || country.IsoCode2 == "CA");
            }
            else
                ddBillingAddressState.Enabled = vBillingAddressState.Enabled = spanStateRequired.Visible = false;
        }

        public void SaveData()
        {
			Data.client_billaddress1 = txtBillingAddress.Text.NullIfEmpty();
            Data.client_billaddress2 = txtBillingAddress2.Text.NullIfEmpty();
            Data.client_billcity = txtBillingAddressCity.Text.NullIfEmpty();
            Data.client_billzipcode = txtBillingAddressZipCode.Text.NullIfEmpty();
            Data.client_billstate = ddBillingAddressState.ISOCode.NullIfEmpty();
            Data.client_billcountry = ddBillingAddressCountry.ISOCode.NullIfEmpty();
        }

        public void LoadData()
        {
			txtBillingAddress.Text = Data.client_billaddress1;
			txtBillingAddress2.Text = Data.client_billaddress2;
			txtBillingAddressCity.Text = Data.client_billcity;
			txtBillingAddressZipCode.Text = Data.client_billzipcode;
			ddBillingAddressCountry.ISOCode = Data.client_billcountry;
			ddBillingAddressState.CountryIsoCode = Data.client_billcountry;
            BillingCountryChange(this, EventArgs.Empty);
            ddBillingAddressState.ISOCode = Data.client_billstate;
        }

        public void Validate() 
        {
            vBillingAddress.Enabled = vBillingAddressCity.Enabled = vBillingAddressZipcode.Enabled = vBillingAddressCountry.Enabled = IsRequired;

            vBillingAddressState.Enabled = false;
            if (ddBillingAddressCountry.SelectedCountry != null)
            {
                if (ddBillingAddressCountry.SelectedCountry.IsoCode2 == "US" || ddBillingAddressCountry.SelectedCountry.IsoCode2 == "CA") {
                    vBillingAddressState.Enabled = true;
                    vBillingAddressState.IsValid = (ddBillingAddressState.SelectedValue.Trim() != "");
                }
            }
        }
    
    }
}