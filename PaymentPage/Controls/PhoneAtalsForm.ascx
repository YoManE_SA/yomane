﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="~/Controls/PhoneAtalsForm.ascx.cs" Inherits="Netpay.PaymentPage.Controls.PhoneAtalsForm" %>
<%@ Register Src="~/Controls/BillingAddress.ascx" TagPrefix="Netpay" TagName="BillingAddress" %>
<%@ Register Src="~/Controls/PersonalDetails.ascx" TagPrefix="Netpay" TagName="PersonalDetails" %>
<table class="FormView" width="100%" border="0">
	<tr>
		<th colspan="3" style="padding-top:0!important;">
			<asp:Label ID="lblPhoneDetails" runat="server" Text="<%$ Resources:PhoneForm.ascx, lblPhoneDetails %>" />
		</th>
	</tr>
	<tr runat="server" id="trAtlast">
		<td><asp:Label ID="Label2" runat="server" Text="<%$ Resources:PhoneForm.ascx, lblPhoneDetails %>" /></td>
		<td colspan="3">
			<span class="distinct">
				<asp:RadioButton runat="server" ID="rbMO" GroupName="MessageType" AutoPostBack="true" Text="<%$ Resources:PhoneForm.ascx, MessageTypeMO %>" />
				<asp:RadioButton runat="server" ID="rbMT" GroupName="MessageType" AutoPostBack="true" Text="<%$ Resources:PhoneForm.ascx, MessageTypeMT %>" />
				<asp:RadioButton runat="server" ID="rbIVR" GroupName="MessageType" AutoPostBack="true" Text="<%$ Resources:PhoneForm.ascx, MessageTypeIVR %>" />
			</span>
		</td>
	</tr>
	<asp:MultiView ID="mvTypeView" runat="server" ActiveViewIndex="0">
		<asp:View ID="View1" runat="server">
			<tr>
			 <td>
				<asp:Literal runat="server" Text="<%$ Resources:PhoneForm.ascx, ltOperator %>" /><span id="span1" runat="server" class="asterisk">*</span><br />
				<asp:DropDownList runat="server" ID="ddlOperator" CssClass="InputField" />
			 </td>
			 <td>
				<asp:Literal runat="server" Text="<%$ Resources:PhoneForm.ascx, ltPhoneNumber %>" /><span id="span2" runat="server" class="asterisk">*</span><br />
			   <asp:TextBox runat="server" ID="txtPhoneNumber" MaxLength="255" CssClass="InputField" />
			 </td>
			</tr>
		</asp:View>
		<asp:View ID="View2" runat="server">
			<tr>
			 <td colspan="3">
			  <asp:Label runat="server" ID="ltMessageText" style="border:solid 1px #C0C0C0;width:100%;" />
			 </td>
			</tr>
		</asp:View>
	</asp:MultiView>
</table>
<Netpay:BillingAddress runat="server" ID="billingAddress" />
<Netpay:PersonalDetails runat="server" id="personalDetails" />
