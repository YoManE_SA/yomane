<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="CreditCardForm.ascx.cs" Inherits="Netpay.PaymentPage.Controls.CreditCardForm" %>
<%@ Register Src="~/Controls/BillingAddress.ascx" TagPrefix="Netpay" TagName="BillingAddress" %>
<%@ Register Src="~/Controls/ShippingAddress.ascx" TagPrefix="Netpay" TagName="ShippingAddress" %>
<%@ Register Src="~/Controls/PersonalDetails.ascx" TagPrefix="Netpay" TagName="PersonalDetails" %>

<asp:CustomValidator ID="vCreditcardLuhn" ControlToValidate="wcCreditcard" OnServerValidate="LuhnValidation" Display="None" ValidationGroup="PaymentForm" ErrorMessage="<%$Resources:CreditCardForm.ascx, ValidatorCreditcardNum%>" runat="server" />
<asp:RequiredFieldValidator ID="vCreditcard" ControlToValidate="wcCreditcard" Display="None" ValidationGroup="PaymentForm" ErrorMessage="<%$Resources:CreditCardForm.ascx, ValidatorCreditcardNum%>" runat="server" />
<asp:RequiredFieldValidator ID="vExpirationMonth" ControlToValidate="wcExpirationMonth" Display="None" ValidationGroup="PaymentForm" ErrorMessage="<%$Resources:CreditCardForm.ascx, ValidatorExpirationMonth%>" runat="server" />
<asp:RequiredFieldValidator ID="vExpirationYear" ControlToValidate="wcExpirationYear" Display="None" ValidationGroup="PaymentForm" ErrorMessage="<%$Resources:CreditCardForm.ascx, ValidatorExpirationYear%>" runat="server" />
<asp:RequiredFieldValidator ID="vCardholderName" ControlToValidate="txtCardholderName" Display="None" ValidationGroup="PaymentForm" ErrorMessage="<%$Resources:CreditCardForm.ascx, ValidatorCardholderName%>" runat="server" />
<asp:CustomValidator ID="vCVVFormat" ControlToValidate="txtSecurityNumber" OnServerValidate="CvvValidation" Display="None" ErrorMessage="<%$Resources:CreditCardForm.ascx, ValidatorCVV%>" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vCVV" ControlToValidate="txtSecurityNumber" Display="None" ErrorMessage="<%$Resources:CreditCardForm.ascx, ValidatorCVV%>" ValidationGroup="PaymentForm" runat="server" />

<table class="FormView" width="100%" border="0">
	<tr>
		<th colspan="3" style="padding-top:0!important; padding-bottom: 6px; ">
			<asp:Label ID="lblCardDetails" runat="server" Text="<%$Resources:CreditCardForm.ascx, lblCardDetails%>" />
		</th>
	</tr>
	<tr>
		<td width="33%">
			<asp:Literal Text="<%$Resources:CreditCardForm.ascx, ltCCNumber%>" runat="server" /><span class="asterisk">*</span><br />
			<netpay:CreditCardInput ID="wcCreditcard" runat="server" CssClass="InputField" ElementWidth="38px" />
		</td>
		<td width="33%">
			<asp:Literal runat="server" Text="<%$Resources:CreditCardForm.ascx, ltExpDate%>" /><span class="asterisk">*</span><br />
			<netpay:MonthDropDown ID="wcExpirationMonth" Width="100" runat="server" EnableBlankSelection="true" CssClass="InputField"></netpay:MonthDropDown>
			&nbsp;/&nbsp;
			<netpay:YearDropDown ID="wcExpirationYear" Width="70" runat="server" CssClass="InputField"></netpay:YearDropDown>
		</td>
		<td width="33%"></td>
	</tr>
	<tr>
		<td>
			<asp:Literal ID="ltCardholderName" runat="server" Text="<%$Resources:CreditCardForm.ascx, ltCardholderName%>" /> <span class="asterisk">*</span><br />
			<asp:TextBox runat="server" ID="txtCardholderName" MaxLength="30" CssClass="InputField" />
		</td>
		<td>
			<asp:Literal ID="ltSecurityNumber" runat="server" Text="<%$Resources:CreditCardForm.ascx, ltSecurityNumber%>" /> <span id="spanCVVRequired" runat="server" class="asterisk">*</span>
			<netpay:PageItemHelp ID="CvvTip" runat="server" Text="<%$Resources:Main, TipCvv%>" Width="300" /><br />
			<asp:TextBox runat="server" ID="txtSecurityNumber" MaxLength="30" CssClass="InputField" AutoCompleteType="None" autocomplete="off" />
		</td>
	</tr>
</table>
<Netpay:BillingAddress runat="server" id="billingAddress" />
<Netpay:PersonalDetails runat="server" id="personalDetails" />
<Netpay:ShippingAddress runat="server" id="shippingAddress" />
