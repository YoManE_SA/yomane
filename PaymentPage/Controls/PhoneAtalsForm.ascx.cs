﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Infrastructure;
using System.Text;
using Netpay.PaymentPage.Code;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Configuration;
using Netpay.Web;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Controls
{
    public partial class PhoneAtalsForm : ControlBase, IFormControl
	{
		protected override void OnLoad(EventArgs e)
		{
			//ddlOperator.DataSource = PhoneDebit.GetOperatorList(WebUtils.CurrentDomain.Host);
			ddlOperator.DataTextField = "Value";
			ddlOperator.DataMember = "Key";
			ddlOperator.DataBind();
			mvTypeView.ActiveViewIndex = rbMO.Checked ? 0 : 1;
			ltMessageText.Text = "Fill in the required details and click continue";
			billingAddress.IsRequired = BasePage.RiskSettings.IsForceInstantDebitBillingAddress;
			base.OnLoad(e);
		}

        public void LoadData()
        {
			//ddPaymentMethodCountry.SelectedCountry = CurrentPage.Data.InstantBankCountry;
			//if (Data.Transaction.PayerData != null) {
			//	Data.Payer.PhoneNumber = Data.Payer.PhoneNumber;
			//}
			if (Data.PaymentMethod == PaymentMethodEnum.MicroPaymentsIN) rbMO.Checked = true;
			else if (Data.PaymentMethod == PaymentMethodEnum.MicroPaymentsOut) rbMT.Checked = true;
			else /*if (CurrentPage.Data.PaymentMethod == PaymentMethod.IvrFreeTime)*/ rbIVR.Checked = true;

            billingAddress.LoadData();
            personalDetails.LoadData();
        }

        public void SaveData() 
		{
			Data.client_phoneNum = txtPhoneNumber.Text;
			if (rbMO.Checked) Data.PaymentMethod = PaymentMethodEnum.MicroPaymentsIN;
			else if(rbMT.Checked) Data.PaymentMethod = PaymentMethodEnum.MicroPaymentsOut;
			else if (rbIVR.Checked) Data.PaymentMethod = PaymentMethodEnum.IvrPerCall;
			//CurrentPage.Data.PhoneDetails.MessageDirection = rbMO.Checked ? PhoneMessageDirection.SystemToUser : PhoneMessageDirection.UserToSystem;
            billingAddress.SaveData();
            personalDetails.SaveData();
        }
        
        public void Validate() 
        {
            billingAddress.Validate();
            Data.PaymentMethodType = PaymentMethodType.PhoneDebit;
        }
	}
}