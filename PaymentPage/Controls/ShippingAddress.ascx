<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingAddress.ascx.cs" Inherits="Netpay.PaymentPage.Controls.ShippingAddress" %>
<asp:RequiredFieldValidator ID="vShippingAddress" ControlToValidate="txtShippingAddress" Display="None" ErrorMessage="<%$Resources:ShippingAddress.ascx, ValidatorAddress%>" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vShippingAddressCity" ControlToValidate="txtShippingAddressCity" Display="None" ErrorMessage="<%$Resources:ShippingAddress.ascx, ValidatorCity%>" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vShippingAddressZipcode" ControlToValidate="txtShippingAddressZipCode" Display="None" ErrorMessage="<%$Resources:ShippingAddress.ascx, ValidatorZipcode%>" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vShippingAddressCountry" ControlToValidate="ddShippingAddressCountry" Display="None" ErrorMessage="<%$Resources:ShippingAddress.ascx, ValidatorCountry%>" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vShippingAddressState" ControlToValidate="ddShippingAddressState" Display="None" ErrorMessage="<%$Resources:ShippingAddress.ascx, ValidatorState%>" ValidationGroup="PaymentForm" runat="server" />
<asp:PlaceHolder runat="server" id="tblShippingAddress">
	<script type="text/javascript">
		function disableShippingFields(bDisabled) {
			document.getElementById('txtShippingAddress').value = ''; document.getElementById('txtShippingAddress').disabled = bDisabled;
			document.getElementById('txtShippingAddressCity').value = ''; document.getElementById('txtShippingAddressCity').disabled = bDisabled;
			document.getElementById('txtShippingAddressZipCode').value = ''; document.getElementById('txtShippingAddressZipCode').disabled = bDisabled;
			document.getElementById('txtShippingAddress2').value = ''; document.getElementById('txtShippingAddress2').disabled = bDisabled;
			document.getElementById('ddShippingAddressCountry').value = ''; document.getElementById('ddShippingAddressCountry').disabled = bDisabled;
			document.getElementById('ddShippingAddressState').value = ''; document.getElementById('ddShippingAddressState').disabled = bDisabled;
		}
	</script>
    <table class="FormView" width="100%" border="0" >
	    <tr>
	     <td width="33%"></td><td width="33%"></td><td width="33%"></td>
	    </tr>
	    <tr>
		    <th colspan="3">
			    <asp:Label ID="ltShippingAddress" runat="server" Text="<%$Resources:ShippingAddress.ascx,ltShippingAddress%>"  />
		    </th>
	    </tr>
	    <tr>
		    <td colspan="3">
			    <asp:CheckBox runat="server" ID="chkUseSameAsBilling" CssClass="distinct" onclick="disableShippingFields(this.checked)" Text="<%$Resources:Main,UseShippingSameAsBilling %>" /><br /><br />
		    </td>
	    </tr><tr>
            <asp:PlaceHolder runat="server" ID="tdAddress1">
		        <td>
			        <asp:Literal runat="server" ID="ltAddress1" Text="<%$Resources:ShippingAddress.ascx, ltAddress1%>" /><span id="spanAddressRequired" runat="server" class="asterisk">*</span>
			        <netpay:PageItemHelp runat="server" ID="pihelpAddress1" Text="<%$Resources:Main,TipAddressLine1%>" Width="300" Height="28" /><br />
			        <asp:TextBox runat="server" ID="txtShippingAddress" MaxLength="255" CssClass="InputField" ClientIDMode="Static" />
		        </td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="tdCity">
		        <td>
			        <asp:Literal runat="server" ID="ltCity" Text="<%$Resources:ShippingAddress.ascx, ltCity%>" /><span id="spanCityRequired" runat="server" class="asterisk">*</span><br />
			        <asp:TextBox runat="server" ID="txtShippingAddressCity" MaxLength="30" CssClass="InputField" ClientIDMode="Static" />
    		    </td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="tdZipCode">
		        <td>
			        <asp:Literal runat="server" ID="ltZipCode" Text="<%$Resources:ShippingAddress.ascx, ltZipCode%>" /><span id="spanZipCodeRequired" runat="server" class="asterisk">*</span><br />
			        <asp:TextBox runat="server" ID="txtShippingAddressZipCode" MaxLength="10" CssClass="InputField" ClientIDMode="Static" />
		        </td>
            </asp:PlaceHolder>
	    </tr>
	    <tr>
            <asp:PlaceHolder runat="server" ID="tdAddress2">
		        <td>
			        <asp:Literal runat="server" Text="<%$Resources:ShippingAddress.ascx, ltAddress2%>" ID="ltAddress2" />
			        <netpay:PageItemHelp runat="server" ID="pihelpAddress2" Text="<%$Resources:Main,TipAddressLine2%>" Width="300" Height="28" /><br />
			        <asp:TextBox runat="server" ID="txtShippingAddress2" MaxLength="255" CssClass="InputField" ClientIDMode="Static" />
		        </td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="tdCountry">
		        <td>
			        <asp:Literal runat="server" Text="<%$Resources:ShippingAddress.ascx, ltCountry%>" ID="ltCountry" /><span id="spanCountryRequired" runat="server" class="asterisk">*</span><br />
			        <netpay:CountryDropDown ID="ddShippingAddressCountry" AutoPostBack="true" OnSelectedIndexChanged="ShippingCountryChange" ClientIDMode="Static" runat="server" CssClass="InputField"></netpay:CountryDropDown>
		        </td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="tdState">
		        <td>
			        <asp:Literal runat="server" Text="<%$Resources:ShippingAddress.ascx, ltState%>" ID="ltState" /><span id="spanStateRequired" runat="server" class="asterisk">*</span><br />
			        <netpay:StateDropDown ID="ddShippingAddressState" StartEmpty="true" runat="server" CssClass="InputField" ClientIDMode="Static" />
		        </td>
            </asp:PlaceHolder>
	    </tr>	
    </table>
</asp:PlaceHolder>
