<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MerchantCustomize.ascx.cs" Inherits="Netpay.PaymentPage.Controls.MerchantCustomize" %>
<table width="100%" border="0" cellpadding="0" cellspacing="0" runat="server" id="tblMerchantText">
	<tr><td><br /></td></tr>
	<tr>
		<td colspan="2" class="MerchantMsg">
			<asp:Image id="LogoLeft" runat="server" style="float:left" visible="false" />
			<asp:Image id="LogoRight" runat="server" style="float:right" visible="false" />
			<asp:Literal runat="server" ID="ltMerchantText" /><br />
		</td>
	</tr>
</table>
