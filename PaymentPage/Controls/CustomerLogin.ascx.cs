﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;
using Netpay.PaymentPage.Code;

namespace Netpay.PaymentPage.Controls
{
	public partial class Customer : ControlBase
	{
		protected void Login_Click(object sender, EventArgs e)
		{
			if (loginEmail.Text.Length == 0 || loginPassword.Text.Length == 0) ValidationWrongUser.IsValid = false;
            if (string.IsNullOrEmpty(ddlAppIdentity.SelectedValue))
            {
                ValidationWrongUser.IsValid = false;
                ValidationWrongUser.ErrorMessage = "Can't login without Account provider";
                return;
            }
			Page.Validate();
			if (!Page.IsValid) return;
			Logout_Click(null, null);
			var result = Web.WebUtils.Login(new UserRole[] { UserRole.Customer }, null, loginEmail.Text, loginPassword.Text, int.Parse(ddlAppIdentity.SelectedValue));
			if (result == LoginResult.Success){
				ValidationWrongUser.IsValid = true;
				ValidationWrongUser.ErrorMessage = string.Empty;
				var walletUser = Bll.Customers.Customer.Current;
				litWalletUser.Text = ((string)GetGlobalResourceObject("Main", "WalletUserIsLoggedIn")).Replace("%FIRST%", walletUser.FirstName).Replace("%LAST%", walletUser.LastName);
			} else {
				ValidationWrongUser.IsValid = false;
                string errorDesc = GetGlobalResourceObject("CustomerLogin.ascx", Infrastructure.Security.LoginResult.WrongPassword.ToString()) as string;
                if (string.IsNullOrEmpty(errorDesc)) errorDesc = result.ToString();
                ValidationWrongUser.ErrorMessage = errorDesc;
			}
			SetActiveView();
		}

		protected void Logout_Click(object sender, EventArgs e)
		{
			WebUtils.Logout();
			if (sender !=null) SetActiveView();
		}

		void SetActiveView()
		{
			mvWallet.ActiveViewIndex = (Bll.Customers.Customer.Current != null ? 1 : 0);
            if (mvWallet.ActiveViewIndex == 1)
            {
                LoadCustomerPaymentOptions();
            }
            else
            {
                LoadAppIdentities();
            }
		}

		protected override void OnLoad(EventArgs e)
		{
            base.OnLoad(e);
			SetActiveView();
		}

        protected  void LoadAppIdentities()
        {
            var currentMerchant = Data.Merchant;
            if (currentMerchant == null) throw new Exception("no merchant is logged in");

            List<Bll.ApplicationIdentity> identities = Bll.ApplicationIdentity
                .Search(new Bll.ApplicationIdentity.SearchFilters()
                {
                    IsActive = true,
                    MerchantGroups = currentMerchant.MerchantGroup.HasValue ? new List<int> { currentMerchant.MerchantGroup.Value } : null,
                    OnlyAppIdentitiesWithoutGroup = currentMerchant.MerchantGroup.HasValue ? null : true.ToNullableBool()
                }, null);

            //Check if any identity was found, if not show error.
            if (identities.Count > 0)
            {
                foreach (var identity in identities)
                {
                    string imgElement = identity.IsAppIdentityHaveLogo ? string.Format("<img src='{0}' /> {1}", identity.GetAppIdentityVirtualLogoPath, identity.Name) : identity.Name;
                    ListItem item = new ListItem(imgElement, identity.ID.ToString());
                    if (!ddlAppIdentity.Items.Contains(item))
                        ddlAppIdentity.Items.Add(item);
                }

                ddlAppIdentity.DataBind();
            }
            else
            {
                ValidationWrongUser.IsValid = false;
                ValidationWrongUser.ErrorMessage = "No account provider available";
                return;
            }
        }

        protected void LoadCustomerPaymentOptions()
		{
			rptCardList.DataSource = Bll.Accounts.StoredPaymentMethod.LoadForAccount();
			rptCardList.DataBind();
		}

		protected void Pay(object sender, EventArgs e)
		{
            //Validate pincode
            bool isPinCodeValid = Netpay.Bll.Customers.Customer.Current.ValidatePinCode(txtPinCode.Text.Trim());
            if (!isPinCodeValid)
            {
                PinCodeActionNotify.SetMessage("Please insert a valid PinCode", true);
                return;
            }
                
            Data.CustomerID = Netpay.Bll.Customers.Customer.Current.ID;
            if (Request["PayWith"].ToInt32(-1) > 0)
				PayByCard();
			else
				PayByBalance();
			LoadCustomerPaymentOptions();
		}

		protected void PayByBalance()
		{
			string sIso = Request["PayWith"];
			if (string.IsNullOrEmpty(sIso)) return;
			sIso = sIso.Trim();
			if (sIso.Length != 3) return;
			var curPay = Netpay.Bll.Currency.Get(sIso);
			if (curPay == null) return;
			decimal amountPay = (Data.Currency.ID == curPay.ID ? Data.trans_amount : System.Math.Round(Data.trans_amount / curPay.BaseRate * Data.Currency.BaseRate, 2));
			txtResponse.Text = "Under construction !!!";
			txtResponse.Text += string.Format("<br />To merchant: {0} {1}", Data.trans_amount, Data.Currency.IsoCode);
			txtResponse.Text += string.Format("<br />From customer: {0} {1}", amountPay, curPay.IsoCode);
			int nMerchantBalanceID = 0; //Bll.Accounts.Balance.PayFromBalance(Data.CustomerLogin, (CommonTypes.Currency)curPay.ID, amountPay, Data.MerchantNum, (CommonTypes.Currency)Data.Currency.ID, Data.Amount, Data.PayFor + " - " + Data.Comment);
			txtResponse.Text += string.Format((nMerchantBalanceID > 0 ? "<br />Paid successfully, Transfer No. {0}" : "<br />Failed to pay, reply code: {0}"), nMerchantBalanceID);
            txtResponse.Visible = true;
        }

		protected void PayByCard()
		{
			int nSelID = Request["PayWith"].ToInt32(-1);
			if (nSelID < 0) return;
			var pInfo = Netpay.Bll.Customers.Customer.Current;
            //Data.Transaction.CustomerID = pInfo.ID;

            //copy personal info
            Data.client_fullName = pInfo.FullName;
            Data.client_email = pInfo.EmailAddress;
            Data.client_phoneNum = pInfo.PhoneNumber;
            Data.client_idNum = pInfo.PersonalNumber;
            Data.client_dateOfBirth = pInfo.DateOfBirth;

            //copy payment method
            var pCard = Netpay.Bll.Accounts.StoredPaymentMethod.Load(nSelID);
            if(pCard != null) { 
                string encValue1, encValue2;
                Data.PaymentMethod = pCard.MethodInstance.PaymentMethodId;
                Data.pm_expDate = pCard.MethodInstance.ExpirationDate;
                pCard.MethodInstance.GetDecryptedData(out encValue1, out encValue2);
                Data.pm_value1 = encValue1;
                Data.pm_value2 = encValue2;
                //Data.Transaction.PayerData..CardholderName = pCard.OwnerName;
            }

            //copy billing info
            Bll.Accounts.AccountAddress address = pCard.BillingAddress;
            if (address == null) address = pInfo.PersonalAddress;
            if (address != null) {
                Data.client_billaddress1 = address.AddressLine1;
                Data.client_billaddress2 = address.AddressLine2;
                Data.client_billcity = address.City;
                Data.client_billzipcode = address.PostalCode;
                Data.client_billstate = address.StateISOCode;
                Data.client_billcountry = address.CountryISOCode;
            }
			BasePage.Redirect("~/Hosted/PaymentConfirm.aspx");
		}
    }
}