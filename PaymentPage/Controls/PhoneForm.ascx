<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PhoneForm.ascx.cs" Inherits="Netpay.PaymentPage.Controls.PhoneForm" %>
<%@ Register Src="~/Controls/BillingAddress.ascx" TagPrefix="Netpay" TagName="BillingAddress" %>
<%@ Register Src="~/Controls/ShippingAddress.ascx" TagPrefix="Netpay" TagName="ShippingAddress" %>
<%@ Register Src="~/Controls/PersonalDetails.ascx" TagPrefix="Netpay" TagName="PersonalDetails" %>
<table class="FormView" width="100%" border="0">
	<tr>
		<th colspan="3" style="padding-top:0!important;">
			<asp:Label ID="lblPhoneDetails" runat="server" Text="<%$ Resources:PhoneForm.ascx, lblPhoneDetails %>" />
		</th>
	</tr>
	<tr>
		<td runat="server" id="td1">
		    <asp:Literal runat="server" Text="<%$ Resources:InstantDebitForm.ascx, ltFullName %>" /><span runat="server" class="asterisk">*</span><br />
		    <asp:TextBox runat="server" ID="txtFullName" CssClass="InputField" />
		</td>
		<td runat="server" id="tdOperator">
		    <asp:Literal runat="server" Text="<%$ Resources:PhoneForm.ascx, ltOperator %>" /><span id="span1" runat="server" class="asterisk">*</span><br />
		    <asp:DropDownList runat="server" ID="ddlOperator" CssClass="InputField" />
		</td>
		<td>
		    <asp:Literal runat="server" Text="<%$ Resources:PhoneForm.ascx, ltPhoneNumber %>" /><span id="span2" runat="server" class="asterisk">*</span><br />
		    <asp:TextBox runat="server" ID="txtPhoneNumber" MaxLength="255" CssClass="InputField" />
		</td>
	</tr>
</table>
<Netpay:BillingAddress runat="server" ID="billingAddress" />
<Netpay:PersonalDetails runat="server" id="personalDetails" PhoneVisible="false" PhoneRequired="false" />
<Netpay:ShippingAddress runat="server" id="shippingAddress" />
