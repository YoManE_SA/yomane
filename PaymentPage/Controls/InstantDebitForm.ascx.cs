﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using System.Text;
using Netpay.PaymentPage.Code;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Configuration;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Controls
{
	public class ImageRadioList : System.Web.UI.WebControls.RadioButtonList, IPostBackEventHandler 
	{
		public string SelectedImage = null;
		public string ItemCssStyle = "padding:2px;";
		public bool HideImages { set; get; }
		public System.Web.UI.WebControls.Unit ItemWidth { get; set; }
		public System.Web.UI.WebControls.Unit ItemHeight { get; set; }
		public ImageRadioList()
		{
			Width = System.Web.UI.WebControls.Unit.Pixel(161);
			Height = System.Web.UI.WebControls.Unit.Pixel(70);
		}

		void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{ 
			string[] sValues = eventArgument.Split(','); 
			base.SelectedIndex = sValues[0].ToInt32(0);
			SelectedImage = sValues[1];
			OnSelectedIndexChanged(new EventArgs());
		}

		public void AddItem(string text, string value, string image)
		{
			ListItem li = new ListItem(text, value);
			li.Attributes.Add("Image", image);
			Items.Add(li);
		}

		protected override void RenderItem(ListItemType itemType, int repeatIndex, RepeatInfo repeatInfo, HtmlTextWriter writer)
		{
			ListItem li = Items[repeatIndex];
			string sImage = li.Attributes["Image"];
			if (string.IsNullOrEmpty(sImage)) sImage = "NoImage";
			writer.Write("<div style=\"width:" + ItemWidth.ToString() + ";height:" + ItemHeight.ToString() + ";" + ItemCssStyle + "\" onmouseover=\"this.className='MouseOver';\"  onmouseout=\"this.className='';\" onclick=\"" + Page.ClientScript.GetPostBackEventReference(this, repeatIndex.ToString() + "," + sImage) + "\">");
			if (!HideImages) writer.Write(" <img style=\"width:" + ItemWidth.ToString() + ";height:" + ItemHeight.ToString() + ";\" src=\"" + sImage + "\" title=\"" + li.Text + "\"></br>");
            writer.Write("<div class=\"button-bank\">" + li.Text + "</div>");
			writer.Write("</div>");
		}
	}

    public partial class InstantDebitForm : ControlBase, IFormControl
	{

        public bool GeneralInfoVisible {
            get { return billingAddress.Visible; }
            set { billingAddress.Visible = personalDetails.Visible = value; }
        }
        
        protected void irlBankIDList_Changed(object sender, EventArgs e)
		{
			if (irlBankIDList.SelectedItem == null) return;
			imgSelectedBank.ImageUrl = irlBankIDList.SelectedImage;
			ltSelectedBank.Text = imgSelectedBank.ToolTip = irlBankIDList.SelectedItem.Text;
			mvInsDebitForm.SetActiveView(viewBankInfo);
            GeneralInfoVisible = true;
		}

        protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
            personalDetails.PersonalIDRequired = false; //CurrentPage.Settings.IsDirectDebitRequiredID;
            personalDetails.EmailRequired = Settings.IsDirectDebitRequiredEmail;
            personalDetails.PhoneRequired = Settings.IsDirectDebitRequiredPhone;
            billingAddress.IsRequired = BasePage.RiskSettings.IsForceInstantDebitBillingAddress;
            //ddPaymentMethod.PaymentMethodTypes = new PaymentMethodType[] { PaymentMethodType.ECheck, PaymentMethodType.InstantDebit };
        }
        protected void UpdateCountries(object sender, EventArgs e)
		{
            //if (Data.PaymentMethod != PaymentMethod.Unknown) return;
			PaymentMethodGroupEnum[] paymentMethodGroups = new PaymentMethodGroupEnum[] { Data.PaymentMethodGroup };
			ddPaymentMethodCountry.DataSource = Bll.PaymentMethods.PaymentMethod.GetPaymentMethodCountries(Data.Currency.ID, new PaymentMethodType[] { PaymentMethodType.ECheck, PaymentMethodType.BankTransfer }, (Data.PaymentMethodGroup != PaymentMethodGroupEnum.Other ? paymentMethodGroups : null));
			ddPaymentMethodCountry.DataBind();
            bool IpCountryExist = (Data.IpCountry != null && ddPaymentMethodCountry.Items.FindByValue(Data.IpCountry.ID.ToString()) != null);
            if (IpCountryExist && (string.IsNullOrEmpty(Data.pm_IssuerCountryIso) || Data.pm_IssuerCountryIso == Data.IpCountry.IsoCode2))
            {
                ddPaymentMethodCountry.SelectedCountry = Data.IpCountry;
                PaymentMethodCountryChange(sender, e);
            }
        }

        protected virtual void PaymentMethodCountryChange(object sender, EventArgs e)
		{
			// set payment methods for country
			var country = ddPaymentMethodCountry.SelectedCountry;
			irlPaymentMethod.Enabled = (country != null);
			Data.pm_IssuerCountryIso = ddPaymentMethodCountry.ISOCode;
            mvInsDebitForm.Visible = GeneralInfoVisible = false;
			irlPaymentMethod.Items.Clear();
			if (country == null) return;
			irlPaymentMethod.SelectedValue = null;
			PaymentMethodGroupEnum[] paymentMethodGroups = new PaymentMethodGroupEnum[] { Data.PaymentMethodGroup };
			var paymentMethods = Bll.PaymentMethods.PaymentMethod.GetCountryPaymentMethods(country.ID, Data.Currency.ID, new PaymentMethodType[] { PaymentMethodType.ECheck, PaymentMethodType.BankTransfer }, (Data.PaymentMethodGroup != PaymentMethodGroupEnum.Other ? paymentMethodGroups : null));
			foreach (var p in paymentMethods)
                irlPaymentMethod.AddItem(p.Name, p.ID.ToString(), getMethodImage(p.ID, true));
			lbPMError.Visible = (paymentMethods.Count == 0);
			mvInsDebitForm.SetActiveView(viewSelectMethod);
			mvInsDebitForm.Visible = true;
            PaymentMethodChange(sender, e);
        }

        protected virtual void PaymentMethodChange(object sender, EventArgs e)
		{
            //mvInsDebitForm.Visible = ddPaymentMethod.IsSelected;
			if (irlPaymentMethod.SelectedItem == null) return;
			imgSelectedMethod.ImageUrl = getMethodImage(irlPaymentMethod.SelectedValue.ToNullableInt32().GetValueOrDefault(), false);
			if (irlPaymentMethod.SelectedItem != null) ltSelectedMethod.Text = irlPaymentMethod.SelectedItem.Text;
			// find if billing address is required by solution or merchant
			int paymentMethodID = irlPaymentMethod.SelectedValue.ToInt32(0);
			var paymentMethod = Bll.PaymentMethods.PaymentMethod.Get(paymentMethodID);
			billingAddress.IsRequired = (paymentMethod.IsBillingAddressMandatory || BasePage.RiskSettings.IsForceInstantDebitBillingAddress);
            mvInsDebitForm.SetActiveView(paymentMethod.IsPMInfoMandatory ? viewAccountDetails : viewAccountNameOnly);
            vAccountNameValidator.Enabled = vAccountNameValidator2.Enabled = paymentMethod.IsPMInfoMandatory;
			//spanAddressRequired.Visible = spanCityRequired.Visible = spanCountryRequired.Visible = spanStateRequired.Visible = spanZipCodeRequired.Visible = isBillingAddressRequired;
            GeneralInfoVisible = (paymentMethodID != (int)PaymentMethodEnum.Unknown) && (paymentMethodID != (int)PaymentMethodEnum.PushBankTransfer);
			if (paymentMethodID != (int)PaymentMethodEnum.PushBankTransfer) return;
            vAccountNameValidator.ControlToValidate = "txtAccountName2";
            vAccountNameValidator2.ControlToValidate = "txtAccountName2";
			string responseData;
			StringBuilder requestBuilder = new StringBuilder();
			requestBuilder.Append(BasePage.CurrentDomain.ProcessUrl + "remoteCharge_echeckGetBanks.asp");
			requestBuilder.Append("?CompanyNum=" + BasePage.Merchant.Number.ToEncodedUrl());
			requestBuilder.Append("&Currency=" + Data.Currency.ID.ToString());
			requestBuilder.Append("&PaymentMethod=" + paymentMethodID);
			requestBuilder.Append("&Country=" + ddPaymentMethodCountry.SelectedValue.ToEncodedUrl());
			//Response.Write(requestBuilder.ToString());
			irlBankIDList.Items.Clear();
			try {
				Data.SendChargeRequest(requestBuilder.ToString(), out responseData, Data.PaymentMethodType);
				System.Collections.Specialized.NameValueCollection responseValues = HttpUtility.ParseQueryString(responseData);
				if (responseValues["Reply"] != "000") return;
				bool HasValues = !string.IsNullOrEmpty(responseValues["BankList"]);
				if (HasValues) {
					string[] sValues = responseValues["BankList"].EmptyIfNull().Split(';');
					for (int i = 0; i < sValues.Length; i++) {
						string[] nSel = sValues[i].Split(',');
						if (nSel.Length == 3) irlBankIDList.AddItem(nSel[1], nSel[0], nSel[2]);
					}
					lbBankError.Visible = false;
                    mvInsDebitForm.Visible = true;
					mvInsDebitForm.SetActiveView(viewSelectBank);
				} else {
					mvInsDebitForm.SetActiveView(viewSelectBank);
					lbBankError.Visible = true;
				}
			} catch(Exception ex) {
				Netpay.Infrastructure.Logger.Log(LogTag.PaymentPage, ex);
			}
		}

        public void setFixedMethod(PaymentMethodEnum pm)
        {
            if (pm != PaymentMethodEnum.Unknown)
            {
                lblPayMethod.Visible = dvBankCountrySelect.Visible = dvBankCountryFixed.Visible = false;
                mvInsDebitForm.Visible = true;
                irlPaymentMethod.AddItem(pm.ToString(), ((int)pm).ToString(), getMethodImage((int)pm, false));
                irlPaymentMethod.SelectedValue = ((int)pm).ToString();
                vPaymentMethodCountry.Enabled = false;
                PaymentMethodChange(this, null);
            }
            else
            {
                GeneralInfoVisible = false;
                mvInsDebitForm.Visible = false;
                mvInsDebitForm.ActiveViewIndex = -1;
                ddPaymentMethodCountry.SelectedValue = null;
                lblPayMethod.Visible = true;
                UpdateCountries(this, new EventArgs());
            }
        }

        public void LoadData()
		{
			txtAccountName3.Text = txtAccountName2.Text = txtAccountName.Text = Data.client_fullName;
            if (ddPaymentMethodCountry.Items.Count == 0) UpdateCountries(this, new EventArgs());
            setFixedMethod(Data.PaymentMethod);
            if (Data.PaymentMethod == PaymentMethodEnum.Unknown) {
                ddPaymentMethodCountry.ISOCode = Data.pm_IssuerCountryIso;
                PaymentMethodCountryChange(this, new EventArgs());
            }
			if (Data.PaymentMethodType == PaymentMethodType.BankTransfer) {
				//var methodData = Data.Transaction.PaymentData.MethodInstance as Bll.PaymentMethods.BankAccount;
				txtRouting.Text = Data.pm_value1;
				txtAccountNumber.Text = Data.pm_value2;
			} else if (Data.PaymentMethod == PaymentMethodEnum.PushBankTransfer) {
				irlPaymentMethod.SelectedValue = ((int)Data.PaymentMethod).ToString();
				//PaymentMethodChange(this, new EventArgs());
				if (!string.IsNullOrEmpty(Data.pm_IssuerId)) {
					irlBankIDList.SelectedValue = Data.pm_IssuerId;
					if (irlBankIDList.SelectedItem != null) {
						imgSelectedBank.ImageUrl = irlBankIDList.SelectedItem.Attributes["Image"];
						ltSelectedBank.Text = irlBankIDList.SelectedItem.Text;
						mvInsDebitForm.SetActiveView(viewBankInfo);
					}
					GeneralInfoVisible = true;
				}
			}
            billingAddress.LoadData();
			if (shippingAddress != null) shippingAddress.LoadData();
			personalDetails.LoadData();
		}

		private string getAccoutName {
            get { 
                switch(mvInsDebitForm.ActiveViewIndex){
                    case 2: return txtAccountName2.Text;
                    case 3: return txtAccountName.Text;
                    case 4: return txtAccountName3.Text;
                }
                return null;
            }
        }

        public void SaveData()
		{
			Data.client_fullName = getAccoutName;

			Data.PaymentMethod = (PaymentMethodEnum)irlPaymentMethod.SelectedValue.EmptyIfNull().ToInt32(0);
			//Data.PaymentMethod = Data.PaymentMethod;
			Data.pm_IssuerName = null;
            if (Data.PaymentMethod == PaymentMethodEnum.PushBankTransfer)
            {
				Data.pm_IssuerId = null;
				if (irlBankIDList.SelectedItem != null)
				{
					Data.pm_IssuerId = irlBankIDList.SelectedItem.Value;
					//pushInfo.BankName = irlBankIDList.SelectedItem.Text;
					Data.pm_IssuerName = irlBankIDList.SelectedItem.Text;
				}
			}else{
				//var bankAccount = Data.Transaction.PaymentData.MethodInstance as Bll.PaymentMethods.BankAccount;
				Data.pm_value1 = txtRouting.Text;
                Data.pm_value2 = txtAccountNumber.Text;
			}
            billingAddress.SaveData();
			if (shippingAddress != null) shippingAddress.SaveData();
			personalDetails.SaveData();
        }

        public void Validate() { 
            billingAddress.Validate();
			if (shippingAddress != null) shippingAddress.Validate();
			Data.PaymentMethodType = PaymentMethodType.BankTransfer;
        }

		protected virtual string getCountryImage(string countryIso)
		{
            if (!string.IsNullOrEmpty(countryIso)) return string.Format("/NPCommon/ImgCountry/18X12/{0}.gif", countryIso);
            return "/NPCommon/ImgCountry/18X12/Unknown.gif";
		}

        protected virtual string getMethodImage(int pmId, bool isBig)
        {
            return "/NPCommon/ImgPaymentMethod/MIX/" + pmId + ".gif";
        }

        protected override void OnPreRender(EventArgs e)
        {
            var compareCountryIso = Data.pm_IssuerCountryIso; /*Data.IpCountry*/
            if ((compareCountryIso != null) && ddPaymentMethodCountry.ISOCode == compareCountryIso)
            {
                dvBankCountrySelect.Style.Add("display", "none");
                dvBankCountryFixed.Style.Add("display", "");
                ltIpCountry.Text = Country.Get(compareCountryIso).Name;
				imgBankCountryFlag.ImageUrl = getCountryImage(compareCountryIso);
            } else {
                dvBankCountrySelect.Style.Add("display", "");
                dvBankCountryFixed.Style.Add("display", "none");
                imgBankCountryFlag.ImageUrl = getCountryImage(null);
            }
            ltSelectedMethod.Visible = imgSelectedMethod.Visible = (irlPaymentMethod.SelectedItem != null);
            bool bEnableContinue = mvInsDebitForm.Visible && ((mvInsDebitForm.GetActiveView() == viewBankInfo) || (mvInsDebitForm.GetActiveView() == viewAccountDetails) || (mvInsDebitForm.GetActiveView() == viewAccountNameOnly));
            RaiseBubbleEvent(this, new ElementVisibleEventArgs(ElementVisibleEventArgs.ElementType.NextButton, bEnableContinue));
            base.OnPreRender(e);
        }

	}
}