<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ElectronicCheckForm.ascx.cs" Inherits="Netpay.PaymentPage.Controls.ElectronicCheckForm" %>
<%@ Register Src="~/Controls/BillingAddress.ascx" TagPrefix="Netpay" TagName="BillingAddress" %>
<%@ Register Src="~/Controls/ShippingAddress.ascx" TagPrefix="Netpay" TagName="ShippingAddress" %>
<%@ Register Src="~/Controls/PersonalDetails.ascx" TagPrefix="Netpay" TagName="PersonalDetails" %>

<table class="FormView" width="100%" border="0">
	<tr>
		<th colspan="3" style="padding-top:0!important; padding-bottom: 6px; ">
			<asp:Label ID="lblCardDetails" runat="server" Text="<%$Resources:CreditCardForm.ascx, lblCardDetails%>" />
		</th>
	</tr>
	<tr>
		<td width="50%">
			<asp:Literal Text="<%$ Resources:ElectronicCheckForm.ascx, lblAccountName %>" runat="server" /><span class="asterisk">*</span><br />
            <asp:TextBox ID="txtAccountName" MaxLength="50" runat="server"></asp:TextBox>
		</td>
		<td>
			<asp:Literal runat="server" Text="<%$ Resources:ElectronicCheckForm.ascx, lblAcoountNumber %>" /><span class="asterisk">*</span><br />
            <asp:TextBox ID="txtAccountNumber" CssClass="meduim" MaxLength="50" runat="server"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td>
			<asp:Literal runat="server" Text="<%$ Resources:ElectronicCheckForm.ascx, ltRoutingNumber %>" /> <span id="asterisk" runat="server" class="asterisk">*</span><br />
            <asp:TextBox ID="txtRoutingNumber" CssClass="meduim" runat="server"></asp:TextBox>
		</td>
	</tr>
</table>
<Netpay:BillingAddress runat="server" id="billingAddress" />
<Netpay:PersonalDetails runat="server" id="personalDetails" />
<Netpay:ShippingAddress runat="server" id="shippingAddress" />
