﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using Netpay.PaymentPage.Code;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Controls
{
	public partial class ElectronicCheckForm : ControlBase, IFormControl
	{
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            personalDetails.PersonalIDRequired = (Settings.IsEcheckRequiredID && (Data.trans_currency == "ILS"));
            personalDetails.EmailRequired = Settings.IsEcheckRequiredID;
            personalDetails.PhoneRequired = Settings.IsEcheckRequiredPhone;
            if (billingAddress != null) billingAddress.IsRequired = Data.RiskSettings.IsForceBillingAddress;
        }

        public void LoadData()
		{

			txtAccountName.Text = Data.client_fullName;
			if (Data.PaymentMethodType == PaymentMethodType.ECheck) 
			{
				txtAccountNumber.Text = Data.pm_value1;
				txtRoutingNumber.Text = Data.pm_value2;
			}

			if (billingAddress != null) billingAddress.LoadData();
			if (shippingAddress != null) shippingAddress.LoadData();
			personalDetails.LoadData();
        }

		public void SaveData()
		{
			Data.client_fullName = txtAccountName.Text;
			Data.PaymentMethod = PaymentMethodEnum.ECCheck;
			//var methodData = Data.Transaction.PaymentData.MethodInstance as Bll.PaymentMethods.BankAccount;
            //methodData.accountName = txtAccountName.Text;
            Data.pm_value1 = txtAccountNumber.Text;
            Data.pm_value2 = txtRoutingNumber.Text;
			//Data.CheckDetails.personalNumber = txtPersonalNum.Text;
			//Data.CheckDetails.birthDate = txtBirthDate.Date != null ? txtBirthDate.Date.Value.ToString("g") : "";
			if (billingAddress != null) billingAddress.SaveData();
			if (shippingAddress != null) shippingAddress.SaveData();
			personalDetails.SaveData();
        }

        public void Validate() 
        {
			if (billingAddress != null) billingAddress.Validate();
			if (shippingAddress != null) shippingAddress.Validate();
			Data.PaymentMethodType = PaymentMethodType.ECheck;
        }
	}
}