﻿using System;
using System.Web.UI;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.PaymentPage.Code;

namespace Netpay.PaymentPage.Controls
{
    public partial class ShippingAddress : ControlBase, IFormControl
    {
        public bool IsRequired { get; set; }
        
		protected override void OnLoad(EventArgs e)
        {
            // set Shipping address requirement by merchant settings
            vShippingAddress.Enabled = vShippingAddressCity.Enabled = vShippingAddressCountry.Enabled = vShippingAddressState.Enabled = vShippingAddressZipcode.Enabled = IsRequired;
            spanAddressRequired.Visible = spanCityRequired.Visible = spanZipCodeRequired.Visible = spanCountryRequired.Visible = IsRequired;
            if (ddShippingAddressCountry.SelectedCountry != null)
                spanStateRequired.Visible = vShippingAddressState.Enabled = (ddShippingAddressCountry.SelectedCountry.IsoCode2 == "US" || ddShippingAddressCountry.SelectedCountry.IsoCode2 == "CA");
			tblShippingAddress.Visible = Settings.IsShippingAddressRequire;
			//if (!IsPostBack) ShippingCountryChange(sender, e);
			base.OnLoad(e);
        }

		protected virtual void ShippingCountryChange(object sender, EventArgs e)
        {
            var country = ddShippingAddressCountry.SelectedCountry;
            if (country != null)
            {
				var selectedId = ddShippingAddressState.SelectedValue;
				ddShippingAddressState.ClearSelection();
                ddShippingAddressState.CountryID = country.ID;
                ddShippingAddressState.DataBind();
				try { ddShippingAddressState.SelectedValue = selectedId; } catch { }
				ddShippingAddressState.Enabled = vShippingAddressState.Enabled = spanStateRequired.Visible = (country.IsoCode2 == "US" || country.IsoCode2 == "CA");
            }
            else
                ddShippingAddressState.Enabled = vShippingAddressState.Enabled = spanStateRequired.Visible = false;
        }

        public void SaveData()
        {
			if (chkUseSameAsBilling.Checked) {
                Data.client_shipaddress1 = Data.client_billaddress1.NullIfEmpty();
                Data.client_shipaddress2 = Data.client_billaddress2.NullIfEmpty();
                Data.client_shipcity = Data.client_billcity.NullIfEmpty();
                Data.client_shipzipcode = Data.client_billzipcode.NullIfEmpty();
                Data.client_shipstate = Data.client_billstate.NullIfEmpty();
                Data.client_shipcountry = Data.client_billcountry.NullIfEmpty();
            } else {
				Data.client_shipaddress1 = txtShippingAddress.Text.NullIfEmpty();
                Data.client_shipaddress2 = txtShippingAddress2.Text.NullIfEmpty();
                Data.client_shipcity = txtShippingAddressCity.Text.NullIfEmpty();
                Data.client_shipzipcode = txtShippingAddressZipCode.Text.NullIfEmpty();
                Data.client_shipstate = ddShippingAddressState.ISOCode.NullIfEmpty();
                Data.client_shipcountry = ddShippingAddressCountry.ISOCode.NullIfEmpty();
            }
        }

        public void LoadData()
        {
			txtShippingAddress.Text = Data.client_shipaddress1;
			txtShippingAddress2.Text = Data.client_shipaddress2;
			txtShippingAddressCity.Text = Data.client_shipcity;
			txtShippingAddressZipCode.Text = Data.client_shipzipcode;
			ddShippingAddressCountry.ISOCode = Data.client_shipcountry;
			ddShippingAddressState.CountryIsoCode = Data.client_shipcountry;
			ddShippingAddressState.ISOCode = Data.client_shipstate;
        }

        public void Validate() 
        {
            vShippingAddress.Enabled = vShippingAddressCity.Enabled = vShippingAddressZipcode.Enabled = vShippingAddressCountry.Enabled = IsRequired;
            vShippingAddressState.Enabled = false;
            if (ddShippingAddressCountry.SelectedCountry != null)
            {
                if (ddShippingAddressCountry.SelectedCountry.IsoCode2 == "US" || ddShippingAddressCountry.SelectedCountry.IsoCode2 == "CA") {
                    vShippingAddressState.Enabled = true;
                    vShippingAddressState.IsValid = (ddShippingAddressState.SelectedValue.Trim() != "");
                }
            }
        }
    
    }
}