<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingAddress.ascx.cs" Inherits="Netpay.PaymentPage.Controls.BillingAddress" %>
<asp:RequiredFieldValidator ID="vBillingAddress" ControlToValidate="txtBillingAddress" Display="None" ErrorMessage="<%$Resources:BillingAddress.ascx, ValidatorAddress%>" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vBillingAddressCity" ControlToValidate="txtBillingAddressCity" Display="None" ErrorMessage="<%$Resources:BillingAddress.ascx, ValidatorCity%>" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vBillingAddressZipcode" ControlToValidate="txtBillingAddressZipCode" Display="None" ErrorMessage="<%$Resources:BillingAddress.ascx, ValidatorZipcode%>" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vBillingAddressCountry" ControlToValidate="ddBillingAddressCountry" Display="None" ErrorMessage="<%$Resources:BillingAddress.ascx, ValidatorCountry%>" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vBillingAddressState" ControlToValidate="ddBillingAddressState" Display="None" ErrorMessage="<%$Resources:BillingAddress.ascx, ValidatorState%>" ValidationGroup="PaymentForm" runat="server" />

<asp:PlaceHolder runat="server" id="tblBillingAddress">
    <table class="FormView" width="100%" border="0" >
	    <tr>
	     <td width="33%"></td><td width="33%"></td><td width="33%"></td>
	    </tr>
	    <tr>
		    <th colspan="3">
			    <asp:Label ID="ltBillingAddress" runat="server" Text="<%$Resources:BillingAddress.ascx,ltBillingAddress%>"  />
		    </th>
	    </tr>
	    <tr>
            <asp:PlaceHolder runat="server" ID="tdAddress1">
		        <td>
			        <asp:Literal runat="server" ID="ltAddress1" Text="<%$Resources:BillingAddress.ascx, ltAddress1%>" /><span id="spanAddressRequired" runat="server" class="asterisk">*</span>
			        <netpay:PageItemHelp runat="server" ID="pihelpAddress1" Text="<%$Resources:Main,TipAddressLine1%>" Width="300" Height="28" /><br />
			        <asp:TextBox runat="server" ID="txtBillingAddress" MaxLength="255" CssClass="InputField" />
		        </td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="tdCity">
		        <td>
			        <asp:Literal runat="server" ID="ltCity" Text="<%$Resources:BillingAddress.ascx, ltCity%>" /><span id="spanCityRequired" runat="server" class="asterisk">*</span><br />
			        <asp:TextBox runat="server" ID="txtBillingAddressCity" MaxLength="30" CssClass="InputField" />
    		    </td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="tdZipCode">
		        <td>
			        <asp:Literal runat="server" ID="ltZipCode" Text="<%$Resources:BillingAddress.ascx, ltZipCode%>" /><span id="spanZipCodeRequired" runat="server" class="asterisk">*</span><br />
			        <asp:TextBox runat="server" ID="txtBillingAddressZipCode" MaxLength="10" CssClass="InputField" />
		        </td>
            </asp:PlaceHolder>
	    </tr>
	    <tr>
            <asp:PlaceHolder runat="server" ID="tdAddress2">
		        <td>
			        <asp:Literal runat="server" Text="<%$Resources:BillingAddress.ascx, ltAddress2%>" ID="ltAddress2" />
			        <netpay:PageItemHelp runat="server" ID="pihelpAddress2" Text="<%$Resources:Main,TipAddressLine2%>" Width="300" Height="28" /><br />
			        <asp:TextBox runat="server" ID="txtBillingAddress2" MaxLength="255" CssClass="InputField" />
		        </td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="tdCountry">
		        <td>
			        <asp:Literal runat="server" Text="<%$Resources:BillingAddress.ascx, ltCountry%>" ID="ltCountry" /><span id="spanCountryRequired" runat="server" class="asterisk">*</span><br />
			        <netpay:CountryDropDown ID="ddBillingAddressCountry" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="BillingCountryChange" runat="server" CssClass="InputField"></netpay:CountryDropDown>
		        </td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="tdState">
		        <td>
			        <asp:Literal runat="server" Text="<%$Resources:BillingAddress.ascx, ltState%>" ID="ltState" /><span id="spanStateRequired" runat="server" class="asterisk">*</span><br />
			        <netpay:StateDropDown ID="ddBillingAddressState" StartEmpty="true" runat="server" CssClass="InputField" />
		        </td>
            </asp:PlaceHolder>
	    </tr>	
    </table>
</asp:PlaceHolder>
