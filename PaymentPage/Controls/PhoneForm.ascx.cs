﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Infrastructure;
using System.Text;
using Netpay.PaymentPage.Code;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Configuration;
using Netpay.Web;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Controls
{
    public partial class PhoneForm : ControlBase, IFormControl
	{
		protected override void OnInit(EventArgs e)
		{
            base.OnInit(e);
            if (!IsPostBack){
				//ddlOperator.DataSource = PhoneDebit.GetOperatorList(WebUtils.CurrentDomain.Host);
				ddlOperator.DataTextField = "Value";
				ddlOperator.DataMember = "Key";
				ddlOperator.DataBind();
			}
			tdOperator.Visible = ddlOperator.Items.Count > 0;
			billingAddress.IsRequired = false;
            personalDetails.PersonalIDRequired = false;
		}

        public void LoadData()
        {
			txtFullName.Text = Data.client_fullName;
			if (Data.PaymentMethodType == PaymentMethodType.PhoneDebit) {
				if (!string.IsNullOrEmpty(Data.pm_IssuerId)) ddlOperator.SelectedValue = Data.pm_IssuerId;
				else ddlOperator.SelectedValue = null;
				txtPhoneNumber.Text = Data.client_phoneNum;
			}
            billingAddress.LoadData();
			shippingAddress.LoadData();
			personalDetails.LoadData();
        }

        public void SaveData() 
		{
			Data.client_fullName = txtFullName.Text;
			Data.client_phoneNum = txtPhoneNumber.Text;

			Data.PaymentMethod = PaymentMethodEnum.MicroPaymentsIN;
			if (!string.IsNullOrEmpty(ddlOperator.SelectedValue)) Data.pm_IssuerId = ddlOperator.SelectedValue;
            billingAddress.SaveData();
			shippingAddress.SaveData();
			personalDetails.SaveData();
		}
        
        public void Validate() 
        {
            billingAddress.Validate();
			shippingAddress.Validate();
			Data.PaymentMethodType = PaymentMethodType.PhoneDebit;
        }
	}
}