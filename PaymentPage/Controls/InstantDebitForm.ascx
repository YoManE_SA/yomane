<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="InstantDebitForm.ascx.cs" Inherits="Netpay.PaymentPage.Controls.InstantDebitForm" %>
<%@ Register Src="~/Controls/BillingAddress.ascx" TagPrefix="Netpay" TagName="BillingAddress" %>
<%@ Register Src="~/Controls/ShippingAddress.ascx" TagPrefix="Netpay" TagName="ShippingAddress" %>
<%@ Register Src="~/Controls/PersonalDetails.ascx" TagPrefix="Netpay" TagName="PersonalDetails" %>
<%@ Register TagPrefix="LC" Namespace="Netpay.PaymentPage.Controls" Assembly="Netpay.PaymentPage" %>

<asp:RequiredFieldValidator ID="vPaymentMethodCountry" ControlToValidate="ddPaymentMethodCountry" ErrorMessage="<%$Resources:InstantDebitForm.ascx, CountryValidator%>" Display="None" ValidationGroup="PaymentForm" runat="server" />
<asp:RequiredFieldValidator ID="vAccountNameValidator" ControlToValidate="txtAccountName" runat="server" ErrorMessage="<%$Resources:InstantDebitForm.ascx, AccountNameValidator1%>" Display="None" ValidationGroup="PaymentForm" />
<asp:RegularExpressionValidator ID="vAccountNameValidator2" ValidationExpression="^[A-Za-z\xBF-\xFF]+[\s][A-Za-z\xBF-\xFF,'.]+$" ControlToValidate="txtAccountName" ErrorMessage="<%$Resources:InstantDebitForm.ascx, AccountNameValidator2%>" Display="None" ValidationGroup="PaymentForm" runat="server" />
<table class="FormView" width="100%" border="0">
	<tr>
		<th colspan="3" style="padding-top:0!important;">
			<asp:Label ID="lblPayMethod" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltPaymentMethod%>" />
		</th>
	</tr>
	<tr>
		<td width="66%" colspan="2">
            <div id="dvBankCountrySelect" runat="server" clientidmode="Static">
			    <div style="margin-bottom:6px;"><asp:Literal ID="ltCountry" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltSelectCountry%>" /><span id="ltCountryAsterisk" runat="server" class="asterisk">*</span></div>
			    <netpay:CountryDropDown ID="ddPaymentMethodCountry" OnSelectedIndexChanged="PaymentMethodCountryChange" AutoPostBack="true" runat="server"></netpay:CountryDropDown>
            </div>
            <div id="dvBankCountryFixed" runat="server" clientidmode="Static">
			    <div style="margin-bottom:6px;"><asp:Literal runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltFixedBankCountry%>" /></div>
                <span style="background-color:#F0F0F0; padding:3px;"><asp:Image runat="server" ID="imgBankCountryFlag" /> <asp:Literal runat="server" ID="ltIpCountry" /></span>
                &nbsp; [<asp:LinkButton runat="server" OnClientClick="document.getElementById('dvBankCountryFixed').style.display='none';document.getElementById('dvBankCountrySelect').style.display='';return false;" Text="Change" />]
            </div>
		</td>
		<td width="33%" style="text-align:center;">
			<asp:Image runat="server" ID="imgSelectedMethod" Height="30" Visible="false" ImageAlign="Top" /><br />
			<asp:Label runat="server" ID="ltSelectedMethod" />
		</td>
	</tr>
	<tr><td><br /></td></tr>

	<asp:MultiView runat="server" ID="mvInsDebitForm" ActiveViewIndex="-1" Visible="false" >
	 <asp:View runat="server" ID="viewSelectMethod">
		<tr>
			<td><asp:Label ID="ltSelectMethod" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltSelectMethod%>" /><br /></td>
		</tr>
		<tr>
			<td colspan="3">
				<LC:ImageRadioList runat="server" id="irlPaymentMethod" AutoPostBack="true" OnSelectedIndexChanged="PaymentMethodChange" RepeatLayout="Table" RepeatColumns="5" CssClass="ImageRadioList" RepeatDirection="Horizontal" ItemCssStyle="margin:10px;cursor:pointer;" /><br />
				<asp:Label ID="lbPMError" runat="server" Text="<%$ Resources:Main,ErrorNoMethodForCountry %>" />
			</td>
		</tr>
	 </asp:View>
	 <asp:View runat="server" ID="viewSelectBank">
		<tr>
			<td><asp:Label runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltSelectBank%>" /></td>
		</tr>
		<tr>
			<td colspan="3">
				<LC:ImageRadioList runat="server" id="irlBankIDList" AutoPostBack="true" OnSelectedIndexChanged="irlBankIDList_Changed" RepeatLayout="Table" RepeatColumns="5" CssClass="ImageRadioList" RepeatDirection="Horizontal" ItemCssStyle="margin:10px;cursor:pointer;" />
				<asp:Label ID="lbBankError" runat="server" Text="<%$ Resources:Main,ErrorNoMethodForCountry %>" />
			</td>
		</tr>
	 </asp:View>
	 <asp:View runat="server" ID="viewBankInfo">
	 <tr>
		<td valign="bottom">
			<asp:Image runat="server" ID="imgSelectedBank" Width="180" /><br />
			<asp:Label runat="server" ID="ltSelectedBank" />
		</td>
		<td>
			<asp:Literal runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltAccountName%>" /> <span class="asterisk">*</span><br />
			<asp:TextBox runat="server" ID="txtAccountName2" MaxLength="30" AutoCompleteType="None" autocomplete="off" />
		</td>
	 </tr>
	 </asp:View>
	 <asp:View runat="server" ID="viewAccountDetails">
	 <tr>
		 <th>
			<asp:Label ID="lblAccountDetails" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltAccountDetails%>" />
		 </th>
	 </tr>
	 <tr>
		<td>
			<asp:Literal ID="ltAccountName" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltAccountName%>" /> <span class="asterisk">*</span><br />
			<asp:TextBox runat="server" ID="txtAccountName" MaxLength="30" CssClass="InputField" AutoCompleteType="None" autocomplete="off" />
		</td>
		<td>
			<asp:Literal ID="ltRouting" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltRouting%>" /> <span class="asterisk">*</span><br />
			<asp:TextBox runat="server" ID="txtRouting" MaxLength="30" CssClass="InputField" AutoCompleteType="None" autocomplete="off" />
		</td>
		<td>
			<asp:Literal ID="ltAccount" runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltAccount%>" /> <span class="asterisk">*</span><br />
			<asp:TextBox runat="server" ID="txtAccountNumber" MaxLength="30" CssClass="InputField" AutoCompleteType="None" autocomplete="off" />
		</td>
	 </tr>
	 </asp:View>
	 <asp:View runat="server" ID="viewAccountNameOnly">
     <tr>
        <td>
            <asp:Literal runat="server" Text="<%$Resources:InstantDebitForm.ascx, ltFullName%>" /> <span class="asterisk">*</span><br />
            <asp:TextBox runat="server" ID="txtAccountName3" MaxLength="30" />
        </td>
     </tr>
     </asp:View>
	</asp:MultiView>
</table>
<Netpay:BillingAddress runat="server" ID="billingAddress" />
<Netpay:PersonalDetails runat="server" id="personalDetails" />
<Netpay:ShippingAddress runat="server" id="shippingAddress" />
