﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MerchantProduct.ascx.cs" Inherits="Netpay.PaymentPage.Controls.MerchantProduct" %>
<div class="shop">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="shop-inner">
                    <div class="shop-setting">
                        <div class="shop-veiw">
                            <p>View as:</p>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#th" aria-controls="th" data-toggle="tab"><i class="fa fa-th"></i></a></li>
                                <li role="presentation"><a href="#list" aria-controls="list" data-toggle="tab"><i class="fa fa-th-list"></i></a></li>
                            </ul>
                        </div>
                        <div class="shop-select hidden-xs">
                            <p>Show:</p>
                            <select class="form-control">
                                <option>9</option>
                                <option>12</option>
                                <option>15</option>
                                <option>18</option>
                            </select>
                        </div>
                        <div class="shop-select">
                            <p>Sort by:</p>
                            <select class="form-control">
                                <option>Default</option>
                                <option>Popularity</option>
                                <option>Rating</option>
                                <option>Newness</option>
                                <option>Price</option>
                            </select>
                        </div>
                    </div>
                    <div class="shop-product tab-content">
                        <div class="tab-pane fade active" id="th">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="single-product">
                                        <div class="product-img">

                                            <a href="javascript:;">
                                                <img src="../img/product/1.jpg" />
                                                <img class="product-disable" src="../img/product/2.jpg" />
                                            </a>
                                        </div>
                                        <div class="product-desc">
                                            <h4 class="product-name"><a href="#">Product Name</a></h4>
                                            <span class="product-price"><del>$80</del><span>$50</span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <div class="product-label">
                                                <span class="sale">Sale</span>
                                                <span class="new">New</span>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" data-toggle="tooltip" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                                <a href="#" data-toggle="modal" title="Quick View" data-target="#quick-view"><i class="fa fa-expand"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i></a>
                                            </div>
                                            <a href="single-product.html">
                                                <img src="img/product/3.jpg" />
                                                <img class="product-disable" src="img/product/4.jpg" />
                                            </a>
                                        </div>
                                        <div class="product-desc">
                                            <h4 class="product-name"><a href="#">Product Name</a></h4>
                                            <span class="product-price"><del>$90</del>$60</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <div class="product-label">
                                                <span class="sale">Sale</span>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" data-toggle="tooltip" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                                <a href="#" data-toggle="modal" title="Quick View" data-target="#quick-view"><i class="fa fa-expand"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i></a>
                                            </div>
                                            <a href="single-product.html">
                                                <img src="img/product/5.jpg" />
                                                <img class="product-disable" src="img/product/6.jpg" />
                                            </a>
                                        </div>
                                        <div class="product-desc">
                                            <h4 class="product-name"><a href="#">Product Name</a></h4>
                                            <span class="product-price"><del>$100</del>$90</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <div class="product-label">
                                                <span class="new">New</span>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" data-toggle="tooltip" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                                <a href="#" data-toggle="modal" title="Quick View" data-target="#quick-view"><i class="fa fa-expand"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i></a>
                                            </div>
                                            <a href="single-product.html">
                                                <img src="img/product/7.jpg" />
                                                <img class="product-disable" src="img/product/8.jpg" />
                                            </a>
                                        </div>
                                        <div class="product-desc">
                                            <h4 class="product-name"><a href="#">Product Name</a></h4>
                                            <span class="product-price"><del>$120</del>$80</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <div class="product-label">
                                                <span class="sale">Sale</span>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" data-toggle="tooltip" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                                <a href="#" data-toggle="modal" title="Quick View" data-target="#quick-view"><i class="fa fa-expand"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i></a>
                                            </div>
                                            <a href="single-product.html">
                                                <img src="img/product/9.jpg" />
                                                <img class="product-disable" src="img/product/10.jpg" />
                                            </a>
                                        </div>
                                        <div class="product-desc">
                                            <h4 class="product-name"><a href="#">Product Name</a></h4>
                                            <span class="product-price"><span>$80</span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <div class="product-label">
                                                <span class="sale">Sale</span>
                                            </div>
                                            <div class="product-action">
                                                <a href="#" data-toggle="tooltip" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                                <a href="#" data-toggle="modal" title="Quick View" data-target="#quick-view"><i class="fa fa-expand"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i></a>
                                            </div>
                                            <a href="single-product.html">
                                                <img src="img/product/11.jpg" />
                                                <img class="product-disable" src="img/product/12.jpg" />
                                            </a>
                                        </div>
                                        <div class="product-desc">
                                            <h4 class="product-name"><a href="#">Product Name</a></h4>
                                            <span class="product-price"><del>$70</del>$40</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="list">
                            <div class="list-product">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="product-img">
                                            <a href="single-product.html">
                                                <img src="img/product/10.jpg" />
                                                <img class="product-disable" src="img/product/9.jpg" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="product-details">
                                            <h4>Product Name</h4>
                                            <div class="product-rating">
                                                <span class="star-rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <a href="#">6 Review</a>
                                                    <a href="#">Add Yor Review</a>
                                                </span>
                                            </div>
                                            <span class="price"><del>$120</del> 100$</span>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
														sed diam<br />
                                                nonummy nibh euismod tincidunt ut laoreet dolore
														magna aliquam<br />
                                                erat volutpat. Ut wisi enim ad minim veniam,
														quis nostrud exerci<br />
                                                tation ullamcorper suscipit lobortis
														nisl ut aliquip ex ea commodo consequat.<br />
                                                Duis autem vel eum 
														iriure dolor in hendrerit in vulputate velit<br />
                                                esse molestie consequat,
														vel illum dolore eu feugiat nulla facilisis at<br />
                                                vero eros et accumsan
														et iusto odio dignissim qui blandit praesent
                                                <br />
                                                luptatum zzril delenit 
														augue duis dolore te feugait nulla facilisi.
                                            </p>
                                            <div class="product-action-details">
                                                <a href="#" data-toggle="tooltip" title="Add To Cart" class="addto">Add To Card</a>
                                                <a href="#" data-toggle="tooltip" title="Mail To"><i class="fa fa-envelope"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-product">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="product-img">
                                            <a href="single-product.html">
                                                <img src="img/product/8.jpg" />
                                                <img class="product-disable" src="img/product/7.jpg" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="product-details">
                                            <h4>Product Name</h4>
                                            <div class="product-rating">
                                                <span class="star-rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <a href="#">6 Review</a>
                                                    <a href="#">Add Yor Review</a>
                                                </span>
                                            </div>
                                            <span class="price">80$</span>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
														sed diam<br />
                                                nonummy nibh euismod tincidunt ut laoreet dolore
														magna aliquam<br />
                                                erat volutpat. Ut wisi enim ad minim veniam,
														quis nostrud exerci<br />
                                                tation ullamcorper suscipit lobortis
														nisl ut aliquip ex ea commodo consequat.<br />
                                                Duis autem vel eum 
														iriure dolor in hendrerit in vulputate velit<br />
                                                esse molestie consequat,
														vel illum dolore eu feugiat nulla facilisis at<br />
                                                vero eros et accumsan
														et iusto odio dignissim qui blandit praesent
                                                <br />
                                                luptatum zzril delenit 
														augue duis dolore te feugait nulla facilisi.
                                            </p>
                                            <div class="product-action-details">
                                                <a href="#" data-toggle="tooltip" title="Add To Cart" class="addto">Add To Card</a>
                                                <a href="#" data-toggle="tooltip" title="Mail To"><i class="fa fa-envelope"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-product">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="product-img">
                                            <a href="single-product.html">
                                                <img src="img/product/5.jpg" />
                                                <img class="product-disable" src="img/product/6.jpg" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="product-details">
                                            <h4>Product Name</h4>
                                            <div class="product-rating">
                                                <span class="star-rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <a href="#">6 Review</a>
                                                    <a href="#">Add Yor Review</a>
                                                </span>
                                            </div>
                                            <span class="price"><del>$100</del> 80$</span>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
														sed diam<br />
                                                nonummy nibh euismod tincidunt ut laoreet dolore
														magna aliquam<br />
                                                erat volutpat. Ut wisi enim ad minim veniam,
														quis nostrud exerci<br />
                                                tation ullamcorper suscipit lobortis
														nisl ut aliquip ex ea commodo consequat.<br />
                                                Duis autem vel eum 
														iriure dolor in hendrerit in vulputate velit<br />
                                                esse molestie consequat,
														vel illum dolore eu feugiat nulla facilisis at<br />
                                                vero eros et accumsan
														et iusto odio dignissim qui blandit praesent
                                                <br />
                                                luptatum zzril delenit 
														augue duis dolore te feugait nulla facilisi.
                                            </p>
                                            <div class="product-action-details">
                                                <a href="#" data-toggle="tooltip" title="Add To Cart" class="addto">Add To Card</a>
                                                <a href="#" data-toggle="tooltip" title="Mail To"><i class="fa fa-envelope"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-product">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="product-img">
                                            <a href="single-product.html">
                                                <img src="img/product/1.jpg" />
                                                <img class="product-disable" src="img/product/2.jpg" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="product-details">
                                            <h4>Product Name</h4>
                                            <div class="product-rating">
                                                <span class="star-rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <a href="#">6 Review</a>
                                                    <a href="#">Add Yor Review</a>
                                                </span>
                                            </div>
                                            <span class="price">60$</span>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
														sed diam<br />
                                                nonummy nibh euismod tincidunt ut laoreet dolore
														magna aliquam<br />
                                                erat volutpat. Ut wisi enim ad minim veniam,
														quis nostrud exerci<br />
                                                tation ullamcorper suscipit lobortis
														nisl ut aliquip ex ea commodo consequat.<br />
                                                Duis autem vel eum 
														iriure dolor in hendrerit in vulputate velit<br />
                                                esse molestie consequat,
														vel illum dolore eu feugiat nulla facilisis at<br />
                                                vero eros et accumsan
														et iusto odio dignissim qui blandit praesent
                                                <br />
                                                luptatum zzril delenit 
														augue duis dolore te feugait nulla facilisi.
                                            </p>
                                            <div class="product-action-details">
                                                <a href="#" data-toggle="tooltip" title="Add To Cart" class="addto">Add To Card</a>
                                                <a href="#" data-toggle="tooltip" title="Mail To"><i class="fa fa-envelope"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                                <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-refresh"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pagenation">
                        <ul>
                            <li class="active">
                                <a href="#">1</a>
                            </li>
                            <li>
                                <a href="#">2</a>
                            </li>
                            <li>
                                <a href="#">3</a>
                            </li>
                            <li class="next">
                                <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
