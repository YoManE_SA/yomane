<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="WebMoneyForm.ascx.cs" Inherits="Netpay.PaymentPage.Controls.WebMoneyForm" %>

<iframe name="wbframe" frameborder="0" style="width:100%; height:800px; border:1px solid dotted;" ></iframe>

<div style=" display:none;">
<input type="hidden" name="LMI_PAYMENT_AMOUNT" value="<%= Data.trans_amount %>" /><br />
<input type="hidden" name="LMI_PAYMENT_DESC" value="<%= Data.trans_comment %>" /><br />
<input type="hidden" name="LMI_PAYMENT_NO" value="123<%= Data.trans_refNum %>" /><br />
<input type="hidden" name="LMI_PAYEE_PURSE" value="<%= Settings.WebMoneyPurse %>" /><br />
<input type="hidden" name="LMI_MODE" value="1" /><br />
<input type="hidden" name="LMI_SIM_MODE" value="2" /><br />


<input type="hidden" name="LMI_SUCCESS_URL" value="<%= WebUtils.CurrentDomain.ProcessV2Url %>hosted/WebMoneyReplyRedirect.aspx?action=successRedirect&replyUrl=<%= Data.ReplyURL.ToEncodedUrl() %>&logId=<%= Data.LogPaymentPageID %>&IsFrame=1" /><br />
<input type="hidden" name="LMI_FAIL_URL" value="<%= WebUtils.CurrentDomain.ProcessV2Url %>hosted/WebMoneyReplyRedirect.aspx?action=failRedirect&replyUrl=<%= Data.ReplyURL.ToEncodedUrl() %>&logId=<%= Data.LogPaymentPageID %>&IsFrame=1" /><br />
<input type="hidden" name="LMI_RESULT_URL" value="<%= WebUtils.CurrentDomain.ProcessV2Url %>hosted/WebMoneyReplyRedirect.aspx?action=notify&notifyUrl=<%= Data.NotifyURL.ToEncodedUrl() %>&logId=<%= Data.LogPaymentPageID %>&IsFrame=1" /><br />
</div>
<%--<input type="button" value="send" onclick="doSubmit()" />--%>

<script type="text/javascript">
	function doSubmit() {
		var form = document.getElementById("aspnetForm");
		var vlastTraget = form.target;
		var vlastAction = form.action;
		form.action = 'https://merchant.wmtransfer.com/lmi/payment.asp';
		form.target = 'wbframe';
		form.submit();
		form.target = vlastTraget;
		form.action = vlastAction;
	}
	doSubmit();
</script>
