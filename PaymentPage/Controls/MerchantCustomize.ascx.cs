﻿using System;
using System.Web;
using Netpay.CommonTypes;
using Netpay.Bll;

namespace Netpay.PaymentPage.Controls
{
    public partial class MerchantCustomize : Code.ControlBase
    {
        public enum CustomizeSection { TopSection, BottomSection }
        public CustomizeSection Section { get; set; }
		public Netpay.Infrastructure.TranslationGroup Group { get; set; }


        protected override void OnPreRender(EventArgs e)
        {
			if (Data == null) return;
			var LanguageDic = Data.MerchantTextData(Web.WebUtils.CurrentLanguage, Group);
            if (LanguageDic != null) {
				string keyName = (Section == CustomizeSection.TopSection) ? "FrontTop" : "FrontBottom";
                if (LanguageDic.ContainsKey(keyName)) ltMerchantText.Text = LanguageDic[keyName];
                tblMerchantText.Visible = !string.IsNullOrEmpty(ltMerchantText.Text);
            }
            if (Data.MerchantCustomization != null)
            {
                (Page.Master as Netpay.PaymentPage.Hosted.Master).InitImage(Section == CustomizeSection.TopSection ? Data.MerchantCustomization.LogoTopLeft : Data.MerchantCustomization.LogoBottomLeft, LogoLeft, "Logo" + (Section == CustomizeSection.TopSection ? "Top" : "Bottom") + "Left");
                (Page.Master as Netpay.PaymentPage.Hosted.Master).InitImage(Section == CustomizeSection.TopSection ? Data.MerchantCustomization.LogoTopRight : Data.MerchantCustomization.LogoBottomRight, LogoRight, "Logo" + (Section == CustomizeSection.TopSection ? "Top" : "Bottom") + "Right");
            }
            base.OnPreRender(e);
        }
    }
}