﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using Netpay.PaymentPage.Code;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.CommonTypes;

namespace Netpay.PaymentPage.Controls
{
    public partial class CreditCardForm : ControlBase, IFormControl
	{
		protected override void OnInit(EventArgs e)
		{
            base.OnInit(e);
            spanCVVRequired.Visible = vCVV.Enabled = Settings.IsCreditCardRequiredCVV;
            if (billingAddress != null) billingAddress.IsRequired = BasePage.RiskSettings != null ? BasePage.RiskSettings.IsForceBillingAddress : false;
            if (personalDetails != null) { 
                personalDetails.PersonalIDRequired = (Settings.IsCreditCardRequiredID && (Data.trans_currency == "ILS"));
                personalDetails.EmailRequired = Settings.IsCreditCardRequiredEmail;
                personalDetails.PhoneRequired = Settings.IsCreditCardRequiredPhone;
            }
		}

        public void LoadData()
		{
            // setup card expiration year
            wcExpirationYear.YearFrom = DateTime.Now.Year;
            wcExpirationYear.YearTo = DateTime.Now.AddYears(20).Year;

			txtCardholderName.Text = Data.client_fullName;
			if (Data.PaymentMethodType == PaymentMethodType.CreditCard)
			{
                var expDate = Data.pm_expDate;
                if(expDate != null) { 
				    wcExpirationMonth.Value = expDate.Value.Month.ToString();
				    wcExpirationYear.SelectedYear = expDate.Value.Year;
                }
                wcCreditcard.Card = new Bll.PaymentMethods.CreditCard() { CardNumber = Data.pm_value1 };
				txtSecurityNumber.Text = Data.pm_value2;
			}
			if (billingAddress != null) billingAddress.LoadData();
			if (shippingAddress != null) shippingAddress.LoadData();
			personalDetails.LoadData();
		}

		public void SaveData()
		{
            Data.client_fullName = txtCardholderName.Text;
            var cardData = wcCreditcard.Card;
            Data.PaymentMethod = cardData.PaymentMethodId;
            Data.pm_value1 = cardData.CardNumber;
            Data.pm_value2 = txtSecurityNumber.Text;
            Data.pm_expDate = new DateTime(wcExpirationYear.Value.ToNullableInt().GetValueOrDefault(1901), wcExpirationMonth.Value.ToNullableInt().GetValueOrDefault(1), 1).AddMonths(1).AddSeconds(-1);
            if (billingAddress != null) billingAddress.SaveData();
			if (shippingAddress != null) shippingAddress.SaveData();
			personalDetails.SaveData();
		}

        public void Validate() 
        {
			if (billingAddress != null) billingAddress.Validate();
			if (shippingAddress != null) shippingAddress.Validate();
			Data.PaymentMethodType = PaymentMethodType.CreditCard;
        }

		protected void CvvValidation(object source, ServerValidateEventArgs args)
		{
			if (args.Value.Length < 3 || args.Value.Length > 8) 
			{
				args.IsValid = false;
				return;
			}

			long cvv;
			if (!long.TryParse(args.Value, out cvv)) 
			{
				args.IsValid = false;
				return;				
			}
		}

		protected void LuhnValidation(object source, ServerValidateEventArgs args)
		{
			args.IsValid = Netpay.Bll.PaymentMethods.CreditCard.Validate(args.Value.EmptyIfNull());
		}
	}
}