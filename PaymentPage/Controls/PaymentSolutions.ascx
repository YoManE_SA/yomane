<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentSolutions.ascx.cs" Inherits="Netpay.PaymentPage.Controls.PaymentSolutions" %>
<br />
<table width="100%" cellspacing="0">
	<asp:Repeater ID="rptSolutionGroups" runat="server" OnItemDataBound="rptSolutionGroups_OnSolutionGroups">
		<ItemTemplate>
			<tr id="trPaymentMethod" style="cursor:pointer;" runat="server" onmouseover="this.style.backgroundColor='#F5F5F5';" onmouseout="this.style.backgroundColor='';">
				<td width="180" valign="top">
					<asp:Image runat="server" ID="imgPayment" Height="44" ImageUrl='<%# "/NPCommon/ImgPaymentMethodGroups/98X44/" + Eval("ID") + ".png" %>' />
				</td>
				<td width="80%" class="preview-text">
					<span class="MainSelectTxtHead"><asp:Literal runat="server" ID="ltPMTitle" /></span><br />
					<span class="MainSelectTxtBody"><asp:Literal runat="server" ID="ltPMText" /></span><br />
				</td>
			</tr>
			<tr><td colspan="2" style="padding:8px 0;"><hr style="border: 1px dashed #e0e0e0;"></td>
			</tr>
		</ItemTemplate>
	</asp:Repeater>
	<asp:Repeater ID="rptSolutions" runat="server" OnItemDataBound="rptSolutions_OnItemDataBound">
		<ItemTemplate>
			<tr id="trPaymentMethod" style="cursor:pointer;" runat="server" onmouseover="this.style.backgroundColor='#F5F5F5';" onmouseout="this.style.backgroundColor='';">
				<td width="180" valign="top">
					<asp:Image runat="server" ID="imgPayment" Height="44" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/MIX" + Eval("ID") + ".png" %>' />
				</td>
				<td width="80%" class="preview-text">
					<span class="MainSelectTxtHead"><asp:Literal runat="server" ID="ltPMTitle" /></span><br />
					<span class="MainSelectTxtBody"><asp:Literal runat="server" ID="ltPMText" /></span><br />
				</td>
			</tr>
			<tr><td colspan="2" style="padding:8px 0;"><hr style="border: 1px dashed #e0e0e0;" size="0"></td>
			</tr>
		</ItemTemplate>
	</asp:Repeater>
</table>
