﻿using System;
using System.Web.UI;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.PaymentPage.Code;


namespace Netpay.PaymentPage.Controls
{
    public partial class PersonalDetails : ControlBase, IFormControl
    {
		public bool PhoneVisible { get; set; }
		public bool BirthDateVisible { get; set; }
		public PersonalDetails() { PhoneVisible = true; BirthDateVisible = true; }

		protected override void OnLoad(EventArgs e)
		{
            // show id fields if lang = hebrew
            tdCardholderID.Visible = Settings.IsShowPersonalID && (Data.trans_currency.EmptyIfNull() == "ILS"); //(WebUtils.CurrentLanguage == Language.Hebrew);

			tdPhone.Visible = Settings.IsShowPhone && PhoneVisible;
            tdEmail.Visible = Settings.IsShowEmail; 
			tdBirthDate.Visible = Settings.IsShowBirthDate;
            vRegEmail.Enabled = tdEmail.Visible;

            //test if params are missing
            if (EmailRequired && Data.client_email.EmptyIfNull().Trim().Length == 0 && !Settings.IsShowEmail)
                BasePage.RaiseError("Required email address is missing, Please consult the support at the referring site");
            if (PhoneRequired && Data.client_phoneNum.EmptyIfNull().Trim().Length == 0 && !Settings.IsShowPhone)
                BasePage.RaiseError("Required phone is missing, Please consult the support at the referring site");
            if (PersonalIDRequired && Data.client_idNum.EmptyIfNull().Trim().Length == 0 && !Settings.IsShowPersonalID)
                BasePage.RaiseError("Required personalID is missing, Please consult the support at the referring site");

            tblPersonalDetails.Visible = tdPhone.Visible || tdEmail.Visible || tdCardholderID.Visible || tdBirthDate.Visible;
        }

        public void LoadData() 
        {
			txtEmail.Text = Data.client_email;
			txtPhone.Text = Data.client_phoneNum;
			txtCardholderID.Text = Data.client_idNum;
			dpBirthDate.Date = Data.client_dateOfBirth;
        }

        public void SaveData()
        {
			Data.client_email = txtEmail.Text.NullIfEmpty(); ;
			Data.client_phoneNum = txtPhone.Text.NullIfEmpty(); ;
			Data.client_idNum = txtCardholderID.Text.NullIfEmpty(); ;
			Data.client_dateOfBirth = dpBirthDate.Date;
		}

        public void Validate(){}
        public bool EmailRequired { get { return vEMailValidator.Enabled; } set { spanEmailRequired.Visible = vEMailValidator.Enabled /*= vRegEmail.Enabled*/ = value; } }
        public bool PhoneRequired { get { return vPhoneValidator.Enabled; } set { spanPhoneRequired.Visible = vPhoneValidator.Enabled = value; } }
        public bool PersonalIDRequired { get { return vCardholderID.Enabled; } set { spanIDRequired.Visible = vCardholderID.Enabled = value; } }
    }
}