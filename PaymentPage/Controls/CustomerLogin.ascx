<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerLogin.ascx.cs" Inherits="Netpay.PaymentPage.Controls.Customer" %>

<asp:ValidationSummary ID="ValidationSummary1" EnableClientScript="false" runat="server" DisplayMode="SingleParagraph" ShowMessageBox="false" ShowSummary="true" ValidationGroup="loginControl" CssClass="ValidSummery" HeaderText="<%$Resources:Main, FieldError%>" />
<asp:CustomValidator runat="server" ID="ValidationWrongUser" ValidationGroup="loginControl" ErrorMessage="<%$ Resources:CustomerLogin.ascx, UserNotFound %>" Display="None" />

<asp:MultiView ID="mvWallet" runat="server">
    <asp:View runat="server">
        <table class="FormView" width="100%" border="0">
            <tr>
                <th colspan="2" style="padding-top: 0!important;">
                    <asp:Label ID="lblLoginDetails" runat="server" Text="<%$ Resources:CustomerLogin.ascx, lblLoginDetails %>" />
                </th>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="ltLoginName" runat="server" Text="<%$ Resources:CustomerLogin.ascx, ltLoginName %>" /><span class="asterisk">*</span><br />
                </td>
                <td>
                    <asp:TextBox ID="loginEmail" runat="server" Width="200" ValidationGroup="loginControl" CssClass="InputField" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="loginEmail"
                        ValidationGroup="loginControl" ErrorMessage="<%$ Resources:CustomerLogin.ascx, EmailAddressIsMissing %>" Display="None" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="loginEmail"
                        ValidationGroup="loginControl" ErrorMessage="<%$ Resources:CustomerLogin.ascx, EmailAddressIsInvalid %>" Display="None"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\w" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="ltPassword" runat="server" Text="<%$ Resources:CustomerLogin.ascx, ltPassword %>" /><span class="asterisk">*</span><br />
                </td>
                <td>
                    <asp:TextBox TextMode="Password" ID="loginPassword" Width="200" runat="server" ValidationGroup="loginControl" CssClass="InputField" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="loginPassword"
                        ValidationGroup="loginControl" ErrorMessage="<%$ Resources:CustomerLogin.ascx, PasswordIsMissing %>" Display="None" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="ltAppIdentity" runat="server" Text="<%$ Resources:CustomerLogin.ascx, ltAppIdentity %>" /></td>
                <td>
                    <asp:RadioButtonList RepeatDirection="Horizontal" runat="server" ID="ddlAppIdentity">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal runat="server" ID="ltRequired" Text="<%$ Resources:Main,RequireMsg %>" /></td>
                <td>
                    <asp:Button ID="btnLogin" OnClick="Login_Click" Width="100px" Text="<%$ Resources:Main,Login %>" runat="server" CssClass="Button" /></td>
            </tr>
        </table>
    </asp:View>
    <asp:View runat="server">
        <asp:Label ID="txtResponse" runat="server" ForeColor="Red" />
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="48%" style="vertical-align: top;">
                    <div class="mainHeading">
                        <asp:Literal ID="litSelectCard" runat="server" Text="<%$Resources:CustomerLogin.ascx, SelectCard%>" />
                    </div>
                    <br />
                    <asp:Repeater runat="server" ID="rptCardList">
                        <ItemTemplate>
                            <div>
                                <input type="radio" class="option" name="PayWith" value="<%# Eval("ID") %>" <%# ((int)Eval("ID") == Request["PayWith"].ToInt32(-1) ? "checked=\"checked\"" : string.Empty) %> />
                                <b>
                                    <netpay:PaymentMethodView ID="PaymentMethodView1" runat="server" MethodData='<%# Eval("MethodInstance") %>' />
                                </b>
                                <br />
                                &nbsp; &nbsp; &nbsp; &nbsp;
								<asp:Literal ID="Literal1" runat="server" Text="<%$Resources:CustomerLogin.ascx, ValidUntil%>" />: <%# Eval("MethodInstance.ExpirationDate", "{0:MM/yyyy}")%>
                                <br />
                                &nbsp; &nbsp; &nbsp; &nbsp;
								<asp:Literal ID="Literal2" runat="server" Text="<%$Resources:CustomerLogin.ascx, CardHolder%>" />: <%# Eval("OwnerName")%>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
                <td class="td-cell-text">
                    <asp:Literal runat="server" Text="Pin code" /><span class="asterisk">*</span>
                </td>
                <td>
                    <netpay:ActionNotify runat="server" ID="PinCodeActionNotify" />
                </td>
                <td class="td-cell">
                    <asp:TextBox TextMode="Password" ID="txtPinCode" Width="200" runat="server" />
                </td>
                <asp:Panel ID="Panel1" Visible="false" runat="server">
                    <td width="2%">&nbsp;</td>
                    <td width="2%" class="columnSeparator">&nbsp;</td>
                    <td width="48%" style="vertical-align: top;">
                        <div class="mainHeading">
                            <asp:Literal ID="litSelectBalance" runat="server" Text="<%$Resources:CustomerLogin.ascx, SelectBalance%>" />
                        </div>
                        <br />
                        <asp:Repeater ID="rptBalance" runat="server">
                            <ItemTemplate>
                                <div>
                                    <input type="radio" class="option" name="PayWith" value="<%# Eval("CurrencyIso") %>" <%# ((Container.DataItem as Netpay.Bll.Accounts.Balance).CurrencyIso == Request["PayWith"] ? "checked=\"checked\"" : string.Empty) %> />
                                    <b><%# Netpay.Bll.Currency.Get((Container.DataItem as Netpay.Bll.Accounts.Balance).CurrencyIso).Name%></b>
                                    <br />
                                    &nbsp; &nbsp; &nbsp; &nbsp;
									You have to pay: <%# (Data.Transaction.Amount * Data.Transaction.Currency.BaseRate / Netpay.Bll.Currency.Get((Container.DataItem as Netpay.Bll.Accounts.Balance).CurrencyIso).BaseRate).ToAmountFormat() + " " + (Container.DataItem as Netpay.Bll.Accounts.Balance).CurrencyIso %>
                                    <br />
                                    &nbsp; &nbsp; &nbsp; &nbsp;
									<asp:Literal ID="Literal3" runat="server" Text="<%$Resources:CustomerLogin.ascx, CurrentBalance%>" />:
									<%# (Container.DataItem as Netpay.Bll.Accounts.Balance).Total.ToAmountFormat()%> <%# Eval("CurrencyIso") %>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </asp:Panel>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <br />
                    <asp:Button UseSubmitBehavior="false" runat="server" Text="<%$Resources:CustomerLogin.ascx, ButtonPay%>" OnClick="Pay" CssClass="Button" />
                    <hr />
                    <asp:Literal ID="litWalletUser" runat="server" />
                    <asp:LinkButton OnClick="Logout_Click" Text="<%$ Resources:Main,Logout %>" runat="server" />
                </td>
            </tr>

        </table>
    </asp:View>
</asp:MultiView>
