﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Netpay.PaymentPage.Default" Codebehind="Default.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<style type="text/css">
		* { font-family:Arial; font-size:12px; }
		.PageHeading { font-size:16px; }
		input.wide { width:400px; }
		input.option { vertical-align:middle; }
		.SecHeading { font-weight:bold; }
	</style>
	<script type="text/javascript" language="JavaScript">
		function HideQuery() {

			document.getElementById('<%=txQuery.ClientID %>').value = "";
			document.getElementById('<%=txQuery.ClientID %>').style.display = "none";
			document.getElementById('<%=btnHideQuery.ClientID %>').style.display = "none";
		}

		function IsKeyDigit() {
			var nKey = event.keyCode;
			if ((nKey == 8) | (nKey == 9) | (nKey == 13) | (nKey == 35) | (nKey == 36) | (nKey == 37) | (nKey == 39) | (nKey == 46) | (nKey == 48) | (nKey == 49) | (nKey == 50) | (nKey == 51) | (nKey == 52) | (nKey == 53) | (nKey == 54) | (nKey == 55) | (nKey == 56) | (nKey == 57) | (nKey == 96) | (nKey == 97) | (nKey == 98) | (nKey == 99) | (nKey == 100) | (nKey == 101) | (nKey == 102) | (nKey == 103) | (nKey == 104) | (nKey == 105) | (nKey == 110) | (nKey == 190)) { return true; };
			return false;
		}

	</script>	
	<script type="text/javascript">
		function showSection(secName) {
			with (document.getElementById(secName)) {
				if (style.display != '')
					style.display = '';
				else
					style.display = 'none';

				document.getElementById(secName + 'Img').src = (style.display != '' ? '/NPCommon/Images/tree_expand.gif' : '/NPCommon/Images/tree_collapse.gif');
			}
		}
	</script>
</head>
<body>
   <form id="form1" runat="server" method="post">
	<center>
	<div style="width:500px;text-align:left;">
	<br /><br />
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td>
			<span class="PageHeading">HOSTED PAYMENT PAGE VER.2</span>
		</td>
	</tr>		
	<tr>
		<td>
			<br />
			The following form launches a window with the hosted payment page helping you to see and experience this method of payment.<br />
			
			<br /><asp:Label ID="lblError" runat="server" ForeColor="Red" style="font-weight: 700"></asp:Label><br />
		</td>
	</tr>
	<tr><td class="SecHeading">Request data<br /><br /></td></tr>
	<tr>
		<td valign="top">
			<table class="InfoTable" border="0" cellspacing="2" cellpadding="1">
			<tr>
				<td>Page URL<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="PageURL" /><br /></td>
			</tr>
			<tr>
				<td>merchantID<br /></td>
				<td>
					<asp:DropDownList runat="server" id="merchantID_Recent" AutoPostBack="True" OnSelectedIndexChanged="merchantID_Recent_SelectedIndexChanged" >
						<asp:ListItem Text="" />
						<asp:ListItem Text="4154588" Selected="True" />
						<asp:ListItem Text="3605100" />
						<asp:ListItem Text="5722306" />
					</asp:DropDownList>
					<asp:TextBox runat="server" id="merchantID" value="" /><br />
				</td>
			</tr>
			<tr>
				<td>url_redirect<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="url_redirect" /><br /></td>
			</tr>
			<tr>
				<td>url_notify<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="url_notify" /><br /></td>
			</tr>
			<tr>
				<td>url_redirect_seconds<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="url_redirect_seconds" /><br /></td>
			</tr>
			<tr><td><br /><br /></td></tr>
			<tr>
				<td>trans_comment<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="trans_comment" value="Testing hosted payment page" /><br /></td>
			</tr>
			<tr>
				<td valign="top">trans_installments<br /></td>
				<td>
					<asp:DropDownList runat="server" id="trans_installments">
						<asp:ListItem value="1" Text="1" />
						<asp:ListItem value="2" Text="2" />
						<asp:ListItem value="3" Text="3" />
					</asp:DropDownList>
				</td>
			</tr>
			<tr>
				<td>trans_recurring1<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="trans_recurring1" /><br /></td>
			</tr>
			<tr>
				<td>trans_recurring2<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="trans_recurring2" /><br /></td>
			</tr>
			<tr>
				<td>disp_recurring<br /></td>
				<td>
					<asp:DropDownList runat="server" id="disp_recurring" >
						<asp:ListItem Text="standard display" Value="0" />
						<asp:ListItem Text="hide last charge length" Value="1" />
					</asp:DropDownList>
					<br />
				</td>
			</tr>
			<tr>
				<td>trans_amount<br /></td>
				<td><input runat="server" onkeydown="return IsKeyDigit();" id="trans_amount" value="1.50" /><br /></td>
			</tr>
			<tr>
				<td>trans_currency<br /></td>
				<td><netpay:CurrencyDropDown ID="trans_currency" runat="server" EnableBlankSelection="false" /></td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td colspan="2" onclick="showSection('tdclientData')" style="cursor: pointer;">
					<img id="tdclientDataImg" src="/NPCommon/Images/tree_expand.gif" align="top" /><span class="SecHeading">Optional client Data</span><br />
				</td>
			</tr>
			<tr>
				<td colspan="2" style="display: none;" id="tdclientData">
					<table class="InfoTable" border="0" cellspacing="2" cellpadding="1">
					<tr>
						<td>client_fullName<br /></td>
						<td><asp:TextBox ID="client_fullName" class="wide" runat="server" /></td>
					</tr>
					<tr>
						<td>client_email<br /></td>
						<td><asp:TextBox ID="client_email" class="wide" runat="server" /></td>
					</tr>
					<tr>
						<td>client_phoneNum<br /></td>
						<td><asp:TextBox ID="client_phoneNum" class="wide" runat="server" /></td>
					</tr>
					<tr>
						<td>client_idNum<br /></td>
						<td><asp:TextBox ID="client_idNum" class="wide" runat="server" /></td>
					</tr>
					<tr><td><br /></td></tr>
					<tr>
						<td>client_billaddress1<br /></td>
						<td><asp:TextBox ID="client_billaddress1" class="wide" runat="server" /></td>
					</tr>
					<tr>
						<td>client_billaddress2<br /></td>
						<td><asp:TextBox ID="client_billaddress2" class="wide" runat="server" /></td>
					</tr>
					<tr>
						<td>client_billcity<br /></td>
						<td><asp:TextBox ID="client_billcity" class="wide" runat="server" /></td>
					</tr>
					<tr>
						<td>client_billzipcode<br /></td>
						<td><asp:TextBox ID="client_billzipcode" class="wide" runat="server" /></td>
					</tr>
					<tr>
						<td>client_billstate<br /></td>
						<td><asp:TextBox ID="client_billstate" class="wide" runat="server" /></td>
					</tr>
					<tr>
						<td>client_billcountry<br /></td>
						<td><asp:TextBox ID="client_billcountry" class="wide" runat="server" /></td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td><br /><br /></td></tr>
	<tr><td class="SecHeading">Display Options<br /><br /></td></tr>	
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>disp_payFor<br /></td>
				<td><asp:TextBox runat="server" class="wide" id="disp_payFor" value="Purchase" /><br /></td>
			</tr>
			<tr>
				<td valign="top">disp_paymentType<br /></td>
				<td>
				 <asp:CheckBox runat="server" id="PaymentMedhodCC" value="CC" />Credit Card &nbsp;
				 <asp:CheckBox runat="server" id="PaymentMedhodEC" value="EC" />ECheck &nbsp;
				 <asp:CheckBox runat="server" id="PaymentMedhodID" value="ID" />Instant Online Bank Transfer &nbsp;
				 <asp:CheckBox runat="server" id="PaymentMedhodDD" value="DD" />European Direct Debit &nbsp;
				 <asp:CheckBox runat="server" id="PaymentMedhodOB" value="OB" />Online Bank Transfer &nbsp;
				 <asp:CheckBox runat="server" id="PaymentMedhodWT" value="WT" />Wallet &nbsp;
				 <asp:CheckBox runat="server" id="PaymentMedhodPD" value="PD" />Phone &nbsp;
				 <asp:CheckBox runat="server" id="PaymentMedhodWM" value="WM" />Web Money &nbsp;
				 <asp:CheckBox runat="server" id="PaymentMedhodPP" value="PP" />Prepaid &nbsp;

				 Other: <asp:TextBox runat="server" id="PaymentMedhodOther" /> &nbsp;

				</td>
			</tr>
			<tr><td><br /><br /></td></tr>
			<tr>
				<td>disp_lng</td>
				<td>
				 <asp:RadioButtonList runat="server" ID="disp_lng" RepeatDirection="Horizontal">
				  <asp:ListItem Text="English" Value="en-us" Selected="True" />
				  <asp:ListItem Text="Hebrew" Value="he-il" />
				  <asp:ListItem Text="Italian" Value="it-it" />
				 </asp:RadioButtonList>
				</td>
			</tr>
			<tr>
				<td>disp_lngList</td>
				<td>
				 <asp:DropDownList runat="server" ID="ddlDisp_lngList">
				  <asp:ListItem Text="" Value=""/>
				  <asp:ListItem Text="all" Value="all"/>
				  <asp:ListItem Text="hide" Value="hide" />
				  <asp:ListItem Text="Language list" Value="list" />
				 </asp:DropDownList>
				 <asp:CheckBoxList runat="server" ID="cblDisp_lngList" RepeatDirection="Horizontal" RepeatLayout="Flow">
				  <asp:ListItem Text="English" Value="en-us" />
				  <asp:ListItem Text="Hebrew" Value="he-il" />
				  <asp:ListItem Text="Italian" Value="it-it" />
				 </asp:CheckBoxList>
				</td>
			</tr>
			<tr><td><br /><br /></td></tr>
			<tr>
				<td>skin_no</td>
				<td><asp:TextBox runat="server" ID="skin_no" /></td>
			</tr>
			<tr>
				<td>mobile version</td>
				<td>
					<asp:DropDownList runat="server" ID="ddldisp_mobile">
						<asp:ListItem Text="auto" />
						<asp:ListItem Text="false" Value="false" />
						<asp:ListItem Text="true" Value="true" />
					</asp:DropDownList>
				</td>
			</tr>
			<tr>
				<td>signature</td>
				<td>
					<asp:DropDownList runat="server" ID="ddlSignature">
						<asp:ListItem Text="" />
						<asp:ListItem Text="MD5" />
						<asp:ListItem Text="SHA256" Selected="True" />
					</asp:DropDownList>
					<asp:CheckBox runat="server" ID="cbSignatureV2" Checked="true" /> Signature V2
				</td>
			</tr>
			<tr><td><br /><br /></td></tr>
			<tr>
				<td colspan="2"><asp:CheckBox runat="server" ID="trans_storePm" />Store payment method</td>
			</tr>
			<tr>
				<td colspan="2"><asp:CheckBox runat="server" ID="client_AutoRegistration" />Auto wallet registration</td>
			</tr>
			<tr><td><br /><br /></td></tr>
			<tr>
				<td colspan="2">
					<asp:Button runat="server" ID="btnOpenPage" OnClick="btnOpenPageHosted_Click" UseSubmitBehavior="true" Text="Open Window" style="background-color:white;font-size:10px;width:100px;cursor:pointer;" />
					&nbsp;&nbsp;
					<asp:Button runat="server" ID="btnGenerate" OnClick="btnGenerateHosted_Click" Text="Generate Link" style="background-color:white;font-size:10px;width:100px;cursor:pointer;" />
					&nbsp;&nbsp;
					<asp:Button runat="server" ID="btnGenerateForm" OnClick="btnGenerateHostedForm_Click" Text="Generate Form" style="background-color:white;font-size:10px;width:100px;cursor:pointer;" />
					<br />
					<asp:Button runat="server" ID="btnOpenStorage" OnClick="btnOpenStorage_Click" UseSubmitBehavior="true" Text="Open Storage Window" style="background-color:white;font-size:10px;width:100px;cursor:pointer;" />
					&nbsp;&nbsp;
					<asp:Button runat="server" ID="btnGenerateStorage" OnClick="btnGenerateStorage_Click" Text="Generate Storage Link" style="background-color:white;font-size:10px;width:100px;cursor:pointer;" />
					&nbsp;&nbsp;
					<asp:Button runat="server" ID="btnOpenPublic" OnClick="btnOpenPublic_Click" Text="Open Public Page" style="background-color:white;font-size:10px;width:100px;cursor:pointer;" />
				</td>
			</tr>
			<tr><td><br /></td></tr>
			<tr>
				<td colspan="2">
					<div runat="server" id="txQuery" style="border:1px dashed #c0c0c0;background-color:#f7f7f7;display:none;overflow:auto;width:550px;height:115px;font:normal 12px Courier New, Monospace;word-wrap:break-word;" />
					<br />
					<asp:Button runat="server" id="btnHideQuery" OnClientClick="HideQuery();return false;" Text="Hide Link" style="display:none;background-color:white;font-size:10px;width:115px;cursor:pointer;" />
				</td>
			</tr>
			</table>
		</td>
	   </tr>
	  </table>
	</div>
  </center>
 </form>
 <asp:Literal ID="OpenScript" runat="server" Mode="PassThrough" ViewStateMode="Disabled" />
</body>
</html>
