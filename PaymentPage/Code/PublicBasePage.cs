﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.PaymentPage.Code
{
    public class PublicBasePage : HostedPageBase
    {
        protected override void InitHostedContext()
        {
            var req = HttpContext.Current.Request;
            if (req["merchantID"] != null) {
                string languageValue = string.IsNullOrEmpty(req["lng"]) ? null : "disp_lng=" + req["lng"];
                if(languageValue == null) languageValue = string.IsNullOrEmpty(req["disp_lng"]) ? null : "disp_lng=" + req["disp_lng"];
                if (req["Item"] != null) Init(IsPostBack ? null : "~/public/Product.aspx", false, ("Item=" + req["Item"] + "&" + languageValue).TrimEnd('&'), false);
                else Init(IsPostBack ? null : "~/public/", false, languageValue, false);
            }
            else base.InitHostedContext();
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            if (Data != null)
            {
                MasterPageFile = "~/public/public.master";
                Master.MasterPageFile = "~/Main.Master";
            }
        }
    }
}