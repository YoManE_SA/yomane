﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.PaymentPage.Code
{
    public class HostedParameters
    {
        protected NameValueCollection _requestValues = new NameValueCollection();
        public HostedParameters(string queryString, string queryForm)
        {
            string allData = queryString.EmptyIfNull();
            if (!string.IsNullOrEmpty(allData) && !string.IsNullOrEmpty(queryForm)) allData += "&";
            allData += queryForm.EmptyIfNull();
            _requestValues = HttpUtility.ParseQueryString(allData);
        }

        protected virtual string this[string key] { get { return _requestValues[key]; } set { _requestValues[key] = value; } }
        public string       signature { get { return this["signature"]; } }

        public int url_redirect_seconds
        {
            get
            {
                int countdown = this["url_redirect_seconds"].ToInt32(10);
                if (countdown < 1)
                    countdown = 1;
                if (countdown > 120)
                    countdown = 120;
                return countdown;
            }
        }

        public string       merchantID { get { return this["merchantID"]; } }
        public int?         skin_no { get { return this["skin_no"].ToNullableInt(); } }

        public bool?        disp_mobile { get { return this["disp_mobile"].ToNullableBool(); } }   //disp_mobile == auto || true
        public string       disp_lngList { get { return this["disp_lngList"]; } }
        public string       disp_lng    { get { return this["disp_lng"]; } }
        public string       disp_payFor { get { return this["disp_payFor"]; } }
        public bool?        disp_recurring { get { return this["disp_recurring"].EmptyIfNull() == "1"; } }
        public string       disp_paymentType { get { return this["disp_paymentType"]; } }

        public bool         client_autoRegistration { get { return this["client_autoRegistration"].EmptyIfNull() == "1"; } set { this["client_autoRegistration"] = value.ToString(); } }
        public string       client_fullName { get { return this["client_fullName"]; } set { this["client_fullName"] = value; } }
        public string       client_email { get { return this["client_email"]; } set { this["client_email"] = value; } }
        public string       client_phoneNum { get { return this["client_phoneNum"]; } set { this["client_phoneNum"] = value; } }
        public string       client_idNum { get { return this["client_idNum"]; } set { this["client_idNum"] = value; } }
        public DateTime?    client_dateOfBirth { get { return this["client_dateOfBirth"].ToNullableDate(); } set { this["client_dateOfBirth"] = value != null ? value.ToString() : null; } }

        public string       client_billaddress1 { get { return this["client_billaddress1"]; } set { this["client_billaddress1"] = value; } }
        public string       client_billaddress2 { get { return this["client_billaddress2"]; } set { this["client_billaddress2"] = value; } }
        public string       client_billcity { get { return this["client_billcity"]; } set { this["client_billcity"] = value; } }
        public string       client_billzipcode { get { return this["client_billzipcode"]; } set { this["client_billzipcode"] = value; } }
        public string       client_billcountry { get { return this["client_billcountry"]; } set { this["client_billcountry"] = value; } }
        public string       client_billstate { get { return this["client_billstate"]; } set { this["client_billstate"] = value; } }

        public string       client_shipaddress1 { get { return this["client_shipaddress1"]; } set { this["client_shipaddress1"] = value; } }
        public string       client_shipaddress2 { get { return this["client_shipaddress2"]; } set { this["client_shipaddress2"] = value; } }
        public string       client_shipcity { get { return this["client_shipcity"]; } set { this["client_shipcity"] = value; } }
        public string       client_shipzipcode { get { return this["client_shipzipcode"]; } set { this["client_shipzipcode"] = value; } }
        public string       client_shipcountry { get { return this["client_shipcountry"]; } set { this["client_shipcountry"] = value; } }
        public string       client_shipstate { get { return this["client_shipstate"]; } set { this["client_shipstate"] = value; } }

        public bool         trans_storePm { get { return this["trans_storePm"].EmptyIfNull() == "1"; } }
        public string       trans_currency { get { return this["trans_currency"]; } }
        public int          trans_installments { get { return this["trans_installments"].ToNullableInt().GetValueOrDefault(1); } }
        public TransactionType trans_type { get { return (TransactionType)this["trans_type"].ToNullableInt().GetValueOrDefault(); } }
        public decimal      trans_amount { get { return this["trans_amount"].ToNullableDecimal().GetValueOrDefault(); } } 
        public string       trans_refNum { get { return this["trans_refNum"]; } }
        public string       trans_comment { get { return this["trans_comment"]; } } 
        public int?         trans_recurringType { get { return this["trans_recurringType"].ToNullableInt(); } } //trans_recurringType

        public string       url_redirect { get { return this["url_redirect"]; } }
        public string       url_notify { get { return this["url_notify"]; } }

        public int?         public_quantity { get { return this["public_quantity"].ToNullableInt(); } }
        public int?         public_itemId { get { return this["public_itemId"].ToNullableInt(); } }
        public System.Guid? shop_cartId { get { return this["shop_cartId"].ToNullableGuid(); } }

        public string RecurringSeries
        {
            get {
                string ret = null;
                for (int i = 1; i < 100; i++)
                {
                    if (_requestValues["trans_recurring" + i] == null) break;
                    if (ret != null) ret += ";";
                    ret += _requestValues["trans_recurring" + i];
                }
                return ret;
            }
        }
    }
}