﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Netpay.PaymentPage.Code
{
	public class MasterPageBase : MasterPage
	{
		protected HostedPageBase BasePage { get { return (HostedPageBase)Page; } }
		public HostedContext Data { get { return HostedContext.Current; } }
        public string DesignPath { get { if (Data == null || string.IsNullOrEmpty(Data.DesignDir)) return string.Empty; return ResolveUrl(Data.DesignDir); } }

        public void RegisterCssFile(string fileName)
		{
			Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />", ResolveUrl(fileName))));
		}

    }
}