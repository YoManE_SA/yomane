﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Threading;
using Netpay.Bll;
using System.Net;
using System.IO;
using System.Globalization;
using System.Text;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using System.Collections.Specialized;
using System.Web.UI;

namespace Netpay.PaymentPage.Code
{
    public class ElementVisibleEventArgs : EventArgs
    {
        public enum ElementType
        {
            NextButton,
            MerchantBackButton,
            SelectMethod,
            TopCustomize,
            BottomCustomize,
            NetpayFooter,
        }
        public ElementType elementType;
        public bool Visible;
        public ElementVisibleEventArgs(ElementType elementType, bool visible) { this.elementType = elementType; this.Visible = visible; }
    }

    public class AppActionEventArgs : EventArgs
    {
        public enum AppAction { Next, Back }
        public AppAction Action;
        public AppActionEventArgs(AppAction action) { Action = action; }
    }

    public interface IFormControl
    {
        void SaveData();
        void LoadData();
        void Validate();
    }

    public enum RedirectWindow
    {
        SameWindow,
        UseFrames,
        UseIFrame,
        NewWindow
    }

    public class HostedPageBase : Netpay.Web.BasePage
    {
        public const string TokenValueName = "token";
        public event EventHandler BubbleEvent;

        protected override void OnPreInit(EventArgs e)
		{
            InitHostedContext();
            if (Data != null) {
                Data.IsMobile = Data.disp_mobile.GetValueOrDefault(IsMobile); 
                MasterPageFile = MapDesignPage(Data, "~/Hosted/Hosted.master");
			    Master.MasterPageFile = MapDesignPage(Data, "~/Main.Master");
            }
            base.OnPreInit(e);
		}

        protected override void InitSecurity()
        {
            base.InitSecurity();
            ViewStateUserKey = Request.UserAgent.EmptyIfNull() + (Data != null ? Data.LogPaymentPageID.ToString() : "");
        }

        protected override bool OnBubbleEvent(object source, EventArgs e)
        {
            if (BubbleEvent != null) BubbleEvent(source, e);
            return base.OnBubbleEvent(source, e);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            if (Data != null) Data.SaveState();
        }

        protected virtual void InitHostedContext()
        {
            string token = Request.QueryString[TokenValueName];
            if (!string.IsNullOrEmpty(token)) Load(token);
            if (HostedContext.Current == null)
                RaiseError(Resources.Main.ErrorSessionEnded);
        }

        public static bool LoadData(int logId) { return HostedContext.Load(logId, true) != null; }
        public static bool Load(string token)
        {
            if (string.IsNullOrEmpty(token)) return false;
            var values = token.Split(new char[] { '-' }, 2);
            if (values.Length > 0 && LoadData(values[0].ToNullableInt().GetValueOrDefault()))
            {
                if (HostedContext.Current.ContextToken != token) HostedContext.Current = null;
                else if (HostedContext.Current.LogPaymentPageID.HasValue)
                { //check CSFR
                    var value = WebUtils.GetGlobalParameter("HPP_" + HostedContext.Current.LogPaymentPageID);
                    if (value != HostedContext.Current.ContextHash) HostedContext.Current = null;
                }
            }
            return HostedContext.Current != null;
        }

        public void Init(string target, bool validateAmount, string parameters = null, bool saveContext = true) {
            HostedContext.Current = new HostedContext(HttpContext.Current, saveContext);
            try { HostedContext.Current.ValidateParameters(validateAmount); } catch(Exception ex) { RaiseError(ex.Message); return; }
            if (HostedContext.Current.LogPaymentPageID.HasValue) WebUtils.SetGlobalParameter("HPP_" + HostedContext.Current.LogPaymentPageID, HostedContext.Current.ContextHash); //set CSFR cookie
            if (target != null) Redirect(target, parameters);
            //HttpContext.Current.Response.End();
        }

        public HostedContext Data { get { return HostedContext.Current; } }
        public Bll.Merchants.Merchant Merchant { get { if (HostedContext.Current == null) return null; return HostedContext.Current.Merchant; } }
        public Bll.Merchants.RiskSettings RiskSettings { get { if (HostedContext.Current == null) return null; return HostedContext.Current.RiskSettings; } }
        public Bll.Merchants.Hosted.PaymentPage PaymentSettings { get { if (HostedContext.Current == null) return null; return HostedContext.Current.PaymentSettings; } }
        public Bll.Shop.Cart.MerchantSettings ShopSettings { get { if (HostedContext.Current == null) return null; return HostedContext.Current.ShopSettings; } }

        public virtual string LogoImage
        {
            get
            {
                if (Merchant != null && PaymentSettings != null)
                {
                    if (string.IsNullOrEmpty(PaymentSettings.LogoImage)) return null;
                    return Merchant.MapPublicVirtualPath(PaymentSettings.LogoImage);
                }
                return null;
            }
        }

        public static string MapDesignPage(HostedContext data, string fileName)
        {
            if (data == null) return fileName;
            if (!string.IsNullOrEmpty(data.DesignDir))
            {
                var pageName = fileName;
                if (pageName.StartsWith("~/")) pageName = pageName.Substring(2);
                string newFileName = data.DesignDir + pageName;
                if (System.IO.File.Exists(HttpContext.Current.Request.MapPath(newFileName))) return newFileName;
            }
            return fileName;
        }

        public static string Token {
            get {
                if (HostedContext.Current == null || !HostedContext.Current.HasToken) return null;
                return TokenValueName + "=" + HostedContext.Current.ContextToken.ToEncodedUrl();
            }
        }
        public static string AppendToken(string url)
        {
            string token = Token;
            if (string.IsNullOrEmpty(token)) return url;
            if (url.IndexOf('?') > -1) return string.Format("{0}&{1}", url, Token);
            return string.Format("{0}?{1}", url, Token);
        }

        public void Redirect(string target, string parameters = null, bool checkExisting = true)
        {
            if (parameters == null) parameters = "";
            if (HostedContext.Current != null && HostedContext.Current.HasToken) {
                HostedContext.Current.SaveState();
                if (!parameters.StartsWith("&")) parameters = "&" + parameters;
                parameters = "?" + Token + parameters;
            } else {
                if (HostedContext.Current != null) parameters = ("merchantId=" + HostedContext.Current.merchantID + "&" + parameters).TrimEnd('&');
                if (parameters != "") parameters = "?" + parameters;
            }
            var fullTarget = ResolveUrl(MapDesignPage(HostedContext.Current, target) + parameters);
            var isOnSamePage = checkExisting ? Request.RawUrl.EndsWith(fullTarget, StringComparison.InvariantCultureIgnoreCase) : false;
            if (!isOnSamePage) HttpContext.Current.Response.Redirect(fullTarget, true);
            //HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        public void RedirectClient(string reply, string replyDesc, int transID, DateTime date, string paymentDisplay, int storageID, string walletID, int recurringID)
		{
			string toAddress = Data.GetReplyParams(!string.IsNullOrEmpty(Data.url_redirect) ? Data.url_redirect : (PaymentSettings != null ? PaymentSettings.RedirectionUrl : null), reply, replyDesc, transID, date, paymentDisplay, storageID, walletID, recurringID);
			if (toAddress != null) Response.Redirect(toAddress, true);
		}

		public virtual void RaiseError(string errorMessage)
		{
			if (!Request.RawUrl.ToLower().Contains("error.aspx"))
                Redirect("~/Error.aspx", "message=" + errorMessage.Replace("<", "[").Replace(">", "]").ToEncodedUrl());
		}

        public void externalRedirect(string target, RedirectWindow window)
		{
			switch (window)
			{
				case RedirectWindow.UseFrames:
                    Redirect("~/Hosted/HostFrames.aspx", "Content=" + target.ToEncodedUrl());
					break;
				case RedirectWindow.UseIFrame:
                    Redirect("~/Hosted/HostIFrame.aspx", "Content=" + target.ToEncodedUrl());
					break;
                case RedirectWindow.NewWindow:
                    Redirect("~/Hosted/HostWindow.aspx", "Content=" + target.ToEncodedUrl());
                    break;
                case RedirectWindow.SameWindow:
					Response.Redirect(target);
					break;
                default:
                    break;
            }
            Response.End();
        }

        public static DateTime ConvertRCDateString(string dateValue)
        {
            DateTime pRet = new DateTime(1900, 1, 1, 0, 0, 1);
            DateTime.TryParse(dateValue, null, DateTimeStyles.AssumeLocal, out pRet);
            return pRet;
        }

		public string ValidateChargeReturn(System.Collections.Specialized.NameValueCollection responseValues, bool failRedirect)
		{
			int transactionNumber = responseValues["TransID"].ToInt32(0);
			string replyCode = responseValues["ReplyCode"] != null ? responseValues["ReplyCode"] : responseValues["Reply"];
			string replyDescription = responseValues["ReplyDesc"];
			string d3Redirect = responseValues["D3Redirect"];
			string transDate = responseValues["Date"];
			string methodDisplay = responseValues["CCType"] + "..." + responseValues["Last4"];
			if (methodDisplay == "...") methodDisplay = "";
			if (transDate == null) transDate = DateTime.Now.ToString();
			int storageID = responseValues["ccStorageID"].ToInt32(0);
			string walletID = responseValues["WalletID"];
			int recurringID = responseValues["recurringSeries"].ToInt32(0);

			var lpp = Data.LogPaymentPage;
			if (lpp != null)
			{
				lpp.ReplyCode = replyCode;
				if (!string.IsNullOrEmpty(replyDescription)) lpp.ReplyDesc = replyDescription;
				lpp.TransactionID = transactionNumber;
				lpp.DateEnd = DateTime.Now;
				lpp.Save();
			}

			if (replyCode == "000" || replyCode == "001")
			{
                Data.SendClientReply(replyCode, replyDescription, transactionNumber, ConvertRCDateString(transDate), methodDisplay, storageID, walletID, recurringID);
				Redirect("~/Hosted/PaymentMsg.aspx", "trans_id=" + transactionNumber + "&replyCode=" + HttpUtility.UrlEncode(replyCode) + "&replyDesc=" + HttpUtility.UrlEncode(replyDescription) + "&trans_date=" + HttpUtility.UrlEncode(transDate) + "&paymentDisplay=" + HttpUtility.UrlEncode(methodDisplay) + "&storage_id=" + storageID + "&client_Wallet_id=" + walletID + "&recurringSeries=" + recurringID);
			}
			else if (replyCode == "552")
			{
                Redirect("~/Hosted/PaymentPagePinCode.aspx", "trans_id=" + transactionNumber + "&trans_currency=" + Data.trans_currency);
			}
			else if (replyCode == "553")
			{
                RedirectWindow redirectWindow = RedirectWindow.UseFrames;
                if (responseValues["D3RedirectMethod"] != null) Enum.TryParse(responseValues["D3RedirectMethod"], out redirectWindow);
                externalRedirect(d3Redirect, redirectWindow);
			}
			else if (replyCode == "554")
			{
                Redirect("~/Hosted/PushTransferInfo.aspx", "trans_id=" + transactionNumber + "&IsTest=" + responseValues["IsTest"]);
			}
			else if (replyCode == "556")
			{
				if (Data.PaymentMethod == PaymentMethodEnum.MicroPaymentsIN)
                    Redirect("~/Hosted/PaymentSendSMS.aspx", "trans_id=" + transactionNumber + "&SMSInstruction=" + HttpUtility.UrlEncode(responseValues["SMSInstruction"]));
                else Redirect("~/Hosted/PaymentIVRCode.aspx", "trans_id=" + transactionNumber + "&SMSInstruction=" + HttpUtility.UrlEncode(responseValues["SMSInstruction"]));
			}
			else
			{
                Data.SendClientReply(replyCode, replyDescription, transactionNumber, ConvertRCDateString(transDate), methodDisplay, storageID, walletID, recurringID);
                if (failRedirect) Redirect("~/Hosted/PaymentMsg.aspx", "trans_id=" + transactionNumber + "&replyCode=" + HttpUtility.UrlEncode(replyCode) + "&replyDesc=" + HttpUtility.UrlEncode(replyDescription) + "&trans_date=" + HttpUtility.UrlEncode(transDate) + "&paymentDisplay=" + HttpUtility.UrlEncode(methodDisplay) + "&storage_id=" + storageID);
				return string.Format("Error ({0}): {1}", replyCode, replyDescription);
			}
			return null;
		}

		public string ValidateExternalReturn(string transID, string methodDisplay, string replyCode, string replyDescription, bool failRedirect, RedirectWindow window)
		{
			var lpp = Data.LogPaymentPage;
			if (lpp != null) {
				lpp.ReplyCode = replyCode;
				if (!string.IsNullOrEmpty(replyDescription)) lpp.ReplyDesc = replyDescription;
				lpp.TransactionID = -1;
				lpp.DateEnd = DateTime.Now;
				lpp.Save();
			}
			DateTime transDate = DateTime.Now;
            Data.SendClientReply(replyCode, replyDescription, transID.ToInt32(), transDate, methodDisplay, 0, "", 0);
            if (replyCode == "000" || replyCode == "001") {
                Redirect("~/Hosted/PaymentMsg.aspx", "trans_id=" + transID + "&replyCode=" + HttpUtility.UrlEncode(replyCode) + "&replyDesc=" + HttpUtility.UrlEncode(replyDescription) + "&trans_date=" + HttpUtility.UrlEncode(transDate.ToString()) + "&paymentDisplay=" + HttpUtility.UrlEncode(methodDisplay) + "&storage_id=" + 0);
			} else {
                if (failRedirect) Redirect("~/Hosted/PaymentMsg.aspx", "trans_id=" + transID + "&replyCode=" + HttpUtility.UrlEncode(replyCode) + "&replyDesc=" + HttpUtility.UrlEncode(replyDescription) + "&trans_date=" + HttpUtility.UrlEncode(transDate.ToString()) + "&paymentDisplay=" + HttpUtility.UrlEncode(methodDisplay) + "&storage_id=" + 0);
				return string.Format("Error ({0}): {1}", replyCode, replyDescription);
			}
			return null;
		}

	}
}
