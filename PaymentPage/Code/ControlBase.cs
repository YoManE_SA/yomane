﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.PaymentPage.Code
{
	public class ControlBase : System.Web.UI.UserControl
	{
		protected HostedPageBase BasePage { get { return Page as HostedPageBase; } }
        protected HostedContext Data { get { return HostedContext.Current; } }
        protected Bll.Merchants.Hosted.PaymentPage Settings { get { return (Data != null) ? Data.PaymentSettings : null; } }

	}
}
