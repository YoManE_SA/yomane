﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Bll;
using Netpay.Infrastructure;
using Netpay.Infrastructure.VO;
using Netpay.Web;

namespace Netpay.PaymentPage.Code
{
	public class CartPageBase : PageBase
	{
		protected Bll.Shop.MerchantSettings _options = null;

		public Bll.Shop.Cart.Basket Cart
		{
			get
			{
				Bll.Shop.Cart.Basket cart = null;
				if (Session["Cart"] is Bll.Shop.Cart.Basket)
					return Session["Cart"] as Bll.Shop.Cart.Basket;
				if (Request.Cookies["CartID"] != null && !string.IsNullOrEmpty(Request.Cookies["CartID"].Value))
				{
					cart = Bll.Shop.Cart.Basket.Load(WebUtils.CurrentDomain.Host, new Guid(Request.Cookies["CartID"].Value));
					if (cart != null)
						return (Session["Cart"] = cart) as Bll.Shop.Cart.Basket;
				}
				cart = new Bll.Shop.Cart.Basket(WebUtils.CurrentDomain.Host, Merchant.ID, null);
				Session["Cart"] = cart;
				//, null, Request["currency"].ToInt32(0)
				HttpCookie basketCookie = new HttpCookie("CartID", cart.ID.ToString());
				//basketCookie.Expires = no need, default is session
				Response.SetCookie(basketCookie);
				
				return cart;
			}
		}

		protected void ClearCart()
		{
			Session["Cart"] = null;
			Response.SetCookie(new HttpCookie("CartID", null));
		}

		public Bll.Shop.MerchantSettings Options
		{
			get
			{
				if (_options != null) 
					return _options;

				return (_options = Netpay.Bll.Shop.MerchantSettings.Load(WebUtils.CurrentDomain.Host, Merchant.ID));
			}
		}

		protected void LoadMerchant()
		{
			if (Request["merchantID"] != null) 
				GetMerchant(Request["merchantID"]);
			else 
				Merchant = Bll.Merchant.Merchant.Load(WebUtils.DomainHost, Cart.MerchantID);
		}

		protected override void OnInit(EventArgs e)
		{
			LoadMerchant();
			base.OnInit(e);
		}
	}
}