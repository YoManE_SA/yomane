﻿using System;
using System.Text;
using System.Web;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Web;

namespace Netpay.PaymentPage.Code
{
    public static class PaymentRequest
    {
        private static string getRecurringParams(HostedRequestData Data)
        {
            string retParams = string.Empty;
            if (string.IsNullOrEmpty(Data.RecurringSeries)) return retParams;
            string[] recurringParams = Data.RecurringSeries.Split(';');
            for (int i = 0; i < recurringParams.Length; i++)
                retParams += "&Recurring" + (i + 1) + "=" + HttpUtility.UrlEncode(recurringParams[i]);
            retParams += "&RecurringTransType=" + Data.RecurringType;
            return retParams;
        }

        public static string CreatePaymentRequestString(HostedRequestData Data, string retUrl, string clientIp)
        {
			var transaction = new Bll.Transactions.Transaction();
			transaction.MerchantID = Data.Settings.MerchantID;


			StringBuilder requestBuilder = new StringBuilder();
			string encValue1, encValue2;
			Data.MethodData.MethodInstance.GetDecryptedData(out encValue1, out encValue2);
			switch(Data.PaymentMethodType){
			case PaymentMethodType.CreditCard:
                requestBuilder.Append(WebUtils.CurrentDomain.ProcessUrl + "remote_charge2.aspx");
                requestBuilder.Append("?CompanyNum=" + Data.MerchantNum.ToEncodedUrl());
				requestBuilder.Append("&TypeCredit=" + (int)(Data.Payments > 1 ? CreditType.Installments : CreditType.Regular));
				requestBuilder.Append("&TransType=" + (int)Data.TransType);
				requestBuilder.Append("&CardNum=" + encValue1.ToEncodedUrl());
				requestBuilder.Append("&ExpMonth=" + Data.MethodData.MethodInstance.ExpirationDate.GetValueOrDefault().Month);
				requestBuilder.Append("&ExpYear=" + Data.MethodData.MethodInstance.ExpirationDate.GetValueOrDefault().Year);
				requestBuilder.Append("&CVV2=" + encValue2.ToEncodedUrl());
				requestBuilder.Append("&Currency=" + Data.Currency.ID.ToString());
				requestBuilder.Append("&Payments=" + Data.Payments);
                requestBuilder.Append("&ClientIP=" + clientIp.ToEncodedUrl());
				requestBuilder.Append("&Order=" + Data.RefNumber.ToEncodedUrl());
				requestBuilder.Append("&Amount=" + Data.Amount.ToString("0.00"));
				requestBuilder.Append("&PayFor=" + Data.PayFor.ToEncodedUrl());
				if (Data.PPItemID != null) requestBuilder.Append("&MerchantProductId=" + Data.PPItemID.Value);
				if (Data.CartID != null) requestBuilder.Append("&MerchantCartID=" + Data.CartID.Value.ToString().ToEncodedUrl());
                requestBuilder.Append("&PhoneNumber=" + Data.Payer.PhoneNumber.ToEncodedUrl());
                requestBuilder.Append("&Email=" + Data.Payer.EmailAddress.ToEncodedUrl());
                requestBuilder.Append("&PersonalNum=" + Data.Payer.PersonalNumber.ToEncodedUrl());
                requestBuilder.Append("&Member=" + Data.Payer.FullName.ToEncodedUrl());
				requestBuilder.Append("&BillingAddress1=" + Data.Address.AddressLine1.ToEncodedUrl());
				requestBuilder.Append("&BillingAddress2=" + Data.Address.AddressLine2.ToEncodedUrl());
				requestBuilder.Append("&BillingCity=" + Data.Address.City.ToEncodedUrl());
				requestBuilder.Append("&BillingZipCode=" + Data.Address.PostalCode.ToEncodedUrl());
				if (!string.IsNullOrEmpty(Data.Address.StateISOCode)) requestBuilder.Append("&BillingState=" + Data.Address.StateISOCode);
				if (!string.IsNullOrEmpty(Data.Address.CountryISOCode)) requestBuilder.Append("&BillingCountry=" + Data.Address.CountryISOCode);

				requestBuilder.Append("&requestSource=" + (WebUtils.CurrentLanguage == Language.Hebrew ? (int)TransactionSource.HostedPageV2He : (int)TransactionSource.HostedPageV2En));
				requestBuilder.Append("&StoreCc=" + (Data.PMStore ? "1" : "0"));
				requestBuilder.Append("&AutoWalletRegistration=" + (Data.ClientAutoRegistration ? "1" : "0"));
				requestBuilder.Append("&RetURL=" + retUrl);
                requestBuilder.Append(getRecurringParams(Data));
				//requestBuilder.Append("&Comment=" + Data.Comment.ToEncodedUrl());
				break;
			case PaymentMethodType.ECheck:
			case PaymentMethodType.BankTransfer:
				requestBuilder.Append(WebUtils.CurrentDomain.ProcessUrl + "remoteCharge_echeck.aspx");
                requestBuilder.Append("?CompanyNum=" + Data.MerchantNum.ToEncodedUrl());
				requestBuilder.Append("&CreditType=" + (int)CreditType.Regular);
                requestBuilder.Append("&AccountName=" + Data.Payer.FullName.ToEncodedUrl());
				requestBuilder.Append("&AccountNumber=" + encValue1.ToEncodedUrl());
				requestBuilder.Append("&RoutingNumber=" + encValue2.ToEncodedUrl());
				//requestBuilder.Append("&BankID=" + Data.MethodData.bankID);
				requestBuilder.Append("&Currency=" + Data.Currency.ID.ToString());
                requestBuilder.Append("&ClientIP=" + clientIp.ToEncodedUrl());
				requestBuilder.Append("&PaymentMethod=" + (int)Data.MethodData.MethodInstance.PaymentMethodId);
				requestBuilder.Append("&Order=" + Data.RefNumber.ToEncodedUrl());
				requestBuilder.Append("&Amount=" + Data.Amount.ToString("0.00"));
				requestBuilder.Append("&PayFor=" + Data.PayFor.ToEncodedUrl());
				if (Data.PPItemID != null) requestBuilder.Append("&MerchantProductId=" + Data.PPItemID.Value);
				if (Data.CartID != null) requestBuilder.Append("&MerchantCartID=" + Data.CartID.Value.ToString().ToEncodedUrl());
				requestBuilder.Append("&PhoneNumber=" + Data.Payer.PhoneNumber.ToEncodedUrl());
                requestBuilder.Append("&Email=" + Data.Payer.EmailAddress.ToEncodedUrl());
                requestBuilder.Append("&PersonalNum=" + Data.Payer.PersonalNumber.ToEncodedUrl());
				requestBuilder.Append("&BillingAddress1=" + Data.Address.AddressLine1.ToEncodedUrl());
				requestBuilder.Append("&BillingAddress2=" + Data.Address.AddressLine2.ToEncodedUrl());
				requestBuilder.Append("&BillingCity=" + Data.Address.City.ToEncodedUrl());
				requestBuilder.Append("&BillingZipCode=" + Data.Address.PostalCode.ToEncodedUrl());
				if (!string.IsNullOrEmpty(Data.Address.StateISOCode)) requestBuilder.Append("&BillingState=" + Data.Address.StateISOCode);
				if (!string.IsNullOrEmpty(Data.Address.CountryISOCode)) requestBuilder.Append("&BillingCountry=" + Data.Address.CountryISOCode);
				requestBuilder.Append("&requestSource=" + (WebUtils.CurrentLanguage == Language.Hebrew ? (int)TransactionSource.HostedPageV2He : (int)TransactionSource.HostedPageV2En));
				requestBuilder.Append("&AutoWalletRegistration=" + (Data.ClientAutoRegistration ? "1" : "0"));
				requestBuilder.Append("&RetURL=" + retUrl);
                requestBuilder.Append(getRecurringParams(Data));
				requestBuilder.Append("&Comment=" + Data.Comment.ToEncodedUrl());
				break;
			case PaymentMethodType.PhoneDebit:
				//if (Data.PhoneDetails.MessageDirection == PhoneMessageDirection.UserToSystem)
				//	Response.Redirect("PaymentSendSMS.aspx");
                requestBuilder.Append(WebUtils.CurrentDomain.ProcessUrl + "remoteCharge_Phone.asp");
				requestBuilder.Append("?CompanyNum=" + Data.MerchantNum.ToEncodedUrl());
				requestBuilder.Append("&PaymentMethod=" + (int)Data.PaymentMethod);
				requestBuilder.Append("&CreditType=" + (int)CreditType.Regular);
				requestBuilder.Append("&Currency=" + Data.Currency.ID.ToString());
                requestBuilder.Append("&ClientIP=" + clientIp.ToEncodedUrl());
				requestBuilder.Append("&Amount=" + Data.Amount.ToString("0.00"));
				requestBuilder.Append("&PayFor=" + Data.PayFor.ToEncodedUrl());
				if (Data.PPItemID != null) requestBuilder.Append("&MerchantProductId=" + Data.PPItemID.Value);
				if (Data.CartID != null) requestBuilder.Append("&MerchantCartID=" + Data.CartID.Value.ToString().ToEncodedUrl());
				requestBuilder.Append("&PhoneNumber=" + Data.Payer.PhoneNumber.ToEncodedUrl());
				requestBuilder.Append("&Order=" + Data.RefNumber.ToEncodedUrl());
				//requestBuilder.Append("&BillingType=" + (int)Data.PhoneDetails.MessageDirection);
                requestBuilder.Append("&FullName=" + Data.Payer.FullName.ToEncodedUrl());
				requestBuilder.Append("&Email=" + Data.Payer.EmailAddress.ToEncodedUrl());
                requestBuilder.Append("&PersonalNum=" + Data.Payer.PersonalNumber.ToEncodedUrl());
				requestBuilder.Append("&BillingAddress1=" + Data.Address.AddressLine1.ToEncodedUrl());
				requestBuilder.Append("&BillingAddress2=" + Data.Address.AddressLine2.ToEncodedUrl());
				requestBuilder.Append("&BillingCity=" + Data.Address.City.ToEncodedUrl());
				requestBuilder.Append("&BillingZipCode=" + Data.Address.PostalCode.ToEncodedUrl());
				if (!string.IsNullOrEmpty(Data.Address.StateISOCode)) requestBuilder.Append("&BillingState=" + Data.Address.StateISOCode);
				if (!string.IsNullOrEmpty(Data.Address.CountryISOCode)) requestBuilder.Append("&BillingCountry=" + Data.Address.CountryISOCode);
				requestBuilder.Append("&requestSource=" + (WebUtils.CurrentLanguage == Language.Hebrew ? (int)TransactionSource.HostedPageV2He : (int)TransactionSource.HostedPageV2En));
				requestBuilder.Append("&AutoWalletRegistration=" + (Data.ClientAutoRegistration ? "1" : "0"));
				requestBuilder.Append("&RetURL=" + retUrl);
                requestBuilder.Append(getRecurringParams(Data));
				requestBuilder.Append("&Comment=" + Data.Comment.ToEncodedUrl());
				//Response.Write(requestBuilder.ToString()); Response.End();
				break;
			default:
                throw new ApplicationException("Unknown Payment Method");
			}

			if (Data.ShippingAddress != null){
				requestBuilder.Append("&Shipping_Address1=" + Data.ShippingAddress.AddressLine1.ToEncodedUrl());
				requestBuilder.Append("&Shipping_Address2=" + Data.ShippingAddress.AddressLine2.ToEncodedUrl());
				requestBuilder.Append("&Shipping_City=" + Data.ShippingAddress.City.ToEncodedUrl());
				requestBuilder.Append("&Shipping_PostalCode=" + Data.ShippingAddress.PostalCode.ToEncodedUrl());
				requestBuilder.Append("&Shipping_StateIso=" + Data.ShippingAddress.StateISOCode.ToEncodedUrl());
				requestBuilder.Append("&Shipping_CountryIso=" + Data.ShippingAddress.CountryISOCode.ToEncodedUrl());
			}

            return requestBuilder.ToString();
        }

        public static string CreateStorageRequestString(HostedRequestData Data, string merchantHash, string clientIp)
        {
            StringBuilder sb = new StringBuilder();
            System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("windows-1255");
			string encValue1, encValue2;
			Data.MethodData.MethodInstance.GetDecryptedData(out encValue1, out encValue2);
			switch (Data.PaymentMethodType)
			{
			case PaymentMethodType.CreditCard:
                sb.Append(WebUtils.CurrentDomain.ProcessUrl + "store_card.asp");
                sb.Append("?CompanyNum=" + Data.MerchantNum.ToEncodedUrl(encoding));
				sb.Append("&CardNum=" + encValue1.ToEncodedUrl(encoding));
				sb.Append("&ExpMonth=" + Data.MethodData.MethodInstance.ExpirationDate.GetValueOrDefault().Month);
				sb.Append("&ExpYear=" + Data.MethodData.MethodInstance.ExpirationDate.GetValueOrDefault().Year);
				sb.Append("&CVV2=" + encValue2.ToEncodedUrl(encoding));
				sb.Append("&CHFullName=" + Data.Payer.FullName.ToEncodedUrl());
				sb.Append("&CHEmail=" + Data.Payer.EmailAddress.ToEncodedUrl());
				sb.Append("&CHPersonalNum=" + Data.Payer.PersonalNumber.ToEncodedUrl());
				sb.Append("&CHPhoneNumber=" + Data.Payer.PhoneNumber.ToEncodedUrl());
                sb.Append("&Comment=" + Data.Comment.ToEncodedUrl(encoding));
                sb.Append("&billingStreet1=" + Data.Address.AddressLine1.ToEncodedUrl(encoding));
                sb.Append("&billingStreet2=" + Data.Address.AddressLine2.ToEncodedUrl(encoding));
                sb.Append("&billingCity=" + Data.Address.City.ToEncodedUrl(encoding));
                sb.Append("&billingZipcode=" + Data.Address.PostalCode.ToEncodedUrl(encoding));
                sb.Append("&billingState=" + Data.Address.StateISOCode);
                sb.Append("&billingCountry=" + Data.Address.CountryISOCode);
                sb.Append("&Signature=" + PageBase.CreateMD5(Data.MerchantNum + merchantHash, encoding).ToEncodedUrl(encoding));
                break;
            default:
                throw new ApplicationException("Unknown Payment Method");
            }
            return sb.ToString();
        }
    }
}
