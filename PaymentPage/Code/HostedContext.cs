﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Bll;

namespace Netpay.PaymentPage.Code
{
    public enum SignatureType { MD5, SHA256 }
    public class HostedException : Exception { public HostedException(string message) : base(message) { } }

    public class HostedContext : HostedParameters
    {
        private NameValueCollection _userValues = HttpUtility.ParseQueryString("");
        private Dictionary<string, object> _runtimeObjects = new Dictionary<string, object>();

        public HostedContext(HttpContext context, bool saveContext) : base(context.Request.QueryString.ToString(), context.Request.Form.ToString())
        {
            if (Merchant != null && saveContext) SavePaymentPageLog(context);
        }

        private HostedContext(Bll.Merchants.Hosted.PaymentPageLog log) : base(log.QueryString, log.RequestForm)
        {
            LogPaymentPage = log;
            _userValues = HttpUtility.ParseQueryString(log.SessionContents);
        }

        public static HostedContext Load(int logId, bool setCurrent)
        {
            var log = Bll.Merchants.Hosted.PaymentPageLog.Load(logId);
            if (log == null) return null;
            var ret = new HostedContext(log);
            if (setCurrent) Current = ret;
            return ret;
        }

        public static HostedContext Current
        {
            get { return HttpContext.Current.Items["HostedRequestData"] as HostedContext; }
            set { HttpContext.Current.Items["HostedRequestData"] = value; }
        }

        protected override string this[string key]
        {
            get
            {
                var ret = _userValues[key];
                if (ret != null) return ret;
                return base[key];
            }
            set
            {
                if (base[key] == value)
                {
                    if (_userValues.AllKeys.Contains(key)) _userValues.Remove(key);
                }
                else _userValues[key] = value;
            }
        }

        private T GetRuntimeObject<T>(string key, Func<T> initProc)
        {
            object ret;
            if (!_runtimeObjects.TryGetValue(key, out ret))
            {
                if (initProc != null)
                    _runtimeObjects.Add(key, ret = initProc());
            }
            return (T)ret;
        }

        public int? CustomerID { get { return _userValues["CustomerID"].ToNullableInt(); } set { _userValues["CustomerID"] = value != null ? value.ToString() : null; } }
        public int? LogPaymentPageID { get { return _userValues["LogPaymentPageID"].ToNullableInt(); } private set { _userValues["LogPaymentPageID"] = value != null ? value.ToString() : null; } }
        //pm data
        public string pm_value1 { get { return Infrastructure.Security.Encryption.DecryptHex(_userValues["pm_value1"]); } set { _userValues["pm_value1"] = Infrastructure.Security.Encryption.EncryptHex(value); } }
        public string pm_value2 { get { return Infrastructure.Security.Encryption.DecryptHex(_userValues["pm_value2"]); } set { _userValues["pm_value2"] = Infrastructure.Security.Encryption.EncryptHex(value); } }
        public DateTime? pm_expDate { get { return _userValues["pm_expDate"].ToNullableDate(); } set { _userValues["pm_expDate"] = value != null ? value.Value.ToString("yyyy-MM-dd") : null; } }

        public string pm_IssuerCountryIso { get { return _userValues["pm_IssuerCountryIso"]; } set { _userValues["pm_IssuerCountryIso"] = value; } }
        public string pm_IssuerId { get { return _userValues["pm_IssuerId"]; } set { _userValues["pm_IssuerId"] = value; } }
        public string pm_IssuerName { get { return _userValues["pm_IssuerName"]; } set { _userValues["pm_IssuerName"] = value; } }

        public string DesignDir { get { return (PaymentSettings != null && PaymentSettings.UIVersion == 2) ? "~/Designs/New/" : (IsMobile ? "~/Designs/Mobile/" : null); } }
        public string CustomCss { get { return (PaymentSettings != null && PaymentSettings.UIVersion == 2) ? PaymentSettings.ThemeCssFileName.NullIfEmpty().ValueIfNull("grey") : null; } }

        public bool HasToken { get { return LogPaymentPageID.HasValue; } }
        public string ContextToken { get { return string.Format("{0}-{1}", LogPaymentPageID, CreateSignature(SignatureType.SHA256, null, "HostedContext" + LogPaymentPageID + Domain.Current.Host)); } }
        public string ContextHash { get { return string.Format("{0}", CreateSignature(SignatureType.SHA256, null, "HostedHash" + LogPaymentPageID + Domain.Current.Host)); } }

        public void ValidateParameters(bool validateAmount)
        {
            if (Merchant == null) throw new HostedException(Resources.Main.ErrorParameterSign);
            if (Merchant.ActiveStatus <= MerchantStatus.Closed) throw new HostedException(Resources.Main.ErrorMerchantClosed);

            if (validateAmount)
            {
                if (PaymentSettings == null) throw new HostedException(Resources.Main.ErrorMerchantUnauthorized);
                if (Currency == null) throw new HostedException(Resources.Main.ErrorParameterCurrency);
                if (trans_amount == 0) throw new HostedException(Resources.Main.ErrorParameterAmount);
                if (trans_type != TransactionType.Capture && trans_type != TransactionType.Authorization) throw new HostedException(Resources.Main.ErrorParameterTransType);
                if (!(ValidateSignature(new string[] { "merchantID", "trans_amount", "trans_currency" }, signature, PaymentSettings.IsSignatureOptional) || ValidateSignature("signature", PaymentSettings.IsSignatureOptional)))
                    throw new HostedException(Resources.Main.ErrorParameterSign);

                var merchantRecurring = Bll.Transactions.Recurring.MerchantSettings.Load(Merchant.ID);
                if ((merchantRecurring == null || !merchantRecurring.IsEnabled) && !string.IsNullOrEmpty(RecurringSeries))
                    throw new HostedException(Resources.Main.ErrorNoRecurring);

                if (!Merchant.ProcessSettings.IsApprovalOnly && trans_type == TransactionType.Authorization)
                    throw new HostedException(Resources.Main.ErrorNoApproval);
                if ((PaymentMethods.Length + PaymentMethodGroups.Count) == 0) throw new HostedException(Resources.Main.ErrorSolutionDisabled);



                if (PaymentMethods.Length == 1) PaymentMethod = (PaymentMethodEnum)PaymentMethods[0].ID;
                else if (PaymentMethodGroups.Count == 1) PaymentMethodGroup = (PaymentMethodGroupEnum)PaymentMethodGroups[0].ID;
                //if (PaymentMethods.Length == 1) return PaymentMethods[0].Group;
                //return PaymentMethodGroupEnum.Other;

                //if (PaymentMethods.Length == 1) return PaymentMethods[0].Type;
                //if (PaymentMethodGroups.Count == 1) return (PaymentMethodType)PaymentMethodGroups[0].Type;
                //return PaymentMethodType.Unknown;
            }
        }

        #region Runtime Objects

        public Bll.Merchants.Hosted.PaymentPageLog LogPaymentPage { get { return GetRuntimeObject("LogPaymentPage", () => LogPaymentPageID == null ? new Bll.Merchants.Hosted.PaymentPageLog() : Bll.Merchants.Hosted.PaymentPageLog.Load(LogPaymentPageID.Value)); } set { _runtimeObjects["LogPaymentPage"] = value; } }
        public Bll.Merchants.Merchant Merchant { get { return GetRuntimeObject("Merchant", () => Bll.Merchants.Merchant.Load(merchantID)); } }
        public Bll.Merchants.RiskSettings RiskSettings { get { if (Merchant == null) return null; return GetRuntimeObject("RiskSettings", () => Bll.Merchants.RiskSettings.Load(Merchant.ID)); } }
        public Bll.Merchants.Hosted.PaymentPage PaymentSettings { get { if (Merchant == null) return null; return GetRuntimeObject("PaymentSettings", () => Bll.Merchants.Hosted.PaymentPage.Load(Merchant.ID)); } }
        public Bll.Shop.Cart.MerchantSettings ShopSettings { get { if (Merchant == null) return null; return GetRuntimeObject("ShopSettings", () => Bll.Shop.Cart.MerchantSettings.Load(Merchant.ID)); } }
        public Bll.Merchants.Hosted.Customization MerchantCustomization { get { if (Merchant == null) return null; return (skin_no == null) ? null : GetRuntimeObject("MerchantCustomization", () => Bll.Merchants.Hosted.Customization.Load(skin_no.Value, Merchant.ID)); } }
        public Bll.Currency Currency { get { return GetRuntimeObject("Currency", () => Bll.Currency.Get(trans_currency.EmptyIfNull())); } }
        public Bll.Country IpCountry { get { return GetRuntimeObject("IpCountry", () => Bll.Country.Get(System.Net.IPAddress.Parse(LogPaymentPage.RemoteIPAddress))); } }
        public bool IsMobile { get { return GetRuntimeObject("IsMobile", () => disp_mobile.GetValueOrDefault()); } set { _runtimeObjects["IsMobile"] = value; } }
        public SignatureType SignType { get { return GetRuntimeObject("SignType", () => SignatureType.SHA256); } set { _runtimeObjects["SignType"] = value; } }

        public Bll.PaymentMethods.PaymentMethod[] AllPaymentMethods
        {
            get
            {
                if (Merchant == null || Currency == null) return null;
                return GetRuntimeObject("AllPaymentMethods", () =>
                {
                    return Bll.Merchants.Hosted.PaymentPage.GetAvailablePaymentMethods(Merchant.ID, Currency.ID, false)
                        .Where(v => v.Type == CommonTypes.PaymentMethodType.CreditCard && v.ID != (int)CommonTypes.PaymentMethodEnum.CCUnknown).ToArray();
                });
            }
        }

        public List<Bll.PaymentMethods.Group> PaymentMethodGroups
        {
            get
            {
                return GetRuntimeObject("PaymentMethodGroups", () =>
                {
                    if (PaymentSettings == null) return null;
                    var ret = PaymentSettings.GetPaymentMethodGroups(true);
                    if (!string.IsNullOrEmpty(disp_paymentType) && disp_paymentType != PaymentMethodEnum.Unknown.ToString())
                    {
                        var displayedPaymentMethods = disp_paymentType.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        ret = ret.Where(pmp => displayedPaymentMethods.Contains(pmp.ShortName)).ToList();
                    }
                    return ret;
                });
            }
        }

        public Bll.PaymentMethods.PaymentMethod[] PaymentMethods
        {
            get
            {
                return GetRuntimeObject("PaymentMethods", () =>
                {
                    var ret = AllPaymentMethods?.Where(v => v.IsPopular);
                    if (ret == null) return null;
                    if (!string.IsNullOrEmpty(disp_paymentType) && disp_paymentType != PaymentMethodEnum.Unknown.ToString())
                    {
                        var displayedPaymentMethods = disp_paymentType.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        ret = ret.Where(pm => displayedPaymentMethods.Contains(pm.Abbreviation));
                    }
                    if (PaymentSettings != null)
                    {
                        if (!PaymentSettings.IsPayPal) ret = ret.Where(pm => pm.ID != (int)PaymentMethodEnum.PayPal);
                        if (!PaymentSettings.IsGoogleCheckout) ret = ret.Where(pm => pm.ID != (int)PaymentMethodEnum.GoogleCheckout);
                        if (!PaymentSettings.IsMoneyBookers) ret = ret.Where(pm => (pm.ID != (int)PaymentMethodEnum.MoneyBookers && pm.ID != (int)PaymentMethodEnum.MoneyBookersHpp));
                        if (!PaymentSettings.IsWebMoney) ret = ret.Where(pm => pm.ID != (int)PaymentMethodEnum.WebMoney);
                    }
                    return ret.ToArray();
                });
            }
        }

        private bool _isUpdatingPaymentMethods = false;
        public PaymentMethodType PaymentMethodType
        {
            get { return this["rt_paymentType"].ToNullableEnumByName<PaymentMethodType>().GetValueOrDefault(); }
            set
            {
                if (value.ToString() == this["rt_paymentType"]) return;
                this["rt_paymentType"] = value.ToString();
                pm_value1 = pm_value2 = null; pm_expDate = null;
                //if (_isUpdatingPaymentMethods) return;
                //_isUpdatingPaymentMethods = true;
                //PaymentMethod = PaymentMethodEnum.Unknown;
                //PaymentMethodGroup = PaymentMethodGroupEnum.Other;
                //_isUpdatingPaymentMethods = false;
            }
        }

        public PaymentMethodEnum PaymentMethod
        {
            get
            {
                int pmint;
                if (int.TryParse(this["rt_paymentMethod"], out pmint))
                {
                    return (PaymentMethodEnum)pmint;
                }
                else
                {
                    return this["rt_paymentMethod"].ToNullableEnumByName<PaymentMethodEnum>().GetValueOrDefault();
                }

            }
            set
            {
                this["rt_paymentMethod"] = value.ToString();
                if (_isUpdatingPaymentMethods) return;
                _isUpdatingPaymentMethods = true;
                var pmData = Bll.PaymentMethods.PaymentMethod.Get(value);
                if (pmData != null)
                {
                    PaymentMethodGroup = pmData.Group;
                    PaymentMethodType = pmData.Type;
                }
                else
                {
                    PaymentMethodGroup = PaymentMethodGroupEnum.Other;
                    PaymentMethodType = PaymentMethodType.Unknown;
                }
                _isUpdatingPaymentMethods = false;
            }
        }

        public PaymentMethodGroupEnum PaymentMethodGroup
        {
            get { return this["rt_paymentGroup"].ToNullableEnumByName<PaymentMethodGroupEnum>().GetValueOrDefault(); ; }
            set
            {
                this["rt_paymentGroup"] = value.ToString();
                if (_isUpdatingPaymentMethods) return;
                _isUpdatingPaymentMethods = true;
                var group = Bll.PaymentMethods.Group.Get(value);
                if (group != null) PaymentMethodType = group.Type.GetValueOrDefault();
                else PaymentMethodType = PaymentMethodType.Unknown;
                PaymentMethod = PaymentMethodEnum.Unknown;
                _isUpdatingPaymentMethods = false;
            }
        }

        public System.Collections.Generic.Dictionary<string, string> MerchantTextData(Language language, Infrastructure.TranslationGroup group)
        {
            if (PaymentSettings == null) return null;
            return GetRuntimeObject(string.Format("{0}-{1}", language, group), () => Bll.Merchants.Hosted.Translation.GetTranslations(PaymentSettings.MerchantID, language, group));
        }

        public string MerchantTextData(Language lng, Infrastructure.TranslationGroup group, string key)
        {
            var col = MerchantTextData(lng, group);
            if (col == null) return string.Empty;
            if (col.ContainsKey(key)) return col[key];
            return string.Empty;
        }

        #endregion

        #region Log
        public string MaskValue(string value)
        {
            int start = 0, end = 0;
            if (string.IsNullOrEmpty(value)) return string.Empty;
            if (value.Length >= 15) { start = 6; end = 4; }
            else if (value.Length >= 10) { start = 2; end = 4; }
            else if (value.Length >= 6) { start = 1; end = 1; }
            else { start = 0; end = 0; }
            string ret = value.Substring(0, start);
            ret += new string('X', value.Length - (start + end));
            ret += value.Substring(value.Length - end, end);
            return ret;
        }

        private void SavePaymentPageLog(HttpContext context)
        {
            var lpp = LogPaymentPage;
            lpp.DateStart = DateTime.Now;
            lpp.MerchantNumber = merchantID;
            lpp.IsSecure = context.Request.IsSecureConnection;
            lpp.RemoteIPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(lpp.RemoteIPAddress)) lpp.RemoteIPAddress = context.Request.UserHostAddress;

            if ((lpp.LocalIPAddress = context.Request.ServerVariables["LOCAL_ADDR"]) == string.Empty) lpp.LocalIPAddress = null;
            if ((lpp.RequestMethod = context.Request.ServerVariables["REQUEST_METHOD"]) == string.Empty) lpp.RequestMethod = null;
            if ((lpp.PathTranslate = context.Request.ServerVariables["PATH_TRANSLATED"]) == string.Empty) lpp.PathTranslate = null;
            if ((lpp.HttpHost = context.Request.ServerVariables["HTTP_HOST"]) == string.Empty) lpp.HttpHost = null;
            if ((lpp.HttpReferer = context.Request.ServerVariables["HTTP_REFERER"]) == string.Empty) lpp.HttpReferer = null;
            if ((lpp.QueryString = context.Request.QueryString.ToString()) == string.Empty) lpp.QueryString = null;
            if ((lpp.RequestForm = context.Request.Form.ToString()) == string.Empty) lpp.RequestForm = null;
            if (lpp.QueryString != null && lpp.QueryString.Length > 4000) lpp.QueryString = lpp.QueryString.Substring(0, 3999);
            if (lpp.RequestForm != null && lpp.RequestForm.Length > 4000) lpp.RequestForm = lpp.RequestForm.Substring(0, 3999);
            if (_userValues != null)
            {
                lpp.SessionContents = _userValues.ToString();
                if (lpp.SessionContents == string.Empty) lpp.SessionContents = null;
                else if (lpp.SessionContents.Length > 4000) lpp.SessionContents = lpp.SessionContents.Substring(0, 3999);
            }

            lpp.Save();
            LogPaymentPageID = lpp.ID;
        }

        public void SaveState()
        {
            if (LogPaymentPageID.HasValue)
                LogPaymentPage.SaveState(_userValues.ToString());
        }

        public void SaveLogTrans(string requestString, string responseString, PaymentMethodType pmt)
        {
            if (LogPaymentPage == null) return;
            string value1 = pm_value1, value2 = pm_value2;
            if (!string.IsNullOrEmpty(value1)) requestString = requestString.Replace(value1, MaskValue(value1));
            if (!string.IsNullOrEmpty(value2)) requestString = requestString.Replace(value2, MaskValue(value2));
            Bll.Process.GenericLog.Log(LogHistoryType.HostedPageCharge_SystemProcess, Merchant.ID, pmt.ToString(), requestString, responseString, null);
            LogPaymentPage.AddTransactionLog(pmt, requestString, responseString, responseString.EmptyIfNull().GetUrlValue("TransID").ToNullableInt());
        }
        #endregion

        #region Signature
        public string CreateSignature(SignatureType signType, Encoding enc, string vParams, bool useRaw_output = true)
        {
            if (enc == null) enc = Encoding.UTF8;
            byte[] hashValue = null;
            using (var md = System.Security.Cryptography.HashAlgorithm.Create(signType == SignatureType.MD5 ? "MD5" : "SHA256"))
                hashValue = md.ComputeHash(enc.GetBytes(vParams));
            if (!useRaw_output) return System.Convert.ToBase64String(BitConverter.ToString(hashValue).ToBytes());
            string result = System.Convert.ToBase64String(hashValue);
            return result;
        }

        protected bool ValidateSignature(string values, string signature, out SignatureType signType)
        {
            var encodings = new Encoding[] { Encoding.UTF8, Encoding.GetEncoding("windows-1255") };
            var signTypes = Enum.GetValues(typeof(SignatureType)) as SignatureType[];
            foreach (var sgn in signTypes)
            {
                signType = sgn;
                foreach (var enc in encodings)
                {
                    if (CreateSignature(signType, enc, values, true) == signature) return true;
                    if (CreateSignature(signType, enc, values, false) == signature) return true;
                }
            }
            signType = SignatureType.SHA256;
            return false;
        }

        protected bool ValidateSignature(string signatureParamName, bool bRequired)
        {
            string signature = _requestValues[signatureParamName];
            if (Merchant == null || string.IsNullOrEmpty(Merchant.HashKey) || string.IsNullOrEmpty(signature))
                return bRequired;

            SignatureType signType;
            StringBuilder parameters = new StringBuilder();
            for (int i = 0; i < _requestValues.Count; i++)
                if (_requestValues.Keys[i] != signatureParamName) parameters.Append(_requestValues[i]);
            parameters.Append(Merchant.HashKey);
            var ret = ValidateSignature(parameters.ToString(), signature, out signType);
            this.SignType = signType;
            return ret;
        }

        protected bool ValidateSignature(string[] checkParams, string signature, bool bRequired)
        {
            if (Merchant == null || string.IsNullOrEmpty(Merchant.HashKey) || string.IsNullOrEmpty(signature))
                return bRequired;

            SignatureType signType;
            StringBuilder parameters = new StringBuilder();
            for (int i = 0; i < checkParams.Length; i++)
                if (_requestValues[checkParams[i]] != null) parameters.Append(_requestValues[checkParams[i]]);
            parameters.Append(Merchant.HashKey);
            var ret = ValidateSignature(parameters.ToString(), signature, out signType);
            this.SignType = signType;
            return ret;
        }
        #endregion

        #region Notification & Reply
        public string ResponseParams { get { return "&merchantID=" + merchantID + "&trans_amount=" + trans_amount + "&trans_installments=" + trans_installments + "&trans_currency=" + trans_currency + "&client_id=&trans_refNum=" + HttpUtility.UrlEncode(trans_refNum); } }
        public string NotifyParams { get { return "&client_fullName=" + client_fullName.EmptyIfNull().ToEncodedUrl() + "&client_phoneNum=" + client_phoneNum.EmptyIfNull().ToEncodedUrl() + "&client_email=" + client_email.EmptyIfNull().ToEncodedUrl(); } }
        public string ResponseExParams
        {
            get
            {
                System.Collections.Generic.HashSet<string> internalParams = new HashSet<string>(
                    ("merchantID,url_notify,url_redirect,signature," +
                    "trans_installments,trans_currency,trans_amount,trans_type,trans_refNum,trans_comment,trans_recurring1,trans_recurring2,trans_recurring3," +
                    "client_fullName,client_email,client_phoneNum,client_idNum," +
                    "disp_payFor,disp_paymentType,disp_lng,skin_no").Split(','));
                string ret = "";
                foreach (string s in _requestValues.Keys)
                    if (!internalParams.Contains(s))
                        ret += ("&" + s + "=" + HttpUtility.UrlEncode(_requestValues[s]));
                return ret;
            }
        }

        public string GetReplyParams(string address, string reply, string replyDesc, int transID, DateTime date, string paymentDisplay, int storageID, string walletID, int recurringID)
        {
            if (string.IsNullOrEmpty(address)) return null;
            string toAddress = address + (address.IndexOf('?') > -1 ? "&" : "?");
            string signValue = reply + transID + Merchant.HashKey;
            signValue = CreateSignature(SignType, null, signValue);
            toAddress += "replyCode=" + HttpUtility.UrlEncode(reply) + "&replyDesc=" + HttpUtility.UrlEncode(replyDesc) + "&trans_id=" + transID + "&trans_date=" + HttpUtility.UrlEncode(date.ToString("u")) + "&storage_id=" + storageID + "&client_Wallet_id=" + walletID + "&recurringSeries_id=" + recurringID + "&paymentDisplay=" + HttpUtility.UrlEncode(paymentDisplay) + ResponseParams + "&signature=" + HttpUtility.UrlEncode(signValue) + ResponseExParams;
            return toAddress;
        }

        public int SendClientReply(string invokeUrl)
        {
            if (!Netpay.Infrastructure.Application.IsProduction)
                Logger.Log(LogSeverity.Info, LogTag.PaymentPage, "invokeUrl", invokeUrl);
            var webRequest = System.Net.WebRequest.Create(invokeUrl);
            webRequest.Timeout = 180 * 1000;
            var webResponse = (System.Net.HttpWebResponse)webRequest.GetResponse();
            return (int)webResponse.StatusCode;
        }

        public int SendClientReply(string reply, string replyDesc, int transID, DateTime date, string paymentDisplay, int storageID, string walletID, int recurringID)
        {
            string toAddress = GetReplyParams(!string.IsNullOrEmpty(url_notify) ? url_notify : (PaymentSettings != null ? PaymentSettings.NotificationUrl : null), reply, replyDesc, transID, date, paymentDisplay, storageID, walletID, recurringID) + NotifyParams;
            if (string.IsNullOrEmpty(toAddress)) return -1;
            int httpRet = 0;
            string httpError = "";
            try { httpRet = SendClientReply(toAddress); }
            catch (Exception ex) { httpError = ex.Message; }
            Bll.Process.GenericLog.Log(LogHistoryType.HostedPageCharge_MerchantNotification, Merchant.ID, "Notification", toAddress, httpError, httpRet.ToString());
            return httpRet;
        }
        #endregion

        #region process

        public void SendChargeRequest(string parameters, out string outParams, PaymentMethodType pmt)
        {
            var hsc = Infrastructure.HttpClient.SendHttpRequest(parameters, out outParams, null, null, 300 * 1000);
            SaveLogTrans(parameters, outParams, pmt);
            if (hsc != System.Net.HttpStatusCode.OK)
            {
                Logger.Log(LogSeverity.Error, LogTag.PaymentPage, outParams, parameters);
                throw new Exception(Resources.Main.ErrorCommunication);
            }
        }

        public Bll.Transactions.Transaction CreateTransaction()
        {
            var trans = new Bll.Transactions.Transaction();
            trans.Merchant = Merchant;
            trans.CustomerID = CustomerID;
            trans.Amount = trans_amount;
            trans.CurrencyIsoCode = trans_currency;
            trans.Installments = (byte)trans_installments;
            trans.CreditType = trans_installments > 1 ? CreditType.Installments : CreditType.Regular;
            trans.TransactionSource = (Web.WebUtils.CurrentLanguage == CommonTypes.Language.Hebrew ? TransactionSource.HostedPageV2He : TransactionSource.HostedPageV2En);
            //trans.ProductId = context.public_itemId;
            trans.IP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(trans.IP)) trans.IP = HttpContext.Current.Request.UserHostAddress;
            trans.OrderNumber = trans_refNum;
            trans.TransType = trans_type;

            trans.PaymentData = new Bll.Transactions.Payment();
            trans.PaymentData.MethodInstance.PaymentMethodId = PaymentMethod;
            if (trans.PaymentData.MethodInstance is Bll.PaymentMethods.CreditCard)
            {
                var cardData = trans.PaymentData.MethodInstance as Bll.PaymentMethods.CreditCard;
                cardData.CardNumber = pm_value1;
                cardData.Cvv = pm_value2;
                cardData.ExpirationDate = pm_expDate;
            }
            else if (trans.PaymentData.MethodInstance is Bll.PaymentMethods.BankAccount)
            {
                var bankAccount = trans.PaymentData.MethodInstance as Bll.PaymentMethods.BankAccount;
                bankAccount.AccountNumber = pm_value1;
                bankAccount.RoutingNumber = pm_value2;
                trans.PaymentData.IssuerCountryIsoCode = pm_IssuerCountryIso;
            }
            else if (trans.PaymentData.MethodInstance is Bll.PaymentMethods.PushAccount)
            {
                var bankAccount = trans.PaymentData.MethodInstance as Bll.PaymentMethods.PushAccount;
                bankAccount.BankID = pm_IssuerId.ToNullableInt().GetValueOrDefault();
                bankAccount.BankName = pm_IssuerName;
                bankAccount.CountryIso = pm_IssuerCountryIso;
            }

            {   //billing address
                var address = new Bll.Transactions.BillingAddress();
                address.AddressLine1 = client_billaddress1;
                address.AddressLine2 = client_billaddress2;
                address.City = client_billcity;
                address.PostalCode = client_billzipcode;
                address.CountryISOCode = client_billcountry;
                address.StateISOCode = client_billstate;
                if (!Bll.Address.IsEmpty(address)) trans.PaymentData.BillingAddress = address;
            }

            var payer = trans.PayerData = new Bll.Transactions.Payer();
            payer.FullName = client_fullName;
            payer.EmailAddress = client_email;
            payer.PersonalNumber = client_idNum;
            payer.PhoneNumber = client_phoneNum;
            payer.DateOfBirth = client_dateOfBirth;
            {
                var shipaddress = new Bll.Transactions.ShippingDetails();
                shipaddress.AddressLine1 = client_shipaddress1;
                shipaddress.AddressLine2 = client_shipaddress2;
                shipaddress.City = client_shipcity;
                shipaddress.PostalCode = client_shipzipcode;
                shipaddress.CountryISOCode = client_shipcountry;
                shipaddress.StateISOCode = client_shipstate;
                if (!Bll.Address.IsEmpty(shipaddress)) payer.ShippingDetails = shipaddress;
            }
            trans.PayForText = disp_payFor;
            trans.Comment = trans_comment;
            return trans;
        }

        public System.Collections.Specialized.NameValueCollection Process(string retUrl)
        {
            var trans = CreateTransaction();
            var reqParams = trans.GetProcessParamters();
            if (public_itemId != null && public_quantity != null)
            {
                reqParams.Add("MerchantProductId", public_itemId.Value.ToString());
                reqParams.Add("MerchantProductQuantity", public_quantity.Value.ToString());
            }
            if (shop_cartId != null) reqParams.Add("CartId", shop_cartId.ToString());
            reqParams.Add("StoreCc", (trans_storePm ? "1" : "0"));

            if (!string.IsNullOrEmpty(RecurringSeries))
            {
                string[] recurringParams = RecurringSeries.Split(';');
                for (int i = 0; i < recurringParams.Length; i++)
                    reqParams.Add("Recurring" + (i + 1), recurringParams[i]);
                if (trans_recurringType != null) reqParams.Add("RecurringTransType", trans_recurringType.Value.ToString());
            }
            reqParams.Add("AutoWalletRegistration", (client_autoRegistration ? "1" : "0"));
            reqParams.Add("RetURL", retUrl);

            var enc = System.Text.Encoding.UTF8; //.GetEncoding("windows-1255");
            string responseData, reqString = Bll.Transactions.Transaction.GetProcessUrl(PaymentMethodGroup) + reqParams.ToQueryString(enc);

            //responseValues = Bll.Transactions.Transaction.ProcessTransaction(Data.Transaction.PaymentData.MethodInstance.PaymentMethodGroupId, reqParams);
            //SendChargeRequest(PaymentRequest.CreatePaymentRequestString(Data, retUrl, Request.UserHostAddress), out responseData, Data.PaymentMethodType);
            SendChargeRequest(reqString, out responseData, PaymentMethodType);
            var responseValues = System.Web.HttpUtility.ParseQueryString(responseData, enc);
            return responseValues;
        }
        #endregion
    }
}