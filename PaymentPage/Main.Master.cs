﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.PaymentPage.Code;
using Netpay.Bll;
using System.Threading;
using System.Globalization;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.PaymentPage
{
    public partial class Main : Code.MasterPageBase
    {
        public const string DefaultLogoFile = "/NPCommon/images/LogoSecure_NetpayIntl.jpg";

        protected override void OnInit(EventArgs e)
        {
            if (BasePage != null) BasePage.BubbleEvent += new EventHandler(BasePage_BubbleEvent);
            base.OnInit(e);
        }

        protected virtual void RegisterCss()
        {
            string themeForlder = WebUtils.CurrentDomain.ThemeFolder;
            RegisterCssFile(ResolveUrl("Templates/" + themeForlder + "/Styles/style.css"));
            RegisterCssFile(ResolveUrl("Templates/" + themeForlder + string.Format("/Styles/style{0}.css", WebUtils.CurrentDirection)));
          

        }

        protected override void OnLoad(EventArgs e)
        {
            RegisterCss();
            Page.Header.Controls.Add(new LiteralControl(string.Format("<link rel=\"icon\" type=\"image/x-icon\" href=\"{0}\" />", ResolveUrl("~/Templates/" + WebUtils.CurrentDomain.ThemeFolder + "/Design/New/favicon.ico"))));
            // language bar
            //if (wcLanguageSelector != null)
            //{
            //	wcLanguageSelector.DirectionFlip = WebUtils.CurrentDirection == "rtl";
            //	if (Request.PhysicalPath.ToLower().IndexOf("hosted\\default.aspx") > -1 && Request.QueryString["merchantID"] != null)
            //		wcLanguageSelector.AlternateDestination = Request.Url.GetLeftPart(UriPartial.Path) + "?LoadPrev=1";
            //	wcLanguageSelector.Cultures.AddRange(getCultures());
            //}
            base.OnLoad(e);
            if (litBottomLine != null && !Page.IsPostBack) litBottomLine.Text = GetGlobalResourceObject("Main", "ltBottomLine").ToString().Replace("%LEGAL_NAME%", WebUtils.CurrentDomain.LegalName);

            var shopFolder = WebUtils.CurrentDomain.ThemeFolder;

        }

        public CultureInfo[] getCultures() 
        {
            string allLanguages = System.Configuration.ConfigurationManager.AppSettings["Languages"].ToLower();
            string showLanguages = string.Empty;
            if (BasePage.Data == null)
            {
                showLanguages = allLanguages;
            } else {
                showLanguages = BasePage.Data.disp_lngList.EmptyIfNull();
                if (string.IsNullOrWhiteSpace(showLanguages) || (showLanguages.ToLower() == "all")) showLanguages = allLanguages;
                if ((!string.IsNullOrWhiteSpace(BasePage.Data.disp_lng)) && (!showLanguages.Contains(BasePage.Data.disp_lng.ToLower())))
                    showLanguages += "," + BasePage.Data.disp_lng;
            }
            if (WebUtils.CurrentDomain == null || !WebUtils.CurrentDomain.IsHebrewVisible) showLanguages = showLanguages.Replace("he-il", string.Empty);
            System.Collections.ArrayList arrCul = new System.Collections.ArrayList();
            foreach (string s in showLanguages.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                if (allLanguages.Contains(s.Trim().ToLower())) arrCul.Add(new CultureInfo(s.Trim()));
            return arrCul.ToArray(typeof(CultureInfo)) as CultureInfo[];
        }

        public Image LogoImage {
            get {
                return new Image();
                  //  imgMerchantLogo;
            } }
        
        protected void BasePage_BubbleEvent(object sender, EventArgs e)
        {
            ElementVisibleEventArgs elementVisibleEventArgs = e as ElementVisibleEventArgs;
            if (elementVisibleEventArgs == null) return;
            //if (elementVisibleEventArgs.elementType == ElementVisibleEventArgs.ElementType.NetpayFooter)
            //    tblFooter.Visible = elementVisibleEventArgs.Visible;
        }

        protected override void OnPreRender(EventArgs e)
        {
            //if (BasePage.Data != null && wcLanguageSelector != null) wcLanguageSelector.Visible = BasePage.Data.disp_lngList.EmptyIfNull().ToLower() != "hide";
            //if (imgMerchantLogo != null)
            //{
            //	if (string.IsNullOrEmpty(imgMerchantLogo.ImageUrl))
            //	{
            //		imgMerchantLogo.ImageUrl = BasePage.LogoImage;
            //		if (string.IsNullOrEmpty(imgMerchantLogo.ImageUrl)) imgMerchantLogo.ImageUrl = DefaultLogoFile;
            //	}
            //	imgMerchantLogo.Visible = (imgMerchantLogo.ImageUrl != DefaultLogoFile);
            //}
            //if (imgCardList != null)
            //	imgCardList.Src = string.Format(@"/NPCommon/Images/{0}", (WebUtils.CurrentLanguage == CommonTypes.Language.Hebrew  ? "IconCardList.jpg" : "IconCardListNoHeb.jpg"));
            Page.Title = String.Format("{0} - {1}", WebUtils.CurrentDomain.BrandName, Page.Title);
            base.OnPreRender(e);
        }

        public void SetMerchantLogo(string imagePath)
        {
            //imgMerchantLogo.ImageUrl = imagePath; 
        }
    }
}