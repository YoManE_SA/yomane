﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServicesTester
{
    public class Program
    {
        private const string APPLICATIONTOKEN = "2e84b4cd-b115-43d9-8dcf-6cc2a37d0256";
        private const string PRIVATEKEY = "1Tm2vI6D";
        private const string SERVICE_BASE_ADDRESS = "http://localhost:83/webservices/v2";

        public static void Main(string[] args)
        {
            UseWSWithWebSericeClient(true);
            //UseJson();

            Console.ReadLine();
        }

        #region webServiceClient - only support soap with values mode, but very easy integration
        public static void UseWSWithWebSericeClient(bool login) 
        {
            Console.Write("\r\nTest WebServiceClient");
            //setup
            Netpay.WebServices.V2.ClientSignature.ServiceBaseAddress = SERVICE_BASE_ADDRESS; //can also auto take from web.config AppSettings key name: 'Netpay-WebServices-V2-Address'
            Netpay.WebServices.V2.ClientSignature.ApplicationToken = APPLICATIONTOKEN; //can also auto take from web.config AppSettings key name: 'Netpay-WebServices-V2-AppToken'
            Netpay.WebServices.V2.ClientSignature.PrivateKey = PRIVATEKEY; //can also auto take from web.config AppSettings key name: 'Netpay-WebServices-V2-AppToken-Hash'

            //use services
            var account = new V2Services.AccountClient();
            var customer = new CustomerService.CustomerClient();
            var transaction = new TransactionsReference.TransactionsClient();
            var paymentMethods = new PaymentMethodReference.PaymentMethodsClient();
            var balance = new BalanceReference.BalanceClient();
            if (login)
            {
                //var loginResult = account.Login("lavi@netpay.co.il", "lavi1234", "Pass123", new V2Services.AccountLoginOptions() { setCookie = false, deviceId = "FCB5BE1A-5A28-4EEF-AE77-B195DBF8B333"  }); // merchant acct
                var loginResult = account.Login("udi@netpay.co.il", null, "Pass123", new V2Services.AccountLoginOptions() { setCookie = false, deviceId = "FCB5BE1A-5A28-4EEF-AE77-B195DBF8B333" }); // customer acct
                Console.WriteLine("\r\nLogin Result: " + loginResult.Message);
                if (!loginResult.IsSuccess)
                {
                    Console.ReadLine();
                    return;
                }

                //user cred set one time only after loggin
                Netpay.WebServices.V2.ClientSignature.CredentialsTokenHeaderName = loginResult.CredentialsHeaderName;
                Netpay.WebServices.V2.ClientSignature.CredentialsToken = loginResult.CredentialsToken;
            }


            //user service
            try
            {
                var result = balance.TransferAmount("1314010", 50, "ILS", "1234", "test trx");
                Console.WriteLine(result.IsSuccess);
                Console.WriteLine(result.Message);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

        }
        #endregion

        #region Json
        private static string CreateBytesSignature(string data, string key)
        {
            using (var hash = System.Security.Cryptography.SHA256.Create()) {
                return System.Convert.ToBase64String(hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(data + key)));
            }
        }

        public static System.Collections.Generic.IDictionary<string, object> SendJson(string procAddress, string tokenName, string credToken, string data)
        {
            using (var wc = new System.Net.WebClient()) {
                wc.Headers.Add("Content-Type", "application/json");
                wc.Headers.Add("applicationToken", APPLICATIONTOKEN);
                wc.Headers.Add("signature", "bytes-SHA256, " + CreateBytesSignature(data, PRIVATEKEY));
                if (!string.IsNullOrEmpty(tokenName)) wc.Headers.Add(tokenName, credToken);
                var result = wc.UploadString(SERVICE_BASE_ADDRESS + procAddress, "POST", data);
                var jsConvert = new System.Web.Script.Serialization.JavaScriptSerializer();
                result = result.Replace("\"__type\"", "\"_net_type\"");
                var res = (jsConvert.DeserializeObject(result) as System.Collections.Generic.IDictionary<string, object>).First().Value;
                return res as System.Collections.Generic.IDictionary<string, object>;
            }
        }

        public static void UseJson() //test bytes signature
        {
            Console.Write("\r\nTest Json");
            var jsConvert = new System.Web.Script.Serialization.JavaScriptSerializer();
            //login
            string loginRequest = "{\"password\":\"Pass123\",\"userName\":null,\"email\":\"udi@netpay.co.il\"," +
                "\"options\":{ \"pushToken\":\"\",\"applicationToken\":\"" + APPLICATIONTOKEN + "\",\"appName\":\"appname\",\"setCookie\":false,\"deviceId\":\"fa966faf18026080\"}}";
            var loginResult = SendJson("/Account.svc/Login", null, null, loginRequest);
            Console.Write("\r\nLogin Result: " + loginResult["Message"]);
            //send 
            string resigterDeviceRequest = "{\"deviceId\":\"234234234\", \"phoneNumber\":\"972-54-6333483\"}";
            var result = SendJson("/Account.svc/RegisterDevice", (string)loginResult["CredentialsHeaderName"], (string)loginResult["CredentialsToken"], resigterDeviceRequest);
            Console.Write("\r\nLogin Result: " + result["Message"]);
        }
        #endregion
    }
}
