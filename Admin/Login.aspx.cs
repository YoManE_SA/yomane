﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure.Security;
using Netpay.Web;

namespace Netpay.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack & Request.QueryString["login"] == "outer")
            {
                string username = Request["username"] != null? Request["username"].Trim(): null;
                string password = Request["password"] != null? Request["password"].Trim() : null;
                string mail = Request["mail"] != null? Request["mail"].Trim() : null;


                if (username != null && password != null && mail != null)
                {
                    //email
                    txtEmail.Visible = false;
                    txtEmail.Text = mail;

                    ////user name
                    //txtUserName.Visible = false;
                    //txtUserName.Text = username;

                    //pass
                    txtPassword.TextMode = TextBoxMode.SingleLine;
                    txtPassword.Visible = false;
                    txtPassword.Text = password;
                }
            }

            else if (!IsPostBack)
            {
                // Check if do logout is required
                if (Page.Request.QueryString.ToString() == "DoLogout")
                {
                    Web.WebUtils.Logout();
                }
                else if (Page.Request.QueryString.ToString() == "PasswordChanged")
                {
                    acnLogin.SetSuccess("Password changed, login with the new one.");
                }

                DataBindChildren();
            }
        }

        protected void Login_Command(object sender, CommandEventArgs e)
        {
            try
            {
                bool bIsSeconed = (string)e.CommandArgument == "Seconed";
                var mfParams = new Infrastructure.Security.MultiFactor.Parameters();
                if (bIsSeconed)
                {
                    mfParams.Mode = ddlSecondFactorMode.SelectedIndex == 0 ? Infrastructure.Security.MultiFactor.Mode.SMS : Infrastructure.Security.MultiFactor.Mode.Phone;
                    mfParams.PhoneNumber = null; //auto
                    mfParams.AuthCode = txtSMSCode.Text;
                }
                else mfParams.Mode = Infrastructure.Security.MultiFactor.Mode.Explicit;
                Infrastructure.Security.MultiFactor.ContextParameters = mfParams;

                var ret = Web.WebUtils.Login(new Infrastructure.Security.UserRole[] { Infrastructure.Security.UserRole.Admin },null, txtEmail.Text, txtPassword.Text);

                if (ret != Infrastructure.Security.LoginResult.Success)
                {
                    //Handle case that the password was inserted more than
                    //Three months ago.
                    if (ret == Infrastructure.Security.LoginResult.ForcePasswordChange)
                    {
                        dlgChangePassword.BindAndShow();
                    }
                    else if (ret == Infrastructure.Security.LoginResult.MultiFactorFailed)
                    {
                        txtSMSCode.Text = "";
                        lblPhoneNumber.Text = mfParams.PhoneNumber;
                        switch (mfParams.Mode)
                        {
                            case Infrastructure.Security.MultiFactor.Mode.SMS: ddlSecondFactorMode.SelectedIndex = 0; break;
                            case Infrastructure.Security.MultiFactor.Mode.Phone: ddlSecondFactorMode.SelectedIndex = 1; break;
                        }
                        mvSecondFactorMode.ActiveViewIndex = ddlSecondFactorMode.SelectedIndex;
                        if (!string.IsNullOrEmpty(mfParams.Error))
                            acnSecond.SetMessage(mfParams.Error, true);
                        if (!bIsSeconed) mdlSecondFactor.RegisterShow();
                    }
                    else
                    {
                        if (bIsSeconed)
                        {
                            acnSecond.SetMessage(ret.ToString(), true);
                        }
                        else
                        {
                            //acnLogin.SetMessage(ret.ToString(), true);
                            acnLogin.SetMessage("Unknown username or bad password", true);
                        }
                    }
                }
                else
                {
                    //If the current user is not AdminOfAdmins , remove AdminUsers and AdminGroups entries
                    if (!Netpay.Infrastructure.ObjectContext.Current.IsCurrentLoginAdminOfAdmins)
                    {
                        var menusToRemove = Admin.Application.Current.Menu.ChildItems.Where(x => x.Title == "Settings").FirstOrDefault().ChildItems.Where(x => x.Title == "Admin Users" || x.Title == "Admin Groups").ToList();

                        if (menusToRemove.Count > 0)
                        {
                            foreach (var menutoremove in menusToRemove)
                            {
                                menutoremove.Parent.RemoveChild(menutoremove);
                            }
                        }
                    }
                    else //If the last user was not admin of admins and now a new user was logged in add the menus
                    {
                        if (!Admin.Application.Current.Menu.ChildItems.Where(x => x.Title == "Settings").FirstOrDefault().ChildItems.Any(x => x.Title == "Admin Users"))
                        {
                            Admin.Application.Current.InstalledModules.Where(x => x.Name == "Security").FirstOrDefault().InstallAdmin(EventArgs.Empty);
                        }
                    }

                    if (WebUtils.LoggedUser.IsFirstLogin)
                    {
                        dlgChangePassword.BindAndShow();
                    }
                    else
                    {
                        Response.Redirect("~/", false);
                    }
                }
            }
            catch (Exception ex)
            {
                acnLogin.SetMessage("Communication error, please try again.", true);
                Infrastructure.Logger.Log(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            lnkicon.Attributes["href"] = ResolveUrl("~/Templates/" + Infrastructure.Domain.Current.ThemeFolder + "/favico/16X16.png");
            base.OnPreRender(e);
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQuery(Page);
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "core/bootstrap.min.js");
        }

        protected void SecondFactorMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            mvSecondFactorMode.ActiveViewIndex = ddlSecondFactorMode.SelectedIndex;
        }

        protected void UpdatePassword_Click(object sender, EventArgs e)
        {
            //Handle the case that the user inserted the old password again.
            if (txtPassword.Text == txtNewPassword.Text)
            {
                acnPasswordMessage.SetMessage("Please insert a new password.", true);
                dlgChangePassword.BindAndUpdate();
            }

            //Handle case when both passwords not equale
            if (txtNewPassword.Text != txtConfirmPassword.Text)
            {
                acnPasswordMessage.SetMessage("Passwords must be the same.", true);
                dlgChangePassword.BindAndUpdate();
            }
            else
            {
                //Set the new password
                var ret = Infrastructure.Security.Login.SetNewPasswordAfterExpiredOrReset(new UserRole[] { UserRole.Admin },null, txtEmail.Text, txtPassword.Text, txtNewPassword.Text, Request.UserHostAddress);
                if (ret == Infrastructure.Security.Login.ChangePasswordResult.Success)
                {
                    Response.Redirect("~/Login.aspx?PasswordChanged");
                }
                else
                {
                    acnPasswordMessage.SetMessage("Unable to change password: " + ret.ToString(), true);
                }
            }
        }

        protected void dlgChangePassword_DialogClose(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/", false);
        }

        protected void dlgChangePassword_DataBinding(object sender, EventArgs e)
        {
            mvChangePass.ActiveViewIndex = WebUtils.IsLoggedin ? 0 : 1;
        }

        protected void btnForgotPassword_Click(object sender, EventArgs e)
        {
            //Close the confirmation modal.
            ConfirmationModalDialog.RegisterHide();

            //Validations
            //if (string.IsNullOrEmpty(txtUserName.Text))
            //{
            //    acnLogin.SetMessage("You must insert a valid user name.", true);
            //    return;
            //}

            if (Validation.IsNotMail(txtEmail))
            {
                acnLogin.SetMessage("You must insert a valid email", true);
                return;
            }

            //Try to reset password.
            bool isReset = Infrastructure.Security.Login.ResetPassword(new UserRole[] { UserRole.Admin }, null, txtEmail.Text.Trim(), Request.UserHostAddress);

            //Show the relevant message.
            if (isReset)
            {
                acnLogin.SetSuccess("New password generated and sent via email.");
            }
            else
            {
                acnLogin.SetMessage("User wasn't found.", true);
            }

            //Update ui.
            upActionNotify.DataBind();
            upActionNotify.Update();
        }
    }
}

