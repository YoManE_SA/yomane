﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
    [System.Web.UI.ParseChildren(false)]
    public class FilterSection : System.Web.UI.WebControls.WebControl
    {
        public string Title { get; set; }
        public string Icon { get; set; }
        public FilterSection() { CssClass = "content-filter"; }
        public bool Collapsed { get; set; }

        protected override System.Web.UI.HtmlTextWriterTag TagKey { get { return System.Web.UI.HtmlTextWriterTag.Div; } }
        //protected override string TagName { get { return "section"; } }

        public override void RenderBeginTag(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<section class=\"border-section-filter \">");
            writer.Write(" <div class=\"head-filter\" onclick=\"$('#" + ClientID + "').slideToggle('fast', function() { $('#" + ClientID + "_Button').attr('class', 'fa ' + ($(this).is(':visible') ? 'fa-angle-down' : 'fa-angle-right')); });\">");
            writer.Write("  <div class=\"pull-left\">" + Title + "</div>");
            writer.Write("  <div class=\"pull-right\" ><i id=\"" + ClientID + "_Button\" class=\"fa " + (Collapsed ? "fa-angle-right" : "fa-angle-down") + "\" style=\"cursor:pointer;\"></i></div>");
            writer.Write("  <div class=\"clearfix\"></div>");
            writer.Write(" </div>");
            base.RenderBeginTag(writer);
        }

        public override void RenderEndTag(System.Web.UI.HtmlTextWriter writer)
        {
            base.RenderEndTag(writer);
            writer.Write("</section>");
        }
        protected override void OnPreRender(EventArgs e)
        {
            Attributes["style"] = (Collapsed ? "display:none;" : "");
            base.OnPreRender(e);
        }
    }
}