﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
    public class LegendColors : System.Web.UI.WebControls.WebControl
    {
        public object Items { get; set; }
        public object ItemsText { get; set; }
        public LegendColors() : base(System.Web.UI.HtmlTextWriterTag.Div) { CssClass = "btn-group"; Title = "Legend"; }
        public string Title { get; set; }
        public bool FloatRight { get; set; }
        private bool _requiresDataBinding = true;
        protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
        {
            if (Items == null) return;
            writer.Write(" <button data-toggle=\"dropdown\" class=\"btn btn-info btn-cons-short dropdown-toggle\"> {0} <span class=\"caret\"></span></button>", Title);
            writer.Write(" <ul class=\"dropdown-menu pull-right\">");
            var dicList = Items as System.Collections.IDictionary;
            var dicItemsText = ItemsText as System.Collections.IDictionary;

            foreach (System.Collections.DictionaryEntry i in dicList)
            {
                string convertedKey = null;
                if (dicItemsText != null && dicItemsText.Contains(i.Key))
                {
                    convertedKey = (string)dicItemsText[i.Key];
                }
                else
                {
                    convertedKey = i.Key.ToString().Replace('_', ' ');
                }
                writer.Write("  <li><a href=\"#\"><span class=\"legend-list\" style=\"background:{1};\"></span> {0}</a></li>", convertedKey, System.Drawing.ColorTranslator.ToHtml((System.Drawing.Color)i.Value));
            }
                            
            writer.Write(" </ul>");
            base.RenderContents(writer);
        }

        public override void DataBind()
        {
            base.DataBind();
            _requiresDataBinding = false;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (_requiresDataBinding) DataBind();
            if (FloatRight && !CssClass.Contains("pull-right")) CssClass += " pull-right";
            else if (!FloatRight && CssClass.Contains("pull-right")) CssClass = CssClass.Replace("pull-right", "").Trim();
            base.OnPreRender(e);
        }
    }
}