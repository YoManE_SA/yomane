﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Admin.Controls.GlobalControls;

namespace Netpay.Admin.Controls
{
    public class AdminList : System.Web.UI.WebControls.GridView, System.Web.UI.IPostBackDataHandler, Infrastructure.ISortAndPage
    {
        // Key to ViewState to store DataItemType - the type of data item in the list
        private readonly string DataItemTypeStr = "DataItemType";

        // Key to store the type of data assembly name
        private readonly string DataItemTypeAssemblyNameStr = "DataItemTypeAssemblyName";

        public event EventHandler OrderComplete;
        public bool BubbleLoadEvent { get; set; }
        public bool UseKeyboardNavigation { get; set; }
        public bool JQWidget { get; set; }
        public bool DisableRowSelect { get; set; }
        public bool EnableEditing { get; set; }
        public bool SaveAjaxState { get; set; }
        public bool IsInListView { get; set; }
        public bool EnableRowReorder { get { return ((bool?)ViewState["EnableRowReorder"]).GetValueOrDefault(); } set { ViewState["EnableRowReorder"] = value; } }

        public override int PageCount { get { return (int)System.Math.Ceiling((double)((Infrastructure.ISortAndPage)this).RowCount / (double)PageSize); } }
        public int PageCurrent { get { return ViewState["PageCurrent"].ToNullableInt().GetValueOrDefault(0); } set { ViewState["PageCurrent"] = value; } }
        public bool CountOnly { get; set; }
        public Func<Infrastructure.ISortAndPage, System.Collections.IEnumerable> DataFunction { get; set; }

        public int[] RowOrder { get; private set; }

        public string SortKey { get { return (string)ViewState["SortKey"]; } set { ViewState["SortKey"] = value; } }
        public bool SortDesc { get { return ((bool?)ViewState["SortDescending"]).GetValueOrDefault(); } set { ViewState["SortDescending"] = value; } }
        int Netpay.Infrastructure.ISortAndPage.RowCount { get { return ViewState["RowCount"].ToNullableInt().GetValueOrDefault(0); } set { ViewState["RowCount"] = value; } }

        public System.Web.UI.ITemplate ExtenderTemplate { get; set; }

        public AdminList()
        {
            //SaveAjaxState = true;
            PageSize = 25;
            base.Sorting += new System.Web.UI.WebControls.GridViewSortEventHandler(AdminList_Sorting);
            base.PageIndexChanging += new GridViewPageEventHandler(AdminList_PageIndexChanging);
            base.RowDataBound += new System.Web.UI.WebControls.GridViewRowEventHandler(AdminList_RowDataBound);
            AllowSorting = true; AllowPaging = true;
            PagerSettings.Visible = false;
            base.GridLines = GridLines.None;
            SelectedRowStyle.CssClass = "selected-row";
            // SelectedRowStyle.BackColor. = "000";
            CssClass = "table table-striped";
            BorderWidth = Unit.Pixel(0);
            //HeaderStyle.CssClass = "ui-widget-header";
            Width = Unit.Percentage(100);
            EmptyDataTemplate = new AdminListEmptyTemplate();
            IsInListView = true; // Default value
        }

        private void AdminList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PageCurrent = e.NewPageIndex;

            //Check if the footer has DataButtons control inside it, if so - Invoke DataBind beacuse
            //It holds boolean expressoins issued with security permissions
            var footer = NamingContainer.Controls.OfType<Netpay.Web.Controls.Dialog.PanelContainer>().Where(x => x.ID == "Footer").SingleOrDefault();
            if (footer != null)
            {
                var dataButtons =  footer.Controls.OfType<Admin.Controls.DataButtons>().ToList();
                if (dataButtons != null && dataButtons.Count == 1)
                {
                    dataButtons.First().DataBind();
                }
                else if (dataButtons != null && dataButtons.Count > 1)
                {
                    var dataButtonsControl = dataButtons.Where(x => x.PagedControlID == this.ID).SingleOrDefault();
                    if (dataButtonsControl != null) dataButtonsControl.DataBind();
                }

            }

            DataBind();
        }

        public void DataLoad()
        {
            if (SaveAjaxState && !Page.IsPostBack) LoadQueryStringState();
            else PageCurrent = 0;
            EditIndex = -1;
            DataBind();

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Read query string state - required for example to handle 'Refresh' scenario
            if (IsInListView)
                LoadQueryStringState();

            if (EnableEditing)
            {
                RowEditing += new GridViewEditEventHandler(AdminList_RowEditing);
                SelectedIndexChanging += new GridViewSelectEventHandler(AdminList_SelectedIndexChanging);
                RowCancelingEdit += new GridViewCancelEditEventHandler(AdminList_RowCancelingEdit);
            }

            int selectedIndex = 0;
            if (IsInListView && int.TryParse(Context.Request.QueryString["SelectedIndex"], out selectedIndex))
            {
                SelectedIndex = selectedIndex;
            }
        }

        private void AdminList_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            base.EditIndex = -1;
            PageCurrent = 0;
            if (SortKey == e.SortExpression)
            {
                SortDesc = !SortDesc;
            }
            else
            {
                SortKey = e.SortExpression;
                SortDesc = false;
            }
            e.Cancel = true;
            DataBind();
        }

        private void AdminList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if ((e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.EmptyDataRow) && !DisableRowSelect)
            //{
            //    e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer';");
            //    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this, "Select$" + e.Row.RowIndex);
            //}

            if (e.Row.RowType == DataControlRowType.DataRow && !DisableRowSelect)
            {
                e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer';");
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this, "Select$" + e.Row.RowIndex);
            }

            //Columns.Add(new DataControlField(
            //e.Row.Controls.Add(new System.Web.UI.LiteralControl("<td></td>"));

            if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            {
                e.Row.CssClass = "EmptyData";
            }
        }


        private void AdminList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = false;
            EditIndex = e.NewEditIndex;
            DataBind();
        }

        private void AdminList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            EditIndex = -1;
            DataBind();
        }

        private void AdminList_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            EditIndex = e.NewSelectedIndex;
        }

        protected override void OnSelectedIndexChanging(GridViewSelectEventArgs e)
        {
            base.OnSelectedIndexChanging(e);
            if (System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack && !string.IsNullOrEmpty(SelectedRowStyle.CssClass))
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AdminList_UnSelect",
                    "$('#" + ClientID + " > tbody > tr:eq(" + SelectedIndex + ")').removeClass('" + SelectedRowStyle.CssClass + "');" +
                    "$('#" + ClientID + " > tbody > tr:eq(" + e.NewSelectedIndex + ")').addClass('" + SelectedRowStyle.CssClass + "');", true);
            }
        }

        private void LoadQueryStringState()
        {
            if (Page.Request.QueryString["Page"] != null) PageCurrent = Page.Request.QueryString["Page"].ToNullableInt().GetValueOrDefault(PageCurrent);
            if (Page.Request.QueryString["SelectedIndex"] != null) SelectedIndex = Page.Request.QueryString["SelectedIndex"].ToNullableInt().GetValueOrDefault(SelectedIndex);
            if (Page.Request.QueryString["SortKey"] != null) SortKey = Page.Request.QueryString["SortKey"];
            if (Page.Request.QueryString["SortDesc"] != null) SortDesc = Page.Request.QueryString["SortDesc"].ToNullableBool().GetValueOrDefault(SortDesc);
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);

            // Get DataKey - if SelectedDataKey is null then get it by SelectedIndex (required for scenario that SimulateRowSelection fires the select event)
            var dataKey = SelectedDataKey;

            if (dataKey == null && DataKeys != null && SelectedIndex >= 0 && SelectedIndex < DataKeys.Count)
            {
                dataKey = DataKeys[SelectedIndex];
            }

            if (BubbleLoadEvent)
                RaiseBubbleEvent(this, new CommandEventArgs("LoadItem", dataKey));
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (HeaderRow != null) HeaderRow.TableSection = TableRowSection.TableHeader;
            GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQuery(Page);
            string script = "";
            if (EnableRowReorder)
            {
                System.Web.UI.ScriptManager.RegisterHiddenField(this, ClientID + "_Order", "");
                script = "" +
                    "$(function () {" +
                    "    $('#" + ClientID + " tbody').sortable({" +
                    "        update: function (event, ui) {" +
                    "            $('#" + ClientID + "_Order').val($('#" + ClientID + " tbody').sortable('toArray', { attribute:'order' }));" +
                    "        }" +
                    "    });" +
                    "    $('#" + ClientID + " tbody').disableSelection();" +
                    "});\r\n";
                Page.RegisterRequiresPostBack(this);
            }
            if (UseKeyboardNavigation)
            {
                bool autoNavigate = true;
                script += "$(document).keydown(function(e){ if(!e.ctrlKey) return true; if(e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 13) { var curIndex = $('#" + ClientID + "').attr('SelectedIndex');  $('#" + ClientID + " tbody tr:eq(' + curIndex + ')').removeClass('hover'); if(e.keyCode == 38) curIndex--; else if(e.keyCode == 40) curIndex++; if(curIndex < 0 || curIndex > $('#" + ClientID + " tbody tr').length) return; if(e.keyCode == 13 || " + autoNavigate.ToString().ToLower() + ") { $('#" + ClientID + " tbody tr:eq(' + curIndex + ')').click(); return; } $('#" + ClientID + " tbody tr:eq(' + curIndex + ')').addClass('hover'); $('#" + ClientID + "').attr('SelectedIndex', curIndex); return false; } });";
            }
            if (!string.IsNullOrEmpty(script)) System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), ClientID, script, true);
            if (SaveAjaxState && System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
            {
                GlobalControls.JQueryControls.JQueryControlHelper.AddAjaxUrlParam(Page, "Page", PageCurrent.ToString());
                if (!string.IsNullOrEmpty(SortKey))
                {
                    GlobalControls.JQueryControls.JQueryControlHelper.AddAjaxUrlParam(Page, "SortKey", SortKey.ToString());
                    GlobalControls.JQueryControls.JQueryControlHelper.AddAjaxUrlParam(Page, "SortDesc", SortDesc.ToString());
                }
                if (SelectedIndex > -1) GlobalControls.JQueryControls.JQueryControlHelper.AddAjaxUrlParam(Page, "SelectedIndex", SelectedIndex.ToString());
            }

            base.OnPreRender(e);
        }

        private void SimulateRowSelection(int selectedIndex)
        {
            base.RaisePostBackEvent("Select$" + selectedIndex);
        }

        protected override GridViewRow CreateRow(int rowIndex, int dataSourceIndex, DataControlRowType rowType, DataControlRowState rowState)
        {
            var row = base.CreateRow(rowIndex, dataSourceIndex, rowType, rowState);
            //if (EnableRowReorder) 
            row.Attributes["order"] = rowIndex.ToString();
            return row;
        }

        public string DataItemType
        {
            get
            {
                return (string)ViewState[DataItemTypeStr];
            }
        }

        public string DataItemTypeAssemblyName
        {
            get
            {
                return (string)ViewState[DataItemTypeAssemblyNameStr];
            }
        }

        public bool IsDataSourceHaveType
        {
            get
            {
                return DataSource != null && DataSource.GetType() != null && DataSource.GetType().GetGenericArguments() != null && !string.IsNullOrEmpty(DataSource.GetType().GetGenericArguments()[0].FullName);
            }
        }

        protected override void OnDataBound(EventArgs e)
        {
            base.OnDataBound(e);

            //Save the admin list data item type inside view state, used for export.
            if (IsDataSourceHaveType)
            {
                if (ViewState[DataItemTypeStr] == null || ViewState[DataItemTypeStr].ToString() != DataSource.GetType().GetGenericArguments()[0].FullName)
                {
                    ViewState[DataItemTypeStr] = DataSource.GetType().GetGenericArguments()[0].FullName;
                    ViewState[DataItemTypeAssemblyNameStr] = DataSource.GetType().GetGenericArguments()[0].Assembly.FullName.Split(',')[0];
                }
            }

            // If query string parameters include 'SelectedIndex', then simulate selection of row
            if (DataKeys.Count > 0)
            {
                int selectedIndex = 0;
                if (IsInListView && int.TryParse(Context.Request.QueryString["SelectedIndex"], out selectedIndex))
                {
                    SimulateRowSelection(selectedIndex);
                }
            }
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (JQWidget) writer.Write("<div class=\"ui-widget ui-widget-content ui-corner-all\" style=\"padding:3px;\" >");
            Attributes["SelectedIndex"] = SelectedIndex.ToString();
            base.Render(writer);
            if (JQWidget) writer.Write("</div>");
        }

        protected override void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument.StartsWith("Export$"))
            {
                //Export(eventArgument.Substring(7));
            }
            else base.RaisePostBackEvent(eventArgument);
        }
        /*
        public virtual void Export(string fileType) 
        {
            int pageSize = base.PageSize;
            PageSize = 1000000;
            PageCurrent = 0;
            SetDataSource(this, EventArgs.Empty);
            //DataBind();
            base.PageSize = pageSize;
            if (DataSource is System.Collections.ICollection) {
                var export = Core.Export.DataExport.Create(fileType, Application.Current.MapPath("Temp"));
                export.Export(DataSource);
                Page.Response.Clear();
                Page.Response.ContentType = "application/octet-stream"; //export.ContentType;
                Page.Response.Headers.Add("Content-Description", "File Transfer");
                Page.Response.Headers.Add("Content-Transfer-Encoding", "binary");
                Page.Response.Headers.Add("Expires", "0");
                Page.Response.Headers.Add("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                Page.Response.Headers.Add("Content-Disposition", "attachment; filename=Export." + export.FileExt);
                //Page.Response.ContentEncoding = System.Text.Encoding.UTF8;
                Page.Response.BinaryWrite(export.Data);
                Page.Response.End();
                export = null;
            }
        }
		*/
        bool System.Web.UI.IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
        {
            var postedValue = postCollection[ClientID + "_Order"];
            if (string.IsNullOrEmpty(postedValue)) return false;
            var orderedValues = postedValue.Split(',');
            RowOrder = Array.ConvertAll(orderedValues, int.Parse);
            return true;
        }

        void System.Web.UI.IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            if (RowOrder != null && OrderComplete != null) OrderComplete(this, EventArgs.Empty);
        }
    }

    public class AdminListExtender : System.Web.UI.WebControls.TemplateField
    {
        /*
		public class AdminListExtenderButton : System.Web.UI.WebControls.ButtonField
		{
			public AdminListExtenderButton(){ Text = "+"; HeaderText = "ExB"; }
			public override void InitializeCell(DataControlFieldCell cell, DataControlCellType cellType, DataControlRowState rowState, int rowIndex)
			{
				base.InitializeCell(cell, cellType, rowState, rowIndex);
				if (cellType != DataControlCellType.DataCell) return;
				(cell.Controls[0] as Button).OnClientClick = "alert('z')";
			}
		}
		*/
        public bool OpenOnRowClick { get; set; }
        public override void InitializeCell(DataControlFieldCell cell, DataControlCellType cellType, DataControlRowState rowState, int rowIndex)
        {
            base.InitializeCell(cell, cellType, rowState, rowIndex);
            if (cellType != DataControlCellType.DataCell) return;
            cell.Controls.AddAt(0, new System.Web.UI.LiteralControl("</td></tr><tr style=\"display:none;\"><td colspan=\"" + (Control as AdminList).Columns.Count + "\">"));
            cell.Controls.Add(new System.Web.UI.LiteralControl("</td></tr><tr style=\"display:none;\"><td>"));
            if (OpenOnRowClick) cell.PreRender += cell_PreRender;
        }

        private void cell_PreRender(object sender, EventArgs e)
        {
            ((sender as DataControlFieldCell).Parent as TableRow).Attributes.Add("onclick", "var dtObl = $(this).next('tr'); if(dtObl.css('display') != 'none') dtObl.slideUp(); else dtObl.slideDown();");
        }

        public override bool Initialize(bool sortingEnabled, System.Web.UI.Control control)
        {
            return base.Initialize(sortingEnabled, control);
        }

        protected override void CopyProperties(DataControlField newField)
        {
            base.CopyProperties(newField);
        }

        protected override DataControlField CreateField()
        {
            var ret = base.CreateField();
            return ret;
        }


    }

}