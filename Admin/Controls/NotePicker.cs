﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Controls
{
    public class NotePicker : AutoCompleteBase
    {
        public int? Value { get { return ValueField.Value.ToNullableInt(); } set { ValueField.Value = (this.Value == null ? "" : this.Value.ToString()); } }

        public int? AccountID { get; set; }

        protected override void SearchItems()
        {
            var items = Bll.Accounts.Note.AutoCompleteForAccount(SearchText,AccountID.Value);
            foreach (var n in items)
                appendItem(n.ID.ToString(), n.Text, null, null, null);
        }
    }
}