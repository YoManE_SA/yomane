﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Controls
{
    public class SecuredObjectSelector
    {
        public SecuredObject ParentObject { get; set; }

        public SecuredObject ChildObject { get; set; }

        public SecuredObjectSelector(SecuredObject parent = null, SecuredObject child = null)
        {
            ParentObject = parent;
            ChildObject = child;
        }

        public SecuredObject GetActivObject
        {
            get
            {
                if (ChildObject == null && ParentObject == null) return null;

                if (ChildObject == null && ParentObject != null) return ParentObject;

                if (ChildObject != null && ParentObject == null) return ChildObject;

                if (!ParentObject.HasPermission(PermissionValue.Edit))
                {
                    if (!ChildObject.HasPermission(PermissionValue.Read))
                    {
                        return ChildObject;
                    }
                    else
                    {
                        return ParentObject;
                    }
                }
                else //Parent object editable
                {
                    return ChildObject;
                }
            }
        }
    }
}