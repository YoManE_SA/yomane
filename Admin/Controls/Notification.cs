﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
	public class Notification : System.Web.UI.WebControls.CompositeControl
	{
		public System.Web.UI.WebControls.Label Title { get; private set; }
		public System.Web.UI.WebControls.Label Message { get; private set; }

        public System.Web.UI.WebControls.HyperLink Link { get; private set; }

		public string TitleText { get { return Title.Text; } set { Title.Text = value; } }
		public string MessageText { get{ return Message.Text; } set{ Message.Text = value; } }

        public string NavigateUrl { get { return Link.NavigateUrl; } set { Link.NavigateUrl = value; } }

        public string LinkText { get { return Link.Text; } set { Link.Text = value; } }

		protected override System.Web.UI.HtmlTextWriterTag TagKey { get { return System.Web.UI.HtmlTextWriterTag.Div; } } 

		public Notification()
		{
			Style.Add("margin-bottom", "10px");
			Controls.Add(Title = new System.Web.UI.WebControls.Label()); Title.Font.Bold = true;
			Controls.Add(new System.Web.UI.LiteralControl("<br/>"));

			Controls.Add(Message = new System.Web.UI.WebControls.Label());
            Controls.Add(new System.Web.UI.LiteralControl("<br/>"));

            Controls.Add(Link = new System.Web.UI.WebControls.HyperLink());
            //base.CreateChildControls();
        }
	}
}