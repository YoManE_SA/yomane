﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
    public class PageRouteHandler : System.Web.Routing.IRouteHandler
    {
        public Admin.Module Module { get; set; }
        public string TemplateName { get; private set; }
        public Type TemplatePageType { get; private set; }

        public PageRouteHandler(string templateName, Type templatePageType, Admin.Module module = null)
        {
            Module = module;
            TemplateName = templateName;
            if (templatePageType == null) templatePageType = typeof(DataTablePage);
            TemplatePageType = templatePageType;
        }
        public IHttpHandler GetHttpHandler(System.Web.Routing.RequestContext requestContext)
        {
            string viewName = null;
            if (requestContext.RouteData.Values.ContainsKey("view")) viewName = (string)requestContext.RouteData.Values["view"];
            var retObject = TemplatePageType.GetConstructor(new Type[] { typeof(string), typeof(string), typeof(Admin.Module) }).Invoke(new object[] { TemplateName, viewName, Module });
            return retObject as IHttpHandler;
        }
    }

    public class Page : System.Web.UI.Page, System.Web.SessionState.IRequiresSessionState
    {
        public Admin.Module CurrentModule { get; set; }
        public bool NoMasterPage { get; set; }
        public string TemplateName { get; protected set; }
        public string ViewName { get; protected set; }
        public Site SiteMaster { get { return Master as Site; } }
        public System.Web.UI.Control ContentContainer { get { return Master.FindControl("BodyContent"); } }
        public System.Collections.Generic.List<Admin.ApplicationMenu> TitleParts = new List<Admin.ApplicationMenu>();

        public System.Web.UI.Control ModuleNotActivatedControl
        {
            get
            {
                return LoadControl("~/Modules/ModuleNotActivated/ModuleNotActivated.ascx");
            }
        }

        public System.Web.UI.Control ModuleNotAuthorizedControl
        {
            get
            {
                return LoadControl("~/Modules/ModuleNotAuthorized/ModuleNotAuthorized.ascx");
            }
        }

        public Page() { EnableEventValidation = false; }
        public Page(string templateName, string viewName = null, Admin.Module module = null) : this()
        {
            TemplateName = templateName;
            ViewName = viewName;
            CurrentModule = module;
            if ((ViewName == "") || (ViewName == "index") || (ViewName == "default")) ViewName = null;
        }

        protected override void OnPreInit(EventArgs e)
        {
            CheckSecurity();
            if (!NoMasterPage && string.IsNullOrEmpty(MasterPageFile))
            {
                AppRelativeVirtualPath = Request.ApplicationPath;
                AppRelativeTemplateSourceDirectory = "~/";
                MasterPageFile = "~/Site.Master";
            }
            base.OnPreInit(e);
        }

        protected virtual void CheckSecurity()
        {
            if (!Web.WebUtils.IsLoggedin)
                Response.Redirect("~/Login.aspx", true);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Admin.Application.Current.raiseInitPage(this, e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack)
                try
                {
                    DataBind();
                }
                catch (Exception ex)
                {
                    Netpay.Infrastructure.Logger.Log(ex);

                    if (ex.Message.Contains("NotAuthorized") || !(Page is DataTablePage))
                    {
                        throw ex;
                    }
                    else if (Page is Controls.DataTablePage)
                    {
                        (Page as DataTablePage).FormActionNotify.SetException(ex);
                        (Page as DataTablePage).PageController.FormView.Update();

                        if ((Page as Controls.DataTablePage).PageController.Mode == DataPageController.ViewMode.FilterList || (Page as Controls.DataTablePage).PageController.Mode == DataPageController.ViewMode.List)
                        {
                            (Page as Controls.DataTablePage).SetListViewMessage(ex.Message, true);
                            (Page as Controls.DataTablePage).PageController.ListView.Update();
                        }
                    }
                }
            base.OnPreRender(e);
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
            GlobalControls.JQueryControls.JQueryControlHelper.RenderHistory(Page);
        }
    }
}