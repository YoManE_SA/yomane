﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
    [System.Web.UI.ParseChildren(true)]
    public class ListSection : PanelSection {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Header.CssClass += " wrap-legend";
        }

    	protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
		}

    }
}