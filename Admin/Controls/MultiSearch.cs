﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
	public class MultiSearch : System.Web.UI.WebControls.ListControl, System.Web.UI.IPostBackDataHandler
	{
		public string OnClientSearch { get; set; }
		public string OnClientSelect { get; set; }
		public System.Web.UI.WebControls.Unit SelectWidth { get; set; }
		public string PlaceHolder { get; set; }
		public string TypePlaceHolder { get; set; }
		public bool ShowSearchButton { get; set; }
		public bool ReadOnlyText { get; set; }
		public string Value { get; set; }
		public string ValueType { get { return base.SelectedValue; } set { base.SelectedValue = value; } }
		public int ValueTypeIndex { get { return base.SelectedIndex; } set { base.SelectedIndex = value; } }
		 
		public string TypeClientID { get { return ClientID + "_Type"; } }
		public string ValueClientID { get { return ClientID + "_Value"; } }

        public MultiSearch() { CssClass = "btn btn-primary"; }

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			var txtTypePlaceHolder = TypePlaceHolder;
            if (txtTypePlaceHolder == null && Items.Count > 0 && SelectedIndex < 0) SelectedIndex = 0; 
			if (base.SelectedItem != null) txtTypePlaceHolder = base.SelectedItem.Text;
            writer.Write("<div class=\"input-group multi-search\">");
            writer.Write(" <div class=\"input-group-btn\">");
			writer.Write("  <button data-toggle=\"dropdown\" tabindex=\"-1\" class=\"" + CssClass + " dropdown-toggle\" style=\"" + (SelectWidth.IsEmpty ? "" : "width:" + SelectWidth.ToString() + ";") + "\" type=\"button\" id=\"" + ClientID + "_TypeText\">" + txtTypePlaceHolder + "</button>");
            writer.Write("  <button data-toggle=\"dropdown\" class=\"" + CssClass + " dropdown-toggle\" type=\"button\">");
			writer.Write("   <span class=\"caret\"></span>");
			writer.Write("   <span class=\"sr-only\">Toggle Dropdown</span>");
			writer.Write("  </button>");
			writer.Write("  <ul class=\"dropdown-menu\">");
			foreach(System.Web.UI.WebControls.ListItem li in Items) {
				if (li.Text == "-") writer.Write("   <li class=\"divider\"></li>");
				else writer.Write("   <li><a style=\"cursor:pointer;\" onclick=\"" + (string.IsNullOrEmpty(OnClientSelect) ? " $('#" + ClientID + "_TypeText').text($(this).text()); $('#" + ClientID + "_Type').val('" + li.Value + "');" : OnClientSelect + "(this,  document.getElementById('" + ClientID + "_Type'), document.getElementById('" + ClientID + "_Value'), '" + li.Value + "')") + "\" >" + li.Text + "</a></li>");
			}
			writer.Write("  </ul>");
			writer.Write(" </div>");
			writer.Write(" <input type=\"hidden\" id=\"" + ClientID + "_Type\" name=\"" + UniqueID + "\" value=\"" + ValueType + "\" />");
			//if (ReadOnlyText) {
			//	writer.Write(" <span id=\"" + ClientID + "_Value\" >" + Value + "</span>");
			//}else{
				writer.Write(" <input type=\"text\" id=\"" + ClientID + "_Value\" name=\"" + UniqueID + "_Value\" class=\"form-control\" placeholder=\"" + PlaceHolder + "\" value=\"" + Value + "\"" + (ReadOnlyText ? " disabled" : "") + " />");
			//}
			if (ShowSearchButton) writer.Write(" <div class=\"input-group-btn\"><button class=\"btn btn-primary\" id=\"" + ClientID + "_Submit\" type=\"button\" onclick=\"" + OnClientSearch + "\"><i class=\"fa fa-search\"></i></button></div>");
            writer.Write("</div>");
		}

		bool System.Web.UI.IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			var selValue = postCollection[UniqueID];
			if(Items.FindByValue(selValue) != null) SelectedValue = selValue;
			Value = postCollection[UniqueID + "_Value"];
			return true;
		}

		void System.Web.UI.IPostBackDataHandler.RaisePostDataChangedEvent() {}
	}
}