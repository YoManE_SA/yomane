﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
    [System.Web.UI.ParseChildren(true)]
	public class ModalDialog : Web.Controls.ModalDialog
    {
        public event System.Web.UI.WebControls.CommandEventHandler SaveDuplicateMerchant;

        public bool IsMerchantDuplicateMessage { get; set; }

        public override Web.Controls.GlobalControls.UpdatePanel UpdatePanel
        {
            get
            {
                return FindControl("UpdatePanel") as Admin.Controls.GlobalControls.UpdatePanel;
            }
        }

        public System.Web.UI.WebControls.Button SaveMerchantButton { get { return Footer.Controls.OfType<System.Web.UI.WebControls.Button>().Where(x => x.ID == "SaveMerchant").SingleOrDefault(); } }

        public System.Web.UI.WebControls.Button CancelSaveMerchantButton { get { return Footer.Controls.OfType<System.Web.UI.WebControls.Button>().Where(x => x.ID == "CancelSaveMerchant").SingleOrDefault(); } }

        protected override void AddUpdatePanelToControls()
        {
            Controls.Add(new Controls.GlobalControls.UpdatePanel() { ID = "UpdatePanel", ChildrenAsTriggers = false, UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional, HtmlTag = System.Web.UI.HtmlTextWriterTag.Div, CssClass = "modal-content", Animation = GlobalControls.UpdatePanel.JQAnimation.None });
        }


        public ModalDialog()
        { 
            SaveDuplicateMerchant += ModalDialog_SaveDuplicateMerchant;
		}

        private void ModalDialog_SaveDuplicateMerchant(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {   
            RaiseBubbleEvent(this, e);
        }

		protected override void OnPreRender(EventArgs e)
		{
            if (IsMerchantDuplicateMessage)
            {
                SaveMerchantButton.Visible = true;
                SaveMerchantButton.OnClientClick = HideJSCommand;
                CancelSaveMerchantButton.Visible = true;
                CancelSaveMerchantButton.OnClientClick = HideJSCommand;
            }

            base.OnPreRender(e);
		}

		public override string HideJSCommand { get { return "$('#" + ClientID + "').modal('hide');"; } }
		public override void RegisterShow() { Visible = true; UpdatePanel.Update(); GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(Page, ShowJSCommand, ClientID + "_Show"); }
		public override void RegisterHide() { GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(Body, HideJSCommand, ClientID + "_Hide"); /* Visible = false; */}

		protected override bool OnBubbleEvent(object source, EventArgs args)
		{
			var e = args as System.Web.UI.WebControls.CommandEventArgs;
			if (e != null)
			{
				base.OnBubbleEvent(this, e);
                if (e.CommandName == "SaveDuplicateMerchant")
                {
                    ModalDialog_SaveDuplicateMerchant(this, e);
                }
                return true;
			}
			return base.OnBubbleEvent(source, args);
		}
    }
}