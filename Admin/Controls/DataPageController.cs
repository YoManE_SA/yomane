﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Controls
{
    public class DataPageController : System.Web.UI.WebControls.CompositeControl
    {
        public enum ViewMode { FilterList = 0, List = 1, ListForm = 2, Form = 3 }
        private ViewMode _mode;
        public System.Web.UI.WebControls.Unit FilterWidth { get; set; }
        public System.Web.UI.WebControls.DataKey DataKey { get; set; }
        public string OnClientViewChange { get; set; }
        public bool HideFilter { get; set; }
        public event EventHandler Export;
        public event EventHandler Search;
        //public event EventHandler Read;
        public event EventHandler NewItem;
        public event EventHandler LoadItem;
        public event EventHandler PreSaveItem;
        public event EventHandler SaveItem;
        public event EventHandler PostSaveItem;
        public event EventHandler DeleteItem;
        public event EventHandler Error;
        public event EventHandler ShowTotals;

        //public string RightArrowImage {get; set; }
        //public string LeftArrowImage {get; set; }

        [System.Web.UI.ParseChildren(false)]
        public class DataPageContainer : Controls.GlobalControls.UpdatePanel /*System.Web.UI.UpdatePanel*/, System.Web.UI.INamingContainer
        {
            public event EventHandler DataBound;
            protected override void AddParsedSubObject(object obj)
            {
                ContentTemplateContainer.Controls.Add(obj as System.Web.UI.Control);
            }

            public DataPageContainer()
            {
                RenderMode = System.Web.UI.UpdatePanelRenderMode.Block;
                UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional;
                ChildrenAsTriggers = false;
                ContentTemplateContainer.ID = "upContent";
            }
            protected override void OnInit(EventArgs e)
            {
                base.ContentTemplate = null;
                base.OnInit(e);
            }

            public override void DataBind()
            {
                base.DataBind();
                if (DataBound != null) DataBound(this, EventArgs.Empty);
            }

            public void BindAndUpdate()
            {
                DataBind();
                Update();
            }
        }

        public ViewMode Mode
        {
            get { return _mode; }
            set
            {
                //if (_mode == value) return;
                _mode = value;
                if (Page != null && System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
                {
                    GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(_mode == ViewMode.FilterList ? ListView : FormView, "DataTableController_ChangeMode('" + ClientID + "', " + (int)Mode + ");");
                }
            }
        }

        public ViewMode ForceModeUpdate
        {
            set
            {
                _mode = value;
                if (Page != null)
                {
                    GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(_mode == ViewMode.FilterList ? ListView : FormView, "DataTableController_ChangeMode('" + ClientID + "', " + (int)Mode + ");");

                    if (_mode == ViewMode.List)
                    {
                        ListView.BindAndUpdate();
                    }
                }
            }
        }

        protected override object SaveViewState()
        {
            return new System.Web.UI.Pair(base.SaveViewState(), (DataKey as System.Web.UI.IStateManager).SaveViewState());
        }
        protected override void LoadViewState(object savedState)
        {
            var pair = savedState as System.Web.UI.Pair;
            base.LoadViewState(pair.First);
            (DataKey as System.Web.UI.IStateManager).LoadViewState(pair.Second);
        }

        public DataPageController()
        {
            DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary());
            //FilterWidth = System.Web.UI.WebControls.Unit.Pixel(400);
            //Width = System.Web.UI.WebControls.Unit.Percentage(100);
            Height = System.Web.UI.WebControls.Unit.Percentage(100);
            CssClass = "row dataPageController";

            //RightArrowImage = "~/Images/DataTableSize.jpg";
            //LeftArrowImage = "~/Images/DataTableSize.jpg";

            Controls.Add(new DataPageContainer() { ID = "FilterView" });
            Controls.Add(new DataPageContainer() { ID = "ListView" });
            Controls.Add(new DataPageContainer() { ID = "FormView", ChildrenAsTriggers = false });

            Mode = ViewMode.FilterList;
        }

        protected override void OnInit(EventArgs e)
        {
            if (Page.IsPostBack && !IsViewStateEnabled)
            {
                var keyValues = new System.Collections.Specialized.OrderedDictionary();
                for (int i = 0; i < 10; i++)
                {
                    var strValues = Page.Request.Form[ClientID + ":" + i].EmptyIfNull().Split('$');
                    if (strValues.Length < 2) break;
                    keyValues.Add(strValues[0], strValues[1]);
                }
                DataKey = new System.Web.UI.WebControls.DataKey(keyValues);
            }
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            //HideFilter = Page.Request["HideFilter"].ToNullableBool().GetValueOrDefault(false);
            base.OnLoad(e);
        }

        protected override System.Web.UI.HtmlTextWriterTag TagKey
        {
            get { return System.Web.UI.HtmlTextWriterTag.Div; }
        }

        [System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public DataPageContainer FilterView { get { return FindControl("FilterView") as DataPageContainer; } }

        [System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public DataPageContainer ListView { get { return FindControl("ListView") as DataPageContainer; } }

        public ListSection ListSection
        {
            get
            {
                if (ListView.ContentTemplateContainer.Controls.Count == 0) return null;

                foreach (var control in ListView.ContentTemplateContainer.Controls)
                {
                    if ((control as System.Web.UI.Control).Controls.OfType<System.Web.UI.Control>().Any(x => x is ListSection))
                        return (control as System.Web.UI.Control).Controls.OfType<ListSection>().SingleOrDefault();
                }

                return null;
            }
        }

        public AdminList AdminList
        {
            get
            {
                //Handle two special case of Balance list and upcomming settlements list that has 
                //Two admin lists inside the body of the ListSection.
                if ((Page as DataTablePage).TemplateName == "Balance")
                {
                    var filter = (Page as DataTablePage).GetFilter<Bll.Accounts.Balance.SearchFilters>();
                    if (filter == null || filter.OnlyCurrentValues)
                    {
                        return ListSection.Body.Controls.OfType<AdminList>().ToList().Where(x => x.ID == "rptTotals").SingleOrDefault();
                    }
                    else
                    {
                        return ListSection.Body.Controls.OfType<AdminList>().ToList().Where(x => x.ID == "rptList").SingleOrDefault();
                    }
                }
                else if ((Page as DataTablePage).TemplateName == "SettlementsUpcoming")
                {
                    var filter = (Page as DataTablePage).GetFilter<Bll.Settlements.UpcomingSettlement.SearchFilters>();
                    if (filter == null || !filter.IsSpecificMerchant.HasValue)
                    {
                        return ListSection.Body.Controls.OfType<AdminList>().ToList().Where(x => x.ID == "rptList").SingleOrDefault();
                    }
                    else
                    {
                        return ListSection.Body.Controls.OfType<AdminList>().ToList().Where(x => x.ID == "rptUnsettled").SingleOrDefault();
                    }
                }
                else
                {
                    return ListSection.Body.Controls.OfType<AdminList>().SingleOrDefault();
                }
            }
        }

        [System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public DataPageContainer FormView { get { return FindControl("FormView") as DataPageContainer; } }

        //public AdminTableContainer ContentView { get { return FindControl("ContentView") as AdminTableContainer; } }

        protected void OnSearch(EventArgs e)
        {
            if (Search != null) Search(this, e);
            OnSearched(e);
        }

        protected void OnExport(EventArgs e)
        {
            var currentAdminList = AdminList;            
            if (currentAdminList == null)
            {
                throw new Exception("AdminList control was not found.");
            }
            if (currentAdminList != null && currentAdminList.Rows.Count == 0)
            {
                throw new Exception("There is nothing to export.");
            }

            var typeName = currentAdminList.DataItemType;
            var assemblyName = currentAdminList.DataItemTypeAssemblyName;
            List<string> typeAndAssembly = new List<string>() { typeName, assemblyName };

            System.Web.UI.WebControls.CommandEventArgs exportArg =
                new System.Web.UI.WebControls.CommandEventArgs(((System.Web.UI.WebControls.CommandEventArgs)e).CommandName, typeAndAssembly);
            
            if (Export != null) Export(this, exportArg);
        }

        protected void OnShowTotals(EventArgs e)
        {
            if (ShowTotals != null) ShowTotals(this, e);
        }

        protected void OnNewItem(EventArgs e)
        {
            Mode = ViewMode.ListForm;
            DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary()); // clear DataKey so that previous AcountId will be cleared
            if (NewItem != null) NewItem(this, e);
            AdminList.SelectedIndex = -1;
            ListView.DataBind();
            ListView.Update();
            FormView.DataBind();
            FormView.Update();
        }

        protected void OnLoadItem(EventArgs e)
        {
            var ca = e as System.Web.UI.WebControls.CommandEventArgs;
            if (ca != null && (ca.CommandArgument is System.Web.UI.WebControls.DataKey))
                DataKey = new System.Web.UI.WebControls.DataKey((ca.CommandArgument as System.Web.UI.WebControls.DataKey).Values);
            if (LoadItem != null) LoadItem(this, e);
            OnOpen(e);
        }

        public void LoadActiveItem()
        {
            if (LoadItem != null) LoadItem(this, EventArgs.Empty);
        }

        public void OnSaveItem(EventArgs e)
        {
            if (LoadItem != null) LoadItem(this, new System.Web.UI.WebControls.CommandEventArgs("BeforeSave", DataKey));
            if (PreSaveItem != null) PreSaveItem(this, e);
            if (SaveItem != null) SaveItem(this, e);
            if (PostSaveItem != null) PostSaveItem(this, e);
            OnSaved(e);
        }

        protected void OnDeleteItem(EventArgs e)
        {
            LoadActiveItem();
            if (DeleteItem != null) DeleteItem(this, e);
            OnDeleted(e);
        }

        public void OnSearched(EventArgs e)
        {
            //Website.Controls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(ListView, "$('#" + ClientID + "_ContentTdSeperatorTd').hide('fade');");
            if (Page.Request.QueryString.AllKeys.Any(i => i == "SelectedIndex"))
            {
                Mode = ViewMode.ListForm;
            }
            else if (System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
            {
                Mode = ViewMode.List;
                ListView.DataBind();
                ListView.Update();
            }
            else if (!(System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack))
            {
                //If you reached here that means you invoked the Search event from the QueryString , so you are not in AsyncPostBack mode.
                ForceModeUpdate = ViewMode.List;
            }
        }

        public void OnSaved(EventArgs e)
        {
            Mode = ViewMode.ListForm;
            FormView.DataBind();
            FormView.Update();
            ListView.DataBind();
            ListView.Update();
        }

        public void OnDeleted(EventArgs e)
        {
            Mode = ViewMode.FilterList;
            AdminList.SelectedIndex = -1;
            ListView.DataBind();
            ListView.Update();
        }

        public void OnOpen(EventArgs e)
        {
            Mode = ViewMode.ListForm;
            FormView.DataBind();
            FormView.Update();
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            try
            {
                var page = (source as System.Web.UI.Control).Page as DataTablePage;
                var cea = args as System.Web.UI.WebControls.CommandEventArgs;
                if (cea != null)
                {    
                    if (cea.CommandName == "Export")
                    {
                        OnExport(args);
                        return true;
                    }
                    if (cea.CommandName == "AddItem")
                    {
                        OnNewItem(args);
                        return true;
                    }
                    else if (cea.CommandName == "Search")
                    {
                        OnSearch(args);
                        return true;
                    }
                    else if (cea.CommandName == "Back")
                    {
                        Mode = ViewMode.FilterList;
                        return true;
                    }
                    else if (cea.CommandName == "LoadItem")
                    {
                        OnLoadItem(args);
                        return true;
                    }
                    else if (cea.CommandName == "SaveItem" || cea.CommandName == "Confirm" || cea.CommandName == "SaveDuplicateMerchant")
                    {
                        if (page.IsValid)
                            OnSaveItem(args);
                        else
                        {
                            page.FormActionNotify.SetMessage("Item didn't saved, validation error", true);
                            FormView.Update();
                        }
                        return true;

                    }
                    else if (cea.CommandName == "DeleteItem")
                    {
                        OnDeleteItem(args);
                        return true;
                    }
                    else if (cea.CommandName == "ShowTotals")
                    {
                        OnShowTotals(args);
                        return true;
                    }
                    else if (cea.CommandName == "Page")
                    {
                        ListView.Update();
                        return true;
                    }
                    else if (cea.CommandName == "FormChanged")
                    {
                        FormView.Update();
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                if (Error != null) Error(ex, args);
            }
            return base.OnBubbleEvent(source, args);
        }

        protected override void OnPreRender(EventArgs e)
        {
            //FilterView.Visible = !HideFilter;
            //public enum ViewMode { FilterList = 0, List = 1, ListForm = 2, Form = 3 }
            if (!string.IsNullOrEmpty(OnClientViewChange))
            {
                string changeModeScript = "function " + ClientID + "_Change(clientId, toMode){\r\n" + OnClientViewChange.EmptyIfNull() + "}\r\n";
                if (!Page.ClientScript.IsClientScriptBlockRegistered(ClientID + "_Change"))
                    Page.ClientScript.RegisterClientScriptBlock(GetType(), ClientID + "_Change", changeModeScript, true);
            }
            GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/Netpay-Plugin/DataPageController.js");
            GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/Netpay-Plugin/FormEnterSubmit.js");
            if (FilterView.Controls.Count > 0)
                FilterView.Attributes["onkeydown"] = "return FormEnterSubmit(this, event, null)";
            if (!IsViewStateEnabled)
            {
                var keys = new string[DataKey.Values.Keys.Count];
                DataKey.Values.Keys.CopyTo(keys, 0);
                for (var i = 0; i < keys.Length; i++)
                    System.Web.UI.ScriptManager.RegisterHiddenField(Page, ClientID + ":" + i, (keys[i] + "$" + DataKey.Values[i]));

            }
            base.OnPreRender(e);
        }

        protected override void RenderContents(System.Web.UI.HtmlTextWriter response)
        {
            if (!HideFilter) // Render the FilterView section
            {
                response.Write("<div id=\"" + ClientID + "_FilterTd\" class=\"col-lg-4 adminPageData\" style=\"" + (Mode != ViewMode.FilterList ? "display:none;" : "") + (FilterWidth.IsEmpty ? "width:" + FilterWidth.ToString() + ";" : "") + "\">");
                FilterView.RenderControl(response);
                //<div class="wrap_search_filter"><div class="search_filter"><i class="fa fa-chevron-right"></i></div></div>
                response.Write("</div>");
            }

            // Render the ListView section
            response.Write("<div id=\"" + ClientID + "_ListTd\" class=\"" + (Mode != ViewMode.FilterList ? "col-lg-6" : "col-lg-8") + " adminPageData\" >");
            //response.Write("  <div id=\"" + ClientID + "_ListButtonFilter\" class=\"wrap-back-list\"><i class=\"fa fa-arrow-left back-window\" id=\"" + ClientID + "_ContentTdSeperatorImage\" onclick=\"DataTableController_ChangeMode('" + ClientID + "', 0);\" ></i></div>");
            response.Write("  <a id=\"" + ClientID + "_ListButtonClose\" class=\"wrap-close-list btn btn-default btn-mini\" onclick=\"DataTableController_ChangeMode('" + ClientID + "', $('#" + ClientID + "_FilterTd').css('display') == 'none' ? 0 : 1);\"><i class=\"fa fa-expand\" id=\"" + ClientID + "_FilterTdSeperatorImage\" ></i></a>");
            ListView.RenderControl(response);
            response.Write("</div>");


            //Render the FormView section
            response.Write("<div id=\"" + ClientID + "_ContentTd\" class=\"" + (Mode != ViewMode.Form ? "col-lg-6" : "col-lg-6") + " adminPageData\" style=\"" + (Mode == ViewMode.FilterList ? "display:none;" : "") + "\">");
            response.Write("  <a id=\"" + ClientID + "_ContentButtonExpend\" class=\"wrap-close-list btn btn-default btn-mini\" onclick=\"DataTableController_ChangeMode('" + ClientID + "', 3);\"><i class=\"fa fa-expand\" id=\"" + ClientID + "_FilterTdSeperatorImage\" ></i></a>");
            FormView.RenderControl(response);
            response.Write("</div>");
        }
    }
}

