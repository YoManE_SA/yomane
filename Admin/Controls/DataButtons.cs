﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Controls
{
    [System.Web.UI.ParseChildren(false)]
    public class DataButtons : CompositeControl
    {
        public string ValidationGroup { get; set; }
        public bool EnableTotals { get; set; }
        public bool EnableNew { get; set; }
        public bool EnableAdd { get; set; }
        public bool EnableExport { get; set; }
        public bool EnableSave { get; set; }
        public bool EnableDelete { get; set; }
        public bool EnableBack { get; set; }
        public bool EnableSearch { get; set; }
        //public string SearchFormContainer{ get; set; }

        public event System.EventHandler ShowTotals;
        public event System.EventHandler Search;
        public event System.EventHandler Delete;
        public event System.EventHandler Save;
        public event System.EventHandler Export;
        public event System.EventHandler Add;
        public event System.EventHandler New;

        //public bool JQWidget { get; set; }
        //private Bll.ISortAndPage _pagedControl { get; set; }
        public string PagedControlID { get; set; }
        /*
        public Bll.ISortAndPage PagedControl {
            get { return (FindControl("dvPages") as Pager).PagedControl; }
            set { (FindControl("dvPages") as Pager).PagedControl = value; }
        }
        */
        public Pager PagerControl { get { return FindControl("dvPages") as Pager; } }

        protected override System.Web.UI.HtmlTextWriterTag TagKey { get { return System.Web.UI.HtmlTextWriterTag.Div; } }
        public string CssFloatLeft { get; set; }
        public string CssFloatRight { get; set; }
        public DataButtons()
        {
            CssFloatLeft = "pull-left";
            CssFloatRight = "pull-right";
            Style.Add("text-align", "center");
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            EnsureChildControls();
        }

        protected override bool OnBubbleEvent(object source, System.EventArgs args)
        {
            var e = args as CommandEventArgs;
            if (e != null)
            {
                switch (e.CommandName)
                {
                    case "Search":
                        if (Search != null) { Search.Invoke(source, e); return true; }
                        break;
                    case "DeleteItem":
                        if (Delete != null) { Delete.Invoke(source, e); return true; }
                        break;
                    case "SaveItem":
                        if (Save != null) { Save.Invoke(source, e); return true; }
                        break;
                    case "Export":
                        if (Export != null) { Export.Invoke(source, e); return true; }
                        //else if (PagedControl != null) { Export_Click(source, e); return true; }
                        break;
                    case "AddItem":
                        if (Add != null) { Add.Invoke(source, e); return true; }
                        break;
                    case "NewItem":
                        if (New != null) { New.Invoke(source, e); return true; }
                        break;
                    case "ShowTotals":
                        if (ShowTotals != null) { ShowTotals.Invoke(source, e); return true; }
                        break;

                }
            }
            return base.OnBubbleEvent(source, args);
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            var resource = new System.Resources.ResourceManager("Resources.DataActions", System.Reflection.Assembly.Load("App_GlobalResources"));
            {
                var button = new Button();
                button.ID = "btnBack";
                button.Text = resource.GetString("Button_Back");
                button.CommandName = "Back";
                button.CssClass = "btn btn-cons-short btn-inverse margin-right-5 " + CssFloatLeft;
                button.UseSubmitBehavior = false;
                if (!string.IsNullOrEmpty(Context.Request.QueryString["Ref"]))
                    button.OnClientClick = "document.location='" + Context.Request.QueryString["Ref"] + "';//";
                Controls.Add(button);
            }
            {                
                var button = new Button();
                button.ID = "btnExport";
                button.Text = resource.GetString("Button_Export");
                button.CommandName = "Export";
                button.CssClass = "btn btn-cons-short btn-success margin-left-5 " + CssFloatLeft;

                //Group button code
                //var button = new GlobalControls.JQueryControls.MenuButton();
                //button.GroupCssClass = CssFloatLeft;
                //button.UseSubmitBehavior = false;
                //button.Items.Add(new MenuItem("Excel", "ExcelHtml"));
                //button.Items.Add(new MenuItem("Access", "Access"));
                //button.Items.Add(new MenuItem("CSV", "CSV"));
                //button.Items.Add(new MenuItem("Xml", "Xml"));

                Controls.Add(button);
                if (Page != null)
                {
                    var sm = System.Web.UI.ScriptManager.GetCurrent(Page);
                    if (sm != null) sm.RegisterPostBackControl(button);
                }
            }

            //if (PagedControl != null)
            Controls.Add(new Controls.Pager() { ID = "dvPages", PagedControlID = this.PagedControlID });

            {
                var button = new Button();
                button.ID = "btnNew";
                button.Text = resource.GetString("Button_New");
                button.CommandName = "NewItem";
                button.CssClass = "btn btn-cons-short btn-primary margin-left-5 " + CssFloatRight;
                button.UseSubmitBehavior = false;
                Controls.Add(button);
            }
            {
                var button = new Button();
                button.ID = "btnAdd";
                button.Text = resource.GetString("Button_Add");
                button.CommandName = "AddItem";
                button.CssClass = "btn btn-cons-short btn-primary margin-left-5 " + CssFloatRight;
                button.UseSubmitBehavior = false;
                Controls.Add(button);
            }
            {
                var button = new Button();
                button.ID = "btnSave";
                button.Text = resource.GetString("Button_Save");
                button.ValidationGroup = ValidationGroup;
                button.CommandName = "SaveItem";
                button.CssClass = "btn btn-cons-short btn-info margin-left-5 " + CssFloatRight;
                button.UseSubmitBehavior = false;
                Controls.Add(button);
            }
            {
                var button = new Button();
                button.ID = "btnDelete";
                button.Text = resource.GetString("Button_Delete");
                button.CommandName = "DeleteItem";
                button.OnClientClick = string.Format("return confirm('{0}');", resource.GetString("Delete_Confirm"));
                button.CssClass = "btn btn-cons-short btn-danger margin-left-5 " + CssFloatRight;
                Controls.Add(button);
            }
            {
                System.Web.UI.WebControls.Button button = null;
                //if (UseSearchButton) {
                button = new GlobalControls.SearchButton() { AjaxMode = true };
                //if (SearchFormContainer != null) (button as Website.Controls.SearchButton).FormContainer = SearchFormContainer;
                //} else button = new Button();
                button.ID = "btnSearch";
                button.UseSubmitBehavior = true;
                button.Text = resource.GetString("Button_Search");
                button.CommandName = "Search";
                button.CssClass = "btn btn-cons-short btn-primary margin-left-5 " + CssFloatRight;
                Controls.Add(button);
            }
            {
                var button = new Button();
                button.ID = "btnTotals";
                button.Text = resource.GetString("Button_ShowTotals");
                button.CommandName = "ShowTotals";
                button.CssClass = "btn btn-cons-short btn-primary margin-left-5 " + CssFloatRight;
                Controls.Add(button);
            }
        }

        public override void RenderEndTag(System.Web.UI.HtmlTextWriter writer)
        {
            base.RenderEndTag(writer);
            writer.Write("<div class=\"clearfix\"></div>");
        }

        protected override void OnPreRender(System.EventArgs e)
        {
            FindControl("btnNew").Visible = EnableNew;
            FindControl("btnAdd").Visible = EnableAdd;
            FindControl("btnExport").Visible = EnableExport;
            FindControl("btnSave").Visible = EnableSave;
            FindControl("btnDelete").Visible = EnableDelete;
            FindControl("btnBack").Visible = EnableBack;
            (FindControl("btnSearch") as System.Web.UI.WebControls.Button).Enabled = FindControl("btnSearch").Visible = EnableSearch;
            FindControl("btnTotals").Visible = EnableTotals;
            base.OnPreRender(e);
        }

        private void Export_Click(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            if (!EnableExport) return;
            string arg = e.CommandArgument != null ? e.CommandArgument.ToString() : "";
            //(PagedControl as System.Web.UI.IPostBackEventHandler).RaisePostBackEvent("Export$" + arg);
        }
    }
}