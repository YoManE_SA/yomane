﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Controls
{
    public class FilePicker : AutoCompleteBase
    {
        public int? Value { get { return ValueField.Value.ToNullableInt(); } set { ValueField.Value = (this.Value == null ? "" : this.Value.ToString()); } }

        protected override void SearchItems()
        {
            var items = Bll.Accounts.File.AutoComplete(SearchText);
            foreach (var l in items)
            appendItem(l.FileId.ToString(), l.FileName, null, null, null);
        }
    }
}