﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace Netpay.Admin.Controls
{
    public abstract class AutoCompleteBase : Web.Controls.AutoCompleteBase
    {
        public override void RegisterJqueryStartUp()
        {
            GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(this,
                "$('#" + SearchBox.ClientID + "').autocomplete({ " +
                    "minLength: 2," +
                    "autoFocus: false," +
                    "source: function(request, response) { " + Page.ClientScript.GetCallbackEventReference(this, "request.term", "function(arg, context){ context(eval(arg)); }", "response") + "; }, " +
                    "select: function(event, ui) { $('#" + SearchBox.ClientID + "').val(ui.item ? ui.item.label : ''); $('#" + ValueField.ClientID + "').val(ui.item ? ui.item.value : ''); $('#" + IDLabel.ClientID + "').text(ui.item ? ui.item.value : ''); " + OnClientClick + "; return false; } " +
                "}).data('ui-autocomplete')._renderItem = function( ul, item ) { " +
                "	var aItem = $('<a>');" +
                "	if (item.statusColor) aItem.append($('<span style=\"float:left;margin-right:3px;margin-left:3px;background-color:' + item.statusColor + ';\">&nbsp;&nbsp;</span>'));" +
                "	if (item.itemType) aItem.append($('<span style=\"float:right;margin-right:3px;margin-left:3px;padding-right:3px;padding-left:3px;background-color:#c0c0c0;\">' + item.itemType.substr(0, 1) + '<span>'));" +
                "	if (item.imageUrl) aItem.append($('<img style=\"margin-right:3px;margin-left:3px;\" src=\"' + item.imageUrl + ' \" />'));" +
                "	aItem.append($('<span>' + item.label + '</span>'));" +
                "	return $('<li>').attr('data-value', item.value).append(aItem).appendTo(ul); };" +
                "$('#" + SearchBox.ClientID + "').keydown(function(e){ if($.inArray(e.keyCode,[9,13,16,17,18,19,20,27,35,36,37,38,39,40,91,93,224]) > -1) return; $('#" + ValueField.ClientID + "').val(''); $('#" + IDLabel.ClientID + "').text('-');" + (DisableButtonOnKeyDown ? "$('#" + ButtonClientIdToDisable + "').prop('disabled',true);" : "") + "});"
                );
        }
	}
}
