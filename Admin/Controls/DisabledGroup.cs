﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
    [System.Web.UI.ParseChildren(false)]
    public class DisabledGroup : System.Web.UI.WebControls.WebControl
    {
        public string CheckboxID { get; set; }

        public DisabledGroup() : base(System.Web.UI.HtmlTextWriterTag.Div) { }

        protected override void OnPreRender(EventArgs e)
        {
            var ctl = NamingContainer.FindControl(CheckboxID);
            string script = "$('#" + ctl.ClientID + "').bind('click', function(){ $('#" + ClientID + " input').attr('disabled', !this.checked); });";
            GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(this, script);
            base.OnPreRender(e);
        }
    }
}