﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
    public class LegendMultiColors : System.Web.UI.WebControls.WebControl
    {
        public object Items { get; set; }
        public object ItemsText { get; set; }

        public object IsX { get; set; }

        public LegendMultiColors() : base(System.Web.UI.HtmlTextWriterTag.Div) { CssClass = "btn-group"; Title = "Legend"; }
        public string Title { get; set; }
        public bool FloatRight { get; set; }
        private bool _requiresDataBinding = true;
        protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
        {
            if (Items == null) return;
            writer.Write(" <button data-toggle=\"dropdown\" class=\"btn btn-info btn-cons-short dropdown-toggle\"> {0} <span class=\"caret\"></span></button>", Title);
            writer.Write(" <ul class=\"dropdown-menu pull-right\">");
            var dicList = Items as IDictionary;
            var dicItemsText = ItemsText as IDictionary;
            var dicItemsIsX = IsX as IDictionary;
            foreach (System.Collections.DictionaryEntry i in dicList)
            {
                string convertedKey = null;
                if (dicItemsText != null && dicItemsText.Contains(i.Key))
                {
                    convertedKey = (string)dicItemsText[i.Key];
                }
                else
                {
                    convertedKey = i.Key.ToString().Replace('_', ' ');
                }

                bool shouldDrawX = false;
                if (dicItemsIsX != null && dicItemsIsX.Contains(i.Key))
                {
                    shouldDrawX = (bool)dicItemsIsX[i.Key];
                }

                Tuple<System.Drawing.Color, System.Drawing.Color> dicItem = (Tuple<System.Drawing.Color, System.Drawing.Color>)i.Value;
                if (shouldDrawX)
                {
                    writer.Write("<li><a href=\"#\"><span class=\"general-span-wrap-spans\"><span class=\"multi-legend-with-x\" style=\"background:{0};\"></span><span class=\"multi-legend-with-x\" style=\"background:{1};\"></span><span class=\"span-wrap-multi-legend-x\">X</span></span> {2}</a></li>", System.Drawing.ColorTranslator.ToHtml((System.Drawing.Color)dicItem.Item1), System.Drawing.ColorTranslator.ToHtml((System.Drawing.Color)dicItem.Item2), convertedKey);
                }
                else
                {
                    writer.Write("<li><a href=\"#\"><span class=\"legend-multi-list\" style=\"background:{1};\"></span><span class=\"legend-multi-list\" style=\"background:{2};\"></span> {0}</a></li>", convertedKey, System.Drawing.ColorTranslator.ToHtml((System.Drawing.Color)dicItem.Item1), System.Drawing.ColorTranslator.ToHtml((System.Drawing.Color)dicItem.Item2));
                }
            }
            writer.Write(" </ul>");
            base.RenderContents(writer);
        }

        public override void DataBind()
        {
            base.DataBind();
            _requiresDataBinding = false;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (_requiresDataBinding) DataBind();
            if (FloatRight && !CssClass.Contains("pull-right")) CssClass += " pull-right";
            else if (!FloatRight && CssClass.Contains("pull-right")) CssClass = CssClass.Replace("pull-right", "").Trim();
            base.OnPreRender(e);
        }
    }
}