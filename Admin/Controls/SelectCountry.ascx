﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectCountry.ascx.cs" Inherits="Netpay.Admin.Controls.SelectCountry" %>
<div class="row">
    <div class="col-lg-12 margin-bottom-10">
        <asp:Label runat="server" ID="lblValue"  />
    </div>
    <div class="col-lg-12">
         <NP:LinkButton ID="btnChange" CssClass="btn btn-primary" runat="server" Text="Change" OnCommand="Dialog_Command" CommandName="ShowDialog" RegisterAsync="true" />
    </div>
</div>
<asp:HiddenField runat="server" ID="hfValue" />
<admin:ModalDialog runat="server" ID="mdSelect" Title="Select Countries" OnCommand="Dialog_Command" OnDataBinding="Dialog_DataBinding">
    <Body>
        <div class="row">
            <div class="col-lg-5">
                <div class="form-group">
                    Selected:
                    <asp:ListBox runat="server" ID="lbSelected" Rows="20" Width="100%" CssClass="form-control" DataTextField="Name" DataValueField="IsoCode2" SelectionMode="Multiple" />
                </div>
            </div>
            <div class="col-lg-2 text-center" style="padding-top: 100px;">
                <asp:LinkButton runat="server" Text="&lt;" Width="50px" CssClass="btn btn-info" CommandName="Add"><i class="fa fa-angle-left"></i></asp:LinkButton><br />
                <br />
                <asp:LinkButton runat="server" Text="&lt; &lt;" Width="50px" CssClass="btn btn-info" CommandName="AddAll"><i class="fa fa-angle-double-left"></i></asp:LinkButton>
                <br />
                <br />
                <asp:LinkButton runat="server" Text="&gt; &gt;" Width="50px" CssClass="btn btn-info" CommandName="RemoveAll"><i class="fa fa-angle-double-right"></i></asp:LinkButton>
                <br />
                <br />
                <asp:LinkButton runat="server" Text="&gt;" Width="50px" CssClass="btn btn-info" CommandName="Remove"><i class="fa fa-angle-right"></i></asp:LinkButton>
            </div>
            <div class="col-lg-5">
                Choose:
                <asp:ListBox runat="server" CssClass="form-control" ID="lbItems" Rows="20" Width="100%" DataTextField="Name" DataValueField="ID" SelectionMode="Multiple" />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-7 col-lg-5">
                <asp:DropDownList runat="server" ID="ddlItemType" CssClass="form-control" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ItemType_SelectedIndexChanged">
                    <asp:ListItem Text="Countries" Value="0" />
                    <asp:ListItem Text="Groups" Value="1" />
                </asp:DropDownList>
            </div>
        </div>
    </Body>
    <Footer>
        <input type="button" class="btn btn-default" value="Cancel" onclick="<%# mdSelect.HideJSCommand %>" />
        <asp:Button runat="server" class="btn btn-primary" Text="Apply" CommandName="Apply" />
    </Footer>
</admin:ModalDialog>
