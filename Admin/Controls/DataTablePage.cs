﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using System.Web.UI.WebControls;
using System.Collections;
using Netpay.Infrastructure.Tasks;
using Netpay.Web.Controls;

namespace Netpay.Admin.Controls
{
    public class DataTablePage : Page
    {
        private static string[] TabsPriority = new string[] { "Summary", "Data", "Process", "Risk", "Terminals" };

        public Infrastructure.Security.SecuredObject CurrentPageSecuredObject
        {
            get
            {
                switch (TemplateName)
                {
                    case "Merchant":
                        return Bll.Merchants.Merchant.SecuredObject;

                    case "DebitCompany":
                        return Bll.DebitCompanies.DebitCompany.SecuredObject;

                    case "Customer":
                        return Bll.Customers.Customer.SecuredObject;

                    case "Affiliate":
                        return Bll.Affiliates.Affiliate.SecuredObject;

                    default:
                        return null;
                }
            }
        }

        //public Admin.Module CurrentModule { get; set; }
        private Dictionary<System.Type, object> ItemData { get; set; }
        private Dictionary<System.Type, object> FilterData { get; set; }
        public DataPageController PageController { get { return ContentContainer.FindControl("PageController") as DataPageController; } }
        public PanelSection FiltersPanel { get { return PageController.FilterView.ContentTemplateContainer.FindControl("FiltersPanel") as PanelSection; } }
        public PanelSection FormPanel { get { return PageController.FormView.ContentTemplateContainer.FindControl("FormPanel") as PanelSection; } }
        public GlobalControls.JQueryControls.TabControl FormTabs { get { return FormPanel.FindControl("FormTabs") as GlobalControls.JQueryControls.TabControl; } }

        public GlobalControls.JQueryControls.TabView GetTabViewControlById(string id)
        {
            return (FormTabs.FindControl(id) as GlobalControls.JQueryControls.TabView);
        }

        public ActionNotify FormActionNotify { get { return FormPanel.FindControl("ActionNotify") as ActionNotify; } }

        public ActionNotify ListActionNotify { get { return ListViewModalDialog.Body.Controls.OfType<ActionNotify>().Single(); ; } }

        public Netpay.Admin.Controls.ModalDialog ListViewModalDialog { get { return PageController.ListView.ContentTemplateContainer.FindControl("ListViewModalDialog") as ModalDialog; } }
        public Netpay.Admin.Controls.ModalDialog FormViewGlobalModalDialog { get { return FormPanel.FindControl("GlobalModalDialog") as Netpay.Admin.Controls.ModalDialog; } }

        public ActionNotify FormViewGlobalActionNotify { get { return FormViewGlobalModalDialog.Body.Controls.OfType<ActionNotify>().Single(); } }

        public void SetListViewMessage(string message, bool iserror)
        {
            (Page as DataTablePage).ListViewModalDialog.Title = iserror ? "Error" : "Success";
            (Page as DataTablePage).ListActionNotify.SetMessage(message, iserror);
            (Page as DataTablePage).ListViewModalDialog.RegisterShow();
        }

        public void SetFormViewMessage(string message, bool iserror)
        {
            (Page as DataTablePage).FormViewGlobalModalDialog.Title = iserror ? "Error" : "Success";
            (Page as DataTablePage).FormViewGlobalActionNotify.SetMessage(message, iserror);
            (Page as DataTablePage).FormViewGlobalModalDialog.RegisterShow();
        }


        public int? ItemID
        {
            get
            {
                if (PageController.DataKey == null) return null;
                return PageController.DataKey.Value.ToNullableInt();
            }
            set
            {
                PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", ItemID } });
            }
        }

        public DataTablePage(string templateName, string viewName = null, Admin.Module currentmodule = null)
            : base(templateName, viewName, currentmodule)
        {
            FilterData = new Dictionary<Type, object>();
            ItemData = new Dictionary<Type, object>();
            //CurrentModule = currentmodule;
        }

        public T GetFilter<T>() where T : class { object ret = null; if (FilterData.TryGetValue(typeof(T), out ret)) return ret as T; return null; }
        public object GetFilter(string typeName)
        {
            object ret = null;
            Type type = Type.GetType(typeName);
            if (FilterData.TryGetValue(type, out ret)) return ret;
            return null;
        }

        public void SetFilter<T>(T value) where T : class
        {
            FilterData[typeof(T)] = value;
        }

        public T GetItemData<T>() where T : class { object ret = null; if (ItemData.TryGetValue(typeof(T), out ret)) return ret as T; return null; }
        public void SetItemData<T>(T value) where T : class { ItemData[typeof(T)] = value; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ContentContainer.Controls.Add(new DataPageController() { ID = "PageController" });
            PageController.FilterView.ContentTemplateContainer.Controls.Add(new PanelSection() { ID = "FiltersPanel", Title = "Filter", CssClass = "panel panel-default" });
            PageController.FormView.ContentTemplateContainer.Controls.Add(new PanelSection() { ID = "FormPanel", Title = "Data", CssClass = "panel panel-default" });
            FormPanel.Body.Controls.Add(new Web.Controls.ActionNotify() { ID = "ActionNotify" });

            //Add action notify to list section
            PageController.ListView.ContentTemplateContainer.Controls.Add(new ModalDialog() { ID = "ListViewModalDialog" });
            PageController.ListView.ContentTemplateContainer.Controls.OfType<ModalDialog>().SingleOrDefault().Body.Controls.Add(new ActionNotify() { ID = "ListActionNotify" });


            //Create form view global modal dialog 
            FormPanel.Body.Controls.Add(new ModalDialog() { ID = "GlobalModalDialog" });

            //Add to the modal dialog body Action Notify
            FormPanel.Body.Controls.OfType<ModalDialog>().Single().Body.Controls.Add(new ActionNotify());
            FormPanel.Body.Controls.OfType<ModalDialog>().Single().Footer.Controls.Add(new Button { CssClass = "btn btn-cons-short btn-info margin-left-5 pull-right", ID = "SaveMerchant", Text = "Save", CommandName = "SaveDuplicateMerchant", Visible = false });
            FormPanel.Body.Controls.OfType<ModalDialog>().Single().Footer.Controls.Add(new Button { CssClass = "btn btn-cons-short btn-danger margin-left-5 pull-right", ID = "CancelSaveMerchant", Text = "Cancel", Visible = false });


            FormPanel.Body.Controls.Add(new GlobalControls.JQueryControls.TabControl() { ID = "FormTabs", UseJQueryUI = false, TrackSelection = true });
            if (!string.IsNullOrEmpty(TemplateName))
            {
                Admin.Application.Current.raiseInitDataTablePage(this, e);
                FiltersPanel.Footer.Controls.Add(new Controls.DataButtons() { ID = "DataButtons", EnableSearch = true });
                FormPanel.Footer.Controls.Add(new Controls.DataButtons() { ID = "DataButtons", EnableSave = true, EnableDelete = true });
                foreach (GlobalControls.JQueryControls.TabView tab in FormTabs.Tabs)
                    tab.Controls.Add(new System.Web.UI.WebControls.Literal() { Text = "<div style=\"clear:both;\" ></div>", Mode = System.Web.UI.WebControls.LiteralMode.PassThrough });
            }
            //add buttons to master page
            var btnFilters = new Image() { ID = "btnShowFilters", ImageUrl = "~/Images/ViewMode/ShowLeftMode.png" };
            btnFilters.Attributes["style"] = "cursor:pointer;";
            btnFilters.Attributes["onclick"] = "DataTableController_ChangeMode('" + PageController.ClientID + "', 0);";
            SiteMaster.TopButtons.Controls.Add(btnFilters);
            var btnList = new System.Web.UI.WebControls.Image() { ID = "btnShowData", ImageUrl = "~/Images/ViewMode/ShowCenterMode.png" };
            btnList.Attributes["style"] = "cursor:pointer; margin-left: 10px;";
            btnList.Attributes["onclick"] = "DataTableController_ChangeMode('" + PageController.ClientID + "', 2);";
            SiteMaster.TopButtons.Controls.Add(btnList);
            PageController.OnClientViewChange =
                "$('#" + btnFilters.ClientID + "').css('opacity', (toMode == 0 ? '0.3' : ''));" +
                "$('#" + btnList.ClientID + "').css('opacity', (toMode == 3 ? '' : '0.3'));";

            PageController.Error += delegate (object s, EventArgs ex)
            {
                Logger.Log((s as Exception));

                if (s.GetType().Name == "MethodAccessDeniedException")
                {
                    Response.Redirect(Web.WebUtils.ApplicationPath + (s as MethodAccessDeniedException).Message);
                }

                if (s.GetType().Name == "MerchantDuplicateFoundException")
                {
                    FormViewGlobalModalDialog.IsMerchantDuplicateMessage = true;
                    FormViewGlobalModalDialog.Title = "Merchant duplicate found";
                    FormViewGlobalActionNotify.SetException(s as MerchantDuplicateFoundException);
                    FormViewGlobalModalDialog.RegisterShow();
                }
                else
                {
                    FormActionNotify.SetMessage((s as Exception).Message, true);
                }

                //Creat two columns if needed
                if (Page != null && System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
                {
                    GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(PageController.FormView, "CreateTwoColumnsIfNeeded('" + PageController.ClientID + "');");
                }

                PageController.FormView.Update();

                if (!(s.GetType().Name == "MerchantDuplicateFoundException"))
                {
                    if ((Page as Controls.DataTablePage).PageController.Mode == DataPageController.ViewMode.FilterList || (Page as Controls.DataTablePage).PageController.Mode == DataPageController.ViewMode.List)
                    {
                        (Page as Controls.DataTablePage).SetListViewMessage((s as Exception).Message, true);
                        (Page as Controls.DataTablePage).PageController.ListView.Update();
                    }
                }
            };

            PageController.Export += PageController_Export;
        }

        private void PageController_Export(object sender, EventArgs e)
        {
            if ((e as CommandEventArgs).CommandArgument != null && (e as CommandEventArgs).CommandArgument is List<string>)
            {
                string typeName = ((e as CommandEventArgs).CommandArgument as List<string>)[0];
                string assemblyName = ((e as CommandEventArgs).CommandArgument as List<string>)[1];
                string fullName = typeName + "," + assemblyName;
                string nameToShow = typeName.Replace("Netpay", "");
                string searchFiltersTypeName = typeName + "+SearchFilters" + "," + assemblyName;

                //Get the type of the current item needed to be exported
                Type currentItemType = Type.GetType(fullName, false, false);
                if (currentItemType == null) throw new Exception("Item data type:" + nameToShow + "wasn't found:");

                //Get the list name as it will show in the exported files list
                string listName = currentItemType.Name;

                //Get the type of the Search Filters of the current type, used in oreder
                //To get the current filter of the list.
                Type searchFiltersType = Type.GetType(searchFiltersTypeName, false, true);
                if (searchFiltersType == null) throw new Exception("Type of Search filters for:" + nameToShow + "wasn't found.");

                //Get the current datatablepage search filters.
                var currentSearchFilters = GetFilter(searchFiltersTypeName);
                if (currentSearchFilters == null) throw new Exception("DataTablePage Search filters for:" + nameToShow + "wasn't found.");

                //Get the search method of the current item data type.
                System.Reflection.MethodInfo searchMethod = currentItemType.GetMethod("Search", new Type[] { currentSearchFilters.GetType(), typeof(SortAndPage) });
                if (searchMethod == null) throw new Exception("Search method for type:" + nameToShow + "wasn't found");

                List<object> args = new List<object>();
                args.Add(currentSearchFilters);
                args.Add(searchMethod);
                args.Add(currentItemType);
                args.Add(listName);

                string methodName = "ExportFromAdminList";

                //TODO: added for debugging
                var userGroupId = string.Empty;
                if (!string.IsNullOrEmpty(Task.GetUserGroupID(Web.WebUtils.LoggedUser)))
                    userGroupId = Task.GetUserGroupID(Web.WebUtils.LoggedUser);

                #region Logging required to see whats wrong with exporting lists on production #964

                Logger.Log(LogSeverity.Info, LogTag.NetpayAdmin, "OBJNULL: Web.WebUtils.LoggedUser = " + (Web.WebUtils.LoggedUser == null ? "NULL" : "NOT NULL"));
                Logger.Log(LogSeverity.Info, LogTag.NetpayAdmin, "OBJNULL: listName = " + (listName == null ? "NULL" : "NOT NULL:" + listName));
                Logger.Log(LogSeverity.Info, LogTag.NetpayAdmin, "OBJNULL: typeof(Bll.Reports.FiledReports).ToString() = " + (typeof(Bll.Reports.FiledReports).ToString() == null ? "NULL" : "NOT NULL: " + typeof(Bll.Reports.FiledReports).ToString()));
                Logger.Log(LogSeverity.Info, LogTag.NetpayAdmin, "OBJNULL: methodName = " + (methodName == null ? "NULL" : "NOT NULL: " + methodName));

                string strArgsToLog = "";

                if (args != null)
                {
                    foreach (var parg in args)
                    {
                        strArgsToLog += args.ToString() + ";";
                    }
                }

                Logger.Log(LogSeverity.Info, LogTag.NetpayAdmin, "OBJNULL: args = " + (args == null ? "NULL" : "NOT NULL: " + strArgsToLog));

                #endregion

                Task task = new Task(Web.WebUtils.LoggedUser, Task.GetUserGroupID(Web.WebUtils.LoggedUser),
                    "Export from " + listName + " list to csv", LogTag.Reports, "Netpay.Bll", typeof(Bll.Reports.FiledReports).ToString(),
                    methodName, args);
                if (!Netpay.Infrastructure.Application.Queue.IsTaskQueued(task))
                    Netpay.Infrastructure.Application.Queue.Enqueue(task);

                //Move to exported reports page
                Response.Redirect(Web.WebUtils.RootUrl + "Modules/ExportedReports/ExportedReports.aspx");
            }
        }

        public DataButtons FilterButtons { get { return FiltersPanel.Footer.FindControl("DataButtons") as DataButtons; } }
        public DataButtons FormButtons { get { return FormPanel.Footer.FindControl("DataButtons") as DataButtons; } }

        protected override void OnLoad(EventArgs e)
        {
            //ItemID = PageController.DataKey.Value.ToNullableInt();
            base.OnLoad(e);
        }

        public static void DisableAllChidControls(System.Web.UI.Control control)
        {
            if (control.Controls.Count == 0)
            {
                if (control is System.Web.UI.WebControls.LinkButton)
                {
                    (control as System.Web.UI.WebControls.LinkButton).Attributes.Add("disabled", "true");
                }

                else if (control is System.Web.UI.WebControls.HyperLink)
                {
                    (control as System.Web.UI.WebControls.HyperLink).Attributes.Add("disabled", "true");
                }

                else if (control is System.Web.UI.WebControls.FileUpload)
                {
                    (control as System.Web.UI.WebControls.FileUpload).Attributes.Add("disabled", "true");
                    (control as System.Web.UI.WebControls.FileUpload).Attributes.Add("style", "cursor:not-allowed;");
                }

                else if (control is System.Web.UI.WebControls.WebControl)
                {
                    (control as System.Web.UI.WebControls.WebControl).Enabled = false;
                }
                return;
            }
            else
            {
                foreach (System.Web.UI.Control innercontrol in control.Controls)
                {
                    if (innercontrol is System.Web.UI.WebControls.LinkButton)
                    {
                        (innercontrol as System.Web.UI.WebControls.LinkButton).Attributes.Add("disabled", "true");
                    }

                    else if (control is System.Web.UI.WebControls.HyperLink)
                    {
                        (control as System.Web.UI.WebControls.HyperLink).Attributes.Add("disabled", "true");
                    }

                    else if (control is System.Web.UI.WebControls.FileUpload)
                    {
                        (control as System.Web.UI.WebControls.FileUpload).Attributes.Add("disabled", "true");
                        (control as System.Web.UI.WebControls.FileUpload).Attributes.Add("style", "cursor:not-allowed;");
                    }

                    else if (innercontrol is System.Web.UI.WebControls.WebControl)
                    {
                        (innercontrol as System.Web.UI.WebControls.WebControl).Enabled = false;
                    }

                    DisableAllChidControls(innercontrol);
                }
            }
        }


        public System.Web.UI.Control AddControlToForm(string tabName, System.Web.UI.Control control, Module module = null, string validationCode = "", Infrastructure.Security.SecuredObject securedobject = null)
        {
            var tab = FormTabs.FindControl(tabName) as GlobalControls.JQueryControls.TabView;
            if (tab == null)
            {
                tab = new GlobalControls.JQueryControls.TabView()
                {
                    ID = tabName.Replace(' ', '_').Replace('&', '_'),
                    Title = tabName,
                    ValidationCode = validationCode
                };
                FormTabs.Tabs.Add(tab);
                var index = Array.IndexOf(TabsPriority, tabName);
                if (index > -1) tab.Priority = 1000 - index;
            }
            else
            {
                tab.ValidationCode = validationCode;
            }

            //Check if the module that the function was called from is active and the user has read permission.
            if (module != null)
            {
                if (module.IsActive)
                {
                    if (securedobject == null)
                    {
                        tab.Controls.Add(control);
                    }
                    else
                    {
                        if (securedobject.HasPermission(Infrastructure.Security.PermissionValue.Read))
                        {
                            if (!securedobject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
                            {
                                DisableAllChidControls(control);
                            }
                            tab.Controls.Add(control);
                        }
                        else
                        {
                            var nactl = ModuleNotAuthorizedControl;

                            var sourceModule = Admin.Application.Current.Modules.Where(x => (x is CoreBasedModule) && (x as CoreBasedModule).CoreModule.ID == securedobject.ModuleID).SingleOrDefault();
                            if (sourceModule != null)
                            {
                                (nactl as Modules.ModuleNotAuthorized.ModuleNotAuthorized).ModuleName = sourceModule.Name + " - " + securedobject.Name;
                            }
                            (nactl as Modules.ModuleNotAuthorized.ModuleNotAuthorized).Permission = "Read";
                            tab.Controls.Add(nactl);
                        }
                    }
                }
                else
                {
                    //if (!tab.Controls.OfType<System.Web.UI.Control>().Any(x => x.GetType() == ModuleNotActivatedControl.GetType()))
                    {
                        var ctl = ModuleNotActivatedControl;
                        (ctl as Modules.ModuleNotActivated.ModuleNotActivated).ModuleName = module.Name;

                        tab.Controls.Add(ctl);
                    }
                }
            }
            else
            {
                tab.Controls.Add(control);
            }

            control.DataBinding += Control_DataBinding;
            return tab;
        }

        public void RemoveControlFromForm(string tabId)
        {
            // Find the tab control by its id
            var tab = FormTabs.FindControl(tabId) as GlobalControls.JQueryControls.TabView;

            if (tab == null)
                return;

            // Unregister from DataBinding event 
            for (int i = 0; i < tab.Controls.Count; ++i)
            {
                var control = tab.Controls[i];
                control.DataBinding -= Control_DataBinding;
            }

            // Remove the tab
            FormTabs.Tabs.Remove(tab);
        }

        private void Control_DataBinding(object sender, EventArgs e)
        {
            if (sender is Web.Controls.ILoadControlParameters)
            {
                System.Collections.Specialized.NameValueCollection collection = new
                    System.Collections.Specialized.NameValueCollection();
                foreach (var item in PageController.DataKey.Values.Keys)
                    collection.Add(item.ToString(), PageController.DataKey.Values[item].ToString());
                (sender as Web.Controls.ILoadControlParameters).LoadControlParameters(collection);
            }
        }

        public void AddControlToFilter(string tabName, System.Web.UI.Control control)
        {
            FiltersPanel.Body.Controls.Add(control);
        }

        public void AddControlToList(System.Web.UI.Control control)
        {
            PageController.ListView.ContentTemplateContainer.Controls.Add(control);
        }

        private void CollapseAllFilters(System.Web.UI.Control ctl, ref int onCount)
        {
            if (ctl is FilterSection)
            {
                (ctl as FilterSection).Collapsed = (onCount-- <= 0);
            }
            else
            {
                foreach (System.Web.UI.Control ct in ctl.Controls)
                    CollapseAllFilters(ct, ref onCount);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            int showCount = 1;
            CollapseAllFilters(FiltersPanel.Body, ref showCount);
            base.OnPreRender(e);
        }
    }

    public class TemplateControlBase : System.Web.UI.UserControl
    {
        public DataTablePage TemplatePage { get { return Page as DataTablePage; } }
    }

    public class AccountPage : DataTablePage
    {
        public event EventHandler AccountChanged;
        public AccountPage(string templateName, string viewName = null, Admin.Module module = null)
            : base(templateName, viewName, module)
        {
        }

        public virtual Bll.Accounts.Account Account
        {
            get { return GetItemData<Bll.Accounts.Account>(); }
            set
            {
                if (GetItemData<Bll.Accounts.Account>() == value) return;
                SetItemData<Bll.Accounts.Account>(value);
                if (value != null)
                {
                    ItemID = value.AccountID;
                    FormPanel.Title = string.Format("Account #{0} - {1}", value.AccountID, value.AccountName);

                }
                if (AccountChanged != null) AccountChanged(this, EventArgs.Empty);
            }
        }

        public Infrastructure.Security.UserRole? AccountRole
        {
            get
            {
                if (Account == null)
                {
                    return null;
                }
                else
                {
                    switch (Account.AccountType)
                    {
                        case Bll.Accounts.AccountType.Affiliate:
                            return Infrastructure.Security.UserRole.Partner;

                        case Bll.Accounts.AccountType.Customer:
                            return Infrastructure.Security.UserRole.Customer;

                        case Bll.Accounts.AccountType.DebitCompany:
                            return Infrastructure.Security.UserRole.DebitCompany;

                        case Bll.Accounts.AccountType.Merchant:
                            return Infrastructure.Security.UserRole.Merchant;
                        default:
                            return null;
                    }
                }
            }
        }


        protected override void OnInitComplete(EventArgs e)
        {
            PageController.LoadItem += PageController_LoadItem;
            base.OnInitComplete(e);
        }

        private void PageController_LoadItem(object sender, EventArgs e)
        {
            ItemID = PageController.DataKey["AccountID"].ToNullableInt();
            if (ItemID != null)
                Account = Bll.Accounts.Account.LoadObject(ItemID.Value);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (ItemID != null && Account == null) Account = Bll.Accounts.Account.LoadObject(ItemID.Value);
        }

        public void SetStatusColor(System.Drawing.Color color)
        {
            var lt = FormPanel.Header.FindControl("MerchantStatusColor") as System.Web.UI.LiteralControl;
            if (lt == null) FormPanel.Header.Controls.Add(lt = new System.Web.UI.LiteralControl() { ID = "MerchantStatusColor" });
            lt.Text = "<span class=\"legend-item-data\" style=\"background-color:" + System.Drawing.ColorTranslator.ToHtml(color) + ";\">&nbsp;</span>";
        }

    }

    public class AccountControlBase : TemplateControlBase
    {
        public AccountPage AccountPage { get { return Page as AccountPage; } }
        public Bll.Accounts.Account Account { get { return AccountPage.Account; } set { AccountPage.Account = value; } }
        //public Bll.Accounts.AccountType AccountType { get { return AccountPage.ItemID; } }
        public int? AccountID { get { return AccountPage.ItemID; } }
    }
}