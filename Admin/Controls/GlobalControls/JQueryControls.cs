﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Controls.GlobalControls.JQueryControls
{
    public static class JQueryControlHelper 
    {               
        public static string GetJqueryFilePath(System.Web.UI.Page page)
        {
            return Web.Controls.GlobalControls.JQueryControls.JQueryControlHelper.GetJqueryFilePath(page);
        }

        public static void AddAjaxUrlParam(System.Web.UI.Page page, string paramName, string paramValue)
        {
            Web.Controls.GlobalControls.JQueryControls.JQueryControlHelper.AddAjaxUrlParam(page, paramName, paramValue);
        }
        public static void AddAjaxUrlParam(System.Web.UI.Page page, System.Collections.Specialized.NameValueCollection values)
        {
            Web.Controls.GlobalControls.JQueryControls.JQueryControlHelper.AddAjaxUrlParam(page, values);
        }

        public static void RenderHistory(System.Web.UI.Page page)
        {
            Web.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RenderHistory(page);
        }

        public static bool RegisterScript(System.Web.UI.Page page, string jsFile)
        {
            return Web.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(page, jsFile);
        }

        public static bool RegisterCss(System.Web.UI.Page page, string url)
        {
            return Web.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterCss(page, url);
        }

        public static void RegisterJQuery(System.Web.UI.Page page)
        {
            Web.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQuery(page);
        }

        public static void RegisterJQueryStartUp(System.Web.UI.Control control, string script, string scriptName = null)
        {
            RegisterJQuery(control.Page);
            Web.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(control, script, scriptName);
        }
    }

    public class Spinner : System.Web.UI.WebControls.TextBox
    {
        public Spinner()
        {
            CssClass = "smallInput";
            MinimumValue = 0;
            MaximumValue = 10000;
        }

        public int MinimumValue { get; set; }
        public int MaximumValue { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            JQueryControlHelper.RegisterJQueryStartUp(this, "$('#" + ClientID + "').spinner({min: " + MinimumValue + ", max: " + MaximumValue + "});", ClientID + "Load");
            base.OnPreRender(e);
        }
        public int? Value { get { return Text.ToNullableInt(); } set { Text = (value != null ? value.ToString() : ""); } }
    }

    public class NumberInt : System.Web.UI.WebControls.TextBox
    {
        public NumberInt()
        {
            Attributes["type"] = "number";
        }
        public int? Step { get { return Attributes["step"].ToNullableInt(); } set { Attributes["step"] = value != null ? value.ToString() : null; } }
        public int? MinValue { get { return Attributes["minValue"].ToNullableInt(); } set { Attributes["minValue"] = value != null ? value.ToString() : null; } }
        public int? MaxValue { get { return Attributes["maxValue"].ToNullableInt(); } set { Attributes["maxValue"] = value != null ? value.ToString() : null; ; } }
        public int? Value { get { return Text.ToNullableInt(); } set { Text = (value != null ? value.ToString() : ""); } }
    }

    public class DatePicker : System.Web.UI.WebControls.TextBox
    {
        public DatePicker() { }

        protected override void OnPreRender(EventArgs e)
        {
            JQueryControlHelper.RegisterJQueryStartUp(this, "$('#" + ClientID + "').datepicker( { dateFormat: '" + System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern.Replace("yy", "y").Replace("M", "m") + "'}, $.datepicker.regional['he']).on('keydown', function(e){ if (e.which == 13) $(this).closest('form').submit(); });", ClientID + "Load");
            base.OnPreRender(e);
        }
        public DateTime? Value { get { return Text.ToNullableDate(); } set { Text = value != null ? value.Value.ToString("d") : ""; } }
    }

    public class Range<T> : System.Web.UI.WebControls.PlaceHolder, System.Web.UI.INamingContainer where T : System.Web.UI.WebControls.WebControl, new()
    {
        public Range() { 
            XSWidth = 6;
            Controls.Add(new T() { ID = "From", CssClass = "form-control" });
            Controls.Add(new T() { ID = "To", CssClass = "form-control" });
        }
        public T From { get { return FindControl("From") as T; } }
        public T To { get { return FindControl("To") as T; } }
        public int XSWidth { get; set; }

        protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
        {
            From.RenderControl(writer);
            writer.Write("</div></div> <div class=\"col-xs-" + XSWidth + "\"><div class=\"form-group\">To");
            To.RenderControl(writer);
        }
    }

    public class DateRange : Range<DatePicker>
    {
        public Infrastructure.Range<DateTime?> Value { get { return new Infrastructure.Range<DateTime?>() { From = From.Value, To = To.Value }; } set { From.Value = value.From; To.Value = value.To; } }
    };
    public class IntRange : Range<NumberInt>
    {
        public Infrastructure.Range<int?> Value { get { return new Infrastructure.Range<int?>() { From = From.Value, To = To.Value }; } set { From.Value = value.From; To.Value = value.To; } }
    };
    public class DecimalRange : Range<NumberInt>
    {
        public Infrastructure.Range<decimal?> Value { get { return new Infrastructure.Range<decimal?>() { From = From.Value, To = To.Value }; } set { From.Value = (int?)value.From; To.Value = (int?)value.To; } }
    };


    [System.Web.UI.ParseChildren(false)]
    public class TabView : System.Web.UI.WebControls.WebControl
    {
        public int Priority { get; set; }
        public string ValidationCode { get; set; }
        public string Title { get { return (string)ViewState["Title"]; } set { ViewState["Title"] = value; } }
        public TabView() : base(System.Web.UI.HtmlTextWriterTag.Div)
        {
            CssClass = "tab-pane fade";
            ValidationCode = "";
        }

        public TabControl TabControl
        {
            get
            {
                System.Web.UI.Control parent = Parent;
                if (parent is System.Web.UI.WebControls.RepeaterItem) parent = parent.Parent.Parent;
                return parent as TabControl;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            TabControl.Tabs._tabs.Add(this);
            base.OnInit(e);
        }

        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);
            TabControl.Tabs._tabs.Remove(this);
        }
        /*
        protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<div class=\"tab-data\">");
            base.RenderContents(writer);
            writer.Write("</div>");
        }
        */
    }

    public class TabsCollection : IEnumerable<TabView>
    {
        private TabControl _owner;
        internal List<TabView> _tabs;
        public TabView this[int i] { get { return (TabView)_tabs[i]; } }
        public TabsCollection(TabControl owner) { _owner = owner; _tabs = new List<TabView>(); }
        public void Add(TabView v) { _owner.Controls.Add(v); }
        public void AddAt(int index, TabView v) { _owner.Controls.AddAt(index, v); }
        public void Remove(TabView v) { _tabs.Remove(v); _owner.Controls.Remove(v); }

        public IEnumerator<TabView> GetEnumerator() { return _tabs.GetEnumerator(); }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { return _tabs.GetEnumerator(); }
    }

    [System.Web.UI.ParseChildren(typeof(TabView))]
    public class TabControl : System.Web.UI.WebControls.WebControl, System.Web.UI.IPostBackDataHandler
    {
        public TabsCollection Tabs { get; private set; }
        public int? SelectedIndex { get; set; }
        public bool TrackSelection { get; set; }
        public bool UseJQueryUI { get; set; }

        public TabControl()
            : base(System.Web.UI.HtmlTextWriterTag.Div)
        {
            Tabs = new TabsCollection(this);
            TrackSelection = true;
        }

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresPostBack(this);
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            var focusedControl = typeof(System.Web.UI.Page).GetProperty("FocusedControl", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(Page, null) as System.Web.UI.Control;
            if (focusedControl != null)
            {
                for (var cparent = focusedControl; cparent.Parent != null; cparent = cparent.Parent)
                    if (Controls.Contains(cparent.Parent))
                    {
                        SelectedIndex = Controls.IndexOf(cparent.Parent);
                        break;
                    }
            }
            if (TrackSelection) Page.ClientScript.RegisterHiddenField(ClientID + "_SelectedItem", SelectedIndex.HasValue ? SelectedIndex.ToString() : null);
            if (UseJQueryUI)
            {
                string script = "$('#" + ClientID + "').tabs({active:" + SelectedIndex.GetValueOrDefault();
                if (SelectedIndex.GetValueOrDefault() != 0)
                    script += ", activate:function(e, ui) { $('#" + ClientID + "_SelectedItem').val(ui.newTab.index()); }";
                script += "});";
                JQueryControlHelper.RegisterJQueryStartUp(this, script);
            }
            else
            {
                string queryString = Page.Request.QueryString.ToString();
                string validationCode = "";
                string script;
                if (SelectedIndex.HasValue)
                {
                    validationCode = Tabs[SelectedIndex.Value].ValidationCode;
                }
                                
                if (queryString.Contains("OpenTab=true"))
                {
                    script = "$('" + ClientID + "').tabs('show');"
                             + "$(document).on('shown.bs.tab', '#" + ClientID + " li a', function(e) { $('#" + ClientID + "_SelectedItem').val( $(e.target.parentNode).index() );" + validationCode + " } );" 
                             + "var url = document.location.toString();if (url.match('#')){$('.nav-tabs a[href=\"#' + url.split('#')[1] + '\"]').tab('show');}";
                }
                else
                {
                    script = "$('" + ClientID + "').tabs('show');"
                              + "$(document).on('shown.bs.tab', '#" + ClientID + " li a', function(e) { $('#" + ClientID + "_SelectedItem').val( $(e.target.parentNode).index() );" + validationCode + " } );" 
                              + "$('#" + ClientID + " li:eq(" + SelectedIndex.GetValueOrDefault() + ") a').tab('show');";
                }

                JQueryControlHelper.RegisterJQueryStartUp(this, script);
            }
            base.OnPreRender(e);
        }

        bool System.Web.UI.IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection) { return this.OnLoadPostData(postDataKey, postCollection); }
        void System.Web.UI.IPostBackDataHandler.RaisePostDataChangedEvent() { this.OnRaisePostDataChangedEvent(); }
        protected virtual void OnRaisePostDataChangedEvent() { }
        protected virtual bool OnLoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
        {
            if (TrackSelection)
            {
                string selectedItemId = postCollection[ClientID + "_SelectedItem"];
                if (!string.IsNullOrEmpty(selectedItemId))
                    SelectedIndex = selectedItemId.Substring(selectedItemId.LastIndexOf('_') + 1).ToNullableInt();
            }
            return true;
        }
        protected override void AddParsedSubObject(object obj)
        {
            if (obj is TabView) { Tabs.Add(obj as TabView); return; }
            if (obj is System.Web.UI.WebControls.Repeater) { this.Controls.Add(obj as System.Web.UI.Control); return; }
            if (!(obj is System.Web.UI.LiteralControl)) throw new HttpException(string.Format("TabControl cannot have children of type {0}", obj.GetType().Name));
        }

        //protected override System.Web.UI.ControlCollection CreateControlCollection() { return new TabsCollection(this); }

        protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<ul class=\"nav nav-tabs\">");
            foreach (TabView tab in Tabs.OrderByDescending(t => t.Priority))
                writer.Write(string.Format("<li><a data-toggle=\"tab\" href=\"#{0}\">{1}</a></li>", tab.ClientID, tab.Title));
            writer.Write("</ul>");
            writer.Write("<div class=\"col-lg-12\">");
            writer.Write(" <div class=\"tab-content\">");
            base.RenderChildren(writer);
            writer.Write(" </div>");
            writer.Write("</div>");
        }
    }

    [System.Web.UI.ParseChildren(false)]
    public class Button : Web.Controls.GlobalControls.JQueryControls.Button
    {
        protected override void OnPreRender(EventArgs e)
        {            
            JQueryControlHelper.RegisterJQuery(Page);
            base.OnPreRender(e);
        }
    }

    [System.Web.UI.ParseChildren(true, "Items")]
    public class MenuButton : Web.Controls.GlobalControls.JQueryControls.MenuButton
    {
    }

    [System.Web.UI.ParseChildren(false)]
    public class StaticDialog : System.Web.UI.WebControls.WebControl
    {
        public string Title { get; set; }
        public StaticDialog()
            : base(System.Web.UI.HtmlTextWriterTag.Div)
        {
            CssClass = "ui-widget ui-widget-content ui-corner-all staticDialog";
        }

        protected override void OnPreRender(EventArgs e)
        {
            JQueryControlHelper.RegisterJQuery(Page);
            base.OnPreRender(e);
        }

        protected override void RenderContents(System.Web.UI.HtmlTextWriter response)
        {
            response.Write("<div class=\"ui-widget-header ui-corner-all dialogTitle\">" + Title + "</div>");
            response.Write("<div class=\"ui-widget\" style=\"padding:10px;\">");
            base.RenderContents(response);
            response.Write("</div>");
        }
    }

    public class MenuControl : System.Web.UI.WebControls.Menu
    {
        public bool ExcludeJS { get; set; }
        public string RegisterJS { get; set; }
        protected override void OnPreRender(EventArgs e)
        {
            JQueryControlHelper.RegisterJQuery(Page);
            if (!string.IsNullOrEmpty(RegisterJS))
                foreach (var f in RegisterJS.Split(new char[] { ',' }))
                    JQueryControlHelper.RegisterScript(Page, f);
            if (!ExcludeJS)
                JQueryControlHelper.RegisterJQueryStartUp(this, string.Format("$('#{0}').menu();", ClientID));
            //base.OnPreRender(e);
        }

        protected bool isMenuChildOf(System.Web.UI.WebControls.MenuItem child, System.Web.UI.WebControls.MenuItem parent)
        {
            while (child != null)
            {
                if (child == parent) return true;
                child = child.Parent;
            }
            return false;
        }

        protected void RenderLevel(System.Web.UI.HtmlTextWriter writer, System.Web.UI.WebControls.MenuItemCollection items)
        {
            foreach (System.Web.UI.WebControls.MenuItem item in items)
            {
                writer.Write(string.Format("<li><a{2} href=\"{0}\">{1}</a>", ResolveClientUrl(item.NavigateUrl), item.Text, (isMenuChildOf(SelectedItem, item) ? " class=\"active\"" : "")));
                if (item.ChildItems != null && item.ChildItems.Count > 0)
                {
                    writer.Write("<ul class=\"sub\">");
                    RenderLevel(writer, item.ChildItems);
                    writer.Write("</ul>");
                }
                writer.Write("</li>");
            }
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            this.AddAttributesToRender(writer);
            writer.RenderBeginTag(System.Web.UI.HtmlTextWriterTag.Ul);
            RenderLevel(writer, base.Items);
            writer.RenderEndTag();
            //base.Render(writer);
        }
    }

    public class HtmlEdit : System.Web.UI.WebControls.TextBox
    {
        public enum HtmlEditMode { Simple, Advanced }
        public HtmlEditMode EditMode { get; set; }
        public string UploadDir { get; set; }
        public string ContentCss { get; set; }
        public HtmlEdit()
        {
            ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
            EditMode = HtmlEditMode.Advanced;
            TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;
        }

        private string GetFixBodyBugScript()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine("var body = tinyMCE.EditorManager.get('" + ClientID + "').getBody()");
            sb.AppendLine("var html  = $(body).parent();");
            sb.AppendLine("$(html).css('height', '92%');");
            sb.AppendLine("$(body).css('height', '100%');");
            return sb.ToString();
        }

        private string GetStartupScript()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine("tinymce.init({ selector: 'textarea#" + ClientID + "'");

            sb.AppendLine(",theme: 'modern'");
            sb.AppendLine(",plugins: 'advlist,autolink,lists,link,image,charmap,print,preview,hr,anchor,pagebreak,searchreplace,wordcount,visualblocks,visualchars,code,fullscreen,insertdatetime,media,nonbreaking,save,table,contextmenu,directionality,emoticons,template,textcolor'"); /*paste,*/
            if (EditMode == HtmlEditMode.Advanced)
            {
                sb.AppendLine(",toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent'");
                sb.AppendLine(",toolbar2: 'print preview media | forecolor backcolor fontsizeselect emoticons | ltr rtl | link image'");
            }
            else
            {
                sb.AppendLine(",toolbar1: 'undo redo | bold underline italic | link'");
                sb.AppendLine(",menubar: false, statusbar: false");
            }
            sb.AppendLine(",image_advtab: true");
            //sb.AppendLine(",language : 'he_IL'");
            if (!string.IsNullOrEmpty(CssClass)) sb.AppendLine(",body_class: '" + CssClass + "'");
            if (Height.IsEmpty) Height = System.Web.UI.WebControls.Unit.Pixel(350);
            if (!Width.IsEmpty) sb.AppendLine(", width: '" + Width.ToString() + "'");
            if (Height.Type != System.Web.UI.WebControls.UnitType.Percentage)
            {
                if (!Height.IsEmpty) sb.AppendLine(", height: '" + Height.Value + "'");
            }
            else sb.AppendLine(", init_instance_callback: " + ClientID + "_Resize");
            sb.AppendLine(", file_browser_callback: tiny_mce_IZWebFileManager");

            string[] cssItems = System.Configuration.ConfigurationManager.AppSettings["JQEditor_Css"].EmptyIfNull().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < cssItems.Length; i++) cssItems[i] = ResolveClientUrl(cssItems[i]);
            sb.AppendLine(", content_css: '" + string.Join(",", cssItems) + "'");
            if (!string.IsNullOrEmpty(ContentCss)) sb.AppendLine(", content_css: '" + ContentCss + "'");
            sb.AppendLine(" });");
            if (Height.Type == System.Web.UI.WebControls.UnitType.Percentage)
                sb.AppendLine("function " + ClientID + "_Resize() { var ed = tinymce.activeEditor; if(!ed) return; ed.theme.resizeTo(null, Math.max(350, $('#" + ClientID + "_Container').height() - 220)); }; $(window).resize(" + ClientID + "_Resize); ");
            return sb.ToString();
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
            {
                JQueryControlHelper.RegisterJQuery(Page);
                JQueryControlHelper.RegisterScript(Page, "Plugins/tinymce/tinymce.min.js" /*"tiny_mce/jquery.tinymce.js"*/);
                JQueryControlHelper.RegisterScript(Page, "Plugins/Netpay-Plugin/My-tiny_mce-PlugInTo-IZWebFileManager.js");
            }
            base.OnLoad(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Visible) return;
            System.Web.UI.ScriptManager.GetCurrent(Page).RegisterDispose(this, "tinymce.remove('#" + ClientID + "');");
            //if (!System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
			JQueryControlHelper.RegisterJQueryStartUp(this, "setTimeout(function() { " + GetStartupScript() + "}, 100);", ClientID + "Load");

            JQueryControlHelper.RegisterJQueryStartUp(this, "setTimeout(function() { " + GetFixBodyBugScript() + "}, 1000);", ClientID + "FixBug");

            //else System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), UniqueID + "Add", "setTimeout(function() { tinymce.execCommand('mceAddEditor', true, '" + ClientID + "'); }, 100);", true);
            if (System.Web.UI.ScriptManager.GetCurrent(Page).GetRegisteredOnSubmitStatements().Where(s => s.Key == "Editor_triggerSave").FirstOrDefault() == null)
                System.Web.UI.ScriptManager.RegisterOnSubmitStatement(Page, Page.GetType(), "Editor_triggerSave", "tinymce.triggerSave();");
            base.OnPreRender(e);
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            //writer.Write("<script type=\"text/javascript\">" + GetStartupScript() + "</script>");
            if (Height.Type == System.Web.UI.WebControls.UnitType.Percentage)
                writer.Write("<div id=\"{0}_Container\" style=\"width:{1};height:{2};\">", ClientID, Width.IsEmpty ? "" : Width.ToString(), Height.ToString());
            base.Render(writer);
            if (Height.Type == System.Web.UI.WebControls.UnitType.Percentage)
                writer.Write("</div>");
        }
    }

    public class Graph : System.Web.UI.WebControls.WebControl
    {
        public enum GraphType { Line = 0, Bar = 1, Radar = 2, PolarArea = 3, Pie = 4, Doughnut = 5 };
        public class GraphSeries
        {
            public string Name { get; set; }
            public System.Drawing.Color? Color { get; set; }
            public System.Drawing.Color? StrokeColor { get; set; }
            public System.Drawing.Color? PointColor { get; set; }
            public System.Drawing.Color? PointStrokeColor { get; set; }
            public List<decimal> Data { get; set; }
        }
        public List<GraphSeries> Series { get; private set; }
        public List<string> Labels { get; set; }
        public GraphType Type { get; set; }
        public Graph() : base("canvas") { Series = new List<GraphSeries>(); }

        protected override void OnPreRender(EventArgs e)
        {
            int fullOpacity = unchecked((int)(uint)0x58000000);
            JQueryControlHelper.RegisterJQuery(Page);
            JQueryControlHelper.RegisterScript(Page, "Chart/Chart.js");
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine("var myGraphData = { ");
            if (Type < GraphType.PolarArea)
            {
                if (Labels != null) sb.AppendFormat(" labels: [{0}], \r\n", string.Join(",", Labels.Select(s => string.Format("'{0}'", s))));
                sb.AppendLine(" datasets: [");
            }
            else sb.Remove(sb.Length - 4, 4).AppendLine("[ ");
            int sIndex = 0;
            foreach (var s in Series)
            {
                if (s.Color == null) s.Color = System.Drawing.Color.FromArgb(new Random(sIndex++).Next(0x00FFFFFF) | fullOpacity);
                sb.Append("{");
                while (s.Data.Count < Labels.Count) s.Data.Add(0m);
                if (Type >= GraphType.PolarArea)
                {
                    sb.AppendFormat(" value: {0}", s.Data.Average());
                    if (s.Color != null) sb.AppendFormat(", color: '{0}'", s.Color.Value.ToHtmlRGBA());
                }
                else
                {
                    sb.AppendFormat(" data: [{0}]", string.Join(", ", s.Data));
                    if (s.Color != null) sb.AppendFormat(", fillColor: '{0}'", s.Color.Value.ToHtmlRGBA());
                    if (s.StrokeColor != null) sb.AppendFormat(", strokeColor: '{0}'", s.StrokeColor.Value.ToHtmlRGBA());
                    if (s.PointColor != null) sb.AppendFormat(", pointColor: '{0}'", s.PointColor.Value.ToHtmlRGBA());
                    if (s.PointStrokeColor != null) sb.AppendFormat(", pointStrokeColor: '{0}'", s.PointStrokeColor.Value.ToHtmlRGBA());
                }
                sb.AppendLine(" }, ");
            }
            if (Series.Count > 0) sb = sb.Remove(sb.Length - 4, 4);
            if (Type < GraphType.PolarArea) sb.AppendLine("]};");
            else sb.AppendLine("];");
            sb.AppendLine("new Chart(document.getElementById('" + ClientID + "').getContext('2d'))." + Type + "(myGraphData);");

            if (Series.Count > 0) JQueryControlHelper.RegisterJQueryStartUp(this, sb.ToString());
            base.OnPreRender(e);
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<div>");
            foreach (var s in Series)
                writer.Write("<span><span style=\"background-color:{0}\">&nbsp;&nbsp;</span>&nbsp;{1}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", s.Color.Value.ToHtmlRGBA(), s.Name);
            writer.Write("</div>");
            base.Render(writer);
        }

        protected override void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
        {
            writer.AddStyleAttribute("direction", "ltr");
            if (!Width.IsEmpty) writer.AddAttribute("width", Width.Value.ToString());
            if (!Height.IsEmpty) writer.AddAttribute("height", Height.Value.ToString());
            base.AddAttributesToRender(writer);
        }

    }
}