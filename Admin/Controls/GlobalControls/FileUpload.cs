﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI;
using System.IO;

namespace Netpay.Admin.Controls.GlobalControls
{
    public class FileUpload : System.Web.UI.WebControls.CompositeControl, System.Web.UI.IPostBackDataHandler
    {
        public event EventHandler Uploaded;
        public event EventHandler Download;
        public event EventHandler Delete;

        public Stream FileStream
        {
            get
            {
                return new MemoryStream(FileBytes);
            }
        }

        public bool? IsDownloadButtonVisible { get; set; } 
        public string AcceptTypes { get; set; }
        public string LocalPath { get; set; }
        public bool IsTemporary { get { if (string.IsNullOrEmpty(LocalPath)) return false; return LocalPath.StartsWith(Infrastructure.Domain.Current.MapTempPath()); } }
        public bool HasFile { get { return !string.IsNullOrEmpty(LocalPath); } }
        public bool HasNewFile { get { return UploadField.HasFile || IsTemporary; } }
        public string FileName { get { return FileNameField.Text; } protected set { FileNameField.Text = value; } }
        public bool ReadOnly { get { return UploadButton.Visible; } set { UploadButton.Visible = !value; } }
        public bool EnableDelete { get; set; }

        protected System.Web.UI.WebControls.FileUpload UploadField { get { return FindControl("Control") as System.Web.UI.WebControls.FileUpload; } }
        protected System.Web.UI.WebControls.TextBox FileNameField { get { return FindControl("FileName") as System.Web.UI.WebControls.TextBox; } }
        protected System.Web.UI.WebControls.LinkButton UploadButton { get { return FindControl("Upload") as System.Web.UI.WebControls.LinkButton; } }
        protected System.Web.UI.WebControls.LinkButton DownloadButton { get { return FindControl("Download") as System.Web.UI.WebControls.LinkButton; } }
        protected System.Web.UI.WebControls.LinkButton DeleteButton { get { return FindControl("Delete") as System.Web.UI.WebControls.LinkButton; } }

        public FileUpload()
        {
            System.Web.UI.Control groupContriner, spanContriner, lastCreated = null;
            Controls.Add(lastCreated = new System.Web.UI.WebControls.FileUpload() { ID = "Control", Width = System.Web.UI.WebControls.Unit.Pixel(200), AllowMultiple = true });
            UploadField.Style.Add("visibility", "hidden");

            Controls.Add(groupContriner = new System.Web.UI.WebControls.WebControl(HtmlTextWriterTag.Div) { CssClass = "input-group" });
            groupContriner.Controls.Add(spanContriner = new System.Web.UI.WebControls.WebControl(HtmlTextWriterTag.Span) { CssClass = "input-group-btn" });
            spanContriner.Controls.Add(lastCreated = new System.Web.UI.WebControls.LinkButton() { ID = "Upload", CausesValidation = false, CommandName = "File_Upload", CssClass = "btn btn-primary btn-FileType" });
            lastCreated.Controls.Add(new System.Web.UI.LiteralControl("<i class=\"fa fa-upload\"></i> " + Resources.DataActions.Button_Upload));
            groupContriner.Controls.Add(lastCreated = new System.Web.UI.WebControls.TextBox() { ID = "FileName", Enabled = false, CssClass = "form-control" });

            groupContriner.Controls.Add(spanContriner = new System.Web.UI.WebControls.WebControl(HtmlTextWriterTag.Span) { CssClass = "input-group-btn" });
            spanContriner.Controls.Add(lastCreated = new System.Web.UI.WebControls.LinkButton() { ID = "Download", CausesValidation = false, CommandName = "File_Download", CssClass = "btn btn-primary btn-FileType"});
            lastCreated.Controls.Add(new System.Web.UI.LiteralControl("<i class=\"fa fa-download\"></i> " + Resources.DataActions.Button_Download));
            spanContriner.Controls.Add(lastCreated = new System.Web.UI.WebControls.LinkButton() { ID = "Delete", CausesValidation = false, CommandName = "File_Delete", CssClass = "btn btn-danger btn-FileType" });
            lastCreated.Controls.Add(new System.Web.UI.LiteralControl("<i class=\"fa fa-trash-o\"></i> " + Resources.DataActions.Button_Delete));
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection) { return true; }
        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            LocalPath = Page.Request.Form[ClientID + "_LocalPath"];
            string reqData = Page.Request.Form[ClientID + "_FileData"];
            if (!string.IsNullOrEmpty(reqData))
            {
                DeleteTemporaryFile();
                var newFileName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(FileNameField.Text);
                LocalPath = Infrastructure.Domain.Current.MapTempPath(newFileName);
                System.IO.File.WriteAllBytes(LocalPath, ExtractDataProtocol(reqData));
                if (Uploaded != null) Uploaded(this, EventArgs.Empty);
                //System.Web.UI.ScriptManager.RegisterHiddenField(Page, ClientID + "_FileData", null);
                //System.Web.UI.ScriptManager.RegisterHiddenField(Page, ClientID + "_LocalPath", LocalPath);
            }
        }

        protected override void DataBindChildren()
        {
            base.DataBindChildren();
            FileName = string.IsNullOrEmpty(LocalPath) ? "" : System.IO.Path.GetFileName(LocalPath);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.RegisterRequiresPostBack(this);
            System.Web.UI.ScriptManager.GetCurrent(Page).RegisterPostBackControl(DownloadButton);
        }

        protected override void OnPreRender(EventArgs e)
        {
            System.Web.UI.ScriptManager.RegisterHiddenField(Page, ClientID + "_FileData", null);
            System.Web.UI.ScriptManager.RegisterHiddenField(Page, ClientID + "_LocalPath", LocalPath);

            if (!string.IsNullOrEmpty(AcceptTypes)) UploadField.Attributes["accept"] = AcceptTypes;
            UploadField.Attributes["onchange"] = "if (this.files[0].size > 5242880) {alert(\"The file it too big.\"); document.getElementById('" + UploadField.ClientID + "').value = ''; return false; }  var reader = new FileReader(); reader.onloadend = function() { document.getElementById('" + ClientID + "_FileData').value = reader.result; }; document.getElementById('" + FileNameField.ClientID + "').value = document.getElementById('" + UploadField.ClientID + "').files[0].name; reader.readAsDataURL(document.getElementById('" + UploadField.ClientID + "').files[0]); return false;";
            UploadButton.OnClientClick = $"$('#{UploadField.ClientID}').click(); return false;";
            bool hasFile = HasFile;
            if (IsDownloadButtonVisible == null || IsDownloadButtonVisible.Value)
            { DownloadButton.Visible = hasFile; }
            else
            {
                DownloadButton.Visible = false;
            }

            DeleteButton.Visible = hasFile && EnableDelete;
            base.OnPreRender(e);
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            var cmde = args as System.Web.UI.WebControls.CommandEventArgs;
            if (cmde != null)
            {
                switch (cmde.CommandName)
                {
                    case "File_Upload":
                        if (Delete != null)
                        {
                            Delete(this, args);
                            return true;
                        }
                        break;
                    case "File_Download":
                        if (Download != null) Download(this, args);
                        else Download_Click(this, args);
                        return true;
                }
            }
            return base.OnBubbleEvent(source, args);
        }

        private void Download_Click(object sender, EventArgs e)
        {
            string fileName = LocalPath;
            if (string.IsNullOrEmpty(fileName)) return;

            //Get the mimeType of the file, if no mimeType found - use the generic application/octet-stream type.
            string mimeType = System.Web.MimeMapping.GetMimeMapping(System.IO.Path.GetFileName(fileName));
            if (string.IsNullOrEmpty(mimeType)) mimeType = "application/octet-stream";

            Page.Response.Clear();
            Page.Response.ContentType = mimeType; 
            Page.Response.Headers.Add("Content-Description", "File Transfer");
            Page.Response.Headers.Add("Content-Transfer-Encoding", "binary");
            Page.Response.Headers.Add("Expires", "0");
            Page.Response.Headers.Add("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            Page.Response.Headers.Add("Content-Disposition", "attachment; filename=" + "\"" + System.IO.Path.GetFileName(fileName) + "\"");
            Page.Response.WriteFile(fileName);
            Page.Response.End();
        }

        private byte[] ExtractDataProtocol(string data)
        {
            int startPos = 0;
            if (data.StartsWith("data:")) startPos = 5;
            int typeIndex = data.IndexOf(';', startPos);
            if (typeIndex < 0) return null;
            string mimeType = data.Substring(startPos, typeIndex - startPos);
            int encodingEndIndex = data.IndexOf(',', typeIndex + 1);
            if (encodingEndIndex < 0) throw new Exception("unsupported file encoding");
            return System.Convert.FromBase64String(data.Substring(encodingEndIndex + 1));
        }

        public byte[] FileBytes
        {
            get
            {
                var path = LocalPath;
                if (string.IsNullOrEmpty(path)) return null;
                return System.IO.File.ReadAllBytes(path);
            }
        }

        public void DeleteTemporaryFile()
        {
            if (IsTemporary)
            {
                if (System.IO.File.Exists(LocalPath)) System.IO.File.Delete(LocalPath);
                LocalPath = null;
            }
        }

        public string SaveAs(string fileName = null)
        {
            if (System.IO.File.Exists(fileName)) System.IO.File.Delete(fileName);
            if (IsTemporary) System.IO.File.Move(LocalPath, fileName);
            else System.IO.File.Copy(LocalPath, fileName);
            LocalPath = fileName;
            FileName = System.IO.Path.GetFileName(fileName);
            return fileName;
        }
    }
}