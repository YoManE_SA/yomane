﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls.GlobalControls
{
	public class Button : System.Web.UI.WebControls.Button
    {
        public bool? RegisterAsync { get; set; }
        protected override void OnLoad(EventArgs e)
        {
            if (RegisterAsync != null)
            {
                if (RegisterAsync.Value) System.Web.UI.ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(this);
                else System.Web.UI.ScriptManager.GetCurrent(Page).RegisterPostBackControl(this);
            }
            base.OnLoad(e);
        }
    }

	public class LinkButton : System.Web.UI.WebControls.LinkButton
    {
        public bool? RegisterAsync { get; set; }
        protected override void OnLoad(EventArgs e)
        {
            if (RegisterAsync != null)
            {
                if (RegisterAsync.Value) System.Web.UI.ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(this);
                else System.Web.UI.ScriptManager.GetCurrent(Page).RegisterPostBackControl(this);
            }
            base.OnLoad(e);
        }
    }

    public class UpdatePanel : Netpay.Web.Controls.GlobalControls.UpdatePanel
    {
        public UpdatePanel()
        {
            UpdatePanelBlockJsPath = "Plugins/Netpay-Plugin/";
        }

        protected override void OnPreRender(EventArgs e)
        {
            JQueryControls.JQueryControlHelper.RegisterJQuery(Page);
            base.OnPreRender(e);
        }
    }
}