﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Bll;
using Netpay.Infrastructure;

namespace Netpay.Admin.Controls.GlobalControls
{
	public class PagedRepeater : System.Web.UI.WebControls.Repeater, ISortAndPage, System.Web.UI.IPostBackEventHandler, System.Web.UI.IPostBackDataHandler, System.Web.UI.IAttributeAccessor
	{
        public event EventHandler PageChanged;
		public event System.Web.UI.WebControls.RepeaterCommandEventHandler SelectItem;
		public event EventHandler SetDataSource;
		public string CssClass { get; set; }
		public System.Web.UI.AttributeCollection Attributes { get; private set; }
		//paging
		public string SortKey { get; set; }
		public bool SortDesc { get; set; }
		public int PageSize { get; set; }
		public int PageCount { get { return (int)System.Math.Ceiling((double)(this as ISortAndPage).RowCount / (double)PageSize); } }
		public int PageCurrent { get { return ViewState["PageCurrent"].ToNullableInt().GetValueOrDefault(0); } set { ViewState["PageCurrent"] = value; } }
		int ISortAndPage.RowCount { get { return ViewState["RowCount"].ToNullableInt().GetValueOrDefault(0); } set { ViewState["RowCount"] = value; } }
		public int RowCount { get { return ((ISortAndPage)this).RowCount; } }
		public bool CountOnly { get; set; }
		public Func<ISortAndPage, System.Collections.IEnumerable> DataFunction { get; set; }


        [System.Web.UI.ParseChildren(false)]
        public class EmptyContainerControl : System.Web.UI.Control { public EmptyContainerControl() { } }

        [System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public EmptyContainerControl EmptyContainer { get; set; }

		protected virtual System.Web.UI.HtmlTextWriterTag TagKey { get; set; }

		string System.Web.UI.IAttributeAccessor.GetAttribute(string key) { return Attributes[key]; }
		void System.Web.UI.IAttributeAccessor.SetAttribute(string key, string value) { this.Attributes[key] = value; }

		public PagedRepeater()
		{
			TagKey = System.Web.UI.HtmlTextWriterTag.Div;
			Attributes = new System.Web.UI.AttributeCollection(new System.Web.UI.StateBag(true));
			PageSize = 10;
		}
        protected override void OnLoad(EventArgs e)
        {
            if ((Page.IsPostBack && !IsViewStateEnabled) && Page.Request.Form["__EVENTTARGET"].EmptyIfNull().StartsWith(UniqueID)) //viewState disabled and target is child of repeater
                DataBind(); 

            base.OnLoad(e);
        }

		void System.Web.UI.IPostBackEventHandler.RaisePostBackEvent(string eventArgument) { this.OnRaisePostBackEvent(eventArgument); }
		bool System.Web.UI.IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection) { return this.OnLoadPostData(postDataKey, postCollection); }
		void System.Web.UI.IPostBackDataHandler.RaisePostDataChangedEvent() { this.OnRaisePostDataChangedEvent(); }

		protected virtual void OnRaisePostDataChangedEvent() { }
		protected virtual bool OnLoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			if (!IsViewStateEnabled)
			{
				var pageValue = Context.Request[UniqueID];
				if (!string.IsNullOrEmpty(pageValue)) OnPageChanging(pageValue.ToNullableInt().GetValueOrDefault() - 1);
			}
			return true;
		}

		protected virtual void OnRaisePostBackEvent(string eventArgument)
		{
			if (eventArgument.StartsWith("Page$"))
				OnPageChanging(eventArgument.Substring(5).ToNullableInt().GetValueOrDefault() - 1);
			if (eventArgument.StartsWith("Select$")) {
				var itemIndex = eventArgument.Substring(7).ToNullableInt().GetValueOrDefault();
				if (SelectItem != null) SelectItem(this, new System.Web.UI.WebControls.RepeaterCommandEventArgs(Items[itemIndex], this, new System.Web.UI.WebControls.CommandEventArgs("Select", null)));
			}
		}

		public string GetSelectItemEventReference(int itemIndex)
		{
			return Page.ClientScript.GetPostBackEventReference(this, "Select$" + itemIndex, false);
		}
		/*
		protected virtual bool OnLoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection) 
		*/
		public virtual void OnPageChanging(int newPageId)
		{
			if (PageCurrent == newPageId) return;
			PageCurrent = newPageId;
			DataBind();
            PageChanged?.Invoke(this, EventArgs.Empty);
        }

		public override void DataBind()
		{
			if (SetDataSource != null) SetDataSource(this, EventArgs.Empty);
            base.DataBind();
            if (EmptyContainer != null) Controls.Add(EmptyContainer);
		}

		public void DataLoad()
		{
			PageCurrent = 0;
			DataBind();
		}

		protected virtual void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
		{
			writer.AddAttribute("id", ClientID);
			foreach (string k in Attributes.Keys)
				writer.AddAttribute(k, Attributes[k]);
			if (!string.IsNullOrEmpty(CssClass)) writer.AddAttribute("class", CssClass);
		}

		public virtual void RenderBeginTag(System.Web.UI.HtmlTextWriter writer)
		{
			AddAttributesToRender(writer);
			writer.RenderBeginTag(TagKey);
		}

		public virtual void RenderEndTag(System.Web.UI.HtmlTextWriter writer)
		{
			writer.RenderEndTag();
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if (!IsViewStateEnabled)
			{
				writer.Write("<input type=\"hidden\" name=\"{0}\" value=\"{1}\" />", UniqueID, PageCurrent + 1);
				//System.Web.UI.ScriptManager.RegisterHiddenField(this, UniqueID, Items.Count.ToString());
			}
            RenderBeginTag(writer);
            if (EmptyContainer != null) EmptyContainer.Visible = (Items.Count == 0);
            if (Items.Count > 0) base.Render(writer);
            else if (EmptyContainer != null) EmptyContainer.RenderControl(writer);
            RenderEndTag(writer);
        }

		protected override void OnPreRender(EventArgs e)
		{
			//if (this.RequiresDataBinding) DataBind();
			base.OnPreRender(e);
		}
	}
}