﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Controls.GlobalControls
{
    public class SearchButton : System.Web.UI.WebControls.Button
    {
        public string PrefixFilter { get; set; }
        public string FormContainer { get; set; }
        public string InvokeUrlParam { get; set; } //not postback
        public bool AjaxMode { get; set; }
        public bool IsTriggered { get; set; }
        public string Target { get; set; }
        private System.Collections.Specialized.NameValueCollection realControlNames;
        protected override void OnInit(EventArgs e)
        {
            InvokeUrlParam = "Search" + Page.Items["SearchButtonCount"].ToNullableInt();
            Page.Items["SearchButtonCount"] = Page.Items["SearchButtonCount"].ToNullableInt().GetValueOrDefault() + 1;
            if (FormContainer == null)
            {
                if (NamingContainer is Controls.DataButtons)
                    FormContainer = Parent.Parent.ID;
                else FormContainer = Parent.ID;
            }
            if (IsEnabled)
            {
                IsTriggered = false;
                if (Page.IsPostBack)
                {
                    if (string.IsNullOrEmpty(InvokeUrlParam)) IsTriggered = (Context.Request.QueryString["__EVENTTARGET"] == UniqueID);
                }
                if (!string.IsNullOrEmpty(InvokeUrlParam))
                {
                    IsTriggered = Context.Request.QueryString[InvokeUrlParam] != null;
                    if (IsTriggered) Page.LoadComplete += new EventHandler(Page_LoadComplete);
                }
                if (IsTriggered) Page_PreLoad(this, e);
            }
            base.OnInit(e);
        }

        private void Page_LoadComplete(object sender, EventArgs e) //for not postback
        {
            if (!IsEnabled) return;
            var leftOvers = new System.Collections.Specialized.NameValueCollection();
            var lstKeys = realControlNames.Keys.Cast<string>().OrderBy(s => s);
            foreach (string key in lstKeys)
            {
                var pbh = (Page.FindControl(key) as System.Web.UI.IPostBackDataHandler);
                if (pbh != null) pbh.RaisePostDataChangedEvent();
                else leftOvers.Add(key, realControlNames[key]);
            }

            foreach (string key in leftOvers.Keys)
            {
                var pbh = (Page.FindControl(key) as System.Web.UI.IPostBackDataHandler);
                if (pbh != null) pbh.LoadPostData(key, realControlNames);
            }

            foreach (string key in leftOvers.Keys)
            {
                var pbh = (Page.FindControl(key) as System.Web.UI.IPostBackDataHandler);
                if (pbh != null) pbh.RaisePostDataChangedEvent();
            }

            int selectedIndex = -1;
            bool hasSelectedIndex = int.TryParse(Context.Request.QueryString["SelectedIndex"], out selectedIndex);
            hasSelectedIndex &= selectedIndex >= 0;


            if (!hasSelectedIndex && Context.Request.QueryString[InvokeUrlParam] == "1" && !Page.IsPostBack)
                RaisePostBackEvent(null);
        }

        private System.Web.UI.Control GetFormContainer
        {
            get
            {
                System.Web.UI.Control formContainer = null;
                for (var nmct = NamingContainer; nmct != null && nmct.Parent != null; nmct = nmct.NamingContainer)
                {
                    formContainer = nmct.FindControl(FormContainer);
                    if (formContainer != null)
                    {
                        if (formContainer is System.Web.UI.INamingContainer) return formContainer;
                        return formContainer.NamingContainer;
                    }
                }
                return null;
            }
        }

        void Page_PreLoad(object sender, EventArgs e)
        {
            string fullPrefix = (GetFormContainer.UniqueID + "$");
            realControlNames = new System.Collections.Specialized.NameValueCollection();
            foreach (string key in Context.Request.QueryString.Keys)
            {
                string fullName = fullPrefix + key.Replace('.', '$');
                realControlNames.Add(fullName, Context.Request.QueryString[key]);
            }
            var lstKeys = realControlNames.Keys.Cast<string>().OrderBy(s => s);
            foreach (string key in lstKeys)
            {
                var pbh = (Page.FindControl(key) as System.Web.UI.IPostBackDataHandler);
                if (pbh != null) pbh.LoadPostData(key, realControlNames);
            }
        }

        public string GetQueryString()
        {
            var container = GetFormContainer;
            var sb = new System.Text.StringBuilder();
            GetQueryStringInternal(sb, container, container);
            sb.AppendFormat("{0}=1", InvokeUrlParam);
            return sb.ToString();
        }

        private void GetQueryStringInternal(System.Text.StringBuilder sb, System.Web.UI.Control container, System.Web.UI.Control parent)
        {
            foreach (System.Web.UI.Control c in parent.Controls)
            {
                string objName = c.UniqueID.Substring(container.UniqueID.Length + 1).Replace('$', '.');
                string value = null;
                if (c is System.Web.UI.WebControls.TextBox)
                {
                    value = (c as System.Web.UI.WebControls.TextBox).Text;
                }
                else if (c is System.Web.UI.WebControls.CheckBoxList)
                {
                    for (int i = 0; i < (c as System.Web.UI.WebControls.ListControl).Items.Count; i++)
                    {
                        var li = (c as System.Web.UI.WebControls.ListControl).Items[i]; //c System.Web.UI.WebControls.ListItem 
                        if (li.Selected) sb.AppendFormat("{0}${1}={2}&", objName, i, HttpUtility.UrlEncode(li.Value));
                    }
                }
                else if (c is System.Web.UI.WebControls.ListControl)
                {
                    value = (c as System.Web.UI.WebControls.ListControl).SelectedValue;
                }
                else if (c is System.Web.UI.WebControls.CheckBox)
                {
                    var ctl = (c as System.Web.UI.WebControls.CheckBox);
                    if (ctl.Checked) value = "on";
                }
                if (!string.IsNullOrEmpty(value)) sb.AppendFormat("{0}={1}&", objName, HttpUtility.UrlEncode(value));
                if (c.HasControls()) GetQueryStringInternal(sb, container, c);
            }
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            IsTriggered = true;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!AjaxMode)
            {
                JQueryControls.JQueryControlHelper.RegisterJQuery(Page);
                string script =
                    "function SearchButtonClick(source, prefix, postbackUrl, invokeUrlParam, isNewWindow) {\r\n" +
                    "   var submitParams = '';\r\n" +
                    "   var includeParams = window.SubmitButtonIncludes || new Array();\r\n" +
                    "   $('input,textarea,select').each(function() {\r\n" +
                    "       var inputName = $(this).attr('name');\r\n" +
                    "       if (inputName && inputName != '') {\r\n" +
                    "           var includeCurrent = includeParams.indexOf(inputName) > -1;\r\n" +
                    "           if (!includeCurrent && inputName.slice(0, prefix.length) == prefix){\r\n" +
                    "               inputName = inputName.substring(prefix.length);\r\n" +
                    "               includeCurrent = true;" +
                    "           }\r\n" +
                    "           if (includeCurrent) {\r\n" +
                    "               var value = $(this).val();\r\n" +
                    "               var inputType = $(this).attr('type');\r\n" +
                    "               if ((inputType == 'checkbox' || inputType == 'radio') && !this.checked) value = null;\r\n" +
                    "               if (this != source && value && value != '')\r\n" +
                    "                   submitParams += '&' + encodeURIComponent(inputName.replace(/\\$/g, '.')) + '=' + encodeURIComponent(value);\r\n" +
                    "           }\r\n" +
                    "       }\r\n" +
                    "   });\r\n" +
                    "   if (invokeUrlParam && invokeUrlParam != '') submitParams += '&' + invokeUrlParam + '=1';\r\n" +
                    "   else submitParams += '&__EVENTTARGET=' + encodeURIComponent($(source).attr('name'));\r\n" +
                    "   submitParams = (postbackUrl && postbackUrl != '' ? postbackUrl : window.location.href.split('?')[0]) + '?' + submitParams.substr(1);\r\n" +
                    "   if (isNewWindow == '_blank') window.open(submitParams);" +
                    "   else document.location.href = submitParams;\r\n" +
                    "   return false;" +
                    "}";
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, GetType(), "SearchButton", script, true);
                if (OnClientClick == null) OnClientClick = "";
                OnClientClick += ";return SearchButtonClick(this, '" + GetFormContainer.UniqueID + "$" + PrefixFilter + "', '" + (!string.IsNullOrEmpty(PostBackUrl) ? ResolveUrl(PostBackUrl) : "") + "','" + InvokeUrlParam + "', '" + Target + "')";
            }
            else
            {
                if (System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
                    GlobalControls.JQueryControls.JQueryControlHelper.AddAjaxUrlParam(Page, HttpUtility.ParseQueryString(GetQueryString()));
                //if (IsTriggered) 
                //Website.Controls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(Page, "history.replaceState(null, null, '" + Context.Request.Url.LocalPath + "?" + GetQueryString() + "');");
                //System.Web.UI.ScriptManager.GetCurrent(Page).ur
                //Website.Controls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(Page, "location.hash = '" + GetQueryString() + "';");
            }
            base.OnPreRender(e);
        }
    }

}