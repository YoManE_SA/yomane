﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
    [System.Web.UI.ParseChildren(true)]
	public class PanelSection : Web.Controls.Dialog
    {
        public PanelSection() 
		{ 
            CssClass = "panel panel-default";
            Controls.Add(new PanelContainer() { ID = "Header", CssClass = "panel-heading" });
			Header.Controls.Add(new System.Web.UI.LiteralControl() { ID = "TitleHtml", ViewStateMode = System.Web.UI.ViewStateMode.Disabled });
            Controls.Add(new PanelContainer() { ID = "Body", CssClass = "panel-body" });
            Controls.Add(new PanelContainer() { ID = "Footer", CssClass = "panel-footer clearfix" });
        }

		protected override void OnPreRender(EventArgs e)
		{
			if (!string.IsNullOrEmpty(Title))
				TitleHtml.Text = string.Format("<i class=\"fa {1} fa-2x box-size-font\"></i> {0} ", Title, Icon);
			Footer.Visible = (Footer.Controls.Count > 0);
			base.OnPreRender(e);
		}
    }
}