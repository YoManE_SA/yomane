﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Controls
{
    public class MerchantProcessPicker : AutoCompleteBase
    {
        public string Value { get { return ValueField.Value; } set {  ValueField.Value = value; } }

        protected override void SearchItems()
        {
            var items = new List<KeyValuePair<string, string>>();
            items.AddRange(Bll.Merchants.Merchant.AutoCompleteByMerchantNumber(SearchText).OrderBy(x => x.Key));
            foreach (var l in items)
                appendItem(l.Key, l.Key + ',' + l.Value, null, null, null);


        }

       
    }
}