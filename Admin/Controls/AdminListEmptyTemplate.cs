﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Netpay.Infrastructure;
using Netpay.Web.Controls;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Controls
{
    public class AdminListEmptyTemplate : System.Web.UI.ITemplate
    {
        void ITemplate.InstantiateIn(Control Control)
        {    
            PlaceHolder ph = new PlaceHolder();
            var literal = new LiteralControl("<div class=\"alert alert-info AdminListEmptyState\">Your search returned no results, use the filter to adjust the search<div>");
            Control.Controls.Add(literal);
        }
    }
}