﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectState.ascx.cs" Inherits="Netpay.Admin.Controls.SelectState" %>
<div class="row">
    <div class="col-lg-12 margin-bottom-10">
         <asp:Label runat="server" ID="lblValue" />
    </div>
    <div class="col-lg-12">
           <NP:LinkButton ID="btnChange" runat="server" Text="Change" OnCommand="List_Command" CommandName="ShowDialog" CssClass="btn btn-primary" RegisterAsync="true" />
    </div>
</div>
<asp:HiddenField runat="server" ID="hfValue" />
<admin:ModalDialog runat="server" ID="mdSelectState" Title="Select Countries">
    <Body>
        <div class="row">
            <div class="col-lg-5">
                <div class="form-group">
                    Selected:
                      <asp:ListBox runat="server" ID="lbSelectedState" Rows="20" Width="100%" CssClass="form-control" DataTextField="Name" DataValueField="IsoCode" SelectionMode="Multiple" />
                </div>
            </div>
            <div class="col-lg-2 text-center" style="padding-top: 100px;">
                <asp:LinkButton runat="server" Text="&lt;" Width="50px" CssClass="btn btn-info" OnCommand="List_Command" CommandName="Add"><i class="fa fa-angle-left"></i></asp:LinkButton><br />
                <br />
                <asp:LinkButton runat="server" Text="&lt; &lt;" Width="50px" CssClass="btn btn-info" OnCommand="List_Command" CommandName="AddAll"><i class="fa fa-angle-double-left"></i></asp:LinkButton>
                <br />
                <br />
                <asp:LinkButton runat="server" Text="&gt; &gt;" Width="50px" CssClass="btn btn-info" OnCommand="List_Command" CommandName="RemoveAll"><i class="fa fa-angle-double-right"></i></asp:LinkButton>
                <br />
                <br />
                <asp:LinkButton runat="server" Text="&gt;" Width="50px" CssClass="btn btn-info" OnCommand="List_Command" CommandName="Remove"><i class="fa fa-angle-right"></i></asp:LinkButton>
            </div>
            <div class="col-lg-5">
                States:
                  <asp:ListBox runat="server" CssClass="form-control" ID="lbItemsState" Rows="20" Width="100%" DataTextField="Name" DataValueField="IsoCode" SelectionMode="Multiple" />
            </div>
        </div>
    </Body>
    <Footer>
        <input type="button" class="btn btn-default" value="Cancel" onclick="<%# mdSelectState.HideJSCommand %>" />
        <asp:Button CssClass="btn btn-primary" runat="server" Text="Apply" OnCommand="List_Command" CommandName="Apply" />
    </Footer>
</admin:ModalDialog>
