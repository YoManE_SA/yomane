﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Controls
{
    public partial class SelectState : System.Web.UI.UserControl
    {
        public string CssClass { get { return lblValue.CssClass; } set { lblValue.CssClass = value; } }
        public List<string> Value
        {
            get { return hfValue.Value.EmptyIfNull().Split(new char[] { ',', ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList(); }
            set
            {
                if (value == null) value = new List<string>();
                var selItems = Bll.State.Cache.Where(c => value.Contains(c.IsoCode));
                lblValue.Text = string.Join(", ", selItems.Select(s => s.Name));
                hfValue.Value = string.Join(",", selItems.Select(s => s.IsoCode));
            }
        }

        protected override void DataBindChildren()
        {
            if (mdSelectState.Visible)
            {
                lbItemsState.DataSource = Bll.State.Cache.Where(c => !Value.Contains(c.IsoCode));
                lbSelectedState.DataSource = Bll.State.Cache.Where(c => Value.Contains(c.IsoCode));
            }
            base.DataBindChildren();
        }

        protected void List_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "ShowDialog")
            {
                mdSelectState.Visible = true;
                DataBindChildren();
                mdSelectState.RegisterShow();
                return;
            }
            var value = new List<string>();
            foreach (ListItem li in lbSelectedState.Items) value.Add(li.Value);
            switch (e.CommandName)
            {
                case "Add":
                    foreach (ListItem li in lbItemsState.Items)
                        if (li.Selected) value.Add(li.Value);
                    break;
                case "AddAll":
                    foreach (ListItem li in lbItemsState.Items) value.Add(li.Value);
                    break;
                case "Remove":
                    foreach (ListItem li in lbSelectedState.Items)
                        if (li.Selected)
                            if (value.Contains(li.Value)) value.Remove(li.Value);
                    break;
                case "RemoveAll":
                    value = new List<string>();
                    break;
                case "Apply":
                    var selItems = Bll.State.Cache.Where(c => value.Contains(c.IsoCode));
                    string script =
                        "$('#" + lblValue.ClientID + "').text('" + string.Join(", ", selItems.Select(s => s.Name)) + "');" +
                        "$('#" + hfValue.ClientID + "').val('" + string.Join(",", selItems.Select(s => s.IsoCode)) + "');";
                    GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(mdSelectState.Body, script, "UpdateField");
                    mdSelectState.RegisterHide();
                    break;
                default:
                    return;
            }
            Value = value;
            DataBindChildren();
        }
    }
}