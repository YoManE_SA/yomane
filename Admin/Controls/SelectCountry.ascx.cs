﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Controls
{
	public partial class SelectCountry : System.Web.UI.UserControl
	{
		public string CssClass { get { return lblValue.CssClass; } set { lblValue.CssClass = value; } }
		public List<string> Value {
			get { return hfValue.Value.EmptyIfNull().Split(new char[] { ',', ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList(); }
			set
			{ 
				if (value == null) value = new List<string>();
				var selItems = Bll.Country.Cache.Where(c => value.Contains(c.IsoCode2));
				lblValue.Text = string.Join(", ", selItems.Select(s => s.Name));
				hfValue.Value = string.Join(",", selItems.Select(s => s.IsoCode2));
			}
		}

		protected void DataBindItemType()
		{

            if (ddlItemType.SelectedIndex == 0) {
				lbItems.DataValueField = "IsoCode2";
				lbItems.DataSource = Bll.Country.Cache.Where(c => !Value.Contains(c.IsoCode2));
			} else {
				lbItems.DataValueField = "ID";
				lbItems.DataSource = Bll.CountryGroup.Cache;
			}
		}

        protected void Dialog_DataBinding(object sender, EventArgs e)
        {
            if (!mdSelect.Visible) return;
            DataBindItemType();
            lbSelected.DataSource = Bll.Country.Cache.Where(c => Value.Contains(c.IsoCode2));
        }

		protected void ItemType_SelectedIndexChanged(object sender, EventArgs e)
		{
			DataBindItemType();
            lbItems.DataBind();
            mdSelect.RegisterShow();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
        }

		protected void Dialog_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "ShowDialog") {
				mdSelect.Visible = true;
				DataBindChildren();
				mdSelect.RegisterShow();
				return;
			}
			var value = new List<string>();
			foreach (ListItem li in lbSelected.Items) value.Add(li.Value);
			switch (e.CommandName)
			{
			case "Add":
				if (ddlItemType.SelectedIndex == 0) {
					foreach (ListItem li in lbItems.Items)
						if (li.Selected) value.Add(li.Value);
				} else {
					foreach (ListItem li in lbItems.Items)
						if (li.Selected)
							value.AddRange(Bll.CountryGroup.Get(li.Value.ToNullableInt().GetValueOrDefault()).Countries.Select(c => c.IsoCode2));
				}
				break;
			case "AddAll":
				if (ddlItemType.SelectedIndex == 0) {
					foreach (ListItem li in lbItems.Items) value.Add(li.Value);
				} else {
					foreach (ListItem li in lbItems.Items)
						value.AddRange(Bll.CountryGroup.Get(li.Value.ToNullableInt().GetValueOrDefault()).Countries.Select(c => c.IsoCode2));
				}
				break;
			case "Remove":
				foreach (ListItem li in lbSelected.Items)
					if (li.Selected)
						if (value.Contains(li.Value)) value.Remove(li.Value);
				break;
			case "RemoveAll":
				value = new List<string>();
				break;
			case "Apply":
				var selItems = Bll.Country.Cache.Where(c => value.Contains(c.IsoCode2)).ToList();
				string script =
					"$('#" + lblValue.ClientID + "').text('" + string.Join(", ", selItems.Select(s => s.Name)) + "');" +
					"$('#" + hfValue.ClientID + "').val('" + string.Join(",", selItems.Select(s => s.IsoCode2)) + "');";
				GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(mdSelect.Body, script, "UpdateField");
				mdSelect.RegisterHide();
				return;
			default:
				return;
			}
			Value = value;
            mdSelect.DataBind();
		}

	}
}