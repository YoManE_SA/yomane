﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
	public class Pager : System.Web.UI.WebControls.CompositeControl, System.Web.UI.IPostBackEventHandler
	{
		public event EventHandler PageChanged;
		private Infrastructure.ISortAndPage _pagedControl { get; set; }
        public Pager() { Style.Add("margin-left", "35%"); }
		public string PagedControlID { get; set; }
		public Infrastructure.ISortAndPage PagedControl
		{
			set { _pagedControl = value; }
			get {
				if (_pagedControl != null) return _pagedControl;
				if (string.IsNullOrEmpty(PagedControlID)) return null;
				var cont = NamingContainer;
				while (cont != null && cont != this.Page)
				{
					var ret = cont.FindControl(PagedControlID) as Infrastructure.ISortAndPage;
					if (ret != null) return (_pagedControl = ret);
					cont = cont.NamingContainer;
				}
				return null;
			}
		}
		protected override System.Web.UI.HtmlTextWriterTag TagKey { get { return System.Web.UI.HtmlTextWriterTag.Div; } }
		public void RaisePostBackEvent(string eventArgument)
		{
			(PagedControl as System.Web.UI.IPostBackEventHandler).RaisePostBackEvent(eventArgument);
			if (PageChanged != null) {
				PageChanged(this, EventArgs.Empty);
			} else {
				var values = eventArgument.Split('$');
				RaiseBubbleEvent(this, new System.Web.UI.WebControls.CommandEventArgs("Page", values[1]));
			}
		}

        protected override void OnPreRender(EventArgs e)
		{
			if (PagedControl != null)
			{
                Controls.Add(new System.Web.UI.LiteralControl("<ul class=\"pagination\" style=\"display:inline;\">"));
                int maxPage = (int)System.Math.Ceiling((double)PagedControl.RowCount / (double)PagedControl.PageSize);
				int curPage = PagedControl.PageCurrent + 1;
				int startPaging = System.Math.Max(1, curPage - 5);
				if (curPage > 1)
				{
                    //pgParent.Controls.Add(new System.Web.UI.LiteralControl("<span class=\"nextSection\">"));
					var lt = new System.Web.UI.WebControls.Literal();
                    lt.Text = string.Format("<li><a href=\"{0}\" class=\"firstPage\">{1}</a></li> ", Page.ClientScript.GetPostBackClientHyperlink(this, "Page$1"), "&laquo;");
                    Controls.Add(lt);
					//lt = new System.Web.UI.WebControls.Literal();
                    //lt.Text = string.Format("<li><a href=\"{0}\" class=\"nextPage\">{1}</a></li> ", Page.ClientScript.GetPostBackClientHyperlink(this, "Page$" + (curPage - 1)), "&lt;");
                    //pgParent.Controls.Add(lt);
                    //pgParent.Controls.Add(new System.Web.UI.LiteralControl("</span>"));
				}
				for (int i = startPaging; i <= System.Math.Min(maxPage, startPaging + 10); i++)
				{
					var lt = new System.Web.UI.WebControls.Literal();
                    if (curPage == i) lt.Text = string.Format("<li class=\"active\"><a>{0}</a></li> ", i);
                    else lt.Text = string.Format("<li><a href=\"{0}\">{1}</a></li> ", Page.ClientScript.GetPostBackClientHyperlink(this, "Page$" + i), i);
                    Controls.Add(lt);
				}
				if (curPage < maxPage)
				{
					//Controls.Add(new System.Web.UI.LiteralControl("<span class=\"prevSection\">"));
					//var lt = new System.Web.UI.WebControls.Literal();
					//lt.Text = string.Format("<a href=\"{0}\" class=\"prevPage\">{1}</a> ", Page.ClientScript.GetPostBackClientHyperlink(this, "Page$" + (curPage + 1)), "&gt;");
					//Controls.Add(lt);
					var lt = new System.Web.UI.WebControls.Literal();
                    lt.Text = string.Format("<li><a href=\"{0}\" class=\"lastPage\">{1}</a></li> ", Page.ClientScript.GetPostBackClientHyperlink(this, "Page$" + maxPage), "&raquo;");
                    Controls.Add(lt);
					//Controls.Add(new System.Web.UI.LiteralControl("</span>"));
				}
                Controls.Add(new System.Web.UI.LiteralControl("</ul>"));
            }
			base.OnPreRender(e);
		}

	}

}