﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
    [System.Web.UI.ParseChildren(false)]
    public class FormSections : System.Web.UI.WebControls.WebControl, System.Web.UI.IPostBackEventHandler
    {
        public event System.Web.UI.WebControls.CommandEventHandler Command;
        public string Title { get; set; }
        public string Icon { get; set; }
        public bool Flexible {get; set;}
        public bool EnableAdd { get; set; }
        public bool SurroundOnly { get; set; }
        public bool IsForSecurityMessage { get; set; }
        public Infrastructure.Security.SecuredObject SecuredObject { get; set; }
        public System.Web.UI.Control ModuleNotAuthorizedControl
        {
            get
            {
                return (Page as Controls.Page).ModuleNotAuthorizedControl;
            }
        }

        public FormSections()
        {
            Flexible = true; /*CssClass = "row";*/
        }

        protected override System.Web.UI.HtmlTextWriterTag TagKey { get { return System.Web.UI.HtmlTextWriterTag.Div; } }
        //protected override string TagName { get { return "section"; } }

        protected override void DataBindChildren()
        {
            if (SecuredObject != null && !SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
            {
                var nactl = ModuleNotAuthorizedControl;

                var sourceModule = Admin.Application.Current.Modules.Where(x => (x is CoreBasedModule) && (x as CoreBasedModule).CoreModule.ID == SecuredObject.ModuleID).SingleOrDefault();
                if (sourceModule != null)
                {
                    (nactl as Modules.ModuleNotAuthorized.ModuleNotAuthorized).ModuleName = sourceModule.Name + " - " + SecuredObject.Name;
                }

               (nactl as Modules.ModuleNotAuthorized.ModuleNotAuthorized).Permission = "Read";

                Controls.Add(nactl);

                foreach(System.Web.UI.Control ctl in Controls)
                {
                    if (!(ctl is Modules.ModuleNotAuthorized.ModuleNotAuthorized))
                    {
                        ctl.Visible = false;
                    }
                }
            }

            base.DataBindChildren();
        }

        protected override void OnPreRender(EventArgs e)
        {
            System.Web.UI.ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(this);

            base.OnPreRender(e);
        }

        public override void RenderBeginTag(System.Web.UI.HtmlTextWriter writer)
        {
            if (SurroundOnly)
            {
                writer.Write("<div>");
                base.RenderBeginTag(writer);
            }
           
            else 
            {
                writer.Write("<div class=\"row " + (Flexible ? "PageCon-Flexible" : "") + "\">");
                writer.Write("<div class=\"col-lg-12 head-tab-panel\"" + (IsForSecurityMessage? "style=\"display:none;\"" : "") + "\">");
                if (EnableAdd)
                {
                    writer.Write("<div class=\"row\">");
                    writer.Write(" <div class=\"col-lg-8 col-md-8\">" + Title + "</div>");
                    writer.Write(" <div class=\"col-lg-4 col-md-4 text-right\"> <button onclick=\"" + Page.ClientScript.GetPostBackEventReference(this, "AddItem") + ";return false;\" class=\"btn btn-default text-uppercase\"> Add <i class=\"fa fa-plus-circle\"></i> </button> </div>");
                    writer.Write(" <div class=\"clearfix\"></div>");
                    writer.Write("</div>");
                }
                else writer.Write(Title);
                writer.Write("</div>");
                writer.Write("<div class=\"col-lg-12 tab-panel\">");
                base.RenderBeginTag(writer);
            }
        }

        public override void RenderEndTag(System.Web.UI.HtmlTextWriter writer)
        {
            if (SurroundOnly)
            {
                base.RenderEndTag(writer);
                writer.Write("</div>");
            }
            else
            {
                base.RenderEndTag(writer);
                writer.Write("</div></div>");
            }
        }

        public void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument == "AddItem")
            {
                if (Command != null) Command(this, new System.Web.UI.WebControls.CommandEventArgs("AddItem", null));
            }
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            var e = args as System.Web.UI.WebControls.CommandEventArgs;
            if (e != null) {
                if (Command != null) Command(source, e);
                base.OnBubbleEvent(this, e);
                return true;
            }
            return base.OnBubbleEvent(source, args);
        }
    }


    [System.Web.UI.ParseChildren(false)]
    public class FormPanel : FormSections
    {
        private Controls.GlobalControls.UpdatePanel _updatePanel;
        public FormPanel() 
        {
            Controls.Add(_updatePanel = new Controls.GlobalControls.UpdatePanel() { ChildrenAsTriggers = false, UpdateMode = System.Web.UI.UpdatePanelUpdateMode.Conditional, HtmlTag = System.Web.UI.HtmlTextWriterTag.Div, Animation = GlobalControls.UpdatePanel.JQAnimation.None });
        }

        protected override void AddParsedSubObject(object obj)
        {
            if (obj is System.Web.UI.Control)
                _updatePanel.ContentTemplateContainer.Controls.Add(obj as System.Web.UI.Control);
            else base.AddParsedSubObject(obj);
        }

        public void Update()
        {
            _updatePanel.Update();
        }

        public void BindAndUpdate()
        {
            _updatePanel.DataBind();
            _updatePanel.Update();
        }
    }
}

