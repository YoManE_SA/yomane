﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Controls
{
	public class TerminalPicker : AutoCompleteBase
	{
		public string Value { get { return ValueField.Value; } set { ValueField.Value = value; } }

		protected override void SearchItems()
		{
			var items = Bll.DebitCompanies.Terminal.AutoComplete(SearchText);
			foreach (var l in items)
				appendItem(l.TerminalNumber, string.Format("{0} - {1}", l.DebitCompanyName, l.TerminalName), null, Infrastructure.Enums.BoolColorMap[l.IsActive], null);
		}
	}
}
