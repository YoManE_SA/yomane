﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Controls
{
	public partial class SelectPaymentMethod : System.Web.UI.UserControl
	{
		public string CssClass { get { return lblValue.CssClass; } set { lblValue.CssClass = value; } }
		public List<CommonTypes.PaymentMethodEnum> Value {
			get { return hfValue.Value.EmptyIfNull().Split(new char[] { ',', ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(i => (CommonTypes.PaymentMethodEnum)int.Parse(i)).ToList(); }
			set
			{ 
				if (value == null) value = new List<CommonTypes.PaymentMethodEnum>();
				var selItems = Bll.PaymentMethods.PaymentMethod.Cache.Where(c => value.Contains((CommonTypes.PaymentMethodEnum)c.ID));
				lblValue.Text = string.Join(", ", selItems.Select(s => s.Name));
				hfValue.Value = string.Join(",", selItems.Select(s => s.ID));
			}
		}

		protected void DataBindItemType()
		{
			lbItems.DataValueField = "ID";
			lbItems.DataSource = Bll.PaymentMethods.PaymentMethod.Cache.Where(c => !Value.Contains((CommonTypes.PaymentMethodEnum)c.ID));
		}

        protected void Dialog_DataBinding(object sender, EventArgs e)
        {
            if (!mdSelect.Visible) return;
            DataBindItemType();
            lbSelected.DataSource = Bll.PaymentMethods.PaymentMethod.Cache.Where(c => Value.Contains((CommonTypes.PaymentMethodEnum)c.ID));
        }

		protected void ItemType_SelectedIndexChanged(object sender, EventArgs e)
		{
			DataBindItemType();
			lbItems.DataBind();
		}

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
        }

		protected void Dialog_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "ShowDialog") {
				mdSelect.Visible = true;
				DataBindChildren();
				mdSelect.RegisterShow();
				return;
			}
			var value = new List<CommonTypes.PaymentMethodEnum> ();
			foreach (ListItem li in lbSelected.Items) value.Add((CommonTypes.PaymentMethodEnum)int.Parse(li.Value));
			switch (e.CommandName)
			{
			case "Add":
				foreach (ListItem li in lbItems.Items)
					if (li.Selected) value.Add((CommonTypes.PaymentMethodEnum)int.Parse(li.Value));
				break;
			case "AddAll":
				foreach (ListItem li in lbItems.Items) value.Add((CommonTypes.PaymentMethodEnum)int.Parse(li.Value));
				break;
			case "Remove":
                    foreach (ListItem li in lbSelected.Items)
                        if (li.Selected)
                        {
                            int paymentMethodId = int.Parse(li.Value);
                            if (value.Contains((CommonTypes.PaymentMethodEnum)paymentMethodId)) value.Remove((CommonTypes.PaymentMethodEnum)paymentMethodId);
                        }
				break;
			case "RemoveAll":
				value = new List<CommonTypes.PaymentMethodEnum> ();
				break;
			case "Apply":
				var selItems = Bll.PaymentMethods.PaymentMethod.Cache.Where(c => value.Contains((CommonTypes.PaymentMethodEnum)c.ID)).ToList();
				string script =
					"$('#" + lblValue.ClientID + "').text('" + string.Join(", ", selItems.Select(s => s.Name)) + "');" +
					"$('#" + hfValue.ClientID + "').val('" + string.Join(",", selItems.Select(s => s.ID)) + "');";
				GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(mdSelect.Body, script, "UpdateField");
				mdSelect.RegisterHide();
				return;
			default:
				return;
			}
			Value = value;
            mdSelect.DataBind();
		}
	}
}