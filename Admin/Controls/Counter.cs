﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Controls
{
    public class Counter : System.Web.UI.WebControls.CompositeControl
    {
		public System.Web.UI.WebControls.Label Title { get; private set; }
		public System.Web.UI.WebControls.Label CounterLabel { get; private set; }
		public string TitleText { get { return Title.Text; } set { Title.Text = value; } }
        public decimal? Value { get { return CounterLabel.Text.ToNullableDecimal(); } set { CounterLabel.Text = value == null ? "0000000" : value.Value.ToString("0000000"); } }

        protected override System.Web.UI.HtmlTextWriterTag TagKey { get { return System.Web.UI.HtmlTextWriterTag.Div; } }

        public Counter()
		{
			Style.Add("margin-bottom", "10px");
			Controls.Add(Title = new System.Web.UI.WebControls.Label()); Title.Font.Bold = true;
			Controls.Add(new System.Web.UI.LiteralControl("<br/>"));
            Controls.Add(CounterLabel = new System.Web.UI.WebControls.Label()); CounterLabel.Font.Size = 24;
		}

    }
}