﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Controls
{
    public class ModulePage : Controls.Page
    {
        public string ModuleName {get; protected set;}

        public bool IsModuleActive
        {
            get
            {
                return Netpay.Admin.Application.Current.Modules.Where(x => x.Name == ModuleName).Single().IsActive;
            }
        }

        public ModulePage(string moduleName)
        {
            ModuleName = moduleName;
        }
    }
}