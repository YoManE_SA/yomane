﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Controls
{
	public class AccountPicker : AutoCompleteBase
	{
		public bool UseTargetID { get; set; }
		public int? Value { get { return ValueField.Value.ToNullableInt(); } set { ValueField.Value = (value == null ? "" : value.ToString()); } }
		public Netpay.Bll.Accounts.AccountType? ValueType { get; set; }

		public List<Netpay.Bll.Accounts.AccountType> LimitToTypes { get; set; }
		public Netpay.Bll.Accounts.AccountType LimitToType { set { LimitToTypes = new List<Bll.Accounts.AccountType>() { value }; } }

		protected override void SearchItems()
		{
			var items = new List<Netpay.Bll.Accounts.AccountInfo>();
			if (LimitToTypes == null || LimitToTypes.Contains(Netpay.Bll.Accounts.AccountType.Merchant)) items.AddRange(Bll.Merchants.Merchant.AutoComplete(SearchText));
			if (LimitToTypes == null || LimitToTypes.Contains(Netpay.Bll.Accounts.AccountType.Customer)) items.AddRange(Bll.Customers.Customer.AutoComplete(SearchText));
			if (LimitToTypes == null || LimitToTypes.Contains(Netpay.Bll.Accounts.AccountType.Affiliate)) items.AddRange(Bll.Affiliates.Affiliate.AutoComplete(SearchText));
			if (LimitToTypes == null || LimitToTypes.Contains(Netpay.Bll.Accounts.AccountType.DebitCompany)) items.AddRange(Bll.DebitCompanies.DebitCompany.AutoComplete(SearchText));
			foreach (var l in items)
				appendItem((UseTargetID ? l.TargetID : l.AccountID).ToString(), l.AccountName, l.AccountType.ToString(), l.StatusColor, null);
		}

	}
}
