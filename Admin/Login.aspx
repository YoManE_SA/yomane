﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Netpay.Admin.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow" />
    <link href="Styles/bootstrap.css" rel="stylesheet" />
    <link href="Styles/bootstrap.css" rel="stylesheet" />
    <link href="Styles/font-awesome.css" rel="stylesheet" />
    <link href="Styles/login.css" rel="stylesheet" />
    <script src="Scripts/core/bootstrap.js"></script>
    <title>Admin Login</title>
    <link rel="icon" runat="server" id="lnkicon" type="image/png"  />
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager runat="server" ID="ScriptManager" />
        <div class="container">
            <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title pull-right text-right">
                            <img src="Templates/<%= Domain.Current.ThemeFolder %>/images/logo-mini.png" />
                        </div>
                        <div class="pull-left ">
                            <div class="panel-text" style="line-height: 35px;">Sign in - backoffice admin</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        <div id="loginform" class="form-horizontal">
                            <div class="row">
                                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" ID="upActionNotify">
                                    <ContentTemplate>
                                        <div class="col-md-12">
                                            <netpay:ActionNotify runat="server" ID="acnLogin" CssClass="alert alert-danger" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                           <%-- <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <asp:TextBox runat="server" ID="txtUserName" type="text" class="form-control" name="username" placeholder="Username" />
                                    </div>
                                </div>
                            </div>--%>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-square fa-fw"></i></span>
                                        <asp:TextBox runat="server" ID="txtEmail" type="text" class="form-control" name="Email" placeholder="Email" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                        <input style="display: none" type="password" id="txtPassword">
                                        <asp:TextBox AutoCompleteType="Disabled" runat="server" ClientIDMode="Static" ID="txtPassword" type="password" class="form-control" name="Password" placeholder="Password" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 controls">
                                    <asp:Button runat="server" ID="btnLogin" CssClass="btn btn-primary" Text="Sign in" OnCommand="Login_Command" />
                                </div>
                                <div class="col-sm-6 controls">
                                    <asp:LinkButton runat="server" OnClientClick='<%# "event.preventDefault();" + ConfirmationModalDialog.ShowJSCommand %>'  ClientIDMode="Static" ID="btnForgotPassword" Text="Forgot password?" CssClass="pull-right">
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%-- second factor modal dialog --%>
        <admin:ModalDialog runat="server" ID="mdlSecondFactor" ChildrenAsTriggers="true" Title="Seconed Factor Authentication Required">
            <Body>
                <div class="row">
                    <div class="col-md-12">
                        <netpay:ActionNotify runat="server" ID="acnSecond" CssClass="alert alert-danger" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <small>Phone number for seconed factor
                            <asp:Label runat="server" ID="lblPhoneNumber" CssClass="text-info" Text="+972-50-3000661" /></small>
                        </p>
                        <netpay:DropDownBase runat="server" ID="ddlSecondFactorMode" DataSource='<%# new string[] { "SMS", "Phone" } %>' EnableBlankSelection="false" OnSelectedIndexChanged="SecondFactorMode_SelectedIndexChanged" AutoPostBack="true" />
                    </div>
                </div>
                <asp:MultiView runat="server" ID="mvSecondFactorMode" ActiveViewIndex="1">
                    <asp:View runat="server" ID="viewSMS">
                        <div class="row">
                            <div class="col-md-12">
                                <p><small>Please enter the code (6 digits), code sent by sms</small></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 ">
                                <div class="form-group">
                                    <asp:TextBox runat="server" ID="txtSMSCode" ype="text" class="form-control" name="Code" placeholder="Code 6 digits" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <small>still not recieved message?
                                    <asp:LinkButton runat="server" OnCommand="Login_Command" CommandArgument="Seconed" Text="Send again" /></small>
                            </div>
                        </div>
                    </asp:View>
                    <asp:View runat="server" ID="viewPhone">
                        <div class="row">
                            <div class="col-md-12">
                                <p><small>we are calling, wait 10 seconds</small></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <small>still not recieved call?
                                    <asp:LinkButton runat="server" OnCommand="Login_Command" CommandArgument="Seconed" CssClass="btn btn-xs btn-info" Text="Call again" /></small>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </Body>
            <Footer>
                <asp:Button runat="server" CssClass="btn btn-default" data-dismiss="modal" Text="Cancel" />
                <asp:Button runat="server" CssClass="btn btn-primary" Text="Continue" OnCommand="Login_Command" CommandArgument="Seconed" />
            </Footer>
        </admin:ModalDialog>

        <%-- enforce change password modal dialog --%>
        <admin:ModalDialog OnDataBinding="dlgChangePassword_DataBinding" OnDialogClose="dlgChangePassword_DialogClose" runat="server" ID="dlgChangePassword" Title="Change Password">
            <Body>
                <asp:MultiView ID="mvChangePass" runat="server">
                    <asp:View runat="server">
                        <div class="alert alert-danger">
                            We recommend you to change your password after it was reset.
                        </div>
                    </asp:View>
                    <asp:View runat="server">
                        <div class="alert alert-danger">
                            Password is too old. Policy requires changing it.
                            <br />
                            Please insert a new password and than log in.
                        </div>
                    </asp:View>
                </asp:MultiView>
                <netpay:ActionNotify runat="server" ID="acnPasswordMessage" />
                <div class="form-group">
                    New Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtNewPassword" TextMode="Password" />
                </div>
                <div class="form-group">
                    Confirm Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtConfirmPassword" TextMode="Password" />
                </div>
            </Body>
            <Footer>
                <asp:Button runat="server" ID="btnClose" ClientIDMode="Static" CssClass="btn btn-inverse btn-cons-short" Text="Cancel" OnClientClick='<%# dlgChangePassword.HideJSCommand %>' />
                <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary btn-cons-short" Text="Save" OnClick="UpdatePassword_Click" />
            </Footer>
        </admin:ModalDialog>

        <admin:ModalDialog ID="ConfirmationModalDialog" Title="Forgot your password?" runat="server">
            <Body>
                Are you sure you want us to generate a new password,
                And send it to you via email?
            </Body>
            <Footer>
                <asp:Button UseSubmitBehavior="false" OnClick="btnForgotPassword_Click" runat="server" Text="Confirm" />
                <asp:Button OnClientClick='<%# ConfirmationModalDialog.HideJSCommand %>' runat="server" Text="Cancel" />
            </Footer>
        </admin:ModalDialog>
    </form>
</body>
</html>
