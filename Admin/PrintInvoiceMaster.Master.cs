﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin
{
    public partial class PrintInvoiceMaster : System.Web.UI.MasterPage
    {
        protected override void OnLoad(EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQuery(Page);
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "core/bootstrap.min.js");
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/Netpay-Plugin/OpenPop.js");
            lnkTemplateStyle.Attributes["href"] = ResolveUrl("~/Templates/" + Domain.Current.ThemeFolder + "/style.css");

            base.OnPreRender(e);
        }
    }
}