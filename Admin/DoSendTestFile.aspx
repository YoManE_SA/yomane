﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DoSendTestFile.aspx.cs" Inherits="Netpay.Admin.DoSendTestFile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Button ID="btnTestFileGeneration" runat="server" OnClick="btnTestFileGeneration_Click" Text="Generate File" />

        </div>
        <asp:Label ID="lblOutput" runat="server"></asp:Label>
        <br />
        <br />
        <asp:Button ID="btnKickOffWinServices" runat="server" OnClick="btnKickOffWinServices_Click" Text="Start Windows Services" />
&nbsp;|
        <asp:Button ID="btnEndWindowsServices" runat="server" OnClick="btnEndWindowsServices_Click" Text="End Windows Services" />
&nbsp;|
        <asp:Button ID="btnECentric" runat="server" OnClick="btnECentric_Click" Text="eCentric Generate" />
    </form>
</body>
</html>
