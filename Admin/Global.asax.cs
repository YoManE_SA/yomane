﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Netpay.Admin
{
    public class Global : System.Web.HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            Web.ContextModule.AppName = "Admin";
        }

        void Application_BeginRequest(object sender, EventArgs e)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3 | System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;

            var application = sender as HttpApplication;
            if (application != null && application.Context != null)
            {
                application.Context.Response.Headers.Remove("Server");
            }

            if (Admin.Application.Current == null)
            {
                //throw new Exception("app is null");
            }
        }

        //void Application_AuthorizeRequest(object sender, EventArgs e)
        //{
        //	lock(this){
        //		if (Admin.Application.Current == null) 
        //			Admin.Application.Current = new Admin.Application();
        //	}
        //}

        void Session_Start(object sender, EventArgs e)
        {
            if (!Web.WebUtils.IsLoggedin) Web.WebUtils.Login();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Infrastructure.Logger.Log(ex);
            if (ex is HttpException)
            {
                if ((ex as HttpException).InnerException.Message.StartsWith("?NotAuthorized"))
                {
                    Response.Redirect(Web.WebUtils.RootUrl + (ex as HttpException).InnerException.Message);
                }
            }
        }

        protected void Application_AcquireRequestState(Object sender, EventArgs e)
        {
            if (Request.IsSecureConnection) return;
            if (Request.ServerVariables["HTTP_HOST"].StartsWith("192.168") ||  Request.ServerVariables["HTTP_HOST"].StartsWith("10.0")  || Request.ServerVariables["HTTP_HOST"] == "localhost") return;
            if (Web.WebUtils.CurrentDomain.ForceSSL)
                Response.Redirect("https://" + Request.Url.ToString().Substring(7));
        }
    }
}