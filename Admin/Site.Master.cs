﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin
{
    public partial class Site : System.Web.UI.MasterPage
    {
        public ApplicationMenu CurrentItem;

        public string CurrentModuleName
        {
            get
            {                
                if (BodyContent.Page is Admin.Controls.DataTablePage || (BodyContent.Page is Admin.Controls.Page && (!(BodyContent.Page is Admin.Controls.ModulePage))))
                {
                    if ((BodyContent.Page as Controls.Page).CurrentModule != null)
                    {
                        return (BodyContent.Page as Controls.Page).CurrentModule.Name;
                    }
                    else
                    {
                        return "";
                    }
                }
                else if (BodyContent.Page is Admin.Controls.ModulePage)
                {
                    return ModulePage.ModuleName;
                }
                else
                {
                    return "";
                }
            }
                
        }

        public bool GetBodyContentActiveViewIndex
        {
            get
            {
                if (BodyContent.Page is Admin.Controls.DataTablePage || (BodyContent.Page is Admin.Controls.Page && (!(BodyContent.Page is Admin.Controls.ModulePage))))
                {
                    if ((BodyContent.Page as Controls.Page).CurrentModule!=null)
                    {
                        if ((BodyContent.Page as Controls.Page).CurrentModule.IsActive)
                        {
                            return true;
                        }
                        else //The current DataTablePage module is not active.
                        {                            
                            return false;
                        }
                    }
                    else
                    {
                        //if the module is null continue regular flow
                        return true;
                    }
                }
                else if (BodyContent.Page is Admin.Controls.ModulePage)
                {
                    if (ModulePage.IsModuleActive)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        public bool IsModulePage
        {
            get
            {
                return (BodyContent.Page is Admin.Controls.ModulePage);
            }
        }

        public Admin.Controls.ModulePage ModulePage
        {
            get
            {
                if (!IsModulePage) return null;

                return BodyContent.Page as Admin.Controls.ModulePage;
            }
        }
        public PlaceHolder TopButtons { get { return phTopButtons; } }
                
        public PlaceHolder SideBarPageMini { get { return phSideBarPageMini; } }

        public PlaceHolder HeadLine { get { return phHeadLine; } }

        public PlaceHolder WrapBradCramps { get { return phWrapBradCramps; } }

        public UpdatePanel UserMenu { get { return UserMenuUpdatePanel; } }

        //That FileManagerPath value should be the exact string like 
        //The url of the Filemanager module menu items, only without 
        //Query string.
        public const string FileManagerPath = "~/Modules/FileManager/FileManager.aspx";

        public const string PrepaidPaymentMethodsPath = "~/Modules/Settings/PrepaidPaymentMethods/PrepaidPaymentMethods.aspx";

        //public UpdatePanel SideMenu { get { return UpSideMenu; }  }

        protected override void OnLoad(EventArgs e)
        {
            var titleParts = (Page as Controls.Page).TitleParts;
            string relPath = "~" + Request.Url.AbsolutePath.Substring(Request.ApplicationPath.Length);

            //Handle a special case of FileManager pages.
            if (relPath == FileManagerPath)
            {
                if (Request["Type"] != null)
                {
                    relPath = string.Format("{0}?{1}", relPath, Request.QueryString.ToString()); 
                }
            }
            else if (relPath == PrepaidPaymentMethodsPath)
            {
                if (Request["Type"] != null)
                {
                    relPath = string.Format("{0}?{1}", relPath, Request.QueryString.ToString());
                }
            }

            if (!string.IsNullOrEmpty(relPath)) CurrentItem = Admin.Application.Current.Menu.FindByUrl(relPath);
            if (CurrentItem == null) CurrentItem = new ApplicationMenu(Page.Title, null); //CurrentItem = Admin.Application.Current.Menu;
            if (CurrentItem != null)
            {
                for (var menu = CurrentItem; menu.Parent != null; menu = menu.Parent)
                    titleParts.Insert(0, menu);
            }

            if (Request["Ref"] != null) {
                var menu = new ApplicationMenu(Request["RefTitle"].NullIfEmpty().ValueIfNull("[Untitled]"), Request["Ref"]);
                titleParts.Insert(0, menu);
            }

            //Check if the QueryString contanins "NoLayout=1" witch means this is a popup mode
            //And some sections should be not visible.
            var queryString = Page.Request.QueryString.ToString();
            if (queryString.Contains("NoLayout=1"))
            {
                SideBarPageMini.Visible = false;
                HeadLine.Visible = false;
                WrapBradCramps.Visible = false;
                dvContainer.Style.Add("margin-left", "");
            }
            
            base.OnLoad(e);
        }

        protected override void OnPreRender(EventArgs e)
        {            
            if (Page is Controls.Page)
            {
                rptTitlePart.DataSource = (Page as Controls.Page).TitleParts;
                rptTitlePart.DataBind();
            }
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQuery(Page);

            /* The functionality of the HideLayout(); JavaScript function
               Is implememted from CodeBehind , in the OnLoad event of this SiteMaster.cs
               File , if the QueryString has the 'NoLayout=1' string the UI is changed. 
                                       
            //string script = "$(document).ready(function () {HideLayout();});";
            //Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(Page, script);
            //Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/Netpay-Plugin/HideLayout.js");
            */

            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "core/bootstrap.min.js");
            //Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "core/docs.js");
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/metisMenu-master/dist/metisMenu.js");
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(this, "$('#menu').metisMenu({ /*doubleTapToGo:true*/ });");
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/Netpay-Plugin/OpenTabByClientId.js");
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/Netpay-Plugin/OpenPop.js");
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/tabdrop/js/bootstrap-tabdrop.js");

            
            rptMenu.DataBind();
            //detect current page
            if (CurrentItem != null && CurrentItem.Parent != null)
            {
                var menu = CurrentItem;
                while (menu.Parent != null && menu.Parent != Admin.Application.Current.Menu) menu = menu.Parent;

                Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(this, "$('#miniSidebar_" + menu.GetPath('_').Replace(' ', '_') + "').addClass('activeOn');", "miniSidebar");
                
				Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(this, "$('#Menu_" + menu.GetPath('_').Replace(' ', '_') + "').toggleClass('active').children('ul').collapse('show');", "SelectMenu");
               
            }
            lnkicon.Attributes["href"] = ResolveUrl("~/Templates/" + Domain.Current.ThemeFolder + "/favico/16X16.png");
            lnkTemplateStyle.Attributes["href"] = ResolveUrl("~/Templates/" + Domain.Current.ThemeFolder + "/style.css");
            base.OnPreRender(e);
            logo.ImageUrl = "~/Templates/" + Domain.Current.ThemeFolder + "/images/logo.png";

            //Update user menu , if the Emails module was
            //Deactivated it should be removed.
            UserMenu.DataBind();
            UserMenu.Update();

            UpMiniSideBar.DataBind();
            UpMiniSideBar.Update();

            //UpSideMenu.DataBind();
            //UpSideMenu.Update();
        }

        protected void QuickSearch_Command(object sender, CommandEventArgs e)
        {
            string targetUrl = null;
            var picker = (sender as Control).FindControl((string)e.CommandArgument) as Controls.AccountPicker;

            //Get the selected account id
            Bll.Accounts.Account accountToSearch;

            if (picker.Value.HasValue)
            {
                accountToSearch = Bll.Accounts.Account.LoadAccount(picker.Value.Value);
                if (accountToSearch != null)
                {
                    switch (accountToSearch.AccountType)
                    {
                        case Bll.Accounts.AccountType.Merchant: targetUrl = "~/Merchants/0?Search=1&SelectedIndex=0&ctl00.apAccount.hdId=" + picker.Value; break;
                        case Bll.Accounts.AccountType.Customer: targetUrl = "~/Customers/0?Search=1&SelectedIndex=0&ctl00.apAccount.hdId=" + picker.Value; break;
                        case Bll.Accounts.AccountType.Affiliate: targetUrl = "~/Affiliates/0?Search=1&SelectedIndex=0&ctl00.apAccount.hdId=" + picker.Value; break;
                        case Bll.Accounts.AccountType.DebitCompany: targetUrl = "~/DebitCompanies/0?Search=1&SelectedIndex=0&ctl00.apAccount.hdId=" + picker.Value; break;
                    }
                    if (!string.IsNullOrEmpty(targetUrl)) Response.Redirect(targetUrl, true);
                }
            }


            /*
            switch (e.CommandName) {
                case "Merchant": targetUrl = "~/Merchants/" + picker.Value; break;
                case "Customer": targetUrl = "~/Customers/" + picker.Value; break;
                case "Affiliate": targetUrl = "~/Affiliate/" + picker.Value; break;
                case "DebitCompany": targetUrl = "~/DebitCompany/" + picker.Value; break;
            }
            if (!string.IsNullOrEmpty(targetUrl)) Response.Redirect(targetUrl, true);
            */
        }
    }
}