﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Layout.aspx.cs" Inherits="Netpay.Admin.Layout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	<script type="text/javascript">
		$(document).ready(function () {
			$("#myModal").modal('show');
		});
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
	<div class="row">
		<div id="Dv1" class="col-lg-4" style="display: none">
			<div class="wrap-close-list"><i class="fa fa-expand close-window"></i></div>
			<div id="BodyContent_PageController_FilterView" class="" onkeydown="return FormEnterSubmit(this, event, null)" animation="Fade" showshadow="false">
				<div id="BodyContent_PageController_FilterView_FiltersPanel" class="panel panel-default filter">
					<div id="BodyContent_PageController_FilterView_FiltersPanel_Body" class="panel-body">
						<section class="margin-bottom-10">
							<div class="head-filter">
								<div class="pull-left">Status</div>
								<div class="pull-right">
									<i onclick="$('#BodyContent_PageController_FilterView_FiltersPanel_ctl00_ctl00').slideToggle('fast', function() { $('#BodyContent_PageController_FilterView_FiltersPanel_ctl00_ctl00_Button').attr('class', 'fa ' + ($(this).is(':visible') ? 'fa-angle-down' : 'fa-angle-right')); });"
										id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_ctl00_Button" class="fa fa-angle-down" style="cursor: pointer;"></i>
								</div>
								<div class="clearfix"></div>
							</div>
							<div id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_ctl00" class="content-filter">
								<div class="form-group">
									<span id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_lblCustomer">Account</span>
									<div id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_apCustomer" class="input-group">
										<span class="input-group-addon">
											<span id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_apCustomer_lblID" class="textBox">-</span></span><span class="ui-helper-hidden-accessible" aria-live="polite" role="status"></span>
										<input autocomplete="off" name="ctl00$BodyContent$PageController$FilterView$FiltersPanel$ctl00$apCustomer$txtSearch" id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_apCustomer_txtSearch" class="form-control ui-autocomplete-input" type="text">
										<input name="ctl00$BodyContent$PageController$FilterView$FiltersPanel$ctl00$apCustomer$hdID" id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_apCustomer_hdID" type="hidden">
									</div>
								</div>
								<div class="form-group">
									<span id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_lblID">ID Range</span>
									<div class="row">
										<div class="col-xs-5">
											<input name="ctl00$BodyContent$PageController$FilterView$FiltersPanel$ctl00$rngIDFrom" id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_rngIDFrom" class="form-control" type="number">
										</div>
										<div class="col-xs-2">To </div>
										<div class="col-xs-5">
											<input name="ctl00$BodyContent$PageController$FilterView$FiltersPanel$ctl00$rngIDTo" id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_rngIDTo" class="form-control" type="number">
										</div>
									</div>
								</div>
								<div class="form-group">
									<span id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_lblInsertDate">Registration Date</span>
									<div class="row">
										<div class="col-xs-5">
											<input name="ctl00$BodyContent$PageController$FilterView$FiltersPanel$ctl00$rngRegisterDateFrom" id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_rngRegisterDateFrom" class="form-control hasDatepicker" type="text">
										</div>
										<div class="col-xs-2">To </div>
										<div class="col-xs-5">
											<input name="ctl00$BodyContent$PageController$FilterView$FiltersPanel$ctl00$rngRegisterDateTo" id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_rngRegisterDateTo" class="form-control hasDatepicker" type="text">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">

										<div class="form-group">
											<span id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_Label1">Status</span>
											<select name="ctl00$BodyContent$PageController$FilterView$FiltersPanel$ctl00$ddlStatus" id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_ddlStatus" class="form-control">
												<option selected="selected" value="" class="emptyOption"></option>
												<option value="1">New</option>
												<option value="2">Blocked</option>
												<option value="10">LoginOnly</option>
												<option value="20">TestingMode</option>
												<option value="30">FullyActive</option>
												<option value="99">Closed</option>
											</select>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<span id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_Identity">Date Range</span>
											<select name="ctl00$BodyContent$PageController$FilterView$FiltersPanel$ctl00$ddlAppIdentitySearch" id="BodyContent_PageController_FilterView_FiltersPanel_ctl00_ddlAppIdentitySearch" class="form-control">
												<option selected="selected" value="" class="emptyOption"></option>
												<option value="1">DEPOSIT-DEPOT</option>
												<option value="2">DEPOSIT-BAY</option>
												<option value="3">e-wallet-networka</option>
												<option value="4">Samsung</option>
												<option value="5">NetpayIntl</option>
												<option value="6">localhost</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
					<div id="BodyContent_PageController_FilterView_FiltersPanel_Footer" class="panel-footer clearfix">
						<div id="BodyContent_PageController_FilterView_FiltersPanel_DataButtons" style="text-align: center;">
							<div id="BodyContent_PageController_FilterView_FiltersPanel_DataButtons_dvPages" style="margin-left: 35%;">
							</div>
							<input name="ctl00$BodyContent$PageController$FilterView$FiltersPanel$DataButtons$btnSearch" value="Search" id="BodyContent_PageController_FilterView_FiltersPanel_DataButtons_btnSearch" class="btn btn-cons-short btn-primary margin-left-5 pull-right" type="submit">
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<div id="Dv2" class="col-lg-8">

			<!-- Only in list Section -->
			<div class="wrap-close-list"><i class="fa fa-expand close-window"></i></div>
			<div class="wrap-back-list"><i class="fa fa-arrow-left back-window"></i></div>
			<!-- End in list Section -->

			<div id="BodyContent_PageController_ListView_ctl01_ListSection1" class="panel panel-default">
				<div class="panel-body">
					<div id="BodyContent_PageController_ListView_ctl01_ListSection1_Header" class="panel-heading box-sub-title">
						<i class="fa  fa-2x box-size-font"></i>Merchants 
	   
					<div class="btn-group pull-right">
						<button data-toggle="dropdown" class="btn btn-info btn-cons-short dropdown-toggle">
							Legend
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right">
							<li><a href="#">
								<span class="legend-list" style="background: #7D7D7D;"></span>
								Archived</a></li>
							<li><a href="#">
								<span class="legend-list" style="background: #FF8040;"></span>
								Blocked</a></li>
							<li><a href="#">
								<span class="legend-list" style="background: #FF6666;"></span>
								Closed</a></li>
							<li><a href="#">
								<span class="legend-list" style="background: #FFEB65;"></span>
								Integration</a></li>
							<li><a href="#">
								<span class="legend-list" style="background: #9E6CFF;"></span>
								LoginOnly</a></li>
							<li><a href="#">
								<span class="legend-list" style="background: #6699CC;"></span>
								New</a></li>
							<li><a href="#">
								<span class="legend-list" style="background: #66CC66;"></span>
								Processing</a></li>
						</ul>
					</div>

					</div>
					<div id="BodyContent_PageController_ListView_ctl01_ListSection1_Body" style="padding: 0px;">
					</div>

					<div>
						<table class="table table-striped" selectedindex="-1" id="BodyContent_PageController_ListView_ctl01_ListSection1_rptList" style="border-width: 0px; width: 100%; border-collapse: collapse;" cellspacing="0">
							<thead>
								<tr order="-1">
									<th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$ID')">ID</a></th>
									<th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$Name')">Name</a></th>
									<th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$Number')">Number</a></th>
									<th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$ContactFirstName')">ContactFirstName</a></th>
									<th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$ContactLastName')">ContactFirstName</a></th>
									<th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$AccountManager')">AccountManager</a></th>
								</tr>
							</thead>
							<tbody>
								<tr style="cursor: pointer;" order="0" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$0')">
									<td>
										<span style="background-color: #FF6666;" class="legend-item"></span>
										&nbsp;
											2
									</td>
									<td>או.בי.אל קומפיוטר - קומפיונט</td>
									<td>7078777</td>
									<td>קומפיונט</td>
									<td>החזר הלוואה</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="1" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$1')">
									<td>
										<span style="background-color: #FF6666;" class="legend-item"></span>
										&nbsp;
						3
									</td>
									<td>אביב שלו מולטימדיה-xline</td>
									<td>7078704</td>
									<td>אביב</td>
									<td>שלו</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="2" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$2')">
									<td>
										<span style="background-color: #FF6666;" class="legend-item"></span>
										&nbsp;
						9
									</td>
									<td>בננה אחזקות בע``מ</td>
									<td>6002924</td>
									<td>בננה אחזקות בע``מ</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr order="3" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$3')">
									<td>
										<span style="background-color: #9E6CFF;" class="legend-item"></span>
										&nbsp;
						10
									</td>
									<td>tymond</td>
									<td>5691087</td>
									<td>דורון</td>
									<td>הופמן</td>
									<td>&nbsp;</td>
								</tr>
								<tr order="4" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$4')">
									<td>
										<span style="background-color: #9E6CFF;" class="legend-item"></span>
										&nbsp;
						11
									</td>
									<td>BooksRus</td>
									<td>7303978</td>
									<td>אלי</td>
									<td>עוזרי</td>
									<td>&nbsp;</td>
								</tr>
								<tr order="5" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$5')">
									<td>
										<span style="background-color: #66CC66;" class="legend-item"></span>
										&nbsp;
						12
									</td>
									<td>Netpay Ltd</td>
									<td>5638207</td>
									<td>אלון אלבז</td>
									<td>נט-פיי</td>
									<td>&nbsp;</td>
								</tr>
								<tr order="6" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$6')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						21
									</td>
									<td>shamir</td>
									<td>7817387</td>
									<td>אלקס</td>
									<td>בלוי</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="7" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$7')">
									<td>
										<span style="background-color: #66CC66;" class="legend-item"></span>
										&nbsp;
						22
									</td>
									<td>demo-001</td>
									<td>3738338</td>
									<td>test</td>
									<td>test</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="8" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$8')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						24
									</td>
									<td>asia</td>
									<td>4045403</td>
									<td>test</td>
									<td>test</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="9" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$9')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						25
									</td>
									<td>Jackpot Casino</td>
									<td>8674054</td>
									<td>Alon</td>
									<td>Elbaz</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="10" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$10')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						27
									</td>
									<td>Internet bussiness</td>
									<td>1652219</td>
									<td>גנדור</td>
									<td>בשארה</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="11" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$11')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						28
									</td>
									<td>bingo90</td>
									<td>2645907</td>
									<td>דובי</td>
									<td>ברוך</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="12" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$12')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						29
									</td>
									<td>officer</td>
									<td>3562595</td>
									<td>ניר</td>
									<td>test</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="13" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$13')">
									<td>
										<span style="background-color: #FF6666;" class="legend-item"></span>
										&nbsp;
						32
									</td>
									<td>hishhay</td>
									<td>8124539</td>
									<td>test</td>
									<td>test</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="14" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$14')">
									<td>
										<span style="background-color: #66CC66;" class="legend-item"></span>
										&nbsp;
						35
									</td>
									<td>Dev Testing</td>
									<td>5722306</td>
									<td>Liran</td>
									<td>Patimer</td>
									<td>tamir</td>
								</tr>
								<tr style="cursor: pointer;" order="15" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$15')">
									<td>
										<span style="background-color: #9E6CFF;" class="legend-item"></span>
										&nbsp;
						37
									</td>
									<td>אגודת החרשים בישראל ע.ר.</td>
									<td>7407491</td>
									<td>עינה</td>
									<td>מרחב</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="16" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$16')">
									<td>
										<span style="background-color: #FF8040;" class="legend-item"></span>
										&nbsp;
						38
									</td>
									<td>עידן 2000</td>
									<td>8224154</td>
									<td>רון</td>
									<td>שמחון</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="17" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$17')">
									<td>
										<span style="background-color: #FF6666;" class="legend-item"></span>
										&nbsp;
						39
									</td>
									<td>אלון אלבז</td>
									<td>3136652</td>
									<td>אוסנת</td>
									<td>אלבז</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="18" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$18')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						42
									</td>
									<td>AccessNet</td>
									<td>4964779</td>
									<td>דיאב</td>
									<td>עווידה</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="19" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$19')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						44
									</td>
									<td>איידקס אונליין</td>
									<td>4683586</td>
									<td>אורי</td>
									<td>מילשטיין</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="20" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$20')">
									<td>
										<span style="background-color: #FF8040;" class="legend-item"></span>
										&nbsp;
						48
									</td>
									<td>marketop</td>
									<td>5235957</td>
									<td>מיכאל</td>
									<td>בן יעקב</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="21" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$21')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						49
									</td>
									<td>D.S. Webstors</td>
									<td>8521482</td>
									<td>פליקס</td>
									<td>לשנו</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="22" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$22')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						51
									</td>
									<td>Network Payment Soloutions</td>
									<td>9183393</td>
									<td>DAvid</td>
									<td>Collins</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="23" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$23')">
									<td>
										<span style="background-color: #7D7D7D;" class="legend-item"></span>
										&nbsp;
						52
									</td>
									<td>Hayashir.com</td>
									<td>6773053</td>
									<td>גיל</td>
									<td>עופר</td>
									<td>&nbsp;</td>
								</tr>
								<tr style="cursor: pointer;" order="24" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$24')">
									<td>
										<span style="background-color: #9E6CFF;" class="legend-item"></span>
										&nbsp;
						58
									</td>
									<td>פוקט</td>
									<td>1575448</td>
									<td>שגיא</td>
									<td>קליין</td>
									<td>&nbsp;</td>
								</tr>
							</tbody>
						</table>
					</div>

				</div>
				<div id="BodyContent_PageController_ListView_ctl01_ListSection1_Footer" class="panel-footer clearfix">

					<div style="text-align: center;">
						<div class="btn-group pull-left">
							<button id="BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnExport" class="btn btn-success" value="Export" data-toggle="dropdown">Export</button>
							<button data-toggle="dropdown" class="btn btn btn-success dropdown-toggle">
								<span class="caret"></span>
							</button>
							<ul id="BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnExportMenu" class="dropdown-menu">
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$btnExport','ExcelHtml')" onclick="$('#BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnExportMenu').hide();">Excel</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$btnExport','CSV')" onclick="$('#BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnExportMenu').hide();">CSV</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$btnExport','Xml')" onclick="$('#BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnExportMenu').hide();">Xml</a></li>
							</ul>
						</div>
						<div id="BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_dvPages" style="margin-left: 35%;">
							<ul class="pagination" style="display: inline;">
								<li class="active"><a>1</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$2')">2</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$3')">3</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$4')">4</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$5')">5</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$6')">6</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$7')">7</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$8')">8</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$9')">9</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$10')">10</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$11')">11</a></li>
								<li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$53')" class="lastPage">»</a></li>
							</ul>
						</div>
						<input name="ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$btnAdd" value="Add" onclick="javascript: __doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$btnAdd','')" id="BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnAdd" class="btn btn-cons-short btn-primary margin-left-5 pull-right"
							type="button">
					</div>
					<div class="clearfix"></div>

				</div>
			</div>
		</div>
		<div id="Dv3" class="col-lg-4">
			<!-- Only in list Section -->
			<div class="wrap-close-tab"><i class="fa fa-expand close-window"></i></div>
			<!-- End in list Section -->
			<div style="display: block;" id="BodyContent_PageController_FormView" class="" animation="Fade" showshadow="false">
				<div id="BodyContent_PageController_FormView_FormTabs">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#BodyContent_PageController_FormView_RefundRequest">RefundRequest</a></li>
						<li><a data-toggle="tab" href="#BodyContent_PageController_FormView_Data">Data</a></li>
						<li><a data-toggle="tab" href="#BodyContent_PageController_FormView_Chargeback">Chargeback</a></li>
						<li><a data-toggle="tab" href="#BodyContent_PageController_FormView_BlockItems">BlockItems</a></li>
					</ul>
					<div class="wrap-tab-content">
						<div class="tab-content">
							<div id="BodyContent_PageController_FormView_RefundRequest" class="tab-pane active">
								<div class="row">
									<div class="col-lg-12 head-tab-panel">New Request</div>
									<div class="col-lg-12 tab-panel">
										<div>

											<div class="row">
												<div class="col-lg-12">
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl01_Label1">Amount</span>
														<input name="ctl00$BodyContent$PageController$FormView$ctl01$txtAmount" id="BodyContent_PageController_FormView_ctl01_txtAmount" class="form-control" type="text">
													</div>
												</div>
												<div class="col-lg-12">
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl01_Label2">Comment</span>
														<textarea name="ctl00$BodyContent$PageController$FormView$ctl01$txtComment" rows="3" cols="20" id="BodyContent_PageController_FormView_ctl01_txtComment" class="form-control"></textarea>
													</div>
												</div>
											</div>
											<input name="ctl00$BodyContent$PageController$FormView$ctl01$btnSave" value="Create" id="BodyContent_PageController_FormView_ctl01_btnSave" class="btn btn-primary" type="submit">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 head-tab-panel">Related Requests</div>
									<div class="col-lg-12 tab-panel">
										<div>
											<table class="table">
												<tbody>
													<tr>
														<th>Date</th>
														<th>Request Amount</th>
														<th>Refund Amount</th>
														<th>Status</th>
														<th>Comment</th>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div style="clear: both;"></div>
							</div>
							<div id="BodyContent_PageController_FormView_Data" class="tab-pane fade">
								<div style="clear: both;"></div>
								<div class="row">
									<div class="col-lg-12 head-tab-panel">Actions</div>
									<div class="col-lg-12 tab-panel">
										<div>
											<div class="row">
												<div class="col-lg-12">
													<ul>
														<li>
															<a id="BodyContent_PageController_FormView_ctl02_lbSendClientEmail" href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$ctl02$lbSendClientEmail','')">Email Client</a></li>
														<li>
															<a id="BodyContent_PageController_FormView_ctl02_lbSendMerchantEmail" href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$ctl02$lbSendMerchantEmail','')">Email Merchant</a></li>
														<li><a href="#" id="lbRefundAsk" onclick="var topContainer; for(var wnd = window; wnd != null; wnd = wnd.parent) { if (wnd.PopupContainer_ShowPopup) topContainer = wnd; if (wnd == wnd.parent) break;} topContainer.PopupContainer_ShowPopup('Refund Request', 500, '/admin/AdminCash/ContentFrame.aspx?control=%2fadmin%2fTransactions%2fRefundRequest.ascx&amp;ID=5370017&amp;Status=Captured');">Ask Refund</a></li>
														<li>
															<a id="BodyContent_PageController_FormView_ctl02_lbCreateInvoice" href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$ctl02$lbCreateInvoice','')">Create Invoice</a></li>
														<li>
															<a id="BodyContent_PageController_FormView_ctl02_hlLogChargeAttempts" href="../Log_ChargeAttempts.aspx?iTransNum=5370017&amp;iTransTbl=Captured" target="_blank">Charge Attempt</a></li>
														<li>
															<a id="BodyContent_PageController_FormView_ctl02_hlVirtualTerminal" href="../charge_form.asp?CompanyID=35&amp;tblName=Captured&amp;MerchantTransID=5370017" target="_top">Virtual Terminal
															</a></li>
													</ul>
												</div>
											</div>

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 head-tab-panel">General</div>
									<div class="col-lg-12 tab-panel">
										<div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<span>Date</span>
														4/28/2014 3:46:22 PM
													</div>
													<div class="form-group">
														<span>Amount</span>
														USD 10.00
													</div>
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label2">Installments</span>
														1
													</div>
													<div class="form-group">
														<span>Is Test Transaction</span>
														True
													</div>
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label4">Credit Type</span>
														Regular
													</div>
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label5">Trans Type</span>
														0
													</div>
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label3">MerchantID</span>
														35
													</div>
													<div class="form-group">
														<span>Order Number</span>

													</div>
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label6">Pay For</span>
														TShirt
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label7">Product Id</span>

													</div>
													<div class="form-group">
														<span>TransactionSource</span>
														RemoteCharge
													</div>
													<div class="form-group">
														<span>Payment Method Name</span>
														Visa
													</div>
													<div class="form-group">
														<span>Payment Method Display</span>
														Visa .... 1111
													</div>
													<div class="form-group">
														<span>IP</span>
														192.168.0.12
													</div>
													<div class="form-group">
														<span>IP Country</span>
														--
													</div>
													<div class="form-group">
														<span>Recurring SeriesID</span>

													</div>
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label8">Passed TransactionID</span>

													</div>
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label9">Comment</span>

													</div>
												</div>
											</div>

										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-12 head-tab-panel">Payer</div>
									<div class="col-lg-12 tab-panel">
										<div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<span>Full Name</span>
														<span>Sahgir Mouhaset</span>
													</div>
													<div class="form-group">
														<span>Invoice Name</span>
														<span></span>
													</div>
													<div class="form-group">
														<span>Personal Number</span>
														<span></span>
													</div>
													<div class="form-group">
														<span>Phone Number</span>
														<span>35799895623</span>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<span>Email Address</span>
														<span>loukas.koniotis@lamdagroup.com</span>
													</div>
													<div class="form-group">
														<span>Comment</span>
														<span></span>
													</div>
													<div class="form-group">
														<span>Birth Date</span>
														<span></span>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-12 head-tab-panel">Payment</div>
									<div class="col-lg-12 tab-panel">
										<div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label10">Display</span>
														Visa...1111
													</div>
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label11">What Payment method do I want to configure</span>
														<span>CCVisa</span>
													</div>
													<div class="form-group">
														<span>First 6</span>
														411111
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<span>Last 4</span>
														1111
													</div>
													<div class="form-group">
														<span>Expiration Date</span>
														6/30/2015
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 head-tab-panel">Billing Address</div>
									<div class="col-lg-12 tab-panel">
										<div>

											<div>Malamatinas</div>
											<div>Malamatinas</div>
											<div>Limassol 564893</div>
											<div>Cyprus</div>

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 head-tab-panel">Fees</div>
									<div class="col-lg-12 tab-panel">
										<div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<span>Transaction</span>
														USD 0.00
													</div>
													<div class="form-group">
														<span>Ratio</span>
														USD 0.35
													</div>
													<div class="form-group">
														<span>Clarification</span>
														USD 0.00
													</div>
													<div class="form-group">
														<span>Chargeback</span>
														USD 0.00
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<span>Handling</span>
														USD 0.00
													</div>
													<div class="form-group">
														<span>Debit</span>
														USD 0.00
													</div>
													<div class="form-group">
														<span>DebitFee</span>
														USD 0.00
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 head-tab-panel">Debit Company</div>
									<div class="col-lg-12 tab-panel">
										<div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<span>Debit Company</span>
														Lamda
													</div>
													<div class="form-group">
														<span>Terminal Number</span>
														63000001
													</div>
													<div class="form-group">
														<span>Reference Code</span>
														1230517500
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<span>Reference Num</span>
														1230517500
													</div>
													<div class="form-group">
														<span id="BodyContent_PageController_FormView_ctl02_Label1">Approval Number</span>
														lpr20140428154510770
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>


								<div style="clear: both;"></div>
							</div>
							<div id="BodyContent_PageController_FormView_Chargeback" class="tab-pane fade">
								<div class="row">
									<div class="col-lg-12 head-tab-panel">Actions</div>
									<div class="col-lg-12 tab-panel">
										<div>

											<div class="form-group">
												<span class="fieldText">Select Chb Reason</span>
												<div class="fieldValue">
													<select name="ctl00$BodyContent$PageController$FormView$ctl03$ddlReason" id="BodyContent_PageController_FormView_ctl03_ddlReason" class="form-control">
														<option value="4801">Requested Data Transaction Not Received</option>
														<option value="4802">Requested/Required Information Illegible or Missing</option>
														<option value="4807">Warning Bulletin File</option>
														<option value="4808">Requested/Required Authorization Not Obtained</option>
														<option value="4812">Account Number Not on File</option>
														<option value="4831">Transaction Amount Differs</option>
														<option value="4834">Duplicate Processing</option>
														<option value="4835">Card Not Valid or Expired</option>
														<option value="4837">No Cardholder Authorization</option>
														<option value="4840">Fraudulent Processing of Transactions</option>
														<option value="4841">Canceled Recurring Transaction</option>
														<option value="4842">Late Presentment</option>
														<option value="4847">Requested/Required Authorization Not Obtained and Fraudulent Transaction</option>
														<option value="4849">Questionable Merchant Activity</option>
														<option value="4850">Credit Posted as a Purchase</option>
														<option value="4853">Cardholder Dispute-Defective/Not as Described</option>
														<option value="4854">Cardholder Dispute-Not Elsewhere Classified (U.S. Region Only)</option>
														<option value="4855">Non-receipt of Merchandise</option>
														<option value="4857">Card-Activated Telephone Transaction</option>
														<option value="4859">Services Not Rendered</option>
														<option value="4860">Credit Not Processed </option>
														<option value="4862">Counterfeit Transaction Magnetic Stripe POS Fraud</option>
														<option value="4863">Cardholder Does Not Recognize-Potential Fraud</option>
														<option value="6305">Does not agree with amount billed</option>
														<option value="6321">Cardholder does not recognize</option>
														<option value="6322">Chip transaction request</option>
														<option value="6323">Personal records request</option>
														<option value="6341">Fraud investigation</option>
														<option value="6342">Potential chargeback/compliance</option>
														<option value="28">Cardholder Requests Copy Bearing Signature.</option>
														<option value="29">Request for T &amp; E Documents.</option>
														<option value="30">Services/Merchandise was not received</option>
														<option value="33">Legal Process or Fraud Analysis</option>
														<option value="34">Legal Process Request</option>
														<option value="41">Cancelled Recurring Transaction</option>
														<option value="53">Not as Described or Defective</option>
														<option value="57">Fraudulent Multiple Drafts</option>
														<option value="60">Copy Illegible or Invalid</option>
														<option value="62">Counterfeit Transaction</option>
														<option value="70">No Verification/Exception File</option>
														<option value="71">Declined Authorization</option>
														<option value="72">No Authorization</option>
														<option value="73">Media Required</option>
														<option value="74">Late Presentment</option>
														<option value="75">Media Required</option>
														<option value="76">Incorrect Transaction Code</option>
														<option value="77">Non Matching Account Number</option>
														<option value="78">Ineligible Transaction (International only)</option>
														<option value="79">Non-receipt of Sales Draft</option>
														<option value="80">Processing Error: Incorrect Amount or Account</option>
														<option value="81">Fraudulent Transaction û Card Present Environment</option>
														<option value="82">Duplicate Processing</option>
														<option value="83">Fraudulent Transaction û Card Absent Environment</option>
														<option value="85">Credit Not Processed</option>
														<option value="86">Altered Amount / Paid by Other Means</option>
														<option value="90">Non-Receipt of Cash or Merchandise</option>
														<option value="93">Risk Identification Service</option>
														<option value="96">Transaction Exceeds Limited Amount Terminal</option>
														<option value="98">A dispute was initiated from Menu ADJ with the intent of only needing a retrieval request.</option>
														<option value="99">A dispute was initiated from Menu ADJ because the item is in question.</option>

													</select>
												</div>
											</div>
											<div class="form-group">
												<span class="fieldText">More Text</span>
												<div class="fieldValue">
													<textarea name="ctl00$BodyContent$PageController$FormView$ctl03$txtText" rows="3" cols="20" id="BodyContent_PageController_FormView_ctl03_txtText" class="form-control"></textarea>
												</div>
											</div>
											<input name="ctl00$BodyContent$PageController$FormView$ctl03$btnUpdate" value="Update" id="BodyContent_PageController_FormView_ctl03_btnUpdate" class="btn btn-primary" type="submit">
										</div>
									</div>
								</div>
								<div style="clear: both;"></div>
							</div>
							<div id="BodyContent_PageController_FormView_BlockItems" class="tab-pane fade">
								<div class="row">
									<div class="col-lg-12 head-tab-panel">Available Blocks</div>
									<div class="col-lg-12 tab-panel">
										<div>




											<div class="row">
												<div class="col-lg-12">
													<div class="form-group">
														Payment Method Status:
			 <select name="ctl00$BodyContent$PageController$FormView$ctl04$ddlListType" id="BodyContent_PageController_FormView_ctl04_ddlListType" class="form-control">
				 <option selected="selected" value=""></option>
				 <option value="0">Bronze</option>
				 <option value="1">Silver</option>
				 <option value="2">Gold</option>
				 <option value="3">Platinum</option>
				 <option value="-1">Black</option>
			 </select>

													</div>
												</div>
												<div class="col-lg-12">
													<div class="form-group">
														<span>Comment</span>
														<textarea name="ctl00$BodyContent$PageController$FormView$ctl04$txtComment" rows="3" cols="20" id="BodyContent_PageController_FormView_ctl04_txtComment" class="form-control"></textarea>
													</div>
												</div>
											</div>

											<input name="ctl00$BodyContent$PageController$FormView$ctl04$btnUpdate" value="Update" id="BodyContent_PageController_FormView_ctl04_btnUpdate" class="btn btn-primary" type="submit">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-12 head-tab-panel">Exsiting Blocks</div>
									<div class="col-lg-12 tab-panel">
										<div>

											<table class="table">
												<tbody>
													<tr>
														<th>Date</th>
														<th>Source</th>
														<th>Type</th>
														<th>Value</th>
														<th>List</th>
														<th>Comment</th>
														<th>Delete</th>
													</tr>

												</tbody>
											</table>

										</div>
									</div>
								</div>
								<div style="clear: both;"></div>
							</div>
						</div>
						<div class="panel-footer clearfix">
							<div id="BodyContent_PageController_FormView_DataButtons" style="text-align: center;">
								<div id="BodyContent_PageController_FormView_DataButtons_dvPages" style="margin-left: 35%;">
								</div>
								<input name="ctl00$BodyContent$PageController$FormView$DataButtons$btnDelete" value="Delete" onclick="return confirm('');" id="BodyContent_PageController_FormView_DataButtons_btnDelete" class="btn btn-cons-short btn-danger margin-left-5 pull-right" type="submit"><input name="ctl00$BodyContent$PageController$FormView$DataButtons$btnSave"
									value="Save" onclick="    javascript: __doPostBack('ctl00$BodyContent$PageController$FormView$DataButtons$btnSave', '')" id="BodyContent_PageController_FormView_DataButtons_btnSave" class="btn btn-cons-short btn-info margin-left-5 pull-right" type="button">
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<asp:Button ClientIDMode="Static" ID="buttonOp" runat="server" Text="Button" />
		</div>
	</div>


</asp:Content>
