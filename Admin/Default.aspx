﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Netpay.Admin.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:MultiView runat="server" ID="MvDefault" ActiveViewIndex='<%# IsErrorMode? 0 : (IsNotAuthorizedMode? 2 : 1) %>'>
        <asp:View runat="server">
            <div class="ErrorMessageDiv">
                <asp:Image runat="server" ImageUrl="~/Images/error-image.png" CssClass="ErrorMsgImg" />
                <h2 class="ErrorMsgTitle">We seem to have encountered an error - no need to do anything, our system admin has been notified and we are working to resolve the issue. </h2>
            </div>
        </asp:View>
        <asp:View runat="server">
            <div class="row">
                <div runat="server" id="phQuickLinks" class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Quick Links</div>
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <admin:MultiSearch runat="server" ID="msQuickSearch" DataSource='<%# Netpay.Admin.Application.Current.QuickSearch.ChildItems %>' DataTextField="Title" DataValueField="Url" ShowSearchButton="true" PlaceHolder="Search..." ViewStateMode="Disabled" />
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer clearfix">
                        </div>
                    </div>
                </div>

                <div runat="server" id="dvStatus" class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Status</div>
                        <div class="panel-body">
                            <asp:PlaceHolder ID="phStatus" runat="server" />
                        </div>
                        <div class="panel-footer clearfix">
                        </div>
                    </div>
                </div>
                <div runat="server" id="dvNotifications" class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Notifications</div>
                        <div class="panel-body">
                            <asp:MultiView runat="server" ActiveViewIndex='<%# Netpay.Admin.Application.Current.Modules.Where(x => x.Name == "Home Page Notifications").Single().IsActive? 0 : 1 %>'>
                                <asp:View runat="server">
                                    <asp:PlaceHolder runat="server" ID="phNotifications" />
                                </asp:View>
                                <asp:View runat="server">
                                    <div class="alert alert-danger">Notifications module is deactivated.</div>
                                </asp:View>
                            </asp:MultiView>
                        </div>

                        <div class="panel-footer clearfix">
                        </div>
                    </div>
                </div>
            </div>

        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-danger">
                <b>Module Not Authorized : <%#NotAuthorizedModuleName %></b><br />
                <b>Secured Object Name : <%#NotAuthorizedSecuredObjectName %></b><br />
                <b>Permission needed : <%# RequiredPermission %></b>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
