﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="TestPage.aspx.cs" Inherits="Netpay.Admin.TestPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">



    <div id="Div1" class="adminPageData col-lg-12">  <a id="BodyContent_PageController_ListButtonClose" class="wrap-close-list btn btn-default btn-mini" onclick="DataTableController_ChangeMode('BodyContent_PageController', $('#BodyContent_PageController_FilterTd').css('display') == 'none' ? 0 : 1);"><i src="../../../Images/DataTableArrowLeft.png" class="fa fa-expand" id="BodyContent_PageController_FilterTdSeperatorImage"></i></a><div id="BodyContent_PageController_ListView" class="" animation="Fade" showshadow="false">
		<div id="BodyContent_PageController_ListView_ctl01_ListSection1" class="panel panel-default">
			<div id="BodyContent_PageController_ListView_ctl01_ListSection1_Header" class="panel-heading wrap-legend">
				<i class="fa  fa-2x box-size-font"></i> Merchants 
        <div class="btn-group pull-right">
					 <button data-toggle="dropdown" class="btn btn-info btn-cons-short dropdown-toggle"> Legend <span class="caret"></span></button> <ul class="dropdown-menu pull-right">  <li><a href="#"><span class="legend-list" style="background:#7D7D7D;"></span> Archived</a></li>  <li><a href="#"><span class="legend-list" style="background:#FF8040;"></span> Blocked</a></li>  <li><a href="#"><span class="legend-list" style="background:#FF6666;"></span> Closed</a></li>  <li><a href="#"><span class="legend-list" style="background:#FFEB65;"></span> Integration</a></li>  <li><a href="#"><span class="legend-list" style="background:#9E6CFF;"></span> LoginOnly</a></li>  <li><a href="#"><span class="legend-list" style="background:#6699CC;"></span> New</a></li>  <li><a href="#"><span class="legend-list" style="background:#66CC66;"></span> Processing</a></li> </ul>
				</div>
    
			</div><div id="BodyContent_PageController_ListView_ctl01_ListSection1_Body" class="panel-body">
				
        <div class="table-responsive">
					<table class="table table-striped" selectedindex="-1" id="Table1" style="border-width:0px;width:100%;border-collapse:collapse;" cellspacing="0">
						<thead>
							<tr order="-1">
								<th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$ID')">ID</a></th><th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$Name')">Name</a></th><th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$Number')">Number</a></th><th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$ContactFirstName')">Contact First Name</a></th><th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$ContactLastName')">Contact First Name</a></th><th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$AccountManager')">Account Manager</a></th>
							</tr>
						</thead><tbody>
							<tr style="cursor: pointer;" order="0" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$0')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    1
				    </td><td>Mahala</td><td>2759394</td><td>Eliad</td><td>Saporta</td><td>eliad</td>
							</tr><tr style="cursor: pointer;" order="1" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$1')">
								<td>
					    <span style="background-color:#FFEB65;" class="legend-item"></span>&nbsp;
					    2
				    </td><td>Mahala test</td><td>8363988</td><td>eliad</td><td>sapporta</td><td>&nbsp;</td>
							</tr><tr class="selected-row" style="cursor: pointer;" order="2" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$2')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    3
				    </td><td>Keep The Faith</td><td>9379282</td><td>Joenethia</td><td>White</td><td>Robert</td>
							</tr><tr style="cursor: pointer;" order="3" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$3')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    4
				    </td><td>Dev-Testing</td><td>4699001</td><td>Liran</td><td>Patimer</td><td>&nbsp;</td>
							</tr><tr style="cursor: pointer;" order="4" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$4')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    5
				    </td><td>SERV International</td><td>3561732</td><td>Joseph</td><td>Garcia</td><td>Robert</td>
							</tr><tr style="cursor: pointer;" order="5" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$5')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    6
				    </td><td>Mandy Gawley Music</td><td>3739325</td><td>Mandy</td><td>Gawley</td><td>Robert</td>
							</tr><tr style="cursor: pointer;" order="6" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$6')">
								<td>
					    <span style="background-color:#FFEB65;" class="legend-item"></span>&nbsp;
					    7
				    </td><td>iENT</td><td>1209666</td><td>&nbsp;</td><td>&nbsp;</td><td>Robert</td>
							</tr><tr style="cursor: pointer;" order="7" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$7')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    8
				    </td><td>Michael Van Uum	</td><td>5402081</td><td>Michael</td><td>Van Uum</td><td>&nbsp;</td>
							</tr><tr style="cursor: pointer;" order="8" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$8')">
								<td>
					    <span style="background-color:#9E6CFF;" class="legend-item"></span>&nbsp;
					    9
				    </td><td>VIDAROO CORP</td><td>3739027</td><td>THOMAS</td><td>MORELAND</td><td>Robert</td>
							</tr><tr style="cursor: pointer;" order="9" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$9')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    10
				    </td><td>Carly Shoe</td><td>7559359</td><td>Carly</td><td>Shoe</td><td>Robert</td>
							</tr><tr style="cursor: pointer;" order="10" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$10')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    11
				    </td><td>Jose E. Creamer</td><td>3743113</td><td>Jose</td><td>E Creamer</td><td>Robert</td>
							</tr><tr style="cursor: pointer;" order="11" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$11')">
								<td>
					    <span style="background-color:#FFEB65;" class="legend-item"></span>&nbsp;
					    12
				    </td><td>Demo Account</td><td>8556212</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr><tr style="cursor: pointer;" order="12" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$12')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    13
				    </td><td>Generations Ministries Inc</td><td>6491332</td><td>robert l</td><td>howze</td><td>Robert</td>
							</tr><tr style="cursor: pointer;" order="13" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$13')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    14
				    </td><td>ATLiance Inc</td><td>1179749</td><td>de brandon</td><td>white</td><td>Robert</td>
							</tr><tr order="14" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$14')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    15
				    </td><td>Solutions AE inc</td><td>3260763</td><td>Ana</td><td>Everett</td><td>Robert</td>
							</tr><tr order="15" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$15')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    16
				    </td><td>Triumph Baptist Church</td><td>3701456</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr><tr order="16" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$16')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    17
				    </td><td>Giving On The Go</td><td>3148563</td><td>&nbsp;</td><td>&nbsp;</td><td>Robert</td>
							</tr><tr order="17" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$17')">
								<td>
					    <span style="background-color:#FFEB65;" class="legend-item"></span>&nbsp;
					    18
				    </td><td>Mahala demo</td><td>5177358</td><td>Eliad</td><td>Saporta</td><td>&nbsp;</td>
							</tr><tr order="18" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$18')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    19
				    </td><td>Christ the Healer Church Inc</td><td>6628648</td><td>Calvin</td><td>Locket</td><td>&nbsp;</td>
							</tr><tr order="19" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$19')">
								<td>
					    <span style="background-color:#6699CC;" class="legend-item"></span>&nbsp;
					    20
				    </td><td>Warren Demo</td><td>6884151</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr><tr order="20" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$20')">
								<td>
					    <span style="background-color:#6699CC;" class="legend-item"></span>&nbsp;
					    21
				    </td><td>eliad saporta</td><td>8807271</td><td>Eliad</td><td>Saporta</td><td>&nbsp;</td>
							</tr><tr order="21" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$21')">
								<td>
					    <span style="background-color:#FFEB65;" class="legend-item"></span>&nbsp;
					    22
				    </td><td>First Step Consultants</td><td>6867225</td><td>Tony</td><td>Byrd</td><td>Robert</td>
							</tr><tr order="22" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$22')">
								<td>
					    <span style="background-color:#FFEB65;" class="legend-item"></span>&nbsp;
					    23
				    </td><td>Crash The Sky</td><td>5564523</td><td>Dale</td><td>Resteghini</td><td>&nbsp;</td>
							</tr><tr order="23" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$23')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    24
				    </td><td>Saint James home of fresh start</td><td>8737891</td><td>James R.</td><td>Woodson</td><td>Robert</td>
							</tr><tr order="24" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$24')">
								<td>
					    <span style="background-color:#66CC66;" class="legend-item"></span>&nbsp;
					    25
				    </td><td>iflixtv.com</td><td>7428402</td><td>Greg</td><td>Galloway</td><td>vanuum</td>
							</tr>
						</tbody>
					</table>
				</div>
    
			</div><div id="BodyContent_PageController_ListView_ctl01_ListSection1_Footer" class="panel-footer clearfix">
				
        <div style="text-align:center;">
					<div class="btn-group pull-left"><button id="BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnExport" class="btn btn-success" value="Export" data-toggle="dropdown">Export</button><button data-toggle="dropdown" class="btn btn btn-success dropdown-toggle"><span class="caret"></span></button><ul id="BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnExportMenu" class="dropdown-menu"><li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$btnExport','ExcelHtml')" onclick="$('#BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnExportMenu').hide();">Excel</a></li><li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$btnExport','CSV')" onclick="$('#BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnExportMenu').hide();">CSV</a></li><li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$btnExport','Xml')" onclick="$('#BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnExportMenu').hide();">Xml</a></li></ul></div><div id="BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_dvPages" style="margin-left:35%;">
						<ul class="pagination" style="display:inline;"><li class="active"><a>1</a></li> <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$2')">2</a></li> <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$3')">3</a></li> <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$dvPages','Page$3')" class="lastPage">»</a></li> </ul>
					</div><input name="ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$btnAdd" value="Add" onclick="javascript: __doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$ctl01$btnAdd', '')" id="BodyContent_PageController_ListView_ctl01_ListSection1_ctl01_btnAdd" class="btn btn-cons-short btn-primary margin-left-5 pull-right" type="button">
				</div><div class="clearfix"></div>
    
			</div>
		</div>
	</div></div>



    <div class="row">
            <div id="BodyContent_PageController_ListTd" class="adminPageData col-lg-6" style="background-color: yellow; height: 600px;">
              
            </div>
            <div id="BodyContent_PageController_ContentTd" class="col-lg-6" style="background-color: #FF6666; height: 600px;">
             
            </div>
    </div>

    <div id="BodyContent_PageController_FormView_FormPanel" class="panel panel-default">
        <div id="BodyContent_PageController_FormView_FormPanel_Header" class="panel-heading">
            <i class="fa  fa-2x box-size-font"></i>Data 

        </div>
        <div id="BodyContent_PageController_FormView_FormPanel_Body" class="panel-body">
            <script type="text/javascript">
                $(document).ready(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>
            <ul class="list-inline">
                <li><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Default tooltip">Tooltip</a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="right" data-original-title="Another tooltip">Another tooltip</a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="A much longer tooltip to demonstrate the max-width of the Bootstrp tooltip.">Large tooltip</a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="left" data-original-title="The last tip!">Last tooltip</a></li>
            </ul>


            <div id="BodyContent_PageController_FormView_FormPanel_FormTabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_Data">Data</a></li>
                    <li class=""><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_File___Notes">File &amp; Notes</a></li>
                    <li class=""><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_Stored_PM">Stored PM</a></li>
                    <li class=""><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_Summary">Summary</a></li>
                    <li class=""><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_Process">Process</a></li>
                    <li class=""><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_Risk">Risk</a></li>
                    <li class=""><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_Terminals">Terminals</a></li>
                    <li class=""><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_Mobile">Mobile</a></li>
                    <li class=""><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_Fees">Fees</a></li>
                    <li class=""><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_Hosted">Hosted</a></li>
                    <li class=""><a data-toggle="tab" href="#BodyContent_PageController_FormView_FormPanel_Content">Content</a></li>
                </ul>
                <div class="col-lg-12">

                    <div class="tab-content">
                        <div id="BodyContent_PageController_FormView_FormPanel_Data" class="tab-pane active">
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-12 head-tab-panel">Login Information</div>
                                        <div class="col-lg-12 tab-panel">
                                            <div>

                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="form-group">
                                                            Username:
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$txtUserName" value="lavi1234" id="BodyContent_PageController_FormView_FormPanel_ctl00_txtUserName" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="form-group">
                                                            Email Address:
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$txtEmailAddress" value="lavi@netpay.co.il" id="BodyContent_PageController_FormView_FormPanel_ctl00_txtEmailAddress" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="form-group">
                                                            <br>
                                                            <input id="BodyContent_PageController_FormView_FormPanel_ctl00_chkIsActive" name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$chkIsActive" checked="checked" type="checkbox">
                                                            Enabled
           
           
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">

                                                        <div class="form-group">
                                                            Password:
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$hfLoginID" id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_hfLoginID" value="505" type="hidden">
                                                            <a onclick="$('#BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgChangePassword').modal('show');;return false;" id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_btnChange" usesubmitbehavior="false" href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$btnChange','')">Change</a> |
                                                    <a id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_btnResetLock" usesubmitbehavior="false" href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$btnResetLock','')">Reset Lock</a> |
                                                    <a id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_btnShow" usesubmitbehavior="false" href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$btnShow','')">Show</a>
                                                            <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgPasswordInfo" class="modal fade">
                                                                <div class="modal-dialog">
                                                                    <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgPasswordInfo_UpdatePanel" class="modal-content" showshadow="false">
                                                                        <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgPasswordInfo_Header" class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title"><i class="fa  fa-2x box-size-font"></i>Password Information</h4>
                                                                        </div>
                                                                        <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgPasswordInfo_Body" class="modal-body">
                                                                            Password:
                                                                    <br>
                                                                            Last Login:
                                                                    <br>
                                                                            Last Fail:
                                                                    <br>
                                                                            Lock Count
                                                                    <br>
                                                                            <div>
                                                                                <table class="table table-striped" selectedindex="-1" id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgPasswordInfo_rptLog" style="border-width: 0px; width: 100%; border-collapse: collapse;" cellspacing="0">
                                                                                    <thead>
                                                                                        <tr order="-1">
                                                                                            <th scope="col">Date</th>
                                                                                            <th scope="col">IP</th>
                                                                                            <th scope="col">Result</th>
                                                                                            <th scope="col">Text</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <tr order="0" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$rptLog','Select$0')">
                                                                                            <td>2/1/2015 12:50:03 PM</td>
                                                                                            <td>192.168.254.27</td>
                                                                                            <td>&nbsp;</td>
                                                                                            <td>Get Password</td>
                                                                                        </tr>
                                                                                        <tr order="1" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$rptLog','Select$1')">
                                                                                            <td>1/29/2015 5:49:50 PM</td>
                                                                                            <td>::1</td>
                                                                                            <td>&nbsp;</td>
                                                                                            <td>Get Password</td>
                                                                                        </tr>
                                                                                        <tr order="2" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$rptLog','Select$2')">
                                                                                            <td>1/29/2015 3:58:56 PM</td>
                                                                                            <td>::1</td>
                                                                                            <td>&nbsp;</td>
                                                                                            <td>Reset lock</td>
                                                                                        </tr>
                                                                                        <tr order="3" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$rptLog','Select$3')">
                                                                                            <td>1/26/2015 4:29:05 PM</td>
                                                                                            <td>&nbsp;</td>
                                                                                            <td>Success</td>
                                                                                            <td>&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr order="4" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$rptLog','Select$4')">
                                                                                            <td>1/26/2015 3:29:30 PM</td>
                                                                                            <td>::1</td>
                                                                                            <td>&nbsp;</td>
                                                                                            <td>Get Password</td>
                                                                                        </tr>
                                                                                        <tr order="5" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$rptLog','Select$5')">
                                                                                            <td>1/26/2015 3:15:48 PM</td>
                                                                                            <td>::1</td>
                                                                                            <td>&nbsp;</td>
                                                                                            <td>Get Password</td>
                                                                                        </tr>
                                                                                        <tr order="6" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$rptLog','Select$6')">
                                                                                            <td>1/25/2015 10:20:49 AM</td>
                                                                                            <td>&nbsp;</td>
                                                                                            <td>Success</td>
                                                                                            <td>&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr order="7" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$rptLog','Select$7')">
                                                                                            <td>1/22/2015 6:18:19 PM</td>
                                                                                            <td>&nbsp;</td>
                                                                                            <td>Success</td>
                                                                                            <td>&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr order="8" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$rptLog','Select$8')">
                                                                                            <td>1/22/2015 4:27:38 PM</td>
                                                                                            <td>&nbsp;</td>
                                                                                            <td>Success</td>
                                                                                            <td>&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr order="9" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$rptLog','Select$9')">
                                                                                            <td>1/22/2015 11:24:22 AM</td>
                                                                                            <td>&nbsp;</td>
                                                                                            <td>Success</td>
                                                                                            <td>&nbsp;</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                        </div>
                                                                        <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgPasswordInfo_Footer" class="modal-footer">

                                                                            <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgPasswordInfo_Pager1" style="margin-left: 35%;">
                                                                                <ul class="pagination" style="display: inline;">
                                                                                    <li class="active"><a>1</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$2')">2</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$3')">3</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$4')">4</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$5')">5</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$6')">6</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$7')">7</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$8')">8</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$9')">9</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$10')">10</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$11')">11</a></li>
                                                                                    <li><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgPasswordInfo$Pager1','Page$24')" class="lastPage">»</a></li>
                                                                                </ul>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgChangePassword" class="modal fade">
                                                                <div class="modal-dialog">
                                                                    <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgChangePassword_UpdatePanel" class="modal-content" showshadow="false">
                                                                        <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgChangePassword_Header" class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title"><i class="fa  fa-2x box-size-font"></i>Change Password</h4>
                                                                        </div>
                                                                        <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgChangePassword_Body" class="modal-body">


                                                                            <div class="form-group">
                                                                                New Password
           
                                                                        <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgChangePassword$txtPassword" id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgChangePassword_txtPassword" class="form-control" type="text">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                Confirm Password
           
                                                                        <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgChangePassword$txtConfirmPassword" id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgChangePassword_txtConfirmPassword" class="form-control" type="text">
                                                                            </div>

                                                                        </div>
                                                                        <div id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgChangePassword_Footer" class="modal-footer">

                                                                            <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgChangePassword$btnClose" value="Cancel" id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgChangePassword_btnClose" class="btn btn-inverse btn-cons-short" type="submit">
                                                                            <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl00$ucPassword$dlgChangePassword$btnSave" value="Save" id="BodyContent_PageController_FormView_FormPanel_ctl00_ucPassword_dlgChangePassword_btnSave" class="btn btn-primary btn-cons-short" type="submit">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 head-tab-panel">Merchant Information</div>
                                        <div class="col-lg-12 tab-panel">
                                            <div>


                                                <div class="row">

                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            Merchant Number
                4699001
               
                                                        </div>
                                                    </div>



                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Company Name
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtName" value="Dev-Testing" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtName" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Billing name
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtBillingName" value="test" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtBillingName" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Legal name
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtLegalName" value="test" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtLegalName" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Legal Number
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtLegalNumber" value="test" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtLegalNumber" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Descriptor
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtDescriptor" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtDescriptor" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Industry
               
                                                    <select name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$ddlIndustry" id="BodyContent_PageController_FormView_FormPanel_ctl04_ddlIndustry" class="form-control">
                                                        <option value="" class="emptyOption"></option>
                                                        <option value="1">E-Commerce</option>
                                                        <option value="2">Adult</option>
                                                        <option value="3">Dating</option>
                                                        <option value="4">Gaming</option>
                                                        <option value="5">Pharmacy</option>
                                                        <option value="6">Computer Software</option>
                                                        <option value="7">Direct Marketing</option>
                                                        <option value="8">Money Transfer</option>
                                                        <option value="9">E-wallet</option>
                                                        <option value="10">Prepay Wallet</option>
                                                        <option value="11">Forex</option>
                                                        <option value="12">Binary Options</option>
                                                        <option value="20">Other</option>
                                                        <option selected="selected" value="0">[Deleted 0]</option>

                                                    </select>
                                                        </div>
                                                        <div class="form-group">
                                                            Identifier
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtIdentifier" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtIdentifier" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Website URL
                    
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtUrl" value="https://www.pizzahut.com" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtUrl" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Support Phone
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtSupportPhone" value="+1 (773) 2272666" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtSupportPhone" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Support Email
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtSupportEmail" value="info@pizzahut.com" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtSupportEmail" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Preferred language
            
               

                                                    <select name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$ddlPrefferedLanguage" id="BodyContent_PageController_FormView_FormPanel_ctl04_ddlPrefferedLanguage" class="form-control">
                                                        <option selected="selected" value="Hebrew">Hebrew</option>
                                                        <option value="English">English</option>

                                                    </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Parent company
               
                                                    <select name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$ddlParentCompany" id="BodyContent_PageController_FormView_FormPanel_ctl04_ddlParentCompany" class="form-control">
                                                    </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Account manager
               
                                                    <select name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$ddlAccountManager" id="BodyContent_PageController_FormView_FormPanel_ctl04_ddlAccountManager" class="form-control">
                                                        <option selected="selected" value="" class="emptyOption"></option>
                                                        <option value="1">Udi Azulay</option>
                                                        <option value="2">Eliad Saporta</option>
                                                        <option value="3">lavi</option>
                                                        <option value="4">Robert Shoe</option>
                                                        <option value="5">Sonny Fisher</option>
                                                        <option value="6">sharon</option>
                                                        <option value="7">michal</option>
                                                        <option value="8">lavitest</option>
                                                        <option value="9">van uum</option>
                                                        <option value="10">Ed</option>
                                                        <option value="11">sonny</option>
                                                        <option value="12">eds</option>
                                                        <option value="13">michael</option>
                                                        <option value="14">rj</option>
                                                        <option value="15">WinServiceUser</option>
                                                        <option value="16">Liran Patimer</option>
                                                        <option value="17">liran</option>

                                                    </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Group
               
                                                    <select name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$dllMerchantGroup" id="BodyContent_PageController_FormView_FormPanel_ctl04_dllMerchantGroup" class="form-control">
                                                        <option selected="selected" value="" class="emptyOption"></option>
                                                        <option value="1">Giving on the GO</option>
                                                        <option value="2">Priemier payments</option>
                                                        <option value="3">Adver Buying</option>

                                                    </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Department
               
                                                    <select name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$ddlDepartment" id="BodyContent_PageController_FormView_FormPanel_ctl04_ddlDepartment" class="form-control">
                                                        <option selected="selected" value="" class="emptyOption"></option>
                                                        <option value="1">Music - Hip Hop</option>
                                                        <option value="2">Music - Rock</option>
                                                        <option value="3">Music - Country</option>
                                                        <option value="4">Music - Gospel</option>
                                                        <option value="5">Music - EDM</option>
                                                        <option value="6">Music - Classical</option>
                                                        <option value="7">Entertainment - Tickets &amp; Experiences</option>
                                                        <option value="8">Entertainment - Extreme sports</option>
                                                        <option value="9">Non-Profits</option>
                                                        <option value="10">Social Good</option>
                                                        <option value="11">Retail - Apparel</option>
                                                        <option value="12">Retail - Jewelry</option>
                                                        <option value="13">Retail - Health &amp; Beauty</option>
                                                        <option value="14">Retail -  Baby &amp; Kids</option>
                                                        <option value="15">Retail - Sporting Goods</option>
                                                        <option value="16">Retail - Automotive</option>
                                                        <option value="17">Retail - Antiques</option>
                                                        <option value="18">Retail - Art </option>
                                                        <option value="19">Retail - baby</option>
                                                        <option value="20">Retail - books</option>
                                                        <option value="21">Retail - Business &amp; Industrial</option>
                                                        <option value="22">Retail - Collectibles</option>
                                                        <option value="23">Retail - Computers</option>
                                                        <option value="24">Retail - Misc. consumer electronics</option>

                                                    </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Link Name
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtLinkName" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtLinkName" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    Open date
            
               

                                                            <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtOpenDate" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtOpenDate" class="form-control hasDatepicker" date="1/1/1900 12:00:00 AM" type="text">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    Close date
            
               

                                                            <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtCloseDate" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtCloseDate" class="form-control hasDatepicker" date="1/1/1900 12:00:00 AM" type="text">
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            Comment
            
               

                                                    <textarea name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtComment" rows="3" cols="20" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtComment" class="form-control"></textarea>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12 head-tab-panel">Business Address</div>
                                        <div class="col-lg-12 tab-panel">
                                            <div>



                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="form-group">
                                                            Address Line 1 
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acBusinessAddress$txtAddress1" value="1820 W. Webster Ave" id="BodyContent_PageController_FormView_FormPanel_ctl04_acBusinessAddress_txtAddress1" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Address Line 2 
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acBusinessAddress$txtAddress2" id="BodyContent_PageController_FormView_FormPanel_ctl04_acBusinessAddress_txtAddress2" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="form-group">
                                                            City 
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acBusinessAddress$txtCity" value="Chicago" id="BodyContent_PageController_FormView_FormPanel_ctl04_acBusinessAddress_txtCity" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Postal 
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acBusinessAddress$txtPostal" value="56735" id="BodyContent_PageController_FormView_FormPanel_ctl04_acBusinessAddress_txtPostal" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4">
                                                        <div class="form-group">
                                                            State 
           
                                                    <select name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acBusinessAddress$ddlState" id="BodyContent_PageController_FormView_FormPanel_ctl04_acBusinessAddress_ddlState" class="form-control">
                                                        <option selected="selected" value="" class="emptyOption"></option>
                                                        <option value="1">Alabama</option>
                                                        <option value="3">Alaska</option>
                                                        <option value="5">Arizona</option>
                                                        <option value="7">Arkansas</option>
                                                        <option value="68">Armed Forces Americas</option>
                                                        <option value="66">Armed Forces Europe, Middle East, Africa and Canada</option>
                                                        <option value="71">Armed Forces Pacific</option>
                                                        <option value="9">California</option>
                                                        <option value="11">Colorado</option>
                                                        <option value="13">Connecticut</option>
                                                        <option value="15">Delaware</option>
                                                        <option value="17">District of Columbia</option>
                                                        <option value="19">Florida</option>
                                                        <option value="21">Georgia</option>
                                                        <option value="25">Hawaii</option>
                                                        <option value="27">Idaho</option>
                                                        <option value="29">Illinois</option>
                                                        <option value="31">Indiana</option>
                                                        <option value="33">Iowa</option>
                                                        <option value="35">Kansas</option>
                                                        <option value="37">Kentucky</option>
                                                        <option value="39">Louisiana</option>
                                                        <option value="41">Maine</option>
                                                        <option value="43">Maryland</option>
                                                        <option value="45">Massachusetts</option>
                                                        <option value="47">Michigan</option>
                                                        <option value="49">Minnesota</option>
                                                        <option value="65">Mississippi</option>
                                                        <option value="2">Missouri</option>
                                                        <option value="4">Montana</option>
                                                        <option value="6">Nebraska</option>
                                                        <option value="8">Nevada</option>
                                                        <option value="10">New Hampshire</option>
                                                        <option value="12">New Jersey</option>
                                                        <option value="14">New Mexico</option>
                                                        <option value="16">New York</option>
                                                        <option value="18">North Carolina</option>
                                                        <option value="20">North Dakota</option>
                                                        <option value="22">Ohio</option>
                                                        <option value="23">Oklahoma</option>
                                                        <option value="24">Oregon</option>
                                                        <option value="26">Pennsylvania</option>
                                                        <option value="28">Rhode Island</option>
                                                        <option value="30">South Carolina</option>
                                                        <option value="32">South Dakota</option>
                                                        <option value="34">Tennessee</option>
                                                        <option value="36">Texas</option>
                                                        <option value="38">Utah</option>
                                                        <option value="40">Vermont</option>
                                                        <option value="42">Virginia</option>
                                                        <option value="44">Washington</option>
                                                        <option value="46">West Virginia</option>
                                                        <option value="48">Wisconsin</option>
                                                        <option value="50">Wyoming</option>

                                                    </select>
                                                        </div>
                                                        <div class="form-group">
                                                            Country
          
                                                    <select name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acBusinessAddress$ddlCountry" id="BodyContent_PageController_FormView_FormPanel_ctl04_acBusinessAddress_ddlCountry" class="form-control">
                                                        <option value="" class="emptyOption"></option>
                                                        <option value="1">Afghanistan</option>
                                                        <option value="303">Aland Islands</option>
                                                        <option value="6">Albania</option>
                                                        <option value="7">Algeria</option>
                                                        <option value="8">American Samoa</option>
                                                        <option value="9">Andorra</option>
                                                        <option value="10">Angola</option>
                                                        <option value="240">Anguilla</option>
                                                        <option value="241">Antarctica</option>
                                                        <option value="12">Antigua</option>
                                                        <option value="13">Argentina</option>
                                                        <option value="14">Armenia</option>
                                                        <option value="242">Aruba</option>
                                                        <option value="17">Australia</option>
                                                        <option value="19">Austria</option>
                                                        <option value="20">Azerbaijan</option>
                                                        <option value="21">Bahamas</option>
                                                        <option value="22">Bahrain</option>
                                                        <option value="23">Bangladesh</option>
                                                        <option value="24">Barbados</option>
                                                        <option value="26">Belarus</option>
                                                        <option value="27">Belgium</option>
                                                        <option value="28">Belize</option>
                                                        <option value="29">Benin</option>
                                                        <option value="30">Bermuda</option>
                                                        <option value="243">Bhutan</option>
                                                        <option value="32">Bolivia</option>
                                                        <option value="33">Bosniaand Herzegovina</option>
                                                        <option value="34">Botswana</option>
                                                        <option value="244">Bouvet island</option>
                                                        <option value="35">Brazil</option>
                                                        <option value="301">British Indian Ocean Territory</option>
                                                        <option value="37">Brunei</option>
                                                        <option value="38">Bulgaria</option>
                                                        <option value="39">Burkina Faso</option>
                                                        <option value="40">Burundi</option>
                                                        <option value="41">Cambodia</option>
                                                        <option value="42">Cameroon</option>
                                                        <option value="43">Canada</option>
                                                        <option value="250">Cape verde</option>
                                                        <option value="45">Cayman Islands</option>
                                                        <option value="247">Central african republic</option>
                                                        <option value="47">Chad</option>
                                                        <option value="48">Chile</option>
                                                        <option value="49">China</option>
                                                        <option value="251">Christmas island</option>
                                                        <option value="245">Cocos (keeling) islands</option>
                                                        <option value="52">Colombia</option>
                                                        <option value="261">Comoros</option>
                                                        <option value="246">Congo, DRC</option>
                                                        <option value="54">Congo, ROC</option>
                                                        <option value="248">Cook islands</option>
                                                        <option value="57">Costa Rica</option>
                                                        <option value="58">Croatia</option>
                                                        <option value="59">Cuba</option>
                                                        <option value="60">Cyprus</option>
                                                        <option value="61">Czech Republic</option>
                                                        <option value="62">Denmark</option>
                                                        <option value="64">Djibouti</option>
                                                        <option value="252">Dominica</option>
                                                        <option value="66">Dominican Republic</option>
                                                        <option value="67">Ecuador</option>
                                                        <option value="68">Egypt</option>
                                                        <option value="69">El Salvador </option>
                                                        <option value="70">Equatorial Guinea</option>
                                                        <option value="71">Eritrea</option>
                                                        <option value="72">Estonia</option>
                                                        <option value="73">Ethiopia</option>
                                                        <option value="74">F.Y.R. of Macedonia</option>
                                                        <option value="254">Falkland islands (malvinas)</option>
                                                        <option value="255">Faroe islands</option>
                                                        <option value="77">Fiji Islands</option>
                                                        <option value="78">Finland</option>
                                                        <option value="79">France</option>
                                                        <option value="81">French Guiana</option>
                                                        <option value="82">French Polynesia</option>
                                                        <option value="302">French Southern and Antarctic Lands</option>
                                                        <option value="83">Gabon</option>
                                                        <option value="84">Gambia</option>
                                                        <option value="85">Georgia</option>
                                                        <option value="86">Germany</option>
                                                        <option value="87">Ghana</option>
                                                        <option value="88">Gibraltar</option>
                                                        <option value="89">Greece</option>
                                                        <option value="90">Greenland</option>
                                                        <option value="256">Grenada</option>
                                                        <option value="92">Guadeloupe</option>
                                                        <option value="93">Guam</option>
                                                        <option value="95">Guatemala</option>
                                                        <option value="298">Guernsey</option>
                                                        <option value="97">Guinea</option>
                                                        <option value="96">Guinea-Bissau</option>
                                                        <option value="98">Guyana</option>
                                                        <option value="99">Haiti</option>
                                                        <option value="300">Heard Island and McDonald Islands</option>
                                                        <option value="288">Holy see (vatican city state)</option>
                                                        <option value="100">Honduras</option>
                                                        <option value="101">Hong Kong SAR</option>
                                                        <option value="102">Hungary</option>
                                                        <option value="103">Iceland</option>
                                                        <option value="104">India</option>
                                                        <option value="105">Indonesia</option>
                                                        <option value="112">Iran</option>
                                                        <option value="113">Iraq</option>
                                                        <option value="114">Ireland</option>
                                                        <option value="296">Isle of Man</option>
                                                        <option value="115">Israel</option>
                                                        <option value="116">Italy</option>
                                                        <option value="117">Ivory Coast</option>
                                                        <option value="118">Jamaica</option>
                                                        <option value="119">Japan</option>
                                                        <option value="297">Jersey</option>
                                                        <option value="120">Jordan</option>
                                                        <option value="121">Kazakhstan</option>
                                                        <option value="122">Kenya</option>
                                                        <option value="260">Kiribati</option>
                                                        <option value="125">Korea</option>
                                                        <option value="124">Korea (North)</option>
                                                        <option value="234">Kuwait</option>
                                                        <option value="126">Kyrgyz Republic</option>
                                                        <option value="263">Lao, PDR</option>
                                                        <option value="128">Latvia</option>
                                                        <option value="129">Lebanon</option>
                                                        <option value="130">Lesotho</option>
                                                        <option value="131">Liberia</option>
                                                        <option value="132">Libya</option>
                                                        <option value="133">Liechtenstein</option>
                                                        <option value="134">Lithuania</option>
                                                        <option value="135">Luxembourg</option>
                                                        <option value="136">Macau</option>
                                                        <option value="137">Madagascar</option>
                                                        <option value="138">Malawi</option>
                                                        <option value="139">Malaysia</option>
                                                        <option value="140">Maldives</option>
                                                        <option value="141">Mali</option>
                                                        <option value="142">Malta</option>
                                                        <option value="265">Marshall islands</option>
                                                        <option value="144">Martinique</option>
                                                        <option value="145">Mauritania</option>
                                                        <option value="146">Mauritius</option>
                                                        <option value="293">Mayotte</option>
                                                        <option value="148">Mexico</option>
                                                        <option value="149">Micronesia</option>
                                                        <option value="150">Moldova</option>
                                                        <option value="151">Monaco</option>
                                                        <option value="152">Mongolia</option>
                                                        <option value="295">Montenegro</option>
                                                        <option value="267">Montserrat</option>
                                                        <option value="154">Morocco</option>
                                                        <option value="155">Mozambique</option>
                                                        <option value="156">Myanmar</option>
                                                        <option value="157">Namibia</option>
                                                        <option value="270">Nauru</option>
                                                        <option value="159">Nepal</option>
                                                        <option value="160">Netherlands</option>
                                                        <option value="161">Netherlands Antilles</option>
                                                        <option value="268">New caledonia</option>
                                                        <option value="164">New Zealand</option>
                                                        <option value="165">Nicaragua</option>
                                                        <option value="166">Niger</option>
                                                        <option value="167">Nigeria</option>
                                                        <option value="271">Niue</option>
                                                        <option value="269">Norfolk island</option>
                                                        <option value="266">Northern mariana islands</option>
                                                        <option value="170">Norway</option>
                                                        <option value="171">Oman</option>
                                                        <option value="172">Pakistan</option>
                                                        <option value="275">Palau</option>
                                                        <option value="274">Palestinian territory</option>
                                                        <option value="174">Panama</option>
                                                        <option value="175">Papua New Guinea</option>
                                                        <option value="176">Paraguay</option>
                                                        <option value="177">Peru</option>
                                                        <option value="178">Philippines</option>
                                                        <option value="273">Pitcairn</option>
                                                        <option value="179">Poland</option>
                                                        <option value="180">Portugal</option>
                                                        <option value="181">Puerto Rico</option>
                                                        <option value="182">Qatar</option>
                                                        <option value="276">R?union</option>
                                                        <option value="184">Romania</option>
                                                        <option value="186">Russia</option>
                                                        <option value="187">Rwanda</option>
                                                        <option value="278">Saint helena</option>
                                                        <option value="262">Saint kitts and nevis</option>
                                                        <option value="264">Saint lucia</option>
                                                        <option value="272">Saint pierre and miquelon</option>
                                                        <option value="289">Saint vincent &amp; the grenadines</option>
                                                        <option value="304">Saint-Barthélemy</option>
                                                        <option value="305">Saint-Martin (French part)</option>
                                                        <option value="190">San Marino</option>
                                                        <option value="280">Sao tome and principe</option>
                                                        <option value="192">Saudi Arabia</option>
                                                        <option value="193">Senegal</option>
                                                        <option value="294">Serbia</option>
                                                        <option value="249">Serbia and montenegro</option>
                                                        <option value="194">Seychelle Islands</option>
                                                        <option value="195">Sierra Leone</option>
                                                        <option value="196">Singapore</option>
                                                        <option value="197">Slovak Republic</option>
                                                        <option value="5">Slovenia</option>
                                                        <option value="277">Solomon islands</option>
                                                        <option value="3">Somalia</option>
                                                        <option value="2">South Africa</option>
                                                        <option value="299">South Georgia and the South Sandwich Islands</option>
                                                        <option value="306">South Sudan</option>
                                                        <option value="198">Spain</option>
                                                        <option value="199">SriLanka</option>
                                                        <option value="204">Sudan</option>
                                                        <option value="205">Suriname</option>
                                                        <option value="279">Svalbard and jan mayen</option>
                                                        <option value="206">Swaziland</option>
                                                        <option value="207">Sweden</option>
                                                        <option value="208">Switzerland</option>
                                                        <option value="209">Syria</option>
                                                        <option value="210">Taiwan</option>
                                                        <option value="211">Tajikistan</option>
                                                        <option value="212">Tanzania</option>
                                                        <option value="213">Thailand</option>
                                                        <option value="284">Timor-leste</option>
                                                        <option value="215">Togo</option>
                                                        <option value="283">Tokelau</option>
                                                        <option value="285">Tonga</option>
                                                        <option value="218">Trinidad and Tobago</option>
                                                        <option value="219">Tunisia</option>
                                                        <option value="220">Turkey</option>
                                                        <option value="221">Turkmenistan</option>
                                                        <option value="281">Turks and caicos islands</option>
                                                        <option value="286">Tuvalu</option>
                                                        <option value="229">U.S.Virgin Islands</option>
                                                        <option value="224">Uganda</option>
                                                        <option value="225">Ukraine</option>
                                                        <option value="226">United Arab Emirates</option>
                                                        <option value="227">United Kingdom</option>
                                                        <option selected="selected" value="228">United States</option>
                                                        <option value="230">Uruguay</option>
                                                        <option value="287">US minor outlying islands</option>
                                                        <option value="18">Uzbekistan</option>
                                                        <option value="291">Vanuatu</option>
                                                        <option value="55">Venezuela</option>
                                                        <option value="233">Vietnam</option>
                                                        <option value="290">Virgin islands, british</option>
                                                        <option value="292">Wallis and futuna</option>
                                                        <option value="253">Western sahara</option>
                                                        <option value="235">Western Samoa</option>
                                                        <option value="236">Yemen</option>
                                                        <option value="238">Zambia</option>
                                                        <option value="239">Zimbabwe</option>

                                                    </select>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="col-lg-12 head-tab-panel">Contact details</div>
                                        <div class="col-lg-12 tab-panel">
                                            <div>

                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            First Name
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtContactFirstName" value="Liran" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtContactFirstName" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Last Name
              
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtContactLastName" value="Patimer" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtContactLastName" class="form-control" type="text">
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Phone
               
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtContactPhone" value="+1 (773) 2272666" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtContactPhone" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Mobile Phone
                 
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtContactMobilePhone" value="+1 (773) 2272666" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtContactMobilePhone" class="form-control" type="text">
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Email
                
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$txtContactEmail" id="BodyContent_PageController_FormView_FormPanel_ctl04_txtContactEmail" class="form-control" type="text">
                                                        </div>

                                                    </div>
                                                </div>
                                                <input id="BodyContent_PageController_FormView_FormPanel_ctl04_chkIsInterestedInNewsletter" name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$chkIsInterestedInNewsletter" checked="checked" type="checkbox"><label for="BodyContent_PageController_FormView_FormPanel_ctl04_chkIsInterestedInNewsletter">Mailing - Agree to receive newsletters</label>

                                            </div>
                                        </div>


                                        <div class="col-lg-12 head-tab-panel">Address</div>
                                        <div class="col-lg-12 tab-panel">
                                            <div>



                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            Address Line 1 
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acPersonalAddress$txtAddress1" value="1820 W. Webster Ave" id="BodyContent_PageController_FormView_FormPanel_ctl04_acPersonalAddress_txtAddress1" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Address Line 2 
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acPersonalAddress$txtAddress2" id="BodyContent_PageController_FormView_FormPanel_ctl04_acPersonalAddress_txtAddress2" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            City 
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acPersonalAddress$txtCity" value="Chicago" id="BodyContent_PageController_FormView_FormPanel_ctl04_acPersonalAddress_txtCity" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Postal 
           
                                                    <input name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acPersonalAddress$txtPostal" value="56735" id="BodyContent_PageController_FormView_FormPanel_ctl04_acPersonalAddress_txtPostal" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            State 
           
                                                    <select name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acPersonalAddress$ddlState" id="BodyContent_PageController_FormView_FormPanel_ctl04_acPersonalAddress_ddlState" class="form-control">
                                                        <option selected="selected" value="" class="emptyOption"></option>
                                                        <option value="1">Alabama</option>
                                                        <option value="3">Alaska</option>
                                                        <option value="5">Arizona</option>
                                                        <option value="7">Arkansas</option>
                                                        <option value="68">Armed Forces Americas</option>
                                                        <option value="66">Armed Forces Europe, Middle East, Africa and Canada</option>
                                                        <option value="71">Armed Forces Pacific</option>
                                                        <option value="9">California</option>
                                                        <option value="11">Colorado</option>
                                                        <option value="13">Connecticut</option>
                                                        <option value="15">Delaware</option>
                                                        <option value="17">District of Columbia</option>
                                                        <option value="19">Florida</option>
                                                        <option value="21">Georgia</option>
                                                        <option value="25">Hawaii</option>
                                                        <option value="27">Idaho</option>
                                                        <option value="29">Illinois</option>
                                                        <option value="31">Indiana</option>
                                                        <option value="33">Iowa</option>
                                                        <option value="35">Kansas</option>
                                                        <option value="37">Kentucky</option>
                                                        <option value="39">Louisiana</option>
                                                        <option value="41">Maine</option>
                                                        <option value="43">Maryland</option>
                                                        <option value="45">Massachusetts</option>
                                                        <option value="47">Michigan</option>
                                                        <option value="49">Minnesota</option>
                                                        <option value="65">Mississippi</option>
                                                        <option value="2">Missouri</option>
                                                        <option value="4">Montana</option>
                                                        <option value="6">Nebraska</option>
                                                        <option value="8">Nevada</option>
                                                        <option value="10">New Hampshire</option>
                                                        <option value="12">New Jersey</option>
                                                        <option value="14">New Mexico</option>
                                                        <option value="16">New York</option>
                                                        <option value="18">North Carolina</option>
                                                        <option value="20">North Dakota</option>
                                                        <option value="22">Ohio</option>
                                                        <option value="23">Oklahoma</option>
                                                        <option value="24">Oregon</option>
                                                        <option value="26">Pennsylvania</option>
                                                        <option value="28">Rhode Island</option>
                                                        <option value="30">South Carolina</option>
                                                        <option value="32">South Dakota</option>
                                                        <option value="34">Tennessee</option>
                                                        <option value="36">Texas</option>
                                                        <option value="38">Utah</option>
                                                        <option value="40">Vermont</option>
                                                        <option value="42">Virginia</option>
                                                        <option value="44">Washington</option>
                                                        <option value="46">West Virginia</option>
                                                        <option value="48">Wisconsin</option>
                                                        <option value="50">Wyoming</option>

                                                    </select>
                                                        </div>
                                                        <div class="form-group">
                                                            Country
          
                                                    <select name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$acPersonalAddress$ddlCountry" id="BodyContent_PageController_FormView_FormPanel_ctl04_acPersonalAddress_ddlCountry" class="form-control">
                                                        <option value="" class="emptyOption"></option>
                                                        <option value="1">Afghanistan</option>
                                                        <option value="303">Aland Islands</option>
                                                        <option value="6">Albania</option>
                                                        <option value="7">Algeria</option>
                                                        <option value="8">American Samoa</option>
                                                        <option value="9">Andorra</option>
                                                        <option value="10">Angola</option>
                                                        <option value="240">Anguilla</option>
                                                        <option value="241">Antarctica</option>
                                                        <option value="12">Antigua</option>
                                                        <option value="13">Argentina</option>
                                                        <option value="14">Armenia</option>
                                                        <option value="242">Aruba</option>
                                                        <option value="17">Australia</option>
                                                        <option value="19">Austria</option>
                                                        <option value="20">Azerbaijan</option>
                                                        <option value="21">Bahamas</option>
                                                        <option value="22">Bahrain</option>
                                                        <option value="23">Bangladesh</option>
                                                        <option value="24">Barbados</option>
                                                        <option value="26">Belarus</option>
                                                        <option value="27">Belgium</option>
                                                        <option value="28">Belize</option>
                                                        <option value="29">Benin</option>
                                                        <option value="30">Bermuda</option>
                                                        <option value="243">Bhutan</option>
                                                        <option value="32">Bolivia</option>
                                                        <option value="33">Bosniaand Herzegovina</option>
                                                        <option value="34">Botswana</option>
                                                        <option value="244">Bouvet island</option>
                                                        <option value="35">Brazil</option>
                                                        <option value="301">British Indian Ocean Territory</option>
                                                        <option value="37">Brunei</option>
                                                        <option value="38">Bulgaria</option>
                                                        <option value="39">Burkina Faso</option>
                                                        <option value="40">Burundi</option>
                                                        <option value="41">Cambodia</option>
                                                        <option value="42">Cameroon</option>
                                                        <option value="43">Canada</option>
                                                        <option value="250">Cape verde</option>
                                                        <option value="45">Cayman Islands</option>
                                                        <option value="247">Central african republic</option>
                                                        <option value="47">Chad</option>
                                                        <option value="48">Chile</option>
                                                        <option value="49">China</option>
                                                        <option value="251">Christmas island</option>
                                                        <option value="245">Cocos (keeling) islands</option>
                                                        <option value="52">Colombia</option>
                                                        <option value="261">Comoros</option>
                                                        <option value="246">Congo, DRC</option>
                                                        <option value="54">Congo, ROC</option>
                                                        <option value="248">Cook islands</option>
                                                        <option value="57">Costa Rica</option>
                                                        <option value="58">Croatia</option>
                                                        <option value="59">Cuba</option>
                                                        <option value="60">Cyprus</option>
                                                        <option value="61">Czech Republic</option>
                                                        <option value="62">Denmark</option>
                                                        <option value="64">Djibouti</option>
                                                        <option value="252">Dominica</option>
                                                        <option value="66">Dominican Republic</option>
                                                        <option value="67">Ecuador</option>
                                                        <option value="68">Egypt</option>
                                                        <option value="69">El Salvador </option>
                                                        <option value="70">Equatorial Guinea</option>
                                                        <option value="71">Eritrea</option>
                                                        <option value="72">Estonia</option>
                                                        <option value="73">Ethiopia</option>
                                                        <option value="74">F.Y.R. of Macedonia</option>
                                                        <option value="254">Falkland islands (malvinas)</option>
                                                        <option value="255">Faroe islands</option>
                                                        <option value="77">Fiji Islands</option>
                                                        <option value="78">Finland</option>
                                                        <option value="79">France</option>
                                                        <option value="81">French Guiana</option>
                                                        <option value="82">French Polynesia</option>
                                                        <option value="302">French Southern and Antarctic Lands</option>
                                                        <option value="83">Gabon</option>
                                                        <option value="84">Gambia</option>
                                                        <option value="85">Georgia</option>
                                                        <option value="86">Germany</option>
                                                        <option value="87">Ghana</option>
                                                        <option value="88">Gibraltar</option>
                                                        <option value="89">Greece</option>
                                                        <option value="90">Greenland</option>
                                                        <option value="256">Grenada</option>
                                                        <option value="92">Guadeloupe</option>
                                                        <option value="93">Guam</option>
                                                        <option value="95">Guatemala</option>
                                                        <option value="298">Guernsey</option>
                                                        <option value="97">Guinea</option>
                                                        <option value="96">Guinea-Bissau</option>
                                                        <option value="98">Guyana</option>
                                                        <option value="99">Haiti</option>
                                                        <option value="300">Heard Island and McDonald Islands</option>
                                                        <option value="288">Holy see (vatican city state)</option>
                                                        <option value="100">Honduras</option>
                                                        <option value="101">Hong Kong SAR</option>
                                                        <option value="102">Hungary</option>
                                                        <option value="103">Iceland</option>
                                                        <option value="104">India</option>
                                                        <option value="105">Indonesia</option>
                                                        <option value="112">Iran</option>
                                                        <option value="113">Iraq</option>
                                                        <option value="114">Ireland</option>
                                                        <option value="296">Isle of Man</option>
                                                        <option value="115">Israel</option>
                                                        <option value="116">Italy</option>
                                                        <option value="117">Ivory Coast</option>
                                                        <option value="118">Jamaica</option>
                                                        <option value="119">Japan</option>
                                                        <option value="297">Jersey</option>
                                                        <option value="120">Jordan</option>
                                                        <option value="121">Kazakhstan</option>
                                                        <option value="122">Kenya</option>
                                                        <option value="260">Kiribati</option>
                                                        <option value="125">Korea</option>
                                                        <option value="124">Korea (North)</option>
                                                        <option value="234">Kuwait</option>
                                                        <option value="126">Kyrgyz Republic</option>
                                                        <option value="263">Lao, PDR</option>
                                                        <option value="128">Latvia</option>
                                                        <option value="129">Lebanon</option>
                                                        <option value="130">Lesotho</option>
                                                        <option value="131">Liberia</option>
                                                        <option value="132">Libya</option>
                                                        <option value="133">Liechtenstein</option>
                                                        <option value="134">Lithuania</option>
                                                        <option value="135">Luxembourg</option>
                                                        <option value="136">Macau</option>
                                                        <option value="137">Madagascar</option>
                                                        <option value="138">Malawi</option>
                                                        <option value="139">Malaysia</option>
                                                        <option value="140">Maldives</option>
                                                        <option value="141">Mali</option>
                                                        <option value="142">Malta</option>
                                                        <option value="265">Marshall islands</option>
                                                        <option value="144">Martinique</option>
                                                        <option value="145">Mauritania</option>
                                                        <option value="146">Mauritius</option>
                                                        <option value="293">Mayotte</option>
                                                        <option value="148">Mexico</option>
                                                        <option value="149">Micronesia</option>
                                                        <option value="150">Moldova</option>
                                                        <option value="151">Monaco</option>
                                                        <option value="152">Mongolia</option>
                                                        <option value="295">Montenegro</option>
                                                        <option value="267">Montserrat</option>
                                                        <option value="154">Morocco</option>
                                                        <option value="155">Mozambique</option>
                                                        <option value="156">Myanmar</option>
                                                        <option value="157">Namibia</option>
                                                        <option value="270">Nauru</option>
                                                        <option value="159">Nepal</option>
                                                        <option value="160">Netherlands</option>
                                                        <option value="161">Netherlands Antilles</option>
                                                        <option value="268">New caledonia</option>
                                                        <option value="164">New Zealand</option>
                                                        <option value="165">Nicaragua</option>
                                                        <option value="166">Niger</option>
                                                        <option value="167">Nigeria</option>
                                                        <option value="271">Niue</option>
                                                        <option value="269">Norfolk island</option>
                                                        <option value="266">Northern mariana islands</option>
                                                        <option value="170">Norway</option>
                                                        <option value="171">Oman</option>
                                                        <option value="172">Pakistan</option>
                                                        <option value="275">Palau</option>
                                                        <option value="274">Palestinian territory</option>
                                                        <option value="174">Panama</option>
                                                        <option value="175">Papua New Guinea</option>
                                                        <option value="176">Paraguay</option>
                                                        <option value="177">Peru</option>
                                                        <option value="178">Philippines</option>
                                                        <option value="273">Pitcairn</option>
                                                        <option value="179">Poland</option>
                                                        <option value="180">Portugal</option>
                                                        <option value="181">Puerto Rico</option>
                                                        <option value="182">Qatar</option>
                                                        <option value="276">R?union</option>
                                                        <option value="184">Romania</option>
                                                        <option value="186">Russia</option>
                                                        <option value="187">Rwanda</option>
                                                        <option value="278">Saint helena</option>
                                                        <option value="262">Saint kitts and nevis</option>
                                                        <option value="264">Saint lucia</option>
                                                        <option value="272">Saint pierre and miquelon</option>
                                                        <option value="289">Saint vincent &amp; the grenadines</option>
                                                        <option value="304">Saint-Barthélemy</option>
                                                        <option value="305">Saint-Martin (French part)</option>
                                                        <option value="190">San Marino</option>
                                                        <option value="280">Sao tome and principe</option>
                                                        <option value="192">Saudi Arabia</option>
                                                        <option value="193">Senegal</option>
                                                        <option value="294">Serbia</option>
                                                        <option value="249">Serbia and montenegro</option>
                                                        <option value="194">Seychelle Islands</option>
                                                        <option value="195">Sierra Leone</option>
                                                        <option value="196">Singapore</option>
                                                        <option value="197">Slovak Republic</option>
                                                        <option value="5">Slovenia</option>
                                                        <option value="277">Solomon islands</option>
                                                        <option value="3">Somalia</option>
                                                        <option value="2">South Africa</option>
                                                        <option value="299">South Georgia and the South Sandwich Islands</option>
                                                        <option value="306">South Sudan</option>
                                                        <option value="198">Spain</option>
                                                        <option value="199">SriLanka</option>
                                                        <option value="204">Sudan</option>
                                                        <option value="205">Suriname</option>
                                                        <option value="279">Svalbard and jan mayen</option>
                                                        <option value="206">Swaziland</option>
                                                        <option value="207">Sweden</option>
                                                        <option value="208">Switzerland</option>
                                                        <option value="209">Syria</option>
                                                        <option value="210">Taiwan</option>
                                                        <option value="211">Tajikistan</option>
                                                        <option value="212">Tanzania</option>
                                                        <option value="213">Thailand</option>
                                                        <option value="284">Timor-leste</option>
                                                        <option value="215">Togo</option>
                                                        <option value="283">Tokelau</option>
                                                        <option value="285">Tonga</option>
                                                        <option value="218">Trinidad and Tobago</option>
                                                        <option value="219">Tunisia</option>
                                                        <option value="220">Turkey</option>
                                                        <option value="221">Turkmenistan</option>
                                                        <option value="281">Turks and caicos islands</option>
                                                        <option value="286">Tuvalu</option>
                                                        <option value="229">U.S.Virgin Islands</option>
                                                        <option value="224">Uganda</option>
                                                        <option value="225">Ukraine</option>
                                                        <option value="226">United Arab Emirates</option>
                                                        <option value="227">United Kingdom</option>
                                                        <option selected="selected" value="228">United States</option>
                                                        <option value="230">Uruguay</option>
                                                        <option value="287">US minor outlying islands</option>
                                                        <option value="18">Uzbekistan</option>
                                                        <option value="291">Vanuatu</option>
                                                        <option value="55">Venezuela</option>
                                                        <option value="233">Vietnam</option>
                                                        <option value="290">Virgin islands, british</option>
                                                        <option value="292">Wallis and futuna</option>
                                                        <option value="253">Western sahara</option>
                                                        <option value="235">Western Samoa</option>
                                                        <option value="236">Yemen</option>
                                                        <option value="238">Zambia</option>
                                                        <option value="239">Zimbabwe</option>

                                                    </select>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                        <div class="col-lg-12 head-tab-panel">Security Settings</div>
                                        <div class="col-lg-12 tab-panel">
                                            <div>

                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            Hash Key
                                                        RZJ5VD43RT
                                                        </div>
                                                        <div class="form-group">
                                                            Security Key
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <input id="BodyContent_PageController_FormView_FormPanel_ctl04_chkShowSensativeData" name="ctl00$BodyContent$PageController$FormView$FormPanel$ctl04$chkShowSensativeData" type="checkbox"><label for="BodyContent_PageController_FormView_FormPanel_ctl04_chkShowSensativeData">Display sensitive information</label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="BodyContent_PageController_FormView_FormPanel_Footer" class="panel-footer clearfix">
            <div id="BodyContent_PageController_FormView_FormPanel_DataButtons" style="text-align: center;">
                <div id="BodyContent_PageController_FormView_FormPanel_DataButtons_dvPages" style="margin-left: 35%;">
                </div>
                <input name="ctl00$BodyContent$PageController$FormView$FormPanel$DataButtons$btnDelete" value="Delete" onclick="return confirm('');" id="BodyContent_PageController_FormView_FormPanel_DataButtons_btnDelete" class="btn btn-cons-short btn-danger margin-left-5 pull-right" type="submit"><input name="ctl00$BodyContent$PageController$FormView$FormPanel$DataButtons$btnSave" value="Save" onclick="    javascript: __doPostBack('ctl00$BodyContent$PageController$FormView$FormPanel$DataButtons$btnSave', '')" id="BodyContent_PageController_FormView_FormPanel_DataButtons_btnSave" class="btn btn-cons-short btn-info margin-left-5 pull-right" type="button">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>




    <div style="display: flex; flex-direction: row; flex-wrap: wrap; align-items: stretch;">
        <div style="background-color: lightgreen; border: 2px solid black; width: 40%;">
            AA<br />
            BB
        </div>
        <div style="background-color: green; border: 2px solid black; width: 40%;">
            AA<br />
            BBAA<br />
            BBasda asa<br />
            addasdasd
        </div>
        <div style="background-color: yellow; border: 2px solid black; width: 40%;">
            AA<br />
            BB
        </div>
        <div style="background-color: yellow; border: 2px solid black; width: 40%;">
            AA<br />
            BB
        </div>
        <div style="background-color: red; border: 2px solid black; width: 40%;">
            AA<br />
            BBAA<br />
            BB
        </div>
    </div>


    <style>
        #main
        {
            width: 100%;
            height: 300px;
            border: 1px solid black;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
        }
    </style>

    <div id="main">
        <div class="col-lg-6" style="background-color: coral;">RED</div>
        <div class="col-lg-6" style="background-color: lightblue;">BLUE</div>

        <div class="col-lg-6" style="background-color: lightgreen;">Green div with more content.</div>
    </div>


    Logged in User Info goes here
    <div class="row">


        <div class="col-lg-12">


            <div class="tabbable ">
                <ul class="nav nav-tabs">
                    <li class="dropdown pull-right tabdrop"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-align-justify"></i><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#tab9" data-toggle="tab">Section 9</a></li>
                        </ul>
                    </li>
                    <li class="active"><a href="#tab1" data-toggle="tab">Section 1</a></li>
                    <li><a href="#tab2" data-toggle="tab">Section 2</a></li>
                    <li><a href="#tab3" data-toggle="tab">Section 3</a></li>
                    <li><a href="#tab4" data-toggle="tab">Section 4</a></li>
                    <li><a href="#tab5" data-toggle="tab">Section 5</a></li>
                    <li><a href="#tab6" data-toggle="tab">Section 6</a></li>
                    <li><a href="#tab7" data-toggle="tab">Section 7</a></li>
                    <li><a href="#tab8" data-toggle="tab">Section 8</a></li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <p>I'm in Section 1.</p>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <p>Howdy, I'm in Section 2.</p>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <p>Howdy, I'm in Section 3.</p>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <p>Howdy, I'm in Section 4.</p>
                    </div>
                    <div class="tab-pane" id="tab5">
                        <p>Howdy, I'm in Section 5.</p>
                    </div>
                    <div class="tab-pane" id="tab6">
                        <p>Howdy, I'm in Section 6.</p>
                    </div>
                    <div class="tab-pane" id="tab7">
                        <p>Howdy, I'm in Section 7.</p>
                    </div>
                    <div class="tab-pane" id="tab8">
                        <p>Howdy, I'm in Section 8.</p>
                    </div>
                    <div class="tab-pane" id="tab9">
                        <p>Howdy, I'm in Section 9.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Reminders</h3>
                </div>
                <div class="panel-body">The requested page has been permanently moved to a new location.</div>
            </div>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Warning</h3>
                </div>
                <div class="panel-body">The request cannot be fulfilled due to bad syntax.</div>
            </div>
            <!-- Other Message -->
            <div class="panel panel-success" style="display: none">
                <div class="panel-heading">
                    <h3 class="panel-title">200 OK</h3>
                </div>
                <div class="panel-body">The server successfully processed the request.</div>
            </div>
            <div class="panel panel-info" style="display: none">
                <div class="panel-heading">
                    <h3 class="panel-title">100 Continue</h3>
                </div>
                <div class="panel-body">The client should continue with its request.</div>
            </div>
            <div class="panel panel-danger" style="display: none">
                <div class="panel-heading">
                    <h3 class="panel-title">503 Service Unavailable</h3>
                </div>
                <div class="panel-body">The server is temporarily unable to handle the request.</div>
            </div>
            <!-- End Other Message -->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">


            <div class="panel panel-default">

                <div class="panel-heading">Search</div>

                <div class="panel-body">


                    <section>

                        <div class="content-filter-default">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <input type="text" class="form-control" placeholder="Date from">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <input type="text" class="form-control" placeholder="Date from">
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="inputEmail">Email</label>
                                        <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="inputPassword">Password</label>
                                        <input type="text" class="form-control" id="inputName" placeholder="Payer Name">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="inputPassword">Currency</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Currency">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="#">USD</a></li>
                                                    <li><a href="#">GBP</a></li>
                                                    <li><a href="#">ILS</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="inputEmail">Method 4</label>
                                        <input type="text" class="form-control" id="inputEnvelope" placeholder="Method 4">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="inputPassword">Method 6</label>
                                        <input type="text" class="form-control" id="inputFirst" placeholder="Method 6">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="inputPayment">Payment Method</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Method">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="#">AMEX</a></li>
                                                    <li><a href="#">CAL</a></li>
                                                    <li><a href="#">ILS</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="head-filter">
                            <div class="pull-left">Terminal</div>
                            <div class="pull-right"><i class="fa fa-angle-down"></i></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="content-filter">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" class="form-control" placeholder="Username">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Amount">
                                        <span class="input-group-addon">.00</span>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" placeholder="USD">
                                        <span class="input-group-addon">.00</span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                <input type="text" class="form-control" placeholder="Username">
                            </div>
                            <br>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Amount">
                                <span class="input-group-addon">.00</span>
                            </div>
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" placeholder="US Dollar">
                                <span class="input-group-addon">.00</span>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="head-filter">
                            <div class="pull-left">Payment Method</div>
                            <div class="pull-right"><i class="fa fa-angle-right"></i></div>
                            <div class="clearfix"></div>
                        </div>
                    </section>
                    <section>
                        <div class="head-filter">
                            <div class="pull-left">Refernce</div>
                            <div class="pull-right"><i class="fa fa-angle-right"></i></div>
                            <div class="clearfix"></div>
                        </div>
                    </section>
                    <section>
                        <div class="head-filter">
                            <div class="pull-left">Debit Company</div>
                            <div class="pull-right"><i class="fa fa-angle-right"></i></div>
                            <div class="clearfix"></div>
                        </div>
                    </section>

                </div>
                <div class="panel-footer clearfix">

                    <div class="pull-right">
                        <a href="#" class="btn btn-primary">Learn More</a>
                        <a href="#" class="btn btn-default">Go Back</a>
                    </div>

                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-6">
                            Merchants List
                        </div>
                        <div class="col-lg-6 text-right">


                            <div class="btn-group">

                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Action <span class="caret"></span></button>

                                <ul class="dropdown-menu">

                                    <li><a href="#">Action</a></li>

                                    <li><a href="#">Another action</a></li>

                                    <li class="divider"></li>

                                    <li><a href="#">Separated link</a></li>

                                </ul>

                            </div>

                            <a href="#" class="btn btn-default  btn-mini"><i class="fa fa-expand"></i></a>

                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-striped" selectedindex="-1" id="BodyContent_PageController_ListView_ctl01_ListSection1_rptList" style="border-width: 0px; width: 100%; border-collapse: collapse;" cellspacing="0">
                        <thead>
                            <tr order="-1">
                                <th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$ID')">ID</a></th>
                                <th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$Name')">Name</a></th>
                                <th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$Number')">Number</a></th>
                                <th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$ContactFirstName')">ContactFirstName</a></th>
                                <th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$ContactLastName')">ContactFirstName</a></th>
                                <th scope="col"><a href="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Sort$AccountManager')">AccountManager</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="cursor: pointer; background-color: transparent;" order="0" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$0')">
                                <td>
                                    <span style="background-color: #FF6666;" class="legend-item"></span>&nbsp;
					    2
                                </td>
                                <td>או.בי.אל קומפיוטר - קומפיונט</td>
                                <td>7078777</td>
                                <td>קומפיונט</td>
                                <td>החזר הלוואה</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="1" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$1')">
                                <td>
                                    <span style="background-color: #FF6666;" class="legend-item"></span>&nbsp;
					    3
                                </td>
                                <td>אביב שלו מולטימדיה-xline</td>
                                <td>7078704</td>
                                <td>אביב</td>
                                <td>שלו</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="2" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$2')">
                                <td>
                                    <span style="background-color: #FF6666;" class="legend-item"></span>&nbsp;
					    9
                                </td>
                                <td>בננה אחזקות בע``מ</td>
                                <td>6002924</td>
                                <td>בננה אחזקות בע``מ</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="3" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$3')">
                                <td>
                                    <span style="background-color: #9E6CFF;" class="legend-item"></span>&nbsp;
					    10
                                </td>
                                <td>tymond</td>
                                <td>5691087</td>
                                <td>דורון</td>
                                <td>הופמן</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="4" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$4')">
                                <td>
                                    <span style="background-color: #9E6CFF;" class="legend-item"></span>&nbsp;
					    11
                                </td>
                                <td>BooksRus</td>
                                <td>7303978</td>
                                <td>אלי</td>
                                <td>עוזרי</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="5" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$5')">
                                <td>
                                    <span style="background-color: #66CC66;" class="legend-item"></span>&nbsp;
					    12
                                </td>
                                <td>Netpay Ltd</td>
                                <td>5638207</td>
                                <td>אלון אלבז</td>
                                <td>נט-פיי</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="6" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$6')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    21
                                </td>
                                <td>shamir</td>
                                <td>7817387</td>
                                <td>אלקס</td>
                                <td>בלוי</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="7" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$7')">
                                <td>
                                    <span style="background-color: #66CC66;" class="legend-item"></span>&nbsp;
					    22
                                </td>
                                <td>demo-001</td>
                                <td>3738338</td>
                                <td>test</td>
                                <td>test</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="8" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$8')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    24
                                </td>
                                <td>asia</td>
                                <td>4045403</td>
                                <td>test</td>
                                <td>test</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="9" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$9')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    25
                                </td>
                                <td>Jackpot Casino</td>
                                <td>8674054</td>
                                <td>Alon</td>
                                <td>Elbaz</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="10" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$10')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    27
                                </td>
                                <td>Internet bussiness</td>
                                <td>1652219</td>
                                <td>גנדור</td>
                                <td>בשארה</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="11" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$11')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    28
                                </td>
                                <td>bingo90</td>
                                <td>2645907</td>
                                <td>דובי</td>
                                <td>ברוך</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="12" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$12')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    29
                                </td>
                                <td>officer</td>
                                <td>3562595</td>
                                <td>ניר</td>
                                <td>test</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="13" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$13')">
                                <td>
                                    <span style="background-color: #FF6666;" class="legend-item"></span>&nbsp;
					    32
                                </td>
                                <td>hishhay</td>
                                <td>8124539</td>
                                <td>test</td>
                                <td>test</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="14" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$14')">
                                <td>
                                    <span style="background-color: #66CC66;" class="legend-item"></span>&nbsp;
					    35
                                </td>
                                <td>Dev Testing</td>
                                <td>5722306</td>
                                <td>Liran</td>
                                <td>Patimer</td>
                                <td>tamir</td>
                            </tr>
                            <tr style="cursor: pointer;" order="15" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$15')">
                                <td>
                                    <span style="background-color: #9E6CFF;" class="legend-item"></span>&nbsp;
					    37
                                </td>
                                <td>אגודת החרשים בישראל ע.ר.</td>
                                <td>7407491</td>
                                <td>עינה</td>
                                <td>מרחב</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="16" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$16')">
                                <td>
                                    <span style="background-color: #FF8040;" class="legend-item"></span>&nbsp;
					    38
                                </td>
                                <td>עידן 2000</td>
                                <td>8224154</td>
                                <td>רון</td>
                                <td>שמחון</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="17" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$17')">
                                <td>
                                    <span style="background-color: #FF6666;" class="legend-item"></span>&nbsp;
					    39
                                </td>
                                <td>אלון אלבז</td>
                                <td>3136652</td>
                                <td>אוסנת</td>
                                <td>אלבז</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="18" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$18')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    42
                                </td>
                                <td>AccessNet</td>
                                <td>4964779</td>
                                <td>דיאב</td>
                                <td>עווידה</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr order="19" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$19')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    44
                                </td>
                                <td>איידקס אונליין</td>
                                <td>4683586</td>
                                <td>אורי</td>
                                <td>מילשטיין</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr order="20" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$20')">
                                <td>
                                    <span style="background-color: #FF8040;" class="legend-item"></span>&nbsp;
					    48
                                </td>
                                <td>marketop</td>
                                <td>5235957</td>
                                <td>מיכאל</td>
                                <td>בן יעקב</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="21" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$21')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    49
                                </td>
                                <td>D.S. Webstors</td>
                                <td>8521482</td>
                                <td>פליקס</td>
                                <td>לשנו</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="cursor: pointer;" order="22" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$22')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    51
                                </td>
                                <td>Network Payment Soloutions</td>
                                <td>9183393</td>
                                <td>DAvid</td>
                                <td>Collins</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr order="23" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$23')">
                                <td>
                                    <span style="background-color: #7D7D7D;" class="legend-item"></span>&nbsp;
					    52
                                </td>
                                <td>Hayashir.com</td>
                                <td>6773053</td>
                                <td>גיל</td>
                                <td>עופר</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr order="24" onmouseover="this.style.cursor='pointer';" onclick="javascript:__doPostBack('ctl00$BodyContent$PageController$ListView$ctl01$ListSection1$rptList','Select$24')">
                                <td>
                                    <span style="background-color: #9E6CFF;" class="legend-item"></span>&nbsp;
					    58
                                </td>
                                <td>פוקט</td>
                                <td>1575448</td>
                                <td>שגיא</td>
                                <td>קליין</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer clearfix">

                    <div class="pull-right">
                        <a href="#" class="btn btn-primary">Learn More</a>
                        <a href="#" class="btn btn-default">Go Back</a>
                    </div>

                </div>
            </div>



            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-6">
                            Data
                        </div>
                        <div class="col-lg-6 text-right">

                            <a href="#" class="btn btn-default  btn-mini"><i class="fa fa-expand"></i></a>

                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="BodyContent_PageController_FormView_FormTabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#BodyContent_PageController_FormView_FileAndNotes">File And Notes</a></li>
                            <li><a data-toggle="tab" href="#BodyContent_PageController_FormView_StoredPaymentMethods">Stored Payment Methods</a></li>
                            <li><a data-toggle="tab" href="#BodyContent_PageController_FormView_Data">Data</a></li>
                            <li><a data-toggle="tab" href="#BodyContent_PageController_FormView_Fees">Fees</a></li>
                        </ul>

                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div id="BodyContent_PageController_FormView_FileAndNotes" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-lg-12 tab-panel">
                                            <div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            Notes
                                                            <textarea name="ctl00$BodyContent$PageController$FormView$ctl01$txtNote" rows="3" cols="20" id="BodyContent_PageController_FormView_ctl01_txtNote" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl01$Button1" value="Add New" id="BodyContent_PageController_FormView_ctl01_Button1" class="btn btn-primary" type="submit">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        Files &amp; notes
                                                    </div>

                                                    <div class="col-lg-12">
                                                        Files [<a href="FileArchiveData.aspx?MerchantID=6875">UPLOAD NEW</a>]
                                                    </div>
                                                </div>
                                                <div class="row">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="BodyContent_PageController_FormView_StoredPaymentMethods" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-lg-12 tab-panel">
                                            <div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <input name="ctl00$BodyContent$PageController$FormView$ctl02$btnAdd" value="Add Item" id="BodyContent_PageController_FormView_ctl02_btnAdd" class="btn btn-primary" type="submit">
                                                            </span>
                                                            <select name="ctl00$BodyContent$PageController$FormView$ctl02$ddlPaymentMethod" id="BodyContent_PageController_FormView_ctl02_ddlPaymentMethod" class="form-control">
                                                                <option selected="selected" value="" class="emptyOption"></option>
                                                                <option value="122">Addebito</option>
                                                                <option value="2">Admin</option>
                                                                <option value="24">Amex</option>
                                                                <option value="143">Autogiro</option>
                                                                <option value="117">Automatische Incasso</option>
                                                                <option value="5">Bank Fees</option>
                                                                <option value="120">Bankeinzug</option>
                                                                <option value="132">Beingreiðslur</option>
                                                                <option value="128">Betalingsservice</option>
                                                                <option value="118">Cargo Bancario</option>
                                                                <option value="20">CC Unknown</option>
                                                                <option value="33">China UnionPay</option>
                                                                <option value="146">China UnionPay</option>
                                                                <option value="123">Debit direct</option>
                                                                <option value="140">Débito Directo</option>
                                                                <option value="136">Debitu Dirett</option>
                                                                <option value="23">Diners</option>
                                                                <option value="26">Direct</option>
                                                                <option value="138">Direct Debets</option>
                                                                <option value="32">Discover</option>
                                                                <option value="124">Dom 80</option>
                                                                <option value="135">DOM-Electronique</option>
                                                                <option value="100">ECheck</option>
                                                                <option value="107">EPS</option>
                                                                <option value="3">Fees</option>
                                                                <option value="101">Giropay</option>
                                                                <option value="232">Google Checkout</option>
                                                                <option value="109">iDEAL</option>
                                                                <option value="127">Inkaso</option>
                                                                <option value="141">Inkaso</option>
                                                                <option value="119">Instant Debit</option>
                                                                <option value="21">Isracard</option>
                                                                <option value="204">IVR FreeTime</option>
                                                                <option value="202">IVR PerCall</option>
                                                                <option value="203">IVR PerMin</option>
                                                                <option value="31">JCB</option>
                                                                <option value="121">Lastschrift</option>
                                                                <option value="34">LiveKash</option>
                                                                <option value="28">Maestro</option>
                                                                <option value="25">Mastercard</option>
                                                                <option value="1">Miscellaneous</option>
                                                                <option value="233">MoneyBookers</option>
                                                                <option value="142">Neposredne Bremenitve</option>
                                                                <option value="149">Online Bank Transfer</option>
                                                                <option value="129">Otsekorraldus</option>
                                                                <option value="231">PayPal</option>
                                                                <option value="115">POLi</option>
                                                                <option value="113">PostFinance</option>
                                                                <option value="137">Prélèvement Bancaire</option>
                                                                <option value="29">PrepaidCards</option>
                                                                <option value="139">Przelew Bankowy</option>
                                                                <option value="106">Przelewy24</option>
                                                                <option value="205">Qiwi</option>
                                                                <option value="4">Rolling Reserve</option>
                                                                <option value="145">SEPA</option>
                                                                <option value="200">SMS IN</option>
                                                                <option value="201">SMS OUT</option>
                                                                <option value="114">Sofort</option>
                                                                <option value="130">Suoraveloitus</option>
                                                                <option value="110">TeleIngreso</option>
                                                                <option value="133">Tiešais Debets</option>
                                                                <option value="134">Tiesioginis Debetas</option>
                                                                <option value="144">TP OBT</option>
                                                                <option value="0">Unknown</option>
                                                                <option value="22">Visa</option>
                                                                <option value="151">WebMoney</option>
                                                                <option value="126">Άμεση Χρέωση</option>
                                                                <option value="131">Άμεση Χρέωση</option>
                                                                <option value="125">Директен дебит</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="BodyContent_PageController_FormView_Data" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-lg-12 tab-panel">
                                            <div>


                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            Name
               
                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl03$txtName" value="Albis" id="BodyContent_PageController_FormView_ctl03_txtName" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Description
            
               

                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl03$txtDescription" id="BodyContent_PageController_FormView_ctl03_txtDescription" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            Emergency Phone
               
                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl03$txtEmergencyPhone" id="BodyContent_PageController_FormView_ctl03_txtEmergencyPhone" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Emergency Contact
                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl03$txtEmergencyContact" id="BodyContent_PageController_FormView_ctl03_txtEmergencyContact" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 tab-panel">
                                            <div>



                                                <div class="row">
                                                    <div class="col-lg-6">

                                                        <div class="form-group">
                                                            Address Line 1 
                       
                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl03$caAddress$txtAddress1" id="BodyContent_PageController_FormView_ctl03_caAddress_txtAddress1" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            Address Line 2 
                       
                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl03$caAddress$txtAddress2" id="BodyContent_PageController_FormView_ctl03_caAddress_txtAddress2" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            City 
                       
                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl03$caAddress$txtCity" id="BodyContent_PageController_FormView_ctl03_caAddress_txtCity" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">

                                                        <div class="form-group">
                                                            Postal 
                       
                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl03$caAddress$txtPostal" id="BodyContent_PageController_FormView_ctl03_caAddress_txtPostal" class="form-control" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            State 
                   
                                                            <select name="ctl00$BodyContent$PageController$FormView$ctl03$caAddress$ddlState" id="BodyContent_PageController_FormView_ctl03_caAddress_ddlState" class="form-control">
                                                                <option selected="selected" value="" class="emptyOption"></option>
                                                                <option value="1">Alabama</option>
                                                                <option value="3">Alaska</option>
                                                                <option value="5">Arizona</option>
                                                                <option value="7">Arkansas</option>
                                                                <option value="68">Armed Forces Americas</option>
                                                                <option value="66">Armed Forces Europe, Middle East, Africa and Canada</option>
                                                                <option value="71">Armed Forces Pacific</option>
                                                                <option value="9">California</option>
                                                                <option value="11">Colorado</option>
                                                                <option value="13">Connecticut</option>
                                                                <option value="15">Delaware</option>
                                                                <option value="17">District of Columbia</option>
                                                                <option value="19">Florida</option>
                                                                <option value="21">Georgia</option>
                                                                <option value="25">Hawaii</option>
                                                                <option value="27">Idaho</option>
                                                                <option value="29">Illinois</option>
                                                                <option value="31">Indiana</option>
                                                                <option value="33">Iowa</option>
                                                                <option value="35">Kansas</option>
                                                                <option value="37">Kentucky</option>
                                                                <option value="39">Louisiana</option>
                                                                <option value="41">Maine</option>
                                                                <option value="43">Maryland</option>
                                                                <option value="45">Massachusetts</option>
                                                                <option value="47">Michigan</option>
                                                                <option value="49">Minnesota</option>
                                                                <option value="65">Mississippi</option>
                                                                <option value="2">Missouri</option>
                                                                <option value="4">Montana</option>
                                                                <option value="6">Nebraska</option>
                                                                <option value="8">Nevada</option>
                                                                <option value="10">New Hampshire</option>
                                                                <option value="12">New Jersey</option>
                                                                <option value="14">New Mexico</option>
                                                                <option value="16">New York</option>
                                                                <option value="18">North Carolina</option>
                                                                <option value="20">North Dakota</option>
                                                                <option value="22">Ohio</option>
                                                                <option value="23">Oklahoma</option>
                                                                <option value="24">Oregon</option>
                                                                <option value="26">Pennsylvania</option>
                                                                <option value="28">Rhode Island</option>
                                                                <option value="30">South Carolina</option>
                                                                <option value="32">South Dakota</option>
                                                                <option value="34">Tennessee</option>
                                                                <option value="36">Texas</option>
                                                                <option value="38">Utah</option>
                                                                <option value="40">Vermont</option>
                                                                <option value="42">Virginia</option>
                                                                <option value="44">Washington</option>
                                                                <option value="46">West Virginia</option>
                                                                <option value="48">Wisconsin</option>
                                                                <option value="50">Wyoming</option>

                                                            </select>

                                                        </div>
                                                        <div class="form-group">
                                                            Country
                   
                                                            <select name="ctl00$BodyContent$PageController$FormView$ctl03$caAddress$ddlCountry" id="BodyContent_PageController_FormView_ctl03_caAddress_ddlCountry" class="form-control">
                                                                <option selected="selected" value="" class="emptyOption"></option>
                                                                <option value="1">Afghanistan</option>
                                                                <option value="303">Aland Islands</option>
                                                                <option value="6">Albania</option>
                                                                <option value="7">Algeria</option>
                                                                <option value="8">American Samoa</option>
                                                                <option value="9">Andorra</option>
                                                                <option value="10">Angola</option>
                                                                <option value="240">Anguilla</option>
                                                                <option value="241">Antarctica</option>
                                                                <option value="12">Antigua</option>
                                                                <option value="13">Argentina</option>
                                                                <option value="14">Armenia</option>
                                                                <option value="242">Aruba</option>
                                                                <option value="17">Australia</option>
                                                                <option value="19">Austria</option>
                                                                <option value="20">Azerbaijan</option>
                                                                <option value="21">Bahamas</option>
                                                                <option value="22">Bahrain</option>
                                                                <option value="23">Bangladesh</option>
                                                                <option value="24">Barbados</option>
                                                                <option value="26">Belarus</option>
                                                                <option value="27">Belgium</option>
                                                                <option value="28">Belize</option>
                                                                <option value="29">Benin</option>
                                                                <option value="30">Bermuda</option>
                                                                <option value="243">Bhutan</option>
                                                                <option value="32">Bolivia</option>
                                                                <option value="33">Bosniaand Herzegovina</option>
                                                                <option value="34">Botswana</option>
                                                                <option value="244">Bouvet island</option>
                                                                <option value="35">Brazil</option>
                                                                <option value="301">British Indian Ocean Territory</option>
                                                                <option value="37">Brunei</option>
                                                                <option value="38">Bulgaria</option>
                                                                <option value="39">Burkina Faso</option>
                                                                <option value="40">Burundi</option>
                                                                <option value="41">Cambodia</option>
                                                                <option value="42">Cameroon</option>
                                                                <option value="43">Canada</option>
                                                                <option value="250">Cape verde</option>
                                                                <option value="45">Cayman Islands</option>
                                                                <option value="247">Central african republic</option>
                                                                <option value="47">Chad</option>
                                                                <option value="48">Chile</option>
                                                                <option value="49">China</option>
                                                                <option value="251">Christmas island</option>
                                                                <option value="245">Cocos (keeling) islands</option>
                                                                <option value="52">Colombia</option>
                                                                <option value="261">Comoros</option>
                                                                <option value="246">Congo, DRC</option>
                                                                <option value="54">Congo, ROC</option>
                                                                <option value="248">Cook islands</option>
                                                                <option value="57">Costa Rica</option>
                                                                <option value="58">Croatia</option>
                                                                <option value="59">Cuba</option>
                                                                <option value="60">Cyprus</option>
                                                                <option value="61">Czech Republic</option>
                                                                <option value="62">Denmark</option>
                                                                <option value="64">Djibouti</option>
                                                                <option value="252">Dominica</option>
                                                                <option value="66">Dominican Republic</option>
                                                                <option value="67">Ecuador</option>
                                                                <option value="68">Egypt</option>
                                                                <option value="69">El Salvador </option>
                                                                <option value="70">Equatorial Guinea</option>
                                                                <option value="71">Eritrea</option>
                                                                <option value="72">Estonia</option>
                                                                <option value="73">Ethiopia</option>
                                                                <option value="74">F.Y.R. of Macedonia</option>
                                                                <option value="254">Falkland islands (malvinas)</option>
                                                                <option value="255">Faroe islands</option>
                                                                <option value="77">Fiji Islands</option>
                                                                <option value="78">Finland</option>
                                                                <option value="79">France</option>
                                                                <option value="81">French Guiana</option>
                                                                <option value="82">French Polynesia</option>
                                                                <option value="302">French Southern and Antarctic Lands</option>
                                                                <option value="83">Gabon</option>
                                                                <option value="84">Gambia</option>
                                                                <option value="85">Georgia</option>
                                                                <option value="86">Germany</option>
                                                                <option value="87">Ghana</option>
                                                                <option value="88">Gibraltar</option>
                                                                <option value="89">Greece</option>
                                                                <option value="90">Greenland</option>
                                                                <option value="256">Grenada</option>
                                                                <option value="92">Guadeloupe</option>
                                                                <option value="93">Guam</option>
                                                                <option value="95">Guatemala</option>
                                                                <option value="298">Guernsey</option>
                                                                <option value="97">Guinea</option>
                                                                <option value="96">Guinea-Bissau</option>
                                                                <option value="98">Guyana</option>
                                                                <option value="99">Haiti</option>
                                                                <option value="300">Heard Island and McDonald Islands</option>
                                                                <option value="288">Holy see (vatican city state)</option>
                                                                <option value="100">Honduras</option>
                                                                <option value="101">Hong Kong SAR</option>
                                                                <option value="102">Hungary</option>
                                                                <option value="103">Iceland</option>
                                                                <option value="104">India</option>
                                                                <option value="105">Indonesia</option>
                                                                <option value="112">Iran</option>
                                                                <option value="113">Iraq</option>
                                                                <option value="114">Ireland</option>
                                                                <option value="296">Isle of Man</option>
                                                                <option value="115">Israel</option>
                                                                <option value="116">Italy</option>
                                                                <option value="117">Ivory Coast</option>
                                                                <option value="118">Jamaica</option>
                                                                <option value="119">Japan</option>
                                                                <option value="297">Jersey</option>
                                                                <option value="120">Jordan</option>
                                                                <option value="121">Kazakhstan</option>
                                                                <option value="122">Kenya</option>
                                                                <option value="260">Kiribati</option>
                                                                <option value="125">Korea</option>
                                                                <option value="124">Korea (North)</option>
                                                                <option value="234">Kuwait</option>
                                                                <option value="126">Kyrgyz Republic</option>
                                                                <option value="263">Lao, PDR</option>
                                                                <option value="128">Latvia</option>
                                                                <option value="129">Lebanon</option>
                                                                <option value="130">Lesotho</option>
                                                                <option value="131">Liberia</option>
                                                                <option value="132">Libya</option>
                                                                <option value="133">Liechtenstein</option>
                                                                <option value="134">Lithuania</option>
                                                                <option value="135">Luxembourg</option>
                                                                <option value="136">Macau</option>
                                                                <option value="137">Madagascar</option>
                                                                <option value="138">Malawi</option>
                                                                <option value="139">Malaysia</option>
                                                                <option value="140">Maldives</option>
                                                                <option value="141">Mali</option>
                                                                <option value="142">Malta</option>
                                                                <option value="265">Marshall islands</option>
                                                                <option value="144">Martinique</option>
                                                                <option value="145">Mauritania</option>
                                                                <option value="146">Mauritius</option>
                                                                <option value="293">Mayotte</option>
                                                                <option value="148">Mexico</option>
                                                                <option value="149">Micronesia</option>
                                                                <option value="150">Moldova</option>
                                                                <option value="151">Monaco</option>
                                                                <option value="152">Mongolia</option>
                                                                <option value="295">Montenegro</option>
                                                                <option value="267">Montserrat</option>
                                                                <option value="154">Morocco</option>
                                                                <option value="155">Mozambique</option>
                                                                <option value="156">Myanmar</option>
                                                                <option value="157">Namibia</option>
                                                                <option value="270">Nauru</option>
                                                                <option value="159">Nepal</option>
                                                                <option value="160">Netherlands</option>
                                                                <option value="161">Netherlands Antilles</option>
                                                                <option value="268">New caledonia</option>
                                                                <option value="164">New Zealand</option>
                                                                <option value="165">Nicaragua</option>
                                                                <option value="166">Niger</option>
                                                                <option value="167">Nigeria</option>
                                                                <option value="271">Niue</option>
                                                                <option value="269">Norfolk island</option>
                                                                <option value="266">Northern mariana islands</option>
                                                                <option value="170">Norway</option>
                                                                <option value="171">Oman</option>
                                                                <option value="172">Pakistan</option>
                                                                <option value="275">Palau</option>
                                                                <option value="274">Palestinian territory</option>
                                                                <option value="174">Panama</option>
                                                                <option value="175">Papua New Guinea</option>
                                                                <option value="176">Paraguay</option>
                                                                <option value="177">Peru</option>
                                                                <option value="178">Philippines</option>
                                                                <option value="273">Pitcairn</option>
                                                                <option value="179">Poland</option>
                                                                <option value="180">Portugal</option>
                                                                <option value="181">Puerto Rico</option>
                                                                <option value="182">Qatar</option>
                                                                <option value="276">R?union</option>
                                                                <option value="184">Romania</option>
                                                                <option value="186">Russia</option>
                                                                <option value="187">Rwanda</option>
                                                                <option value="278">Saint helena</option>
                                                                <option value="262">Saint kitts and nevis</option>
                                                                <option value="264">Saint lucia</option>
                                                                <option value="272">Saint pierre and miquelon</option>
                                                                <option value="289">Saint vincent &amp; the grenadines</option>
                                                                <option value="304">Saint-Barthélemy</option>
                                                                <option value="305">Saint-Martin (French part)</option>
                                                                <option value="190">San Marino</option>
                                                                <option value="280">Sao tome and principe</option>
                                                                <option value="192">Saudi Arabia</option>
                                                                <option value="193">Senegal</option>
                                                                <option value="294">Serbia</option>
                                                                <option value="249">Serbia and montenegro</option>
                                                                <option value="194">Seychelle Islands</option>
                                                                <option value="195">Sierra Leone</option>
                                                                <option value="196">Singapore</option>
                                                                <option value="197">Slovak Republic</option>
                                                                <option value="5">Slovenia</option>
                                                                <option value="277">Solomon islands</option>
                                                                <option value="3">Somalia</option>
                                                                <option value="2">South Africa</option>
                                                                <option value="299">South Georgia and the South Sandwich Islands</option>
                                                                <option value="306">South Sudan</option>
                                                                <option value="198">Spain</option>
                                                                <option value="199">SriLanka</option>
                                                                <option value="204">Sudan</option>
                                                                <option value="205">Suriname</option>
                                                                <option value="279">Svalbard and jan mayen</option>
                                                                <option value="206">Swaziland</option>
                                                                <option value="207">Sweden</option>
                                                                <option value="208">Switzerland</option>
                                                                <option value="209">Syria</option>
                                                                <option value="210">Taiwan</option>
                                                                <option value="211">Tajikistan</option>
                                                                <option value="212">Tanzania</option>
                                                                <option value="213">Thailand</option>
                                                                <option value="284">Timor-leste</option>
                                                                <option value="215">Togo</option>
                                                                <option value="283">Tokelau</option>
                                                                <option value="285">Tonga</option>
                                                                <option value="218">Trinidad and Tobago</option>
                                                                <option value="219">Tunisia</option>
                                                                <option value="220">Turkey</option>
                                                                <option value="221">Turkmenistan</option>
                                                                <option value="281">Turks and caicos islands</option>
                                                                <option value="286">Tuvalu</option>
                                                                <option value="229">U.S.Virgin Islands</option>
                                                                <option value="224">Uganda</option>
                                                                <option value="225">Ukraine</option>
                                                                <option value="226">United Arab Emirates</option>
                                                                <option value="227">United Kingdom</option>
                                                                <option value="228">United States</option>
                                                                <option value="230">Uruguay</option>
                                                                <option value="287">US minor outlying islands</option>
                                                                <option value="18">Uzbekistan</option>
                                                                <option value="291">Vanuatu</option>
                                                                <option value="55">Venezuela</option>
                                                                <option value="233">Vietnam</option>
                                                                <option value="290">Virgin islands, british</option>
                                                                <option value="292">Wallis and futuna</option>
                                                                <option value="253">Western sahara</option>
                                                                <option value="235">Western Samoa</option>
                                                                <option value="236">Yemen</option>
                                                                <option value="238">Zambia</option>
                                                                <option value="239">Zimbabwe</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 tab-panel">
                                            <div>

                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <ul>
                                                            <li>
                                                                <input id="BodyContent_PageController_FormView_ctl03_chkIsActive" name="ctl00$BodyContent$PageController$FormView$ctl03$chkIsActive" type="checkbox"><label for="BodyContent_PageController_FormView_ctl03_chkIsActive">Is Active</label></li>
                                                            <li>
                                                                <input id="BodyContent_PageController_FormView_ctl03_chkIsBlocked" name="ctl00$BodyContent$PageController$FormView$ctl03$chkIsBlocked" type="checkbox"><label for="BodyContent_PageController_FormView_ctl03_chkIsBlocked">Is Blocked</label></li>
                                                            <li>
                                                                <input id="BodyContent_PageController_FormView_ctl03_chkIsDynamicDescriptor" name="ctl00$BodyContent$PageController$FormView$ctl03$chkIsDynamicDescriptor" type="checkbox"><label for="BodyContent_PageController_FormView_ctl03_chkIsDynamicDescriptor">Is Dynamic Descriptor</label></li>
                                                            <li>
                                                                <input id="BodyContent_PageController_FormView_ctl03_chkIsReturnCode" name="ctl00$BodyContent$PageController$FormView$ctl03$chkIsReturnCode" checked="checked" type="checkbox"><label for="BodyContent_PageController_FormView_ctl03_chkIsReturnCode">Is ReturnCode</label></li>
                                                            <li>
                                                                <input id="BodyContent_PageController_FormView_ctl03_chkIsAllowPayoutWithoutTransfer" name="ctl00$BodyContent$PageController$FormView$ctl03$chkIsAllowPayoutWithoutTransfer" type="checkbox"><label for="BodyContent_PageController_FormView_ctl03_chkIsAllowPayoutWithoutTransfer">Is Allow Payout Without Transfer</label></li>

                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <ul>
                                                            <li>
                                                                <input id="BodyContent_PageController_FormView_ctl03_chkIsAllowRefund" name="ctl00$BodyContent$PageController$FormView$ctl03$chkIsAllowRefund" checked="checked" type="checkbox"><label for="BodyContent_PageController_FormView_ctl03_chkIsAllowRefund">Is Allow Refund</label></li>
                                                            <li>
                                                                <input id="BodyContent_PageController_FormView_ctl03_chkIsAllowPartialRefund" name="ctl00$BodyContent$PageController$FormView$ctl03$chkIsAllowPartialRefund" type="checkbox"><label for="BodyContent_PageController_FormView_ctl03_chkIsAllowPartialRefund">Is Allow Partial Refund</label></li>
                                                            <li>
                                                                <input id="BodyContent_PageController_FormView_ctl03_chkIsAutoRefund" name="ctl00$BodyContent$PageController$FormView$ctl03$chkIsAutoRefund" type="checkbox"><label for="BodyContent_PageController_FormView_ctl03_chkIsAutoRefund">Is Auto Refund</label></li>
                                                            <li>
                                                                <input id="BodyContent_PageController_FormView_ctl03_chkIsNotifyRetReqAfterRefund" name="ctl00$BodyContent$PageController$FormView$ctl03$chkIsNotifyRetReqAfterRefund" type="checkbox"><label for="BodyContent_PageController_FormView_ctl03_chkIsNotifyRetReqAfterRefund">Is Notify RetReq After Refund</label></li>
                                                        </ul>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="BodyContent_PageController_FormView_Fees" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-lg-12 tab-panel">
                                            <div>


                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            Name : 
               
                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl04$txtTitle" id="BodyContent_PageController_FormView_ctl04_txtTitle" class="form-control" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            Currency:
               
                                                            <select name="ctl00$BodyContent$PageController$FormView$ctl04$ddlCurrency" id="BodyContent_PageController_FormView_ctl04_ddlCurrency" class="form-control">
                                                                <option selected="selected" value="" class="emptyOption"></option>
                                                                <option value="0">ILS</option>
                                                                <option value="1">USD</option>
                                                                <option value="2">EUR</option>
                                                                <option value="3">GBP</option>
                                                                <option value="4">AUD</option>
                                                                <option value="5">CAD</option>
                                                                <option value="6">JPY</option>
                                                                <option value="7">NOK</option>
                                                                <option value="8">PLN</option>
                                                                <option value="9">MXN</option>
                                                                <option value="10">ZAR</option>
                                                                <option value="11">RUB</option>
                                                                <option value="12">TRY</option>
                                                                <option value="13">CHF</option>
                                                                <option value="14">INR</option>
                                                                <option value="15">DKK</option>
                                                                <option value="16">SEK</option>
                                                                <option value="17">CNY</option>
                                                                <option value="18">HUF</option>
                                                                <option value="19">NZD</option>
                                                                <option value="20">HKD</option>
                                                                <option value="21">KRW</option>
                                                                <option value="22">SGD</option>
                                                                <option value="23">THB</option>
                                                                <option value="24">BSD</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <input name="ctl00$BodyContent$PageController$FormView$ctl04$Button1" value="Add" id="BodyContent_PageController_FormView_ctl04_Button1" class="btn btn-primary" type="submit">
                                                        </div>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 head-tab-panel">Settlement Templates</div>
                                        <div class="col-lg-12 tab-panel">
                                            <div>


                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <input name="ctl00$BodyContent$PageController$FormView$ctl05$btnAddSettings" value="Add Settlement Template" id="BodyContent_PageController_FormView_ctl05_btnAddSettings" class="btn btn-primary" type="submit">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel-footer clearfix">

                    <div class="pull-right">
                        <a href="#" class="btn btn-primary">Learn More</a>
                        <a href="#" class="btn btn-default">Go Back</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
        </div>
    </div>



    <script src="http://www.eyecon.ro/bootstrap-tabdrop/js/google-code-prettify/prettify.js"></script>
    <script src="http://www.eyecon.ro/bootstrap-tabdrop/js/jquery.js"></script>
    <script src="http://www.eyecon.ro/bootstrap-tabdrop/js/bootstrap-dropdown.js"></script>
    <script src="http://www.eyecon.ro/bootstrap-tabdrop/js/bootstrap-tab.js"></script>
    <script src="http://www.eyecon.ro/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>
    <script>
        if (top.location != location) {
            top.location.href = document.location.href;
        }
        $(function () {
            window.prettyPrint && prettyPrint();
            $('.nav-tabs:first').tabdrop();
            $('.nav-tabs:last').tabdrop({ text: 'More options' });

        });
    </script>



</asp:Content>
