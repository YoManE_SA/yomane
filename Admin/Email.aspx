﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Email.aspx.cs" Inherits="Netpay.Admin.Email" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <style>
        .email-list {
            width: 100%;
        }

            .email-list th {
                background: #808080;
            }

            .email-list td {
                padding: 10px;
                font-size: 14px;
                color: #576475;
            }

        .muted {
            color: #95989A !important;
        }

        .email-list tr:nth-child(even) {
            background: #F5F5F5;
        }

        .email-list tr:nth-child(odd) {
            background: #fff;
        }
    </style>
    <admin:PanelSection runat="server" Title="Action" Icon="fa-bell">
        <Body>
            <div class="col-md-4">
                10 Items to
               <div class="btn-group">
                   <button class="btn btn-default">Action</button>
                   <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="caret"></span></button>
                   <ul class="dropdown-menu">
                       <li><a href="#">Inbox</a></li>
                       <li><a href="#">Sent Items</a></li>
                       <li><a href="#">Archive</a></li>
                   </ul>
               </div>
            </div>
            <div class="col-md-4">
            </div>

            <div class="col-md-4">
            </div>


        </Body>
        <Footer>
            <asp:Button runat="server" CssClass="btn btn-primary btn-cons-short" Text="Save" Style="float: right;" />
        </Footer>
    </admin:PanelSection>

    <admin:PanelSection runat="server" Title="Advanced Search" Icon="fa-search">
        <Body>
            <div class="col-md-12">
                <div class="form-group">
                    Search Text
                 <div class="input-group">
                     <div class="input-group-btn">
                         <button tabindex="-1" class="btn btn-primary" type="button">Search</button>
                         <button tabindex="-1" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">
                             <span class="caret"></span>
                             <span class="sr-only">Toggle Dropdown</span>
                         </button>
                         <ul class="dropdown-menu">
                             <li><a href="#">All</a></li>
                             <li><a href="#">Inbox</a></li>
                             <li><a href="#">Sent Items</a></li>
                             <li><a href="#">Archive</a></li>
                         </ul>
                     </div>
                     <input type="text" class="form-control" placeholder="Text for search...">
                 </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    Status
               <asp:DropDownList runat="server" CssClass="form-control">
                   <asp:ListItem>Test</asp:ListItem>
               </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    Date Range
                     <div class="row">

                         <JQ:DateRange runat="server" />

                     </div>
                </div>

            </div>

            <div class="col-md-4">
                <div class="form-group">
                    By User
                    <asp:DropDownList CssClass="form-control" runat="server">
                        <asp:ListItem>Liran</asp:ListItem>
                        <asp:ListItem>Udi</asp:ListItem>
                        <asp:ListItem>Liran</asp:ListItem>
                        <asp:ListItem>Udi</asp:ListItem>
                    </asp:DropDownList>
                </div>

            </div>


        </Body>
        <Footer>
            <asp:Button runat="server" CssClass="btn btn-primary btn-cons-short" Text="Search" Style="float: right;" />
        </Footer>
    </admin:PanelSection>

    <admin:ListSection ID="ListSection1" runat="server" Icon="fa fa-envelope-o" Title="Email">
        <Header></Header>
        <Body>
            <table class="email-list">


                <tbody>
                    <tr>
                        <td style="width: 30px;">
                            <asp:CheckBox runat="server" /></td>
                        <td style="width: 30px;"><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-warning">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-success">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-success">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td style="width: 30px;">
                            <asp:CheckBox runat="server" /></td>
                        <td style="width: 30px;"><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-warning">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-success">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-success">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td style="width: 30px;">
                            <asp:CheckBox runat="server" /></td>
                        <td style="width: 30px;"><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-warning">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-success">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-success">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td style="width: 30px;">
                            <asp:CheckBox runat="server" /></td>
                        <td style="width: 30px;"><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-warning">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted">Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-success">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-success">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" /></td>
                        <td><i class="fa fa-paperclip"></i></td>
                        <td>David Nester</td>
                        <td class="muted"><span class="label label-success">HOME</span> Less spam, and mobile access. Gmail is email that's intuitive, ...efficient, and useful. And maybe even fun.</td>
                        <td>Yesterday</td>
                        <td><i class="fa fa-reply"></i></td>
                    </tr>
                </tbody>
            </table>
        </Body>
        <Footer>


            <asp:Button runat="server" CssClass="btn btn-success btn-cons-short" Text="Export to Excel" />
            <asp:Button runat="server" CssClass="btn btn-danger btn-cons-short" Text="Action" />
            <asp:Button runat="server" CssClass="btn btn-primary btn-cons-short" Text="New Message" />
            <asp:Button runat="server" CssClass="btn btn-info btn-cons-short" Text="Templates" />


        </Footer>
    </admin:ListSection>

    <admin:PanelSection runat="server" Title="New Message" Icon="fa-search">
        <Body>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">




                        <div class="btn-group">

                            <button class="btn">From</button>

                            <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>

                            <ul class="dropdown-menu">

                                <li><a href="#">Sales@netpay-intl.com</a></li>
                                <li><a href="#">liran@netpay-intl.com</a></li>
                                <li><a href="#">udi@netpay-intl.com</a></li>
                                <li><a href="#">zeev@netpay-intl.com</a></li>
                            </ul>

                            <span style="padding: 0 10px; line-height: 25px;"><u><i>Sales@netpay-intl.com</i></u></span>
                        </div>




                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        To
               <div class="input-group">
                   <div class="input-group-btn">
                       <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                       <ul class="dropdown-menu">
                           <li><a href="#">Other</a></li>
                           <li><a href="#">udi@netpay-intl.com</a></li>
                           <li><a href="#">udi@netpay-intl.com</a></li>
                           <li><a href="#">udi@netpay-intl.com</a></li>
                       </ul>
                   </div>
                   <input type="text" class="form-control">
               </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        CC
               <div class="input-group">
                   <div class="input-group-btn">
                       <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                       <ul class="dropdown-menu">
                           <li><a href="#">Other</a></li>
                           <li><a href="#">udi@netpay-intl.com</a></li>
                           <li><a href="#">udi@netpay-intl.com</a></li>
                           <li><a href="#">udi@netpay-intl.com</a></li>
                       </ul>
                   </div>
                   <input type="text" class="form-control">
               </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        Subject 
                    <asp:TextBox runat="server" CssClass="form-control" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        Message:
                        <JQ:HtmlEdit ID="RadEditor1" runat="server" Width="100%" Height="200" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-primary btn-file">Browse…
                                                   <asp:FileUpload runat="server" ID="FileUpload1" AllowMultiple="true" />
                                </span>
                            </span>
                            <input class="form-control" readonly="" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        Answer from template:
                        <asp:DropDownList runat="server" CssClass="form-control">
                            <asp:ListItem>Select a template...</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </Body>
        <Footer>
            <admin:DataButtons runat="server" EnableAdd="true" />
        </Footer>
    </admin:PanelSection>


</asp:Content>
