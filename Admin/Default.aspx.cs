﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin
{
    public partial class Default : Controls.Page
    {
        public bool IsErrorMode { get; set; }
        public bool IsNotAuthorizedMode { get; set; }
        public string NotAuthorizedModuleName { get; set; }
        public string NotAuthorizedSecuredObjectName { get; set; }
        public string RequiredPermission { get; set; }

        public Default() : base("Home") { }
        public System.Web.UI.HtmlControls.HtmlControl QuickLinks { get { return phQuickLinks; } }
        public System.Web.UI.WebControls.PlaceHolder Status { get { return phStatus; } }
        public System.Web.UI.WebControls.PlaceHolder Notifications { get { return phNotifications; } }

        public Control ModuleNotActivated
        {
            get
            {
                return LoadControl("~/Modules/ModuleNotActivated/ModuleNotActivated.ascx");
            }
        }

        public Control ModuleNotAuthorized
        {
            get
            {
                return LoadControl("~/Modules/ModuleNotAuthorized/ModuleNotAuthorized.ascx");
            }
        }


        protected override void OnPreLoad(EventArgs e)
        {
            var querystring = Page.Request.QueryString.ToString();
            if (querystring.Contains("ErrorMode"))
            {
                IsErrorMode = true;
            }
            else if (querystring.Contains("NotAuthorized"))
            {
                IsNotAuthorizedMode = true;

                if (!string.IsNullOrEmpty(Page.Request.QueryString["ModuleID"]))
                {
                    var module = Admin.Application.Current.Modules.Where(y => (y is CoreBasedModule) && (y as CoreBasedModule).CoreModule.ID == int.Parse(Page.Request.QueryString["ModuleID"])).SingleOrDefault();
                    if (module != null)
                        NotAuthorizedModuleName = module.Name;
                }

                else
                    NotAuthorizedModuleName = "";

                if (!string.IsNullOrEmpty(Page.Request.QueryString["Permission"]))
                {
                    RequiredPermission = Page.Request.QueryString["Permission"];
                }
                else
                {
                    RequiredPermission = "";
                }

                if (!string.IsNullOrEmpty(Page.Request.QueryString["SecuredObject"]))
                {
                    NotAuthorizedSecuredObjectName = Page.Request.QueryString["SecuredObject"];
                }
                else
                {
                    NotAuthorizedSecuredObjectName = "";
                }
            }

            msQuickSearch.OnClientSearch = "if (document.getElementById('" + msQuickSearch.ValueClientID + "').value == '') return false; document.location = $('#" + msQuickSearch.TypeClientID + "').val().replace(/~/, '" + Web.WebUtils.RootUrl + "') + $('#" + msQuickSearch.ValueClientID + "').val();";
            msQuickSearch.DataBind();

            base.OnPreLoad(e);
        }

    }
}