﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleNotAuthorized.ascx.cs" Inherits="Netpay.Admin.Modules.ModuleNotAuthorized.ModuleNotAuthorized" %>
<admin:FormSections runat="server" IsForSecurityMessage="true" ID="ModuleNotAuthorize">
    <div class="alert alert-danger alert-security">
        <asp:Label runat="server" Font-Bold="true" Text='<%# ModuleName + " Module not authorized" %>'>
        </asp:Label><br />
        <asp:Label runat="server" Font-Bold="true" Text='<%# "Permission needed:" + Permission %>'>
        </asp:Label>
    </div>
</admin:FormSections>