﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.ModuleNotAuthorized
{
    public partial class ModuleNotAuthorized : System.Web.UI.UserControl
    {
        public string ModuleName { get; set; }
        public string Permission { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}