﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Risk.MaxMind
{
	public partial class List : Controls.AccountControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
			rptList.DataSource = Bll.MaxMind.Log.Search(TemplatePage.GetFilter<Bll.MaxMind.Log.SearchFilters>(), rptList);
		}
	}
}