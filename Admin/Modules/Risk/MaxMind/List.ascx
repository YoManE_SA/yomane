﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Risk.MaxMind.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Risk Items">
    <Header>
        <admin:LegendColors ID="LegendColors1" runat="server" Items='<%# Netpay.Bll.MaxMind.Log.StatusColor %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" PageSize="25">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Bll.MaxMind.Log.StatusColor.MapHtmlColor((Netpay.Bll.MaxMind.Log.LogStatus)Eval("Status")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="InsertDate" SortExpression="InsertDate">
                        <ItemTemplate>
                            <%# Eval("InsertDate") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Merchant Id" DataField="MerchantId" SortExpression="MerchantId" />
                    <asp:BoundField HeaderText="Payment Display" DataField="PaymentMethodDisplay" SortExpression="PaymentMethodDisplay" />
                    <asp:TemplateField HeaderText="Amount" SortExpression="Amount">
                        <ItemTemplate><%# ((decimal)Eval("Amount")).ToAmountFormat((int)(Netpay.CommonTypes.Currency)Eval("Currency")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Allowed Score / New" SortExpression="AllowedRiskScore">
                        <ItemTemplate><%# Eval("AllowedScore") %> / <%# Eval("AllowedRiskScore") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Return Score / New" SortExpression="ReturnScore">
                        <ItemTemplate><%# Eval("ReturnScore") %> / <%# Eval("ReturnRiskScore") %></ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="Return Bin" DataField="ReturnBinCountry" SortExpression="ReturnBinCountry" />
                    <asp:BoundField HeaderText="Reference" DataField="ReferenceCode" SortExpression="ReferenceCode" />
                    <asp:BoundField HeaderText="Status" DataField="Status" SortExpression="Status" />

                    <admin:AdminListExtender OpenOnRowClick="true">
                        <ItemTemplate>
                            <div style="padding: 15px;">
                                <div>
                                    <b>SENT:</b><br />
                                    <%# Eval("SendingString") %>
                                </div>
                                <div>
                                    <b>ANSWER:</b><br />
                                    <%# Eval("ReturnAnswer") %><br />
                                    <div>
                                        <b>Explanation:</b><br />
                                        <%# Eval("ReturnExplanation") %><br />
                                    </div>
                        </ItemTemplate>
                    </admin:AdminListExtender>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>
</admin:ListSection>
