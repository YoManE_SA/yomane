﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Risk.MaxMind
{
	public partial class MerchantSettings : Controls.AccountControlBase
	{
		protected Netpay.Bll.MaxMind.MerchantSettings ItemData { get; private set; }
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.SaveItem += PageController_SaveItem;
			base.OnLoad(e);
		}
		
		protected override void DataBindChildren()
		{
			if (Account != null) ItemData = Netpay.Bll.MaxMind.MerchantSettings.Load(Account.MerchantID.GetValueOrDefault());
			if (ItemData == null) ItemData = new Netpay.Bll.MaxMind.MerchantSettings(0);
			base.DataBindChildren();
		}
		
		public void PageController_SaveItem(object sender, EventArgs e)
		{
			ItemData = Netpay.Bll.MaxMind.MerchantSettings.Load(Account.MerchantID.GetValueOrDefault());
			if (ItemData == null) ItemData = new Netpay.Bll.MaxMind.MerchantSettings(Account.MerchantID.GetValueOrDefault());

			ItemData.IsEnabled = chk_mxm_ProcessSettingsMaxMind.Checked;
			ItemData.RiskScoreAllowed = txt_mxm_RiskScoreAllowed.Text.ToDecimal(0);
			ItemData.ScoreAllowed = txt_mxm_ScoreAllowed.Text.ToDecimal(0);
			ItemData.IsSkipVirtualTerminal = chk_mxm_IsSkipVirtualTerminal.Checked;
			ItemData.IsAllowPublicEmail = chk_mxm_IsAllowPublicEmail.Checked;
			ItemData.SkipIPList = txt_mxm_SkipIPList.Text;

            //There is exception when saving the Risk tab that comes from here
            //so for now i comment this function because "Max mind" issue is
            //not for now.
			//ItemData.Save();
		}

	}
}