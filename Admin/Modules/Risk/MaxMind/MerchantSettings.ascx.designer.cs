﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Netpay.Admin.Risk.MaxMind {
    
    
    public partial class MerchantSettings {
        
        /// <summary>
        /// FormSections1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Admin.Controls.FormSections FormSections1;
        
        /// <summary>
        /// txt_mxm_RiskScoreAllowed control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txt_mxm_RiskScoreAllowed;
        
        /// <summary>
        /// txt_mxm_ScoreAllowed control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txt_mxm_ScoreAllowed;
        
        /// <summary>
        /// txt_mxm_SkipIPList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txt_mxm_SkipIPList;
        
        /// <summary>
        /// chk_mxm_ProcessSettingsMaxMind control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox chk_mxm_ProcessSettingsMaxMind;
        
        /// <summary>
        /// chk_mxm_IsSkipVirtualTerminal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox chk_mxm_IsSkipVirtualTerminal;
        
        /// <summary>
        /// chk_mxm_IsAllowPublicEmail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox chk_mxm_IsAllowPublicEmail;
    }
}
