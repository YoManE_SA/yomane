﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MerchantSettings.ascx.cs" Inherits="Netpay.Admin.Risk.MaxMind.MerchantSettings" %>
<admin:FormSections ID="FormSections1" runat="server" Title="MaxMind Settings">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                Max Risk to Process (OLD)
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                    <asp:TextBox ID="txt_mxm_RiskScoreAllowed" class="form-control" runat="server" Text="<%# ItemData.RiskScoreAllowed %>" />
                </div>
            </div>
            <div class="form-group">
                Max Risk to Process (NEW)
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                    <asp:TextBox ID="txt_mxm_ScoreAllowed" runat="server" CssClass="form-control" Text="<%# ItemData.ScoreAllowed %>" />
                </div>
            </div>
            <div class="form-group">
                IP Whitelist
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-laptop"></span></span>
                    <asp:TextBox ID="txt_mxm_SkipIPList" CssClass="form-control" runat="server" Text="<%# ItemData.SkipIPList %>" />
                </div>
            </div>
       </div>
        <div class="col-lg-6 col-md-6">
         <ul class="list-group">
                <li class="list-group-item"><asp:CheckBox ID="chk_mxm_ProcessSettingsMaxMind" runat="server" Text="Assess transactions before processing" Checked="<%# ItemData.IsEnabled %>" /></li>
                <li class="list-group-item"><asp:CheckBox ID="chk_mxm_IsSkipVirtualTerminal" runat="server" Text="Skip fraud detection in Virtual Terminal" Checked="<%# ItemData.IsSkipVirtualTerminal %>" /></li>
               <li class="list-group-item"><asp:CheckBox ID="chk_mxm_IsAllowPublicEmail" runat="server" Text="Ignore public e-mail in risk scoring" Checked="<%#ItemData.IsAllowPublicEmail %>" /></li>
            </ul> 
        </div>
    </div>
</admin:FormSections>
