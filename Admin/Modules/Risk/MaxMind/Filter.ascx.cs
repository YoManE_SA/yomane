﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Risk.MaxMind
{
	public partial class Filter : Controls.AccountControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
		}

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
		{
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.MaxMind.Log.SearchFilters();
            sf.ID = rng_ID.Value;
            sf.InsertDate = rng_InsertDate.Value;

            if (apMerchant.Value.HasValue)
            {
                var account = Bll.Accounts.Account.LoadAccount(apMerchant.Value.Value);
                if (account != null && account.AccountType == Bll.Accounts.AccountType.Merchant)
                    sf.MerchantId = account.MerchantID.Value;
            }
            
            sf.Amount = rng_Amount.Value;
            sf.Currency = ddlCurrency.Value.ToNullableInt();
            TemplatePage.SetFilter(sf);
        }
    }
}