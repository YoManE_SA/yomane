﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Risk.MaxMind
{
	public class Module : Admin.CoreBasedModule
    {
		private ApplicationMenu menuItem;
		public override string Name { get { return "MaxMind Risk"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

        public Module() : base(Bll.MaxMind.Module.Current) { }

		protected override void OnInit(EventArgs e)
		{
            //Create the menu item
            menuItem = new ApplicationMenu("MaxMind Log", "~/Risk/MaxMind/0", null, 80);

            //Register route
            Application.RegisterRoute("Risk/MaxMind/{id}", "MaxMind", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;
            
            base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{						
			base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{						
			base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Risk", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
			if (page.TemplateName == "MaxMind") {
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Risk/MaxMind/Filter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/Risk/MaxMind/List.ascx"));
			}
            else if (page.TemplateName == "Merchant" && (page.ViewName == null))
			{
                page.AddControlToForm("Risk", page.LoadControl("~/Modules/Risk/MaxMind/MerchantSettings.ascx"), this, "", new Controls.SecuredObjectSelector(Bll.Merchants.Merchant.SecuredObject, Bll.MaxMind.MerchantSettings.SecuredObject).GetActivObject);
			}
		}
	}
}