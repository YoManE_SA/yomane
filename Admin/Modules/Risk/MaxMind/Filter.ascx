﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Risk.MaxMind.Filter" %>
<admin:FilterSection ID="FilterSection1" runat="server" Title="Record Information">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="ID" />
                <JQ:IntRange runat="server" ID="rng_ID" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label5" runat="server" Text="Date" />
                <JQ:DateRange runat="server" ID="rng_InsertDate" />
            </div>
        </div>
    </div>
</admin:FilterSection>
<admin:FilterSection ID="FilterSection2" runat="server" Title="General">
    <div class="form-group">
        <asp:Label ID="Label2" runat="server" Text="Merchant" />
        <admin:AccountPicker runat="server" ID="apMerchant" UseTargetID="false" LimitToType="Merchant" />
    </div>
    <div class="form-group">
        <asp:Label ID="Label4" runat="server" Text="Currency" />
        <netpay:CurrencyDropDown runat="server" ID="ddlCurrency" CssClass="form-control" />
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="Amount Range" />
                <JQ:DecimalRange runat="server" ID="rng_Amount" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <asp:Label ID="Label6" runat="server" Text="Reference" />
        <asp:TextBox runat="server" ID="txtValue" CssClass="form-control" />
    </div>
</admin:FilterSection>
