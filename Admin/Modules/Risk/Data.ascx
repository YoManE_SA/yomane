﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Risk.Data" %>
<admin:FormSections runat="server" Title="Details">
    <asp:HiddenField runat="server" ID="hfRiskItemID" Value='<%# Item.UniqueRiskItemStr %>' />

    <asp:MultiView runat="server" ActiveViewIndex='<%# (Item == null || Item.ID == 0) ? 0 : 1 %>'>
        <asp:View runat="server">

            <div class="row">
                <div class="col=lg-12 col-md-12">
                    <span>Risk Item Type</span>
                </div>
                <div class="col-lg-12 col-md-12">
                    <NP:UpdatePanel runat="server" ID="upRadioButtons" UpdateMode="Always" ChildrenAsTriggers="true" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:RadioButtonList ID="rbtRiskItemType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="RiskItemTypesList" OnSelectedIndexChanged="OnRadioRiskItemTypeSelectedIndexChanged">
                            </asp:RadioButtonList>
                        </ContentTemplate>
                    </NP:UpdatePanel>
                </div>
            </div>
            <hr />
            <NP:UpdatePanel runat="server" ID="upRiskItemTypeView" UpdateMode="Conditional" ChildrenAsTriggers="false" RenderMode="Inline">
                <ContentTemplate>
                    <asp:MultiView ID="mvRiskItemTypeView" runat="server">
                        <asp:View runat="server">
                            <!-- Black List -->
                            <div class="row">
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        Credit card number
                                           <div class="input-group">
                                               <span class="input-group-addon"><span class="fa fa-credit-card fa-fw"></span></span>
                                               <asp:TextBox runat="server" CssClass="form-control" MaxLength="20" ID="txtBlackValue" Text='<%# Item.Value %>' />
                                           </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <i class="fa fa-edit fa-fw"></i>
                                        Comment
                                        <asp:TextBox runat="server" TextMode="MultiLine" CssClass="form-control" ID="txtBlackComment" Text='<%# Item.Comment %>' />
                                    </div>
                                </div>
                            </div>
                        </asp:View>
                        <asp:View runat="server">
                            <!-- White List -->
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        Credit Card Number
                                         <div class="input-group">
                                             <span class="input-group-addon"><span class="fa fa-credit-card fa-fw"></span></span>
                                             <asp:TextBox runat="server" CssClass="form-control" MaxLength="20" ID="txtWhiteValue" Text='<%# Item.Value %>' />
                                         </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    Card Expiration
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-7">
                                                <netpay:MonthDropDown ID="mddExpirationMonth" class="form-control" runat="server"></netpay:MonthDropDown></div>
                                            <div class="col-lg-5">
                                                <netpay:YearDropDown ID="yddExpirationYear" class="form-control" runat="server" YearFrom='<%# System.DateTime.Now.Year %>' YearTo='<%# System.DateTime.Now.Year+8 %>'></netpay:YearDropDown></div>
                                        </div>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <asp:Label ID="lbMerchant" runat="server" Text="Merchant"  />
                                        <admin:AccountPicker runat="server" ID="apWhiteMerchant" UseTargetID="false" LimitToType="Merchant" />
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        Card Holder
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtCardHolder" Text='<%# Item.CardHolder %>' />
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        Level
                                        <netpay:EnumDropDown runat="server" CssClass="form-control" ViewEnumName="Netpay.Bll.Risk.RiskItem+RiskListType" ID="ddlRiskListType" />
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <i class="fa fa-edit fa-fw"></i>Comment
                                        <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" ID="txtWhiteComment" Text='<%# Item.Comment %>' />
                                    </div>
                                </div>
                            </div>
                        </asp:View>
                        <asp:View runat="server">
                            <!-- Common Black List -->
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="Label1" runat="server" Text="Merchant"  />
                                        <admin:AccountPicker runat="server" ID="apCommonMerchant" UseTargetID="false" LimitToType="Merchant" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        Value Type:
                                        <netpay:EnumDropDown runat="server" CssClass="form-control" ViewEnumName="Netpay.Bll.Risk.RiskItem+RiskValueType" ID="ddlCommonValueType" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        Value
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtCommonValue" Text='<%# Item.Value %>' />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <i class="fa fa-edit fa-fw"></i>Comment
                                        <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" ID="txtCommonComment" Text='<%# Item.Comment %>' />
                                    </div>
                                </div>
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </ContentTemplate>
            </NP:UpdatePanel>
        </asp:View>
        <asp:View runat="server">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <ul class="list-group">
                        <li class="list-group-item"><b>Risk Type:</b>
                            <asp:Label runat="server" ID="lblRiskType" Text='<%# Netpay.Web.Controls.EnumHelper.EnumNameToEnumDisplayText(Item.RiskType.ToString()) %>' /></li>
                        <li class="list-group-item"><b>Merchant:</b>
                            <asp:Label runat="server" ID="lblMerchant" Text='<%# MerchantName %>' /></li>
                        <li class="list-group-item"><b>Insert Date:</b>
                            <asp:Label runat="server" ID="lblInsertDate" Text='<%# Item.InsertDate.ToString("dd/MM/yyyy HH:mm") %>' /></li>
                        <li class="list-group-item"><b>List Type:</b>
                            <asp:Label runat="server" ID="lblListType" Text='<%# Item.ListType %>' /></li>
                        <li class="list-group-item"><b>Value Type:</b>
                            <asp:Label runat="server" ID="lblValueType" Text='<%# Item.ValueType %>' /></li>
                        <li class="list-group-item"><b>Value:</b>
                            <asp:Label runat="server" ID="lblValue" Text='<%# Item.Value %>' /></li>
                        <li class="list-group-item"><b>Display:</b>
                            <asp:Label runat="server" ID="lblDisplay" Text='<%# Item.Display %>' /></li>
                        <li class="list-group-item"><b>Source:</b>
                            <asp:Label runat="server" ID="lblSource" Text='<%# Item.Source %>' /></li>
                        <li class="list-group-item"><b>Duration:</b>
                            <asp:Label runat="server" ID="lblDuration" Text='<%# Item.Duration %>' /></li>
                        <li class="list-group-item"><b>Comment:</b>
                            <asp:Label runat="server" ID="lblComment" Text='<%# Item.Comment %>' /></li>
                    </ul>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>

</admin:FormSections>
