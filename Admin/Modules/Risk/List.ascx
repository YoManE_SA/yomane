﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Risk.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Risk Items">
    <Header>
        <admin:LegendMultiColors runat="server" Items='<%# Netpay.Bll.Risk.RiskItem.BlackListLegendMultiColor %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="RiskItemUniqueId" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <%-- Legend Color --%>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <div class="div-wrap-cell-test-transaction">
                                <span style="background-color: <%# Eval("BlackListLegendVal") != null? Netpay.Bll.Risk.RiskItem.BlackListLegendMultiColor.MapHtmlMultiColorLeft((Netpay.Bll.Risk.RiskItem.BlackListLegendValue)Eval("BlackListLegendVal")) : System.Drawing.ColorTranslator.ToHtml(System.Drawing.Color.Transparent) %>;" class="legend-multi-item"></span>
                                <span style="background-color: <%# Eval("BlackListLegendVal") != null? Netpay.Bll.Risk.RiskItem.BlackListLegendMultiColor.MapHtmlMultiColorRight((Netpay.Bll.Risk.RiskItem.BlackListLegendValue)Eval("BlackListLegendVal")) : System.Drawing.ColorTranslator.ToHtml(System.Drawing.Color.Transparent) %>;" class="legend-multi-item"></span>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Risk Type --%>
                    <asp:TemplateField HeaderText="Risk Type">
                        <ItemTemplate>
                            <%# Netpay.Web.Controls.EnumHelper.EnumNameToEnumDisplayText(((Netpay.Bll.Risk.RiskItem.RiskItemType)Eval("RiskType")).ToString()) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Merchant  --%>
                    <asp:TemplateField HeaderText="Merchant">
                        <ItemTemplate>
                            <%# GetMerchantName((int?)Eval("MerchantID")) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Date --%>
                    <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <%# Eval("InsertDate") != null ? ((System.DateTime)Eval("InsertDate")).ToString("dd/MM/yyyy HH:mm") : "" %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Source --%>
                    <asp:BoundField DataField="Source" HeaderText="Source" />

                    <%-- Value Type --%>
                    <asp:BoundField DataField="ValueType" HeaderText="Value Type" />

                    <%-- Value --%>
                    <asp:TemplateField HeaderText="Value">
                        <ItemTemplate>
                            <asp:Literal runat="server" Text='<%# GetValue() %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- List Type  --%>
                    <asp:BoundField DataField="ListType" HeaderText="List Type" />

                    <%-- Comment --%>
                    <asp:BoundField DataField="Comment" HeaderText="Comment" />

                    <%-- Delete button --%>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnRemove" runat="server" CommandName="RemoveItem" OnCommand="btnRemove_Command" CommandArgument='<%#  Eval("RiskItemUniqueId") %>' OnClientClick="event.cancelBubble=true; var isConfirmed = confirm('Are you sure?'); return isConfirmed;"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd='<%# Netpay.Bll.Risk.RiskItem.SecuredObject.HasPermission(Netpay.Infrastructure.Security.PermissionValue.Add) %>' />
    </Footer>
</admin:ListSection>
