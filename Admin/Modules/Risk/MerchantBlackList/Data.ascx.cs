﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;


namespace Netpay.Admin.Modules.Risk.MerchantBlackList
{
    public partial class Data : Controls.TemplateControlBase
    {
        public Bll.MerchantBlackList.MerchantBlackListSettings Item { get { return TemplatePage.GetItemData<Bll.MerchantBlackList.MerchantBlackListSettings>(); } set { TemplatePage.SetItemData(value); } }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += Save_Click;
            TemplatePage.PageController.DeleteItem += Delete_Click;
            base.OnLoad(e);
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            Item = Netpay.Bll.MerchantBlackList.MerchantBlackListSettings.Load(hfItemID.Value.ToNullableInt().GetValueOrDefault());
            Item.Delete();
        }

        protected override void DataBindChildren()
        {

            if (Item == null) Item = new Bll.MerchantBlackList.MerchantBlackListSettings();
            base.DataBindChildren();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            if (Item == null) //Add new item flow
            {
                //Create new item and give it the values that were created in the NewItem event of the Module file.
                Item = new Bll.MerchantBlackList.MerchantBlackListSettings();
                Item.InsertDate = DateTime.Parse(InsertedDate.Text);
                Item.InsertedBy = InsertedBy.Text;
            }
            
            
            Item.FirstName = FirstName.Text;
            Item.LastName = LastName.Text;
            Item.City= City.Text;
            Item.Street= Streat.Text;
            Item.Phone= Phone.Text;
            Item.Cellular = Cellular.Text;
            Item.IDNumber = IDNumber.Text;
            Item.CompanyName= CompanyName.Text;
            Item.CompanyLegalName= CompanyLegalName.Text;
            Item.CompanyLegalNumber= CompanyLegalNumber.Text;
            Item.CompanyStreet= CompanyStreet.Text;
            Item.CompanyCity= CompanyCity.Text;
            Item.CompanyPhone= CompanyPhone.Text;
            Item.CompanyFax= CompanyFax.Text;
            Item.Mail= ComapnyMail.Text;
            Item.URL= URL.Text;
            Item.MerchantSupportEmail= MerchantSupportEmail.Text;
            Item.MerchantSupportPhoneNum= MerchantSupportPhoneNum.Text;
            Item.IpOnReg= IpOnReg.Text;
            Item.PaymentPayeeName = PaymentPayeeName.Text;
            Item.PaymentBank= PaymentBank.Text;
            Item.PaymentBranch= PaymentBranch.Text;
            Item.PaymentAccount= PaymentAccount.Text;

            Item.Save();
        }
    }
}