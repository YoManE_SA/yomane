﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Risk.MerchantBlackList.Filter" %>
<admin:FilterSection ID="FilterSection1" runat="server" Title="Merchant">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label14" runat="server" Text="Merchant" />
                <admin:AccountPicker runat="server" LimitToType="Merchant" UseTargetID="false" ID="apMerchant" />
            </div>
        </div>
        <%--<div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label14" runat="server" Text="ID" />
                <asp:TextBox runat="server" ID="ID" CssClass="form-control" />
            </div>
        </div>--%>

        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="Merchant No." />
                <asp:TextBox runat="server" ID="MerchantNo" CssClass="form-control" />
            </div>
        </div>
    </div>
    <div class="row">
        <%--<div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label3" runat="server" Text="Company name" />
                <asp:TextBox runat="server" ID="CompanyName" CssClass="form-control" />
            </div>
        </div>--%>

        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label5" runat="server" Text="Legal number" />
                <asp:TextBox runat="server" ID="LegalNumber" CssClass="form-control" />
            </div>
        </div>
    </div>

</admin:FilterSection>
<admin:FilterSection ID="FilterSection12" runat="server" Title="Personal">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label2" runat="server" Text="ID number" />
                <asp:TextBox runat="server" ID="IDNumber" CssClass="form-control" />
            </div>
        </div>

        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label4" runat="server" Text="First name" />
                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label6" runat="server" Text="Last name" />
                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" />
            </div>
        </div>


    </div>
</admin:FilterSection>
<admin:FilterSection ID="FilterSection2" runat="server" Title="Contact">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label7" runat="server" Text="Phone" />
                <asp:TextBox runat="server" ID="Phone" CssClass="form-control" />
            </div>
        </div>

        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label8" runat="server" Text="Fax" />
                <asp:TextBox runat="server" ID="Fax" CssClass="form-control" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label9" runat="server" Text="Cellular" />
                <asp:TextBox runat="server" ID="Cellular" CssClass="form-control" />
            </div>
        </div>

        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label10" runat="server" Text="Mail" />
                <asp:TextBox runat="server" ID="Mail" CssClass="form-control" />
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label11" runat="server" Text="URL" />
                <asp:TextBox runat="server" ID="URL" CssClass="form-control" />
            </div>
        </div>
    </div>

</admin:FilterSection>
<admin:FilterSection ID="FilterSection3" runat="server"  Title="Filter Settings">
    <div class="row">
        <div class="col-md-4">
            <asp:RadioButtonList ID="rbtIsOr" runat="server" RepeatLayout="UnorderedList" CssClass="radiobutton-list">
                <asp:ListItem Text="AND" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="OR" Value="0"></asp:ListItem>
            </asp:RadioButtonList>
        </div>

    </div>
</admin:FilterSection>
