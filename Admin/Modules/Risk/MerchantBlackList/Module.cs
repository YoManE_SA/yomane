﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Risk.MerchantBlackList
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuItem;
        public override string Name { get { return "Merchant Black List"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.MerchantBlackList.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //create menu
            menuItem = new ApplicationMenu("MerchantBlackList Log", "~/Risk/MerchantBlackList/0", null, 90);

            //Register route
            Application.RegisterRoute("Risk/MerchantBlackList/{id}", "MerchantBlackList", typeof(Controls.DataTablePage),module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {                        
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {                     
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Risk", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "MerchantBlackList")
            {
                page.PageController.LoadItem += LoadMerchantBlackListItem;
                page.PageController.NewItem += PageController_NewItem;
                page.PageController.PostSaveItem += PageController_PostSaveItem;
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Risk/MerchantBlackList/Filter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/Risk/MerchantBlackList/List.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Risk/MerchantBlackList/Data.ascx"), this, "", Bll.MerchantBlackList.MerchantBlackListSettings.SecuredObject);
            }
        }

        private void PageController_PostSaveItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            var item = page.GetItemData<Bll.MerchantBlackList.MerchantBlackListSettings>();
            if (item == null) return;

            page.PageController.DataKey = new DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", item.ID } });
        }

        void LoadMerchantBlackListItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            if (page.ItemID != null) page.SetItemData(Bll.MerchantBlackList.MerchantBlackListSettings.Load(page.ItemID.GetValueOrDefault()));
        }

        private void PageController_NewItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            var NewItem = new Bll.MerchantBlackList.MerchantBlackListSettings()
            {
                InsertDate = DateTime.Now,
                InsertedBy = Web.WebUtils.IsLoggedin ? Web.WebUtils.LoggedUser.UserName : ""
            };
            page.SetItemData(NewItem);
        }
    }
}