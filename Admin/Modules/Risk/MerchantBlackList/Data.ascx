﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Modules.Risk.MerchantBlackList.Data" %>

<admin:FormSections runat="server" Title="Contact Details">
    <asp:HiddenField runat="server" ID="hfItemID" Value='<%# Item.ID %>' />
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                Inserted Date
                <asp:TextBox runat="server" CssClass="form-control" ReadOnly="true" ID="InsertedDate" Text='<%#Item!=null ? Item.InsertDate.ToString(): "" %> ' />
            </div>
            <div class="form-group">
                First Name
				<asp:TextBox runat="server" CssClass="form-control" ID="FirstName" Text='<%# Item.FirstName%>' />
            </div>

            <div class="form-group">
                City
				<asp:TextBox runat="server" CssClass="form-control" ID="City" Text='<%# Item.City %>' />
            </div>
            <div class="form-group">
                Phone
                        <asp:TextBox runat="server" CssClass="form-control" ID="Phone" Text='<%# Item.Phone%>' />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                InsertedBy
				<asp:TextBox runat="server" CssClass="form-control" ReadOnly="true" ID="InsertedBy" Text='<%#Item!=null ? Item.InsertedBy : "" %> ' />
            </div>
            <div class="form-group">
                Last Name
				<asp:TextBox runat="server" CssClass="form-control" ID="LastName" Text='<%# Item.LastName%>' />
            </div>
            <div class="form-group">
                Streat
				<asp:TextBox runat="server" CssClass="form-control" ID="Streat" Text='<%# Item.Street%>' />
            </div>
            <div class="form-group">
                Mobile
        				<asp:TextBox runat="server" CssClass="form-control" ID="Cellular" Text='<%# Item.Cellular%>' />
            </div>
        </div>
    </div>
</admin:FormSections>

<admin:FormSections runat="server" Title="Company Details">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                ID Number
				<asp:TextBox runat="server" CssClass="form-control" ID="IDNumber" Text='<%# Item.IDNumber%>' />
            </div>
            <div class="form-group">
                Legal Number
				<asp:TextBox runat="server" CssClass="form-control" ID="CompanyLegalNumber" Text='<%# Item.CompanyLegalNumber%>' />
            </div>
            <div class="form-group">
                City
				<asp:TextBox runat="server" CssClass="form-control" ID="CompanyCity" Text='<%# Item.CompanyCity%>' />
            </div>
            <div class="form-group">
                Phone 
				<asp:TextBox runat="server" CssClass="form-control" ID="CompanyPhone" Text='<%# Item.CompanyPhone%>' />
            </div>
            <div class="form-group">
                Mail
				<asp:TextBox runat="server" CssClass="form-control" ID="ComapnyMail" Text='<%# Item.Mail%>' />
            </div>
            <div class="form-group">
                Support Phone 
				<asp:TextBox runat="server" CssClass="form-control" ID="MerchantSupportPhoneNum" Text='<%# Item.MerchantSupportPhoneNum%>' />
            </div>
            <div class="form-group">
                IpOnReg
				<asp:TextBox runat="server" CssClass="form-control" ID="IpOnReg" Text='<%# Item.IpOnReg%>' />
            </div>
            <div class="form-group">
                Beneficiary
				<asp:TextBox runat="server" CssClass="form-control" ID="PaymentPayeeName" Text='<%# Item.PaymentPayeeName%>' />
            </div>
   
            <div class="form-group">
                Bank
				<asp:TextBox runat="server" CssClass="form-control" ID="PaymentBank" Text='<%# Item.PaymentBank%>' />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Name 
        				<asp:TextBox runat="server" CssClass="form-control" ID="CompanyName" Text='<%# Item.CompanyName%>' />
            </div>
            <div class="form-group">
                Legal Name
				<asp:TextBox runat="server" CssClass="form-control" ID="CompanyLegalName" Text='<%# Item.CompanyLegalName%>' />
            </div>
            <div class="form-group">
                Street 
				<asp:TextBox runat="server" CssClass="form-control" ID="CompanyStreet" Text='<%# Item.CompanyStreet%>' />
            </div>
            <div class="form-group">
                Fax 
				<asp:TextBox runat="server" CssClass="form-control" ID="CompanyFax" Text='<%# Item.CompanyFax%>' />
            </div>

            <div class="form-group">
                URL
				<asp:TextBox runat="server" CssClass="form-control" ID="URL" Text='<%# Item.URL%>' />
            </div>
            <div class="form-group">
                Support Email
				<asp:TextBox runat="server" CssClass="form-control" ID="MerchantSupportEmail" Text='<%# Item.MerchantSupportEmail%>' />
            </div>
            <div class="form-group">
                Account
				<asp:TextBox runat="server" CssClass="form-control" ID="PaymentAccount" Text='<%# Item.PaymentAccount%>' />
            </div>
                     <div class="form-group">
                Branch
				<asp:TextBox runat="server" CssClass="form-control" ID="PaymentBranch" Text='<%# Item.PaymentBranch %>' />
            </div>
              </div>
         </div>
</admin:FormSections>
