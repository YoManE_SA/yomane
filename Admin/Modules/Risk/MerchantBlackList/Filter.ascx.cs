﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Risk.MerchantBlackList
{
    public partial class Filter : Controls.AccountControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {            
            var sf = new Bll.MerchantBlackList.MerchantBlackListSettings.SearchFilters();
            if (Page.Request.QueryString.ToString().Contains("SearchMerchantID"))
            {
                int Id = int.Parse(Page.Request.QueryString["SearchMerchantID"]);
                Bll.Merchants.Merchant item = Bll.Merchants.Merchant.Load(Id);

                sf.CompanyID = item.ID;
                sf.MerchantNo = item.Number;
                sf.CompanyName = item.Name;
                sf.CompanyLegalNumber = item.LegalNumber;
                sf.IDNumber = item.OwnerSSN;
                sf.FirstName = item.ContactFirstName;
                sf.LastName = item.ContactLastName;
                sf.Phone = item.ContactPhone;
                sf.Fax = item.Fax;
                sf.Cellular = item.ContactMobilePhone;
                sf.Mail = item.Mail;
                sf.URL = item.Url;
                sf.IsOrCondition = true;
                TemplatePage.SetFilter(sf);
            }
            else
            {
                if (apMerchant.Value.HasValue)
                {
                    var merchant = Bll.Merchants.Merchant.LoadByAccountId(apMerchant.Value.Value);
                    if (merchant != null)
                    {
                        sf.MerchantId = merchant.ID;
                        sf.CompanyName = merchant.Name;
                    }
                }
                sf.CompanyLegalNumber = LegalNumber.Text;
                sf.MerchantNo = MerchantNo.Text;
                sf.IDNumber = IDNumber.Text;
                sf.FirstName = FirstName.Text;
                sf.LastName = LastName.Text;
                sf.Phone = Phone.Text;
                sf.Fax = Fax.Text;
                sf.Cellular = Cellular.Text;
                sf.Mail = Mail.Text;
                sf.URL = URL.Text;
                sf.IsOrCondition = rbtIsOr.SelectedValue=="0";
                TemplatePage.SetFilter(sf);
            }
        }
    }
}