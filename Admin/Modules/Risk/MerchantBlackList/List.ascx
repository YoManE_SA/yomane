﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Risk.MerchantBlackList.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Merchant Black List">
     
    <%--<Header>
        <admin:LegendColors ID="LegendColors1" runat="server" Items='<%# Netpay.Bll.MaxMind.Log.StatusColor %>' FloatRight="true" />
    </Header>--%>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" AutoGenerateColumns="false" PageSize="25" BubbleLoadEvent="true" DataKeyNames="ID">
                <Columns>
                    <asp:BoundField HeaderText="First Name" NullDisplayText="---" DataField="FirstName" />
                    <asp:BoundField HeaderText="Last Name" NullDisplayText="---" DataField="LastName" />
                    <asp:BoundField HeaderText="Contact Phone" NullDisplayText="---" DataField="Phone" />
                    <asp:BoundField HeaderText="Contact Mobile" NullDisplayText="---" DataField="Cellular" />
                    <asp:BoundField HeaderText="ID Number" NullDisplayText="---" DataField="IDNumber" />
                    <asp:BoundField HeaderText="Name" NullDisplayText="---" DataField="CompanyName" />
                    <asp:BoundField DataField="CompanyLegalNumber" NullDisplayText="---" HeaderText="Legal Number"  />
                    <asp:BoundField DataField="CompanyPhone" NullDisplayText="---" HeaderText="Phone" />
                    <asp:BoundField DataField="CompanyFax" NullDisplayText="---" HeaderText="Fax" />
                    <asp:BoundField DataField="Mail" NullDisplayText="---" HeaderText="Mail" />
                    <asp:BoundField DataField="URL" NullDisplayText="---" HeaderText="URL"  />
                    <asp:BoundField DataField="MerchantSupportEmail" NullDisplayText="---" HeaderText="Support Mail" />
                    <asp:BoundField DataField="MerchantSupportPhoneNum" NullDisplayText="---" HeaderText="Support Phone"  />
                    <asp:TemplateField HeaderText="Search" >  
                            <ItemTemplate>
                            <asp:HyperLink
                                Style="cursor: pointer"
                                runat="server"
                                onclick='<%# "OpenPop(" + "\"" +  string.Format("../../Merchants/0?MerchantBlackListID={0}", Eval("ID")) + "\"" + ",\"Unsettled Balance\",\"1330\",\"820\" , \"1\"); event.cancelBubble=true; return false;"  %>'
                                Text="Merchants"
                                Target="_blank" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd='<%# Netpay.Bll.MerchantBlackList.MerchantBlackListSettings.SecuredObject.HasPermission(Netpay.Infrastructure.Security.PermissionValue.Add) %>' />
    </Footer>
</admin:ListSection>
