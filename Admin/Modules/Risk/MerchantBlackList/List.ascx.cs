﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Risk.MerchantBlackList
{
    public partial class List : Controls.AccountControlBase
    {
        public Bll.MerchantBlackList.MerchantBlackListSettings Item { get { return TemplatePage.GetItemData<Bll.MerchantBlackList.MerchantBlackListSettings>(); } set { TemplatePage.SetItemData(value); } }
        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            var sf = TemplatePage.GetFilter<Bll.MerchantBlackList.MerchantBlackListSettings.SearchFilters>();
            rptList.DataSource = Bll.MerchantBlackList.MerchantBlackListSettings.Search(sf, rptList);
        }

       
        protected void Edit(object sender, CommandEventArgs e)
        {
            
        }
        protected void Delete(object sender, CommandEventArgs e)
        {

        }
    }
}