﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll.Risk;

namespace Netpay.Admin.Risk
{
	public partial class List : Controls.TemplateControlBase
	{
        protected Dictionary<int, string> m_merchantNames;

        protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
            var list = Bll.Risk.RiskItem.Search(TemplatePage.GetFilter<Bll.Risk.RiskItem.SearchFilters>(), rptList);
            if (list != null)
                m_merchantNames = Bll.Accounts.Account.GetMerchantNames((list.Where(i => i.MerchantID != null).Select(i => (int)i.MerchantID).ToList()));
            rptList.DataSource = list;
		}

        protected string GetValue()
        {
            if ((RiskItem.RiskValueType)Eval("ValueType") != RiskItem.RiskValueType.AccountValue1)
            {
                return Eval("Value").ToString();
            }
            else
            {
                return Eval("Display").ToString();
            }
        }

        protected string GetMerchantName(int? id)
        {
            if (id == null)
                return null;
            string ret;
            if (!m_merchantNames.TryGetValue((int)id, out ret)) return null;
            return ret;
        }
               
        protected void btnRemove_Command(object sender, CommandEventArgs e)
        {
            string riskItemUniqueStr = (string)e.CommandArgument;
            Bll.Risk.RiskItem riskItem = Bll.Risk.RiskItem.RiskItemFromUniqueStr(riskItemUniqueStr);
            riskItem.Delete();

            ((sender as LinkButton).Page as Controls.DataTablePage).PageController.Mode = Netpay.Admin.Controls.DataPageController.ViewMode.FilterList;
            ((sender as LinkButton).Page as Controls.DataTablePage).PageController.ListView.DataBind();
            ((sender as LinkButton).Page as Controls.DataTablePage).PageController.ListView.Update();
        }
    }
}
