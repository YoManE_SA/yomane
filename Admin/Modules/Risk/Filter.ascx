﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Risk.Filter" %>
<admin:FilterSection runat="server" Title="Record Information">
    <!-- In current implementation ID has no meaning as it is a union of 3 tables 
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="ID" />
                <JQ:IntRange runat="server" ID="rng_ID" />
            </div>
        </div>
    </div>
    -->
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label3" runat="server" Text="Insert Date" />
                <JQ:DateRange runat="server" ID="rng_InsertDate" />
            </div>
        </div>
    </div>
</admin:FilterSection>
<admin:FilterSection runat="server" Title="General">
    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <asp:Label runat="server" Text="Risk Item Types" />
            </div>
            <div class="col-lg-12">
                <netpay:EnumMultiSelect runat="server" ID="emsRiskType" CssClass="checkbox-list" ViewEnumName="Netpay.Bll.Risk.RiskItem+RiskItemType" RepeatLayout="UnorderedList" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <asp:Label ID="Label2" runat="server" Text="Merchant"  />
                <admin:AccountPicker runat="server" ID="apMerchant" UseTargetID="false" LimitToType="Merchant" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <asp:Label ID="Label7" runat="server" Text="List Type" />
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa "></span></span>
                    <netpay:EnumDropDown runat="server" ID="ddlListType" CssClass="form-control" ViewEnumName="Netpay.Bll.Risk.RiskItem+RiskListType" />
                </div>
            </div>
        </div>
    </div>
</admin:FilterSection>
<admin:FilterSection runat="server" Title="Search Value">
    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <asp:Label runat="server" Text="Value Type" />
                <netpay:EnumDropDown runat="server" ID="ddlValueType" CssClass="form-control" ViewEnumName="Netpay.Bll.Risk.RiskItem+RiskValueType" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <asp:Label ID="Label6" runat="server" Text="Value" />
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-credit-card"></span></span>
                    <asp:TextBox runat="server" ID="txtValue" CssClass="form-control" placeholder="Value" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-8">
                <asp:Label ID="Label4" runat="server" Text="Method First 6" />
                <asp:TextBox runat="server" ID="txtPaymentFirst6" CssClass="form-control" />
            </div>
            <div class="col-lg-4">
                <asp:Label ID="Label5" runat="server" Text="Method Last 4" />
                <asp:TextBox runat="server" ID="txtPaymentLast4" CssClass="form-control" />
            </div>
        </div>
    </div>
</admin:FilterSection>
