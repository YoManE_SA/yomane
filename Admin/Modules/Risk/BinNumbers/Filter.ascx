﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Risk.BinNumbers.Filter" %>
<%@ Register Src="~/Controls/SelectCountry.ascx" TagPrefix="CUC" TagName="SelectCountry" %>
<%@ Register Src="~/Controls/SelectPaymentMethod.ascx" TagPrefix="CUC" TagName="SelectPaymentMethod" %>

<admin:FilterSection runat="server" Title="Search">

    <div class="row">
        <div class="col-lg-12">
            Countries:
			    <CUC:SelectCountry runat="server" ID="scCountries" CssClass="form-control-multiline" />
            <br />
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12">
            Card Types:
			    <CUC:SelectPaymentMethod runat="server" ID="spmPaymentMethod" CssClass="form-control-multiline" />
            <br />
        </div>
    </div>

    <hr />
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="Bin Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>

</admin:FilterSection>
