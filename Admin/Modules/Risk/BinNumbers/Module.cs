﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Risk.BinNumbers
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuItemBinNumbers;

        public override string Name { get { return "BinNumbers"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Manage credit card bin numbers."; } }

        public Module() : base(Bll.PaymentMethods.BinNumbers.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuItemBinNumbers = new ApplicationMenu("Bin Numbers", "~/Risk/BinNumbers", null, 70);

            //Register route
            Application.RegisterRoute("Risk/BinNumbers", "BinNumbers", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {           
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {               
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Risk", menuItemBinNumbers);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItemBinNumbers.Parent.RemoveChild(menuItemBinNumbers);
            base.OnUninstallAdmin(e);
        }
        

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "BinNumbers")
            {
                page.PageController.LoadItem += PageController_LoadItemBinNumbers;
                page.AddControlToList(page.LoadControl("~/Modules/Risk/BinNumbers/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Risk/BinNumbers/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Risk/BinNumbers/Data.ascx"));
            }
        }

        void PageController_LoadItemBinNumbers(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            if (page.ItemID != null) page.SetItemData(Netpay.Bll.PaymentMethods.CreditCardBin.Load(page.ItemID.GetValueOrDefault()));

            page.FormButtons.EnableSave = false;
        }
    }
}