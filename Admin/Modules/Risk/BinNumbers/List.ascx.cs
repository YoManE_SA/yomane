﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Admin.Modules.Utils;

namespace Netpay.Admin.Risk.BinNumbers
{
	public partial class List : Controls.TemplateControlBase
	{
        protected Dictionary<int, string> accountNames;

        protected void List_DataBinding(object sender, EventArgs e)
        {
            var list =  Bll.PaymentMethods.CreditCardBin.Search(TemplatePage.GetFilter<Bll.PaymentMethods.CreditCardBin.SearchFilters>(), rptList);
            //accountNames = Bll.Accounts.Account.GetNames(list.Select(i => i.Account_id).ToList());
            rptList.DataSource = list;
        }

        protected string GetAccountName(int acccountId)
        {
            string ret;
            if (!accountNames.TryGetValue(acccountId, out ret)) return null;
            return ret;
        }
    }
}