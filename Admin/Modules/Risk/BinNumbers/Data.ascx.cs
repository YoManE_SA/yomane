﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Risk.BinNumbers
{
    public partial class Data : Controls.TemplateControlBase
    {
        public Bll.PaymentMethods.CreditCardBin Item { get { return TemplatePage.GetItemData<Bll.PaymentMethods.CreditCardBin>(); } set { TemplatePage.SetItemData(value); } }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += Save_Click;
            TemplatePage.PageController.DeleteItem += Delete_Click;
            base.OnLoad(e);
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            Item = Netpay.Bll.PaymentMethods.CreditCardBin.Load(hfFileID.Value.ToNullableInt().GetValueOrDefault());
            Item.Delete();
        }

        protected override void DataBindChildren()
        {
            if (Item == null) Item = new Bll.PaymentMethods.CreditCardBin();
            base.DataBindChildren();
        }
        
        protected void Save_Click(object sender, EventArgs e)
        {
            if (Item != null)
                return; // Don't support editing bin-numbers. Only add new or delete.
                Item = new Netpay.Bll.PaymentMethods.CreditCardBin();

            int binNumber;
            if (!int.TryParse(txtBinNumber.Text, out binNumber))
                return; //TBD: Report error
            Item.BINNumber = binNumber;
            Item.BIN = txtBinNumber.Text;
            Item.BINLen = txtBinNumber.Text.Length;

            Item.CountryIsoCode = ddlCountry.ISOCode;

            Item.PaymentMethodID = (CommonTypes.PaymentMethodEnum)ddlPaymentMethod.Value.ToNullableEnumByValue<CommonTypes.PaymentMethodEnum>();
            Item.CCName = Bll.PaymentMethods.PaymentMethod.Cache.Where(i => i.ID == (int)Item.PaymentMethodID).Select(i => i.Abbreviation).FirstOrDefault();
            // Item.CCType = TBD;
            Item.InsertDate = DateTime.UtcNow;

            Item.Save();
        }


        protected void File_Command(object sender, CommandEventArgs e)
        {
        }
    }
}