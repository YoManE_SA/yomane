﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Risk.BinNumbers.List" %>
<admin:ListSection ID="ListSectionBinNumbers" runat="server" Title="Bin Numbers">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" OnDataBinding="List_DataBinding" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="ID" />
                    <asp:BoundField HeaderText="BIN" DataField="BIN" SortExpression="BIN" />
                    <asp:TemplateField HeaderText="Country">
                        <ItemTemplate>
                            <%# Netpay.Admin.Modules.Utils.CountryMixins.GetCountryName((string)Eval("CountryIsoCode")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Card Type">
                        <ItemTemplate>
                            <%# Netpay.Admin.Modules.Utils.PaymentMethodMixins.GetCardName((int)Eval("PaymentMethodID")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Import Date" DataField="ImportDate" SortExpression="ImportDate" />
                    <asp:BoundField HeaderText="Insert Date" DataField="InsertDate" SortExpression="InsertDate" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd='<%# Netpay.Bll.PaymentMethods.CreditCardBin.SecuredObject.HasPermission(Netpay.Infrastructure.Security.PermissionValue.Add) %>' />
    </Footer>
</admin:ListSection>

