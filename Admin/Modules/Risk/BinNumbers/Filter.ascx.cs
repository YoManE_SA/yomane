﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Risk.BinNumbers
{
	public partial class Filter : Controls.TemplateControlBase
	{
        protected List<string> CardTypes { get; set; }

        protected void Page_Load(object sender, EventArgs e)
		{
            if (!TemplatePage.IsCallback) CardTypes = Bll.PaymentMethods.PaymentMethod.Cache.Select(p => p.Name).ToList();
            UpdateSearchFilter();
            TemplatePage.PageController.Search += PageController_Search;
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.PaymentMethods.CreditCardBin.SearchFilters();
            sf.CountriesCode = scCountries.Value;
            sf.PaymentMethods = spmPaymentMethod.Value;
            sf.BINRange = rngID.Value;

            /*
            sf.ID = apFileName.Value.HasValue ? new Range<int?>(apFileName.Value) : rngID.Value;
            sf.FileDate = rngFileDate.Value;

            sf.FileTypes = new List<string>();
            foreach (ListItem l in fileTypes.Items)
                if (l.Selected) sf.FileTypes.Add(l.Value);

            */
            TemplatePage.SetFilter(sf);
        }

    }
}