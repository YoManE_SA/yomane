﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Risk.BinNumbers.Data" %>
<admin:FormSections runat="server" Title="Details">
    <asp:HiddenField runat="server" ID="hfFileID" Value='<%# Item.ID %>' />

    <asp:MultiView runat="server" ActiveViewIndex='<%# (Item == null || Item.ID == 0) ? 0 : 1 %>'>
        <asp:View runat="server">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="form-group">
                        BIN:
                        <asp:TextBox ID="txtBinNumber" runat="server" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        Country
                       <netpay:CountryDropDown CssClass="form-control" runat="server" ID="ddlCountry" />
                    </div>
                    <div class="form-group">
                        Card Type
            			<netpay:PaymentMethodDropDown CssClass="form-control" runat="server" ID="ddlPaymentMethod" />
                    </div>
                </div>
            </div> 
        </asp:View>
        <asp:View runat="server">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <ul class="list-group">
                        <li class="list-group-item">BIN:
                            <asp:Label runat="server" Text='<%# Item != null ? Item.BIN : "" %>' /></li>
                        <li class="list-group-item">Country:
                            <asp:Label runat="server" Text='<%# Item != null ? Netpay.Admin.Modules.Utils.CountryMixins.GetCountryName(Item.CountryIsoCode) : "" %>' /></li>
                        <li class="list-group-item">Card Type
                            <asp:Label runat="server" Text='<%# Item != null ? Netpay.Admin.Modules.Utils.PaymentMethodMixins.GetCardName((int)Item.PaymentMethodID) : "" %>' /></li>
                        <li class="list-group-item">Import Date:
                            <asp:Label runat="server" Text='<%# Item != null ? Item.ImportDate.ToString() : "" %>' /></li>
                        <li class="list-group-item">Insert Date:
                            <asp:Label runat="server" Text='<%# Item != null ? Item.InsertDate.ToString() : "" %>' /></li>
                    </ul>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>

</admin:FormSections>
