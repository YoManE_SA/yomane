﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Risk
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
		}

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
		{
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var selectedRiskTypesStr = emsRiskType.SelectedItems;
            List<Bll.Risk.RiskItem.RiskItemType> selectedRiskTypes = emsRiskType.SelectedToEnumValues<Bll.Risk.RiskItem.RiskItemType>();

            var sf = new Bll.Risk.RiskItem.SearchFilters();
            sf.ID = rng_ID.Value;
            sf.RiskTypes = selectedRiskTypes;
            sf.InsertDate = rng_InsertDate.Value;
            if (apMerchant.Value.HasValue)
            {
                var account = Bll.Accounts.Account.LoadAccount(apMerchant.Value.Value);
                if (account != null && account.AccountType == Bll.Accounts.AccountType.Merchant)
                sf.MerchantID = account.MerchantID.Value;
            }
            
            sf.ValueType = ddlValueType.Value.ToNullableEnumByValue<Bll.Risk.RiskItem.RiskValueType>();
            sf.ListType = ddlListType.Value.ToNullableEnumByValue<Bll.Risk.RiskItem.RiskListType>();
            sf.Value = txtValue.Text.NullIfEmpty();
            sf.Account1First6 = txtPaymentFirst6.Text.NullIfEmpty();
            sf.Account1Last4 = txtPaymentLast4.Text.NullIfEmpty();
            TemplatePage.SetFilter(sf);
        }

    }
}